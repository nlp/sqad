<s>
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
</s>
<s>
Neil	Neil	k1gMnSc1
Alden	Aldna	k1gFnPc2
Armstrong	Armstrong	k1gMnSc1
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
roku	rok	k1gInSc2
1969	#num#	k4
<g/>
Astronaut	astronaut	k1gMnSc1
USAF	USAF	kA
<g/>
/	/	kIx~
<g/>
NASA	NASA	kA
Státní	státní	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgNnSc4d1
Datum	datum	k1gNnSc4
narození	narození	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1930	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Wapakoneta	Wapakoneta	k1gFnSc1
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2012	#num#	k4
(	(	kIx(
<g/>
82	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Cincinnati	Cincinnat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
Předchozízaměstnání	Předchozízaměstnání	k1gNnSc1
</s>
<s>
zkušební	zkušební	k2eAgInSc1d1
pilot	pilot	k1gInSc1
Čas	čas	k1gInSc1
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
</s>
<s>
8	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
14	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
0	#num#	k4
minut	minuta	k1gFnPc2
Kosmonaut	kosmonaut	k1gMnSc1
od	od	k7c2
</s>
<s>
1958	#num#	k4
(	(	kIx(
<g/>
krátce	krátce	k6eAd1
<g/>
,	,	kIx,
USAF	USAF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1960	#num#	k4
(	(	kIx(
<g/>
USAF	USAF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1962	#num#	k4
(	(	kIx(
<g/>
NASA	NASA	kA
<g/>
)	)	kIx)
Mise	mise	k1gFnSc1
</s>
<s>
Gemini	Gemin	k1gMnPc1
8	#num#	k4
<g/>
,	,	kIx,
Apollo	Apollo	k1gMnSc1
11	#num#	k4
Znaky	znak	k1gInPc7
misí	mise	k1gFnSc7
</s>
<s>
Kosmonaut	kosmonaut	k1gMnSc1
do	do	k7c2
</s>
<s>
duben	duben	k1gInSc1
1970	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Neil	Neil	k1gMnSc1
Alden	Aldna	k1gFnPc2
Armstrong	Armstrong	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1930	#num#	k4
Wapakoneta	Wapakoneta	k1gFnSc1
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2012	#num#	k4
Cincinnati	Cincinnati	k1gFnPc2
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
<g/>
,	,	kIx,
astronaut	astronaut	k1gMnSc1
a	a	k8xC
univerzitní	univerzitní	k2eAgMnSc1d1
profesor	profesor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
vstoupil	vstoupit	k5eAaPmAgMnS
na	na	k7c4
povrch	povrch	k1gInSc4
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Než	než	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
astronautem	astronaut	k1gMnSc7
<g/>
,	,	kIx,
sloužil	sloužit	k5eAaImAgMnS
jako	jako	k9
pilot	pilot	k1gMnSc1
v	v	k7c6
námořnictvu	námořnictvo	k1gNnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
bojoval	bojovat	k5eAaImAgMnS
ve	v	k7c6
válce	válka	k1gFnSc6
v	v	k7c6
Koreji	Korea	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
odchodu	odchod	k1gInSc6
z	z	k7c2
námořnictva	námořnictvo	k1gNnSc2
vystudoval	vystudovat	k5eAaPmAgMnS
letecké	letecký	k2eAgNnSc1d1
inženýrství	inženýrství	k1gNnSc1
na	na	k7c6
Purdueově	Purdueův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
zkušebním	zkušební	k2eAgMnSc7d1
pilotem	pilot	k1gMnSc7
Národního	národní	k2eAgInSc2d1
poradního	poradní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
letectví	letectví	k1gNnSc4
v	v	k7c6
dnešním	dnešní	k2eAgNnSc6d1
Armstrongově	Armstrongův	k2eAgNnSc6d1
leteckém	letecký	k2eAgNnSc6d1
výzkumném	výzkumný	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
při	při	k7c6
více	hodně	k6eAd2
než	než	k8xS
900	#num#	k4
letech	léto	k1gNnPc6
účastnil	účastnit	k5eAaImAgMnS
testů	test	k1gInPc2
řady	řada	k1gFnSc2
typů	typ	k1gInPc2
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
se	se	k3xPyFc4
postupně	postupně	k6eAd1
zapojil	zapojit	k5eAaPmAgMnS
do	do	k7c2
programu	program	k1gInSc2
letectva	letectvo	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgFnPc2d1
Man	mana	k1gFnPc2
In	In	k1gFnSc1
Space	Space	k1gFnSc1
Soonest	Soonest	k1gFnSc1
<g/>
,	,	kIx,
testování	testování	k1gNnSc1
raketového	raketový	k2eAgInSc2d1
letounu	letoun	k1gInSc2
North	North	k1gMnSc1
American	American	k1gMnSc1
X-	X-	k1gMnSc1
<g/>
15	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
vývoje	vývoj	k1gInSc2
raketoplánu	raketoplán	k1gInSc2
Boeing	boeing	k1gInSc1
X-20	X-20	k1gMnSc1
Dyna-Soar	Dyna-Soar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1962	#num#	k4
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
oddílu	oddíl	k1gInSc2
astronautů	astronaut	k1gMnPc2
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc3
prvním	první	k4xOgInSc7
kosmickým	kosmický	k2eAgInSc7d1
letem	let	k1gInSc7
byla	být	k5eAaImAgFnS
mise	mise	k1gFnSc1
Gemini	Gemin	k2eAgMnPc1d1
8	#num#	k4
v	v	k7c6
březnu	březen	k1gInSc6
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
ní	on	k3xPp3gFnSc2
se	se	k3xPyFc4
loď	loď	k1gFnSc1
Gemini	Gemin	k2eAgMnPc1d1
8	#num#	k4
setkala	setkat	k5eAaPmAgFnS
a	a	k8xC
spojila	spojit	k5eAaPmAgFnS
s	s	k7c7
bezpilotním	bezpilotní	k2eAgInSc7d1
raketovým	raketový	k2eAgInSc7d1
stupněm	stupeň	k1gInSc7
Agena	Ageno	k1gNnSc2
TV-	TV-	k1gFnSc2
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronauti	astronaut	k1gMnPc1
Armstrong	Armstrong	k1gMnSc1
a	a	k8xC
David	David	k1gMnSc1
Scott	Scott	k1gMnSc1
tak	tak	k6eAd1
uskutečnili	uskutečnit	k5eAaPmAgMnP
první	první	k4xOgFnSc4
spojení	spojení	k1gNnSc2
dvou	dva	k4xCgNnPc2
těles	těleso	k1gNnPc2
na	na	k7c6
oběžné	oběžný	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInPc3
a	a	k8xC
posledním	poslední	k2eAgInSc7d1
letem	let	k1gInSc7
Armstronga	Armstrong	k1gMnSc2
byla	být	k5eAaImAgFnS
mise	mise	k1gFnSc1
Apollo	Apollo	k1gNnSc1
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
Michael	Michael	k1gMnSc1
Collins	Collins	k1gInSc4
kroužil	kroužit	k5eAaImAgMnS
ve	v	k7c6
velitelském	velitelský	k2eAgInSc6d1
modulu	modul	k1gInSc6
kolem	kolem	k7c2
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
a	a	k8xC
Buzz	Buzz	k1gMnSc1
Aldrin	aldrin	k1gInSc4
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1969	#num#	k4
jako	jako	k8xC,k8xS
první	první	k4xOgMnPc1
lidé	člověk	k1gMnPc1
na	na	k7c6
Měsíci	měsíc	k1gInSc6
přistáli	přistát	k5eAaPmAgMnP,k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armstrong	Armstrong	k1gMnSc1
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
vystoupil	vystoupit	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
člověk	člověk	k1gMnSc1
na	na	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
povrch	povrch	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
získal	získat	k5eAaPmAgMnS
tím	ten	k3xDgNnSc7
světovou	světový	k2eAgFnSc4d1
proslulost	proslulost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1971	#num#	k4
odešel	odejít	k5eAaPmAgMnS
z	z	k7c2
NASA	NASA	kA
a	a	k8xC
vyučoval	vyučovat	k5eAaImAgInS
na	na	k7c6
Cincinnatské	Cincinnatský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
byl	být	k5eAaImAgInS
mluvčím	mluvčí	k1gMnSc7
a	a	k8xC
členem	člen	k1gInSc7
správních	správní	k2eAgFnPc2d1
rad	rada	k1gFnPc2
několika	několik	k4yIc2
společností	společnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
2012	#num#	k4
na	na	k7c4
komplikace	komplikace	k1gFnPc4
po	po	k7c6
operaci	operace	k1gFnSc6
srdce	srdce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
<g/>
,	,	kIx,
studium	studium	k1gNnSc1
<g/>
,	,	kIx,
vojenský	vojenský	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
</s>
<s>
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1930	#num#	k4
ve	v	k7c6
městě	město	k1gNnSc6
Wapakoneta	Wapakoneto	k1gNnSc2
ve	v	k7c6
státě	stát	k1gInSc6
Ohio	Ohio	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnPc7
rodiči	rodič	k1gMnPc7
byli	být	k5eAaImAgMnP
Stephen	Stephen	k2eAgMnSc1d1
Koenig	Koenig	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
a	a	k8xC
Viola	Viola	k1gFnSc1
Louise	Louis	k1gMnSc2
Engelová	Engelová	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
měl	mít	k5eAaImAgInS
dva	dva	k4xCgMnPc4
mladší	mladý	k2eAgMnPc4d2
sourozence	sourozenec	k1gMnPc4
<g/>
,	,	kIx,
June	jun	k1gMnSc5
a	a	k8xC
Deana	Dean	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc4
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
auditorem	auditor	k1gMnSc7
pracujícím	pracující	k1gMnPc3
pro	pro	k7c4
ohijskou	ohijský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
,	,	kIx,
rodina	rodina	k1gFnSc1
se	se	k3xPyFc4
proto	proto	k8xC
často	často	k6eAd1
stěhovala	stěhovat	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Už	už	k6eAd1
v	v	k7c6
dětství	dětství	k1gNnSc6
se	se	k3xPyFc4
zapálil	zapálit	k5eAaPmAgMnS
pro	pro	k7c4
létání	létání	k1gNnSc4
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
viděl	vidět	k5eAaImAgInS
letadla	letadlo	k1gNnPc4
jako	jako	k8xS,k8xC
dvouletý	dvouletý	k2eAgInSc4d1
<g/>
,	,	kIx,
když	když	k8xS
ho	on	k3xPp3gMnSc4
otec	otec	k1gMnSc1
vzal	vzít	k5eAaPmAgMnS
na	na	k7c4
letecké	letecký	k2eAgInPc4d1
závody	závod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letadle	letadlo	k1gNnSc6
seděl	sedět	k5eAaImAgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
šesti	šest	k4xCc6
letech	léto	k1gNnPc6
při	při	k7c6
vyhlídkovém	vyhlídkový	k2eAgInSc6d1
letu	let	k1gInSc6
ve	v	k7c6
Fordu	ford	k1gInSc6
Tri-Motor	Tri-Motor	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
sedmi	sedm	k4xCc6
letech	léto	k1gNnPc6
si	se	k3xPyFc3
vydělal	vydělat	k5eAaPmAgMnS
první	první	k4xOgInPc4
peníze	peníz	k1gInPc4
–	–	k?
za	za	k7c4
sekání	sekání	k1gNnSc4
trávy	tráva	k1gFnSc2
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
<g/>
,	,	kIx,
později	pozdě	k6eAd2
si	se	k3xPyFc3
přivydělával	přivydělávat	k5eAaImAgMnS
pochůzkami	pochůzka	k1gFnPc7
pro	pro	k7c4
místní	místní	k2eAgFnSc4d1
drogerii	drogerie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Stavěl	stavět	k5eAaImAgMnS
letecké	letecký	k2eAgInPc4d1
modely	model	k1gInPc4
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1944	#num#	k4
docházel	docházet	k5eAaImAgInS
do	do	k7c2
leteckého	letecký	k2eAgInSc2d1
kurzu	kurz	k1gInSc2
a	a	k8xC
v	v	k7c6
patnácti	patnáct	k4xCc6
letech	léto	k1gNnPc6
získal	získat	k5eAaPmAgInS
pilotní	pilotní	k2eAgInSc1d1
průkaz	průkaz	k1gInSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
<g />
.	.	kIx.
</s>
<s hack="1">
než	než	k8xS
řidičské	řidičský	k2eAgNnSc4d1
oprávnění	oprávnění	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
aktivním	aktivní	k2eAgMnSc7d1
skautem	skaut	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
při	při	k7c6
letu	let	k1gInSc6
k	k	k7c3
Měsíci	měsíc	k1gInSc3
poslal	poslat	k5eAaPmAgMnS
pozdrav	pozdrav	k1gInSc4
zrovna	zrovna	k6eAd1
probíhajícímu	probíhající	k2eAgNnSc3d1
národnímu	národní	k2eAgNnSc3d1
setkání	setkání	k1gNnSc3
skautů	skaut	k1gMnPc2
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
skautský	skautský	k2eAgInSc1d1
emblém	emblém	k1gInSc1
byl	být	k5eAaImAgInS
mezi	mezi	k7c7
osobními	osobní	k2eAgFnPc7d1
věcmi	věc	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
do	do	k7c2
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1947	#num#	k4
začal	začít	k5eAaPmAgInS
studovat	studovat	k5eAaImF
na	na	k7c6
Purdueově	Purdueův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
(	(	kIx(
<g/>
Purdue	Purdue	k1gInSc1
University	universita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obor	obor	k1gInSc4
letecké	letecký	k2eAgNnSc4d1
inženýrství	inženýrství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
teprve	teprve	k6eAd1
druhým	druhý	k4xOgInSc7
členem	člen	k1gInSc7
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k6eAd1
přijat	přijmout	k5eAaPmNgMnS
na	na	k7c4
Massachusettský	massachusettský	k2eAgInSc4d1
technologický	technologický	k2eAgInSc4d1
institut	institut	k1gInSc4
(	(	kIx(
<g/>
MIT	MIT	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
jeden	jeden	k4xCgMnSc1
jeho	jeho	k3xOp3gMnSc1
známý	známý	k1gMnSc1
–	–	k?
absolvent	absolvent	k1gMnSc1
institutu	institut	k1gInSc2
–	–	k?
ho	on	k3xPp3gMnSc4
přesvědčil	přesvědčit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
kvalitní	kvalitní	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
nemusí	muset	k5eNaImIp3nS
odcházet	odcházet	k5eAaImF
do	do	k7c2
Massachusetts	Massachusetts	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
Purdueově	Purdueův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
létal	létat	k5eAaImAgMnS
v	v	k7c6
tamním	tamní	k2eAgNnSc6d1
leteckém	letecký	k2eAgNnSc6d1
klubu	klub	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
ve	v	k7c6
druhém	druhý	k4xOgInSc6
ročníku	ročník	k1gInSc6
hrál	hrát	k5eAaImAgMnS
v	v	k7c6
univerzitním	univerzitní	k2eAgInSc6d1
koncertním	koncertní	k2eAgInSc6d1
souboru	soubor	k1gInSc6
na	na	k7c4
barytonový	barytonový	k2eAgInSc4d1
roh	roh	k1gInSc4
<g/>
;	;	kIx,
k	k	k7c3
souboru	soubor	k1gInSc3
se	se	k3xPyFc4
hlásil	hlásit	k5eAaImAgMnS
i	i	k9
po	po	k7c6
odchodu	odchod	k1gInSc6
z	z	k7c2
NASA	NASA	kA
a	a	k8xC
poskytl	poskytnout	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
nemalý	malý	k2eNgInSc4d1
finanční	finanční	k2eAgInSc4d1
dar	dar	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Armstrongovo	Armstrongův	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
financovalo	financovat	k5eAaBmAgNnS
námořnictvo	námořnictvo	k1gNnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
v	v	k7c6
rámci	rámec	k1gInSc6
tzv.	tzv.	kA
Hollowayova	Hollowayův	k2eAgInSc2d1
plánu	plán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
plánu	plán	k1gInSc2
měl	mít	k5eAaImAgInS
za	za	k7c4
úhradu	úhrada	k1gFnSc4
nákladů	náklad	k1gInPc2
studia	studio	k1gNnSc2
student	student	k1gMnSc1
mezi	mezi	k7c7
druhým	druhý	k4xOgInSc7
a	a	k8xC
třetím	třetí	k4xOgInSc7
ročníkem	ročník	k1gInSc7
školy	škola	k1gFnSc2
odsloužit	odsloužit	k5eAaPmF
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
v	v	k7c6
námořnictvu	námořnictvo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Letouny	letoun	k1gInPc1
F9F	F9F	k1gFnSc2
Panther	Panthra	k1gFnPc2
na	na	k7c6
palubě	paluba	k1gFnSc6
letadlové	letadlový	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
Essex	Essex	k1gInSc4
roku	rok	k1gInSc2
1951	#num#	k4
</s>
<s>
Námořnictvo	námořnictvo	k1gNnSc1
ho	on	k3xPp3gMnSc4
povolalo	povolat	k5eAaPmAgNnS
do	do	k7c2
služby	služba	k1gFnSc2
v	v	k7c6
lednu	leden	k1gInSc6
1949	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základně	základna	k1gFnSc6
Pensacola	Pensacola	k1gFnSc1
prošel	projít	k5eAaPmAgInS
osmnáctiměsíčním	osmnáctiměsíční	k2eAgInSc7d1
leteckým	letecký	k2eAgInSc7d1
výcvikem	výcvik	k1gInSc7
<g/>
,	,	kIx,
během	během	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
získal	získat	k5eAaPmAgInS
kvalifikaci	kvalifikace	k1gFnSc4
námořního	námořní	k2eAgMnSc2d1
letce	letec	k1gMnSc2
<g/>
,	,	kIx,
schopného	schopný	k2eAgMnSc2d1
sloužit	sloužit	k5eAaImF
na	na	k7c6
letadlových	letadlový	k2eAgFnPc6d1
lodích	loď	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1950	#num#	k4
<g/>
,	,	kIx,
dva	dva	k4xCgInPc4
týdny	týden	k1gInPc4
po	po	k7c6
jeho	jeho	k3xOp3gFnPc6
dvacátých	dvacátý	k4xOgFnPc6
narozeninách	narozeniny	k1gFnPc6
<g/>
,	,	kIx,
námořnictvo	námořnictvo	k1gNnSc4
Armstronga	Armstrong	k1gMnSc2
vyrozumělo	vyrozumět	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
plně	plně	k6eAd1
kvalifikovaný	kvalifikovaný	k2eAgMnSc1d1
námořní	námořní	k2eAgMnSc1d1
letec	letec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Přidělen	přidělen	k2eAgInSc1d1
byl	být	k5eAaImAgInS
na	na	k7c4
základnu	základna	k1gFnSc4
v	v	k7c6
San	San	k1gFnSc6
Diegu	Dieg	k1gInSc2
<g/>
,	,	kIx,
o	o	k7c4
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
později	pozdě	k6eAd2
přešel	přejít	k5eAaPmAgInS
k	k	k7c3
51	#num#	k4
<g/>
.	.	kIx.
stíhací	stíhací	k2eAgFnSc2d1
peruti	peruť	k1gFnSc2
(	(	kIx(
<g/>
VF-	VF-	k1gFnSc1
<g/>
51	#num#	k4
<g/>
)	)	kIx)
vyzbrojené	vyzbrojený	k2eAgInPc1d1
proudovými	proudový	k2eAgFnPc7d1
F9F	F9F	k1gFnPc7
Panther	Panthra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Pantheru	Panthero	k1gNnSc6
poprvé	poprvé	k6eAd1
vzlétl	vzlétnout	k5eAaPmAgInS
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1951	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
1951	#num#	k4
poprvé	poprvé	k6eAd1
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
na	na	k7c6
letadlové	letadlový	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
USS	USS	kA
Essex	Essex	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
června	červen	k1gInSc2
eskadra	eskadra	k1gFnSc1
na	na	k7c6
palubě	paluba	k1gFnSc6
letadlové	letadlový	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
Essex	Essex	k1gInSc1
vyrazila	vyrazit	k5eAaPmAgFnS
do	do	k7c2
korejské	korejský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
prvnímu	první	k4xOgInSc3
bojovému	bojový	k2eAgInSc3d1
letu	let	k1gInSc3
v	v	k7c6
Koreji	Korea	k1gFnSc6
vzlétl	vzlétnout	k5eAaPmAgInS
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1951	#num#	k4
<g/>
;	;	kIx,
doprovázel	doprovázet	k5eAaImAgMnS
průzkumné	průzkumný	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
při	při	k7c6
fotografování	fotografování	k1gNnSc6
Sŏ	Sŏ	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
pět	pět	k4xCc4
dní	den	k1gInPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
<g/>
,	,	kIx,
při	při	k7c6
bombardování	bombardování	k1gNnSc6
skladů	sklad	k1gInPc2
západně	západně	k6eAd1
od	od	k7c2
Wonsanu	Wonsan	k1gInSc2
v	v	k7c6
nízké	nízký	k2eAgFnSc3d1
výšce	výška	k1gFnSc3
jeho	jeho	k3xOp3gNnSc4
letadlo	letadlo	k1gNnSc4
zasáhla	zasáhnout	k5eAaPmAgFnS
palba	palba	k1gFnSc1
severokorejské	severokorejský	k2eAgFnSc2d1
protiletadlové	protiletadlový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
získat	získat	k5eAaPmF
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
strojem	stroj	k1gInSc7
<g/>
,	,	kIx,
vrazil	vrazit	k5eAaPmAgMnS
křídlem	křídlo	k1gNnSc7
do	do	k7c2
asi	asi	k9
šestimetrové	šestimetrový	k2eAgFnSc2d1
tyče	tyč	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
na	na	k7c6
křídle	křídlo	k1gNnSc6
prorazila	prorazit	k5eAaPmAgFnS
asi	asi	k9
metrovou	metrový	k2eAgFnSc4d1
trhlinu	trhlina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
I	i	k9
přesto	přesto	k8xC
se	se	k3xPyFc4
dokázal	dokázat	k5eAaPmAgMnS
vrátit	vrátit	k5eAaPmF
nad	nad	k7c4
vlastní	vlastní	k2eAgNnSc4d1
území	území	k1gNnSc4
a	a	k8xC
katapultovat	katapultovat	k5eAaBmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
nasazení	nasazení	k1gNnSc2
v	v	k7c6
Koreji	Korea	k1gFnSc6
provedl	provést	k5eAaPmAgInS
78	#num#	k4
bojových	bojový	k2eAgInPc2d1
letů	let	k1gInPc2
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
121	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
vesměs	vesměs	k6eAd1
v	v	k7c6
lednu	leden	k1gInSc6
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdržel	obdržet	k5eAaPmAgMnS
Leteckou	letecký	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
za	za	k7c4
dvacet	dvacet	k4xCc4
bojových	bojový	k2eAgInPc2d1
letů	let	k1gInPc2
<g/>
,	,	kIx,
Zlatou	zlatý	k2eAgFnSc4d1
hvězdu	hvězda	k1gFnSc4
za	za	k7c4
dalších	další	k2eAgFnPc2d1
dvacet	dvacet	k4xCc4
a	a	k8xC
Medaili	medaile	k1gFnSc4
Za	za	k7c4
službu	služba	k1gFnSc4
v	v	k7c6
Koreji	Korea	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
námořnictva	námořnictvo	k1gNnSc2
odešel	odejít	k5eAaPmAgInS
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1952	#num#	k4
v	v	k7c6
hodnosti	hodnost	k1gFnSc6
mladšího	mladý	k2eAgMnSc2d2
poručíka	poručík	k1gMnSc2
(	(	kIx(
<g/>
Lieutenant	Lieutenant	k1gMnSc1
(	(	kIx(
<g/>
junior	junior	k1gMnSc1
grade	grad	k1gInSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
LTJG	LTJG	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
osm	osm	k4xCc1
let	léto	k1gNnPc2
zůstal	zůstat	k5eAaPmAgMnS
v	v	k7c6
záloze	záloha	k1gFnSc6
<g/>
,	,	kIx,
do	do	k7c2
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1960	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
služby	služba	k1gFnSc2
u	u	k7c2
námořnictva	námořnictvo	k1gNnSc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
na	na	k7c4
univerzitu	univerzita	k1gFnSc4
<g/>
,	,	kIx,
dokončil	dokončit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
roku	rok	k1gInSc2
1955	#num#	k4
s	s	k7c7
titulem	titul	k1gInSc7
bakalář	bakalář	k1gMnSc1
(	(	kIx(
<g/>
Bachelor	Bachelor	k1gMnSc1
of	of	k?
Science	Science	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1970	#num#	k4
<g/>
,	,	kIx,
absolvoval	absolvovat	k5eAaPmAgInS
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
Jižní	jižní	k2eAgFnSc2d1
Kalifornie	Kalifornie	k1gFnSc2
(	(	kIx(
<g/>
University	universita	k1gFnPc1
of	of	k?
Southern	Southern	k1gNnSc1
California	Californium	k1gNnSc2
<g/>
)	)	kIx)
magisterské	magisterský	k2eAgNnSc1d1
studium	studium	k1gNnSc1
(	(	kIx(
<g/>
s	s	k7c7
titulem	titul	k1gInSc7
Master	master	k1gMnSc1
of	of	k?
Science	Science	k1gFnSc1
<g/>
)	)	kIx)
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
oboru	obor	k1gInSc6
<g/>
,	,	kIx,
leteckém	letecký	k2eAgNnSc6d1
inženýrství	inženýrství	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
univerzitě	univerzita	k1gFnSc6
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
se	s	k7c7
studentkou	studentka	k1gFnSc7
ekonomie	ekonomie	k1gFnSc2
Janet	Janeta	k1gFnPc2
Elizabeth	Elizabeth	k1gFnSc7
Shearonovou	Shearonův	k2eAgFnSc7d1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
v	v	k7c6
lednu	leden	k1gInSc6
1956	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Janet	Janet	k1gInSc1
s	s	k7c7
ním	on	k3xPp3gMnSc7
odešla	odejít	k5eAaPmAgFnS
do	do	k7c2
Kalifornie	Kalifornie	k1gFnSc2
a	a	k8xC
nedokončila	dokončit	k5eNaPmAgFnS
studium	studium	k1gNnSc4
<g/>
,	,	kIx,
čehož	což	k3yRnSc2,k3yQnSc2
později	pozdě	k6eAd2
litovala	litovat	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zkušební	zkušební	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
</s>
<s>
Šestadvacetiletý	šestadvacetiletý	k2eAgMnSc1d1
Armstrong	Armstrong	k1gMnSc1
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k8xS,k8xC
zkušební	zkušební	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
NACA	NACA	kA
na	na	k7c6
Edwardsově	Edwardsův	k2eAgFnSc6d1
základně	základna	k1gFnSc6
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
</s>
<s>
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
univerzity	univerzita	k1gFnSc2
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
stát	stát	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
civilním	civilní	k2eAgInSc7d1
zkušebním	zkušební	k2eAgInSc7d1
pilotem	pilot	k1gInSc7
Národního	národní	k2eAgInSc2d1
poradního	poradní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
letectví	letectví	k1gNnSc4
(	(	kIx(
<g/>
NACA	NACA	kA
<g/>
,	,	kIx,
předchůdce	předchůdce	k1gMnSc2
NASA	NASA	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
zažádal	zažádat	k5eAaPmAgMnS
si	se	k3xPyFc3
o	o	k7c4
zaměstnání	zaměstnání	k1gNnSc4
v	v	k7c4
High-Speed	High-Speed	k1gInSc4
Flight	Flight	k2eAgInSc4d1
Station	station	k1gInSc4
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Armstrongovo	Armstrongův	k2eAgNnSc1d1
letecké	letecký	k2eAgNnSc1d1
výzkumné	výzkumný	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
<g/>
)	)	kIx)
na	na	k7c6
Edwardsově	Edwardsův	k2eAgFnSc6d1
základně	základna	k1gFnSc6
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebylo	být	k5eNaImAgNnS
zde	zde	k6eAd1
volné	volný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
proto	proto	k8xC
přijal	přijmout	k5eAaPmAgMnS
místo	místo	k1gNnSc4
v	v	k7c6
jiném	jiný	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
NACA	NACA	kA
<g/>
,	,	kIx,
Lewisově	Lewisův	k2eAgFnSc6d1
laboratoři	laboratoř	k1gFnSc6
leteckého	letecký	k2eAgInSc2d1
pohonu	pohon	k1gInSc2
v	v	k7c6
Clevelandu	Cleveland	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
od	od	k7c2
března	březen	k1gInSc2
1955	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červenci	červenec	k1gInSc6
1955	#num#	k4
přešel	přejít	k5eAaPmAgInS
na	na	k7c4
Edwardsovu	Edwardsův	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
Edwardsově	Edwardsův	k2eAgFnSc6d1
základně	základna	k1gFnSc6
zprvu	zprvu	k6eAd1
pilotoval	pilotovat	k5eAaImAgInS
doprovodná	doprovodný	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
sledovala	sledovat	k5eAaImAgFnS
experimentální	experimentální	k2eAgInPc4d1
letouny	letoun	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Létal	létat	k5eAaImAgMnS
i	i	k9
na	na	k7c6
upravených	upravený	k2eAgInPc6d1
bombardérech	bombardér	k1gInPc6
<g/>
,	,	kIx,
při	při	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
těchto	tento	k3xDgInPc2
letů	let	k1gInPc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
první	první	k4xOgFnSc1
letecká	letecký	k2eAgFnSc1d1
nehoda	nehoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1956	#num#	k4
měla	mít	k5eAaImAgFnS
posádka	posádka	k1gFnSc1
Boeingu	boeing	k1gInSc2
B-29	B-29	k1gFnSc2
Superfortress	Superfortressa	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
Armstrongem	Armstrong	k1gMnSc7
jako	jako	k9
druhým	druhý	k4xOgMnSc7
pilotem	pilot	k1gMnSc7
<g/>
,	,	kIx,
vypustit	vypustit	k5eAaPmF
podvěšený	podvěšený	k2eAgInSc4d1
experimentální	experimentální	k2eAgInSc4d1
proudový	proudový	k2eAgInSc4d1
letoun	letoun	k1gInSc4
Douglas	Douglas	k1gMnSc1
D-558-2	D-558-2	k1gMnSc1
Skyrocket	Skyrocket	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
vystoupili	vystoupit	k5eAaPmAgMnP
do	do	k7c2
výšky	výška	k1gFnSc2
9,1	9,1	k4
km	km	kA
<g/>
,	,	kIx,
motor	motor	k1gInSc1
č.	č.	k?
4	#num#	k4
Boeingu	boeing	k1gInSc2
selhal	selhat	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistát	přistát	k5eAaPmF,k5eAaImF
s	s	k7c7
podvěšeným	podvěšený	k2eAgMnSc7d1
Skyrocketem	Skyrocket	k1gMnSc7
nemohli	moct	k5eNaImAgMnP
a	a	k8xC
neměli	mít	k5eNaImAgMnP
dostatečnou	dostatečný	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
(	(	kIx(
<g/>
338	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
vypuštění	vypuštění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitán	kapitán	k1gMnSc1
Boeingu	boeing	k1gInSc2
Stan	stan	k1gInSc1
Butchart	Butchart	k1gInSc1
proto	proto	k8xC
stočil	stočit	k5eAaPmAgMnS
letadlo	letadlo	k1gNnSc4
do	do	k7c2
klesání	klesání	k1gNnSc2
<g/>
,	,	kIx,
když	když	k8xS
získali	získat	k5eAaPmAgMnP
rychlost	rychlost	k1gFnSc4
<g/>
,	,	kIx,
vypustili	vypustit	k5eAaPmAgMnP
Skyrocket	Skyrocket	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
chvíli	chvíle	k1gFnSc6
se	se	k3xPyFc4
vrtule	vrtule	k1gFnSc1
čtvrtého	čtvrtý	k4xOgInSc2
motoru	motor	k1gInSc2
rozpadla	rozpadnout	k5eAaPmAgFnS
a	a	k8xC
její	její	k3xOp3gInPc4
úlomky	úlomek	k1gInPc4
poškodily	poškodit	k5eAaPmAgInP
motory	motor	k1gInPc1
č.	č.	k?
3	#num#	k4
a	a	k8xC
trochu	trochu	k6eAd1
i	i	k9
č.	č.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Butchart	Butcharta	k1gFnPc2
a	a	k8xC
Armstrong	Armstrong	k1gMnSc1
museli	muset	k5eAaImAgMnP
vypnout	vypnout	k5eAaPmF
poškozený	poškozený	k2eAgInSc4d1
motor	motor	k1gInSc4
č.	č.	k?
3	#num#	k4
a	a	k8xC
i	i	k9
kvůli	kvůli	k7c3
vyrovnání	vyrovnání	k1gNnSc3
momentu	moment	k1gInSc2
motor	motor	k1gInSc1
č.	č.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
pouze	pouze	k6eAd1
jedním	jeden	k4xCgInSc7
motorem	motor	k1gInSc7
v	v	k7c6
chodu	chod	k1gInSc6
poté	poté	k6eAd1
pomalu	pomalu	k6eAd1
krouživým	krouživý	k2eAgInSc7d1
letem	let	k1gInSc7
snížili	snížit	k5eAaPmAgMnP
výšku	výška	k1gFnSc4
a	a	k8xC
bezpečně	bezpečně	k6eAd1
přistáli	přistát	k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prvního	první	k4xOgInSc2
letu	let	k1gInSc2
v	v	k7c6
raketovém	raketový	k2eAgNnSc6d1
letadle	letadlo	k1gNnSc6
se	se	k3xPyFc4
Armstrong	Armstrong	k1gMnSc1
dočkal	dočkat	k5eAaPmAgMnS
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1957	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c4
Bell	bell	k1gInSc4
X-1B	X-1B	k1gFnSc2
vylétl	vylétnout	k5eAaPmAgMnS
do	do	k7c2
výšky	výška	k1gFnSc2
18,3	18,3	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
přistání	přistání	k1gNnSc6
ulomil	ulomit	k5eAaPmAgMnS
příďové	příďový	k2eAgNnSc4d1
kolo	kolo	k1gNnSc4
podvozku	podvozek	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
nehoda	nehoda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
u	u	k7c2
X-1	X-1	k1gFnSc2
již	již	k6eAd1
dříve	dříve	k6eAd2
stala	stát	k5eAaPmAgFnS
asi	asi	k9
tucetkrát	tucetkrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
příčinou	příčina	k1gFnSc7
byla	být	k5eAaImAgFnS
vada	vada	k1gFnSc1
konstrukce	konstrukce	k1gFnSc2
letadla	letadlo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
října	říjen	k1gInSc2
roku	rok	k1gInSc2
1958	#num#	k4
se	se	k3xPyFc4
účastnil	účastnit	k5eAaImAgMnS
testování	testování	k1gNnSc4
raketového	raketový	k2eAgInSc2d1
letounu	letoun	k1gInSc2
North	North	k1gMnSc1
American	American	k1gMnSc1
X-	X-	k1gMnSc1
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
listopadu	listopad	k1gInSc2
1960	#num#	k4
do	do	k7c2
července	červenec	k1gInSc2
1962	#num#	k4
uskutečnil	uskutečnit	k5eAaPmAgInS
s	s	k7c7
letouny	letoun	k1gMnPc7
X-15	X-15	k1gFnSc1
celkem	celkem	k6eAd1
sedm	sedm	k4xCc1
letů	let	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
na	na	k7c6
stroji	stroj	k1gInSc6
X-15-3	X-15-3	k1gFnPc2
dosáhl	dosáhnout	k5eAaPmAgInS
výšky	výška	k1gFnSc2
63,2	63,2	k4
km	km	kA
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
na	na	k7c6
stroji	stroj	k1gInSc6
X-15-1	X-15-1	k1gMnSc1
dosáhl	dosáhnout	k5eAaPmAgMnS
rychlosti	rychlost	k1gFnSc3
Mach	Mach	k1gMnSc1
5,74	5,74	k4
(	(	kIx(
<g/>
tj.	tj.	kA
6	#num#	k4
615	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Armstrong	Armstrong	k1gMnSc1
byl	být	k5eAaImAgMnS
zapleten	zapleten	k2eAgInSc4d1
do	do	k7c2
několika	několik	k4yIc2
incidentů	incident	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
na	na	k7c6
Edwardsově	Edwardsův	k2eAgFnSc6d1
základně	základna	k1gFnSc6
dostaly	dostat	k5eAaPmAgInP
do	do	k7c2
tamního	tamní	k2eAgInSc2d1
folklóru	folklór	k1gInSc2
a	a	k8xC
vzpomínek	vzpomínka	k1gFnPc2
pilotů	pilot	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc7
nastal	nastat	k5eAaPmAgMnS
během	během	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
šestého	šestý	k4xOgInSc2
letu	let	k1gInSc2
s	s	k7c7
X-15	X-15	k1gFnSc7
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1962	#num#	k4
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgInSc6
testoval	testovat	k5eAaImAgInS
automatický	automatický	k2eAgInSc1d1
kontrolní	kontrolní	k2eAgInSc1d1
systém	systém	k1gInSc1
letounu	letoun	k1gInSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
pomáhal	pomáhat	k5eAaImAgInS
vyvíjet	vyvíjet	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Stoupal	stoupat	k5eAaImAgInS
do	do	k7c2
výšky	výška	k1gFnSc2
více	hodně	k6eAd2
než	než	k8xS
63	#num#	k4
km	km	kA
(	(	kIx(
<g/>
nejvyšší	vysoký	k2eAgFnSc4d3
jaké	jaký	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
dosáhl	dosáhnout	k5eAaPmAgMnS
před	před	k7c7
kosmickými	kosmický	k2eAgNnPc7d1
lety	léto	k1gNnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
během	během	k7c2
sestupu	sestup	k1gInSc2
příď	příď	k1gFnSc4
letadla	letadlo	k1gNnSc2
zvedl	zvednout	k5eAaPmAgMnS
příliš	příliš	k6eAd1
a	a	k8xC
X-15	X-15	k1gMnSc1
se	se	k3xPyFc4
odrazil	odrazit	k5eAaPmAgMnS
od	od	k7c2
atmosféry	atmosféra	k1gFnSc2
zpět	zpět	k6eAd1
do	do	k7c2
43	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
domovským	domovský	k2eAgNnSc7d1
letištěm	letiště	k1gNnSc7
prolétl	prolétnout	k5eAaPmAgInS
trojnásobkem	trojnásobek	k1gInSc7
rychlosti	rychlost	k1gFnSc2
zvuku	zvuk	k1gInSc2
ve	v	k7c6
výšce	výška	k1gFnSc6
30	#num#	k4
km	km	kA
<g/>
,	,	kIx,
a	a	k8xC
dostal	dostat	k5eAaPmAgMnS
se	s	k7c7
64	#num#	k4
km	km	kA
jižně	jižně	k6eAd1
od	od	k7c2
Edwards	Edwardsa	k1gFnPc2
(	(	kIx(
<g/>
legendy	legenda	k1gFnPc1
základny	základna	k1gFnSc2
říkají	říkat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
doletěl	doletět	k5eAaPmAgInS
až	až	k9
ke	k	k7c3
stadiónu	stadión	k1gInSc3
Rose	Rose	k1gMnSc1
Bowl	Bowl	k1gMnSc1
v	v	k7c6
Pasadeně	Pasadena	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dostatečném	dostatečný	k2eAgInSc6d1
sestupu	sestup	k1gInSc6
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
na	na	k7c4
Edwards	Edwards	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
jen	jen	k9
taktak	taktak	k6eAd1
dokázal	dokázat	k5eAaPmAgMnS
přistát	přistát	k5eAaPmF,k5eAaImF
bez	bez	k7c2
nárazu	náraz	k1gInSc2
do	do	k7c2
juk	juka	k1gFnPc2
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
konci	konec	k1gInSc6
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
nejdelší	dlouhý	k2eAgInSc4d3
let	let	k1gInSc4
X-15	X-15	k1gFnSc2
jak	jak	k8xC,k8xS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
času	čas	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
vzdálenosti	vzdálenost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
North	North	k1gMnSc1
American	American	k1gMnSc1
X-15	X-15	k1gMnSc1
za	za	k7c2
letu	let	k1gInSc2
<g/>
,	,	kIx,
kolem	kolem	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
</s>
<s>
Armstrong	Armstrong	k1gMnSc1
před	před	k7c7
experimentálním	experimentální	k2eAgInSc7d1
letounem	letoun	k1gInSc7
X-	X-	k1gFnSc1
<g/>
15	#num#	k4
<g/>
,	,	kIx,
Edwardsova	Edwardsův	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
základna	základna	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1960	#num#	k4
</s>
<s>
Později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
Armstrong	Armstrong	k1gMnSc1
účastníkem	účastník	k1gMnSc7
jiného	jiný	k2eAgInSc2d1
incidentu	incident	k1gInSc2
<g/>
,	,	kIx,
při	při	k7c6
jediné	jediný	k2eAgFnSc6d1
příležitosti	příležitost	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
letěl	letět	k5eAaImAgMnS
s	s	k7c7
Chuckem	Chucko	k1gNnSc7
Yeagerem	Yeager	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
s	s	k7c7
Lockheedem	Lockheed	k1gInSc7
T-33	T-33	k1gFnSc2
zhodnotit	zhodnotit	k5eAaPmF
vhodnost	vhodnost	k1gFnSc4
vyschlého	vyschlý	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
Smith	Smith	k1gMnSc1
Ranch	Ranch	k1gMnSc1
Dry	Dry	k1gMnSc7
Lake	Lake	k1gNnSc2
jako	jako	k8xC,k8xS
místa	místo	k1gNnSc2
pro	pro	k7c4
nouzového	nouzový	k2eAgNnSc2d1
přistání	přistání	k1gNnSc2
v	v	k7c6
programu	program	k1gInSc6
X-	X-	k1gFnSc2
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
autobiografii	autobiografie	k1gFnSc6
Yeager	Yeager	k1gMnSc1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jezero	jezero	k1gNnSc1
bylo	být	k5eAaImAgNnS
po	po	k7c6
nedávných	dávný	k2eNgInPc6d1
deštích	dešť	k1gInPc6
nevhodné	vhodný	k2eNgInPc4d1
pro	pro	k7c4
přistání	přistání	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
Armstrong	Armstrong	k1gMnSc1
trval	trvat	k5eAaImAgMnS
na	na	k7c6
letu	let	k1gInSc6
v	v	k7c6
každém	každý	k3xTgInSc6
případě	případ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
pokusu	pokus	k1gInSc6
o	o	k7c4
letmé	letmý	k2eAgNnSc4d1
přistání	přistání	k1gNnSc4
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
kola	kola	k1gFnSc1
zabořila	zabořit	k5eAaPmAgFnS
do	do	k7c2
bahna	bahno	k1gNnSc2
a	a	k8xC
museli	muset	k5eAaImAgMnP
čekat	čekat	k5eAaImF
na	na	k7c4
záchranu	záchrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Armstrongovy	Armstrongův	k2eAgFnSc2d1
verze	verze	k1gFnSc2
událostí	událost	k1gFnPc2
se	se	k3xPyFc4
Yeager	Yeager	k1gMnSc1
nikdy	nikdy	k6eAd1
nepokoušel	pokoušet	k5eNaImAgMnS
rozmluvit	rozmluvit	k5eAaPmF
mu	on	k3xPp3gMnSc3
let	léto	k1gNnPc2
a	a	k8xC
úspěšně	úspěšně	k6eAd1
přistáli	přistát	k5eAaPmAgMnP,k5eAaImAgMnP
na	na	k7c6
východní	východní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
jezera	jezero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
Yeager	Yeager	k1gMnSc1
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
to	ten	k3xDgNnSc4
zkusili	zkusit	k5eAaPmAgMnP
znovu	znovu	k6eAd1
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
trochu	trochu	k6eAd1
pomaleji	pomale	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
druhém	druhý	k4xOgInSc6
přistání	přistání	k1gNnSc6
se	se	k3xPyFc4
přilepili	přilepit	k5eAaPmAgMnP
k	k	k7c3
povrchu	povrch	k1gInSc3
a	a	k8xC
Yeager	Yeager	k1gInSc1
podle	podle	k7c2
Armstronga	Armstrong	k1gMnSc2
reagoval	reagovat	k5eAaBmAgMnS
záchvatem	záchvat	k1gInSc7
smíchu	smích	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mnoho	mnoho	k4c1
zkušebních	zkušební	k2eAgMnPc2d1
pilotů	pilot	k1gMnPc2
na	na	k7c6
Edwardsově	Edwardsův	k2eAgFnSc6d1
základně	základna	k1gFnSc6
chválilo	chválit	k5eAaImAgNnS
Armstrongovy	Armstrongův	k2eAgFnPc4d1
inženýrské	inženýrský	k2eAgFnPc4d1
dovednosti	dovednost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milt	Milt	k1gMnSc1
Thompson	Thompson	k1gMnSc1
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
„	„	k?
<g/>
technicky	technicky	k6eAd1
nejschopnější	schopný	k2eAgInPc1d3
z	z	k7c2
prvních	první	k4xOgInPc2
pilotů	pilot	k1gInPc2
X-	X-	k1gFnSc4
<g/>
15	#num#	k4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bill	Bill	k1gMnSc1
Dana	Dana	k1gFnSc1
o	o	k7c6
Armstrongovi	Armstrong	k1gMnSc6
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
jeho	jeho	k3xOp3gFnSc4
mysl	mysl	k1gFnSc4
vstřebávala	vstřebávat	k5eAaImAgFnS
informace	informace	k1gFnSc1
jako	jako	k8xC,k8xS
houba	houba	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenští	vojenský	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
měli	mít	k5eAaImAgMnP
tendenci	tendence	k1gFnSc4
mít	mít	k5eAaImF
jiný	jiný	k2eAgInSc4d1
názor	názor	k1gInSc4
<g/>
,	,	kIx,
zejména	zejména	k9
pokud	pokud	k8xS
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
Yeager	Yeager	k1gInSc1
a	a	k8xC
Pete	Pete	k1gInSc1
Knight	Knighta	k1gFnPc2
<g/>
,	,	kIx,
neměli	mít	k5eNaImAgMnP
inženýrské	inženýrský	k2eAgInPc4d1
diplomy	diplom	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Knight	Knight	k1gMnSc1
se	se	k3xPyFc4
nechal	nechat	k5eAaPmAgMnS
slyšet	slyšet	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
piloti	pilot	k1gMnPc1
<g/>
–	–	k?
<g/>
inženýři	inženýr	k1gMnPc1
se	se	k3xPyFc4
častěji	často	k6eAd2
dostávali	dostávat	k5eAaImAgMnP
do	do	k7c2
potíží	potíž	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
při	při	k7c6
svém	svůj	k3xOyFgInSc6
mechanickém	mechanický	k2eAgInSc6d1
přístupu	přístup	k1gInSc6
neměli	mít	k5eNaImAgMnP
přirozený	přirozený	k2eAgInSc4d1
cit	cit	k1gInSc4
pro	pro	k7c4
létání	létání	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1962	#num#	k4
se	se	k3xPyFc4
Armstrong	Armstrong	k1gMnSc1
stal	stát	k5eAaPmAgMnS
účastníkem	účastník	k1gMnSc7
nehody	nehoda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vešla	vejít	k5eAaPmAgFnS
do	do	k7c2
folklóru	folklór	k1gInSc2
Edwardsovy	Edwardsův	k2eAgFnSc2d1
základny	základna	k1gFnSc2
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Nelliská	Nelliská	k1gFnSc1
aféra	aféra	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Měl	mít	k5eAaImAgInS
v	v	k7c4
Lockheed	Lockheed	k1gInSc4
F-104	F-104	k1gMnSc1
Starfighter	Starfighter	k1gMnSc1
obhlédnout	obhlédnout	k5eAaPmF
další	další	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
pro	pro	k7c4
nouzové	nouzový	k2eAgNnSc4d1
přistání	přistání	k1gNnSc4
<g/>
,	,	kIx,
Delamar	Delamar	k1gInSc4
Dry	Dry	k1gFnSc2
Lake	Lak	k1gFnSc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Nevadě	Nevada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armstrong	Armstrong	k1gMnSc1
při	při	k7c6
přistávání	přistávání	k1gNnSc6
podcenil	podcenit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
výšku	výška	k1gFnSc4
a	a	k8xC
neuvědomil	uvědomit	k5eNaPmAgInS
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
nemá	mít	k5eNaImIp3nS
úplně	úplně	k6eAd1
vysunutý	vysunutý	k2eAgInSc1d1
podvozek	podvozek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
dosednutí	dosednutí	k1gNnSc6
se	se	k3xPyFc4
podvozek	podvozek	k1gInSc1
začal	začít	k5eAaPmAgInS
zatahovat	zatahovat	k5eAaImF
zpět	zpět	k6eAd1
<g/>
,	,	kIx,
povedlo	povést	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
přerušit	přerušit	k5eAaPmF
přistání	přistání	k1gNnSc4
a	a	k8xC
vzlétnout	vzlétnout	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
poškozeným	poškozený	k2eAgInSc7d1
podvozkem	podvozek	k1gInSc7
a	a	k8xC
nefunkční	funkční	k2eNgFnSc7d1
vysílačkou	vysílačka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistál	přistát	k5eAaPmAgMnS,k5eAaImAgMnS
na	na	k7c6
blízké	blízký	k2eAgFnSc6d1
Nellisově	Nellisův	k2eAgFnSc6d1
letecké	letecký	k2eAgFnSc6d1
základně	základna	k1gFnSc6
přičemž	přičemž	k6eAd1
poškodil	poškodit	k5eAaPmAgInS
dráhu	dráha	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Domů	domů	k6eAd1
ho	on	k3xPp3gMnSc4
měl	mít	k5eAaImAgInS
dopravit	dopravit	k5eAaPmF
Milt	Milt	k2eAgInSc1d1
Thompson	Thompson	k1gInSc1
v	v	k7c6
dvoumístném	dvoumístný	k2eAgInSc6d1
F-	F-	k1gMnSc2
<g/>
104	#num#	k4
<g/>
B	B	kA
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
pilotoval	pilotovat	k5eAaImAgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
životě	život	k1gInSc6
(	(	kIx(
<g/>
jiné	jiný	k2eAgNnSc1d1
vícemístné	vícemístný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
zrovna	zrovna	k6eAd1
nebylo	být	k5eNaImAgNnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Thompson	Thompson	k1gMnSc1
se	se	k3xPyFc4
s	s	k7c7
velkými	velký	k2eAgFnPc7d1
potížemi	potíž	k1gFnPc7
dostal	dostat	k5eAaPmAgMnS
nad	nad	k7c4
Nellisovu	Nellisův	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
kvůli	kvůli	k7c3
silnému	silný	k2eAgInSc3d1
bočnímu	boční	k2eAgInSc3d1
větru	vítr	k1gInSc3
přistál	přistát	k5eAaImAgInS,k5eAaPmAgInS
příliš	příliš	k6eAd1
tvrdě	tvrdě	k6eAd1
<g/>
,	,	kIx,
praskla	prasknout	k5eAaPmAgFnS
mu	on	k3xPp3gMnSc3
levá	levý	k2eAgFnSc1d1
pneumatika	pneumatika	k1gFnSc1
a	a	k8xC
dráha	dráha	k1gFnSc1
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
znovu	znovu	k6eAd1
uklízet	uklízet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
oba	dva	k4xCgInPc4
piloty	pilot	k1gInPc4
přiletěl	přiletět	k5eAaPmAgMnS
kolega	kolega	k1gMnSc1
Bill	Bill	k1gMnSc1
Dana	Dana	k1gFnSc1
v	v	k7c6
T-33	T-33	k1gFnSc6
Shooting	Shooting	k1gInSc1
Star	Star	kA
<g/>
,	,	kIx,
když	když	k8xS
nelliský	nelliský	k1gMnSc1
řídící	řídící	k2eAgFnSc2d1
letů	let	k1gInPc2
viděl	vidět	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnPc3
(	(	kIx(
<g/>
příliš	příliš	k6eAd1
dlouhé	dlouhý	k2eAgNnSc4d1
<g/>
)	)	kIx)
přistání	přistání	k1gNnSc4
<g/>
,	,	kIx,
radši	rád	k6eAd2
zajistil	zajistit	k5eAaPmAgMnS
zkušebním	zkušební	k2eAgMnSc7d1
pilotům	pilot	k1gMnPc3
z	z	k7c2
Edwards	Edwardsa	k1gFnPc2
odvoz	odvoz	k1gInSc4
po	po	k7c6
silnici	silnice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jako	jako	k8xC,k8xS
zkušební	zkušební	k2eAgInSc1d1
pilot	pilot	k1gInSc1
se	se	k3xPyFc4
Armstrong	Armstrong	k1gMnSc1
účastnil	účastnit	k5eAaImAgMnS
testů	test	k1gInPc2
letadel	letadlo	k1gNnPc2
F-100A	F-100A	k1gMnSc7
a	a	k8xC
F-	F-	k1gFnSc7
<g/>
100	#num#	k4
<g/>
C	C	kA
<g/>
,	,	kIx,
F-101	F-101	k1gMnSc3
a	a	k8xC
F-	F-	k1gMnSc3
<g/>
104	#num#	k4
<g/>
A.	A.	kA
Létal	létat	k5eAaImAgMnS
i	i	k9
na	na	k7c6
F-	F-	k1gFnSc6
<g/>
105	#num#	k4
<g/>
,	,	kIx,
F-	F-	k1gFnSc1
<g/>
106	#num#	k4
<g/>
,	,	kIx,
B-	B-	k1gFnSc1
<g/>
47	#num#	k4
<g/>
,	,	kIx,
KC-	KC-	k1gFnSc1
<g/>
135	#num#	k4
<g/>
,	,	kIx,
experimentálních	experimentální	k2eAgFnPc2d1
X-1B	X-1B	k1gFnPc2
a	a	k8xC
X-	X-	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
roku	rok	k1gInSc2
1962	#num#	k4
z	z	k7c2
High-Speed	High-Speed	k1gInSc4
Flight	Flight	k2eAgInSc1d1
Station	station	k1gInSc1
odešel	odejít	k5eAaPmAgInS
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
nalétáno	nalétán	k2eAgNnSc4d1
přes	přes	k7c4
2450	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
létal	létat	k5eAaImAgInS
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
dvou	dva	k4xCgNnPc6
stech	sto	k4xCgNnPc6
typech	typ	k1gInPc6
letadel	letadlo	k1gNnPc2
<g/>
,	,	kIx,
vrtulníků	vrtulník	k1gInPc2
<g/>
,	,	kIx,
kosmických	kosmický	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
a	a	k8xC
jiných	jiný	k2eAgInPc2d1
létacích	létací	k2eAgInPc2d1
aparátů	aparát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
volných	volný	k2eAgFnPc6d1
chvílích	chvíle	k1gFnPc6
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
bezmotorovému	bezmotorový	k2eAgNnSc3d1
létání	létání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
také	také	k9
vedl	vést	k5eAaImAgInS
oddíl	oddíl	k1gInSc1
skautů	skaut	k1gMnPc2
při	při	k7c6
místní	místní	k2eAgFnSc6d1
metodistické	metodistický	k2eAgFnSc6d1
obci	obec	k1gFnSc6
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
však	však	k9
označoval	označovat	k5eAaImAgInS
za	za	k7c4
deistu	deista	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Astronaut	astronaut	k1gMnSc1
</s>
<s>
Vojenské	vojenský	k2eAgInPc1d1
programy	program	k1gInPc1
<g/>
,	,	kIx,
příchod	příchod	k1gInSc1
do	do	k7c2
NASA	NASA	kA
</s>
<s>
Prototyp	prototyp	k1gInSc4
kosmoplánu	kosmoplán	k1gInSc2
Boeing	boeing	k1gInSc1
X-20	X-20	k1gMnSc1
Dyna-Soar	Dyna-Soar	k1gMnSc1
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
1958	#num#	k4
byl	být	k5eAaImAgMnS
vybrán	vybrat	k5eAaPmNgMnS
mezi	mezi	k7c4
devět	devět	k4xCc4
budoucích	budoucí	k2eAgMnPc2d1
astronautů	astronaut	k1gMnPc2
programu	program	k1gInSc2
letectva	letectvo	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgFnPc2d1
Man	mana	k1gFnPc2
In	In	k1gMnSc1
Space	Space	k1gMnSc1
Soonest	Soonest	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Člověk	člověk	k1gMnSc1
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
co	co	k9
nejdříve	dříve	k6eAd3
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
byl	být	k5eAaImAgInS
brzký	brzký	k2eAgInSc1d1
–	–	k?
roku	rok	k1gInSc2
1960	#num#	k4
–	–	k?
pilotovaný	pilotovaný	k2eAgInSc4d1
kosmický	kosmický	k2eAgInSc4d1
let	let	k1gInSc4
v	v	k7c6
malé	malý	k2eAgFnSc6d1
jednomístné	jednomístný	k2eAgFnSc6d1
kosmické	kosmický	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
1958	#num#	k4
ale	ale	k8xC
byl	být	k5eAaImAgInS
Národní	národní	k2eAgInSc1d1
poradní	poradní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
pro	pro	k7c4
letectví	letectví	k1gNnSc4
(	(	kIx(
<g/>
NACA	NACA	kA
<g/>
)	)	kIx)
reorganizován	reorganizovat	k5eAaBmNgInS
v	v	k7c4
Národní	národní	k2eAgInSc4d1
úřad	úřad	k1gInSc4
pro	pro	k7c4
letectví	letectví	k1gNnSc4
a	a	k8xC
kosmonautiku	kosmonautika	k1gFnSc4
(	(	kIx(
<g/>
NASA	NASA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yQgMnSc3,k3yIgMnSc3
prezident	prezident	k1gMnSc1
uložil	uložit	k5eAaPmAgInS
odpovědnost	odpovědnost	k1gFnSc4
za	za	k7c4
první	první	k4xOgInPc4
pilotované	pilotovaný	k2eAgInPc4d1
kosmické	kosmický	k2eAgInPc4d1
lety	let	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenské	vojenský	k2eAgInPc1d1
letectvo	letectvo	k1gNnSc1
proto	proto	k8xC
v	v	k7c6
srpnu	srpen	k1gInSc6
1958	#num#	k4
svůj	svůj	k3xOyFgInSc4
program	program	k1gInSc4
Man	Man	k1gMnSc1
In	In	k1gMnSc1
Space	Space	k1gMnSc1
Soonest	Soonest	k1gMnSc1
zrušilo	zrušit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
dubna	duben	k1gInSc2
1960	#num#	k4
patřil	patřit	k5eAaImAgMnS
Armstrong	Armstrong	k1gMnSc1
do	do	k7c2
skupiny	skupina	k1gFnSc2
sedmi	sedm	k4xCc2
astronautů	astronaut	k1gMnPc2
zařazených	zařazený	k2eAgMnPc2d1
do	do	k7c2
programu	program	k1gInSc2
vojenského	vojenský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
vyvinutí	vyvinutí	k1gNnSc1
kosmoplánu	kosmoplán	k1gInSc2
Boeing	boeing	k1gInSc1
X-20	X-20	k1gMnSc1
Dyna-Soar	Dyna-Soar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armstrong	Armstrong	k1gMnSc1
se	se	k3xPyFc4
při	při	k7c6
letech	léto	k1gNnPc6
s	s	k7c7
upravenými	upravený	k2eAgFnPc7d1
stíhačkami	stíhačka	k1gFnPc7
F-102A	F-102A	k1gFnSc2
Delta	delta	k1gNnSc2
Dagger	Dagger	k1gMnSc1
a	a	k8xC
F5D	F5D	k1gMnSc1
Skylancer	Skylancer	k1gMnSc1
podílel	podílet	k5eAaImAgMnS
na	na	k7c6
určování	určování	k1gNnSc6
způsobu	způsob	k1gInSc2
přistání	přistání	k1gNnSc2
kosmoplánu	kosmoplán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
perspektivnější	perspektivní	k2eAgInSc4d2
však	však	k8xC
považoval	považovat	k5eAaImAgMnS
kosmický	kosmický	k2eAgInSc4d1
program	program	k1gInSc4
NASA	NASA	kA
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
v	v	k7c6
létě	léto	k1gNnSc6
1962	#num#	k4
přihlásil	přihlásit	k5eAaPmAgMnS
do	do	k7c2
jejího	její	k3xOp3gInSc2
oddílu	oddíl	k1gInSc2
astronautů	astronaut	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
vzpomínal	vzpomínat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
rozhodování	rozhodování	k1gNnSc1
nebylo	být	k5eNaImAgNnS
jednoduché	jednoduchý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
programu	program	k1gInSc6
X-15	X-15	k1gFnSc2
měl	mít	k5eAaImAgInS
reálnou	reálný	k2eAgFnSc4d1
šanci	šance	k1gFnSc4
stát	stát	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
hlavním	hlavní	k2eAgMnSc7d1
pilotem	pilot	k1gMnSc7
a	a	k8xC
X-20	X-20	k1gFnSc7
se	se	k3xPyFc4
z	z	k7c2
„	„	k?
<g/>
papírové	papírový	k2eAgFnSc2d1
<g/>
“	“	k?
lodě	loď	k1gFnSc2
mohlo	moct	k5eAaImAgNnS
stát	stát	k5eAaPmF,k5eAaImF
skutečnou	skutečný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
kosmických	kosmický	k2eAgInPc2d1
letů	let	k1gInPc2
bylo	být	k5eAaImAgNnS
zase	zase	k9
podle	podle	k7c2
Armstronga	Armstrong	k1gMnSc2
menší	malý	k2eAgNnSc4d2
riziko	riziko	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
kosmonautice	kosmonautika	k1gFnSc6
bylo	být	k5eAaImAgNnS
více	hodně	k6eAd2
bezpečnostních	bezpečnostní	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
<g/>
,	,	kIx,
záloh	záloha	k1gFnPc2
a	a	k8xC
analýz	analýza	k1gFnPc2
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
zkušební	zkušební	k2eAgMnPc1d1
piloti	pilot	k1gMnPc1
létali	létat	k5eAaImAgMnP
na	na	k7c6
hranicích	hranice	k1gFnPc6
možností	možnost	k1gFnPc2
svých	svůj	k3xOyFgInPc2
strojů	stroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Se	s	k7c7
žádostí	žádost	k1gFnSc7
se	se	k3xPyFc4
opozdil	opozdit	k5eAaPmAgInS
týden	týden	k1gInSc1
po	po	k7c6
posledním	poslední	k2eAgInSc6d1
termínu	termín	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
měl	mít	k5eAaImAgMnS
štěstí	štěstí	k1gNnSc4
–	–	k?
přítel	přítel	k1gMnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
předtím	předtím	k6eAd1
spolupracoval	spolupracovat	k5eAaImAgMnS
na	na	k7c6
Edwardsově	Edwardsův	k2eAgFnSc6d1
základně	základna	k1gFnSc6
<g/>
,	,	kIx,
přesunul	přesunout	k5eAaPmAgMnS
v	v	k7c6
nestřeženém	střežený	k2eNgInSc6d1
okamžiku	okamžik	k1gInSc6
jeho	jeho	k3xOp3gFnSc4
přihlášku	přihláška	k1gFnSc4
na	na	k7c4
správnou	správný	k2eAgFnSc4d1
hromádku	hromádka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Úspěšně	úspěšně	k6eAd1
prošel	projít	k5eAaPmAgInS
lékařskými	lékařský	k2eAgInPc7d1
testy	test	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
později	pozdě	k6eAd2
<g />
.	.	kIx.
</s>
<s hack="1">
popsal	popsat	k5eAaPmAgMnS
jako	jako	k9
obtížné	obtížný	k2eAgNnSc4d1
a	a	k8xC
občas	občas	k6eAd1
zdánlivě	zdánlivě	k6eAd1
zbytečné	zbytečný	k2eAgNnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
jedním	jeden	k4xCgMnSc7
z	z	k7c2
devíti	devět	k4xCc2
astronautů	astronaut	k1gMnPc2
druhého	druhý	k4xOgInSc2
výběru	výběr	k1gInSc2
NASA	NASA	kA
(	(	kIx(
<g/>
tiskem	tisk	k1gInSc7
nazvaných	nazvaný	k2eAgFnPc2d1
„	„	k?
<g/>
Nová	nový	k2eAgFnSc1d1
devítka	devítka	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
–	–	k?
sedmičlenná	sedmičlenný	k2eAgFnSc1d1
–	–	k?
skupina	skupina	k1gFnSc1
byla	být	k5eAaImAgFnS
vybrána	vybrat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiálně	oficiálně	k6eAd1
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
členy	člen	k1gMnPc7
oddílu	oddíl	k1gInSc2
NASA	NASA	kA
17	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1962	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Společně	společně	k6eAd1
s	s	k7c7
Elliottem	Elliotto	k1gNnSc7
See	See	k1gFnSc2
<g/>
,	,	kIx,
také	také	k6eAd1
bývalým	bývalý	k2eAgMnSc7d1
námořním	námořní	k2eAgMnSc7d1
letcem	letec	k1gMnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
byli	být	k5eAaImAgMnP
prvními	první	k4xOgMnPc7
civilisty	civilista	k1gMnPc4
mezi	mezi	k7c4
astronauty	astronaut	k1gMnPc4
NASA	NASA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
osobním	osobní	k2eAgInSc6d1
životě	život	k1gInSc6
postihla	postihnout	k5eAaPmAgFnS
Armstronga	Armstrong	k1gMnSc4
roku	rok	k1gInSc2
1962	#num#	k4
těžká	těžký	k2eAgFnSc1d1
ztráta	ztráta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Janet	Janet	k1gInSc1
měl	mít	k5eAaImAgMnS
tři	tři	k4xCgFnPc4
děti	dítě	k1gFnPc4
–	–	k?
Erica	Erica	k1gMnSc1
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Karen	Karen	k1gInSc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
Marka	Marek	k1gMnSc4
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červnu	červen	k1gInSc6
1961	#num#	k4
lékaři	lékař	k1gMnPc1
u	u	k7c2
Karen	Karna	k1gFnPc2
diagnostikovali	diagnostikovat	k5eAaBmAgMnP
nádor	nádor	k1gInSc4
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
části	část	k1gFnSc6
mozkového	mozkový	k2eAgInSc2d1
kmene	kmen	k1gInSc2
<g/>
,	,	kIx,
léčba	léčba	k1gFnSc1
neuspěla	uspět	k5eNaPmAgFnS
a	a	k8xC
v	v	k7c6
lednu	leden	k1gInSc6
1962	#num#	k4
Karen	Karna	k1gFnPc2
zemřela	zemřít	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Gemini	Gemin	k1gMnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Program	program	k1gInSc1
Gemini	Gemin	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
1965	#num#	k4
NASA	NASA	kA
oznámila	oznámit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gNnSc4
jmenování	jmenování	k1gNnSc4
velícím	velící	k2eAgMnSc7d1
pilotem	pilot	k1gMnSc7
(	(	kIx(
<g/>
tj.	tj.	kA
velitelem	velitel	k1gMnSc7
lodi	loď	k1gFnSc6
<g/>
)	)	kIx)
záložní	záložní	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
mise	mise	k1gFnSc2
Gemini	Gemin	k2eAgMnPc1d1
5	#num#	k4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
kolegou	kolega	k1gMnSc7
v	v	k7c6
posádce	posádka	k1gFnSc6
byl	být	k5eAaImAgMnS
Elliot	Elliot	k1gMnSc1
See	See	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Let	let	k1gInSc1
Gemini	Gemin	k2eAgMnPc1d1
5	#num#	k4
úspěšně	úspěšně	k6eAd1
proběhl	proběhnout	k5eAaPmAgInS
v	v	k7c6
srpnu	srpen	k1gInSc6
1965	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyzdvižení	vyzdvižený	k2eAgMnPc1d1
Gemini	Gemin	k1gMnPc1
8	#num#	k4
z	z	k7c2
hladiny	hladina	k1gFnSc2
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1965	#num#	k4
NASA	NASA	kA
zveřejnila	zveřejnit	k5eAaPmAgFnS
složení	složení	k1gNnSc4
hlavní	hlavní	k2eAgFnSc2d1
a	a	k8xC
záložní	záložní	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
letu	let	k1gInSc2
Gemini	Gemin	k1gMnPc1
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc4d1
posádku	posádka	k1gFnSc4
tvořil	tvořit	k5eAaImAgMnS
Armstrong	Armstrong	k1gMnSc1
(	(	kIx(
<g/>
velící	velící	k2eAgMnSc1d1
pilot	pilot	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
David	David	k1gMnSc1
Scott	Scott	k1gMnSc1
(	(	kIx(
<g/>
pilot	pilot	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
záložní	záložní	k2eAgFnSc4d1
posádku	posádka	k1gFnSc4
Charles	Charles	k1gMnSc1
Conrad	Conrada	k1gFnPc2
a	a	k8xC
Richard	Richard	k1gMnSc1
Gordon	Gordon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Let	let	k1gInSc1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
komplexnější	komplexní	k2eAgInSc1d2
než	než	k8xS
dosavadní	dosavadní	k2eAgMnSc1d1
<g/>
,	,	kIx,
plánovalo	plánovat	k5eAaImAgNnS
se	se	k3xPyFc4
uskutečnění	uskutečnění	k1gNnSc1
prvního	první	k4xOgNnSc2
setkání	setkání	k1gNnSc2
a	a	k8xC
spojení	spojení	k1gNnSc2
s	s	k7c7
jiným	jiný	k2eAgNnSc7d1
vesmírným	vesmírný	k2eAgNnSc7d1
tělesem	těleso	k1gNnSc7
–	–	k?
raketovým	raketový	k2eAgInSc7d1
stupněm	stupeň	k1gInSc7
Agena	Ageen	k2eAgFnSc1d1
TV-8	TV-8	k1gFnSc1
–	–	k?
a	a	k8xC
poté	poté	k6eAd1
měl	mít	k5eAaImAgMnS
Scott	Scott	k1gMnSc1
provést	provést	k5eAaPmF
druhý	druhý	k4xOgInSc4
americký	americký	k2eAgInSc4d1
výstup	výstup	k1gInSc4
do	do	k7c2
otevřeného	otevřený	k2eAgInSc2d1
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Let	let	k1gInSc1
měl	mít	k5eAaImAgInS
trvat	trvat	k5eAaImF
75	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
tj.	tj.	kA
55	#num#	k4
oběhů	oběh	k1gInPc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agena	Agena	k1gFnSc1
odstartovala	odstartovat	k5eAaPmAgFnS
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1966	#num#	k4
v	v	k7c4
15	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
3	#num#	k4
UTC	UTC	kA
<g/>
,	,	kIx,
Gemini	Gemin	k2eAgMnPc1d1
8	#num#	k4
s	s	k7c7
astronauty	astronaut	k1gMnPc7
o	o	k7c4
necelé	celý	k2eNgFnPc4d1
dvě	dva	k4xCgFnPc4
hodiny	hodina	k1gFnPc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
16	#num#	k4
<g/>
:	:	kIx,
<g/>
41	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
2	#num#	k4
UTC	UTC	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Setkání	setkání	k1gNnSc1
a	a	k8xC
spojení	spojení	k1gNnSc1
bylo	být	k5eAaImAgNnS
úspěšně	úspěšně	k6eAd1
završeno	završit	k5eAaPmNgNnS
6,5	6,5	k4
hodiny	hodina	k1gFnSc2
po	po	k7c6
startu	start	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
soulodí	soulodí	k1gNnSc1
prolétalo	prolétat	k5eAaImAgNnS,k5eAaPmAgNnS
nad	nad	k7c7
Indií	Indie	k1gFnSc7
a	a	k8xC
posádka	posádka	k1gFnSc1
byla	být	k5eAaImAgFnS
proto	proto	k8xC
bez	bez	k7c2
spojení	spojení	k1gNnSc2
s	s	k7c7
řídícím	řídící	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
otáčet	otáčet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronauti	astronaut	k1gMnPc1
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
rotaci	rotace	k1gFnSc3
zastavit	zastavit	k5eAaPmF
pomocí	pomocí	k7c2
orbitálního	orbitální	k2eAgInSc2d1
manévrovacího	manévrovací	k2eAgInSc2d1
systému	systém	k1gInSc2
(	(	kIx(
<g/>
OAMS	OAMS	kA
<g/>
)	)	kIx)
Gemini	Gemin	k2eAgMnPc1d1
a	a	k8xC
vypnutí	vypnutí	k1gNnSc6
systémů	systém	k1gInPc2
orientace	orientace	k1gFnSc2
Ageny	Agena	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
s	s	k7c7
přechodným	přechodný	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
obnovení	obnovení	k1gNnSc6
rotace	rotace	k1gFnSc2
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
tělesa	těleso	k1gNnSc2
rozpojit	rozpojit	k5eAaPmF
<g/>
,	,	kIx,
načež	načež	k6eAd1
rotace	rotace	k1gFnSc1
Gemini	Gemin	k2eAgMnPc1d1
začala	začít	k5eAaPmAgNnP
růst	růst	k1gInSc4
až	až	k9
na	na	k7c4
rychlost	rychlost	k1gFnSc4
jedné	jeden	k4xCgFnSc2
otáčky	otáčka	k1gFnSc2
za	za	k7c4
sekundu	sekunda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklé	vzniklý	k2eAgNnSc1d1
přetížení	přetížení	k1gNnSc1
bylo	být	k5eAaImAgNnS
na	na	k7c6
hranici	hranice	k1gFnSc6
únosnosti	únosnost	k1gFnSc2
a	a	k8xC
mohlo	moct	k5eAaImAgNnS
vést	vést	k5eAaImF
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
vědomí	vědomí	k1gNnSc2
astronautů	astronaut	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armstrong	Armstrong	k1gMnSc1
se	se	k3xPyFc4
Scottem	Scott	k1gInSc7
se	se	k3xPyFc4
rychle	rychle	k6eAd1
rozhodli	rozhodnout	k5eAaPmAgMnP
aktivovat	aktivovat	k5eAaBmF
motory	motor	k1gInPc4
systému	systém	k1gInSc2
řízení	řízení	k1gNnSc2
lodi	loď	k1gFnSc2
během	během	k7c2
návratu	návrat	k1gInSc2
do	do	k7c2
atmosféry	atmosféra	k1gFnSc2
(	(	kIx(
<g/>
RCS	RCS	kA
<g/>
)	)	kIx)
a	a	k8xC
stabilizovali	stabilizovat	k5eAaBmAgMnP
loď	loď	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
zkoušením	zkoušení	k1gNnSc7
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
problém	problém	k1gInSc4
způsobila	způsobit	k5eAaPmAgFnS
zaseknutá	zaseknutý	k2eAgFnSc1d1
tryska	tryska	k1gFnSc1
č.	č.	k?
8	#num#	k4
systému	systém	k1gInSc2
OAMS	OAMS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řídící	řídící	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
nařídilo	nařídit	k5eAaPmAgNnS
zrušení	zrušení	k1gNnSc4
dalšího	další	k2eAgInSc2d1
programu	program	k1gInSc2
–	–	k?
Scottova	Scottův	k2eAgInSc2d1
výstupu	výstup	k1gInSc2
–	–	k?
a	a	k8xC
předčasné	předčasný	k2eAgNnSc1d1
přistání	přistání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistání	přistání	k1gNnSc1
proběhlo	proběhnout	k5eAaPmAgNnS
hladce	hladko	k6eAd1
v	v	k7c6
Tichém	tichý	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
,	,	kIx,
asi	asi	k9
800	#num#	k4
km	km	kA
východně	východně	k6eAd1
od	od	k7c2
Okinawy	Okinawa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Let	let	k1gInSc1
trval	trvat	k5eAaImAgInS
10	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
41	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
letu	let	k1gInSc6
byl	být	k5eAaImAgMnS
Armstrong	Armstrong	k1gMnSc1
deprimovaný	deprimovaný	k2eAgMnSc1d1
ze	z	k7c2
zkrácení	zkrácení	k1gNnSc2
letu	let	k1gInSc2
<g/>
,	,	kIx,
nedosažení	dosažený	k2eNgMnPc1d1
většiny	většina	k1gFnSc2
cílů	cíl	k1gInPc2
mise	mise	k1gFnSc2
a	a	k8xC
zrušení	zrušení	k1gNnSc4
Scottova	Scottův	k2eAgInSc2d1
výstupu	výstup	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Několik	několik	k4yIc1
dní	den	k1gInPc2
po	po	k7c6
přistání	přistání	k1gNnSc6
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1966	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
velícím	velící	k2eAgMnSc7d1
pilotem	pilot	k1gMnSc7
záložní	záložní	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
letu	let	k1gInSc2
Gemini	Gemin	k1gMnPc1
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkušený	zkušený	k2eAgMnSc1d1
Armstrong	Armstrong	k1gMnSc1
byl	být	k5eAaImAgMnS
pro	pro	k7c4
svého	svůj	k3xOyFgMnSc4
kolegu	kolega	k1gMnSc4
v	v	k7c6
posádce	posádka	k1gFnSc6
<g/>
,	,	kIx,
nováčka	nováček	k1gMnSc2
Williama	William	k1gMnSc2
Anderse	Anderse	k1gFnSc2
<g/>
,	,	kIx,
spíše	spíše	k9
učitelem	učitel	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
letu	let	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
proběhl	proběhnout	k5eAaPmAgInS
v	v	k7c6
září	září	k1gNnSc6
1966	#num#	k4
<g/>
,	,	kIx,
sloužil	sloužit	k5eAaImAgInS
jako	jako	k9
„	„	k?
<g/>
capcom	capcom	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgMnSc1d1
spojař	spojař	k1gMnSc1
komunikující	komunikující	k2eAgFnSc2d1
s	s	k7c7
posádkou	posádka	k1gFnSc7
kosmické	kosmický	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
řídícím	řídící	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
letu	let	k1gInSc6
byl	být	k5eAaImAgMnS
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
spoluastronaut	spoluastronaut	k1gMnSc1
Richard	Richard	k1gMnSc1
Gordon	Gordon	k1gMnSc1
<g/>
,	,	kIx,
zástupce	zástupce	k1gMnSc1
ředitele	ředitel	k1gMnSc2
Střediska	středisko	k1gNnSc2
pilotovaných	pilotovaný	k2eAgInPc2d1
kosmických	kosmický	k2eAgInPc2d1
letů	let	k1gInPc2
NASA	NASA	kA
v	v	k7c6
Houstonu	Houston	k1gInSc6
George	Georg	k1gMnSc4
Low	Low	k1gMnSc4
a	a	k8xC
další	další	k2eAgInSc4d1
s	s	k7c7
manželkami	manželka	k1gFnPc7
vysláni	vyslán	k2eAgMnPc1d1
na	na	k7c4
několikatýdenní	několikatýdenní	k2eAgNnSc4d1
propagační	propagační	k2eAgNnSc4d1
turné	turné	k1gNnSc4
po	po	k7c6
Latinské	latinský	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
během	během	k7c2
něhož	jenž	k3xRgInSc2
navštívili	navštívit	k5eAaPmAgMnP
čtrnáct	čtrnáct	k4xCc1
měst	město	k1gNnPc2
v	v	k7c6
jedenácti	jedenáct	k4xCc6
státech	stát	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Apollo	Apollo	k1gMnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Apollo	Apollo	k1gNnSc1
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Start	start	k1gInSc1
Saturnu	Saturn	k1gInSc2
V	V	kA
s	s	k7c7
Apollem	Apollo	k1gNnSc7
11	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
listenlist	listenlist	k1gInSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
url	url	k?
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
//	//	k?
<g/>
upload	upload	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikimedia	wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
/	/	kIx~
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
commons	commons	k1gInSc1
<g/>
/	/	kIx~
<g/>
thumb	thumb	k1gInSc1
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
94	#num#	k4
<g/>
/	/	kIx~
<g/>
Gnome-speakernotes	Gnome-speakernotesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
px-Gnome-speakernotes	px-Gnome-speakernotesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
.	.	kIx.
<g/>
png	png	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
padding-left	padding-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
min-height	min-height	k1gInSc1
<g/>
:	:	kIx,
<g/>
50	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
margin	margin	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
background-position	background-position	k1gInSc4
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
left	left	k1gInSc1
<g/>
;	;	kIx,
<g/>
background-repeat	background-repeat	k1gInSc1
<g/>
:	:	kIx,
<g/>
no-repeat	no-repeat	k5eAaPmF,k5eAaBmF,k5eAaImF
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
list-style-type	list-style-typ	k1gInSc5
<g/>
:	:	kIx,
<g/>
none	none	k1gNnPc6
<g/>
;	;	kIx,
<g/>
list-style-image	list-style-image	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
margin	margin	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
li	li	k8xS
<g/>
{	{	kIx(
<g/>
padding-bottom	padding-bottom	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.5	0.5	k4
<g/>
em	em	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
li	li	k8xS
li	li	k8xS
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
91	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
padding-bottom	padding-bottom	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
malý	malý	k2eAgInSc1d1
krůček	krůček	k1gInSc1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
velký	velký	k2eAgInSc1d1
skok	skok	k1gInSc1
pro	pro	k7c4
lidstvo	lidstvo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
Měsíc	měsíc	k1gInSc1
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1969	#num#	k4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
UTC	UTC	kA
</s>
<s>
Problémy	problém	k1gInPc1
s	s	k7c7
přehráváním	přehrávání	k1gNnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nápověda	nápověda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
programu	program	k1gInSc6
Apollo	Apollo	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
listopadu	listopad	k1gInSc6
1967	#num#	k4
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
velitelem	velitel	k1gMnSc7
záložní	záložní	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
Apolla	Apollo	k1gNnSc2
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
změně	změna	k1gFnSc6
obsahu	obsah	k1gInSc2
letů	let	k1gInPc2
Apolla	Apollo	k1gNnPc4
8	#num#	k4
a	a	k8xC
9	#num#	k4
kvůli	kvůli	k7c3
problémům	problém	k1gInPc3
s	s	k7c7
výrobou	výroba	k1gFnSc7
lunárních	lunární	k2eAgInPc2d1
modulů	modul	k1gInPc2
přešel	přejít	k5eAaPmAgInS
(	(	kIx(
<g/>
v	v	k7c6
listopadu	listopad	k1gInSc6
1968	#num#	k4
<g/>
)	)	kIx)
na	na	k7c4
místo	místo	k1gNnSc4
velitele	velitel	k1gMnSc2
zálohy	záloha	k1gFnSc2
pro	pro	k7c4
Apollo	Apollo	k1gNnSc4
8	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
tehdejšího	tehdejší	k2eAgInSc2d1
systému	systém	k1gInSc2
nasazování	nasazování	k1gNnSc4
posádek	posádka	k1gFnPc2
měla	mít	k5eAaImAgFnS
záloha	záloha	k1gFnSc1
Apolla	Apollo	k1gNnSc2
8	#num#	k4
letět	letět	k5eAaImF
v	v	k7c6
Apollu	Apollo	k1gNnSc6
11	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
přistání	přistání	k1gNnSc1
na	na	k7c6
Měsíci	měsíc	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiálně	oficiálně	k6eAd1
byla	být	k5eAaImAgFnS
jako	jako	k9
posádka	posádka	k1gFnSc1
Apolla	Apollo	k1gNnSc2
11	#num#	k4
potvrzena	potvrzen	k2eAgFnSc1d1
v	v	k7c6
prosinci	prosinec	k1gInSc6
1968	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Armstrong	Armstrong	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
velitelem	velitel	k1gMnSc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnPc7
kolegy	kolega	k1gMnPc7
byli	být	k5eAaImAgMnP
pilot	pilot	k1gInSc4
velitelského	velitelský	k2eAgInSc2d1
modulu	modul	k1gInSc2
Fred	Fred	k1gMnSc1
Haise	Haise	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
lednu	leden	k1gInSc6
1969	#num#	k4
nahrazený	nahrazený	k2eAgInSc4d1
Michaelem	Michael	k1gMnSc7
Collinsem	Collins	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
pilot	pilot	k1gMnSc1
lunárního	lunární	k2eAgInSc2d1
modulu	modul	k1gInSc2
Edwin	Edwin	k2eAgInSc1d1
"	"	kIx"
<g/>
Buzz	Buzz	k1gInSc1
<g/>
"	"	kIx"
Aldrin	aldrin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zprvu	zprvu	k6eAd1
novináři	novinář	k1gMnPc1
i	i	k8xC
někteří	některý	k3yIgMnPc1
pracovníci	pracovník	k1gMnPc1
NASA	NASA	kA
soudili	soudit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
prvním	první	k4xOgMnSc7
mužem	muž	k1gMnSc7
na	na	k7c6
Měsíci	měsíc	k1gInSc6
bude	být	k5eAaImBp3nS
Aldrin	aldrin	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
analogie	analogie	k1gFnSc2
s	s	k7c7
loděmi	loď	k1gFnPc7
Gemini	Gemin	k2eAgMnPc1d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
výstupy	výstup	k1gInPc1
do	do	k7c2
vesmíru	vesmír	k1gInSc2
prováděl	provádět	k5eAaImAgMnS
pilot	pilot	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
1969	#num#	k4
se	se	k3xPyFc4
však	však	k9
vedoucí	vedoucí	k2eAgMnPc1d1
pracovníci	pracovník	k1gMnPc1
NASA	NASA	kA
Deke	Deke	k1gFnSc1
Slayton	Slayton	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
George	George	k1gFnSc1
Low	Low	k1gMnSc1
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
Gilruth	Gilruth	k1gMnSc1
a	a	k8xC
Chris	Chris	k1gInSc1
Kraft	Kraft	k1gMnSc1
dohodli	dohodnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k9
Armstrong	Armstrong	k1gMnSc1
bude	být	k5eAaImBp3nS
prvním	první	k4xOgMnSc7
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
sestoupí	sestoupit	k5eAaPmIp3nS
na	na	k7c4
povrch	povrch	k1gInSc4
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
i	i	k9
kvůli	kvůli	k7c3
jeho	jeho	k3xOp3gFnSc3
skromnosti	skromnost	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
14	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1969	#num#	k4
zdůvodnili	zdůvodnit	k5eAaPmAgMnP
Armstrongovo	Armstrongův	k2eAgNnSc4d1
prvenství	prvenství	k1gNnSc4
konstrukcí	konstrukce	k1gFnPc2
lunárního	lunární	k2eAgInSc2d1
modulu	modul	k1gInSc2
–	–	k?
venkovní	venkovní	k2eAgInSc1d1
průlez	průlez	k1gInSc1
se	se	k3xPyFc4
otvíral	otvírat	k5eAaImAgInS
dovnitř	dovnitř	k6eAd1
a	a	k8xC
doprava	doprava	k1gFnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
vpravo	vpravo	k6eAd1
sedícího	sedící	k2eAgMnSc4d1
pilota	pilot	k1gMnSc4
by	by	kYmCp3nS
proto	proto	k6eAd1
v	v	k7c6
těsném	těsný	k2eAgInSc6d1
modulu	modul	k1gInSc6
bylo	být	k5eAaImAgNnS
obtížné	obtížný	k2eAgNnSc1d1
vylézt	vylézt	k5eAaPmF
před	před	k7c7
velitelem	velitel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slayton	Slayton	k1gInSc1
k	k	k7c3
tomu	ten	k3xDgNnSc3
dodal	dodat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
podle	podle	k7c2
něj	on	k3xPp3gInSc2
by	by	kYmCp3nS
velitel	velitel	k1gMnSc1
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
první	první	k4xOgInSc4
i	i	k8xC
z	z	k7c2
protokolárních	protokolární	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Ovšem	ovšem	k9
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
čtveřice	čtveřice	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
důsledky	důsledek	k1gInPc7
umístění	umístění	k1gNnSc2
průlezu	průlez	k1gInSc2
si	se	k3xPyFc3
neuvědomovala	uvědomovat	k5eNaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
trénování	trénování	k1gNnSc6
přistání	přistání	k1gNnSc2
na	na	k7c6
Měsíci	měsíc	k1gInSc6
s	s	k7c7
letovým	letový	k2eAgInSc7d1
trenažérem	trenažér	k1gInSc7
LLRV	LLRV	kA
(	(	kIx(
<g/>
Lunar	Lunar	k1gInSc1
Landing	Landing	k1gInSc1
Research	Research	k1gInSc4
Vehicle	Vehicle	k1gFnSc2
<g/>
)	)	kIx)
došlo	dojít	k5eAaPmAgNnS
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1968	#num#	k4
k	k	k7c3
selhání	selhání	k1gNnSc3
tohoto	tento	k3xDgInSc2
trenažéru	trenažér	k1gInSc2
a	a	k8xC
jen	jen	k9
pohotová	pohotový	k2eAgFnSc1d1
katapultace	katapultace	k1gFnSc1
zachránila	zachránit	k5eAaPmAgFnS
Armstrongovi	Armstrongův	k2eAgMnPc1d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
přestálé	přestálý	k2eAgNnSc4d1
nebezpečí	nebezpečí	k1gNnSc4
Armstrong	Armstrong	k1gMnSc1
trval	trvat	k5eAaImAgMnS
na	na	k7c6
výcviku	výcvik	k1gInSc6
s	s	k7c7
LLRV	LLRV	kA
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
úspěšné	úspěšný	k2eAgNnSc4d1
přistání	přistání	k1gNnSc4
na	na	k7c6
Měsíci	měsíc	k1gInSc6
je	být	k5eAaImIp3nS
nezbytný	zbytný	k2eNgInSc1d1,k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
dostává	dostávat	k5eAaImIp3nS
letové	letový	k2eAgInPc4d1
plány	plán	k1gInPc4
pro	pro	k7c4
let	let	k1gInSc4
Apollo	Apollo	k1gNnSc4
11	#num#	k4
</s>
<s>
Mise	mise	k1gFnSc1
začala	začít	k5eAaPmAgFnS
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1969	#num#	k4
v	v	k7c4
13	#num#	k4
<g/>
:	:	kIx,
<g/>
32	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
UTC	UTC	kA
startem	start	k1gInSc7
z	z	k7c2
Kennedyho	Kennedy	k1gMnSc2
vesmírného	vesmírný	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
na	na	k7c6
mysu	mys	k1gInSc6
Canaveral	Canaveral	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
necelých	celý	k2eNgInPc6d1
dvou	dva	k4xCgInPc6
obězích	oběh	k1gInPc6
Země	zem	k1gFnSc2
třetí	třetí	k4xOgInSc1
stupeň	stupeň	k1gInSc1
nosné	nosný	k2eAgFnSc2d1
rakety	raketa	k1gFnSc2
Saturn	Saturn	k1gMnSc1
V	V	kA
uvedl	uvést	k5eAaPmAgMnS
Apollo	Apollo	k1gMnSc1
na	na	k7c4
dráhu	dráha	k1gFnSc4
k	k	k7c3
Měsíci	měsíc	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronauti	astronaut	k1gMnPc1
přestavěli	přestavět	k5eAaPmAgMnP
loď	loď	k1gFnSc4
–	–	k?
s	s	k7c7
velitelským	velitelský	k2eAgInSc7d1
modulem	modul	k1gInSc7
pojmenovaným	pojmenovaný	k2eAgInSc7d1
Columbia	Columbia	k1gFnSc1
–	–	k?
se	se	k3xPyFc4
odpojili	odpojit	k5eAaPmAgMnP
<g/>
,	,	kIx,
otočili	otočit	k5eAaPmAgMnP
ho	on	k3xPp3gNnSc4
a	a	k8xC
připojili	připojit	k5eAaPmAgMnP
k	k	k7c3
lunárnímu	lunární	k2eAgInSc3d1
modulu	modul	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nesl	nést	k5eAaImAgInS
jméno	jméno	k1gNnSc4
Eagle	Eagle	k1gFnSc2
(	(	kIx(
<g/>
Orel	Orel	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
odhodili	odhodit	k5eAaPmAgMnP
nepotřebný	potřebný	k2eNgInSc4d1
třetí	třetí	k4xOgInSc4
stupeň	stupeň	k1gInSc4
Saturnu	Saturn	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Loď	loď	k1gFnSc1
Apollo	Apollo	k1gMnSc1
byla	být	k5eAaImAgFnS
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
malou	malá	k1gFnSc7
Gemini	Gemin	k1gMnPc1
relativně	relativně	k6eAd1
prostorná	prostorný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snad	snad	k9
právě	právě	k9
možnost	možnost	k1gFnSc4
pohybu	pohyb	k1gInSc2
po	po	k7c6
lodi	loď	k1gFnSc6
byla	být	k5eAaImAgFnS
důvodem	důvod	k1gInSc7
<g/>
,	,	kIx,
proč	proč	k6eAd1
nikdo	nikdo	k3yNnSc1
z	z	k7c2
posádky	posádka	k1gFnSc2
netrpěl	trpět	k5eNaImAgMnS
nevolnostmi	nevolnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armstrong	Armstrong	k1gMnSc1
byl	být	k5eAaImAgMnS
zvláště	zvláště	k6eAd1
spokojený	spokojený	k2eAgMnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
dětském	dětský	k2eAgInSc6d1
věku	věk	k1gInSc6
byl	být	k5eAaImAgMnS
náchylný	náchylný	k2eAgMnSc1d1
ke	k	k7c3
kinetóze	kinetóza	k1gFnSc3
a	a	k8xC
trpěl	trpět	k5eAaImAgMnS
nevolnostmi	nevolnost	k1gFnPc7
<g/>
,	,	kIx,
pokud	pokud	k8xS
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
cvičil	cvičit	k5eAaImAgInS
leteckou	letecký	k2eAgFnSc4d1
akrobacii	akrobacie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Let	let	k1gInSc1
k	k	k7c3
Měsíci	měsíc	k1gInSc3
trval	trvat	k5eAaImAgInS
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volných	volný	k2eAgFnPc6d1
chvílích	chvíle	k1gFnPc6
astronauti	astronaut	k1gMnPc1
odpočívali	odpočívat	k5eAaImAgMnP
při	při	k7c6
hudbě	hudba	k1gFnSc6
<g/>
,	,	kIx,
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
si	se	k3xPyFc3
za	za	k7c7
tímto	tento	k3xDgInSc7
účelem	účel	k1gInSc7
vyžádal	vyžádat	k5eAaPmAgMnS
dvě	dva	k4xCgFnPc4
skladby	skladba	k1gFnPc4
–	–	k?
Novosvětskou	novosvětský	k2eAgFnSc4d1
symfonii	symfonie	k1gFnSc4
Antonína	Antonín	k1gMnSc2
Dvořáka	Dvořák	k1gMnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
si	se	k3xPyFc3
oblíbil	oblíbit	k5eAaPmAgMnS
už	už	k6eAd1
když	když	k8xS
hrál	hrát	k5eAaImAgMnS
v	v	k7c6
univerzitním	univerzitní	k2eAgInSc6d1
koncertním	koncertní	k2eAgInSc6d1
souboru	soubor	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
album	album	k1gNnSc1
Music	Musice	k1gFnPc2
out	out	k?
of	of	k?
the	the	k?
Moon	Moon	k1gInSc1
Samuela	Samuel	k1gMnSc2
Hoffmana	Hoffman	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Asi	asi	k9
v	v	k7c6
polovině	polovina	k1gFnSc6
cesty	cesta	k1gFnSc2
přišel	přijít	k5eAaPmAgMnS
čas	čas	k1gInSc4
na	na	k7c4
korekci	korekce	k1gFnSc4
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
17	#num#	k4
<g/>
:	:	kIx,
<g/>
21	#num#	k4
UTC	UTC	kA
19	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
<g/>
,	,	kIx,
nad	nad	k7c7
odvrácenou	odvrácený	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
Měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
Apollo	Apollo	k1gNnSc1
přešlo	přejít	k5eAaPmAgNnS
na	na	k7c4
oběžnou	oběžný	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
v	v	k7c6
17	#num#	k4
<g/>
:	:	kIx,
<g/>
47	#num#	k4
UTC	UTC	kA
<g/>
,	,	kIx,
se	se	k3xPyFc4
Armstrong	Armstrong	k1gMnSc1
a	a	k8xC
Aldrin	aldrin	k1gInSc1
v	v	k7c6
lunárním	lunární	k2eAgInSc6d1
modulu	modul	k1gInSc6
odpojili	odpojit	k5eAaPmAgMnP
od	od	k7c2
velitelského	velitelský	k2eAgInSc2d1
modulu	modul	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
zůstal	zůstat	k5eAaPmAgInS
Collins	Collins	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
19	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
je	být	k5eAaImIp3nS
motor	motor	k1gInSc1
lunárního	lunární	k2eAgInSc2d1
modulu	modul	k1gInSc2
(	(	kIx(
<g/>
DPS	DPS	kA
<g/>
)	)	kIx)
uvedl	uvést	k5eAaPmAgMnS
na	na	k7c4
sestupovou	sestupový	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
s	s	k7c7
nejnižším	nízký	k2eAgInSc7d3
bodem	bod	k1gInSc7
15	#num#	k4
km	km	kA
nad	nad	k7c7
povrchem	povrch	k1gInSc7
měsíce	měsíc	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
5	#num#	k4
zapálili	zapálit	k5eAaPmAgMnP
DPS	DPS	kA
podruhé	podruhé	k6eAd1
a	a	k8xC
zahájili	zahájit	k5eAaPmAgMnP
vlastní	vlastní	k2eAgInSc4d1
sestup	sestup	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Armstrong	Armstrong	k1gMnSc1
sestupuje	sestupovat	k5eAaImIp3nS
na	na	k7c4
povrch	povrch	k1gInSc4
Měsíce	měsíc	k1gInSc2
</s>
<s>
Při	při	k7c6
sestupu	sestup	k1gInSc6
astronauty	astronaut	k1gMnPc4
znepokojilo	znepokojit	k5eAaPmAgNnS
červené	červený	k2eAgNnSc4d1
světlo	světlo	k1gNnSc4
poplachu	poplach	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
operátor	operátor	k1gInSc4
v	v	k7c6
Houstonu	Houston	k1gInSc6
jim	on	k3xPp3gMnPc3
doporučil	doporučit	k5eAaPmAgMnS
výstrahu	výstraha	k1gFnSc4
ignorovat	ignorovat	k5eAaImF
(	(	kIx(
<g/>
šlo	jít	k5eAaImAgNnS
o	o	k7c6
přetížení	přetížení	k1gNnSc6
počítače	počítač	k1gInSc2
způsobené	způsobený	k2eAgMnPc4d1
zapnutým	zapnutý	k2eAgInSc7d1
setkávacím	setkávací	k2eAgInSc7d1
radarem	radar	k1gInSc7
<g/>
,	,	kIx,
při	při	k7c6
přistání	přistání	k1gNnSc6
nepotřebným	potřebný	k2eNgMnPc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěrečné	závěrečný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
přistání	přistání	k1gNnSc2
řídil	řídit	k5eAaImAgMnS
ručně	ručně	k6eAd1
Armstrong	Armstrong	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
vyhýbat	vyhýbat	k5eAaImF
se	se	k3xPyFc4
kamenitému	kamenitý	k2eAgInSc3d1
terénu	terén	k1gInSc3
<g/>
,	,	kIx,
proto	proto	k8xC
ve	v	k7c6
20	#num#	k4
<g/>
:	:	kIx,
<g/>
17	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
UTC	UTC	kA
přistál	přistát	k5eAaImAgInS,k5eAaPmAgInS
s	s	k7c7
méně	málo	k6eAd2
než	než	k8xS
třicetivteřinovou	třicetivteřinový	k2eAgFnSc7d1
zásobou	zásoba	k1gFnSc7
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Přistáli	přistát	k5eAaPmAgMnP,k5eAaImAgMnP
v	v	k7c6
Mare	Mare	k1gFnSc6
Tranquillitatis	Tranquillitatis	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
0	#num#	k4
<g/>
°	°	k?
<g/>
40	#num#	k4
<g/>
'	'	kIx"
<g/>
26,69	26,69	k4
<g/>
”	”	k?
N	N	kA
a	a	k8xC
23	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
'	'	kIx"
<g/>
22,69	22,69	k4
<g/>
”	”	k?
E.	E.	kA
</s>
<s>
O	o	k7c4
šest	šest	k4xCc4
hodin	hodina	k1gFnPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
ve	v	k7c6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
56	#num#	k4
UTC	UTC	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
vstoupil	vstoupit	k5eAaPmAgMnS
Armstrong	Armstrong	k1gMnSc1
jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
člověk	člověk	k1gMnSc1
na	na	k7c4
povrch	povrch	k1gInSc4
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
pronesl	pronést	k5eAaPmAgMnS
slavnou	slavný	k2eAgFnSc4d1
větu	věta	k1gFnSc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
That	That	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
one	one	k?
small	smalnout	k5eAaPmAgInS
step	step	k1gInSc1
for	forum	k1gNnPc2
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
man	man	k1gMnSc1
–	–	k?
one	one	k?
giant	giant	k1gMnSc1
leap	leap	k1gMnSc1
for	forum	k1gNnPc2
mankind	mankinda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
V	v	k7c6
českém	český	k2eAgInSc6d1
překladu	překlad	k1gInSc6
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
malý	malý	k2eAgInSc1d1
krok	krok	k1gInSc1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
obrovský	obrovský	k2eAgInSc4d1
skok	skok	k1gInSc4
pro	pro	k7c4
lidstvo	lidstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Armstrong	Armstrong	k1gMnSc1
ke	k	k7c3
konci	konec	k1gInSc3
vycházky	vycházka	k1gFnSc2
na	na	k7c6
Měsíci	měsíc	k1gInSc6
ukládá	ukládat	k5eAaImIp3nS
vzorky	vzorek	k1gInPc4
</s>
<s>
V	v	k7c6
přímém	přímý	k2eAgInSc6d1
přenosu	přenos	k1gInSc6
Armstrongův	Armstrongův	k2eAgInSc1d1
sestup	sestup	k1gInSc1
po	po	k7c6
žebříku	žebřík	k1gInSc6
a	a	k8xC
první	první	k4xOgInPc4
kroky	krok	k1gInPc4
na	na	k7c6
Měsíci	měsíc	k1gInSc6
vidělo	vidět	k5eAaImAgNnS
a	a	k8xC
slyšelo	slyšet	k5eAaImAgNnS
odhadem	odhad	k1gInSc7
na	na	k7c4
600	#num#	k4
miliónů	milión	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
pětina	pětina	k1gFnSc1
lidstva	lidstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c7
ním	on	k3xPp3gMnSc7
vystoupil	vystoupit	k5eAaPmAgMnS
Buzz	Buzz	k1gMnSc1
Aldrin	aldrin	k1gInSc4
<g/>
,	,	kIx,
společně	společně	k6eAd1
pak	pak	k6eAd1
vztyčili	vztyčit	k5eAaPmAgMnP
americkou	americký	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
a	a	k8xC
plnili	plnit	k5eAaImAgMnP
připravený	připravený	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotografovali	fotografovat	k5eAaImAgMnP
<g/>
,	,	kIx,
sbírali	sbírat	k5eAaImAgMnP
vzorky	vzorek	k1gInPc4
hornin	hornina	k1gFnPc2
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
jich	on	k3xPp3gMnPc2
přivezli	přivézt	k5eAaPmAgMnP
na	na	k7c4
Zem	zem	k1gFnSc4
22	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
rozmístili	rozmístit	k5eAaPmAgMnP
přístroje	přístroj	k1gInPc4
sady	sada	k1gFnSc2
EASEP	EASEP	kA
–	–	k?
koutový	koutový	k2eAgInSc4d1
odražeč	odražeč	k1gInSc4
a	a	k8xC
seismograf	seismograf	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Fotografování	fotografování	k1gNnSc6
měl	mít	k5eAaImAgMnS
za	za	k7c4
úkol	úkol	k1gInSc4
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
proto	proto	k8xC
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
několika	několik	k4yIc6
snímcích	snímek	k1gInPc6
pořízených	pořízený	k2eAgInPc6d1
Aldrinem	aldrin	k1gInSc7
<g/>
,	,	kIx,
kterému	který	k3yRgMnSc3,k3yQgMnSc3,k3yIgMnSc3
aparát	aparát	k1gInSc1
na	na	k7c4
chvíli	chvíle	k1gFnSc4
půjčil	půjčit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
57	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
8	#num#	k4
UTC	UTC	kA
Aldrin	aldrin	k1gInSc4
a	a	k8xC
v	v	k7c4
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
UTC	UTC	kA
i	i	k8xC
Armstrong	Armstrong	k1gMnSc1
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
modulu	modul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
8	#num#	k4
do	do	k7c2
15	#num#	k4
hodin	hodina	k1gFnPc2
odpočívali	odpočívat	k5eAaImAgMnP
(	(	kIx(
<g/>
Armstrong	Armstrong	k1gMnSc1
v	v	k7c6
nepohodlné	pohodlný	k2eNgFnSc6d1
pozici	pozice	k1gFnSc6
nemohl	moct	k5eNaImAgMnS
usnout	usnout	k5eAaPmF
a	a	k8xC
vrtěl	vrtět	k5eAaImAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
seismograf	seismograf	k1gInSc1
byl	být	k5eAaImAgInS
tak	tak	k6eAd1
citlivý	citlivý	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
zachytil	zachytit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gInPc4
pohyby	pohyb	k1gInPc4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
pak	pak	k6eAd1
už	už	k6eAd1
byl	být	k5eAaImAgInS
na	na	k7c6
programu	program	k1gInSc6
návrat	návrat	k1gInSc4
na	na	k7c4
velitelský	velitelský	k2eAgInSc4d1
modul	modul	k1gInSc4
Columbia	Columbia	k1gFnSc1
ke	k	k7c3
Collinsovi	Collins	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Měsíce	měsíc	k1gInSc2
odstartovali	odstartovat	k5eAaPmAgMnP
v	v	k7c6
17	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
UTC	UTC	kA
<g/>
,	,	kIx,
oba	dva	k4xCgInPc1
moduly	modul	k1gInPc1
se	se	k3xPyFc4
spojily	spojit	k5eAaPmAgInP
ve	v	k7c6
21	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
UTC	UTC	kA
21	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Následující	následující	k2eAgInSc4d1
den	den	k1gInSc4
se	se	k3xPyFc4
vydali	vydat	k5eAaPmAgMnP
na	na	k7c4
zpáteční	zpáteční	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
k	k	k7c3
Zemi	zem	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
tři	tři	k4xCgMnPc4
astronauti	astronaut	k1gMnPc1
v	v	k7c6
pořádku	pořádek	k1gInSc6
přistáli	přistát	k5eAaImAgMnP,k5eAaPmAgMnP
v	v	k7c6
Tichém	tichý	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1969	#num#	k4
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
okamžitě	okamžitě	k6eAd1
si	se	k3xPyFc3
vysloužili	vysloužit	k5eAaPmAgMnP
celosvětové	celosvětový	k2eAgNnSc4d1
uznání	uznání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Museli	muset	k5eAaImAgMnP
ovšem	ovšem	k9
napřed	napřed	k6eAd1
tři	tři	k4xCgInPc4
týdny	týden	k1gInPc4
prožít	prožít	k5eAaPmF
v	v	k7c6
karanténě	karanténa	k1gFnSc6
kvůli	kvůli	k7c3
ochraně	ochrana	k1gFnSc3
proti	proti	k7c3
zavlečení	zavlečení	k1gNnSc3
mimozemských	mimozemský	k2eAgInPc2d1
organismů	organismus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhý	druhý	k4xOgInSc1
Armstrongův	Armstrongův	k2eAgInSc1d1
kosmický	kosmický	k2eAgInSc1d1
let	let	k1gInSc1
dosáhl	dosáhnout	k5eAaPmAgInS
délky	délka	k1gFnSc2
8	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
3	#num#	k4
hodiny	hodina	k1gFnPc4
<g/>
,	,	kIx,
18	#num#	k4
minut	minuta	k1gFnPc2
a	a	k8xC
35	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
dva	dva	k4xCgInPc1
jeho	jeho	k3xOp3gInPc1
lety	léto	k1gNnPc7
do	do	k7c2
vesmíru	vesmír	k1gInSc2
dohromady	dohromady	k6eAd1
trvaly	trvat	k5eAaImAgInP
8	#num#	k4
dní	den	k1gInPc2
a	a	k8xC
14	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
;	;	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
2	#num#	k4
hodiny	hodina	k1gFnPc4
<g/>
,	,	kIx,
17	#num#	k4
minut	minuta	k1gFnPc2
a	a	k8xC
44	#num#	k4
sekundy	sekunda	k1gFnSc2
strávil	strávit	k5eAaPmAgInS
vycházkou	vycházka	k1gFnSc7
na	na	k7c6
povrchu	povrch	k1gInSc6
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
Měsíce	měsíc	k1gInSc2
</s>
<s>
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
Edwin	Edwin	k1gMnSc1
Aldrin	aldrin	k1gInSc1
a	a	k8xC
Michael	Michael	k1gMnSc1
Collins	Collins	k1gInSc4
se	s	k7c7
sombrery	sombrero	k1gNnPc7
obklopeni	obklopen	k2eAgMnPc1d1
davem	dav	k1gInSc7
v	v	k7c4
Ciudad	Ciudad	k1gInSc4
de	de	k?
México	México	k6eAd1
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1969	#num#	k4
</s>
<s>
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
a	a	k8xC
Michael	Michael	k1gMnSc1
Collins	Collins	k1gInSc4
s	s	k7c7
brazilským	brazilský	k2eAgNnSc7d1
vyznamenáním	vyznamenání	k1gNnSc7
<g/>
,	,	kIx,
1969	#num#	k4
</s>
<s>
Úspěch	úspěch	k1gInSc4
Apolla	Apollo	k1gNnSc2
11	#num#	k4
vláda	vláda	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
neopomněla	opomnět	k5eNaPmAgFnS
využít	využít	k5eAaPmF
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
propagaci	propagace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronauti	astronaut	k1gMnPc1
procestovali	procestovat	k5eAaPmAgMnP
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
a	a	k8xC
koncem	koncem	k7c2
září	září	k1gNnSc2
vyrazili	vyrazit	k5eAaPmAgMnP
na	na	k7c6
„	„	k?
<g/>
Velký	velký	k2eAgInSc4d1
skok	skok	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
pětačtyřicetidenní	pětačtyřicetidenní	k2eAgNnSc4d1
putování	putování	k1gNnSc4
po	po	k7c6
ne	ne	k9
méně	málo	k6eAd2
než	než	k8xS
třiadvaceti	třiadvacet	k4xCc6
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
mělo	mít	k5eAaImAgNnS
demonstrovat	demonstrovat	k5eAaBmF
dobrou	dobrý	k2eAgFnSc4d1
vůli	vůle	k1gFnSc4
Ameriky	Amerika	k1gFnSc2
a	a	k8xC
předvést	předvést	k5eAaPmF
program	program	k1gInSc4
Apollo	Apollo	k1gMnSc1
jako	jako	k9
úspěch	úspěch	k1gInSc4
celého	celý	k2eAgNnSc2d1
lidstva	lidstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
těchto	tento	k3xDgNnPc2
setkání	setkání	k1gNnPc2
s	s	k7c7
lidmi	člověk	k1gMnPc7
mělo	mít	k5eAaImAgNnS
možnost	možnost	k1gFnSc4
vidět	vidět	k5eAaImF
posádku	posádka	k1gFnSc4
Apolla	Apollo	k1gNnSc2
11	#num#	k4
ohromné	ohromný	k2eAgNnSc1d1
množství	množství	k1gNnSc3
návštěvníků	návštěvník	k1gMnPc2
<g/>
,	,	kIx,
odhaduje	odhadovat	k5eAaImIp3nS
se	s	k7c7
100	#num#	k4
až	až	k9
150	#num#	k4
miliónů	milión	k4xCgInPc2
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
okolo	okolo	k6eAd1
25	#num#	k4
tisícům	tisíc	k4xCgInPc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
s	s	k7c7
astronauty	astronaut	k1gMnPc7
podat	podat	k5eAaPmF
si	se	k3xPyFc3
ruku	ruka	k1gFnSc4
či	či	k8xC
obdržet	obdržet	k5eAaPmF
autogram	autogram	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Armstrong	Armstrong	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
mimořádně	mimořádně	k6eAd1
populárním	populární	k2eAgMnSc6d1
<g/>
,	,	kIx,
i	i	k8xC
ve	v	k7c6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
pro	pro	k7c4
Američany	Američan	k1gMnPc4
prvním	první	k4xOgMnSc7
z	z	k7c2
hrdinů	hrdina	k1gMnPc2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Navzdory	navzdory	k7c3
své	svůj	k3xOyFgFnSc3
slávě	sláva	k1gFnSc3
si	se	k3xPyFc3
Armstrong	Armstrong	k1gMnSc1
zachoval	zachovat	k5eAaPmAgMnS
pokoru	pokora	k1gFnSc4
a	a	k8xC
skromnost	skromnost	k1gFnSc4
<g/>
,	,	kIx,
popularitu	popularita	k1gFnSc4
se	se	k3xPyFc4
nesnažil	snažit	k5eNaImAgMnS
využívat	využívat	k5eAaPmF,k5eAaImF
za	za	k7c4
každou	každý	k3xTgFnSc4
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říkali	říkat	k5eAaImAgMnP
mu	on	k3xPp3gMnSc3
„	„	k?
<g/>
zdráhající	zdráhající	k2eAgMnSc1d1
se	s	k7c7
hrdina	hrdina	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Nikdy	nikdy	k6eAd1
neměl	mít	k5eNaImAgInS
zájem	zájem	k1gInSc1
o	o	k7c4
pozornost	pozornost	k1gFnSc4
médií	médium	k1gNnPc2
<g/>
,	,	kIx,
ani	ani	k8xC
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
slavným	slavný	k2eAgMnSc7d1
<g/>
,	,	kIx,
ani	ani	k8xC
potom	potom	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebaže	třebaže	k8xS
jeho	jeho	k3xOp3gNnSc1
jméno	jméno	k1gNnSc1
bylo	být	k5eAaImAgNnS
slavné	slavný	k2eAgNnSc1d1
<g/>
,	,	kIx,
on	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
nepředváděl	předvádět	k5eNaImAgMnS
<g/>
,	,	kIx,
Cincinnati	Cincinnati	k1gMnPc1
Post	posta	k1gFnPc2
ho	on	k3xPp3gNnSc4
vystihl	vystihnout	k5eAaPmAgInS
titulkem	titulek	k1gInSc7
článku	článek	k1gInSc2
k	k	k7c3
25	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
letu	let	k1gInSc3
Apolla	Apollo	k1gNnSc2
11	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Cincinnatský	Cincinnatský	k2eAgMnSc1d1
neviditelný	viditelný	k2eNgMnSc1d1
hrdina	hrdina	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
přelomu	přelom	k1gInSc6
května	květen	k1gInSc2
a	a	k8xC
června	červen	k1gInSc2
1970	#num#	k4
Armstrong	Armstrong	k1gMnSc1
navštívil	navštívit	k5eAaPmAgMnS
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
člen	člen	k1gMnSc1
delegace	delegace	k1gFnSc2
NASA	NASA	kA
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
výroční	výroční	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
Výboru	výbor	k1gInSc2
pro	pro	k7c4
kosmický	kosmický	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
(	(	kIx(
<g/>
COSPAR	COSPAR	kA
<g/>
)	)	kIx)
v	v	k7c6
Leningradu	Leningrad	k1gInSc2
v	v	k7c6
Sovětském	sovětský	k2eAgInSc6d1
svazu	svaz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konferenci	konference	k1gFnSc6
byl	být	k5eAaImAgInS
uvítán	uvítat	k5eAaPmNgInS
bouřlivými	bouřlivý	k2eAgFnPc7d1
ovacemi	ovace	k1gFnPc7
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
na	na	k7c6
ní	on	k3xPp3gFnSc6
přednášku	přednáška	k1gFnSc4
o	o	k7c6
svém	svůj	k3xOyFgInSc6
letu	let	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
průběhu	průběh	k1gInSc6
konference	konference	k1gFnSc2
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c4
prodloužení	prodloužení	k1gNnSc4
návštěvy	návštěva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Zastavil	zastavit	k5eAaPmAgInS
se	se	k3xPyFc4
v	v	k7c6
Novosibirsku	Novosibirsk	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
poté	poté	k6eAd1
ho	on	k3xPp3gInSc4
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
přijal	přijmout	k5eAaPmAgMnS
sovětský	sovětský	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
Alexej	Alexej	k1gMnSc1
Kosygin	Kosygin	k1gMnSc1
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vystoupil	vystoupit	k5eAaPmAgMnS
i	i	k9
v	v	k7c6
Akademii	akademie	k1gFnSc6
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Krátce	krátce	k6eAd1
po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
Měsíce	měsíc	k1gInSc2
Armstrong	Armstrong	k1gMnSc1
ohlásil	ohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
neplánuje	plánovat	k5eNaImIp3nS
další	další	k2eAgInSc4d1
kosmický	kosmický	k2eAgInSc4d1
let	let	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
oddílu	oddíl	k1gInSc2
astronautů	astronaut	k1gMnPc2
NASA	NASA	kA
odešel	odejít	k5eAaPmAgMnS
v	v	k7c6
dubnu	duben	k1gInSc6
1970	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
vzápětí	vzápětí	k6eAd1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
zástupcem	zástupce	k1gMnSc7
vedoucího	vedoucí	k1gMnSc2
oddělení	oddělení	k1gNnSc2
pro	pro	k7c4
letectví	letectví	k1gNnSc4
v	v	k7c6
centrále	centrála	k1gFnSc6
NASA	NASA	kA
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
6	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
novém	nový	k2eAgNnSc6d1
místě	místo	k1gNnSc6
dohlížel	dohlížet	k5eAaImAgInS
na	na	k7c4
výzkum	výzkum	k1gInSc4
NASA	NASA	kA
v	v	k7c6
oblasti	oblast	k1gFnSc6
letectví	letectví	k1gNnSc2
a	a	k8xC
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
oboru	obor	k1gInSc6
zodpovídal	zodpovídat	k5eAaPmAgMnS,k5eAaImAgMnS
za	za	k7c4
koordinaci	koordinace	k1gFnSc4
mezi	mezi	k7c7
NASA	NASA	kA
a	a	k8xC
průmyslem	průmysl	k1gInSc7
i	i	k8xC
zainteresovanými	zainteresovaný	k2eAgFnPc7d1
vládními	vládní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Získaná	získaný	k2eAgFnSc1d1
autorita	autorita	k1gFnSc1
mu	on	k3xPp3gMnSc3
vynesla	vynést	k5eAaPmAgFnS
i	i	k9
členství	členství	k1gNnSc4
v	v	k7c6
komisi	komise	k1gFnSc6
NASA	NASA	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
od	od	k7c2
dubna	duben	k1gInSc2
do	do	k7c2
června	červen	k1gInSc2
1970	#num#	k4
šetřila	šetřit	k5eAaImAgFnS
nehodu	nehoda	k1gFnSc4
Apolla	Apollo	k1gNnSc2
13	#num#	k4
<g/>
,	,	kIx,
počátkem	počátkem	k7c2
června	červen	k1gInSc2
1970	#num#	k4
ale	ale	k8xC
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Washingtonu	Washington	k1gInSc2
a	a	k8xC
řešení	řešení	k1gNnSc4
následků	následek	k1gInPc2
nehody	nehoda	k1gFnSc2
již	již	k9
neovlivňoval	ovlivňovat	k5eNaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Kancelářská	kancelářská	k1gFnSc1
práce	práce	k1gFnSc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
nelíbila	líbit	k5eNaImAgFnS
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
v	v	k7c6
srpnu	srpen	k1gInSc6
1971	#num#	k4
z	z	k7c2
NASA	NASA	kA
odešel	odejít	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kontakt	kontakt	k1gInSc1
s	s	k7c7
NASA	NASA	kA
neztratil	ztratit	k5eNaPmAgMnS
ani	ani	k8xC
po	po	k7c6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
ještě	ještě	k9
roku	rok	k1gInSc2
1986	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
místopředsedou	místopředseda	k1gMnSc7
prezidentské	prezidentský	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyšetřovala	vyšetřovat	k5eAaImAgFnS
nehodu	nehoda	k1gFnSc4
raketoplánu	raketoplán	k1gInSc2
Challenger	Challengra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
manažer	manažer	k1gMnSc1
</s>
<s>
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
hovoří	hovořit	k5eAaImIp3nS
na	na	k7c6
slavnostní	slavnostní	k2eAgFnSc6d1
večeři	večeře	k1gFnSc6
na	na	k7c6
Ohijské	ohijský	k2eAgFnSc6d1
státní	státní	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
pořádané	pořádaný	k2eAgFnSc6d1
k	k	k7c3
50	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
kosmického	kosmický	k2eAgInSc2d1
letu	let	k1gInSc2
Johna	John	k1gMnSc2
Glenna	Glenn	k1gMnSc2
<g/>
,	,	kIx,
prvního	první	k4xOgMnSc2
amerického	americký	k2eAgMnSc2d1
astronauta	astronaut	k1gMnSc2
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2012	#num#	k4
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1971	#num#	k4
až	až	k9
1979	#num#	k4
vyučoval	vyučovat	k5eAaImAgInS
letecké	letecký	k2eAgNnSc4d1
inženýrství	inženýrství	k1gNnSc4
na	na	k7c6
Cincinnatské	Cincinnatský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
Vybrat	vybrat	k5eAaPmF
si	se	k3xPyFc3
mohl	moct	k5eAaImAgMnS
z	z	k7c2
množství	množství	k1gNnSc2
univerzit	univerzita	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
většině	většina	k1gFnSc6
z	z	k7c2
nich	on	k3xPp3gMnPc2
však	však	k9
mohl	moct	k5eAaImAgMnS
očekávat	očekávat	k5eAaImF
místo	místo	k1gNnSc4
ve	v	k7c6
správní	správní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
Cincinnati	Cincinnati	k1gFnSc6
dostal	dostat	k5eAaPmAgMnS
profesuru	profesura	k1gFnSc4
(	(	kIx(
<g/>
přestože	přestože	k8xS
neměl	mít	k5eNaImAgMnS
doktorát	doktorát	k1gInSc4
–	–	k?
všechny	všechen	k3xTgInPc1
jeho	jeho	k3xOp3gInPc1
doktorské	doktorský	k2eAgInPc1d1
tituly	titul	k1gInPc1
byly	být	k5eAaImAgInP
pouze	pouze	k6eAd1
čestné	čestný	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Měl	mít	k5eAaImAgMnS
možnost	možnost	k1gFnSc4
stát	stát	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
politikem	politikum	k1gNnSc7
<g/>
,	,	kIx,
po	po	k7c6
skončení	skončení	k1gNnSc6
kariéry	kariéra	k1gFnSc2
v	v	k7c6
NASA	NASA	kA
ho	on	k3xPp3gMnSc4
oslovili	oslovit	k5eAaPmAgMnP
zástupci	zástupce	k1gMnPc1
obou	dva	k4xCgFnPc2
hlavních	hlavní	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
–	–	k?
demokratické	demokratický	k2eAgFnSc2d1
i	i	k8xC
republikánské	republikánský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
svých	svůj	k3xOyFgMnPc2
kolegů	kolega	k1gMnPc2
a	a	k8xC
pozdějších	pozdní	k2eAgMnPc2d2
senátorů	senátor	k1gMnPc2
Johna	John	k1gMnSc2
Glenna	Glenn	k1gMnSc2
a	a	k8xC
Harrisona	Harrison	k1gMnSc2
Schmitta	Schmitt	k1gInSc2
všechny	všechen	k3xTgFnPc4
nabídky	nabídka	k1gFnPc4
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobně	osobně	k6eAd1
se	se	k3xPyFc4
vyjadřoval	vyjadřovat	k5eAaImAgMnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
práv	právo	k1gNnPc2
států	stát	k1gInPc2
a	a	k8xC
proti	proti	k7c3
úloze	úloha	k1gFnSc3
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
jako	jako	k8xC,k8xS
„	„	k?
<g/>
světového	světový	k2eAgMnSc2d1
četníka	četník	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
odchodu	odchod	k1gInSc6
z	z	k7c2
univerzity	univerzita	k1gFnSc2
žil	žít	k5eAaImAgMnS
ve	v	k7c6
městě	město	k1gNnSc6
Lebanon	Lebanona	k1gFnPc2
v	v	k7c6
Ohiu	Ohio	k1gNnSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
tváří	tvář	k1gFnSc7
reklamních	reklamní	k2eAgInPc2d1
kampaní	kampaň	k1gFnSc7
několika	několik	k4yIc2
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
především	především	k9
Chrysler	Chrysler	k1gInSc1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
spolupracoval	spolupracovat	k5eAaImAgMnS
od	od	k7c2
ledna	leden	k1gInSc2
1979	#num#	k4
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
před	před	k7c7
odchodem	odchod	k1gInSc7
z	z	k7c2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
7	#num#	k4
<g/>
]	]	kIx)
Angažoval	angažovat	k5eAaBmAgMnS
se	se	k3xPyFc4
i	i	k9
<g />
.	.	kIx.
</s>
<s hack="1">
pro	pro	k7c4
General	General	k1gFnSc4
Time	Tim	k1gFnSc2
Corporation	Corporation	k1gInSc1
a	a	k8xC
Bankers	Bankers	k1gInSc1
Association	Association	k1gInSc1
of	of	k?
America	Americ	k1gInSc2
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
ryze	ryze	k6eAd1
americké	americký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	let	k1gInPc6
1982	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
vedl	vést	k5eAaImAgInS
společnost	společnost	k1gFnSc4
Computing	Computing	k1gInSc1
Technologies	Technologies	k1gMnSc1
for	forum	k1gNnPc2
Aviation	Aviation	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	léto	k1gNnPc6
1989	#num#	k4
až	až	k9
2002	#num#	k4
byl	být	k5eAaImAgInS
ředitelem	ředitel	k1gMnSc7
AIL	AIL	kA
Technologies	Technologiesa	k1gFnPc2
<g/>
,	,	kIx,
společnosti	společnost	k1gFnSc2
vyrábějící	vyrábějící	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
komponenty	komponenta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2000	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
předsedal	předsedat	k5eAaImAgMnS
správní	správní	k2eAgFnSc3d1
radě	rada	k1gFnSc3
EDO	Eda	k1gMnSc5
Corporation	Corporation	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
Současně	současně	k6eAd1
již	již	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
působil	působit	k5eAaImAgMnS
ve	v	k7c6
správních	správní	k2eAgFnPc6d1
radách	rada	k1gFnPc6
různých	různý	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
,	,	kIx,
první	první	k4xOgInSc1
byla	být	k5eAaImAgFnS
Learjet	Learjet	k1gInSc4
<g/>
,	,	kIx,
dalšími	další	k2eAgMnPc7d1
např.	např.	kA
Marathon	Marathon	k1gMnSc1
Oil	Oil	k1gMnSc1
<g/>
,	,	kIx,
Cincinnati	Cincinnati	k1gMnSc1
Gas	Gas	k1gMnSc1
&	&	k?
Electric	Electric	k1gMnSc1
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
Taft	taft	k1gInSc1
Broadcasting	Broadcasting	k1gInSc1
<g/>
,	,	kIx,
United	United	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
,	,	kIx,
Eaton	Eaton	k1gMnSc1
Corporation	Corporation	k1gInSc1
<g/>
,	,	kIx,
AIL	AIL	kA
Systems	Systems	k1gInSc1
a	a	k8xC
Thiokol	Thiokol	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozloučení	rozloučení	k1gNnSc1
s	s	k7c7
Neilem	Neil	k1gMnSc7
Armstrongem	Armstrong	k1gMnSc7
v	v	k7c6
Národní	národní	k2eAgFnSc6d1
katedrále	katedrála	k1gFnSc6
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
</s>
<s>
Roku	rok	k1gInSc2
1994	#num#	k4
se	se	k3xPyFc4
Armstrong	Armstrong	k1gMnSc1
po	po	k7c6
38	#num#	k4
letech	léto	k1gNnPc6
manželství	manželství	k1gNnSc2
rozvedl	rozvést	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
Carol	Carol	k1gInSc4
Held	Held	k1gMnSc1
Knightovou	Knightův	k2eAgFnSc7d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
poznal	poznat	k5eAaPmAgMnS
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Žili	žít	k5eAaImAgMnP
v	v	k7c6
cincinnatském	cincinnatský	k2eAgNnSc6d1
předměstí	předměstí	k1gNnSc6
Indian	Indiana	k1gFnPc2
Hill	Hill	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
žil	žít	k5eAaImAgMnS
v	v	k7c4
ústraní	ústraní	k1gNnSc4
<g/>
,	,	kIx,
prakticky	prakticky	k6eAd1
nedával	dávat	k5eNaImAgMnS
interview	interview	k1gNnSc4
médiím	médium	k1gNnPc3
a	a	k8xC
veřejné	veřejný	k2eAgNnSc4d1
dění	dění	k1gNnSc4
komentoval	komentovat	k5eAaBmAgInS
pouze	pouze	k6eAd1
zřídka	zřídka	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgNnSc7d1
významným	významný	k2eAgNnSc7d1
vystoupením	vystoupení	k1gNnSc7
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
2010	#num#	k4
kritika	kritika	k1gFnSc1
zrušení	zrušení	k1gNnSc2
programu	program	k1gInSc2
Constellation	Constellation	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2012	#num#	k4
zveřejnila	zveřejnit	k5eAaPmAgFnS
organizace	organizace	k1gFnSc1
australských	australský	k2eAgMnPc2d1
účetních	účetní	k1gMnPc2
neobvykle	obvykle	k6eNd1
rozsáhlý	rozsáhlý	k2eAgInSc1d1
<g/>
,	,	kIx,
hodinový	hodinový	k2eAgInSc1d1
<g/>
,	,	kIx,
rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Armstrongem	Armstrong	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
<g/>
,	,	kIx,
podstoupil	podstoupit	k5eAaPmAgMnS
operaci	operace	k1gFnSc4
srdce	srdce	k1gNnSc2
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
dostal	dostat	k5eAaPmAgMnS
bypass	bypass	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
Problémy	problém	k1gInPc1
se	s	k7c7
srdcem	srdce	k1gNnSc7
měl	mít	k5eAaImAgInS
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
<g/>
,	,	kIx,
už	už	k6eAd1
v	v	k7c6
únoru	únor	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
rok	rok	k1gInSc4
po	po	k7c6
smrti	smrt	k1gFnSc6
otce	otec	k1gMnSc2
a	a	k8xC
devět	devět	k4xCc1
měsíců	měsíc	k1gInPc2
po	po	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
úmrtí	úmrtí	k1gNnSc3
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
dostal	dostat	k5eAaPmAgMnS
slabý	slabý	k2eAgInSc4d1
srdeční	srdeční	k2eAgInSc4d1
záchvat	záchvat	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
operaci	operace	k1gFnSc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
zprvu	zprvu	k6eAd1
dařilo	dařit	k5eAaImAgNnS
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
jeho	jeho	k3xOp3gInSc1
stav	stav	k1gInSc1
zhoršil	zhoršit	k5eAaPmAgInS
a	a	k8xC
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2012	#num#	k4
v	v	k7c6
Cincinnati	Cincinnati	k1gFnSc6
v	v	k7c6
Ohiu	Ohio	k1gNnSc6
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
Rozloučení	rozloučení	k1gNnSc1
s	s	k7c7
Armstrongem	Armstrong	k1gMnSc7
za	za	k7c2
účasti	účast	k1gFnSc2
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
přátel	přítel	k1gMnPc2
a	a	k8xC
kolegů	kolega	k1gMnPc2
proběhlo	proběhnout	k5eAaPmAgNnS
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
v	v	k7c6
zaplněné	zaplněný	k2eAgFnSc6d1
Národní	národní	k2eAgFnSc6d1
katedrále	katedrála	k1gFnSc6
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Druhý	druhý	k4xOgInSc1
den	den	k1gInSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
Armstrongovy	Armstrongův	k2eAgInPc1d1
zpopelněné	zpopelněný	k2eAgInPc1d1
ostatky	ostatek	k1gInPc1
rozptýleny	rozptýlen	k2eAgInPc1d1
do	do	k7c2
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
během	během	k7c2
pohřební	pohřební	k2eAgFnSc2d1
ceremonie	ceremonie	k1gFnSc2
na	na	k7c6
palubě	paluba	k1gFnSc6
raketového	raketový	k2eAgInSc2d1
křižníku	křižník	k1gInSc2
USS	USS	kA
Philippine	Philippin	k1gInSc5
Sea	Sea	k1gMnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Přijetí	přijetí	k1gNnSc1
posádky	posádka	k1gFnSc2
Apolla	Apollo	k1gNnSc2
11	#num#	k4
v	v	k7c6
Bílém	bílý	k2eAgInSc6d1
domě	dům	k1gInSc6
k	k	k7c3
35	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc3
přistání	přistání	k1gNnSc2
na	na	k7c6
Měsíci	měsíc	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zleva	zleva	k6eAd1
Michael	Michael	k1gMnSc1
Collins	Collinsa	k1gFnPc2
<g/>
,	,	kIx,
prezident	prezident	k1gMnSc1
USA	USA	kA
George	George	k1gNnSc2
W.	W.	kA
Bush	Bush	k1gMnSc1
<g/>
,	,	kIx,
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
a	a	k8xC
Buzz	Buzz	k1gMnSc1
Aldrin	aldrin	k1gInSc4
</s>
<s>
Krátce	krátce	k6eAd1
po	po	k7c6
přistání	přistání	k1gNnSc6
přejmenovala	přejmenovat	k5eAaPmAgFnS
Mezinárodní	mezinárodní	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
IAU	IAU	kA
<g/>
)	)	kIx)
kráter	kráter	k1gInSc1
Sabine	Sabin	k1gInSc5
E	E	kA
poblíž	poblíž	k7c2
místa	místo	k1gNnSc2
přistání	přistání	k1gNnSc2
Apolla	Apollo	k1gNnSc2
11	#num#	k4
na	na	k7c4
Armstrong	Armstrong	k1gMnSc1
podle	podle	k7c2
astronautova	astronautův	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
(	(	kIx(
<g/>
zároveň	zároveň	k6eAd1
i	i	k8xC
další	další	k2eAgInPc1d1
dva	dva	k4xCgInPc1
blízké	blízký	k2eAgInPc1d1
krátery	kráter	k1gInPc1
Sabine	Sabin	k1gInSc5
B	B	kA
na	na	k7c4
Aldrin	aldrin	k1gInSc4
a	a	k8xC
Sabine	Sabin	k1gInSc5
D	D	kA
na	na	k7c4
Collins	Collins	k1gInSc4
podle	podle	k7c2
kolegů	kolega	k1gMnPc2
astronautů	astronaut	k1gMnPc2
z	z	k7c2
Apolla	Apollo	k1gNnSc2
11	#num#	k4
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
jméno	jméno	k1gNnSc4
nese	nést	k5eAaImIp3nS
i	i	k9
asteroid	asteroid	k1gInSc4
hlavního	hlavní	k2eAgInSc2d1
pásu	pás	k1gInSc2
(	(	kIx(
<g/>
6469	#num#	k4
<g/>
)	)	kIx)
Armstrong	Armstrong	k1gMnSc1
objevený	objevený	k2eAgInSc1d1
roku	rok	k1gInSc2
1982	#num#	k4
českým	český	k2eAgMnSc7d1
astronomem	astronom	k1gMnSc7
Antonínem	Antonín	k1gMnSc7
Mrkosem	Mrkos	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
jeho	jeho	k3xOp3gNnSc6
rodném	rodný	k2eAgNnSc6d1
městě	město	k1gNnSc6
po	po	k7c6
něm	on	k3xPp3gMnSc6
bylo	být	k5eAaImAgNnS
pojmenováno	pojmenovat	k5eAaPmNgNnS
muzeum	muzeum	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
letectví	letectví	k1gNnSc1
a	a	k8xC
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
světě	svět	k1gInSc6
je	být	k5eAaImIp3nS
po	po	k7c6
něm	on	k3xPp3gNnSc6
pojmenováno	pojmenován	k2eAgNnSc1d1
množství	množství	k1gNnSc1
ulic	ulice	k1gFnPc2
<g/>
,	,	kIx,
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
škol	škola	k1gFnPc2
a	a	k8xC
jiných	jiný	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
;	;	kIx,
jen	jen	k9
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
nese	nést	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc4
jméno	jméno	k1gNnSc4
více	hodně	k6eAd2
než	než	k8xS
tucet	tucet	k1gInSc1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1972	#num#	k4
přijel	přijet	k5eAaPmAgMnS
na	na	k7c4
pozvání	pozvání	k1gNnSc4
místních	místní	k2eAgFnPc2d1
do	do	k7c2
skotského	skotský	k2eAgNnSc2d1
města	město	k1gNnSc2
Langholm	Langholmo	k1gNnPc2
<g/>
,	,	kIx,
tradičního	tradiční	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
klanu	klan	k1gInSc2
Armstrongů	Armstrong	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
přijal	přijmout	k5eAaPmAgMnS
jmenování	jmenování	k1gNnSc4
občanem	občan	k1gMnSc7
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
obdržel	obdržet	k5eAaPmAgInS
nejméně	málo	k6eAd3
devatenáct	devatenáct	k4xCc4
čestných	čestný	k2eAgInPc2d1
doktorátů	doktorát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
vyznamenán	vyznamenat	k5eAaPmNgInS
Prezidentskou	prezidentský	k2eAgFnSc7d1
medailí	medaile	k1gFnSc7
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
Kosmickou	kosmický	k2eAgFnSc7d1
medailí	medaile	k1gFnSc7
cti	čest	k1gFnSc2
Kongresu	kongres	k1gInSc2
USA	USA	kA
<g/>
,	,	kIx,
Zlatou	zlatá	k1gFnSc4
medailí	medaile	k1gFnPc2
Kongresu	kongres	k1gInSc2
USA	USA	kA
a	a	k8xC
řadou	řada	k1gFnSc7
dalších	další	k2eAgFnPc2d1
cen	cena	k1gFnPc2
a	a	k8xC
medailí	medaile	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Má	mít	k5eAaImIp3nS
hvězdu	hvězda	k1gFnSc4
na	na	k7c6
Hollywoodském	hollywoodský	k2eAgInSc6d1
chodníku	chodník	k1gInSc6
slávy	sláva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Ohiu	Ohio	k1gNnSc6
byl	být	k5eAaImAgMnS
zapsán	zapsat	k5eAaPmNgMnS
do	do	k7c2
National	National	k1gFnSc2
Aviation	Aviation	k1gInSc1
Hall	Hall	k1gMnSc1
of	of	k?
Fame	Fame	k1gFnSc1
(	(	kIx(
<g/>
Národní	národní	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
síň	síň	k1gFnSc1
slávy	sláva	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
do	do	k7c2
floridské	floridský	k2eAgFnSc2d1
Síně	síň	k1gFnSc2
slávy	sláva	k1gFnSc2
astronautů	astronaut	k1gMnPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Purdueova	Purdueův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
se	se	k3xPyFc4
roku	rok	k1gInSc2
2004	#num#	k4
rozhodla	rozhodnout	k5eAaPmAgFnS
pojmenovat	pojmenovat	k5eAaPmF
po	po	k7c6
něm	on	k3xPp3gInSc6
nově	nov	k1gInSc6
stavěnou	stavěný	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
<g/>
,	,	kIx,
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
Hall	Hall	k1gMnSc1
of	of	k?
Engineering	Engineering	k1gInSc1
<g/>
,	,	kIx,
slavnostně	slavnostně	k6eAd1
otevřenou	otevřený	k2eAgFnSc4d1
roku	rok	k1gInSc2
2007	#num#	k4
za	za	k7c2
účasti	účast	k1gFnSc2
patnácti	patnáct	k4xCc2
dalších	další	k2eAgMnPc2d1
astronautů	astronaut	k1gMnPc2
–	–	k?
absolventů	absolvent	k1gMnPc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
jeho	jeho	k3xOp3gNnSc6
rodném	rodný	k2eAgNnSc6d1
městě	město	k1gNnSc6
Wapakonetě	Wapakonet	k1gInSc6
bylo	být	k5eAaImAgNnS
roku	rok	k1gInSc2
1972	#num#	k4
otevřeno	otevřít	k5eAaPmNgNnS
Muzeum	muzeum	k1gNnSc1
letectví	letectví	k1gNnSc2
a	a	k8xC
vesmíru	vesmír	k1gInSc2
Neila	Neil	k1gMnSc2
Armstronga	Armstrong	k1gMnSc2
<g/>
,	,	kIx,
dokumentující	dokumentující	k2eAgInSc1d1
ohijský	ohijský	k2eAgInSc1d1
a	a	k8xC
zejména	zejména	k9
Armstrongův	Armstrongův	k2eAgInSc4d1
příspěvek	příspěvek	k1gInSc4
k	k	k7c3
letectví	letectví	k1gNnSc3
a	a	k8xC
kosmonautice	kosmonautika	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
letiště	letiště	k1gNnSc2
v	v	k7c6
New	New	k1gFnSc6
Knoxville	Knoxville	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
naučil	naučit	k5eAaPmAgMnS
létat	létat	k5eAaImF
<g/>
,	,	kIx,
nese	nést	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc4
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2012	#num#	k4
námořnictvo	námořnictvo	k1gNnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
oznámilo	oznámit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgFnSc1
loď	loď	k1gFnSc1
nové	nový	k2eAgFnSc2d1
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
výzkumných	výzkumný	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
ponese	nést	k5eAaImIp3nS,k5eAaPmIp3nS
jméno	jméno	k1gNnSc1
RV	RV	kA
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
(	(	kIx(
<g/>
AGOR-	AGOR-	k1gFnSc1
<g/>
27	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
Výrobce	výrobce	k1gMnSc1
ji	on	k3xPp3gFnSc4
předal	předat	k5eAaPmAgMnS
námořnictvu	námořnictvo	k1gNnSc3
v	v	k7c6
srpnu	srpen	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
Určena	určen	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
ke	k	k7c3
komplexnímu	komplexní	k2eAgInSc3d1
vědeckému	vědecký	k2eAgInSc3d1
výzkumu	výzkum	k1gInSc3
moří	mořit	k5eAaImIp3nS
<g/>
,	,	kIx,
včetně	včetně	k7c2
mapování	mapování	k1gNnSc2
nejhlubších	hluboký	k2eAgFnPc2d3
částí	část	k1gFnPc2
oceánského	oceánský	k2eAgNnSc2d1
dna	dno	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Roku	rok	k1gInSc2
2010	#num#	k4
namluvil	namluvit	k5eAaPmAgInS,k5eAaBmAgInS
roli	role	k1gFnSc4
Dr	dr	kA
<g/>
.	.	kIx.
Jacka	Jacka	k1gMnSc1
Morrowa	Morrowa	k1gMnSc1
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
v	v	k7c4
Quantum	Quantum	k1gNnSc4
Quest	Quest	k1gInSc4
<g/>
:	:	kIx,
A	a	k9
Cassini	Cassin	k2eAgMnPc1d1
Space	Space	k1gMnSc1
Odyssey	Odyssea	k1gFnSc2
<g/>
,	,	kIx,
animovaném	animovaný	k2eAgNnSc6d1
vzdělávacím	vzdělávací	k2eAgNnSc6d1
dobrodružném	dobrodružný	k2eAgNnSc6d1
sci-fi	sci-fi	k1gNnSc6
filmu	film	k1gInSc2
finančně	finančně	k6eAd1
podporovaném	podporovaný	k2eAgInSc6d1
Jet	jet	k5eAaImF
Propulsion	Propulsion	k1gInSc4
Laboratory	Laborator	k1gInPc7
NASA	NASA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
informací	informace	k1gFnPc2
Českého	český	k2eAgInSc2d1
rozhlasu	rozhlas	k1gInSc2
měl	mít	k5eAaImAgMnS
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
nadšený	nadšený	k2eAgMnSc1d1
skaut	skaut	k1gMnSc1
<g/>
,	,	kIx,
poslat	poslat	k5eAaPmF
měsíční	měsíční	k2eAgInSc4d1
kámen	kámen	k1gInSc4
na	na	k7c4
skautskou	skautský	k2eAgFnSc4d1
mohylu	mohyla	k1gFnSc4
Ivančenu	Ivančen	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
ČR	ČR	kA
v	v	k7c6
Beskydech	Beskyd	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1
shoda	shoda	k1gFnSc1
jména	jméno	k1gNnSc2
Armstrongova	Armstrongův	k2eAgNnSc2d1
bydliště	bydliště	k1gNnSc2
Lebanonu	Lebanon	k1gInSc2
v	v	k7c6
Ohiu	Ohio	k1gNnSc6
s	s	k7c7
názvem	název	k1gInSc7
arabského	arabský	k2eAgInSc2d1
Libanonu	Libanon	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
angličtině	angličtina	k1gFnSc6
také	také	k9
Lebanon	Lebanon	k1gInSc1
<g/>
)	)	kIx)
vedla	vést	k5eAaImAgFnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
fámy	fáma	k1gFnSc2
<g/>
,	,	kIx,
rozšířené	rozšířený	k2eAgFnSc6d1
v	v	k7c6
Indonésii	Indonésie	k1gFnSc6
<g/>
,	,	kIx,
Malajsii	Malajsie	k1gFnSc6
a	a	k8xC
Egyptě	Egypt	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
konvertoval	konvertovat	k5eAaBmAgInS
k	k	k7c3
islámu	islám	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1983	#num#	k4
fámu	fáma	k1gFnSc4
v	v	k7c6
oficiálním	oficiální	k2eAgNnSc6d1
prohlášení	prohlášení	k1gNnSc6
popřelo	popřít	k5eAaPmAgNnS
ministerstvo	ministerstvo	k1gNnSc1
zahraničí	zahraničí	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
přesto	přesto	k8xC
se	se	k3xPyFc4
objevovala	objevovat	k5eAaImAgFnS
i	i	k9
nadále	nadále	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Rodina	rodina	k1gFnSc1
Armstrongů	Armstrong	k1gMnPc2
se	se	k3xPyFc4
přestěhovala	přestěhovat	k5eAaPmAgFnS
ze	z	k7c2
Skotska	Skotsko	k1gNnSc2
do	do	k7c2
Pensylvánie	Pensylvánie	k1gFnSc2
na	na	k7c6
přelomu	přelom	k1gInSc6
30	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předkové	předek	k1gMnPc1
Violy	Viola	k1gFnPc4
Engelové	Engel	k1gMnPc1
pocházeli	pocházet	k5eAaImAgMnP
z	z	k7c2
Vestfálska	Vestfálsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Jako	jako	k8xC,k8xS
slavný	slavný	k2eAgMnSc1d1
astronaut	astronaut	k1gMnSc1
získal	získat	k5eAaPmAgMnS
skautské	skautský	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
Distinguished	Distinguished	k1gMnSc1
Eagle	Eagl	k1gMnSc2
Scout	Scout	k1gMnSc1
Award	Award	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Elliott	Elliott	k1gMnSc1
See	See	k1gMnSc1
byl	být	k5eAaImAgMnS
společně	společně	k6eAd1
s	s	k7c7
Armstrongem	Armstrong	k1gMnSc7
v	v	k7c6
záložní	záložní	k2eAgFnSc6d1
posádce	posádka	k1gFnSc6
Gemini	Gemin	k1gMnPc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
letět	letět	k5eAaImF
do	do	k7c2
vesmíru	vesmír	k1gInSc2
v	v	k7c6
Gemini	Gemin	k2eAgMnPc1d1
9	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
kolegou	kolega	k1gMnSc7
z	z	k7c2
posádky	posádka	k1gFnSc2
zahynul	zahynout	k5eAaPmAgMnS
při	při	k7c6
leteckém	letecký	k2eAgNnSc6d1
neštěstí	neštěstí	k1gNnSc6
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
před	před	k7c7
startem	start	k1gInSc7
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Březnová	březnový	k2eAgFnSc1d1
schůzka	schůzka	k1gFnSc1
a	a	k8xC
rozhodnutí	rozhodnutí	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
na	na	k7c6
ní	on	k3xPp3gFnSc6
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
<g/>
,	,	kIx,
zůstaly	zůstat	k5eAaPmAgFnP
neznámé	známý	k2eNgFnPc1d1
až	až	k9
do	do	k7c2
vydání	vydání	k1gNnSc2
Kraftových	Kraftův	k2eAgFnPc2d1
pamětí	paměť	k1gFnPc2
roku	rok	k1gInSc2
2001	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
V	v	k7c6
záznamu	záznam	k1gInSc6
není	být	k5eNaImIp3nS
slyšet	slyšet	k5eAaImF
a	a	k8xC
před	před	k7c7
man	mana	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mění	měnit	k5eAaImIp3nS
smysl	smysl	k1gInSc4
věty	věta	k1gFnSc2
(	(	kIx(
<g/>
z	z	k7c2
„	„	k?
<g/>
malý	malý	k2eAgInSc1d1
krok	krok	k1gInSc1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
(	(	kIx(
<g/>
a	a	k8xC
man	man	k1gMnSc1
<g/>
)	)	kIx)
<g/>
…	…	k?
<g/>
“	“	k?
na	na	k7c6
„	„	k?
<g/>
malý	malý	k2eAgInSc4d1
krok	krok	k1gInSc4
pro	pro	k7c4
lidstvo	lidstvo	k1gNnSc4
(	(	kIx(
<g/>
man	man	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obrovský	obrovský	k2eAgInSc1d1
krok	krok	k1gInSc1
pro	pro	k7c4
lidstvo	lidstvo	k1gNnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
i	i	k8xC
Armstrong	Armstrong	k1gMnSc1
léta	léto	k1gNnSc2
trvali	trvat	k5eAaImAgMnP
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
a	a	k8xC
bylo	být	k5eAaImAgNnS
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
není	být	k5eNaImIp3nS
zřetelné	zřetelný	k2eAgNnSc1d1
z	z	k7c2
nekvalitního	kvalitní	k2eNgInSc2d1
záznamu	záznam	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
opakovaném	opakovaný	k2eAgInSc6d1
poslechu	poslech	k1gInSc6
nahrávky	nahrávka	k1gFnSc2
Armstrong	Armstrong	k1gMnSc1
připustil	připustit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
a	a	k8xC
vynechal	vynechat	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Armstrong	Armstrong	k1gMnSc1
doporučil	doporučit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
přepisu	přepis	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
jeho	jeho	k3xOp3gFnSc2
věty	věta	k1gFnSc2
bylo	být	k5eAaImAgNnS
a	a	k8xC
v	v	k7c6
závorce	závorka	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
protože	protože	k8xS
úmysl	úmysl	k1gInSc4
říci	říct	k5eAaPmF
větu	věta	k1gFnSc4
s	s	k7c7
ním	on	k3xPp3gInSc7
je	být	k5eAaImIp3nS
zřejmý	zřejmý	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Stal	stát	k5eAaPmAgInS
se	se	k3xPyFc4
deputy	deput	k1gMnPc7
associate	associat	k1gInSc5
administrator	administrator	k1gMnSc1
for	forum	k1gNnPc2
aeronautics	aeronautics	k6eAd1
for	forum	k1gNnPc2
the	the	k?
Office	Office	kA
of	of	k?
Advanced	Advanced	k1gMnSc1
Research	Research	k1gMnSc1
and	and	k?
Technology	technolog	k1gMnPc7
<g/>
,	,	kIx,
jmenování	jmenování	k1gNnSc3
<g />
.	.	kIx.
</s>
<s hack="1">
NASA	NASA	kA
oficiálně	oficiálně	k6eAd1
oznámila	oznámit	k5eAaPmAgFnS
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Z	z	k7c2
univerzity	univerzita	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
v	v	k7c6
srpnu	srpen	k1gInSc6
1979	#num#	k4
<g/>
,	,	kIx,
za	za	k7c4
rok	rok	k1gInSc4
1979	#num#	k4
tvořil	tvořit	k5eAaImAgInS
jeho	jeho	k3xOp3gInSc1
univerzitní	univerzitní	k2eAgInSc1d1
plat	plat	k1gInSc1
(	(	kIx(
<g/>
18	#num#	k4
tisíc	tisíc	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
)	)	kIx)
pouze	pouze	k6eAd1
malou	malý	k2eAgFnSc4d1
část	část	k1gFnSc4
příjmu	příjem	k1gInSc2
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
236	#num#	k4
tisíc	tisíc	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
;	;	kIx,
LÁLA	Lála	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
kosmonautiky	kosmonautika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Kosmonauti-pilotu	Kosmonauti-pilot	k1gInSc2
USA	USA	kA
<g/>
,	,	kIx,
s.	s.	k?
357	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
HANSEN	HANSEN	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
R.	R.	kA
First	First	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Life	Lif	k1gFnSc2
of	of	k?
Neil	Neil	k1gMnSc1
A.	A.	kA
Armstrong	Armstrong	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Simon	Simon	k1gMnSc1
&	&	k?
Schuster	Schuster	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
769	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7432	#num#	k4
<g/>
-	-	kIx~
<g/>
5631	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
13	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
a	a	k8xC
20	#num#	k4
<g/>
–	–	k?
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Hansen	Hansen	k2eAgMnSc1d1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Project	Project	k1gMnSc1
Apollo	Apollo	k1gMnSc1
<g/>
:	:	kIx,
Astronaut	astronaut	k1gMnSc1
Biographies	Biographies	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
37	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
49	#num#	k4
<g/>
–	–	k?
<g/>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Distinguished	Distinguished	k1gInSc1
Eagle	Eagle	k1gFnSc2
Scouts	Scoutsa	k1gFnPc2
[	[	kIx(
<g/>
PDF	PDF	kA
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scouting	Scouting	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WOODS	WOODS	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
;	;	kIx,
MACTAGGART	MACTAGGART	kA
<g/>
,	,	kIx,
Ken	Ken	k1gMnSc1
<g/>
;	;	kIx,
O	O	kA
<g/>
'	'	kIx"
<g/>
BRIEN	BRIEN	kA
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Apollo	Apollo	k1gMnSc1
11	#num#	k4
Flight	Flightum	k1gNnPc2
Journal	Journal	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
,	,	kIx,
History	Histor	k1gInPc1
Division	Division	k1gInSc1
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2010-04-11	2010-04-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Apollo	Apollo	k1gNnSc1
11	#num#	k4
-	-	kIx~
Day	Day	k1gFnPc2
3	#num#	k4
<g/>
,	,	kIx,
part	part	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
Entering	Entering	k1gInSc1
Eagle	Eagle	k1gFnSc2
-	-	kIx~
Transcript	Transcript	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
World	World	k1gInSc1
Organization	Organization	k1gInSc1
of	of	k?
the	the	k?
Scout	Scout	k1gMnSc1
Movement	Movement	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	Worlda	k1gFnPc2
Scouting	Scouting	k1gInSc1
salutes	salutes	k1gMnSc1
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geneva	Geneva	k1gFnSc1
<g/>
,	,	kIx,
Switzerland	Switzerland	k1gInSc1
<g/>
:	:	kIx,
World	World	k1gInSc1
Organization	Organization	k1gInSc1
of	of	k?
the	the	k?
Scout	Scout	k1gMnSc1
Movement	Movement	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
55	#num#	k4
<g/>
–	–	k?
<g/>
56	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Purdue	Purdu	k1gMnPc4
University	universita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Purdue	Purdu	k1gInSc2
remembers	remembers	k1gInSc1
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
West	West	k1gInSc1
Lafayette	Lafayett	k1gInSc5
<g/>
,	,	kIx,
IN	IN	kA
<g/>
:	:	kIx,
Purdue	Purdue	k1gInSc1
University	universita	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Purdue	Purdue	k1gFnSc1
University	universita	k1gFnSc2
Bands	Bandsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Purdue	Purdue	k1gFnPc2
Bands	Bandsa	k1gFnPc2
remembers	remembers	k1gInSc1
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
–	–	k?
and	and	k?
you	you	k?
can	can	k?
too	too	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
West	West	k1gInSc1
Lafayette	Lafayett	k1gInSc5
<g/>
:	:	kIx,
Purdue	Purdue	k1gNnSc3
University	universita	k1gFnSc2
Bands	Bands	k1gInSc1
<g/>
,	,	kIx,
Purdue	Purdue	k1gInSc1
University	universita	k1gFnSc2
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
68	#num#	k4
<g/>
–	–	k?
<g/>
78	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
79	#num#	k4
<g/>
–	–	k?
<g/>
85	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
90	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
92	#num#	k4
<g/>
–	–	k?
<g/>
93	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
95	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
112	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
118.1	118.1	k4
2	#num#	k4
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronaut	astronaut	k1gMnSc1
Bio	Bio	k?
<g/>
:	:	kIx,
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2012-8	2012-8	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
124	#num#	k4
<g/>
–	–	k?
<g/>
127	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
119	#num#	k4
<g/>
–	–	k?
<g/>
120	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
122	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
134.1	134.1	k4
2	#num#	k4
CREECH	CREECH	kA
<g/>
,	,	kIx,
Gray	Gray	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
From	From	k1gMnSc1
the	the	k?
Mojave	Mojav	k1gInSc5
to	ten	k3xDgNnSc1
the	the	k?
Moon	Moon	k1gInSc1
<g/>
:	:	kIx,
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Early	earl	k1gMnPc7
NASA	NASA	kA
Years	Years	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
Dryden	Drydna	k1gFnPc2
Flight	Flight	k1gMnSc1
Research	Research	k1gMnSc1
Center	centrum	k1gNnPc2
Public	publicum	k1gNnPc2
Affairs	Affairsa	k1gFnPc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2007-11-22	2007-11-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
134	#num#	k4
<g/>
–	–	k?
<g/>
136	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
145.1	145.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
IVANOV	IVANOV	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
К	К	k?
э	э	k?
ASTROnote	ASTROnot	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-08-09	2009-08-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Neil	Neil	k1gMnSc1
Alden	Alden	k2eAgMnSc1d1
Armstrong	Armstrong	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Ivanov	Ivanov	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
CURRY	CURRY	kA
<g/>
,	,	kIx,
Marty	Marta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neil	Neil	k1gMnSc1
A.	A.	kA
Armstrong	Armstrong	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2012-8-27	2012-8-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
178	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
210	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
178	#num#	k4
<g/>
–	–	k?
<g/>
192.1	192.1	k4
2	#num#	k4
Hansen	Hansna	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
178	#num#	k4
<g/>
–	–	k?
<g/>
184	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
184	#num#	k4
<g/>
–	–	k?
<g/>
189	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
138	#num#	k4
<g/>
–	–	k?
<g/>
139.1	139.1	k4
2	#num#	k4
3	#num#	k4
Hansen	Hansna	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
189	#num#	k4
<g/>
–	–	k?
<g/>
192	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
33	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ivanov	Ivanov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2012-04-15	2012-04-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Man	mana	k1gFnPc2
in	in	k?
Space	Space	k1gMnSc1
Soonest	Soonest	k1gMnSc1
-	-	kIx~
MISS	miss	k1gFnSc1
<g/>
.	.	kIx.
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
195	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
195	#num#	k4
<g/>
–	–	k?
<g/>
204	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ivanov	Ivanov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2012-04-15	2012-04-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
2	#num#	k4
<g/>
-й	-й	k?
н	н	k?
а	а	k?
NASA	NASA	kA
<g/>
.	.	kIx.
↑	↑	k?
Astronaut	astronaut	k1gMnSc1
Bio	Bio	k?
<g/>
:	:	kIx,
Elliot	Elliot	k1gMnSc1
M.	M.	kA
See	See	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
1966-2	1966-2	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
128	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
161	#num#	k4
<g/>
–	–	k?
<g/>
164	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Х	Х	k?
<g/>
,	,	kIx,
С	С	k?
П	П	k?
<g/>
.	.	kIx.
Э	Э	k?
К	К	k?
"	"	kIx"
<g/>
Д	Д	k?
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Х	Х	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
kap	kap	k1gInSc1
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Gemini	Gemin	k1gMnPc1
1965	#num#	k4
<g/>
–	–	k?
<g/>
1966	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spacecollection	Spacecollection	k1gInSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MERRITT	MERRITT	kA
<g/>
,	,	kIx,
Larry	Larr	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
abbreviated	abbreviated	k1gMnSc1
flight	flighta	k1gFnPc2
of	of	k?
Gemini	Gemin	k1gMnPc1
8	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boeing	boeing	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2009-3	2009-3	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kolektiv	kolektiv	k1gInSc4
astronautického	astronautický	k2eAgInSc2d1
klubu	klub	k1gInSc2
S.	S.	kA
<g/>
P.	P.	kA
<g/>
A.C.E.	A.C.E.	k1gMnSc1
(	(	kIx(
<g/>
A.	A.	kA
<g/>
Vítek	Vítek	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GT	GT	kA
-	-	kIx~
8	#num#	k4
Dramatický	dramatický	k2eAgInSc1d1
let	let	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
kosmonautiky	kosmonautika	k1gFnSc2
(	(	kIx(
<g/>
MEK	mek	k0
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2001-5-6	2001-5-6	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOLUB	Holub	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MEK	mek	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
kosmonautiky	kosmonautika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2001-5-6	2001-5-6	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Gemini	Gemin	k2eAgMnPc1d1
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
274	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
292	#num#	k4
<g/>
–	–	k?
<g/>
293	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
295	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
296	#num#	k4
<g/>
–	–	k?
<g/>
297	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Х	Х	k?
<g/>
.	.	kIx.
Э	Э	k?
К	К	k?
"	"	kIx"
<g/>
А	А	k?
<g/>
"	"	kIx"
1966-1970	1966-1970	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NELSON	Nelson	k1gMnSc1
<g/>
,	,	kIx,
Craig	Craig	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rocket	Rocket	k1gMnSc1
Men	Men	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Epic	Epic	k1gFnSc1
Story	story	k1gFnSc1
of	of	k?
the	the	k?
First	First	k1gMnSc1
Men	Men	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Moon	Moon	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
John	John	k1gMnSc1
Murray	Murraa	k1gFnSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781101057735	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
SHERROD	SHERROD	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Men	Men	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
Moon	Moon	k1gMnSc1
:	:	kIx,
How	How	k1gFnSc1
they	thea	k1gFnSc2
were	werat	k5eAaPmIp3nS
chosen	chosen	k2eAgMnSc1d1
and	and	k?
trained	trained	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
CORTRIGHT	CORTRIGHT	kA
<g/>
,	,	kIx,
Edgar	Edgar	k1gMnSc1
M.	M.	kA
Apollo	Apollo	k1gMnSc1
:	:	kIx,
Expeditions	Expeditions	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
Moon	Moon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
DC	DC	kA
<g/>
:	:	kIx,
NASA	NASA	kA
<g/>
,	,	kIx,
Scientific	Scientific	k1gMnSc1
and	and	k?
Technical	Technical	k1gFnSc1
Information	Information	k1gInSc1
Office	Office	kA
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
8.7	8.7	k4
Choosing	Choosing	k1gInSc1
the	the	k?
first	first	k1gMnSc1
Man	Man	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Moon	Moon	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
160	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
371	#num#	k4
<g/>
–	–	k?
<g/>
373	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
334.1	334.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
;	;	kIx,
KRUPIČKA	Krupička	k1gMnSc1
<g/>
,	,	kIx,
Jozef	Jozef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apollo	Apollo	k1gNnSc1
11	#num#	k4
–	–	k?
popis	popis	k1gInSc1
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
1994	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
411	#num#	k4
<g/>
–	–	k?
<g/>
412	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
423.1	423.1	k4
2	#num#	k4
3	#num#	k4
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apollo	Apollo	k1gNnSc1
11	#num#	k4
-	-	kIx~
Tabulka	tabulka	k1gFnSc1
časů	čas	k1gInPc2
a	a	k8xC
událostí	událost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
1970	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
MIKKELSON	MIKKELSON	kA
<g/>
,	,	kIx,
Barbara	Barbara	k1gFnSc1
<g/>
;	;	kIx,
MIKKELSON	MIKKELSON	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
One	One	k1gMnSc1
Small	Small	k1gMnSc1
Misstep	Misstep	k1gMnSc1
<g/>
:	:	kIx,
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
First	First	k1gInSc1
Words	Words	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Moon	Moon	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snopes	Snopes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
Com	Com	k1gMnSc1
Urban	Urban	k1gMnSc1
Legends	Legends	k1gInSc4
Reference	reference	k1gFnSc2
Pages	Pagesa	k1gFnPc2
<g/>
,	,	kIx,
2006-10	2006-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
AP.	ap.	kA
Famous	Famous	k1gMnSc1
Lost	Lost	k1gMnSc1
Word	Word	kA
In	In	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
'	'	kIx"
<g/>
Mankind	Mankind	k1gMnSc1
<g/>
'	'	kIx"
Quote	Quot	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Newsvine	Newsvin	k1gInSc5
<g/>
,	,	kIx,
2012-8-25	2012-8-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
NICKELL	NICKELL	kA
<g/>
,	,	kIx,
Duane	Duan	k1gInSc5
S.	S.	kA
Guidebook	Guidebook	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
scientific	scientific	k1gMnSc1
traveler	traveler	k1gMnSc1
<g/>
:	:	kIx,
visiting	visiting	k1gInSc1
astronomy	astronom	k1gMnPc4
and	and	k?
space	space	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piscataway	Piscatawaa	k1gFnPc1
<g/>
:	:	kIx,
Rutgers	Rutgers	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8135	#num#	k4
<g/>
-	-	kIx~
<g/>
4374	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
175	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HANUSKOVÁ	HANUSKOVÁ	kA
<g/>
,	,	kIx,
Gabriela	Gabriela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malý	malý	k2eAgInSc1d1
krok	krok	k1gInSc1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
<g/>
...	...	k?
<g/>
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
udělal	udělat	k5eAaPmAgMnS
ve	v	k7c6
výroku	výrok	k1gInSc6
chybu	chyba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-6-5	2009-6-5	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SARKISSIAN	SARKISSIAN	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
M.	M.	kA
On	on	k3xPp3gMnSc1
Eagle	Eagle	k1gNnSc3
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Wings	Wingsa	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Parkes	Parkes	k1gInSc1
Observatory	Observator	k1gInPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Support	support	k1gInSc1
of	of	k?
the	the	k?
Apollo	Apollo	k1gMnSc1
11	#num#	k4
Mission	Mission	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Publications	Publications	k1gInSc1
of	of	k?
the	the	k?
Astronomical	Astronomical	k1gMnSc1
Society	societa	k1gFnSc2
of	of	k?
Australia	Australia	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2001	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
18	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
287	#num#	k4
<g/>
–	–	k?
<g/>
310	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
s.	s.	k?
287	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apollo	Apollo	k1gNnSc1
11	#num#	k4
/	/	kIx~
Vzorky	vzorek	k1gInPc7
z	z	k7c2
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
1970	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JONES	JONES	kA
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
M.	M.	kA
Apollo	Apollo	k1gNnSc1
Lunar	Lunar	k1gInSc1
Surface	Surface	k1gFnSc1
Journal	Journal	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2010-3-2	2010-3-2	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
AS	as	k1gNnSc2
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
40	#num#	k4
<g/>
-	-	kIx~
<g/>
5886	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LIND	Linda	k1gFnPc2
<g/>
,	,	kIx,
Don	Don	k1gMnSc1
L.	L.	kA
<g/>
;	;	kIx,
WRIGHT	WRIGHT	kA
<g/>
,	,	kIx,
Rebecca	Rebeccum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
JSC	JSC	kA
Oral	orat	k5eAaImAgInS
History	Histor	k1gInPc4
Project	Projecta	k1gFnPc2
:	:	kIx,
Oral	orat	k5eAaImAgMnS
History	Histor	k1gInPc4
Transcript	Transcripta	k1gFnPc2
Don	dona	k1gFnPc2
L.	L.	kA
Lind	Linda	k1gFnPc2
interviewed	interviewed	k1gMnSc1
by	by	k9
Rebecca	Rebecca	k1gMnSc1
Wright	Wright	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Houston	Houston	k1gInSc1
<g/>
:	:	kIx,
Johnson	Johnson	k1gMnSc1
Space	Space	k1gFnSc2
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
NASA	NASA	kA
<g/>
,	,	kIx,
2005-5-27	2005-5-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
14	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
National	National	k1gMnSc1
Air	Air	k1gMnSc1
and	and	k?
Space	Space	k1gFnSc2
Museum	museum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
After	After	k1gMnSc1
Splashdown	Splashdown	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
<g/>
:	:	kIx,
National	National	k1gFnSc1
Air	Air	k1gFnSc2
and	and	k?
Space	Space	k1gFnSc2
Museum	museum	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
276	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
280	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Space	Space	k1gMnSc4
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Spaec	k1gInSc2
Foundation	Foundation	k1gInSc1
Survey	Survea	k1gFnSc2
Reveals	Revealsa	k1gFnPc2
Broad	Broad	k1gInSc1
Range	Rang	k1gInSc2
of	of	k?
Space	Space	k1gMnSc1
Heroes	Heroes	k1gMnSc1
<g/>
;	;	kIx,
Early	earl	k1gMnPc4
Astronauts	Astronauts	k1gInSc1
Still	Still	k1gInSc1
the	the	k?
Most	most	k1gInSc1
Inspirational	Inspirational	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Colorado	Colorado	k1gNnSc1
Springs	Springsa	k1gFnPc2
<g/>
:	:	kIx,
Space	Space	k1gFnSc1
Foundation	Foundation	k1gInSc1
<g/>
,	,	kIx,
2012-10-27	2012-10-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
AFP	AFP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
US	US	kA
astronaut	astronaut	k1gMnSc1
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
dead	dead	k1gMnSc1
at	at	k?
82	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Nation	Nation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-8-26	2012-8-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SMITH	SMITH	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moondust	Moondust	k1gMnSc1
:	:	kIx,
In	In	k1gMnSc1
Search	Search	k1gMnSc1
of	of	k?
the	the	k?
Men	Men	k1gMnSc1
Who	Who	k1gMnSc1
Fell	Fell	k1gMnSc1
to	ten	k3xDgNnSc4
Earth	Earth	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Bloomsbury	Bloomsbur	k1gInPc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7475	#num#	k4
<g/>
-	-	kIx~
<g/>
6368	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
137	#num#	k4
<g/>
–	–	k?
<g/>
138	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Science	Science	k1gFnSc1
and	and	k?
Technology	technolog	k1gMnPc4
Division	Division	k1gInSc1
Library	Librar	k1gInPc1
of	of	k?
Congress	Congress	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronautics	Astronautics	k1gInSc1
and	and	k?
Aeronautics	Aeronautics	k1gInSc1
<g/>
,	,	kIx,
1970	#num#	k4
:	:	kIx,
Chronology	chronolog	k1gMnPc4
of	of	k?
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
Technology	technolog	k1gMnPc4
and	and	k?
Policy	Policum	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
DC	DC	kA
<g/>
:	:	kIx,
Scientific	Scientific	k1gMnSc1
and	and	k?
Technical	Technical	k1gFnSc1
Information	Information	k1gInSc1
Office	Office	kA
<g/>
,	,	kIx,
NASA	NASA	kA
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
176	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Science	Science	k1gFnSc1
and	and	k?
Technology	technolog	k1gMnPc4
Division	Division	k1gInSc4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Science	Science	k1gFnSc1
and	and	k?
Technology	technolog	k1gMnPc4
Division	Division	k1gInSc4
<g/>
,	,	kIx,
s.	s.	k?
195	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Science	Science	k1gFnSc1
and	and	k?
Technology	technolog	k1gMnPc4
Division	Division	k1gInSc4
<g/>
,	,	kIx,
s.	s.	k?
185	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Science	Science	k1gFnSc1
and	and	k?
Technology	technolog	k1gMnPc4
Division	Division	k1gInSc4
<g/>
,	,	kIx,
s.	s.	k?
190	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Science	Science	k1gFnSc1
and	and	k?
Technology	technolog	k1gMnPc4
Division	Division	k1gInSc4
<g/>
,	,	kIx,
s.	s.	k?
192	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
RILEY	RILEY	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
moon	moon	k1gMnSc1
walkers	walkers	k1gInSc1
<g/>
:	:	kIx,
Twelve	Twelev	k1gFnPc1
men	men	k?
who	who	k?
have	have	k1gInSc1
visited	visited	k1gMnSc1
another	anothra	k1gFnPc2
world	world	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guardian	Guardian	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2009-07-10	2009-07-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
602	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
584	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
600	#num#	k4
<g/>
–	–	k?
<g/>
603	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
589	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
610	#num#	k4
<g/>
–	–	k?
<g/>
616	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CODR	CODR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sto	sto	k4xCgNnSc4
hvězdných	hvězdný	k2eAgMnPc2d1
kapitánů	kapitán	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Práce	práce	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Neil	Neil	k1gMnSc1
Alden	Alden	k2eAgMnSc1d1
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
257	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
589	#num#	k4
<g/>
–	–	k?
<g/>
591	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
600	#num#	k4
<g/>
–	–	k?
<g/>
601	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
<g/>
595	#num#	k4
<g/>
–	–	k?
<g/>
596	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
EDO	Eda	k1gMnSc5
Corporation	Corporation	k1gInSc1
CEO	CEO	kA
James	James	k1gMnSc1
M.	M.	kA
Smith	Smith	k1gMnSc1
to	ten	k3xDgNnSc1
become	becom	k1gInSc5
Chairman	Chairman	k1gMnSc1
upon	upon	k1gNnSc1
retirement	retirement	k1gMnSc1
of	of	k?
Neil	Neil	k1gMnSc1
A.	A.	kA
Armstrong	Armstrong	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EDO	Eda	k1gMnSc5
Corporation	Corporation	k1gInSc1
<g/>
,	,	kIx,
2002-02-08	2002-02-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
596	#num#	k4
<g/>
–	–	k?
<g/>
598	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SCHORN	SCHORN	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Being	Being	k1gMnSc1
The	The	k1gMnSc1
First	First	k1gMnSc1
Man	Man	k1gMnSc1
On	on	k3xPp3gMnSc1
The	The	k1gMnSc7
Moon	Moon	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CBS	CBS	kA
News	News	k1gInSc1
web	web	k1gInSc1
site	site	k1gFnPc2
<g/>
,	,	kIx,
2009-7-15	2009-7-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
644	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DIXON-ENGEL	DIXON-ENGEL	k1gMnSc1
<g/>
,	,	kIx,
Tara	Tara	k1gMnSc1
<g/>
;	;	kIx,
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
Mike	Mike	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
:	:	kIx,
One	One	k1gMnSc1
Giant	Giant	k1gMnSc1
Leap	Leap	k1gMnSc1
for	forum	k1gNnPc2
Mankind	Mankinda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Sterling	sterling	k1gInSc1
Publishing	Publishing	k1gInSc1
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Sterling	sterling	k1gInSc1
Biographies	Biographies	k1gMnSc1
Series	Series	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781402760617	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WILFORD	WILFORD	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
Noble	Noble	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
First	First	k1gMnSc1
Man	Man	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Moon	Moon	k1gNnSc1
<g/>
,	,	kIx,
Dies	Dies	k1gInSc1
at	at	k?
82	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-8-25	2012-8-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MILLIS	MILLIS	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
P.	P.	kA
Biography	Biographa	k1gFnPc1
of	of	k?
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
:	:	kIx,
Man	Man	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
First	First	k1gInSc1
Steps	Steps	k1gInSc1
on	on	k3xPp3gMnSc1
the	the	k?
Moon	Moon	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
About	About	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JHA	jho	k1gNnSc2
<g/>
,	,	kIx,
Alok	Aloka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
breaks	breaks	k6eAd1
his	his	k1gNnSc4
silence	silenka	k1gFnSc3
to	ten	k3xDgNnSc1
give	givat	k5eAaPmIp3nS
accountants	accountants	k6eAd1
moon	moon	k1gInSc4
exclusive	exclusiev	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
2012-5-23	2012-5-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CBC	CBC	kA
News	News	k1gInSc1
:	:	kIx,
Technology	technolog	k1gMnPc4
&	&	k?
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
grants	grants	k6eAd1
rare	rarat	k5eAaPmIp3nS
interview	interview	k1gInSc1
to	ten	k3xDgNnSc4
accountants	accountants	k6eAd1
organization	organization	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CBC	CBC	kA
<g/>
,	,	kIx,
2012-5-24	2012-5-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SUTTON	SUTTON	kA
<g/>
,	,	kIx,
Jane	Jan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
first	first	k1gMnSc1
man	man	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
moon	moon	k1gNnSc1
<g/>
,	,	kIx,
recovering	recovering	k1gInSc1
from	from	k1gInSc1
heart	heart	k1gInSc4
surgery	surgera	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reutersa	k1gFnPc2
<g/>
,	,	kIx,
2012-8-8	2012-8-8	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
639	#num#	k4
<g/>
–	–	k?
<g/>
640	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CNN	CNN	kA
Wire	Wire	k1gInSc1
Staff	Staff	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Space	Space	k1gFnSc1
legend	legenda	k1gFnPc2
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
dies	dies	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
,	,	kIx,
2012-8-26	2012-8-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
první	první	k4xOgMnSc1
člověk	člověk	k1gMnSc1
na	na	k7c6
Měsíci	měsíc	k1gInSc6
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2012-8-26	2012-8-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TERRETT	TERRETT	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Above	Aboev	k1gFnSc2
the	the	k?
stars	stars	k1gInSc1
now	now	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
:	:	kIx,
Al	ala	k1gFnPc2
Jazeera	Jazeero	k1gNnSc2
English	Englisha	k1gFnPc2
<g/>
,	,	kIx,
2012-9-15	2012-9-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DUNBAR	DUNBAR	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
Laid	Laid	k1gMnSc1
to	ten	k3xDgNnSc4
Rest	rest	k6eAd1
in	in	k?
Atlantic	Atlantice	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2012-9-25	2012-9-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Planetary	Planetar	k1gInPc1
Names	Names	k1gInSc1
<g/>
:	:	kIx,
Crater	Crater	k1gInSc1
<g/>
,	,	kIx,
craters	craters	k1gInSc1
<g/>
:	:	kIx,
Armstrong	Armstrong	k1gMnSc1
on	on	k3xPp3gMnSc1
Moon	Moon	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
USGS	USGS	kA
Astrogeology	Astrogeolog	k1gMnPc4
Science	Science	k1gFnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2010-10-18	2010-10-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RÜKL	RÜKL	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
Měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aventinum	Aventinum	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85277	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Arago	Arago	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
96	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
6469	#num#	k4
Armstrong	Armstrong	k1gMnSc1
(	(	kIx(
<g/>
1982	#num#	k4
PC	PC	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
JPL	JPL	kA
<g/>
,	,	kIx,
NASA	NASA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
KNIGHT	KNIGHT	kA
<g/>
,	,	kIx,
Andy	Anda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
the	the	k?
moon	moon	k1gInSc1
:	:	kIx,
Armstrong	Armstrong	k1gMnSc1
space	spako	k6eAd1
museum	museum	k1gNnSc4
offers	offersa	k1gFnPc2
history	histor	k1gInPc1
lessons	lessons	k6eAd1
on	on	k3xPp3gMnSc1
space	space	k1gMnSc1
travel	travel	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cincinnati	Cincinnati	k1gMnSc1
<g/>
.	.	kIx.
<g/>
Com	Com	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Search	Search	k1gInSc1
for	forum	k1gNnPc2
Public	publicum	k1gNnPc2
schools	schools	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
DC	DC	kA
<g/>
:	:	kIx,
National	National	k1gMnSc2
Center	centrum	k1gNnPc2
for	forum	k1gNnPc2
Education	Education	k1gInSc4
Statistics	Statisticsa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JOHNSTON	JOHNSTON	kA
<g/>
,	,	kIx,
Willie	Willie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recalling	Recalling	k1gInSc1
Moon	Moon	k1gMnSc1
man	man	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
'	'	kIx"
<g/>
muckle	muckle	k6eAd1
<g/>
'	'	kIx"
leap	leap	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
2009-7-20	2009-7-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
591	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
VENERE	VENERE	kA
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
Hall	Hall	k1gMnSc1
is	is	k?
new	new	k?
home	homat	k5eAaPmIp3nS
to	ten	k3xDgNnSc4
Purdue	Purdue	k1gNnSc4
engineering	engineering	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Purdue	Purdu	k1gInSc2
University	universita	k1gFnSc2
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
2007-10-27	2007-10-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Armstrong	Armstrong	k1gMnSc1
Air	Air	k1gMnSc1
&	&	k?
Space	Space	k1gFnSc2
Museum	museum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
About	About	k1gMnSc1
the	the	k?
Museum	museum	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wapakoneta	Wapakoneta	k1gFnSc1
<g/>
:	:	kIx,
Armstrong	Armstrong	k1gMnSc1
Air	Air	k1gMnSc1
&	&	k?
Space	Space	k1gFnSc1
Museum	museum	k1gNnSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
US	US	kA
Navy	Navy	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navy	Navy	k?
Announces	Announces	k1gMnSc1
Research	Research	k1gMnSc1
Vessel	Vessel	k1gMnSc1
to	ten	k3xDgNnSc1
be	be	k?
Named	Named	k1gInSc1
in	in	k?
Honor	Honor	k1gFnSc2
of	of	k?
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
:	:	kIx,
US	US	kA
Navy	Navy	k?
<g/>
,	,	kIx,
2012-9-24	2012-9-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
US	US	kA
Navy	Navy	k?
receives	receives	k1gMnSc1
new	new	k?
research	research	k1gMnSc1
vessel	vessel	k1gMnSc1
R	R	kA
/	/	kIx~
V	V	kA
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval-technology	Naval-technolog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2015-09-29	2015-09-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jupiter	Jupiter	k1gMnSc1
<g/>
9	#num#	k4
<g/>
Productions	Productionsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Quantum	Quantum	k1gNnSc1
Quest	Quest	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jupiter	Jupiter	k1gInSc1
<g/>
9	#num#	k4
<g/>
Productions	Productionsa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Films	Filmsa	k1gFnPc2
in	in	k?
production	production	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Jupiter	Jupiter	k1gInSc4
<g/>
9	#num#	k4
<g/>
Productions	Productionsa	k1gFnPc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jupiter	Jupiter	k1gMnSc1
<g/>
9	#num#	k4
<g/>
Productions	Productions	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
↑	↑	k?
KUBICOVÁ	Kubicová	k1gFnSc1
<g/>
,	,	kIx,
Romana	Romana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památné	památné	k1gNnSc1
místo	místo	k6eAd1
skautů	skaut	k1gMnPc2
v	v	k7c6
Beskydech	Beskyd	k1gInPc6
mohyla	mohyla	k1gFnSc1
Ivančena	Ivančen	k2eAgFnSc1d1
mění	měnit	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
podobu	podoba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Hansen	Hansen	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
630	#num#	k4
<g/>
–	–	k?
<g/>
631	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BARBREE	BARBREE	kA
<g/>
,	,	kIx,
Jay	Jay	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
:	:	kIx,
A	A	kA
Life	Life	k1gInSc1
of	of	k?
Flight	Flight	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
NY	NY	kA
<g/>
:	:	kIx,
Thomas	Thomas	k1gMnSc1
Dunne	Dunn	k1gInSc5
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
320	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781250040718	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HANSEN	HANSEN	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
R.	R.	kA
First	First	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Life	Lif	k1gFnSc2
of	of	k?
Neil	Neil	k1gMnSc1
A.	A.	kA
Armstrong	Armstrong	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Simon	Simon	k1gMnSc1
&	&	k?
Schuster	Schuster	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
769	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7432	#num#	k4
<g/>
-	-	kIx~
<g/>
5631	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Autorizovaný	autorizovaný	k2eAgInSc1d1
životopis	životopis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
CHAIKIN	CHAIKIN	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
man	man	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
moon	moon	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Penguin	Penguin	k2eAgInSc1d1
books	books	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
24146	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
FRANCIS	FRANCIS	kA
<g/>
,	,	kIx,
French	French	k1gMnSc1
<g/>
;	;	kIx,
BURGESS	BURGESS	kA
<g/>
,	,	kIx,
Colin	Colin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
the	the	k?
Shadow	Shadow	k1gMnSc1
of	of	k?
the	the	k?
Moon	Moon	k1gInSc1
:	:	kIx,
A	a	k9
Challenging	Challenging	k1gInSc1
Journey	Journea	k1gMnSc2
to	ten	k3xDgNnSc1
Tranquility	Tranquilit	k1gInPc1
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
-	-	kIx~
<g/>
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lincoln	Lincoln	k1gMnSc1
<g/>
,	,	kIx,
Nebraska	Nebraska	k1gFnSc1
<g/>
:	:	kIx,
U	u	k7c2
of	of	k?
Nebraska	Nebrask	k1gInSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780803209848	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KRANZ	KRANZ	kA
<g/>
,	,	kIx,
Gene	gen	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Failure	Failur	k1gMnSc5
is	is	k?
not	nota	k1gFnPc2
an	an	k?
Option	Option	k1gInSc1
<g/>
:	:	kIx,
Mission	Mission	k1gInSc1
Control	Controla	k1gFnPc2
From	From	k1gMnSc1
Mercury	Mercura	k1gFnSc2
to	ten	k3xDgNnSc4
Apollo	Apollo	k1gNnSc4
13	#num#	k4
and	and	k?
Beyond	Beyond	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Simon	Simon	k1gMnSc1
&	&	k?
Schuster	Schuster	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7432	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
79	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NELSON	Nelson	k1gMnSc1
<g/>
,	,	kIx,
Craig	Craig	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rocket	Rocket	k1gMnSc1
Men	Men	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Epic	Epic	k1gFnSc1
Story	story	k1gFnSc1
of	of	k?
the	the	k?
First	First	k1gMnSc1
Men	Men	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
Moon	Moon	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc4
<g/>
:	:	kIx,
John	John	k1gMnSc1
Murray	Murraa	k1gFnSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781101057735	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SHERROD	SHERROD	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apollo	Apollo	k1gNnSc1
Expeditions	Expeditionsa	k1gFnPc2
to	ten	k3xDgNnSc1
the	the	k?
Moon	Moon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.	D.	kA
C.	C.	kA
<g/>
:	:	kIx,
NASA	NASA	kA
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Men	Men	k1gFnSc1
for	forum	k1gNnPc2
the	the	k?
Moon	Moon	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
THOMPSON	THOMPSON	kA
<g/>
,	,	kIx,
Milton	Milton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
At	At	k1gFnSc1
The	The	k1gFnSc1
Edge	Edge	k1gFnSc1
Of	Of	k1gFnSc1
Space	Space	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
X-15	X-15	k1gFnPc2
Flight	Flight	k2eAgInSc1d1
Program	program	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
<g/>
:	:	kIx,
Smithsonian	Smithsonian	k1gInSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
56098	#num#	k4
<g/>
-	-	kIx~
<g/>
107	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
VÍTEK	Vítek	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
;	;	kIx,
KRUPIČKA	Krupička	k1gMnSc1
<g/>
,	,	kIx,
Jozef	Jozef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apollo	Apollo	k1gNnSc1
11	#num#	k4
–	–	k?
popis	popis	k1gInSc1
letu	let	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
1994	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Včetně	včetně	k7c2
odkazů	odkaz	k1gInPc2
na	na	k7c4
další	další	k2eAgInPc4d1
články	článek	k1gInPc4
o	o	k7c6
letu	let	k1gInSc6
Apolla	Apollo	k1gNnSc2
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
NASA	NASA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronaut	astronaut	k1gMnSc1
Bio	Bio	k?
<g/>
:	:	kIx,
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2012-8	2012-8	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biografie	biografie	k1gFnSc1
Neila	Neilo	k1gNnSc2
Armstronga	Armstrong	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
CORNISH	CORNISH	kA
<g/>
,	,	kIx,
Scott	Scott	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
Signature	Signatur	k1gMnSc5
Exemplars	Exemplars	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CollectSpace	CollectSpace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
JONES	JONES	kA
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
One	One	k1gMnSc1
Small	Small	k1gMnSc1
Step	step	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apollo	Apollo	k1gNnSc1
11	#num#	k4
Lunar	Lunara	k1gFnPc2
Surface	Surface	k1gFnSc1
Journal	Journal	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
,	,	kIx,
1995	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MALLEY	MALLEY	kA
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CPA	cpát	k5eAaImSgInS
Australia	Australius	k1gMnSc2
Presents	Presentsa	k1gFnPc2
:	:	kIx,
An	An	k1gMnSc1
Audience	audience	k1gFnSc2
with	with	k1gMnSc1
Neil	Neil	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CPA	cpát	k5eAaImSgInS
Australia	Australius	k1gMnSc2
<g/>
,	,	kIx,
2012-5-23	2012-5-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyřdílný	čtyřdílný	k2eAgInSc1d1
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
hodinový	hodinový	k2eAgInSc1d1
<g/>
,	,	kIx,
rozhovor	rozhovor	k1gInSc1
Alexe	Alex	k1gMnSc5
Malleyho	Malley	k1gMnSc2
s	s	k7c7
Neilem	Neil	k1gMnSc7
Armstrongem	Armstrong	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
skuk	skuk	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
45	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
11896416X	11896416X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2147	#num#	k4
959X	959X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80008815	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500490014	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
111826406	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80008815	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
|	|	kIx~
Měsíc	měsíc	k1gInSc1
</s>
