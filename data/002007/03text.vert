<s>
Kolektivní	kolektivní	k2eAgInSc1d1	kolektivní
sport	sport	k1gInSc1	sport
je	být	k5eAaImIp3nS	být
opakem	opak	k1gInSc7	opak
individuálního	individuální	k2eAgNnSc2d1	individuální
<g/>
,	,	kIx,	,
a	a	k8xC	a
jako	jako	k9	jako
takový	takový	k3xDgInSc1	takový
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
kolektiv	kolektiv	k1gInSc4	kolektiv
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kolektivních	kolektivní	k2eAgInPc6d1	kolektivní
sportech	sport	k1gInPc6	sport
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
tedy	tedy	k8xC	tedy
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgFnSc7d2	vyšší
měrou	míra	k1gFnSc7wR	míra
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
součinností	součinnost	k1gFnSc7	součinnost
a	a	k8xC	a
souhrou	souhra	k1gFnSc7	souhra
celého	celý	k2eAgInSc2d1	celý
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
než	než	k8xS	než
individuálními	individuální	k2eAgInPc7d1	individuální
výkony	výkon	k1gInPc7	výkon
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
kolektivních	kolektivní	k2eAgMnPc2d1	kolektivní
sportů	sport	k1gInPc2	sport
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
počet	počet	k1gInSc4	počet
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
a	a	k8xC	a
možnosti	možnost	k1gFnSc6	možnost
střídání	střídání	k1gNnSc2	střídání
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rekreačního	rekreační	k2eAgNnSc2d1	rekreační
provozování	provozování	k1gNnSc2	provozování
těchto	tento	k3xDgInPc2	tento
sportů	sport	k1gInPc2	sport
není	být	k5eNaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
bezpodmínečně	bezpodmínečně	k6eAd1	bezpodmínečně
nutné	nutný	k2eAgNnSc1d1	nutné
se	se	k3xPyFc4	se
těmito	tento	k3xDgFnPc7	tento
pravidly	pravidlo	k1gNnPc7	pravidlo
řídit	řídit	k5eAaImF	řídit
a	a	k8xC	a
počet	počet	k1gInSc1	počet
hráčů	hráč	k1gMnPc2	hráč
i	i	k8xC	i
podmínky	podmínka	k1gFnSc2	podmínka
střídání	střídání	k1gNnSc2	střídání
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
upravit	upravit	k5eAaPmF	upravit
dle	dle	k7c2	dle
vlastních	vlastní	k2eAgFnPc2d1	vlastní
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
hry	hra	k1gFnSc2	hra
obvykle	obvykle	k6eAd1	obvykle
řídí	řídit	k5eAaImIp3nS	řídit
a	a	k8xC	a
posuzuje	posuzovat	k5eAaImIp3nS	posuzovat
více	hodně	k6eAd2	hodně
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
kolektivních	kolektivní	k2eAgInPc2d1	kolektivní
sportů	sport	k1gInPc2	sport
<g/>
.	.	kIx.	.
<g/>
''	''	k?	''
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kolektivní	kolektivní	k2eAgInSc4d1	kolektivní
sport	sport	k1gInSc4	sport
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
