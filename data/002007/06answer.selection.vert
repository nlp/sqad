<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
kolektivních	kolektivní	k2eAgMnPc2d1	kolektivní
sportů	sport	k1gInPc2	sport
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
počet	počet	k1gInSc4	počet
hráčů	hráč	k1gMnPc2	hráč
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
a	a	k8xC	a
možnosti	možnost	k1gFnSc6	možnost
střídání	střídání	k1gNnSc2	střídání
hráčů	hráč	k1gMnPc2	hráč
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
