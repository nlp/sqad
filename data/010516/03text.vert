<p>
<s>
Jablečný	jablečný	k2eAgInSc1d1	jablečný
závin	závin	k1gInSc1	závin
neboli	neboli	k8xC	neboli
štrúdl	štrúdl	k?	štrúdl
<g/>
,	,	kIx,	,
také	také	k9	také
štrudle	štrudle	k?	štrudle
či	či	k8xC	či
štrúdle	štrúdle	k?	štrúdle
[	[	kIx(	[
<g/>
f.	f.	k?	f.
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Apfelstrudel	Apfelstrudlo	k1gNnPc2	Apfelstrudlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
moučníku	moučník	k1gInSc2	moučník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
včetně	včetně	k7c2	včetně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
závinů	závin	k1gInPc2	závin
–	–	k?	–
například	například	k6eAd1	například
tvarohový	tvarohový	k2eAgInSc4d1	tvarohový
<g/>
,	,	kIx,	,
višňový	višňový	k2eAgInSc4d1	višňový
<g/>
,	,	kIx,	,
makový	makový	k2eAgInSc4d1	makový
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
slané	slaný	k2eAgInPc4d1	slaný
záviny	závin	k1gInPc4	závin
se	s	k7c7	s
špenátem	špenát	k1gInSc7	špenát
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
a	a	k8xC	a
příprava	příprava	k1gFnSc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Jablečný	jablečný	k2eAgInSc1d1	jablečný
závin	závin	k1gInSc1	závin
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
podlouhlého	podlouhlý	k2eAgInSc2d1	podlouhlý
plátu	plát	k1gInSc2	plát
těsta	těsto	k1gNnSc2	těsto
s	s	k7c7	s
náplní	náplň	k1gFnSc7	náplň
z	z	k7c2	z
nakrájených	nakrájený	k2eAgInPc2d1	nakrájený
nebo	nebo	k8xC	nebo
hrubě	hrubě	k6eAd1	hrubě
nastrouhaných	nastrouhaný	k2eAgNnPc2d1	nastrouhané
jablek	jablko	k1gNnPc2	jablko
<g/>
,	,	kIx,	,
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
skořice	skořice	k1gFnSc2	skořice
<g/>
,	,	kIx,	,
rozinek	rozinka	k1gFnPc2	rozinka
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
strouhanky	strouhanka	k1gFnSc2	strouhanka
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
přidávají	přidávat	k5eAaImIp3nP	přidávat
i	i	k9	i
vlašské	vlašský	k2eAgInPc4d1	vlašský
ořechy	ořech	k1gInPc4	ořech
či	či	k8xC	či
nasekané	nasekaný	k2eAgFnPc4d1	nasekaná
mandle	mandle	k1gFnPc4	mandle
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ochucení	ochucení	k1gNnSc3	ochucení
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
použít	použít	k5eAaPmF	použít
rum	rum	k1gInSc4	rum
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
pečením	pečení	k1gNnSc7	pečení
se	se	k3xPyFc4	se
těsto	těsto	k1gNnSc1	těsto
s	s	k7c7	s
náplní	náplň	k1gFnSc7	náplň
zavine	zavinout	k5eAaPmIp3nS	zavinout
jako	jako	k9	jako
roláda	roláda	k1gFnSc1	roláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šťavnatý	šťavnatý	k2eAgInSc1d1	šťavnatý
jablečný	jablečný	k2eAgInSc1d1	jablečný
závin	závin	k1gInSc1	závin
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
péci	péct	k5eAaImF	péct
z	z	k7c2	z
kvalitních	kvalitní	k2eAgNnPc2d1	kvalitní
jablek	jablko	k1gNnPc2	jablko
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
mírně	mírně	k6eAd1	mírně
natrpklá	natrpklý	k2eAgNnPc1d1	natrpklé
<g/>
,	,	kIx,	,
svěží	svěží	k2eAgNnPc1d1	svěží
a	a	k8xC	a
aromatická	aromatický	k2eAgNnPc1d1	aromatické
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
těsto	těsto	k1gNnSc1	těsto
na	na	k7c4	na
jablečný	jablečný	k2eAgInSc4d1	jablečný
závin	závin	k1gInSc4	závin
lze	lze	k6eAd1	lze
(	(	kIx(	(
<g/>
plnohodnotně	plnohodnotně	k6eAd1	plnohodnotně
<g/>
)	)	kIx)	)
nahradit	nahradit	k5eAaPmF	nahradit
těstem	těsto	k1gNnSc7	těsto
listovým	listový	k2eAgNnSc7d1	listové
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podávání	podávání	k1gNnSc2	podávání
===	===	k?	===
</s>
</p>
<p>
<s>
Hotový	hotový	k2eAgInSc1d1	hotový
štrúdl	štrúdl	k?	štrúdl
se	se	k3xPyFc4	se
peče	péct	k5eAaImIp3nS	péct
v	v	k7c6	v
troubě	trouba	k1gFnSc6	trouba
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
podáván	podáván	k2eAgInSc1d1	podáván
ještě	ještě	k6eAd1	ještě
teplý	teplý	k2eAgInSc1d1	teplý
<g/>
,	,	kIx,	,
posypaný	posypaný	k2eAgInSc1d1	posypaný
moučkovým	moučkový	k2eAgInSc7d1	moučkový
cukrem	cukr	k1gInSc7	cukr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
také	také	k9	také
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
podávat	podávat	k5eAaImF	podávat
závin	závin	k1gInSc4	závin
s	s	k7c7	s
vanilkovou	vanilkový	k2eAgFnSc7d1	vanilková
zmrzlinou	zmrzlina	k1gFnSc7	zmrzlina
<g/>
,	,	kIx,	,
šlehačkou	šlehačka	k1gFnSc7	šlehačka
nebo	nebo	k8xC	nebo
vanilkovým	vanilkový	k2eAgInSc7d1	vanilkový
krémem	krém	k1gInSc7	krém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Apfelstrudel	Apfelstrudlo	k1gNnPc2	Apfelstrudlo
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jablečný	jablečný	k2eAgInSc4d1	jablečný
závin	závin	k1gInSc4	závin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
štrúdl	štrúdl	k?	štrúdl
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Recept	recept	k1gInSc1	recept
na	na	k7c6	na
ekuchařce	ekuchařka	k1gFnSc6	ekuchařka
</s>
</p>
<p>
<s>
Recept	recept	k1gInSc1	recept
s	s	k7c7	s
ořechy	ořech	k1gInPc7	ořech
na	na	k7c4	na
Online	Onlin	k1gMnSc5	Onlin
kuchařce	kuchařka	k1gFnSc3	kuchařka
</s>
</p>
<p>
<s>
Recept	recept	k1gInSc1	recept
na	na	k7c6	na
webu	web	k1gInSc6	web
Ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Závin	závin	k1gInSc1	závin
</s>
</p>
