<s>
Rusko-čchingská	Rusko-čchingský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1652	#num#	k4
<g/>
–	–	k?
<g/>
1689	#num#	k4
byly	být	k5eAaImAgInP
boje	boj	k1gInPc1
mezi	mezi	k7c7
kozáky	kozák	k1gMnPc7
<g/>
,	,	kIx,
kolonisty	kolonista	k1gMnPc7
a	a	k8xC
vojáky	voják	k1gMnPc7
ruského	ruský	k2eAgNnSc2d1
carství	carství	k1gNnSc2
a	a	k8xC
vojsky	vojsky	k6eAd1
říše	říše	k1gFnSc1
Čching	Čching	k1gInSc4
o	o	k7c4
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
územím	území	k1gNnSc7
na	na	k7c6
levém	levý	k2eAgInSc6d1
(	(	kIx(
<g/>
severním	severní	k2eAgInSc6d1
<g/>
)	)	kIx)
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Amuru	Amur	k1gInSc2
<g/>
.	.	kIx.
</s>