<s>
Rusko-čchingská	Rusko-čchingský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Rusko-čchingská	Rusko-čchingský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Šrafovaně	Šrafovaně	k6eAd1
sporné	sporný	k2eAgNnSc1d1
území	území	k1gNnSc1
mezi	mezi	k7c7
Ruskem	Rusko	k1gNnSc7
(	(	kIx(
<g/>
růžové	růžový	k2eAgFnPc1d1
<g/>
)	)	kIx)
a	a	k8xC
Čchingskou	Čchingský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
(	(	kIx(
<g/>
modře	modro	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černě	černě	k6eAd1
vyznačena	vyznačen	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
podle	podle	k7c2
Něrčinské	Něrčinský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ukončila	ukončit	k5eAaPmAgFnS
konflikt	konflikt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1652	#num#	k4
<g/>
–	–	k?
<g/>
1689	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Dálný	dálný	k2eAgInSc1d1
východ	východ	k1gInSc1
<g/>
,	,	kIx,
Mandžusko	Mandžusko	k1gNnSc1
<g/>
,	,	kIx,
Zabajkalsko	Zabajkalsko	k1gNnSc1
</s>
<s>
casus	casus	k1gInSc4
belli	bell	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
Poamuří	Poamuří	k1gNnSc2
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
vítězství	vítězství	k1gNnSc1
Čchingů	Čching	k1gInPc2
</s>
<s>
uzavření	uzavření	k1gNnSc1
míru	mír	k1gInSc2
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
</s>
<s>
vytlačení	vytlačení	k1gNnSc1
Rusů	Rus	k1gMnPc2
z	z	k7c2
Poamuří	Poamuří	k1gNnSc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Říše	říše	k1gFnSc1
Čching	Čching	k1gInSc1
Království	království	k1gNnSc6
Čoson	Čoson	k1gInSc1
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
carství	carství	k1gNnSc1
Ruské	ruský	k2eAgFnSc2d1
carstvíkozáci	carstvíkozák	k1gMnPc5
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
čchingští	čchingský	k1gMnPc1
<g/>
:	:	kIx,
<g/>
císař	císař	k1gMnSc1
Kchang-siChajse	Kchang-siChajse	k1gFnSc2
(	(	kIx(
<g/>
海	海	k?
<g/>
)	)	kIx)
<g/>
Chife	Chif	k1gInSc5
(	(	kIx(
<g/>
希	希	k?
<g/>
)	)	kIx)
<g/>
Minggadari	Minggadar	k1gFnSc2
(	(	kIx(
<g/>
明	明	k?
<g/>
)	)	kIx)
<g/>
ŠarhudaPengcunLangtanSabsu	ŠarhudaPengcunLangtanSabs	k1gInSc2
SonggotuTchung	SonggotuTchung	k1gMnSc1
Kuo-kangmongolští	Kuo-kangmongolský	k2eAgMnPc1d1
<g/>
:	:	kIx,
<g/>
ČichundordžCecen-nojonŠidiširi	ČichundordžCecen-nojonŠidiširi	k1gNnPc2
bagatur-chuntajdži	bagatur-chuntajdzat	k5eAaPmIp1nS
</s>
<s>
Jerofej	Jerofat	k5eAaPmRp2nS
Chabarov	Chabarovo	k1gNnPc2
Onufrij	Onufrij	k1gFnSc2
Stěpanov	Stěpanovo	k1gNnPc2
Alexej	Alexej	k1gMnSc1
Tolbuzin	Tolbuzin	k1gMnSc1
Afanasij	Afanasij	k1gFnPc2
Bejton	Bejton	k1gInSc1
Fjodor	Fjodor	k1gMnSc1
Golovin	Golovina	k1gFnPc2
Ivan	Ivana	k1gFnPc2
Vlasov	Vlasov	k1gInSc1
Fjodor	Fjodor	k1gMnSc1
Skrypicyn	Skrypicyn	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Grabov	Grabov	k1gInSc4
Anton	Anton	k1gMnSc1
Smalenberg	Smalenberg	k1gMnSc1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
3	#num#	k4
000	#num#	k4
-	-	kIx~
10	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
korouhve	korouhev	k1gFnPc1
Mandžuůa	Mandžuůum	k1gNnSc2
chanští	chanštit	k5eAaPmIp3nP
vojáci	voják	k1gMnPc1
<g/>
)	)	kIx)
200	#num#	k4
korejskýchdělostřelců	korejskýchdělostřelec	k1gInPc2
<g/>
,	,	kIx,
60	#num#	k4
<g/>
důstojníků	důstojník	k1gMnPc2
a	a	k8xC
tlumočníků	tlumočník	k1gMnPc2
</s>
<s>
2	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
několik	několik	k4yIc1
stovek-	stovek-	k?
<g/>
5000	#num#	k4
32	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
mrtvých	mrtvý	k1gMnPc2
<g/>
,24	,24	k4
raněných	raněný	k1gMnPc2
<g/>
,1	,1	k4
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c6
zranění	zranění	k1gNnSc6
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
cca	cca	kA
800	#num#	k4
<g/>
-	-	kIx~
<g/>
1000	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rusko-čchingská	Rusko-čchingský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1652	#num#	k4
<g/>
–	–	k?
<g/>
1689	#num#	k4
byly	být	k5eAaImAgInP
boje	boj	k1gInPc1
mezi	mezi	k7c7
kozáky	kozák	k1gMnPc7
<g/>
,	,	kIx,
kolonisty	kolonista	k1gMnPc7
a	a	k8xC
vojáky	voják	k1gMnPc7
ruského	ruský	k2eAgNnSc2d1
carství	carství	k1gNnSc2
a	a	k8xC
vojsky	vojsky	k6eAd1
říše	říše	k1gFnSc1
Čching	Čching	k1gInSc4
o	o	k7c4
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
územím	území	k1gNnSc7
na	na	k7c6
levém	levý	k2eAgInSc6d1
(	(	kIx(
<g/>
severním	severní	k2eAgInSc6d1
<g/>
)	)	kIx)
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Amuru	Amur	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konflikt	konflikt	k1gInSc1
vyvolala	vyvolat	k5eAaPmAgFnS
ruská	ruský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
levobřeží	levobřeží	k1gNnSc2
Amuru	Amur	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
říše	říš	k1gFnSc2
Čching	Čching	k1gInSc1
počítala	počítat	k5eAaImAgFnS
mezi	mezi	k7c4
svou	svůj	k3xOyFgFnSc4
sféru	sféra	k1gFnSc4
vlivu	vliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boje	boj	k1gInSc2
vyvrcholily	vyvrcholit	k5eAaPmAgFnP
dvojím	dvojit	k5eAaImIp1nS
obležením	obležení	k1gNnSc7
ruské	ruský	k2eAgFnSc2d1
pevnosti	pevnost	k1gFnSc2
Albazin	Albazina	k1gFnPc2
v	v	k7c6
letech	léto	k1gNnPc6
1685	#num#	k4
a	a	k8xC
1686	#num#	k4
<g/>
–	–	k?
<g/>
1687	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válku	válek	k1gInSc2
ukončila	ukončit	k5eAaPmAgFnS
Něrčinská	Něrčinský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
uzavřená	uzavřený	k2eAgFnSc1d1
roku	rok	k1gInSc2
1689	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
sporné	sporný	k2eAgNnSc1d1
území	území	k1gNnSc1
přešlo	přejít	k5eAaPmAgNnS
k	k	k7c3
říši	říš	k1gFnSc3
Čching	Čching	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Počátkem	počátkem	k7c2
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Rusové	Rusová	k1gFnSc2
během	během	k7c2
průzkumu	průzkum	k1gInSc2
a	a	k8xC
zabírání	zabírání	k1gNnSc2
Sibiře	Sibiř	k1gFnSc2
a	a	k8xC
Dálného	dálný	k2eAgInSc2d1
východu	východ	k1gInSc2
dospěli	dochvít	k5eAaPmAgMnP
do	do	k7c2
povodí	povodí	k1gNnSc2
Amuru	Amur	k1gInSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1643	#num#	k4
ho	on	k3xPp3gNnSc2
prozkoumávali	prozkoumávat	k5eAaImAgMnP
a	a	k8xC
kolonizovali	kolonizovat	k5eAaBmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postup	postup	k1gInSc1
Rusů	Rus	k1gMnPc2
vzbudil	vzbudit	k5eAaPmAgInS
nevoli	nevole	k1gFnSc4
u	u	k7c2
vlády	vláda	k1gFnSc2
mandžuské	mandžuský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
Čching	Čching	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandžuové	Mandžu	k1gMnPc1
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
vyhnat	vyhnat	k5eAaPmF
Rusy	Rus	k1gMnPc4
z	z	k7c2
Poamuří	Poamuří	k1gNnSc2
a	a	k8xC
roku	rok	k1gInSc2
1652	#num#	k4
zahájili	zahájit	k5eAaPmAgMnP
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1658	#num#	k4
vysídlili	vysídlit	k5eAaPmAgMnP
většinu	většina	k1gFnSc4
domorodců	domorodec	k1gMnPc2
z	z	k7c2
horního	horní	k2eAgInSc2d1
a	a	k8xC
středního	střední	k2eAgInSc2d1
toku	tok	k1gInSc2
Amuru	Amur	k1gInSc2
na	na	k7c4
jih	jih	k1gInSc4
k	k	k7c3
řekám	řeka	k1gFnPc3
Non	Non	k1gFnPc2
a	a	k8xC
Sungari	Sungar	k1gMnPc1
a	a	k8xC
rozbili	rozbít	k5eAaPmAgMnP
ruské	ruský	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
na	na	k7c6
Amuru	Amur	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
amurském	amurský	k2eAgInSc6d1
regionu	region	k1gInSc6
pak	pak	k6eAd1
na	na	k7c4
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
nezůstala	zůstat	k5eNaPmAgFnS
žádná	žádný	k3yNgFnSc1
organizovaná	organizovaný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
a	a	k8xC
nikdo	nikdo	k3yNnSc1
ho	on	k3xPp3gMnSc4
nekontroloval	kontrolovat	k5eNaImAgMnS,k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
několika	několik	k4yIc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
Rusové	Rus	k1gMnPc1
vrátili	vrátit	k5eAaPmAgMnP
zpět	zpět	k6eAd1
a	a	k8xC
roku	rok	k1gInSc2
1665	#num#	k4
na	na	k7c6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
Amuru	Amur	k1gInSc2
vystavěli	vystavět	k5eAaPmAgMnP
městečko	městečko	k1gNnSc4
Albazin	Albazina	k1gFnPc2
a	a	k8xC
začali	začít	k5eAaPmAgMnP
osidlovat	osidlovat	k5eAaImF
region	region	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
se	se	k3xPyFc4
ruská	ruský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
pokusila	pokusit	k5eAaPmAgFnS
s	s	k7c7
říší	říš	k1gFnSc7
Čching	Čching	k1gInSc4
vyjednávat	vyjednávat	k5eAaImF
a	a	k8xC
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
–	–	k?
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
vyslala	vyslat	k5eAaPmAgFnS
do	do	k7c2
Pekingu	Peking	k1gInSc2
několik	několik	k4yIc4
diplomatických	diplomatický	k2eAgFnPc2d1
misí	mise	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
navázat	navázat	k5eAaPmF
diplomatické	diplomatický	k2eAgInPc4d1
a	a	k8xC
obchodní	obchodní	k2eAgInPc4d1
styky	styk	k1gInPc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
otázka	otázka	k1gFnSc1
hranic	hranice	k1gFnPc2
zůstala	zůstat	k5eAaPmAgFnS
nevyřešená	vyřešený	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
porážce	porážka	k1gFnSc6
povstání	povstání	k1gNnSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
roku	rok	k1gInSc2
1681	#num#	k4
si	se	k3xPyFc3
Mandžuové	Mandžu	k1gMnPc1
uvolnili	uvolnit	k5eAaPmAgMnP
síly	síla	k1gFnPc4
pro	pro	k7c4
boj	boj	k1gInSc4
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pečlivé	pečlivý	k2eAgFnSc6d1
přípravě	příprava	k1gFnSc6
zahájili	zahájit	k5eAaPmAgMnP
ofenzívu	ofenzíva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1683	#num#	k4
vybudovali	vybudovat	k5eAaPmAgMnP
základnu	základna	k1gFnSc4
na	na	k7c6
středním	střední	k2eAgInSc6d1
toku	tok	k1gInSc6
Amuru	Amur	k1gInSc2
<g/>
,	,	kIx,
Ajgun	Ajgun	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
zničili	zničit	k5eAaPmAgMnP
blízké	blízký	k2eAgFnPc4d1
ruské	ruský	k2eAgFnPc4d1
pevnůstky	pevnůstka	k1gFnPc4
na	na	k7c4
Zeje	zet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1685	#num#	k4
čchingská	čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
vytáhla	vytáhnout	k5eAaPmAgFnS
na	na	k7c4
Albazin	Albazin	k1gInSc4
a	a	k8xC
během	během	k7c2
několika	několik	k4yIc2
dní	den	k1gInPc2
ho	on	k3xPp3gMnSc4
dobyla	dobýt	k5eAaPmAgFnS
a	a	k8xC
zničila	zničit	k5eAaPmAgFnS
<g/>
,	,	kIx,
načež	načež	k6eAd1
se	se	k3xPyFc4
vrátila	vrátit	k5eAaPmAgFnS
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusové	k2eAgFnSc1d1
vzápětí	vzápětí	k6eAd1
Albazin	Albazin	k1gInSc4
obnovili	obnovit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čchingský	Čchingský	k1gMnSc1
císař	císař	k1gMnSc1
Kchang-si	Kchang-se	k1gFnSc4
nařídil	nařídit	k5eAaPmAgMnS
nový	nový	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1686	#num#	k4
čchingská	čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
dorazila	dorazit	k5eAaPmAgFnS
k	k	k7c3
Albazinu	Albazin	k1gInSc3
a	a	k8xC
zaútočila	zaútočit	k5eAaPmAgFnS
na	na	k7c4
něj	on	k3xPp3gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
byli	být	k5eAaImAgMnP
tentokrát	tentokrát	k6eAd1
mnohem	mnohem	k6eAd1
lépe	dobře	k6eAd2
připraveni	připravit	k5eAaPmNgMnP
a	a	k8xC
opevněni	opevnit	k5eAaPmNgMnP
a	a	k8xC
úporně	úporně	k6eAd1
se	se	k3xPyFc4
bránili	bránit	k5eAaImAgMnP
do	do	k7c2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
1687	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Kchang-si	Kchang-se	k1gFnSc4
nechal	nechat	k5eAaPmAgMnS
obléhání	obléhání	k1gNnSc3
ukončit	ukončit	k5eAaPmF
a	a	k8xC
dal	dát	k5eAaPmAgMnS
přednost	přednost	k1gFnSc4
diplomatickému	diplomatický	k2eAgNnSc3d1
jednání	jednání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
–	–	k?
pod	pod	k7c7
čchingským	čchingský	k2eAgInSc7d1
tlakem	tlak	k1gInSc7
–	–	k?
v	v	k7c6
letech	let	k1gInPc6
1685	#num#	k4
<g/>
–	–	k?
<g/>
1688	#num#	k4
na	na	k7c4
ruské	ruský	k2eAgMnPc4d1
osady	osada	k1gFnSc2
a	a	k8xC
pevnosti	pevnost	k1gFnSc2
v	v	k7c6
Zabajkalsku	Zabajkalsko	k1gNnSc6
útočili	útočit	k5eAaImAgMnP
Mongolové	Mongol	k1gMnPc1
<g/>
,	,	kIx,
než	než	k8xS
byli	být	k5eAaImAgMnP
roku	rok	k1gInSc2
1688	#num#	k4
poraženi	porazit	k5eAaPmNgMnP
Rusy	Rus	k1gMnPc4
u	u	k7c2
Seleginského	Seleginský	k2eAgInSc2d1
ostrohu	ostroh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
roku	rok	k1gInSc2
1686	#num#	k4
vyslala	vyslat	k5eAaPmAgFnS
Fjodora	Fjodora	k1gFnSc1
Golovina	Golovina	k1gFnSc1
k	k	k7c3
jednání	jednání	k1gNnSc3
o	o	k7c4
míru	míra	k1gFnSc4
<g/>
,	,	kIx,
se	s	k7c7
zástupci	zástupce	k1gMnPc7
říše	říš	k1gFnSc2
Čching	Čching	k1gInSc1
se	se	k3xPyFc4
setkal	setkat	k5eAaPmAgInS
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
v	v	k7c6
srpnu	srpen	k1gInSc6
1689	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jednání	jednání	k1gNnSc6
si	se	k3xPyFc3
Mandžuové	Mandžu	k1gMnPc1
nárokovali	nárokovat	k5eAaImAgMnP
země	zem	k1gFnSc2
až	až	k6eAd1
k	k	k7c3
Bajkalu	Bajkal	k1gInSc3
s	s	k7c7
odůvodněním	odůvodnění	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
kdysi	kdysi	k6eAd1
patřily	patřit	k5eAaImAgFnP
mongolské	mongolský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
Jüan	jüan	k1gInSc1
<g/>
;	;	kIx,
Rusové	Rus	k1gMnPc1
navrhovali	navrhovat	k5eAaImAgMnP
hranici	hranice	k1gFnSc4
na	na	k7c6
Amuru	Amur	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čchingští	Čchingský	k2eAgMnPc1d1
vyslanci	vyslanec	k1gMnPc1
využili	využít	k5eAaPmAgMnP
absolutní	absolutní	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
vojska	vojsko	k1gNnSc2
u	u	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
k	k	k7c3
nátlaku	nátlak	k1gInSc3
na	na	k7c4
ruské	ruský	k2eAgMnPc4d1
vyjednavače	vyjednavač	k1gMnPc4
a	a	k8xC
donutili	donutit	k5eAaPmAgMnP
je	on	k3xPp3gInPc4
přijmout	přijmout	k5eAaPmF
většinu	většina	k1gFnSc4
čchingských	čchingský	k2eAgInPc2d1
požadavků	požadavek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
Golovin	Golovina	k1gFnPc2
vzdal	vzdát	k5eAaPmAgMnS
Albazinu	Albazina	k1gFnSc4
a	a	k8xC
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
hranicí	hranice	k1gFnSc7
na	na	k7c6
horském	horský	k2eAgInSc6d1
hřbetu	hřbet	k1gInSc6
severně	severně	k6eAd1
od	od	k7c2
Amuru	Amur	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohoda	dohoda	k1gFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
i	i	k9
pravidla	pravidlo	k1gNnPc4
obchodních	obchodní	k2eAgInPc2d1
styků	styk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Poamuří	Poamuřit	k5eAaPmIp3nS,k5eAaImIp3nS
a	a	k8xC
Mandžusko	Mandžusko	k1gNnSc4
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Koncem	koncem	k7c2
16	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
začátkem	začátkem	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	stoletý	k2eAgMnPc1d1
Nurhači	Nurhač	k1gMnPc1
sjednotil	sjednotit	k5eAaPmAgInS
kmeny	kmen	k1gInPc4
ťienčouských	ťienčouský	k2eAgMnPc2d1
Džürčenů	Džürčen	k1gMnPc2
a	a	k8xC
roku	rok	k1gInSc2
1616	#num#	k4
se	se	k3xPyFc4
prohlásil	prohlásit	k5eAaPmAgMnS
za	za	k7c2
chána	chán	k1gMnSc2
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
Pozdní	pozdní	k2eAgFnSc2d1
<g/>
)	)	kIx)
Ťin	Ťin	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1636	#num#	k4
jeho	jeho	k3xOp3gFnSc4
nástupce	nástupce	k1gMnSc1
Chuang	Chuang	k1gMnSc1
Tchaj-ťi	Tchaj-ťe	k1gFnSc4
přejmenoval	přejmenovat	k5eAaPmAgMnS
říši	říše	k1gFnSc4
na	na	k7c4
Čching	Čching	k1gInSc4
a	a	k8xC
svůj	svůj	k3xOyFgInSc4
národ	národ	k1gInSc4
na	na	k7c4
Mandžuy	Mandžu	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Kmeny	kmen	k1gInPc1
Poamuří	Poamuří	k1gNnSc2
a	a	k8xC
Zabajkalska	Zabajkalsko	k1gNnSc2
v	v	k7c6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
mandžuská	mandžuský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Čching	Čching	k1gInSc4
kontrolovala	kontrolovat	k5eAaImAgFnS
jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
moderního	moderní	k2eAgNnSc2d1
Mandžuska	Mandžusko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
území	území	k1gNnPc1
na	na	k7c6
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
řeky	řeka	k1gFnSc2
Liao-che	Liao-ch	k1gFnSc2
<g/>
,	,	kIx,
poloostrov	poloostrov	k1gInSc4
Liao-tung	Liao-tunga	k1gFnPc2
a	a	k8xC
oblast	oblast	k1gFnSc4
horního	horní	k2eAgInSc2d1
toku	tok	k1gInSc2
řeky	řeka	k1gFnSc2
Sungari	Sungar	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Severněji	severně	k6eAd2
<g/>
,	,	kIx,
mezi	mezi	k7c7
Amurem	Amur	k1gInSc7
a	a	k8xC
čchingským	čchingský	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
ležel	ležet	k5eAaImAgInS
široký	široký	k2eAgInSc1d1
pás	pás	k1gInSc1
osídlený	osídlený	k2eAgInSc1d1
svobodnými	svobodný	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
–	–	k?
Evenky	Evenek	k1gMnPc7
(	(	kIx(
<g/>
Tunguzy	Tunguz	k1gMnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Daury	Daur	k1gMnPc7
<g/>
,	,	kIx,
Ďučery	Ďučer	k1gMnPc7
a	a	k8xC
dalšími	další	k2eAgInPc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
povodí	povodí	k1gNnSc6
Amuru	Amur	k1gInSc2
na	na	k7c6
Šilce	Šilka	k1gFnSc6
a	a	k8xC
Arguni	Argueň	k1gFnSc6
žily	žít	k5eAaImAgInP
tunguzské	tunguzský	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
podél	podél	k7c2
Amuru	Amur	k1gInSc2
po	po	k7c4
Zeju	zet	k5eAaImIp1nS
mongolojazyční	mongolojazyční	k2eAgFnSc1d1
Daurové	Daurová	k1gFnSc3
a	a	k8xC
podauření	podauření	k1gNnSc3
Evenkové	Evenkové	k2eAgNnSc3d1
<g/>
,	,	kIx,
níže	nízce	k6eAd2
po	po	k7c6
proudu	proud	k1gInSc6
Amuru	Amur	k1gInSc2
tunguzští	tunguzský	k2eAgMnPc1d1
Ďučeři	Ďučer	k1gMnPc1
<g/>
,	,	kIx,
na	na	k7c6
dolním	dolní	k2eAgInSc6d1
Amuru	Amur	k1gInSc6
Natci	Natce	k1gMnPc1
a	a	k8xC
Ačani	Ačan	k1gMnPc1
(	(	kIx(
<g/>
předkové	předek	k1gMnPc1
Nanajců	Nanajce	k1gMnPc2
a	a	k8xC
Ulčů	Ulč	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
ústí	ústí	k1gNnSc2
Amuru	Amur	k1gInSc2
a	a	k8xC
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Giljaci	Giljak	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severních	severní	k2eAgInPc6d1
přítocích	přítok	k1gInPc6
Amuru	Amur	k1gInSc2
až	až	k6eAd1
k	k	k7c3
Ochotskému	ochotský	k2eAgNnSc3d1
moři	moře	k1gNnSc3
žili	žít	k5eAaImAgMnP
Evenkové	Evenek	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Amurské	amurský	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
vesměs	vesměs	k6eAd1
žily	žít	k5eAaImAgInP
z	z	k7c2
lovu	lov	k1gInSc2
a	a	k8xC
rybolovu	rybolov	k1gInSc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
Dauři	Daur	k1gMnPc1
a	a	k8xC
Ďučeři	Ďučer	k1gMnPc1
byli	být	k5eAaImAgMnP
zemědělci	zemědělec	k1gMnPc1
pod	pod	k7c7
vládou	vláda	k1gFnSc7
knížat	kníže	k1gNnPc2
sídlících	sídlící	k2eAgNnPc2d1
v	v	k7c6
opevněných	opevněný	k2eAgNnPc6d1
městečkách	městečko	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
těmto	tento	k3xDgInPc3
kmenům	kmen	k1gInPc3
<g/>
,	,	kIx,
v	v	k7c6
Číně	Čína	k1gFnSc6
nazývaným	nazývaný	k2eAgInSc7d1
„	„	k?
<g/>
Divocí	divoký	k2eAgMnPc1d1
Džürčeni	Džürčen	k2eAgMnPc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
podnikli	podniknout	k5eAaPmAgMnP
Nurhači	Nurhač	k1gMnPc1
a	a	k8xC
Chuang	Chuang	k1gInSc1
Tchaj-ťi	Tchaj-ť	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
1613	#num#	k4
řadu	řada	k1gFnSc4
nájezdů	nájezd	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
nicméně	nicméně	k8xC
nepočítali	počítat	k5eNaImAgMnP
severní	severní	k2eAgInPc4d1
kraje	kraj	k1gInPc4
mezi	mezi	k7c4
své	svůj	k3xOyFgFnPc4
země	zem	k1gFnPc4
a	a	k8xC
tamní	tamní	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
mezi	mezi	k7c4
poddané	poddaná	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
neobsadili	obsadit	k5eNaPmAgMnP
jejich	jejich	k3xOp3gFnSc4
zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
při	při	k7c6
nájezdech	nájezd	k1gInPc6
získávali	získávat	k5eAaImAgMnP
zajatce	zajatec	k1gMnSc4
a	a	k8xC
vyžadovali	vyžadovat	k5eAaImAgMnP
přinášení	přinášení	k1gNnSc4
tributu	tribut	k1gInSc2
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
tím	ten	k3xDgNnSc7
uznání	uznání	k1gNnSc4
vazalské	vazalský	k2eAgFnSc2d1
závislosti	závislost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
čchingské	čchingský	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
v	v	k7c6
Poamuří	Poamuří	k1gNnSc6
bylo	být	k5eAaImAgNnS
vytvoření	vytvoření	k1gNnSc1
pásu	pás	k1gInSc2
řídce	řídce	k6eAd1
osídlených	osídlený	k2eAgNnPc2d1
závislých	závislý	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
by	by	kYmCp3nS
zabezpečil	zabezpečit	k5eAaPmAgInS
říši	říše	k1gFnSc4
ze	z	k7c2
severního	severní	k2eAgInSc2d1
směru	směr	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
zahájena	zahájit	k5eAaPmNgFnS
expanze	expanze	k1gFnSc1
na	na	k7c4
jih	jih	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátkem	počátkem	k7c2
40	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Rusové	Rusová	k1gFnSc2
během	během	k7c2
průzkumu	průzkum	k1gInSc2
a	a	k8xC
zabírání	zabírání	k1gNnSc2
Sibiře	Sibiř	k1gFnSc2
a	a	k8xC
Dálného	dálný	k2eAgInSc2d1
východu	východ	k1gInSc2
dospěli	dochvít	k5eAaPmAgMnP
do	do	k7c2
povodí	povodí	k1gNnSc2
Amuru	Amur	k1gInSc2
<g/>
,	,	kIx,
potenciálně	potenciálně	k6eAd1
významnému	významný	k2eAgInSc3d1
svým	svůj	k3xOyFgInSc7
zemědělským	zemědělský	k2eAgInSc7d1
potenciálem	potenciál	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1643	#num#	k4
<g/>
–	–	k?
<g/>
1646	#num#	k4
horní	horní	k2eAgInSc4d1
a	a	k8xC
střední	střední	k2eAgInSc4d1
tok	tok	k1gInSc4
Amuru	Amur	k1gInSc2
prozkoumala	prozkoumat	k5eAaPmAgFnS
expedice	expedice	k1gFnSc1
Vasilije	Vasilije	k1gMnSc2
Pojarkova	Pojarkův	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poté	poté	k6eAd1
Rusové	Rus	k1gMnPc1
začali	začít	k5eAaPmAgMnP
zabírat	zabírat	k5eAaImF
povodí	povodí	k1gNnSc4
řeky	řeka	k1gFnSc2
<g/>
:	:	kIx,
expedice	expedice	k1gFnSc1
Jerofeje	Jerofej	k1gMnSc2
Chabarova	Chabarův	k2eAgInSc2d1
si	se	k3xPyFc3
podřídila	podřídit	k5eAaPmAgFnS
Daury	Daura	k1gFnPc4
na	na	k7c6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
Amuru	Amur	k1gInSc2
a	a	k8xC
zimu	zima	k1gFnSc4
1650	#num#	k4
<g/>
/	/	kIx~
<g/>
1651	#num#	k4
strávila	strávit	k5eAaPmAgFnS
v	v	k7c6
městečku	městečko	k1gNnSc6
daurského	daurský	k2eAgMnSc2d1
náčelníka	náčelník	k1gMnSc2
Albazy	Albaz	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
Následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
postupovala	postupovat	k5eAaImAgFnS
po	po	k7c6
Amuru	Amur	k1gInSc6
a	a	k8xC
silou	síla	k1gFnSc7
si	se	k3xPyFc3
podrobovala	podrobovat	k5eAaImAgFnS
místní	místní	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
na	na	k7c4
podzim	podzim	k1gInSc4
se	se	k3xPyFc4
usadila	usadit	k5eAaPmAgFnS
v	v	k7c6
Ačansku	Ačansko	k1gNnSc6
(	(	kIx(
<g/>
v	v	k7c6
okolí	okolí	k1gNnSc6
moderního	moderní	k2eAgInSc2d1
Chabarovsku	Chabarovsk	k1gInSc2
<g/>
)	)	kIx)
k	k	k7c3
přezimování	přezimování	k1gNnSc3
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vzápětí	vzápětí	k6eAd1
odrazila	odrazit	k5eAaPmAgFnS
útok	útok	k1gInSc4
Nanajců	Nanajce	k1gMnPc2
a	a	k8xC
Ďučerů	Ďučer	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Postup	postup	k1gInSc1
Rusů	Rus	k1gMnPc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
paniky	panika	k1gFnSc2
u	u	k7c2
místních	místní	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
vzbudil	vzbudit	k5eAaPmAgInS
nevoli	nevole	k1gFnSc4
u	u	k7c2
vlády	vláda	k1gFnSc2
mandžuské	mandžuský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
Čching	Čching	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
domorodce	domorodec	k1gMnPc4
totiž	totiž	k9
Rusové	Rus	k1gMnPc1
představovali	představovat	k5eAaImAgMnP
alternativu	alternativa	k1gFnSc4
k	k	k7c3
mandžuské	mandžuský	k2eAgFnSc3d1
nadvládě	nadvláda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
po	po	k7c6
roce	rok	k1gInSc6
1644	#num#	k4
čchingská	čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
–	–	k?
vojsko	vojsko	k1gNnSc1
osmi	osm	k4xCc2
korouhví	korouhev	k1gFnPc2
–	–	k?
odešla	odejít	k5eAaPmAgFnS
do	do	k7c2
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
Mandžusku	Mandžusko	k1gNnSc6
zůstaly	zůstat	k5eAaPmAgFnP
jen	jen	k9
malé	malý	k2eAgFnPc1d1
síly	síla	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čchingská	Čchingský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
proto	proto	k8xC
obávala	obávat	k5eAaImAgFnS
o	o	k7c4
bezpečnost	bezpečnost	k1gFnSc4
své	svůj	k3xOyFgFnPc4
severní	severní	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c2
této	tento	k3xDgFnSc2
situace	situace	k1gFnSc2
se	se	k3xPyFc4
Mandžuové	Mandžu	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
vyhnat	vyhnat	k5eAaPmF
Rusy	Rus	k1gMnPc4
z	z	k7c2
Poamuří	Poamuří	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
roku	rok	k1gInSc2
1652	#num#	k4
zahájili	zahájit	k5eAaPmAgMnP
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rusko-čchingské	Rusko-čchingské	k1gNnSc1
boje	boj	k1gInSc2
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
</s>
<s>
Bitva	bitva	k1gFnSc1
v	v	k7c6
Ačansku	Ačansko	k1gNnSc6
(	(	kIx(
<g/>
1652	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ačanského	Ačanský	k2eAgInSc2d1
ostrohu	ostroh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ukázka	ukázka	k1gFnSc1
opevnění	opevnění	k1gNnSc2
ruských	ruský	k2eAgInPc2d1
ostrohů	ostroh	k1gInPc2
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
ilimského	ilimský	k2eAgInSc2d1
ostrohu	ostroh	k1gInSc2
s	s	k7c7
věží	věž	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1667	#num#	k4
<g/>
,	,	kIx,
přemístěná	přemístěný	k2eAgFnSc1d1
do	do	k7c2
muzea	muzeum	k1gNnSc2
Talcy	Talca	k1gFnSc2
u	u	k7c2
Irkutska	Irkutsk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Proti	proti	k7c3
ruským	ruský	k2eAgMnPc3d1
kozákům	kozák	k1gMnPc3
čchingské	čchingské	k1gNnSc4
úřady	úřad	k1gInPc1
vypravily	vypravit	k5eAaPmAgInP
z	z	k7c2
Ninguty	Ningut	k1gInPc4
2000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
koncem	koncem	k7c2
února	únor	k1gInSc2
1652	#num#	k4
dorazili	dorazit	k5eAaPmAgMnP
do	do	k7c2
Ačansku	Ačansko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
zaskočit	zaskočit	k5eAaPmF
Rusy	Rus	k1gMnPc4
překvapivým	překvapivý	k2eAgInSc7d1
útokem	útok	k1gInSc7
<g/>
,	,	kIx,
během	během	k7c2
něhož	jenž	k3xRgInSc2
dělostřelbou	dělostřelba	k1gFnSc7
poničili	poničit	k5eAaPmAgMnP
ruské	ruský	k2eAgNnSc4d1
opevnění	opevnění	k1gNnSc4
a	a	k8xC
průlomem	průlom	k1gInSc7
v	v	k7c6
palisádě	palisáda	k1gFnSc6
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
vtrhnout	vtrhnout	k5eAaPmF
do	do	k7c2
pevnosti	pevnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průlomu	průlom	k1gInSc6
je	být	k5eAaImIp3nS
však	však	k9
kozáci	kozák	k1gMnPc1
rozstříleli	rozstřílet	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pozdější	pozdní	k2eAgFnSc2d2
zprávy	zpráva	k1gFnSc2
Chabarova	Chabarův	k2eAgFnSc1d1
v	v	k7c6
bitvě	bitva	k1gFnSc6
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
téměř	téměř	k6eAd1
sedm	sedm	k4xCc1
set	sto	k4xCgNnPc2
útočníků	útočník	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
deset	deset	k4xCc1
Rusů	Rus	k1gMnPc2
<g/>
,	,	kIx,
dalších	další	k2eAgMnPc2d1
78	#num#	k4
Rusů	Rus	k1gMnPc2
bylo	být	k5eAaImAgNnS
zraněno	zranit	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přestože	přestože	k8xS
zvítězili	zvítězit	k5eAaPmAgMnP
<g/>
,	,	kIx,
Rusové	Rus	k1gMnPc1
chápali	chápat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
střetli	střetnout	k5eAaPmAgMnP
s	s	k7c7
mnohem	mnohem	k6eAd1
nebezpečnějším	bezpečný	k2eNgMnSc7d2
protivníkem	protivník	k1gMnSc7
<g/>
,	,	kIx,
než	než	k8xS
byly	být	k5eAaImAgInP
místní	místní	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
1652	#num#	k4
opustili	opustit	k5eAaPmAgMnP
Ačanský	Ačanský	k2eAgInSc4d1
ostroh	ostroh	k1gInSc4
a	a	k8xC
odpluli	odplout	k5eAaPmAgMnP
vzhůru	vzhůru	k6eAd1
po	po	k7c6
Amuru	Amur	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Čchingská	Čchingský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
také	také	k6eAd1
považovala	považovat	k5eAaImAgFnS
nového	nový	k2eAgMnSc4d1
protivníka	protivník	k1gMnSc4
za	za	k7c4
nebezpečného	bezpečný	k2eNgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitelé	velitel	k1gMnPc1
poraženého	poražený	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
byli	být	k5eAaImAgMnP
odvoláni	odvolán	k2eAgMnPc1d1
a	a	k8xC
potrestáni	potrestán	k2eAgMnPc1d1
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
velení	velení	k1gNnSc2
v	v	k7c6
Ningutě	Ningutě	k1gFnSc6
převzal	převzít	k5eAaPmAgMnS
zkušený	zkušený	k2eAgMnSc1d1
mandžuský	mandžuský	k2eAgMnSc1d1
vojevůdce	vojevůdce	k1gMnSc1
Šarhuda	Šarhuda	k1gMnSc1
<g/>
,	,	kIx,
pocházející	pocházející	k2eAgMnSc1d1
z	z	k7c2
tamního	tamní	k2eAgInSc2d1
regionu	region	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
začal	začít	k5eAaPmAgInS
systematickou	systematický	k2eAgFnSc4d1
přípravu	příprava	k1gFnSc4
na	na	k7c4
boj	boj	k1gInSc4
s	s	k7c7
Rusy	Rus	k1gMnPc7
zahrnující	zahrnující	k2eAgFnSc2d1
organizaci	organizace	k1gFnSc4
vojska	vojsko	k1gNnSc2
z	z	k7c2
místních	místní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
výstavbu	výstavba	k1gFnSc4
říčního	říční	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
a	a	k8xC
přípravu	příprava	k1gFnSc4
zásob	zásoba	k1gFnPc2
potravin	potravina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
na	na	k7c6
Sungari	Sungari	k1gNnSc6
(	(	kIx(
<g/>
1654	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Bitva	bitva	k1gFnSc1
na	na	k7c6
Sungari	Sungar	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Akce	akce	k1gFnSc1
Rusů	Rus	k1gMnPc2
(	(	kIx(
<g/>
červeně	červeň	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Čchingů	Čching	k1gInPc2
(	(	kIx(
<g/>
modře	modro	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
pohyb	pohyb	k1gInSc1
amurských	amurský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
na	na	k7c4
jih	jih	k1gInSc4
(	(	kIx(
<g/>
zeleně	zeleně	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
Zabajkalsku	Zabajkalsko	k1gNnSc6
a	a	k8xC
Poamuří	Poamuří	k1gNnSc6
v	v	k7c6
letech	let	k1gInPc6
1652	#num#	k4
<g/>
–	–	k?
<g/>
1658	#num#	k4
</s>
<s>
Čchingské	Čchingský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
chtěly	chtít	k5eAaImAgInP
Rusy	Rus	k1gMnPc4
zbavit	zbavit	k5eAaPmF
zdrojů	zdroj	k1gInPc2
potravin	potravina	k1gFnPc2
vysídlením	vysídlení	k1gNnSc7
Daurů	Daur	k1gMnPc2
z	z	k7c2
horního	horní	k2eAgNnSc2d1
Poamuří	Poamuří	k1gNnSc2
na	na	k7c4
jih	jih	k1gInSc4
na	na	k7c4
řeku	řeka	k1gFnSc4
Sungari	Sungar	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
roku	rok	k1gInSc2
1656	#num#	k4
přesídlily	přesídlit	k5eAaPmAgFnP
na	na	k7c4
jih	jih	k1gInSc4
i	i	k8xC
Ďučery	Ďučera	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesídlování	přesídlování	k1gNnPc2
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
kvůli	kvůli	k7c3
odporu	odpor	k1gInSc3
části	část	k1gFnSc2
domorodců	domorodec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
dali	dát	k5eAaPmAgMnP
přednost	přednost	k1gFnSc4
poddanství	poddanství	k1gNnPc2
Rusům	Rus	k1gMnPc3
<g/>
,	,	kIx,
nepodařilo	podařit	k5eNaPmAgNnS
úplně	úplně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
jaře	jaro	k1gNnSc6
1654	#num#	k4
Čchingové	Čchingový	k2eAgFnPc1d1
vypravili	vypravit	k5eAaPmAgMnP
další	další	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
v	v	k7c6
dubnu	duben	k1gInSc6
1654	#num#	k4
na	na	k7c6
řece	řeka	k1gFnSc6
Sungari	Sungari	k1gNnSc2
napadla	napadnout	k5eAaPmAgFnS
oddíl	oddíl	k1gInSc4
kozáků	kozák	k1gInPc2
vedený	vedený	k2eAgInSc4d1
Onufrijem	Onufrij	k1gMnSc7
Stěpanovem	Stěpanov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stěpanovových	Stěpanovových	k2eAgFnSc1d1
370	#num#	k4
kozáků	kozák	k1gInPc2
využilo	využít	k5eAaPmAgNnS
převahy	převaha	k1gFnSc2
svých	svůj	k3xOyFgFnPc2
větších	veliký	k2eAgFnPc2d2
lodí	loď	k1gFnPc2
a	a	k8xC
palebné	palebný	k2eAgFnPc1d1
síly	síla	k1gFnPc1
a	a	k8xC
zahnalo	zahnat	k5eAaPmAgNnS
1000	#num#	k4
mužů	muž	k1gMnPc2
nepřítele	nepřítel	k1gMnSc4
z	z	k7c2
lodí	loď	k1gFnPc2
na	na	k7c4
břeh	břeh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
však	však	k9
Rusové	Rus	k1gMnPc1
narazili	narazit	k5eAaPmAgMnP
na	na	k7c4
stovku	stovka	k1gFnSc4
elitních	elitní	k2eAgMnPc2d1
korejských	korejský	k2eAgMnPc2d1
mušketýrů	mušketýr	k1gMnPc2
podpořenou	podpořený	k2eAgFnSc4d1
300	#num#	k4
Daury	Daura	k1gFnSc2
a	a	k8xC
300	#num#	k4
Mandžuy	Mandžu	k1gMnPc7
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
zakopané	zakopaný	k2eAgFnPc1d1
na	na	k7c6
blízkém	blízký	k2eAgInSc6d1
vršku	vršek	k1gInSc6
<g/>
,	,	kIx,
kontrolujícím	kontrolující	k2eAgInSc6d1
soutok	soutok	k1gInSc1
řek	řeka	k1gFnPc2
Sungari	Sungar	k1gFnSc2
a	a	k8xC
Mudan	Mudana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čchingské	čchingský	k2eAgFnSc6d1
střelbě	střelba	k1gFnSc6
se	se	k3xPyFc4
ruské	ruský	k2eAgInPc1d1
útoky	útok	k1gInPc1
zhroutily	zhroutit	k5eAaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Ustupující	ustupující	k2eAgInPc1d1
ruské	ruský	k2eAgInPc1d1
kozáky	kozák	k1gInPc1
pak	pak	k6eAd1
čchingské	čchingská	k1gFnSc2
vojsko	vojsko	k1gNnSc1
pronásledovalo	pronásledovat	k5eAaImAgNnS
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
důsledku	důsledek	k1gInSc6
porážky	porážka	k1gFnSc2
Rusové	Rus	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
přístup	přístup	k1gInSc4
na	na	k7c4
Sungari	Sungare	k1gFnSc4
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
ocitli	ocitnout	k5eAaPmAgMnP
pod	pod	k7c7
tlakem	tlak	k1gInSc7
nedostatku	nedostatek	k1gInSc2
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
řečeno	říct	k5eAaPmNgNnS
jejich	jejich	k3xOp3gNnSc7
slovy	slovo	k1gNnPc7
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Ze	z	k7c2
Šingaly	Šingala	k1gFnSc2
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
nás	my	k3xPp1nPc2
<g/>
,	,	kIx,
sluhů	sluha	k1gMnPc2
tvých	tvůj	k3xOp2gInPc2
<g/>
,	,	kIx,
bogdojští	bogdojský	k2eAgMnPc1d1
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
4	#num#	k4
<g/>
]	]	kIx)
vojenští	vojenský	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
vyhnali	vyhnat	k5eAaPmAgMnP
a	a	k8xC
obilí	obilí	k1gNnSc4
nedali	dát	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
my	my	k3xPp1nPc1
<g/>
,	,	kIx,
sluhové	sluhová	k1gFnSc2
tví	tvůj	k3xOp2gMnPc1
<g/>
,	,	kIx,
šli	jít	k5eAaImAgMnP
jsme	být	k5eAaImIp1nP
z	z	k7c2
ústí	ústí	k1gNnSc2
Šingaly	Šingala	k1gFnSc2
vzhůru	vzhůru	k6eAd1
po	po	k7c6
Amuru	Amur	k1gInSc6
a	a	k8xC
měli	mít	k5eAaImAgMnP
velikou	veliký	k2eAgFnSc4d1
nouzi	nouze	k1gFnSc4
bez	bez	k7c2
obilí	obilí	k1gNnSc2
a	a	k8xC
propříště	propříště	k6eAd1
my	my	k3xPp1nPc1
<g/>
,	,	kIx,
sluhové	sluhová	k1gFnSc2
tví	tvůj	k3xOp2gMnPc1
<g/>
,	,	kIx,
nevíme	vědět	k5eNaImIp1nP
kde	kde	k6eAd1
se	se	k3xPyFc4
uživit	uživit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
“	“	k?
</s>
<s>
Obléhání	obléhání	k1gNnSc1
Kumarsku	Kumarsek	k1gInSc2
(	(	kIx(
<g/>
1655	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Obléhání	obléhání	k1gNnSc2
Kumarského	Kumarský	k2eAgInSc2d1
ostrohu	ostroh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
bitvě	bitva	k1gFnSc6
na	na	k7c6
Sungari	Sungar	k1gInSc6
se	se	k3xPyFc4
čchingské	čchingský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
vrátilo	vrátit	k5eAaPmAgNnS
do	do	k7c2
Ninguty	Ningut	k2eAgInPc4d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Stěpanov	Stěpanov	k1gInSc1
se	se	k3xPyFc4
svými	svůj	k3xOyFgMnPc7
muži	muž	k1gMnPc7
přezimoval	přezimovat	k5eAaBmAgMnS
v	v	k7c4
Kumarsku	Kumarska	k1gFnSc4
na	na	k7c6
středním	střední	k2eAgInSc6d1
toku	tok	k1gInSc6
Amuru	Amur	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Připojilo	připojit	k5eAaPmAgNnS
se	se	k3xPyFc4
k	k	k7c3
němu	on	k3xPp3gInSc3
několik	několik	k4yIc1
menších	malý	k2eAgFnPc2d2
ruských	ruský	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
přišlých	přišlý	k2eAgFnPc2d1
ze	z	k7c2
Zabajkalska	Zabajkalsko	k1gNnSc2
a	a	k8xC
Sibiře	Sibiř	k1gFnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
měl	mít	k5eAaImAgMnS
kolem	kolem	k7c2
pěti	pět	k4xCc2
<g />
.	.	kIx.
</s>
<s hack="1">
stovek	stovka	k1gFnPc2
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Kvůli	kvůli	k7c3
obavám	obava	k1gFnPc3
z	z	k7c2
čchingské	čchingský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
se	se	k3xPyFc4
Rusové	Rus	k1gMnPc1
snažili	snažit	k5eAaImAgMnP
Kumarsk	Kumarsk	k1gInSc4
co	co	k9
nejlépe	dobře	k6eAd3
opevnit	opevnit	k5eAaPmF
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
namísto	namísto	k7c2
běžné	běžný	k2eAgFnSc2d1
palisády	palisáda	k1gFnSc2
nasypali	nasypat	k5eAaPmAgMnP
za	za	k7c7
širokým	široký	k2eAgInSc7d1
příkopem	příkop	k1gInSc7
zemní	zemní	k2eAgInSc1d1
val	val	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgMnSc6
postavili	postavit	k5eAaPmAgMnP
dvojitou	dvojitý	k2eAgFnSc4d1
palisádu	palisáda	k1gFnSc4
vyplněnou	vyplněný	k2eAgFnSc4d1
hlínou	hlína	k1gFnSc7
a	a	k8xC
kamením	kamení	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zatímco	zatímco	k8xS
Šarhuda	Šarhuda	k1gFnSc1
s	s	k7c7
novými	nový	k2eAgInPc7d1
útoky	útok	k1gInPc7
na	na	k7c4
Rusy	Rus	k1gMnPc4
nespěchal	spěchat	k5eNaImAgMnS
a	a	k8xC
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
se	se	k3xPyFc4
vysidlování	vysidlování	k1gNnSc4
domorodých	domorodý	k2eAgMnPc2d1
Daurů	Daur	k1gMnPc2
<g/>
,	,	kIx,
Ďučerů	Ďučer	k1gMnPc2
a	a	k8xC
Gogulů	Gogul	k1gMnPc2
z	z	k7c2
Poamuří	Poamuří	k1gNnSc2
na	na	k7c4
Non	Non	k1gFnSc4
a	a	k8xC
horní	horní	k2eAgInSc4d1
tok	tok	k1gInSc4
Sungari	Sungar	k1gFnSc2
<g/>
,	,	kIx,
čchingský	čchingský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
Minggadari	Minggadar	k1gFnSc2
<g/>
,	,	kIx,
poslaný	poslaný	k2eAgInSc1d1
z	z	k7c2
Pekingu	Peking	k1gInSc2
do	do	k7c2
Ninguty	Ningut	k2eAgFnPc4d1
s	s	k7c7
oddílem	oddíl	k1gInSc7
korouhevních	korouhevní	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
na	na	k7c4
Rusy	Rus	k1gMnPc4
zaútočit	zaútočit	k5eAaPmF
ještě	ještě	k9
před	před	k7c7
koncem	konec	k1gInSc7
zimy	zima	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Měl	mít	k5eAaImAgInS
asi	asi	k9
tisíc	tisíc	k4xCgInPc2
mandžuských	mandžuský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
s	s	k7c7
oddíly	oddíl	k1gInPc7
domorodců	domorodec	k1gMnPc2
početnost	početnost	k1gFnSc1
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
vojska	vojsko	k1gNnSc2
Rusové	Rusová	k1gFnSc2
odhadovali	odhadovat	k5eAaImAgMnP
na	na	k7c4
celkem	celkem	k6eAd1
deset	deset	k4xCc4
tisíc	tisíc	k4xCgInPc2
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandžuové	Mandžu	k1gMnPc1
měli	mít	k5eAaImAgMnP
15	#num#	k4
děl	dělo	k1gNnPc2
a	a	k8xC
široký	široký	k2eAgInSc1d1
výběr	výběr	k1gInSc1
obléhacích	obléhací	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
únoru	únor	k1gInSc6
1655	#num#	k4
oblehli	oblehnout	k5eAaPmAgMnP
Kumarsk	Kumarsk	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
obléhání	obléhání	k1gNnSc1
bylo	být	k5eAaImAgNnS
neúspěšné	úspěšný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
odolali	odolat	k5eAaPmAgMnP
čchingským	čchingský	k2eAgMnPc3d1
útokům	útok	k1gInPc3
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
jim	on	k3xPp3gFnPc3
začaly	začít	k5eAaPmAgFnP
docházet	docházet	k5eAaImF
střelivo	střelivo	k1gNnSc4
a	a	k8xC
potraviny	potravina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Nedostatkem	nedostatek	k1gInSc7
zásob	zásoba	k1gFnPc2
ale	ale	k8xC
trpěli	trpět	k5eAaImAgMnP
i	i	k9
obléhatelé	obléhatel	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
čchingská	čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
po	po	k7c6
třech	tři	k4xCgInPc6
týdnech	týden	k1gInPc6
obležení	obležení	k1gNnSc2
stáhla	stáhnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
v	v	k7c6
Korčejevské	Korčejevský	k2eAgFnSc6d1
zátoce	zátoka	k1gFnSc6
(	(	kIx(
<g/>
1658	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Bitva	bitva	k1gFnSc1
v	v	k7c6
Korčejevské	Korčejevský	k2eAgFnSc6d1
zátoce	zátoka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1657	#num#	k4
založili	založit	k5eAaPmAgMnP
Mandžuové	Mandžu	k1gMnPc1
na	na	k7c6
místě	místo	k1gNnSc6
dnešního	dnešní	k2eAgInSc2d1
Girinu	Girin	k1gInSc2
loděnice	loděnice	k1gFnSc2
a	a	k8xC
znovu	znovu	k6eAd1
přikázali	přikázat	k5eAaPmAgMnP
Korejcům	Korejec	k1gMnPc3
přivést	přivést	k5eAaPmF
posily	posila	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
vyrazila	vyrazit	k5eAaPmAgFnS
do	do	k7c2
útoku	útok	k1gInSc2
čchingská	čchingskat	k5eAaPmIp3nS
armáda	armáda	k1gFnSc1
o	o	k7c6
síle	síla	k1gFnSc6
cca	cca	kA
1400	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
stovky	stovka	k1gFnSc2
dělostřelců	dělostřelec	k1gMnPc2
s	s	k7c7
padesáti	padesát	k4xCc7
kanóny	kanón	k1gInPc7
<g/>
,	,	kIx,
dvou	dva	k4xCgNnPc2
set	sto	k4xCgNnPc2
korejských	korejský	k2eAgMnPc2d1
mušketýrů	mušketýr	k1gMnPc2
a	a	k8xC
stovky	stovka	k1gFnSc2
střelců	střelec	k1gMnPc2
mandžuských	mandžuský	k2eAgInPc2d1
<g/>
,	,	kIx,
vycvičených	vycvičený	k2eAgInPc2d1
mnohem	mnohem	k6eAd1
hůře	zle	k6eAd2
než	než	k8xS
Korejci	Korejec	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Začátkem	začátkem	k7c2
června	červen	k1gInSc2
se	se	k3xPyFc4
na	na	k7c6
Amuru	Amur	k1gInSc6
<g/>
,	,	kIx,
nedaleko	daleko	k6eNd1
od	od	k7c2
ústí	ústí	k1gNnSc2
Sungari	Sungar	k1gFnSc2
<g/>
,	,	kIx,
střetla	střetnout	k5eAaPmAgFnS
s	s	k7c7
cca	cca	kA
pěti	pět	k4xCc3
sty	sto	k4xCgNnPc7
kozáky	kozák	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
roku	rok	k1gInSc3
1654	#num#	k4
měli	mít	k5eAaImAgMnP
Mandžuové	Mandžu	k1gMnPc1
mohutnější	mohutný	k2eAgFnSc2d2
lodě	loď	k1gFnSc2
a	a	k8xC
početné	početný	k2eAgNnSc1d1
dělostřelectvo	dělostřelectvo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Rusové	Rus	k1gMnPc1
se	se	k3xPyFc4
před	před	k7c7
přesilou	přesila	k1gFnSc7
pokusili	pokusit	k5eAaPmAgMnP
ustoupit	ustoupit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
nebyli	být	k5eNaImAgMnP
schopni	schopen	k2eAgMnPc1d1
setřást	setřást	k5eAaPmF
rychlejší	rychlý	k2eAgFnPc4d2
čchingské	čchingský	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
a	a	k8xC
asi	asi	k9
po	po	k7c4
10	#num#	k4
km	km	kA
pronásledování	pronásledování	k1gNnSc1
Stěpanov	Stěpanov	k1gInSc1
soustředil	soustředit	k5eAaPmAgInS
své	svůj	k3xOyFgFnPc4
lodě	loď	k1gFnPc4
do	do	k7c2
obranné	obranný	k2eAgFnSc2d1
linie	linie	k1gFnSc2
v	v	k7c6
Korčejevské	Korčejevský	k2eAgFnSc6d1
zátoce	zátoka	k1gFnSc6
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Amuru	Amur	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čchingové	Čchingový	k2eAgFnPc4d1
obklopili	obklopit	k5eAaPmAgMnP
ruské	ruský	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
a	a	k8xC
rozvinula	rozvinout	k5eAaPmAgFnS
se	se	k3xPyFc4
prudká	prudký	k2eAgFnSc1d1
přestřelka	přestřelka	k1gFnSc1
s	s	k7c7
použitím	použití	k1gNnSc7
děl	dělo	k1gNnPc2
a	a	k8xC
ručnic	ručnice	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
Rusové	Rus	k1gMnPc1
podlehli	podlehnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děl	dít	k5eAaBmAgMnS,k5eAaImAgMnS
totiž	totiž	k9
měli	mít	k5eAaImAgMnP
mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
<g/>
,	,	kIx,
a	a	k8xC
do	do	k7c2
ručnic	ručnice	k1gFnPc2
měli	mít	k5eAaImAgMnP
málo	málo	k1gNnSc4
střeliva	střelivo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Sedm	sedm	k4xCc1
z	z	k7c2
jedenácti	jedenáct	k4xCc2
ruských	ruský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
čchingští	čchingský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
zapálili	zapálit	k5eAaPmAgMnP
<g/>
,	,	kIx,
tři	tři	k4xCgMnPc1
ukořistili	ukořistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
na	na	k7c4
zbylé	zbylý	k2eAgInPc4d1
několik	několik	k4yIc1
desítek	desítka	k1gFnPc2
Rusů	Rus	k1gMnPc2
uprchlo	uprchnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
další	další	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
se	se	k3xPyFc4
skryla	skrýt	k5eAaPmAgFnS
v	v	k7c6
lesích	les	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
boji	boj	k1gInSc6
podle	podle	k7c2
různých	různý	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
zahynulo	zahynout	k5eAaPmAgNnS
209	#num#	k4
až	až	k8xS
270	#num#	k4
kozáků	kozák	k1gMnPc2
včetně	včetně	k7c2
jejich	jejich	k3xOp3gMnSc2
velitele	velitel	k1gMnSc2
Stěpanova	Stěpanův	k2eAgFnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
110	#num#	k4
čchingských	čchingský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
(	(	kIx(
<g/>
v	v	k7c6
tom	ten	k3xDgMnSc6
osm	osm	k4xCc1
Korejců	Korejec	k1gMnPc2
<g/>
)	)	kIx)
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čchingská	Čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
po	po	k7c6
bitvě	bitva	k1gFnSc6
vrátila	vrátit	k5eAaPmAgFnS
do	do	k7c2
Ninguty	Ningut	k1gInPc7
<g/>
,	,	kIx,
přeživší	přeživší	k2eAgMnPc1d1
kozáci	kozák	k1gMnPc1
se	se	k3xPyFc4
stáhli	stáhnout	k5eAaPmAgMnP
na	na	k7c4
dolní	dolní	k2eAgInSc4d1
tok	tok	k1gInSc4
Amuru	Amur	k1gInSc2
a	a	k8xC
následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
odešli	odejít	k5eAaPmAgMnP
na	na	k7c4
sever	sever	k1gInSc4
k	k	k7c3
Jakutsku	Jakutsk	k1gInSc3
a	a	k8xC
na	na	k7c4
severozápad	severozápad	k1gInSc4
k	k	k7c3
Ilimsku	Ilimsko	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
amurském	amurský	k2eAgInSc6d1
regionu	region	k1gInSc6
pak	pak	k6eAd1
na	na	k7c4
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
nezůstala	zůstat	k5eNaPmAgFnS
žádná	žádný	k3yNgFnSc1
organizovaná	organizovaný	k2eAgFnSc1d1
síla	síla	k1gFnSc1
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
nikdo	nikdo	k3yNnSc1
ho	on	k3xPp3gMnSc4
nekontroloval	nekontrolovat	k5eAaImAgMnS,k5eNaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Ruský	ruský	k2eAgInSc1d1
vliv	vliv	k1gInSc1
v	v	k7c6
Poamuří	Poamuří	k1gNnSc6
byl	být	k5eAaImAgInS
po	po	k7c6
roce	rok	k1gInSc6
1658	#num#	k4
omezen	omezit	k5eAaPmNgMnS
na	na	k7c4
horní	horní	k2eAgInSc4d1
tok	tok	k1gInSc4
Amuru	Amur	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ruští	ruský	k2eAgMnPc1d1
kozáci	kozák	k1gMnPc1
z	z	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
vybírali	vybírat	k5eAaImAgMnP
jasak	jasak	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
přestože	přestože	k8xS
jich	on	k3xPp3gMnPc2
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
nebyla	být	k5eNaImAgFnS
ani	ani	k8xC
stovka	stovka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
Poamuří	Poamuří	k1gNnSc2
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
Poamuří	Poamuř	k1gFnPc2
a	a	k8xC
východního	východní	k2eAgNnSc2d1
Zabajkalska	Zabajkalsko	k1gNnSc2
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
počátku	počátek	k1gInSc2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
několika	několik	k4yIc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
Rusové	Rus	k1gMnPc1
vrátili	vrátit	k5eAaPmAgMnP
zpět	zpět	k6eAd1
na	na	k7c4
Amur	Amur	k1gInSc4
a	a	k8xC
na	na	k7c6
místě	místo	k1gNnSc6
zimoviště	zimoviště	k1gNnSc2
Jerofeje	Jerofej	k1gInSc2
Chabarova	Chabarův	k2eAgInSc2d1
z	z	k7c2
let	léto	k1gNnPc2
1650	#num#	k4
<g/>
/	/	kIx~
<g/>
51	#num#	k4
vystavěli	vystavět	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1665	#num#	k4
Albazin	Albazina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Založili	založit	k5eAaPmAgMnP
ho	on	k3xPp3gNnSc4
ruští	ruský	k2eAgMnPc1d1
osídlenci	osídlenec	k1gMnPc1
z	z	k7c2
Ilimska	Ilimsko	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
vzbouřili	vzbouřit	k5eAaPmAgMnP
proti	proti	k7c3
místnímu	místní	k2eAgMnSc3d1
vojevodovi	vojevod	k1gMnSc3
<g/>
,	,	kIx,
zabili	zabít	k5eAaPmAgMnP
ho	on	k3xPp3gNnSc4
a	a	k8xC
odešli	odejít	k5eAaPmAgMnP
„	„	k?
<g/>
do	do	k7c2
daurské	daurský	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedl	vést	k5eAaImAgMnS
je	on	k3xPp3gNnSc4
Nikifor	Nikifor	k1gMnSc1
Černigovskij	Černigovskij	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začali	začít	k5eAaPmAgMnP
vybírat	vybírat	k5eAaImF
jasak	jasak	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
odesílali	odesílat	k5eAaImAgMnP
přes	přes	k7c4
Něrčinsk	Něrčinsk	k1gInSc4
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Místní	místní	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
získat	získat	k5eAaPmF
nabídkou	nabídka	k1gFnSc7
ochrany	ochrana	k1gFnSc2
a	a	k8xC
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
Mandžuové	Mandžu	k1gMnPc1
místním	místní	k2eAgMnPc3d1
přikázali	přikázat	k5eAaPmAgMnP
odejít	odejít	k5eAaPmF
od	od	k7c2
Amuru	Amur	k1gInSc2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nP
je	on	k3xPp3gNnSc4
měli	mít	k5eAaImAgMnP
pod	pod	k7c7
kontrolou	kontrola	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osídlení	osídlení	k1gNnSc1
Albazinu	Albazin	k1gInSc2
a	a	k8xC
okolí	okolí	k1gNnSc2
rychle	rychle	k6eAd1
rostlo	růst	k5eAaImAgNnS
<g/>
,	,	kIx,
region	region	k1gInSc4
by	by	kYmCp3nS
začátkem	začátkem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
ostatním	ostatní	k2eAgNnSc7d1
Zabajkalskem	Zabajkalsko	k1gNnSc7
nejvíce	nejvíce	k6eAd1,k6eAd3
obydlený	obydlený	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
tehdy	tehdy	k6eAd1
v	v	k7c6
Zabajkalí	Zabajkalí	k1gNnSc6
a	a	k8xC
Poamuří	Poamuří	k1gNnSc6
žilo	žít	k5eAaImAgNnS
cca	cca	kA
1500	#num#	k4
mužů	muž	k1gMnPc2
ruského	ruský	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
a	a	k8xC
stovky	stovka	k1gFnSc2
žen	žena	k1gFnPc2
a	a	k8xC
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
třetiny	třetina	k1gFnPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
v	v	k7c6
Albazinu	Albazin	k1gInSc6
a	a	k8xC
okolních	okolní	k2eAgFnPc6d1
vsích	ves	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
Amur	Amur	k1gInSc4
samovolně	samovolně	k6eAd1
přesídlilo	přesídlit	k5eAaPmAgNnS
tolik	tolik	k4xDc1,k4yIc1
Rusů	Rus	k1gMnPc2
<g/>
,	,	kIx,
až	až	k6eAd1
se	se	k3xPyFc4
sibiřské	sibiřský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
od	od	k7c2
Jeniseje	Jenisej	k1gInSc2
po	po	k7c4
Jakutsk	Jakutsk	k1gInSc4
začaly	začít	k5eAaPmAgFnP
obávat	obávat	k5eAaImF
vylidnění	vylidnění	k1gNnPc4
„	„	k?
<g/>
svých	svůj	k3xOyFgNnPc6
<g/>
“	“	k?
regionů	region	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1679	#num#	k4
<g/>
–	–	k?
<g/>
1681	#num#	k4
albazinští	albazinský	k2eAgMnPc1d1
kozáci	kozák	k1gMnPc1
podle	podle	k7c2
příkazu	příkaz	k1gInSc2
z	z	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
postavili	postavit	k5eAaPmAgMnP
menší	malý	k2eAgInPc4d2
ostrohy	ostroh	k1gInPc4
na	na	k7c6
středním	střední	k2eAgInSc6d1
Amuru	Amur	k1gInSc6
v	v	k7c6
povodí	povodí	k1gNnSc6
Zeji	zet	k5eAaImIp1nS
–	–	k?
roku	rok	k1gInSc2
1679	#num#	k4
Selemdžinský	Selemdžinský	k2eAgInSc4d1
v	v	k7c4
ústí	ústí	k1gNnSc4
Selemdži	Selemdž	k1gFnSc3
a	a	k8xC
Verchnězejský	Verchnězejský	k2eAgMnSc1d1
na	na	k7c6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
Zeji	zet	k5eAaImIp1nS
<g/>
,	,	kIx,
a	a	k8xC
roku	rok	k1gInSc2
1680	#num#	k4
Dolonský	Dolonský	k2eAgInSc4d1
na	na	k7c4
Zeje	zet	k5eAaImIp3nS
u	u	k7c2
ústí	ústí	k1gNnSc2
Dolonky	Dolonka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1681	#num#	k4
něrčinští	něrčinský	k2eAgMnPc1d1
kozáci	kozák	k1gMnPc1
postavili	postavit	k5eAaPmAgMnP
Argunský	Argunský	k2eAgInSc4d1
ostroh	ostroh	k1gInSc4
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Argun	Arguna	k1gFnPc2
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rusko-čchingské	Rusko-čchingský	k2eAgInPc4d1
diplomatické	diplomatický	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
–	–	k?
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
</s>
<s>
Bajkovova	Bajkovův	k2eAgFnSc1d1
mise	mise	k1gFnSc1
(	(	kIx(
<g/>
1654	#num#	k4
<g/>
–	–	k?
<g/>
1658	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1652	#num#	k4
ruská	ruský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
souhlasila	souhlasit	k5eAaImAgFnS
s	s	k7c7
návrhem	návrh	k1gInSc7
tobolských	tobolský	k2eAgMnPc2d1
kupců	kupec	k1gMnPc2
(	(	kIx(
<g/>
původem	původ	k1gInSc7
z	z	k7c2
Buchary	buchar	k1gInPc7
<g/>
)	)	kIx)
a	a	k8xC
kozáků	kozák	k1gMnPc2
na	na	k7c6
otevření	otevření	k1gNnSc6
obchodní	obchodní	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
z	z	k7c2
Tobolska	Tobolsk	k1gInSc2
do	do	k7c2
Číny	Čína	k1gFnSc2
přes	přes	k7c4
území	území	k1gNnSc4
Kalmyků	Kalmyk	k1gMnPc2
a	a	k8xC
Mongolů	Mongol	k1gMnPc2
a	a	k8xC
vyslala	vyslat	k5eAaPmAgFnS
moskevského	moskevský	k2eAgMnSc4d1
šlechtice	šlechtic	k1gMnSc4
Fjodora	Fjodor	k1gMnSc4
Bajkova	Bajkov	k1gInSc2
do	do	k7c2
Tobolska	Tobolsk	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zorganizoval	zorganizovat	k5eAaPmAgMnS
první	první	k4xOgFnPc4
karavany	karavana	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
V	v	k7c6
únoru	únor	k1gInSc6
1654	#num#	k4
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgInS
Bajkov	Bajkov	k1gInSc1
postaven	postavit	k5eAaPmNgInS
do	do	k7c2
čela	čelo	k1gNnSc2
první	první	k4xOgFnSc2
ruské	ruský	k2eAgFnSc2d1
oficiální	oficiální	k2eAgFnSc2d1
diplomatické	diplomatický	k2eAgFnSc2d1
mise	mise	k1gFnSc2
k	k	k7c3
čchingské	čchingský	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Měl	mít	k5eAaImAgInS
navázat	navázat	k5eAaPmF
diplomatické	diplomatický	k2eAgInPc4d1
styky	styk	k1gInPc4
a	a	k8xC
uzavřít	uzavřít	k5eAaPmF
dohodu	dohoda	k1gFnSc4
o	o	k7c6
vzájemném	vzájemný	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
1654	#num#	k4
dorazil	dorazit	k5eAaPmAgMnS
do	do	k7c2
Tobolska	Tobolsk	k1gInSc2
a	a	k8xC
od	od	k7c2
června	červen	k1gInSc2
1654	#num#	k4
pokračoval	pokračovat	k5eAaImAgInS
po	po	k7c6
Irtyši	Irtyš	k1gInSc6
a	a	k8xC
skrze	skrze	k?
mongolské	mongolský	k2eAgFnSc2d1
stepi	step	k1gFnSc2
do	do	k7c2
Pekingu	Peking	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
od	od	k7c2
března	březen	k1gInSc2
1656	#num#	k4
vedl	vést	k5eAaImAgInS
rozhovory	rozhovor	k1gInPc4
s	s	k7c7
čchingskými	čchingský	k2eAgMnPc7d1
úředníky	úředník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc4
oficiální	oficiální	k2eAgNnSc4d1
přijetí	přijetí	k1gNnSc4
čchingským	čchingský	k2eAgMnSc7d1
císařem	císař	k1gMnSc7
(	(	kIx(
<g/>
Šun-č	Šun-č	k1gFnSc1
<g/>
’	’	k?
<g/>
,	,	kIx,
vládl	vládnout	k5eAaImAgMnS
1643	#num#	k4
<g/>
–	–	k?
<g/>
1661	#num#	k4
<g/>
)	)	kIx)
ztroskotalo	ztroskotat	k5eAaPmAgNnS
na	na	k7c6
protokolu	protokol	k1gInSc6
<g/>
;	;	kIx,
čchingská	čchingský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
očekávala	očekávat	k5eAaImAgFnS
audienci	audience	k1gFnSc4
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
protokolem	protokol	k1gInSc7
používaným	používaný	k2eAgInSc7d1
u	u	k7c2
každého	každý	k3xTgMnSc2
jiného	jiný	k2eAgMnSc2d1
podřízeného	podřízený	k1gMnSc2
vazala	vazal	k1gMnSc2
říše	říše	k1gFnSc1
Čching	Čching	k1gInSc1
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
Rusové	Rus	k1gMnPc1
neústupně	ústupně	k6eNd1
žádali	žádat	k5eAaImAgMnP
přijetí	přijetí	k1gNnSc4
vycházející	vycházející	k2eAgNnSc4d1
z	z	k7c2
rovnoprávnosti	rovnoprávnost	k1gFnSc2
čchingského	čchingský	k2eAgMnSc2d1
a	a	k8xC
ruského	ruský	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
začátkem	začátkem	k7c2
září	září	k1gNnSc2
1656	#num#	k4
došla	dojít	k5eAaPmAgFnS
čchingské	čchingská	k1gFnSc3
vládě	vláda	k1gFnSc3
trpělivost	trpělivost	k1gFnSc4
a	a	k8xC
poslala	poslat	k5eAaPmAgFnS
Bajkova	Bajkův	k2eAgFnSc1d1
zpět	zpět	k6eAd1
do	do	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
Moskvy	Moskva	k1gFnSc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
na	na	k7c4
podzim	podzim	k1gInSc4
1658	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mise	mise	k1gFnSc1
Perfiljeva	Perfiljeva	k1gFnSc1
a	a	k8xC
Ablina	Ablina	k1gFnSc1
(	(	kIx(
<g/>
1658	#num#	k4
<g/>
–	–	k?
<g/>
1662	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
další	další	k2eAgFnSc2d1
mise	mise	k1gFnSc2
Ablina	Ablina	k1gMnSc1
(	(	kIx(
<g/>
1668	#num#	k4
<g/>
–	–	k?
<g/>
1671	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čchingský	Čchingský	k1gMnSc1
císař	císař	k1gMnSc1
Kchang-si	Kchang-se	k1gFnSc4
za	za	k7c7
pracovním	pracovní	k2eAgInSc7d1
stolem	stol	k1gInSc7
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
1657	#num#	k4
začala	začít	k5eAaPmAgFnS
ruská	ruský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
,	,	kIx,
znepokojená	znepokojený	k2eAgFnSc1d1
osudem	osud	k1gInSc7
Bajkova	Bajkův	k2eAgInSc2d1
<g/>
,	,	kIx,
připravovat	připravovat	k5eAaImF
další	další	k2eAgNnSc4d1
poselstvo	poselstvo	k1gNnSc4
k	k	k7c3
čchingskému	čchingský	k1gMnSc3
císaři	císař	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
čela	čelo	k1gNnSc2
postavila	postavit	k5eAaPmAgFnS
syna	syn	k1gMnSc4
bojarského	bojarský	k2eAgMnSc2d1
Ivana	Ivan	k1gMnSc2
Perfiljeva	Perfiljev	k1gMnSc2
a	a	k8xC
tobolského	tobolský	k2eAgMnSc4d1
kupce	kupec	k1gMnSc4
bucharského	bucharský	k2eAgInSc2d1
původu	původ	k1gInSc2
Seitkula	Seitkul	k1gMnSc4
Ablina	Ablin	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
cestu	cesta	k1gFnSc4
vyrazili	vyrazit	k5eAaPmAgMnP
roku	rok	k1gInSc2
1658	#num#	k4
<g/>
,	,	kIx,
do	do	k7c2
Pekingu	Peking	k1gInSc2
přijeli	přijet	k5eAaPmAgMnP
počátkem	počátkem	k7c2
června	červen	k1gInSc2
1660	#num#	k4
<g/>
,	,	kIx,
carův	carův	k2eAgInSc1d1
list	list	k1gInSc1
s	s	k7c7
návrhem	návrh	k1gInSc7
na	na	k7c6
ustavení	ustavení	k1gNnSc6
vzájemných	vzájemný	k2eAgInPc2d1
styků	styk	k1gInPc2
předali	předat	k5eAaPmAgMnP
v	v	k7c6
Dvoru	dvůr	k1gInSc6
závislých	závislý	k2eAgFnPc2d1
držav	država	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čchingská	Čchingský	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
navrhla	navrhnout	k5eAaPmAgFnS
list	list	k1gInSc4
odmítnout	odmítnout	k5eAaPmF
jako	jako	k8xS,k8xC
nevhodný	vhodný	k2eNgInSc1d1
–	–	k?
nepoužívající	používající	k2eNgInSc4d1
čínský	čínský	k2eAgInSc4d1
kalendář	kalendář	k1gInSc4
a	a	k8xC
neúměrně	úměrně	k6eNd1
vyvyšující	vyvyšující	k2eAgFnSc1d1
cara	car	k1gMnSc2
přisvojujícího	přisvojující	k2eAgInSc2d1
si	se	k3xPyFc3
tituly	titul	k1gInPc1
neschválené	schválený	k2eNgFnSc2d1
čchingským	čchingský	k1gMnSc7
císařem	císař	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
nakonec	nakonec	k6eAd1
byly	být	k5eAaImAgInP
dary	dar	k1gInPc4
a	a	k8xC
list	list	k1gInSc4
přijaty	přijmout	k5eAaPmNgInP
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
na	na	k7c4
něj	on	k3xPp3gMnSc4
nebylo	být	k5eNaImAgNnS
odpovězeno	odpovězen	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ablin	Ablin	k1gMnSc1
se	se	k3xPyFc4
do	do	k7c2
Číny	Čína	k1gFnSc2
s	s	k7c7
oficiálním	oficiální	k2eAgNnSc7d1
pověřením	pověření	k1gNnSc7
vydal	vydat	k5eAaPmAgMnS
znovu	znovu	k6eAd1
<g/>
,	,	kIx,
po	po	k7c6
dvouleté	dvouletý	k2eAgFnSc6d1
přípravě	příprava	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1668	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Pekingu	Peking	k1gInSc2
přijel	přijet	k5eAaPmAgMnS
v	v	k7c6
říjnu	říjen	k1gInSc6
1669	#num#	k4
<g/>
,	,	kIx,
setrval	setrvat	k5eAaPmAgInS
v	v	k7c6
něm	on	k3xPp3gNnSc6
tři	tři	k4xCgFnPc1
a	a	k8xC
půl	půl	k1xP
měsíce	měsíc	k1gInSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
neformálně	formálně	k6eNd1
přijat	přijmout	k5eAaPmNgMnS
císařem	císař	k1gMnSc7
Kchang-sim	Kchang-sim	k1gMnSc1
(	(	kIx(
<g/>
vládl	vládnout	k5eAaImAgMnS
1661	#num#	k4
<g/>
–	–	k?
<g/>
1722	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Ablinův	Ablinův	k2eAgInSc1d1
zájem	zájem	k1gInSc1
byl	být	k5eAaImAgInS
především	především	k6eAd1
obchodní	obchodní	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
politické	politický	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
nechal	nechat	k5eAaPmAgMnS
stranou	stranou	k6eAd1
<g/>
,	,	kIx,
čchingská	čchingskat	k5eAaPmIp3nS
strana	strana	k1gFnSc1
však	však	k9
ústně	ústně	k6eAd1
vznesla	vznést	k5eAaPmAgFnS
žádost	žádost	k1gFnSc4
o	o	k7c4
vydání	vydání	k1gNnSc4
Gantimura	Gantimur	k1gMnSc2
<g/>
,	,	kIx,
evenckého	evencký	k1gMnSc2
náčelníka	náčelník	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
roku	rok	k1gInSc2
1667	#num#	k4
přešel	přejít	k5eAaPmAgInS
od	od	k7c2
Čchingů	Čching	k1gInPc2
k	k	k7c3
Rusům	Rus	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
7	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
odjezdu	odjezd	k1gInSc6
se	se	k3xPyFc4
Ablin	Ablin	k1gInSc1
na	na	k7c4
rok	rok	k1gInSc4
zdržel	zdržet	k5eAaPmAgInS
kvůli	kvůli	k7c3
neklidu	neklid	k1gInSc3
v	v	k7c6
Mongolsku	Mongolsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Číny	Čína	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
až	až	k9
v	v	k7c6
létě	léto	k1gNnSc6
1671	#num#	k4
<g/>
,	,	kIx,
do	do	k7c2
Tobolsku	Tobolsek	k1gInSc2
dorazil	dorazit	k5eAaPmAgMnS
v	v	k7c6
říjnu	říjen	k1gInSc6
1671	#num#	k4
a	a	k8xC
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
v	v	k7c6
únoru	únor	k1gInSc6
1672	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
cesta	cesta	k1gFnSc1
byla	být	k5eAaImAgFnS
hodnocena	hodnotit	k5eAaImNgFnS
velmi	velmi	k6eAd1
příznivě	příznivě	k6eAd1
kvůli	kvůli	k7c3
finančnímu	finanční	k2eAgInSc3d1
zisku	zisk	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
přinesla	přinést	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
státního	státní	k2eAgMnSc2d1
se	se	k3xPyFc4
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
rozvíjel	rozvíjet	k5eAaImAgInS
i	i	k8xC
soukromý	soukromý	k2eAgInSc1d1
obchod	obchod	k1gInSc1
mezi	mezi	k7c7
ruskou	ruský	k2eAgFnSc7d1
Sibiří	Sibiř	k1gFnSc7
a	a	k8xC
Čínou	Čína	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
roku	rok	k1gInSc2
1672	#num#	k4
byl	být	k5eAaImAgInS
do	do	k7c2
Číny	Čína	k1gFnSc2
vyvezeno	vyvézt	k5eAaPmNgNnS
ze	z	k7c2
Sibiře	Sibiř	k1gFnSc2
na	na	k7c4
13	#num#	k4
tisíc	tisíc	k4xCgInPc2
sobolích	sobolí	k2eAgFnPc2d1
kožešin	kožešina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Aršinského	Aršinský	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
s	s	k7c7
Čchingy	Čching	k1gInPc7
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
1669	#num#	k4
přišla	přijít	k5eAaPmAgFnS
k	k	k7c3
Něrčinsku	Něrčinsko	k1gNnSc3
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
dvoudenního	dvoudenní	k2eAgInSc2d1
pochodu	pochod	k1gInSc2
šestitisícová	šestitisícový	k2eAgFnSc1d1
čchingská	čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Malou	Malá	k1gFnSc7
<g/>
,	,	kIx,
zkušeným	zkušený	k2eAgMnSc7d1
mandžuským	mandžuský	k2eAgMnSc7d1
diplomatem	diplomat	k1gMnSc7
a	a	k8xC
úředníkem	úředník	k1gMnSc7
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
zástupce	zástupce	k1gMnSc4
si	se	k3xPyFc3
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
stěžoval	stěžovat	k5eAaImAgMnS
na	na	k7c4
agresivitu	agresivita	k1gFnSc4
albazinských	albazinský	k2eAgInPc2d1
kozáků	kozák	k1gInPc2
vůči	vůči	k7c3
Daurům	Daur	k1gMnPc3
a	a	k8xC
Ďučerům	Ďučer	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
1670	#num#	k4
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
čchingští	čchingský	k2eAgMnPc1d1
poslové	posel	k1gMnPc1
požadovali	požadovat	k5eAaImAgMnP
–	–	k?
v	v	k7c6
zájmu	zájem	k1gInSc6
mírových	mírový	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
–	–	k?
vydání	vydání	k1gNnSc1
Gantimura	Gantimura	k1gFnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vojenské	vojenský	k2eAgFnPc4d1
akce	akce	k1gFnPc4
proti	proti	k7c3
kozákům	kozák	k1gMnPc3
z	z	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
omlouvali	omlouvat	k5eAaImAgMnP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
čchingské	čchingský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
je	on	k3xPp3gMnPc4
měly	mít	k5eAaImAgInP
za	za	k7c4
bandity	bandita	k1gMnPc4
a	a	k8xC
nevěděli	vědět	k5eNaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
ruskými	ruský	k2eAgMnPc7d1
poddanými	poddaný	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Něrčinský	Něrčinský	k2eAgMnSc1d1
vojevoda	vojevoda	k1gMnSc1
Daniil	Daniil	k1gMnSc1
Aršinskij	Aršinskij	k1gMnSc1
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
iniciativy	iniciativa	k1gFnSc2
reagoval	reagovat	k5eAaBmAgMnS
vysláním	vyslání	k1gNnSc7
desátníka	desátník	k1gMnSc2
Ignatije	Ignatije	k1gMnSc2
Milovanova	Milovanův	k2eAgMnSc2d1
do	do	k7c2
Pekingu	Peking	k1gInSc2
s	s	k7c7
listem	list	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
vyzýval	vyzývat	k5eAaImAgMnS
čchingského	čchingský	k2eAgMnSc4d1
císaře	císař	k1gMnSc4
k	k	k7c3
podřízení	podřízení	k1gNnSc3
se	se	k3xPyFc4
carovi	carův	k2eAgMnPc1d1
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
nevydání	nevydání	k1gNnSc4
Gantimura	Gantimur	k1gMnSc2
vysvětloval	vysvětlovat	k5eAaImAgMnS
jeho	jeho	k3xOp3gNnSc7
stářím	stáří	k1gNnSc7
a	a	k8xC
nemocí	nemoc	k1gFnSc7
a	a	k8xC
neexistencí	neexistence	k1gFnSc7
carského	carský	k2eAgNnSc2d1
rozhodnutí	rozhodnutí	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c6
něm	on	k3xPp3gMnSc6
a	a	k8xC
albazinské	albazinský	k2eAgInPc4d1
kozáky	kozák	k1gInPc4
omlouval	omlouvat	k5eAaImAgMnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jen	jen	k9
bránili	bránit	k5eAaImAgMnP
útokům	útok	k1gInPc3
Daurů	Daur	k1gMnPc2
a	a	k8xC
Ďučerů	Ďučer	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
tucet	tucet	k1gInSc4
Rusů	Rus	k1gMnPc2
zabili	zabít	k5eAaPmAgMnP
a	a	k8xC
ukradli	ukradnout	k5eAaPmAgMnP
jim	on	k3xPp3gMnPc3
skot	skot	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Milovanov	Milovanov	k1gInSc1
vyrazil	vyrazit	k5eAaPmAgMnS
v	v	k7c6
dubnu	duben	k1gInSc6
1670	#num#	k4
<g/>
,	,	kIx,
během	během	k7c2
měsíce	měsíc	k1gInSc2
dojel	dojet	k5eAaPmAgMnS
do	do	k7c2
Pekingu	Peking	k1gInSc2
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
56	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
předal	předat	k5eAaPmAgMnS
list	list	k1gInSc4
v	v	k7c6
Dvoru	dvůr	k1gInSc6
závislých	závislý	k2eAgFnPc2d1
držav	država	k1gFnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc1
úředníci	úředník	k1gMnPc1
ho	on	k3xPp3gNnSc4
oficiálně	oficiálně	k6eAd1
přeložili	přeložit	k5eAaPmAgMnP
císaři	císař	k1gMnPc1
jako	jako	k9
nabídku	nabídka	k1gFnSc4
cara	car	k1gMnSc2
na	na	k7c4
podřízení	podřízení	k1gNnSc4
se	se	k3xPyFc4
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
jest	být	k5eAaImIp3nS
právě	právě	k9
opačně	opačně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
Milovanova	Milovanův	k2eAgFnSc1d1
oficiálně	oficiálně	k6eAd1
přijal	přijmout	k5eAaPmAgMnS
císař	císař	k1gMnSc1
před	před	k7c7
celým	celý	k2eAgInSc7d1
dvorem	dvůr	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
se	se	k3xPyFc4
Milovanov	Milovanov	k1gInSc1
vrátil	vrátit	k5eAaPmAgInS
s	s	k7c7
čchingským	čchingský	k1gMnSc7
zástupcem	zástupce	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
Aršinskému	Aršinský	k2eAgMnSc3d1
předal	předat	k5eAaPmAgInS
císařův	císařův	k2eAgInSc1d1
list	list	k1gInSc1
carovi	car	k1gMnSc3
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
císař	císař	k1gMnSc1
za	za	k7c4
vydání	vydání	k1gNnSc4
Gantimura	Gantimur	k1gMnSc2
sliboval	slibovat	k5eAaImAgMnS
mír	mír	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rusové	Rus	k1gMnPc1
na	na	k7c4
čchingský	čchingský	k2eAgInSc4d1
návrh	návrh	k1gInSc4
bezprostředně	bezprostředně	k6eAd1
nereagovali	reagovat	k5eNaBmAgMnP
<g/>
,	,	kIx,
vyslanec	vyslanec	k1gMnSc1
Nikolaj	Nikolaj	k1gMnSc1
Spafarij	Spafarij	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
o	o	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
odvolával	odvolávat	k5eAaImAgInS
na	na	k7c4
tento	tento	k3xDgInSc4
list	list	k1gInSc4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nepřijel	přijet	k5eNaPmAgMnS
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
Moskvy	Moskva	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
čchingskou	čchingský	k2eAgFnSc4d1
výzvu	výzva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
1672	#num#	k4
se	se	k3xPyFc4
u	u	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
opět	opět	k6eAd1
objevil	objevit	k5eAaPmAgMnS
čchingský	čchingský	k2eAgMnSc1d1
posel	posel	k1gMnSc1
s	s	k7c7
nevelkým	velký	k2eNgInSc7d1
vojenským	vojenský	k2eAgInSc7d1
oddílem	oddíl	k1gInSc7
a	a	k8xC
přesvědčoval	přesvědčovat	k5eAaImAgMnS
náčelníky	náčelník	k1gInPc4
Rusům	Rus	k1gMnPc3
poddaných	poddaný	k1gMnPc2
domorodců	domorodec	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
přešli	přejít	k5eAaPmAgMnP
k	k	k7c3
Čchingům	Čching	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spafarijova	Spafarijův	k2eAgFnSc1d1
mise	mise	k1gFnSc1
(	(	kIx(
<g/>
1675	#num#	k4
<g/>
–	–	k?
<g/>
1678	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ruští	ruský	k2eAgMnPc1d1
vyslanci	vyslanec	k1gMnPc1
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnPc1
časopisu	časopis	k1gInSc2
Niva	niva	k1gFnSc1
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Mezi	mezi	k7c7
rokem	rok	k1gInSc7
1670	#num#	k4
a	a	k8xC
příchodem	příchod	k1gInSc7
Spafarije	Spafarije	k1gFnSc2
se	se	k3xPyFc4
Čchingové	Čchingový	k2eAgMnPc4d1
třikrát	třikrát	k6eAd1
dotazovali	dotazovat	k5eAaImAgMnP
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
už	už	k6eAd1
přijede	přijet	k5eAaPmIp3nS
carův	carův	k2eAgMnSc1d1
vyslanec	vyslanec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1675	#num#	k4
se	se	k3xPyFc4
ruská	ruský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
vyslat	vyslat	k5eAaPmF
do	do	k7c2
Pekingu	Peking	k1gInSc2
zkušeného	zkušený	k2eAgMnSc2d1
diplomata	diplomat	k1gMnSc2
moldavského	moldavský	k2eAgInSc2d1
původu	původ	k1gInSc2
Nikolaje	Nikolaj	k1gMnSc4
Spafarije	Spafarije	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
za	za	k7c4
úkol	úkol	k1gInSc4
ustavit	ustavit	k5eAaPmF
oficiální	oficiální	k2eAgInPc4d1
obchodní	obchodní	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
a	a	k8xC
diplomatické	diplomatický	k2eAgInPc4d1
styky	styk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
teritoriálním	teritoriální	k2eAgFnPc3d1
otázkám	otázka	k1gFnPc3
se	se	k3xPyFc4
neměl	mít	k5eNaImAgMnS
vyjadřovat	vyjadřovat	k5eAaImF
<g/>
,	,	kIx,
ruská	ruský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
totiž	totiž	k9
měla	mít	k5eAaImAgFnS
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c6
ruských	ruský	k2eAgFnPc6d1
poddaných	poddaná	k1gFnPc6
a	a	k8xC
území	území	k1gNnSc6
není	být	k5eNaImIp3nS
třeba	třeba	k6eAd1
diskutovat	diskutovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spafarij	Spafarít	k5eAaPmRp2nS
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
vyjel	vyjet	k5eAaPmAgInS
začátkem	začátkem	k7c2
března	březen	k1gInSc2
1675	#num#	k4
<g/>
,	,	kIx,
cestoval	cestovat	k5eAaImAgMnS
přes	přes	k7c4
Irkutsk	Irkutsk	k1gInSc4
<g/>
,	,	kIx,
Něrčinsk	Něrčinsk	k1gInSc4
a	a	k8xC
Albazin	Albazin	k1gInSc4
a	a	k8xC
na	na	k7c6
Velkém	velký	k2eAgInSc6d1
Chinganu	Chingan	k1gInSc6
se	se	k3xPyFc4
setkal	setkat	k5eAaPmAgInS
s	s	k7c7
čchingskými	čchingský	k2eAgMnPc7d1
zástupci	zástupce	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Rozhovory	rozhovor	k1gInPc1
se	s	k7c7
Spafarijem	Spafarij	k1gInSc7
vedli	vést	k5eAaImAgMnP
Mandžuové	Mandžu	k1gMnPc1
na	na	k7c6
Noně	Nona	k1gFnSc6
po	po	k7c4
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
do	do	k7c2
začátku	začátek	k1gInSc2
března	březen	k1gInSc2
1676	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
zahájením	zahájení	k1gNnSc7
jednání	jednání	k1gNnSc2
po	po	k7c6
něm	on	k3xPp3gMnSc6
chtěli	chtít	k5eAaImAgMnP
předání	předání	k1gNnSc4
carova	carův	k2eAgInSc2d1
listu	list	k1gInSc2
a	a	k8xC
vydání	vydání	k1gNnSc2
Gantimura	Gantimura	k1gFnSc1
<g/>
;	;	kIx,
o	o	k7c6
obojím	obojí	k4xRgInSc6
odmítl	odmítnout	k5eAaPmAgMnS
i	i	k9
jen	jen	k6eAd1
diskutovat	diskutovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
uchýlil	uchýlit	k5eAaPmAgMnS
k	k	k7c3
přetvářce	přetvářka	k1gFnSc3
a	a	k8xC
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
car	car	k1gMnSc1
nemohl	moct	k5eNaImAgMnS
svolit	svolit	k5eAaPmF
k	k	k7c3
vydání	vydání	k1gNnSc3
Gantimura	Gantimur	k1gMnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
nerozuměli	rozumět	k5eNaImAgMnP
císařovu	císařův	k2eAgFnSc4d1
listu	lista	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1670	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
čemuž	což	k3yRnSc3,k3yQnSc3
mandžuští	mandžuský	k2eAgMnPc1d1
diplomaté	diplomat	k1gMnPc1
na	na	k7c6
Noně	Nona	k1gFnSc6
moc	moc	k6eAd1
nevěřili	věřit	k5eNaImAgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
císařská	císařský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
v	v	k7c6
Pekingu	Peking	k1gInSc6
vysvětlení	vysvětlení	k1gNnSc2
přijala	přijmout	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
Pekingu	Peking	k1gInSc2
Spafarij	Spafarij	k1gMnSc2
dorazil	dorazit	k5eAaPmAgMnS
v	v	k7c6
polovině	polovina	k1gFnSc6
května	květen	k1gInSc2
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
na	na	k7c4
mnoho	mnoho	k4c4
týdnů	týden	k1gInPc2
se	se	k3xPyFc4
střetl	střetnout	k5eAaPmAgInS
s	s	k7c7
pekingským	pekingský	k2eAgInSc7d1
dvorským	dvorský	k2eAgInSc7d1
ceremoniálem	ceremoniál	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
předpokládal	předpokládat	k5eAaImAgInS
nižší	nízký	k2eAgInSc4d2
status	status	k1gInSc4
všech	všecek	k3xTgInPc2
cizích	cizí	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
automaticky	automaticky	k6eAd1
považovaných	považovaný	k2eAgInPc2d1
za	za	k7c4
vazaly	vazal	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
prestižních	prestižní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
–	–	k?
při	při	k7c6
problémech	problém	k1gInPc6
v	v	k7c6
Číně	Čína	k1gFnSc6
(	(	kIx(
<g/>
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
tří	tři	k4xCgMnPc2
vazalů	vazal	k1gMnPc2
v	v	k7c6
letech	let	k1gInPc6
1673	#num#	k4
<g/>
–	–	k?
<g/>
1681	#num#	k4
<g/>
)	)	kIx)
–	–	k?
čchingská	čchingský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
nechtěla	chtít	k5eNaImAgFnS
ustoupit	ustoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neformálně	formálně	k6eNd1
byl	být	k5eAaImAgInS
v	v	k7c6
červnu	červen	k1gInSc6
přijat	přijmout	k5eAaPmNgInS
císařem	císař	k1gMnSc7
a	a	k8xC
delegaci	delegace	k1gFnSc3
bylo	být	k5eAaImAgNnS
dovoleno	dovolen	k2eAgNnSc1d1
obchodovat	obchodovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Nakonec	nakonec	k6eAd1
Čchingové	Čchingový	k2eAgFnPc1d1
podmínili	podmínit	k5eAaPmAgMnP
uzavření	uzavření	k1gNnSc4
obchodní	obchodní	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
vydáním	vydání	k1gNnSc7
Gantimura	Gantimura	k1gFnSc1
a	a	k8xC
dodržením	dodržení	k1gNnSc7
ceremoniálu	ceremoniál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neoficiálně	oficiálně	k6eNd1,k6eAd1
se	s	k7c7
Spafarij	Spafarij	k1gFnPc7
dozvěděl	dozvědět	k5eAaPmAgMnS
o	o	k7c6
záměru	záměr	k1gInSc6
vyhnat	vyhnat	k5eAaPmF
Rusy	Rus	k1gMnPc4
z	z	k7c2
Albazinu	Albazin	k1gInSc2
a	a	k8xC
Něrčinska	Něrčinsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
září	září	k1gNnSc6
1676	#num#	k4
odjel	odjet	k5eAaPmAgMnS
z	z	k7c2
Pekingu	Peking	k1gInSc2
do	do	k7c2
vlasti	vlast	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
dorazil	dorazit	k5eAaPmAgMnS
začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1678	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čchingská	Čchingský	k2eAgFnSc1d1
příprava	příprava	k1gFnSc1
na	na	k7c4
válku	válka	k1gFnSc4
a	a	k8xC
podněcování	podněcování	k1gNnSc4
Mongolů	Mongol	k1gMnPc2
(	(	kIx(
<g/>
počátek	počátek	k1gInSc4
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Po	po	k7c6
porážce	porážka	k1gFnSc6
povstání	povstání	k1gNnSc2
tří	tři	k4xCgMnPc2
vazalů	vazal	k1gMnPc2
v	v	k7c6
Číně	Čína	k1gFnSc6
roku	rok	k1gInSc2
1681	#num#	k4
si	se	k3xPyFc3
Čchingové	Čchingový	k2eAgFnPc1d1
uvolnili	uvolnit	k5eAaPmAgMnP
síly	síla	k1gFnPc4
pro	pro	k7c4
boj	boj	k1gInSc4
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
od	od	k7c2
začátku	začátek	k1gInSc2
80	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
začali	začít	k5eAaPmAgMnP
připravovat	připravovat	k5eAaImF
útok	útok	k1gInSc4
na	na	k7c4
Rusy	Rus	k1gMnPc4
na	na	k7c6
Amuru	Amur	k1gInSc6
a	a	k8xC
v	v	k7c6
Zabajkalí	Zabajkalí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc7
nejsevernějšími	severní	k2eAgInPc7d3
opěrnými	opěrný	k2eAgInPc7d1
body	bod	k1gInPc7
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byly	být	k5eAaImAgFnP
Ninguta	Ningut	k2eAgMnSc4d1
na	na	k7c6
řece	řeka	k1gFnSc6
Mu-tan-ťiang	Mu-tan-ťianga	k1gFnPc2
<g/>
,	,	kIx,
jižním	jižní	k2eAgInSc6d1
přítoku	přítok	k1gInSc6
Sungari	Sungar	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
Girin	Girin	k1gInSc1
na	na	k7c6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
Sungari	Sungar	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
němž	jenž	k3xRgInSc6
od	od	k7c2
roku	rok	k1gInSc2
1657	#num#	k4
fungovaly	fungovat	k5eAaImAgFnP
loděnice	loděnice	k1gFnPc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
od	od	k7c2
roku	rok	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
1674	#num#	k4
pevnost	pevnost	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
roku	rok	k1gInSc2
1676	#num#	k4
od	od	k7c2
Ninguty	Ningut	k2eAgFnPc1d1
převzal	převzít	k5eAaPmAgInS
roli	role	k1gFnSc4
sídla	sídlo	k1gNnSc2
mandžuské	mandžuský	k2eAgFnSc2d1
správy	správa	k1gFnSc2
regionu	region	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Paralelně	paralelně	k6eAd1
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
obraně	obrana	k1gFnSc3
mandžuských	mandžuský	k2eAgNnPc2d1
území	území	k1gNnPc2
před	před	k7c7
usazováním	usazování	k1gNnSc7
Mongolů	Mongol	k1gMnPc2
<g/>
,	,	kIx,
Korejců	Korejec	k1gMnPc2
a	a	k8xC
Rusů	Rus	k1gMnPc2
<g/>
,	,	kIx,
císař	císař	k1gMnSc1
roku	rok	k1gInSc2
1678	#num#	k4
nařídil	nařídit	k5eAaPmAgMnS
na	na	k7c6
hranicích	hranice	k1gFnPc6
říše	říš	k1gFnSc2
Čching	Čching	k1gInSc1
vystavět	vystavět	k5eAaPmF
opevněnou	opevněný	k2eAgFnSc4d1
linii	linie	k1gFnSc4
<g/>
,	,	kIx,
takzvanou	takzvaný	k2eAgFnSc4d1
vrbovou	vrbový	k2eAgFnSc4d1
palisádu	palisáda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
1681	#num#	k4
<g/>
–	–	k?
<g/>
1683	#num#	k4
<g/>
,	,	kIx,
strávili	strávit	k5eAaPmAgMnP
Mandžuové	Mandžu	k1gMnPc1
přípravami	příprava	k1gFnPc7
–	–	k?
budováním	budování	k1gNnSc7
cest	cesta	k1gFnPc2
<g/>
,	,	kIx,
stavbou	stavba	k1gFnSc7
lodí	loď	k1gFnSc7
<g/>
,	,	kIx,
shromažďovaním	shromažďovaní	k1gNnSc7
zásob	zásoba	k1gFnPc2
<g/>
,	,	kIx,
zpravodajskou	zpravodajský	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naplánovali	naplánovat	k5eAaBmAgMnP
útok	útok	k1gInSc4
na	na	k7c4
Albazin	Albazin	k1gInSc4
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
úspěchu	úspěch	k1gInSc2
počítali	počítat	k5eAaImAgMnP
s	s	k7c7
pokračováním	pokračování	k1gNnSc7
na	na	k7c4
Něrčinsk	Něrčinsk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenským	vojenský	k2eAgNnSc7d1
akcím	akce	k1gFnPc3
předcházel	předcházet	k5eAaImAgInS
diplomatický	diplomatický	k2eAgInSc1d1
tlak	tlak	k1gInSc1
<g/>
,	,	kIx,
proto	proto	k8xC
roku	rok	k1gInSc2
1681	#num#	k4
mandžuští	mandžuský	k2eAgMnPc1d1
vyslanci	vyslanec	k1gMnPc1
v	v	k7c6
Albazinu	Albazino	k1gNnSc6
protestovali	protestovat	k5eAaBmAgMnP
proti	proti	k7c3
výstavbě	výstavba	k1gFnSc3
ruských	ruský	k2eAgFnPc2d1
pevnůstek	pevnůstka	k1gFnPc2
na	na	k7c6
Zeje	zet	k5eAaImIp3nS
<g/>
,	,	kIx,
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
na	na	k7c4
jejich	jejich	k3xOp3gFnSc4
hranici	hranice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
odvětili	odvětit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
kmeny	kmen	k1gInPc1
na	na	k7c4
Zeje	zet	k5eAaImIp3nS
jsou	být	k5eAaImIp3nP
jejich	jejich	k3xOp3gMnPc7
poddanými	poddaný	k2eAgMnPc7d1
a	a	k8xC
nejlepší	dobrý	k2eAgFnPc4d3
hranice	hranice	k1gFnPc4
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
na	na	k7c6
Amuru	Amur	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Albazinci	Albazinec	k1gMnPc1
žádali	žádat	k5eAaImAgMnP
ruskou	ruský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
o	o	k7c4
posílení	posílení	k1gNnSc4
obrany	obrana	k1gFnSc2
před	před	k7c4
Mandžuy	Mandžu	k1gMnPc4
<g/>
;	;	kIx,
z	z	k7c2
jihu	jih	k1gInSc2
v	v	k7c6
prosinci	prosinec	k1gInSc6
1681	#num#	k4
přišla	přijít	k5eAaPmAgFnS
od	od	k7c2
daurského	daurský	k2eAgMnSc2d1
náčelníka	náčelník	k1gMnSc2
Loskadoje	Loskadoj	k1gInSc2
a	a	k8xC
tunguzských	tunguzský	k2eAgFnPc2d1
Ajusiho	Ajusi	k1gMnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Kajaldy	Kajald	k1gInPc1
(	(	kIx(
<g/>
odehnaných	odehnaný	k2eAgMnPc2d1
z	z	k7c2
Amuru	Amur	k1gInSc2
Mandžuy	Mandžu	k1gMnPc4
v	v	k7c4
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
)	)	kIx)
prosba	prosba	k1gFnSc1
o	o	k7c4
přijetí	přijetí	k1gNnSc4
do	do	k7c2
ruského	ruský	k2eAgNnSc2d1
poddanství	poddanství	k1gNnSc2
a	a	k8xC
ochranu	ochrana	k1gFnSc4
při	při	k7c6
cestě	cesta	k1gFnSc6
na	na	k7c4
sever	sever	k1gInSc4
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
je	on	k3xPp3gMnPc4
Mandžuové	Mandžu	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
přesídlit	přesídlit	k5eAaPmF
dále	daleko	k6eAd2
na	na	k7c4
jih	jih	k1gInSc4
do	do	k7c2
Číny	Čína	k1gFnSc2
a	a	k8xC
sami	sám	k3xTgMnPc1
chtějí	chtít	k5eAaImIp3nP
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
zejské	zejský	k2eAgFnPc4d1
ostrohy	ostroha	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
západního	západní	k2eAgNnSc2d1
Zabajkalska	Zabajkalsko	k1gNnSc2
od	od	k7c2
40	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
1682	#num#	k4
do	do	k7c2
Albazinu	Albazin	k1gInSc2
a	a	k8xC
Něrčinska	Něrčinsko	k1gNnSc2
dorazili	dorazit	k5eAaPmAgMnP
čchingští	čchingský	k2eAgMnPc1d1
poslové	posel	k1gMnPc1
s	s	k7c7
žádostí	žádost	k1gFnSc7
o	o	k7c4
rozhovory	rozhovor	k1gInPc4
na	na	k7c4
Nonu	Nona	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
nepřinesly	přinést	k5eNaPmAgInP
výsledky	výsledek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
se	se	k3xPyFc4
nakrátko	nakrátko	k6eAd1
pod	pod	k7c7
Albazinem	Albazino	k1gNnSc7
objevil	objevit	k5eAaPmAgInS
tisícičlenný	tisícičlenný	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
mandžuské	mandžuský	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
vedený	vedený	k2eAgInSc1d1
generály	generál	k1gMnPc7
Pengcunem	Pengcun	k1gMnSc7
a	a	k8xC
Langtanem	Langtan	k1gInSc7
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
pod	pod	k7c7
záminkou	záminka	k1gFnSc7
žádosti	žádost	k1gFnSc2
o	o	k7c6
vydání	vydání	k1gNnSc6
dvou	dva	k4xCgMnPc2
utečenců	utečenec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
hodnotili	hodnotit	k5eAaImAgMnP
situaci	situace	k1gFnSc4
v	v	k7c6
Albazinu	Albazin	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Langtan	Langtan	k1gInSc1
usoudil	usoudit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
dobytí	dobytí	k1gNnSc3
postačí	postačit	k5eAaPmIp3nS
tři	tři	k4xCgInPc1
tisíce	tisíc	k4xCgInPc4
vojáků	voják	k1gMnPc2
s	s	k7c7
20	#num#	k4
děly	dělo	k1gNnPc7
<g/>
,	,	kIx,
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
vojsko	vojsko	k1gNnSc1
by	by	kYmCp3nP
mělo	mít	k5eAaImAgNnS
přijít	přijít	k5eAaPmF
po	po	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
zásoby	zásoba	k1gFnPc4
dopravit	dopravit	k5eAaPmF
po	po	k7c6
řece	řeka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současně	současně	k6eAd1
roku	rok	k1gInSc2
1681	#num#	k4
mongolské	mongolský	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
narušovaly	narušovat	k5eAaImAgInP
hranici	hranice	k1gFnSc4
v	v	k7c6
Zabajkalsku	Zabajkalsko	k1gNnSc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
Selenginska	Selenginsko	k1gNnSc2
a	a	k8xC
agent	agent	k1gMnSc1
mongolského	mongolský	k2eAgMnSc2d1
náčelníka	náčelník	k1gMnSc2
Cecen-nojona	Cecen-nojon	k1gMnSc2
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgMnS
proti	proti	k7c3
Rusům	Rus	k1gMnPc3
pozvednout	pozvednout	k5eAaPmF
místní	místní	k2eAgMnPc4d1
Tunguzy	Tunguz	k1gMnPc4
a	a	k8xC
Burjaty	Burjat	k1gMnPc4
<g/>
;	;	kIx,
ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
však	však	k9
postavili	postavit	k5eAaPmAgMnP
proti	proti	k7c3
němu	on	k3xPp3gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
následného	následný	k2eAgNnSc2d1
jednání	jednání	k1gNnSc2
s	s	k7c7
mongolskými	mongolský	k2eAgMnPc7d1
vůdci	vůdce	k1gMnPc7
<g/>
,	,	kIx,
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Tüšetü-chánem	Tüšetü-chán	k1gMnSc7
Čichundordžem	Čichundordž	k1gMnSc7
<g/>
,	,	kIx,
vyplynulo	vyplynout	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Mongolové	Mongol	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
vrátit	vrátit	k5eAaPmF
Burjaty	Burjat	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
odkočovali	odkočovat	k5eAaImAgMnP,k5eAaPmAgMnP
k	k	k7c3
Rusům	Rus	k1gMnPc3
<g/>
,	,	kIx,
a	a	k8xC
zrušení	zrušení	k1gNnSc4
ruských	ruský	k2eAgFnPc2d1
pohraničních	pohraniční	k2eAgFnPc2d1
pevností	pevnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obojí	oboj	k1gFnPc2
Rusové	Rus	k1gMnPc1
jednoznačně	jednoznačně	k6eAd1
odmítli	odmítnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1681	#num#	k4
s	s	k7c7
Čichundordžem	Čichundordž	k1gMnSc7
jednali	jednat	k5eAaImAgMnP
též	též	k9
Mandžuové	Mandžu	k1gMnPc1
a	a	k8xC
přiměli	přimět	k5eAaPmAgMnP
ho	on	k3xPp3gMnSc4
k	k	k7c3
útokům	útok	k1gInPc3
na	na	k7c4
Rusy	Rus	k1gMnPc4
<g/>
,	,	kIx,
od	od	k7c2
září	září	k1gNnSc2
1681	#num#	k4
následovaly	následovat	k5eAaImAgInP
nájezdy	nájezd	k1gInPc1
mongolských	mongolský	k2eAgMnPc2d1
oddílů	oddíl	k1gInPc2
na	na	k7c4
Zabajkalsko	Zabajkalsko	k1gNnSc4
trvající	trvající	k2eAgNnSc4d1
do	do	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
1682	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paralelně	paralelně	k6eAd1
k	k	k7c3
Rusům	Rus	k1gMnPc3
utíkali	utíkat	k5eAaImAgMnP
řadoví	řadový	k2eAgMnPc1d1
Mongolové	Mongol	k1gMnPc1
a	a	k8xC
Burjati	Burjat	k1gMnPc1
<g/>
,	,	kIx,
k	k	k7c3
velké	velký	k2eAgFnSc3d1
nevoli	nevole	k1gFnSc3
mongolských	mongolský	k2eAgMnPc2d1
chánů	chán	k1gMnPc2
a	a	k8xC
nojonů	nojon	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1682	#num#	k4
však	však	k9
někteří	některý	k3yIgMnPc1
mongolští	mongolský	k2eAgMnPc1d1
tajši	tajš	k1gMnPc1
(	(	kIx(
<g/>
náčelníci	náčelník	k1gMnPc1
<g/>
)	)	kIx)
zahájili	zahájit	k5eAaPmAgMnP
s	s	k7c7
Rusy	Rus	k1gMnPc4
mírové	mírový	k2eAgInPc1d1
rozhovory	rozhovor	k1gInPc1
a	a	k8xC
na	na	k7c4
podzim	podzim	k1gInSc4
dohodli	dohodnout	k5eAaPmAgMnP
mír	mír	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Boje	boj	k1gInPc1
v	v	k7c4
Poamuří	Poamuří	k1gNnSc4
a	a	k8xC
Zabajkalí	Zabajkalí	k1gNnSc4
1683	#num#	k4
<g/>
–	–	k?
<g/>
1686	#num#	k4
</s>
<s>
Boje	boj	k1gInPc1
na	na	k7c6
Amuru	Amur	k1gInSc6
(	(	kIx(
<g/>
1683	#num#	k4
<g/>
–	–	k?
<g/>
1684	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čchingská	Čchingský	k2eAgFnSc1d1
ofenzíva	ofenzíva	k1gFnSc1
v	v	k7c6
Poamuří	Poamuří	k1gNnSc6
v	v	k7c6
letech	let	k1gInPc6
1683	#num#	k4
<g/>
–	–	k?
<g/>
84	#num#	k4
(	(	kIx(
<g/>
modře	modř	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
mongolský	mongolský	k2eAgInSc4d1
výpad	výpad	k1gInSc4
k	k	k7c3
Tunkinskému	Tunkinský	k2eAgInSc3d1
ostrohu	ostroh	k1gInSc3
roku	rok	k1gInSc2
1684	#num#	k4
(	(	kIx(
<g/>
zeleně	zeleň	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
opevněná	opevněný	k2eAgFnSc1d1
sídla	sídlo	k1gNnPc1
vyznačena	vyznačit	k5eAaPmNgNnP
červeně	červeně	k6eAd1
<g/>
,	,	kIx,
čchingská	čchingskat	k5eAaPmIp3nS
modře	modro	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jaře	jaro	k1gNnSc6
1683	#num#	k4
čchingská	čchingská	k1gFnSc1
vojska	vojsko	k1gNnSc2
začala	začít	k5eAaPmAgFnS
přesun	přesun	k1gInSc4
k	k	k7c3
Amuru	Amur	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandžuové	Mandžu	k1gMnPc1
se	se	k3xPyFc4
usadili	usadit	k5eAaPmAgMnP
v	v	k7c6
ústí	ústí	k1gNnSc6
Zeji	zet	k5eAaImIp1nS
a	a	k8xC
vybudovali	vybudovat	k5eAaPmAgMnP
zde	zde	k6eAd1
pevnost	pevnost	k1gFnSc4
Ajgun	Ajguna	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
ústí	ústí	k1gNnSc2
Kumary	Kumara	k1gFnSc2
stejnojmennou	stejnojmenný	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
Kumara	Kumar	k1gMnSc2
a	a	k8xC
ještě	ještě	k9
jednu	jeden	k4xCgFnSc4
pevnost	pevnost	k1gFnSc4
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červnu	červen	k1gInSc6
Mandžuové	Mandžu	k1gMnPc1
u	u	k7c2
ústí	ústí	k1gNnSc2
Zeji	zet	k5eAaImIp1nS
zajali	zajmout	k5eAaPmAgMnP
většinu	většina	k1gFnSc4
ruského	ruský	k2eAgInSc2d1
oddílu	oddíl	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
plul	plout	k5eAaImAgInS
z	z	k7c2
Albazinu	Albazin	k1gInSc2
vystřídat	vystřídat	k5eAaPmF
posádku	posádka	k1gFnSc4
zejských	zejský	k2eAgInPc2d1
ostrohů	ostroh	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
třech	tři	k4xCgInPc6
ruských	ruský	k2eAgInPc6d1
ostrozích	ostroh	k1gInPc6
na	na	k7c6
Zeje	zet	k5eAaImIp3nS
bylo	být	k5eAaImAgNnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
celkem	celkem	k6eAd1
67	#num#	k4
kozáků	kozák	k1gMnPc2
a	a	k8xC
v	v	k7c6
Albazinu	Albazin	k1gInSc6
něco	něco	k3yInSc4
přes	přes	k7c4
stovku	stovka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
a	a	k8xC
zabajkalských	zabajkalský	k2eAgInPc6d1
ostrozích	ostroh	k1gInPc6
měl	mít	k5eAaImAgMnS
tamní	tamní	k2eAgMnSc1d1
vojevoda	vojevoda	k1gMnSc1
dohromady	dohromady	k6eAd1
dvě	dva	k4xCgFnPc4
stovky	stovka	k1gFnPc1
kozáků	kozák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Velikost	velikost	k1gFnSc1
čchingské	čchingský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
odhadli	odhadnout	k5eAaPmAgMnP
Rusové	Rus	k1gMnPc1
na	na	k7c4
deset	deset	k4xCc4
tisíc	tisíc	k4xCgInPc2
lidí	člověk	k1gMnPc2
na	na	k7c6
300	#num#	k4
lodích	loď	k1gFnPc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
300	#num#	k4
velkými	velký	k2eAgNnPc7d1
i	i	k8xC
malými	malý	k2eAgNnPc7d1
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
1683	#num#	k4
Mandžuové	Mandžu	k1gMnPc1
podnikli	podniknout	k5eAaPmAgMnP
výpad	výpad	k1gInSc4
k	k	k7c3
jezeru	jezero	k1gNnSc3
Dalaj	Dalaj	k1gInSc1
núr	núr	k?
a	a	k8xC
pomocí	pomocí	k7c2
oddílů	oddíl	k1gInPc2
o	o	k7c4
400	#num#	k4
<g/>
–	–	k?
<g/>
500	#num#	k4
mužích	muž	k1gMnPc6
obsadili	obsadit	k5eAaPmAgMnP
ruské	ruský	k2eAgFnPc4d1
ostrohy	ostroha	k1gFnPc4
v	v	k7c6
povodí	povodí	k1gNnSc6
Zeji	zet	k5eAaImIp1nS
<g/>
,	,	kIx,
poslední	poslední	k2eAgInSc1d1
(	(	kIx(
<g/>
Verchnozejský	Verchnozejský	k2eAgInSc1d1
<g/>
)	)	kIx)
v	v	k7c6
únoru	únor	k1gInSc6
1684	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
zajetí	zajetí	k1gNnSc2
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
kolem	kolem	k7c2
stovky	stovka	k1gFnSc2
ruských	ruský	k2eAgMnPc2d1
kozáků	kozák	k1gMnPc2
a	a	k8xC
lovců	lovec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
říjnu	říjen	k1gInSc6
1683	#num#	k4
do	do	k7c2
Albazinu	Albazin	k1gInSc2
dorazili	dorazit	k5eAaPmAgMnP
dva	dva	k4xCgInPc4
ze	z	k7c2
zajatců	zajatec	k1gMnPc2
s	s	k7c7
dopisem	dopis	k1gInSc7
čchingského	čchingský	k1gMnSc2
císaře	císař	k1gMnSc2
s	s	k7c7
tradičními	tradiční	k2eAgInPc7d1
požadavky	požadavek	k1gInPc7
–	–	k?
vydat	vydat	k5eAaPmF
Gantimura	Gantimur	k1gMnSc4
<g/>
,	,	kIx,
vyklidit	vyklidit	k5eAaPmF
Albazin	Albazin	k1gInSc4
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
hrozil	hrozit	k5eAaImAgMnS
válkou	válka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
července	červenec	k1gInSc2
1684	#num#	k4
čchingský	čchingský	k2eAgInSc1d1
jízdní	jízdní	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
obsadil	obsadit	k5eAaPmAgInS
ruskou	ruský	k2eAgFnSc4d1
ves	ves	k1gFnSc4
Čulkovo	Čulkův	k2eAgNnSc1d1
nedaleko	nedaleko	k7c2
Albazinu	Albazin	k1gInSc2
a	a	k8xC
zajal	zajmout	k5eAaPmAgMnS
část	část	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
ruští	ruský	k2eAgMnPc1d1
rolníci	rolník	k1gMnPc1
z	z	k7c2
okolí	okolí	k1gNnSc2
stáhli	stáhnout	k5eAaPmAgMnP
do	do	k7c2
Albazinu	Albazin	k1gInSc2
a	a	k8xC
lovci	lovec	k1gMnPc1
také	také	k9
nevyrazili	vyrazit	k5eNaPmAgMnP
za	za	k7c4
soboly	sobol	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celek	k1gInSc7
se	se	k3xPyFc4
ve	v	k7c6
městě	město	k1gNnSc6
shromáždilo	shromáždit	k5eAaPmAgNnS
120	#num#	k4
služilich	služilicha	k1gFnPc2
ljuděj	ljudět	k5eAaPmRp2nS,k5eAaImRp2nS
(	(	kIx(
<g/>
vojáků	voják	k1gMnPc2
a	a	k8xC
úředníků	úředník	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
97	#num#	k4
rolníků	rolník	k1gMnPc2
a	a	k8xC
250	#num#	k4
ostatních	ostatní	k2eAgFnPc2d1
<g/>
,	,	kIx,
především	především	k9
lovců	lovec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Čchingská	Čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
–	–	k?
přes	přes	k7c4
císařovo	císařův	k2eAgNnSc4d1
zdůrazňování	zdůrazňování	k1gNnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
zmocnit	zmocnit	k5eAaPmF
se	se	k3xPyFc4
sklizně	sklizeň	k1gFnSc2
–	–	k?
neobjevila	objevit	k5eNaPmAgFnS
a	a	k8xC
na	na	k7c4
podzim	podzim	k1gInSc4
Rusové	Rus	k1gMnPc1
překonali	překonat	k5eAaPmAgMnP
obavy	obava	k1gFnPc4
z	z	k7c2
Mandžuů	Mandžu	k1gMnPc2
a	a	k8xC
sklidili	sklidit	k5eAaPmAgMnP
úrodu	úroda	k1gFnSc4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
opožděně	opožděně	k6eAd1
–	–	k?
obilí	obilí	k1gNnSc2
již	již	k6eAd1
z	z	k7c2
poloviny	polovina	k1gFnSc2
vypadalo	vypadat	k5eAaImAgNnS,k5eAaPmAgNnS
z	z	k7c2
klasů	klas	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současně	současně	k6eAd1
s	s	k7c7
událostmi	událost	k1gFnPc7
na	na	k7c6
Amuru	Amur	k1gInSc6
čchingský	čchingský	k1gMnSc1
císař	císař	k1gMnSc1
žádal	žádat	k5eAaImAgMnS
chána	chán	k1gMnSc4
Čichundordže	Čichundordž	k1gFnSc2
o	o	k7c4
útok	útok	k1gInSc4
na	na	k7c4
Selenginsk	Selenginsk	k1gInSc4
a	a	k8xC
Irkutsk	Irkutsk	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
zatím	zatím	k6eAd1
vůdce	vůdce	k1gMnSc1
mírové	mírový	k2eAgFnSc2d1
strany	strana	k1gFnSc2
mezi	mezi	k7c4
Mongoly	Mongol	k1gMnPc4
<g/>
,	,	kIx,
Čichundordžův	Čichundordžův	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
Dzanabadzar	Dzanabadzar	k1gMnSc1
<g/>
,	,	kIx,
navázal	navázat	k5eAaPmAgMnS
kontakt	kontakt	k1gInSc4
s	s	k7c7
Rusy	Rus	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chánovi	chánův	k2eAgMnPc1d1
poslové	posel	k1gMnPc1
v	v	k7c6
dubnu	duben	k1gInSc6
1684	#num#	k4
v	v	k7c6
Selenginsku	Selenginsko	k1gNnSc6
požadovali	požadovat	k5eAaImAgMnP
zpět	zpět	k6eAd1
„	„	k?
<g/>
své	své	k1gNnSc4
<g/>
“	“	k?
Burjaty	Burjat	k1gMnPc4
a	a	k8xC
hrozili	hrozit	k5eAaImAgMnP
čchingským	čchingský	k2eAgInSc7d1
útokem	útok	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
přijali	přijmout	k5eAaPmAgMnP
Mongoly	mongol	k1gInPc4
zdvořile	zdvořile	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
neústupně	ústupně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chán	chán	k1gMnSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
rozhodl	rozhodnout	k5eAaPmAgInS
zachovat	zachovat	k5eAaPmF
neutralitu	neutralita	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
i	i	k8xC
když	když	k8xS
drobné	drobný	k2eAgInPc4d1
nájezdy	nájezd	k1gInPc4
zejména	zejména	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
Tunkinského	Tunkinský	k2eAgInSc2d1
ostrohu	ostroh	k1gInSc2
pokračovaly	pokračovat	k5eAaImAgFnP
i	i	k9
v	v	k7c6
létě	léto	k1gNnSc6
1684	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
je	on	k3xPp3gMnPc4
odráželi	odrážet	k5eAaImAgMnP
za	za	k7c2
pomoci	pomoc	k1gFnSc2
místních	místní	k2eAgMnPc2d1
Burjatů	Burjat	k1gMnPc2
a	a	k8xC
Tunguzů	Tunguz	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reakce	reakce	k1gFnSc1
Moskvy	Moskva	k1gFnSc2
<g/>
:	:	kIx,
posilování	posilování	k1gNnSc3
obrany	obrana	k1gFnSc2
(	(	kIx(
<g/>
1683	#num#	k4
<g/>
–	–	k?
<g/>
1685	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Carská	carský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
reagovala	reagovat	k5eAaBmAgFnS
začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1683	#num#	k4
reorganizací	reorganizace	k1gFnSc7
správy	správa	k1gFnSc2
východní	východní	k2eAgFnSc2d1
Sibiře	Sibiř	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
všechny	všechen	k3xTgInPc1
východosibiřské	východosibiřský	k2eAgInPc1d1
újezdy	újezd	k1gInPc1
<g/>
:	:	kIx,
Jakutský	jakutský	k2eAgInSc1d1
<g/>
,	,	kIx,
Irkutský	irkutský	k2eAgInSc1d1
(	(	kIx(
<g/>
vzniklý	vzniklý	k2eAgInSc1d1
roku	rok	k1gInSc2
1682	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Ilimský	Ilimský	k2eAgInSc4d1
<g/>
,	,	kIx,
Něrčinský	Něrčinský	k2eAgInSc4d1
a	a	k8xC
demonstrativně	demonstrativně	k6eAd1
nově	nově	k6eAd1
zřízený	zřízený	k2eAgInSc4d1
Albazinský	Albazinský	k2eAgInSc4d1
újezd	újezd	k1gInSc4
soustředila	soustředit	k5eAaPmAgFnS
v	v	k7c6
Jenisejském	jenisejský	k2eAgInSc6d1
razrjadu	razrjad	k1gInSc6
(	(	kIx(
<g/>
založeném	založený	k2eAgNnSc6d1
nedávno	nedávno	k6eAd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1677	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
podřídila	podřídit	k5eAaPmAgFnS
novému	nový	k2eAgMnSc3d1
jenisejskému	jenisejský	k2eAgMnSc3d1
vojevodovi	vojevod	k1gMnSc3
Konstantinovi	Konstantin	k1gMnSc3
Ščerbatovovi	Ščerbatova	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
dospěly	dochvít	k5eAaPmAgFnP
zprávy	zpráva	k1gFnPc1
o	o	k7c6
bojích	boj	k1gInPc6
roku	rok	k1gInSc2
1683	#num#	k4
<g/>
,	,	kIx,
přikázala	přikázat	k5eAaPmAgFnS
v	v	k7c6
lednu	leden	k1gInSc6
1684	#num#	k4
tobolskému	tobolský	k2eAgMnSc3d1
vojevodovi	vojevod	k1gMnSc3
Alexeji	Alexej	k1gMnSc3
Golicynovi	Golicyn	k1gMnSc3
sebrat	sebrat	k5eAaPmF
z	z	k7c2
vojáků	voják	k1gMnPc2
na	na	k7c6
západní	západní	k2eAgFnSc6d1
Sibiři	Sibiř	k1gFnSc6
600	#num#	k4
lidí	člověk	k1gMnPc2
a	a	k8xC
na	na	k7c6
jaře	jaro	k1gNnSc6
je	být	k5eAaImIp3nS
poslat	poslat	k5eAaPmF
do	do	k7c2
Jenisejska	Jenisejsk	k1gInSc2
Ščerbatovovi	Ščerbatova	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
k	k	k7c3
nim	on	k3xPp3gInPc3
něl	něl	k?
přidat	přidat	k5eAaPmF
400	#num#	k4
dalších	další	k2eAgMnPc2d1
a	a	k8xC
vyslat	vyslat	k5eAaPmF
za	za	k7c4
Bajkal	Bajkal	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
na	na	k7c6
Sibiři	Sibiř	k1gFnSc6
vyvolalo	vyvolat	k5eAaPmAgNnS
vlnu	vlna	k1gFnSc4
horečné	horečný	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
Albazinu	Albazin	k1gInSc2
byl	být	k5eAaImAgInS
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
poslán	poslán	k2eAgMnSc1d1
Alexej	Alexej	k1gMnSc1
Tolbuzin	Tolbuzin	k1gMnSc1
(	(	kIx(
<g/>
na	na	k7c4
místo	místo	k1gNnSc4
přijel	přijet	k5eAaPmAgMnS
v	v	k7c6
květnu	květen	k1gInSc6
1684	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vyměněni	vyměnit	k5eAaPmNgMnP
byli	být	k5eAaImAgMnP
i	i	k9
ostatní	ostatní	k2eAgMnPc1d1
východosibiřští	východosibiřský	k2eAgMnPc1d1
vojevodové	vojevod	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noví	nový	k2eAgMnPc1d1
náčelníci	náčelník	k1gMnPc1
posilovali	posilovat	k5eAaImAgMnP
opevnění	opevnění	k1gNnSc4
Albazinu	Albazin	k1gInSc2
i	i	k8xC
Něrčinska	Něrčinsko	k1gNnSc2
i	i	k9
dalších	další	k2eAgInPc2d1
ostrohů	ostroh	k1gInPc2
<g/>
;	;	kIx,
od	od	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
1684	#num#	k4
jim	on	k3xPp3gMnPc3
začaly	začít	k5eAaPmAgFnP
přicházet	přicházet	k5eAaImF
posily	posila	k1gFnPc1
a	a	k8xC
výzbroj	výzbroj	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1685	#num#	k4
tak	tak	k6eAd1
bylo	být	k5eAaImAgNnS
v	v	k7c6
důležitých	důležitý	k2eAgInPc6d1
ostrozích	ostroh	k1gInPc6
–	–	k?
Albazinu	Albazino	k1gNnSc6
<g/>
,	,	kIx,
Něrčinsku	Něrčinsko	k1gNnSc6
a	a	k8xC
Irkutsku	Irkutsk	k1gInSc6
–	–	k?
po	po	k7c6
dvou	dva	k4xCgFnPc6
až	až	k6eAd1
čtyřech	čtyři	k4xCgInPc6
dělech	dělo	k1gNnPc6
a	a	k8xC
stovce	stovka	k1gFnSc6
píšťal	píšťala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmíněné	zmíněný	k2eAgFnPc4d1
posily	posila	k1gFnPc4
z	z	k7c2
Tobolska	Tobolsko	k1gNnSc2
a	a	k8xC
Jenisejska	Jenisejsko	k1gNnSc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Afanasije	Afanasije	k1gMnSc2
Bejtona	Bejton	k1gMnSc2
musely	muset	k5eAaImAgInP
koncem	konec	k1gInSc7
roku	rok	k1gInSc2
1684	#num#	k4
děla	dělo	k1gNnSc2
a	a	k8xC
nemalou	malý	k2eNgFnSc4d1
část	část	k1gFnSc4
materiálu	materiál	k1gInSc2
nechat	nechat	k5eAaPmF
na	na	k7c6
zamrzlé	zamrzlý	k2eAgFnSc6d1
Angaře	Angara	k1gFnSc6
a	a	k8xC
v	v	k7c6
lednu	leden	k1gInSc6
1685	#num#	k4
vyrazily	vyrazit	k5eAaPmAgFnP
z	z	k7c2
Ilimska	Ilimsk	k1gInSc2
na	na	k7c4
východ	východ	k1gInSc4
„	„	k?
<g/>
nalehko	nalehko	k6eAd1
<g/>
“	“	k?
bez	bez	k7c2
nich	on	k3xPp3gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
Ščerbakov	Ščerbakov	k1gInSc1
přikázal	přikázat	k5eAaPmAgMnS
podřízeným	podřízený	k2eAgMnSc7d1
<g />
.	.	kIx.
</s>
<s hack="1">
vojevodům	vojevod	k1gMnPc3
posílit	posílit	k5eAaPmF
opevnění	opevnění	k1gNnSc4
ostrohů	ostroh	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
Selenginský	Selenginský	k2eAgInSc1d1
ostroh	ostroh	k1gInSc1
byl	být	k5eAaImAgInS
nově	nově	k6eAd1
vystavěn	vystavět	k5eAaPmNgInS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
nabrat	nabrat	k5eAaPmF
dalších	další	k2eAgInPc2d1
350	#num#	k4
vojáků	voják	k1gMnPc2
z	z	k7c2
místních	místní	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
Moskvě	Moskva	k1gFnSc6
si	se	k3xPyFc3
stěžoval	stěžovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nemají	mít	k5eNaImIp3nP
výcvik	výcvik	k1gInSc4
a	a	k8xC
žádal	žádat	k5eAaImAgMnS
o	o	k7c6
zaslání	zaslání	k1gNnSc6
nejméně	málo	k6eAd3
700	#num#	k4
zkušených	zkušený	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
kterými	který	k3yRgMnPc7,k3yIgMnPc7,k3yQgMnPc7
chtěl	chtít	k5eAaImAgMnS
vyztužit	vyztužit	k5eAaPmF
obranu	obrana	k1gFnSc4
od	od	k7c2
Jenisejska	Jenisejsk	k1gInSc2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
omezila	omezit	k5eAaPmAgFnS
na	na	k7c6
zaslání	zaslání	k1gNnSc6
10	#num#	k4
důstojníků	důstojník	k1gMnPc2
<g/>
,	,	kIx,
20	#num#	k4
dělostřelců	dělostřelec	k1gMnPc2
a	a	k8xC
výzbroje	výzbroj	k1gFnSc2
–	–	k?
10	#num#	k4
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
střelného	střelný	k2eAgInSc2d1
prachu	prach	k1gInSc2
a	a	k8xC
olova	olovo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obležení	obležení	k1gNnSc1
a	a	k8xC
pád	pád	k1gInSc1
Albazinu	Albazin	k1gInSc2
(	(	kIx(
<g/>
1685	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
První	první	k4xOgNnPc1
obléhání	obléhání	k1gNnSc1
Albazinu	Albazin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Čchingská	Čchingský	k2eAgFnSc1d1
ofenzíva	ofenzíva	k1gFnSc1
v	v	k7c6
Poamuří	Poamuří	k1gNnSc6
roku	rok	k1gInSc2
1685	#num#	k4
–	–	k?
obležení	obležení	k1gNnSc6
Albazinu	Albazin	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
dobytí	dobytí	k1gNnSc2
(	(	kIx(
<g/>
modře	modř	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ústup	ústup	k1gInSc1
Rusů	Rus	k1gMnPc2
k	k	k7c3
Něrčinsku	Něrčinsko	k1gNnSc3
(	(	kIx(
<g/>
červeně	červeně	k6eAd1
čárkovaně	čárkovaně	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
návrat	návrat	k1gInSc1
Rusů	Rus	k1gMnPc2
do	do	k7c2
Albazinu	Albazin	k1gInSc2
(	(	kIx(
<g/>
červeně	červeň	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
příchod	příchod	k1gInSc1
Bejtonova	Bejtonův	k2eAgInSc2d1
oddílu	oddíl	k1gInSc2
do	do	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
(	(	kIx(
<g/>
červeně	červeně	k6eAd1
čerchovaně	čerchovaně	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
útoky	útok	k1gInPc1
Mongolů	Mongol	k1gMnPc2
(	(	kIx(
<g/>
zeleně	zeleň	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
nevýrazné	výrazný	k2eNgFnSc6d1
kampani	kampaň	k1gFnSc6
roku	rok	k1gInSc2
1684	#num#	k4
Kchang-si	Kchang-s	k1gMnSc6
odvolal	odvolat	k5eAaPmAgMnS
váhavého	váhavý	k2eAgMnSc4d1
velitele	velitel	k1gMnSc4
čchingské	čchingský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
na	na	k7c6
Amuru	Amur	k1gInSc6
Sabsua	Sabsu	k1gInSc2
a	a	k8xC
v	v	k7c6
únoru	únor	k1gInSc6
1685	#num#	k4
vydal	vydat	k5eAaPmAgInS
edikt	edikt	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
zopakoval	zopakovat	k5eAaPmAgMnS
obvinění	obvinění	k1gNnPc4
Rusů	Rus	k1gMnPc2
z	z	k7c2
expanze	expanze	k1gFnSc2
<g/>
,	,	kIx,
přikázal	přikázat	k5eAaPmAgInS
jejich	jejich	k3xOp3gNnSc4
zničení	zničení	k1gNnSc4
a	a	k8xC
nakázal	nakázat	k5eAaBmAgMnS,k5eAaPmAgMnS
svým	svůj	k3xOyFgMnPc3
vojevůdcům	vojevůdce	k1gMnPc3
poslat	poslat	k5eAaPmF
do	do	k7c2
Albazinu	Albazin	k1gInSc2
ještě	ještě	k9
jedno	jeden	k4xCgNnSc4
poselstvo	poselstvo	k1gNnSc4
s	s	k7c7
výzvou	výzva	k1gFnSc7
stáhnout	stáhnout	k5eAaPmF
se	se	k3xPyFc4
z	z	k7c2
čchingského	čchingský	k2eAgNnSc2d1
území	území	k1gNnSc2
do	do	k7c2
Jakutska	Jakutsk	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
měl	mít	k5eAaImAgInS
potom	potom	k6eAd1
tvořit	tvořit	k5eAaImF
hranici	hranice	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
1685	#num#	k4
čchingský	čchingský	k1gMnSc1
císař	císař	k1gMnSc1
poslal	poslat	k5eAaPmAgMnS
ruskému	ruský	k2eAgMnSc3d1
carovi	car	k1gMnSc3
listy	lista	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
ruštině	ruština	k1gFnSc6
<g/>
,	,	kIx,
latině	latina	k1gFnSc6
a	a	k8xC
čínštině	čínština	k1gFnSc6
<g/>
)	)	kIx)
s	s	k7c7
podmínkami	podmínka	k1gFnPc7
míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruským	ruský	k2eAgInPc3d1
úřadům	úřad	k1gInPc3
v	v	k7c6
Albazinu	Albazin	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Jakutsku	Jakutsk	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
Selenginsku	Selenginsko	k1gNnSc6
a	a	k8xC
v	v	k7c6
Tobolsku	Tobolsek	k1gInSc6
je	on	k3xPp3gNnSc4
předali	předat	k5eAaPmAgMnP
kozáci	kozák	k1gMnPc1
propuštění	propuštění	k1gNnSc6
Čchingy	Čching	k1gInPc4
ze	z	k7c2
zajetí	zajetí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Moskvy	Moskva	k1gFnSc2
první	první	k4xOgFnSc2
z	z	k7c2
listů	list	k1gInPc2
dorazil	dorazit	k5eAaPmAgMnS
v	v	k7c6
listopadu	listopad	k1gInSc6
1685	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listech	list	k1gInPc6
císař	císař	k1gMnSc1
nabízel	nabízet	k5eAaImAgInS
mír	mír	k1gInSc4
a	a	k8xC
obchodní	obchodní	k2eAgInPc4d1
styky	styk	k1gInPc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
bude	být	k5eAaImBp3nS
dohodnuta	dohodnout	k5eAaPmNgFnS
hranice	hranice	k1gFnSc1
u	u	k7c2
Jakutska	Jakutsk	k1gInSc2
a	a	k8xC
Jenisejska	Jenisejsk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
1685	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Zabajkalí	Zabajkalí	k1gNnSc6
množily	množit	k5eAaImAgFnP
zprávy	zpráva	k1gFnPc1
o	o	k7c6
připravovaném	připravovaný	k2eAgInSc6d1
mongolském	mongolský	k2eAgInSc6d1
útoku	útok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1685	#num#	k4
se	se	k3xPyFc4
třísetčlenný	třísetčlenný	k2eAgInSc1d1
jezdecký	jezdecký	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
Mandžuů	Mandžu	k1gMnPc2
objevil	objevit	k5eAaPmAgInS
pod	pod	k7c7
Albazinem	Albazin	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
města	město	k1gNnSc2
zajal	zajmout	k5eAaPmAgMnS
či	či	k8xC
zabil	zabít	k5eAaPmAgMnS
osazenstvo	osazenstvo	k1gNnSc4
jednoho	jeden	k4xCgMnSc4
mlýnu	mlýn	k1gInSc3
a	a	k8xC
přítomné	přítomný	k2eAgInPc1d1
kozáky	kozák	k1gInPc1
a	a	k8xC
odešel	odejít	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
rolníci	rolník	k1gMnPc1
v	v	k7c6
okolí	okolí	k1gNnSc6
Albazinu	Albazin	k1gInSc2
zaseli	zasít	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čchingské	Čchingský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
obléhá	obléhat	k5eAaImIp3nS
Albazin	Albazin	k1gInSc4
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnPc1
čínské	čínský	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
z	z	k7c2
konce	konec	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Začátkem	začátkem	k7c2
června	červen	k1gInSc2
1685	#num#	k4
čchingská	čchingský	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
odehnala	odehnat	k5eAaPmAgFnS
albazinským	albazinský	k2eAgInSc7d1
koně	kůň	k1gMnSc4
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
se	se	k3xPyFc4
hlavní	hlavní	k2eAgFnPc1d1
síly	síla	k1gFnPc1
čchingské	čchingské	k2eAgFnPc2d1
armády	armáda	k1gFnSc2
vylodily	vylodit	k5eAaPmAgFnP
u	u	k7c2
Albazinu	Albazin	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Vedli	vést	k5eAaImAgMnP
je	on	k3xPp3gNnSc4
Pengcun	Pengcun	k1gNnSc4
a	a	k8xC
Langtan	Langtan	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
následujících	následující	k2eAgInPc6d1
dvou	dva	k4xCgInPc6
dnech	den	k1gInPc6
čchingské	čchingský	k1gMnPc4
vojsko	vojsko	k1gNnSc1
obklíčilo	obklíčit	k5eAaPmAgNnS
Albazin	Albazin	k1gInSc4
<g/>
,	,	kIx,
stavělo	stavět	k5eAaImAgNnS
pozice	pozice	k1gFnPc4
pro	pro	k7c4
děla	dělo	k1gNnPc4
a	a	k8xC
od	od	k7c2
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
začalo	začít	k5eAaPmAgNnS
obležení	obležení	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Mandžuové	Mandžu	k1gMnPc1
měli	mít	k5eAaImAgMnP
4200	#num#	k4
mužů	muž	k1gMnPc2
včetně	včetně	k7c2
1200	#num#	k4
nevojáků	nevoják	k1gMnPc2
<g/>
,	,	kIx,
30	#num#	k4
velkých	velká	k1gFnPc2
a	a	k8xC
15	#num#	k4
malých	malý	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
;	;	kIx,
navíc	navíc	k6eAd1
1000	#num#	k4
jezdců	jezdec	k1gMnPc2
přišlo	přijít	k5eAaPmAgNnS
po	po	k7c6
břehu	břeh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
stovka	stovka	k1gFnSc1
Mandžuů	Mandžu	k1gMnPc2
měla	mít	k5eAaImAgFnS
píšťaly	píšťala	k1gFnPc1
<g/>
,	,	kIx,
ukořistěné	ukořistěný	k2eAgFnPc1d1
u	u	k7c2
Rusů	Rus	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Obránců	obránce	k1gMnPc2
Albazinu	Albazina	k1gFnSc4
bylo	být	k5eAaImAgNnS
450	#num#	k4
<g/>
,	,	kIx,
se	s	k7c7
3	#num#	k4
děly	dělo	k1gNnPc7
a	a	k8xC
300	#num#	k4
píšťalami	píšťala	k1gFnPc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
městě	město	k1gNnSc6
bylo	být	k5eAaImAgNnS
i	i	k8xC
několik	několik	k4yIc1
set	sto	k4xCgNnPc2
žen	žena	k1gFnPc2
a	a	k8xC
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
deseti	deset	k4xCc6
dnech	den	k1gInPc6
obležení	obležení	k1gNnSc2
Albazinu	Albazina	k1gFnSc4
Mandžuové	Mandžu	k1gMnPc1
zahájili	zahájit	k5eAaPmAgMnP
všeobecný	všeobecný	k2eAgInSc4d1
útok	útok	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Čchingská	Čchingský	k2eAgFnSc1d1
dělostřelba	dělostřelba	k1gFnSc1
ničila	ničit	k5eAaImAgFnS
palisádu	palisáda	k1gFnSc4
<g/>
,	,	kIx,
od	od	k7c2
zápalných	zápalný	k2eAgInPc2d1
šípů	šíp	k1gInPc2
shořely	shořet	k5eAaPmAgFnP
sýpky	sýpka	k1gFnPc1
<g/>
,	,	kIx,
část	část	k1gFnSc1
budov	budova	k1gFnPc2
a	a	k8xC
kostel	kostel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
den	den	k1gInSc4
bojů	boj	k1gInPc2
zahynula	zahynout	k5eAaPmAgFnS
více	hodně	k6eAd2
než	než	k8xS
stovka	stovka	k1gFnSc1
obránců	obránce	k1gMnPc2
a	a	k8xC
skoro	skoro	k6eAd1
150	#num#	k4
útočníků	útočník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
také	také	k6eAd1
zničeno	zničit	k5eAaPmNgNnS
jedno	jeden	k4xCgNnSc1
ze	z	k7c2
tří	tři	k4xCgNnPc2
ruských	ruský	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Rusům	Rus	k1gMnPc3
došly	dojít	k5eAaPmAgInP
zásoby	zásoba	k1gFnPc4
střelného	střelný	k2eAgInSc2d1
prachu	prach	k1gInSc2
i	i	k9
kulí	kulit	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
Vojevoda	vojevoda	k1gMnSc1
Tolbuzin	Tolbuzin	k1gMnSc1
proto	proto	k8xC
požádal	požádat	k5eAaPmAgMnS
čchingského	čchingský	k2eAgMnSc4d1
velitele	velitel	k1gMnSc4
Langtana	Langtan	k1gMnSc2
o	o	k7c4
svolení	svolení	k1gNnSc4
k	k	k7c3
odchodu	odchod	k1gInSc3
ruské	ruský	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
i	i	k8xC
civilistů	civilista	k1gMnPc2
z	z	k7c2
Albazinu	Albazin	k1gInSc2
do	do	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
Čchingové	Čchingový	k2eAgFnPc4d1
přijali	přijmout	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jen	jen	k9
krátce	krátce	k6eAd1
před	před	k7c7
Albazinci	Albazinec	k1gMnPc7
<g/>
,	,	kIx,
9	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
<g/>
,	,	kIx,
do	do	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
konečně	konečně	k6eAd1
dospěl	dochvít	k5eAaPmAgInS
Bejton	Bejton	k1gInSc1
se	s	k7c7
447	#num#	k4
vojáky	voják	k1gMnPc7
a	a	k8xC
třemi	tři	k4xCgNnPc7
lehkými	lehký	k2eAgNnPc7d1
děly	dělo	k1gNnPc7
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc4
oddílu	oddíl	k1gInSc2
–	–	k?
126	#num#	k4
lidí	člověk	k1gMnPc2
s	s	k7c7
děly	dělo	k1gNnPc7
a	a	k8xC
zásobami	zásoba	k1gFnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
zůstali	zůstat	k5eAaPmAgMnP
na	na	k7c6
Angaře	Angara	k1gFnSc6
–	–	k?
dorazil	dorazit	k5eAaPmAgMnS
později	pozdě	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
dobytí	dobytí	k1gNnSc6
Albazinu	Albazin	k1gInSc2
zaslal	zaslat	k5eAaPmAgMnS
císař	císař	k1gMnSc1
list	lista	k1gFnPc2
na	na	k7c4
Udský	Udský	k2eAgInSc4d1
ostroh	ostroh	k1gInSc4
s	s	k7c7
požadavkem	požadavek	k1gInSc7
vyklidit	vyklidit	k5eAaPmF
řeku	řeka	k1gFnSc4
Udu	Udu	k1gFnSc2
a	a	k8xC
stáhnout	stáhnout	k5eAaPmF
se	se	k3xPyFc4
na	na	k7c6
„	„	k?
<g/>
hranici	hranice	k1gFnSc6
<g/>
“	“	k?
do	do	k7c2
Jakutska	Jakutsk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandžuové	Mandžu	k1gMnPc1
se	se	k3xPyFc4
objevili	objevit	k5eAaPmAgMnP
i	i	k9
na	na	k7c6
horním	horní	k2eAgInSc6d1
toku	tok	k1gInSc6
Udy	Udy	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znepokojený	znepokojený	k2eAgMnSc1d1
jakutský	jakutský	k2eAgMnSc1d1
vojevoda	vojevoda	k1gMnSc1
začal	začít	k5eAaPmAgMnS
posilovat	posilovat	k5eAaImF
opevnění	opevnění	k1gNnSc4
Jakutska	Jakutsk	k1gInSc2
a	a	k8xC
domorodci	domorodec	k1gMnPc1
z	z	k7c2
pobřeží	pobřeží	k1gNnSc2
Ochotského	ochotský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
žádali	žádat	k5eAaImAgMnP
v	v	k7c6
Ochotském	ochotský	k2eAgInSc6d1
ostrohu	ostroh	k1gInSc6
o	o	k7c4
ochranu	ochrana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mongolské	mongolský	k2eAgInPc4d1
útoky	útok	k1gInPc4
na	na	k7c4
Rusy	Rus	k1gMnPc4
(	(	kIx(
<g/>
1685	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
[[	[[	k?
<g/>
file	file	k1gNnPc3
<g/>
:	:	kIx,
<g/>
Outline	Outlin	k1gInSc5
Map	mapa	k1gFnPc2
of	of	k?
Zabaikalsky	Zabaikalsky	k1gMnPc1
Krai	Kra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
360	#num#	k4
<g/>
px	px	k?
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
<g/>
|	|	kIx~
<g/>
{{{	{{{	k?
<g/>
alt	alt	k1gInSc1
<g/>
}}}	}}}	k?
<g/>
|	|	kIx~
<g/>
]]	]]	k?
<g/>
Verchněangarsk	Verchněangarsk	k1gInSc1
</s>
<s>
Barguzinsk	Barguzinsk	k1gInSc1
</s>
<s>
Telembinsk	Telembinsk	k1gInSc1
</s>
<s>
Irgensk	Irgensk	k1gInSc1
</s>
<s>
Něrčinsk	Něrčinsk	k1gInSc1
</s>
<s>
Argunsk	Argunsk	k1gInSc1
</s>
<s>
Jeravninsk	Jeravninsk	k1gInSc1
</s>
<s>
Ruské	ruský	k2eAgFnPc1d1
ostrohy	ostroha	k1gFnPc1
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
Zabajkalsku	Zabajkalsko	k1gNnSc6
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Zabajkalský	zabajkalský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
a	a	k8xC
okolí	okolí	k1gNnSc1
<g/>
)	)	kIx)
od	od	k7c2
40	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
:	:	kIx,
–	–	k?
založeno	založit	k5eAaPmNgNnS
1647	#num#	k4
<g/>
–	–	k?
<g/>
48	#num#	k4
<g/>
;	;	kIx,
–	–	k?
založeno	založit	k5eAaPmNgNnS
1653	#num#	k4
<g/>
–	–	k?
<g/>
58	#num#	k4
–	–	k?
založeno	založen	k2eAgNnSc1d1
1675	#num#	k4
<g/>
;	;	kIx,
–	–	k?
založeno	založit	k5eAaPmNgNnS
1681	#num#	k4
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
1685	#num#	k4
dorazil	dorazit	k5eAaPmAgInS
Bejtonův	Bejtonův	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
do	do	k7c2
Udinsku	Udinsko	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
zde	zde	k6eAd1
se	se	k3xPyFc4
měsíc	měsíc	k1gInSc4
zdržel	zdržet	k5eAaPmAgInS
<g/>
,	,	kIx,
protože	protože	k8xS
dvoutisícový	dvoutisícový	k2eAgInSc4d1
mongolský	mongolský	k2eAgInSc4d1
oddíl	oddíl	k1gInSc4
mu	on	k3xPp3gMnSc3
odehnal	odehnat	k5eAaPmAgMnS
koně	kůň	k1gMnSc4
a	a	k8xC
tažná	tažný	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útočníky	útočník	k1gMnPc7
se	se	k3xPyFc4
vydalo	vydat	k5eAaPmAgNnS
pronásledovat	pronásledovat	k5eAaImF
přes	přes	k7c4
400	#num#	k4
kozáků	kozák	k1gInPc2
a	a	k8xC
necelé	celý	k2eNgFnPc1d1
tři	tři	k4xCgFnPc1
stovky	stovka	k1gFnPc1
místních	místní	k2eAgMnPc2d1
lovců	lovec	k1gMnPc2
<g/>
,	,	kIx,
potulných	potulný	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
a	a	k8xC
Tunguzů	Tunguz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
na	na	k7c6
Mongolech	Mongol	k1gMnPc6
vydobýt	vydobýt	k5eAaPmF
část	část	k1gFnSc1
koní	kůň	k1gMnPc2
<g/>
,	,	kIx,
150	#num#	k4
hlav	hlava	k1gFnPc2
skotu	skot	k1gInSc2
a	a	k8xC
tisícovku	tisícovka	k1gFnSc4
ovcí	ovce	k1gFnPc2
a	a	k8xC
beze	beze	k7c2
ztrát	ztráta	k1gFnPc2
se	se	k3xPyFc4
vrátit	vrátit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
1685	#num#	k4
v	v	k7c6
Irkutsku	Irkutsk	k1gInSc6
Čichundordžovi	Čichundordžův	k2eAgMnPc1d1
poslové	posel	k1gMnPc1
opět	opět	k6eAd1
požadovali	požadovat	k5eAaImAgMnP
zpět	zpět	k6eAd1
„	„	k?
<g/>
své	své	k1gNnSc4
<g/>
“	“	k?
Burjaty	Burjat	k1gMnPc4
a	a	k8xC
hrozili	hrozit	k5eAaImAgMnP
čchingským	čchingský	k2eAgInSc7d1
útokem	útok	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
Mongolové	Mongol	k1gMnPc1
zaútočili	zaútočit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
června	červen	k1gInSc2
1685	#num#	k4
oblehli	oblehnout	k5eAaPmAgMnP
Selenginsk	Selenginsk	k1gInSc4
a	a	k8xC
blokovali	blokovat	k5eAaImAgMnP
Udinsk	Udinsk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusové	k2eAgInPc2d1
se	se	k3xPyFc4
Mongolů	mongol	k1gInPc2
nebáli	bát	k5eNaImAgMnP
<g/>
,	,	kIx,
obávali	obávat	k5eAaImAgMnP
se	se	k3xPyFc4
však	však	k9
mandžuské	mandžuský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mongolsko-čchingská	Mongolsko-čchingský	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
omezila	omezit	k5eAaPmAgFnS
na	na	k7c6
zaslání	zaslání	k1gNnSc3
nejméně	málo	k6eAd3
pěti	pět	k4xCc2
tisíc	tisíc	k4xCgInPc2
mongolských	mongolský	k2eAgMnPc2d1
jezdců	jezdec	k1gMnPc2
k	k	k7c3
výcviku	výcvik	k1gInSc3
u	u	k7c2
Pekingu	Peking	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
také	také	k9
mongolské	mongolský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
Cecen-nojona	Cecen-nojona	k1gFnSc1
zaútočilo	zaútočit	k5eAaPmAgNnS
na	na	k7c4
Tunkinský	Tunkinský	k2eAgInSc4d1
ostroh	ostroh	k1gInSc4
osazený	osazený	k2eAgInSc4d1
43	#num#	k4
kozáky	kozák	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
třídenních	třídenní	k2eAgFnPc6d1
srážkách	srážka	k1gFnPc6
zahynuli	zahynout	k5eAaPmAgMnP
tři	tři	k4xCgMnPc1
kozáci	kozák	k1gMnPc1
a	a	k8xC
několik	několik	k4yIc1
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
do	do	k7c2
zajetí	zajetí	k1gNnSc2
<g/>
,	,	kIx,
Mongolové	Mongol	k1gMnPc1
se	se	k3xPyFc4
stáhli	stáhnout	k5eAaPmAgMnP
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
přiblížila	přiblížit	k5eAaPmAgFnS
ruská	ruský	k2eAgFnSc1d1
posila	posila	k1gFnSc1
z	z	k7c2
Irkutska	Irkutsk	k1gInSc2
o	o	k7c4
120	#num#	k4
mužích	muž	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dzanabadzar	Dzanabadzar	k1gInSc1
poté	poté	k6eAd1
své	svůj	k3xOyFgInPc4
oddíly	oddíl	k1gInPc4
stáhl	stáhnout	k5eAaPmAgMnS
z	z	k7c2
ruského	ruský	k2eAgNnSc2d1
území	území	k1gNnSc2
a	a	k8xC
pustil	pustit	k5eAaPmAgMnS
na	na	k7c4
svobodu	svoboda	k1gFnSc4
ruské	ruský	k2eAgMnPc4d1
zajatce	zajatec	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
vymohl	vymoct	k5eAaPmAgInS
na	na	k7c6
Cecen-nojonovi	Cecen-nojon	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
1685	#num#	k4
Mongolové	Mongol	k1gMnPc1
útočili	útočit	k5eAaImAgMnP
i	i	k9
v	v	k7c4
okolí	okolí	k1gNnSc4
Něrčinsku	Něrčinsko	k1gNnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
odháněli	odhánět	k5eAaImAgMnP
dobytek	dobytek	k1gInSc4
a	a	k8xC
zastrašovali	zastrašovat	k5eAaImAgMnP
domorodce	domorodec	k1gMnSc4
<g/>
;	;	kIx,
v	v	k7c6
červenci	červenec	k1gInSc6
až	až	k8xS
srpnu	srpen	k1gInSc6
1685	#num#	k4
obléhali	obléhat	k5eAaImAgMnP
Argunský	Argunský	k2eAgInSc4d1
ostroh	ostroh	k1gInSc4
<g/>
,	,	kIx,
útočili	útočit	k5eAaImAgMnP
i	i	k9
na	na	k7c4
Telembinský	Telembinský	k2eAgInSc4d1
ostroh	ostroh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Zabajkalí	Zabajkalí	k1gNnSc6
Mongolové	Mongol	k1gMnPc1
odešli	odejít	k5eAaPmAgMnP
od	od	k7c2
Selenginsku	Selenginsko	k1gNnSc6
a	a	k8xC
Udinsku	Udinsko	k1gNnSc6
<g/>
,	,	kIx,
neklid	neklid	k1gInSc4
však	však	k9
trval	trvat	k5eAaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obnovení	obnovení	k1gNnSc1
Albazinu	Albazin	k1gInSc2
(	(	kIx(
<g/>
1685	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
července	červenec	k1gInSc2
1685	#num#	k4
ruský	ruský	k2eAgInSc1d1
průzkumný	průzkumný	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
zjistil	zjistit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
Mandžuové	Mandžu	k1gMnPc1
opustili	opustit	k5eAaPmAgMnP
Albazin	Albazin	k1gInSc4
a	a	k8xC
odešli	odejít	k5eAaPmAgMnP
k	k	k7c3
Nonu	Nonum	k1gNnSc3
a	a	k8xC
dále	daleko	k6eAd2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
;	;	kIx,
pouze	pouze	k6eAd1
v	v	k7c4
Ajgunu	Ajguna	k1gFnSc4
nechali	nechat	k5eAaPmAgMnP
nevelký	velký	k2eNgInSc4d1
oddíl	oddíl	k1gInSc4
o	o	k7c4
500	#num#	k4
mužích	muž	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stáhli	stáhnout	k5eAaPmAgMnP
se	se	k3xPyFc4
tak	tak	k9
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
nestačili	stačit	k5eNaBmAgMnP
zničit	zničit	k5eAaPmF
úrodu	úroda	k1gFnSc4
na	na	k7c6
polích	pole	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
Sklizeň	sklizeň	k1gFnSc1
z	z	k7c2
polí	pole	k1gFnPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Albazinu	Albazin	k1gInSc2
byla	být	k5eAaImAgFnS
rozhodující	rozhodující	k2eAgFnSc1d1
pro	pro	k7c4
udržení	udržení	k1gNnSc4
města	město	k1gNnSc2
<g/>
,	,	kIx,
zásobování	zásobování	k1gNnSc2
velké	velký	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
z	z	k7c2
Irkutska	Irkutsk	k1gInSc2
by	by	kYmCp3nS
totiž	totiž	k9
bylo	být	k5eAaImAgNnS
velmi	velmi	k6eAd1
obtížné	obtížný	k2eAgNnSc1d1
až	až	k8xS
nereálné	reálný	k2eNgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
Vlasov	Vlasov	k1gInSc1
neprodleně	prodleně	k6eNd1
vyslal	vyslat	k5eAaPmAgInS
Tolbuzina	Tolbuzin	k1gMnSc4
<g/>
,	,	kIx,
Bejtona	Bejton	k1gMnSc4
a	a	k8xC
většinu	většina	k1gFnSc4
svých	svůj	k3xOyFgMnPc2
kozáků	kozák	k1gMnPc2
do	do	k7c2
Albazinu	Albazin	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c4
podzim	podzim	k1gInSc4
bylo	být	k5eAaImAgNnS
už	už	k9
514	#num#	k4
kozáků	kozák	k1gMnPc2
a	a	k8xC
155	#num#	k4
rolníků	rolník	k1gMnPc2
a	a	k8xC
lovců	lovec	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
zůstalo	zůstat	k5eAaPmAgNnS
jen	jen	k6eAd1
173	#num#	k4
kozáků	kozák	k1gMnPc2
a	a	k8xC
úředníků	úředník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tolbuzinovým	Tolbuzinův	k2eAgInSc7d1
lidem	lid	k1gInSc7
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
sklidit	sklidit	k5eAaPmF
většinu	většina	k1gFnSc4
úrody	úroda	k1gFnSc2
<g/>
,	,	kIx,
současně	současně	k6eAd1
stavěli	stavět	k5eAaImAgMnP
nové	nový	k2eAgNnSc4d1
<g/>
,	,	kIx,
lepší	dobrý	k2eAgNnSc4d2
opevnění	opevnění	k1gNnSc4
Albazinu	Albazin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1686	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
Albazinu	Albazino	k1gNnSc6
už	už	k6eAd1
725	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
a	a	k8xC
okolních	okolní	k2eAgInPc6d1
ostrozích	ostroh	k1gInPc6
340	#num#	k4
kozáků	kozák	k1gMnPc2
a	a	k8xC
stovka	stovka	k1gFnSc1
lovců	lovec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mandžuové	Mandžu	k1gMnPc1
rychle	rychle	k6eAd1
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Rusové	Rus	k1gMnPc1
vrátili	vrátit	k5eAaPmAgMnP
<g/>
,	,	kIx,
a	a	k8xC
vysílali	vysílat	k5eAaImAgMnP
proti	proti	k7c3
nim	on	k3xPp3gInPc3
jízdní	jízdní	k2eAgInPc4d1
oddíly	oddíl	k1gInPc4
a	a	k8xC
útočili	útočit	k5eAaImAgMnP
na	na	k7c4
osady	osada	k1gFnPc4
v	v	k7c6
okolí	okolí	k1gNnSc6
pevnosti	pevnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tolbuzin	Tolbuzin	k1gMnSc1
zorganizoval	zorganizovat	k5eAaPmAgMnS
aktivní	aktivní	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
pomocí	pomocí	k7c2
kozáckého	kozácký	k2eAgNnSc2d1
jezdectva	jezdectvo	k1gNnSc2
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Bejtona	Bejton	k1gMnSc2
<g/>
,	,	kIx,
od	od	k7c2
října	říjen	k1gInSc2
1685	#num#	k4
do	do	k7c2
jara	jaro	k1gNnSc2
1686	#num#	k4
tak	tak	k9
mezi	mezi	k7c7
Albazinem	Albazin	k1gInSc7
a	a	k8xC
Ajgunem	Ajgun	k1gInSc7
probíhaly	probíhat	k5eAaImAgFnP
srážky	srážka	k1gFnPc1
mezi	mezi	k7c7
Mandžuy	Mandžu	k1gMnPc7
a	a	k8xC
kozáky	kozák	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
se	se	k3xPyFc4
Mandžuové	Mandžu	k1gMnPc1
stáhli	stáhnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
ruští	ruský	k2eAgMnPc1d1
rolníci	rolník	k1gMnPc1
proto	proto	k8xC
mohli	moct	k5eAaImAgMnP
obnovit	obnovit	k5eAaPmF
některé	některý	k3yIgFnPc4
vsi	ves	k1gFnPc4
<g/>
,	,	kIx,
dovést	dovést	k5eAaPmF
dobytek	dobytek	k1gInSc4
z	z	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
a	a	k8xC
zasít	zasít	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhé	druhý	k4xOgNnSc1
obležení	obležení	k1gNnSc1
Albazinu	Albazin	k1gInSc2
(	(	kIx(
<g/>
1686	#num#	k4
<g/>
–	–	k?
<g/>
1687	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Druhé	druhý	k4xOgNnSc1
obléhání	obléhání	k1gNnSc1
Albazinu	Albazin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Obležení	obležení	k1gNnSc1
Albazinu	Albazina	k1gFnSc4
čchingským	čchingský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
9	#num#	k4
<g/>
]	]	kIx)
Nizozemská	nizozemský	k2eAgFnSc1d1
rytina	rytina	k1gFnSc1
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
1686	#num#	k4
Mandžuové	Mandžu	k1gMnPc1
vedení	vedení	k1gNnPc2
Sabsuem	Sabsuum	k1gNnSc7
a	a	k8xC
Langtanem	Langtan	k1gInSc7
vytáhli	vytáhnout	k5eAaPmAgMnP
na	na	k7c6
Albazin	Albazin	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Kozácké	kozácký	k2eAgFnPc1d1
hlídky	hlídka	k1gFnPc1
pochod	pochod	k1gInSc4
nepřátelské	přátelský	k2eNgFnSc2d1
armády	armáda	k1gFnSc2
včas	včas	k6eAd1
zjistily	zjistit	k5eAaPmAgFnP
a	a	k8xC
varovaly	varovat	k5eAaImAgFnP
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
skrylo	skrýt	k5eAaPmAgNnS
v	v	k7c6
pevnosti	pevnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ní	on	k3xPp3gFnSc6
měl	mít	k5eAaImAgMnS
Tolbuzin	Tolbuzin	k1gMnSc1
826	#num#	k4
obránců	obránce	k1gMnPc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc4
moždíř	moždíř	k1gInSc4
a	a	k8xC
osm	osm	k4xCc4
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
dostatek	dostatek	k1gInSc4
prachu	prach	k1gInSc2
a	a	k8xC
olova	olovo	k1gNnSc2
<g/>
,	,	kIx,
zásobu	zásoba	k1gFnSc4
obilí	obilí	k1gNnSc2
do	do	k7c2
léta	léto	k1gNnSc2
1687	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obléhatelé	obléhatel	k1gMnPc1
připluli	připlout	k5eAaPmAgMnP
k	k	k7c3
Albazinu	Albazin	k1gInSc3
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
na	na	k7c6
150	#num#	k4
lodích	loď	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měli	mít	k5eAaImAgMnP
do	do	k7c2
5	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
až	až	k9
6	#num#	k4
500	#num#	k4
mužů	muž	k1gMnPc2
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
se	s	k7c7
40	#num#	k4
děly	dělo	k1gNnPc7
<g/>
,	,	kIx,
velel	velet	k5eAaImAgInS
jim	on	k3xPp3gMnPc3
opět	opět	k5eAaPmF
Langtan	Langtan	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
dobýt	dobýt	k5eAaPmF
Albazin	Albazin	k1gInSc4
a	a	k8xC
bez	bez	k7c2
odkladu	odklad	k1gInSc2
i	i	k8xC
Něrčinsk	Něrčinsk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
pochodu	pochod	k1gInSc3
na	na	k7c4
něj	on	k3xPp3gMnSc4
měl	mít	k5eAaImAgInS
3	#num#	k4
000	#num#	k4
koní	kůň	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
týdnu	týden	k1gInSc6
bojů	boj	k1gInPc2
byl	být	k5eAaImAgInS
Tolbuzin	Tolbuzin	k1gInSc1
raněn	ranit	k5eAaPmNgInS
a	a	k8xC
po	po	k7c6
několika	několik	k4yIc6
dnech	den	k1gInPc6
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
velení	velení	k1gNnSc1
po	po	k7c6
něm	on	k3xPp3gInSc6
převzal	převzít	k5eAaPmAgInS
Bejton	Bejton	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
se	se	k3xPyFc4
Rusové	Rus	k1gMnPc1
ubránili	ubránit	k5eAaPmAgMnP
náporu	nápor	k1gInSc6
prvních	první	k4xOgInPc2
dní	den	k1gInPc2
útoku	útok	k1gInSc2
<g/>
,	,	kIx,
císař	císař	k1gMnSc1
Kchang-si	Kchang-se	k1gFnSc4
svolil	svolit	k5eAaPmAgMnS
k	k	k7c3
dlouhodobému	dlouhodobý	k2eAgNnSc3d1
obléhání	obléhání	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
října	říjen	k1gInSc2
obránci	obránce	k1gMnPc1
provedli	provést	k5eAaPmAgMnP
pět	pět	k4xCc4
výpadů	výpad	k1gInPc2
<g/>
,	,	kIx,
zabili	zabít	k5eAaPmAgMnP
na	na	k7c4
150	#num#	k4
nepřátel	nepřítel	k1gMnPc2
a	a	k8xC
ztratili	ztratit	k5eAaPmAgMnP
65	#num#	k4
mužů	muž	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandžuové	Mandžu	k1gMnPc1
zatím	zatím	k6eAd1
obehnali	obehnat	k5eAaPmAgMnP
pevnost	pevnost	k1gFnSc1
valy	val	k1gInPc4
<g/>
,	,	kIx,
z	z	k7c2
týlu	týl	k1gInSc2
zajištěnými	zajištěný	k2eAgInPc7d1
zákopy	zákop	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
protějším	protější	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Amuru	Amur	k1gInSc2
vybudovali	vybudovat	k5eAaPmAgMnP
valem	valem	k6eAd1
opevněný	opevněný	k2eAgInSc4d1
tábor	tábor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
září	září	k1gNnSc4
Čchingové	Čchingový	k2eAgNnSc4d1
znova	znova	k6eAd1
podnikli	podniknout	k5eAaPmAgMnP
útok	útok	k1gInSc4
–	–	k?
neúspěšně	úspěšně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neuspěl	uspět	k5eNaPmAgMnS
ani	ani	k9
pokus	pokus	k1gInSc4
podkopat	podkopat	k5eAaPmF
se	se	k3xPyFc4
pod	pod	k7c4
val	val	k1gInSc4
<g/>
,	,	kIx,
podkop	podkop	k1gInSc4
zničili	zničit	k5eAaPmAgMnP
kozáci	kozák	k1gMnPc1
při	při	k7c6
výpadu	výpad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
měl	mít	k5eAaImAgInS
Langtan	Langtan	k1gInSc1
už	už	k6eAd1
10	#num#	k4
tisíc	tisíc	k4xCgInPc2
mužů	muž	k1gMnPc2
a	a	k8xC
zaútočil	zaútočit	k5eAaPmAgMnS
znovu	znovu	k6eAd1
<g/>
,	,	kIx,
k	k	k7c3
útoku	útok	k1gInSc3
připravil	připravit	k5eAaPmAgMnS
dvě	dva	k4xCgFnPc4
dřevěné	dřevěný	k2eAgFnPc4d1
hradby	hradba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kozáci	Kozák	k1gMnPc1
<g/>
,	,	kIx,
obávající	obávající	k2eAgFnPc1d1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gNnSc2
přitažení	přitažení	k1gNnSc2
k	k	k7c3
pevnosti	pevnost	k1gFnSc3
a	a	k8xC
zapálení	zapálení	k1gNnSc3
<g/>
,	,	kIx,
jednu	jeden	k4xCgFnSc4
zapálili	zapálit	k5eAaPmAgMnP
při	při	k7c6
výpadu	výpad	k1gInSc6
a	a	k8xC
druhou	druhý	k4xOgFnSc7
vyhodili	vyhodit	k5eAaPmAgMnP
do	do	k7c2
povětří	povětří	k1gNnSc2
z	z	k7c2
podkopu	podkop	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
Mandžuové	Mandžu	k1gMnPc1
„	„	k?
<g/>
zametali	zametat	k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
“	“	k?
pevnost	pevnost	k1gFnSc4
dřevěnými	dřevěný	k2eAgNnPc7d1
poleny	poleno	k1gNnPc7
z	z	k7c2
katapultů	katapult	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
což	což	k3yQnSc1,k3yRnSc1
Rusům	Rus	k1gMnPc3
řešilo	řešit	k5eAaImAgNnS
nedostatek	nedostatek	k1gInSc4
palivového	palivový	k2eAgNnSc2d1
dříví	dříví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
až	až	k8xS
listopadu	listopad	k1gInSc6
zahynulo	zahynout	k5eAaPmAgNnS
přes	přes	k7c4
100	#num#	k4
obránců	obránce	k1gMnPc2
při	při	k7c6
výpadech	výpad	k1gInPc6
a	a	k8xC
bombardování	bombardování	k1gNnSc6
<g/>
,	,	kIx,
přes	přes	k7c4
500	#num#	k4
dalších	další	k2eAgMnPc2d1
zemřelo	zemřít	k5eAaPmAgNnS
od	od	k7c2
kurdějí	kurděje	k1gFnPc2
a	a	k8xC
naživu	naživu	k6eAd1
zůstalo	zůstat	k5eAaPmAgNnS
jen	jen	k9
150	#num#	k4
Rusů	Rus	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
však	však	k9
byli	být	k5eAaImAgMnP
kvůli	kvůli	k7c3
kurdějím	kurděje	k1gFnPc3
ze	z	k7c2
dvou	dva	k4xCgFnPc2
třetin	třetina	k1gFnPc2
neschopni	schopen	k2eNgMnPc1d1
boje	boj	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Čchingská	Čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
v	v	k7c6
bojích	boj	k1gInPc6
a	a	k8xC
následkem	následkem	k7c2
hladovění	hladovění	k1gNnSc2
a	a	k8xC
nemocí	nemoc	k1gFnPc2
přišla	přijít	k5eAaPmAgFnS
o	o	k7c4
dva	dva	k4xCgInPc4
a	a	k8xC
půl	půl	k1xP
tisíce	tisíc	k4xCgInSc2
vojáků	voják	k1gMnPc2
a	a	k8xC
mnoho	mnoho	k6eAd1
nevojenského	vojenský	k2eNgInSc2d1
personálu	personál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reakce	reakce	k1gFnSc1
Moskvy	Moskva	k1gFnSc2
<g/>
:	:	kIx,
Golovinův	Golovinův	k2eAgInSc1d1
pochod	pochod	k1gInSc1
s	s	k7c7
posilami	posila	k1gFnPc7
(	(	kIx(
<g/>
1686	#num#	k4
<g/>
–	–	k?
<g/>
1687	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vojáci	voják	k1gMnPc1
moskevských	moskevský	k2eAgInPc2d1
střeleckých	střelecký	k2eAgInPc2d1
pluků	pluk	k1gInPc2
<g/>
,	,	kIx,
rok	rok	k1gInSc4
1674	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrace	ilustrace	k1gFnSc1
z	z	k7c2
Istoričeskoje	Istoričeskoj	k1gInSc2
opisanije	opisanít	k5eAaPmIp3nS
oděždy	oděždy	k6eAd1
i	i	k9
vooruženija	vooruženija	k6eAd1
rossijskich	rossijskich	k1gInSc1
vojsk	vojsko	k1gNnPc2
(	(	kIx(
<g/>
Historický	historický	k2eAgInSc1d1
popis	popis	k1gInSc1
oděvu	oděv	k1gInSc2
a	a	k8xC
výzbroje	výzbroj	k1gFnSc2
ruských	ruský	k2eAgFnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
,	,	kIx,
1841	#num#	k4
<g/>
–	–	k?
<g/>
62	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1685	#num#	k4
ruská	ruský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
o	o	k7c4
posílení	posílení	k1gNnSc4
vojsk	vojsko	k1gNnPc2
v	v	k7c6
Zabajkalí	Zabajkalí	k1gNnSc6
500	#num#	k4
střelci	střelec	k1gMnPc7
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
a	a	k8xC
1	#num#	k4
400	#num#	k4
ze	z	k7c2
Sibiře	Sibiř	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
v	v	k7c6
lednu	leden	k1gInSc6
1686	#num#	k4
vypravila	vypravit	k5eAaPmAgFnS
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
1	#num#	k4
000	#num#	k4
píšťal	píšťala	k1gFnPc2
<g/>
,	,	kIx,
20	#num#	k4
děl	dělo	k1gNnPc2
a	a	k8xC
těžký	těžký	k2eAgInSc1d1
moždíř	moždíř	k1gInSc1
a	a	k8xC
značné	značný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
střelného	střelný	k2eAgInSc2d1
prachu	prach	k1gInSc2
<g/>
,	,	kIx,
olova	olovo	k1gNnSc2
<g/>
,	,	kIx,
dělových	dělový	k2eAgMnPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
koulí	koule	k1gFnPc2
a	a	k8xC
granátů	granát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
Zajištění	zajištění	k1gNnSc4
přesunu	přesun	k1gInSc2
takového	takový	k3xDgNnSc2
množství	množství	k1gNnSc2
vojáků	voják	k1gMnPc2
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
sibiřské	sibiřský	k2eAgInPc4d1
úřady	úřad	k1gInPc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
mimořádným	mimořádný	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
Rozhodnutí	rozhodnutí	k1gNnSc1
znamenalo	znamenat	k5eAaImAgNnS
značný	značný	k2eAgInSc4d1
růst	růst	k1gInSc4
sibiřských	sibiřský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
<g/>
:	:	kIx,
k	k	k7c3
roku	rok	k1gInSc3
1688	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
celé	celý	k2eAgFnSc6d1
Sibiři	Sibiř	k1gFnSc6
7	#num#	k4
215	#num#	k4
služilich	služilicha	k1gFnPc2
ljuděj	ljudět	k5eAaImRp2nS,k5eAaPmRp2nS
(	(	kIx(
<g/>
vojáků	voják	k1gMnPc2
i	i	k8xC
úředníků	úředník	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současně	současně	k6eAd1
v	v	k7c6
prosinci	prosinec	k1gInSc6
1685	#num#	k4
carská	carský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
zplnomocnila	zplnomocnit	k5eAaPmAgFnS
k	k	k7c3
vedení	vedení	k1gNnSc3
mírových	mírový	k2eAgInPc2d1
rozhovorů	rozhovor	k1gInPc2
Fjodora	Fjodora	k1gFnSc1
Golovina	Golovina	k1gFnSc1
a	a	k8xC
pověřila	pověřit	k5eAaPmAgFnS
ho	on	k3xPp3gMnSc4
obranou	obrana	k1gFnSc7
a	a	k8xC
obnovou	obnova	k1gFnSc7
válkou	válka	k1gFnSc7
poškozeného	poškozený	k2eAgNnSc2d1
pohraničního	pohraniční	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
Cílem	cíl	k1gInSc7
ruské	ruský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
bylo	být	k5eAaImAgNnS
potvrzení	potvrzení	k1gNnSc1
hranice	hranice	k1gFnSc2
na	na	k7c6
Amuru	Amur	k1gInSc6
až	až	k9
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
ústí	ústit	k5eAaImIp3nP
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
považovala	považovat	k5eAaImAgFnS
za	za	k7c4
přirozenou	přirozený	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
krajním	krajní	k2eAgInSc6d1
případě	případ	k1gInSc6
Golovinovi	Golovinův	k2eAgMnPc1d1
dovolila	dovolit	k5eAaPmAgFnS
ustavení	ustavení	k1gNnSc2
hranice	hranice	k1gFnSc2
na	na	k7c6
Amuru	Amur	k1gInSc6
po	po	k7c6
ústí	ústí	k1gNnSc6
Zeji	zet	k5eAaImIp1nS
<g/>
,	,	kIx,
v	v	k7c6
nejkrajnějším	krajní	k2eAgMnSc6d3
u	u	k7c2
Albazinu	Albazin	k1gInSc2
s	s	k7c7
právem	právo	k1gNnSc7
lovu	lov	k1gInSc2
až	až	k6eAd1
po	po	k7c6
Zeju	zet	k5eAaImIp1nS
<g/>
,	,	kIx,
a	a	k8xC
zbytek	zbytek	k1gInSc4
nechat	nechat	k5eAaPmF
nerozhraničený	rozhraničený	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
každém	každý	k3xTgInSc6
případě	případ	k1gInSc6
chtěla	chtít	k5eAaImAgFnS
zachovat	zachovat	k5eAaPmF
ruské	ruský	k2eAgNnSc4d1
osídlení	osídlení	k1gNnSc4
a	a	k8xC
svrchovanost	svrchovanost	k1gFnSc4
nad	nad	k7c7
domorodci	domorodec	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
již	již	k6eAd1
přijali	přijmout	k5eAaPmAgMnP
ruské	ruský	k2eAgNnSc4d1
poddanství	poddanství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Současně	současně	k6eAd1
požadovala	požadovat	k5eAaImAgFnS
odškodnění	odškodnění	k1gNnSc4
za	za	k7c7
čchingské	čchingské	k2eAgInPc7d1
útoky	útok	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhlasila	souhlasit	k5eAaImAgFnS
s	s	k7c7
výměnou	výměna	k1gFnSc7
přeběhlíků	přeběhlík	k1gMnPc2
<g/>
,	,	kIx,
ne	ne	k9
však	však	k9
s	s	k7c7
vydáním	vydání	k1gNnSc7
ruských	ruský	k2eAgMnPc2d1
poddaných	poddaný	k1gMnPc2
(	(	kIx(
<g/>
potomků	potomek	k1gMnPc2
a	a	k8xC
lidí	člověk	k1gMnPc2
Gantimura	Gantimur	k1gMnSc2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
již	již	k6eAd1
pokřtěných	pokřtěný	k2eAgMnPc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Usilovným	usilovný	k2eAgInSc7d1
pochodem	pochod	k1gInSc7
dorazil	dorazit	k5eAaPmAgMnS
Golovin	Golovina	k1gFnPc2
s	s	k7c7
moskevským	moskevský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
před	před	k7c7
začátkem	začátek	k1gInSc7
zimy	zima	k1gFnSc2
1686	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
do	do	k7c2
Rybinsku	Rybinsko	k1gNnSc3
na	na	k7c6
Angaře	Angara	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
přezimoval	přezimovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
vojáků	voják	k1gMnPc2
však	však	k9
poslal	poslat	k5eAaPmAgMnS
zimními	zimní	k2eAgFnPc7d1
cestami	cesta	k1gFnPc7
vpřed	vpřed	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
Irkutsku	Irkutsk	k1gInSc6
se	se	k3xPyFc4
k	k	k7c3
nim	on	k3xPp3gFnPc3
připojily	připojit	k5eAaPmAgInP
místní	místní	k2eAgInPc1d1
posily	posila	k1gFnPc4
a	a	k8xC
tak	tak	k6eAd1
koncem	koncem	k7c2
března	březen	k1gInSc2
1687	#num#	k4
došlo	dojít	k5eAaPmAgNnS
do	do	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
227	#num#	k4
kozáků	kozák	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
do	do	k7c2
Udinska	Udinsko	k1gNnSc2
144	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
v	v	k7c6
létě	léto	k1gNnSc6
1687	#num#	k4
Mandžuové	Mandžu	k1gMnPc1
opevňovali	opevňovat	k5eAaImAgMnP
Ajgun	Ajgun	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
červenci	červenec	k1gInSc6
Golovin	Golovina	k1gFnPc2
dorazil	dorazit	k5eAaPmAgInS
s	s	k7c7
vojskem	vojsko	k1gNnSc7
do	do	k7c2
Irkutska	Irkutsk	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
překročil	překročit	k5eAaPmAgInS
Bajkal	Bajkal	k1gInSc1
a	a	k8xC
v	v	k7c6
polovině	polovina	k1gFnSc6
září	září	k1gNnSc2
1687	#num#	k4
soustředil	soustředit	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
vojáky	voják	k1gMnPc4
v	v	k7c6
Udinsku	Udinsko	k1gNnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Udinsku	Udinsko	k1gNnSc6
se	se	k3xPyFc4
Golovin	Golovina	k1gFnPc2
soustředil	soustředit	k5eAaPmAgInS
na	na	k7c6
vyjednání	vyjednání	k1gNnSc6
dohody	dohoda	k1gFnSc2
s	s	k7c7
Mongoly	Mongol	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabízel	nabízet	k5eAaImAgMnS
jim	on	k3xPp3gMnPc3
společnou	společný	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
proti	proti	k7c3
Čchingům	Čching	k1gInPc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
posílila	posílit	k5eAaPmAgFnS
bezpečnost	bezpečnost	k1gFnSc1
obou	dva	k4xCgFnPc2
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čchingové	Čchingový	k2eAgMnPc4d1
naopak	naopak	k6eAd1
přesvědčovali	přesvědčovat	k5eAaImAgMnP
Mongoly	mongol	k1gInPc4
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
Rusy	Rus	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1687	#num#	k4
Mandžuové	Mandžu	k1gMnPc1
získali	získat	k5eAaPmAgMnP
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
Tüšetü-chána	Tüšetü-chán	k2eAgFnSc1d1
Čichundordže	Čichundordž	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společný	společný	k2eAgInSc1d1
mandžusko-mongolský	mandžusko-mongolský	k2eAgInSc1d1
útok	útok	k1gInSc1
na	na	k7c4
Něrčinsk	Něrčinsk	k1gInSc4
byl	být	k5eAaImAgInS
naplánován	naplánovat	k5eAaBmNgInS
na	na	k7c4
jaro	jaro	k1gNnSc4
1688	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
Dzanabadzar	Dzanabadzar	k1gInSc1
však	však	k9
trval	trvat	k5eAaImAgInS
na	na	k7c4
zachování	zachování	k1gNnSc4
míru	mír	k1gInSc2
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
odmítl	odmítnout	k5eAaPmAgMnS
se	se	k3xPyFc4
války	válka	k1gFnSc2
účastnit	účastnit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Golovin	Golovina	k1gFnPc2
pochopil	pochopit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Čichundordže	Čichundordž	k1gFnPc1
nepřemluví	přemluvit	k5eNaPmIp3nP
<g/>
,	,	kIx,
a	a	k8xC
od	od	k7c2
léta	léto	k1gNnSc2
1687	#num#	k4
o	o	k7c6
přátelských	přátelský	k2eAgInPc6d1
úmyslech	úmysl	k1gInPc6
Rusů	Rus	k1gMnPc2
přesvědčoval	přesvědčovat	k5eAaImAgInS
jiné	jiný	k2eAgInPc4d1
vybrané	vybraný	k2eAgInPc4d1
mongolské	mongolský	k2eAgInPc4d1
náčelníky	náčelník	k1gInPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
sice	sice	k8xC
Cecen-nojona	Cecen-nojona	k1gFnSc1
a	a	k8xC
další	další	k2eAgInPc1d1
<g/>
,	,	kIx,
včetně	včetně	k7c2
Dzanabadzara	Dzanabadzar	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
během	během	k7c2
léta	léto	k1gNnSc2
1687	#num#	k4
Mongolové	Mongol	k1gMnPc1
prováděli	provádět	k5eAaImAgMnP
menší	malý	k2eAgInPc4d2
nájezdy	nájezd	k1gInPc4
na	na	k7c4
ruské	ruský	k2eAgNnSc4d1
území	území	k1gNnSc4
a	a	k8xC
odháněli	odhánět	k5eAaImAgMnP
skot	skot	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koncem	koncem	k7c2
října	říjen	k1gInSc2
1687	#num#	k4
se	se	k3xPyFc4
Golovin	Golovina	k1gFnPc2
přesunul	přesunout	k5eAaPmAgMnS
z	z	k7c2
Udinska	Udinsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ponechal	ponechat	k5eAaPmAgInS
většinu	většina	k1gFnSc4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
do	do	k7c2
Selenginska	Selenginsko	k1gNnSc2
<g/>
,	,	kIx,
blíž	blízce	k6eAd2
k	k	k7c3
hranicím	hranice	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
1687	#num#	k4
vypravil	vypravit	k5eAaPmAgMnS
posla	posel	k1gMnSc2
do	do	k7c2
Pekingu	Peking	k1gInSc2
s	s	k7c7
informací	informace	k1gFnSc7
o	o	k7c6
svém	svůj	k3xOyFgInSc6
příjezdu	příjezd	k1gInSc6
a	a	k8xC
návrhem	návrh	k1gInSc7
na	na	k7c4
určení	určení	k1gNnSc4
místa	místo	k1gNnSc2
a	a	k8xC
doby	doba	k1gFnSc2
rozhovorů	rozhovor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reakce	reakce	k1gFnSc1
Moskvy	Moskva	k1gFnSc2
<g/>
:	:	kIx,
mise	mise	k1gFnSc1
Veňukova	Veňukův	k2eAgFnSc1d1
a	a	k8xC
Favorova	Favorův	k2eAgFnSc1d1
a	a	k8xC
ukončení	ukončení	k1gNnSc1
obležení	obležení	k1gNnSc2
Albazinu	Albazin	k1gInSc2
(	(	kIx(
<g/>
1686	#num#	k4
<g/>
–	–	k?
<g/>
1687	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Současně	současně	k6eAd1
s	s	k7c7
vysláním	vyslání	k1gNnSc7
Golovina	Golovina	k1gFnSc1
do	do	k7c2
Zabajkalska	Zabajkalsko	k1gNnSc2
carská	carský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
poslala	poslat	k5eAaPmAgFnS
do	do	k7c2
Pekingu	Peking	k1gInSc2
vyslance	vyslanec	k1gMnSc2
Nikifora	Nikifor	k1gMnSc2
Veňukova	Veňukův	k2eAgMnSc2d1
a	a	k8xC
Ivana	Ivan	k1gMnSc2
Favorova	Favorův	k2eAgInSc2d1
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
úkolem	úkol	k1gInSc7
předat	předat	k5eAaPmF
carovu	carův	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
s	s	k7c7
vyjádřením	vyjádření	k1gNnSc7
rozhořčení	rozhořčení	k1gNnSc2
nad	nad	k7c7
čchingskou	čchingský	k2eAgFnSc7d1
agresí	agrese	k1gFnSc7
a	a	k8xC
návrhem	návrh	k1gInSc7
na	na	k7c4
mírovou	mírový	k2eAgFnSc4d1
konferenci	konference	k1gFnSc4
v	v	k7c6
Albazinu	Albazin	k1gInSc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
Čchingové	Čchingový	k2eAgInPc1d1
ukončí	ukončit	k5eAaPmIp3nP
boje	boj	k1gInPc1
a	a	k8xC
odejdou	odejít	k5eAaPmIp3nP
z	z	k7c2
ruského	ruský	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
Vyslanci	vyslanec	k1gMnPc1
v	v	k7c6
srpnu	srpen	k1gInSc6
1686	#num#	k4
dorazili	dorazit	k5eAaPmAgMnP
na	na	k7c4
mongolské	mongolský	k2eAgNnSc4d1
území	území	k1gNnSc4
a	a	k8xC
více	hodně	k6eAd2
než	než	k8xS
měsíc	měsíc	k1gInSc4
vedli	vést	k5eAaImAgMnP
rozhovory	rozhovor	k1gInPc4
s	s	k7c7
bojovně	bojovně	k6eAd1
naladěným	naladěný	k2eAgMnSc7d1
Čichundordžem	Čichundordž	k1gMnSc7
i	i	k8xC
jeho	jeho	k3xOp3gMnSc7
bratrem	bratr	k1gMnSc7
Dzanabadzarem	Dzanabadzar	k1gMnSc7
<g/>
,	,	kIx,
prosazujícím	prosazující	k2eAgNnSc7d1
mír	mír	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Veňukov	Veňukov	k1gInSc4
a	a	k8xC
Faforov	Faforov	k1gInSc4
přijeli	přijet	k5eAaPmAgMnP
do	do	k7c2
Pekingu	Peking	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1686	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
Již	již	k6eAd1
následující	následující	k2eAgInSc1d1
den	den	k1gInSc1
začala	začít	k5eAaPmAgFnS
jednání	jednání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandžuové	Mandžu	k1gMnPc1
tvrdili	tvrdit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
nejsou	být	k5eNaImIp3nP
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
boje	boj	k1gInPc4
zdůvodňovali	zdůvodňovat	k5eAaImAgMnP
svévolně	svévolně	k6eAd1
se	se	k3xPyFc4
usazujícími	usazující	k2eAgInPc7d1
kozáky	kozák	k1gInPc7
a	a	k8xC
odmítnutím	odmítnutí	k1gNnSc7
vydat	vydat	k5eAaPmF
Gantimura	Gantimura	k1gFnSc1
něrčinskými	něrčinský	k2eAgInPc7d1
úřady	úřad	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdili	tvrdit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
čchingský	čchingský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
chce	chtít	k5eAaImIp3nS
mír	mír	k1gInSc4
a	a	k8xC
ukončí	ukončit	k5eAaPmIp3nS
obležení	obležení	k1gNnSc1
Albazinu	Albazin	k1gInSc2
<g/>
;	;	kIx,
teritoriální	teritoriální	k2eAgInPc4d1
nároky	nárok	k1gInPc4
omezili	omezit	k5eAaPmAgMnP
na	na	k7c4
Poamuří	Poamuří	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
rozhovorů	rozhovor	k1gInPc2
byla	být	k5eAaImAgFnS
dohoda	dohoda	k1gFnSc1
o	o	k7c6
příměří	příměří	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
polovině	polovina	k1gFnSc6
listopadu	listopad	k1gInSc2
ruští	ruský	k2eAgMnPc1d1
vyslanci	vyslanec	k1gMnPc1
odjeli	odjet	k5eAaPmAgMnP
z	z	k7c2
Pekingu	Peking	k1gInSc2
do	do	k7c2
vlasti	vlast	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
Albazinu	Albazin	k1gInSc3
se	se	k3xPyFc4
informace	informace	k1gFnSc1
o	o	k7c6
příměří	příměří	k1gNnSc6
dostala	dostat	k5eAaPmAgFnS
počátkem	počátkem	k7c2
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čchingská	Čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
poté	poté	k6eAd1
zastavila	zastavit	k5eAaPmAgFnS
ostřelování	ostřelování	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
neodešla	odejít	k5eNaPmAgFnS
od	od	k7c2
města	město	k1gNnSc2
<g/>
;	;	kIx,
Albazinským	Albazinský	k2eAgFnPc3d1
sice	sice	k8xC
umožnila	umožnit	k5eAaPmAgFnS
styk	styk	k1gInSc4
s	s	k7c7
Něrčinskem	Něrčinsko	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
dovoz	dovoz	k1gInSc1
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
bránila	bránit	k5eAaImAgFnS
jim	on	k3xPp3gMnPc3
v	v	k7c6
získání	získání	k1gNnSc6
borového	borový	k2eAgNnSc2d1
chvojí	chvojí	k1gNnSc2
(	(	kIx(
<g/>
používaného	používaný	k2eAgNnSc2d1
proti	proti	k7c3
kurdějím	kurděje	k1gFnPc3
<g/>
)	)	kIx)
a	a	k8xC
dřeva	dřevo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
Až	až	k6eAd1
v	v	k7c6
květnu	květen	k1gInSc6
1687	#num#	k4
Mandžuové	Mandžu	k1gMnPc1
odešli	odejít	k5eAaPmAgMnP
do	do	k7c2
4	#num#	k4
km	km	kA
vzdáleného	vzdálený	k2eAgInSc2d1
tábora	tábor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Albazinu	Albazin	k1gInSc6
tehdy	tehdy	k6eAd1
zůstalo	zůstat	k5eAaPmAgNnS
naživu	naživu	k6eAd1
již	již	k6eAd1
jen	jen	k9
66	#num#	k4
kozáků	kozák	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Koncem	koncem	k7c2
srpna	srpen	k1gInSc2
1687	#num#	k4
se	se	k3xPyFc4
Mandžuové	Mandžu	k1gMnPc1
stáhli	stáhnout	k5eAaPmAgMnP
do	do	k7c2
Ajgunu	Ajgun	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
když	když	k8xS
bylo	být	k5eAaImAgNnS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Golovin	Golovina	k1gFnPc2
s	s	k7c7
posilami	posila	k1gFnPc7
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
nepřijede	přijet	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
odchodu	odchod	k1gInSc6
nepřátelského	přátelský	k2eNgNnSc2d1
vojska	vojsko	k1gNnSc2
Albazinští	Albazinský	k2eAgMnPc1d1
začali	začít	k5eAaPmAgMnP
obnovovat	obnovovat	k5eAaImF
stavení	stavení	k1gNnSc4
a	a	k8xC
připravovat	připravovat	k5eAaImF
pole	pole	k1gNnPc4
k	k	k7c3
setbě	setba	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rusko-čchingské	Rusko-čchingský	k2eAgNnSc4d1
příměří	příměří	k1gNnSc4
<g/>
,	,	kIx,
jednání	jednání	k1gNnPc1
a	a	k8xC
boje	boj	k1gInPc1
Mongolů	Mongol	k1gMnPc2
a	a	k8xC
Džúngarů	Džúngar	k1gMnPc2
(	(	kIx(
<g/>
1686	#num#	k4
<g/>
–	–	k?
<g/>
1689	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mongolské	mongolský	k2eAgInPc1d1
neúspěšné	úspěšný	k2eNgInPc1d1
útoky	útok	k1gInPc1
na	na	k7c4
Rusy	Rus	k1gMnPc4
a	a	k8xC
Džúngary	Džúngara	k1gFnPc4
(	(	kIx(
<g/>
1688	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Čchingská	Čchingský	k2eAgFnSc1d1
ofenzíva	ofenzíva	k1gFnSc1
v	v	k7c6
Poamuří	Poamuří	k1gNnSc6
v	v	k7c6
letech	let	k1gInPc6
1686	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
(	(	kIx(
<g/>
modře	modř	k1gFnSc2
<g/>
,	,	kIx,
cesta	cesta	k1gFnSc1
a	a	k8xC
návrat	návrat	k1gInSc1
vyslanců	vyslanec	k1gMnPc2
modře	modro	k6eAd1
čerchovaně	čerchovaně	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
mongolské	mongolský	k2eAgInPc1d1
útoky	útok	k1gInPc1
(	(	kIx(
<g/>
zeleně	zeleň	k1gFnPc1
<g/>
)	)	kIx)
a	a	k8xC
postup	postup	k1gInSc4
Golovinova	Golovinův	k2eAgInSc2d1
pluku	pluk	k1gInSc2
(	(	kIx(
<g/>
červeně	červeň	k1gFnSc2
<g/>
;	;	kIx,
postup	postup	k1gInSc1
menšího	malý	k2eAgInSc2d2
oddílu	oddíl	k1gInSc2
do	do	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
červeně	červeně	k6eAd1
čerchovaně	čerchovaně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Obléhání	obléhání	k1gNnSc2
Selenginského	Selenginský	k2eAgInSc2d1
ostrohu	ostroh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Už	už	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1686	#num#	k4
se	se	k3xPyFc4
zhoršovaly	zhoršovat	k5eAaImAgInP
vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
Mongoly	Mongol	k1gMnPc7
a	a	k8xC
Džúngary	Džúngar	k1gMnPc7
<g/>
,	,	kIx,
navzdory	navzdory	k7c3
snaze	snaha	k1gFnSc3
čchingské	čchingský	k2eAgFnSc2d1
diplomacie	diplomacie	k1gFnSc2
vyzývající	vyzývající	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
ke	k	k7c3
smíru	smír	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
měli	mít	k5eAaImAgMnP
Mongolové	Mongol	k1gMnPc1
volné	volný	k2eAgFnSc2d1
ruce	ruka	k1gFnPc4
k	k	k7c3
válce	válka	k1gFnSc3
s	s	k7c7
Rusy	Rus	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1687	#num#	k4
mongolský	mongolský	k2eAgInSc1d1
chán	chán	k1gMnSc1
Čichundordž	Čichundordž	k1gFnSc4
zaútočil	zaútočit	k5eAaPmAgMnS
na	na	k7c4
svého	svůj	k3xOyFgMnSc4
západního	západní	k2eAgMnSc4d1
souseda	soused	k1gMnSc4
a	a	k8xC
spojence	spojenec	k1gMnPc4
Džúngarů	Džúngar	k1gInPc2
<g/>
,	,	kIx,
Dzasagtu-chána	Dzasagtu-chán	k2eAgFnSc1d1
a	a	k8xC
zabil	zabít	k5eAaPmAgMnS
ho	on	k3xPp3gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
boji	boj	k1gInSc6
padl	padnout	k5eAaImAgMnS,k5eAaPmAgMnS
i	i	k9
bratr	bratr	k1gMnSc1
džúngarského	džúngarský	k2eAgMnSc2d1
chána	chán	k1gMnSc2
Galdana	Galdan	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
Současně	současně	k6eAd1
se	se	k3xPyFc4
Mongolové	Mongol	k1gMnPc1
rozhodli	rozhodnout	k5eAaPmAgMnP
rozpoutat	rozpoutat	k5eAaPmF
válku	válka	k1gFnSc4
proti	proti	k7c3
dvěma	dva	k4xCgMnPc3
protivníkům	protivník	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
Zatímco	zatímco	k8xS
Čichundordž	Čichundordž	k1gFnSc4
se	se	k3xPyFc4
soustředil	soustředit	k5eAaPmAgInS
na	na	k7c4
boj	boj	k1gInSc4
s	s	k7c7
Džúngary	Džúngar	k1gInPc7
<g/>
,	,	kIx,
v	v	k7c6
lednu	leden	k1gInSc6
1688	#num#	k4
Čichundordžův	Čichundordžův	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
Šidiširi	Šidiširi	k1gNnSc2
bagatur-chuntajdži	bagatur-chuntajdzat	k5eAaPmIp1nS
vyrazil	vyrazit	k5eAaPmAgMnS
proti	proti	k7c3
Rusům	Rus	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Šidiširiho	Šidiširize	k6eAd1
Mongolové	Mongol	k1gMnPc1
kvůli	kvůli	k7c3
sněhu	sníh	k1gInSc3
nemohli	moct	k5eNaImAgMnP
projít	projít	k5eAaPmF
za	za	k7c4
Bajkal	Bajkal	k1gInSc4
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
Selenginsk	Selenginsk	k1gInSc4
a	a	k8xC
Udinsk	Udinsk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
plánu	plán	k1gInSc2
měla	mít	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
část	část	k1gFnSc1
vojska	vojsko	k1gNnSc2
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
Selenginsk	Selenginsk	k1gInSc4
a	a	k8xC
druhá	druhý	k4xOgFnSc1
zablokovat	zablokovat	k5eAaPmF
cestu	cesta	k1gFnSc4
na	na	k7c4
západ	západ	k1gInSc4
<g/>
,	,	kIx,
k	k	k7c3
Bajkalu	Bajkal	k1gInSc3
a	a	k8xC
do	do	k7c2
Irkutska	Irkutsk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
různých	různý	k2eAgInPc2d1
odhadů	odhad	k1gInPc2
současníků	současník	k1gMnPc2
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
4	#num#	k4
až	až	k8xS
12	#num#	k4
tisíc	tisíc	k4xCgInPc2
a	a	k8xC
protentokrát	protentokrát	k6eAd1
disponovali	disponovat	k5eAaBmAgMnP
děly	dělo	k1gNnPc7
a	a	k8xC
píšťalami	píšťala	k1gFnPc7
zaslanými	zaslaný	k2eAgFnPc7d1
z	z	k7c2
Pekingu	Peking	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
měli	mít	k5eAaImAgMnP
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
v	v	k7c4
Zabajkalí	Zabajkalí	k1gNnSc4
kolem	kolem	k7c2
dvou	dva	k4xCgInPc2
tisíc	tisíc	k4xCgInPc2
vojáků	voják	k1gMnPc2
<g/>
:	:	kIx,
v	v	k7c6
Albazinu	Albazin	k1gInSc6
stovku	stovka	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
přes	přes	k7c4
400	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Selenginsku	Selenginsko	k1gNnSc6
kolem	kolem	k7c2
250	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Udinsku	Udinsko	k1gNnSc6
a	a	k8xC
okolních	okolní	k2eAgFnPc6d1
vsích	ves	k1gFnPc6
něco	něco	k6eAd1
přes	přes	k7c4
tisíc	tisíc	k4xCgInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
Začátkem	začátkem	k7c2
ledna	leden	k1gInSc2
1688	#num#	k4
Mongolové	Mongol	k1gMnPc1
v	v	k7c6
počtu	počet	k1gInSc6
asi	asi	k9
5	#num#	k4
tisíc	tisíc	k4xCgInPc2
oblehli	oblehnout	k5eAaPmAgMnP
Golovina	Golovina	k1gFnSc1
v	v	k7c6
Selenginsku	Selenginsko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
přišli	přijít	k5eAaPmAgMnP
k	k	k7c3
Udinsku	Udinsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iniciativu	iniciativa	k1gFnSc4
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
převzal	převzít	k5eAaPmAgMnS
velitel	velitel	k1gMnSc1
moskevských	moskevský	k2eAgMnPc2d1
střelců	střelec	k1gMnPc2
<g/>
,	,	kIx,
plukovník	plukovník	k1gMnSc1
Fjodor	Fjodor	k1gMnSc1
Skrypicyn	Skrypicyn	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
s	s	k7c7
500	#num#	k4
střelci	střelec	k1gMnPc7
vytáhl	vytáhnout	k5eAaPmAgMnS
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
z	z	k7c2
Iljinské	Iljinský	k2eAgFnSc2d1
slobody	sloboda	k1gFnSc2
(	(	kIx(
<g/>
severozápadně	severozápadně	k6eAd1
od	od	k7c2
Udinska	Udinsko	k1gNnSc2
<g/>
)	)	kIx)
k	k	k7c3
Udinsku	Udinsko	k1gNnSc3
<g/>
,	,	kIx,
třikrát	třikrát	k6eAd1
se	se	k3xPyFc4
srazil	srazit	k5eAaPmAgMnS
s	s	k7c7
Mongoly	Mongol	k1gMnPc7
<g/>
,	,	kIx,
obsadil	obsadit	k5eAaPmAgMnS
jejich	jejich	k3xOp3gInSc4
tábor	tábor	k1gInSc4
a	a	k8xC
v	v	k7c6
třídenním	třídenní	k2eAgInSc6d1
boji	boj	k1gInSc6
se	se	k3xPyFc4
ubránil	ubránit	k5eAaPmAgMnS
třem	tři	k4xCgInPc3
tisícům	tisíc	k4xCgInPc3
útočících	útočící	k2eAgMnPc2d1
Mongolů	Mongol	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mongolové	Mongol	k1gMnPc1
se	se	k3xPyFc4
poté	poté	k6eAd1
vzdali	vzdát	k5eAaPmAgMnP
plánu	plán	k1gInSc2
postupovat	postupovat	k5eAaImF
k	k	k7c3
Bajkalu	Bajkal	k1gInSc3
a	a	k8xC
na	na	k7c4
Irkutsk	Irkutsk	k1gInSc4
a	a	k8xC
stáhli	stáhnout	k5eAaPmAgMnP
se	se	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskvané	Moskvan	k1gMnPc1
měli	mít	k5eAaImAgMnP
jen	jen	k9
málo	málo	k4c4
mrtvých	mrtvý	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
polovina	polovina	k1gFnSc1
jich	on	k3xPp3gInPc2
utrpěla	utrpět	k5eAaPmAgFnS
zranění	zranění	k1gNnSc3
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
neodvážili	odvážit	k5eNaPmAgMnP
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
Mongoly	Mongol	k1gMnPc4
obléhající	obléhající	k2eAgInSc4d1
Selenginsk	Selenginsk	k1gInSc4
a	a	k8xC
požádali	požádat	k5eAaPmAgMnP
v	v	k7c6
Irkutsku	Irkutsk	k1gInSc6
o	o	k7c4
posily	posila	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
bojů	boj	k1gInPc2
se	se	k3xPyFc4
poté	poté	k6eAd1
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
zapojili	zapojit	k5eAaPmAgMnP
i	i	k9
místní	místní	k2eAgMnPc1d1
Burjati	Burjat	k1gMnPc1
a	a	k8xC
Tunguzové	Tunguz	k1gMnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc1
nájezdy	nájezd	k1gInPc1
do	do	k7c2
Mongolska	Mongolsko	k1gNnSc2
museli	muset	k5eAaImAgMnP
ruští	ruský	k2eAgMnPc1d1
velitelé	velitel	k1gMnPc1
dokonce	dokonce	k9
mírnit	mírnit	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
nezkomplikovaly	zkomplikovat	k5eNaPmAgInP
vztahy	vztah	k1gInPc1
s	s	k7c7
mongolskými	mongolský	k2eAgInPc7d1
náčelníky	náčelník	k1gInPc7
neúčastnícími	účastnící	k2eNgInPc7d1
se	se	k3xPyFc4
útoku	útok	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
polovině	polovina	k1gFnSc6
března	březen	k1gInSc2
plukovníci	plukovník	k1gMnPc1
Skrypicyn	Skrypicyn	k1gInSc4
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Grabov	Grabov	k1gInSc1
a	a	k8xC
Anton	Anton	k1gMnSc1
Smalenberg	Smalenberg	k1gMnSc1
shromáždili	shromáždit	k5eAaPmAgMnP
v	v	k7c6
Udinsku	Udinsko	k1gNnSc6
1	#num#	k4
500	#num#	k4
mužů	muž	k1gMnPc2
a	a	k8xC
vyrazili	vyrazit	k5eAaPmAgMnP
na	na	k7c4
pomoc	pomoc	k1gFnSc4
Selenginsku	Selenginsko	k1gNnSc3
<g/>
,	,	kIx,
stále	stále	k6eAd1
se	se	k3xPyFc4
bránícímu	bránící	k2eAgNnSc3d1
mongolským	mongolský	k2eAgMnPc3d1
útokům	útok	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šidiširi	Šidiširi	k1gNnSc1
s	s	k7c7
vojskem	vojsko	k1gNnSc7
vytáhl	vytáhnout	k5eAaPmAgMnS
proti	proti	k7c3
příchozím	příchozí	k1gMnPc3
Rusům	Rus	k1gMnPc3
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
na	na	k7c4
ně	on	k3xPp3gNnSc4
zaútočil	zaútočit	k5eAaPmAgMnS
ve	v	k7c6
stepi	step	k1gFnSc6
20	#num#	k4
km	km	kA
od	od	k7c2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
pomocí	pomoc	k1gFnSc7
jízdy	jízda	k1gFnSc2
ze	z	k7c2
Selenginsku	Selenginsko	k1gNnSc3
<g/>
,	,	kIx,
vyslané	vyslaný	k2eAgFnSc3d1
Golovinem	Golovino	k1gNnSc7
do	do	k7c2
zad	záda	k1gNnPc2
nepřátel	nepřítel	k1gMnPc2
<g/>
,	,	kIx,
Rusové	Rusové	k2eAgMnPc4d1
Mongoly	Mongol	k1gMnPc4
rozdrtili	rozdrtit	k5eAaPmAgMnP
a	a	k8xC
zahnali	zahnat	k5eAaPmAgMnP
do	do	k7c2
Mongolska	Mongolsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
mongolské	mongolský	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
Čchingové	Čchingový	k2eAgFnSc2d1
odložili	odložit	k5eAaPmAgMnP
útok	útok	k1gInSc4
na	na	k7c4
Něrčinsk	Něrčinsk	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
připravovaný	připravovaný	k2eAgInSc1d1
na	na	k7c4
duben	duben	k1gInSc4
1688	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
rozhodli	rozhodnout	k5eAaPmAgMnP
se	se	k3xPyFc4
využít	využít	k5eAaPmF
mírových	mírový	k2eAgFnPc2d1
jednání	jednání	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
navrhli	navrhnout	k5eAaPmAgMnP
provést	provést	k5eAaPmF
v	v	k7c6
Selenginsku	Selenginsko	k1gNnSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
každá	každý	k3xTgFnSc1
strana	strana	k1gFnSc1
přivede	přivést	k5eAaPmIp3nS
maximálně	maximálně	k6eAd1
500	#num#	k4
vojáků	voják	k1gMnPc2
(	(	kIx(
<g/>
což	což	k9
nemínili	mínit	k5eNaImAgMnP
dodržet	dodržet	k5eAaPmF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
Dzanabadzar	Dzanabadzar	k1gMnSc1
informoval	informovat	k5eAaBmAgMnS
Rusy	Rus	k1gMnPc4
o	o	k7c6
čchingských	čchingský	k2eAgFnPc6d1
aktivitách	aktivita	k1gFnPc6
i	i	k8xC
dění	dění	k1gNnSc4
v	v	k7c6
Mongolsku	Mongolsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
I	i	k9
během	během	k7c2
bojů	boj	k1gInPc2
Dzanabadzar	Dzanabadzara	k1gFnPc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
stejně	stejně	k6eAd1
jako	jako	k9
chán	chán	k1gMnSc1
požadoval	požadovat	k5eAaImAgMnS
vydání	vydání	k1gNnSc4
Burjatů	Burjat	k1gMnPc2
poddaných	poddaná	k1gFnPc2
Rusům	Rus	k1gMnPc3
<g/>
,	,	kIx,
prosazoval	prosazovat	k5eAaImAgMnS
mír	mír	k1gInSc4
<g/>
,	,	kIx,
žádal	žádat	k5eAaImAgMnS
po	po	k7c6
Šidiširovi	Šidišir	k1gMnSc6
přerušení	přerušení	k1gNnPc4
obležení	obležení	k1gNnSc2
a	a	k8xC
ujišťoval	ujišťovat	k5eAaImAgMnS
Golovina	Golovina	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gMnPc1
lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
bojů	boj	k1gInPc2
neúčastní	účastnit	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Paralelní	paralelní	k2eAgInSc1d1
boj	boj	k1gInSc1
Mongolů	Mongol	k1gMnPc2
s	s	k7c7
Džúngary	Džúngar	k1gMnPc7
byl	být	k5eAaImAgMnS
také	také	k9
neúspěšný	úspěšný	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Galdan	Galdan	k1gInSc1
na	na	k7c6
jaře	jaro	k1gNnSc6
1688	#num#	k4
porazil	porazit	k5eAaPmAgMnS
Čichundordžovy	Čichundordžův	k2eAgFnPc4d1
síly	síla	k1gFnPc4
a	a	k8xC
pak	pak	k6eAd1
postupoval	postupovat	k5eAaImAgInS
na	na	k7c4
východ	východ	k1gInSc4
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
území	území	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
poplenil	poplenit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
Mongolové	Mongol	k1gMnPc1
před	před	k7c7
Džúngary	Džúngar	k1gMnPc7
prchali	prchat	k5eAaImAgMnP
na	na	k7c4
všechny	všechen	k3xTgFnPc4
strany	strana	k1gFnPc4
<g/>
,	,	kIx,
k	k	k7c3
Rusům	Rus	k1gMnPc3
i	i	k8xC
Čchingům	Čching	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Porážka	porážka	k1gFnSc1
Mongolů	mongol	k1gInPc2
Džúngary	Džúngara	k1gFnSc2
a	a	k8xC
odložení	odložení	k1gNnSc4
čchingsko	čchingsko	k6eAd1
<g/>
–	–	k?
<g/>
ruské	ruský	k2eAgFnSc2d1
mírové	mírový	k2eAgFnSc2d1
konference	konference	k1gFnSc2
(	(	kIx(
<g/>
1688	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Fjodor	Fjodor	k1gMnSc1
Alexejevič	Alexejevič	k1gMnSc1
Golovin	Golovina	k1gFnPc2
</s>
<s>
Golovin	Golovina	k1gFnPc2
na	na	k7c6
jaře	jaro	k1gNnSc6
1688	#num#	k4
opevňoval	opevňovat	k5eAaImAgInS
Udinsk	Udinsk	k1gInSc1
a	a	k8xC
Selenginsk	Selenginsk	k1gInSc1
a	a	k8xC
naplánoval	naplánovat	k5eAaBmAgInS
útok	útok	k1gInSc1
na	na	k7c4
Mongoly	mongol	k1gInPc4
ze	z	k7c2
dvou	dva	k4xCgInPc2
směrů	směr	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
však	však	k9
odložil	odložit	k5eAaPmAgInS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
přišly	přijít	k5eAaPmAgFnP
informace	informace	k1gFnPc1
o	o	k7c6
džúngarském	džúngarský	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
nad	nad	k7c7
Čichundordžem	Čichundordž	k1gInSc7
a	a	k8xC
postupu	postup	k1gInSc2
Džúngarů	Džúngar	k1gInPc2
na	na	k7c4
východ	východ	k1gInSc4
do	do	k7c2
Mongolska	Mongolsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Císař	Císař	k1gMnSc1
Kchang-si	Kchang-se	k1gFnSc4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
do	do	k7c2
čela	čelo	k1gNnSc2
čchingské	čchingský	k2eAgFnSc2d1
delegace	delegace	k1gFnSc2
určené	určený	k2eAgFnPc4d1
pro	pro	k7c4
jednání	jednání	k1gNnPc4
s	s	k7c7
Rusy	Rus	k1gMnPc7
zkušeného	zkušený	k2eAgMnSc4d1
státníka	státník	k1gMnSc4
Songgotu	Songgot	k1gInSc2
<g/>
,	,	kIx,
svého	svůj	k1gMnSc2
strýce	strýc	k1gMnSc2
Tchung	Tchung	k1gMnSc1
Kuo-kanga	Kuo-kang	k1gMnSc2
<g/>
,	,	kIx,
vojevůdce	vojevůdce	k1gMnSc2
Maciho	Maci	k1gMnSc2
a	a	k8xC
další	další	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Pekingu	Peking	k1gInSc2
vyjeli	vyjet	k5eAaPmAgMnP
s	s	k7c7
početným	početný	k2eAgInSc7d1
doprovodem	doprovod	k1gInSc7
koncem	koncem	k7c2
května	květen	k1gInSc2
1688	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
července	červenec	k1gInSc2
<g/>
,	,	kIx,
již	již	k6eAd1
na	na	k7c6
mongolském	mongolský	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
,	,	kIx,
začali	začít	k5eAaPmAgMnP
potkávat	potkávat	k5eAaImF
mongolské	mongolský	k2eAgMnPc4d1
uprchlíky	uprchlík	k1gMnPc4
z	z	k7c2
války	válka	k1gFnSc2
s	s	k7c7
Galdanem	Galdan	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Songgotu	Songgot	k1gInSc2
proto	proto	k8xC
zastavil	zastavit	k5eAaPmAgMnS
cestu	cesta	k1gFnSc4
a	a	k8xC
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
k	k	k7c3
němu	on	k3xPp3gNnSc3
dorazili	dorazit	k5eAaPmAgMnP
kurýři	kurýr	k1gMnPc1
z	z	k7c2
Pekingu	Peking	k1gInSc2
přikazující	přikazující	k2eAgInSc4d1
návrat	návrat	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
odložení	odložení	k1gNnSc6
mírových	mírový	k2eAgNnPc2d1
jednání	jednání	k1gNnPc2
kvůli	kvůli	k7c3
válce	válka	k1gFnSc3
Džúngarů	Džúngar	k1gInPc2
s	s	k7c7
Mongoly	Mongol	k1gMnPc4
Čchingové	Čchingový	k2eAgMnPc4d1
neprodleně	prodleně	k6eNd1
informovali	informovat	k5eAaBmAgMnP
Golovina	Golovina	k1gFnSc1
<g/>
;	;	kIx,
Golovin	Golovina	k1gFnPc2
odvětil	odvětit	k5eAaPmAgInS
požadavkem	požadavek	k1gInSc7
na	na	k7c4
stažení	stažení	k1gNnSc4
čchingských	čchingský	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
z	z	k7c2
ruských	ruský	k2eAgNnPc2d1
území	území	k1gNnPc2
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgInS
se	se	k3xPyFc4
přesunout	přesunout	k5eAaPmF
do	do	k7c2
Albazinu	Albazin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
koncem	koncem	k7c2
srpna	srpen	k1gInSc2
vytáhl	vytáhnout	k5eAaPmAgMnS
se	s	k7c7
svým	svůj	k3xOyFgInSc7
plukem	pluk	k1gInSc7
(	(	kIx(
<g/>
1	#num#	k4
160	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
na	na	k7c4
Něrčinsk	Něrčinsk	k1gInSc4
<g/>
;	;	kIx,
v	v	k7c6
Udinsku	Udinsko	k1gNnSc6
nechal	nechat	k5eAaPmAgInS
posádku	posádka	k1gFnSc4
o	o	k7c4
120	#num#	k4
a	a	k8xC
v	v	k7c6
Selenginsku	Selenginsko	k1gNnSc6
o	o	k7c4
164	#num#	k4
mužích	muž	k1gMnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zatím	zatím	k6eAd1
začátkem	začátkem	k7c2
července	červenec	k1gInSc2
1688	#num#	k4
pod	pod	k7c4
Albazin	Albazin	k1gInSc4
připlula	připlout	k5eAaPmAgFnS
velká	velký	k2eAgFnSc1d1
čchingská	čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
jdoucí	jdoucí	k2eAgFnSc1d1
na	na	k7c4
mírová	mírový	k2eAgNnPc4d1
jednání	jednání	k1gNnPc4
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
požadující	požadující	k2eAgFnPc1d1
od	od	k7c2
Bejtona	Bejton	k1gMnSc2
podporu	podpor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
podzim	podzim	k1gInSc4
1688	#num#	k4
čchingská	čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
v	v	k7c6
albazinském	albazinský	k2eAgInSc6d1
regionu	region	k1gInSc6
zničila	zničit	k5eAaPmAgFnS
úrodu	úroda	k1gFnSc4
na	na	k7c6
polích	pole	k1gNnPc6
a	a	k8xC
tamní	tamní	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
se	se	k3xPyFc4
ocitla	ocitnout	k5eAaPmAgFnS
v	v	k7c6
nebezpečí	nebezpečí	k1gNnSc6
hladu	hlad	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
v	v	k7c6
květnu	květen	k1gInSc6
1689	#num#	k4
Albazinští	Albazinský	k2eAgMnPc1d1
znovu	znovu	k6eAd1
zaseli	zasít	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Golovin	Golovina	k1gFnPc2
proto	proto	k6eAd1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
kvůli	kvůli	k7c3
obavě	obava	k1gFnSc3
z	z	k7c2
mandžusko-mongolské	mandžusko-mongolský	k2eAgFnSc2d1
ofenzivy	ofenziva	k1gFnSc2
proti	proti	k7c3
Galdanovi	Galdan	k1gMnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
ohrozit	ohrozit	k5eAaPmF
i	i	k9
Zabajkalí	Zabajkalí	k1gNnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
na	na	k7c4
podzim	podzim	k1gInSc4
1688	#num#	k4
přerušil	přerušit	k5eAaPmAgMnS
přesun	přesun	k1gInSc4
do	do	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
a	a	k8xC
zaměřil	zaměřit	k5eAaPmAgMnS
se	se	k3xPyFc4
na	na	k7c4
řešení	řešení	k1gNnSc4
problému	problém	k1gInSc2
mongolských	mongolský	k2eAgMnPc2d1
přesídlenců	přesídlenec	k1gMnPc2
<g/>
,	,	kIx,
prchajících	prchající	k2eAgMnPc2d1
na	na	k7c4
ruské	ruský	k2eAgNnSc4d1
území	území	k1gNnSc4
před	před	k7c7
válkou	válka	k1gFnSc7
po	po	k7c6
stovkách	stovka	k1gFnPc6
a	a	k8xC
tisících	tisící	k4xOgMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgInS
s	s	k7c7
tabunutskými	tabunutský	k2eAgMnPc7d1
tajši	tajš	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
odmítali	odmítat	k5eAaImAgMnP
ruské	ruský	k2eAgNnSc4d1
podanství	podanství	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
současně	současně	k6eAd1
trvali	trvat	k5eAaImAgMnP
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
právu	právo	k1gNnSc6
kočovat	kočovat	k5eAaImF
v	v	k7c6
Zabajkalí	Zabajkalí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Golovin	Golovina	k1gFnPc2
na	na	k7c4
ně	on	k3xPp3gNnSc4
koncem	koncem	k7c2
září	září	k1gNnSc2
1688	#num#	k4
zaútočil	zaútočit	k5eAaPmAgInS
s	s	k7c7
oddílem	oddíl	k1gInSc7
500	#num#	k4
Rusů	Rus	k1gMnPc2
a	a	k8xC
300	#num#	k4
Burjatů	Burjat	k1gMnPc2
a	a	k8xC
Tunguzů	Tunguz	k1gMnPc2
a	a	k8xC
porazil	porazit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
jich	on	k3xPp3gMnPc2
3	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
tom	ten	k3xDgInSc6
1	#num#	k4
200	#num#	k4
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
přijalo	přijmout	k5eAaPmAgNnS
ruské	ruský	k2eAgNnSc1d1
poddanství	poddanství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijetí	přijetí	k1gNnSc4
poddanství	poddanství	k1gNnSc2
navrhl	navrhnout	k5eAaPmAgMnS
i	i	k8xC
dalším	další	k2eAgFnPc3d1
mongolským	mongolský	k2eAgFnPc3d1
skupinám	skupina	k1gFnPc3
zdržujícím	zdržující	k2eAgFnPc3d1
se	se	k3xPyFc4
podél	podél	k7c2
hranice	hranice	k1gFnSc2
<g/>
,	,	kIx,
mnozí	mnohý	k2eAgMnPc1d1
souhlasili	souhlasit	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
od	od	k7c2
července	červenec	k1gInSc2
1688	#num#	k4
do	do	k7c2
března	březen	k1gInSc2
1689	#num#	k4
přišlo	přijít	k5eAaPmAgNnS
k	k	k7c3
Rusům	Rus	k1gMnPc3
z	z	k7c2
Mongolska	Mongolsko	k1gNnSc2
na	na	k7c4
10	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
náčelníci	náčelník	k1gMnPc1
hledali	hledat	k5eAaImAgMnP
u	u	k7c2
Rusů	Rus	k1gMnPc2
pouze	pouze	k6eAd1
dočasné	dočasný	k2eAgNnSc4d1
spojenectví	spojenectví	k1gNnSc4
a	a	k8xC
nechtěli	chtít	k5eNaImAgMnP
se	se	k3xPyFc4
vzdát	vzdát	k5eAaPmF
politické	politický	k2eAgFnSc3d1
samostatnosti	samostatnost	k1gFnSc3
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
do	do	k7c2
Mongolska	Mongolsko	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
jejich	jejich	k3xOp3gMnPc1
lidé	člověk	k1gMnPc1
však	však	k9
mnohdy	mnohdy	k6eAd1
zůstávali	zůstávat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
1688	#num#	k4
pokračovala	pokračovat	k5eAaImAgFnS
i	i	k9
mongolsko-džúngarská	mongolsko-džúngarský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čchingové	Čchingový	k2eAgFnSc6d1
posilovali	posilovat	k5eAaImAgMnP
ostrahu	ostraha	k1gFnSc4
hranic	hranice	k1gFnPc2
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
pokusili	pokusit	k5eAaPmAgMnP
se	se	k3xPyFc4
přesvědčit	přesvědčit	k5eAaPmF
Galdana	Galdana	k1gFnSc1
k	k	k7c3
míru	mír	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čichundordž	Čichundordž	k1gFnSc1
požádal	požádat	k5eAaPmAgInS
Čchingy	Čching	k1gInPc4
o	o	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ti	ty	k3xPp2nSc3
na	na	k7c6
něm	on	k3xPp3gMnSc6
požadovali	požadovat	k5eAaImAgMnP
vzdání	vzdání	k1gNnSc4
se	se	k3xPyFc4
samostatnosti	samostatnost	k1gFnSc2
a	a	k8xC
přijetí	přijetí	k1gNnSc2
poddanství	poddanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mongolové	Mongol	k1gMnPc1
odmítli	odmítnout	k5eAaPmAgMnP
a	a	k8xC
opevnili	opevnit	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c6
ostrůvcích	ostrůvek	k1gInPc6
jezera	jezero	k1gNnSc2
Olgoj	Olgoj	k1gInSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zneutralizovali	zneutralizovat	k5eAaPmAgMnP
převahu	převaha	k1gFnSc4
džúngarské	džúngarský	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
I	i	k9
tak	tak	k6eAd1
byli	být	k5eAaImAgMnP
koncem	koncem	k7c2
srpna	srpen	k1gInSc2
1688	#num#	k4
v	v	k7c6
několikadenním	několikadenní	k2eAgInSc6d1
boji	boj	k1gInSc6
poraženi	poražen	k2eAgMnPc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
Čichundorž	Čichundorž	k1gFnSc1
<g/>
,	,	kIx,
Dzanabadzar	Dzanabadzar	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnPc1d1
mongolští	mongolský	k2eAgMnPc1d1
náčelníci	náčelník	k1gMnPc1
se	se	k3xPyFc4
zachránili	zachránit	k5eAaPmAgMnP
útěkem	útěk	k1gInSc7
k	k	k7c3
Čchingům	Čching	k1gInPc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
načež	načež	k6eAd1
jim	on	k3xPp3gMnPc3
nezbylo	zbýt	k5eNaPmAgNnS
než	než	k8xS
přijmout	přijmout	k5eAaPmF
čchingské	čchingské	k2eAgNnPc4d1
poddanství	poddanství	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galdan	Galdan	k1gMnSc1
šířil	šířit	k5eAaImAgMnS
fámy	fáma	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
své	svůj	k3xOyFgFnPc4
akce	akce	k1gFnSc1
koordinuje	koordinovat	k5eAaBmIp3nS
s	s	k7c7
Rusy	Rus	k1gMnPc7
a	a	k8xC
ruské	ruský	k2eAgFnSc2d1
akce	akce	k1gFnSc2
proti	proti	k7c3
tabunutským	tabunutský	k2eAgMnPc3d1
Mongolům	Mongol	k1gMnPc3
na	na	k7c4
podzim	podzim	k1gInSc4
1688	#num#	k4
jeho	jeho	k3xOp3gFnSc4
propagandu	propaganda	k1gFnSc4
podpořily	podpořit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
1688	#num#	k4
navíc	navíc	k6eAd1
vyslal	vyslat	k5eAaPmAgMnS
poselstvo	poselstvo	k1gNnSc4
do	do	k7c2
Irkutska	Irkutsk	k1gInSc2
s	s	k7c7
návrhem	návrh	k1gInSc7
na	na	k7c4
společnou	společný	k2eAgFnSc4d1
válku	válka	k1gFnSc4
proti	proti	k7c3
Mongolům	mongol	k1gInPc3
a	a	k8xC
navázání	navázání	k1gNnSc3
obchodních	obchodní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
vstřícný	vstřícný	k2eAgInSc1d1
krok	krok	k1gInSc1
se	se	k3xPyFc4
vzdal	vzdát	k5eAaPmAgInS
nároků	nárok	k1gInPc2
na	na	k7c4
obyvatelstvo	obyvatelstvo	k1gNnSc4
Sajan	Sajana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Golovin	Golovina	k1gFnPc2
nespěchal	spěchat	k5eNaImAgInS
s	s	k7c7
odpovědí	odpověď	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obával	obávat	k5eAaImAgMnS
se	se	k3xPyFc4
návratu	návrat	k1gInSc6
Galdana	Galdana	k1gFnSc1
na	na	k7c4
západ	západ	k1gInSc4
a	a	k8xC
následné	následný	k2eAgFnSc2d1
mandžusko-mongolské	mandžusko-mongolský	k2eAgFnSc2d1
ofenzívy	ofenzíva	k1gFnSc2
do	do	k7c2
Zabajkalí	Zabajkalí	k1gNnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
Galdanovi	Galdan	k1gMnSc3
odpověděl	odpovědět	k5eAaPmAgMnS
návrhem	návrh	k1gInSc7
„	„	k?
<g/>
neuzavírat	uzavírat	k5eNaImF
mír	mír	k1gInSc4
s	s	k7c7
Mongoly	Mongol	k1gMnPc7
ani	ani	k8xC
Mandžuy	Mandžu	k1gMnPc7
před	před	k7c7
rusko-čchingskými	rusko-čchingský	k2eAgNnPc7d1
jednáními	jednání	k1gNnPc7
a	a	k8xC
neustupovat	ustupovat	k5eNaImF
před	před	k7c7
nimi	on	k3xPp3gFnPc7
do	do	k7c2
Džúngarska	Džúngarska	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
seznámení	seznámení	k1gNnSc6
se	se	k3xPyFc4
s	s	k7c7
Golovinovým	Golovinův	k2eAgInSc7d1
postupem	postup	k1gInSc7
analogický	analogický	k2eAgInSc1d1
dopis	dopis	k1gInSc1
poslala	poslat	k5eAaPmAgFnS
v	v	k7c6
červenci	červenec	k1gInSc6
1689	#num#	k4
Galdanovi	Galdanův	k2eAgMnPc1d1
ruská	ruský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dohoda	dohoda	k1gFnSc1
o	o	k7c6
sjezdu	sjezd	k1gInSc6
vyslanců	vyslanec	k1gMnPc2
a	a	k8xC
čchingské	čchingský	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
na	na	k7c4
Něrčinsk	Něrčinsk	k1gInSc4
(	(	kIx(
<g/>
1689	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pohyb	pohyb	k1gInSc1
čchingského	čchingské	k1gNnSc2
(	(	kIx(
<g/>
modře	modro	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
ruského	ruský	k2eAgMnSc2d1
(	(	kIx(
<g/>
červeně	červeň	k1gFnSc2
<g/>
)	)	kIx)
vojska	vojsko	k1gNnSc2
a	a	k8xC
vyslanců	vyslanec	k1gMnPc2
k	k	k7c3
Něrčinsku	Něrčinsko	k1gNnSc3
roku	rok	k1gInSc2
1689	#num#	k4
</s>
<s>
Golovin	Golovina	k1gFnPc2
v	v	k7c6
lednu	leden	k1gInSc6
1689	#num#	k4
poslal	poslat	k5eAaPmAgMnS
do	do	k7c2
Pekingu	Peking	k1gInSc2
Ivana	Ivan	k1gMnSc2
Loginova	Loginův	k2eAgMnSc2d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
do	do	k7c2
Pekingu	Peking	k1gInSc2
dorazil	dorazit	k5eAaPmAgMnS
v	v	k7c6
polovině	polovina	k1gFnSc6
května	květen	k1gInSc2
s	s	k7c7
úkolem	úkol	k1gInSc7
dohodnout	dohodnout	k5eAaPmF
okolnosti	okolnost	k1gFnSc3
setkání	setkání	k1gNnSc2
vyslanců	vyslanec	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
seznámit	seznámit	k5eAaPmF
chingskou	chingský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
s	s	k7c7
návrhy	návrh	k1gInPc7
mírové	mírový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
připravenými	připravený	k2eAgInPc7d1
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
setkání	setkání	k1gNnSc4
navrhl	navrhnout	k5eAaPmAgMnS
Albazin	Albazin	k1gInSc4
nebo	nebo	k8xC
Něrčinsk	Něrčinsk	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
Mandžuové	Mandžu	k1gMnPc1
nechtěli	chtít	k5eNaImAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Golovin	Golovina	k1gFnPc2
s	s	k7c7
vojskem	vojsko	k1gNnSc7
přijel	přijet	k5eAaPmAgMnS
do	do	k7c2
Albazinu	Albazin	k1gInSc2
<g/>
,	,	kIx,
proto	proto	k8xC
Loginovovi	Loginovův	k2eAgMnPc1d1
v	v	k7c6
Pekingu	Peking	k1gInSc6
jednoznačně	jednoznačně	k6eAd1
oznámili	oznámit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jednání	jednání	k1gNnPc1
vyslanců	vyslanec	k1gMnPc2
proběhnou	proběhnout	k5eAaPmIp3nP
u	u	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
a	a	k8xC
s	s	k7c7
vyslanci	vyslanec	k1gMnPc7
pošlou	poslat	k5eAaPmIp3nP
tisíc	tisíc	k4xCgInPc2
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
Mandžuové	Mandžu	k1gMnPc1
od	od	k7c2
jara	jaro	k1gNnSc2
posilovali	posilovat	k5eAaImAgMnP
své	svůj	k3xOyFgNnSc4
vojsko	vojsko	k1gNnSc4
v	v	k7c6
Ajgunu	Ajgun	k1gInSc6
–	–	k?
naváželi	navážet	k5eAaImAgMnP
zásoby	zásoba	k1gFnPc4
<g/>
,	,	kIx,
opravovali	opravovat	k5eAaImAgMnP
lodě	loď	k1gFnPc4
a	a	k8xC
posílali	posílat	k5eAaImAgMnP
nové	nový	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
;	;	kIx,
což	což	k3yRnSc1,k3yQnSc1
znepokojilo	znepokojit	k5eAaPmAgNnS
Rusy	Rus	k1gMnPc4
v	v	k7c6
Albazinu	Albazino	k1gNnSc6
i	i	k8xC
Něrčinsku	Něrčinsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červenci	červenec	k1gInSc6
Golovin	Golovina	k1gFnPc2
přikázal	přikázat	k5eAaPmAgMnS
Bejtonovi	Bejton	k1gMnSc3
v	v	k7c6
Albazinu	Albazino	k1gNnSc6
sklidit	sklidit	k5eAaPmF
obilí	obilí	k1gNnSc4
a	a	k8xC
mandžuským	mandžuský	k2eAgMnPc3d1
zástupcům	zástupce	k1gMnPc3
přítomným	přítomný	k2eAgInPc3d1
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
si	se	k3xPyFc3
stěžoval	stěžovat	k5eAaImAgMnS
na	na	k7c4
narušování	narušování	k1gNnSc4
příměří	příměří	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
1689	#num#	k4
se	se	k3xPyFc4
císař	císař	k1gMnSc1
Kchang-si	Kchang-se	k1gFnSc3
stále	stále	k6eAd1
snažil	snažit	k5eAaImAgMnS
diplomatickou	diplomatický	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
usmířit	usmířit	k5eAaPmF
Mongoly	mongol	k1gInPc4
s	s	k7c7
Džúngary	Džúngar	k1gInPc7
a	a	k8xC
Džúngary	Džúngar	k1gInPc7
přesvědčit	přesvědčit	k5eAaPmF
ke	k	k7c3
stáhnutí	stáhnutí	k1gNnSc3
se	se	k3xPyFc4
z	z	k7c2
Mongolska	Mongolsko	k1gNnSc2
a	a	k8xC
návratu	návrat	k1gInSc2
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
Golovin	Golovina	k1gFnPc2
zase	zase	k9
v	v	k7c6
létě	léto	k1gNnSc6
1689	#num#	k4
pochopil	pochopit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
nedojde	dojít	k5eNaPmIp3nS
k	k	k7c3
čchingskému	čchingské	k1gNnSc3
útoku	útok	k1gInSc2
na	na	k7c4
Galdana	Galdan	k1gMnSc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
koncem	koncem	k7c2
června	červen	k1gInSc2
začal	začít	k5eAaPmAgMnS
přesouvat	přesouvat	k5eAaImF
s	s	k7c7
celým	celý	k2eAgInSc7d1
svým	svůj	k3xOyFgInSc7
plukem	pluk	k1gInSc7
z	z	k7c2
Udinska	Udinsko	k1gNnSc2
do	do	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
uzavření	uzavření	k1gNnSc6
dohody	dohoda	k1gFnSc2
o	o	k7c6
sjezdu	sjezd	k1gInSc6
vyslanců	vyslanec	k1gMnPc2
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
Loginov	Loginovo	k1gNnPc2
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1689	#num#	k4
odjel	odjet	k5eAaPmAgMnS
z	z	k7c2
Pekingu	Peking	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čchingští	Čchingský	k2eAgMnPc1d1
vyslanci	vyslanec	k1gMnPc1
ho	on	k3xPp3gNnSc4
však	však	k9
následovali	následovat	k5eAaImAgMnP
už	už	k6eAd1
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
,	,	kIx,
o	o	k7c4
týden	týden	k1gInSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
bylo	být	k5eAaImAgNnS
dohodnuto	dohodnout	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Téhož	týž	k3xTgNnSc2
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
se	se	k3xPyFc4
čchingské	čchingský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
na	na	k7c6
60	#num#	k4
lodích	loď	k1gFnPc6
objevilo	objevit	k5eAaPmAgNnS
pod	pod	k7c7
Albazinem	Albazin	k1gInSc7
pod	pod	k7c7
záminkou	záminka	k1gFnSc7
„	„	k?
<g/>
zásobování	zásobování	k1gNnSc2
vyslanců	vyslanec	k1gMnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
potom	potom	k6eAd1
čchingští	čchingský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
překročili	překročit	k5eAaPmAgMnP
Argun	Argun	k1gInSc4
a	a	k8xC
na	na	k7c6
Šilce	Šilka	k1gFnSc6
shromáždili	shromáždit	k5eAaPmAgMnP
800	#num#	k4
koní	kůň	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
července	červenec	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
dozvěděli	dozvědět	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
kolem	kolem	k7c2
Albazinu	Albazin	k1gInSc2
na	na	k7c6
120	#num#	k4
lodích	loď	k1gFnPc6
projelo	projet	k5eAaPmAgNnS
už	už	k6eAd1
4	#num#	k4
000	#num#	k4
mandžuských	mandžuský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
se	s	k7c7
45	#num#	k4
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Golovin	Golovina	k1gFnPc2
reagoval	reagovat	k5eAaBmAgMnS
upozorněním	upozornění	k1gNnSc7
něrčinské	něrčinský	k2eAgFnSc2d1
posádky	posádka	k1gFnSc2
<g/>
,	,	kIx,
pokračováním	pokračování	k1gNnSc7
přesunu	přesun	k1gInSc2
svého	svůj	k3xOyFgInSc2
pluku	pluk	k1gInSc2
<g/>
,	,	kIx,
odesláním	odeslání	k1gNnSc7
domorodých	domorodý	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
z	z	k7c2
okolí	okolí	k1gNnSc2
Něrčinska	Něrčinsko	k1gNnSc2
hlouběji	hluboko	k6eAd2
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
a	a	k8xC
vysláním	vyslání	k1gNnSc7
posla	posel	k1gMnSc2
k	k	k7c3
čchingskému	čchingský	k2eAgNnSc3d1
poselství	poselství	k1gNnSc3
s	s	k7c7
protestem	protest	k1gInSc7
proti	proti	k7c3
příchodu	příchod	k1gInSc3
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
čchingské	čchingský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
o	o	k7c6
70	#num#	k4
lodích	loď	k1gFnPc6
s	s	k7c7
34	#num#	k4
děly	dělo	k1gNnPc7
zakotvilo	zakotvit	k5eAaPmAgNnS
na	na	k7c6
Šilce	Šilka	k1gFnSc6
kilometr	kilometr	k1gInSc4
nad	nad	k7c7
Něrčinskem	Něrčinsko	k1gNnSc7
a	a	k8xC
blokovalo	blokovat	k5eAaImAgNnS
tak	tak	k8xC,k8xS
Rusům	Rus	k1gMnPc3
cestu	cesta	k1gFnSc4
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
protestech	protest	k1gInPc6
Vlasova	Vlasův	k2eAgInSc2d1
se	s	k7c7
18	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
stáhlo	stáhnout	k5eAaPmAgNnS
po	po	k7c6
proudu	proud	k1gInSc6
na	na	k7c4
nové	nový	k2eAgNnSc4d1
místo	místo	k1gNnSc4
naproti	naproti	k7c3
Něrčinsku	Něrčinsko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
do	do	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
přijeli	přijet	k5eAaPmAgMnP
poslové	posel	k1gMnPc1
čchingských	čchingských	k2eAgFnSc2d1
vyslanců	vyslanec	k1gMnPc2
informující	informující	k2eAgInSc1d1
Rusy	Rus	k1gMnPc7
o	o	k7c6
jejich	jejich	k3xOp3gInSc6
postupu	postup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlasov	Vlasov	k1gInSc1
vyslancům	vyslanec	k1gMnPc3
poslal	poslat	k5eAaPmAgInS
naproti	naproti	k6eAd1
nevelký	velký	k2eNgInSc1d1
oddíl	oddíl	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgInS
překvapen	překvapit	k5eAaPmNgMnS
jejich	jejich	k3xOp3gFnSc7
doprovodem	doprovod	k1gInSc7
–	–	k?
5	#num#	k4
000	#num#	k4
vojáky	voják	k1gMnPc7
s	s	k7c7
dělostřelectvem	dělostřelectvo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
se	se	k3xPyFc4
Mandžuové	Mandžu	k1gMnPc1
soustředili	soustředit	k5eAaPmAgMnP
v	v	k7c6
bezprostřední	bezprostřední	k2eAgFnSc6d1
blízkosti	blízkost	k1gFnSc6
Něrčinska	Něrčinsko	k1gNnSc2
<g/>
,	,	kIx,
za	za	k7c2
protestů	protest	k1gInPc2
Rusů	Rus	k1gMnPc2
(	(	kIx(
<g/>
Vlasova	Vlasův	k2eAgFnSc1d1
i	i	k8xC
Golovina	Golovina	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
stále	stále	k6eAd1
ještě	ještě	k6eAd1
na	na	k7c6
cestě	cesta	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
července	červenec	k1gInSc2
Mandžuové	Mandžu	k1gMnPc1
ustoupili	ustoupit	k5eAaPmAgMnP
k	k	k7c3
soutoku	soutok	k1gInSc3
Šilky	Šilka	k1gFnSc2
a	a	k8xC
Něrči	Něrč	k1gInPc7
<g/>
,	,	kIx,
dále	daleko	k6eAd2
však	však	k9
neodešli	odejít	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
do	do	k7c2
Něrčinska	Něrčinsko	k1gNnSc2
dorazil	dorazit	k5eAaPmAgMnS
Golovin	Golovina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mandžuové	Mandžu	k1gMnPc1
byli	být	k5eAaImAgMnP
zklamáni	zklamat	k5eAaPmNgMnP
přítomností	přítomnost	k1gFnSc7
jeho	jeho	k3xOp3gInPc2
pomocných	pomocný	k2eAgInPc2d1
burjatských	burjatský	k2eAgInPc2d1
a	a	k8xC
tunguzských	tunguzský	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Očekávali	očekávat	k5eAaImAgMnP
totiž	totiž	k9
<g/>
,	,	kIx,
že	že	k8xS
místní	místní	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rusy	Rus	k1gMnPc4
nepodpoří	podpořit	k5eNaPmIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednání	jednání	k1gNnSc1
v	v	k7c6
Nerčinsku	Nerčinsko	k1gNnSc6
a	a	k8xC
konec	konec	k1gInSc1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1689	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ruské	ruský	k2eAgFnPc1d1
(	(	kIx(
<g/>
růžově	růžově	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
čchingské	čchingský	k2eAgFnSc2d1
(	(	kIx(
<g/>
modře	modř	k1gFnSc2
<g/>
)	)	kIx)
území	území	k1gNnSc4
k	k	k7c3
počátku	počátek	k1gInSc3
roku	rok	k1gInSc2
1689	#num#	k4
a	a	k8xC
hranice	hranice	k1gFnPc1
podle	podle	k7c2
Něrčinské	Něrčinský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
(	(	kIx(
<g/>
černě	černě	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červeně	červeně	k6eAd1
vyznačen	vyznačit	k5eAaPmNgInS
ruský	ruský	k2eAgInSc1d1
návrh	návrh	k1gInSc1
hranice	hranice	k1gFnSc2
na	na	k7c6
Amuru	Amur	k1gInSc6
a	a	k8xC
světle	světlo	k1gNnSc6
modře	modro	k6eAd1
čchingský	čchingský	k2eAgInSc4d1
požadavek	požadavek	k1gInSc4
hranice	hranice	k1gFnSc2
u	u	k7c2
Bajkalu	Bajkal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Něrčinská	Něrčinský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Cílem	cíl	k1gInSc7
Mandžuů	Mandžu	k1gMnPc2
bylo	být	k5eAaImAgNnS
dosažení	dosažení	k1gNnSc1
souhlasu	souhlas	k1gInSc2
Ruska	Ruska	k1gFnSc1
s	s	k7c7
mandžuskou	mandžuský	k2eAgFnSc7d1
anexí	anexe	k1gFnSc7
ruských	ruský	k2eAgNnPc2d1
území	území	k1gNnPc2
na	na	k7c6
Amuru	Amur	k1gInSc6
a	a	k8xC
uzavření	uzavření	k1gNnSc6
míru	mír	k1gInSc2
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
by	by	kYmCp3nS
jim	on	k3xPp3gMnPc3
umožnilo	umožnit	k5eAaPmAgNnS
pustit	pustit	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
války	válka	k1gFnSc2
s	s	k7c7
Džúngary	Džúngar	k1gInPc7
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
naopak	naopak	k6eAd1
zabránilo	zabránit	k5eAaPmAgNnS
protičchingské	protičchingský	k2eAgFnSc3d1
džúngarsko-ruské	džúngarsko-ruský	k2eAgFnSc3d1
alianci	aliance	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
Něrčinskem	Něrčinsko	k1gNnSc7
měli	mít	k5eAaImAgMnP
kolem	kolem	k7c2
15	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
a	a	k8xC
50	#num#	k4
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rus	k1gMnPc1
měli	mít	k5eAaImAgMnP
původně	původně	k6eAd1
100	#num#	k4
vojáků	voják	k1gMnPc2
v	v	k7c6
Albazinu	Albazino	k1gNnSc6
a	a	k8xC
600	#num#	k4
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
<g/>
;	;	kIx,
po	po	k7c6
příchodu	příchod	k1gInSc6
Golovina	Golovina	k1gFnSc1
soustředili	soustředit	k5eAaPmAgMnP
v	v	k7c6
Něrčinsku	Něrčinsko	k1gNnSc6
kolem	kolem	k7c2
2	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
(	(	kIx(
<g/>
přibyl	přibýt	k5eAaPmAgInS
tisícičlenný	tisícičlenný	k2eAgInSc1d1
Golovinův	Golovinův	k2eAgInSc1d1
pluk	pluk	k1gInSc1
plus	plus	k1gInSc1
pomocné	pomocný	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
domorodců	domorodec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměr	poměr	k1gInSc1
sil	síla	k1gFnPc2
byl	být	k5eAaImAgInS
podobný	podobný	k2eAgInSc1d1
jako	jako	k9
při	při	k7c6
obležení	obležení	k1gNnSc6
Albazinu	Albazin	k1gInSc2
<g/>
,	,	kIx,
Něrčinsk	Něrčinsk	k1gInSc1
ale	ale	k8xC
neměl	mít	k5eNaImAgInS
tak	tak	k6eAd1
dobré	dobrý	k2eAgNnSc4d1
opevnění	opevnění	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
Jednání	jednání	k1gNnSc6
začala	začít	k5eAaPmAgFnS
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c4
Rusko	Rusko	k1gNnSc4
jednali	jednat	k5eAaImAgMnP
Fjodor	Fjodor	k1gInSc4
Golovin	Golovina	k1gFnPc2
a	a	k8xC
Ivan	Ivan	k1gMnSc1
Vlasov	Vlasov	k1gInSc4
<g/>
,	,	kIx,
za	za	k7c4
říši	říše	k1gFnSc4
Čching	Čching	k1gInSc4
Songgotu	Songgot	k1gInSc2
<g/>
,	,	kIx,
Tchun	Tchun	k1gMnSc1
Kuo-kang	Kuo-kang	k1gMnSc1
<g/>
,	,	kIx,
Langtan	Langtan	k1gInSc1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
Čchingové	Čchingový	k2eAgFnSc2d1
si	se	k3xPyFc3
nárokovali	nárokovat	k5eAaImAgMnP
země	zem	k1gFnSc2
až	až	k6eAd1
k	k	k7c3
Bajkalu	Bajkal	k1gInSc3
s	s	k7c7
odůvodněním	odůvodnění	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
kdysi	kdysi	k6eAd1
patřily	patřit	k5eAaImAgFnP
mongolské	mongolský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
Jüan	jüan	k1gInSc1
<g/>
;	;	kIx,
Rusové	Rus	k1gMnPc1
navrhovali	navrhovat	k5eAaImAgMnP
hranici	hranice	k1gFnSc4
na	na	k7c6
Amuru	Amur	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čchingská	Čchingský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
s	s	k7c7
Džúngary	Džúngar	k1gMnPc7
u	u	k7c2
Ulan	Ulana	k1gFnPc2
Butungu	Butung	k1gInSc2
(	(	kIx(
<g/>
1690	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrace	ilustrace	k1gFnSc1
z	z	k7c2
čínské	čínský	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
čchingského	čchingský	k2eAgNnSc2d1
období	období	k1gNnSc2
</s>
<s>
Čchingští	Čchingský	k2eAgMnPc1d1
vyslanci	vyslanec	k1gMnPc1
využili	využít	k5eAaPmAgMnP
svou	svůj	k3xOyFgFnSc4
absolutní	absolutní	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
k	k	k7c3
nátlaku	nátlak	k1gInSc3
na	na	k7c4
ruské	ruský	k2eAgMnPc4d1
vyjednavače	vyjednavač	k1gMnPc4
a	a	k8xC
donutili	donutit	k5eAaPmAgMnP
je	on	k3xPp3gInPc4
přijmout	přijmout	k5eAaPmF
většinu	většina	k1gFnSc4
čchingských	čchingský	k2eAgInPc2d1
požadavků	požadavek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Golovin	Golovina	k1gFnPc2
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
vzdal	vzdát	k5eAaPmAgMnS
Albazinu	Albazina	k1gFnSc4
a	a	k8xC
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
hranicí	hranice	k1gFnSc7
na	na	k7c6
horském	horský	k2eAgInSc6d1
hřbetu	hřbet	k1gInSc6
severně	severně	k6eAd1
od	od	k7c2
Amuru	Amur	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
Smlouva	smlouva	k1gFnSc1
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
27	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
tři	tři	k4xCgInPc1
články	článek	k1gInPc1
se	se	k3xPyFc4
týkaly	týkat	k5eAaImAgInP
hranic	hranice	k1gFnPc2
<g/>
,	,	kIx,
čtvrtý	čtvrtý	k4xOgMnSc1
zakotvoval	zakotvovat	k5eAaImAgMnS
vydávání	vydávání	k1gNnSc4
přeběhlíků	přeběhlík	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosavadní	dosavadní	k2eAgInPc1d1
přeběhlíky	přeběhlík	k1gMnPc4
Rusové	Rusové	k2eAgMnPc4d1
kategoricky	kategoricky	k6eAd1
odmítli	odmítnout	k5eAaPmAgMnP
vydat	vydat	k5eAaPmF
<g/>
,	,	kIx,
Mandžuové	Mandžu	k1gMnPc1
zase	zase	k9
nevydali	vydat	k5eNaPmAgMnP
zajatce	zajatec	k1gMnSc4
od	od	k7c2
Albazinu	Albazin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pátý	pátý	k4xOgInSc1
článek	článek	k1gInSc1
se	se	k3xPyFc4
týkal	týkat	k5eAaImAgInS
obchodu	obchod	k1gInSc2
otevřeného	otevřený	k2eAgMnSc2d1
pro	pro	k7c4
kupce	kupec	k1gMnSc4
s	s	k7c7
průvodními	průvodní	k2eAgInPc7d1
listy	list	k1gInPc7
<g/>
,	,	kIx,
šestý	šestý	k4xOgMnSc1
zakotvoval	zakotvovat	k5eAaImAgMnS
nenarušování	nenarušování	k1gNnSc4
hranic	hranice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
Moskvy	Moskva	k1gFnSc2
se	se	k3xPyFc4
první	první	k4xOgFnPc1
informace	informace	k1gFnPc1
o	o	k7c6
smlouvě	smlouva	k1gFnSc6
dostaly	dostat	k5eAaPmAgInP
koncem	koncem	k7c2
května	květen	k1gInSc2
1690	#num#	k4
<g/>
,	,	kIx,
podrobné	podrobný	k2eAgInPc1d1
v	v	k7c6
červnu	červen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Posolském	Posolský	k2eAgInSc6d1
prikaze	prikaz	k1gInSc6
se	se	k3xPyFc4
ke	k	k7c3
smlouvě	smlouva	k1gFnSc3
a	a	k8xC
Golovinovi	Golovinův	k2eAgMnPc1d1
postavili	postavit	k5eAaPmAgMnP
kriticky	kriticky	k6eAd1
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
po	po	k7c6
příjezdu	příjezd	k1gInSc6
do	do	k7c2
Moskvy	Moskva	k1gFnSc2
vláda	vláda	k1gFnSc1
vyjádřila	vyjádřit	k5eAaPmAgFnS
Golovinovi	Golovin	k1gMnSc3
uznání	uznání	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
říši	říše	k1gFnSc4
Čching	Čching	k1gInSc1
urovnání	urovnání	k1gNnSc2
vztahů	vztah	k1gInPc2
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
odvrátilo	odvrátit	k5eAaPmAgNnS
hrozbu	hrozba	k1gFnSc4
rusko-džúngarského	rusko-džúngarský	k2eAgNnSc2d1
spojenectví	spojenectví	k1gNnSc2
<g/>
,	,	kIx,
mohla	moct	k5eAaImAgFnS
se	se	k3xPyFc4
nyní	nyní	k6eAd1
soustředit	soustředit	k5eAaPmF
na	na	k7c4
válku	válka	k1gFnSc4
s	s	k7c7
Džúngary	Džúngar	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
Rusku	Ruska	k1gFnSc4
zase	zase	k9
uspořádání	uspořádání	k1gNnSc4
vztahů	vztah	k1gInPc2
s	s	k7c7
čchingskou	čchingský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
umožnilo	umožnit	k5eAaPmAgNnS
nerušený	rušený	k2eNgInSc4d1
dovoz	dovoz	k1gInSc4
čínského	čínský	k2eAgNnSc2d1
zboži	zbozat	k5eAaPmIp1nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
džúngarský	džúngarský	k2eAgMnSc1d1
chán	chán	k1gMnSc1
Galdan	Galdan	k1gMnSc1
se	se	k3xPyFc4
celé	celá	k1gFnSc2
následující	následující	k2eAgNnSc4d1
desetiletí	desetiletí	k1gNnSc4
snažil	snažit	k5eAaImAgMnS
získat	získat	k5eAaPmF
ruskou	ruský	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
<g/>
,	,	kIx,
neuspěl	uspět	k5eNaPmAgMnS
však	však	k9
<g/>
,	,	kIx,
protože	protože	k8xS
nebyl	být	k5eNaImAgInS
schopen	schopen	k2eAgInSc1d1
ruské	ruský	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
nabídnout	nabídnout	k5eAaPmF
náhradu	náhrada	k1gFnSc4
za	za	k7c4
obětování	obětování	k1gNnSc4
výnosného	výnosný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
s	s	k7c7
Čínou	Čína	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
nově	nově	k6eAd1
získaných	získaný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
v	v	k7c6
povodí	povodí	k1gNnSc6
Amuru	Amur	k1gInSc2
ponechala	ponechat	k5eAaPmAgFnS
čchingská	čchingský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
místní	místní	k2eAgFnSc2d1
kmeny	kmen	k1gInPc1
jejich	jejich	k3xOp3gInSc3
vlastnímu	vlastní	k2eAgInSc3d1
životu	život	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
Zakázala	zakázat	k5eAaPmAgFnS
komukoliv	kdokoliv	k3yInSc3
se	se	k3xPyFc4
na	na	k7c6
nich	on	k3xPp3gInPc6
usazovat	usazovat	k5eAaImF
a	a	k8xC
poté	poté	k6eAd1
o	o	k7c4
ně	on	k3xPp3gMnPc4
ztratila	ztratit	k5eAaPmAgFnS
zájem	zájem	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Rusko	Rusko	k1gNnSc1
prosadilo	prosadit	k5eAaPmAgNnS
posunutí	posunutí	k1gNnSc4
hranice	hranice	k1gFnSc2
na	na	k7c4
Amur	Amur	k1gInSc4
až	až	k9
po	po	k7c6
dvou	dva	k4xCgNnPc6
staletích	staletí	k1gNnPc6
<g/>
:	:	kIx,
ve	v	k7c6
čtyřicátých	čtyřicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
zahájilo	zahájit	k5eAaPmAgNnS
průzkum	průzkum	k1gInSc4
a	a	k8xC
kolonizaci	kolonizace	k1gFnSc4
Poamuří	Poamuř	k1gFnPc2
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1858	#num#	k4
<g/>
/	/	kIx~
<g/>
1860	#num#	k4
v	v	k7c6
Ajgunské	Ajgunský	k2eAgFnSc6d1
a	a	k8xC
Pekingské	pekingský	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
získalo	získat	k5eAaPmAgNnS
území	území	k1gNnSc4
severně	severně	k6eAd1
od	od	k7c2
Amuru	Amur	k1gInSc2
a	a	k8xC
východně	východně	k6eAd1
od	od	k7c2
Ussuri	Ussur	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Poslední	poslední	k2eAgInSc4d1
pokus	pokus	k1gInSc4
amurských	amurský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
o	o	k7c4
odpor	odpor	k1gInSc4
Mandžuové	Mandžu	k1gMnPc1
zlomili	zlomit	k5eAaPmAgMnP
roku	rok	k1gInSc2
1640	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
porazili	porazit	k5eAaPmAgMnP
a	a	k8xC
zajali	zajmout	k5eAaPmAgMnP
evenckého	evencký	k1gMnSc4
vůdce	vůdce	k1gMnSc4
Bombogora	Bombogor	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Na	na	k7c6
místě	místo	k1gNnSc6
tohoto	tento	k3xDgNnSc2
městečka	městečko	k1gNnSc2
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
polovině	polovina	k1gFnSc6
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
pevnost	pevnost	k1gFnSc4
Albazino	Albazino	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Tehdejší	tehdejší	k2eAgInSc4d1
ruský	ruský	k2eAgInSc4d1
název	název	k1gInSc4
pro	pro	k7c4
Sungari	Sungare	k1gFnSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Tehdejší	tehdejší	k2eAgInSc4d1
ruský	ruský	k2eAgInSc4d1
název	název	k1gInSc4
pro	pro	k7c4
Mandžuy	Mandžu	k1gMnPc4
a	a	k8xC
Číňany	Číňan	k1gMnPc4
<g/>
,	,	kIx,
podle	podle	k7c2
titulu	titul	k1gInSc2
bogdochán	bogdochán	k2eAgInSc1d1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
nosil	nosit	k5eAaImAgMnS
císař	císař	k1gMnSc1
říše	říš	k1gFnSc2
Čching	Čching	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
V	v	k7c6
originále	originál	k1gInSc6
<g/>
:	:	kIx,
«	«	k?
<g/>
И	И	k?
Ш	Ш	k?
н	н	k?
<g/>
,	,	kIx,
х	х	k?
т	т	k?
<g/>
,	,	kIx,
б	б	k?
<g />
.	.	kIx.
</s>
<s hack="1">
в	в	k?
л	л	k?
в	в	k?
и	и	k?
х	х	k?
н	н	k?
д	д	k?
<g/>
.	.	kIx.
И	И	k?
м	м	k?
<g/>
,	,	kIx,
х	х	k?
т	т	k?
<g/>
,	,	kIx,
п	п	k?
с	с	k?
у	у	k?
Ш	Ш	k?
в	в	k?
п	п	k?
А	А	k?
и	и	k?
п	п	k?
в	в	k?
н	н	k?
б	б	k?
х	х	k?
<g/>
,	,	kIx,
и	и	k?
в	в	k?
м	м	k?
<g/>
,	,	kIx,
х	х	k?
т	т	k?
<g/>
,	,	kIx,
п	п	k?
н	н	k?
в	в	k?
г	г	k?
<g/>
»	»	k?
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
26	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Obchod	obchod	k1gInSc1
s	s	k7c7
Čínou	Čína	k1gFnSc7
byl	být	k5eAaImAgInS
velmi	velmi	k6eAd1
výnosný	výnosný	k2eAgInSc1d1
<g/>
:	:	kIx,
z	z	k7c2
Tobolska	Tobolsk	k1gInSc2
se	se	k3xPyFc4
Ablin	Ablin	k1gInSc1
vypravil	vypravit	k5eAaPmAgInS
se	s	k7c7
zbožím	zboží	k1gNnSc7
–	–	k?
suknem	sukno	k1gNnSc7
a	a	k8xC
kožešinami	kožešina	k1gFnPc7
–	–	k?
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
4	#num#	k4
539	#num#	k4
rublů	rubl	k1gInPc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
darů	dar	k1gInPc2
čchingskému	čchingské	k1gNnSc3
císaři	císař	k1gMnPc1
za	za	k7c2
380	#num#	k4
rublů	rubl	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Pekingu	Peking	k1gInSc6
obdržel	obdržet	k5eAaPmAgMnS
od	od	k7c2
císaře	císař	k1gMnSc2
dary	dar	k1gInPc4
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
1	#num#	k4
045	#num#	k4
rublů	rubl	k1gInPc2
<g/>
,	,	kIx,
své	svůj	k3xOyFgNnSc4
zboží	zboží	k1gNnSc4
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
(	(	kIx(
<g/>
moskevské	moskevský	k2eAgFnSc3d1
ceně	cena	k1gFnSc3
<g/>
)	)	kIx)
3	#num#	k4
480	#num#	k4
rublů	rubl	k1gInPc2
prodal	prodat	k5eAaPmAgInS
za	za	k7c4
9	#num#	k4
206	#num#	k4
rublů	rubl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vydělaných	vydělaný	k2eAgInPc2d1
peněz	peníze	k1gInPc2
použil	použít	k5eAaPmAgInS
8	#num#	k4
876	#num#	k4
rublů	rubl	k1gInPc2
na	na	k7c4
zakoupení	zakoupení	k1gNnSc4
čínského	čínský	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
cena	cena	k1gFnSc1
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
dosahovala	dosahovat	k5eAaImAgFnS
18	#num#	k4
751	#num#	k4
rublů	rubl	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
↑	↑	k?
Gantimur	Gantimur	k1gMnSc1
<g/>
,	,	kIx,
náčelník	náčelník	k1gMnSc1
Neljudů	Neljud	k1gInPc2
a	a	k8xC
Daurů	Daur	k1gInPc2
na	na	k7c6
Šilce	Šilka	k1gFnSc6
<g/>
,	,	kIx,
přijal	přijmout	k5eAaPmAgMnS
roku	rok	k1gInSc2
1651	#num#	k4
dobrovolně	dobrovolně	k6eAd1
ruské	ruský	k2eAgNnSc1d1
poddanství	poddanství	k1gNnSc1
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
cca	cca	kA
500	#num#	k4
bojovníků	bojovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
50	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
mandžuská	mandžuský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
odehnala	odehnat	k5eAaPmAgFnS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
lid	lid	k1gInSc4
na	na	k7c4
jih	jih	k1gInSc4
na	na	k7c4
řeku	řeka	k1gFnSc4
Non	Non	k1gFnSc2
(	(	kIx(
<g/>
přítok	přítok	k1gInSc1
Sungari	Sungar	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nimi	on	k3xPp3gMnPc7
odešel	odejít	k5eAaPmAgMnS
i	i	k9
Gantimur	Gantimur	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
Čchingů	Čching	k1gInPc2
dostal	dostat	k5eAaPmAgInS
plat	plat	k1gInSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
úřednickou	úřednický	k2eAgFnSc4d1
hodnost	hodnost	k1gFnSc4
a	a	k8xC
pravomoc	pravomoc	k1gFnSc4
nad	nad	k7c7
svými	svůj	k3xOyFgMnPc7
lidmi	člověk	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
aktivizaci	aktivizace	k1gFnSc6
Rusů	Rus	k1gMnPc2
na	na	k7c6
Amuru	Amur	k1gInSc6
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
Čchingové	Čchingový	k2eAgFnPc1d1
přikázali	přikázat	k5eAaPmAgMnP
Gantimurovi	Gantimur	k1gMnSc3
zaútočit	zaútočit	k5eAaPmF
na	na	k7c4
Rusy	Rus	k1gMnPc4
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
však	však	k9
při	při	k7c6
první	první	k4xOgFnSc6
příležitosti	příležitost	k1gFnSc6
–	–	k?
roku	rok	k1gInSc2
1667	#num#	k4
–	–	k?
se	s	k7c7
všemi	všecek	k3xTgInPc7
svými	svůj	k3xOyFgMnPc7
lidmi	člověk	k1gMnPc7
přešel	přejít	k5eAaPmAgMnS
zpět	zpět	k6eAd1
na	na	k7c4
ruské	ruský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čchingové	Čchingový	k2eAgInPc1d1
v	v	k7c6
tom	ten	k3xDgNnSc6
viděli	vidět	k5eAaImAgMnP
nebezpečný	bezpečný	k2eNgInSc4d1
precedent	precedent	k1gInSc4
a	a	k8xC
projev	projev	k1gInSc4
nálad	nálada	k1gFnPc2
mezi	mezi	k7c7
místními	místní	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
<g/>
,	,	kIx,
protestovali	protestovat	k5eAaBmAgMnP
u	u	k7c2
něrčinského	něrčinský	k2eAgMnSc2d1
vojevody	vojevoda	k1gMnSc2
a	a	k8xC
žádali	žádat	k5eAaImAgMnP
Gantimurovo	Gantimurův	k2eAgNnSc4d1
vydání	vydání	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Tehdejší	tehdejší	k2eAgMnSc1d1
irkutský	irkutský	k2eAgMnSc1d1
vojevoda	vojevoda	k1gMnSc1
v	v	k7c6
hlášení	hlášení	k1gNnSc6
pro	pro	k7c4
Konstantina	Konstantin	k1gMnSc4
Ščerbatova	Ščerbatův	k2eAgMnSc4d1
v	v	k7c6
Jenisejsku	Jenisejsek	k1gInSc6
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
mongolské	mongolský	k2eAgNnSc4d1
„	„	k?
<g/>
desetitisícové	desetitisícový	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
vojsko	vojsko	k1gNnSc1
<g/>
“	“	k?
obléhalo	obléhat	k5eAaImAgNnS
Tunkinský	Tunkinský	k2eAgInSc4d1
ostroh	ostroh	k1gInSc4
měsíc	měsíc	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Na	na	k7c6
opevnění	opevnění	k1gNnSc6
Albazinu	Albazin	k1gInSc2
jsou	být	k5eAaImIp3nP
nápadné	nápadný	k2eAgFnPc1d1
bastiony	bastiona	k1gFnPc1
evropského	evropský	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
,	,	kIx,
se	s	k7c7
šikmými	šikmý	k2eAgFnPc7d1
stěnami	stěna	k1gFnPc7
<g/>
,	,	kIx,
vhodné	vhodný	k2eAgNnSc1d1
pro	pro	k7c4
boční	boční	k2eAgFnSc4d1
palbu	palba	k1gFnSc4
<g/>
,	,	kIx,
kontrastující	kontrastující	k2eAgInSc1d1
s	s	k7c7
mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
obranyschopnějším	obranyschopný	k2eAgNnSc7d2
opevněním	opevnění	k1gNnSc7
čchingského	čchingský	k2eAgInSc2d1
tábora	tábor	k1gInSc2
na	na	k7c6
spodu	spod	k1gInSc6
rytiny	rytina	k1gFnSc2
s	s	k7c7
jeho	jeho	k3xOp3gFnPc7
obdélnými	obdélný	k2eAgFnPc7d1
vysunutými	vysunutý	k2eAgFnPc7d1
branami	brána	k1gFnPc7
<g/>
,	,	kIx,
charakteristickými	charakteristický	k2eAgInPc7d1
pro	pro	k7c4
čínské	čínský	k2eAgNnSc4d1
pevnostní	pevnostní	k2eAgNnSc4d1
stavitelství	stavitelství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://runivers.ru/vh/kitayskaya_1686.php	https://runivers.ru/vh/kitayskaya_1686.php	k1gInSc1
<g/>
↑	↑	k?
Р	Р	k?
о	о	k?
1689	#num#	k4
<g/>
—	—	k?
<g/>
1916	#num#	k4
<g/>
.	.	kIx.
О	О	k?
д	д	k?
<g/>
,	,	kIx,
М	М	k?
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Р	Р	k?
о	о	k?
1689	#num#	k4
<g/>
—	—	k?
<g/>
1916	#num#	k4
<g/>
.	.	kIx.
О	О	k?
д	д	k?
<g/>
,	,	kIx,
М	М	k?
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Н	Н	k?
<g/>
,	,	kIx,
О	О	k?
Е	Е	k?
<g/>
.	.	kIx.
И	И	k?
К	К	k?
<g/>
.	.	kIx.
Э	Э	k?
Ц	Ц	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
XVII	XVII	kA
–	–	k?
н	н	k?
XX	XX	kA
в	в	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
В	В	k?
л	л	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
712	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
18400	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
82	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Nepomnin	Nepomnin	k2eAgMnSc1d1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Nepomnin	Nepomnin	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
73	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
А	А	k?
<g/>
,	,	kIx,
В	В	k?
А	А	k?
<g/>
.	.	kIx.
Р	Р	k?
н	н	k?
д	д	k?
р	р	k?
(	(	kIx(
<g/>
в	в	k?
п	п	k?
XVII	XVII	kA
в	в	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Н	Н	k?
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Alexandrov	Alexandrov	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
7.1	7.1	k4
2	#num#	k4
LI	li	k8xS
<g/>
,	,	kIx,
Gertraude	Gertraud	k1gMnSc5
Roth	Roth	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
State	status	k1gInSc5
Building	Building	k1gInSc1
before	befor	k1gInSc5
1644	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
PETERSON	PETERSON	kA
<g/>
,	,	kIx,
Willard	Willard	k1gMnSc1
J.	J.	kA
The	The	k1gMnSc1
Cambridge	Cambridge	k1gFnSc2
History	Histor	k1gInPc4
of	of	k?
China	China	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volume	volum	k1gInSc5
9	#num#	k4
<g/>
:	:	kIx,
Part	part	k1gInSc1
One	One	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gFnSc2
Ch	Ch	kA
<g/>
’	’	k?
<g/>
ing	ing	kA
Empire	empir	k1gInSc5
to	ten	k3xDgNnSc1
1800	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
UK	UK	kA
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
24334	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
–	–	k?
<g/>
72	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
s.	s.	k?
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
CROSSLEY	CROSSLEY	kA
<g/>
,	,	kIx,
Pamela	Pamela	k1gFnSc1
Kyle	Kyle	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Translucent	Translucent	k1gMnSc1
Mirror	Mirror	k1gMnSc1
<g/>
:	:	kIx,
History	Histor	k1gInPc1
and	and	k?
Identity	identita	k1gFnSc2
in	in	k?
Qing	Qing	k1gMnSc1
Imperial	Imperial	k1gMnSc1
Ideology	ideolog	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berkeley	Berkelea	k1gFnPc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
California	Californium	k1gNnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
417	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
520928849	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9780520928848	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
196	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
9.1	9.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
49	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PERDUE	PERDUE	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
C.	C.	kA
China	China	k1gFnSc1
marches	marches	k1gMnSc1
west	west	k1gMnSc1
:	:	kIx,
the	the	k?
Qing	Qing	k1gMnSc1
conquest	conquest	k1gMnSc1
of	of	k?
Central	Central	k1gMnSc4
Eurasia	Eurasius	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
:	:	kIx,
Belknap	Belknap	k1gInSc1
Press	Press	k1gInSc1
of	of	k?
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780674057432	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
87	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Perdue	Perdue	k1gFnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KANG	KANG	kA
<g/>
,	,	kIx,
Hyeok	Hyeok	k1gInSc4
Hweon	Hweona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Big	Big	k1gFnSc1
Heads	Headsa	k1gFnPc2
and	and	k?
Buddhist	Buddhist	k1gInSc1
Demons	Demons	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Korean	Korean	k1gMnSc1
Musketry	Musketr	k1gInPc1
Revolution	Revolution	k1gInSc1
and	and	k?
the	the	k?
Northern	Northern	k1gInSc1
Expeditions	Expeditions	k1gInSc1
of	of	k?
1654	#num#	k4
and	and	k?
1658	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Chinese	Chinese	k1gFnSc2
Military	Militara	k1gFnSc2
History	Histor	k1gInPc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
127	#num#	k4
<g/>
–	–	k?
<g/>
189	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
s.	s.	k?
135	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Kang	Kang	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Kang	Kanga	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
136	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nepomnin	Nepomnin	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
74.1	74.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Nepomnin	Nepomnina	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
98	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Б	Б	k?
<g/>
,	,	kIx,
Е	Е	k?
Л	Л	k?
П	П	k?
в	в	k?
с	с	k?
р	р	k?
о	о	k?
<g/>
.	.	kIx.
Х	Х	k?
<g/>
:	:	kIx,
К	К	k?
<g/>
.	.	kIx.
и	и	k?
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
40	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
M.	M.	kA
<g/>
:	:	kIx,
Н	Н	k?
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Bezprozvannych	Bezprozvannych	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Kang	Kang	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
137	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
В	В	k?
<g/>
,	,	kIx,
Ю	Ю	k?
М	М	k?
Г	Г	k?
и	и	k?
А	А	k?
г	г	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
В	В	k?
Д	Д	k?
Р	Р	k?
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
179	#num#	k4
<g/>
–	–	k?
<g/>
188	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
М	М	k?
<g/>
,	,	kIx,
А	А	k?
Н	Н	k?
<g/>
;	;	kIx,
Ш	Ш	k?
<g/>
,	,	kIx,
В	В	k?
Г	Г	k?
А	А	k?
с	с	k?
н	н	k?
н	н	k?
А	А	k?
в	в	k?
1652	#num#	k4
г	г	k?
<g/>
.	.	kIx.
В	В	k?
ж	ж	k?
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
72	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HUMMEL	HUMMEL	kA
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
William	William	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eminent	Eminent	k1gInSc1
Chinese	Chinese	k1gFnSc2
of	of	k?
the	the	k?
Ch	Ch	kA
<g/>
'	'	kIx"
<g/>
ing	ing	kA
period	perioda	k1gFnPc2
(	(	kIx(
<g/>
1644	#num#	k4
<g/>
-	-	kIx~
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
:	:	kIx,
Library	Librara	k1gFnPc1
of	of	k?
Congress	Congress	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orientalia	Orientalius	k1gMnSc2
Division	Division	k1gInSc4
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
632	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Hummel	Hummel	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
11.1	11.1	k4
2	#num#	k4
Kang	Kanga	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
160	#num#	k4
<g/>
–	–	k?
<g/>
161.1	161.1	k4
2	#num#	k4
3	#num#	k4
Kang	Kanga	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
163	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Ч	Ч	k?
«	«	k?
<g/>
о	о	k?
<g/>
»	»	k?
а	а	k?
с	с	k?
л	л	k?
Я	Я	k?
Н	Н	k?
с	с	k?
т	т	k?
<g/>
,	,	kIx,
п	п	k?
п	п	k?
ч	ч	k?
О	О	k?
С	С	k?
<g/>
,	,	kIx,
с	с	k?
о	о	k?
и	и	k?
п	п	k?
п	п	k?
А	А	k?
и	и	k?
с	с	k?
с	с	k?
м	м	k?
в	в	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
В	В	k?
Л	Л	k?
<g/>
.	.	kIx.
С	С	k?
и	и	k?
и	и	k?
В	В	k?
и	и	k?
З	З	k?
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
Л	Л	k?
А	А	k?
С	С	k?
<g/>
,	,	kIx,
ф	ф	k?
П	П	k?
М	М	k?
<g/>
,	,	kIx,
о	о	k?
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
к	к	k?
<g/>
.	.	kIx.
31	#num#	k4
<g/>
,	,	kIx,
№	№	k?
52	#num#	k4
<g/>
,	,	kIx,
л	л	k?
<g/>
.	.	kIx.
87	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
-	-	kIx~
<g/>
88	#num#	k4
<g/>
.	.	kIx.
К	К	k?
XVIII	XVIII	kA
в	в	k?
с	с	k?
с	с	k?
<g/>
..	..	k?
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Л	Л	k?
<g/>
,	,	kIx,
Г	Г	k?
А	А	k?
З	З	k?
Е	Е	k?
П	П	k?
Х	Х	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
П	П	k?
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
144	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1904	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
С	С	k?
Х	Х	k?
в	в	k?
<g/>
,	,	kIx,
s.	s.	k?
107	#num#	k4
<g/>
–	–	k?
<g/>
114	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Kang	Kanga	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
164	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kang	Kang	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
165	#num#	k4
<g/>
–	–	k?
<g/>
166.1	166.1	k4
2	#num#	k4
Kang	Kanga	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
167.1	167.1	k4
2	#num#	k4
П	П	k?
<g/>
,	,	kIx,
А	А	k?
М	М	k?
К	К	k?
п	п	k?
т	т	k?
с	с	k?
в	в	k?
XVII	XVII	kA
в	в	k?
и	и	k?
п	п	k?
у	у	k?
к	к	k?
в	в	k?
в	в	k?
А	А	k?
п	п	k?
м	м	k?
а	а	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
К	К	k?
<g/>
,	,	kIx,
Л	Л	k?
Р	Р	k?
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
Р	Р	k?
к	к	k?
<g/>
.	.	kIx.
А	А	k?
<g/>
.	.	kIx.
В	В	k?
ч	ч	k?
№	№	k?
4	#num#	k4
<g/>
..	..	k?
М	М	k?
<g/>
:	:	kIx,
М	М	k?
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
118	#num#	k4
<g/>
–	–	k?
<g/>
144	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
С	С	k?
<g/>
,	,	kIx,
Т	Т	k?
М	М	k?
У	У	k?
к	к	k?
о	о	k?
в	в	k?
А	А	k?
в	в	k?
1654	#num#	k4
и	и	k?
1658	#num#	k4
<g/>
:	:	kIx,
И	И	k?
и	и	k?
и	и	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Т	Т	k?
к	к	k?
В	В	k?
А	А	k?
<g/>
:	:	kIx,
с	с	k?
<g/>
.	.	kIx.
с	с	k?
<g/>
.	.	kIx.
-	-	kIx~
В	В	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
Б	Б	k?
<g/>
:	:	kIx,
И	И	k?
А	А	k?
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
179	#num#	k4
<g/>
–	–	k?
<g/>
188	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Kang	Kang	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
169	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SPENCE	SPENCE	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
K	k	k7c3
<g/>
’	’	k?
<g/>
ang-hsi	ang-hse	k1gFnSc3
Reign	Reign	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
PETERSON	PETERSON	kA
<g/>
,	,	kIx,
Willard	Willard	k1gMnSc1
J.	J.	kA
The	The	k1gMnSc1
Cambridge	Cambridge	k1gFnSc2
History	Histor	k1gInPc4
of	of	k?
China	China	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volume	volum	k1gInSc5
9	#num#	k4
<g/>
:	:	kIx,
Part	part	k1gInSc1
One	One	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gFnSc2
Ch	Ch	kA
<g/>
’	’	k?
<g/>
ing	ing	kA
Empire	empir	k1gInSc5
to	ten	k3xDgNnSc1
1800	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
UK	UK	kA
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
24334	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
120	#num#	k4
<g/>
–	–	k?
<g/>
229	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
s.	s.	k?
177	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Perdue	Perdue	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
165	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
28	#num#	k4
a	a	k8xC
34	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
32	#num#	k4
<g/>
–	–	k?
<g/>
33	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Э	Э	k?
З	З	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
А	А	k?
о	о	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
М	М	k?
э	э	k?
З	З	k?
<g/>
.	.	kIx.
А	А	k?
/	/	kIx~
г	г	k?
<g/>
.	.	kIx.
р	р	k?
<g/>
.	.	kIx.
Р	Р	k?
Ф	Ф	k?
Г	Г	k?
<g/>
.	.	kIx.
–	–	k?
Н	Н	k?
<g/>
:	:	kIx,
Н	Н	k?
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
;	;	kIx,
autoři	autor	k1gMnPc1
hesla	heslo	k1gNnSc2
К	К	k?
Н	Н	k?
<g/>
,	,	kIx,
Ф	Ф	k?
Р	Р	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
П	П	k?
Ф	Ф	k?
И	И	k?
Б	Б	k?
в	в	k?
К	К	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přepis	přepis	k1gInSc1
z	z	k7c2
Д	Д	k?
Н	Н	k?
Ф	Ф	k?
<g/>
,	,	kIx,
М	М	k?
В	В	k?
С	С	k?
П	П	k?
р	р	k?
д	д	k?
в	в	k?
К	К	k?
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Р	Р	k?
<g/>
"	"	kIx"
И	И	k?
П	П	k?
и	и	k?
с	с	k?
с	с	k?
Ф	Ф	k?
И	И	k?
Б	Б	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
М	М	k?
Н	Н	k?
<g/>
.	.	kIx.
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
В	В	k?
<g/>
,	,	kIx,
Г	Г	k?
В	В	k?
<g/>
.	.	kIx.
М	М	k?
ц	ц	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
Т	Т	k?
<g/>
:	:	kIx,
Л	Л	k?
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
85929	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
С	С	k?
<g/>
,	,	kIx,
м	м	k?
<g/>
,	,	kIx,
к	к	k?
и	и	k?
б	б	k?
<g/>
,	,	kIx,
1654-1667	1654-1667	k4
г	г	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
62	#num#	k4
<g/>
–	–	k?
<g/>
76	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Э	Э	k?
З	З	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
П	П	k?
Б	Б	k?
1654	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Р	Р	k?
<g/>
.	.	kIx.
<g/>
-к	-к	k?
<g/>
.	.	kIx.
о	о	k?
в	в	k?
XVII	XVII	kA
в	в	k?
<g/>
:	:	kIx,
м	м	k?
и	и	k?
д	д	k?
<g/>
.	.	kIx.
–	–	k?
М	М	k?
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
–	–	k?
Т	Т	k?
1	#num#	k4
<g/>
.	.	kIx.
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
hesla	heslo	k1gNnSc2
К	К	k?
И	И	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
М	М	k?
<g/>
,	,	kIx,
В	В	k?
С	С	k?
<g/>
.	.	kIx.
И	И	k?
Ц	Ц	k?
и	и	k?
Р	Р	k?
г	г	k?
в	в	k?
XVII	XVII	kA
в	в	k?
<g/>
.	.	kIx.
Х	Х	k?
<g/>
:	:	kIx,
Х	Х	k?
к	к	k?
и	и	k?
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
S.	S.	kA
150	#num#	k4
<g/>
–	–	k?
<g/>
151	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Mjasnikov	Mjasnikov	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Mjasnikov	Mjasnikov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
152	#num#	k4
<g/>
–	–	k?
<g/>
153	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mjasnikov	Mjasnikov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
154	#num#	k4
<g/>
–	–	k?
<g/>
155	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mjasnikov	Mjasnikov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
164	#num#	k4
<g/>
–	–	k?
<g/>
165	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ф	Ф	k?
<g/>
,	,	kIx,
В	В	k?
М	М	k?
Н	Н	k?
д	д	k?
о	о	k?
п	п	k?
С	С	k?
А	А	k?
<g/>
.	.	kIx.
С	С	k?
к	к	k?
<g/>
.	.	kIx.
1958	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
50	#num#	k4
<g/>
–	–	k?
<g/>
51	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mjasnikov	Mjasnikov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
168	#num#	k4
a	a	k8xC
184	#num#	k4
<g/>
–	–	k?
<g/>
185.1	185.1	k4
2	#num#	k4
Mjasnikov	Mjasnikov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
184	#num#	k4
<g/>
–	–	k?
<g/>
185	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mjasnikov	Mjasnikov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
162	#num#	k4
<g/>
–	–	k?
<g/>
163	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mjasnikov	Mjasnikov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
168.1	168.1	k4
2	#num#	k4
Э	Э	k?
З	З	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
П	П	k?
М	М	k?
1670	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
hesla	heslo	k1gNnSc2
К	К	k?
И	И	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mjasnikov	Mjasnikov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
173	#num#	k4
<g/>
–	–	k?
<g/>
174.1	174.1	k4
2	#num#	k4
3	#num#	k4
Mjasnikov	Mjasnikov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
176	#num#	k4
<g/>
–	–	k?
<g/>
179.1	179.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
53	#num#	k4
<g/>
–	–	k?
<g/>
54.1	54.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
57	#num#	k4
<g/>
–	–	k?
<g/>
58	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
55	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
56.1	56.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
59	#num#	k4
<g/>
–	–	k?
<g/>
60.1	60.1	k4
2	#num#	k4
В	В	k?
<g/>
,	,	kIx,
Г	Г	k?
В	В	k?
<g/>
.	.	kIx.
М	М	k?
ц	ц	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
Т	Т	k?
<g/>
:	:	kIx,
Л	Л	k?
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
85929	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Р	Р	k?
о	о	k?
(	(	kIx(
<g/>
1667	#num#	k4
<g/>
-	-	kIx~
<g/>
1682	#num#	k4
г	г	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
197	#num#	k4
<g/>
–	–	k?
<g/>
208	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
61	#num#	k4
<g/>
–	–	k?
<g/>
62.1	62.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
101	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bezprozvannych	Bezprozvannych	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
56	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
102	#num#	k4
<g/>
–	–	k?
<g/>
103.1	103.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
104	#num#	k4
<g/>
–	–	k?
<g/>
105.1	105.1	k4
2	#num#	k4
3	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
110	#num#	k4
<g/>
–	–	k?
<g/>
112.1	112.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
HUMMEL	HUMMEL	kA
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
William	William	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eminent	Eminent	k1gInSc1
Chinese	Chinese	k1gFnSc2
of	of	k?
the	the	k?
Ch	Ch	kA
<g/>
'	'	kIx"
<g/>
ing	ing	kA
period	perioda	k1gFnPc2
(	(	kIx(
<g/>
1644	#num#	k4
<g/>
-	-	kIx~
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
:	:	kIx,
Library	Librara	k1gFnPc1
of	of	k?
Congress	Congress	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orientalia	Orientalius	k1gMnSc2
Division	Division	k1gInSc4
<g/>
,	,	kIx,
1943	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
442	#num#	k4
<g/>
–	–	k?
<g/>
443	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
106	#num#	k4
<g/>
–	–	k?
<g/>
107.1	107.1	k4
2	#num#	k4
Bezprozvannych	Bezprozvannycha	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
58	#num#	k4
<g/>
–	–	k?
<g/>
59	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
113	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
114	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
Bezprozvannych	Bezprozvannych	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
60	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
114	#num#	k4
<g/>
–	–	k?
<g/>
115.1	115.1	k4
2	#num#	k4
3	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
127	#num#	k4
<g/>
–	–	k?
<g/>
128.1	128.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
120	#num#	k4
<g/>
–	–	k?
<g/>
122	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
123	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
И	И	k?
у	у	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
И	И	k?
<g/>
,	,	kIx,
2011-2017	2011-2017	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
А	А	k?
<g/>
,	,	kIx,
М	М	k?
Е	Е	k?
р	р	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
Н	Н	k?
<g/>
:	:	kIx,
Б	Б	k?
с	с	k?
к	к	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
115	#num#	k4
<g/>
–	–	k?
<g/>
116	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
116	#num#	k4
<g/>
–	–	k?
<g/>
117.1	117.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
118	#num#	k4
<g/>
–	–	k?
<g/>
119.1	119.1	k4
2	#num#	k4
Э	Э	k?
З	З	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
С	С	k?
о	о	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
hesla	heslo	k1gNnSc2
К	К	k?
И	И	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bezprozvannych	Bezprozvannych	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
61.1	61.1	k4
2	#num#	k4
3	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
129.1	129.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
130	#num#	k4
<g/>
–	–	k?
<g/>
131.1	131.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
131	#num#	k4
<g/>
–	–	k?
<g/>
133	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bezprozvannych	Bezprozvannych	k1gInSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
s.	s.	k?
62	#num#	k4
<g/>
–	–	k?
<g/>
63.1	63.1	k4
2	#num#	k4
Bezprozvannych	Bezprozvannycha	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
64	#num#	k4
<g/>
–	–	k?
<g/>
65.1	65.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
135.1	135.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
133	#num#	k4
<g/>
–	–	k?
<g/>
134.1	134.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
136	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
137	#num#	k4
<g/>
–	–	k?
<g/>
138	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ANDRADE	ANDRADE	kA
<g/>
,	,	kIx,
Tonio	Tonio	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Gunpowder	Gunpowder	k1gMnSc1
Age	Age	k1gMnSc1
<g/>
:	:	kIx,
China	China	k1gFnSc1
<g/>
,	,	kIx,
Military	Militara	k1gFnPc1
Innovation	Innovation	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
the	the	k?
Rise	Rise	k1gInSc1
of	of	k?
the	the	k?
West	West	k1gInSc1
in	in	k?
World	World	k1gInSc1
History	Histor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princenton	Princenton	k1gInSc1
<g/>
:	:	kIx,
Princenton	Princenton	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
448	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
1400874440	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781400874446	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
177	#num#	k4
a	a	k8xC
181	#num#	k4
<g/>
–	–	k?
<g/>
184	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
139.1	139.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
А	А	k?
<g/>
,	,	kIx,
А	А	k?
Р	Р	k?
Н	Н	k?
м	м	k?
о	о	k?
г	г	k?
о	о	k?
А	А	k?
о	о	k?
в	в	k?
1685	#num#	k4
и	и	k?
1686	#num#	k4
<g/>
—	—	k?
<g/>
1687	#num#	k4
г	г	k?
<g/>
.	.	kIx.
В	В	k?
<g/>
.	.	kIx.
Д	Д	k?
<g/>
.	.	kIx.
о	о	k?
Р	Р	k?
<g/>
.	.	kIx.
А	А	k?
<g/>
.	.	kIx.
н	н	k?
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
/	/	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
129	#num#	k4
<g/>
–	–	k?
<g/>
136	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
140	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
141.1	141.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
142.1	142.1	k4
2	#num#	k4
3	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
146	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
150	#num#	k4
<g/>
–	–	k?
<g/>
151	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
142	#num#	k4
<g/>
–	–	k?
<g/>
143	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
173	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
174	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
148	#num#	k4
<g/>
–	–	k?
<g/>
149	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
150.1	150.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
152	#num#	k4
<g/>
–	–	k?
<g/>
153.1	153.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
154.1	154.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
155.1	155.1	k4
2	#num#	k4
3	#num#	k4
Э	Э	k?
З	З	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
П	П	k?
В	В	k?
и	и	k?
Ф	Ф	k?
1686	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Р	Р	k?
<g/>
,	,	kIx,
С	С	k?
<g/>
.	.	kIx.
п	п	k?
<g/>
,	,	kIx,
с	с	k?
<g/>
.	.	kIx.
965	#num#	k4
<g/>
;	;	kIx,
Р	Р	k?
<g/>
.	.	kIx.
<g/>
-к	-к	k?
<g/>
.	.	kIx.
о	о	k?
в	в	k?
XVII	XVII	kA
в	в	k?
<g/>
:	:	kIx,
м	м	k?
и	и	k?
д	д	k?
<g/>
.	.	kIx.
–	–	k?
М	М	k?
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
–	–	k?
Т	Т	k?
2	#num#	k4
<g/>
.	.	kIx.
<g/>
;	;	kIx,
autor	autor	k1gMnSc1
hesla	heslo	k1gNnSc2
К	К	k?
И	И	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
143	#num#	k4
<g/>
–	–	k?
<g/>
144.1	144.1	k4
2	#num#	k4
3	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
144	#num#	k4
<g/>
–	–	k?
<g/>
145	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
175.1	175.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
157.1	157.1	k4
2	#num#	k4
3	#num#	k4
Perdue	Perdue	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
148.1	148.1	k4
2	#num#	k4
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
4	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
159.1	159.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
160	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
161.1	161.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
163	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
162.1	162.1	k4
2	#num#	k4
3	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
165.1	165.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
164	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hummel	Hummel	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
663	#num#	k4
<g/>
–	–	k?
<g/>
666	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
176	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
178.1	178.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
168	#num#	k4
<g/>
–	–	k?
<g/>
169	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
172.1	172.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
169	#num#	k4
<g/>
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
171.1	171.1	k4
2	#num#	k4
Perdue	Perdue	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
149	#num#	k4
<g/>
–	–	k?
<g/>
150.1	150.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
166	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexandrov	Alexandrov	k1gInSc1
<g/>
,	,	kIx,
167.1	167.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
179	#num#	k4
<g/>
–	–	k?
<g/>
180	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Э	Э	k?
З	З	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
П	П	k?
Л	Л	k?
1687	#num#	k4
<g/>
-	-	kIx~
<g/>
89	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
hesla	heslo	k1gNnSc2
К	К	k?
И	И	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
183	#num#	k4
<g/>
–	–	k?
<g/>
184.1	184.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
181	#num#	k4
<g/>
–	–	k?
<g/>
182.1	182.1	k4
2	#num#	k4
3	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
185	#num#	k4
<g/>
–	–	k?
<g/>
186.1	186.1	k4
2	#num#	k4
Perdue	Perdue	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
166	#num#	k4
<g/>
–	–	k?
<g/>
167.1	167.1	k4
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
187	#num#	k4
<g/>
–	–	k?
<g/>
188	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Perdue	Perdue	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
169.1	169.1	k4
2	#num#	k4
Alexandrov	Alexandrovo	k1gNnPc2
<g/>
,	,	kIx,
189	#num#	k4
<g/>
–	–	k?
<g/>
190	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Perdue	Perdue	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
138	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Perdue	Perdue	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
172	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
FLETCHER	FLETCHER	kA
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ch	Ch	kA
<g/>
'	'	kIx"
<g/>
ing	ing	kA
Inner	Inner	k1gInSc1
Asia	Asia	k1gFnSc1
c.	c.	k?
1800	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
FAIRBANK	FAIRBANK	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
K.	K.	kA
The	The	k1gMnSc1
Cambridge	Cambridge	k1gFnSc2
History	Histor	k1gInPc4
of	of	k?
China	China	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volume	volum	k1gInSc5
10	#num#	k4
<g/>
:	:	kIx,
Late	lat	k1gInSc5
Ch	Ch	kA
<g/>
'	'	kIx"
<g/>
ing	ing	kA
<g/>
,	,	kIx,
1800	#num#	k4
<g/>
-	-	kIx~
<g/>
1911	#num#	k4
<g/>
,	,	kIx,
Part	part	k1gInSc1
I.	I.	kA
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
UK	UK	kA
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
21447	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
35	#num#	k4
<g/>
–	–	k?
<g/>
106	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
s.	s.	k?
38	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
FLETCHER	FLETCHER	kA
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sino-Russian	Sino-Russiany	k1gInPc2
relations	relationsa	k1gFnPc2
<g/>
,	,	kIx,
1800	#num#	k4
<g/>
-	-	kIx~
<g/>
62	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
FAIRBANK	FAIRBANK	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
K.	K.	kA
The	The	k1gMnSc1
Cambridge	Cambridge	k1gFnSc2
History	Histor	k1gInPc4
of	of	k?
China	China	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volume	volum	k1gInSc5
10	#num#	k4
<g/>
:	:	kIx,
Late	lat	k1gInSc5
Ch	Ch	kA
<g/>
'	'	kIx"
<g/>
ing	ing	kA
<g/>
,	,	kIx,
1800	#num#	k4
<g/>
-	-	kIx~
<g/>
1911	#num#	k4
<g/>
,	,	kIx,
Part	part	k1gInSc1
I.	I.	kA
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
UK	UK	kA
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k6eAd1
Fletcher	Fletchra	k1gFnPc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
21447	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
318	#num#	k4
<g/>
–	–	k?
<g/>
350	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
s.	s.	k?
335	#num#	k4
<g/>
–	–	k?
<g/>
336	#num#	k4
a	a	k8xC
341	#num#	k4
<g/>
–	–	k?
<g/>
348	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
А	А	k?
<g/>
,	,	kIx,
В	В	k?
А	А	k?
<g/>
.	.	kIx.
Р	Р	k?
н	н	k?
д	д	k?
р	р	k?
(	(	kIx(
<g/>
в	в	k?
п	п	k?
XVII	XVII	kA
в	в	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Н	Н	k?
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Б	Б	k?
<g/>
,	,	kIx,
Е	Е	k?
Л	Л	k?
П	П	k?
в	в	k?
с	с	k?
р	р	k?
о	о	k?
<g/>
.	.	kIx.
Х	Х	k?
<g/>
:	:	kIx,
К	К	k?
<g/>
.	.	kIx.
и	и	k?
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
M.	M.	kA
<g/>
:	:	kIx,
Н	Н	k?
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Я	Я	k?
<g/>
,	,	kIx,
П	П	k?
П	П	k?
р	р	k?
д	д	k?
1689	#num#	k4
г	г	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
M.	M.	kA
<g/>
:	:	kIx,
И	И	k?
А	А	k?
н	н	k?
С	С	k?
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
К	К	k?
<g/>
,	,	kIx,
Г	Г	k?
Е	Е	k?
П	П	k?
Х	Х	k?
:	:	kIx,
д	д	k?
<g/>
.	.	kIx.
п	п	k?
<g/>
.	.	kIx.
Х	Х	k?
<g/>
:	:	kIx,
Р	Р	k?
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
744	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
88570	#num#	k4
<g/>
-	-	kIx~
<g/>
107	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Л	Л	k?
<g/>
,	,	kIx,
Г	Г	k?
А	А	k?
З	З	k?
Е	Е	k?
П	П	k?
Х	Х	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
П	П	k?
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
144	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1904	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
М	М	k?
<g/>
,	,	kIx,
И	И	k?
В	В	k?
<g/>
;	;	kIx,
П	П	k?
<g/>
,	,	kIx,
А	А	k?
М	М	k?
О	О	k?
и	и	k?
Р	Р	k?
Д	Д	k?
В	В	k?
<g/>
.	.	kIx.
К	К	k?
п	п	k?
<g/>
:	:	kIx,
Р	Р	k?
к	к	k?
П	П	k?
<g/>
,	,	kIx,
С	С	k?
С	С	k?
и	и	k?
А	А	k?
XVI	XVI	kA
<g/>
–	–	k?
<g/>
XVIII	XVIII	kA
в	в	k?
<g/>
.	.	kIx.
Х	Х	k?
<g/>
:	:	kIx,
Д	Д	k?
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
383	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
М	М	k?
<g/>
,	,	kIx,
Г	Г	k?
В	В	k?
М	М	k?
н	н	k?
С	С	k?
(	(	kIx(
<g/>
XVII	XVII	kA
в	в	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
Н	Н	k?
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
М	М	k?
<g/>
,	,	kIx,
В	В	k?
С	С	k?
<g/>
.	.	kIx.
И	И	k?
Ц	Ц	k?
и	и	k?
Р	Р	k?
г	г	k?
в	в	k?
XVII	XVII	kA
в	в	k?
<g/>
.	.	kIx.
Х	Х	k?
<g/>
:	:	kIx,
Х	Х	k?
к	к	k?
и	и	k?
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
512	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
М	М	k?
<g/>
,	,	kIx,
В	В	k?
С	С	k?
<g/>
.	.	kIx.
Д	Д	k?
с	с	k?
у	у	k?
<g/>
:	:	kIx,
д	д	k?
и	и	k?
р	р	k?
г	г	k?
XVII-XX	XVII-XX	k1gFnSc2
в	в	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
М	М	k?
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
479	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
311	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Н	Н	k?
<g/>
,	,	kIx,
О	О	k?
Е	Е	k?
<g/>
.	.	kIx.
И	И	k?
К	К	k?
<g/>
.	.	kIx.
Э	Э	k?
Ц	Ц	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
XVII	XVII	kA
–	–	k?
н	н	k?
XX	XX	kA
в	в	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
В	В	k?
л	л	k?
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
712	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
18400	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
PERDUE	PERDUE	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
C.	C.	kA
China	China	k1gFnSc1
marches	marches	k1gMnSc1
west	west	k1gMnSc1
:	:	kIx,
the	the	k?
Qing	Qing	k1gMnSc1
conquest	conquest	k1gMnSc1
of	of	k?
Central	Central	k1gMnSc4
Eurasia	Eurasius	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
:	:	kIx,
Belknap	Belknap	k1gInSc1
Press	Press	k1gInSc1
of	of	k?
Harvard	Harvard	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780674057432	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
П	П	k?
<g/>
,	,	kIx,
И	И	k?
М	М	k?
Р	Р	k?
и	и	k?
К	К	k?
<g/>
:	:	kIx,
300	#num#	k4
л	л	k?
н	н	k?
г	г	k?
в	в	k?
<g/>
.	.	kIx.
М	М	k?
<g/>
:	:	kIx,
А	А	k?
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
511	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
19955	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
5	#num#	k4
<g/>
-	-	kIx~
<g/>
271	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7502	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
Г	Г	k?
3	#num#	k4
<g/>
.	.	kIx.
Г	Г	k?
о	о	k?
А	А	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Э	Э	k?
З	З	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
Н	Н	k?
ц	ц	k?
«	«	k?
<g/>
Э	Э	k?
З	З	k?
<g/>
»	»	k?
З	З	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Rusko-čchingská	Rusko-čchingský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ačanského	Ačanský	k2eAgInSc2d1
ostrohu	ostroh	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
na	na	k7c6
Sungari	Sungar	k1gFnSc6
•	•	k?
Obléhání	obléhání	k1gNnSc2
Kumarského	Kumarský	k2eAgInSc2d1
ostrohu	ostroh	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
v	v	k7c6
Korčejevské	Korčejevský	k2eAgFnSc6d1
zátoce	zátoka	k1gFnSc6
•	•	k?
První	první	k4xOgNnPc1
obléhání	obléhání	k1gNnSc1
Albazinu	Albazin	k1gInSc2
•	•	k?
Druhé	druhý	k4xOgNnSc1
obléhání	obléhání	k1gNnSc1
Albazinu	Albazin	k1gInSc2
•	•	k?
Obléhání	obléhání	k1gNnSc1
Selenginského	Selenginský	k2eAgInSc2d1
ostrohu	ostroh	k1gInSc2
•	•	k?
Něrčinská	Něrčinský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Válka	válka	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
|	|	kIx~
Čína	Čína	k1gFnSc1
</s>
