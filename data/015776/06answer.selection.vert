<s desamb="1">
Válku	válek	k1gInSc2
ukončila	ukončit	k5eAaPmAgFnS
Něrčinská	Něrčinský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
uzavřená	uzavřený	k2eAgFnSc1d1
roku	rok	k1gInSc2
1689	#num#	k4
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
sporné	sporný	k2eAgNnSc1d1
území	území	k1gNnSc1
přešlo	přejít	k5eAaPmAgNnS
k	k	k7c3
říši	říš	k1gFnSc3
Čching	Čching	k1gInSc4
<g/>
.	.	kIx.
</s>