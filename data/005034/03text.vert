<s>
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gFnSc5	Galile
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1564	[number]	k4	1564
<g/>
,	,	kIx,	,
Pisa	Pisa	k1gFnSc1	Pisa
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1642	[number]	k4	1642
<g/>
,	,	kIx,	,
Arcetri	Arcetre	k1gFnSc4	Arcetre
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
toskánský	toskánský	k2eAgMnSc1d1	toskánský
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
těsně	těsně	k6eAd1	těsně
spjatý	spjatý	k2eAgMnSc1d1	spjatý
s	s	k7c7	s
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
úspěchy	úspěch	k1gInPc7	úspěch
řadíme	řadit	k5eAaImIp1nP	řadit
vylepšení	vylepšení	k1gNnSc4	vylepšení
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
rozmanitá	rozmanitý	k2eAgNnPc4d1	rozmanité
astronomická	astronomický	k2eAgNnPc4d1	astronomické
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
z	z	k7c2	z
Newtonových	Newtonových	k2eAgInPc2d1	Newtonových
zákonů	zákon	k1gInPc2	zákon
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
účinnou	účinný	k2eAgFnSc4d1	účinná
podporu	podpora	k1gFnSc4	podpora
Koperníka	Koperník	k1gMnSc2	Koperník
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k9	jako
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
moderní	moderní	k2eAgFnSc2d1	moderní
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
moderní	moderní	k2eAgFnSc2d1	moderní
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
"	"	kIx"	"
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
vědy	věda	k1gFnSc2	věda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
experimentální	experimentální	k2eAgFnSc1d1	experimentální
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
důležitý	důležitý	k2eAgInSc4d1	důležitý
doplněk	doplněk	k1gInSc4	doplněk
spisů	spis	k1gInPc2	spis
Francise	Francise	k1gFnSc1	Francise
Bacona	Bacona	k1gFnSc1	Bacona
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
moderní	moderní	k2eAgFnSc1d1	moderní
vědecká	vědecký	k2eAgFnSc1d1	vědecká
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Galileova	Galileův	k2eAgFnSc1d1	Galileova
kariéra	kariéra	k1gFnSc1	kariéra
se	se	k3xPyFc4	se
kryla	krýt	k5eAaImAgFnS	krýt
s	s	k7c7	s
tvůrčím	tvůrčí	k2eAgNnSc7d1	tvůrčí
obdobím	období	k1gNnSc7	období
Johannese	Johannese	k1gFnSc2	Johannese
Keplera	Kepler	k1gMnSc2	Kepler
<g/>
.	.	kIx.	.
</s>
<s>
Galileovo	Galileův	k2eAgNnSc1d1	Galileovo
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
průlom	průlom	k1gInSc4	průlom
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Aristotelových	Aristotelův	k2eAgFnPc2d1	Aristotelova
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
jeho	jeho	k3xOp3gInSc4	jeho
konflikt	konflikt	k1gInSc4	konflikt
s	s	k7c7	s
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
je	být	k5eAaImIp3nS	být
brán	brát	k5eAaImNgInS	brát
jako	jako	k8xS	jako
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
příklad	příklad	k1gInSc1	příklad
počátečního	počáteční	k2eAgInSc2d1	počáteční
konfliktu	konflikt	k1gInSc2	konflikt
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
svobodné	svobodný	k2eAgFnSc2d1	svobodná
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
vědou	věda	k1gFnSc7	věda
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
nuceně	nuceně	k6eAd1	nuceně
"	"	kIx"	"
<g/>
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1589	[number]	k4	1589
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
nabídnuta	nabídnut	k2eAgFnSc1d1	nabídnuta
pozice	pozice	k1gFnSc1	pozice
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
matematiku	matematika	k1gFnSc4	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
na	na	k7c4	na
Univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
fakultě	fakulta	k1gFnSc6	fakulta
sloužil	sloužit	k5eAaImAgMnS	sloužit
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
jako	jako	k8xS	jako
učitel	učitel	k1gMnSc1	učitel
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
,	,	kIx,	,
mechaniky	mechanika	k1gFnSc2	mechanika
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1610	[number]	k4	1610
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
učinil	učinit	k5eAaImAgMnS	učinit
mnoho	mnoho	k4c4	mnoho
významných	významný	k2eAgInPc2d1	významný
objevů	objev	k1gInPc2	objev
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
postavami	postava	k1gFnPc7	postava
vědecké	vědecký	k2eAgFnSc2d1	vědecká
revoluce	revoluce	k1gFnSc2	revoluce
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
Galileo	Galilea	k1gFnSc5	Galilea
vysoké	vysoký	k2eAgNnSc4d1	vysoké
postavení	postavení	k1gNnPc4	postavení
především	především	k6eAd1	především
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
průkopnické	průkopnický	k2eAgNnSc4d1	průkopnické
užívání	užívání	k1gNnSc4	užívání
kvantitativních	kvantitativní	k2eAgInPc2d1	kvantitativní
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
výsledky	výsledek	k1gInPc4	výsledek
matematicky	matematicky	k6eAd1	matematicky
analyzoval	analyzovat	k5eAaImAgInS	analyzovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
neměly	mít	k5eNaImAgFnP	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
evropském	evropský	k2eAgNnSc6d1	Evropské
myšlení	myšlení	k1gNnSc6	myšlení
velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
<g/>
;	;	kIx,	;
asi	asi	k9	asi
největší	veliký	k2eAgMnSc1d3	veliký
experimentátor	experimentátor	k1gMnSc1	experimentátor
předcházející	předcházející	k2eAgFnSc2d1	předcházející
Galileovi	Galileus	k1gMnSc3	Galileus
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Gilbert	gilbert	k1gInSc1	gilbert
<g/>
,	,	kIx,	,
kvantitativní	kvantitativní	k2eAgInPc1d1	kvantitativní
postupy	postup	k1gInPc1	postup
nepoužíval	používat	k5eNaImAgMnS	používat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Galileův	Galileův	k2eAgMnSc1d1	Galileův
otec	otec	k1gMnSc1	otec
Vincenzo	Vincenza	k1gFnSc5	Vincenza
Galilei	Galilei	k1gNnSc4	Galilei
však	však	k9	však
prováděl	provádět	k5eAaImAgMnS	provádět
experimenty	experiment	k1gInPc4	experiment
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc2	který
objevil	objevit	k5eAaPmAgMnS	objevit
asi	asi	k9	asi
nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
nelineární	lineární	k2eNgInSc1d1	nelineární
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
vztah	vztah	k1gInSc1	vztah
–	–	k?	–
mezi	mezi	k7c7	mezi
napětím	napětí	k1gNnSc7	napětí
<g/>
,	,	kIx,	,
frekvencí	frekvence	k1gFnSc7	frekvence
a	a	k8xC	a
délkou	délka	k1gFnSc7	délka
natažené	natažený	k2eAgFnSc2d1	natažená
struny	struna	k1gFnSc2	struna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
také	také	k9	také
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
odmítnutí	odmítnutí	k1gNnSc3	odmítnutí
slepé	slepý	k2eAgFnSc2d1	slepá
důvěry	důvěra	k1gFnSc2	důvěra
k	k	k7c3	k
autoritám	autorita	k1gFnPc3	autorita
(	(	kIx(	(
<g/>
např.	např.	kA	např.
církvi	církev	k1gFnSc6	církev
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgMnPc3d1	jiný
myslitelům	myslitel	k1gMnPc3	myslitel
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Aristotelovi	Aristotelův	k2eAgMnPc1d1	Aristotelův
<g/>
)	)	kIx)	)
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
vědy	věda	k1gFnSc2	věda
od	od	k7c2	od
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgInPc1d1	hlavní
důvody	důvod	k1gInPc1	důvod
jeho	on	k3xPp3gNnSc2	on
označování	označování	k1gNnSc2	označování
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
otce	otec	k1gMnSc2	otec
vědy	věda	k1gFnSc2	věda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
kriticky	kriticky	k6eAd1	kriticky
rozebrali	rozebrat	k5eAaPmAgMnP	rozebrat
opravdovost	opravdovost	k1gFnSc4	opravdovost
Galileiho	Galilei	k1gMnSc4	Galilei
experimentů	experiment	k1gInPc2	experiment
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
významný	významný	k2eAgMnSc1d1	významný
francouzský	francouzský	k2eAgMnSc1d1	francouzský
historik	historik	k1gMnSc1	historik
vědy	věda	k1gFnSc2	věda
Alexandre	Alexandr	k1gInSc5	Alexandr
Koyré	Koyrý	k2eAgFnPc5d1	Koyrý
<g/>
.	.	kIx.	.
</s>
<s>
Experimenty	experiment	k1gInPc1	experiment
popsané	popsaný	k2eAgInPc1d1	popsaný
v	v	k7c6	v
Matematických	matematický	k2eAgFnPc6d1	matematická
rozpravách	rozprava	k1gFnPc6	rozprava
a	a	k8xC	a
pokusech	pokus	k1gInPc6	pokus
(	(	kIx(	(
<g/>
Discorsi	Discorse	k1gFnSc3	Discorse
e	e	k0	e
dimostrazioni	dimostrazion	k1gMnPc1	dimostrazion
matematiche	matematiche	k1gNnSc1	matematiche
<g/>
,	,	kIx,	,
intorno	intorno	k1gNnSc1	intorno
à	à	k?	à
due	due	k?	due
nuove	nuovat	k5eAaPmIp3nS	nuovat
scienze	scienze	k1gFnSc1	scienze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
zákona	zákon	k1gInSc2	zákon
pro	pro	k7c4	pro
zrychlení	zrychlení	k1gNnPc4	zrychlení
padajících	padající	k2eAgNnPc2d1	padající
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
přesné	přesný	k2eAgNnSc4d1	přesné
měření	měření	k1gNnSc4	měření
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
být	být	k5eAaImF	být
nemožným	nemožná	k1gFnPc3	nemožná
technologií	technologie	k1gFnPc2	technologie
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Koyrého	Koyrý	k2eAgNnSc2d1	Koyré
byl	být	k5eAaImAgInS	být
zákon	zákon	k1gInSc1	zákon
určen	určit	k5eAaPmNgInS	určit
deduktivně	deduktivně	k6eAd1	deduktivně
a	a	k8xC	a
experimenty	experiment	k1gInPc1	experiment
byly	být	k5eAaImAgInP	být
prováděny	provádět	k5eAaImNgInP	provádět
jen	jen	k9	jen
pro	pro	k7c4	pro
experimenty	experiment	k1gInPc4	experiment
a	a	k8xC	a
aby	aby	kYmCp3nP	aby
zákon	zákon	k1gInSc1	zákon
přibližně	přibližně	k6eAd1	přibližně
ilustrovaly	ilustrovat	k5eAaBmAgInP	ilustrovat
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInSc1d2	pozdější
výzkum	výzkum	k1gInSc1	výzkum
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
platnost	platnost	k1gFnSc4	platnost
těchto	tento	k3xDgInPc2	tento
experimentů	experiment	k1gInPc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Experimenty	experiment	k1gInPc1	experiment
s	s	k7c7	s
padajícími	padající	k2eAgNnPc7d1	padající
tělesy	těleso	k1gNnPc7	těleso
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
s	s	k7c7	s
valícími	valící	k2eAgFnPc7d1	valící
se	se	k3xPyFc4	se
koulemi	koule	k1gFnPc7	koule
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
zopakovány	zopakovat	k5eAaPmNgInP	zopakovat
metodami	metoda	k1gFnPc7	metoda
popsanými	popsaný	k2eAgFnPc7d1	popsaná
Galileem	Galileus	k1gMnSc7	Galileus
(	(	kIx(	(
<g/>
Settle	Settle	k1gFnSc1	Settle
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
a	a	k8xC	a
přesnost	přesnost	k1gFnSc1	přesnost
výsledků	výsledek	k1gInPc2	výsledek
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
Galileovými	Galileův	k2eAgFnPc7d1	Galileova
zprávami	zpráva	k1gFnPc7	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
výzkumy	výzkum	k1gInPc1	výzkum
Galileových	Galileův	k2eAgInPc2d1	Galileův
nepublikovaných	publikovaný	k2eNgInPc2d1	nepublikovaný
pracovních	pracovní	k2eAgInPc2d1	pracovní
zápisků	zápisek	k1gInPc2	zápisek
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1604	[number]	k4	1604
jasněji	jasně	k6eAd2	jasně
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
reálnost	reálnost	k1gFnSc4	reálnost
experimentů	experiment	k1gInPc2	experiment
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
naznačily	naznačit	k5eAaPmAgInP	naznačit
mezivýsledky	mezivýsledek	k1gInPc1	mezivýsledek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
odvození	odvození	k1gNnSc3	odvození
zákona	zákon	k1gInSc2	zákon
obsahujícího	obsahující	k2eAgInSc2d1	obsahující
kvadrát	kvadrát	k1gInSc1	kvadrát
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
Drake	Drake	k1gInSc1	Drake
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
byli	být	k5eAaImAgMnP	být
astronomové	astronom	k1gMnPc1	astronom
zaměstnáni	zaměstnat	k5eAaPmNgMnP	zaměstnat
velkým	velký	k2eAgInSc7d1	velký
sporem	spor	k1gInSc7	spor
mezi	mezi	k7c7	mezi
Koperníkovým	Koperníkův	k2eAgInSc7d1	Koperníkův
systémem	systém	k1gInSc7	systém
(	(	kIx(	(
<g/>
planety	planeta	k1gFnSc2	planeta
kroužící	kroužící	k2eAgMnSc1d1	kroužící
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
a	a	k8xC	a
geocentrickým	geocentrický	k2eAgInSc7d1	geocentrický
systémem	systém	k1gInSc7	systém
(	(	kIx(	(
<g/>
planety	planeta	k1gFnPc1	planeta
a	a	k8xC	a
Slunce	slunce	k1gNnSc1	slunce
kroužící	kroužící	k2eAgNnSc1d1	kroužící
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1604	[number]	k4	1604
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Galileo	Galilea	k1gFnSc5	Galilea
svou	svůj	k3xOyFgFnSc7	svůj
podporu	podpora	k1gFnSc4	podpora
Koperníkově	Koperníkův	k2eAgFnSc6d1	Koperníkova
myšlenkové	myšlenkový	k2eAgFnSc6d1	myšlenková
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
scházely	scházet	k5eAaImAgFnP	scházet
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
prostředky	prostředek	k1gInPc4	prostředek
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
svého	svůj	k3xOyFgInSc2	svůj
úsudku	úsudek	k1gInSc2	úsudek
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
lidová	lidový	k2eAgFnSc1d1	lidová
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
Galileovi	Galileus	k1gMnSc6	Galileus
vynalézajícím	vynalézající	k2eAgMnSc6d1	vynalézající
dalekohled	dalekohled	k1gInSc4	dalekohled
není	být	k5eNaImIp3nS	být
přesná	přesný	k2eAgFnSc1d1	přesná
<g/>
,	,	kIx,	,
rozhodně	rozhodně	k6eAd1	rozhodně
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
dalekohled	dalekohled	k1gInSc4	dalekohled
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Inspirován	inspirován	k2eAgInSc1d1	inspirován
náčrtkovými	náčrtkový	k2eAgInPc7d1	náčrtkový
popisy	popis	k1gInPc7	popis
dalekohledů	dalekohled	k1gInPc2	dalekohled
vynalezených	vynalezený	k2eAgInPc2d1	vynalezený
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1608	[number]	k4	1608
<g/>
,	,	kIx,	,
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
Galileo	Galilea	k1gFnSc5	Galilea
jeden	jeden	k4xCgMnSc1	jeden
přibližující	přibližující	k2eAgNnSc4d1	přibližující
8	[number]	k4	8
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
vylepšené	vylepšený	k2eAgInPc4d1	vylepšený
modely	model	k1gInPc4	model
přibližující	přibližující	k2eAgInPc4d1	přibližující
až	až	k6eAd1	až
dvacetinásobně	dvacetinásobně	k6eAd1	dvacetinásobně
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
dalekohled	dalekohled	k1gInSc4	dalekohled
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1609	[number]	k4	1609
před	před	k7c7	před
benátskými	benátský	k2eAgMnPc7d1	benátský
zákonodárci	zákonodárce	k1gMnPc7	zákonodárce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
zařízení	zařízení	k1gNnSc6	zařízení
mu	on	k3xPp3gMnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
výnosný	výnosný	k2eAgInSc4d1	výnosný
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
kupci	kupec	k1gMnPc7	kupec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
shledali	shledat	k5eAaPmAgMnP	shledat
užitečným	užitečný	k2eAgMnSc7d1	užitečný
při	při	k7c6	při
námořním	námořní	k2eAgInSc6d1	námořní
obchodu	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
první	první	k4xOgNnPc4	první
teleskopická	teleskopický	k2eAgNnPc4d1	teleskopické
astronomická	astronomický	k2eAgNnPc4d1	astronomické
pozorování	pozorování	k1gNnPc4	pozorování
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
objevu	objev	k1gInSc2	objev
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
publikoval	publikovat	k5eAaBmAgInS	publikovat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
v	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
pojednání	pojednání	k1gNnSc6	pojednání
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Sidereus	Sidereus	k1gInSc1	Sidereus
Nuncius	nuncius	k1gMnSc1	nuncius
(	(	kIx(	(
<g/>
Hvězdný	hvězdný	k2eAgMnSc1d1	hvězdný
posel	posel	k1gMnSc1	posel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
vysokém	vysoký	k2eAgInSc6d1	vysoký
nákladu	náklad	k1gInSc6	náklad
550	[number]	k4	550
výtisků	výtisk	k1gInPc2	výtisk
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
rozebráno	rozebrat	k5eAaPmNgNnS	rozebrat
za	za	k7c4	za
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g />
.	.	kIx.	.
</s>
<s>
1610	[number]	k4	1610
objevil	objevit	k5eAaPmAgMnS	objevit
Galileo	Galilea	k1gFnSc5	Galilea
Jupiterovy	Jupiterův	k2eAgInPc4d1	Jupiterův
čtyři	čtyři	k4xCgInPc4	čtyři
největší	veliký	k2eAgInPc4d3	veliký
satelity	satelit	k1gInPc4	satelit
(	(	kIx(	(
<g/>
měsíce	měsíc	k1gInPc4	měsíc
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Io	Io	k1gFnSc1	Io
<g/>
,	,	kIx,	,
Europa	Europa	k1gFnSc1	Europa
<g/>
,	,	kIx,	,
Ganymed	Ganymed	k1gMnSc1	Ganymed
a	a	k8xC	a
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tato	tento	k3xDgFnSc1	tento
jména	jméno	k1gNnPc4	jméno
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
nedal	dát	k5eNaPmAgMnS	dát
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
německý	německý	k2eAgMnSc1d1	německý
astronom	astronom	k1gMnSc1	astronom
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
publikoval	publikovat	k5eAaBmAgMnS	publikovat
až	až	k9	až
o	o	k7c4	o
4	[number]	k4	4
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dožadoval	dožadovat	k5eAaImAgInS	dožadovat
se	se	k3xPyFc4	se
prvenství	prvenství	k1gNnSc1	prvenství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Určil	určit	k5eAaPmAgMnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
měsíce	měsíc	k1gInPc1	měsíc
obíhají	obíhat	k5eAaImIp3nP	obíhat
planetu	planeta	k1gFnSc4	planeta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
občas	občas	k6eAd1	občas
mizely	mizet	k5eAaImAgFnP	mizet
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc4	ten
připisoval	připisovat	k5eAaImAgMnS	připisovat
jejich	jejich	k3xOp3gInSc2	jejich
pohybu	pohyb	k1gInSc2	pohyb
za	za	k7c7	za
Jupiterem	Jupiter	k1gMnSc7	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
přinesl	přinést	k5eAaPmAgInS	přinést
Galileimu	Galileima	k1gFnSc4	Galileima
jmenování	jmenování	k1gNnPc2	jmenování
čestným	čestný	k2eAgMnSc7d1	čestný
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
a	a	k8xC	a
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
zvýšen	zvýšen	k2eAgInSc4d1	zvýšen
plat	plat	k1gInSc4	plat
s	s	k7c7	s
doživotní	doživotní	k2eAgFnSc7d1	doživotní
platností	platnost	k1gFnSc7	platnost
na	na	k7c4	na
1	[number]	k4	1
000	[number]	k4	000
florinů	florin	k1gInPc2	florin
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
pozorování	pozorování	k1gNnSc1	pozorování
měsíců	měsíc	k1gInPc2	měsíc
vykonal	vykonat	k5eAaPmAgMnS	vykonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Astronomové	astronom	k1gMnPc1	astronom
později	pozdě	k6eAd2	pozdě
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
Galileovo	Galileův	k2eAgNnSc4d1	Galileovo
pojmenování	pojmenování	k1gNnSc4	pojmenování
těchto	tento	k3xDgNnPc2	tento
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
změnili	změnit	k5eAaPmAgMnP	změnit
jeho	jeho	k3xOp3gFnPc4	jeho
medičejské	medičejský	k2eAgFnPc4d1	medičejský
hvězdy	hvězda	k1gFnPc4	hvězda
na	na	k7c4	na
galileovské	galileovský	k2eAgInPc4d1	galileovský
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Ukázka	ukázka	k1gFnSc1	ukázka
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgFnPc4d2	menší
planety	planeta	k1gFnPc4	planeta
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
obíhající	obíhající	k2eAgFnSc2d1	obíhající
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
velmi	velmi	k6eAd1	velmi
problematická	problematický	k2eAgNnPc1d1	problematické
pro	pro	k7c4	pro
spořádaný	spořádaný	k2eAgInSc4d1	spořádaný
a	a	k8xC	a
úplný	úplný	k2eAgInSc4d1	úplný
obraz	obraz	k1gInSc4	obraz
geocentrického	geocentrický	k2eAgInSc2d1	geocentrický
modelu	model	k1gInSc2	model
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vše	všechen	k3xTgNnSc1	všechen
obíhá	obíhat	k5eAaImIp3nS	obíhat
okolo	okolo	k7c2	okolo
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
stejnou	stejný	k2eAgFnSc4d1	stejná
sadu	sada	k1gFnSc4	sada
fází	fáze	k1gFnPc2	fáze
jako	jako	k8xS	jako
Měsíc	měsíc	k1gInSc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
jasnost	jasnost	k1gFnSc1	jasnost
Venuše	Venuše	k1gFnSc2	Venuše
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
téměř	téměř	k6eAd1	téměř
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
,	,	kIx,	,
Galileo	Galilea	k1gFnSc5	Galilea
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venuše	Venuše	k1gFnSc1	Venuše
nemůže	moct	k5eNaImIp3nS	moct
kroužit	kroužit	k5eAaImF	kroužit
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
konstantní	konstantní	k2eAgFnSc6d1	konstantní
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
heliocentrickém	heliocentrický	k2eAgInSc6d1	heliocentrický
modelu	model	k1gInSc6	model
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
vytvořeném	vytvořený	k2eAgInSc6d1	vytvořený
Koperníkem	Koperník	k1gInSc7	Koperník
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
stálá	stálý	k2eAgFnSc1d1	stálá
jasnost	jasnost	k1gFnSc1	jasnost
elegantně	elegantně	k6eAd1	elegantně
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
"	"	kIx"	"
<g/>
plné	plný	k2eAgFnSc2d1	plná
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
planety	planeta	k1gFnPc1	planeta
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
mnohem	mnohem	k6eAd1	mnohem
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
na	na	k7c6	na
protilehlých	protilehlý	k2eAgFnPc6d1	protilehlá
stranách	strana	k1gFnPc6	strana
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Venušina	Venušin	k2eAgFnSc1d1	Venušina
osvětlená	osvětlený	k2eAgFnSc1d1	osvětlená
polokoule	polokoule	k1gFnSc1	polokoule
je	být	k5eAaImIp3nS	být
přivrácena	přivrátit	k5eAaPmNgFnS	přivrátit
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
sluneční	sluneční	k2eAgFnPc4d1	sluneční
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
existují	existovat	k5eAaImIp3nP	existovat
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
čínští	čínský	k2eAgMnPc1d1	čínský
astronomové	astronom	k1gMnPc1	astronom
tak	tak	k6eAd1	tak
činili	činit	k5eAaImAgMnP	činit
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
existence	existence	k1gFnSc1	existence
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
ukázala	ukázat	k5eAaPmAgFnS	ukázat
další	další	k2eAgNnSc4d1	další
úskalí	úskalí	k1gNnSc4	úskalí
předpokladu	předpoklad	k1gInSc2	předpoklad
dokonalosti	dokonalost	k1gFnSc2	dokonalost
nebes	nebesa	k1gNnPc2	nebesa
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
každoroční	každoroční	k2eAgFnPc1d1	každoroční
odchylky	odchylka	k1gFnPc1	odchylka
v	v	k7c6	v
jejich	jejich	k3xOp3gInPc6	jejich
pohybech	pohyb	k1gInPc6	pohyb
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc6	který
si	se	k3xPyFc3	se
poprvé	poprvé	k6eAd1	poprvé
všiml	všimnout	k5eAaPmAgMnS	všimnout
Francesco	Francesco	k6eAd1	Francesco
Sizzi	Sizze	k1gFnSc4	Sizze
<g/>
,	,	kIx,	,
představovaly	představovat	k5eAaImAgFnP	představovat
velké	velký	k2eAgFnPc4d1	velká
potíže	potíž	k1gFnPc4	potíž
ať	ať	k8xS	ať
už	už	k6eAd1	už
pro	pro	k7c4	pro
geocentrický	geocentrický	k2eAgInSc4d1	geocentrický
systém	systém	k1gInSc4	systém
tak	tak	k6eAd1	tak
i	i	k9	i
pro	pro	k7c4	pro
systém	systém	k1gInSc4	systém
Tychona	Tychon	k1gMnSc2	Tychon
Brahe	Brah	k1gMnSc2	Brah
<g/>
.	.	kIx.	.
</s>
<s>
Diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c4	o
prvenství	prvenství	k1gNnSc4	prvenství
objevu	objev	k1gInSc2	objev
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
dlouhému	dlouhý	k2eAgMnSc3d1	dlouhý
a	a	k8xC	a
trpkému	trpký	k2eAgInSc3d1	trpký
sporu	spor	k1gInSc3	spor
s	s	k7c7	s
Christophem	Christoph	k1gInSc7	Christoph
Scheinerem	scheiner	k1gInSc7	scheiner
<g/>
;	;	kIx,	;
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
oba	dva	k4xCgMnPc1	dva
předběhl	předběhnout	k5eAaPmAgMnS	předběhnout
David	David	k1gMnSc1	David
Fabricius	Fabricius	k1gMnSc1	Fabricius
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Johannes	Johannes	k1gMnSc1	Johannes
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
podal	podat	k5eAaPmAgMnS	podat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
měsíčních	měsíční	k2eAgNnPc6d1	měsíční
pohořích	pohoří	k1gNnPc6	pohoří
a	a	k8xC	a
kráterech	kráter	k1gInPc6	kráter
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
existenci	existence	k1gFnSc4	existence
vyvodil	vyvodit	k5eAaPmAgMnS	vyvodit
ze	z	k7c2	z
vzorů	vzor	k1gInPc2	vzor
vymodelovaných	vymodelovaný	k2eAgInPc2d1	vymodelovaný
světlem	světlo	k1gNnSc7	světlo
a	a	k8xC	a
stínem	stín	k1gInSc7	stín
na	na	k7c6	na
měsíčním	měsíční	k2eAgInSc6d1	měsíční
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
výšku	výška	k1gFnSc4	výška
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ho	on	k3xPp3gMnSc4	on
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
Měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
hrbolatý	hrbolatý	k2eAgMnSc1d1	hrbolatý
a	a	k8xC	a
nerovný	rovný	k2eNgMnSc1d1	nerovný
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
samotný	samotný	k2eAgInSc1d1	samotný
povrch	povrch	k1gInSc1	povrch
Země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
přesnou	přesný	k2eAgFnSc7d1	přesná
koulí	koule	k1gFnSc7	koule
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
Aristotelés	Aristotelés	k1gInSc4	Aristotelés
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
Mléčnou	mléčný	k2eAgFnSc4d1	mléčná
dráhu	dráha	k1gFnSc4	dráha
považovanou	považovaný	k2eAgFnSc4d1	považovaná
dosud	dosud	k6eAd1	dosud
za	za	k7c4	za
mrak	mrak	k1gInSc4	mrak
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
spousty	spousta	k1gFnSc2	spousta
hvězd	hvězda	k1gFnPc2	hvězda
namačkaných	namačkaný	k2eAgFnPc2d1	namačkaná
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
tak	tak	k6eAd1	tak
těsně	těsně	k6eAd1	těsně
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
mrak	mrak	k1gInSc1	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Lokalizoval	lokalizovat	k5eAaBmAgMnS	lokalizovat
také	také	k9	také
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
hvězd	hvězda	k1gFnPc2	hvězda
příliš	příliš	k6eAd1	příliš
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
viditelné	viditelný	k2eAgFnSc2d1	viditelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1611	[number]	k4	1611
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
také	také	k9	také
planetu	planeta	k1gFnSc4	planeta
Neptun	Neptun	k1gInSc4	Neptun
<g/>
,	,	kIx,	,
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
jí	jíst	k5eAaImIp3nS	jíst
však	však	k9	však
žádnou	žádný	k3yNgFnSc4	žádný
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
;	;	kIx,	;
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
poznámkách	poznámka	k1gFnPc6	poznámka
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
jen	jen	k9	jen
jako	jako	k9	jako
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
nezajímavých	zajímavý	k2eNgFnPc2d1	nezajímavá
nejasných	jasný	k2eNgFnPc2d1	nejasná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Galileova	Galileův	k2eAgFnSc1d1	Galileova
teoretická	teoretický	k2eAgFnSc1d1	teoretická
a	a	k8xC	a
experimentální	experimentální	k2eAgFnSc1d1	experimentální
práce	práce	k1gFnSc1	práce
o	o	k7c6	o
pohybech	pohyb	k1gInPc6	pohyb
těles	těleso	k1gNnPc2	těleso
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
prakticky	prakticky	k6eAd1	prakticky
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
prací	práce	k1gFnSc7	práce
Keplera	Kepler	k1gMnSc2	Kepler
a	a	k8xC	a
Reného	René	k1gMnSc2	René
Descarta	Descart	k1gMnSc2	Descart
byla	být	k5eAaImAgFnS	být
předchůdcem	předchůdce	k1gMnSc7	předchůdce
klasické	klasický	k2eAgFnSc2d1	klasická
mechaniky	mechanika	k1gFnSc2	mechanika
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
Isaacem	Isaace	k1gMnSc7	Isaace
Newtonem	Newton	k1gMnSc7	Newton
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
<g/>
,	,	kIx,	,
přinejmenším	přinejmenším	k6eAd1	přinejmenším
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
tradici	tradice	k1gFnSc6	tradice
<g/>
,	,	kIx,	,
v	v	k7c6	v
provádění	provádění	k1gNnSc6	provádění
pečlivých	pečlivý	k2eAgInPc2d1	pečlivý
experimentů	experiment	k1gInPc2	experiment
a	a	k8xC	a
v	v	k7c6	v
trvání	trvání	k1gNnSc6	trvání
na	na	k7c6	na
matematickém	matematický	k2eAgInSc6d1	matematický
popisu	popis	k1gInSc6	popis
přírodních	přírodní	k2eAgInPc2d1	přírodní
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
historek	historka	k1gFnPc2	historka
o	o	k7c6	o
Galileovi	Galileův	k2eAgMnPc1d1	Galileův
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouštěl	pouštět	k5eAaImAgMnS	pouštět
koule	koule	k1gFnPc4	koule
o	o	k7c6	o
rozdílných	rozdílný	k2eAgFnPc6d1	rozdílná
hmotnostech	hmotnost	k1gFnPc6	hmotnost
z	z	k7c2	z
nakloněné	nakloněný	k2eAgFnSc2d1	nakloněná
věže	věž	k1gFnSc2	věž
v	v	k7c6	v
Pise	Pisa	k1gFnSc6	Pisa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlost	rychlost	k1gFnSc1	rychlost
jejich	jejich	k3xOp3gInSc2	jejich
pádu	pád	k1gInSc2	pád
je	být	k5eAaImIp3nS	být
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
hmotnosti	hmotnost	k1gFnSc6	hmotnost
(	(	kIx(	(
<g/>
neuvažujeme	uvažovat	k5eNaImIp1nP	uvažovat
<g/>
-li	i	k?	-li
omezený	omezený	k2eAgInSc1d1	omezený
vliv	vliv	k1gInSc1	vliv
odporu	odpor	k1gInSc2	odpor
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výsledek	výsledek	k1gInSc1	výsledek
odporoval	odporovat	k5eAaImAgInS	odporovat
Aristotelovu	Aristotelův	k2eAgFnSc4d1	Aristotelova
tvrzení	tvrzení	k1gNnSc3	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
těžší	těžký	k2eAgInPc1d2	těžší
objekty	objekt	k1gInPc1	objekt
padají	padat	k5eAaImIp3nP	padat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
lehčí	lehčit	k5eAaImIp3nP	lehčit
<g/>
,	,	kIx,	,
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
úměře	úměra	k1gFnSc6	úměra
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
hmotnosti	hmotnost	k1gFnSc3	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
příběh	příběh	k1gInSc1	příběh
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životopise	životopis	k1gInSc6	životopis
sestaveném	sestavený	k2eAgInSc6d1	sestavený
Galileovým	Galileův	k2eAgMnSc7d1	Galileův
žákem	žák	k1gMnSc7	žák
Vincenzem	Vincenz	k1gMnSc7	Vincenz
Vivianim	Vivianim	k1gInSc4	Vivianim
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Galileo	Galilea	k1gFnSc5	Galilea
skutečně	skutečně	k6eAd1	skutečně
prováděl	provádět	k5eAaImAgMnS	provádět
experimenty	experiment	k1gInPc4	experiment
včetně	včetně	k7c2	včetně
valení	valení	k1gNnSc2	valení
koulí	koulet	k5eAaImIp3nS	koulet
dolů	dolů	k6eAd1	dolů
po	po	k7c6	po
nakloněné	nakloněný	k2eAgFnSc6d1	nakloněná
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dokazoval	dokazovat	k5eAaImAgInS	dokazovat
tutéž	týž	k3xTgFnSc4	týž
věc	věc	k1gFnSc4	věc
<g/>
:	:	kIx,	:
padající	padající	k2eAgInPc1d1	padající
nebo	nebo	k8xC	nebo
koulející	koulející	k2eAgInPc1d1	koulející
se	se	k3xPyFc4	se
objekty	objekt	k1gInPc7	objekt
(	(	kIx(	(
<g/>
koulení	koulení	k1gNnPc2	koulení
je	být	k5eAaImIp3nS	být
pomalejší	pomalý	k2eAgFnSc7d2	pomalejší
verzí	verze	k1gFnSc7	verze
padání	padání	k1gNnSc2	padání
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zrychlují	zrychlovat	k5eAaImIp3nP	zrychlovat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Určil	určit	k5eAaPmAgMnS	určit
správné	správný	k2eAgNnSc4d1	správné
matematické	matematický	k2eAgNnSc4d1	matematické
vyjádření	vyjádření	k1gNnSc4	vyjádření
zákona	zákon	k1gInSc2	zákon
pro	pro	k7c4	pro
zrychlení	zrychlení	k1gNnSc4	zrychlení
<g/>
:	:	kIx,	:
počínal	počínat	k5eAaImAgInS	počínat
<g/>
-li	i	k?	-li
pohyb	pohyb	k1gInSc1	pohyb
z	z	k7c2	z
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
uražená	uražený	k2eAgFnSc1d1	uražená
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
přímo	přímo	k6eAd1	přímo
úměrná	úměrný	k2eAgFnSc1d1	úměrná
čtverci	čtverec	k1gInSc3	čtverec
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
předchůdce	předchůdce	k1gMnSc2	předchůdce
mnoha	mnoho	k4c2	mnoho
pozdějších	pozdní	k2eAgInPc2d2	pozdější
vědeckých	vědecký	k2eAgInPc2d1	vědecký
zákonů	zákon	k1gInPc2	zákon
vyjádřených	vyjádřený	k2eAgInPc2d1	vyjádřený
matematickou	matematický	k2eAgFnSc7d1	matematická
formou	forma	k1gFnSc7	forma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Usuzoval	usuzovat	k5eAaImAgMnS	usuzovat
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
objekty	objekt	k1gInPc4	objekt
si	se	k3xPyFc3	se
udržují	udržovat	k5eAaImIp3nP	udržovat
svou	svůj	k3xOyFgFnSc4	svůj
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
nepůsobí	působit	k5eNaImIp3nS	působit
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
tření	tření	k1gNnSc1	tření
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
popřel	popřít	k5eAaPmAgMnS	popřít
uznávanou	uznávaný	k2eAgFnSc4d1	uznávaná
Aristotelovu	Aristotelův	k2eAgFnSc4d1	Aristotelova
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
objekty	objekt	k1gInPc1	objekt
přirozeně	přirozeně	k6eAd1	přirozeně
zpomalují	zpomalovat	k5eAaImIp3nP	zpomalovat
a	a	k8xC	a
zastaví	zastavit	k5eAaPmIp3nP	zastavit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
na	na	k7c4	na
ně	on	k3xPp3gInPc4	on
nepůsobí	působit	k5eNaImIp3nS	působit
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
Newton	newton	k1gInSc4	newton
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
zákonů	zákon	k1gInPc2	zákon
pohybu	pohyb	k1gInSc2	pohyb
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
také	také	k9	také
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kmity	kmit	k1gInPc4	kmit
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
vždy	vždy	k6eAd1	vždy
trvají	trvat	k5eAaImIp3nP	trvat
stejný	stejný	k2eAgInSc4d1	stejný
časový	časový	k2eAgInSc4d1	časový
úsek	úsek	k1gInSc4	úsek
<g/>
,	,	kIx,	,
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
amplitudě	amplituda	k1gFnSc6	amplituda
výchylky	výchylka	k1gFnSc2	výchylka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Galileo	Galilea	k1gFnSc5	Galilea
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rovnost	rovnost	k1gFnSc1	rovnost
periody	perioda	k1gFnSc2	perioda
je	být	k5eAaImIp3nS	být
přesná	přesný	k2eAgFnSc1d1	přesná
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
vztah	vztah	k1gInSc4	vztah
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
výchylky	výchylka	k1gFnPc4	výchylka
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
to	ten	k3xDgNnSc1	ten
však	však	k9	však
dobře	dobře	k6eAd1	dobře
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
usměrňování	usměrňování	k1gNnSc3	usměrňování
hodinových	hodinový	k2eAgInPc2d1	hodinový
impulsů	impuls	k1gInPc2	impuls
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
si	se	k3xPyFc3	se
Galileo	Galilea	k1gFnSc5	Galilea
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
Technologie	technologie	k1gFnSc2	technologie
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
prvního	první	k4xOgNnSc2	první
desetiletí	desetiletí	k1gNnSc2	desetiletí
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zkusil	zkusit	k5eAaPmAgMnS	zkusit
Galileo	Galilea	k1gFnSc5	Galilea
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
asistentem	asistent	k1gMnSc7	asistent
změřit	změřit	k5eAaPmF	změřit
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Stáli	stát	k5eAaImAgMnP	stát
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
různých	různý	k2eAgInPc6d1	různý
kopcích	kopec	k1gInPc6	kopec
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
držel	držet	k5eAaImAgMnS	držet
lucernu	lucerna	k1gFnSc4	lucerna
s	s	k7c7	s
okenicemi	okenice	k1gFnPc7	okenice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Galileo	Galilea	k1gFnSc5	Galilea
otevřel	otevřít	k5eAaPmAgInS	otevřít
svou	svůj	k3xOyFgFnSc4	svůj
okenici	okenice	k1gFnSc4	okenice
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
jeho	jeho	k3xOp3gMnSc1	jeho
asistent	asistent	k1gMnSc1	asistent
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
uvidí	uvidět	k5eAaPmIp3nS	uvidět
záblesk	záblesk	k1gInSc4	záblesk
<g/>
,	,	kIx,	,
otevřít	otevřít	k5eAaPmF	otevřít
svou	svůj	k3xOyFgFnSc4	svůj
okenici	okenice	k1gFnSc4	okenice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
menší	malý	k2eAgFnSc4d2	menší
než	než	k8xS	než
míli	míle	k1gFnSc4	míle
Galileo	Galilea	k1gFnSc5	Galilea
nezjistil	zjistit	k5eNaPmAgInS	zjistit
rozdíl	rozdíl	k1gInSc4	rozdíl
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
chyba	chyba	k1gFnSc1	chyba
měření	měření	k1gNnSc2	měření
oproti	oproti	k7c3	oproti
případu	případ	k1gInSc3	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
asistent	asistent	k1gMnSc1	asistent
byli	být	k5eAaImAgMnP	být
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
yardů	yard	k1gInPc2	yard
vzdáleni	vzdálen	k2eAgMnPc1d1	vzdálen
<g/>
.	.	kIx.	.
</s>
<s>
Neučinil	učinit	k5eNaPmAgMnS	učinit
předčasný	předčasný	k2eAgInSc4d1	předčasný
závěr	závěr	k1gInSc4	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
šíří	šířit	k5eAaImIp3nS	šířit
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
raději	rád	k6eAd2	rád
uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
kopci	kopec	k1gInPc7	kopec
byla	být	k5eAaImAgFnS	být
asi	asi	k9	asi
příliš	příliš	k6eAd1	příliš
malá	malý	k2eAgFnSc1d1	malá
pro	pro	k7c4	pro
dobré	dobrý	k2eAgNnSc4d1	dobré
měření	měření	k1gNnSc4	měření
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
případech	případ	k1gInPc6	případ
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
k	k	k7c3	k
odlišení	odlišení	k1gNnSc3	odlišení
od	od	k7c2	od
"	"	kIx"	"
<g/>
čisté	čistý	k2eAgFnSc2d1	čistá
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
"	"	kIx"	"
nazývá	nazývat	k5eAaImIp3nS	nazývat
technologií	technologie	k1gFnSc7	technologie
<g/>
,	,	kIx,	,
a	a	k8xC	a
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
ostatní	ostatní	k2eAgMnSc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tentýž	týž	k3xTgInSc1	týž
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
používal	používat	k5eAaImAgInS	používat
Aristotelés	Aristotelés	k1gInSc4	Aristotelés
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
považoval	považovat	k5eAaImAgInS	považovat
všechnu	všechen	k3xTgFnSc4	všechen
Galileovu	Galileův	k2eAgFnSc4d1	Galileova
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
techne	technout	k5eAaPmIp3nS	technout
neboli	neboli	k8xC	neboli
užitečnou	užitečný	k2eAgFnSc4d1	užitečná
znalost	znalost	k1gFnSc4	znalost
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
episteme	episíst	k5eAaPmIp1nP	episíst
neboli	neboli	k8xC	neboli
filosofickému	filosofický	k2eAgNnSc3d1	filosofické
zkoumání	zkoumání	k1gNnSc3	zkoumání
příčin	příčina	k1gFnPc2	příčina
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1595	[number]	k4	1595
<g/>
–	–	k?	–
<g/>
1598	[number]	k4	1598
Galileo	Galilea	k1gFnSc5	Galilea
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
a	a	k8xC	a
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
"	"	kIx"	"
<g/>
geometrický	geometrický	k2eAgInSc4d1	geometrický
a	a	k8xC	a
vojenský	vojenský	k2eAgInSc4d1	vojenský
kompas	kompas	k1gInSc4	kompas
<g/>
"	"	kIx"	"
vhodný	vhodný	k2eAgMnSc1d1	vhodný
pro	pro	k7c4	pro
dělostřelce	dělostřelec	k1gMnSc4	dělostřelec
a	a	k8xC	a
zeměměřiče	zeměměřič	k1gMnSc4	zeměměřič
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
tím	ten	k3xDgNnSc7	ten
dřívější	dřívější	k2eAgInPc4d1	dřívější
přístroje	přístroj	k1gInPc4	přístroj
navržené	navržený	k2eAgInPc4d1	navržený
Niccolou	Niccola	k1gFnSc7	Niccola
Tartagliou	Tartagliý	k2eAgFnSc7d1	Tartagliý
a	a	k8xC	a
Guidobaldim	Guidobaldi	k1gNnSc7	Guidobaldi
del	del	k?	del
Monte	Mont	k1gMnSc5	Mont
<g/>
.	.	kIx.	.
</s>
<s>
Dělostřelcům	dělostřelec	k1gMnPc3	dělostřelec
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
nové	nový	k2eAgFnSc2d1	nová
a	a	k8xC	a
bezpečnější	bezpečný	k2eAgFnSc2d2	bezpečnější
cesty	cesta	k1gFnSc2	cesta
přesného	přesný	k2eAgNnSc2d1	přesné
zvedání	zvedání	k1gNnSc2	zvedání
kanónů	kanón	k1gInPc2	kanón
<g/>
,	,	kIx,	,
také	také	k9	také
způsob	způsob	k1gInSc1	způsob
rychlého	rychlý	k2eAgNnSc2d1	rychlé
spočítání	spočítání	k1gNnSc2	spočítání
náplně	náplň	k1gFnSc2	náplň
střelného	střelný	k2eAgInSc2d1	střelný
prachu	prach	k1gInSc2	prach
pro	pro	k7c4	pro
dělové	dělový	k2eAgFnPc4d1	dělová
koule	koule	k1gFnPc4	koule
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
a	a	k8xC	a
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
geometrické	geometrický	k2eAgFnPc1d1	geometrická
pomůcky	pomůcka	k1gFnPc1	pomůcka
umožnily	umožnit	k5eAaPmAgFnP	umožnit
konstrukci	konstrukce	k1gFnSc4	konstrukce
libovolného	libovolný	k2eAgInSc2d1	libovolný
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
mnohoúhelníku	mnohoúhelník	k1gInSc2	mnohoúhelník
<g/>
,	,	kIx,	,
výpočet	výpočet	k1gInSc1	výpočet
povrchu	povrch	k1gInSc2	povrch
libovolného	libovolný	k2eAgInSc2d1	libovolný
mnohoúhelníku	mnohoúhelník	k1gInSc2	mnohoúhelník
nebo	nebo	k8xC	nebo
kruhové	kruhový	k2eAgFnSc2d1	kruhová
výseče	výseč	k1gFnSc2	výseč
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgInPc2d1	další
výpočtů	výpočet	k1gInPc2	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1606	[number]	k4	1606
<g/>
–	–	k?	–
<g/>
1607	[number]	k4	1607
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
možná	možná	k9	možná
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
jiný	jiný	k2eAgInSc1d1	jiný
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
1597	[number]	k4	1597
<g/>
)	)	kIx)	)
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
Galileo	Galilea	k1gFnSc5	Galilea
teploměr	teploměr	k1gInSc1	teploměr
<g/>
,	,	kIx,	,
využiv	využít	k5eAaPmDgInS	využít
expanze	expanze	k1gFnSc1	expanze
a	a	k8xC	a
kontrakce	kontrakce	k1gFnSc1	kontrakce
bubliny	bublina	k1gFnSc2	bublina
vzduchu	vzduch	k1gInSc2	vzduch
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
vodního	vodní	k2eAgInSc2d1	vodní
sloupce	sloupec	k1gInSc2	sloupec
v	v	k7c6	v
připojené	připojený	k2eAgFnSc6d1	připojená
trubici	trubice	k1gFnSc6	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1610	[number]	k4	1610
použil	použít	k5eAaPmAgMnS	použít
dalekohledu	dalekohled	k1gInSc2	dalekohled
jako	jako	k8xC	jako
složeného	složený	k2eAgInSc2d1	složený
mikroskopu	mikroskop	k1gInSc2	mikroskop
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1623	[number]	k4	1623
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
vylepšené	vylepšený	k2eAgInPc4d1	vylepšený
mikroskopy	mikroskop	k1gInPc4	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
počin	počin	k1gInSc1	počin
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgNnSc7	první
dokumentovaným	dokumentovaný	k2eAgNnSc7d1	dokumentované
užitím	užití	k1gNnSc7	užití
složeného	složený	k2eAgInSc2d1	složený
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1612	[number]	k4	1612
<g/>
,	,	kIx,	,
když	když	k8xS	když
určil	určit	k5eAaPmAgInS	určit
doby	doba	k1gFnPc4	doba
oběhů	oběh	k1gInPc2	oběh
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
satelitů	satelit	k1gInPc2	satelit
<g/>
,	,	kIx,	,
Galileo	Galilea	k1gFnSc5	Galilea
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
dostatečně	dostatečně	k6eAd1	dostatečně
přesnou	přesný	k2eAgFnSc7d1	přesná
znalostí	znalost	k1gFnSc7	znalost
jejich	jejich	k3xOp3gFnPc2	jejich
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
užít	užít	k5eAaPmF	užít
jejich	jejich	k3xOp3gFnPc4	jejich
pozice	pozice	k1gFnPc4	pozice
jako	jako	k8xC	jako
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
umožnit	umožnit	k5eAaPmF	umožnit
určení	určení	k1gNnSc1	určení
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
problému	problém	k1gInSc6	problém
z	z	k7c2	z
času	čas	k1gInSc2	čas
na	na	k7c4	na
čas	čas	k1gInSc4	čas
pracoval	pracovat	k5eAaImAgMnS	pracovat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
;	;	kIx,	;
praktické	praktický	k2eAgInPc1d1	praktický
problémy	problém	k1gInPc1	problém
byly	být	k5eAaImAgInP	být
stále	stále	k6eAd1	stále
příliš	příliš	k6eAd1	příliš
vážné	vážný	k2eAgFnPc1d1	vážná
<g/>
.	.	kIx.	.
</s>
<s>
Metodu	metoda	k1gFnSc4	metoda
poprvé	poprvé	k6eAd1	poprvé
úspěšně	úspěšně	k6eAd1	úspěšně
použil	použít	k5eAaPmAgMnS	použít
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
Domenico	Domenico	k6eAd1	Domenico
Cassini	Cassin	k2eAgMnPc1d1	Cassin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1681	[number]	k4	1681
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
široce	široko	k6eAd1	široko
využívána	využívat	k5eAaImNgFnS	využívat
v	v	k7c6	v
kartografii	kartografie	k1gFnSc6	kartografie
<g/>
;	;	kIx,	;
první	první	k4xOgFnSc7	první
praktickou	praktický	k2eAgFnSc7d1	praktická
metodou	metoda	k1gFnSc7	metoda
pro	pro	k7c4	pro
navigaci	navigace	k1gFnSc4	navigace
byl	být	k5eAaImAgInS	být
chronometer	chronometer	k1gInSc1	chronometer
Johna	John	k1gMnSc2	John
Harrisona	Harrison	k1gMnSc2	Harrison
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
roce	rok	k1gInSc6	rok
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
úplně	úplně	k6eAd1	úplně
slepý	slepý	k2eAgMnSc1d1	slepý
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
krokové	krokový	k2eAgNnSc4d1	krokové
ústrojí	ústrojí	k1gNnSc4	ústrojí
kyvadlových	kyvadlový	k2eAgFnPc2d1	kyvadlová
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgFnPc4d1	funkční
kyvadlové	kyvadlový	k2eAgFnPc4d1	kyvadlová
hodiny	hodina	k1gFnPc4	hodina
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Christiaan	Christiaan	k1gInSc1	Christiaan
Huygens	Huygensa	k1gFnPc2	Huygensa
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
skici	skica	k1gFnSc6	skica
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
vynálezů	vynález	k1gInPc2	vynález
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
kombinaci	kombinace	k1gFnSc4	kombinace
svíčky	svíčka	k1gFnSc2	svíčka
a	a	k8xC	a
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
k	k	k7c3	k
odrážení	odrážení	k1gNnSc3	odrážení
světla	světlo	k1gNnSc2	světlo
po	po	k7c6	po
budovách	budova	k1gFnPc6	budova
<g/>
,	,	kIx,	,
automatický	automatický	k2eAgInSc1d1	automatický
sběrač	sběrač	k1gInSc1	sběrač
rajčat	rajče	k1gNnPc2	rajče
<g/>
,	,	kIx,	,
kapesní	kapesní	k2eAgInSc1d1	kapesní
hřeben	hřeben	k1gInSc1	hřeben
sloužící	sloužící	k2eAgInSc1d1	sloužící
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k8xC	jako
jídelní	jídelní	k2eAgInSc1d1	jídelní
příbor	příbor	k1gInSc1	příbor
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
nyní	nyní	k6eAd1	nyní
nazýváme	nazývat	k5eAaImIp1nP	nazývat
kuličkovým	kuličkový	k2eAgNnSc7d1	kuličkové
perem	pero	k1gNnSc7	pero
<g/>
.	.	kIx.	.
</s>
<s>
Galileovy	Galileův	k2eAgInPc4d1	Galileův
spisy	spis	k1gInPc4	spis
o	o	k7c6	o
koperníkovském	koperníkovský	k2eAgInSc6d1	koperníkovský
heliocentrismu	heliocentrismus	k1gInSc6	heliocentrismus
pobouřily	pobouřit	k5eAaPmAgFnP	pobouřit
některé	některý	k3yIgFnPc1	některý
představitele	představitel	k1gMnPc4	představitel
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c4	v
geocentrický	geocentrický	k2eAgInSc4d1	geocentrický
model	model	k1gInSc4	model
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
argumentovali	argumentovat	k5eAaImAgMnP	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
heliocentrismus	heliocentrismus	k1gInSc1	heliocentrismus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
biblí	bible	k1gFnSc7	bible
<g/>
,	,	kIx,	,
přinejmenším	přinejmenším	k6eAd1	přinejmenším
jak	jak	k6eAd1	jak
byla	být	k5eAaImAgFnS	být
interpretována	interpretován	k2eAgFnSc1d1	interpretována
církevními	církevní	k2eAgMnPc7d1	církevní
Otci	otec	k1gMnPc7	otec
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
vysoce	vysoce	k6eAd1	vysoce
ceněnými	ceněný	k2eAgInPc7d1	ceněný
starými	starý	k2eAgInPc7d1	starý
spisy	spis	k1gInPc7	spis
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
a	a	k8xC	a
Platóna	Platón	k1gMnSc2	Platón
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelovská	aristotelovský	k2eAgFnSc1d1	aristotelovská
fyzika	fyzika	k1gFnSc1	fyzika
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
čtyřech	čtyři	k4xCgInPc6	čtyři
živlech	živel	k1gInPc6	živel
(	(	kIx(	(
<g/>
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
vody	voda	k1gFnPc1	voda
<g/>
,	,	kIx,	,
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
ohně	oheň	k1gInSc2	oheň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
skládá	skládat	k5eAaImIp3nS	skládat
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišovala	rozlišovat	k5eAaImAgFnS	rozlišovat
pohyb	pohyb	k1gInSc4	pohyb
na	na	k7c4	na
přirozený	přirozený	k2eAgInSc4d1	přirozený
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
složením	složení	k1gNnSc7	složení
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
těžké	těžký	k2eAgInPc1d1	těžký
živly	živel	k1gInPc1	živel
směrem	směr	k1gInSc7	směr
dolu	dol	k1gInSc2	dol
a	a	k8xC	a
lehké	lehký	k2eAgNnSc4d1	lehké
nahoru	nahoru	k6eAd1	nahoru
<g/>
)	)	kIx)	)
a	a	k8xC	a
nepřirozený	přirozený	k2eNgInSc1d1	nepřirozený
působený	působený	k2eAgInSc1d1	působený
vnější	vnější	k2eAgFnSc7d1	vnější
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Setrvačný	setrvačný	k2eAgInSc1d1	setrvačný
pohyb	pohyb	k1gInSc1	pohyb
nějaké	nějaký	k3yIgNnSc4	nějaký
smysluplnější	smysluplný	k2eAgNnSc4d2	smysluplnější
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
postrádal	postrádat	k5eAaImAgMnS	postrádat
<g/>
.	.	kIx.	.
</s>
<s>
Nebeská	nebeský	k2eAgNnPc1d1	nebeské
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žádný	žádný	k3yNgInSc4	žádný
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
pohybů	pohyb	k1gInPc2	pohyb
nevykazovala	vykazovat	k5eNaImAgFnS	vykazovat
tedy	tedy	k8xC	tedy
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
cosi	cosi	k3yInSc4	cosi
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
vlastními	vlastní	k2eAgInPc7d1	vlastní
zákony	zákon	k1gInPc7	zákon
<g/>
,	,	kIx,	,
odlišnými	odlišný	k2eAgInPc7d1	odlišný
od	od	k7c2	od
těch	ten	k3xDgInPc2	ten
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jevy	jev	k1gInPc1	jev
se	se	k3xPyFc4	se
dělily	dělit	k5eAaImAgInP	dělit
na	na	k7c6	na
sublunární	sublunární	k2eAgFnSc6d1	sublunární
a	a	k8xC	a
nadlunární	nadlunární	k2eAgFnSc6d1	nadlunární
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
tedy	tedy	k9	tedy
vycházel	vycházet	k5eAaImAgMnS	vycházet
aristotelovsko-ptolemaiovský	aristotelovskotolemaiovský	k2eAgInSc4d1	aristotelovsko-ptolemaiovský
geocentrický	geocentrický	k2eAgInSc4d1	geocentrický
model	model	k1gInSc4	model
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
představy	představa	k1gFnPc1	představa
v	v	k7c6	v
povědomí	povědomí	k1gNnSc6	povědomí
lidí	člověk	k1gMnPc2	člověk
stále	stále	k6eAd1	stále
přetrvávaly	přetrvávat	k5eAaImAgInP	přetrvávat
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
již	již	k6eAd1	již
našly	najít	k5eAaPmAgFnP	najít
i	i	k8xC	i
své	svůj	k3xOyFgMnPc4	svůj
odpůrce	odpůrce	k1gMnPc4	odpůrce
(	(	kIx(	(
<g/>
Jean	Jean	k1gMnSc1	Jean
Buridan	Buridan	k1gMnSc1	Buridan
<g/>
,	,	kIx,	,
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Oresme	Oresme	k1gMnSc1	Oresme
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
odehrával	odehrávat	k5eAaImAgInS	odehrávat
mezi	mezi	k7c7	mezi
starou	starý	k2eAgFnSc7d1	stará
(	(	kIx(	(
<g/>
aristotelovskou	aristotelovský	k2eAgFnSc7d1	aristotelovská
<g/>
)	)	kIx)	)
koncepcí	koncepce	k1gFnSc7	koncepce
a	a	k8xC	a
novou	nova	k1gFnSc7	nova
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohyby	pohyb	k1gInPc1	pohyb
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
jsou	být	k5eAaImIp3nP	být
řízeny	řízen	k2eAgInPc1d1	řízen
stejnými	stejný	k2eAgInPc7d1	stejný
fyzikálními	fyzikální	k2eAgInPc7d1	fyzikální
zákony	zákon	k1gInPc7	zákon
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc4	jaký
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
Galileovy	Galileův	k2eAgInPc1d1	Galileův
názory	názor	k1gInPc1	názor
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
i	i	k9	i
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Buridan	Buridan	k1gInSc1	Buridan
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1300	[number]	k4	1300
<g/>
–	–	k?	–
<g/>
1358	[number]	k4	1358
<g/>
)	)	kIx)	)
hájil	hájit	k5eAaImAgMnS	hájit
zákon	zákon	k1gInSc4	zákon
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
a	a	k8xC	a
oponoval	oponovat	k5eAaImAgMnS	oponovat
tím	ten	k3xDgNnSc7	ten
stoupencům	stoupenec	k1gMnPc3	stoupenec
aristotelovské	aristotelovský	k2eAgFnSc2d1	aristotelovská
koncepce	koncepce	k1gFnSc2	koncepce
<g/>
.	.	kIx.	.
</s>
<s>
Oresme	Oresit	k5eAaImRp1nP	Oresit
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1325	[number]	k4	1325
<g/>
–	–	k?	–
<g/>
1382	[number]	k4	1382
<g/>
)	)	kIx)	)
cca	cca	kA	cca
170	[number]	k4	170
let	léto	k1gNnPc2	léto
před	před	k7c7	před
Koperníkem	Koperník	k1gInSc7	Koperník
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
oběhu	oběh	k1gInSc6	oběh
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
objevil	objevit	k5eAaPmAgInS	objevit
zákon	zákon	k1gInSc1	zákon
volného	volný	k2eAgInSc2d1	volný
pádu	pád	k1gInSc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Buridanem	Buridan	k1gMnSc7	Buridan
odmítali	odmítat	k5eAaImAgMnP	odmítat
také	také	k9	také
rozdělování	rozdělování	k1gNnSc4	rozdělování
na	na	k7c4	na
sublunární	sublunární	k2eAgFnSc4d1	sublunární
a	a	k8xC	a
nadlunární	nadlunární	k2eAgFnSc4d1	nadlunární
sféru	sféra	k1gFnSc4	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Albert	Albert	k1gMnSc1	Albert
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1206	[number]	k4	1206
<g/>
–	–	k?	–
<g/>
1280	[number]	k4	1280
<g/>
)	)	kIx)	)
radil	radit	k5eAaImAgMnS	radit
nespoléhat	spoléhat	k5eNaImF	spoléhat
na	na	k7c4	na
výroky	výrok	k1gInPc4	výrok
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
autorit	autorita	k1gFnPc2	autorita
(	(	kIx(	(
<g/>
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ověřovat	ověřovat	k5eAaImF	ověřovat
pomocí	pomocí	k7c2	pomocí
experimentů	experiment	k1gInPc2	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
se	se	k3xPyFc4	se
nebránil	bránit	k5eNaImAgMnS	bránit
odlišnému	odlišný	k2eAgNnSc3d1	odlišné
pojetí	pojetí	k1gNnSc3	pojetí
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
hodnocení	hodnocení	k1gNnSc3	hodnocení
domněnek	domněnka	k1gFnPc2	domněnka
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
souhlas	souhlas	k1gInSc4	souhlas
s	s	k7c7	s
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
se	se	k3xPyFc4	se
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
církevními	církevní	k2eAgMnPc7d1	církevní
představiteli	představitel	k1gMnPc7	představitel
setkávali	setkávat	k5eAaImAgMnP	setkávat
také	také	k9	také
<g/>
,	,	kIx,	,
rozhodně	rozhodně	k6eAd1	rozhodně
ne	ne	k9	ne
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
míře	míra	k1gFnSc6	míra
jako	jako	k8xC	jako
Galilei	Galile	k1gFnSc6	Galile
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
však	však	k9	však
Galilei	Galile	k1gFnPc1	Galile
psal	psát	k5eAaImAgInS	psát
některá	některý	k3yIgNnPc4	některý
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
i	i	k8xC	i
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
dostávala	dostávat	k5eAaImAgFnS	dostávat
mimo	mimo	k7c4	mimo
úzký	úzký	k2eAgInSc4d1	úzký
okruh	okruh	k1gInSc4	okruh
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
tak	tak	k9	tak
ke	k	k7c3	k
špatným	špatný	k2eAgFnPc3d1	špatná
interpretacím	interpretace	k1gFnPc3	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Teologové	teolog	k1gMnPc1	teolog
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohli	moct	k5eAaImAgMnP	moct
obávat	obávat	k5eAaImF	obávat
i	i	k9	i
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zaměňování	zaměňování	k1gNnSc2	zaměňování
"	"	kIx"	"
<g/>
nebe	nebe	k1gNnSc2	nebe
<g/>
"	"	kIx"	"
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
astronomickém	astronomický	k2eAgInSc6d1	astronomický
a	a	k8xC	a
náboženském	náboženský	k2eAgInSc6d1	náboženský
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podkopána	podkopán	k2eAgFnSc1d1	podkopána
víra	víra	k1gFnSc1	víra
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
nevzdělaného	vzdělaný	k2eNgInSc2d1	nevzdělaný
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
spor	spor	k1gInSc1	spor
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
zemské	zemský	k2eAgFnSc2d1	zemská
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Galilei	Galilei	k6eAd1	Galilei
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
Dialogu	dialog	k1gInSc6	dialog
o	o	k7c6	o
dvou	dva	k4xCgInPc6	dva
největších	veliký	k2eAgInPc6d3	veliký
systémech	systém	k1gInPc6	systém
světa	svět	k1gInSc2	svět
snažil	snažit	k5eAaImAgMnS	snažit
vyvodit	vyvodit	k5eAaPmF	vyvodit
z	z	k7c2	z
existence	existence	k1gFnSc2	existence
slapových	slapový	k2eAgInPc2d1	slapový
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
se	se	k3xPyFc4	se
však	však	k9	však
zmýlil	zmýlit	k5eAaPmAgMnS	zmýlit
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
Bellarmino	Bellarmin	k2eAgNnSc1d1	Bellarmino
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1615	[number]	k4	1615
Galileovi	Galileus	k1gMnSc3	Galileus
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
názor	názor	k1gInSc1	názor
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
správný	správný	k2eAgInSc1d1	správný
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
opustit	opustit	k5eAaPmF	opustit
stávající	stávající	k2eAgInSc4d1	stávající
výklad	výklad	k1gInSc4	výklad
biblických	biblický	k2eAgFnPc2d1	biblická
pasáží	pasáž	k1gFnPc2	pasáž
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
výroky	výrok	k1gInPc4	výrok
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
Slunce	slunce	k1gNnSc2	slunce
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
dokud	dokud	k6eAd1	dokud
přesvědčivý	přesvědčivý	k2eAgInSc1d1	přesvědčivý
důkaz	důkaz	k1gInSc1	důkaz
chyběl	chybět	k5eAaImAgInS	chybět
<g/>
,	,	kIx,	,
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
držení	držení	k1gNnSc6	držení
se	se	k3xPyFc4	se
stávajícího	stávající	k2eAgNnSc2d1	stávající
pojetí	pojetí	k1gNnSc2	pojetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
karmelitánu	karmelitán	k1gMnSc3	karmelitán
Foscarinimu	Foscarinim	k1gMnSc3	Foscarinim
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zastával	zastávat	k5eAaImAgMnS	zastávat
Galileiho	Galilei	k1gMnSc4	Galilei
pozice	pozice	k1gFnSc2	pozice
a	a	k8xC	a
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
teologického	teologický	k2eAgNnSc2d1	teologické
hlediska	hledisko	k1gNnSc2	hledisko
Belarmini	Belarmin	k2eAgMnPc1d1	Belarmin
připomíná	připomínat	k5eAaImIp3nS	připomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
heliocentrizmus	heliocentrizmus	k1gInSc1	heliocentrizmus
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
pouhou	pouhý	k2eAgFnSc7d1	pouhá
hypotézou	hypotéza	k1gFnSc7	hypotéza
<g/>
,	,	kIx,	,
pokusem	pokus	k1gInSc7	pokus
jak	jak	k8xS	jak
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
určité	určitý	k2eAgInPc4d1	určitý
fenomény	fenomén	k1gInPc4	fenomén
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
mluvit	mluvit	k5eAaImF	mluvit
jako	jako	k8xS	jako
o	o	k7c6	o
hypotéze	hypotéza	k1gFnSc6	hypotéza
a	a	k8xC	a
ve	v	k7c6	v
sporných	sporný	k2eAgInPc6d1	sporný
případech	případ	k1gInPc6	případ
dát	dát	k5eAaPmF	dát
přednost	přednost	k1gFnSc4	přednost
Písmu	písmo	k1gNnSc3	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
prokázala	prokázat	k5eAaPmAgFnS	prokázat
platnost	platnost	k1gFnSc4	platnost
této	tento	k3xDgFnSc2	tento
hypotézy	hypotéza	k1gFnSc2	hypotéza
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
budou	být	k5eAaImBp3nP	být
teologové	teolog	k1gMnPc1	teolog
vyzváni	vyzvat	k5eAaPmNgMnP	vyzvat
k	k	k7c3	k
reinterpretaci	reinterpretace	k1gFnSc3	reinterpretace
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pokládáno	pokládat	k5eAaImNgNnS	pokládat
za	za	k7c2	za
učení	učení	k1gNnSc2	učení
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Bellarmino	Bellarmin	k2eAgNnSc1d1	Bellarmino
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k1gMnPc2	jiný
<g/>
)	)	kIx)	)
nedomníval	domnívat	k5eNaImAgInS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
nehybnost	nehybnost	k1gFnSc1	nehybnost
Země	zem	k1gFnSc2	zem
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
víry	vír	k1gInPc1	vír
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
vědeckého	vědecký	k2eAgInSc2d1	vědecký
důkazu	důkaz	k1gInSc2	důkaz
byl	být	k5eAaImAgInS	být
ochoten	ochoten	k2eAgInSc1d1	ochoten
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
upustit	upustit	k5eAaPmF	upustit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Oresme	Oresme	k1gFnSc1	Oresme
již	již	k6eAd1	již
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rotaci	rotace	k1gFnSc4	rotace
Země	zem	k1gFnSc2	zem
nelze	lze	k6eNd1	lze
ani	ani	k8xC	ani
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k8xC	ani
z	z	k7c2	z
bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Galilei	Galilei	k1gNnSc1	Galilei
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
zase	zase	k9	zase
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
k	k	k7c3	k
opatrnosti	opatrnost	k1gFnSc3	opatrnost
v	v	k7c6	v
interpretaci	interpretace	k1gFnSc6	interpretace
Písma	písmo	k1gNnSc2	písmo
ohledně	ohledně	k7c2	ohledně
přírodovědeckých	přírodovědecký	k2eAgFnPc2d1	Přírodovědecká
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
psáno	psát	k5eAaImNgNnS	psát
neodborným	odborný	k2eNgInSc7d1	neodborný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
Galilei	Galile	k1gFnSc2	Galile
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
důkaz	důkaz	k1gInSc4	důkaz
něco	něco	k6eAd1	něco
<g/>
,	,	kIx,	,
co	co	k9	co
důkazem	důkaz	k1gInSc7	důkaz
nebylo	být	k5eNaImAgNnS	být
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
mnozí	mnohý	k2eAgMnPc1d1	mnohý
teologové	teolog	k1gMnPc1	teolog
chápali	chápat	k5eAaImAgMnP	chápat
rotaci	rotace	k1gFnSc4	rotace
Země	zem	k1gFnSc2	zem
jako	jako	k8xS	jako
tvrzení	tvrzení	k1gNnSc1	tvrzení
odporující	odporující	k2eAgNnSc1d1	odporující
Písmu	písmo	k1gNnSc3	písmo
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
odporovala	odporovat	k5eAaImAgFnS	odporovat
především	především	k9	především
konvenčnímu	konvenční	k2eAgNnSc3d1	konvenční
chápání	chápání	k1gNnSc3	chápání
přírodních	přírodní	k2eAgInPc2d1	přírodní
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
formulovala	formulovat	k5eAaImAgFnS	formulovat
aristotelovská	aristotelovský	k2eAgFnSc1d1	aristotelovská
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
značně	značně	k6eAd1	značně
zkomplikovala	zkomplikovat	k5eAaPmAgNnP	zkomplikovat
i	i	k9	i
pozorování	pozorování	k1gNnPc1	pozorování
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
provedl	provést	k5eAaPmAgMnS	provést
Tycho	Tyc	k1gMnSc4	Tyc
Brahe	Brah	k1gMnSc4	Brah
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
heliocentrismus	heliocentrismus	k1gInSc1	heliocentrismus
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nasvědčující	nasvědčující	k2eAgMnSc1d1	nasvědčující
necyklickým	cyklický	k2eNgFnPc3d1	necyklická
dráhám	dráha	k1gFnPc3	dráha
komet	kometa	k1gFnPc2	kometa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
cyklickými	cyklický	k2eAgFnPc7d1	cyklická
drahami	draha	k1gFnPc7	draha
heliocentrického	heliocentrický	k2eAgInSc2d1	heliocentrický
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Galilei	Galilei	k6eAd1	Galilei
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
komety	kometa	k1gFnPc1	kometa
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
jako	jako	k8xC	jako
optické	optický	k2eAgInPc4d1	optický
klamy	klam	k1gInPc4	klam
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Keplera	Kepler	k1gMnSc2	Kepler
Galilei	Galile	k1gFnSc2	Galile
eliptické	eliptický	k2eAgFnSc2d1	eliptická
dráhy	dráha	k1gFnSc2	dráha
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případu	případ	k1gInSc6	případ
Galilea	Galilea	k1gFnSc1	Galilea
nejde	jít	k5eNaImIp3nS	jít
ani	ani	k8xC	ani
tak	tak	k6eAd1	tak
o	o	k7c4	o
spor	spor	k1gInSc4	spor
mezi	mezi	k7c7	mezi
náboženstvím	náboženství	k1gNnSc7	náboženství
a	a	k8xC	a
vědou	věda	k1gFnSc7	věda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
o	o	k7c4	o
nedorozumění	nedorozumění	k1gNnSc4	nedorozumění
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
kompetence	kompetence	k1gFnSc2	kompetence
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
co	co	k3yQnSc1	co
do	do	k7c2	do
kompetence	kompetence	k1gFnSc2	kompetence
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
mezi	mezi	k7c7	mezi
přírodními	přírodní	k2eAgFnPc7d1	přírodní
vědami	věda	k1gFnPc7	věda
a	a	k8xC	a
náboženstvím	náboženství	k1gNnSc7	náboženství
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
rozpor	rozpor	k1gInSc4	rozpor
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
i	i	k9	i
Francis	Francis	k1gFnSc4	Francis
Bacon	Bacon	k1gInSc4	Bacon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
tvůrců	tvůrce	k1gMnPc2	tvůrce
vědecké	vědecký	k2eAgFnSc2d1	vědecká
metody	metoda	k1gFnSc2	metoda
<g/>
:	:	kIx,	:
V	v	k7c6	v
době	doba	k1gFnSc6	doba
sporů	spor	k1gInPc2	spor
se	se	k3xPyFc4	se
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
vzdala	vzdát	k5eAaPmAgFnS	vzdát
Ptolemaiova	Ptolemaiův	k2eAgInSc2d1	Ptolemaiův
modelu	model	k1gInSc2	model
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
modelu	model	k1gInSc2	model
Tychona	Tychon	k1gMnSc2	Tychon
Brahe	Brah	k1gMnSc2	Brah
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
stojí	stát	k5eAaImIp3nP	stát
Země	zem	k1gFnPc1	zem
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc1	slunce
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
krouží	kroužit	k5eAaImIp3nP	kroužit
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
geometricky	geometricky	k6eAd1	geometricky
ekvivalentní	ekvivalentní	k2eAgInPc4d1	ekvivalentní
Koperníkově	Koperníkův	k2eAgInSc6d1	Koperníkův
modelu	model	k1gInSc6	model
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
další	další	k2eAgFnSc4d1	další
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepředpokládal	předpokládat	k5eNaImAgMnS	předpokládat
paralaxu	paralaxa	k1gFnSc4	paralaxa
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
kterýžto	kterýžto	k?	kterýžto
efekt	efekt	k1gInSc1	efekt
nebylo	být	k5eNaImAgNnS	být
možno	možno	k6eAd1	možno
zjistit	zjistit	k5eAaPmF	zjistit
tehdejšími	tehdejší	k2eAgInPc7d1	tehdejší
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Tychonova	Tychonův	k2eAgInSc2d1	Tychonův
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
tento	tento	k3xDgInSc4	tento
model	model	k1gInSc4	model
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
pozorování	pozorování	k1gNnSc1	pozorování
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
geocentrický	geocentrický	k2eAgInSc1d1	geocentrický
model	model	k1gInSc1	model
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc1	závěr
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
správný	správný	k2eAgInSc1d1	správný
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgMnS	být
pominut	pominut	k2eAgMnSc1d1	pominut
žádný	žádný	k1gMnSc1	žádný
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc4d1	malý
jev	jev	k1gInSc4	jev
<g/>
:	:	kIx,	:
jako	jako	k8xC	jako
například	například	k6eAd1	například
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tehdejší	tehdejší	k2eAgInPc1d1	tehdejší
přístroje	přístroj	k1gInPc1	přístroj
nebyly	být	k5eNaImAgInP	být
zcela	zcela	k6eAd1	zcela
přesné	přesný	k2eAgInPc1d1	přesný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
vesmír	vesmír	k1gInSc1	vesmír
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
pozdější	pozdní	k2eAgFnSc2d2	pozdější
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
víra	víra	k1gFnSc1	víra
ve	v	k7c4	v
věčný	věčný	k2eAgInSc4d1	věčný
a	a	k8xC	a
nekonečný	konečný	k2eNgInSc4d1	nekonečný
vesmír	vesmír	k1gInSc4	vesmír
součástí	součást	k1gFnPc2	součást
kacířských	kacířský	k2eAgFnPc2d1	kacířská
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
panteismus	panteismus	k1gInSc1	panteismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yIgNnSc4	který
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
byl	být	k5eAaImAgMnS	být
upálen	upálit	k5eAaPmNgMnS	upálit
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Pochopení	pochopení	k1gNnSc1	pochopení
tohoto	tento	k3xDgInSc2	tento
sporu	spor	k1gInSc2	spor
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
věnování	věnování	k1gNnSc1	věnování
pozornosti	pozornost	k1gFnSc2	pozornost
ne	ne	k9	ne
pouze	pouze	k6eAd1	pouze
politice	politika	k1gFnSc3	politika
náboženských	náboženský	k2eAgFnPc2d1	náboženská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
politice	politika	k1gFnSc3	politika
akademicko-filosofických	akademickoilosofický	k2eAgFnPc2d1	akademicko-filosofický
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1611	[number]	k4	1611
byl	být	k5eAaImAgMnS	být
Galilei	Galilee	k1gFnSc4	Galilee
slavnostně	slavnostně	k6eAd1	slavnostně
přijat	přijat	k2eAgInSc1d1	přijat
Římským	římský	k2eAgNnSc7d1	římské
kolegiem	kolegium	k1gNnSc7	kolegium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
observatoř	observatoř	k1gFnSc1	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
disputaci	disputace	k1gFnSc4	disputace
a	a	k8xC	a
předložil	předložit	k5eAaPmAgInS	předložit
výsledky	výsledek	k1gInPc4	výsledek
svého	svůj	k3xOyFgNnSc2	svůj
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
zčásti	zčásti	k6eAd1	zčásti
přijaty	přijmout	k5eAaPmNgFnP	přijmout
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
neprůkaznost	neprůkaznost	k1gFnSc4	neprůkaznost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
disputace	disputace	k1gFnSc1	disputace
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
zcela	zcela	k6eAd1	zcela
na	na	k7c6	na
vědecké	vědecký	k2eAgFnSc6d1	vědecká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
začaly	začít	k5eAaPmAgInP	začít
až	až	k6eAd1	až
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Galilea	Galilea	k1gFnSc1	Galilea
obvinili	obvinit	k5eAaPmAgMnP	obvinit
dva	dva	k4xCgMnPc4	dva
dominikánští	dominikánský	k2eAgMnPc1d1	dominikánský
kazatelé	kazatel	k1gMnPc1	kazatel
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
heliocentrická	heliocentrický	k2eAgFnSc1d1	heliocentrická
teorie	teorie	k1gFnSc1	teorie
přímo	přímo	k6eAd1	přímo
odporuje	odporovat	k5eAaImIp3nS	odporovat
bibli	bible	k1gFnSc3	bible
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
se	se	k3xPyFc4	se
bránil	bránit	k5eAaImAgMnS	bránit
a	a	k8xC	a
v	v	k7c6	v
písemné	písemný	k2eAgFnSc6d1	písemná
obhajobě	obhajoba	k1gFnSc6	obhajoba
adresované	adresovaný	k2eAgFnSc2d1	adresovaná
svému	svůj	k3xOyFgMnSc3	svůj
spolupracovníkovi	spolupracovník	k1gMnSc3	spolupracovník
formuloval	formulovat	k5eAaImAgMnS	formulovat
metodu	metoda	k1gFnSc4	metoda
výkladu	výklad	k1gInSc2	výklad
Písma	písmo	k1gNnSc2	písmo
<g/>
:	:	kIx,	:
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
-li	i	k?	-li
ke	k	k7c3	k
střetu	střet	k1gInSc3	střet
mezi	mezi	k7c7	mezi
doslovnou	doslovný	k2eAgFnSc7d1	doslovná
výpovědí	výpověď	k1gFnSc7	výpověď
bible	bible	k1gFnSc2	bible
a	a	k8xC	a
vědeckými	vědecký	k2eAgInPc7d1	vědecký
závěry	závěr	k1gInPc7	závěr
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
doslovná	doslovný	k2eAgFnSc1d1	doslovná
interpretace	interpretace	k1gFnSc1	interpretace
bible	bible	k1gFnSc1	bible
jistotě	jistota	k1gFnSc3	jistota
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
Galileo	Galilea	k1gFnSc5	Galilea
převrátil	převrátit	k5eAaPmAgInS	převrátit
metodu	metoda	k1gFnSc4	metoda
používanou	používaný	k2eAgFnSc4d1	používaná
teology	teolog	k1gMnPc7	teolog
<g/>
:	:	kIx,	:
ti	ten	k3xDgMnPc1	ten
naopak	naopak	k6eAd1	naopak
ve	v	k7c6	v
sporných	sporný	k2eAgFnPc6d1	sporná
otázkách	otázka	k1gFnPc6	otázka
dávali	dávat	k5eAaImAgMnP	dávat
přednost	přednost	k1gFnSc4	přednost
doslovnému	doslovný	k2eAgInSc3d1	doslovný
výkladu	výklad	k1gInSc3	výklad
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Galileovými	Galileův	k2eAgInPc7d1	Galileův
problémy	problém	k1gInPc7	problém
s	s	k7c7	s
jezuity	jezuita	k1gMnPc7	jezuita
a	a	k8xC	a
s	s	k7c7	s
dominikánským	dominikánský	k2eAgInSc7d1	dominikánský
mnichem	mnich	k1gInSc7	mnich
Caccinim	Caccinim	k1gMnSc1	Caccinim
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
z	z	k7c2	z
kazatelny	kazatelna	k1gFnSc2	kazatelna
<g/>
,	,	kIx,	,
slyšel	slyšet	k5eAaImAgMnS	slyšet
jeho	jeho	k3xOp3gMnSc1	jeho
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
proklínán	proklínat	k5eAaImNgInS	proklínat
za	za	k7c4	za
protiřečení	protiřečení	k1gNnSc4	protiřečení
Písmu	písmo	k1gNnSc3	písmo
svatému	svatý	k1gMnSc3	svatý
profesorem	profesor	k1gMnSc7	profesor
filosofie	filosofie	k1gFnSc2	filosofie
Cosimem	Cosim	k1gMnSc7	Cosim
Boscagliou	Boscaglia	k1gMnSc7	Boscaglia
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nebyl	být	k5eNaImAgInS	být
ani	ani	k9	ani
teologem	teolog	k1gMnSc7	teolog
ani	ani	k8xC	ani
knězem	kněz	k1gMnSc7	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
Galilea	Galilea	k1gFnSc1	Galilea
zastal	zastat	k5eAaPmAgMnS	zastat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
benediktinský	benediktinský	k2eAgMnSc1d1	benediktinský
opat	opat	k1gMnSc1	opat
Benedetto	Benedetto	k1gNnSc4	Benedetto
Castelli	Castelle	k1gFnSc4	Castelle
(	(	kIx(	(
<g/>
Castelli	Castelle	k1gFnSc4	Castelle
zůstal	zůstat	k5eAaPmAgMnS	zůstat
Galileovým	Galileův	k2eAgMnSc7d1	Galileův
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
ho	on	k3xPp3gMnSc4	on
v	v	k7c4	v
Arcetri	Arcetre	k1gFnSc4	Arcetre
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
Galileiho	Galilei	k1gMnSc2	Galilei
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
po	po	k7c6	po
měsících	měsíc	k1gInPc6	měsíc
úsilí	úsilí	k1gNnSc2	úsilí
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
povolení	povolení	k1gNnSc2	povolení
od	od	k7c2	od
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
vůbec	vůbec	k9	vůbec
mohl	moct	k5eAaImAgMnS	moct
činit	činit	k5eAaImF	činit
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
také	také	k9	také
profesorem	profesor	k1gMnSc7	profesor
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
bývalým	bývalý	k2eAgMnSc7d1	bývalý
Galileovým	Galileův	k2eAgMnSc7d1	Galileův
žákem	žák	k1gMnSc7	žák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
mu	on	k3xPp3gInSc3	on
Galileo	Galilea	k1gFnSc5	Galilea
napsal	napsat	k5eAaBmAgMnS	napsat
Dopis	dopis	k1gInSc4	dopis
velkovévodkyni	velkovévodkyně	k1gFnSc3	velkovévodkyně
Kristině	Kristina	k1gFnSc3	Kristina
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
už	už	k6eAd1	už
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
svou	svůj	k3xOyFgFnSc4	svůj
nelibost	nelibost	k1gFnSc4	nelibost
nad	nad	k7c7	nad
nekompetentností	nekompetentnost	k1gFnSc7	nekompetentnost
teologů	teolog	k1gMnPc2	teolog
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
Popuzení	popuzený	k2eAgMnPc1d1	popuzený
odpůrci	odpůrce	k1gMnPc1	odpůrce
na	na	k7c4	na
nic	nic	k3yNnSc4	nic
nečekali	čekat	k5eNaImAgMnP	čekat
a	a	k8xC	a
udali	udat	k5eAaPmAgMnP	udat
ho	on	k3xPp3gMnSc4	on
Římské	římský	k2eAgFnSc3d1	římská
inkvizici	inkvizice	k1gFnSc3	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Lorini	Lorin	k1gMnPc1	Lorin
dokazoval	dokazovat	k5eAaImAgInS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Galileiho	Galilei	k1gMnSc2	Galilei
doktrína	doktrína	k1gFnSc1	doktrína
není	být	k5eNaImIp3nS	být
jen	jen	k6eAd1	jen
kacířská	kacířský	k2eAgFnSc1d1	kacířská
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
ateistická	ateistický	k2eAgFnSc1d1	ateistická
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
a	a	k8xC	a
naléhavě	naléhavě	k6eAd1	naléhavě
se	se	k3xPyFc4	se
dožadoval	dožadovat	k5eAaImAgInS	dožadovat
intervence	intervence	k1gFnSc2	intervence
inkvizice	inkvizice	k1gFnSc1	inkvizice
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Fiesolský	Fiesolský	k2eAgMnSc1d1	Fiesolský
biskup	biskup	k1gMnSc1	biskup
ve	v	k7c6	v
vzteku	vztek	k1gInSc6	vztek
vykřikoval	vykřikovat	k5eAaImAgInS	vykřikovat
proti	proti	k7c3	proti
koperníkovskému	koperníkovský	k2eAgInSc3d1	koperníkovský
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
veřejně	veřejně	k6eAd1	veřejně
urážel	urážet	k5eAaPmAgMnS	urážet
Galilea	Galilea	k1gFnSc1	Galilea
a	a	k8xC	a
pomlouval	pomlouvat	k5eAaImAgMnS	pomlouvat
ho	on	k3xPp3gNnSc4	on
u	u	k7c2	u
velkovévody	velkovévoda	k1gMnSc2	velkovévoda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Pisy	Pisa	k1gFnSc2	Pisa
tajně	tajně	k6eAd1	tajně
přikázal	přikázat	k5eAaPmAgInS	přikázat
najít	najít	k5eAaPmF	najít
a	a	k8xC	a
chytit	chytit	k5eAaPmF	chytit
Galilea	Galilea	k1gFnSc1	Galilea
a	a	k8xC	a
předvést	předvést	k5eAaPmF	předvést
ho	on	k3xPp3gMnSc4	on
před	před	k7c4	před
inkvizici	inkvizice	k1gFnSc4	inkvizice
v	v	k7c6	v
Řimě	Řima	k1gFnSc6	Řima
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Florencie	Florencie	k1gFnSc2	Florencie
formálně	formálně	k6eAd1	formálně
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
nové	nový	k2eAgFnPc4d1	nová
nauky	nauka	k1gFnPc4	nauka
jako	jako	k8xC	jako
nebiblické	biblický	k2eNgNnSc4d1	nebiblické
<g/>
;	;	kIx,	;
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
V.	V.	kA	V.
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
objímal	objímat	k5eAaImAgMnS	objímat
Galilea	Galilea	k1gFnSc1	Galilea
a	a	k8xC	a
zval	zvát	k5eAaImAgMnS	zvát
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xS	jako
největšího	veliký	k2eAgMnSc4d3	veliký
astronoma	astronom	k1gMnSc4	astronom
světa	svět	k1gInSc2	svět
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
tajně	tajně	k6eAd1	tajně
poslal	poslat	k5eAaPmAgMnS	poslat
arcibiskupa	arcibiskup	k1gMnSc4	arcibiskup
Pisy	Pisa	k1gFnSc2	Pisa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
posbíral	posbírat	k5eAaPmAgInS	posbírat
důkazy	důkaz	k1gInPc4	důkaz
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Galileo	Galilea	k1gFnSc5	Galilea
měl	mít	k5eAaImAgInS	mít
však	však	k9	však
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
církve	církev	k1gFnSc2	církev
i	i	k9	i
své	svůj	k3xOyFgMnPc4	svůj
příznivce	příznivec	k1gMnSc4	příznivec
–	–	k?	–
benediktýna	benediktýn	k1gMnSc4	benediktýn
Castelliho	Castelli	k1gMnSc4	Castelli
a	a	k8xC	a
např.	např.	kA	např.
ještě	ještě	k6eAd1	ještě
karmelitána	karmelitán	k1gMnSc4	karmelitán
Foscariniho	Foscarini	k1gMnSc4	Foscarini
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
jeho	jeho	k3xOp3gFnPc4	jeho
teze	teze	k1gFnPc4	teze
obhajovat	obhajovat	k5eAaImF	obhajovat
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
teologického	teologický	k2eAgNnSc2d1	teologické
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
oponentem	oponent	k1gMnSc7	oponent
však	však	k8xC	však
byl	být	k5eAaImAgMnS	být
kardinál	kardinál	k1gMnSc1	kardinál
Roberto	Roberta	k1gFnSc5	Roberta
Bellarmino	Bellarmin	k2eAgNnSc1d1	Bellarmino
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
teologů	teolog	k1gMnPc2	teolog
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
svědomitý	svědomitý	k2eAgInSc1d1	svědomitý
<g/>
,	,	kIx,	,
upřímný	upřímný	k2eAgInSc1d1	upřímný
a	a	k8xC	a
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
<g/>
,	,	kIx,	,
trval	trvat	k5eAaImAgInS	trvat
však	však	k9	však
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
drželo	držet	k5eAaImAgNnS	držet
starého	starý	k2eAgInSc2d1	starý
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
ten	ten	k3xDgInSc1	ten
nový	nový	k2eAgInSc1d1	nový
dostatečně	dostatečně	k6eAd1	dostatečně
prokázán	prokázán	k2eAgInSc1d1	prokázán
<g/>
.	.	kIx.	.
</s>
<s>
Bellarmino	Bellarmin	k2eAgNnSc1d1	Bellarmino
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
jeho	jeho	k3xOp3gMnPc2	jeho
kolegů	kolega	k1gMnPc2	kolega
nelpěl	lpět	k5eNaImAgInS	lpět
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nehybnost	nehybnost	k1gFnSc1	nehybnost
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
ostatních	ostatní	k2eAgMnPc2d1	ostatní
teologů	teolog	k1gMnPc2	teolog
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
odlišného	odlišný	k2eAgInSc2d1	odlišný
názoru	názor	k1gInSc2	názor
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Lecazre	Lecazr	k1gInSc5	Lecazr
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
vrhá	vrhat	k5eAaImIp3nS	vrhat
podezření	podezření	k1gNnSc4	podezření
na	na	k7c4	na
dogma	dogma	k1gNnSc4	dogma
o	o	k7c6	o
vtělení	vtělení	k1gNnSc6	vtělení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jiní	jiný	k1gMnPc1	jiný
prohlašovali	prohlašovat	k5eAaImAgMnP	prohlašovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
uráží	urážet	k5eAaPmIp3nS	urážet
samé	samý	k3xTgInPc4	samý
základy	základ	k1gInPc4	základ
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
Země	zem	k1gFnSc2	zem
planetou	planeta	k1gFnSc7	planeta
a	a	k8xC	a
jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
mezi	mezi	k7c7	mezi
několika	několik	k4yIc7	několik
planetami	planeta	k1gFnPc7	planeta
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
těch	ten	k3xDgFnPc2	ten
velkých	velký	k2eAgFnPc2d1	velká
věcí	věc	k1gFnPc2	věc
byla	být	k5eAaImAgFnS	být
dělána	dělat	k5eAaImNgFnS	dělat
čistě	čistě	k6eAd1	čistě
jen	jen	k6eAd1	jen
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
křesťanství	křesťanství	k1gNnSc1	křesťanství
učí	učit	k5eAaImIp3nS	učit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
jiné	jiný	k2eAgFnPc1d1	jiná
planety	planeta	k1gFnPc1	planeta
a	a	k8xC	a
protože	protože	k8xS	protože
Bůh	bůh	k1gMnSc1	bůh
nedělá	dělat	k5eNaImIp3nS	dělat
nic	nic	k3yNnSc1	nic
nadarmo	nadarmo	k6eAd1	nadarmo
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
obydlené	obydlený	k2eAgInPc1d1	obydlený
<g/>
;	;	kIx,	;
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gMnPc1	jejich
obyvatelé	obyvatel	k1gMnPc1	obyvatel
potomky	potomek	k1gMnPc7	potomek
Adamovými	Adamův	k2eAgMnPc7d1	Adamův
<g/>
?	?	kIx.	?
</s>
<s>
Jak	jak	k6eAd1	jak
mohou	moct	k5eAaImIp3nP	moct
odvozovat	odvozovat	k5eAaImF	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
z	z	k7c2	z
Noemovy	Noemův	k2eAgFnSc2d1	Noemova
archy	archa	k1gFnSc2	archa
<g/>
?	?	kIx.	?
</s>
<s>
Jak	jak	k6eAd1	jak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vykoupeni	vykoupen	k2eAgMnPc1d1	vykoupen
Spasitelem	spasitel	k1gMnSc7	spasitel
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ačkoliv	ačkoliv	k8xS	ačkoliv
tohoto	tento	k3xDgInSc2	tento
argumentu	argument	k1gInSc2	argument
používali	používat	k5eAaImAgMnP	používat
především	především	k9	především
katoličtí	katolický	k2eAgMnPc1d1	katolický
teologové	teolog	k1gMnPc1	teolog
<g/>
,	,	kIx,	,
protestant	protestant	k1gMnSc1	protestant
Melanchthon	Melanchthon	k1gMnSc1	Melanchthon
jej	on	k3xPp3gMnSc4	on
již	již	k6eAd1	již
použil	použít	k5eAaPmAgMnS	použít
při	při	k7c6	při
jeho	jeho	k3xOp3gInPc6	jeho
útocích	útok	k1gInPc6	útok
na	na	k7c4	na
Koperníka	Koperník	k1gMnSc4	Koperník
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nařčeních	nařčení	k1gNnPc6	nařčení
se	se	k3xPyFc4	se
Galileo	Galilea	k1gFnSc5	Galilea
optimisticky	optimisticky	k6eAd1	optimisticky
vydal	vydat	k5eAaPmAgMnS	vydat
(	(	kIx(	(
<g/>
maje	mít	k5eAaImSgMnS	mít
mezi	mezi	k7c7	mezi
jezuity	jezuita	k1gMnPc7	jezuita
pověst	pověst	k1gFnSc4	pověst
seriózního	seriózní	k2eAgMnSc4d1	seriózní
vědce	vědec	k1gMnSc4	vědec
<g/>
)	)	kIx)	)
bránit	bránit	k5eAaImF	bránit
své	svůj	k3xOyFgInPc4	svůj
postoje	postoj	k1gInPc4	postoj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
byla	být	k5eAaImAgFnS	být
odmítnuta	odmítnut	k2eAgFnSc1d1	odmítnuta
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
disputace	disputace	k1gFnSc1	disputace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
tvrzení	tvrzení	k1gNnSc1	tvrzení
o	o	k7c6	o
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
byla	být	k5eAaImAgFnS	být
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1616	[number]	k4	1616
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
jako	jako	k9	jako
absurdní	absurdní	k2eAgFnSc1d1	absurdní
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
propagování	propagování	k1gNnSc1	propagování
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Galileia	Galileius	k1gMnSc4	Galileius
varovali	varovat	k5eAaImAgMnP	varovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neschvaloval	schvalovat	k5eNaImAgMnS	schvalovat
ani	ani	k8xC	ani
nebránil	bránit	k5eNaImAgMnS	bránit
hypotézu	hypotéza	k1gFnSc4	hypotéza
obsaženou	obsažený	k2eAgFnSc4d1	obsažená
v	v	k7c6	v
Koperníkově	Koperníkův	k2eAgInSc6d1	Koperníkův
spisu	spis	k1gInSc6	spis
O	o	k7c6	o
obězích	oběh	k1gInPc6	oběh
sfér	sféra	k1gFnPc2	sféra
nebeských	nebeský	k2eAgMnPc2d1	nebeský
(	(	kIx(	(
<g/>
De	De	k?	De
revolutionibus	revolutionibus	k1gInSc1	revolutionibus
orbium	orbium	k1gNnSc1	orbium
coelestium	coelestium	k1gNnSc1	coelestium
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
předmětem	předmět	k1gInSc7	předmět
debat	debata	k1gFnPc2	debata
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
také	také	k9	také
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
srozuměn	srozuměn	k2eAgInSc1d1	srozuměn
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
heliocentrickou	heliocentrický	k2eAgFnSc4d1	heliocentrická
teorii	teorie	k1gFnSc4	teorie
"	"	kIx"	"
<g/>
učit	učit	k5eAaImF	učit
žádným	žádný	k3yNgInSc7	žádný
způsobem	způsob	k1gInSc7	způsob
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
Galileo	Galilea	k1gFnSc5	Galilea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1633	[number]	k4	1633
zkusil	zkusit	k5eAaPmAgMnS	zkusit
<g/>
,	,	kIx,	,
inkvizice	inkvizice	k1gFnSc1	inkvizice
zahájila	zahájit	k5eAaPmAgFnS	zahájit
soudní	soudní	k2eAgFnSc4d1	soudní
při	pře	k1gFnSc4	pře
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
přikázáno	přikázat	k5eAaPmNgNnS	přikázat
neučit	učit	k5eNaImF	učit
ji	on	k3xPp3gFnSc4	on
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
založenou	založený	k2eAgFnSc7d1	založená
na	na	k7c6	na
listině	listina	k1gFnSc6	listina
se	se	k3xPyFc4	se
záznamy	záznam	k1gInPc7	záznam
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1616	[number]	k4	1616
<g/>
,	,	kIx,	,
Galileo	Galilea	k1gFnSc5	Galilea
však	však	k9	však
dodal	dodat	k5eAaPmAgMnS	dodat
dopis	dopis	k1gInSc4	dopis
od	od	k7c2	od
kardinála	kardinál	k1gMnSc2	kardinál
Ballarmina	Ballarmin	k2eAgFnSc1d1	Ballarmin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
pouze	pouze	k6eAd1	pouze
příkaz	příkaz	k1gInSc1	příkaz
"	"	kIx"	"
<g/>
neschvalovat	schvalovat	k5eNaImF	schvalovat
ani	ani	k8xC	ani
nebránit	bránit	k5eNaImF	bránit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Listina	listina	k1gFnSc1	listina
byla	být	k5eAaImAgFnS	být
psána	psán	k2eAgMnSc4d1	psán
Bellarmineho	Bellarmine	k1gMnSc4	Bellarmine
vlastní	vlastní	k2eAgFnSc7d1	vlastní
rukou	ruka	k1gFnSc7	ruka
a	a	k8xC	a
nepochybně	pochybně	k6eNd1	pochybně
autentická	autentický	k2eAgFnSc1d1	autentická
<g/>
;	;	kIx,	;
listina	listina	k1gFnSc1	listina
se	s	k7c7	s
záznamy	záznam	k1gInPc7	záznam
však	však	k9	však
byla	být	k5eAaImAgFnS	být
jen	jen	k6eAd1	jen
nepodepsanou	podepsaný	k2eNgFnSc7d1	nepodepsaná
kopií	kopie	k1gFnSc7	kopie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
porušovalo	porušovat	k5eAaImAgNnS	porušovat
vlastní	vlastní	k2eAgNnSc1d1	vlastní
pravidlo	pravidlo	k1gNnSc1	pravidlo
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
,	,	kIx,	,
že	že	k8xS	že
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
takových	takový	k3xDgNnPc6	takový
varováních	varování	k1gNnPc6	varování
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
podepsány	podepsán	k2eAgInPc4d1	podepsán
všemi	všecek	k3xTgFnPc7	všecek
stranami	strana	k1gFnPc7	strana
a	a	k8xC	a
notářsky	notářsky	k6eAd1	notářsky
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Necháme	nechat	k5eAaPmIp1nP	nechat
<g/>
-li	i	k?	-li
stranou	stranou	k6eAd1	stranou
technická	technický	k2eAgNnPc4d1	technické
pravidla	pravidlo	k1gNnPc4	pravidlo
evidence	evidence	k1gFnSc2	evidence
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
lze	lze	k6eAd1	lze
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
reálné	reálný	k2eAgFnPc4d1	reálná
události	událost	k1gFnPc4	událost
<g/>
?	?	kIx.	?
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
dva	dva	k4xCgInPc4	dva
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Stillmana	Stillman	k1gMnSc2	Stillman
Drakea	Drakeus	k1gMnSc2	Drakeus
byl	být	k5eAaImAgInS	být
příkaz	příkaz	k1gInSc4	příkaz
neučit	učit	k5eNaImF	učit
doručen	doručit	k5eAaPmNgInS	doručit
neoficiálně	oficiálně	k6eNd1	oficiálně
a	a	k8xC	a
nesprávně	správně	k6eNd1	správně
<g/>
;	;	kIx,	;
Bellarmino	Bellarmin	k2eAgNnSc1d1	Bellarmino
nedovolil	dovolit	k5eNaPmAgMnS	dovolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
formální	formální	k2eAgInSc1d1	formální
záznam	záznam	k1gInSc1	záznam
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
doručení	doručení	k1gNnSc4	doručení
a	a	k8xC	a
Galileiho	Galilei	k1gMnSc2	Galilei
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
ujistil	ujistit	k5eAaPmAgMnS	ujistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediný	jediný	k2eAgInSc1d1	jediný
příkaz	příkaz	k1gInSc1	příkaz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
byl	být	k5eAaImAgInS	být
"	"	kIx"	"
<g/>
neschvalovat	schvalovat	k5eNaImF	schvalovat
ani	ani	k8xC	ani
nebránit	bránit	k5eNaImF	bránit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
podle	podle	k7c2	podle
Giorgio	Giorgio	k1gMnSc1	Giorgio
di	di	k?	di
Santillana	Santillan	k1gMnSc4	Santillan
byl	být	k5eAaImAgInS	být
nepodepsaný	podepsaný	k2eNgInSc1d1	nepodepsaný
záznam	záznam	k1gInSc1	záznam
jednoduše	jednoduše	k6eAd1	jednoduše
falsifikátem	falsifikát	k1gInSc7	falsifikát
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
inkvizicí	inkvizice	k1gFnSc7	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1623	[number]	k4	1623
zemřel	zemřít	k5eAaPmAgMnS	zemřít
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
XV	XV	kA	XV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Galileův	Galileův	k2eAgMnSc1d1	Galileův
blízký	blízký	k2eAgMnSc1d1	blízký
přítel	přítel	k1gMnSc1	přítel
Maffeo	Maffeo	k6eAd1	Maffeo
Barberini	Barberin	k1gMnPc1	Barberin
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
papežem	papež	k1gMnSc7	papež
Urbanem	Urban	k1gMnSc7	Urban
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
papež	papež	k1gMnSc1	papež
dal	dát	k5eAaPmAgMnS	dát
Galileovi	Galileus	k1gMnSc3	Galileus
vágní	vágní	k2eAgNnSc1d1	vágní
svolení	svolení	k1gNnSc1	svolení
ignorovat	ignorovat	k5eAaImF	ignorovat
zákaz	zákaz	k1gInSc4	zákaz
a	a	k8xC	a
napsat	napsat	k5eAaBmF	napsat
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
názorech	názor	k1gInPc6	názor
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
stále	stále	k6eAd1	stále
nesměl	smět	k5eNaImAgMnS	smět
otevřeně	otevřeně	k6eAd1	otevřeně
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
podporovat	podporovat	k5eAaImF	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
a	a	k8xC	a
napsal	napsat	k5eAaBmAgMnS	napsat
své	svůj	k3xOyFgNnSc4	svůj
mistrovské	mistrovský	k2eAgNnSc4d1	mistrovské
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
Dialogy	dialog	k1gInPc7	dialog
o	o	k7c6	o
dvou	dva	k4xCgInPc6	dva
největších	veliký	k2eAgInPc6d3	veliký
systémech	systém	k1gInPc6	systém
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Dialogo	Dialogo	k1gMnSc1	Dialogo
sopra	sopra	k1gMnSc1	sopra
i	i	k8xC	i
due	due	k?	due
massimi	massi	k1gFnPc7	massi
sistemi	siste	k1gFnPc7	siste
del	del	k?	del
mondo	mondo	k1gNnSc4	mondo
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
zkracováno	zkracován	k2eAgNnSc1d1	zkracováno
jako	jako	k8xC	jako
Dialogy	dialog	k1gInPc7	dialog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
argumentaci	argumentace	k1gFnSc4	argumentace
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
intelektuály	intelektuál	k1gMnPc7	intelektuál
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgInSc7	jeden
geocentrickým	geocentrický	k2eAgInSc7d1	geocentrický
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgMnSc7	druhý
heliocentrickým	heliocentrický	k2eAgMnSc7d1	heliocentrický
a	a	k8xC	a
laikem	laik	k1gMnSc7	laik
<g/>
,	,	kIx,	,
neutrálním	neutrální	k2eAgMnSc7d1	neutrální
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jevícím	jevící	k2eAgInSc7d1	jevící
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
pohledu	pohled	k1gInSc2	pohled
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
geocentrik	geocentrik	k1gMnSc1	geocentrik
byl	být	k5eAaImAgMnS	být
vykreslen	vykreslit	k5eAaPmNgMnS	vykreslit
hloupě	hloupě	k6eAd1	hloupě
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
heliocentrikovy	heliocentrikův	k2eAgInPc1d1	heliocentrikův
argumenty	argument	k1gInPc1	argument
často	často	k6eAd1	často
dominovaly	dominovat	k5eAaImAgInP	dominovat
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
přesvědčily	přesvědčit	k5eAaPmAgFnP	přesvědčit
i	i	k9	i
neutrálního	neutrální	k2eAgMnSc4d1	neutrální
účastníka	účastník	k1gMnSc4	účastník
sporu	spor	k1gInSc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Dialogy	dialog	k1gInPc1	dialog
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1632	[number]	k4	1632
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
katolických	katolický	k2eAgMnPc2d1	katolický
censorů	censor	k1gMnPc2	censor
<g/>
.	.	kIx.	.
</s>
<s>
Intelektuálové	intelektuál	k1gMnPc1	intelektuál
jim	on	k3xPp3gMnPc3	on
tleskali	tleskat	k5eAaImAgMnP	tleskat
<g/>
,	,	kIx,	,
z	z	k7c2	z
církevního	církevní	k2eAgInSc2d1	církevní
tábora	tábor	k1gInSc2	tábor
se	se	k3xPyFc4	se
však	však	k9	však
vzedmul	vzedmout	k5eAaPmAgMnS	vzedmout
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
Galileovu	Galileův	k2eAgNnSc3d1	Galileovo
neustávajícímu	ustávající	k2eNgNnSc3d1	neustávající
trvání	trvání	k1gNnSc3	trvání
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
byla	být	k5eAaImAgFnS	být
čistě	čistě	k6eAd1	čistě
teoretická	teoretický	k2eAgFnSc1d1	teoretická
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
jeho	jeho	k3xOp3gNnSc3	jeho
přesnému	přesný	k2eAgNnSc3d1	přesné
dodržování	dodržování	k1gNnSc3	dodržování
církevního	církevní	k2eAgInSc2d1	církevní
protokolu	protokol	k1gInSc2	protokol
pro	pro	k7c4	pro
publikování	publikování	k1gNnSc4	publikování
svých	svůj	k3xOyFgFnPc2	svůj
prací	práce	k1gFnPc2	práce
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
prvotní	prvotní	k2eAgNnSc1d1	prvotní
prozkoumání	prozkoumání	k1gNnSc1	prozkoumání
církevními	církevní	k2eAgMnPc7d1	církevní
censory	censor	k1gMnPc7	censor
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
žádost	žádost	k1gFnSc4	žádost
<g/>
)	)	kIx)	)
a	a	k8xC	a
navzdory	navzdory	k7c3	navzdory
jeho	jeho	k3xOp3gNnSc3	jeho
těsnému	těsný	k2eAgNnSc3d1	těsné
přátelství	přátelství	k1gNnSc3	přátelství
s	s	k7c7	s
papežem	papež	k1gMnSc7	papež
(	(	kIx(	(
<g/>
který	který	k3yRgInSc1	který
předsedal	předsedat	k5eAaImAgMnS	předsedat
řádům	řád	k1gInPc3	řád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Galileo	Galilea	k1gFnSc5	Galilea
roku	rok	k1gInSc2	rok
1633	[number]	k4	1633
předvolán	předvolat	k5eAaPmNgMnS	předvolat
před	před	k7c4	před
tribunál	tribunál	k1gInSc4	tribunál
římské	římský	k2eAgFnSc2d1	římská
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Inkvizice	inkvizice	k1gFnSc1	inkvizice
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
počáteční	počáteční	k2eAgFnPc4d1	počáteční
Galileovy	Galileův	k2eAgFnPc4d1	Galileova
prosby	prosba	k1gFnPc4	prosba
odložit	odložit	k5eAaPmF	odložit
nebo	nebo	k8xC	nebo
přeložit	přeložit	k5eAaPmF	přeložit
proces	proces	k1gInSc4	proces
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
podlomené	podlomený	k2eAgNnSc4d1	podlomené
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
předsedané	předsedaný	k2eAgFnSc2d1	předsedaný
papežem	papež	k1gMnSc7	papež
Urbanem	Urban	k1gMnSc7	Urban
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
inkvizice	inkvizice	k1gFnSc1	inkvizice
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
informovat	informovat	k5eAaBmF	informovat
Galileiho	Galilei	k1gMnSc4	Galilei
<g/>
,	,	kIx,	,
že	že	k8xS	že
buďto	buďto	k8xC	buďto
přijde	přijít	k5eAaPmIp3nS	přijít
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
nebo	nebo	k8xC	nebo
bude	být	k5eAaImBp3nS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
dopraven	dopravit	k5eAaPmNgInS	dopravit
tam	tam	k6eAd1	tam
v	v	k7c6	v
řetězech	řetěz	k1gInPc6	řetěz
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
před	před	k7c4	před
inkviziční	inkviziční	k2eAgInSc4d1	inkviziční
tribunál	tribunál	k1gInSc4	tribunál
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1633	[number]	k4	1633
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvoutýdenní	dvoutýdenní	k2eAgFnSc6d1	dvoutýdenní
karanténě	karanténa	k1gFnSc6	karanténa
byl	být	k5eAaImAgInS	být
Galileo	Galilea	k1gFnSc5	Galilea
držen	držet	k5eAaImNgInS	držet
v	v	k7c6	v
pohodlné	pohodlný	k2eAgFnSc6d1	pohodlná
rezidenci	rezidence	k1gFnSc6	rezidence
toskánského	toskánský	k2eAgMnSc2d1	toskánský
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
jako	jako	k8xC	jako
projev	projev	k1gInSc4	projev
přízně	přízeň	k1gFnSc2	přízeň
vlivného	vlivný	k2eAgMnSc2d1	vlivný
velkovévody	velkovévoda	k1gMnSc2	velkovévoda
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Medici	medik	k1gMnPc1	medik
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Galileův	Galileův	k2eAgInSc4d1	Galileův
příchod	příchod	k1gInSc4	příchod
a	a	k8xC	a
ptal	ptat	k5eAaImAgMnS	ptat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dlouho	dlouho	k6eAd1	dlouho
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
trvat	trvat	k5eAaImF	trvat
soudní	soudní	k2eAgNnSc4d1	soudní
řízení	řízení	k1gNnSc4	řízení
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Svatý	svatý	k2eAgInSc1d1	svatý
stolec	stolec	k1gInSc1	stolec
postupuje	postupovat	k5eAaImIp3nS	postupovat
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
příprav	příprava	k1gFnPc2	příprava
na	na	k7c6	na
zahájení	zahájení	k1gNnSc6	zahájení
formálního	formální	k2eAgNnSc2d1	formální
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
podrobil	podrobit	k5eAaPmAgInS	podrobit
naléhavým	naléhavý	k2eAgFnPc3d1	naléhavá
žádostem	žádost	k1gFnPc3	žádost
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
dostavit	dostavit	k5eAaPmF	dostavit
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
Galileo	Galilea	k1gFnSc5	Galilea
čekat	čekat	k5eAaImF	čekat
téměř	téměř	k6eAd1	téměř
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
než	než	k8xS	než
soudní	soudní	k2eAgNnSc1d1	soudní
řízení	řízení	k1gNnSc1	řízení
vůbec	vůbec	k9	vůbec
mohlo	moct	k5eAaImAgNnS	moct
začít	začít	k5eAaPmF	začít
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1633	[number]	k4	1633
byl	být	k5eAaImAgInS	být
Galileo	Galilea	k1gFnSc5	Galilea
předveden	předvést	k5eAaPmNgInS	předvést
před	před	k7c4	před
tribunál	tribunál	k1gInSc4	tribunál
a	a	k8xC	a
formální	formální	k2eAgInSc1d1	formální
výslech	výslech	k1gInSc1	výslech
vedený	vedený	k2eAgInSc1d1	vedený
inkvizicí	inkvizice	k1gFnSc7	inkvizice
začal	začít	k5eAaPmAgMnS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
výslechu	výslech	k1gInSc2	výslech
Galileo	Galilea	k1gFnSc5	Galilea
uváděl	uvádět	k5eAaImAgMnS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
neobhajoval	obhajovat	k5eNaImAgInS	obhajovat
Koperníkovu	Koperníkův	k2eAgFnSc4d1	Koperníkova
teorii	teorie	k1gFnSc4	teorie
a	a	k8xC	a
citoval	citovat	k5eAaBmAgInS	citovat
dopis	dopis	k1gInSc1	dopis
kardinála	kardinál	k1gMnSc4	kardinál
Bellarmina	Bellarmin	k2eAgMnSc4d1	Bellarmin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1615	[number]	k4	1615
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podpořil	podpořit	k5eAaPmAgMnS	podpořit
toto	tento	k3xDgNnSc4	tento
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
.	.	kIx.	.
</s>
<s>
Inkvizice	inkvizice	k1gFnSc1	inkvizice
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
dotázala	dotázat	k5eAaPmAgFnS	dotázat
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
mu	on	k3xPp3gMnSc3	on
nebylo	být	k5eNaImAgNnS	být
přikázáno	přikázat	k5eAaPmNgNnS	přikázat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1616	[number]	k4	1616
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neučil	učit	k5eNaImAgMnS	učit
koperníkovské	koperníkovský	k2eAgFnSc2d1	koperníkovská
myšlenky	myšlenka	k1gFnSc2	myšlenka
žádným	žádný	k3yNgInSc7	žádný
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
výše	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
on	on	k3xPp3gMnSc1	on
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
žádný	žádný	k1gMnSc1	žádný
takový	takový	k3xDgInSc4	takový
příkaz	příkaz	k1gInSc4	příkaz
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
a	a	k8xC	a
předložil	předložit	k5eAaPmAgMnS	předložit
dopis	dopis	k1gInSc4	dopis
od	od	k7c2	od
Bellarmina	Bellarmin	k2eAgNnSc2d1	Bellarmino
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
nesmí	smět	k5eNaImIp3nS	smět
zastávat	zastávat	k5eAaImF	zastávat
ani	ani	k8xC	ani
obhajovat	obhajovat	k5eAaImF	obhajovat
tyto	tento	k3xDgFnPc4	tento
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
držen	držen	k2eAgInSc4d1	držen
18	[number]	k4	18
dní	den	k1gInPc2	den
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
inkvizičního	inkviziční	k2eAgInSc2d1	inkviziční
úřadu	úřad	k1gInSc2	úřad
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
ve	v	k7c6	v
vězeňské	vězeňský	k2eAgFnSc6d1	vězeňská
cele	cela	k1gFnSc6	cela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
jej	on	k3xPp3gMnSc4	on
několikrát	několikrát	k6eAd1	několikrát
navštívil	navštívit	k5eAaPmAgMnS	navštívit
hlavní	hlavní	k2eAgMnSc1d1	hlavní
komisař	komisař	k1gMnSc1	komisař
inkvizice	inkvizice	k1gFnSc2	inkvizice
Vincenzo	Vincenza	k1gFnSc5	Vincenza
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgMnSc1d2	pozdější
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
)	)	kIx)	)
Maculano	Maculana	k1gFnSc5	Maculana
s	s	k7c7	s
nabídkou	nabídka	k1gFnSc7	nabídka
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
přiznání	přiznání	k1gNnSc6	přiznání
se	se	k3xPyFc4	se
k	k	k7c3	k
vině	vina	k1gFnSc3	vina
<g/>
,	,	kIx,	,
přesvědčoval	přesvědčovat	k5eAaImAgMnS	přesvědčovat
Galilea	Galilea	k1gFnSc1	Galilea
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
knihy	kniha	k1gFnSc2	kniha
zašel	zajít	k5eAaPmAgMnS	zajít
příliš	příliš	k6eAd1	příliš
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhém	druhý	k4xOgNnSc6	druhý
slyšení	slyšení	k1gNnSc6	slyšení
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Galileo	Galilea	k1gFnSc5	Galilea
uznal	uznat	k5eAaPmAgInS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochybil	pochybit	k5eAaPmAgMnS	pochybit
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgFnPc3	svůj
marnivým	marnivý	k2eAgFnPc3d1	marnivá
ambicím	ambice	k1gFnPc3	ambice
<g/>
,	,	kIx,	,
nepochopení	nepochopení	k1gNnSc2	nepochopení
a	a	k8xC	a
nepozornosti	nepozornost	k1gFnSc2	nepozornost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
umožněn	umožněn	k2eAgInSc4d1	umožněn
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
rezidence	rezidence	k1gFnSc2	rezidence
toskánského	toskánský	k2eAgMnSc2d1	toskánský
velvyslance	velvyslanec	k1gMnSc2	velvyslanec
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
odevzdal	odevzdat	k5eAaPmAgMnS	odevzdat
svoji	svůj	k3xOyFgFnSc4	svůj
psanou	psaný	k2eAgFnSc4d1	psaná
obhajobu	obhajoba	k1gFnSc4	obhajoba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
brání	bránit	k5eAaImIp3nP	bránit
proti	proti	k7c3	proti
obvinění	obvinění	k1gNnSc3	obvinění
z	z	k7c2	z
porušování	porušování	k1gNnSc2	porušování
církevního	církevní	k2eAgInSc2d1	církevní
příkazu	příkaz	k1gInSc2	příkaz
<g/>
,	,	kIx,	,
uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochybil	pochybit	k5eAaPmAgMnS	pochybit
pýchou	pýcha	k1gFnSc7	pýcha
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
své	svůj	k3xOyFgFnSc2	svůj
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
a	a	k8xC	a
prosí	prosit	k5eAaImIp3nS	prosit
o	o	k7c4	o
prominutí	prominutí	k1gNnSc4	prominutí
trestu	trest	k1gInSc2	trest
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
věku	věk	k1gInSc3	věk
a	a	k8xC	a
podlomenému	podlomený	k2eAgNnSc3d1	podlomené
zdraví	zdraví	k1gNnSc3	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
papeže	papež	k1gMnSc2	papež
podroben	podrobit	k5eAaPmNgInS	podrobit
zkoumání	zkoumání	k1gNnSc3	zkoumání
záměrů	záměr	k1gInPc2	záměr
<g/>
,	,	kIx,	,
formálnímu	formální	k2eAgInSc3d1	formální
procesu	proces	k1gInSc3	proces
zahrnujícímu	zahrnující	k2eAgInSc3d1	zahrnující
ukazování	ukazování	k1gNnSc4	ukazování
nástrojů	nástroj	k1gInPc2	nástroj
mučení	mučení	k1gNnPc2	mučení
obviněnému	obviněný	k1gMnSc3	obviněný
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
byl	být	k5eAaImAgInS	být
poslušný	poslušný	k2eAgMnSc1d1	poslušný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
já	já	k3xPp1nSc1	já
nezastával	zastávat	k5eNaImAgMnS	zastávat
koperníkovské	koperníkovský	k2eAgInPc4d1	koperníkovský
názory	názor	k1gInPc4	názor
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
bylo	být	k5eAaImAgNnS	být
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
učiněno	učinit	k5eAaPmNgNnS	učinit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsem	být	k5eAaImIp1nS	být
již	již	k6eAd1	již
řekl	říct	k5eAaPmAgMnS	říct
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1633	[number]	k4	1633
inkvizice	inkvizice	k1gFnSc1	inkvizice
konala	konat	k5eAaImAgFnS	konat
závěrečné	závěrečný	k2eAgNnSc4d1	závěrečné
slyšení	slyšení	k1gNnSc4	slyšení
Galilea	Galilea	k1gFnSc1	Galilea
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
69	[number]	k4	69
let	léto	k1gNnPc2	léto
a	a	k8xC	a
žádal	žádat	k5eAaImAgInS	žádat
o	o	k7c6	o
slitování	slitování	k1gNnSc6	slitování
s	s	k7c7	s
poukazem	poukaz	k1gInSc7	poukaz
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
politovánihodný	politovánihodný	k2eAgInSc4d1	politovánihodný
stav	stav	k1gInSc4	stav
tělesné	tělesný	k2eAgFnSc2d1	tělesná
indispozice	indispozice	k1gFnSc2	indispozice
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vyhrožováním	vyhrožování	k1gNnSc7	vyhrožování
mučením	mučení	k1gNnSc7	mučení
<g/>
,	,	kIx,	,
vězněním	věznění	k1gNnSc7	věznění
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
donutil	donutit	k5eAaPmAgInS	donutit
demonstrační	demonstrační	k2eAgInSc1d1	demonstrační
proces	proces	k1gInSc1	proces
Galilea	Galilea	k1gFnSc1	Galilea
"	"	kIx"	"
<g/>
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
,	,	kIx,	,
zatratit	zatratit	k5eAaPmF	zatratit
a	a	k8xC	a
zošklivit	zošklivit	k5eAaPmF	zošklivit
si	se	k3xPyFc3	se
<g/>
"	"	kIx"	"
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
slíbit	slíbit	k5eAaPmF	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odsoudí	odsoudit	k5eAaPmIp3nS	odsoudit
jiné	jiný	k2eAgMnPc4d1	jiný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
budou	být	k5eAaImBp3nP	být
zastávat	zastávat	k5eAaImF	zastávat
jeho	jeho	k3xOp3gInSc4	jeho
předchozí	předchozí	k2eAgInSc4d1	předchozí
pohled	pohled	k1gInSc4	pohled
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Galileo	Galilea	k1gFnSc5	Galilea
udělal	udělat	k5eAaPmAgMnS	udělat
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
církev	církev	k1gFnSc1	církev
žádala	žádat	k5eAaImAgFnS	žádat
a	a	k8xC	a
dodržel	dodržet	k5eAaPmAgMnS	dodržet
tak	tak	k6eAd1	tak
(	(	kIx(	(
<g/>
nakolik	nakolik	k6eAd1	nakolik
jen	jen	k9	jen
můžeme	moct	k5eAaImIp1nP	moct
říct	říct	k5eAaPmF	říct
<g/>
)	)	kIx)	)
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c4	o
přiznání	přiznání	k1gNnSc4	přiznání
viny	vina	k1gFnSc2	vina
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
před	před	k7c7	před
dvěma	dva	k4xCgInPc7	dva
měsíci	měsíc	k1gInPc7	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
a	a	k8xC	a
potrestán	potrestán	k2eAgInSc1d1	potrestán
doživotním	doživotní	k2eAgNnSc7d1	doživotní
vězením	vězení	k1gNnSc7	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
případu	případ	k1gInSc6	případ
zasedalo	zasedat	k5eAaImAgNnS	zasedat
10	[number]	k4	10
kardinálů	kardinál	k1gMnPc2	kardinál
inkvizitorů	inkvizitor	k1gMnPc2	inkvizitor
<g/>
,	,	kIx,	,
rozsudek	rozsudek	k1gInSc1	rozsudek
vynesený	vynesený	k2eAgInSc1d1	vynesený
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
nesl	nést	k5eAaImAgMnS	nést
podpisy	podpis	k1gInPc4	podpis
jen	jen	k9	jen
sedmi	sedm	k4xCc2	sedm
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
;	;	kIx,	;
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
chybějících	chybějící	k2eAgInPc2d1	chybějící
byl	být	k5eAaImAgInS	být
kardinál	kardinál	k1gMnSc1	kardinál
Barberini	Barberin	k1gMnPc1	Barberin
<g/>
,	,	kIx,	,
papežův	papežův	k2eAgMnSc1d1	papežův
synovec	synovec	k1gMnSc1	synovec
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
podporovat	podporovat	k5eAaImF	podporovat
rozsudek	rozsudek	k1gInSc4	rozsudek
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
podepsali	podepsat	k5eAaPmAgMnP	podepsat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
v	v	k7c4	v
den	den	k1gInSc4	den
procesu	proces	k1gInSc2	proces
<g/>
;	;	kIx,	;
nicméně	nicméně	k8xC	nicméně
kardinálové	kardinál	k1gMnPc1	kardinál
Barberini	Barberin	k2eAgMnPc1d1	Barberin
a	a	k8xC	a
Borgia	Borgia	k1gFnSc1	Borgia
byli	být	k5eAaImAgMnP	být
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
na	na	k7c6	na
audienci	audience	k1gFnSc6	audience
u	u	k7c2	u
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
záznamů	záznam	k1gInPc2	záznam
inkvizice	inkvizice	k1gFnSc1	inkvizice
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přítomnost	přítomnost	k1gFnSc4	přítomnost
pouze	pouze	k6eAd1	pouze
sedmi	sedm	k4xCc2	sedm
z	z	k7c2	z
deseti	deset	k4xCc2	deset
kardinálů	kardinál	k1gMnPc2	kardinál
nebyla	být	k5eNaImAgFnS	být
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
zpochybnit	zpochybnit	k5eAaPmF	zpochybnit
závěr	závěr	k1gInSc4	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
Barberini	Barberin	k1gMnPc1	Barberin
protestoval	protestovat	k5eAaBmAgMnS	protestovat
proti	proti	k7c3	proti
rozsudku	rozsudek	k1gInSc3	rozsudek
<g/>
.	.	kIx.	.
</s>
<s>
Hrozba	hrozba	k1gFnSc1	hrozba
mučením	mučení	k1gNnSc7	mučení
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
byl	být	k5eAaImAgInS	být
Galileo	Galilea	k1gFnSc5	Galilea
vystaven	vystavit	k5eAaPmNgInS	vystavit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
církví	církev	k1gFnSc7	církev
již	již	k6eAd1	již
vykonána	vykonán	k2eAgFnSc1d1	vykonána
v	v	k7c6	v
dřívějším	dřívější	k2eAgInSc6d1	dřívější
procesu	proces	k1gInSc6	proces
proti	proti	k7c3	proti
Giordanu	Giordan	k1gMnSc3	Giordan
Brunovi	Bruna	k1gMnSc3	Bruna
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
který	který	k3yIgInSc1	který
shořel	shořet	k5eAaPmAgInS	shořet
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zastával	zastávat	k5eAaImAgMnS	zastávat
panteistické	panteistický	k2eAgFnPc4d1	panteistická
a	a	k8xC	a
dokétistické	dokétistický	k2eAgFnPc4d1	dokétistický
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
byl	být	k5eAaImAgMnS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
žaláři	žalář	k1gInSc3	žalář
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
vysoký	vysoký	k2eAgInSc4d1	vysoký
věk	věk	k1gInSc4	věk
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
kvůli	kvůli	k7c3	kvůli
církevní	církevní	k2eAgFnSc3d1	církevní
politice	politika	k1gFnSc3	politika
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
rozsudek	rozsudek	k1gInSc1	rozsudek
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
domácí	domácí	k2eAgNnSc4d1	domácí
vězení	vězení	k1gNnSc4	vězení
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
vilách	vila	k1gFnPc6	vila
v	v	k7c6	v
Arcetri	Arcetr	k1gFnSc6	Arcetr
a	a	k8xC	a
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bolestivou	bolestivý	k2eAgFnSc4d1	bolestivá
kýlu	kýla	k1gFnSc4	kýla
žádal	žádat	k5eAaImAgMnS	žádat
o	o	k7c4	o
dovolení	dovolení	k1gNnSc4	dovolení
poradit	poradit	k5eAaPmF	poradit
se	se	k3xPyFc4	se
s	s	k7c7	s
lékaři	lékař	k1gMnPc7	lékař
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
odmítnuto	odmítnut	k2eAgNnSc1d1	odmítnuto
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
varován	varovat	k5eAaImNgMnS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobné	podobný	k2eAgFnPc1d1	podobná
žádosti	žádost	k1gFnPc1	žádost
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
uvěznění	uvěznění	k1gNnSc3	uvěznění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
pravidelně	pravidelně	k6eAd1	pravidelně
recitovat	recitovat	k5eAaImF	recitovat
kající	kající	k2eAgInPc4d1	kající
žalmy	žalm	k1gInPc4	žalm
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
odmítat	odmítat	k5eAaImF	odmítat
návštěvy	návštěva	k1gFnPc4	návštěva
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
méně	málo	k6eAd2	málo
kontroverzních	kontroverzní	k2eAgInPc6d1	kontroverzní
výzkumech	výzkum	k1gInPc6	výzkum
a	a	k8xC	a
potrestání	potrestání	k1gNnSc6	potrestání
zákazem	zákaz	k1gInSc7	zákaz
sociálních	sociální	k2eAgInPc2d1	sociální
kontaktů	kontakt	k1gInPc2	kontakt
nebylo	být	k5eNaImAgNnS	být
uplatňováno	uplatňovat	k5eAaImNgNnS	uplatňovat
tak	tak	k6eAd1	tak
přísně	přísně	k6eAd1	přísně
<g/>
.	.	kIx.	.
</s>
<s>
Publikace	publikace	k1gFnPc1	publikace
byly	být	k5eAaImAgFnP	být
další	další	k2eAgInPc1d1	další
věcí	věc	k1gFnSc7	věc
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
Dialogy	dialog	k1gInPc1	dialog
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1634	[number]	k4	1634
zařazeny	zařadit	k5eAaPmNgInP	zařadit
na	na	k7c4	na
Index	index	k1gInSc4	index
librorum	librorum	k1gInSc1	librorum
prohibitorum	prohibitorum	k1gInSc1	prohibitorum
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc1d1	oficiální
seznam	seznam	k1gInSc1	seznam
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
rozsudek	rozsudek	k1gInSc4	rozsudek
vynesený	vynesený	k2eAgInSc4d1	vynesený
proti	proti	k7c3	proti
Galileimu	Galileim	k1gInSc3	Galileim
neuváděl	uvádět	k5eNaImAgMnS	uvádět
další	další	k2eAgFnPc4d1	další
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
Galileo	Galilea	k1gFnSc5	Galilea
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
publikace	publikace	k1gFnPc4	publikace
o	o	k7c6	o
čemkoliv	cokoliv	k3yInSc6	cokoliv
<g/>
,	,	kIx,	,
co	co	k9	co
kdy	kdy	k6eAd1	kdy
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
tiše	tiš	k1gFnPc1	tiš
zakázány	zakázán	k2eAgFnPc1d1	zakázána
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zákaz	zákaz	k1gInSc1	zákaz
byl	být	k5eAaImAgInS	být
striktně	striktně	k6eAd1	striktně
uplatňován	uplatňovat	k5eAaImNgInS	uplatňovat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
německých	německý	k2eAgInPc6d1	německý
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
například	například	k6eAd1	například
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Galileovy	Galileův	k2eAgInPc1d1	Galileův
Dialogy	dialog	k1gInPc1	dialog
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
Indexu	index	k1gInSc2	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
však	však	k9	však
postupně	postupně	k6eAd1	postupně
uvolňovala	uvolňovat	k5eAaImAgFnS	uvolňovat
zákaz	zákaz	k1gInSc4	zákaz
šíření	šíření	k1gNnSc2	šíření
Koperníkovy	Koperníkův	k2eAgFnSc2d1	Koperníkova
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
připustila	připustit	k5eAaPmAgFnS	připustit
i	i	k9	i
její	její	k3xOp3gNnSc4	její
šíření	šíření	k1gNnSc4	šíření
tiskem	tisk	k1gInSc7	tisk
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
vyškrtla	vyškrtnout	k5eAaPmAgFnS	vyškrtnout
Galileův	Galileův	k2eAgInSc4d1	Galileův
spis	spis	k1gInSc4	spis
z	z	k7c2	z
Indexu	index	k1gInSc2	index
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
stále	stále	k6eAd1	stále
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
Galileo	Galilea	k1gFnSc5	Galilea
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1638	[number]	k4	1638
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
domu	dům	k1gInSc2	dům
poblíž	poblíž	k7c2	poblíž
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
slepý	slepý	k2eAgMnSc1d1	slepý
<g/>
,	,	kIx,	,
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
učení	učení	k1gNnSc6	učení
a	a	k8xC	a
psaní	psaní	k1gNnSc6	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
Arcetri	Arcetr	k1gFnSc6	Arcetr
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
359	[number]	k4	359
let	léto	k1gNnPc2	léto
po	po	k7c6	po
Galileově	Galileův	k2eAgInSc6d1	Galileův
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
omluvu	omluva	k1gFnSc4	omluva
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
ruší	rušit	k5eAaImIp3nS	rušit
výnos	výnos	k1gInSc1	výnos
inkvizice	inkvizice	k1gFnSc2	inkvizice
proti	proti	k7c3	proti
Galileovi	Galileus	k1gMnSc3	Galileus
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Galileo	Galilea	k1gFnSc5	Galilea
pociťoval	pociťovat	k5eAaImAgMnS	pociťovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
vědeckých	vědecký	k2eAgInPc6d1	vědecký
výzkumech	výzkum	k1gInPc6	výzkum
přítomnost	přítomnost	k1gFnSc1	přítomnost
Stvořitele	Stvořitel	k1gMnSc2	Stvořitel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
podnítil	podnítit	k5eAaPmAgMnS	podnítit
hloubku	hloubka	k1gFnSc4	hloubka
jeho	jeho	k3xOp3gMnSc2	jeho
ducha	duch	k1gMnSc2	duch
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
povzbuzoval	povzbuzovat	k5eAaImAgMnS	povzbuzovat
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
předesílal	předesílat	k5eAaImAgMnS	předesílat
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgMnS	podporovat
jeho	jeho	k3xOp3gFnSc2	jeho
intuice	intuice	k1gFnSc2	intuice
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
...	...	k?	...
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
,	,	kIx,	,
upřímný	upřímný	k2eAgMnSc1d1	upřímný
věřící	věřící	k1gMnSc1	věřící
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
popisu	popis	k1gInSc2	popis
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
a	a	k8xC	a
biblických	biblický	k2eAgFnPc2d1	biblická
pravd	pravda	k1gFnPc2	pravda
mnohem	mnohem	k6eAd1	mnohem
citlivější	citlivý	k2eAgMnPc1d2	citlivější
než	než	k8xS	než
teologové	teolog	k1gMnPc1	teolog
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
stáli	stát	k5eAaImAgMnP	stát
proti	proti	k7c3	proti
<g />
.	.	kIx.	.
</s>
<s>
němu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Současný	současný	k2eAgInSc1d1	současný
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
vědu	věda	k1gFnSc4	věda
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
dokument	dokument	k1gInSc1	dokument
Druhého	druhý	k4xOgInSc2	druhý
vatikánského	vatikánský	k2eAgInSc2d1	vatikánský
koncilu	koncil	k1gInSc2	koncil
Gaudium	gaudium	k1gNnSc4	gaudium
et	et	k?	et
spes	spesa	k1gFnPc2	spesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Když	když	k8xS	když
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
metodické	metodický	k2eAgNnSc4d1	metodické
bádání	bádání	k1gNnSc4	bádání
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
vědních	vědní	k2eAgInPc6d1	vědní
oborech	obor	k1gInPc6	obor
skutečně	skutečně	k6eAd1	skutečně
vědecky	vědecky	k6eAd1	vědecky
a	a	k8xC	a
podle	podle	k7c2	podle
mravních	mravní	k2eAgFnPc2d1	mravní
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
nikdy	nikdy	k6eAd1	nikdy
ve	v	k7c6	v
skutečném	skutečný	k2eAgInSc6d1	skutečný
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
vírou	víra	k1gFnSc7	víra
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
věci	věc	k1gFnPc4	věc
světské	světský	k2eAgFnPc4d1	světská
i	i	k8xC	i
věci	věc	k1gFnPc1	věc
víry	víra	k1gFnSc2	víra
pocházejí	pocházet	k5eAaImIp3nP	pocházet
od	od	k7c2	od
jednoho	jeden	k4xCgMnSc2	jeden
a	a	k8xC	a
téhož	týž	k3xTgMnSc2	týž
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Některá	některý	k3yIgNnPc1	některý
místa	místo	k1gNnPc1	místo
sporu	spor	k1gInSc2	spor
stále	stále	k6eAd1	stále
čekají	čekat	k5eAaImIp3nP	čekat
na	na	k7c6	na
objasnění	objasnění	k1gNnSc6	objasnění
<g/>
.	.	kIx.	.
</s>
<s>
Nejasností	nejasnost	k1gFnSc7	nejasnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
nepodepsané	podepsaný	k2eNgNnSc1d1	nepodepsané
a	a	k8xC	a
nedatované	datovaný	k2eNgNnSc1d1	nedatované
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgMnSc1d1	spadající
údajně	údajně	k6eAd1	údajně
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
prvního	první	k4xOgInSc2	první
inkvizičního	inkviziční	k2eAgInSc2d1	inkviziční
procesu	proces	k1gInSc2	proces
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1616	[number]	k4	1616
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
Galileovi	Galileus	k1gMnSc3	Galileus
šíření	šíření	k1gNnSc4	šíření
Koperníkovy	Koperníkův	k2eAgFnSc2d1	Koperníkova
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravé	pravý	k2eAgNnSc1d1	pravé
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
podvrh	podvrh	k1gInSc4	podvrh
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokument	dokument	k1gInSc1	dokument
postrádá	postrádat	k5eAaImIp3nS	postrádat
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
datování	datování	k1gNnSc4	datování
i	i	k8xC	i
podpis	podpis	k1gInSc4	podpis
autority	autorita	k1gFnSc2	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
na	na	k7c6	na
oné	onen	k3xDgFnSc6	onen
tajné	tajný	k2eAgFnSc6d1	tajná
schůzce	schůzka	k1gFnSc6	schůzka
během	během	k7c2	během
druhého	druhý	k4xOgInSc2	druhý
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
Galileiho	Galilei	k1gMnSc4	Galilei
přiznání	přiznání	k1gNnSc2	přiznání
<g/>
.	.	kIx.	.
</s>
<s>
Italský	italský	k2eAgMnSc1d1	italský
historik	historik	k1gMnSc1	historik
Pietro	Pietro	k1gNnSc4	Pietro
Redondi	Redond	k1gMnPc1	Redond
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
materiál	materiál	k1gInSc4	materiál
Galileiho	Galilei	k1gMnSc2	Galilei
případu	případ	k1gInSc2	případ
ve	v	k7c6	v
Vatikánském	vatikánský	k2eAgInSc6d1	vatikánský
archívu	archív	k1gInSc6	archív
<g/>
,	,	kIx,	,
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
teorií	teorie	k1gFnSc7	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
totiž	totiž	k9	totiž
spis	spis	k1gInSc1	spis
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vinil	vinit	k5eAaImAgMnS	vinit
Galileiho	Galilei	k1gMnSc4	Galilei
z	z	k7c2	z
hereze	hereze	k1gFnSc2	hereze
proti	proti	k7c3	proti
dogmatu	dogma	k1gNnSc3	dogma
transsubstanciace	transsubstanciace	k1gFnSc2	transsubstanciace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
Il	Il	k1gFnSc2	Il
Saggiatore	Saggiator	k1gMnSc5	Saggiator
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
totiž	totiž	k9	totiž
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
neslučitelná	slučitelný	k2eNgFnSc1d1	neslučitelná
s	s	k7c7	s
učením	učení	k1gNnSc7	učení
o	o	k7c6	o
eucharistii	eucharistie	k1gFnSc6	eucharistie
<g/>
.	.	kIx.	.
</s>
<s>
Italský	italský	k2eAgMnSc1d1	italský
historik	historik	k1gMnSc1	historik
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
inkviziční	inkviziční	k2eAgInSc1d1	inkviziční
proces	proces	k1gInSc1	proces
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1633	[number]	k4	1633
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
rouškou	rouška	k1gFnSc7	rouška
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
pozornost	pozornost	k1gFnSc4	pozornost
od	od	k7c2	od
hlubšího	hluboký	k2eAgInSc2d2	hlubší
problému	problém	k1gInSc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
je	být	k5eAaImIp3nS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
jen	jen	k9	jen
za	za	k7c4	za
neposlušnost	neposlušnost	k1gFnSc4	neposlušnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
obžalování	obžalování	k1gNnSc2	obžalování
z	z	k7c2	z
hereze	hereze	k1gFnSc2	hereze
proti	proti	k7c3	proti
eucharistii	eucharistie	k1gFnSc3	eucharistie
by	by	kYmCp3nS	by
takřka	takřka	k6eAd1	takřka
jistě	jistě	k6eAd1	jistě
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
předmětem	předmět	k1gInSc7	předmět
bádání	bádání	k1gNnSc2	bádání
<g/>
,	,	kIx,	,
faktem	fakt	k1gInSc7	fakt
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
osvětlila	osvětlit	k5eAaPmAgFnS	osvětlit
některé	některý	k3yIgFnPc4	některý
nesrovnalosti	nesrovnalost	k1gFnPc4	nesrovnalost
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
že	že	k8xS	že
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
odvolání	odvolání	k1gNnSc6	odvolání
zvedl	zvednout	k5eAaPmAgMnS	zvednout
z	z	k7c2	z
kolen	koleno	k1gNnPc2	koleno
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
"	"	kIx"	"
<g/>
Přece	přece	k8xC	přece
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Eppur	Eppur	k1gMnSc1	Eppur
si	se	k3xPyFc3	se
muove	muovat	k5eAaPmIp3nS	muovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
pravda	pravda	k1gFnSc1	pravda
<g/>
;	;	kIx,	;
říct	říct	k5eAaPmF	říct
něco	něco	k3yInSc4	něco
takového	takový	k3xDgNnSc2	takový
před	před	k7c4	před
úředníky	úředník	k1gMnPc4	úředník
inkvizice	inkvizice	k1gFnSc2	inkvizice
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
takřka	takřka	k6eAd1	takřka
jistě	jistě	k6eAd1	jistě
rozsudek	rozsudek	k1gInSc4	rozsudek
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc4d1	celý
incident	incident	k1gInSc4	incident
si	se	k3xPyFc3	se
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1757	[number]	k4	1757
novinář	novinář	k1gMnSc1	novinář
Giuseppe	Giusepp	k1gInSc5	Giusepp
Barreti	Barret	k1gMnPc5	Barret
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
také	také	k9	také
falešná	falešný	k2eAgFnSc1d1	falešná
<g/>
.	.	kIx.	.
</s>
<s>
Španělské	španělský	k2eAgNnSc1d1	španělské
plátno	plátno	k1gNnSc1	plátno
<g/>
,	,	kIx,	,
datované	datovaný	k2eAgFnPc1d1	datovaná
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c4	mezi
léta	léto	k1gNnPc4	léto
1643	[number]	k4	1643
a	a	k8xC	a
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
Galilea	Galilea	k1gFnSc1	Galilea
zapisujícího	zapisující	k2eAgInSc2d1	zapisující
tuto	tento	k3xDgFnSc4	tento
frázi	fráze	k1gFnSc4	fráze
na	na	k7c4	na
zeď	zeď	k1gFnSc4	zeď
vězeňské	vězeňský	k2eAgFnSc2d1	vězeňská
cely	cela	k1gFnSc2	cela
<g/>
.	.	kIx.	.
</s>
<s>
Tady	tady	k6eAd1	tady
tedy	tedy	k9	tedy
máme	mít	k5eAaImIp1nP	mít
druhou	druhý	k4xOgFnSc4	druhý
verzi	verze	k1gFnSc4	verze
této	tento	k3xDgFnSc2	tento
historky	historka	k1gFnSc2	historka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
také	také	k9	také
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Galileo	Galilea	k1gFnSc5	Galilea
nebyl	být	k5eNaImAgMnS	být
nikdy	nikdy	k6eAd1	nikdy
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
;	;	kIx,	;
malba	malba	k1gFnSc1	malba
však	však	k9	však
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
stejný	stejný	k2eAgInSc1d1	stejný
příběh	příběh	k1gInSc1	příběh
"	"	kIx"	"
<g/>
Eppur	Eppur	k1gMnSc1	Eppur
si	se	k3xPyFc3	se
muove	muovat	k5eAaPmIp3nS	muovat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
který	který	k3yRgMnSc1	který
koloval	kolovat	k5eAaImAgInS	kolovat
už	už	k6eAd1	už
od	od	k7c2	od
Galileových	Galileův	k2eAgInPc2d1	Galileův
časů	čas	k1gInPc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
verze	verze	k1gFnSc1	verze
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Galileo	Galilea	k1gFnSc5	Galilea
pronesl	pronést	k5eAaPmAgMnS	pronést
tuto	tento	k3xDgFnSc4	tento
větu	věta	k1gFnSc4	věta
před	před	k7c7	před
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Ascaniem	Ascanium	k1gNnSc7	Ascanium
Piccolominim	Piccolominimo	k1gNnPc2	Piccolominimo
ze	z	k7c2	z
Sieny	Siena	k1gFnSc2	Siena
<g/>
,	,	kIx,	,
vzdělaným	vzdělaný	k2eAgMnSc7d1	vzdělaný
mužem	muž	k1gMnSc7	muž
a	a	k8xC	a
sympatizujícím	sympatizující	k2eAgMnSc7d1	sympatizující
hostitelem	hostitel	k1gMnSc7	hostitel
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
měsících	měsíc	k1gInPc6	měsíc
bezprostředně	bezprostředně	k6eAd1	bezprostředně
následujících	následující	k2eAgInPc2d1	následující
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
odsouzení	odsouzení	k1gNnSc6	odsouzení
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
prý	prý	k9	prý
pronesl	pronést	k5eAaPmAgMnS	pronést
tuto	tento	k3xDgFnSc4	tento
poznámku	poznámka	k1gFnSc4	poznámka
před	před	k7c7	před
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nejspíš	nejspíš	k9	nejspíš
napsal	napsat	k5eAaBmAgMnS	napsat
své	svůj	k3xOyFgFnSc3	svůj
rodině	rodina	k1gFnSc3	rodina
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
příhodě	příhoda	k1gFnSc6	příhoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
stálým	stálý	k2eAgNnSc7d1	stálé
převypravováním	převypravování	k1gNnSc7	převypravování
překroucena	překroutit	k5eAaPmNgFnS	překroutit
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
upřímným	upřímný	k2eAgMnSc7d1	upřímný
katolíkem	katolík	k1gMnSc7	katolík
<g/>
,	,	kIx,	,
Galileo	Galilea	k1gFnSc5	Galilea
byl	být	k5eAaImAgInS	být
otcem	otec	k1gMnSc7	otec
tří	tři	k4xCgNnPc2	tři
dětí	dítě	k1gFnPc2	dítě
mimo	mimo	k7c4	mimo
manželství	manželství	k1gNnSc4	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Všechno	všechen	k3xTgNnSc1	všechen
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
děti	dítě	k1gFnPc1	dítě
Galilea	Galilea	k1gFnSc1	Galilea
a	a	k8xC	a
Mariny	Marina	k1gFnPc1	Marina
Gamby	gamba	k1gFnSc2	gamba
<g/>
.	.	kIx.	.
</s>
<s>
Virginia	Virginium	k1gNnPc1	Virginium
(	(	kIx(	(
<g/>
narozená	narozený	k2eAgFnSc1d1	narozená
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
přijala	přijmout	k5eAaPmAgFnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Maria	Mario	k1gMnSc2	Mario
Celeste	Celest	k1gInSc5	Celest
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Galileovo	Galileův	k2eAgNnSc4d1	Galileovo
nejstarší	starý	k2eAgNnSc4d3	nejstarší
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
nejmilovanější	milovaný	k2eAgFnSc1d3	nejmilovanější
<g/>
,	,	kIx,	,
zdědila	zdědit	k5eAaPmAgFnS	zdědit
otcovu	otcův	k2eAgFnSc4d1	otcova
pronikavou	pronikavý	k2eAgFnSc4d1	pronikavá
mysl	mysl	k1gFnSc4	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Livia	Livia	k1gFnSc1	Livia
Vincenzio	Vincenzio	k1gNnSc1	Vincenzio
1610	[number]	k4	1610
Hvězdný	hvězdný	k2eAgMnSc1d1	hvězdný
posel	posel	k1gMnSc1	posel
(	(	kIx(	(
<g/>
Sidereus	Sidereus	k1gMnSc1	Sidereus
Nuncius	nuncius	k1gMnSc1	nuncius
<g/>
)	)	kIx)	)
1615	[number]	k4	1615
Dopis	dopis	k1gInSc1	dopis
velkovévodkyni	velkovévodkyně	k1gFnSc6	velkovévodkyně
Kristině	Kristin	k2eAgFnSc6d1	Kristina
1623	[number]	k4	1623
Prubíř	prubíř	k1gMnSc1	prubíř
(	(	kIx(	(
<g/>
Il	Il	k1gMnSc5	Il
saggiatore	saggiator	k1gMnSc5	saggiator
<g/>
)	)	kIx)	)
1632	[number]	k4	1632
Dialogy	dialog	k1gInPc7	dialog
o	o	k7c6	o
dvou	dva	k4xCgInPc6	dva
největších	veliký	k2eAgInPc6d3	veliký
systémech	systém	k1gInPc6	systém
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Dialogo	Dialogo	k1gMnSc1	Dialogo
sopra	sopra	k1gMnSc1	sopra
i	i	k8xC	i
due	due	k?	due
massimi	massi	k1gFnPc7	massi
sistemi	siste	k1gFnPc7	siste
del	del	k?	del
mondo	mondo	k1gNnSc4	mondo
<g/>
)	)	kIx)	)
1638	[number]	k4	1638
Matematické	matematický	k2eAgFnPc1d1	matematická
rozpravy	rozprava	k1gFnPc1	rozprava
a	a	k8xC	a
pokusy	pokus	k1gInPc1	pokus
(	(	kIx(	(
<g/>
Discorsi	Discorse	k1gFnSc3	Discorse
<g />
.	.	kIx.	.
</s>
<s>
e	e	k0	e
dimostrazioni	dimostrazion	k1gMnPc1	dimostrazion
matematiche	matematiche	k1gNnSc1	matematiche
<g/>
,	,	kIx,	,
intorno	intorno	k1gNnSc1	intorno
à	à	k?	à
due	due	k?	due
nuove	nuovat	k5eAaPmIp3nS	nuovat
scienze	scienze	k6eAd1	scienze
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
<g/>
)	)	kIx)	)
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gFnPc1	Galile
–	–	k?	–
opera	opera	k1gFnSc1	opera
od	od	k7c2	od
Philipa	Philip	k1gMnSc2	Philip
Glasse	Glass	k1gMnSc2	Glass
Galileo	Galilea	k1gFnSc5	Galilea
–	–	k?	–
hra	hra	k1gFnSc1	hra
od	od	k7c2	od
Bertolta	Bertolt	k1gMnSc2	Bertolt
Brechta	Brecht	k1gMnSc2	Brecht
Galileo	Galilea	k1gFnSc5	Galilea
–	–	k?	–
muzikál	muzikál	k1gInSc1	muzikál
Janka	Janek	k1gMnSc2	Janek
Ledeckého	Ledecký	k2eAgMnSc2d1	Ledecký
Galileo	Galilea	k1gFnSc5	Galilea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dream	Dream	k1gInSc1	Dream
-	-	kIx~	-
kniha	kniha	k1gFnSc1	kniha
od	od	k7c2	od
Kima	Kim	k1gInSc2	Kim
Stanleyho	Stanley	k1gMnSc2	Stanley
Robinsona	Robinson	k1gMnSc2	Robinson
Mise	mise	k1gFnSc1	mise
Galileo	Galilea	k1gFnSc5	Galilea
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
Jupiter	Jupiter	k1gInSc4	Jupiter
Galileovské	Galileovské	k2eAgInSc2d1	Galileovské
měsíce	měsíc	k1gInSc2	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
Galileo	Galilea	k1gFnSc5	Galilea
Regio	Regio	k1gNnSc1	Regio
(	(	kIx(	(
<g/>
Galileova	Galileův	k2eAgFnSc1d1	Galileova
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
na	na	k7c6	na
Ganymedu	Ganymed	k1gMnSc6	Ganymed
Kráter	kráter	k1gInSc4	kráter
Galilaei	Galilaei	k1gNnSc2	Galilaei
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
Rima	Rim	k1gInSc2	Rim
Galilaei	Galilae	k1gFnSc2	Galilae
brázda	brázda	k1gFnSc1	brázda
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
poblíže	poblíže	k7c2	poblíže
kráteru	kráter	k1gInSc2	kráter
Galilaei	Galilae	k1gFnSc2	Galilae
Kráter	kráter	k1gInSc1	kráter
Galilaei	Galilaee	k1gFnSc4	Galilaee
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
Planetka	planetka	k1gFnSc1	planetka
697	[number]	k4	697
Galilea	Galilea	k1gFnSc1	Galilea
(	(	kIx(	(
<g/>
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
300	[number]	k4	300
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
objevu	objev	k1gInSc2	objev
galileovských	galileovský	k2eAgInPc2d1	galileovský
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
Evropský	evropský	k2eAgInSc1d1	evropský
satelitní	satelitní	k2eAgInSc1d1	satelitní
navigační	navigační	k2eAgInSc1d1	navigační
systém	systém	k1gInSc1	systém
Galileo	Galilea	k1gFnSc5	Galilea
</s>
