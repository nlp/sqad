<s>
Jaký	jaký	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
název	název	k1gInSc1
ekonomického	ekonomický	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
jsou	být	k5eAaImIp3nP
výrobní	výrobní	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
v	v	k7c6
soukromém	soukromý	k2eAgNnSc6d1
vlastnictví	vlastnictví	k1gNnSc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
provozovány	provozovat	k5eAaImNgFnP
v	v	k7c6
prostředí	prostředí	k1gNnSc6
tržní	tržní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
za	za	k7c7
účelem	účel	k1gInSc7
dosažení	dosažení	k1gNnSc2
zisku	zisk	k1gInSc2
<g/>
?	?	kIx.
</s>