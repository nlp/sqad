<s>
Kapitalismus	kapitalismus	k1gInSc1
je	být	k5eAaImIp3nS
ekonomický	ekonomický	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
jsou	být	k5eAaImIp3nP
výrobní	výrobní	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
v	v	k7c6
soukromém	soukromý	k2eAgNnSc6d1
vlastnictví	vlastnictví	k1gNnSc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
provozovány	provozovat	k5eAaImNgFnP
v	v	k7c4
prostředí	prostředí	k1gNnSc4
tržní	tržní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
za	za	k7c7
účelem	účel	k1gInSc7
dosažení	dosažení	k1gNnSc1
zisku	zisk	k1gInSc2
<g/>
.	.	kIx.
</s>