<s>
Kapitalismus	kapitalismus	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Kapitalismus	kapitalismus	k1gInSc1
je	být	k5eAaImIp3nS
ekonomický	ekonomický	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
jsou	být	k5eAaImIp3nP
výrobní	výrobní	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
v	v	k7c6
soukromém	soukromý	k2eAgNnSc6d1
vlastnictví	vlastnictví	k1gNnSc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
provozovány	provozovat	k5eAaImNgFnP
v	v	k7c4
prostředí	prostředí	k1gNnSc4
tržní	tržní	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
za	za	k7c7
účelem	účel	k1gInSc7
dosažení	dosažení	k1gNnSc1
zisku	zisk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akumulací	akumulace	k1gFnPc2
(	(	kIx(
<g/>
části	část	k1gFnSc2
<g/>
)	)	kIx)
zisku	zisk	k1gInSc2
vzniká	vznikat	k5eAaImIp3nS
kapitál	kapitál	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
znovu	znovu	k6eAd1
investován	investovat	k5eAaBmNgMnS
do	do	k7c2
výroby	výroba	k1gFnSc2
za	za	k7c7
účelem	účel	k1gInSc7
navýšení	navýšení	k1gNnSc2
zisku	zisk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
více	hodně	k6eAd2
pojetí	pojetí	k1gNnSc1
kapitalismu	kapitalismus	k1gInSc2
závisející	závisející	k2eAgNnSc1d1
na	na	k7c6
historické	historický	k2eAgFnSc6d1
době	doba	k1gFnSc6
a	a	k8xC
osobě	osoba	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
jej	on	k3xPp3gMnSc4
používá	používat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
také	také	k6eAd1
zpravidla	zpravidla	k6eAd1
nezahrnuje	zahrnovat	k5eNaImIp3nS
jen	jen	k9
ekonomický	ekonomický	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
společenský	společenský	k2eAgInSc4d1
a	a	k8xC
politický	politický	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
opak	opak	k1gInSc1
socialismu	socialismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Stát	stát	k1gInSc1
si	se	k3xPyFc3
většinou	většinou	k6eAd1
ponechává	ponechávat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
takový	takový	k3xDgInSc4
ekonomický	ekonomický	k2eAgInSc4d1
systém	systém	k1gInSc4
regulovat	regulovat	k5eAaImF
zákony	zákon	k1gInPc4
<g/>
,	,	kIx,
např.	např.	kA
zákoníkem	zákoník	k1gInSc7
práce	práce	k1gFnSc2
či	či	k8xC
obchodním	obchodní	k2eAgInSc7d1
zákoníkem	zákoník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anarchokapitalismus	Anarchokapitalismus	k1gInSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
název	název	k1gInSc4
pro	pro	k7c4
systém	systém	k1gInSc4
kapitalismu	kapitalismus	k1gInSc2
bez	bez	k7c2
přítomnosti	přítomnost	k1gFnSc2
jakékoli	jakýkoli	k3yIgFnSc2
státní	státní	k2eAgFnSc2d1
regulace	regulace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomiky	ekonomika	k1gFnPc1
takových	takový	k3xDgInPc2
států	stát	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
stát	stát	k1gInSc1
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
ekonomiky	ekonomika	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
smíšené	smíšený	k2eAgInPc1d1
<g/>
;	;	kIx,
tržní	tržní	k2eAgInPc1d1
principy	princip	k1gInPc1
fungují	fungovat	k5eAaImIp3nP
v	v	k7c6
určitých	určitý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
jiných	jiná	k1gFnPc6
však	však	k9
vládne	vládnout	k5eAaImIp3nS
monopol	monopol	k1gInSc1
či	či	k8xC
monopson	monopson	k1gInSc1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
kapitalismu	kapitalismus	k1gInSc2
</s>
<s>
Americká	americký	k2eAgFnSc1d1
burza	burza	k1gFnSc1
NYSE	NYSE	kA
<g/>
,	,	kIx,
rok	rok	k1gInSc4
1963	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kapitalismu	kapitalismus	k1gInSc6
hrají	hrát	k5eAaImIp3nP
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
nabídka	nabídka	k1gFnSc1
a	a	k8xC
poptávka	poptávka	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
které	který	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
se	se	k3xPyFc4
vyvíjí	vyvíjet	k5eAaImIp3nS
běh	běh	k1gInSc1
celého	celý	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
</s>
<s>
Kapitalistické	kapitalistický	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
v	v	k7c6
hospodářství	hospodářství	k1gNnSc6
se	se	k3xPyFc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
objevují	objevovat	k5eAaImIp3nP
již	již	k6eAd1
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
začaly	začít	k5eAaPmAgFnP
pomalu	pomalu	k6eAd1
vytlačovat	vytlačovat	k5eAaImF
vztahy	vztah	k1gInPc4
feudální	feudální	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
s	s	k7c7
rozvojem	rozvoj	k1gInSc7
průmyslové	průmyslový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
však	však	k9
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
rozšíření	rozšíření	k1gNnSc3
kapitalismu	kapitalismus	k1gInSc2
do	do	k7c2
většiny	většina	k1gFnSc2
světa	svět	k1gInSc2
a	a	k8xC
k	k	k7c3
likvidaci	likvidace	k1gFnSc3
původních	původní	k2eAgInPc2d1
hospodářských	hospodářský	k2eAgInPc2d1
a	a	k8xC
společenských	společenský	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
rozvoji	rozvoj	k1gInSc3
dochází	docházet	k5eAaImIp3nS
převážně	převážně	k6eAd1
v	v	k7c6
oblastech	oblast	k1gFnPc6
s	s	k7c7
vyšší	vysoký	k2eAgFnSc7d2
gramotností	gramotnost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Definice	definice	k1gFnSc1
</s>
<s>
Will	Will	k1gInSc1
Hutton	Hutton	k1gInSc1
a	a	k8xC
Anthony	Anthona	k1gFnPc1
Giddens	Giddensa	k1gFnPc2
identifikují	identifikovat	k5eAaBmIp3nP
tři	tři	k4xCgFnPc4
základní	základní	k2eAgFnPc4d1
charakteristiky	charakteristika	k1gFnPc4
kapitalismu	kapitalismus	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Systém	systém	k1gInSc1
soukromého	soukromý	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
majetku	majetek	k1gInSc2
</s>
<s>
Hospodářskou	hospodářský	k2eAgFnSc4d1
činnost	činnost	k1gFnSc4
řídí	řídit	k5eAaImIp3nS
cenové	cenový	k2eAgInPc4d1
signály	signál	k1gInPc4
trhu	trh	k1gInSc2
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS
motivaci	motivace	k1gFnSc4
pro	pro	k7c4
činnost	činnost	k1gFnSc4
vycházející	vycházející	k2eAgFnSc4d1
z	z	k7c2
hledání	hledání	k1gNnSc2
zisku	zisk	k1gInSc2
a	a	k8xC
na	na	k7c6
této	tento	k3xDgFnSc6
motivaci	motivace	k1gFnSc6
je	být	k5eAaImIp3nS
závislý	závislý	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
definuje	definovat	k5eAaBmIp3nS
kapitalismus	kapitalismus	k1gInSc4
jako	jako	k8xC,k8xS
výrobní	výrobní	k2eAgInSc4d1
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
zakládá	zakládat	k5eAaImIp3nS
na	na	k7c4
námezdní	námezdní	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
termín	termín	k1gInSc1
kapitalismus	kapitalismus	k1gInSc4
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
pejorativním	pejorativní	k2eAgNnSc7d1
označením	označení	k1gNnSc7
nedostatků	nedostatek	k1gInPc2
západního	západní	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
používán	používat	k5eAaImNgInS
byl	být	k5eAaImAgInS
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
míře	míra	k1gFnSc6
především	především	k6eAd1
levicově	levicově	k6eAd1
smýšlejícími	smýšlející	k2eAgFnPc7d1
silami	síla	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
začal	začít	k5eAaPmAgInS
představovat	představovat	k5eAaImF
ale	ale	k8xC
i	i	k9
symbol	symbol	k1gInSc4
západu	západ	k1gInSc2
<g/>
,	,	kIx,
protiklad	protiklad	k1gInSc1
tehdejšího	tehdejší	k2eAgInSc2d1
východního	východní	k2eAgInSc2d1
socialismu	socialismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kritika	kritika	k1gFnSc1
kapitalismu	kapitalismus	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
alternativy	alternativa	k1gFnSc2
</s>
<s>
Děti	dítě	k1gFnPc1
pracující	pracující	k2eAgFnPc1d1
v	v	k7c6
uhelném	uhelný	k2eAgInSc6d1
dole	dol	k1gInSc6
v	v	k7c6
USA	USA	kA
v	v	k7c6
období	období	k1gNnSc6
neregulovaného	regulovaný	k2eNgInSc2d1
kapitalismu	kapitalismus	k1gInSc2
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Kapitalismus	kapitalismus	k1gInSc1
je	být	k5eAaImIp3nS
kritizován	kritizovat	k5eAaImNgInS
kvůli	kvůli	k7c3
údajným	údajný	k2eAgInPc3d1
bezcitným	bezcitný	k2eAgInPc3d1
bojům	boj	k1gInPc3
<g/>
,	,	kIx,
válkám	válka	k1gFnPc3
o	o	k7c4
peníze	peníz	k1gInPc4
<g/>
,	,	kIx,
ropu	ropa	k1gFnSc4
<g/>
,	,	kIx,
pracovní	pracovní	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
odpůrci	odpůrce	k1gMnPc1
této	tento	k3xDgFnSc2
politické	politický	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kapitalismus	kapitalismus	k1gInSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
namísto	namísto	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
společně	společně	k6eAd1
něco	něco	k3yInSc4
užitečného	užitečný	k2eAgNnSc2d1
vytvářeli	vytvářet	k5eAaImAgMnP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
posílání	posílání	k1gNnSc4
do	do	k7c2
nerovné	rovný	k2eNgFnSc2d1
soutěže	soutěž	k1gFnSc2
při	při	k7c6
jejich	jejich	k3xOp3gNnSc6
získávání	získávání	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
vítězí	vítězit	k5eAaImIp3nS
jen	jen	k9
hrstka	hrstka	k1gFnSc1
jedinců	jedinec	k1gMnPc2
na	na	k7c4
úkor	úkor	k1gInSc4
všech	všecek	k3xTgMnPc2
ostatních	ostatní	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tihle	tenhle	k3xDgMnPc1
jedinci	jedinec	k1gMnPc1
postupně	postupně	k6eAd1
ovládají	ovládat	k5eAaImIp3nP
úplně	úplně	k6eAd1
všechno	všechen	k3xTgNnSc4
a	a	k8xC
v	v	k7c6
zájmu	zájem	k1gInSc6
udržení	udržení	k1gNnSc1
si	se	k3xPyFc3
svých	svůj	k3xOyFgNnPc2
privilegií	privilegium	k1gNnPc2
a	a	k8xC
zisků	zisk	k1gInPc2
<g/>
,	,	kIx,
manipulují	manipulovat	k5eAaImIp3nP
veřejným	veřejný	k2eAgNnSc7d1
míněním	mínění	k1gNnSc7
<g/>
,	,	kIx,
omezují	omezovat	k5eAaImIp3nP
lidské	lidský	k2eAgFnPc4d1
svobody	svoboda	k1gFnPc4
<g/>
,	,	kIx,
bezcitně	bezcitně	k6eAd1
vykořisťují	vykořisťovat	k5eAaImIp3nP
pracující	pracující	k2eAgMnPc1d1
<g/>
,	,	kIx,
rozpoutávají	rozpoutávat	k5eAaImIp3nP
války	válka	k1gFnSc2
nebo	nebo	k8xC
bezohledně	bezohledně	k6eAd1
drancují	drancovat	k5eAaImIp3nP
planetu	planeta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
alternativám	alternativa	k1gFnPc3
systémů	systém	k1gInPc2
společenského	společenský	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
a	a	k8xC
reformám	reforma	k1gFnPc3
kapitalismu	kapitalismus	k1gInSc2
patří	patřit	k5eAaImIp3nS
např.	např.	kA
samospráva	samospráva	k1gFnSc1
pracujících	pracující	k1gMnPc2
participativní	participativní	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
<g/>
,	,	kIx,
základní	základní	k2eAgInSc1d1
nepodmíněný	podmíněný	k2eNgInSc1d1
příjem	příjem	k1gInSc1
<g/>
,	,	kIx,
komunismus	komunismus	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
ekonomie	ekonomie	k1gFnSc1
obnovitelných	obnovitelný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Zastánci	zastánce	k1gMnPc1
úplného	úplný	k2eAgInSc2d1
volného	volný	k2eAgInSc2d1
trhu	trh	k1gInSc2
a	a	k8xC
pravicoví	pravicový	k2eAgMnPc1d1
libertariáni	libertarián	k1gMnPc1
kritizují	kritizovat	k5eAaImIp3nP
systém	systém	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
dle	dle	k7c2
nich	on	k3xPp3gFnPc2
zvýhodňovány	zvýhodňovat	k5eAaImNgFnP
některé	některý	k3yIgFnPc1
banky	banka	k1gFnPc1
a	a	k8xC
korporace	korporace	k1gFnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
podle	podle	k7c2
libertariánů	libertarián	k1gMnPc2
nemá	mít	k5eNaImIp3nS
s	s	k7c7
principy	princip	k1gInPc7
volného	volný	k2eAgInSc2d1
trhu	trh	k1gInSc2
nic	nic	k3yNnSc1
společného	společný	k2eAgNnSc2d1
a	a	k8xC
považují	považovat	k5eAaImIp3nP
ho	on	k3xPp3gMnSc4
za	za	k7c4
formu	forma	k1gFnSc4
socialismu	socialismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kapitál	kapitál	k1gInSc1
je	být	k5eAaImIp3nS
navíc	navíc	k6eAd1
dědičný	dědičný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gatsbyho	Gatsby	k1gMnSc2
křivka	křivka	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
sociální	sociální	k2eAgFnSc1d1
nerovnost	nerovnost	k1gFnSc1
(	(	kIx(
<g/>
Giniho	Gini	k1gMnSc2
koeficient	koeficient	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
často	často	k6eAd1
nadále	nadále	k6eAd1
děděná	děděný	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xC,k8xS
za	za	k7c2
feudalismu	feudalismus	k1gInSc2
a	a	k8xC
společnost	společnost	k1gFnSc1
nenabízí	nabízet	k5eNaImIp3nS
rovné	rovný	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Marxisté	marxista	k1gMnPc1
považují	považovat	k5eAaImIp3nP
kapitalismus	kapitalismus	k1gInSc4
za	za	k7c4
poslední	poslední	k2eAgNnSc4d1
období	období	k1gNnSc4
nerovné	rovný	k2eNgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
podle	podle	k7c2
marxismu	marxismus	k1gInSc2
vykořisťování	vykořisťování	k1gNnSc2
a	a	k8xC
odcizení	odcizení	k1gNnSc2
člověka	člověk	k1gMnSc2
<g/>
:	:	kIx,
</s>
<s>
Dělník	dělník	k1gMnSc1
vyrobí	vyrobit	k5eAaPmIp3nS
produkt	produkt	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
tvoří	tvořit	k5eAaImIp3nP
nadhodnotu	nadhodnota	k1gFnSc4
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
výrobek	výrobek	k1gInSc1
neslouží	sloužit	k5eNaImIp3nS
ani	ani	k8xC
jemu	on	k3xPp3gMnSc3
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
ani	ani	k8xC
však	však	k9
se	se	k3xPyFc4
nestává	stávat	k5eNaImIp3nS
jeho	jeho	k3xOp3gInSc7
vlastním	vlastní	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
směny	směna	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
od	od	k7c2
tohoto	tento	k3xDgInSc2
konečného	konečný	k2eAgInSc2d1
výrobku	výrobek	k1gInSc2
odcizen	odcizen	k2eAgMnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
nepodílí	podílet	k5eNaImIp3nS
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
směně	směna	k1gFnSc6
a	a	k8xC
nemá	mít	k5eNaImIp3nS
z	z	k7c2
něj	on	k3xPp3gMnSc2
tedy	tedy	k9
spravedlivý	spravedlivý	k2eAgInSc1d1
výnos	výnos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrobek	výrobek	k1gInSc1
patří	patřit	k5eAaImIp3nS
kapitalistovi	kapitalista	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
bohatne	bohatnout	k5eAaImIp3nS
z	z	k7c2
práce	práce	k1gFnSc2
jiných	jiný	k2eAgInPc2d1
<g/>
,	,	kIx,
odlidštěných	odlidštěný	k2eAgInPc2d1
<g/>
,	,	kIx,
od	od	k7c2
výsledků	výsledek	k1gInPc2
své	svůj	k3xOyFgFnSc2
práce	práce	k1gFnSc2
oddělených	oddělený	k2eAgInPc2d1
<g/>
,	,	kIx,
totiž	totiž	k9
samotných	samotný	k2eAgMnPc2d1
dělníků	dělník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
roste	růst	k5eAaImIp3nS
i	i	k9
zisk	zisk	k1gInSc4
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
nikoli	nikoli	k9
dělníka	dělník	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
tvorbě	tvorba	k1gFnSc6
výrobku	výrobek	k1gInSc2
účast	účast	k1gFnSc1
svou	svůj	k3xOyFgFnSc7
prací	práce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
odtržen	odtrhnout	k5eAaPmNgMnS
od	od	k7c2
své	svůj	k3xOyFgFnSc2
podstaty	podstata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
pracovní	pracovní	k2eAgFnSc7d1
silou	síla	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
představuje	představovat	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
pro	pro	k7c4
kapitalistu	kapitalista	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Leninismus	leninismus	k1gInSc1
přidává	přidávat	k5eAaImIp3nS
pojem	pojem	k1gInSc1
imperialismus	imperialismus	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yQgFnPc3,k3yIgFnPc3,k3yRgFnPc3
Lenin	Lenin	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
označil	označit	k5eAaPmAgMnS
soudobou	soudobý	k2eAgFnSc4d1
<g/>
,	,	kIx,
dle	dle	k7c2
něj	on	k3xPp3gInSc2
poslední	poslední	k2eAgFnSc6d1
fázi	fáze	k1gFnSc3
vývoje	vývoj	k1gInSc2
kapitalismu	kapitalismus	k1gInSc2
<g/>
,	,	kIx,
charakterizovanou	charakterizovaný	k2eAgFnSc7d1
nadvládou	nadvláda	k1gFnSc7
monopolů	monopol	k1gInPc2
a	a	k8xC
dělením	dělení	k1gNnSc7
světa	svět	k1gInSc2
mezi	mezi	k7c4
světové	světový	k2eAgFnPc4d1
mocnosti	mocnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Současně	současně	k6eAd1
nejvýznamnějším	významný	k2eAgInSc7d3
argumentem	argument	k1gInSc7
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kapitalismus	kapitalismus	k1gInSc1
se	se	k3xPyFc4
vylučuje	vylučovat	k5eAaImIp3nS
s	s	k7c7
udržitelným	udržitelný	k2eAgInSc7d1
rozvojem	rozvoj	k1gInSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
maximální	maximální	k2eAgFnSc1d1
produkce	produkce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
maximálnímu	maximální	k2eAgInSc3d1
zisku	zisk	k1gInSc3
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
zákonitě	zákonitě	k6eAd1
fungovat	fungovat	k5eAaImF
na	na	k7c4
úkor	úkor	k1gInSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
i	i	k8xC
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
financována	financovat	k5eAaBmNgFnS
sociální	sociální	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
(	(	kIx(
<g/>
př	př	kA
<g/>
.	.	kIx.
mateřská	mateřský	k2eAgFnSc1d1
<g/>
,	,	kIx,
důchody	důchod	k1gInPc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
také	také	k9
vznikají	vznikat	k5eAaImIp3nP
různé	různý	k2eAgInPc4d1
apokalyptické	apokalyptický	k2eAgInPc4d1
scénáře	scénář	k1gInPc4
vývoje	vývoj	k1gInSc2
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
nezdá	zdát	k5eNaImIp3nS,k5eNaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
kapitalismus	kapitalismus	k1gInSc4
jen	jen	k6eAd1
tak	tak	k6eAd1
zrušit	zrušit	k5eAaPmF
a	a	k8xC
nahradit	nahradit	k5eAaPmF
ho	on	k3xPp3gMnSc4
jiným	jiný	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
by	by	kYmCp3nS
dával	dávat	k5eAaImAgInS
stejné	stejný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
prostoru	prostor	k1gInSc2
ve	v	k7c6
všech	všecek	k3xTgInPc6
třech	tři	k4xCgInPc6
pilířích	pilíř	k1gInPc6
udržitelného	udržitelný	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
(	(	kIx(
<g/>
ekonomickém	ekonomický	k2eAgNnSc6d1
<g/>
,	,	kIx,
environmentálním	environmentální	k2eAgNnSc6d1
a	a	k8xC
sociálním	sociální	k2eAgNnSc6d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Proti	proti	k7c3
kapitalismu	kapitalismus	k1gInSc3
také	také	k9
vystupují	vystupovat	k5eAaImIp3nP
ekonomičtí	ekonomický	k2eAgMnPc1d1
disidenti	disident	k1gMnPc1
(	(	kIx(
<g/>
Molly	Molla	k1gMnSc2
Scott	Scott	k1gMnSc1
Cato	Cato	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
Česku	Česko	k1gNnSc6
Naďa	Naďa	k1gFnSc1
Johanisová	Johanisový	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přínosy	přínos	k1gInPc1
a	a	k8xC
obhajoba	obhajoba	k1gFnSc1
kapitalismu	kapitalismus	k1gInSc2
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1
ekonomická	ekonomický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
</s>
<s>
Světový	světový	k2eAgInSc1d1
růst	růst	k1gInSc1
HDP	HDP	kA
ukazuje	ukazovat	k5eAaImIp3nS
exponenciální	exponenciální	k2eAgInSc4d1
růst	růst	k1gInSc4
od	od	k7c2
začátku	začátek	k1gInSc2
průmyslové	průmyslový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kapitalismus	kapitalismus	k1gInSc1
a	a	k8xC
ekonomika	ekonomika	k1gFnSc1
Čínské	čínský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Dle	dle	k7c2
ekonomů	ekonom	k1gMnPc2
rakouské	rakouský	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
Ludwig	Ludwig	k1gInSc1
von	von	k1gInSc1
Mises	Mises	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kapitalismus	kapitalismus	k1gInSc4
systém	systém	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgNnSc6
je	být	k5eAaImIp3nS
díky	díky	k7c3
volnému	volný	k2eAgInSc3d1
trhu	trh	k1gInSc3
<g/>
,	,	kIx,
osobnímu	osobní	k2eAgNnSc3d1
vlastnictví	vlastnictví	k1gNnSc3
a	a	k8xC
dobrovolné	dobrovolný	k2eAgFnSc3d1
směně	směna	k1gFnSc3
možné	možný	k2eAgFnSc3d1
dosáhnout	dosáhnout	k5eAaPmF
maximálního	maximální	k2eAgNnSc2d1
bohatství	bohatství	k1gNnSc2
a	a	k8xC
naplnění	naplnění	k1gNnSc2
lidských	lidský	k2eAgFnPc2d1
potřeb	potřeba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
díky	díky	k7c3
soukromému	soukromý	k2eAgNnSc3d1
vlastnictví	vlastnictví	k1gNnSc3
výrobních	výrobní	k2eAgInPc2d1
statků	statek	k1gInPc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
motivaci	motivace	k1gFnSc3
spořit	spořit	k5eAaImF
a	a	k8xC
tím	ten	k3xDgNnSc7
akumulovat	akumulovat	k5eAaBmF
bohatství	bohatství	k1gNnSc4
a	a	k8xC
zvyšovat	zvyšovat	k5eAaImF
produktivitu	produktivita	k1gFnSc4
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1
růst	růst	k1gInSc1
</s>
<s>
S	s	k7c7
nástupem	nástup	k1gInSc7
kapitalismu	kapitalismus	k1gInSc2
je	být	k5eAaImIp3nS
spjato	spjat	k2eAgNnSc1d1
i	i	k8xC
ohromné	ohromný	k2eAgNnSc1d1
zrychlení	zrychlení	k1gNnSc1
bohatnutí	bohatnutí	k1gNnSc2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
popsal	popsat	k5eAaPmAgMnS
již	již	k6eAd1
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
díle	díl	k1gInSc6
Bohatství	bohatství	k1gNnSc2
národů	národ	k1gInPc2
<g/>
,	,	kIx,
společnosti	společnost	k1gFnPc1
bohatnou	bohatnout	k5eAaImIp3nP
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
jim	on	k3xPp3gInPc3
umožněna	umožnit	k5eAaPmNgFnS
dobrovolná	dobrovolný	k2eAgFnSc1d1
směna	směna	k1gFnSc1
a	a	k8xC
vlastnictví	vlastnictví	k1gNnSc1
kapitálu	kapitál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takové	takový	k3xDgFnSc6
situaci	situace	k1gFnSc6
se	se	k3xPyFc4
i	i	k9
zdánlivě	zdánlivě	k6eAd1
špatné	špatný	k2eAgFnPc4d1
lidské	lidský	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
např.	např.	kA
chamtivost	chamtivost	k1gFnSc4
<g/>
,	,	kIx,
projeví	projevit	k5eAaPmIp3nS
snahou	snaha	k1gFnSc7
co	co	k9
nejlépe	dobře	k6eAd3
uspokojit	uspokojit	k5eAaPmF
zákazníka	zákazník	k1gMnSc4
<g/>
,	,	kIx,
a	a	k8xC
ve	v	k7c6
výsledku	výsledek	k1gInSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
obohacení	obohacení	k1gNnSc3
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
transakce	transakce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kapitalismus	kapitalismus	k1gInSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
K	k	k7c3
vzniku	vznik	k1gInSc3
kapitalismu	kapitalismus	k1gInSc2
se	se	k3xPyFc4
váže	vázat	k5eAaImIp3nS
i	i	k9
kupónová	kupónový	k2eAgFnSc1d1
privatizace	privatizace	k1gFnSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
a	a	k8xC
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
symbolický	symbolický	k2eAgInSc4d1
poplatek	poplatek	k1gInSc4
se	se	k3xPyFc4
občan	občan	k1gMnSc1
stal	stát	k5eAaPmAgMnS
akcionářem	akcionář	k1gMnSc7
v	v	k7c6
podniku	podnik	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
díky	díky	k7c3
kupónové	kupónový	k2eAgFnSc3d1
privatizaci	privatizace	k1gFnSc3
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
častému	častý	k2eAgNnSc3d1
tunelování	tunelování	k1gNnSc3
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
hlavní	hlavní	k2eAgMnPc4d1
aktéry	aktér	k1gMnPc4
této	tento	k3xDgFnSc2
privatizace	privatizace	k1gFnSc2
jsou	být	k5eAaImIp3nP
považováni	považován	k2eAgMnPc1d1
<g/>
:	:	kIx,
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Ježek	Ježek	k1gMnSc1
a	a	k8xC
Dušan	Dušan	k1gMnSc1
Tříska	Tříska	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
privatizaci	privatizace	k1gFnSc3
vzniklo	vzniknout	k5eAaPmAgNnS
mnoho	mnoho	k4c1
politických	politický	k2eAgFnPc2d1
kauz	kauza	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
Kauza	kauza	k1gFnSc1
harvardských	harvardský	k2eAgInPc2d1
fondů	fond	k1gInPc2
<g/>
,	,	kIx,
Kauza	kauza	k1gFnSc1
bytů	byt	k1gInPc2
OKD	OKD	kA
apod.	apod.	kA
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Znak	znak	k1gInSc1
dolaru	dolar	k1gInSc2
je	být	k5eAaImIp3nS
typický	typický	k2eAgInSc1d1
symbol	symbol	k1gInSc1
kapitalismu	kapitalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Citát	citát	k1gInSc1
</s>
<s>
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
ta	ten	k3xDgFnSc1
naše	náš	k3xOp1gFnSc1
svoboda	svoboda	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
taková	takový	k3xDgFnSc1
byla	být	k5eAaImAgFnS
vždycky	vždycky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fatální	fatální	k2eAgInSc1d1
na	na	k7c6
tom	ten	k3xDgNnSc6
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jsme	být	k5eAaImIp1nP
si	se	k3xPyFc3
opravdu	opravdu	k6eAd1
mysleli	myslet	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jsme	být	k5eAaImIp1nP
svobodní	svobodný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
máme	mít	k5eAaImIp1nP
jenom	jenom	k9
svobodu	svoboda	k1gFnSc4
pohybu	pohyb	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
vázána	vázat	k5eAaImNgFnS
na	na	k7c4
výdělek	výdělek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
—	—	k?
<g/>
Max	max	kA
von	von	k1gInSc1
der	drát	k5eAaImRp2nS
Grün	Grün	k1gNnSc1
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://cliodynamics.ru/index.php?option=com_content&	http://cliodynamics.ru/index.php?option=com_content&	k?
Archivováno	archivován	k2eAgNnSc1d1
17	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
-	-	kIx~
Literacy	Literac	k1gMnPc4
and	and	k?
the	the	k?
Spirit	Spirit	k1gInSc1
of	of	k?
Capitalism	Capitalism	k1gInSc1
<g/>
↑	↑	k?
On	on	k3xPp3gMnSc1
the	the	k?
edge	edge	k1gNnSc1
:	:	kIx,
living	living	k1gInSc1
with	witha	k1gFnPc2
global	globat	k5eAaImAgInS
capitalism	capitalism	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Will	Willa	k1gFnPc2
Hutton	Hutton	k1gInSc1
<g/>
,	,	kIx,
Anthony	Anthon	k1gInPc1
Giddens	Giddensa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Jonathan	Jonathan	k1gMnSc1
Cape	capat	k5eAaImIp3nS
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
224	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5937	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
ZNOJ	znoj	k1gFnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marxismus	marxismus	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
revize	revize	k1gFnSc1
<g/>
:	:	kIx,
historicko-filozofická	historicko-filozofický	k2eAgFnSc1d1
reminiscence	reminiscence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnPc2
<g/>
:	:	kIx,
Nezapomenuté	zapomenutý	k2eNgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
k	k	k7c3
70	#num#	k4
<g/>
.	.	kIx.
narozeninám	narozeniny	k1gFnPc3
Františka	František	k1gMnSc4
Svátka	Svátek	k1gMnSc2
<g/>
..	..	k?
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Doplněk	doplněk	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7239	#num#	k4
<g/>
-	-	kIx~
<g/>
208	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
60	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://www.ceskatelevize.cz/ct24/archiv/1525542-chudi-versus-bohati-majetek-je-o-odpovednosti-rika-cesky-miliardar	http://www.ceskatelevize.cz/ct24/archiv/1525542-chudi-versus-bohati-majetek-je-o-odpovednosti-rika-cesky-miliardar	k1gInSc1
-	-	kIx~
Chudí	chudit	k5eAaImIp3nS
versus	versus	k7c1
bohatí	bohatý	k2eAgMnPc1d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Majetek	majetek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
odpovědnosti	odpovědnost	k1gFnSc6
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
miliardář	miliardář	k1gMnSc1
<g/>
↑	↑	k?
FOSTER	FOSTER	kA
<g/>
,	,	kIx,
JOHN	John	k1gMnSc1
BELLAMY	BELLAMY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecology	Ecolog	k1gInPc7
against	against	k1gMnSc1
capitalism	capitalism	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Monthly	Monthly	k1gMnSc1
Review	Review	k1gMnSc1
Press	Press	k1gInSc4
176	#num#	k4
pages	pages	k1gMnSc1
s.	s.	k?
ISBN	ISBN	kA
1583670564	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781583670569	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
49531108	#num#	k4
↑	↑	k?
MEADOWS	MEADOWS	kA
<g/>
,	,	kIx,
DONELLA	DONELLA	kA
H.	H.	kA
The	The	k1gMnSc1
limits	limits	k6eAd1
to	ten	k3xDgNnSc4
growth	growth	k1gMnSc1
:	:	kIx,
the	the	k?
30	#num#	k4
<g/>
-year	-year	k1gInSc1
update	update	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
White	Whit	k1gInSc5
River	Rivra	k1gFnPc2
Junction	Junction	k1gInSc1
<g/>
,	,	kIx,
Vt	Vt	k1gFnSc1
<g/>
:	:	kIx,
Chelsea	Chelsea	k1gFnSc1
Green	Green	k1gInSc4
Publishing	Publishing	k1gInSc1
Company	Compana	k1gFnSc2
xxii	xxi	k1gFnSc2
<g/>
,	,	kIx,
338	#num#	k4
pages	pages	k1gInSc1
s.	s.	k?
ISBN	ISBN	kA
1931498512	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
9781931498517	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
54035081	#num#	k4
↑	↑	k?
JOHANISOVÁ	JOHANISOVÁ	kA
<g/>
,	,	kIx,
NAĎA	Naďa	k1gFnSc1
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
-	-	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomičtí	ekonomický	k2eAgMnPc1d1
disidenti	disident	k1gMnPc1
:	:	kIx,
kapitoly	kapitola	k1gFnPc1
z	z	k7c2
historie	historie	k1gFnSc2
alternativního	alternativní	k2eAgNnSc2d1
ekonomického	ekonomický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Volarech	Volar	k1gInPc6
<g/>
:	:	kIx,
Stehlík	Stehlík	k1gMnSc1
124	#num#	k4
s.	s.	k?
s.	s.	k?
ISBN	ISBN	kA
9788086913124	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
8086913120	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
872343309	#num#	k4
↑	↑	k?
MADDISON	MADDISON	kA
<g/>
,	,	kIx,
Angus	Angus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Economy	Econom	k1gInPc7
<g/>
:	:	kIx,
A	a	k9
Millennial	Millennial	k1gInSc1
Perspective	Perspectiv	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
OECD	OECD	kA
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
92	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
<g/>
-	-	kIx~
<g/>
18998	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
↑	↑	k?
MISES	MISES	kA
<g/>
,	,	kIx,
Ludwig	Ludwig	k1gMnSc1
von	von	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Human	Human	k1gInSc1
Action	Action	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
ISBN	ISBN	kA
9780865976313	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SMITH	SMITH	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojednání	pojednání	k1gNnSc1
o	o	k7c6
podstatě	podstata	k1gFnSc6
a	a	k8xC
původu	původ	k1gInSc2
bohatství	bohatství	k1gNnSc2
národů	národ	k1gInPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Liberální	liberální	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86389	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Způsob	způsob	k1gInSc1
privatizace	privatizace	k1gFnSc2
v	v	k7c6
ČR	ČR	kA
http://www.okforum.eu/?page=2	http://www.okforum.eu/?page=2	k4
<g/>
↑	↑	k?
Před	před	k7c7
dvaceti	dvacet	k4xCc7
lety	léto	k1gNnPc7
začala	začít	k5eAaPmAgFnS
kuponová	kuponový	k2eAgFnSc1d1
privatizace	privatizace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	s	k7c7
http://ekonomika.idnes.cz/pred-dvaceti-lety-zacala-kuponova-privatizace-podivejte-se-p4z-/ekonomika.aspx?c=A111031_111505_ekonomika_vem	http://ekonomika.idnes.cz/pred-dvaceti-lety-zacala-kuponova-privatizace-podivejte-se-p4z-/ekonomika.aspx?c=A111031_111505_ekonomika_vo	k1gNnSc7
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BRAUDEL	BRAUDEL	kA
<g/>
,	,	kIx,
Fernand	Fernand	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dynamika	dynamika	k1gFnSc1
kapitalismu	kapitalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
81	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
193	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DVOŘÁK	Dvořák	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
:	:	kIx,
Kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
vládne	vládnout	k5eAaImIp3nS
světu	svět	k1gInSc3
<g/>
,	,	kIx,
Eko-konzult	Eko-konzult	k1gMnSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
2004	#num#	k4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-8079-019-1	80-8079-019-1	k4
</s>
<s>
GRÜN	GRÜN	kA
<g/>
,	,	kIx,
Max	max	kA
von	von	k1gInSc1
der	drát	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc1
dopisy	dopis	k1gInPc1
Pospischielovi	Pospischiel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doslov	doslov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NZB	NZB	kA
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
274	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
905864	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JOHANISOVÁ	JOHANISOVÁ	kA
<g/>
,	,	kIx,
Naďa	Naďa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomičtí	ekonomický	k2eAgMnPc1d1
disidenti	disident	k1gMnPc1
<g/>
:	:	kIx,
kapitoly	kapitola	k1gFnPc1
z	z	k7c2
historie	historie	k1gFnSc2
alternativního	alternativní	k2eAgNnSc2d1
ekonomického	ekonomický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Volarech	Volar	k1gInPc6
<g/>
:	:	kIx,
Stehlík	Stehlík	k1gMnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiná	jiný	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86913	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MEADOWS	MEADOWS	kA
<g/>
,	,	kIx,
Donella	Donella	k1gFnSc1
H.	H.	kA
<g/>
,	,	kIx,
Jø	Jø	k1gInSc1
RANDERS	RANDERS	kA
a	a	k8xC
Dennis	Dennis	k1gInSc1
L.	L.	kA
MEADOWS	MEADOWS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The_Limits	The_Limits	k1gInSc1
to	ten	k3xDgNnSc4
Growth	Growth	k1gMnSc1
<g/>
:	:	kIx,
the	the	k?
30	#num#	k4
<g/>
-year	-year	k1gInSc1
update	update	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
White	Whit	k1gInSc5
River	Rivra	k1gFnPc2
Junction	Junction	k1gInSc1
<g/>
,	,	kIx,
Vt	Vt	k1gFnSc1
<g/>
:	:	kIx,
Chelsea	Chelsea	k1gFnSc1
Green	Green	k1gInSc4
Publishing	Publishing	k1gInSc1
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
c	c	k0
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
931498	#num#	k4
<g/>
-	-	kIx~
<g/>
58	#num#	k4
<g/>
-X	-X	k?
<g/>
<	<	kIx(
<g/>
.	.	kIx.
</s>
<s>
URBAN	Urban	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitalismus	kapitalismus	k1gInSc4
a	a	k8xC
česká	český	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
:	:	kIx,
k	k	k7c3
otázkám	otázka	k1gFnPc3
formování	formování	k1gNnSc2
české	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Martin	Martin	k1gMnSc1
Sekera	Sekera	k1gMnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
323	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
500	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
1978	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Socialismus	socialismus	k1gInSc1
</s>
<s>
Komunismus	komunismus	k1gInSc1
</s>
<s>
Antikapitalismus	antikapitalismus	k1gInSc1
</s>
<s>
Trh	trh	k1gInSc1
(	(	kIx(
<g/>
ekonomie	ekonomie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Globalizace	globalizace	k1gFnSc1
</s>
<s>
Sociální	sociální	k2eAgInSc4d1
darwinismus	darwinismus	k1gInSc4
</s>
<s>
Sweatshop	Sweatshop	k1gInSc1
</s>
<s>
Kumpánský	kumpánský	k2eAgInSc4d1
kapitalismus	kapitalismus	k1gInSc4
(	(	kIx(
<g/>
crony	cron	k1gInPc4
capitalism	capitalism	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Tržní	tržní	k2eAgInSc1d1
anarchismus	anarchismus	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kapitalismus	kapitalismus	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
kapitalismus	kapitalismus	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Téma	téma	k1gNnSc1
Kapitalismus	kapitalismus	k1gInSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4029577-1	4029577-1	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
1529	#num#	k4
</s>
