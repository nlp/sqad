<s>
Joan	Joan	k1gMnSc1	Joan
Miró	Miró	k1gMnSc1	Miró
i	i	k8xC	i
Ferrà	Ferrà	k1gMnSc1	Ferrà
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1893	[number]	k4	1893
Barcelona	Barcelona	k1gFnSc1	Barcelona
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1983	[number]	k4	1983
Palma	palma	k1gFnSc1	palma
de	de	k?	de
Mallorca	Mallorca	k1gMnSc1	Mallorca
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
katalánský	katalánský	k2eAgMnSc1d1	katalánský
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
keramik	keramik	k1gMnSc1	keramik
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
starobylé	starobylý	k2eAgFnSc6d1	starobylá
katalánské	katalánský	k2eAgFnSc6d1	katalánská
rodině	rodina	k1gFnSc6	rodina
zručných	zručný	k2eAgMnPc2d1	zručný
řemeslníků	řemeslník	k1gMnPc2	řemeslník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
majetným	majetný	k2eAgMnSc7d1	majetný
zlatníkem	zlatník	k1gMnSc7	zlatník
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
staré	starý	k2eAgFnSc2d1	stará
Barcelony	Barcelona	k1gFnSc2	Barcelona
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
Mallorky	Mallorka	k1gFnSc2	Mallorka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
Joan	Joan	k1gMnSc1	Joan
oznámil	oznámit	k5eAaPmAgMnS	oznámit
rodině	rodina	k1gFnSc3	rodina
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
stát	stát	k5eAaPmF	stát
malířem	malíř	k1gMnSc7	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
všemu	všecek	k3xTgNnSc3	všecek
naléhání	naléhání	k1gNnSc3	naléhání
odolával	odolávat	k5eAaImAgMnS	odolávat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
musel	muset	k5eAaImAgMnS	muset
mladý	mladý	k2eAgMnSc1d1	mladý
Miró	Miró	k1gMnSc1	Miró
v	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
nastoupit	nastoupit	k5eAaPmF	nastoupit
jako	jako	k9	jako
úředník	úředník	k1gMnSc1	úředník
v	v	k7c6	v
kanceláři	kancelář	k1gFnSc6	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
také	také	k9	také
školu	škola	k1gFnSc4	škola
Escuelu	Escuel	k1gInSc2	Escuel
de	de	k?	de
Bellas	Bellas	k1gMnSc1	Bellas
Artes	Artes	k1gMnSc1	Artes
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dvanáct	dvanáct	k4xCc1	dvanáct
let	léto	k1gNnPc2	léto
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
studoval	studovat	k5eAaImAgMnS	studovat
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
učitelé	učitel	k1gMnPc1	učitel
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
úžasu	úžas	k1gInSc6	úžas
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gInSc7	jeho
talentem	talent	k1gInSc7	talent
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nemohl	moct	k5eNaImAgMnS	moct
věnovat	věnovat	k5eAaPmF	věnovat
umění	umění	k1gNnSc4	umění
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yIgFnSc7	jaký
by	by	k9	by
chtěl	chtít	k5eAaImAgInS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
tak	tak	k9	tak
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
vážně	vážně	k6eAd1	vážně
nervově	nervově	k6eAd1	nervově
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
ho	on	k3xPp3gMnSc4	on
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
zotavenou	zotavená	k1gFnSc4	zotavená
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
venkovský	venkovský	k2eAgInSc1d1	venkovský
statek	statek	k1gInSc1	statek
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Tarragony	Tarragona	k1gFnSc2	Tarragona
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pocházeli	pocházet	k5eAaImAgMnP	pocházet
jeho	jeho	k3xOp3gMnPc1	jeho
předkové	předek	k1gMnPc1	předek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pobyt	pobyt	k1gInSc1	pobyt
na	na	k7c6	na
statku	statek	k1gInSc6	statek
byl	být	k5eAaImAgMnS	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
Miróových	Miróová	k1gFnPc2	Miróová
raných	raný	k2eAgFnPc2d1	raná
maleb	malba	k1gFnPc2	malba
katalánské	katalánský	k2eAgFnSc2d1	katalánská
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Miró	Miró	k1gMnSc1	Miró
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Škole	škola	k1gFnSc6	škola
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
učitelem	učitel	k1gMnSc7	učitel
Francesc	Francesc	k1gInSc1	Francesc
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Assís	Assís	k1gInSc1	Assís
Galí	Galí	k1gFnSc2	Galí
<g/>
.	.	kIx.	.
</s>
<s>
Gali	Gali	k6eAd1	Gali
měl	mít	k5eAaImAgInS	mít
na	na	k7c4	na
Miróa	Miróum	k1gNnPc4	Miróum
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
ho	on	k3xPp3gInSc4	on
opravdově	opravdově	k6eAd1	opravdově
milovat	milovat	k5eAaImF	milovat
a	a	k8xC	a
rozumět	rozumět	k5eAaImF	rozumět
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
pozorovat	pozorovat	k5eAaImF	pozorovat
a	a	k8xC	a
tvořit	tvořit	k5eAaImF	tvořit
s	s	k7c7	s
citem	cit	k1gInSc7	cit
a	a	k8xC	a
láskou	láska	k1gFnSc7	láska
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
pojetím	pojetí	k1gNnSc7	pojetí
francouzského	francouzský	k2eAgNnSc2d1	francouzské
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
básnictví	básnictví	k1gNnSc2	básnictví
a	a	k8xC	a
malířství	malířství	k1gNnSc2	malířství
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
stávají	stávat	k5eAaImIp3nP	stávat
jednou	jednou	k6eAd1	jednou
neoddělitelnou	oddělitelný	k2eNgFnSc7d1	neoddělitelná
veličinou	veličina	k1gFnSc7	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
přitahován	přitahovat	k5eAaImNgInS	přitahovat
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
tak	tak	k6eAd1	tak
musí	muset	k5eAaImIp3nS	muset
Miró	Miró	k1gFnSc1	Miró
zůstat	zůstat	k5eAaPmF	zůstat
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Neklid	neklid	k1gInSc1	neklid
a	a	k8xC	a
frustrace	frustrace	k1gFnSc1	frustrace
ho	on	k3xPp3gMnSc4	on
nutí	nutit	k5eAaImIp3nS	nutit
malovat	malovat	k5eAaImF	malovat
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
podkrovní	podkrovní	k2eAgFnSc6d1	podkrovní
komůrce	komůrka	k1gFnSc6	komůrka
v	v	k7c4	v
Pasaje	Pasaj	k1gInPc4	Pasaj
del	del	k?	del
Credito	Credita	k1gFnSc5	Credita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
Miró	Miró	k1gFnSc1	Miró
pořádá	pořádat	k5eAaImIp3nS	pořádat
první	první	k4xOgFnSc4	první
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
Galerias	Galeriasa	k1gFnPc2	Galeriasa
Dalmau	Dalmaus	k1gInSc2	Dalmaus
<g/>
.	.	kIx.	.
</s>
<s>
Vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
úžas	úžas	k1gInSc1	úžas
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
návštěvníků	návštěvník	k1gMnPc2	návštěvník
nevšedním	všednět	k5eNaImIp1nS	všednět
užitím	užití	k1gNnSc7	užití
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
originalitou	originalita	k1gFnSc7	originalita
svého	svůj	k3xOyFgInSc2	svůj
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
fauvismem	fauvismus	k1gInSc7	fauvismus
a	a	k8xC	a
kubismem	kubismus	k1gInSc7	kubismus
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
nesdílí	sdílet	k5eNaImIp3nS	sdílet
přesně	přesně	k6eAd1	přesně
stejnou	stejný	k2eAgFnSc4d1	stejná
techniku	technika	k1gFnSc4	technika
analýzy	analýza	k1gFnSc2	analýza
jako	jako	k8xS	jako
Picasso	Picassa	k1gFnSc5	Picassa
a	a	k8xC	a
Braque	Braque	k1gNnPc3	Braque
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
geometrické	geometrický	k2eAgInPc4d1	geometrický
tvary	tvar	k1gInPc4	tvar
k	k	k7c3	k
podpoření	podpoření	k1gNnSc3	podpoření
dojmu	dojem	k1gInSc2	dojem
pohybu	pohyb	k1gInSc2	pohyb
vedoucího	vedoucí	k1gMnSc2	vedoucí
oko	oko	k1gNnSc4	oko
diváka	divák	k1gMnSc4	divák
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
barev	barva	k1gFnPc2	barva
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
zátiších	zátiší	k1gNnPc6	zátiší
a	a	k8xC	a
krajinách	krajina	k1gFnPc6	krajina
otevírá	otevírat	k5eAaImIp3nS	otevírat
nové	nový	k2eAgFnPc4d1	nová
a	a	k8xC	a
překvapující	překvapující	k2eAgFnPc4d1	překvapující
možnosti	možnost	k1gFnPc4	možnost
zobrazení	zobrazení	k1gNnSc2	zobrazení
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
nevšedních	všední	k2eNgInPc2d1	nevšední
prvků	prvek	k1gInPc2	prvek
k	k	k7c3	k
zvýraznění	zvýraznění	k1gNnSc3	zvýraznění
emocionálního	emocionální	k2eAgInSc2d1	emocionální
efektu	efekt	k1gInSc2	efekt
typického	typický	k2eAgMnSc4d1	typický
pro	pro	k7c4	pro
katalánské	katalánský	k2eAgNnSc4d1	katalánské
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
přínosem	přínos	k1gInSc7	přínos
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
jeho	jeho	k3xOp3gInPc4	jeho
nevšední	všední	k2eNgInPc4d1	nevšední
akty	akt	k1gInPc4	akt
a	a	k8xC	a
zobrazení	zobrazení	k1gNnSc4	zobrazení
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
opěvuje	opěvovat	k5eAaImIp3nS	opěvovat
novou	nový	k2eAgFnSc4d1	nová
osobní	osobní	k2eAgFnSc4d1	osobní
mystickou	mystický	k2eAgFnSc4d1	mystická
analýzu	analýza	k1gFnSc4	analýza
ženské	ženský	k2eAgFnSc2d1	ženská
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
reprezentací	reprezentace	k1gFnSc7	reprezentace
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
ženské	ženský	k2eAgFnSc2d1	ženská
přítomnosti	přítomnost	k1gFnSc2	přítomnost
a	a	k8xC	a
kouzla	kouzlo	k1gNnSc2	kouzlo
v	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
obrazech	obraz	k1gInPc6	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Výstava	výstava	k1gFnSc1	výstava
však	však	k9	však
není	být	k5eNaImIp3nS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
obrazy	obraz	k1gInPc1	obraz
dělají	dělat	k5eAaImIp3nP	dělat
dojem	dojem	k1gInSc4	dojem
jen	jen	k9	jen
na	na	k7c4	na
malou	malý	k2eAgFnSc4d1	malá
skupinku	skupinka	k1gFnSc4	skupinka
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
návštěvníků	návštěvník	k1gMnPc2	návštěvník
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
pochopit	pochopit	k5eAaPmF	pochopit
a	a	k8xC	a
rozeznat	rozeznat	k5eAaPmF	rozeznat
jeho	jeho	k3xOp3gInSc4	jeho
nevšední	všední	k2eNgInSc4d1	nevšední
talent	talent	k1gInSc4	talent
<g/>
.	.	kIx.	.
</s>
<s>
Apatie	apatie	k1gFnSc1	apatie
a	a	k8xC	a
nevšímavost	nevšímavost	k1gFnSc1	nevšímavost
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
ho	on	k3xPp3gInSc4	on
plně	plně	k6eAd1	plně
přesvědčila	přesvědčit	k5eAaPmAgFnS	přesvědčit
opustit	opustit	k5eAaPmF	opustit
domov	domov	k1gInSc4	domov
a	a	k8xC	a
odjet	odjet	k5eAaPmF	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
návštěva	návštěva	k1gFnSc1	návštěva
Paříže	Paříž	k1gFnSc2	Paříž
nebyla	být	k5eNaImAgFnS	být
pro	pro	k7c4	pro
Miróa	Miróum	k1gNnPc4	Miróum
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
prodej	prodej	k1gInSc1	prodej
jeho	jeho	k3xOp3gFnPc2	jeho
maleb	malba	k1gFnPc2	malba
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
ještě	ještě	k6eAd1	ještě
riskantnější	riskantní	k2eAgFnPc4d2	riskantnější
než	než	k8xS	než
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
neměl	mít	k5eNaImAgInS	mít
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
veliké	veliký	k2eAgFnSc6d1	veliká
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Setkal	setkat	k5eAaPmAgInS	setkat
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
Picassem	Picass	k1gMnSc7	Picass
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
prodal	prodat	k5eAaPmAgInS	prodat
svůj	svůj	k3xOyFgInSc4	svůj
autoportrét	autoportrét	k1gInSc4	autoportrét
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
si	se	k3xPyFc3	se
Picasso	Picassa	k1gFnSc5	Picassa
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
sbírce	sbírka	k1gFnSc6	sbírka
velmi	velmi	k6eAd1	velmi
cenil	cenit	k5eAaImAgMnS	cenit
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
1919	[number]	k4	1919
tráví	trávit	k5eAaImIp3nS	trávit
Miró	Miró	k1gFnSc1	Miró
v	v	k7c4	v
Mont-roig	Montoig	k1gInSc4	Mont-roig
del	del	k?	del
Camp	camp	k1gInSc1	camp
<g/>
.	.	kIx.	.
</s>
<s>
Zdokonaluje	zdokonalovat	k5eAaImIp3nS	zdokonalovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
stylu	styl	k1gInSc6	styl
s	s	k7c7	s
detailním	detailní	k2eAgInSc7d1	detailní
precizním	precizní	k2eAgInSc7d1	precizní
rytmem	rytmus	k1gInSc7	rytmus
tradičního	tradiční	k2eAgNnSc2d1	tradiční
harmonického	harmonický	k2eAgNnSc2d1	harmonické
katalánského	katalánský	k2eAgNnSc2d1	katalánské
umění	umění	k1gNnSc2	umění
venkova	venkov	k1gInSc2	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
obrazy	obraz	k1gInPc4	obraz
maluje	malovat	k5eAaImIp3nS	malovat
v	v	k7c6	v
jasně	jasně	k6eAd1	jasně
definované	definovaný	k2eAgFnSc6d1	definovaná
geometrické	geometrický	k2eAgFnSc6d1	geometrická
struktuře	struktura	k1gFnSc6	struktura
dvou	dva	k4xCgInPc2	dva
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
fantazie	fantazie	k1gFnSc2	fantazie
a	a	k8xC	a
harmonie	harmonie	k1gFnSc2	harmonie
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tam	tam	k6eAd1	tam
vystavoval	vystavovat	k5eAaImAgInS	vystavovat
svoje	svůj	k3xOyFgInPc4	svůj
obrazy	obraz	k1gInPc4	obraz
z	z	k7c2	z
neslavné	slavný	k2eNgFnSc2d1	neslavná
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
vsak	vsak	k1gInSc1	vsak
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
nepřijímá	přijímat	k5eNaImIp3nS	přijímat
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
lépe	dobře	k6eAd2	dobře
a	a	k8xC	a
tak	tak	k6eAd1	tak
výstava	výstava	k1gFnSc1	výstava
končí	končit	k5eAaImIp3nS	končit
fiaskem	fiasko	k1gNnSc7	fiasko
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
se	se	k3xPyFc4	se
osudově	osudově	k6eAd1	osudově
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Andre	Andr	k1gInSc5	Andr
Bretonem	Breton	k1gMnSc7	Breton
<g/>
,	,	kIx,	,
Paulem	Paul	k1gMnSc7	Paul
Eluardem	Eluard	k1gMnSc7	Eluard
a	a	k8xC	a
Louisem	Louis	k1gMnSc7	Louis
Aragonem	Aragon	k1gMnSc7	Aragon
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
právě	právě	k6eAd1	právě
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
přinášejí	přinášet	k5eAaImIp3nP	přinášet
vznik	vznik	k1gInSc4	vznik
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčují	přesvědčovat	k5eAaImIp3nP	přesvědčovat
Miróa	Miróa	k1gFnSc1	Miróa
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
všem	všecek	k3xTgNnSc6	všecek
utrpení	utrpení	k1gNnSc6	utrpení
a	a	k8xC	a
krutostech	krutost	k1gFnPc6	krutost
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
objevit	objevit	k5eAaPmF	objevit
nové	nový	k2eAgFnPc4d1	nová
metody	metoda	k1gFnPc4	metoda
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Přejí	přejíst	k5eAaPmIp3nS	přejíst
si	se	k3xPyFc3	se
proniknout	proniknout	k5eAaPmF	proniknout
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
základní	základní	k2eAgFnSc2d1	základní
povahy	povaha	k1gFnSc2	povaha
a	a	k8xC	a
inspirace	inspirace	k1gFnSc2	inspirace
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
všechny	všechen	k3xTgInPc4	všechen
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
logickou	logický	k2eAgFnSc4d1	logická
analýzu	analýza	k1gFnSc4	analýza
reality	realita	k1gFnSc2	realita
selhaly	selhat	k5eAaPmAgFnP	selhat
<g/>
,	,	kIx,	,
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
hlubšímu	hluboký	k2eAgNnSc3d2	hlubší
porozumění	porozumění	k1gNnSc3	porozumění
vědomí	vědomí	k1gNnSc2	vědomí
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
podotkl	podotknout	k5eAaPmAgMnS	podotknout
Freud	Freud	k1gMnSc1	Freud
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
podvědomí	podvědomí	k1gNnSc2	podvědomí
v	v	k7c6	v
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
možném	možný	k2eAgInSc6d1	možný
úhlu	úhel	k1gInSc6	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Manifest	manifest	k1gInSc1	manifest
surrealismu	surrealismus	k1gInSc2	surrealismus
je	být	k5eAaImIp3nS	být
sepsán	sepsat	k5eAaPmNgInS	sepsat
André	André	k1gMnSc7	André
Bretonem	Breton	k1gMnSc7	Breton
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
se	se	k3xPyFc4	se
myšlenky	myšlenka	k1gFnSc2	myšlenka
surrealismu	surrealismus	k1gInSc2	surrealismus
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
umění	umění	k1gNnSc6	umění
chápe	chápat	k5eAaImIp3nS	chápat
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
však	však	k9	však
ostatní	ostatní	k2eAgMnPc1d1	ostatní
surrealisté	surrealista	k1gMnPc1	surrealista
využívají	využívat	k5eAaPmIp3nP	využívat
vidin	vidina	k1gFnPc2	vidina
snů	sen	k1gInPc2	sen
<g/>
,	,	kIx,	,
halucinací	halucinace	k1gFnPc2	halucinace
<g/>
,	,	kIx,	,
hysterie	hysterie	k1gFnSc2	hysterie
a	a	k8xC	a
šílenství	šílenství	k1gNnSc2	šílenství
k	k	k7c3	k
proniknutí	proniknutí	k1gNnSc3	proniknutí
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
podvědomí	podvědomí	k1gNnSc2	podvědomí
<g/>
,	,	kIx,	,
Miró	Miró	k1gFnSc1	Miró
se	se	k3xPyFc4	se
nechává	nechávat	k5eAaImIp3nS	nechávat
vést	vést	k5eAaImF	vést
svou	svůj	k3xOyFgFnSc7	svůj
představivostí	představivost	k1gFnSc7	představivost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
nemá	mít	k5eNaImIp3nS	mít
peníze	peníz	k1gInPc4	peníz
ani	ani	k8xC	ani
na	na	k7c4	na
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
za	za	k7c4	za
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
jen	jen	k9	jen
několika	několik	k4yIc7	několik
suchými	suchý	k2eAgInPc7d1	suchý
fíky	fík	k1gInPc7	fík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
pyšný	pyšný	k2eAgMnSc1d1	pyšný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
žádal	žádat	k5eAaImAgMnS	žádat
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jen	jen	k9	jen
sedí	sedit	k5eAaImIp3nP	sedit
hodiny	hodina	k1gFnPc1	hodina
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
studiu	studio	k1gNnSc6	studio
<g/>
,	,	kIx,	,
zírá	zírat	k5eAaImIp3nS	zírat
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
a	a	k8xC	a
pak	pak	k6eAd1	pak
jen	jen	k9	jen
maluje	malovat	k5eAaImIp3nS	malovat
na	na	k7c4	na
kus	kus	k1gInSc4	kus
papíru	papír	k1gInSc2	papír
nebo	nebo	k8xC	nebo
plátna	plátno	k1gNnSc2	plátno
veden	veden	k2eAgInSc4d1	veden
představivostí	představivost	k1gFnSc7	představivost
vyvolanou	vyvolaný	k2eAgFnSc7d1	vyvolaná
hladem	hlad	k1gInSc7	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Miróova	Miróův	k2eAgFnSc1d1	Miróův
představivost	představivost	k1gFnSc1	představivost
byla	být	k5eAaImAgFnS	být
nevyčerpatelným	vyčerpatelný	k2eNgInSc7d1	nevyčerpatelný
zdrojem	zdroj	k1gInSc7	zdroj
novotvarů	novotvar	k1gInPc2	novotvar
a	a	k8xC	a
detailů	detail	k1gInPc2	detail
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Miróa	Miróus	k1gMnSc4	Miróus
stalo	stát	k5eAaPmAgNnS	stát
dětským	dětský	k2eAgNnSc7d1	dětské
hřištěm	hřiště	k1gNnSc7	hřiště
i	i	k8xC	i
tanečním	taneční	k2eAgInSc7d1	taneční
parketem	parket	k1gInSc7	parket
precizního	precizní	k2eAgInSc2d1	precizní
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
detailu	detail	k1gInSc2	detail
a	a	k8xC	a
charakteristického	charakteristický	k2eAgInSc2d1	charakteristický
výrazu	výraz	k1gInSc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Přísný	přísný	k2eAgInSc1d1	přísný
rytmus	rytmus	k1gInSc1	rytmus
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
barvy	barva	k1gFnPc4	barva
a	a	k8xC	a
formy	forma	k1gFnSc2	forma
svou	svůj	k3xOyFgFnSc7	svůj
poetickou	poetický	k2eAgFnSc7d1	poetická
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
objevuje	objevovat	k5eAaImIp3nS	objevovat
symboly	symbol	k1gInPc4	symbol
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
viditelné	viditelný	k2eAgFnPc1d1	viditelná
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
skryté	skrytý	k2eAgFnPc1d1	skrytá
a	a	k8xC	a
nevysvětlitelné	vysvětlitelný	k2eNgFnPc1d1	nevysvětlitelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
Miró	Miró	k1gFnSc1	Miró
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
Ernestem	Ernest	k1gMnSc7	Ernest
Hemingwayem	Hemingway	k1gMnSc7	Hemingway
<g/>
,	,	kIx,	,
Jacquem	Jacqu	k1gMnSc7	Jacqu
Prévertem	Prévert	k1gMnSc7	Prévert
a	a	k8xC	a
Henry	Henry	k1gMnSc1	Henry
Millerem	Miller	k1gMnSc7	Miller
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zlomu	zlom	k1gInSc3	zlom
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
chudobě	chudoba	k1gFnSc6	chudoba
–	–	k?	–
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
slavnější	slavný	k2eAgInPc1d2	slavnější
a	a	k8xC	a
tak	tak	k6eAd1	tak
pomalu	pomalu	k6eAd1	pomalu
začíná	začínat	k5eAaImIp3nS	začínat
prodávat	prodávat	k5eAaImF	prodávat
svoje	svůj	k3xOyFgFnPc4	svůj
malby	malba	k1gFnPc4	malba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1926	[number]	k4	1926
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
nové	nový	k2eAgFnPc4d1	nová
Galerie	galerie	k1gFnPc4	galerie
Surrealisté	surrealista	k1gMnPc1	surrealista
společně	společně	k6eAd1	společně
s	s	k7c7	s
umělci	umělec	k1gMnPc7	umělec
jako	jako	k8xS	jako
Yves	Yvesa	k1gFnPc2	Yvesa
Tanguy	Tangua	k1gFnSc2	Tangua
<g/>
,	,	kIx,	,
Giorgio	Giorgio	k1gMnSc1	Giorgio
de	de	k?	de
Chirico	Chirico	k1gMnSc1	Chirico
<g/>
,	,	kIx,	,
Man	Man	k1gMnSc1	Man
Ray	Ray	k1gMnSc1	Ray
<g/>
,	,	kIx,	,
Rose	Rose	k1gMnSc1	Rose
Selavy	Selava	k1gFnSc2	Selava
(	(	kIx(	(
<g/>
Marcel	Marcel	k1gMnSc1	Marcel
Duchamp	Duchamp	k1gMnSc1	Duchamp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francis	Francis	k1gFnSc1	Francis
Picabia	Picabia	k1gFnSc1	Picabia
<g/>
,	,	kIx,	,
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
a	a	k8xC	a
Max	Max	k1gMnSc1	Max
Ernst	Ernst	k1gMnSc1	Ernst
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
své	svůj	k3xOyFgFnPc4	svůj
malby	malba	k1gFnPc4	malba
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
lyrickou	lyrický	k2eAgFnSc4d1	lyrická
svobodu	svoboda	k1gFnSc4	svoboda
výrazu	výraz	k1gInSc2	výraz
a	a	k8xC	a
ducha	duch	k1gMnSc2	duch
v	v	k7c6	v
hlubinách	hlubina	k1gFnPc6	hlubina
svého	svůj	k3xOyFgNnSc2	svůj
povědomí	povědomí	k1gNnSc2	povědomí
<g/>
.	.	kIx.	.
</s>
<s>
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
stylu	styl	k1gInSc6	styl
dokonce	dokonce	k9	dokonce
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nejsurrealističtější	surrealistický	k2eAgMnSc1d3	surrealistický
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
surrealistů	surrealista	k1gMnPc2	surrealista
<g/>
.	.	kIx.	.
</s>
<s>
Zjednodušením	zjednodušení	k1gNnSc7	zjednodušení
struktur	struktura	k1gFnPc2	struktura
obrazu	obraz	k1gInSc2	obraz
se	se	k3xPyFc4	se
Miró	Miró	k1gMnSc1	Miró
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
období	období	k1gNnSc2	období
tzv.	tzv.	kA	tzv.
snových	snový	k2eAgFnPc2d1	snová
maleb	malba	k1gFnPc2	malba
<g/>
.	.	kIx.	.
</s>
<s>
Potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
dokonce	dokonce	k9	dokonce
symboly	symbol	k1gInPc4	symbol
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
abstrakce	abstrakce	k1gFnSc2	abstrakce
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
své	svůj	k3xOyFgInPc4	svůj
sny	sen	k1gInPc4	sen
z	z	k7c2	z
pamětí	paměť	k1gFnPc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
malby	malba	k1gFnPc1	malba
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgNnSc4d1	plné
napětí	napětí	k1gNnSc4	napětí
mysteriozní	mysteriozní	k2eAgFnSc2d1	mysteriozní
kosmické	kosmický	k2eAgFnSc2d1	kosmická
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
neodvolatelného	odvolatelný	k2eNgNnSc2d1	neodvolatelné
setkání	setkání	k1gNnSc2	setkání
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
katastrofu	katastrofa	k1gFnSc4	katastrofa
nebo	nebo	k8xC	nebo
zjevení	zjevení	k1gNnSc1	zjevení
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
bez	bez	k7c2	bez
předstírání	předstírání	k1gNnSc2	předstírání
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
malby	malba	k1gFnPc1	malba
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
zjednodušeny	zjednodušen	k2eAgFnPc1d1	zjednodušena
až	až	k9	až
do	do	k7c2	do
užití	užití	k1gNnSc2	užití
pouhé	pouhý	k2eAgFnSc2d1	pouhá
černé	černá	k1gFnSc2	černá
a	a	k8xC	a
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
,	,	kIx,	,
pozadí	pozadí	k1gNnSc1	pozadí
dělá	dělat	k5eAaImIp3nS	dělat
dojem	dojem	k1gInSc4	dojem
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
,	,	kIx,	,
snových	snový	k2eAgNnPc2d1	snové
setkání	setkání	k1gNnPc2	setkání
<g/>
,	,	kIx,	,
tance	tanec	k1gInSc2	tanec
v	v	k7c6	v
nekontrolovatelných	kontrolovatelný	k2eNgFnPc6d1	nekontrolovatelná
sílách	síla	k1gFnPc6	síla
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
také	také	k9	také
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
'	'	kIx"	'
<g/>
snových	snový	k2eAgFnPc6d1	snová
malbách	malba	k1gFnPc6	malba
<g/>
'	'	kIx"	'
používá	používat	k5eAaImIp3nS	používat
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Apollinaira	Apollinairo	k1gNnSc2	Apollinairo
kaligramu	kaligram	k1gInSc2	kaligram
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
Miró	Miró	k1gFnSc1	Miró
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
ateliéru	ateliér	k1gInSc2	ateliér
na	na	k7c6	na
Rue	Rue	k1gFnSc6	Rue
Blomet	Blometa	k1gFnPc2	Blometa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dveře	dveře	k1gFnPc4	dveře
si	se	k3xPyFc3	se
přibíjí	přibíjet	k5eAaImIp3nP	přibíjet
desku	deska	k1gFnSc4	deska
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
'	'	kIx"	'
<g/>
train	train	k2eAgMnSc1d1	train
passant	passant	k1gMnSc1	passant
sans	sansa	k1gFnPc2	sansa
arret	arret	k1gMnSc1	arret
<g/>
'	'	kIx"	'
–	–	k?	–
'	'	kIx"	'
<g/>
vlak	vlak	k1gInSc4	vlak
projíždějící	projíždějící	k2eAgInSc4d1	projíždějící
bez	bez	k7c2	bez
zastavení	zastavení	k1gNnPc2	zastavení
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Přátelí	přátelit	k5eAaImIp3nS	přátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
Arpem	Arp	k1gMnSc7	Arp
<g/>
,	,	kIx,	,
Eluardem	Eluard	k1gMnSc7	Eluard
i	i	k8xC	i
Ernstem	Ernst	k1gMnSc7	Ernst
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
historka	historka	k1gFnSc1	historka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
skončit	skončit	k5eAaPmF	skončit
neštěstím	neštěstí	k1gNnSc7	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
ukazoval	ukazovat	k5eAaImAgMnS	ukazovat
Miróovi	Miróa	k1gMnSc3	Miróa
svoje	svůj	k3xOyFgInPc4	svůj
nejnovější	nový	k2eAgInPc4d3	nejnovější
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
s	s	k7c7	s
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nevídanou	vídaný	k2eNgFnSc7d1	nevídaná
koláží	koláž	k1gFnSc7	koláž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
pak	pak	k6eAd1	pak
po	po	k7c6	po
Miróovi	Miróa	k1gMnSc6	Miróa
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
ukázal	ukázat	k5eAaPmAgInS	ukázat
jeho	jeho	k3xOp3gInPc4	jeho
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
řekl	říct	k5eAaPmAgInS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
dealer	dealer	k1gMnSc1	dealer
zakázal	zakázat	k5eAaPmAgMnS	zakázat
komukoliv	kdokoliv	k3yInSc3	kdokoliv
ukazovat	ukazovat	k5eAaImF	ukazovat
<g/>
.	.	kIx.	.
</s>
<s>
Ernstovi	Ernstův	k2eAgMnPc1d1	Ernstův
to	ten	k3xDgNnSc4	ten
vadilo	vadit	k5eAaImAgNnS	vadit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
pozval	pozvat	k5eAaPmAgMnS	pozvat
k	k	k7c3	k
sobě	se	k3xPyFc3	se
do	do	k7c2	do
ateliéru	ateliér	k1gInSc2	ateliér
ostatní	ostatní	k2eAgMnPc4d1	ostatní
surrealisty	surrealista	k1gMnPc4	surrealista
a	a	k8xC	a
opili	opít	k5eAaPmAgMnP	opít
se	se	k3xPyFc4	se
vínem	víno	k1gNnSc7	víno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
náladě	nálada	k1gFnSc6	nálada
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
navštívit	navštívit	k5eAaPmF	navštívit
Miróa	Miróus	k1gMnSc4	Miróus
a	a	k8xC	a
vymámit	vymámit	k5eAaPmF	vymámit
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
ukázal	ukázat	k5eAaPmAgMnS	ukázat
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
si	se	k3xPyFc3	se
však	však	k9	však
jen	jen	k9	jen
maloval	malovat	k5eAaImAgMnS	malovat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
nevšímal	všímat	k5eNaImAgMnS	všímat
si	se	k3xPyFc3	se
jich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Ernst	Ernst	k1gMnSc1	Ernst
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
svou	svůj	k3xOyFgFnSc7	svůj
opilostí	opilost	k1gFnSc7	opilost
agresivní	agresivní	k2eAgFnSc7d1	agresivní
–	–	k?	–
odtáhli	odtáhnout	k5eAaPmAgMnP	odtáhnout
Miróa	Miróus	k1gMnSc4	Miróus
na	na	k7c4	na
schodiště	schodiště	k1gNnSc4	schodiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ernst	Ernst	k1gMnSc1	Ernst
našel	najít	k5eAaPmAgMnS	najít
Miróuv	Miróuv	k1gInSc4	Miróuv
horolezecký	horolezecký	k2eAgInSc4d1	horolezecký
provaz	provaz	k1gInSc4	provaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
chtěli	chtít	k5eAaImAgMnP	chtít
Miróa	Miróus	k1gMnSc4	Miróus
oběsit	oběsit	k5eAaPmF	oběsit
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
se	se	k3xPyFc4	se
zachránil	zachránit	k5eAaPmAgMnS	zachránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c4	mezi
smyčku	smyčka	k1gFnSc4	smyčka
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
krk	krk	k1gInSc4	krk
dal	dát	k5eAaPmAgMnS	dát
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
vysmekl	vysmeknout	k5eAaPmAgMnS	vysmeknout
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
a	a	k8xC	a
utekl	utéct	k5eAaPmAgMnS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
ho	on	k3xPp3gMnSc4	on
neviděl	vidět	k5eNaImAgMnS	vidět
po	po	k7c4	po
celé	celý	k2eAgInPc4d1	celý
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
a	a	k8xC	a
Ernst	Ernst	k1gMnSc1	Ernst
zůstali	zůstat	k5eAaPmAgMnP	zůstat
i	i	k9	i
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
poněkud	poněkud	k6eAd1	poněkud
drastické	drastický	k2eAgFnSc3d1	drastická
události	událost	k1gFnSc3	událost
dobrými	dobrý	k2eAgMnPc7d1	dobrý
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
1929	[number]	k4	1929
tráví	trávit	k5eAaImIp3nS	trávit
Miró	Miró	k1gFnSc1	Miró
v	v	k7c6	v
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
si	se	k3xPyFc3	se
bere	brát	k5eAaImIp3nS	brát
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
Pilar	Pilar	k1gMnSc1	Pilar
Juncosu	Juncos	k1gInSc2	Juncos
ze	z	k7c2	z
starobylé	starobylý	k2eAgFnSc2d1	starobylá
majorské	majorský	k2eAgFnSc2d1	majorský
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
se	se	k3xPyFc4	se
jim	on	k3xPp3gInPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
jediná	jediný	k2eAgFnSc1d1	jediná
dcera	dcera	k1gFnSc1	dcera
Dolores	Doloresa	k1gFnPc2	Doloresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
štěstí	štěstí	k1gNnSc2	štěstí
byl	být	k5eAaImAgInS	být
Miró	Miró	k1gFnSc4	Miró
nesmírně	smírně	k6eNd1	smírně
produktivní	produktivní	k2eAgMnSc1d1	produktivní
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
technika	technika	k1gFnSc1	technika
snové	snový	k2eAgFnSc2d1	snová
malby	malba	k1gFnSc2	malba
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
však	však	k9	však
stává	stávat	k5eAaImIp3nS	stávat
nedostačující	dostačující	k2eNgNnSc1d1	nedostačující
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Miró	Miró	k1gFnSc1	Miró
absolutně	absolutně	k6eAd1	absolutně
odmítá	odmítat	k5eAaImIp3nS	odmítat
olej	olej	k1gInSc4	olej
a	a	k8xC	a
temperu	tempera	k1gFnSc4	tempera
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
malby	malba	k1gFnPc1	malba
jsou	být	k5eAaImIp3nP	být
absolutně	absolutně	k6eAd1	absolutně
abstraktní	abstraktní	k2eAgMnSc1d1	abstraktní
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
koláže	koláž	k1gFnPc4	koláž
a	a	k8xC	a
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
konstrukce	konstrukce	k1gFnPc4	konstrukce
v	v	k7c6	v
přesně	přesně	k6eAd1	přesně
vypočítaném	vypočítaný	k2eAgInSc6d1	vypočítaný
vztahu	vztah	k1gInSc6	vztah
formy	forma	k1gFnSc2	forma
a	a	k8xC	a
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Miró	Miró	k1gFnSc1	Miró
svou	svůj	k3xOyFgFnSc7	svůj
technikou	technika	k1gFnSc7	technika
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
politické	politický	k2eAgFnPc4d1	politická
události	událost	k1gFnPc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
plátna	plátno	k1gNnPc1	plátno
jsou	být	k5eAaImIp3nP	být
plná	plný	k2eAgNnPc1d1	plné
monster	monstrum	k1gNnPc2	monstrum
vyvedených	vyvedený	k2eAgMnPc2d1	vyvedený
v	v	k7c6	v
drásavých	drásavý	k2eAgFnPc6d1	drásavá
barvách	barva	k1gFnPc6	barva
na	na	k7c6	na
černé	černá	k1gFnSc6	černá
nebo	nebo	k8xC	nebo
červené	červený	k2eAgFnSc3d1	červená
obloze	obloha	k1gFnSc3	obloha
konfliktů	konflikt	k1gInPc2	konflikt
absurdit	absurdita	k1gFnPc2	absurdita
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Násilí	násilí	k1gNnSc1	násilí
Miró	Miró	k1gFnSc2	Miró
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
maximální	maximální	k2eAgFnSc7d1	maximální
abstrakcí	abstrakce	k1gFnSc7	abstrakce
<g/>
,	,	kIx,	,
v	v	k7c6	v
malbách	malba	k1gFnPc6	malba
využívá	využívat	k5eAaPmIp3nS	využívat
abstraktní	abstraktní	k2eAgInPc4d1	abstraktní
prvky	prvek	k1gInPc4	prvek
jako	jako	k8xC	jako
ustříhané	ustříhaný	k2eAgInPc4d1	ustříhaný
nehty	nehet	k1gInPc4	nehet
<g/>
,	,	kIx,	,
staré	starý	k2eAgInPc4d1	starý
zubní	zubní	k2eAgInPc4d1	zubní
kartáčky	kartáček	k1gInPc4	kartáček
<g/>
,	,	kIx,	,
rezavé	rezavý	k2eAgInPc4d1	rezavý
řetězy	řetěz	k1gInPc4	řetěz
<g/>
,	,	kIx,	,
pružiny	pružina	k1gFnPc4	pružina
a	a	k8xC	a
smotané	smotaný	k2eAgInPc4d1	smotaný
provazy	provaz	k1gInPc4	provaz
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
začátkem	začátek	k1gInSc7	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Miró	Miró	k1gFnPc1	Miró
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
do	do	k7c2	do
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
,	,	kIx,	,
do	do	k7c2	do
vesničky	vesnička	k1gFnSc2	vesnička
Le	Le	k1gFnSc2	Le
Clos	Closa	k1gFnPc2	Closa
des	des	k1gNnSc2	des
Sansonnets	Sansonnetsa	k1gFnPc2	Sansonnetsa
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
však	však	k9	však
odtamtud	odtamtud	k6eAd1	odtamtud
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Němci	Němec	k1gMnPc1	Němec
na	na	k7c6	na
Normandii	Normandie	k1gFnSc6	Normandie
útočí	útočit	k5eAaImIp3nS	útočit
bombardováním	bombardování	k1gNnSc7	bombardování
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
umění	umění	k1gNnSc2	umění
zachraňuje	zachraňovat	k5eAaImIp3nS	zachraňovat
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
pouze	pouze	k6eAd1	pouze
'	'	kIx"	'
<g/>
Konstelace	konstelace	k1gFnSc1	konstelace
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Konstelace	konstelace	k1gFnPc1	konstelace
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
depresi	deprese	k1gFnSc6	deprese
<g/>
,	,	kIx,	,
Miró	Miró	k1gMnSc1	Miró
nevěřil	věřit	k5eNaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
Němci	Němec	k1gMnPc1	Němec
mohli	moct	k5eAaImAgMnP	moct
nevyhrát	vyhrát	k5eNaPmF	vyhrát
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
vytratily	vytratit	k5eAaPmAgFnP	vytratit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
všechny	všechen	k3xTgInPc4	všechen
důvody	důvod	k1gInPc4	důvod
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
se	se	k3xPyFc4	se
v	v	k7c6	v
Konstelacích	konstelace	k1gFnPc6	konstelace
snaží	snažit	k5eAaImIp3nP	snažit
hledat	hledat	k5eAaImF	hledat
nacisty	nacista	k1gMnSc2	nacista
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
jistotu	jistota	k1gFnSc4	jistota
v	v	k7c4	v
harmonii	harmonie	k1gFnSc4	harmonie
symbolů	symbol	k1gInPc2	symbol
svých	svůj	k3xOyFgInPc2	svůj
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Symboly	symbol	k1gInPc1	symbol
na	na	k7c6	na
plátnech	plátno	k1gNnPc6	plátno
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
hledání	hledání	k1gNnSc3	hledání
jednotnosti	jednotnost	k1gFnSc2	jednotnost
<g/>
,	,	kIx,	,
ornamenty	ornament	k1gInPc1	ornament
a	a	k8xC	a
plnost	plnost	k1gFnSc1	plnost
kosmického	kosmický	k2eAgInSc2d1	kosmický
pořádku	pořádek	k1gInSc2	pořádek
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
klidem	klid	k1gInSc7	klid
a	a	k8xC	a
vyrovnaností	vyrovnanost	k1gFnSc7	vyrovnanost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
Miró	Miró	k1gFnPc2	Miró
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
na	na	k7c4	na
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
uspořádána	uspořádán	k2eAgFnSc1d1	uspořádána
výstava	výstava	k1gFnSc1	výstava
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
vzbudí	vzbudit	k5eAaPmIp3nS	vzbudit
takový	takový	k3xDgInSc4	takový
ohlas	ohlas	k1gInSc4	ohlas
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Miró	Miró	k1gMnSc1	Miró
požádán	požádat	k5eAaPmNgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dělal	dělat	k5eAaImAgMnS	dělat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
nástěnné	nástěnný	k2eAgFnPc4d1	nástěnná
malby	malba	k1gFnPc4	malba
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
úspěch	úspěch	k1gInSc4	úspěch
i	i	k9	i
v	v	k7c6	v
sochařství	sochařství	k1gNnSc6	sochařství
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
návštěva	návštěva	k1gFnSc1	návštěva
USA	USA	kA	USA
stává	stávat	k5eAaImIp3nS	stávat
rozšířením	rozšíření	k1gNnSc7	rozšíření
jeho	jeho	k3xOp3gInPc2	jeho
uměleckých	umělecký	k2eAgInPc2d1	umělecký
obzorů	obzor	k1gInPc2	obzor
<g/>
.	.	kIx.	.
</s>
<s>
Miró	Miró	k?	Miró
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
surrealistů	surrealista	k1gMnPc2	surrealista
<g/>
,	,	kIx,	,
našel	najít	k5eAaPmAgMnS	najít
cestu	cesta	k1gFnSc4	cesta
z	z	k7c2	z
neznámých	známý	k2eNgFnPc2d1	neznámá
uliček	ulička	k1gFnPc2	ulička
podvědomí	podvědomí	k1gNnSc2	podvědomí
<g/>
,	,	kIx,	,
našel	najít	k5eAaPmAgInS	najít
nesmírnou	smírný	k2eNgFnSc4d1	nesmírná
sílu	síla	k1gFnSc4	síla
symbolů	symbol	k1gInPc2	symbol
a	a	k8xC	a
kombinací	kombinace	k1gFnPc2	kombinace
barev	barva	k1gFnPc2	barva
svou	svůj	k3xOyFgFnSc7	svůj
tvořivostí	tvořivost	k1gFnSc7	tvořivost
<g/>
.	.	kIx.	.
</s>
<s>
Joan	Joan	k1gMnSc1	Joan
Miró	Miró	k1gMnSc1	Miró
zemřel	zemřít	k5eAaPmAgMnS	zemřít
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1983	[number]	k4	1983
v	v	k7c6	v
Palmě	palma	k1gFnSc6	palma
de	de	k?	de
Mallorca	Mallorcum	k1gNnSc2	Mallorcum
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
rodné	rodný	k2eAgFnSc6d1	rodná
Barceloně	Barcelona	k1gFnSc6	Barcelona
(	(	kIx(	(
<g/>
Montjuï	Montjuï	k1gFnSc6	Montjuï
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Miróova	Miróův	k2eAgNnPc4d1	Miróův
díla	dílo	k1gNnPc4	dílo
napsal	napsat	k5eAaPmAgInS	napsat
sbírku	sbírka	k1gFnSc4	sbírka
veršů	verš	k1gInPc2	verš
básník	básník	k1gMnSc1	básník
Rafael	Rafael	k1gMnSc1	Rafael
Alberti	Albert	k1gMnPc1	Albert
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Joan	Joana	k1gFnPc2	Joana
Miró	Miró	k1gFnSc2	Miró
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Joan	Joan	k1gInSc1	Joan
Miró	Miró	k1gFnSc4	Miró
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Joan	Joan	k1gMnSc1	Joan
Miró	Miró	k1gMnSc1	Miró
Joan	Joan	k1gMnSc1	Joan
Miro	Mira	k1gFnSc5	Mira
<g/>
:	:	kIx,	:
A	a	k9	a
Virtual	Virtual	k1gMnSc1	Virtual
Art	Art	k1gFnSc2	Art
Gallery	Galler	k1gInPc4	Galler
</s>
