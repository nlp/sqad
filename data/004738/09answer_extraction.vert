slovenský	slovenský	k2eAgMnSc1d1	slovenský
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
osobnost	osobnost	k1gFnSc1	osobnost
slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
signatář	signatář	k1gMnSc1	signatář
Martinské	martinský	k2eAgFnSc2d1	Martinská
deklarace	deklarace	k1gFnSc2	deklarace
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
osobnost	osobnost	k1gFnSc1	osobnost
Hlinkovy	Hlinkův	k2eAgFnSc2d1	Hlinkova
slovenské	slovenský	k2eAgFnSc2d1	slovenská
ľudové	ľudové	k2eAgFnSc2d1	ľudové
strany	strana	k1gFnSc2	strana
