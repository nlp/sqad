<s>
Andrej	Andrej	k1gMnSc1	Andrej
Hlinka	Hlinka	k1gMnSc1	Hlinka
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
Černová	Černová	k1gFnSc1	Černová
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
část	část	k1gFnSc1	část
Ružomberku	Ružomberk	k1gInSc2	Ružomberk
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Ružomberok	Ružomberok	k1gInSc1	Ružomberok
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
osobnost	osobnost	k1gFnSc1	osobnost
slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
signatář	signatář	k1gMnSc1	signatář
Martinské	martinský	k2eAgFnSc2d1	Martinská
deklarace	deklarace	k1gFnSc2	deklarace
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
osobnost	osobnost	k1gFnSc1	osobnost
Hlinkovy	Hlinkův	k2eAgFnSc2d1	Hlinkova
slovenské	slovenský	k2eAgFnSc2d1	slovenská
ľudové	ľudové	k2eAgFnSc2d1	ľudové
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Ľuďáků	Ľuďák	k1gInPc2	Ľuďák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlinka	Hlinka	k1gMnSc1	Hlinka
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
kněžský	kněžský	k2eAgInSc4d1	kněžský
seminář	seminář	k1gInSc4	seminář
při	při	k7c6	při
Spišské	spišský	k2eAgFnSc6d1	Spišská
kapitule	kapitula	k1gFnSc6	kapitula
<g/>
,	,	kIx,	,
na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
byl	být	k5eAaImAgMnS	být
vysvěcen	vysvěcen	k2eAgMnSc1d1	vysvěcen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
farářem	farář	k1gMnSc7	farář
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
Ružomberku	Ružomberk	k1gInSc6	Ružomberk
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c4	v
mládí	mládí	k1gNnSc4	mládí
začal	začít	k5eAaPmAgMnS	začít
politickou	politický	k2eAgFnSc4d1	politická
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
křesťanskodemokratické	křesťanskodemokratický	k2eAgFnSc2d1	Křesťanskodemokratická
Ľudové	Ľudové	k2eAgFnSc2d1	Ľudové
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
vedené	vedený	k2eAgNnSc1d1	vedené
hrabětem	hrabě	k1gMnSc7	hrabě
Zichym	Zichym	k1gInSc1	Zichym
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
opustil	opustit	k5eAaPmAgMnS	opustit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
principů	princip	k1gInPc2	princip
národnostní	národnostní	k2eAgFnSc2d1	národnostní
tolerance	tolerance	k1gFnSc2	tolerance
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
sa	sa	k?	sa
významným	významný	k2eAgMnSc7d1	významný
představitelem	představitel	k1gMnSc7	představitel
pravicového	pravicový	k2eAgNnSc2d1	pravicové
křídla	křídlo	k1gNnSc2	křídlo
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
ľudové	ľudové	k2eAgFnSc2d1	ľudové
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
redaktorem	redaktor	k1gMnSc7	redaktor
Ľudových	Ľudův	k2eAgFnPc2d1	Ľudův
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
založil	založit	k5eAaPmAgMnS	založit
Ľudovou	Ľudový	k2eAgFnSc4d1	Ľudová
banku	banka	k1gFnSc4	banka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
slovenské	slovenský	k2eAgFnSc2d1	slovenská
tiskárny	tiskárna	k1gFnSc2	tiskárna
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1906	[number]	k4	1906
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
proti	proti	k7c3	proti
národnostnímu	národnostní	k2eAgInSc3d1	národnostní
útlaku	útlak	k1gInSc3	útlak
v	v	k7c6	v
Uhersku	Uhersko	k1gNnSc6	Uhersko
a	a	k8xC	a
žádal	žádat	k5eAaImAgMnS	žádat
pro	pro	k7c4	pro
Slováky	Slovák	k1gMnPc4	Slovák
slovenské	slovenský	k2eAgFnSc2d1	slovenská
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
a	a	k8xC	a
polemizoval	polemizovat	k5eAaImAgMnS	polemizovat
s	s	k7c7	s
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
maďarským	maďarský	k2eAgMnSc7d1	maďarský
komunistickým	komunistický	k2eAgMnSc7d1	komunistický
vůdcem	vůdce	k1gMnSc7	vůdce
Bélou	Béla	k1gMnSc7	Béla
Kunem	Kunem	k?	Kunem
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
a	a	k8xC	a
překládal	překládat	k5eAaImAgInS	překládat
teologická	teologický	k2eAgNnPc4d1	teologické
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
Hlinka	Hlinka	k1gMnSc1	Hlinka
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
nově	nově	k6eAd1	nově
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
počátku	počátek	k1gInSc6	počátek
byl	být	k5eAaImAgInS	být
nakloněn	nakloněn	k2eAgInSc1d1	nakloněn
myšlence	myšlenka	k1gFnSc3	myšlenka
jednotného	jednotný	k2eAgInSc2d1	jednotný
"	"	kIx"	"
<g/>
československého	československý	k2eAgInSc2d1	československý
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
ľudové	ľudové	k2eAgFnSc2d1	ľudové
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
delegace	delegace	k1gFnSc2	delegace
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1919	[number]	k4	1919
působila	působit	k5eAaImAgFnS	působit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
na	na	k7c6	na
Versailleské	versailleský	k2eAgFnSc6d1	Versailleská
konferenci	konference	k1gFnSc6	konference
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žádal	žádat	k5eAaImAgMnS	žádat
autonomii	autonomie	k1gFnSc4	autonomie
pro	pro	k7c4	pro
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
konferenci	konference	k1gFnSc4	konference
však	však	k9	však
nebyl	být	k5eNaImAgMnS	být
Hlinka	Hlinka	k1gMnSc1	Hlinka
vůbec	vůbec	k9	vůbec
vpuštěn	vpuštěn	k2eAgMnSc1d1	vpuštěn
a	a	k8xC	a
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
byl	být	k5eAaImAgMnS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
uvěznění	uvěznění	k1gNnSc2	uvěznění
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
cestoval	cestovat	k5eAaImAgMnS	cestovat
do	do	k7c2	do
Versailles	Versailles	k1gFnSc2	Versailles
na	na	k7c4	na
falešný	falešný	k2eAgInSc4d1	falešný
pas	pas	k1gInSc4	pas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
papežským	papežský	k2eAgMnSc7d1	papežský
komořím	komoří	k1gMnSc7	komoří
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
apoštolským	apoštolský	k2eAgMnSc7d1	apoštolský
protonotářem	protonotář	k1gMnSc7	protonotář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
strana	strana	k1gFnSc1	strana
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Hlinkovu	Hlinkův	k2eAgFnSc4d1	Hlinkova
slovenskú	slovenskú	k?	slovenskú
ľudovú	ľudovú	k?	ľudovú
stranu	strana	k1gFnSc4	strana
(	(	kIx(	(
<g/>
HSĽS	HSĽS	kA	HSĽS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
byl	být	k5eAaImAgInS	být
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
lídrem	lídr	k1gMnSc7	lídr
a	a	k8xC	a
hlavním	hlavní	k2eAgMnSc7d1	hlavní
ideologem	ideolog	k1gMnSc7	ideolog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
až	až	k9	až
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
vůdce	vůdce	k1gMnSc4	vůdce
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
frakce	frakce	k1gFnSc2	frakce
SĽS	SĽS	kA	SĽS
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
HSĽS	HSĽS	kA	HSĽS
v	v	k7c6	v
československém	československý	k2eAgNnSc6d1	Československé
Národním	národní	k2eAgNnSc6d1	národní
shromáždění	shromáždění	k1gNnSc6	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
na	na	k7c6	na
spolupráci	spolupráce	k1gFnSc6	spolupráce
mezi	mezi	k7c7	mezi
HSĽS	HSĽS	kA	HSĽS
<g/>
,	,	kIx,	,
Konradem	Konrad	k1gInSc7	Konrad
Henleinem	Henlein	k1gInSc7	Henlein
a	a	k8xC	a
politickým	politický	k2eAgMnSc7d1	politický
vůdcem	vůdce	k1gMnSc7	vůdce
maďarské	maďarský	k2eAgFnSc2d1	maďarská
menšiny	menšina	k1gFnSc2	menšina
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
hrabětem	hrabě	k1gMnSc7	hrabě
Esterházym	Esterházym	k1gInSc4	Esterházym
<g/>
.	.	kIx.	.
</s>
<s>
Ideologický	ideologický	k2eAgInSc1d1	ideologický
základ	základ	k1gInSc1	základ
Hlinkova	Hlinkův	k2eAgNnSc2d1	Hlinkovo
politického	politický	k2eAgNnSc2d1	politické
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
a	a	k8xC	a
politiky	politika	k1gFnSc2	politika
tvořilo	tvořit	k5eAaImAgNnS	tvořit
hluboké	hluboký	k2eAgNnSc1d1	hluboké
vlastenecké	vlastenecký	k2eAgNnSc1d1	vlastenecké
cítění	cítění	k1gNnSc1	cítění
(	(	kIx(	(
<g/>
zvýrazněné	zvýrazněný	k2eAgNnSc1d1	zvýrazněné
nejprve	nejprve	k6eAd1	nejprve
maďarskou	maďarský	k2eAgFnSc7d1	maďarská
nadvládou	nadvláda	k1gFnSc7	nadvláda
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
zřetelně	zřetelně	k6eAd1	zřetelně
slabším	slabý	k2eAgNnSc7d2	slabší
postavením	postavení	k1gNnSc7	postavení
Slováků	Slováky	k1gInPc2	Slováky
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klerikalismus	klerikalismus	k1gInSc1	klerikalismus
a	a	k8xC	a
antikomunismus	antikomunismus	k1gInSc1	antikomunismus
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
byl	být	k5eAaImAgMnS	být
odpůrcem	odpůrce	k1gMnSc7	odpůrce
útlaku	útlak	k1gInSc2	útlak
Slováků	Slovák	k1gMnPc2	Slovák
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
,	,	kIx,	,
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
odpůrcem	odpůrce	k1gMnSc7	odpůrce
"	"	kIx"	"
<g/>
pražské	pražský	k2eAgFnSc2d1	Pražská
centralizace	centralizace	k1gFnSc2	centralizace
<g/>
"	"	kIx"	"
a	a	k8xC	a
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinu	většina	k1gFnSc4	většina
moci	moc	k1gFnSc2	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
soustředili	soustředit	k5eAaPmAgMnP	soustředit
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
Češi	česat	k5eAaImIp1nS	česat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Hlinkově	Hlinkův	k2eAgFnSc6d1	Hlinkova
smrti	smrt	k1gFnSc6	smrt
stanul	stanout	k5eAaPmAgMnS	stanout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
jeho	jeho	k3xOp3gFnSc2	jeho
strany	strana	k1gFnSc2	strana
Jozef	Jozef	k1gMnSc1	Jozef
Tiso	Tisa	k1gFnSc5	Tisa
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
postupně	postupně	k6eAd1	postupně
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
situaci	situace	k1gFnSc4	situace
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Slovenského	slovenský	k2eAgInSc2d1	slovenský
štátu	štát	k1gInSc2	štát
převzala	převzít	k5eAaPmAgFnS	převzít
veškerou	veškerý	k3xTgFnSc4	veškerý
moc	moc	k1gFnSc4	moc
(	(	kIx(	(
<g/>
teoreticky	teoreticky	k6eAd1	teoreticky
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gFnSc4	její
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
moc	moc	k1gFnSc4	moc
jejích	její	k3xOp3gMnPc2	její
představitelů	představitel	k1gMnPc2	představitel
omezena	omezit	k5eAaPmNgFnS	omezit
přáními	přání	k1gNnPc7	přání
Německa	Německo	k1gNnSc2	Německo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
ve	v	k7c4	v
skutečně	skutečně	k6eAd1	skutečně
masovou	masový	k2eAgFnSc4d1	masová
a	a	k8xC	a
z	z	k7c2	z
Hlinky	hlinka	k1gFnSc2	hlinka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kultovní	kultovní	k2eAgFnSc1d1	kultovní
osobnost	osobnost	k1gFnSc1	osobnost
nového	nový	k2eAgInSc2d1	nový
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Odvolávaly	odvolávat	k5eAaImAgFnP	odvolávat
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
i	i	k9	i
nově	nově	k6eAd1	nově
založené	založený	k2eAgFnSc2d1	založená
organizace	organizace	k1gFnSc2	organizace
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
garda	garda	k1gFnSc1	garda
(	(	kIx(	(
<g/>
napodobenina	napodobenina	k1gFnSc1	napodobenina
německých	německý	k2eAgFnPc2d1	německá
Sturmabteilung	Sturmabteilunga	k1gFnPc2	Sturmabteilunga
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hlinkova	Hlinkův	k2eAgFnSc1d1	Hlinkova
mládež	mládež	k1gFnSc1	mládež
(	(	kIx(	(
<g/>
napodobenina	napodobenina	k1gFnSc1	napodobenina
německých	německý	k2eAgFnPc2d1	německá
Hitlerjugend	Hitlerjugenda	k1gFnPc2	Hitlerjugenda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1939	[number]	k4	1939
přijal	přijmout	k5eAaPmAgInS	přijmout
Slovenský	slovenský	k2eAgInSc1d1	slovenský
sněm	sněm	k1gInSc1	sněm
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
zásluhách	zásluha	k1gFnPc6	zásluha
Andreje	Andrej	k1gMnSc2	Andrej
Hlinky	Hlinka	k1gMnSc2	Hlinka
(	(	kIx(	(
<g/>
83	[number]	k4	83
<g/>
/	/	kIx~	/
<g/>
1939	[number]	k4	1939
Sl.	Sl.	k1gFnSc2	Sl.
<g/>
z.	z.	k?	z.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obnovení	obnovení	k1gNnSc6	obnovení
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
a	a	k8xC	a
příchodu	příchod	k1gInSc2	příchod
komunistů	komunista	k1gMnPc2	komunista
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Hlinka	Hlinka	k1gMnSc1	Hlinka
jakožto	jakožto	k8xS	jakožto
antikomunista	antikomunista	k1gMnSc1	antikomunista
<g/>
,	,	kIx,	,
klerikalista	klerikalista	k1gMnSc1	klerikalista
a	a	k8xC	a
odpůrce	odpůrce	k1gMnSc1	odpůrce
pragocentrismu	pragocentrismus	k1gInSc2	pragocentrismus
a	a	k8xC	a
centralizace	centralizace	k1gFnSc2	centralizace
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
komunisty	komunista	k1gMnPc4	komunista
označen	označit	k5eAaPmNgMnS	označit
jako	jako	k9	jako
fašista	fašista	k1gMnSc1	fašista
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
negativní	negativní	k2eAgInSc1d1	negativní
obraz	obraz	k1gInSc1	obraz
některými	některý	k3yIgFnPc7	některý
změněn	změnit	k5eAaPmNgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Slovak	Slovak	k1gInSc4	Slovak
National	National	k1gFnSc2	National
Biography	Biographa	k1gFnPc4	Biographa
uveřejněné	uveřejněný	k2eAgFnPc4d1	uveřejněná
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
napsané	napsaný	k2eAgInPc1d1	napsaný
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
one	one	k?	one
of	of	k?	of
the	the	k?	the
most	most	k1gInSc1	most
significant	significant	k1gMnSc1	significant
personalities	personalities	k1gMnSc1	personalities
in	in	k?	in
modern	modern	k1gMnSc1	modern
Slovak	Slovak	k1gMnSc1	Slovak
history	histor	k1gInPc4	histor
<g/>
,	,	kIx,	,
a	a	k8xC	a
nationalistic	nationalistice	k1gFnPc2	nationalistice
Christian	Christian	k1gMnSc1	Christian
politician	politician	k1gMnSc1	politician
and	and	k?	and
representative	representativ	k1gInSc5	representativ
of	of	k?	of
Slovak	Slovak	k1gInSc1	Slovak
autonomic	autonomic	k1gMnSc1	autonomic
efforts	efforts	k1gInSc1	efforts
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
osobností	osobnost	k1gFnPc2	osobnost
moderní	moderní	k2eAgFnSc2d1	moderní
slovenské	slovenský	k2eAgFnSc2d1	slovenská
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
nacionalistický	nacionalistický	k2eAgMnSc1d1	nacionalistický
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
představitel	představitel	k1gMnSc1	představitel
slovenských	slovenský	k2eAgFnPc2d1	slovenská
autonomistických	autonomistický	k2eAgFnPc2d1	autonomistická
snah	snaha	k1gFnPc2	snaha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvář	tvář	k1gFnSc1	tvář
byla	být	k5eAaImAgFnS	být
vyobrazena	vyobrazit	k5eAaPmNgFnS	vyobrazit
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
slovenské	slovenský	k2eAgFnSc6d1	slovenská
tisícikorunové	tisícikorunový	k2eAgFnSc6d1	tisícikorunová
bankovce	bankovka	k1gFnSc6	bankovka
před	před	k7c7	před
zavedením	zavedení	k1gNnSc7	zavedení
eura	euro	k1gNnSc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
pak	pak	k6eAd1	pak
Národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
přijala	přijmout	k5eAaPmAgFnS	přijmout
zákon	zákon	k1gInSc4	zákon
č.	č.	k?	č.
531	[number]	k4	531
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
Z.z.	Z.z.	k1gFnPc2	Z.z.
<g/>
,	,	kIx,	,
o	o	k7c6	o
zásluhách	zásluha	k1gFnPc6	zásluha
Andreje	Andrej	k1gMnSc2	Andrej
Hlinky	Hlinka	k1gMnSc2	Hlinka
o	o	k7c4	o
státotvorný	státotvorný	k2eAgInSc4d1	státotvorný
slovenský	slovenský	k2eAgInSc4d1	slovenský
národ	národ	k1gInSc4	národ
a	a	k8xC	a
o	o	k7c4	o
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
slovenský	slovenský	k2eAgInSc1d1	slovenský
Řád	řád	k1gInSc1	řád
Andreje	Andrej	k1gMnSc2	Andrej
Hlinky	Hlinka	k1gMnSc2	Hlinka
<g/>
.	.	kIx.	.
</s>
