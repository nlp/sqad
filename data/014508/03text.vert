<s>
Corpus	corpus	k1gInSc1
iuris	iuris	k1gFnSc2
civilis	civilis	k1gFnSc2
</s>
<s>
Corpus	corpus	k1gInPc1
iuris	iuris	k1gFnSc1
civilis	civilis	k1gInSc1
Codex	Codex	k1gInSc1
Iustinianus	Iustinianus	k1gInSc4
<g/>
,	,	kIx,
Lyon	Lyon	k1gInSc4
1556	#num#	k4
<g/>
Autor	autor	k1gMnSc1
</s>
<s>
Justinián	Justinián	k1gMnSc1
I.	I.	kA
Byzantský	byzantský	k2eAgInSc1d1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Corpus	corpus	k1gInSc1
Iuris	Iuris	k1gFnSc2
Civilis	Civilis	k1gFnSc2
Jazyk	jazyk	k1gInSc1
</s>
<s>
latina	latina	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Corpus	corpus	k1gInSc1
iuris	iuris	k1gFnSc2
civilis	civilis	k1gFnSc2
představuje	představovat	k5eAaImIp3nS
kodifikaci	kodifikace	k1gFnSc4
římského	římský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
provedenou	provedený	k2eAgFnSc4d1
na	na	k7c4
podnět	podnět	k1gInSc4
byzantského	byzantský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Justiniána	Justinián	k1gMnSc2
I.	I.	kA
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
významný	významný	k2eAgInSc4d1
právní	právní	k2eAgInSc4d1
počin	počin	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
provedla	provést	k5eAaPmAgFnS
komise	komise	k1gFnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
vynikajícím	vynikající	k2eAgMnSc7d1
právníkem	právník	k1gMnSc7
Tribonianem	Tribonian	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Obsah	obsah	k1gInSc1
právního	právní	k2eAgInSc2d1
kodexu	kodex	k1gInSc2
</s>
<s>
Občanský	občanský	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
nazývaný	nazývaný	k2eAgInSc1d1
Corpus	corpus	k1gInSc1
iuris	iuris	k1gFnSc2
civilis	civilis	k1gFnSc2
se	se	k3xPyFc4
skládal	skládat	k5eAaImAgMnS
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
první	první	k4xOgMnSc1
byl	být	k5eAaImAgInS
zveřejněn	zveřejněn	k2eAgInSc1d1
Codex	Codex	k1gInSc1
Iustinianus	Iustinianus	k1gInSc1
roku	rok	k1gInSc2
529	#num#	k4
<g/>
,	,	kIx,
soubor	soubor	k1gInSc1
dosud	dosud	k6eAd1
platných	platný	k2eAgNnPc2d1
císařských	císařský	k2eAgNnPc2d1
nařízení	nařízení	k1gNnPc2
a	a	k8xC
zákonů	zákon	k1gInPc2
od	od	k7c2
doby	doba	k1gFnSc2
císaře	císař	k1gMnSc2
Hadriána	Hadrián	k1gMnSc2
(	(	kIx(
<g/>
76	#num#	k4
–	–	k?
<g/>
138	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
spis	spis	k1gInSc1
měl	mít	k5eAaImAgInS
nahradit	nahradit	k5eAaPmF
starší	starý	k2eAgFnPc4d2
právní	právní	k2eAgFnPc4d1
sbírky	sbírka	k1gFnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
Codex	Codex	k1gInSc1
Theodosianus	Theodosianus	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
438	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Druhou	druhý	k4xOgFnSc7
součástí	součást	k1gFnSc7
byla	být	k5eAaImAgFnS
Digesta	Digesta	k1gFnSc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Pandektai	Pandektai	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sbírka	sbírka	k1gFnSc1
spisů	spis	k1gInPc2
antických	antický	k2eAgMnPc2d1
římských	římský	k2eAgMnPc2d1
právníků	právník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP
Institutiones	Institutiones	k1gInSc4
<g/>
,	,	kIx,
stručná	stručný	k2eAgFnSc1d1
rukověť	rukověť	k1gFnSc1
římského	římský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgNnSc1d1
právní	právní	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
dovršily	dovršit	k5eAaPmAgInP
Narai	Nara	k1gFnSc2
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
Novellae	Novella	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aktuální	aktuální	k2eAgFnPc4d1
císařská	císařský	k2eAgNnPc4d1
nařízení	nařízení	k1gNnPc4
vydaná	vydaný	k2eAgNnPc4d1
v	v	k7c6
letech	let	k1gInPc6
534	#num#	k4
<g/>
–	–	k?
<g/>
569	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
předchozích	předchozí	k2eAgInPc2d1
latinských	latinský	k2eAgInPc2d1
spisů	spis	k1gInPc2
byla	být	k5eAaImAgFnS
jazykem	jazyk	k1gInSc7
novel	novela	k1gFnPc2
už	už	k9
většinou	většinou	k6eAd1
řečtina	řečtina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
převládnutí	převládnutí	k1gNnSc6
řečtiny	řečtina	k1gFnSc2
na	na	k7c6
byzantském	byzantský	k2eAgInSc6d1
dvoře	dvůr	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Corpus	corpus	k1gInSc1
iuris	iuris	k1gInSc1
civilis	civilis	k1gInSc4
uzákonil	uzákonit	k5eAaPmAgInS
současné	současný	k2eAgFnPc4d1
normy	norma	k1gFnPc4
společenského	společenský	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jeho	jeho	k3xOp3gNnPc2
ustanovení	ustanovení	k1gNnPc2
se	se	k3xPyFc4
v	v	k7c6
osobě	osoba	k1gFnSc6
císaře	císař	k1gMnSc4
pojila	pojit	k5eAaImAgFnS
neomezená	omezený	k2eNgFnSc1d1
moc	moc	k1gFnSc1
světská	světský	k2eAgFnSc1d1
i	i	k9
duchovní	duchovní	k1gMnPc4
(	(	kIx(
<g/>
tento	tento	k3xDgInSc1
systém	systém	k1gInSc1
se	se	k3xPyFc4
nazýval	nazývat	k5eAaImAgInS
caesaropapismus	caesaropapismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiálním	oficiální	k2eAgNnSc7d1
náboženstvím	náboženství	k1gNnSc7
bylo	být	k5eAaImAgNnS
křesťanství	křesťanství	k1gNnSc1
v	v	k7c6
ortodoxní	ortodoxní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Justinián	Justinián	k1gMnSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
jak	jak	k6eAd1
vymýtit	vymýtit	k5eAaPmF
staré	starý	k2eAgInPc4d1
pohanské	pohanský	k2eAgInPc4d1
kulty	kult	k1gInPc4
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
529	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
zveřejněn	zveřejnit	k5eAaPmNgInS
Codex	Codex	k1gInSc1
Iustinianus	Iustinianus	k1gInSc1
<g/>
,	,	kIx,
dal	dát	k5eAaPmAgInS
například	například	k6eAd1
uzavřít	uzavřít	k5eAaPmF
starou	starý	k2eAgFnSc4d1
antickou	antický	k2eAgFnSc4d1
akademii	akademie	k1gFnSc4
v	v	k7c6
Athénách	Athéna	k1gFnPc6
<g/>
,	,	kIx,
centrum	centrum	k1gNnSc1
novoplatonismu	novoplatonismus	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
také	také	k9
zabránit	zabránit	k5eAaPmF
šíření	šíření	k1gNnSc4
herezí	hereze	k1gFnPc2
<g/>
,	,	kIx,
především	především	k9
monofyzitismu	monofyzitismus	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
mělo	mít	k5eAaImAgNnS
sociální	sociální	k2eAgInSc4d1
i	i	k8xC
politický	politický	k2eAgInSc4d1
podtext	podtext	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2
svých	svůj	k3xOyFgMnPc2
zákoníků	zákoník	k1gMnPc2
císař	císař	k1gMnSc1
Justinián	Justinián	k1gMnSc1
I.	I.	kA
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c6
upevnění	upevnění	k1gNnSc6
práv	právo	k1gNnPc2
staré	starý	k2eAgFnSc2d1
otrokářské	otrokářský	k2eAgFnSc2d1
aristokracie	aristokracie	k1gFnSc2
a	a	k8xC
podporu	podpora	k1gFnSc4
hospodářství	hospodářství	k1gNnSc2
založeného	založený	k2eAgNnSc2d1
na	na	k7c6
práci	práce	k1gFnSc6
otroků	otrok	k1gMnPc2
a	a	k8xC
kolonů	kolon	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
hospodářský	hospodářský	k2eAgInSc1d1
systém	systém	k1gInSc1
se	se	k3xPyFc4
však	však	k9
již	již	k6eAd1
začal	začít	k5eAaPmAgMnS
přežívat	přežívat	k5eAaImF
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
dokazuje	dokazovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
propouštět	propouštět	k5eAaImF
otroky	otrok	k1gMnPc4
na	na	k7c4
svobodu	svoboda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
uzákoněna	uzákonit	k5eAaPmNgFnS
v	v	k7c6
Novelách	novela	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Corpus	corpus	k1gInSc1
iuris	iuris	k1gFnSc2
civilis	civilis	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
význam	význam	k1gInSc1
ve	v	k7c6
středověku	středověk	k1gInSc6
</s>
<s>
Corpus	corpus	k1gInSc4
iuris	iuris	k1gInSc1
civilis	civilis	k1gInSc1
platil	platit	k5eAaImAgInS
po	po	k7c4
řadu	řada	k1gFnSc4
staletí	staletí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
si	se	k3xPyFc3
však	však	k9
změny	změna	k1gFnPc1
ve	v	k7c6
společnosti	společnost	k1gFnSc6
směřující	směřující	k2eAgFnSc6d1
k	k	k7c3
feudalizaci	feudalizace	k1gFnSc3
vynutily	vynutit	k5eAaPmAgInP
jak	jak	k8xC,k8xS
revizi	revize	k1gFnSc3
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
ustanovení	ustanovení	k1gNnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
nechali	nechat	k5eAaPmAgMnP
pod	pod	k7c7
názvem	název	k1gInSc7
Basilika	basilika	k1gFnSc1
provést	provést	k5eAaPmF
císaři	císař	k1gMnSc3
Basileios	Basileios	k1gInSc4
I.	I.	kA
a	a	k8xC
Leon	Leona	k1gFnPc2
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
vydání	vydání	k1gNnSc1
nových	nový	k2eAgInPc2d1
zákoníků	zákoník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
Justiniánovy	Justiniánův	k2eAgInPc1d1
zákoníky	zákoník	k1gInPc1
začaly	začít	k5eAaPmAgInP
studovat	studovat	k5eAaImF
na	na	k7c6
právnických	právnický	k2eAgFnPc6d1
školách	škola	k1gFnPc6
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
na	na	k7c6
boloňské	boloňský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
staly	stát	k5eAaPmAgInP
se	s	k7c7
základem	základ	k1gInSc7
přijetí	přijetí	k1gNnSc2
římského	římský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
ve	v	k7c6
středověku	středověk	k1gInSc6
a	a	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
dalšího	další	k2eAgNnSc2d1
šíření	šíření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MOMMSEN	MOMMSEN	kA
<g/>
,	,	kIx,
Theodor	Theodor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digesta	Digesta	k1gMnSc1
Iustiniani	Iustinian	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Nugis	Nugis	k1gFnSc1
Finem	Fin	k1gMnSc7
Publishing	Publishing	k1gInSc4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
887	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
906685	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
latina	latina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Corpus	corpus	k1gInSc2
iuris	iuris	k1gFnSc2
civilis	civilis	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Corpus	corpus	k1gInSc2
iuris	iuris	k1gFnSc2
civilis	civilis	k1gFnPc2
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
<g/>
)	)	kIx)
Corpus	corpus	k1gInSc1
iuris	iuris	k1gFnSc2
civilis	civilis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ed	Ed	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mommsen	Mommsen	k1gInSc1
<g/>
,	,	kIx,
Krueger	Krueger	k1gMnSc1
<g/>
,	,	kIx,
Schoell	Schoell	k1gMnSc1
&	&	k?
Kroll	Kroll	k1gMnSc1
The	The	k1gMnSc1
Roman	Roman	k1gMnSc1
Law	Law	k1gMnSc1
Library	Librara	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Anglický	anglický	k2eAgInSc1d1
překlad	překlad	k1gInSc1
CIC	cic	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
unn	unn	k?
<g/>
2008451137	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4113244-0	4113244-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80104777	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
185086370	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80104777	#num#	k4
</s>
