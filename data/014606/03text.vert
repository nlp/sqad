<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
doplnit	doplnit	k5eAaPmF
či	či	k8xC
upravit	upravit	k5eAaPmF
literaturu	literatura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
konec	konec	k1gInSc4
článku	článek	k1gInSc2
přidáte	přidat	k5eAaPmIp2nP
(	(	kIx(
<g/>
resp.	resp.	kA
upravíte	upravit	k5eAaPmIp2nP
<g/>
)	)	kIx)
literaturu	literatura	k1gFnSc4
a	a	k8xC
uvedete	uvést	k5eAaPmIp2nP
vhodné	vhodný	k2eAgFnPc4d1
knihy	kniha	k1gFnPc4
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
lze	lze	k6eAd1
o	o	k7c6
daném	daný	k2eAgNnSc6d1
tématu	téma	k1gNnSc6
čerpat	čerpat	k5eAaImF
více	hodně	k6eAd2
informací	informace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
(	(	kIx(
<g/>
značí	značit	k5eAaImIp3nS
se	se	k3xPyFc4
n	n	k0
nebo	nebo	k8xC
N	N	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bezrozměrná	bezrozměrný	k2eAgFnSc1d1
fyzikální	fyzikální	k2eAgFnSc1d1
veličina	veličina	k1gFnSc1
popisující	popisující	k2eAgNnSc4d1
šíření	šíření	k1gNnSc4
světla	světlo	k1gNnSc2
a	a	k8xC
všeobecně	všeobecně	k6eAd1
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
záření	záření	k1gNnSc2
v	v	k7c6
látkách	látka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
základní	základní	k2eAgNnPc4d1
využití	využití	k1gNnPc4
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
modelování	modelování	k1gNnSc6
lomu	lom	k1gInSc2
světla	světlo	k1gNnSc2
na	na	k7c4
rozhraní	rozhraní	k1gNnSc4
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
se	se	k3xPyFc4
světlo	světlo	k1gNnSc1
šíří	šířit	k5eAaImIp3nS
různou	různý	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
(	(	kIx(
<g/>
Snellův	Snellův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
k	k	k7c3
výpočtům	výpočet	k1gInPc3
míry	míra	k1gFnSc2
odrazu	odraz	k1gInSc2
a	a	k8xC
průchodu	průchod	k1gInSc2
světla	světlo	k1gNnSc2
ve	v	k7c6
Fresnelových	Fresnelův	k2eAgFnPc6d1
rovnicích	rovnice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
jako	jako	k8xS,k8xC
konstanta	konstanta	k1gFnSc1
</s>
<s>
V	v	k7c6
nejjednodušším	jednoduchý	k2eAgInSc6d3
případě	případ	k1gInSc6
–	–	k?
pro	pro	k7c4
průhledné	průhledný	k2eAgFnPc4d1
a	a	k8xC
čiré	čirý	k2eAgFnPc4d1
látky	látka	k1gFnPc4
–	–	k?
lze	lze	k6eAd1
index	index	k1gInSc1
lomu	lom	k1gInSc2
n	n	k0
považovat	považovat	k5eAaImF
za	za	k7c4
konstantu	konstanta	k1gFnSc4
<g/>
,	,	kIx,
vztahující	vztahující	k2eAgFnPc4d1
se	se	k3xPyFc4
k	k	k7c3
celému	celý	k2eAgInSc3d1
rozsahu	rozsah	k1gInSc3
viditelného	viditelný	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tom	ten	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
index	index	k1gInSc1
lomu	lom	k1gInSc2
vždy	vždy	k6eAd1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
1	#num#	k4
a	a	k8xC
rychlost	rychlost	k1gFnSc4
šíření	šíření	k1gNnSc2
světla	světlo	k1gNnSc2
v	v	k7c6
dané	daný	k2eAgFnSc6d1
látce	látka	k1gFnSc6
v	v	k7c6
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
vztahem	vztah	k1gInSc7
</s>
<s>
v	v	k7c6
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
v	v	k7c6
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
c	c	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}}}	}}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k9
c	c	k0
je	být	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc4
světla	světlo	k1gNnSc2
ve	v	k7c6
vakuu	vakuum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
definovaný	definovaný	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
absolutní	absolutní	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
přechod	přechod	k1gInSc4
z	z	k7c2
prostředí	prostředí	k1gNnSc2
s	s	k7c7
indexem	index	k1gInSc7
lomu	lom	k1gInSc2
</s>
<s>
n	n	k0
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
do	do	k7c2
prostředí	prostředí	k1gNnSc2
s	s	k7c7
indexem	index	k1gInSc7
lomu	lom	k1gInSc2
</s>
<s>
n	n	k0
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
se	se	k3xPyFc4
často	často	k6eAd1
používá	používat	k5eAaImIp3nS
relativní	relativní	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
n	n	k0
</s>
<s>
21	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
21	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
definován	definovat	k5eAaBmNgInS
jako	jako	k9
</s>
<s>
n	n	k0
</s>
<s>
21	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
n	n	k0
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
21	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
n_	n_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
n_	n_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
Pro	pro	k7c4
přechod	přechod	k1gInSc4
vlnění	vlnění	k1gNnSc2
opačným	opačný	k2eAgInSc7d1
směrem	směr	k1gInSc7
je	být	k5eAaImIp3nS
index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
n	n	k0
</s>
<s>
12	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
21	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
12	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gInSc1
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
n_	n_	k?
<g/>
{	{	kIx(
<g/>
21	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
Pomocí	pomocí	k7c2
relativního	relativní	k2eAgInSc2d1
indexu	index	k1gInSc2
lomu	lom	k1gInSc2
lze	lze	k6eAd1
psát	psát	k5eAaImF
</s>
<s>
n	n	k0
</s>
<s>
21	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
v	v	k7c6
</s>
<s>
1	#num#	k4
</s>
<s>
v	v	k7c6
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
21	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
v_	v_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
v_	v_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
kde	kde	k6eAd1
</s>
<s>
v	v	k7c6
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
v_	v_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc4
šíření	šíření	k1gNnSc2
vln	vlna	k1gFnPc2
v	v	k7c6
prvním	první	k4xOgNnSc6
prostředí	prostředí	k1gNnSc6
(	(	kIx(
<g/>
s	s	k7c7
indexem	index	k1gInSc7
lomu	lom	k1gInSc2
</s>
<s>
n	n	k0
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
)	)	kIx)
a	a	k8xC
</s>
<s>
v	v	k7c6
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
v_	v_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc4
šíření	šíření	k1gNnSc3
ve	v	k7c6
druhém	druhý	k4xOgNnSc6
prostředí	prostředí	k1gNnSc6
(	(	kIx(
<g/>
s	s	k7c7
indexem	index	k1gInSc7
lomu	lom	k1gInSc2
</s>
<s>
n	n	k0
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
rovinném	rovinný	k2eAgNnSc6d1
rozhraní	rozhraní	k1gNnSc6
dvou	dva	k4xCgFnPc2
látek	látka	k1gFnPc2
s	s	k7c7
různými	různý	k2eAgInPc7d1
indexy	index	k1gInPc7
lomu	lom	k1gInSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
lomu	lom	k1gInSc2
světla	světlo	k1gNnSc2
dle	dle	k7c2
Snellova	Snellův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
sin	sin	kA
</s>
<s>
α	α	k?
</s>
<s>
sin	sin	kA
</s>
<s>
β	β	k?
</s>
<s>
=	=	kIx~
</s>
<s>
v	v	k7c6
</s>
<s>
1	#num#	k4
</s>
<s>
v	v	k7c6
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
n	n	k0
</s>
<s>
2	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sin	sin	kA
\	\	kIx~
<g/>
alpha	alpha	k1gMnSc1
}	}	kIx)
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
sin	sin	kA
\	\	kIx~
<g/>
beta	beta	k1gNnSc1
}}	}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
v_	v_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
v_	v_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}}	}}}	k?
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
n_	n_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
<g/>
{	{	kIx(
<g/>
n_	n_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}}}	}}}}	k?
</s>
<s>
kde	kde	k6eAd1
α	α	k?
je	být	k5eAaImIp3nS
úhel	úhel	k1gInSc4
svíraný	svíraný	k2eAgInSc4d1
daným	daný	k2eAgInSc7d1
proudem	proud	k1gInSc7
světla	světlo	k1gNnSc2
a	a	k8xC
kolmicí	kolmice	k1gFnSc7
na	na	k7c4
rovinu	rovina	k1gFnSc4
dopadu	dopad	k1gInSc2
na	na	k7c4
materiál	materiál	k1gInSc4
a	a	k8xC
β	β	k?
je	být	k5eAaImIp3nS
úhel	úhel	k1gInSc4
svíraný	svíraný	k2eAgInSc4d1
proudem	proud	k1gInSc7
světla	světlo	k1gNnSc2
a	a	k8xC
kolmicí	kolmice	k1gFnSc7
na	na	k7c4
rovinu	rovina	k1gFnSc4
dopadu	dopad	k1gInSc2
ze	z	k7c2
strany	strana	k1gFnSc2
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
se	se	k3xPyFc4
dané	daný	k2eAgNnSc1d1
světlo	světlo	k1gNnSc1
láme	lámat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Absolutní	absolutní	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
některých	některý	k3yIgFnPc2
látek	látka	k1gFnPc2
je	být	k5eAaImIp3nS
uveden	uvést	k5eAaPmNgInS
v	v	k7c6
následující	následující	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Látkaindex	Látkaindex	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
vakuum	vakuum	k1gNnSc1
<g/>
1	#num#	k4
</s>
<s>
vzduch	vzduch	k1gInSc1
(	(	kIx(
<g/>
normální	normální	k2eAgInSc1d1
tlak	tlak	k1gInSc1
<g/>
)	)	kIx)
<g/>
1,000	1,000	k4
<g/>
26	#num#	k4
</s>
<s>
led	led	k1gInSc1
<g/>
1,31	1,31	k4
</s>
<s>
voda	voda	k1gFnSc1
<g/>
1,33	1,33	k4
</s>
<s>
etanol	etanol	k1gInSc1
<g/>
1,36	1,36	k4
</s>
<s>
glycerol	glycerol	k1gInSc1
<g/>
1,473	1,473	k4
</s>
<s>
sklo	sklo	k1gNnSc1
<g/>
1,5	1,5	k4
až	až	k8xS
1,9	1,9	k4
</s>
<s>
sůl	sůl	k1gFnSc1
<g/>
1,52	1,52	k4
</s>
<s>
safír	safír	k1gInSc1
<g/>
1,77	1,77	k4
</s>
<s>
diamant	diamant	k1gInSc1
<g/>
2,42	2,42	k4
</s>
<s>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Máme	mít	k5eAaImIp1nP
<g/>
-li	-li	k?
dvě	dva	k4xCgNnPc4
prostředí	prostředí	k1gNnPc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
prostředí	prostředí	k1gNnSc4
s	s	k7c7
větším	veliký	k2eAgInSc7d2
absolutním	absolutní	k2eAgInSc7d1
indexem	index	k1gInSc7
lomu	lom	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
opticky	opticky	k6eAd1
hustší	hustý	k2eAgFnSc1d2
<g/>
,	,	kIx,
a	a	k8xC
prostředí	prostředí	k1gNnSc4
s	s	k7c7
menším	malý	k2eAgInSc7d2
absolutním	absolutní	k2eAgInSc7d1
indexem	index	k1gInSc7
lomu	lom	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
opticky	opticky	k6eAd1
řidší	řídký	k2eAgNnSc4d2
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
přechodu	přechod	k1gInSc6
z	z	k7c2
opticky	opticky	k6eAd1
hustšího	hustý	k2eAgNnSc2d2
prostředí	prostředí	k1gNnSc2
do	do	k7c2
prostředí	prostředí	k1gNnSc2
opticky	opticky	k6eAd1
řidšího	řídký	k2eAgNnSc2d2
je	být	k5eAaImIp3nS
relativní	relativní	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
menší	malý	k2eAgFnSc1d2
než	než	k8xS
jedna	jeden	k4xCgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
při	při	k7c6
přechodu	přechod	k1gInSc6
z	z	k7c2
prostředí	prostředí	k1gNnSc2
opticky	opticky	k6eAd1
řidšího	řídký	k2eAgNnSc2d2
prostředí	prostředí	k1gNnSc2
do	do	k7c2
prostředí	prostředí	k1gNnSc2
opticky	opticky	k6eAd1
hustšího	hustý	k2eAgNnSc2d2
je	být	k5eAaImIp3nS
relativní	relativní	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
jedna	jeden	k4xCgFnSc1
<g/>
.	.	kIx.
</s>
<s>
Frekvenčně	frekvenčně	k6eAd1
závislý	závislý	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
Tak	tak	k9
jako	jako	k9
všechny	všechen	k3xTgFnPc4
optické	optický	k2eAgFnPc4d1
konstanty	konstanta	k1gFnPc4
je	být	k5eAaImIp3nS
i	i	k9
index	index	k1gInSc1
lomu	lom	k1gInSc2
obecně	obecně	k6eAd1
komplexní	komplexní	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
frekvence	frekvence	k1gFnSc2
(	(	kIx(
<g/>
resp.	resp.	kA
vlnové	vlnový	k2eAgFnPc4d1
délky	délka	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
N	N	kA
<g/>
(	(	kIx(
<g/>
ω	ω	k?
<g/>
)	)	kIx)
<g/>
=	=	kIx~
<g/>
n	n	k0
<g/>
(	(	kIx(
<g/>
ω	ω	k?
<g/>
)	)	kIx)
+	+	kIx~
i	i	k9
κ	κ	k?
<g/>
(	(	kIx(
<g/>
ω	ω	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
tedy	tedy	k9
reálnou	reálný	k2eAgFnSc4d1
a	a	k8xC
imaginární	imaginární	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Reálná	reálný	k2eAgFnSc1d1
část	část	k1gFnSc1
</s>
<s>
Reálná	reálný	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
zobecněním	zobecnění	k1gNnSc7
indexu	index	k1gInSc2
lomu	lom	k1gInSc2
popsaného	popsaný	k2eAgInSc2d1
v	v	k7c6
předešlém	předešlý	k2eAgInSc6d1
odstavci	odstavec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Látky	látka	k1gFnPc1
se	se	k3xPyFc4
často	často	k6eAd1
vyznačují	vyznačovat	k5eAaImIp3nP
přítomností	přítomnost	k1gFnSc7
několika	několik	k4yIc2
oblastí	oblast	k1gFnPc2
průhlednosti	průhlednost	k1gFnSc2
v	v	k7c6
elektromagnetickém	elektromagnetický	k2eAgNnSc6d1
spektru	spektrum	k1gNnSc6
<g/>
;	;	kIx,
v	v	k7c6
každé	každý	k3xTgFnSc6
z	z	k7c2
nich	on	k3xPp3gFnPc2
je	být	k5eAaImIp3nS
n	n	k0
téměř	téměř	k6eAd1
konstantní	konstantní	k2eAgFnSc1d1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
tyto	tento	k3xDgFnPc1
konstantní	konstantní	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
rostou	růst	k5eAaImIp3nP
směrem	směr	k1gInSc7
k	k	k7c3
větším	veliký	k2eAgFnPc3d2
frekvencím	frekvence	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Frekvenčně	frekvenčně	k6eAd1
závislý	závislý	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
také	také	k9
popisuje	popisovat	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc1
šíření	šíření	k1gNnSc2
světla	světlo	k1gNnSc2
v	v	k7c6
látce	látka	k1gFnSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
navíc	navíc	k6eAd1
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
rozlišovat	rozlišovat	k5eAaImF
mezi	mezi	k7c7
fázovou	fázový	k2eAgFnSc7d1
a	a	k8xC
grupovou	grupový	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
<g/>
:	:	kIx,
zatímco	zatímco	k8xS
fázová	fázový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
popisuje	popisovat	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc4
šíření	šíření	k1gNnSc4
ploch	plocha	k1gFnPc2
se	s	k7c7
stejnou	stejný	k2eAgFnSc7d1
fází	fáze	k1gFnSc7
vlnění	vlnění	k1gNnSc2
<g/>
,	,	kIx,
grupová	grupový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
k	k	k7c3
obálce	obálka	k1gFnSc3
amplitudy	amplituda	k1gFnSc2
<g/>
,	,	kIx,
neboli	neboli	k8xC
k	k	k7c3
rychlosti	rychlost	k1gFnSc3
šíření	šíření	k1gNnSc2
signálu	signál	k1gInSc2
(	(	kIx(
<g/>
informace	informace	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Fázová	fázový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
má	mít	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
v	v	k7c6
</s>
<s>
(	(	kIx(
</s>
<s>
ω	ω	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
ω	ω	k?
</s>
<s>
)	)	kIx)
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
v	v	k7c6
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
c	c	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
)	)	kIx)
<g/>
}}}	}}}	k?
</s>
<s>
a	a	k8xC
grupová	grupový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
v	v	k7c6
</s>
<s>
g	g	kA
</s>
<s>
(	(	kIx(
</s>
<s>
ω	ω	k?
</s>
<s>
)	)	kIx)
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
n	n	k0
</s>
<s>
(	(	kIx(
</s>
<s>
ω	ω	k?
</s>
<s>
)	)	kIx)
</s>
<s>
+	+	kIx~
</s>
<s>
ω	ω	k?
</s>
<s>
d	d	k?
</s>
<s>
n	n	k0
</s>
<s>
d	d	k?
</s>
<s>
ω	ω	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
v_	v_	k?
<g/>
{	{	kIx(
<g/>
g	g	kA
<g/>
}	}	kIx)
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
)	)	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
c	c	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
(	(	kIx(
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
)	)	kIx)
<g/>
+	+	kIx~
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
dn	dn	k?
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
d	d	k?
<g/>
\	\	kIx~
<g/>
omega	omega	k1gFnSc1
}}}}}	}}}}}	k?
</s>
<s>
(	(	kIx(
<g/>
jmenovatel	jmenovatel	k1gInSc1
se	se	k3xPyFc4
také	také	k9
označuje	označovat	k5eAaImIp3nS
pojmem	pojem	k1gInSc7
grupový	grupový	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Grupová	Grupový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
nemůže	moct	k5eNaImIp3nS
přesáhnout	přesáhnout	k5eAaPmF
hodnotu	hodnota	k1gFnSc4
c	c	k0
ve	v	k7c6
shodě	shoda	k1gFnSc6
s	s	k7c7
teorií	teorie	k1gFnSc7
relativity	relativita	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
opticky	opticky	k6eAd1
čerpaném	čerpaný	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
(	(	kIx(
<g/>
čerpání	čerpání	k1gNnSc1
typu	typ	k1gInSc2
používaného	používaný	k2eAgInSc2d1
v	v	k7c6
laserech	laser	k1gInPc6
<g/>
)	)	kIx)
však	však	k9
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
záporná	záporný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děje	děj	k1gInSc2
se	se	k3xPyFc4
tak	tak	k9
vždy	vždy	k6eAd1
výhradně	výhradně	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
současně	současně	k6eAd1
velmi	velmi	k6eAd1
silná	silný	k2eAgFnSc1d1
absorpce	absorpce	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vyžadují	vyžadovat	k5eAaImIp3nP
Kramersovy-Kronigovy	Kramersovy-Kronigův	k2eAgFnPc1d1
relace	relace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
2006	#num#	k4
oznámil	oznámit	k5eAaPmAgInS
tým	tým	k1gInSc1
Univerzity	univerzita	k1gFnSc2
v	v	k7c6
Rochesteru	Rochester	k1gInSc6
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
vedený	vedený	k2eAgInSc1d1
Robertem	Robert	k1gMnSc7
Boydem	Boyd	k1gMnSc7
důkaz	důkaz	k1gInSc4
záporné	záporný	k2eAgFnSc2d1
grupové	grupový	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
v	v	k7c6
časopise	časopis	k1gInSc6
Science	Science	k1gFnSc2
–	–	k?
experiment	experiment	k1gInSc1
prokázal	prokázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
takovém	takový	k3xDgNnSc6
prostředí	prostředí	k1gNnSc6
světelný	světelný	k2eAgInSc4d1
puls	puls	k1gInSc4
šíří	šířit	k5eAaImIp3nS
opravdu	opravdu	k6eAd1
pozpátku	pozpátku	k6eAd1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Naproti	naproti	k7c3
tomu	ten	k3xDgMnSc3
fázová	fázový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
není	být	k5eNaImIp3nS
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
přenosem	přenos	k1gInSc7
informace	informace	k1gFnSc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
nabývat	nabývat	k5eAaImF
téměř	téměř	k6eAd1
libovolných	libovolný	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
,	,	kIx,
vyšších	vysoký	k2eAgFnPc2d2
než	než	k8xS
c	c	k0
nebo	nebo	k8xC
dokonce	dokonce	k9
záporných	záporný	k2eAgInPc2d1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Imaginární	imaginární	k2eAgFnSc1d1
část	část	k1gFnSc1
</s>
<s>
Index	index	k1gInSc1
absorpce	absorpce	k1gFnSc2
<g/>
,	,	kIx,
κ	κ	k?
<g/>
(	(	kIx(
<g/>
ω	ω	k?
<g/>
)	)	kIx)
udává	udávat	k5eAaImIp3nS
míru	míra	k1gFnSc4
útlumu	útlum	k1gInSc2
procházejícího	procházející	k2eAgNnSc2d1
záření	záření	k1gNnSc2
v	v	k7c6
dané	daný	k2eAgFnSc6d1
látce	látka	k1gFnSc6
pohlcením	pohlcení	k1gNnSc7
(	(	kIx(
<g/>
absorpcí	absorpce	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
z	z	k7c2
něj	on	k3xPp3gMnSc2
určit	určit	k5eAaPmF
např.	např.	kA
absorpční	absorpční	k2eAgFnSc4d1
délku	délka	k1gFnSc4
da	da	k?
<g/>
(	(	kIx(
<g/>
ω	ω	k?
<g/>
)	)	kIx)
pomocí	pomocí	k7c2
vztahu	vztah	k1gInSc2
</s>
<s>
d	d	k?
</s>
<s>
a	a	k8xC
</s>
<s>
=	=	kIx~
</s>
<s>
c	c	k0
</s>
<s>
2	#num#	k4
</s>
<s>
ω	ω	k?
</s>
<s>
κ	κ	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
d_	d_	k?
<g/>
{	{	kIx(
<g/>
a	a	k8xC
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k6eAd1
{	{	kIx(
<g/>
c	c	k0
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
\	\	kIx~
<g/>
omega	omega	k1gNnSc1
\	\	kIx~
<g/>
kappa	kappa	k1gNnSc1
}}}	}}}	k?
</s>
<s>
</s>
<s>
Urazí	urazit	k5eAaPmIp3nS
<g/>
-li	-li	k?
v	v	k7c6
dané	daný	k2eAgFnSc6d1
látce	látka	k1gFnSc6
záření	záření	k1gNnSc2
o	o	k7c6
úhlové	úhlový	k2eAgFnSc6d1
frekvenci	frekvence	k1gFnSc6
ω	ω	k?
vzdálenost	vzdálenost	k1gFnSc1
da	da	k?
<g/>
,	,	kIx,
poklesne	poklesnout	k5eAaPmIp3nS
jeho	jeho	k3xOp3gFnSc1
intenzita	intenzita	k1gFnSc1
na	na	k7c4
hodnotu	hodnota	k1gFnSc4
1	#num#	k4
<g/>
/	/	kIx~
<g/>
e	e	k0
<g/>
,	,	kIx,
tj.	tj.	kA
asi	asi	k9
na	na	k7c4
36,8	36,8	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Značení	značení	k1gNnSc1
</s>
<s>
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
index	index	k1gInSc1
lomu	lom	k1gInSc2
značí	značit	k5eAaImIp3nS
n	n	k0
<g/>
,	,	kIx,
ale	ale	k8xC
protože	protože	k8xS
doopravdy	doopravdy	k6eAd1
závisí	záviset	k5eAaImIp3nS
index	index	k1gInSc1
lomu	lom	k1gInSc2
na	na	k7c6
vlnové	vlnový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
právě	právě	k9
toho	ten	k3xDgNnSc2
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
se	se	k3xPyFc4
láme	lámat	k5eAaImIp3nS
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
se	se	k3xPyFc4
u	u	k7c2
materiálů	materiál	k1gInPc2
většinou	většinou	k6eAd1
index	index	k1gInSc1
lomu	lom	k1gInSc2
značí	značit	k5eAaImIp3nS
n	n	k0
s	s	k7c7
dolním	dolní	k2eAgInSc7d1
indexem	index	k1gInSc7
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
je	být	k5eAaImIp3nS
písmenné	písmenný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
vlnové	vlnový	k2eAgFnSc2d1
délky	délka	k1gFnSc2
-	-	kIx~
barvy	barva	k1gFnPc1
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
daný	daný	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
platí	platit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Většinou	většina	k1gFnSc7
se	se	k3xPyFc4
u	u	k7c2
materiálů	materiál	k1gInPc2
v	v	k7c6
rychlosti	rychlost	k1gFnSc6
uvádí	uvádět	k5eAaImIp3nS
index	index	k1gInSc1
lomu	lom	k1gInSc2
nP	nP	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
index	index	k1gInSc4
lomu	lom	k1gInSc2
pro	pro	k7c4
světlo	světlo	k1gNnSc4
z	z	k7c2
Fraunhoferovy	Fraunhoferův	k2eAgFnSc2d1
čáry	čára	k1gFnSc2
D	D	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
589,26	589,26	k4
nm	nm	k?
vlnové	vlnový	k2eAgFnSc2d1
délky	délka	k1gFnSc2
světla	světlo	k1gNnSc2
(	(	kIx(
<g/>
D-linie	D-linie	k1gFnSc1
spektra	spektrum	k1gNnSc2
Na	na	k7c6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
měření	měření	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
ještě	ještě	k9
poměrně	poměrně	k6eAd1
často	často	k6eAd1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
je	být	k5eAaImIp3nS
index	index	k1gInSc1
lomu	lom	k1gInSc2
pro	pro	k7c4
světlo	světlo	k1gNnSc4
odpovídající	odpovídající	k2eAgFnSc6d1
vlnové	vlnový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
670,784	670,784	k4
3	#num#	k4
nm	nm	k?
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
značí	značit	k5eAaImIp3nP
nLi	nLi	k?
a	a	k8xC
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
α	α	k1gFnSc6
spektra	spektrum	k1gNnSc2
Li	li	k9
<g/>
.	.	kIx.
</s>
<s>
Lom	lom	k1gInSc1
světla	světlo	k1gNnSc2
v	v	k7c6
materiálu	materiál	k1gInSc6
může	moct	k5eAaImIp3nS
nastávat	nastávat	k5eAaImF
ve	v	k7c6
směrech	směr	k1gInPc6
krystalografických	krystalografický	k2eAgFnPc2d1
os	osa	k1gFnPc2
a	a	k8xC
takto	takto	k6eAd1
změřené	změřený	k2eAgFnPc1d1
veličiny	veličina	k1gFnPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
značí	značit	k5eAaImIp3nS
např.	např.	kA
<g/>
:	:	kIx,
nPa	nPa	k?
nPb	nPb	k?
nebo	nebo	k8xC
nLic	nLic	k6eAd1
nebo	nebo	k8xC
pokud	pokud	k8xS
světlo	světlo	k1gNnSc1
dopadá	dopadat	k5eAaImIp3nS
na	na	k7c4
stěnu	stěna	k1gFnSc4
dvojlomného	dvojlomný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
rozdělení	rozdělení	k1gNnSc1
na	na	k7c4
dva	dva	k4xCgInPc4
paprsky	paprsek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
řádný	řádný	k2eAgInSc1d1
(	(	kIx(
<g/>
ordinární	ordinární	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
označuje	označovat	k5eAaImIp3nS
se	se	k3xPyFc4
nPř	nPř	k?
nebo	nebo	k8xC
nLiř	nLiř	k1gInSc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
paprsek	paprsek	k1gInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
mimořádný	mimořádný	k2eAgInSc1d1
(	(	kIx(
<g/>
extraordinární	extraordinární	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
označuje	označovat	k5eAaImIp3nS
se	se	k3xPyFc4
nPm	nPm	k?
nebo	nebo	k8xC
nLim	nLim	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
toho	ten	k3xDgNnSc2
jakým	jaký	k3yRgNnSc7,k3yQgNnSc7,k3yIgNnSc7
světlem	světlo	k1gNnSc7
se	se	k3xPyFc4
látka	látka	k1gFnSc1
proměřuje	proměřovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Záporný	záporný	k2eAgInSc1d1
index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
Šíření	šíření	k1gNnSc1
elektromagnetických	elektromagnetický	k2eAgFnPc2d1
vln	vlna	k1gFnPc2
v	v	k7c6
látce	látka	k1gFnSc6
popisují	popisovat	k5eAaImIp3nP
Maxwellovy	Maxwellův	k2eAgFnPc1d1
rovnice	rovnice	k1gFnPc1
spolu	spolu	k6eAd1
se	s	k7c7
vztahy	vztah	k1gInPc7
D	D	kA
=	=	kIx~
ε	ε	k?
E	E	kA
<g/>
,	,	kIx,
B	B	kA
=	=	kIx~
μ	μ	k?
H	H	kA
kde	kde	k6eAd1
ε	ε	k?
je	být	k5eAaImIp3nS
komplexní	komplexní	k2eAgFnSc1d1
permitivita	permitivita	k1gFnSc1
a	a	k8xC
μ	μ	k?
magnetická	magnetický	k2eAgFnSc1d1
permeabilita	permeabilita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záporný	záporný	k2eAgInSc1d1
lom	lom	k1gInSc1
zkoumal	zkoumat	k5eAaImAgInS
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
sovětský	sovětský	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
V.	V.	kA
G.	G.	kA
Veselago	Veselago	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
si	se	k3xPyFc3
všiml	všimnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
kromě	kromě	k7c2
obvyklých	obvyklý	k2eAgNnPc2d1
řešení	řešení	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
reálné	reálný	k2eAgFnSc2d1
části	část	k1gFnSc2
ε	ε	k?
<g/>
,	,	kIx,
μ	μ	k?
a	a	k8xC
n	n	k0
jsou	být	k5eAaImIp3nP
kladné	kladný	k2eAgFnPc1d1
<g/>
,	,	kIx,
formálně	formálně	k6eAd1
existují	existovat	k5eAaImIp3nP
i	i	k9
řešení	řešení	k1gNnSc4
se	s	k7c7
zápornými	záporný	k2eAgFnPc7d1
hodnotami	hodnota	k1gFnPc7
těchto	tento	k3xDgFnPc2
veličin	veličina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpověděl	předpovědět	k5eAaPmAgMnS
tak	tak	k9
<g/>
,	,	kIx,
že	že	k8xS
takovýto	takovýto	k3xDgInSc1
materiál	materiál	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
některé	některý	k3yIgFnPc4
neobvyklé	obvyklý	k2eNgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
<g/>
:	:	kIx,
lom	lom	k1gInSc1
světla	světlo	k1gNnSc2
by	by	kYmCp3nS
podle	podle	k7c2
Snellova	Snellův	k2eAgInSc2d1
zákona	zákon	k1gInSc2
obracel	obracet	k5eAaImAgInS
směr	směr	k1gInSc1
šíření	šíření	k1gNnSc2
paprsků	paprsek	k1gInPc2
vůči	vůči	k7c3
kolmici	kolmice	k1gFnSc3
dopadu	dopad	k1gInSc2
a	a	k8xC
fázová	fázový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
záporná	záporný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vytvořit	vytvořit	k5eAaPmF
takovou	takový	k3xDgFnSc4
látku	látka	k1gFnSc4
ve	v	k7c6
formě	forma	k1gFnSc6
tzv.	tzv.	kA
metamateriálu	metamateriál	k1gInSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
až	až	k9
po	po	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
však	však	k9
jen	jen	k9
pro	pro	k7c4
jednu	jeden	k4xCgFnSc4
frekvenci	frekvence	k1gFnSc4
vlnění	vlnění	k1gNnSc2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
jen	jen	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
mikrovlnného	mikrovlnný	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
sestavení	sestavení	k1gNnSc4
podobných	podobný	k2eAgInPc2d1
metamateriálů	metamateriál	k1gInPc2
pro	pro	k7c4
viditelné	viditelný	k2eAgNnSc4d1
světlo	světlo	k1gNnSc4
pracují	pracovat	k5eAaImIp3nP
v	v	k7c6
současnosti	současnost	k1gFnSc6
některé	některý	k3yIgInPc1
výzkumné	výzkumný	k2eAgInPc1d1
týmy	tým	k1gInPc1
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gNnSc1
použití	použití	k1gNnSc1
by	by	kYmCp3nP
znamenalo	znamenat	k5eAaImAgNnS
významný	významný	k2eAgInSc4d1
pokrok	pokrok	k1gInSc4
v	v	k7c6
optice	optika	k1gFnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
by	by	kYmCp3nS
umožnilo	umožnit	k5eAaPmAgNnS
optické	optický	k2eAgNnSc1d1
zobrazování	zobrazování	k1gNnSc4
objektů	objekt	k1gInPc2
podstatně	podstatně	k6eAd1
menších	malý	k2eAgInPc2d2
než	než	k8xS
vlnová	vlnový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
použitého	použitý	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
bez	bez	k7c2
nutnosti	nutnost	k1gFnSc2
používat	používat	k5eAaImF
skenující	skenující	k2eAgFnSc4d1
vlnovodnou	vlnovodný	k2eAgFnSc4d1
sondu	sonda	k1gFnSc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
SNOM	SNOM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejde	jít	k5eNaImIp3nS
však	však	k9
jen	jen	k9
o	o	k7c4
technologický	technologický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
-	-	kIx~
stále	stále	k6eAd1
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
vyřešit	vyřešit	k5eAaPmF
koncepční	koncepční	k2eAgFnPc4d1
překážky	překážka	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
prostorová	prostorový	k2eAgFnSc1d1
disperze	disperze	k1gFnSc1
(	(	kIx(
<g/>
tj.	tj.	kA
zejm.	zejm.	k?
závislost	závislost	k1gFnSc4
efektivního	efektivní	k2eAgInSc2d1
indexu	index	k1gInSc2
lomu	lom	k1gInSc2
na	na	k7c6
směru	směr	k1gInSc6
šíření	šíření	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
především	především	k9
silná	silný	k2eAgFnSc1d1
absorpce	absorpce	k1gFnSc1
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
předpovídána	předpovídat	k5eAaImNgFnS
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
struktury	struktura	k1gFnPc4
dosud	dosud	k6eAd1
navržené	navržený	k2eAgInPc1d1
z	z	k7c2
realistických	realistický	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
v	v	k7c6
optické	optický	k2eAgFnSc6d1
nebo	nebo	k8xC
infračervené	infračervený	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Light	Light	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Most	most	k1gInSc1
Exotic	Exotice	k1gFnPc2
Trick	Trick	k1gMnSc1
Yet	Yet	k1gMnSc1
<g/>
:	:	kIx,
So	So	kA
Fast	Fast	k2eAgInSc1d1
it	it	k?
Goes	Goes	k1gInSc1
…	…	k?
Backwards	Backwards	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
↑	↑	k?
Gehring	Gehring	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Observation	Observation	k1gInSc1
of	of	k?
Backward	Backward	k1gInSc1
Pulse	puls	k1gInSc5
Propagation	Propagation	k1gInSc4
Through	Through	k1gInSc1
a	a	k8xC
Medium	medium	k1gNnSc1
with	witha	k1gFnPc2
a	a	k8xC
Negative	negativ	k1gInSc5
Group	Group	k1gInSc4
Velocity	Velocit	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
312	#num#	k4
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
895	#num#	k4
-	-	kIx~
897	#num#	k4
<g/>
,	,	kIx,
doi	doi	k?
<g/>
:	:	kIx,
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.1124524	.1124524	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Lom	lom	k1gInSc1
světla	světlo	k1gNnSc2
</s>
<s>
Snellův	Snellův	k2eAgInSc1d1
zákon	zákon	k1gInSc1
</s>
<s>
Imerzní	Imerzní	k2eAgFnSc1d1
tekutina	tekutina	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4146524-6	4146524-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85112261	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85112261	#num#	k4
</s>
