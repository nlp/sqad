<s>
Vorvaň	vorvaň	k1gMnSc1	vorvaň
<g/>
,	,	kIx,	,
též	též	k9	též
vorvaň	vorvaň	k1gMnSc1	vorvaň
obrovský	obrovský	k2eAgMnSc1d1	obrovský
<g/>
,	,	kIx,	,
tuponosý	tuponosý	k2eAgMnSc1d1	tuponosý
nebo	nebo	k8xC	nebo
tupočelý	tupočelý	k2eAgMnSc1d1	tupočelý
(	(	kIx(	(
<g/>
Physeter	Physeter	k1gMnSc1	Physeter
catodon	catodon	k1gMnSc1	catodon
<g/>
,	,	kIx,	,
macrocephalus	macrocephalus	k1gMnSc1	macrocephalus
<g/>
,	,	kIx,	,
orthodon	orthodon	k1gMnSc1	orthodon
nebo	nebo	k8xC	nebo
australasianus	australasianus	k1gMnSc1	australasianus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
ozubených	ozubený	k2eAgFnPc2d1	ozubená
velryb	velryba	k1gFnPc2	velryba
a	a	k8xC	a
věří	věřit	k5eAaImIp3nS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vůbec	vůbec	k9	vůbec
největší	veliký	k2eAgNnSc4d3	veliký
ozubené	ozubený	k2eAgNnSc4d1	ozubené
zvíře	zvíře	k1gNnSc4	zvíře
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc1	jaký
kdy	kdy	k6eAd1	kdy
žilo	žít	k5eAaImAgNnS	žít
–	–	k?	–
největší	veliký	k2eAgMnSc1d3	veliký
doložený	doložený	k2eAgMnSc1d1	doložený
jedinec	jedinec	k1gMnSc1	jedinec
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
až	až	k9	až
28	[number]	k4	28
metrů	metr	k1gInPc2	metr
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
váhy	váha	k1gFnSc2	váha
150	[number]	k4	150
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
Sperm	Sperm	k1gInSc1	Sperm
Whale	Whale	k1gInSc4	Whale
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
mléčně	mléčně	k6eAd1	mléčně
bílou	bílý	k2eAgFnSc7d1	bílá
substancí	substance	k1gFnSc7	substance
spermacet	spermacet	k1gInSc4	spermacet
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
hlavě	hlava	k1gFnSc6	hlava
a	a	k8xC	a
původně	původně	k6eAd1	původně
zaměněnou	zaměněný	k2eAgFnSc4d1	zaměněná
se	s	k7c7	s
spermatem	sperma	k1gNnSc7	sperma
<g/>
.	.	kIx.	.
</s>
<s>
Obrovská	obrovský	k2eAgFnSc1d1	obrovská
hlava	hlava	k1gFnSc1	hlava
vorvaně	vorvaň	k1gMnSc2	vorvaň
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc4	on
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
ústřední	ústřední	k2eAgFnSc2d1	ústřední
role	role	k1gFnSc2	role
v	v	k7c6	v
románu	román	k1gInSc6	román
Hermana	Herman	k1gMnSc2	Herman
Melvilla	Melvill	k1gMnSc2	Melvill
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
líčili	líčit	k5eAaImAgMnP	líčit
jako	jako	k8xS	jako
archetyp	archetyp	k1gInSc4	archetyp
velryby	velryba	k1gFnSc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
díky	díky	k7c3	díky
Melvillovi	Melvill	k1gMnSc3	Melvill
je	být	k5eAaImIp3nS	být
vorvaň	vorvaň	k1gMnSc1	vorvaň
obecně	obecně	k6eAd1	obecně
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
napůl	napůl	k6eAd1	napůl
mýtickým	mýtický	k2eAgInSc7d1	mýtický
Leviatanem	leviatan	k1gInSc7	leviatan
z	z	k7c2	z
biblických	biblický	k2eAgInPc2d1	biblický
příběhů	příběh	k1gInPc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
в	в	k?	в
[	[	kIx(	[
<g/>
vorvaň	vorvaň	k1gMnSc1	vorvaň
<g/>
]	]	kIx)	]
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
označuje	označovat	k5eAaImIp3nS	označovat
tuk	tuk	k1gInSc4	tuk
některých	některý	k3yIgNnPc2	některý
mořských	mořský	k2eAgNnPc2d1	mořské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
velryby	velryba	k1gFnPc1	velryba
a	a	k8xC	a
tuleni	tuleň	k1gMnPc1	tuleň
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc4d1	český
název	název	k1gInSc4	název
zavedl	zavést	k5eAaPmAgMnS	zavést
Jan	Jan	k1gMnSc1	Jan
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Presl	Presl	k1gInSc4	Presl
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
staroruského	staroruský	k2eAgMnSc2d1	staroruský
"	"	kIx"	"
<g/>
vorvoň	vorvonit	k5eAaPmRp2nS	vorvonit
<g/>
"	"	kIx"	"
jasný	jasný	k2eAgInSc1d1	jasný
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
je	být	k5eAaImIp3nS	být
norský	norský	k2eAgMnSc1d1	norský
nebo	nebo	k8xC	nebo
laponský	laponský	k2eAgMnSc1d1	laponský
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
a	a	k8xC	a
ve	v	k7c6	v
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
ruštině	ruština	k1gFnSc6	ruština
<g/>
,	,	kIx,	,
polštině	polština	k1gFnSc6	polština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velryba	velryba	k1gFnSc1	velryba
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
kašalot	kašalota	k1gFnPc2	kašalota
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
původně	původně	k6eAd1	původně
z	z	k7c2	z
portugalského	portugalský	k2eAgInSc2d1	portugalský
cachalote	cachalot	k1gMnSc5	cachalot
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odvozeného	odvozený	k2eAgMnSc2d1	odvozený
z	z	k7c2	z
cachola	cachola	k1gFnSc1	cachola
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
"	"	kIx"	"
<g/>
hlava	hlava	k1gFnSc1	hlava
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vorvani	vorvaň	k1gMnPc1	vorvaň
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
portugalském	portugalský	k2eAgNnSc6d1	portugalské
atlantickém	atlantický	k2eAgNnSc6d1	atlantické
souostroví	souostroví	k1gNnSc6	souostroví
Azory	Azory	k1gFnPc4	Azory
až	až	k9	až
donedávna	donedávna	k6eAd1	donedávna
loveni	loven	k2eAgMnPc1d1	loven
<g/>
.	.	kIx.	.
</s>
<s>
Vorvaň	vorvaň	k1gMnSc1	vorvaň
je	být	k5eAaImIp3nS	být
výjimečný	výjimečný	k2eAgMnSc1d1	výjimečný
svou	svůj	k3xOyFgFnSc7	svůj
velkou	velký	k2eAgFnSc7d1	velká
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
samců	samec	k1gMnPc2	samec
tvoří	tvořit	k5eAaImIp3nS	tvořit
zpravidla	zpravidla	k6eAd1	zpravidla
třetinu	třetina	k1gFnSc4	třetina
délky	délka	k1gFnSc2	délka
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
odborných	odborný	k2eAgNnPc2d1	odborné
druhových	druhový	k2eAgNnPc2d1	druhové
jmen	jméno	k1gNnPc2	jméno
macrocephalus	macrocephalus	k1gMnSc1	macrocephalus
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
velká	velký	k2eAgFnSc1d1	velká
hlava	hlava	k1gFnSc1	hlava
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hladké	hladký	k2eAgFnSc2d1	hladká
kůže	kůže	k1gFnSc2	kůže
většiny	většina	k1gFnSc2	většina
jiných	jiný	k2eAgFnPc2d1	jiná
velryb	velryba	k1gFnPc2	velryba
je	být	k5eAaImIp3nS	být
kůže	kůže	k1gFnSc1	kůže
na	na	k7c6	na
hřbetu	hřbet	k1gInSc6	hřbet
vorvaně	vorvaň	k1gMnSc2	vorvaň
obvykle	obvykle	k6eAd1	obvykle
hrbolatá	hrbolatý	k2eAgFnSc1d1	hrbolatá
<g/>
,	,	kIx,	,
dobrovolníky	dobrovolník	k1gMnPc4	dobrovolník
pozorujícími	pozorující	k2eAgFnPc7d1	pozorující
velryby	velryba	k1gFnSc2	velryba
bývá	bývat	k5eAaImIp3nS	bývat
přirovnávána	přirovnávat	k5eAaImNgFnS	přirovnávat
k	k	k7c3	k
sušené	sušený	k2eAgFnSc3d1	sušená
švestce	švestka	k1gFnSc3	švestka
<g/>
.	.	kIx.	.
</s>
<s>
Vorvani	vorvaň	k1gMnPc1	vorvaň
mají	mít	k5eAaImIp3nP	mít
jednotně	jednotně	k6eAd1	jednotně
šedou	šedý	k2eAgFnSc4d1	šedá
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
slunečním	sluneční	k2eAgNnSc6d1	sluneční
světle	světlo	k1gNnSc6	světlo
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
hnědá	hnědý	k2eAgFnSc1d1	hnědá
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
"	"	kIx"	"
z	z	k7c2	z
Melvillova	Melvillův	k2eAgInSc2d1	Melvillův
románu	román	k1gInSc2	román
byla	být	k5eAaImAgFnS	být
albínem	albín	k1gInSc7	albín
<g/>
,	,	kIx,	,
a	a	k8xC	a
bílí	bílý	k2eAgMnPc1d1	bílý
vorvani	vorvaň	k1gMnPc1	vorvaň
byli	být	k5eAaImAgMnP	být
skutečně	skutečně	k6eAd1	skutečně
pozorováni	pozorován	k2eAgMnPc1d1	pozorován
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
Mocha	mocha	k1gFnSc1	mocha
Dick	Dick	k1gMnSc1	Dick
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepřekvapuje	překvapovat	k5eNaImIp3nS	překvapovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mozek	mozek	k1gInSc1	mozek
vorvaně	vorvaň	k1gMnSc2	vorvaň
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
a	a	k8xC	a
nejtěžším	těžký	k2eAgNnSc7d3	nejtěžší
(	(	kIx(	(
<g/>
mozek	mozek	k1gInSc1	mozek
vzrostlého	vzrostlý	k2eAgInSc2d1	vzrostlý
samce	samec	k1gInSc2	samec
váží	vážit	k5eAaImIp3nS	vážit
průměrně	průměrně	k6eAd1	průměrně
7	[number]	k4	7
kg	kg	kA	kg
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
i	i	k8xC	i
exempláře	exemplář	k1gInPc1	exemplář
vážící	vážící	k2eAgInPc1d1	vážící
9,2	[number]	k4	9,2
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
těla	tělo	k1gNnSc2	tělo
však	však	k8xC	však
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
velký	velký	k2eAgInSc4d1	velký
mozek	mozek	k1gInSc4	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Dýchací	dýchací	k2eAgInSc1d1	dýchací
otvor	otvor	k1gInSc1	otvor
tvaru	tvar	k1gInSc2	tvar
'	'	kIx"	'
<g/>
S	s	k7c7	s
<g/>
'	'	kIx"	'
je	být	k5eAaImIp3nS	být
blízko	blízko	k7c2	blízko
vrcholu	vrchol	k1gInSc2	vrchol
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
posunutý	posunutý	k2eAgMnSc1d1	posunutý
na	na	k7c4	na
levou	levý	k2eAgFnSc4d1	levá
stranu	strana	k1gFnSc4	strana
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Vorvani	vorvaň	k1gMnPc1	vorvaň
jím	on	k3xPp3gInSc7	on
vypouštějí	vypouštět	k5eAaImIp3nP	vypouštět
směrem	směr	k1gInSc7	směr
dopředu	dopředu	k6eAd1	dopředu
proudy	proud	k1gInPc1	proud
páry	pára	k1gFnSc2	pára
<g/>
,	,	kIx,	,
zřetelně	zřetelně	k6eAd1	zřetelně
slyšitelné	slyšitelný	k2eAgFnPc1d1	slyšitelná
na	na	k7c4	na
míle	míle	k1gFnPc4	míle
daleko	daleko	k6eAd1	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Hřbetní	hřbetní	k2eAgFnSc1d1	hřbetní
ploutev	ploutev	k1gFnSc1	ploutev
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
asi	asi	k9	asi
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
třetinách	třetina	k1gFnPc6	třetina
páteře	páteř	k1gFnSc2	páteř
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
hlavy	hlava	k1gFnSc2	hlava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
trojúhelníkovitý	trojúhelníkovitý	k2eAgInSc1d1	trojúhelníkovitý
hrb	hrb	k1gInSc1	hrb
vysoký	vysoký	k2eAgInSc1d1	vysoký
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
cm	cm	kA	cm
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ploutev	ploutev	k1gFnSc4	ploutev
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
hrbem	hrb	k1gInSc7	hrb
následuje	následovat	k5eAaImIp3nS	následovat
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
menších	malý	k2eAgFnPc2d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Ocasní	ocasní	k2eAgFnSc1d1	ocasní
ploutev	ploutev	k1gFnSc1	ploutev
je	být	k5eAaImIp3nS	být
také	také	k9	také
trojúhelníková	trojúhelníkový	k2eAgFnSc1d1	trojúhelníková
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
tlustá	tlustý	k2eAgFnSc1d1	tlustá
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
ponořením	ponoření	k1gNnSc7	ponoření
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
ji	on	k3xPp3gFnSc4	on
velryba	velryba	k1gFnSc1	velryba
zvedá	zvedat	k5eAaImIp3nS	zvedat
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c4	nad
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vorvani	vorvaň	k1gMnPc1	vorvaň
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
čelisti	čelist	k1gFnSc6	čelist
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
párů	pár	k1gInPc2	pár
kuželovitých	kuželovitý	k2eAgInPc2d1	kuželovitý
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
může	moct	k5eAaImIp3nS	moct
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
jeden	jeden	k4xCgInSc4	jeden
kilogram	kilogram	k1gInSc4	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
existence	existence	k1gFnSc2	existence
zubů	zub	k1gInPc2	zub
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
chobotnic	chobotnice	k1gFnPc2	chobotnice
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
sekci	sekce	k1gFnSc4	sekce
Potrava	potrava	k1gFnSc1	potrava
<g/>
)	)	kIx)	)
nutné	nutný	k2eAgInPc1d1	nutný
nejsou	být	k5eNaImIp3nP	být
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
byli	být	k5eAaImAgMnP	být
nalezeni	nalezen	k2eAgMnPc1d1	nalezen
i	i	k9	i
dobře	dobře	k6eAd1	dobře
živení	živený	k2eAgMnPc1d1	živený
vorvani	vorvaň	k1gMnPc1	vorvaň
zcela	zcela	k6eAd1	zcela
bezzubí	bezzubý	k2eAgMnPc1d1	bezzubý
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zuby	zub	k1gInPc1	zub
mohou	moct	k5eAaImIp3nP	moct
užívat	užívat	k5eAaImF	užívat
samci	samec	k1gMnPc1	samec
téhož	týž	k3xTgInSc2	týž
druhu	druh	k1gInSc2	druh
při	při	k7c6	při
útocích	útok	k1gInPc6	útok
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
hypotéze	hypotéza	k1gFnSc3	hypotéza
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
tvar	tvar	k1gInSc1	tvar
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
široké	široký	k2eAgFnPc1d1	široká
mezery	mezera	k1gFnPc1	mezera
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
čelisti	čelist	k1gFnSc6	čelist
jsou	být	k5eAaImIp3nP	být
přítomny	přítomen	k2eAgInPc1d1	přítomen
rudimentární	rudimentární	k2eAgInPc1d1	rudimentární
zuby	zub	k1gInPc1	zub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyvinou	vyvinout	k5eAaPmIp3nP	vyvinout
se	se	k3xPyFc4	se
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgInSc1d1	sexuální
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
(	(	kIx(	(
<g/>
velká	velký	k2eAgFnSc1d1	velká
odlišnost	odlišnost	k1gFnSc1	odlišnost
samců	samec	k1gMnPc2	samec
oproti	oproti	k7c3	oproti
samicím	samice	k1gFnPc3	samice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
u	u	k7c2	u
vorvaňů	vorvaň	k1gMnPc2	vorvaň
projevuje	projevovat	k5eAaImIp3nS	projevovat
nejvýrazněji	výrazně	k6eAd3	výrazně
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
kytovci	kytovec	k1gMnPc7	kytovec
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
jsou	být	k5eAaImIp3nP	být
typicky	typicky	k6eAd1	typicky
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
delší	dlouhý	k2eAgMnSc1d2	delší
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
m	m	kA	m
<g/>
)	)	kIx)	)
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
asi	asi	k9	asi
dvakrát	dvakrát	k6eAd1	dvakrát
těžší	těžký	k2eAgMnSc1d2	těžší
(	(	kIx(	(
<g/>
50	[number]	k4	50
tun	tuna	k1gFnPc2	tuna
proti	proti	k7c3	proti
25	[number]	k4	25
tunám	tuna	k1gFnPc3	tuna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
narození	narození	k1gNnSc6	narození
měří	měřit	k5eAaImIp3nP	měřit
samci	samec	k1gMnPc1	samec
i	i	k8xC	i
samice	samice	k1gFnPc1	samice
kolem	kolem	k7c2	kolem
4	[number]	k4	4
m	m	kA	m
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
1000	[number]	k4	1000
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rozsáhlému	rozsáhlý	k2eAgInSc3d1	rozsáhlý
lovu	lov	k1gInSc2	lov
velryb	velryba	k1gFnPc2	velryba
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
vorvaňů	vorvaň	k1gMnPc2	vorvaň
dramaticky	dramaticky	k6eAd1	dramaticky
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
přednostnímu	přednostní	k2eAgNnSc3d1	přednostní
a	a	k8xC	a
intenzívnějšímu	intenzívní	k2eAgNnSc3d2	intenzívnější
vybíjení	vybíjení	k1gNnSc3	vybíjení
větších	veliký	k2eAgInPc2d2	veliký
samců	samec	k1gInPc2	samec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
spermacetu	spermacet	k1gInSc2	spermacet
(	(	kIx(	(
<g/>
spermacetový	spermacetový	k2eAgInSc1d1	spermacetový
olej	olej	k1gInSc1	olej
měl	mít	k5eAaImAgInS	mít
velkou	velký	k2eAgFnSc4d1	velká
hodnotu	hodnota	k1gFnSc4	hodnota
zejména	zejména	k9	zejména
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c4	v
Nantucket	Nantucket	k1gInSc4	Nantucket
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
čelistní	čelistní	k2eAgFnSc1d1	čelistní
kost	kost	k1gFnSc1	kost
vorvaně	vorvaň	k1gMnSc2	vorvaň
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
5,5	[number]	k4	5,5
m.	m.	k?	m.
Čelistní	čelistní	k2eAgFnSc1d1	čelistní
kost	kost	k1gFnSc1	kost
tvoří	tvořit	k5eAaImIp3nS	tvořit
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
délky	délka	k1gFnSc2	délka
těla	tělo	k1gNnSc2	tělo
vorvaně	vorvaň	k1gMnSc2	vorvaň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
samec	samec	k1gInSc1	samec
tedy	tedy	k9	tedy
mohl	moct	k5eAaImAgInS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
28	[number]	k4	28
metrů	metr	k1gInPc2	metr
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
vážit	vážit	k5eAaImF	vážit
150	[number]	k4	150
metrických	metrický	k2eAgFnPc2d1	metrická
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důkazem	důkaz	k1gInSc7	důkaz
velkých	velký	k2eAgMnPc2d1	velký
jedinců	jedinec	k1gMnPc2	jedinec
je	být	k5eAaImIp3nS	být
5,2	[number]	k4	5,2
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
čelist	čelist	k1gFnSc1	čelist
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
New	New	k1gFnSc6	New
Bedfordu	Bedford	k1gInSc2	Bedford
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mohla	moct	k5eAaImAgFnS	moct
patřit	patřit	k5eAaImF	patřit
samci	samec	k1gMnSc3	samec
25,6	[number]	k4	25,6
m	m	kA	m
velkému	velký	k2eAgInSc3d1	velký
vážícímu	vážící	k2eAgInSc3d1	vážící
120	[number]	k4	120
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Lodní	lodní	k2eAgFnPc1d1	lodní
knihy	kniha	k1gFnPc1	kniha
v	v	k7c6	v
nantucketském	nantucketský	k2eAgNnSc6d1	nantucketský
a	a	k8xC	a
bedforském	bedforský	k2eAgNnSc6d1	bedforský
muzeu	muzeum	k1gNnSc6	muzeum
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgFnPc1d1	plná
odkazů	odkaz	k1gInPc2	odkaz
na	na	k7c4	na
velké	velký	k2eAgMnPc4d1	velký
vorvaně	vorvaň	k1gMnPc4	vorvaň
a	a	k8xC	a
záznamů	záznam	k1gInPc2	záznam
o	o	k7c4	o
množství	množství	k1gNnSc4	množství
oleje	olej	k1gInSc2	olej
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
získaného	získaný	k2eAgNnSc2d1	získané
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dobře	dobře	k6eAd1	dobře
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
velikosti	velikost	k1gFnPc4	velikost
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
příkladů	příklad	k1gInPc2	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
vorvaní	vorvaní	k2eAgInSc1d1	vorvaní
samec	samec	k1gInSc1	samec
Mocha	mocha	k1gFnSc1	mocha
Dick	Dick	k1gMnSc1	Dick
měřil	měřit	k5eAaImAgInS	měřit
zřejmě	zřejmě	k6eAd1	zřejmě
25,7	[number]	k4	25,7
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgMnPc1d1	dnešní
vorvani	vorvaň	k1gMnPc1	vorvaň
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
18	[number]	k4	18
m	m	kA	m
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
52	[number]	k4	52
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Vorvaň	vorvaň	k1gMnSc1	vorvaň
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
příkladem	příklad	k1gInSc7	příklad
živočicha	živočich	k1gMnSc2	živočich
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
K-selekcí	Kelekce	k1gFnSc7	K-selekce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
druhům	druh	k1gMnPc3	druh
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
odborníci	odborník	k1gMnPc1	odborník
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgInP	vyvíjet
ve	v	k7c6	v
značně	značně	k6eAd1	značně
stabilních	stabilní	k2eAgFnPc6d1	stabilní
životních	životní	k2eAgFnPc6d1	životní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
relativně	relativně	k6eAd1	relativně
"	"	kIx"	"
<g/>
snadná	snadný	k2eAgFnSc1d1	snadná
<g/>
"	"	kIx"	"
evoluce	evoluce	k1gFnSc1	evoluce
je	on	k3xPp3gInPc4	on
přivedla	přivést	k5eAaPmAgFnS	přivést
k	k	k7c3	k
nízké	nízký	k2eAgFnSc3d1	nízká
porodnosti	porodnost	k1gFnSc3	porodnost
<g/>
,	,	kIx,	,
pomalému	pomalý	k2eAgNnSc3d1	pomalé
dozrávání	dozrávání	k1gNnSc3	dozrávání
a	a	k8xC	a
k	k	k7c3	k
dlouhověkosti	dlouhověkost	k1gFnSc3	dlouhověkost
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
březí	březí	k1gNnSc4	březí
každých	každý	k3xTgInPc2	každý
čtyři	čtyři	k4xCgNnPc4	čtyři
až	až	k9	až
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
a	a	k8xC	a
březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
protáhnout	protáhnout	k5eAaPmF	protáhnout
až	až	k9	až
na	na	k7c4	na
18	[number]	k4	18
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Starost	starost	k1gFnSc1	starost
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
zabírá	zabírat	k5eAaImIp3nS	zabírat
dva	dva	k4xCgInPc4	dva
až	až	k6eAd1	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
samců	samec	k1gMnPc2	samec
trvá	trvat	k5eAaImIp3nS	trvat
puberta	puberta	k1gFnSc1	puberta
asi	asi	k9	asi
10	[number]	k4	10
let	léto	k1gNnPc2	léto
mezi	mezi	k7c4	mezi
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
rostou	růst	k5eAaImIp3nP	růst
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
až	až	k9	až
40	[number]	k4	40
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
věku	věk	k1gInSc2	věk
a	a	k8xC	a
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gMnPc3	on
50	[number]	k4	50
<g/>
,	,	kIx,	,
nabývají	nabývat	k5eAaImIp3nP	nabývat
již	již	k6eAd1	již
plné	plný	k2eAgFnPc4d1	plná
velikosti	velikost	k1gFnPc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Vorvani	vorvaň	k1gMnPc1	vorvaň
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
80	[number]	k4	80
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vorvani	vorvaň	k1gMnPc1	vorvaň
drží	držet	k5eAaImIp3nP	držet
několik	několik	k4yIc4	několik
absolutních	absolutní	k2eAgInPc2d1	absolutní
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
<g/>
:	:	kIx,	:
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
největší	veliký	k2eAgNnSc4d3	veliký
ozubení	ozubení	k1gNnSc4	ozubení
tvorové	tvor	k1gMnPc1	tvor
<g/>
,	,	kIx,	,
jací	jaký	k3yRgMnPc1	jaký
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
žili	žít	k5eAaImAgMnP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Vorvani	vorvaň	k1gMnPc1	vorvaň
jsou	být	k5eAaImIp3nP	být
nejhlouběji	hluboko	k6eAd3	hluboko
se	se	k3xPyFc4	se
potápějící	potápějící	k2eAgMnPc1d1	potápějící
savci	savec	k1gMnPc1	savec
(	(	kIx(	(
<g/>
byli	být	k5eAaImAgMnP	být
nalezeni	nalezen	k2eAgMnPc1d1	nalezen
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
2200	[number]	k4	2200
metrů	metr	k1gInPc2	metr
zamotaní	zamotaný	k2eAgMnPc1d1	zamotaný
do	do	k7c2	do
telegrafních	telegrafní	k2eAgInPc2d1	telegrafní
kabelů	kabel	k1gInPc2	kabel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
největší	veliký	k2eAgInSc4d3	veliký
mozek	mozek	k1gInSc4	mozek
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgMnPc7	všecek
žijícími	žijící	k2eAgMnPc7d1	žijící
tvory	tvor	k1gMnPc7	tvor
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
mozek	mozek	k1gInSc1	mozek
vorvaního	vorvaní	k2eAgMnSc2d1	vorvaní
samce	samec	k1gMnSc2	samec
váží	vážit	k5eAaImIp3nS	vážit
7	[number]	k4	7
kg	kg	kA	kg
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
jedinci	jedinec	k1gMnPc1	jedinec
s	s	k7c7	s
až	až	k6eAd1	až
devítikilogramovými	devítikilogramový	k2eAgInPc7d1	devítikilogramový
mozky	mozek	k1gInPc7	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
kůže	kůže	k1gFnSc1	kůže
je	být	k5eAaImIp3nS	být
nejtlustší	tlustý	k2eAgFnSc1d3	nejtlustší
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
a	a	k8xC	a
hlavě	hlava	k1gFnSc6	hlava
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tloušťka	tloušťka	k1gFnSc1	tloušťka
vorvaní	vorvaní	k2eAgFnSc2d1	vorvaní
kůže	kůže	k1gFnSc2	kůže
(	(	kIx(	(
<g/>
nepočítaje	nepočítaje	k7c4	nepočítaje
podkožní	podkožní	k2eAgInSc4d1	podkožní
tuk	tuk	k1gInSc4	tuk
<g/>
)	)	kIx)	)
asi	asi	k9	asi
36	[number]	k4	36
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vorvaně	vorvaň	k1gMnSc4	vorvaň
poprvé	poprvé	k6eAd1	poprvé
kategorizoval	kategorizovat	k5eAaBmAgMnS	kategorizovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
Linné	Linná	k1gFnSc2	Linná
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zařadil	zařadit	k5eAaPmAgMnS	zařadit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
Physeter	Physeter	k1gInSc4	Physeter
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgInPc4	čtyři
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
brzy	brzy	k6eAd1	brzy
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
současných	současný	k2eAgFnPc2d1	současná
publikací	publikace	k1gFnPc2	publikace
je	být	k5eAaImIp3nS	být
vorvaň	vorvaň	k1gMnSc1	vorvaň
zařazen	zařadit	k5eAaPmNgMnS	zařadit
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
čeledi	čeleď	k1gFnSc2	čeleď
Physeteridae	Physeterida	k1gFnSc2	Physeterida
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
jediný	jediný	k2eAgInSc4d1	jediný
druh	druh	k1gInSc4	druh
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližšími	blízký	k2eAgFnPc7d3	nejbližší
příbuznými	příbuzná	k1gFnPc7	příbuzná
vorvaně	vorvaň	k1gMnSc2	vorvaň
jsou	být	k5eAaImIp3nP	být
kogie	kogie	k1gFnPc1	kogie
tuponosá	tuponosý	k2eAgFnSc1d1	tuponosá
a	a	k8xC	a
kogie	kogie	k1gFnSc1	kogie
Owenova	Owenov	k1gInSc2	Owenov
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Kogiidae	Kogiida	k1gFnSc2	Kogiida
<g/>
.	.	kIx.	.
</s>
<s>
Mead	Mead	k1gMnSc1	Mead
a	a	k8xC	a
Brownell	Brownell	k1gMnSc1	Brownell
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zařadili	zařadit	k5eAaPmAgMnP	zařadit
všechny	všechen	k3xTgInPc4	všechen
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
Kogiidae	Kogiida	k1gFnSc2	Kogiida
a	a	k8xC	a
vorvaňovi	vorvaňův	k2eAgMnPc1d1	vorvaňův
přiřadili	přiřadit	k5eAaPmAgMnP	přiřadit
binomické	binomický	k2eAgNnSc4d1	binomické
jméno	jméno	k1gNnSc4	jméno
Physeter	Physeter	k1gMnSc1	Physeter
catodon	catodon	k1gMnSc1	catodon
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
výňatkem	výňatek	k1gInSc7	výňatek
z	z	k7c2	z
Melvilleovy	Melvilleův	k2eAgFnSc2d1	Melvilleův
knihy	kniha	k1gFnSc2	kniha
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
široce	široko	k6eAd1	široko
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
pojmenování	pojmenování	k1gNnSc4	pojmenování
a	a	k8xC	a
lidové	lidový	k2eAgFnSc2d1	lidová
tradice	tradice	k1gFnSc2	tradice
související	související	k2eAgFnSc1d1	související
s	s	k7c7	s
vorvaněm	vorvaň	k1gMnSc7	vorvaň
<g/>
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
velryba	velryba	k1gFnSc1	velryba
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
starým	starý	k2eAgMnPc3d1	starý
Angličanům	Angličan	k1gMnPc3	Angličan
pod	pod	k7c7	pod
neurčitým	určitý	k2eNgNnSc7d1	neurčité
jménem	jméno	k1gNnSc7	jméno
velryba	velryba	k1gFnSc1	velryba
trumpa	trumpa	k1gFnSc1	trumpa
a	a	k8xC	a
physeter	physeter	k1gInSc1	physeter
nebo	nebo	k8xC	nebo
velryba	velryba	k1gFnSc1	velryba
s	s	k7c7	s
kovadlinovou	kovadlinový	k2eAgFnSc7d1	kovadlinový
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnešní	dnešní	k2eAgInSc1d1	dnešní
cachalot	cachalot	k1gInSc1	cachalot
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Pottfish	Pottfish	k1gInSc1	Pottfish
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
macrocephalus	macrocephalus	k1gInSc1	macrocephalus
v	v	k7c6	v
učeném	učený	k2eAgInSc6d1	učený
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
část	část	k1gFnSc1	část
textu	text	k1gInSc2	text
vynechána	vynechat	k5eAaPmNgFnS	vynechat
<g/>
]	]	kIx)	]
Teď	teď	k6eAd1	teď
se	se	k3xPyFc4	se
zabýváme	zabývat	k5eAaImIp1nP	zabývat
hlavně	hlavně	k9	hlavně
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
filologického	filologický	k2eAgNnSc2d1	filologické
je	být	k5eAaImIp3nS	být
nesmyslné	smyslný	k2eNgNnSc1d1	nesmyslné
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
několika	několik	k4yIc7	několik
staletími	staletí	k1gNnPc7	staletí
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vlastní	vlastní	k2eAgFnSc1d1	vlastní
hodnota	hodnota	k1gFnSc1	hodnota
vorvaně	vorvaň	k1gMnSc2	vorvaň
byla	být	k5eAaImAgFnS	být
téměř	téměř	k6eAd1	téměř
naprosto	naprosto	k6eAd1	naprosto
neznáma	neznámo	k1gNnSc2	neznámo
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
tuk	tuk	k1gInSc1	tuk
získával	získávat	k5eAaImAgInS	získávat
jen	jen	k9	jen
náhodou	náhodou	k6eAd1	náhodou
z	z	k7c2	z
ryby	ryba	k1gFnSc2	ryba
vyvržené	vyvržený	k2eAgFnSc2d1	vyvržená
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
patrně	patrně	k6eAd1	patrně
všeobecně	všeobecně	k6eAd1	všeobecně
usuzováno	usuzován	k2eAgNnSc1d1	usuzováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
vorvaninu	vorvanina	k1gFnSc4	vorvanina
lze	lze	k6eAd1	lze
vytěžit	vytěžit	k5eAaPmF	vytěžit
z	z	k7c2	z
tvora	tvor	k1gMnSc2	tvor
totožného	totožný	k2eAgMnSc2d1	totožný
s	s	k7c7	s
velrybou	velryba	k1gFnSc7	velryba
grónskou	grónský	k2eAgFnSc7d1	grónská
neboli	neboli	k8xC	neboli
pravou	pravá	k1gFnSc7	pravá
<g/>
,	,	kIx,	,
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
už	už	k6eAd1	už
známou	známý	k2eAgFnSc4d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
převládal	převládat	k5eAaImAgInS	převládat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
vorvanina	vorvanina	k1gFnSc1	vorvanina
neboli	neboli	k8xC	neboli
spermacet	spermacet	k1gInSc1	spermacet
je	být	k5eAaImIp3nS	být
sražená	sražený	k2eAgFnSc1d1	sražená
šťáva	šťáva	k1gFnSc1	šťáva
grónské	grónský	k2eAgFnSc2d1	grónská
velryby	velryba	k1gFnSc2	velryba
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
označuje	označovat	k5eAaImIp3nS	označovat
první	první	k4xOgFnSc1	první
půlka	půlka	k1gFnSc1	půlka
cizího	cizí	k2eAgNnSc2d1	cizí
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oněch	onen	k3xDgFnPc6	onen
dobách	doba	k1gFnPc6	doba
byla	být	k5eAaImAgFnS	být
vorvanina	vorvanina	k1gFnSc1	vorvanina
nesmírně	smírně	k6eNd1	smírně
vzácná	vzácný	k2eAgFnSc1d1	vzácná
a	a	k8xC	a
neužívalo	užívat	k5eNaImAgNnS	užívat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
k	k	k7c3	k
svícení	svícení	k1gNnSc3	svícení
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
jako	jako	k9	jako
masti	mast	k1gFnSc2	mast
a	a	k8xC	a
léku	lék	k1gInSc2	lék
<g/>
.	.	kIx.	.
</s>
<s>
Kupovala	kupovat	k5eAaImAgFnS	kupovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
v	v	k7c6	v
lékárně	lékárna	k1gFnSc6	lékárna
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tam	tam	k6eAd1	tam
dnes	dnes	k6eAd1	dnes
kupujeme	kupovat	k5eAaImIp1nP	kupovat
unci	unce	k1gFnSc4	unce
reveně	reveň	k1gFnSc2	reveň
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
domnívám	domnívat	k5eAaImIp1nS	domnívat
<g/>
,	,	kIx,	,
postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
objevena	objeven	k2eAgFnSc1d1	objevena
pravá	pravý	k2eAgFnSc1d1	pravá
podstata	podstata	k1gFnSc1	podstata
vorvaniny	vorvanina	k1gFnSc2	vorvanina
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
nezměnili	změnit	k5eNaPmAgMnP	změnit
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
nepochybně	pochybně	k6eNd1	pochybně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zvýšili	zvýšit	k5eAaPmAgMnP	zvýšit
její	její	k3xOp3gFnSc4	její
cenu	cena	k1gFnSc4	cena
tak	tak	k6eAd1	tak
podivným	podivný	k2eAgInSc7d1	podivný
náznakem	náznak	k1gInSc7	náznak
její	její	k3xOp3gFnSc2	její
vzácnosti	vzácnost	k1gFnSc2	vzácnost
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
to	ten	k3xDgNnSc1	ten
pojmenování	pojmenování	k1gNnSc1	pojmenování
posléze	posléze	k6eAd1	posléze
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dáno	dát	k5eAaPmNgNnS	dát
velrybě	velryba	k1gFnSc3	velryba
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yIgFnSc2	který
spermacet	spermacet	k1gInSc1	spermacet
skutečně	skutečně	k6eAd1	skutečně
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
z	z	k7c2	z
Melvillovy	Melvillův	k2eAgFnSc2d1	Melvillova
knihy	kniha	k1gFnSc2	kniha
Bílá	bílý	k2eAgFnSc1d1	bílá
velryba	velryba	k1gFnSc1	velryba
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Kornelová	Kornelová	k1gFnSc1	Kornelová
<g/>
,	,	kIx,	,
Kosatík	Kosatík	k1gMnSc1	Kosatík
<g/>
,	,	kIx,	,
Bednář	Bednář	k1gMnSc1	Bednář
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902223	[number]	k4	902223
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Moby	Moba	k1gFnPc1	Moba
Dick	Dicka	k1gFnPc2	Dicka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kapitoly	kapitola	k1gFnPc1	kapitola
32	[number]	k4	32
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
"	"	kIx"	"
<g/>
Cetologie	Cetologie	k1gFnSc2	Cetologie
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
str	str	kA	str
<g/>
.	.	kIx.	.
181	[number]	k4	181
<g/>
–	–	k?	–
<g/>
182	[number]	k4	182
<g/>
]	]	kIx)	]
Vědci	vědec	k1gMnPc1	vědec
soudí	soudit	k5eAaImIp3nP	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vorvani	vorvaň	k1gMnPc1	vorvaň
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
podřádu	podřád	k1gInSc2	podřád
oddělili	oddělit	k5eAaPmAgMnP	oddělit
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
ozubených	ozubený	k2eAgFnPc2d1	ozubená
velryb	velryba	k1gFnPc2	velryba
poměrně	poměrně	k6eAd1	poměrně
brzy	brzy	k6eAd1	brzy
–	–	k?	–
asi	asi	k9	asi
před	před	k7c7	před
dvaceti	dvacet	k4xCc7	dvacet
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Spermacet	spermacet	k1gInSc1	spermacet
je	být	k5eAaImIp3nS	být
polotekutá	polotekutý	k2eAgFnSc1d1	polotekutá
<g/>
,	,	kIx,	,
voskovitá	voskovitý	k2eAgFnSc1d1	voskovitá
substance	substance	k1gFnSc1	substance
obsažená	obsažený	k2eAgFnSc1d1	obsažená
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
vorvaně	vorvaň	k1gMnSc2	vorvaň
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
pozdně	pozdně	k6eAd1	pozdně
latinského	latinský	k2eAgInSc2d1	latinský
sperma	sperma	k1gNnSc4	sperma
ceti	cet	k1gFnPc1	cet
(	(	kIx(	(
<g/>
obě	dva	k4xCgNnPc4	dva
slova	slovo	k1gNnPc4	slovo
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vypůjčená	vypůjčený	k2eAgFnSc1d1	vypůjčená
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
)	)	kIx)	)
znamenajícího	znamenající	k2eAgNnSc2d1	znamenající
"	"	kIx"	"
<g/>
sperma	sperma	k1gNnSc1	sperma
velryby	velryba	k1gFnSc2	velryba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslovně	doslovně	k6eAd1	doslovně
"	"	kIx"	"
<g/>
sperma	sperma	k1gNnSc4	sperma
mořské	mořský	k2eAgFnSc2d1	mořská
nestvůry	nestvůra	k1gFnSc2	nestvůra
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
od	od	k7c2	od
spermacetu	spermacet	k1gInSc2	spermacet
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
lidový	lidový	k2eAgInSc4d1	lidový
název	název	k1gInSc4	název
této	tento	k3xDgFnSc2	tento
velryby	velryba	k1gFnSc2	velryba
(	(	kIx(	(
<g/>
Sperm	Sperm	k1gInSc1	Sperm
Whale	Whale	k1gFnSc2	Whale
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Substance	substance	k1gFnSc1	substance
samozřejmě	samozřejmě	k6eAd1	samozřejmě
není	být	k5eNaImIp3nS	být
velrybím	velrybí	k2eAgNnSc7d1	velrybí
semenem	semeno	k1gNnSc7	semeno
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
záměna	záměna	k1gFnSc1	záměna
byla	být	k5eAaImAgFnS	být
omylem	omyl	k1gInSc7	omyl
prvních	první	k4xOgInPc2	první
velrybářů	velrybář	k1gMnPc2	velrybář
<g/>
.	.	kIx.	.
</s>
<s>
Spermacet	spermacet	k1gInSc1	spermacet
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
spermacetovém	spermacetový	k2eAgInSc6d1	spermacetový
orgánu	orgán	k1gInSc6	orgán
neboli	neboli	k8xC	neboli
v	v	k7c6	v
hlavových	hlavový	k2eAgFnPc6d1	hlavová
dutinách	dutina	k1gFnPc6	dutina
vpředu	vpředu	k6eAd1	vpředu
a	a	k8xC	a
nahoře	nahoře	k6eAd1	nahoře
nad	nad	k7c7	nad
lebkou	lebka	k1gFnSc7	lebka
velryby	velryba	k1gFnSc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
hmotu	hmota	k1gFnSc4	hmota
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
přední	přední	k2eAgFnSc2d1	přední
části	část	k1gFnSc2	část
hlavy	hlava	k1gFnSc2	hlava
velryby	velryba	k1gFnSc2	velryba
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
nad	nad	k7c7	nad
horní	horní	k2eAgFnSc7d1	horní
čelistí	čelist	k1gFnSc7	čelist
<g/>
,	,	kIx,	,
nazývají	nazývat	k5eAaImIp3nP	nazývat
velrybáři	velrybář	k1gMnPc1	velrybář
junk	junk	k6eAd1	junk
(	(	kIx(	(
<g/>
v	v	k7c6	v
doslovném	doslovný	k2eAgInSc6d1	doslovný
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
odpad	odpad	k1gInSc4	odpad
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spermacetovém	spermacetový	k2eAgInSc6d1	spermacetový
orgánu	orgán	k1gInSc6	orgán
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
měkká	měkký	k2eAgFnSc1d1	měkká
bílá	bílý	k2eAgFnSc1d1	bílá
substance	substance	k1gFnSc1	substance
plná	plný	k2eAgFnSc1d1	plná
spermacetu	spermacet	k1gInSc2	spermacet
<g/>
.	.	kIx.	.
</s>
<s>
Junk	Junk	k1gInSc1	Junk
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
pevnější	pevný	k2eAgFnSc7d2	pevnější
substancí	substance	k1gFnSc7	substance
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
funkce	funkce	k1gFnSc1	funkce
spermacetu	spermacet	k1gInSc2	spermacet
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
jím	on	k3xPp3gMnSc7	on
vyplněných	vyplněný	k2eAgInPc6d1	vyplněný
není	být	k5eNaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
přinejmenším	přinejmenším	k6eAd1	přinejmenším
tři	tři	k4xCgFnPc4	tři
(	(	kIx(	(
<g/>
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
nevylučující	vylučující	k2eNgFnPc1d1	nevylučující
<g/>
)	)	kIx)	)
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
:	:	kIx,	:
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
ostatně	ostatně	k6eAd1	ostatně
už	už	k6eAd1	už
v	v	k7c6	v
Melvillově	Melvillův	k2eAgFnSc6d1	Melvillova
Moby	Moba	k1gFnPc4	Moba
Dickovi	Dicek	k1gMnSc3	Dicek
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
jistý	jistý	k2eAgInSc1d1	jistý
druh	druh	k1gInSc1	druh
beranidla	beranidlo	k1gNnSc2	beranidlo
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
mezi	mezi	k7c7	mezi
samci	samec	k1gMnPc7	samec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
dobře	dobře	k6eAd1	dobře
zdokumentovanému	zdokumentovaný	k2eAgNnSc3d1	zdokumentované
potopení	potopení	k1gNnSc3	potopení
lodí	loď	k1gFnPc2	loď
Essex	Essex	k1gInSc1	Essex
a	a	k8xC	a
Ann	Ann	k1gFnSc1	Ann
Alexander	Alexandra	k1gFnPc2	Alexandra
způsobenému	způsobený	k2eAgInSc3d1	způsobený
útoky	útok	k1gInPc7	útok
vorvaňů	vorvaň	k1gMnPc2	vorvaň
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
váha	váha	k1gFnSc1	váha
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnout	k5eAaPmNgFnS	odhadnout
na	na	k7c4	na
pouhou	pouhý	k2eAgFnSc4d1	pouhá
jednu	jeden	k4xCgFnSc4	jeden
pětinu	pětina	k1gFnSc4	pětina
váhy	váha	k1gFnSc2	váha
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
módě	móda	k1gFnSc6	móda
považovat	považovat	k5eAaImF	považovat
spermacet	spermacet	k1gInSc4	spermacet
za	za	k7c4	za
prostředek	prostředek	k1gInSc4	prostředek
sexuálního	sexuální	k2eAgInSc2d1	sexuální
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
pevně	pevně	k6eAd1	pevně
zakořeněnou	zakořeněný	k2eAgFnSc7d1	zakořeněná
představou	představa	k1gFnSc7	představa
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
velrybě	velryba	k1gFnSc3	velryba
regulovat	regulovat	k5eAaImF	regulovat
vztlak	vztlak	k1gInSc4	vztlak
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
voskové	voskový	k2eAgFnSc2d1	vosková
substance	substance	k1gFnSc2	substance
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
jejím	její	k3xOp3gNnSc7	její
ochlazováním	ochlazování	k1gNnSc7	ochlazování
vodou	voda	k1gFnSc7	voda
protékající	protékající	k2eAgFnSc4d1	protékající
výdechovým	výdechový	k2eAgInSc7d1	výdechový
otvorem	otvor	k1gInSc7	otvor
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
a	a	k8xC	a
pomáhat	pomáhat	k5eAaImF	pomáhat
velrybě	velryba	k1gFnSc3	velryba
ponořit	ponořit	k5eAaPmF	ponořit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
vytlačování	vytlačování	k1gNnSc4	vytlačování
teplé	teplý	k2eAgFnSc2d1	teplá
vody	voda	k1gFnSc2	voda
dýchacím	dýchací	k2eAgInSc7d1	dýchací
otvorem	otvor	k1gInSc7	otvor
ven	ven	k6eAd1	ven
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
spermacet	spermacet	k1gInSc4	spermacet
zahřívat	zahřívat	k5eAaImF	zahřívat
<g/>
,	,	kIx,	,
snížit	snížit	k5eAaPmF	snížit
jeho	jeho	k3xOp3gFnSc4	jeho
hustotu	hustota	k1gFnSc4	hustota
a	a	k8xC	a
ulehčit	ulehčit	k5eAaPmF	ulehčit
velrybě	velryba	k1gFnSc3	velryba
vynořování	vynořování	k1gNnSc2	vynořování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
však	však	k9	však
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
na	na	k7c4	na
veřejnosti	veřejnost	k1gFnPc4	veřejnost
populární	populární	k2eAgFnSc2d1	populární
teorie	teorie	k1gFnSc2	teorie
zpochybněna	zpochybnit	k5eAaPmNgFnS	zpochybnit
její	její	k3xOp3gFnSc1	její
hodnověrnost	hodnověrnost	k1gFnSc1	hodnověrnost
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
výzkum	výzkum	k1gInSc1	výzkum
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapilární	kapilární	k2eAgInPc1d1	kapilární
jevy	jev	k1gInPc1	jev
nemohou	moct	k5eNaImIp3nP	moct
mít	mít	k5eAaImF	mít
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
významně	významně	k6eAd1	významně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
vznosnost	vznosnost	k1gFnSc4	vznosnost
50	[number]	k4	50
<g/>
tunové	tunový	k2eAgFnSc2d1	tunová
velryby	velryba	k1gFnSc2	velryba
(	(	kIx(	(
<g/>
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Teda	Ted	k1gMnSc2	Ted
Cranforda	Cranford	k1gMnSc2	Cranford
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc4	seznam
listin	listina	k1gFnPc2	listina
popisující	popisující	k2eAgInPc1d1	popisující
detaily	detail	k1gInPc1	detail
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Jako	jako	k8xS	jako
třetí	třetí	k4xOgFnSc4	třetí
možnost	možnost	k1gFnSc4	možnost
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
věci	věc	k1gFnSc6	věc
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spermacet	spermacet	k1gInSc1	spermacet
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
echolokaci	echolokace	k1gFnSc4	echolokace
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
tohoto	tento	k3xDgInSc2	tento
orgánu	orgán	k1gInSc2	orgán
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
okamžiku	okamžik	k1gInSc6	okamžik
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zaostřování	zaostřování	k1gNnSc1	zaostřování
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
kužele	kužel	k1gInSc2	kužel
vysílaného	vysílaný	k2eAgInSc2d1	vysílaný
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Zvukové	zvukový	k2eAgFnPc1d1	zvuková
vlny	vlna	k1gFnPc1	vlna
lze	lze	k6eAd1	lze
soustředit	soustředit	k5eAaPmF	soustředit
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohou	moct	k5eAaImIp3nP	moct
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
zneschopňující	zneschopňující	k2eAgFnSc4d1	zneschopňující
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
dočasně	dočasně	k6eAd1	dočasně
paralyzující	paralyzující	k2eAgFnSc4d1	paralyzující
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
možností	možnost	k1gFnPc2	možnost
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
proudu	proud	k1gInSc6	proud
<g/>
.	.	kIx.	.
</s>
<s>
Spermacet	spermacet	k1gInSc1	spermacet
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
vyhledávaný	vyhledávaný	k2eAgMnSc1d1	vyhledávaný
velrybáři	velrybář	k1gMnPc1	velrybář
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Substance	substance	k1gFnSc1	substance
našla	najít	k5eAaPmAgFnS	najít
mnoho	mnoho	k4c4	mnoho
různých	různý	k2eAgNnPc2d1	různé
komerčních	komerční	k2eAgNnPc2d1	komerční
využití	využití	k1gNnPc2	využití
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
olej	olej	k1gInSc1	olej
do	do	k7c2	do
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
,	,	kIx,	,
kapalina	kapalina	k1gFnSc1	kapalina
pro	pro	k7c4	pro
automatickou	automatický	k2eAgFnSc4d1	automatická
převodovku	převodovka	k1gFnSc4	převodovka
<g/>
,	,	kIx,	,
mazivo	mazivo	k1gNnSc4	mazivo
pro	pro	k7c4	pro
jemné	jemný	k2eAgInPc4d1	jemný
výškové	výškový	k2eAgInPc4d1	výškový
přístroje	přístroj	k1gInPc4	přístroj
<g/>
,	,	kIx,	,
kosmetiku	kosmetika	k1gFnSc4	kosmetika
<g/>
,	,	kIx,	,
aditiva	aditivum	k1gNnSc2	aditivum
do	do	k7c2	do
motorových	motorový	k2eAgInPc2d1	motorový
olejů	olej	k1gInPc2	olej
<g/>
,	,	kIx,	,
glycerinu	glycerin	k1gInSc2	glycerin
<g/>
,	,	kIx,	,
protikorozních	protikorozní	k2eAgFnPc2d1	protikorozní
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
čisticích	čisticí	k2eAgInPc2d1	čisticí
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
chemických	chemický	k2eAgFnPc2d1	chemická
vláken	vlákna	k1gFnPc2	vlákna
<g/>
,	,	kIx,	,
vitaminů	vitamin	k1gInPc2	vitamin
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
farmaceutických	farmaceutický	k2eAgFnPc2d1	farmaceutická
směsí	směs	k1gFnPc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Vorvani	vorvaň	k1gMnPc1	vorvaň
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vorvaňovcem	vorvaňovec	k1gMnSc7	vorvaňovec
anarnakem	anarnak	k1gMnSc7	anarnak
a	a	k8xC	a
vorvaňovcem	vorvaňovec	k1gInSc7	vorvaňovec
plochočelým	plochočelý	k2eAgInSc7d1	plochočelý
nejhlouběji	hluboko	k6eAd3	hluboko
se	se	k3xPyFc4	se
potápějící	potápějící	k2eAgMnPc1d1	potápějící
savci	savec	k1gMnPc1	savec
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Věří	věřit	k5eAaImIp3nS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
potápět	potápět	k5eAaImF	potápět
až	až	k9	až
k	k	k7c3	k
mořskému	mořský	k2eAgNnSc3d1	mořské
dnu	dno	k1gNnSc3	dno
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
3	[number]	k4	3
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dvou	dva	k4xCgFnPc2	dva
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgInPc1d1	typický
ponory	ponor	k1gInPc1	ponor
směřují	směřovat	k5eAaImIp3nP	směřovat
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
trvají	trvat	k5eAaImIp3nP	trvat
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
několika	několik	k4yIc7	několik
druhy	druh	k1gInPc1	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
krakaticemi	krakatice	k1gFnPc7	krakatice
<g/>
,	,	kIx,	,
chobotnicemi	chobotnice	k1gFnPc7	chobotnice
a	a	k8xC	a
živočichy	živočich	k1gMnPc7	živočich
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
víme	vědět	k5eAaImIp1nP	vědět
o	o	k7c6	o
hlubokomořských	hlubokomořský	k2eAgFnPc6d1	hlubokomořská
krakaticích	krakatice	k1gFnPc6	krakatice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vědci	vědec	k1gMnPc7	vědec
dověděli	dovědět	k5eAaPmAgMnP	dovědět
z	z	k7c2	z
exemplářů	exemplář	k1gInPc2	exemplář
nalezených	nalezený	k2eAgInPc2d1	nalezený
v	v	k7c6	v
žaludcích	žaludek	k1gInPc6	žaludek
vorvaňů	vorvaň	k1gMnPc2	vorvaň
<g/>
.	.	kIx.	.
</s>
<s>
Historky	historka	k1gFnPc1	historka
o	o	k7c6	o
titánských	titánský	k2eAgFnPc6d1	titánská
bitvách	bitva	k1gFnPc6	bitva
mezi	mezi	k7c7	mezi
vorvani	vorvaň	k1gMnPc7	vorvaň
a	a	k8xC	a
krakaticemi	krakatice	k1gFnPc7	krakatice
dorůstajícími	dorůstající	k2eAgInPc7d1	dorůstající
10	[number]	k4	10
metrů	metr	k1gInPc2	metr
délky	délka	k1gFnSc2	délka
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
jsou	být	k5eAaImIp3nP	být
nejspíš	nejspíš	k9	nejspíš
jen	jen	k9	jen
součástí	součást	k1gFnSc7	součást
legend	legenda	k1gFnPc2	legenda
–	–	k?	–
podobné	podobný	k2eAgFnSc2d1	podobná
bitvy	bitva	k1gFnSc2	bitva
nebyly	být	k5eNaImAgInP	být
nikdy	nikdy	k6eAd1	nikdy
pozorovány	pozorován	k2eAgInPc1d1	pozorován
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bílé	bílý	k2eAgFnPc1d1	bílá
jizvy	jizva	k1gFnPc1	jizva
na	na	k7c6	na
tělech	tělo	k1gNnPc6	tělo
vorvaňů	vorvaň	k1gMnPc2	vorvaň
byly	být	k5eAaImAgInP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
krakaticemi	krakatice	k1gFnPc7	krakatice
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
hypotéza	hypotéza	k1gFnSc1	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostrý	ostrý	k2eAgInSc1d1	ostrý
zobák	zobák	k1gInSc1	zobák
zkonzumované	zkonzumovaný	k2eAgFnSc2d1	zkonzumovaná
krakatice	krakatice	k1gFnSc2	krakatice
usazený	usazený	k2eAgInSc4d1	usazený
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
velryby	velryba	k1gFnSc2	velryba
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
ambry	ambra	k1gFnSc2	ambra
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
tvorby	tvorba	k1gFnSc2	tvorba
perel	perla	k1gFnPc2	perla
<g/>
.	.	kIx.	.
</s>
<s>
Vorvani	vorvaň	k1gMnPc1	vorvaň
jsou	být	k5eAaImIp3nP	být
neuvěřitelní	uvěřitelný	k2eNgMnPc1d1	neuvěřitelný
jedlíci	jedlík	k1gMnPc1	jedlík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
pozřou	pozřít	k5eAaPmIp3nP	pozřít
asi	asi	k9	asi
3	[number]	k4	3
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
tělesné	tělesný	k2eAgFnSc2d1	tělesná
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vorvani	vorvaň	k1gMnPc1	vorvaň
dohromady	dohromady	k6eAd1	dohromady
každoročně	každoročně	k6eAd1	každoročně
zkonzumují	zkonzumovat	k5eAaPmIp3nP	zkonzumovat
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
hlubokomořských	hlubokomořský	k2eAgMnPc2d1	hlubokomořský
tvorů	tvor	k1gMnPc2	tvor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
srovnatelné	srovnatelný	k2eAgNnSc1d1	srovnatelné
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
každoroční	každoroční	k2eAgFnSc7d1	každoroční
spotřebou	spotřeba	k1gFnSc7	spotřeba
mořských	mořský	k2eAgMnPc2d1	mořský
živočichů	živočich	k1gMnPc2	živočich
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Fyziologie	fyziologie	k1gFnSc1	fyziologie
vorvaňů	vorvaň	k1gMnPc2	vorvaň
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
adaptací	adaptace	k1gFnPc2	adaptace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zvládli	zvládnout	k5eAaPmAgMnP	zvládnout
drastické	drastický	k2eAgFnPc4d1	drastická
změny	změna	k1gFnPc4	změna
tlaku	tlak	k1gInSc2	tlak
při	při	k7c6	při
potápění	potápění	k1gNnSc6	potápění
<g/>
.	.	kIx.	.
</s>
<s>
Žebra	žebro	k1gNnPc1	žebro
jsou	být	k5eAaImIp3nP	být
ohebná	ohebný	k2eAgNnPc1d1	ohebné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dovolila	dovolit	k5eAaPmAgFnS	dovolit
kolaps	kolaps	k1gInSc4	kolaps
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
a	a	k8xC	a
srdeční	srdeční	k2eAgFnSc1d1	srdeční
frekvence	frekvence	k1gFnSc1	frekvence
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
snížit	snížit	k5eAaPmF	snížit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
déle	dlouho	k6eAd2	dlouho
uchovala	uchovat	k5eAaPmAgFnS	uchovat
zásoba	zásoba	k1gFnSc1	zásoba
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Myoglobin	Myoglobin	k2eAgInSc1d1	Myoglobin
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
kyslík	kyslík	k1gInSc1	kyslík
ve	v	k7c6	v
svalové	svalový	k2eAgFnSc6d1	svalová
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
úroveň	úroveň	k1gFnSc1	úroveň
kyslíku	kyslík	k1gInSc2	kyslík
sníží	snížit	k5eAaPmIp3nS	snížit
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
krev	krev	k1gFnSc4	krev
nasměrovat	nasměrovat	k5eAaPmF	nasměrovat
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
jiným	jiný	k2eAgInPc3d1	jiný
důležitým	důležitý	k2eAgInPc3d1	důležitý
orgánům	orgán	k1gInPc3	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
i	i	k8xC	i
spermacetový	spermacetový	k2eAgInSc4d1	spermacetový
orgán	orgán	k1gInSc4	orgán
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
výše	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
vorvani	vorvaň	k1gMnPc1	vorvaň
dobře	dobře	k6eAd1	dobře
adaptováni	adaptován	k2eAgMnPc1d1	adaptován
k	k	k7c3	k
potápění	potápění	k1gNnSc3	potápění
<g/>
,	,	kIx,	,
opakované	opakovaný	k2eAgInPc1d1	opakovaný
ponory	ponor	k1gInPc1	ponor
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
hloubek	hloubka	k1gFnPc2	hloubka
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
mají	mít	k5eAaImIp3nP	mít
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
neblahý	blahý	k2eNgInSc4d1	neblahý
dopad	dopad	k1gInSc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Kostry	kostra	k1gFnPc1	kostra
vorvaňů	vorvaň	k1gMnPc2	vorvaň
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
důlkovou	důlkový	k2eAgFnSc4d1	důlková
korozi	koroze	k1gFnSc4	koroze
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
často	často	k6eAd1	často
známkou	známka	k1gFnSc7	známka
dekompresní	dekompresní	k2eAgFnSc2d1	dekompresní
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Kostry	kostra	k1gFnPc1	kostra
nejstarších	starý	k2eAgFnPc2d3	nejstarší
velryb	velryba	k1gFnPc2	velryba
jsou	být	k5eAaImIp3nP	být
postiženy	postižen	k2eAgInPc1d1	postižen
nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3	nejrozsáhlejší
korozí	koroze	k1gFnSc7	koroze
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
kostry	kostra	k1gFnPc1	kostra
mláďat	mládě	k1gNnPc2	mládě
žádná	žádný	k3yNgFnSc1	žádný
poškození	poškození	k1gNnSc4	poškození
nejeví	jevit	k5eNaImIp3nS	jevit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
poškození	poškození	k1gNnSc1	poškození
může	moct	k5eAaImIp3nS	moct
signalizovat	signalizovat	k5eAaImF	signalizovat
citlivost	citlivost	k1gFnSc1	citlivost
vorvaňů	vorvaň	k1gMnPc2	vorvaň
na	na	k7c4	na
dekompresní	dekompresní	k2eAgFnSc4d1	dekompresní
nemoc	nemoc	k1gFnSc4	nemoc
a	a	k8xC	a
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
rychlé	rychlý	k2eAgNnSc4d1	rychlé
vynoření	vynoření	k1gNnSc4	vynoření
smrtelné	smrtelný	k2eAgNnSc4d1	smrtelné
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ponory	ponor	k1gInPc4	ponor
se	se	k3xPyFc4	se
vorvaň	vorvaň	k1gMnSc1	vorvaň
vynořuje	vynořovat	k5eAaImIp3nS	vynořovat
na	na	k7c4	na
hladinu	hladina	k1gFnSc4	hladina
k	k	k7c3	k
nadechnutí	nadechnutí	k1gNnSc3	nadechnutí
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zde	zde	k6eAd1	zde
většinou	většina	k1gFnSc7	většina
na	na	k7c4	na
osm	osm	k4xCc4	osm
až	až	k9	až
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
před	před	k7c7	před
dalším	další	k2eAgInSc7d1	další
ponorem	ponor	k1gInSc7	ponor
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
struktura	struktura	k1gFnSc1	struktura
vorvaňů	vorvaň	k1gMnPc2	vorvaň
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
extrémně	extrémně	k6eAd1	extrémně
družnými	družný	k2eAgMnPc7d1	družný
tvory	tvor	k1gMnPc7	tvor
–	–	k?	–
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
díky	díky	k7c3	díky
relativně	relativně	k6eAd1	relativně
jednoduché	jednoduchý	k2eAgFnSc3d1	jednoduchá
evoluční	evoluční	k2eAgFnSc3d1	evoluční
cestě	cesta	k1gFnSc3	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
o	o	k7c6	o
asi	asi	k9	asi
tuctu	tucet	k1gInSc3	tucet
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
opouštějí	opouštět	k5eAaImIp3nP	opouštět
své	svůj	k3xOyFgFnPc4	svůj
"	"	kIx"	"
<g/>
mateřské	mateřský	k2eAgFnPc4d1	mateřská
školky	školka	k1gFnPc4	školka
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
4	[number]	k4	4
a	a	k8xC	a
21	[number]	k4	21
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
připojují	připojovat	k5eAaImIp3nP	připojovat
se	se	k3xPyFc4	se
k	k	k7c3	k
"	"	kIx"	"
<g/>
mládeneckým	mládenecký	k2eAgFnPc3d1	mládenecká
školám	škola	k1gFnPc3	škola
<g/>
"	"	kIx"	"
tvořenými	tvořený	k2eAgMnPc7d1	tvořený
jinými	jiný	k2eAgMnPc7d1	jiný
samci	samec	k1gMnPc7	samec
podobného	podobný	k2eAgInSc2d1	podobný
věku	věk	k1gInSc2	věk
a	a	k8xC	a
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
samci	samec	k1gMnPc1	samec
stárnou	stárnout	k5eAaImIp3nP	stárnout
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc4	sklon
se	se	k3xPyFc4	se
rozptýlit	rozptýlit	k5eAaPmF	rozptýlit
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
skupinky	skupinka	k1gFnPc4	skupinka
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnPc1d3	nejstarší
samci	samec	k1gMnPc1	samec
žijí	žít	k5eAaImIp3nP	žít
obvykle	obvykle	k6eAd1	obvykle
samotářským	samotářský	k2eAgInSc7d1	samotářský
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
přesto	přesto	k8xC	přesto
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
problémech	problém	k1gInPc6	problém
pospolu	pospolu	k6eAd1	pospolu
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
stupeň	stupeň	k1gInSc4	stupeň
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zcela	zcela	k6eAd1	zcela
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Vorvani	vorvaň	k1gMnPc1	vorvaň
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
druhy	druh	k1gInPc4	druh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
hojní	hojnit	k5eAaImIp3nS	hojnit
od	od	k7c2	od
arktických	arktický	k2eAgFnPc2d1	arktická
vod	voda	k1gFnPc2	voda
až	až	k9	až
k	k	k7c3	k
rovníku	rovník	k1gInSc3	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
hustší	hustý	k2eAgFnSc1d2	hustší
při	při	k7c6	při
kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
šelfu	šelf	k1gInSc6	šelf
a	a	k8xC	a
kaňonech	kaňon	k1gInPc6	kaňon
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
snadnějšímu	snadný	k2eAgNnSc3d2	snazší
shánění	shánění	k1gNnSc3	shánění
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Vorvaně	vorvaň	k1gMnPc4	vorvaň
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
v	v	k7c6	v
nehlubokých	hluboký	k2eNgFnPc6d1	nehluboká
vodách	voda	k1gFnPc6	voda
při	při	k7c6	při
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
zde	zde	k6eAd1	zde
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
šelf	šelf	k1gInSc1	šelf
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
vorvaňů	vorvaň	k1gMnPc2	vorvaň
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
není	být	k5eNaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
<g/>
.	.	kIx.	.
</s>
<s>
Hrubé	Hrubé	k2eAgInPc1d1	Hrubé
odhady	odhad	k1gInPc1	odhad
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
průzkumech	průzkum	k1gInPc6	průzkum
malých	malý	k2eAgFnPc2d1	malá
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
extrapolaci	extrapolace	k1gFnSc4	extrapolace
výsledků	výsledek	k1gInPc2	výsledek
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
oceány	oceán	k1gInPc4	oceán
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
200	[number]	k4	200
000	[number]	k4	000
do	do	k7c2	do
600	[number]	k4	600
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
(	(	kIx(	(
<g/>
nejspíše	nejspíše	k9	nejspíše
okolo	okolo	k7c2	okolo
360	[number]	k4	360
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jsou	být	k5eAaImIp3nP	být
vorvani	vorvaň	k1gMnPc1	vorvaň
loveni	loven	k2eAgMnPc1d1	loven
po	po	k7c6	po
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
kvůli	kvůli	k7c3	kvůli
masu	maso	k1gNnSc3	maso
<g/>
,	,	kIx,	,
oleji	olej	k1gInSc3	olej
(	(	kIx(	(
<g/>
užívanému	užívaný	k2eAgNnSc3d1	užívané
jako	jako	k8xC	jako
mazadlo	mazadlo	k1gNnSc1	mazadlo
ve	v	k7c6	v
strojírenství	strojírenství	k1gNnSc6	strojírenství
<g/>
)	)	kIx)	)
a	a	k8xC	a
spermacetu	spermacet	k1gInSc3	spermacet
(	(	kIx(	(
<g/>
užívanému	užívaný	k2eAgNnSc3d1	užívané
ve	v	k7c6	v
svíčkách	svíčka	k1gFnPc6	svíčka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
vorvaně	vorvaň	k1gMnSc4	vorvaň
je	být	k5eAaImIp3nS	být
optimističtější	optimistický	k2eAgMnSc1d2	optimističtější
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
velryb	velryba	k1gFnPc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
příbřežního	příbřežní	k2eAgInSc2d1	příbřežní
rybolovu	rybolov	k1gInSc2	rybolov
malých	malý	k2eAgInPc2d1	malý
rozměrů	rozměr	k1gInPc2	rozměr
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
jsou	být	k5eAaImIp3nP	být
vorvani	vorvaň	k1gMnPc1	vorvaň
chráněni	chránit	k5eAaImNgMnP	chránit
prakticky	prakticky	k6eAd1	prakticky
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
.	.	kIx.	.
</s>
<s>
Rybáři	rybář	k1gMnPc1	rybář
neloví	lovit	k5eNaImIp3nP	lovit
hlubokomořské	hlubokomořský	k2eAgMnPc4d1	hlubokomořský
tvory	tvor	k1gMnPc4	tvor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
potravou	potrava	k1gFnSc7	potrava
vorvaňů	vorvaň	k1gMnPc2	vorvaň
a	a	k8xC	a
hluboké	hluboký	k2eAgNnSc1d1	hluboké
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mnohem	mnohem	k6eAd1	mnohem
odolnější	odolný	k2eAgMnSc1d2	odolnější
k	k	k7c3	k
znečištění	znečištění	k1gNnSc3	znečištění
než	než	k8xS	než
vrstvy	vrstva	k1gFnSc2	vrstva
u	u	k7c2	u
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Zotavení	zotavení	k1gNnSc1	zotavení
z	z	k7c2	z
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
velrybářských	velrybářský	k2eAgNnPc2d1	velrybářské
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
pomalý	pomalý	k2eAgInSc1d1	pomalý
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
přinejmenším	přinejmenším	k6eAd1	přinejmenším
v	v	k7c6	v
Jižním	jižní	k2eAgInSc6d1	jižní
pacifiku	pacifik	k1gInSc6	pacifik
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
samců	samec	k1gMnPc2	samec
schopných	schopný	k2eAgMnPc2d1	schopný
rozmnožování	rozmnožování	k1gNnPc4	rozmnožování
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgFnPc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
vorvaňů	vorvaň	k1gMnPc2	vorvaň
není	být	k5eNaImIp3nS	být
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgFnPc3d1	jiná
velrybám	velryba	k1gFnPc3	velryba
právě	právě	k6eAd1	právě
nejjednodušší	jednoduchý	k2eAgInSc4d3	nejjednodušší
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gFnPc3	jejich
dlouhým	dlouhý	k2eAgFnPc3d1	dlouhá
dobám	doba	k1gFnPc3	doba
ponoru	ponor	k1gInSc2	ponor
a	a	k8xC	a
schopnosti	schopnost	k1gFnSc2	schopnost
urazit	urazit	k5eAaPmF	urazit
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
jsou	být	k5eAaImIp3nP	být
pozorování	pozorování	k1gNnSc4	pozorování
stále	stále	k6eAd1	stále
populárnější	populární	k2eAgInSc1d2	populárnější
především	především	k9	především
díky	díky	k7c3	díky
charakteristickému	charakteristický	k2eAgInSc3d1	charakteristický
vzhledu	vzhled	k1gInSc3	vzhled
a	a	k8xC	a
rozměru	rozměr	k1gInSc3	rozměr
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
často	často	k6eAd1	často
užívají	užívat	k5eAaImIp3nP	užívat
hydrofony	hydrofon	k1gInPc1	hydrofon
k	k	k7c3	k
naslouchání	naslouchání	k1gNnSc3	naslouchání
cvakání	cvakání	k1gNnSc2	cvakání
velryb	velryba	k1gFnPc2	velryba
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zaměření	zaměření	k1gNnSc3	zaměření
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
vynoří	vynořit	k5eAaPmIp3nS	vynořit
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
místem	místo	k1gNnSc7	místo
pozorování	pozorování	k1gNnSc2	pozorování
vorvaňů	vorvaň	k1gMnPc2	vorvaň
je	být	k5eAaImIp3nS	být
pitoreskní	pitoreskní	k2eAgFnSc1d1	pitoreskní
Kaikoura	Kaikoura	k1gFnSc1	Kaikoura
na	na	k7c6	na
novozélandském	novozélandský	k2eAgInSc6d1	novozélandský
Jižním	jižní	k2eAgInSc6d1	jižní
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
šelf	šelf	k1gInSc1	šelf
tak	tak	k6eAd1	tak
mělký	mělký	k2eAgInSc1d1	mělký
<g/>
,	,	kIx,	,
že	že	k8xS	že
lze	lze	k6eAd1	lze
velryby	velryba	k1gFnPc4	velryba
pozorovat	pozorovat	k5eAaImF	pozorovat
ze	z	k7c2	z
břehu	břeh	k1gInSc2	břeh
<g/>
,	,	kIx,	,
a	a	k8xC	a
Andenes	Andenes	k1gInSc1	Andenes
v	v	k7c6	v
arktickém	arktický	k2eAgNnSc6d1	arktické
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
jsou	být	k5eAaImIp3nP	být
vorvani	vorvaň	k1gMnPc1	vorvaň
často	často	k6eAd1	často
pozorováni	pozorován	k2eAgMnPc1d1	pozorován
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cca	cca	kA	cca
12	[number]	k4	12
<g/>
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
Alankuda	Alankudo	k1gNnSc2	Alankudo
beach	beacha	k1gFnPc2	beacha
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Kalpitiya	Kalpitiy	k1gInSc2	Kalpitiy
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc2	lance
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
v	v	k7c4	v
okolí	okolí	k1gNnSc4	okolí
Mirissy	Mirissa	k1gFnSc2	Mirissa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
pobřeží	pobřeží	k1gNnSc4	pobřeží
jižní	jižní	k2eAgNnSc4d1	jižní
Chile	Chile	k1gNnSc4	Chile
velká	velký	k2eAgFnSc1d1	velká
hrouda	hrouda	k1gFnSc1	hrouda
bílého	bílý	k2eAgNnSc2d1	bílé
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
masa	maso	k1gNnPc4	maso
rosolovité	rosolovitý	k2eAgFnSc2d1	rosolovitá
tkáně	tkáň	k1gFnSc2	tkáň
podnítila	podnítit	k5eAaPmAgFnS	podnítit
spekulace	spekulace	k1gFnSc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
dříve	dříve	k6eAd2	dříve
neznámá	známý	k2eNgFnSc1d1	neznámá
obrovská	obrovský	k2eAgFnSc1d1	obrovská
chobotnice	chobotnice	k1gFnSc1	chobotnice
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumníci	výzkumník	k1gMnPc1	výzkumník
ze	z	k7c2	z
santiagského	santiagský	k2eAgInSc2d1	santiagský
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Natural	Natural	k?	Natural
History	Histor	k1gMnPc7	Histor
usoudili	usoudit	k5eAaPmAgMnP	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrouda	hrouda	k1gFnSc1	hrouda
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vnitřnosti	vnitřnost	k1gFnSc2	vnitřnost
vorvaně	vorvaň	k1gMnSc2	vorvaň
<g/>
,	,	kIx,	,
především	především	k9	především
díky	díky	k7c3	díky
prozkoumání	prozkoumání	k1gNnSc3	prozkoumání
kožních	kožní	k2eAgFnPc2d1	kožní
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
orgány	orgán	k1gInPc1	orgán
vorvaně	vorvaň	k1gMnSc2	vorvaň
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nestanou	stanout	k5eNaPmIp3nP	stanout
polotekutou	polotekutý	k2eAgFnSc7d1	polotekutá
masou	masa	k1gFnSc7	masa
polapenou	polapený	k2eAgFnSc7d1	polapená
uvnitř	uvnitř	k7c2	uvnitř
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
zřejmě	zřejmě	k6eAd1	zřejmě
kůže	kůže	k1gFnSc1	kůže
praskla	prasknout	k5eAaPmAgFnS	prasknout
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
se	se	k3xPyFc4	se
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
vyplaven	vyplavit	k5eAaPmNgInS	vyplavit
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
vorvani	vorvaň	k1gMnPc1	vorvaň
jsou	být	k5eAaImIp3nP	být
vyplavováni	vyplavovat	k5eAaImNgMnP	vyplavovat
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
docela	docela	k6eAd1	docela
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
nakládáním	nakládání	k1gNnSc7	nakládání
a	a	k8xC	a
identifikací	identifikace	k1gFnSc7	identifikace
mrtvých	mrtvý	k2eAgNnPc2d1	mrtvé
těl	tělo	k1gNnPc2	tělo
se	s	k7c7	s
správci	správce	k1gMnPc7	správce
pláží	pláž	k1gFnSc7	pláž
obávají	obávat	k5eAaImIp3nP	obávat
žraloků	žralok	k1gMnPc2	žralok
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
žraloka	žralok	k1gMnSc2	žralok
bílého	bílé	k1gNnSc2	bílé
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
na	na	k7c4	na
pláže	pláž	k1gFnPc4	pláž
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
rozkládající	rozkládající	k2eAgNnSc1d1	rozkládající
se	se	k3xPyFc4	se
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
uvést	uvést	k5eAaPmF	uvést
návštěvníky	návštěvník	k1gMnPc7	návštěvník
pláže	pláž	k1gFnSc2	pláž
do	do	k7c2	do
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
jsou	být	k5eAaImIp3nP	být
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
vorvani	vorvaň	k1gMnPc1	vorvaň
často	často	k6eAd1	často
vlečeni	vlečen	k2eAgMnPc1d1	vlečen
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
otevřené	otevřený	k2eAgFnSc6d1	otevřená
moře	mora	k1gFnSc6	mora
dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
skutečnému	skutečný	k2eAgNnSc3d1	skutečné
vyplavení	vyplavení	k1gNnSc3	vyplavení
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2004	[number]	k4	2004
se	se	k3xPyFc4	se
podobná	podobný	k2eAgFnSc1d1	podobná
událost	událost	k1gFnSc1	událost
stala	stát	k5eAaPmAgFnS	stát
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
na	na	k7c4	na
Oahu	Oaha	k1gFnSc4	Oaha
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
velryba	velryba	k1gFnSc1	velryba
odvlečena	odvlečen	k2eAgFnSc1d1	odvlečena
35	[number]	k4	35
mil	míle	k1gFnPc2	míle
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
vyplavena	vyplavit	k5eAaPmNgFnS	vyplavit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejznámější	známý	k2eAgFnSc1d3	nejznámější
část	část	k1gFnSc1	část
lidových	lidový	k2eAgFnPc2d1	lidová
historek	historka	k1gFnPc2	historka
o	o	k7c6	o
vorvaních	vorvaní	k2eAgMnPc2d1	vorvaní
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Florence	Florenc	k1gFnSc2	Florenc
v	v	k7c6	v
Oregonu	Oregon	k1gInSc6	Oregon
vyplaven	vyplaven	k2eAgInSc4d1	vyplaven
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
7,26	[number]	k4	7,26
tunový	tunový	k2eAgInSc4d1	tunový
<g/>
,	,	kIx,	,
13,7	[number]	k4	13,7
<g/>
metrový	metrový	k2eAgInSc1d1	metrový
kus	kus	k1gInSc1	kus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
to	ten	k3xDgNnSc4	ten
pro	pro	k7c4	pro
místní	místní	k2eAgNnSc4d1	místní
byla	být	k5eAaImAgFnS	být
rarita	rarita	k1gFnSc1	rarita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
pláž	pláž	k1gFnSc1	pláž
stala	stát	k5eAaPmAgFnS	stát
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
zastávkou	zastávka	k1gFnSc7	zastávka
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dálniční	dálniční	k2eAgFnSc1d1	dálniční
policie	policie	k1gFnSc1	policie
státu	stát	k1gInSc2	stát
Oregon	Oregona	k1gFnPc2	Oregona
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
se	se	k3xPyFc4	se
mrtvoly	mrtvola	k1gFnPc4	mrtvola
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Naplnili	naplnit	k5eAaPmAgMnP	naplnit
tělo	tělo	k1gNnSc4	tělo
půl	půl	k6eAd1	půl
tunou	tuna	k1gFnSc7	tuna
dynamitu	dynamit	k1gInSc2	dynamit
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byl	být	k5eAaImAgInS	být
dynamit	dynamit	k1gInSc1	dynamit
odpálen	odpálit	k5eAaPmNgInS	odpálit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výbuch	výbuch	k1gInSc1	výbuch
nesměřoval	směřovat	k5eNaImAgInS	směřovat
na	na	k7c4	na
oceán	oceán	k1gInSc4	oceán
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nebyl	být	k5eNaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
,	,	kIx,	,
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc1	jeden
auto	auto	k1gNnSc1	auto
rozmačkáno	rozmačkat	k5eAaPmNgNnS	rozmačkat
padajícím	padající	k2eAgInSc7d1	padající
tukem	tuk	k1gInSc7	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Očití	očitý	k2eAgMnPc1d1	očitý
svědci	svědek	k1gMnPc1	svědek
byli	být	k5eAaImAgMnP	být
pokryti	pokrýt	k5eAaPmNgMnP	pokrýt
nechutně	chutně	k6eNd1	chutně
páchnoucími	páchnoucí	k2eAgInPc7d1	páchnoucí
zbytky	zbytek	k1gInPc7	zbytek
mrtvé	mrtvý	k2eAgFnSc2d1	mrtvá
velryby	velryba	k1gFnSc2	velryba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
se	se	k3xPyFc4	se
do	do	k7c2	do
hledáčku	hledáček	k1gInSc2	hledáček
světových	světový	k2eAgNnPc2d1	světové
médií	médium	k1gNnPc2	médium
dostal	dostat	k5eAaPmAgInS	dostat
dramatičtější	dramatický	k2eAgInSc1d2	dramatičtější
případ	případ	k1gInSc1	případ
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
vorvaňů	vorvaň	k1gMnPc2	vorvaň
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
tělo	tělo	k1gNnSc1	tělo
velryby	velryba	k1gFnSc2	velryba
<g/>
,	,	kIx,	,
17	[number]	k4	17
metrů	metr	k1gInPc2	metr
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
vážící	vážící	k2eAgInSc1d1	vážící
50	[number]	k4	50
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vyplaveno	vyplavit	k5eAaPmNgNnS	vyplavit
na	na	k7c4	na
pláž	pláž	k1gFnSc4	pláž
v	v	k7c6	v
tchajwanském	tchajwanský	k2eAgInSc6d1	tchajwanský
Tainan	Tainany	k1gInPc2	Tainany
City	city	k1gNnSc1	city
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
přepravy	přeprava	k1gFnSc2	přeprava
na	na	k7c4	na
městskou	městský	k2eAgFnSc4d1	městská
univerzitu	univerzita	k1gFnSc4	univerzita
způsobil	způsobit	k5eAaPmAgInS	způsobit
tlak	tlak	k1gInSc1	tlak
plynu	plyn	k1gInSc2	plyn
z	z	k7c2	z
rozkládajícího	rozkládající	k2eAgMnSc2d1	rozkládající
se	se	k3xPyFc4	se
těla	tělo	k1gNnSc2	tělo
explozi	exploze	k1gFnSc3	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nebyl	být	k5eNaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krev	krev	k1gFnSc1	krev
a	a	k8xC	a
vnitřnosti	vnitřnost	k1gFnPc1	vnitřnost
se	se	k3xPyFc4	se
rozstříkly	rozstříknout	k5eAaPmAgFnP	rozstříknout
po	po	k7c6	po
několika	několik	k4yIc6	několik
autech	aut	k1gInPc6	aut
a	a	k8xC	a
kolemjdoucích	kolemjdoucí	k2eAgFnPc6d1	kolemjdoucí
<g/>
.	.	kIx.	.
</s>
