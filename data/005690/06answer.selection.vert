<s>
Laguna	laguna	k1gFnSc1	laguna
je	být	k5eAaImIp3nS	být
mělký	mělký	k2eAgInSc1d1	mělký
mořský	mořský	k2eAgInSc1d1	mořský
vodní	vodní	k2eAgInSc1d1	vodní
útvar	útvar	k1gInSc1	útvar
naplněný	naplněný	k2eAgInSc1d1	naplněný
slanou	slaný	k2eAgFnSc7d1	slaná
nebo	nebo	k8xC	nebo
brakickou	brakický	k2eAgFnSc7d1	brakická
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
oddělen	oddělit	k5eAaPmNgInS	oddělit
od	od	k7c2	od
hlubšího	hluboký	k2eAgNnSc2d2	hlubší
moře	moře	k1gNnSc2	moře
korálovým	korálový	k2eAgInSc7d1	korálový
útesem	útes	k1gInSc7	útes
nebo	nebo	k8xC	nebo
písečným	písečný	k2eAgInSc7d1	písečný
valem	val	k1gInSc7	val
<g/>
.	.	kIx.	.
</s>
