<s>
Laguna	laguna	k1gFnSc1	laguna
je	být	k5eAaImIp3nS	být
mělký	mělký	k2eAgInSc1d1	mělký
mořský	mořský	k2eAgInSc1d1	mořský
vodní	vodní	k2eAgInSc1d1	vodní
útvar	útvar	k1gInSc1	útvar
naplněný	naplněný	k2eAgInSc1d1	naplněný
slanou	slaný	k2eAgFnSc7d1	slaná
nebo	nebo	k8xC	nebo
brakickou	brakický	k2eAgFnSc7d1	brakická
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
oddělen	oddělit	k5eAaPmNgInS	oddělit
od	od	k7c2	od
hlubšího	hluboký	k2eAgNnSc2d2	hlubší
moře	moře	k1gNnSc2	moře
korálovým	korálový	k2eAgInSc7d1	korálový
útesem	útes	k1gInSc7	útes
nebo	nebo	k8xC	nebo
písečným	písečný	k2eAgInSc7d1	písečný
valem	val	k1gInSc7	val
<g/>
.	.	kIx.	.
</s>
<s>
Laguny	laguna	k1gFnPc1	laguna
jsou	být	k5eAaImIp3nP	být
jak	jak	k6eAd1	jak
laguny	laguna	k1gFnPc1	laguna
atolů	atol	k1gInPc2	atol
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
růstem	růst	k1gInSc7	růst
korálových	korálový	k2eAgInPc2d1	korálový
útesů	útes	k1gInPc2	útes
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
laguny	laguna	k1gFnSc2	laguna
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
vznikem	vznik	k1gInSc7	vznik
písčitých	písčitý	k2eAgInPc2d1	písčitý
valů	val	k1gInPc2	val
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
laguny	laguna	k1gFnPc1	laguna
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
přílivy	příliv	k1gInPc4	příliv
malé	malý	k2eAgNnSc4d1	malé
vzedmutí	vzedmutí	k1gNnSc4	vzedmutí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
moře	moře	k1gNnSc2	moře
bývají	bývat	k5eAaImIp3nP	bývat
odděleny	oddělit	k5eAaPmNgInP	oddělit
bariérou	bariéra	k1gFnSc7	bariéra
ostrovů	ostrov	k1gInPc2	ostrov
ležícich	ležícicha	k1gFnPc2	ležícicha
obecně	obecně	k6eAd1	obecně
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
s	s	k7c7	s
pobřežím	pobřeží	k1gNnSc7	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Vlnobití	vlnobití	k1gNnSc1	vlnobití
nebo	nebo	k8xC	nebo
pobřežní	pobřežní	k2eAgInPc1d1	pobřežní
proudy	proud	k1gInPc1	proud
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zanášejí	zanášet	k5eAaImIp3nP	zanášet
hrubé	hrubý	k2eAgInPc1d1	hrubý
sedimenty	sediment	k1gInPc1	sediment
mimo	mimo	k7c4	mimo
břehy	břeh	k1gInPc4	břeh
pláží	pláž	k1gFnPc2	pláž
<g/>
,	,	kIx,	,
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
lagunové	lagunový	k2eAgInPc1d1	lagunový
valy	val	k1gInPc1	val
bez	bez	k7c2	bez
mořských	mořský	k2eAgInPc2d1	mořský
útesů	útes	k1gInPc2	útes
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
val	val	k1gInSc1	val
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
usazovat	usazovat	k5eAaImF	usazovat
i	i	k8xC	i
jemnější	jemný	k2eAgInPc4d2	jemnější
sedimenty	sediment	k1gInPc4	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
laguny	laguna	k1gFnPc1	laguna
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
pouze	pouze	k6eAd1	pouze
úzkými	úzký	k2eAgInPc7d1	úzký
průlivy	průliv	k1gInPc7	průliv
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
lagunách	laguna	k1gFnPc6	laguna
je	být	k5eAaImIp3nS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
mořské	mořský	k2eAgFnSc2d1	mořská
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
slanost	slanost	k1gFnSc4	slanost
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc4	obsah
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
sedimentů	sediment	k1gInPc2	sediment
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
kalová	kalový	k2eAgFnSc1d1	kalová
laguna	laguna	k1gFnSc1	laguna
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
kalojem	kalojem	k1gInSc1	kalojem
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
umělá	umělý	k2eAgFnSc1d1	umělá
zemní	zemní	k2eAgFnSc1d1	zemní
nádrž	nádrž	k1gFnSc1	nádrž
k	k	k7c3	k
uskladnění	uskladnění	k1gNnSc3	uskladnění
(	(	kIx(	(
<g/>
a	a	k8xC	a
usazování	usazování	k1gNnSc2	usazování
<g/>
)	)	kIx)	)
kalu	kal	k1gInSc2	kal
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
laguna	laguna	k1gFnSc1	laguna
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
geomorfologické	geomorfologický	k2eAgInPc4d1	geomorfologický
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
tak	tak	k9	tak
nazývat	nazývat	k5eAaImF	nazývat
zálivy	záliv	k1gInPc4	záliv
<g/>
,	,	kIx,	,
zátoky	zátok	k1gInPc4	zátok
<g/>
,	,	kIx,	,
limany	liman	k1gInPc4	liman
<g/>
,	,	kIx,	,
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Lagoa	Lagoa	k1gFnSc1	Lagoa
dos	dos	k?	dos
Patos	patos	k1gInSc1	patos
</s>
