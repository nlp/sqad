<s>
Termínem	termín	k1gInSc7	termín
bílý	bílý	k2eAgInSc1d1	bílý
čaj	čaj	k1gInSc1	čaj
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
takový	takový	k3xDgInSc1	takový
druh	druh	k1gInSc1	druh
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mladé	mladý	k2eAgFnSc3d1	mladá
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
lehce	lehko	k6eAd1	lehko
a	a	k8xC	a
specificky	specificky	k6eAd1	specificky
zpracované	zpracovaný	k2eAgInPc4d1	zpracovaný
lístky	lístek	k1gInPc4	lístek
čajovníku	čajovník	k1gInSc2	čajovník
čínského	čínský	k2eAgInSc2d1	čínský
(	(	kIx(	(
<g/>
Camellia	Camellia	k1gFnSc1	Camellia
sinensis	sinensis	k1gFnSc2	sinensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
