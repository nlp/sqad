<p>
<s>
Termínem	termín	k1gInSc7	termín
bílý	bílý	k2eAgInSc1d1	bílý
čaj	čaj	k1gInSc1	čaj
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
označuje	označovat	k5eAaImIp3nS	označovat
takový	takový	k3xDgInSc1	takový
druh	druh	k1gInSc1	druh
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mladé	mladý	k2eAgFnSc3d1	mladá
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
lehce	lehko	k6eAd1	lehko
a	a	k8xC	a
specificky	specificky	k6eAd1	specificky
zpracované	zpracovaný	k2eAgInPc4d1	zpracovaný
lístky	lístek	k1gInPc4	lístek
čajovníku	čajovník	k1gInSc2	čajovník
čínského	čínský	k2eAgInSc2d1	čínský
(	(	kIx(	(
<g/>
Camellia	Camellia	k1gFnSc1	Camellia
sinensis	sinensis	k1gFnSc2	sinensis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepanuje	panovat	k5eNaImIp3nS	panovat
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
shoda	shoda	k1gFnSc1	shoda
na	na	k7c4	na
definici	definice	k1gFnSc4	definice
bílého	bílý	k2eAgInSc2d1	bílý
čaje	čaj	k1gInSc2	čaj
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
používají	používat	k5eAaImIp3nP	používat
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
sušen	sušen	k2eAgInSc1d1	sušen
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
není	být	k5eNaImIp3nS	být
zpracováván	zpracováván	k2eAgMnSc1d1	zpracováván
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
tak	tak	k6eAd1	tak
označují	označovat	k5eAaImIp3nP	označovat
čaj	čaj	k1gInSc4	čaj
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
z	z	k7c2	z
pupenů	pupen	k1gInPc2	pupen
a	a	k8xC	a
nezralých	zralý	k2eNgInPc2d1	nezralý
čajových	čajový	k2eAgInPc2d1	čajový
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
sklízejí	sklízet	k5eAaImIp3nP	sklízet
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
pupeny	pupen	k1gInPc1	pupen
zcela	zcela	k6eAd1	zcela
otevřou	otevřít	k5eAaPmIp3nP	otevřít
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
nechají	nechat	k5eAaPmIp3nP	nechat
zavadnout	zavadnout	k5eAaPmF	zavadnout
a	a	k8xC	a
usušit	usušit	k5eAaPmF	usušit
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiní	jiný	k1gMnPc1	jiný
sem	sem	k6eAd1	sem
řadí	řadit	k5eAaImIp3nP	řadit
pupeny	pupen	k1gInPc4	pupen
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
mladé	mladý	k2eAgInPc4d1	mladý
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
před	před	k7c7	před
sušením	sušení	k1gNnSc7	sušení
spařeny	spařit	k5eAaPmNgFnP	spařit
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
deaktivoval	deaktivovat	k5eAaImAgInS	deaktivovat
enzym	enzym	k1gInSc1	enzym
polyfenoloxidáza	polyfenoloxidáza	k1gFnSc1	polyfenoloxidáza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
definic	definice	k1gFnPc2	definice
se	se	k3xPyFc4	se
ale	ale	k9	ale
shoduje	shodovat	k5eAaImIp3nS	shodovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
lístky	lístek	k1gInPc4	lístek
bílého	bílý	k2eAgInSc2d1	bílý
čaje	čaj	k1gInSc2	čaj
nejsou	být	k5eNaImIp3nP	být
svinovány	svinován	k2eAgFnPc1d1	svinován
ani	ani	k8xC	ani
oxidovány	oxidován	k2eAgFnPc1d1	oxidována
<g/>
;	;	kIx,	;
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
jemnější	jemný	k2eAgFnSc4d2	jemnější
a	a	k8xC	a
sladší	sladký	k2eAgFnSc4d2	sladší
chuť	chuť	k1gFnSc4	chuť
než	než	k8xS	než
u	u	k7c2	u
zelených	zelený	k2eAgInPc2d1	zelený
nebo	nebo	k8xC	nebo
tradičních	tradiční	k2eAgInPc2d1	tradiční
černých	černý	k2eAgInPc2d1	černý
čajů	čaj	k1gInPc2	čaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
čaj	čaj	k1gInSc1	čaj
dostal	dostat	k5eAaPmAgInS	dostat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
jemných	jemný	k2eAgInPc2d1	jemný
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílých	bílý	k2eAgInPc2d1	bílý
chloupků	chloupek	k1gInPc2	chloupek
na	na	k7c6	na
neotevřených	otevřený	k2eNgInPc6d1	neotevřený
pupenech	pupen	k1gInPc6	pupen
čajovníku	čajovník	k1gInSc2	čajovník
<g/>
.	.	kIx.	.
</s>
<s>
Nálev	nálev	k1gInSc1	nálev
z	z	k7c2	z
bílého	bílý	k2eAgInSc2d1	bílý
čaje	čaj	k1gInSc2	čaj
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bledě	bledě	k6eAd1	bledě
žlutý	žlutý	k2eAgMnSc1d1	žlutý
až	až	k8xS	až
žlutohnědý	žlutohnědý	k2eAgMnSc1d1	žlutohnědý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
čaj	čaj	k1gInSc1	čaj
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
sklízí	sklízet	k5eAaImIp3nS	sklízet
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Fu-ťien	Fu-ťina	k1gFnPc2	Fu-ťina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
i	i	k9	i
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Nepálu	Nepál	k1gInSc6	Nepál
<g/>
,	,	kIx,	,
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
,	,	kIx,	,
severním	severní	k2eAgNnSc6d1	severní
Thajsku	Thajsko	k1gNnSc6	Thajsko
<g/>
,	,	kIx,	,
Galle	Galla	k1gFnSc6	Galla
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc2d1	jižní
Srí	Srí	k1gFnSc2	Srí
Lance	lance	k1gNnSc2	lance
<g/>
)	)	kIx)	)
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
White	Whit	k1gInSc5	Whit
tea	tea	k1gFnSc1	tea
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bílý	bílý	k2eAgInSc4d1	bílý
čaj	čaj	k1gInSc4	čaj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
