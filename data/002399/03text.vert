<s>
Listerióza	Listerióza	k1gFnSc1	Listerióza
je	být	k5eAaImIp3nS	být
infekční	infekční	k2eAgNnSc1d1	infekční
onemocnění	onemocnění	k1gNnSc1	onemocnění
způsobené	způsobený	k2eAgNnSc1d1	způsobené
grampozitivní	grampozitivní	k2eAgFnSc7d1	grampozitivní
bakterií	bakterie	k1gFnSc7	bakterie
Listeria	Listerium	k1gNnSc2	Listerium
monocytogenes	monocytogenes	k1gMnSc1	monocytogenes
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
spíše	spíše	k9	spíše
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
i	i	k9	i
k	k	k7c3	k
epidemiím	epidemie	k1gFnPc3	epidemie
<g/>
,	,	kIx,	,
listerióza	listerióza	k1gFnSc1	listerióza
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
nemocniční	nemocniční	k2eAgFnSc7d1	nemocniční
nákazou	nákaza	k1gFnSc7	nákaza
(	(	kIx(	(
<g/>
nosokomiální	nosokomiální	k2eAgInSc1d1	nosokomiální
výskyt	výskyt	k1gInSc1	výskyt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zdravých	zdravý	k2eAgMnPc2d1	zdravý
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
imunitou	imunita	k1gFnSc7	imunita
probíhá	probíhat	k5eAaImIp3nS	probíhat
bezpříznakově	bezpříznakově	k6eAd1	bezpříznakově
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
onemocnění	onemocnění	k1gNnSc1	onemocnění
podobné	podobný	k2eAgNnSc1d1	podobné
chřipce	chřipka	k1gFnSc3	chřipka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
potratů	potrat	k1gInPc2	potrat
u	u	k7c2	u
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
celkových	celkový	k2eAgInPc2d1	celkový
septických	septický	k2eAgInPc2d1	septický
onemocnění	onemocnění	k1gNnSc4	onemocnění
novorozenců	novorozenec	k1gMnPc2	novorozenec
a	a	k8xC	a
zánětů	zánět	k1gInPc2	zánět
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
mozkových	mozkový	k2eAgFnPc2d1	mozková
blan	blána	k1gFnPc2	blána
u	u	k7c2	u
starých	starý	k2eAgFnPc2d1	stará
a	a	k8xC	a
oslabených	oslabený	k2eAgFnPc2d1	oslabená
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
listeriózy	listerióza	k1gFnSc2	listerióza
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
grampozitivní	grampozitivní	k2eAgFnSc1d1	grampozitivní
tyčinka	tyčinka	k1gFnSc1	tyčinka
<g/>
,	,	kIx,	,
L.	L.	kA	L.
monocytogenes	monocytogenes	k1gInSc1	monocytogenes
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
fakultativní	fakultativní	k2eAgMnSc1d1	fakultativní
intracelulární	intracelulární	k2eAgMnSc1d1	intracelulární
parazit	parazit	k1gMnSc1	parazit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
přežívat	přežívat	k5eAaImF	přežívat
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
i	i	k8xC	i
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
10	[number]	k4	10
<g/>
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
tuto	tento	k3xDgFnSc4	tento
bakterii	bakterie	k1gFnSc4	bakterie
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
projevily	projevit	k5eAaPmAgFnP	projevit
příznaky	příznak	k1gInPc4	příznak
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Listerióza	Listerióza	k1gFnSc1	Listerióza
je	být	k5eAaImIp3nS	být
zoonóza	zoonóza	k1gFnSc1	zoonóza
<g/>
,	,	kIx,	,
onemocnět	onemocnět	k5eAaPmF	onemocnět
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
zvířata	zvíře	k1gNnPc1	zvíře
včetně	včetně	k7c2	včetně
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
mezi	mezi	k7c7	mezi
zvířaty	zvíře	k1gNnPc7	zvíře
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
bezsymptomatičtí	bezsymptomatický	k2eAgMnPc1d1	bezsymptomatický
nosiči	nosič	k1gMnPc1	nosič
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
bakterii	bakterie	k1gFnSc4	bakterie
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
tělními	tělní	k2eAgInPc7d1	tělní
sekrety	sekret	k1gInPc7	sekret
včetně	včetně	k7c2	včetně
výkalů	výkal	k1gInPc2	výkal
a	a	k8xC	a
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jsou	být	k5eAaImIp3nP	být
rezervoárem	rezervoár	k1gInSc7	rezervoár
nákazy	nákaza	k1gFnSc2	nákaza
především	především	k9	především
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Známa	znám	k2eAgFnSc1d1	známa
je	být	k5eAaImIp3nS	být
také	také	k9	také
listerióza	listerióza	k1gFnSc1	listerióza
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Listerióza	Listerióza	k1gFnSc1	Listerióza
je	být	k5eAaImIp3nS	být
alimentární	alimentární	k2eAgNnPc4d1	alimentární
onemocnění	onemocnění	k1gNnPc4	onemocnění
<g/>
,	,	kIx,	,
k	k	k7c3	k
nákaze	nákaza	k1gFnSc3	nákaza
dochází	docházet	k5eAaImIp3nS	docházet
nejčastěji	často	k6eAd3	často
požitím	požití	k1gNnSc7	požití
kontaminované	kontaminovaný	k2eAgFnSc2d1	kontaminovaná
potraviny	potravina	k1gFnSc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
bakteriálními	bakteriální	k2eAgFnPc7d1	bakteriální
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
přenášejí	přenášet	k5eAaImIp3nP	přenášet
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
:	:	kIx,	:
Alimentární	alimentární	k2eAgFnSc1d1	alimentární
infekce	infekce	k1gFnSc1	infekce
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
-	-	kIx~	-
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
výskyt	výskyt	k1gInSc1	výskyt
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Údaje	údaj	k1gInSc2	údaj
z	z	k7c2	z
Centra	centrum	k1gNnSc2	centrum
epidemiologie	epidemiologie	k1gFnSc2	epidemiologie
a	a	k8xC	a
Mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
nakazí	nakazit	k5eAaPmIp3nP	nakazit
kojenci	kojenec	k1gMnPc1	kojenec
mladší	mladý	k2eAgMnPc1d2	mladší
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
staří	starý	k2eAgMnPc1d1	starý
lidé	člověk	k1gMnPc1	člověk
nad	nad	k7c4	nad
60	[number]	k4	60
let	léto	k1gNnPc2	léto
,	,	kIx,	,
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
30	[number]	k4	30
<g/>
%	%	kIx~	%
případů	případ	k1gInPc2	případ
jsou	být	k5eAaImIp3nP	být
pacientkami	pacientka	k1gFnPc7	pacientka
těhotné	těhotný	k2eAgFnSc2d1	těhotná
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Těhotné	těhotný	k2eAgFnPc1d1	těhotná
ženy	žena	k1gFnPc1	žena
ve	v	k7c6	v
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
od	od	k7c2	od
10	[number]	k4	10
do	do	k7c2	do
40	[number]	k4	40
let	léto	k1gNnPc2	léto
tvoří	tvořit	k5eAaImIp3nS	tvořit
dokonce	dokonce	k9	dokonce
60	[number]	k4	60
<g/>
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
zaznamenaných	zaznamenaný	k2eAgInPc2d1	zaznamenaný
případů	případ	k1gInPc2	případ
listeriózy	listerióza	k1gFnSc2	listerióza
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
ohroženou	ohrožený	k2eAgFnSc7d1	ohrožená
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
byl	být	k5eAaImAgMnS	být
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
oslaben	oslaben	k2eAgMnSc1d1	oslaben
<g/>
:	:	kIx,	:
onkologičtí	onkologický	k2eAgMnPc1d1	onkologický
pacienti	pacient	k1gMnPc1	pacient
<g/>
,	,	kIx,	,
HIV	HIV	kA	HIV
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
onemocněním	onemocnění	k1gNnSc7	onemocnění
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
diabetici	diabetik	k1gMnPc1	diabetik
nebo	nebo	k8xC	nebo
pacienti	pacient	k1gMnPc1	pacient
po	po	k7c6	po
transplantacích	transplantace	k1gFnPc6	transplantace
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
užívají	užívat	k5eAaImIp3nP	užívat
léky	lék	k1gInPc4	lék
potlačující	potlačující	k2eAgFnSc1d1	potlačující
imunitní	imunitní	k2eAgFnSc1d1	imunitní
odpověď	odpověď	k1gFnSc1	odpověď
organismu	organismus	k1gInSc2	organismus
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
užívající	užívající	k2eAgInPc1d1	užívající
léky	lék	k1gInPc1	lék
obsahující	obsahující	k2eAgInPc1d1	obsahující
glukokortikoidy	glukokortikoid	k1gInPc1	glukokortikoid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
listerióza	listerióza	k1gFnSc1	listerióza
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
jako	jako	k9	jako
jednotlivé	jednotlivý	k2eAgNnSc4d1	jednotlivé
<g/>
,	,	kIx,	,
sporadické	sporadický	k2eAgNnSc4d1	sporadické
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
obtížné	obtížný	k2eAgNnSc1d1	obtížné
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
nákaza	nákaza	k1gFnSc1	nákaza
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kontaminaci	kontaminace	k1gFnSc6	kontaminace
potravin	potravina	k1gFnPc2	potravina
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
nebo	nebo	k8xC	nebo
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
<g/>
,	,	kIx,	,
dostanou	dostat	k5eAaPmIp3nP	dostat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
takové	takový	k3xDgFnPc1	takový
potraviny	potravina	k1gFnPc1	potravina
ke	k	k7c3	k
většímu	veliký	k2eAgNnSc3d2	veliký
množství	množství	k1gNnSc3	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
dochází	docházet	k5eAaImIp3nS	docházet
nejčastěji	často	k6eAd3	často
perorálně	perorálně	k6eAd1	perorálně
infikovanými	infikovaný	k2eAgFnPc7d1	infikovaná
potravinami	potravina	k1gFnPc7	potravina
<g/>
,	,	kIx,	,
k	k	k7c3	k
pomnožení	pomnožení	k1gNnSc3	pomnožení
L.	L.	kA	L.
monocytogenes	monocytogenes	k1gInSc4	monocytogenes
v	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
při	při	k7c6	při
primární	primární	k2eAgFnSc6d1	primární
kontaminaci	kontaminace	k1gFnSc6	kontaminace
syrových	syrový	k2eAgFnPc2d1	syrová
potravin	potravina	k1gFnPc2	potravina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nedostatečně	dostatečně	k6eNd1	dostatečně
pasterizované	pasterizovaný	k2eAgNnSc4d1	pasterizované
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
měkké	měkký	k2eAgInPc4d1	měkký
sýry	sýr	k1gInPc4	sýr
<g/>
,	,	kIx,	,
paštika	paštika	k1gFnSc1	paštika
<g/>
,	,	kIx,	,
nemytá	mytý	k2eNgFnSc1d1	nemytá
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
při	při	k7c6	při
sekundární	sekundární	k2eAgFnSc6d1	sekundární
kontaminaci	kontaminace	k1gFnSc6	kontaminace
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vlastního	vlastní	k2eAgNnSc2d1	vlastní
vaření	vaření	k1gNnSc2	vaření
a	a	k8xC	a
uchování	uchování	k1gNnSc2	uchování
hotových	hotový	k2eAgInPc2d1	hotový
pokrmů	pokrm	k1gInPc2	pokrm
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
zkřížené	zkřížený	k2eAgFnSc6d1	zkřížená
kontaminaci	kontaminace	k1gFnSc6	kontaminace
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
při	při	k7c6	při
nedostatečné	dostatečný	k2eNgFnSc6d1	nedostatečná
osobní	osobní	k2eAgFnSc6d1	osobní
hygieně	hygiena	k1gFnSc6	hygiena
pracovníků	pracovník	k1gMnPc2	pracovník
manipulujících	manipulující	k2eAgMnPc2d1	manipulující
s	s	k7c7	s
potravinami	potravina	k1gFnPc7	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
rizikové	rizikový	k2eAgFnPc4d1	riziková
potraviny	potravina	k1gFnPc4	potravina
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
nákazy	nákaza	k1gFnSc2	nákaza
listeriózou	listerióza	k1gFnSc7	listerióza
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
nepasterizované	pasterizovaný	k2eNgInPc1d1	nepasterizovaný
výrobky	výrobek	k1gInPc1	výrobek
<g/>
,	,	kIx,	,
nedostatečně	dostatečně	k6eNd1	dostatečně
pasterizované	pasterizovaný	k2eAgNnSc4d1	pasterizované
mléko	mléko	k1gNnSc4	mléko
lahůdkové	lahůdkový	k2eAgInPc4d1	lahůdkový
výrobky	výrobek	k1gInPc4	výrobek
saláty	salát	k1gInPc4	salát
uzeniny	uzenina	k1gFnSc2	uzenina
zrající	zrající	k2eAgInPc4d1	zrající
sýry	sýr	k1gInPc4	sýr
nemytá	mytý	k2eNgFnSc1d1	nemytá
zelenina	zelenina	k1gFnSc1	zelenina
nedostatečně	dostatečně	k6eNd1	dostatečně
tepelně	tepelně	k6eAd1	tepelně
zpracované	zpracovaný	k2eAgInPc4d1	zpracovaný
pokrmy	pokrm	k1gInPc4	pokrm
ohřívané	ohřívaný	k2eAgInPc4d1	ohřívaný
pokrmy	pokrm	k1gInPc4	pokrm
Obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
rizikem	riziko	k1gNnSc7	riziko
všechny	všechen	k3xTgFnPc4	všechen
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
vlastní	vlastní	k2eAgFnSc7d1	vlastní
konzumací	konzumace	k1gFnSc7	konzumace
tepelně	tepelně	k6eAd1	tepelně
neupravují	upravovat	k5eNaImIp3nP	upravovat
a	a	k8xC	a
pokrmy	pokrm	k1gInPc4	pokrm
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
dlouho	dlouho	k6eAd1	dlouho
skladovány	skladovat	k5eAaImNgInP	skladovat
v	v	k7c6	v
chladničce	chladnička	k1gFnSc6	chladnička
-	-	kIx~	-
L.	L.	kA	L.
monocytogenes	monocytogenes	k1gInSc1	monocytogenes
se	se	k3xPyFc4	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
množit	množit	k5eAaImF	množit
i	i	k9	i
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
z	z	k7c2	z
původně	původně	k6eAd1	původně
neškodné	škodný	k2eNgFnSc2d1	neškodná
potraviny	potravina	k1gFnSc2	potravina
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
líhniště	líhniště	k1gNnSc4	líhniště
listérií	listérie	k1gFnPc2	listérie
<g/>
.	.	kIx.	.
</s>
<s>
Listérie	Listérie	k1gFnSc1	Listérie
se	se	k3xPyFc4	se
zničí	zničit	k5eAaPmIp3nS	zničit
varem	var	k1gInSc7	var
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
přenosu	přenos	k1gInSc2	přenos
je	být	k5eAaImIp3nS	být
vertikální	vertikální	k2eAgFnSc1d1	vertikální
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
plod	plod	k1gInSc1	plod
nebo	nebo	k8xC	nebo
novorozenec	novorozenec	k1gMnSc1	novorozenec
nakazí	nakazit	k5eAaPmIp3nS	nakazit
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
in	in	k?	in
utero	utero	k6eAd1	utero
nebo	nebo	k8xC	nebo
během	během	k7c2	během
porodu	porod	k1gInSc2	porod
stykem	styk	k1gInSc7	styk
s	s	k7c7	s
vaginálními	vaginální	k2eAgInPc7d1	vaginální
sekrety	sekret	k1gInPc7	sekret
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
živé	živý	k2eAgFnPc1d1	živá
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Možný	možný	k2eAgInSc1d1	možný
je	být	k5eAaImIp3nS	být
také	také	k9	také
přenos	přenos	k1gInSc1	přenos
kontaminovanými	kontaminovaný	k2eAgFnPc7d1	kontaminovaná
pomůckami	pomůcka	k1gFnPc7	pomůcka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
popsány	popsat	k5eAaPmNgInP	popsat
hromadné	hromadný	k2eAgInPc1d1	hromadný
výskyty	výskyt	k1gInPc1	výskyt
na	na	k7c6	na
novorozeneckých	novorozenecký	k2eAgNnPc6d1	novorozenecké
odděleních	oddělení	k1gNnPc6	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Vzácnější	vzácný	k2eAgInPc4d2	vzácnější
popsané	popsaný	k2eAgInPc4d1	popsaný
případy	případ	k1gInPc4	případ
je	být	k5eAaImIp3nS	být
nákaza	nákaza	k1gFnSc1	nákaza
skrze	skrze	k?	skrze
spojivku	spojivka	k1gFnSc4	spojivka
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
popsaná	popsaný	k2eAgFnSc1d1	popsaná
u	u	k7c2	u
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
drůbežích	drůbeží	k2eAgFnPc2d1	drůbeží
jatek	jatka	k1gFnPc2	jatka
<g/>
,	,	kIx,	,
a	a	k8xC	a
kožní	kožní	k2eAgFnSc1d1	kožní
forma	forma	k1gFnSc1	forma
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
u	u	k7c2	u
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
s	s	k7c7	s
nemocnými	nemocný	k2eAgMnPc7d1	nemocný
novorozenci	novorozenec	k1gMnPc7	novorozenec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
veterinářů	veterinář	k1gMnPc2	veterinář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
nemocnými	mocný	k2eNgNnPc7d1	nemocné
zvířaty	zvíře	k1gNnPc7	zvíře
nebo	nebo	k8xC	nebo
potracenými	potracený	k2eAgInPc7d1	potracený
plody	plod	k1gInPc7	plod
<g/>
.	.	kIx.	.
</s>
<s>
Infekční	infekční	k2eAgFnSc1d1	infekční
dávka	dávka	k1gFnSc1	dávka
u	u	k7c2	u
citlivých	citlivý	k2eAgFnPc2d1	citlivá
osob	osoba	k1gFnPc2	osoba
může	moct	k5eAaImIp3nS	moct
činit	činit	k5eAaImF	činit
pouhých	pouhý	k2eAgFnPc2d1	pouhá
1000	[number]	k4	1000
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
buněk	buňka	k1gFnPc2	buňka
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
záleží	záležet	k5eAaImIp3nS	záležet
také	také	k9	také
na	na	k7c4	na
virulenci	virulence	k1gFnSc4	virulence
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
u	u	k7c2	u
listeriózy	listerióza	k1gFnSc2	listerióza
není	být	k5eNaImIp3nS	být
její	její	k3xOp3gNnSc4	její
rozšíření	rozšíření	k1gNnSc4	rozšíření
a	a	k8xC	a
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
velká	velký	k2eAgFnSc1d1	velká
mortalita	mortalita	k1gFnSc1	mortalita
u	u	k7c2	u
vnímavých	vnímavý	k2eAgFnPc2d1	vnímavá
skupin	skupina	k1gFnPc2	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
mortalita	mortalita	k1gFnSc1	mortalita
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
nad	nad	k7c4	nad
60	[number]	k4	60
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
již	již	k6eAd1	již
blíží	blížit	k5eAaImIp3nS	blížit
60	[number]	k4	60
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
neléčená	léčený	k2eNgFnSc1d1	neléčená
encefalická	encefalický	k2eAgFnSc1d1	encefalický
forma	forma	k1gFnSc1	forma
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
i	i	k9	i
v	v	k7c6	v
70	[number]	k4	70
<g/>
%	%	kIx~	%
případů	případ	k1gInPc2	případ
.	.	kIx.	.
</s>
<s>
Listerióza	Listerióza	k1gFnSc1	Listerióza
často	často	k6eAd1	často
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
nenarozeného	narozený	k2eNgInSc2d1	nenarozený
plodu	plod	k1gInSc2	plod
a	a	k8xC	a
k	k	k7c3	k
potratu	potrat	k1gInSc3	potrat
<g/>
,	,	kIx,	,
smrtnost	smrtnost	k1gFnSc1	smrtnost
novorozenců	novorozenec	k1gMnPc2	novorozenec
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
<g/>
%	%	kIx~	%
U	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
zdravých	zdravý	k2eAgFnPc2d1	zdravá
osob	osoba	k1gFnPc2	osoba
naopak	naopak	k6eAd1	naopak
často	často	k6eAd1	často
probíhá	probíhat	k5eAaImIp3nS	probíhat
bezpříznakově	bezpříznakově	k6eAd1	bezpříznakově
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
případy	případ	k1gInPc1	případ
často	často	k6eAd1	často
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
neodhaleny	odhalen	k2eNgFnPc1d1	neodhalena
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
nemocné	mocný	k2eNgFnPc1d1	mocný
osoby	osoba	k1gFnPc1	osoba
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
bakterie	bakterie	k1gFnPc4	bakterie
v	v	k7c6	v
tělních	tělní	k2eAgInPc6d1	tělní
sekretech	sekret	k1gInPc6	sekret
a	a	k8xC	a
ve	v	k7c6	v
výkalech	výkal	k1gInPc6	výkal
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
inkubační	inkubační	k2eAgFnSc6d1	inkubační
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
nejčastěji	často	k6eAd3	často
kolem	kolem	k7c2	kolem
3	[number]	k4	3
týdnů	týden	k1gInPc2	týden
(	(	kIx(	(
<g/>
rozpětí	rozpětí	k1gNnSc2	rozpětí
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
70	[number]	k4	70
<g/>
dnů	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
symptomy	symptom	k1gInPc4	symptom
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Listerióza	Listerióza	k1gFnSc1	Listerióza
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevovat	projevovat	k5eAaImF	projevovat
několika	několik	k4yIc7	několik
odlišnými	odlišný	k2eAgInPc7d1	odlišný
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
Febrilní	febrilní	k2eAgFnSc1d1	febrilní
gastroenteritida	gastroenteritida	k1gFnSc1	gastroenteritida
Tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
listeriózy	listerióza	k1gFnSc2	listerióza
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
u	u	k7c2	u
předtím	předtím	k6eAd1	předtím
zdravých	zdravý	k2eAgMnPc2d1	zdravý
<g/>
,	,	kIx,	,
netěhotných	těhotný	k2eNgMnPc2d1	netěhotný
dospělých	dospělí	k1gMnPc2	dospělí
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
do	do	k7c2	do
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
2	[number]	k4	2
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
vodnatým	vodnatý	k2eAgInSc7d1	vodnatý
průjmem	průjem	k1gInSc7	průjem
<g/>
,	,	kIx,	,
nevolností	nevolnost	k1gFnSc7	nevolnost
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
bolesti	bolest	k1gFnSc3	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
kloubů	kloub	k1gInPc2	kloub
a	a	k8xC	a
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
<g/>
monocytogenes	monocytogenes	k1gInSc1	monocytogenes
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
ve	v	k7c6	v
střevech	střevo	k1gNnPc6	střevo
<g/>
,	,	kIx,	,
nešíří	šířit	k5eNaImIp3nP	šířit
se	se	k3xPyFc4	se
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Bakterémie	Bakterémie	k1gFnSc1	Bakterémie
Napadne	napadnout	k5eAaPmIp3nS	napadnout
<g/>
-li	i	k?	-li
L.	L.	kA	L.
monocytogenes	monocytogenes	k1gMnSc1	monocytogenes
bílé	bílý	k2eAgFnSc2d1	bílá
krvinky	krvinka	k1gFnSc2	krvinka
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Bakterémie	Bakterémie	k1gFnSc1	Bakterémie
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
nacházejí	nacházet	k5eAaImIp3nP	nacházet
živé	živý	k2eAgFnPc1d1	živá
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Klinicky	klinicky	k6eAd1	klinicky
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
akutní	akutní	k2eAgNnSc4d1	akutní
horečnaté	horečnatý	k2eAgNnSc4d1	horečnaté
onemocnění	onemocnění	k1gNnSc4	onemocnění
provázené	provázený	k2eAgNnSc4d1	provázené
bolestmi	bolest	k1gFnPc7	bolest
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
kloubů	kloub	k1gInPc2	kloub
<g/>
,	,	kIx,	,
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
zad	záda	k1gNnPc2	záda
<g/>
,	,	kIx,	,
můžou	můžou	k?	můžou
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
průjmy	průjem	k1gInPc4	průjem
nebo	nebo	k8xC	nebo
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
postihuje	postihovat	k5eAaImIp3nS	postihovat
dospělé	dospělý	k2eAgFnPc4d1	dospělá
netěhotné	těhotný	k2eNgFnPc4d1	netěhotná
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
oslabení	oslabení	k1gNnSc6	oslabení
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
může	moct	k5eAaImIp3nS	moct
přejít	přejít	k5eAaPmF	přejít
v	v	k7c4	v
encefalickou	encefalický	k2eAgFnSc4d1	encefalický
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Encefalická	Encefalický	k2eAgFnSc1d1	Encefalický
forma	forma	k1gFnSc1	forma
Tato	tento	k3xDgFnSc1	tento
vážná	vážný	k2eAgFnSc1d1	vážná
forma	forma	k1gFnSc1	forma
listeriózy	listerióza	k1gFnSc2	listerióza
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
u	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
,	,	kIx,	,
starých	starý	k2eAgFnPc2d1	stará
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
osob	osoba	k1gFnPc2	osoba
oslabených	oslabený	k2eAgFnPc2d1	oslabená
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
nádorová	nádorový	k2eAgNnPc4d1	nádorové
onemocnění	onemocnění	k1gNnPc4	onemocnění
<g/>
,	,	kIx,	,
HIV	HIV	kA	HIV
nebo	nebo	k8xC	nebo
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
diabetes	diabetes	k1gInSc1	diabetes
<g/>
,	,	kIx,	,
cirhóza	cirhóza	k1gFnSc1	cirhóza
jater	játra	k1gNnPc2	játra
nebo	nebo	k8xC	nebo
astma	astma	k1gFnSc1	astma
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
po	po	k7c6	po
transplantacích	transplantace	k1gFnPc6	transplantace
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
užívají	užívat	k5eAaImIp3nP	užívat
léky	lék	k1gInPc4	lék
potlačující	potlačující	k2eAgFnSc1d1	potlačující
imunitní	imunitní	k2eAgFnSc1d1	imunitní
odpověď	odpověď	k1gFnSc1	odpověď
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
monocytogenes	monocytogenes	k1gMnSc1	monocytogenes
má	mít	k5eAaImIp3nS	mít
afinitu	afinita	k1gFnSc4	afinita
k	k	k7c3	k
mozkové	mozkový	k2eAgFnSc3d1	mozková
tkáni	tkáň	k1gFnSc3	tkáň
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
k	k	k7c3	k
mozkovému	mozkový	k2eAgInSc3d1	mozkový
kmeni	kmen	k1gInSc3	kmen
a	a	k8xC	a
obalům	obal	k1gInPc3	obal
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
a	a	k8xC	a
nezabrání	zabránit	k5eNaPmIp3nP	zabránit
<g/>
-li	i	k?	-li
imunitní	imunitní	k2eAgInSc1d1	imunitní
systém	systém	k1gInSc1	systém
šíření	šíření	k1gNnSc2	šíření
nákazy	nákaza	k1gFnSc2	nákaza
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
<g/>
,	,	kIx,	,
projeví	projevit	k5eAaPmIp3nS	projevit
se	se	k3xPyFc4	se
listerióza	listerióza	k1gFnSc1	listerióza
jako	jako	k8xC	jako
meningitida	meningitida	k1gFnSc1	meningitida
<g/>
,	,	kIx,	,
<g/>
meningoencefalitida	meningoencefalitida	k1gFnSc1	meningoencefalitida
nebo	nebo	k8xC	nebo
encefalitida	encefalitida	k1gFnSc1	encefalitida
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
změnami	změna	k1gFnPc7	změna
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
,	,	kIx,	,
křečemi	křeč	k1gFnPc7	křeč
<g/>
,	,	kIx,	,
zmateností	zmatenost	k1gFnSc7	zmatenost
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
nervovými	nervový	k2eAgInPc7d1	nervový
příznaky	příznak	k1gInPc7	příznak
<g/>
,	,	kIx,	,
v	v	k7c6	v
pokročilém	pokročilý	k2eAgNnSc6d1	pokročilé
stadiu	stadion	k1gNnSc6	stadion
kómatem	kóma	k1gNnSc7	kóma
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
monocytogenes	monocytogenes	k1gInSc1	monocytogenes
je	být	k5eAaImIp3nS	být
až	až	k9	až
pátá	pátý	k4xOgFnSc1	pátý
nejčastější	častý	k2eAgFnSc1d3	nejčastější
příčina	příčina	k1gFnSc1	příčina
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zato	zato	k6eAd1	zato
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
největší	veliký	k2eAgFnSc4d3	veliký
mortalitu	mortalita	k1gFnSc4	mortalita
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
%	%	kIx~	%
.	.	kIx.	.
</s>
<s>
Hnisavá	hnisavý	k2eAgFnSc1d1	hnisavá
forma	forma	k1gFnSc1	forma
encefalitidy	encefalitida	k1gFnSc2	encefalitida
je	být	k5eAaImIp3nS	být
běžná	běžný	k2eAgFnSc1d1	běžná
u	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
,	,	kIx,	,
po	po	k7c6	po
vyléčení	vyléčení	k1gNnSc6	vyléčení
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
hydrocefalu	hydrocefal	k1gInSc3	hydrocefal
nebo	nebo	k8xC	nebo
mentálnímu	mentální	k2eAgNnSc3d1	mentální
postižení	postižení	k1gNnSc3	postižení
<g/>
.	.	kIx.	.
</s>
<s>
Rhomboencefalitida	Rhomboencefalitida	k1gFnSc1	Rhomboencefalitida
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zánět	zánět	k1gInSc1	zánět
mozkového	mozkový	k2eAgInSc2d1	mozkový
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
forma	forma	k1gFnSc1	forma
listeriózy	listerióza	k1gFnSc2	listerióza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
u	u	k7c2	u
zdravých	zdravý	k2eAgFnPc2d1	zdravá
<g/>
,	,	kIx,	,
dospělých	dospělý	k2eAgFnPc2d1	dospělá
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
horečka	horečka	k1gFnSc1	horečka
provázená	provázený	k2eAgFnSc1d1	provázená
nevolností	nevolnost	k1gFnSc7	nevolnost
<g/>
,	,	kIx,	,
zvracením	zvracení	k1gNnSc7	zvracení
a	a	k8xC	a
bolestí	bolest	k1gFnSc7	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
poruchy	porucha	k1gFnPc1	porucha
funkce	funkce	k1gFnSc2	funkce
hlavových	hlavový	k2eAgInPc2d1	hlavový
nervů	nerv	k1gInPc2	nerv
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnSc2	porucha
funkce	funkce	k1gFnSc2	funkce
mozečku	mozeček	k1gInSc2	mozeček
nebo	nebo	k8xC	nebo
hemiparézy	hemiparéza	k1gFnSc2	hemiparéza
a	a	k8xC	a
hypestézie	hypestézie	k1gFnSc2	hypestézie
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
poruchy	porucha	k1gFnPc1	porucha
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
smrtnost	smrtnost	k1gFnSc1	smrtnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
51	[number]	k4	51
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
neléčených	léčený	k2eNgMnPc2d1	neléčený
pacientů	pacient	k1gMnPc2	pacient
je	být	k5eAaImIp3nS	být
100	[number]	k4	100
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
ampicilínem	ampicilín	k1gInSc7	ampicilín
nebo	nebo	k8xC	nebo
penicilínem	penicilín	k1gInSc7	penicilín
méně	málo	k6eAd2	málo
než	než	k8xS	než
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
61	[number]	k4	61
<g/>
%	%	kIx~	%
vyléčených	vyléčený	k2eAgMnPc2d1	vyléčený
pacientů	pacient	k1gMnPc2	pacient
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
neurologické	urologický	k2eNgFnPc1d1	urologický
potíže	potíž	k1gFnPc1	potíž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
%	%	kIx~	%
případů	případ	k1gInPc2	případ
encefalické	encefalický	k2eAgFnSc2d1	encefalický
formy	forma	k1gFnSc2	forma
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
abscesy	absces	k1gInPc1	absces
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
.	.	kIx.	.
nákaza	nákaza	k1gFnSc1	nákaza
plodu	plod	k1gInSc2	plod
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
Těhotné	těhotný	k2eAgFnSc2d1	těhotná
ženy	žena	k1gFnSc2	žena
jsou	být	k5eAaImIp3nP	být
zvláště	zvláště	k6eAd1	zvláště
vnímavé	vnímavý	k2eAgInPc4d1	vnímavý
k	k	k7c3	k
listerióze	listerióza	k1gFnSc3	listerióza
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
bakteriémie	bakteriémie	k1gFnSc2	bakteriémie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
encefalická	encefalický	k2eAgFnSc1d1	encefalický
forma	forma	k1gFnSc1	forma
je	být	k5eAaImIp3nS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
listérie	listérie	k1gFnPc1	listérie
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
v	v	k7c6	v
pohlavních	pohlavní	k2eAgInPc6d1	pohlavní
orgánech	orgán	k1gInPc6	orgán
<g/>
,	,	kIx,	,
napadají	napadat	k5eAaImIp3nP	napadat
dělohu	děloha	k1gFnSc4	děloha
a	a	k8xC	a
fétus	fétus	k1gInSc4	fétus
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
samotných	samotný	k2eAgMnPc2d1	samotný
probíhá	probíhat	k5eAaImIp3nS	probíhat
infekce	infekce	k1gFnPc4	infekce
bez	bez	k7c2	bez
vnějších	vnější	k2eAgInPc2d1	vnější
příznaků	příznak	k1gInPc2	příznak
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
symptomy	symptom	k1gInPc1	symptom
připomínající	připomínající	k2eAgFnSc4d1	připomínající
chřipku	chřipka	k1gFnSc4	chřipka
<g/>
,	,	kIx,	,
infekce	infekce	k1gFnPc4	infekce
ale	ale	k8xC	ale
způsobí	způsobit	k5eAaPmIp3nS	způsobit
zánět	zánět	k1gInSc4	zánět
placenty	placenta	k1gFnSc2	placenta
a	a	k8xC	a
plodových	plodový	k2eAgInPc2d1	plodový
obalů	obal	k1gInPc2	obal
a	a	k8xC	a
infekci	infekce	k1gFnSc3	infekce
plodu	plod	k1gInSc2	plod
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
potratu	potrat	k1gInSc3	potrat
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
trimestru	trimestr	k1gInSc6	trimestr
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
předčasný	předčasný	k2eAgInSc4d1	předčasný
porod	porod	k1gInSc4	porod
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
22	[number]	k4	22
<g/>
%	%	kIx~	%
případů	případ	k1gInPc2	případ
končí	končit	k5eAaImIp3nP	končit
intrauterinní	intrauterinný	k2eAgMnPc1d1	intrauterinný
smrtí	smrt	k1gFnSc7	smrt
nebo	nebo	k8xC	nebo
smrtí	smrt	k1gFnSc7	smrt
novorozence	novorozenec	k1gMnSc2	novorozenec
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
celkové	celkový	k2eAgFnSc2d1	celková
sepse	sepse	k1gFnSc2	sepse
<g/>
,	,	kIx,	,
u	u	k7c2	u
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
přežívajících	přežívající	k2eAgMnPc2d1	přežívající
novorozenců	novorozenec	k1gMnPc2	novorozenec
se	se	k3xPyFc4	se
listerióza	listerióza	k1gFnSc1	listerióza
objeví	objevit	k5eAaPmIp3nS	objevit
až	až	k9	až
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
jako	jako	k8xS	jako
celková	celkový	k2eAgFnSc1d1	celková
sepse	sepse	k1gFnSc1	sepse
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
hnisavá	hnisavý	k2eAgFnSc1d1	hnisavá
meningitida	meningitida	k1gFnSc1	meningitida
<g/>
.	.	kIx.	.
infekce	infekce	k1gFnSc1	infekce
novorozenců	novorozenec	k1gMnPc2	novorozenec
Novorozenci	novorozenec	k1gMnPc7	novorozenec
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
vnímaví	vnímavý	k2eAgMnPc1d1	vnímavý
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
už	už	k6eAd1	už
během	během	k7c2	během
porodu	porod	k1gInSc2	porod
stykem	styk	k1gInSc7	styk
s	s	k7c7	s
infikovanými	infikovaný	k2eAgInPc7d1	infikovaný
vaginálními	vaginální	k2eAgInPc7d1	vaginální
sekrety	sekret	k1gInPc7	sekret
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
matka	matka	k1gFnSc1	matka
listeriózu	listerióza	k1gFnSc4	listerióza
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
popsány	popsat	k5eAaPmNgInP	popsat
i	i	k8xC	i
případy	případ	k1gInPc1	případ
přenosu	přenos	k1gInSc2	přenos
infikovaným	infikovaný	k2eAgInSc7d1	infikovaný
materiálem	materiál	k1gInSc7	materiál
na	na	k7c6	na
novorozeneckém	novorozenecký	k2eAgNnSc6d1	novorozenecké
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
onemocnění	onemocnění	k1gNnSc2	onemocnění
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
první	první	k4xOgInPc4	první
příznaky	příznak	k1gInPc4	příznak
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
novorozenců	novorozenec	k1gMnPc2	novorozenec
mladších	mladý	k2eAgInPc2d2	mladší
5	[number]	k4	5
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
sepsí	sepse	k1gFnSc7	sepse
<g/>
,	,	kIx,	,
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
dýcháním	dýchání	k1gNnSc7	dýchání
a	a	k8xC	a
neurologickými	neurologický	k2eAgFnPc7d1	neurologická
poruchami	porucha	k1gFnPc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
granulomů	granulom	k1gInPc2	granulom
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
orgánech	orgán	k1gInPc6	orgán
a	a	k8xC	a
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Mortalita	mortalita	k1gFnSc1	mortalita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
novorozenců	novorozenec	k1gMnPc2	novorozenec
je	být	k5eAaImIp3nS	být
typickým	typický	k2eAgInSc7d1	typický
nálezem	nález	k1gInSc7	nález
meningitida	meningitida	k1gFnSc1	meningitida
<g/>
.	.	kIx.	.
endokarditida	endokarditida	k1gFnSc1	endokarditida
Endokarditida	endokarditida	k1gFnSc1	endokarditida
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
zánět	zánět	k1gInSc1	zánět
osrdečníku	osrdečník	k1gInSc2	osrdečník
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
asi	asi	k9	asi
7,5	[number]	k4	7,5
<g/>
%	%	kIx~	%
případů	případ	k1gInPc2	případ
listeriózy	listerióza	k1gFnSc2	listerióza
dospělých	dospělý	k2eAgFnPc2d1	dospělá
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
této	tento	k3xDgFnSc2	tento
formy	forma	k1gFnSc2	forma
je	být	k5eAaImIp3nS	být
vysoké	vysoký	k2eAgNnSc1d1	vysoké
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
sepse	sepse	k1gFnSc2	sepse
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
smrtnost	smrtnost	k1gFnSc4	smrtnost
(	(	kIx(	(
<g/>
48	[number]	k4	48
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
peritonitida	peritonitida	k1gFnSc1	peritonitida
Zánět	zánět	k1gInSc1	zánět
pobřišnice	pobřišnice	k1gFnSc1	pobřišnice
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
cirhózou	cirhóza	k1gFnSc7	cirhóza
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
kožní	kožní	k2eAgInPc4d1	kožní
příznaky	příznak	k1gInPc4	příznak
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
Kožní	kožní	k2eAgInPc4d1	kožní
příznaky	příznak	k1gInPc4	příznak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vyrážka	vyrážka	k1gFnSc1	vyrážka
nebo	nebo	k8xC	nebo
puchýře	puchýř	k1gInPc1	puchýř
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
provázené	provázený	k2eAgNnSc1d1	provázené
horečnou	horečný	k2eAgFnSc7d1	horečná
nebo	nebo	k8xC	nebo
zimnicí	zimnice	k1gFnSc7	zimnice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
infekčním	infekční	k2eAgInSc7d1	infekční
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
nemocní	nemocný	k2eAgMnPc1d1	nemocný
novorozenci	novorozenec	k1gMnPc1	novorozenec
nebo	nebo	k8xC	nebo
mrtvé	mrtvý	k2eAgInPc1d1	mrtvý
plody	plod	k1gInPc1	plod
<g/>
.	.	kIx.	.
</s>
<s>
Dostane	dostat	k5eAaPmIp3nS	dostat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
bakterií	bakterie	k1gFnPc2	bakterie
do	do	k7c2	do
oka	oko	k1gNnSc2	oko
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
zánět	zánět	k1gInSc4	zánět
spojivek	spojivka	k1gFnPc2	spojivka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
popsané	popsaný	k2eAgInPc1d1	popsaný
projevy	projev	k1gInPc1	projev
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
septickou	septický	k2eAgFnSc4d1	septická
artritidu	artritida	k1gFnSc4	artritida
<g/>
,	,	kIx,	,
osteomyelitidu	osteomyelitida	k1gFnSc4	osteomyelitida
nebo	nebo	k8xC	nebo
zápaly	zápal	k1gInPc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
listerióze	listerióza	k1gFnSc3	listerióza
je	být	k5eAaImIp3nS	být
vnímavých	vnímavý	k2eAgInPc2d1	vnímavý
i	i	k8xC	i
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
domácích	domácí	k2eAgFnPc2d1	domácí
i	i	k8xC	i
divoce	divoce	k6eAd1	divoce
žijící	žijící	k2eAgNnPc1d1	žijící
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
z	z	k7c2	z
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
jsou	být	k5eAaImIp3nP	být
nejcitlivější	citlivý	k2eAgMnPc1d3	nejcitlivější
přežvýkavci	přežvýkavec	k1gMnPc1	přežvýkavec
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
skot	skot	k1gInSc1	skot
a	a	k8xC	a
ovce	ovce	k1gFnSc1	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
projevem	projev	k1gInSc7	projev
je	být	k5eAaImIp3nS	být
meningoencefalitida	meningoencefalitida	k1gFnSc1	meningoencefalitida
často	často	k6eAd1	často
končící	končící	k2eAgFnPc1d1	končící
úhynem	úhyn	k1gInSc7	úhyn
nebo	nebo	k8xC	nebo
aborty	abort	k1gInPc1	abort
a	a	k8xC	a
záněty	zánět	k1gInPc1	zánět
dělohy	děloha	k1gFnSc2	děloha
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
nakazí	nakazit	k5eAaPmIp3nP	nakazit
kontaminovanou	kontaminovaný	k2eAgFnSc7d1	kontaminovaná
siláží	siláž	k1gFnSc7	siláž
<g/>
.	.	kIx.	.
</s>
<s>
Nakažená	nakažený	k2eAgNnPc4d1	nakažené
zvířata	zvíře	k1gNnPc4	zvíře
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
původce	původce	k1gMnSc1	původce
výkaly	výkal	k1gInPc4	výkal
i	i	k8xC	i
mlékem	mléko	k1gNnSc7	mléko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
listerióza	listerióza	k1gFnSc1	listerióza
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
sporadicky	sporadicky	k6eAd1	sporadicky
jako	jako	k9	jako
enzootie	enzootie	k1gFnSc1	enzootie
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
listeriózy	listerióza	k1gFnSc2	listerióza
je	být	k5eAaImIp3nS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
kultivačním	kultivační	k2eAgInSc7d1	kultivační
nálezem	nález	k1gInSc7	nález
L.	L.	kA	L.
<g/>
monocytogenes	monocytogenes	k1gInSc1	monocytogenes
ve	v	k7c6	v
stolici	stolice	k1gFnSc6	stolice
<g/>
,	,	kIx,	,
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
<g/>
,	,	kIx,	,
v	v	k7c6	v
placentě	placenta	k1gFnSc6	placenta
a	a	k8xC	a
mrtvém	mrtvý	k2eAgInSc6d1	mrtvý
plodu	plod	k1gInSc6	plod
při	při	k7c6	při
potratu	potrat	k1gInSc6	potrat
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vzorcích	vzorec	k1gInPc6	vzorec
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
ledvin	ledvina	k1gFnPc2	ledvina
nebo	nebo	k8xC	nebo
sleziny	slezina	k1gFnSc2	slezina
při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
pacientů	pacient	k1gMnPc2	pacient
se	s	k7c7	s
septickou	septický	k2eAgFnSc7d1	septická
formou	forma	k1gFnSc7	forma
<g/>
,	,	kIx,	,
event.	event.	k?	event.
ve	v	k7c6	v
vzorcích	vzorec	k1gInPc6	vzorec
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
při	při	k7c6	při
encefalické	encefalický	k2eAgFnSc6d1	encefalický
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
L.	L.	kA	L.
monocytogenes	monocytogenes	k1gInSc1	monocytogenes
je	být	k5eAaImIp3nS	být
citlivá	citlivý	k2eAgFnSc1d1	citlivá
k	k	k7c3	k
penicilinu	penicilin	k1gInSc3	penicilin
<g/>
,	,	kIx,	,
ampicilinu	ampicilin	k1gInSc3	ampicilin
<g/>
,	,	kIx,	,
vancomycinu	vancomycin	k1gInSc3	vancomycin
<g/>
,	,	kIx,	,
ciprofloxacinu	ciprofloxacin	k1gInSc3	ciprofloxacin
<g/>
,	,	kIx,	,
linezolidům	linezolid	k1gMnPc3	linezolid
<g/>
,	,	kIx,	,
azithromycinu	azithromycin	k1gInSc3	azithromycin
a	a	k8xC	a
kotrimoxazolu	kotrimoxazol	k1gInSc3	kotrimoxazol
<g/>
.	.	kIx.	.
</s>
<s>
Betalaktamová	Betalaktamový	k2eAgNnPc1d1	Betalaktamový
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
aminoglykosidy	aminoglykosid	k1gMnPc7	aminoglykosid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Gentamicin	Gentamicin	k2eAgInSc1d1	Gentamicin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
trvá	trvat	k5eAaImIp3nS	trvat
2-6	[number]	k4	2-6
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
typu	typ	k1gInSc6	typ
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
listerióze	listerióza	k1gFnSc3	listerióza
není	být	k5eNaImIp3nS	být
očkování	očkování	k1gNnSc1	očkování
<g/>
,	,	kIx,	,
jedinou	jediný	k2eAgFnSc7d1	jediná
prevencí	prevence	k1gFnSc7	prevence
je	být	k5eAaImIp3nS	být
důkladná	důkladný	k2eAgFnSc1d1	důkladná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
úprava	úprava	k1gFnSc1	úprava
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
dodržování	dodržování	k1gNnSc4	dodržování
hygienických	hygienický	k2eAgNnPc2d1	hygienické
opatření	opatření	k1gNnPc2	opatření
<g/>
.	.	kIx.	.
</s>
