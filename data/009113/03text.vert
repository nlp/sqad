<p>
<s>
Kvazar	kvazar	k1gInSc1	kvazar
<g/>
,	,	kIx,	,
kvasar	kvasar	k1gInSc1	kvasar
(	(	kIx(	(
<g/>
transkripce	transkripce	k1gFnSc1	transkripce
anglického	anglický	k2eAgInSc2d1	anglický
akronymu	akronym	k1gInSc2	akronym
QUASi-stellAR	QUASitellAR	k1gMnSc1	QUASi-stellAR
radio	radio	k1gNnSc1	radio
sources	sources	k1gMnSc1	sources
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vesmírné	vesmírný	k2eAgNnSc4d1	vesmírné
těleso	těleso	k1gNnSc4	těleso
s	s	k7c7	s
výrazným	výrazný	k2eAgInSc7d1	výrazný
rudým	rudý	k2eAgInSc7d1	rudý
posuvem	posuv	k1gInSc7	posuv
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
optickém	optický	k2eAgInSc6d1	optický
dalekohledu	dalekohled	k1gInSc6	dalekohled
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
bodový	bodový	k2eAgInSc4d1	bodový
zdroj	zdroj	k1gInSc4	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Sloanova	Sloanův	k2eAgFnSc1d1	Sloanova
digitální	digitální	k2eAgFnSc1d1	digitální
prohlídka	prohlídka	k1gFnSc1	prohlídka
oblohy	obloha	k1gFnSc2	obloha
(	(	kIx(	(
<g/>
SDSS	SDSS	kA	SDSS
<g/>
)	)	kIx)	)
jich	on	k3xPp3gMnPc2	on
objevila	objevit	k5eAaPmAgFnS	objevit
přes	přes	k7c4	přes
200	[number]	k4	200
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
kvasary	kvasar	k1gInPc1	kvasar
mění	měnit	k5eAaImIp3nP	měnit
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
svoji	svůj	k3xOyFgFnSc4	svůj
svítivost	svítivost	k1gFnSc4	svítivost
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
malou	malý	k2eAgFnSc4d1	malá
velikost	velikost	k1gFnSc4	velikost
(	(	kIx(	(
<g/>
těleso	těleso	k1gNnSc1	těleso
nemůže	moct	k5eNaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
stavy	stav	k1gInPc4	stav
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
než	než	k8xS	než
trvá	trvat	k5eAaImIp3nS	trvat
přenos	přenos	k1gInSc1	přenos
informace	informace	k1gFnSc2	informace
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
jeho	jeho	k3xOp3gInSc2	jeho
konce	konec	k1gInSc2	konec
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
informace	informace	k1gFnPc1	informace
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
nejvýše	vysoce	k6eAd3	vysoce
rychlostí	rychlost	k1gFnSc7	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mechanismus	mechanismus	k1gInSc1	mechanismus
fungování	fungování	k1gNnSc2	fungování
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
kvasarů	kvasar	k1gInPc2	kvasar
se	se	k3xPyFc4	se
rudý	rudý	k2eAgInSc1d1	rudý
posuv	posuv	k1gInSc1	posuv
z	z	k7c2	z
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
0,1	[number]	k4	0,1
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
důsledkem	důsledek	k1gInSc7	důsledek
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdálené	vzdálený	k2eAgInPc4d1	vzdálený
kvasary	kvasar	k1gInPc4	kvasar
musí	muset	k5eAaImIp3nS	muset
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
více	hodně	k6eAd2	hodně
energie	energie	k1gFnPc4	energie
než	než	k8xS	než
desítky	desítka	k1gFnPc4	desítka
běžných	běžný	k2eAgFnPc2d1	běžná
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgInSc1d3	Nejbližší
kvasar	kvasar	k1gInSc1	kvasar
je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
240	[number]	k4	240
Mpc	Mpc	k1gMnSc1	Mpc
<g/>
,	,	kIx,	,
nejvzdálenější	vzdálený	k2eAgMnSc1d3	nejvzdálenější
známý	známý	k1gMnSc1	známý
5	[number]	k4	5
500	[number]	k4	500
Mpc	Mpc	k1gFnPc2	Mpc
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
leží	ležet	k5eAaImIp3nS	ležet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
Mpc	Mpc	k1gMnPc2	Mpc
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Protože	protože	k8xS	protože
urazit	urazit	k5eAaPmF	urazit
tyto	tento	k3xDgFnPc4	tento
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
trvá	trvat	k5eAaImIp3nS	trvat
světlu	světlo	k1gNnSc3	světlo
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
většinou	většinou	k6eAd1	většinou
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
již	již	k6eAd1	již
neexistující	existující	k2eNgInPc1d1	neexistující
objekty	objekt	k1gInPc1	objekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
mechanismus	mechanismus	k1gInSc1	mechanismus
fungování	fungování	k1gNnSc2	fungování
kvasaru	kvasar	k1gInSc2	kvasar
dosud	dosud	k6eAd1	dosud
není	být	k5eNaImIp3nS	být
plně	plně	k6eAd1	plně
objasněn	objasnit	k5eAaPmNgInS	objasnit
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
teorií	teorie	k1gFnPc2	teorie
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
aktivní	aktivní	k2eAgNnPc4d1	aktivní
jádra	jádro	k1gNnPc4	jádro
velmi	velmi	k6eAd1	velmi
starých	starý	k2eAgFnPc2d1	stará
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
středu	střed	k1gInSc6	střed
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obří	obří	k2eAgFnSc1d1	obří
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
kvasary	kvasar	k1gInPc1	kvasar
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
aktivní	aktivní	k2eAgFnPc4d1	aktivní
galaxie	galaxie	k1gFnPc4	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgFnSc1d1	okolní
hmota	hmota	k1gFnSc1	hmota
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
padá	padat	k5eAaImIp3nS	padat
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
akreční	akreční	k2eAgInSc4d1	akreční
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
třením	tření	k1gNnSc7	tření
intenzivně	intenzivně	k6eAd1	intenzivně
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
<g/>
.	.	kIx.	.
</s>
<s>
Padající	padající	k2eAgFnSc1d1	padající
žhavá	žhavý	k2eAgFnSc1d1	žhavá
hmota	hmota	k1gFnSc1	hmota
se	se	k3xPyFc4	se
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
energie	energie	k1gFnSc1	energie
elektromagnetickým	elektromagnetický	k2eAgNnSc7d1	elektromagnetické
zářením	záření	k1gNnSc7	záření
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
hmota	hmota	k1gFnSc1	hmota
urychlována	urychlován	k2eAgFnSc1d1	urychlována
na	na	k7c6	na
obrovské	obrovský	k2eAgFnSc6d1	obrovská
rychlosti	rychlost	k1gFnSc6	rychlost
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
relativistický	relativistický	k2eAgInSc1d1	relativistický
polární	polární	k2eAgInSc1d1	polární
jet	jet	k5eAaImF	jet
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc4d1	znám
žádný	žádný	k3yNgInSc4	žádný
jiný	jiný	k2eAgInSc4d1	jiný
mechanismus	mechanismus	k1gInSc4	mechanismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc1d1	velký
zářivý	zářivý	k2eAgInSc1d1	zářivý
výkon	výkon	k1gInSc1	výkon
s	s	k7c7	s
tak	tak	k6eAd1	tak
rychlými	rychlý	k2eAgFnPc7d1	rychlá
změnami	změna	k1gFnPc7	změna
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
mají	mít	k5eAaImIp3nP	mít
kvasary	kvasar	k1gInPc4	kvasar
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Některé	některý	k3yIgInPc1	některý
kvasary	kvasar	k1gInPc1	kvasar
ale	ale	k9	ale
přestávají	přestávat	k5eAaImIp3nP	přestávat
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
let	léto	k1gNnPc2	léto
místo	místo	k7c2	místo
očekávaných	očekávaný	k2eAgInPc2d1	očekávaný
mnoha	mnoho	k4c2	mnoho
tisíců	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jen	jen	k9	jen
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
dosavadní	dosavadní	k2eAgNnSc1d1	dosavadní
nepochopení	nepochopení	k1gNnSc1	nepochopení
mechanismu	mechanismus	k1gInSc2	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
také	také	k9	také
i	i	k9	i
náhlost	náhlost	k1gFnSc1	náhlost
jejich	jejich	k3xOp3gNnSc2	jejich
"	"	kIx"	"
<g/>
zapnutí	zapnutí	k1gNnSc2	zapnutí
<g/>
"	"	kIx"	"
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
dosavadním	dosavadní	k2eAgFnPc3d1	dosavadní
představám	představa	k1gFnPc3	představa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objev	objev	k1gInSc1	objev
prvního	první	k4xOgInSc2	první
kvasaru	kvasar	k1gInSc2	kvasar
==	==	k?	==
</s>
</p>
<p>
<s>
Kvasary	kvasar	k1gInPc1	kvasar
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
koncem	koncem	k7c2	koncem
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
rádiových	rádiový	k2eAgFnPc6d1	rádiová
mapách	mapa	k1gFnPc6	mapa
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dobrém	dobrý	k2eAgNnSc6d1	dobré
rozlišení	rozlišení	k1gNnSc6	rozlišení
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
téměř	téměř	k6eAd1	téměř
bodové	bodový	k2eAgInPc1d1	bodový
zdroje	zdroj	k1gInPc1	zdroj
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
rádiové	rádiový	k2eAgFnPc4d1	rádiová
hvězdy	hvězda	k1gFnPc4	hvězda
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
astronomové	astronom	k1gMnPc1	astronom
ale	ale	k8xC	ale
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c4	v
jejich	jejich	k3xOp3gInSc4	jejich
extragalaktický	extragalaktický	k2eAgInSc4d1	extragalaktický
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc4	jejich
optické	optický	k2eAgInPc4d1	optický
protějšky	protějšek	k1gInPc4	protějšek
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
slabé	slabý	k2eAgMnPc4d1	slabý
a	a	k8xC	a
spojit	spojit	k5eAaPmF	spojit
je	on	k3xPp3gInPc4	on
s	s	k7c7	s
jejich	jejich	k3xOp3gInPc7	jejich
radiovými	radiový	k2eAgInPc7d1	radiový
protějšky	protějšek	k1gInPc7	protějšek
nebylo	být	k5eNaImAgNnS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zdroj	zdroj	k1gInSc1	zdroj
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgMnSc2	který
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
3C	[number]	k4	3C
48	[number]	k4	48
z	z	k7c2	z
Třetího	třetí	k4xOgInSc2	třetí
cambridgeského	cambridgeský	k2eAgInSc2d1	cambridgeský
katalogu	katalog	k1gInSc2	katalog
radiových	radiový	k2eAgInPc2d1	radiový
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Pořízení	pořízení	k1gNnSc1	pořízení
spektra	spektrum	k1gNnSc2	spektrum
objektu	objekt	k1gInSc2	objekt
ale	ale	k9	ale
odkrylo	odkrýt	k5eAaPmAgNnS	odkrýt
další	další	k2eAgFnPc4d1	další
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Spojité	spojitý	k2eAgNnSc1d1	spojité
spektrum	spektrum	k1gNnSc1	spektrum
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
široké	široký	k2eAgFnPc4d1	široká
emisní	emisní	k2eAgFnPc4d1	emisní
čáry	čára	k1gFnPc4	čára
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vědci	vědec	k1gMnPc1	vědec
nebyli	být	k5eNaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
s	s	k7c7	s
čarami	čára	k1gFnPc7	čára
známých	známý	k2eAgMnPc2d1	známý
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
případ	případ	k1gInSc1	případ
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
jako	jako	k8xC	jako
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
hvězda	hvězda	k1gFnSc1	hvězda
emitující	emitující	k2eAgFnSc1d1	emitující
rádiové	rádiový	k2eAgNnSc4d1	rádiové
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
astronom	astronom	k1gMnSc1	astronom
Maarten	Maarten	k2eAgMnSc1d1	Maarten
Schmidt	Schmidt	k1gMnSc1	Schmidt
odhalil	odhalit	k5eAaPmAgMnS	odhalit
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
kvasaru	kvasar	k1gInSc2	kvasar
3C	[number]	k4	3C
273	[number]	k4	273
Balmerovu	Balmerův	k2eAgFnSc4d1	Balmerova
sérii	série	k1gFnSc4	série
čar	čára	k1gFnPc2	čára
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nikdo	nikdo	k3yNnSc1	nikdo
nehledal	hledat	k5eNaImAgMnS	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
byl	být	k5eAaImAgInS	být
rudý	rudý	k2eAgInSc1d1	rudý
posuv	posuv	k1gInSc1	posuv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rudý	rudý	k2eAgInSc1d1	rudý
posuv	posuv	k1gInSc1	posuv
kvasaru	kvasar	k1gInSc2	kvasar
3C	[number]	k4	3C
273	[number]	k4	273
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
z	z	k7c2	z
=	=	kIx~	=
0,1583	[number]	k4	0,1583
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
posuvu	posuv	k1gInSc2	posuv
lze	lze	k6eAd1	lze
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
rozpínáním	rozpínání	k1gNnSc7	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
zjistit	zjistit	k5eAaPmF	zjistit
podle	podle	k7c2	podle
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
zákona	zákon	k1gInSc2	zákon
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
kvasaru	kvasar	k1gInSc2	kvasar
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
pak	pak	k6eAd1	pak
i	i	k8xC	i
absolutní	absolutní	k2eAgFnSc4d1	absolutní
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
zářivý	zářivý	k2eAgInSc4d1	zářivý
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
u	u	k7c2	u
3C	[number]	k4	3C
273	[number]	k4	273
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
desítkám	desítka	k1gFnPc3	desítka
biliónů	bilión	k4xCgInPc2	bilión
Sluncí	slunce	k1gNnPc2	slunce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
až	až	k9	až
padesátkrát	padesátkrát	k6eAd1	padesátkrát
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
výkon	výkon	k1gInSc4	výkon
nejjasnějších	jasný	k2eAgFnPc2d3	nejjasnější
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenosti	vzdálenost	k1gFnPc1	vzdálenost
kvasarů	kvasar	k1gInPc2	kvasar
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
řádově	řádově	k6eAd1	řádově
několik	několik	k4yIc4	několik
miliard	miliarda	k4xCgFnPc2	miliarda
světelných	světelný	k2eAgFnPc2d1	světelná
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
objekty	objekt	k1gInPc1	objekt
z	z	k7c2	z
raného	raný	k2eAgNnSc2d1	rané
období	období	k1gNnSc2	období
vývoje	vývoj	k1gInSc2	vývoj
galaxií	galaxie	k1gFnPc2	galaxie
a	a	k8xC	a
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc1	rozdělení
kvasarů	kvasar	k1gInPc2	kvasar
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
blízkém	blízký	k2eAgInSc6d1	blízký
vesmíru	vesmír	k1gInSc6	vesmír
se	se	k3xPyFc4	se
už	už	k9	už
kvasary	kvasar	k1gInPc1	kvasar
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgFnPc1d1	Černé
díry	díra	k1gFnPc1	díra
ve	v	k7c6	v
středech	střed	k1gInPc6	střed
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
i	i	k9	i
naše	náš	k3xOp1gFnPc1	náš
Galaxie	galaxie	k1gFnPc1	galaxie
nebo	nebo	k8xC	nebo
galaxie	galaxie	k1gFnPc1	galaxie
v	v	k7c6	v
Andromedě	Andromeda	k1gFnSc6	Andromeda
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
materiálu	materiál	k1gInSc2	materiál
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
spotřebovaly	spotřebovat	k5eAaPmAgFnP	spotřebovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
všechny	všechen	k3xTgInPc1	všechen
kvasary	kvasar	k1gInPc1	kvasar
mají	mít	k5eAaImIp3nP	mít
silnou	silný	k2eAgFnSc4d1	silná
rádiovou	rádiový	k2eAgFnSc4d1	rádiová
emisi	emise	k1gFnSc4	emise
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
–	–	k?	–
naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
kvasarů	kvasar	k1gInPc2	kvasar
<g/>
,	,	kIx,	,
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
<g/>
,	,	kIx,	,
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
v	v	k7c6	v
rádiové	rádiový	k2eAgFnSc6d1	rádiová
oblasti	oblast	k1gFnSc6	oblast
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
optickým	optický	k2eAgInSc7d1	optický
oborem	obor	k1gInSc7	obor
slabě	slabě	k6eAd1	slabě
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
tzv.	tzv.	kA	tzv.
radio-loud	radiooud	k6eAd1	radio-loud
(	(	kIx(	(
<g/>
hlasité	hlasitý	k2eAgNnSc1d1	hlasité
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
vyzařující	vyzařující	k2eAgFnSc1d1	vyzařující
v	v	k7c6	v
rádiové	rádiový	k2eAgFnSc6d1	rádiová
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
)	)	kIx)	)
a	a	k8xC	a
radio-quiet	radiouiet	k5eAaPmF	radio-quiet
(	(	kIx(	(
<g/>
tiché	tichý	k2eAgInPc4d1	tichý
<g/>
)	)	kIx)	)
kvasary	kvasar	k1gInPc4	kvasar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mohutnou	mohutný	k2eAgFnSc4d1	mohutná
rádiovou	rádiový	k2eAgFnSc4d1	rádiová
emisi	emise	k1gFnSc4	emise
<g/>
,	,	kIx,	,
odlišující	odlišující	k2eAgFnSc4d1	odlišující
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
radio-loud	radiooud	k6eAd1	radio-loud
a	a	k8xC	a
radio-quiet	radiouiet	k5eAaImF	radio-quiet
kvasary	kvasar	k1gInPc4	kvasar
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
relativistické	relativistický	k2eAgInPc1d1	relativistický
elektrony	elektron	k1gInPc1	elektron
ve	v	k7c6	v
výtryscích	výtrysk	k1gInPc6	výtrysk
radio-loud	radiooud	k6eAd1	radio-loud
kvasaru	kvasar	k1gInSc2	kvasar
<g/>
.	.	kIx.	.
</s>
<s>
Kvasar	kvasar	k1gInSc1	kvasar
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
velmi	velmi	k6eAd1	velmi
hmotné	hmotný	k2eAgFnSc2d1	hmotná
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
akrečního	akreční	k2eAgInSc2d1	akreční
disku	disk	k1gInSc2	disk
a	a	k8xC	a
jetu	jet	k2eAgFnSc4d1	jeta
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
výtrysku	výtrysk	k1gInSc2	výtrysk
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
až	až	k9	až
několik	několik	k4yIc4	několik
megaparseků	megaparsek	k1gInPc2	megaparsek
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Šířka	šířka	k1gFnSc1	šířka
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
těchto	tento	k3xDgInPc2	tento
objektů	objekt	k1gInPc2	objekt
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
rychlostem	rychlost	k1gFnPc3	rychlost
emitujícího	emitující	k2eAgInSc2d1	emitující
plynu	plyn	k1gInSc2	plyn
nad	nad	k7c7	nad
10	[number]	k4	10
000	[number]	k4	000
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Kvasary	kvasar	k1gInPc1	kvasar
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgNnSc4d1	stejné
spektrum	spektrum	k1gNnSc4	spektrum
jako	jako	k8xS	jako
Seyfertovy	Seyfertův	k2eAgFnSc2d1	Seyfertova
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
kvantitativní	kvantitativní	k2eAgInSc4d1	kvantitativní
<g/>
,	,	kIx,	,
určen	určen	k2eAgInSc4d1	určen
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Jasnější	jasný	k2eAgInPc1d2	jasnější
objekty	objekt	k1gInPc1	objekt
s	s	k7c7	s
absolutní	absolutní	k2eAgFnSc7d1	absolutní
magnitudou	magnitudý	k2eAgFnSc7d1	magnitudý
menší	malý	k2eAgFnSc7d2	menší
než	než	k8xS	než
-23	-23	k4	-23
jsou	být	k5eAaImIp3nP	být
kvasary	kvasar	k1gInPc1	kvasar
a	a	k8xC	a
slabší	slabý	k2eAgFnSc1d2	slabší
s	s	k7c7	s
magnitudou	magnitudý	k2eAgFnSc7d1	magnitudý
větší	veliký	k2eAgFnSc7d2	veliký
než	než	k8xS	než
-23	-23	k4	-23
mag	mag	k?	mag
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
Seyfertovy	Seyfertův	k2eAgFnPc1d1	Seyfertova
galaxie	galaxie	k1gFnPc1	galaxie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spektra	spektrum	k1gNnPc1	spektrum
kvasarů	kvasar	k1gInPc2	kvasar
mají	mít	k5eAaImIp3nP	mít
z	z	k7c2	z
optické	optický	k2eAgFnSc2d1	optická
do	do	k7c2	do
rádiové	rádiový	k2eAgFnSc2d1	rádiová
oblasti	oblast	k1gFnSc2	oblast
mocninný	mocninný	k2eAgInSc4d1	mocninný
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
mohutná	mohutný	k2eAgFnSc1d1	mohutná
netermální	termální	k2eNgFnSc1d1	netermální
emise	emise	k1gFnSc1	emise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
OVV	OVV	kA	OVV
kvasary	kvasar	k1gInPc4	kvasar
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
optickém	optický	k2eAgInSc6d1	optický
oboru	obor	k1gInSc6	obor
prudce	prudko	k6eAd1	prudko
se	se	k3xPyFc4	se
měnící	měnící	k2eAgInPc1d1	měnící
kvasary	kvasar	k1gInPc1	kvasar
(	(	kIx(	(
<g/>
Optically	Opticalla	k1gFnPc1	Opticalla
Violently	Violently	k1gFnSc2	Violently
Variable	Variable	k1gFnSc2	Variable
Quasars	Quasarsa	k1gFnPc2	Quasarsa
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
kvasary	kvasar	k1gInPc4	kvasar
se	s	k7c7	s
silným	silný	k2eAgNnSc7d1	silné
rádiovým	rádiový	k2eAgNnSc7d1	rádiové
zářením	záření	k1gNnSc7	záření
se	s	k7c7	s
spojitým	spojitý	k2eAgNnSc7d1	spojité
spektrem	spektrum	k1gNnSc7	spektrum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
mocninný	mocninný	k2eAgInSc1d1	mocninný
charakter	charakter	k1gInSc1	charakter
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
rádiové	rádiový	k2eAgFnSc3d1	rádiová
oblasti	oblast	k1gFnSc3	oblast
(	(	kIx(	(
<g/>
způsobený	způsobený	k2eAgInSc1d1	způsobený
netermální	termální	k2eNgFnSc7d1	netermální
emisí	emise	k1gFnSc7	emise
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	s	k7c7	s
silnými	silný	k2eAgFnPc7d1	silná
emisními	emisní	k2eAgFnPc7d1	emisní
čarami	čára	k1gFnPc7	čára
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
běžných	běžný	k2eAgInPc2d1	běžný
kvasarů	kvasar	k1gInPc2	kvasar
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
prudkými	prudký	k2eAgInPc7d1	prudký
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
nepravidelnými	pravidelný	k2eNgFnPc7d1	nepravidelná
změnami	změna	k1gFnPc7	změna
jasnosti	jasnost	k1gFnSc2	jasnost
v	v	k7c6	v
optickém	optický	k2eAgInSc6d1	optický
i	i	k8xC	i
rádiovém	rádiový	k2eAgInSc6d1	rádiový
oboru	obor	k1gInSc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
roky	rok	k1gInPc4	rok
bez	bez	k7c2	bez
výrazných	výrazný	k2eAgFnPc2d1	výrazná
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
náhle	náhle	k6eAd1	náhle
zjasní	zjasnit	k5eAaPmIp3nS	zjasnit
o	o	k7c4	o
několik	několik	k4yIc4	několik
magnitud	magnitudo	k1gNnPc2	magnitudo
během	během	k7c2	během
týdnů	týden	k1gInPc2	týden
i	i	k8xC	i
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
outburst	outburst	k1gInSc1	outburst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
lineárně	lineárně	k6eAd1	lineárně
polarizované	polarizovaný	k2eAgNnSc1d1	polarizované
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
zdroje	zdroj	k1gInSc2	zdroj
nachází	nacházet	k5eAaImIp3nS	nacházet
materiál	materiál	k1gInSc1	materiál
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
prach	prach	k1gInSc1	prach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
k	k	k7c3	k
polarizací	polarizace	k1gFnSc7	polarizace
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
katalogizovaných	katalogizovaný	k2eAgMnPc2d1	katalogizovaný
OVV	OVV	kA	OVV
kvasarů	kvasar	k1gInPc2	kvasar
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
deseti	deset	k4xCc2	deset
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Blazar	Blazar	k1gMnSc1	Blazar
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kvasar	kvasar	k1gInSc1	kvasar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
kvasarů	kvasar	k1gInPc2	kvasar
na	na	k7c4	na
Aldebaran	Aldebaran	k1gInSc4	Aldebaran
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Aktivní	aktivní	k2eAgFnPc1d1	aktivní
galaxie	galaxie	k1gFnPc1	galaxie
a	a	k8xC	a
kvasary	kvasar	k1gInPc1	kvasar
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
