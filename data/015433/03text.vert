<s>
1	#num#	k4
Geminorum	Geminorum	k1gNnSc1
</s>
<s>
1	#num#	k4
Geminorum	Geminorum	k1gInSc4
Astrometrická	Astrometrický	k2eAgNnPc4d1
data	datum	k1gNnPc4
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000.0	2000.0	k4
<g/>
)	)	kIx)
Souhvězdí	souhvězdí	k1gNnSc2
</s>
<s>
Blíženci	blíženec	k1gMnPc1
(	(	kIx(
<g/>
Gemini	Gemin	k1gMnPc1
<g/>
)	)	kIx)
Rektascenze	rektascenze	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
h	h	k?
4	#num#	k4
<g/>
m	m	kA
7,215	7,215	k4
44	#num#	k4
<g/>
s	s	k7c7
Deklinace	deklinace	k1gFnSc2
</s>
<s>
23	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
'	'	kIx"
<g/>
48,040	48,040	k4
1	#num#	k4
<g/>
"	"	kIx"
Paralaxa	paralaxa	k1gFnSc1
</s>
<s>
21,03	21,03	k4
<g/>
±	±	k?
<g/>
0,90	0,90	k4
mas	maso	k1gNnPc2
Vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
155,4	155,4	k4
<g/>
±	±	k?
<g/>
6,6	6,6	k4
ly	ly	k?
<g/>
(	(	kIx(
<g/>
47,6	47,6	k4
<g/>
±	±	k?
<g/>
2,0	2,0	k4
pc	pc	k?
<g/>
)	)	kIx)
Zdánlivá	zdánlivý	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
4,15	4,15	k4
Radiální	radiální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
</s>
<s>
22,39	22,39	k4
<g/>
±	±	k?
<g/>
0,28	0,28	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Vlastní	vlastní	k2eAgFnSc7d1
pohyb	pohyb	k1gInSc4
v	v	k7c6
rektascenzi	rektascenze	k1gFnSc6
</s>
<s>
-1,61	-1,61	k4
<g/>
±	±	k?
<g/>
1,49	1,49	k4
mas	masa	k1gFnPc2
za	za	k7c4
rok	rok	k1gInSc4
Vlastní	vlastní	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
v	v	k7c6
deklinaci	deklinace	k1gFnSc6
</s>
<s>
-118,33	-118,33	k4
<g/>
±	±	k?
<g/>
0,96	0,96	k4
<g/>
"	"	kIx"
mas	masa	k1gFnPc2
za	za	k7c4
rok	rok	k1gInSc4
</s>
<s>
1	#num#	k4
Gem	gem	k1gInSc4
A	a	k9
Rektascenze	rektascenze	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
h	h	k?
4	#num#	k4
<g/>
m	m	kA
7,22	7,22	k4
<g/>
s	s	k7c7
Deklinace	deklinace	k1gFnSc2
</s>
<s>
23	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
'	'	kIx"
<g/>
49,1	49,1	k4
<g/>
"	"	kIx"
Zdánlivá	zdánlivý	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
4,2	4,2	k4
Absolutní	absolutní	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
0,8	0,8	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
U-B	U-B	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
B-V	B-V	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
0,8	0,8	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
V-R	V-R	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
R-I	R-I	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s>
1	#num#	k4
Gem	gem	k1gInSc1
B	B	kA
Rektascenze	rektascenze	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
h	h	k?
4	#num#	k4
<g/>
m	m	kA
7,2	7,2	k4
<g/>
s	s	k7c7
Deklinace	deklinace	k1gFnSc2
</s>
<s>
23	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
'	'	kIx"
<g/>
52	#num#	k4
<g/>
"	"	kIx"
Zdánlivá	zdánlivý	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
5,50	5,50	k4
Absolutní	absolutní	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
2,11	2,11	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
U-B	U-B	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
B-V	B-V	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
-0,3	-0,3	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
V-R	V-R	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
R-I	R-I	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1
charakteristiky	charakteristika	k1gFnPc1
</s>
<s>
1	#num#	k4
Gem	gem	k1gInSc4
A	a	k9
Spektrální	spektrální	k2eAgInSc4d1
typ	typ	k1gInSc4
</s>
<s>
G5III	G5III	k4
</s>
<s>
1	#num#	k4
Gem	gem	k1gInSc1
B	B	kA
Typ	typ	k1gInSc4
proměnnosti	proměnnost	k1gFnSc2
</s>
<s>
</s>
<s desamb="1">
Spektrální	spektrální	k2eAgInSc1d1
typ	typ	k1gInSc1
</s>
<s>
</s>
<s>
Systém	systém	k1gInSc1
Primární	primární	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
</s>
<s>
1	#num#	k4
Gem	gem	k1gInSc4
A	a	k9
Průvodce	průvodce	k1gMnSc1
</s>
<s>
1	#num#	k4
Gem	gem	k1gInSc1
B	B	kA
</s>
<s>
Označení	označení	k1gNnSc1
Henry	henry	k1gInSc2
Draper	Draper	k1gInSc1
Catalogue	Catalogu	k1gFnSc2
</s>
<s>
HD	HD	kA
41116	#num#	k4
Bonner	Bonnero	k1gNnPc2
Durchmusterung	Durchmusterunga	k1gFnPc2
</s>
<s>
BD	BD	kA
+23	+23	k4
<g/>
°	°	k?
1170AB	1170AB	k4
Bright	Bright	k1gInSc1
Star	Star	kA
katalog	katalog	k1gInSc1
</s>
<s>
HR	hr	k6eAd1
2134	#num#	k4
SAO	SAO	kA
katalog	katalog	k1gInSc4
</s>
<s>
SAO	SAO	kA
77915	#num#	k4
Katalog	katalog	k1gInSc1
Hipparcos	Hipparcos	k1gInSc4
</s>
<s>
HIP	hip	k0
28734	#num#	k4
Tychův	Tychův	k2eAgInSc4d1
katalog	katalog	k1gInSc4
</s>
<s>
TYC	TYC	kA
1864-2011-1	1864-2011-1	k4
General	General	k1gFnSc2
Catalogue	Catalogue	k1gFnPc2
</s>
<s>
GC	GC	kA
7676	#num#	k4
Flamsteedovo	Flamsteedův	k2eAgNnSc4d1
označení	označení	k1gNnSc1
</s>
<s>
1	#num#	k4
Gem	gem	k1gInSc1
Synonyma	synonymum	k1gNnSc2
</s>
<s>
AG	AG	kA
<g/>
+	+	kIx~
<g/>
23	#num#	k4
<g/>
°	°	k?
596	#num#	k4
<g/>
,	,	kIx,
GCRV	GCRV	kA
3801	#num#	k4
<g/>
,	,	kIx,
IRAS	IRAS	kA
0	#num#	k4
<g/>
6011	#num#	k4
<g/>
+	+	kIx~
<g/>
2316	#num#	k4
<g/>
,	,	kIx,
2MASS	2MASS	k4
J	J	kA
<g/>
0	#num#	k4
<g/>
6040720	#num#	k4
<g/>
+	+	kIx~
<g/>
2315481	#num#	k4
<g/>
,	,	kIx,
PLX	PLX	kA
1398	#num#	k4
<g/>
,	,	kIx,
PPM	PPM	kA
95324	#num#	k4
<g/>
,	,	kIx,
UBV	UBV	kA
6132	#num#	k4
<g/>
,	,	kIx,
WDS	WDS	kA
J	J	kA
<g/>
0	#num#	k4
<g/>
6041	#num#	k4
<g/>
+	+	kIx~
<g/>
2316	#num#	k4
<g/>
AB	AB	kA
Databáze	databáze	k1gFnSc2
SIMBAD	SIMBAD	kA
</s>
<s>
data	datum	k1gNnPc1
(	(	kIx(
<g/>
systém	systém	k1gInSc1
<g/>
)	)	kIx)
SIMBAD	SIMBAD	kA
</s>
<s>
data	datum	k1gNnPc4
(	(	kIx(
<g/>
1	#num#	k4
Gem	gem	k1gInSc1
A	A	kA
<g/>
)	)	kIx)
SIMBAD	SIMBAD	kA
</s>
<s>
data	datum	k1gNnPc4
(	(	kIx(
<g/>
1	#num#	k4
Gem	gem	k1gInSc1
B	B	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
–	–	k?
měření	měření	k1gNnSc2
provedena	proveden	k2eAgFnSc1d1
ve	v	k7c6
viditelném	viditelný	k2eAgNnSc6d1
světle	světlo	k1gNnSc6
</s>
<s>
1	#num#	k4
Geminorum	Geminorum	k1gNnSc1
(	(	kIx(
<g/>
1	#num#	k4
Gem	gem	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hvězda	hvězda	k1gFnSc1
spektrálního	spektrální	k2eAgInSc2d1
typu	typ	k1gInSc2
G5III	G5III	k1gFnSc2
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Blíženců	blíženec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
zdánlivá	zdánlivý	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
je	být	k5eAaImIp3nS
4,15	4,15	k4
<g/>
m.	m.	k?
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
spektroskopickou	spektroskopický	k2eAgFnSc4d1
dvojhvězdu	dvojhvězda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Když	když	k8xS
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
John	John	k1gMnSc1
Flamsteed	Flamsteed	k1gMnSc1
začal	začít	k5eAaPmAgMnS
jasnější	jasný	k2eAgFnPc4d2
hvězdy	hvězda	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
souhvězdích	souhvězdí	k1gNnPc6
ze	z	k7c2
západu	západ	k1gInSc2
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
1	#num#	k4
Geminorum	Geminorum	k1gNnSc4
první	první	k4xOgFnSc7
hvězdou	hvězda	k1gFnSc7
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Blíženců	blíženec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
katalogu	katalog	k1gInSc6
Bright	Brighta	k1gFnPc2
Star	Star	kA
je	být	k5eAaImIp3nS
vedena	veden	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
HR	hr	k2eAgFnSc1d1
2134	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
HR	hr	k6eAd1
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
Harvardský	harvardský	k2eAgInSc4d1
revidovaný	revidovaný	k2eAgInSc4d1
katalog	katalog	k1gInSc4
<g/>
,	,	kIx,
předchůdce	předchůdce	k1gMnSc1
katalogu	katalog	k1gInSc2
Bright	Bright	k1gMnSc1
Star	Star	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
1	#num#	k4
Geminorum	Geminorum	k1gInSc1
je	být	k5eAaImIp3nS
těsnou	těsný	k2eAgFnSc7d1
dvojhvězdou	dvojhvězda	k1gFnSc7
při	při	k7c6
pozorovaní	pozorovaný	k2eAgMnPc1d1
planety	planeta	k1gFnSc2
Uran	Uran	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
počátečních	počáteční	k2eAgNnPc2d1
pozorování	pozorování	k1gNnPc2
spektra	spektrum	k1gNnSc2
se	se	k3xPyFc4
odhadovalo	odhadovat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
obě	dva	k4xCgFnPc1
složky	složka	k1gFnPc1
jsou	být	k5eAaImIp3nP
obři	obr	k1gMnPc1
a	a	k8xC
že	že	k8xS
sekundární	sekundární	k2eAgFnSc1d1
složka	složka	k1gFnSc1
je	být	k5eAaImIp3nS
dvojhvězdou	dvojhvězda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kolísání	kolísání	k1gNnSc4
radiální	radiální	k2eAgFnSc2d1
rychlosti	rychlost	k1gFnSc2
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
spektru	spektrum	k1gNnSc6
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
detekovat	detekovat	k5eAaImF
pouze	pouze	k6eAd1
jednu	jeden	k4xCgFnSc4
řadu	řada	k1gFnSc4
absorpčních	absorpční	k2eAgFnPc2d1
linií	linie	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
vypočítat	vypočítat	k5eAaPmF
spolehlivou	spolehlivý	k2eAgFnSc4d1
oběžnou	oběžný	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
dvojhvězdy	dvojhvězda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
Geminorum	Geminorum	k1gInSc1
je	být	k5eAaImIp3nS
trojhvězda	trojhvězda	k1gFnSc1
0,17	0,17	k4
stupně	stupeň	k1gInSc2
jižně	jižně	k6eAd1
od	od	k7c2
ekliptiky	ekliptika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Primární	primární	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
systému	systém	k1gInSc2
<g/>
,	,	kIx,
1	#num#	k4
Geminorum	Geminorum	k1gInSc1
A	A	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
červená	červený	k2eAgFnSc1d1
obří	obří	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
typu	typ	k1gInSc2
spektrálního	spektrální	k2eAgInSc2d1
typu	typ	k1gInSc2
K	K	kA
o	o	k7c6
dvojnásobku	dvojnásobek	k1gInSc6
hmoty	hmota	k1gFnSc2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Složka	složka	k1gFnSc1
A	a	k9
obíhá	obíhat	k5eAaImIp3nS
spektroskopickou	spektroskopický	k2eAgFnSc4d1
dvojhvězdu	dvojhvězda	k1gFnSc4
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
9,4	9,4	k4
astronomických	astronomický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
jednou	jednou	k6eAd1
za	za	k7c4
4877,6	4877,6	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc4
sekundární	sekundární	k2eAgFnPc4d1
složky	složka	k1gFnPc4
<g/>
,	,	kIx,
1	#num#	k4
Geminorum	Geminorum	k1gInSc4
Ba	ba	k9
a	a	k8xC
Bb	Bb	k1gFnSc1
<g/>
,	,	kIx,
dosud	dosud	k6eAd1
nebyly	být	k5eNaImAgInP
spolehlivě	spolehlivě	k6eAd1
odlišeny	odlišen	k2eAgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
pravidelné	pravidelný	k2eAgInPc1d1
periodické	periodický	k2eAgInPc1d1
dopplerovské	dopplerovský	k2eAgInPc1d1
posuny	posun	k1gInPc1
ve	v	k7c6
spektru	spektrum	k1gNnSc6
naznačují	naznačovat	k5eAaImIp3nP
oběžný	oběžný	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
binárního	binární	k2eAgInSc2d1
hvězdy	hvězda	k1gFnPc1
sestávajícího	sestávající	k2eAgMnSc4d1
z	z	k7c2
podobra	podobr	k1gInSc2
spekrálního	spekrální	k2eAgInSc2d1
typu	typ	k1gInSc2
F	F	kA
a	a	k8xC
hvězdy	hvězda	k1gFnSc2
o	o	k7c6
hmotnosti	hmotnost	k1gFnSc6
Slunce	slunce	k1gNnSc2
sluneční	sluneční	k2eAgFnSc2d1
hmoty	hmota	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
spektrální	spektrální	k2eAgFnPc4d1
ypu	ypu	k?
G	G	kA
<g/>
,	,	kIx,
vzdálené	vzdálený	k2eAgFnSc2d1
od	od	k7c2
sebe	sebe	k3xPyFc4
přibližně	přibližně	k6eAd1
0,1234	0,1234	k4
astronomických	astronomický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1893	#num#	k4
Sherburne	Sherburn	k1gInSc5
Wesley	Wesley	k1gInPc1
Burnhamová	Burnhamový	k2eAgFnSc1d1
94	#num#	k4
minut	minuta	k1gFnPc2
od	od	k7c2
hvězdy	hvězda	k1gFnSc2
při	při	k7c6
pozorování	pozorování	k1gNnSc6
s	s	k7c7
dalekohledem	dalekohled	k1gInSc7
pouhým	pouhý	k2eAgNnSc7d1
okem	oke	k1gNnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
našla	najít	k5eAaPmAgFnS
hvězdu	hvězda	k1gFnSc4
14	#num#	k4
<g/>
.	.	kIx.
vězdné	vězdný	k2eAgFnSc2d1
velikosti	velikost	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
o	o	k7c4
vzdálený	vzdálený	k2eAgInSc4d1
objekt	objekt	k1gInSc4
v	v	k7c6
pozadí	pozadí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
Geminorum	Geminorum	k1gInSc1
je	být	k5eAaImIp3nS
uvedena	uveden	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
podezřelá	podezřelý	k2eAgFnSc1d1
proměnná	proměnný	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
s	s	k7c7
amplitudou	amplituda	k1gFnSc7
0,05	0,05	k4
magnitudy	magnitud	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
1	#num#	k4
Gem	gem	k1gInSc1
<g/>
.	.	kIx.
sim-basic	sim-basic	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
KUIPER	KUIPER	kA
<g/>
,	,	kIx,
Gerard	Gerard	k1gInSc1
P.	P.	kA
A	a	k9
New	New	k1gFnSc1
Bright	Brighta	k1gFnPc2
Double	double	k2eAgInPc2d1
Star	Star	kA
<g/>
..	..	k?
ApJ	ApJ	k1gMnSc1
<g/>
.	.	kIx.
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
108	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
542	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
637	#num#	k4
<g/>
X.	X.	kA
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
145095	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ZHAO	ZHAO	kA
<g/>
,	,	kIx,
G.	G.	kA
<g/>
;	;	kIx,
QIU	QIU	kA
<g/>
,	,	kIx,
H.	H.	kA
M.	M.	kA
<g/>
;	;	kIx,
MAO	MAO	kA
<g/>
,	,	kIx,
Shude	Shud	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
High-Resolution	High-Resolution	k1gInSc4
Spectroscopic	Spectroscopice	k1gInPc2
Observations	Observationsa	k1gFnPc2
of	of	k?
Hipparcos	Hipparcos	k1gMnSc1
Red	Red	k1gFnSc2
Clump	Clump	k1gMnSc1
Giants	Giants	k1gInSc1
<g/>
:	:	kIx,
Metallicity	Metallicita	k1gFnPc1
and	and	k?
Mass	Mass	k1gInSc1
Determinations	Determinations	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Astrophysical	Astrophysical	k1gMnSc1
Journal	Journal	k1gMnSc1
Letters	Lettersa	k1gFnPc2
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
551	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
L	L	kA
<g/>
85	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1538	#num#	k4
<g/>
-	-	kIx~
<g/>
4357	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
319832	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LANE	LANE	kA
<g/>
,	,	kIx,
Benjamin	Benjamin	k1gMnSc1
F.	F.	kA
<g/>
;	;	kIx,
MUTERSPAUGH	MUTERSPAUGH	kA
<g/>
,	,	kIx,
Matthew	Matthew	k1gMnSc1
W.	W.	kA
<g/>
;	;	kIx,
GRIFFIN	GRIFFIN	kA
<g/>
,	,	kIx,
R.	R.	kA
F.	F.	kA
THE	THE	kA
ORBITS	ORBITS	kA
OF	OF	kA
THE	THE	kA
TRIPLE-STAR	TRIPLE-STAR	k1gFnPc2
SYSTEM	SYSTEM	kA
1	#num#	k4
GEMINORUM	GEMINORUM	kA
FROM	FROM	kA
PHASES	PHASES	kA
DIFFERENTIAL	DIFFERENTIAL	kA
ASTROMETRY	ASTROMETRY	kA
AND	Anda	k1gFnPc2
SPECTROSCOPY	SPECTROSCOPY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Astrophysical	Astrophysical	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
783	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
637	#num#	k4
<g/>
X.	X.	kA
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
637	#num#	k4
<g/>
X	X	kA
<g/>
/	/	kIx~
<g/>
783	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MASON	mason	k1gMnSc1
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
D.	D.	kA
<g/>
;	;	kIx,
WYCOFF	WYCOFF	kA
<g/>
,	,	kIx,
Gary	Gary	k1gInPc7
L.	L.	kA
<g/>
;	;	kIx,
HARTKOPF	HARTKOPF	kA
<g/>
,	,	kIx,
William	William	k1gInSc4
I.	I.	kA
The	The	k1gFnSc7
2001	#num#	k4
US	US	kA
Naval	navalit	k5eAaPmRp2nS
Observatory	Observator	k1gInPc4
Double	double	k2eAgFnPc1d1
Star	star	k1gFnSc2
CD-ROM	CD-ROM	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Double	double	k1gInSc1
Star	Star	kA
Catalog	Catalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Astronomical	Astronomical	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
2001	#num#	k4
December	Decembra	k1gFnPc2
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
122	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3466	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1538	#num#	k4
<g/>
-	-	kIx~
<g/>
3881	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
323920	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
VizieR	VizieR	k1gFnPc2
<g/>
.	.	kIx.
vizier	vizier	k1gMnSc1
<g/>
.	.	kIx.
<g/>
u-strasbg	u-strasbg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hvězdy	hvězda	k1gFnPc1
souhvězdí	souhvězdí	k1gNnSc2
Blíženců	blíženec	k1gMnPc2
</s>
<s>
1	#num#	k4
•	•	k?
2	#num#	k4
•	•	k?
3	#num#	k4
•	•	k?
4	#num#	k4
•	•	k?
5	#num#	k4
•	•	k?
6	#num#	k4
•	•	k?
η	η	k?
(	(	kIx(
<g/>
Propus	Propus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
8	#num#	k4
•	•	k?
9	#num#	k4
•	•	k?
10	#num#	k4
•	•	k?
11	#num#	k4
•	•	k?
12	#num#	k4
•	•	k?
μ	μ	k?
•	•	k?
14	#num#	k4
•	•	k?
15	#num#	k4
•	•	k?
16	#num#	k4
•	•	k?
ν	ν	k?
•	•	k?
19	#num#	k4
•	•	k?
20	#num#	k4
•	•	k?
22	#num#	k4
•	•	k?
23	#num#	k4
•	•	k?
γ	γ	k?
(	(	kIx(
<g/>
Alhena	Alhena	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
25	#num#	k4
•	•	k?
26	#num#	k4
•	•	k?
ε	ε	k?
(	(	kIx(
<g/>
Mebsuta	Mebsut	k1gMnSc2
<g/>
)	)	kIx)
•	•	k?
28	#num#	k4
•	•	k?
30	#num#	k4
•	•	k?
ξ	ξ	k?
(	(	kIx(
<g/>
Alzirr	Alzirr	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
32	#num#	k4
•	•	k?
33	#num#	k4
•	•	k?
θ	θ	k?
•	•	k?
35	#num#	k4
•	•	k?
36	#num#	k4
•	•	k?
37	#num#	k4
•	•	k?
38	#num#	k4
•	•	k?
39	#num#	k4
•	•	k?
40	#num#	k4
•	•	k?
41	#num#	k4
•	•	k?
ω	ω	k?
•	•	k?
ζ	ζ	k?
•	•	k?
44	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
45	#num#	k4
•	•	k?
τ	τ	k?
•	•	k?
47	#num#	k4
•	•	k?
48	#num#	k4
•	•	k?
49	#num#	k4
•	•	k?
51	#num#	k4
•	•	k?
52	#num#	k4
•	•	k?
53	#num#	k4
•	•	k?
λ	λ	k?
•	•	k?
δ	δ	k?
(	(	kIx(
<g/>
Wasat	Wasat	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
56	#num#	k4
•	•	k?
57	#num#	k4
•	•	k?
58	#num#	k4
•	•	k?
59	#num#	k4
•	•	k?
ι	ι	k?
•	•	k?
61	#num#	k4
•	•	k?
ρ	ρ	k?
•	•	k?
63	#num#	k4
•	•	k?
64	#num#	k4
•	•	k?
65	#num#	k4
•	•	k?
α	α	k?
(	(	kIx(
<g/>
Castor	Castor	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
67	#num#	k4
•	•	k?
68	#num#	k4
•	•	k?
υ	υ	k?
•	•	k?
70	#num#	k4
•	•	k?
ο	ο	k?
•	•	k?
74	#num#	k4
•	•	k?
σ	σ	k?
•	•	k?
76	#num#	k4
•	•	k?
κ	κ	k?
•	•	k?
β	β	k?
(	(	kIx(
<g/>
Pollux	Pollux	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
79	#num#	k4
•	•	k?
π	π	k?
•	•	k?
81	#num#	k4
•	•	k?
82	#num#	k4
•	•	k?
φ	φ	k?
•	•	k?
84	#num#	k4
•	•	k?
85	#num#	k4
•	•	k?
χ	χ	k?
•	•	k?
3	#num#	k4
Lyn	Lyn	k1gMnPc2
Seznam	seznam	k1gInSc4
hvězd	hvězda	k1gFnPc2
souhvězdí	souhvězdí	k1gNnSc2
Blíženců	blíženec	k1gMnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
