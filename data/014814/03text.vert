<s>
Třída	třída	k1gFnSc1
Barceló	Barceló	k1gFnSc2
</s>
<s>
Třída	třída	k1gFnSc1
Barceló	Barceló	k1gFnSc2
Cándido	Cándida	k1gFnSc5
Pérez	Pérez	k1gInSc1
(	(	kIx(
<g/>
P-	P-	k1gFnSc1
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
Obecné	obecný	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Uživatelé	uživatel	k1gMnPc1
</s>
<s>
Španělské	španělský	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
Typ	typ	k1gInSc1
</s>
<s>
hlídková	hlídkový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Lodě	loď	k1gFnSc2
</s>
<s>
6	#num#	k4
Osud	osud	k1gInSc1
</s>
<s>
vyřazeny	vyřazen	k2eAgMnPc4d1
Předchůdce	předchůdce	k1gMnPc4
</s>
<s>
třída	třída	k1gFnSc1
Lazaga	Lazag	k1gMnSc2
Nástupce	nástupce	k1gMnSc2
</s>
<s>
třída	třída	k1gFnSc1
Conajera	Conajero	k1gNnSc2
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Výtlak	výtlak	k1gInSc4
</s>
<s>
110	#num#	k4
t	t	k?
(	(	kIx(
<g/>
standardní	standardní	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
134	#num#	k4
t	t	k?
(	(	kIx(
<g/>
plný	plný	k2eAgInSc4d1
<g/>
)	)	kIx)
Délka	délka	k1gFnSc1
</s>
<s>
36,2	36,2	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
5,8	5,8	k4
m	m	kA
Ponor	ponor	k1gInSc1
</s>
<s>
2,5	2,5	k4
m	m	kA
Pohon	pohon	k1gInSc1
</s>
<s>
2	#num#	k4
diesely	diesel	k1gInPc5
Rychlost	rychlost	k1gFnSc1
</s>
<s>
36,5	36,5	k4
uzlu	uzel	k1gInSc2
Dosah	dosah	k1gInSc1
</s>
<s>
1200	#num#	k4
nám.	nám.	k?
mil	míle	k1gFnPc2
při	při	k7c6
16	#num#	k4
uzlech	uzel	k1gInPc6
Posádka	posádka	k1gFnSc1
</s>
<s>
19	#num#	k4
Výzbroj	výzbroj	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
40	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc4
<g/>
1	#num#	k4
<g/>
×	×	k?
20	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc4
<g/>
2	#num#	k4
<g/>
×	×	k?
12,7	12,7	k4
<g/>
mm	mm	kA
kulomet	kulomet	k1gInSc1
Radar	radar	k1gInSc1
</s>
<s>
Raytheon	Raytheon	k1gInSc1
1220	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
XB	XB	kA
</s>
<s>
Třída	třída	k1gFnSc1
Barceló	Barceló	k1gFnSc2
byla	být	k5eAaImAgFnS
třída	třída	k1gFnSc1
hlídkových	hlídkový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
postavených	postavený	k2eAgFnPc2d1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
70	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
pro	pro	k7c4
Španělské	španělský	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
na	na	k7c6
základě	základ	k1gInSc6
projektu	projekt	k1gInSc2
FPB-36	FPB-36	k1gFnSc2
německé	německý	k2eAgFnSc2d1
loděnice	loděnice	k1gFnSc2
Lürssen	Lürssna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
šest	šest	k4xCc1
člunů	člun	k1gInPc2
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavba	stavba	k1gFnSc1
</s>
<s>
Plavidla	plavidlo	k1gNnSc2
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
byla	být	k5eAaImAgFnS
navržena	navrhnout	k5eAaPmNgFnS
německou	německý	k2eAgFnSc7d1
loděnicí	loděnice	k1gFnSc7
Lürssen	Lürssna	k1gFnPc2
na	na	k7c6
základě	základ	k1gInSc6
její	její	k3xOp3gFnSc2
typové	typový	k2eAgFnSc2d1
řady	řada	k1gFnSc2
FPB-36	FPB-36	k1gFnSc2
(	(	kIx(
<g/>
číslo	číslo	k1gNnSc1
značí	značit	k5eAaImIp3nS
délku	délka	k1gFnSc4
trupu	trup	k1gInSc2
v	v	k7c6
metrech	metr	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šest	šest	k4xCc1
plavidel	plavidlo	k1gNnPc2
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
bylo	být	k5eAaImAgNnS
do	do	k7c2
služby	služba	k1gFnSc2
přijato	přijmout	k5eAaPmNgNnS
v	v	k7c6
letech	let	k1gInPc6
1976	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
prototyp	prototyp	k1gInSc4
postavila	postavit	k5eAaPmAgFnS
loděnice	loděnice	k1gFnSc1
Lürssen	Lürssna	k1gFnPc2
ve	v	k7c6
Vegesacku	Vegesack	k1gInSc6
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgNnPc4d1
plavidla	plavidlo	k1gNnPc4
postavila	postavit	k5eAaPmAgFnS
španělská	španělský	k2eAgFnSc1d1
loděnice	loděnice	k1gFnSc1
Empresa	Empresa	k1gFnSc1
Nacional	Nacional	k1gFnSc1
Bazan	Bazan	k1gInSc4
v	v	k7c6
La	la	k1gNnSc6
Carraca	Carrac	k1gInSc2
v	v	k7c6
Cádizu	Cádiz	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednotky	jednotka	k1gFnPc1
třídy	třída	k1gFnSc2
Barceló	Barceló	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
JménoSpuštěnaVstup	JménoSpuštěnaVstup	k1gInSc1
do	do	k7c2
službyStatus	službyStatus	k1gInSc1
</s>
<s>
Barceló	Barceló	k?
(	(	kIx(
<g/>
P	P	kA
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
197526	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1976	#num#	k4
<g/>
Vyřazena	vyřadit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Laya	Laya	k1gMnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
197523	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1976	#num#	k4
<g/>
Vyřazena	vyřadit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Javier	Javier	k1gInSc1
Quiroga	Quiroga	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
13	#num#	k4
<g/>
)	)	kIx)
<g/>
19751	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1977	#num#	k4
<g/>
Vyřazena	vyřazen	k2eAgFnSc1d1
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ordóñ	Ordóñ	k1gMnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
14	#num#	k4
<g/>
)	)	kIx)
<g/>
19767	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1977	#num#	k4
<g/>
Vyřazena	vyřadit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Acevedo	Acevedo	k1gNnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
197614	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1977	#num#	k4
<g/>
Vyřazena	vyřadit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Cándido	Cándida	k1gFnSc5
Pérez	Pérez	k1gMnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
197725	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1977	#num#	k4
<g/>
Vyřazena	vyřadit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
</s>
<s>
Barceló	Barceló	k?
(	(	kIx(
<g/>
P	P	kA
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Plavidla	plavidlo	k1gNnPc1
byla	být	k5eAaImAgNnP
vybavena	vybavit	k5eAaPmNgNnP
radarem	radar	k1gInSc7
Raytheon	Raytheon	k1gInSc4
1220	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
XB	XB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzbroj	výzbroj	k1gFnSc1
španělských	španělský	k2eAgInPc2d1
člunů	člun	k1gInPc2
tvořil	tvořit	k5eAaImAgInS
jeden	jeden	k4xCgMnSc1
40	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc4
Bofors	Boforsa	k1gFnPc2
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc4
20	#num#	k4
<g/>
mm	mm	kA
kanón	kanón	k1gInSc4
Oerlikon	Oerlikona	k1gFnPc2
a	a	k8xC
dva	dva	k4xCgInPc1
12,7	12,7	k4
<g/>
mm	mm	kA
kulomety	kulomet	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
výzbroj	výzbroj	k1gFnSc4
posílit	posílit	k5eAaPmF
o	o	k7c4
dva	dva	k4xCgInPc4
jednohlavňové	jednohlavňový	k2eAgInPc1d1
533	#num#	k4
<g/>
mm	mm	kA
torpédomety	torpédomet	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Pohonný	pohonný	k2eAgInSc1d1
systém	systém	k1gInSc1
tvořily	tvořit	k5eAaImAgInP
dva	dva	k4xCgInPc1
diesely	diesel	k1gInPc1
Bazan-MTU	Bazan-MTU	k1gFnSc2
16V538	16V538	k4
TB	TB	kA
<g/>
90	#num#	k4
<g/>
,	,	kIx,
o	o	k7c6
celkovém	celkový	k2eAgInSc6d1
výkonu	výkon	k1gInSc6
7300	#num#	k4
bhp	bhp	k?
<g/>
,	,	kIx,
pohánějící	pohánějící	k2eAgInPc4d1
dva	dva	k4xCgInPc4
lodní	lodní	k2eAgInPc4d1
šrouby	šroub	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
rychlost	rychlost	k1gFnSc1
dosahovala	dosahovat	k5eAaImAgFnS
36,5	36,5	k4
uzlu	uzel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosah	dosah	k1gInSc1
byl	být	k5eAaImAgInS
1200	#num#	k4
námořních	námořní	k2eAgFnPc2d1
mil	míle	k1gFnPc2
při	při	k7c6
rychlosti	rychlost	k1gFnSc6
16	#num#	k4
uzlů	uzel	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Class	Classa	k1gFnPc2
Barcelo	Barcelo	k1gFnSc2
Patrol	Patrol	k?
Boat	Boat	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Worldwarships	Worldwarships	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PEJČOCH	PEJČOCH	kA
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
;	;	kIx,
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
;	;	kIx,
HÁJEK	Hájek	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečné	válečný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
7	#num#	k4
–	–	k?
Druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
zemí	zem	k1gFnPc2
Evropy	Evropa	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ares	Ares	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86158	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
173	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PEJČOCH	PEJČOCH	kA
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
;	;	kIx,
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
;	;	kIx,
HÁJEK	Hájek	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečné	válečný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
7	#num#	k4
–	–	k?
Druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
zemí	zem	k1gFnPc2
Evropy	Evropa	k1gFnSc2
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ares	Ares	k1gMnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86158	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
353	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
PEJČOCH	PEJČOCH	kA
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečné	válečný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
8	#num#	k4
–	–	k?
Námořnictva	námořnictvo	k1gNnSc2
na	na	k7c6
přelomu	přelom	k1gInSc6
tisíciletí	tisíciletí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ares	Ares	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86158	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
455	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Třída	třída	k1gFnSc1
Barceló	Barceló	k1gMnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
</s>
