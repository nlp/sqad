<p>
<s>
Raynham	Raynham	k6eAd1	Raynham
Hall	Hall	k1gInSc1	Hall
je	být	k5eAaImIp3nS	být
venkovské	venkovský	k2eAgNnSc4d1	venkovské
šlechtické	šlechtický	k2eAgNnSc4d1	šlechtické
sídlo	sídlo	k1gNnSc4	sídlo
rodu	rod	k1gInSc2	rod
Townshendů	Townshend	k1gInPc2	Townshend
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
South	South	k1gInSc1	South
Raynham	Raynham	k1gInSc1	Raynham
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
hrabství	hrabství	k1gNnSc6	hrabství
Norfolk	Norfolka	k1gFnPc2	Norfolka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
domu	dům	k1gInSc2	dům
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1619	[number]	k4	1619
a	a	k8xC	a
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
průtahů	průtah	k1gInPc2	průtah
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
1637	[number]	k4	1637
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
architekta	architekt	k1gMnSc2	architekt
se	se	k3xPyFc4	se
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
<g/>
,	,	kIx,	,
každopádně	každopádně	k6eAd1	každopádně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
unikátní	unikátní	k2eAgInSc4d1	unikátní
příklad	příklad	k1gInSc4	příklad
počínajících	počínající	k2eAgInPc2d1	počínající
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
vlivů	vliv	k1gInPc2	vliv
na	na	k7c4	na
anglickou	anglický	k2eAgFnSc4d1	anglická
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
z	z	k7c2	z
červených	červený	k2eAgFnPc2d1	červená
cihel	cihla	k1gFnPc2	cihla
je	být	k5eAaImIp3nS	být
třípatrová	třípatrový	k2eAgFnSc1d1	třípatrová
<g/>
,	,	kIx,	,
s	s	k7c7	s
půdorysem	půdorys	k1gInSc7	půdorys
písmene	písmeno	k1gNnSc2	písmeno
H	H	kA	H
<g/>
,	,	kIx,	,
průčelí	průčelí	k1gNnSc1	průčelí
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
prvky	prvek	k1gInPc4	prvek
palladiánské	palladiánský	k2eAgFnSc2d1	palladiánský
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Interiéry	interiér	k1gInPc4	interiér
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
William	William	k1gInSc1	William
Kent	Kentum	k1gNnPc2	Kentum
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
cenné	cenný	k2eAgInPc1d1	cenný
jsou	být	k5eAaImIp3nP	být
mozaikové	mozaikový	k2eAgInPc1d1	mozaikový
stropy	strop	k1gInPc1	strop
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
byl	být	k5eAaImAgInS	být
proslulý	proslulý	k2eAgMnSc1d1	proslulý
množstvím	množství	k1gNnSc7	množství
vzácných	vzácný	k2eAgInPc2d1	vzácný
obrazů	obraz	k1gInPc2	obraz
od	od	k7c2	od
Anthonise	Anthonise	k1gFnSc2	Anthonise
van	vana	k1gFnPc2	vana
Dycka	Dycko	k1gNnSc2	Dycko
<g/>
,	,	kIx,	,
Joshuy	Joshua	k1gMnSc2	Joshua
Reynoldse	Reynolds	k1gMnSc2	Reynolds
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
prodána	prodat	k5eAaPmNgFnS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
krátce	krátce	k6eAd1	krátce
pobýval	pobývat	k5eAaImAgMnS	pobývat
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Stuart	Stuart	k1gInSc1	Stuart
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Townshendové	Townshend	k1gMnPc1	Townshend
byli	být	k5eAaImAgMnP	být
jeho	jeho	k3xOp3gMnPc7	jeho
blízkými	blízký	k2eAgMnPc7d1	blízký
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
proslulý	proslulý	k2eAgInSc1d1	proslulý
legendou	legenda	k1gFnSc7	legenda
o	o	k7c6	o
Hnědé	hnědý	k2eAgFnSc6d1	hnědá
paní	paní	k1gFnSc6	paní
z	z	k7c2	z
Raynham	Raynham	k1gInSc4	Raynham
Hall	Halla	k1gFnPc2	Halla
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
spisovatel	spisovatel	k1gMnSc1	spisovatel
Frederick	Frederick	k1gMnSc1	Frederick
Marryat	Marryat	k2eAgMnSc1d1	Marryat
<g/>
,	,	kIx,	,
popisovala	popisovat	k5eAaImAgNnP	popisovat
noční	noční	k2eAgNnPc1d1	noční
setkání	setkání	k1gNnPc1	setkání
s	s	k7c7	s
přízrakem	přízrak	k1gInSc7	přízrak
ženy	žena	k1gFnSc2	žena
v	v	k7c6	v
hnědých	hnědý	k2eAgInPc6d1	hnědý
šatech	šat	k1gInPc6	šat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zjevení	zjevení	k1gNnSc1	zjevení
je	být	k5eAaImIp3nS	být
dáváno	dávat	k5eAaImNgNnS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
Lady	lady	k1gFnSc7	lady
Dorothy	Dorotha	k1gFnSc2	Dorotha
(	(	kIx(	(
<g/>
1686	[number]	k4	1686
<g/>
–	–	k?	–
<g/>
1726	[number]	k4	1726
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sestrou	sestra	k1gFnSc7	sestra
Roberta	Robert	k1gMnSc2	Robert
Walpolea	Walpoleus	k1gMnSc2	Walpoleus
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
její	její	k3xOp3gMnSc1	její
žárlivý	žárlivý	k2eAgMnSc1d1	žárlivý
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
Charles	Charles	k1gMnSc1	Charles
Townshend	Townshend	k1gMnSc1	Townshend
<g/>
,	,	kIx,	,
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
věznil	věznit	k5eAaImAgMnS	věznit
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
byl	být	k5eAaImAgMnS	být
duch	duch	k1gMnSc1	duch
dokonce	dokonce	k9	dokonce
vyfotografován	vyfotografován	k2eAgMnSc1d1	vyfotografován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snímek	snímek	k1gInSc1	snímek
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
trik	trik	k1gInSc4	trik
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
vícenásobné	vícenásobný	k2eAgFnSc2d1	vícenásobná
expozice	expozice	k1gFnSc2	expozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
http://news.bbc.co.uk/local/norfolk/hi/people_and_places/history/newsid_8058000/8058145.stm	[url]	k6eAd1	http://news.bbc.co.uk/local/norfolk/hi/people_and_places/history/newsid_8058000/8058145.stm
</s>
</p>
<p>
<s>
http://www.britishlistedbuildings.co.uk/en-223885-raynham-hall-north-east-service-wing-and#.Ven_cNLtlHw	[url]	k4	http://www.britishlistedbuildings.co.uk/en-223885-raynham-hall-north-east-service-wing-and#.Ven_cNLtlHw
</s>
</p>
<p>
<s>
https://web.archive.org/web/20110726052925/http://www.mysteriousbritain.co.uk/england/norfolk/hauntings/the-brown-lady-of-raynham-hall.html	[url]	k1gMnSc1	https://web.archive.org/web/20110726052925/http://www.mysteriousbritain.co.uk/england/norfolk/hauntings/the-brown-lady-of-raynham-hall.html
</s>
</p>
