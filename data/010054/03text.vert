<p>
<s>
Káva	káva	k1gFnSc1	káva
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
horký	horký	k2eAgInSc4d1	horký
<g/>
)	)	kIx)	)
nápoj	nápoj	k1gInSc4	nápoj
z	z	k7c2	z
upražených	upražený	k2eAgNnPc2d1	upražené
a	a	k8xC	a
rozemletých	rozemletý	k2eAgNnPc2d1	rozemleté
semen	semeno	k1gNnPc2	semeno
plodů	plod	k1gInPc2	plod
kávovníku	kávovník	k1gInSc2	kávovník
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
i	i	k9	i
samotná	samotný	k2eAgNnPc1d1	samotné
semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
semena	semeno	k1gNnPc1	semeno
rozemletá	rozemletý	k2eAgNnPc1d1	rozemleté
na	na	k7c4	na
prášek	prášek	k1gInSc4	prášek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Káva	káva	k1gFnSc1	káva
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
svou	svůj	k3xOyFgFnSc7	svůj
silnou	silný	k2eAgFnSc7d1	silná
vůní	vůně	k1gFnSc7	vůně
(	(	kIx(	(
<g/>
aroma	aroma	k1gNnSc1	aroma
<g/>
)	)	kIx)	)
a	a	k8xC	a
černou	černý	k2eAgFnSc7d1	černá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
alkaloid	alkaloid	k1gInSc4	alkaloid
kofein	kofein	k1gInSc1	kofein
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
povzbuzuje	povzbuzovat	k5eAaImIp3nS	povzbuzovat
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
se	se	k3xPyFc4	se
káva	káva	k1gFnSc1	káva
pije	pít	k5eAaImIp3nS	pít
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
povzbuzující	povzbuzující	k2eAgInPc4d1	povzbuzující
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
nápojem	nápoj	k1gInSc7	nápoj
při	při	k7c6	při
setkávání	setkávání	k1gNnSc6	setkávání
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
podávána	podáván	k2eAgFnSc1d1	podávána
po	po	k7c6	po
jídle	jídlo	k1gNnSc6	jídlo
(	(	kIx(	(
<g/>
jako	jako	k9	jako
jakási	jakýsi	k3yIgFnSc1	jakýsi
"	"	kIx"	"
<g/>
tečka	tečka	k1gFnSc1	tečka
<g/>
"	"	kIx"	"
či	či	k8xC	či
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
trávení	trávení	k1gNnSc4	trávení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Káva	káva	k1gFnSc1	káva
se	se	k3xPyFc4	se
odborně	odborně	k6eAd1	odborně
připravuje	připravovat	k5eAaImIp3nS	připravovat
jako	jako	k9	jako
směs	směs	k1gFnSc1	směs
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
kávovníku	kávovník	k1gInSc2	kávovník
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
druhem	druh	k1gInSc7	druh
Coffea	Coffeus	k1gMnSc2	Coffeus
robusta	robust	k1gMnSc2	robust
a	a	k8xC	a
Coffea	Coffeus	k1gMnSc2	Coffeus
arabica	arabicus	k1gMnSc2	arabicus
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
používané	používaný	k2eAgFnPc1d1	používaná
Coffea	Coffea	k1gFnSc1	Coffea
excelsa	excelsa	k1gFnSc1	excelsa
a	a	k8xC	a
Coffea	Coffe	k2eAgFnSc1d1	Coffea
liberica	liberica	k1gFnSc1	liberica
se	se	k3xPyFc4	se
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
zřídka	zřídka	k6eAd1	zřídka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
kávy	káva	k1gFnSc2	káva
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
káva	káva	k1gFnSc1	káva
arabica	arabica	k1gFnSc1	arabica
a	a	k8xC	a
robusta	robusta	k1gFnSc1	robusta
<g/>
.	.	kIx.	.
</s>
<s>
Arabika	Arabika	k1gFnSc1	Arabika
(	(	kIx(	(
<g/>
cca	cca	kA	cca
70	[number]	k4	70
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
káva	káva	k1gFnSc1	káva
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
obsahem	obsah	k1gInSc7	obsah
kofeinu	kofein	k1gInSc2	kofein
a	a	k8xC	a
širokým	široký	k2eAgNnSc7d1	široké
spektrem	spektrum	k1gNnSc7	spektrum
jemných	jemný	k2eAgFnPc2d1	jemná
chutí	chuť	k1gFnPc2	chuť
a	a	k8xC	a
vůní	vůně	k1gFnPc2	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
(	(	kIx(	(
<g/>
900	[number]	k4	900
<g/>
–	–	k?	–
<g/>
2800	[number]	k4	2800
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
na	na	k7c6	na
sopečné	sopečný	k2eAgFnSc6d1	sopečná
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
či	či	k8xC	či
jako	jako	k9	jako
lesní	lesní	k2eAgFnSc1d1	lesní
forma	forma	k1gFnSc1	forma
(	(	kIx(	(
<g/>
např.	např.	kA	např.
etiopská	etiopský	k2eAgFnSc1d1	etiopská
káva	káva	k1gFnSc1	káva
Harar	Harar	k1gMnSc1	Harar
či	či	k8xC	či
Wild	Wild	k1gMnSc1	Wild
Forest	Forest	k1gMnSc1	Forest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zrna	zrno	k1gNnPc1	zrno
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgNnPc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
robusty	robusta	k1gFnSc2	robusta
a	a	k8xC	a
během	během	k7c2	během
pražení	pražení	k1gNnSc2	pražení
i	i	k9	i
při	při	k7c6	při
samotné	samotný	k2eAgFnSc6d1	samotná
přípravě	příprava	k1gFnSc6	příprava
nápoje	nápoj	k1gInSc2	nápoj
jsou	být	k5eAaImIp3nP	být
zrnka	zrnko	k1gNnPc1	zrnko
arabiky	arabika	k1gFnSc2	arabika
velice	velice	k6eAd1	velice
citlivá	citlivý	k2eAgNnPc1d1	citlivé
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Robusta	Robusta	k1gFnSc1	Robusta
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
náročná	náročný	k2eAgFnSc1d1	náročná
odrůda	odrůda	k1gFnSc1	odrůda
kávovníku	kávovník	k1gInSc2	kávovník
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
produkci	produkce	k1gFnSc6	produkce
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
25	[number]	k4	25
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgMnPc3d3	veliký
producentům	producent	k1gMnPc3	producent
patří	patřit	k5eAaImIp3nS	patřit
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
a	a	k8xC	a
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Zrnka	zrnko	k1gNnPc1	zrnko
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgNnPc1d2	menší
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
cca	cca	kA	cca
2	[number]	k4	2
<g/>
x	x	k?	x
více	hodně	k6eAd2	hodně
kofeinu	kofein	k1gInSc2	kofein
a	a	k8xC	a
nápoj	nápoj	k1gInSc4	nápoj
je	být	k5eAaImIp3nS	být
chuťově	chuťově	k6eAd1	chuťově
výraznější	výrazný	k2eAgFnSc1d2	výraznější
<g/>
,	,	kIx,	,
zemitější	zemitý	k2eAgFnSc1d2	zemitější
<g/>
.	.	kIx.	.
</s>
<s>
Káva	káva	k1gFnSc1	káva
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
ropě	ropa	k1gFnSc6	ropa
nejprodávanější	prodávaný	k2eAgFnSc7d3	nejprodávanější
komoditou	komodita	k1gFnSc7	komodita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kávovník	kávovník	k1gInSc4	kávovník
==	==	k?	==
</s>
</p>
<p>
<s>
Kávová	kávový	k2eAgNnPc4d1	kávové
semena	semeno	k1gNnPc4	semeno
jsou	být	k5eAaImIp3nP	být
semena	semeno	k1gNnPc1	semeno
plodů	plod	k1gInPc2	plod
rostliny	rostlina	k1gFnPc1	rostlina
zvané	zvaný	k2eAgFnSc2d1	zvaná
kávovník	kávovník	k1gInSc4	kávovník
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc4	plod
lze	lze	k6eAd1	lze
přirovnat	přirovnat	k5eAaPmF	přirovnat
k	k	k7c3	k
tvrdé	tvrdý	k2eAgFnSc3d1	tvrdá
třešni	třešeň	k1gFnSc3	třešeň
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
tvarem	tvar	k1gInSc7	tvar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
sklizně	sklizeň	k1gFnSc2	sklizeň
červená	červený	k2eAgFnSc1d1	červená
nebo	nebo	k8xC	nebo
červenofialová	červenofialový	k2eAgFnSc1d1	červenofialová
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
plodu	plod	k1gInSc2	plod
jsou	být	k5eAaImIp3nP	být
ukryta	ukryt	k2eAgNnPc1d1	ukryto
dvě	dva	k4xCgFnPc4	dva
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
položená	položená	k1gFnSc1	položená
semena	semeno	k1gNnSc2	semeno
–	–	k?	–
zelená	zelenat	k5eAaImIp3nS	zelenat
kávová	kávový	k2eAgNnPc4d1	kávové
zrna	zrno	k1gNnPc4	zrno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
rostlina	rostlina	k1gFnSc1	rostlina
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
mořenovitých	mořenovitý	k2eAgFnPc2d1	mořenovitý
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
divoce	divoce	k6eAd1	divoce
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
strom	strom	k1gInSc1	strom
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
na	na	k7c6	na
kávovníkových	kávovníkový	k2eAgFnPc6d1	kávovníkový
plantážích	plantáž	k1gFnPc6	plantáž
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
buď	buď	k8xC	buď
jako	jako	k9	jako
strom	strom	k1gInSc1	strom
nebo	nebo	k8xC	nebo
keř	keř	k1gInSc1	keř
<g/>
.	.	kIx.	.
</s>
<s>
Existující	existující	k2eAgInPc1d1	existující
druhy	druh	k1gInPc1	druh
kávovníků	kávovník	k1gInPc2	kávovník
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
kříží	křížit	k5eAaImIp3nP	křížit
a	a	k8xC	a
šlechtí	šlechtit	k5eAaImIp3nP	šlechtit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
větší	veliký	k2eAgFnSc7d2	veliký
odolnosti	odolnost	k1gFnSc6	odolnost
proti	proti	k7c3	proti
škůdcům	škůdce	k1gMnPc3	škůdce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pokud	pokud	k8xS	pokud
napadnou	napadnout	k5eAaPmIp3nP	napadnout
kávovníky	kávovník	k1gInPc1	kávovník
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
škody	škoda	k1gFnPc4	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
cizopasná	cizopasný	k2eAgFnSc1d1	cizopasná
houba	houba	k1gFnSc1	houba
dokáže	dokázat	k5eAaPmIp3nS	dokázat
totálně	totálně	k6eAd1	totálně
zničit	zničit	k5eAaPmF	zničit
i	i	k9	i
celé	celý	k2eAgFnPc4d1	celá
plantáže	plantáž	k1gFnPc4	plantáž
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc4	příklad
této	tento	k3xDgFnSc2	tento
zkázy	zkáza	k1gFnSc2	zkáza
můžeme	moct	k5eAaImIp1nP	moct
uvést	uvést	k5eAaPmF	uvést
ostrov	ostrov	k1gInSc4	ostrov
Cejlon	Cejlon	k1gInSc1	Cejlon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
kávovníkové	kávovníkový	k2eAgFnPc1d1	kávovníkový
plantáže	plantáž	k1gFnPc1	plantáž
zcela	zcela	k6eAd1	zcela
nahrazeny	nahrazen	k2eAgInPc4d1	nahrazen
čajovníky	čajovník	k1gInPc4	čajovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravlastí	pravlast	k1gFnSc7	pravlast
kávovníku	kávovník	k1gInSc2	kávovník
je	být	k5eAaImIp3nS	být
africký	africký	k2eAgInSc1d1	africký
kontinent	kontinent	k1gInSc1	kontinent
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
oblast	oblast	k1gFnSc4	oblast
Kaffa	Kaff	k1gMnSc2	Kaff
kde	kde	k6eAd1	kde
káva	káva	k1gFnSc1	káva
dodnes	dodnes	k6eAd1	dodnes
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
divoce	divoce	k6eAd1	divoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
káva	káva	k1gFnSc1	káva
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
kolébku	kolébka	k1gFnSc4	kolébka
kávy	káva	k1gFnSc2	káva
-	-	kIx~	-
nápoje	nápoj	k1gInPc1	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
kávovníky	kávovník	k1gInPc1	kávovník
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Všude	všude	k6eAd1	všude
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
růst	růst	k1gInSc4	růst
příznivé	příznivý	k2eAgNnSc4d1	příznivé
podnebí	podnebí	k1gNnSc4	podnebí
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
ekologické	ekologický	k2eAgFnPc4d1	ekologická
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Kávovníky	kávovník	k1gInPc1	kávovník
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgInSc2	svůj
růstu	růst	k1gInSc2	růst
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
dostatek	dostatek	k1gInSc4	dostatek
vláhy	vláha	k1gFnSc2	vláha
i	i	k8xC	i
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nejlépe	dobře	k6eAd3	dobře
daří	dařit	k5eAaImIp3nS	dařit
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
tropickém	tropický	k2eAgNnSc6d1	tropické
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgInSc3	každý
druhu	druh	k1gInSc3	druh
kávovníku	kávovník	k1gInSc2	kávovník
ale	ale	k8xC	ale
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
trochu	trochu	k6eAd1	trochu
odlišné	odlišný	k2eAgNnSc4d1	odlišné
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Kávovníkové	kávovníkový	k2eAgInPc1d1	kávovníkový
stromy	strom	k1gInPc1	strom
nebo	nebo	k8xC	nebo
keře	keř	k1gInPc1	keř
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
poměrně	poměrně	k6eAd1	poměrně
vysokého	vysoký	k2eAgInSc2d1	vysoký
věku	věk	k1gInSc2	věk
(	(	kIx(	(
<g/>
až	až	k9	až
třiceti	třicet	k4xCc2	třicet
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
nižší	nízký	k2eAgFnSc4d2	nižší
úrodu	úroda	k1gFnSc4	úroda
<g/>
;	;	kIx,	;
nejvyšší	vysoký	k2eAgInPc1d3	Nejvyšší
výnosy	výnos	k1gInPc1	výnos
dávají	dávat	k5eAaImIp3nP	dávat
kávovníky	kávovník	k1gInPc4	kávovník
po	po	k7c6	po
pěti	pět	k4xCc6	pět
až	až	k8xS	až
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k9	co
nejvyšším	vysoký	k2eAgInPc3d3	Nejvyšší
výnosům	výnos	k1gInPc3	výnos
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
pomoci	pomoct	k5eAaPmF	pomoct
správnou	správný	k2eAgFnSc7d1	správná
péčí	péče	k1gFnSc7	péče
<g/>
.	.	kIx.	.
</s>
<s>
Kávovníky	kávovník	k1gInPc1	kávovník
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jiné	jiný	k2eAgNnSc1d1	jiné
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
méně	málo	k6eAd2	málo
exotické	exotický	k2eAgFnSc2d1	exotická
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
přihnojovat	přihnojovat	k5eAaImF	přihnojovat
<g/>
,	,	kIx,	,
prořezávat	prořezávat	k5eAaImF	prořezávat
a	a	k8xC	a
chránit	chránit	k5eAaImF	chránit
nově	nově	k6eAd1	nově
vzrostlé	vzrostlý	k2eAgFnPc1d1	vzrostlá
rostliny	rostlina	k1gFnPc1	rostlina
proti	proti	k7c3	proti
prudkému	prudký	k2eAgNnSc3d1	prudké
a	a	k8xC	a
žhnoucímu	žhnoucí	k2eAgNnSc3d1	žhnoucí
slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
keře	keř	k1gInSc2	keř
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
až	až	k9	až
2,5	[number]	k4	2,5
kg	kg	kA	kg
zelené	zelený	k2eAgFnSc2d1	zelená
kávy	káva	k1gFnSc2	káva
a	a	k8xC	a
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
0,5	[number]	k4	0,5
kg	kg	kA	kg
pražené	pražený	k2eAgFnSc2d1	pražená
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
základní	základní	k2eAgInPc1d1	základní
druhy	druh	k1gInPc1	druh
kávovníků	kávovník	k1gInPc2	kávovník
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
růstem	růst	k1gInSc7	růst
<g/>
,	,	kIx,	,
svými	svůj	k3xOyFgInPc7	svůj
nároky	nárok	k1gInPc7	nárok
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
i	i	k8xC	i
finální	finální	k2eAgFnSc7d1	finální
chutí	chuť	k1gFnSc7	chuť
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
kávovník	kávovník	k1gInSc1	kávovník
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
chuť	chuť	k1gFnSc4	chuť
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Kávovníky	kávovník	k1gInPc1	kávovník
pěstované	pěstovaný	k2eAgInPc1d1	pěstovaný
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
kávová	kávový	k2eAgNnPc4d1	kávové
zrna	zrno	k1gNnPc4	zrno
jemnější	jemný	k2eAgFnSc2d2	jemnější
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
obsahem	obsah	k1gInSc7	obsah
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
druh	druh	k1gInSc4	druh
kávovníku	kávovník	k1gInSc2	kávovník
patří	patřit	k5eAaImIp3nS	patřit
Arabský	arabský	k2eAgInSc4d1	arabský
kávovník	kávovník	k1gInSc4	kávovník
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Kávovníky	kávovník	k1gInPc1	kávovník
rostoucí	rostoucí	k2eAgInPc1d1	rostoucí
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
výškách	výška	k1gFnPc6	výška
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
odolné	odolný	k2eAgFnPc1d1	odolná
proti	proti	k7c3	proti
škůdcům	škůdce	k1gMnPc3	škůdce
a	a	k8xC	a
chorobám	choroba	k1gFnPc3	choroba
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
velmi	velmi	k6eAd1	velmi
choulostivé	choulostivý	k2eAgNnSc1d1	choulostivé
na	na	k7c4	na
mrazíky	mrazík	k1gInPc4	mrazík
<g/>
.	.	kIx.	.
</s>
<s>
Vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
mu	on	k3xPp3gNnSc3	on
mírnější	mírný	k2eAgFnPc1d2	mírnější
teploty	teplota	k1gFnPc1	teplota
nepřesahující	přesahující	k2eNgFnSc2d1	nepřesahující
24	[number]	k4	24
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Plody	plod	k1gInPc1	plod
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
nejkvalitnější	kvalitní	k2eAgNnPc4d3	nejkvalitnější
kávová	kávový	k2eAgNnPc4d1	kávové
zrna	zrno	k1gNnPc4	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Káva	káva	k1gFnSc1	káva
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
má	mít	k5eAaImIp3nS	mít
nízký	nízký	k2eAgInSc4d1	nízký
obsah	obsah	k1gInSc4	obsah
kofeinu	kofein	k1gInSc2	kofein
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
chuť	chuť	k1gFnSc1	chuť
je	být	k5eAaImIp3nS	být
jemná	jemný	k2eAgFnSc1d1	jemná
a	a	k8xC	a
nasládle	nasládle	k6eAd1	nasládle
lahodná	lahodný	k2eAgFnSc1d1	lahodná
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
produkce	produkce	k1gFnSc2	produkce
kávy	káva	k1gFnSc2	káva
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
zpracování	zpracování	k1gNnSc1	zpracování
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
,	,	kIx,	,
zrna	zrno	k1gNnPc1	zrno
se	se	k3xPyFc4	se
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
"	"	kIx"	"
<g/>
mokrou	mokrý	k2eAgFnSc7d1	mokrá
metodou	metoda	k1gFnSc7	metoda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
káva	káva	k1gFnSc1	káva
projde	projít	k5eAaPmIp3nS	projít
procesem	proces	k1gInSc7	proces
fermentace	fermentace	k1gFnSc2	fermentace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
zpracování	zpracování	k1gNnSc2	zpracování
je	být	k5eAaImIp3nS	být
finančně	finančně	k6eAd1	finančně
náročnější	náročný	k2eAgNnSc1d2	náročnější
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
také	také	k9	také
výsledná	výsledný	k2eAgFnSc1d1	výsledná
cena	cena	k1gFnSc1	cena
této	tento	k3xDgFnSc2	tento
kávy	káva	k1gFnSc2	káva
vyšší	vysoký	k2eAgInPc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhým	druhý	k4xOgInSc7	druhý
významným	významný	k2eAgInSc7d1	významný
kávovníkem	kávovník	k1gInSc7	kávovník
je	být	k5eAaImIp3nS	být
robusta	robusta	k1gFnSc1	robusta
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
tmavší	tmavý	k2eAgFnSc7d2	tmavší
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
výraznější	výrazný	k2eAgFnSc1d2	výraznější
<g/>
,	,	kIx,	,
zemitější	zemitý	k2eAgFnSc1d2	zemitější
(	(	kIx(	(
<g/>
drsnější	drsný	k2eAgFnSc1d2	drsnější
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
vytříbenou	vytříbený	k2eAgFnSc7d1	vytříbená
chutí	chuť	k1gFnSc7	chuť
než	než	k8xS	než
arabský	arabský	k2eAgInSc1d1	arabský
kávovník	kávovník	k1gInSc1	kávovník
<g/>
.	.	kIx.	.
</s>
<s>
Robusta	Robusta	k1gFnSc1	Robusta
není	být	k5eNaImIp3nS	být
tolik	tolik	k6eAd1	tolik
náročná	náročný	k2eAgFnSc1d1	náročná
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odolnější	odolný	k2eAgMnSc1d2	odolnější
proti	proti	k7c3	proti
škůdcům	škůdce	k1gMnPc3	škůdce
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vyšší	vysoký	k2eAgInPc4d2	vyšší
výnosy	výnos	k1gInPc4	výnos
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
kávovníku	kávovník	k1gInSc3	kávovník
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
stálejší	stálý	k2eAgNnSc1d2	stálejší
počasí	počasí	k1gNnSc1	počasí
s	s	k7c7	s
vyššími	vysoký	k2eAgFnPc7d2	vyšší
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
snáší	snášet	k5eAaImIp3nS	snášet
teploty	teplota	k1gFnPc4	teplota
kolem	kolem	k7c2	kolem
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Kávová	kávový	k2eAgNnPc1d1	kávové
zrna	zrno	k1gNnPc1	zrno
jsou	být	k5eAaImIp3nP	být
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
suchou	suchý	k2eAgFnSc7d1	suchá
metodou	metoda	k1gFnSc7	metoda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
finančně	finančně	k6eAd1	finančně
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
cena	cena	k1gFnSc1	cena
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
především	především	k9	především
do	do	k7c2	do
směsí	směs	k1gFnPc2	směs
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
druhy	druh	k1gInPc7	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
arabikou	arabika	k1gFnSc7	arabika
snižuje	snižovat	k5eAaImIp3nS	snižovat
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejméně	málo	k6eAd3	málo
významným	významný	k2eAgMnSc7d1	významný
druhem	druh	k1gMnSc7	druh
kávovníku	kávovník	k1gInSc2	kávovník
je	být	k5eAaImIp3nS	být
Coffea	Coffe	k2eAgFnSc1d1	Coffea
liberica	liberica	k1gFnSc1	liberica
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůstem	vzrůst	k1gInSc7	vzrůst
je	být	k5eAaImIp3nS	být
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
kávovníku	kávovník	k1gInSc2	kávovník
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
<g/>
.	.	kIx.	.
</s>
<s>
Dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
až	až	k9	až
18	[number]	k4	18
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
arabika	arabika	k1gFnSc1	arabika
8	[number]	k4	8
m	m	kA	m
<g/>
,	,	kIx,	,
robusta	robusta	k1gFnSc1	robusta
10	[number]	k4	10
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
plody	plod	k1gInPc1	plod
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
velká	velký	k2eAgNnPc4d1	velké
zrna	zrno	k1gNnPc4	zrno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
hořké	hořká	k1gFnPc4	hořká
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Liberica	Liberica	k6eAd1	Liberica
sice	sice	k8xC	sice
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
velké	velký	k2eAgInPc4d1	velký
výnosy	výnos	k1gInPc4	výnos
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
kvalita	kvalita	k1gFnSc1	kvalita
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
pouze	pouze	k6eAd1	pouze
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
našem	náš	k3xOp1gInSc6	náš
trhu	trh	k1gInSc6	trh
se	se	k3xPyFc4	se
prodávají	prodávat	k5eAaImIp3nP	prodávat
převážně	převážně	k6eAd1	převážně
směsi	směs	k1gFnPc1	směs
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
základních	základní	k2eAgInPc2d1	základní
druhů	druh	k1gInPc2	druh
zrn	zrno	k1gNnPc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
správnou	správný	k2eAgFnSc7d1	správná
kombinací	kombinace	k1gFnSc7	kombinace
vhodně	vhodně	k6eAd1	vhodně
zvolených	zvolený	k2eAgInPc2d1	zvolený
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
směs	směs	k1gFnSc1	směs
s	s	k7c7	s
harmonickou	harmonický	k2eAgFnSc7d1	harmonická
a	a	k8xC	a
ucelenou	ucelený	k2eAgFnSc7d1	ucelená
chutí	chuť	k1gFnSc7	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
čisté	čistý	k2eAgInPc1d1	čistý
druhy	druh	k1gInPc1	druh
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
specializovaných	specializovaný	k2eAgInPc6d1	specializovaný
obchodech	obchod	k1gInPc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Směsi	směs	k1gFnPc1	směs
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
připravují	připravovat	k5eAaImIp3nP	připravovat
ze	z	k7c2	z
zelených	zelená	k1gFnPc2	zelená
zrn	zrno	k1gNnPc2	zrno
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
dohromady	dohromady	k6eAd1	dohromady
praží	pražit	k5eAaImIp3nS	pražit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
směsi	směs	k1gFnSc2	směs
jsou	být	k5eAaImIp3nP	být
rozhodující	rozhodující	k2eAgFnPc4d1	rozhodující
nejen	nejen	k6eAd1	nejen
požadované	požadovaný	k2eAgFnPc4d1	požadovaná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
pije	pít	k5eAaImIp3nS	pít
káva	káva	k1gFnSc1	káva
ze	z	k7c2	z
zrnek	zrnko	k1gNnPc2	zrnko
druhu	druh	k1gInSc2	druh
arabika	arabik	k1gMnSc2	arabik
a	a	k8xC	a
robusta	robust	k1gMnSc2	robust
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
výsledná	výsledný	k2eAgFnSc1d1	výsledná
káva	káva	k1gFnSc1	káva
byla	být	k5eAaImAgFnS	být
co	co	k9	co
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
chuti	chuť	k1gFnPc4	chuť
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
odrůdě	odrůda	k1gFnSc6	odrůda
pěstovaných	pěstovaný	k2eAgInPc2d1	pěstovaný
kávovníků	kávovník	k1gInPc2	kávovník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
na	na	k7c6	na
sklizni	sklizeň	k1gFnSc6	sklizeň
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
následném	následný	k2eAgNnSc6d1	následné
zpracování	zpracování	k1gNnSc6	zpracování
kávových	kávový	k2eAgNnPc2d1	kávové
zrn	zrno	k1gNnPc2	zrno
<g/>
,	,	kIx,	,
na	na	k7c6	na
správném	správný	k2eAgNnSc6d1	správné
pražení	pražení	k1gNnSc6	pražení
<g/>
,	,	kIx,	,
uskladnění	uskladnění	k1gNnSc6	uskladnění
a	a	k8xC	a
expedování	expedování	k1gNnSc6	expedování
kávových	kávový	k2eAgInPc2d1	kávový
žoků	žok	k1gInPc2	žok
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
dílčích	dílčí	k2eAgFnPc2d1	dílčí
prací	práce	k1gFnPc2	práce
má	mít	k5eAaImIp3nS	mít
pevná	pevný	k2eAgNnPc4d1	pevné
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
nedodržení	nedodržení	k1gNnPc2	nedodržení
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
projeví	projevit	k5eAaPmIp3nS	projevit
negativně	negativně	k6eAd1	negativně
na	na	k7c6	na
výsledné	výsledný	k2eAgFnSc6d1	výsledná
chuti	chuť	k1gFnSc6	chuť
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
kávy	káva	k1gFnSc2	káva
==	==	k?	==
</s>
</p>
<p>
<s>
Příprava	příprava	k1gFnSc1	příprava
kávy	káva	k1gFnSc2	káva
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c1	mnoho
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
<g/>
:	:	kIx,	:
filtrovaná	filtrovaný	k2eAgFnSc1d1	filtrovaná
káva	káva	k1gFnSc1	káva
(	(	kIx(	(
<g/>
drip	drip	k1gInSc1	drip
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
espresso	espressa	k1gFnSc5	espressa
<g/>
,	,	kIx,	,
vacuum	vacuum	k1gInSc1	vacuum
pot	pot	k1gInSc1	pot
<g/>
,	,	kIx,	,
Aeropress	Aeropress	k1gInSc1	Aeropress
<g/>
,	,	kIx,	,
cafeteria	cafeterium	k1gNnPc1	cafeterium
(	(	kIx(	(
<g/>
french	french	k1gInSc1	french
press	press	k1gInSc1	press
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mokka	mokka	k1gNnSc1	mokka
press	pressa	k1gFnPc2	pressa
<g/>
,	,	kIx,	,
instantní	instantní	k2eAgFnSc1d1	instantní
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
turecká	turecký	k2eAgNnPc1d1	turecké
mokka	mokka	k1gNnPc1	mokka
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
také	také	k9	také
tzv.	tzv.	kA	tzv.
turecká	turecký	k2eAgFnSc1d1	turecká
káva	káva	k1gFnSc1	káva
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
kávy	káva	k1gFnSc2	káva
připravované	připravovaný	k2eAgFnSc2d1	připravovaná
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
alternativy	alternativa	k1gFnPc1	alternativa
k	k	k7c3	k
espressu	espress	k1gInSc3	espress
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
jednotlivou	jednotlivý	k2eAgFnSc4d1	jednotlivá
přípravu	příprava	k1gFnSc4	příprava
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
především	především	k6eAd1	především
použít	použít	k5eAaPmF	použít
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
čerstvě	čerstvě	k6eAd1	čerstvě
praženou	pražený	k2eAgFnSc4d1	pražená
kávu	káva	k1gFnSc4	káva
a	a	k8xC	a
čistou	čistý	k2eAgFnSc4d1	čistá
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
důležité	důležitý	k2eAgNnSc1d1	důležité
dodržet	dodržet	k5eAaPmF	dodržet
správný	správný	k2eAgInSc4d1	správný
postup	postup	k1gInSc4	postup
přípravy	příprava	k1gFnSc2	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
rozhodujících	rozhodující	k2eAgInPc2d1	rozhodující
faktorů	faktor	k1gInPc2	faktor
je	být	k5eAaImIp3nS	být
správné	správný	k2eAgNnSc4d1	správné
mletí	mletí	k1gNnSc4	mletí
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
především	především	k9	především
pro	pro	k7c4	pro
přípravy	příprava	k1gFnPc4	příprava
filtrovaných	filtrovaný	k2eAgFnPc2d1	filtrovaná
káv	káva	k1gFnPc2	káva
čerstvě	čerstvě	k6eAd1	čerstvě
a	a	k8xC	a
světle	světle	k6eAd1	světle
praženou	pražený	k2eAgFnSc4d1	pražená
kávu	káva	k1gFnSc4	káva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Turecká	turecký	k2eAgFnSc1d1	turecká
káva	káva	k1gFnSc1	káva
===	===	k?	===
</s>
</p>
<p>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
turek	turek	k1gMnSc1	turek
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
slitím	slití	k1gNnSc7	slití
jemně	jemně	k6eAd1	jemně
namleté	namletý	k2eAgFnSc2d1	namletá
kávy	káva	k1gFnSc2	káva
v	v	k7c6	v
šálku	šálek	k1gInSc6	šálek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
příprava	příprava	k1gFnSc1	příprava
kávy	káva	k1gFnSc2	káva
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgFnSc7d1	populární
a	a	k8xC	a
tradiční	tradiční	k2eAgFnSc7d1	tradiční
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
přípravy	příprava	k1gFnSc2	příprava
kávy	káva	k1gFnSc2	káva
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
obvyklý	obvyklý	k2eAgInSc1d1	obvyklý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Prudké	Prudké	k2eAgNnSc1d1	Prudké
spaření	spaření	k1gNnSc1	spaření
kávy	káva	k1gFnSc2	káva
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nešetrné	šetrný	k2eNgNnSc1d1	nešetrné
a	a	k8xC	a
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
třísloviny	tříslovina	k1gFnPc4	tříslovina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
nakyslou	nakyslý	k2eAgFnSc4d1	nakyslá
až	až	k9	až
řezavou	řezavý	k2eAgFnSc4d1	řezavá
chuť	chuť	k1gFnSc4	chuť
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Turecká	turecký	k2eAgNnPc1d1	turecké
mokka	mokka	k1gNnPc1	mokka
===	===	k?	===
</s>
</p>
<p>
<s>
Turecká	turecký	k2eAgNnPc4d1	turecké
mokka	mokka	k1gNnPc4	mokka
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
způsob	způsob	k1gInSc1	způsob
pití	pití	k1gNnSc2	pití
kávy	káva	k1gFnSc2	káva
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
kovová	kovový	k2eAgFnSc1d1	kovová
konvička	konvička	k1gFnSc1	konvička
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
džezva	džezva	k1gFnSc1	džezva
<g/>
,	,	kIx,	,
ibrik	ibrik	k1gInSc1	ibrik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgNnPc4d1	turecké
mokka	mokka	k1gNnPc4	mokka
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
asi	asi	k9	asi
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejsilnějších	silný	k2eAgFnPc2d3	nejsilnější
káv	káva	k1gFnPc2	káva
co	co	k8xS	co
se	se	k3xPyFc4	se
obsahu	obsah	k1gInSc2	obsah
kofeinu	kofein	k1gInSc2	kofein
týče	týkat	k5eAaImIp3nS	týkat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
silnější	silný	k2eAgMnSc1d2	silnější
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc1	její
chuť	chuť	k1gFnSc1	chuť
kultivovanější	kultivovaný	k2eAgFnSc1d2	kultivovanější
než	než	k8xS	než
u	u	k7c2	u
"	"	kIx"	"
<g/>
českého	český	k2eAgInSc2d1	český
turka	turek	k1gInSc2	turek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
turecká	turecký	k2eAgFnSc1d1	turecká
káva	káva	k1gFnSc1	káva
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
připravována	připravovat	k5eAaImNgFnS	připravovat
rovnou	rovnou	k6eAd1	rovnou
s	s	k7c7	s
cukrem	cukr	k1gInSc7	cukr
a	a	k8xC	a
kořením	koření	k1gNnSc7	koření
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
skořice	skořice	k1gFnSc1	skořice
<g/>
,	,	kIx,	,
vanilka	vanilka	k1gFnSc1	vanilka
a	a	k8xC	a
kardamom	kardamom	k1gInSc1	kardamom
<g/>
.	.	kIx.	.
</s>
<s>
Káva	káva	k1gFnSc1	káva
bez	bez	k7c2	bez
těchto	tento	k3xDgFnPc2	tento
uvedených	uvedený	k2eAgFnPc2d1	uvedená
ingrediencí	ingredience	k1gFnPc2	ingredience
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pije	pít	k5eAaImIp3nS	pít
jen	jen	k9	jen
při	při	k7c6	při
smutečních	smuteční	k2eAgFnPc6d1	smuteční
hostinách	hostina	k1gFnPc6	hostina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
džezvy	džezva	k1gFnSc2	džezva
nasypeme	nasypat	k5eAaPmIp1nP	nasypat
velmi	velmi	k6eAd1	velmi
jemně	jemně	k6eAd1	jemně
semletou	semletý	k2eAgFnSc4d1	semletá
kávu	káva	k1gFnSc4	káva
s	s	k7c7	s
kořením	koření	k1gNnSc7	koření
a	a	k8xC	a
cukrem	cukr	k1gInSc7	cukr
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
cca	cca	kA	cca
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
zalijeme	zalít	k5eAaPmIp1nP	zalít
studenou	studený	k2eAgFnSc7d1	studená
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
jen	jen	k9	jen
ve	v	k7c6	v
výjimečných	výjimečný	k2eAgInPc6d1	výjimečný
případech	případ	k1gInPc6	případ
mírně	mírně	k6eAd1	mírně
předehřátou	předehřátý	k2eAgFnSc4d1	předehřátá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postavíme	postavit	k5eAaPmIp1nP	postavit
na	na	k7c4	na
tepelný	tepelný	k2eAgInSc4d1	tepelný
zdroj	zdroj	k1gInSc4	zdroj
a	a	k8xC	a
čekáme	čekat	k5eAaImIp1nP	čekat
kdy	kdy	k6eAd1	kdy
káva	káva	k1gFnSc1	káva
poprvé	poprvé	k6eAd1	poprvé
vzkypí	vzkypět	k5eAaPmIp3nS	vzkypět
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
obsah	obsah	k1gInSc1	obsah
díky	díky	k7c3	díky
pomalému	pomalý	k2eAgInSc3d1	pomalý
ohřevu	ohřev	k1gInSc3	ohřev
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
zúženou	zúžený	k2eAgFnSc4d1	zúžená
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nejdůležitější	důležitý	k2eAgFnSc3d3	nejdůležitější
fázi	fáze	k1gFnSc3	fáze
přípravy	příprava	k1gFnSc2	příprava
<g/>
,	,	kIx,	,
pomalé	pomalý	k2eAgNnSc1d1	pomalé
propařování	propařování	k1gNnSc1	propařování
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
káva	káva	k1gFnSc1	káva
vzkypí	vzkypět	k5eAaPmIp3nS	vzkypět
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
by	by	kYmCp3nS	by
z	z	k7c2	z
džezvy	džezva	k1gFnSc2	džezva
přetekla	přetéct	k5eAaPmAgFnS	přetéct
<g/>
,	,	kIx,	,
odstavíme	odstavit	k5eAaPmIp1nP	odstavit
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
asi	asi	k9	asi
půl	půl	k6eAd1	půl
minuty	minuta	k1gFnPc4	minuta
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
proces	proces	k1gInSc4	proces
opakujme	opakovat	k5eAaImRp1nP	opakovat
dle	dle	k7c2	dle
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
celkem	celkem	k6eAd1	celkem
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nápoj	nápoj	k1gInSc4	nápoj
nepřivedeme	přivést	k5eNaPmIp1nP	přivést
k	k	k7c3	k
varu	var	k1gInSc3	var
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
připravená	připravený	k2eAgFnSc1d1	připravená
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
zdaleka	zdaleka	k6eAd1	zdaleka
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
chuťových	chuťový	k2eAgFnPc2d1	chuťová
kvalit	kvalita	k1gFnPc2	kvalita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sebou	se	k3xPyFc7	se
už	už	k6eAd1	už
jako	jako	k8xC	jako
surovina	surovina	k1gFnSc1	surovina
nese	nést	k5eAaImIp3nS	nést
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vařící	vařící	k2eAgFnSc7d1	vařící
vodou	voda	k1gFnSc7	voda
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
pouze	pouze	k6eAd1	pouze
a	a	k8xC	a
výlučně	výlučně	k6eAd1	výlučně
<g/>
,	,	kIx,	,
pravý	pravý	k2eAgInSc4d1	pravý
český	český	k2eAgInSc4d1	český
turek	turek	k1gInSc4	turek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
velmi	velmi	k6eAd1	velmi
jemnému	jemný	k2eAgNnSc3d1	jemné
mletí	mletí	k1gNnSc3	mletí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
přípravy	příprava	k1gFnSc2	příprava
kávy	káva	k1gFnSc2	káva
nutné	nutný	k2eAgFnPc4d1	nutná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
sedlina	sedlina	k1gFnSc1	sedlina
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
džezvy	džezva	k1gFnSc2	džezva
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
kávu	káva	k1gFnSc4	káva
filtrovat	filtrovat	k5eAaImF	filtrovat
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
jí	on	k3xPp3gFnSc7	on
pohodlně	pohodlně	k6eAd1	pohodlně
servírovat	servírovat	k5eAaBmF	servírovat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
šálků	šálek	k1gInPc2	šálek
bez	bez	k7c2	bez
plovoucích	plovoucí	k2eAgNnPc2d1	plovoucí
zrnek	zrnko	k1gNnPc2	zrnko
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Nápoj	nápoj	k1gInSc1	nápoj
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
přípravě	příprava	k1gFnSc3	příprava
také	také	k6eAd1	také
vydatnější	vydatný	k2eAgFnSc1d2	vydatnější
a	a	k8xC	a
podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
jen	jen	k9	jen
se	s	k7c7	s
sklenicí	sklenice	k1gFnSc7	sklenice
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
způsoby	způsob	k1gInPc1	způsob
===	===	k?	===
</s>
</p>
<p>
<s>
Může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
filtrovanou	filtrovaný	k2eAgFnSc4d1	filtrovaná
kávu	káva	k1gFnSc4	káva
<g/>
,	,	kIx,	,
označovanou	označovaný	k2eAgFnSc4d1	označovaná
i	i	k8xC	i
jako	jako	k9	jako
drip	drip	k1gInSc1	drip
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
o	o	k7c4	o
přípravu	příprava	k1gFnSc4	příprava
kávy	káva	k1gFnSc2	káva
v	v	k7c6	v
kafetieru	kafetiero	k1gNnSc6	kafetiero
(	(	kIx(	(
<g/>
cafetier	cafetier	k1gMnSc1	cafetier
<g/>
,	,	kIx,	,
french	french	k1gMnSc1	french
press	press	k1gInSc1	press
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aeropress	Aeropress	k6eAd1	Aeropress
je	být	k5eAaImIp3nS	být
příprava	příprava	k1gFnSc1	příprava
kávy	káva	k1gFnSc2	káva
v	v	k7c6	v
přístroji	přístroj	k1gInSc6	přístroj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
káva	káva	k1gFnSc1	káva
stlačována	stlačovat	k5eAaImNgFnS	stlačovat
vzduchovým	vzduchový	k2eAgInSc7d1	vzduchový
válcem	válec	k1gInSc7	válec
přes	přes	k7c4	přes
filtrační	filtrační	k2eAgInSc4d1	filtrační
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
a	a	k8xC	a
přístroj	přístroj	k1gInSc4	přístroj
byla	být	k5eAaImAgFnS	být
vynalezena	vynaleznout	k5eAaPmNgFnS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Vacuum	Vacuum	k1gInSc1	Vacuum
pot	pot	k1gInSc1	pot
(	(	kIx(	(
<g/>
kávový	kávový	k2eAgInSc1d1	kávový
sifon	sifon	k1gInSc1	sifon
<g/>
)	)	kIx)	)
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přípravu	příprava	k1gFnSc4	příprava
filtrované	filtrovaný	k2eAgFnSc2d1	filtrovaná
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Káva	káva	k1gFnSc1	káva
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
přetlaku	přetlak	k1gInSc3	přetlak
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
vytlačena	vytlačit	k5eAaPmNgFnS	vytlačit
přes	přes	k7c4	přes
filtr	filtr	k1gInSc4	filtr
do	do	k7c2	do
skleněné	skleněný	k2eAgFnSc2d1	skleněná
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moka	moka	k1gNnSc1	moka
<g/>
,	,	kIx,	,
moka	moka	k1gNnSc1	moka
press	pressa	k1gFnPc2	pressa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
přípravy	příprava	k1gFnSc2	příprava
v	v	k7c6	v
kovové	kovový	k2eAgFnSc6d1	kovová
konvičce	konvička	k1gFnSc6	konvička
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
typy	typ	k1gInPc4	typ
konviček	konvička	k1gFnPc2	konvička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
kombinací	kombinace	k1gFnSc7	kombinace
kovu	kov	k1gInSc2	kov
a	a	k8xC	a
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
.	.	kIx.	.
</s>
<s>
Principem	princip	k1gInSc7	princip
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
až	až	k6eAd1	až
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
v	v	k7c4	v
páru	pára	k1gFnSc4	pára
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ze	z	k7c2	z
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
probublá	probublat	k5eAaPmIp3nS	probublat
přes	přes	k7c4	přes
kovové	kovový	k2eAgNnSc4d1	kovové
sítko	sítko	k1gNnSc4	sítko
naplněné	naplněný	k2eAgNnSc4d1	naplněné
kávou	káva	k1gFnSc7	káva
do	do	k7c2	do
vrchní	vrchní	k2eAgFnSc2d1	vrchní
nádobky	nádobka	k1gFnSc2	nádobka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
šálků	šálek	k1gInPc2	šálek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
i	i	k9	i
tradiční	tradiční	k2eAgFnSc2d1	tradiční
přípravy	příprava	k1gFnSc2	příprava
kávy	káva	k1gFnSc2	káva
často	často	k6eAd1	často
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
místní	místní	k2eAgFnSc7d1	místní
kulturou	kultura	k1gFnSc7	kultura
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
příprava	příprava	k1gFnSc1	příprava
kávy	káva	k1gFnSc2	káva
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
nebo	nebo	k8xC	nebo
etiopský	etiopský	k2eAgInSc4d1	etiopský
kávový	kávový	k2eAgInSc4d1	kávový
obřad	obřad	k1gInSc4	obřad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
káva	káva	k1gFnSc1	káva
sváží	svážet	k5eAaImIp3nS	svážet
s	s	k7c7	s
keramické	keramický	k2eAgFnSc6d1	keramická
konvičce	konvička	k1gFnSc6	konvička
tzv.	tzv.	kA	tzv.
jebena	jeben	k2eAgFnSc1d1	jeben
(	(	kIx(	(
<g/>
džebena	džeben	k2eAgFnSc1d1	džeben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Arabský	arabský	k2eAgInSc4d1	arabský
rituál	rituál	k1gInSc4	rituál
pití	pití	k1gNnSc2	pití
kávy	káva	k1gFnSc2	káva
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
především	především	k6eAd1	především
italský	italský	k2eAgMnSc1d1	italský
"	"	kIx"	"
<g/>
rituál	rituál	k1gInSc1	rituál
<g/>
"	"	kIx"	"
pití	pití	k1gNnSc1	pití
kávy	káva	k1gFnSc2	káva
<g/>
;	;	kIx,	;
starší	starý	k2eAgFnPc1d2	starší
a	a	k8xC	a
trochu	trochu	k6eAd1	trochu
složitější	složitý	k2eAgMnSc1d2	složitější
je	být	k5eAaImIp3nS	být
rituál	rituál	k1gInSc1	rituál
arabský	arabský	k2eAgInSc1d1	arabský
<g/>
.	.	kIx.	.
</s>
<s>
Nabídnutou	nabídnutý	k2eAgFnSc4d1	nabídnutá
kávu	káva	k1gFnSc4	káva
prakticky	prakticky	k6eAd1	prakticky
nelze	lze	k6eNd1	lze
odmítnout	odmítnout	k5eAaPmF	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Kávu	káva	k1gFnSc4	káva
obvykle	obvykle	k6eAd1	obvykle
roznáší	roznášet	k5eAaImIp3nS	roznášet
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
obdobou	obdoba	k1gFnSc7	obdoba
"	"	kIx"	"
<g/>
našeho	náš	k3xOp1gMnSc2	náš
<g/>
"	"	kIx"	"
číšníka	číšník	k1gMnSc2	číšník
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tácek	tácek	k1gInSc4	tácek
a	a	k8xC	a
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
šálky	šálka	k1gFnPc1	šálka
<g/>
.	.	kIx.	.
</s>
<s>
Nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
kávu	káva	k1gFnSc4	káva
<g/>
,	,	kIx,	,
tu	ten	k3xDgFnSc4	ten
host	host	k1gMnSc1	host
vypije	vypít	k5eAaPmIp3nS	vypít
a	a	k8xC	a
pokud	pokud	k8xS	pokud
chce	chtít	k5eAaImIp3nS	chtít
další	další	k2eAgNnSc1d1	další
<g/>
,	,	kIx,	,
položí	položit	k5eAaPmIp3nS	položit
šálek	šálek	k1gInSc1	šálek
na	na	k7c4	na
tácek	tácek	k1gInSc4	tácek
dnem	dnem	k7c2	dnem
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
standardní	standardní	k2eAgFnSc2d1	standardní
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
už	už	k6eAd1	už
další	další	k2eAgFnSc4d1	další
kávu	káva	k1gFnSc4	káva
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
položí	položit	k5eAaPmIp3nS	položit
šálek	šálek	k1gInSc4	šálek
dnem	dnem	k7c2	dnem
vzhůru	vzhůru	k6eAd1	vzhůru
nebo	nebo	k8xC	nebo
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
pro	pro	k7c4	pro
Evropany	Evropan	k1gMnPc4	Evropan
i	i	k8xC	i
Američany	Američan	k1gMnPc4	Američan
trochu	trochu	k6eAd1	trochu
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
stavějí	stavět	k5eAaImIp3nP	stavět
šálky	šálek	k1gInPc7	šálek
dnem	dnem	k7c2	dnem
dolů	dol	k1gInPc2	dol
a	a	k8xC	a
"	"	kIx"	"
<g/>
číšník	číšník	k1gMnSc1	číšník
<g/>
"	"	kIx"	"
jim	on	k3xPp3gMnPc3	on
s	s	k7c7	s
úsměvem	úsměv	k1gInSc7	úsměv
nalévá	nalévat	k5eAaImIp3nS	nalévat
další	další	k2eAgFnSc4d1	další
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
kávu	káva	k1gFnSc4	káva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zdraví	zdraví	k1gNnSc1	zdraví
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
vypracována	vypracován	k2eAgFnSc1d1	vypracována
na	na	k7c6	na
základě	základ	k1gInSc6	základ
201	[number]	k4	201
metanalýz	metanalýza	k1gFnPc2	metanalýza
souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
studie	studie	k1gFnSc1	studie
kompletující	kompletující	k2eAgFnSc1d1	kompletující
dostupné	dostupný	k2eAgNnSc4d1	dostupné
poznání	poznání	k1gNnSc4	poznání
(	(	kIx(	(
<g/>
do	do	k7c2	do
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
vlivu	vliv	k1gInSc2	vliv
konzumace	konzumace	k1gFnSc2	konzumace
kávy	káva	k1gFnSc2	káva
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
;	;	kIx,	;
podkladem	podklad	k1gInSc7	podklad
byly	být	k5eAaImAgFnP	být
většinou	většinou	k6eAd1	většinou
observační	observační	k2eAgFnPc1d1	observační
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dokládají	dokládat	k5eAaImIp3nP	dokládat
vztah	vztah	k1gInSc4	vztah
(	(	kIx(	(
<g/>
korelaci	korelace	k1gFnSc4	korelace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
prokázat	prokázat	k5eAaPmF	prokázat
příčinnou	příčinný	k2eAgFnSc4d1	příčinná
souvislost	souvislost	k1gFnSc4	souvislost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
korelace	korelace	k1gFnSc2	korelace
neimplikuje	implikovat	k5eNaImIp3nS	implikovat
kauzalitu	kauzalita	k1gFnSc4	kauzalita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
populační	populační	k2eAgFnSc6d1	populační
úrovni	úroveň	k1gFnSc6	úroveň
je	být	k5eAaImIp3nS	být
pití	pití	k1gNnSc1	pití
kávy	káva	k1gFnSc2	káva
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
celkovou	celkový	k2eAgFnSc7d1	celková
úmrtností	úmrtnost	k1gFnSc7	úmrtnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
úmrtností	úmrtnost	k1gFnSc7	úmrtnost
na	na	k7c4	na
kardiovaskulární	kardiovaskulární	k2eAgNnSc4d1	kardiovaskulární
onemocnění	onemocnění	k1gNnSc4	onemocnění
a	a	k8xC	a
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
výskytem	výskyt	k1gInSc7	výskyt
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
rakoviny	rakovina	k1gFnSc2	rakovina
prostaty	prostata	k1gFnSc2	prostata
<g/>
,	,	kIx,	,
dělohy	děloha	k1gFnSc2	děloha
<g/>
,	,	kIx,	,
rakoviny	rakovina	k1gFnSc2	rakovina
kůže	kůže	k1gFnSc2	kůže
(	(	kIx(	(
<g/>
melanomové	melanomový	k2eAgNnSc1d1	melanomový
i	i	k8xC	i
nemelanomové	melanomový	k2eNgNnSc1d1	melanomový
<g/>
)	)	kIx)	)
a	a	k8xC	a
rakoviny	rakovina	k1gFnSc2	rakovina
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
prokázána	prokázán	k2eAgFnSc1d1	prokázána
souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
konzumací	konzumace	k1gFnSc7	konzumace
kávy	káva	k1gFnSc2	káva
a	a	k8xC	a
lepšího	dobrý	k2eAgNnSc2d2	lepší
metabolického	metabolický	k2eAgNnSc2d1	metabolické
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nižšího	nízký	k2eAgInSc2d2	nižší
výskytu	výskyt	k1gInSc2	výskyt
diabetu	diabetes	k1gInSc2	diabetes
typu	typ	k1gInSc2	typ
2	[number]	k4	2
<g/>
,	,	kIx,	,
metabolického	metabolický	k2eAgInSc2d1	metabolický
syndromu	syndrom	k1gInSc2	syndrom
<g/>
,	,	kIx,	,
žlučových	žlučový	k2eAgInPc2d1	žlučový
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
dnou	dna	k1gFnSc7	dna
a	a	k8xC	a
ledvinovými	ledvinový	k2eAgInPc7d1	ledvinový
kameny	kámen	k1gInPc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
statisticky	statisticky	k6eAd1	statisticky
výrazně	výrazně	k6eAd1	výrazně
je	být	k5eAaImIp3nS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
příznivý	příznivý	k2eAgInSc1d1	příznivý
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
pitím	pití	k1gNnSc7	pití
kávy	káva	k1gFnSc2	káva
a	a	k8xC	a
jaterními	jaterní	k2eAgFnPc7d1	jaterní
potížemi	potíž	k1gFnPc7	potíž
(	(	kIx(	(
<g/>
snížená	snížený	k2eAgFnSc1d1	snížená
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
jaterní	jaterní	k2eAgFnSc2d1	jaterní
fibrózy	fibróza	k1gFnSc2	fibróza
<g/>
,	,	kIx,	,
jaterní	jaterní	k2eAgFnSc2d1	jaterní
cirhózy	cirhóza	k1gFnSc2	cirhóza
<g/>
,	,	kIx,	,
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
na	na	k7c4	na
jaterní	jaterní	k2eAgFnSc4d1	jaterní
cirhózu	cirhóza	k1gFnSc4	cirhóza
a	a	k8xC	a
chronického	chronický	k2eAgNnSc2d1	chronické
onemocnění	onemocnění	k1gNnSc2	onemocnění
jater	játra	k1gNnPc2	játra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
publikován	publikován	k2eAgMnSc1d1	publikován
statisticky	statisticky	k6eAd1	statisticky
významná	významný	k2eAgFnSc1d1	významná
souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
pitím	pití	k1gNnSc7	pití
kávy	káva	k1gFnSc2	káva
a	a	k8xC	a
nižším	nízký	k2eAgInSc7d2	nižší
výskytem	výskyt	k1gInSc7	výskyt
Parkinsonovy	Parkinsonův	k2eAgFnSc2d1	Parkinsonova
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
klinické	klinický	k2eAgFnSc2d1	klinická
deprese	deprese	k1gFnSc2	deprese
a	a	k8xC	a
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1	Alzheimerova
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
chybí	chybit	k5eAaPmIp3nS	chybit
statisticky	statisticky	k6eAd1	statisticky
přesvědčivé	přesvědčivý	k2eAgInPc4d1	přesvědčivý
důkazy	důkaz	k1gInPc4	důkaz
negativní	negativní	k2eAgInPc4d1	negativní
účinků	účinek	k1gInPc2	účinek
pití	pití	k1gNnSc4	pití
kávy	káva	k1gFnSc2	káva
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
k	k	k7c3	k
těhotenství	těhotenství	k1gNnSc3	těhotenství
(	(	kIx(	(
<g/>
pití	pití	k1gNnSc3	pití
kávy	káva	k1gFnSc2	káva
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
porodní	porodní	k2eAgFnSc7d1	porodní
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
předčasnými	předčasný	k2eAgInPc7d1	předčasný
porody	porod	k1gInPc7	porod
i	i	k8xC	i
spontánními	spontánní	k2eAgInPc7d1	spontánní
potraty	potrat	k1gInPc7	potrat
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
akutní	akutní	k2eAgFnSc7d1	akutní
dětskou	dětský	k2eAgFnSc7d1	dětská
leukémií	leukémie	k1gFnSc7	leukémie
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
negativní	negativní	k2eAgFnSc2d1	negativní
souvislosti	souvislost	k1gFnSc2	souvislost
vysoké	vysoký	k2eAgFnSc2d1	vysoká
konzumace	konzumace	k1gFnSc2	konzumace
kávy	káva	k1gFnSc2	káva
a	a	k8xC	a
zvýšeného	zvýšený	k2eAgNnSc2d1	zvýšené
rizika	riziko	k1gNnSc2	riziko
vzniku	vznik	k1gInSc2	vznik
zlomenin	zlomenina	k1gFnPc2	zlomenina
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
množství	množství	k1gNnSc1	množství
kávy	káva	k1gFnSc2	káva
maximalizující	maximalizující	k2eAgNnSc1d1	maximalizující
předpokládané	předpokládaný	k2eAgNnSc1d1	předpokládané
příznivé	příznivý	k2eAgNnSc1d1	příznivé
zdravotní	zdravotní	k2eAgInPc1d1	zdravotní
účinky	účinek	k1gInPc1	účinek
kávy	káva	k1gFnSc2	káva
byly	být	k5eAaImAgInP	být
doporučeny	doporučit	k5eAaPmNgInP	doporučit
3-4	[number]	k4	3-4
šálky	šálek	k1gInPc1	šálek
kávy	káva	k1gFnSc2	káva
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgNnSc1	takovýto
množství	množství	k1gNnSc1	množství
kávy	káva	k1gFnSc2	káva
navíc	navíc	k6eAd1	navíc
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
kofeinu	kofein	k1gInSc2	kofein
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
projevit	projevit	k5eAaPmF	projevit
jeho	jeho	k3xOp3gInSc1	jeho
močopudný	močopudný	k2eAgInSc1d1	močopudný
účinek	účinek	k1gInSc1	účinek
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
příjmu	příjem	k1gInSc2	příjem
tekutin	tekutina	k1gFnPc2	tekutina
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
množství	množství	k1gNnSc4	množství
kávy	káva	k1gFnSc2	káva
shodné	shodný	k2eAgInPc1d1	shodný
s	s	k7c7	s
pitím	pití	k1gNnSc7	pití
stejného	stejný	k2eAgNnSc2d1	stejné
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pití	pití	k1gNnSc4	pití
kávy	káva	k1gFnSc2	káva
a	a	k8xC	a
rakovina	rakovina	k1gFnSc1	rakovina
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
rakoviny	rakovina	k1gFnSc2	rakovina
(	(	kIx(	(
<g/>
IARC	IARC	kA	IARC
<g/>
)	)	kIx)	)
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pití	pití	k1gNnSc1	pití
kávy	káva	k1gFnSc2	káva
nemůže	moct	k5eNaImIp3nS	moct
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
jako	jako	k9	jako
karcinogen	karcinogen	k1gInSc4	karcinogen
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
pití	pití	k1gNnSc1	pití
horkých	horký	k2eAgInPc2d1	horký
nápojů	nápoj	k1gInPc2	nápoj
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
kávy	káva	k1gFnSc2	káva
<g/>
)	)	kIx)	)
tato	tento	k3xDgFnSc1	tento
agentura	agentura	k1gFnSc1	agentura
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
jako	jako	k9	jako
pravděpodobný	pravděpodobný	k2eAgInSc1d1	pravděpodobný
karcinogen	karcinogen	k1gInSc1	karcinogen
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
2	[number]	k4	2
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čerstvost	čerstvost	k1gFnSc1	čerstvost
kávy	káva	k1gFnSc2	káva
==	==	k?	==
</s>
</p>
<p>
<s>
Káva	káva	k1gFnSc1	káva
vydrží	vydržet	k5eAaPmIp3nS	vydržet
opravdu	opravdu	k6eAd1	opravdu
čerstvá	čerstvý	k2eAgFnSc1d1	čerstvá
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
max	max	kA	max
<g/>
.	.	kIx.	.
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Pražená	pražený	k2eAgFnSc1d1	pražená
káva	káva	k1gFnSc1	káva
je	být	k5eAaImIp3nS	být
tepelně	tepelně	k6eAd1	tepelně
opracovaný	opracovaný	k2eAgInSc1d1	opracovaný
produkt	produkt	k1gInSc1	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
těkavé	těkavý	k2eAgFnPc4d1	těkavá
látky	látka	k1gFnPc4	látka
a	a	k8xC	a
oleje	olej	k1gInPc4	olej
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
i	i	k8xC	i
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
atmosférách	atmosféra	k1gFnPc6	atmosféra
žluknou	žluknout	k5eAaBmIp3nP	žluknout
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
prodávaných	prodávaný	k2eAgFnPc2d1	prodávaná
káv	káva	k1gFnPc2	káva
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
žluklé	žluklý	k2eAgFnPc1d1	žluklá
<g/>
,	,	kIx,	,
vyčpělé	vyčpělý	k2eAgFnPc1d1	vyčpělá
kávy	káva	k1gFnPc1	káva
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
obyčejné	obyčejný	k2eAgFnPc4d1	obyčejná
nejlevnější	levný	k2eAgFnPc4d3	nejlevnější
kávy	káva	k1gFnPc4	káva
typu	typ	k1gInSc2	typ
robusta	robusta	k1gFnSc1	robusta
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
kávy	káva	k1gFnSc2	káva
renomovaných	renomovaný	k2eAgFnPc2d1	renomovaná
značek	značka	k1gFnPc2	značka
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
lidi	člověk	k1gMnPc4	člověk
nenechají	nechat	k5eNaPmIp3nP	nechat
dopustit	dopustit	k5eAaPmF	dopustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mletí	mletí	k1gNnSc3	mletí
zrnkové	zrnkový	k2eAgFnSc2d1	zrnková
kávy	káva	k1gFnSc2	káva
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
kávy	káva	k1gFnSc2	káva
je	být	k5eAaImIp3nS	být
nejlépe	dobře	k6eAd3	dobře
umlít	umlít	k5eAaPmF	umlít
pouze	pouze	k6eAd1	pouze
tolik	tolik	k4yIc4	tolik
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
–	–	k?	–
mletá	mletý	k2eAgFnSc1d1	mletá
káva	káva	k1gFnSc1	káva
si	se	k3xPyFc3	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
dlouho	dlouho	k6eAd1	dlouho
držet	držet	k5eAaImF	držet
své	svůj	k3xOyFgNnSc4	svůj
aroma	aroma	k1gNnSc4	aroma
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
-	-	kIx~	-
asi	asi	k9	asi
do	do	k7c2	do
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
po	po	k7c6	po
umletí	umletí	k1gNnSc6	umletí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Orientační	orientační	k2eAgFnSc1d1	orientační
hrubost	hrubost	k1gFnSc1	hrubost
mletí	mletí	k1gNnSc2	mletí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
velmi	velmi	k6eAd1	velmi
jemné	jemný	k2eAgNnSc1d1	jemné
(	(	kIx(	(
<g/>
na	na	k7c4	na
prach	prach	k1gInSc4	prach
<g/>
)	)	kIx)	)
–	–	k?	–
turecká	turecký	k2eAgFnSc1d1	turecká
káva	káva	k1gFnSc1	káva
(	(	kIx(	(
<g/>
Džezva	džezva	k1gFnSc1	džezva
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jemné	jemný	k2eAgFnPc4d1	jemná
–	–	k?	–
Espresso	Espressa	k1gFnSc5	Espressa
</s>
</p>
<p>
<s>
střední	střední	k2eAgFnSc1d1	střední
-	-	kIx~	-
Filtrovaná	filtrovaný	k2eAgFnSc1d1	filtrovaná
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
překapávaná	překapávaný	k2eAgFnSc1d1	překapávaná
káva	káva	k1gFnSc1	káva
(	(	kIx(	(
<g/>
French	French	k1gInSc1	French
press	press	k1gInSc1	press
<g/>
,	,	kIx,	,
aeropress	aeropress	k1gInSc1	aeropress
<g/>
,	,	kIx,	,
chemex	chemex	k1gInSc1	chemex
a	a	k8xC	a
drip	drip	k1gInSc1	drip
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hrubé	hrubý	k2eAgNnSc4d1	hrubé
mletí	mletí	k1gNnSc4	mletí
–	–	k?	–
Mokka	mokka	k1gNnSc2	mokka
</s>
</p>
<p>
<s>
==	==	k?	==
Plantážní	plantážní	k2eAgFnSc1d1	plantážní
káva	káva	k1gFnSc1	káva
==	==	k?	==
</s>
</p>
<p>
<s>
Tímto	tento	k3xDgNnSc7	tento
pojmenováním	pojmenování	k1gNnSc7	pojmenování
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
rozumí	rozumět	k5eAaImIp3nS	rozumět
jakostní	jakostní	k2eAgFnSc1d1	jakostní
jednodruhová	jednodruhový	k2eAgFnSc1d1	jednodruhová
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
nějakém	nějaký	k3yIgNnSc6	nějaký
konkrétním	konkrétní	k2eAgNnSc6d1	konkrétní
zeměpisném	zeměpisný	k2eAgNnSc6d1	zeměpisné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
sbírána	sbírán	k2eAgFnSc1d1	sbírána
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
se	se	k3xPyFc4	se
také	také	k9	také
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
typické	typický	k2eAgFnPc4d1	typická
chuťové	chuťový	k2eAgFnPc4d1	chuťová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
kávy	káva	k1gFnSc2	káva
==	==	k?	==
</s>
</p>
<p>
<s>
Kávu	káva	k1gFnSc4	káva
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
a	a	k8xC	a
okolností	okolnost	k1gFnSc7	okolnost
zpracování	zpracování	k1gNnSc1	zpracování
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zrnková	zrnkový	k2eAgFnSc1d1	zrnková
káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
Mletá	mletý	k2eAgFnSc1d1	mletá
káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
Porcovaná	porcovaný	k2eAgFnSc1d1	porcovaná
káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
Fair	fair	k6eAd1	fair
trade	trade	k6eAd1	trade
káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
Instantní	instantní	k2eAgFnSc1d1	instantní
káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
Bio	Bio	k?	Bio
káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
BezkofeinováPříprava	BezkofeinováPříprava	k1gFnSc1	BezkofeinováPříprava
a	a	k8xC	a
pití	pití	k1gNnSc1	pití
kávy	káva	k1gFnSc2	káva
v	v	k7c6	v
bojových	bojový	k2eAgFnPc6d1	bojová
polních	polní	k2eAgFnPc6d1	polní
podmínkách	podmínka	k1gFnPc6	podmínka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
AUGUSTÍN	AUGUSTÍN	kA	AUGUSTÍN
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kávy	káva	k1gFnSc2	káva
o	o	k7c6	o
kávě	káva	k1gFnSc6	káva
a	a	k8xC	a
kávovinách	kávovina	k1gFnPc6	kávovina
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Pri	Pri	k1gFnSc1	Pri
káve	kávat	k5eAaPmIp3nS	kávat
o	o	k7c6	o
káve	káve	k1gFnPc6	káve
a	a	k8xC	a
kávovinách	kávovina	k1gFnPc6	kávovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Katarína	Katarín	k1gMnSc2	Katarín
Kašpárková	Kašpárková	k1gFnSc1	Kašpárková
Koišová	Koišová	k1gFnSc1	Koišová
<g/>
.	.	kIx.	.
aktualizované	aktualizovaný	k2eAgFnPc4d1	aktualizovaná
a	a	k8xC	a
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
360	[number]	k4	360
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7462	[number]	k4	7462
<g/>
-	-	kIx~	-
<g/>
850	[number]	k4	850
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kavárna	kavárna	k1gFnSc1	kavárna
</s>
</p>
<p>
<s>
Barista	Barista	k1gMnSc1	Barista
</s>
</p>
<p>
<s>
Mistr	mistr	k1gMnSc1	mistr
kávy	káva	k1gFnSc2	káva
</s>
</p>
<p>
<s>
Latté	Latta	k1gMnPc1	Latta
art	art	k?	art
</s>
</p>
<p>
<s>
Káva	káva	k1gFnSc1	káva
bez	bez	k7c2	bez
kofeinu	kofein	k1gInSc2	kofein
</s>
</p>
<p>
<s>
Instantní	instantní	k2eAgFnSc1d1	instantní
káva	káva	k1gFnSc1	káva
(	(	kIx(	(
<g/>
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
káva	káva	k1gFnSc1	káva
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Easy	Easa	k1gFnPc1	Easa
Serving	Serving	k1gInSc1	Serving
Espresso	Espressa	k1gFnSc5	Espressa
</s>
</p>
<p>
<s>
Kávové	kávový	k2eAgFnPc4d1	kávová
odrůdy	odrůda	k1gFnPc4	odrůda
</s>
</p>
<p>
<s>
Julius	Julius	k1gMnSc1	Julius
MeinlDruhy	MeinlDruha	k1gFnSc2	MeinlDruha
přípravy	příprava	k1gFnSc2	příprava
kávyCappuccino	kávyCappuccino	k1gNnSc1	kávyCappuccino
</s>
</p>
<p>
<s>
Espresso	Espressa	k1gFnSc5	Espressa
(	(	kIx(	(
<g/>
preso	presa	k1gFnSc5	presa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moccacino	Moccacino	k1gNnSc1	Moccacino
</s>
</p>
<p>
<s>
Moka	moka	k1gFnSc1	moka
</s>
</p>
<p>
<s>
Alžírská	alžírský	k2eAgFnSc1d1	alžírská
káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
Holandská	holandský	k2eAgFnSc1d1	holandská
káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
Irská	irský	k2eAgFnSc1d1	irská
káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
Karibská	karibský	k2eAgFnSc1d1	karibská
káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
Café	café	k1gNnSc1	café
au	au	k0	au
lait	lait	k5eAaPmF	lait
</s>
</p>
<p>
<s>
Caffè	Caffè	k?	Caffè
crema	crema	k1gFnSc1	crema
</s>
</p>
<p>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
melangeNádoby	melangeNádoba	k1gFnPc1	melangeNádoba
a	a	k8xC	a
zařízení	zařízení	k1gNnSc1	zařízení
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
kávyDžezva	kávyDžezva	k6eAd1	kávyDžezva
</s>
</p>
<p>
<s>
Kávovar	kávovar	k1gInSc1	kávovar
</s>
</p>
<p>
<s>
Presovač	Presovač	k1gMnSc1	Presovač
</s>
</p>
<p>
<s>
Moka	moka	k1gFnSc1	moka
konvička	konvička	k1gFnSc1	konvička
(	(	kIx(	(
<g/>
bialettka	bialettka	k1gFnSc1	bialettka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
French	French	k1gMnSc1	French
press	press	k6eAd1	press
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
káva	káva	k1gFnSc1	káva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Káva	káva	k1gFnSc1	káva
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
káva	káva	k1gFnSc1	káva
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Káva	káva	k1gFnSc1	káva
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
</s>
</p>
<p>
<s>
Víte	vědět	k5eAaImIp2nP	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
udělat	udělat	k5eAaPmF	udělat
espreso	espreso	k1gNnSc4	espreso
a	a	k8xC	a
kapučíno	kapučína	k1gFnSc5	kapučína
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
ekonomika	ekonomika	k1gFnSc1	ekonomika
<g/>
.	.	kIx.	.
<g/>
idnes	idnes	k1gInSc1	idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Káva	káva	k1gFnSc1	káva
–	–	k?	–
nápoj	nápoj	k1gInSc4	nápoj
plný	plný	k2eAgInSc4d1	plný
otáznikov	otáznikov	k1gInSc4	otáznikov
-	-	kIx~	-
primar	primar	k1gInSc4	primar
<g/>
.	.	kIx.	.
<g/>
sme	sme	k?	sme
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
co	co	k9	co
dělá	dělat	k5eAaImIp3nS	dělat
<g/>
:	:	kIx,	:
Káva	káva	k1gFnSc1	káva
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
na	na	k7c4	na
lepší	dobrý	k2eAgFnSc4d2	lepší
kávu	káva	k1gFnSc4	káva
(	(	kIx(	(
<g/>
hobby	hobby	k1gNnSc4	hobby
<g/>
.	.	kIx.	.
<g/>
idnes	idnes	k1gInSc1	idnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
</s>
</p>
