<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
alkaloid	alkaloid	k1gInSc4	alkaloid
kofein	kofein	k1gInSc1	kofein
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
povzbuzuje	povzbuzovat	k5eAaImIp3nS	povzbuzovat
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
