<s>
Pouta	pouto	k1gNnPc1	pouto
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
producenta	producent	k1gMnSc2	producent
Vratislava	Vratislav	k1gMnSc2	Vratislav
Šlajera	Šlajer	k1gMnSc2	Šlajer
<g/>
,	,	kIx,	,
scenáristy	scenárista	k1gMnSc2	scenárista
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Štindla	Štindla	k1gMnSc2	Štindla
a	a	k8xC	a
režiséra	režisér	k1gMnSc2	režisér
Radima	Radim	k1gMnSc2	Radim
Špačka	Špaček	k1gMnSc2	Špaček
oceněný	oceněný	k2eAgInSc4d1	oceněný
pěti	pět	k4xCc7	pět
Českými	český	k2eAgInPc7d1	český
lvy	lev	k1gInPc7	lev
a	a	k8xC	a
pěti	pět	k4xCc7	pět
Cenami	cena	k1gFnPc7	cena
české	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
bezejmenném	bezejmenný	k2eAgNnSc6d1	bezejmenné
českém	český	k2eAgNnSc6d1	české
městě	město	k1gNnSc6	město
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
plánované	plánovaný	k2eAgNnSc1d1	plánované
vročení	vročení	k1gNnSc1	vročení
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
pomocí	pomocí	k7c2	pomocí
titulku	titulek	k1gInSc2	titulek
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
filmu	film	k1gInSc2	film
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
Antonín	Antonín	k1gMnSc1	Antonín
Rusnák	Rusnák	k1gMnSc1	Rusnák
(	(	kIx(	(
<g/>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Malý	Malý	k1gMnSc1	Malý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příslušník	příslušník	k1gMnSc1	příslušník
Státní	státní	k2eAgFnSc2d1	státní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
film	film	k1gInSc1	film
podobný	podobný	k2eAgInSc1d1	podobný
německému	německý	k2eAgInSc3d1	německý
oskarovému	oskarový	k2eAgInSc3d1	oskarový
filmu	film	k1gInSc3	film
Životy	život	k1gInPc1	život
těch	ten	k3xDgMnPc2	ten
druhých	druhý	k4xOgFnPc2	druhý
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgNnSc3	který
bývá	bývat	k5eAaImIp3nS	bývat
přirovnáván	přirovnáván	k2eAgInSc1d1	přirovnáván
<g/>
,	,	kIx,	,
vyznění	vyznění	k1gNnSc1	vyznění
Pout	pout	k1gInSc1	pout
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
opačné	opačný	k2eAgNnSc1d1	opačné
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
příslušník	příslušník	k1gMnSc1	příslušník
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
má	mít	k5eAaImIp3nS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
nezacílený	zacílený	k2eNgInSc4d1	zacílený
vztek	vztek	k1gInSc4	vztek
a	a	k8xC	a
všechno	všechen	k3xTgNnSc1	všechen
kolem	kolem	k6eAd1	kolem
-	-	kIx~	-
práce	práce	k1gFnSc1	práce
i	i	k9	i
rodinný	rodinný	k2eAgInSc4d1	rodinný
život	život	k1gInSc4	život
-	-	kIx~	-
ho	on	k3xPp3gNnSc4	on
ubíjí	ubíjet	k5eAaImIp3nS	ubíjet
a	a	k8xC	a
nudí	nudit	k5eAaImIp3nS	nudit
<g/>
.	.	kIx.	.
</s>
<s>
Upne	upnout	k5eAaPmIp3nS	upnout
se	se	k3xPyFc4	se
na	na	k7c4	na
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc2	on
nedosažitelnou	dosažitelný	k2eNgFnSc4d1	nedosažitelná
dívku	dívka	k1gFnSc4	dívka
Kláru	Klára	k1gFnSc4	Klára
<g/>
,	,	kIx,	,
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
chce	chtít	k5eAaImIp3nS	chtít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chce	chtít	k5eAaImIp3nS	chtít
to	ten	k3xDgNnSc1	ten
strašně	strašně	k6eAd1	strašně
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
láska	láska	k1gFnSc1	láska
ani	ani	k8xC	ani
jiný	jiný	k2eAgInSc1d1	jiný
druh	druh	k1gInSc1	druh
čisté	čistý	k2eAgFnSc2d1	čistá
vášně	vášeň	k1gFnSc2	vášeň
–	–	k?	–
pouze	pouze	k6eAd1	pouze
spalující	spalující	k2eAgFnSc1d1	spalující
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
iluzi	iluze	k1gFnSc6	iluze
útěku	útěk	k1gInSc2	útěk
z	z	k7c2	z
klece	klec	k1gFnSc2	klec
nudného	nudný	k2eAgInSc2d1	nudný
života	život	k1gInSc2	život
beze	beze	k7c2	beze
smyslu	smysl	k1gInSc2	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Antonínova	Antonínův	k2eAgFnSc1d1	Antonínova
nesmyslná	smyslný	k2eNgFnSc1d1	nesmyslná
snaha	snaha	k1gFnSc1	snaha
získat	získat	k5eAaPmF	získat
Kláru	Klára	k1gFnSc4	Klára
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
ho	on	k3xPp3gMnSc4	on
obrací	obracet	k5eAaImIp3nS	obracet
i	i	k9	i
proti	proti	k7c3	proti
vlastním	vlastní	k2eAgMnPc3d1	vlastní
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ale	ale	k9	ale
Antoním	Antoň	k1gFnPc3	Antoň
porušuje	porušovat	k5eAaImIp3nS	porušovat
pravidla	pravidlo	k1gNnPc4	pravidlo
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
slouží	sloužit	k5eAaImIp3nS	sloužit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
gesto	gesto	k1gNnSc1	gesto
občanské	občanský	k2eAgNnSc1d1	občanské
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
politické	politický	k2eAgFnSc2d1	politická
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vzpoura	vzpoura	k1gFnSc1	vzpoura
čistě	čistě	k6eAd1	čistě
osobní	osobní	k2eAgFnSc1d1	osobní
a	a	k8xC	a
zběsilá	zběsilý	k2eAgFnSc1d1	zběsilá
<g/>
.	.	kIx.	.
</s>
<s>
Antonínova	Antonínův	k2eAgFnSc1d1	Antonínova
zkáza	zkáza	k1gFnSc1	zkáza
v	v	k7c6	v
sobě	se	k3xPyFc3	se
ale	ale	k8xC	ale
možná	možný	k2eAgFnSc1d1	možná
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
jakousi	jakýsi	k3yIgFnSc4	jakýsi
prchavou	prchavý	k2eAgFnSc4d1	prchavá
naději	naděje	k1gFnSc4	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Pouta	pouto	k1gNnPc1	pouto
jsou	být	k5eAaImIp3nP	být
thriller	thriller	k1gInSc4	thriller
s	s	k7c7	s
temným	temný	k2eAgInSc7d1	temný
příběhem	příběh	k1gInSc7	příběh
a	a	k8xC	a
nepředvídatelně	předvídatelně	k6eNd1	předvídatelně
jednajícím	jednající	k2eAgMnSc7d1	jednající
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
,	,	kIx,	,
prostoupený	prostoupený	k2eAgInSc1d1	prostoupený
pocitem	pocit	k1gInSc7	pocit
ohrožení	ohrožení	k1gNnSc1	ohrožení
<g/>
,	,	kIx,	,
strhující	strhující	k2eAgInSc4d1	strhující
a	a	k8xC	a
napínavý	napínavý	k2eAgInSc4d1	napínavý
<g/>
.	.	kIx.	.
</s>
<s>
Hybatelem	hybatel	k1gMnSc7	hybatel
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
nevyzpytatelná	vyzpytatelný	k2eNgFnSc1d1	nevyzpytatelná
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
,	,	kIx,	,
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc1	okolí
<g/>
,	,	kIx,	,
s	s	k7c7	s
mocí	moc	k1gFnSc7	moc
a	a	k8xC	a
možnostmi	možnost	k1gFnPc7	možnost
příslušníka	příslušník	k1gMnSc2	příslušník
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
v	v	k7c6	v
totalitním	totalitní	k2eAgInSc6d1	totalitní
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
svým	svůj	k3xOyFgInSc7	svůj
cílem	cíl	k1gInSc7	cíl
jde	jít	k5eAaImIp3nS	jít
sebezničujícím	sebezničující	k2eAgInSc7d1	sebezničující
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
postavy	postava	k1gFnPc1	postava
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
diváka	divák	k1gMnSc4	divák
<g/>
,	,	kIx,	,
udržuje	udržovat	k5eAaImIp3nS	udržovat
ve	v	k7c6	v
stálém	stálý	k2eAgNnSc6d1	stálé
napětí	napětí	k1gNnSc6	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
filmu	film	k1gInSc2	film
vznikal	vznikat	k5eAaImAgInS	vznikat
během	během	k7c2	během
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
verze	verze	k1gFnSc2	verze
začal	začít	k5eAaPmAgMnS	začít
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
publicista	publicista	k1gMnSc1	publicista
Ondřej	Ondřej	k1gMnSc1	Ondřej
Štindl	Štindl	k1gMnSc1	Štindl
psát	psát	k5eAaImF	psát
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
přes	přes	k7c4	přes
deset	deset	k4xCc4	deset
verzí	verze	k1gFnPc2	verze
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
fázích	fáze	k1gFnPc6	fáze
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
dramaturgem	dramaturg	k1gMnSc7	dramaturg
Jiřím	Jiří	k1gMnSc7	Jiří
Soukupem	Soukup	k1gMnSc7	Soukup
<g/>
,	,	kIx,	,
detaily	detail	k1gInPc1	detail
také	také	k9	také
diskutoval	diskutovat	k5eAaImAgMnS	diskutovat
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Radimem	Radim	k1gMnSc7	Radim
Špačkem	Špaček	k1gMnSc7	Špaček
a	a	k8xC	a
kameramanem	kameraman	k1gMnSc7	kameraman
Jaromírem	Jaromír	k1gMnSc7	Jaromír
Kačerem	kačer	k1gMnSc7	kačer
<g/>
.	.	kIx.	.
</s>
<s>
Štindl	Štindnout	k5eAaPmAgMnS	Štindnout
měl	mít	k5eAaImAgMnS	mít
během	během	k7c2	během
natáčení	natáčení	k1gNnSc2	natáčení
filmu	film	k1gInSc2	film
poměrně	poměrně	k6eAd1	poměrně
silnou	silný	k2eAgFnSc4d1	silná
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
scenáristů	scenárista	k1gMnPc2	scenárista
výjimečné	výjimečný	k2eAgInPc1d1	výjimečný
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
získal	získat	k5eAaPmAgInS	získat
celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Český	český	k2eAgMnSc1d1	český
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
5	[number]	k4	5
proměnil	proměnit	k5eAaPmAgMnS	proměnit
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
film	film	k1gInSc4	film
-	-	kIx~	-
Vratislav	Vratislav	k1gMnSc1	Vratislav
Šlajer	Šlajer	k?	Šlajer
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
režie	režie	k1gFnSc1	režie
-	-	kIx~	-
Radim	Radim	k1gMnSc1	Radim
Špaček	Špaček	k1gMnSc1	Špaček
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
scénář	scénář	k1gInSc1	scénář
-	-	kIx~	-
Ondřej	Ondřej	k1gMnSc1	Ondřej
Štindl	Štindl	k1gMnSc1	Štindl
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
kamera	kamera	k1gFnSc1	kamera
-	-	kIx~	-
Jaromír	Jaromír	k1gMnSc1	Jaromír
Kačer	kačer	k1gMnSc1	kačer
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
-	-	kIx~	-
Ondřej	Ondřej	k1gMnSc1	Ondřej
Malý	Malý	k1gMnSc1	Malý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5	[number]	k4	5
z	z	k7c2	z
celkových	celkový	k2eAgFnPc2d1	celková
7	[number]	k4	7
nominací	nominace	k1gFnPc2	nominace
proměnil	proměnit	k5eAaPmAgMnS	proměnit
také	také	k9	také
u	u	k7c2	u
konkurenčních	konkurenční	k2eAgFnPc2d1	konkurenční
Cen	cena	k1gFnPc2	cena
české	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
kritiky	kritika	k1gFnSc2	kritika
(	(	kIx(	(
<g/>
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
<g/>
,	,	kIx,	,
mužský	mužský	k2eAgInSc1d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
a	a	k8xC	a
Cena	cena	k1gFnSc1	cena
RWE	RWE	kA	RWE
pro	pro	k7c4	pro
objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
pro	pro	k7c4	pro
Ondřeje	Ondřej	k1gMnSc4	Ondřej
Malého	Malý	k1gMnSc4	Malý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Českého	český	k2eAgInSc2d1	český
lva	lev	k1gInSc2	lev
získal	získat	k5eAaPmAgMnS	získat
také	také	k6eAd1	také
Cenu	cena	k1gFnSc4	cena
filmových	filmový	k2eAgMnPc2d1	filmový
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
teoretiků	teoretik	k1gMnPc2	teoretik
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
hraný	hraný	k2eAgInSc4d1	hraný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
byl	být	k5eAaImAgInS	být
filmem	film	k1gInSc7	film
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
nominací	nominace	k1gFnPc2	nominace
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
získal	získat	k5eAaPmAgMnS	získat
čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
herce	herec	k1gMnSc4	herec
(	(	kIx(	(
<g/>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Malý	Malý	k1gMnSc1	Malý
<g/>
)	)	kIx)	)
na	na	k7c6	na
plzeňském	plzeňský	k2eAgInSc6d1	plzeňský
festivalu	festival	k1gInSc6	festival
Finále	finále	k1gNnSc4	finále
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
FITES	FITES	kA	FITES
Trilobit	trilobit	k1gMnSc1	trilobit
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Fuka	Fuka	k1gMnSc1	Fuka
film	film	k1gInSc4	film
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
za	za	k7c4	za
několik	několik	k4yIc4	několik
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
a	a	k8xC	a
možná	možná	k9	možná
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
český	český	k2eAgInSc4d1	český
porevoluční	porevoluční	k2eAgInSc4d1	porevoluční
film	film	k1gInSc4	film
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
Kinobox	Kinobox	k1gInSc1	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Kamil	Kamil	k1gMnSc1	Kamil
Fila	Fila	k1gMnSc1	Fila
<g/>
,	,	kIx,	,
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
Kamila	Kamila	k1gFnSc1	Kamila
Boháčková	Boháčková	k1gFnSc1	Boháčková
<g/>
,	,	kIx,	,
A2	A2	k1gFnSc1	A2
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2010	[number]	k4	2010
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
Jan	Jan	k1gMnSc1	Jan
Gregor	Gregor	k1gMnSc1	Gregor
<g/>
,	,	kIx,	,
Respekt	respekt	k1gInSc1	respekt
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
Dostupné	dostupný	k2eAgFnSc2d1	dostupná
online	onlinout	k5eAaPmIp3nS	onlinout
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
placené	placený	k2eAgFnSc6d1	placená
části	část	k1gFnSc6	část
webu	web	k1gInSc2	web
Vít	Vít	k1gMnSc1	Vít
Schmarc	Schmarc	k1gFnSc1	Schmarc
<g/>
,	,	kIx,	,
MovieZone	MovieZon	k1gMnSc5	MovieZon
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
František	František	k1gMnSc1	František
Fuka	Fuka	k1gMnSc1	Fuka
<g/>
,	,	kIx,	,
FFFilm	FFFilm	k1gMnSc1	FFFilm
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
soutěži	soutěž	k1gFnSc6	soutěž
na	na	k7c6	na
Varšavském	varšavský	k2eAgInSc6d1	varšavský
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
2010	[number]	k4	2010
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
filmových	filmový	k2eAgInPc6d1	filmový
festivalech	festival	k1gInPc6	festival
v	v	k7c6	v
Pusanu	Pusan	k1gInSc6	Pusan
a	a	k8xC	a
Chotěbuzi	Chotěbuz	k1gFnSc6	Chotěbuz
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
také	také	k9	také
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c6	na
Evropském	evropský	k2eAgInSc6d1	evropský
filmovém	filmový	k2eAgInSc6d1	filmový
trhu	trh	k1gInSc6	trh
při	při	k7c6	při
Berlinale	Berlinale	k1gFnSc6	Berlinale
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
však	však	k9	však
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
projekce	projekce	k1gFnPc1	projekce
na	na	k7c4	na
území	území	k1gNnSc4	území
Ruska	Rusko	k1gNnSc2	Rusko
je	být	k5eAaImIp3nS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Pouta	pouto	k1gNnPc1	pouto
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Pouta	pouto	k1gNnSc2	pouto
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
