<p>
<s>
Svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
(	(	kIx(	(
<g/>
též	též	k9	též
suverenita	suverenita	k1gFnSc1	suverenita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
právo	právo	k1gNnSc4	právo
vykonávat	vykonávat	k5eAaImF	vykonávat
neomezeně	omezeně	k6eNd1	omezeně
moc	moc	k6eAd1	moc
na	na	k7c6	na
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
náleží	náležet	k5eAaImIp3nS	náležet
každému	každý	k3xTgInSc3	každý
nezávislému	závislý	k2eNgInSc3d1	nezávislý
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
reprezentativních	reprezentativní	k2eAgFnPc6d1	reprezentativní
demokraciích	demokracie	k1gFnPc6	demokracie
propůjčují	propůjčovat	k5eAaImIp3nP	propůjčovat
občané	občan	k1gMnPc1	občan
toto	tento	k3xDgNnSc4	tento
své	svůj	k3xOyFgNnSc4	svůj
právo	právo	k1gNnSc4	právo
k	k	k7c3	k
vykonáváním	vykonávání	k1gNnSc7	vykonávání
svým	svůj	k3xOyFgMnPc3	svůj
reprezentantům	reprezentant	k1gMnPc3	reprezentant
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
jejich	jejich	k3xOp3gInSc2	jejich
mandátu	mandát	k1gInSc2	mandát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
suverenity	suverenita	k1gFnSc2	suverenita
==	==	k?	==
</s>
</p>
<p>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
suverenity	suverenita	k1gFnSc2	suverenita
byla	být	k5eAaImAgFnS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
již	již	k6eAd1	již
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ve	v	k7c4	v
Francii	Francie	k1gFnSc4	Francie
větou	větý	k2eAgFnSc4d1	větá
"	"	kIx"	"
<g/>
král	král	k1gMnSc1	král
je	být	k5eAaImIp3nS	být
vládcem	vládce	k1gMnSc7	vládce
svého	své	k1gNnSc2	své
vlastního	vlastní	k2eAgNnSc2d1	vlastní
království	království	k1gNnSc2	království
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
stojí	stát	k5eAaImIp3nP	stát
nad	nad	k7c7	nad
všemi	všecek	k3xTgMnPc7	všecek
pány	pan	k1gMnPc7	pan
a	a	k8xC	a
šlechtici	šlechtic	k1gMnPc7	šlechtic
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
začala	začít	k5eAaPmAgFnS	začít
hrát	hrát	k5eAaImF	hrát
roli	role	k1gFnSc4	role
v	v	k7c6	v
debatách	debata	k1gFnPc6	debata
ohledně	ohledně	k7c2	ohledně
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
předstupněm	předstupeň	k1gInSc7	předstupeň
rozkvětu	rozkvět	k1gInSc2	rozkvět
absolutistické	absolutistický	k2eAgFnSc2d1	absolutistická
monarchie	monarchie	k1gFnSc2	monarchie
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
etapou	etapa	k1gFnSc7	etapa
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
moderního	moderní	k2eAgInSc2d1	moderní
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyvstala	vyvstat	k5eAaPmAgFnS	vyvstat
nutnost	nutnost	k1gFnSc4	nutnost
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
moc	moc	k6eAd1	moc
v	v	k7c6	v
komunitě	komunita	k1gFnSc6	komunita
kvůli	kvůli	k7c3	kvůli
náboženským	náboženský	k2eAgFnPc3d1	náboženská
rozepřím	rozepře	k1gFnPc3	rozepře
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
raně	raně	k6eAd1	raně
moderní	moderní	k2eAgFnSc6d1	moderní
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
právě	právě	k9	právě
suverenita	suverenita	k1gFnSc1	suverenita
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
teritoriální	teritoriální	k2eAgFnSc7d1	teritoriální
organizací	organizace	k1gFnSc7	organizace
subjektů	subjekt	k1gInPc2	subjekt
podřízených	podřízený	k1gMnPc2	podřízený
panovníkovi	panovníkův	k2eAgMnPc1d1	panovníkův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
významným	významný	k2eAgMnSc7d1	významný
teoretikem	teoretik	k1gMnSc7	teoretik
suverenity	suverenita	k1gFnSc2	suverenita
byl	být	k5eAaImAgMnS	být
Jean	Jean	k1gMnSc1	Jean
Bodin	Bodin	k1gMnSc1	Bodin
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
definováním	definování	k1gNnSc7	definování
kritérií	kritérion	k1gNnPc2	kritérion
suverenity	suverenita	k1gFnSc2	suverenita
<g/>
.	.	kIx.	.
</s>
<s>
Suverenita	suverenita	k1gFnSc1	suverenita
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
typem	typ	k1gInSc7	typ
autority	autorita	k1gFnSc2	autorita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgFnSc1d1	absolutní
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
suverénní	suverénní	k2eAgFnSc1d1	suverénní
autorita	autorita	k1gFnSc1	autorita
znamená	znamenat	k5eAaImIp3nS	znamenat
autoritu	autorita	k1gFnSc4	autorita
neomezenou	omezený	k2eNgFnSc7d1	neomezená
mocí	moc	k1gFnSc7	moc
<g/>
,	,	kIx,	,
působením	působení	k1gNnSc7	působení
ani	ani	k8xC	ani
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
tuto	tento	k3xDgFnSc4	tento
suverenitu	suverenita	k1gFnSc4	suverenita
drží	držet	k5eAaImIp3nS	držet
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
nikomu	nikdo	k3yNnSc3	nikdo
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
zákonům	zákon	k1gInPc3	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Suverén	suverén	k1gMnSc1	suverén
zákony	zákon	k1gInPc4	zákon
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
jim	on	k3xPp3gMnPc3	on
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Panovník	panovník	k1gMnSc1	panovník
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
dává	dávat	k5eAaImIp3nS	dávat
zákony	zákon	k1gInPc4	zákon
jiným	jiný	k2eAgMnPc3d1	jiný
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Zákon	zákon	k1gInSc1	zákon
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
příkaz	příkaz	k1gInSc1	příkaz
suveréna	suverén	k1gMnSc2	suverén
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
suverenita	suverenita	k1gFnSc1	suverenita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
daného	daný	k2eAgNnSc2d1	dané
teritoriálního	teritoriální	k2eAgNnSc2d1	teritoriální
společenství	společenství	k1gNnSc2	společenství
(	(	kIx(	(
<g/>
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
myšlenka	myšlenka	k1gFnSc1	myšlenka
vnější	vnější	k2eAgFnSc2d1	vnější
suverenity	suverenita	k1gFnSc2	suverenita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
autoritě	autorita	k1gFnSc3	autorita
cizího	cizí	k2eAgInSc2d1	cizí
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
vnější	vnější	k2eAgFnSc2d1	vnější
suverenity	suverenita	k1gFnSc2	suverenita
byl	být	k5eAaImAgMnS	být
rozporuplný	rozporuplný	k2eAgMnSc1d1	rozporuplný
zejména	zejména	k9	zejména
během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
existovala	existovat	k5eAaImAgFnS	existovat
nevyjasněná	vyjasněný	k2eNgFnSc1d1	nevyjasněná
role	role	k1gFnSc1	role
mezi	mezi	k7c7	mezi
císařem	císař	k1gMnSc7	císař
a	a	k8xC	a
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Vestfálský	vestfálský	k2eAgInSc1d1	vestfálský
mír	mír	k1gInSc1	mír
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
však	však	k9	však
dal	dát	k5eAaPmAgInS	dát
jednotlivým	jednotlivý	k2eAgMnPc3d1	jednotlivý
subjektům	subjekt	k1gInPc3	subjekt
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
říše	říš	k1gFnSc2	říš
právo	právo	k1gNnSc4	právo
řídit	řídit	k5eAaImF	řídit
své	svůj	k3xOyFgFnPc4	svůj
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgInS	být
posílen	posílit	k5eAaPmNgInS	posílit
princip	princip	k1gInSc1	princip
suverenity	suverenita	k1gFnSc2	suverenita
jako	jako	k8xC	jako
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ustálila	ustálit	k5eAaPmAgFnS	ustálit
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
mezi	mezi	k7c4	mezi
nezávislé	závislý	k2eNgInPc4d1	nezávislý
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
suverén	suverén	k1gMnSc1	suverén
má	mít	k5eAaImIp3nS	mít
absolutní	absolutní	k2eAgFnSc4d1	absolutní
autoritu	autorita	k1gFnSc4	autorita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgNnSc2	svůj
království	království	k1gNnSc2	království
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nulovou	nulový	k2eAgFnSc4d1	nulová
autoritu	autorita	k1gFnSc4	autorita
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
politickém	politický	k2eAgNnSc6d1	politické
myšlení	myšlení	k1gNnSc6	myšlení
tak	tak	k6eAd1	tak
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
chápání	chápání	k1gNnSc2	chápání
suverenity	suverenita	k1gFnSc2	suverenita
==	==	k?	==
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgNnSc1d1	tradiční
chápání	chápání	k1gNnSc1	chápání
suverenity	suverenita	k1gFnSc2	suverenita
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Emmericha	Emmerich	k1gMnSc2	Emmerich
de	de	k?	de
Vattel	Vattel	k1gMnSc1	Vattel
(	(	kIx(	(
<g/>
1714	[number]	k4	1714
<g/>
–	–	k?	–
<g/>
1767	[number]	k4	1767
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
upevnil	upevnit	k5eAaPmAgInS	upevnit
významy	význam	k1gInPc4	význam
ústředních	ústřední	k2eAgInPc2d1	ústřední
termínů	termín	k1gInPc2	termín
moderního	moderní	k2eAgNnSc2d1	moderní
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
právu	právo	k1gNnSc6	právo
rovné	rovný	k2eAgNnSc4d1	rovné
postavení	postavení	k1gNnSc4	postavení
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
slabé	slabý	k2eAgFnPc1d1	slabá
či	či	k8xC	či
mocné	mocný	k2eAgFnPc1d1	mocná
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Heinrich	Heinrich	k1gMnSc1	Heinrich
von	von	k1gInSc4	von
Treitschke	Treitschk	k1gInSc2	Treitschk
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
–	–	k?	–
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obránce	obránce	k1gMnSc1	obránce
pruského	pruský	k2eAgInSc2d1	pruský
expansionismu	expansionismus	k1gInSc2	expansionismus
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
suverénní	suverénní	k2eAgMnSc1d1	suverénní
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
maximálně	maximálně	k6eAd1	maximálně
soběstačný	soběstačný	k2eAgMnSc1d1	soběstačný
<g/>
.	.	kIx.	.
</s>
<s>
Opravdu	opravdu	k6eAd1	opravdu
suverénní	suverénní	k2eAgMnSc1d1	suverénní
podle	podle	k7c2	podle
něho	on	k3xPp3gNnSc2	on
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ale	ale	k8xC	ale
jen	jen	k9	jen
velké	velký	k2eAgInPc1d1	velký
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Treitschke	Treitschke	k6eAd1	Treitschke
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejčistších	čistý	k2eAgMnPc2d3	nejčistší
obhájců	obhájce	k1gMnPc2	obhájce
suverénního	suverénní	k2eAgInSc2d1	suverénní
státu	stát	k1gInSc2	stát
jakožto	jakožto	k8xS	jakožto
instituce	instituce	k1gFnSc2	instituce
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
vedena	vést	k5eAaImNgFnS	vést
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
státy	stát	k1gInPc7	stát
a	a	k8xC	a
která	který	k3yRgFnSc1	který
nesnese	snést	k5eNaPmIp3nS	snést
žádná	žádný	k3yNgNnPc4	žádný
omezení	omezení	k1gNnPc4	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Treitschke	Treitschke	k6eAd1	Treitschke
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
si	se	k3xPyFc3	se
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
myslelo	myslet	k5eAaImAgNnS	myslet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neodvážili	odvážit	k5eNaPmAgMnP	odvážit
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
vyslovit	vyslovit	k5eAaPmF	vyslovit
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vystihl	vystihnout	k5eAaPmAgMnS	vystihnout
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
diplomatickou	diplomatický	k2eAgFnSc4d1	diplomatická
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
<g/>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
výzvy	výzva	k1gFnSc2	výzva
tradičnímu	tradiční	k2eAgInSc3d1	tradiční
systému	systém	k1gInSc3	systém
chápání	chápání	k1gNnSc2	chápání
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
systému	systém	k1gInSc2	systém
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
především	především	k9	především
od	od	k7c2	od
představitelů	představitel	k1gMnPc2	představitel
evropského	evropský	k2eAgNnSc2d1	Evropské
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Immanuel	Immanuel	k1gMnSc1	Immanuel
Kant	Kant	k1gMnSc1	Kant
(	(	kIx(	(
<g/>
1724	[number]	k4	1724
<g/>
–	–	k?	–
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
předpovídal	předpovídat	k5eAaImAgInS	předpovídat
vytvoření	vytvoření	k1gNnSc4	vytvoření
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
federace	federace	k1gFnSc2	federace
států	stát	k1gInPc2	stát
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
republikanismu	republikanismus	k1gInSc6	republikanismus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
umožnila	umožnit	k5eAaPmAgFnS	umožnit
věčný	věčný	k2eAgInSc4d1	věčný
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
federaci	federace	k1gFnSc6	federace
by	by	kYmCp3nS	by
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
rozum	rozum	k1gInSc1	rozum
jako	jako	k8xC	jako
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
morální	morální	k2eAgFnSc1d1	morální
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
zavedení	zavedení	k1gNnSc4	zavedení
republikánského	republikánský	k2eAgInSc2d1	republikánský
principu	princip	k1gInSc2	princip
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
válečných	válečný	k2eAgFnPc2d1	válečná
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pacifistická	pacifistický	k2eAgFnSc1d1	pacifistická
federace	federace	k1gFnSc1	federace
není	být	k5eNaImIp3nS	být
světovou	světový	k2eAgFnSc7d1	světová
vládou	vláda	k1gFnSc7	vláda
ani	ani	k8xC	ani
světovým	světový	k2eAgInSc7d1	světový
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
národ	národ	k1gInSc1	národ
federace	federace	k1gFnSc2	federace
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
suverénním	suverénní	k2eAgMnSc7d1	suverénní
a	a	k8xC	a
svobodným	svobodný	k2eAgMnPc3d1	svobodný
od	od	k7c2	od
vnější	vnější	k2eAgFnSc2d1	vnější
regulace	regulace	k1gFnSc2	regulace
domácích	domácí	k2eAgFnPc2d1	domácí
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
<g/>
Tzv.	tzv.	kA	tzv.
realistickou	realistický	k2eAgFnSc4d1	realistická
interpretaci	interpretace	k1gFnSc4	interpretace
suverenity	suverenita	k1gFnSc2	suverenita
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
suverénních	suverénní	k2eAgInPc6d1	suverénní
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
národním	národní	k2eAgInSc6d1	národní
zájmu	zájem	k1gInSc6	zájem
a	a	k8xC	a
použití	použití	k1gNnSc6	použití
vojenské	vojenský	k2eAgFnSc2d1	vojenská
síly	síla	k1gFnSc2	síla
jako	jako	k8xS	jako
legitimního	legitimní	k2eAgInSc2d1	legitimní
prostředku	prostředek	k1gInSc2	prostředek
politického	politický	k2eAgInSc2d1	politický
boje	boj	k1gInSc2	boj
nabourala	nabourat	k5eAaPmAgFnS	nabourat
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
teorie	teorie	k1gFnSc2	teorie
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
závislosti	závislost	k1gFnSc2	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Keohane	Keohan	k1gMnSc5	Keohan
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
globalizace	globalizace	k1gFnSc1	globalizace
a	a	k8xC	a
prohlubující	prohlubující	k2eAgFnSc1d1	prohlubující
se	se	k3xPyFc4	se
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
institucionalizace	institucionalizace	k1gFnSc1	institucionalizace
–	–	k?	–
internacionalizace	internacionalizace	k1gFnSc1	internacionalizace
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesly	přinést	k5eAaPmAgInP	přinést
proměnu	proměna	k1gFnSc4	proměna
státní	státní	k2eAgFnSc2d1	státní
suverenity	suverenita	k1gFnSc2	suverenita
<g/>
.	.	kIx.	.
</s>
<s>
Suverenita	suverenita	k1gFnSc1	suverenita
již	již	k6eAd1	již
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
státům	stát	k1gInPc3	stát
vykonávat	vykonávat	k5eAaImF	vykonávat
efektivní	efektivní	k2eAgFnSc4d1	efektivní
nadvládu	nadvláda	k1gFnSc4	nadvláda
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
na	na	k7c6	na
jejich	jejich	k3xOp3gNnPc6	jejich
teritoriích	teritorium	k1gNnPc6	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Firemní	firemní	k2eAgNnPc1d1	firemní
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
jsou	být	k5eAaImIp3nP	být
činěna	činit	k5eAaImNgNnP	činit
na	na	k7c6	na
globální	globální	k2eAgFnSc6d1	globální
úrovni	úroveň	k1gFnSc6	úroveň
a	a	k8xC	a
politiky	politika	k1gFnSc2	politika
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
mají	mít	k5eAaImIp3nP	mít
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jejich	jejich	k3xOp3gFnPc2	jejich
vlastních	vlastní	k2eAgFnPc2d1	vlastní
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Teritoriálně	teritoriálně	k6eAd1	teritoriálně
vymezená	vymezený	k2eAgFnSc1d1	vymezená
státní	státní	k2eAgFnSc1d1	státní
suverenita	suverenita	k1gFnSc1	suverenita
tak	tak	k6eAd1	tak
přestala	přestat	k5eAaPmAgFnS	přestat
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
odpovídat	odpovídat	k5eAaImF	odpovídat
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
a	a	k8xC	a
režimy	režim	k1gInPc1	režim
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
upozaděny	upozaděn	k2eAgFnPc1d1	upozaděna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
důležitou	důležitý	k2eAgFnSc7d1	důležitá
platformou	platforma	k1gFnSc7	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
působení	působení	k1gNnSc1	působení
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
zájmy	zájem	k1gInPc4	zájem
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
participují	participovat	k5eAaImIp3nP	participovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
si	se	k3xPyFc3	se
vynucuje	vynucovat	k5eAaImIp3nS	vynucovat
proměnu	proměna	k1gFnSc4	proměna
chápání	chápání	k1gNnSc2	chápání
suverenity	suverenita	k1gFnSc2	suverenita
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
přijaly	přijmout	k5eAaPmAgInP	přijmout
omezení	omezení	k1gNnSc4	omezení
jejich	jejich	k3xOp3gFnPc2	jejich
dříve	dříve	k6eAd2	dříve
suverénní	suverénní	k2eAgFnPc1d1	suverénní
autority	autorita	k1gFnPc1	autorita
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
přistoupení	přistoupení	k1gNnSc2	přistoupení
k	k	k7c3	k
mnohostranným	mnohostranný	k2eAgInPc3d1	mnohostranný
režimům	režim	k1gInPc3	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
anarchie	anarchie	k1gFnSc1	anarchie
mezi	mezi	k7c7	mezi
suverénními	suverénní	k2eAgInPc7d1	suverénní
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
globální	globální	k2eAgInSc4d1	globální
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
interdependence	interdependence	k1gFnPc4	interdependence
dané	daný	k2eAgFnPc4d1	daná
jednou	jednou	k6eAd1	jednou
provždy	provždy	k6eAd1	provždy
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
proměnlivými	proměnlivý	k2eAgNnPc7d1	proměnlivé
uspořádáními	uspořádání	k1gNnPc7	uspořádání
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
nastolována	nastolován	k2eAgNnPc1d1	nastolováno
a	a	k8xC	a
udržována	udržovat	k5eAaImNgFnS	udržovat
institucionálně	institucionálně	k6eAd1	institucionálně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Belling	Belling	k1gInSc1	Belling
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Zrození	zrození	k1gNnSc1	zrození
suveréna	suverén	k1gMnSc2	suverén
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
suverenity	suverenita	k1gFnSc2	suverenita
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
kritika	kritika	k1gFnSc1	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
CDK	CDK	kA	CDK
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Souverain	Souveraina	k1gFnPc2	Souveraina
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
23	[number]	k4	23
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
745	[number]	k4	745
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Politická	politický	k2eAgFnSc1d1	politická
moc	moc	k1gFnSc1	moc
</s>
</p>
<p>
<s>
Stát	stát	k1gInSc1	stát
</s>
</p>
<p>
<s>
Suzerenita	Suzerenita	k1gFnSc1	Suzerenita
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Souverain	Souveraina	k1gFnPc2	Souveraina
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
