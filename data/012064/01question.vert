<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
právo	právo	k1gNnSc1	právo
vykonávat	vykonávat	k5eAaImF	vykonávat
neomezeně	omezeně	k6eNd1	omezeně
moc	moc	k6eAd1	moc
na	na	k7c6	na
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
náleží	náležet	k5eAaImIp3nS	náležet
každému	každý	k3xTgInSc3	každý
nezávislému	závislý	k2eNgInSc3d1	nezávislý
státu	stát	k1gInSc3	stát
<g/>
?	?	kIx.	?
</s>
