<s>
Svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
(	(	kIx(	(
<g/>
též	též	k9	též
suverenita	suverenita	k1gFnSc1	suverenita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
právo	právo	k1gNnSc4	právo
vykonávat	vykonávat	k5eAaImF	vykonávat
neomezeně	omezeně	k6eNd1	omezeně
moc	moc	k6eAd1	moc
na	na	k7c6	na
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
náleží	náležet	k5eAaImIp3nS	náležet
každému	každý	k3xTgInSc3	každý
nezávislému	závislý	k2eNgInSc3d1	nezávislý
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
