<s>
Jakou	jaký	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
zkratku	zkratka	k1gFnSc4
má	mít	k5eAaImIp3nS
standardizovaný	standardizovaný	k2eAgInSc1d1
strukturovaný	strukturovaný	k2eAgInSc1d1
dotazovací	dotazovací	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
daty	data	k1gNnPc7
v	v	k7c6
relačních	relační	k2eAgFnPc6d1
databázích	databáze	k1gFnPc6
a	a	k8xC
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
nástupcem	nástupce	k1gMnSc7
jazyka	jazyk	k1gInSc2
SEQUEL	SEQUEL	kA
<g/>
?	?	kIx.
</s>