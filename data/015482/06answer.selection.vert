<s>
SQL	SQL	kA
(	(	kIx(
<g/>
vyslovováno	vyslovován	k2eAgNnSc1d1
anglicky	anglicky	k6eAd1
es-kjů-el	es-kjů-lo	k1gNnPc2
[	[	kIx(
<g/>
ɛ	ɛ	k1gInSc1
kjuː	kjuː	k?
ɛ	ɛ	k?
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Structured	Structured	k1gInSc1
Query	Quera	k1gFnSc2
Language	language	k1gFnSc2
<g/>
)	)	kIx)
pro	pro	k7c4
standardizovaný	standardizovaný	k2eAgInSc4d1
strukturovaný	strukturovaný	k2eAgInSc4d1
dotazovací	dotazovací	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
daty	datum	k1gNnPc7
v	v	k7c6
relačních	relační	k2eAgFnPc6d1
databázích	databáze	k1gFnPc6
<g/>
.	.	kIx.
</s>