<s>
SQL	SQL	kA
</s>
<s>
SQL	SQL	kA
(	(	kIx(
<g/>
vyslovováno	vyslovován	k2eAgNnSc1d1
anglicky	anglicky	k6eAd1
es-kjů-el	es-kjů-lo	k1gNnPc2
[	[	kIx(
<g/>
ɛ	ɛ	k1gInSc1
kjuː	kjuː	k?
ɛ	ɛ	k?
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Structured	Structured	k1gInSc1
Query	Quera	k1gFnSc2
Language	language	k1gFnSc2
<g/>
)	)	kIx)
pro	pro	k7c4
standardizovaný	standardizovaný	k2eAgInSc4d1
strukturovaný	strukturovaný	k2eAgInSc4d1
dotazovací	dotazovací	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
používán	používat	k5eAaImNgInS
pro	pro	k7c4
práci	práce	k1gFnSc4
s	s	k7c7
daty	datum	k1gNnPc7
v	v	k7c6
relačních	relační	k2eAgFnPc6d1
databázích	databáze	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
SQL	SQL	kA
je	být	k5eAaImIp3nS
nástupcem	nástupce	k1gMnSc7
jazyka	jazyk	k1gInSc2
SEQUEL	SEQUEL	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Structured	Structured	k1gInSc1
English	Englisha	k1gFnPc2
Query	Quera	k1gFnSc2
Language	language	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
SQL	SQL	kA
</s>
<s>
V	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
probíhal	probíhat	k5eAaImAgMnS
ve	v	k7c6
firmě	firma	k1gFnSc6
IBM	IBM	kA
výzkum	výzkum	k1gInSc1
relačních	relační	k2eAgFnPc2d1
databází	databáze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
vytvořit	vytvořit	k5eAaPmF
sadu	sada	k1gFnSc4
příkazů	příkaz	k1gInPc2
pro	pro	k7c4
ovládání	ovládání	k1gNnSc4
těchto	tento	k3xDgFnPc2
databází	databáze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikl	vzniknout	k5eAaPmAgInS
tak	tak	k9
jazyk	jazyk	k1gInSc1
SEQUEL	SEQUEL	kA
(	(	kIx(
<g/>
Structured	Structured	k1gMnSc1
English	English	k1gMnSc1
Query	Quera	k1gFnSc2
Language	language	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
vytvořit	vytvořit	k5eAaPmF
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgNnSc6,k3yRgNnSc6,k3yIgNnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
příkazy	příkaz	k1gInPc1
tvořily	tvořit	k5eAaImAgInP
syntakticky	syntakticky	k6eAd1
co	co	k9
nejblíže	blízce	k6eAd3
přirozenému	přirozený	k2eAgInSc3d1
jazyku	jazyk	k1gInSc3
(	(	kIx(
<g/>
angličtině	angličtina	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
vývoji	vývoj	k1gInSc3
jazyka	jazyk	k1gInSc2
se	se	k3xPyFc4
přidaly	přidat	k5eAaPmAgFnP
další	další	k2eAgFnPc1d1
firmy	firma	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
r.	r.	kA
1979	#num#	k4
uvedla	uvést	k5eAaPmAgFnS
na	na	k7c4
trh	trh	k1gInSc4
firma	firma	k1gFnSc1
Relational	Relational	k1gFnSc1
Software	software	k1gInSc1
<g/>
,	,	kIx,
Inc	Inc	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgInSc1d1
Oracle	Oracle	k1gInSc1
Corporation	Corporation	k1gInSc1
<g/>
)	)	kIx)
svoji	svůj	k3xOyFgFnSc4
relační	relační	k2eAgFnSc4d1
databázovou	databázový	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
Oracle	Oracla	k1gFnSc3
Database	Databasa	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
IBM	IBM	kA
uvedla	uvést	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
nový	nový	k2eAgInSc1d1
systém	systém	k1gInSc1
SQL	SQL	kA
<g/>
/	/	kIx~
<g/>
DS	DS	kA
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
systém	systém	k1gInSc1
DB	db	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
systémy	systém	k1gInPc7
byly	být	k5eAaImAgFnP
např.	např.	kA
Progress	Progress	k1gInSc1
<g/>
,	,	kIx,
Informix	Informix	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
SyBase	SyBasa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
všech	všecek	k3xTgInPc6
těchto	tento	k3xDgInPc6
systémech	systém	k1gInPc6
se	se	k3xPyFc4
používala	používat	k5eAaImAgFnS
varianta	varianta	k1gFnSc1
jazyka	jazyk	k1gInSc2
SEQUEL	SEQUEL	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
přejmenován	přejmenovat	k5eAaPmNgInS
na	na	k7c6
SQL	SQL	kA
<g/>
.	.	kIx.
</s>
<s>
Relační	relační	k2eAgFnPc1d1
databáze	databáze	k1gFnPc1
byly	být	k5eAaImAgFnP
stále	stále	k6eAd1
významnější	významný	k2eAgFnPc1d2
<g/>
,	,	kIx,
a	a	k8xC
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
jejich	jejich	k3xOp3gInSc4
jazyk	jazyk	k1gInSc4
standardizovat	standardizovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americký	americký	k2eAgInSc1d1
institut	institut	k1gInSc1
ANSI	ANSI	kA
původně	původně	k6eAd1
chtěl	chtít	k5eAaImAgMnS
vydat	vydat	k5eAaPmF
jako	jako	k9
standard	standard	k1gInSc4
zcela	zcela	k6eAd1
nový	nový	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
RDL	RDL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
SQL	SQL	kA
se	se	k3xPyFc4
však	však	k9
prosadil	prosadit	k5eAaPmAgMnS
jako	jako	k9
de	de	k?
facto	fact	k2eAgNnSc1d1
standard	standard	k1gInSc4
a	a	k8xC
ANSI	ANSI	kA
založil	založit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
standard	standard	k1gInSc1
na	na	k7c6
tomto	tento	k3xDgInSc6
jazyku	jazyk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
standard	standard	k1gInSc1
bývá	bývat	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
jako	jako	k9
SQL-86	SQL-86	k1gMnPc3
podle	podle	k7c2
roku	rok	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dalších	další	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
SQL-86	SQL-86	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
některé	některý	k3yIgInPc4
nedostatky	nedostatek	k1gInPc4
a	a	k8xC
naopak	naopak	k6eAd1
v	v	k7c6
něm	on	k3xPp3gInSc6
nejsou	být	k5eNaImIp3nP
obsaženy	obsáhnout	k5eAaPmNgInP
některé	některý	k3yIgInPc1
důležité	důležitý	k2eAgInPc1d1
prvky	prvek	k1gInPc1
týkající	týkající	k2eAgInPc1d1
se	se	k3xPyFc4
hlavně	hlavně	k6eAd1
integrity	integrita	k1gFnSc2
databáze	databáze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
byl	být	k5eAaImAgInS
proto	proto	k8xC
přijat	přijmout	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
standard	standard	k1gInSc1
SQL-92	SQL-92	k1gFnSc2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
jen	jen	k9
SQL	SQL	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
nejnovějším	nový	k2eAgInSc7d3
standardem	standard	k1gInSc7
je	být	k5eAaImIp3nS
SQL3	SQL3	k1gFnSc1
(	(	kIx(
<g/>
SQL-	SQL-	k1gFnSc1
<g/>
99	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
reaguje	reagovat	k5eAaBmIp3nS
na	na	k7c4
potřeby	potřeba	k1gFnPc4
nejmodernějších	moderní	k2eAgFnPc2d3
databází	databáze	k1gFnPc2
s	s	k7c7
objektovými	objektový	k2eAgInPc7d1
prvky	prvek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Standardy	standard	k1gInPc4
podporuje	podporovat	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
každá	každý	k3xTgFnSc1
relační	relační	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
obvykle	obvykle	k6eAd1
nejsou	být	k5eNaImIp3nP
implementovány	implementován	k2eAgInPc4d1
vždy	vždy	k6eAd1
všechny	všechen	k3xTgInPc4
požadavky	požadavek	k1gInPc4
normy	norma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
naopak	naopak	k6eAd1
<g/>
,	,	kIx,
každá	každý	k3xTgFnSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
obsahuje	obsahovat	k5eAaImIp3nS
prvky	prvek	k1gInPc4
a	a	k8xC
konstrukce	konstrukce	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
nejsou	být	k5eNaImIp3nP
ve	v	k7c6
standardech	standard	k1gInPc6
obsaženy	obsáhnout	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenositelnost	přenositelnost	k1gFnSc1
SQL	SQL	kA
dotazů	dotaz	k1gInPc2
mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
databázemi	databáze	k1gFnPc7
je	být	k5eAaImIp3nS
proto	proto	k8xC
omezená	omezený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
jazyka	jazyk	k1gInSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Příkazy	příkaz	k1gInPc4
jazyka	jazyk	k1gInSc2
SQL	SQL	kA
<g/>
.	.	kIx.
</s>
<s>
SQL	SQL	kA
příkazy	příkaz	k1gInPc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
na	na	k7c4
čtyři	čtyři	k4xCgFnPc4
základní	základní	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
data	datum	k1gNnPc4
definition	definition	k1gInSc1
language	language	k1gFnSc1
(	(	kIx(
<g/>
DDL	DDL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
příkazy	příkaz	k1gInPc4
pro	pro	k7c4
definici	definice	k1gFnSc4
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
CREATE	CREATE	kA
<g/>
,	,	kIx,
ALTER	ALTER	kA
<g/>
,	,	kIx,
DROP	drop	k1gMnSc1
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
data	datum	k1gNnPc4
manipulation	manipulation	k1gInSc1
language	language	k1gFnSc1
(	(	kIx(
<g/>
DML	DML	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
příkazy	příkaz	k1gInPc4
pro	pro	k7c4
manipulaci	manipulace	k1gFnSc4
s	s	k7c7
daty	datum	k1gNnPc7
(	(	kIx(
<g/>
SELECT	SELECT	kA
<g/>
,	,	kIx,
INSERT	insert	k1gInSc1
<g/>
,	,	kIx,
UPDATE	update	k1gInSc1
<g/>
,	,	kIx,
DELETE	DELETE	kA
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
data	datum	k1gNnPc4
control	control	k1gInSc1
language	language	k1gFnSc1
(	(	kIx(
<g/>
DCL	DCL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
transaction	transaction	k1gInSc1
control	control	k1gInSc1
commands	commands	k1gInSc1
(	(	kIx(
<g/>
TCC	TCC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
příkazy	příkaz	k1gInPc4
pro	pro	k7c4
řízení	řízení	k1gNnSc4
transakcí	transakce	k1gFnPc2
(	(	kIx(
<g/>
START	start	k1gInSc1
TRANSACTION	TRANSACTION	kA
<g/>
,	,	kIx,
COMMIT	COMMIT	kA
<g/>
,	,	kIx,
ROLLBACK	ROLLBACK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
příkazy	příkaz	k1gInPc1
pro	pro	k7c4
řízení	řízení	k1gNnSc4
přístupových	přístupový	k2eAgNnPc2d1
práv	právo	k1gNnPc2
(	(	kIx(
<g/>
GRANT	grant	k1gInSc1
<g/>
,	,	kIx,
REVOKE	REVOKE	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
ostatní	ostatní	k2eAgInPc1d1
nebo	nebo	k8xC
speciální	speciální	k2eAgInPc1d1
příkazy	příkaz	k1gInPc1
(	(	kIx(
<g/>
číslovače	číslovač	k1gMnPc4
<g/>
,	,	kIx,
schemata	schema	k1gNnPc4
<g/>
,	,	kIx,
...	...	k?
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Informix	Informix	kA
<g/>
.	.	kIx.
www.intax.cz	www.intax.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Databáze	databáze	k1gFnPc1
</s>
<s>
Primární	primární	k2eAgInSc1d1
klíč	klíč	k1gInSc1
</s>
<s>
Cizí	cizí	k2eAgInSc1d1
klíč	klíč	k1gInSc1
</s>
<s>
Tabulka	tabulka	k1gFnSc1
</s>
<s>
Databázová	databázový	k2eAgFnSc1d1
transakce	transakce	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
SQL	SQL	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
Tutoriál	tutoriál	k1gInSc1
SQL	SQL	kA
</s>
<s>
Tutoriál	tutoriál	k1gInSc1
PostgreSQL	PostgreSQL	k1gMnPc2
<g/>
,	,	kIx,
MySQL	MySQL	k1gMnPc2
<g/>
,	,	kIx,
SQLite	SQLit	k1gMnSc5
a	a	k8xC
Oracle	Oracl	k1gMnSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Programovací	programovací	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
abecední	abecední	k2eAgInSc1d1
seznam	seznam	k1gInSc4
programovacích	programovací	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
multiparadigmatické	multiparadigmatický	k2eAgFnSc2d1
</s>
<s>
Ada	Ada	kA
•	•	k?
C	C	kA
<g/>
++	++	k?
•	•	k?
Common	Common	k1gInSc1
Lisp	Lisp	k1gInSc1
•	•	k?
D	D	kA
•	•	k?
F	F	kA
<g/>
#	#	kIx~
•	•	k?
Go	Go	k1gMnSc1
•	•	k?
Oberon	Oberon	k1gMnSc1
•	•	k?
Perl	perl	k1gInSc1
•	•	k?
PHP	PHP	kA
•	•	k?
Python	Python	k1gMnSc1
•	•	k?
Ruby	rub	k1gInPc1
•	•	k?
Rust	Rust	k1gInSc1
•	•	k?
Scala	scát	k5eAaImAgFnS
•	•	k?
Swift	Swift	k1gMnSc1
•	•	k?
Tcl	Tcl	k1gMnSc1
(	(	kIx(
<g/>
Tk	Tk	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Vala	Vala	k1gMnSc1
strukturované	strukturovaný	k2eAgFnPc4d1
(	(	kIx(
<g/>
procedurální	procedurální	k2eAgFnPc4d1
<g/>
)	)	kIx)
</s>
<s>
AWK	AWK	kA
•	•	k?
C	C	kA
•	•	k?
COBOL	Cobol	kA
•	•	k?
DCL	DCL	kA
•	•	k?
Forth	Forth	k1gInSc1
•	•	k?
Fortran	Fortran	kA
•	•	k?
Lua	Lua	k1gMnSc1
•	•	k?
Modula-	Modula-	k1gMnSc1
<g/>
2	#num#	k4
/	/	kIx~
Modula-	Modula-	k1gFnSc1
<g/>
3	#num#	k4
•	•	k?
Pascal	pascal	k1gInSc1
•	•	k?
Pawn	Pawn	k1gInSc1
•	•	k?
PL	PL	kA
<g/>
/	/	kIx~
<g/>
SQL	SQL	kA
objektové	objektový	k2eAgInPc1d1
</s>
<s>
BETA	beta	k1gNnSc1
•	•	k?
Boo	boa	k1gFnSc5
•	•	k?
C	C	kA
<g/>
#	#	kIx~
•	•	k?
Eiffel	Eiffel	k1gMnSc1
•	•	k?
Java	Java	k1gMnSc1
(	(	kIx(
<g/>
Groovy	Groov	k1gInPc1
<g/>
,	,	kIx,
Kotlin	kotlina	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
JavaScript	JavaScript	k1gMnSc1
•	•	k?
Object	Object	k1gMnSc1
Pascal	Pascal	k1gMnSc1
•	•	k?
Objective-C	Objective-C	k1gMnSc1
•	•	k?
Smalltalk	Smalltalk	k1gMnSc1
•	•	k?
VB	VB	kA
<g/>
.	.	kIx.
<g/>
NET	NET	kA
funkcionální	funkcionální	k2eAgInSc1d1
</s>
<s>
Clean	Clean	k1gInSc1
•	•	k?
Ceylon	Ceylon	k1gInSc1
•	•	k?
Erlang	Erlang	k1gInSc1
•	•	k?
Haskell	Haskell	k1gInSc1
•	•	k?
J	J	kA
•	•	k?
Lisp	Lisp	k1gMnSc1
•	•	k?
Mathematica	Mathematica	k1gMnSc1
•	•	k?
Miranda	Miranda	k1gFnSc1
•	•	k?
OCaml	OCaml	k1gInSc1
•	•	k?
Scheme	Schem	k1gInSc5
dotazovací	dotazovací	k2eAgFnSc6d1
</s>
<s>
LINQ	LINQ	kA
•	•	k?
SPARQL	SPARQL	kA
•	•	k?
SQL	SQL	kA
•	•	k?
XQuery	XQuera	k1gFnSc2
•	•	k?
XSL	XSL	kA
(	(	kIx(
<g/>
XSLT	XSLT	kA
•	•	k?
XSL-FO	XSL-FO	k1gMnSc1
•	•	k?
XPath	XPath	k1gInSc1
<g/>
)	)	kIx)
logické	logický	k2eAgNnSc1d1
</s>
<s>
Gödel	Gödel	k1gInSc1
•	•	k?
Prolog	prolog	k1gInSc1
výukové	výukový	k2eAgInPc1d1
</s>
<s>
Baltazar	Baltazar	k1gMnSc1
•	•	k?
Baltík	Baltík	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
•	•	k?
Kodu	Kodus	k1gInSc2
Game	game	k1gInSc1
Lab	Lab	k1gFnSc1
•	•	k?
Logo	logo	k1gNnSc1
•	•	k?
Microsoft	Microsoft	kA
Small	Small	k1gInSc4
Basic	Basic	kA
•	•	k?
Petr	Petr	k1gMnSc1
•	•	k?
Scratch	Scratch	k1gMnSc1
ezoterické	ezoterický	k2eAgInPc4d1
</s>
<s>
Befunge	Befunge	k1gFnSc1
•	•	k?
Brainfuck	Brainfuck	k1gInSc1
•	•	k?
HQ	HQ	kA
<g/>
9	#num#	k4
<g/>
+	+	kIx~
•	•	k?
Malbolge	Malbolge	k1gInSc1
•	•	k?
Ook	Ook	k1gFnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
•	•	k?
Piet	pieta	k1gFnPc2
•	•	k?
Whitespace	Whitespace	k1gFnSc2
historické	historický	k2eAgFnSc2d1
</s>
<s>
ALGOL	Algol	k1gInSc1
•	•	k?
APL	APL	kA
•	•	k?
B	B	kA
•	•	k?
BASIC	Basic	kA
•	•	k?
CPL	CPL	kA
(	(	kIx(
<g/>
BCPL	BCPL	kA
<g/>
)	)	kIx)
•	•	k?
J	J	kA
•	•	k?
MUMPS	MUMPS	kA
•	•	k?
PL	PL	kA
<g/>
/	/	kIx~
<g/>
I	i	k9
•	•	k?
Simula	Simula	k1gFnSc1
67	#num#	k4
•	•	k?
SNOBOL	SNOBOL	kA
další	další	k2eAgFnSc1d1
</s>
<s>
ABAP	ABAP	kA
•	•	k?
AppleScript	AppleScript	k1gInSc1
•	•	k?
ColdFusion	ColdFusion	k1gInSc1
•	•	k?
JSA	být	k5eAaImSgInS
•	•	k?
Julia	Julius	k1gMnSc2
•	•	k?
MATLAB	MATLAB	kA
•	•	k?
R	R	kA
•	•	k?
Visual	Visual	k1gInSc1
Basic	Basic	kA
(	(	kIx(
<g/>
VBScript	VBScript	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Vimscript	Vimscript	k1gMnSc1
•	•	k?
Visual	Visual	k1gMnSc1
FoxPro	FoxPro	k1gNnSc1
skriptovací	skriptovací	k2eAgFnPc1d1
<g/>
/	/	kIx~
<g/>
strojový	strojový	k2eAgInSc1d1
kód	kód	k1gInSc1
•	•	k?
kompilované	kompilovaný	k2eAgFnPc4d1
<g/>
/	/	kIx~
<g/>
interpretované	interpretovaný	k2eAgFnPc4d1
•	•	k?
interaktivní	interaktivní	k2eAgFnPc4d1
<g/>
/	/	kIx~
<g/>
dávkové	dávkový	k2eAgFnPc4d1
•	•	k?
WYSIWYG	WYSIWYG	kA
</s>
<s>
SQL	SQL	kA
příkazy	příkaz	k1gInPc4
jazyka	jazyk	k1gInSc2
SQL	SQL	kA
</s>
<s>
DML	DML	kA
<g/>
:	:	kIx,
SELECT	SELECT	kA
•	•	k?
INSERT	insert	k1gInSc1
•	•	k?
UPDATE	update	k1gInSc1
•	•	k?
DELETE	DELETE	kA
|	|	kIx~
DDL	DDL	kA
<g/>
:	:	kIx,
CREATE	CREATE	kA
•	•	k?
DROP	drop	k1gMnSc1
•	•	k?
ALTER	ALTER	kA
•	•	k?
CHECK	CHECK	kA
•	•	k?
SHOW	show	k1gFnSc2
•	•	k?
EXPLAIN	EXPLAIN	kA
•	•	k?
MERGE	MERGE	kA
•	•	k?
TRUNCATE	TRUNCATE	kA
•	•	k?
LOAD	LOAD	kA
DATA	datum	k1gNnSc2
|	|	kIx~
DCL	DCL	kA
<g/>
:	:	kIx,
GRANT	grant	k1gInSc1
•	•	k?
REVOKE	REVOKE	kA
|	|	kIx~
START	start	k1gInSc1
TRANSACTION	TRANSACTION	kA
•	•	k?
COMMIT	COMMIT	kA
•	•	k?
ROLLBACK	ROLLBACK	kA
klíčová	klíčový	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
pro	pro	k7c4
dotazování	dotazování	k1gNnSc4
</s>
<s>
TOP	topit	k5eAaImRp2nS
•	•	k?
LIMIT	limit	k1gInSc1
•	•	k?
JOIN	JOIN	kA
(	(	kIx(
<g/>
FULL	FULL	kA
LEFT	LEFT	kA
RIGHT	RIGHT	kA
INNER	INNER	kA
CROSS	CROSS	kA
<g/>
)	)	kIx)
ON	on	k3xPp3gMnSc1
•	•	k?
UNION	union	k1gInSc1
•	•	k?
ORDER	ORDER	kA
BY	by	k9
•	•	k?
WHERE	WHERE	kA
•	•	k?
GROUP	GROUP	kA
BY	by	k9
•	•	k?
WITH	WITH	kA
ROLLUP	ROLLUP	kA
•	•	k?
HAVING	HAVING	kA
objekty	objekt	k1gInPc7
</s>
<s>
TABLE	tablo	k1gNnSc6
•	•	k?
VIEW	VIEW	kA
•	•	k?
INDEX	index	k1gInSc1
•	•	k?
CONSTRAINT	CONSTRAINT	kA
•	•	k?
TRANSACTION	TRANSACTION	kA
•	•	k?
TRIGGER	TRIGGER	kA
ostatní	ostatní	k2eAgInPc1d1
pojmy	pojem	k1gInPc1
</s>
<s>
Relační	relační	k2eAgFnSc1d1
databáze	databáze	k1gFnSc1
•	•	k?
Systém	systém	k1gInSc1
řízení	řízení	k1gNnSc2
báze	báze	k1gFnSc1
dat	datum	k1gNnPc2
•	•	k?
Primární	primární	k2eAgInSc1d1
klíč	klíč	k1gInSc1
•	•	k?
Cizí	cizí	k2eAgInSc1d1
klíč	klíč	k1gInSc1
•	•	k?
Poddotaz	Poddotaz	k1gInSc1
•	•	k?
Uložená	uložený	k2eAgFnSc1d1
procedura	procedura	k1gFnSc1
•	•	k?
Uživatelem	uživatel	k1gMnSc7
definovaná	definovaný	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
•	•	k?
Partition	Partition	k1gInSc4
•	•	k?
Agregační	agregační	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
•	•	k?
Referenční	referenční	k2eAgFnSc1d1
integrita	integrita	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4134010-3	4134010-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
12536	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
86006628	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
86006628	#num#	k4
</s>
