<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
jehly	jehla	k1gFnPc4	jehla
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
injekčních	injekční	k2eAgFnPc2d1	injekční
jehel	jehla	k1gFnPc2	jehla
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
termín	termín	k1gInSc1	termín
trypanofobie	trypanofobie	k1gFnSc2	trypanofobie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nůžky	nůžky	k1gFnPc4	nůžky
<g/>
,	,	kIx,	,
špičaté	špičatý	k2eAgInPc4d1	špičatý
konce	konec	k1gInPc4	konec
deštníků	deštník	k1gInPc2	deštník
a	a	k8xC	a
ostrý	ostrý	k2eAgInSc1d1	ostrý
hrot	hrot	k1gInSc1	hrot
pera	pero	k1gNnSc2	pero
<g/>
.	.	kIx.	.
</s>
