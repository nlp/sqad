<p>
<s>
Aichmofobie	Aichmofobie	k1gFnSc1	Aichmofobie
je	být	k5eAaImIp3nS	být
strach	strach	k1gInSc4	strach
z	z	k7c2	z
ostrých	ostrý	k2eAgInPc2d1	ostrý
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
druhy	druh	k1gInPc4	druh
fóbií	fóbie	k1gFnPc2	fóbie
<g/>
.	.	kIx.	.
<g/>
Určitý	určitý	k2eAgInSc4d1	určitý
strach	strach	k1gInSc4	strach
z	z	k7c2	z
ostrých	ostrý	k2eAgInPc2d1	ostrý
předmětů	předmět	k1gInPc2	předmět
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc1d1	přirozený
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vrozený	vrozený	k2eAgInSc1d1	vrozený
reflex	reflex	k1gInSc1	reflex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
aichmofobie	aichmofobie	k1gFnSc2	aichmofobie
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
strach	strach	k1gInSc4	strach
nepřirozený-přehnaný	přirozenýřehnaný	k2eNgInSc4d1	přirozený-přehnaný
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
o	o	k7c4	o
jehly	jehla	k1gFnPc4	jehla
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
injekčních	injekční	k2eAgFnPc2d1	injekční
jehel	jehla	k1gFnPc2	jehla
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
termín	termín	k1gInSc1	termín
trypanofobie	trypanofobie	k1gFnSc2	trypanofobie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nůžky	nůžky	k1gFnPc4	nůžky
<g/>
,	,	kIx,	,
špičaté	špičatý	k2eAgInPc4d1	špičatý
konce	konec	k1gInPc4	konec
deštníků	deštník	k1gInPc2	deštník
a	a	k8xC	a
ostrý	ostrý	k2eAgInSc1d1	ostrý
hrot	hrot	k1gInSc1	hrot
pera	pero	k1gNnSc2	pero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Projevy	projev	k1gInPc4	projev
==	==	k?	==
</s>
</p>
<p>
<s>
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
úzkost	úzkost	k1gFnSc1	úzkost
</s>
</p>
<p>
<s>
pocení	pocení	k1gNnSc1	pocení
</s>
</p>
<p>
<s>
nevolnost	nevolnost	k1gFnSc1	nevolnost
</s>
</p>
<p>
<s>
sucho	sucho	k1gNnSc1	sucho
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
</s>
</p>
<p>
<s>
bušení	bušení	k1gNnSc1	bušení
srdce	srdce	k1gNnSc2	srdce
</s>
</p>
<p>
<s>
točení	točení	k1gNnSc1	točení
hlavya	hlavy	k1gInSc2	hlavy
další	další	k2eAgFnSc2d1	další
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
trpící	trpící	k2eAgMnPc1d1	trpící
fóbií	fóbie	k1gFnPc2	fóbie
z	z	k7c2	z
ostrých	ostrý	k2eAgInPc2d1	ostrý
předmětů	předmět	k1gInPc2	předmět
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
se	se	k3xPyFc4	se
kontaktu	kontakt	k1gInSc3	kontakt
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepoužívají	používat	k5eNaImIp3nP	používat
ostré	ostrý	k2eAgFnPc4d1	ostrá
nůžky	nůžky	k1gFnPc4	nůžky
a	a	k8xC	a
nože	nůž	k1gInPc4	nůž
a	a	k8xC	a
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
se	se	k3xPyFc4	se
očkování	očkování	k1gNnSc2	očkování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
lékařský	lékařský	k2eAgInSc1d1	lékařský
zákrok	zákrok	k1gInSc1	zákrok
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
odběr	odběr	k1gInSc1	odběr
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
chirurgický	chirurgický	k2eAgInSc1d1	chirurgický
zákrok	zákrok	k1gInSc1	zákrok
či	či	k8xC	či
očkování	očkování	k1gNnSc1	očkování
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
aichnofobie	aichnofobie	k1gFnSc1	aichnofobie
příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
chová	chovat	k5eAaImIp3nS	chovat
velmi	velmi	k6eAd1	velmi
agresivně	agresivně	k6eAd1	agresivně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
cítí	cítit	k5eAaImIp3nS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
to	ten	k3xDgNnSc1	ten
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
na	na	k7c6	na
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Projevy	projev	k1gInPc7	projev
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Také	také	k9	také
tuto	tento	k3xDgFnSc4	tento
fobii	fobie	k1gFnSc4	fobie
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
mohl	moct	k5eAaImAgInS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
aichmofobie	aichmofobie	k1gFnSc2	aichmofobie
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
používá	používat	k5eAaImIp3nS	používat
behaviorální	behaviorální	k2eAgFnSc1d1	behaviorální
terapie	terapie	k1gFnSc1	terapie
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
může	moct	k5eAaImIp3nS	moct
též	též	k9	též
předepsat	předepsat	k5eAaPmF	předepsat
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zmírňují	zmírňovat	k5eAaImIp3nP	zmírňovat
úzkost	úzkost	k1gFnSc4	úzkost
(	(	kIx(	(
<g/>
antidepresiva	antidepresivum	k1gNnSc2	antidepresivum
či	či	k8xC	či
anxiolytika	anxiolytikum	k1gNnSc2	anxiolytikum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Šípková	šípkový	k2eAgFnSc1d1	šípková
Růženka	Růženka	k1gFnSc1	Růženka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Aichmofobie	Aichmofobie	k1gFnSc1	Aichmofobie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
