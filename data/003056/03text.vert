<s>
Inflace	inflace	k1gFnSc1	inflace
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
ekonomů	ekonom	k1gMnPc2	ekonom
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
nárůst	nárůst	k1gInSc1	nárůst
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
cenové	cenový	k2eAgFnSc2d1	cenová
hladiny	hladina	k1gFnSc2	hladina
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
časovém	časový	k2eAgNnSc6d1	časové
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Ekvivalentně	ekvivalentně	k6eAd1	ekvivalentně
lze	lze	k6eAd1	lze
inflaci	inflace	k1gFnSc4	inflace
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
snížení	snížení	k1gNnSc4	snížení
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Změnu	změna	k1gFnSc4	změna
cenové	cenový	k2eAgFnSc2d1	cenová
hladiny	hladina	k1gFnSc2	hladina
za	za	k7c4	za
určité	určitý	k2eAgNnSc4d1	určité
období	období	k1gNnSc4	období
udává	udávat	k5eAaImIp3nS	udávat
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc1	inflace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vypočítává	vypočítávat	k5eAaImIp3nS	vypočítávat
jako	jako	k9	jako
poměr	poměr	k1gInSc1	poměr
vybraného	vybraný	k2eAgInSc2d1	vybraný
cenového	cenový	k2eAgInSc2d1	cenový
indexu	index	k1gInSc2	index
na	na	k7c6	na
konci	konec	k1gInSc6	konec
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Nejpoužívanějšími	používaný	k2eAgInPc7d3	nejpoužívanější
cenovými	cenový	k2eAgInPc7d1	cenový
indexy	index	k1gInPc7	index
jsou	být	k5eAaImIp3nP	být
index	index	k1gInSc4	index
spotřebitelských	spotřebitelský	k2eAgFnPc2d1	spotřebitelská
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
index	index	k1gInSc1	index
cen	cena	k1gFnPc2	cena
výrobců	výrobce	k1gMnPc2	výrobce
a	a	k8xC	a
deflátor	deflátor	k1gInSc1	deflátor
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomové	ekonom	k1gMnPc1	ekonom
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
shodují	shodovat	k5eAaImIp3nP	shodovat
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysoká	vysoký	k2eAgFnSc1d1	vysoká
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc2	inflace
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
nadměrným	nadměrný	k2eAgInSc7d1	nadměrný
růstem	růst	k1gInSc7	růst
peněžní	peněžní	k2eAgFnPc4d1	peněžní
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
faktory	faktor	k1gInPc1	faktor
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
nízkou	nízký	k2eAgFnSc4d1	nízká
až	až	k9	až
střední	střední	k2eAgFnSc4d1	střední
míru	míra	k1gFnSc4	míra
inflace	inflace	k1gFnSc2	inflace
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
rozmanitější	rozmanitý	k2eAgFnPc1d2	rozmanitější
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
až	až	k8xS	až
mírná	mírný	k2eAgFnSc1d1	mírná
inflace	inflace	k1gFnSc1	inflace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vedle	vedle	k7c2	vedle
růstu	růst	k1gInSc2	růst
peněžní	peněžní	k2eAgFnPc1d1	peněžní
zásoby	zásoba	k1gFnPc1	zásoba
vysvětlována	vysvětlován	k2eAgFnSc1d1	vysvětlována
i	i	k8xC	i
kolísáním	kolísání	k1gNnSc7	kolísání
reálné	reálný	k2eAgFnSc2d1	reálná
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c4	po
zboží	zboží	k1gNnSc4	zboží
nebo	nebo	k8xC	nebo
výkyvy	výkyv	k1gInPc4	výkyv
v	v	k7c6	v
nabídce	nabídka	k1gFnSc6	nabídka
a	a	k8xC	a
dodávkách	dodávka	k1gFnPc6	dodávka
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
panuje	panovat	k5eAaImIp3nS	panovat
konsenzus	konsenzus	k1gInSc1	konsenzus
<g/>
,	,	kIx,	,
že	že	k8xS	že
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
období	období	k1gNnSc1	období
významné	významný	k2eAgFnSc2d1	významná
inflace	inflace	k1gFnSc2	inflace
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
rychlejším	rychlý	k2eAgInSc7d2	rychlejší
růstem	růst	k1gInSc7	růst
peněžní	peněžní	k2eAgFnSc2d1	peněžní
zásoby	zásoba	k1gFnSc2	zásoba
než	než	k8xS	než
celkového	celkový	k2eAgInSc2d1	celkový
produktu	produkt	k1gInSc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Účinky	účinek	k1gInPc1	účinek
inflace	inflace	k1gFnSc2	inflace
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
negativní	negativní	k2eAgInPc4d1	negativní
dopady	dopad	k1gInPc4	dopad
inflace	inflace	k1gFnSc2	inflace
patří	patřit	k5eAaImIp3nP	patřit
snížení	snížení	k1gNnSc4	snížení
reálné	reálný	k2eAgFnSc2d1	reálná
hodnoty	hodnota	k1gFnSc2	hodnota
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
peněžních	peněžní	k2eAgNnPc2d1	peněžní
aktiv	aktivum	k1gNnPc2	aktivum
<g/>
,	,	kIx,	,
nejistota	nejistota	k1gFnSc1	nejistota
ohledně	ohledně	k7c2	ohledně
budoucího	budoucí	k2eAgInSc2d1	budoucí
vývoje	vývoj	k1gInSc2	vývoj
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
odrazuje	odrazovat	k5eAaImIp3nS	odrazovat
investice	investice	k1gFnPc4	investice
a	a	k8xC	a
úspory	úspora	k1gFnPc4	úspora
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
inflace	inflace	k1gFnSc1	inflace
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
nedostatku	nedostatek	k1gInSc3	nedostatek
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	on	k3xPp3gMnPc4	on
spotřebitelé	spotřebitel	k1gMnPc1	spotřebitel
začnou	začít	k5eAaPmIp3nP	začít
hromadit	hromadit	k5eAaImF	hromadit
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
ceny	cena	k1gFnPc1	cena
se	se	k3xPyFc4	se
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
škol	škola	k1gFnPc2	škola
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
účinky	účinek	k1gInPc4	účinek
inflace	inflace	k1gFnSc2	inflace
i	i	k9	i
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
rozšíření	rozšíření	k1gNnSc4	rozšíření
možností	možnost	k1gFnPc2	možnost
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
upravovat	upravovat	k5eAaImF	upravovat
úrokové	úrokový	k2eAgFnPc4d1	úroková
sazby	sazba	k1gFnPc4	sazba
(	(	kIx(	(
<g/>
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
zmírnění	zmírnění	k1gNnSc4	zmírnění
recese	recese	k1gFnSc2	recese
<g/>
)	)	kIx)	)
a	a	k8xC	a
stimulace	stimulace	k1gFnSc1	stimulace
investic	investice	k1gFnPc2	investice
do	do	k7c2	do
nefinančních	finanční	k2eNgInPc2d1	nefinanční
investičních	investiční	k2eAgInPc2d1	investiční
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
ekonomů	ekonom	k1gMnPc2	ekonom
hlavního	hlavní	k2eAgInSc2d1	hlavní
proudu	proud	k1gInSc2	proud
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
pro	pro	k7c4	pro
nízkou	nízký	k2eAgFnSc4d1	nízká
stabilní	stabilní	k2eAgFnSc4d1	stabilní
inflaci	inflace	k1gFnSc4	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
nulové	nulový	k2eAgFnSc2d1	nulová
inflace	inflace	k1gFnSc2	inflace
nebo	nebo	k8xC	nebo
deflace	deflace	k1gFnSc2	deflace
<g/>
)	)	kIx)	)
inflace	inflace	k1gFnSc1	inflace
může	moct	k5eAaImIp3nS	moct
snížit	snížit	k5eAaPmF	snížit
závažnost	závažnost	k1gFnSc4	závažnost
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
recese	recese	k1gFnSc2	recese
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rychlejší	rychlý	k2eAgNnSc1d2	rychlejší
vyčištění	vyčištění	k1gNnSc1	vyčištění
trhu	trh	k1gInSc2	trh
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
snižuje	snižovat	k5eAaImIp3nS	snižovat
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
pasti	past	k1gFnSc2	past
na	na	k7c4	na
likviditu	likvidita	k1gFnSc4	likvidita
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
jako	jako	k8xC	jako
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
stabilizaci	stabilizace	k1gFnSc4	stabilizace
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úkol	úkol	k1gInSc4	úkol
zachovat	zachovat	k5eAaPmF	zachovat
nízkou	nízký	k2eAgFnSc4d1	nízká
a	a	k8xC	a
stabilní	stabilní	k2eAgFnSc4d1	stabilní
míru	míra	k1gFnSc4	míra
inflace	inflace	k1gFnSc2	inflace
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
svěřen	svěřit	k5eAaPmNgMnS	svěřit
měnovým	měnový	k2eAgInPc3d1	měnový
orgánům	orgán	k1gInPc3	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
těmito	tento	k3xDgInPc7	tento
měnovými	měnový	k2eAgInPc7d1	měnový
orgány	orgán	k1gInPc7	orgán
jsou	být	k5eAaImIp3nP	být
centrální	centrální	k2eAgFnSc1d1	centrální
banky	banka	k1gFnPc1	banka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
řídí	řídit	k5eAaImIp3nP	řídit
velikost	velikost	k1gFnSc4	velikost
peněžní	peněžní	k2eAgFnSc2d1	peněžní
zásoby	zásoba	k1gFnSc2	zásoba
nastavením	nastavení	k1gNnSc7	nastavení
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
<g/>
,	,	kIx,	,
operacemi	operace	k1gFnPc7	operace
na	na	k7c6	na
volném	volný	k2eAgInSc6d1	volný
trhu	trh	k1gInSc6	trh
a	a	k8xC	a
stanovením	stanovení	k1gNnSc7	stanovení
minimálních	minimální	k2eAgFnPc2d1	minimální
bankovních	bankovní	k2eAgFnPc2d1	bankovní
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
Změnu	změna	k1gFnSc4	změna
cenové	cenový	k2eAgFnSc2d1	cenová
hladiny	hladina	k1gFnSc2	hladina
za	za	k7c4	za
určité	určitý	k2eAgNnSc4d1	určité
období	období	k1gNnSc4	období
udává	udávat	k5eAaImIp3nS	udávat
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
inflace	inflace	k1gFnSc2	inflace
je	být	k5eAaImIp3nS	být
měřena	měřen	k2eAgNnPc1d1	měřeno
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
cenových	cenový	k2eAgInPc2d1	cenový
indexů	index	k1gInPc2	index
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Deflátor	deflátor	k1gInSc1	deflátor
HDP	HDP	kA	HDP
-	-	kIx~	-
poměr	poměr	k1gInSc1	poměr
nominálního	nominální	k2eAgInSc2d1	nominální
a	a	k8xC	a
reálného	reálný	k2eAgInSc2d1	reálný
HDP	HDP	kA	HDP
Index	index	k1gInSc1	index
spotřebitelských	spotřebitelský	k2eAgFnPc2d1	spotřebitelská
cen	cena	k1gFnPc2	cena
(	(	kIx(	(
<g/>
CPI	cpi	kA	cpi
-	-	kIx~	-
Consumer	Consumer	k1gMnSc1	Consumer
price	price	k1gFnSc2	price
index	index	k1gInSc1	index
<g/>
)	)	kIx)	)
Index	index	k1gInSc1	index
cen	cena	k1gFnPc2	cena
výrobců	výrobce	k1gMnPc2	výrobce
(	(	kIx(	(
<g/>
PPI	ppi	kA	ppi
-	-	kIx~	-
Producer	Producer	k1gMnSc1	Producer
price	priko	k6eAd1	priko
index	index	k1gInSc4	index
<g/>
)	)	kIx)	)
Čistou	čistý	k2eAgFnSc4d1	čistá
inflaci	inflace	k1gFnSc4	inflace
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
ekonomice	ekonomika	k1gFnSc6	ekonomika
vypočítává	vypočítávat	k5eAaImIp3nS	vypočítávat
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
jako	jako	k8xC	jako
přírůstek	přírůstek	k1gInSc1	přírůstek
cen	cena	k1gFnPc2	cena
v	v	k7c6	v
neregulované	regulovaný	k2eNgFnSc6d1	neregulovaná
části	část	k1gFnSc6	část
spotřebního	spotřební	k2eAgInSc2d1	spotřební
koše	koš	k1gInSc2	koš
očištěný	očištěný	k2eAgInSc4d1	očištěný
od	od	k7c2	od
vlivu	vliv	k1gInSc2	vliv
nepřímých	přímý	k2eNgFnPc2d1	nepřímá
daní	daň	k1gFnPc2	daň
a	a	k8xC	a
dotací	dotace	k1gFnPc2	dotace
<g/>
.	.	kIx.	.
</s>
<s>
Opačným	opačný	k2eAgInSc7d1	opačný
jevem	jev	k1gInSc7	jev
k	k	k7c3	k
inflaci	inflace	k1gFnSc3	inflace
je	být	k5eAaImIp3nS	být
deflace	deflace	k1gFnSc1	deflace
-	-	kIx~	-
pokles	pokles	k1gInSc1	pokles
cenové	cenový	k2eAgFnSc2d1	cenová
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Příbuzný	příbuzný	k2eAgInSc1d1	příbuzný
pojem	pojem	k1gInSc1	pojem
dezinflace	dezinflace	k1gFnSc2	dezinflace
označuje	označovat	k5eAaImIp3nS	označovat
pokles	pokles	k1gInSc1	pokles
míry	míra	k1gFnSc2	míra
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ekonomové	ekonom	k1gMnPc1	ekonom
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
k	k	k7c3	k
Rakouské	rakouský	k2eAgFnSc3d1	rakouská
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
škole	škola	k1gFnSc3	škola
<g/>
)	)	kIx)	)
definují	definovat	k5eAaBmIp3nP	definovat
inflaci	inflace	k1gFnSc4	inflace
odlišně	odlišně	k6eAd1	odlišně
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
jako	jako	k9	jako
jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
nárůst	nárůst	k1gInSc1	nárůst
nabídky	nabídka	k1gFnPc4	nabídka
peněz	peníze	k1gInPc2	peníze
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nepředstavuje	představovat	k5eNaImIp3nS	představovat
nárůst	nárůst	k1gInSc4	nárůst
zásoby	zásoba	k1gFnSc2	zásoba
měnového	měnový	k2eAgInSc2d1	měnový
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Inflací	inflace	k1gFnSc7	inflace
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
pojetí	pojetí	k1gNnSc6	pojetí
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
samo	sám	k3xTgNnSc1	sám
zvýšení	zvýšení	k1gNnSc1	zvýšení
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nafouknutí	nafouknutí	k1gNnSc1	nafouknutí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nabídky	nabídka	k1gFnSc2	nabídka
nekrytých	krytý	k2eNgInPc2d1	nekrytý
papírových	papírový	k2eAgInPc2d1	papírový
peněz	peníze	k1gInPc2	peníze
<g/>
;	;	kIx,	;
nárůst	nárůst	k1gInSc1	nárůst
cen	cena	k1gFnPc2	cena
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
inflace	inflace	k1gFnSc1	inflace
v	v	k7c6	v
mainstreamové	mainstreamový	k2eAgFnSc6d1	mainstreamová
definici	definice	k1gFnSc6	definice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
důsledkem	důsledek	k1gInSc7	důsledek
tohoto	tento	k3xDgNnSc2	tento
zvýšení	zvýšení	k1gNnSc2	zvýšení
<g/>
.	.	kIx.	.
</s>
<s>
Cenová	cenový	k2eAgFnSc1d1	cenová
hladina	hladina	k1gFnSc1	hladina
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
celkovým	celkový	k2eAgNnSc7d1	celkové
množstvím	množství	k1gNnSc7	množství
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
celkovým	celkový	k2eAgInSc7d1	celkový
objemem	objem	k1gInSc7	objem
statků	statek	k1gInPc2	statek
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yQgInPc4	který
jsou	být	k5eAaImIp3nP	být
směňovány	směňován	k2eAgFnPc1d1	směňována
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
nebo	nebo	k8xC	nebo
pokles	pokles	k1gInSc1	pokles
cenové	cenový	k2eAgFnSc2d1	cenová
hladiny	hladina	k1gFnSc2	hladina
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
řada	řada	k1gFnSc1	řada
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
množství	množství	k1gNnSc4	množství
peněz	peníze	k1gInPc2	peníze
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
vliv	vliv	k1gInSc1	vliv
rozhodování	rozhodování	k1gNnSc2	rozhodování
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc2	množství
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
je	být	k5eAaImIp3nS	být
ovlivňováno	ovlivňovat	k5eAaImNgNnS	ovlivňovat
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
jen	jen	k9	jen
tržními	tržní	k2eAgFnPc7d1	tržní
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
produkčními	produkční	k2eAgFnPc7d1	produkční
schopnostmi	schopnost	k1gFnPc7	schopnost
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
vlivy	vliv	k1gInPc4	vliv
působí	působit	k5eAaImIp3nS	působit
protichůdně	protichůdně	k6eAd1	protichůdně
<g/>
.	.	kIx.	.
</s>
<s>
Faktory	faktor	k1gInPc1	faktor
působící	působící	k2eAgInPc1d1	působící
na	na	k7c4	na
rovnováhu	rovnováha	k1gFnSc4	rovnováha
cenové	cenový	k2eAgFnSc2d1	cenová
hladiny	hladina	k1gFnSc2	hladina
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
poptávky	poptávka	k1gFnSc2	poptávka
nebo	nebo	k8xC	nebo
nabídky	nabídka	k1gFnSc2	nabídka
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgFnPc7d1	základní
příčinami	příčina	k1gFnPc7	příčina
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
poptávky	poptávka	k1gFnSc2	poptávka
je	být	k5eAaImIp3nS	být
růst	růst	k1gInSc1	růst
množství	množství	k1gNnSc2	množství
peněz	peníze	k1gInPc2	peníze
při	při	k7c6	při
stejné	stejný	k2eAgFnSc6d1	stejná
velikosti	velikost	k1gFnSc6	velikost
trhu	trh	k1gInSc2	trh
a	a	k8xC	a
růst	růst	k1gInSc1	růst
trhu	trh	k1gInSc2	trh
tzn.	tzn.	kA	tzn.
poptávajících	poptávající	k2eAgFnPc2d1	poptávající
<g/>
,	,	kIx,	,
při	při	k7c6	při
neměnném	neměnný	k2eAgNnSc6d1	neměnné
množství	množství	k1gNnSc6	množství
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
množství	množství	k1gNnSc6	množství
peněz	peníze	k1gInPc2	peníze
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
novodobých	novodobý	k2eAgFnPc6d1	novodobá
dějinách	dějiny	k1gFnPc6	dějiny
vliv	vliv	k1gInSc1	vliv
pouze	pouze	k6eAd1	pouze
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
oběživa	oběživo	k1gNnSc2	oběživo
je	být	k5eAaImIp3nS	být
zvyšováno	zvyšovat	k5eAaImNgNnS	zvyšovat
emisí	emise	k1gFnSc7	emise
nových	nový	k2eAgInPc2d1	nový
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
snižováno	snižován	k2eAgNnSc4d1	snižováno
stahováním	stahování	k1gNnSc7	stahování
peněz	peníze	k1gInPc2	peníze
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
princip	princip	k1gInSc1	princip
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
banka	banka	k1gFnSc1	banka
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
bezhotovostních	bezhotovostní	k2eAgInPc2d1	bezhotovostní
peněz	peníze	k1gInPc2	peníze
zvyšováním	zvyšování	k1gNnSc7	zvyšování
a	a	k8xC	a
snižováním	snižování	k1gNnSc7	snižování
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
trhu	trh	k1gInSc2	trh
působí	působit	k5eAaImIp3nS	působit
populační	populační	k2eAgInSc1d1	populační
růst	růst	k1gInSc1	růst
nebo	nebo	k8xC	nebo
příliv	příliv	k1gInSc1	příliv
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
investorů	investor	k1gMnPc2	investor
<g/>
.	.	kIx.	.
</s>
<s>
Populační	populační	k2eAgInSc1d1	populační
růst	růst	k1gInSc1	růst
<g/>
/	/	kIx~	/
<g/>
pokles	pokles	k1gInSc1	pokles
působí	působit	k5eAaImIp3nS	působit
většinou	většina	k1gFnSc7	většina
velmi	velmi	k6eAd1	velmi
zvolna	zvolna	k6eAd1	zvolna
<g/>
,	,	kIx,	,
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Příliv	příliv	k1gInSc1	příliv
a	a	k8xC	a
odliv	odliv	k1gInSc1	odliv
investorů	investor	k1gMnPc2	investor
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
poptávku	poptávka	k1gFnSc4	poptávka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
cenovou	cenový	k2eAgFnSc4d1	cenová
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
až	až	k9	až
skokově	skokově	k6eAd1	skokově
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgInPc3d1	významný
psychologickým	psychologický	k2eAgInPc3d1	psychologický
vlivům	vliv	k1gInPc3	vliv
patří	patřit	k5eAaImIp3nS	patřit
očekávání	očekávání	k1gNnSc1	očekávání
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
totiž	totiž	k9	totiž
spotřebitelé	spotřebitel	k1gMnPc1	spotřebitel
očekávají	očekávat	k5eAaImIp3nP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
situace	situace	k1gFnSc1	situace
bude	být	k5eAaImBp3nS	být
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jejich	jejich	k3xOp3gFnSc1	jejich
poptávka	poptávka	k1gFnSc1	poptávka
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc4	jeden
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
cenovou	cenový	k2eAgFnSc4d1	cenová
hladinu	hladina	k1gFnSc4	hladina
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
držení	držení	k1gNnSc6	držení
peněžních	peněžní	k2eAgInPc2d1	peněžní
zůstatků	zůstatek	k1gInPc2	zůstatek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
přejí	přát	k5eAaImIp3nP	přát
náhle	náhle	k6eAd1	náhle
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
držet	držet	k5eAaImF	držet
větší	veliký	k2eAgFnSc4d2	veliký
zásobu	zásoba	k1gFnSc4	zásoba
hotovosti	hotovost	k1gFnSc2	hotovost
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
jednotky	jednotka	k1gFnSc2	jednotka
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lidé	člověk	k1gMnPc1	člověk
více	hodně	k6eAd2	hodně
prodávají	prodávat	k5eAaImIp3nP	prodávat
zboží	zboží	k1gNnSc4	zboží
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
ochotni	ochoten	k2eAgMnPc1d1	ochoten
nakupovat	nakupovat	k5eAaBmF	nakupovat
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgInSc1d1	relativní
převis	převis	k1gInSc1	převis
nabídky	nabídka	k1gFnSc2	nabídka
nad	nad	k7c7	nad
poptávkou	poptávka	k1gFnSc7	poptávka
sníží	snížit	k5eAaPmIp3nS	snížit
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
pokles	pokles	k1gInSc1	pokles
ochoty	ochota	k1gFnSc2	ochota
lidí	člověk	k1gMnPc2	člověk
držet	držet	k5eAaImF	držet
peníze	peníz	k1gInPc4	peníz
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
inflaci	inflace	k1gFnSc3	inflace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
poptávka	poptávka	k1gFnSc1	poptávka
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
nabídku	nabídka	k1gFnSc4	nabídka
a	a	k8xC	a
ceny	cena	k1gFnPc4	cena
všeobecně	všeobecně	k6eAd1	všeobecně
rostou	růst	k5eAaImIp3nP	růst
<g/>
.	.	kIx.	.
</s>
<s>
Drobné	drobný	k2eAgFnPc1d1	drobná
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
poptávce	poptávka	k1gFnSc6	poptávka
po	po	k7c6	po
penězích	peníze	k1gInPc6	peníze
obvykle	obvykle	k6eAd1	obvykle
nejsou	být	k5eNaImIp3nP	být
příliš	příliš	k6eAd1	příliš
významné	významný	k2eAgFnPc1d1	významná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
legislativní	legislativní	k2eAgNnSc4d1	legislativní
vymezení	vymezení	k1gNnSc4	vymezení
zákonného	zákonný	k2eAgNnSc2d1	zákonné
platidla	platidlo	k1gNnSc2	platidlo
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
lidem	člověk	k1gMnPc3	člověk
používat	používat	k5eAaImF	používat
jinou	jiný	k2eAgFnSc4d1	jiná
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
s	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
svou	svůj	k3xOyFgFnSc7	svůj
nespokojeni	spokojen	k2eNgMnPc1d1	nespokojen
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ale	ale	k8xC	ale
měnová	měnový	k2eAgFnSc1d1	měnová
politika	politika	k1gFnSc1	politika
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
je	být	k5eAaImIp3nS	být
nezodpovědná	zodpovědný	k2eNgFnSc1d1	nezodpovědná
a	a	k8xC	a
růst	růst	k1gInSc1	růst
množství	množství	k1gNnSc2	množství
peněz	peníze	k1gInPc2	peníze
je	být	k5eAaImIp3nS	být
rychlý	rychlý	k2eAgMnSc1d1	rychlý
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
velkých	velký	k2eAgFnPc2d1	velká
obtíží	obtíž	k1gFnPc2	obtíž
zbavovat	zbavovat	k5eAaImF	zbavovat
svých	svůj	k3xOyFgInPc2	svůj
peněžních	peněžní	k2eAgInPc2d1	peněžní
zůstatků	zůstatek	k1gInPc2	zůstatek
<g/>
,	,	kIx,	,
utíkají	utíkat	k5eAaImIp3nP	utíkat
k	k	k7c3	k
reálným	reálný	k2eAgFnPc3d1	reálná
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
inflace	inflace	k1gFnSc1	inflace
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
fázi	fáze	k1gFnSc6	fáze
hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
naprostému	naprostý	k2eAgNnSc3d1	naprosté
zhroucení	zhroucení	k1gNnSc3	zhroucení
peněžního	peněžní	k2eAgInSc2d1	peněžní
oběhu	oběh	k1gInSc2	oběh
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
naprosto	naprosto	k6eAd1	naprosto
zbavit	zbavit	k5eAaPmF	zbavit
všech	všecek	k3xTgInPc2	všecek
svých	svůj	k3xOyFgInPc2	svůj
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
padá	padat	k5eAaImIp3nS	padat
nezadržitelně	zadržitelně	k6eNd1	zadržitelně
k	k	k7c3	k
nule	nula	k1gFnSc3	nula
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nezanedbatelné	zanedbatelný	k2eNgFnPc4d1	nezanedbatelná
příčiny	příčina	k1gFnPc4	příčina
patří	patřit	k5eAaImIp3nS	patřit
růst	růst	k1gInSc1	růst
cen	cena	k1gFnPc2	cena
materiálových	materiálový	k2eAgInPc2d1	materiálový
zdrojů	zdroj	k1gInPc2	zdroj
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jejich	jejich	k3xOp3gNnSc2	jejich
postupného	postupný	k2eAgNnSc2d1	postupné
vyčerpávání	vyčerpávání	k1gNnSc2	vyčerpávání
a	a	k8xC	a
horší	zlý	k2eAgFnSc2d2	horší
výtěžnosti	výtěžnost	k1gFnSc2	výtěžnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
nabídky	nabídka	k1gFnSc2	nabídka
také	také	k9	také
významně	významně	k6eAd1	významně
působí	působit	k5eAaImIp3nS	působit
populační	populační	k2eAgInSc4d1	populační
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
změna	změna	k1gFnSc1	změna
poměru	poměr	k1gInSc2	poměr
produktivních	produktivní	k2eAgInPc2d1	produktivní
ku	k	k7c3	k
neproduktivním	produktivní	k2eNgInPc3d1	neproduktivní
členům	člen	k1gInPc3	člen
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
<g/>
-li	i	k?	-li
podíl	podíl	k1gInSc1	podíl
neproduktivních	produktivní	k2eNgFnPc2d1	neproduktivní
složek	složka	k1gFnPc2	složka
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
důchodců	důchodce	k1gMnPc2	důchodce
<g/>
)	)	kIx)	)
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
produktivních	produktivní	k2eAgFnPc2d1	produktivní
<g/>
,	,	kIx,	,
při	při	k7c6	při
zanedbání	zanedbání	k1gNnSc6	zanedbání
ostatních	ostatní	k2eAgInPc2d1	ostatní
vlivů	vliv	k1gInPc2	vliv
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
nabídky	nabídka	k1gFnSc2	nabídka
relativně	relativně	k6eAd1	relativně
k	k	k7c3	k
poptávce	poptávka	k1gFnSc3	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
produktivity	produktivita	k1gFnSc2	produktivita
práce	práce	k1gFnSc2	práce
působí	působit	k5eAaImIp3nS	působit
protiinflačně	protiinflačně	k6eAd1	protiinflačně
<g/>
.	.	kIx.	.
</s>
<s>
Trvalý	trvalý	k2eAgInSc1d1	trvalý
vzestup	vzestup	k1gInSc1	vzestup
množství	množství	k1gNnSc2	množství
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
statků	statek	k1gInPc2	statek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
nabízeny	nabízet	k5eAaImNgInP	nabízet
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tatáž	týž	k3xTgFnSc1	týž
peněžní	peněžní	k2eAgFnSc1d1	peněžní
jednotka	jednotka	k1gFnSc1	jednotka
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
koupit	koupit	k5eAaPmF	koupit
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yInSc7	co
rychleji	rychle	k6eAd2	rychle
roste	růst	k5eAaImIp3nS	růst
produktivita	produktivita	k1gFnSc1	produktivita
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
rychleji	rychle	k6eAd2	rychle
roste	růst	k5eAaImIp3nS	růst
kupní	kupní	k2eAgFnSc1d1	kupní
síla	síla	k1gFnSc1	síla
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
před	před	k7c7	před
zřízením	zřízení	k1gNnSc7	zřízení
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
byla	být	k5eAaImAgFnS	být
deflace	deflace	k1gFnSc2	deflace
běžným	běžný	k2eAgInSc7d1	běžný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
celá	celý	k2eAgFnSc1d1	celá
ekonomika	ekonomika	k1gFnSc1	ekonomika
natolik	natolik	k6eAd1	natolik
přizpůsobena	přizpůsobit	k5eAaPmNgFnS	přizpůsobit
aktuální	aktuální	k2eAgFnSc3d1	aktuální
míře	míra	k1gFnSc3	míra
produkce	produkce	k1gFnSc2	produkce
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
jejího	její	k3xOp3gNnSc2	její
skokového	skokový	k2eAgNnSc2d1	skokové
snížení	snížení	k1gNnSc2	snížení
by	by	kYmCp3nP	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
citelnému	citelný	k2eAgInSc3d1	citelný
šoku	šok	k1gInSc3	šok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
makroekonomických	makroekonomický	k2eAgInPc2d1	makroekonomický
ukazatelů	ukazatel	k1gInPc2	ukazatel
propadla	propadlo	k1gNnSc2	propadlo
dolů	dol	k1gInPc2	dol
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
ceně	cena	k1gFnSc6	cena
dezinflace	dezinflace	k1gFnSc2	dezinflace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
nabídky	nabídka	k1gFnSc2	nabídka
působí	působit	k5eAaImIp3nP	působit
výpadky	výpadek	k1gInPc1	výpadek
produkčních	produkční	k2eAgFnPc2d1	produkční
kapacit	kapacita	k1gFnPc2	kapacita
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
přírodních	přírodní	k2eAgFnPc2d1	přírodní
katastrof	katastrofa	k1gFnPc2	katastrofa
<g/>
,	,	kIx,	,
zničení	zničení	k1gNnSc2	zničení
nebo	nebo	k8xC	nebo
dlouhodobého	dlouhodobý	k2eAgInSc2d1	dlouhodobý
výpadku	výpadek	k1gInSc2	výpadek
technické	technický	k2eAgFnSc2d1	technická
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
energetické	energetický	k2eAgFnSc2d1	energetická
a	a	k8xC	a
dopravní	dopravní	k2eAgFnSc2d1	dopravní
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
krátkodobých	krátkodobý	k2eAgFnPc2d1	krátkodobá
příčin	příčina	k1gFnPc2	příčina
inflace	inflace	k1gFnSc2	inflace
můžeme	moct	k5eAaImIp1nP	moct
jmenovat	jmenovat	k5eAaBmF	jmenovat
lokální	lokální	k2eAgFnPc4d1	lokální
přírodní	přírodní	k2eAgFnPc4d1	přírodní
katastrofy	katastrofa	k1gFnPc4	katastrofa
(	(	kIx(	(
<g/>
obnova	obnova	k1gFnSc1	obnova
zničených	zničený	k2eAgInPc2d1	zničený
statků	statek	k1gInPc2	statek
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
skokové	skokový	k2eAgFnPc1d1	skoková
změny	změna	k1gFnPc1	změna
ceny	cena	k1gFnSc2	cena
nějaké	nějaký	k3yIgFnPc1	nějaký
významné	významný	k2eAgFnPc1d1	významná
komodity	komodita	k1gFnPc1	komodita
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
ropa	ropa	k1gFnSc1	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
však	však	k9	však
ekonomika	ekonomika	k1gFnSc1	ekonomika
vždy	vždy	k6eAd1	vždy
vzpamatovává	vzpamatovávat	k5eAaImIp3nS	vzpamatovávat
<g/>
,	,	kIx,	,
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
se	se	k3xPyFc4	se
změně	změna	k1gFnSc3	změna
dostupnosti	dostupnost	k1gFnSc2	dostupnost
suroviny	surovina	k1gFnSc2	surovina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
zotavuje	zotavovat	k5eAaImIp3nS	zotavovat
ze	z	k7c2	z
škod	škoda	k1gFnPc2	škoda
způsobených	způsobený	k2eAgFnPc2d1	způsobená
přírodními	přírodní	k2eAgInPc7d1	přírodní
živly	živel	k1gInPc7	živel
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
takové	takový	k3xDgInPc4	takový
šoky	šok	k1gInPc4	šok
neovlivňují	ovlivňovat	k5eNaImIp3nP	ovlivňovat
cenovou	cenový	k2eAgFnSc7d1	cenová
hladinou	hladina	k1gFnSc7	hladina
trvale	trvale	k6eAd1	trvale
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
krátkodobě	krátkodobě	k6eAd1	krátkodobě
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
účinek	účinek	k1gInSc1	účinek
na	na	k7c4	na
inflaci	inflace	k1gFnSc4	inflace
nemůže	moct	k5eNaImIp3nS	moct
mít	mít	k5eAaImF	mít
ani	ani	k8xC	ani
růst	růst	k1gInSc4	růst
spotřebitelských	spotřebitelský	k2eAgInPc2d1	spotřebitelský
výdajů	výdaj	k1gInPc2	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
proveditelné	proveditelný	k2eAgNnSc1d1	proveditelné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lidé	člověk	k1gMnPc1	člověk
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vydávali	vydávat	k5eAaPmAgMnP	vydávat
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
kolik	kolik	k9	kolik
činí	činit	k5eAaImIp3nS	činit
jejich	jejich	k3xOp3gInSc4	jejich
příjem	příjem	k1gInSc4	příjem
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
brzy	brzy	k6eAd1	brzy
jejich	jejich	k3xOp3gFnSc1	jejich
poptávka	poptávka	k1gFnSc1	poptávka
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
na	na	k7c4	na
udržitelnou	udržitelný	k2eAgFnSc4d1	udržitelná
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
názoru	názor	k1gInSc6	názor
na	na	k7c4	na
inflaci	inflace	k1gFnSc4	inflace
se	se	k3xPyFc4	se
různé	různý	k2eAgFnPc1d1	různá
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
školy	škola	k1gFnPc1	škola
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Keynesovští	Keynesovský	k2eAgMnPc1d1	Keynesovský
ekonomové	ekonom	k1gMnPc1	ekonom
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Maynard	Maynard	k1gMnSc1	Maynard
Keynes	Keynes	k1gMnSc1	Keynes
-	-	kIx~	-
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
obvykle	obvykle	k6eAd1	obvykle
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
podřizují	podřizovat	k5eAaImIp3nP	podřizovat
cíli	cíl	k1gInSc3	cíl
plné	plný	k2eAgFnSc2d1	plná
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
(	(	kIx(	(
<g/>
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
krizi	krize	k1gFnSc4	krize
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právě	právě	k9	právě
manipulace	manipulace	k1gFnSc1	manipulace
(	(	kIx(	(
<g/>
zvyšování	zvyšování	k1gNnSc1	zvyšování
<g/>
)	)	kIx)	)
s	s	k7c7	s
cenovou	cenový	k2eAgFnSc7d1	cenová
hladinou	hladina	k1gFnSc7	hladina
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
užitečných	užitečný	k2eAgInPc2d1	užitečný
nástrojů	nástroj	k1gInPc2	nástroj
zvyšování	zvyšování	k1gNnSc2	zvyšování
výkonu	výkon	k1gInSc2	výkon
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
negativními	negativní	k2eAgInPc7d1	negativní
následky	následek	k1gInPc7	následek
jejich	jejich	k3xOp3gInPc2	jejich
přístupu	přístup	k1gInSc2	přístup
varují	varovat	k5eAaImIp3nP	varovat
monetaristé	monetarista	k1gMnPc1	monetarista
(	(	kIx(	(
<g/>
Milton	Milton	k1gInSc1	Milton
Friedman	Friedman	k1gMnSc1	Friedman
-	-	kIx~	-
poradce	poradce	k1gMnSc1	poradce
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
preferují	preferovat	k5eAaImIp3nP	preferovat
stabilní	stabilní	k2eAgFnSc4d1	stabilní
mírnou	mírný	k2eAgFnSc4d1	mírná
inflaci	inflace	k1gFnSc4	inflace
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
potřebu	potřeba	k1gFnSc4	potřeba
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
množství	množství	k1gNnSc4	množství
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
růst	růst	k1gInSc1	růst
objemu	objem	k1gInSc2	objem
zboží	zboží	k1gNnSc2	zboží
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
celková	celkový	k2eAgFnSc1d1	celková
cenová	cenový	k2eAgFnSc1d1	cenová
hladina	hladina	k1gFnSc1	hladina
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
neměnila	měnit	k5eNaImAgFnS	měnit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
praktických	praktický	k2eAgInPc6d1	praktický
případech	případ	k1gInPc6	případ
znamená	znamenat	k5eAaImIp3nS	znamenat
její	její	k3xOp3gNnSc1	její
mírný	mírný	k2eAgInSc1d1	mírný
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomové	ekonom	k1gMnPc1	ekonom
rakouské	rakouský	k2eAgFnSc2d1	rakouská
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
oběma	dva	k4xCgInPc7	dva
předchozími	předchozí	k2eAgInPc7d1	předchozí
anglosaskými	anglosaský	k2eAgInPc7d1	anglosaský
soudí	soudit	k5eAaImIp3nP	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
manipulace	manipulace	k1gFnSc1	manipulace
s	s	k7c7	s
úroky	úrok	k1gInPc7	úrok
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
peněz	peníze	k1gInPc2	peníze
může	moct	k5eAaImIp3nS	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
cyklus	cyklus	k1gInSc4	cyklus
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
požadují	požadovat	k5eAaImIp3nP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vláda	vláda	k1gFnSc1	vláda
inflaci	inflace	k1gFnSc4	inflace
neprováděla	provádět	k5eNaImAgFnS	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
<g/>
-li	i	k?	-li
celková	celkový	k2eAgFnSc1d1	celková
úroveň	úroveň	k1gFnSc1	úroveň
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
za	za	k7c4	za
peněžní	peněžní	k2eAgFnSc4d1	peněžní
jednotku	jednotka	k1gFnSc4	jednotka
koupit	koupit	k5eAaPmF	koupit
méně	málo	k6eAd2	málo
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Dopady	dopad	k1gInPc1	dopad
inflace	inflace	k1gFnSc2	inflace
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
rozloženy	rozložit	k5eAaPmNgInP	rozložit
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
někteří	některý	k3yIgMnPc1	některý
účastníci	účastník	k1gMnPc1	účastník
na	na	k7c4	na
inflaci	inflace	k1gFnSc4	inflace
tratí	trať	k1gFnPc2	trať
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
získávají	získávat	k5eAaImIp3nP	získávat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
na	na	k7c4	na
inflaci	inflace	k1gFnSc4	inflace
získají	získat	k5eAaPmIp3nP	získat
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
k	k	k7c3	k
nově	nově	k6eAd1	nově
vzniklým	vzniklý	k2eAgInPc3d1	vzniklý
penězům	peníze	k1gInPc3	peníze
dostanou	dostat	k5eAaPmIp3nP	dostat
nejdříve	dříve	k6eAd3	dříve
<g/>
;	;	kIx,	;
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
dostanou	dostat	k5eAaPmIp3nP	dostat
nejpozději	pozdě	k6eAd3	pozdě
<g/>
,	,	kIx,	,
na	na	k7c4	na
inflaci	inflace	k1gFnSc4	inflace
tratí	trať	k1gFnPc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
inflace	inflace	k1gFnSc1	inflace
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
emisí	emise	k1gFnSc7	emise
státních	státní	k2eAgInPc2d1	státní
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
stát	stát	k1gInSc1	stát
tak	tak	k6eAd1	tak
získá	získat	k5eAaPmIp3nS	získat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
inflaci	inflace	k1gFnSc6	inflace
mohou	moct	k5eAaImIp3nP	moct
vlastníci	vlastník	k1gMnPc1	vlastník
nepeněžních	peněžní	k2eNgNnPc2d1	nepeněžní
aktiv	aktivum	k1gNnPc2	aktivum
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
nemovitosti	nemovitost	k1gFnPc4	nemovitost
<g/>
,	,	kIx,	,
akcie	akcie	k1gFnPc4	akcie
nebo	nebo	k8xC	nebo
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnota	hodnota	k1gFnSc1	hodnota
aktiv	aktivum	k1gNnPc2	aktivum
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
je	on	k3xPp3gNnPc4	on
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
muset	muset	k5eAaImF	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Reálný	reálný	k2eAgInSc1d1	reálný
příjem	příjem	k1gInSc1	příjem
jedince	jedinec	k1gMnSc2	jedinec
bude	být	k5eAaImBp3nS	být
záviset	záviset	k5eAaImF	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
do	do	k7c2	do
jaké	jaký	k3yIgFnSc2	jaký
míry	míra	k1gFnSc2	míra
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
příjem	příjem	k1gInSc1	příjem
fixován	fixován	k2eAgInSc1d1	fixován
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
příjmy	příjem	k1gInPc1	příjem
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
důchodců	důchodce	k1gMnPc2	důchodce
často	často	k6eAd1	často
zaostávají	zaostávat	k5eAaImIp3nP	zaostávat
za	za	k7c7	za
inflací	inflace	k1gFnSc7	inflace
a	a	k8xC	a
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
osob	osoba	k1gFnPc2	osoba
je	být	k5eAaImIp3nS	být
výše	výše	k1gFnSc1	výše
příjmu	příjem	k1gInSc2	příjem
pevně	pevně	k6eAd1	pevně
daná	daný	k2eAgFnSc1d1	daná
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
držitelé	držitel	k1gMnPc1	držitel
hotovosti	hotovost	k1gFnSc2	hotovost
zaznamenají	zaznamenat	k5eAaPmIp3nP	zaznamenat
pokles	pokles	k1gInSc4	pokles
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Neočekávané	očekávaný	k2eNgNnSc1d1	neočekávané
zvýšení	zvýšení	k1gNnSc1	zvýšení
míry	míra	k1gFnSc2	míra
inflace	inflace	k1gFnSc2	inflace
povede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
reálné	reálný	k2eAgFnSc2d1	reálná
úrokové	úrokový	k2eAgFnSc2d1	úroková
míry	míra	k1gFnSc2	míra
<g/>
.	.	kIx.	.
</s>
<s>
Dlužníci	dlužník	k1gMnPc1	dlužník
splácející	splácející	k2eAgMnPc1d1	splácející
dluhy	dluh	k1gInPc4	dluh
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
nominální	nominální	k2eAgFnSc7d1	nominální
úrokovou	úrokový	k2eAgFnSc7d1	úroková
sazbou	sazba	k1gFnSc7	sazba
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
měrou	míra	k1gFnSc7wR	míra
inflace	inflace	k1gFnSc2	inflace
zaznamenají	zaznamenat	k5eAaPmIp3nP	zaznamenat
snížení	snížení	k1gNnPc4	snížení
reálné	reálný	k2eAgFnSc2d1	reálná
úrokové	úrokový	k2eAgFnSc2d1	úroková
sazby	sazba	k1gFnSc2	sazba
<g/>
.	.	kIx.	.
</s>
<s>
Klesající	klesající	k2eAgFnSc1d1	klesající
reálná	reálný	k2eAgFnSc1d1	reálná
úroková	úrokový	k2eAgFnSc1d1	úroková
míra	míra	k1gFnSc1	míra
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
výhodné	výhodný	k2eAgNnSc1d1	výhodné
spořit	spořit	k5eAaImF	spořit
a	a	k8xC	a
více	hodně	k6eAd2	hodně
výhodné	výhodný	k2eAgNnSc1d1	výhodné
utrácet	utrácet	k5eAaImF	utrácet
a	a	k8xC	a
zadlužovat	zadlužovat	k5eAaImF	zadlužovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Banky	banka	k1gFnPc1	banka
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
věřitelé	věřitel	k1gMnPc1	věřitel
předcházejí	předcházet	k5eAaImIp3nP	předcházet
tomuto	tento	k3xDgNnSc3	tento
riziku	riziko	k1gNnSc3	riziko
snížení	snížení	k1gNnSc1	snížení
reálné	reálný	k2eAgFnSc2d1	reálná
úrokové	úrokový	k2eAgFnSc2d1	úroková
míry	míra	k1gFnSc2	míra
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
buď	buď	k8xC	buď
přičítají	přičítat	k5eAaImIp3nP	přičítat
k	k	k7c3	k
fixním	fixní	k2eAgFnPc3d1	fixní
úrokovým	úrokový	k2eAgFnPc3d1	úroková
sazbám	sazba	k1gFnPc3	sazba
úvěrů	úvěr	k1gInPc2	úvěr
inflační	inflační	k2eAgFnSc4d1	inflační
rizikovou	rizikový	k2eAgFnSc4d1	riziková
prémii	prémie	k1gFnSc4	prémie
nebo	nebo	k8xC	nebo
vážou	vázat	k5eAaImIp3nP	vázat
úrokovou	úrokový	k2eAgFnSc4d1	úroková
sazbu	sazba	k1gFnSc4	sazba
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
nebo	nebo	k8xC	nebo
nepředvídatelná	předvídatelný	k2eNgFnSc1d1	nepředvídatelná
inflace	inflace	k1gFnSc1	inflace
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
škodlivou	škodlivý	k2eAgFnSc4d1	škodlivá
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
neefektivní	efektivní	k2eNgNnSc1d1	neefektivní
fungování	fungování	k1gNnSc1	fungování
trhů	trh	k1gInPc2	trh
a	a	k8xC	a
znesnadňuje	znesnadňovat	k5eAaImIp3nS	znesnadňovat
firmám	firma	k1gFnPc3	firma
sestavování	sestavování	k1gNnSc6	sestavování
dlouhodobých	dlouhodobý	k2eAgInPc2d1	dlouhodobý
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
cenových	cenový	k2eAgFnPc2d1	cenová
kalkulací	kalkulace	k1gFnPc2	kalkulace
a	a	k8xC	a
rozpočtů	rozpočet	k1gInPc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
brzda	brzda	k1gFnSc1	brzda
na	na	k7c4	na
produktivity	produktivita	k1gFnPc4	produktivita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
společnosti	společnost	k1gFnPc1	společnost
jsou	být	k5eAaImIp3nP	být
nuceny	nucen	k2eAgFnPc1d1	nucena
přesunout	přesunout	k5eAaPmF	přesunout
zdroje	zdroj	k1gInPc4	zdroj
od	od	k7c2	od
poskytování	poskytování	k1gNnSc2	poskytování
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
k	k	k7c3	k
finančním	finanční	k2eAgFnPc3d1	finanční
operacím	operace	k1gFnPc3	operace
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
ztrátám	ztráta	k1gFnPc3	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Nejistota	nejistota	k1gFnSc1	nejistota
ohledně	ohledně	k7c2	ohledně
budoucí	budoucí	k2eAgFnSc2d1	budoucí
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
peněz	peníze	k1gInPc2	peníze
odrazuje	odrazovat	k5eAaImIp3nS	odrazovat
od	od	k7c2	od
investování	investování	k1gNnSc2	investování
a	a	k8xC	a
spoření	spoření	k1gNnSc2	spoření
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
taxflaci	taxflace	k1gFnSc4	taxflace
-	-	kIx~	-
skryté	skrytý	k2eAgNnSc4d1	skryté
zvyšování	zvyšování	k1gNnSc4	zvyšování
zdanění	zdanění	k1gNnSc2	zdanění
<g/>
,	,	kIx,	,
když	když	k8xS	když
nominálně	nominálně	k6eAd1	nominálně
vyšší	vysoký	k2eAgInPc1d2	vyšší
příjmy	příjem	k1gInPc1	příjem
posunují	posunovat	k5eAaImIp3nP	posunovat
daňové	daňový	k2eAgMnPc4d1	daňový
poplatníky	poplatník	k1gMnPc4	poplatník
do	do	k7c2	do
vyšších	vysoký	k2eAgFnPc2d2	vyšší
pásem	pásmo	k1gNnPc2	pásmo
sazby	sazba	k1gFnSc2	sazba
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Mzdově	mzdově	k6eAd1	mzdově
cenová	cenový	k2eAgFnSc1d1	cenová
spirála	spirála	k1gFnSc1	spirála
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
inflace	inflace	k1gFnSc1	inflace
vede	vést	k5eAaImIp3nS	vést
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
požadují	požadovat	k5eAaImIp3nP	požadovat
rychlejší	rychlý	k2eAgNnSc4d2	rychlejší
zvyšování	zvyšování	k1gNnSc4	zvyšování
mezd	mzda	k1gFnPc2	mzda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
kolektivního	kolektivní	k2eAgNnSc2d1	kolektivní
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
bude	být	k5eAaImBp3nS	být
růst	růst	k5eAaImF	růst
mezd	mzda	k1gFnPc2	mzda
záviset	záviset	k5eAaImF	záviset
na	na	k7c6	na
inflačních	inflační	k2eAgNnPc6d1	inflační
očekáváních	očekávání	k1gNnPc6	očekávání
<g/>
;	;	kIx,	;
ta	ten	k3xDgNnPc1	ten
budou	být	k5eAaImBp3nP	být
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
inflace	inflace	k1gFnSc1	inflace
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
mzdovou	mzdový	k2eAgFnSc4d1	mzdová
spirálu	spirála	k1gFnSc4	spirála
-	-	kIx~	-
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
vazbu	vazba	k1gFnSc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Stručně	stručně	k6eAd1	stručně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
inflace	inflace	k1gFnSc1	inflace
plodí	plodit	k5eAaImIp3nS	plodit
inflační	inflační	k2eAgNnSc4d1	inflační
očekávání	očekávání	k1gNnSc4	očekávání
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
plodí	plodit	k5eAaImIp3nS	plodit
další	další	k2eAgFnSc4d1	další
inflaci	inflace	k1gFnSc4	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Hromadění	hromadění	k1gNnSc1	hromadění
zboží	zboží	k1gNnSc4	zboží
Lidé	člověk	k1gMnPc1	člověk
nakupují	nakupovat	k5eAaBmIp3nP	nakupovat
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
uchování	uchování	k1gNnSc2	uchování
bohatství	bohatství	k1gNnSc2	bohatství
trvanlivé	trvanlivý	k2eAgNnSc4d1	trvanlivé
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
tohoto	tento	k3xDgNnSc2	tento
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
plýtvání	plýtvání	k1gNnSc2	plýtvání
<g/>
.	.	kIx.	.
</s>
<s>
Hyperinflace	hyperinflace	k1gFnPc1	hyperinflace
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
inflace	inflace	k1gFnSc1	inflace
dostane	dostat	k5eAaPmIp3nS	dostat
zcela	zcela	k6eAd1	zcela
mimo	mimo	k7c4	mimo
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
silně	silně	k6eAd1	silně
narušeno	narušit	k5eAaPmNgNnS	narušit
fungování	fungování	k1gNnSc1	fungování
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
poškozena	poškozen	k2eAgFnSc1d1	poškozena
její	její	k3xOp3gFnSc4	její
schopnost	schopnost	k1gFnSc4	schopnost
vyrábět	vyrábět	k5eAaImF	vyrábět
a	a	k8xC	a
dodávat	dodávat	k5eAaImF	dodávat
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Hyperinflace	hyperinflace	k1gFnSc1	hyperinflace
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
nahrazení	nahrazení	k1gNnSc3	nahrazení
národní	národní	k2eAgFnSc2d1	národní
měny	měna	k1gFnSc2	měna
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
měnou	měna	k1gFnSc7	měna
<g/>
,	,	kIx,	,
lokálními	lokální	k2eAgFnPc7d1	lokální
měnami	měna	k1gFnPc7	měna
nebo	nebo	k8xC	nebo
k	k	k7c3	k
částečnému	částečný	k2eAgMnSc3d1	částečný
či	či	k8xC	či
úplnému	úplný	k2eAgInSc3d1	úplný
přechodu	přechod	k1gInSc3	přechod
na	na	k7c4	na
směnný	směnný	k2eAgInSc4d1	směnný
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Alokační	alokační	k2eAgFnSc1d1	alokační
neefektivnost	neefektivnost	k1gFnSc1	neefektivnost
Změna	změna	k1gFnSc1	změna
nabídky	nabídka	k1gFnSc2	nabídka
nebo	nebo	k8xC	nebo
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c4	po
zboží	zboží	k1gNnSc4	zboží
obvykle	obvykle	k6eAd1	obvykle
způsobí	způsobit	k5eAaPmIp3nS	způsobit
změnu	změna	k1gFnSc4	změna
jeho	jeho	k3xOp3gFnSc2	jeho
tržní	tržní	k2eAgFnSc2d1	tržní
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
spotřebitelům	spotřebitel	k1gMnPc3	spotřebitel
a	a	k8xC	a
výrobcům	výrobce	k1gMnPc3	výrobce
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
nové	nový	k2eAgFnPc4d1	nová
tržní	tržní	k2eAgFnPc4d1	tržní
podmínky	podmínka	k1gFnPc4	podmínka
přerozdělením	přerozdělení	k1gNnSc7	přerozdělení
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ceny	cena	k1gFnPc1	cena
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
inflace	inflace	k1gFnSc2	inflace
neustále	neustále	k6eAd1	neustále
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
odlišit	odlišit	k5eAaPmF	odlišit
změny	změna	k1gFnPc4	změna
cen	cena	k1gFnPc2	cena
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změn	změna	k1gFnPc2	změna
nabídky	nabídka	k1gFnSc2	nabídka
a	a	k8xC	a
poptávky	poptávka	k1gFnSc2	poptávka
od	od	k7c2	od
změn	změna	k1gFnPc2	změna
cen	cena	k1gFnPc2	cena
způsobených	způsobený	k2eAgFnPc2d1	způsobená
všeobecnou	všeobecný	k2eAgFnSc7d1	všeobecná
inflací	inflace	k1gFnSc7	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
trhu	trh	k1gInSc2	trh
tak	tak	k6eAd1	tak
reagují	reagovat	k5eAaBmIp3nP	reagovat
předčasně	předčasně	k6eAd1	předčasně
<g/>
,	,	kIx,	,
opožděně	opožděně	k6eAd1	opožděně
nebo	nebo	k8xC	nebo
nepřiměřeně	přiměřeně	k6eNd1	přiměřeně
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
neefektivní	efektivní	k2eNgNnSc1d1	neefektivní
vynaložení	vynaložení	k1gNnSc1	vynaložení
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Přesun	přesun	k1gInSc1	přesun
bohatství	bohatství	k1gNnSc2	bohatství
Inflace	inflace	k1gFnSc1	inflace
nepůsobí	působit	k5eNaImIp3nP	působit
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
subjekty	subjekt	k1gInPc4	subjekt
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
cen	cena	k1gFnPc2	cena
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
určitých	určitý	k2eAgNnPc6d1	určité
odvětvích	odvětví	k1gNnPc6	odvětví
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
příčin	příčina	k1gFnPc2	příčina
inflace	inflace	k1gFnSc2	inflace
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
ta	ten	k3xDgNnPc4	ten
odvětví	odvětví	k1gNnPc4	odvětví
<g/>
,	,	kIx,	,
skrz	skrz	k6eAd1	skrz
která	který	k3yQgFnSc1	který
do	do	k7c2	do
ekonomiky	ekonomika	k1gFnSc2	ekonomika
proudí	proudit	k5eAaPmIp3nP	proudit
nové	nový	k2eAgInPc1d1	nový
peníze	peníz	k1gInPc1	peníz
z	z	k7c2	z
úvěrů	úvěr	k1gInPc2	úvěr
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Odvětví	odvětví	k1gNnPc1	odvětví
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
zasažena	zasažen	k2eAgNnPc1d1	zasaženo
jako	jako	k8xC	jako
první	první	k4xOgFnPc1	první
<g/>
,	,	kIx,	,
nakupují	nakupovat	k5eAaBmIp3nP	nakupovat
za	za	k7c4	za
staré	starý	k2eAgFnPc4d1	stará
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gMnPc1	jejich
zákazníci	zákazník	k1gMnPc1	zákazník
jim	on	k3xPp3gMnPc3	on
již	již	k9	již
platí	platit	k5eAaImIp3nP	platit
nové	nový	k2eAgFnPc1d1	nová
(	(	kIx(	(
<g/>
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
)	)	kIx)	)
ceny	cena	k1gFnPc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Efektivně	efektivně	k6eAd1	efektivně
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přesunu	přesun	k1gInSc2	přesun
bohatství	bohatství	k1gNnSc2	bohatství
od	od	k7c2	od
jedněch	jeden	k4xCgFnPc2	jeden
subjektů	subjekt	k1gInPc2	subjekt
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
cen	cena	k1gFnPc2	cena
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
postupně	postupně	k6eAd1	postupně
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
skrz	skrz	k7c4	skrz
celou	celý	k2eAgFnSc4d1	celá
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
ceny	cena	k1gFnSc2	cena
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
odvětvích	odvětví	k1gNnPc6	odvětví
nedosáhnou	dosáhnout	k5eNaPmIp3nP	dosáhnout
nové	nový	k2eAgFnPc1d1	nová
úrovně	úroveň	k1gFnPc1	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
odřených	odřený	k2eAgFnPc2d1	odřená
podrážek	podrážka	k1gFnPc2	podrážka
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
inflace	inflace	k1gFnSc1	inflace
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
náklady	náklad	k1gInPc4	náklad
příležitosti	příležitost	k1gFnSc2	příležitost
z	z	k7c2	z
držby	držba	k1gFnSc2	držba
peněžních	peněžní	k2eAgInPc2d1	peněžní
zůstatků	zůstatek	k1gInPc2	zůstatek
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
přimět	přimět	k5eAaPmF	přimět
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přesouvali	přesouvat	k5eAaImAgMnP	přesouvat
svá	svůj	k3xOyFgNnPc4	svůj
aktiva	aktivum	k1gNnPc4	aktivum
na	na	k7c4	na
termínované	termínovaný	k2eAgInPc4d1	termínovaný
vklady	vklad	k1gInPc4	vklad
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
hotovost	hotovost	k1gFnSc4	hotovost
k	k	k7c3	k
životu	život	k1gInSc3	život
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
více	hodně	k6eAd2	hodně
"	"	kIx"	"
<g/>
pochůzek	pochůzka	k1gFnPc2	pochůzka
do	do	k7c2	do
banky	banka	k1gFnSc2	banka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
každá	každý	k3xTgFnSc1	každý
taková	takový	k3xDgFnSc1	takový
cesta	cesta	k1gFnSc1	cesta
"	"	kIx"	"
<g/>
opotřebovává	opotřebovávat	k5eAaImIp3nS	opotřebovávat
podrážky	podrážka	k1gFnPc4	podrážka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
jídelníčku	jídelníček	k1gInSc2	jídelníček
(	(	kIx(	(
<g/>
přeceňovací	přeceňovací	k2eAgInPc4d1	přeceňovací
náklady	náklad	k1gInPc4	náklad
<g/>
)	)	kIx)	)
Za	za	k7c2	za
vysoké	vysoký	k2eAgFnSc2d1	vysoká
inflace	inflace	k1gFnSc2	inflace
musí	muset	k5eAaImIp3nP	muset
výrobci	výrobce	k1gMnPc1	výrobce
často	často	k6eAd1	často
měnit	měnit	k5eAaImF	měnit
své	svůj	k3xOyFgFnPc4	svůj
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nese	nést	k5eAaImIp3nS	nést
náklady	náklad	k1gInPc1	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
cyklus	cyklus	k1gInSc4	cyklus
Podle	podle	k7c2	podle
rakouské	rakouský	k2eAgFnSc2d1	rakouská
teorie	teorie	k1gFnSc2	teorie
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
cyklu	cyklus	k1gInSc2	cyklus
je	být	k5eAaImIp3nS	být
inflace	inflace	k1gFnSc1	inflace
důsledkem	důsledek	k1gInSc7	důsledek
uvolnění	uvolnění	k1gNnSc2	uvolnění
monetární	monetární	k2eAgFnSc2d1	monetární
politiky	politika	k1gFnSc2	politika
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
úvěrové	úvěrový	k2eAgFnSc2d1	úvěrová
expanze	expanze	k1gFnSc2	expanze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
"	"	kIx"	"
<g/>
roztáčí	roztáčet	k5eAaImIp3nP	roztáčet
<g/>
"	"	kIx"	"
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
nízké	nízký	k2eAgFnPc1d1	nízká
úrokové	úrokový	k2eAgFnPc1d1	úroková
sazby	sazba	k1gFnPc1	sazba
a	a	k8xC	a
související	související	k2eAgNnSc1d1	související
zvyšování	zvyšování	k1gNnSc1	zvyšování
nabídky	nabídka	k1gFnSc2	nabídka
peněz	peníze	k1gInPc2	peníze
podporují	podporovat	k5eAaImIp3nP	podporovat
úvěry	úvěr	k1gInPc1	úvěr
na	na	k7c4	na
nové	nový	k2eAgFnPc4d1	nová
investice	investice	k1gFnPc4	investice
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
by	by	kYmCp3nP	by
v	v	k7c6	v
přirozených	přirozený	k2eAgFnPc6d1	přirozená
podmínkách	podmínka	k1gFnPc6	podmínka
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
peněžní	peněžní	k2eAgFnSc2d1	peněžní
nabídky	nabídka	k1gFnSc2	nabídka
skrze	skrze	k?	skrze
nové	nový	k2eAgInPc4d1	nový
úvěry	úvěr	k1gInPc4	úvěr
po	po	k7c6	po
určitém	určitý	k2eAgInSc6d1	určitý
čase	čas	k1gInSc6	čas
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
inflaci	inflace	k1gFnSc4	inflace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
"	"	kIx"	"
<g/>
sežere	sežrat	k5eAaPmIp3nS	sežrat
<g/>
"	"	kIx"	"
ziskovost	ziskovost	k1gFnSc1	ziskovost
některých	některý	k3yIgInPc2	některý
nových	nový	k2eAgInPc2d1	nový
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
ukáží	ukázat	k5eAaPmIp3nP	ukázat
být	být	k5eAaImF	být
jako	jako	k9	jako
špatné	špatný	k2eAgNnSc4d1	špatné
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
se	s	k7c7	s
ztrátami	ztráta	k1gFnPc7	ztráta
odepsány	odepsán	k2eAgFnPc1d1	odepsána
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
pružnost	pružnost	k1gFnSc1	pružnost
trhu	trh	k1gInSc2	trh
práce	práce	k1gFnSc2	práce
Keynesiánci	keynesiánec	k1gMnPc1	keynesiánec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nominální	nominální	k2eAgFnSc2d1	nominální
mzdy	mzda	k1gFnSc2	mzda
se	se	k3xPyFc4	se
jen	jen	k9	jen
neochotně	ochotně	k6eNd1	ochotně
mění	měnit	k5eAaImIp3nS	měnit
směrem	směr	k1gInSc7	směr
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
prodloužení	prodloužení	k1gNnSc3	prodloužení
období	období	k1gNnSc2	období
nerovnováhy	nerovnováha	k1gFnSc2	nerovnováha
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc2d1	vysoká
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
inflace	inflace	k1gFnSc1	inflace
sníží	snížit	k5eAaPmIp3nS	snížit
reálné	reálný	k2eAgFnPc4d1	reálná
mzdy	mzda	k1gFnPc4	mzda
<g/>
,	,	kIx,	,
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
<g/>
-li	i	k?	-li
nominální	nominální	k2eAgFnSc2d1	nominální
mzdy	mzda	k1gFnSc2	mzda
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
,	,	kIx,	,
keynesiánci	keynesiánec	k1gMnPc1	keynesiánec
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mírná	mírný	k2eAgFnSc1d1	mírná
inflace	inflace	k1gFnSc1	inflace
prospívá	prospívat	k5eAaImIp3nS	prospívat
ekonomice	ekonomika	k1gFnSc3	ekonomika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
trh	trh	k1gInSc1	trh
práce	práce	k1gFnSc2	práce
rychleji	rychle	k6eAd2	rychle
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Manévrovací	manévrovací	k2eAgInSc1d1	manévrovací
prostor	prostor	k1gInSc1	prostor
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
Primárním	primární	k2eAgInSc7d1	primární
nástrojem	nástroj	k1gInSc7	nástroj
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
nabídky	nabídka	k1gFnSc2	nabídka
peněz	peníze	k1gInPc2	peníze
je	být	k5eAaImIp3nS	být
diskontní	diskontní	k2eAgFnSc1d1	diskontní
sazba	sazba	k1gFnSc1	sazba
<g/>
,	,	kIx,	,
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
komerční	komerční	k2eAgInPc4d1	komerční
banky	bank	k1gInPc4	bank
půjčovat	půjčovat	k5eAaImF	půjčovat
od	od	k7c2	od
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
a	a	k8xC	a
operace	operace	k1gFnPc1	operace
na	na	k7c6	na
volném	volný	k2eAgInSc6d1	volný
trhu	trh	k1gInSc6	trh
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nákupy	nákup	k1gInPc4	nákup
a	a	k8xC	a
prodeje	prodej	k1gInPc4	prodej
dluhopisů	dluhopis	k1gInPc2	dluhopis
centrální	centrální	k2eAgFnSc7d1	centrální
bankou	banka	k1gFnSc7	banka
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
nominální	nominální	k2eAgFnPc4d1	nominální
úrokové	úrokový	k2eAgFnPc4d1	úroková
sazby	sazba	k1gFnPc4	sazba
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c4	v
recesi	recese	k1gFnSc4	recese
a	a	k8xC	a
nominální	nominální	k2eAgFnPc4d1	nominální
úrokové	úrokový	k2eAgFnPc4d1	úroková
sazby	sazba	k1gFnPc4	sazba
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
nule	nula	k1gFnSc3	nula
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
už	už	k9	už
banka	banka	k1gFnSc1	banka
nemůže	moct	k5eNaImIp3nS	moct
snížit	snížit	k5eAaPmF	snížit
tyto	tento	k3xDgFnPc4	tento
sazby	sazba	k1gFnPc4	sazba
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
stimulovat	stimulovat	k5eAaImF	stimulovat
ekonomiku	ekonomika	k1gFnSc4	ekonomika
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
past	past	k1gFnSc1	past
na	na	k7c4	na
likviditu	likvidita	k1gFnSc4	likvidita
<g/>
.	.	kIx.	.
</s>
<s>
Mírná	mírný	k2eAgFnSc1d1	mírná
inflace	inflace	k1gFnSc1	inflace
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nominální	nominální	k2eAgFnPc4d1	nominální
úrokové	úrokový	k2eAgFnPc4d1	úroková
sazby	sazba	k1gFnPc4	sazba
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
dostatečně	dostatečně	k6eAd1	dostatečně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
mohla	moct	k5eAaImAgFnS	moct
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
nominální	nominální	k2eAgFnSc2d1	nominální
úrokové	úrokový	k2eAgFnSc2d1	úroková
sazby	sazba	k1gFnSc2	sazba
snížit	snížit	k5eAaPmF	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
udržení	udržení	k1gNnSc4	udržení
inflace	inflace	k1gFnSc2	inflace
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
různé	různý	k2eAgFnPc1d1	různá
metody	metoda	k1gFnPc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měnová	měnový	k2eAgFnSc1d1	měnová
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Primárním	primární	k2eAgInSc7d1	primární
nástrojem	nástroj	k1gInSc7	nástroj
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
inflace	inflace	k1gFnSc2	inflace
je	být	k5eAaImIp3nS	být
měnová	měnový	k2eAgFnSc1d1	měnová
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
centrálních	centrální	k2eAgFnPc2d1	centrální
bank	banka	k1gFnPc2	banka
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
udržovat	udržovat	k5eAaImF	udržovat
mezibankovní	mezibankovní	k2eAgFnSc4d1	mezibankovní
výpůjční	výpůjční	k2eAgFnSc4d1	výpůjční
sazbu	sazba	k1gFnSc4	sazba
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
mezi	mezi	k7c7	mezi
2	[number]	k4	2
a	a	k8xC	a
3	[number]	k4	3
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
uvnitř	uvnitř	k7c2	uvnitř
intervalu	interval	k1gInSc2	interval
inflačního	inflační	k2eAgInSc2d1	inflační
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
mezi	mezi	k7c7	mezi
2	[number]	k4	2
a	a	k8xC	a
6	[number]	k4	6
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
však	však	k9	však
PRIBOR	PRIBOR	kA	PRIBOR
pod	pod	k7c7	pod
1	[number]	k4	1
%	%	kIx~	%
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
nízká	nízký	k2eAgFnSc1d1	nízká
kladná	kladný	k2eAgFnSc1d1	kladná
inflace	inflace	k1gFnSc1	inflace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
deflace	deflace	k1gFnSc1	deflace
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yRnSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
za	za	k7c4	za
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
navrženy	navrhnout	k5eAaPmNgFnP	navrhnout
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
inflace	inflace	k1gFnSc2	inflace
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnPc1d1	centrální
banky	banka	k1gFnPc1	banka
mohou	moct	k5eAaImIp3nP	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
inflaci	inflace	k1gFnSc4	inflace
ve	v	k7c6	v
významné	významný	k2eAgFnSc6d1	významná
míře	míra	k1gFnSc6	míra
pomocí	pomocí	k7c2	pomocí
stanovení	stanovení	k1gNnSc2	stanovení
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
a	a	k8xC	a
přes	přes	k7c4	přes
jiné	jiný	k2eAgFnPc4d1	jiná
operace	operace	k1gFnPc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
úrokové	úrokový	k2eAgFnPc1d1	úroková
sazby	sazba	k1gFnPc1	sazba
a	a	k8xC	a
pomalý	pomalý	k2eAgInSc1d1	pomalý
růst	růst	k1gInSc1	růst
peněžní	peněžní	k2eAgFnSc2d1	peněžní
zásoby	zásoba	k1gFnSc2	zásoba
jsou	být	k5eAaImIp3nP	být
tradiční	tradiční	k2eAgInPc4d1	tradiční
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
centrální	centrální	k2eAgInPc1d1	centrální
banky	bank	k1gInPc1	bank
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
inflací	inflace	k1gFnSc7	inflace
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
prevenci	prevence	k1gFnSc4	prevence
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc4d1	různý
přístupy	přístup	k1gInPc4	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
některé	některý	k3yIgInPc4	některý
sledují	sledovat	k5eAaImIp3nP	sledovat
symetrický	symetrický	k2eAgInSc1d1	symetrický
inflační	inflační	k2eAgInSc1d1	inflační
cíl	cíl	k1gInSc1	cíl
(	(	kIx(	(
<g/>
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
jak	jak	k6eAd1	jak
proti	proti	k7c3	proti
vysoké	vysoká	k1gFnSc3	vysoká
<g/>
,	,	kIx,	,
tak	tak	k9	tak
proti	proti	k7c3	proti
nízké	nízký	k2eAgFnSc3d1	nízká
inflaci	inflace	k1gFnSc3	inflace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiná	k1gFnPc1	jiná
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
proti	proti	k7c3	proti
inflaci	inflace	k1gFnSc3	inflace
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zvedne	zvednout	k5eAaPmIp3nS	zvednout
nad	nad	k7c4	nad
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
výslovně	výslovně	k6eAd1	výslovně
stanovený	stanovený	k2eAgInSc1d1	stanovený
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
implicitní	implicitní	k2eAgInSc1d1	implicitní
<g/>
.	.	kIx.	.
</s>
<s>
Monetaristé	Monetarista	k1gMnPc1	Monetarista
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
zachování	zachování	k1gNnSc4	zachování
stabilního	stabilní	k2eAgNnSc2d1	stabilní
tempa	tempo	k1gNnSc2	tempo
růstu	růst	k1gInSc2	růst
peněžní	peněžní	k2eAgFnSc2d1	peněžní
zásoby	zásoba	k1gFnSc2	zásoba
a	a	k8xC	a
užití	užití	k1gNnSc2	užití
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
inflace	inflace	k1gFnSc2	inflace
(	(	kIx(	(
<g/>
zvýšení	zvýšení	k1gNnSc1	zvýšení
úrokových	úrokový	k2eAgFnPc2d1	úroková
sazeb	sazba	k1gFnPc2	sazba
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plynoucí	plynoucí	k2eAgNnSc1d1	plynoucí
zpomalení	zpomalení	k1gNnSc1	zpomalení
růstu	růst	k1gInSc2	růst
peněžní	peněžní	k2eAgFnSc2d1	peněžní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Keynesiánci	keynesiánec	k1gMnPc1	keynesiánec
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
snížení	snížení	k1gNnSc4	snížení
agregátní	agregátní	k2eAgFnSc2d1	agregátní
poptávky	poptávka	k1gFnSc2	poptávka
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
expanze	expanze	k1gFnSc2	expanze
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
poptávky	poptávka	k1gFnSc2	poptávka
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
recese	recese	k1gFnSc2	recese
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
udržela	udržet	k5eAaPmAgFnS	udržet
inflaci	inflace	k1gFnSc4	inflace
stabilní	stabilní	k2eAgFnSc4d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Agregátní	agregátní	k2eAgFnSc4d1	agregátní
poptávku	poptávka	k1gFnSc4	poptávka
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
omezit	omezit	k5eAaPmF	omezit
i	i	k9	i
fiskální	fiskální	k2eAgFnSc7d1	fiskální
politikou	politika	k1gFnSc7	politika
(	(	kIx(	(
<g/>
zvýšením	zvýšení	k1gNnSc7	zvýšení
daní	daň	k1gFnSc7	daň
nebo	nebo	k8xC	nebo
snížením	snížení	k1gNnSc7	snížení
vládních	vládní	k2eAgInPc2d1	vládní
výdajů	výdaj	k1gInPc2	výdaj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pevný	pevný	k2eAgInSc1d1	pevný
směnný	směnný	k2eAgInSc1d1	směnný
kurz	kurz	k1gInSc1	kurz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
systému	systém	k1gInSc6	systém
pevných	pevný	k2eAgInPc2d1	pevný
směnných	směnný	k2eAgInPc2d1	směnný
kurzů	kurz	k1gInPc2	kurz
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
místní	místní	k2eAgFnSc2d1	místní
měny	měna	k1gFnSc2	měna
vázána	vázat	k5eAaImNgFnS	vázat
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
jiné	jiný	k2eAgFnSc2d1	jiná
měny	měna	k1gFnSc2	měna
nebo	nebo	k8xC	nebo
na	na	k7c4	na
koš	koš	k1gInSc4	koš
jiných	jiný	k2eAgFnPc2d1	jiná
měn	měna	k1gFnPc2	měna
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
měřítko	měřítko	k1gNnSc4	měřítko
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
zlato	zlato	k1gNnSc1	zlato
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pevný	pevný	k2eAgInSc1d1	pevný
kurz	kurz	k1gInSc1	kurz
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
stabilizaci	stabilizace	k1gFnSc4	stabilizace
hodnoty	hodnota	k1gFnSc2	hodnota
měny	měna	k1gFnSc2	měna
vůči	vůči	k7c3	vůči
měně	měna	k1gFnSc3	měna
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
zavěšen	zavěšen	k2eAgMnSc1d1	zavěšen
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k6eAd1	také
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xC	jako
prostředek	prostředek	k1gInSc1	prostředek
kontroly	kontrola	k1gFnSc2	kontrola
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
když	když	k8xS	když
hodnota	hodnota	k1gFnSc1	hodnota
referenční	referenční	k2eAgFnSc2d1	referenční
měny	měna	k1gFnSc2	měna
roste	růst	k5eAaImIp3nS	růst
nebo	nebo	k8xC	nebo
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
analogicky	analogicky	k6eAd1	analogicky
i	i	k9	i
zavěšená	zavěšený	k2eAgFnSc1d1	zavěšená
měna	měna	k1gFnSc1	měna
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
inflace	inflace	k1gFnSc1	inflace
"	"	kIx"	"
<g/>
dováží	dovázat	k5eAaPmIp3nP	dovázat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
fixní	fixní	k2eAgInPc1d1	fixní
kurz	kurz	k1gInSc1	kurz
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
vládě	vláda	k1gFnSc3	vláda
vlastní	vlastní	k2eAgFnSc4d1	vlastní
měnovou	měnový	k2eAgFnSc4d1	měnová
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
Bretton	Bretton	k1gInSc4	Bretton
Woods	Woods	k1gInSc4	Woods
měla	mít	k5eAaImAgFnS	mít
většina	většina	k1gFnSc1	většina
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
měny	měna	k1gFnSc2	měna
se	s	k7c7	s
stanoveným	stanovený	k2eAgInSc7d1	stanovený
pevným	pevný	k2eAgInSc7d1	pevný
kurzem	kurz	k1gInSc7	kurz
k	k	k7c3	k
americkému	americký	k2eAgInSc3d1	americký
dolaru	dolar	k1gInSc3	dolar
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
omezovalo	omezovat	k5eAaImAgNnS	omezovat
inflaci	inflace	k1gFnSc4	inflace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
je	on	k3xPp3gFnPc4	on
vystavilo	vystavit	k5eAaPmAgNnS	vystavit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
spekulativních	spekulativní	k2eAgInPc2d1	spekulativní
útoků	útok	k1gInPc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
byl	být	k5eAaImAgInS	být
brettonwoodský	brettonwoodský	k2eAgInSc1d1	brettonwoodský
systém	systém	k1gInSc1	systém
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
opuštěn	opuštěn	k2eAgInSc4d1	opuštěn
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
postupně	postupně	k6eAd1	postupně
přešly	přejít	k5eAaPmAgInP	přejít
k	k	k7c3	k
systémům	systém	k1gInPc3	systém
plovoucích	plovoucí	k2eAgInPc2d1	plovoucí
devizových	devizový	k2eAgInPc2d1	devizový
kurzů	kurz	k1gInPc2	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
koncem	koncem	k7c2	koncem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
vrátily	vrátit	k5eAaPmAgFnP	vrátit
k	k	k7c3	k
pevnému	pevný	k2eAgInSc3d1	pevný
kurzu	kurz	k1gInSc3	kurz
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
snahy	snaha	k1gFnSc2	snaha
udržet	udržet	k5eAaPmF	udržet
inflaci	inflace	k1gFnSc4	inflace
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Argentina	Argentina	k1gFnSc1	Argentina
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc1	Chile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zlatý	zlatý	k2eAgInSc4d1	zlatý
standard	standard	k1gInSc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
standard	standard	k1gInSc1	standard
je	být	k5eAaImIp3nS	být
peněžní	peněžní	k2eAgInSc1d1	peněžní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
jsou	být	k5eAaImIp3nP	být
platidlem	platidlo	k1gNnSc7	platidlo
papírové	papírový	k2eAgFnSc2d1	papírová
bankovky	bankovka	k1gFnSc2	bankovka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
směnitelné	směnitelný	k2eAgFnSc2d1	směnitelná
za	za	k7c4	za
předem	předem	k6eAd1	předem
stanovené	stanovený	k2eAgNnSc4d1	stanovené
<g/>
,	,	kIx,	,
fixní	fixní	k2eAgNnSc4d1	fixní
množství	množství	k1gNnSc4	množství
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přesně	přesně	k6eAd1	přesně
dáno	dát	k5eAaPmNgNnS	dát
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
krytí	krytí	k1gNnSc4	krytí
realizováno	realizován	k2eAgNnSc4d1	realizováno
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
množství	množství	k1gNnSc2	množství
kovu	kov	k1gInSc2	kov
připadajícího	připadající	k2eAgMnSc2d1	připadající
na	na	k7c4	na
měnovou	měnový	k2eAgFnSc4d1	měnová
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Měna	měna	k1gFnSc1	měna
sama	sám	k3xTgFnSc1	sám
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obchodníci	obchodník	k1gMnPc1	obchodník
ji	on	k3xPp3gFnSc4	on
přijímají	přijímat	k5eAaImIp3nP	přijímat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyměněna	vyměnit	k5eAaPmNgFnS	vyměnit
za	za	k7c4	za
ekvivalentní	ekvivalentní	k2eAgNnSc4d1	ekvivalentní
množství	množství	k1gNnSc4	množství
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
americký	americký	k2eAgInSc1d1	americký
stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
certifikát	certifikát	k1gInSc1	certifikát
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
za	za	k7c4	za
skutečný	skutečný	k2eAgInSc4d1	skutečný
kus	kus	k1gInSc4	kus
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
standard	standard	k1gInSc1	standard
byl	být	k5eAaImAgInS	být
částečně	částečně	k6eAd1	částečně
opuštěn	opuštěn	k2eAgInSc1d1	opuštěn
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
přijat	přijmout	k5eAaPmNgInS	přijmout
brettonwoodský	brettonwoodský	k2eAgInSc1d1	brettonwoodský
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
hlavní	hlavní	k2eAgFnPc4d1	hlavní
měny	měna	k1gFnPc4	měna
pevně	pevně	k6eAd1	pevně
vázány	vázat	k5eAaImNgFnP	vázat
na	na	k7c4	na
dolar	dolar	k1gInSc4	dolar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
vázán	vázat	k5eAaImNgMnS	vázat
na	na	k7c4	na
zlato	zlato	k1gNnSc4	zlato
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
35	[number]	k4	35
dolarů	dolar	k1gInPc2	dolar
za	za	k7c4	za
unci	unce	k1gFnSc4	unce
<g/>
.	.	kIx.	.
</s>
<s>
Brettonwoodský	Brettonwoodský	k2eAgInSc1d1	Brettonwoodský
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
opuštěn	opustit	k5eAaPmNgInS	opustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
zemí	zem	k1gFnPc2	zem
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
peníze	peníz	k1gInPc4	peníz
s	s	k7c7	s
nuceným	nucený	k2eAgInSc7d1	nucený
oběhem	oběh	k1gInSc7	oběh
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
měnu	měna	k1gFnSc4	měna
krytou	krytý	k2eAgFnSc4d1	krytá
pouze	pouze	k6eAd1	pouze
zákony	zákon	k1gInPc7	zákon
dané	daný	k2eAgFnSc2d1	daná
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
platnosti	platnost	k1gFnSc6	platnost
zlatého	zlatý	k2eAgInSc2d1	zlatý
standardu	standard	k1gInSc2	standard
by	by	kYmCp3nS	by
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc2	inflace
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
určena	určit	k5eAaPmNgFnS	určit
tempem	tempo	k1gNnSc7	tempo
růstu	růst	k1gInSc2	růst
zásob	zásoba	k1gFnPc2	zásoba
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
celkové	celkový	k2eAgFnSc3d1	celková
produkci	produkce	k1gFnSc3	produkce
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yRnSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
argumentují	argumentovat	k5eAaImIp3nP	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
nahodilým	nahodilý	k2eAgInPc3d1	nahodilý
výkyvům	výkyv	k1gInPc3	výkyv
v	v	k7c6	v
míře	míra	k1gFnSc6	míra
inflace	inflace	k1gFnSc2	inflace
a	a	k8xC	a
že	že	k8xS	že
měnová	měnový	k2eAgFnSc1d1	měnová
politika	politika	k1gFnSc1	politika
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stala	stát	k5eAaPmAgFnS	stát
závislou	závislý	k2eAgFnSc4d1	závislá
na	na	k7c6	na
těžbě	těžba	k1gFnSc6	těžba
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Další	další	k2eAgFnSc7d1	další
vyzkoušenou	vyzkoušený	k2eAgFnSc7d1	vyzkoušená
metodou	metoda	k1gFnSc7	metoda
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
mzdové	mzdový	k2eAgInPc1d1	mzdový
a	a	k8xC	a
cenové	cenový	k2eAgFnSc2d1	cenová
regulace	regulace	k1gFnSc2	regulace
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
příjmová	příjmový	k2eAgFnSc1d1	příjmová
politika	politika	k1gFnSc1	politika
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Regulace	regulace	k1gFnSc1	regulace
se	se	k3xPyFc4	se
osvědčily	osvědčit	k5eAaPmAgInP	osvědčit
ve	v	k7c6	v
válečných	válečný	k2eAgNnPc6d1	válečné
obdobích	období	k1gNnPc6	období
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
přídělovým	přídělový	k2eAgInSc7d1	přídělový
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jejich	jejich	k3xOp3gNnSc4	jejich
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
souvislostech	souvislost	k1gFnPc6	souvislost
není	být	k5eNaImIp3nS	být
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgInSc7d1	známý
případem	případ	k1gInSc7	případ
selhání	selhání	k1gNnSc2	selhání
je	být	k5eAaImIp3nS	být
zavedení	zavedení	k1gNnSc1	zavedení
platových	platový	k2eAgFnPc2d1	platová
a	a	k8xC	a
cenových	cenový	k2eAgFnPc2d1	cenová
regulací	regulace	k1gFnPc2	regulace
administrativou	administrativa	k1gFnSc7	administrativa
Richarda	Richard	k1gMnSc2	Richard
Nixona	Nixon	k1gMnSc2	Nixon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
mzdové	mzdový	k2eAgInPc1d1	mzdový
a	a	k8xC	a
cenové	cenový	k2eAgFnSc2d1	cenová
regulace	regulace	k1gFnSc2	regulace
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
dočasné	dočasný	k2eAgNnSc4d1	dočasné
a	a	k8xC	a
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
opatření	opatření	k1gNnSc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
zvrácené	zvrácený	k2eAgInPc4d1	zvrácený
efekty	efekt	k1gInPc4	efekt
<g/>
,	,	kIx,	,
zkreslují	zkreslovat	k5eAaImIp3nP	zkreslovat
signály	signál	k1gInPc4	signál
pro	pro	k7c4	pro
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
uměle	uměle	k6eAd1	uměle
nízké	nízký	k2eAgFnPc1d1	nízká
ceny	cena	k1gFnPc1	cena
způsobí	způsobit	k5eAaPmIp3nP	způsobit
plýtvání	plýtvání	k1gNnSc1	plýtvání
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
odradí	odradit	k5eAaPmIp3nP	odradit
budoucí	budoucí	k2eAgFnPc4d1	budoucí
investice	investice	k1gFnPc4	investice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
hlubšímu	hluboký	k2eAgInSc3d2	hlubší
nedostatku	nedostatek	k1gInSc3	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
je	být	k5eAaImIp3nS	být
zvyšováno	zvyšován	k2eAgNnSc1d1	zvyšováno
množství	množství	k1gNnSc1	množství
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
drahý	drahý	k2eAgInSc4d1	drahý
kov	kov	k1gInSc4	kov
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
jeho	jeho	k3xOp3gNnSc4	jeho
množství	množství	k1gNnSc4	množství
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
snadno	snadno	k6eAd1	snadno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nejprve	nejprve	k6eAd1	nejprve
vytěžen	vytěžen	k2eAgMnSc1d1	vytěžen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nákladný	nákladný	k2eAgInSc1d1	nákladný
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
vyhnout	vyhnout	k5eAaPmF	vyhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
drahý	drahý	k2eAgInSc1d1	drahý
kov	kov	k1gInSc1	kov
nahradí	nahradit	k5eAaPmIp3nS	nahradit
nějakým	nějaký	k3yIgMnSc7	nějaký
levnějším	levný	k2eAgMnSc7d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
probíhalo	probíhat	k5eAaImAgNnS	probíhat
znehodnocování	znehodnocování	k1gNnSc4	znehodnocování
měny	měna	k1gFnSc2	měna
<g/>
.	.	kIx.	.
</s>
<s>
Mince	mince	k1gFnPc1	mince
byly	být	k5eAaImAgFnP	být
stahovány	stahovat	k5eAaImNgFnP	stahovat
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
a	a	k8xC	a
nahrazovány	nahrazovat	k5eAaImNgFnP	nahrazovat
jinými	jiný	k2eAgFnPc7d1	jiná
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
byl	být	k5eAaImAgInS	být
zvýšen	zvýšen	k2eAgInSc1d1	zvýšen
obsah	obsah	k1gInSc1	obsah
například	například	k6eAd1	například
mědi	měď	k1gFnSc2	měď
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
zlata	zlato	k1gNnSc2	zlato
nebo	nebo	k8xC	nebo
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
takovou	takový	k3xDgFnSc4	takový
záměnu	záměna	k1gFnSc4	záměna
mincí	mince	k1gFnPc2	mince
provedl	provést	k5eAaPmAgMnS	provést
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
čistého	čistý	k2eAgInSc2d1	čistý
zisku	zisk	k1gInSc2	zisk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
množství	množství	k1gNnSc4	množství
měnového	měnový	k2eAgInSc2d1	měnový
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yRgInSc4	který
připravil	připravit	k5eAaPmAgMnS	připravit
nové	nový	k2eAgFnPc4d1	nová
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
ražebné	ražebné	k1gNnSc1	ražebné
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
všimli	všimnout	k5eAaPmAgMnP	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nová	nový	k2eAgFnSc1d1	nová
mince	mince	k1gFnSc1	mince
je	být	k5eAaImIp3nS	být
lehčí	lehký	k2eAgFnSc1d2	lehčí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
levnější	levný	k2eAgInPc1d2	levnější
kovy	kov	k1gInPc1	kov
obvykle	obvykle	k6eAd1	obvykle
mívaly	mívat	k5eAaImAgInP	mívat
nižší	nízký	k2eAgFnSc4d2	nižší
hustotu	hustota	k1gFnSc4	hustota
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
"	"	kIx"	"
<g/>
zlehčování	zlehčování	k1gNnSc4	zlehčování
mince	mince	k1gFnSc2	mince
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
penězi	peníze	k1gInPc7	peníze
papírové	papírový	k2eAgFnSc2d1	papírová
bankovky	bankovka	k1gFnSc2	bankovka
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
natisknout	natisknout	k5eAaPmF	natisknout
mnohem	mnohem	k6eAd1	mnohem
snáze	snadno	k6eAd2	snadno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
nepoměrně	poměrně	k6eNd1	poměrně
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
inflace	inflace	k1gFnSc1	inflace
papírových	papírový	k2eAgInPc2d1	papírový
peněz	peníze	k1gInPc2	peníze
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
skutečně	skutečně	k6eAd1	skutečně
ohromných	ohromný	k2eAgInPc2d1	ohromný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
svět	svět	k1gInSc1	svět
metalického	metalický	k2eAgInSc2d1	metalický
měnového	měnový	k2eAgInSc2d1	měnový
standardu	standard	k1gInSc2	standard
neznal	znát	k5eNaImAgInS	znát
<g/>
.	.	kIx.	.
</s>
<s>
Hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
hyperinflaci	hyperinflace	k1gFnSc6	hyperinflace
<g/>
.	.	kIx.	.
</s>
<s>
Případů	případ	k1gInPc2	případ
takového	takový	k3xDgInSc2	takový
nezřízeného	zřízený	k2eNgInSc2d1	nezřízený
tisku	tisk	k1gInSc2	tisk
bankovek	bankovka	k1gFnPc2	bankovka
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgInPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
francouzské	francouzský	k2eAgInPc1d1	francouzský
asignáty	asignát	k1gInPc1	asignát
<g/>
,	,	kIx,	,
americké	americký	k2eAgInPc1d1	americký
greenbacky	greenback	k1gInPc1	greenback
a	a	k8xC	a
německá	německý	k2eAgFnSc1d1	německá
marka	marka	k1gFnSc1	marka
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bankovnímu	bankovní	k2eAgInSc3d1	bankovní
systému	systém	k1gInSc2	systém
částečných	částečný	k2eAgFnPc2d1	částečná
rezerv	rezerva	k1gFnPc2	rezerva
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
nové	nový	k2eAgInPc4d1	nový
peníze	peníz	k1gInPc4	peníz
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
ani	ani	k8xC	ani
natisknout	natisknout	k5eAaPmF	natisknout
<g/>
.	.	kIx.	.
</s>
<s>
Formu	forma	k1gFnSc4	forma
bankovek	bankovka	k1gFnPc2	bankovka
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
bere	brát	k5eAaImIp3nS	brát
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbytek	zbytek	k1gInSc1	zbytek
existuje	existovat	k5eAaImIp3nS	existovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
záznamy	záznam	k1gInPc1	záznam
na	na	k7c6	na
bankovních	bankovní	k2eAgInPc6d1	bankovní
účtech	účet	k1gInPc6	účet
<g/>
.	.	kIx.	.
</s>
<s>
Jistě	jistě	k6eAd1	jistě
se	se	k3xPyFc4	se
najde	najít	k5eAaPmIp3nS	najít
ještě	ještě	k9	ještě
dřívější	dřívější	k2eAgInSc4d1	dřívější
příklad	příklad	k1gInSc4	příklad
nějaké	nějaký	k3yIgFnSc2	nějaký
inflace	inflace	k1gFnSc2	inflace
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejcitelnější	citelný	k2eAgInSc1d3	nejcitelnější
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
zdokumentovaná	zdokumentovaný	k2eAgFnSc1d1	zdokumentovaná
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
děla	dít	k5eAaImAgFnS	dít
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
ještě	ještě	k6eAd1	ještě
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c4	o
hyperinflaci	hyperinflace	k1gFnSc4	hyperinflace
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
metalický	metalický	k2eAgInSc1d1	metalický
standard	standard	k1gInSc1	standard
tak	tak	k6eAd1	tak
masivní	masivní	k2eAgFnSc4d1	masivní
devaluaci	devaluace	k1gFnSc4	devaluace
měny	měna	k1gFnSc2	měna
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnPc4	inflace
římský	římský	k2eAgInSc1d1	římský
svět	svět	k1gInSc1	svět
postihla	postihnout	k5eAaPmAgFnS	postihnout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
pozdního	pozdní	k2eAgNnSc2d1	pozdní
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
pomocí	pomoc	k1gFnSc7	pomoc
vláda	vláda	k1gFnSc1	vláda
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
financovat	financovat	k5eAaBmF	financovat
své	svůj	k3xOyFgInPc4	svůj
rostoucí	rostoucí	k2eAgInPc4d1	rostoucí
výdaje	výdaj	k1gInPc4	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
byly	být	k5eAaImAgFnP	být
ceny	cena	k1gFnPc1	cena
<g/>
,	,	kIx,	,
především	především	k9	především
základních	základní	k2eAgFnPc2d1	základní
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
regulovány	regulován	k2eAgInPc1d1	regulován
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
prakticky	prakticky	k6eAd1	prakticky
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
brzy	brzy	k6eAd1	brzy
byly	být	k5eAaImAgFnP	být
příliš	příliš	k6eAd1	příliš
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zemědělcům	zemědělec	k1gMnPc3	zemědělec
dále	daleko	k6eAd2	daleko
vyplácelo	vyplácet	k5eAaImAgNnS	vyplácet
pěstovat	pěstovat	k5eAaImF	pěstovat
obilí	obilí	k1gNnSc4	obilí
a	a	k8xC	a
obchodníkům	obchodník	k1gMnPc3	obchodník
dodávat	dodávat	k5eAaImF	dodávat
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tuhá	tuhý	k2eAgFnSc1d1	tuhá
byrokratizace	byrokratizace	k1gFnSc1	byrokratizace
impéria	impérium	k1gNnSc2	impérium
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
uměle	uměle	k6eAd1	uměle
stanovené	stanovený	k2eAgInPc1d1	stanovený
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
nízké	nízký	k2eAgFnPc1d1	nízká
ceny	cena	k1gFnPc1	cena
(	(	kIx(	(
<g/>
nominálně	nominálně	k6eAd1	nominálně
stále	stále	k6eAd1	stále
tytéž	týž	k3xTgFnPc1	týž
<g/>
,	,	kIx,	,
reálně	reálně	k6eAd1	reálně
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
inflaci	inflace	k1gFnSc3	inflace
stále	stále	k6eAd1	stále
klesající	klesající	k2eAgFnPc1d1	klesající
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
pod	pod	k7c7	pod
tvrdými	tvrdý	k2eAgInPc7d1	tvrdý
tresty	trest	k1gInPc7	trest
efektivně	efektivně	k6eAd1	efektivně
vymáhány	vymáhat	k5eAaImNgFnP	vymáhat
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyQgNnSc7	čí
však	však	k9	však
účinnější	účinný	k2eAgFnSc1d2	účinnější
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
snaha	snaha	k1gFnSc1	snaha
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
směřovalo	směřovat	k5eAaImAgNnS	směřovat
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
začala	začít	k5eAaPmAgFnS	začít
vylidňovat	vylidňovat	k5eAaImF	vylidňovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
všeobecnému	všeobecný	k2eAgInSc3d1	všeobecný
úpadku	úpadek	k1gInSc3	úpadek
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Bohatí	bohatý	k2eAgMnPc1d1	bohatý
pozemkoví	pozemkový	k2eAgMnPc1d1	pozemkový
vlastníci	vlastník	k1gMnPc1	vlastník
začali	začít	k5eAaPmAgMnP	začít
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
doménách	doména	k1gFnPc6	doména
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c6	o
autarkii	autarkie	k1gFnSc6	autarkie
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
soběstačnost	soběstačnost	k1gFnSc4	soběstačnost
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
antická	antický	k2eAgFnSc1d1	antická
civilizace	civilizace	k1gFnSc1	civilizace
proměnila	proměnit	k5eAaPmAgFnS	proměnit
ve	v	k7c4	v
feudální	feudální	k2eAgNnSc4d1	feudální
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
bývalých	bývalý	k2eAgMnPc2d1	bývalý
pozemkových	pozemkový	k2eAgMnPc2d1	pozemkový
vlastníků	vlastník	k1gMnPc2	vlastník
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
stali	stát	k5eAaPmAgMnP	stát
de	de	k?	de
facto	facto	k1gNnSc4	facto
lenní	lenní	k2eAgMnPc1d1	lenní
páni	pan	k1gMnPc1	pan
a	a	k8xC	a
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
statků	statek	k1gInPc2	statek
de	de	k?	de
facto	facto	k1gNnSc1	facto
léna	léno	k1gNnSc2	léno
<g/>
.	.	kIx.	.
</s>
<s>
Prudký	prudký	k2eAgInSc1d1	prudký
úpadek	úpadek	k1gInSc1	úpadek
hospodářství	hospodářství	k1gNnSc2	hospodářství
postupem	postupem	k7c2	postupem
doby	doba	k1gFnSc2	doba
oslabil	oslabit	k5eAaPmAgInS	oslabit
kdysi	kdysi	k6eAd1	kdysi
silnou	silný	k2eAgFnSc4d1	silná
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
nakonec	nakonec	k6eAd1	nakonec
říše	říše	k1gFnSc1	říše
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
nájezdům	nájezd	k1gInPc3	nájezd
barbarů	barbar	k1gMnPc2	barbar
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
inflace	inflace	k1gFnSc2	inflace
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
objevení	objevení	k1gNnSc1	objevení
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
platnost	platnost	k1gFnSc4	platnost
kvantitativní	kvantitativní	k2eAgFnSc2d1	kvantitativní
teorie	teorie	k1gFnSc2	teorie
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
prakticky	prakticky	k6eAd1	prakticky
použitelné	použitelný	k2eAgNnSc1d1	použitelné
množství	množství	k1gNnSc1	množství
peněz	peníze	k1gInPc2	peníze
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
jako	jako	k8xC	jako
libovolné	libovolný	k2eAgFnPc4d1	libovolná
jiné	jiný	k2eAgFnPc4d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Zdvojnásobení	zdvojnásobení	k1gNnSc1	zdvojnásobení
objemu	objem	k1gInSc2	objem
peněz	peníze	k1gInPc2	peníze
nepřináší	přinášet	k5eNaImIp3nS	přinášet
dvakrát	dvakrát	k6eAd1	dvakrát
tak	tak	k6eAd1	tak
vysoké	vysoký	k2eAgNnSc4d1	vysoké
bohatství	bohatství	k1gNnSc4	bohatství
všem	všecek	k3xTgMnPc3	všecek
zúčastněným	zúčastněný	k2eAgMnPc3d1	zúčastněný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
dvojnásobné	dvojnásobný	k2eAgFnPc4d1	dvojnásobná
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
už	už	k6eAd1	už
skotský	skotský	k2eAgMnSc1d1	skotský
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
myslitel	myslitel	k1gMnSc1	myslitel
David	David	k1gMnSc1	David
Hume	Hume	k1gFnSc1	Hume
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
získali	získat	k5eAaPmAgMnP	získat
ohromný	ohromný	k2eAgInSc4d1	ohromný
zdroj	zdroj	k1gInSc4	zdroj
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
způsobily	způsobit	k5eAaPmAgInP	způsobit
růst	růst	k1gInSc4	růst
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
inflaci	inflace	k1gFnSc4	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jejími	její	k3xOp3gFnPc7	její
dnešními	dnešní	k2eAgFnPc7d1	dnešní
dimenzemi	dimenze	k1gFnPc7	dimenze
šlo	jít	k5eAaImAgNnS	jít
však	však	k8xC	však
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
mírný	mírný	k2eAgInSc4d1	mírný
růst	růst	k1gInSc4	růst
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
kovů	kov	k1gInPc2	kov
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
byly	být	k5eAaImAgInP	být
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
vysoké	vysoký	k2eAgFnPc4d1	vysoká
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
lácí	láce	k1gFnSc7	láce
dnešních	dnešní	k2eAgInPc2d1	dnešní
inflačních	inflační	k2eAgInPc2d1	inflační
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
dováželo	dovážet	k5eAaImAgNnS	dovážet
z	z	k7c2	z
kolonií	kolonie	k1gFnPc2	kolonie
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
španělské	španělský	k2eAgFnSc6d1	španělská
ekonomice	ekonomika	k1gFnSc6	ekonomika
nic	nic	k3yNnSc1	nic
nepřineslo	přinést	k5eNaPmAgNnS	přinést
<g/>
,	,	kIx,	,
tamější	tamější	k2eAgMnPc1d1	tamější
podnikatelé	podnikatel	k1gMnPc1	podnikatel
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
němu	on	k3xPp3gNnSc3	on
nestali	stát	k5eNaPmAgMnP	stát
efektivnější	efektivní	k2eAgMnPc1d2	efektivnější
a	a	k8xC	a
nevyráběli	vyrábět	k5eNaImAgMnP	vyrábět
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
panovníkům	panovník	k1gMnPc3	panovník
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vést	vést	k5eAaImF	vést
nákladné	nákladný	k2eAgFnPc4d1	nákladná
války	válka	k1gFnSc2	válka
zejména	zejména	k9	zejména
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
šlechtě	šlechta	k1gFnSc3	šlechta
upevnit	upevnit	k5eAaPmF	upevnit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
třetím	třetí	k4xOgInSc7	třetí
stavem	stav	k1gInSc7	stav
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
nedošlo	dojít	k5eNaPmAgNnS	dojít
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
řemeslné	řemeslný	k2eAgFnSc2d1	řemeslná
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
tomu	ten	k3xDgNnSc3	ten
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
vše	všechen	k3xTgNnSc1	všechen
dováželo	dovážet	k5eAaImAgNnS	dovážet
a	a	k8xC	a
řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
výroba	výroba	k1gFnSc1	výroba
upadala	upadat	k5eAaPmAgFnS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
budovaly	budovat	k5eAaImAgFnP	budovat
nové	nový	k2eAgFnPc1d1	nová
manufaktury	manufaktura	k1gFnPc1	manufaktura
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ty	ten	k3xDgFnPc1	ten
španělské	španělský	k2eAgFnPc1d1	španělská
krachovaly	krachovat	k5eAaBmAgFnP	krachovat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
španělské	španělský	k2eAgFnSc2d1	španělská
inteligence	inteligence	k1gFnSc2	inteligence
s	s	k7c7	s
nelibostí	nelibost	k1gFnSc7	nelibost
sledovala	sledovat	k5eAaImAgFnS	sledovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
mnohem	mnohem	k6eAd1	mnohem
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
rodné	rodný	k2eAgFnSc6d1	rodná
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
zvětšovala	zvětšovat	k5eAaImAgFnS	zvětšovat
a	a	k8xC	a
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
takové	takový	k3xDgFnPc4	takový
míry	míra	k1gFnPc4	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
kdysi	kdysi	k6eAd1	kdysi
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
mocnosti	mocnost	k1gFnSc2	mocnost
na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kořist	kořist	k1gFnSc1	kořist
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
hyperinflace	hyperinflace	k1gFnSc1	hyperinflace
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
vzájemně	vzájemně	k6eAd1	vzájemně
protichůdná	protichůdný	k2eAgNnPc1d1	protichůdné
vysvětlení	vysvětlení	k1gNnPc1	vysvětlení
jejích	její	k3xOp3gFnPc2	její
příčin	příčina	k1gFnPc2	příčina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
popisu	popis	k1gInSc6	popis
katastrofálních	katastrofální	k2eAgInPc2d1	katastrofální
následků	následek	k1gInPc2	následek
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
chvíle	chvíle	k1gFnSc2	chvíle
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
veškeré	veškerý	k3xTgFnPc1	veškerý
úspory	úspora	k1gFnPc1	úspora
lidí	člověk	k1gMnPc2	člověk
byly	být	k5eAaImAgFnP	být
znehodnoceny	znehodnotit	k5eAaPmNgFnP	znehodnotit
narůstající	narůstající	k2eAgFnSc7d1	narůstající
měnou	měna	k1gFnSc7	měna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
inflace	inflace	k1gFnSc2	inflace
se	se	k3xPyFc4	se
jeden	jeden	k4xCgInSc1	jeden
dolar	dolar	k1gInSc1	dolar
rovnal	rovnat	k5eAaImAgInS	rovnat
4,2	[number]	k4	4,2
bilionům	bilion	k4xCgInPc3	bilion
(	(	kIx(	(
<g/>
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
spekulace	spekulace	k1gFnPc1	spekulace
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
chvíli	chvíle	k1gFnSc4	chvíle
až	až	k9	až
12	[number]	k4	12
bilionům	bilion	k4xCgInPc3	bilion
<g/>
)	)	kIx)	)
výmarských	výmarský	k2eAgFnPc2d1	Výmarská
marek	marka	k1gFnPc2	marka
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
pak	pak	k6eAd1	pak
chodili	chodit	k5eAaImAgMnP	chodit
nakupovat	nakupovat	k5eAaBmF	nakupovat
do	do	k7c2	do
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
košem	koš	k1gInSc7	koš
plným	plný	k2eAgNnSc7d1	plné
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
dokonce	dokonce	k9	dokonce
i	i	k9	i
penězi	peníze	k1gInPc7	peníze
topili	topit	k5eAaImAgMnP	topit
<g/>
,	,	kIx,	,
hospodářství	hospodářství	k1gNnSc1	hospodářství
se	se	k3xPyFc4	se
hroutilo	hroutit	k5eAaImAgNnS	hroutit
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
začali	začít	k5eAaPmAgMnP	začít
pozorněji	pozorně	k6eAd2	pozorně
naslouchat	naslouchat	k5eAaImF	naslouchat
hlasu	hlas	k1gInSc3	hlas
demagogů	demagog	k1gMnPc2	demagog
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
slibovali	slibovat	k5eAaImAgMnP	slibovat
návrat	návrat	k1gInSc4	návrat
předválečné	předválečný	k2eAgFnSc2d1	předválečná
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
výklady	výklad	k1gInPc1	výklad
se	se	k3xPyFc4	se
však	však	k9	však
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
koho	kdo	k3yQnSc4	kdo
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
viníka	viník	k1gMnSc4	viník
této	tento	k3xDgFnSc2	tento
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
a	a	k8xC	a
častější	častý	k2eAgFnSc1d2	častější
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
především	především	k9	především
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Německo	Německo	k1gNnSc1	Německo
bylo	být	k5eAaImAgNnS	být
značně	značně	k6eAd1	značně
oslabeno	oslabit	k5eAaPmNgNnS	oslabit
<g/>
,	,	kIx,	,
zdecimováno	zdecimován	k2eAgNnSc1d1	zdecimováno
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
muselo	muset	k5eAaImAgNnS	muset
platit	platit	k5eAaImF	platit
ještě	ještě	k9	ještě
válečné	válečný	k2eAgFnPc1d1	válečná
reparace	reparace	k1gFnPc1	reparace
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
tak	tak	k6eAd1	tak
velké	velký	k2eAgFnPc1d1	velká
<g/>
,	,	kIx,	,
že	že	k8xS	že
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
rozloženy	rozložit	k5eAaPmNgFnP	rozložit
do	do	k7c2	do
vícero	vícero	k4xRyIgNnSc4	vícero
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
ale	ale	k8xC	ale
úroky	úrok	k1gInPc1	úrok
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
reparací	reparace	k1gFnPc2	reparace
začaly	začít	k5eAaPmAgFnP	začít
přerůstat	přerůstat	k5eAaImF	přerůstat
únosnou	únosný	k2eAgFnSc4d1	únosná
mez	mez	k1gFnSc4	mez
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
si	se	k3xPyFc3	se
začalo	začít	k5eAaPmAgNnS	začít
půjčovat	půjčovat	k5eAaImF	půjčovat
další	další	k2eAgInPc4d1	další
úvěry	úvěr	k1gInPc4	úvěr
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
splácení	splácení	k1gNnSc4	splácení
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
situace	situace	k1gFnSc1	situace
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ani	ani	k8xC	ani
veškerý	veškerý	k3xTgInSc1	veškerý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
země	zem	k1gFnSc2	zem
nestačil	stačit	k5eNaBmAgInS	stačit
ke	k	k7c3	k
splacení	splacení	k1gNnSc3	splacení
válečných	válečný	k2eAgFnPc2d1	válečná
reparací	reparace	k1gFnPc2	reparace
a	a	k8xC	a
úroků	úrok	k1gInPc2	úrok
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
vláda	vláda	k1gFnSc1	vláda
neměla	mít	k5eNaImAgFnS	mít
na	na	k7c4	na
vybranou	vybraná	k1gFnSc4	vybraná
a	a	k8xC	a
platit	platit	k5eAaImF	platit
musela	muset	k5eAaImAgFnS	muset
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
Říšská	říšský	k2eAgFnSc1d1	říšská
banka	banka	k1gFnSc1	banka
peníze	peníz	k1gInPc4	peníz
jednoduše	jednoduše	k6eAd1	jednoduše
tisknout	tisknout	k5eAaImF	tisknout
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nebyly	být	k5eNaImAgInP	být
nijak	nijak	k6eAd1	nijak
svázány	svázán	k2eAgInPc1d1	svázán
se	s	k7c7	s
skutečnými	skutečný	k2eAgFnPc7d1	skutečná
hodnotami	hodnota	k1gFnPc7	hodnota
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
okamžitě	okamžitě	k6eAd1	okamžitě
následovala	následovat	k5eAaImAgFnS	následovat
mohutná	mohutný	k2eAgFnSc1d1	mohutná
inflace	inflace	k1gFnSc1	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obviňuje	obviňovat	k5eAaImIp3nS	obviňovat
mocnosti	mocnost	k1gFnPc4	mocnost
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obrací	obracet	k5eAaImIp3nP	obracet
pozornost	pozornost	k1gFnSc4	pozornost
k	k	k7c3	k
samotné	samotný	k2eAgFnSc3d1	samotná
německé	německý	k2eAgFnSc3d1	německá
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
válečné	válečný	k2eAgFnPc4d1	válečná
reparace	reparace	k1gFnPc4	reparace
jako	jako	k8xS	jako
možnou	možný	k2eAgFnSc4d1	možná
příčinu	příčina	k1gFnSc4	příčina
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
například	například	k6eAd1	například
Niall	Niall	k1gMnSc1	Niall
Fergusson	Fergusson	k1gMnSc1	Fergusson
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
The	The	k1gFnPc4	The
Pity	pit	k2eAgFnPc4d1	pita
of	of	k?	of
War	War	k1gFnPc4	War
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jako	jako	k9	jako
Nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Německu	Německo	k1gNnSc3	Německo
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
placení	placení	k1gNnSc3	placení
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
předepsané	předepsaný	k2eAgFnSc2d1	předepsaná
sumy	suma	k1gFnSc2	suma
(	(	kIx(	(
<g/>
z	z	k7c2	z
132	[number]	k4	132
na	na	k7c4	na
37	[number]	k4	37
mld	mld	k?	mld
marek	marka	k1gFnPc2	marka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
reparací	reparace	k1gFnPc2	reparace
jako	jako	k8xC	jako
příčiny	příčina	k1gFnPc4	příčina
německé	německý	k2eAgFnSc2d1	německá
hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
pak	pak	k6eAd1	pak
prý	prý	k9	prý
stvořil	stvořit	k5eAaPmAgMnS	stvořit
především	především	k9	především
lord	lord	k1gMnSc1	lord
Keynes	Keynes	k1gMnSc1	Keynes
svou	svůj	k3xOyFgFnSc7	svůj
knihou	kniha	k1gFnSc7	kniha
Ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
důsledky	důsledek	k1gInPc1	důsledek
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
angažmá	angažmá	k1gNnSc6	angažmá
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
britské	britský	k2eAgFnSc2d1	britská
delegace	delegace	k1gFnSc2	delegace
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
stanovit	stanovit	k5eAaPmF	stanovit
poválečné	poválečný	k2eAgNnSc4d1	poválečné
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnou	skutečný	k2eAgFnSc7d1	skutečná
příčinou	příčina	k1gFnSc7	příčina
německé	německý	k2eAgFnSc2d1	německá
hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
podle	podle	k7c2	podle
Fergussona	Fergussona	k1gFnSc1	Fergussona
byla	být	k5eAaImAgFnS	být
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
vláda	vláda	k1gFnSc1	vláda
výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
politicky	politicky	k6eAd1	politicky
bezbolestně	bezbolestně	k6eAd1	bezbolestně
chtěla	chtít	k5eAaImAgFnS	chtít
zbavit	zbavit	k5eAaPmF	zbavit
ohromných	ohromný	k2eAgInPc2d1	ohromný
válečných	válečný	k2eAgInPc2d1	válečný
dluhů	dluh	k1gInPc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
guvernér	guvernér	k1gMnSc1	guvernér
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
Bernard	Bernard	k1gMnSc1	Bernard
Lietaer	Lietaer	k1gMnSc1	Lietaer
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Budoucnost	budoucnost	k1gFnSc1	budoucnost
peněz	peníze	k1gInPc2	peníze
popisuje	popisovat	k5eAaImIp3nS	popisovat
případ	případ	k1gInSc1	případ
jistého	jistý	k2eAgInSc2d1	jistý
regionu	region	k1gInSc2	region
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1923	[number]	k4	1923
<g/>
-	-	kIx~	-
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
největší	veliký	k2eAgFnSc2d3	veliký
hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
<g/>
.	.	kIx.	.
</s>
<s>
Tamní	tamní	k2eAgMnSc1d1	tamní
vlastník	vlastník	k1gMnSc1	vlastník
uhelného	uhelný	k2eAgInSc2d1	uhelný
dolu	dol	k1gInSc2	dol
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgInPc4d1	vlastní
lokální	lokální	k2eAgInPc4d1	lokální
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
denominoval	denominovat	k5eAaPmAgInS	denominovat
uhlím	uhlí	k1gNnSc7	uhlí
vytěženým	vytěžený	k2eAgFnPc3d1	vytěžená
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
dolu	dol	k1gInSc2	dol
(	(	kIx(	(
<g/>
stanovil	stanovit	k5eAaPmAgInS	stanovit
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
jeho	jeho	k3xOp3gInPc2	jeho
lokálních	lokální	k2eAgInPc2d1	lokální
peněz	peníze	k1gInPc2	peníze
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jednomu	jeden	k4xCgInSc3	jeden
pytli	pytel	k1gInSc3	pytel
uhlí	uhlí	k1gNnSc2	uhlí
určité	určitý	k2eAgFnSc2d1	určitá
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
stanovil	stanovit	k5eAaPmAgInS	stanovit
(	(	kIx(	(
<g/>
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c6	na
bankovkách	bankovka	k1gFnPc6	bankovka
této	tento	k3xDgFnSc2	tento
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
měny	měna	k1gFnSc2	měna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
bankovka	bankovka	k1gFnSc1	bankovka
je	být	k5eAaImIp3nS	být
směnitelná	směnitelný	k2eAgFnSc1d1	směnitelná
za	za	k7c4	za
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
množství	množství	k1gNnSc4	množství
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
podnik	podnik	k1gInSc1	podnik
se	se	k3xPyFc4	se
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
tuto	tento	k3xDgFnSc4	tento
směnu	směna	k1gFnSc4	směna
provést	provést	k5eAaPmF	provést
<g/>
.	.	kIx.	.
</s>
<s>
Bankovky	bankovka	k1gFnPc1	bankovka
byly	být	k5eAaImAgFnP	být
navíc	navíc	k6eAd1	navíc
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
kolonkami	kolonka	k1gFnPc7	kolonka
(	(	kIx(	(
<g/>
každá	každý	k3xTgFnSc1	každý
představovala	představovat	k5eAaImAgFnS	představovat
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
lepily	lepit	k5eAaImAgInP	lepit
kolky	kolek	k1gInPc1	kolek
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
směně	směna	k1gFnSc6	směna
bankovky	bankovka	k1gFnSc2	bankovka
za	za	k7c4	za
množství	množství	k1gNnSc4	množství
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Bankovky	bankovka	k1gFnPc1	bankovka
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
chyběly	chybět	k5eAaImAgInP	chybět
kolky	kolek	k1gInPc1	kolek
za	za	k7c4	za
současný	současný	k2eAgInSc4d1	současný
i	i	k8xC	i
minulý	minulý	k2eAgInSc4d1	minulý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
platnost	platnost	k1gFnSc4	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
opatření	opatření	k1gNnSc1	opatření
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
mělo	mít	k5eAaImAgNnS	mít
motivovat	motivovat	k5eAaBmF	motivovat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
peníze	peníz	k1gInPc4	peníz
směňovali	směňovat	k5eAaImAgMnP	směňovat
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
rostl	růst	k5eAaImAgInS	růst
geometrickou	geometrický	k2eAgFnSc7d1	geometrická
řadou	řada	k1gFnSc7	řada
a	a	k8xC	a
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jinak	jinak	k6eAd1	jinak
bezútěšné	bezútěšný	k2eAgFnSc6d1	bezútěšná
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
daleko	daleko	k6eAd1	daleko
za	za	k7c4	za
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
důl	důl	k1gInSc1	důl
působil	působit	k5eAaImAgInS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
fungovaly	fungovat	k5eAaImAgInP	fungovat
i	i	k9	i
po	po	k7c6	po
finanční	finanční	k2eAgFnSc6d1	finanční
stabilizaci	stabilizace	k1gFnSc6	stabilizace
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
(	(	kIx(	(
<g/>
legislativního	legislativní	k2eAgNnSc2d1	legislativní
nařízení	nařízení	k1gNnSc2	nařízení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
lokální	lokální	k2eAgFnPc4d1	lokální
měnu	měna	k1gFnSc4	měna
jako	jako	k8xS	jako
takovou	takový	k3xDgFnSc4	takový
postavil	postavit	k5eAaPmAgMnS	postavit
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lietaer	Lietaer	k1gInSc1	Lietaer
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
případu	případ	k1gInSc6	případ
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
klíčovým	klíčový	k2eAgInSc7d1	klíčový
aspektem	aspekt	k1gInSc7	aspekt
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
spojení	spojení	k1gNnSc1	spojení
nominální	nominální	k2eAgFnSc2d1	nominální
hodnoty	hodnota	k1gFnSc2	hodnota
dané	daný	k2eAgFnSc2d1	daná
měny	měna	k1gFnSc2	měna
se	s	k7c7	s
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
hodnotou	hodnota	k1gFnSc7	hodnota
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
<g/>
:	:	kIx,	:
zatímco	zatímco	k8xS	zatímco
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
výmarská	výmarský	k2eAgFnSc1d1	Výmarská
státní	státní	k2eAgFnSc1d1	státní
pokladna	pokladna	k1gFnSc1	pokladna
svou	svůj	k3xOyFgFnSc4	svůj
měnu	měna	k1gFnSc4	měna
s	s	k7c7	s
nuceným	nucený	k2eAgInSc7d1	nucený
oběhem	oběh	k1gInSc7	oběh
tiskla	tisknout	k5eAaImAgFnS	tisknout
pro	pro	k7c4	pro
splátky	splátka	k1gFnPc4	splátka
válečných	válečný	k2eAgFnPc2d1	válečná
reparací	reparace	k1gFnPc2	reparace
bez	bez	k7c2	bez
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
relace	relace	k1gFnSc2	relace
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
"	"	kIx"	"
<g/>
peněz	peníze	k1gInPc2	peníze
krytých	krytý	k2eAgFnPc2d1	krytá
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
"	"	kIx"	"
majitel	majitel	k1gMnSc1	majitel
dolu	dol	k1gInSc2	dol
nikdy	nikdy	k6eAd1	nikdy
nevydal	vydat	k5eNaPmAgInS	vydat
více	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
<g/>
,	,	kIx,	,
než	než	k8xS	než
vytěžil	vytěžit	k5eAaPmAgMnS	vytěžit
uhlí	uhlí	k1gNnSc4	uhlí
(	(	kIx(	(
<g/>
tím	ten	k3xDgNnSc7	ten
mohl	moct	k5eAaImAgInS	moct
dostát	dostát	k5eAaPmF	dostát
slibu	slib	k1gInSc6	slib
napsaném	napsaný	k2eAgInSc6d1	napsaný
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
bankovkách	bankovka	k1gFnPc6	bankovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
inflační	inflační	k2eAgFnSc1d1	inflační
krize	krize	k1gFnSc1	krize
otřásla	otřást	k5eAaPmAgFnS	otřást
bývalou	bývalý	k2eAgFnSc4d1	bývalá
Jugoslávii	Jugoslávie	k1gFnSc4	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
federace	federace	k1gFnSc2	federace
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
podobná	podobný	k2eAgFnSc1d1	podobná
jako	jako	k8xS	jako
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
transformaci	transformace	k1gFnSc3	transformace
z	z	k7c2	z
plánovaného	plánovaný	k2eAgInSc2d1	plánovaný
na	na	k7c4	na
tržní	tržní	k2eAgNnSc4d1	tržní
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Tisknutí	tisknutí	k1gNnSc1	tisknutí
nových	nový	k2eAgInPc2d1	nový
peněz	peníze	k1gInPc2	peníze
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
rekordní	rekordní	k2eAgFnSc4d1	rekordní
inflaci	inflace	k1gFnSc4	inflace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
19	[number]	k4	19
810,2	[number]	k4	810,2
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
vládě	vláda	k1gFnSc3	vláda
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
peníze	peníz	k1gInPc4	peníz
od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zakládaní	zakládaný	k2eAgMnPc1d1	zakládaný
velkých	velký	k2eAgFnPc2d1	velká
bank	banka	k1gFnPc2	banka
a	a	k8xC	a
investičních	investiční	k2eAgFnPc2d1	investiční
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
fungovaly	fungovat	k5eAaImAgFnP	fungovat
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Prostředky	prostředek	k1gInPc1	prostředek
z	z	k7c2	z
Dafíny	Dafína	k1gFnSc2	Dafína
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
lidem	lid	k1gInSc7	lid
vracet	vracet	k5eAaImF	vracet
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
po	po	k7c6	po
iniciativě	iniciativa	k1gFnSc6	iniciativa
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jugoslávský	jugoslávský	k2eAgInSc1d1	jugoslávský
dinár	dinár	k1gInSc1	dinár
<g/>
,	,	kIx,	,
stabilní	stabilní	k2eAgFnSc1d1	stabilní
měna	měna	k1gFnSc1	měna
někdejší	někdejší	k2eAgFnSc2d1	někdejší
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
kurzu	kurz	k1gInSc2	kurz
7	[number]	k4	7
dinárů	dinár	k1gInPc2	dinár
za	za	k7c4	za
marku	marka	k1gFnSc4	marka
propadl	propadnout	k5eAaPmAgMnS	propadnout
do	do	k7c2	do
takové	takový	k3xDgFnSc2	takový
inflace	inflace	k1gFnSc2	inflace
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vydáván	vydávat	k5eAaPmNgInS	vydávat
v	v	k7c6	v
miliardových	miliardový	k2eAgFnPc6d1	miliardová
hodnotách	hodnota	k1gFnPc6	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
měnová	měnový	k2eAgFnSc1d1	měnová
reforma	reforma	k1gFnSc1	reforma
a	a	k8xC	a
zavedení	zavedení	k1gNnSc1	zavedení
"	"	kIx"	"
<g/>
nových	nový	k2eAgInPc2d1	nový
dinárů	dinár	k1gInPc2	dinár
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nastal	nastat	k5eAaPmAgInS	nastat
nedostatek	nedostatek	k1gInSc4	nedostatek
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
přispěly	přispět	k5eAaPmAgFnP	přispět
i	i	k9	i
sankce	sankce	k1gFnPc1	sankce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
na	na	k7c4	na
celou	celá	k1gFnSc4	celá
SFRJ	SFRJ	kA	SFRJ
uvalila	uvalit	k5eAaPmAgFnS	uvalit
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Peníze	peníz	k1gInPc1	peníz
ztratily	ztratit	k5eAaPmAgInP	ztratit
funkci	funkce	k1gFnSc4	funkce
oběživa	oběživo	k1gNnSc2	oběživo
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
fungovat	fungovat	k5eAaImF	fungovat
směnný	směnný	k2eAgInSc1d1	směnný
obchod	obchod	k1gInSc1	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
míra	míra	k1gFnSc1	míra
inflace	inflace	k1gFnSc1	inflace
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
všechny	všechen	k3xTgFnPc4	všechen
republiky	republika	k1gFnPc4	republika
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
rychle	rychle	k6eAd1	rychle
přeorientovat	přeorientovat	k5eAaPmF	přeorientovat
na	na	k7c4	na
nové	nový	k2eAgInPc4d1	nový
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
jugoslávský	jugoslávský	k2eAgInSc4d1	jugoslávský
dinár	dinár	k1gInSc4	dinár
zprvu	zprvu	k6eAd1	zprvu
svým	svůj	k3xOyFgInSc7	svůj
vlastním	vlastní	k2eAgInSc7d1	vlastní
chorvatským	chorvatský	k2eAgInSc7d1	chorvatský
dinárem	dinár	k1gInSc7	dinár
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
následně	následně	k6eAd1	následně
kunou	kuna	k1gFnSc7	kuna
<g/>
.	.	kIx.	.
</s>
<s>
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
a	a	k8xC	a
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
měnu	měna	k1gFnSc4	měna
německou	německý	k2eAgFnSc4d1	německá
marku	marka	k1gFnSc4	marka
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
pak	pak	k6eAd1	pak
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
používaly	používat	k5eAaImAgFnP	používat
tři	tři	k4xCgFnPc1	tři
různé	různý	k2eAgFnPc1d1	různá
měny	měna	k1gFnPc1	měna
(	(	kIx(	(
<g/>
chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
kuna	kuna	k1gFnSc1	kuna
<g/>
,	,	kIx,	,
bosenský	bosenský	k2eAgInSc4d1	bosenský
dinár	dinár	k1gInSc4	dinár
a	a	k8xC	a
dinár	dinár	k1gInSc4	dinár
Republiky	republika	k1gFnSc2	republika
srbské	srbský	k2eAgFnSc2d1	Srbská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
bosenská	bosenský	k2eAgFnSc1d1	bosenská
konvertibilní	konvertibilní	k2eAgFnSc1d1	konvertibilní
marka	marka	k1gFnSc1	marka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pevně	pevně	k6eAd1	pevně
navázána	navázán	k2eAgFnSc1d1	navázána
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
v	v	k7c6	v
kurzu	kurz	k1gInSc6	kurz
1,955	[number]	k4	1,955
KM	km	kA	km
<g/>
/	/	kIx~	/
<g/>
€	€	k?	€
<g/>
.	.	kIx.	.
</s>
<s>
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
stabilní	stabilní	k2eAgFnSc4d1	stabilní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
růstem	růst	k1gInSc7	růst
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
změnila	změnit	k5eAaPmAgFnS	změnit
a	a	k8xC	a
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
v	v	k7c6	v
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
krizi	krize	k1gFnSc6	krize
s	s	k7c7	s
pádivou	pádivý	k2eAgFnSc7d1	pádivá
inflací	inflace	k1gFnSc7	inflace
během	během	k7c2	během
posledních	poslední	k2eAgInPc2d1	poslední
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
června	červen	k1gInSc2	červen
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
pak	pak	k6eAd1	pak
inflace	inflace	k1gFnSc1	inflace
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
týdenní	týdenní	k2eAgFnPc4d1	týdenní
úrovně	úroveň	k1gFnPc4	úroveň
až	až	k9	až
300	[number]	k4	300
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
analytika	analytik	k1gMnSc2	analytik
portálu	portál	k1gInSc2	portál
Patria	Patrium	k1gNnSc2	Patrium
<g/>
,	,	kIx,	,
Davida	David	k1gMnSc2	David
Marka	Marek	k1gMnSc2	Marek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vině	vina	k1gFnSc6	vina
hostilní	hostilní	k2eAgFnSc1d1	hostilní
politika	politika	k1gFnSc1	politika
prezidenta	prezident	k1gMnSc2	prezident
Roberta	Robert	k1gMnSc2	Robert
Mugabeho	Mugabe	k1gMnSc2	Mugabe
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
důsledku	důsledek	k1gInSc6	důsledek
zemi	zem	k1gFnSc4	zem
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2002	[number]	k4	2002
opustily	opustit	k5eAaPmAgFnP	opustit
asi	asi	k9	asi
4	[number]	k4	4
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
varoval	varovat	k5eAaImAgMnS	varovat
MMF	MMF	kA	MMF
<g/>
,	,	kIx,	,
že	že	k8xS	že
inflace	inflace	k1gFnSc1	inflace
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
předpoklady	předpoklad	k1gInPc1	předpoklad
byly	být	k5eAaImAgInP	být
překonány	překonat	k5eAaPmNgInP	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
se	se	k3xPyFc4	se
držela	držet	k5eAaImAgFnS	držet
inflace	inflace	k1gFnSc1	inflace
na	na	k7c6	na
2,2	[number]	k4	2,2
milionech	milion	k4xCgInPc6	milion
procent	procento	k1gNnPc2	procento
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
ale	ale	k8xC	ale
již	již	k6eAd1	již
pokořila	pokořit	k5eAaPmAgFnS	pokořit
i	i	k9	i
největší	veliký	k2eAgInPc4d3	veliký
rekordy	rekord	k1gInPc4	rekord
a	a	k8xC	a
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
nevídaných	vídaný	k2eNgInPc6d1	nevídaný
11,3	[number]	k4	11,3
milionu	milion	k4xCgInSc2	milion
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
tehdy	tehdy	k6eAd1	tehdy
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
k	k	k7c3	k
reformě	reforma	k1gFnSc3	reforma
měny	měna	k1gFnSc2	měna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
deseti	deset	k4xCc2	deset
miliard	miliarda	k4xCgFnPc2	miliarda
zimbabwských	zimbabwský	k2eAgInPc2d1	zimbabwský
dolarů	dolar	k1gInPc2	dolar
udělá	udělat	k5eAaPmIp3nS	udělat
jeden	jeden	k4xCgInSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Reforma	reforma	k1gFnSc1	reforma
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
chaos	chaos	k1gInSc4	chaos
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
ještě	ještě	k6eAd1	ještě
prohloubila	prohloubit	k5eAaPmAgFnS	prohloubit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
inflace	inflace	k1gFnSc2	inflace
oficiálně	oficiálně	k6eAd1	oficiálně
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
230	[number]	k4	230
miliónů	milión	k4xCgInPc2	milión
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Cílování	Cílování	k1gNnSc1	Cílování
inflace	inflace	k1gFnSc2	inflace
je	být	k5eAaImIp3nS	být
postup	postup	k1gInSc4	postup
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
předem	předem	k6eAd1	předem
veřejně	veřejně	k6eAd1	veřejně
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc4	jaký
inflace	inflace	k1gFnSc1	inflace
chce	chtít	k5eAaImIp3nS	chtít
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
období	období	k1gNnSc6	období
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníkem	průkopník	k1gMnSc7	průkopník
této	tento	k3xDgFnSc2	tento
politiky	politika	k1gFnSc2	politika
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
politiku	politika	k1gFnSc4	politika
v	v	k7c6	v
ČR	ČR	kA	ČR
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
provádí	provádět	k5eAaImIp3nS	provádět
ČNB	ČNB	kA	ČNB
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
pak	pak	k6eAd1	pak
tzv.	tzv.	kA	tzv.
cílování	cílování	k1gNnSc1	cílování
měnového	měnový	k2eAgInSc2d1	měnový
kurzu	kurz	k1gInSc2	kurz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
autonomie	autonomie	k1gFnSc2	autonomie
měnové	měnový	k2eAgFnSc2d1	měnová
politiky	politika	k1gFnSc2	politika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
klesla	klesnout	k5eAaPmAgFnS	klesnout
inflace	inflace	k1gFnSc1	inflace
z	z	k7c2	z
předchozích	předchozí	k2eAgFnPc2d1	předchozí
hodnot	hodnota	k1gFnPc2	hodnota
okolo	okolo	k7c2	okolo
10	[number]	k4	10
<g/>
%	%	kIx~	%
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgInS	začít
však	však	k9	však
růst	růst	k1gInSc1	růst
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
mylnou	mylný	k2eAgFnSc4d1	mylná
doktrínu	doktrína	k1gFnSc4	doktrína
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
politiku	politika	k1gFnSc4	politika
provádí	provádět	k5eAaImIp3nS	provádět
i	i	k9	i
Evropská	evropský	k2eAgFnSc1d1	Evropská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
<g/>
.	.	kIx.	.
</s>
<s>
Plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
inflace	inflace	k1gFnSc1	inflace
je	být	k5eAaImIp3nS	být
kritiky	kritika	k1gFnSc2	kritika
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
centrální	centrální	k2eAgNnSc4d1	centrální
plánování	plánování	k1gNnSc4	plánování
a	a	k8xC	a
mýtus	mýtus	k1gInSc4	mýtus
deflace	deflace	k1gFnSc2	deflace
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Jeffrey	Jeffrea	k1gFnSc2	Jeffrea
Frankel	Frankel	k1gMnSc1	Frankel
tuto	tento	k3xDgFnSc4	tento
politiku	politika	k1gFnSc4	politika
považuje	považovat	k5eAaImIp3nS	považovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
za	za	k7c4	za
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
udržitelná	udržitelný	k2eAgFnSc1d1	udržitelná
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
nulová	nulový	k2eAgFnSc1d1	nulová
inflace	inflace	k1gFnSc1	inflace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádná	žádný	k3yNgFnSc1	žádný
měna	měna	k1gFnSc1	měna
necíluje	cílovat	k5eNaImIp3nS	cílovat
inflaci	inflace	k1gFnSc4	inflace
na	na	k7c4	na
nulu	nula	k1gFnSc4	nula
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
toho	ten	k3xDgNnSc2	ten
zastáncem	zastánce	k1gMnSc7	zastánce
například	například	k6eAd1	například
i	i	k9	i
Alan	Alan	k1gMnSc1	Alan
Greenspan	Greenspan	k1gMnSc1	Greenspan
<g/>
.	.	kIx.	.
</s>
<s>
Vyspělé	vyspělý	k2eAgInPc1d1	vyspělý
státy	stát	k1gInPc1	stát
cílují	cílovat	k5eAaPmIp3nP	cílovat
inflaci	inflace	k1gFnSc4	inflace
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
%	%	kIx~	%
a	a	k8xC	a
3	[number]	k4	3
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
