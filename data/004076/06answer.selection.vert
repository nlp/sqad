<s>
Mechy	mech	k1gInPc1	mech
(	(	kIx(	(
<g/>
Bryopsida	Bryopsida	k1gFnSc1	Bryopsida
(	(	kIx(	(
<g/>
sensu	sens	k1gInSc2	sens
lato	lata	k1gMnSc5	lata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bryophyta	Bryophyta	k1gFnSc1	Bryophyta
(	(	kIx(	(
<g/>
sensu	sens	k1gInSc2	sens
stricto	stricto	k1gNnSc1	stricto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Musci	Musce	k1gMnPc1	Musce
či	či	k8xC	či
Muscophyta	Muscophyta	k1gFnSc1	Muscophyta
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
výtrusné	výtrusný	k2eAgFnPc1d1	výtrusná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgMnPc2	který
nejsou	být	k5eNaImIp3nP	být
plně	plně	k6eAd1	plně
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
cévní	cévní	k2eAgInPc1d1	cévní
svazky	svazek	k1gInPc1	svazek
a	a	k8xC	a
gametofyt	gametofyt	k1gInSc1	gametofyt
výrazně	výrazně	k6eAd1	výrazně
převládá	převládat	k5eAaImIp3nS	převládat
nad	nad	k7c7	nad
sporofytem	sporofyt	k1gInSc7	sporofyt
<g/>
.	.	kIx.	.
</s>
