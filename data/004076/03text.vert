<s>
Mechy	mech	k1gInPc1	mech
(	(	kIx(	(
<g/>
Bryopsida	Bryopsida	k1gFnSc1	Bryopsida
(	(	kIx(	(
<g/>
sensu	sens	k1gInSc2	sens
lato	lata	k1gMnSc5	lata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bryophyta	Bryophyta	k1gFnSc1	Bryophyta
(	(	kIx(	(
<g/>
sensu	sens	k1gInSc2	sens
stricto	stricto	k1gNnSc1	stricto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Musci	Musce	k1gMnPc1	Musce
či	či	k8xC	či
Muscophyta	Muscophyta	k1gFnSc1	Muscophyta
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
výtrusné	výtrusný	k2eAgFnPc1d1	výtrusná
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgMnPc2	který
nejsou	být	k5eNaImIp3nP	být
plně	plně	k6eAd1	plně
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
cévní	cévní	k2eAgInPc1d1	cévní
svazky	svazek	k1gInPc1	svazek
a	a	k8xC	a
gametofyt	gametofyt	k1gInSc1	gametofyt
výrazně	výrazně	k6eAd1	výrazně
převládá	převládat	k5eAaImIp3nS	převládat
nad	nad	k7c7	nad
sporofytem	sporofyt	k1gInSc7	sporofyt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradičního	tradiční	k2eAgInSc2d1	tradiční
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
hlevíky	hlevík	k1gMnPc7	hlevík
a	a	k8xC	a
játrovkami	játrovka	k1gFnPc7	játrovka
řadily	řadit	k5eAaImAgFnP	řadit
do	do	k7c2	do
oddělení	oddělení	k1gNnSc2	oddělení
mechorosty	mechorost	k1gInPc4	mechorost
<g/>
,	,	kIx,	,
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
pojetí	pojetí	k1gNnSc6	pojetí
jsou	být	k5eAaImIp3nP	být
vlastním	vlastní	k2eAgNnSc7d1	vlastní
oddělením	oddělení	k1gNnSc7	oddělení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
v	v	k7c6	v
700	[number]	k4	700
rodech	rod	k1gInPc6	rod
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhé	druhý	k4xOgNnSc4	druhý
nejpočetnější	početní	k2eAgNnSc4d3	nejpočetnější
oddělení	oddělení	k1gNnSc4	oddělení
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
po	po	k7c6	po
krytosemenných	krytosemenný	k2eAgFnPc6d1	krytosemenná
<g/>
.	.	kIx.	.
</s>
<s>
Mechy	mech	k1gInPc1	mech
jsou	být	k5eAaImIp3nP	být
zelené	zelený	k2eAgFnPc4d1	zelená
vyšší	vysoký	k2eAgFnPc4d2	vyšší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
necévnaté	cévnatý	k2eNgFnPc1d1	cévnatý
rostliny	rostlina	k1gFnPc1	rostlina
malého	malý	k2eAgInSc2d1	malý
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
,	,	kIx,	,
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
schopností	schopnost	k1gFnSc7	schopnost
zadržovat	zadržovat	k5eAaImF	zadržovat
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
preferují	preferovat	k5eAaImIp3nP	preferovat
vlhká	vlhký	k2eAgNnPc1d1	vlhké
a	a	k8xC	a
stinná	stinný	k2eAgNnPc1d1	stinné
stanoviště	stanoviště	k1gNnPc1	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
pionýrské	pionýrský	k2eAgFnPc1d1	Pionýrská
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životním	životní	k2eAgInSc6d1	životní
cyklu	cyklus	k1gInSc6	cyklus
výrazně	výrazně	k6eAd1	výrazně
převládá	převládat	k5eAaImIp3nS	převládat
haploidní	haploidní	k2eAgInSc4d1	haploidní
gametofyt	gametofyt	k1gInSc4	gametofyt
<g/>
.	.	kIx.	.
</s>
<s>
Gametofyt	gametofyt	k1gInSc1	gametofyt
<g/>
,	,	kIx,	,
mechová	mechový	k2eAgFnSc1d1	mechová
rostlinka	rostlinka	k1gFnSc1	rostlinka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
fotosyntézy	fotosyntéza	k1gFnPc1	fotosyntéza
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
složitou	složitý	k2eAgFnSc4d1	složitá
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Stélka	stélka	k1gFnSc1	stélka
je	být	k5eAaImIp3nS	být
rozlišena	rozlišit	k5eAaPmNgFnS	rozlišit
na	na	k7c4	na
kořínky	kořínek	k1gInPc4	kořínek
<g/>
,	,	kIx,	,
příchytná	příchytný	k2eAgNnPc1d1	příchytné
vlákna	vlákno	k1gNnPc1	vlákno
(	(	kIx(	(
<g/>
rhizoidy	rhizoid	k1gInPc1	rhizoid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lodyžku	lodyžka	k1gFnSc4	lodyžka
(	(	kIx(	(
<g/>
kauloid	kauloid	k1gInSc4	kauloid
<g/>
)	)	kIx)	)
a	a	k8xC	a
lístky	lístek	k1gInPc4	lístek
(	(	kIx(	(
<g/>
fyloidy	fyloida	k1gFnPc4	fyloida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnohobuněčné	mnohobuněčný	k2eAgInPc1d1	mnohobuněčný
rhizoidy	rhizoid	k1gInPc1	rhizoid
fixují	fixovat	k5eAaImIp3nP	fixovat
mechovou	mechový	k2eAgFnSc4d1	mechová
rostlinku	rostlinka	k1gFnSc4	rostlinka
k	k	k7c3	k
podkladu	podklad	k1gInSc3	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Lodyžka	lodyžka	k1gFnSc1	lodyžka
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
diferencovaná	diferencovaný	k2eAgNnPc4d1	diferencované
pletiva	pletivo	k1gNnPc4	pletivo
<g/>
,	,	kIx,	,
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
i	i	k9	i
jednoduchá	jednoduchý	k2eAgNnPc4d1	jednoduché
vodivá	vodivý	k2eAgNnPc4d1	vodivé
pletiva	pletivo	k1gNnPc4	pletivo
<g/>
.	.	kIx.	.
</s>
<s>
Stěna	stěna	k1gFnSc1	stěna
lodyžky	lodyžka	k1gFnSc2	lodyžka
je	být	k5eAaImIp3nS	být
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
ztlustlá	ztlustlý	k2eAgFnSc1d1	ztlustlá
<g/>
,	,	kIx,	,
pokožka	pokožka	k1gFnSc1	pokožka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pigmentovaná	pigmentovaný	k2eAgFnSc1d1	pigmentovaná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
olistěná	olistěný	k2eAgFnSc1d1	olistěná
<g/>
,	,	kIx,	,
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
zelené	zelený	k2eAgInPc1d1	zelený
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Gametangia	gametangium	k1gNnPc1	gametangium
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
lodyžky	lodyžka	k1gFnSc2	lodyžka
<g/>
.	.	kIx.	.
</s>
<s>
Mechové	mechový	k2eAgFnPc1d1	mechová
rostlinky	rostlinka	k1gFnPc1	rostlinka
mají	mít	k5eAaImIp3nP	mít
oddělená	oddělený	k2eAgNnPc4d1	oddělené
pohlaví	pohlaví	k1gNnPc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Samčí	samčí	k2eAgInPc1d1	samčí
pohlavní	pohlavní	k2eAgInPc1d1	pohlavní
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
pelatky	pelatka	k1gFnPc1	pelatka
(	(	kIx(	(
<g/>
antheridia	antheridium	k1gNnPc1	antheridium
<g/>
)	)	kIx)	)
produkují	produkovat	k5eAaImIp3nP	produkovat
dvoubičíkaté	dvoubičíkatý	k2eAgInPc4d1	dvoubičíkatý
spermatozoidy	spermatozoid	k1gInPc4	spermatozoid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
prostředí	prostředí	k1gNnSc6	prostředí
aktivně	aktivně	k6eAd1	aktivně
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
<g/>
.	.	kIx.	.
</s>
<s>
Samičí	samičí	k2eAgInPc1d1	samičí
zárodečníky	zárodečník	k1gInPc1	zárodečník
(	(	kIx(	(
<g/>
archegonia	archegonium	k1gNnPc1	archegonium
<g/>
)	)	kIx)	)
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velkou	velký	k2eAgFnSc4d1	velká
nepohyblivou	pohyblivý	k2eNgFnSc4d1	nepohyblivá
vaječnou	vaječný	k2eAgFnSc4d1	vaječná
buňku	buňka	k1gFnSc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oplození	oplození	k1gNnSc6	oplození
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
gemetofytu	gemetofyt	k1gInSc6	gemetofyt
sporofyt	sporofyt	k1gInSc1	sporofyt
<g/>
.	.	kIx.	.
</s>
<s>
Sporofyt	sporofyt	k1gInSc1	sporofyt
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
nohou	noha	k1gFnSc7	noha
<g/>
,	,	kIx,	,
štětem	štět	k1gInSc7	štět
a	a	k8xC	a
sporangiem	sporangium	k1gNnSc7	sporangium
-	-	kIx~	-
tobolkou	tobolka	k1gFnSc7	tobolka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
produkuje	produkovat	k5eAaImIp3nS	produkovat
haploidní	haploidní	k2eAgInPc4d1	haploidní
výtrusy	výtrus	k1gInPc4	výtrus
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rašeliníků	rašeliník	k1gInPc2	rašeliník
a	a	k8xC	a
štěrbovek	štěrbovka	k1gFnPc2	štěrbovka
štět	štět	k1gInSc4	štět
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Tobolka	tobolka	k1gFnSc1	tobolka
má	mít	k5eAaImIp3nS	mít
dvoubuněčné	dvoubuněčný	k2eAgInPc4d1	dvoubuněčný
průduchy	průduch	k1gInPc4	průduch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyztužena	vyztužit	k5eAaPmNgFnS	vyztužit
středním	střední	k2eAgInSc7d1	střední
sloupkem	sloupek	k1gInSc7	sloupek
(	(	kIx(	(
<g/>
columela	columela	k1gFnSc1	columela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kterého	který	k3yRgNnSc2	který
je	být	k5eAaImIp3nS	být
výtrusorodé	výtrusorodý	k2eAgNnSc1d1	výtrusorodý
pletivo	pletivo	k1gNnSc1	pletivo
(	(	kIx(	(
<g/>
archesporum	archesporum	k1gInSc1	archesporum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
buňky	buňka	k1gFnPc1	buňka
prodělávají	prodělávat	k5eAaImIp3nP	prodělávat
meiózu	meióza	k1gFnSc4	meióza
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
výtrusy	výtrus	k1gInPc7	výtrus
<g/>
.	.	kIx.	.
</s>
<s>
Sporangium	Sporangium	k1gNnSc1	Sporangium
je	být	k5eAaImIp3nS	být
kryté	krytý	k2eAgNnSc1d1	kryté
víčkem	víčko	k1gNnSc7	víčko
a	a	k8xC	a
čepičkou	čepička	k1gFnSc7	čepička
(	(	kIx(	(
<g/>
kalyptra	kalyptra	k1gFnSc1	kalyptra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výtrusy	výtrus	k1gInPc1	výtrus
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vypadávají	vypadávat	k5eAaImIp3nP	vypadávat
postupně	postupně	k6eAd1	postupně
díky	díky	k7c3	díky
obústí	obústí	k1gNnSc3	obústí
<g/>
,	,	kIx,	,
věnci	věnec	k1gInPc7	věnec
hydroskopických	hydroskopický	k2eAgInPc2d1	hydroskopický
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Obústí	obústí	k1gNnSc1	obústí
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitý	důležitý	k2eAgInSc1d1	důležitý
systematický	systematický	k2eAgInSc1d1	systematický
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Oplození	oplození	k1gNnSc1	oplození
může	moct	k5eAaImIp3nS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
jen	jen	k9	jen
v	v	k7c6	v
dostatečně	dostatečně	k6eAd1	dostatečně
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
prostředí	prostředí	k1gNnSc6	prostředí
(	(	kIx(	(
<g/>
za	za	k7c4	za
deště	dešť	k1gInPc4	dešť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
spermatozoidy	spermatozoid	k1gInPc1	spermatozoid
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
vaječným	vaječný	k2eAgFnPc3d1	vaječná
buňkám	buňka	k1gFnPc3	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oplození	oplození	k1gNnSc6	oplození
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
diploidní	diploidní	k2eAgFnSc1d1	diploidní
zygota	zygota	k1gFnSc1	zygota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
ve	v	k7c4	v
sporofyt	sporofyt	k1gInSc4	sporofyt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
mateřského	mateřský	k2eAgInSc2d1	mateřský
gametofytu	gametofyt	k1gInSc2	gametofyt
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
plně	plně	k6eAd1	plně
závislý	závislý	k2eAgMnSc1d1	závislý
<g/>
.	.	kIx.	.
</s>
<s>
Mechy	mech	k1gInPc1	mech
nemají	mít	k5eNaImIp3nP	mít
cévní	cévní	k2eAgInPc1d1	cévní
svazky	svazek	k1gInPc1	svazek
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
schopnost	schopnost	k1gFnSc1	schopnost
rozvádět	rozvádět	k5eAaImF	rozvádět
vodu	voda	k1gFnSc4	voda
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
omezená	omezený	k2eAgFnSc1d1	omezená
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
jejich	jejich	k3xOp3gInSc2	jejich
malého	malý	k2eAgInSc2d1	malý
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Vodu	voda	k1gFnSc4	voda
přijímají	přijímat	k5eAaImIp3nP	přijímat
celým	celý	k2eAgInSc7d1	celý
povrchem	povrch	k1gInSc7	povrch
stélky	stélka	k1gFnSc2	stélka
a	a	k8xC	a
rozvádějí	rozvádět	k5eAaImIp3nP	rozvádět
ji	on	k3xPp3gFnSc4	on
vodivými	vodivý	k2eAgNnPc7d1	vodivé
pletivy	pletivo	k1gNnPc7	pletivo
(	(	kIx(	(
<g/>
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
buňky	buňka	k1gFnPc1	buňka
hydroidy	hydroida	k1gFnPc1	hydroida
<g/>
,	,	kIx,	,
kapilární	kapilární	k2eAgFnPc1d1	kapilární
prostory	prostora	k1gFnPc1	prostora
uvnitř	uvnitř	k7c2	uvnitř
stélky	stélka	k1gFnSc2	stélka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
po	po	k7c6	po
snadno	snadno	k6eAd1	snadno
smáčivém	smáčivý	k2eAgInSc6d1	smáčivý
povrchu	povrch	k1gInSc6	povrch
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
vyvinuta	vyvinut	k2eAgFnSc1d1	vyvinuta
voskovitá	voskovitý	k2eAgFnSc1d1	voskovitá
kutikula	kutikula	k1gFnSc1	kutikula
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
usměrnění	usměrnění	k1gNnSc3	usměrnění
toku	tok	k1gInSc2	tok
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
určitých	určitý	k2eAgFnPc2d1	určitá
částí	část	k1gFnPc2	část
mechové	mechový	k2eAgFnSc2d1	mechová
rostlinky	rostlinka	k1gFnSc2	rostlinka
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
mechy	mech	k1gInPc1	mech
hospodaří	hospodařit	k5eAaImIp3nP	hospodařit
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
využít	využít	k5eAaPmF	využít
i	i	k9	i
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgFnPc2d1	malá
a	a	k8xC	a
horizontálních	horizontální	k2eAgFnPc2d1	horizontální
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rosa	rosa	k1gFnSc1	rosa
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Velký	velký	k2eAgInSc4d1	velký
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
snadná	snadný	k2eAgFnSc1d1	snadná
prostupnost	prostupnost	k1gFnSc1	prostupnost
stélky	stélka	k1gFnSc2	stélka
také	také	k9	také
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mechy	mech	k1gInPc1	mech
jsou	být	k5eAaImIp3nP	být
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
ovzduší	ovzduší	k1gNnSc2	ovzduší
i	i	k8xC	i
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
bioindikátory	bioindikátor	k1gInPc1	bioindikátor
znečištění	znečištění	k1gNnSc2	znečištění
<g/>
.	.	kIx.	.
</s>
<s>
Mechy	mech	k1gInPc1	mech
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
také	také	k9	také
velkou	velký	k2eAgFnSc7d1	velká
substrátovou	substrátový	k2eAgFnSc7d1	substrátová
specifitou	specifita	k1gFnSc7	specifita
(	(	kIx(	(
<g/>
pH	ph	kA	ph
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
charakter	charakter	k1gInSc4	charakter
substrátu	substrát	k1gInSc2	substrát
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rhizoidy	rhizoida	k1gFnPc1	rhizoida
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
tak	tak	k9	tak
dobré	dobrý	k2eAgNnSc4d1	dobré
využití	využití	k1gNnSc4	využití
živin	živina	k1gFnPc2	živina
ze	z	k7c2	z
substrátu	substrát	k1gInSc2	substrát
jako	jako	k8xS	jako
u	u	k7c2	u
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
mechové	mechový	k2eAgFnPc1d1	mechová
rostlinky	rostlinka	k1gFnPc1	rostlinka
jsou	být	k5eAaImIp3nP	být
přizpůsobené	přizpůsobený	k2eAgFnPc1d1	přizpůsobená
podmínkám	podmínka	k1gFnPc3	podmínka
stanoviště	stanoviště	k1gNnPc1	stanoviště
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
způsobem	způsob	k1gInSc7	způsob
růstu	růst	k1gInSc2	růst
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
růstová	růstový	k2eAgFnSc1d1	růstová
forma	forma	k1gFnSc1	forma
mechu	mech	k1gInSc2	mech
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
růstové	růstový	k2eAgFnPc4d1	růstová
formy	forma	k1gFnPc4	forma
<g/>
:	:	kIx,	:
vzpřímené	vzpřímený	k2eAgInPc4d1	vzpřímený
(	(	kIx(	(
<g/>
orthotropické	orthotropický	k2eAgInPc4d1	orthotropický
<g/>
)	)	kIx)	)
mechy	mech	k1gInPc1	mech
poléhavé	poléhavý	k2eAgInPc1d1	poléhavý
<g/>
,	,	kIx,	,
plazivé	plazivý	k2eAgInPc1d1	plazivý
(	(	kIx(	(
<g/>
plagiotropické	plagiotropický	k2eAgInPc1d1	plagiotropický
<g/>
)	)	kIx)	)
mechy	mech	k1gInPc1	mech
Jen	jen	k9	jen
málo	málo	k1gNnSc1	málo
mechů	mech	k1gInPc2	mech
žije	žít	k5eAaImIp3nS	žít
jednotlivě	jednotlivě	k6eAd1	jednotlivě
<g/>
.	.	kIx.	.
</s>
<s>
Interakcí	interakce	k1gFnSc7	interakce
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
mechových	mechový	k2eAgFnPc2d1	mechová
rostlinek	rostlinka	k1gFnPc2	rostlinka
a	a	k8xC	a
prostředí	prostředí	k1gNnSc6	prostředí
vznikají	vznikat	k5eAaImIp3nP	vznikat
životní	životní	k2eAgFnPc4d1	životní
formy	forma	k1gFnPc4	forma
mechů	mech	k1gInPc2	mech
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
druh	druh	k1gInSc1	druh
mechu	mech	k1gInSc2	mech
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
i	i	k9	i
více	hodně	k6eAd2	hodně
životních	životní	k2eAgFnPc2d1	životní
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jednotlivě	jednotlivě	k6eAd1	jednotlivě
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
mechů	mech	k1gInPc2	mech
se	se	k3xPyFc4	se
životní	životní	k2eAgFnSc1d1	životní
forma	forma	k1gFnSc1	forma
rovná	rovnat	k5eAaImIp3nS	rovnat
formě	forma	k1gFnSc3	forma
růstové	růstový	k2eAgFnSc3d1	růstová
<g/>
.	.	kIx.	.
jednotlivci	jednotlivec	k1gMnSc6	jednotlivec
povlaky	povlak	k1gInPc1	povlak
nízké	nízký	k2eAgInPc1d1	nízký
trsy	trs	k1gInPc1	trs
vysoké	vysoký	k2eAgInPc1d1	vysoký
trsy	trs	k1gInPc4	trs
ocásky	ocásek	k1gInPc1	ocásek
polštáře	polštář	k1gInSc2	polštář
propletené	propletený	k2eAgInPc1d1	propletený
porosty	porost	k1gInPc1	porost
záclony	záclona	k1gFnSc2	záclona
<g/>
,	,	kIx,	,
visící	visící	k2eAgInPc1d1	visící
mechy	mech	k1gInPc1	mech
Zadržují	zadržovat	k5eAaImIp3nP	zadržovat
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
zpevňovat	zpevňovat	k5eAaImF	zpevňovat
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Rašelina	rašelina	k1gFnSc1	rašelina
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
jako	jako	k8xS	jako
fosilní	fosilní	k2eAgInSc1d1	fosilní
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Rašelina	rašelina	k1gFnSc1	rašelina
a	a	k8xC	a
výluhy	výluh	k1gInPc4	výluh
zní	znět	k5eAaImIp3nS	znět
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
akvaristice	akvaristika	k1gFnSc6	akvaristika
<g/>
,	,	kIx,	,
též	též	k9	též
jako	jako	k9	jako
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
dno	dno	k1gNnSc4	dno
akvária	akvárium	k1gNnSc2	akvárium
Systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
proměnlivý	proměnlivý	k2eAgMnSc1d1	proměnlivý
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
třídy	třída	k1gFnPc1	třída
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
přidávaly	přidávat	k5eAaImAgFnP	přidávat
se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
se	se	k3xPyFc4	se
úrovní	úroveň	k1gFnSc7	úroveň
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
poznání	poznání	k1gNnSc2	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tříd	třída	k1gFnPc2	třída
Takakiopsida	Takakiopsid	k1gMnSc2	Takakiopsid
<g/>
,	,	kIx,	,
Andreaeopsida	Andreaeopsid	k1gMnSc2	Andreaeopsid
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ještě	ještě	k9	ještě
Tetraphidopsida	Tetraphidopsida	k1gFnSc1	Tetraphidopsida
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
několik	několik	k4yIc1	několik
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Sphagnopsida	Sphagnopsida	k1gFnSc1	Sphagnopsida
-	-	kIx~	-
rašeliníky	rašeliník	k1gInPc4	rašeliník
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Sphagnales	Sphagnales	k1gInSc1	Sphagnales
-	-	kIx~	-
rašeliníkotvaré	rašeliníkotvarý	k2eAgInPc4d1	rašeliníkotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Ambuchananiales	Ambuchananiales	k1gInSc1	Ambuchananiales
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Takakiopsida	Takakiopsida	k1gFnSc1	Takakiopsida
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Andreaeopsida	Andreaeopsida	k1gFnSc1	Andreaeopsida
-	-	kIx~	-
štěrbovky	štěrbovka	k1gFnSc2	štěrbovka
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Andreaeales	Andreaeales	k1gInSc1	Andreaeales
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
štěrbovkotvaré	štěrbovkotvarý	k2eAgFnPc1d1	štěrbovkotvarý
třída	třída	k1gFnSc1	třída
<g/>
:	:	kIx,	:
Briopsida	Briopsida	k1gFnSc1	Briopsida
-	-	kIx~	-
pravé	pravý	k2eAgInPc1d1	pravý
mechy	mech	k1gInPc1	mech
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Polytrichales	Polytrichales	k1gInSc1	Polytrichales
-	-	kIx~	-
ploníkotvaré	ploníkotvarý	k2eAgFnSc6d1	ploníkotvarý
–	–	k?	–
někdy	někdy	k6eAd1	někdy
samostatná	samostatný	k2eAgFnSc1d1	samostatná
třída	třída	k1gFnSc1	třída
Polytrichopsida	Polytrichopsida	k1gFnSc1	Polytrichopsida
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Tetraphidales	Tetraphidales	k1gInSc1	Tetraphidales
-	-	kIx~	-
čtyřzoubkotvaré	čtyřzoubkotvarý	k2eAgFnSc6d1	čtyřzoubkotvarý
–	–	k?	–
někdy	někdy	k6eAd1	někdy
samostatná	samostatný	k2eAgFnSc1d1	samostatná
třída	třída	k1gFnSc1	třída
Tetraphidopsida	Tetraphidopsida	k1gFnSc1	Tetraphidopsida
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Buxbaumiales	Buxbaumiales	k1gInSc1	Buxbaumiales
-	-	kIx~	-
šikouškotvaré	šikouškotvarý	k2eAgInPc4d1	šikouškotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Diphysciales	Diphysciales	k1gInSc4	Diphysciales
-	-	kIx~	-
krčankotvaré	krčankotvarý	k2eAgInPc4d1	krčankotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Timmiales	Timmiales	k1gInSc1	Timmiales
-	-	kIx~	-
podnožitkotvaré	podnožitkotvarý	k2eAgFnSc2d1	podnožitkotvarý
řád	řád	k1gInSc4	řád
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Encalyptales	Encalyptales	k1gInSc4	Encalyptales
-	-	kIx~	-
čepičnatkotvaré	čepičnatkotvarý	k2eAgInPc4d1	čepičnatkotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Funariales	Funariales	k1gInSc4	Funariales
-	-	kIx~	-
zkrutkotvaré	zkrutkotvarý	k2eAgInPc4d1	zkrutkotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Archidiales	Archidiales	k1gInSc4	Archidiales
-	-	kIx~	-
úpolníčkotvaré	úpolníčkotvarý	k2eAgInPc4d1	úpolníčkotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Grimmiales	Grimmiales	k1gInSc4	Grimmiales
-	-	kIx~	-
děrkavkotvaré	děrkavkotvarý	k2eAgInPc4d1	děrkavkotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Dicranales	Dicranales	k1gInSc4	Dicranales
-	-	kIx~	-
dvouhrotcotvaré	dvouhrotcotvarý	k2eAgInPc4d1	dvouhrotcotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Pottiales	Pottiales	k1gInSc4	Pottiales
-	-	kIx~	-
pozemničkotvaré	pozemničkotvarý	k2eAgInPc4d1	pozemničkotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Splachnales	Splachnales	k1gInSc4	Splachnales
-	-	kIx~	-
volatkotvaré	volatkotvarý	k2eAgInPc4d1	volatkotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Orthotrichales	Orthotrichales	k1gInSc4	Orthotrichales
-	-	kIx~	-
šurpekotvaré	šurpekotvarý	k2eAgInPc4d1	šurpekotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Hedwigiales	Hedwigiales	k1gInSc4	Hedwigiales
-	-	kIx~	-
těhovcotvaré	těhovcotvarý	k2eAgInPc4d1	těhovcotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Bryales	Bryales	k1gInSc4	Bryales
-	-	kIx~	-
prutníkotvaré	prutníkotvarý	k2eAgInPc4d1	prutníkotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Rhizogoniales	Rhizogoniales	k1gInSc1	Rhizogoniales
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Ptychomniales	Ptychomniales	k1gInSc1	Ptychomniales
řád	řád	k1gInSc1	řád
<g/>
:	:	kIx,	:
Hookeriales	Hookeriales	k1gInSc1	Hookeriales
-	-	kIx~	-
kápuškotvaré	kápuškotvarý	k2eAgInPc4d1	kápuškotvarý
řád	řád	k1gInSc4	řád
<g/>
:	:	kIx,	:
Hypnales	Hypnales	k1gInSc1	Hypnales
-	-	kIx~	-
rokytotvaré	rokytotvarý	k2eAgNnSc1d1	rokytotvarý
</s>
