<s>
Pyroklastický	pyroklastický	k2eAgInSc1d1	pyroklastický
proud	proud	k1gInSc1	proud
označovaný	označovaný	k2eAgInSc1d1	označovaný
také	také	k9	také
jako	jako	k9	jako
žhavé	žhavý	k2eAgNnSc1d1	žhavé
mračno	mračno	k1gNnSc1	mračno
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
nuée	nuéus	k1gMnSc5	nuéus
ardente	ardent	k1gMnSc5	ardent
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInSc1d1	vyskytující
projev	projev	k1gInSc1	projev
explozivních	explozivní	k2eAgFnPc2d1	explozivní
sopečných	sopečný	k2eAgFnPc2d1	sopečná
erupcí	erupce	k1gFnPc2	erupce
<g/>
.	.	kIx.	.
</s>
