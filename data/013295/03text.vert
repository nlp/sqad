<p>
<s>
Pyroklastický	pyroklastický	k2eAgInSc1d1	pyroklastický
proud	proud	k1gInSc1	proud
označovaný	označovaný	k2eAgInSc1d1	označovaný
také	také	k9	také
jako	jako	k9	jako
žhavé	žhavý	k2eAgNnSc1d1	žhavé
mračno	mračno	k1gNnSc1	mračno
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
nuée	nuéus	k1gMnSc5	nuéus
ardente	ardent	k1gMnSc5	ardent
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInSc1d1	vyskytující
projev	projev	k1gInSc1	projev
explozivních	explozivní	k2eAgFnPc2d1	explozivní
sopečných	sopečný	k2eAgFnPc2d1	sopečná
erupcí	erupce	k1gFnPc2	erupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pohyblivou	pohyblivý	k2eAgFnSc4d1	pohyblivá
<g/>
,	,	kIx,	,
fluidizovanou	fluidizovaný	k2eAgFnSc4d1	fluidizovaný
směs	směs	k1gFnSc4	směs
žhavých	žhavý	k2eAgInPc2d1	žhavý
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
úlomků	úlomek	k1gInPc2	úlomek
magmatu	magma	k1gNnSc2	magma
a	a	k8xC	a
sopečného	sopečný	k2eAgInSc2d1	sopečný
popela	popel	k1gInSc2	popel
<g/>
,	,	kIx,	,
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
od	od	k7c2	od
100	[number]	k4	100
do	do	k7c2	do
1100	[number]	k4	1100
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
lavina	lavina	k1gFnSc1	lavina
<g/>
)	)	kIx)	)
po	po	k7c6	po
sopečném	sopečný	k2eAgInSc6d1	sopečný
svahu	svah	k1gInSc6	svah
dolů	dolů	k6eAd1	dolů
velkými	velký	k2eAgFnPc7d1	velká
rychlostmi	rychlost	k1gFnPc7	rychlost
(	(	kIx(	(
<g/>
150	[number]	k4	150
<g/>
-	-	kIx~	-
<g/>
700	[number]	k4	700
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgFnPc7	tento
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
devastujících	devastující	k2eAgFnPc2d1	devastující
forem	forma	k1gFnPc2	forma
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Pyroklastické	pyroklastický	k2eAgInPc1d1	pyroklastický
proudy	proud	k1gInPc1	proud
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
při	při	k7c6	při
explozivních	explozivní	k2eAgFnPc6d1	explozivní
erupcích	erupce	k1gFnPc6	erupce
viskózního	viskózní	k2eAgNnSc2d1	viskózní
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnSc2	příčina
vzniku	vznik	k1gInSc2	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Příčiny	příčina	k1gFnPc4	příčina
vzniku	vznik	k1gInSc2	vznik
pyroklastických	pyroklastický	k2eAgInPc2d1	pyroklastický
proudů	proud	k1gInPc2	proud
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgFnPc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kolaps	kolaps	k1gInSc1	kolaps
mraku	mrak	k1gInSc2	mrak
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
popela	popel	k1gInSc2	popel
(	(	kIx(	(
<g/>
peléeský	peléeský	k2eAgInSc1d1	peléeský
typ	typ	k1gInSc1	typ
erupce	erupce	k1gFnSc2	erupce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tlak	tlak	k1gInSc1	tlak
exploze	exploze	k1gFnSc2	exploze
není	být	k5eNaImIp3nS	být
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
k	k	k7c3	k
vytlačení	vytlačení	k1gNnSc3	vytlačení
mraků	mrak	k1gInPc2	mrak
popele	popel	k1gInSc2	popel
vysoko	vysoko	k6eAd1	vysoko
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
(	(	kIx(	(
<g/>
pliniovský	pliniovský	k2eAgInSc1d1	pliniovský
typ	typ	k1gInSc1	typ
erupce	erupce	k1gFnSc2	erupce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyvržený	vyvržený	k2eAgInSc1d1	vyvržený
materiál	materiál	k1gInSc1	materiál
padá	padat	k5eAaImIp3nS	padat
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
dolů	dolů	k6eAd1	dolů
po	po	k7c6	po
svazích	svah	k1gInPc6	svah
sopky	sopka	k1gFnSc2	sopka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolaps	kolaps	k1gInSc1	kolaps
sopečného	sopečný	k2eAgInSc2d1	sopečný
dómu	dóm	k1gInSc2	dóm
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
následný	následný	k2eAgInSc1d1	následný
rozpad	rozpad	k1gInSc1	rozpad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zničení	zničení	k1gNnSc1	zničení
části	část	k1gFnSc2	část
sopky	sopka	k1gFnSc2	sopka
při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
explozi	exploze	k1gFnSc6	exploze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Zpěnění	zpěnění	k1gNnPc1	zpěnění
<g/>
"	"	kIx"	"
lávy	láva	k1gFnSc2	láva
během	během	k7c2	během
procesu	proces	k1gInSc2	proces
jejího	její	k3xOp3gNnSc2	její
odplynění	odplynění	k1gNnSc2	odplynění
při	při	k7c6	při
erupci	erupce	k1gFnSc6	erupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pyroklastických	pyroklastický	k2eAgInPc2d1	pyroklastický
proudů	proud	k1gInPc2	proud
==	==	k?	==
</s>
</p>
<p>
<s>
Pyroklastické	pyroklastický	k2eAgInPc1d1	pyroklastický
proudy	proud	k1gInPc1	proud
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
nebezpečnější	bezpečný	k2eNgFnSc1d2	nebezpečnější
než	než	k8xS	než
samotná	samotný	k2eAgFnSc1d1	samotná
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
láva	láva	k1gFnSc1	láva
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
požáry	požár	k1gInPc4	požár
<g/>
,	,	kIx,	,
či	či	k8xC	či
usmrtit	usmrtit	k5eAaPmF	usmrtit
člověka	člověk	k1gMnSc4	člověk
během	během	k7c2	během
několika	několik	k4yIc3	několik
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
moři	moře	k1gNnSc6	moře
mohou	moct	k5eAaImIp3nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
velké	velký	k2eAgFnPc4d1	velká
vlny	vlna	k1gFnPc4	vlna
tsunami	tsunami	k1gNnSc2	tsunami
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pyroklastický	pyroklastický	k2eAgInSc1d1	pyroklastický
proud	proud	k1gInSc1	proud
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
po	po	k7c6	po
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
a	a	k8xC	a
popsáno	popsat	k5eAaPmNgNnS	popsat
již	již	k6eAd1	již
při	při	k7c6	při
výbuchu	výbuch	k1gInSc6	výbuch
sopky	sopka	k1gFnSc2	sopka
Krakatoa	Krakato	k1gInSc2	Krakato
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pyroklastický	pyroklastický	k2eAgInSc1d1	pyroklastický
proud	proud	k1gInSc1	proud
ze	z	k7c2	z
sopky	sopka	k1gFnSc2	sopka
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
i	i	k9	i
přes	přes	k7c4	přes
40	[number]	k4	40
km	km	kA	km
vzdálené	vzdálený	k2eAgNnSc1d1	vzdálené
pobřeží	pobřeží	k1gNnSc1	pobřeží
Sumatry	Sumatra	k1gFnSc2	Sumatra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výbuchu	výbuch	k1gInSc6	výbuch
karibské	karibský	k2eAgFnSc2d1	karibská
sopky	sopka	k1gFnSc2	sopka
Soufriè	Soufriè	k1gFnSc2	Soufriè
Hills	Hillsa	k1gFnPc2	Hillsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgInS	být
nafilmován	nafilmován	k2eAgInSc1d1	nafilmován
pyroklastický	pyroklastický	k2eAgInSc1d1	pyroklastický
proud	proud	k1gInSc1	proud
putující	putující	k2eAgInSc1d1	putující
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
německé	německý	k2eAgFnSc2d1	německá
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Kielu	Kiel	k1gInSc6	Kiel
při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
se	se	k3xPyFc4	se
potopí	potopit	k5eAaPmIp3nS	potopit
těžší	těžký	k2eAgFnSc1d2	těžší
částice	částice	k1gFnSc1	částice
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
horkem	horko	k1gNnSc7	horko
vypařovat	vypařovat	k5eAaImF	vypařovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
odlehčený	odlehčený	k2eAgInSc1d1	odlehčený
proud	proud	k1gInSc1	proud
popela	popel	k1gInSc2	popel
a	a	k8xC	a
plynů	plyn	k1gInPc2	plyn
poháněný	poháněný	k2eAgMnSc1d1	poháněný
párou	pára	k1gFnSc7	pára
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
ještě	ještě	k6eAd1	ještě
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
předtím	předtím	k6eAd1	předtím
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
katastrof	katastrofa	k1gFnPc2	katastrofa
způsobených	způsobený	k2eAgFnPc2d1	způsobená
pyroklastickými	pyroklastický	k2eAgInPc7d1	pyroklastický
proudy	proud	k1gInPc7	proud
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
uvést	uvést	k5eAaPmF	uvést
nejznámější	známý	k2eAgMnPc4d3	nejznámější
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zničení	zničení	k1gNnSc1	zničení
města	město	k1gNnSc2	město
Pompeje	Pompeje	k1gInPc4	Pompeje
v	v	k7c6	v
roce	rok	k1gInSc6	rok
79	[number]	k4	79
při	při	k7c6	při
erupci	erupce	k1gFnSc6	erupce
sopky	sopka	k1gFnSc2	sopka
Vesuv	Vesuv	k1gInSc1	Vesuv
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
</s>
</p>
<p>
<s>
erupce	erupce	k1gFnSc1	erupce
Krakatau	Krakataus	k1gInSc2	Krakataus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
nejrozsáhlejším	rozsáhlý	k2eAgMnPc3d3	nejrozsáhlejší
známým	známý	k2eAgMnPc3d1	známý
pyroklastickým	pyroklastický	k2eAgMnPc3d1	pyroklastický
proudům	proud	k1gInPc3	proud
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
i	i	k9	i
vlny	vlna	k1gFnPc1	vlna
tsunami	tsunami	k1gNnSc2	tsunami
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
katastrofě	katastrofa	k1gFnSc6	katastrofa
utonulo	utonout	k5eAaPmAgNnS	utonout
nebo	nebo	k8xC	nebo
bylo	být	k5eAaImAgNnS	být
sežehnuto	sežehnout	k5eAaPmNgNnS	sežehnout
na	na	k7c4	na
36	[number]	k4	36
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zničeny	zničen	k2eAgFnPc4d1	zničena
stovky	stovka	k1gFnPc4	stovka
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
exploze	exploze	k1gFnSc1	exploze
sopky	sopka	k1gFnSc2	sopka
Mont	Mont	k1gInSc1	Mont
Pelée	Pelée	k1gFnSc4	Pelée
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zabila	zabít	k5eAaPmAgFnS	zabít
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
mračna	mračna	k1gFnSc1	mračna
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnout	k5eAaPmNgFnS	odhadnout
na	na	k7c4	na
1100	[number]	k4	1100
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
částečně	částečně	k6eAd1	částečně
roztavených	roztavený	k2eAgInPc2d1	roztavený
skleněných	skleněný	k2eAgInPc2d1	skleněný
artefaktů	artefakt	k1gInPc2	artefakt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
astronom	astronom	k1gMnSc1	astronom
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
Winifred	Winifred	k1gMnSc1	Winifred
Cameron	Cameron	k1gMnSc1	Cameron
<g/>
,	,	kIx,	,
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
klikatá	klikatý	k2eAgNnPc1d1	klikaté
údolí	údolí	k1gNnPc1	údolí
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Scroterovo	Scroterův	k2eAgNnSc1d1	Scroterův
údolí	údolí	k1gNnSc1	údolí
<g/>
)	)	kIx)	)
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zformována	zformovat	k5eAaPmNgFnS	zformovat
měsíční	měsíční	k2eAgFnSc7d1	měsíční
obdobou	obdoba	k1gFnSc7	obdoba
zemských	zemský	k2eAgInPc2d1	zemský
pyroklastických	pyroklastický	k2eAgInPc2d1	pyroklastický
proudů	proud	k1gInPc2	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oběti	oběť	k1gFnPc1	oběť
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pohlceny	pohlcen	k2eAgMnPc4d1	pohlcen
pyroklastickým	pyroklastický	k2eAgInSc7d1	pyroklastický
proudem	proud	k1gInSc7	proud
neshoří	shořet	k5eNaPmIp3nS	shořet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
zmrazeny	zmrazit	k5eAaPmNgFnP	zmrazit
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
promění	proměnit	k5eAaPmIp3nS	proměnit
se	se	k3xPyFc4	se
v	v	k7c6	v
sochy	socha	k1gFnPc1	socha
zachycující	zachycující	k2eAgInPc4d1	zachycující
poslední	poslední	k2eAgInPc4d1	poslední
okamžiky	okamžik	k1gInPc4	okamžik
jejich	jejich	k3xOp3gInSc2	jejich
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sigurdson	Sigurdson	k1gMnSc1	Sigurdson
<g/>
,	,	kIx,	,
Haraldur	Haraldur	k1gMnSc1	Haraldur
<g/>
:	:	kIx,	:
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
volcanoes	volcanoes	k1gMnSc1	volcanoes
<g/>
.	.	kIx.	.
</s>
<s>
Academic	Academic	k1gMnSc1	Academic
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
str	str	kA	str
546	[number]	k4	546
<g/>
-	-	kIx~	-
<g/>
548	[number]	k4	548
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-12-643140-X	[number]	k4	0-12-643140-X
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Vesuv	Vesuv	k1gInSc1	Vesuv
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pyroklastický	pyroklastický	k2eAgInSc4d1	pyroklastický
proud	proud	k1gInSc4	proud
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Definice	definice	k1gFnSc1	definice
podle	podle	k7c2	podle
USGS	USGS	kA	USGS
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Pyroclastic	Pyroclastice	k1gFnPc2	Pyroclastice
flow	flow	k?	flow
videos	videos	k1gMnSc1	videos
</s>
</p>
