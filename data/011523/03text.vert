<p>
<s>
PZL	PZL	kA	PZL
Kania	Kania	k1gFnSc1	Kania
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Luňák	Luňák	k1gMnSc1	Luňák
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
lehký	lehký	k2eAgInSc1d1	lehký
dvoumotorový	dvoumotorový	k2eAgInSc1d1	dvoumotorový
víceúčelový	víceúčelový	k2eAgInSc1d1	víceúčelový
vrtulník	vrtulník	k1gInSc1	vrtulník
s	s	k7c7	s
třílistým	třílistý	k2eAgInSc7d1	třílistý
nosným	nosný	k2eAgInSc7d1	nosný
a	a	k8xC	a
dvoulistým	dvoulistý	k2eAgInSc7d1	dvoulistý
tlačným	tlačný	k2eAgInSc7d1	tlačný
vyrovnávacím	vyrovnávací	k2eAgInSc7d1	vyrovnávací
rotorem	rotor	k1gInSc7	rotor
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nástupce	nástupce	k1gMnSc4	nástupce
sovětského	sovětský	k2eAgInSc2d1	sovětský
vrtulníku	vrtulník	k1gInSc2	vrtulník
Mil	míle	k1gFnPc2	míle
Mi-	Mi-	k1gFnSc4	Mi-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
Kania	Kanium	k1gNnSc2	Kanium
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
a	a	k8xC	a
vyráběn	vyráběn	k2eAgInSc1d1	vyráběn
polskou	polský	k2eAgFnSc7d1	polská
společností	společnost	k1gFnSc7	společnost
PZL-Świdnik	PZL-Świdnik	k1gMnSc1	PZL-Świdnik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
o	o	k7c6	o
výrobě	výroba	k1gFnSc6	výroba
lehkého	lehký	k2eAgInSc2d1	lehký
sovětského	sovětský	k2eAgInSc2d1	sovětský
vrtulníku	vrtulník	k1gInSc2	vrtulník
Mil	míle	k1gFnPc2	míle
Mi-	Mi-	k1gFnSc4	Mi-
<g/>
2	[number]	k4	2
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
závodě	závod	k1gInSc6	závod
PZL-Świdnik	PZL-Świdnik	k1gInSc1	PZL-Świdnik
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
vrtulník	vrtulník	k1gInSc4	vrtulník
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
moderznizované	moderznizovaný	k2eAgFnSc2d1	moderznizovaný
verze	verze	k1gFnSc2	verze
Mi-	Mi-	k1gFnSc2	Mi-
<g/>
2	[number]	k4	2
započal	započnout	k5eAaPmAgInS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
PZL-Świdnik	PZL-Świdnik	k1gInSc1	PZL-Świdnik
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vyvinout	vyvinout	k5eAaPmF	vyvinout
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
výrobcem	výrobce	k1gMnSc7	výrobce
leteckých	letecký	k2eAgInPc2d1	letecký
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
společností	společnost	k1gFnPc2	společnost
Allison	Allisona	k1gFnPc2	Allisona
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc1d1	nový
vrtulník	vrtulník	k1gInSc1	vrtulník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bude	být	k5eAaImBp3nS	být
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
Mi-	Mi-	k1gFnSc2	Mi-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bude	být	k5eAaImBp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
export	export	k1gInSc4	export
na	na	k7c4	na
západní	západní	k2eAgInPc4d1	západní
trhy	trh	k1gInPc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisely	souviset	k5eAaImAgFnP	souviset
úpravy	úprava	k1gFnPc1	úprava
Mi-	Mi-	k1gFnSc1	Mi-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgFnPc7d3	nejdůležitější
změnami	změna	k1gFnPc7	změna
byly	být	k5eAaImAgFnP	být
nové	nový	k2eAgInPc4d1	nový
výkonnější	výkonný	k2eAgInPc4d2	výkonnější
turbohřídelové	turbohřídelový	k2eAgInPc4d1	turbohřídelový
motory	motor	k1gInPc4	motor
Rolls-Royce	Rolls-Royce	k1gMnSc1	Rolls-Royce
Allison	Allison	k1gInSc4	Allison
250	[number]	k4	250
<g/>
-C	-C	k?	-C
<g/>
20	[number]	k4	20
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
313	[number]	k4	313
kW	kW	kA	kW
<g/>
,	,	kIx,	,
úpravy	úprava	k1gFnSc2	úprava
trupu	trup	k1gInSc2	trup
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc1d1	nový
kompozitní	kompozitní	k2eAgInPc1d1	kompozitní
listy	list	k1gInPc1	list
rotoru	rotor	k1gInSc2	rotor
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
avionika	avionika	k1gFnSc1	avionika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
prototyp	prototyp	k1gInSc4	prototyp
využívající	využívající	k2eAgInSc4d1	využívající
modifikovaný	modifikovaný	k2eAgInSc4d1	modifikovaný
drak	drak	k1gInSc4	drak
Mi-	Mi-	k1gFnSc4	Mi-
<g/>
2	[number]	k4	2
vzlétnul	vzlétnout	k5eAaPmAgInS	vzlétnout
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušky	zkouška	k1gFnPc1	zkouška
provedené	provedený	k2eAgFnPc1d1	provedená
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
ceritifikaci	ceritifikace	k1gFnSc3	ceritifikace
vrtulníku	vrtulník	k1gInSc2	vrtulník
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1	vrtulník
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
několika	několik	k4yIc2	několik
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
modernizované	modernizovaný	k2eAgFnSc2d1	modernizovaná
verze	verze	k1gFnSc2	verze
Mi-	Mi-	k1gFnSc2	Mi-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nevyrábí	vyrábět	k5eNaImIp3nS	vyrábět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prototyp	prototyp	k1gInSc1	prototyp
vrtulníku	vrtulník	k1gInSc2	vrtulník
s	s	k7c7	s
imatrikulací	imatrikulace	k1gFnSc7	imatrikulace
SP-SSC	SP-SSC	k1gFnPc2	SP-SSC
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
účastnil	účastnit	k5eAaImAgInS	účastnit
5	[number]	k4	5
<g/>
.	.	kIx.	.
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
vrtulníků	vrtulník	k1gInPc2	vrtulník
v	v	k7c4	v
Castle	Castle	k1gFnPc4	Castle
Ashby	Ashba	k1gFnSc2	Ashba
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
započala	započnout	k5eAaPmAgFnS	započnout
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1	vrtulník
byl	být	k5eAaImAgInS	být
úspornější	úsporný	k2eAgMnSc1d2	úspornější
a	a	k8xC	a
nabízel	nabízet	k5eAaImAgInS	nabízet
vyšší	vysoký	k2eAgInSc4d2	vyšší
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
západními	západní	k2eAgInPc7d1	západní
vrtulníky	vrtulník	k1gInPc7	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
obměnu	obměna	k1gFnSc4	obměna
starších	starší	k1gMnPc2	starší
Mi-	Mi-	k1gFnSc2	Mi-
<g/>
2	[number]	k4	2
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
prodáván	prodávat	k5eAaImNgInS	prodávat
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
certifikací	certifikace	k1gFnSc7	certifikace
se	se	k3xPyFc4	se
vrtulník	vrtulník	k1gInSc1	vrtulník
nestal	stát	k5eNaPmAgInS	stát
populární	populární	k2eAgMnSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
zemí	zem	k1gFnPc2	zem
také	také	k9	také
odmítala	odmítat	k5eAaImAgFnS	odmítat
dovážet	dovážet	k5eAaImF	dovážet
díly	díl	k1gInPc4	díl
z	z	k7c2	z
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
měnou	měna	k1gFnSc7	měna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
pouze	pouze	k6eAd1	pouze
14	[number]	k4	14
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
prototypů	prototyp	k1gInPc2	prototyp
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
uživatelem	uživatel	k1gMnSc7	uživatel
je	být	k5eAaImIp3nS	být
polská	polský	k2eAgFnSc1d1	polská
pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
stráž	stráž	k1gFnSc1	stráž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
provozovala	provozovat	k5eAaImAgFnS	provozovat
sedm	sedm	k4xCc4	sedm
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
využívala	využívat	k5eAaPmAgFnS	využívat
i	i	k9	i
polská	polský	k2eAgFnSc1d1	polská
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1987-1990	[number]	k4	1987-1990
létaly	létat	k5eAaImAgInP	létat
tři	tři	k4xCgInPc1	tři
vrtulníky	vrtulník	k1gInPc1	vrtulník
Kania	Kanium	k1gNnSc2	Kanium
v	v	k7c6	v
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
nasazovány	nasazovat	k5eAaImNgInP	nasazovat
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
letecké	letecký	k2eAgFnSc2d1	letecká
záchranné	záchranný	k2eAgFnSc2d1	záchranná
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
létal	létat	k5eAaImAgInS	létat
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
PZL	PZL	kA	PZL
Kania	Kania	k1gFnSc1	Kania
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgInPc1	čtyři
vrtulníky	vrtulník	k1gInPc1	vrtulník
Kania	Kanium	k1gNnSc2	Kanium
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgInSc1d1	polský
vrtulník	vrtulník	k1gInSc1	vrtulník
PZL	PZL	kA	PZL
Kania	Kania	k1gFnSc1	Kania
(	(	kIx(	(
<g/>
imatrikulace	imatrikulace	k1gFnSc1	imatrikulace
SP-SSE	SP-SSE	k1gMnSc1	SP-SSE
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
předváděn	předvádět	k5eAaImNgInS	předvádět
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stroj	stroj	k1gInSc1	stroj
si	se	k3xPyFc3	se
pronajala	pronajmout	k5eAaPmAgFnS	pronajmout
Letecká	letecký	k2eAgFnSc1d1	letecká
služba	služba	k1gFnSc1	služba
Federálního	federální	k2eAgInSc2d1	federální
policejního	policejní	k2eAgInSc2d1	policejní
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
československého	československý	k2eAgNnSc2d1	Československé
policejního	policejní	k2eAgNnSc2d1	policejní
letectva	letectvo	k1gNnSc2	letectvo
létal	létat	k5eAaImAgMnS	létat
s	s	k7c7	s
imatrikulací	imatrikulace	k1gFnSc7	imatrikulace
B-	B-	k1gFnSc7	B-
<g/>
3211	[number]	k4	3211
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
prověřován	prověřovat	k5eAaImNgInS	prověřovat
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
letecké	letecký	k2eAgFnSc2d1	letecká
záchranné	záchranný	k2eAgFnSc2d1	záchranná
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
Kryštof	Kryštof	k1gMnSc1	Kryštof
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1992	[number]	k4	1992
do	do	k7c2	do
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1993	[number]	k4	1993
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
letecké	letecký	k2eAgFnSc2d1	letecká
záchranné	záchranný	k2eAgFnSc2d1	záchranná
služby	služba	k1gFnSc2	služba
nasazen	nasadit	k5eAaPmNgInS	nasadit
trvale	trvale	k6eAd1	trvale
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
vrtulník	vrtulník	k1gInSc4	vrtulník
střední	střední	k2eAgFnSc2d1	střední
váhové	váhový	k2eAgFnSc2d1	váhová
kategorie	kategorie	k1gFnSc2	kategorie
Bell	bell	k1gInSc1	bell
412	[number]	k4	412
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
výrobci	výrobce	k1gMnPc1	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Odtamtud	odtamtud	k6eAd1	odtamtud
jej	on	k3xPp3gNnSc4	on
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
slovenská	slovenský	k2eAgFnSc1d1	slovenská
společnost	společnost	k1gFnSc1	společnost
Bel-Air	Bel-Aira	k1gFnPc2	Bel-Aira
<g/>
.	.	kIx.	.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1	vrtulník
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
společnosti	společnost	k1gFnSc2	společnost
ATE	ATE	kA	ATE
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
létal	létat	k5eAaImAgMnS	létat
pod	pod	k7c7	pod
imatrikulací	imatrikulace	k1gFnSc7	imatrikulace
OM-TFA	OM-TFA	k1gFnSc2	OM-TFA
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
Letecké	letecký	k2eAgFnSc2d1	letecká
služby	služba	k1gFnSc2	služba
Federálního	federální	k2eAgInSc2d1	federální
policejního	policejní	k2eAgInSc2d1	policejní
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
Letecké	letecký	k2eAgFnSc2d1	letecká
služby	služba	k1gFnSc2	služba
Policie	policie	k1gFnSc1	policie
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
létaly	létat	k5eAaImAgInP	létat
tři	tři	k4xCgInPc1	tři
vrtulníky	vrtulník	k1gInPc1	vrtulník
Kania	Kanium	k1gNnSc2	Kanium
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
soukromé	soukromý	k2eAgFnSc2d1	soukromá
společnosti	společnost	k1gFnSc2	společnost
Helicopter	Helicoptra	k1gFnPc2	Helicoptra
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
provozovala	provozovat	k5eAaImAgFnS	provozovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
leteckou	letecký	k2eAgFnSc4d1	letecká
záchrannou	záchranný	k2eAgFnSc4d1	záchranná
službu	služba	k1gFnSc4	služba
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Kryštof	Kryštof	k1gMnSc1	Kryštof
13	[number]	k4	13
v	v	k7c6	v
Hosíně	Hosín	k1gInSc6	Hosín
u	u	k7c2	u
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
LZS	LZS	kA	LZS
nasazovány	nasazován	k2eAgInPc1d1	nasazován
vrtulníky	vrtulník	k1gInPc1	vrtulník
Kania	Kanium	k1gNnSc2	Kanium
(	(	kIx(	(
<g/>
imatrikulace	imatrikulace	k1gFnSc1	imatrikulace
OK-WIM	OK-WIM	k1gMnSc1	OK-WIM
<g/>
,	,	kIx,	,
OK-VIL	OK-VIL	k1gMnSc1	OK-VIL
<g/>
,	,	kIx,	,
OK-MIK	OK-MIK	k1gMnSc1	OK-MIK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1	vrtulník
s	s	k7c7	s
imatrikulací	imatrikulace	k1gFnSc7	imatrikulace
OK-WIM	OK-WIM	k1gFnSc2	OK-WIM
havaroval	havarovat	k5eAaPmAgMnS	havarovat
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1997	[number]	k4	1997
a	a	k8xC	a
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
s	s	k7c7	s
imatrikulací	imatrikulace	k1gFnSc7	imatrikulace
OK-MIK	OK-MIK	k1gFnPc2	OK-MIK
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vrácen	vrátit	k5eAaPmNgMnS	vrátit
výrobci	výrobce	k1gMnPc7	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
změna	změna	k1gFnSc1	změna
provozovatele	provozovatel	k1gMnSc2	provozovatel
letecké	letecký	k2eAgFnSc2d1	letecká
záchranné	záchranný	k2eAgFnSc2d1	záchranná
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Vrtulník	vrtulník	k1gInSc1	vrtulník
tak	tak	k9	tak
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
potřebný	potřebný	k2eAgInSc1d1	potřebný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
odprodán	odprodat	k5eAaPmNgInS	odprodat
jinému	jiný	k2eAgMnSc3d1	jiný
soukromému	soukromý	k2eAgMnSc3d1	soukromý
dopravci	dopravce	k1gMnSc3	dopravce
také	také	k9	také
třetí	třetí	k4xOgInSc1	třetí
vrtulník	vrtulník	k1gInSc1	vrtulník
PZL	PZL	kA	PZL
Kania	Kania	k1gFnSc1	Kania
s	s	k7c7	s
imatrikulací	imatrikulace	k1gFnSc7	imatrikulace
OK-VIL	OK-VIL	k1gFnPc2	OK-VIL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Varianty	variant	k1gInPc4	variant
==	==	k?	==
</s>
</p>
<p>
<s>
Osobní	osobní	k2eAgFnSc1d1	osobní
standardní	standardní	k2eAgFnSc1d1	standardní
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
Možnost	možnost	k1gFnSc1	možnost
přepravovat	přepravovat	k5eAaImF	přepravovat
až	až	k9	až
devět	devět	k4xCc4	devět
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobní	osobní	k2eAgFnSc1d1	osobní
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
Vrtulník	vrtulník	k1gInSc1	vrtulník
v	v	k7c6	v
luxusní	luxusní	k2eAgFnSc6d1	luxusní
úpravě	úprava	k1gFnSc6	úprava
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
přepravovat	přepravovat	k5eAaImF	přepravovat
pět	pět	k4xCc4	pět
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nákladní	nákladní	k2eAgFnSc1d1	nákladní
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
Možnost	možnost	k1gFnSc1	možnost
přepravovat	přepravovat	k5eAaImF	přepravovat
břemena	břemeno	k1gNnSc2	břemeno
jako	jako	k8xS	jako
letecký	letecký	k2eAgInSc4d1	letecký
jeřáb	jeřáb	k1gInSc4	jeřáb
nebo	nebo	k8xC	nebo
nést	nést	k5eAaImF	nést
až	až	k9	až
1200	[number]	k4	1200
kg	kg	kA	kg
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Verze	verze	k1gFnSc1	verze
pro	pro	k7c4	pro
leteckou	letecký	k2eAgFnSc4d1	letecká
záchrannou	záchranný	k2eAgFnSc4d1	záchranná
službu	služba	k1gFnSc4	služba
<g/>
:	:	kIx,	:
Zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
prostor	prostor	k1gInSc1	prostor
až	až	k9	až
pro	pro	k7c4	pro
čtyři	čtyři	k4xCgNnPc4	čtyři
nosítka	nosítka	k1gNnPc4	nosítka
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
osádky	osádka	k1gFnSc2	osádka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
nosítek	nosítka	k1gNnPc2	nosítka
se	s	k7c7	s
zdravotnickým	zdravotnický	k2eAgInSc7d1	zdravotnický
personálem	personál	k1gInSc7	personál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
verze	verze	k1gFnSc1	verze
<g/>
:	:	kIx,	:
Možnost	možnost	k1gFnSc1	možnost
nést	nést	k5eAaImF	nést
až	až	k9	až
1000	[number]	k4	1000
kg	kg	kA	kg
chemikálií	chemikálie	k1gFnPc2	chemikálie
pro	pro	k7c4	pro
chemický	chemický	k2eAgInSc4d1	chemický
postřik	postřik	k1gInSc4	postřik
nebo	nebo	k8xC	nebo
1000	[number]	k4	1000
kg	kg	kA	kg
jiného	jiný	k2eAgInSc2d1	jiný
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spitfire	Spitfir	k1gMnSc5	Spitfir
Taurus	Taurus	k1gMnSc1	Taurus
II	II	kA	II
nebo	nebo	k8xC	nebo
Super	super	k2eAgFnSc1d1	super
Kania	Kania	k1gFnSc1	Kania
<g/>
:	:	kIx,	:
Americká	americký	k2eAgFnSc1d1	americká
jednomotorová	jednomotorový	k2eAgFnSc1d1	jednomotorová
verze	verze	k1gFnSc1	verze
vrtulníku	vrtulník	k1gInSc2	vrtulník
Kania	Kania	k1gFnSc1	Kania
s	s	k7c7	s
motorem	motor	k1gInSc7	motor
Allison	Allison	k1gInSc4	Allison
250	[number]	k4	250
<g/>
-C	-C	k?	-C
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
vrtulník	vrtulník	k1gInSc1	vrtulník
nebyl	být	k5eNaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Specifikace	specifikace	k1gFnSc1	specifikace
==	==	k?	==
</s>
</p>
<p>
<s>
Data	datum	k1gNnPc1	datum
podle	podle	k7c2	podle
<g/>
:	:	kIx,	:
Jane	Jan	k1gMnSc5	Jan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
All	All	k1gFnSc7	All
The	The	k1gFnSc1	The
World	World	k1gMnSc1	World
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Aircraft	Aircraft	k1gInSc1	Aircraft
1988-89	[number]	k4	1988-89
</s>
</p>
<p>
<s>
===	===	k?	===
Technické	technický	k2eAgInPc1d1	technický
údaje	údaj	k1gInPc1	údaj
===	===	k?	===
</s>
</p>
<p>
<s>
Posádka	posádka	k1gFnSc1	posádka
<g/>
:	:	kIx,	:
1	[number]	k4	1
pilot	pilot	k1gMnSc1	pilot
</s>
</p>
<p>
<s>
Užitečná	užitečný	k2eAgFnSc1d1	užitečná
zátěž	zátěž	k1gFnSc1	zátěž
<g/>
:	:	kIx,	:
9	[number]	k4	9
pasažérů	pasažér	k1gMnPc2	pasažér
nebo	nebo	k8xC	nebo
1200	[number]	k4	1200
kg	kg	kA	kg
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
nebo	nebo	k8xC	nebo
800	[number]	k4	800
kg	kg	kA	kg
vnějšího	vnější	k2eAgInSc2d1	vnější
nákladu	náklad	k1gInSc2	náklad
</s>
</p>
<p>
<s>
Průměr	průměr	k1gInSc1	průměr
rotoru	rotor	k1gInSc2	rotor
<g/>
:	:	kIx,	:
14,56	[number]	k4	14,56
m	m	kA	m
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
trupu	trup	k1gInSc2	trup
<g/>
:	:	kIx,	:
12,03	[number]	k4	12,03
m	m	kA	m
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
3,75	[number]	k4	3,75
m	m	kA	m
</s>
</p>
<p>
<s>
Plocha	plocha	k1gFnSc1	plocha
rotoru	rotor	k1gInSc2	rotor
<g/>
:	:	kIx,	:
167	[number]	k4	167
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Prázdná	prázdný	k2eAgFnSc1d1	prázdná
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
2000	[number]	k4	2000
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
3550	[number]	k4	3550
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
×	×	k?	×
turbohřídelový	turbohřídelový	k2eAgInSc1d1	turbohřídelový
motor	motor	k1gInSc1	motor
Allison	Allison	k1gNnSc1	Allison
250	[number]	k4	250
<g/>
-C	-C	k?	-C
<g/>
20	[number]	k4	20
<g/>
B	B	kA	B
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
313	[number]	k4	313
kW	kW	kA	kW
</s>
</p>
<p>
<s>
===	===	k?	===
Výkony	výkon	k1gInPc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
116	[number]	k4	116
KIAS	KIAS	kA	KIAS
(	(	kIx(	(
<g/>
215	[number]	k4	215
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cestovní	cestovní	k2eAgFnSc1d1	cestovní
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
102	[number]	k4	102
KIAS	KIAS	kA	KIAS
(	(	kIx(	(
<g/>
190	[number]	k4	190
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stoupavost	stoupavost	k1gFnSc1	stoupavost
u	u	k7c2	u
země	zem	k1gFnSc2	zem
<g/>
:	:	kIx,	:
8,75	[number]	k4	8,75
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
Praktický	praktický	k2eAgInSc1d1	praktický
dostup	dostup	k1gInSc1	dostup
<g/>
:	:	kIx,	:
4000	[number]	k4	4000
m	m	kA	m
</s>
</p>
<p>
<s>
Statický	statický	k2eAgInSc1d1	statický
dostup	dostup	k1gInSc1	dostup
bez	bez	k7c2	bez
vlivu	vliv	k1gInSc2	vliv
země	zem	k1gFnSc2	zem
<g/>
:	:	kIx,	:
1375	[number]	k4	1375
m	m	kA	m
</s>
</p>
<p>
<s>
Dolet	dolet	k1gInSc1	dolet
<g/>
:	:	kIx,	:
493	[number]	k4	493
km	km	kA	km
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
PZL	PZL	kA	PZL
Kania	Kanius	k1gMnSc2	Kanius
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FOJTÍK	Fojtík	k1gMnSc1	Fojtík
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Víceúčelový	víceúčelový	k2eAgInSc1d1	víceúčelový
vrtulník	vrtulník	k1gInSc1	vrtulník
Mi-	Mi-	k1gFnSc1	Mi-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Cheb	Cheb	k1gInSc1	Cheb
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
191	[number]	k4	191
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86808	[number]	k4	86808
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Mil	míle	k1gFnPc2	míle
Mi-	Mi-	k1gFnSc4	Mi-
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
PZL	PZL	kA	PZL
Kania	Kanium	k1gNnSc2	Kanium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
PZL	PZL	kA	PZL
Kania	Kania	k1gFnSc1	Kania
historie	historie	k1gFnSc2	historie
vrtulníků	vrtulník	k1gInPc2	vrtulník
PZL	PZL	kA	PZL
Kania	Kanius	k1gMnSc2	Kanius
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
a	a	k8xC	a
Česku	Česko	k1gNnSc6	Česko
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
vrtulnik	vrtulnik	k1gInSc1	vrtulnik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
