<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
vydal	vydat	k5eAaPmAgMnS	vydat
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
Surrealistický	surrealistický	k2eAgInSc4d1	surrealistický
Manifest	manifest	k1gInSc4	manifest
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
začal	začít	k5eAaPmAgInS	začít
vycházet	vycházet	k5eAaImF	vycházet
časopis	časopis	k1gInSc1	časopis
La	la	k1gNnSc6	la
Révolution	Révolution	k1gInSc1	Révolution
Surréaliste	Surréalist	k1gMnSc5	Surréalist
(	(	kIx(	(
<g/>
Surrealistická	surrealistický	k2eAgFnSc1d1	surrealistická
Revoluce	revoluce	k1gFnSc1	revoluce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dostávali	dostávat	k5eAaImAgMnP	dostávat
prostor	prostor	k1gInSc4	prostor
především	především	k9	především
surrealisté	surrealista	k1gMnPc1	surrealista
<g/>
.	.	kIx.	.
</s>
