<p>
<s>
Florida	Florida	k1gFnSc1	Florida
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Florida	Florida	k1gFnSc1	Florida
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
a	a	k8xC	a
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c4	v
Jihoatlantské	Jihoatlantský	k2eAgFnPc4d1	Jihoatlantský
oblasti	oblast	k1gFnPc4	oblast
jižního	jižní	k2eAgInSc2d1	jižní
regionu	region	k1gInSc2	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
poloostrově	poloostrov	k1gInSc6	poloostrov
a	a	k8xC	a
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejjižnějším	jižní	k2eAgInSc7d3	nejjižnější
státem	stát	k1gInSc7	stát
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
USA	USA	kA	USA
<g/>
,	,	kIx,	,
hraničí	hraničit	k5eAaImIp3nP	hraničit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Georgií	Georgie	k1gFnSc7	Georgie
a	a	k8xC	a
Alabamou	Alabama	k1gFnSc7	Alabama
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnSc1d1	západní
ohraničení	ohraničení	k1gNnSc1	ohraničení
státu	stát	k1gInSc2	stát
tvoří	tvořit	k5eAaImIp3nS	tvořit
Mexický	mexický	k2eAgInSc1d1	mexický
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
z	z	k7c2	z
východu	východ	k1gInSc2	východ
je	být	k5eAaImIp3nS	být
ohraničena	ohraničit	k5eAaPmNgFnS	ohraničit
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
170	[number]	k4	170
304	[number]	k4	304
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Florida	Florida	k1gFnSc1	Florida
22	[number]	k4	22
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
20,6	[number]	k4	20,6
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
třetím	třetí	k4xOgInSc7	třetí
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
147	[number]	k4	147
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Tallahassee	Tallahassee	k1gInSc1	Tallahassee
se	s	k7c7	s
190	[number]	k4	190
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Jacksonville	Jacksonville	k1gNnSc4	Jacksonville
s	s	k7c7	s
850	[number]	k4	850
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Miami	Miami	k1gNnSc1	Miami
(	(	kIx(	(
<g/>
430	[number]	k4	430
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tampa	Tampa	k1gFnSc1	Tampa
(	(	kIx(	(
<g/>
360	[number]	k4	360
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Orlando	Orlanda	k1gFnSc5	Orlanda
(	(	kIx(	(
<g/>
260	[number]	k4	260
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Petersburg	Petersburg	k1gInSc1	Petersburg
(	(	kIx(	(
<g/>
250	[number]	k4	250
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hialeah	Hialeah	k1gMnSc1	Hialeah
(	(	kIx(	(
<g/>
240	[number]	k4	240
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Floridě	Florida	k1gFnSc3	Florida
patří	patřit	k5eAaImIp3nS	patřit
2713	[number]	k4	2713
km	km	kA	km
pobřeží	pobřeží	k1gNnSc1	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Britton	Britton	k1gInSc1	Britton
Hill	Hill	k1gInSc1	Hill
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
105	[number]	k4	105
m	m	kA	m
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnSc2	řeka
Perdido	Perdida	k1gFnSc5	Perdida
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnSc4d1	tvořící
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Alabamou	Alabama	k1gFnSc7	Alabama
<g/>
,	,	kIx,	,
Apalachicola	Apalachicola	k1gFnSc1	Apalachicola
a	a	k8xC	a
St.	st.	kA	st.
Johns	Johns	k1gInSc1	Johns
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
vlévají	vlévat	k5eAaImIp3nP	vlévat
do	do	k7c2	do
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
a	a	k8xC	a
Suwannee	Suwanne	k1gFnPc1	Suwanne
<g/>
,	,	kIx,	,
vlévající	vlévající	k2eAgFnPc1d1	vlévající
se	se	k3xPyFc4	se
do	do	k7c2	do
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
vodní	vodní	k2eAgFnSc7d1	vodní
plochou	plocha	k1gFnSc7	plocha
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
Okeechobee	Okeechobe	k1gFnSc2	Okeechobe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Florida	Florida	k1gFnSc1	Florida
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgNnSc7	první
územím	území	k1gNnSc7	území
dnešních	dnešní	k2eAgInPc2d1	dnešní
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
navštívili	navštívit	k5eAaPmAgMnP	navštívit
a	a	k8xC	a
osídlili	osídlit	k5eAaPmAgMnP	osídlit
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
záznam	záznam	k1gInSc1	záznam
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1513	[number]	k4	1513
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
poloostrova	poloostrov	k1gInSc2	poloostrov
přistál	přistát	k5eAaImAgMnS	přistát
Španěl	Španěl	k1gMnSc1	Španěl
Juan	Juan	k1gMnSc1	Juan
Ponce	Ponce	k1gMnSc1	Ponce
de	de	k?	de
León	León	k1gMnSc1	León
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
oblast	oblast	k1gFnSc4	oblast
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
La	la	k1gNnPc1	la
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
země	země	k1gFnSc1	země
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
trvalé	trvalý	k2eAgNnSc1d1	trvalé
osídlení	osídlení	k1gNnSc1	osídlení
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
roku	rok	k1gInSc2	rok
1559	[number]	k4	1559
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zanedlouho	zanedlouho	k6eAd1	zanedlouho
bylo	být	k5eAaImAgNnS	být
opuštěno	opustit	k5eAaPmNgNnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1565	[number]	k4	1565
založili	založit	k5eAaPmAgMnP	založit
Španělé	Španěl	k1gMnPc1	Španěl
sídlo	sídlo	k1gNnSc4	sídlo
San	San	k1gMnPc2	San
Agustín	Agustín	k1gMnSc1	Agustín
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgMnSc1d1	dnešní
St.	st.	kA	st.
Augustine	Augustin	k1gMnSc5	Augustin
<g/>
.	.	kIx.	.
</s>
<s>
Florida	Florida	k1gFnSc1	Florida
zůstala	zůstat	k5eAaPmAgFnS	zůstat
španělskou	španělský	k2eAgFnSc7d1	španělská
kolonií	kolonie	k1gFnSc7	kolonie
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledku	výsledek	k1gInSc2	výsledek
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
získali	získat	k5eAaPmAgMnP	získat
Britové	Brit	k1gMnPc1	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
ji	on	k3xPp3gFnSc4	on
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
samostatné	samostatný	k2eAgFnPc4d1	samostatná
provincie	provincie	k1gFnPc4	provincie
<g/>
,	,	kIx,	,
Východní	východní	k2eAgInPc1d1	východní
(	(	kIx(	(
<g/>
vlastní	vlastní	k2eAgInSc1d1	vlastní
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
)	)	kIx)	)
a	a	k8xC	a
Západní	západní	k2eAgFnSc4d1	západní
Floridu	Florida	k1gFnSc4	Florida
(	(	kIx(	(
<g/>
pobřeží	pobřeží	k1gNnSc2	pobřeží
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
drželi	držet	k5eAaImAgMnP	držet
území	území	k1gNnSc4	území
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
válce	válka	k1gFnSc6	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
opět	opět	k6eAd1	opět
získali	získat	k5eAaPmAgMnP	získat
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
rozdělení	rozdělení	k1gNnSc4	rozdělení
regionu	region	k1gInSc2	region
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
ponechali	ponechat	k5eAaPmAgMnP	ponechat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
seminolské	seminolský	k2eAgFnSc6d1	seminolský
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
obě	dva	k4xCgFnPc4	dva
území	území	k1gNnPc2	území
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
Adams-Onís	Adams-Onísa	k1gFnPc2	Adams-Onísa
předána	předat	k5eAaPmNgFnS	předat
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
sloučily	sloučit	k5eAaPmAgFnP	sloučit
do	do	k7c2	do
společného	společný	k2eAgNnSc2d1	společné
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	s	k7c7	s
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1845	[number]	k4	1845
stalo	stát	k5eAaPmAgNnS	stát
27	[number]	k4	27
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
Florida	Florida	k1gFnSc1	Florida
v	v	k7c6	v
letech	let	k1gInPc6	let
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1865	[number]	k4	1865
součástí	součást	k1gFnPc2	součást
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
k	k	k7c3	k
Unii	unie	k1gFnSc3	unie
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
připojena	připojen	k2eAgFnSc1d1	připojena
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
velmi	velmi	k6eAd1	velmi
časté	častý	k2eAgInPc1d1	častý
hurikány	hurikán	k1gInPc1	hurikán
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tu	tu	k6eAd1	tu
teplota	teplota	k1gFnSc1	teplota
není	být	k5eNaImIp3nS	být
až	až	k9	až
tak	tak	k6eAd1	tak
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
teplota	teplota	k1gFnSc1	teplota
často	často	k6eAd1	často
nad	nad	k7c4	nad
třicet	třicet	k4xCc4	třicet
stupňů	stupeň	k1gInPc2	stupeň
a	a	k8xC	a
noci	noc	k1gFnPc1	noc
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
teplé	teplý	k2eAgFnPc1d1	teplá
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
třiceti	třicet	k4xCc2	třicet
stupních	stupeň	k1gInPc6	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
žilo	žít	k5eAaImAgNnS	žít
18	[number]	k4	18
801	[number]	k4	801
310	[number]	k4	310
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
18	[number]	k4	18
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
13	[number]	k4	13
%	%	kIx~	%
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Florida	Florida	k1gFnSc1	Florida
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
nejrychleji	rychle	k6eAd3	rychle
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	díky	k7c3	díky
velké	velký	k2eAgFnSc3d1	velká
poptávce	poptávka	k1gFnSc3	poptávka
po	po	k7c6	po
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
teplému	teplý	k2eAgNnSc3d1	teplé
klimatu	klima	k1gNnSc3	klima
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
nízkým	nízký	k2eAgInPc3d1	nízký
nákladům	náklad	k1gInPc3	náklad
na	na	k7c4	na
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Florida	Florida	k1gFnSc1	Florida
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
americké	americký	k2eAgInPc4d1	americký
poměry	poměr	k1gInPc4	poměr
hustě	hustě	k6eAd1	hustě
osídlena	osídlen	k2eAgFnSc1d1	osídlena
<g/>
;	;	kIx,	;
hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
činí	činit	k5eAaImIp3nS	činit
136,4	[number]	k4	136,4
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Česko	Česko	k1gNnSc1	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
metropolitních	metropolitní	k2eAgFnPc6d1	metropolitní
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgInPc4d3	nejznámější
je	být	k5eAaImIp3nS	být
Everglades	Everglades	k1gInSc4	Everglades
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
žije	žít	k5eAaImIp3nS	žít
početná	početný	k2eAgFnSc1d1	početná
komunita	komunita	k1gFnSc1	komunita
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rasové	rasový	k2eAgNnSc4d1	rasové
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
75,0	[number]	k4	75,0
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
(	(	kIx(	(
<g/>
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
57,9	[number]	k4	57,9
%	%	kIx~	%
+	+	kIx~	+
běloši	běloch	k1gMnPc1	běloch
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
17,1	[number]	k4	17,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
16,0	[number]	k4	16,0
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
</s>
</p>
<p>
<s>
0,4	[number]	k4	0,4
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
</s>
</p>
<p>
<s>
2,4	[number]	k4	2,4
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
</s>
</p>
<p>
<s>
0,1	[number]	k4	0,1
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
</s>
</p>
<p>
<s>
3,6	[number]	k4	3,6
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
</s>
</p>
<p>
<s>
2,5	[number]	k4	2,5
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
rasObyvatelé	rasObyvatel	k1gMnPc1	rasObyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
22,5	[number]	k4	22,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Florida	Florida	k1gFnSc1	Florida
je	být	k5eAaImIp3nS	být
většinově	většinově	k6eAd1	většinově
protestantská	protestantský	k2eAgFnSc1d1	protestantská
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
katolickou	katolický	k2eAgFnSc7d1	katolická
komunitou	komunita	k1gFnSc7	komunita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
roste	růst	k5eAaImIp3nS	růst
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
imigrací	imigrace	k1gFnSc7	imigrace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
celkem	celkem	k6eAd1	celkem
hodně	hodně	k6eAd1	hodně
židů	žid	k1gMnPc2	žid
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
kterémkoli	kterýkoli	k3yIgInSc6	kterýkoli
z	z	k7c2	z
jižních	jižní	k2eAgInPc2d1	jižní
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
křesťané	křesťan	k1gMnPc1	křesťan
82	[number]	k4	82
%	%	kIx~	%
</s>
</p>
<p>
<s>
protestanti	protestant	k1gMnPc1	protestant
54	[number]	k4	54
%	%	kIx~	%
</s>
</p>
<p>
<s>
baptisté	baptista	k1gMnPc1	baptista
19	[number]	k4	19
%	%	kIx~	%
</s>
</p>
<p>
<s>
metodisté	metodista	k1gMnPc1	metodista
6	[number]	k4	6
%	%	kIx~	%
</s>
</p>
<p>
<s>
presbyteriáni	presbyterián	k1gMnPc1	presbyterián
4	[number]	k4	4
%	%	kIx~	%
</s>
</p>
<p>
<s>
episkopální	episkopální	k2eAgFnPc1d1	episkopální
církve	církev	k1gFnPc1	církev
3	[number]	k4	3
%	%	kIx~	%
</s>
</p>
<p>
<s>
luteráni	luterán	k1gMnPc1	luterán
3	[number]	k4	3
%	%	kIx~	%
</s>
</p>
<p>
<s>
letniční	letniční	k2eAgNnSc4d1	letniční
3	[number]	k4	3
%	%	kIx~	%
</s>
</p>
<p>
<s>
ostatní	ostatní	k2eAgMnPc1d1	ostatní
protestanti	protestant	k1gMnPc1	protestant
16	[number]	k4	16
%	%	kIx~	%
</s>
</p>
<p>
<s>
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
26	[number]	k4	26
%	%	kIx~	%
</s>
</p>
<p>
<s>
ostatní	ostatní	k2eAgMnPc1d1	ostatní
křesťané	křesťan	k1gMnPc1	křesťan
2	[number]	k4	2
%	%	kIx~	%
</s>
</p>
<p>
<s>
židé	žid	k1gMnPc1	žid
4	[number]	k4	4
%	%	kIx~	%
</s>
</p>
<p>
<s>
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnPc2	vyznání
14	[number]	k4	14
%	%	kIx~	%
</s>
</p>
<p>
<s>
==	==	k?	==
Turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
jsou	být	k5eAaImIp3nP	být
močály	močál	k1gInPc1	močál
Everglades	Evergladesa	k1gFnPc2	Evergladesa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
turisticky	turisticky	k6eAd1	turisticky
zajímavá	zajímavý	k2eAgNnPc4d1	zajímavé
místa	místo	k1gNnPc4	místo
patří	patřit	k5eAaImIp3nS	patřit
i	i	k8xC	i
Kennedy	Kenneda	k1gMnSc2	Kenneda
Space	Space	k1gFnSc2	Space
Center	centrum	k1gNnPc2	centrum
na	na	k7c6	na
Cape	capat	k5eAaImIp3nS	capat
Canaveral	Canaveral	k1gFnPc2	Canaveral
(	(	kIx(	(
<g/>
východní	východní	k2eAgNnSc1d1	východní
pobřeží	pobřeží	k1gNnSc1	pobřeží
Floridy	Florida	k1gFnSc2	Florida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
středisko	středisko	k1gNnSc1	středisko
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
startovaly	startovat	k5eAaBmAgInP	startovat
raketoplány	raketoplán	k1gInPc1	raketoplán
i	i	k8xC	i
menší	malý	k2eAgFnPc1d2	menší
rakety	raketa	k1gFnPc1	raketa
<g/>
,	,	kIx,	,
vynášející	vynášející	k2eAgNnSc1d1	vynášející
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
družice	družice	k1gFnSc2	družice
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
komplexu	komplex	k1gInSc2	komplex
je	být	k5eAaImIp3nS	být
i	i	k9	i
návštěvní	návštěvní	k2eAgNnSc1d1	návštěvní
středisko	středisko	k1gNnSc1	středisko
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
muzeí	muzeum	k1gNnPc2	muzeum
a	a	k8xC	a
atrakcí	atrakce	k1gFnPc2	atrakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejstarší	starý	k2eAgNnSc1d3	nejstarší
dosud	dosud	k6eAd1	dosud
obydlené	obydlený	k2eAgNnSc1d1	obydlené
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
Evropany	Evropan	k1gMnPc4	Evropan
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Augustine	Augustin	k1gMnSc5	Augustin
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
s	s	k7c7	s
pouhými	pouhý	k2eAgInPc7d1	pouhý
11	[number]	k4	11
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
jehož	jehož	k3xOyRp3gInSc7	jehož
historie	historie	k1gFnSc1	historie
začíná	začínat	k5eAaImIp3nS	začínat
již	již	k6eAd1	již
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turisticky	turisticky	k6eAd1	turisticky
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
je	být	k5eAaImIp3nS	být
i	i	k9	i
rezervace	rezervace	k1gFnSc1	rezervace
Everglades	Evergladesa	k1gFnPc2	Evergladesa
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
tisíce	tisíc	k4xCgInPc1	tisíc
aligátorů	aligátor	k1gMnPc2	aligátor
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
navštívit	navštívit	k5eAaPmF	navštívit
na	na	k7c6	na
speciálních	speciální	k2eAgNnPc6d1	speciální
lodích-vznášedlech	lodíchznášedlo	k1gNnPc6	lodích-vznášedlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
Floridy	Florida	k1gFnSc2	Florida
u	u	k7c2	u
města	město	k1gNnSc2	město
Orlando	Orlanda	k1gFnSc5	Orlanda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
zábavních	zábavní	k2eAgInPc2d1	zábavní
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Universal	Universal	k1gFnSc1	Universal
Orlando	Orlanda	k1gFnSc5	Orlanda
Resort	resort	k1gInSc1	resort
nebo	nebo	k8xC	nebo
obrovský	obrovský	k2eAgInSc1d1	obrovský
zábavní	zábavní	k2eAgInSc1d1	zábavní
komplex	komplex	k1gInSc1	komplex
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
World	Worlda	k1gFnPc2	Worlda
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
jej	on	k3xPp3gNnSc2	on
několik	několik	k4yIc1	několik
samostatných	samostatný	k2eAgNnPc2d1	samostatné
"	"	kIx"	"
<g/>
měst	město	k1gNnPc2	město
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Magic	Magic	k1gMnSc1	Magic
Kingdom	Kingdom	k1gInSc1	Kingdom
<g/>
,	,	kIx,	,
království	království	k1gNnSc1	království
pohádek	pohádka	k1gFnPc2	pohádka
</s>
</p>
<p>
<s>
Epcot	Epcot	k1gInSc1	Epcot
<g/>
,	,	kIx,	,
areál	areál	k1gInSc1	areál
technických	technický	k2eAgFnPc2d1	technická
inovací	inovace	k1gFnPc2	inovace
a	a	k8xC	a
typických	typický	k2eAgNnPc2d1	typické
měst	město	k1gNnPc2	město
různých	různý	k2eAgInPc2d1	různý
národů	národ	k1gInPc2	národ
a	a	k8xC	a
kultur	kultura	k1gFnPc2	kultura
</s>
</p>
<p>
<s>
Hollywood	Hollywood	k1gInSc1	Hollywood
Studios	Studios	k?	Studios
</s>
</p>
<p>
<s>
Animal	animal	k1gMnSc1	animal
Kingdom	Kingdom	k1gInSc4	Kingdom
Park	park	k1gInSc1	park
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Mottem	motto	k1gNnSc7	motto
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
In	In	k1gMnSc1	In
God	God	k1gFnSc2	God
We	We	k1gMnSc1	We
Trust	trust	k1gInSc1	trust
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
květinou	květina	k1gFnSc7	květina
květ	květ	k1gInSc4	květ
pomerančovníku	pomerančovník	k1gInSc2	pomerančovník
<g/>
,	,	kIx,	,
stromem	strom	k1gInSc7	strom
palma	palma	k1gFnSc1	palma
Sabal	Sabal	k1gMnSc1	Sabal
palmetto	palmetto	k1gNnSc1	palmetto
<g/>
,	,	kIx,	,
ptákem	pták	k1gMnSc7	pták
drozd	drozd	k1gMnSc1	drozd
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
Swanee	Swanee	k1gFnSc7	Swanee
River	Rivra	k1gFnPc2	Rivra
<g/>
.	.	kIx.	.
<g/>
Symbolem	symbol	k1gInSc7	symbol
tohoto	tento	k3xDgInSc2	tento
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
pelikán	pelikán	k1gInSc1	pelikán
<g/>
.	.	kIx.	.
</s>
<s>
Floridu	Florida	k1gFnSc4	Florida
objevili	objevit	k5eAaPmAgMnP	objevit
španělští	španělský	k2eAgMnPc1d1	španělský
námořníci	námořník	k1gMnPc1	námořník
v	v	k7c6	v
době	doba	k1gFnSc6	doba
velikonočních	velikonoční	k2eAgInPc2d1	velikonoční
svátků	svátek	k1gInPc2	svátek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Pascua	Pascu	k2eAgFnSc1d1	Pascu
Florida	Florida	k1gFnSc1	Florida
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
Florida	Florida	k1gFnSc1	Florida
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nížinatá	nížinatý	k2eAgFnSc1d1	nížinatá
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vlhké	vlhký	k2eAgNnSc4d1	vlhké
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hladina	hladina	k1gFnSc1	hladina
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgInPc1d1	zdejší
domy	dům	k1gInPc1	dům
proto	proto	k8xC	proto
většinou	většinou	k6eAd1	většinou
nemají	mít	k5eNaImIp3nP	mít
sklepy	sklep	k1gInPc4	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
také	také	k9	také
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
dobře	dobře	k6eAd1	dobře
stavěné	stavěný	k2eAgNnSc1d1	stavěné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odolaly	odolat	k5eAaPmAgFnP	odolat
hurikánům	hurikán	k1gInPc3	hurikán
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
časté	častý	k2eAgInPc1d1	častý
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
existují	existovat	k5eAaImIp3nP	existovat
zákony	zákon	k1gInPc1	zákon
a	a	k8xC	a
směrnice	směrnice	k1gFnPc1	směrnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Florida	Florida	k1gFnSc1	Florida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Florida	Florida	k1gFnSc1	Florida
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
státu	stát	k1gInSc2	stát
Florida	Florida	k1gFnSc1	Florida
</s>
</p>
