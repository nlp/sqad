<s>
Crespi	Crespi	k6eAd1
d	d	k?
<g/>
'	'	kIx"
<g/>
Adda	Adda	k1gMnSc1
</s>
<s>
Crespi	Crespi	k6eAd1
d	d	k?
<g/>
'	'	kIx"
<g/>
Adda	Add	k2eAgFnSc1d1
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
48	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
9	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
180	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
Crespi	Crespi	k6eAd1
d	d	k?
<g/>
'	'	kIx"
<g/>
Adda	Adda	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
0,6	0,6	k4
km²	km²	k?
Správa	správa	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1878	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
crespidaddaunesco	crespidaddaunesco	k1gNnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
02	#num#	k4
PSČ	PSČ	kA
</s>
<s>
24042	#num#	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
BG	BG	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Crespi	Crespi	k6eAd1
d	d	k?
<g/>
'	'	kIx"
<g/>
AddaSvětové	AddaSvětové	k2eAgNnPc2d1
dědictví	dědictví	k1gNnPc2
UNESCO	UNESCO	kA
Smluvní	smluvní	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
Typ	typ	k1gInSc1
</s>
<s>
kulturní	kulturní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
Kritérium	kritérium	k1gNnSc1
</s>
<s>
iv	iv	k?
<g/>
,	,	kIx,
v	v	k7c4
Odkaz	odkaz	k1gInSc4
</s>
<s>
730	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zařazení	zařazení	k1gNnSc1
do	do	k7c2
seznamu	seznam	k1gInSc2
Zařazení	zařazení	k1gNnSc1
</s>
<s>
1995	#num#	k4
(	(	kIx(
<g/>
19	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc6
<g/>
)	)	kIx)
</s>
<s>
Crespi	Crespi	k6eAd1
d	d	k?
<g/>
'	'	kIx"
<g/>
Adda	Addus	k1gMnSc2
je	být	k5eAaImIp3nS
malé	malý	k2eAgNnSc1d1
město	město	k1gNnSc1
v	v	k7c6
italské	italský	k2eAgFnSc6d1
Lombardii	Lombardie	k1gFnSc6
čítající	čítající	k2eAgInSc4d1
okolo	okolo	k7c2
sedmi	sedm	k4xCc2
set	sto	k4xCgNnPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
známé	známý	k2eAgNnSc1d1
především	především	k6eAd1
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
patří	patřit	k5eAaImIp3nS
ke	k	k7c3
kulturním	kulturní	k2eAgFnPc3d1
památkám	památka	k1gFnPc3
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
původně	původně	k6eAd1
dělnické	dělnický	k2eAgNnSc4d1
město	město	k1gNnSc4
s	s	k7c7
jedinečnou	jedinečný	k2eAgFnSc7d1
architekturou	architektura	k1gFnSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc1
počátky	počátek	k1gInPc1
sahají	sahat	k5eAaImIp3nP
do	do	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Crespi	Cresp	k1gFnSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
Adda	Adda	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Italské	italský	k2eAgFnSc2d1
památky	památka	k1gFnSc2
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
</s>
<s>
Agrigento	Agrigento	k1gNnSc1
•	•	k?
Alberobello	Alberobello	k1gNnSc1
•	•	k?
Amalfinské	Amalfinský	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
•	•	k?
Aquileia	Aquileius	k1gMnSc2
•	•	k?
Assisi	Assise	k1gFnSc4
•	•	k?
Benátky	Benátky	k1gFnPc1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
laguna	laguna	k1gFnSc1
•	•	k?
Benátské	benátský	k2eAgFnSc2d1
obranné	obranný	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
•	•	k?
Botanická	botanický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
v	v	k7c6
Padově	Padova	k1gFnSc6
•	•	k?
Castel	Castela	k1gFnPc2
del	del	k?
Monte	Mont	k1gInSc5
•	•	k?
Cerveteri	Cerveter	k1gFnSc2
a	a	k8xC
Tarquinia	Tarquinium	k1gNnSc2
•	•	k?
Cilento	Cilento	k1gNnSc1
<g/>
,	,	kIx,
Vallo	Vallo	k1gNnSc1
di	di	k?
Diano	Diana	k1gFnSc5
<g/>
,	,	kIx,
Paestum	Paestum	k1gNnSc1
<g/>
,	,	kIx,
Velia	Velia	k1gFnSc1
a	a	k8xC
Certosa	Certosa	k1gFnSc1
di	di	k?
Padula	Padula	k1gFnSc1
•	•	k?
Crespi	Cresp	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
d	d	k?
<g/>
'	'	kIx"
<g/>
Adda	Add	k2eAgFnSc1d1
•	•	k?
Dolomity	Dolomity	k1gInPc4
•	•	k?
Etna	Etna	k1gFnSc1
•	•	k?
Ferrara	Ferrara	k1gFnSc1
•	•	k?
Florencie	Florencie	k1gFnSc2
•	•	k?
Hadriánova	Hadriánův	k2eAgFnSc1d1
vila	vila	k1gFnSc1
•	•	k?
Ivrea	Ivrea	k1gFnSc1
•	•	k?
Janov	Janov	k1gInSc1
<g/>
:	:	kIx,
Le	Le	k1gFnSc1
Strade	Strad	k1gInSc5
Nuove	Nuoev	k1gFnPc4
a	a	k8xC
systém	systém	k1gInSc4
Palazzi	Palazze	k1gFnSc4
dei	dei	k?
Rolli	Rolle	k1gFnSc3
•	•	k?
Kopce	kopec	k1gInSc2
prosecca	prosecca	k6eAd1
Conegliano	Conegliana	k1gFnSc5
a	a	k8xC
Valdobbiadene	Valdobbiaden	k1gInSc5
•	•	k?
královský	královský	k2eAgInSc1d1
palác	palác	k1gInSc1
Caserta	Caserta	k1gFnSc1
•	•	k?
Liparské	Liparský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Mantova	Mantova	k1gFnSc1
a	a	k8xC
Sabbioneta	Sabbionet	k2eAgFnSc1d1
•	•	k?
Matera	Matera	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
mocenská	mocenský	k2eAgFnSc1d1
střediska	středisko	k1gNnPc1
Langobardů	Langobard	k1gMnPc2
v	v	k7c6
Itálii	Itálie	k1gFnSc6
•	•	k?
katedrála	katedrála	k1gFnSc1
Nanebevzetí	nanebevzetí	k1gNnPc2
Panny	Panna	k1gFnSc2
Marie	Marie	k1gFnSc1
<g/>
,	,	kIx,
brána	brána	k1gFnSc1
Torre	torr	k1gInSc5
Civica	Civica	k1gFnSc1
a	a	k8xC
náměstí	náměstí	k1gNnSc1
Piazza	Piazz	k1gMnSc2
Grande	grand	k1gMnSc5
v	v	k7c6
Modeně	Modena	k1gFnSc6
•	•	k?
Monte	Mont	k1gMnSc5
San	San	k1gMnSc5
Giorgio	Giorgia	k1gMnSc5
•	•	k?
Neapol	Neapol	k1gFnSc1
•	•	k?
Palermo	Palermo	k1gNnSc1
a	a	k8xC
katedrály	katedrála	k1gFnPc1
v	v	k7c6
Cefalú	Cefalú	k1gFnSc6
a	a	k8xC
v	v	k7c6
Monreale	Monreala	k1gFnSc6
•	•	k?
Piazza	Piazza	k1gFnSc1
del	del	k?
Duomo	Duoma	k1gFnSc5
<g/>
,	,	kIx,
Pisa	Pisa	k1gFnSc1
•	•	k?
Pienza	Pienza	k1gFnSc1
•	•	k?
Pompeje	Pompeje	k1gInPc1
<g/>
,	,	kIx,
Herculaneum	Herculaneum	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Torre	torr	k1gInSc5
Annunziata	Annunziat	k2eAgFnSc1d1
•	•	k?
Portovenere	Portovener	k1gMnSc5
<g/>
,	,	kIx,
Cinque	Cinquus	k1gMnSc5
Terre	Terr	k1gMnSc5
a	a	k8xC
ostrovy	ostrov	k1gInPc1
Palmaria	Palmarium	k1gNnSc2
<g/>
,	,	kIx,
Tino	Tina	k1gFnSc5
a	a	k8xC
Tinetto	Tinett	k2eAgNnSc4d1
•	•	k?
prehistorická	prehistorický	k2eAgNnPc1d1
kůlová	kůlový	k2eAgNnPc1d1
obydlí	obydlí	k1gNnPc1
v	v	k7c6
Alpách	Alpy	k1gFnPc6
•	•	k?
Původní	původní	k2eAgInPc4d1
bukové	bukový	k2eAgInPc4d1
lesy	les	k1gInPc4
Karpat	Karpaty	k1gInPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Evropy	Evropa	k1gFnSc2
•	•	k?
Ravenna	Ravenna	k1gFnSc1
•	•	k?
rezidence	rezidence	k1gFnSc2
královského	královský	k2eAgInSc2d1
rodu	rod	k1gInSc2
Savojských	savojský	k2eAgInPc2d1
•	•	k?
Rhétská	Rhétský	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
•	•	k?
Řím	Řím	k1gInSc1
•	•	k?
Sacri	Sacr	k1gFnSc2
Monti	Monť	k1gFnSc2
•	•	k?
San	San	k1gMnSc1
Gimignano	Gimignana	k1gFnSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Santa	Santa	k1gFnSc1
Maria	Maria	k1gFnSc1
delle	delle	k1gFnSc1
Grazie	Grazie	k1gFnSc1
•	•	k?
Siena	Siena	k1gFnSc1
•	•	k?
skalní	skalní	k2eAgFnPc4d1
kresby	kresba	k1gFnPc4
ve	v	k7c4
Val	val	k1gInSc4
Camonice	Camonice	k1gFnSc2
•	•	k?
Su	Su	k?
Nuraxi	Nuraxe	k1gFnSc4
di	di	k?
Barumini	Barumin	k2eAgMnPc1d1
•	•	k?
Syrakusy	Syrakusy	k1gFnPc4
a	a	k8xC
Pantalica	Pantalica	k1gMnSc1
•	•	k?
Urbino	Urbino	k1gNnSc4
•	•	k?
Val	val	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Orcia	Orcia	k1gFnSc1
•	•	k?
Val	val	k1gInSc4
di	di	k?
Noto	nota	k1gFnSc5
•	•	k?
Verona	Verona	k1gFnSc1
•	•	k?
Vicenza	Vicenz	k1gMnSc2
a	a	k8xC
Palladiovy	Palladiovy	k?
vily	vila	k1gFnSc2
v	v	k7c6
Benátsku	Benátsko	k1gNnSc6
•	•	k?
Villa	Villa	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
•	•	k?
Villa	Vill	k1gMnSc4
Romana	Roman	k1gMnSc4
del	del	k?
Casale	Casala	k1gFnSc6
•	•	k?
vily	vila	k1gFnSc2
a	a	k8xC
zahrady	zahrada	k1gFnSc2
Medicejských	Medicejský	k2eAgMnPc2d1
v	v	k7c6
Toskánsku	Toskánsko	k1gNnSc6
•	•	k?
vinařská	vinařský	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
Piemontu	Piemont	k1gInSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4670207-6	4670207-6	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
249407557	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Itálie	Itálie	k1gFnSc1
</s>
