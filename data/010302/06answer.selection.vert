<s>
Alžírská	alžírský	k2eAgFnSc1d1	alžírská
káva	káva	k1gFnSc1	káva
či	či	k8xC	či
lidově	lidově	k6eAd1	lidově
jen	jen	k9	jen
alžír	alžír	k1gMnSc1	alžír
je	být	k5eAaImIp3nS	být
kávový	kávový	k2eAgInSc4d1	kávový
nápoj	nápoj	k1gInSc4	nápoj
připravovaný	připravovaný	k2eAgInSc4d1	připravovaný
z	z	k7c2	z
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
šlehačky	šlehačka	k1gFnSc2	šlehačka
a	a	k8xC	a
vaječného	vaječný	k2eAgInSc2d1	vaječný
likéru	likér	k1gInSc2	likér
<g/>
.	.	kIx.	.
</s>
