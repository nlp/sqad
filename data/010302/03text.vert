<p>
<s>
Alžírská	alžírský	k2eAgFnSc1d1	alžírská
káva	káva	k1gFnSc1	káva
či	či	k8xC	či
lidově	lidově	k6eAd1	lidově
jen	jen	k9	jen
alžír	alžír	k1gMnSc1	alžír
je	být	k5eAaImIp3nS	být
kávový	kávový	k2eAgInSc4d1	kávový
nápoj	nápoj	k1gInSc4	nápoj
připravovaný	připravovaný	k2eAgInSc4d1	připravovaný
z	z	k7c2	z
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
šlehačky	šlehačka	k1gFnSc2	šlehačka
a	a	k8xC	a
vaječného	vaječný	k2eAgInSc2d1	vaječný
likéru	likér	k1gInSc2	likér
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
je	být	k5eAaImIp3nS	být
nápoj	nápoj	k1gInSc4	nápoj
známý	známý	k2eAgInSc4d1	známý
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
eggnog	eggnog	k1gInSc1	eggnog
latté	lattý	k2eAgInPc1d1	lattý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anglicky	anglicky	k6eAd1	anglicky
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
zejména	zejména	k9	zejména
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Káva	káva	k1gFnSc1	káva
-	-	kIx~	-
recepty	recept	k1gInPc1	recept
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
i	i	k8xC	i
neobvyklé	obvyklý	k2eNgFnSc2d1	neobvyklá
</s>
</p>
<p>
<s>
Alžírská	alžírský	k2eAgFnSc1d1	alžírská
káva	káva	k1gFnSc1	káva
na	na	k7c4	na
Alkoholium	Alkoholium	k1gNnSc4	Alkoholium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
