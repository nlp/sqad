<s>
Tantal	tantal	k1gInSc1	tantal
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Ta	ten	k3xDgFnSc1	ten
latinsky	latinsky	k6eAd1	latinsky
Tantalum	Tantalum	k?	Tantalum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgInSc4d1	vzácný
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgInSc4d1	tvrdý
<g/>
,	,	kIx,	,
modro-šedý	modro-šedý	k2eAgInSc4d1	modro-šedý
<g/>
,	,	kIx,	,
lesklý	lesklý	k2eAgInSc4d1	lesklý
<g/>
,	,	kIx,	,
přechodný	přechodný	k2eAgInSc4d1	přechodný
kov	kov	k1gInSc4	kov
<g/>
.	.	kIx.	.
</s>
