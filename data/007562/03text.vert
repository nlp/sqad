<s>
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
HST	HST	kA	HST
z	z	k7c2	z
Hubble	Hubble	k1gFnSc2	Hubble
Space	Space	k1gMnSc5	Space
Telescope	Telescop	k1gMnSc5	Telescop
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
Hubble	Hubble	k1gFnSc1	Hubble
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dalekohled	dalekohled	k1gInSc1	dalekohled
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Země	zem	k1gFnSc2	zem
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
600	[number]	k4	600
kilometrů	kilometr	k1gInPc2	kilometr
vynesl	vynést	k5eAaPmAgInS	vynést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
při	při	k7c6	při
letu	let	k1gInSc6	let
STS-31	STS-31	k1gFnSc2	STS-31
americký	americký	k2eAgInSc4d1	americký
raketoplán	raketoplán	k1gInSc4	raketoplán
Discovery	Discovera	k1gFnSc2	Discovera
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
569	[number]	k4	569
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
předává	předávat	k5eAaImIp3nS	předávat
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
obrazy	obraz	k1gInPc1	obraz
vesmíru	vesmír	k1gInSc2	vesmír
neovlivněné	ovlivněný	k2eNgInPc1d1	neovlivněný
zemskou	zemský	k2eAgFnSc7d1	zemská
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
umístění	umístění	k1gNnSc1	umístění
mimo	mimo	k7c4	mimo
zemskou	zemský	k2eAgFnSc4d1	zemská
atmosféru	atmosféra	k1gFnSc4	atmosféra
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pořizovat	pořizovat	k5eAaImF	pořizovat
velmi	velmi	k6eAd1	velmi
ostré	ostrý	k2eAgInPc1d1	ostrý
snímky	snímek	k1gInPc1	snímek
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
vypuštění	vypuštění	k1gNnSc2	vypuštění
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
dalekohledů	dalekohled	k1gInPc2	dalekohled
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
astronomie	astronomie	k1gFnSc2	astronomie
a	a	k8xC	a
významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
prohloubení	prohloubení	k1gNnSc4	prohloubení
poznatků	poznatek	k1gInPc2	poznatek
o	o	k7c6	o
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
mnohým	mnohý	k2eAgInPc3d1	mnohý
klíčovým	klíčový	k2eAgInPc3d1	klíčový
objevům	objev	k1gInPc3	objev
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pomohly	pomoct	k5eAaPmAgInP	pomoct
astronomům	astronom	k1gMnPc3	astronom
lépe	dobře	k6eAd2	dobře
porozumět	porozumět	k5eAaPmF	porozumět
základním	základní	k2eAgInPc3d1	základní
problémům	problém	k1gInPc3	problém
astrofyziky	astrofyzika	k1gFnSc2	astrofyzika
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
ceněné	ceněný	k2eAgFnPc1d1	ceněná
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
snímky	snímek	k1gInPc1	snímek
tzv.	tzv.	kA	tzv.
Hubbleových	Hubbleův	k2eAgFnPc2d1	Hubbleova
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
polí	pole	k1gFnPc2	pole
(	(	kIx(	(
<g/>
Hubble	Hubble	k1gFnSc2	Hubble
ultra	ultra	k2eAgMnSc1d1	ultra
deep	deep	k1gMnSc1	deep
fields	fieldsa	k1gFnPc2	fieldsa
<g/>
)	)	kIx)	)
s	s	k7c7	s
nejvzdálenějšími	vzdálený	k2eAgInPc7d3	nejvzdálenější
objekty	objekt	k1gInPc7	objekt
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zatím	zatím	k6eAd1	zatím
lidstvo	lidstvo	k1gNnSc1	lidstvo
bylo	být	k5eAaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
teleskop	teleskop	k1gInSc1	teleskop
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
série	série	k1gFnSc2	série
Velkých	velký	k2eAgFnPc2d1	velká
kosmických	kosmický	k2eAgFnPc2d1	kosmická
observatoří	observatoř	k1gFnPc2	observatoř
<g/>
,	,	kIx,	,
programu	program	k1gInSc2	program
výzkumu	výzkum	k1gInSc2	výzkum
vesmíru	vesmír	k1gInSc2	vesmír
amerického	americký	k2eAgInSc2d1	americký
Národního	národní	k2eAgInSc2d1	národní
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
(	(	kIx(	(
<g/>
NASA	NASA	kA	NASA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
observatořemi	observatoř	k1gFnPc7	observatoř
jsou	být	k5eAaImIp3nP	být
Comptonova	Comptonův	k2eAgNnSc2d1	Comptonův
gama	gama	k1gNnSc2	gama
observatoř	observatoř	k1gFnSc1	observatoř
(	(	kIx(	(
<g/>
Compton	Compton	k1gInSc1	Compton
Gamma	Gammum	k1gNnSc2	Gammum
Ray	Ray	k1gFnSc2	Ray
Observatory	Observator	k1gInPc1	Observator
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rentgenový	rentgenový	k2eAgInSc1d1	rentgenový
teleskop	teleskop	k1gInSc1	teleskop
Chandra	chandra	k1gFnSc1	chandra
(	(	kIx(	(
<g/>
Chandra	chandra	k1gFnSc1	chandra
X-ray	Xaa	k1gFnSc2	X-raa
Observatory	Observator	k1gInPc1	Observator
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spitzerův	Spitzerův	k2eAgInSc1d1	Spitzerův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
(	(	kIx(	(
<g/>
Spitzer	Spitzer	k1gInSc1	Spitzer
Space	Space	k1gMnSc5	Space
Telescope	Telescop	k1gMnSc5	Telescop
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
nespadající	spadající	k2eNgInSc1d1	nespadající
do	do	k7c2	do
programu	program	k1gInSc2	program
Velkých	velký	k2eAgFnPc2d1	velká
kosmických	kosmický	k2eAgFnPc2d1	kosmická
observatoří	observatoř	k1gFnPc2	observatoř
je	být	k5eAaImIp3nS	být
Vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
Jamese	Jamese	k1gFnSc1	Jamese
Webba	Webba	k1gMnSc1	Webba
(	(	kIx(	(
<g/>
JWST	JWST	kA	JWST
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
vypuštěn	vypustit	k5eAaPmNgInS	vypustit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
a	a	k8xC	a
který	který	k3yRgInSc1	který
bude	být	k5eAaImBp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
budoucnost	budoucnost	k1gFnSc1	budoucnost
dalekohledu	dalekohled	k1gInSc2	dalekohled
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
předmětem	předmět	k1gInSc7	předmět
diskuse	diskuse	k1gFnSc2	diskuse
odborníků	odborník	k1gMnPc2	odborník
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
servisní	servisní	k2eAgFnSc1d1	servisní
mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
raketoplánu	raketoplán	k1gInSc2	raketoplán
Columbia	Columbia	k1gFnSc1	Columbia
zcela	zcela	k6eAd1	zcela
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
vědecké	vědecký	k2eAgFnSc2d1	vědecká
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
astronomů	astronom	k1gMnPc2	astronom
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
NASA	NASA	kA	NASA
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
předběžně	předběžně	k6eAd1	předběžně
znovu	znovu	k6eAd1	znovu
zařadila	zařadit	k5eAaPmAgFnS	zařadit
do	do	k7c2	do
plánu	plán	k1gInSc2	plán
letů	let	k1gInPc2	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
technických	technický	k2eAgInPc2d1	technický
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
však	však	k9	však
termín	termín	k1gInSc1	termín
startu	start	k1gInSc2	start
mise	mise	k1gFnSc2	mise
STS-125	STS-125	k1gFnPc2	STS-125
odsunut	odsunut	k2eAgMnSc1d1	odsunut
na	na	k7c4	na
květen	květen	k1gInSc4	květen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
další	další	k2eAgFnSc7d1	další
servisní	servisní	k2eAgFnSc7d1	servisní
misí	mise	k1gFnSc7	mise
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nepočítá	počítat	k5eNaImIp3nS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
servisní	servisní	k2eAgFnSc3d1	servisní
misi	mise	k1gFnSc3	mise
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
by	by	kYmCp3nP	by
selhaly	selhat	k5eAaPmAgFnP	selhat
stabilizační	stabilizační	k2eAgInPc4d1	stabilizační
setrvačníky	setrvačník	k1gInPc4	setrvačník
a	a	k8xC	a
fotovoltaické	fotovoltaický	k2eAgInPc4d1	fotovoltaický
články	článek	k1gInPc4	článek
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
desetiletí	desetiletí	k1gNnPc2	desetiletí
by	by	kYmCp3nS	by
dalekohled	dalekohled	k1gInSc1	dalekohled
přestal	přestat	k5eAaPmAgInS	přestat
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenější	vzdálený	k2eAgFnSc6d2	vzdálenější
budoucnosti	budoucnost	k1gFnSc6	budoucnost
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
v	v	k7c6	v
hustých	hustý	k2eAgFnPc6d1	hustá
vrstvách	vrstva	k1gFnPc6	vrstva
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
Lyman	Lyman	k1gMnSc1	Lyman
Spitzer	Spitzer	k1gMnSc1	Spitzer
publikoval	publikovat	k5eAaBmAgMnS	publikovat
článek	článek	k1gInSc4	článek
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
Astronomické	astronomický	k2eAgNnSc4d1	astronomické
výhody	výhoda	k1gFnPc4	výhoda
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
mimo	mimo	k7c4	mimo
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
článku	článek	k1gInSc6	článek
pojednával	pojednávat	k5eAaImAgMnS	pojednávat
o	o	k7c6	o
dvou	dva	k4xCgFnPc6	dva
hlavních	hlavní	k2eAgFnPc6d1	hlavní
výhodách	výhoda	k1gFnPc6	výhoda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
vesmírné	vesmírný	k2eAgFnPc4d1	vesmírná
observatoře	observatoř	k1gFnPc4	observatoř
oproti	oproti	k7c3	oproti
pozemským	pozemský	k2eAgInPc3d1	pozemský
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
úhlové	úhlový	k2eAgNnSc1d1	úhlové
rozlišení	rozlišení	k1gNnSc1	rozlišení
(	(	kIx(	(
<g/>
nejmenší	malý	k2eAgFnSc1d3	nejmenší
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
objekty	objekt	k1gInPc4	objekt
jasně	jasně	k6eAd1	jasně
rozlišitelné	rozlišitelný	k2eAgInPc4d1	rozlišitelný
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
limitované	limitovaný	k2eAgNnSc1d1	limitované
pouze	pouze	k6eAd1	pouze
difrakcí	difrakce	k1gFnPc2	difrakce
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
turbulencí	turbulence	k1gFnPc2	turbulence
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
blikání	blikání	k1gNnSc4	blikání
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
astronomům	astronom	k1gMnPc3	astronom
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
seeing	seeing	k1gInSc1	seeing
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byly	být	k5eAaImAgFnP	být
pozemské	pozemský	k2eAgInPc4d1	pozemský
dalekohledy	dalekohled	k1gInPc4	dalekohled
limitované	limitovaný	k2eAgFnSc2d1	limitovaná
typickým	typický	k2eAgNnSc7d1	typické
rozlišením	rozlišení	k1gNnSc7	rozlišení
0,25	[number]	k4	0,25
<g/>
–	–	k?	–
<g/>
0,5	[number]	k4	0,5
úhlových	úhlový	k2eAgFnPc2d1	úhlová
vteřin	vteřina	k1gFnPc2	vteřina
<g/>
,	,	kIx,	,
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
teoretickým	teoretický	k2eAgNnSc7d1	teoretické
difrakčním	difrakční	k2eAgNnSc7d1	difrakční
omezením	omezení	k1gNnSc7	omezení
okolo	okolo	k7c2	okolo
0,1	[number]	k4	0,1
úhlové	úhlový	k2eAgFnSc2d1	úhlová
vteřiny	vteřina	k1gFnSc2	vteřina
pro	pro	k7c4	pro
teleskopy	teleskop	k1gInPc4	teleskop
se	s	k7c7	s
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
2,5	[number]	k4	2,5
m.	m.	k?	m.
Druhá	druhý	k4xOgFnSc1	druhý
hlavní	hlavní	k2eAgFnSc1d1	hlavní
výhoda	výhoda	k1gFnSc1	výhoda
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
infračervené	infračervený	k2eAgNnSc4d1	infračervené
a	a	k8xC	a
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Spitzer	Spitzer	k1gMnSc1	Spitzer
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
vývoji	vývoj	k1gInSc3	vývoj
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
správa	správa	k1gFnSc1	správa
americké	americký	k2eAgFnSc2d1	americká
národní	národní	k2eAgFnSc2d1	národní
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
doporučila	doporučit	k5eAaPmAgFnS	doporučit
vývoj	vývoj	k1gInSc4	vývoj
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
jako	jako	k8xS	jako
část	část	k1gFnSc4	část
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
programu	program	k1gInSc2	program
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
byl	být	k5eAaImAgMnS	být
Spitzer	Spitzer	k1gMnSc1	Spitzer
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
definovat	definovat	k5eAaBmF	definovat
jeho	jeho	k3xOp3gInPc4	jeho
vědecké	vědecký	k2eAgInPc4d1	vědecký
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
astronomie	astronomie	k1gFnSc1	astronomie
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
pomalu	pomalu	k6eAd1	pomalu
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vědci	vědec	k1gMnPc1	vědec
našli	najít	k5eAaPmAgMnP	najít
možné	možný	k2eAgNnSc4d1	možné
uplatnění	uplatnění	k1gNnSc4	uplatnění
pro	pro	k7c4	pro
vynálezy	vynález	k1gInPc4	vynález
raketové	raketový	k2eAgFnSc2d1	raketová
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
bylo	být	k5eAaImAgNnS	být
pořízeno	pořízen	k2eAgNnSc1d1	pořízeno
první	první	k4xOgNnSc4	první
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
spektrum	spektrum	k1gNnSc4	spektrum
Slunce	slunce	k1gNnSc2	slunce
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
88	[number]	k4	88
km	km	kA	km
pomocí	pomocí	k7c2	pomocí
přístrojů	přístroj	k1gInPc2	přístroj
nesených	nesený	k2eAgInPc2d1	nesený
v	v	k7c6	v
trupu	trup	k1gInSc6	trup
rakety	raketa	k1gFnSc2	raketa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
byla	být	k5eAaImAgFnS	být
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
vypuštěna	vypustit	k5eAaPmNgNnP	vypustit
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Země	zem	k1gFnSc2	zem
sluneční	sluneční	k2eAgFnSc4d1	sluneční
observatoř	observatoř	k1gFnSc4	observatoř
jako	jako	k8xS	jako
část	část	k1gFnSc4	část
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
programu	program	k1gInSc2	program
Ariel	Ariela	k1gFnPc2	Ariela
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
zažil	zažít	k5eAaPmAgMnS	zažít
svět	svět	k1gInSc4	svět
vypuštění	vypuštění	k1gNnSc2	vypuštění
první	první	k4xOgFnSc2	první
Orbitální	orbitální	k2eAgFnSc2d1	orbitální
astronomické	astronomický	k2eAgFnSc2d1	astronomická
observatoře	observatoř	k1gFnSc2	observatoř
(	(	kIx(	(
<g/>
OAO	OAO	kA	OAO
<g/>
)	)	kIx)	)
vesmírnou	vesmírný	k2eAgFnSc7d1	vesmírná
agenturou	agentura	k1gFnSc7	agentura
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
Misi	mise	k1gFnSc3	mise
OAO-1	OAO-1	k1gFnSc2	OAO-1
selhaly	selhat	k5eAaPmAgFnP	selhat
baterie	baterie	k1gFnPc1	baterie
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
dnech	den	k1gInPc6	den
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
následující	následující	k2eAgFnSc1d1	následující
mise	mise	k1gFnSc1	mise
OAO-2	OAO-2	k1gFnSc2	OAO-2
prováděla	provádět	k5eAaImAgFnS	provádět
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
pozorování	pozorování	k1gNnSc4	pozorování
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
galaxií	galaxie	k1gFnPc2	galaxie
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
startu	start	k1gInSc2	start
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
hranicí	hranice	k1gFnSc7	hranice
původně	původně	k6eAd1	původně
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
životnosti	životnost	k1gFnSc2	životnost
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnPc1	mise
OAO	OAO	kA	OAO
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
by	by	kYmCp3nS	by
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
mohlo	moct	k5eAaImAgNnS	moct
sehrávat	sehrávat	k5eAaImF	sehrávat
pozorování	pozorování	k1gNnSc1	pozorování
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
zrodily	zrodit	k5eAaPmAgInP	zrodit
smělé	smělý	k2eAgInPc1d1	smělý
plány	plán	k1gInPc1	plán
NASA	NASA	kA	NASA
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
zrcadlového	zrcadlový	k2eAgInSc2d1	zrcadlový
dalekohledu	dalekohled	k1gInSc2	dalekohled
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
3	[number]	k4	3
metry	metr	k1gMnPc7	metr
<g/>
,	,	kIx,	,
známým	známý	k1gMnSc7	známý
jako	jako	k8xC	jako
Velký	velký	k2eAgInSc1d1	velký
orbitální	orbitální	k2eAgInSc1d1	orbitální
dalekohled	dalekohled	k1gInSc1	dalekohled
či	či	k8xC	či
Velký	velký	k2eAgInSc1d1	velký
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
datum	datum	k1gNnSc1	datum
jeho	jeho	k3xOp3gNnSc2	jeho
vypuštění	vypuštění	k1gNnSc2	vypuštění
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
rok	rok	k1gInSc1	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
zdůrazňovaly	zdůrazňovat	k5eAaImAgInP	zdůrazňovat
potřebu	potřeba	k1gFnSc4	potřeba
servisních	servisní	k2eAgFnPc2d1	servisní
misí	mise	k1gFnPc2	mise
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
posádkou	posádka	k1gFnSc7	posádka
k	k	k7c3	k
teleskopu	teleskop	k1gInSc3	teleskop
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
zaručené	zaručený	k2eAgNnSc1d1	zaručené
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k9	tak
finančně	finančně	k6eAd1	finančně
náročný	náročný	k2eAgInSc1d1	náročný
projekt	projekt	k1gInSc1	projekt
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
příslušně	příslušně	k6eAd1	příslušně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
životnost	životnost	k1gFnSc4	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Paralelně	paralelně	k6eAd1	paralelně
vyvíjené	vyvíjený	k2eAgInPc1d1	vyvíjený
plány	plán	k1gInPc1	plán
pro	pro	k7c4	pro
znovupoužitelné	znovupoužitelný	k2eAgInPc4d1	znovupoužitelný
kosmické	kosmický	k2eAgInPc4d1	kosmický
raketoplány	raketoplán	k1gInPc4	raketoplán
naznačovaly	naznačovat	k5eAaImAgFnP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožní	umožnit	k5eAaPmIp3nS	umožnit
servisní	servisní	k2eAgInPc4d1	servisní
lety	let	k1gInPc4	let
k	k	k7c3	k
dalekohledu	dalekohled	k1gInSc3	dalekohled
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
brzy	brzy	k6eAd1	brzy
dostupná	dostupný	k2eAgFnSc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
programu	program	k1gInSc2	program
OAO	OAO	kA	OAO
podpořil	podpořit	k5eAaPmAgMnS	podpořit
zvyšující	zvyšující	k2eAgMnSc1d1	zvyšující
se	se	k3xPyFc4	se
konsenzus	konsenzus	k1gInSc1	konsenzus
mezi	mezi	k7c7	mezi
astronomickou	astronomický	k2eAgFnSc7d1	astronomická
komunitou	komunita	k1gFnSc7	komunita
<g/>
,	,	kIx,	,
že	že	k8xS	že
Velký	velký	k2eAgInSc1d1	velký
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
LST	LST	kA	LST
–	–	k?	–
Large	Largus	k1gMnSc5	Largus
Space	Space	k1gMnSc5	Space
Telescope	Telescop	k1gMnSc5	Telescop
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
NASA	NASA	kA	NASA
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
dvě	dva	k4xCgFnPc4	dva
komise	komise	k1gFnPc4	komise
<g/>
:	:	kIx,	:
jednu	jeden	k4xCgFnSc4	jeden
pro	pro	k7c4	pro
konstrukční	konstrukční	k2eAgFnSc4d1	konstrukční
část	část	k1gFnSc4	část
projektu	projekt	k1gInSc2	projekt
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
vědeckých	vědecký	k2eAgInPc2d1	vědecký
cílů	cíl	k1gInPc2	cíl
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
bylo	být	k5eAaImAgNnS	být
získání	získání	k1gNnSc1	získání
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
mnohem	mnohem	k6eAd1	mnohem
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
teleskop	teleskop	k1gInSc1	teleskop
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
si	se	k3xPyFc3	se
nejprve	nejprve	k6eAd1	nejprve
vynutil	vynutit	k5eAaPmAgMnS	vynutit
škrty	škrt	k1gInPc7	škrt
v	v	k7c6	v
plánovací	plánovací	k2eAgFnSc6d1	plánovací
fázi	fáze	k1gFnSc6	fáze
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
detailních	detailní	k2eAgNnPc2d1	detailní
studií	studio	k1gNnPc2	studio
potenciálních	potenciální	k2eAgInPc2d1	potenciální
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
technického	technický	k2eAgNnSc2d1	technické
vybavení	vybavení	k1gNnSc2	vybavení
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
škrty	škrt	k1gInPc7	škrt
ve	v	k7c6	v
veřejných	veřejný	k2eAgInPc6d1	veřejný
výdajích	výdaj	k1gInPc6	výdaj
<g/>
,	,	kIx,	,
podnícené	podnícený	k2eAgNnSc1d1	podnícené
prezidentem	prezident	k1gMnSc7	prezident
Geraldem	Gerald	k1gMnSc7	Gerald
Fordem	ford	k1gInSc7	ford
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgInP	vést
Kongres	kongres	k1gInSc4	kongres
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
celého	celý	k2eAgNnSc2d1	celé
financování	financování	k1gNnSc2	financování
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
teleskopu	teleskop	k1gInSc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědí	odpověď	k1gFnSc7	odpověď
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
USA	USA	kA	USA
začátek	začátek	k1gInSc4	začátek
celonárodního	celonárodní	k2eAgNnSc2d1	celonárodní
lobbistického	lobbistický	k2eAgNnSc2d1	lobbistické
úsilí	úsilí	k1gNnSc2	úsilí
za	za	k7c4	za
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
,	,	kIx,	,
koordinovaného	koordinovaný	k2eAgNnSc2d1	koordinované
astronomy	astronom	k1gMnPc4	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
kongresmany	kongresman	k1gMnPc7	kongresman
a	a	k8xC	a
senátory	senátor	k1gMnPc7	senátor
osobně	osobně	k6eAd1	osobně
a	a	k8xC	a
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
organizovali	organizovat	k5eAaBmAgMnP	organizovat
také	také	k9	také
dopisové	dopisový	k2eAgFnPc4d1	dopisová
kampaně	kampaň	k1gFnPc4	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
vydala	vydat	k5eAaPmAgFnS	vydat
zprávu	zpráva	k1gFnSc4	zpráva
zdůrazňující	zdůrazňující	k2eAgFnSc4d1	zdůrazňující
potřebu	potřeba	k1gFnSc4	potřeba
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
teleskopu	teleskop	k1gInSc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
nakonec	nakonec	k6eAd1	nakonec
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
přiznáním	přiznání	k1gNnSc7	přiznání
alespoň	alespoň	k9	alespoň
poloviny	polovina	k1gFnSc2	polovina
původní	původní	k2eAgFnSc2d1	původní
finanční	finanční	k2eAgFnSc2d1	finanční
částky	částka	k1gFnSc2	částka
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
400	[number]	k4	400
<g/>
−	−	k?	−
<g/>
500	[number]	k4	500
miliónů	milión	k4xCgInPc2	milión
USD	USD	kA	USD
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Sněmovna	sněmovna	k1gFnSc1	sněmovna
dříve	dříve	k6eAd2	dříve
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
s	s	k7c7	s
financováním	financování	k1gNnSc7	financování
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
navrhovaný	navrhovaný	k2eAgInSc1d1	navrhovaný
průměr	průměr	k1gInSc1	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
z	z	k7c2	z
3	[number]	k4	3
m	m	kA	m
na	na	k7c4	na
2,4	[number]	k4	2,4
m	m	kA	m
a	a	k8xC	a
navrhovaný	navrhovaný	k2eAgInSc1d1	navrhovaný
0,5	[number]	k4	0,5
m	m	kA	m
teleskop	teleskop	k1gInSc1	teleskop
pro	pro	k7c4	pro
testování	testování	k1gNnSc4	testování
systémů	systém	k1gInPc2	systém
použitých	použitý	k2eAgInPc2d1	použitý
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
dalekohledu	dalekohled	k1gInSc6	dalekohled
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Redukce	redukce	k1gFnSc1	redukce
však	však	k9	však
vedla	vést	k5eAaImAgFnS	vést
také	také	k9	také
ke	k	k7c3	k
kompaktnější	kompaktní	k2eAgFnSc3d2	kompaktnější
a	a	k8xC	a
efektivnější	efektivní	k2eAgFnSc3d2	efektivnější
konfiguraci	konfigurace	k1gFnSc3	konfigurace
technického	technický	k2eAgNnSc2d1	technické
vybavení	vybavení	k1gNnSc2	vybavení
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgInPc1d1	finanční
problémy	problém	k1gInPc1	problém
naznačily	naznačit	k5eAaPmAgInP	naznačit
nutnost	nutnost	k1gFnSc4	nutnost
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
agenturou	agentura	k1gFnSc7	agentura
(	(	kIx(	(
<g/>
ESA	eso	k1gNnSc2	eso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
dodávkou	dodávka	k1gFnSc7	dodávka
několika	několik	k4yIc2	několik
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
i	i	k8xC	i
solárních	solární	k2eAgInPc2d1	solární
panelů	panel	k1gInPc2	panel
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
uhradila	uhradit	k5eAaPmAgFnS	uhradit
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
%	%	kIx~	%
nákladů	náklad	k1gInPc2	náklad
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
garanci	garance	k1gFnSc4	garance
15	[number]	k4	15
%	%	kIx~	%
pozorovacího	pozorovací	k2eAgInSc2d1	pozorovací
času	čas	k1gInSc2	čas
teleskopu	teleskop	k1gInSc2	teleskop
pro	pro	k7c4	pro
evropské	evropský	k2eAgMnPc4d1	evropský
astronomy	astronom	k1gMnPc4	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
poté	poté	k6eAd1	poté
schválil	schválit	k5eAaPmAgInS	schválit
částku	částka	k1gFnSc4	částka
36	[number]	k4	36
000	[number]	k4	000
000	[number]	k4	000
USD	USD	kA	USD
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
již	již	k6eAd1	již
mohly	moct	k5eAaImAgFnP	moct
začít	začít	k5eAaPmF	začít
vlastní	vlastní	k2eAgFnPc4d1	vlastní
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vynesením	vynesení	k1gNnSc7	vynesení
dalekohledu	dalekohled	k1gInSc2	dalekohled
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
teleskop	teleskop	k1gInSc1	teleskop
dostal	dostat	k5eAaPmAgInS	dostat
nové	nový	k2eAgNnSc4d1	nové
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
Edwinu	Edwin	k1gMnSc6	Edwin
Hubbleovi	Hubbleus	k1gMnSc6	Hubbleus
<g/>
,	,	kIx,	,
americkém	americký	k2eAgMnSc6d1	americký
astronomovi	astronom	k1gMnSc6	astronom
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
svými	svůj	k3xOyFgMnPc7	svůj
pozorováními	pozorování	k1gNnPc7	pozorování
prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vesmír	vesmír	k1gInSc1	vesmír
se	se	k3xPyFc4	se
prostírá	prostírat	k5eAaImIp3nS	prostírat
i	i	k9	i
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
naší	náš	k3xOp1gFnSc2	náš
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yRgInSc1	který
svým	svůj	k3xOyFgInSc7	svůj
objevem	objev	k1gInSc7	objev
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yRnSc7	co
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
vesmírné	vesmírný	k2eAgInPc1d1	vesmírný
objekty	objekt	k1gInPc1	objekt
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
rychleji	rychle	k6eAd2	rychle
se	se	k3xPyFc4	se
vzdalují	vzdalovat	k5eAaImIp3nP	vzdalovat
<g/>
,	,	kIx,	,
podpořil	podpořit	k5eAaPmAgMnS	podpořit
teorii	teorie	k1gFnSc4	teorie
rozpínání	rozpínání	k1gNnSc2	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
dostal	dostat	k5eAaPmAgInS	dostat
program	program	k1gInSc1	program
zelenou	zelená	k1gFnSc4	zelená
<g/>
,	,	kIx,	,
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
se	se	k3xPyFc4	se
úlohy	úloha	k1gFnSc2	úloha
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
za	za	k7c4	za
design	design	k1gInSc4	design
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
výstavbu	výstavba	k1gFnSc4	výstavba
mělo	mít	k5eAaImAgNnS	mít
Marshall	Marshall	k1gInSc4	Marshall
Space	Spaec	k1gInSc2	Spaec
Flight	Flight	k1gInSc4	Flight
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Goddardovo	Goddardův	k2eAgNnSc4d1	Goddardovo
centrum	centrum	k1gNnSc4	centrum
pro	pro	k7c4	pro
vesmírné	vesmírný	k2eAgInPc4d1	vesmírný
lety	let	k1gInPc4	let
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
celkový	celkový	k2eAgInSc1d1	celkový
dozor	dozor	k1gInSc1	dozor
nad	nad	k7c7	nad
vědeckými	vědecký	k2eAgInPc7d1	vědecký
přístroji	přístroj	k1gInPc7	přístroj
a	a	k8xC	a
pozemní	pozemní	k2eAgNnSc4d1	pozemní
řízení	řízení	k1gNnSc4	řízení
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
Marshallovo	Marshallův	k2eAgNnSc1d1	Marshallovo
centrum	centrum	k1gNnSc1	centrum
pověřilo	pověřit	k5eAaPmAgNnS	pověřit
společnost	společnost	k1gFnSc4	společnost
Perkin-Elmer	Perkin-Elmer	k1gInSc4	Perkin-Elmer
výrobou	výroba	k1gFnSc7	výroba
optické	optický	k2eAgFnSc2d1	optická
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
Optical	Optical	k1gMnSc5	Optical
Telescope	Telescop	k1gMnSc5	Telescop
Assembly	Assembly	k1gMnSc5	Assembly
–	–	k?	–
OTA	Oto	k1gMnSc4	Oto
<g/>
)	)	kIx)	)
a	a	k8xC	a
senzorů	senzor	k1gInPc2	senzor
pro	pro	k7c4	pro
jemné	jemný	k2eAgNnSc4d1	jemné
navádění	navádění	k1gNnSc4	navádění
(	(	kIx(	(
<g/>
Fine	Fin	k1gMnSc5	Fin
Guidance	Guidanec	k1gInPc4	Guidanec
Sensors	Sensors	k1gInSc1	Sensors
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Korpus	korpus	k1gInSc4	korpus
dalekohledu	dalekohled	k1gInSc2	dalekohled
měla	mít	k5eAaImAgFnS	mít
zhotovit	zhotovit	k5eAaPmF	zhotovit
společnost	společnost	k1gFnSc1	společnost
Lockheed	Lockheed	k1gInSc4	Lockheed
<g/>
.	.	kIx.	.
</s>
<s>
Optická	optický	k2eAgFnSc1d1	optická
soustava	soustava	k1gFnSc1	soustava
dalekohledu	dalekohled	k1gInSc2	dalekohled
používá	používat	k5eAaImIp3nS	používat
systém	systém	k1gInSc4	systém
hyperbolických	hyperbolický	k2eAgNnPc2d1	hyperbolické
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
Ritchey-Chretien	Ritchey-Chretina	k1gFnPc2	Ritchey-Chretina
<g/>
.	.	kIx.	.
</s>
<s>
Zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
a	a	k8xC	a
optická	optický	k2eAgFnSc1d1	optická
soustava	soustava	k1gFnSc1	soustava
teleskopu	teleskop	k1gInSc2	teleskop
byly	být	k5eAaImAgFnP	být
jeho	jeho	k3xOp3gFnPc7	jeho
nejdůležitějšími	důležitý	k2eAgFnPc7d3	nejdůležitější
součástmi	součást	k1gFnPc7	součást
a	a	k8xC	a
designéři	designér	k1gMnPc1	designér
je	on	k3xPp3gMnPc4	on
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
splňovaly	splňovat	k5eAaImAgFnP	splňovat
náročné	náročný	k2eAgFnPc1d1	náročná
podmínky	podmínka	k1gFnPc1	podmínka
kosmického	kosmický	k2eAgNnSc2d1	kosmické
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohledy	dalekohled	k1gInPc1	dalekohled
mívají	mívat	k5eAaImIp3nP	mívat
obvykle	obvykle	k6eAd1	obvykle
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
vyleštěná	vyleštěný	k2eAgFnSc1d1	vyleštěná
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
na	na	k7c4	na
asi	asi	k9	asi
desetinu	desetina	k1gFnSc4	desetina
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnPc4	pozorování
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
od	od	k7c2	od
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
až	až	k9	až
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
záření	záření	k1gNnSc2	záření
blízkého	blízký	k2eAgNnSc2d1	blízké
infračervenému	infračervený	k2eAgInSc3d1	infračervený
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
vyleštěné	vyleštěný	k2eAgNnSc1d1	vyleštěné
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
65	[number]	k4	65
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
10	[number]	k4	10
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Perkin	Perkina	k1gFnPc2	Perkina
Elmer	Elmra	k1gFnPc2	Elmra
navrhovala	navrhovat	k5eAaImAgFnS	navrhovat
důmyslné	důmyslný	k2eAgFnPc4d1	důmyslná
<g/>
,	,	kIx,	,
počítačem	počítač	k1gInSc7	počítač
řízené	řízený	k2eAgNnSc1d1	řízené
leštící	leštící	k2eAgInPc1d1	leštící
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
vybrousily	vybrousit	k5eAaPmAgFnP	vybrousit
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
do	do	k7c2	do
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
možným	možný	k2eAgFnPc3d1	možná
komplikacím	komplikace	k1gFnPc3	komplikace
pověřil	pověřit	k5eAaPmAgInS	pověřit
tým	tým	k1gInSc1	tým
konstruktérů	konstruktér	k1gMnPc2	konstruktér
firmu	firma	k1gFnSc4	firma
Kodak	Kodak	kA	Kodak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sestrojila	sestrojit	k5eAaPmAgFnS	sestrojit
záložní	záložní	k2eAgNnSc4d1	záložní
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
broušené	broušený	k2eAgNnSc4d1	broušené
tradičními	tradiční	k2eAgInPc7d1	tradiční
brusnými	brusný	k2eAgFnPc7d1	brusná
technikami	technika	k1gFnPc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
trvale	trvale	k6eAd1	trvale
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
ve	v	k7c6	v
Smithsonově	Smithsonův	k2eAgInSc6d1	Smithsonův
institutu	institut	k1gInSc6	institut
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
záložní	záložní	k2eAgNnSc1d1	záložní
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
vyrobené	vyrobený	k2eAgNnSc1d1	vyrobené
firmou	firma	k1gFnSc7	firma
Itek	Iteka	k1gFnPc2	Iteka
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
využíváno	využívat	k5eAaPmNgNnS	využívat
v	v	k7c6	v
teleskopu	teleskop	k1gInSc6	teleskop
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
2,4	[number]	k4	2,4
metru	metr	k1gInSc2	metr
v	v	k7c6	v
Magdalena	Magdalena	k1gFnSc1	Magdalena
Ridge	Ridge	k1gFnSc7	Ridge
Observatory	Observator	k1gInPc1	Observator
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
společností	společnost	k1gFnSc7	společnost
Perkin	Perkin	k2eAgInSc4d1	Perkin
Elmer	Elmer	k1gInSc4	Elmer
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Použito	použit	k2eAgNnSc4d1	použito
bylo	být	k5eAaImAgNnS	být
nízkoroztažné	nízkoroztažný	k2eAgNnSc1d1	nízkoroztažný
sklo	sklo	k1gNnSc1	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
co	co	k3yQnSc1	co
nejmenší	malý	k2eAgFnSc1d3	nejmenší
<g/>
,	,	kIx,	,
skládalo	skládat	k5eAaImAgNnS	skládat
se	se	k3xPyFc4	se
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
silných	silný	k2eAgFnPc2d1	silná
asi	asi	k9	asi
2,5	[number]	k4	2,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
mřížka	mřížka	k1gFnSc1	mřížka
se	s	k7c7	s
strukturou	struktura	k1gFnSc7	struktura
včelí	včelí	k2eAgFnSc2d1	včelí
plástve	plástev	k1gFnSc2	plástev
<g/>
.	.	kIx.	.
</s>
<s>
Broušení	broušení	k1gNnSc1	broušení
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
května	květen	k1gInSc2	květen
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
práce	práce	k1gFnPc1	práce
měly	mít	k5eAaImAgFnP	mít
zpoždění	zpoždění	k1gNnSc4	zpoždění
a	a	k8xC	a
překročen	překročen	k2eAgMnSc1d1	překročen
byl	být	k5eAaImAgInS	být
také	také	k9	také
rozpočet	rozpočet	k1gInSc1	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
úspory	úspora	k1gFnSc2	úspora
financí	finance	k1gFnPc2	finance
NASA	NASA	kA	NASA
zastavila	zastavit	k5eAaPmAgFnS	zastavit
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
a	a	k8xC	a
přesunula	přesunout	k5eAaPmAgFnS	přesunout
datum	datum	k1gNnSc4	datum
startu	start	k1gInSc2	start
na	na	k7c4	na
říjen	říjen	k1gInSc4	říjen
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
bylo	být	k5eAaImAgNnS	být
definitivně	definitivně	k6eAd1	definitivně
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
přidáním	přidání	k1gNnSc7	přidání
odrazové	odrazový	k2eAgFnSc2d1	odrazová
vrstvy	vrstva	k1gFnSc2	vrstva
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
silné	silný	k2eAgFnSc2d1	silná
75	[number]	k4	75
nm	nm	k?	nm
<g/>
,	,	kIx,	,
a	a	k8xC	a
ochranného	ochranný	k2eAgInSc2d1	ochranný
nátěru	nátěr	k1gInSc2	nátěr
z	z	k7c2	z
fluoridu	fluorid	k1gInSc2	fluorid
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
,	,	kIx,	,
silného	silný	k2eAgNnSc2d1	silné
25	[number]	k4	25
nm	nm	k?	nm
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zvětšil	zvětšit	k5eAaPmAgInS	zvětšit
odrazivost	odrazivost	k1gFnSc4	odrazivost
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
v	v	k7c6	v
ultrafialovém	ultrafialový	k2eAgNnSc6d1	ultrafialové
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
schopnostech	schopnost	k1gFnPc6	schopnost
společnosti	společnost	k1gFnSc2	společnost
Perkin	Perkin	k2eAgInSc1d1	Perkin
Elmer	Elmer	k1gInSc1	Elmer
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
takto	takto	k6eAd1	takto
náročném	náročný	k2eAgInSc6d1	náročný
projektu	projekt	k1gInSc6	projekt
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
odložila	odložit	k5eAaPmAgFnS	odložit
datum	datum	k1gNnSc4	datum
startu	start	k1gInSc2	start
až	až	k6eAd1	až
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Skluz	skluz	k1gInSc1	skluz
v	v	k7c6	v
plánech	plán	k1gInPc6	plán
narůstal	narůstat	k5eAaImAgMnS	narůstat
asi	asi	k9	asi
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
za	za	k7c4	za
čtvrt	čtvrt	k1gFnSc4	čtvrt
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
byla	být	k5eAaImAgFnS	být
nucená	nucený	k2eAgFnSc1d1	nucená
odložit	odložit	k5eAaPmF	odložit
datum	datum	k1gNnSc4	datum
startu	start	k1gInSc2	start
až	až	k6eAd1	až
do	do	k7c2	do
podzimu	podzim	k1gInSc2	podzim
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
okamžiku	okamžik	k1gInSc6	okamžik
již	již	k6eAd1	již
celkový	celkový	k2eAgInSc1d1	celkový
rozpočet	rozpočet	k1gInSc1	rozpočet
projektu	projekt	k1gInSc2	projekt
činil	činit	k5eAaImAgInS	činit
1,175	[number]	k4	1,175
miliardy	miliarda	k4xCgFnSc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Obal	obal	k1gInSc1	obal
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
zabudovaný	zabudovaný	k2eAgInSc1d1	zabudovaný
dalekohled	dalekohled	k1gInSc1	dalekohled
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
přístroji	přístroj	k1gInPc7	přístroj
<g/>
,	,	kIx,	,
představoval	představovat	k5eAaImAgMnS	představovat
pro	pro	k7c4	pro
inženýry	inženýr	k1gMnPc4	inženýr
další	další	k2eAgMnPc4d1	další
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
složitý	složitý	k2eAgInSc4d1	složitý
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odolával	odolávat	k5eAaImAgMnS	odolávat
častým	častý	k2eAgFnPc3d1	častá
změnám	změna	k1gFnPc3	změna
teploty	teplota	k1gFnSc2	teplota
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
ze	z	k7c2	z
zemského	zemský	k2eAgInSc2d1	zemský
stínu	stín	k1gInSc2	stín
na	na	k7c4	na
přímé	přímý	k2eAgNnSc4d1	přímé
sluneční	sluneční	k2eAgNnSc4d1	sluneční
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
aby	aby	kYmCp3nS	aby
jeho	jeho	k3xOp3gFnSc1	jeho
stabilita	stabilita	k1gFnSc1	stabilita
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
mimořádně	mimořádně	k6eAd1	mimořádně
přesné	přesný	k2eAgNnSc4d1	přesné
zaměření	zaměření	k1gNnSc4	zaměření
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Vícevrstvý	vícevrstvý	k2eAgInSc1d1	vícevrstvý
izolační	izolační	k2eAgInSc1d1	izolační
plášť	plášť	k1gInSc1	plášť
udržuje	udržovat	k5eAaImIp3nS	udržovat
konstantní	konstantní	k2eAgFnSc4d1	konstantní
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
teplotu	teplota	k1gFnSc4	teplota
celého	celý	k2eAgNnSc2d1	celé
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
lehkou	lehký	k2eAgFnSc4d1	lehká
hliníkovou	hliníkový	k2eAgFnSc4d1	hliníková
schránku	schránka	k1gFnSc4	schránka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
dalekohled	dalekohled	k1gInSc1	dalekohled
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
přístroje	přístroj	k1gInPc1	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
schránky	schránka	k1gFnSc2	schránka
drží	držet	k5eAaImIp3nS	držet
grafito-epoxidová	grafitopoxidový	k2eAgFnSc1d1	grafito-epoxidový
kostra	kostra	k1gFnSc1	kostra
nejdůležitější	důležitý	k2eAgFnSc2d3	nejdůležitější
části	část	k1gFnSc2	část
celého	celý	k2eAgInSc2d1	celý
komplexu	komplex	k1gInSc2	komplex
pevně	pevně	k6eAd1	pevně
usazené	usazený	k2eAgFnPc1d1	usazená
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
konstrukce	konstrukce	k1gFnSc1	konstrukce
obalu	obal	k1gInSc2	obal
postupovala	postupovat	k5eAaImAgFnS	postupovat
o	o	k7c6	o
poznání	poznání	k1gNnSc6	poznání
hladčeji	hladko	k6eAd2	hladko
než	než	k8xS	než
konstrukce	konstrukce	k1gFnSc1	konstrukce
optické	optický	k2eAgFnSc2d1	optická
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
Lockheed	Lockheed	k1gInSc1	Lockheed
byla	být	k5eAaImAgFnS	být
neustále	neustále	k6eAd1	neustále
v	v	k7c6	v
časovém	časový	k2eAgInSc6d1	časový
skluzu	skluz	k1gInSc6	skluz
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
navyšováním	navyšování	k1gNnSc7	navyšování
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1985	[number]	k4	1985
překročila	překročit	k5eAaPmAgFnS	překročit
konstrukce	konstrukce	k1gFnSc1	konstrukce
obalu	obal	k1gInSc2	obal
rozpočet	rozpočet	k1gInSc4	rozpočet
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
a	a	k8xC	a
časový	časový	k2eAgInSc1d1	časový
plán	plán	k1gInSc1	plán
byl	být	k5eAaImAgInS	být
překročen	překročit	k5eAaPmNgInS	překročit
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Marshallova	Marshallův	k2eAgNnSc2d1	Marshallovo
střediska	středisko	k1gNnSc2	středisko
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
letů	let	k1gInPc2	let
hovořila	hovořit	k5eAaImAgFnS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
Lockheed	Lockheed	k1gInSc1	Lockheed
se	se	k3xPyFc4	se
raději	rád	k6eAd2	rád
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
na	na	k7c4	na
příkazy	příkaz	k1gInPc4	příkaz
od	od	k7c2	od
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
převzala	převzít	k5eAaPmAgFnS	převzít
iniciativu	iniciativa	k1gFnSc4	iniciativa
do	do	k7c2	do
vlastních	vlastní	k2eAgFnPc2d1	vlastní
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
HST	HST	kA	HST
nesl	nést	k5eAaImAgInS	nést
při	při	k7c6	při
startu	start	k1gInSc6	start
pět	pět	k4xCc4	pět
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
:	:	kIx,	:
kombinovanou	kombinovaný	k2eAgFnSc4d1	kombinovaná
širokoúhlou	širokoúhlý	k2eAgFnSc4d1	širokoúhlá
a	a	k8xC	a
planetární	planetární	k2eAgFnSc4d1	planetární
kameru	kamera	k1gFnSc4	kamera
WF	WF	kA	WF
<g/>
/	/	kIx~	/
<g/>
PC	PC	kA	PC
(	(	kIx(	(
<g/>
WF	WF	kA	WF
<g/>
/	/	kIx~	/
<g/>
PC	PC	kA	PC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spektrometr	spektrometr	k1gInSc1	spektrometr
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
(	(	kIx(	(
<g/>
GHRS	GHRS	kA	GHRS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vysokorychlostní	vysokorychlostní	k2eAgInSc1d1	vysokorychlostní
fotometr	fotometr	k1gInSc1	fotometr
(	(	kIx(	(
<g/>
HSP	HSP	kA	HSP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kameru	kamera	k1gFnSc4	kamera
pro	pro	k7c4	pro
záznam	záznam	k1gInSc4	záznam
velmi	velmi	k6eAd1	velmi
slabých	slabý	k2eAgInPc2d1	slabý
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
FOC	FOC	kA	FOC
<g/>
)	)	kIx)	)
a	a	k8xC	a
spektrograf	spektrograf	k1gInSc1	spektrograf
slabých	slabý	k2eAgInPc2d1	slabý
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
FOS	fosa	k1gFnPc2	fosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
WF	WF	kA	WF
<g/>
/	/	kIx~	/
<g/>
PC	PC	kA	PC
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k8xC	jako
dvě	dva	k4xCgNnPc4	dva
kamerová	kamerový	k2eAgNnPc4d1	kamerové
zařízení	zařízení	k1gNnPc4	zařízení
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
primárně	primárně	k6eAd1	primárně
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
ve	v	k7c6	v
viditelné	viditelný	k2eAgFnSc6d1	viditelná
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
pracovat	pracovat	k5eAaImF	pracovat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
režimech	režim	k1gInPc6	režim
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
prvním	první	k4xOgMnSc6	první
mělo	mít	k5eAaImAgNnS	mít
jeho	jeho	k3xOp3gFnPc4	jeho
zorné	zorný	k2eAgFnPc4d1	zorná
pole	pole	k1gFnPc4	pole
velikost	velikost	k1gFnSc4	velikost
2,7	[number]	k4	2,7
×	×	k?	×
2,7	[number]	k4	2,7
obloukových	obloukový	k2eAgFnPc2d1	oblouková
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhém	druhý	k4xOgMnSc6	druhý
pak	pak	k6eAd1	pak
1,2	[number]	k4	1,2
×	×	k?	×
1,2	[number]	k4	1,2
obloukových	obloukový	k2eAgFnPc2d1	oblouková
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
režim	režim	k1gInSc1	režim
sloužil	sloužit	k5eAaImAgInS	sloužit
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
vzdálenějších	vzdálený	k2eAgInPc2d2	vzdálenější
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
pro	pro	k7c4	pro
sledování	sledování	k1gNnSc4	sledování
bližších	blízký	k2eAgNnPc2d2	bližší
těles	těleso	k1gNnPc2	těleso
(	(	kIx(	(
<g/>
např.	např.	kA	např.
planet	planeta	k1gFnPc2	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc4	zařízení
zkonstruovala	zkonstruovat	k5eAaPmAgFnS	zkonstruovat
Laboratoř	laboratoř	k1gFnSc1	laboratoř
proudového	proudový	k2eAgInSc2d1	proudový
pohonu	pohon	k1gInSc2	pohon
(	(	kIx(	(
<g/>
Jet	jet	k2eAgInSc1d1	jet
Propulsion	Propulsion	k1gInSc1	Propulsion
Laboratory	Laborator	k1gInPc1	Laborator
<g/>
)	)	kIx)	)
při	při	k7c6	při
NASA	NASA	kA	NASA
a	a	k8xC	a
zakomponovala	zakomponovat	k5eAaPmAgFnS	zakomponovat
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
sadu	sad	k1gInSc2	sad
48	[number]	k4	48
filtrů	filtr	k1gInPc2	filtr
izolujících	izolující	k2eAgInPc2d1	izolující
spektrální	spektrální	k2eAgInPc4d1	spektrální
čáry	čár	k1gInPc4	čár
se	s	k7c7	s
speciálním	speciální	k2eAgInSc7d1	speciální
astrofyzikálním	astrofyzikální	k2eAgInSc7d1	astrofyzikální
významem	význam	k1gInSc7	význam
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
8	[number]	k4	8
CCD	CCD	kA	CCD
čipů	čip	k1gInPc2	čip
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
kamer	kamera	k1gFnPc2	kamera
využívala	využívat	k5eAaPmAgFnS	využívat
přesně	přesně	k6eAd1	přesně
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
Kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
širokoúhlá	širokoúhlý	k2eAgFnSc1d1	širokoúhlá
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
WFC	WFC	kA	WFC
<g/>
)	)	kIx)	)
pokrývala	pokrývat	k5eAaImAgFnS	pokrývat
úhlově	úhlově	k6eAd1	úhlově
větší	veliký	k2eAgNnSc4d2	veliký
pole	pole	k1gNnSc4	pole
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
nižšího	nízký	k2eAgNnSc2d2	nižší
rozlišení	rozlišení	k1gNnSc2	rozlišení
<g/>
,	,	kIx,	,
planetární	planetární	k2eAgFnSc1d1	planetární
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
PC	PC	kA	PC
<g/>
)	)	kIx)	)
zachycovala	zachycovat	k5eAaImAgFnS	zachycovat
snímky	snímek	k1gInPc4	snímek
pomocí	pomocí	k7c2	pomocí
větší	veliký	k2eAgFnSc2d2	veliký
ohniskové	ohniskový	k2eAgFnSc2d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
než	než	k8xS	než
čipy	čip	k1gInPc4	čip
u	u	k7c2	u
WFC	WFC	kA	WFC
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
většího	veliký	k2eAgNnSc2d2	veliký
zvětšení	zvětšení	k1gNnSc2	zvětšení
<g/>
.	.	kIx.	.
</s>
<s>
GHRS	GHRS	kA	GHRS
byl	být	k5eAaImAgInS	být
spektrograf	spektrograf	k1gInSc4	spektrograf
určený	určený	k2eAgInSc4d1	určený
na	na	k7c6	na
snímání	snímání	k1gNnSc6	snímání
objektů	objekt	k1gInPc2	objekt
v	v	k7c6	v
ultrafialové	ultrafialový	k2eAgFnSc6d1	ultrafialová
části	část	k1gFnSc6	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
v	v	k7c6	v
Goddardově	Goddardův	k2eAgNnSc6d1	Goddardovo
centru	centrum	k1gNnSc6	centrum
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
schopné	schopný	k2eAgNnSc1d1	schopné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
spektrálního	spektrální	k2eAgNnSc2d1	spektrální
rozlišení	rozlišení	k1gNnSc2	rozlišení
90	[number]	k4	90
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snímkování	snímkování	k1gNnSc4	snímkování
v	v	k7c6	v
ultrafialové	ultrafialový	k2eAgFnSc6d1	ultrafialová
oblasti	oblast	k1gFnSc6	oblast
byla	být	k5eAaImAgFnS	být
optimalizována	optimalizován	k2eAgFnSc1d1	optimalizována
i	i	k8xC	i
zařízení	zařízení	k1gNnSc1	zařízení
FOC	FOC	kA	FOC
a	a	k8xC	a
FOS	fosa	k1gFnPc2	fosa
<g/>
;	;	kIx,	;
obě	dva	k4xCgFnPc1	dva
měla	mít	k5eAaImAgFnS	mít
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
prostorové	prostorový	k2eAgNnSc4d1	prostorové
rozlišení	rozlišení	k1gNnSc4	rozlišení
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
zařízení	zařízení	k1gNnPc2	zařízení
na	na	k7c6	na
dalekohledu	dalekohled	k1gInSc6	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
FOC	FOC	kA	FOC
bylo	být	k5eAaImAgNnS	být
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
trojstupňovým	trojstupňový	k2eAgInSc7d1	trojstupňový
fotonásobičem	fotonásobič	k1gInSc7	fotonásobič
a	a	k8xC	a
sloužilo	sloužit	k5eAaImAgNnS	sloužit
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
velmi	velmi	k6eAd1	velmi
slabých	slabý	k2eAgInPc2d1	slabý
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
FOS	fosa	k1gFnPc2	fosa
byl	být	k5eAaImAgInS	být
spektrograf	spektrograf	k1gInSc1	spektrograf
a	a	k8xC	a
studoval	studovat	k5eAaImAgInS	studovat
optická	optický	k2eAgNnPc4d1	optické
spektra	spektrum	k1gNnPc4	spektrum
velmi	velmi	k6eAd1	velmi
vzdálených	vzdálený	k2eAgInPc2d1	vzdálený
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
detektory	detektor	k1gInPc1	detektor
nevyužívaly	využívat	k5eNaImAgInP	využívat
CCD	CCD	kA	CCD
čipy	čip	k1gInPc1	čip
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
digikony	digikon	k1gInPc1	digikon
na	na	k7c4	na
počítání	počítání	k1gNnSc4	počítání
fotonů	foton	k1gInPc2	foton
<g/>
.	.	kIx.	.
</s>
<s>
FOC	FOC	kA	FOC
zkonstruovala	zkonstruovat	k5eAaPmAgFnS	zkonstruovat
Evropská	evropský	k2eAgFnSc1d1	Evropská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
a	a	k8xC	a
FOS	fosa	k1gFnPc2	fosa
postavila	postavit	k5eAaPmAgFnS	postavit
společnost	společnost	k1gFnSc1	společnost
Martin	Martin	k1gMnSc1	Martin
Marietta	Marietta	k1gMnSc1	Marietta
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
pod	pod	k7c7	pod
společností	společnost	k1gFnSc7	společnost
Lockheed	Lockheed	k1gMnSc1	Lockheed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
HSP	HSP	kA	HSP
byl	být	k5eAaImAgInS	být
navržený	navržený	k2eAgInSc4d1	navržený
a	a	k8xC	a
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
na	na	k7c6	na
Wisconsinsko-Madisonské	Wisconsinsko-Madisonský	k2eAgFnSc6d1	Wisconsinsko-Madisonský
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
pozorovat	pozorovat	k5eAaImF	pozorovat
proměnné	proměnný	k2eAgFnPc4d1	proměnná
hvězdy	hvězda	k1gFnPc4	hvězda
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc4d1	ostatní
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mění	měnit	k5eAaImIp3nP	měnit
svoji	svůj	k3xOyFgFnSc4	svůj
jasnost	jasnost	k1gFnSc4	jasnost
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
ultrafialovém	ultrafialový	k2eAgNnSc6d1	ultrafialové
a	a	k8xC	a
viditelném	viditelný	k2eAgNnSc6d1	viditelné
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Měřil	měřit	k5eAaImAgMnS	měřit
rychle	rychle	k6eAd1	rychle
změny	změna	k1gFnSc2	změna
světelného	světelný	k2eAgInSc2d1	světelný
toku	tok	k1gInSc2	tok
a	a	k8xC	a
polarizaci	polarizace	k1gFnSc4	polarizace
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
provést	provést	k5eAaPmF	provést
naráz	naráz	k6eAd1	naráz
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
000	[number]	k4	000
měření	měření	k1gNnPc2	měření
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
s	s	k7c7	s
fotometrickou	fotometrický	k2eAgFnSc7d1	fotometrická
přesností	přesnost	k1gFnSc7	přesnost
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
%	%	kIx~	%
nebo	nebo	k8xC	nebo
i	i	k9	i
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
senzory	senzor	k1gInPc1	senzor
naváděcího	naváděcí	k2eAgInSc2d1	naváděcí
systému	systém	k1gInSc2	systém
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
(	(	kIx(	(
<g/>
Fine	Fin	k1gMnSc5	Fin
Guidance	Guidance	k1gFnPc1	Guidance
Sensors	Sensors	k1gInSc1	Sensors
–	–	k?	–
FGS	FGS	kA	FGS
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
primárně	primárně	k6eAd1	primárně
určené	určený	k2eAgFnPc1d1	určená
na	na	k7c4	na
udržení	udržení	k1gNnSc4	udržení
přesného	přesný	k2eAgNnSc2d1	přesné
zaměření	zaměření	k1gNnSc2	zaměření
teleskopu	teleskop	k1gInSc2	teleskop
během	během	k7c2	během
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
využít	využít	k5eAaPmF	využít
i	i	k9	i
k	k	k7c3	k
mimořádně	mimořádně	k6eAd1	mimořádně
přesné	přesný	k2eAgFnSc3d1	přesná
fotometrii	fotometrie	k1gFnSc3	fotometrie
<g/>
;	;	kIx,	;
přístroj	přístroj	k1gInSc1	přístroj
dokáže	dokázat	k5eAaPmIp3nS	dokázat
měřit	měřit	k5eAaImF	měřit
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
na	na	k7c4	na
0,000	[number]	k4	0,000
3	[number]	k4	3
úhlové	úhlový	k2eAgFnSc2d1	úhlová
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vědecké	vědecký	k2eAgNnSc4d1	vědecké
řízení	řízení	k1gNnSc4	řízení
dalekohledu	dalekohled	k1gInSc2	dalekohled
a	a	k8xC	a
předávání	předávání	k1gNnSc2	předávání
získaných	získaný	k2eAgNnPc2d1	získané
dat	datum	k1gNnPc2	datum
astronomům	astronom	k1gMnPc3	astronom
je	být	k5eAaImIp3nS	být
odpovědný	odpovědný	k2eAgInSc1d1	odpovědný
Vědecký	vědecký	k2eAgInSc1d1	vědecký
institut	institut	k1gInSc1	institut
Vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
(	(	kIx(	(
<g/>
STScI	STScI	k1gFnSc1	STScI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
po	po	k7c6	po
roztržkách	roztržka	k1gFnPc6	roztržka
mezi	mezi	k7c7	mezi
NASA	NASA	kA	NASA
a	a	k8xC	a
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
komunitou	komunita	k1gFnSc7	komunita
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
NASA	NASA	kA	NASA
snažila	snažit	k5eAaImAgFnS	snažit
udržet	udržet	k5eAaPmF	udržet
kontrolu	kontrola	k1gFnSc4	kontrola
činnosti	činnost	k1gFnSc2	činnost
dalekohledu	dalekohled	k1gInSc2	dalekohled
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svých	svůj	k3xOyFgFnPc2	svůj
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vědecká	vědecký	k2eAgFnSc1d1	vědecká
obec	obec	k1gFnSc1	obec
ji	on	k3xPp3gFnSc4	on
chtěla	chtít	k5eAaImAgFnS	chtít
vykonávat	vykonávat	k5eAaImF	vykonávat
na	na	k7c6	na
akademické	akademický	k2eAgFnSc6d1	akademická
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
STScI	STScI	k?	STScI
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Univerzity	univerzita	k1gFnSc2	univerzita
Johna	John	k1gMnSc2	John
Hopkinse	Hopkins	k1gMnSc2	Hopkins
v	v	k7c6	v
Baltimoru	Baltimore	k1gInSc6	Baltimore
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Maryland	Marylanda	k1gFnPc2	Marylanda
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
ho	on	k3xPp3gMnSc4	on
Asociace	asociace	k1gFnSc1	asociace
univerzit	univerzita	k1gFnPc2	univerzita
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
(	(	kIx(	(
<g/>
AURA	aura	k1gFnSc1	aura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
34	[number]	k4	34
americkými	americký	k2eAgFnPc7d1	americká
institucemi	instituce	k1gFnPc7	instituce
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
univerzitami	univerzita	k1gFnPc7	univerzita
<g/>
)	)	kIx)	)
a	a	k8xC	a
sedmi	sedm	k4xCc7	sedm
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
pobočkami	pobočka	k1gFnPc7	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
Evropským	evropský	k2eAgMnPc3d1	evropský
astronomům	astronom	k1gMnPc3	astronom
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
podobnou	podobný	k2eAgFnSc4d1	podobná
službu	služba	k1gFnSc4	služba
Space	Space	k1gFnSc2	Space
Telescope	Telescop	k1gInSc5	Telescop
<g/>
−	−	k?	−
<g/>
European	European	k1gInSc1	European
Coordinating	Coordinating	k1gInSc1	Coordinating
Facility	Facilita	k1gFnSc2	Facilita
(	(	kIx(	(
<g/>
ST	St	kA	St
<g/>
–	–	k?	–
<g/>
ECF	ECF	kA	ECF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
Garchingu	Garching	k1gInSc6	Garching
u	u	k7c2	u
Mnichova	Mnichov	k1gInSc2	Mnichov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
STScI	STScI	k?	STScI
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
plánování	plánování	k1gNnSc2	plánování
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
dalekohled	dalekohled	k1gInSc1	dalekohled
obíhá	obíhat	k5eAaImIp3nS	obíhat
Zemi	zem	k1gFnSc4	zem
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
výhoda	výhoda	k1gFnSc1	výhoda
pro	pro	k7c4	pro
servisní	servisní	k2eAgFnPc4d1	servisní
mise	mise	k1gFnPc4	mise
raketoplánů	raketoplán	k1gInPc2	raketoplán
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
současně	současně	k6eAd1	současně
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
množství	množství	k1gNnSc4	množství
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
objektů	objekt	k1gInPc2	objekt
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
oběžné	oběžný	k2eAgFnSc2d1	oběžná
doby	doba	k1gFnSc2	doba
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
úrovni	úroveň	k1gFnSc3	úroveň
radiace	radiace	k1gFnSc2	radiace
také	také	k6eAd1	také
pozorování	pozorování	k1gNnSc2	pozorování
nemohou	moct	k5eNaImIp3nP	moct
probíhat	probíhat	k5eAaImF	probíhat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
teleskop	teleskop	k1gInSc1	teleskop
přelétá	přelétat	k5eAaImIp3nS	přelétat
nad	nad	k7c7	nad
tzv.	tzv.	kA	tzv.
Jihoatlantickou	Jihoatlantický	k2eAgFnSc7d1	Jihoatlantický
anomálií	anomálie	k1gFnSc7	anomálie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
největšího	veliký	k2eAgNnSc2d3	veliký
přiblížení	přiblížení	k1gNnSc2	přiblížení
tzv.	tzv.	kA	tzv.
van	vana	k1gFnPc2	vana
Allenových	Allenová	k1gFnPc2	Allenová
radiačních	radiační	k2eAgInPc2d1	radiační
pásů	pás	k1gInPc2	pás
k	k	k7c3	k
zemskému	zemský	k2eAgInSc3d1	zemský
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
také	také	k9	také
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
namířen	namířit	k5eAaPmNgInS	namířit
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
optické	optický	k2eAgFnSc2d1	optická
soustavy	soustava	k1gFnSc2	soustava
(	(	kIx(	(
<g/>
OTA	Ota	k1gMnSc1	Ota
<g/>
)	)	kIx)	)
gama	gama	k1gNnSc7	gama
zářením	záření	k1gNnSc7	záření
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mj.	mj.	kA	mj.
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
pozorování	pozorování	k1gNnSc1	pozorování
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
kolem	kolem	k7c2	kolem
50	[number]	k4	50
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
naváděcím	naváděcí	k2eAgInPc3d1	naváděcí
senzorům	senzor	k1gInPc3	senzor
FGS	FGS	kA	FGS
se	se	k3xPyFc4	se
dalekohled	dalekohled	k1gInSc1	dalekohled
musí	muset	k5eAaImIp3nS	muset
vyhýbat	vyhýbat	k5eAaImF	vyhýbat
i	i	k8xC	i
světlu	světlo	k1gNnSc3	světlo
odraženému	odražený	k2eAgNnSc3d1	odražené
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jejich	jejich	k3xOp3gInSc7	jejich
směrem	směr	k1gInSc7	směr
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
natočen	natočit	k5eAaBmNgInS	natočit
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
FGS	FGS	kA	FGS
vypnuté	vypnutý	k2eAgFnPc1d1	vypnutá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
dalekohledu	dalekohled	k1gInSc2	dalekohled
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
tzv.	tzv.	kA	tzv.
oblasti	oblast	k1gFnSc2	oblast
souvislého	souvislý	k2eAgNnSc2d1	souvislé
pozorování	pozorování	k1gNnSc2	pozorování
(	(	kIx(	(
<g/>
continuous	continuous	k1gInSc1	continuous
viewing	viewing	k1gInSc1	viewing
zone	zone	k1gFnSc1	zone
–	–	k?	–
CVZ	CVZ	kA	CVZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velké	velký	k2eAgInPc1d1	velký
asi	asi	k9	asi
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vesmírné	vesmírný	k2eAgInPc1d1	vesmírný
objekty	objekt	k1gInPc1	objekt
pozorovat	pozorovat	k5eAaImF	pozorovat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
precesi	precese	k1gFnSc3	precese
dráhy	dráha	k1gFnSc2	dráha
se	se	k3xPyFc4	se
poloha	poloha	k1gFnSc1	poloha
CVZ	CVZ	kA	CVZ
pomalu	pomalu	k6eAd1	pomalu
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
periodicitou	periodicita	k1gFnSc7	periodicita
8	[number]	k4	8
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pozorování	pozorování	k1gNnPc2	pozorování
v	v	k7c6	v
CVZ	CVZ	kA	CVZ
může	moct	k5eAaImIp3nS	moct
světelné	světelný	k2eAgNnSc1d1	světelné
záření	záření	k1gNnSc1	záření
odrážené	odrážený	k2eAgNnSc1d1	odrážené
Zemí	zem	k1gFnSc7	zem
po	po	k7c4	po
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
dosahovat	dosahovat	k5eAaImF	dosahovat
značně	značně	k6eAd1	značně
velké	velký	k2eAgFnPc4d1	velká
intenzity	intenzita	k1gFnPc4	intenzita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
limbus	limbus	k1gInSc1	limbus
planety	planeta	k1gFnSc2	planeta
zabírá	zabírat	k5eAaImIp3nS	zabírat
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
oblastech	oblast	k1gFnPc6	oblast
CVZ	CVZ	kA	CVZ
až	až	k9	až
30	[number]	k4	30
<g/>
°	°	k?	°
zorného	zorný	k2eAgNnSc2d1	zorné
pole	pole	k1gNnSc2	pole
teleskopu	teleskop	k1gInSc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
až	až	k9	až
do	do	k7c2	do
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
vrstev	vrstva	k1gFnPc2	vrstva
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
oběhu	oběh	k1gInSc6	oběh
okolo	okolo	k7c2	okolo
Země	zem	k1gFnSc2	zem
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
přesně	přesně	k6eAd1	přesně
předvídatelné	předvídatelný	k2eAgNnSc1d1	předvídatelné
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
těchto	tento	k3xDgFnPc2	tento
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgInPc6d1	různý
faktorech	faktor	k1gInPc6	faktor
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
odchylka	odchylka	k1gFnSc1	odchylka
při	při	k7c6	při
předpovědích	předpověď	k1gFnPc6	předpověď
dráhy	dráha	k1gFnSc2	dráha
dalekohledu	dalekohled	k1gInSc2	dalekohled
na	na	k7c4	na
nejbližších	blízký	k2eAgNnPc2d3	nejbližší
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
může	moct	k5eAaImIp3nS	moct
narůst	narůst	k5eAaPmF	narůst
až	až	k9	až
na	na	k7c4	na
4	[number]	k4	4
000	[number]	k4	000
km	km	kA	km
v	v	k7c6	v
horizontálním	horizontální	k2eAgInSc6d1	horizontální
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
pozorování	pozorování	k1gNnSc2	pozorování
se	se	k3xPyFc4	se
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
vypracovává	vypracovávat	k5eAaImIp3nS	vypracovávat
jen	jen	k9	jen
na	na	k7c4	na
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
dopředu	dopředu	k6eAd1	dopředu
<g/>
.	.	kIx.	.
</s>
<s>
Technickou	technický	k2eAgFnSc4d1	technická
údržbu	údržba	k1gFnSc4	údržba
dalekohledu	dalekohled	k1gInSc2	dalekohled
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
NASA	NASA	kA	NASA
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Goddardovým	Goddardův	k2eAgNnSc7d1	Goddardovo
střediskem	středisko	k1gNnSc7	středisko
pro	pro	k7c4	pro
vesmírné	vesmírný	k2eAgInPc4d1	vesmírný
lety	let	k1gInPc4	let
sídlícím	sídlící	k2eAgInSc7d1	sídlící
v	v	k7c6	v
Greenbelte	Greenbelte	k1gFnSc6	Greenbelte
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Maryland	Marylanda	k1gFnPc2	Marylanda
<g/>
,	,	kIx,	,
48	[number]	k4	48
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
STScI	STScI	k1gFnSc2	STScI
<g/>
.	.	kIx.	.
</s>
<s>
Správnou	správný	k2eAgFnSc4d1	správná
dráhu	dráha	k1gFnSc4	dráha
dalekohledu	dalekohled	k1gInSc2	dalekohled
monitorují	monitorovat	k5eAaImIp3nP	monitorovat
nepřetržitě	přetržitě	k6eNd1	přetržitě
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
čtyři	čtyři	k4xCgInPc1	čtyři
týmy	tým	k1gInPc1	tým
letových	letový	k2eAgMnPc2d1	letový
kontrolorů	kontrolor	k1gMnPc2	kontrolor
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnPc1d1	tvořící
spolu	spolu	k6eAd1	spolu
tzv.	tzv.	kA	tzv.
Tým	tým	k1gInSc1	tým
letové	letový	k2eAgFnSc2d1	letová
dráhy	dráha	k1gFnSc2	dráha
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
(	(	kIx(	(
<g/>
Hubble	Hubble	k1gFnSc1	Hubble
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Flight	Flight	k1gInSc1	Flight
Operations	Operations	k1gInSc1	Operations
Team	team	k1gInSc1	team
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
vypadal	vypadat	k5eAaImAgInS	vypadat
říjnový	říjnový	k2eAgInSc1d1	říjnový
start	start	k1gInSc1	start
dalekohledu	dalekohled	k1gInSc2	dalekohled
již	již	k6eAd1	již
celkem	celkem	k6eAd1	celkem
reálně	reálně	k6eAd1	reálně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
havárie	havárie	k1gFnSc1	havárie
raketoplánu	raketoplán	k1gInSc2	raketoplán
Challenger	Challenger	k1gInSc1	Challenger
zastavila	zastavit	k5eAaPmAgFnS	zastavit
americký	americký	k2eAgInSc4d1	americký
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Flotila	flotila	k1gFnSc1	flotila
zbylých	zbylý	k2eAgInPc2d1	zbylý
raketoplánů	raketoplán	k1gInPc2	raketoplán
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
hangárech	hangár	k1gInPc6	hangár
a	a	k8xC	a
start	start	k1gInSc4	start
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
posunut	posunout	k5eAaPmNgInS	posunout
<g/>
.	.	kIx.	.
</s>
<s>
Teleskop	teleskop	k1gInSc1	teleskop
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
místnosti	místnost	k1gFnSc6	místnost
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
prohlížen	prohlížet	k5eAaImNgInS	prohlížet
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
byl	být	k5eAaImAgMnS	být
čištěn	čistit	k5eAaImNgMnS	čistit
dusíkem	dusík	k1gInSc7	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
údržba	údržba	k1gFnSc1	údržba
stála	stát	k5eAaImAgFnS	stát
měsíčně	měsíčně	k6eAd1	měsíčně
asi	asi	k9	asi
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
projekt	projekt	k1gInSc4	projekt
dále	daleko	k6eAd2	daleko
prodražovalo	prodražovat	k5eAaImAgNnS	prodražovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
inženýři	inženýr	k1gMnPc1	inženýr
využili	využít	k5eAaPmAgMnP	využít
získaný	získaný	k2eAgInSc4d1	získaný
čas	čas	k1gInSc4	čas
na	na	k7c4	na
vykonání	vykonání	k1gNnSc4	vykonání
rozsáhlejších	rozsáhlý	k2eAgInPc2d2	rozsáhlejší
testů	test	k1gInPc2	test
a	a	k8xC	a
na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
vylepšení	vylepšení	k1gNnSc4	vylepšení
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
program	program	k1gInSc1	program
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
spuštěn	spustit	k5eAaPmNgInS	spustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
úspěšně	úspěšně	k6eAd1	úspěšně
vynesla	vynést	k5eAaPmAgFnS	vynést
dalekohled	dalekohled	k1gInSc4	dalekohled
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
mise	mise	k1gFnSc2	mise
STS-31	STS-31	k1gFnSc2	STS-31
pomocí	pomocí	k7c2	pomocí
raketoplánu	raketoplán	k1gInSc2	raketoplán
Discovery	Discovera	k1gFnSc2	Discovera
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původně	původně	k6eAd1	původně
odhadované	odhadovaný	k2eAgFnSc2d1	odhadovaná
sumy	suma	k1gFnSc2	suma
400	[number]	k4	400
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
stálo	stát	k5eAaImAgNnS	stát
vybudování	vybudování	k1gNnSc1	vybudování
dalekohledu	dalekohled	k1gInSc2	dalekohled
doposud	doposud	k6eAd1	doposud
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2,5	[number]	k4	2,5
miliardy	miliarda	k4xCgFnSc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
cena	cena	k1gFnSc1	cena
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
několikanásobek	několikanásobek	k1gInSc4	několikanásobek
konstrukční	konstrukční	k2eAgFnSc2d1	konstrukční
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
výdaje	výdaj	k1gInPc1	výdaj
USA	USA	kA	USA
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
4,5	[number]	k4	4,5
miliardy	miliarda	k4xCgFnSc2	miliarda
až	až	k8xS	až
6	[number]	k4	6
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Evropský	evropský	k2eAgInSc1d1	evropský
finanční	finanční	k2eAgInSc1d1	finanční
příspěvek	příspěvek	k1gInSc1	příspěvek
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
593	[number]	k4	593
milionů	milion	k4xCgInPc2	milion
euro	euro	k1gNnSc1	euro
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
po	po	k7c4	po
vypuštění	vypuštění	k1gNnSc4	vypuštění
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dalekohled	dalekohled	k1gInSc1	dalekohled
má	mít	k5eAaImIp3nS	mít
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
optiky	optika	k1gFnSc2	optika
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
první	první	k4xOgInPc1	první
snímky	snímek	k1gInPc1	snímek
pořízené	pořízený	k2eAgInPc1d1	pořízený
dalekohledem	dalekohled	k1gInSc7	dalekohled
vypadaly	vypadat	k5eAaPmAgInP	vypadat
ostřejší	ostrý	k2eAgInPc1d2	ostřejší
než	než	k8xS	než
srovnatelné	srovnatelný	k2eAgInPc1d1	srovnatelný
snímky	snímek	k1gInPc1	snímek
z	z	k7c2	z
pozemních	pozemní	k2eAgInPc2d1	pozemní
teleskopů	teleskop	k1gInPc2	teleskop
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
kvalita	kvalita	k1gFnSc1	kvalita
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
očekávání	očekávání	k1gNnSc2	očekávání
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
bodových	bodový	k2eAgInPc2d1	bodový
zdrojů	zdroj	k1gInPc2	zdroj
měly	mít	k5eAaImAgInP	mít
průměr	průměr	k1gInSc4	průměr
až	až	k8xS	až
1	[number]	k4	1
úhlovou	úhlový	k2eAgFnSc4d1	úhlová
vteřinu	vteřina	k1gFnSc4	vteřina
namísto	namísto	k7c2	namísto
zamýšlené	zamýšlený	k2eAgFnSc2d1	zamýšlená
desetiny	desetina	k1gFnSc2	desetina
úhlové	úhlový	k2eAgFnSc2d1	úhlová
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
snímků	snímek	k1gInPc2	snímek
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčinou	příčina	k1gFnSc7	příčina
problému	problém	k1gInSc2	problém
je	být	k5eAaImIp3nS	být
chybně	chybně	k6eAd1	chybně
vybroušené	vybroušený	k2eAgNnSc1d1	vybroušené
primární	primární	k2eAgNnSc1d1	primární
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejpřesněji	přesně	k6eAd3	přesně
vytvarované	vytvarovaný	k2eAgNnSc1d1	vytvarované
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
odchylka	odchylka	k1gFnSc1	odchylka
od	od	k7c2	od
ideální	ideální	k2eAgFnSc2d1	ideální
tvaru	tvar	k1gInSc6	tvar
nepřesahovala	přesahovat	k5eNaImAgFnS	přesahovat
předepsanou	předepsaný	k2eAgFnSc4d1	předepsaná
odchylku	odchylka	k1gFnSc4	odchylka
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
65	[number]	k4	65
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
krajích	kraj	k1gInPc6	kraj
příliš	příliš	k6eAd1	příliš
ploché	plochý	k2eAgNnSc1d1	ploché
a	a	k8xC	a
odchylovalo	odchylovat	k5eAaImAgNnS	odchylovat
se	se	k3xPyFc4	se
až	až	k9	až
o	o	k7c4	o
2,3	[number]	k4	2,3
mikrometru	mikrometr	k1gInSc2	mikrometr
od	od	k7c2	od
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byla	být	k5eAaImAgFnS	být
vážná	vážný	k2eAgFnSc1d1	vážná
sférická	sférický	k2eAgFnSc1d1	sférická
aberace	aberace	k1gFnSc1	aberace
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
chyba	chyba	k1gFnSc1	chyba
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
světlo	světlo	k1gNnSc1	světlo
odražené	odražený	k2eAgNnSc1d1	odražené
z	z	k7c2	z
okrajů	okraj	k1gInPc2	okraj
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
soustředí	soustředit	k5eAaPmIp3nP	soustředit
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
než	než	k8xS	než
světlo	světlo	k1gNnSc1	světlo
odražené	odražený	k2eAgNnSc1d1	odražené
od	od	k7c2	od
středu	střed	k1gInSc2	střed
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Závažnost	závažnost	k1gFnSc1	závažnost
chyby	chyba	k1gFnSc2	chyba
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
závisela	záviset	k5eAaImAgFnS	záviset
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
jasných	jasný	k2eAgInPc2d1	jasný
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
spektroskopická	spektroskopický	k2eAgNnPc4d1	spektroskopické
pozorování	pozorování	k1gNnPc4	pozorování
nebyly	být	k5eNaImAgFnP	být
chybou	chyba	k1gFnSc7	chyba
téměř	téměř	k6eAd1	téměř
vůbec	vůbec	k9	vůbec
ovlivněné	ovlivněný	k2eAgNnSc1d1	ovlivněné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
byla	být	k5eAaImAgFnS	být
vybroušená	vybroušený	k2eAgFnSc1d1	vybroušená
v	v	k7c6	v
požadovaném	požadovaný	k2eAgInSc6d1	požadovaný
tvaru	tvar	k1gInSc6	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Znatelně	znatelně	k6eAd1	znatelně
snížená	snížený	k2eAgFnSc1d1	snížená
však	však	k9	však
byla	být	k5eAaImAgFnS	být
kvalita	kvalita	k1gFnSc1	kvalita
snímků	snímek	k1gInPc2	snímek
matnějších	matný	k2eAgInPc2d2	matnější
a	a	k8xC	a
slabších	slabý	k2eAgInPc2d2	slabší
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
snímků	snímek	k1gInPc2	snímek
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
kontrastem	kontrast	k1gInSc7	kontrast
<g/>
.	.	kIx.	.
</s>
<s>
Znamenalo	znamenat	k5eAaImAgNnS	znamenat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
rozběhnout	rozběhnout	k5eAaPmF	rozběhnout
téměř	téměř	k6eAd1	téměř
žádný	žádný	k3yNgInSc4	žádný
kosmologický	kosmologický	k2eAgInSc4d1	kosmologický
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
takové	takový	k3xDgInPc1	takový
programy	program	k1gInPc1	program
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
právě	právě	k9	právě
zmiňované	zmiňovaný	k2eAgNnSc4d1	zmiňované
snímkování	snímkování	k1gNnSc4	snímkování
matných	matný	k2eAgInPc2d1	matný
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
třech	tři	k4xCgInPc6	tři
letech	léto	k1gNnPc6	léto
mise	mise	k1gFnSc2	mise
schopen	schopen	k2eAgMnSc1d1	schopen
vykonat	vykonat	k5eAaPmF	vykonat
množství	množství	k1gNnSc4	množství
produktivních	produktivní	k2eAgNnPc2d1	produktivní
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
podařilo	podařit	k5eAaPmAgNnS	podařit
vadu	vada	k1gFnSc4	vada
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
,	,	kIx,	,
astronomové	astronom	k1gMnPc1	astronom
mohli	moct	k5eAaImAgMnP	moct
optimalizovat	optimalizovat	k5eAaBmF	optimalizovat
výsledky	výsledek	k1gInPc4	výsledek
pozorování	pozorování	k1gNnSc2	pozorování
pomocí	pomocí	k7c2	pomocí
důmyslných	důmyslný	k2eAgFnPc2d1	důmyslná
technik	technika	k1gFnPc2	technika
zpracování	zpracování	k1gNnSc2	zpracování
obrázků	obrázek	k1gInPc2	obrázek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
dekonvoluce	dekonvoluce	k1gFnSc1	dekonvoluce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zmenšit	zmenšit	k5eAaPmF	zmenšit
tak	tak	k9	tak
její	její	k3xOp3gInSc4	její
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
vedená	vedený	k2eAgFnSc1d1	vedená
Lew	Lew	k1gMnSc7	Lew
Allenem	Allen	k1gMnSc7	Allen
<g/>
,	,	kIx,	,
ředitelem	ředitel	k1gMnSc7	ředitel
Laboratoří	laboratoř	k1gFnPc2	laboratoř
proudového	proudový	k2eAgInSc2d1	proudový
pohonu	pohon	k1gInSc2	pohon
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mohla	moct	k5eAaImAgFnS	moct
chyba	chyba	k1gFnSc1	chyba
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zařízení	zařízení	k1gNnSc2	zařízení
testující	testující	k2eAgInSc4d1	testující
přesný	přesný	k2eAgInSc4d1	přesný
tvar	tvar	k1gInSc4	tvar
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
nullcorrector	nullcorrector	k1gInSc1	nullcorrector
<g/>
)	)	kIx)	)
nebylo	být	k5eNaImAgNnS	být
správně	správně	k6eAd1	správně
zaostřené	zaostřený	k2eAgNnSc1d1	zaostřené
–	–	k?	–
jedna	jeden	k4xCgFnSc1	jeden
čočka	čočka	k1gFnSc1	čočka
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
s	s	k7c7	s
1,3	[number]	k4	1,3
mm	mm	kA	mm
odchylkou	odchylka	k1gFnSc7	odchylka
od	od	k7c2	od
správného	správný	k2eAgNnSc2d1	správné
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
konstatovala	konstatovat	k5eAaBmAgFnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
Perkin-Elmer	Perkin-Elmra	k1gFnPc2	Perkin-Elmra
během	během	k7c2	během
leštění	leštění	k1gNnSc2	leštění
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
sice	sice	k8xC	sice
jeho	on	k3xPp3gInSc4	on
povrch	povrch	k1gInSc4	povrch
analyzovala	analyzovat	k5eAaImAgFnS	analyzovat
ještě	ještě	k9	ještě
dvěma	dva	k4xCgFnPc7	dva
dalšími	další	k2eAgNnPc7d1	další
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
shodně	shodně	k6eAd1	shodně
naznačovala	naznačovat	k5eAaImAgFnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
má	mít	k5eAaImIp3nS	mít
sférickou	sférický	k2eAgFnSc4d1	sférická
aberacii	aberacie	k1gFnSc4	aberacie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
výsledky	výsledek	k1gInPc4	výsledek
testů	test	k1gMnPc2	test
ignorovat	ignorovat	k5eAaImF	ignorovat
<g/>
.	.	kIx.	.
</s>
<s>
Věřila	věřit	k5eAaImAgFnS	věřit
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInSc4	první
test	test	k1gInSc4	test
uskutečněný	uskutečněný	k2eAgInSc4d1	uskutečněný
primárním	primární	k2eAgInSc7d1	primární
přístrojem	přístroj	k1gInSc7	přístroj
byl	být	k5eAaImAgMnS	být
přesnější	přesný	k2eAgMnSc1d2	přesnější
než	než	k8xS	než
zbylé	zbylý	k2eAgInPc1d1	zbylý
dva	dva	k4xCgInPc1	dva
kontrolní	kontrolní	k2eAgInPc1d1	kontrolní
testy	test	k1gInPc1	test
<g/>
.	.	kIx.	.
</s>
<s>
Komise	komise	k1gFnSc1	komise
obvinila	obvinit	k5eAaPmAgFnS	obvinit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
společnost	společnost	k1gFnSc1	společnost
Perkin-Elmer	Perkin-Elmra	k1gFnPc2	Perkin-Elmra
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
NASA	NASA	kA	NASA
a	a	k8xC	a
Perkin-Elmer	Perkin-Elmra	k1gFnPc2	Perkin-Elmra
byly	být	k5eAaImAgFnP	být
během	během	k7c2	během
konstrukce	konstrukce	k1gFnSc2	konstrukce
celého	celý	k2eAgInSc2d1	celý
komplexu	komplex	k1gInSc2	komplex
dalekohledu	dalekohled	k1gInSc2	dalekohled
kvůli	kvůli	k7c3	kvůli
časovému	časový	k2eAgInSc3d1	časový
skluzu	skluz	k1gInSc3	skluz
a	a	k8xC	a
předražení	předražení	k1gNnSc4	předražení
výstavby	výstavba	k1gFnSc2	výstavba
velmi	velmi	k6eAd1	velmi
napjaté	napjatý	k2eAgNnSc1d1	napjaté
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Perkin-Elmer	Perkin-Elmra	k1gFnPc2	Perkin-Elmra
nekontrolovala	nekontrolovat	k5eAaImAgFnS	nekontrolovat
výrobu	výroba	k1gFnSc4	výroba
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
dostatečným	dostatečný	k2eAgInSc7d1	dostatečný
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
že	že	k8xS	že
tímto	tento	k3xDgInSc7	tento
úkolem	úkol	k1gInSc7	úkol
nepověřila	pověřit	k5eNaPmAgFnS	pověřit
své	svůj	k3xOyFgMnPc4	svůj
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
odborníky	odborník	k1gMnPc4	odborník
na	na	k7c4	na
optiku	optika	k1gFnSc4	optika
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
NASA	NASA	kA	NASA
si	se	k3xPyFc3	se
rovněž	rovněž	k9	rovněž
vysloužila	vysloužit	k5eAaPmAgFnS	vysloužit
kritiku	kritika	k1gFnSc4	kritika
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
na	na	k7c4	na
výsledky	výsledek	k1gInPc4	výsledek
testu	test	k1gInSc2	test
kontroly	kontrola	k1gFnSc2	kontrola
kvality	kvalita	k1gFnSc2	kvalita
jen	jen	k9	jen
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
přístroje	přístroj	k1gInSc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vyslat	vyslat	k5eAaPmF	vyslat
servisní	servisní	k2eAgFnPc4d1	servisní
techniky	technika	k1gFnPc4	technika
a	a	k8xC	a
opravit	opravit	k5eAaPmF	opravit
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
společně	společně	k6eAd1	společně
s	s	k7c7	s
techniky	technik	k1gMnPc7	technik
začali	začít	k5eAaPmAgMnP	začít
okamžitě	okamžitě	k6eAd1	okamžitě
hledat	hledat	k5eAaImF	hledat
nějaké	nějaký	k3yIgNnSc4	nějaký
možné	možný	k2eAgNnSc4d1	možné
řešení	řešení	k1gNnSc4	řešení
daného	daný	k2eAgInSc2d1	daný
problému	problém	k1gInSc2	problém
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
použít	použít	k5eAaPmF	použít
při	při	k7c6	při
první	první	k4xOgFnSc6	první
servisní	servisní	k2eAgFnSc6d1	servisní
misi	mise	k1gFnSc6	mise
<g/>
,	,	kIx,	,
naplánované	naplánovaný	k2eAgFnPc1d1	naplánovaná
na	na	k7c4	na
rok	rok	k1gInSc4	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnSc3	společnost
Kodak	Kodak	kA	Kodak
a	a	k8xC	a
Itek	Itek	k1gInSc1	Itek
měly	mít	k5eAaImAgInP	mít
každá	každý	k3xTgFnSc1	každý
vybroušené	vybroušený	k2eAgNnSc4d1	vybroušené
náhradní	náhradní	k2eAgNnSc4d1	náhradní
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
<g/>
,	,	kIx,	,
měnit	měnit	k5eAaImF	měnit
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Přenést	přenést	k5eAaPmF	přenést
celý	celý	k2eAgInSc4d1	celý
dalekohled	dalekohled	k1gInSc4	dalekohled
dočasně	dočasně	k6eAd1	dočasně
na	na	k7c4	na
opravu	oprava	k1gFnSc4	oprava
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
nákladné	nákladný	k2eAgNnSc1d1	nákladné
a	a	k8xC	a
časově	časově	k6eAd1	časově
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dalekohled	dalekohled	k1gInSc1	dalekohled
měl	mít	k5eAaImAgInS	mít
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
vybroušené	vybroušený	k2eAgFnSc2d1	vybroušená
sice	sice	k8xC	sice
do	do	k7c2	do
chybného	chybný	k2eAgNnSc2d1	chybné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesně	přesně	k6eAd1	přesně
známého	známý	k2eAgInSc2d1	známý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
vyrobit	vyrobit	k5eAaPmF	vyrobit
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
optickou	optický	k2eAgFnSc4d1	optická
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
opačným	opačný	k2eAgNnSc7d1	opačné
znaménkem	znaménko	k1gNnSc7	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
doplňkový	doplňkový	k2eAgInSc1d1	doplňkový
přístroj	přístroj	k1gInSc1	přístroj
měl	mít	k5eAaImAgInS	mít
hrát	hrát	k5eAaImF	hrát
podobnou	podobný	k2eAgFnSc4d1	podobná
úlohu	úloha	k1gFnSc4	úloha
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mají	mít	k5eAaImIp3nP	mít
brýle	brýle	k1gFnPc4	brýle
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
zjistit	zjistit	k5eAaPmF	zjistit
přesnou	přesný	k2eAgFnSc4d1	přesná
hodnotu	hodnota	k1gFnSc4	hodnota
chyby	chyba	k1gFnSc2	chyba
hlavního	hlavní	k2eAgNnSc2d1	hlavní
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Analýzou	analýza	k1gFnSc7	analýza
obrázků	obrázek	k1gInPc2	obrázek
bodových	bodový	k2eAgInPc2d1	bodový
zdrojů	zdroj	k1gInPc2	zdroj
dospěli	dochvít	k5eAaPmAgMnP	dochvít
astronomové	astronom	k1gMnPc1	astronom
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnota	hodnota	k1gFnSc1	hodnota
kónické	kónický	k2eAgFnSc2d1	kónická
konstanty	konstanta	k1gFnSc2	konstanta
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
byla	být	k5eAaImAgFnS	být
−	−	k?	−
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
−	−	k?	−
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Stejná	stejný	k2eAgFnSc1d1	stejná
hodnota	hodnota	k1gFnSc1	hodnota
vyšla	vyjít	k5eAaPmAgFnS	vyjít
i	i	k9	i
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
analýzy	analýza	k1gFnSc2	analýza
zařízení	zařízení	k1gNnSc2	zařízení
na	na	k7c4	na
leštění	leštění	k1gNnSc4	leštění
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
používala	používat	k5eAaImAgFnS	používat
společnost	společnost	k1gFnSc1	společnost
Perkin-Elmer	Perkin-Elmra	k1gFnPc2	Perkin-Elmra
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
analýzy	analýza	k1gFnSc2	analýza
interferogramů	interferogram	k1gInPc2	interferogram
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
technici	technik	k1gMnPc1	technik
získali	získat	k5eAaPmAgMnP	získat
při	při	k7c6	při
pozemních	pozemní	k2eAgFnPc6d1	pozemní
zkouškách	zkouška	k1gFnPc6	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
designu	design	k1gInSc3	design
zařízení	zařízení	k1gNnSc2	zařízení
umístěných	umístěný	k2eAgInPc2d1	umístěný
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
dalekohledu	dalekohled	k1gInSc2	dalekohled
byly	být	k5eAaImAgInP	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
korekce	korekce	k1gFnSc2	korekce
<g/>
.	.	kIx.	.
</s>
<s>
Kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
širokoúhlá	širokoúhlý	k2eAgFnSc1d1	širokoúhlá
a	a	k8xC	a
planetární	planetární	k2eAgFnSc1d1	planetární
kamera	kamera	k1gFnSc1	kamera
2	[number]	k4	2
s	s	k7c7	s
novými	nový	k2eAgNnPc7d1	nové
vyměněnými	vyměněný	k2eAgNnPc7d1	vyměněné
zrcadly	zrcadlo	k1gNnPc7	zrcadlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
směřovala	směřovat	k5eAaImAgFnS	směřovat
paprsky	paprsek	k1gInPc4	paprsek
na	na	k7c4	na
8	[number]	k4	8
navzájem	navzájem	k6eAd1	navzájem
oddělených	oddělený	k2eAgInPc2d1	oddělený
CCD	CCD	kA	CCD
čipů	čip	k1gInPc2	čip
tvořících	tvořící	k2eAgFnPc2d1	tvořící
obě	dva	k4xCgFnPc1	dva
kamery	kamera	k1gFnPc1	kamera
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
nahradit	nahradit	k5eAaPmF	nahradit
původní	původní	k2eAgFnSc4d1	původní
kombinovanou	kombinovaný	k2eAgFnSc4d1	kombinovaná
širokoúhlou	širokoúhlý	k2eAgFnSc4d1	širokoúhlá
a	a	k8xC	a
planetární	planetární	k2eAgFnSc4d1	planetární
kameru	kamera	k1gFnSc4	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Inverzní	inverzní	k2eAgFnSc1d1	inverzní
optická	optický	k2eAgFnSc1d1	optická
chyba	chyba	k1gFnSc1	chyba
zabudovaná	zabudovaný	k2eAgFnSc1d1	zabudovaná
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
čipů	čip	k1gInPc2	čip
měla	mít	k5eAaImAgNnP	mít
úplně	úplně	k6eAd1	úplně
odstranit	odstranit	k5eAaPmF	odstranit
aberaci	aberace	k1gFnSc4	aberace
hlavního	hlavní	k2eAgNnSc2d1	hlavní
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
přístrojů	přístroj	k1gInPc2	přístroj
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
podobný	podobný	k2eAgInSc1d1	podobný
zásah	zásah	k1gInSc1	zásah
možný	možný	k2eAgInSc1d1	možný
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
externí	externí	k2eAgNnSc4d1	externí
korekční	korekční	k2eAgNnSc4d1	korekční
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
na	na	k7c4	na
korekci	korekce	k1gFnSc4	korekce
aberace	aberace	k1gFnSc2	aberace
pro	pro	k7c4	pro
FOC	FOC	kA	FOC
<g/>
,	,	kIx,	,
FOS	fosa	k1gFnPc2	fosa
a	a	k8xC	a
GHRS	GHRS	kA	GHRS
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Korekční	korekční	k2eAgFnSc1d1	korekční
osová	osový	k2eAgFnSc1d1	Osová
náhrada	náhrada	k1gFnSc1	náhrada
optiky	optika	k1gFnSc2	optika
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
(	(	kIx(	(
<g/>
Corrective	Correctiv	k1gInSc5	Correctiv
Optics	Optics	k1gInSc1	Optics
Space	Spaka	k1gFnSc3	Spaka
Telescope	Telescop	k1gInSc5	Telescop
Axial	Axial	k1gInSc1	Axial
Replacement	Replacement	k1gInSc4	Replacement
–	–	k?	–
COSTAR	COSTAR	kA	COSTAR
<g/>
)	)	kIx)	)
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
umístěných	umístěný	k2eAgNnPc2d1	umístěné
v	v	k7c6	v
dráze	dráha	k1gFnSc6	dráha
světla	světlo	k1gNnSc2	světlo
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
vytvarované	vytvarovaný	k2eAgNnSc1d1	vytvarované
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odstranilo	odstranit	k5eAaPmAgNnS	odstranit
aberaci	aberace	k1gFnSc4	aberace
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
korekční	korekční	k2eAgFnSc1d1	korekční
náhrada	náhrada	k1gFnSc1	náhrada
mohla	moct	k5eAaImAgFnS	moct
vtěsnat	vtěsnat	k5eAaPmF	vtěsnat
do	do	k7c2	do
útrob	útroba	k1gFnPc2	útroba
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
astronauti	astronaut	k1gMnPc1	astronaut
při	při	k7c6	při
servisní	servisní	k2eAgFnSc6d1	servisní
misi	mise	k1gFnSc6	mise
demontovat	demontovat	k5eAaBmF	demontovat
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Vybrán	vybrán	k2eAgInSc1d1	vybrán
byl	být	k5eAaImAgInS	být
vysokorychlostní	vysokorychlostní	k2eAgInSc1d1	vysokorychlostní
fotometr	fotometr	k1gInSc1	fotometr
(	(	kIx(	(
<g/>
HSP	HSP	kA	HSP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
prvopočátku	prvopočátek	k1gInSc2	prvopočátek
konstruován	konstruovat	k5eAaImNgInS	konstruovat
tak	tak	k8xC	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
pravidelně	pravidelně	k6eAd1	pravidelně
udržován	udržovat	k5eAaImNgInS	udržovat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
problémy	problém	k1gInPc1	problém
se	s	k7c7	s
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
<g/>
,	,	kIx,	,
ukázala	ukázat	k5eAaPmAgFnS	ukázat
první	první	k4xOgFnSc1	první
servisní	servisní	k2eAgFnSc1d1	servisní
mise	mise	k1gFnSc1	mise
<g/>
,	,	kIx,	,
že	že	k8xS	že
astronauti	astronaut	k1gMnPc1	astronaut
budou	být	k5eAaImBp3nP	být
nuceni	nutit	k5eAaImNgMnP	nutit
provést	provést	k5eAaPmF	provést
sérii	série	k1gFnSc4	série
dodatečných	dodatečný	k2eAgFnPc2d1	dodatečná
úprav	úprava	k1gFnPc2	úprava
a	a	k8xC	a
instalaci	instalace	k1gFnSc4	instalace
korekční	korekční	k2eAgFnSc2d1	korekční
optiky	optika	k1gFnSc2	optika
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
astronautů	astronaut	k1gMnPc2	astronaut
vybraných	vybraný	k2eAgMnPc2d1	vybraný
pro	pro	k7c4	pro
servisní	servisní	k2eAgFnSc4d1	servisní
misi	mise	k1gFnSc4	mise
trénovalo	trénovat	k5eAaImAgNnS	trénovat
používání	používání	k1gNnSc1	používání
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
speciálního	speciální	k2eAgNnSc2d1	speciální
nářadí	nářadí	k1gNnSc2	nářadí
potřebného	potřebné	k1gNnSc2	potřebné
k	k	k7c3	k
opravě	oprava	k1gFnSc3	oprava
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
teleskopu	teleskop	k1gInSc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
STS-61	STS-61	k1gFnSc2	STS-61
(	(	kIx(	(
<g/>
raketoplánu	raketoplán	k1gInSc2	raketoplán
Endeavour	Endeavour	k1gInSc1	Endeavour
<g/>
)	)	kIx)	)
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1993	[number]	k4	1993
a	a	k8xC	a
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
instalaci	instalace	k1gFnSc4	instalace
několika	několik	k4yIc2	několik
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
různého	různý	k2eAgNnSc2d1	různé
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejpodstatnější	podstatný	k2eAgFnPc4d3	nejpodstatnější
úpravy	úprava	k1gFnPc4	úprava
patří	patřit	k5eAaImIp3nS	patřit
výměna	výměna	k1gFnSc1	výměna
vysokorychlostního	vysokorychlostní	k2eAgInSc2d1	vysokorychlostní
fotometru	fotometr	k1gInSc2	fotometr
za	za	k7c4	za
"	"	kIx"	"
<g/>
balíček	balíček	k1gInSc4	balíček
<g/>
"	"	kIx"	"
korekční	korekční	k2eAgFnPc4d1	korekční
optiky	optika	k1gFnPc4	optika
COSTAR	COSTAR	kA	COSTAR
a	a	k8xC	a
výměna	výměna	k1gFnSc1	výměna
WFPC	WFPC	kA	WFPC
(	(	kIx(	(
<g/>
Wide	Wid	k1gMnSc2	Wid
Field	Field	k1gInSc1	Field
and	and	k?	and
Planetary	Planetara	k1gFnSc2	Planetara
Camera	Camero	k1gNnSc2	Camero
<g/>
)	)	kIx)	)
za	za	k7c4	za
WFPC2	WFPC2	k1gFnSc4	WFPC2
s	s	k7c7	s
interním	interní	k2eAgInSc7d1	interní
korekčním	korekční	k2eAgInSc7d1	korekční
optickým	optický	k2eAgInSc7d1	optický
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
řídící	řídící	k2eAgFnSc2d1	řídící
elektroniky	elektronika	k1gFnSc2	elektronika
solárních	solární	k2eAgInPc2d1	solární
panelů	panel	k1gInPc2	panel
<g/>
,	,	kIx,	,
čtyř	čtyři	k4xCgNnPc2	čtyři
gyroskopů	gyroskop	k1gInPc2	gyroskop
<g/>
,	,	kIx,	,
dvou	dva	k4xCgFnPc6	dva
elektronických	elektronický	k2eAgFnPc2d1	elektronická
kontrolních	kontrolní	k2eAgFnPc2d1	kontrolní
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
magnetometrů	magnetometr	k1gInPc2	magnetometr
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byl	být	k5eAaImAgInS	být
upgradován	upgradovat	k5eAaImNgInS	upgradovat
software	software	k1gInSc1	software
palubních	palubní	k2eAgMnPc2d1	palubní
počítačů	počítač	k1gMnPc2	počítač
a	a	k8xC	a
Hubble	Hubble	k1gMnPc2	Hubble
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
posunut	posunout	k5eAaPmNgInS	posunout
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
během	během	k7c2	během
3	[number]	k4	3
let	léto	k1gNnPc2	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
přílišnému	přílišný	k2eAgInSc3d1	přílišný
poklesu	pokles	k1gInSc3	pokles
vlivem	vlivem	k7c2	vlivem
tření	tření	k1gNnSc2	tření
o	o	k7c4	o
řídké	řídký	k2eAgFnPc4d1	řídká
svrchní	svrchní	k2eAgFnPc4d1	svrchní
vrstvy	vrstva	k1gFnPc4	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1994	[number]	k4	1994
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
že	že	k8xS	že
mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
úspěšně	úspěšně	k6eAd1	úspěšně
dokončena	dokončit	k5eAaPmNgFnS	dokončit
a	a	k8xC	a
představila	představit	k5eAaPmAgFnS	představit
sérii	série	k1gFnSc4	série
mnoha	mnoho	k4c2	mnoho
ostrých	ostrý	k2eAgInPc2d1	ostrý
záběrů	záběr	k1gInPc2	záběr
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
servisní	servisní	k2eAgFnSc1d1	servisní
mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejkomplexnějších	komplexní	k2eAgFnPc2d3	nejkomplexnější
misí	mise	k1gFnPc2	mise
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc1	jaký
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
podniknuty	podniknut	k2eAgFnPc1d1	podniknuta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pěti	pět	k4xCc2	pět
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
výstupů	výstup	k1gInPc2	výstup
do	do	k7c2	do
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
prostoru	prostor	k1gInSc2	prostor
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
mnoho	mnoho	k4c1	mnoho
úprav	úprava	k1gFnPc2	úprava
i	i	k9	i
vně	vně	k7c2	vně
teleskopu	teleskop	k1gInSc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
servisní	servisní	k2eAgFnSc2d1	servisní
mise	mise	k1gFnSc2	mise
2	[number]	k4	2
Discovery	Discovera	k1gFnSc2	Discovera
(	(	kIx(	(
<g/>
STS-	STS-	k1gFnSc1	STS-
<g/>
82	[number]	k4	82
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
výměna	výměna	k1gFnSc1	výměna
zařízení	zařízení	k1gNnSc2	zařízení
GHRS	GHRS	kA	GHRS
a	a	k8xC	a
FOS	fosa	k1gFnPc2	fosa
za	za	k7c7	za
spektrometry	spektrometr	k1gInPc7	spektrometr
Space	Space	k1gFnSc2	Space
Telescope	Telescop	k1gInSc5	Telescop
Imaging	Imaging	k1gInSc1	Imaging
Spectrograph	Spectrograph	k1gMnSc1	Spectrograph
(	(	kIx(	(
<g/>
STIS	STIS	kA	STIS
<g/>
)	)	kIx)	)
a	a	k8xC	a
Near	Near	k1gMnSc1	Near
Infrared	Infrared	k1gMnSc1	Infrared
Camera	Camero	k1gNnSc2	Camero
and	and	k?	and
Multi-Object	Multi-Object	k1gMnSc1	Multi-Object
Spectrometer	Spectrometer	k1gMnSc1	Spectrometer
(	(	kIx(	(
<g/>
NICMOS	NICMOS	kA	NICMOS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
technického	technický	k2eAgInSc2d1	technický
a	a	k8xC	a
výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
páskového	páskový	k2eAgInSc2d1	páskový
záznamníku	záznamník	k1gInSc2	záznamník
za	za	k7c4	za
nový	nový	k2eAgInSc4d1	nový
elektronický	elektronický	k2eAgInSc4d1	elektronický
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
opravena	opravit	k5eAaPmNgFnS	opravit
tepelná	tepelný	k2eAgFnSc1d1	tepelná
izolace	izolace	k1gFnSc1	izolace
a	a	k8xC	a
znova	znova	k6eAd1	znova
upravena	upravit	k5eAaPmNgFnS	upravit
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
NICMOS	NICMOS	kA	NICMOS
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
chladič	chladič	k1gInSc4	chladič
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
v	v	k7c6	v
pevné	pevný	k2eAgFnSc6d1	pevná
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sloužil	sloužit	k5eAaImAgMnS	sloužit
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
tepelného	tepelný	k2eAgNnSc2d1	tepelné
záření	záření	k1gNnSc2	záření
z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
instalaci	instalace	k1gFnSc6	instalace
se	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
tepla	teplo	k1gNnSc2	teplo
neočekávaně	očekávaně	k6eNd1	očekávaně
roztáhl	roztáhnout	k5eAaPmAgInS	roztáhnout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
optickou	optický	k2eAgFnSc7d1	optická
clonou	clona	k1gFnSc7	clona
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
tepelný	tepelný	k2eAgInSc1d1	tepelný
zkrat	zkrat	k1gInSc1	zkrat
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
nadměrnému	nadměrný	k2eAgNnSc3d1	nadměrné
zahřívání	zahřívání	k1gNnSc3	zahřívání
NICMOSu	NICMOSus	k1gInSc2	NICMOSus
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
260	[number]	k4	260
K	K	kA	K
místo	místo	k7c2	místo
plánovaných	plánovaný	k2eAgInPc2d1	plánovaný
~	~	kIx~	~
<g/>
61	[number]	k4	61
K	k	k7c3	k
<g/>
)	)	kIx)	)
a	a	k8xC	a
rychlejší	rychlý	k2eAgFnSc4d2	rychlejší
sublimaci	sublimace	k1gFnSc4	sublimace
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zkrátilo	zkrátit	k5eAaPmAgNnS	zkrátit
jeho	jeho	k3xOp3gFnSc4	jeho
očekávanou	očekávaný	k2eAgFnSc4d1	očekávaná
životnost	životnost	k1gFnSc4	životnost
z	z	k7c2	z
4,5	[number]	k4	4,5
let	léto	k1gNnPc2	léto
na	na	k7c4	na
2	[number]	k4	2
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Servisní	servisní	k2eAgFnPc4d1	servisní
mise	mise	k1gFnPc4	mise
3A	[number]	k4	3A
Discovery	Discovera	k1gFnSc2	Discovera
(	(	kIx(	(
<g/>
STS-	STS-	k1gFnSc1	STS-
<g/>
103	[number]	k4	103
<g/>
)	)	kIx)	)
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
odštěpnou	odštěpný	k2eAgFnSc4d1	odštěpný
misi	mise	k1gFnSc4	mise
od	od	k7c2	od
původně	původně	k6eAd1	původně
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
servisní	servisní	k2eAgFnSc2d1	servisní
mise	mise	k1gFnSc2	mise
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
varianty	varianta	k1gFnSc2	varianta
3A	[number]	k4	3A
byl	být	k5eAaImAgInS	být
vynucen	vynucen	k2eAgInSc1d1	vynucen
nenadálým	nenadálý	k2eAgNnSc7d1	nenadálé
selháním	selhání	k1gNnSc7	selhání
tří	tři	k4xCgInPc2	tři
palubních	palubní	k2eAgInPc2d1	palubní
gyroskopů	gyroskop	k1gInPc2	gyroskop
(	(	kIx(	(
<g/>
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
gyroskop	gyroskop	k1gInSc1	gyroskop
selhal	selhat	k5eAaPmAgInS	selhat
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
mise	mise	k1gFnSc2	mise
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Hubbleovi	Hubbleus	k1gMnSc6	Hubbleus
zcela	zcela	k6eAd1	zcela
znemožnilo	znemožnit	k5eAaPmAgNnS	znemožnit
provádět	provádět	k5eAaImF	provádět
vědecká	vědecký	k2eAgNnPc4d1	vědecké
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mise	mise	k1gFnSc1	mise
3A	[number]	k4	3A
nahradila	nahradit	k5eAaPmAgFnS	nahradit
všech	všecek	k3xTgInPc2	všecek
šest	šest	k4xCc4	šest
gyroskopů	gyroskop	k1gInPc2	gyroskop
<g/>
,	,	kIx,	,
vyměnila	vyměnit	k5eAaPmAgFnS	vyměnit
sensor	sensor	k1gInSc4	sensor
pro	pro	k7c4	pro
jemnou	jemný	k2eAgFnSc4d1	jemná
navigaci	navigace	k1gFnSc4	navigace
(	(	kIx(	(
<g/>
Fine	Fin	k1gMnSc5	Fin
Guidance	Guidanec	k1gMnSc2	Guidanec
Sensor	Sensora	k1gFnPc2	Sensora
<g/>
)	)	kIx)	)
a	a	k8xC	a
počítač	počítač	k1gInSc4	počítač
<g/>
,	,	kIx,	,
instalovala	instalovat	k5eAaBmAgFnS	instalovat
Voltage	Voltage	k1gInSc4	Voltage
<g/>
/	/	kIx~	/
<g/>
temperature	temperatur	k1gMnSc5	temperatur
Improvement	Improvement	k1gMnSc1	Improvement
Kit	kit	k1gInSc1	kit
(	(	kIx(	(
<g/>
VIK	Vik	k1gMnSc1	Vik
<g/>
)	)	kIx)	)
–	–	k?	–
zařízení	zařízení	k1gNnSc2	zařízení
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
baterií	baterie	k1gFnPc2	baterie
před	před	k7c7	před
přebíjením	přebíjení	k1gNnSc7	přebíjení
–	–	k?	–
a	a	k8xC	a
opět	opět	k6eAd1	opět
vyměnila	vyměnit	k5eAaPmAgFnS	vyměnit
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
izolaci	izolace	k1gFnSc4	izolace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgInSc1d1	nový
počítač	počítač	k1gInSc1	počítač
(	(	kIx(	(
<g/>
25	[number]	k4	25
MHz	Mhz	kA	Mhz
Intel	Intel	kA	Intel
80486	[number]	k4	80486
s	s	k7c7	s
2MB	[number]	k4	2MB
RAM	RAM	kA	RAM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlediska	hledisko	k1gNnSc2	hledisko
takřka	takřka	k6eAd1	takřka
muzejním	muzejní	k2eAgInSc7d1	muzejní
exponátem	exponát	k1gInSc7	exponát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
20	[number]	k4	20
<g/>
x	x	k?	x
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
DF-	DF-	k1gFnSc2	DF-
<g/>
224	[number]	k4	224
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nový	nový	k2eAgInSc1d1	nový
<g/>
"	"	kIx"	"
počítač	počítač	k1gInSc1	počítač
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
efektivitu	efektivita	k1gFnSc4	efektivita
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
provádět	provádět	k5eAaImF	provádět
některé	některý	k3yIgFnPc4	některý
výpočetní	výpočetní	k2eAgFnPc4d1	výpočetní
operace	operace	k1gFnPc4	operace
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
Hubblea	Hubble	k1gInSc2	Hubble
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
šetří	šetřit	k5eAaImIp3nS	šetřit
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
použití	použití	k1gNnSc1	použití
nových	nový	k2eAgInPc2d1	nový
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
mise	mise	k1gFnSc2	mise
3B	[number]	k4	3B
Columbia	Columbia	k1gFnSc1	Columbia
(	(	kIx(	(
<g/>
STS-	STS-	k1gFnSc1	STS-
<g/>
109	[number]	k4	109
<g/>
)	)	kIx)	)
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2002	[number]	k4	2002
bylo	být	k5eAaImAgNnS	být
nainstalováno	nainstalován	k2eAgNnSc1d1	nainstalováno
nové	nový	k2eAgNnSc1d1	nové
zařízení	zařízení	k1gNnSc1	zařízení
–	–	k?	–
Pokročilá	pokročilý	k2eAgFnSc1d1	pokročilá
pozorovací	pozorovací	k2eAgFnSc1d1	pozorovací
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
ACS	ACS	kA	ACS
<g/>
)	)	kIx)	)
–	–	k?	–
která	který	k3yIgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
FOC	FOC	kA	FOC
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
opravě	oprava	k1gFnSc3	oprava
zařízení	zařízení	k1gNnSc2	zařízení
NICMOS	NICMOS	kA	NICMOS
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vyčerpalo	vyčerpat	k5eAaPmAgNnS	vyčerpat
zásobu	zásoba	k1gFnSc4	zásoba
chladicí	chladicí	k2eAgFnSc2d1	chladicí
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
nový	nový	k2eAgInSc1d1	nový
chladicí	chladicí	k2eAgInSc1d1	chladicí
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
teplotu	teplota	k1gFnSc4	teplota
dostatečně	dostatečně	k6eAd1	dostatečně
snížil	snížit	k5eAaPmAgInS	snížit
<g/>
,	,	kIx,	,
a	a	k8xC	a
zařízení	zařízení	k1gNnSc1	zařízení
tedy	tedy	k9	tedy
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
použitelné	použitelný	k2eAgNnSc1d1	použitelné
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
udržovaná	udržovaný	k2eAgFnSc1d1	udržovaná
teplota	teplota	k1gFnSc1	teplota
77,15	[number]	k4	77,15
K	K	kA	K
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
s	s	k7c7	s
jakou	jaký	k3yRgFnSc7	jaký
počítaly	počítat	k5eAaImAgFnP	počítat
původní	původní	k2eAgInPc4d1	původní
plány	plán	k1gInPc4	plán
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
61	[number]	k4	61
K	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zase	zase	k9	zase
mnohem	mnohem	k6eAd1	mnohem
stabilnější	stabilní	k2eAgMnSc1d2	stabilnější
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
mise	mise	k1gFnSc2	mise
3B	[number]	k4	3B
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
podruhé	podruhé	k6eAd1	podruhé
vyměněny	vyměnit	k5eAaPmNgInP	vyměnit
solární	solární	k2eAgInPc1d1	solární
panely	panel	k1gInPc1	panel
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
nových	nový	k2eAgInPc2d1	nový
panelů	panel	k1gInPc2	panel
byla	být	k5eAaImAgFnS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
panelů	panel	k1gInPc2	panel
použitých	použitý	k2eAgInPc2d1	použitý
u	u	k7c2	u
družic	družice	k1gFnPc2	družice
komunikačního	komunikační	k2eAgInSc2d1	komunikační
systému	systém	k1gInSc2	systém
Iridium	iridium	k1gNnSc1	iridium
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
velikost	velikost	k1gFnSc1	velikost
činila	činit	k5eAaImAgFnS	činit
pouhé	pouhý	k2eAgFnPc1d1	pouhá
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
původních	původní	k2eAgInPc2d1	původní
panelů	panel	k1gInPc2	panel
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ale	ale	k8xC	ale
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
více	hodně	k6eAd2	hodně
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
přírůstek	přírůstek	k1gInSc1	přírůstek
energie	energie	k1gFnSc2	energie
umožnil	umožnit	k5eAaPmAgInS	umožnit
simultánní	simultánní	k2eAgNnSc4d1	simultánní
spouštění	spouštění	k1gNnSc4	spouštění
palubních	palubní	k2eAgInPc2d1	palubní
přístrojů	přístroj	k1gInPc2	přístroj
a	a	k8xC	a
zredukoval	zredukovat	k5eAaPmAgInS	zredukovat
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
vibrací	vibrace	k1gFnSc7	vibrace
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikaly	vznikat	k5eAaImAgInP	vznikat
ve	v	k7c6	v
chvílích	chvíle	k1gFnPc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
starší	starý	k2eAgMnSc1d2	starší
a	a	k8xC	a
méně	málo	k6eAd2	málo
pevné	pevný	k2eAgInPc1d1	pevný
panely	panel	k1gInPc1	panel
dostávaly	dostávat	k5eAaImAgInP	dostávat
do	do	k7c2	do
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
velikost	velikost	k1gFnSc1	velikost
panelů	panel	k1gInPc2	panel
znamenala	znamenat	k5eAaImAgFnS	znamenat
také	také	k9	také
menší	malý	k2eAgNnSc4d2	menší
tření	tření	k1gNnSc4	tření
o	o	k7c4	o
svrchní	svrchní	k2eAgFnPc4d1	svrchní
vrstvy	vrstva	k1gFnPc4	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
vyměněna	vyměnit	k5eAaPmNgFnS	vyměnit
Hubble	Hubble	k1gFnSc1	Hubble
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Power	Power	k1gInSc1	Power
Distribution	Distribution	k1gInSc1	Distribution
Unit	Unita	k1gFnPc2	Unita
(	(	kIx(	(
<g/>
energetická	energetický	k2eAgFnSc1d1	energetická
distribuční	distribuční	k2eAgFnSc1d1	distribuční
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
provádět	provádět	k5eAaImF	provádět
kompletní	kompletní	k2eAgFnPc4d1	kompletní
restarty	restarta	k1gFnPc4	restarta
všech	všecek	k3xTgNnPc2	všecek
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Servisní	servisní	k2eAgFnSc1d1	servisní
mise	mise	k1gFnSc1	mise
4	[number]	k4	4
(	(	kIx(	(
<g/>
SM	SM	kA	SM
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
mise	mise	k1gFnSc1	mise
raketoplánu	raketoplán	k1gInSc2	raketoplán
Atlantis	Atlantis	k1gFnSc1	Atlantis
(	(	kIx(	(
<g/>
STS-	STS-	k1gFnSc1	STS-
<g/>
125	[number]	k4	125
<g/>
)	)	kIx)	)
k	k	k7c3	k
údržbě	údržba	k1gFnSc3	údržba
Hubble	Hubble	k1gFnSc2	Hubble
Space	Space	k1gFnSc2	Space
Telescopu	Telescop	k1gInSc2	Telescop
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
5	[number]	k4	5
výstupů	výstup	k1gInPc2	výstup
astronautů	astronaut	k1gMnPc2	astronaut
do	do	k7c2	do
kosmu	kosmos	k1gInSc2	kosmos
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nich	on	k3xPp3gMnPc6	on
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
výměna	výměna	k1gFnSc1	výměna
porouchané	porouchaný	k2eAgFnSc2d1	porouchaná
jednotky	jednotka	k1gFnSc2	jednotka
SI	si	k1gNnSc2	si
C	C	kA	C
<g/>
&	&	k?	&
<g/>
DH	DH	kA	DH
(	(	kIx(	(
<g/>
Science	Science	k1gFnSc1	Science
Instrument	instrument	k1gInSc1	instrument
Command	Command	k1gInSc1	Command
and	and	k?	and
Data	datum	k1gNnPc1	datum
Handling	Handling	k1gInSc1	Handling
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výměna	výměna	k1gFnSc1	výměna
obou	dva	k4xCgInPc2	dva
bloků	blok	k1gInPc2	blok
baterií	baterie	k1gFnPc2	baterie
BMU	BMU	kA	BMU
(	(	kIx(	(
<g/>
Battery	Batter	k1gInPc1	Batter
Module	modul	k1gInSc5	modul
Unit	Unita	k1gFnPc2	Unita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výměna	výměna	k1gFnSc1	výměna
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
jednotek	jednotka	k1gFnPc2	jednotka
gyroskopů	gyroskop	k1gInPc2	gyroskop
RSU	RSU	kA	RSU
(	(	kIx(	(
<g/>
Rate	Rat	k1gMnSc2	Rat
Sensor	Sensor	k1gMnSc1	Sensor
Unit	Unit	k1gMnSc1	Unit
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
jednotka	jednotka	k1gFnSc1	jednotka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
gyroskopech	gyroskop	k1gInPc6	gyroskop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
instalace	instalace	k1gFnSc1	instalace
úchopového	úchopový	k2eAgNnSc2d1	úchopový
zařízení	zařízení	k1gNnSc2	zařízení
SCM	SCM	kA	SCM
(	(	kIx(	(
<g/>
Soft	Soft	k?	Soft
Capture	Captur	k1gMnSc5	Captur
Mechanism	Mechanismo	k1gNnPc2	Mechanismo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
instalace	instalace	k1gFnSc1	instalace
nové	nový	k2eAgFnSc2d1	nová
kamery	kamera	k1gFnSc2	kamera
WFC-3	WFC-3	k1gFnSc2	WFC-3
(	(	kIx(	(
<g/>
Wide	Wid	k1gInSc2	Wid
Field	Field	k1gInSc1	Field
Camera	Camera	k1gFnSc1	Camera
3	[number]	k4	3
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
místo	místo	k7c2	místo
starého	starý	k2eAgInSc2d1	starý
přístroje	přístroj	k1gInSc2	přístroj
Wide-Field	Wide-Field	k1gMnSc1	Wide-Field
Planetary	Planetara	k1gFnSc2	Planetara
Camera	Camera	k1gFnSc1	Camera
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
instalace	instalace	k1gFnSc1	instalace
nového	nový	k2eAgInSc2d1	nový
spektrografu	spektrograf	k1gInSc2	spektrograf
COS	cos	kA	cos
(	(	kIx(	(
<g/>
Cosmic	Cosmic	k1gMnSc1	Cosmic
Origins	Originsa	k1gFnPc2	Originsa
Spectrograph	Spectrograph	k1gMnSc1	Spectrograph
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
místo	místo	k7c2	místo
už	už	k6eAd1	už
nepotřebné	potřebný	k2eNgFnSc2d1	nepotřebná
korektivní	korektivní	k2eAgFnSc2d1	korektivní
optiky	optika	k1gFnSc2	optika
COSTAR	COSTAR	kA	COSTAR
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
oprava	oprava	k1gFnSc1	oprava
kamery	kamera	k1gFnSc2	kamera
ACS	ACS	kA	ACS
(	(	kIx(	(
<g/>
Advanced	Advanced	k1gMnSc1	Advanced
Camera	Camero	k1gNnSc2	Camero
for	forum	k1gNnPc2	forum
Surveys	Surveys	k1gInSc1	Surveys
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oprava	oprava	k1gFnSc1	oprava
spektrografu	spektrograf	k1gInSc2	spektrograf
STIS	STIS	kA	STIS
(	(	kIx(	(
<g/>
Space	Space	k1gMnSc5	Space
Telescope	Telescop	k1gMnSc5	Telescop
Imaging	Imaging	k1gInSc4	Imaging
Spectrograph	Spectrograph	k1gInSc1	Spectrograph
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výměna	výměna	k1gFnSc1	výměna
navigačního	navigační	k2eAgInSc2d1	navigační
senzoru	senzor	k1gInSc2	senzor
FGS-2	FGS-2	k1gFnSc2	FGS-2
(	(	kIx(	(
<g/>
Fine	Fin	k1gMnSc5	Fin
Guidance	Guidanec	k1gInPc4	Guidanec
Sensor	Sensor	k1gInSc1	Sensor
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
instalace	instalace	k1gFnSc1	instalace
tří	tři	k4xCgInPc2	tři
panelů	panel	k1gInPc2	panel
tepelné	tepelný	k2eAgFnSc2d1	tepelná
izolace	izolace	k1gFnSc2	izolace
NOBL	NOBL	kA	NOBL
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
Outer	Outer	k1gInSc4	Outer
Blanket	blanket	k1gInSc1	blanket
Layers	Layers	k1gInSc1	Layers
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cíle	cíl	k1gInPc1	cíl
mise	mise	k1gFnSc2	mise
byly	být	k5eAaImAgInP	být
splněny	splněn	k2eAgInPc1d1	splněn
<g/>
:	:	kIx,	:
Teleskop	teleskop	k1gInSc1	teleskop
je	být	k5eAaImIp3nS	být
připraven	připraven	k2eAgInSc1d1	připraven
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
návštěvu	návštěva	k1gFnSc4	návštěva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
pošle	poslat	k5eAaPmIp3nS	poslat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
poslední	poslední	k2eAgFnSc4d1	poslední
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
cca	cca	kA	cca
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
už	už	k9	už
nebudou	být	k5eNaImBp3nP	být
potřeba	potřeba	k6eAd1	potřeba
žádná	žádný	k3yNgNnPc4	žádný
dodatečná	dodatečný	k2eAgNnPc4d1	dodatečné
korekční	korekční	k2eAgNnPc4d1	korekční
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
eliminaci	eliminace	k1gFnSc4	eliminace
sférické	sférický	k2eAgFnSc2d1	sférická
aberace	aberace	k1gFnSc2	aberace
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tuto	tento	k3xDgFnSc4	tento
korekci	korekce	k1gFnSc4	korekce
nyní	nyní	k6eAd1	nyní
už	už	k6eAd1	už
provádí	provádět	k5eAaImIp3nS	provádět
sama	sám	k3xTgFnSc1	sám
optika	optika	k1gFnSc1	optika
<g/>
:	:	kIx,	:
Druhotné	druhotný	k2eAgInPc1d1	druhotný
korekční	korekční	k2eAgInPc1d1	korekční
členy	člen	k1gInPc4	člen
z	z	k7c2	z
předešlých	předešlý	k2eAgFnPc2d1	předešlá
misí	mise	k1gFnPc2	mise
byly	být	k5eAaImAgInP	být
naopak	naopak	k6eAd1	naopak
odstraněny	odstranit	k5eAaPmNgInP	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
konečně	konečně	k6eAd1	konečně
může	moct	k5eAaImIp3nS	moct
HST	HST	kA	HST
využívat	využívat	k5eAaImF	využívat
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
funkce	funkce	k1gFnPc4	funkce
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
plnou	plný	k2eAgFnSc7d1	plná
přesností	přesnost	k1gFnSc7	přesnost
a	a	k8xC	a
kapacitou	kapacita	k1gFnSc7	kapacita
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
projektovány	projektován	k2eAgFnPc1d1	projektována
<g/>
.	.	kIx.	.
</s>
<s>
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
pomohl	pomoct	k5eAaPmAgInS	pomoct
astronomům	astronom	k1gMnPc3	astronom
rozluštit	rozluštit	k5eAaPmF	rozluštit
mnohé	mnohý	k2eAgFnPc4d1	mnohá
dlouhotrvající	dlouhotrvající	k2eAgFnPc4d1	dlouhotrvající
otázky	otázka	k1gFnPc4	otázka
a	a	k8xC	a
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
též	též	k9	též
impuls	impuls	k1gInSc1	impuls
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nových	nový	k2eAgFnPc2d1	nová
otázek	otázka	k1gFnPc2	otázka
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
prvořadé	prvořadý	k2eAgInPc4d1	prvořadý
úkoly	úkol	k1gInPc4	úkol
mise	mise	k1gFnSc1	mise
patřila	patřit	k5eAaImAgFnS	patřit
měření	měření	k1gNnSc4	měření
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
Cefeid	Cefeida	k1gFnPc2	Cefeida
<g/>
,	,	kIx,	,
typu	typ	k1gInSc2	typ
proměnných	proměnná	k1gFnPc2	proměnná
hvězd	hvězda	k1gFnPc2	hvězda
vykazujících	vykazující	k2eAgInPc2d1	vykazující
přímý	přímý	k2eAgInSc4d1	přímý
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
periodou	perioda	k1gFnSc7	perioda
proměnlivosti	proměnlivost	k1gFnSc2	proměnlivost
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
absolutní	absolutní	k2eAgFnSc7d1	absolutní
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
měření	měření	k1gNnPc1	měření
byla	být	k5eAaImAgNnP	být
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
přesnější	přesný	k2eAgInPc4d2	přesnější
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
předchozí	předchozí	k2eAgFnSc1d1	předchozí
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
výrazně	výrazně	k6eAd1	výrazně
omezit	omezit	k5eAaPmF	omezit
rozsah	rozsah	k1gInSc4	rozsah
odhadovaných	odhadovaný	k2eAgFnPc2d1	odhadovaná
hodnot	hodnota	k1gFnPc2	hodnota
Hubbleovy	Hubbleův	k2eAgFnSc2d1	Hubbleova
konstanty	konstanta	k1gFnSc2	konstanta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
určuje	určovat	k5eAaImIp3nS	určovat
rychlost	rychlost	k1gFnSc4	rychlost
rozpínání	rozpínání	k1gNnSc2	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
která	který	k3yIgFnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vypuštěním	vypuštění	k1gNnSc7	vypuštění
Hubblea	Hubble	k1gInSc2	Hubble
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
chyba	chyba	k1gFnSc1	chyba
při	při	k7c6	při
odhadování	odhadování	k1gNnSc6	odhadování
konstanty	konstanta	k1gFnSc2	konstanta
i	i	k9	i
50	[number]	k4	50
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
Cefeid	Cefeida	k1gFnPc2	Cefeida
v	v	k7c6	v
kupě	kupa	k1gFnSc6	kupa
galaxií	galaxie	k1gFnPc2	galaxie
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Panny	Panna	k1gFnSc2	Panna
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
vzdálených	vzdálený	k2eAgFnPc6d1	vzdálená
kupách	kupa	k1gFnPc6	kupa
galaxií	galaxie	k1gFnPc2	galaxie
pomocí	pomocí	k7c2	pomocí
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
snížila	snížit	k5eAaPmAgFnS	snížit
tuto	tento	k3xDgFnSc4	tento
chybu	chyba	k1gFnSc4	chyba
na	na	k7c4	na
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
teleskopu	teleskop	k1gInSc2	teleskop
zpochybnila	zpochybnit	k5eAaPmAgFnS	zpochybnit
některé	některý	k3yIgFnPc4	některý
teorie	teorie	k1gFnPc4	teorie
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
a	a	k8xC	a
budoucnosti	budoucnost	k1gFnSc3	budoucnost
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
z	z	k7c2	z
týmu	tým	k1gInSc2	tým
"	"	kIx"	"
<g/>
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
<g/>
–	–	k?	–
<g/>
z	z	k7c2	z
pro	pro	k7c4	pro
hledání	hledání	k1gNnSc4	hledání
supernov	supernova	k1gFnPc2	supernova
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
High-z	High	k1gInSc1	High-z
Supernova	supernova	k1gFnSc1	supernova
Search	Search	k1gMnSc1	Search
Team	team	k1gInSc1	team
<g/>
)	)	kIx)	)
a	a	k8xC	a
Supernova	supernova	k1gFnSc1	supernova
Cosmology	Cosmolog	k1gMnPc4	Cosmolog
Project	Projecta	k1gFnPc2	Projecta
použili	použít	k5eAaPmAgMnP	použít
dalekohled	dalekohled	k1gInSc4	dalekohled
také	také	k9	také
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnPc4	pozorování
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
supernov	supernova	k1gFnPc2	supernova
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgFnPc2	jenž
objevili	objevit	k5eAaPmAgMnP	objevit
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozpínání	rozpínání	k1gNnSc1	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
vlivem	vlivem	k7c2	vlivem
gravitace	gravitace	k1gFnSc2	gravitace
nezpomaluje	zpomalovat	k5eNaImIp3nS	zpomalovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
<g/>
.	.	kIx.	.
</s>
<s>
Zrychlování	zrychlování	k1gNnPc4	zrychlování
poté	poté	k6eAd1	poté
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
i	i	k9	i
některé	některý	k3yIgInPc1	některý
pozemské	pozemský	k2eAgInPc4d1	pozemský
teleskopy	teleskop	k1gInPc4	teleskop
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalekohledy	dalekohled	k1gInPc7	dalekohled
na	na	k7c6	na
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
hodnotu	hodnota	k1gFnSc4	hodnota
určily	určit	k5eAaPmAgFnP	určit
ještě	ještě	k9	ještě
přesněji	přesně	k6eAd2	přesně
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgInSc3	ten
však	však	k9	však
příčina	příčina	k1gFnSc1	příčina
tohoto	tento	k3xDgNnSc2	tento
zrychlování	zrychlování	k1gNnSc2	zrychlování
není	být	k5eNaImIp3nS	být
stále	stále	k6eAd1	stále
zcela	zcela	k6eAd1	zcela
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc4	snímek
a	a	k8xC	a
spektra	spektrum	k1gNnPc4	spektrum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
využita	využít	k5eAaPmNgFnS	využít
i	i	k9	i
při	při	k7c6	při
dokazování	dokazování	k1gNnSc6	dokazování
přítomnosti	přítomnost	k1gFnSc2	přítomnost
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
v	v	k7c6	v
centrech	centr	k1gInPc6	centr
blízkých	blízký	k2eAgFnPc2d1	blízká
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
v	v	k7c6	v
centrech	centr	k1gInPc6	centr
galaxií	galaxie	k1gFnPc2	galaxie
se	se	k3xPyFc4	se
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
již	již	k6eAd1	již
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
i	i	k9	i
několik	několik	k4yIc1	několik
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
teprve	teprve	k6eAd1	teprve
až	až	k9	až
výzkum	výzkum	k1gInSc1	výzkum
provedený	provedený	k2eAgInSc1d1	provedený
Hubbleovým	Hubbleův	k2eAgInSc7d1	Hubbleův
dalekohledem	dalekohled	k1gInSc7	dalekohled
naznačil	naznačit	k5eAaPmAgInS	naznačit
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
centrech	centrum	k1gNnPc6	centrum
všech	všecek	k3xTgFnPc2	všecek
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
dále	daleko	k6eAd2	daleko
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hmotnost	hmotnost	k1gFnSc1	hmotnost
těchto	tento	k3xDgFnPc2	tento
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
spjatá	spjatý	k2eAgFnSc1d1	spjatá
s	s	k7c7	s
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
HST	HST	kA	HST
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
také	také	k9	také
srážku	srážka	k1gFnSc4	srážka
komety	kometa	k1gFnSc2	kometa
Shoemaker-Levy	Shoemaker-Leva	k1gFnSc2	Shoemaker-Leva
9	[number]	k4	9
s	s	k7c7	s
planetou	planeta	k1gFnSc7	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
naštěstí	naštěstí	k6eAd1	naštěstí
pro	pro	k7c4	pro
astronomy	astronom	k1gMnPc4	astronom
<g/>
)	)	kIx)	)
udála	udát	k5eAaPmAgFnS	udát
pouhých	pouhý	k2eAgInPc2d1	pouhý
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
opravě	oprava	k1gFnSc6	oprava
optiky	optika	k1gFnSc2	optika
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
fotografie	fotografia	k1gFnPc1	fotografia
byly	být	k5eAaImAgFnP	být
ostřejší	ostrý	k2eAgInPc1d2	ostřejší
než	než	k8xS	než
jakékoliv	jakýkoliv	k3yIgInPc1	jakýkoliv
předchozí	předchozí	k2eAgInPc1d1	předchozí
snímky	snímek	k1gInPc1	snímek
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kolem	kolem	k7c2	kolem
Jupitera	Jupiter	k1gMnSc2	Jupiter
proletěla	proletět	k5eAaPmAgFnS	proletět
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
hrály	hrát	k5eAaImAgFnP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
dynamiky	dynamika	k1gFnSc2	dynamika
srážky	srážka	k1gFnSc2	srážka
komety	kometa	k1gFnSc2	kometa
s	s	k7c7	s
Jupiterem	Jupiter	k1gInSc7	Jupiter
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
událost	událost	k1gFnSc1	událost
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
astronomové	astronom	k1gMnPc1	astronom
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nastává	nastávat	k5eAaImIp3nS	nastávat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
pouze	pouze	k6eAd1	pouze
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
významné	významný	k2eAgInPc4d1	významný
objevy	objev	k1gInPc4	objev
patří	patřit	k5eAaImIp3nP	patřit
protoplanetární	protoplanetární	k2eAgInPc4d1	protoplanetární
disky	disk	k1gInPc4	disk
v	v	k7c6	v
mlhovině	mlhovina	k1gFnSc6	mlhovina
M	M	kA	M
<g/>
42	[number]	k4	42
<g/>
,	,	kIx,	,
důkazy	důkaz	k1gInPc4	důkaz
přítomnosti	přítomnost	k1gFnSc2	přítomnost
extrasolárních	extrasolární	k2eAgFnPc2d1	extrasolární
planet	planeta	k1gFnPc2	planeta
okolo	okolo	k7c2	okolo
hvězd	hvězda	k1gFnPc2	hvězda
podobných	podobný	k2eAgFnPc2d1	podobná
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
objev	objev	k1gInSc1	objev
optických	optický	k2eAgInPc2d1	optický
protějšků	protějšek	k1gInPc2	protějšek
stále	stále	k6eAd1	stále
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
objasněných	objasněná	k1gFnPc2	objasněná
gamma	gamma	k1gFnSc1	gamma
záblesků	záblesk	k1gInPc2	záblesk
<g/>
.	.	kIx.	.
</s>
<s>
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
byl	být	k5eAaImAgInS	být
využit	využit	k2eAgInSc1d1	využit
i	i	k9	i
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
objektů	objekt	k1gInPc2	objekt
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
–	–	k?	–
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
planet	planeta	k1gFnPc2	planeta
Pluta	Pluto	k1gNnSc2	Pluto
a	a	k8xC	a
Eris	Eris	k1gFnSc1	Eris
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
pro	pro	k7c4	pro
sledování	sledování	k1gNnSc4	sledování
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
planet	planeta	k1gFnPc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgNnSc2	tento
pozorování	pozorování	k1gNnSc2	pozorování
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
animace	animace	k1gFnSc1	animace
ukazující	ukazující	k2eAgFnSc2d1	ukazující
dynamické	dynamický	k2eAgFnSc2d1	dynamická
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Unikátním	unikátní	k2eAgInSc7d1	unikátní
odkazem	odkaz	k1gInSc7	odkaz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nám	my	k3xPp1nPc3	my
projekt	projekt	k1gInSc4	projekt
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
zanechal	zanechat	k5eAaPmAgMnS	zanechat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
snímky	snímek	k1gInPc1	snímek
tzv.	tzv.	kA	tzv.
Hubbleových	Hubbleův	k2eAgFnPc2d1	Hubbleova
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
a	a	k8xC	a
ultra-hlubokých	ultraluboký	k2eAgFnPc2d1	ultra-hluboký
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
snímání	snímání	k1gNnSc6	snímání
byla	být	k5eAaImAgFnS	být
využita	využít	k5eAaPmNgFnS	využít
jedinečná	jedinečný	k2eAgFnSc1d1	jedinečná
citlivost	citlivost	k1gFnSc1	citlivost
přístroje	přístroj	k1gInSc2	přístroj
na	na	k7c4	na
vlnové	vlnový	k2eAgFnPc4d1	vlnová
délky	délka	k1gFnPc4	délka
ve	v	k7c6	v
viditelném	viditelný	k2eAgInSc6d1	viditelný
oboru	obor	k1gInSc6	obor
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
tak	tak	k9	tak
velice	velice	k6eAd1	velice
detailní	detailní	k2eAgInPc4d1	detailní
snímky	snímek	k1gInPc4	snímek
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgFnPc2d1	malá
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Pece	Pec	k1gFnSc2	Pec
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgNnPc6	jenž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
galaxie	galaxie	k1gFnPc4	galaxie
vzdálené	vzdálený	k2eAgFnPc4d1	vzdálená
i	i	k9	i
několik	několik	k4yIc4	několik
miliard	miliarda	k4xCgFnPc2	miliarda
světelných	světelný	k2eAgFnPc2d1	světelná
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
astronomům	astronom	k1gMnPc3	astronom
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zobrazit	zobrazit	k5eAaPmF	zobrazit
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
světle	světlo	k1gNnSc6	světlo
takto	takto	k6eAd1	takto
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
objekty	objekt	k1gInPc1	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
dalekohled	dalekohled	k1gInSc1	dalekohled
tak	tak	k6eAd1	tak
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
pohled	pohled	k1gInSc1	pohled
do	do	k7c2	do
raného	raný	k2eAgNnSc2d1	rané
stádia	stádium	k1gNnSc2	stádium
života	život	k1gInSc2	život
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
dopad	dopad	k1gInSc1	dopad
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
na	na	k7c4	na
astronomii	astronomie	k1gFnSc4	astronomie
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
objektivních	objektivní	k2eAgNnPc2d1	objektivní
měřítek	měřítko	k1gNnPc2	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
4000	[number]	k4	4000
prací	práce	k1gFnPc2	práce
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
datech	datum	k1gNnPc6	datum
poskytnutých	poskytnutý	k2eAgFnPc2d1	poskytnutá
Hubblem	Hubbl	k1gInSc7	Hubbl
bylo	být	k5eAaImAgNnS	být
zveřejněno	zveřejnit	k5eAaPmNgNnS	zveřejnit
v	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
časopisech	časopis	k1gInPc6	časopis
a	a	k8xC	a
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
představeno	představit	k5eAaPmNgNnS	představit
a	a	k8xC	a
diskutováno	diskutovat	k5eAaImNgNnS	diskutovat
na	na	k7c6	na
astronomických	astronomický	k2eAgFnPc6d1	astronomická
konferencích	konference	k1gFnPc6	konference
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
podíváme	podívat	k5eAaPmIp1nP	podívat
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
odborné	odborný	k2eAgFnPc4d1	odborná
astronomické	astronomický	k2eAgFnPc4d1	astronomická
práce	práce	k1gFnPc4	práce
z	z	k7c2	z
období	období	k1gNnPc2	období
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
zveřejnění	zveřejnění	k1gNnSc6	zveřejnění
<g/>
,	,	kIx,	,
asi	asi	k9	asi
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
citována	citovat	k5eAaBmNgFnS	citovat
<g/>
,	,	kIx,	,
z	z	k7c2	z
prací	práce	k1gFnPc2	práce
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
údajích	údaj	k1gInPc6	údaj
z	z	k7c2	z
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
citována	citovat	k5eAaBmNgFnS	citovat
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
Hubbleových	Hubbleův	k2eAgNnPc6d1	Hubbleovo
pozorováních	pozorování	k1gNnPc6	pozorování
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
citována	citován	k2eAgFnSc1d1	citována
dvakrát	dvakrát	k6eAd1	dvakrát
tak	tak	k6eAd1	tak
často	často	k6eAd1	často
než	než	k8xS	než
jiná	jiný	k2eAgNnPc4d1	jiné
astronomická	astronomický	k2eAgNnPc4d1	astronomické
pojednání	pojednání	k1gNnPc4	pojednání
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
nejvíce	nejvíce	k6eAd1	nejvíce
citovaných	citovaný	k2eAgFnPc2d1	citovaná
prací	práce	k1gFnPc2	práce
zveřejňovaných	zveřejňovaný	k2eAgFnPc2d1	zveřejňovaná
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
10	[number]	k4	10
%	%	kIx~	%
založeno	založit	k5eAaPmNgNnS	založit
právě	právě	k9	právě
na	na	k7c6	na
datech	datum	k1gNnPc6	datum
poskytnutých	poskytnutý	k2eAgFnPc2d1	poskytnutá
Hubbleovým	Hubbleův	k2eAgInSc7d1	Hubbleův
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
měl	mít	k5eAaImAgInS	mít
HST	HST	kA	HST
velmi	velmi	k6eAd1	velmi
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
astronomii	astronomie	k1gFnSc4	astronomie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nepopiratelné	popiratelný	k2eNgNnSc1d1	nepopiratelné
<g/>
,	,	kIx,	,
že	že	k8xS	že
suma	suma	k1gFnSc1	suma
vynaložená	vynaložený	k2eAgFnSc1d1	vynaložená
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
misi	mise	k1gFnSc4	mise
byla	být	k5eAaImAgFnS	být
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
.	.	kIx.	.
</s>
<s>
Vypracovaná	vypracovaný	k2eAgFnSc1d1	vypracovaná
studie	studie	k1gFnSc1	studie
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
relativních	relativní	k2eAgInPc2d1	relativní
dopadů	dopad	k1gInPc2	dopad
různých	různý	k2eAgInPc2d1	různý
dalekohledů	dalekohled	k1gInPc2	dalekohled
na	na	k7c4	na
astronomii	astronomie	k1gFnSc4	astronomie
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
data	datum	k1gNnPc4	datum
z	z	k7c2	z
HST	HST	kA	HST
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
např.	např.	kA	např.
oproti	oproti	k7c3	oproti
4	[number]	k4	4
<g/>
m	m	kA	m
pozemnímu	pozemní	k2eAgInSc3d1	pozemní
dalekohledu	dalekohled	k1gInSc3	dalekohled
Williama	William	k1gMnSc2	William
Herschela	Herschel	k1gMnSc2	Herschel
15	[number]	k4	15
×	×	k?	×
častěji	často	k6eAd2	často
citována	citován	k2eAgFnSc1d1	citována
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
stavba	stavba	k1gFnSc1	stavba
a	a	k8xC	a
provoz	provoz	k1gInSc1	provoz
HST	HST	kA	HST
stojí	stát	k5eAaImIp3nS	stát
100	[number]	k4	100
×	×	k?	×
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Učinit	učinit	k5eAaPmF	učinit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
mezi	mezi	k7c7	mezi
investováním	investování	k1gNnSc7	investování
do	do	k7c2	do
pozemního	pozemní	k2eAgInSc2d1	pozemní
dalekohledu	dalekohled	k1gInSc2	dalekohled
versus	versus	k7c1	versus
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgNnSc7d1	velké
dilematem	dilema	k1gNnSc7	dilema
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
adaptivní	adaptivní	k2eAgFnSc2d1	adaptivní
optiky	optika	k1gFnSc2	optika
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
možnosti	možnost	k1gFnPc4	možnost
snímkování	snímkování	k1gNnSc2	snímkování
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
i	i	k8xC	i
pomocí	pomoc	k1gFnSc7	pomoc
pozemních	pozemní	k2eAgMnPc2d1	pozemní
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
kvalitě	kvalita	k1gFnSc6	kvalita
pozorování	pozorování	k1gNnSc2	pozorování
mezi	mezi	k7c7	mezi
adaptivní	adaptivní	k2eAgFnSc7d1	adaptivní
optikou	optika	k1gFnSc7	optika
a	a	k8xC	a
HST	HST	kA	HST
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
případ	případ	k1gInSc4	případ
od	od	k7c2	od
případu	případ	k1gInSc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
však	však	k9	však
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
světle	světlo	k1gNnSc6	světlo
může	moct	k5eAaImIp3nS	moct
adaptivní	adaptivní	k2eAgFnSc1d1	adaptivní
optika	optika	k1gFnSc1	optika
zaostřit	zaostřit	k5eAaPmF	zaostřit
pouze	pouze	k6eAd1	pouze
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
zorného	zorný	k2eAgNnSc2d1	zorné
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
HST	HST	kA	HST
může	moct	k5eAaImIp3nS	moct
provádět	provádět	k5eAaImF	provádět
snímkování	snímkování	k1gNnPc4	snímkování
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rozlišením	rozlišení	k1gNnSc7	rozlišení
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
úhlu	úhel	k1gInSc6	úhel
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
O	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
díl	díl	k1gInSc4	díl
pozorovacího	pozorovací	k2eAgInSc2d1	pozorovací
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
přihlásit	přihlásit	k5eAaPmF	přihlásit
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
<g/>
;	;	kIx,	;
neexistují	existovat	k5eNaImIp3nP	existovat
žádná	žádný	k3yNgNnPc1	žádný
omezení	omezení	k1gNnPc1	omezení
co	co	k9	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
národnosti	národnost	k1gFnSc2	národnost
či	či	k8xC	či
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
určité	určitý	k2eAgFnSc3d1	určitá
akademické	akademický	k2eAgFnSc3d1	akademická
instituci	instituce	k1gFnSc3	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
možnost	možnost	k1gFnSc4	možnost
pozorování	pozorování	k1gNnPc2	pozorování
pomocí	pomocí	k7c2	pomocí
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
je	být	k5eAaImIp3nS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
–	–	k?	–
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
6	[number]	k4	6
x	x	k?	x
až	až	k9	až
9	[number]	k4	9
x	x	k?	x
reálnou	reálný	k2eAgFnSc4d1	reálná
časovou	časový	k2eAgFnSc4d1	časová
kapacitu	kapacita	k1gFnSc4	kapacita
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Žádosti	žádost	k1gFnPc4	žádost
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zasílat	zasílat	k5eAaImF	zasílat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
předem	předem	k6eAd1	předem
vypsaném	vypsaný	k2eAgNnSc6d1	vypsané
období	období	k1gNnSc6	období
jednou	jednou	k6eAd1	jednou
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
vítězné	vítězný	k2eAgInPc4d1	vítězný
návrhy	návrh	k1gInPc4	návrh
jsou	být	k5eAaImIp3nP	být
poté	poté	k6eAd1	poté
zapracovány	zapracovat	k5eAaPmNgFnP	zapracovat
do	do	k7c2	do
jednoletého	jednoletý	k2eAgInSc2d1	jednoletý
pozorovacího	pozorovací	k2eAgInSc2d1	pozorovací
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Návrhy	návrh	k1gInPc1	návrh
jsou	být	k5eAaImIp3nP	být
děleny	dělit	k5eAaImNgInP	dělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
Všeobecná	všeobecný	k2eAgNnPc1d1	všeobecné
pozorování	pozorování	k1gNnPc1	pozorování
(	(	kIx(	(
<g/>
General	General	k1gFnSc1	General
observer	observer	k1gMnSc1	observer
<g/>
)	)	kIx)	)
–	–	k?	–
ta	ten	k3xDgFnSc1	ten
jsou	být	k5eAaImIp3nP	být
nejčastější	častý	k2eAgFnPc1d3	nejčastější
a	a	k8xC	a
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
rutinní	rutinní	k2eAgNnSc4d1	rutinní
pozorování	pozorování	k1gNnSc4	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Snímkovací	snímkovací	k2eAgNnSc1d1	snímkovací
pozorování	pozorování	k1gNnSc1	pozorování
(	(	kIx(	(
<g/>
Snapshot	Snapshot	k1gInSc1	Snapshot
observations	observations	k1gInSc1	observations
<g/>
)	)	kIx)	)
–	–	k?	–
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
krátká	krátký	k2eAgNnPc1d1	krátké
pozorování	pozorování	k1gNnPc1	pozorování
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nepřesahují	přesahovat	k5eNaImIp3nP	přesahovat
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
celkového	celkový	k2eAgInSc2d1	celkový
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Snímkovací	snímkovací	k2eAgNnSc1d1	snímkovací
pozorování	pozorování	k1gNnSc1	pozorování
obvykle	obvykle	k6eAd1	obvykle
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zaplnění	zaplnění	k1gNnSc3	zaplnění
časových	časový	k2eAgFnPc2d1	časová
mezer	mezera	k1gFnPc2	mezera
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
nevyužity	využít	k5eNaPmNgFnP	využít
mezi	mezi	k7c7	mezi
různými	různý	k2eAgNnPc7d1	různé
všeobecnými	všeobecný	k2eAgNnPc7d1	všeobecné
pozorováními	pozorování	k1gNnPc7	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
ucházet	ucházet	k5eAaImF	ucházet
i	i	k9	i
o	o	k7c4	o
pozorovací	pozorovací	k2eAgFnSc4d1	pozorovací
možnost	možnost	k1gFnSc4	možnost
zvanou	zvaný	k2eAgFnSc4d1	zvaná
"	"	kIx"	"
<g/>
Cíl	cíl	k1gInSc1	cíl
příležitosti	příležitost	k1gFnSc2	příležitost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Target	Target	k1gInSc1	Target
of	of	k?	of
Opportunity	Opportunita	k1gFnSc2	Opportunita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
dalekohled	dalekohled	k1gInSc1	dalekohled
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
chvíli	chvíle	k1gFnSc6	chvíle
během	během	k7c2	během
řádné	řádný	k2eAgFnSc2d1	řádná
mise	mise	k1gFnSc2	mise
náhodou	náhoda	k1gFnSc7	náhoda
"	"	kIx"	"
<g/>
podívá	podívat	k5eAaImIp3nS	podívat
<g/>
"	"	kIx"	"
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
Cílem	cíl	k1gInSc7	cíl
příležitosti	příležitost	k1gFnSc2	příležitost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
daná	daný	k2eAgFnSc1d1	daná
chvíle	chvíle	k1gFnSc1	chvíle
využita	využít	k5eAaPmNgFnS	využít
ke	k	k7c3	k
splnění	splnění	k1gNnSc3	splnění
tohoto	tento	k3xDgInSc2	tento
požadavku	požadavek	k1gInSc2	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
na	na	k7c6	na
základě	základ	k1gInSc6	základ
žádosti	žádost	k1gFnSc2	žádost
využít	využít	k5eAaPmF	využít
i	i	k9	i
tzv.	tzv.	kA	tzv.
Ředitelský	ředitelský	k2eAgInSc4d1	ředitelský
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
Director	Director	k1gInSc1	Director
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Discretionary	Discretionar	k1gMnPc7	Discretionar
(	(	kIx(	(
<g/>
DD	DD	kA	DD
<g/>
)	)	kIx)	)
Time	Tim	k1gMnSc2	Tim
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
možnost	možnost	k1gFnSc4	možnost
se	se	k3xPyFc4	se
astronomové	astronom	k1gMnPc1	astronom
mohou	moct	k5eAaImIp3nP	moct
ucházet	ucházet	k5eAaImF	ucházet
kdykoliv	kdykoliv	k6eAd1	kdykoliv
během	během	k7c2	během
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Přednost	přednost	k1gFnSc4	přednost
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
takové	takový	k3xDgInPc4	takový
návrhy	návrh	k1gInPc4	návrh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
pozorování	pozorování	k1gNnSc4	pozorování
nějaké	nějaký	k3yIgFnSc2	nějaký
výjimečné	výjimečný	k2eAgFnSc2d1	výjimečná
události	událost	k1gFnSc2	událost
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
výbuch	výbuch	k1gInSc4	výbuch
supernovy	supernova	k1gFnSc2	supernova
<g/>
.	.	kIx.	.
</s>
<s>
Času	čas	k1gInSc3	čas
DD	DD	kA	DD
bylo	být	k5eAaImAgNnS	být
využito	využít	k5eAaPmNgNnS	využít
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
tzv.	tzv.	kA	tzv.
Hubbleových	Hubbleův	k2eAgFnPc2d1	Hubbleova
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
polí	pole	k1gFnPc2	pole
a	a	k8xC	a
Hubbleových	Hubbleův	k2eAgFnPc2d1	Hubbleova
ultrahlubokých	ultrahluboký	k2eAgFnPc2d1	ultrahluboký
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgInPc2	první
čtyř	čtyři	k4xCgInPc2	čtyři
cyklů	cyklus	k1gInPc2	cyklus
dostali	dostat	k5eAaPmAgMnP	dostat
možnost	možnost	k1gFnSc4	možnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
času	čas	k1gInSc2	čas
DD	DD	kA	DD
i	i	k9	i
amatérští	amatérský	k2eAgMnPc1d1	amatérský
astronomové	astronom	k1gMnPc1	astronom
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
ředitel	ředitel	k1gMnSc1	ředitel
STScI	STScI	k1gMnSc1	STScI
Riccardo	Riccardo	k1gNnSc4	Riccardo
Giacconi	Giaccoň	k1gFnSc6	Giaccoň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přenechá	přenechat	k5eAaPmIp3nS	přenechat
část	část	k1gFnSc1	část
času	čas	k1gInSc2	čas
DD	DD	kA	DD
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
amatérským	amatérský	k2eAgMnPc3d1	amatérský
astronomům	astronom	k1gMnPc3	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čas	čas	k1gInSc1	čas
sice	sice	k8xC	sice
představoval	představovat	k5eAaImAgInS	představovat
pouhých	pouhý	k2eAgFnPc2d1	pouhá
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
za	za	k7c4	za
celý	celý	k2eAgInSc4d1	celý
cyklus	cyklus	k1gInSc4	cyklus
<g/>
,	,	kIx,	,
i	i	k8xC	i
přesto	přesto	k8xC	přesto
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
amatérských	amatérský	k2eAgMnPc2d1	amatérský
astronomů	astronom	k1gMnPc2	astronom
obrovský	obrovský	k2eAgInSc4d1	obrovský
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
pozorování	pozorování	k1gNnSc4	pozorování
byly	být	k5eAaImAgFnP	být
přísně	přísně	k6eAd1	přísně
revidovány	revidovat	k5eAaImNgFnP	revidovat
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
komisí	komise	k1gFnSc7	komise
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
amatérských	amatérský	k2eAgMnPc2d1	amatérský
astronomů	astronom	k1gMnPc2	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Schváleny	schválen	k2eAgFnPc1d1	schválena
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
takové	takový	k3xDgInPc4	takový
návrhy	návrh	k1gInPc4	návrh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
vědecky	vědecky	k6eAd1	vědecky
přínosné	přínosný	k2eAgNnSc1d1	přínosné
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
neduplikovaly	duplikovat	k5eNaBmAgFnP	duplikovat
s	s	k7c7	s
požadavky	požadavek	k1gInPc7	požadavek
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
které	který	k3yRgInPc1	který
šly	jít	k5eAaImAgInP	jít
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
pouze	pouze	k6eAd1	pouze
díky	díky	k7c3	díky
jedinečným	jedinečný	k2eAgFnPc3d1	jedinečná
vlastnostem	vlastnost	k1gFnPc3	vlastnost
HST	HST	kA	HST
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
byl	být	k5eAaImAgInS	být
pozorovací	pozorovací	k2eAgInSc1d1	pozorovací
čas	čas	k1gInSc1	čas
přidělen	přidělen	k2eAgInSc1d1	přidělen
13	[number]	k4	13
amatérům	amatér	k1gMnPc3	amatér
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
pozorování	pozorování	k1gNnSc1	pozorování
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc4	projekt
s	s	k7c7	s
názvem	název	k1gInSc7	název
Transition	Transition	k1gInSc1	Transition
Comets	Comets	k1gInSc4	Comets
–	–	k?	–
UV	UV	kA	UV
Search	Search	k1gMnSc1	Search
for	forum	k1gNnPc2	forum
OH	OH	kA	OH
Emissions	Emissions	k1gInSc1	Emissions
in	in	k?	in
Asteroids	Asteroids	k1gInSc1	Asteroids
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ovšem	ovšem	k9	ovšem
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výrazným	výrazný	k2eAgInPc3d1	výrazný
škrtům	škrt	k1gInPc3	škrt
v	v	k7c6	v
rozpočtu	rozpočet	k1gInSc6	rozpočet
STScI	STScI	k1gFnSc2	STScI
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
úplné	úplný	k2eAgNnSc1d1	úplné
zastavení	zastavení	k1gNnSc1	zastavení
dalších	další	k2eAgNnPc2d1	další
amatérských	amatérský	k2eAgNnPc2d1	amatérské
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
získaná	získaný	k2eAgNnPc1d1	získané
dalekohledem	dalekohled	k1gInSc7	dalekohled
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
uložena	uložit	k5eAaPmNgFnS	uložit
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
teleskopu	teleskop	k1gInSc6	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
Hubble	Hubble	k1gMnPc3	Hubble
vypuštěn	vypuštěn	k2eAgMnSc1d1	vypuštěn
<g/>
,	,	kIx,	,
k	k	k7c3	k
uskladnění	uskladnění	k1gNnSc3	uskladnění
sloužily	sloužit	k5eAaImAgInP	sloužit
magnetické	magnetický	k2eAgInPc1d1	magnetický
pásky	pásek	k1gInPc1	pásek
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
během	během	k7c2	během
servisní	servisní	k2eAgFnSc2d1	servisní
mise	mise	k1gFnSc2	mise
2	[number]	k4	2
a	a	k8xC	a
3A	[number]	k4	3A
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgNnPc4	tento
média	médium	k1gNnPc4	médium
nahrazena	nahrazen	k2eAgNnPc4d1	nahrazeno
zařízením	zařízení	k1gNnPc3	zařízení
solid-state	solidtat	k1gInSc5	solid-stat
drive	drive	k1gInSc5	drive
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
paluby	paluba	k1gFnSc2	paluba
teleskopu	teleskop	k1gInSc2	teleskop
jsou	být	k5eAaImIp3nP	být
data	datum	k1gNnPc1	datum
odesílána	odesílán	k2eAgNnPc1d1	odesíláno
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
přes	přes	k7c4	přes
soustavu	soustava	k1gFnSc4	soustava
satelitů	satelit	k1gInPc2	satelit
pro	pro	k7c4	pro
přenášení	přenášení	k1gNnSc4	přenášení
dat	datum	k1gNnPc2	datum
TDRSS	TDRSS	kA	TDRSS
(	(	kIx(	(
<g/>
Tracking	Tracking	k1gInSc1	Tracking
and	and	k?	and
Data	datum	k1gNnSc2	datum
Relay	Relaa	k1gMnSc2	Relaa
Satellite	Satellit	k1gInSc5	Satellit
System	Systo	k1gNnSc7	Systo
<g/>
)	)	kIx)	)
–	–	k?	–
systém	systém	k1gInSc4	systém
satelitů	satelit	k1gMnPc2	satelit
na	na	k7c6	na
nízké	nízký	k2eAgFnSc6d1	nízká
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
komunikovat	komunikovat	k5eAaImF	komunikovat
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
přibližně	přibližně	k6eAd1	přibližně
85	[number]	k4	85
%	%	kIx~	%
času	čas	k1gInSc2	čas
jednoho	jeden	k4xCgInSc2	jeden
oběhu	oběh	k1gInSc2	oběh
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
TDRSS	TDRSS	kA	TDRSS
přenášena	přenášen	k2eAgFnSc1d1	přenášena
na	na	k7c4	na
pozemní	pozemní	k2eAgNnPc4d1	pozemní
střediska	středisko	k1gNnPc4	středisko
<g/>
;	;	kIx,	;
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
Goddardova	Goddardův	k2eAgNnSc2d1	Goddardovo
centra	centrum	k1gNnSc2	centrum
pro	pro	k7c4	pro
vesmírné	vesmírný	k2eAgInPc4d1	vesmírný
lety	let	k1gInPc4	let
a	a	k8xC	a
poté	poté	k6eAd1	poté
finálně	finálně	k6eAd1	finálně
do	do	k7c2	do
Vědeckého	vědecký	k2eAgInSc2d1	vědecký
institutu	institut	k1gInSc2	institut
Vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
(	(	kIx(	(
<g/>
STScl	STScl	k1gInSc1	STScl
<g/>
)	)	kIx)	)
k	k	k7c3	k
archivaci	archivace	k1gFnSc3	archivace
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
data	datum	k1gNnPc1	datum
z	z	k7c2	z
Hubblea	Hubble	k1gInSc2	Hubble
jsou	být	k5eAaImIp3nP	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
STScI	STScI	k1gFnSc2	STScI
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
podléhají	podléhat	k5eAaImIp3nP	podléhat
po	po	k7c6	po
pořízení	pořízení	k1gNnSc6	pořízení
jednoroční	jednoroční	k2eAgFnSc2d1	jednoroční
ochranné	ochranný	k2eAgFnSc2d1	ochranná
lhůtě	lhůta	k1gFnSc6	lhůta
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yQgFnSc4	který
jsou	být	k5eAaImIp3nP	být
poskytována	poskytován	k2eAgFnSc1d1	poskytována
pouze	pouze	k6eAd1	pouze
oprávněným	oprávněný	k2eAgFnPc3d1	oprávněná
osobám	osoba	k1gFnPc3	osoba
a	a	k8xC	a
astronomům	astronom	k1gMnPc3	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Oprávněné	oprávněný	k2eAgFnPc1d1	oprávněná
osoby	osoba	k1gFnPc1	osoba
z	z	k7c2	z
STScI	STScI	k1gFnSc2	STScI
mohou	moct	k5eAaImIp3nP	moct
tuto	tento	k3xDgFnSc4	tento
roční	roční	k2eAgFnSc4d1	roční
dobu	doba	k1gFnSc4	doba
zkrátit	zkrátit	k5eAaPmF	zkrátit
či	či	k8xC	či
prodloužit	prodloužit	k5eAaPmF	prodloužit
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
okolnostem	okolnost	k1gFnPc3	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
<g/>
,	,	kIx,	,
uskutečněná	uskutečněný	k2eAgFnSc1d1	uskutečněná
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Director	Director	k1gMnSc1	Director
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Discretionary	Discretionar	k1gInPc7	Discretionar
Time	Tim	k1gFnSc2	Tim
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
nařízení	nařízení	k1gNnSc2	nařízení
vyjmuta	vyjmout	k5eAaPmNgFnS	vyjmout
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
poskytnuta	poskytnout	k5eAaPmNgFnS	poskytnout
široké	široký	k2eAgFnSc3d1	široká
veřejnosti	veřejnost	k1gFnSc3	veřejnost
téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
.	.	kIx.	.
</s>
<s>
Kalibrační	kalibrační	k2eAgNnPc1d1	kalibrační
data	datum	k1gNnPc1	datum
jsou	být	k5eAaImIp3nP	být
taktéž	taktéž	k?	taktéž
přístupná	přístupný	k2eAgFnSc1d1	přístupná
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
data	datum	k1gNnPc1	datum
z	z	k7c2	z
teleskopu	teleskop	k1gInSc2	teleskop
jsou	být	k5eAaImIp3nP	být
archivována	archivován	k2eAgNnPc1d1	archivováno
v	v	k7c6	v
grafickém	grafický	k2eAgInSc6d1	grafický
formátu	formát	k1gInSc6	formát
FITS	FITS	kA	FITS
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
astronomické	astronomický	k2eAgFnPc4d1	astronomická
analýzy	analýza	k1gFnPc4	analýza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
pro	pro	k7c4	pro
veřejné	veřejný	k2eAgNnSc4d1	veřejné
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
projektu	projekt	k1gInSc3	projekt
Hubbleova	Hubbleův	k2eAgNnSc2d1	Hubbleovo
dědictví	dědictví	k1gNnSc2	dědictví
(	(	kIx(	(
<g/>
Hubble	Hubble	k1gFnSc1	Hubble
Heritage	Heritag	k1gFnSc2	Heritag
Project	Projecta	k1gFnPc2	Projecta
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vybrané	vybraný	k2eAgFnPc1d1	vybraná
fotografie	fotografia	k1gFnPc1	fotografia
zpracovávány	zpracovávat	k5eAaImNgFnP	zpracovávat
do	do	k7c2	do
formátů	formát	k1gInPc2	formát
JPEG	JPEG	kA	JPEG
a	a	k8xC	a
TIFF	TIFF	kA	TIFF
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jsou	být	k5eAaImIp3nP	být
prezentovány	prezentovat	k5eAaBmNgFnP	prezentovat
široké	široký	k2eAgFnPc1d1	široká
veřejnosti	veřejnost	k1gFnPc1	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Astronomická	astronomický	k2eAgNnPc1d1	astronomické
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
zachycená	zachycený	k2eAgFnSc1d1	zachycená
na	na	k7c6	na
CCD	CCD	kA	CCD
čipech	čip	k1gInPc6	čip
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
prodělat	prodělat	k5eAaPmF	prodělat
několik	několik	k4yIc4	několik
kalibračních	kalibrační	k2eAgInPc2d1	kalibrační
kroků	krok	k1gInPc2	krok
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
použitelná	použitelný	k2eAgFnSc1d1	použitelná
pro	pro	k7c4	pro
astronomické	astronomický	k2eAgFnPc4d1	astronomická
analýzy	analýza	k1gFnPc4	analýza
<g/>
.	.	kIx.	.
</s>
<s>
STScI	STScI	k?	STScI
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
sofistikovaný	sofistikovaný	k2eAgInSc1d1	sofistikovaný
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
požadovaná	požadovaný	k2eAgNnPc1d1	požadované
data	datum	k1gNnPc1	datum
z	z	k7c2	z
archivu	archiv	k1gInSc2	archiv
automaticky	automaticky	k6eAd1	automaticky
kalibrovat	kalibrovat	k5eAaImF	kalibrovat
pomocí	pomocí	k7c2	pomocí
nejvhodnějšího	vhodný	k2eAgInSc2d3	nejvhodnější
dostupného	dostupný	k2eAgInSc2d1	dostupný
formátu	formát	k1gInSc2	formát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
počítače	počítač	k1gInPc1	počítač
během	během	k7c2	během
výpočetního	výpočetní	k2eAgInSc2d1	výpočetní
času	čas	k1gInSc2	čas
zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
úkoly	úkol	k1gInPc7	úkol
kalibrují	kalibrovat	k5eAaImIp3nP	kalibrovat
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
velká	velká	k1gFnSc1	velká
množství	množství	k1gNnSc2	množství
žádostí	žádost	k1gFnPc2	žádost
o	o	k7c4	o
data	datum	k1gNnPc4	datum
jsou	být	k5eAaImIp3nP	být
vyřízena	vyřízen	k2eAgFnSc1d1	vyřízena
a	a	k8xC	a
vrácena	vrácen	k2eAgFnSc1d1	vrácena
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
žadateli	žadatel	k1gMnSc3	žadatel
během	během	k7c2	během
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
pozemské	pozemský	k2eAgFnPc1d1	pozemská
observatoře	observatoř	k1gFnPc1	observatoř
využívají	využívat	k5eAaPmIp3nP	využívat
proces	proces	k1gInSc4	proces
automatické	automatický	k2eAgFnSc2d1	automatická
kalibrace	kalibrace	k1gFnSc2	kalibrace
dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
pipeline	pipelin	k1gInSc5	pipelin
reduction	reduction	k1gInSc4	reduction
<g/>
)	)	kIx)	)
ve	v	k7c6	v
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
astronomové	astronom	k1gMnPc1	astronom
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
získat	získat	k5eAaPmF	získat
soubory	soubor	k1gInPc4	soubor
s	s	k7c7	s
kalibračními	kalibrační	k2eAgNnPc7d1	kalibrační
daty	datum	k1gNnPc7	datum
a	a	k8xC	a
provádět	provádět	k5eAaImF	provádět
kalibraci	kalibrace	k1gFnSc4	kalibrace
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
střediscích	středisko	k1gNnPc6	středisko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
zejména	zejména	k9	zejména
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
ke	k	k7c3	k
kalibraci	kalibrace	k1gFnSc3	kalibrace
potřeba	potřeba	k6eAd1	potřeba
jiná	jiný	k2eAgFnSc1d1	jiná
než	než	k8xS	než
automaticky	automaticky	k6eAd1	automaticky
vybraná	vybraný	k2eAgNnPc1d1	vybrané
data	datum	k1gNnPc1	datum
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
z	z	k7c2	z
teleskopu	teleskop	k1gInSc2	teleskop
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
analyzována	analyzován	k2eAgFnSc1d1	analyzována
pomocí	pomocí	k7c2	pomocí
mnoha	mnoho	k4c2	mnoho
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
STScI	STScI	k?	STScI
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
vyrobený	vyrobený	k2eAgInSc4d1	vyrobený
software	software	k1gInSc4	software
Systém	systém	k1gInSc1	systém
pro	pro	k7c4	pro
analýzu	analýza	k1gFnSc4	analýza
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
(	(	kIx(	(
<g/>
Space	Space	k1gMnSc5	Space
Telescope	Telescop	k1gMnSc5	Telescop
Science	Science	k1gFnSc1	Science
Data	datum	k1gNnSc2	datum
Analysis	Analysis	k1gFnSc2	Analysis
System	Syst	k1gInSc7	Syst
–	–	k?	–
STSDAS	STSDAS	kA	STSDAS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgInPc4	všechen
programy	program	k1gInPc4	program
potřebné	potřebný	k2eAgInPc4d1	potřebný
pro	pro	k7c4	pro
kalibraci	kalibrace	k1gFnSc4	kalibrace
nezpracovaných	zpracovaný	k2eNgNnPc2d1	nezpracované
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
další	další	k2eAgInPc4d1	další
plug-iny	plugn	k1gInPc4	plug-in
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
astronomických	astronomický	k2eAgNnPc2d1	astronomické
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
upravený	upravený	k2eAgInSc1d1	upravený
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
software	software	k1gInSc1	software
běží	běžet	k5eAaImIp3nS	běžet
jako	jako	k9	jako
modul	modul	k1gInSc1	modul
počítačového	počítačový	k2eAgInSc2d1	počítačový
programu	program	k1gInSc2	program
Image	image	k1gFnSc2	image
Reduction	Reduction	k1gInSc1	Reduction
and	and	k?	and
Analysis	Analysis	k1gFnSc2	Analysis
Facility	Facilita	k1gFnSc2	Facilita
(	(	kIx(	(
<g/>
IRAF	IRAF	kA	IRAF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
populárního	populární	k2eAgInSc2d1	populární
softwaru	software	k1gInSc2	software
pro	pro	k7c4	pro
zpracovávání	zpracovávání	k1gNnPc4	zpracovávání
astronomických	astronomický	k2eAgNnPc2d1	astronomické
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
projekt	projekt	k1gInSc4	projekt
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
důležité	důležitý	k2eAgNnSc1d1	důležité
mít	mít	k5eAaImF	mít
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
straně	strana	k1gFnSc6	strana
přízeň	přízeň	k1gFnSc4	přízeň
široké	široký	k2eAgFnSc3d1	široká
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
provoz	provoz	k1gInSc4	provoz
byly	být	k5eAaImAgInP	být
vynaloženy	vynaložen	k2eAgInPc1d1	vynaložen
značné	značný	k2eAgInPc1d1	značný
příspěvky	příspěvek	k1gInPc1	příspěvek
daňových	daňový	k2eAgMnPc2d1	daňový
poplatníků	poplatník	k1gMnPc2	poplatník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těžkých	těžký	k2eAgInPc6d1	těžký
začátcích	začátek	k1gInPc6	začátek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
veřejnosti	veřejnost	k1gFnSc2	veřejnost
kvůli	kvůli	k7c3	kvůli
chybě	chyba	k1gFnSc3	chyba
hlavního	hlavní	k2eAgNnSc2d1	hlavní
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
"	"	kIx"	"
<g/>
pošramocen	pošramocen	k2eAgInSc1d1	pošramocen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
servisní	servisní	k2eAgFnSc1d1	servisní
mise	mise	k1gFnSc1	mise
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
nová	nový	k2eAgFnSc1d1	nová
optika	optika	k1gFnSc1	optika
v	v	k7c6	v
dalekohledu	dalekohled	k1gInSc6	dalekohled
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
dodávat	dodávat	k5eAaImF	dodávat
mnoho	mnoho	k6eAd1	mnoho
mimořádných	mimořádný	k2eAgFnPc2d1	mimořádná
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
reputaci	reputace	k1gFnSc4	reputace
napravila	napravit	k5eAaPmAgFnS	napravit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
informovanost	informovanost	k1gFnSc4	informovanost
veřejnosti	veřejnost	k1gFnSc2	veřejnost
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
Hubbleova	Hubbleův	k2eAgNnSc2d1	Hubbleovo
dědictví	dědictví	k1gNnSc2	dědictví
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
lidem	člověk	k1gMnPc3	člověk
zpřístupňoval	zpřístupňovat	k5eAaImAgInS	zpřístupňovat
výběr	výběr	k1gInSc1	výběr
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
snímků	snímek	k1gInPc2	snímek
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
a	a	k8xC	a
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
projektu	projekt	k1gInSc2	projekt
je	být	k5eAaImIp3nS	být
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
amatérských	amatérský	k2eAgMnPc2d1	amatérský
i	i	k8xC	i
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
astronomů	astronom	k1gMnPc2	astronom
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
zdůrazňovat	zdůrazňovat	k5eAaImF	zdůrazňovat
estetickou	estetický	k2eAgFnSc4d1	estetická
stránku	stránka	k1gFnSc4	stránka
pořízených	pořízený	k2eAgInPc2d1	pořízený
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
vizuálně	vizuálně	k6eAd1	vizuálně
zajímavých	zajímavý	k2eAgInPc2d1	zajímavý
vesmírných	vesmírný	k2eAgInPc2d1	vesmírný
objektů	objekt	k1gInPc2	objekt
nebyly	být	k5eNaImAgInP	být
dosud	dosud	k6eAd1	dosud
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
studií	studie	k1gFnPc2	studie
pořízeny	pořízen	k2eAgInPc1d1	pořízen
snímky	snímek	k1gInPc1	snímek
v	v	k7c6	v
dostatečně	dostatečně	k6eAd1	dostatečně
širokém	široký	k2eAgNnSc6d1	široké
barevném	barevný	k2eAgNnSc6d1	barevné
spektru	spektrum	k1gNnSc6	spektrum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
daly	dát	k5eAaPmAgFnP	dát
sestavit	sestavit	k5eAaPmF	sestavit
také	také	k9	také
atraktivní	atraktivní	k2eAgFnPc4d1	atraktivní
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
má	mít	k5eAaImIp3nS	mít
projekt	projekt	k1gInSc1	projekt
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
určité	určitý	k2eAgNnSc1d1	určité
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
vlastního	vlastní	k2eAgInSc2d1	vlastní
pozorovacího	pozorovací	k2eAgInSc2d1	pozorovací
času	čas	k1gInSc2	čas
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnost	veřejnost	k1gFnSc1	veřejnost
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
výsledcích	výsledek	k1gInPc6	výsledek
HST	HST	kA	HST
informována	informovat	k5eAaBmNgFnS	informovat
také	také	k6eAd1	také
institutem	institut	k1gInSc7	institut
STScI	STScI	k1gFnSc2	STScI
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
několik	několik	k4yIc4	několik
obsáhlých	obsáhlý	k2eAgFnPc2d1	obsáhlá
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
činnosti	činnost	k1gFnPc4	činnost
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
Kancelář	kancelář	k1gFnSc1	kancelář
pro	pro	k7c4	pro
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
veřejností	veřejnost	k1gFnSc7	veřejnost
(	(	kIx(	(
<g/>
Office	Office	kA	Office
for	forum	k1gNnPc2	forum
Public	publicum	k1gNnPc2	publicum
Outreach	Outreach	k1gInSc1	Outreach
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgNnPc1d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
za	za	k7c4	za
podporu	podpora	k1gFnSc4	podpora
celého	celý	k2eAgInSc2d1	celý
programu	program	k1gInSc2	program
zajistila	zajistit	k5eAaPmAgFnS	zajistit
americkým	americký	k2eAgMnPc3d1	americký
daňovým	daňový	k2eAgMnPc3d1	daňový
poplatníkům	poplatník	k1gMnPc3	poplatník
určitou	určitý	k2eAgFnSc4d1	určitá
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
vazbu	vazba	k1gFnSc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
institucí	instituce	k1gFnSc7	instituce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
propagaci	propagace	k1gFnSc4	propagace
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
Hubbleovo	Hubbleův	k2eAgNnSc4d1	Hubbleovo
centrum	centrum	k1gNnSc4	centrum
Evropské	evropský	k2eAgFnSc2d1	Evropská
kosmické	kosmický	k2eAgFnSc2d1	kosmická
agentury	agentura	k1gFnSc2	agentura
(	(	kIx(	(
<g/>
Hubble	Hubble	k1gMnSc1	Hubble
European	European	k1gMnSc1	European
Space	Space	k1gMnSc1	Space
Agency	Agenca	k1gFnSc2	Agenca
Information	Information	k1gInSc1	Information
Centre	centr	k1gInSc5	centr
–	–	k?	–
HEIC	HEIC	kA	HEIC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kancelář	kancelář	k1gFnSc1	kancelář
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
koordinačním	koordinační	k2eAgNnSc6d1	koordinační
středisku	středisko	k1gNnSc6	středisko
Space	Space	k1gMnSc5	Space
Telescope	Telescop	k1gMnSc5	Telescop
-	-	kIx~	-
European	European	k1gInSc1	European
Coordinating	Coordinating	k1gInSc1	Coordinating
Facility	Facilita	k1gFnSc2	Facilita
(	(	kIx(	(
<g/>
ST-ECF	ST-ECF	k1gMnSc1	ST-ECF
<g/>
)	)	kIx)	)
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
činností	činnost	k1gFnSc7	činnost
kanceláře	kancelář	k1gFnSc2	kancelář
je	být	k5eAaImIp3nS	být
zveřejňování	zveřejňování	k1gNnSc1	zveřejňování
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
nejnovějších	nový	k2eAgInPc2d3	nejnovější
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
soudního	soudní	k2eAgInSc2d1	soudní
dvoru	dvůr	k1gInSc6	dvůr
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Marshfield	Marshfielda	k1gFnPc2	Marshfielda
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Missouri	Missouri	k1gNnSc2	Missouri
<g/>
,	,	kIx,	,
rodišti	rodiště	k1gNnSc3	rodiště
Edwina	Edwin	k2eAgInSc2d1	Edwin
Hubblea	Hubble	k1gInSc2	Hubble
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
replika	replika	k1gFnSc1	replika
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Minulé	minulý	k2eAgFnPc1d1	minulá
servisní	servisní	k2eAgFnPc1d1	servisní
mise	mise	k1gFnPc1	mise
nahradily	nahradit	k5eAaPmAgFnP	nahradit
staré	starý	k2eAgNnSc4d1	staré
přístrojové	přístrojový	k2eAgNnSc4d1	přístrojové
vybavení	vybavení	k1gNnSc4	vybavení
za	za	k7c4	za
nové	nový	k2eAgNnSc4d1	nové
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
nejen	nejen	k6eAd1	nejen
oddálily	oddálit	k5eAaPmAgFnP	oddálit
jeho	jeho	k3xOp3gNnSc4	jeho
selhání	selhání	k1gNnSc4	selhání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
nové	nový	k2eAgFnPc1d1	nová
možnosti	možnost	k1gFnPc1	možnost
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2004	[number]	k4	2004
selhal	selhat	k5eAaPmAgInS	selhat
pohonný	pohonný	k2eAgInSc1d1	pohonný
systém	systém	k1gInSc1	systém
spektrometru	spektrometr	k1gInSc2	spektrometr
(	(	kIx(	(
<g/>
Space	Space	k1gMnSc5	Space
Telescope	Telescop	k1gMnSc5	Telescop
Imaging	Imaging	k1gInSc1	Imaging
Spectrograph	Spectrograph	k1gInSc4	Spectrograph
–	–	k?	–
STIS	STIS	kA	STIS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
celé	celý	k2eAgNnSc1d1	celé
zařízení	zařízení	k1gNnSc1	zařízení
přestalo	přestat	k5eAaPmAgNnS	přestat
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
hlavní	hlavní	k2eAgFnSc1d1	hlavní
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
Advanced	Advanced	k1gMnSc1	Advanced
Camera	Camero	k1gNnSc2	Camero
for	forum	k1gNnPc2	forum
Surveys	Surveys	k1gInSc1	Surveys
–	–	k?	–
ACS	ACS	kA	ACS
<g/>
)	)	kIx)	)
selhala	selhat	k5eAaPmAgFnS	selhat
elektronicky	elektronicky	k6eAd1	elektronicky
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
a	a	k8xC	a
elektronika	elektronika	k1gFnSc1	elektronika
záložní	záložní	k2eAgFnSc1d1	záložní
selhala	selhat	k5eAaPmAgFnS	selhat
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pracuje	pracovat	k5eAaImIp3nS	pracovat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
původním	původní	k2eAgNnSc7d1	původní
elektronickým	elektronický	k2eAgNnSc7d1	elektronické
vybavením	vybavení	k1gNnSc7	vybavení
Solar	Solar	k1gMnSc1	Solar
Blind	Blind	k1gMnSc1	Blind
Channel	Channel	k1gMnSc1	Channel
–	–	k?	–
SBC	SBC	kA	SBC
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
kanály	kanál	k1gInPc1	kanál
<g/>
,	,	kIx,	,
ve	v	k7c6	v
viditelném	viditelný	k2eAgMnSc6d1	viditelný
a	a	k8xC	a
UV	UV	kA	UV
světle	světle	k6eAd1	světle
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgInP	zůstat
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
taktéž	taktéž	k?	taktéž
nepoužitelné	použitelný	k2eNgNnSc1d1	nepoužitelné
<g/>
.	.	kIx.	.
</s>
<s>
Zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
výzkum	výzkum	k1gInSc1	výzkum
mohl	moct	k5eAaImAgInS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
nadále	nadále	k6eAd1	nadále
bez	bez	k7c2	bez
servisní	servisní	k2eAgFnSc2d1	servisní
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
Hubble	Hubble	k6eAd1	Hubble
využívá	využívat	k5eAaImIp3nS	využívat
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
stabilizaci	stabilizace	k1gFnSc3	stabilizace
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
gyroskopy	gyroskop	k1gInPc4	gyroskop
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gMnSc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
udržovat	udržovat	k5eAaImF	udržovat
přesnou	přesný	k2eAgFnSc4d1	přesná
a	a	k8xC	a
pevnou	pevný	k2eAgFnSc4d1	pevná
polohu	poloha	k1gFnSc4	poloha
k	k	k7c3	k
zaměření	zaměření	k1gNnSc3	zaměření
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
na	na	k7c4	na
vybrané	vybraný	k2eAgInPc4d1	vybraný
astronomické	astronomický	k2eAgInPc4d1	astronomický
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
provozu	provoz	k1gInSc6	provoz
jsou	být	k5eAaImIp3nP	být
potřeba	potřeba	k6eAd1	potřeba
gyroskopy	gyroskop	k1gInPc4	gyroskop
tři	tři	k4xCgFnPc4	tři
<g/>
,	,	kIx,	,
dalekohled	dalekohled	k1gInSc1	dalekohled
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nasměrovat	nasměrovat	k5eAaPmF	nasměrovat
i	i	k9	i
za	za	k7c2	za
použití	použití	k1gNnPc2	použití
dvou	dva	k4xCgNnPc2	dva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
pozorovací	pozorovací	k2eAgInSc1d1	pozorovací
prostor	prostor	k1gInSc1	prostor
omezen	omezit	k5eAaPmNgInS	omezit
a	a	k8xC	a
observatoř	observatoř	k1gFnSc1	observatoř
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
provádět	provádět	k5eAaImF	provádět
snímkování	snímkování	k1gNnSc4	snímkování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
přesné	přesný	k2eAgNnSc4d1	přesné
zaměření	zaměření	k1gNnSc4	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
přepnout	přepnout	k5eAaPmF	přepnout
dalekohled	dalekohled	k1gInSc4	dalekohled
do	do	k7c2	do
módu	mód	k1gInSc2	mód
řízení	řízení	k1gNnSc2	řízení
pouhými	pouhý	k2eAgInPc7d1	pouhý
dvěma	dva	k4xCgInPc7	dva
gyroskopy	gyroskop	k1gInPc7	gyroskop
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
doba	doba	k1gFnSc1	doba
mise	mise	k1gFnSc2	mise
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
gyroskopy	gyroskop	k1gInPc1	gyroskop
zůstaly	zůstat	k5eAaPmAgInP	zůstat
nevyužité	využitý	k2eNgInPc4d1	nevyužitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
připravené	připravený	k2eAgFnPc1d1	připravená
jako	jako	k9	jako
záloha	záloha	k1gFnSc1	záloha
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
další	další	k2eAgInPc1d1	další
gyroskopy	gyroskop	k1gInPc1	gyroskop
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
nefunkční	funkční	k2eNgInPc1d1	nefunkční
a	a	k8xC	a
nepoužitelné	použitelný	k2eNgInPc1d1	nepoužitelný
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
Hubble	Hubble	k1gMnSc1	Hubble
zůstal	zůstat	k5eAaPmAgMnS	zůstat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
gyroskopu	gyroskop	k1gInSc6	gyroskop
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
odhadů	odhad	k1gInPc2	odhad
mohlo	moct	k5eAaImAgNnS	moct
stát	stát	k5eAaPmF	stát
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
by	by	kYmCp3nS	by
nadále	nadále	k6eAd1	nadále
nepoužitelný	použitelný	k2eNgInSc1d1	nepoužitelný
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
gyroskopů	gyroskop	k1gInPc2	gyroskop
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
Hubble	Hubble	k1gMnSc1	Hubble
vyměnit	vyměnit	k5eAaPmF	vyměnit
také	také	k9	také
baterie	baterie	k1gFnPc4	baterie
<g/>
.	.	kIx.	.
</s>
<s>
Robotická	robotický	k2eAgFnSc1d1	robotická
servisní	servisní	k2eAgFnSc1d1	servisní
mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
vyhodnocena	vyhodnotit	k5eAaPmNgFnS	vyhodnotit
jako	jako	k8xS	jako
příliš	příliš	k6eAd1	příliš
choulostivá	choulostivý	k2eAgFnSc1d1	choulostivá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
důsledku	důsledek	k1gInSc6	důsledek
nevratně	vratně	k6eNd1	vratně
poškodit	poškodit	k5eAaPmF	poškodit
celý	celý	k2eAgInSc4d1	celý
teleskop	teleskop	k1gInSc4	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
dalekohled	dalekohled	k1gInSc1	dalekohled
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
během	během	k7c2	během
servisních	servisní	k2eAgFnPc2d1	servisní
misí	mise	k1gFnPc2	mise
mohl	moct	k5eAaImAgInS	moct
získávat	získávat	k5eAaImF	získávat
energii	energie	k1gFnSc4	energie
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
považovali	považovat	k5eAaImAgMnP	považovat
využití	využití	k1gNnSc4	využití
tohoto	tento	k3xDgInSc2	tento
externího	externí	k2eAgInSc2d1	externí
zdroje	zdroj	k1gInSc2	zdroj
za	za	k7c4	za
výhodnější	výhodný	k2eAgFnSc4d2	výhodnější
než	než	k8xS	než
výměnu	výměna	k1gFnSc4	výměna
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Hubble	Hubble	k6eAd1	Hubble
obíhá	obíhat	k5eAaImIp3nS	obíhat
Zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
extrémně	extrémně	k6eAd1	extrémně
řídkých	řídký	k2eAgFnPc6d1	řídká
horních	horní	k2eAgFnPc6d1	horní
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
nerovnoměrný	rovnoměrný	k2eNgInSc1d1	nerovnoměrný
pokles	pokles	k1gInSc1	pokles
jeho	jeho	k3xOp3gFnSc2	jeho
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgInS	být
znovu	znovu	k6eAd1	znovu
naveden	navést	k5eAaPmNgInS	navést
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
by	by	kYmCp3nS	by
do	do	k7c2	do
hustších	hustý	k2eAgFnPc2d2	hustší
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2010	[number]	k4	2010
až	až	k9	až
2032	[number]	k4	2032
<g/>
.	.	kIx.	.
</s>
<s>
Přesnější	přesný	k2eAgNnSc1d2	přesnější
datum	datum	k1gNnSc1	datum
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
závisí	záviset	k5eAaImIp3nS	záviset
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
na	na	k7c6	na
sluneční	sluneční	k2eAgFnSc6d1	sluneční
aktivitě	aktivita	k1gFnSc6	aktivita
<g/>
,	,	kIx,	,
ovlivňující	ovlivňující	k2eAgFnPc4d1	ovlivňující
horní	horní	k2eAgFnPc4d1	horní
vrstvy	vrstva	k1gFnPc4	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
klesání	klesání	k1gNnSc6	klesání
teleskopu	teleskop	k1gInSc2	teleskop
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
i	i	k8xC	i
funkčnost	funkčnost	k1gFnSc4	funkčnost
Hubbleových	Hubbleův	k2eAgInPc2d1	Hubbleův
gyroskopů	gyroskop	k1gInPc2	gyroskop
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnSc1	jeho
vhodná	vhodný	k2eAgFnSc1d1	vhodná
orientace	orientace	k1gFnSc1	orientace
snižuje	snižovat	k5eAaImIp3nS	snižovat
tření	tření	k1gNnPc4	tření
o	o	k7c4	o
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
hustších	hustý	k2eAgFnPc2d2	hustší
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
by	by	kYmCp3nS	by
nejspíše	nejspíše	k9	nejspíše
nebyl	být	k5eNaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
celý	celý	k2eAgInSc1d1	celý
dalekohled	dalekohled	k1gInSc1	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
konstrukce	konstrukce	k1gFnSc2	konstrukce
a	a	k8xC	a
hlavního	hlavní	k2eAgNnSc2d1	hlavní
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
by	by	kYmCp3nP	by
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přežily	přežít	k5eAaPmAgFnP	přežít
a	a	k8xC	a
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
odhadů	odhad	k1gInPc2	odhad
existuje	existovat	k5eAaImIp3nS	existovat
při	při	k7c6	při
nekontrolovaném	kontrolovaný	k2eNgInSc6d1	nekontrolovaný
pádu	pád	k1gInSc6	pád
Hubblea	Hubble	k1gInSc2	Hubble
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
určité	určitý	k2eAgNnSc4d1	určité
riziko	riziko	k1gNnSc4	riziko
(	(	kIx(	(
<g/>
až	až	k9	až
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
700	[number]	k4	700
<g/>
)	)	kIx)	)
ztrát	ztráta	k1gFnPc2	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mise	mise	k1gFnSc1	mise
STS-125	STS-125	k1gFnSc2	STS-125
byla	být	k5eAaImAgFnS	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
vyměnit	vyměnit	k5eAaPmF	vyměnit
vadné	vadný	k2eAgInPc4d1	vadný
gyroskopy	gyroskop	k1gInPc4	gyroskop
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
přirozený	přirozený	k2eAgInSc1d1	přirozený
pokles	pokles	k1gInSc1	pokles
dráhy	dráha	k1gFnSc2	dráha
dalekohledu	dalekohled	k1gInSc2	dalekohled
oddálen	oddálit	k5eAaPmNgInS	oddálit
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
původně	původně	k6eAd1	původně
plánovala	plánovat	k5eAaImAgFnS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hubblea	Hubblea	k1gFnSc1	Hubblea
během	během	k7c2	během
mise	mise	k1gFnSc2	mise
STS-144	STS-144	k1gFnSc2	STS-144
bezpečně	bezpečně	k6eAd1	bezpečně
sejme	sejmout	k5eAaPmIp3nS	sejmout
z	z	k7c2	z
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
dopraví	dopravit	k5eAaPmIp3nS	dopravit
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
by	by	kYmCp3nS	by
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
vystaven	vystavit	k5eAaPmNgInS	vystavit
ve	v	k7c6	v
Smithsonově	Smithsonův	k2eAgInSc6d1	Smithsonův
institutu	institut	k1gInSc6	institut
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
však	však	k9	však
již	již	k6eAd1	již
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
praktické	praktický	k2eAgNnSc4d1	praktické
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
cena	cena	k1gFnSc1	cena
jednoho	jeden	k4xCgInSc2	jeden
letu	let	k1gInSc2	let
raketoplánu	raketoplán	k1gInSc2	raketoplán
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
500	[number]	k4	500
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
odhadů	odhad	k1gInPc2	odhad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
program	program	k1gInSc1	program
raketoplánů	raketoplán	k1gInPc2	raketoplán
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
ukončen	ukončit	k5eAaPmNgInS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
hovoří	hovořit	k5eAaImIp3nP	hovořit
i	i	k9	i
obavy	obava	k1gFnPc1	obava
o	o	k7c4	o
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
posádky	posádka	k1gFnSc2	posádka
raketoplánu	raketoplán	k1gInSc2	raketoplán
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
NASA	NASA	kA	NASA
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
dalekohledu	dalekohled	k1gInSc3	dalekohled
připojí	připojit	k5eAaPmIp3nP	připojit
externí	externí	k2eAgFnSc4d1	externí
pohonnou	pohonný	k2eAgFnSc4d1	pohonná
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nP	by
ho	on	k3xPp3gInSc4	on
bezpečně	bezpečně	k6eAd1	bezpečně
navedla	navést	k5eAaPmAgFnS	navést
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
a	a	k8xC	a
pomohla	pomoct	k5eAaPmAgFnS	pomoct
tak	tak	k6eAd1	tak
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
jeho	on	k3xPp3gInSc4	on
pád	pád	k1gInSc4	pád
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
nebude	být	k5eNaImBp3nS	být
připojena	připojit	k5eAaPmNgFnS	připojit
při	při	k7c6	při
nastávající	nastávající	k2eAgFnSc6d1	nastávající
misi	mise	k1gFnSc6	mise
STS-	STS-	k1gFnSc1	STS-
<g/>
125	[number]	k4	125
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
k	k	k7c3	k
dalekohledu	dalekohled	k1gInSc3	dalekohled
nainstalováno	nainstalován	k2eAgNnSc1d1	nainstalováno
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ji	on	k3xPp3gFnSc4	on
umožní	umožnit	k5eAaPmIp3nS	umožnit
připojit	připojit	k5eAaPmF	připojit
později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
jiné	jiný	k2eAgFnSc6d1	jiná
<g/>
,	,	kIx,	,
robotické	robotický	k2eAgFnSc6d1	robotická
misi	mise	k1gFnSc6	mise
<g/>
.	.	kIx.	.
</s>
<s>
Raketoplán	raketoplán	k1gInSc1	raketoplán
Columbia	Columbia	k1gFnSc1	Columbia
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
navštívit	navštívit	k5eAaPmF	navštívit
Hubbleův	Hubbleův	k2eAgInSc4d1	Hubbleův
dalekohled	dalekohled	k1gInSc4	dalekohled
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
úlohou	úloha	k1gFnSc7	úloha
mise	mise	k1gFnSc2	mise
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
náhrada	náhrada	k1gFnSc1	náhrada
naváděcích	naváděcí	k2eAgMnPc2d1	naváděcí
senzorů	senzor	k1gInPc2	senzor
<g/>
,	,	kIx,	,
dvou	dva	k4xCgInPc2	dva
nefunkčních	funkční	k2eNgInPc2d1	nefunkční
gyroskopů	gyroskop	k1gInPc2	gyroskop
<g/>
,	,	kIx,	,
umístění	umístění	k1gNnSc4	umístění
ochranné	ochranný	k2eAgFnSc2d1	ochranná
pokrývky	pokrývka	k1gFnSc2	pokrývka
na	na	k7c4	na
poničenou	poničený	k2eAgFnSc4d1	poničená
izolaci	izolace	k1gFnSc4	izolace
<g/>
,	,	kIx,	,
výměna	výměna	k1gFnSc1	výměna
kombinované	kombinovaný	k2eAgFnSc2d1	kombinovaná
širokoúhlé	širokoúhlý	k2eAgFnSc2d1	širokoúhlá
a	a	k8xC	a
planetární	planetární	k2eAgFnSc2d1	planetární
kamery	kamera	k1gFnSc2	kamera
2	[number]	k4	2
novější	nový	k2eAgFnSc7d2	novější
širokoúhlou	širokoúhlý	k2eAgFnSc7d1	širokoúhlá
kamerou	kamera	k1gFnSc7	kamera
3	[number]	k4	3
a	a	k8xC	a
instalace	instalace	k1gFnSc1	instalace
přístroje	přístroj	k1gInSc2	přístroj
Cosmic	Cosmice	k1gFnPc2	Cosmice
Origins	Origins	k1gInSc1	Origins
Spectrograph	Spectrograph	k1gMnSc1	Spectrograph
(	(	kIx(	(
<g/>
COS	cos	kA	cos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ředitel	ředitel	k1gMnSc1	ředitel
NASA	NASA	kA	NASA
Sean	Sean	k1gMnSc1	Sean
O	o	k7c6	o
<g/>
'	'	kIx"	'
<g/>
Keefe	Keef	k1gInSc5	Keef
nicméně	nicméně	k8xC	nicméně
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
před	před	k7c7	před
opakováním	opakování	k1gNnSc7	opakování
havárie	havárie	k1gFnSc2	havárie
raketoplánu	raketoplán	k1gInSc2	raketoplán
Columbia	Columbia	k1gFnSc1	Columbia
rozdhodl	rozdhodnout	k5eAaPmAgMnS	rozdhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc4	všechen
budoucí	budoucí	k2eAgInPc4d1	budoucí
lety	let	k1gInPc4	let
raketoplánů	raketoplán	k1gInPc2	raketoplán
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
naplánovány	naplánovat	k5eAaBmNgInP	naplánovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
bezpečně	bezpečně	k6eAd1	bezpečně
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
stanice	stanice	k1gFnSc2	stanice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
vyřešeny	vyřešen	k2eAgFnPc4d1	vyřešena
případné	případný	k2eAgFnPc4d1	případná
závady	závada	k1gFnPc4	závada
a	a	k8xC	a
odkud	odkud	k6eAd1	odkud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
raketoplán	raketoplán	k1gInSc1	raketoplán
mohl	moct	k5eAaImAgInS	moct
bezpečně	bezpečně	k6eAd1	bezpečně
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
raketoplány	raketoplán	k1gInPc1	raketoplán
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
zároveň	zároveň	k6eAd1	zároveň
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
a	a	k8xC	a
při	při	k7c6	při
stejné	stejný	k2eAgFnSc6d1	stejná
misi	mise	k1gFnSc6	mise
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
i	i	k9	i
na	na	k7c4	na
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
ISS	ISS	kA	ISS
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
plánované	plánovaný	k2eAgFnPc4d1	plánovaná
pilotované	pilotovaný	k2eAgFnPc4d1	pilotovaná
mise	mise	k1gFnPc4	mise
k	k	k7c3	k
teleskopu	teleskop	k1gInSc3	teleskop
byly	být	k5eAaImAgInP	být
proto	proto	k6eAd1	proto
zrušeny	zrušit	k5eAaPmNgInP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
napadeno	napadnout	k5eAaPmNgNnS	napadnout
mnoha	mnoho	k4c3	mnoho
astronomy	astronom	k1gMnPc4	astronom
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
považovali	považovat	k5eAaImAgMnP	považovat
hodnotu	hodnota	k1gFnSc4	hodnota
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
za	za	k7c4	za
dostatečně	dostatečně	k6eAd1	dostatečně
velkou	velký	k2eAgFnSc4d1	velká
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
člověk	člověk	k1gMnSc1	člověk
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
určité	určitý	k2eAgNnSc4d1	určité
riziko	riziko	k1gNnSc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Pomyslný	pomyslný	k2eAgMnSc1d1	pomyslný
nástupce	nástupce	k1gMnSc1	nástupce
HST	HST	kA	HST
<g/>
,	,	kIx,	,
Vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
Jamese	Jamese	k1gFnSc1	Jamese
Webba	Webba	k1gMnSc1	Webba
(	(	kIx(	(
<g/>
JWST	JWST	kA	JWST
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
vypuštěn	vypuštěn	k2eAgInSc1d1	vypuštěn
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
–	–	k?	–
tedy	tedy	k8xC	tedy
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Hubble	Hubble	k1gMnSc1	Hubble
může	moct	k5eAaImIp3nS	moct
snímat	snímat	k5eAaImF	snímat
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
a	a	k8xC	a
viditelné	viditelný	k2eAgNnSc1d1	viditelné
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
JWST	JWST	kA	JWST
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
přímým	přímý	k2eAgMnSc7d1	přímý
nástupcem	nástupce	k1gMnSc7	nástupce
HST	HST	kA	HST
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
prioritou	priorita	k1gFnSc7	priorita
pro	pro	k7c4	pro
astronomickou	astronomický	k2eAgFnSc4d1	astronomická
veřejnost	veřejnost	k1gFnSc4	veřejnost
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
co	co	k9	co
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
vypuštění	vypuštění	k1gNnSc4	vypuštění
JWST	JWST	kA	JWST
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
velice	velice	k6eAd1	velice
přínosný	přínosný	k2eAgInSc4d1	přínosný
projekt	projekt	k1gInSc4	projekt
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
umístit	umístit	k5eAaPmF	umístit
JWST	JWST	kA	JWST
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgNnSc4	který
nebude	být	k5eNaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
případě	případ	k1gInSc6	případ
náhlé	náhlý	k2eAgFnSc2d1	náhlá
poruchy	porucha	k1gFnSc2	porucha
rychle	rychle	k6eAd1	rychle
zjednat	zjednat	k5eAaPmF	zjednat
nápravu	náprava	k1gFnSc4	náprava
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
rozdmýchala	rozdmýchat	k5eAaPmAgFnS	rozdmýchat
diskusi	diskuse	k1gFnSc4	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Astronomická	astronomický	k2eAgFnSc1d1	astronomická
veřejnost	veřejnost	k1gFnSc1	veřejnost
však	však	k9	však
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
poslední	poslední	k2eAgFnSc1d1	poslední
servisní	servisní	k2eAgFnSc1d1	servisní
mise	mise	k1gFnSc1	mise
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
hrazena	hradit	k5eAaImNgFnS	hradit
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
JWST	JWST	kA	JWST
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
mělo	mít	k5eAaImAgNnS	mít
upustit	upustit	k5eAaPmF	upustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Keefe	Keef	k1gInSc5	Keef
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
velkého	velký	k2eAgInSc2d1	velký
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
veřejnosti	veřejnost	k1gFnSc3	veřejnost
a	a	k8xC	a
žádosti	žádost	k1gFnSc3	žádost
Kongresu	kongres	k1gInSc2	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
zrevidovat	zrevidovat	k5eAaPmF	zrevidovat
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c6	o
zrušení	zrušení	k1gNnSc6	zrušení
poslední	poslední	k2eAgFnSc2d1	poslední
servisní	servisní	k2eAgFnSc2d1	servisní
mise	mise	k1gFnSc2	mise
Hubblea	Hubblea	k1gMnSc1	Hubblea
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
panelem	panel	k1gInSc7	panel
Národní	národní	k2eAgFnSc2d1	národní
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
vydáno	vydán	k2eAgNnSc4d1	vydáno
doporučení	doporučení	k1gNnSc4	doporučení
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
Hubble	Hubble	k1gMnSc1	Hubble
uchován	uchovat	k5eAaPmNgMnS	uchovat
i	i	k9	i
přes	přes	k7c4	přes
zjevná	zjevný	k2eAgNnPc4d1	zjevné
rizika	riziko	k1gNnPc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnně	souhrnně	k6eAd1	souhrnně
toto	tento	k3xDgNnSc1	tento
doporučení	doporučení	k1gNnSc1	doporučení
sdělovalo	sdělovat	k5eAaImAgNnS	sdělovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
NASA	NASA	kA	NASA
neměla	mít	k5eNaImAgFnS	mít
žádným	žádný	k3yNgInSc7	žádný
způsobem	způsob	k1gInSc7	způsob
pokoušet	pokoušet	k5eAaImF	pokoušet
bránit	bránit	k5eAaImF	bránit
v	v	k7c6	v
provedení	provedení	k1gNnSc6	provedení
servisní	servisní	k2eAgFnSc2d1	servisní
mise	mise	k1gFnSc2	mise
na	na	k7c6	na
Hubbleově	Hubbleův	k2eAgInSc6d1	Hubbleův
vesmírném	vesmírný	k2eAgInSc6d1	vesmírný
dalekohledu	dalekohled	k1gInSc6	dalekohled
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2004	[number]	k4	2004
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Keefe	Keef	k1gInSc5	Keef
oficiálně	oficiálně	k6eAd1	oficiálně
požádal	požádat	k5eAaPmAgMnS	požádat
Goddardovo	Goddardův	k2eAgNnSc4d1	Goddardovo
centrum	centrum	k1gNnSc4	centrum
pro	pro	k7c4	pro
vesmírné	vesmírný	k2eAgInPc4d1	vesmírný
lety	let	k1gInPc4	let
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připravilo	připravit	k5eAaPmAgNnS	připravit
detailní	detailní	k2eAgInSc4d1	detailní
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
robotickou	robotický	k2eAgFnSc4d1	robotická
servisní	servisní	k2eAgFnSc4d1	servisní
misi	mise	k1gFnSc4	mise
k	k	k7c3	k
Hubbleu	Hubbleus	k1gInSc3	Hubbleus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
plány	plán	k1gInPc1	plán
však	však	k9	však
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
zrušeny	zrušit	k5eAaPmNgInP	zrušit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
robotická	robotický	k2eAgFnSc1d1	robotická
mise	mise	k1gFnSc1	mise
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
jako	jako	k9	jako
nereálná	reálný	k2eNgFnSc1d1	nereálná
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
členů	člen	k1gInPc2	člen
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
<g/>
,	,	kIx,	,
vedených	vedený	k2eAgInPc2d1	vedený
senátorkou	senátorka	k1gFnSc7	senátorka
Barbarou	Barbara	k1gFnSc7	Barbara
Mikulski	Mikulski	k1gNnSc2	Mikulski
<g/>
,	,	kIx,	,
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
vést	vést	k5eAaImF	vést
veřejná	veřejný	k2eAgNnPc4d1	veřejné
slyšení	slyšení	k1gNnSc4	slyšení
a	a	k8xC	a
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
podporou	podpora	k1gFnSc7	podpora
veřejnosti	veřejnost	k1gFnSc2	veřejnost
se	se	k3xPyFc4	se
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
docílit	docílit	k5eAaPmF	docílit
přehodnocení	přehodnocení	k1gNnSc4	přehodnocení
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
zrušit	zrušit	k5eAaPmF	zrušit
záchrannou	záchranný	k2eAgFnSc4d1	záchranná
servisní	servisní	k2eAgFnSc4d1	servisní
misi	mise	k1gFnSc4	mise
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
administrativou	administrativa	k1gFnSc7	administrativa
prezidenta	prezident	k1gMnSc2	prezident
George	George	k1gNnSc2	George
W.	W.	kA	W.
Bushe	Bush	k1gMnSc4	Bush
i	i	k8xC	i
s	s	k7c7	s
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
nového	nový	k2eAgMnSc2d1	nový
ředitele	ředitel	k1gMnSc2	ředitel
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
Michaela	Michael	k1gMnSc2	Michael
D.	D.	kA	D.
Griffina	Griffin	k1gMnSc2	Griffin
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgInS	změnit
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
servisní	servisní	k2eAgFnSc3d1	servisní
misi	mise	k1gFnSc3	mise
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
Griffin	Griffin	k1gMnSc1	Griffin
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
znovu	znovu	k6eAd1	znovu
zvážit	zvážit	k5eAaPmF	zvážit
možnost	možnost	k1gFnSc4	možnost
servisní	servisní	k2eAgFnSc2d1	servisní
mise	mise	k1gFnSc2	mise
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
posádkou	posádka	k1gFnSc7	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
uvedení	uvedení	k1gNnSc6	uvedení
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
požádal	požádat	k5eAaPmAgMnS	požádat
Goddardovo	Goddardův	k2eAgNnSc4d1	Goddardovo
centrum	centrum	k1gNnSc4	centrum
pro	pro	k7c4	pro
vesmírné	vesmírný	k2eAgInPc4d1	vesmírný
lety	let	k1gInPc4	let
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
v	v	k7c6	v
přerušených	přerušený	k2eAgFnPc6d1	přerušená
přípravách	příprava	k1gFnPc6	příprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
Griffin	Griffin	k1gInSc1	Griffin
vydal	vydat	k5eAaPmAgInS	vydat
konečné	konečný	k2eAgNnSc4d1	konečné
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
dává	dávat	k5eAaImIp3nS	dávat
misi	mise	k1gFnSc4	mise
definitivně	definitivně	k6eAd1	definitivně
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Jedenáctidenní	jedenáctidenní	k2eAgFnSc1d1	jedenáctidenní
mise	mise	k1gFnSc1	mise
STS-125	STS-125	k1gFnSc2	STS-125
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
provedena	provést	k5eAaPmNgFnS	provést
pomocí	pomocí	k7c2	pomocí
raketoplánu	raketoplán	k1gInSc2	raketoplán
Atlantis	Atlantis	k1gFnSc1	Atlantis
a	a	k8xC	a
původní	původní	k2eAgInSc1d1	původní
datum	datum	k1gInSc1	datum
startu	start	k1gInSc2	start
představoval	představovat	k5eAaImAgInS	představovat
14	[number]	k4	14
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vážných	vážný	k2eAgInPc2d1	vážný
technických	technický	k2eAgInPc2d1	technický
problémů	problém	k1gInPc2	problém
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
systému	systém	k1gInSc2	systém
Side	Sid	k1gFnSc2	Sid
A	A	kA	A
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nimž	jenž	k3xRgFnPc3	jenž
byly	být	k5eAaImAgFnP	být
přerušeny	přerušit	k5eAaPmNgFnP	přerušit
veškeré	veškerý	k3xTgFnPc1	veškerý
výzkumné	výzkumný	k2eAgFnPc1d1	výzkumná
operace	operace	k1gFnPc1	operace
Hubbleova	Hubbleův	k2eAgInSc2d1	Hubbleův
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
start	start	k1gInSc1	start
opět	opět	k6eAd1	opět
odložen	odložen	k2eAgInSc1d1	odložen
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Servisní	servisní	k2eAgFnSc1d1	servisní
mise	mise	k1gFnSc1	mise
4	[number]	k4	4
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
výměnu	výměna	k1gFnSc4	výměna
starých	starý	k2eAgFnPc2d1	stará
baterií	baterie	k1gFnPc2	baterie
za	za	k7c4	za
nové	nový	k2eAgMnPc4d1	nový
<g/>
,	,	kIx,	,
výměnu	výměna	k1gFnSc4	výměna
všech	všecek	k3xTgMnPc2	všecek
gyroskopů	gyroskop	k1gInPc2	gyroskop
<g/>
,	,	kIx,	,
instalaci	instalace	k1gFnSc4	instalace
širokoúhlé	širokoúhlý	k2eAgFnSc2d1	širokoúhlá
planetární	planetární	k2eAgFnSc2d1	planetární
kamery	kamera	k1gFnSc2	kamera
a	a	k8xC	a
spektrografu	spektrograf	k1gInSc2	spektrograf
Cosmic	Cosmice	k1gInPc2	Cosmice
Origins	Originsa	k1gFnPc2	Originsa
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
mise	mise	k1gFnSc2	mise
STS-125	STS-125	k1gFnSc2	STS-125
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
raketoplán	raketoplán	k1gInSc1	raketoplán
Atlantis	Atlantis	k1gFnSc1	Atlantis
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
HST	HST	kA	HST
a	a	k8xC	a
současně	současně	k6eAd1	současně
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
startovací	startovací	k2eAgFnSc6d1	startovací
rampě	rampa	k1gFnSc6	rampa
připraven	připravit	k5eAaPmNgInS	připravit
raketoplán	raketoplán	k1gInSc1	raketoplán
Endeavour	Endeavour	k1gInSc1	Endeavour
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
odstartovat	odstartovat	k5eAaPmF	odstartovat
a	a	k8xC	a
zachránit	zachránit	k5eAaPmF	zachránit
posádku	posádka	k1gFnSc4	posádka
Atlantisu	Atlantis	k1gInSc2	Atlantis
<g/>
.	.	kIx.	.
</s>
