<s>
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
HST	HST	kA	HST
z	z	k7c2	z
Hubble	Hubble	k1gFnSc2	Hubble
Space	Space	k1gMnSc5	Space
Telescope	Telescop	k1gMnSc5	Telescop
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
Hubble	Hubble	k1gFnSc1	Hubble
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dalekohled	dalekohled	k1gInSc1	dalekohled
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Země	zem	k1gFnSc2	zem
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
600	[number]	k4	600
kilometrů	kilometr	k1gInPc2	kilometr
vynesl	vynést	k5eAaPmAgInS	vynést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
při	při	k7c6	při
letu	let	k1gInSc6	let
STS-31	STS-31	k1gFnSc2	STS-31
americký	americký	k2eAgInSc4d1	americký
raketoplán	raketoplán	k1gInSc4	raketoplán
Discovery	Discovera	k1gFnSc2	Discovera
<g/>
.	.	kIx.	.
</s>
