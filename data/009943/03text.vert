<p>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
714	[number]	k4	714
<g/>
/	/	kIx~	/
<g/>
715	[number]	k4	715
Řím	Řím	k1gInSc1	Řím
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
757	[number]	k4	757
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
papežem	papež	k1gMnSc7	papež
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
752	[number]	k4	752
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Očíslování	očíslování	k1gNnSc1	očíslování
jeho	jeho	k3xOp3gNnSc2	jeho
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
sv.	sv.	kA	sv.
Zachariáše	Zachariáš	k1gMnSc2	Zachariáš
byl	být	k5eAaImAgMnS	být
papežem	papež	k1gMnSc7	papež
zvolen	zvolit	k5eAaPmNgMnS	zvolit
rovněž	rovněž	k6eAd1	rovněž
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ale	ale	k9	ale
zemřel	zemřít	k5eAaPmAgMnS	zemřít
již	již	k6eAd1	již
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
volbě	volba	k1gFnSc6	volba
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
vysvěcen	vysvětit	k5eAaPmNgMnS	vysvětit
na	na	k7c4	na
biskupa	biskup	k1gMnSc4	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
pojetí	pojetí	k1gNnSc6	pojetí
nebyl	být	k5eNaImAgInS	být
papežem	papež	k1gMnSc7	papež
(	(	kIx(	(
<g/>
biskupem	biskup	k1gInSc7	biskup
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
podle	podle	k7c2	podle
pojetí	pojetí	k1gNnSc2	pojetí
papeže	papež	k1gMnSc2	papež
jako	jako	k8xC	jako
světského	světský	k2eAgMnSc4d1	světský
panovníka	panovník	k1gMnSc4	panovník
jím	jíst	k5eAaImIp1nS	jíst
byl	být	k5eAaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
vznešené	vznešený	k2eAgFnSc2d1	vznešená
římské	římský	k2eAgFnSc2d1	římská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
jistého	jistý	k2eAgMnSc2d1	jistý
Konstantina	Konstantin	k1gMnSc2	Konstantin
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
osiřel	osiřet	k5eAaPmAgMnS	osiřet
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
vychován	vychovat	k5eAaPmNgMnS	vychovat
v	v	k7c6	v
lateránské	lateránský	k2eAgFnSc6d1	Lateránská
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
svěcení	svěcení	k1gNnSc4	svěcení
jáhna	jáhen	k1gMnSc2	jáhen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
stejnojmenného	stejnojmenný	k2eAgMnSc2d1	stejnojmenný
předchůdce	předchůdce	k1gMnSc2	předchůdce
musel	muset	k5eAaImAgInS	muset
řešit	řešit	k5eAaImF	řešit
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
rozpínavost	rozpínavost	k1gFnSc4	rozpínavost
Langobardského	Langobardský	k2eAgNnSc2d1	Langobardský
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
král	král	k1gMnSc1	král
Aistulf	Aistulf	k1gMnSc1	Aistulf
již	již	k6eAd1	již
za	za	k7c2	za
pontifikátu	pontifikát	k1gInSc2	pontifikát
papeže	papež	k1gMnSc2	papež
Zachariáše	Zachariáš	k1gMnSc2	Zachariáš
obsadil	obsadit	k5eAaPmAgMnS	obsadit
roku	rok	k1gInSc2	rok
751	[number]	k4	751
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
území	území	k1gNnSc4	území
Ravennského	Ravennský	k2eAgInSc2d1	Ravennský
exarchátu	exarchát	k1gInSc2	exarchát
patříciho	patřícize	k6eAd1	patřícize
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
k	k	k7c3	k
Byzantské	byzantský	k2eAgFnSc3d1	byzantská
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
jmenovaného	jmenovaný	k1gMnSc2	jmenovaný
papeže	papež	k1gMnSc2	papež
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
podmatit	podmatit	k5eAaImF	podmatit
i	i	k9	i
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečí	nebezpeč	k1gFnSc7wB	nebezpeč
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
nový	nový	k2eAgMnSc1d1	nový
papež	papež	k1gMnSc1	papež
Štěpán	Štěpán	k1gMnSc1	Štěpán
II	II	kA	II
<g/>
.	.	kIx.	.
zažehnat	zažehnat	k5eAaPmF	zažehnat
bohatým	bohatý	k2eAgInSc7d1	bohatý
úplatkem	úplatek	k1gInSc7	úplatek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
langobarský	langobarský	k2eAgMnSc1d1	langobarský
král	král	k1gMnSc1	král
i	i	k9	i
přes	přes	k7c4	přes
podepsání	podepsání	k1gNnSc4	podepsání
dohody	dohoda	k1gFnSc2	dohoda
požadoval	požadovat	k5eAaImAgMnS	požadovat
i	i	k9	i
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
daň	daň	k1gFnSc4	daň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
nedočkal	dočkat	k5eNaPmAgMnS	dočkat
žádného	žádný	k3yNgInSc2	žádný
vojenského	vojenský	k2eAgInSc2d1	vojenský
zásahu	zásah	k1gInSc2	zásah
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
byzantského	byzantský	k2eAgMnSc2d1	byzantský
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
formálního	formální	k2eAgNnSc2d1	formální
potvrzení	potvrzení	k1gNnSc2	potvrzení
jeho	jeho	k3xOp3gNnSc2	jeho
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
k	k	k7c3	k
franskému	franský	k2eAgMnSc3d1	franský
králi	král	k1gMnSc3	král
Pipinovi	Pipin	k1gMnSc3	Pipin
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
754	[number]	k4	754
osobně	osobně	k6eAd1	osobně
tohoto	tento	k3xDgMnSc4	tento
krále	král	k1gMnSc4	král
navštívil	navštívit	k5eAaPmAgMnS	navštívit
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
krále	král	k1gMnSc2	král
Pipina	pipina	k1gFnSc1	pipina
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
syny	syn	k1gMnPc7	syn
opětovně	opětovně	k6eAd1	opětovně
pomazal	pomazat	k5eAaPmAgMnS	pomazat
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
Saint-Denis	Saint-Denis	k1gFnSc2	Saint-Denis
<g/>
..	..	k?	..
Oplátkou	oplátka	k1gFnSc7	oplátka
překročil	překročit	k5eAaPmAgMnS	překročit
tento	tento	k3xDgMnSc1	tento
král	král	k1gMnSc1	král
Alpy	alpa	k1gFnSc2	alpa
a	a	k8xC	a
donutil	donutit	k5eAaPmAgMnS	donutit
Aistulfa	Aistulf	k1gMnSc4	Aistulf
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
ravennského	ravennský	k2eAgInSc2d1	ravennský
exarchátu	exarchát	k1gInSc2	exarchát
papeži	papež	k1gMnSc3	papež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
II	II	kA	II
<g/>
.	.	kIx.	.
přijal	přijmout	k5eAaPmAgMnS	přijmout
fakticky	fakticky	k6eAd1	fakticky
od	od	k7c2	od
franského	franský	k2eAgMnSc2d1	franský
panovníka	panovník	k1gMnSc2	panovník
Pipina	pipina	k1gFnSc1	pipina
Krátkého	krátké	k1gNnSc2	krátké
tato	tento	k3xDgFnSc1	tento
území	území	k1gNnSc6	území
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Pipinova	Pipinův	k2eAgFnSc1d1	Pipinova
donace	donace	k1gFnSc1	donace
-	-	kIx~	-
písemně	písemně	k6eAd1	písemně
ji	on	k3xPp3gFnSc4	on
papež	papež	k1gMnSc1	papež
obdržel	obdržet	k5eAaPmAgMnS	obdržet
již	již	k6eAd1	již
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
roku	rok	k1gInSc2	rok
754	[number]	k4	754
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
počátkem	počátkem	k7c2	počátkem
existence	existence	k1gFnSc2	existence
papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aistulf	Aistulf	k1gInSc1	Aistulf
se	se	k3xPyFc4	se
s	s	k7c7	s
porážkou	porážka	k1gFnSc7	porážka
nesmířil	smířit	k5eNaPmAgMnS	smířit
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
755	[number]	k4	755
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
na	na	k7c4	na
Řím	Řím	k1gInSc4	Řím
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Pipin	pipina	k1gFnPc2	pipina
Krátký	Krátký	k1gMnSc1	Krátký
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
opět	opět	k6eAd1	opět
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
Pávii	pávie	k1gFnSc4	pávie
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Langobardů	Langobard	k1gInPc2	Langobard
<g/>
.	.	kIx.	.
</s>
<s>
Aistulf	Aistulf	k1gMnSc1	Aistulf
kapituloval	kapitulovat	k5eAaBmAgMnS	kapitulovat
<g/>
,	,	kIx,	,
podepsal	podepsat	k5eAaPmAgMnS	podepsat
mír	mír	k1gInSc4	mír
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
podpořil	podpořit	k5eAaPmAgMnS	podpořit
papež	papež	k1gMnSc1	papež
Štěpán	Štěpán	k1gMnSc1	Štěpán
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
zájemců	zájemce	k1gMnPc2	zájemce
o	o	k7c4	o
langobardský	langobardský	k2eAgInSc4d1	langobardský
trůn	trůn	k1gInSc4	trůn
Desideria	desiderium	k1gNnSc2	desiderium
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
slíbil	slíbit	k5eAaPmAgMnS	slíbit
zachování	zachování	k1gNnSc4	zachování
mocenského	mocenský	k2eAgInSc2d1	mocenský
a	a	k8xC	a
územního	územní	k2eAgInSc2d1	územní
stavu	stav	k1gInSc2	stav
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
tento	tento	k3xDgInSc4	tento
slib	slib	k1gInSc4	slib
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Štěpán	Štěpána	k1gFnPc2	Štěpána
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stephen	Stephen	k1gInSc1	Stephen
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
)	)	kIx)	)
III	III	kA	III
na	na	k7c6	na
Catholic	Catholice	k1gFnPc2	Catholice
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
britannica	britannica	k1gFnSc1	britannica
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Stephen-II	Stephen-II	k1gMnSc1	Stephen-II
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
archive	archiv	k1gInSc5	archiv
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
Všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
církevní	církevní	k2eAgInSc1d1	církevní
dějepis	dějepis	k1gInSc1	dějepis
2	[number]	k4	2
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
I.	I.	kA	I.
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Aistulf	Aistulf	k1gInSc1	Aistulf
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
555	[number]	k4	555
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
epizodní	epizodní	k2eAgMnSc1d1	epizodní
papež	papež	k1gMnSc1	papež
<g/>
)	)	kIx)	)
</s>
</p>
