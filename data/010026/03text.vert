<p>
<s>
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
Inšpruk	Inšpruka	k1gFnPc2	Inšpruka
nebo	nebo	k8xC	nebo
Inomostí	Inomost	k1gFnPc2	Inomost
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Oenipontum	Oenipontum	k1gNnSc1	Oenipontum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
rakouské	rakouský	k2eAgFnSc2d1	rakouská
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Innu	Innus	k1gInSc2	Innus
<g/>
,	,	kIx,	,
v	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
podhůří	podhůří	k1gNnSc6	podhůří
Alp	Alpy	k1gFnPc2	Alpy
<g/>
,	,	kIx,	,
asi	asi	k9	asi
30	[number]	k4	30
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Brennerského	Brennerský	k2eAgInSc2d1	Brennerský
průsmyku	průsmyk	k1gInSc2	průsmyk
<g/>
,	,	kIx,	,
nejschůdnější	schůdný	k2eAgFnPc4d3	nejschůdnější
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgFnPc1d1	vysoká
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Hafelekar	Hafelekara	k1gFnPc2	Hafelekara
(	(	kIx(	(
<g/>
2334	[number]	k4	2334
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Serles	Serlesa	k1gFnPc2	Serlesa
(	(	kIx(	(
<g/>
2718	[number]	k4	2718
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
složeno	složit	k5eAaPmNgNnS	složit
z	z	k7c2	z
německých	německý	k2eAgInPc2d1	německý
názvů	název	k1gInPc2	název
řeky	řeka	k1gFnSc2	řeka
Inn	Inn	k1gFnSc1	Inn
a	a	k8xC	a
most	most	k1gInSc1	most
Brücke	Brück	k1gFnSc2	Brück
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
tedy	tedy	k9	tedy
most	most	k1gInSc4	most
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Inn	Inn	k1gFnSc2	Inn
<g/>
.	.	kIx.	.
</s>
<s>
Innsbruck	Innsbruck	k6eAd1	Innsbruck
je	být	k5eAaImIp3nS	být
páté	pátý	k4xOgNnSc1	pátý
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Rakouska	Rakousko	k1gNnSc2	Rakousko
(	(	kIx(	(
<g/>
po	po	k7c6	po
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
Linci	Linec	k1gInSc6	Linec
a	a	k8xC	a
Salzburgu	Salzburg	k1gInSc6	Salzburg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
131	[number]	k4	131
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Innsbruck	Innsbruck	k6eAd1	Innsbruck
má	mít	k5eAaImIp3nS	mít
univerzitu	univerzita	k1gFnSc4	univerzita
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
biskupa	biskup	k1gInSc2	biskup
a	a	k8xC	a
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
cílem	cíl	k1gInSc7	cíl
turistů	turist	k1gMnPc2	turist
a	a	k8xC	a
lyžařů	lyžař	k1gMnPc2	lyžař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
kolem	kolem	k7c2	kolem
3000	[number]	k4	3000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
nálezy	nález	k1gInPc4	nález
z	z	k7c2	z
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
a	a	k8xC	a
urny	urna	k1gFnSc2	urna
s	s	k7c7	s
ostatky	ostatek	k1gInPc7	ostatek
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
období	období	k1gNnSc2	období
Laténské	laténský	k2eAgFnSc2d1	laténská
kultury	kultura	k1gFnSc2	kultura
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c4	po
Kristu	Krista	k1gFnSc4	Krista
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Wiltenu	Wilten	k1gInSc6	Wilten
římská	římský	k2eAgFnSc1d1	římská
pevnost	pevnost	k1gFnSc1	pevnost
Vedildena	Vedildena	k1gFnSc1	Vedildena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
ochránit	ochránit	k5eAaPmF	ochránit
římskou	římský	k2eAgFnSc4d1	římská
cestu	cesta	k1gFnSc4	cesta
spojující	spojující	k2eAgFnSc4d1	spojující
Veronu	Verona	k1gFnSc4	Verona
s	s	k7c7	s
Brennerským	Brennerský	k2eAgInSc7d1	Brennerský
průsmykem	průsmyk	k1gInSc7	průsmyk
a	a	k8xC	a
Řeznem	Řezno	k1gNnSc7	Řezno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
stěhování	stěhování	k1gNnPc2	stěhování
národů	národ	k1gInPc2	národ
pevnost	pevnost	k1gFnSc4	pevnost
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
sídla	sídlo	k1gNnPc1	sídlo
jsou	být	k5eAaImIp3nP	být
známa	znám	k2eAgNnPc1d1	známo
jako	jako	k8xS	jako
Ambras	Ambras	k1gMnSc1	Ambras
<g/>
,	,	kIx,	,
Hötting	Hötting	k1gInSc4	Hötting
a	a	k8xC	a
Mühlau	Mühlaa	k1gFnSc4	Mühlaa
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1138	[number]	k4	1138
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
klášter	klášter	k1gInSc1	klášter
premonstrátů	premonstrát	k1gMnPc2	premonstrát
Wilten	Wilten	k2eAgMnSc1d1	Wilten
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
provozoval	provozovat	k5eAaImAgMnS	provozovat
převoz	převoz	k1gInSc4	převoz
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Inn	Inn	k1gFnSc2	Inn
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
osada	osada	k1gFnSc1	osada
při	pře	k1gFnSc3	pře
mostu	most	k1gInSc2	most
a	a	k8xC	a
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1205	[number]	k4	1205
dostala	dostat	k5eAaPmAgFnS	dostat
osada	osada	k1gFnSc1	osada
městská	městský	k2eAgFnSc1d1	městská
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
osady	osada	k1gFnSc2	osada
rychle	rychle	k6eAd1	rychle
rostl	růst	k5eAaImAgInS	růst
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kontrolovala	kontrolovat	k5eAaImAgFnS	kontrolovat
obchodní	obchodní	k2eAgFnSc4d1	obchodní
stezku	stezka	k1gFnSc4	stezka
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
Berchtold	Berchtold	k1gMnSc1	Berchtold
V.	V.	kA	V.
zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
1170	[number]	k4	1170
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgFnSc4	první
tržnici	tržnice	k1gFnSc4	tržnice
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
nejdříve	dříve	k6eAd3	dříve
Insprucke	Insprucke	k1gFnSc4	Insprucke
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
Oenipontum	Oenipontum	k1gNnSc1	Oenipontum
nebo	nebo	k8xC	nebo
Oeni	Oeni	k1gNnSc1	Oeni
pons	ponsa	k1gFnPc2	ponsa
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
Innsbruck	Innsbruck	k1gInSc4	Innsbruck
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
most	most	k1gInSc4	most
na	na	k7c4	na
Innu	Inna	k1gFnSc4	Inna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1187	[number]	k4	1187
a	a	k8xC	a
1205	[number]	k4	1205
obdržel	obdržet	k5eAaPmAgInS	obdržet
trh	trh	k1gInSc4	trh
městská	městský	k2eAgNnPc4d1	Městské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
celních	celní	k2eAgInPc2d1	celní
poplatků	poplatek	k1gInPc2	poplatek
za	za	k7c4	za
obchodní	obchodní	k2eAgFnSc4d1	obchodní
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
a	a	k8xC	a
zisk	zisk	k1gInSc1	zisk
značně	značně	k6eAd1	značně
rostl	růst	k5eAaImAgInS	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1267	[number]	k4	1267
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
městská	městský	k2eAgFnSc1d1	městská
pečeť	pečeť	k1gFnSc1	pečeť
a	a	k8xC	a
znak	znak	k1gInSc1	znak
města	město	k1gNnSc2	město
viděného	viděný	k2eAgNnSc2d1	viděné
z	z	k7c2	z
ptačí	ptačí	k2eAgFnSc2d1	ptačí
perspektivy	perspektiva	k1gFnSc2	perspektiva
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
rostlo	růst	k5eAaImAgNnS	růst
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pozemky	pozemka	k1gFnPc1	pozemka
patřily	patřit	k5eAaImAgFnP	patřit
klášteru	klášter	k1gInSc3	klášter
ve	v	k7c6	v
Wiltenu	Wilten	k1gInSc6	Wilten
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
je	on	k3xPp3gNnPc4	on
pokaždé	pokaždé	k6eAd1	pokaždé
směnit	směnit	k5eAaPmF	směnit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1281	[number]	k4	1281
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
o	o	k7c4	o
Nové	Nové	k2eAgNnSc4d1	Nové
město	město	k1gNnSc4	město
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
oblast	oblast	k1gFnSc1	oblast
ulice	ulice	k1gFnSc2	ulice
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
přes	přes	k7c4	přes
Brenner	Brenner	k1gInSc4	Brenner
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
zpevněna	zpevněn	k2eAgFnSc1d1	zpevněna
a	a	k8xC	a
opevněna	opevněn	k2eAgFnSc1d1	opevněna
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jí	on	k3xPp3gFnSc7	on
mohly	moct	k5eAaImAgFnP	moct
projíždět	projíždět	k5eAaImF	projíždět
i	i	k9	i
kočáry	kočár	k1gInPc4	kočár
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
konkurenční	konkurenční	k2eAgFnSc1d1	konkurenční
výhoda	výhoda	k1gFnSc1	výhoda
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgInPc3d1	ostatní
průsmykům	průsmyk	k1gInPc3	průsmyk
jako	jako	k9	jako
například	například	k6eAd1	například
sousední	sousední	k2eAgInPc1d1	sousední
Via	via	k7c4	via
Rosia	Rosium	k1gNnPc4	Rosium
nebo	nebo	k8xC	nebo
via	via	k7c4	via
Claudia	Claudia	k1gFnSc1	Claudia
Augusta	August	k1gMnSc2	August
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k9	ještě
dlouho	dlouho	k6eAd1	dlouho
sloužili	sloužit	k5eAaImAgMnP	sloužit
k	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
mezci	mezek	k1gMnPc1	mezek
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
první	první	k4xOgFnSc1	první
městská	městský	k2eAgFnSc1d1	městská
nemocnice	nemocnice	k1gFnSc1	nemocnice
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
náměstí	náměstí	k1gNnSc6	náměstí
Adolfa	Adolf	k1gMnSc2	Adolf
Pichlera	Pichler	k1gMnSc2	Pichler
a	a	k8xC	a
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
byl	být	k5eAaImAgInS	být
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
residencí	residence	k1gFnSc7	residence
tyrolského	tyrolský	k2eAgMnSc2d1	tyrolský
hraběte	hrabě	k1gMnSc2	hrabě
Fridricha	Fridrich	k1gMnSc2	Fridrich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
a	a	k8xC	a
během	během	k7c2	během
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgFnPc2d1	významná
staveb	stavba	k1gFnPc2	stavba
<g/>
:	:	kIx,	:
Stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
(	(	kIx(	(
<g/>
1358	[number]	k4	1358
<g/>
)	)	kIx)	)
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
věží	věž	k1gFnSc7	věž
(	(	kIx(	(
<g/>
1450	[number]	k4	1450
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hofburg	Hofburg	k1gInSc1	Hofburg
(	(	kIx(	(
<g/>
1456	[number]	k4	1456
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ottův	Ottův	k2eAgInSc1d1	Ottův
hrádek	hrádek	k1gInSc1	hrádek
(	(	kIx(	(
<g/>
1495	[number]	k4	1495
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stará	starý	k2eAgFnSc1d1	stará
zbrojnice	zbrojnice	k1gFnSc1	zbrojnice
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1500	[number]	k4	1500
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1485	[number]	k4	1485
zde	zde	k6eAd1	zde
iniciátor	iniciátor	k1gMnSc1	iniciátor
"	"	kIx"	"
<g/>
Kladiva	kladivo	k1gNnPc4	kladivo
na	na	k7c4	na
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
<g/>
"	"	kIx"	"
Heinrich	Heinrich	k1gMnSc1	Heinrich
Institoris	Institoris	k1gFnSc2	Institoris
zahájil	zahájit	k5eAaPmAgMnS	zahájit
čarodějnické	čarodějnický	k2eAgInPc4d1	čarodějnický
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
byly	být	k5eAaImAgFnP	být
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zastaveny	zastavit	k5eAaPmNgFnP	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
roku	rok	k1gInSc2	rok
1519	[number]	k4	1519
zde	zde	k6eAd1	zde
sídlil	sídlit	k5eAaImAgMnS	sídlit
císař	císař	k1gMnSc1	císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc4d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
sem	sem	k6eAd1	sem
přesunul	přesunout	k5eAaPmAgMnS	přesunout
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
blízkosti	blízkost	k1gFnSc2	blízkost
stříbrných	stříbrný	k2eAgInPc2d1	stříbrný
dolů	dol	k1gInPc2	dol
v	v	k7c6	v
Hallu	Hall	k1gInSc6	Hall
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
razil	razit	k5eAaImAgMnS	razit
císařskou	císařský	k2eAgFnSc4d1	císařská
minci	mince	k1gFnSc4	mince
i	i	k8xC	i
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1503-1513	[number]	k4	1503-1513
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
císařském	císařský	k2eAgInSc6d1	císařský
hradě	hrad	k1gInSc6	hrad
založen	založit	k5eAaPmNgInS	založit
dvorský	dvorský	k2eAgInSc1d1	dvorský
kostel	kostel	k1gInSc1	kostel
(	(	kIx(	(
<g/>
Hofkirche	Hofkirche	k1gInSc1	Hofkirche
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
synovec	synovec	k1gMnSc1	synovec
císaře	císař	k1gMnSc2	císař
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
dal	dát	k5eAaPmAgMnS	dát
postavit	postavit	k5eAaPmF	postavit
mramorové	mramorový	k2eAgNnSc4d1	mramorové
mauzoleum	mauzoleum	k1gNnSc4	mauzoleum
s	s	k7c7	s
výjevy	výjev	k1gInPc7	výjev
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
na	na	k7c6	na
bronzových	bronzový	k2eAgFnPc6d1	bronzová
deskách	deska	k1gFnPc6	deska
(	(	kIx(	(
<g/>
kenotaf	kenotaf	k1gInSc4	kenotaf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1562	[number]	k4	1562
pozval	pozvat	k5eAaPmAgMnS	pozvat
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Tyrolský	tyrolský	k2eAgInSc1d1	tyrolský
spoluzakladatele	spoluzakladatel	k1gMnPc4	spoluzakladatel
řádu	řád	k1gInSc2	řád
jezuitů	jezuita	k1gMnPc2	jezuita
Petra	Petr	k1gMnSc2	Petr
Canisia	Canisius	k1gMnSc2	Canisius
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
založil	založit	k5eAaPmAgMnS	založit
jezuitské	jezuitský	k2eAgNnSc4d1	jezuitské
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Akademické	akademický	k2eAgNnSc4d1	akademické
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1630	[number]	k4	1630
hrabě	hrabě	k1gMnSc1	hrabě
Leopold	Leopold	k1gMnSc1	Leopold
V.	V.	kA	V.
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgNnSc4	první
operní	operní	k2eAgNnSc4d1	operní
divadlo	divadlo	k1gNnSc4	divadlo
mimo	mimo	k7c4	mimo
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
;	;	kIx,	;
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
místě	místo	k1gNnSc6	místo
stojí	stát	k5eAaImIp3nS	stát
dnešní	dnešní	k2eAgInSc1d1	dnešní
Kongresshaus	Kongresshaus	k1gInSc1	Kongresshaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyrolští	tyrolský	k2eAgMnPc1d1	tyrolský
Habsburkové	Habsburk	k1gMnPc1	Habsburk
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
po	po	k7c6	po
meči	meč	k1gInSc6	meč
úmrtím	úmrtí	k1gNnSc7	úmrtí
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Franze	Franze	k1gFnSc2	Franze
roku	rok	k1gInSc2	rok
1665	[number]	k4	1665
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
naposledy	naposledy	k6eAd1	naposledy
jejich	jejich	k3xOp3gNnSc7	jejich
hlavním	hlavní	k2eAgNnSc7d1	hlavní
sídlem	sídlo	k1gNnSc7	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
zůstal	zůstat	k5eAaPmAgInS	zůstat
gotický	gotický	k2eAgInSc4d1	gotický
a	a	k8xC	a
renesanční	renesanční	k2eAgInSc4d1	renesanční
ráz	ráz	k1gInSc4	ráz
města	město	k1gNnSc2	město
nezměněn	změnit	k5eNaPmNgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1669	[number]	k4	1669
založil	založit	k5eAaPmAgMnS	založit
císař	císař	k1gMnSc1	císař
Leopold	Leopold	k1gMnSc1	Leopold
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc4d1	habsburský
v	v	k7c6	v
Innsbrucku	Innsbrucko	k1gNnSc6	Innsbrucko
Univerzitu	univerzita	k1gFnSc4	univerzita
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
fakultami	fakulta	k1gFnPc7	fakulta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
byl	být	k5eAaImAgInS	být
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
spojen	spojit	k5eAaPmNgInS	spojit
železnicí	železnice	k1gFnSc7	železnice
s	s	k7c7	s
Mnichovem	Mnichov	k1gInSc7	Mnichov
a	a	k8xC	a
1867	[number]	k4	1867
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
železnice	železnice	k1gFnSc1	železnice
přes	přes	k7c4	přes
Brennerský	Brennerský	k2eAgInSc4d1	Brennerský
průsmyk	průsmyk	k1gInSc4	průsmyk
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
byla	být	k5eAaImAgFnS	být
železnice	železnice	k1gFnSc1	železnice
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
až	až	k6eAd1	až
do	do	k7c2	do
Garmisch-Partenkirchenu	Garmisch-Partenkirchen	k1gInSc2	Garmisch-Partenkirchen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
obsazen	obsadit	k5eAaPmNgInS	obsadit
italskými	italský	k2eAgFnPc7d1	italská
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
odtáhly	odtáhnout	k5eAaPmAgFnP	odtáhnout
až	až	k9	až
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
Innsbruck	Innsbruck	k1gInSc4	Innsbruck
22	[number]	k4	22
<g/>
krát	krát	k6eAd1	krát
bombardován	bombardovat	k5eAaImNgInS	bombardovat
spojeneckými	spojenecký	k2eAgFnPc7d1	spojenecká
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
byl	být	k5eAaImAgInS	být
těžce	těžce	k6eAd1	těžce
poškozen	poškodit	k5eAaPmNgInS	poškodit
dvorský	dvorský	k2eAgInSc1d1	dvorský
kostel	kostel	k1gInSc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
rakouských	rakouský	k2eAgNnPc2d1	rakouské
měst	město	k1gNnPc2	město
byl	být	k5eAaImAgMnS	být
Innsbruck	Innsbruck	k1gInSc4	Innsbruck
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
války	válka	k1gFnSc2	válka
obsazen	obsadit	k5eAaPmNgInS	obsadit
americkými	americký	k2eAgMnPc7d1	americký
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
do	do	k7c2	do
francouzské	francouzský	k2eAgFnSc2d1	francouzská
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1948	[number]	k4	1948
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
nově	nově	k6eAd1	nově
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
letiště	letiště	k1gNnSc1	letiště
<g/>
,	,	kIx,	,
významné	významný	k2eAgNnSc1d1	významné
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
turistického	turistický	k2eAgInSc2d1	turistický
ruchu	ruch	k1gInSc2	ruch
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
připojení	připojení	k1gNnSc6	připojení
jižního	jižní	k2eAgNnSc2d1	jižní
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
k	k	k7c3	k
Itálii	Itálie	k1gFnSc3	Itálie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Innsbruck	Innsbruck	k1gMnSc1	Innsbruck
centrem	centrum	k1gNnSc7	centrum
aktivistů	aktivista	k1gMnPc2	aktivista
toužících	toužící	k2eAgInPc2d1	toužící
po	po	k7c6	po
navrácení	navrácení	k1gNnSc6	navrácení
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
rakouskému	rakouský	k2eAgInSc3d1	rakouský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
konaly	konat	k5eAaImAgFnP	konat
Zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1964	[number]	k4	1964
a	a	k8xC	a
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
a	a	k8xC	a
zimní	zimní	k2eAgFnPc1d1	zimní
Paralympijské	paralympijský	k2eAgFnPc1d1	paralympijská
hry	hra	k1gFnPc1	hra
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
a	a	k8xC	a
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záměr	záměr	k1gInSc1	záměr
konání	konání	k1gNnSc2	konání
dalších	další	k2eAgFnPc2d1	další
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
měl	mít	k5eAaImAgInS	mít
sice	sice	k8xC	sice
podporu	podpora	k1gFnSc4	podpora
přilehlých	přilehlý	k2eAgFnPc2d1	přilehlá
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Innsbrucku	Innsbruck	k1gInSc2	Innsbruck
byli	být	k5eAaImAgMnP	být
proti	proti	k7c3	proti
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
do	do	k7c2	do
města	město	k1gNnSc2	město
Hallu	Hall	k1gInSc2	Hall
elektrická	elektrický	k2eAgFnSc1d1	elektrická
lanovka	lanovka	k1gFnSc1	lanovka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
stavbu	stavba	k1gFnSc4	stavba
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
starosta	starosta	k1gMnSc1	starosta
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
největší	veliký	k2eAgFnSc4d3	veliký
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
sloužil	sloužit	k5eAaImAgMnS	sloužit
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
mši	mše	k1gFnSc4	mše
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
na	na	k7c6	na
stadiónu	stadión	k1gInSc6	stadión
Bergisel	Bergisel	k1gInSc1	Bergisel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
stadiónu	stadión	k1gInSc6	stadión
konala	konat	k5eAaImAgFnS	konat
snowboardová	snowboardový	k2eAgFnSc1d1	snowboardová
soutěž	soutěž	k1gFnSc1	soutěž
Air	Air	k1gFnSc2	Air
<g/>
&	&	k?	&
<g/>
Style	styl	k1gInSc5	styl
Contest	Contest	k1gFnSc1	Contest
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
skončila	skončit	k5eAaPmAgFnS	skončit
úmrtím	úmrtí	k1gNnSc7	úmrtí
šesti	šest	k4xCc2	šest
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
nehodě	nehoda	k1gFnSc6	nehoda
byly	být	k5eAaImAgInP	být
stadión	stadión	k1gInSc4	stadión
i	i	k8xC	i
můstek	můstek	k1gInSc4	můstek
přebudovány	přebudován	k2eAgInPc1d1	přebudován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
provedl	provést	k5eAaPmAgMnS	provést
profesor	profesor	k1gMnSc1	profesor
Bodner	Bodner	k1gMnSc1	Bodner
na	na	k7c6	na
zdejší	zdejší	k2eAgFnSc6d1	zdejší
Univerzitní	univerzitní	k2eAgFnSc6d1	univerzitní
klinice	klinika	k1gFnSc6	klinika
českému	český	k2eAgMnSc3d1	český
prezidentu	prezident	k1gMnSc3	prezident
Václavu	Václav	k1gMnSc3	Václav
Havlovi	Havel	k1gMnSc3	Havel
operaci	operace	k1gFnSc4	operace
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
mu	on	k3xPp3gMnSc3	on
zachránil	zachránit	k5eAaPmAgMnS	zachránit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Architektura	architektura	k1gFnSc1	architektura
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
začala	začít	k5eAaPmAgFnS	začít
obnova	obnova	k1gFnSc1	obnova
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zásadní	zásadní	k2eAgFnPc1d1	zásadní
změny	změna	k1gFnPc1	změna
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
před	před	k7c7	před
Olympiádou	olympiáda	k1gFnSc7	olympiáda
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
a	a	k8xC	a
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
úplně	úplně	k6eAd1	úplně
nová	nový	k2eAgFnSc1d1	nová
čtvrť	čtvrť	k1gFnSc1	čtvrť
Reichenau	Reichenaus	k1gInSc2	Reichenaus
a	a	k8xC	a
Olympijská	olympijský	k2eAgFnSc1d1	olympijská
vesnička	vesnička	k1gFnSc1	vesnička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
byly	být	k5eAaImAgFnP	být
zahájeny	zahájen	k2eAgFnPc1d1	zahájena
stavby	stavba	k1gFnPc1	stavba
Kongresního	kongresní	k2eAgInSc2d1	kongresní
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
Koncertního	koncertní	k2eAgInSc2d1	koncertní
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
Veletržního	veletržní	k2eAgNnSc2d1	veletržní
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
s	s	k7c7	s
tyrolským	tyrolský	k2eAgInSc7d1	tyrolský
šarmem	šarm	k1gInSc7	šarm
v	v	k7c6	v
dostupnosti	dostupnost	k1gFnSc6	dostupnost
městského	městský	k2eAgNnSc2d1	Městské
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
Kongresového	kongresový	k2eAgNnSc2d1	Kongresové
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
architekti	architekt	k1gMnPc1	architekt
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
Marschalek	Marschalek	k1gInSc1	Marschalek
<g/>
,	,	kIx,	,
Ladstätter	Ladstätter	k1gMnSc1	Ladstätter
<g/>
,	,	kIx,	,
Gantar	Gantar	k1gMnSc1	Gantar
<g/>
,	,	kIx,	,
Prachensky	Prachensky	k1gMnSc1	Prachensky
<g/>
,	,	kIx,	,
Heiss	Heiss	k1gInSc1	Heiss
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architektky	architektka	k1gFnSc2	architektka
Z.	Z.	kA	Z.
Hadid	Hadid	k1gInSc4	Hadid
postaven	postavit	k5eAaPmNgInS	postavit
nový	nový	k2eAgInSc1d1	nový
můstek	můstek	k1gInSc1	můstek
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Bergisel	Bergisel	k1gInSc1	Bergisel
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
architekt	architekt	k1gMnSc1	architekt
Dominique	Dominique	k1gNnSc2	Dominique
Perrault	Perrault	k1gMnSc1	Perrault
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
radniční	radniční	k2eAgFnSc4d1	radniční
galerii	galerie	k1gFnSc4	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
lékařství	lékařství	k1gNnSc2	lékařství
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
založila	založit	k5eAaPmAgFnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
Lékařskou	lékařský	k2eAgFnSc4d1	lékařská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konala	konat	k5eAaImAgFnS	konat
Univerziáda	univerziáda	k1gFnSc1	univerziáda
(	(	kIx(	(
<g/>
Olympiáda	olympiáda	k1gFnSc1	olympiáda
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
hokeji	hokej	k1gInSc6	hokej
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
zde	zde	k6eAd1	zde
a	a	k8xC	a
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
nádraží	nádraží	k1gNnSc4	nádraží
Nordkettenbahn	Nordkettenbahna	k1gFnPc2	Nordkettenbahna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
zde	zde	k6eAd1	zde
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
,	,	kIx,	,
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
hokeji	hokej	k1gInSc6	hokej
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
zde	zde	k6eAd1	zde
a	a	k8xC	a
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konalo	konat	k5eAaImAgNnS	konat
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konaly	konat	k5eAaImAgInP	konat
první	první	k4xOgFnPc4	první
Zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Helblingův	Helblingův	k2eAgInSc1d1	Helblingův
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
Helblinghaus	Helblinghaus	k1gInSc1	Helblinghaus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
stříška	stříška	k1gFnSc1	stříška
(	(	kIx(	(
<g/>
Goldenes	Goldenes	k1gMnSc1	Goldenes
Dachl	Dachl	k1gMnSc1	Dachl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pozdně	pozdně	k6eAd1	pozdně
gotický	gotický	k2eAgInSc1d1	gotický
arkýř	arkýř	k1gInSc1	arkýř
císaře	císař	k1gMnSc2	císař
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
I.	I.	kA	I.
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1497-1500	[number]	k4	1497-1500
</s>
</p>
<p>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
dóm	dóm	k1gInSc1	dóm
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1717-1724	[number]	k4	1717-1724
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
staršího	starý	k2eAgInSc2d2	starší
kostela	kostel	k1gInSc2	kostel
ze	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Císařský	císařský	k2eAgInSc1d1	císařský
zámek	zámek	k1gInSc1	zámek
(	(	kIx(	(
<g/>
Hofburg	Hofburg	k1gInSc1	Hofburg
<g/>
)	)	kIx)	)
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
dvorním	dvorní	k2eAgInSc7d1	dvorní
kostelem	kostel	k1gInSc7	kostel
(	(	kIx(	(
<g/>
Hofkirche	Hofkirch	k1gFnPc1	Hofkirch
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
založil	založit	k5eAaPmAgMnS	založit
císař	císař	k1gMnSc1	císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
sídlo	sídlo	k1gNnSc4	sídlo
a	a	k8xC	a
rezidenci	rezidence	k1gFnSc4	rezidence
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1500	[number]	k4	1500
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
s	s	k7c7	s
parkem	park	k1gInSc7	park
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
barokním	barokní	k2eAgInSc6d1	barokní
stylu	styl	k1gInSc6	styl
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1760	[number]	k4	1760
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvorní	dvorní	k2eAgInSc1d1	dvorní
kostel	kostel	k1gInSc1	kostel
(	(	kIx(	(
<g/>
Hofkirche	Hofkirche	k1gInSc1	Hofkirche
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgNnSc4d1	renesanční
síňové	síňový	k2eAgNnSc4d1	síňové
trojlodí	trojlodí	k1gNnSc4	trojlodí
založil	založit	k5eAaPmAgMnS	založit
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
rezidenci	rezidence	k1gFnSc6	rezidence
císař	císař	k1gMnSc1	císař
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
Uvnitř	uvnitř	k7c2	uvnitř
kostela	kostel	k1gInSc2	kostel
stojí	stát	k5eAaImIp3nS	stát
náhrobek	náhrobek	k1gInSc1	náhrobek
císaře	císař	k1gMnSc2	císař
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
I.	I.	kA	I.
s	s	k7c7	s
plastikou	plastika	k1gFnSc7	plastika
sedícího	sedící	k2eAgInSc2d1	sedící
panovnického	panovnický	k2eAgInSc2d1	panovnický
páru	pár	k1gInSc2	pár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
umělecky	umělecky	k6eAd1	umělecky
kovanou	kovaný	k2eAgFnSc7d1	kovaná
mříží	mříž	k1gFnSc7	mříž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
chrámové	chrámový	k2eAgFnSc2d1	chrámová
lodi	loď	k1gFnSc2	loď
stojí	stát	k5eAaImIp3nP	stát
bronzové	bronzový	k2eAgFnPc1d1	bronzová
plastiky	plastika	k1gFnPc1	plastika
členů	člen	k1gMnPc2	člen
habsburského	habsburský	k2eAgInSc2d1	habsburský
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
několika	několik	k4yIc2	několik
dalších	další	k2eAgMnPc2d1	další
evropských	evropský	k2eAgMnPc2d1	evropský
panovníků	panovník	k1gMnPc2	panovník
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
manželek	manželka	k1gFnPc2	manželka
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
je	být	k5eAaImIp3nS	být
však	však	k9	však
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
ve	v	k7c6	v
Vídeňském	vídeňský	k2eAgNnSc6d1	Vídeňské
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Unikátní	unikátní	k2eAgFnSc1d1	unikátní
renesanční	renesanční	k2eAgFnSc1d1	renesanční
sochařská	sochařský	k2eAgFnSc1d1	sochařská
práce	práce	k1gFnSc1	práce
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
kovolitecké	kovolitecký	k2eAgFnSc2d1	kovolitecká
dílny	dílna	k1gFnSc2	dílna
sochaře	sochař	k1gMnSc2	sochař
Petra	Petr	k1gMnSc2	Petr
Vischera	Vischer	k1gMnSc2	Vischer
staršího	starší	k1gMnSc2	starší
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgFnSc1d1	barokní
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1725-1728	[number]	k4	1725-1728
</s>
</p>
<p>
<s>
Kamenný	kamenný	k2eAgInSc1d1	kamenný
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
modeloval	modelovat	k5eAaImAgInS	modelovat
C.	C.	kA	C.
Benedetti	Benedetť	k1gFnSc2	Benedetť
roku	rok	k1gInSc2	rok
1706	[number]	k4	1706
</s>
</p>
<p>
<s>
Premonstrátský	premonstrátský	k2eAgInSc1d1	premonstrátský
klášter	klášter	k1gInSc1	klášter
Wilten	Wilten	k2eAgInSc1d1	Wilten
s	s	k7c7	s
barokním	barokní	k2eAgInSc7d1	barokní
kostelem	kostel	k1gInSc7	kostel
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1751-1756	[number]	k4	1751-1756
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
raně	raně	k6eAd1	raně
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
kostelíka	kostelík	k1gInSc2	kostelík
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
románského	románský	k2eAgInSc2d1	románský
kláštera	klášter	k1gInSc2	klášter
ze	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Renesanční	renesanční	k2eAgInSc1d1	renesanční
zámek	zámek	k1gInSc1	zámek
Ambras	Ambras	k1gInSc1	Ambras
zbudoval	zbudovat	k5eAaPmAgMnS	zbudovat
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Tyrolský	tyrolský	k2eAgInSc4d1	tyrolský
na	na	k7c4	na
návrší	návrší	k1gNnSc4	návrší
JV	JV	kA	JV
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
přenesl	přenést	k5eAaPmAgInS	přenést
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
své	svůj	k3xOyFgFnPc4	svůj
umělecké	umělecký	k2eAgFnPc4d1	umělecká
sbírky	sbírka	k1gFnPc4	sbírka
z	z	k7c2	z
Křivoklátu	Křivoklát	k1gInSc2	Křivoklát
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
sídlí	sídlet	k5eAaImIp3nS	sídlet
dodnes	dodnes	k6eAd1	dodnes
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
vídeňského	vídeňský	k2eAgNnSc2d1	Vídeňské
Uměleckohistorického	uměleckohistorický	k2eAgNnSc2d1	Uměleckohistorické
muzea	muzeum	k1gNnSc2	muzeum
</s>
</p>
<p>
<s>
Lanovka	lanovka	k1gFnSc1	lanovka
vede	vést	k5eAaImIp3nS	vést
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
na	na	k7c4	na
Hungerburg	Hungerburg	k1gInSc4	Hungerburg
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
1800	[number]	k4	1800
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skokanský	skokanský	k2eAgInSc1d1	skokanský
můstek	můstek	k1gInSc1	můstek
na	na	k7c4	na
Bergisel	Bergisel	k1gInSc4	Bergisel
byl	být	k5eAaImAgInS	být
zbudován	zbudovat	k5eAaPmNgInS	zbudovat
pro	pro	k7c4	pro
zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
skokan	skokan	k1gMnSc1	skokan
Dalibor	Dalibor	k1gMnSc1	Dalibor
Motejlek	Motejlek	k1gMnSc1	Motejlek
získal	získat	k5eAaPmAgMnS	získat
jedinou	jediný	k2eAgFnSc4d1	jediná
(	(	kIx(	(
<g/>
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
<g/>
)	)	kIx)	)
medaili	medaile	k1gFnSc4	medaile
pro	pro	k7c4	pro
československou	československý	k2eAgFnSc4d1	Československá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
výpravu	výprava	k1gFnSc4	výprava
<g/>
;	;	kIx,	;
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
můstek	můstek	k1gInSc1	můstek
znám	znám	k2eAgInSc1d1	znám
z	z	k7c2	z
každoročního	každoroční	k2eAgNnSc2d1	každoroční
Turné	turné	k1gNnSc2	turné
čtyř	čtyři	k4xCgInPc2	čtyři
můstků	můstek	k1gInPc2	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc4d1	dnešní
architektonickou	architektonický	k2eAgFnSc4d1	architektonická
podobu	podoba	k1gFnSc4	podoba
navrhlo	navrhnout	k5eAaPmAgNnS	navrhnout
arch	arch	k1gInSc4	arch
<g/>
.	.	kIx.	.
studio	studio	k1gNnSc1	studio
Zaha	Zahum	k1gNnSc2	Zahum
Hadid	Hadida	k1gFnPc2	Hadida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Městské	městský	k2eAgFnPc1d1	městská
parní	parní	k2eAgFnPc1d1	parní
lázně	lázeň	k1gFnPc1	lázeň
Innsbruck	Innsbrucka	k1gFnPc2	Innsbrucka
<g/>
,	,	kIx,	,
památkově	památkově	k6eAd1	památkově
chráněná	chráněný	k2eAgFnSc1d1	chráněná
budova	budova	k1gFnSc1	budova
na	na	k7c4	na
Salurner	Salurner	k1gInSc4	Salurner
Straße	Straß	k1gFnSc2	Straß
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
tří	tři	k4xCgFnPc2	tři
svatých	svatá	k1gFnPc2	svatá
(	(	kIx(	(
<g/>
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
památkově	památkově	k6eAd1	památkově
chráněný	chráněný	k2eAgInSc4d1	chráněný
kostel	kostel	k1gInSc4	kostel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
během	během	k7c2	během
morové	morový	k2eAgFnSc2d1	morová
rány	rána	k1gFnSc2	rána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
(	(	kIx(	(
<g/>
1415	[number]	k4	1415
<g/>
–	–	k?	–
<g/>
1493	[number]	k4	1493
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
</s>
</p>
<p>
<s>
Hermann	Hermann	k1gMnSc1	Hermann
Buhl	Buhl	k1gMnSc1	Buhl
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
horolezec	horolezec	k1gMnSc1	horolezec
</s>
</p>
<p>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Josef	Josef	k1gMnSc1	Josef
Lotrinský	lotrinský	k2eAgMnSc1d1	lotrinský
(	(	kIx(	(
<g/>
1679	[number]	k4	1679
<g/>
–	–	k?	–
<g/>
1729	[number]	k4	1729
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
Štěpána	Štěpán	k1gMnSc2	Štěpán
Lotrinského	lotrinský	k2eAgMnSc2d1	lotrinský
</s>
</p>
<p>
<s>
Barbara	Barbara	k1gFnSc1	Barbara
Schettová	Schettová	k1gFnSc1	Schettová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Tyrolská	tyrolský	k2eAgFnSc1d1	tyrolská
(	(	kIx(	(
<g/>
1585	[number]	k4	1585
<g/>
–	–	k?	–
<g/>
1618	[number]	k4	1618
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římská	římský	k2eAgFnSc1d1	římská
císařovna	císařovna	k1gFnSc1	císařovna
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Matyáše	Matyáš	k1gMnSc2	Matyáš
Habsburského	habsburský	k2eAgInSc2d1	habsburský
</s>
</p>
<p>
<s>
Klaudie	Klaudie	k1gFnSc1	Klaudie
Felicitas	Felicitas	k1gFnSc2	Felicitas
Tyrolská	tyrolský	k2eAgFnSc1d1	tyrolská
(	(	kIx(	(
<g/>
1653	[number]	k4	1653
<g/>
–	–	k?	–
<g/>
1676	[number]	k4	1676
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
římská	římský	k2eAgFnSc1d1	římská
císařovna	císařovna	k1gFnSc1	císařovna
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Leopolda	Leopolda	k1gFnSc1	Leopolda
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgNnSc4d1	správní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Innsbruck	Innsbruck	k6eAd1	Innsbruck
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
do	do	k7c2	do
devíti	devět	k4xCc2	devět
katastrálních	katastrální	k2eAgFnPc2d1	katastrální
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
tyto	tento	k3xDgFnPc1	tento
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
menších	malý	k2eAgFnPc2d2	menší
částí	část	k1gFnPc2	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
jimi	on	k3xPp3gNnPc7	on
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Innsbruck-střed	Innsbrucktřed	k1gInSc1	Innsbruck-střed
<g/>
:	:	kIx,	:
Altstadt	Altstadt	k1gInSc1	Altstadt
<g/>
,	,	kIx,	,
Dreiheiligen	Dreiheiligen	k1gInSc1	Dreiheiligen
<g/>
,	,	kIx,	,
Saggen	Saggen	k1gInSc1	Saggen
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Nikolaus	Nikolaus	k1gInSc1	Nikolaus
<g/>
,	,	kIx,	,
Mariahilf	Mariahilf	k1gInSc1	Mariahilf
</s>
</p>
<p>
<s>
Wilten	Wilten	k2eAgMnSc1d1	Wilten
<g/>
:	:	kIx,	:
Mentlberg	Mentlberg	k1gMnSc1	Mentlberg
<g/>
,	,	kIx,	,
Sieglanger	Sieglanger	k1gMnSc1	Sieglanger
<g/>
,	,	kIx,	,
Wilten	Wilten	k2eAgMnSc1d1	Wilten
West	West	k1gMnSc1	West
</s>
</p>
<p>
<s>
Pradl	Pradnout	k5eAaPmAgInS	Pradnout
<g/>
:	:	kIx,	:
Pradler-Saggen	Pradler-Saggen	k1gInSc1	Pradler-Saggen
</s>
</p>
<p>
<s>
Reichenau	Reichenau	k6eAd1	Reichenau
</s>
</p>
<p>
<s>
Hötting	Hötting	k1gInSc1	Hötting
<g/>
:	:	kIx,	:
Höttinger	Höttinger	k1gInSc1	Höttinger
Au	au	k0	au
<g/>
,	,	kIx,	,
Hötting	Hötting	k1gInSc1	Hötting
West	West	k1gInSc1	West
<g/>
,	,	kIx,	,
Sadrach	Sadrach	k1gInSc1	Sadrach
<g/>
,	,	kIx,	,
Allerheiligen	Allerheiligen	k1gInSc1	Allerheiligen
<g/>
,	,	kIx,	,
Kranebitten	Kranebitten	k2eAgInSc1d1	Kranebitten
</s>
</p>
<p>
<s>
Hungerburg	Hungerburg	k1gMnSc1	Hungerburg
<g/>
/	/	kIx~	/
<g/>
Hoch-Innsbruck	Hoch-Innsbruck	k1gMnSc1	Hoch-Innsbruck
</s>
</p>
<p>
<s>
Mühlau	Mühlau	k6eAd1	Mühlau
</s>
</p>
<p>
<s>
Amras	Amras	k1gInSc1	Amras
<g/>
:	:	kIx,	:
Roßau	Roßaus	k1gInSc2	Roßaus
</s>
</p>
<p>
<s>
Arzl	Arzl	k1gMnSc1	Arzl
<g/>
:	:	kIx,	:
Neuarzl	Neuarzl	k1gMnSc1	Neuarzl
<g/>
,	,	kIx,	,
Olympisches	Olympisches	k1gMnSc1	Olympisches
Dorf	Dorf	k1gMnSc1	Dorf
</s>
</p>
<p>
<s>
Vill	Vill	k1gMnSc1	Vill
</s>
</p>
<p>
<s>
Igls	Igls	k6eAd1	Igls
</s>
</p>
<p>
<s>
==	==	k?	==
Klimatické	klimatický	k2eAgInPc1d1	klimatický
poměry	poměr	k1gInPc1	poměr
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
činí	činit	k5eAaImIp3nS	činit
8,6	[number]	k4	8,6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
911	[number]	k4	911
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
se	se	k3xPyFc4	se
na	na	k7c6	na
západě	západ	k1gInSc6	západ
města	město	k1gNnSc2	město
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Kranebitten	Kranebittno	k1gNnPc2	Kranebittno
nachází	nacházet	k5eAaImIp3nS	nacházet
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
přepravilo	přepravit	k5eAaPmAgNnS	přepravit
za	za	k7c4	za
rok	rok	k1gInSc4	rok
poprvé	poprvé	k6eAd1	poprvé
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
dálnici	dálnice	k1gFnSc6	dálnice
A12	A12	k1gMnSc1	A12
(	(	kIx(	(
<g/>
Inntalautobahn	Inntalautobahn	k1gMnSc1	Inntalautobahn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
město	město	k1gNnSc4	město
spojuje	spojovat	k5eAaImIp3nS	spojovat
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
S16	S16	k1gFnSc7	S16
Arlbergschnellstraße	Arlbergschnellstraße	k1gFnSc7	Arlbergschnellstraße
a	a	k8xC	a
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
s	s	k7c7	s
křižovatkou	křižovatka	k1gFnSc7	křižovatka
Autobahndreieck	Autobahndreieck	k1gMnSc1	Autobahndreieck
Rosenheim	Rosenheim	k1gMnSc1	Rosenheim
<g/>
,	,	kIx,	,
z	z	k7c2	z
které	který	k3yRgFnSc2	který
vychází	vycházet	k5eAaImIp3nS	vycházet
dálnice	dálnice	k1gFnPc4	dálnice
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
a	a	k8xC	a
Salzburgu	Salzburg	k1gInSc2	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Brennerský	Brennerský	k2eAgInSc1d1	Brennerský
průsmyk	průsmyk	k1gInSc1	průsmyk
s	s	k7c7	s
dálnicí	dálnice	k1gFnSc7	dálnice
A13	A13	k1gMnSc1	A13
(	(	kIx(	(
<g/>
Brenner	Brenner	k1gMnSc1	Brenner
Autobahn	Autobahn	k1gMnSc1	Autobahn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnSc1	silnice
B177	B177	k1gFnSc1	B177
(	(	kIx(	(
<g/>
Seefelder	Seefelder	k1gInSc1	Seefelder
Straße	Straß	k1gInSc2	Straß
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
Zirler	Zirler	k1gInSc4	Zirler
Berg	Berga	k1gFnPc2	Berga
do	do	k7c2	do
města	město	k1gNnSc2	město
Seefeld	Seefelda	k1gFnPc2	Seefelda
in	in	k?	in
Tirol	Tirola	k1gFnPc2	Tirola
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
přes	přes	k7c4	přes
Mittenwald	Mittenwald	k1gInSc4	Mittenwald
do	do	k7c2	do
německého	německý	k2eAgMnSc2d1	německý
Garmisch-Partenkirchen	Garmisch-Partenkirchen	k1gInSc4	Garmisch-Partenkirchen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
železniční	železniční	k2eAgFnSc7d1	železniční
křižovatkou	křižovatka	k1gFnSc7	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
Unterinntalbahn	Unterinntalbahna	k1gFnPc2	Unterinntalbahna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Westbahn	Westbahna	k1gFnPc2	Westbahna
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
přes	přes	k7c4	přes
Linec	Linec	k1gInSc4	Linec
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc4	Salzburg
<g/>
,	,	kIx,	,
Wörgl	Wörgl	k1gInSc4	Wörgl
a	a	k8xC	a
Innsbruck	Innsbruck	k1gInSc4	Innsbruck
do	do	k7c2	do
Curychu	Curych	k1gInSc2	Curych
<g/>
/	/	kIx~	/
<g/>
Basileje	Basilej	k1gFnSc2	Basilej
a	a	k8xC	a
Bregenzu	Bregenz	k1gInSc2	Bregenz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Innsbruck	Innsbrucka	k1gFnPc2	Innsbrucka
vychází	vycházet	k5eAaImIp3nS	vycházet
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
úsek	úsek	k1gInSc1	úsek
zvaný	zvaný	k2eAgInSc4d1	zvaný
Arlbergbahn	Arlbergbahn	k1gInSc4	Arlbergbahn
<g/>
.	.	kIx.	.
</s>
<s>
Východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
jezdí	jezdit	k5eAaImIp3nP	jezdit
po	po	k7c4	po
Westbahn	Westbahn	k1gNnSc4	Westbahn
také	také	k9	také
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Wörgl	Wörgla	k1gFnPc2	Wörgla
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
po	po	k7c6	po
trati	trať	k1gFnSc6	trať
Giselabahn	Giselabahna	k1gFnPc2	Giselabahna
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
Klagenfurt	Klagenfurt	k1gInSc1	Klagenfurt
a	a	k8xC	a
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
do	do	k7c2	do
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
mezi	mezi	k7c7	mezi
Innsbruckem	Innsbrucko	k1gNnSc7	Innsbrucko
a	a	k8xC	a
Wörglem	Wörglo	k1gNnSc7	Wörglo
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvytíženějších	vytížený	k2eAgFnPc2d3	nejvytíženější
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
(	(	kIx(	(
<g/>
až	až	k9	až
430	[number]	k4	430
vlaků	vlak	k1gInPc2	vlak
denně	denně	k6eAd1	denně
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
výstavby	výstavba	k1gFnSc2	výstavba
koridoru	koridor	k1gInSc2	koridor
TEN	ten	k3xDgInSc1	ten
(	(	kIx(	(
<g/>
Berlín	Berlín	k1gInSc1	Berlín
-	-	kIx~	-
Palermo	Palermo	k1gNnSc1	Palermo
<g/>
)	)	kIx)	)
její	její	k3xOp3gNnSc4	její
zečtyřkolejnění	zečtyřkolejnění	k1gNnSc4	zečtyřkolejnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
severojižním	severojižní	k2eAgInSc6d1	severojižní
směru	směr	k1gInSc6	směr
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
město	město	k1gNnSc4	město
trať	trať	k1gFnSc4	trať
Mnichov-Verona	Mnichov-Verona	k1gFnSc1	Mnichov-Verona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
Innsbrucku	Innsbruck	k1gInSc2	Innsbruck
na	na	k7c4	na
jih	jih	k1gInSc4	jih
nazývá	nazývat	k5eAaImIp3nS	nazývat
Brennerbahn	Brennerbahn	k1gMnSc1	Brennerbahn
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
Brennerský	Brennerský	k2eAgInSc4d1	Brennerský
průsmyk	průsmyk	k1gInSc4	průsmyk
<g/>
.	.	kIx.	.
</s>
<s>
Severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
vede	vést	k5eAaImIp3nS	vést
trať	trať	k1gFnSc1	trať
Mittenwaldbahn	Mittenwaldbahn	k1gInSc4	Mittenwaldbahn
do	do	k7c2	do
německého	německý	k2eAgMnSc2d1	německý
Garmisch-Partenkirchen	Garmisch-Partenkirchen	k1gInSc4	Garmisch-Partenkirchen
</s>
</p>
<p>
<s>
Uzlem	uzel	k1gInSc7	uzel
těchto	tento	k3xDgFnPc2	tento
tratí	trať	k1gFnPc2	trať
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Aalborg	Aalborg	k1gInSc1	Aalborg
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
</s>
</p>
<p>
<s>
Freiburg	Freiburg	k1gInSc1	Freiburg
im	im	k?	im
Breisgau	Breisgaus	k1gInSc2	Breisgaus
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
</s>
</p>
<p>
<s>
Grenoble	Grenoble	k1gInSc1	Grenoble
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
</s>
</p>
<p>
<s>
Krakov	Krakov	k1gInSc1	Krakov
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
New	New	k?	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
<g/>
,	,	kIx,	,
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
Tbilisi	Tbilisi	k1gNnSc1	Tbilisi
<g/>
,	,	kIx,	,
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc4d1	další
informace	informace	k1gFnPc4	informace
==	==	k?	==
</s>
</p>
<p>
<s>
Nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
565	[number]	k4	565
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
2641	[number]	k4	2641
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
NUTS	NUTS	kA	NUTS
III	III	kA	III
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
AT332	AT332	k1gMnPc2	AT332
INNSBRUCK	INNSBRUCK	kA	INNSBRUCK
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Innsbruck	Innsbrucka	k1gFnPc2	Innsbrucka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Historie	historie	k1gFnSc1	historie
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
:	:	kIx,	:
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
dopravní	dopravní	k2eAgInSc1d1	dopravní
systém	systém	k1gInSc1	systém
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
</s>
</p>
