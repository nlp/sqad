<s>
Python	Python	k1gMnSc1	Python
je	být	k5eAaImIp3nS	být
vysokoúrovňový	vysokoúrovňový	k2eAgInSc1d1	vysokoúrovňový
skriptovací	skriptovací	k2eAgInSc1d1	skriptovací
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Guido	Guido	k1gNnSc4	Guido
van	vana	k1gFnPc2	vana
Rossum	Rossum	k1gNnSc4	Rossum
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
dynamickou	dynamický	k2eAgFnSc4d1	dynamická
kontrolu	kontrola	k1gFnSc4	kontrola
datových	datový	k2eAgInPc2d1	datový
typů	typ	k1gInPc2	typ
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
různá	různý	k2eAgNnPc4d1	různé
programovací	programovací	k2eAgNnPc4d1	programovací
paradigmata	paradigma	k1gNnPc4	paradigma
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
objektově	objektově	k6eAd1	objektově
orientovaného	orientovaný	k2eAgInSc2d1	orientovaný
<g/>
,	,	kIx,	,
imperativního	imperativní	k2eAgInSc2d1	imperativní
<g/>
,	,	kIx,	,
procedurálního	procedurální	k2eAgInSc2d1	procedurální
nebo	nebo	k8xC	nebo
funkcionálního	funkcionální	k2eAgInSc2d1	funkcionální
<g/>
.	.	kIx.	.
</s>
<s>
Python	Python	k1gMnSc1	Python
je	být	k5eAaImIp3nS	být
vyvíjen	vyvíjen	k2eAgMnSc1d1	vyvíjen
jako	jako	k8xC	jako
open	open	k1gMnSc1	open
source	source	k1gMnSc1	source
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zdarma	zdarma	k6eAd1	zdarma
nabízí	nabízet	k5eAaImIp3nS	nabízet
instalační	instalační	k2eAgInPc4d1	instalační
balíky	balík	k1gInPc4	balík
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
běžných	běžný	k2eAgFnPc2d1	běžná
platforem	platforma	k1gFnPc2	platforma
(	(	kIx(	(
<g/>
Unix	Unix	k1gInSc1	Unix
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
Mac	Mac	kA	Mac
OS	OS	kA	OS
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
distribucí	distribuce	k1gFnPc2	distribuce
systému	systém	k1gInSc2	systém
Linux	Linux	kA	Linux
je	být	k5eAaImIp3nS	být
Python	Python	k1gMnSc1	Python
součástí	součást	k1gFnPc2	součást
základní	základní	k2eAgFnSc2d1	základní
instalace	instalace	k1gFnSc2	instalace
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
implementován	implementován	k2eAgInSc1d1	implementován
aplikační	aplikační	k2eAgInSc4d1	aplikační
server	server	k1gInSc4	server
Zope	Zop	k1gFnSc2	Zop
<g/>
,	,	kIx,	,
instalátor	instalátor	k1gMnSc1	instalátor
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
konfiguračních	konfigurační	k2eAgInPc2d1	konfigurační
nástrojů	nástroj	k1gInPc2	nástroj
Linuxové	linuxový	k2eAgFnSc2d1	linuxová
distribuce	distribuce	k1gFnSc2	distribuce
firmy	firma	k1gFnSc2	firma
Red	Red	k1gFnSc2	Red
Hat	hat	k0	hat
<g/>
.	.	kIx.	.
</s>
<s>
Python	Python	k1gMnSc1	Python
je	být	k5eAaImIp3nS	být
dynamický	dynamický	k2eAgInSc4d1	dynamický
interpretovaný	interpretovaný	k2eAgInSc4d1	interpretovaný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
zařazován	zařazovat	k5eAaImNgInS	zařazovat
mezi	mezi	k7c4	mezi
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
skriptovací	skriptovací	k2eAgInPc4d1	skriptovací
jazyky	jazyk	k1gInPc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
možnosti	možnost	k1gFnPc1	možnost
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
větší	veliký	k2eAgInPc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Python	Python	k1gMnSc1	Python
byl	být	k5eAaImAgMnS	být
navržen	navrhnout	k5eAaPmNgMnS	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
tvorbu	tvorba	k1gFnSc4	tvorba
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
<g/>
,	,	kIx,	,
plnohodnotných	plnohodnotný	k2eAgFnPc2d1	plnohodnotná
aplikací	aplikace	k1gFnPc2	aplikace
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
grafického	grafický	k2eAgNnSc2d1	grafické
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
například	například	k6eAd1	například
wxPython	wxPython	k1gInSc4	wxPython
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
využívá	využívat	k5eAaPmIp3nS	využívat
wxWidgets	wxWidgets	k6eAd1	wxWidgets
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
PySide	PySid	k1gInSc5	PySid
a	a	k8xC	a
PyQT	PyQT	k1gMnPc7	PyQT
pro	pro	k7c4	pro
Qt	Qt	k1gFnSc4	Qt
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
PyGTK	PyGTK	k1gFnSc1	PyGTK
pro	pro	k7c4	pro
GTK	GTK	kA	GTK
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Python	Python	k1gMnSc1	Python
je	být	k5eAaImIp3nS	být
hybridní	hybridní	k2eAgInSc4d1	hybridní
jazyk	jazyk	k1gInSc4	jazyk
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
víceparadigmatický	víceparadigmatický	k2eAgMnSc1d1	víceparadigmatický
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
programů	program	k1gInPc2	program
používat	používat	k5eAaImF	používat
nejen	nejen	k6eAd1	nejen
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgNnSc4d1	orientované
paradigma	paradigma	k1gNnSc4	paradigma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
procedurální	procedurální	k2eAgMnSc1d1	procedurální
a	a	k8xC	a
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
i	i	k9	i
funkcionální	funkcionální	k2eAgInSc1d1	funkcionální
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
komu	kdo	k3yQnSc3	kdo
co	co	k3yRnSc1	co
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
úlohu	úloha	k1gFnSc4	úloha
hodí	hodit	k5eAaImIp3nS	hodit
nejlépe	dobře	k6eAd3	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Python	Python	k1gMnSc1	Python
má	mít	k5eAaImIp3nS	mít
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
vynikající	vynikající	k2eAgFnPc1d1	vynikající
vyjadřovací	vyjadřovací	k2eAgFnPc1d1	vyjadřovací
schopnosti	schopnost	k1gFnPc1	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc1	kód
programu	program	k1gInSc2	program
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
jazyky	jazyk	k1gInPc7	jazyk
krátký	krátký	k2eAgInSc4d1	krátký
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
čitelný	čitelný	k2eAgInSc1d1	čitelný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
význačným	význačný	k2eAgFnPc3d1	význačná
vlastnostem	vlastnost	k1gFnPc3	vlastnost
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
patří	patřit	k5eAaImIp3nS	patřit
jeho	jeho	k3xOp3gFnSc4	jeho
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
dokonce	dokonce	k9	dokonce
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvhodnějších	vhodný	k2eAgInPc2d3	nejvhodnější
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
pro	pro	k7c4	pro
začátečníky	začátečník	k1gMnPc4	začátečník
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
silných	silný	k2eAgInPc2d1	silný
inspiračních	inspirační	k2eAgInPc2d1	inspirační
zdrojů	zdroj	k1gInPc2	zdroj
byl	být	k5eAaImAgInS	být
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
jazyk	jazyk	k1gInSc1	jazyk
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
a	a	k8xC	a
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
začátečníky	začátečník	k1gMnPc4	začátečník
přímo	přímo	k6eAd1	přímo
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Python	Python	k1gMnSc1	Python
ale	ale	k9	ale
současně	současně	k6eAd1	současně
bourá	bourat	k5eAaImIp3nS	bourat
zažitou	zažitý	k2eAgFnSc4d1	zažitá
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
není	být	k5eNaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
praxi	praxe	k1gFnSc4	praxe
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnou	podstatný	k2eAgFnSc7d1	podstatná
měrou	míra	k1gFnSc7wR	míra
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přispívá	přispívat	k5eAaImIp3nS	přispívat
čistota	čistota	k1gFnSc1	čistota
a	a	k8xC	a
jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
syntaxe	syntaxe	k1gFnSc1	syntaxe
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
jazyka	jazyk	k1gInSc2	jazyk
hodně	hodně	k6eAd1	hodně
dbá	dbát	k5eAaImIp3nS	dbát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
definici	definice	k1gFnSc3	definice
bloků	blok	k1gInPc2	blok
se	se	k3xPyFc4	se
v	v	k7c6	v
Pythonu	Python	k1gMnSc6	Python
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
odsazování	odsazování	k1gNnSc1	odsazování
<g/>
.	.	kIx.	.
</s>
<s>
Význačnou	význačný	k2eAgFnSc7d1	význačná
vlastností	vlastnost	k1gFnSc7	vlastnost
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
je	být	k5eAaImIp3nS	být
produktivnost	produktivnost	k1gFnSc4	produktivnost
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
rychlosti	rychlost	k1gFnSc2	rychlost
psaní	psaní	k1gNnSc2	psaní
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Týká	týkat	k5eAaImIp3nS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
jak	jak	k8xS	jak
nejjednodušších	jednoduchý	k2eAgInPc2d3	nejjednodušší
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aplikací	aplikace	k1gFnPc2	aplikace
velmi	velmi	k6eAd1	velmi
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
programů	program	k1gInPc2	program
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
projevuje	projevovat	k5eAaImIp3nS	projevovat
především	především	k9	především
stručností	stručnost	k1gFnSc7	stručnost
zápisu	zápis	k1gInSc2	zápis
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
velkých	velký	k2eAgFnPc2d1	velká
aplikací	aplikace	k1gFnPc2	aplikace
je	být	k5eAaImIp3nS	být
produktivnost	produktivnost	k1gFnSc1	produktivnost
podpořena	podpořen	k2eAgFnSc1d1	podpořena
rysy	rys	k1gInPc4	rys
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
programování	programování	k1gNnSc6	programování
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
přirozená	přirozený	k2eAgFnSc1d1	přirozená
podpora	podpora	k1gFnSc1	podpora
jmenných	jmenný	k2eAgInPc2d1	jmenný
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc1	používání
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
standardně	standardně	k6eAd1	standardně
dodávané	dodávaný	k2eAgInPc1d1	dodávaný
prostředky	prostředek	k1gInPc1	prostředek
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
testů	test	k1gInPc2	test
(	(	kIx(	(
<g/>
unit	unit	k2eAgInSc1d1	unit
testing	testing	k1gInSc1	testing
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
produktivností	produktivnost	k1gFnSc7	produktivnost
souvisí	souviset	k5eAaImIp3nS	souviset
dostupnost	dostupnost	k1gFnSc1	dostupnost
a	a	k8xC	a
snadná	snadný	k2eAgFnSc1d1	snadná
použitelnost	použitelnost	k1gFnSc1	použitelnost
široké	široký	k2eAgFnSc2d1	široká
škály	škála	k1gFnSc2	škála
knihovních	knihovní	k2eAgInPc2d1	knihovní
modulů	modul	k1gInPc2	modul
<g/>
,	,	kIx,	,
umožňujících	umožňující	k2eAgInPc2d1	umožňující
snadné	snadný	k2eAgNnSc4d1	snadné
řešení	řešení	k1gNnSc4	řešení
úloh	úloha	k1gFnPc2	úloha
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Python	Python	k1gMnSc1	Python
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
vkládá	vkládat	k5eAaImIp3nS	vkládat
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
aplikací	aplikace	k1gFnPc2	aplikace
(	(	kIx(	(
<g/>
embedding	embedding	k1gInSc1	embedding
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
jejich	jejich	k3xOp3gInSc1	jejich
skriptovací	skriptovací	k2eAgInSc1d1	skriptovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
lze	lze	k6eAd1	lze
aplikacím	aplikace	k1gFnPc3	aplikace
psaným	psaný	k2eAgFnPc3d1	psaná
v	v	k7c6	v
kompilovaných	kompilovaný	k2eAgInPc6d1	kompilovaný
programovacích	programovací	k2eAgInPc6d1	programovací
jazycích	jazyk	k1gInPc6	jazyk
dodávat	dodávat	k5eAaImF	dodávat
chybějící	chybějící	k2eAgFnSc4d1	chybějící
pružnost	pružnost	k1gFnSc4	pružnost
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
aplikace	aplikace	k1gFnPc1	aplikace
nebo	nebo	k8xC	nebo
aplikační	aplikační	k2eAgFnPc1d1	aplikační
knihovny	knihovna	k1gFnPc1	knihovna
mohou	moct	k5eAaImIp3nP	moct
naopak	naopak	k6eAd1	naopak
implementovat	implementovat	k5eAaImF	implementovat
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
umožní	umožnit	k5eAaPmIp3nS	umožnit
jejich	jejich	k3xOp3gNnSc4	jejich
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
roli	role	k1gFnSc6	role
pythonovského	pythonovský	k2eAgInSc2d1	pythonovský
modulu	modul	k1gInSc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
pythonovský	pythonovský	k2eAgInSc1d1	pythonovský
program	program	k1gInSc1	program
je	on	k3xPp3gMnPc4	on
může	moct	k5eAaImIp3nS	moct
využívat	využívat	k5eAaPmF	využívat
jako	jako	k9	jako
modul	modul	k1gInSc1	modul
dostupný	dostupný	k2eAgInSc1d1	dostupný
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
extending	extending	k1gInSc4	extending
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
sekce	sekce	k1gFnSc1	sekce
Spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
aplikacemi	aplikace	k1gFnPc7	aplikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Programování	programování	k1gNnSc1	programování
v	v	k7c6	v
Pythonu	Python	k1gMnSc6	Python
klade	klást	k5eAaImIp3nS	klást
velký	velký	k2eAgInSc4d1	velký
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
produktivitu	produktivita	k1gFnSc4	produktivita
práce	práce	k1gFnSc2	práce
programátora	programátor	k1gMnSc2	programátor
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
návrhu	návrh	k1gInSc2	návrh
jazyka	jazyk	k1gInSc2	jazyk
jsou	být	k5eAaImIp3nP	být
shrnuty	shrnout	k5eAaPmNgFnP	shrnout
ve	v	k7c4	v
filosofii	filosofie	k1gFnSc4	filosofie
Pythonu	Python	k1gMnSc3	Python
<g/>
.	.	kIx.	.
</s>
<s>
Klasický	klasický	k2eAgMnSc1d1	klasický
Python	Python	k1gMnSc1	Python
je	být	k5eAaImIp3nS	být
implementován	implementovat	k5eAaImNgMnS	implementovat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
C	C	kA	C
(	(	kIx(	(
<g/>
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
jako	jako	k9	jako
CPython	CPython	k1gInSc1	CPython
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
probíhá	probíhat	k5eAaImIp3nS	probíhat
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnPc1	verze
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
jsou	být	k5eAaImIp3nP	být
zveřejňovány	zveřejňován	k2eAgFnPc4d1	zveřejňována
jak	jak	k6eAd1	jak
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
přeložených	přeložený	k2eAgInPc2d1	přeložený
instalačních	instalační	k2eAgInPc2d1	instalační
balíků	balík	k1gInPc2	balík
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
cílové	cílový	k2eAgFnPc4d1	cílová
platformy	platforma	k1gFnPc4	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Dostupnost	dostupnost	k1gFnSc1	dostupnost
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnSc2	vlastnost
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
zabudovat	zabudovat	k5eAaPmF	zabudovat
interpret	interpret	k1gMnSc1	interpret
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
aplikace	aplikace	k1gFnSc2	aplikace
psané	psaný	k2eAgFnSc2d1	psaná
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
C	C	kA	C
nebo	nebo	k8xC	nebo
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
zabudovaný	zabudovaný	k2eAgMnSc1d1	zabudovaný
interpret	interpret	k1gMnSc1	interpret
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
pak	pak	k6eAd1	pak
představuje	představovat	k5eAaImIp3nS	představovat
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
pružné	pružný	k2eAgNnSc4d1	pružné
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
funkčnosti	funkčnost	k1gFnSc2	funkčnost
výsledné	výsledný	k2eAgFnSc2d1	výsledná
aplikace	aplikace	k1gFnSc2	aplikace
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
projekt	projekt	k1gInSc1	projekt
pro	pro	k7c4	pro
užší	úzký	k2eAgFnSc4d2	užší
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
C	C	kA	C
<g/>
++	++	k?	++
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Boost	Boost	k1gInSc1	Boost
<g/>
.	.	kIx.	.
<g/>
Python	Python	k1gMnSc1	Python
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
–	–	k?	–
a	a	k8xC	a
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
obecně	obecně	k6eAd1	obecně
vysokému	vysoký	k2eAgInSc3d1	vysoký
výkonu	výkon	k1gInSc3	výkon
aplikací	aplikace	k1gFnPc2	aplikace
psaných	psaný	k2eAgFnPc2d1	psaná
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
C	C	kA	C
–	–	k?	–
je	být	k5eAaImIp3nS	být
CPython	CPython	k1gNnSc4	CPython
nejpoužívanější	používaný	k2eAgFnSc7d3	nejpoužívanější
implementací	implementace	k1gFnSc7	implementace
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
implementace	implementace	k1gFnSc1	implementace
Pythonu	Python	k1gMnSc6	Python
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Java	Jav	k1gInSc2	Jav
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Jython	Jython	k1gInSc1	Jython
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc4	kód
napsaný	napsaný	k2eAgInSc4d1	napsaný
v	v	k7c6	v
Jythonu	Jython	k1gInSc6	Jython
běží	běžet	k5eAaImIp3nS	běžet
v	v	k7c6	v
JVM	JVM	kA	JVM
Javy	Jav	k1gMnPc7	Jav
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
všechny	všechen	k3xTgFnPc4	všechen
knihovny	knihovna	k1gFnPc4	knihovna
prostředí	prostředí	k1gNnSc2	prostředí
Java	Jav	k1gInSc2	Jav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Javě	Java	k1gFnSc6	Java
lze	lze	k6eAd1	lze
naopak	naopak	k6eAd1	naopak
používat	používat	k5eAaImF	používat
všechny	všechen	k3xTgFnPc4	všechen
knihovny	knihovna	k1gFnPc4	knihovna
napsané	napsaný	k2eAgFnPc4d1	napsaná
v	v	k7c6	v
Jythonu	Jython	k1gInSc6	Jython
<g/>
.	.	kIx.	.
</s>
<s>
Implementace	implementace	k1gFnSc1	implementace
Jython	Jythona	k1gFnPc2	Jythona
zaostává	zaostávat	k5eAaImIp3nS	zaostávat
za	za	k7c7	za
implementací	implementace	k1gFnSc7	implementace
CPython	CPythona	k1gFnPc2	CPythona
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
význačných	význačný	k2eAgFnPc2d1	význačná
vlastností	vlastnost	k1gFnPc2	vlastnost
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
se	se	k3xPyFc4	se
však	však	k9	však
objevuje	objevovat	k5eAaImIp3nS	objevovat
již	již	k6eAd1	již
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
Python	Python	k1gMnSc1	Python
2.2	[number]	k4	2.2
a	a	k8xC	a
stabilní	stabilní	k2eAgFnSc1d1	stabilní
verze	verze	k1gFnSc1	verze
Jython	Jython	k1gNnSc1	Jython
2.2	[number]	k4	2.2
<g/>
.1	.1	k4	.1
byla	být	k5eAaImAgFnS	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
používání	používání	k1gNnSc6	používání
Jythonu	Jython	k1gInSc2	Jython
z	z	k7c2	z
javovských	javovský	k2eAgFnPc2d1	javovská
aplikací	aplikace	k1gFnPc2	aplikace
navíc	navíc	k6eAd1	navíc
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
otázka	otázka	k1gFnSc1	otázka
nedostupnosti	nedostupnost	k1gFnSc2	nedostupnost
novějších	nový	k2eAgInPc2d2	novější
rysů	rys	k1gInPc2	rys
tak	tak	k6eAd1	tak
palčivá	palčivý	k2eAgFnSc1d1	palčivá
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
Jythonu	Jython	k1gInSc2	Jython
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	se	k3xPyFc4	se
na	na	k7c6	na
dalších	další	k2eAgFnPc6d1	další
verzích	verze	k1gFnPc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1	aktuální
vývoj	vývoj	k1gInSc1	vývoj
viz	vidět	k5eAaImRp2nS	vidět
stránky	stránka	k1gFnPc4	stránka
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
se	se	k3xPyFc4	se
na	na	k7c4	na
implementaci	implementace	k1gFnSc4	implementace
Pythonu	Python	k1gMnSc3	Python
pro	pro	k7c4	pro
prostředí	prostředí	k1gNnSc4	prostředí
.	.	kIx.	.
<g/>
NET	NET	kA	NET
<g/>
/	/	kIx~	/
<g/>
Mono	mono	k6eAd1	mono
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
IronPython	IronPythona	k1gFnPc2	IronPythona
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výhody	výhoda	k1gFnPc4	výhoda
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Python	Python	k1gMnSc1	Python
tímto	tento	k3xDgInSc7	tento
stává	stávat	k5eAaImIp3nS	stávat
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jazyků	jazyk	k1gInPc2	jazyk
pro	pro	k7c4	pro
platformu	platforma	k1gFnSc4	platforma
.	.	kIx.	.
<g/>
NET	NET	kA	NET
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
současně	současně	k6eAd1	současně
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
přímo	přímo	k6eAd1	přímo
využívat	využívat	k5eAaPmF	využívat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
jazycích	jazyk	k1gInPc6	jazyk
platformy	platforma	k1gFnSc2	platforma
.	.	kIx.	.
<g/>
NET	NET	kA	NET
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
významu	význam	k1gInSc3	význam
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
platformě	platforma	k1gFnSc3	platforma
.	.	kIx.	.
<g/>
NET	NET	kA	NET
přikládá	přikládat	k5eAaImIp3nS	přikládat
firma	firma	k1gFnSc1	firma
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
význam	význam	k1gInSc1	význam
implementace	implementace	k1gFnSc2	implementace
IronPython	IronPythona	k1gFnPc2	IronPythona
dále	daleko	k6eAd2	daleko
poroste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vlastnostem	vlastnost	k1gFnPc3	vlastnost
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
lze	lze	k6eAd1	lze
také	také	k9	také
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
implementace	implementace	k1gFnSc2	implementace
IronPython	IronPythona	k1gFnPc2	IronPythona
stane	stanout	k5eAaPmIp3nS	stanout
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
podporovanou	podporovaný	k2eAgFnSc7d1	podporovaná
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnPc1	verze
IronPython	IronPython	k1gNnSc1	IronPython
2.0	[number]	k4	2.0
funkčně	funkčně	k6eAd1	funkčně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
Python	Python	k1gMnSc1	Python
2.5	[number]	k4	2.5
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
všech	všecek	k3xTgMnPc2	všecek
podstatných	podstatný	k2eAgMnPc2d1	podstatný
rysů	rys	k1gInPc2	rys
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Verze	verze	k1gFnSc1	verze
Python	Python	k1gMnSc1	Python
2.6	[number]	k4	2.6
má	mít	k5eAaImIp3nS	mít
pomoci	pomoct	k5eAaPmF	pomoct
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
mezi	mezi	k7c7	mezi
Python	Python	k1gMnSc1	Python
2.5	[number]	k4	2.5
a	a	k8xC	a
Python	Python	k1gMnSc1	Python
3.0	[number]	k4	3.0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Python	Python	k1gMnSc1	Python
3.0	[number]	k4	3.0
realizuje	realizovat	k5eAaBmIp3nS	realizovat
větší	veliký	k2eAgFnSc1d2	veliký
změny	změna	k1gFnPc1	změna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Negativně	negativně	k6eAd1	negativně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vnímána	vnímán	k2eAgFnSc1d1	vnímána
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
implementace	implementace	k1gFnSc1	implementace
IronPython	IronPythona	k1gFnPc2	IronPythona
je	být	k5eAaImIp3nS	být
vyvíjena	vyvíjet	k5eAaImNgFnS	vyvíjet
firmou	firma	k1gFnSc7	firma
Microsoft	Microsoft	kA	Microsoft
pod	pod	k7c7	pod
Microsoft	Microsoft	kA	Microsoft
Public	publicum	k1gNnPc2	publicum
License	Licens	k1gMnSc2	Licens
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
aplikací	aplikace	k1gFnPc2	aplikace
napsaných	napsaný	k2eAgFnPc2d1	napsaná
v	v	k7c6	v
Pythonu	Python	k1gMnSc6	Python
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgMnSc1d1	dobrý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
výkonově	výkonově	k6eAd1	výkonově
kritické	kritický	k2eAgFnPc1d1	kritická
knihovny	knihovna	k1gFnPc1	knihovna
jsou	být	k5eAaImIp3nP	být
implementovány	implementovat	k5eAaImNgFnP	implementovat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
C	C	kA	C
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yRgMnSc7	který
Python	Python	k1gMnSc1	Python
výborně	výborně	k6eAd1	výborně
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
samotný	samotný	k2eAgInSc1d1	samotný
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
interpretovanými	interpretovaný	k2eAgInPc7d1	interpretovaný
jazyky	jazyk	k1gInPc7	jazyk
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
3	[number]	k4	3
až	až	k9	až
5	[number]	k4	5
krát	krát	k6eAd1	krát
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
než	než	k8xS	než
PHP	PHP	kA	PHP
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pro	pro	k7c4	pro
Python	Python	k1gMnSc1	Python
navíc	navíc	k6eAd1	navíc
existuje	existovat	k5eAaImIp3nS	existovat
snadno	snadno	k6eAd1	snadno
použitelná	použitelný	k2eAgFnSc1d1	použitelná
knihovna	knihovna	k1gFnSc1	knihovna
Psyco	Psyco	k6eAd1	Psyco
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
transparentně	transparentně	k6eAd1	transparentně
optimalizuje	optimalizovat	k5eAaBmIp3nS	optimalizovat
kód	kód	k1gInSc4	kód
Pythonu	Python	k1gMnSc3	Python
na	na	k7c4	na
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
operace	operace	k1gFnPc1	operace
jsou	být	k5eAaImIp3nP	být
pomocí	pomoc	k1gFnSc7	pomoc
Psyco	Psyco	k6eAd1	Psyco
urychleny	urychlit	k5eAaPmNgInP	urychlit
až	až	k9	až
řádově	řádově	k6eAd1	řádově
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
Python	Python	k1gMnSc1	Python
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
vkládá	vkládat	k5eAaImIp3nS	vkládat
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
jejich	jejich	k3xOp3gInSc1	jejich
skriptovací	skriptovací	k2eAgInSc1d1	skriptovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
najít	najít	k5eAaPmF	najít
např.	např.	kA	např.
v	v	k7c6	v
3D	[number]	k4	3D
programu	program	k1gInSc2	program
Blender	Blender	k1gInSc1	Blender
<g/>
,	,	kIx,	,
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
hře	hra	k1gFnSc6	hra
Civilization	Civilization	k1gInSc1	Civilization
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
v	v	k7c6	v
kancelářském	kancelářský	k2eAgInSc6d1	kancelářský
balíku	balík	k1gInSc6	balík
OpenOffice	OpenOffice	k1gFnSc2	OpenOffice
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
v	v	k7c6	v
textovém	textový	k2eAgInSc6d1	textový
editoru	editor	k1gInSc6	editor
Vim	Vim	k?	Vim
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
alternativně	alternativně	k6eAd1	alternativně
použít	použít	k5eAaPmF	použít
jako	jako	k8xC	jako
skriptovací	skriptovací	k2eAgInSc1d1	skriptovací
jazyk	jazyk	k1gInSc1	jazyk
aplikace	aplikace	k1gFnSc2	aplikace
GIMP	GIMP	kA	GIMP
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
pythonovská	pythonovský	k2eAgNnPc1d1	pythonovské
aplikační	aplikační	k2eAgNnPc1d1	aplikační
rozhraní	rozhraní	k1gNnPc1	rozhraní
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
projektů	projekt	k1gInPc2	projekt
–	–	k?	–
například	například	k6eAd1	například
pro	pro	k7c4	pro
ImageMagick	ImageMagick	k1gInSc4	ImageMagick
<g/>
.	.	kIx.	.
</s>
<s>
Varianta	varianta	k1gFnSc1	varianta
Jython	Jythona	k1gFnPc2	Jythona
(	(	kIx(	(
<g/>
implementace	implementace	k1gFnSc1	implementace
Pythonu	Python	k1gMnSc6	Python
v	v	k7c6	v
Javě	Jav	k1gInSc6	Jav
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
skriptovací	skriptovací	k2eAgInSc4d1	skriptovací
jazyk	jazyk	k1gInSc4	jazyk
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
skripty	skript	k1gInPc4	skript
v	v	k7c6	v
Javě	Java	k1gFnSc6	Java
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
editoru	editor	k1gInSc6	editor
jEdit	jEdit	k5eAaPmF	jEdit
<g/>
.	.	kIx.	.
</s>
<s>
Ukázkový	ukázkový	k2eAgInSc1d1	ukázkový
program	program	k1gInSc1	program
Hello	Hello	k1gNnSc1	Hello
world	world	k6eAd1	world
vypadá	vypadat	k5eAaImIp3nS	vypadat
velmi	velmi	k6eAd1	velmi
jednoduše	jednoduše	k6eAd1	jednoduše
<g/>
:	:	kIx,	:
Program	program	k1gInSc1	program
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
obsahu	obsah	k1gInSc2	obsah
kruhu	kruh	k1gInSc2	kruh
ze	z	k7c2	z
zadaného	zadaný	k2eAgInSc2d1	zadaný
poloměru	poloměr	k1gInSc2	poloměr
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
vypadat	vypadat	k5eAaImF	vypadat
například	například	k6eAd1	například
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Výpočet	výpočet	k1gInSc1	výpočet
faktoriálu	faktoriál	k1gInSc2	faktoriál
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
C	C	kA	C
<g/>
:	:	kIx,	:
Každá	každý	k3xTgFnSc1	každý
proměnná	proměnná	k1gFnSc1	proměnná
se	se	k3xPyFc4	se
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
pojmenovaný	pojmenovaný	k2eAgInSc4d1	pojmenovaný
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc4	jméno
proměnné	proměnná	k1gFnSc2	proměnná
je	být	k5eAaImIp3nS	být
svázáno	svázat	k5eAaPmNgNnS	svázat
s	s	k7c7	s
jinak	jinak	k6eAd1	jinak
bezejmenným	bezejmenný	k2eAgInSc7d1	bezejmenný
objektem	objekt	k1gInSc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Příkaz	příkaz	k1gInSc1	příkaz
přiřazení	přiřazení	k1gNnSc2	přiřazení
nezajistí	zajistit	k5eNaPmIp3nS	zajistit
okopírování	okopírování	k1gNnSc1	okopírování
hodnoty	hodnota	k1gFnSc2	hodnota
navázaného	navázaný	k2eAgInSc2d1	navázaný
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Provede	provést	k5eAaPmIp3nS	provést
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
svázání	svázání	k1gNnSc1	svázání
nového	nový	k2eAgNnSc2d1	nové
jména	jméno	k1gNnSc2	jméno
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
objektem	objekt	k1gInSc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc4	jméno
a	a	k8xC	a
i	i	k8xC	i
b	b	k?	b
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
svázána	svázat	k5eAaPmNgFnS	svázat
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
objektem	objekt	k1gInSc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
objekt	objekt	k1gInSc1	objekt
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
měněn	měněn	k2eAgMnSc1d1	měněn
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
změna	změna	k1gFnSc1	změna
provedená	provedený	k2eAgFnSc1d1	provedená
přes	přes	k7c4	přes
jméno	jméno	k1gNnSc4	jméno
b	b	k?	b
projeví	projevit	k5eAaPmIp3nS	projevit
i	i	k9	i
při	při	k7c6	při
následném	následný	k2eAgInSc6d1	následný
přístupu	přístup	k1gInSc6	přístup
přes	přes	k7c4	přes
jméno	jméno	k1gNnSc4	jméno
a.	a.	k?	a.
Příklad	příklad	k1gInSc1	příklad
–	–	k?	–
zrušíme	zrušit	k5eAaPmIp1nP	zrušit
první	první	k4xOgInSc4	první
prvek	prvek	k1gInSc4	prvek
seznamu	seznam	k1gInSc2	seznam
přes	přes	k7c4	přes
jméno	jméno	k1gNnSc4	jméno
b	b	k?	b
a	a	k8xC	a
zobrazíme	zobrazit	k5eAaPmIp1nP	zobrazit
obsah	obsah	k1gInSc4	obsah
seznamu	seznam	k1gInSc2	seznam
přes	přes	k7c4	přes
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
<g/>
:	:	kIx,	:
Zobrazí	zobrazit	k5eAaPmIp3nS	zobrazit
se	se	k3xPyFc4	se
Funkce	funkce	k1gFnSc1	funkce
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
běžný	běžný	k2eAgInSc4d1	běžný
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
zavolána	zavolán	k2eAgFnSc1d1	zavolána
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
manipulovat	manipulovat	k5eAaImF	manipulovat
<g/>
,	,	kIx,	,
ukládat	ukládat	k5eAaImF	ukládat
do	do	k7c2	do
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
manipuluje	manipulovat	k5eAaImIp3nS	manipulovat
se	se	k3xPyFc4	se
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
objektem	objekt	k1gInSc7	objekt
funkce	funkce	k1gFnSc2	funkce
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
svázat	svázat	k5eAaPmF	svázat
i	i	k9	i
nové	nový	k2eAgNnSc4d1	nové
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
kdykoliv	kdykoliv	k6eAd1	kdykoliv
předefinovat	předefinovat	k5eAaPmF	předefinovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
složených	složený	k2eAgFnPc2d1	složená
datových	datový	k2eAgFnPc2d1	datová
struktur	struktura	k1gFnPc2	struktura
se	se	k3xPyFc4	se
ukládají	ukládat	k5eAaImIp3nP	ukládat
odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
objekty	objekt	k1gInPc1	objekt
samotné	samotný	k2eAgInPc1d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
objektu	objekt	k1gInSc2	objekt
není	být	k5eNaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
odkaz	odkaz	k1gInSc4	odkaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
svázán	svázat	k5eAaPmNgMnS	svázat
až	až	k6eAd1	až
s	s	k7c7	s
odkazovaným	odkazovaný	k2eAgInSc7d1	odkazovaný
objektem	objekt	k1gInSc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
seznamu	seznam	k1gInSc2	seznam
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
současně	současně	k6eAd1	současně
uložit	uložit	k5eAaPmF	uložit
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
objekty	objekt	k1gInPc4	objekt
libovolného	libovolný	k2eAgInSc2d1	libovolný
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
z	z	k7c2	z
technického	technický	k2eAgNnSc2d1	technické
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
odkazy	odkaz	k1gInPc4	odkaz
všechny	všechen	k3xTgMnPc4	všechen
stejného	stejný	k2eAgInSc2d1	stejný
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
interního	interní	k2eAgInSc2d1	interní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
typu	typ	k1gInSc3	typ
odkazovaného	odkazovaný	k2eAgInSc2d1	odkazovaný
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
lze	lze	k6eAd1	lze
tedy	tedy	k8xC	tedy
seznam	seznam	k1gInSc4	seznam
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
homogenní	homogenní	k2eAgInSc4d1	homogenní
datový	datový	k2eAgInSc4d1	datový
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uživatelského	uživatelský	k2eAgInSc2d1	uživatelský
pohledu	pohled	k1gInSc2	pohled
to	ten	k3xDgNnSc1	ten
vypadá	vypadat	k5eAaImIp3nS	vypadat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
můžeme	moct	k5eAaImIp1nP	moct
vkládat	vkládat	k5eAaImF	vkládat
hodnoty	hodnota	k1gFnPc4	hodnota
různého	různý	k2eAgInSc2d1	různý
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
–	–	k?	–
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
se	se	k3xPyFc4	se
nevkládají	vkládat	k5eNaImIp3nP	vkládat
hodnoty	hodnota	k1gFnPc1	hodnota
daných	daný	k2eAgInPc2d1	daný
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
beztypové	beztypový	k2eAgInPc1d1	beztypový
odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
příslušné	příslušný	k2eAgInPc4d1	příslušný
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
při	při	k7c6	při
deklaraci	deklarace	k1gFnSc6	deklarace
proměnné	proměnná	k1gFnSc2	proměnná
uvádí	uvádět	k5eAaImIp3nS	uvádět
souvislost	souvislost	k1gFnSc1	souvislost
jména	jméno	k1gNnSc2	jméno
proměnné	proměnná	k1gFnSc2	proměnná
s	s	k7c7	s
typem	typ	k1gInSc7	typ
ukládané	ukládaný	k2eAgFnSc2d1	ukládaná
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Python	Python	k1gMnSc1	Python
je	být	k5eAaImIp3nS	být
proměnná	proměnný	k2eAgFnSc1d1	proměnná
jen	jen	k6eAd1	jen
pojmenovaným	pojmenovaný	k2eAgInSc7d1	pojmenovaný
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
vázán	vázat	k5eAaImNgMnS	vázat
na	na	k7c4	na
odkazovaný	odkazovaný	k2eAgInSc4d1	odkazovaný
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
na	na	k7c4	na
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Potřeba	potřeba	k1gFnSc1	potřeba
deklarace	deklarace	k1gFnSc2	deklarace
proměnné	proměnná	k1gFnSc2	proměnná
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
určení	určení	k1gNnSc2	určení
souvisejícího	související	k2eAgInSc2d1	související
typu	typ	k1gInSc2	typ
dat	datum	k1gNnPc2	datum
tedy	tedy	k9	tedy
odpadá	odpadat	k5eAaImIp3nS	odpadat
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
<g/>
,	,	kIx,	,
či	či	k8xC	či
neexistence	neexistence	k1gFnSc1	neexistence
jména	jméno	k1gNnSc2	jméno
přímo	přímo	k6eAd1	přímo
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
či	či	k8xC	či
neexistencí	neexistence	k1gFnSc7	neexistence
hodnotového	hodnotový	k2eAgInSc2d1	hodnotový
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
deklarace	deklarace	k1gFnSc2	deklarace
proměnné	proměnná	k1gFnSc2	proměnná
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
popisu	popis	k1gInSc2	popis
existence	existence	k1gFnSc2	existence
související	související	k2eAgFnPc4d1	související
hodnoty	hodnota	k1gFnPc4	hodnota
tedy	tedy	k8xC	tedy
rovněž	rovněž	k9	rovněž
odpadá	odpadat	k5eAaImIp3nS	odpadat
<g/>
.	.	kIx.	.
</s>
<s>
Proměnná	proměnná	k1gFnSc1	proměnná
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
pojmenovaný	pojmenovaný	k2eAgInSc4d1	pojmenovaný
odkaz	odkaz	k1gInSc4	odkaz
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc1	jméno
objeví	objevit	k5eAaPmIp3nS	objevit
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
přiřazovacího	přiřazovací	k2eAgInSc2d1	přiřazovací
příkazu	příkaz	k1gInSc2	příkaz
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
proměnné	proměnná	k1gFnSc2	proměnná
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
později	pozdě	k6eAd2	pozdě
svázáno	svázat	k5eAaPmNgNnS	svázat
dalším	další	k2eAgNnSc7d1	další
přiřazením	přiřazení	k1gNnSc7	přiřazení
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
objektem	objekt	k1gInSc7	objekt
zcela	zcela	k6eAd1	zcela
jiného	jiný	k2eAgInSc2d1	jiný
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
běžné	běžný	k2eAgFnPc4d1	běžná
praktiky	praktika	k1gFnPc4	praktika
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
objektu	objekt	k1gInSc2	objekt
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
založení	založení	k1gNnSc3	založení
používaných	používaný	k2eAgFnPc2d1	používaná
členských	členský	k2eAgFnPc2d1	členská
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
obrat	obrat	k1gInSc1	obrat
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Python	Python	k1gMnSc1	Python
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k9	jako
užitečná	užitečný	k2eAgFnSc1d1	užitečná
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
jako	jako	k8xC	jako
nutnost	nutnost	k1gFnSc4	nutnost
<g/>
.	.	kIx.	.
</s>
<s>
Členské	členský	k2eAgFnPc1d1	členská
proměnné	proměnná	k1gFnPc1	proměnná
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
proměnné	proměnná	k1gFnPc1	proměnná
uvnitř	uvnitř	k7c2	uvnitř
objektu	objekt	k1gInSc2	objekt
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
až	až	k9	až
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
techniky	technika	k1gFnPc1	technika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
prostředky	prostředek	k1gInPc7	prostředek
jazyka	jazyk	k1gInSc2	jazyk
zamezit	zamezit	k5eAaPmF	zamezit
možnost	možnost	k1gFnSc4	možnost
dodatečného	dodatečný	k2eAgNnSc2d1	dodatečné
přidávání	přidávání	k1gNnSc2	přidávání
členských	členský	k2eAgFnPc2d1	členská
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
operacích	operace	k1gFnPc6	operace
nad	nad	k7c7	nad
objekty	objekt	k1gInPc7	objekt
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
silná	silný	k2eAgFnSc1d1	silná
typová	typový	k2eAgFnSc1d1	typová
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kompilovaných	kompilovaný	k2eAgInPc2d1	kompilovaný
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
ale	ale	k9	ale
provádí	provádět	k5eAaImIp3nS	provádět
až	až	k9	až
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
kladl	klást	k5eAaImAgMnS	klást
a	a	k8xC	a
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
operátory	operátor	k1gInPc1	operátor
nebyly	být	k5eNaImAgInP	být
vázány	vázat	k5eAaImNgInP	vázat
na	na	k7c4	na
specifické	specifický	k2eAgInPc4d1	specifický
datové	datový	k2eAgInPc4d1	datový
typy	typ	k1gInPc4	typ
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
možné	možný	k2eAgNnSc1d1	možné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přípustnost	přípustnost	k1gFnSc1	přípustnost
použití	použití	k1gNnSc2	použití
operátoru	operátor	k1gInSc2	operátor
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
operandy	operand	k1gInPc4	operand
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
až	až	k9	až
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
například	například	k6eAd1	například
následující	následující	k2eAgFnSc4d1	následující
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
používá	používat	k5eAaImIp3nS	používat
operátor	operátor	k1gInSc1	operátor
plus	plus	k1gInSc1	plus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
předat	předat	k5eAaPmF	předat
jednak	jednak	k8xC	jednak
číselné	číselný	k2eAgFnPc1d1	číselná
a	a	k8xC	a
jednak	jednak	k8xC	jednak
řetězcové	řetězcový	k2eAgInPc4d1	řetězcový
argumenty	argument	k1gInPc4	argument
<g/>
:	:	kIx,	:
Nejde	jít	k5eNaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
zajímavou	zajímavý	k2eAgFnSc4d1	zajímavá
hříčku	hříčka	k1gFnSc4	hříčka
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgFnSc2d1	běžná
pythonovské	pythonovský	k2eAgFnSc2d1	pythonovská
funkce	funkce	k1gFnSc2	funkce
tím	ten	k3xDgNnSc7	ten
získávají	získávat	k5eAaImIp3nP	získávat
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
generické	generický	k2eAgNnSc1d1	generické
programování	programování	k1gNnSc1	programování
<g/>
.	.	kIx.	.
</s>
<s>
Interpret	interpret	k1gMnSc1	interpret
jazyka	jazyk	k1gInSc2	jazyk
Python	Python	k1gMnSc1	Python
můžeme	moct	k5eAaImIp1nP	moct
spustit	spustit	k5eAaPmF	spustit
v	v	k7c6	v
interaktivním	interaktivní	k2eAgInSc6d1	interaktivní
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
režim	režim	k1gInSc1	režim
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
především	především	k9	především
pro	pro	k7c4	pro
rychlé	rychlý	k2eAgInPc4d1	rychlý
pokusy	pokus	k1gInPc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Řádkový	řádkový	k2eAgInSc1d1	řádkový
vstup	vstup	k1gInSc1	vstup
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
uvozen	uvozen	k2eAgInSc4d1	uvozen
znaky	znak	k1gInPc4	znak
>>>	>>>	k?	>>>
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
očekáván	očekáván	k2eAgInSc1d1	očekáván
pokračovací	pokračovací	k2eAgInSc1d1	pokračovací
řádek	řádek	k1gInSc1	řádek
zápisu	zápis	k1gInSc2	zápis
dosud	dosud	k6eAd1	dosud
nedokončené	dokončený	k2eNgFnPc1d1	nedokončená
konstrukce	konstrukce	k1gFnPc1	konstrukce
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
vstupní	vstupní	k2eAgInSc4d1	vstupní
řádek	řádek	k1gInSc4	řádek
uvozen	uvozen	k2eAgInSc4d1	uvozen
znaky	znak	k1gInPc4	znak
....	....	k?	....
Dokončení	dokončení	k1gNnSc3	dokončení
zápisu	zápis	k1gInSc2	zápis
konstrukce	konstrukce	k1gFnSc2	konstrukce
vyjadřujeme	vyjadřovat	k5eAaImIp1nP	vyjadřovat
v	v	k7c6	v
interaktivním	interaktivní	k2eAgInSc6d1	interaktivní
režimu	režim	k1gInSc6	režim
zadáním	zadání	k1gNnSc7	zadání
prázdného	prázdný	k2eAgInSc2d1	prázdný
řádku	řádek	k1gInSc2	řádek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
interaktivním	interaktivní	k2eAgInSc6d1	interaktivní
režimu	režim	k1gInSc6	režim
většinou	většinou	k6eAd1	většinou
nepoužíváme	používat	k5eNaImIp1nP	používat
příkaz	příkaz	k1gInSc4	příkaz
print	print	k1gInSc1	print
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
nic	nic	k3yNnSc1	nic
nám	my	k3xPp1nPc3	my
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
nebrání	bránit	k5eNaImIp3nS	bránit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
zobrazit	zobrazit	k5eAaPmF	zobrazit
obsah	obsah	k1gInSc4	obsah
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
za	za	k7c4	za
úvodní	úvodní	k2eAgInPc4d1	úvodní
znaky	znak	k1gInPc4	znak
zapsat	zapsat	k5eAaPmF	zapsat
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Proměnná	proměnná	k1gFnSc1	proměnná
_	_	kIx~	_
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
poslední	poslední	k2eAgFnSc4d1	poslední
takto	takto	k6eAd1	takto
použitou	použitý	k2eAgFnSc4d1	použitá
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Python	Python	k1gMnSc1	Python
je	být	k5eAaImIp3nS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
pragmatičnost	pragmatičnost	k1gFnSc4	pragmatičnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vývoj	vývoj	k1gInSc1	vývoj
jeho	jeho	k3xOp3gFnPc2	jeho
verzí	verze	k1gFnPc2	verze
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
evoluční	evoluční	k2eAgFnSc1d1	evoluční
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeným	přirozený	k2eAgInSc7d1	přirozený
důsledkem	důsledek	k1gInSc7	důsledek
takového	takový	k3xDgInSc2	takový
přístupu	přístup	k1gInSc2	přístup
je	být	k5eAaImIp3nS	být
i	i	k9	i
zpětné	zpětný	k2eAgNnSc1d1	zpětné
hodnocení	hodnocení	k1gNnSc1	hodnocení
dobrých	dobrý	k2eAgFnPc2d1	dobrá
a	a	k8xC	a
horších	zlý	k2eAgFnPc2d2	horší
vlastností	vlastnost	k1gFnPc2	vlastnost
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
Python	Python	k1gMnSc1	Python
3000	[number]	k4	3000
(	(	kIx(	(
<g/>
Py	Py	k1gFnSc6	Py
<g/>
3	[number]	k4	3
<g/>
k	k	k7c3	k
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
základ	základ	k1gInSc4	základ
vývoje	vývoj	k1gInSc2	vývoj
přelomové	přelomový	k2eAgFnSc2d1	přelomová
verze	verze	k1gFnSc2	verze
Python	Python	k1gMnSc1	Python
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Stabilní	stabilní	k2eAgFnSc1d1	stabilní
verze	verze	k1gFnSc1	verze
Python	Python	k1gMnSc1	Python
3.0	[number]	k4	3.0
byla	být	k5eAaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
v	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zpětně	zpětně	k6eAd1	zpětně
nekompatibilní	kompatibilní	k2eNgNnSc1d1	nekompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
Přechodovou	přechodový	k2eAgFnSc7d1	přechodová
verzí	verze	k1gFnSc7	verze
mezi	mezi	k7c7	mezi
Python	Python	k1gMnSc1	Python
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
x	x	k?	x
a	a	k8xC	a
Python	Python	k1gMnSc1	Python
3.0	[number]	k4	3.0
představuje	představovat	k5eAaImIp3nS	představovat
Python	Python	k1gMnSc1	Python
2.7	[number]	k4	2.7
(	(	kIx(	(
<g/>
varování	varování	k1gNnSc1	varování
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
syntaxe	syntax	k1gFnSc2	syntax
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nebude	být	k5eNaImBp3nS	být
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
3.0	[number]	k4	3.0
platná	platný	k2eAgFnSc1d1	platná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byly	být	k5eAaImAgFnP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
nástroje	nástroj	k1gInPc1	nástroj
pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
konverze	konverze	k1gFnSc2	konverze
starších	starý	k2eAgInPc2d2	starší
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
textů	text	k1gInPc2	text
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
pro	pro	k7c4	pro
verzi	verze	k1gFnSc4	verze
Python	Python	k1gMnSc1	Python
3.0	[number]	k4	3.0
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejviditelnějších	viditelný	k2eAgFnPc2d3	nejviditelnější
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
Python	Python	k1gMnSc1	Python
3.0	[number]	k4	3.0
(	(	kIx(	(
<g/>
pragmatický	pragmatický	k2eAgInSc4d1	pragmatický
pohled	pohled	k1gInSc4	pohled
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
prostého	prostý	k2eAgMnSc2d1	prostý
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
převedení	převedení	k1gNnSc1	převedení
příkazu	příkaz	k1gInSc2	příkaz
print	printa	k1gFnPc2	printa
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
print	print	k1gInSc1	print
"	"	kIx"	"
<g/>
hello	hello	k1gNnSc1	hello
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
neplatná	platný	k2eNgFnSc1d1	neplatná
<g/>
,	,	kIx,	,
správný	správný	k2eAgInSc1d1	správný
zápis	zápis	k1gInSc1	zápis
je	být	k5eAaImIp3nS	být
print	print	k1gInSc4	print
<g/>
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hello	hello	k1gNnSc1	hello
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc7d1	významná
změnou	změna	k1gFnSc7	změna
je	být	k5eAaImIp3nS	být
důsledné	důsledný	k2eAgNnSc1d1	důsledné
oddělení	oddělení	k1gNnSc1	oddělení
abstrakcí	abstrakce	k1gFnPc2	abstrakce
řetězec	řetězec	k1gInSc4	řetězec
a	a	k8xC	a
posloupnost	posloupnost	k1gFnSc4	posloupnost
bajtů	bajt	k1gInPc2	bajt
<g/>
.	.	kIx.	.
</s>
<s>
Řetězce	řetězec	k1gInPc1	řetězec
se	se	k3xPyFc4	se
důsledně	důsledně	k6eAd1	důsledně
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
typ	typ	k1gInSc4	typ
unicode	unicod	k1gInSc5	unicod
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hruška	hruška	k1gFnSc1	hruška
<g/>
"	"	kIx"	"
bude	být	k5eAaImBp3nS	být
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
s	s	k7c7	s
dnešní	dnešní	k2eAgFnSc7d1	dnešní
u	u	k7c2	u
<g/>
"	"	kIx"	"
<g/>
hruška	hruška	k1gFnSc1	hruška
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
posloupnosti	posloupnost	k1gFnPc4	posloupnost
bajtů	bajt	k1gInPc2	bajt
je	být	k5eAaImIp3nS	být
zaveden	zavést	k5eAaPmNgInS	zavést
nový	nový	k2eAgInSc1d1	nový
typ	typ	k1gInSc1	typ
bytes	bytesa	k1gFnPc2	bytesa
(	(	kIx(	(
<g/>
immutable	immutable	k6eAd1	immutable
<g/>
)	)	kIx)	)
a	a	k8xC	a
bytearray	bytearra	k2eAgFnPc1d1	bytearra
(	(	kIx(	(
<g/>
mutable	mutable	k6eAd1	mutable
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vzájemném	vzájemný	k2eAgInSc6d1	vzájemný
převodu	převod	k1gInSc6	převod
mezi	mezi	k7c7	mezi
řetězcem	řetězec	k1gInSc7	řetězec
a	a	k8xC	a
posloupností	posloupnost	k1gFnSc7	posloupnost
bajtů	bajt	k1gInPc2	bajt
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vždy	vždy	k6eAd1	vždy
uvádět	uvádět	k5eAaImF	uvádět
požadované	požadovaný	k2eAgNnSc4d1	požadované
kódování	kódování	k1gNnSc4	kódování
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
důslednému	důsledný	k2eAgNnSc3d1	důsledné
vyřešení	vyřešení	k1gNnSc3	vyřešení
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
projevovaly	projevovat	k5eAaImAgInP	projevovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
znaky	znak	k1gInPc7	znak
národních	národní	k2eAgFnPc2d1	národní
abeced	abeceda	k1gFnPc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
raw_input	raw_input	k1gInSc1	raw_input
bude	být	k5eAaImBp3nS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
input	input	k1gInSc4	input
(	(	kIx(	(
<g/>
současný	současný	k2eAgInSc4d1	současný
input	input	k1gInSc4	input
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
iterativní	iterativní	k2eAgFnPc4d1	iterativní
varianty	varianta	k1gFnPc4	varianta
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
metod	metoda	k1gFnPc2	metoda
nahradí	nahradit	k5eAaPmIp3nP	nahradit
svoje	svůj	k3xOyFgMnPc4	svůj
předchůdce	předchůdce	k1gMnPc4	předchůdce
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
vracely	vracet	k5eAaImAgInP	vracet
seznamy	seznam	k1gInPc1	seznam
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
například	například	k6eAd1	například
funkce	funkce	k1gFnSc1	funkce
range	rang	k1gFnSc2	rang
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
dnešní	dnešní	k2eAgFnSc2d1	dnešní
xrange	xrang	k1gFnSc2	xrang
<g/>
,	,	kIx,	,
analogicky	analogicky	k6eAd1	analogicky
tomu	ten	k3xDgNnSc3	ten
bude	být	k5eAaImBp3nS	být
s	s	k7c7	s
file	file	k1gFnPc7	file
<g/>
.	.	kIx.	.
<g/>
readlines	readlines	k1gInSc1	readlines
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
file	file	k1gInSc1	file
<g/>
.	.	kIx.	.
<g/>
xreadlines	xreadlines	k1gInSc1	xreadlines
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
další	další	k2eAgFnPc4d1	další
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
vracející	vracející	k2eAgInSc4d1	vracející
list	list	k1gInSc4	list
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
vracet	vracet	k5eAaImF	vracet
iterátor	iterátor	k1gInSc4	iterátor
–	–	k?	–
např.	např.	kA	např.
dict	dict	k1gInSc1	dict
<g/>
.	.	kIx.	.
<g/>
keys	keys	k1gInSc1	keys
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Další	další	k2eAgFnSc7d1	další
viditelnější	viditelný	k2eAgFnSc7d2	viditelnější
změnou	změna	k1gFnSc7	změna
je	být	k5eAaImIp3nS	být
chování	chování	k1gNnSc1	chování
operátoru	operátor	k1gInSc2	operátor
dělení	dělení	k1gNnSc2	dělení
/	/	kIx~	/
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
s	s	k7c7	s
celými	celý	k2eAgNnPc7d1	celé
čísly	číslo	k1gNnPc7	číslo
<g/>
:	:	kIx,	:
Zmizí	zmizet	k5eAaPmIp3nP	zmizet
dict	dict	k5eAaPmF	dict
<g/>
.	.	kIx.	.
<g/>
has_key	has_key	k1gInPc4	has_key
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zápis	zápis	k1gInSc1	zápis
množin	množina	k1gFnPc2	množina
se	se	k3xPyFc4	se
zjednoduší	zjednodušit	k5eAaPmIp3nP	zjednodušit
<g/>
,	,	kIx,	,
set	set	k1gInSc1	set
<g/>
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
napsat	napsat	k5eAaBmF	napsat
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
přibude	přibýt	k5eAaPmIp3nS	přibýt
řetězcům	řetězec	k1gInPc3	řetězec
nová	nový	k2eAgFnSc1d1	nová
metoda	metoda	k1gFnSc1	metoda
format	format	k1gInSc1	format
jako	jako	k8xC	jako
alternativa	alternativa	k1gFnSc1	alternativa
ke	k	k7c3	k
současnému	současný	k2eAgNnSc3d1	současné
formátování	formátování	k1gNnSc3	formátování
řetězců	řetězec	k1gInPc2	řetězec
procentovou	procentový	k2eAgFnSc7d1	procentová
notací	notace	k1gFnSc7	notace
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
porušení	porušení	k1gNnSc2	porušení
zpětné	zpětný	k2eAgFnSc2d1	zpětná
kompatibility	kompatibilita	k1gFnSc2	kompatibilita
je	být	k5eAaImIp3nS	být
zavedení	zavedení	k1gNnSc1	zavedení
nové	nový	k2eAgFnSc2d1	nová
syntaxe	syntax	k1gFnSc2	syntax
pro	pro	k7c4	pro
oktalová	oktalový	k2eAgNnPc4d1	oktalový
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
doposud	doposud	k6eAd1	doposud
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
s	s	k7c7	s
nulou	nula	k1gFnSc7	nula
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
0	[number]	k4	0
<g/>
777	[number]	k4	777
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
formát	formát	k1gInSc1	formát
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
neplatným	platný	k2eNgInSc7d1	neplatný
a	a	k8xC	a
nový	nový	k2eAgMnSc1d1	nový
bude	být	k5eAaImBp3nS	být
analogický	analogický	k2eAgInSc1d1	analogický
se	s	k7c7	s
zápisem	zápis	k1gInSc7	zápis
hexadecimálních	hexadecimální	k2eAgNnPc2d1	hexadecimální
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
x	x	k?	x
<g/>
1	[number]	k4	1
<g/>
ff	ff	kA	ff
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Správný	správný	k2eAgInSc1d1	správný
zápis	zápis	k1gInSc1	zápis
bude	být	k5eAaImBp3nS	být
tedy	tedy	k9	tedy
0	[number]	k4	0
<g/>
o	o	k7c4	o
<g/>
777	[number]	k4	777
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zapisovat	zapisovat	k5eAaImF	zapisovat
i	i	k8xC	i
čísla	číslo	k1gNnPc4	číslo
v	v	k7c6	v
binární	binární	k2eAgFnSc6d1	binární
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
>>>	>>>	k?	>>>
012	[number]	k4	012
File	File	k1gNnPc2	File
"	"	kIx"	"
<g/>
<stdin>
"	"	kIx"	"
<g/>
,	,	kIx,	,
line	linout	k5eAaImIp3nS	linout
1	[number]	k4	1
012	[number]	k4	012
^	^	kIx~	^
SyntaxError	SyntaxError	k1gInSc1	SyntaxError
<g/>
:	:	kIx,	:
invalid	invalid	k1gInSc1	invalid
token	token	k1gInSc1	token
>>>	>>>	k?	>>>
10	[number]	k4	10
==	==	k?	==
0	[number]	k4	0
<g/>
xa	xa	k?	xa
==	==	k?	==
0	[number]	k4	0
<g/>
o	o	k7c4	o
<g/>
12	[number]	k4	12
==	==	k?	==
0	[number]	k4	0
<g/>
b	b	k?	b
<g/>
1010	[number]	k4	1010
True	True	k1gInSc1	True
Podrobnější	podrobný	k2eAgInSc4d2	podrobnější
seznam	seznam	k1gInSc4	seznam
změn	změna	k1gFnPc2	změna
najdete	najít	k5eAaPmIp2nP	najít
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
v	v	k7c6	v
dokumentu	dokument	k1gInSc6	dokument
What	Whata	k1gFnPc2	Whata
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
New	New	k1gFnSc7	New
In	In	k1gMnSc1	In
Python	Python	k1gMnSc1	Python
3.0	[number]	k4	3.0
<g/>
.	.	kIx.	.
</s>
