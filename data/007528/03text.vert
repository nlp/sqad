<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Reduta	reduta	k1gFnSc1	reduta
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
Reduta	reduta	k1gFnSc1	reduta
je	být	k5eAaImIp3nS	být
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1608	[number]	k4	1608
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
nejstarší	starý	k2eAgInSc1d3	nejstarší
divadelní	divadelní	k2eAgFnSc7d1	divadelní
budovou	budova	k1gFnSc7	budova
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1767	[number]	k4	1767
zde	zde	k6eAd1	zde
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
malý	malý	k2eAgMnSc1d1	malý
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
není	být	k5eNaImIp3nS	být
zařízená	zařízený	k2eAgFnSc1d1	zařízená
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
stálé	stálý	k2eAgFnSc2d1	stálá
divadelní	divadelní	k2eAgFnSc2d1	divadelní
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
tak	tak	k6eAd1	tak
různé	různý	k2eAgInPc4d1	různý
hostující	hostující	k2eAgInPc4d1	hostující
soubory	soubor	k1gInPc4	soubor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgInSc1d1	divadelní
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
redutní	redutní	k2eAgInSc1d1	redutní
sál	sál	k1gInSc1	sál
a	a	k8xC	a
salonek	salonek	k1gInSc1	salonek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
budova	budova	k1gFnSc1	budova
z	z	k7c2	z
komplexu	komplex	k1gInSc2	komplex
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
nemá	mít	k5eNaImIp3nS	mít
jméno	jméno	k1gNnSc4	jméno
po	po	k7c4	po
známé	známý	k2eAgFnPc4d1	známá
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
.	.	kIx.	.
</s>
