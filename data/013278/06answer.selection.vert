<s>
Kytice	kytice	k1gFnSc1	kytice
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vydaná	vydaný	k2eAgFnSc1d1	vydaná
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Kytice	kytice	k1gFnSc1	kytice
z	z	k7c2	z
pověstí	pověst	k1gFnPc2	pověst
národních	národní	k2eAgFnPc2d1	národní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stěžejní	stěžejní	k2eAgNnSc1d1	stěžejní
dílo	dílo	k1gNnSc1	dílo
Karla	Karel	k1gMnSc2	Karel
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Erbena	Erben	k1gMnSc2	Erben
<g/>
.	.	kIx.	.
</s>
