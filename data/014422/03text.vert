<s>
Chatbot	Chatbot	k1gMnSc1
</s>
<s>
Chatbot	Chatbot	k1gInSc1
je	být	k5eAaImIp3nS
počítačový	počítačový	k2eAgInSc1d1
program	program	k1gInSc1
určený	určený	k2eAgInSc1d1
k	k	k7c3
automatizované	automatizovaný	k2eAgFnSc3d1
komunikaci	komunikace	k1gFnSc3
s	s	k7c7
lidmi	člověk	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
nejčastěji	často	k6eAd3
používané	používaný	k2eAgFnPc4d1
platformy	platforma	k1gFnPc4
patří	patřit	k5eAaImIp3nS
Facebook	Facebook	k1gInSc1
Messenger	Messengra	k1gFnPc2
<g/>
,	,	kIx,
Skype	Skyp	k1gMnSc5
<g/>
,	,	kIx,
Viber	Viber	k1gInSc1
<g/>
,	,	kIx,
WhatsApp	WhatsApp	k1gInSc1
<g/>
,	,	kIx,
Telegram	telegram	k1gInSc1
<g/>
,	,	kIx,
WeChat	WeChat	k1gMnSc1
<g/>
,	,	kIx,
Kik	Kik	k1gMnSc1
a	a	k8xC
Slack	Slack	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Využíván	využíván	k2eAgInSc1d1
bývá	bývat	k5eAaImIp3nS
zejména	zejména	k9
v	v	k7c6
zákaznické	zákaznický	k2eAgFnSc6d1
podpoře	podpora	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
nahrazuje	nahrazovat	k5eAaImIp3nS
živé	živý	k2eAgInPc4d1
operátory	operátor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ČR	ČR	kA
přes	přes	k7c4
chatbota	chatbot	k1gMnSc4
již	již	k6eAd1
lze	lze	k6eAd1
koupit	koupit	k5eAaPmF
i	i	k9
některé	některý	k3yIgInPc4
produkty	produkt	k1gInPc4
a	a	k8xC
služby	služba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazné	výrazný	k2eAgFnPc4d1
využití	využití	k1gNnSc3
nacházejí	nacházet	k5eAaImIp3nP
chatboty	chatbota	k1gFnPc1
v	v	k7c6
HR	hr	k2eAgNnSc6d1
odvětví	odvětví	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
používány	používat	k5eAaImNgFnP
pro	pro	k7c4
hledání	hledání	k1gNnSc4
a	a	k8xC
nábor	nábor	k1gInSc4
zaměstnanců	zaměstnanec	k1gMnPc2
(	(	kIx(
<g/>
sourcing	sourcing	k1gInSc1
<g/>
,	,	kIx,
recruiting	recruiting	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
První	první	k4xOgInPc1
návrhy	návrh	k1gInPc1
pocházejí	pocházet	k5eAaImIp3nP
už	už	k6eAd1
z	z	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
publikován	publikován	k2eAgInSc4d1
tzv.	tzv.	kA
Turingův	Turingův	k2eAgInSc4d1
test	test	k1gInSc4
pro	pro	k7c4
umělou	umělý	k2eAgFnSc4d1
inteligenci	inteligence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reálný	reálný	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
měl	mít	k5eAaImAgMnS
až	až	k9
profesor	profesor	k1gMnSc1
Weizenbaum	Weizenbaum	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
vyvinul	vyvinout	k5eAaPmAgInS
program	program	k1gInSc1
Eliza	Eliz	k1gMnSc2
simulující	simulující	k2eAgInSc4d1
psychoterapeutický	psychoterapeutický	k2eAgInSc4d1
rozhovor	rozhovor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
novějších	nový	k2eAgInPc2d2
programů	program	k1gInPc2
je	být	k5eAaImIp3nS
relativně	relativně	k6eAd1
úspěšný	úspěšný	k2eAgMnSc1d1
A.L.I.C.E.	A.L.I.C.E.	k1gMnSc1
z	z	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
ChatterBot	ChatterBot	k1gInSc4
je	být	k5eAaImIp3nS
připisován	připisovat	k5eAaImNgInS
Michaelu	Michael	k1gMnSc3
Mauldinovi	Mauldin	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
publikoval	publikovat	k5eAaBmAgMnS
programy	program	k1gInPc7
jako	jako	k8xS,k8xC
Lycos	Lycos	k1gInSc1
(	(	kIx(
<g/>
webový	webový	k2eAgMnSc1d1
vyhledávač	vyhledávač	k1gMnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
Verbot	Verbot	k1gInSc1
(	(	kIx(
<g/>
chatovací	chatovací	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
konverzační	konverzační	k2eAgMnSc1d1
chatbot	chatbot	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s>
První	první	k4xOgInSc1
typ	typ	k1gInSc1
chatbotu	chatbot	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
běžný	běžný	k2eAgInSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
klasický	klasický	k2eAgInSc4d1
chat	chata	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
místo	místo	k1gNnSc1
operátora	operátor	k1gMnSc2
odpovídá	odpovídat	k5eAaImIp3nS
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoduše	jednoduše	k6eAd1
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
uživatel	uživatel	k1gMnSc1
napíše	napsat	k5eAaPmIp3nS,k5eAaBmIp3nS
svou	svůj	k3xOyFgFnSc4
otázku	otázka	k1gFnSc4
a	a	k8xC
chatbot	chatbot	k1gInSc4
odpoví	odpovědět	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
uživatel	uživatel	k1gMnSc1
zeptá	zeptat	k5eAaPmIp3nS
na	na	k7c4
otázku	otázka	k1gFnSc4
“	“	k?
<g/>
X	X	kA
<g/>
”	”	k?
<g/>
,	,	kIx,
chatbot	chatbot	k1gInSc1
musí	muset	k5eAaImIp3nS
napsat	napsat	k5eAaPmF,k5eAaBmF
odpověď	odpověď	k1gFnSc1
“	“	k?
<g/>
X	X	kA
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uživatel	uživatel	k1gMnSc1
tedy	tedy	k9
nemusí	muset	k5eNaImIp3nS
ani	ani	k8xC
vědět	vědět	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
nepíše	psát	k5eNaImIp3nS
s	s	k7c7
člověkem	člověk	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problém	problém	k1gInSc4
ale	ale	k9
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
chatbot	chatbot	k1gMnSc1
své	svůj	k3xOyFgFnSc2
odpovědi	odpověď	k1gFnSc2
píše	psát	k5eAaImIp3nS
na	na	k7c6
základě	základ	k1gInSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
vyhodnotí	vyhodnotit	k5eAaPmIp3nS
zprávu	zpráva	k1gFnSc4
uživatele	uživatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
textu	text	k1gInSc6
uživatele	uživatel	k1gMnSc2
slovo	slovo	k1gNnSc4
“	“	k?
<g/>
matka	matka	k1gFnSc1
<g/>
”	”	k?
<g/>
,	,	kIx,
chatbot	chatbot	k1gInSc1
odpoví	odpovědět	k5eAaPmIp3nS
větu	věta	k1gFnSc4
“	“	k?
<g/>
Řekni	říct	k5eAaPmRp2nS
mi	já	k3xPp1nSc3
více	hodně	k6eAd2
o	o	k7c6
tvojí	tvůj	k3xOp2gFnSc6
rodině	rodina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
”	”	k?
Pokud	pokud	k8xS
ale	ale	k8xC
uživatel	uživatel	k1gMnSc1
potřebuje	potřebovat	k5eAaImIp3nS
informace	informace	k1gFnPc4
o	o	k7c6
maticích	matice	k1gFnPc6
a	a	k8xC
šroubech	šroub	k1gInPc6
<g/>
,	,	kIx,
tahle	tenhle	k3xDgFnSc1
odpověď	odpověď	k1gFnSc1
v	v	k7c6
kontextu	kontext	k1gInSc6
konverzace	konverzace	k1gFnSc2
nedává	dávat	k5eNaImIp3nS
smysl	smysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebo	nebo	k8xC
když	když	k8xS
uživatel	uživatel	k1gMnSc1
napíše	napsat	k5eAaPmIp3nS,k5eAaBmIp3nS
stejnou	stejný	k2eAgFnSc4d1
otázku	otázka	k1gFnSc4
dvakrát	dvakrát	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
jinak	jinak	k6eAd1
ji	on	k3xPp3gFnSc4
formuluje	formulovat	k5eAaImIp3nS
<g/>
,	,	kIx,
chatbot	chatbot	k1gInSc1
už	už	k6eAd1
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS
jiná	jiný	k2eAgNnPc4d1
klíčová	klíčový	k2eAgNnPc4d1
slova	slovo	k1gNnPc4
a	a	k8xC
uživatel	uživatel	k1gMnSc1
dostane	dostat	k5eAaPmIp3nS
různé	různý	k2eAgFnPc4d1
odpovědi	odpověď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
chatbotů	chatbot	k1gInPc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
technologii	technologie	k1gFnSc3
NLP	NLP	kA
<g/>
,	,	kIx,
tedy	tedy	k8xC
„	„	k?
<g/>
natural	natural	k?
language	language	k1gFnSc1
processing	processing	k1gInSc1
<g/>
“	“	k?
v	v	k7c6
češtině	čeština	k1gFnSc6
zpracování	zpracování	k1gNnSc2
přirozeného	přirozený	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
chatbota	chatbot	k1gMnSc2
pro	pro	k7c4
nábor	nábor	k1gInSc4
a	a	k8xC
HR	hr	k6eAd1
</s>
<s>
Automatický	automatický	k2eAgInSc1d1
test	test	k1gInSc1
základních	základní	k2eAgFnPc2d1
dovedností	dovednost	k1gFnPc2
a	a	k8xC
znalostí	znalost	k1gFnPc2
</s>
<s>
Chatbot	Chatbot	k1gInSc1
za	za	k7c4
vás	vy	k3xPp2nPc4
dokáže	dokázat	k5eAaPmIp3nS
zjistit	zjistit	k5eAaPmF
základní	základní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
o	o	k7c6
uchazeči	uchazeč	k1gMnSc6
<g/>
,	,	kIx,
časové	časový	k2eAgFnSc2d1
možnosti	možnost	k1gFnSc2
nástupu	nástup	k1gInSc2
i	i	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
očekávání	očekávání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeptá	zeptat	k5eAaPmIp3nS
se	se	k3xPyFc4
na	na	k7c4
předchozí	předchozí	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc1
znalosti	znalost	k1gFnPc1
a	a	k8xC
dovednosti	dovednost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Může	moct	k5eAaImIp3nS
například	například	k6eAd1
otestovat	otestovat	k5eAaPmF
základní	základní	k2eAgFnPc4d1
jazykové	jazykový	k2eAgFnPc4d1
znalosti	znalost	k1gFnPc4
(	(	kIx(
<g/>
např.	např.	kA
přeložte	přeložit	k5eAaPmRp2nP
mi	já	k3xPp1nSc3
prosím	prosit	k5eAaImIp1nS
<g/>
:	:	kIx,
Onion	Onion	k1gNnSc1
rings	ringsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
chtít	chtít	k5eAaImF
vypracovat	vypracovat	k5eAaPmF
nějaký	nějaký	k3yIgInSc4
úkol	úkol	k1gInSc4
(	(	kIx(
<g/>
např.	např.	kA
vytvoření	vytvoření	k1gNnSc4
grafu	graf	k1gInSc2
z	z	k7c2
tabulky	tabulka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pomůže	pomoct	k5eAaPmIp3nS
vám	vy	k3xPp2nPc3
získat	získat	k5eAaPmF
jeho	jeho	k3xOp3gInPc4
CV	CV	kA
<g/>
,	,	kIx,
motivační	motivační	k2eAgInSc1d1
dopis	dopis	k1gInSc1
<g/>
,	,	kIx,
doporučení	doporučení	k1gNnSc1
apod.	apod.	kA
</s>
<s>
Odchycení	odchycení	k1gNnSc1
vhodného	vhodný	k2eAgMnSc2d1
kandidáta	kandidát	k1gMnSc2
</s>
<s>
Představte	představit	k5eAaPmRp2nP
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
vaše	váš	k3xOp2gFnPc4
stránky	stránka	k1gFnPc4
zavítá	zavítat	k5eAaPmIp3nS
někdo	někdo	k3yInSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
jen	jen	k9
koketuje	koketovat	k5eAaImIp3nS
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
změnit	změnit	k5eAaPmF
zaměstnání	zaměstnání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmto	tento	k3xDgMnPc3
náhodným	náhodný	k2eAgMnPc3d1
kolemjdoucím	kolemjdoucí	k1gMnPc3
může	moct	k5eAaImIp3nS
chatbot	chatbot	k1gMnSc1
pomoci	pomoc	k1gFnSc2
zodpovědět	zodpovědět	k5eAaPmF
klíčové	klíčový	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
o	o	k7c6
vaší	váš	k3xOp2gFnSc6
společnosti	společnost	k1gFnSc6
a	a	k8xC
přesměrovat	přesměrovat	k5eAaPmF
je	on	k3xPp3gMnPc4
na	na	k7c4
příslušnou	příslušný	k2eAgFnSc4d1
pracovní	pracovní	k2eAgFnSc4d1
nabídku	nabídka	k1gFnSc4
ve	v	k7c6
vaší	váš	k3xOp2gFnSc6
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
pro	pro	k7c4
daného	daný	k2eAgMnSc4d1
návštěvníka	návštěvník	k1gMnSc4
není	být	k5eNaImIp3nS
aktuálně	aktuálně	k6eAd1
otevřená	otevřený	k2eAgFnSc1d1
vhodná	vhodný	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
<g/>
,	,	kIx,
umí	umět	k5eAaImIp3nS
od	od	k7c2
něj	on	k3xPp3gMnSc2
chatbot	chatbot	k1gInSc1
získat	získat	k5eAaPmF
kontakt	kontakt	k1gInSc4
a	a	k8xC
poslat	poslat	k5eAaPmF
jej	on	k3xPp3gMnSc4
přímo	přímo	k6eAd1
vašemu	váš	k3xOp2gMnSc3
HR	hr	k2eAgMnSc3d1
specialistovi	specialista	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
budoucnu	budoucno	k1gNnSc6
se	se	k3xPyFc4
vám	vy	k3xPp2nPc3
třeba	třeba	k6eAd1
bude	být	k5eAaImBp3nS
takový	takový	k3xDgMnSc1
kandidát	kandidát	k1gMnSc1
na	na	k7c4
nějakou	nějaký	k3yIgFnSc4
pozici	pozice	k1gFnSc4
hodit	hodit	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Zodpovězení	zodpovězený	k2eAgMnPc1d1
FAQ	FAQ	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
</s>
<s>
Kandidáti	kandidát	k1gMnPc1
mají	mít	k5eAaImIp3nP
otázky	otázka	k1gFnPc4
<g/>
,	,	kIx,
chatbot	chatbot	k1gInSc1
má	mít	k5eAaImIp3nS
odpovědi	odpověď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
pohovoru	pohovor	k1gInSc2
může	moct	k5eAaImIp3nS
chatbot	chatbot	k1gInSc1
snadno	snadno	k6eAd1
odpovědět	odpovědět	k5eAaPmF
na	na	k7c4
dotazy	dotaz	k1gInPc4
žadatelů	žadatel	k1gMnPc2
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
pracovní	pracovní	k2eAgFnSc1d1
doba	doba	k1gFnSc1
<g/>
,	,	kIx,
výhody	výhoda	k1gFnPc1
<g/>
,	,	kIx,
pravidla	pravidlo	k1gNnPc1
<g/>
,	,	kIx,
atd	atd	kA
<g/>
…	…	k?
</s>
<s>
Podpora	podpora	k1gFnSc1
náborové	náborový	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
</s>
<s>
Chatbot	Chatbot	k1gMnSc1
dokáže	dokázat	k5eAaPmIp3nS
podpořit	podpořit	k5eAaPmF
HR	hr	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
na	na	k7c6
různých	různý	k2eAgFnPc6d1
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
být	být	k5eAaImF
personifikovaný	personifikovaný	k2eAgMnSc1d1
na	na	k7c4
míru	míra	k1gFnSc4
dané	daný	k2eAgFnSc3d1
pozici	pozice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Neustále	neustále	k6eAd1
k	k	k7c3
dispozici	dispozice	k1gFnSc3
</s>
<s>
Chatbot	Chatbot	k1gInSc1
je	on	k3xPp3gMnPc4
online	onlinout	k5eAaPmIp3nS
24	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
365	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interakce	interakce	k1gFnSc1
s	s	k7c7
potencionálním	potencionální	k2eAgMnSc7d1
kandidátem	kandidát	k1gMnSc7
tedy	tedy	k9
může	moct	k5eAaImIp3nS
probíhat	probíhat	k5eAaImF
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
vy	vy	k3xPp2nPc1
už	už	k6eAd1
máte	mít	k5eAaImIp2nP
po	po	k7c6
pracovní	pracovní	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepotřebuje	potřebovat	k5eNaImIp3nS
pauzu	pauza	k1gFnSc4
na	na	k7c4
oběd	oběd	k1gInSc4
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
náladový	náladový	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přidaná	přidaná	k1gFnSc1
hodnota	hodnota	k1gFnSc1
chatbota	chatbota	k1gFnSc1
není	být	k5eNaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
částečné	částečný	k2eAgFnSc6d1
automatizaci	automatizace	k1gFnSc6
procesu	proces	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
chatbotu	chatbot	k1gInSc3
můžete	moct	k5eAaImIp2nP
také	také	k9
získat	získat	k5eAaPmF
informace	informace	k1gFnPc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k9
uchazeče	uchazeč	k1gMnSc4
nejvíce	nejvíce	k6eAd1,k6eAd3
zajímá	zajímat	k5eAaImIp3nS
(	(	kIx(
<g/>
plat	plat	k1gInSc1
<g/>
,	,	kIx,
firemní	firemní	k2eAgFnSc1d1
politika	politika	k1gFnSc1
<g/>
,	,	kIx,
pracovní	pracovní	k2eAgFnSc1d1
doba	doba	k1gFnSc1
<g/>
,	,	kIx,
apod	apod	kA
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zajímavý	zajímavý	k2eAgInSc4d1
ukazatel	ukazatel	k1gInSc4
výběru	výběr	k1gInSc2
potencionálního	potencionální	k2eAgMnSc2d1
zaměstnavatele	zaměstnavatel	k1gMnSc2
z	z	k7c2
pohledu	pohled	k1gInSc2
kandidátů	kandidát	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gMnSc1
Complete	Comple	k1gNnSc2
Beginner	Beginner	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Guide	Guid	k1gInSc5
To	ten	k3xDgNnSc1
Chatbots	Chatbots	k1gInSc1
<g/>
↑	↑	k?
100	#num#	k4
BEST	BEST	kA
BOTS	BOTS	kA
FOR	forum	k1gNnPc2
BRANDS	BRANDS	kA
&	&	k?
BUSINESSES	BUSINESSES	kA
<g/>
↑	↑	k?
HR	hr	k2eAgInSc4d1
Chatbot	Chatbot	k1gInSc4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Internetový	internetový	k2eAgInSc1d1
bot	bota	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4841533-9	4841533-9	k4
</s>
