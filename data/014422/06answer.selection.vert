<s>
Chatbot	Chatbot	k1gInSc1
je	být	k5eAaImIp3nS
počítačový	počítačový	k2eAgInSc1d1
program	program	k1gInSc1
určený	určený	k2eAgInSc1d1
k	k	k7c3
automatizované	automatizovaný	k2eAgFnSc3d1
komunikaci	komunikace	k1gFnSc3
s	s	k7c7
lidmi	člověk	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
nejčastěji	často	k6eAd3
používané	používaný	k2eAgFnPc4d1
platformy	platforma	k1gFnPc4
patří	patřit	k5eAaImIp3nS
Facebook	Facebook	k1gInSc1
Messenger	Messengra	k1gFnPc2
<g/>
,	,	kIx,
Skype	Skyp	k1gMnSc5
<g/>
,	,	kIx,
Viber	Viber	k1gInSc1
<g/>
,	,	kIx,
WhatsApp	WhatsApp	k1gInSc1
<g/>
,	,	kIx,
Telegram	telegram	k1gInSc1
<g/>
,	,	kIx,
WeChat	WeChat	k1gMnSc1
<g/>
,	,	kIx,
Kik	Kik	k1gMnSc1
a	a	k8xC
Slack	Slack	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Využíván	využíván	k2eAgInSc1d1
bývá	bývat	k5eAaImIp3nS
zejména	zejména	k9
v	v	k7c6
zákaznické	zákaznický	k2eAgFnSc6d1
podpoře	podpora	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
nahrazuje	nahrazovat	k5eAaImIp3nS
živé	živý	k2eAgInPc4d1
operátory	operátor	k1gInPc4
<g/>
.	.	kIx.
</s>