<p>
<s>
Murmansk	Murmansk	k1gInSc1	Murmansk
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
М	М	k?	М
<g/>
,	,	kIx,	,
sámsky	sámsek	k1gInPc1	sámsek
<g/>
:	:	kIx,	:
Murmanska	Murmansk	k1gInSc2	Murmansk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
Kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Norskem	Norsko	k1gNnSc7	Norsko
a	a	k8xC	a
Finskem	Finsko	k1gNnSc7	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Murmansk	Murmansk	k1gInSc1	Murmansk
je	být	k5eAaImIp3nS	být
správním	správní	k2eAgNnSc7d1	správní
střediskem	středisko	k1gNnSc7	středisko
Murmanské	murmanský	k2eAgFnSc2d1	Murmanská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
298	[number]	k4	298
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
a	a	k8xC	a
klima	klima	k1gNnSc1	klima
==	==	k?	==
</s>
</p>
<p>
<s>
Murmansk	Murmansk	k1gInSc1	Murmansk
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
za	za	k7c7	za
severním	severní	k2eAgInSc7d1	severní
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
(	(	kIx(	(
<g/>
68	[number]	k4	68
<g/>
°	°	k?	°
<g/>
58	[number]	k4	58
<g/>
'	'	kIx"	'
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
–	–	k?	–
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
307	[number]	k4	307
257	[number]	k4	257
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
po	po	k7c6	po
železnici	železnice	k1gFnSc6	železnice
je	být	k5eAaImIp3nS	být
1445	[number]	k4	1445
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
přístavem	přístav	k1gInSc7	přístav
<g/>
,	,	kIx,	,
jediným	jediný	k2eAgMnSc7d1	jediný
ruským	ruský	k2eAgMnSc7d1	ruský
v	v	k7c6	v
Barentsově	barentsově	k6eAd1	barentsově
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
nezamrzá	zamrzat	k5eNaImIp3nS	zamrzat
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
vděčí	vděčit	k5eAaImIp3nS	vděčit
teplému	teplý	k2eAgInSc3d1	teplý
Golfskému	golfský	k2eAgInSc3d1	golfský
proudu	proud	k1gInSc3	proud
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1918	[number]	k4	1918
odsud	odsud	k6eAd1	odsud
vyplul	vyplout	k5eAaPmAgMnS	vyplout
poslední	poslední	k2eAgInSc4d1	poslední
transport	transport	k1gInSc4	transport
československých	československý	k2eAgMnPc2d1	československý
legionářů	legionář	k1gMnPc2	legionář
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Severní	severní	k2eAgFnSc7d1	severní
cestou	cesta	k1gFnSc7	cesta
<g/>
"	"	kIx"	"
na	na	k7c4	na
bojiště	bojiště	k1gNnSc4	bojiště
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
již	již	k6eAd1	již
odjížděly	odjíždět	k5eAaImAgFnP	odjíždět
po	po	k7c6	po
magistrále	magistrála	k1gFnSc6	magistrála
do	do	k7c2	do
Vladivostoku	Vladivostok	k1gInSc2	Vladivostok
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
subarktickém	subarktický	k2eAgInSc6d1	subarktický
podnebném	podnebný	k2eAgInSc6d1	podnebný
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
nízké	nízký	k2eAgInPc4d1	nízký
(	(	kIx(	(
<g/>
376	[number]	k4	376
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejdeštivějším	deštivý	k2eAgInSc7d3	nejdeštivější
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
srpen	srpen	k1gInSc1	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
činí	činit	k5eAaImIp3nS	činit
0,1	[number]	k4	0,1
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
12,8	[number]	k4	12,8
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
-9,9	-9,9	k4	-9,9
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
byla	být	k5eAaImAgFnS	být
přivedena	přiveden	k2eAgFnSc1d1	přivedena
železnice	železnice	k1gFnSc1	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
během	během	k7c2	během
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
ruské	ruský	k2eAgNnSc1d1	ruské
pobřeží	pobřeží	k1gNnSc1	pobřeží
Baltu	Balt	k1gInSc2	Balt
a	a	k8xC	a
Černého	černé	k1gNnSc2	černé
moře	moře	k1gNnSc2	moře
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Murmansk	Murmansk	k1gInSc1	Murmansk
vhodným	vhodný	k2eAgMnSc7d1	vhodný
kandidátem	kandidát	k1gMnSc7	kandidát
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
spojil	spojit	k5eAaPmAgInS	spojit
Rusko	Rusko	k1gNnSc4	Rusko
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
Romanov-na-Murmaně	Romanova-Murmaň	k1gFnPc4	Romanov-na-Murmaň
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
ruské	ruský	k2eAgFnSc2d1	ruská
královské	královský	k2eAgFnSc2d1	královská
dynastie	dynastie	k1gFnSc2	dynastie
Romanovců	Romanovec	k1gMnPc2	Romanovec
(	(	kIx(	(
<g/>
Murman	Murman	k1gMnSc1	Murman
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc4d1	starý
ruský	ruský	k2eAgInSc4d1	ruský
název	název	k1gInSc4	název
pro	pro	k7c4	pro
Barentsovo	Barentsův	k2eAgNnSc4d1	Barentsovo
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
pro	pro	k7c4	pro
Nory	Nor	k1gMnPc4	Nor
<g/>
,	,	kIx,	,
odvozený	odvozený	k2eAgMnSc1d1	odvozený
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
Norman	Norman	k1gMnSc1	Norman
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
teorie	teorie	k1gFnSc1	teorie
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Murman	Murman	k1gMnSc1	Murman
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
ze	z	k7c2	z
sámského	sámský	k2eAgNnSc2d1	sámský
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
moře	moře	k1gNnSc4	moře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Murmansk	Murmansk	k1gInSc4	Murmansk
po	po	k7c6	po
Únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Murmansk	Murmansk	k1gInSc1	Murmansk
je	být	k5eAaImIp3nS	být
konečnou	konečný	k2eAgFnSc7d1	konečná
stanicí	stanice	k1gFnSc7	stanice
Murmanské	murmanský	k2eAgFnSc2d1	Murmanská
(	(	kIx(	(
<g/>
Kirovské	Kirovský	k2eAgFnSc2d1	Kirovský
<g/>
)	)	kIx)	)
železnice	železnice	k1gFnSc2	železnice
z	z	k7c2	z
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
1445	[number]	k4	1445
km	km	kA	km
<g/>
;	;	kIx,	;
elektrifikovaná	elektrifikovaný	k2eAgFnSc1d1	elektrifikovaná
<g/>
,	,	kIx,	,
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
spojnicí	spojnice	k1gFnSc7	spojnice
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
části	část	k1gFnPc1	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
;	;	kIx,	;
denně	denně	k6eAd1	denně
tudy	tudy	k6eAd1	tudy
jezdí	jezdit	k5eAaImIp3nP	jezdit
rychlíky	rychlík	k1gInPc1	rychlík
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
,	,	kIx,	,
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
Vologdy	Vologda	k1gFnSc2	Vologda
<g/>
,	,	kIx,	,
sezónně	sezónně	k6eAd1	sezónně
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
destinací	destinace	k1gFnPc2	destinace
<g/>
.	.	kIx.	.
</s>
<s>
Končí	končit	k5eAaImIp3nS	končit
zde	zde	k6eAd1	zde
také	také	k9	také
federální	federální	k2eAgFnSc1d1	federální
silnice	silnice	k1gFnSc1	silnice
M18	M18	k1gFnSc1	M18
"	"	kIx"	"
<g/>
Kola	Kola	k1gFnSc1	Kola
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Murmansku	Murmansk	k1gInSc6	Murmansk
jsou	být	k5eAaImIp3nP	být
střední	střední	k2eAgFnPc1d1	střední
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc1d2	vyšší
námořnická	námořnický	k2eAgFnSc1d1	námořnická
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
Murmanská	murmanský	k2eAgFnSc1d1	Murmanská
státní	státní	k2eAgFnSc1d1	státní
technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
)	)	kIx)	)
a	a	k8xC	a
několik	několik	k4yIc4	několik
výzkumných	výzkumný	k2eAgNnPc2d1	výzkumné
středisek	středisko	k1gNnPc2	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
průmyslu	průmysl	k1gInSc2	průmysl
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
stavba	stavba	k1gFnSc1	stavba
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
železářský	železářský	k2eAgInSc1d1	železářský
a	a	k8xC	a
dřevozpracující	dřevozpracující	k2eAgInSc1d1	dřevozpracující
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
rybný	rybný	k2eAgInSc1d1	rybný
kombinát	kombinát	k1gInSc1	kombinát
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
dopravu	doprava	k1gFnSc4	doprava
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
trolejbusy	trolejbus	k1gInPc1	trolejbus
(	(	kIx(	(
<g/>
nejsevernější	severní	k2eAgInSc1d3	nejsevernější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
dřívější	dřívější	k2eAgFnSc3d1	dřívější
relativní	relativní	k2eAgFnSc3d1	relativní
bohatosti	bohatost	k1gFnSc3	bohatost
města	město	k1gNnSc2	město
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
pestrého	pestrý	k2eAgNnSc2d1	pestré
národnostního	národnostní	k2eAgNnSc2d1	národnostní
složení	složení	k1gNnSc2	složení
byla	být	k5eAaImAgFnS	být
přítomnost	přítomnost	k1gFnSc4	přítomnost
základny	základna	k1gFnSc2	základna
ruského	ruský	k2eAgNnSc2d1	ruské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
smutně	smutně	k6eAd1	smutně
proslula	proslout	k5eAaPmAgFnS	proslout
katastrofou	katastrofa	k1gFnSc7	katastrofa
jaderné	jaderný	k2eAgFnSc2d1	jaderná
ponorky	ponorka	k1gFnSc2	ponorka
Kursk	Kursk	k1gInSc1	Kursk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
části	část	k1gFnSc2	část
ponorky	ponorka	k1gFnSc2	ponorka
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
památník	památník	k1gInSc1	památník
obětem	oběť	k1gFnPc3	oběť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Murmansk	Murmansk	k1gInSc1	Murmansk
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
v	v	k7c6	v
Arktickém	arktický	k2eAgInSc6d1	arktický
kruhu	kruh	k1gInSc6	kruh
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vylidňuje	vylidňovat	k5eAaImIp3nS	vylidňovat
<g/>
.	.	kIx.	.
</s>
<s>
Kolaps	kolaps	k1gInSc1	kolaps
řízené	řízený	k2eAgFnSc2d1	řízená
ekonomiky	ekonomika	k1gFnSc2	ekonomika
byl	být	k5eAaImAgInS	být
příčinou	příčina	k1gFnSc7	příčina
odchodu	odchod	k1gInSc2	odchod
části	část	k1gFnSc2	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
teplejších	teplý	k2eAgFnPc2d2	teplejší
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
za	za	k7c7	za
lepšími	dobrý	k2eAgFnPc7d2	lepší
pracovními	pracovní	k2eAgFnPc7d1	pracovní
příležitostmi	příležitost	k1gFnPc7	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zamířili	zamířit	k5eAaPmAgMnP	zamířit
do	do	k7c2	do
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
a	a	k8xC	a
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
rozpadem	rozpad	k1gInSc7	rozpad
SSSR	SSSR	kA	SSSR
byl	být	k5eAaImAgInS	být
Murmansk	Murmansk	k1gInSc1	Murmansk
domovem	domov	k1gInSc7	domov
téměř	téměř	k6eAd1	téměř
500	[number]	k4	500
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Přilákala	přilákat	k5eAaPmAgFnS	přilákat
je	být	k5eAaImIp3nS	být
sem	sem	k6eAd1	sem
privilegia	privilegium	k1gNnPc4	privilegium
jako	jako	k8xC	jako
vyšší	vysoký	k2eAgFnPc4d2	vyšší
mzdy	mzda	k1gFnPc4	mzda
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgFnSc1d2	delší
dovolená	dovolená	k1gFnSc1	dovolená
a	a	k8xC	a
nižší	nízký	k2eAgInSc1d2	nižší
penzijní	penzijní	k2eAgInSc1d1	penzijní
věk	věk	k1gInSc1	věk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
opustilo	opustit	k5eAaPmAgNnS	opustit
město	město	k1gNnSc1	město
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
rozčarovaných	rozčarovaný	k2eAgFnPc6d1	rozčarovaná
ztrátou	ztráta	k1gFnSc7	ztráta
výhod	výhoda	k1gFnPc2	výhoda
–	–	k?	–
a	a	k8xC	a
zhroucením	zhroucení	k1gNnSc7	zhroucení
centrálního	centrální	k2eAgInSc2d1	centrální
městského	městský	k2eAgInSc2d1	městský
systému	systém	k1gInSc2	systém
vytápění	vytápění	k1gNnSc2	vytápění
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
307	[number]	k4	307
257	[number]	k4	257
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
85	[number]	k4	85
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
zejména	zejména	k9	zejména
ruskojazyční	ruskojazyčný	k2eAgMnPc1d1	ruskojazyčný
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
Bělorusové	Bělorus	k1gMnPc1	Bělorus
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
zajímavosti	zajímavost	k1gFnSc2	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
Murmanskem	Murmansk	k1gInSc7	Murmansk
ční	čnět	k5eAaImIp3nS	čnět
socha	socha	k1gFnSc1	socha
vojáka	voják	k1gMnSc2	voják
v	v	k7c6	v
notně	notně	k6eAd1	notně
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Familiárně	familiárně	k6eAd1	familiárně
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
Aljoša	Aljoša	k1gMnSc1	Aljoša
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
sochu	socha	k1gFnSc4	socha
nechala	nechat	k5eAaPmAgFnS	nechat
ruská	ruský	k2eAgFnSc1d1	ruská
vláda	vláda	k1gFnSc1	vláda
postavit	postavit	k5eAaPmF	postavit
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
námořníkům	námořník	k1gMnPc3	námořník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
chránili	chránit	k5eAaImAgMnP	chránit
město	město	k1gNnSc4	město
a	a	k8xC	a
přístav	přístav	k1gInSc4	přístav
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Murmansk	Murmansk	k1gInSc1	Murmansk
byl	být	k5eAaImAgInS	být
také	také	k9	také
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
čestným	čestný	k2eAgInSc7d1	čestný
titulem	titul	k1gInSc7	titul
Město-hrdina	Městordina	k1gFnSc1	Město-hrdina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
kotví	kotvit	k5eAaImIp3nS	kotvit
ledoborec	ledoborec	k1gMnSc1	ledoborec
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dalších	další	k2eAgNnPc2d1	další
čtyř	čtyři	k4xCgNnPc2	čtyři
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
<g/>
;	;	kIx,	;
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
reprezentačním	reprezentační	k2eAgInPc3d1	reprezentační
účelům	účel	k1gInPc3	účel
a	a	k8xC	a
jako	jako	k8xC	jako
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vždy	vždy	k6eAd1	vždy
poslední	poslední	k2eAgInSc4d1	poslední
týden	týden	k1gInSc4	týden
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Murmanska	Murmansk	k1gInSc2	Murmansk
koná	konat	k5eAaImIp3nS	konat
tradiční	tradiční	k2eAgInSc1d1	tradiční
Festival	festival	k1gInSc1	festival
Severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
oslav	oslava	k1gFnPc2	oslava
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
oblíbené	oblíbený	k2eAgInPc4d1	oblíbený
sobí	sobí	k2eAgInPc4d1	sobí
závody	závod	k1gInPc4	závod
a	a	k8xC	a
lyžařské	lyžařský	k2eAgInPc4d1	lyžařský
maratóny	maratón	k1gInPc4	maratón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Murmansku	Murmansk	k1gInSc6	Murmansk
sídlí	sídlet	k5eAaImIp3nS	sídlet
krajská	krajský	k2eAgFnSc1d1	krajská
pobočka	pobočka	k1gFnSc1	pobočka
Všeruské	všeruský	k2eAgFnSc2d1	Všeruská
státní	státní	k2eAgFnSc2d1	státní
televizní	televizní	k2eAgFnSc2d1	televizní
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
společnosti	společnost	k1gFnSc2	společnost
<g/>
:	:	kIx,	:
stanice	stanice	k1gFnSc2	stanice
Murman	Murman	k1gMnSc1	Murman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
pražská	pražský	k2eAgFnSc1d1	Pražská
ulice	ulice	k1gFnSc1	ulice
Murmanská	murmanský	k2eAgFnSc1d1	Murmanská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Murmanská	murmanský	k2eAgFnSc1d1	Murmanská
železniční	železniční	k2eAgFnSc1d1	železniční
magistrála	magistrála	k1gFnSc1	magistrála
</s>
</p>
<p>
<s>
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Murmansku	Murmansk	k1gInSc6	Murmansk
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Murmansk	Murmansk	k1gInSc1	Murmansk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
městský	městský	k2eAgInSc1d1	městský
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
http://www.rightplacerighttime.cz/journal/murmansk.html	[url]	k1gInSc1	http://www.rightplacerighttime.cz/journal/murmansk.html
-	-	kIx~	-
cestopis	cestopis	k1gInSc1	cestopis
z	z	k7c2	z
Murmansku	Murmansk	k1gInSc2	Murmansk
</s>
</p>
