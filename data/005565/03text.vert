<s>
Infarkt	infarkt	k1gInSc1	infarkt
myokardu	myokard	k1gInSc2	myokard
(	(	kIx(	(
<g/>
srdeční	srdeční	k2eAgFnSc1d1	srdeční
mrtvice	mrtvice	k1gFnSc1	mrtvice
<g/>
,	,	kIx,	,
srdeční	srdeční	k2eAgFnSc1d1	srdeční
záhať	záhať	k1gFnSc1	záhať
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
náhlé	náhlý	k2eAgNnSc1d1	náhlé
přerušení	přerušení	k1gNnSc1	přerušení
krevního	krevní	k2eAgNnSc2d1	krevní
zásobování	zásobování	k1gNnSc2	zásobování
části	část	k1gFnSc2	část
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
náhlým	náhlý	k2eAgNnPc3d1	náhlé
uzávěrem	uzávěr	k1gInSc7	uzávěr
srdeční	srdeční	k2eAgFnPc4d1	srdeční
(	(	kIx(	(
<g/>
koronární	koronární	k2eAgFnPc4d1	koronární
<g/>
)	)	kIx)	)
tepny	tepna	k1gFnPc4	tepna
-	-	kIx~	-
nejčastěji	často	k6eAd3	často
vznikem	vznik	k1gInSc7	vznik
krevní	krevní	k2eAgFnSc2d1	krevní
sraženiny	sraženina	k1gFnSc2	sraženina
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1	krevní
sraženina	sraženina	k1gFnSc1	sraženina
(	(	kIx(	(
<g/>
trombus	trombus	k1gInSc1	trombus
<g/>
)	)	kIx)	)
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
při	při	k7c6	při
prasknutí	prasknutí	k1gNnSc6	prasknutí
aterosklerotického	aterosklerotický	k2eAgInSc2d1	aterosklerotický
plátu	plát	k1gInSc2	plát
čímž	což	k3yRnSc7	což
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
protisrážlivá	protisrážlivý	k2eAgFnSc1d1	protisrážlivá
ochrana	ochrana	k1gFnSc1	ochrana
povrchu	povrch	k1gInSc2	povrch
tepny	tepna	k1gFnSc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Aterosklerotický	Aterosklerotický	k2eAgInSc1d1	Aterosklerotický
plát	plát	k1gInSc1	plát
vzniká	vznikat	k5eAaImIp3nS	vznikat
dlouhodobým	dlouhodobý	k2eAgNnSc7d1	dlouhodobé
ukládáním	ukládání	k1gNnSc7	ukládání
tukových	tukový	k2eAgFnPc2d1	tuková
látek	látka	k1gFnPc2	látka
do	do	k7c2	do
stěny	stěna	k1gFnSc2	stěna
cévy	céva	k1gFnSc2	céva
<g/>
,	,	kIx,	,
podkladem	podklad	k1gInSc7	podklad
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
příčinou	příčina	k1gFnSc7	příčina
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vmetek	vmetek	k1gInSc4	vmetek
krevní	krevní	k2eAgFnSc2d1	krevní
sraženiny	sraženina	k1gFnSc2	sraženina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
cévního	cévní	k2eAgNnSc2d1	cévní
řečiště	řečiště	k1gNnSc2	řečiště
<g/>
,	,	kIx,	,
céva	céva	k1gFnSc1	céva
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
také	také	k9	také
vzduchovou	vzduchový	k2eAgFnSc7d1	vzduchová
bublinkou	bublinka	k1gFnSc7	bublinka
(	(	kIx(	(
<g/>
příhody	příhoda	k1gFnSc2	příhoda
při	při	k7c6	při
potápění	potápění	k1gNnSc6	potápění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následek	následek	k1gInSc1	následek
uzávěru	uzávěr	k1gInSc2	uzávěr
koronární	koronární	k2eAgFnSc2d1	koronární
tepny	tepna	k1gFnSc2	tepna
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zástava	zástava	k1gFnSc1	zástava
oběhu	oběh	k1gInSc2	oběh
(	(	kIx(	(
<g/>
náhlá	náhlý	k2eAgFnSc1d1	náhlá
srdeční	srdeční	k2eAgFnSc1d1	srdeční
smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
akutní	akutní	k2eAgFnSc6d1	akutní
fázi	fáze	k1gFnSc6	fáze
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
také	také	k9	také
kritické	kritický	k2eAgNnSc4d1	kritické
oslabení	oslabení	k1gNnSc4	oslabení
srdeční	srdeční	k2eAgFnSc2d1	srdeční
činnosti	činnost	k1gFnSc2	činnost
s	s	k7c7	s
kardiogenním	kardiogenní	k2eAgInSc7d1	kardiogenní
šokem	šok	k1gInSc7	šok
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
krevní	krevní	k2eAgInSc4d1	krevní
proud	proud	k1gInSc4	proud
v	v	k7c6	v
postižené	postižený	k2eAgFnSc6d1	postižená
tepně	tepna	k1gFnSc6	tepna
obnoven	obnoven	k2eAgInSc4d1	obnoven
do	do	k7c2	do
2	[number]	k4	2
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nevratnému	vratný	k2eNgNnSc3d1	nevratné
poškození	poškození	k1gNnSc3	poškození
postižené	postižený	k2eAgFnSc2d1	postižená
části	část	k1gFnSc2	část
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Obnovení	obnovení	k1gNnSc1	obnovení
průtoku	průtok	k1gInSc2	průtok
krve	krev	k1gFnSc2	krev
i	i	k9	i
po	po	k7c6	po
2	[number]	k4	2
hodinách	hodina	k1gFnPc6	hodina
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
blahodárný	blahodárný	k2eAgInSc4d1	blahodárný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
při	při	k7c6	při
podezření	podezření	k1gNnSc6	podezření
vždy	vždy	k6eAd1	vždy
angažovat	angažovat	k5eAaBmF	angažovat
zdravotnickou	zdravotnický	k2eAgFnSc4d1	zdravotnická
záchrannou	záchranný	k2eAgFnSc4d1	záchranná
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Infarkt	infarkt	k1gInSc1	infarkt
myokardu	myokard	k1gInSc2	myokard
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
více	hodně	k6eAd2	hodně
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
než	než	k8xS	než
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
ohroženi	ohrožen	k2eAgMnPc1d1	ohrožen
jsou	být	k5eAaImIp3nP	být
muži	muž	k1gMnPc1	muž
nad	nad	k7c4	nad
50	[number]	k4	50
let	léto	k1gNnPc2	léto
a	a	k8xC	a
ženy	žena	k1gFnSc2	žena
nad	nad	k7c4	nad
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
onemocnění	onemocnění	k1gNnSc1	onemocnění
se	se	k3xPyFc4	se
však	však	k9	však
nevyhýbá	vyhýbat	k5eNaImIp3nS	vyhýbat
také	také	k9	také
mladším	mladý	k2eAgMnPc3d2	mladší
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
nadměrnou	nadměrný	k2eAgFnSc4d1	nadměrná
srážlivost	srážlivost	k1gFnSc4	srážlivost
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
genetické	genetický	k2eAgFnPc4d1	genetická
dispozice	dispozice	k1gFnPc4	dispozice
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
špatnou	špatný	k2eAgFnSc4d1	špatná
životosprávu	životospráva	k1gFnSc4	životospráva
<g/>
.	.	kIx.	.
</s>
<s>
Infarkt	infarkt	k1gInSc1	infarkt
myokardu	myokard	k1gInSc2	myokard
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
anginou	angina	k1gFnSc7	angina
pectoris	pectoris	k1gFnPc2	pectoris
jsou	být	k5eAaImIp3nP	být
ischemickou	ischemický	k2eAgFnSc7d1	ischemická
chorobou	choroba	k1gFnSc7	choroba
srdeční	srdeční	k2eAgFnSc7d1	srdeční
(	(	kIx(	(
<g/>
ICHS	ICHS	kA	ICHS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
ročně	ročně	k6eAd1	ročně
umírají	umírat	k5eAaImIp3nP	umírat
statisíce	statisíce	k1gInPc4	statisíce
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Infarkt	infarkt	k1gInSc1	infarkt
myokardu	myokard	k1gInSc2	myokard
(	(	kIx(	(
<g/>
IM	IM	kA	IM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
projevů	projev	k1gInPc2	projev
ischemické	ischemický	k2eAgFnSc2d1	ischemická
choroby	choroba	k1gFnSc2	choroba
srdeční	srdeční	k2eAgFnSc2d1	srdeční
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ischemické	ischemický	k2eAgNnSc4d1	ischemické
ložiskové	ložiskový	k2eAgNnSc4d1	ložiskové
odumření	odumření	k1gNnSc4	odumření
části	část	k1gFnSc2	část
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
uzávěru	uzávěr	k1gInSc6	uzávěr
nebo	nebo	k8xC	nebo
výrazném	výrazný	k2eAgNnSc6d1	výrazné
zúžení	zúžení	k1gNnSc6	zúžení
v	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
levé	levý	k2eAgFnSc2d1	levá
nebo	nebo	k8xC	nebo
pravé	pravý	k2eAgFnSc2d1	pravá
věnčité	věnčitý	k2eAgFnSc2d1	věnčitá
tepny	tepna	k1gFnSc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgInSc4d1	zadní
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
–	–	k?	–
postihuje	postihovat	k5eAaImIp3nS	postihovat
zadní	zadní	k2eAgFnSc4d1	zadní
stěnu	stěna	k1gFnSc4	stěna
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
Přední	přední	k2eAgInSc4d1	přední
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
–	–	k?	–
postihuje	postihovat	k5eAaImIp3nS	postihovat
přední	přední	k2eAgFnSc4d1	přední
stěnu	stěna	k1gFnSc4	stěna
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
Spodní	spodní	k2eAgInSc4d1	spodní
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
–	–	k?	–
postihuje	postihovat	k5eAaImIp3nS	postihovat
spodní	spodní	k2eAgFnSc4d1	spodní
stěnu	stěna	k1gFnSc4	stěna
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
kombinace	kombinace	k1gFnPc4	kombinace
těchto	tento	k3xDgFnPc2	tento
lokalizací	lokalizace	k1gFnPc2	lokalizace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
předozadní	předozadní	k2eAgInSc4d1	předozadní
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
Akutní	akutní	k2eAgInSc4d1	akutní
infarkt	infarkt	k1gInSc4	infarkt
myokardu	myokard	k1gInSc2	myokard
-	-	kIx~	-
infarkt	infarkt	k1gInSc1	infarkt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pacienta	pacient	k1gMnSc4	pacient
přímo	přímo	k6eAd1	přímo
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
na	na	k7c6	na
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
akutním	akutní	k2eAgInSc6d1	akutní
infarktu	infarkt	k1gInSc6	infarkt
myokardu	myokard	k1gInSc2	myokard
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
koronárních	koronární	k2eAgFnPc6d1	koronární
tepnách	tepna	k1gFnPc6	tepna
více	hodně	k6eAd2	hodně
uzávěrů	uzávěr	k1gInPc2	uzávěr
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
uzávěr	uzávěr	k1gInSc1	uzávěr
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
nad	nad	k7c7	nad
větvením	větvení	k1gNnSc7	větvení
koronární	koronární	k2eAgFnSc2d1	koronární
tepny	tepna	k1gFnSc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
rozsah	rozsah	k1gInSc1	rozsah
poškození	poškození	k1gNnSc2	poškození
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k9	jako
u	u	k7c2	u
Angina	angina	k1gFnSc1	angina
pectoris	pectoris	k1gFnPc7	pectoris
<g/>
.	.	kIx.	.
</s>
<s>
Tupá	tupý	k2eAgFnSc1d1	tupá
svíravá	svíravý	k2eAgFnSc1d1	svíravá
bolest	bolest	k1gFnSc1	bolest
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vystřeluje	vystřelovat	k5eAaImIp3nS	vystřelovat
do	do	k7c2	do
levé	levý	k2eAgFnSc2d1	levá
horní	horní	k2eAgFnSc2d1	horní
končetiny	končetina	k1gFnSc2	končetina
<g/>
,	,	kIx,	,
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
studený	studený	k2eAgInSc4d1	studený
pot	pot	k1gInSc4	pot
<g/>
.	.	kIx.	.
kouření	kouření	k1gNnSc4	kouření
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
tuků	tuk	k1gInPc2	tuk
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
krevní	krevní	k2eAgInSc1d1	krevní
tlak	tlak	k1gInSc1	tlak
(	(	kIx(	(
<g/>
i	i	k9	i
léčený	léčený	k2eAgMnSc1d1	léčený
<g/>
)	)	kIx)	)
rodinná	rodinný	k2eAgFnSc1d1	rodinná
zátěž	zátěž	k1gFnSc1	zátěž
(	(	kIx(	(
<g/>
mrtvice	mrtvice	k1gFnSc1	mrtvice
a	a	k8xC	a
infarkt	infarkt	k1gInSc4	infarkt
u	u	k7c2	u
pokrevních	pokrevní	k2eAgMnPc2d1	pokrevní
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
)	)	kIx)	)
Obezita	obezita	k1gFnSc1	obezita
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
srážlivost	srážlivost	k1gFnSc1	srážlivost
krve	krev	k1gFnSc2	krev
Cukrovka	cukrovka	k1gFnSc1	cukrovka
Základní	základní	k2eAgFnSc1d1	základní
příčinou	příčina	k1gFnSc7	příčina
je	být	k5eAaImIp3nS	být
ukládání	ukládání	k1gNnSc1	ukládání
cholesterolu	cholesterol	k1gInSc2	cholesterol
do	do	k7c2	do
stěny	stěna	k1gFnSc2	stěna
tepny	tepna	k1gFnSc2	tepna
<g/>
.	.	kIx.	.
</s>
<s>
Postupné	postupný	k2eAgNnSc1d1	postupné
zvětšování	zvětšování	k1gNnSc1	zvětšování
tukového	tukový	k2eAgInSc2d1	tukový
plátu	plát	k1gInSc2	plát
zužuje	zužovat	k5eAaImIp3nS	zužovat
její	její	k3xOp3gInSc4	její
průsvit	průsvit	k1gInSc4	průsvit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
námahové	námahový	k2eAgFnPc4d1	námahová
potíže	potíž	k1gFnPc4	potíž
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
angina	angina	k1gFnSc1	angina
pectoris	pectoris	k1gFnSc1	pectoris
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okamžiku	okamžik	k1gInSc6	okamžik
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
tvořit	tvořit	k5eAaImF	tvořit
krevní	krevní	k2eAgFnSc1d1	krevní
sraženina	sraženina	k1gFnSc1	sraženina
<g/>
,	,	kIx,	,
nasedávající	nasedávající	k2eAgFnPc1d1	nasedávající
na	na	k7c4	na
tukové	tukový	k2eAgFnPc4d1	tuková
hmoty	hmota	k1gFnPc4	hmota
<g/>
,	,	kIx,	,
dráždící	dráždící	k2eAgFnPc4d1	dráždící
krevní	krevní	k2eAgFnPc4d1	krevní
destičky	destička	k1gFnPc4	destička
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
okamžik	okamžik	k1gInSc1	okamžik
nastává	nastávat	k5eAaImIp3nS	nastávat
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
vyvolávající	vyvolávající	k2eAgInPc1d1	vyvolávající
momenty	moment	k1gInPc1	moment
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vystavení	vystavení	k1gNnSc4	vystavení
stresu	stres	k1gInSc2	stres
<g/>
,	,	kIx,	,
chladu	chlad	k1gInSc2	chlad
<g/>
,	,	kIx,	,
fyzická	fyzický	k2eAgFnSc1d1	fyzická
zátěž	zátěž	k1gFnSc1	zátěž
nebo	nebo	k8xC	nebo
změna	změna	k1gFnSc1	změna
počasí	počasí	k1gNnSc1	počasí
jsou	být	k5eAaImIp3nP	být
málo	málo	k6eAd1	málo
časté	častý	k2eAgMnPc4d1	častý
<g/>
.	.	kIx.	.
déletrvající	déletrvající	k2eAgFnSc7d1	déletrvající
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
tlaková	tlakový	k2eAgFnSc1d1	tlaková
krutá	krutý	k2eAgFnSc1d1	krutá
svíravá	svíravý	k2eAgFnSc1d1	svíravá
bolest	bolest	k1gFnSc1	bolest
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
hrudní	hrudní	k2eAgFnSc2d1	hrudní
kosti	kost	k1gFnSc2	kost
bolest	bolest	k1gFnSc1	bolest
neustupuje	ustupovat	k5eNaImIp3nS	ustupovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
silná	silný	k2eAgFnSc1d1	silná
v	v	k7c6	v
jakýchkoli	jakýkoli	k3yIgFnPc6	jakýkoli
polohách	poloha	k1gFnPc6	poloha
<g />
.	.	kIx.	.
</s>
<s>
typické	typický	k2eAgNnSc1d1	typické
je	být	k5eAaImIp3nS	být
vyzařování	vyzařování	k1gNnSc1	vyzařování
bolesti	bolest	k1gFnSc2	bolest
do	do	k7c2	do
ramene	rameno	k1gNnSc2	rameno
<g/>
,	,	kIx,	,
krku	krk	k1gInSc2	krk
a	a	k8xC	a
levé	levý	k2eAgFnSc2d1	levá
ruky	ruka	k1gFnSc2	ruka
a	a	k8xC	a
lopatky	lopatka	k1gFnSc2	lopatka
<g/>
.	.	kIx.	.
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
pocení	pocení	k1gNnSc4	pocení
úzkost	úzkost	k1gFnSc4	úzkost
a	a	k8xC	a
dušnost	dušnost	k1gFnSc4	dušnost
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
také	také	k9	také
objevit	objevit	k5eAaPmF	objevit
bolesti	bolest	k1gFnPc4	bolest
zad	záda	k1gNnPc2	záda
<g/>
,	,	kIx,	,
břicha	břicho	k1gNnSc2	břicho
a	a	k8xC	a
čelisti	čelist	k1gFnSc2	čelist
<g/>
.	.	kIx.	.
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
bolest	bolest	k1gFnSc1	bolest
dostavuje	dostavovat	k5eAaImIp3nS	dostavovat
náhle	náhle	k6eAd1	náhle
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
nutno	nutno	k6eAd1	nutno
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
)	)	kIx)	)
primárně	primárně	k6eAd1	primárně
akutní	akutní	k2eAgNnSc4d1	akutní
levostranné	levostranný	k2eAgNnSc4d1	levostranné
selhání	selhání	k1gNnSc4	selhání
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
plicní	plicní	k2eAgFnSc4d1	plicní
embolii	embolie	k1gFnSc4	embolie
<g/>
,	,	kIx,	,
plicní	plicní	k2eAgFnSc4d1	plicní
hypertenzi	hypertenze	k1gFnSc4	hypertenze
<g/>
,	,	kIx,	,
perikardiální	perikardiální	k2eAgFnSc4d1	perikardiální
tamponádu	tamponáda	k1gFnSc4	tamponáda
<g/>
,	,	kIx,	,
Tietzův	Tietzův	k2eAgInSc4d1	Tietzův
syndrom	syndrom	k1gInSc4	syndrom
Při	při	k7c6	při
podezření	podezření	k1gNnSc6	podezření
na	na	k7c4	na
infarkt	infarkt	k1gInSc4	infarkt
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
nemocný	nemocný	k2eAgMnSc1d1	nemocný
jakkoli	jakkoli	k6eAd1	jakkoli
namáhat	namáhat	k5eAaImF	namáhat
(	(	kIx(	(
<g/>
ani	ani	k8xC	ani
fyzicky	fyzicky	k6eAd1	fyzicky
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
psychicky	psychicky	k6eAd1	psychicky
<g/>
;	;	kIx,	;
dokonce	dokonce	k9	dokonce
i	i	k9	i
chození	chození	k1gNnSc1	chození
po	po	k7c6	po
bytě	byt	k1gInSc6	byt
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
smrtící	smrtící	k2eAgInSc1d1	smrtící
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postiženému	postižený	k1gMnSc3	postižený
uvolníme	uvolnit	k5eAaPmIp1nP	uvolnit
těsný	těsný	k2eAgInSc1d1	těsný
oděv	oděv	k1gInSc1	oděv
a	a	k8xC	a
uložíme	uložit	k5eAaPmIp1nP	uložit
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
polosedu	polosedu	k6eAd1	polosedu
(	(	kIx(	(
<g/>
s	s	k7c7	s
opřenými	opřený	k2eAgNnPc7d1	opřené
zády	záda	k1gNnPc7	záda
(	(	kIx(	(
<g/>
přinejhorším	přinejhorším	k6eAd1	přinejhorším
opření	opření	k1gNnSc1	opření
o	o	k7c6	o
vlastní	vlastní	k2eAgFnSc6d1	vlastní
ruce	ruka	k1gFnSc6	ruka
<g/>
))	))	k?	))
a	a	k8xC	a
zajistíme	zajistit	k5eAaPmIp1nP	zajistit
přívod	přívod	k1gInSc4	přívod
čerstvého	čerstvý	k2eAgInSc2d1	čerstvý
vzduchu	vzduch	k1gInSc2	vzduch
(	(	kIx(	(
<g/>
otevřeme	otevřít	k5eAaPmIp1nP	otevřít
okno	okno	k1gNnSc4	okno
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
venku	venku	k6eAd1	venku
smog	smog	k1gInSc1	smog
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zavoláme	zavolat	k5eAaPmIp1nP	zavolat
zdravotnickou	zdravotnický	k2eAgFnSc4d1	zdravotnická
záchrannou	záchranný	k2eAgFnSc4d1	záchranná
službu	služba	k1gFnSc4	služba
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
ho	on	k3xPp3gMnSc4	on
nechat	nechat	k5eAaPmF	nechat
rozžvýkat	rozžvýkat	k5eAaPmF	rozžvýkat
půl	půl	k6eAd1	půl
tablety	tableta	k1gFnPc4	tableta
acylpyrinu	acylpyrin	k1gInSc2	acylpyrin
(	(	kIx(	(
<g/>
zastaví	zastavit	k5eAaPmIp3nS	zastavit
zvětšování	zvětšování	k1gNnSc1	zvětšování
sraženiny	sraženina	k1gFnSc2	sraženina
<g/>
;	;	kIx,	;
POZOR	Pozor	k1gMnSc1	Pozor
však	však	k9	však
na	na	k7c4	na
kontraindikace	kontraindikace	k1gFnPc4	kontraindikace
acylpirinu	acylpirin	k1gInSc2	acylpirin
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Užívání	užívání	k1gNnSc1	užívání
kyseliny	kyselina	k1gFnSc2	kyselina
acetylsalicylové	acetylsalicylový	k2eAgNnSc1d1	acetylsalicylové
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
při	při	k7c6	při
známé	známý	k2eAgFnSc6d1	známá
přecitlivělosti	přecitlivělost	k1gFnSc6	přecitlivělost
na	na	k7c4	na
salicyláty	salicylát	k1gInPc4	salicylát
<g/>
,	,	kIx,	,
při	při	k7c6	při
chorobné	chorobný	k2eAgFnSc6d1	chorobná
krvácivosti	krvácivost	k1gFnSc6	krvácivost
<g/>
,	,	kIx,	,
při	při	k7c6	při
chirurgických	chirurgický	k2eAgInPc6d1	chirurgický
zákrocích	zákrok	k1gInPc6	zákrok
spojených	spojený	k2eAgInPc6d1	spojený
s	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
krvácením	krvácení	k1gNnSc7	krvácení
<g/>
,	,	kIx,	,
při	při	k7c6	při
vředové	vředový	k2eAgFnSc6d1	vředová
nemoci	nemoc	k1gFnSc6	nemoc
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
při	při	k7c6	při
průduškovém	průduškový	k2eAgNnSc6d1	průduškové
astmatu	astma	k1gNnSc6	astma
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
třech	tři	k4xCgInPc6	tři
měsících	měsíc	k1gInPc6	měsíc
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
riziku	riziko	k1gNnSc3	riziko
Reyova	Reyův	k2eAgInSc2d1	Reyův
syndromu	syndrom	k1gInSc2	syndrom
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
podávání	podávání	k1gNnSc1	podávání
dětem	dítě	k1gFnPc3	dítě
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
provádíme	provádět	k5eAaImIp1nP	provádět
ošetření	ošetření	k1gNnSc4	ošetření
jako	jako	k9	jako
u	u	k7c2	u
klasického	klasický	k2eAgNnSc2d1	klasické
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
a	a	k8xC	a
pečlivě	pečlivě	k6eAd1	pečlivě
kontrolujeme	kontrolovat	k5eAaImIp1nP	kontrolovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
postižený	postižený	k2eAgMnSc1d1	postižený
stále	stále	k6eAd1	stále
dýchá	dýchat	k5eAaImIp3nS	dýchat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
postižený	postižený	k2eAgMnSc1d1	postižený
nedýchá	dýchat	k5eNaImIp3nS	dýchat
<g/>
,	,	kIx,	,
neodkladně	odkladně	k6eNd1	odkladně
zahajujeme	zahajovat	k5eAaImIp1nP	zahajovat
resuscitaci	resuscitace	k1gFnSc4	resuscitace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
možno	možno	k6eAd1	možno
podat	podat	k5eAaPmF	podat
léky	lék	k1gInPc4	lék
obsahující	obsahující	k2eAgInSc4d1	obsahující
nitroglycerin	nitroglycerin	k1gInSc4	nitroglycerin
a	a	k8xC	a
ještě	ještě	k9	ještě
účinnější	účinný	k2eAgInSc4d2	účinnější
isosorbid	isosorbid	k1gInSc4	isosorbid
dinitrát	dinitrát	k1gInSc1	dinitrát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
oxid	oxid	k1gInSc1	oxid
dusnatý	dusnatý	k2eAgInSc1d1	dusnatý
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
NO	no	k9	no
je	být	k5eAaImIp3nS	být
vasodilatans	vasodilatans	k1gInSc1	vasodilatans
působící	působící	k2eAgInSc1d1	působící
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
na	na	k7c4	na
zúžené	zúžený	k2eAgFnPc4d1	zúžená
koronární	koronární	k2eAgFnPc4d1	koronární
tepny	tepna	k1gFnPc4	tepna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
přednemocniční	přednemocniční	k2eAgFnSc6d1	přednemocniční
fázi	fáze	k1gFnSc6	fáze
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
pacientovi	pacient	k1gMnSc3	pacient
podat	podat	k5eAaPmF	podat
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
acetylsalicylovou	acetylsalicylový	k2eAgFnSc4d1	acetylsalicylová
<g/>
,	,	kIx,	,
klopidogrel	klopidogrel	k1gInSc4	klopidogrel
a	a	k8xC	a
heparin	heparin	k1gInSc4	heparin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
také	také	k9	také
beta-blokátor	betalokátor	k1gInSc1	beta-blokátor
<g/>
,	,	kIx,	,
nitrát	nitrát	k1gInSc1	nitrát
a	a	k8xC	a
léky	lék	k1gInPc1	lék
proti	proti	k7c3	proti
bolesti	bolest	k1gFnSc3	bolest
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
fentanyl	fentanyl	k1gInSc1	fentanyl
i.v.	i.v.	k?	i.v.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pacientovi	pacient	k1gMnSc3	pacient
je	být	k5eAaImIp3nS	být
provedeno	proveden	k2eAgNnSc1d1	provedeno
vyšetření	vyšetření	k1gNnSc1	vyšetření
EKG	EKG	kA	EKG
a	a	k8xC	a
odebrána	odebrán	k2eAgFnSc1d1	odebrána
krev	krev	k1gFnSc1	krev
na	na	k7c6	na
stanovení	stanovení	k1gNnSc6	stanovení
ukazatelů	ukazatel	k1gMnPc2	ukazatel
myokardiální	myokardiální	k2eAgFnSc2d1	myokardiální
nekrózy	nekróza	k1gFnSc2	nekróza
(	(	kIx(	(
<g/>
troponin	troponin	k1gInSc1	troponin
<g/>
,	,	kIx,	,
CK-MB	CK-MB	k1gFnSc1	CK-MB
mass	massa	k1gFnPc2	massa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
pacient	pacient	k1gMnSc1	pacient
se	s	k7c7	s
STEMI	STEMI	kA	STEMI
nebo	nebo	k8xC	nebo
s	s	k7c7	s
NSTEMI	NSTEMI	kA	NSTEMI
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
rizikem	riziko	k1gNnSc7	riziko
by	by	kYmCp3nP	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
ihned	ihned	k6eAd1	ihned
indikován	indikovat	k5eAaBmNgInS	indikovat
do	do	k7c2	do
PCI	PCI	kA	PCI
centra	centrum	k1gNnPc1	centrum
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
p-PCI	p-PCI	k?	p-PCI
(	(	kIx(	(
<g/>
primární	primární	k2eAgFnSc1d1	primární
perkutánní	perkutánní	k2eAgFnSc1d1	perkutánní
koronární	koronární	k2eAgFnSc1d1	koronární
intervence	intervence	k1gFnSc1	intervence
<g/>
;	;	kIx,	;
koronarografie	koronarografie	k1gFnSc1	koronarografie
a	a	k8xC	a
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
koronární	koronární	k2eAgFnSc1d1	koronární
angioplastika	angioplastika	k1gFnSc1	angioplastika
–	–	k?	–
dilatace	dilatace	k1gFnSc2	dilatace
zúžené	zúžený	k2eAgFnSc2d1	zúžená
části	část	k1gFnSc2	část
věnčité	věnčitý	k2eAgFnSc2d1	věnčitá
tepny	tepna	k1gFnSc2	tepna
a	a	k8xC	a
implantace	implantace	k1gFnSc2	implantace
intrakoronárního	intrakoronární	k2eAgInSc2d1	intrakoronární
stentu	stent	k1gInSc2	stent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trombolýza	Trombolýza	k1gFnSc1	Trombolýza
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
menší	malý	k2eAgFnSc4d2	menší
účinnost	účinnost	k1gFnSc4	účinnost
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
p-PCI	p-PCI	k?	p-PCI
dnes	dnes	k6eAd1	dnes
používána	používán	k2eAgFnSc1d1	používána
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
léčebnou	léčebný	k2eAgFnSc7d1	léčebná
modalitou	modalita	k1gFnSc7	modalita
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vytvoření	vytvoření	k1gNnSc4	vytvoření
aortokoronárního	aortokoronární	k2eAgInSc2d1	aortokoronární
bypassu	bypass	k1gInSc2	bypass
na	na	k7c6	na
specializovaném	specializovaný	k2eAgNnSc6d1	specializované
kardiochirurgickém	kardiochirurgický	k2eAgNnSc6d1	Kardiochirurgické
pracovišti	pracoviště	k1gNnSc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Pacient	pacient	k1gMnSc1	pacient
s	s	k7c7	s
NSTEMI	NSTEMI	kA	NSTEMI
se	s	k7c7	s
středním	střední	k2eAgNnSc7d1	střední
rizikem	riziko	k1gNnSc7	riziko
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
PCI	PCI	kA	PCI
podstoupit	podstoupit	k5eAaPmF	podstoupit
do	do	k7c2	do
72	[number]	k4	72
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
Aneurysma	aneurysma	k1gNnSc2	aneurysma
(	(	kIx(	(
<g/>
výduť	výduť	k1gFnSc1	výduť
<g/>
)	)	kIx)	)
vazivové	vazivový	k2eAgFnPc4d1	vazivová
jizvy	jizva	k1gFnPc4	jizva
Trvalé	trvalý	k2eAgNnSc1d1	trvalé
poškození	poškození	k1gNnSc1	poškození
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
-	-	kIx~	-
zhoršení	zhoršení	k1gNnSc3	zhoršení
funkční	funkční	k2eAgFnSc2d1	funkční
kapacity	kapacita	k1gFnSc2	kapacita
srdce	srdce	k1gNnSc2	srdce
Nekouřit	kouřit	k5eNaImF	kouřit
Vyhýbat	vyhýbat	k5eAaImF	vyhýbat
se	se	k3xPyFc4	se
tučným	tučný	k2eAgNnPc3d1	tučné
a	a	k8xC	a
mastným	mastný	k2eAgNnPc3d1	mastné
jídlům	jídlo	k1gNnPc3	jídlo
Pravidelně	pravidelně	k6eAd1	pravidelně
cvičit	cvičit	k5eAaImF	cvičit
Nahrazovat	nahrazovat	k5eAaImF	nahrazovat
živočišné	živočišný	k2eAgInPc4d1	živočišný
tuky	tuk	k1gInPc4	tuk
rostlinnými	rostlinný	k2eAgFnPc7d1	rostlinná
Mít	mít	k5eAaImF	mít
optimální	optimální	k2eAgFnSc4d1	optimální
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
váhu	váha	k1gFnSc4	váha
Nepřetěžovat	přetěžovat	k5eNaImF	přetěžovat
organismus	organismus	k1gInSc4	organismus
(	(	kIx(	(
<g/>
velkou	velká	k1gFnSc7	velká
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
nebo	nebo	k8xC	nebo
psychickou	psychický	k2eAgFnSc7d1	psychická
námahou	námaha	k1gFnSc7	námaha
<g/>
)	)	kIx)	)
</s>
