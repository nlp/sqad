<s>
Andorra	Andorra	k1gFnSc1	Andorra
je	být	k5eAaImIp3nS	být
knížectví	knížectví	k1gNnSc4	knížectví
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Evropě	Evropa	k1gFnSc6	Evropa
ležící	ležící	k2eAgMnPc1d1	ležící
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
na	na	k7c6	na
severu	sever	k1gInSc6	sever
(	(	kIx(	(
<g/>
56,6	[number]	k4	56,6
km	km	kA	km
hranice	hranice	k1gFnSc2	hranice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Španělskem	Španělsko	k1gNnSc7	Španělsko
(	(	kIx(	(
<g/>
63,7	[number]	k4	63,7
km	km	kA	km
hranice	hranice	k1gFnSc2	hranice
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
900	[number]	k4	900
až	až	k9	až
2946	[number]	k4	2946
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
