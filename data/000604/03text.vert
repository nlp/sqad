<p>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
je	být	k5eAaImIp3nS	být
knížectví	knížectví	k1gNnSc4	knížectví
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Evropě	Evropa	k1gFnSc6	Evropa
ležící	ležící	k2eAgMnPc1d1	ležící
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
na	na	k7c6	na
severu	sever	k1gInSc6	sever
(	(	kIx(	(
<g/>
56,6	[number]	k4	56,6
km	km	kA	km
hranice	hranice	k1gFnSc2	hranice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Španělskem	Španělsko	k1gNnSc7	Španělsko
(	(	kIx(	(
<g/>
63,7	[number]	k4	63,7
km	km	kA	km
hranice	hranice	k1gFnSc2	hranice
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
900	[number]	k4	900
až	až	k9	až
2946	[number]	k4	2946
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
malý	malý	k2eAgInSc1d1	malý
stát	stát	k1gInSc1	stát
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
kruhovité	kruhovitý	k2eAgFnSc6d1	kruhovitá
kotlině	kotlina	k1gFnSc6	kotlina
obklopen	obklopen	k2eAgInSc4d1	obklopen
hřebeny	hřeben	k1gInPc4	hřeben
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
nejmenší	malý	k2eAgFnSc7d3	nejmenší
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
podle	podle	k7c2	podle
rozlohy	rozloha	k1gFnSc2	rozloha
(	(	kIx(	(
<g/>
468	[number]	k4	468
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
nejmenší	malý	k2eAgInPc1d3	nejmenší
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
asi	asi	k9	asi
77	[number]	k4	77
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
šestou	šestý	k4xOgFnSc4	šestý
nejmenší	malý	k2eAgFnSc4d3	nejmenší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Andorra	Andorra	k1gFnSc1	Andorra
la	la	k1gNnSc1	la
Vella	Vell	k1gMnSc2	Vell
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Valira	Valir	k1gInSc2	Valir
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvýše	vysoce	k6eAd3	vysoce
položeným	položený	k2eAgNnSc7d1	položené
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
1023	[number]	k4	1023
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Andořané	Andořan	k1gMnPc1	Andořan
jsou	být	k5eAaImIp3nP	být
románská	románský	k2eAgFnSc1d1	románská
etnická	etnický	k2eAgFnSc1d1	etnická
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
katalánského	katalánský	k2eAgInSc2d1	katalánský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
katalánština	katalánština	k1gFnSc1	katalánština
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
mluví	mluvit	k5eAaImIp3nS	mluvit
také	také	k9	také
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
portugalsky	portugalsky	k6eAd1	portugalsky
a	a	k8xC	a
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
několika	několik	k4yIc2	několik
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
jako	jako	k8xC	jako
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
od	od	k7c2	od
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rada	rada	k1gFnSc1	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
přijala	přijmout	k5eAaPmAgFnS	přijmout
však	však	k9	však
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
je	být	k5eAaImIp3nS	být
diarchií	diarchie	k1gFnSc7	diarchie
(	(	kIx(	(
<g/>
dvojvládím	dvojvládí	k1gNnSc7	dvojvládí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
o	o	k7c4	o
roli	role	k1gFnSc4	role
monarchy	monarcha	k1gMnSc2	monarcha
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
dělí	dělit	k5eAaImIp3nS	dělit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
urgellský	urgellský	k2eAgMnSc1d1	urgellský
biskup	biskup	k1gMnSc1	biskup
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
heslo	heslo	k1gNnSc4	heslo
spolukníže	spolukníž	k1gFnSc2	spolukníž
Andorry	Andorra	k1gFnSc2	Andorra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
státu	stát	k1gInSc2	stát
Andorra	Andorra	k1gFnSc1	Andorra
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Urgellské	Urgellský	k2eAgFnSc2d1	Urgellský
diecéze	diecéze	k1gFnSc2	diecéze
s	s	k7c7	s
episkopálním	episkopální	k2eAgNnSc7d1	episkopální
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Urgell	Urgella	k1gFnPc2	Urgella
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Andorru	Andorra	k1gFnSc4	Andorra
navštíví	navštívit	k5eAaPmIp3nS	navštívit
každoročně	každoročně	k6eAd1	každoročně
okolo	okolo	k7c2	okolo
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
měli	mít	k5eAaImAgMnP	mít
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Andorry	Andorra	k1gFnSc2	Andorra
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
očekávanou	očekávaný	k2eAgFnSc4d1	očekávaná
délku	délka	k1gFnSc4	délka
života	život	k1gInSc2	život
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
81	[number]	k4	81
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
803	[number]	k4	803
osvobodil	osvobodit	k5eAaPmAgMnS	osvobodit
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
(	(	kIx(	(
<g/>
742	[number]	k4	742
<g/>
-	-	kIx~	-
<g/>
814	[number]	k4	814
<g/>
)	)	kIx)	)
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
území	území	k1gNnSc2	území
zpod	zpod	k7c2	zpod
nadvlády	nadvláda	k1gFnSc2	nadvláda
muslimských	muslimský	k2eAgMnPc2d1	muslimský
Maurů	Maur	k1gMnPc2	Maur
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
měli	mít	k5eAaImAgMnP	mít
získat	získat	k5eAaPmF	získat
tzv.	tzv.	kA	tzv.
list	list	k1gInSc4	list
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
malém	malý	k2eAgNnSc6d1	malé
pyrenejském	pyrenejský	k2eAgNnSc6d1	pyrenejské
knížectví	knížectví	k1gNnSc6	knížectví
z	z	k7c2	z
roku	rok	k1gInSc2	rok
805	[number]	k4	805
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
Karolinské	karolinský	k2eAgFnSc6d1	Karolinská
listině	listina	k1gFnSc6	listina
<g/>
.	.	kIx.	.
</s>
<s>
Vnuk	vnuk	k1gMnSc1	vnuk
Karla	Karel	k1gMnSc4	Karel
Velikého	veliký	k2eAgMnSc4d1	veliký
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Holý	Holý	k1gMnSc1	Holý
<g/>
,	,	kIx,	,
daroval	darovat	k5eAaPmAgMnS	darovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
843	[number]	k4	843
území	území	k1gNnPc2	území
španělskému	španělský	k2eAgMnSc3d1	španělský
hraběti	hrabě	k1gMnSc3	hrabě
z	z	k7c2	z
Urgellu	Urgell	k1gInSc2	Urgell
<g/>
.	.	kIx.	.
</s>
<s>
Potomci	potomek	k1gMnPc1	potomek
hraběte	hrabě	k1gMnSc2	hrabě
ho	on	k3xPp3gMnSc4	on
následně	následně	k6eAd1	následně
roku	rok	k1gInSc2	rok
1133	[number]	k4	1133
darovali	darovat	k5eAaPmAgMnP	darovat
biskupovi	biskupův	k2eAgMnPc1d1	biskupův
ze	z	k7c2	z
Seo	Seo	k1gFnSc2	Seo
de	de	k?	de
Urgell	Urgell	k1gMnSc1	Urgell
<g/>
.	.	kIx.	.
</s>
<s>
Biskup	biskup	k1gMnSc1	biskup
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
zajištění	zajištění	k1gNnSc3	zajištění
míru	mír	k1gInSc2	mír
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1159	[number]	k4	1159
domluvil	domluvit	k5eAaPmAgInS	domluvit
s	s	k7c7	s
rodem	rod	k1gInSc7	rod
Caboetů	Caboet	k1gMnPc2	Caboet
na	na	k7c6	na
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svěřila	svěřit	k5eAaPmAgFnS	svěřit
Andorru	Andorra	k1gFnSc4	Andorra
jako	jako	k8xS	jako
léno	léno	k1gNnSc1	léno
Caboetům	Caboet	k1gMnPc3	Caboet
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
svrchovaná	svrchovaný	k2eAgFnSc1d1	svrchovaná
moc	moc	k1gFnSc1	moc
náležela	náležet	k5eAaImAgFnS	náležet
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
caboetský	caboetský	k2eAgInSc1d1	caboetský
nárok	nárok	k1gInSc1	nárok
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
,	,	kIx,	,
přešlo	přejít	k5eAaPmAgNnS	přejít
dědictví	dědictví	k1gNnSc4	dědictví
na	na	k7c4	na
hrabata	hrabě	k1gNnPc4	hrabě
z	z	k7c2	z
Foix	Foix	k1gInSc4	Foix
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozporům	rozpor	k1gInPc3	rozpor
s	s	k7c7	s
biskupem	biskup	k1gMnSc7	biskup
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1278	[number]	k4	1278
Andorra	Andorra	k1gFnSc1	Andorra
díky	díky	k7c3	díky
Paréagské	Paréagský	k2eAgFnSc3d1	Paréagský
smlouvě	smlouva	k1gFnSc3	smlouva
potvrzené	potvrzená	k1gFnSc2	potvrzená
papežem	papež	k1gMnSc7	papež
stala	stát	k5eAaPmAgFnS	stát
kondominiem	kondominium	k1gNnSc7	kondominium
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
dvou	dva	k4xCgFnPc2	dva
spoluknížat	spoluknížat	k5eAaImF	spoluknížat
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
prezidenta	prezident	k1gMnSc2	prezident
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
španělského	španělský	k2eAgInSc2d1	španělský
biskupa	biskup	k1gInSc2	biskup
ze	z	k7c2	z
Séo	Séo	k1gFnSc2	Séo
de	de	k?	de
Urgell	Urgell	k1gInSc1	Urgell
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Práva	právo	k1gNnSc2	právo
těchto	tento	k3xDgMnPc2	tento
knížat	kníže	k1gMnPc2wR	kníže
tedy	tedy	k9	tedy
přešla	přejít	k5eAaPmAgFnS	přejít
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
rod	rod	k1gInSc4	rod
hrabat	hrabě	k1gNnPc2	hrabě
z	z	k7c2	z
Foix	Foix	k1gInSc4	Foix
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vymření	vymření	k1gNnSc2	vymření
linie	linie	k1gFnSc2	linie
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Foix	Foix	k1gInSc1	Foix
a	a	k8xC	a
postupným	postupný	k2eAgNnSc7d1	postupné
dědictvím	dědictví	k1gNnSc7	dědictví
(	(	kIx(	(
<g/>
Foix	Foix	k1gInSc1	Foix
→	→	k?	→
Foix-Béarn	Foix-Béarn	k1gInSc1	Foix-Béarn
→	→	k?	→
Foix-Grailly	Foix-Grailla	k1gFnSc2	Foix-Grailla
→	→	k?	→
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Albret	Albreta	k1gFnPc2	Albreta
→	→	k?	→
Bourboni	Bourbon	k1gMnPc1	Bourbon
<g/>
)	)	kIx)	)
na	na	k7c4	na
Jindřicha	Jindřich	k1gMnSc4	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1589	[number]	k4	1589
<g/>
–	–	k?	–
<g/>
1610	[number]	k4	1610
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
na	na	k7c4	na
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
korunu	koruna	k1gFnSc4	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1419	[number]	k4	1419
bylo	být	k5eAaImAgNnS	být
jak	jak	k6eAd1	jak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Aragonu	Aragona	k1gFnSc4	Aragona
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
požadavkům	požadavek	k1gInPc3	požadavek
na	na	k7c4	na
samosprávu	samospráva	k1gFnSc4	samospráva
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
tzv.	tzv.	kA	tzv.
Zemské	zemský	k2eAgFnSc2d1	zemská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
předchůdkyni	předchůdkyně	k1gFnSc4	předchůdkyně
současné	současný	k2eAgFnSc2d1	současná
Generální	generální	k2eAgFnSc2d1	generální
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
byl	být	k5eAaImAgInS	být
protektorát	protektorát	k1gInSc1	protektorát
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byly	být	k5eAaImAgFnP	být
paréagské	paréagský	k2eAgFnPc1d1	paréagský
dohody	dohoda	k1gFnPc1	dohoda
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
feudální	feudální	k2eAgInSc4d1	feudální
přežitek	přežitek	k1gInSc4	přežitek
<g/>
.	.	kIx.	.
</s>
<s>
Překvapivě	překvapivě	k6eAd1	překvapivě
sama	sám	k3xTgFnSc1	sám
andorrská	andorrský	k2eAgFnSc1d1	andorrská
samospráva	samospráva	k1gFnSc1	samospráva
požádala	požádat	k5eAaPmAgFnS	požádat
Napoleona	Napoleon	k1gMnSc4	Napoleon
Bonaparta	Bonaparta	k1gFnSc1	Bonaparta
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
francouzské	francouzský	k2eAgFnSc2d1	francouzská
suverenity	suverenita	k1gFnSc2	suverenita
<g/>
.	.	kIx.	.
</s>
<s>
Reformní	reformní	k2eAgNnPc1d1	reformní
hnutí	hnutí	k1gNnPc1	hnutí
v	v	k7c6	v
letech	let	k1gInPc6	let
1866	[number]	k4	1866
<g/>
–	–	k?	–
<g/>
1868	[number]	k4	1868
omezilo	omezit	k5eAaPmAgNnS	omezit
biskupskou	biskupský	k2eAgFnSc4d1	biskupská
moc	moc	k1gFnSc4	moc
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Nová	nový	k2eAgFnSc1d1	nová
reforma	reforma	k1gFnSc1	reforma
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
jak	jak	k6eAd1	jak
biskup	biskup	k1gMnSc1	biskup
z	z	k7c2	z
Urgell	Urgella	k1gFnPc2	Urgella
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Napoleon	Napoleon	k1gMnSc1	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Andorra	Andorra	k1gFnSc1	Andorra
symbolicky	symbolicky	k6eAd1	symbolicky
válku	válka	k1gFnSc4	válka
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
byla	být	k5eAaImAgFnS	být
krátce	krátce	k6eAd1	krátce
okupována	okupovat	k5eAaBmNgFnS	okupovat
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
v	v	k7c6	v
Urgellu	Urgell	k1gInSc6	Urgell
bělogvardějec	bělogvardějec	k1gMnSc1	bělogvardějec
Boris	Boris	k1gMnSc1	Boris
Skossyreff	Skossyreff	k1gMnSc1	Skossyreff
(	(	kIx(	(
<g/>
Skosyrev	Skosyrev	k1gFnSc1	Skosyrev
<g/>
)	)	kIx)	)
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
suverénním	suverénní	k2eAgMnSc7d1	suverénní
knížetem	kníže	k1gMnSc7	kníže
Andorry	Andorra	k1gFnSc2	Andorra
jako	jako	k8xC	jako
Boris	Boris	k1gMnSc1	Boris
I.	I.	kA	I.
a	a	k8xC	a
regentem	regens	k1gMnSc7	regens
za	za	k7c4	za
Jeho	jeho	k3xOp3gNnPc4	jeho
Veličenstvo	veličenstvo	k1gNnSc4	veličenstvo
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
válku	válka	k1gFnSc4	válka
biskupovi	biskupův	k2eAgMnPc1d1	biskupův
ze	z	k7c2	z
Seo	Seo	k1gFnSc2	Seo
de	de	k?	de
Urgell	Urgell	k1gMnSc1	Urgell
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
však	však	k9	však
španělskými	španělský	k2eAgInPc7d1	španělský
úřady	úřad	k1gInPc7	úřad
zatčen	zatknout	k5eAaPmNgInS	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
ruské	ruský	k2eAgInPc1d1	ruský
zdroje	zdroj	k1gInPc1	zdroj
ale	ale	k9	ale
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vládl	vládnout	k5eAaImAgInS	vládnout
Andoře	Andorra	k1gFnSc3	Andorra
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
volební	volební	k2eAgNnSc1d1	volební
právo	právo	k1gNnSc1	právo
pro	pro	k7c4	pro
hlavy	hlava	k1gFnPc4	hlava
místních	místní	k2eAgFnPc2d1	místní
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
schválilo	schválit	k5eAaPmAgNnS	schválit
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
Andorra	Andorra	k1gFnSc1	Andorra
stala	stát	k5eAaPmAgFnS	stát
suverénním	suverénní	k2eAgNnSc7d1	suverénní
parlamentním	parlamentní	k2eAgNnSc7d1	parlamentní
knížectvím	knížectví	k1gNnSc7	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
nahradila	nahradit	k5eAaPmAgFnS	nahradit
smlouvu	smlouva	k1gFnSc4	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1278	[number]	k4	1278
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
španělský	španělský	k2eAgMnSc1d1	španělský
biskup	biskup	k1gMnSc1	biskup
Seu	Seu	k1gMnSc1	Seu
de	de	k?	de
Urgell	Urgell	k1gMnSc1	Urgell
a	a	k8xC	a
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Foix	Foix	k1gInSc4	Foix
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
<g/>
)	)	kIx)	)
dělili	dělit	k5eAaImAgMnP	dělit
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Státní	státní	k2eAgNnSc1d1	státní
zřízení	zřízení	k1gNnSc1	zřízení
==	==	k?	==
</s>
</p>
<p>
<s>
Andorrské	Andorrský	k2eAgFnPc1d1	Andorrská
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
vlastní	vlastní	k2eAgFnPc4d1	vlastní
reformy	reforma	k1gFnPc4	reforma
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
reformní	reformní	k2eAgInSc1d1	reformní
zákon	zákon	k1gInSc1	zákon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ustanovoval	ustanovovat	k5eAaImAgInS	ustanovovat
Výkonnou	výkonný	k2eAgFnSc4d1	výkonná
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
odděloval	oddělovat	k5eAaImAgMnS	oddělovat
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
od	od	k7c2	od
legislativní	legislativní	k2eAgFnSc2d1	legislativní
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
feudální	feudální	k2eAgInSc1d1	feudální
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
modifikován	modifikovat	k5eAaBmNgInS	modifikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
když	když	k8xS	když
Andořané	Andořan	k1gMnPc1	Andořan
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
schválili	schválit	k5eAaPmAgMnP	schválit
první	první	k4xOgFnSc4	první
psanou	psaný	k2eAgFnSc4d1	psaná
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
reformní	reformní	k2eAgFnPc1d1	reformní
snahy	snaha	k1gFnPc1	snaha
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1993	[number]	k4	1993
je	být	k5eAaImIp3nS	být
Andorra	Andorra	k1gFnSc1	Andorra
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
demokracií	demokracie	k1gFnSc7	demokracie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
titulárními	titulární	k2eAgFnPc7d1	titulární
hlavami	hlava	k1gFnPc7	hlava
státu	stát	k1gInSc2	stát
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
francouzskými	francouzský	k2eAgMnPc7d1	francouzský
občany	občan	k1gMnPc7	občan
volený	volený	k2eAgMnSc1d1	volený
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
papežem	papež	k1gMnSc7	papež
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
biskup	biskup	k1gMnSc1	biskup
ze	z	k7c2	z
Séo	Séo	k1gFnSc2	Séo
de	de	k?	de
Urgell	Urgell	k1gMnSc1	Urgell
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Andoře	Andorra	k1gFnSc6	Andorra
zastupováni	zastupovat	k5eAaImNgMnP	zastupovat
místními	místní	k2eAgMnPc7d1	místní
zástupci	zástupce	k1gMnPc7	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
osobní	osobní	k2eAgMnPc1d1	osobní
reprezentanti	reprezentant	k1gMnPc1	reprezentant
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
vikáři	vikář	k1gMnPc1	vikář
<g/>
.	.	kIx.	.
</s>
<s>
Spoluknížata	Spoluknížata	k1gFnSc1	Spoluknížata
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
oficiálně	oficiálně	k6eAd1	oficiálně
setkala	setkat	k5eAaPmAgFnS	setkat
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Španělskem	Španělsko	k1gNnSc7	Španělsko
a	a	k8xC	a
Andorrou	Andorra	k1gFnSc7	Andorra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
pozici	pozice	k1gFnSc4	pozice
biskupa	biskup	k1gMnSc2	biskup
ze	z	k7c2	z
Séo	Séo	k1gFnSc2	Séo
de	de	k?	de
Urgell	Urgell	k1gInSc1	Urgell
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
působícím	působící	k2eAgInSc7d1	působící
biskupem	biskup	k1gInSc7	biskup
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
hlavou	hlava	k1gFnSc7	hlava
andorrského	andorrský	k2eAgNnSc2d1	andorrský
knížectví	knížectví	k1gNnSc2	knížectví
jako	jako	k8xS	jako
mezinárodně	mezinárodně	k6eAd1	mezinárodně
chráněnou	chráněný	k2eAgFnSc4d1	chráněná
osobu	osoba	k1gFnSc4	osoba
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
imunitou	imunita	k1gFnSc7	imunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spoluknížatům	Spoluknížat	k1gMnPc3	Spoluknížat
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
reprezentativní	reprezentativní	k2eAgFnPc4d1	reprezentativní
a	a	k8xC	a
symbolické	symbolický	k2eAgFnPc4d1	symbolická
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
legislativní	legislativní	k2eAgFnPc1d1	legislativní
a	a	k8xC	a
exekutivní	exekutivní	k2eAgFnPc1d1	exekutivní
pravomoci	pravomoc	k1gFnPc1	pravomoc
byly	být	k5eAaImAgFnP	být
přeneseny	přenést	k5eAaPmNgFnP	přenést
na	na	k7c4	na
Generální	generální	k2eAgFnSc4d1	generální
radu	rada	k1gFnSc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
stále	stále	k6eAd1	stále
spoluknížata	spolukníže	k1gNnPc4	spolukníže
zodpovědná	zodpovědný	k2eAgNnPc4d1	zodpovědné
např.	např.	kA	např.
za	za	k7c4	za
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
jmenování	jmenování	k1gNnSc4	jmenování
premiéra	premiér	k1gMnSc2	premiér
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jednají	jednat	k5eAaImIp3nP	jednat
víceméně	víceméně	k9	víceméně
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
andorrských	andorrský	k2eAgMnPc6d1	andorrský
politicích	politik	k1gMnPc6	politik
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgFnSc1d1	generální
rada	rada	k1gFnSc1	rada
je	být	k5eAaImIp3nS	být
andorrským	andorrský	k2eAgInSc7d1	andorrský
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednokomorová	jednokomorový	k2eAgFnSc1d1	jednokomorová
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
mezi	mezi	k7c7	mezi
28	[number]	k4	28
a	a	k8xC	a
42	[number]	k4	42
členy	člen	k1gInPc7	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
polovina	polovina	k1gFnSc1	polovina
volena	volit	k5eAaImNgFnS	volit
celostátně	celostátně	k6eAd1	celostátně
a	a	k8xC	a
polovina	polovina	k1gFnSc1	polovina
je	být	k5eAaImIp3nS	být
zastupiteli	zastupitel	k1gMnPc7	zastupitel
zvolenými	zvolený	k2eAgMnPc7d1	zvolený
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
farnosti	farnost	k1gFnSc6	farnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vládu	vláda	k1gFnSc4	vláda
(	(	kIx(	(
<g/>
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
radu	rada	k1gFnSc4	rada
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
premiér	premiér	k1gMnSc1	premiér
(	(	kIx(	(
<g/>
volený	volený	k2eAgMnSc1d1	volený
generální	generální	k2eAgMnSc1d1	generální
radou	rada	k1gMnSc7	rada
a	a	k8xC	a
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
spoluknížaty	spoluknížat	k1gMnPc7	spoluknížat
<g/>
)	)	kIx)	)
a	a	k8xC	a
ministři	ministr	k1gMnPc1	ministr
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
sám	sám	k3xTgMnSc1	sám
premiér	premiér	k1gMnSc1	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
premiérem	premiér	k1gMnSc7	premiér
je	být	k5eAaImIp3nS	být
Antoni	Anton	k1gMnPc1	Anton
Martí	Martý	k2eAgMnPc1d1	Martý
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
Andoře	Andorra	k1gFnSc6	Andorra
získaly	získat	k5eAaPmAgFnP	získat
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
svátek	svátek	k1gInSc1	svátek
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
(	(	kIx(	(
<g/>
Svátek	svátek	k1gInSc4	svátek
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgFnSc1d1	neutrální
stát	stát	k5eAaPmF	stát
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
vojenského	vojenský	k2eAgMnSc2d1	vojenský
náčelníka	náčelník	k1gMnSc2	náčelník
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
nepřátelské	přátelský	k2eNgFnSc3d1	nepřátelská
intervenci	intervence	k1gFnSc3	intervence
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
andorrské	andorrský	k2eAgFnSc2d1	andorrská
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
dobrém	dobrý	k2eAgNnSc6d1	dobré
sousedství	sousedství	k1gNnSc6	sousedství
<g/>
,	,	kIx,	,
přátelství	přátelství	k1gNnSc6	přátelství
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
neovlivňovat	ovlivňovat	k5eNaImF	ovlivňovat
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
vztahy	vztah	k1gInPc4	vztah
Andorry	Andorra	k1gFnSc2	Andorra
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
povinností	povinnost	k1gFnSc7	povinnost
mužů	muž	k1gMnPc2	muž
od	od	k7c2	od
16	[number]	k4	16
do	do	k7c2	do
60	[number]	k4	60
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
zacházet	zacházet	k5eAaImF	zacházet
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
<g/>
Andorra	Andorra	k1gFnSc1	Andorra
sdílí	sdílet	k5eAaImIp3nS	sdílet
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Španělskem	Španělsko	k1gNnSc7	Španělsko
zastoupení	zastoupení	k1gNnSc2	zastoupení
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nemá	mít	k5eNaImIp3nS	mít
Andorra	Andorra	k1gFnSc1	Andorra
vlastní	vlastní	k2eAgFnSc2d1	vlastní
konzulát	konzulát	k1gInSc4	konzulát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
7	[number]	k4	7
farností	farnost	k1gFnPc2	farnost
(	(	kIx(	(
<g/>
parrò	parrò	k?	parrò
<g/>
,	,	kIx,	,
jednotné	jednotný	k2eAgNnSc1d1	jednotné
číslo	číslo	k1gNnSc1	číslo
parrò	parrò	k?	parrò
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
la	la	k1gNnSc2	la
Vella	Vello	k1gNnSc2	Vello
</s>
</p>
<p>
<s>
Canillo	Canillo	k1gNnSc1	Canillo
</s>
</p>
<p>
<s>
Encamp	Encamp	k1gMnSc1	Encamp
</s>
</p>
<p>
<s>
Escaldes-Engordany	Escaldes-Engordana	k1gFnPc1	Escaldes-Engordana
</s>
</p>
<p>
<s>
La	la	k1gNnSc1	la
Massana	Massan	k1gMnSc2	Massan
</s>
</p>
<p>
<s>
Ordino	Ordino	k1gNnSc1	Ordino
</s>
</p>
<p>
<s>
Sant	Sant	k2eAgMnSc1d1	Sant
Julià	Julià	k1gMnSc1	Julià
de	de	k?	de
Lò	Lò	k1gMnSc1	Lò
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
mezi	mezi	k7c7	mezi
Španělskem	Španělsko	k1gNnSc7	Španělsko
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
468	[number]	k4	468
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
nejmenším	malý	k2eAgInSc7d3	nejmenší
státem	stát	k1gInSc7	stát
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
velkým	velký	k2eAgInSc7d1	velký
téměř	téměř	k6eAd1	téměř
jako	jako	k9	jako
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
horami	hora	k1gFnPc7	hora
dosahujícími	dosahující	k2eAgFnPc7d1	dosahující
3000	[number]	k4	3000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Údolím	údolí	k1gNnSc7	údolí
řeky	řeka	k1gFnSc2	řeka
Valira	Valir	k1gInSc2	Valir
probíhá	probíhat	k5eAaImIp3nS	probíhat
hranice	hranice	k1gFnPc4	hranice
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
země	zem	k1gFnSc2	zem
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
polohách	poloha	k1gFnPc6	poloha
nad	nad	k7c7	nad
hranicí	hranice	k1gFnSc7	hranice
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
jen	jen	k9	jen
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
oblastí	oblast	k1gFnPc2	oblast
položených	položený	k2eAgFnPc6d1	položená
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Na	na	k7c6	na
zalesněných	zalesněný	k2eAgInPc6d1	zalesněný
svazích	svah	k1gInPc6	svah
převažuje	převažovat	k5eAaImIp3nS	převažovat
borovice	borovice	k1gFnSc1	borovice
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k9	jako
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
jenom	jenom	k6eAd1	jenom
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
velkým	velký	k2eAgFnPc3d1	velká
nadmořským	nadmořský	k2eAgFnPc3d1	nadmořská
výškám	výška	k1gFnPc3	výška
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
víc	hodně	k6eAd2	hodně
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
chladnější	chladný	k2eAgNnPc4d2	chladnější
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Coma	Com	k2eAgFnSc1d1	Coma
Pedrosa	Pedrosa	k1gFnSc1	Pedrosa
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
2946	[number]	k4	2946
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
Riu	Riu	k1gFnSc4	Riu
Runer	Runra	k1gFnPc2	Runra
v	v	k7c6	v
výšce	výška	k1gFnSc6	výška
840	[number]	k4	840
m	m	kA	m
n.	n.	k?	n.
n.	n.	k?	n.
Nejvýše	nejvýše	k6eAd1	nejvýše
položené	položený	k2eAgNnSc4d1	položené
místo	místo	k1gNnSc4	místo
je	být	k5eAaImIp3nS	být
Coma	Com	k2eAgFnSc1d1	Coma
Pedrosa	Pedrosa	k1gFnSc1	Pedrosa
<g/>
,	,	kIx,	,
2946	[number]	k4	2946
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Andorra	Andorra	k1gFnSc1	Andorra
není	být	k5eNaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
celní	celní	k2eAgFnSc2d1	celní
unie	unie	k1gFnSc2	unie
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
průmyslovými	průmyslový	k2eAgInPc7d1	průmyslový
výrobky	výrobek	k1gInPc7	výrobek
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
člena	člen	k1gMnSc4	člen
EU	EU	kA	EU
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
zemědělskými	zemědělský	k2eAgInPc7d1	zemědělský
výrobky	výrobek	k1gInPc7	výrobek
pak	pak	k6eAd1	pak
za	za	k7c4	za
nečlena	nečlen	k1gMnSc4	nečlen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
je	být	k5eAaImIp3nS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
jen	jen	k9	jen
1	[number]	k4	1
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc4	chov
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
je	být	k5eAaImIp3nS	být
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Andorru	Andorra	k1gFnSc4	Andorra
až	až	k9	až
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
přijíždějí	přijíždět	k5eAaImIp3nP	přijíždět
zalyžovat	zalyžovat	k5eAaPmF	zalyžovat
a	a	k8xC	a
především	především	k9	především
za	za	k7c4	za
nákupy	nákup	k1gInPc4	nákup
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
využívají	využívat	k5eAaPmIp3nP	využívat
možnosti	možnost	k1gFnPc1	možnost
nákupu	nákup	k1gInSc2	nákup
zboží	zboží	k1gNnSc2	zboží
bez	bez	k7c2	bez
daní	daň	k1gFnPc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
daňovým	daňový	k2eAgInPc3d1	daňový
rájům	ráj	k1gInPc3	ráj
<g/>
.	.	kIx.	.
</s>
<s>
Neplatí	platit	k5eNaImIp3nS	platit
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
žádné	žádný	k3yNgFnPc4	žádný
přímé	přímý	k2eAgFnPc4d1	přímá
daně	daň	k1gFnPc4	daň
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
4	[number]	k4	4
<g/>
%	%	kIx~	%
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
přidané	přidaný	k2eAgFnSc2d1	přidaná
hodnoty	hodnota	k1gFnSc2	hodnota
(	(	kIx(	(
<g/>
nově	nově	k6eAd1	nově
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
42	[number]	k4	42
500	[number]	k4	500
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
v	v	k7c6	v
Andoře	Andorra	k1gFnSc6	Andorra
platilo	platit	k5eAaImAgNnS	platit
jak	jak	k6eAd1	jak
španělskými	španělský	k2eAgFnPc7d1	španělská
pesetami	peseta	k1gFnPc7	peseta
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
francouzskými	francouzský	k2eAgInPc7d1	francouzský
franky	frank	k1gInPc7	frank
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
platí	platit	k5eAaImIp3nP	platit
eury	euro	k1gNnPc7	euro
<g/>
.	.	kIx.	.
</s>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
razí	razit	k5eAaImIp3nP	razit
vlastní	vlastní	k2eAgInPc4d1	vlastní
euromince	eurominec	k1gInPc4	eurominec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
77	[number]	k4	77
281	[number]	k4	281
(	(	kIx(	(
<g/>
k	k	k7c3	k
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
Andorru	Andorra	k1gFnSc4	Andorra
obývalo	obývat	k5eAaImAgNnS	obývat
pouze	pouze	k6eAd1	pouze
5000	[number]	k4	5000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
33	[number]	k4	33
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
Andořané	Andořan	k1gMnPc1	Andořan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
etnickým	etnický	k2eAgInSc7d1	etnický
původem	původ	k1gInSc7	původ
Katalánci	Katalánec	k1gMnPc1	Katalánec
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
Španělé	Španěl	k1gMnPc1	Španěl
43	[number]	k4	43
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Portugalci	Portugalec	k1gMnSc3	Portugalec
11	[number]	k4	11
<g/>
%	%	kIx~	%
a	a	k8xC	a
Francouzi	Francouz	k1gMnSc3	Francouz
7,5	[number]	k4	7,5
<g/>
%	%	kIx~	%
Jediným	jediný	k2eAgInSc7d1	jediný
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
katalánština	katalánština	k1gFnSc1	katalánština
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
španělština	španělština	k1gFnSc1	španělština
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
běžně	běžně	k6eAd1	běžně
užívány	užíván	k2eAgFnPc1d1	užívána
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
také	také	k9	také
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
používáním	používání	k1gNnSc7	používání
angličtiny	angličtina	k1gFnSc2	angličtina
a	a	k8xC	a
portugalštiny	portugalština	k1gFnSc2	portugalština
<g/>
.	.	kIx.	.
</s>
<s>
Andorrská	Andorrský	k2eAgFnSc1d1	Andorrská
vláda	vláda	k1gFnSc1	vláda
nicméně	nicméně	k8xC	nicméně
silně	silně	k6eAd1	silně
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
používání	používání	k1gNnSc4	používání
katalánštiny	katalánština	k1gFnSc2	katalánština
<g/>
.	.	kIx.	.
</s>
<s>
Financuje	financovat	k5eAaBmIp3nS	financovat
komisi	komise	k1gFnSc4	komise
pro	pro	k7c4	pro
katalánskou	katalánský	k2eAgFnSc4d1	katalánská
toponymii	toponymie	k1gFnSc4	toponymie
(	(	kIx(	(
<g/>
la	la	k1gNnSc4	la
Comissió	Comissió	k1gFnSc2	Comissió
de	de	k?	de
Toponímia	Toponímia	k1gFnSc1	Toponímia
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Andorra	Andorra	k1gFnSc1	Andorra
<g/>
)	)	kIx)	)
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
bezplatné	bezplatný	k2eAgInPc4d1	bezplatný
kurzy	kurz	k1gInPc4	kurz
katalánštiny	katalánština	k1gFnSc2	katalánština
přistěhovalcům	přistěhovalec	k1gMnPc3	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Andorrské	Andorrský	k2eAgFnPc1d1	Andorrská
televizní	televizní	k2eAgFnPc1d1	televizní
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgFnPc1d1	rozhlasová
stanice	stanice	k1gFnPc1	stanice
používají	používat	k5eAaImIp3nP	používat
výhradně	výhradně	k6eAd1	výhradně
katalánštinu	katalánština	k1gFnSc4	katalánština
<g/>
.	.	kIx.	.
</s>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
Monakem	Monako	k1gNnSc7	Monako
a	a	k8xC	a
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nikdy	nikdy	k6eAd1	nikdy
nepodepsaly	podepsat	k5eNaPmAgInP	podepsat
Rámcovou	rámcový	k2eAgFnSc4d1	rámcová
úmluvu	úmluva	k1gFnSc4	úmluva
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
o	o	k7c6	o
národnostních	národnostní	k2eAgFnPc6d1	národnostní
menšinách	menšina	k1gFnPc6	menšina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
88.2	[number]	k4	88.2
<g/>
%	%	kIx~	%
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
<g/>
Dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
nemají	mít	k5eNaImIp3nP	mít
andorrskou	andorrský	k2eAgFnSc4d1	andorrská
státní	státní	k2eAgFnSc4d1	státní
příslušnost	příslušnost	k1gFnSc4	příslušnost
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
tudíž	tudíž	k8xC	tudíž
právo	právo	k1gNnSc4	právo
volit	volit	k5eAaImF	volit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
nesmějí	smát	k5eNaImIp3nP	smát
být	být	k5eAaImF	být
zvoleni	zvolit	k5eAaPmNgMnP	zvolit
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vlastnit	vlastnit	k5eAaImF	vlastnit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
33	[number]	k4	33
<g/>
%	%	kIx~	%
základního	základní	k2eAgInSc2d1	základní
kapitálu	kapitál	k1gInSc2	kapitál
soukromé	soukromý	k2eAgFnSc2d1	soukromá
obchodní	obchodní	k2eAgFnSc2d1	obchodní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
andorrským	andorrský	k2eAgMnSc7d1	andorrský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
je	být	k5eAaImIp3nS	být
Albert	Albert	k1gMnSc1	Albert
Salvadó	Salvadó	k1gMnSc1	Salvadó
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
historických	historický	k2eAgInPc2d1	historický
románů	román	k1gInPc2	román
i	i	k8xC	i
detektivek	detektivka	k1gFnPc2	detektivka
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
katalánštině	katalánština	k1gFnSc6	katalánština
i	i	k8xC	i
španělštině	španělština	k1gFnSc6	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Organizátorem	organizátor	k1gMnSc7	organizátor
andorrského	andorrský	k2eAgInSc2d1	andorrský
literárního	literární	k2eAgInSc2d1	literární
života	život	k1gInSc2	život
byl	být	k5eAaImAgInS	být
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Rossend	Rossend	k1gMnSc1	Rossend
Marsol	Marsola	k1gFnPc2	Marsola
Clua	Clua	k1gMnSc1	Clua
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
proti-francovské	protirancovský	k2eAgInPc4d1	proti-francovský
postoje	postoj	k1gInPc4	postoj
a	a	k8xC	a
katalánské	katalánský	k2eAgNnSc4d1	katalánské
vlastenectví	vlastenectví	k1gNnSc4	vlastenectví
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
andorrskou	andorrský	k2eAgFnSc7d1	andorrská
hudební	hudební	k2eAgFnSc7d1	hudební
skupinou	skupina	k1gFnSc7	skupina
je	být	k5eAaImIp3nS	být
metalová	metalový	k2eAgFnSc1d1	metalová
Persefone	Persefone	k1gFnSc1	Persefone
<g/>
.	.	kIx.	.
</s>
<s>
Udržován	udržován	k2eAgInSc1d1	udržován
je	být	k5eAaImIp3nS	být
i	i	k9	i
folklór	folklór	k1gInSc1	folklór
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
lidové	lidový	k2eAgInPc4d1	lidový
tance	tanec	k1gInPc4	tanec
contrapà	contrapà	k?	contrapà
a	a	k8xC	a
marratxa	marratx	k1gInSc2	marratx
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sant	Sant	k2eAgMnSc1d1	Sant
Julià	Julià	k1gMnSc1	Julià
de	de	k?	de
Lò	Lò	k1gMnSc1	Lò
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Katalánci	Katalánec	k1gMnPc7	Katalánec
Andořané	Andořan	k1gMnPc1	Andořan
udržují	udržovat	k5eAaImIp3nP	udržovat
tradici	tradice	k1gFnSc4	tradice
tance	tanec	k1gInSc2	tanec
zvaného	zvaný	k2eAgMnSc2d1	zvaný
sardana	sardan	k1gMnSc2	sardan
<g/>
.	.	kIx.	.
</s>
<s>
Patronkou	patronka	k1gFnSc7	patronka
Andorry	Andorra	k1gFnSc2	Andorra
je	být	k5eAaImIp3nS	být
Panna	Panna	k1gFnSc1	Panna
Maria	Maria	k1gFnSc1	Maria
Meritxellská	Meritxellský	k2eAgFnSc1d1	Meritxellský
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
svátek	svátek	k1gInSc1	svátek
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
je	být	k5eAaImIp3nS	být
také	také	k9	také
největším	veliký	k2eAgInSc7d3	veliký
svátkem	svátek	k1gInSc7	svátek
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
disponuje	disponovat	k5eAaBmIp3nS	disponovat
řadou	řada	k1gFnSc7	řada
velmi	velmi	k6eAd1	velmi
starých	starý	k2eAgFnPc2d1	stará
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
románských	románský	k2eAgMnPc2d1	románský
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
patří	patřit	k5eAaImIp3nS	patřit
kostel	kostel	k1gInSc4	kostel
Sant	Sant	k2eAgMnSc1d1	Sant
Joan	Joan	k1gMnSc1	Joan
de	de	k?	de
Caselles	Caselles	k1gMnSc1	Caselles
nedaleko	nedaleko	k7c2	nedaleko
Canilla	Canillo	k1gNnSc2	Canillo
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc4	kostel
Sant	Sant	k2eAgInSc4d1	Sant
Martí	Marť	k1gFnSc7	Marť
v	v	k7c6	v
La	la	k1gNnSc6	la
Cortinada	Cortinada	k1gFnSc1	Cortinada
nebo	nebo	k8xC	nebo
kostel	kostel	k1gInSc1	kostel
v	v	k7c4	v
Santa	Sant	k1gMnSc4	Sant
Coloma	Colom	k1gMnSc4	Colom
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
státní	státní	k2eAgFnSc1d1	státní
Andorrská	Andorrský	k2eAgFnSc1d1	Andorrská
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
Universitat	Universitat	k1gFnSc1	Universitat
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Andorra	Andorra	k1gFnSc1	Andorra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rozvinuto	rozvinout	k5eAaPmNgNnS	rozvinout
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
virtuální	virtuální	k2eAgFnSc4d1	virtuální
vyučování	vyučování	k1gNnSc4	vyučování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sportu	sport	k1gInSc2	sport
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
Andořané	Andořan	k1gMnPc1	Andořan
největších	veliký	k2eAgInPc2d3	veliký
úspěchů	úspěch	k1gInPc2	úspěch
ve	v	k7c6	v
skialpinismu	skialpinismus	k1gInSc6	skialpinismus
<g/>
,	,	kIx,	,
Ariadna	Ariadna	k1gFnSc1	Ariadna
Tudel	Tudela	k1gFnPc2	Tudela
Cuberesová	Cuberesový	k2eAgFnSc1d1	Cuberesový
a	a	k8xC	a
Sophie	Sophie	k1gFnSc1	Sophie
Dusautoir	Dusautoira	k1gFnPc2	Dusautoira
Bertrandová	Bertrandový	k2eAgFnSc1d1	Bertrandová
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
sportu	sport	k1gInSc6	sport
bronzové	bronzový	k2eAgFnSc2d1	bronzová
medaile	medaile	k1gFnSc2	medaile
z	z	k7c2	z
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Sjezdový	sjezdový	k2eAgMnSc1d1	sjezdový
lyžař	lyžař	k1gMnSc1	lyžař
Marc	Marc	k1gFnSc4	Marc
Oliveras	Oliveras	k1gInSc1	Oliveras
získal	získat	k5eAaPmAgInS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
zimní	zimní	k2eAgFnSc6d1	zimní
univerziádě	univerziáda	k1gFnSc6	univerziáda
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
Carmina	Carmin	k2eAgFnSc1d1	Carmina
Pallasová	Pallasová	k1gFnSc1	Pallasová
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
akci	akce	k1gFnSc6	akce
získala	získat	k5eAaPmAgFnS	získat
stříbro	stříbro	k1gNnSc4	stříbro
a	a	k8xC	a
bronz	bronz	k1gInSc4	bronz
<g/>
.	.	kIx.	.
</s>
<s>
Běžec	běžec	k1gMnSc1	běžec
Antoni	Anton	k1gMnPc1	Anton
Bernadó	Bernadó	k1gMnSc1	Bernadó
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
jediným	jediný	k2eAgMnSc7d1	jediný
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
běžel	běžet	k5eAaImAgMnS	běžet
pět	pět	k4xCc4	pět
olympijských	olympijský	k2eAgInPc2d1	olympijský
závodů	závod	k1gInPc2	závod
v	v	k7c6	v
maratónu	maratón	k1gInSc6	maratón
<g/>
.	.	kIx.	.
</s>
<s>
Basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
BC	BC	kA	BC
Andorra	Andorra	k1gFnSc1	Andorra
hraje	hrát	k5eAaImIp3nS	hrát
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
španělskou	španělský	k2eAgFnSc4d1	španělská
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Rugbyový	Rugbyový	k2eAgInSc1d1	Rugbyový
klub	klub	k1gInSc1	klub
VPC	VPC	kA	VPC
Andorra	Andorra	k1gFnSc1	Andorra
XV	XV	kA	XV
zase	zase	k9	zase
hraje	hrát	k5eAaImIp3nS	hrát
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
první	první	k4xOgFnSc4	první
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Specifickou	specifický	k2eAgFnSc7d1	specifická
sportovní	sportovní	k2eAgFnSc7d1	sportovní
disciplínou	disciplína	k1gFnSc7	disciplína
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
provozovanou	provozovaný	k2eAgFnSc7d1	provozovaná
i	i	k8xC	i
v	v	k7c6	v
Katalánsku	Katalánsko	k1gNnSc6	Katalánsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
castell	castell	k1gInSc1	castell
<g/>
,	,	kIx,	,
stavění	stavění	k1gNnSc1	stavění
věží	věž	k1gFnPc2	věž
z	z	k7c2	z
lidských	lidský	k2eAgNnPc2d1	lidské
těl	tělo	k1gNnPc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Andorra	Andorra	k1gFnSc1	Andorra
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-02-08	[number]	k4	2011-02-08
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Andorra	Andorra	k1gFnSc1	Andorra
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Andorra	Andorra	k1gFnSc1	Andorra
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2010-12-15	[number]	k4	2010-12-15
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RODRIGUEZ	RODRIGUEZ	kA	RODRIGUEZ
<g/>
,	,	kIx,	,
Vicente	Vicent	k1gMnSc5	Vicent
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
Andoře	Andorra	k1gFnSc6	Andorra
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
spoluknížat	spoluknížat	k1gInSc1	spoluknížat
Andorry	Andorra	k1gFnSc2	Andorra
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Andorra	Andorra	k1gFnSc1	Andorra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Andorra	Andorra	k1gFnSc1	Andorra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Andorra	Andorra	k1gFnSc1	Andorra
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Andorra	Andorra	k1gFnSc1	Andorra
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
