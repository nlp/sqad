<p>
<s>
Mikve	Mikvat	k5eAaPmIp3nS	Mikvat
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
מ	מ	k?	מ
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
sebrání	sebrání	k1gNnSc1	sebrání
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
vody	voda	k1gFnPc1	voda
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
židovská	židovský	k2eAgFnSc1d1	židovská
rituální	rituální	k2eAgFnSc1d1	rituální
lázeň	lázeň	k1gFnSc1	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
nádrží	nádrž	k1gFnSc7	nádrž
s	s	k7c7	s
přírodní	přírodní	k2eAgFnSc7d1	přírodní
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
rituálnímu	rituální	k2eAgNnSc3d1	rituální
očištění	očištění	k1gNnSc3	očištění
osob	osoba	k1gFnPc2	osoba
nebo	nebo	k8xC	nebo
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Mikve	Mikev	k1gFnPc4	Mikev
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
pojmout	pojmout	k5eAaPmF	pojmout
nejméně	málo	k6eAd3	málo
762	[number]	k4	762
litrů	litr	k1gInPc2	litr
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
dost	dost	k6eAd1	dost
hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
umožnila	umožnit	k5eAaPmAgFnS	umožnit
dospělému	dospělý	k1gMnSc3	dospělý
úplné	úplný	k2eAgNnSc1d1	úplné
ponoření	ponoření	k1gNnSc1	ponoření
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Izraeli	Izrael	k1gInSc6	Izrael
používali	používat	k5eAaImAgMnP	používat
mikve	mikev	k1gFnPc4	mikev
kněží	kněz	k1gMnPc2	kněz
k	k	k7c3	k
rituální	rituální	k2eAgFnSc3d1	rituální
očistě	očista	k1gFnSc3	očista
před	před	k7c7	před
službou	služba	k1gFnSc7	služba
v	v	k7c6	v
Jeruzalémském	jeruzalémský	k2eAgInSc6d1	jeruzalémský
chrámu	chrám	k1gInSc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
mikve	mikev	k1gFnSc2	mikev
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
očistu	očista	k1gFnSc4	očista
ženy	žena	k1gFnSc2	žena
po	po	k7c6	po
menstruaci	menstruace	k1gFnSc6	menstruace
<g/>
,	,	kIx,	,
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
či	či	k8xC	či
jiném	jiný	k2eAgInSc6d1	jiný
krvavém	krvavý	k2eAgInSc6d1	krvavý
výtoku	výtok	k1gInSc6	výtok
<g/>
.	.	kIx.	.
</s>
<s>
Ponoření	ponoření	k1gNnSc1	ponoření
do	do	k7c2	do
mikve	mikev	k1gFnSc2	mikev
je	být	k5eAaImIp3nS	být
také	také	k9	také
nezbytnou	nezbytný	k2eAgFnSc7d1	nezbytná
součástí	součást	k1gFnSc7	součást
konverze	konverze	k1gFnPc4	konverze
k	k	k7c3	k
judaismu	judaismus	k1gInSc3	judaismus
(	(	kIx(	(
<g/>
musí	muset	k5eAaImIp3nP	muset
je	on	k3xPp3gInPc4	on
podstoupit	podstoupit	k5eAaPmF	podstoupit
konvertující	konvertující	k2eAgMnPc1d1	konvertující
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ponořením	ponoření	k1gNnSc7	ponoření
v	v	k7c6	v
mikvi	mikev	k1gFnSc6	mikev
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
rituálně	rituálně	k6eAd1	rituálně
očišťují	očišťovat	k5eAaImIp3nP	očišťovat
některé	některý	k3yIgInPc4	některý
druhy	druh	k1gInPc4	druh
nádobí	nádobí	k1gNnSc2	nádobí
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
nežidy	nežid	k1gMnPc4	nežid
před	před	k7c7	před
použitím	použití	k1gNnSc7	použití
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
chodí	chodit	k5eAaImIp3nP	chodit
do	do	k7c2	do
mikve	mikev	k1gFnSc2	mikev
zpravidla	zpravidla	k6eAd1	zpravidla
pouze	pouze	k6eAd1	pouze
před	před	k7c7	před
vysokými	vysoký	k2eAgInPc7d1	vysoký
svátky	svátek	k1gInPc7	svátek
(	(	kIx(	(
<g/>
Roš	Roš	k1gMnSc2	Roš
ha-šana	ha-šan	k1gMnSc2	ha-šan
a	a	k8xC	a
Jom	Jom	k1gMnSc2	Jom
kipur	kipura	k1gFnPc2	kipura
<g/>
)	)	kIx)	)
a	a	k8xC	a
poutními	poutní	k2eAgInPc7d1	poutní
svátky	svátek	k1gInPc7	svátek
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
duchovní	duchovní	k2eAgFnSc2d1	duchovní
přípravy	příprava	k1gFnSc2	příprava
a	a	k8xC	a
očisty	očista	k1gFnSc2	očista
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
započetím	započetí	k1gNnSc7	započetí
Jom	Jom	k1gFnSc2	Jom
kipur	kipura	k1gFnPc2	kipura
je	být	k5eAaImIp3nS	být
takovéto	takovýto	k3xDgNnSc1	takovýto
ponořování	ponořování	k1gNnSc1	ponořování
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
micvu	micva	k1gFnSc4	micva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
doporučováno	doporučován	k2eAgNnSc1d1	doporučováno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
ještě	ještě	k9	ještě
před	před	k7c7	před
minchou	mincha	k1gFnSc7	mincha
–	–	k?	–
samotné	samotný	k2eAgNnSc1d1	samotné
ponoření	ponoření	k1gNnSc1	ponoření
je	být	k5eAaImIp3nS	být
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xS	jako
akt	akt	k1gInSc1	akt
pokání	pokání	k1gNnSc2	pokání
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
muž	muž	k1gMnSc1	muž
v	v	k7c6	v
mikvi	mikev	k1gFnSc6	mikev
ponořil	ponořit	k5eAaPmAgInS	ponořit
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
je	být	k5eAaImIp3nS	být
také	také	k9	také
zvyk	zvyk	k1gInSc1	zvyk
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
mikve	mikev	k1gFnSc2	mikev
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
šabatu	šabat	k1gInSc2	šabat
<g/>
.	.	kIx.	.
</s>
<s>
Chasidský	chasidský	k2eAgInSc1d1	chasidský
zvyk	zvyk	k1gInSc1	zvyk
je	být	k5eAaImIp3nS	být
chodit	chodit	k5eAaImF	chodit
se	se	k3xPyFc4	se
ponořit	ponořit	k5eAaPmF	ponořit
každé	každý	k3xTgNnSc4	každý
ráno	ráno	k1gNnSc4	ráno
před	před	k7c7	před
ranní	ranní	k2eAgFnSc7d1	ranní
modlitbou	modlitba	k1gFnSc7	modlitba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
ženské	ženský	k2eAgFnSc3d1	ženská
sekci	sekce	k1gFnSc3	sekce
moderní	moderní	k2eAgFnSc2d1	moderní
mikve	mikev	k1gFnSc2	mikev
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
připojen	připojit	k5eAaPmNgInS	připojit
kosmetický	kosmetický	k2eAgInSc1d1	kosmetický
salón	salón	k1gInSc1	salón
nebo	nebo	k8xC	nebo
kadeřnictví	kadeřnictví	k1gNnSc1	kadeřnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
mikve	mikev	k1gFnPc1	mikev
umísťovány	umísťovat	k5eAaImNgFnP	umísťovat
do	do	k7c2	do
sklepů	sklep	k1gInPc2	sklep
domů	dům	k1gInPc2	dům
sousedících	sousedící	k2eAgInPc2d1	sousedící
s	s	k7c7	s
modlitebnami	modlitebna	k1gFnPc7	modlitebna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kamenné	kamenný	k2eAgFnPc1d1	kamenná
nádrže	nádrž	k1gFnPc1	nádrž
byly	být	k5eAaImAgFnP	být
napájeny	napájen	k2eAgInPc4d1	napájen
podzemními	podzemní	k2eAgInPc7d1	podzemní
prameny	pramen	k1gInPc7	pramen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
musely	muset	k5eAaImAgFnP	muset
mít	mít	k5eAaImF	mít
přirozený	přirozený	k2eAgInSc4d1	přirozený
přítok	přítok	k1gInSc4	přítok
a	a	k8xC	a
odtok	odtok	k1gInSc4	odtok
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
zákonech	zákon	k1gInPc6	zákon
rozhodujících	rozhodující	k2eAgInPc6d1	rozhodující
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
budují	budovat	k5eAaImIp3nP	budovat
rituální	rituální	k2eAgFnPc1d1	rituální
lázně	lázeň	k1gFnPc1	lázeň
a	a	k8xC	a
jakého	jaký	k3yQgInSc2	jaký
typu	typ	k1gInSc2	typ
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
<g/>
,	,	kIx,	,
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
traktát	traktát	k1gInSc1	traktát
Mikva	Mikvo	k1gNnSc2	Mikvo
<g/>
'	'	kIx"	'
<g/>
ot	ot	k1gMnSc1	ot
v	v	k7c6	v
Mišně	Mišna	k1gFnSc6	Mišna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
bylo	být	k5eAaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
několik	několik	k4yIc1	několik
mikví	mikev	k1gFnPc2	mikev
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
stejným	stejný	k2eAgInPc3d1	stejný
halachickým	halachický	k2eAgInPc3d1	halachický
předpisům	předpis	k1gInPc3	předpis
jako	jako	k8xS	jako
současné	současný	k2eAgFnPc1d1	současná
rituální	rituální	k2eAgFnPc1d1	rituální
lázně	lázeň	k1gFnPc1	lázeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mikve	Mikev	k1gFnSc2	Mikev
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
několik	několik	k4yIc1	několik
starých	starý	k2eAgFnPc2d1	stará
židovských	židovský	k2eAgFnPc2d1	židovská
lázní	lázeň	k1gFnPc2	lázeň
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
využívány	využívat	k5eAaPmNgInP	využívat
převážně	převážně	k6eAd1	převážně
jako	jako	k8xC	jako
turistické	turistický	k2eAgFnPc1d1	turistická
atrakce	atrakce	k1gFnPc1	atrakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historická	historický	k2eAgFnSc1d1	historická
mikve	mikev	k1gFnPc1	mikev
u	u	k7c2	u
Pinkasovy	Pinkasův	k2eAgFnSc2d1	Pinkasova
synagogy	synagoga	k1gFnSc2	synagoga
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
využívána	využíván	k2eAgFnSc1d1	využívána
<g/>
,	,	kIx,	,
v	v	k7c6	v
třebíčské	třebíčský	k2eAgFnSc6d1	Třebíčská
židovské	židovský	k2eAgFnSc6d1	židovská
čtvrti	čtvrt	k1gFnSc6	čtvrt
se	se	k3xPyFc4	se
dvě	dva	k4xCgFnPc1	dva
rituální	rituální	k2eAgFnPc1d1	rituální
lázně	lázeň	k1gFnPc1	lázeň
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Blahoslavova	Blahoslavův	k2eAgMnSc2d1	Blahoslavův
(	(	kIx(	(
<g/>
v	v	k7c6	v
domech	dům	k1gInPc6	dům
č.	č.	k?	č.
10	[number]	k4	10
a	a	k8xC	a
č.	č.	k?	č.
23	[number]	k4	23
<g/>
,	,	kIx,	,
přístupné	přístupný	k2eAgFnPc4d1	přístupná
veřejnosti	veřejnost	k1gFnPc4	veřejnost
po	po	k7c6	po
předchozí	předchozí	k2eAgFnSc6d1	předchozí
domluvě	domluva	k1gFnSc6	domluva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
kolínské	kolínský	k2eAgFnSc6d1	Kolínská
židovské	židovský	k2eAgFnSc6d1	židovská
čtvrti	čtvrt	k1gFnSc6	čtvrt
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
tři	tři	k4xCgFnPc1	tři
mikve	mikev	k1gFnPc1	mikev
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
rabínského	rabínský	k2eAgInSc2d1	rabínský
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
124	[number]	k4	124
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
napájena	napájet	k5eAaImNgFnS	napájet
přírodním	přírodní	k2eAgInSc7d1	přírodní
zdrojem	zdroj	k1gInSc7	zdroj
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
mikve	mikev	k1gFnPc1	mikev
zatím	zatím	k6eAd1	zatím
nejsou	být	k5eNaImIp3nP	být
přístupné	přístupný	k2eAgInPc1d1	přístupný
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
Boskovicích	Boskovice	k1gInPc6	Boskovice
na	na	k7c6	na
Blanensku	Blanensko	k1gNnSc6	Blanensko
náhodně	náhodně	k6eAd1	náhodně
objevena	objeven	k2eAgFnSc1d1	objevena
lázeň	lázeň	k1gFnSc1	lázeň
zasypaná	zasypaný	k2eAgFnSc1d1	zasypaná
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
,	,	kIx,	,
po	po	k7c6	po
částečné	částečný	k2eAgFnSc6d1	částečná
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
<g />
.	.	kIx.	.
</s>
<s>
přístupná	přístupný	k2eAgFnSc1d1	přístupná
turistům	turist	k1gMnPc3	turist
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mikulově	Mikulov	k1gInSc6	Mikulov
na	na	k7c6	na
Břeclavsku	Břeclavsko	k1gNnSc6	Břeclavsko
náhodně	náhodně	k6eAd1	náhodně
objevená	objevený	k2eAgFnSc1d1	objevená
zasypaná	zasypaný	k2eAgFnSc1d1	zasypaná
mikve	mikev	k1gFnPc1	mikev
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
činžovního	činžovní	k2eAgInSc2d1	činžovní
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Všerubech	Všeruby	k1gInPc6	Všeruby
na	na	k7c6	na
Domažlicku	Domažlicko	k1gNnSc6	Domažlicko
ve	v	k7c6	v
sklepení	sklepení	k1gNnSc6	sklepení
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Kdyni	Kdyně	k1gFnSc6	Kdyně
na	na	k7c6	na
Domažlicku	Domažlicko	k1gNnSc6	Domažlicko
ve	v	k7c6	v
výjimečně	výjimečně	k6eAd1	výjimečně
zachovalé	zachovalý	k2eAgFnSc3d1	zachovalá
synagóze	synagóga	k1gFnSc3	synagóga
<g/>
,	,	kIx,	,
dům	dům	k1gInSc4	dům
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
v	v	k7c6	v
Lázních	lázeň	k1gFnPc6	lázeň
Kynžvart	Kynžvart	k1gInSc1	Kynžvart
(	(	kIx(	(
<g/>
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
v	v	k7c6	v
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
<g/>
)	)	kIx)	)
mikve	mikvat	k5eAaPmIp3nS	mikvat
přístupná	přístupný	k2eAgFnSc1d1	přístupná
veřejnosti	veřejnost	k1gFnPc1	veřejnost
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
přístupná	přístupný	k2eAgFnSc1d1	přístupná
mikve	mikev	k1gFnSc2	mikev
nevyužívaná	využívaný	k2eNgFnSc1d1	nevyužívaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1454	[number]	k4	1454
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dobrušce	Dobruška	k1gFnSc6	Dobruška
na	na	k7c6	na
Rychnovsku	Rychnovsko	k1gNnSc6	Rychnovsko
míkve	míkvat	k5eAaPmIp3nS	míkvat
přístupná	přístupný	k2eAgFnSc1d1	přístupná
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prohlídky	prohlídka	k1gFnSc2	prohlídka
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
v	v	k7c6	v
Hřešihlavech	Hřešihlav	k1gInPc6	Hřešihlav
na	na	k7c6	na
Rokycansku	Rokycansko	k1gNnSc6	Rokycansko
v	v	k7c6	v
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
34	[number]	k4	34
při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
nepřístupno	přístupen	k2eNgNnSc1d1	nepřístupno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
slouží	sloužit	k5eAaImIp3nS	sloužit
Židům	Žid	k1gMnPc3	Žid
k	k	k7c3	k
očistné	očistný	k2eAgFnSc3d1	očistná
lázni	lázeň	k1gFnSc3	lázeň
tři	tři	k4xCgMnPc4	tři
mikve	mikvat	k5eAaPmIp3nS	mikvat
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
historická	historický	k2eAgFnSc1d1	historická
mikve	mikev	k1gFnPc1	mikev
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
Pinkasovy	Pinkasův	k2eAgFnSc2d1	Pinkasova
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
košer	košer	k2eAgInSc2d1	košer
hotelu	hotel	k1gInSc2	hotel
King	King	k1gMnSc1	King
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
a	a	k8xC	a
třetí	třetí	k4xOgInSc1	třetí
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
sklepních	sklepní	k2eAgFnPc6d1	sklepní
prostorách	prostora	k1gFnPc6	prostora
domu	dům	k1gInSc2	dům
na	na	k7c6	na
třídě	třída	k1gFnSc6	třída
Kapitána	kapitán	k1gMnSc2	kapitán
Jaroše	Jaroš	k1gMnSc2	Jaroš
patřícího	patřící	k2eAgMnSc2d1	patřící
židovské	židovský	k2eAgFnPc1d1	židovská
obci	obec	k1gFnSc3	obec
<g/>
;	;	kIx,	;
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
nově	nově	k6eAd1	nově
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
žádná	žádný	k3yNgFnSc1	žádný
historická	historický	k2eAgFnSc1d1	historická
lázeň	lázeň	k1gFnSc1	lázeň
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mikve	mikev	k1gFnSc2	mikev
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
NEWMAN	NEWMAN	kA	NEWMAN
<g/>
,	,	kIx,	,
Ja	Ja	k?	Ja
<g/>
'	'	kIx"	'
<g/>
akov	akov	k1gMnSc1	akov
<g/>
;	;	kIx,	;
SIVAN	SIVAN	kA	SIVAN
<g/>
,	,	kIx,	,
Gavri	Gavri	k1gNnSc1	Gavri
<g/>
'	'	kIx"	'
<g/>
el.	el.	k?	el.
Judaismus	judaismus	k1gInSc1	judaismus
od	od	k7c2	od
A	A	kA	A
do	do	k7c2	do
Z.	Z.	kA	Z.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Sefer	Sefer	k1gMnSc1	Sefer
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
285	[number]	k4	285
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
900895	[number]	k4	900895
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
