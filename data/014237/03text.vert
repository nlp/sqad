<s>
Skytalé	Skytalý	k2eAgNnSc1d1
</s>
<s>
Skytalé	Skytalé	k1gNnSc1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
šifrování	šifrování	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
válce	válec	k1gInSc2
a	a	k8xC
na	na	k7c6
něm	on	k3xPp3gInSc6
navinutém	navinutý	k2eAgInSc6d1
papyru	papyrus	k1gInSc6
či	či	k8xC
pergamenu	pergamen	k1gInSc6
na	na	k7c6
kterém	který	k3yRgInSc6
je	být	k5eAaImIp3nS
napsaný	napsaný	k2eAgInSc1d1
vzkaz	vzkaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
druh	druh	k1gInSc1
šifrování	šifrování	k1gNnSc2
používali	používat	k5eAaImAgMnP
Řekové	Řek	k1gMnPc1
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
Sparťané	Sparťan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
ji	on	k3xPp3gFnSc4
využívali	využívat	k5eAaImAgMnP,k5eAaPmAgMnP
během	během	k7c2
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Skytalé	Skytalý	k2eAgNnSc1d1
</s>
<s>
Skytalé	Skytalý	k2eAgNnSc1d1
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Z	z	k7c2
nepřímých	přímý	k2eNgInPc2d1
důkazů	důkaz	k1gInPc2
se	se	k3xPyFc4
o	o	k7c6
Skytalé	Skytalý	k2eAgFnSc6d1
první	první	k4xOgFnSc6
zmínil	zmínit	k5eAaPmAgMnS
řecký	řecký	k2eAgMnSc1d1
basník	basník	k1gMnSc1
Archilochos	Archilochos	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
žil	žít	k5eAaImAgMnS
v	v	k7c6
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgNnSc7d1
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
zmínil	zmínit	k5eAaPmAgMnS
o	o	k7c4
Skytalé	Skytalý	k2eAgNnSc4d1
byl	být	k5eAaImAgMnS
až	až	k8xS
polovině	polovina	k1gFnSc6
3	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Apollónios	Apollónios	k1gInSc1
z	z	k7c2
Rhodu	Rhodos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popis	popis	k1gInSc1
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
fungovala	fungovat	k5eAaImAgFnS
skytalé	skytalý	k2eAgNnSc4d1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
až	až	k8xS
Plútarchos	Plútarchos	k1gMnSc1
(	(	kIx(
<g/>
50	#num#	k4
až	až	k9
120	#num#	k4
nl	nl	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Šifrování	šifrování	k1gNnSc1
</s>
<s>
Princip	princip	k1gInSc1
šifrování	šifrování	k1gNnSc2
údajů	údaj	k1gInPc2
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
na	na	k7c4
dřevěný	dřevěný	k2eAgInSc4d1
válec	válec	k1gInSc4
předem	předem	k6eAd1
dohodnutého	dohodnutý	k2eAgInSc2d1
průměru	průměr	k1gInSc2
namotal	namotat	k5eAaPmAgInS,k5eAaBmAgInS
pásek	pásek	k1gInSc1
pergamenu	pergamen	k1gInSc2
a	a	k8xC
ve	v	k7c6
směru	směr	k1gInSc6
hlavní	hlavní	k2eAgFnSc2d1
osy	osa	k1gFnSc2
válce	válec	k1gInSc2
se	se	k3xPyFc4
na	na	k7c4
něj	on	k3xPp3gMnSc4
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
text	text	k1gInSc1
od	od	k7c2
jednoho	jeden	k4xCgInSc2
konce	konec	k1gInSc2
k	k	k7c3
druhému	druhý	k4xOgMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gInSc6
odmotaní	odmotaný	k2eAgMnPc1d1
na	na	k7c6
pásku	pásek	k1gInSc6
zůstala	zůstat	k5eAaPmAgNnP
písmena	písmeno	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
nedávala	dávat	k5eNaImAgFnS
spolu	spolu	k6eAd1
smysl	smysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
dešifrovat	dešifrovat	k5eAaBmF
text	text	k1gInSc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
namotaní	namotaný	k2eAgMnPc1d1
pásky	pásek	k1gInPc1
na	na	k7c4
válec	válec	k1gInSc4
o	o	k7c6
stejné	stejný	k2eAgFnSc6d1
velikosti	velikost	k1gFnSc6
průměru	průměr	k1gInSc2
.	.	kIx.
</s>
<s>
Páska	páska	k1gFnSc1
na	na	k7c6
válci	válec	k1gInSc6
vypadala	vypadat	k5eAaPmAgFnS,k5eAaImAgFnS
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
_____________________________________________________________	_____________________________________________________________	k?
</s>
<s>
|	|	kIx~
|	|	kIx~
|	|	kIx~
|	|	kIx~
|	|	kIx~
|	|	kIx~
|	|	kIx~
</s>
<s>
|	|	kIx~
P	P	kA
|	|	kIx~
O	O	kA
|	|	kIx~
M	M	kA
|	|	kIx~
O	O	kA
|	|	kIx~
Z	z	k7c2
|	|	kIx~
|	|	kIx~
</s>
<s>
__	__	k?
<g/>
|	|	kIx~
M	M	kA
|	|	kIx~
I	I	kA
|	|	kIx~
J	J	kA
|	|	kIx~
S	s	k7c7
|	|	kIx~
E	E	kA
|	|	kIx~
<g/>
__	__	k?
|	|	kIx~
</s>
<s>
|	|	kIx~
|	|	kIx~
M	M	kA
|	|	kIx~
P	P	kA
|	|	kIx~
O	O	kA
|	|	kIx~
D	D	kA
|	|	kIx~
P	P	kA
|	|	kIx~
</s>
<s>
|	|	kIx~
|	|	kIx~
A	A	kA
|	|	kIx~
L	L	kA
|	|	kIx~
B	B	kA
|	|	kIx~
O	O	kA
|	|	kIx~
U	u	k7c2
|	|	kIx~
</s>
<s>
|	|	kIx~
|	|	kIx~
|	|	kIx~
|	|	kIx~
|	|	kIx~
|	|	kIx~
|	|	kIx~
</s>
<s>
_____________________________________________________________	_____________________________________________________________	k?
</s>
<s>
Po	po	k7c6
roztažení	roztažení	k1gNnSc6
vypadala	vypadat	k5eAaPmAgFnS,k5eAaImAgFnS
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
PMMAOIPLMJOBOSDOZEPU	PMMAOIPLMJOBOSDOZEPU	kA
</s>
<s>
Dešifrování	dešifrování	k1gNnSc1
</s>
<s>
Později	pozdě	k6eAd2
ale	ale	k8xC
přišli	přijít	k5eAaPmAgMnP
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
šifrování	šifrování	k1gNnSc2
není	být	k5eNaImIp3nS
dokonalý	dokonalý	k2eAgMnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
stačilo	stačit	k5eAaBmAgNnS
pásku	páska	k1gFnSc4
namotat	namotat	k5eAaBmF,k5eAaPmF
na	na	k7c4
kužel	kužel	k1gInSc4
a	a	k8xC
posouvat	posouvat	k5eAaImF
jím	jíst	k5eAaImIp1nS
dokud	dokud	k8xS
jste	být	k5eAaImIp2nP
nedostali	dostat	k5eNaPmAgMnP
smysluplné	smysluplný	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Scytale	Scytal	k1gMnSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kryptografie	kryptografie	k1gFnSc1
-	-	kIx~
od	od	k7c2
skytalé	skytalý	k2eAgFnSc2d1
ke	k	k7c3
kvantové	kvantový	k2eAgFnSc3d1
fyzice	fyzika	k1gFnSc3
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Staršie	Staršie	k1gFnSc1
kódovacie	kódovacie	k1gFnSc2
systémy	systém	k1gInPc7
</s>
<s>
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
Dôležité	Dôležitý	k2eAgMnPc4d1
medzníky	medzník	k1gMnPc4
v	v	k7c6
histórii	histórie	k1gFnSc6
kryptografie	kryptografie	k1gFnSc2
</s>
