<s>
Skytalé	Skytalé	k1gNnSc1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
šifrování	šifrování	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
válce	válec	k1gInSc2
a	a	k8xC
na	na	k7c6
něm	on	k3xPp3gInSc6
navinutém	navinutý	k2eAgInSc6d1
papyru	papyrus	k1gInSc6
či	či	k8xC
pergamenu	pergamen	k1gInSc6
na	na	k7c6
kterém	který	k3yRgInSc6
je	být	k5eAaImIp3nS
napsaný	napsaný	k2eAgInSc1d1
vzkaz	vzkaz	k1gInSc1
<g/>
.	.	kIx.
</s>