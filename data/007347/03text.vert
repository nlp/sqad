<s>
Krajka	krajka	k1gFnSc1	krajka
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
:	:	kIx,	:
lace	laka	k1gFnSc6	laka
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
<g/>
:	:	kIx,	:
Spitze	Spitze	k1gFnSc1	Spitze
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plošná	plošný	k2eAgFnSc1d1	plošná
ozdobná	ozdobný	k2eAgFnSc1d1	ozdobná
textilie	textilie	k1gFnSc1	textilie
<g/>
,	,	kIx,	,
vzdušná	vzdušný	k2eAgFnSc1d1	vzdušná
a	a	k8xC	a
průhledná	průhledný	k2eAgFnSc1d1	průhledná
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
z	z	k7c2	z
nití	nit	k1gFnPc2	nit
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
ornamenty	ornament	k1gInPc4	ornament
nebo	nebo	k8xC	nebo
kresby	kresba	k1gFnPc4	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Užívala	užívat	k5eAaImAgNnP	užívat
se	se	k3xPyFc4	se
a	a	k8xC	a
užívá	užívat	k5eAaImIp3nS	užívat
jako	jako	k9	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
svátečních	sváteční	k2eAgInPc2d1	sváteční
šatů	šat	k1gInPc2	šat
<g/>
,	,	kIx,	,
dámského	dámský	k2eAgMnSc2d1	dámský
<g/>
,	,	kIx,	,
stolního	stolní	k2eAgNnSc2d1	stolní
i	i	k8xC	i
ložního	ložní	k2eAgNnSc2d1	ložní
prádla	prádlo	k1gNnSc2	prádlo
<g/>
,	,	kIx,	,
liturgických	liturgický	k2eAgNnPc2d1	liturgické
rouch	roucho	k1gNnPc2	roucho
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
odborníci	odborník	k1gMnPc1	odborník
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
krajku	krajka	k1gFnSc4	krajka
jen	jen	k9	jen
ručně	ručně	k6eAd1	ručně
zhotovené	zhotovený	k2eAgFnPc4d1	zhotovená
textilie	textilie	k1gFnPc4	textilie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
definicí	definice	k1gFnPc2	definice
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
krajka	krajka	k1gFnSc1	krajka
jen	jen	k9	jen
napůl	napůl	k6eAd1	napůl
"	"	kIx"	"
<g/>
pravá	pravý	k2eAgFnSc1d1	pravá
<g/>
"	"	kIx"	"
jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
paličkování	paličkování	k1gNnSc1	paličkování
nebo	nebo	k8xC	nebo
vyšívání	vyšívání	k1gNnSc1	vyšívání
provádělo	provádět	k5eAaImAgNnS	provádět
na	na	k7c6	na
strojně	strojně	k6eAd1	strojně
vyrobeném	vyrobený	k2eAgInSc6d1	vyrobený
tylu	tyl	k1gInSc6	tyl
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
<g/>
,	,	kIx,	,
všeobecně	všeobecně	k6eAd1	všeobecně
platná	platný	k2eAgFnSc1d1	platná
definice	definice	k1gFnSc1	definice
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
ohraničení	ohraničení	k1gNnSc1	ohraničení
asi	asi	k9	asi
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnoho	mnoho	k6eAd1	mnoho
krajek	krajek	k1gInSc1	krajek
vzniká	vznikat	k5eAaImIp3nS	vznikat
kombinací	kombinace	k1gFnSc7	kombinace
různých	různý	k2eAgFnPc2d1	různá
technik	technika	k1gFnPc2	technika
(	(	kIx(	(
<g/>
pozamentování	pozamentování	k1gNnSc1	pozamentování
<g/>
,	,	kIx,	,
vázání	vázání	k1gNnSc1	vázání
<g/>
,	,	kIx,	,
splétání	splétání	k1gNnSc1	splétání
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přechod	přechod	k1gInSc1	přechod
od	od	k7c2	od
krajky	krajka	k1gFnSc2	krajka
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
druhům	druh	k1gInPc3	druh
textilií	textilie	k1gFnPc2	textilie
je	být	k5eAaImIp3nS	být
plynulý	plynulý	k2eAgMnSc1d1	plynulý
a	a	k8xC	a
názory	názor	k1gInPc1	názor
odborníků	odborník	k1gMnPc2	odborník
na	na	k7c4	na
obsah	obsah	k1gInSc4	obsah
pojmu	pojmout	k5eAaPmIp1nS	pojmout
krajka	krajka	k1gFnSc1	krajka
se	se	k3xPyFc4	se
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
zejména	zejména	k9	zejména
dva	dva	k4xCgInPc4	dva
popisy	popis	k1gInPc4	popis
Krajka	krajka	k1gFnSc1	krajka
je	být	k5eAaImIp3nS	být
průhledná	průhledný	k2eAgFnSc1d1	průhledná
textilie	textilie	k1gFnSc1	textilie
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
stuhy	stuha	k1gFnSc2	stuha
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
do	do	k7c2	do
cca	cca	kA	cca
25	[number]	k4	25
cm	cm	kA	cm
s	s	k7c7	s
rovnými	rovný	k2eAgFnPc7d1	rovná
nebo	nebo	k8xC	nebo
zoubkovanými	zoubkovaný	k2eAgFnPc7d1	zoubkovaná
okraji	okraj	k1gInSc6	okraj
Krajka	krajka	k1gFnSc1	krajka
je	být	k5eAaImIp3nS	být
plošná	plošný	k2eAgFnSc1d1	plošná
textilie	textilie	k1gFnSc1	textilie
neomezené	omezený	k2eNgFnSc2d1	neomezená
velikosti	velikost	k1gFnSc2	velikost
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
ornamentů	ornament	k1gInPc2	ornament
s	s	k7c7	s
otvory	otvor	k1gInPc7	otvor
vzorových	vzorový	k2eAgFnPc2d1	vzorová
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
prosvítá	prosvítat	k5eAaImIp3nS	prosvítat
pozadí	pozadí	k1gNnSc4	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Krajka	krajka	k1gFnSc1	krajka
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyrábět	vyrábět	k5eAaImF	vyrábět
ručně	ručně	k6eAd1	ručně
nebo	nebo	k8xC	nebo
strojně	strojně	k6eAd1	strojně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
formulace	formulace	k1gFnSc2	formulace
vychází	vycházet	k5eAaImIp3nS	vycházet
např.	např.	kA	např.
práce	práce	k1gFnSc2	práce
F.	F.	kA	F.
<g/>
Schönera	Schöner	k1gMnSc2	Schöner
Spitzen	Spitzna	k1gFnPc2	Spitzna
uvedená	uvedený	k2eAgFnSc1d1	uvedená
v	v	k7c6	v
odkazech	odkaz	k1gInPc6	odkaz
na	na	k7c4	na
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
článku	článek	k1gInSc6	článek
odvozeny	odvodit	k5eAaPmNgInP	odvodit
všechny	všechen	k3xTgInPc1	všechen
základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
zemi	zem	k1gFnSc4	zem
původu	původ	k1gInSc2	původ
krajky	krajka	k1gFnSc2	krajka
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
výroba	výroba	k1gFnSc1	výroba
krajek	krajek	k1gInSc4	krajek
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
nálezy	nález	k1gInPc1	nález
(	(	kIx(	(
<g/>
pletené	pletený	k2eAgFnPc1d1	pletená
krajky	krajka	k1gFnPc1	krajka
ve	v	k7c6	v
starém	starý	k2eAgInSc6d1	starý
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
nástroje	nástroj	k1gInPc4	nástroj
k	k	k7c3	k
paličkování	paličkování	k1gNnSc3	paličkování
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
sítované	sítovaný	k2eAgFnPc4d1	sítovaná
krajky	krajka	k1gFnPc4	krajka
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
Českomoravské	českomoravský	k2eAgFnSc6d1	Českomoravská
vysočině	vysočina	k1gFnSc6	vysočina
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
krajkářství	krajkářství	k1gNnSc1	krajkářství
bylo	být	k5eAaImAgNnS	být
známé	známý	k2eAgNnSc1d1	známé
už	už	k6eAd1	už
mnohem	mnohem	k6eAd1	mnohem
dřív	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
výroba	výroba	k1gFnSc1	výroba
krajek	krajka	k1gFnPc2	krajka
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
zejména	zejména	k9	zejména
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Flander	Flandry	k1gInPc2	Flandry
a	a	k8xC	a
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1768	[number]	k4	1768
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
Angličan	Angličan	k1gMnSc1	Angličan
Hammond	Hammond	k1gMnSc1	Hammond
první	první	k4xOgInSc4	první
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
dala	dát	k5eAaPmAgFnS	dát
vyrábět	vyrábět	k5eAaImF	vyrábět
síťovina	síťovina	k1gFnSc1	síťovina
na	na	k7c4	na
vyšívanou	vyšívaný	k2eAgFnSc4d1	vyšívaná
krajku	krajka	k1gFnSc4	krajka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
patentován	patentován	k2eAgInSc4d1	patentován
první	první	k4xOgInSc4	první
paličkovací	paličkovací	k2eAgInSc4d1	paličkovací
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
předvedl	předvést	k5eAaPmAgMnS	předvést
vynálezce	vynálezce	k1gMnSc1	vynálezce
Leavers	Leavers	k1gInSc4	Leavers
bobinetový	bobinetový	k2eAgInSc4d1	bobinetový
stroj	stroj	k1gInSc4	stroj
s	s	k7c7	s
žakárovým	žakárový	k2eAgNnSc7d1	žakárové
ústrojím	ústrojí	k1gNnSc7	ústrojí
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
byl	být	k5eAaImAgInS	být
vynalezen	vynalezen	k2eAgInSc1d1	vynalezen
stroj	stroj	k1gInSc1	stroj
na	na	k7c4	na
krajkové	krajkový	k2eAgFnPc4d1	krajková
záclony	záclona	k1gFnPc4	záclona
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
vyšívací	vyšívací	k2eAgInSc1d1	vyšívací
stroj	stroj	k1gInSc1	stroj
Francouze	Francouz	k1gMnSc2	Francouz
Heilmanna	Heilmann	k1gMnSc2	Heilmann
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
člunečkový	člunečkový	k2eAgInSc1d1	člunečkový
vyšívací	vyšívací	k2eAgInSc1d1	vyšívací
automat	automat	k1gInSc1	automat
švýcara	švýcar	k1gMnSc2	švýcar
Gröbliho	Gröbli	k1gMnSc2	Gröbli
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
paličkovací	paličkovací	k2eAgInSc1d1	paličkovací
stroj	stroj	k1gInSc1	stroj
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1748	[number]	k4	1748
od	od	k7c2	od
Angličana	Angličan	k1gMnSc2	Angličan
Walforda	Walford	k1gMnSc2	Walford
<g/>
,	,	kIx,	,
výrobu	výroba	k1gFnSc4	výroba
krajek	krajka	k1gFnPc2	krajka
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
strojích	stroj	k1gInPc6	stroj
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
několik	několik	k4yIc1	několik
konstrukcí	konstrukce	k1gFnPc2	konstrukce
různých	různý	k2eAgMnPc2d1	různý
vynálezců	vynálezce	k1gMnPc2	vynálezce
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
prakticky	prakticky	k6eAd1	prakticky
použitelný	použitelný	k2eAgInSc1d1	použitelný
osnovní	osnovní	k2eAgInSc1d1	osnovní
pletací	pletací	k2eAgInSc1d1	pletací
stroj	stroj	k1gInSc1	stroj
postavil	postavit	k5eAaPmAgMnS	postavit
Angličan	Angličan	k1gMnSc1	Angličan
Redgate	Redgat	k1gInSc5	Redgat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
rašl	rašl	k1gInSc1	rašl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zdaleka	zdaleka	k6eAd1	zdaleka
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
strojem	stroj	k1gInSc7	stroj
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
krajek	krajka	k1gFnPc2	krajka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
sdružovalo	sdružovat	k5eAaImAgNnS	sdružovat
na	na	k7c4	na
2000	[number]	k4	2000
výrobců	výrobce	k1gMnPc2	výrobce
z	z	k7c2	z
33	[number]	k4	33
států	stát	k1gInPc2	stát
v	v	k7c4	v
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
organizaci	organizace	k1gFnSc4	organizace
pro	pro	k7c4	pro
(	(	kIx(	(
<g/>
ručně	ručně	k6eAd1	ručně
<g/>
)	)	kIx)	)
paličkovanou	paličkovaný	k2eAgFnSc4d1	paličkovaná
a	a	k8xC	a
šitou	šitý	k2eAgFnSc4d1	šitá
krajku	krajka	k1gFnSc4	krajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
není	být	k5eNaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
žádná	žádný	k3yNgFnSc1	žádný
souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
informace	informace	k1gFnSc1	informace
ani	ani	k8xC	ani
o	o	k7c6	o
celkovém	celkový	k2eAgNnSc6d1	celkové
množství	množství	k1gNnSc6	množství
ani	ani	k8xC	ani
o	o	k7c6	o
rozsahu	rozsah	k1gInSc6	rozsah
výroby	výroba	k1gFnSc2	výroba
ručně	ručně	k6eAd1	ručně
a	a	k8xC	a
strojně	strojně	k6eAd1	strojně
zhotovených	zhotovený	k2eAgFnPc2d1	zhotovená
krajek	krajka	k1gFnPc2	krajka
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
bylo	být	k5eAaImAgNnS	být
krajkářství	krajkářství	k1gNnSc1	krajkářství
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
známé	známá	k1gFnSc2	známá
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
a	a	k8xC	a
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
(	(	kIx(	(
<g/>
Vamberk	Vamberk	k1gInSc1	Vamberk
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
krajkářství	krajkářství	k1gNnSc1	krajkářství
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
zejména	zejména	k9	zejména
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začala	začít	k5eAaPmAgFnS	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
varianta	varianta	k1gFnSc1	varianta
bruselské	bruselský	k2eAgFnSc2d1	bruselská
krajky	krajka	k1gFnSc2	krajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
se	se	k3xPyFc4	se
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
zabývalo	zabývat	k5eAaImAgNnS	zabývat
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
domáckou	domácký	k2eAgFnSc7d1	domácká
<g/>
)	)	kIx)	)
výrobou	výroba	k1gFnSc7	výroba
krajek	krajka	k1gFnPc2	krajka
asi	asi	k9	asi
80	[number]	k4	80
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
odbyt	odbyt	k1gInSc4	odbyt
krajkového	krajkový	k2eAgNnSc2d1	krajkové
zboží	zboží	k1gNnSc2	zboží
značně	značně	k6eAd1	značně
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
,	,	kIx,	,
tradice	tradice	k1gFnSc1	tradice
krajkářství	krajkářství	k1gNnSc2	krajkářství
se	se	k3xPyFc4	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
jen	jen	k9	jen
v	v	k7c6	v
amatérské	amatérský	k2eAgFnSc6d1	amatérská
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
jako	jako	k9	jako
umělecké	umělecký	k2eAgNnSc4d1	umělecké
řemeslo	řemeslo	k1gNnSc4	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
české	český	k2eAgFnPc1d1	Česká
oděvní	oděvní	k2eAgFnPc1d1	oděvní
výtvarnice	výtvarnice	k1gFnPc1	výtvarnice
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
příspěvky	příspěvek	k1gInPc4	příspěvek
k	k	k7c3	k
modernímu	moderní	k2eAgNnSc3d1	moderní
krajkářství	krajkářství	k1gNnSc3	krajkářství
řadu	řad	k1gInSc2	řad
úspěchů	úspěch	k1gInPc2	úspěch
na	na	k7c6	na
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
výstavách	výstava	k1gFnPc6	výstava
a	a	k8xC	a
soutěžích	soutěž	k1gFnPc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
začala	začít	k5eAaPmAgFnS	začít
výroba	výroba	k1gFnSc1	výroba
krajek	krajka	k1gFnPc2	krajka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
v	v	k7c6	v
Letovicích	Letovice	k1gFnPc6	Letovice
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
na	na	k7c6	na
bobinetových	bobinetový	k2eAgInPc6d1	bobinetový
strojích	stroj	k1gInPc6	stroj
pašovaných	pašovaný	k2eAgInPc6d1	pašovaný
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
továrna	továrna	k1gFnSc1	továrna
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
existoval	existovat	k5eAaImAgMnS	existovat
asi	asi	k9	asi
jediný	jediný	k2eAgMnSc1d1	jediný
český	český	k2eAgMnSc1d1	český
výrobce	výrobce	k1gMnSc1	výrobce
krajek	krajka	k1gFnPc2	krajka
v	v	k7c6	v
moravských	moravský	k2eAgInPc6d1	moravský
Drnovicích	Drnovice	k1gInPc6	Drnovice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
určující	určující	k2eAgInPc4d1	určující
znaky	znak	k1gInPc4	znak
krajky	krajka	k1gFnSc2	krajka
se	se	k3xPyFc4	se
pokládá	pokládat	k5eAaImIp3nS	pokládat
druh	druh	k1gInSc1	druh
ornamentu	ornament	k1gInSc2	ornament
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
struktura	struktura	k1gFnSc1	struktura
vzoru	vzor	k1gInSc2	vzor
a	a	k8xC	a
(	(	kIx(	(
<g/>
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
<g/>
)	)	kIx)	)
použitý	použitý	k2eAgInSc4d1	použitý
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
vzorování	vzorování	k1gNnSc2	vzorování
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
technice	technika	k1gFnSc6	technika
zhotovení	zhotovení	k1gNnSc2	zhotovení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
historického	historický	k2eAgInSc2d1	historický
vývoje	vývoj	k1gInSc2	vývoj
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
několika	několik	k4yIc2	několik
základních	základní	k2eAgInPc2d1	základní
směrů	směr	k1gInPc2	směr
nespočetná	spočetný	k2eNgFnSc1d1	nespočetná
řada	řada	k1gFnSc1	řada
variant	varianta	k1gFnPc2	varianta
jak	jak	k8xS	jak
ručně	ručně	k6eAd1	ručně
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
strojně	strojně	k6eAd1	strojně
zhotovených	zhotovený	k2eAgInPc2d1	zhotovený
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
používaných	používaný	k2eAgInPc2d1	používaný
materiálů	materiál	k1gInPc2	materiál
převažuje	převažovat	k5eAaImIp3nS	převažovat
u	u	k7c2	u
ručně	ručně	k6eAd1	ručně
zhotovených	zhotovený	k2eAgFnPc2d1	zhotovená
krajek	krajka	k1gFnPc2	krajka
bavlněná	bavlněný	k2eAgFnSc1d1	bavlněná
<g/>
,	,	kIx,	,
lněná	lněný	k2eAgFnSc1d1	lněná
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pokovovaná	pokovovaný	k2eAgFnSc1d1	pokovovaná
příze	příze	k1gFnSc1	příze
nebo	nebo	k8xC	nebo
přírodní	přírodní	k2eAgNnSc1d1	přírodní
hedvábí	hedvábí	k1gNnSc1	hedvábí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
strojní	strojní	k2eAgFnSc6d1	strojní
výrobě	výroba	k1gFnSc6	výroba
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
polyesterové	polyesterový	k2eAgInPc1d1	polyesterový
<g/>
,	,	kIx,	,
polyakrylové	polyakrylový	k2eAgInPc1d1	polyakrylový
a	a	k8xC	a
elastické	elastický	k2eAgInPc4d1	elastický
filamenty	filament	k1gInPc4	filament
v	v	k7c6	v
širokém	široký	k2eAgInSc6d1	široký
rozsahu	rozsah	k1gInSc6	rozsah
jemnosti	jemnost	k1gFnSc2	jemnost
cca	cca	kA	cca
5-100	[number]	k4	5-100
tex	tex	k?	tex
<g/>
.	.	kIx.	.
</s>
<s>
Krajkovina	krajkovina	k1gFnSc1	krajkovina
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
už	už	k6eAd1	už
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
oděvní	oděvní	k2eAgInPc4d1	oděvní
doplňky	doplněk	k1gInPc4	doplněk
nebo	nebo	k8xC	nebo
celé	celý	k2eAgFnPc4d1	celá
části	část	k1gFnPc4	část
oděvů	oděv	k1gInPc2	oděv
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
bytovým	bytový	k2eAgInPc3d1	bytový
účelům	účel	k1gInPc3	účel
(	(	kIx(	(
<g/>
ubrusy	ubrus	k1gInPc4	ubrus
<g/>
,	,	kIx,	,
záclony	záclona	k1gFnPc4	záclona
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
punčochové	punčochový	k2eAgInPc4d1	punčochový
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
podle	podle	k7c2	podle
techniky	technika	k1gFnSc2	technika
zhotovení	zhotovení	k1gNnSc2	zhotovení
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
zejména	zejména	k9	zejména
<g/>
:	:	kIx,	:
Základní	základní	k2eAgInSc1d1	základní
prvek	prvek	k1gInSc1	prvek
šité	šitý	k2eAgFnSc2d1	šitá
krajky	krajka	k1gFnSc2	krajka
je	být	k5eAaImIp3nS	být
smyčkový	smyčkový	k2eAgInSc4d1	smyčkový
steh	steh	k1gInSc4	steh
<g/>
,	,	kIx,	,
spojováním	spojování	k1gNnSc7	spojování
stehů	steh	k1gInPc2	steh
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
niti	nit	k1gFnSc2	nit
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
vzory	vzor	k1gInPc1	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Modifikace	modifikace	k1gFnSc1	modifikace
šité	šitý	k2eAgFnSc2d1	šitá
krajky	krajka	k1gFnSc2	krajka
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
Orientální	orientální	k2eAgInPc4d1	orientální
krajky	krajek	k1gInPc4	krajek
se	s	k7c7	s
začátkem	začátek	k1gInSc7	začátek
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
tkaniny	tkanina	k1gFnSc2	tkanina
a	a	k8xC	a
šité	šitý	k2eAgInPc1d1	šitý
bez	bez	k7c2	bez
podložky	podložka	k1gFnSc2	podložka
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
jako	jako	k8xS	jako
lemování	lemování	k1gNnSc2	lemování
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
orientální	orientální	k2eAgFnSc1d1	orientální
<g/>
,	,	kIx,	,
arménská	arménský	k2eAgFnSc1d1	arménská
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
snímek	snímek	k1gInSc1	snímek
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
))	))	k?	))
Evropská	evropský	k2eAgFnSc1d1	Evropská
krajka	krajka	k1gFnSc1	krajka
Kontura	kontura	k1gFnSc1	kontura
zakresleného	zakreslený	k2eAgInSc2d1	zakreslený
vzoru	vzor	k1gInSc2	vzor
se	se	k3xPyFc4	se
připevní	připevnit	k5eAaPmIp3nP	připevnit
stehy	steh	k1gInPc1	steh
šicí	šicí	k2eAgFnSc2d1	šicí
niti	nit	k1gFnSc2	nit
na	na	k7c4	na
tkanou	tkaný	k2eAgFnSc4d1	tkaná
podložku	podložka	k1gFnSc4	podložka
<g/>
,	,	kIx,	,
smyčkami	smyčka	k1gFnPc7	smyčka
niti	nit	k1gFnSc2	nit
vyčnívajícími	vyčnívající	k2eAgFnPc7d1	vyčnívající
nad	nad	k7c7	nad
podložkou	podložka	k1gFnSc7	podložka
se	se	k3xPyFc4	se
provleče	provléct	k5eAaPmIp3nS	provléct
vodicí	vodicí	k2eAgFnSc1d1	vodicí
nit	nit	k1gFnSc1	nit
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
podložkou	podložka	k1gFnSc7	podložka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyplní	vyplnit	k5eAaPmIp3nS	vyplnit
plocha	plocha	k1gFnSc1	plocha
mezi	mezi	k7c7	mezi
okraji	okraj	k1gInPc7	okraj
vzoru	vzor	k1gInSc6	vzor
efekty	efekt	k1gInPc1	efekt
z	z	k7c2	z
krajkových	krajkový	k2eAgInPc2d1	krajkový
stehů	steh	k1gInPc2	steh
(	(	kIx(	(
<g/>
v	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
na	na	k7c4	na
čtyřicet	čtyřicet	k4xCc4	čtyřicet
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hotový	hotový	k2eAgInSc1d1	hotový
vzor	vzor	k1gInSc1	vzor
se	se	k3xPyFc4	se
oddělí	oddělit	k5eAaPmIp3nS	oddělit
od	od	k7c2	od
podložky	podložka	k1gFnSc2	podložka
a	a	k8xC	a
spojí	spojit	k5eAaPmIp3nP	spojit
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
vzory	vzor	k1gInPc7	vzor
zhotovenými	zhotovený	k2eAgInPc7d1	zhotovený
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Krajkářství	krajkářství	k1gNnSc1	krajkářství
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
úroveň	úroveň	k1gFnSc4	úroveň
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
retičela	retičet	k5eAaImAgNnP	retičet
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
snímek	snímek	k1gInSc1	snímek
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
benátská	benátský	k2eAgFnSc1d1	Benátská
krajka	krajka	k1gFnSc1	krajka
<g/>
,	,	kIx,	,
point	pointa	k1gFnPc2	pointa
de	de	k?	de
gaze	gaze	k1gFnPc2	gaze
<g/>
,	,	kIx,	,
point	pointa	k1gFnPc2	pointa
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Alencon	Alencon	k1gInSc1	Alencon
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
krajka	krajka	k1gFnSc1	krajka
Pracuje	pracovat	k5eAaImIp3nS	pracovat
se	se	k3xPyFc4	se
také	také	k9	také
s	s	k7c7	s
podložkou	podložka	k1gFnSc7	podložka
<g/>
,	,	kIx,	,
na	na	k7c4	na
tu	ten	k3xDgFnSc4	ten
se	se	k3xPyFc4	se
upevní	upevnit	k5eAaPmIp3nS	upevnit
vzor	vzor	k1gInSc1	vzor
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
středu	střed	k1gInSc6	střed
výšivkou	výšivka	k1gFnSc7	výšivka
stonkovým	stonkový	k2eAgInSc7d1	stonkový
stehem	steh	k1gInSc7	steh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
řádek	řádek	k1gInSc4	řádek
stonkových	stonkový	k2eAgInPc2d1	stonkový
stehů	steh	k1gInPc2	steh
se	se	k3xPyFc4	se
přišije	přišít	k5eAaPmIp3nS	přišít
začátek	začátek	k1gInSc1	začátek
dvojité	dvojitý	k2eAgFnSc2d1	dvojitá
kovové	kovový	k2eAgFnSc2d1	kovová
niti	nit	k1gFnSc2	nit
(	(	kIx(	(
<g/>
gimpy	gimpa	k1gFnSc2	gimpa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ovíjí	ovíjet	k5eAaImIp3nS	ovíjet
smyčkovými	smyčkový	k2eAgInPc7d1	smyčkový
stehy	steh	k1gInPc7	steh
z	z	k7c2	z
pestrého	pestrý	k2eAgNnSc2d1	pestré
hedvábí	hedvábí	k1gNnSc2	hedvábí
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tvar	tvar	k1gInSc4	tvar
části	část	k1gFnSc2	část
krajkového	krajkový	k2eAgInSc2d1	krajkový
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
části	část	k1gFnPc1	část
vzoru	vzor	k1gInSc2	vzor
se	se	k3xPyFc4	se
napojují	napojovat	k5eAaImIp3nP	napojovat
přišitím	přišití	k1gNnSc7	přišití
na	na	k7c4	na
stonkové	stonkový	k2eAgInPc4d1	stonkový
stehy	steh	k1gInPc4	steh
a	a	k8xC	a
hotový	hotový	k2eAgInSc4d1	hotový
vzor	vzor	k1gInSc4	vzor
se	se	k3xPyFc4	se
oddělí	oddělit	k5eAaPmIp3nP	oddělit
od	od	k7c2	od
podložky	podložka	k1gFnSc2	podložka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přestřihnou	přestřihnout	k5eAaPmIp3nP	přestřihnout
stonkové	stonkový	k2eAgInPc1d1	stonkový
stehy	steh	k1gInPc1	steh
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
snímek	snímek	k1gInSc1	snímek
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
))	))	k?	))
Hedebo	Hedeba	k1gFnSc5	Hedeba
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
se	se	k3xPyFc4	se
buďto	buďto	k8xC	buďto
jako	jako	k9	jako
lemování	lemování	k1gNnSc1	lemování
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
efekty	efekt	k1gInPc4	efekt
všívané	všívaný	k2eAgInPc4d1	všívaný
ve	v	k7c6	v
tkanině	tkanina	k1gFnSc6	tkanina
<g/>
.	.	kIx.	.
</s>
<s>
Zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
se	se	k3xPyFc4	se
technikou	technika	k1gFnSc7	technika
hedebo	hedeba	k1gFnSc5	hedeba
<g/>
,	,	kIx,	,
obtáčecích	obtáčecí	k2eAgInPc2d1	obtáčecí
nebo	nebo	k8xC	nebo
tylových	tylový	k2eAgInPc2d1	tylový
stehů	steh	k1gInPc2	steh
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
snímek	snímek	k1gInSc1	snímek
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Tenerifa	Tenerif	k1gMnSc2	Tenerif
zvaná	zvaný	k2eAgFnSc1d1	zvaná
také	také	k9	také
sluníčková	sluníčkový	k2eAgFnSc1d1	sluníčková
(	(	kIx(	(
<g/>
kruhový	kruhový	k2eAgInSc4d1	kruhový
tvar	tvar	k1gInSc4	tvar
vzoru	vzor	k1gInSc2	vzor
s	s	k7c7	s
paprskovitě	paprskovitě	k6eAd1	paprskovitě
uspořádanými	uspořádaný	k2eAgInPc7d1	uspořádaný
spoji	spoj	k1gInPc7	spoj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krajka	krajka	k1gFnSc1	krajka
se	se	k3xPyFc4	se
šije	šít	k5eAaImIp3nS	šít
na	na	k7c6	na
podložce	podložka	k1gFnSc6	podložka
kruhového	kruhový	k2eAgInSc2d1	kruhový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
po	po	k7c4	po
zhotovení	zhotovení	k1gNnSc4	zhotovení
odstřihne	odstřihnout	k5eAaPmIp3nS	odstřihnout
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
<g/>
:	:	kIx,	:
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
splétání	splétání	k1gNnSc1	splétání
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
nití	nit	k1gFnPc2	nit
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
směrech	směr	k1gInPc6	směr
<g/>
,	,	kIx,	,
sešikmení	sešikmení	k1gNnSc1	sešikmení
a	a	k8xC	a
seskávání	seskávání	k1gNnSc1	seskávání
nití	nit	k1gFnPc2	nit
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
krajky	krajka	k1gFnSc2	krajka
<g/>
.	.	kIx.	.
</s>
<s>
Nástroje	nástroj	k1gInPc1	nástroj
<g/>
:	:	kIx,	:
paličkovací	paličkovací	k2eAgInSc1d1	paličkovací
polštář	polštář	k1gInSc1	polštář
<g/>
,	,	kIx,	,
paličky	palička	k1gFnPc1	palička
a	a	k8xC	a
špendlíky	špendlík	k1gInPc1	špendlík
Druhy	druh	k1gMnPc4	druh
vazeb	vazba	k1gFnPc2	vazba
(	(	kIx(	(
<g/>
hody	hod	k1gInPc1	hod
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
splétací	splétací	k2eAgInSc1d1	splétací
hod	hod	k1gInSc1	hod
<g/>
,	,	kIx,	,
tvarovací	tvarovací	k2eAgInSc1d1	tvarovací
hod	hod	k1gInSc1	hod
<g/>
,	,	kIx,	,
plátýnko	plátýnko	k1gNnSc1	plátýnko
<g/>
,	,	kIx,	,
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
otvor	otvor	k1gInSc1	otvor
Z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
variant	varianta	k1gFnPc2	varianta
paličkované	paličkovaný	k2eAgFnSc2d1	paličkovaná
krajky	krajka	k1gFnSc2	krajka
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
např.	např.	kA	např.
<g/>
:	:	kIx,	:
bruselská	bruselský	k2eAgFnSc1d1	bruselská
<g/>
,	,	kIx,	,
valencienská	valencienský	k2eAgFnSc1d1	valencienský
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
duchesse	duchesse	k1gFnSc1	duchesse
<g/>
,	,	kIx,	,
vamberecká	vamberecký	k2eAgFnSc1d1	vamberecká
Princip	princip	k1gInSc1	princip
<g/>
:	:	kIx,	:
proplétání	proplétání	k1gNnSc4	proplétání
oček	očko	k1gNnPc2	očko
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
niti	nit	k1gFnSc2	nit
<g/>
,	,	kIx,	,
vytváření	vytváření	k1gNnSc1	vytváření
smyček	smyčka	k1gFnPc2	smyčka
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
háčkovací	háčkovací	k2eAgFnSc2d1	háčkovací
jehly	jehla	k1gFnSc2	jehla
Druhy	druh	k1gInPc1	druh
háčkovacích	háčkovací	k2eAgNnPc2d1	háčkovací
oček	očko	k1gNnPc2	očko
<g/>
:	:	kIx,	:
řetízkové	řetízkový	k2eAgNnSc1d1	řetízkové
<g/>
,	,	kIx,	,
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
,	,	kIx,	,
sloupek	sloupek	k1gInSc1	sloupek
<g/>
,	,	kIx,	,
dvojité	dvojitý	k2eAgFnPc1d1	dvojitá
<g/>
,	,	kIx,	,
svazková	svazkový	k2eAgFnSc1d1	svazková
<g/>
,	,	kIx,	,
obtáčená	obtáčený	k2eAgFnSc1d1	obtáčená
Jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
<g/>
,	,	kIx,	,
tuniské	tuniský	k2eAgNnSc4d1	tuniské
<g/>
,	,	kIx,	,
filetové	filetový	k2eAgNnSc4d1	filetový
<g/>
,	,	kIx,	,
vidličkové	vidličkový	k2eAgNnSc4d1	vidličkový
<g/>
,	,	kIx,	,
tyčinkové	tyčinkový	k2eAgNnSc4d1	tyčinkové
<g/>
,	,	kIx,	,
makramé	makramý	k2eAgNnSc4d1	makramé
<g/>
,	,	kIx,	,
irské	irský	k2eAgNnSc4d1	irské
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
snímek	snímek	k1gInSc1	snímek
<g/>
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
))	))	k?	))
Pletená	pletený	k2eAgFnSc1d1	pletená
krajka	krajka	k1gFnSc1	krajka
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
provlékáním	provlékání	k1gNnSc7	provlékání
smyček	smyčka	k1gFnPc2	smyčka
a	a	k8xC	a
kliček	klička	k1gFnPc2	klička
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
dvou	dva	k4xCgNnPc2	dva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
jehlic	jehlice	k1gFnPc2	jehlice
<g/>
.	.	kIx.	.
</s>
<s>
Krajka	krajka	k1gFnSc1	krajka
má	mít	k5eAaImIp3nS	mít
rub	rub	k1gInSc4	rub
a	a	k8xC	a
líc	líc	k1gInSc4	líc
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
páratelná	páratelný	k2eAgFnSc1d1	páratelný
<g/>
.	.	kIx.	.
</s>
<s>
Pletením	pleteň	k1gFnPc3	pleteň
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
krajek	krajka	k1gFnPc2	krajka
<g/>
.	.	kIx.	.
</s>
<s>
Pletení	pletení	k1gNnSc1	pletení
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
úkony	úkon	k1gInPc4	úkon
<g/>
:	:	kIx,	:
nahození	nahození	k1gNnSc1	nahození
<g/>
,	,	kIx,	,
pletení	pletení	k1gNnSc1	pletení
a	a	k8xC	a
odřetízkování	odřetízkování	k1gNnSc1	odřetízkování
Zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
efekty	efekt	k1gInPc1	efekt
<g/>
:	:	kIx,	:
Ovinuté	ovinutý	k2eAgNnSc1d1	ovinuté
očko	očko	k1gNnSc1	očko
<g/>
,	,	kIx,	,
odnímání	odnímání	k1gNnSc1	odnímání
<g/>
,	,	kIx,	,
zkřížení	zkřížení	k1gNnSc1	zkřížení
<g/>
,	,	kIx,	,
zešikmení	zešikmení	k1gNnSc1	zešikmení
oček	očko	k1gNnPc2	očko
<g/>
,	,	kIx,	,
přetahování	přetahování	k1gNnSc1	přetahování
oček	očko	k1gNnPc2	očko
<g/>
,	,	kIx,	,
spletení	spletený	k2eAgMnPc1d1	spletený
více	hodně	k6eAd2	hodně
oček	očko	k1gNnPc2	očko
dohromady	dohromady	k6eAd1	dohromady
Drhání	drhání	k1gNnSc4	drhání
neboli	neboli	k8xC	neboli
makramé	makramý	k2eAgNnSc1d1	makramé
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
textilních	textilní	k2eAgFnPc2d1	textilní
technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
uzlík	uzlík	k1gInSc1	uzlík
<g/>
,	,	kIx,	,
nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
vazba	vazba	k1gFnSc1	vazba
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
organická	organický	k2eAgFnSc1d1	organická
třáseň	třáseň	k1gFnSc1	třáseň
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
svazováním	svazování	k1gNnSc7	svazování
nití	nit	k1gFnPc2	nit
odstávajících	odstávající	k2eAgFnPc2d1	odstávající
ze	z	k7c2	z
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
speciálním	speciální	k2eAgInPc3d1	speciální
efektům	efekt	k1gInPc3	efekt
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
křížové	křížový	k2eAgInPc4d1	křížový
<g/>
,	,	kIx,	,
řetízkové	řetízkový	k2eAgInPc4d1	řetízkový
<g/>
,	,	kIx,	,
perlové	perlový	k2eAgInPc4d1	perlový
uzly	uzel	k1gInPc4	uzel
<g/>
,	,	kIx,	,
reliéfové	reliéfový	k2eAgInPc4d1	reliéfový
obrazce	obrazec	k1gInPc4	obrazec
z	z	k7c2	z
uzlíků	uzlík	k1gInPc2	uzlík
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mouche	mouche	k6eAd1	mouche
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
drhané	drhaný	k2eAgFnSc2d1	drhaná
krajky	krajka	k1gFnSc2	krajka
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
K	k	k7c3	k
původním	původní	k2eAgFnPc3d1	původní
technikám	technika	k1gFnPc3	technika
makramé	makramý	k2eAgInPc4d1	makramý
se	se	k3xPyFc4	se
nepoužívaly	používat	k5eNaImAgInP	používat
žádné	žádný	k3yNgInPc1	žádný
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ke	k	k7c3	k
kombinacím	kombinace	k1gFnPc3	kombinace
s	s	k7c7	s
pozamenty	pozamenty	k?	pozamenty
nebo	nebo	k8xC	nebo
s	s	k7c7	s
paličkováním	paličkování	k1gNnSc7	paličkování
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
háčkovací	háčkovací	k2eAgFnPc1d1	háčkovací
nebo	nebo	k8xC	nebo
šicí	šicí	k2eAgFnPc1d1	šicí
jehly	jehla	k1gFnPc1	jehla
<g/>
,	,	kIx,	,
vázací	vázací	k2eAgInPc1d1	vázací
polštáře	polštář	k1gInPc1	polštář
aj.	aj.	kA	aj.
zvaná	zvaný	k2eAgFnSc1d1	zvaná
také	také	k9	také
occhi	occhi	k6eAd1	occhi
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
bohatými	bohatý	k2eAgInPc7d1	bohatý
zdobenými	zdobený	k2eAgInPc7d1	zdobený
vzory	vzor	k1gInPc7	vzor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
tvořené	tvořený	k2eAgInPc1d1	tvořený
z	z	k7c2	z
vázaných	vázaný	k2eAgInPc2d1	vázaný
uzlíků	uzlík	k1gInPc2	uzlík
<g/>
,	,	kIx,	,
vytvarovaných	vytvarovaný	k2eAgInPc2d1	vytvarovaný
do	do	k7c2	do
obloučků	oblouček	k1gInPc2	oblouček
<g/>
,	,	kIx,	,
slziček	slzička	k1gFnPc2	slzička
a	a	k8xC	a
kroužků	kroužek	k1gInPc2	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
varianta	varianta	k1gFnSc1	varianta
výroby	výroba	k1gFnSc2	výroba
frivolitek	frivolitka	k1gFnPc2	frivolitka
je	být	k5eAaImIp3nS	být
vázání	vázání	k1gNnSc1	vázání
z	z	k7c2	z
člunku	člunek	k1gInSc2	člunek
a	a	k8xC	a
klubíčka	klubíčko	k1gNnSc2	klubíčko
<g/>
.	.	kIx.	.
.	.	kIx.	.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
snímek	snímek	k1gInSc1	snímek
<g/>
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
))	))	k?	))
Člunek	člunek	k1gInSc1	člunek
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
zásobník	zásobník	k1gInSc1	zásobník
nitě	nit	k1gFnSc2	nit
<g/>
.	.	kIx.	.
</s>
<s>
Složitější	složitý	k2eAgFnSc7d2	složitější
variantou	varianta	k1gFnSc7	varianta
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc4	použití
dvou	dva	k4xCgInPc2	dva
člunků	člunek	k1gInPc2	člunek
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
člunku	člunek	k1gInSc2	člunek
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
jehlu	jehla	k1gFnSc4	jehla
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
šitou	šitý	k2eAgFnSc4d1	šitá
frivolitkovou	frivolitkový	k2eAgFnSc4d1	Frivolitková
krajku	krajka	k1gFnSc4	krajka
Krajka	krajka	k1gFnSc1	krajka
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
kombinací	kombinace	k1gFnSc7	kombinace
vázání	vázání	k1gNnSc1	vázání
(	(	kIx(	(
<g/>
zhotovení	zhotovení	k1gNnSc1	zhotovení
sítě	síť	k1gFnSc2	síť
<g/>
)	)	kIx)	)
a	a	k8xC	a
šití	šití	k1gNnSc4	šití
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zhotovit	zhotovit	k5eAaPmF	zhotovit
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
niti	nit	k1gFnSc2	nit
v	v	k7c6	v
několika	několik	k4yIc6	několik
formách	forma	k1gFnPc6	forma
ručně	ručně	k6eAd1	ručně
(	(	kIx(	(
<g/>
čtyřúhelníky	čtyřúhelník	k1gInPc4	čtyřúhelník
<g/>
,	,	kIx,	,
pruhy	pruh	k1gInPc4	pruh
<g/>
,	,	kIx,	,
šikmo	šikmo	k6eAd1	šikmo
<g/>
,	,	kIx,	,
do	do	k7c2	do
kruhu	kruh	k1gInSc2	kruh
<g/>
,	,	kIx,	,
zdobená	zdobený	k2eAgFnSc1d1	zdobená
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
strojově	strojově	k6eAd1	strojově
(	(	kIx(	(
<g/>
vázání	vázání	k1gNnSc4	vázání
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
nití	nit	k1gFnPc2	nit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
síť	síť	k1gFnSc4	síť
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
skaná	skaný	k2eAgFnSc1d1	skaná
příze	příze	k1gFnSc1	příze
ze	z	k7c2	z
lnu	len	k1gInSc2	len
<g/>
,	,	kIx,	,
bavlny	bavlna	k1gFnSc2	bavlna
nebo	nebo	k8xC	nebo
i	i	k9	i
ze	z	k7c2	z
syntetických	syntetický	k2eAgInPc2d1	syntetický
filamentů	filament	k1gInPc2	filament
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
se	se	k3xPyFc4	se
napne	napnout	k5eAaPmIp3nS	napnout
do	do	k7c2	do
rámu	rám	k1gInSc2	rám
zaplňuje	zaplňovat	k5eAaImIp3nS	zaplňovat
"	"	kIx"	"
<g/>
šitím	šití	k1gNnSc7	šití
<g/>
"	"	kIx"	"
resp.	resp.	kA	resp.
látáním	látání	k1gNnSc7	látání
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
plátnového	plátnový	k2eAgInSc2d1	plátnový
nebo	nebo	k8xC	nebo
látacího	látací	k2eAgInSc2d1	látací
stehu	steh	k1gInSc2	steh
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
různé	různý	k2eAgInPc1d1	různý
efekty	efekt	k1gInPc1	efekt
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kníry	knír	k1gInPc1	knír
<g/>
,	,	kIx,	,
piké	piké	k1gNnPc1	piké
<g/>
,	,	kIx,	,
gobelínový	gobelínový	k2eAgInSc1d1	gobelínový
steh	steh	k1gInSc1	steh
<g/>
,	,	kIx,	,
smyčky	smyčka	k1gFnPc1	smyčka
aj.	aj.	kA	aj.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
snímek	snímek	k1gInSc1	snímek
<g/>
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
<g />
.	.	kIx.	.
</s>
<s>
))	))	k?	))
(	(	kIx(	(
<g/>
point	pointa	k1gFnPc2	pointa
lace	laka	k1gFnSc6	laka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
tkaný	tkaný	k2eAgInSc1d1	tkaný
<g/>
,	,	kIx,	,
pletený	pletený	k2eAgInSc1d1	pletený
<g/>
,	,	kIx,	,
háčkovaný	háčkovaný	k2eAgInSc1d1	háčkovaný
nebo	nebo	k8xC	nebo
paličkovaný	paličkovaný	k2eAgInSc1d1	paličkovaný
<g/>
)	)	kIx)	)
pásek	pásek	k1gInSc1	pásek
(	(	kIx(	(
<g/>
líčko	líčko	k1gNnSc1	líčko
<g/>
)	)	kIx)	)
zformuje	zformovat	k5eAaPmIp3nS	zformovat
do	do	k7c2	do
určitého	určitý	k2eAgInSc2d1	určitý
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
našije	našít	k5eAaPmIp3nS	našít
na	na	k7c4	na
podkladovou	podkladový	k2eAgFnSc4d1	podkladová
tkaninu	tkanina	k1gFnSc4	tkanina
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
evropská	evropský	k2eAgFnSc1d1	Evropská
krajka	krajka	k1gFnSc1	krajka
<g/>
.	.	kIx.	.
</s>
<s>
Líčko	líčko	k1gNnSc1	líčko
se	se	k3xPyFc4	se
doplní	doplnit	k5eAaPmIp3nS	doplnit
resp.	resp.	kA	resp.
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
s	s	k7c7	s
šitými	šitý	k2eAgInPc7d1	šitý
nebo	nebo	k8xC	nebo
paličkovanými	paličkovaný	k2eAgInPc7d1	paličkovaný
efekty	efekt	k1gInPc7	efekt
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
snímek	snímek	k1gInSc1	snímek
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
))	))	k?	))
Základní	základní	k2eAgInSc1d1	základní
element	element	k1gInSc1	element
vyšívání	vyšívání	k1gNnSc2	vyšívání
je	být	k5eAaImIp3nS	být
steh	steh	k1gInSc1	steh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
variantách	varianta	k1gFnPc6	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
řetízkový	řetízkový	k2eAgInSc1d1	řetízkový
<g/>
,	,	kIx,	,
kroužkovací	kroužkovací	k2eAgInSc1d1	kroužkovací
<g/>
,	,	kIx,	,
plný	plný	k2eAgInSc1d1	plný
steh	steh	k1gInSc1	steh
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Kombinací	kombinace	k1gFnSc7	kombinace
vyšívání	vyšívání	k1gNnSc2	vyšívání
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
technikami	technika	k1gFnPc7	technika
vznikají	vznikat	k5eAaImIp3nP	vznikat
zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
druhy	druh	k1gInPc1	druh
krajek	krajka	k1gFnPc2	krajka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Tylová	tylový	k2eAgFnSc1d1	tylová
krajka	krajka	k1gFnSc1	krajka
se	se	k3xPyFc4	se
zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
technikou	technika	k1gFnSc7	technika
vyšívání	vyšívání	k1gNnPc2	vyšívání
<g/>
,	,	kIx,	,
vplétáním	vplétání	k1gNnSc7	vplétání
niti	nit	k1gFnSc2	nit
do	do	k7c2	do
tylového	tylový	k2eAgInSc2d1	tylový
podkladu	podklad	k1gInSc2	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Dotahovaná	dotahovaný	k2eAgFnSc1d1	dotahovaná
(	(	kIx(	(
<g/>
point	pointa	k1gFnPc2	pointa
de	de	k?	de
Saxe	sax	k1gInSc5	sax
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
jemná	jemný	k2eAgFnSc1d1	jemná
krajka	krajka	k1gFnSc1	krajka
na	na	k7c6	na
bavlněném	bavlněný	k2eAgInSc6d1	bavlněný
batistu	batist	k1gInSc6	batist
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Sasku	Sasko	k1gNnSc6	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Ažurová	ažurový	k2eAgFnSc1d1	ažurový
krajka	krajka	k1gFnSc1	krajka
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
výšivek	výšivka	k1gFnPc2	výšivka
s	s	k7c7	s
nitěmi	nit	k1gFnPc7	nit
vytaženými	vytažený	k2eAgFnPc7d1	vytažená
ze	z	k7c2	z
lněné	lněný	k2eAgFnSc2d1	lněná
nebo	nebo	k8xC	nebo
bavlněné	bavlněný	k2eAgFnSc2d1	bavlněná
tkaniny	tkanina	k1gFnSc2	tkanina
v	v	k7c6	v
plátnové	plátnový	k2eAgFnSc6d1	plátnová
vazbě	vazba	k1gFnSc6	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
ažura	ažura	k1gFnSc1	ažura
se	se	k3xPyFc4	se
vyšívá	vyšívat	k5eAaImIp3nS	vyšívat
jen	jen	k9	jen
z	z	k7c2	z
osnovních	osnovní	k2eAgFnPc2d1	osnovní
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
z	z	k7c2	z
útkových	útkový	k2eAgFnPc2d1	útková
nití	nit	k1gFnPc2	nit
<g/>
,	,	kIx,	,
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ažura	ažura	k1gFnSc1	ažura
není	být	k5eNaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
uznávána	uznáván	k2eAgFnSc1d1	uznávána
jako	jako	k8xS	jako
krajka	krajka	k1gFnSc1	krajka
<g/>
.	.	kIx.	.
</s>
<s>
Vystřihovaná	vystřihovaný	k2eAgFnSc1d1	vystřihovaná
krajka	krajka	k1gFnSc1	krajka
-	-	kIx~	-
V	v	k7c6	v
hotových	hotový	k2eAgInPc6d1	hotový
vzorech	vzor	k1gInPc6	vzor
vyšitých	vyšitý	k2eAgMnPc2d1	vyšitý
na	na	k7c6	na
tkanině	tkanina	k1gFnSc6	tkanina
se	se	k3xPyFc4	se
vystřihují	vystřihovat	k5eAaImIp3nP	vystřihovat
otvory	otvor	k1gInPc1	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
richelieu	richelieu	k1gNnSc1	richelieu
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
snímek	snímek	k1gInSc1	snímek
<g/>
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
otvorů	otvor	k1gInPc2	otvor
všívány	všíván	k2eAgInPc4d1	všíván
pikotky	pikotek	k1gInPc4	pikotek
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
k	k	k7c3	k
průmyslové	průmyslový	k2eAgFnSc3d1	průmyslová
výrobě	výroba	k1gFnSc3	výroba
krajek	krajka	k1gFnPc2	krajka
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
rašl	rašl	k1gInSc1	rašl
<g/>
,	,	kIx,	,
bobinet	bobinet	k1gInSc1	bobinet
<g/>
,	,	kIx,	,
vyšívací	vyšívací	k2eAgInSc1d1	vyšívací
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
paličkovací	paličkovací	k2eAgInSc1d1	paličkovací
stroj	stroj	k1gInSc1	stroj
a	a	k8xC	a
ojediněle	ojediněle	k6eAd1	ojediněle
galonový	galonový	k2eAgInSc1d1	galonový
stávek	stávek	k1gInSc1	stávek
nebo	nebo	k8xC	nebo
okrouhlý	okrouhlý	k2eAgInSc1d1	okrouhlý
pletací	pletací	k2eAgInSc1d1	pletací
stroj	stroj	k1gInSc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Osnovní	osnovní	k2eAgFnPc1d1	osnovní
stávky	stávka	k1gFnPc1	stávka
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rovněž	rovněž	k9	rovněž
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
na	na	k7c4	na
zhotovení	zhotovení	k1gNnSc4	zhotovení
krajkoviny	krajkovina	k1gFnSc2	krajkovina
<g/>
,	,	kIx,	,
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
však	však	k9	však
není	být	k5eNaImIp3nS	být
jejich	jejich	k3xOp3gNnPc4	jejich
použití	použití	k1gNnPc4	použití
známé	známý	k2eAgFnPc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Vzorovací	vzorovací	k2eAgNnPc1d1	vzorovací
zařízení	zařízení	k1gNnPc1	zařízení
na	na	k7c6	na
rašlu	rašl	k1gInSc6	rašl
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
dají	dát	k5eAaPmIp3nP	dát
imitovat	imitovat	k5eAaBmF	imitovat
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgMnPc4	všechen
techniky	technik	k1gMnPc4	technik
ručně	ručně	k6eAd1	ručně
vyráběných	vyráběný	k2eAgFnPc2d1	vyráběná
krajek	krajka	k1gFnPc2	krajka
při	při	k7c6	při
vysokém	vysoký	k2eAgInSc6d1	vysoký
výkonu	výkon	k1gInSc6	výkon
stroje	stroj	k1gInSc2	stroj
(	(	kIx(	(
<g/>
nejméně	málo	k6eAd3	málo
20	[number]	k4	20
m	m	kA	m
<g/>
2	[number]	k4	2
pletené	pletený	k2eAgFnSc2d1	pletená
krajkoviny	krajkovina	k1gFnSc2	krajkovina
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzorování	vzorování	k1gNnSc1	vzorování
<g/>
:	:	kIx,	:
kladecí	kladecí	k2eAgFnPc1d1	kladecí
jehly	jehla	k1gFnPc1	jehla
na	na	k7c6	na
vzorovacích	vzorovací	k2eAgFnPc6d1	vzorovací
lištách	lišta	k1gFnPc6	lišta
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
90	[number]	k4	90
na	na	k7c4	na
stroj	stroj	k1gInSc4	stroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srážecí	srážecí	k2eAgInSc1d1	srážecí
plech	plech	k1gInSc1	plech
<g/>
,	,	kIx,	,
zanášení	zanášení	k1gNnSc1	zanášení
útku	útek	k1gInSc2	útek
<g/>
,	,	kIx,	,
žakárové	žakárový	k2eAgNnSc4d1	žakárové
ústrojí	ústrojí	k1gNnSc4	ústrojí
Krajkovina	krajkovina	k1gFnSc1	krajkovina
z	z	k7c2	z
rašlů	rašl	k1gInPc2	rašl
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
na	na	k7c4	na
záclony	záclona	k1gFnPc4	záclona
<g/>
,	,	kIx,	,
ubrusy	ubrus	k1gInPc4	ubrus
<g/>
,	,	kIx,	,
svrchní	svrchní	k2eAgInPc4d1	svrchní
oděvy	oděv	k1gInPc4	oděv
<g/>
,	,	kIx,	,
prádlo	prádlo	k1gNnSc4	prádlo
a	a	k8xC	a
lemovky	lemovka	k1gFnSc2	lemovka
Moderní	moderní	k2eAgInPc1d1	moderní
stroje	stroj	k1gInPc1	stroj
<g />
.	.	kIx.	.
</s>
<s>
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
specializované	specializovaný	k2eAgInPc1d1	specializovaný
na	na	k7c4	na
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
použití	použití	k1gNnSc2	použití
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Na	na	k7c6	na
rašlu	rašl	k1gInSc6	rašl
s	s	k7c7	s
žakárovým	žakárový	k2eAgNnSc7d1	žakárové
ústrojím	ústrojí	k1gNnSc7	ústrojí
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vyrábět	vyrábět	k5eAaImF	vyrábět
krajkové	krajkový	k2eAgFnPc1d1	krajková
záclony	záclona	k1gFnPc1	záclona
s	s	k7c7	s
pracovní	pracovní	k2eAgFnSc7d1	pracovní
šířkou	šířka	k1gFnSc7	šířka
330	[number]	k4	330
cm	cm	kA	cm
při	při	k7c6	při
700	[number]	k4	700
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Stroj	stroj	k1gInSc1	stroj
se	s	k7c7	s
vzorovacími	vzorovací	k2eAgFnPc7d1	vzorovací
kladecími	kladecí	k2eAgFnPc7d1	kladecí
jehlami	jehla	k1gFnPc7	jehla
na	na	k7c6	na
30	[number]	k4	30
lištách	lišta	k1gFnPc6	lišta
může	moct	k5eAaImIp3nS	moct
vyrábět	vyrábět	k5eAaImF	vyrábět
při	při	k7c6	při
340	[number]	k4	340
cm	cm	kA	cm
prac	prac	k1gInSc1	prac
<g/>
.	.	kIx.	.
šířky	šířka	k1gFnSc2	šířka
např.	např.	kA	např.
tuhé	tuhý	k2eAgFnSc2d1	tuhá
nebo	nebo	k8xC	nebo
elastické	elastický	k2eAgFnSc2d1	elastická
krajkové	krajkový	k2eAgFnSc2d1	krajková
stuhy	stuha	k1gFnSc2	stuha
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
příklad	příklad	k1gInSc4	příklad
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
))	))	k?	))
při	při	k7c6	při
800	[number]	k4	800
otáčkách	otáčka	k1gFnPc6	otáčka
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
strojích	stroj	k1gInPc6	stroj
s	s	k7c7	s
kladecími	kladecí	k2eAgFnPc7d1	kladecí
jehlami	jehla	k1gFnPc7	jehla
na	na	k7c6	na
2	[number]	k4	2
lištách	lišta	k1gFnPc6	lišta
pro	pro	k7c4	pro
základní	základní	k2eAgFnSc4d1	základní
vazbu	vazba	k1gFnSc4	vazba
<g/>
,	,	kIx,	,
na	na	k7c4	na
90	[number]	k4	90
pro	pro	k7c4	pro
"	"	kIx"	"
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
"	"	kIx"	"
vzorování	vzorování	k1gNnSc4	vzorování
a	a	k8xC	a
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
kladecí	kladecí	k2eAgFnSc7d1	kladecí
lištou	lišta	k1gFnSc7	lišta
řízenou	řízený	k2eAgFnSc4d1	řízená
žakárovým	žakárový	k2eAgNnSc7d1	žakárové
ústrojím	ústrojí	k1gNnSc7	ústrojí
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vyrábět	vyrábět	k5eAaImF	vyrábět
krajkovina	krajkovina	k1gFnSc1	krajkovina
pro	pro	k7c4	pro
oděvní	oděvní	k2eAgInPc4d1	oděvní
účely	účel	k1gInPc4	účel
s	s	k7c7	s
pracovní	pracovní	k2eAgFnSc7d1	pracovní
šířkou	šířka	k1gFnSc7	šířka
cca	cca	kA	cca
335	[number]	k4	335
cm	cm	kA	cm
při	při	k7c6	při
400	[number]	k4	400
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
na	na	k7c4	na
šaty	šat	k1gInPc4	šat
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
))	))	k?	))
Stroj	stroj	k1gInSc1	stroj
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
ruční	ruční	k2eAgNnSc4d1	ruční
paličkování	paličkování	k1gNnSc4	paličkování
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
bobinetové	bobinetový	k2eAgFnSc2d1	bobinetový
vazby	vazba	k1gFnSc2	vazba
<g/>
:	:	kIx,	:
Drobný	drobný	k2eAgInSc1d1	drobný
člunek	člunek	k1gInSc1	člunek
s	s	k7c7	s
bobinovou	bobinový	k2eAgFnSc7d1	bobinový
nití	nit	k1gFnSc7	nit
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
diagonálně	diagonálně	k6eAd1	diagonálně
k	k	k7c3	k
osnově	osnova	k1gFnSc3	osnova
a	a	k8xC	a
obepíná	obepínat	k5eAaImIp3nS	obepínat
dvě	dva	k4xCgFnPc4	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
osnovních	osnovní	k2eAgFnPc2d1	osnovní
nití	nit	k1gFnPc2	nit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
síťový	síťový	k2eAgInSc4d1	síťový
útvar	útvar	k1gInSc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Vazba	vazba	k1gFnSc1	vazba
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
kombinovat	kombinovat	k5eAaImF	kombinovat
dalším	další	k2eAgMnPc3d1	další
vzorovacími	vzorovací	k2eAgFnPc7d1	vzorovací
nitěmi	nit	k1gFnPc7	nit
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
odvíjejí	odvíjet	k5eAaImIp3nP	odvíjet
z	z	k7c2	z
cívek	cívka	k1gFnPc2	cívka
<g/>
.	.	kIx.	.
</s>
<s>
Vzorování	vzorování	k1gNnSc1	vzorování
na	na	k7c6	na
bobinetových	bobinetový	k2eAgInPc6d1	bobinetový
strojích	stroj	k1gInPc6	stroj
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
řízeno	řídit	k5eAaImNgNnS	řídit
žakárovým	žakárový	k2eAgNnSc7d1	žakárové
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
bobinetové	bobinetový	k2eAgInPc4d1	bobinetový
stroje	stroj	k1gInPc4	stroj
ze	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vazebním	vazební	k2eAgInPc3d1	vazební
efektům	efekt	k1gInPc3	efekt
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
centre	centr	k1gInSc5	centr
gimp	gimp	k1gInSc4	gimp
(	(	kIx(	(
<g/>
z	z	k7c2	z
hrubších	hrubý	k2eAgFnPc2d2	hrubší
přízí	příz	k1gFnPc2	příz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
valenciennes	valenciennes	k1gMnSc1	valenciennes
(	(	kIx(	(
<g/>
velmi	velmi	k6eAd1	velmi
jemná	jemný	k2eAgFnSc1d1	jemná
napodobenina	napodobenina	k1gFnSc1	napodobenina
valencienské	valencienský	k2eAgFnSc2d1	valencienský
krajky	krajka	k1gFnSc2	krajka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
imitace	imitace	k1gFnSc1	imitace
ručně	ručně	k6eAd1	ručně
paličkované	paličkovaný	k2eAgFnSc2d1	paličkovaná
chantilly	chantilla	k1gFnSc2	chantilla
aj.	aj.	kA	aj.
Varianta	varianta	k1gFnSc1	varianta
bobinetu	bobinet	k1gInSc2	bobinet
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
vyvinutý	vyvinutý	k2eAgMnSc1d1	vyvinutý
<g/>
)	)	kIx)	)
tzv.	tzv.	kA	tzv.
záclonový	záclonový	k2eAgInSc1d1	záclonový
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
zhotovovat	zhotovovat	k5eAaImF	zhotovovat
vzory	vzor	k1gInPc4	vzor
s	s	k7c7	s
otevřenější	otevřený	k2eAgFnSc7d2	otevřenější
strukturou	struktura	k1gFnSc7	struktura
a	a	k8xC	a
větší	veliký	k2eAgFnSc7d2	veliký
střídou	střída	k1gFnSc7	střída
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
efektům	efekt	k1gInPc3	efekt
vazební	vazební	k2eAgFnSc2d1	vazební
techniky	technika	k1gFnSc2	technika
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
stroji	stroj	k1gInSc6	stroj
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
single-tie	singleie	k1gFnSc1	single-tie
<g/>
,	,	kIx,	,
double-tie	doubleie	k1gFnSc1	double-tie
a	a	k8xC	a
double-action	doublection	k1gInSc1	double-action
Zatímco	zatímco	k8xS	zatímco
bobinetové	bobinetový	k2eAgInPc1d1	bobinetový
stroje	stroj	k1gInPc1	stroj
měly	mít	k5eAaImAgInP	mít
pracovní	pracovní	k2eAgFnSc4d1	pracovní
šířku	šířka	k1gFnSc4	šířka
cca	cca	kA	cca
5	[number]	k4	5
m	m	kA	m
<g/>
,	,	kIx,	,
obnášela	obnášet	k5eAaImAgFnS	obnášet
pracovní	pracovní	k2eAgFnSc1d1	pracovní
šířka	šířka	k1gFnSc1	šířka
záclonových	záclonový	k2eAgInPc2d1	záclonový
strojů	stroj	k1gInPc2	stroj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
známé	známý	k2eAgInPc1d1	známý
exempláře	exemplář	k1gInPc1	exemplář
od	od	k7c2	od
obou	dva	k4xCgFnPc2	dva
typů	typ	k1gInPc2	typ
stroje	stroj	k1gInPc1	stroj
pocházejí	pocházet	k5eAaImIp3nP	pocházet
asi	asi	k9	asi
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
třetiny	třetina	k1gFnSc2	třetina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
není	být	k5eNaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
žádný	žádný	k3yNgInSc4	žádný
výrobce	výrobce	k1gMnPc4	výrobce
těchto	tento	k3xDgNnPc2	tento
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyšívání	vyšívání	k1gNnSc3	vyšívání
krajek	krajka	k1gFnPc2	krajka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
člunkové	člunkový	k2eAgInPc1d1	člunkový
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
vyšívací	vyšívací	k2eAgInPc4d1	vyšívací
steh	steh	k1gInSc4	steh
tvoří	tvořit	k5eAaImIp3nP	tvořit
spojení	spojení	k1gNnPc1	spojení
jehelní	jehelní	k2eAgFnSc2d1	jehelní
a	a	k8xC	a
člunkové	člunkový	k2eAgFnSc2d1	člunková
niti	nit	k1gFnSc2	nit
s	s	k7c7	s
podkladovou	podkladový	k2eAgFnSc7d1	podkladová
textilií	textilie	k1gFnSc7	textilie
<g/>
.	.	kIx.	.
</s>
<s>
Stehy	steh	k1gInPc1	steh
(	(	kIx(	(
<g/>
šicí	šicí	k2eAgFnSc2d1	šicí
<g/>
,	,	kIx,	,
ploché	plochý	k2eAgFnSc2d1	plochá
<g/>
,	,	kIx,	,
stonkové	stonkový	k2eAgFnSc2d1	stonková
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
lemují	lemovat	k5eAaImIp3nP	lemovat
nebo	nebo	k8xC	nebo
zaplňují	zaplňovat	k5eAaImIp3nP	zaplňovat
otvory	otvor	k1gInPc1	otvor
v	v	k7c6	v
podkladu	podklad	k1gInSc6	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Otvory	otvor	k1gInPc1	otvor
jsou	být	k5eAaImIp3nP	být
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
elementů	element	k1gInPc2	element
krajky	krajka	k1gFnSc2	krajka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
názvu	název	k1gInSc2	název
techniky	technika	k1gFnSc2	technika
jejich	jejich	k3xOp3gNnSc2	jejich
zhotovení	zhotovení	k1gNnSc2	zhotovení
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označují	označovat	k5eAaImIp3nP	označovat
druhy	druh	k1gInPc1	druh
vyšívané	vyšívaný	k2eAgFnSc2d1	vyšívaná
krajky	krajka	k1gFnSc2	krajka
<g/>
:	:	kIx,	:
Tylové	Tylové	k2eAgInSc1d1	Tylové
-	-	kIx~	-
tyl	tyl	k1gInSc1	tyl
jako	jako	k8xS	jako
podkladová	podkladový	k2eAgFnSc1d1	podkladová
textilie	textilie	k1gFnSc1	textilie
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
otvorů	otvor	k1gInPc2	otvor
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
jen	jen	k9	jen
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
výrobě	výroba	k1gFnSc6	výroba
zamýšlenému	zamýšlený	k2eAgInSc3d1	zamýšlený
vzoru	vzor	k1gInSc3	vzor
krajky	krajka	k1gFnSc2	krajka
Vrtané	vrtaný	k2eAgFnSc2d1	vrtaná
–	–	k?	–
otvory	otvor	k1gInPc7	otvor
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
podkladu	podklad	k1gInSc2	podklad
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nevrtají	vrtat	k5eNaImIp3nP	vrtat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vpichují	vpichovat	k5eAaImIp3nP	vpichovat
<g/>
,	,	kIx,	,
vpichování	vpichování	k1gNnSc1	vpichování
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
člunkovém	člunkový	k2eAgInSc6d1	člunkový
stroji	stroj	k1gInSc6	stroj
Leptané	leptaný	k2eAgFnPc1d1	leptaná
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
nejčastěji	často	k6eAd3	často
zvané	zvaný	k2eAgFnPc1d1	zvaná
<g/>
:	:	kIx,	:
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
–	–	k?	–
podkladová	podkladový	k2eAgFnSc1d1	podkladová
textilie	textilie	k1gFnSc1	textilie
se	se	k3xPyFc4	se
na	na	k7c6	na
určitých	určitý	k2eAgNnPc6d1	určité
místech	místo	k1gNnPc6	místo
zničí	zničit	k5eAaPmIp3nS	zničit
chemickými	chemický	k2eAgInPc7d1	chemický
prostředky	prostředek	k1gInPc7	prostředek
Vystřihované	vystřihovaný	k2eAgInPc1d1	vystřihovaný
–	–	k?	–
otvory	otvor	k1gInPc1	otvor
vznikají	vznikat	k5eAaImIp3nP	vznikat
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k9	jako
u	u	k7c2	u
ručně	ručně	k6eAd1	ručně
zhotovených	zhotovený	k2eAgFnPc2d1	zhotovená
výšivek	výšivka	k1gFnPc2	výšivka
<g/>
.	.	kIx.	.
</s>
<s>
Člunkové	člunkový	k2eAgInPc1d1	člunkový
stroje	stroj	k1gInPc1	stroj
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nP	stavit
pro	pro	k7c4	pro
vyšívání	vyšívání	k1gNnSc4	vyšívání
pásů	pás	k1gInPc2	pás
textilií	textilie	k1gFnPc2	textilie
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
do	do	k7c2	do
cca	cca	kA	cca
30	[number]	k4	30
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
pracuje	pracovat	k5eAaImIp3nS	pracovat
synchronně	synchronně	k6eAd1	synchronně
až	až	k9	až
s	s	k7c7	s
1100	[number]	k4	1100
vyšívacími	vyšívací	k2eAgFnPc7d1	vyšívací
hlavami	hlava	k1gFnPc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
stroje	stroj	k1gInPc1	stroj
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
teoreticky	teoreticky	k6eAd1	teoreticky
výkonu	výkon	k1gInSc2	výkon
600	[number]	k4	600
stehů	steh	k1gInPc2	steh
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Vyšívací	vyšívací	k2eAgInSc1d1	vyšívací
stroj	stroj	k1gInSc1	stroj
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
Funkce	funkce	k1gFnSc1	funkce
stroje	stroj	k1gInSc2	stroj
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vyšívání	vyšívání	k1gNnSc2	vyšívání
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
principu	princip	k1gInSc6	princip
splétací	splétací	k2eAgInSc1d1	splétací
stroj	stroj	k1gInSc1	stroj
upravený	upravený	k2eAgInSc1d1	upravený
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
splétacím	splétací	k2eAgInSc6d1	splétací
stole	stol	k1gInSc6	stol
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
spojeno	spojit	k5eAaPmNgNnS	spojit
několik	několik	k4yIc1	několik
paličkovacích	paličkovací	k2eAgFnPc2d1	paličkovací
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Paličky	palička	k1gFnPc1	palička
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
pohybovat	pohybovat	k5eAaImF	pohybovat
po	po	k7c6	po
vlastní	vlastní	k2eAgFnSc6d1	vlastní
dráze	dráha	k1gFnSc6	dráha
nebo	nebo	k8xC	nebo
přecházet	přecházet	k5eAaImF	přecházet
na	na	k7c4	na
sousední	sousední	k2eAgNnSc4d1	sousední
a	a	k8xC	a
tak	tak	k6eAd1	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
plošný	plošný	k2eAgInSc1d1	plošný
výrobek	výrobek	k1gInSc1	výrobek
z	z	k7c2	z
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
vzájemně	vzájemně	k6eAd1	vzájemně
seskávaných	seskávaný	k2eAgFnPc2d1	seskávaný
nití	nit	k1gFnPc2	nit
a	a	k8xC	a
různě	různě	k6eAd1	různě
tvarovaných	tvarovaný	k2eAgInPc2d1	tvarovaný
otvorů	otvor	k1gInPc2	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
paliček	palička	k1gFnPc2	palička
je	být	k5eAaImIp3nS	být
řízen	řídit	k5eAaImNgInS	řídit
přes	přes	k7c4	přes
systém	systém	k1gInSc4	systém
magnetů	magnet	k1gInPc2	magnet
elektronickým	elektronický	k2eAgNnSc7d1	elektronické
vzorovacím	vzorovací	k2eAgNnSc7d1	vzorovací
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
složité	složitý	k2eAgInPc4d1	složitý
vzory	vzor	k1gInPc4	vzor
jsou	být	k5eAaImIp3nP	být
stroje	stroj	k1gInSc2	stroj
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
žakárovým	žakárový	k2eAgNnSc7d1	žakárové
ústrojím	ústrojí	k1gNnSc7	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
Výkon	výkon	k1gInSc1	výkon
moderních	moderní	k2eAgInPc2d1	moderní
strojů	stroj	k1gInPc2	stroj
<g/>
:	:	kIx,	:
stuha	stuha	k1gFnSc1	stuha
široká	široký	k2eAgFnSc1d1	široká
40	[number]	k4	40
cm	cm	kA	cm
=	=	kIx~	=
cca	cca	kA	cca
10	[number]	k4	10
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
min	min	kA	min
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
4	[number]	k4	4
cm	cm	kA	cm
š.	š.	k?	š.
=	=	kIx~	=
cca	cca	kA	cca
40	[number]	k4	40
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
min	min	kA	min
<g/>
.	.	kIx.	.
Základní	základní	k2eAgInSc1d1	základní
vazební	vazební	k2eAgInSc1d1	vazební
element	element	k1gInSc1	element
je	být	k5eAaImIp3nS	být
síť	síť	k1gFnSc4	síť
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
variantách	varianta	k1gFnPc6	varianta
(	(	kIx(	(
<g/>
mřížková	mřížkový	k2eAgFnSc1d1	mřížková
<g/>
,	,	kIx,	,
hrášková	hráškový	k2eAgFnSc1d1	hrášková
<g/>
,	,	kIx,	,
bruselský	bruselský	k2eAgInSc1d1	bruselský
tyl	tyl	k1gInSc1	tyl
<g/>
,	,	kIx,	,
torchon	torchon	k1gInSc1	torchon
<g/>
,	,	kIx,	,
hvězdicová	hvězdicový	k2eAgFnSc1d1	hvězdicová
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
se	se	k3xPyFc4	se
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
překříženými	překřížený	k2eAgFnPc7d1	překřížená
nebo	nebo	k8xC	nebo
vzájemně	vzájemně	k6eAd1	vzájemně
seskanými	seskaný	k2eAgFnPc7d1	seskaná
nitěmi	nit	k1gFnPc7	nit
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
tvarech	tvar	k1gInPc6	tvar
a	a	k8xC	a
tloušťkách	tloušťka	k1gFnPc6	tloušťka
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
osnovní	osnovní	k2eAgInSc1d1	osnovní
pletací	pletací	k2eAgInSc1d1	pletací
stroj	stroj	k1gInSc1	stroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
háčkování	háčkování	k1gNnSc4	háčkování
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
vzorování	vzorování	k1gNnSc2	vzorování
<g/>
:	:	kIx,	:
Osnovní	osnovní	k2eAgFnPc1d1	osnovní
niti	nit	k1gFnPc1	nit
tvoří	tvořit	k5eAaImIp3nP	tvořit
řetízkovou	řetízkový	k2eAgFnSc4d1	řetízková
vazbu	vazba	k1gFnSc4	vazba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
provlékají	provlékat	k5eAaImIp3nP	provlékat
vzorovací	vzorovací	k2eAgFnPc1d1	vzorovací
útkové	útkový	k2eAgFnPc1d1	útková
niti	nit	k1gFnPc1	nit
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
speciálním	speciální	k2eAgNnPc3d1	speciální
vzorovacím	vzorovací	k2eAgNnPc3d1	vzorovací
zařízením	zařízení	k1gNnPc3	zařízení
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
ulitové	ulitový	k2eAgFnSc2d1	ulitový
jehly	jehla	k1gFnSc2	jehla
používané	používaný	k2eAgFnSc2d1	používaná
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
ornamentů	ornament	k1gInPc2	ornament
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc1	stroj
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
zejména	zejména	k9	zejména
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
krajkových	krajkový	k2eAgFnPc2d1	krajková
stuh	stuha	k1gFnPc2	stuha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
maloprůměrovém	maloprůměrový	k2eAgInSc6d1	maloprůměrový
okrouhlém	okrouhlý	k2eAgInSc6d1	okrouhlý
stroji	stroj	k1gInSc6	stroj
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vyrábět	vyrábět	k5eAaImF	vyrábět
např.	např.	kA	např.
módní	módní	k2eAgFnSc2d1	módní
krajkové	krajkový	k2eAgFnSc2d1	krajková
punčochy	punčocha	k1gFnSc2	punčocha
a	a	k8xC	a
punčocháče	punčocháče	k1gInPc4	punčocháče
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
snímek	snímek	k1gInSc1	snímek
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Údaje	údaj	k1gInPc1	údaj
k	k	k7c3	k
výrobní	výrobní	k2eAgFnSc3d1	výrobní
technologii	technologie	k1gFnSc3	technologie
nebyly	být	k5eNaImAgFnP	být
dosud	dosud	k6eAd1	dosud
publikovány	publikován	k2eAgInPc1d1	publikován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
