<s>
Jako	jako	k8xS	jako
celuloid	celuloid	k1gInSc1	celuloid
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
skupina	skupina	k1gFnSc1	skupina
termoplastů	termoplast	k1gInPc2	termoplast
připravených	připravený	k2eAgFnPc2d1	připravená
reakcí	reakce	k1gFnPc2	reakce
nitrocelulózy	nitrocelulóza	k1gFnSc2	nitrocelulóza
s	s	k7c7	s
kafrem	kafr	k1gInSc7	kafr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
první	první	k4xOgInSc4	první
termoplast	termoplast	k1gInSc4	termoplast
–	–	k?	–
připraven	připraven	k2eAgMnSc1d1	připraven
byl	být	k5eAaImAgMnS	být
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
tavitelnost	tavitelnost	k1gFnSc4	tavitelnost
a	a	k8xC	a
tvárnost	tvárnost	k1gFnSc4	tvárnost
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
používal	používat	k5eAaImAgInS	používat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
různých	různý	k2eAgInPc2d1	různý
okrasných	okrasný	k2eAgInPc2d1	okrasný
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
především	především	k6eAd1	především
jako	jako	k9	jako
laciná	laciný	k2eAgFnSc1d1	laciná
náhrada	náhrada	k1gFnSc1	náhrada
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
želvoviny	želvovina	k1gFnSc2	želvovina
<g/>
,	,	kIx,	,
ebenového	ebenový	k2eAgNnSc2d1	ebenové
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
perleti	perleť	k1gFnSc2	perleť
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
klasických	klasický	k2eAgInPc2d1	klasický
fotografických	fotografický	k2eAgInPc2d1	fotografický
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
výrobky	výrobek	k1gInPc7	výrobek
jsou	být	k5eAaImIp3nP	být
míčky	míček	k1gInPc4	míček
na	na	k7c4	na
stolní	stolní	k2eAgInSc4d1	stolní
tenis	tenis	k1gInSc4	tenis
a	a	k8xC	a
trsátka	trsátko	k1gNnPc4	trsátko
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
strunné	strunný	k2eAgInPc4d1	strunný
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Celuloid	celuloid	k1gInSc1	celuloid
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
hořlavý	hořlavý	k2eAgInSc1d1	hořlavý
a	a	k8xC	a
časem	časem	k6eAd1	časem
křehne	křehnout	k5eAaImIp3nS	křehnout
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nevyužívá	využívat	k5eNaImIp3nS	využívat
v	v	k7c6	v
tak	tak	k6eAd1	tak
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
jako	jako	k8xS	jako
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Zvětrávání	zvětrávání	k1gNnSc1	zvětrávání
celuloidu	celuloid	k1gInSc2	celuloid
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
problém	problém	k1gInSc1	problém
při	při	k7c6	při
archivaci	archivace	k1gFnSc6	archivace
starých	starý	k2eAgInPc2d1	starý
fotografických	fotografický	k2eAgInPc2d1	fotografický
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
celuloid	celuloid	k1gInSc1	celuloid
připravil	připravit	k5eAaPmAgInS	připravit
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
Alexander	Alexandra	k1gFnPc2	Alexandra
Parkes	Parkesa	k1gFnPc2	Parkesa
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
městě	město	k1gNnSc6	město
Birmingham	Birmingham	k1gInSc1	Birmingham
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc4	ten
však	však	k9	však
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
Parkesin	Parkesin	k1gInSc1	Parkesin
nenašel	najít	k5eNaPmAgInS	najít
komerčně	komerčně	k6eAd1	komerčně
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Celuloid	celuloid	k1gInSc1	celuloid
získal	získat	k5eAaPmAgInS	získat
jako	jako	k9	jako
tuhý	tuhý	k2eAgInSc1d1	tuhý
zbytek	zbytek	k1gInSc1	zbytek
po	po	k7c6	po
odpaření	odpaření	k1gNnSc6	odpaření
rozpouštědla	rozpouštědlo	k1gNnSc2	rozpouštědlo
z	z	k7c2	z
fotografického	fotografický	k2eAgNnSc2d1	fotografické
kolodia	kolodium	k1gNnSc2	kolodium
<g/>
,	,	kIx,	,
produkt	produkt	k1gInSc4	produkt
sám	sám	k3xTgInSc4	sám
popsal	popsat	k5eAaPmAgInS	popsat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
<g/>
,	,	kIx,	,
elastickou	elastický	k2eAgFnSc4d1	elastická
a	a	k8xC	a
vodě	voda	k1gFnSc3	voda
odolnou	odolný	k2eAgFnSc4d1	odolná
látku	látka	k1gFnSc4	látka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Parkes	Parkes	k1gMnSc1	Parkes
patentoval	patentovat	k5eAaBmAgMnS	patentovat
celuloid	celuloid	k1gInSc4	celuloid
jako	jako	k8xC	jako
vodě	voda	k1gFnSc3	voda
odolný	odolný	k2eAgInSc1d1	odolný
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
tkané	tkaný	k2eAgFnPc4d1	tkaná
textilie	textilie	k1gFnPc4	textilie
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
předvedl	předvést	k5eAaPmAgMnS	předvést
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
celuloid	celuloid	k1gInSc4	celuloid
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Parkesin	Parkesin	k1gMnSc1	Parkesin
<g/>
)	)	kIx)	)
odměněn	odměnit	k5eAaPmNgMnS	odměnit
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
medailí	medaile	k1gFnSc7	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Parkesin	Parkesin	k1gMnSc1	Parkesin
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgMnS	připravovat
odpařením	odpaření	k1gNnSc7	odpaření
roztoku	roztok	k1gInSc2	roztok
nitrocelulózy	nitrocelulóza	k1gFnSc2	nitrocelulóza
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrovaný	koncentrovaný	k2eAgInSc1d1	koncentrovaný
roztok	roztok	k1gInSc1	roztok
nitrocelulózy	nitrocelulóza	k1gFnSc2	nitrocelulóza
se	se	k3xPyFc4	se
přiváděl	přivádět	k5eAaImAgInS	přivádět
do	do	k7c2	do
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
rozpouštědla	rozpouštědlo	k1gNnSc2	rozpouštědlo
při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
tlaku	tlak	k1gInSc3	tlak
odpařila	odpařit	k5eAaPmAgFnS	odpařit
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
přidala	přidat	k5eAaPmAgFnS	přidat
barviva	barvivo	k1gNnSc2	barvivo
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pevná	pevný	k2eAgFnSc1d1	pevná
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
tvárná	tvárný	k2eAgFnSc1d1	tvárná
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Parkes	Parkes	k1gMnSc1	Parkes
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
pokusil	pokusit	k5eAaPmAgMnS	pokusit
prorazit	prorazit	k5eAaPmF	prorazit
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
vynálezem	vynález	k1gInSc7	vynález
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
společnost	společnost	k1gFnSc4	společnost
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
prodej	prodej	k1gInSc4	prodej
Parkesinu	Parkesina	k1gFnSc4	Parkesina
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
neuspěla	uspět	k5eNaPmAgFnS	uspět
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
Parkese	Parkese	k1gFnSc2	Parkese
založil	založit	k5eAaPmAgMnS	založit
Angličan	Angličan	k1gMnSc1	Angličan
Daniel	Daniel	k1gMnSc1	Daniel
Spill	Spill	k1gMnSc1	Spill
společnost	společnost	k1gFnSc1	společnost
Xylonite	Xylonit	k1gInSc5	Xylonit
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
a	a	k8xC	a
prodávala	prodávat	k5eAaImAgFnS	prodávat
produkt	produkt	k1gInSc4	produkt
podobný	podobný	k2eAgInSc4d1	podobný
Parkesinu	Parkesin	k2eAgFnSc4d1	Parkesin
<g/>
.	.	kIx.	.
</s>
<s>
Spill	Spill	k1gMnSc1	Spill
neuspěl	uspět	k5eNaPmAgMnS	uspět
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
bankrot	bankrot	k1gInSc1	bankrot
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
založil	založit	k5eAaPmAgMnS	založit
firmu	firma	k1gFnSc4	firma
Daniel	Daniel	k1gMnSc1	Daniel
Spill	Spill	k1gMnSc1	Spill
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
<g/>
;	;	kIx,	;
přel	přít	k5eAaImAgMnS	přít
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
s	s	k7c7	s
bratry	bratr	k1gMnPc7	bratr
Hyattovými	Hyattův	k2eAgMnPc7d1	Hyattův
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc2	jejich
patent	patent	k1gInSc4	patent
na	na	k7c4	na
celuloid	celuloid	k1gInSc4	celuloid
v	v	k7c6	v
letech	let	k1gInPc6	let
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
Američan	Američan	k1gMnSc1	Američan
John	John	k1gMnSc1	John
Wesley	Weslea	k1gFnSc2	Weslea
Hyatt	Hyatt	k1gMnSc1	Hyatt
s	s	k7c7	s
nitrocelulózou	nitrocelulóza	k1gFnSc7	nitrocelulóza
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
nalézt	nalézt	k5eAaPmF	nalézt
materiál	materiál	k1gInSc4	materiál
vhodný	vhodný	k2eAgInSc4d1	vhodný
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
biliardových	biliardův	k2eAgFnPc2d1	biliardův
koulí	koule	k1gFnPc2	koule
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
doposud	doposud	k6eAd1	doposud
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
z	z	k7c2	z
drahé	drahý	k2eAgFnSc2d1	drahá
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
materiál	materiál	k1gInSc1	materiál
použil	použít	k5eAaPmAgInS	použít
plátno	plátno	k1gNnSc4	plátno
<g/>
,	,	kIx,	,
slonovinový	slonovinový	k2eAgInSc4d1	slonovinový
prach	prach	k1gInSc4	prach
<g/>
,	,	kIx,	,
šelak	šelak	k1gInSc4	šelak
a	a	k8xC	a
především	především	k9	především
kolódium	kolódium	k1gNnSc1	kolódium
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
John	John	k1gMnSc1	John
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Isaiah	Isaiaha	k1gFnPc2	Isaiaha
patentovali	patentovat	k5eAaBmAgMnP	patentovat
proces	proces	k1gInSc4	proces
výroby	výroba	k1gFnSc2	výroba
materiálu	materiál	k1gInSc2	materiál
podobajícího	podobající	k2eAgInSc2d1	podobající
se	se	k3xPyFc4	se
slonovině	slonovina	k1gFnSc3	slonovina
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
nitrocelulózy	nitrocelulóza	k1gFnSc2	nitrocelulóza
a	a	k8xC	a
kafru	kafr	k1gInSc2	kafr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
také	také	k9	také
Parkes	Parkes	k1gMnSc1	Parkes
a	a	k8xC	a
Spill	Spill	k1gMnSc1	Spill
uváděli	uvádět	k5eAaImAgMnP	uvádět
jako	jako	k9	jako
přísadu	přísada	k1gFnSc4	přísada
kafr	kafr	k1gInSc4	kafr
při	při	k7c6	při
experimentech	experiment	k1gInPc6	experiment
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
bratři	bratr	k1gMnPc1	bratr
Hyattovi	Hyattův	k2eAgMnPc1d1	Hyattův
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zjistili	zjistit	k5eAaPmAgMnP	zjistit
jeho	jeho	k3xOp3gNnPc4	jeho
přesná	přesný	k2eAgNnPc4d1	přesné
množství	množství	k1gNnPc4	množství
na	na	k7c4	na
plastifikaci	plastifikace	k1gFnSc4	plastifikace
nitrocelulózy	nitrocelulóza	k1gFnSc2	nitrocelulóza
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Celluloid	Celluloid	k1gInSc1	Celluloid
<g/>
"	"	kIx"	"
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
(	(	kIx(	(
<g/>
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
sporech	spor	k1gInPc6	spor
se	s	k7c7	s
Spillem	Spillo	k1gNnSc7	Spillo
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
obchodní	obchodní	k2eAgFnSc1d1	obchodní
známka	známka	k1gFnSc1	známka
společnosti	společnost	k1gFnSc2	společnost
Celluloid	Celluloid	k1gInSc1	Celluloid
Manufacturing	Manufacturing	k1gInSc1	Manufacturing
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sa	sa	k?	sa
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
celuloidu	celuloid	k1gInSc2	celuloid
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
fotografických	fotografický	k2eAgInPc2d1	fotografický
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
Goodwin	Goodwin	k1gInSc1	Goodwin
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
a	a	k8xC	a
Eastman	Eastman	k1gMnSc1	Eastman
Company	Compana	k1gFnSc2	Compana
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
získali	získat	k5eAaPmAgMnP	získat
patent	patent	k1gInSc4	patent
na	na	k7c4	na
film	film	k1gInSc4	film
z	z	k7c2	z
celuloidu	celuloid	k1gInSc2	celuloid
<g/>
.	.	kIx.	.
</s>
<s>
Goodwin	Goodwin	k1gInSc1	Goodwin
a	a	k8xC	a
investoři	investor	k1gMnPc1	investor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nejdříve	dříve	k6eAd3	dříve
své	svůj	k3xOyFgInPc4	svůj
patenty	patent	k1gInPc4	patent
prodali	prodat	k5eAaPmAgMnP	prodat
<g/>
,	,	kIx,	,
vedli	vést	k5eAaImAgMnP	vést
vůči	vůči	k7c3	vůči
společnosti	společnost	k1gFnSc3	společnost
Eastman	Eastman	k1gMnSc1	Eastman
Kodak	Kodak	kA	Kodak
spor	spor	k1gInSc1	spor
o	o	k7c4	o
patent	patent	k1gInSc4	patent
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
celuloidových	celuloidový	k2eAgInPc2d1	celuloidový
filmů	film	k1gInPc2	film
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
fotografický	fotografický	k2eAgInSc4d1	fotografický
průmysl	průmysl	k1gInSc4	průmysl
–	–	k?	–
začala	začít	k5eAaPmAgFnS	začít
výroba	výroba	k1gFnSc1	výroba
kinofilmů	kinofilm	k1gInPc2	kinofilm
(	(	kIx(	(
<g/>
24	[number]	k4	24
×	×	k?	×
36	[number]	k4	36
mm	mm	kA	mm
<g/>
)	)	kIx)	)
a	a	k8xC	a
svitkových	svitkový	k2eAgInPc2d1	svitkový
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
celuloidových	celuloidový	k2eAgInPc2d1	celuloidový
filmů	film	k1gInPc2	film
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
jejich	jejich	k3xOp3gFnSc1	jejich
jediná	jediný	k2eAgFnSc1d1	jediná
konkurence	konkurence	k1gFnSc1	konkurence
–	–	k?	–
drahé	drahý	k2eAgFnPc4d1	drahá
fotografické	fotografický	k2eAgFnPc4d1	fotografická
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jedna	jeden	k4xCgFnSc1	jeden
nevýhoda	nevýhoda	k1gFnSc1	nevýhoda
celuloidu	celuloid	k1gInSc2	celuloid
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hořlavý	hořlavý	k2eAgInSc1d1	hořlavý
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
filmu	film	k1gInSc2	film
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jinými	jiný	k2eAgInPc7d1	jiný
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
hořlavými	hořlavý	k2eAgInPc7d1	hořlavý
plasty	plast	k1gInPc7	plast
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
termoplasty	termoplast	k1gInPc1	termoplast
našly	najít	k5eAaPmAgInP	najít
celuloidy	celuloid	k1gInPc4	celuloid
široké	široký	k2eAgNnSc4d1	široké
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběly	vyrábět	k5eAaImAgInP	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
například	například	k6eAd1	například
hřebeny	hřeben	k1gInPc1	hřeben
<g/>
,	,	kIx,	,
rámečky	rámeček	k1gInPc1	rámeček
brýlí	brýle	k1gFnPc2	brýle
<g/>
,	,	kIx,	,
rukojeti	rukojeť	k1gFnPc1	rukojeť
nožů	nůž	k1gInPc2	nůž
<g/>
,	,	kIx,	,
psací	psací	k2eAgMnPc1d1	psací
pera	pero	k1gNnSc2	pero
<g/>
,	,	kIx,	,
pravítka	pravítko	k1gNnSc2	pravítko
atd.	atd.	kA	atd.
Počátkem	počátkem	k7c2	počátkem
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
celuloid	celuloid	k1gInSc1	celuloid
postupně	postupně	k6eAd1	postupně
nahradil	nahradit	k5eAaPmAgInS	nahradit
různými	různý	k2eAgInPc7d1	různý
acetáty	acetát	k1gInPc7	acetát
celulózy	celulóza	k1gFnSc2	celulóza
a	a	k8xC	a
polyesterem	polyester	k1gInSc7	polyester
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1	relativní
permitivita	permitivita	k1gFnSc1	permitivita
ε	ε	k?	ε
celuloidu	celuloid	k1gInSc2	celuloid
je	být	k5eAaImIp3nS	být
3,5	[number]	k4	3,5
až	až	k9	až
6,2	[number]	k4	6,2
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
celuloid	celuloid	k1gInSc1	celuloid
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
dílů	díl	k1gInPc2	díl
nitrocelulózy	nitrocelulóza	k1gFnSc2	nitrocelulóza
(	(	kIx(	(
<g/>
s	s	k7c7	s
11	[number]	k4	11
<g/>
%	%	kIx~	%
obsahem	obsah	k1gInSc7	obsah
dusíku	dusík	k1gInSc2	dusík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
30	[number]	k4	30
dílů	díl	k1gInPc2	díl
kafru	kafr	k1gInSc2	kafr
<g/>
,	,	kIx,	,
0	[number]	k4	0
až	až	k9	až
14	[number]	k4	14
dílů	díl	k1gInPc2	díl
barviva	barvivo	k1gNnSc2	barvivo
<g/>
,	,	kIx,	,
1	[number]	k4	1
až	až	k9	až
5	[number]	k4	5
dílů	díl	k1gInPc2	díl
etanolu	etanol	k1gInSc2	etanol
a	a	k8xC	a
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgInPc2d1	různý
stabilizátorů	stabilizátor	k1gInPc2	stabilizátor
a	a	k8xC	a
přísad	přísada	k1gFnPc2	přísada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
činí	činit	k5eAaImIp3nP	činit
celuloid	celuloid	k1gInSc4	celuloid
trvanlivějším	trvanlivý	k2eAgInSc7d2	trvanlivější
a	a	k8xC	a
méně	málo	k6eAd2	málo
hořlavým	hořlavý	k2eAgNnSc7d1	hořlavé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Celuloid	celuloid	k1gInSc1	celuloid
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
