<s>
Německé	německý	k2eAgFnPc1d1
volby	volba	k1gFnPc1
do	do	k7c2
říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
1912	#num#	k4
</s>
<s>
Výsledek	výsledek	k1gInSc1
voleb	volba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barvy	barva	k1gFnPc1
odpovídají	odpovídat	k5eAaImIp3nP
barvám	barva	k1gFnPc3
v	v	k7c6
tabulce	tabulka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Německé	německý	k2eAgFnSc2d1
federální	federální	k2eAgFnSc2d1
volby	volba	k1gFnSc2
1912	#num#	k4
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1912	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
to	ten	k3xDgNnSc1
poslední	poslední	k2eAgFnPc1d1
volby	volba	k1gFnPc1
do	do	k7c2
Reichstagu	Reichstag	k1gInSc2
před	před	k7c7
1	#num#	k4
<g/>
.	.	kIx.
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
a	a	k8xC
zároveň	zároveň	k6eAd1
poslední	poslední	k2eAgFnPc4d1
volby	volba	k1gFnPc4
v	v	k7c6
německé	německý	k2eAgFnSc6d1
říši	říš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Volební	volební	k2eAgFnSc1d1
účast	účast	k1gFnSc1
byla	být	k5eAaImAgFnS
kolem	kolem	k7c2
85	#num#	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
stejně	stejně	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
v	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1907	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SPD	SPD	kA
byl	být	k5eAaImAgMnS
jasný	jasný	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
voleb	volba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdržel	obdržet	k5eAaPmAgMnS
4,25	4,25	k4
milionu	milion	k4xCgInSc2
hlasů	hlas	k1gInPc2
(	(	kIx(
<g/>
34,8	34,8	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Druhou	druhý	k4xOgFnSc7
nejpočetnější	početní	k2eAgFnSc7d3
skupinou	skupina	k1gFnSc7
bylo	být	k5eAaImAgNnS
centrum	centrum	k1gNnSc1
s	s	k7c7
91	#num#	k4
poslanci	poslanec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Vláda	vláda	k1gFnSc1
kancléře	kancléř	k1gMnSc2
Theobald	Theobald	k1gInSc1
von	von	k1gInSc4
Bethmann-Hollwega	Bethmann-Hollwega	k1gFnSc1
ztratila	ztratit	k5eAaPmAgFnS
hodně	hodně	k6eAd1
hlasů	hlas	k1gInPc2
a	a	k8xC
křesel	křeslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
sloučení	sloučení	k1gNnSc3
několika	několik	k4yIc2
levicových	levicový	k2eAgFnPc2d1
liberálních	liberální	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
nově	nově	k6eAd1
založena	založen	k2eAgFnSc1d1
strana	strana	k1gFnSc1
se	se	k3xPyFc4
nazvala	nazvat	k5eAaPmAgFnS,k5eAaBmAgFnS
Pokroková	pokrokový	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Volební	volební	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
</s>
<s>
Politický	politický	k2eAgInSc1d1
směr	směr	k1gInSc1
</s>
<s>
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
</s>
<s>
Hlasy	hlas	k1gInPc1
</s>
<s>
Mandáty	mandát	k1gInPc1
v	v	k7c6
Reichstagu	Reichstag	k1gInSc6
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
Procenta	procento	k1gNnPc1
</s>
<s>
volby	volba	k1gFnPc1
1907	#num#	k4
</s>
<s>
Počet	počet	k1gInSc1
</s>
<s>
Podíl	podíl	k1gInSc1
</s>
<s>
volby	volba	k1gFnPc1
1907	#num#	k4
</s>
<s>
Konzervatismus	konzervatismus	k1gInSc1
</s>
<s>
Německá	německý	k2eAgFnSc1d1
konzervativní	konzervativní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
DKP	DKP	kA
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
042	#num#	k4
000	#num#	k4
</s>
<s>
8,5	8,5	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
%	%	kIx~
</s>
<s>
43	#num#	k4
</s>
<s>
10,8	10,8	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
</s>
<s>
Svobodná	svobodný	k2eAgFnSc1d1
konzervativní	konzervativní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
DRP	DRP	kA
<g/>
)	)	kIx)
</s>
<s>
367	#num#	k4
000	#num#	k4
</s>
<s>
3,0	3,0	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
%	%	kIx~
</s>
<s>
14	#num#	k4
</s>
<s>
3,5	3,5	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
</s>
<s>
Liberalismus	liberalismus	k1gInSc1
</s>
<s>
Pravicový	pravicový	k2eAgInSc1d1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
liberální	liberální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
NLP	NLP	kA
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
663	#num#	k4
000	#num#	k4
</s>
<s>
13,6	13,6	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
%	%	kIx~
</s>
<s>
45	#num#	k4
</s>
<s>
11,3	11,3	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
</s>
<s>
Levicový	levicový	k2eAgInSc1d1
</s>
<s>
Pokroková	pokrokový	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
FVP	FVP	kA
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
497	#num#	k4
000	#num#	k4
</s>
<s>
12,3	12,3	k4
%	%	kIx~
</s>
<s>
▲	▲	k?
+1,3	+1,3	k4
%	%	kIx~
</s>
<s>
42	#num#	k4
</s>
<s>
10,6	10,6	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
</s>
<s>
katolicismus	katolicismus	k1gInSc1
</s>
<s>
Centrum	centrum	k1gNnSc1
</s>
<s>
1	#num#	k4
997	#num#	k4
000	#num#	k4
</s>
<s>
16,4	16,4	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
%	%	kIx~
</s>
<s>
91	#num#	k4
</s>
<s>
22,9	22,9	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
</s>
<s>
Socialismus	socialismus	k1gInSc1
</s>
<s>
Sociálnědemokratická	sociálnědemokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Německa	Německo	k1gNnSc2
(	(	kIx(
<g/>
SPD	SPD	kA
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
250	#num#	k4
000	#num#	k4
</s>
<s>
34,8	34,8	k4
%	%	kIx~
</s>
<s>
▲	▲	k?
+5,9	+5,9	k4
%	%	kIx~
</s>
<s>
110	#num#	k4
</s>
<s>
27,7	27,7	k4
%	%	kIx~
</s>
<s>
▲	▲	k?
+67	+67	k4
</s>
<s>
Národnostní	národnostní	k2eAgFnPc1d1
menšinyRegionální	menšinyRegionální	k2eAgFnPc1d1
strany	strana	k1gFnPc1
</s>
<s>
Poláci	Polák	k1gMnPc1
</s>
<s>
442	#num#	k4
000	#num#	k4
</s>
<s>
3,6	3,6	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
%	%	kIx~
</s>
<s>
18	#num#	k4
</s>
<s>
4,5	4,5	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
</s>
<s>
Alsasko-Lotrinsko	Alsasko-Lotrinsko	k6eAd1
</s>
<s>
162	#num#	k4
000	#num#	k4
</s>
<s>
1,3	1,3	k4
%	%	kIx~
</s>
<s>
▲	▲	k?
+0,4	+0,4	k4
%	%	kIx~
</s>
<s>
9	#num#	k4
</s>
<s>
2,3	2,3	k4
%	%	kIx~
</s>
<s>
▲	▲	k?
<g/>
+	+	kIx~
<g/>
2	#num#	k4
</s>
<s>
Německo-hannoverská	německo-hannoverský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
DHP	DHP	kA
<g/>
)	)	kIx)
</s>
<s>
85	#num#	k4
000	#num#	k4
</s>
<s>
0,7	0,7	k4
%	%	kIx~
</s>
<s>
▬	▬	k?
±	±	k?
<g/>
0,0	0,0	k4
%	%	kIx~
</s>
<s>
5	#num#	k4
</s>
<s>
1,3	1,3	k4
%	%	kIx~
</s>
<s>
▲	▲	k?
<g/>
+	+	kIx~
<g/>
4	#num#	k4
</s>
<s>
Dánové	Dán	k1gMnPc1
</s>
<s>
17	#num#	k4
000	#num#	k4
</s>
<s>
0,1	0,1	k4
%	%	kIx~
</s>
<s>
▬	▬	k?
±	±	k?
<g/>
0,0	0,0	k4
%	%	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
0,3	0,3	k4
%	%	kIx~
</s>
<s>
▬	▬	k?
±	±	k?
<g/>
0	#num#	k4
</s>
<s>
Další	další	k2eAgFnSc1d1
</s>
<s>
Rolnická	rolnický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
234	#num#	k4
000	#num#	k4
</s>
<s>
1,9	1,9	k4
%	%	kIx~
</s>
<s>
▲	▲	k?
+0,2	+0,2	k4
%	%	kIx~
</s>
<s>
7	#num#	k4
</s>
<s>
1,8	1,8	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
</s>
<s>
Antisemitská	antisemitský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
300	#num#	k4
000	#num#	k4
</s>
<s>
2,5	2,5	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
%	%	kIx~
</s>
<s>
10	#num#	k4
</s>
<s>
2,5	2,5	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
</s>
<s>
Další	další	k2eAgFnSc1d1
</s>
<s>
152	#num#	k4
000	#num#	k4
</s>
<s>
1,2	1,2	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
%	%	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
0,5	0,5	k4
%	%	kIx~
</s>
<s>
▼	▼	k?
−	−	k?
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
</s>
<s>
12	#num#	k4
208	#num#	k4
000	#num#	k4
</s>
<s>
100,0	100,0	k4
%	%	kIx~
</s>
<s>
397	#num#	k4
</s>
<s>
100,0	100,0	k4
%	%	kIx~
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Volby	volba	k1gFnPc1
z	z	k7c2
roku	rok	k1gInSc2
1912	#num#	k4
s	s	k7c7
grafikou	grafika	k1gFnSc7
</s>
<s>
Volby	volba	k1gFnPc1
v	v	k7c6
Německu	Německo	k1gNnSc6
do	do	k7c2
roku	rok	k1gInSc2
1912	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Volby	volba	k1gFnPc1
v	v	k7c6
Německu	Německo	k1gNnSc6
Federální	federální	k2eAgFnSc2d1
volby	volba	k1gFnSc2
</s>
<s>
1871	#num#	k4
•	•	k?
1874	#num#	k4
•	•	k?
1877	#num#	k4
•	•	k?
1878	#num#	k4
•	•	k?
1881	#num#	k4
•	•	k?
1884	#num#	k4
•	•	k?
1887	#num#	k4
•	•	k?
1890	#num#	k4
•	•	k?
1893	#num#	k4
•	•	k?
1898	#num#	k4
<g/>
•	•	k?
1903	#num#	k4
•	•	k?
1907	#num#	k4
•	•	k?
1912	#num#	k4
•	•	k?
1919	#num#	k4
•	•	k?
1920	#num#	k4
•	•	k?
1924	#num#	k4
(	(	kIx(
<g/>
květen	květen	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
1924	#num#	k4
(	(	kIx(
<g/>
prosinec	prosinec	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
1928	#num#	k4
•	•	k?
1930	#num#	k4
•	•	k?
1932	#num#	k4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
červenec	červenec	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
1932	#num#	k4
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
1933	#num#	k4
(	(	kIx(
<g/>
březen	březen	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
1933	#num#	k4
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
1936	#num#	k4
•	•	k?
1938	#num#	k4
•	•	k?
1949	#num#	k4
•	•	k?
1953	#num#	k4
•	•	k?
1957	#num#	k4
•	•	k?
1961	#num#	k4
•	•	k?
1965	#num#	k4
•	•	k?
1969	#num#	k4
•	•	k?
1972	#num#	k4
•	•	k?
1976	#num#	k4
•	•	k?
1980	#num#	k4
•	•	k?
1983	#num#	k4
•	•	k?
1987	#num#	k4
•	•	k?
1990	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1998	#num#	k4
•	•	k?
2002	#num#	k4
•	•	k?
2005	#num#	k4
•	•	k?
2009	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2017	#num#	k4
Prezidentské	prezidentský	k2eAgFnPc1d1
volby	volba	k1gFnPc1
</s>
<s>
1925	#num#	k4
•	•	k?
1932	#num#	k4
Volby	volba	k1gFnSc2
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
</s>
<s>
1979	#num#	k4
•	•	k?
1984	#num#	k4
•	•	k?
1989	#num#	k4
•	•	k?
1994	#num#	k4
•	•	k?
1999	#num#	k4
•	•	k?
2004	#num#	k4
•	•	k?
2009	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2019	#num#	k4
Referenda	referendum	k1gNnSc2
</s>
<s>
1933	#num#	k4
•	•	k?
1934	#num#	k4
•	•	k?
1936	#num#	k4
•	•	k?
1938	#num#	k4
</s>
