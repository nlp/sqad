<s>
Auckland	Auckland	k1gInSc1	Auckland
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
s	s	k7c7	s
1	[number]	k4	1
618	[number]	k4	618
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
Severním	severní	k2eAgNnSc6d1	severní
ostrově	ostrov	k1gInSc6	ostrov
mezi	mezi	k7c4	mezi
Manakau	Manakaa	k1gFnSc4	Manakaa
Harbour	Harboura	k1gFnPc2	Harboura
a	a	k8xC	a
zálivem	záliv	k1gInSc7	záliv
Hauraki	Hauraki	k1gNnSc2	Hauraki
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
samotné	samotný	k2eAgNnSc1d1	samotné
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
z	z	k7c2	z
vyhaslých	vyhaslý	k2eAgFnPc2d1	vyhaslá
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zřetelně	zřetelně	k6eAd1	zřetelně
vyčnívají	vyčnívat	k5eAaImIp3nP	vyčnívat
z	z	k7c2	z
reliéfu	reliéf	k1gInSc2	reliéf
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známá	k1gFnPc1	známá
také	také	k9	také
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Město	město	k1gNnSc1	město
Plachet	plachta	k1gFnPc2	plachta
podle	podle	k7c2	podle
přístavů	přístav	k1gInPc2	přístav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
kotví	kotvit	k5eAaImIp3nS	kotvit
nespočet	nespočet	k1gInSc1	nespočet
plachetnic	plachetnice	k1gFnPc2	plachetnice
a	a	k8xC	a
jachet	jachta	k1gFnPc2	jachta
patřící	patřící	k2eAgFnSc1d1	patřící
místním	místní	k2eAgMnPc3d1	místní
obyvatelům	obyvatel	k1gMnPc3	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
je	být	k5eAaImIp3nS	být
Sky	Sky	k1gMnSc1	Sky
Tower	Tower	k1gMnSc1	Tower
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
největší	veliký	k2eAgNnSc4d3	veliký
a	a	k8xC	a
nejrušnější	rušný	k2eAgNnSc4d3	nejrušnější
letiště	letiště	k1gNnSc4	letiště
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Auckland	Auckland	k1gInSc1	Auckland
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
založen	založit	k5eAaPmNgInS	založit
britskými	britský	k2eAgMnPc7d1	britský
kolonizátory	kolonizátor	k1gMnPc7	kolonizátor
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1840	[number]	k4	1840
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
získané	získaný	k2eAgFnSc6d1	získaná
od	od	k7c2	od
domorodého	domorodý	k2eAgInSc2d1	domorodý
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Auckland	Aucklanda	k1gFnPc2	Aucklanda
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
George	Georg	k1gMnSc2	Georg
Edena	Eden	k1gMnSc2	Eden
<g/>
,	,	kIx,	,
prvního	první	k4xOgMnSc4	první
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Aucklandu	Auckland	k1gInSc2	Auckland
(	(	kIx(	(
<g/>
1784	[number]	k4	1784
<g/>
-	-	kIx~	-
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Auckland	Auckland	k1gInSc1	Auckland
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
oficiálně	oficiálně	k6eAd1	oficiálně
fungoval	fungovat	k5eAaImAgInS	fungovat
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tento	tento	k3xDgInSc4	tento
status	status	k1gInSc4	status
převzal	převzít	k5eAaPmAgMnS	převzít
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
a	a	k8xC	a
strategickému	strategický	k2eAgInSc3d1	strategický
přístavu	přístav	k1gInSc3	přístav
Wellington	Wellington	k1gInSc4	Wellington
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
aucklandském	aucklandský	k2eAgInSc6d1	aucklandský
přístavu	přístav	k1gInSc6	přístav
potopena	potopen	k2eAgFnSc1d1	potopena
vlajková	vlajkový	k2eAgFnSc1d1	vlajková
loď	loď	k1gFnSc1	loď
organizace	organizace	k1gFnSc2	organizace
Greenpeace	Greenpeace	k1gFnSc1	Greenpeace
Rainbow	Rainbow	k1gMnSc1	Rainbow
Warrior	Warrior	k1gMnSc1	Warrior
<g/>
.	.	kIx.	.
</s>
<s>
Teroristický	teroristický	k2eAgInSc1d1	teroristický
útok	útok	k1gInSc1	útok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
skandál	skandál	k1gInSc1	skandál
<g/>
,	,	kIx,	,
spáchali	spáchat	k5eAaPmAgMnP	spáchat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
výbušnin	výbušnina	k1gFnPc2	výbušnina
agenti	agent	k1gMnPc1	agent
francouzské	francouzský	k2eAgFnSc2d1	francouzská
tajné	tajný	k2eAgFnSc2d1	tajná
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
znemožnili	znemožnit	k5eAaPmAgMnP	znemožnit
plánované	plánovaný	k2eAgInPc4d1	plánovaný
protesty	protest	k1gInPc4	protest
proti	proti	k7c3	proti
francouzským	francouzský	k2eAgFnPc3d1	francouzská
zkouškám	zkouška	k1gFnPc3	zkouška
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
potopené	potopený	k2eAgFnSc6d1	potopená
lodi	loď	k1gFnSc6	loď
zahynul	zahynout	k5eAaPmAgMnS	zahynout
člen	člen	k1gMnSc1	člen
posádky	posádka	k1gFnSc2	posádka
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
Fernando	Fernanda	k1gFnSc5	Fernanda
Pereira	Pereir	k1gInSc2	Pereir
<g/>
.	.	kIx.	.
</s>
<s>
Rainbow	Rainbow	k?	Rainbow
Warrior	Warrior	k1gInSc1	Warrior
byl	být	k5eAaImAgInS	být
vyzdvižen	vyzdvihnout	k5eAaPmNgInS	vyzdvihnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
oprava	oprava	k1gFnSc1	oprava
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
nerentabilní	rentabilní	k2eNgInPc4d1	nerentabilní
-	-	kIx~	-
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
potopen	potopit	k5eAaPmNgInS	potopit
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1987	[number]	k4	1987
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Matauri	Mataur	k1gFnSc2	Mataur
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
umělým	umělý	k2eAgInSc7d1	umělý
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
útes	útes	k1gInSc4	útes
a	a	k8xC	a
"	"	kIx"	"
<g/>
rybí	rybí	k2eAgInSc1d1	rybí
svatostánek	svatostánek	k1gInSc1	svatostánek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
