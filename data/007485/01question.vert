<s>
Jak	jak	k8xC	jak
starý	starý	k1gMnSc1	starý
je	být	k5eAaImIp3nS	být
rukou	ruka	k1gFnSc7	ruka
opracovaný	opracovaný	k2eAgInSc1d1	opracovaný
kámen	kámen	k1gInSc1	kámen
nalezený	nalezený	k2eAgInSc1d1	nalezený
na	na	k7c6	na
Červeném	červený	k2eAgInSc6d1	červený
kopci	kopec	k1gInSc6	kopec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
dokladem	doklad	k1gInSc7	doklad
lidského	lidský	k2eAgNnSc2d1	lidské
osídlení	osídlení	k1gNnSc2	osídlení
území	území	k1gNnSc2	území
Brna	Brno	k1gNnSc2	Brno
<g/>
?	?	kIx.	?
</s>
