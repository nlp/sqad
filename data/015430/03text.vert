<s>
Děkanát	děkanát	k1gInSc4
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
Děkanát	děkanát	k1gInSc1
Uherské	uherský	k2eAgFnSc2d1
HradištěDiecéze	HradištěDiecéza	k1gFnSc3
</s>
<s>
Arcidiecéze	arcidiecéze	k1gFnSc1
olomoucká	olomoucký	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1
Děkan	děkan	k1gMnSc1
</s>
<s>
R.	R.	kA
D.	D.	kA
Josef	Josef	k1gMnSc1
Říha	Říha	k1gMnSc1
Další	další	k2eAgInSc1d1
úřad	úřad	k1gInSc1
děkana	děkan	k1gMnSc2
</s>
<s>
farář	farář	k1gMnSc1
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgFnSc2d1
k	k	k7c3
prosinci	prosinec	k1gInSc3
2019	#num#	k4
</s>
<s>
Děkanát	děkanát	k1gInSc4
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
je	být	k5eAaImIp3nS
územní	územní	k2eAgFnSc1d1
část	část	k1gFnSc1
Olomoucké	olomoucký	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ho	on	k3xPp3gNnSc2
25	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgFnPc6,k3yIgFnPc6,k3yQgFnPc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
58	#num#	k4
kostelů	kostel	k1gInPc2
a	a	k8xC
kaplí	kaple	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děkanem	děkan	k1gMnSc7
je	být	k5eAaImIp3nS
R.	R.	kA
D.	D.	kA
Josef	Josef	k1gMnSc1
Říha	Říha	k1gMnSc1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místoděkanem	Místoděkan	k1gInSc7
je	být	k5eAaImIp3nS
R.	R.	kA
D.	D.	kA
Karel	Karel	k1gMnSc1
Šenk	Šenk	k1gMnSc1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
v	v	k7c6
Uherském	uherský	k2eAgInSc6d1
Ostrohu	Ostroh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
děkanátu	děkanát	k1gInSc6
působí	působit	k5eAaImIp3nS
25	#num#	k4
diecézních	diecézní	k2eAgMnPc2d1
a	a	k8xC
10	#num#	k4
řeholních	řeholní	k2eAgMnPc2d1
kněží	kněz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
děkanátu	děkanát	k1gInSc2
je	být	k5eAaImIp3nS
530	#num#	k4
km²	km²	k?
a	a	k8xC
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
území	území	k1gNnSc6
žije	žít	k5eAaImIp3nS
92	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
;	;	kIx,
ke	k	k7c3
katolictví	katolictví	k1gNnSc3
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nS
52	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
56,52	56,52	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
11	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
21,15	21,15	k4
%	%	kIx~
<g/>
)	)	kIx)
z	z	k7c2
nich	on	k3xPp3gFnPc2
se	se	k3xPyFc4
pravidelně	pravidelně	k6eAd1
účastní	účastnit	k5eAaImIp3nS
nedělních	nedělní	k2eAgFnPc2d1
mší	mše	k1gFnPc2
svatých	svatá	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Existence	existence	k1gFnSc1
hradišťského	hradišťský	k2eAgInSc2d1
děkanátu	děkanát	k1gInSc2
je	být	k5eAaImIp3nS
doložena	doložit	k5eAaPmNgFnS
už	už	k9
k	k	k7c3
roku	rok	k1gInSc3
1601	#num#	k4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
tehdejší	tehdejší	k2eAgInSc1d1
rozsah	rozsah	k1gInSc1
ovšem	ovšem	k9
není	být	k5eNaImIp3nS
známý	známý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1644	#num#	k4
se	se	k3xPyFc4
území	území	k1gNnSc4
děkanátu	děkanát	k1gInSc2
přibližně	přibližně	k6eAd1
shodovalo	shodovat	k5eAaImAgNnS
s	s	k7c7
územím	území	k1gNnSc7
tehdejšího	tehdejší	k2eAgInSc2d1
hradišťského	hradišťský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
z	z	k7c2
něj	on	k3xPp3gInSc2
byly	být	k5eAaImAgFnP
vyčleňovány	vyčleňován	k2eAgInPc1d1
nově	nově	k6eAd1
zakládané	zakládaný	k2eAgInPc1d1
děkanáty	děkanát	k1gInPc1
<g/>
:	:	kIx,
1658	#num#	k4
uherskobrodský	uherskobrodský	k2eAgInSc1d1
<g/>
,	,	kIx,
1665	#num#	k4
strážnický	strážnický	k2eAgMnSc1d1
a	a	k8xC
1730	#num#	k4
bzenecký	bzenecký	k2eAgInSc4d1
(	(	kIx(
<g/>
ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
vyvinul	vyvinout	k5eAaPmAgInS
v	v	k7c4
děkanát	děkanát	k1gInSc4
kyjovský	kyjovský	k2eAgInSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úřad	úřad	k1gInSc1
hradišťského	hradišťský	k2eAgMnSc2d1
děkana	děkan	k1gMnSc2
zastával	zastávat	k5eAaImAgMnS
v	v	k7c4
období	období	k1gNnSc4
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1601	#num#	k4
a	a	k8xC
1813	#num#	k4
vždy	vždy	k6eAd1
uherskohradišťský	uherskohradišťský	k2eAgMnSc1d1
farář	farář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1813	#num#	k4
se	se	k3xPyFc4
děkanem	děkan	k1gMnSc7
stal	stát	k5eAaPmAgMnS
hlucký	hlucký	k2eAgMnSc1d1
farář	farář	k1gMnSc1
Rafael	Rafael	k1gMnSc1
Tučapský	Tučapský	k2eAgMnSc1d1
a	a	k8xC
po	po	k7c6
něm	on	k3xPp3gMnSc6
už	už	k6eAd1
nikdy	nikdy	k6eAd1
tyto	tento	k3xDgFnPc4
dvě	dva	k4xCgFnPc4
funkce	funkce	k1gFnPc4
nebyly	být	k5eNaImAgFnP
pevně	pevně	k6eAd1
svázány	svázán	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnPc1
zmínky	zmínka	k1gFnPc1
o	o	k7c6
viceděkanech	viceděkan	k1gInPc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1778	#num#	k4
náleží	náležet	k5eAaImIp3nS
hradišťský	hradišťský	k2eAgInSc4d1
děkanát	děkanát	k1gInSc4
pod	pod	k7c4
kroměřížské	kroměřížský	k2eAgNnSc4d1
arcikněžství	arcikněžství	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
tehdy	tehdy	k6eAd1
bylo	být	k5eAaImAgNnS
ustaveno	ustaven	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Farnosti	farnost	k1gFnPc1
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
1691	#num#	k4
zahrnoval	zahrnovat	k5eAaImAgInS
děkanát	děkanát	k1gInSc1
tyto	tento	k3xDgFnPc1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Derfle	Derfle	k1gInSc1
<g/>
,	,	kIx,
Uherský	uherský	k2eAgInSc1d1
Ostroh	Ostroh	k1gInSc1
<g/>
,	,	kIx,
Hluk	hluk	k1gInSc1
<g/>
,	,	kIx,
Bílovice	Bílovice	k1gInPc1
<g/>
,	,	kIx,
Kostelec	Kostelec	k1gInSc1
<g/>
,	,	kIx,
Bohuslavice	Bohuslavice	k1gFnPc1
<g/>
,	,	kIx,
Spytihněv	Spytihněv	k1gFnPc1
<g/>
,	,	kIx,
Napajedla	Napajedla	k1gNnPc1
<g/>
,	,	kIx,
Blatnice	Blatnice	k1gFnPc1
<g/>
,	,	kIx,
Bzenec	Bzenec	k1gInSc1
<g/>
,	,	kIx,
Osvětimany	Osvětiman	k1gInPc1
<g/>
,	,	kIx,
Ježov	Ježov	k1gInSc1
<g/>
,	,	kIx,
Střílky	Střílek	k1gMnPc4
<g/>
,	,	kIx,
Koryčany	Koryčan	k1gMnPc4
<g/>
,	,	kIx,
Buchlovice	Buchlovice	k1gFnPc4
<g/>
,	,	kIx,
Vracov	Vracov	k1gInSc1
<g/>
;	;	kIx,
řád	řád	k1gInSc1
cisterciáků	cisterciák	k1gMnPc2
spravoval	spravovat	k5eAaImAgInS
ještě	ještě	k6eAd1
farnosti	farnost	k1gFnSc2
na	na	k7c6
Velehradě	Velehrad	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
Boršicích	Boršice	k1gFnPc6
a	a	k8xC
v	v	k7c6
Polešovicích	Polešovice	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1745	#num#	k4
děkanát	děkanát	k1gInSc1
tvořily	tvořit	k5eAaImAgFnP
tyto	tento	k3xDgFnPc1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Bílovice	Bílovice	k1gInPc1
<g/>
,	,	kIx,
Buchlovice	Buchlovice	k1gFnPc1
<g/>
,	,	kIx,
Derfla	Derfla	k1gFnPc1
<g/>
,	,	kIx,
Hluk	hluk	k1gInSc1
<g/>
,	,	kIx,
Napajedla	Napajedla	k1gNnPc1
<g/>
,	,	kIx,
Uherský	uherský	k2eAgInSc1d1
Ostroh	Ostroh	k1gInSc1
<g/>
;	;	kIx,
navíc	navíc	k6eAd1
opět	opět	k5eAaPmF
některé	některý	k3yIgFnPc4
své	svůj	k3xOyFgFnPc4
farnosti	farnost	k1gFnPc4
spravovali	spravovat	k5eAaImAgMnP
cisterciáci	cisterciák	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1751	#num#	k4
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
první	první	k4xOgFnSc1
lokální	lokální	k2eAgFnSc4d1
kaplanství	kaplanství	k1gNnSc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
Ostrožská	ostrožský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
(	(	kIx(
<g/>
farnost	farnost	k1gFnSc4
Uherský	uherský	k2eAgInSc4d1
Ostroh	Ostroh	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
Dolní	dolní	k2eAgNnSc1d1
Němčí	němčit	k5eAaImIp3nS
(	(	kIx(
<g/>
farnost	farnost	k1gFnSc1
Hluk	hluk	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1753	#num#	k4
byla	být	k5eAaImAgFnS
přenesena	přenést	k5eAaPmNgFnS
farnost	farnost	k1gFnSc1
z	z	k7c2
Derfle	Derfle	k1gFnSc2
do	do	k7c2
Kunovic	Kunovice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
1778	#num#	k4
tvořily	tvořit	k5eAaImAgFnP
děkanát	děkanát	k1gInSc1
tyto	tento	k3xDgFnPc1
farnosti	farnost	k1gFnPc1
<g/>
:	:	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Bílovice	Bílovice	k1gInPc1
<g/>
,	,	kIx,
Boršice	Boršice	k1gInPc1
<g/>
,	,	kIx,
Buchlovice	Buchlovice	k1gFnPc1
<g/>
,	,	kIx,
Hluk	hluk	k1gInSc1
<g/>
,	,	kIx,
Jalubí	Jalubí	k1gNnPc1
<g/>
,	,	kIx,
Kunovice	Kunovice	k1gFnPc1
<g/>
,	,	kIx,
Napajedla	Napajedla	k1gNnPc1
<g/>
,	,	kIx,
Uherský	uherský	k2eAgInSc1d1
Ostroh	Ostroh	k1gInSc1
<g/>
;	;	kIx,
řeholní	řeholní	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
Polešovice	Polešovice	k1gFnSc1
<g/>
,	,	kIx,
Spytihněv	Spytihněv	k1gFnSc1
<g/>
,	,	kIx,
Velehrad	Velehrad	k1gInSc1
<g/>
;	;	kIx,
lokální	lokální	k2eAgNnSc1d1
kaplanství	kaplanství	k1gNnSc1
Dolní	dolní	k2eAgNnSc1d1
Němčí	němčit	k5eAaImIp3nS
<g/>
,	,	kIx,
Derfle	Derfle	k1gFnSc1
a	a	k8xC
Ostrožská	ostrožský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1781	#num#	k4
vzniklo	vzniknout	k5eAaPmAgNnS
i	i	k9
lokální	lokální	k2eAgNnSc1d1
kaplanství	kaplanství	k1gNnSc1
Boršice	Boršice	k1gFnSc2
u	u	k7c2
Blatnice	Blatnice	k1gFnPc4
(	(	kIx(
<g/>
z	z	k7c2
farnosti	farnost	k1gFnSc2
Hluk	hluk	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
formace	formace	k1gFnSc2
brněnské	brněnský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1787	#num#	k4
tvořily	tvořit	k5eAaImAgFnP
uherskohradišťský	uherskohradišťský	k2eAgInSc4d1
děkanát	děkanát	k1gInSc4
tyto	tento	k3xDgInPc4
duchovní	duchovní	k1gMnSc1
správy	správa	k1gFnSc2
<g/>
:	:	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Halenkovice	Halenkovice	k1gFnPc1
<g/>
,	,	kIx,
Bílovice	Bílovice	k1gInPc1
<g/>
,	,	kIx,
Buchlovice	Buchlovice	k1gFnPc1
<g/>
,	,	kIx,
Hluk	hluk	k1gInSc1
<g/>
,	,	kIx,
Jalubí	Jalubí	k1gNnPc1
<g/>
,	,	kIx,
Kunovice	Kunovice	k1gFnPc1
<g/>
,	,	kIx,
Uherský	uherský	k2eAgInSc1d1
Ostroh	Ostroh	k1gInSc1
<g/>
,	,	kIx,
Velehrad	Velehrad	k1gInSc1
<g/>
,	,	kIx,
Březolupy	Březolup	k1gInPc1
<g/>
,	,	kIx,
Sady	sad	k1gInPc1
(	(	kIx(
<g/>
Derfle	Derfle	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ostrožská	ostrožský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
složení	složení	k1gNnSc1
platilo	platit	k5eAaImAgNnS
až	až	k9
na	na	k7c4
chybějící	chybějící	k2eAgFnPc4d1
Halenkovice	Halenkovice	k1gFnPc4
i	i	k9
v	v	k7c6
roce	rok	k1gInSc6
1813	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
děkanem	děkan	k1gMnSc7
hlucký	hlucký	k2eAgMnSc1d1
farář	farář	k1gMnSc1
Rafael	Rafael	k1gMnSc1
Tučapský	Tučapský	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vznikly	vzniknout	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc4
nové	nový	k2eAgFnPc4d1
farnosti	farnost	k1gFnPc4
–	–	k?
Jankovice	Jankovice	k1gFnSc1
a	a	k8xC
Stupava	Stupava	k1gFnSc1
–	–	k?
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
pak	pak	k6eAd1
další	další	k2eAgFnPc4d1
čtyři	čtyři	k4xCgFnPc4
–	–	k?
Huštěnovice	Huštěnovice	k1gFnPc4
<g/>
,	,	kIx,
Kostelany	Kostelana	k1gFnPc4
<g/>
,	,	kIx,
Popovice	Popovice	k1gFnPc4
a	a	k8xC
Stříbrnice	Stříbrnice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Znak	znak	k1gInSc1
děkanátu	děkanát	k1gInSc2
</s>
<s>
znak	znak	k1gInSc1
děkanátu	děkanát	k1gInSc2
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
V	v	k7c6
modrém	modrý	k2eAgNnSc6d1
poli	pole	k1gNnSc6
stříbrné	stříbrný	k2eAgFnSc2d1
desky	deska	k1gFnSc2
Desatera	desatero	k1gNnSc2
s	s	k7c7
černým	černý	k2eAgInSc7d1
církevněslovanským	církevněslovanský	k2eAgInSc7d1
nápisem	nápis	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
deskách	deska	k1gFnPc6
vzpřímeně	vzpřímeně	k6eAd1
zlatý	zlatý	k2eAgInSc4d1
arcibiskupský	arcibiskupský	k2eAgInSc4d1
procesní	procesní	k2eAgInSc4d1
dvojitý	dvojitý	k2eAgInSc4d1
kříž	kříž	k1gInSc4
<g/>
,	,	kIx,
doprovázený	doprovázený	k2eAgInSc1d1
v	v	k7c6
hlavě	hlava	k1gFnSc6
štítu	štít	k1gInSc2
dvěma	dva	k4xCgFnPc7
zlatými	zlatý	k2eAgFnPc7d1
liliemi	lilie	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Území	území	k1gNnSc1
uherskohradišťského	uherskohradišťský	k2eAgInSc2d1
děkanátu	děkanát	k1gInSc2
je	být	k5eAaImIp3nS
spojeno	spojit	k5eAaPmNgNnS
s	s	k7c7
působením	působení	k1gNnSc7
věrozvěstů	věrozvěst	k1gMnPc2
sv.	sv.	kA
Cyrila	Cyril	k1gMnSc4
a	a	k8xC
Metoděje	Metoděj	k1gMnSc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
atributem	atribut	k1gInSc7
jsou	být	k5eAaImIp3nP
desky	deska	k1gFnPc4
desatera	desatero	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgFnPc6,k3yRgFnPc6,k3yIgFnPc6
jsou	být	k5eAaImIp3nP
jednotlivá	jednotlivý	k2eAgNnPc1d1
přikázání	přikázání	k1gNnPc1
reprezentována	reprezentován	k2eAgNnPc1d1
jednotlivými	jednotlivý	k2eAgNnPc7d1
písmeny	písmeno	k1gNnPc7
abecedy	abeceda	k1gFnSc2
hlaholice	hlaholice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojramenný	dvojramenný	k2eAgInSc1d1
(	(	kIx(
<g/>
patriarchální	patriarchální	k2eAgInSc1d1
<g/>
)	)	kIx)
kříž	kříž	k1gInSc1
je	být	k5eAaImIp3nS
dalším	další	k2eAgInSc7d1
odkazem	odkaz	k1gInSc7
na	na	k7c4
jejich	jejich	k3xOp3gNnSc4
působení	působení	k1gNnSc4
v	v	k7c6
době	doba	k1gFnSc6
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lilie	lilie	k1gFnSc1
zase	zase	k9
značí	značit	k5eAaImIp3nS
příslušnost	příslušnost	k1gFnSc4
velehradského	velehradský	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
k	k	k7c3
řádů	řád	k1gInPc2
cisterciáků	cisterciák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Farnosti	farnost	k1gFnPc1
</s>
<s>
Farnosti	farnost	k1gFnPc1
děkanátu	děkanát	k1gInSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
FarnostSprávceSídloFarní	FarnostSprávceSídloFarní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
</s>
<s>
Babice	babice	k1gFnSc1
u	u	k7c2
Uherského	uherský	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
–	–	k?
Spytihněv	Spytihněv	k1gMnSc1
</s>
<s>
Babice	babice	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Cyril	Cyril	k1gMnSc1
a	a	k8xC
Metoděj	Metoděj	k1gMnSc1
</s>
<s>
Bílovice	Bílovice	k1gInPc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Jan	Jan	k1gMnSc1
Liška	Liška	k1gMnSc1
</s>
<s>
Bílovice	Bílovice	k1gInPc1
</s>
<s>
Narození	narození	k1gNnSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
</s>
<s>
Boršice	Boršice	k1gFnSc1
u	u	k7c2
Blatnice	Blatnice	k1gFnPc5
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Norbert	Norbert	k1gMnSc1
Nawrath	Nawrath	k1gMnSc1
</s>
<s>
Boršice	Boršice	k1gFnSc1
u	u	k7c2
Blatnice	Blatnice	k1gFnPc4
</s>
<s>
sv.	sv.	kA
Kateřina	Kateřina	k1gFnSc1
Alexandrijská	alexandrijský	k2eAgFnSc1d1
</s>
<s>
Boršice	Boršice	k1gFnSc1
u	u	k7c2
Buchlovic	Buchlovice	k1gFnPc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Anton	Anton	k1gMnSc1
Kasan	Kasan	k1gMnSc1
</s>
<s>
Boršice	Boršice	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Václav	Václav	k1gMnSc1
</s>
<s>
Březolupy	Březolupa	k1gFnPc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
–	–	k?
Bílovice	Bílovice	k1gInPc1
</s>
<s>
Březolupy	Březolupa	k1gFnPc1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Buchlovice	Buchlovice	k1gFnPc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Rudolf	Rudolf	k1gMnSc1
Chmelař	Chmelař	k1gMnSc1
</s>
<s>
Buchlovice	Buchlovice	k1gFnPc1
</s>
<s>
sv.	sv.	kA
Martin	Martin	k1gMnSc1
</s>
<s>
Hluk	hluk	k1gInSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Josef	Josef	k1gMnSc1
Janek	Janek	k1gMnSc1
</s>
<s>
Hluk	hluk	k1gInSc1
</s>
<s>
sv.	sv.	kA
Vavřinec	Vavřinec	k1gMnSc1
</s>
<s>
Huštěnovice	Huštěnovice	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
–	–	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
u	u	k7c2
Uherského	uherský	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
</s>
<s>
Huštěnovice	Huštěnovice	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Anna	Anna	k1gFnSc1
</s>
<s>
Jalubí	Jalubit	k5eAaPmIp3nS
</s>
<s>
administrátor	administrátor	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Pavel	Pavel	k1gMnSc1
Jagoš	Jagoš	k1gMnSc1
</s>
<s>
Jalubí	Jalubit	k5eAaPmIp3nS
</s>
<s>
sv.	sv.	kA
Jan	Jan	k1gMnSc1
Křtitel	křtitel	k1gMnSc1
</s>
<s>
Jankovice	Jankovice	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
–	–	k?
Jalubí	Jalubí	k1gNnSc2
</s>
<s>
Jankovice	Jankovice	k1gFnSc1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Kostelany	Kostelana	k1gFnPc1
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
–	–	k?
Nedakonice	Nedakonice	k1gFnSc1
</s>
<s>
Kostelany	Kostelana	k1gFnPc1
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
</s>
<s>
sv.	sv.	kA
Florián	Florián	k1gMnSc1
</s>
<s>
Kunovice	Kunovice	k1gFnPc4
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Jaroslav	Jaroslav	k1gMnSc1
Polách	Polách	k1gMnSc1
</s>
<s>
Kunovice	Kunovice	k1gFnPc4
</s>
<s>
sv.	sv.	kA
Petr	Petr	k1gMnSc1
a	a	k8xC
Pavel	Pavel	k1gMnSc1
</s>
<s>
Nedakonice	Nedakonice	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
R.	R.	kA
D.	D.	kA
ICLic	ICLic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mgr.	Mgr.	kA
Josef	Josef	k1gMnSc1
Rýznar	Rýznar	k1gMnSc1
</s>
<s>
Nedakonice	Nedakonice	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Florián	Florián	k1gMnSc1
</s>
<s>
Ostrožská	ostrožský	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Miroslav	Miroslav	k1gMnSc1
Reif	Reif	k1gMnSc1
</s>
<s>
Ostrožská	ostrožský	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Jakub	Jakub	k1gMnSc1
Starší	starší	k1gMnSc1
</s>
<s>
Ostrožská	ostrožský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
</s>
<s>
farář	farář	k1gMnSc1
P.	P.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Žaluda	Žalud	k1gMnSc2
<g/>
,	,	kIx,
SDB	SDB	kA
</s>
<s>
Ostrožská	ostrožský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Václav	Václav	k1gMnSc1
</s>
<s>
Osvětimany	Osvětiman	k1gInPc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
–	–	k?
Boršice	Boršice	k1gFnPc4
u	u	k7c2
Buchlovic	Buchlovice	k1gFnPc2
</s>
<s>
Osvětimany	Osvětiman	k1gInPc1
</s>
<s>
sv.	sv.	kA
Havel	Havel	k1gMnSc1
</s>
<s>
Polešovice	Polešovice	k1gFnSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
ThLic	ThLic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jozef	Jozef	k1gMnSc1
Červeň	červeň	k1gFnSc4
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
</s>
<s>
Polešovice	Polešovice	k1gFnSc1
</s>
<s>
sv.	sv.	kA
Petr	Petr	k1gMnSc1
a	a	k8xC
Pavel	Pavel	k1gMnSc1
</s>
<s>
Popovice	Popovice	k1gFnPc1
u	u	k7c2
Uherského	uherský	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
–	–	k?
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
–	–	k?
Sady	sada	k1gFnSc2
</s>
<s>
Popovice	Popovice	k1gFnPc1
</s>
<s>
Růžencová	růžencový	k2eAgFnSc1d1
Panna	Panna	k1gFnSc1
Maria	Maria	k1gFnSc1
</s>
<s>
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
u	u	k7c2
Uherského	uherský	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Miroslav	Miroslav	k1gMnSc1
Suchomel	Suchomel	k1gMnSc1
</s>
<s>
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
</s>
<s>
sv.	sv.	kA
Michael	Michael	k1gMnSc1
archanděl	archanděl	k1gMnSc1
</s>
<s>
Stříbrnice	Stříbrnice	k1gFnPc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
–	–	k?
Boršice	Boršice	k1gFnPc4
u	u	k7c2
Buchlovic	Buchlovice	k1gFnPc2
</s>
<s>
Stříbrnice	Stříbrnice	k1gFnPc1
</s>
<s>
sv.	sv.	kA
Prokop	Prokop	k1gMnSc1
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Josef	Josef	k1gMnSc1
Říha	Říha	k1gMnSc1
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
sv.	sv.	kA
František	František	k1gMnSc1
Xaverský	xaverský	k2eAgMnSc1d1
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
–	–	k?
Sady	sada	k1gFnSc2
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Mgr.	Mgr.	kA
Lubomír	Lubomír	k1gMnSc1
Vaďura	Vaďur	k1gMnSc2
</s>
<s>
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
–	–	k?
Sady	sada	k1gFnSc2
</s>
<s>
Narození	narození	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Uherský	uherský	k2eAgInSc1d1
Ostroh	Ostroh	k1gInSc1
</s>
<s>
farář	farář	k1gMnSc1
R.	R.	kA
D.	D.	kA
Karel	Karel	k1gMnSc1
Šenk	Šenk	k1gMnSc1
</s>
<s>
Uherský	uherský	k2eAgInSc1d1
Ostroh	Ostroh	k1gInSc1
</s>
<s>
sv.	sv.	kA
Ondřej	Ondřej	k1gMnSc1
</s>
<s>
Velehrad	Velehrad	k1gInSc1
</s>
<s>
farář	farář	k1gMnSc1
P.	P.	kA
Petr	Petr	k1gMnSc1
Přádka	Přádek	k1gMnSc2
SJ	SJ	kA
</s>
<s>
Velehrad	Velehrad	k1gInSc1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
a	a	k8xC
sv.	sv.	kA
Cyril	Cyril	k1gMnSc1
a	a	k8xC
Metoděj	Metoděj	k1gMnSc1
</s>
<s>
Zlechov	Zlechov	k1gInSc1
</s>
<s>
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
–	–	k?
Buchlovice	Buchlovice	k1gFnPc4
</s>
<s>
Zlechov	Zlechov	k1gInSc1
</s>
<s>
sv.	sv.	kA
Anna	Anna	k1gFnSc1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
bl.	bl.	k?
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Děkanát	děkanát	k1gInSc4
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
|	|	kIx~
Arcidiecéze	arcidiecéze	k1gFnSc1
olomoucká	olomoucký	k2eAgFnSc1d1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
TEŤHAL	TEŤHAL	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc4d1
století	století	k1gNnSc1
uherskohradišťské	uherskohradišťský	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
(	(	kIx(
<g/>
1678	#num#	k4
<g/>
–	–	k?
<g/>
1778	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
FROLEC	FROLEC	kA
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovácko	Slovácko	k1gNnSc1
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
:	:	kIx,
Slovácké	slovácký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86185	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročník	ročník	k1gInSc1
XLV	XLV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradišťský	hradišťský	k2eAgInSc1d1
děkanát	děkanát	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
217	#num#	k4
<g/>
–	–	k?
<g/>
232	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
TEŤHAL	TEŤHAL	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Farnost	farnost	k1gFnSc1
sv.	sv.	kA
Františka	František	k1gMnSc2
Xaverského	xaverský	k2eAgMnSc2d1
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
v	v	k7c6
letech	let	k1gInPc6
1778	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
FROLEC	FROLEC	kA
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovácko	Slovácko	k1gNnSc1
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
:	:	kIx,
Slovácké	slovácký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86185	#num#	k4
<g/>
-	-	kIx~
<g/>
41	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročník	ročník	k1gInSc1
XLVI	XLVI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherskohradišťský	uherskohradišťský	k2eAgInSc1d1
děkanát	děkanát	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
209	#num#	k4
<g/>
–	–	k?
<g/>
232	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
KREJSA	Krejsa	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katalog	katalog	k1gInSc1
olomoucké	olomoucký	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Arcibiskupství	arcibiskupství	k1gNnSc1
olomoucké	olomoucký	k2eAgNnSc1d1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Děkanát	děkanát	k1gInSc4
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
466	#num#	k4
<g/>
-	-	kIx~
<g/>
491	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Seznam	seznam	k1gInSc1
farností	farnost	k1gFnPc2
|	|	kIx~
Arcidiecéze	arcidiecéze	k1gFnSc1
olomoucká	olomoucký	k2eAgFnSc1d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
rodinu	rodina	k1gFnSc4
děkanátu	děkanát	k1gInSc2
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
Mládež	mládež	k1gFnSc4
děkanátu	děkanát	k1gInSc2
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
uherskohradišťských	uherskohradišťský	k2eAgMnPc2d1
děkanů	děkan	k1gMnPc2
Archivováno	archivován	k2eAgNnSc1d1
13	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Seznam	seznam	k1gInSc1
uherskohradišťských	uherskohradišťský	k2eAgInPc2d1
víceděkanů	víceděkan	k1gInPc2
Archivováno	archivován	k2eAgNnSc1d1
13	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Archivováno	archivován	k2eAgNnSc4d1
13	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Farnosti	farnost	k1gFnSc2
děkanátu	děkanát	k1gInSc2
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
</s>
<s>
Babice	babice	k1gFnSc1
u	u	k7c2
Uherského	uherský	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
•	•	k?
Bílovice	Bílovice	k1gInPc1
•	•	k?
Boršice	Boršic	k1gMnSc4
u	u	k7c2
Blatnice	Blatnice	k1gFnPc4
•	•	k?
Boršice	Boršice	k1gFnPc4
u	u	k7c2
Buchlovic	Buchlovice	k1gFnPc2
•	•	k?
Březolupy	Březolupa	k1gFnPc4
•	•	k?
Buchlovice	Buchlovice	k1gFnPc4
•	•	k?
Hluk	hluk	k1gInSc1
•	•	k?
Huštěnovice	Huštěnovice	k1gFnSc1
•	•	k?
Jalubí	Jalubí	k1gNnPc2
•	•	k?
Jankovice	Jankovice	k1gFnSc2
•	•	k?
Kostelany	Kostelana	k1gFnSc2
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
•	•	k?
Kunovice	Kunovice	k1gFnPc4
•	•	k?
Nedakonice	Nedakonice	k1gFnSc2
•	•	k?
Ostrožská	ostrožský	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Ostrožská	ostrožský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Osvětimany	Osvětiman	k1gInPc1
•	•	k?
Polešovice	Polešovice	k1gFnSc2
•	•	k?
Popovice	Popovice	k1gFnPc1
u	u	k7c2
Uherského	uherský	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
•	•	k?
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
u	u	k7c2
Uherského	uherský	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
•	•	k?
Stříbrnice	Stříbrnice	k1gFnPc4
•	•	k?
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
–	–	k?
Sady	sada	k1gFnSc2
•	•	k?
Uherský	uherský	k2eAgInSc1d1
Ostroh	Ostroh	k1gInSc1
•	•	k?
Velehrad	Velehrad	k1gInSc1
(	(	kIx(
<g/>
duchovní	duchovní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
<g/>
:	:	kIx,
Stojanov	Stojanov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Zlechov	Zlechov	k1gInSc1
</s>
<s>
Děkanáty	děkanát	k1gInPc1
Arcidiecéze	arcidiecéze	k1gFnSc2
olomoucké	olomoucký	k2eAgFnSc2d1
</s>
<s>
Holešov	Holešov	k1gInSc1
•	•	k?
Hranice	hranice	k1gFnSc2
•	•	k?
Konice	Konice	k1gFnSc2
•	•	k?
Kroměříž	Kroměříž	k1gFnSc1
•	•	k?
Kyjov	Kyjov	k1gInSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Prostějov	Prostějov	k1gInSc1
•	•	k?
Přerov	Přerov	k1gInSc1
•	•	k?
Svitavy	Svitava	k1gFnSc2
•	•	k?
Šternberk	Šternberk	k1gInSc1
•	•	k?
Šumperk	Šumperk	k1gInSc1
•	•	k?
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
•	•	k?
Valašské	valašský	k2eAgInPc1d1
Klobouky	Klobouky	k1gInPc1
•	•	k?
Valašské	valašský	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
•	•	k?
Veselí	veselí	k1gNnSc1
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
•	•	k?
Vizovice	Vizovice	k1gFnPc4
•	•	k?
Vsetín	Vsetín	k1gInSc1
•	•	k?
Vyškov	Vyškov	k1gInSc1
•	•	k?
Zábřeh	Zábřeh	k1gInSc1
•	•	k?
Zlín	Zlín	k1gInSc1
</s>
