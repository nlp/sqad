<p>
<s>
Chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
atomů	atom	k1gInPc2	atom
jednoho	jeden	k4xCgMnSc2	jeden
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
atomů	atom	k1gInPc2	atom
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Počet	počet	k1gInSc1	počet
prvků	prvek	k1gInPc2	prvek
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
94	[number]	k4	94
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
prvky	prvek	k1gInPc1	prvek
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
uměle	uměle	k6eAd1	uměle
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
systematicky	systematicky	k6eAd1	systematicky
uspořádat	uspořádat	k5eAaPmF	uspořádat
do	do	k7c2	do
periodické	periodický	k2eAgFnSc2d1	periodická
soustavy	soustava	k1gFnSc2	soustava
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
známe	znát	k5eAaImIp1nP	znát
celkem	celkem	k6eAd1	celkem
118	[number]	k4	118
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Protonové	protonový	k2eAgNnSc1d1	protonové
číslo	číslo	k1gNnSc1	číslo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Určující	určující	k2eAgFnSc7d1	určující
vlastností	vlastnost	k1gFnSc7	vlastnost
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
protonové	protonový	k2eAgNnSc1d1	protonové
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
Z	z	k7c2	z
<g/>
)	)	kIx)	)
–	–	k?	–
počet	počet	k1gInSc1	počet
protonů	proton	k1gInPc2	proton
(	(	kIx(	(
<g/>
kladných	kladný	k2eAgInPc2d1	kladný
nábojů	náboj	k1gInPc2	náboj
<g/>
)	)	kIx)	)
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
s	s	k7c7	s
1	[number]	k4	1
protonem	proton	k1gInSc7	proton
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
vodík	vodík	k1gInSc1	vodík
<g/>
,	,	kIx,	,
prvek	prvek	k1gInSc1	prvek
s	s	k7c7	s
94	[number]	k4	94
protony	proton	k1gInPc7	proton
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
plutonium	plutonium	k1gNnSc1	plutonium
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc4d1	ostatní
prvky	prvek	k1gInPc4	prvek
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
mají	mít	k5eAaImIp3nP	mít
počet	počet	k1gInSc4	počet
protonů	proton	k1gInPc2	proton
mezi	mezi	k7c7	mezi
1	[number]	k4	1
a	a	k8xC	a
94	[number]	k4	94
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
prvky	prvek	k1gInPc1	prvek
s	s	k7c7	s
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
větším	veliký	k2eAgNnSc7d2	veliký
než	než	k8xS	než
94	[number]	k4	94
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
prvky	prvek	k1gInPc1	prvek
s	s	k7c7	s
nižším	nízký	k2eAgNnSc7d2	nižší
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
jsou	být	k5eAaImIp3nP	být
nestabilní	stabilní	k2eNgMnPc1d1	nestabilní
a	a	k8xC	a
samovolně	samovolně	k6eAd1	samovolně
se	se	k3xPyFc4	se
rozpadají	rozpadat	k5eAaPmIp3nP	rozpadat
(	(	kIx(	(
<g/>
přirozená	přirozený	k2eAgFnSc1d1	přirozená
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgInPc1d1	český
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
České	český	k2eAgInPc1d1	český
názvy	název	k1gInPc1	název
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
přejaty	přejat	k2eAgInPc1d1	přejat
z	z	k7c2	z
latinských	latinský	k2eAgInPc2d1	latinský
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Karel	Karel	k1gMnSc1	Karel
Slavoj	Slavoj	k1gMnSc1	Slavoj
Amerling	Amerling	k1gInSc1	Amerling
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Presl	Presl	k1gInSc4	Presl
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
však	však	k9	však
neujaly	ujmout	k5eNaPmAgFnP	ujmout
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
solík	solík	k1gMnSc1	solík
<g/>
,	,	kIx,	,
barvík	barvík	k1gMnSc1	barvík
<g/>
,	,	kIx,	,
ďasík	ďasík	k1gMnSc1	ďasík
<g/>
,	,	kIx,	,
kostík	kostík	k1gInSc1	kostík
či	či	k8xC	či
voník	voník	k1gInSc1	voník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Důležité	důležitý	k2eAgFnPc4d1	důležitá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
prvků	prvek	k1gInPc2	prvek
==	==	k?	==
</s>
</p>
<p>
<s>
Důležitými	důležitý	k2eAgFnPc7d1	důležitá
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
prvků	prvek	k1gInPc2	prvek
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
relativní	relativní	k2eAgFnSc1d1	relativní
atomová	atomový	k2eAgFnSc1d1	atomová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
molární	molární	k2eAgFnSc1d1	molární
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
hustota	hustota	k1gFnSc1	hustota
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
elektronová	elektronový	k2eAgFnSc1d1	elektronová
konfigurace	konfigurace	k1gFnSc1	konfigurace
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
elektronegativita	elektronegativita	k1gFnSc1	elektronegativita
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
běžná	běžný	k2eAgNnPc1d1	běžné
oxidační	oxidační	k2eAgNnPc1d1	oxidační
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
teplotní	teplotní	k2eAgInSc1d1	teplotní
součinitel	součinitel	k1gInSc1	součinitel
délkové	délkový	k2eAgFnSc2d1	délková
roztažnosti	roztažnost	k1gFnSc2	roztažnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
měrná	měrný	k2eAgFnSc1d1	měrná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
kapacita	kapacita	k1gFnSc1	kapacita
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
měrné	měrný	k2eAgNnSc1d1	měrné
skupenské	skupenský	k2eAgNnSc1d1	skupenské
teplo	teplo	k1gNnSc1	teplo
tání	tání	k1gNnSc2	tání
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
měrné	měrný	k2eAgNnSc1d1	měrné
skupenské	skupenský	k2eAgNnSc1d1	skupenské
teplo	teplo	k1gNnSc1	teplo
varu	var	k1gInSc2	var
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
poločas	poločas	k1gInSc1	poločas
rozpadu	rozpad	k1gInSc2	rozpad
(	(	kIx(	(
<g/>
u	u	k7c2	u
radioaktivních	radioaktivní	k2eAgInPc2d1	radioaktivní
prvků	prvek	k1gInPc2	prvek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
pojmy	pojem	k1gInPc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Prvek	prvek	k1gInSc1	prvek
–	–	k?	–
Prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
složená	složený	k2eAgFnSc1d1	složená
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
shodné	shodný	k2eAgNnSc4d1	shodné
protonové	protonový	k2eAgNnSc4d1	protonové
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jejich	jejich	k3xOp3gNnPc1	jejich
nukleonová	nukleonový	k2eAgNnPc1d1	nukleonový
čísla	číslo	k1gNnPc1	číslo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
různá	různý	k2eAgNnPc1d1	různé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nuklid	Nuklid	k1gInSc1	Nuklid
–	–	k?	–
Jako	jako	k8xC	jako
nuklid	nuklid	k1gInSc1	nuklid
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
neutrálních	neutrální	k2eAgInPc2d1	neutrální
atomů	atom	k1gInPc2	atom
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
všechny	všechen	k3xTgInPc1	všechen
atomy	atom	k1gInPc1	atom
mají	mít	k5eAaImIp3nP	mít
shodné	shodný	k2eAgNnSc4d1	shodné
protonové	protonový	k2eAgNnSc4d1	protonové
číslo	číslo	k1gNnSc4	číslo
i	i	k8xC	i
nukleonové	nukleonový	k2eAgNnSc4d1	nukleonový
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Radionuklidy	radionuklid	k1gInPc1	radionuklid
–	–	k?	–
Jako	jako	k8xC	jako
radionuklidy	radionuklid	k1gInPc1	radionuklid
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
takové	takový	k3xDgFnPc1	takový
nuklidy	nuklida	k1gFnPc1	nuklida
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
jádra	jádro	k1gNnPc1	jádro
podléhají	podléhat	k5eAaImIp3nP	podléhat
samovolné	samovolný	k2eAgFnSc3d1	samovolná
radioaktivní	radioaktivní	k2eAgFnSc3d1	radioaktivní
přeměně	přeměna	k1gFnSc3	přeměna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izotopy	izotop	k1gInPc1	izotop
–	–	k?	–
Jako	jako	k8xC	jako
izotopy	izotop	k1gInPc1	izotop
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
nuklidy	nuklid	k1gInPc1	nuklid
stejného	stejný	k2eAgInSc2d1	stejný
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgNnSc4d1	stejné
protonové	protonový	k2eAgNnSc4d1	protonové
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odlišné	odlišný	k2eAgNnSc1d1	odlišné
nukleonové	nukleonový	k2eAgNnSc1d1	nukleonový
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
počtem	počet	k1gInSc7	počet
neutronů	neutron	k1gInPc2	neutron
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izobary	izobara	k1gFnPc1	izobara
–	–	k?	–
Jako	jako	k8xS	jako
izobary	izobara	k1gFnPc1	izobara
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
nuklidy	nuklid	k1gInPc1	nuklid
různých	různý	k2eAgInPc2d1	různý
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
shodné	shodný	k2eAgNnSc4d1	shodné
nukleonové	nukleonový	k2eAgNnSc4d1	nukleonový
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
(	(	kIx(	(
<g/>
samozřejmě	samozřejmě	k6eAd1	samozřejmě
<g/>
)	)	kIx)	)
odlišné	odlišný	k2eAgNnSc1d1	odlišné
protonové	protonový	k2eAgNnSc1d1	protonové
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izotony	Izoton	k1gInPc1	Izoton
–	–	k?	–
Izotony	Izoton	k1gInPc1	Izoton
jsou	být	k5eAaImIp3nP	být
nuklidy	nuklida	k1gFnPc4	nuklida
různých	různý	k2eAgInPc2d1	různý
prvků	prvek	k1gInPc2	prvek
se	s	k7c7	s
stejným	stejný	k2eAgNnSc7d1	stejné
neutronovým	neutronový	k2eAgNnSc7d1	neutronové
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Izotony	Izoton	k1gInPc1	Izoton
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
nukleonovém	nukleonový	k2eAgNnSc6d1	nukleonový
čísle	číslo	k1gNnSc6	číslo
i	i	k8xC	i
protonovém	protonový	k2eAgNnSc6d1	protonové
čísle	číslo	k1gNnSc6	číslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Izomery	izomer	k1gInPc1	izomer
–	–	k?	–
Izomery	izomer	k1gInPc1	izomer
jsou	být	k5eAaImIp3nP	být
atomy	atom	k1gInPc1	atom
stejného	stejný	k2eAgInSc2d1	stejný
nuklidu	nuklid	k1gInSc2	nuklid
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
jádra	jádro	k1gNnPc1	jádro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
odlišných	odlišný	k2eAgInPc6d1	odlišný
energetických	energetický	k2eAgInPc6d1	energetický
stavech	stav	k1gInPc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
všech	všecek	k3xTgInPc2	všecek
objevených	objevený	k2eAgInPc2d1	objevený
prvků	prvek	k1gInPc2	prvek
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
jen	jen	k9	jen
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
unií	unie	k1gFnSc7	unie
pro	pro	k7c4	pro
čistou	čistý	k2eAgFnSc4d1	čistá
a	a	k8xC	a
užitou	užitý	k2eAgFnSc4d1	užitá
chemii	chemie	k1gFnSc4	chemie
(	(	kIx(	(
<g/>
IUPAC	IUPAC	kA	IUPAC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
*	*	kIx~	*
<g/>
)	)	kIx)	)
hodnota	hodnota	k1gFnSc1	hodnota
platí	platit	k5eAaImIp3nS	platit
jen	jen	k9	jen
pro	pro	k7c4	pro
nejběžnější	běžný	k2eAgInSc4d3	nejběžnější
izotop	izotop	k1gInSc4	izotop
(	(	kIx(	(
<g/>
izotop	izotop	k1gInSc4	izotop
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
**	**	k?	**
<g/>
)	)	kIx)	)
oficiální	oficiální	k2eAgInSc1d1	oficiální
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
dosud	dosud	k6eAd1	dosud
neschválen	schválen	k2eNgInSc1d1	neschválen
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
</s>
</p>
<p>
<s>
Symbol	symbol	k1gInSc1	symbol
prvku	prvek	k1gInSc2	prvek
</s>
</p>
<p>
<s>
Sloučenina	sloučenina	k1gFnSc1	sloučenina
</s>
</p>
<p>
<s>
Směs	směs	k1gFnSc1	směs
</s>
</p>
<p>
<s>
Ion	ion	k1gInSc1	ion
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
