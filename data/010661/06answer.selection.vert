<s>
Tyrolské	tyrolský	k2eAgFnPc4d1	tyrolská
elegie	elegie	k1gFnPc4	elegie
je	být	k5eAaImIp3nS	být
báseň	báseň	k1gFnSc4	báseň
Karla	Karel	k1gMnSc4	Karel
Havlíčka	Havlíček	k1gMnSc4	Havlíček
Borovského	Borovský	k1gMnSc4	Borovský
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
internace	internace	k1gFnSc2	internace
v	v	k7c6	v
Brixenu	Brixen	k1gInSc6	Brixen
<g/>
.	.	kIx.	.
</s>
