<p>
<s>
Tyrolské	tyrolský	k2eAgFnPc4d1	tyrolská
elegie	elegie	k1gFnPc4	elegie
je	být	k5eAaImIp3nS	být
báseň	báseň	k1gFnSc4	báseň
Karla	Karel	k1gMnSc4	Karel
Havlíčka	Havlíček	k1gMnSc4	Havlíček
Borovského	Borovský	k1gMnSc4	Borovský
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
internace	internace	k1gFnSc2	internace
v	v	k7c6	v
Brixenu	Brixen	k1gInSc6	Brixen
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ostrou	ostrý	k2eAgFnSc4d1	ostrá
<g/>
,	,	kIx,	,
útočnou	útočný	k2eAgFnSc4d1	útočná
satiru	satira	k1gFnSc4	satira
na	na	k7c4	na
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
státní	státní	k2eAgFnSc4d1	státní
policii	policie	k1gFnSc4	policie
<g/>
.	.	kIx.	.
</s>
<s>
Rukopis	rukopis	k1gInSc1	rukopis
byl	být	k5eAaImAgInS	být
dokončen	dokončen	k2eAgInSc1d1	dokončen
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
časopisecky	časopisecky	k6eAd1	časopisecky
pak	pak	k6eAd1	pak
dílo	dílo	k1gNnSc1	dílo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
9	[number]	k4	9
zpěvů	zpěv	k1gInPc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
Havlíčkově	Havlíčkův	k2eAgNnSc6d1	Havlíčkovo
zatčení	zatčení	k1gNnSc6	zatčení
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
následné	následný	k2eAgFnSc2d1	následná
deportaci	deportace	k1gFnSc3	deportace
do	do	k7c2	do
Brixenu	Brixen	k1gInSc2	Brixen
<g/>
.	.	kIx.	.
</s>
<s>
Promlouvá	promlouvat	k5eAaImIp3nS	promlouvat
k	k	k7c3	k
měsíčku	měsíček	k1gInSc3	měsíček
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
vlastně	vlastně	k9	vlastně
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
jeho	jeho	k3xOp3gInPc4	jeho
vlastní	vlastní	k2eAgInPc4d1	vlastní
pocity	pocit	k1gInPc4	pocit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
osamocen	osamotit	k5eAaPmNgMnS	osamotit
<g/>
,	,	kIx,	,
odříznut	odříznout	k5eAaPmNgMnS	odříznout
od	od	k7c2	od
domova	domov	k1gInSc2	domov
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
chce	chtít	k5eAaImIp3nS	chtít
Čechům	Čech	k1gMnPc3	Čech
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
a	a	k8xC	a
proč	proč	k6eAd1	proč
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
zesměšňuje	zesměšňovat	k5eAaImIp3nS	zesměšňovat
zaostalý	zaostalý	k2eAgInSc1d1	zaostalý
státní	státní	k2eAgInSc1d1	státní
aparát	aparát	k1gInSc1	aparát
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
svazující	svazující	k2eAgFnSc4d1	svazující
nadvládu	nadvláda	k1gFnSc4	nadvláda
cizího	cizí	k2eAgInSc2d1	cizí
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
navíc	navíc	k6eAd1	navíc
vládne	vládnout	k5eAaImIp3nS	vládnout
despotický	despotický	k2eAgMnSc1d1	despotický
Bach	Bach	k1gMnSc1	Bach
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vládou	vláda	k1gFnSc7	vláda
teroru	teror	k1gInSc2	teror
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
drží	držet	k5eAaImIp3nP	držet
zkrátka	zkrátka	k6eAd1	zkrátka
ostatní	ostatní	k2eAgInPc1d1	ostatní
národy	národ	k1gInPc1	národ
v	v	k7c6	v
Rakouské	rakouský	k2eAgFnSc6d1	rakouská
monarchii	monarchie	k1gFnSc6	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
i	i	k9	i
církev	církev	k1gFnSc1	církev
za	za	k7c4	za
její	její	k3xOp3gFnSc4	její
zaostalost	zaostalost	k1gFnSc4	zaostalost
a	a	k8xC	a
pomáhání	pomáhání	k1gNnSc4	pomáhání
vladaři	vladař	k1gMnSc3	vladař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
svoji	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
od	od	k7c2	od
příchodu	příchod	k1gInSc2	příchod
policistů	policista	k1gMnPc2	policista
<g/>
,	,	kIx,	,
loučení	loučení	k1gNnSc4	loučení
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
jízdu	jízda	k1gFnSc4	jízda
okolo	okolo	k7c2	okolo
rodné	rodný	k2eAgFnSc2d1	rodná
Borové	borový	k2eAgFnSc2d1	Borová
po	po	k7c6	po
loučení	loučení	k1gNnSc6	loučení
s	s	k7c7	s
vlastí	vlast	k1gFnSc7	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Vlastně	vlastně	k9	vlastně
ani	ani	k8xC	ani
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jede	jet	k5eAaImIp3nS	jet
a	a	k8xC	a
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
někdy	někdy	k6eAd1	někdy
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c6	na
horské	horský	k2eAgFnSc6d1	horská
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
vozu	vůz	k1gInSc2	vůz
splašili	splašit	k5eAaPmAgMnP	splašit
koně	kůň	k1gMnPc1	kůň
a	a	k8xC	a
kočí	kočí	k1gMnSc1	kočí
zmizel	zmizet	k5eAaPmAgMnS	zmizet
<g/>
,	,	kIx,	,
policajti	policajti	k?	policajti
vyskákali	vyskákat	k5eAaPmAgMnP	vyskákat
hned	hned	k6eAd1	hned
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
strachy	strach	k1gInPc1	strach
celí	celet	k5eAaImIp3nP	celet
bez	bez	k7c2	bez
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Havlíček	Havlíček	k1gMnSc1	Havlíček
však	však	k9	však
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ve	v	k7c6	v
voze	vůz	k1gInSc6	vůz
a	a	k8xC	a
dojel	dojet	k5eAaPmAgMnS	dojet
až	až	k6eAd1	až
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
dojeli	dojet	k5eAaPmAgMnP	dojet
do	do	k7c2	do
Brixenu	Brixen	k1gInSc2	Brixen
a	a	k8xC	a
Havlíčkovi	Havlíček	k1gMnSc3	Havlíček
se	se	k3xPyFc4	se
začínala	začínat	k5eAaImAgFnS	začínat
vytrácet	vytrácet	k5eAaImF	vytrácet
naděje	naděje	k1gFnPc4	naděje
na	na	k7c4	na
návrat	návrat	k1gInSc4	návrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úryvek	úryvek	k1gInSc4	úryvek
==	==	k?	==
</s>
</p>
<p>
<s>
Ach	ach	k0	ach
ty	ten	k3xDgFnPc1	ten
světe	svět	k1gInSc5	svět
<g/>
,	,	kIx,	,
obrácený	obrácený	k2eAgInSc1d1	obrácený
světe	svět	k1gInSc5	svět
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
Vzhůru	vzhůru	k6eAd1	vzhůru
nohama	noha	k1gFnPc7	noha
ve	v	k7c6	v
škarpě	škarpa	k1gFnSc6	škarpa
leží	ležet	k5eAaImIp3nS	ležet
stráž	stráž	k1gFnSc1	stráž
<g/>
,	,	kIx,	,
<g/>
ale	ale	k8xC	ale
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
delikventem	delikvent	k1gMnSc7	delikvent
samým	samý	k3xTgInPc3	samý
kluše	klusat	k5eAaImIp3nS	klusat
ekypáž	ekypáž	k?	ekypáž
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
Ach	ach	k0	ach
ty	ten	k3xDgFnPc1	ten
vládo	vláda	k1gFnSc5	vláda
<g/>
,	,	kIx,	,
převrácená	převrácený	k2eAgFnSc5d1	převrácená
vládo	vláda	k1gFnSc5	vláda
<g/>
!	!	kIx.	!
</s>
<s>
Národy	národ	k1gInPc1	národ
na	na	k7c6	na
šňůrce	šňůrka	k1gFnSc6	šňůrka
vodit	vodit	k5eAaImF	vodit
chceš	chtít	k5eAaImIp2nS	chtít
<g/>
,	,	kIx,	,
<g/>
ale	ale	k8xC	ale
s	s	k7c7	s
čtyřmi	čtyři	k4xCgMnPc7	čtyři
koňmi	kůň	k1gMnPc7	kůň
na	na	k7c6	na
opratíchvládnout	opratíchvládnout	k5eAaImF	opratíchvládnout
nemůžeš	moct	k5eNaImIp2nS	moct
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
online	onlinout	k5eAaPmIp3nS	onlinout
==	==	k?	==
</s>
</p>
<p>
<s>
BOROVSKÝ	Borovský	k1gMnSc1	Borovský
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
.	.	kIx.	.
</s>
<s>
Tyrolské	tyrolský	k2eAgFnPc1d1	tyrolská
elegie	elegie	k1gFnPc1	elegie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Hofbauer	Hofbauer	k1gMnSc1	Hofbauer
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Digitalizovaná	digitalizovaný	k2eAgFnSc1d1	digitalizovaná
podoba	podoba	k1gFnSc1	podoba
vydání	vydání	k1gNnSc2	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BOROVSKÝ	Borovský	k1gMnSc1	Borovský
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
.	.	kIx.	.
</s>
<s>
Tyrolské	tyrolský	k2eAgFnPc1d1	tyrolská
elegie	elegie	k1gFnPc1	elegie
<g/>
.	.	kIx.	.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1	ilustrace
Karel	Karel	k1gMnSc1	Karel
Stroff	Stroff	k1gMnSc1	Stroff
<g/>
.	.	kIx.	.
</s>
<s>
Bystřice	Bystřice	k1gFnSc1	Bystřice
pod	pod	k7c7	pod
Hostýnem	Hostýn	k1gInSc7	Hostýn
<g/>
:	:	kIx,	:
K.	K.	kA	K.
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Digitalizovaná	digitalizovaný	k2eAgFnSc1d1	digitalizovaná
podoba	podoba	k1gFnSc1	podoba
vydání	vydání	k1gNnSc2	vydání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BOROVSKÝ	Borovský	k1gMnSc1	Borovský
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
.	.	kIx.	.
</s>
<s>
Tyrolské	tyrolský	k2eAgFnPc1d1	tyrolská
elegie	elegie	k1gFnPc1	elegie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Elektronické	elektronický	k2eAgNnSc1d1	elektronické
vydání	vydání	k1gNnSc1	vydání
textu	text	k1gInSc2	text
(	(	kIx(	(
<g/>
e-kniha	eniha	k1gFnSc1	e-kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČERVENKA	Červenka	k1gMnSc1	Červenka
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
básnických	básnický	k2eAgFnPc2d1	básnická
knih	kniha	k1gFnPc2	kniha
<g/>
:	:	kIx,	:
díla	dílo	k1gNnPc1	dílo
české	český	k2eAgFnSc2d1	Česká
poezie	poezie	k1gFnSc2	poezie
od	od	k7c2	od
obrození	obrození	k1gNnSc2	obrození
do	do	k7c2	do
r.	r.	kA	r.
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
428	[number]	k4	428
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
202	[number]	k4	202
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
217	[number]	k4	217
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
[	[	kIx(	[
<g/>
Stať	stať	k1gFnSc1	stať
"	"	kIx"	"
<g/>
Tyrolské	tyrolský	k2eAgFnSc2d1	tyrolská
elegie	elegie	k1gFnSc2	elegie
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
str	str	kA	str
<g/>
.	.	kIx.	.
338	[number]	k4	338
<g/>
–	–	k?	–
<g/>
340	[number]	k4	340
<g/>
;	;	kIx,	;
autor	autor	k1gMnSc1	autor
Vladimír	Vladimír	k1gMnSc1	Vladimír
Macura	Macura	k1gMnSc1	Macura
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Tyrolské	tyrolský	k2eAgFnSc2d1	tyrolská
elegie	elegie	k1gFnSc2	elegie
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
