<s>
Descartova	Descartův	k2eAgFnSc1d1	Descartova
fyzika	fyzika	k1gFnSc1	fyzika
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
geometrická	geometrický	k2eAgFnSc1d1	geometrická
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ztotožňuje	ztotožňovat	k5eAaImIp3nS	ztotožňovat
hmotu	hmota	k1gFnSc4	hmota
s	s	k7c7	s
rozlehlostí	rozlehlost	k1gFnSc7	rozlehlost
a	a	k8xC	a
ruší	rušit	k5eAaImIp3nS	rušit
starší	starý	k2eAgFnPc4d2	starší
představy	představa	k1gFnPc4	představa
tíže	tíž	k1gFnSc2	tíž
a	a	k8xC	a
účelu	účel	k1gInSc3	účel
<g/>
,	,	kIx,	,
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
