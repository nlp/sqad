<s desamb="1">
Zakladatelem	zakladatel	k1gMnSc7
fondu	fond	k1gInSc2
je	být	k5eAaImIp3nS
podnikatel	podnikatel	k1gMnSc1
Karel	Karel	k1gMnSc1
Janeček	Janeček	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
čele	čelo	k1gNnSc6
fondu	fond	k1gInSc2
pět	pět	k4xCc4
Neuron	neuron	k1gInSc4
Founders	Foundersa	k1gFnPc2
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Kysilka	Kysilka	k1gFnSc1
<g/>
,	,	kIx,
Taťána	Taťána	k1gFnSc1
le	le	k?
Moigne	Moign	k1gMnSc5
<g/>
,	,	kIx,
Eduard	Eduard	k1gMnSc1
Kučera	Kučera	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Wichterle	Wichterle	k1gFnSc2
a	a	k8xC
Monika	Monika	k1gFnSc1
Vondráková	Vondráková	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
fond	fond	k1gInSc4
řídí	řídit	k5eAaImIp3nS
od	od	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
vzniku	vznik	k1gInSc2
<g/>
.	.	kIx.
</s>