<s>
Nadační	nadační	k2eAgInSc1d1
fond	fond	k1gInSc1
Neuron	neuron	k1gInSc4
na	na	k7c4
podporu	podpora	k1gFnSc4
vědy	věda	k1gFnSc2
</s>
<s>
Nadační	nadační	k2eAgInSc1d1
fond	fond	k1gInSc1
Neuron	neuron	k1gInSc4
na	na	k7c4
podporu	podpora	k1gFnSc4
vědy	věda	k1gFnSc2
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Janeček	Janeček	k1gMnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
2010	#num#	k4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
nadační	nadační	k2eAgInSc4d1
fond	fond	k1gInSc4
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Tržiště	tržiště	k1gNnSc1
366	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
118	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Oficiální	oficiální	k2eAgNnSc1d1
web	web	k1gInSc4
</s>
<s>
www.nfneuron.cz	www.nfneuron.cz	k1gMnSc1
IČO	IČO	kA
</s>
<s>
01871188	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nadační	nadační	k2eAgInSc1d1
fond	fond	k1gInSc1
Neuron	neuron	k1gInSc1
je	být	k5eAaImIp3nS
český	český	k2eAgInSc4d1
nadační	nadační	k2eAgInSc4d1
fond	fond	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
podpora	podpora	k1gFnSc1
vědkyň	vědkyně	k1gFnPc2
a	a	k8xC
vědců	vědec	k1gMnPc2
<g/>
,	,	kIx,
popularizace	popularizace	k1gFnSc2
vědy	věda	k1gFnSc2
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
moderního	moderní	k2eAgNnSc2d1
mecenášství	mecenášství	k1gNnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fond	fond	k1gInSc1
uděluje	udělovat	k5eAaImIp3nS
mj.	mj.	kA
ocenění	ocenění	k1gNnSc4
Ceny	cena	k1gFnSc2
Neuron	neuron	k1gInSc4
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
finanční	finanční	k2eAgFnSc7d1
odměnou	odměna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
fond	fond	k1gInSc1
celkově	celkově	k6eAd1
podpořil	podpořit	k5eAaPmAgInS
českou	český	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
částkou	částka	k1gFnSc7
přes	přes	k7c4
60	#num#	k4
mil	míle	k1gFnPc2
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fond	fond	k1gInSc1
je	být	k5eAaImIp3nS
financován	financovat	k5eAaBmNgInS
výhradně	výhradně	k6eAd1
z	z	k7c2
příspěvků	příspěvek	k1gInPc2
mecenášů	mecenáš	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakladatelem	zakladatel	k1gMnSc7
fondu	fond	k1gInSc2
je	být	k5eAaImIp3nS
podnikatel	podnikatel	k1gMnSc1
Karel	Karel	k1gMnSc1
Janeček	Janeček	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
čele	čelo	k1gNnSc6
fondu	fond	k1gInSc2
pět	pět	k4xCc4
Neuron	neuron	k1gInSc4
Founders	Foundersa	k1gFnPc2
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Kysilka	Kysilka	k1gFnSc1
<g/>
,	,	kIx,
Taťána	Taťána	k1gFnSc1
le	le	k?
Moigne	Moign	k1gMnSc5
<g/>
,	,	kIx,
Eduard	Eduard	k1gMnSc1
Kučera	Kučera	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Wichterle	Wichterle	k1gFnSc2
a	a	k8xC
Monika	Monika	k1gFnSc1
Vondráková	Vondráková	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
fond	fond	k1gInSc4
řídí	řídit	k5eAaImIp3nS
od	od	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
vzniku	vznik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etapa	etapa	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
-	-	kIx~
2017	#num#	k4
</s>
<s>
První	první	k4xOgFnSc1
etapa	etapa	k1gFnSc1
existence	existence	k1gFnSc2
NF	NF	kA
Neuron	neuron	k1gInSc1
odstartovala	odstartovat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
Nadační	nadační	k2eAgInSc1d1
fond	fond	k1gInSc1
Karla	Karel	k1gMnSc2
Janečka	Janeček	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc1
vznik	vznik	k1gInSc4
iniciovali	iniciovat	k5eAaBmAgMnP
Karel	Karel	k1gMnSc1
Janeček	Janeček	k1gMnSc1
<g/>
,	,	kIx,
Monika	Monika	k1gFnSc1
Vondráková	Vondráková	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
fond	fond	k1gInSc4
od	od	k7c2
počátku	počátek	k1gInSc2
řídí	řídit	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
lékař	lékař	k1gMnSc1
Josef	Josef	k1gMnSc1
Veselka	Veselka	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původním	původní	k2eAgInSc7d1
impulsem	impuls	k1gInSc7
pro	pro	k7c4
vznik	vznik	k1gInSc4
byla	být	k5eAaImAgFnS
snaha	snaha	k1gFnSc1
o	o	k7c6
kompenzaci	kompenzace	k1gFnSc6
snížených	snížený	k2eAgFnPc2d1
veřejných	veřejný	k2eAgFnPc2d1
financí	finance	k1gFnPc2
na	na	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
výzkum	výzkum	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
pomoci	pomoct	k5eAaPmF
mladým	mladý	k2eAgMnPc3d1
vědcům	vědec	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
získali	získat	k5eAaPmAgMnP
zázemí	zázemí	k1gNnSc4
v	v	k7c6
ČR	ČR	kA
a	a	k8xC
byli	být	k5eAaImAgMnP
tak	tak	k6eAd1
motivováni	motivovat	k5eAaBmNgMnP
vracet	vracet	k5eAaImF
se	se	k3xPyFc4
ze	z	k7c2
zahraničí	zahraničí	k1gNnSc2
zpět	zpět	k6eAd1
do	do	k7c2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
fond	fond	k1gInSc1
od	od	k7c2
počátku	počátek	k1gInSc2
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c6
představování	představování	k1gNnSc6
výjimečných	výjimečný	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
vědy	věda	k1gFnSc2
veřejnosti	veřejnost	k1gFnSc2
a	a	k8xC
kontinuálně	kontinuálně	k6eAd1
oceňuje	oceňovat	k5eAaImIp3nS
zasloužilé	zasloužilý	k2eAgMnPc4d1
vědce	vědec	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
jsou	být	k5eAaImIp3nP
inspirací	inspirace	k1gFnSc7
dalším	další	k2eAgFnPc3d1
generacím	generace	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transformace	transformace	k1gFnSc1
na	na	k7c4
Nadační	nadační	k2eAgInSc4d1
fond	fond	k1gInSc4
Neuron	neuron	k1gInSc4
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
fond	fond	k1gInSc1
přijal	přijmout	k5eAaPmAgInS
dary	dar	k1gInPc4
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
16	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
,	,	kIx,
když	když	k8xS
mezi	mezi	k7c4
největší	veliký	k2eAgMnPc4d3
dárce	dárce	k1gMnPc4
patřili	patřit	k5eAaImAgMnP
Nadace	nadace	k1gFnSc1
RSJ	RSJ	kA
(	(	kIx(
<g/>
5	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
Winkler	Winkler	k1gMnSc1
(	(	kIx(
<g/>
2,5	2,5	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
)	)	kIx)
a	a	k8xC
Nadace	nadace	k1gFnSc1
Karla	Karel	k1gMnSc2
Janečka	Janeček	k1gMnSc2
(	(	kIx(
<g/>
2,5	2,5	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prvními	první	k4xOgInPc7
podporovanými	podporovaný	k2eAgInPc7d1
obory	obor	k1gInPc7
byly	být	k5eAaImAgFnP
matematika	matematika	k1gFnSc1
<g/>
,	,	kIx,
medicína	medicína	k1gFnSc1
a	a	k8xC
ekonomie	ekonomie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
fond	fond	k1gInSc1
rozšířil	rozšířit	k5eAaPmAgInS
podporu	podpora	k1gFnSc4
i	i	k9
na	na	k7c4
obor	obor	k1gInSc4
fyzika	fyzika	k1gFnSc1
<g/>
,	,	kIx,
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
biologie	biologie	k1gFnSc1
a	a	k8xC
computer	computer	k1gInSc1
science	scienec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
předávání	předávání	k1gNnPc2
cen	cena	k1gFnPc2
a	a	k8xC
udělování	udělování	k1gNnSc4
podpory	podpora	k1gFnSc2
projektům	projekt	k1gInPc3
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etapa	etapa	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
revizi	revize	k1gFnSc3
dosavadního	dosavadní	k2eAgInSc2d1
způsobu	způsob	k1gInSc2
podpory	podpora	k1gFnSc2
vědců	vědec	k1gMnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
finanční	finanční	k2eAgFnSc1d1
odměna	odměna	k1gFnSc1
<g/>
,	,	kIx,
spojená	spojený	k2eAgFnSc1d1
s	s	k7c7
Cenami	cena	k1gFnPc7
Neuron	neuron	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
věnována	věnovat	k5eAaPmNgFnS,k5eAaImNgFnS
čistě	čistě	k6eAd1
na	na	k7c4
osobní	osobní	k2eAgFnPc4d1
potřeby	potřeba	k1gFnPc4
vědkyň	vědkyně	k1gFnPc2
a	a	k8xC
vědců	vědec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
změnil	změnit	k5eAaPmAgInS
fond	fond	k1gInSc1
svou	svůj	k3xOyFgFnSc4
strategii	strategie	k1gFnSc4
a	a	k8xC
změnil	změnit	k5eAaPmAgInS
se	se	k3xPyFc4
způsob	způsob	k1gInSc1
podpory	podpora	k1gFnSc2
vědců	vědec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fond	fond	k1gInSc1
se	se	k3xPyFc4
soustřeďuje	soustřeďovat	k5eAaImIp3nS
zejména	zejména	k9
na	na	k7c4
udělování	udělování	k1gNnSc4
prestižních	prestižní	k2eAgFnPc2d1
Cen	cena	k1gFnPc2
Neuron	neuron	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Ceny	cena	k1gFnPc1
Neuron	neuron	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Cena	cena	k1gFnSc1
Neuron	neuron	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Ceny	cena	k1gFnPc1
Neuron	neuron	k1gInSc1
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgFnP
s	s	k7c7
vysokým	vysoký	k2eAgNnSc7d1
finančním	finanční	k2eAgNnSc7d1
ohodnocením	ohodnocení	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
laureáti	laureát	k1gMnPc1
mohou	moct	k5eAaImIp3nP
využít	využít	k5eAaPmF
k	k	k7c3
jakýmkoliv	jakýkoliv	k3yIgInPc3
účelům	účel	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ročně	ročně	k6eAd1
jde	jít	k5eAaImIp3nS
až	až	k9
o	o	k7c4
8,5	8,5	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
ze	z	k7c2
soukromých	soukromý	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
vůbec	vůbec	k9
nejvyšší	vysoký	k2eAgFnSc1d3
podpora	podpora	k1gFnSc1
pro	pro	k7c4
ocenění	ocenění	k1gNnSc4
vědy	věda	k1gFnSc2
v	v	k7c6
naší	náš	k3xOp1gFnSc6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Ceny	cena	k1gFnPc1
jsou	být	k5eAaImIp3nP
udělovány	udělovat	k5eAaImNgFnP
ve	v	k7c6
třech	tři	k4xCgFnPc6
kategoriích	kategorie	k1gFnPc6
a	a	k8xC
v	v	k7c6
sedmi	sedm	k4xCc6
vědeckých	vědecký	k2eAgFnPc6d1
disciplínách	disciplína	k1gFnPc6
–	–	k?
společenské	společenský	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
medicína	medicína	k1gFnSc1
<g/>
,	,	kIx,
matematika	matematika	k1gFnSc1
<g/>
,	,	kIx,
fyzika	fyzika	k1gFnSc1
<g/>
,	,	kIx,
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
biologie	biologie	k1gFnSc1
a	a	k8xC
computer	computer	k1gInSc1
science	scienec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceny	cena	k1gFnPc1
se	se	k3xPyFc4
předávají	předávat	k5eAaImIp3nP
se	s	k7c7
vždy	vždy	k6eAd1
jednou	jeden	k4xCgFnSc7
ročně	ročně	k6eAd1
v	v	k7c6
rámci	rámec	k1gInSc6
slavnostního	slavnostní	k2eAgInSc2d1
večera	večer	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
činnosti	činnost	k1gFnPc1
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
popularizace	popularizace	k1gFnSc2
vědy	věda	k1gFnSc2
fond	fond	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
také	také	k9
terénní	terénní	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
českých	český	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
do	do	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
(	(	kIx(
<g/>
Expedice	expedice	k1gFnSc1
Neuron	neuron	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podpořil	podpořit	k5eAaPmAgMnS
již	již	k9
8	#num#	k4
expedic	expedice	k1gFnPc2
v	v	k7c6
celkové	celkový	k2eAgFnSc6d1
hodnotě	hodnota	k1gFnSc6
přes	přes	k7c4
3	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
minulosti	minulost	k1gFnSc6
také	také	k9
podporoval	podporovat	k5eAaImAgMnS
<g/>
:	:	kIx,
</s>
<s>
Cenu	cena	k1gFnSc4
pro	pro	k7c4
pedagogy	pedagog	k1gMnPc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
200.000	200.000	k4
Kč	Kč	kA
vyhlašovanou	vyhlašovaný	k2eAgFnSc7d1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Učenou	učený	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
ČR	ČR	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
oceňuje	oceňovat	k5eAaImIp3nS
pedagožky	pedagožka	k1gFnPc4
a	a	k8xC
pedagogy	pedagog	k1gMnPc4
<g/>
,	,	kIx,
vedoucí	vedoucí	k1gMnSc1
své	svůj	k3xOyFgMnPc4
žáky	žák	k1gMnPc4
k	k	k7c3
zájmu	zájem	k1gInSc2
o	o	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
výzkum	výzkum	k1gInSc4
už	už	k6eAd1
na	na	k7c6
středních	střední	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
</s>
<s>
Cenu	cena	k1gFnSc4
za	za	k7c4
popularizaci	popularizace	k1gFnSc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
100	#num#	k4
000	#num#	k4
Kč	Kč	kA
vyhlašovanou	vyhlašovaný	k2eAgFnSc7d1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Akademií	akademie	k1gFnSc7
věd	věda	k1gFnPc2
ČR	ČR	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
oceňuje	oceňovat	k5eAaImIp3nS
mladé	mladý	k2eAgFnPc4d1
vědkyně	vědkyně	k1gFnPc4
a	a	k8xC
vědce	vědec	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
kromě	kromě	k7c2
výzkumné	výzkumný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
soustavně	soustavně	k6eAd1
věnují	věnovat	k5eAaPmIp3nP,k5eAaImIp3nP
i	i	k9
popularizaci	popularizace	k1gFnSc4
vědy	věda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
O	o	k7c4
nás	my	k3xPp1nPc4
|	|	kIx~
NF	NF	kA
Neuron	neuron	k1gInSc1
<g/>
.	.	kIx.
www.nfneuron.cz	www.nfneuron.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Veřejný	veřejný	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
a	a	k8xC
Sbírka	sbírka	k1gFnSc1
listin	listina	k1gFnPc2
–	–	k?
Ministerstvo	ministerstvo	k1gNnSc1
spravedlnosti	spravedlnost	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
or	or	k?
<g/>
.	.	kIx.
<g/>
justice	justice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nadační	nadační	k2eAgInSc1d1
fond	fond	k1gInSc1
Neuron	neuron	k1gInSc4
na	na	k7c4
podporu	podpora	k1gFnSc4
české	český	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
–	–	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
2016	#num#	k4
<g/>
.	.	kIx.
or	or	k?
<g/>
.	.	kIx.
<g/>
justice	justice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadační	nadační	k2eAgInSc1d1
fond	fond	k1gInSc1
Neuron	neuron	k1gInSc4
na	na	k7c4
podporu	podpora	k1gFnSc4
české	český	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
https://web.archive.org/web/20180510185332/http://www.nfneuron.cz/cs	https://web.archive.org/web/20180510185332/http://www.nfneuron.cz/cs	k6eAd1
</s>
<s>
https://prazsky.denik.cz/zpravy_region/ceny-neuron-za-vedu-se-zmeni-penize-pujdou-primo-vedcum-20180122.html	https://prazsky.denik.cz/zpravy_region/ceny-neuron-za-vedu-se-zmeni-penize-pujdou-primo-vedcum-20180122.html	k1gMnSc1
</s>
<s>
https://archiv.ihned.cz/c1-66022810-mecenassky-fond-neuron-chysta-novou-ceskou-nobelovku-pro-vedce	https://archiv.ihned.cz/c1-66022810-mecenassky-fond-neuron-chysta-novou-ceskou-nobelovku-pro-vedce	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
av	av	k?
<g/>
2014833240	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
310634949	#num#	k4
</s>
