<s>
Kladivo	kladivo	k1gNnSc1	kladivo
na	na	k7c4	na
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Otakara	Otakar	k1gMnSc2	Otakar
Vávry	Vávra	k1gMnSc2	Vávra
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
knihy	kniha	k1gFnSc2	kniha
Václava	Václav	k1gMnSc2	Václav
Kaplického	Kaplický	k2eAgMnSc2d1	Kaplický
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
čarodějnických	čarodějnický	k2eAgInPc6d1	čarodějnický
procesech	proces	k1gInPc6	proces
ve	v	k7c6	v
Velkých	velká	k1gFnPc6	velká
Losinách	Losiny	k1gFnPc6	Losiny
a	a	k8xC	a
v	v	k7c6	v
Šumperku	Šumperk	k1gInSc6	Šumperk
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1678	[number]	k4	1678
-	-	kIx~	-
1695	[number]	k4	1695
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inkvizitor	inkvizitor	k1gMnSc1	inkvizitor
nechává	nechávat	k5eAaImIp3nS	nechávat
upalovat	upalovat	k5eAaImF	upalovat
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
nějaký	nějaký	k3yIgInSc1	nějaký
náznak	náznak	k1gInSc1	náznak
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zaříkávání	zaříkávání	k1gNnSc2	zaříkávání
<g/>
.	.	kIx.	.
</s>
<s>
Oběti	oběť	k1gFnPc4	oběť
postupně	postupně	k6eAd1	postupně
vybírá	vybírat	k5eAaImIp3nS	vybírat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zlikvidoval	zlikvidovat	k5eAaPmAgInS	zlikvidovat
názorové	názorový	k2eAgMnPc4d1	názorový
odpůrce	odpůrce	k1gMnPc4	odpůrce
a	a	k8xC	a
bohaté	bohatý	k2eAgMnPc4d1	bohatý
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
majetek	majetek	k1gInSc1	majetek
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
mohl	moct	k5eAaImAgMnS	moct
zkonfiskovat	zkonfiskovat	k5eAaPmF	zkonfiskovat
<g/>
.	.	kIx.	.
</s>
<s>
Přiznání	přiznání	k1gNnSc1	přiznání
vynucuje	vynucovat	k5eAaImIp3nS	vynucovat
mučením	mučení	k1gNnSc7	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
oběti	oběť	k1gFnPc4	oběť
procesů	proces	k1gInPc2	proces
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
dostanou	dostat	k5eAaPmIp3nP	dostat
i	i	k9	i
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
hájící	hájící	k2eAgMnSc1d1	hájící
nespravedlivě	spravedlivě	k6eNd1	spravedlivě
odsouzené	odsouzený	k2eAgInPc4d1	odsouzený
a	a	k8xC	a
ukazující	ukazující	k2eAgInPc4d1	ukazující
na	na	k7c4	na
nelidské	lidský	k2eNgFnPc4d1	nelidská
metody	metoda	k1gFnPc4	metoda
výslechů	výslech	k1gInPc2	výslech
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
vznik	vznik	k1gInSc4	vznik
navozování	navozování	k1gNnSc2	navozování
atmosféry	atmosféra	k1gFnSc2	atmosféra
strachu	strach	k1gInSc2	strach
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
metody	metoda	k1gFnPc1	metoda
vynucování	vynucování	k1gNnSc2	vynucování
"	"	kIx"	"
<g/>
přiznání	přiznání	k1gNnSc2	přiznání
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
připomínaly	připomínat	k5eAaImAgFnP	připomínat
dobu	doba	k1gFnSc4	doba
stalinismu	stalinismus	k1gInSc2	stalinismus
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
promítán	promítán	k2eAgMnSc1d1	promítán
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
kinech	kino	k1gNnPc6	kino
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
stažen	stáhnout	k5eAaPmNgInS	stáhnout
z	z	k7c2	z
distribuce	distribuce	k1gFnSc2	distribuce
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
promítán	promítat	k5eAaImNgInS	promítat
pouze	pouze	k6eAd1	pouze
příležitostně	příležitostně	k6eAd1	příležitostně
ve	v	k7c6	v
filmových	filmový	k2eAgInPc6d1	filmový
klubech	klub	k1gInPc6	klub
<g/>
,	,	kIx,	,
na	na	k7c6	na
televizních	televizní	k2eAgFnPc6d1	televizní
obrazovkách	obrazovka	k1gFnPc6	obrazovka
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
román	román	k1gInSc1	román
Kladivo	kladivo	k1gNnSc1	kladivo
na	na	k7c4	na
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
Václava	Václav	k1gMnSc2	Václav
Kaplického	Kaplický	k2eAgInSc2d1	Kaplický
Malleus	Malleus	k1gInSc4	Malleus
maleficarum	maleficarum	k1gInSc4	maleficarum
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Kladivo	kladivo	k1gNnSc1	kladivo
na	na	k7c4	na
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
<g/>
)	)	kIx)	)
–	–	k?	–
syntéza	syntéza	k1gFnSc1	syntéza
představ	představa	k1gFnPc2	představa
o	o	k7c4	o
čarodějnictví	čarodějnictví	k1gNnSc4	čarodějnictví
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1486	[number]	k4	1486
</s>
