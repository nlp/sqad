<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
pojmenování	pojmenování	k1gNnSc1	pojmenování
biedermeier	biedermeier	k1gInSc1	biedermeier
použil	použít	k5eAaPmAgInS	použít
poprvé	poprvé	k6eAd1	poprvé
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
Joseph	Joseph	k1gMnSc1	Joseph
Victor	Victor	k1gMnSc1	Victor
von	von	k1gInSc4	von
Scheffel	Scheffela	k1gFnPc2	Scheffela
v	v	k7c6	v
mnichovském	mnichovský	k2eAgInSc6d1	mnichovský
časopise	časopis	k1gInSc6	časopis
Fliegende	Fliegend	k1gInSc5	Fliegend
Blätter	Blätter	k1gInSc4	Blätter
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
spojením	spojení	k1gNnSc7	spojení
příjmení	příjmení	k1gNnSc4	příjmení
Biedermann	Biedermann	k1gInSc1	Biedermann
a	a	k8xC	a
Bummelmeyer	Bummelmeyer	k1gInSc1	Bummelmeyer
<g/>
.	.	kIx.	.
</s>
