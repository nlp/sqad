<s>
1884	[number]	k4	1884
(	(	kIx(	(
<g/>
MDCCCLXXXIV	MDCCCLXXXIV	kA	MDCCCLXXXIV
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dle	dle	k7c2	dle
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
započal	započnout	k5eAaPmAgInS	započnout
úterým	úterý	k1gNnSc7	úterý
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
připojení	připojení	k1gNnSc2	připojení
obce	obec	k1gFnSc2	obec
Holešovice-Bubny	Holešovice-Bubna	k1gFnSc2	Holešovice-Bubna
k	k	k7c3	k
městu	město	k1gNnSc3	město
Praze	Praha	k1gFnSc6	Praha
jako	jako	k8xC	jako
nové	nový	k2eAgFnSc3d1	nová
pražské	pražský	k2eAgFnSc3d1	Pražská
čtvrti	čtvrt	k1gFnSc3	čtvrt
nazvané	nazvaný	k2eAgFnPc1d1	nazvaná
Praha	Praha	k1gFnSc1	Praha
VII	VII	kA	VII
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zahájen	zahájen	k2eAgInSc4d1	zahájen
provoz	provoz	k1gInSc4	provoz
parní	parní	k2eAgFnSc2d1	parní
tramvaje	tramvaj	k1gFnSc2	tramvaj
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
založena	založit	k5eAaPmNgFnS	založit
česká	český	k2eAgFnSc1d1	Česká
kolej	kolej	k1gFnSc1	kolej
Bohemica	Bohemica	k1gFnSc1	Bohemica
Svět	svět	k1gInSc1	svět
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Založena	založen	k2eAgFnSc1d1	založena
kolonie	kolonie	k1gFnSc1	kolonie
Německá	německý	k2eAgFnSc1d1	německá
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
Afrika	Afrika	k1gFnSc1	Afrika
5	[number]	k4	5
<g />
.	.	kIx.	.
</s>
<s>
<g/>
července	červenec	k1gInSc2	červenec
–	–	k?	–
Založena	založen	k2eAgFnSc1d1	založena
německá	německý	k2eAgFnSc1d1	německá
kolonie	kolonie	k1gFnSc1	kolonie
Togoland	Togoland	k1gInSc1	Togoland
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Založena	založen	k2eAgFnSc1d1	založena
německá	německý	k2eAgFnSc1d1	německá
kolonie	kolonie	k1gFnSc1	kolonie
Kamerun	Kamerun	k1gInSc1	Kamerun
říjen	říjen	k1gInSc1	říjen
–	–	k?	–
Na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
konferenci	konference	k1gFnSc6	konference
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
základní	základní	k2eAgInSc1d1	základní
poledník	poledník	k1gInSc1	poledník
procházející	procházející	k2eAgInSc1d1	procházející
Královskou	královský	k2eAgFnSc7d1	královská
observatoří	observatoř	k1gFnSc7	observatoř
v	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Temešvár	Temešvár	k1gInSc1	Temešvár
se	se	k3xPyFc4	se
po	po	k7c6	po
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
stal	stát	k5eAaPmAgInS	stát
druhým	druhý	k4xOgNnSc7	druhý
městem	město	k1gNnSc7	město
s	s	k7c7	s
elektricky	elektricky	k6eAd1	elektricky
osvětlenými	osvětlený	k2eAgFnPc7d1	osvětlená
ulicemi	ulice	k1gFnPc7	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Koreje	Korea	k1gFnSc2	Korea
přišli	přijít	k5eAaPmAgMnP	přijít
první	první	k4xOgMnPc1	první
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
misionáři	misionář	k1gMnPc1	misionář
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
světová	světový	k2eAgFnSc1d1	světová
premiéra	premiéra	k1gFnSc1	premiéra
opery	opera	k1gFnSc2	opera
Manon	Manona	k1gFnPc2	Manona
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Lester	Lester	k1gMnSc1	Lester
Allan	Allan	k1gMnSc1	Allan
Pelton	Pelton	k1gInSc4	Pelton
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
rychloběžnou	rychloběžný	k2eAgFnSc4d1	rychloběžná
vodní	vodní	k2eAgFnSc4d1	vodní
turbínu	turbína	k1gFnSc4	turbína
Paul	Paul	k1gMnSc1	Paul
Nipkow	Nipkow	k1gMnSc1	Nipkow
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
mechanický	mechanický	k2eAgInSc4d1	mechanický
rozklad	rozklad	k1gInSc4	rozklad
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
fotočlánkem	fotočlánek	k1gInSc7	fotočlánek
se	se	k3xPyFc4	se
otáčel	otáčet	k5eAaImAgInS	otáčet
kotouč	kotouč	k1gInSc1	kotouč
s	s	k7c7	s
otvory	otvor	k1gInPc7	otvor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
spirálu	spirála	k1gFnSc4	spirála
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
pracovala	pracovat	k5eAaImAgFnS	pracovat
většina	většina	k1gFnSc1	většina
systémů	systém	k1gInPc2	systém
mechanické	mechanický	k2eAgFnSc2d1	mechanická
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Edwin	Edwin	k2eAgInSc1d1	Edwin
Abbott	Abbott	k2eAgInSc1d1	Abbott
Abbott	Abbott	k1gInSc1	Abbott
–	–	k?	–
Plochozemě	Plochozema	k1gFnSc3	Plochozema
<g/>
:	:	kIx,	:
román	román	k1gInSc1	román
mnoha	mnoho	k4c2	mnoho
rozměrů	rozměr	k1gInPc2	rozměr
Jakub	Jakub	k1gMnSc1	Jakub
Arbes	Arbes	k1gMnSc1	Arbes
–	–	k?	–
Zázračná	zázračný	k2eAgFnSc1d1	zázračná
madona	madona	k1gFnSc1	madona
Joris	Joris	k1gFnSc2	Joris
Karl	Karla	k1gFnPc2	Karla
Huysmans	Huysmans	k1gInSc1	Huysmans
–	–	k?	–
Naruby	naruby	k6eAd1	naruby
Alois	Alois	k1gMnSc1	Alois
Jirásek	Jirásek	k1gMnSc1	Jirásek
–	–	k?	–
Psohlavci	psohlavec	k1gMnSc6	psohlavec
André	André	k1gMnSc1	André
Laurie	Laurie	k1gFnSc2	Laurie
–	–	k?	–
Dědic	dědic	k1gMnSc1	dědic
Robinsonův	Robinsonův	k2eAgMnSc1d1	Robinsonův
Henryk	Henryk	k1gMnSc1	Henryk
Sienkiewicz	Sienkiewicz	k1gMnSc1	Sienkiewicz
–	–	k?	–
Ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
mečem	meč	k1gInSc7	meč
Anselm	Anselma	k1gFnPc2	Anselma
Schott	Schott	k2eAgInSc1d1	Schott
–	–	k?	–
Schottův	Schottův	k2eAgInSc4d1	Schottův
misál	misál	k1gInSc4	misál
Mark	Mark	k1gMnSc1	Mark
Twain	Twain	k1gMnSc1	Twain
–	–	k?	–
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
Huckleberryho	Huckleberry	k1gMnSc2	Huckleberry
Finna	Finn	k1gMnSc2	Finn
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
–	–	k?	–
Archipel	archipel	k1gInSc4	archipel
v	v	k7c6	v
plamenech	plamen	k1gInPc6	plamen
Jules	Julesa	k1gFnPc2	Julesa
Verne	Vern	k1gInSc5	Vern
–	–	k?	–
Fffff	Fffff	k1gInSc1	Fffff
<g/>
...	...	k?	...
<g/>
plesk	plesk	k1gInSc1	plesk
<g/>
!	!	kIx.	!
</s>
<s>
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gInSc5	Vern
–	–	k?	–
Hvězda	Hvězda	k1gMnSc1	Hvězda
jihu	jih	k1gInSc2	jih
Poutník	poutník	k1gMnSc1	poutník
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
Automatický	automatický	k2eAgMnSc1d1	automatický
abecedně	abecedně	k6eAd1	abecedně
řazený	řazený	k2eAgInSc4d1	řazený
seznam	seznam	k1gInSc4	seznam
viz	vidět	k5eAaImRp2nS	vidět
Kategorie	kategorie	k1gFnSc1	kategorie
<g/>
:	:	kIx,	:
<g/>
Narození	narození	k1gNnSc1	narození
1884	[number]	k4	1884
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Rudolf	Rudolf	k1gMnSc1	Rudolf
Koppitz	Koppitz	k1gMnSc1	Koppitz
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
fotograf	fotograf	k1gMnSc1	fotograf
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
†	†	k?	†
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Lucie	Lucie	k1gFnSc1	Lucie
Klímová	Klímová	k1gFnSc1	Klímová
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
malířka	malířka	k1gFnSc1	malířka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
(	(	kIx(	(
<g/>
†	†	k?	†
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Josef	Josef	k1gMnSc1	Josef
Ryšavý	Ryšavý	k1gMnSc1	Ryšavý
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
geodezie	geodezie	k1gFnSc2	geodezie
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
na	na	k7c4	na
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Sucharda	Sucharda	k1gMnSc1	Sucharda
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
řezbář	řezbář	k1gMnSc1	řezbář
a	a	k8xC	a
loutkář	loutkář	k1gMnSc1	loutkář
(	(	kIx(	(
<g/>
†	†	k?	†
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Vilém	Vilém	k1gMnSc1	Vilém
Kreibich	Kreibich	k1gMnSc1	Kreibich
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Karl	Karl	k1gMnSc1	Karl
Breu	Breus	k1gInSc2	Breus
<g/>
,	,	kIx,	,
iluzionista	iluzionista	k1gMnSc1	iluzionista
(	(	kIx(	(
<g/>
†	†	k?	†
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
<g />
.	.	kIx.	.
</s>
<s>
ledna	leden	k1gInSc2	leden
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Stránský	Stránský	k1gMnSc1	Stránský
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
právník	právník	k1gMnSc1	právník
(	(	kIx(	(
<g/>
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Heinrich	Heinrich	k1gMnSc1	Heinrich
Blum	bluma	k1gFnPc2	bluma
<g/>
,	,	kIx,	,
brněnský	brněnský	k2eAgMnSc1d1	brněnský
funkcionalistický	funkcionalistický	k2eAgMnSc1d1	funkcionalistický
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
†	†	k?	†
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Marušák	Marušák	k1gMnSc1	Marušák
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
vězeň	vězeň	k1gMnSc1	vězeň
(	(	kIx(	(
<g/>
†	†	k?	†
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Zázvorka	zázvorka	k1gMnSc1	zázvorka
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Helena	Helena	k1gFnSc1	Helena
Johnová	Johnová	k1gFnSc1	Johnová
<g/>
,	,	kIx,	,
sochařka	sochařka	k1gFnSc1	sochařka
<g/>
,	,	kIx,	,
keramička	keramička	k1gFnSc1	keramička
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
†	†	k?	†
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
C.	C.	kA	C.
Vondrouš	Vondrouš	k1gMnSc1	Vondrouš
<g/>
,	,	kIx,	,
rytec	rytec	k1gMnSc1	rytec
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
(	(	kIx(	(
<g/>
†	†	k?	†
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Julie	Julie	k1gFnSc1	Julie
Šupichová	Šupichová	k1gFnSc1	Šupichová
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
esperantistka	esperantistka	k1gFnSc1	esperantistka
(	(	kIx(	(
<g/>
†	†	k?	†
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g />
.	.	kIx.	.
</s>
<s>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Linka	linka	k1gFnSc1	linka
Procházková	Procházková	k1gFnSc1	Procházková
<g/>
,	,	kIx,	,
malířka	malířka	k1gFnSc1	malířka
(	(	kIx(	(
<g/>
†	†	k?	†
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
František	František	k1gMnSc1	František
Smotlacha	smotlacha	k1gMnSc1	smotlacha
<g/>
,	,	kIx,	,
mykolog	mykolog	k1gMnSc1	mykolog
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
českého	český	k2eAgInSc2d1	český
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
†	†	k?	†
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
<g />
.	.	kIx.	.
</s>
<s>
února	únor	k1gInSc2	únor
–	–	k?	–
Blažej	Blažej	k1gMnSc1	Blažej
Ráček	Ráček	k1gMnSc1	Ráček
<g/>
,	,	kIx,	,
katolický	katolický	k2eAgMnSc1d1	katolický
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
církevní	církevní	k2eAgMnSc1d1	církevní
[[	[[	k?	[[
<g/>
historik	historik	k1gMnSc1	historik
(	(	kIx(	(
<g/>
†	†	k?	†
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Vlastislav	Vlastislav	k1gMnSc1	Vlastislav
Hofman	Hofman	k1gMnSc1	Hofman
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
urbanista	urbanista	k1gMnSc1	urbanista
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
†	†	k?	†
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Valentin	Valentin	k1gMnSc1	Valentin
Skurský	Skurský	k2eAgMnSc1d1	Skurský
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
Znojma	Znojmo	k1gNnSc2	Znojmo
(	(	kIx(	(
<g/>
†	†	k?	†
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
Karl	Karl	k1gMnSc1	Karl
Friedrich	Friedrich	k1gMnSc1	Friedrich
Kühn	Kühn	k1gMnSc1	Kühn
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
†	†	k?	†
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
legionář	legionář	k1gMnSc1	legionář
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
místopředseda	místopředseda	k1gMnSc1	místopředseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
†	†	k?	†
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
března	březen	k1gInSc2	březen
–	–	k?	–
Ivan	Ivan	k1gMnSc1	Ivan
Dérer	Dérer	k1gMnSc1	Dérer
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
několika	několik	k4yIc2	několik
vlád	vláda	k1gFnPc2	vláda
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
března	březen	k1gInSc2	březen
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Bedřich	Bedřich	k1gMnSc1	Bedřich
Feigl	Feigl	k1gMnSc1	Feigl
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
(	(	kIx(	(
<g/>
†	†	k?	†
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Trýb	Trýb	k1gMnSc1	Trýb
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Alexandr	Alexandr	k1gMnSc1	Alexandr
Podaševský	Podaševský	k2eAgMnSc1d1	Podaševský
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
†	†	k?	†
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
<g />
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
†	†	k?	†
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
Alfred	Alfred	k1gMnSc1	Alfred
Maria	Maria	k1gFnSc1	Maria
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
sbormistr	sbormistr	k1gMnSc1	sbormistr
(	(	kIx(	(
<g/>
†	†	k?	†
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
Oldřich	Oldřich	k1gMnSc1	Oldřich
Starý	Starý	k1gMnSc1	Starý
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
rektor	rektor	k1gMnSc1	rektor
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Rudolf	Rudolf	k1gMnSc1	Rudolf
Piskáček	Piskáček	k1gMnSc1	Piskáček
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
†	†	k?	†
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Fridolín	Fridolín	k1gMnSc1	Fridolín
Macháček	Macháček	k1gMnSc1	Macháček
<g/>
,	,	kIx,	,
plzeňský	plzeňský	k2eAgMnSc1d1	plzeňský
muzeolog	muzeolog	k1gMnSc1	muzeolog
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
(	(	kIx(	(
<g/>
†	†	k?	†
29	[number]	k4	29
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
března	březen	k1gInSc2	březen
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Otakar	Otakar	k1gMnSc1	Otakar
Pertold	Pertold	k1gMnSc1	Pertold
<g/>
,	,	kIx,	,
indolog	indolog	k1gMnSc1	indolog
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
etnolog	etnolog	k1gMnSc1	etnolog
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Vašata	Vašata	k1gFnSc1	Vašata
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
†	†	k?	†
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g />
.	.	kIx.	.
</s>
<s>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Bernardin	bernardin	k1gMnSc1	bernardin
Skácel	Skácel	k1gMnSc1	Skácel
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
(	(	kIx(	(
<g/>
†	†	k?	†
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Mezník	mezník	k1gInSc1	mezník
<g/>
,	,	kIx,	,
viceprezident	viceprezident	k1gMnSc1	viceprezident
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
zemský	zemský	k2eAgMnSc1d1	zemský
prezident	prezident	k1gMnSc1	prezident
<g/>
<g />
.	.	kIx.	.
</s>
<s>
]]	]]	k?	]]
(	(	kIx(	(
<g/>
†	†	k?	†
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Alois	Alois	k1gMnSc1	Alois
Reiser	Reiser	k1gMnSc1	Reiser
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
violoncellista	violoncellista	k1gMnSc1	violoncellista
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Otakar	Otakar	k1gMnSc1	Otakar
Vindyš	Vindyš	k1gMnSc1	Vindyš
<g/>
,	,	kIx,	,
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
mistr	mistr	k1gMnSc1	mistr
<g />
.	.	kIx.	.
</s>
<s>
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
(	(	kIx(	(
<g/>
†	†	k?	†
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Rudolf	Rudolf	k1gMnSc1	Rudolf
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
,	,	kIx,	,
mykolog	mykolog	k1gMnSc1	mykolog
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Milada	Milada	k1gFnSc1	Milada
Gampeová	Gampeová	k1gFnSc1	Gampeová
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
†	†	k?	†
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g />
.	.	kIx.	.
</s>
<s>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Chalupník	Chalupník	k1gMnSc1	Chalupník
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
a	a	k8xC	a
senátor	senátor	k1gMnSc1	senátor
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Moravské	moravský	k2eAgFnSc2d1	Moravská
Ostravy	Ostrava	k1gFnSc2	Ostrava
(	(	kIx(	(
<g/>
†	†	k?	†
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hněvkovský	Hněvkovský	k2eAgMnSc1d1	Hněvkovský
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
"	"	kIx"	"
<g/>
malíř	malíř	k1gMnSc1	malíř
Indie	Indie	k1gFnSc2	Indie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
†	†	k?	†
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
František	František	k1gMnSc1	František
Nábělek	Nábělek	k1gMnSc1	Nábělek
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Oskar	Oskar	k1gMnSc1	Oskar
Rosenfeld	Rosenfeld	k1gMnSc1	Rosenfeld
<g/>
,	,	kIx,	,
židovský	židovský	k2eAgMnSc1d1	židovský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
(	(	kIx(	(
<g/>
†	†	k?	†
srpen	srpen	k1gInSc1	srpen
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g />
.	.	kIx.	.
</s>
<s>
<g/>
května	květen	k1gInSc2	květen
–	–	k?	–
Otakar	Otakar	k1gMnSc1	Otakar
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Protektorátu	protektorát	k1gInSc2	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
(	(	kIx(	(
<g/>
†	†	k?	†
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Karel	Karel	k1gMnSc1	Karel
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
†	†	k?	†
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
Eduard	Eduard	k1gMnSc1	Eduard
Kühnl	Kühnl	k1gMnSc1	Kühnl
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
diplomat	diplomat	k1gMnSc1	diplomat
a	a	k8xC	a
esperantista	esperantista	k1gMnSc1	esperantista
(	(	kIx(	(
<g/>
†	†	k?	†
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Sadílek	Sadílek	k1gMnSc1	Sadílek
<g/>
,	,	kIx,	,
legionář	legionář	k1gMnSc1	legionář
a	a	k8xC	a
odbojář	odbojář	k1gMnSc1	odbojář
(	(	kIx(	(
<g/>
†	†	k?	†
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Rosipal	Rosipal	k1gMnSc1	Rosipal
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
designér	designér	k1gMnSc1	designér
(	(	kIx(	(
<g/>
†	†	k?	†
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1914	[number]	k4	1914
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Max	max	kA	max
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
pražský	pražský	k2eAgMnSc1d1	pražský
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgMnSc1d1	píšící
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
†	†	k?	†
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
československý	československý	k2eAgMnSc1d1	československý
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1948	[number]	k4	1948
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Prokop	Prokop	k1gMnSc1	Prokop
Maxa	Maxa	k1gMnSc1	Maxa
<g/>
,	,	kIx,	,
legionář	legionář	k1gMnSc1	legionář
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
(	(	kIx(	(
<g/>
†	†	k?	†
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Milada	Milada	k1gFnSc1	Milada
Špálová	Špálová	k1gFnSc1	Špálová
<g/>
,	,	kIx,	,
malířka	malířka	k1gFnSc1	malířka
(	(	kIx(	(
<g/>
†	†	k?	†
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Naske	Nask	k1gFnSc2	Nask
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
dekoratér	dekoratér	k1gMnSc1	dekoratér
a	a	k8xC	a
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
(	(	kIx(	(
<g/>
†	†	k?	†
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Ralph	Ralph	k1gMnSc1	Ralph
Benatzky	Benatzka	k1gFnSc2	Benatzka
<g/>
,	,	kIx,	,
česko-rakouský	českoakouský	k2eAgMnSc1d1	česko-rakouský
operetní	operetní	k2eAgMnSc1d1	operetní
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
†	†	k?	†
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Ladislav	Ladislav	k1gMnSc1	Ladislav
Prokeš	Prokeš	k1gMnSc1	Prokeš
<g/>
,	,	kIx,	,
šachový	šachový	k2eAgMnSc1d1	šachový
mistr	mistr	k1gMnSc1	mistr
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
†	†	k?	†
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Florian	Florian	k1gMnSc1	Florian
Zapletal	Zapletal	k1gMnSc1	Zapletal
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
voják	voják	k1gMnSc1	voják
(	(	kIx(	(
<g/>
†	†	k?	†
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Antonín	Antonín	k1gMnSc1	Antonín
Dolenský	Dolenský	k2eAgMnSc1d1	Dolenský
<g/>
,	,	kIx,	,
bibliograf	bibliograf	k1gMnSc1	bibliograf
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
(	(	kIx(	(
<g/>
†	†	k?	†
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Petr	Petr	k1gMnSc1	Petr
Zenkl	Zenkl	k1gInSc1	Zenkl
<g/>
,	,	kIx,	,
primátor	primátor	k1gMnSc1	primátor
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Rady	rada	k1gFnSc2	rada
svobodného	svobodný	k2eAgNnSc2d1	svobodné
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Miloš	Miloš	k1gMnSc1	Miloš
Kössler	Kössler	k1gMnSc1	Kössler
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
(	(	kIx(	(
<g/>
†	†	k?	†
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Jindřich	Jindřich	k1gMnSc1	Jindřich
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Myron	Myron	k1gMnSc1	Myron
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
†	†	k?	†
25	[number]	k4	25
<g />
.	.	kIx.	.
</s>
<s>
<g/>
ledna	leden	k1gInSc2	leden
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Gruss	Gruss	k1gInSc1	Gruss
<g/>
,	,	kIx,	,
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgMnSc1d1	sportovní
funkcionář	funkcionář	k1gMnSc1	funkcionář
a	a	k8xC	a
organizátor	organizátor	k1gMnSc1	organizátor
(	(	kIx(	(
<g/>
†	†	k?	†
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
František	František	k1gMnSc1	František
Bednář	Bednář	k1gMnSc1	Bednář
<g/>
,	,	kIx,	,
evangelický	evangelický	k2eAgMnSc1d1	evangelický
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
†	†	k?	†
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Jindřich	Jindřich	k1gMnSc1	Jindřich
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Ježek	Ježek	k1gMnSc1	Ježek
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
v	v	k7c6	v
protektorátní	protektorátní	k2eAgFnSc6d1	protektorátní
vládě	vláda	k1gFnSc6	vláda
Aloise	Alois	k1gMnSc2	Alois
Eliáše	Eliáš	k1gMnSc2	Eliáš
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Alois	Alois	k1gMnSc1	Alois
Tylínek	Tylínek	k1gMnSc1	Tylínek
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
papežský	papežský	k2eAgMnSc1d1	papežský
komoří	komoří	k1gMnSc1	komoří
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kubišta	Kubišta	k1gMnSc1	Kubišta
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
teoretik	teoretik	k1gMnSc1	teoretik
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Vašica	Vašica	k1gMnSc1	Vašica
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
filolog	filolog	k1gMnSc1	filolog
<g/>
,	,	kIx,	,
biblista	biblista	k1gMnSc1	biblista
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
(	(	kIx(	(
<g/>
†	†	k?	†
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Bedřich	Bedřich	k1gMnSc1	Bedřich
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
†	†	k?	†
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Emil	Emil	k1gMnSc1	Emil
Edgar	Edgar	k1gMnSc1	Edgar
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
†	†	k?	†
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Miloslav	Miloslav	k1gMnSc1	Miloslav
Jeník	Jeník	k1gMnSc1	Jeník
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
a	a	k8xC	a
operní	operní	k2eAgMnSc1d1	operní
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
†	†	k?	†
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rošický	Rošický	k2eAgMnSc1d1	Rošický
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
odbojář	odbojář	k1gMnSc1	odbojář
(	(	kIx(	(
<g/>
†	†	k?	†
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Váchal	Váchal	k1gMnSc1	Váchal
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1969	[number]	k4	1969
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Felix	Felix	k1gMnSc1	Felix
Weltsch	Weltsch	k1gMnSc1	Weltsch
<g/>
,	,	kIx,	,
česko-izraelský	českozraelský	k2eAgMnSc1d1	česko-izraelský
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
i	i	k9	i
hebrejsky	hebrejsky	k6eAd1	hebrejsky
píšící	píšící	k2eAgMnSc1d1	píšící
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
(	(	kIx(	(
<g/>
†	†	k?	†
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Jára	Jára	k1gMnSc1	Jára
Sedláček	Sedláček	k1gMnSc1	Sedláček
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
†	†	k?	†
28	[number]	k4	28
<g />
.	.	kIx.	.
</s>
<s>
<g/>
února	únor	k1gInSc2	únor
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Křovák	Křovák	k1gMnSc1	Křovák
<g/>
,	,	kIx,	,
geodet	geodet	k1gMnSc1	geodet
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Richard	Richard	k1gMnSc1	Richard
Weiner	Weiner	k1gMnSc1	Weiner
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Josef	Josef	k1gMnSc1	Josef
Rozsíval	Rozsíval	k1gMnSc1	Rozsíval
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Šidlík	Šidlík	k1gMnSc1	Šidlík
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Československých	československý	k2eAgFnPc2d1	Československá
legií	legie	k1gFnPc2	legie
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
(	(	kIx(	(
<g/>
†	†	k?	†
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Miloš	Miloš	k1gMnSc1	Miloš
Čeleda	Čeleda	k1gMnSc1	Čeleda
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Růžena	Růžena	k1gFnSc1	Růžena
Nasková	Nasková	k1gFnSc1	Nasková
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
†	†	k?	†
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
Marie	Marie	k1gFnSc1	Marie
Rýdlová	Rýdlová	k1gFnSc1	Rýdlová
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
†	†	k?	†
28	[number]	k4	28
<g />
.	.	kIx.	.
</s>
<s>
<g/>
října	říjen	k1gInSc2	říjen
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Bartovský	Bartovský	k2eAgMnSc1d1	Bartovský
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
†	†	k?	†
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
Petr	Petr	k1gMnSc1	Petr
Křička	Křička	k1gMnSc1	Křička
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
(	(	kIx(	(
<g/>
†	†	k?	†
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
Jiří	Jiří	k1gMnSc1	Jiří
<g />
.	.	kIx.	.
</s>
<s>
Horák	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
slavista	slavista	k1gMnSc1	slavista
<g/>
,	,	kIx,	,
folklorista	folklorista	k1gMnSc1	folklorista
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
(	(	kIx(	(
<g/>
†	†	k?	†
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Hostinský	hostinský	k1gMnSc1	hostinský
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
fyzik	fyzik	k1gMnSc1	fyzik
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Antonín	Antonín	k1gMnSc1	Antonín
Zápotocký	Zápotocký	k1gMnSc1	Zápotocký
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
odborový	odborový	k2eAgMnSc1d1	odborový
předák	předák	k1gMnSc1	předák
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Tomáš	Tomáš	k1gMnSc1	Tomáš
Dytrych	Dytrych	k1gMnSc1	Dytrych
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Košťál	Košťál	k1gMnSc1	Košťál
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
teorie	teorie	k1gFnSc2	teorie
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
stavby	stavba	k1gFnPc1	stavba
spalovacích	spalovací	k2eAgInPc2d1	spalovací
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
rektor	rektor	k1gMnSc1	rektor
ČVUT	ČVUT	kA	ČVUT
(	(	kIx(	(
<g/>
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Anděl	Anděl	k1gMnSc1	Anděl
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
(	(	kIx(	(
<g/>
†	†	k?	†
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Otakar	Otakar	k1gMnSc1	Otakar
Štáfl	Štáfl	k1gMnSc1	Štáfl
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
†	†	k?	†
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
listopad	listopad	k1gInSc1	listopad
–	–	k?	–
Milan	Milan	k1gMnSc1	Milan
Babuška	babuška	k1gFnSc1	babuška
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
†	†	k?	†
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Jeroným	Jeroným	k1gMnSc1	Jeroným
Jegír	Jegír	k1gMnSc1	Jegír
<g/>
,	,	kIx,	,
naturalistický	naturalistický	k2eAgMnSc1d1	naturalistický
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Ben	Ben	k1gInSc1	Ben
Cijon	Cijon	k1gMnSc1	Cijon
Dinur	Dinur	k1gMnSc1	Dinur
<g/>
,	,	kIx,	,
rabín	rabín	k1gMnSc1	rabín
a	a	k8xC	a
izraelský	izraelský	k2eAgMnSc1d1	izraelský
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
†	†	k?	†
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Alonzo	Alonza	k1gFnSc5	Alonza
G.	G.	kA	G.
Decker	Decker	k1gMnSc1	Decker
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
teosof	teosof	k1gMnSc1	teosof
(	(	kIx(	(
<g/>
†	†	k?	†
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Arthur	Arthur	k1gMnSc1	Arthur
Ransome	Ransom	k1gInSc5	Ransom
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Max	max	kA	max
von	von	k1gInSc1	von
Scheubner-Richter	Scheubner-Richter	k1gInSc1	Scheubner-Richter
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
diplomat	diplomat	k1gMnSc1	diplomat
(	(	kIx(	(
<g/>
†	†	k?	†
<g />
.	.	kIx.	.
</s>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Edward	Edward	k1gMnSc1	Edward
Sapir	Sapir	k1gMnSc1	Sapir
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
a	a	k8xC	a
antropolog	antropolog	k1gMnSc1	antropolog
(	(	kIx(	(
<g/>
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Auguste	August	k1gMnSc5	August
Piccard	Piccard	k1gMnSc1	Piccard
<g/>
,	,	kIx,	,
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
batyskafu	batyskaf	k1gInSc2	batyskaf
(	(	kIx(	(
<g/>
†	†	k?	†
24	[number]	k4	24
<g/>
<g />
.	.	kIx.	.
</s>
<s>
března	březen	k1gInSc2	březen
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Theodor	Theodor	k1gMnSc1	Theodor
Heuss	Heussa	k1gFnPc2	Heussa
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Jevgenij	Jevgenij	k1gFnSc2	Jevgenij
Zamjatin	Zamjatina	k1gFnPc2	Zamjatina
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Josep	Josep	k1gMnSc1	Josep
Carner	Carner	k1gMnSc1	Carner
<g/>
,	,	kIx,	,
katalánský	katalánský	k2eAgMnSc1d1	katalánský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
(	(	kIx(	(
<g/>
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
Max	Max	k1gMnSc1	Max
Beckmann	Beckmann	k1gMnSc1	Beckmann
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
prosince	prosinec	k1gInSc2	prosinec
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
Johan	Johan	k1gMnSc1	Johan
Laidoner	Laidoner	k1gMnSc1	Laidoner
<g/>
,	,	kIx,	,
estonský	estonský	k2eAgMnSc1d1	estonský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Alfred	Alfred	k1gMnSc1	Alfred
Gilbert	Gilbert	k1gMnSc1	Gilbert
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
ve	v	k7c6	v
skoku	skok	k1gInSc6	skok
o	o	k7c6	o
tyči	tyč	k1gFnSc6	tyč
(	(	kIx(	(
<g/>
†	†	k?	†
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
14	[number]	k4	14
<g/>
<g />
.	.	kIx.	.
</s>
<s>
února	únor	k1gInSc2	únor
–	–	k?	–
Kostas	Kostas	k1gInSc1	Kostas
Varnalis	Varnalis	k1gInSc1	Varnalis
<g/>
,	,	kIx,	,
řecký	řecký	k2eAgMnSc1d1	řecký
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Robert	Robert	k1gMnSc1	Robert
J.	J.	kA	J.
Flaherty	Flahert	k1gInPc1	Flahert
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
režisér	režisér	k1gMnSc1	režisér
(	(	kIx(	(
<g/>
†	†	k?	†
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Maciej	Maciej	k1gInSc1	Maciej
Rataj	Rataje	k1gInPc2	Rataje
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
prozatímní	prozatímní	k2eAgMnSc1d1	prozatímní
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
†	†	k?	†
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Constantin	Constantin	k1gMnSc1	Constantin
Constantinescu	Constantinescus	k1gInSc2	Constantinescus
<g/>
,	,	kIx,	,
rumunský	rumunský	k2eAgMnSc1d1	rumunský
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
vězeň	vězeň	k1gMnSc1	vězeň
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
(	(	kIx(	(
<g/>
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Kazimierz	Kazimierz	k1gInSc1	Kazimierz
Funk	funk	k1gInSc1	funk
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
polský	polský	k2eAgMnSc1d1	polský
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
vitamínu	vitamín	k1gInSc2	vitamín
B1	B1	k1gFnSc2	B1
(	(	kIx(	(
<g/>
†	†	k?	†
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Angelos	Angelos	k1gMnSc1	Angelos
Sikelianos	Sikelianos	k1gMnSc1	Sikelianos
<g/>
,	,	kIx,	,
řecký	řecký	k2eAgMnSc1d1	řecký
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
(	(	kIx(	(
<g/>
†	†	k?	†
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Alexandr	Alexandr	k1gMnSc1	Alexandr
Romanovič	Romanovič	k1gMnSc1	Romanovič
Běljajev	Běljajev	k1gMnSc1	Běljajev
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ruský	ruský	k2eAgMnSc1d1	ruský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Nachum	Nachum	k1gNnSc1	Nachum
Nir	Nir	k1gMnSc1	Nir
<g/>
,	,	kIx,	,
izraelský	izraelský	k2eAgMnSc1d1	izraelský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Knesetu	Kneset	k1gInSc2	Kneset
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
března	březen	k1gInSc2	březen
–	–	k?	–
George	Georg	k1gMnSc4	Georg
David	David	k1gMnSc1	David
Birkhoff	Birkhoff	k1gMnSc1	Birkhoff
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
matematik	matematik	k1gMnSc1	matematik
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
Philipp	Philipp	k1gMnSc1	Philipp
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
(	(	kIx(	(
<g/>
†	†	k?	†
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
Peter	Peter	k1gMnSc1	Peter
Debye	Deby	k1gFnSc2	Deby
<g/>
,	,	kIx,	,
holandský	holandský	k2eAgMnSc1d1	holandský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
teoretický	teoretický	k2eAgMnSc1d1	teoretický
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s>
chemii	chemie	k1gFnSc4	chemie
1936	[number]	k4	1936
(	(	kIx(	(
<g/>
†	†	k?	†
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Henri	Henr	k1gFnSc2	Henr
Queuille	Queuille	k1gFnSc2	Queuille
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
a	a	k8xC	a
několikanásobný	několikanásobný	k2eAgMnSc1d1	několikanásobný
ministr	ministr	k1gMnSc1	ministr
(	(	kIx(	(
<g/>
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
březen	březen	k1gInSc1	březen
–	–	k?	–
Nora	Nora	k1gFnSc1	Nora
Barnacleová	Barnacleová	k1gFnSc1	Barnacleová
<g/>
,	,	kIx,	,
milenka	milenka	k1gFnSc1	milenka
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
zdroj	zdroj	k1gInSc1	zdroj
inspirace	inspirace	k1gFnSc2	inspirace
Jamese	Jamese	k1gFnSc2	Jamese
Joyce	Joyce	k1gFnSc2	Joyce
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Isoroku	Isorok	k1gInSc2	Isorok
Jamamoto	Jamamota	k1gFnSc5	Jamamota
<g/>
,	,	kIx,	,
japonský	japonský	k2eAgMnSc1d1	japonský
admirál	admirál	k1gMnSc1	admirál
(	(	kIx(	(
<g/>
†	†	k?	†
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Andrej	Andrej	k1gMnSc1	Andrej
Bubnov	Bubnov	k1gInSc4	Bubnov
<g/>
,	,	kIx,	,
bolševický	bolševický	k2eAgMnSc1d1	bolševický
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
†	†	k?	†
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1938	[number]	k4	1938
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Bronisław	Bronisław	k1gMnSc1	Bronisław
Malinowski	Malinowsk	k1gMnPc1	Malinowsk
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
a	a	k8xC	a
britský	britský	k2eAgMnSc1d1	britský
antropolog	antropolog	k1gMnSc1	antropolog
<g/>
,	,	kIx,	,
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
etnograf	etnograf	k1gMnSc1	etnograf
(	(	kIx(	(
<g/>
†	†	k?	†
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Otto	Otto	k1gMnSc1	Otto
Fritz	Fritz	k1gMnSc1	Fritz
Meyerhof	Meyerhof	k1gMnSc1	Meyerhof
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
biochemik	biochemik	k1gMnSc1	biochemik
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
1922	[number]	k4	1922
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
†	†	k?	†
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Michel	Michel	k1gInSc1	Michel
Fingesten	Fingesten	k2eAgInSc1d1	Fingesten
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
malí	malý	k1gMnPc1	malý
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
(	(	kIx(	(
<g/>
†	†	k?	†
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Ludwig	Ludwig	k1gMnSc1	Ludwig
Meidner	Meidner	k1gMnSc1	Meidner
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
†	†	k?	†
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
<g />
.	.	kIx.	.
</s>
<s>
dubna	duben	k1gInSc2	duben
–	–	k?	–
Oliver	Oliver	k1gMnSc1	Oliver
Kirk	Kirk	k1gMnSc1	Kirk
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
boxer	boxer	k1gMnSc1	boxer
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
(	(	kIx(	(
<g/>
†	†	k?	†
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Otto	Otto	k1gMnSc1	Otto
Rank	rank	k1gInSc1	rank
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
(	(	kIx(	(
<g/>
†	†	k?	†
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Alfons	Alfons	k1gMnSc1	Alfons
Genrichovič	Genrichovič	k1gMnSc1	Genrichovič
Ukke-Ugovec	Ukke-Ugovec	k1gMnSc1	Ukke-Ugovec
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
generálmajor	generálmajor	k1gMnSc1	generálmajor
(	(	kIx(	(
<g/>
†	†	k?	†
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Arthur	Arthur	k1gMnSc1	Arthur
Wieferich	Wieferich	k1gMnSc1	Wieferich
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
matematik	matematik	k1gMnSc1	matematik
(	(	kIx(	(
<g/>
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Carl	Carl	k1gMnSc1	Carl
Osburn	Osburn	k1gMnSc1	Osburn
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
sportovní	sportovní	k2eAgMnSc1d1	sportovní
střelec	střelec	k1gMnSc1	střelec
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
(	(	kIx(	(
<g/>
†	†	k?	†
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g />
.	.	kIx.	.
</s>
<s>
<g/>
května	květen	k1gInSc2	květen
–	–	k?	–
Harry	Harra	k1gFnSc2	Harra
S.	S.	kA	S.
Truman	Truman	k1gMnSc1	Truman
<g/>
,	,	kIx,	,
33	[number]	k4	33
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
†	†	k?	†
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Claude	Claud	k1gInSc5	Claud
Dornier	Dornier	k1gMnSc1	Dornier
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
letecký	letecký	k2eAgMnSc1d1	letecký
konstruktér	konstruktér	k1gMnSc1	konstruktér
(	(	kIx(	(
<g/>
†	†	k?	†
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Georgij	Georgij	k1gFnSc1	Georgij
Brusilov	Brusilovo	k1gNnPc2	Brusilovo
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
polárník	polárník	k1gMnSc1	polárník
a	a	k8xC	a
badatel	badatel	k1gMnSc1	badatel
(	(	kIx(	(
<g/>
†	†	k?	†
?	?	kIx.	?
</s>
<s>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Corrado	Corrada	k1gFnSc5	Corrada
Gini	Gini	k1gNnPc6	Gini
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
sociolog	sociolog	k1gMnSc1	sociolog
a	a	k8xC	a
statistik	statistik	k1gMnSc1	statistik
(	(	kIx(	(
<g/>
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Clark	Clark	k1gInSc1	Clark
Leonard	Leonard	k1gMnSc1	Leonard
Hull	Hull	k1gMnSc1	Hull
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
psycholog	psycholog	k1gMnSc1	psycholog
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
<g />
.	.	kIx.	.
</s>
<s>
června	červen	k1gInSc2	červen
–	–	k?	–
Leo	Leo	k1gMnSc1	Leo
Birinski	Birinsk	k1gMnPc1	Birinsk
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
†	†	k?	†
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Burrill	Burrill	k1gMnSc1	Burrill
Bernard	Bernard	k1gMnSc1	Bernard
Crohn	Crohn	k1gMnSc1	Crohn
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
lékař	lékař	k1gMnSc1	lékař
(	(	kIx(	(
<g/>
†	†	k?	†
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Anton	Anton	k1gMnSc1	Anton
Drexler	Drexler	k1gMnSc1	Drexler
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
nacistické	nacistický	k2eAgFnSc2d1	nacistická
strany	strana	k1gFnSc2	strana
NSDAP	NSDAP	kA	NSDAP
(	(	kIx(	(
<g/>
†	†	k?	†
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
Gerald	Gerald	k1gMnSc1	Gerald
Gardner	Gardner	k1gMnSc1	Gardner
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
antropolog	antropolog	k1gMnSc1	antropolog
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
okultista	okultista	k1gMnSc1	okultista
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
Étienne	Étienn	k1gInSc5	Étienn
Gilson	Gilson	k1gMnSc1	Gilson
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
†	†	k?	†
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Leon	Leona	k1gFnPc2	Leona
Chwistek	Chwistka	k1gFnPc2	Chwistka
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
†	†	k?	†
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Édouard	Édouard	k1gMnSc1	Édouard
Daladier	Daladier	k1gMnSc1	Daladier
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
historik	historik	k1gMnSc1	historik
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Pierre-Henri	Pierre-Henr	k1gFnSc2	Pierre-Henr
Cami	Cam	k1gFnSc2	Cam
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
humorista	humorista	k1gMnSc1	humorista
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Claude	Claud	k1gInSc5	Claud
Auchinleck	Auchinleck	k1gMnSc1	Auchinleck
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
maršál	maršál	k1gMnSc1	maršál
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
†	†	k?	†
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Adolf	Adolf	k1gMnSc1	Adolf
Záturecký	Záturecký	k2eAgMnSc1d1	Záturecký
<g/>
,	,	kIx,	,
slovenský	slovenský	k2eAgMnSc1d1	slovenský
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
místopředseda	místopředseda	k1gMnSc1	místopředseda
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Daniel-Henry	Daniel-Henra	k1gFnSc2	Daniel-Henra
Kahnweiler	Kahnweiler	k1gMnSc1	Kahnweiler
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
obrazy	obraz	k1gInPc7	obraz
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vydavatel	vydavatel	k1gMnSc1	vydavatel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Gaston	Gaston	k1gInSc1	Gaston
Bachelard	Bachelard	k1gInSc1	Bachelard
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Pedro	Pedro	k1gNnSc1	Pedro
Henríquez	Henríquez	k1gMnSc1	Henríquez
Ureñ	Ureñ	k1gMnSc1	Ureñ
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
filolog	filolog	k1gMnSc1	filolog
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
z	z	k7c2	z
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
†	†	k?	†
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Lion	Lion	k1gMnSc1	Lion
Feuchtwanger	Feuchtwanger	k1gMnSc1	Feuchtwanger
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Bernard	Bernard	k1gMnSc1	Bernard
Parker	Parker	k1gMnSc1	Parker
Haigh	Haigh	k1gMnSc1	Haigh
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
profesor	profesor	k1gMnSc1	profesor
<g />
.	.	kIx.	.
</s>
<s>
aplikované	aplikovaný	k2eAgFnPc1d1	aplikovaná
mechaniky	mechanika	k1gFnPc1	mechanika
(	(	kIx(	(
<g/>
†	†	k?	†
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Louis	Louis	k1gMnSc1	Louis
B.	B.	kA	B.
Mayer	Mayer	k1gMnSc1	Mayer
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
(	(	kIx(	(
<g/>
†	†	k?	†
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Amedeo	Amedeo	k6eAd1	Amedeo
Modigliani	Modiglian	k1gMnPc1	Modiglian
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
(	(	kIx(	(
<g/>
†	†	k?	†
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Alfred	Alfred	k1gMnSc1	Alfred
Jansa	Jansa	k1gMnSc1	Jansa
<g/>
,	,	kIx,	,
náčelník	náčelník	k1gMnSc1	náčelník
štábu	štáb	k1gInSc2	štáb
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
†	†	k?	†
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Eduard	Eduard	k1gMnSc1	Eduard
Sasko-Kobursko-Gothajský	Sasko-Kobursko-Gothajský	k2eAgMnSc1d1	Sasko-Kobursko-Gothajský
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgMnSc1d1	poslední
sasko-kobursko-gothajský	saskooburskoothajský	k2eAgMnSc1d1	sasko-kobursko-gothajský
vévoda	vévoda	k1gMnSc1	vévoda
(	(	kIx(	(
<g/>
†	†	k?	†
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
<g />
.	.	kIx.	.
</s>
<s>
července	červenec	k1gInSc2	červenec
–	–	k?	–
Emil	Emil	k1gMnSc1	Emil
Jannings	Jannings	k1gInSc1	Jannings
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
herec	herec	k1gMnSc1	herec
(	(	kIx(	(
<g/>
†	†	k?	†
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Davidson	Davidson	k1gMnSc1	Davidson
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
paleoantropolog	paleoantropolog	k1gMnSc1	paleoantropolog
(	(	kIx(	(
<g/>
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Boris	Boris	k1gMnSc1	Boris
Vladimirovič	Vladimirovič	k1gMnSc1	Vladimirovič
Asafjev	Asafjev	k1gMnSc1	Asafjev
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ruský	ruský	k2eAgMnSc1d1	ruský
muzikolog	muzikolog	k1gMnSc1	muzikolog
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Georges	Georges	k1gMnSc1	Georges
Duhamel	Duhamel	k1gMnSc1	Duhamel
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
myslitel	myslitel	k1gMnSc1	myslitel
(	(	kIx(	(
<g/>
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Rómulo	Rómula	k1gFnSc5	Rómula
Gallegos	Gallegos	k1gInSc1	Gallegos
<g/>
,	,	kIx,	,
venezuelský	venezuelský	k2eAgInSc1d1	venezuelský
<g />
.	.	kIx.	.
</s>
<s>
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
†	†	k?	†
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Marg	Marga	k1gFnPc2	Marga
Moll	moll	k1gNnPc2	moll
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
malířka	malířka	k1gFnSc1	malířka
(	(	kIx(	(
<g/>
†	†	k?	†
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Henri	Henri	k1gNnSc1	Henri
Cornet	Cornet	k1gMnSc1	Cornet
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
cyklista	cyklista	k1gMnSc1	cyklista
(	(	kIx(	(
<g/>
†	†	k?	†
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
Sigmund	Sigmund	k1gMnSc1	Sigmund
<g />
.	.	kIx.	.
</s>
<s>
Mowinckel	Mowinckel	k1gMnSc1	Mowinckel
<g/>
,	,	kIx,	,
norský	norský	k2eAgMnSc1d1	norský
teolog	teolog	k1gMnSc1	teolog
(	(	kIx(	(
<g/>
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Hugo	Hugo	k1gMnSc1	Hugo
Gernsback	Gernsback	k1gMnSc1	Gernsback
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
editor	editor	k1gMnSc1	editor
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
(	(	kIx(	(
<g/>
†	†	k?	†
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Rudolf	Rudolf	k1gMnSc1	Rudolf
Bultmann	Bultmann	k1gMnSc1	Bultmann
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
<g />
.	.	kIx.	.
</s>
<s>
luterský	luterský	k2eAgMnSc1d1	luterský
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
biblista	biblista	k1gMnSc1	biblista
(	(	kIx(	(
<g/>
†	†	k?	†
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Earl	earl	k1gMnSc1	earl
Derr	Derr	k1gMnSc1	Derr
Biggers	Biggers	k1gInSc4	Biggers
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
detektivních	detektivní	k2eAgInPc2d1	detektivní
románů	román	k1gInPc2	román
(	(	kIx(	(
<g/>
†	†	k?	†
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Alekhsandre	Alekhsandr	k1gInSc5	Alekhsandr
Abašeli	Abašeli	k1gMnSc1	Abašeli
<g/>
,	,	kIx,	,
gruzínský	gruzínský	k2eAgMnSc1d1	gruzínský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Vincent	Vincent	k1gMnSc1	Vincent
Auriol	Auriola	k1gFnPc2	Auriola
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
†	†	k?	†
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Sergej	Sergej	k1gMnSc1	Sergej
Josifovič	Josifovič	k1gMnSc1	Josifovič
Karcevskij	Karcevskij	k1gMnSc1	Karcevskij
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
lingvista	lingvista	k1gMnSc1	lingvista
(	(	kIx(	(
<g/>
†	†	k?	†
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
Peter	Peter	k1gMnSc1	Peter
Wust	Wust	k1gMnSc1	Wust
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
německý	německý	k2eAgMnSc1d1	německý
filosof	filosof	k1gMnSc1	filosof
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Theodor	Theodor	k1gMnSc1	Theodor
Svedberg	Svedberg	k1gMnSc1	Svedberg
<g/>
,	,	kIx,	,
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
(	(	kIx(	(
<g/>
†	†	k?	†
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Germana	German	k1gMnSc2	German
Toskánská	toskánský	k2eAgFnSc1d1	Toskánská
<g/>
,	,	kIx,	,
rakouská	rakouský	k2eAgFnSc1d1	rakouská
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
toskánská	toskánský	k2eAgFnSc1d1	Toskánská
princezna	princezna	k1gFnSc1	princezna
(	(	kIx(	(
<g/>
†	†	k?	†
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
–	–	k?	–
Constantin	Constantin	k1gInSc1	Constantin
von	von	k1gInSc1	von
Mitschke-Collande	Mitschke-Colland	k1gInSc5	Mitschke-Colland
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Dénes	Dénes	k1gMnSc1	Dénes
Kőnig	Kőnig	k1gMnSc1	Kőnig
<g/>
,	,	kIx,	,
maďarský	maďarský	k2eAgMnSc1d1	maďarský
matematik	matematik	k1gMnSc1	matematik
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
†	†	k?	†
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
Gustave	Gustav	k1gMnSc5	Gustav
Garrigou	Garriga	k1gFnSc7	Garriga
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
cyklista	cyklista	k1gMnSc1	cyklista
(	(	kIx(	(
<g/>
†	†	k?	†
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
İ	İ	k?	İ
İ	İ	k?	İ
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
turecký	turecký	k2eAgMnSc1d1	turecký
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
†	†	k?	†
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Hugo	Hugo	k1gMnSc1	Hugo
Schmeisser	Schmeisser	k1gMnSc1	Schmeisser
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
<g />
.	.	kIx.	.
</s>
<s>
konstruktér	konstruktér	k1gMnSc1	konstruktér
pěchotních	pěchotní	k2eAgFnPc2d1	pěchotní
zbraní	zbraň	k1gFnPc2	zbraň
(	(	kIx(	(
<g/>
†	†	k?	†
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Tanzan	Tanzan	k1gMnSc1	Tanzan
Išibaši	Išibaš	k1gMnPc1	Išibaš
<g/>
,	,	kIx,	,
japonský	japonský	k2eAgMnSc1d1	japonský
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
(	(	kIx(	(
<g/>
†	†	k?	†
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Forrest	Forrest	k1gMnSc1	Forrest
Smithson	Smithson	k1gMnSc1	Smithson
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
110	[number]	k4	110
metrů	metr	k1gInPc2	metr
překážek	překážka	k1gFnPc2	překážka
(	(	kIx(	(
<g/>
†	†	k?	†
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
David	David	k1gMnSc1	David
Katz	Katz	k1gMnSc1	Katz
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
psycholog	psycholog	k1gMnSc1	psycholog
(	(	kIx(	(
<g/>
†	†	k?	†
únor	únor	k1gInSc1	únor
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Lloyd	Lloyd	k1gMnSc1	Lloyd
Spooner	Spooner	k1gMnSc1	Spooner
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
sportovní	sportovní	k2eAgMnSc1d1	sportovní
střelec	střelec	k1gMnSc1	střelec
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
(	(	kIx(	(
<g/>
†	†	k?	†
<g />
.	.	kIx.	.
</s>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Józef	Józef	k1gMnSc1	Józef
Unrug	Unrug	k1gMnSc1	Unrug
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
viceadmirál	viceadmirál	k1gMnSc1	viceadmirál
(	(	kIx(	(
<g/>
†	†	k?	†
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Helene	Helen	k1gInSc5	Helen
Deutschová	Deutschový	k2eAgFnSc1d1	Deutschový
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
psychoanalytička	psychoanalytička	k1gFnSc1	psychoanalytička
(	(	kIx(	(
<g/>
†	†	k?	†
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g />
.	.	kIx.	.
</s>
<s>
<g/>
října	říjen	k1gInSc2	říjen
Friedrich	Friedrich	k1gMnSc1	Friedrich
Bergius	Bergius	k1gMnSc1	Bergius
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
(	(	kIx(	(
<g/>
†	†	k?	†
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
Eleanor	Eleanor	k1gMnSc1	Eleanor
Rooseveltová	Rooseveltová	k1gFnSc1	Rooseveltová
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
32	[number]	k4	32
<g/>
.	.	kIx.	.
prezidenta	prezident	k1gMnSc4	prezident
USA	USA	kA	USA
Franklina	Franklin	k2eAgMnSc4d1	Franklin
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
(	(	kIx(	(
<g/>
†	†	k?	†
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
<g />
.	.	kIx.	.
</s>
<s>
října	říjen	k1gInSc2	říjen
–	–	k?	–
Zinovij	Zinovij	k1gFnSc1	Zinovij
Peškov	Peškov	k1gInSc1	Peškov
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Alexandre	Alexandr	k1gInSc5	Alexandr
Mercereau	Mercereaum	k1gNnSc6	Mercereaum
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
symbolistický	symbolistický	k2eAgMnSc1d1	symbolistický
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Eivind	Eivind	k1gMnSc1	Eivind
Berggrav	Berggrav	k1gMnSc1	Berggrav
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
norský	norský	k2eAgMnSc1d1	norský
luteránský	luteránský	k2eAgMnSc1d1	luteránský
biskup	biskup	k1gMnSc1	biskup
(	(	kIx(	(
<g/>
†	†	k?	†
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Dušan	Dušan	k1gMnSc1	Dušan
Simović	Simović	k1gMnSc1	Simović
<g/>
,	,	kIx,	,
jugoslávský	jugoslávský	k2eAgMnSc1d1	jugoslávský
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
exilový	exilový	k2eAgMnSc1d1	exilový
premiér	premiér	k1gMnSc1	premiér
(	(	kIx(	(
<g/>
†	†	k?	†
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Annibale	Annibal	k1gInSc6	Annibal
Bergonzoli	Bergonzole	k1gFnSc4	Bergonzole
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
generál	generál	k1gMnSc1	generál
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
†	†	k?	†
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Leo	Leo	k1gMnSc1	Leo
Perutz	Perutz	k1gMnSc1	Perutz
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
(	(	kIx(	(
<g/>
†	†	k?	†
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
Helena	Helena	k1gFnSc1	Helena
Karađorđević	Karađorđević	k1gFnSc1	Karađorđević
<g/>
,	,	kIx,	,
srbská	srbský	k2eAgFnSc1d1	Srbská
princezna	princezna	k1gFnSc1	princezna
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
<g />
.	.	kIx.	.
</s>
<s>
srbského	srbský	k2eAgMnSc4d1	srbský
krále	král	k1gMnSc4	král
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
(	(	kIx(	(
<g/>
†	†	k?	†
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
Miloš	Miloš	k1gMnSc1	Miloš
Vančo	Vančo	k1gMnSc1	Vančo
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
autonomní	autonomní	k2eAgFnSc2d1	autonomní
slovenské	slovenský	k2eAgFnSc2d1	slovenská
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
†	†	k?	†
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Hermann	Hermann	k1gMnSc1	Hermann
Rorschach	Rorschach	k1gMnSc1	Rorschach
<g/>
,	,	kIx,	,
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
psycholog	psycholog	k1gMnSc1	psycholog
a	a	k8xC	a
psychiatr	psychiatr	k1gMnSc1	psychiatr
(	(	kIx(	(
<g/>
†	†	k?	†
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
dubna	duben	k1gInSc2	duben
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Zofia	Zofia	k1gFnSc1	Zofia
Nałkowska	Nałkowska	k1gFnSc1	Nałkowska
<g/>
,	,	kIx,	,
polská	polský	k2eAgFnSc1d1	polská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
(	(	kIx(	(
<g/>
†	†	k?	†
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
George	George	k1gInSc1	George
Goulding	Goulding	k1gInSc1	Goulding
<g/>
,	,	kIx,	,
kanadský	kanadský	k2eAgMnSc1d1	kanadský
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
chůzi	chůze	k1gFnSc6	chůze
na	na	k7c4	na
10	[number]	k4	10
kilometrů	kilometr	k1gInPc2	kilometr
(	(	kIx(	(
<g/>
†	†	k?	†
31	[number]	k4	31
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
ledna	leden	k1gInSc2	leden
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
Jack	Jack	k1gMnSc1	Jack
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
velšský	velšský	k2eAgMnSc1d1	velšský
romanopisec	romanopisec	k1gMnSc1	romanopisec
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
(	(	kIx(	(
<g/>
†	†	k?	†
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Jicchak	Jicchak	k1gInSc1	Jicchak
Ben	Ben	k1gInSc1	Ben
Cvi	Cvi	k1gFnSc1	Cvi
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Izraele	Izrael	k1gInSc2	Izrael
(	(	kIx(	(
<g/>
†	†	k?	†
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Karl	Karl	k1gMnSc1	Karl
Schmidt-Rottluff	Schmidt-Rottluff	k1gMnSc1	Schmidt-Rottluff
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
expresionistický	expresionistický	k2eAgMnSc1d1	expresionistický
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
(	(	kIx(	(
<g/>
†	†	k?	†
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Rádžéndra	Rádžéndra	k1gFnSc1	Rádžéndra
Prasád	Prasáda	k1gFnPc2	Prasáda
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
indický	indický	k2eAgMnSc1d1	indický
prezident	prezident	k1gMnSc1	prezident
(	(	kIx(	(
<g/>
†	†	k?	†
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Petru	Petr	k1gMnSc6	Petr
Groza	Groza	k1gFnSc1	Groza
<g/>
,	,	kIx,	,
rumunský	rumunský	k2eAgMnSc1d1	rumunský
prezident	prezident	k1gMnSc1	prezident
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
†	†	k?	†
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
10	[number]	k4	10
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Albert	Albert	k1gMnSc1	Albert
Steffen	Steffen	k2eAgMnSc1d1	Steffen
<g/>
,	,	kIx,	,
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
†	†	k?	†
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Evelyn	Evelyna	k1gFnPc2	Evelyna
Nesbitová	Nesbitový	k2eAgFnSc1d1	Nesbitová
<g/>
,	,	kIx,	,
americká	americký	k2eAgFnSc1d1	americká
modelka	modelka	k1gFnSc1	modelka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
†	†	k?	†
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Joseph	Joseph	k1gInSc1	Joseph
Pholien	Pholien	k1gInSc1	Pholien
<g/>
,	,	kIx,	,
premiér	premiér	k1gMnSc1	premiér
Belgie	Belgie	k1gFnSc2	Belgie
(	(	kIx(	(
<g/>
†	†	k?	†
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Hideki	Hideki	k1gNnSc1	Hideki
Tódžó	Tódžó	k1gMnSc1	Tódžó
<g/>
,	,	kIx,	,
japonský	japonský	k2eAgMnSc1d1	japonský
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
†	†	k?	†
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
George	George	k1gFnPc2	George
W.	W.	kA	W.
Ackerman	Ackerman	k1gMnSc1	Ackerman
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
vládní	vládní	k2eAgMnSc1d1	vládní
fotograf	fotograf	k1gMnSc1	fotograf
(	(	kIx(	(
<g/>
†	†	k?	†
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Philipp	Philipp	k1gMnSc1	Philipp
Bauknecht	Bauknecht	k1gMnSc1	Bauknecht
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
experionistický	experionistický	k2eAgMnSc1d1	experionistický
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
†	†	k?	†
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Nicolaas	Nicolaas	k1gInSc1	Nicolaas
G.	G.	kA	G.
W.	W.	kA	W.
H.	H.	kA	H.
Beeger	Beeger	k1gMnSc1	Beeger
<g/>
,	,	kIx,	,
holandský	holandský	k2eAgMnSc1d1	holandský
matematik	matematik	k1gMnSc1	matematik
(	(	kIx(	(
<g/>
†	†	k?	†
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
David	David	k1gMnSc1	David
Bloch	Bloch	k1gMnSc1	Bloch
Blumenfeld	Blumenfeld	k1gMnSc1	Blumenfeld
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
starosta	starosta	k1gMnSc1	starosta
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc3	Aviv
(	(	kIx(	(
<g/>
†	†	k?	†
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Naftule	Naftule	k1gFnSc1	Naftule
Brandwein	Brandwein	k1gMnSc1	Brandwein
<g/>
,	,	kIx,	,
klezmerový	klezmerový	k2eAgMnSc1d1	klezmerový
klarinetista	klarinetista	k1gMnSc1	klarinetista
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
(	(	kIx(	(
<g/>
†	†	k?	†
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Luis	Luisa	k1gFnPc2	Luisa
Ramón	Ramón	k1gMnSc1	Ramón
Marín	Marína	k1gFnPc2	Marína
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
fotograf	fotograf	k1gMnSc1	fotograf
a	a	k8xC	a
reportér	reportér	k1gMnSc1	reportér
(	(	kIx(	(
<g/>
†	†	k?	†
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Vasilij	Vasilij	k1gMnSc1	Vasilij
Slesarjev	Slesarjev	k1gMnSc1	Slesarjev
<g/>
,	,	kIx,	,
ruským	ruský	k2eAgMnPc3d1	ruský
letecký	letecký	k2eAgMnSc1d1	letecký
konstruktér	konstruktér	k1gMnSc1	konstruktér
(	(	kIx(	(
<g/>
†	†	k?	†
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Paul	Paul	k1gMnSc1	Paul
Thalheimer	Thalheimer	k1gMnSc1	Thalheimer
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
†	†	k?	†
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Automatický	automatický	k2eAgMnSc1d1	automatický
abecedně	abecedně	k6eAd1	abecedně
řazený	řazený	k2eAgInSc4d1	řazený
seznam	seznam	k1gInSc4	seznam
existujících	existující	k2eAgFnPc2d1	existující
biografií	biografie	k1gFnPc2	biografie
viz	vidět	k5eAaImRp2nS	vidět
Kategorie	kategorie	k1gFnSc1	kategorie
<g/>
:	:	kIx,	:
<g/>
Úmrtí	úmrtí	k1gNnSc1	úmrtí
1884	[number]	k4	1884
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
genetiky	genetika	k1gFnSc2	genetika
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
dubna	duben	k1gInSc2	duben
–	–	k?	–
Bedřich	Bedřich	k1gMnSc1	Bedřich
Hoppe	Hopp	k1gInSc5	Hopp
<g/>
,	,	kIx,	,
organizátor	organizátor	k1gMnSc1	organizátor
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Carl	Carl	k1gMnSc1	Carl
Budischowsky	Budischowska	k1gFnSc2	Budischowska
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
podnikatel	podnikatel	k1gMnSc1	podnikatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
František	František	k1gMnSc1	František
Bedřich	Bedřich	k1gMnSc1	Bedřich
Kott	Kott	k1gMnSc1	Kott
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
varhaník	varhaník	k1gMnSc1	varhaník
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1808	[number]	k4	1808
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1824	[number]	k4	1824
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Larisch-Mönnich	Larisch-Mönnich	k1gMnSc1	Larisch-Mönnich
<g/>
,	,	kIx,	,
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Václav	Václav	k1gMnSc1	Václav
Beneš	Beneš	k1gMnSc1	Beneš
Třebízský	Třebízský	k1gMnSc1	Třebízský
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
František	František	k1gMnSc1	František
Tomáš	Tomáš	k1gMnSc1	Tomáš
Bratránek	bratránek	k1gMnSc1	bratránek
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Miroslav	Miroslav	k1gMnSc1	Miroslav
Tyrš	Tyrš	k1gMnSc1	Tyrš
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
estetik	estetik	k1gMnSc1	estetik
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
dějin	dějiny	k1gFnPc2	dějiny
na	na	k7c4	na
UK	UK	kA	UK
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
Sokola	Sokol	k1gMnSc2	Sokol
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Jakub	Jakub	k1gMnSc1	Jakub
Bursa	bursa	k1gFnSc1	bursa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
jihočeský	jihočeský	k2eAgMnSc1d1	jihočeský
lidový	lidový	k2eAgMnSc1d1	lidový
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
zedník	zedník	k1gMnSc1	zedník
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Saturnin	Saturnin	k1gMnSc1	Saturnin
Heller	Heller	k1gMnSc1	Heller
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Eduard	Eduard	k1gMnSc1	Eduard
Claudi	Claud	k1gMnPc1	Claud
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g />
.	.	kIx.	.
</s>
<s>
německé	německý	k2eAgFnPc4d1	německá
národnosti	národnost	k1gFnPc4	národnost
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Filip	Filip	k1gMnSc1	Filip
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kodym	Kodym	k1gInSc1	Kodym
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
<g/>
,	,	kIx,	,
odborný	odborný	k2eAgMnSc1d1	odborný
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1811	[number]	k4	1811
<g/>
)	)	kIx)	)
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Maixner	Maixner	k1gMnSc1	Maixner
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Slavoj	Slavoj	k1gMnSc1	Slavoj
Amerling	Amerling	k1gInSc1	Amerling
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1807	[number]	k4	1807
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
František	František	k1gMnSc1	František
Doucha	Doucha	k1gMnSc1	Doucha
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g />
.	.	kIx.	.
</s>
<s>
<g/>
srpna	srpen	k1gInSc2	srpen
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Franz	Franz	k1gMnSc1	Franz
Klier	Klier	k1gMnSc1	Klier
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1819	[number]	k4	1819
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Franz	Franz	k1gMnSc1	Franz
Jordan	Jordan	k1gMnSc1	Jordan
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
listopadu	listopad	k1gInSc2	listopad
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Johann	Johann	k1gMnSc1	Johann
Gerhard	Gerhard	k1gMnSc1	Gerhard
Oncken	Oncken	k2eAgMnSc1d1	Oncken
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
baptistický	baptistický	k2eAgMnSc1d1	baptistický
kazatel	kazatel	k1gMnSc1	kazatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Daniel	Daniel	k1gMnSc1	Daniel
Harrwitz	Harrwitz	k1gMnSc1	Harrwitz
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
šachový	šachový	k2eAgMnSc1d1	šachový
mistr	mistr	k1gMnSc1	mistr
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
17	[number]	k4	17
<g />
.	.	kIx.	.
</s>
<s>
<g/>
ledna	leden	k1gInSc2	leden
–	–	k?	–
Hermann	Hermann	k1gMnSc1	Hermann
Schlegel	Schlegel	k1gMnSc1	Schlegel
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
zoolog	zoolog	k1gMnSc1	zoolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
–	–	k?	–
Auguste	August	k1gMnSc5	August
Franchomme	Franchomme	k1gMnSc5	Franchomme
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
violoncellista	violoncellista	k1gMnSc1	violoncellista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1808	[number]	k4	1808
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Auguste	August	k1gMnSc5	August
<g />
.	.	kIx.	.
</s>
<s>
Mestral	Mestrat	k5eAaPmAgMnS	Mestrat
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fotograf	fotograf	k1gMnSc1	fotograf
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
–	–	k?	–
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
<g/>
,	,	kIx,	,
portugalská	portugalský	k2eAgFnSc1d1	portugalská
a	a	k8xC	a
saská	saský	k2eAgFnSc1d1	saská
princezna	princezna	k1gFnSc1	princezna
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Bernardo	Bernardo	k1gNnSc1	Bernardo
Guimarã	Guimarã	k1gMnSc1	Guimarã
<g/>
,	,	kIx,	,
brazilský	brazilský	k2eAgMnSc1d1	brazilský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1825	[number]	k4	1825
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Elias	Elias	k1gMnSc1	Elias
Lönnrot	Lönnrot	k1gMnSc1	Lönnrot
<g/>
,	,	kIx,	,
finský	finský	k2eAgMnSc1d1	finský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
–	–	k?	–
Leopold	Leopold	k1gMnSc1	Leopold
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Albany	Albana	k1gFnSc2	Albana
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
britské	britský	k2eAgFnSc2d1	britská
královny	královna	k1gFnSc2	královna
Viktorie	Viktoria	k1gFnSc2	Viktoria
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1853	[number]	k4	1853
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Anna	Anna	k1gFnSc1	Anna
Ottendorfer	Ottendorfer	k1gInSc1	Ottendorfer
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
filantropka	filantropka	k1gFnSc1	filantropka
a	a	k8xC	a
vydavatelka	vydavatelka	k1gFnSc1	vydavatelka
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
–	–	k?	–
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Dumas	Dumas	k1gMnSc1	Dumas
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
července	červenec	k1gInSc2	červenec
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Marie	Marie	k1gFnSc1	Marie
Anna	Anna	k1gFnSc1	Anna
Savojská	savojský	k2eAgFnSc1d1	Savojská
<g/>
,	,	kIx,	,
rakouská	rakouský	k2eAgFnSc1d1	rakouská
císařovna	císařovna	k1gFnSc1	císařovna
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
Charles	Charles	k1gMnSc1	Charles
Adolphe	Adolphe	k1gNnSc1	Adolphe
Wurtz	Wurtz	k1gMnSc1	Wurtz
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1817	[number]	k4	1817
<g/>
)	)	kIx)	)
31	[number]	k4	31
<g/>
<g />
.	.	kIx.	.
</s>
<s>
května	květen	k1gInSc2	květen
–	–	k?	–
Bethel	Bethel	k1gMnSc1	Bethel
Henry	Henry	k1gMnSc1	Henry
Strousberg	Strousberg	k1gMnSc1	Strousberg
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
podnikatel	podnikatel	k1gMnSc1	podnikatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Arnošt	Arnošt	k1gMnSc1	Arnošt
Smoler	Smoler	k1gMnSc1	Smoler
<g/>
,	,	kIx,	,
lužickosrbský	lužickosrbský	k2eAgMnSc1d1	lužickosrbský
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1816	[number]	k4	1816
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Thora	Thora	k1gMnSc1	Thora
Hallager	Hallager	k1gMnSc1	Hallager
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
dánská	dánský	k2eAgFnSc1d1	dánská
fotografka	fotografka	k1gFnSc1	fotografka
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Alexandr	Alexandr	k1gMnSc1	Alexandr
Oranžský	oranžský	k2eAgMnSc1d1	oranžský
<g/>
,	,	kIx,	,
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
princ	princ	k1gMnSc1	princ
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Oranje-Nassau	Oranje-Nassaus	k1gInSc2	Oranje-Nassaus
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1851	[number]	k4	1851
<g/>
)	)	kIx)	)
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Victor	Victor	k1gMnSc1	Victor
Massé	Massý	k2eAgFnSc2d1	Massý
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
–	–	k?	–
Adrian	Adrian	k1gMnSc1	Adrian
Ludwig	Ludwig	k1gMnSc1	Ludwig
Richter	Richter	k1gMnSc1	Richter
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
Paul	Paula	k1gFnPc2	Paula
Morphy	Morpha	k1gFnSc2	Morpha
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
šachista	šachista	k1gMnSc1	šachista
(	(	kIx(	(
<g/>
*	*	kIx~	*
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1837	[number]	k4	1837
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Karl	Karl	k1gMnSc1	Karl
Richard	Richard	k1gMnSc1	Richard
Lepsius	Lepsius	k1gMnSc1	Lepsius
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
německé	německý	k2eAgFnSc2d1	německá
egyptologie	egyptologie	k1gFnSc2	egyptologie
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
Gustave	Gustav	k1gMnSc5	Gustav
Le	Le	k1gMnSc5	Le
Gray	Graa	k1gFnSc2	Graa
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fotograf	fotograf	k1gMnSc1	fotograf
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1820	[number]	k4	1820
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Paul	Paul	k1gMnSc1	Paul
Abadie	Abadie	k1gFnSc2	Abadie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
francouzský	francouzský	k2eAgMnSc1d1	francouzský
architekt	architekt	k1gMnSc1	architekt
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Ödön	Ödön	k1gMnSc1	Ödön
Tömösváry	Tömösvár	k1gInPc4	Tömösvár
<g/>
,	,	kIx,	,
maďarský	maďarský	k2eAgMnSc1d1	maďarský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1852	[number]	k4	1852
<g/>
)	)	kIx)	)
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Giuseppe	Giusepp	k1gInSc5	Giusepp
De	De	k?	De
Nittis	Nittis	k1gInSc1	Nittis
<g/>
,	,	kIx,	,
italský	italský	k2eAgMnSc1d1	italský
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
<g />
.	.	kIx.	.
</s>
<s>
února	únor	k1gInSc2	únor
1846	[number]	k4	1846
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
–	–	k?	–
Antonio	Antonio	k1gMnSc1	Antonio
García	García	k1gMnSc1	García
Gutiérrez	Gutiérrez	k1gMnSc1	Gutiérrez
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
romantický	romantický	k2eAgMnSc1d1	romantický
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Engerth	Engerth	k1gMnSc1	Engerth
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
stavitel	stavitel	k1gMnSc1	stavitel
železnic	železnice	k1gFnPc2	železnice
a	a	k8xC	a
konstruktér	konstruktér	k1gMnSc1	konstruktér
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
<g />
.	.	kIx.	.
</s>
<s>
května	květen	k1gInSc2	květen
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Leopold	Leopold	k1gMnSc1	Leopold
Fitzinger	Fitzinger	k1gMnSc1	Fitzinger
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
zoolog	zoolog	k1gMnSc1	zoolog
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
–	–	k?	–
Eugè	Eugè	k1gFnSc1	Eugè
Bourdon	bourdon	k1gInSc1	bourdon
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1808	[number]	k4	1808
<g/>
)	)	kIx)	)
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
října	říjen	k1gInSc2	říjen
–	–	k?	–
Hans	Hans	k1gMnSc1	Hans
Makart	Makart	k1gInSc1	Makart
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1840	[number]	k4	1840
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Jean	Jean	k1gMnSc1	Jean
Becker	Becker	k1gMnSc1	Becker
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
houslista	houslista	k1gMnSc1	houslista
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1833	[number]	k4	1833
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Macusaburó	Macusaburó	k1gMnSc2	Macusaburó
Jokojama	Jokojam	k1gMnSc2	Jokojam
<g/>
,	,	kIx,	,
japonský	japonský	k2eAgMnSc1d1	japonský
fotograf	fotograf	k1gMnSc1	fotograf
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Vilém	Vilém	k1gMnSc1	Vilém
Brunšvický	brunšvický	k2eAgMnSc1d1	brunšvický
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Braunschweigu	Braunschweig	k1gInSc2	Braunschweig
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1806	[number]	k4	1806
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
–	–	k?	–
Adolf	Adolf	k1gMnSc1	Adolf
Kriegs-Au	Kriegs-Aus	k1gInSc2	Kriegs-Aus
<g/>
,	,	kIx,	,
předlitavský	předlitavský	k2eAgMnSc1d1	předlitavský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
státní	státní	k2eAgMnSc1d1	státní
úředník	úředník	k1gMnSc1	úředník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1819	[number]	k4	1819
<g/>
)	)	kIx)	)
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Alfred	Alfred	k1gMnSc1	Alfred
Brehm	Brehm	k1gMnSc1	Brehm
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
zoolog	zoolog	k1gMnSc1	zoolog
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
(	(	kIx(	(
<g/>
*	*	kIx~	*
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
František	františek	k1gInSc1	františek
Chvostek	chvostek	k1gInSc1	chvostek
starší	starý	k2eAgInSc1d2	starší
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
vojenský	vojenský	k2eAgMnSc1d1	vojenský
lékař	lékař	k1gMnSc1	lékař
moravského	moravský	k2eAgInSc2d1	moravský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
<g />
.	.	kIx.	.
</s>
<s>
tzv.	tzv.	kA	tzv.
Chvostkův	Chvostkův	k2eAgInSc4d1	Chvostkův
příznak	příznak	k1gInSc4	příznak
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1835	[number]	k4	1835
<g/>
)	)	kIx)	)
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
–	–	k?	–
Eduard	Eduard	k1gMnSc1	Eduard
Rüppell	Rüppell	k1gMnSc1	Rüppell
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
(	(	kIx(	(
<g/>
*	*	kIx~	*
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Adolph	Adolph	k1gMnSc1	Adolph
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Hermann	Hermann	k1gMnSc1	Hermann
Kolbe	Kolb	k1gInSc5	Kolb
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
chemik	chemik	k1gMnSc1	chemik
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
–	–	k?	–
Fanny	Fanna	k1gFnSc2	Fanna
Elßlerová	Elßlerový	k2eAgFnSc1d1	Elßlerový
<g/>
,	,	kIx,	,
rakouská	rakouský	k2eAgFnSc1d1	rakouská
tanečnice	tanečnice	k1gFnSc1	tanečnice
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
Ambroży	Ambroży	k1gInPc1	Ambroży
Mieroszewski	Mieroszewski	k1gNnPc2	Mieroszewski
<g/>
,	,	kIx,	,
polský	polský	k2eAgMnSc1d1	polský
malíř	malíř	k1gMnSc1	malíř
(	(	kIx(	(
<g/>
*	*	kIx~	*
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
?	?	kIx.	?
</s>
<s>
–	–	k?	–
William	William	k1gInSc1	William
H.	H.	kA	H.
Mumler	Mumler	k1gInSc1	Mumler
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
podvodník	podvodník	k1gMnSc1	podvodník
se	s	k7c7	s
"	"	kIx"	"
<g/>
spirituální	spirituální	k2eAgFnSc7d1	spirituální
fotografií	fotografia	k1gFnSc7	fotografia
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
*	*	kIx~	*
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
České	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
–	–	k?	–
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
Papež	Papež	k1gMnSc1	Papež
–	–	k?	–
Lev	Lev	k1gMnSc1	Lev
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1903	[number]	k4	1903
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Království	království	k1gNnSc1	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
–	–	k?	–
Viktorie	Viktoria	k1gFnSc2	Viktoria
(	(	kIx(	(
<g/>
1837	[number]	k4	1837
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
Francie	Francie	k1gFnSc1	Francie
–	–	k?	–
Jules	Jules	k1gInSc4	Jules
Grévy	Gréva	k1gFnSc2	Gréva
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
Uherské	uherský	k2eAgNnSc1d1	Uherské
království	království	k1gNnSc1	království
–	–	k?	–
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
Rakouské	rakouský	k2eAgNnSc1d1	rakouské
císařství	císařství	k1gNnSc1	císařství
–	–	k?	–
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1848	[number]	k4	1848
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
Rusko	Rusko	k1gNnSc1	Rusko
–	–	k?	–
Alexandr	Alexandr	k1gMnSc1	Alexandr
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
Prusko	Prusko	k1gNnSc1	Prusko
–	–	k?	–
Vilém	Vilém	k1gMnSc1	Vilém
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
Dánsko	Dánsko	k1gNnSc1	Dánsko
–	–	k?	–
Kristián	Kristián	k1gMnSc1	Kristián
IX	IX	kA	IX
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
Švédsko	Švédsko	k1gNnSc1	Švédsko
–	–	k?	–
Oskar	Oskar	k1gMnSc1	Oskar
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
Belgie	Belgie	k1gFnSc1	Belgie
–	–	k?	–
Leopold	Leopolda	k1gFnPc2	Leopolda
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Belgický	belgický	k2eAgMnSc1d1	belgický
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
–	–	k?	–
Vilém	Vilém	k1gMnSc1	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
Řecko	Řecko	k1gNnSc1	Řecko
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
I.	I.	kA	I.
Řecký	řecký	k2eAgMnSc1d1	řecký
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
<g/>
–	–	k?	–
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
Španělsko	Španělsko	k1gNnSc1	Španělsko
–	–	k?	–
Alfons	Alfons	k1gMnSc1	Alfons
XII	XII	kA	XII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
–	–	k?	–
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
–	–	k?	–
Ludvík	Ludvík	k1gMnSc1	Ludvík
I.	I.	kA	I.
Portugalský	portugalský	k2eAgMnSc1d1	portugalský
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
Itálie	Itálie	k1gFnSc1	Itálie
–	–	k?	–
Umberto	Umberta	k1gFnSc5	Umberta
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
Rumunský	rumunský	k2eAgMnSc1d1	rumunský
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
–	–	k?	–
<g/>
1881	[number]	k4	1881
kníže	kníže	k1gMnSc1	kníže
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1914	[number]	k4	1914
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
–	–	k?	–
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
Bulharský	bulharský	k2eAgMnSc1d1	bulharský
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
–	–	k?	–
Abdulhamid	Abdulhamida	k1gFnPc2	Abdulhamida
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
USA	USA	kA	USA
–	–	k?	–
Chester	Chester	k1gMnSc1	Chester
A.	A.	kA	A.
Arthur	Arthur	k1gMnSc1	Arthur
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
Japonsko	Japonsko	k1gNnSc1	Japonsko
–	–	k?	–
Meidži	Meidž	k1gFnSc6	Meidž
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
<g />
.	.	kIx.	.
</s>
<s>
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
1884	[number]	k4	1884
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Digitalizované	digitalizovaný	k2eAgFnSc2d1	digitalizovaná
noviny	novina	k1gFnSc2	novina
a	a	k8xC	a
časopisy	časopis	k1gInPc1	časopis
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
:	:	kIx,	:
Národní	národní	k2eAgInPc1d1	národní
listy	list	k1gInPc1	list
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
24	[number]	k4	24
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
Národní	národní	k2eAgFnSc1d1	národní
politika	politika	k1gFnSc1	politika
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
2	[number]	k4	2
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
Pražský	pražský	k2eAgInSc1d1	pražský
deník	deník	k1gInSc1	deník
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
19	[number]	k4	19
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
Moravská	moravský	k2eAgFnSc1d1	Moravská
orlice	orlice	k1gFnSc1	orlice
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
22	[number]	k4	22
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
Opavský	opavský	k2eAgInSc1d1	opavský
<g />
.	.	kIx.	.
</s>
<s>
Týdenník	týdenník	k1gInSc1	týdenník
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
15	[number]	k4	15
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
Budivoj	Budivoj	k1gInSc4	Budivoj
(	(	kIx(	(
<g/>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
)	)	kIx)	)
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
20	[number]	k4	20
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
(	(	kIx(	(
<g/>
odkaz	odkaz	k1gInSc1	odkaz
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
přehled	přehled	k1gInSc4	přehled
ročníků	ročník	k1gInPc2	ročník
<g/>
)	)	kIx)	)
Květy	květ	k1gInPc1	květ
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
6	[number]	k4	6
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
Časopis	časopis	k1gInSc1	časopis
Musea	museum	k1gNnSc2	museum
království	království	k1gNnSc2	království
Českého	český	k2eAgInSc2d1	český
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
58	[number]	k4	58
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
Lumír	Lumír	k1gInSc1	Lumír
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
12	[number]	k4	12
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
archiv	archiv	k1gInSc1	archiv
ÚČL	ÚČL	kA	ÚČL
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
týž	týž	k3xTgInSc4	týž
ročník	ročník	k1gInSc4	ročník
v	v	k7c6	v
NK	NK	kA	NK
ČR	ČR	kA	ČR
Světozor	světozor	k1gInSc1	světozor
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
18	[number]	k4	18
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
(	(	kIx(	(
<g/>
archiv	archiv	k1gInSc1	archiv
ÚČL	ÚČL	kA	ÚČL
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
týž	týž	k3xTgInSc4	týž
ročník	ročník	k1gInSc4	ročník
v	v	k7c6	v
NK	NK	kA	NK
ČR	ČR	kA	ČR
Zlatá	zlatá	k1gFnSc1	zlatá
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
1	[number]	k4	1
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
(	(	kIx(	(
<g/>
archiv	archiv	k1gInSc1	archiv
ÚČL	ÚČL	kA	ÚČL
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
týž	týž	k3xTgInSc4	týž
ročník	ročník	k1gInSc4	ročník
v	v	k7c6	v
NK	NK	kA	NK
ČR	ČR	kA	ČR
Vesmír	vesmír	k1gInSc1	vesmír
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
13	[number]	k4	13
rok	rok	k1gInSc1	rok
1883-1884	[number]	k4	1883-1884
Humoristické	humoristický	k2eAgInPc1d1	humoristický
listy	list	k1gInPc1	list
–	–	k?	–
ročník	ročník	k1gInSc4	ročník
26	[number]	k4	26
rok	rok	k1gInSc1	rok
1884	[number]	k4	1884
(	(	kIx(	(
<g/>
archiv	archiv	k1gInSc1	archiv
ÚČL	ÚČL	kA	ÚČL
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
týž	týž	k3xTgInSc4	týž
ročník	ročník	k1gInSc4	ročník
v	v	k7c6	v
NK	NK	kA	NK
ČR	ČR	kA	ČR
Říšský	říšský	k2eAgInSc4d1	říšský
zákoník	zákoník	k1gInSc4	zákoník
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
verze	verze	k1gFnSc1	verze
<g/>
)	)	kIx)	)
–	–	k?	–
rok	rok	k1gInSc4	rok
1884	[number]	k4	1884
</s>
