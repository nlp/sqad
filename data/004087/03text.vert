<s>
Kostkový	kostkový	k2eAgInSc1d1	kostkový
cukr	cukr	k1gInSc1	cukr
je	být	k5eAaImIp3nS	být
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
cukr	cukr	k1gInSc4	cukr
upravený	upravený	k2eAgInSc4d1	upravený
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
kostky	kostka	k1gFnSc2	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
cukr	cukr	k1gInSc1	cukr
distribuoval	distribuovat	k5eAaBmAgInS	distribuovat
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
cukrových	cukrový	k2eAgFnPc2d1	cukrová
homolí	homole	k1gFnPc2	homole
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
objevitele	objevitel	k1gMnSc4	objevitel
kostkového	kostkový	k2eAgInSc2d1	kostkový
cukru	cukr	k1gInSc2	cukr
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Jakub	Jakub	k1gMnSc1	Jakub
Kryštof	Kryštof	k1gMnSc1	Kryštof
Rad	rada	k1gFnPc2	rada
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
dačické	dačický	k2eAgFnSc2d1	Dačická
rafinérie	rafinérie	k1gFnSc2	rafinérie
a	a	k8xC	a
kostkový	kostkový	k2eAgInSc4d1	kostkový
cukr	cukr	k1gInSc4	cukr
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
<g/>
.	.	kIx.	.
</s>
<s>
Radovy	Radův	k2eAgFnPc1d1	Radova
první	první	k4xOgFnPc1	první
kostky	kostka	k1gFnPc1	kostka
měly	mít	k5eAaImAgFnP	mít
tvar	tvar	k1gInSc4	tvar
kvádru	kvádr	k1gInSc2	kvádr
blízkého	blízký	k2eAgInSc2d1	blízký
krychli	krychle	k1gFnSc4	krychle
s	s	k7c7	s
rozměry	rozměr	k1gInPc7	rozměr
hran	hrana	k1gFnPc2	hrana
1,2	[number]	k4	1,2
cm	cm	kA	cm
a	a	k8xC	a
1,5	[number]	k4	1,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
u	u	k7c2	u
různých	různý	k2eAgMnPc2d1	různý
výrobců	výrobce	k1gMnPc2	výrobce
lišit	lišit	k5eAaImF	lišit
a	a	k8xC	a
také	také	k9	také
tvar	tvar	k1gInSc1	tvar
se	se	k3xPyFc4	se
měnil	měnit	k5eAaImAgInS	měnit
od	od	k7c2	od
krychle	krychle	k1gFnSc2	krychle
po	po	k7c4	po
různé	různý	k2eAgInPc4d1	různý
geometrické	geometrický	k2eAgInPc4d1	geometrický
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
znaků	znak	k1gInPc2	znak
karetních	karetní	k2eAgFnPc2d1	karetní
barev	barva	k1gFnPc2	barva
bridže	bridž	k1gInSc2	bridž
-	-	kIx~	-
kříže	kříž	k1gInPc4	kříž
<g/>
,	,	kIx,	,
káry	kára	k1gFnPc4	kára
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnPc4	srdce
a	a	k8xC	a
piky	pik	k1gInPc4	pik
<g/>
.	.	kIx.	.
</s>
<s>
Barevnost	barevnost	k1gFnSc1	barevnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různá	různý	k2eAgFnSc1d1	různá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
užitím	užití	k1gNnSc7	užití
rafinovaného	rafinovaný	k2eAgInSc2d1	rafinovaný
cukru	cukr	k1gInSc2	cukr
bez	bez	k7c2	bez
přídavku	přídavek	k1gInSc2	přídavek
barviv	barvivo	k1gNnPc2	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Kostka	kostka	k1gFnSc1	kostka
cukru	cukr	k1gInSc2	cukr
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
vládou	vláda	k1gFnSc7	vláda
ČR	ČR	kA	ČR
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
jejího	její	k3xOp3gNnSc2	její
předsednictví	předsednictví	k1gNnSc2	předsednictví
EU	EU	kA	EU
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
propagačních	propagační	k2eAgInPc6d1	propagační
materiálech	materiál	k1gInPc6	materiál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
reklama	reklama	k1gFnSc1	reklama
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
s	s	k7c7	s
přehlídkou	přehlídka	k1gFnSc7	přehlídka
osobností	osobnost	k1gFnPc2	osobnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kostkový	kostkový	k2eAgInSc4d1	kostkový
cukr	cukr	k1gInSc4	cukr
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Radio	radio	k1gNnSc1	radio
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
První	první	k4xOgFnSc1	první
kostka	kostka	k1gFnSc1	kostka
cukru	cukr	k1gInSc2	cukr
na	na	k7c6	na
světě	svět	k1gInSc6	svět
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
v	v	k7c6	v
Dačicích	Dačice	k1gFnPc6	Dačice
Dačice	Dačice	k1gFnPc1	Dačice
-	-	kIx~	-
Kostka	kostka	k1gFnSc1	kostka
cukru	cukr	k1gInSc2	cukr
Euroskop	Euroskop	k1gInSc1	Euroskop
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Kostka	kostka	k1gFnSc1	kostka
cukru	cukr	k1gInSc2	cukr
<g/>
?	?	kIx.	?
</s>
<s>
Kampaň	kampaň	k1gFnSc1	kampaň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
i	i	k9	i
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vondra	Vondra	k1gMnSc1	Vondra
(	(	kIx(	(
<g/>
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
noviny	novina	k1gFnPc1	novina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
Euroskop	Euroskop	k1gInSc1	Euroskop
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Evropě	Evropa	k1gFnSc6	Evropa
to	ten	k3xDgNnSc1	ten
osladíme	osladit	k5eAaPmIp1nP	osladit
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
5.9	[number]	k4	5.9
<g/>
.2008	.2008	k4	.2008
</s>
