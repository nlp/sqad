<s>
Za	za	k7c4	za
objevitele	objevitel	k1gMnSc4	objevitel
kostkového	kostkový	k2eAgInSc2d1	kostkový
cukru	cukr	k1gInSc2	cukr
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Jakub	Jakub	k1gMnSc1	Jakub
Kryštof	Kryštof	k1gMnSc1	Kryštof
Rad	rada	k1gFnPc2	rada
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
dačické	dačický	k2eAgFnSc2d1	Dačická
rafinérie	rafinérie	k1gFnSc2	rafinérie
a	a	k8xC	a
kostkový	kostkový	k2eAgInSc4d1	kostkový
cukr	cukr	k1gInSc4	cukr
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1843	[number]	k4	1843
<g/>
.	.	kIx.	.
</s>
