<s>
Ve	v	k7c6
jménu	jméno	k1gNnSc6
Angela	angel	k1gMnSc2
</s>
<s>
Ve	v	k7c6
jménu	jméno	k1gNnSc6
Angela	angel	k1gMnSc2
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Avenging	Avenging	k1gInSc1
Angelo	Angela	k1gFnSc5
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéFrancie	americkéFrancie	k1gFnSc2
Francie	Francie	k1gFnSc2
Jazyk	jazyk	k1gInSc1
</s>
<s>
angličtina	angličtina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
96	#num#	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
Žánry	žánr	k1gInPc1
</s>
<s>
akční	akční	k2eAgMnSc1d1
<g/>
,	,	kIx,
komedie	komedie	k1gFnSc1
<g/>
,	,	kIx,
krimi	krimi	k1gNnSc1
Scénář	scénář	k1gInSc1
</s>
<s>
Will	Will	k1gMnSc1
AldisSteve	AldisSteev	k1gFnSc2
Mackall	Mackall	k1gMnSc1
Režie	režie	k1gFnSc1
</s>
<s>
Martyn	Martyn	k1gNnSc1
Burke	Burke	k1gFnSc1
Obsazení	obsazení	k1gNnSc1
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Sylvester	Sylvester	k1gMnSc1
StalloneMadeleine	StalloneMadelein	k1gInSc5
StoweAnthony	StoweAnthona	k1gFnPc1
QuinnRaoul	QuinnRaoul	k1gInSc4
BovaHarry	BovaHarra	k1gFnSc2
Van	van	k1gInSc1
Gorkum	Gorkum	k1gInSc1
Produkce	produkce	k1gFnSc1
</s>
<s>
Tarak	Tarak	k6eAd1
Ben	Ben	k1gInSc1
AmmarElie	AmmarElie	k1gFnSc2
SamahaStanley	SamahaStanlea	k1gFnSc2
Wilson	Wilson	k1gMnSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
Bill	Bill	k1gMnSc1
ContiJon	ContiJon	k1gMnSc1
Bon	bon	k1gInSc4
Jovi	Jov	k1gFnSc2
Kamera	kamera	k1gFnSc1
</s>
<s>
Ousama	Ousama	k1gFnSc1
Rawi	Raw	k1gFnSc2
Střih	střih	k1gInSc1
</s>
<s>
David	David	k1gMnSc1
Codron	Codron	k1gMnSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc4
2002	#num#	k4
Distribuce	distribuce	k1gFnSc1
</s>
<s>
Warner	Warner	k1gMnSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
jménu	jméno	k1gNnSc6
Angela	angel	k1gMnSc2
na	na	k7c4
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
jménu	jméno	k1gNnSc6
Angela	Angel	k1gMnSc2
(	(	kIx(
<g/>
v	v	k7c6
americkém	americký	k2eAgInSc6d1
originále	originál	k1gInSc6
<g/>
:	:	kIx,
Avenging	Avenging	k2eAgMnSc1d1
Angelo	Angelo	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgInSc1d1
akční	akční	k2eAgInSc1d1
film	film	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režisérem	režisér	k1gMnSc7
filmu	film	k1gInSc2
je	být	k5eAaImIp3nS
Martyn	Martyn	k1gMnSc1
Burke	Burke	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
ve	v	k7c6
filmu	film	k1gInSc6
ztvárnili	ztvárnit	k5eAaPmAgMnP
Sylvester	Sylvester	k1gMnSc1
Stallone	Stallon	k1gInSc5
<g/>
,	,	kIx,
Madeleine	Madeleine	k1gFnSc1
Stowe	Stowe	k1gFnSc1
<g/>
,	,	kIx,
Anthony	Anthona	k1gFnPc1
Quinn	Quinn	k1gInSc1
<g/>
,	,	kIx,
Raoul	Raoul	k1gInSc1
Bova	Bovum	k1gNnSc2
a	a	k8xC
Harry	Harra	k1gFnSc2
Van	vana	k1gFnPc2
Gorkum	Gorkum	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Reakce	reakce	k1gFnSc1
</s>
<s>
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
4	#num#	k4
<g/>
.	.	kIx.
srpnu	srpen	k1gInSc3
2014	#num#	k4
</s>
<s>
Film	film	k1gInSc1
získal	získat	k5eAaPmAgInS
mezi	mezi	k7c7
diváky	divák	k1gMnPc7
na	na	k7c6
největších	veliký	k2eAgFnPc6d3
filmových	filmový	k2eAgFnPc6d1
databázích	databáze	k1gFnPc6
spíše	spíše	k9
průměrné	průměrný	k2eAgNnSc4d1
hodnocení	hodnocení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
csfd	csfd	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
54	#num#	k4
%	%	kIx~
</s>
<s>
imdb	imdb	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
:	:	kIx,
5,2	5,2	k4
z	z	k7c2
10	#num#	k4
</s>
<s>
fdb	fdb	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
67,2	67,2	k4
%	%	kIx~
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Sylvester	Sylvester	k1gMnSc1
Stallone	Stallon	k1gInSc5
</s>
<s>
Frankie	Frankie	k1gFnSc5
Delano	Delana	k1gFnSc5
</s>
<s>
Madeleine	Madeleine	k1gFnSc1
Stowe	Stow	k1gFnSc2
</s>
<s>
Jennifer	Jennifer	k1gMnSc1
Allieghieri	Allieghieri	k1gNnSc2
Barrett	Barrett	k1gMnSc1
</s>
<s>
Anthony	Anthona	k1gFnPc1
Quinn	Quinna	k1gFnPc2
</s>
<s>
Angelo	Angela	k1gFnSc5
Allieghieri	Allieghier	k1gFnSc5
</s>
<s>
Raoul	Raoul	k1gInSc1
Bova	Bov	k1gInSc2
</s>
<s>
Marcello	Marcello	k1gNnSc4
<g/>
/	/	kIx~
<g/>
Gianni	Gianen	k2eAgMnPc1d1
Carboni	Carbon	k1gMnPc1
</s>
<s>
Harry	Harra	k1gFnSc2
Van	vana	k1gFnPc2
Gorkum	Gorkum	k1gNnSc1
</s>
<s>
Kip	Kip	k?
Barrett	Barrett	k1gInSc1
</s>
<s>
Billy	Bill	k1gMnPc4
Gardell	Gardellum	k1gNnPc2
</s>
<s>
Bruno	Bruno	k1gMnSc1
</s>
<s>
George	Georg	k1gMnSc2
Touliatos	Touliatos	k1gMnSc1
</s>
<s>
Lucio	Lucio	k1gMnSc1
Malatesta	Malatesta	k1gMnSc1
</s>
<s>
Angelo	Angela	k1gFnSc5
Celeste	Celest	k1gInSc5
</s>
<s>
kněz	kněz	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Ve	v	k7c6
jménu	jméno	k1gNnSc6
Angela	Angela	k1gFnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Ve	v	k7c6
jménu	jméno	k1gNnSc6
Angela	angel	k1gMnSc2
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
jménu	jméno	k1gNnSc6
Angela	Angela	k1gFnSc1
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
