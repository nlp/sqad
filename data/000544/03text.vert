<s>
Pondělí	pondělí	k1gNnSc1	pondělí
(	(	kIx(	(
<g/>
zast.	zast.	k?	zast.
a	a	k8xC	a
nář	nář	k?	nář
<g/>
.	.	kIx.	.
pondělek	pondělek	k1gInSc1	pondělek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
dnů	den	k1gInPc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
občanském	občanský	k2eAgInSc6d1	občanský
kalendáři	kalendář	k1gInSc6	kalendář
je	být	k5eAaImIp3nS	být
pondělí	pondělí	k1gNnSc4	pondělí
první	první	k4xOgInSc1	první
den	den	k1gInSc1	den
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
tradičním	tradiční	k2eAgMnSc6d1	tradiční
židovském	židovský	k2eAgMnSc6d1	židovský
a	a	k8xC	a
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
kalendáři	kalendář	k1gInSc6	kalendář
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
den	den	k1gInSc4	den
druhý	druhý	k4xOgInSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Pondělím	pondělí	k1gNnSc7	pondělí
také	také	k9	také
začíná	začínat	k5eAaImIp3nS	začínat
pracovní	pracovní	k2eAgInSc4d1	pracovní
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
jej	on	k3xPp3gNnSc4	on
lidé	člověk	k1gMnPc1	člověk
často	často	k6eAd1	často
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
nejméně	málo	k6eAd3	málo
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Název	název	k1gInSc1	název
pondělí	pondělí	k1gNnSc2	pondělí
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
názvu	název	k1gInSc2	název
neděle	neděle	k1gFnSc2	neděle
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
"	"	kIx"	"
<g/>
po	po	k7c6	po
neděli	neděle	k1gFnSc6	neděle
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
severština	severština	k1gFnSc1	severština
pondělí	pondělí	k1gNnSc2	pondělí
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
po	po	k7c4	po
Manim	Manim	k1gInSc4	Manim
<g/>
,	,	kIx,	,
bohu	bůh	k1gMnSc6	bůh
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
v	v	k7c6	v
na	na	k7c6	na
latinském	latinský	k2eAgNnSc6d1	latinské
základě	základ	k1gInSc6	základ
založených	založený	k2eAgInPc6d1	založený
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
(	(	kIx(	(
<g/>
Lunedì	Lunedì	k1gFnPc6	Lunedì
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
(	(	kIx(	(
<g/>
lundi	lund	k1gMnPc1	lund
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
španělštině	španělština	k1gFnSc6	španělština
(	(	kIx(	(
<g/>
Lunes	Lunes	k1gInSc1	Lunes
<g/>
)	)	kIx)	)
a	a	k8xC	a
rumunštině	rumunština	k1gFnSc6	rumunština
(	(	kIx(	(
<g/>
Luni	Lun	k1gInSc6	Lun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
názvy	název	k1gInPc1	název
vycházející	vycházející	k2eAgInPc1d1	vycházející
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
Měsíc	měsíc	k1gInSc4	měsíc
neboli	neboli	k8xC	neboli
lunu	luna	k1gFnSc4	luna
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc4d1	ruské
slovo	slovo	k1gNnSc4	slovo
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k9	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
-	-	kIx~	-
п	п	k?	п
(	(	kIx(	(
<g/>
ponědělnik	ponědělnik	k1gMnSc1	ponědělnik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
je	být	k5eAaImIp3nS	být
pondělí	pondělí	k1gNnSc1	pondělí
považováno	považován	k2eAgNnSc1d1	považováno
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
západní	západní	k2eAgInSc4d1	západní
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
systém	systém	k1gInSc4	systém
představen	představit	k5eAaPmNgInS	představit
až	až	k6eAd1	až
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
pondělí	pondělí	k1gNnSc4	pondělí
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
význam	význam	k1gInSc1	význam
jako	jako	k9	jako
"	"	kIx"	"
<g/>
den	den	k1gInSc1	den
začátku	začátek	k1gInSc2	začátek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
se	se	k3xPyFc4	se
pondělí	pondělí	k1gNnSc1	pondělí
nazývá	nazývat	k5eAaImIp3nS	nazývat
xingqi	xingqi	k6eAd1	xingqi
yi	yi	k?	yi
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
den	den	k1gInSc1	den
číslo	číslo	k1gNnSc1	číslo
jedna	jeden	k4xCgFnSc1	jeden
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
standard	standard	k1gInSc4	standard
<g/>
,	,	kIx,	,
ISO	ISO	kA	ISO
8601	[number]	k4	8601
<g/>
,	,	kIx,	,
definuje	definovat	k5eAaBmIp3nS	definovat
pondělí	pondělí	k1gNnSc4	pondělí
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
týdne	týden	k1gInSc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
židovsko-křesťanského	židovskořesťanský	k2eAgNnSc2d1	židovsko-křesťanské
počítání	počítání	k1gNnSc2	počítání
je	být	k5eAaImIp3nS	být
pondělí	pondělí	k1gNnSc4	pondělí
druhým	druhý	k4xOgInSc7	druhý
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
prvním	první	k4xOgInSc7	první
dnem	den	k1gInSc7	den
týdne	týden	k1gInSc2	týden
je	být	k5eAaImIp3nS	být
neděle	neděle	k1gFnPc4	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
také	také	k9	také
standardním	standardní	k2eAgInSc7d1	standardní
formátem	formát	k1gInSc7	formát
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pondělí	pondělí	k1gNnSc2	pondělí
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
arménština	arménština	k1gFnSc1	arménština
<g/>
,	,	kIx,	,
gruzínština	gruzínština	k1gFnSc1	gruzínština
<g/>
,	,	kIx,	,
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
,	,	kIx,	,
hebrejština	hebrejština	k1gFnSc1	hebrejština
<g/>
,	,	kIx,	,
perština	perština	k1gFnSc1	perština
<g/>
,	,	kIx,	,
portugalština	portugalština	k1gFnSc1	portugalština
a	a	k8xC	a
syrština	syrština	k1gFnSc1	syrština
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kvakeři	kvaker	k1gMnPc1	kvaker
také	také	k9	také
tradičně	tradičně	k6eAd1	tradičně
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
pondělí	pondělí	k1gNnSc4	pondělí
jako	jako	k8xC	jako
o	o	k7c4	o
"	"	kIx"	"
<g/>
druhém	druhý	k4xOgInSc6	druhý
dnu	den	k1gInSc6	den
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vyvarovali	vyvarovat	k5eAaPmAgMnP	vyvarovat
jeho	jeho	k3xOp3gInSc3	jeho
standardnímu	standardní	k2eAgInSc3d1	standardní
anglickému	anglický	k2eAgInSc3d1	anglický
názvu	název	k1gInSc3	název
(	(	kIx(	(
<g/>
Monday	Monday	k1gInPc1	Monday
<g/>
)	)	kIx)	)
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
pohanským	pohanský	k2eAgInSc7d1	pohanský
původem	původ	k1gInSc7	původ
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
podobných	podobný	k2eAgInPc2d1	podobný
důvodů	důvod	k1gInPc2	důvod
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
liturgického	liturgický	k2eAgInSc2d1	liturgický
kalendáře	kalendář	k1gInSc2	kalendář
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
odkazováno	odkazovat	k5eAaImNgNnS	odkazovat
na	na	k7c6	na
pondělí	pondělí	k1gNnSc6	pondělí
jako	jako	k8xS	jako
na	na	k7c4	na
"	"	kIx"	"
<g/>
Feria	Ferius	k1gMnSc4	Ferius
II	II	kA	II
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Portugalský	portugalský	k2eAgInSc4d1	portugalský
název	název	k1gInSc4	název
pro	pro	k7c4	pro
pondělí	pondělí	k1gNnSc6	pondělí
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
reflektuje	reflektovat	k5eAaImIp3nS	reflektovat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
dny	den	k1gInPc4	den
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
soboty	sobota	k1gFnSc2	sobota
a	a	k8xC	a
neděle	neděle	k1gFnSc2	neděle
<g/>
:	:	kIx,	:
Portugalský	portugalský	k2eAgInSc1d1	portugalský
název	název	k1gInSc1	název
pondělí	pondělí	k1gNnSc2	pondělí
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
segunda-feira	segundaeira	k6eAd1	segunda-feira
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
kultura	kultura	k1gFnSc1	kultura
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dívá	dívat	k5eAaImIp3nS	dívat
na	na	k7c4	na
pondělí	pondělí	k1gNnSc4	pondělí
jako	jako	k8xC	jako
na	na	k7c4	na
začátek	začátek	k1gInSc4	začátek
pracovního	pracovní	k2eAgInSc2d1	pracovní
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
typickém	typický	k2eAgNnSc6d1	typické
pondělí	pondělí	k1gNnSc6	pondělí
jdou	jít	k5eAaImIp3nP	jít
dospělí	dospělý	k2eAgMnPc1d1	dospělý
lidé	člověk	k1gMnPc1	člověk
po	po	k7c6	po
víkendu	víkend	k1gInSc6	víkend
opět	opět	k6eAd1	opět
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Pondělí	pondělí	k1gNnPc1	pondělí
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
často	často	k6eAd1	často
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
nešťastné	šťastný	k2eNgMnPc4d1	nešťastný
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
neoblíbené	oblíbený	k2eNgInPc4d1	neoblíbený
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
je	být	k5eAaImIp3nS	být
začátkem	začátkem	k7c2	začátkem
pracovního	pracovní	k2eAgInSc2d1	pracovní
týdne	týden	k1gInSc2	týden
obvykle	obvykle	k6eAd1	obvykle
sobota	sobota	k1gFnSc1	sobota
(	(	kIx(	(
<g/>
čtvrtek	čtvrtek	k1gInSc1	čtvrtek
a	a	k8xC	a
pátek	pátek	k1gInSc1	pátek
jsou	být	k5eAaImIp3nP	být
dodržovány	dodržovat	k5eAaImNgFnP	dodržovat
jako	jako	k8xC	jako
víkend	víkend	k1gInSc1	víkend
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
pracovním	pracovní	k2eAgInSc7d1	pracovní
dnem	den	k1gInSc7	den
týdne	týden	k1gInSc2	týden
neděle	neděle	k1gFnSc2	neděle
<g/>
.	.	kIx.	.
</s>
<s>
Pátek	pátek	k1gInSc1	pátek
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
pracovní	pracovní	k2eAgInSc4d1	pracovní
den	den	k1gInSc4	den
a	a	k8xC	a
páteční	páteční	k2eAgFnSc1d1	páteční
noc	noc	k1gFnSc1	noc
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
sobota	sobota	k1gFnSc1	sobota
náleží	náležet	k5eAaImIp3nS	náležet
sabatu	sabat	k1gInSc2	sabat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
pondělím	pondělí	k1gNnSc7	pondělí
spojena	spojit	k5eAaPmNgFnS	spojit
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
u	u	k7c2	u
Thajského	thajský	k2eAgInSc2d1	thajský
solárního	solární	k2eAgInSc2d1	solární
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
výraz	výraz	k1gInSc4	výraz
modré	modrý	k2eAgNnSc1d1	modré
pondělí	pondělí	k1gNnSc1	pondělí
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dříve	dříve	k6eAd2	dříve
označoval	označovat	k5eAaImAgInS	označovat
poslední	poslední	k2eAgNnSc4d1	poslední
masopustní	masopustní	k2eAgNnSc4d1	masopustní
pondělí	pondělí	k1gNnSc4	pondělí
<g/>
.	.	kIx.	.
</s>
<s>
Kostely	kostel	k1gInPc1	kostel
se	se	k3xPyFc4	se
zdobily	zdobit	k5eAaImAgInP	zdobit
modrým	modrý	k2eAgInSc7d1	modrý
nebo	nebo	k8xC	nebo
fialovým	fialový	k2eAgNnSc7d1	fialové
suknem	sukno	k1gNnSc7	sukno
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Pondělí	pondělí	k1gNnSc2	pondělí
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
protagonisty	protagonista	k1gMnSc2	protagonista
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
australský	australský	k2eAgMnSc1d1	australský
sci-fi	scii	k1gFnPc4	sci-fi
autor	autor	k1gMnSc1	autor
Garth	Garth	k1gMnSc1	Garth
Nix	Nix	k1gMnSc1	Nix
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pondělí	pondělí	k1gNnSc2	pondělí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Černé	Černé	k2eAgNnSc4d1	Černé
pondělí	pondělí	k1gNnSc4	pondělí
Modré	modrý	k2eAgNnSc4d1	modré
pondělí	pondělí	k1gNnSc4	pondělí
Čisté	čistá	k1gFnSc2	čistá
pondělí	pondělí	k1gNnSc2	pondělí
Rosen	rosen	k2eAgMnSc1d1	rosen
Montag	Montag	k1gMnSc1	Montag
Velikonoční	velikonoční	k2eAgNnSc4d1	velikonoční
pondělí	pondělí	k1gNnSc4	pondělí
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pondělí	pondělí	k1gNnSc4	pondělí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
