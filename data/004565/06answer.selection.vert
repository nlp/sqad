<s>
Introspekci	introspekce	k1gFnSc4	introspekce
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
metodu	metoda	k1gFnSc4	metoda
zkoumání	zkoumání	k1gNnSc2	zkoumání
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
(	(	kIx(	(
<g/>
vědomí	vědomí	k1gNnSc1	vědomí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
anglický	anglický	k2eAgMnSc1d1	anglický
myslitel	myslitel	k1gMnSc1	myslitel
John	John	k1gMnSc1	John
Locke	Locke	k1gFnSc1	Locke
(	(	kIx(	(
<g/>
1632	[number]	k4	1632
<g/>
-	-	kIx~	-
<g/>
1704	[number]	k4	1704
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
