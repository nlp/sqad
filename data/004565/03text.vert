<s>
Introspekci	introspekce	k1gFnSc4	introspekce
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
metodu	metoda	k1gFnSc4	metoda
zkoumání	zkoumání	k1gNnSc2	zkoumání
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
(	(	kIx(	(
<g/>
vědomí	vědomí	k1gNnSc1	vědomí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
anglický	anglický	k2eAgMnSc1d1	anglický
myslitel	myslitel	k1gMnSc1	myslitel
John	John	k1gMnSc1	John
Locke	Locke	k1gFnSc1	Locke
(	(	kIx(	(
<g/>
1632	[number]	k4	1632
<g/>
-	-	kIx~	-
<g/>
1704	[number]	k4	1704
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
introspekce	introspekce	k1gFnSc2	introspekce
(	(	kIx(	(
<g/>
sebepozorování	sebepozorování	k1gNnSc1	sebepozorování
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
dospět	dospět	k5eAaPmF	dospět
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
jakýchsi	jakýsi	k3yIgInPc2	jakýsi
elementů	element	k1gInPc2	element
(	(	kIx(	(
<g/>
prvků	prvek	k1gInPc2	prvek
vědomí	vědomí	k1gNnSc1	vědomí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
spojování	spojování	k1gNnSc2	spojování
(	(	kIx(	(
<g/>
asociací	asociace	k1gFnSc7	asociace
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
veškerý	veškerý	k3xTgInSc4	veškerý
duševní	duševní	k2eAgInSc4d1	duševní
vědomý	vědomý	k2eAgInSc4d1	vědomý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
<g/>
:	:	kIx,	:
sensation	sensation	k1gInSc4	sensation
-	-	kIx~	-
počitky	počitek	k1gInPc4	počitek
a	a	k8xC	a
vjemy	vjem	k1gInPc4	vjem
vnějších	vnější	k2eAgInPc2d1	vnější
objektů	objekt	k1gInPc2	objekt
reflexion	reflexion	k1gInSc1	reflexion
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
též	též	k9	též
intuition	intuition	k1gInSc1	intuition
<g/>
)	)	kIx)	)
-	-	kIx~	-
prostředky	prostředek	k1gInPc1	prostředek
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
dějů	děj	k1gInPc2	děj
mysli	mysl	k1gFnSc2	mysl
samotné	samotný	k2eAgInPc1d1	samotný
Pojem	pojem	k1gInSc1	pojem
introspekce	introspekce	k1gFnSc2	introspekce
se	se	k3xPyFc4	se
též	též	k9	též
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
přímý	přímý	k2eAgInSc4d1	přímý
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
sebezkoumání	sebezkoumání	k1gNnSc4	sebezkoumání
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
obrácení	obrácení	k1gNnSc4	obrácení
pozornosti	pozornost	k1gFnSc2	pozornost
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
myšlenkové	myšlenkový	k2eAgFnSc6d1	myšlenková
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
například	například	k6eAd1	například
i	i	k9	i
citové	citový	k2eAgNnSc1d1	citové
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
okolní	okolní	k2eAgInPc4d1	okolní
podměty	podmět	k1gInPc4	podmět
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgMnSc3	ten
empirické	empirický	k2eAgInPc1d1	empirický
směry	směr	k1gInPc1	směr
poznání	poznání	k1gNnSc2	poznání
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
(	(	kIx(	(
<g/>
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
intelektu	intelekt	k1gInSc6	intelekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
počitků	počitek	k1gInPc2	počitek
a	a	k8xC	a
vjemů	vjem	k1gInPc2	vjem
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
mysl	mysl	k1gFnSc1	mysl
prázdná	prázdný	k2eAgFnSc1d1	prázdná
<g/>
.	.	kIx.	.
-	-	kIx~	-
"	"	kIx"	"
<g/>
Nic	nic	k3yNnSc1	nic
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
nebylo	být	k5eNaImAgNnS	být
dříve	dříve	k6eAd2	dříve
ve	v	k7c6	v
smyslech	smysl	k1gInPc6	smysl
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
J.	J.	kA	J.
Locke	Locke	k1gInSc4	Locke
<g/>
)	)	kIx)	)
Teorii	teorie	k1gFnSc4	teorie
elementů	element	k1gInPc2	element
později	pozdě	k6eAd2	pozdě
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
Edward	Edward	k1gMnSc1	Edward
C.	C.	kA	C.
Tolman	Tolman	k1gMnSc1	Tolman
v	v	k7c6	v
neobehaviorismu	neobehaviorismus	k1gInSc6	neobehaviorismus
a	a	k8xC	a
kognitivní	kognitivní	k2eAgFnSc3d1	kognitivní
psychologii	psychologie	k1gFnSc3	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Introspekce	introspekce	k1gFnSc1	introspekce
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
metod	metoda	k1gFnPc2	metoda
psychologie	psychologie	k1gFnSc2	psychologie
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
zejména	zejména	k9	zejména
zástupci	zástupce	k1gMnPc1	zástupce
behaviorismu	behaviorismus	k1gInSc2	behaviorismus
<g/>
,	,	kIx,	,
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
jako	jako	k8xC	jako
nevědecká	vědecký	k2eNgFnSc1d1	nevědecká
a	a	k8xC	a
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
extrospekcí	extrospekce	k1gFnSc7	extrospekce
<g/>
.	.	kIx.	.
</s>
<s>
Introspekce	introspekce	k1gFnSc1	introspekce
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
kvalitativního	kvalitativní	k2eAgInSc2d1	kvalitativní
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
R.	R.	kA	R.
Descartem	Descart	k1gMnSc7	Descart
(	(	kIx(	(
<g/>
1649	[number]	k4	1649
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
psychologická	psychologický	k2eAgFnSc1d1	psychologická
metoda	metoda	k1gFnSc1	metoda
pozorování	pozorování	k1gNnPc2	pozorování
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc2d1	vlastní
psychiky	psychika	k1gFnSc2	psychika
<g/>
,	,	kIx,	,
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
prožitků	prožitek	k1gInPc2	prožitek
a	a	k8xC	a
procesů	proces	k1gInPc2	proces
probíhajících	probíhající	k2eAgInPc2d1	probíhající
ve	v	k7c6	v
vědomí	vědomí	k1gNnSc6	vědomí
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Psychologický	psychologický	k2eAgInSc1d1	psychologický
slovník	slovník	k1gInSc1	slovník
<g/>
;	;	kIx,	;
Hartl	Hartl	k1gMnSc1	Hartl
a	a	k8xC	a
Hartlová	Hartlová	k1gFnSc1	Hartlová
<g/>
;	;	kIx,	;
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaImNgFnS	využívat
i	i	k9	i
v	v	k7c6	v
první	první	k4xOgFnSc6	první
psychologické	psychologický	k2eAgFnSc6d1	psychologická
laboratoři	laboratoř	k1gFnSc6	laboratoř
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
W.	W.	kA	W.
Wundtem	Wundto	k1gNnSc7	Wundto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
přísně	přísně	k6eAd1	přísně
kontrolována	kontrolován	k2eAgFnSc1d1	kontrolována
<g/>
.	.	kIx.	.
</s>
