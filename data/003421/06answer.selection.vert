<s>
Pak	pak	k6eAd1	pak
následují	následovat	k5eAaImIp3nP	následovat
obřadní	obřadní	k2eAgInPc4d1	obřadní
tance	tanec	k1gInPc4	tanec
a	a	k8xC	a
po	po	k7c6	po
spáření	spáření	k1gNnSc6	spáření
samice	samice	k1gFnSc2	samice
snese	snést	k5eAaPmIp3nS	snést
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc4	jeden
veliké	veliký	k2eAgNnSc4d1	veliké
bílé	bílý	k2eAgNnSc4d1	bílé
vejce	vejce	k1gNnSc4	vejce
s	s	k7c7	s
hnědými	hnědý	k2eAgFnPc7d1	hnědá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
až	až	k9	až
500	[number]	k4	500
gramů	gram	k1gInPc2	gram
(	(	kIx(	(
<g/>
slepičí	slepičit	k5eAaImIp3nS	slepičit
80	[number]	k4	80
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
