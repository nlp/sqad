<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Vácha	Vácha	k1gMnSc1	Vácha
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
brankář	brankář	k1gMnSc1	brankář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
lize	liga	k1gFnSc6	liga
hrál	hrát	k5eAaImAgInS	hrát
za	za	k7c4	za
FK	FK	kA	FK
Švarc	Švarc	k1gMnSc1	Švarc
Benešov	Benešov	k1gInSc1	Benešov
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
19	[number]	k4	19
ligových	ligový	k2eAgNnPc6d1	ligové
utkáních	utkání	k1gNnPc6	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
lize	liga	k1gFnSc6	liga
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c7	za
VTJ	VTJ	kA	VTJ
Tábor	Tábor	k1gInSc1	Tábor
a	a	k8xC	a
BSS	BSS	kA	BSS
Brandýs	Brandýs	k1gInSc1	Brandýs
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ligová	ligový	k2eAgFnSc1d1	ligová
bilance	bilance	k1gFnSc1	bilance
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Dávid	Dávida	k1gFnPc2	Dávida
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Futbal	Futbal	k1gInSc1	Futbal
Ročenka	ročenka	k1gFnSc1	ročenka
83	[number]	k4	83
<g/>
/	/	kIx~	/
<g/>
84	[number]	k4	84
–	–	k?	–
Šport	Šport	k1gInSc1	Šport
<g/>
,	,	kIx,	,
slovenské	slovenský	k2eAgNnSc1d1	slovenské
telovýchovné	telovýchovný	k2eAgNnSc1d1	telovýchovný
vydavatelstvo	vydavatelstvo	k1gNnSc1	vydavatelstvo
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1984	[number]	k4	1984
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Player	Player	k1gInSc1	Player
History	Histor	k1gInPc1	Histor
</s>
</p>
<p>
<s>
JFK-Fotbal	JFK-Fotbal	k1gInSc1	JFK-Fotbal
–	–	k?	–
I.	I.	kA	I.
liga	liga	k1gFnSc1	liga
–	–	k?	–
hráči	hráč	k1gMnPc7	hráč
</s>
</p>
