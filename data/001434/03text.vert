<s>
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
A	a	k9	a
Brief	Brief	k1gInSc1	Brief
History	Histor	k1gInPc1	Histor
of	of	k?	of
Time	Tim	k1gInSc2	Tim
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
populárně	populárně	k6eAd1	populárně
vědecká	vědecký	k2eAgFnSc1d1	vědecká
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
kosmolog	kosmolog	k1gMnSc1	kosmolog
Stephen	Stephen	k2eAgInSc4d1	Stephen
Hawking	Hawking	k1gInSc4	Hawking
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
bestsellerem	bestseller	k1gInSc7	bestseller
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
9	[number]	k4	9
miliónů	milión	k4xCgInPc2	milión
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
autor	autor	k1gMnSc1	autor
laickému	laický	k2eAgMnSc3d1	laický
čtenáři	čtenář	k1gMnSc3	čtenář
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
kosmologických	kosmologický	k2eAgNnPc2d1	kosmologické
témat	téma	k1gNnPc2	téma
včetně	včetně	k7c2	včetně
teorie	teorie	k1gFnSc2	teorie
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
<g/>
,	,	kIx,	,
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
světelných	světelný	k2eAgInPc2d1	světelný
kuželů	kužel	k1gInPc2	kužel
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
superstrun	superstruna	k1gFnPc2	superstruna
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
pojednání	pojednání	k1gNnSc2	pojednání
o	o	k7c6	o
těchto	tento	k3xDgNnPc6	tento
tématech	téma	k1gNnPc6	téma
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
snaží	snažit	k5eAaImIp3nS	snažit
také	také	k9	také
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
část	část	k1gFnSc4	část
složité	složitý	k2eAgFnSc2d1	složitá
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
poznamenává	poznamenávat	k5eAaImIp3nS	poznamenávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
vydavatel	vydavatel	k1gMnSc1	vydavatel
varoval	varovat	k5eAaImAgMnS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
každé	každý	k3xTgFnSc6	každý
rovnici	rovnice	k1gFnSc6	rovnice
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
by	by	kYmCp3nS	by
ve	v	k7c6	v
čtení	čtení	k1gNnSc6	čtení
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
pouze	pouze	k6eAd1	pouze
polovina	polovina	k1gFnSc1	polovina
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
jedinou	jediný	k2eAgFnSc4d1	jediná
rovnici	rovnice	k1gFnSc4	rovnice
<g/>
:	:	kIx,	:
E	E	kA	E
<g/>
=	=	kIx~	=
<g/>
mc2	mc2	k4	mc2
<g/>
.	.	kIx.	.
</s>
<s>
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-204-0169-5	[number]	k4	80-204-0169-5
</s>
