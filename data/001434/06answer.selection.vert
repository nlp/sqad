<s>
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
A	a	k9	a
Brief	Brief	k1gInSc1	Brief
History	Histor	k1gInPc1	Histor
of	of	k?	of
Time	Tim	k1gInSc2	Tim
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
populárně	populárně	k6eAd1	populárně
vědecká	vědecký	k2eAgFnSc1d1	vědecká
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
kosmolog	kosmolog	k1gMnSc1	kosmolog
Stephen	Stephen	k2eAgInSc4d1	Stephen
Hawking	Hawking	k1gInSc4	Hawking
<g/>
.	.	kIx.	.
</s>
