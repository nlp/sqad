<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
hmotná	hmotný	k2eAgNnPc4d1	hmotné
tělesa	těleso	k1gNnPc4	těleso
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
<g/>
.	.	kIx.	.
</s>
