<s>
Karát	karát	k1gInSc1	karát
(	(	kIx(	(
<g/>
značeno	značit	k5eAaImNgNnS	značit
zpravidla	zpravidla	k6eAd1	zpravidla
Kt	Kt	k1gMnPc2	Kt
nebo	nebo	k8xC	nebo
kt	kt	k?	kt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bezrozměrná	bezrozměrný	k2eAgFnSc1d1	bezrozměrná
jednotka	jednotka	k1gFnSc1	jednotka
ryzosti	ryzost	k1gFnSc2	ryzost
klenotnických	klenotnický	k2eAgFnPc2d1	klenotnická
zlatých	zlatý	k2eAgFnPc2d1	zlatá
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Ryzost	ryzost	k1gFnSc1	ryzost
čistého	čistý	k2eAgNnSc2d1	čisté
zlata	zlato	k1gNnSc2	zlato
je	být	k5eAaImIp3nS	být
definována	definován	k2eAgFnSc1d1	definována
jako	jako	k8xC	jako
24	[number]	k4	24
kt	kt	k?	kt
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
karát	karát	k1gInSc1	karát
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
hmotnostního	hmotnostní	k2eAgInSc2d1	hmotnostní
podílu	podíl	k1gInSc2	podíl
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
hodnota	hodnota	k1gFnSc1	hodnota
v	v	k7c6	v
karátech	karát	k1gInPc6	karát
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
rovna	roven	k2eAgFnSc1d1	rovna
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
=	=	kIx~	=
24	[number]	k4	24
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
u	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
<g/>
24	[number]	k4	24
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
M_	M_	k1gFnSc1	M_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Au	au	k0	au
<g/>
}	}	kIx)	}
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
M	M	kA	M
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
ryzost	ryzost	k1gFnSc1	ryzost
v	v	k7c6	v
karátech	karát	k1gInPc6	karát
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
u	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M_	M_	k1gMnPc6	M_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
Au	au	k0	au
<g/>
}	}	kIx)	}
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
čistého	čistý	k2eAgNnSc2d1	čisté
zlata	zlato	k1gNnSc2	zlato
obsaženého	obsažený	k2eAgNnSc2d1	obsažené
v	v	k7c6	v
hodnoceném	hodnocený	k2eAgInSc6d1	hodnocený
materiálu	materiál	k1gInSc6	materiál
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
hodnoceného	hodnocený	k2eAgInSc2d1	hodnocený
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
obsah	obsah	k1gInSc4	obsah
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
běžně	běžně	k6eAd1	běžně
používaných	používaný	k2eAgFnPc6d1	používaná
slitinách	slitina	k1gFnPc6	slitina
zlata	zlato	k1gNnSc2	zlato
v	v	k7c6	v
karátech	karát	k1gInPc6	karát
a	a	k8xC	a
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
šperky	šperk	k1gInPc1	šperk
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
z	z	k7c2	z
čistých	čistý	k2eAgInPc2d1	čistý
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
získávaly	získávat	k5eAaImAgFnP	získávat
z	z	k7c2	z
rýžování	rýžování	k1gNnSc2	rýžování
<g/>
,	,	kIx,	,
či	či	k8xC	či
těžby	těžba	k1gFnSc2	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
drahé	drahý	k2eAgInPc1d1	drahý
kovy	kov	k1gInPc1	kov
začaly	začít	k5eAaPmAgInP	začít
legovat	legovat	k5eAaImF	legovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
docíleny	docílen	k2eAgFnPc1d1	docílena
další	další	k2eAgFnPc1d1	další
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jako	jako	k8xS	jako
barva	barva	k1gFnSc1	barva
či	či	k8xC	či
tvrdost	tvrdost	k1gFnSc1	tvrdost
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
se	se	k3xPyFc4	se
tomu	ten	k3xDgInSc3	ten
děje	dít	k5eAaImIp3nS	dít
u	u	k7c2	u
legování	legování	k1gNnSc2	legování
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
bronzu	bronz	k1gInSc2	bronz
atd.	atd.	kA	atd.
Označení	označení	k1gNnSc1	označení
karát	karát	k1gInSc1	karát
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
dvanáctková	dvanáctkový	k2eAgFnSc1d1	dvanáctková
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
používáním	používání	k1gNnSc7	používání
procent	procento	k1gNnPc2	procento
<g/>
,	,	kIx,	,
či	či	k8xC	či
desítkové	desítkový	k2eAgFnSc2d1	desítková
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Karát	karát	k1gInSc1	karát
jako	jako	k8xS	jako
vyjádření	vyjádření	k1gNnSc1	vyjádření
ryzosti	ryzost	k1gFnSc2	ryzost
<g/>
,	,	kIx,	,
kvality	kvalita	k1gFnSc2	kvalita
slitiny	slitina	k1gFnSc2	slitina
byl	být	k5eAaImAgInS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
ryzosti	ryzost	k1gFnSc2	ryzost
(	(	kIx(	(
<g/>
kvality	kvalita	k1gFnSc2	kvalita
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
zlato	zlato	k1gNnSc4	zlato
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
9	[number]	k4	9
kt	kt	k?	kt
<g/>
,	,	kIx,	,
14	[number]	k4	14
kt	kt	k?	kt
<g/>
,	,	kIx,	,
18	[number]	k4	18
kt	kt	k?	kt
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stříbro	stříbro	k1gNnSc4	stříbro
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
označení	označení	k1gNnSc4	označení
například	například	k6eAd1	například
13	[number]	k4	13
lotů	lot	k1gInPc2	lot
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgMnSc1d3	Nejběžnější
dnes	dnes	k6eAd1	dnes
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
klenotnické	klenotnický	k2eAgFnPc1d1	klenotnická
slitiny	slitina	k1gFnPc1	slitina
mají	mít	k5eAaImIp3nP	mít
ryzost	ryzost	k1gFnSc4	ryzost
14	[number]	k4	14
kt	kt	k?	kt
a	a	k8xC	a
18	[number]	k4	18
kt	kt	k?	kt
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
místních	místní	k2eAgFnPc6d1	místní
zvyklostech	zvyklost	k1gFnPc6	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
je	být	k5eAaImIp3nS	být
nejběžnější	běžný	k2eAgNnSc1d3	nejběžnější
zlato	zlato	k1gNnSc1	zlato
14	[number]	k4	14
kt	kt	k?	kt
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
9	[number]	k4	9
kt	kt	k?	kt
není	být	k5eNaImIp3nS	být
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
18	[number]	k4	18
kt	kt	k?	kt
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
drahé	drahý	k2eAgNnSc4d1	drahé
<g/>
,	,	kIx,	,
či	či	k8xC	či
luxusní	luxusní	k2eAgInSc1d1	luxusní
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
prodává	prodávat	k5eAaImIp3nS	prodávat
i	i	k8xC	i
zlato	zlato	k1gNnSc1	zlato
9	[number]	k4	9
kt	kt	k?	kt
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
arabských	arabský	k2eAgFnPc6d1	arabská
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
prodává	prodávat	k5eAaImIp3nS	prodávat
zlato	zlato	k1gNnSc1	zlato
s	s	k7c7	s
ryzostí	ryzost	k1gFnSc7	ryzost
nejméně	málo	k6eAd3	málo
18	[number]	k4	18
kt	kt	k?	kt
a	a	k8xC	a
zlato	zlato	k1gNnSc1	zlato
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
ryzostí	ryzost	k1gFnSc7	ryzost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
14	[number]	k4	14
kt	kt	k?	kt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
šperků	šperk	k1gInPc2	šperk
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
do	do	k7c2	do
drahého	drahý	k2eAgInSc2d1	drahý
kovu	kov	k1gInSc2	kov
přidává	přidávat	k5eAaImIp3nS	přidávat
tolik	tolik	k4xDc1	tolik
příměsí	příměs	k1gFnPc2	příměs
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
šperků	šperk	k1gInPc2	šperk
s	s	k7c7	s
dodržením	dodržení	k1gNnSc7	dodržení
ryzosti	ryzost	k1gFnSc2	ryzost
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
i	i	k8xC	i
ostatní	ostatní	k2eAgInPc1d1	ostatní
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
určují	určovat	k5eAaImIp3nP	určovat
výslednou	výsledný	k2eAgFnSc4d1	výsledná
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
14	[number]	k4	14
kt	kt	k?	kt
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
žluté	žlutý	k2eAgFnPc4d1	žlutá
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc4d1	červená
i	i	k8xC	i
bílé	bílý	k2eAgFnPc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
udávaná	udávaný	k2eAgFnSc1d1	udávaná
poměrem	poměr	k1gInSc7	poměr
přidávaných	přidávaný	k2eAgInPc2d1	přidávaný
zbývajících	zbývající	k2eAgInPc2d1	zbývající
10	[number]	k4	10
dílů	díl	k1gInPc2	díl
do	do	k7c2	do
celku	celek	k1gInSc2	celek
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
čtrnáctikarátové	čtrnáctikarátový	k2eAgNnSc4d1	čtrnáctikarátový
zlato	zlato	k1gNnSc4	zlato
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
Au	au	k0	au
14	[number]	k4	14
kt	kt	k?	kt
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc1	směs
58,5	[number]	k4	58,5
%	%	kIx~	%
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
32	[number]	k4	32
%	%	kIx~	%
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ryzostech	ryzost	k1gFnPc6	ryzost
tedy	tedy	k9	tedy
0,585	[number]	k4	0,585
<g/>
Au	au	k0	au
0,320	[number]	k4	0,320
<g/>
Ag	Ag	k1gFnPc1	Ag
...	...	k?	...
v	v	k7c6	v
zlatnické	zlatnický	k2eAgFnSc6d1	Zlatnická
žargonu	žargon	k1gInSc6	žargon
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
žlutá	žlutý	k2eAgFnSc1d1	žlutá
třistadvacítka	třistadvacítka	k1gFnSc1	třistadvacítka
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
zlatníci	zlatník	k1gMnPc1	zlatník
většinou	většinou	k6eAd1	většinou
nelegují	legovat	k5eNaImIp3nP	legovat
zlato	zlato	k1gNnSc4	zlato
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mění	měnit	k5eAaImIp3nS	měnit
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
ve	v	k7c6	v
zlatnickém	zlatnický	k2eAgInSc6d1	zlatnický
velkoobchodě	velkoobchod	k1gInSc6	velkoobchod
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Zlomkové	zlomkový	k2eAgNnSc1d1	zlomkové
zlato	zlato	k1gNnSc1	zlato
vymění	vyměnit	k5eAaPmIp3nS	vyměnit
za	za	k7c4	za
nové	nový	k2eAgFnPc4d1	nová
legované	legovaný	k2eAgFnPc4d1	legovaná
v	v	k7c6	v
patřičné	patřičný	k2eAgFnSc6d1	patřičná
ryzosti	ryzost	k1gFnSc6	ryzost
<g/>
,	,	kIx,	,
barvě	barva	k1gFnSc6	barva
a	a	k8xC	a
tvrdosti	tvrdost	k1gFnSc6	tvrdost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
karátů	karát	k1gInPc2	karát
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
tisícinné	tisícinný	k2eAgInPc1d1	tisícinný
poměry	poměr	k1gInPc1	poměr
–	–	k?	–
např.	např.	kA	např.
833	[number]	k4	833
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
833	[number]	k4	833
<g/>
/	/	kIx~	/
<g/>
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
namísto	namísto	k7c2	namísto
20	[number]	k4	20
Kt	Kt	k1gFnPc2	Kt
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
20	[number]	k4	20
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
neplést	plést	k5eNaImF	plést
si	se	k3xPyFc3	se
karát	karát	k1gInSc4	karát
ryzostní	ryzostnit	k5eAaPmIp3nS	ryzostnit
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
kt	kt	k?	kt
<g/>
)	)	kIx)	)
a	a	k8xC	a
váhový	váhový	k2eAgInSc1d1	váhový
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
ct	ct	k?	ct
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
výraz	výraz	k1gInSc1	výraz
náhrdelník	náhrdelník	k1gInSc1	náhrdelník
zlato	zlato	k1gNnSc1	zlato
18	[number]	k4	18
kt	kt	k?	kt
smaragd	smaragd	k1gInSc1	smaragd
18	[number]	k4	18
ct	ct	k?	ct
znamená	znamenat	k5eAaImIp3nS	znamenat
šperk	šperk	k1gInSc4	šperk
z	z	k7c2	z
18	[number]	k4	18
<g/>
kt	kt	k?	kt
zlata	zlato	k1gNnSc2	zlato
(	(	kIx(	(
<g/>
ryzost	ryzost	k1gFnSc1	ryzost
750	[number]	k4	750
<g/>
/	/	kIx~	/
<g/>
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
se	s	k7c7	s
smaragdem	smaragd	k1gInSc7	smaragd
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
18	[number]	k4	18
(	(	kIx(	(
<g/>
váhových	váhový	k2eAgInPc2d1	váhový
<g/>
)	)	kIx)	)
karátů	karát	k1gInPc2	karát
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
3,6	[number]	k4	3,6
gramu	gram	k1gInSc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Ryzost	ryzost	k1gFnSc1	ryzost
Karát	karát	k1gInSc1	karát
(	(	kIx(	(
<g/>
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
)	)	kIx)	)
Investiční	investiční	k2eAgNnSc1d1	investiční
zlato	zlato	k1gNnSc1	zlato
Zlato	zlato	k1gNnSc1	zlato
(	(	kIx(	(
<g/>
minerál	minerál	k1gInSc1	minerál
<g/>
)	)	kIx)	)
Co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
karát	karát	k1gInSc1	karát
<g/>
?	?	kIx.	?
</s>
<s>
Zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
ryzost	ryzost	k1gFnSc1	ryzost
a	a	k8xC	a
přepočet	přepočet	k1gInSc1	přepočet
ryzosti	ryzost	k1gFnSc2	ryzost
na	na	k7c4	na
Karáty	karát	k1gInPc4	karát
</s>
