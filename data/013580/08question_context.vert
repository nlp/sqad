<s desamb="1">
Hotel	hotel	k1gInSc1
byl	být	k5eAaImAgInS
navržen	navržen	k2eAgInSc4d1
Ing.	ing.	kA
Arch	arch	kA
<g/>
.	.	kIx.
</s>
<s>
Hotel	hotel	k1gInSc1
Bellevue	bellevue	k1gInSc1
-	-	kIx~
Tlapák	Tlapák	k1gInSc1
je	být	k5eAaImIp3nS
funkcionalistický	funkcionalistický	k2eAgInSc1d1
hotel	hotel	k1gInSc1
postavený	postavený	k2eAgInSc1d1
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
části	část	k1gFnSc6
lázeňského	lázeňský	k2eAgInSc2d1
parku	park	k1gInSc2
v	v	k7c6
Poděbradech	Poděbrady	k1gInPc6
<g/>
.	.	kIx.
</s>