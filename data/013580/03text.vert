<s>
Hotel	hotel	k1gInSc1
Bellevue	bellevue	k1gFnSc2
Tlapák	Tlapák	k1gMnSc1
</s>
<s>
Hotel	hotel	k1gInSc1
Bellevue	bellevue	k1gFnSc2
-	-	kIx~
Tlapák	Tlapák	k1gInSc1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Typ	typ	k1gInSc1
</s>
<s>
Funkcionalistický	funkcionalistický	k2eAgInSc1d1
lázeňský	lázeňský	k2eAgInSc1d1
hotel	hotel	k1gInSc1
(	(	kIx(
<g/>
****	****	k?
<g/>
)	)	kIx)
Adresa	adresa	k1gFnSc1
</s>
<s>
HOTEL	hotel	k1gInSc1
BELLEVUE	bellevue	k1gFnSc2
-	-	kIx~
TLAPÁK	TLAPÁK	kA
****	****	k?
<g/>
Náměstí	náměstí	k1gNnSc1
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
654	#num#	k4
<g/>
/	/	kIx~
<g/>
III	III	kA
Poděbrady	Poděbrady	k1gInPc1
<g/>
290	#num#	k4
33	#num#	k4
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Lokalita	lokalita	k1gFnSc1
</s>
<s>
Poděbrady	Poděbrady	k1gInPc1
Počet	počet	k1gInSc1
pokojů	pokoj	k1gInPc2
</s>
<s>
jednolůžkové	jednolůžkový	k2eAgInPc4d1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
dvoulůžkové	dvoulůžkový	k2eAgFnSc2d1
(	(	kIx(
<g/>
49	#num#	k4
<g/>
)	)	kIx)
<g/>
apartmány	apartmán	k1gInPc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
43,5	43,5	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
15,5	15,5	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Vybavení	vybavení	k1gNnSc1
hotelu	hotel	k1gInSc2
</s>
<s>
balneorehabilitační	balneorehabilitační	k2eAgFnSc1d1
centrumrestaurace	centrumrestaurace	k1gFnSc1
s	s	k7c7
kavárnouminerální	kavárnouminerální	k2eAgFnSc7d1
pramennonstop	pramennonstop	k1gInSc4
recepcevlastní	recepcevlastní	k2eAgNnSc4d1
parkoviště	parkoviště	k1gNnSc4
Vybavení	vybavení	k1gNnSc2
pokojů	pokoj	k1gInPc2
</s>
<s>
Koupelna	koupelna	k1gFnSc1
se	s	k7c7
sprchou	sprcha	k1gFnSc7
/	/	kIx~
vanouWCTVTelefonTrezorInternetMinibar	vanouWCTVTelefonTrezorInternetMinibar	k1gInSc1
</s>
<s>
Hotel	hotel	k1gInSc1
Bellevue	bellevue	k1gInSc1
-	-	kIx~
Tlapák	Tlapák	k1gInSc1
je	být	k5eAaImIp3nS
funkcionalistický	funkcionalistický	k2eAgInSc1d1
hotel	hotel	k1gInSc1
postavený	postavený	k2eAgInSc1d1
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
části	část	k1gFnSc6
lázeňského	lázeňský	k2eAgInSc2d1
parku	park	k1gInSc2
v	v	k7c6
Poděbradech	Poděbrady	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hotel	hotel	k1gInSc1
byl	být	k5eAaImAgInS
navržen	navržen	k2eAgInSc4d1
Ing.	ing.	kA
Arch	arch	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Františkem	František	k1gMnSc7
Tlapákem	Tlapák	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
také	také	k9
stal	stát	k5eAaPmAgInS
jeho	jeho	k3xOp3gInSc1
prvním	první	k4xOgMnSc6
majitelem	majitel	k1gMnSc7
a	a	k8xC
provozovatelem	provozovatel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
svém	svůj	k3xOyFgNnSc6
dokončení	dokončení	k1gNnSc6
budova	budova	k1gFnSc1
nesla	nést	k5eAaImAgFnS
název	název	k1gInSc4
Parkhotel	parkhotel	k1gInSc1
Tlapák	Tlapák	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
bezprostřední	bezprostřední	k2eAgFnSc6d1
blízkosti	blízkost	k1gFnSc6
hotelu	hotel	k1gInSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
květinové	květinový	k2eAgFnPc1d1
hodiny	hodina	k1gFnPc1
<g/>
,	,	kIx,
patřící	patřící	k2eAgFnSc1d1
od	od	k7c2
40	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
k	k	k7c3
symbolům	symbol	k1gInPc3
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1939-1945	1939-1945	k4
byl	být	k5eAaImAgInS
hotel	hotel	k1gInSc1
uzavřen	uzavřít	k5eAaPmNgInS
a	a	k8xC
následně	následně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
znárodněn	znárodnit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
novým	nový	k2eAgInSc7d1
názvem	název	k1gInSc7
Dům	dům	k1gInSc1
Fučík	Fučík	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
součástí	součást	k1gFnSc7
státní	státní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Lázně	lázeň	k1gFnSc2
Poděbrady	Poděbrady	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
byl	být	k5eAaImAgInS
objekt	objekt	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
restituce	restituce	k1gFnSc2
<g/>
,	,	kIx,
vrácen	vrátit	k5eAaPmNgInS
dědičce	dědička	k1gFnSc3
po	po	k7c6
architektu	architekt	k1gMnSc6
Tlapákovi	Tlapák	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následná	následný	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
původní	původní	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
Tlapák	Tlapák	k1gInSc1
a	a	k8xC
její	její	k3xOp3gNnSc1
rozšíření	rozšíření	k1gNnSc1
o	o	k7c4
nové	nový	k2eAgFnPc4d1
budovy	budova	k1gFnPc4
Homér	Homér	k1gMnSc1
a	a	k8xC
Jiřík	Jiřík	k1gMnSc1
<g/>
,	,	kIx,
vytvořily	vytvořit	k5eAaPmAgInP
současnou	současný	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
hotelového	hotelový	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
provozovatelem	provozovatel	k1gMnSc7
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc4
TICO	TICO	kA
IFC	IFC	kA
<g/>
,	,	kIx,
a.s.	a.s.	k?
</s>
<s>
Součástí	součást	k1gFnSc7
hotelového	hotelový	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
je	být	k5eAaImIp3nS
balneorehabilitační	balneorehabilitační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
s	s	k7c7
možností	možnost	k1gFnSc7
lékařské	lékařský	k2eAgFnSc2d1
a	a	k8xC
rehabilitační	rehabilitační	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
budovy	budova	k1gFnSc2
je	být	k5eAaImIp3nS
zavedena	zaveden	k2eAgFnSc1d1
minerální	minerální	k2eAgFnSc1d1
voda	voda	k1gFnSc1
Poděbradka	poděbradka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
byl	být	k5eAaImAgInS
hotel	hotel	k1gInSc1
rekonstruován	rekonstruovat	k5eAaBmNgInS
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
novým	nový	k2eAgMnSc7d1
provozovatelem	provozovatel	k1gMnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Lázně	lázeň	k1gFnSc2
Poděbrady	Poděbrady	k1gInPc1
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Hotel	hotel	k1gInSc1
Bellevue	bellevue	k1gFnSc2
-	-	kIx~
Tlapák	Tlapák	k1gInSc1
</s>
<s>
Květinové	květinový	k2eAgFnPc1d1
hodiny	hodina	k1gFnPc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ROBEK	robka	k1gFnPc2
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lázně	lázeň	k1gFnSc2
Poděbrady	Poděbrady	k1gInPc1
:	:	kIx,
1908-1978	1908-1978	k4
:	:	kIx,
historický	historický	k2eAgInSc1d1
nástin	nástin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poděbrady	Poděbrady	k1gInPc4
<g/>
:	:	kIx,
Polabské	polabský	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
v	v	k7c6
Poděbradech	Poděbrady	k1gInPc6
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZEMAN	Zeman	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
<g/>
;	;	kIx,
ZATLOUKAL	Zatloukal	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavné	slavný	k2eAgFnPc4d1
lázně	lázeň	k1gFnPc4
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Foibos	Foibos	k1gMnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
394	#num#	k4
<g/>
–	–	k?
<g/>
409	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Lázně	lázeň	k1gFnPc1
Poděbrady	Poděbrady	k1gInPc1
</s>
<s>
Poděbradka	poděbradka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
Hotelu	hotel	k1gInSc2
Bellevue	bellevue	k1gFnSc2
Tlapák	Tlapák	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
společnosti	společnost	k1gFnSc2
Lázně	lázeň	k1gFnSc2
Poděbrady	Poděbrady	k1gInPc4
a.	a.	k?
s.	s.	k?
</s>
<s>
Facebook	Facebook	k1gInSc1
Lázně	lázeň	k1gFnSc2
Poděbrady	Poděbrady	k1gInPc4
a.	a.	k?
s.	s.	k?
</s>
<s>
Twitter	Twitter	k1gMnSc1
Lázně	lázeň	k1gFnSc2
Poděbrady	Poděbrady	k1gInPc4
a.	a.	k?
s.	s.	k?
</s>
<s>
Youtube	Youtubat	k5eAaPmIp3nS
kanál	kanál	k1gInSc4
Lázně	lázeň	k1gFnSc2
Poděbrady	Poděbrady	k1gInPc4
a.	a.	k?
s.	s.	k?
</s>
