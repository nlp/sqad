<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Koperník	Koperník	k1gMnSc1	Koperník
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
Mikołaj	Mikołaj	k1gMnSc1	Mikołaj
Kopernik	Kopernik	k1gMnSc1	Kopernik
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1473	[number]	k4	1473
<g/>
,	,	kIx,	,
Toruň	Toruň	k1gFnSc1	Toruň
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1543	[number]	k4	1543
<g/>
,	,	kIx,	,
Frombork	Frombork	k1gInSc1	Frombork
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
polský	polský	k2eAgMnSc1d1	polský
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
stratég	stratég	k1gMnSc1	stratég
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
duchovní	duchovní	k1gMnSc1	duchovní
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc2	tvůrce
heliocentrické	heliocentrický	k2eAgFnSc2d1	heliocentrická
(	(	kIx(	(
<g/>
sluncestředné	sluncestředný	k2eAgFnSc2d1	sluncestředný
<g/>
)	)	kIx)	)
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenovány	pojmenován	k2eAgInPc1d1	pojmenován
krátery	kráter	k1gInPc1	kráter
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
(	(	kIx(	(
<g/>
Koperník	Koperník	k1gInSc1	Koperník
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
(	(	kIx(	(
<g/>
Koperník	Koperník	k1gMnSc1	Koperník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
objevený	objevený	k2eAgInSc4d1	objevený
prvek	prvek	k1gInSc4	prvek
kopernicium	kopernicium	k1gNnSc4	kopernicium
s	s	k7c7	s
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
112	[number]	k4	112
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
kupce	kupec	k1gMnSc2	kupec
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
a	a	k8xC	a
Barbary	Barbara	k1gFnSc2	Barbara
rozené	rozený	k2eAgFnSc2d1	rozená
Watzenrode	Watzenrod	k1gMnSc5	Watzenrod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otec	k1gMnSc2	otec
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
ujal	ujmout	k5eAaPmAgMnS	ujmout
strýc	strýc	k1gMnSc1	strýc
(	(	kIx(	(
<g/>
matčin	matčin	k2eAgMnSc1d1	matčin
bratr	bratr	k1gMnSc1	bratr
<g/>
)	)	kIx)	)
Lukáš	Lukáš	k1gMnSc1	Lukáš
Watzenrode	Watzenrod	k1gInSc5	Watzenrod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1489	[number]	k4	1489
zvolen	zvolit	k5eAaPmNgInS	zvolit
biskupem	biskup	k1gInSc7	biskup
warminským	warminský	k2eAgInSc7d1	warminský
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
úsilí	úsilí	k1gNnSc3	úsilí
strýce	strýc	k1gMnSc2	strýc
ukončil	ukončit	k5eAaPmAgMnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1491	[number]	k4	1491
farní	farní	k2eAgFnSc4d1	farní
školu	škola	k1gFnSc4	škola
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
v	v	k7c6	v
Toruni	Toruň	k1gFnSc6	Toruň
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgMnS	zahájit
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
krakovské	krakovský	k2eAgFnSc6d1	Krakovská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zakončil	zakončit	k5eAaPmAgMnS	zakončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1495	[number]	k4	1495
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1496	[number]	k4	1496
začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Boloni	Boloňa	k1gFnSc6	Boloňa
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
právnickou	právnický	k2eAgFnSc4d1	právnická
praxi	praxe	k1gFnSc4	praxe
v	v	k7c6	v
papežské	papežský	k2eAgFnSc6d1	Papežská
kanceláři	kancelář	k1gFnSc6	kancelář
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
získal	získat	k5eAaPmAgMnS	získat
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
studií	studie	k1gFnPc2	studie
medicíny	medicína	k1gFnSc2	medicína
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pokračováním	pokračování	k1gNnSc7	pokračování
studia	studio	k1gNnSc2	studio
práva	práv	k2eAgFnSc1d1	práva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1503	[number]	k4	1503
se	se	k3xPyFc4	se
ve	v	k7c6	v
Ferraře	Ferrara	k1gFnSc6	Ferrara
stal	stát	k5eAaPmAgInS	stát
doktorem	doktor	k1gMnSc7	doktor
kanonického	kanonický	k2eAgNnSc2d1	kanonické
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
ukončil	ukončit	k5eAaPmAgInS	ukončit
lékařská	lékařský	k2eAgNnPc4d1	lékařské
studia	studio	k1gNnPc4	studio
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
právo	právo	k1gNnSc4	právo
provádět	provádět	k5eAaImF	provádět
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
fromborské	fromborský	k2eAgFnSc2d1	fromborský
kapituly	kapitula	k1gFnSc2	kapitula
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1507	[number]	k4	1507
osobním	osobnit	k5eAaImIp1nS	osobnit
lékařem	lékař	k1gMnSc7	lékař
varmiňského	varmiňský	k2eAgInSc2d1	varmiňský
biskupa	biskup	k1gInSc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
komentář	komentář	k1gInSc4	komentář
k	k	k7c3	k
teorii	teorie	k1gFnSc3	teorie
pohybu	pohyb	k1gInSc2	pohyb
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
náčrt	náčrt	k1gInSc1	náčrt
heliocentrické	heliocentrický	k2eAgFnSc2d1	heliocentrická
teorie	teorie	k1gFnSc2	teorie
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
ho	on	k3xPp3gNnSc4	on
v	v	k7c6	v
četných	četný	k2eAgInPc6d1	četný
dopisech	dopis	k1gInPc6	dopis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1510	[number]	k4	1510
se	se	k3xPyFc4	se
přenesl	přenést	k5eAaPmAgInS	přenést
do	do	k7c2	do
Fromborku	Fromborek	k1gInSc2	Fromborek
a	a	k8xC	a
sestavil	sestavit	k5eAaPmAgMnS	sestavit
mapu	mapa	k1gFnSc4	mapa
Warmie	Warmie	k1gFnSc2	Warmie
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
fromborskou	fromborský	k2eAgFnSc7d1	fromborský
kapitulou	kapitula	k1gFnSc7	kapitula
složil	složit	k5eAaPmAgMnS	složit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1512	[number]	k4	1512
přísahu	přísaha	k1gFnSc4	přísaha
věrnosti	věrnost	k1gFnSc2	věrnost
polskému	polský	k2eAgMnSc3d1	polský
králi	král	k1gMnSc3	král
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
I.	I.	kA	I.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1513	[number]	k4	1513
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
lateránského	lateránský	k2eAgInSc2d1	lateránský
koncilu	koncil	k1gInSc2	koncil
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
a	a	k8xC	a
poslal	poslat	k5eAaPmAgMnS	poslat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
vlastní	vlastní	k2eAgInSc4d1	vlastní
projekt	projekt	k1gInSc4	projekt
reformy	reforma	k1gFnSc2	reforma
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Fromborku	Fromborek	k1gInSc6	Fromborek
si	se	k3xPyFc3	se
opatřil	opatřit	k5eAaPmAgMnS	opatřit
dům	dům	k1gInSc4	dům
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
příznivém	příznivý	k2eAgNnSc6d1	příznivé
k	k	k7c3	k
astronomickým	astronomický	k2eAgNnPc3d1	astronomické
pozorováním	pozorování	k1gNnPc3	pozorování
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
pozorovatelnu	pozorovatelna	k1gFnSc4	pozorovatelna
a	a	k8xC	a
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
astronomické	astronomický	k2eAgInPc4d1	astronomický
přístroje	přístroj	k1gInPc4	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1514	[number]	k4	1514
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
De	De	k?	De
Revolutionibus	Revolutionibus	k1gMnSc1	Revolutionibus
Orbium	Orbium	k1gNnSc1	Orbium
Coelestium	Coelestium	k1gNnSc4	Coelestium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1516	[number]	k4	1516
až	až	k9	až
1519	[number]	k4	1519
působil	působit	k5eAaImAgInS	působit
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
správce	správce	k1gMnSc2	správce
kapitulního	kapitulní	k2eAgInSc2d1	kapitulní
majetku	majetek	k1gInSc2	majetek
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Olštýně	Olštýna	k1gFnSc6	Olštýna
<g/>
.	.	kIx.	.
</s>
<s>
Přivedl	přivést	k5eAaPmAgMnS	přivést
nové	nový	k2eAgMnPc4d1	nový
osadníky	osadník	k1gMnPc4	osadník
na	na	k7c4	na
hospodářství	hospodářství	k1gNnSc4	hospodářství
kapituly	kapitula	k1gFnSc2	kapitula
a	a	k8xC	a
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
připravoval	připravovat	k5eAaImAgMnS	připravovat
olštýnský	olštýnský	k2eAgInSc4d1	olštýnský
hrad	hrad	k1gInSc4	hrad
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
před	před	k7c7	před
očekávaným	očekávaný	k2eAgInSc7d1	očekávaný
útokem	útok	k1gInSc7	útok
křižáků	křižák	k1gMnPc2	křižák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1522	[number]	k4	1522
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
stavů	stav	k1gInPc2	stav
Královského	královský	k2eAgNnSc2d1	královské
Pruska	Prusko	k1gNnSc2	Prusko
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Grudziądz	Grudziądz	k1gMnSc1	Grudziądz
přednesl	přednést	k5eAaPmAgMnS	přednést
traktát	traktát	k1gInSc4	traktát
o	o	k7c6	o
mincích	mince	k1gFnPc6	mince
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
úřadech	úřad	k1gInPc6	úřad
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
poslancem	poslanec	k1gMnSc7	poslanec
<g/>
,	,	kIx,	,
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
,	,	kIx,	,
vizitátorem	vizitátor	k1gMnSc7	vizitátor
a	a	k8xC	a
kapitulním	kapitulní	k2eAgMnSc7d1	kapitulní
správcem	správce	k1gMnSc7	správce
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Bernardem	Bernard	k1gMnSc7	Bernard
Wapowským	Wapowský	k1gMnSc7	Wapowský
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
Království	království	k1gNnSc4	království
polského	polský	k2eAgMnSc2d1	polský
a	a	k8xC	a
Litvy	Litva	k1gFnPc4	Litva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1533	[number]	k4	1533
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
názory	názor	k1gInPc1	názor
v	v	k7c6	v
rukopise	rukopis	k1gInSc6	rukopis
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
De	De	k?	De
Revolutionibus	Revolutionibus	k1gMnSc1	Revolutionibus
předneseny	přednesen	k2eAgInPc4d1	přednesen
papeži	papež	k1gMnSc3	papež
Klemensovi	Klemensův	k2eAgMnPc1d1	Klemensův
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1537	[number]	k4	1537
byl	být	k5eAaImAgMnS	být
králem	král	k1gMnSc7	král
potvrzen	potvrdit	k5eAaPmNgMnS	potvrdit
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c6	na
biskupa	biskup	k1gMnSc2	biskup
warminského	warminský	k2eAgMnSc2d1	warminský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1539	[number]	k4	1539
jej	on	k3xPp3gMnSc4	on
biskup	biskup	k1gMnSc1	biskup
Jan	Jan	k1gMnSc1	Jan
Dantyszek	Dantyszek	k1gInSc4	Dantyszek
obvinil	obvinit	k5eAaPmAgMnS	obvinit
z	z	k7c2	z
konkubinátu	konkubinát	k1gInSc2	konkubinát
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
nařídil	nařídit	k5eAaPmAgInS	nařídit
mu	on	k3xPp3gMnSc3	on
propustit	propustit	k5eAaPmF	propustit
hospodyni	hospodyně	k1gFnSc4	hospodyně
a	a	k8xC	a
připravoval	připravovat	k5eAaImAgMnS	připravovat
kanonický	kanonický	k2eAgInSc4d1	kanonický
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Mikuláši	Mikuláš	k1gMnSc3	Mikuláš
Koperníkovi	Koperník	k1gMnSc3	Koperník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1539	[number]	k4	1539
přijel	přijet	k5eAaPmAgMnS	přijet
Georg	Georg	k1gMnSc1	Georg
Joachim	Joachim	k1gMnSc1	Joachim
von	von	k1gInSc4	von
Lauchen	Lauchen	k2eAgInSc4d1	Lauchen
zvaný	zvaný	k2eAgInSc4d1	zvaný
Rhaeticus	Rhaeticus	k1gInSc4	Rhaeticus
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
matematiky	matematika	k1gFnSc2	matematika
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
naukou	nauka	k1gFnSc7	nauka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
své	své	k1gNnSc4	své
mínění	mínění	k1gNnSc2	mínění
o	o	k7c6	o
Mikuláši	mikuláš	k1gInSc6	mikuláš
Koperníkovi	Koperník	k1gMnSc3	Koperník
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
<g/>
:	:	kIx,	:
Ten	ten	k3xDgMnSc1	ten
hlupák	hlupák	k1gMnSc1	hlupák
chce	chtít	k5eAaImIp3nS	chtít
převrátit	převrátit	k5eAaPmF	převrátit
celé	celý	k2eAgNnSc4d1	celé
umění	umění	k1gNnSc4	umění
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
!	!	kIx.	!
</s>
<s>
Ale	ale	k9	ale
jak	jak	k6eAd1	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
Písmo	písmo	k1gNnSc4	písmo
svaté	svatý	k2eAgFnSc2d1	svatá
<g/>
,	,	kIx,	,
Jozue	Jozue	k1gMnSc1	Jozue
kázal	kázat	k5eAaImAgMnS	kázat
zastavit	zastavit	k5eAaPmF	zastavit
se	se	k3xPyFc4	se
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
a	a	k8xC	a
ne	ne	k9	ne
Zemi	zem	k1gFnSc4	zem
<g/>
!	!	kIx.	!
</s>
<s>
Norimberský	norimberský	k2eAgMnSc1d1	norimberský
teolog	teolog	k1gMnSc1	teolog
Andreas	Andreas	k1gMnSc1	Andreas
Osiander	Osiander	k1gMnSc1	Osiander
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
Koperníka	Koperník	k1gMnSc4	Koperník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
předmluvě	předmluva	k1gFnSc6	předmluva
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
dílu	dílo	k1gNnSc3	dílo
představil	představit	k5eAaPmAgInS	představit
svůj	svůj	k3xOyFgInSc4	svůj
model	model	k1gInSc4	model
jako	jako	k8xS	jako
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nP	by
zjemnil	zjemnit	k5eAaPmAgInS	zjemnit
příliš	příliš	k6eAd1	příliš
odvážné	odvážný	k2eAgFnPc4d1	odvážná
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1541	[number]	k4	1541
tiskem	tisk	k1gInSc7	tisk
druhé	druhý	k4xOgNnSc4	druhý
vydání	vydání	k1gNnSc4	vydání
Narratio	Narratio	k6eAd1	Narratio
prima	prima	k6eAd1	prima
<g/>
,	,	kIx,	,
Koperník	Koperník	k1gInSc1	Koperník
vykonal	vykonat	k5eAaPmAgInS	vykonat
své	svůj	k3xOyFgNnSc4	svůj
poslední	poslední	k2eAgNnSc4d1	poslední
astronomické	astronomický	k2eAgNnSc4d1	astronomické
pozorování	pozorování	k1gNnSc4	pozorování
v	v	k7c6	v
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
zatmění	zatmění	k1gNnSc6	zatmění
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
jeho	jeho	k3xOp3gMnSc1	jeho
žák	žák	k1gMnSc1	žák
Georg	Georg	k1gMnSc1	Georg
Joachim	Joachim	k1gMnSc1	Joachim
zv	zv	k?	zv
<g/>
.	.	kIx.	.
</s>
<s>
Reticus	Reticus	k1gInSc1	Reticus
opouští	opouštět	k5eAaImIp3nS	opouštět
Frombork	Frombork	k1gInSc4	Frombork
s	s	k7c7	s
rukopisem	rukopis	k1gInSc7	rukopis
De	De	k?	De
Revolutionibus	Revolutionibus	k1gInSc1	Revolutionibus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
vytisknout	vytisknout	k5eAaPmF	vytisknout
<g/>
.	.	kIx.	.
</s>
<s>
Úplný	úplný	k2eAgInSc1d1	úplný
název	název	k1gInSc1	název
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Koperník	Koperník	k1gMnSc1	Koperník
shrnul	shrnout	k5eAaPmAgMnS	shrnout
své	svůj	k3xOyFgInPc4	svůj
poznatky	poznatek	k1gInPc4	poznatek
v	v	k7c4	v
celistvou	celistvý	k2eAgFnSc4d1	celistvá
heliocentrickou	heliocentrický	k2eAgFnSc4d1	heliocentrická
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
zní	znět	k5eAaImIp3nS	znět
De	De	k?	De
Revolutionibus	Revolutionibus	k1gInSc1	Revolutionibus
orbium	orbium	k1gNnSc1	orbium
coelesticum	coelesticum	k1gInSc1	coelesticum
libri	libri	k1gNnSc4	libri
VI	VI	kA	VI
(	(	kIx(	(
<g/>
Šest	šest	k4xCc4	šest
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
obězích	oběh	k1gInPc6	oběh
sfér	sféra	k1gFnPc2	sféra
nebeských	nebeský	k2eAgFnPc2d1	nebeská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lutherův	Lutherův	k2eAgMnSc1d1	Lutherův
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Filip	Filip	k1gMnSc1	Filip
Melanchton	Melanchton	k1gInSc4	Melanchton
shrnul	shrnout	k5eAaPmAgMnS	shrnout
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
na	na	k7c4	na
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
Koperníka	Koperník	k1gMnSc4	Koperník
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Někteří	některý	k3yIgMnPc1	některý
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vynikající	vynikající	k2eAgMnSc1d1	vynikající
vypracovat	vypracovat	k5eAaPmF	vypracovat
věc	věc	k1gFnSc4	věc
tak	tak	k6eAd1	tak
absurdní	absurdní	k2eAgMnSc1d1	absurdní
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
onen	onen	k3xDgMnSc1	onen
sarmatský	sarmatský	k2eAgMnSc1d1	sarmatský
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
Zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
zadržuje	zadržovat	k5eAaImIp3nS	zadržovat
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Moudří	moudrý	k2eAgMnPc1d1	moudrý
vládci	vládce	k1gMnPc1	vládce
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
zajisté	zajisté	k9	zajisté
ovládnout	ovládnout	k5eAaPmF	ovládnout
talentovanou	talentovaný	k2eAgFnSc4d1	talentovaná
lehkomyslnost	lehkomyslnost	k1gFnSc4	lehkomyslnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1542	[number]	k4	1542
vyšly	vyjít	k5eAaPmAgFnP	vyjít
z	z	k7c2	z
tiskařských	tiskařský	k2eAgInPc2d1	tiskařský
lisů	lis	k1gInPc2	lis
první	první	k4xOgInPc1	první
dva	dva	k4xCgInPc1	dva
archy	arch	k1gInPc1	arch
De	De	k?	De
Revolutionibus	Revolutionibus	k1gInSc1	Revolutionibus
<g/>
.	.	kIx.	.
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Koperník	Koperník	k1gMnSc1	Koperník
posílá	posílat	k5eAaImIp3nS	posílat
do	do	k7c2	do
Norimberka	Norimberk	k1gInSc2	Norimberk
předmluvu	předmluva	k1gFnSc4	předmluva
dedikovanou	dedikovaný	k2eAgFnSc4d1	dedikovaná
papeži	papež	k1gMnSc3	papež
Pavlu	Pavla	k1gFnSc4	Pavla
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Kapitoly	kapitola	k1gFnPc1	kapitola
13	[number]	k4	13
a	a	k8xC	a
14	[number]	k4	14
první	první	k4xOgInSc4	první
knihy	kniha	k1gFnSc2	kniha
vyšly	vyjít	k5eAaPmAgInP	vyjít
ve	v	k7c6	v
Wittenbergu	Wittenberg	k1gInSc6	Wittenberg
tiskem	tisk	k1gInSc7	tisk
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
samostatné	samostatný	k2eAgFnSc2d1	samostatná
knihy	kniha	k1gFnSc2	kniha
De	De	k?	De
lateribus	lateribus	k1gInSc1	lateribus
et	et	k?	et
angulis	angulis	k1gInSc1	angulis
triangulorum	triangulorum	k1gInSc1	triangulorum
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
O	o	k7c6	o
stěnách	stěna	k1gFnPc6	stěna
a	a	k8xC	a
úhlech	úhel	k1gInPc6	úhel
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
<g/>
)	)	kIx)	)
s	s	k7c7	s
předmluvou	předmluva	k1gFnSc7	předmluva
Georga	Georg	k1gMnSc2	Georg
Joachima	Joachim	k1gMnSc2	Joachim
zv	zv	k?	zv
<g/>
.	.	kIx.	.
</s>
<s>
Retica	Retica	k6eAd1	Retica
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
tiskem	tisk	k1gInSc7	tisk
celá	celý	k2eAgFnSc1d1	celá
kniha	kniha	k1gFnSc1	kniha
De	De	k?	De
Revolutionibus	Revolutionibus	k1gInSc1	Revolutionibus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
byla	být	k5eAaImAgNnP	být
především	především	k6eAd1	především
církevními	církevní	k2eAgFnPc7d1	církevní
autoritami	autorita	k1gFnPc7	autorita
považována	považován	k2eAgNnPc1d1	považováno
přinejmenším	přinejmenším	k6eAd1	přinejmenším
dalších	další	k2eAgNnPc6d1	další
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
za	za	k7c4	za
nepřijatelnou	přijatelný	k2eNgFnSc4d1	nepřijatelná
<g/>
,	,	kIx,	,
přinesla	přinést	k5eAaPmAgFnS	přinést
základní	základní	k2eAgInSc4d1	základní
obrat	obrat	k1gInSc4	obrat
v	v	k7c6	v
chápání	chápání	k1gNnSc6	chápání
místa	místo	k1gNnSc2	místo
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
kosmu	kosmos	k1gInSc6	kosmos
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ptolemaiovského	ptolemaiovský	k2eAgInSc2d1	ptolemaiovský
systému	systém	k1gInSc2	systém
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
přiznat	přiznat	k5eAaPmF	přiznat
Zemi	zem	k1gFnSc4	zem
nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
všehomíra	všehomír	k1gInSc2	všehomír
<g/>
.	.	kIx.	.
</s>
<s>
Koperníkův	Koperníkův	k2eAgInSc1d1	Koperníkův
spis	spis	k1gInSc1	spis
De	De	k?	De
Revolutionibus	Revolutionibus	k1gInSc4	Revolutionibus
zařadila	zařadit	k5eAaPmAgFnS	zařadit
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
roku	rok	k1gInSc2	rok
1616	[number]	k4	1616
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
zákazu	zákaz	k1gInSc2	zákaz
šíření	šíření	k1gNnSc2	šíření
Koperníkovy	Koperníkův	k2eAgFnSc2d1	Koperníkova
teorie	teorie	k1gFnSc2	teorie
docházelo	docházet	k5eAaImAgNnS	docházet
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
bylo	být	k5eAaImAgNnS	být
církví	církev	k1gFnSc7	církev
povoleno	povolen	k2eAgNnSc4d1	povoleno
vytištění	vytištění	k1gNnSc4	vytištění
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnSc1	jejíž
autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
zastával	zastávat	k5eAaImAgMnS	zastávat
soustavy	soustava	k1gFnSc2	soustava
Koperníkovy	Koperníkův	k2eAgFnSc2d1	Koperníkova
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
příslušná	příslušný	k2eAgFnSc1d1	příslušná
římská	římský	k2eAgFnSc1d1	římská
kongregace	kongregace	k1gFnSc1	kongregace
že	že	k8xS	že
"	"	kIx"	"
<g/>
tisknouti	tisknout	k5eAaImF	tisknout
a	a	k8xC	a
vydávati	vydávat	k5eAaImF	vydávat
knihy	kniha	k1gFnPc4	kniha
jednající	jednající	k2eAgFnPc4d1	jednající
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
klidu	klid	k1gInSc2	klid
Slunce	slunce	k1gNnSc2	slunce
dle	dle	k7c2	dle
všeobecného	všeobecný	k2eAgInSc2d1	všeobecný
názoru	názor	k1gInSc2	názor
novějších	nový	k2eAgMnPc2d2	novější
hvězdářů	hvězdář	k1gMnPc2	hvězdář
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
je	být	k5eAaImIp3nS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Přesto	přesto	k8xC	přesto
ještě	ještě	k9	ještě
ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
Indexu	index	k1gInSc2	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
je	být	k5eAaImIp3nS	být
Koperníkovo	Koperníkův	k2eAgNnSc1d1	Koperníkovo
dílo	dílo	k1gNnSc1	dílo
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
vydání	vydání	k1gNnSc6	vydání
Indexu	index	k1gInSc2	index
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
již	jenž	k3xRgFnSc4	jenž
Koperníkův	Koperníkův	k2eAgInSc1d1	Koperníkův
spis	spis	k1gInSc1	spis
není	být	k5eNaImIp3nS	být
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
moderního	moderní	k2eAgInSc2d1	moderní
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
přisvojují	přisvojovat	k5eAaImIp3nP	přisvojovat
Poláci	Polák	k1gMnPc1	Polák
i	i	k8xC	i
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
současné	současný	k2eAgNnSc1d1	současné
pojetí	pojetí	k1gNnSc1	pojetí
národnosti	národnost	k1gFnSc2	národnost
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
neexistovalo	existovat	k5eNaImAgNnS	existovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
národním	národní	k2eAgInSc7d1	národní
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
poddaným	poddaný	k1gMnSc7	poddaný
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
bojů	boj	k1gInPc2	boj
proti	proti	k7c3	proti
křižákům	křižák	k1gMnPc3	křižák
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozdělení	rozdělení	k1gNnSc1	rozdělení
politické	politický	k2eAgNnSc1d1	politické
a	a	k8xC	a
etnicko-jazykově-národnostní	etnickoazykověárodnostní	k2eAgNnSc1d1	etnicko-jazykově-národnostní
se	se	k3xPyFc4	se
překrývalo	překrývat	k5eAaImAgNnS	překrývat
jen	jen	k9	jen
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zdrojů	zdroj	k1gInPc2	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Koperník	Koperník	k1gMnSc1	Koperník
byl	být	k5eAaImAgMnS	být
Polákem	Polák	k1gMnSc7	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Soudobí	soudobý	k2eAgMnPc1d1	soudobý
vědci	vědec	k1gMnPc1	vědec
ho	on	k3xPp3gNnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
polského	polský	k2eAgMnSc4d1	polský
astronoma	astronom	k1gMnSc4	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Dedikoval	dedikovat	k5eAaBmAgMnS	dedikovat
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
polskému	polský	k2eAgMnSc3d1	polský
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
původem	původ	k1gInSc7	původ
Němka	Němka	k1gFnSc1	Němka
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
otce	otec	k1gMnSc2	otec
je	být	k5eAaImIp3nS	být
sporný	sporný	k2eAgInSc1d1	sporný
<g/>
.	.	kIx.	.
</s>
<s>
Příjmení	příjmení	k1gNnSc1	příjmení
Koperník	Koperník	k1gMnSc1	Koperník
může	moct	k5eAaImIp3nS	moct
pocházet	pocházet	k5eAaImF	pocházet
od	od	k7c2	od
dolnoněmeckého	dolnoněmecký	k2eAgMnSc2d1	dolnoněmecký
Kopper	Kopper	k1gInSc4	Kopper
(	(	kIx(	(
<g/>
měď	měď	k1gFnSc4	měď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
polskou	polský	k2eAgFnSc4d1	polská
koncovku	koncovka	k1gFnSc4	koncovka
"	"	kIx"	"
<g/>
-nik	ik	k6eAd1	-nik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toruň	Toruň	k1gFnSc4	Toruň
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
německým	německý	k2eAgNnSc7d1	německé
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
uměl	umět	k5eAaImAgMnS	umět
polsky	polsky	k6eAd1	polsky
<g/>
.	.	kIx.	.
</s>
<s>
Zachovaly	zachovat	k5eAaPmAgFnP	zachovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
jeho	jeho	k3xOp3gInPc1	jeho
latinské	latinský	k2eAgInPc1d1	latinský
a	a	k8xC	a
německé	německý	k2eAgInPc1d1	německý
texty	text	k1gInPc1	text
<g/>
.	.	kIx.	.
</s>
<s>
Polština	polština	k1gFnSc1	polština
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
písemném	písemný	k2eAgInSc6d1	písemný
projevu	projev	k1gInSc6	projev
příliš	příliš	k6eAd1	příliš
používána	používán	k2eAgFnSc1d1	používána
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
ve	v	k7c6	v
fromborské	fromborský	k2eAgFnSc6d1	fromborský
katedrále	katedrála	k1gFnSc6	katedrála
u	u	k7c2	u
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yRgInSc4	který
se	se	k3xPyFc4	se
staral	starat	k5eAaImAgMnS	starat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
u	u	k7c2	u
kanovníků	kanovník	k1gMnPc2	kanovník
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
však	však	k9	však
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
oltář	oltář	k1gInSc1	oltář
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
<g/>
.	.	kIx.	.
</s>
<s>
Polští	polský	k2eAgMnPc1d1	polský
archeologové	archeolog	k1gMnPc1	archeolog
řadu	řad	k1gInSc2	řad
let	let	k1gInSc4	let
jeho	jeho	k3xOp3gInSc4	jeho
hrob	hrob	k1gInSc4	hrob
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
hledali	hledat	k5eAaImAgMnP	hledat
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
nalezená	nalezený	k2eAgFnSc1d1	nalezená
lebka	lebka	k1gFnSc1	lebka
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
patřila	patřit	k5eAaImAgFnS	patřit
Mikuláši	mikuláš	k1gInSc6	mikuláš
Koperníkovi	Koperník	k1gMnSc3	Koperník
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
počítačová	počítačový	k2eAgFnSc1d1	počítačová
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
tváře	tvář	k1gFnSc2	tvář
v	v	k7c6	v
Ústřední	ústřední	k2eAgFnSc6d1	ústřední
kriminalistické	kriminalistický	k2eAgFnSc6d1	kriminalistická
laboratoři	laboratoř	k1gFnSc6	laboratoř
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Stoprocentní	stoprocentní	k2eAgFnSc2d1	stoprocentní
jistoty	jistota	k1gFnSc2	jistota
však	však	k9	však
nešlo	jít	k5eNaImAgNnS	jít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chyběly	chybět	k5eAaImAgFnP	chybět
testy	testa	k1gFnPc1	testa
DNA	dno	k1gNnSc2	dno
jeho	jeho	k3xOp3gMnPc2	jeho
potomků	potomek	k1gMnPc2	potomek
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
duchovní	duchovní	k2eAgInSc1d1	duchovní
nemohl	moct	k5eNaImAgInS	moct
Koperník	Koperník	k1gInSc1	Koperník
mít	mít	k5eAaImF	mít
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autentičnost	autentičnost	k1gFnSc1	autentičnost
ostatků	ostatek	k1gInPc2	ostatek
byla	být	k5eAaImAgFnS	být
prokázána	prokázat	k5eAaPmNgFnS	prokázat
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
když	když	k8xS	když
polští	polský	k2eAgMnPc1d1	polský
vědci	vědec	k1gMnPc1	vědec
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
švédských	švédský	k2eAgMnPc2d1	švédský
kolegů	kolega	k1gMnPc2	kolega
porovnali	porovnat	k5eAaPmAgMnP	porovnat
DNA	dno	k1gNnSc2	dno
vlasu	vlas	k1gInSc2	vlas
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Koperník	Koperník	k1gMnSc1	Koperník
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
DNA	dna	k1gFnSc1	dna
jeho	on	k3xPp3gInSc2	on
zubu	zub	k1gInSc2	zub
z	z	k7c2	z
objeveného	objevený	k2eAgInSc2d1	objevený
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
tak	tak	k9	tak
mohli	moct	k5eAaImAgMnP	moct
konstatovat	konstatovat	k5eAaBmF	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
skutečně	skutečně	k6eAd1	skutečně
o	o	k7c4	o
astronomovy	astronomův	k2eAgInPc4d1	astronomův
ostatky	ostatek	k1gInPc4	ostatek
<g/>
.	.	kIx.	.
</s>
<s>
Souborné	souborný	k2eAgNnSc1d1	souborné
Koperníkovo	Koperníkův	k2eAgNnSc1d1	Koperníkovo
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
rozvržené	rozvržený	k2eAgNnSc1d1	rozvržené
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
svazků	svazek	k1gInPc2	svazek
vydává	vydávat	k5eAaPmIp3nS	vydávat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Nicolaus	Nicolaus	k1gMnSc1	Nicolaus
Copernicus	Copernicus	k1gMnSc1	Copernicus
Gesamtausgabe	Gesamtausgab	k1gInSc5	Gesamtausgab
bavorské	bavorský	k2eAgFnPc1d1	bavorská
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Akademie	akademie	k1gFnSc2	akademie
Verlag	Verlaga	k1gFnPc2	Verlaga
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
revolutionibus	revolutionibus	k1gInSc1	revolutionibus
orbium	orbium	k1gNnSc1	orbium
coelestium	coelestium	k1gNnSc1	coelestium
(	(	kIx(	(
<g/>
O	o	k7c6	o
obězích	oběh	k1gInPc6	oběh
nebeských	nebeský	k2eAgFnPc2d1	nebeská
sfér	sféra	k1gFnPc2	sféra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Faksimile	faksimile	k1gNnSc1	faksimile
rukopisu	rukopis	k1gInSc2	rukopis
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
na	na	k7c6	na
webu	web	k1gInSc6	web
Jagelonské	jagelonský	k2eAgFnSc2d1	Jagelonská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Krakově	Krakov	k1gInSc6	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Faksimile	faksimile	k1gNnSc1	faksimile
vydání	vydání	k1gNnSc2	vydání
z	z	k7c2	z
r.	r.	kA	r.
1543	[number]	k4	1543
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
stránek	stránka	k1gFnPc2	stránka
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
je	být	k5eAaImIp3nS	být
také	také	k9	také
slovenský	slovenský	k2eAgInSc1d1	slovenský
překlad	překlad	k1gInSc1	překlad
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Horského	Horský	k1gMnSc2	Horský
aj.	aj.	kA	aj.
<g/>
:	:	kIx,	:
Obehy	Obeh	k1gInPc1	Obeh
nebeských	nebeský	k2eAgFnPc2d1	nebeská
sfér	sféra	k1gFnPc2	sféra
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
VEDA	vést	k5eAaImSgInS	vést
vydavateľstvo	vydavateľstvo	k1gNnSc4	vydavateľstvo
Slovenskej	Slovenskej	k?	Slovenskej
akadémie	akadémie	k1gFnSc2	akadémie
vied	vieda	k1gFnPc2	vieda
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Koperníkova	Koperníkův	k2eAgNnPc1d1	Koperníkovo
díla	dílo	k1gNnPc1	dílo
na	na	k7c6	na
Projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gInSc1	Gutenberg
<g/>
.	.	kIx.	.
</s>
<s>
Spis	spis	k1gInSc1	spis
O	o	k7c6	o
obězích	oběh	k1gInPc6	oběh
nebeských	nebeský	k2eAgFnPc2d1	nebeská
sfér	sféra	k1gFnPc2	sféra
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
obsáhlými	obsáhlý	k2eAgFnPc7d1	obsáhlá
tabulkami	tabulka	k1gFnPc7	tabulka
a	a	k8xC	a
propočty	propočet	k1gInPc7	propočet
<g/>
.	.	kIx.	.
</s>
<s>
Koperník	Koperník	k1gInSc1	Koperník
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
geocentrickou	geocentrický	k2eAgFnSc4d1	geocentrická
koncepci	koncepce	k1gFnSc4	koncepce
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
vypracovanou	vypracovaný	k2eAgFnSc7d1	vypracovaná
ve	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
Klaudiem	Klaudium	k1gNnSc7	Klaudium
Ptolemaiem	Ptolemaios	k1gMnSc7	Ptolemaios
<g/>
.	.	kIx.	.
</s>
