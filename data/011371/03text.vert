<p>
<s>
Skaní	skaní	k1gNnSc1	skaní
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
plying	plying	k1gInSc1	plying
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Zwirnen	Zwirnen	k2eAgMnSc1d1	Zwirnen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
úprava	úprava	k1gFnSc1	úprava
příze	příze	k1gFnSc1	příze
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
a	a	k8xC	a
zakrucují	zakrucovat	k5eAaImIp3nP	zakrucovat
dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
nití	nit	k1gFnPc2	nit
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skát	skát	k5eAaImF	skát
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
staplové	staplový	k2eAgFnPc1d1	staplová
příze	příz	k1gFnPc1	příz
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
jemnosti	jemnost	k1gFnSc2	jemnost
a	a	k8xC	a
barvy	barva	k1gFnSc2	barva
jak	jak	k8xS	jak
spolu	spolu	k6eAd1	spolu
navzájem	navzájem	k6eAd1	navzájem
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
s	s	k7c7	s
filamenty	filament	k1gInPc7	filament
(	(	kIx(	(
<g/>
přízí	příz	k1gFnSc7	příz
z	z	k7c2	z
nekonečného	konečný	k2eNgNnSc2d1	nekonečné
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
z	z	k7c2	z
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
výroby	výroba	k1gFnSc2	výroba
staplových	staplův	k2eAgFnPc2d1	staplův
a	a	k8xC	a
filamentových	filamentův	k2eAgFnPc2d1	filamentův
přízí	příz	k1gFnPc2	příz
vice	vika	k1gFnSc3	vika
než	než	k8xS	než
pětina	pětina	k1gFnSc1	pětina
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
skaním	skaní	k1gNnSc7	skaní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
řádově	řádově	k6eAd1	řádově
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zákrut	zákrut	k1gInSc4	zákrut
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
charakter	charakter	k1gInSc4	charakter
skané	skaný	k2eAgFnSc2d1	skaná
příze	příz	k1gFnSc2	příz
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
směr	směr	k1gInSc1	směr
a	a	k8xC	a
počet	počet	k1gInSc1	počet
zákrutů	zákrut	k1gInPc2	zákrut
jak	jak	k8xC	jak
u	u	k7c2	u
předkládaných	předkládaný	k2eAgFnPc2d1	Předkládaná
tak	tak	k9	tak
i	i	k9	i
u	u	k7c2	u
výsledných	výsledný	k2eAgFnPc2d1	výsledná
nití	nit	k1gFnPc2	nit
<g/>
.	.	kIx.	.
</s>
<s>
Směr	směr	k1gInSc1	směr
zákrutu	zákrut	k1gInSc2	zákrut
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
svisle	svisle	k6eAd1	svisle
drženou	držený	k2eAgFnSc4d1	držená
nit	nit	k1gFnSc4	nit
(	(	kIx(	(
<g/>
náčrt	náčrt	k1gInSc1	náčrt
vlevo	vlevo	k6eAd1	vlevo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
spirála	spirála	k1gFnSc1	spirála
zákrutu	zákruta	k1gFnSc4	zákruta
probíhá	probíhat	k5eAaImIp3nS	probíhat
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
jako	jako	k8xC	jako
prostřední	prostřednět	k5eAaImIp3nP	prostřednět
část	část	k1gFnSc4	část
písmene	písmeno	k1gNnSc2	písmeno
"	"	kIx"	"
<g/>
Z	z	k7c2	z
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přízi	příze	k1gFnSc4	příze
točenou	točený	k2eAgFnSc4d1	točená
k	k	k7c3	k
sobě	se	k3xPyFc3	se
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgInSc1d1	vytvářející
pravý	pravý	k2eAgInSc1d1	pravý
závit	závit	k1gInSc1	závit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pravého	pravý	k2eAgInSc2d1	pravý
zákrutu	zákrut	k1gInSc2	zákrut
k	k	k7c3	k
sobě	se	k3xPyFc3	se
(	(	kIx(	(
<g/>
levého	levý	k2eAgInSc2d1	levý
závitu	závit	k1gInSc2	závit
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
spirála	spirála	k1gFnSc1	spirála
podobá	podobat	k5eAaImIp3nS	podobat
písmenu	písmeno	k1gNnSc3	písmeno
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
skací	skací	k2eAgInSc1d1	skací
zákrut	zákrut	k1gInSc1	zákrut
probíhá	probíhat	k5eAaImIp3nS	probíhat
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
jako	jako	k9	jako
u	u	k7c2	u
obou	dva	k4xCgFnPc2	dva
předkládaných	předkládaný	k2eAgFnPc2d1	Předkládaná
nití	nit	k1gFnPc2	nit
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
velmi	velmi	k6eAd1	velmi
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
příze	příze	k1gFnSc1	příze
(	(	kIx(	(
<g/>
vhodná	vhodný	k2eAgFnSc1d1	vhodná
například	například	k6eAd1	například
na	na	k7c4	na
pneumatikové	pneumatik	k1gMnPc1	pneumatik
kordy	kord	k1gInPc4	kord
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
příze	příze	k1gFnSc1	příze
s	s	k7c7	s
opačným	opačný	k2eAgNnSc7d1	opačné
točením	točení	k1gNnSc7	točení
při	při	k7c6	při
skaní	skaní	k1gNnSc6	skaní
je	být	k5eAaImIp3nS	být
měkčí	měkký	k2eAgInSc1d2	měkčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
stoupajícím	stoupající	k2eAgInSc7d1	stoupající
počtem	počet	k1gInSc7	počet
zákrutů	zákrut	k1gInPc2	zákrut
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
hranice	hranice	k1gFnSc2	hranice
<g/>
)	)	kIx)	)
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
pevnost	pevnost	k1gFnSc1	pevnost
příze	příze	k1gFnSc1	příze
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
U	u	k7c2	u
krepových	krepový	k2eAgFnPc2d1	krepová
přízí	příz	k1gFnPc2	příz
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
mez	mez	k1gFnSc1	mez
překračuje	překračovat	k5eAaImIp3nS	překračovat
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
dostanou	dostat	k5eAaPmIp3nP	dostat
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
ve	v	k7c6	v
tkanině	tkanina	k1gFnSc6	tkanina
zrnitý	zrnitý	k2eAgInSc4d1	zrnitý
povrch	povrch	k1gInSc4	povrch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hladké	Hladké	k2eAgFnPc1d1	Hladké
skané	skaný	k2eAgFnPc1d1	skaná
příze	příz	k1gFnPc1	příz
==	==	k?	==
</s>
</p>
<p>
<s>
Účelem	účel	k1gInSc7	účel
skaní	skaní	k1gNnSc2	skaní
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zvýšit	zvýšit	k5eAaPmF	zvýšit
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
stejnoměrnost	stejnoměrnost	k1gFnSc4	stejnoměrnost
výsledné	výsledný	k2eAgFnSc2d1	výsledná
příze	příz	k1gFnSc2	příz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jsou	být	k5eAaImIp3nP	být
potřebné	potřebný	k2eAgFnPc1d1	potřebná
zejména	zejména	k9	zejména
u	u	k7c2	u
tkalcovských	tkalcovský	k2eAgFnPc2d1	tkalcovská
osnov	osnova	k1gFnPc2	osnova
<g/>
,	,	kIx,	,
šicích	šicí	k2eAgFnPc2d1	šicí
nití	nit	k1gFnPc2	nit
a	a	k8xC	a
pneumatikových	pneumatikův	k2eAgInPc2d1	pneumatikův
kordů	kord	k1gInPc2	kord
(	(	kIx(	(
<g/>
skaných	skaný	k2eAgMnPc2d1	skaný
z	z	k7c2	z
textilních	textilní	k2eAgInPc2d1	textilní
materiálů	materiál	k1gInPc2	materiál
i	i	k9	i
z	z	k7c2	z
ocelových	ocelový	k2eAgInPc2d1	ocelový
drátů	drát	k1gInPc2	drát
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Příprava	příprava	k1gFnSc1	příprava
===	===	k?	===
</s>
</p>
<p>
<s>
Staplové	Staplový	k2eAgFnPc1d1	Staplový
příze	příz	k1gFnPc1	příz
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc4	sklon
ke	k	k7c3	k
smyčkování	smyčkování	k1gNnSc3	smyčkování
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
zamezilo	zamezit	k5eAaPmAgNnS	zamezit
<g/>
,	,	kIx,	,
vkládají	vkládat	k5eAaImIp3nP	vkládat
se	se	k3xPyFc4	se
potáče	potáč	k1gInSc2	potáč
nebo	nebo	k8xC	nebo
nasoukané	nasoukaný	k2eAgFnSc2d1	nasoukaná
cívky	cívka	k1gFnSc2	cívka
na	na	k7c4	na
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
do	do	k7c2	do
pařáku	pařák	k1gInSc2	pařák
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
°	°	k?	°
<g/>
C.	C.	kA	C.
<g/>
Příze	příze	k1gFnSc1	příze
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
kropí	kropit	k5eAaImIp3nS	kropit
emulzí	emulze	k1gFnSc7	emulze
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
mastné	mastný	k2eAgFnSc2d1	mastná
kyseliny	kyselina	k1gFnSc2	kyselina
(	(	kIx(	(
<g/>
cca	cca	kA	cca
0,5	[number]	k4	0,5
%	%	kIx~	%
váhy	váha	k1gFnSc2	váha
cívek	cívka	k1gFnPc2	cívka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
sníží	snížit	k5eAaPmIp3nS	snížit
tření	tření	k1gNnSc1	tření
a	a	k8xC	a
oděr	oděr	k1gInSc1	oděr
příze	příz	k1gFnSc2	příz
při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
zpracování	zpracování	k1gNnSc6	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
přízí	příz	k1gFnPc2	příz
prochází	procházet	k5eAaImIp3nS	procházet
před	před	k7c7	před
skaním	skaní	k1gNnSc7	skaní
sdružovacím	sdružovací	k2eAgInSc7d1	sdružovací
strojem	stroj	k1gInSc7	stroj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
navíjí	navíjet	k5eAaImIp3nS	navíjet
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
nití	nit	k1gFnPc2	nit
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
cívku	cívka	k1gFnSc4	cívka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skací	skací	k2eAgInPc1d1	skací
stroje	stroj	k1gInPc1	stroj
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
velkovýrobu	velkovýroba	k1gFnSc4	velkovýroba
skaných	skaný	k2eAgFnPc2d1	skaná
přízí	příz	k1gFnPc2	příz
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používají	používat	k5eAaImIp3nP	používat
dvouzákrutové	dvouzákrutový	k2eAgInPc4d1	dvouzákrutový
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Funkce	funkce	k1gFnSc1	funkce
<g/>
:	:	kIx,	:
Předloha	předloha	k1gFnSc1	předloha
se	se	k3xPyFc4	se
navléká	navlékat	k5eAaImIp3nS	navlékat
na	na	k7c4	na
duté	dutý	k2eAgNnSc4d1	duté
vřeteno	vřeteno	k1gNnSc4	vřeteno
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
v	v	k7c6	v
pevně	pevně	k6eAd1	pevně
stojícím	stojící	k2eAgInSc6d1	stojící
hrnci	hrnec	k1gInSc6	hrnec
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příze	příze	k1gFnSc1	příze
prochází	procházet	k5eAaImIp3nS	procházet
shora	shora	k6eAd1	shora
vřetenem	vřeteno	k1gNnSc7	vřeteno
a	a	k8xC	a
kolem	kolo	k1gNnSc7	kolo
vnější	vnější	k2eAgFnSc2d1	vnější
stěny	stěna	k1gFnSc2	stěna
hrnce	hrnec	k1gInSc2	hrnec
k	k	k7c3	k
vodiči	vodič	k1gInSc3	vodič
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
nad	nad	k7c7	nad
středem	střed	k1gInSc7	střed
vřetene	vřeten	k1gInSc5	vřeten
k	k	k7c3	k
navíjecímu	navíjecí	k2eAgNnSc3d1	navíjecí
ústrojí	ústrojí	k1gNnSc3	ústrojí
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
otáčce	otáčka	k1gFnSc6	otáčka
vřetene	vřeten	k1gInSc5	vřeten
se	se	k3xPyFc4	se
vkládá	vkládat	k5eAaImIp3nS	vkládat
do	do	k7c2	do
příze	příz	k1gFnSc2	příz
první	první	k4xOgInSc4	první
zákrut	zákrut	k1gInSc4	zákrut
mezi	mezi	k7c7	mezi
předlohou	předloha	k1gFnSc7	předloha
a	a	k8xC	a
špičkou	špička	k1gFnSc7	špička
vřetene	vřeten	k1gInSc5	vřeten
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
mezi	mezi	k7c7	mezi
dolním	dolní	k2eAgInSc7d1	dolní
otvorem	otvor	k1gInSc7	otvor
ve	v	k7c6	v
vřetenu	vřeteno	k1gNnSc6	vřeteno
a	a	k8xC	a
vodičem	vodič	k1gInSc7	vodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otáčky	otáčka	k1gFnPc1	otáčka
vřeten	vřeten	k1gInSc1	vřeten
mohou	moct	k5eAaImIp3nP	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
12	[number]	k4	12
000	[number]	k4	000
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Praktický	praktický	k2eAgInSc1d1	praktický
příklad	příklad	k1gInSc1	příklad
výkonu	výkon	k1gInSc2	výkon
jednoho	jeden	k4xCgMnSc4	jeden
vřetene	vřeten	k1gInSc5	vřeten
<g/>
:	:	kIx,	:
Dvojmo	dvojmo	k6eAd1	dvojmo
skaná	skaný	k2eAgFnSc1d1	skaná
směsová	směsový	k2eAgFnSc1d1	směsová
příze	příze	k1gFnSc1	příze
17	[number]	k4	17
tex	tex	k?	tex
s	s	k7c7	s
830	[number]	k4	830
(	(	kIx(	(
<g/>
přádními	přádní	k2eAgInPc7d1	přádní
<g/>
)	)	kIx)	)
Z-zákruty	Zákrut	k1gInPc7	Z-zákrut
na	na	k7c4	na
metr	metr	k1gInSc4	metr
při	při	k7c6	při
11	[number]	k4	11
000	[number]	k4	000
otáčkách	otáčka	k1gFnPc6	otáčka
vřetene	vřeten	k1gInSc5	vřeten
a	a	k8xC	a
650	[number]	k4	650
skacích	skací	k2eAgInPc6d1	skací
zákrutech	zákrut	k1gInPc6	zákrut
(	(	kIx(	(
<g/>
S	s	k7c7	s
<g/>
)	)	kIx)	)
=	=	kIx~	=
62	[number]	k4	62
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
skaní	skaní	k1gNnSc4	skaní
malých	malý	k2eAgFnPc2d1	malá
partií	partie	k1gFnPc2	partie
a	a	k8xC	a
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
účely	účel	k1gInPc4	účel
jsou	být	k5eAaImIp3nP	být
vhodnější	vhodný	k2eAgInPc4d2	vhodnější
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
skacích	skací	k2eAgInPc2d1	skací
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
prstencové	prstencový	k2eAgFnPc1d1	prstencová
<g/>
,	,	kIx,	,
dvoustupňové	dvoustupňový	k2eAgFnPc1d1	dvoustupňová
<g/>
,	,	kIx,	,
křídlové	křídlový	k2eAgFnPc1d1	křídlová
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
snímek	snímek	k1gInSc4	snímek
vpravo	vpravo	k6eAd1	vpravo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
kablovací	kablovací	k2eAgMnSc1d1	kablovací
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
textilních	textilní	k2eAgInPc2d1	textilní
strojů	stroj	k1gInPc2	stroj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
předváděn	předváděn	k2eAgInSc1d1	předváděn
třízákrutový	třízákrutový	k2eAgInSc1d1	třízákrutový
stroj	stroj	k1gInSc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
skaní	skaní	k1gNnSc6	skaní
zde	zde	k6eAd1	zde
příze	příze	k1gFnSc1	příze
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
dva	dva	k4xCgInPc4	dva
balóny	balón	k1gInPc4	balón
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jeden	jeden	k4xCgMnSc1	jeden
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
v	v	k7c6	v
protisměru	protisměr	k1gInSc6	protisměr
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
dvojnásobnou	dvojnásobný	k2eAgFnSc7d1	dvojnásobná
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vkládají	vkládat	k5eAaImIp3nP	vkládat
na	na	k7c4	na
každou	každý	k3xTgFnSc4	každý
otáčku	otáčka	k1gFnSc4	otáčka
vřetene	vřeten	k1gInSc5	vřeten
do	do	k7c2	do
příze	příz	k1gFnSc2	příz
tři	tři	k4xCgFnPc4	tři
zákruty	zákruta	k1gFnPc4	zákruta
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zdobné	zdobný	k2eAgFnSc2d1	zdobná
(	(	kIx(	(
<g/>
efektní	efektní	k2eAgFnSc2d1	efektní
<g/>
)	)	kIx)	)
příze	příz	k1gFnSc2	příz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Druhy	druh	k1gInPc4	druh
přízí	příz	k1gFnPc2	příz
===	===	k?	===
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
příze	příz	k1gFnPc1	příz
vznikají	vznikat	k5eAaImIp3nP	vznikat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
<s>
kolem	kolem	k7c2	kolem
základní	základní	k2eAgFnSc2d1	základní
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
nebo	nebo	k8xC	nebo
skané	skaný	k2eAgFnSc2d1	skaná
niti	nit	k1gFnSc2	nit
se	se	k3xPyFc4	se
obtáčí	obtáčet	k5eAaImIp3nP	obtáčet
příze	příz	k1gFnPc1	příz
vytvářející	vytvářející	k2eAgFnPc1d1	vytvářející
efekty	efekt	k1gInPc4	efekt
různých	různý	k2eAgInPc2d1	různý
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
buklé	buklé	k1gInSc1	buklé
<g/>
,	,	kIx,	,
loop	loop	k1gInSc1	loop
<g/>
,	,	kIx,	,
froté	froté	k1gNnSc1	froté
a	a	k8xC	a
pod	pod	k7c4	pod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
ozdobné	ozdobný	k2eAgFnPc1d1	ozdobná
niti	nit	k1gFnPc1	nit
se	se	k3xPyFc4	se
podávají	podávat	k5eAaImIp3nP	podávat
s	s	k7c7	s
proměnlivou	proměnlivý	k2eAgFnSc7d1	proměnlivá
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
kolem	kolem	k7c2	kolem
základní	základní	k2eAgFnSc2d1	základní
niti	nit	k1gFnSc2	nit
smyčky	smyčka	k1gFnSc2	smyčka
<g/>
,	,	kIx,	,
nopky	nopka	k1gFnSc2	nopka
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
<s>
mezi	mezi	k7c4	mezi
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
nitě	nit	k1gFnPc4	nit
se	se	k3xPyFc4	se
zaskávají	zaskávat	k5eAaPmIp3nP	zaskávat
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
zakrucování	zakrucování	k1gNnSc2	zakrucování
chomáčky	chomáček	k1gInPc1	chomáček
vláken	vlákno	k1gNnPc2	vlákno
nebo	nebo	k8xC	nebo
kousky	kousek	k1gInPc1	kousek
příze	příz	k1gFnSc2	příz
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
průběžně	průběžně	k6eAd1	průběžně
nebo	nebo	k8xC	nebo
v	v	k7c6	v
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
<g/>
Zdobné	zdobný	k2eAgFnPc4d1	zdobná
příze	příz	k1gFnPc4	příz
se	se	k3xPyFc4	se
požívají	požívat	k5eAaImIp3nP	požívat
k	k	k7c3	k
ručnímu	ruční	k2eAgNnSc3d1	ruční
a	a	k8xC	a
strojnímu	strojní	k2eAgNnSc3d1	strojní
pletení	pletení	k1gNnSc3	pletení
<g/>
,	,	kIx,	,
na	na	k7c4	na
dekorační	dekorační	k2eAgFnPc4d1	dekorační
tkaniny	tkanina	k1gFnPc4	tkanina
a	a	k8xC	a
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
efektů	efekt	k1gInPc2	efekt
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
nitěmi	nit	k1gFnPc7	nit
ve	v	k7c6	v
tkaninách	tkanina	k1gFnPc6	tkanina
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
v	v	k7c6	v
tapetách	tapeta	k1gFnPc6	tapeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
vpravo	vpravo	k6eAd1	vpravo
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
příkladů	příklad	k1gInPc2	příklad
zdobných	zdobný	k2eAgFnPc2d1	zdobná
přízí	příz	k1gFnPc2	příz
na	na	k7c4	na
ruční	ruční	k2eAgNnSc4d1	ruční
pletení	pletení	k1gNnSc4	pletení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tématem	téma	k1gNnSc7	téma
se	se	k3xPyFc4	se
podrobněji	podrobně	k6eAd2	podrobně
zabývá	zabývat	k5eAaImIp3nS	zabývat
článek	článek	k1gInSc1	článek
Efektní	efektní	k2eAgFnSc2d1	efektní
příze	příz	k1gFnSc2	příz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Žinylka	Žinylko	k1gNnSc2	Žinylko
====	====	k?	====
</s>
</p>
<p>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
žinylka	žinylka	k1gFnSc1	žinylka
(	(	kIx(	(
<g/>
chenille	chenille	k1gInSc1	chenille
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
příze	příze	k1gFnSc1	příze
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
housenka	housenka	k1gFnSc1	housenka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
jádro	jádro	k1gNnSc1	jádro
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
skaných	skaný	k2eAgFnPc2d1	skaná
nití	nit	k1gFnPc2	nit
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
a	a	k8xC	a
obvodu	obvod	k1gInSc6	obvod
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
pravidelně	pravidelně	k6eAd1	pravidelně
odstávajícími	odstávající	k2eAgInPc7d1	odstávající
vlasy	vlas	k1gInPc7	vlas
(	(	kIx(	(
<g/>
flórem	flór	k1gInSc7	flór
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlasová	vlasový	k2eAgFnSc1d1	vlasová
nit	nit	k1gFnSc1	nit
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
během	během	k7c2	během
zakrucování	zakrucování	k1gNnSc2	zakrucování
mezi	mezi	k7c4	mezi
obě	dva	k4xCgFnPc4	dva
niti	nit	k1gFnPc4	nit
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
průběžně	průběžně	k6eAd1	průběžně
uřezává	uřezávat	k5eAaImIp3nS	uřezávat
na	na	k7c4	na
přesnou	přesný	k2eAgFnSc4d1	přesná
délku	délka	k1gFnSc4	délka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
4	[number]	k4	4
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žinylka	Žinylka	k1gFnSc1	Žinylka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
navíjí	navíjet	k5eAaImIp3nS	navíjet
na	na	k7c4	na
potáč	potáč	k1gInSc4	potáč
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
na	na	k7c6	na
konvenčních	konvenční	k2eAgInPc6d1	konvenční
prstencových	prstencový	k2eAgInPc6d1	prstencový
strojích	stroj	k1gInPc6	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výroba	výroba	k1gFnSc1	výroba
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
efektních	efektní	k2eAgFnPc2d1	efektní
přízí	příz	k1gFnPc2	příz
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
na	na	k7c6	na
prstencových	prstencový	k2eAgInPc6d1	prstencový
skacích	skací	k2eAgInPc6d1	skací
strojích	stroj	k1gInPc6	stroj
a	a	k8xC	a
na	na	k7c6	na
strojích	stroj	k1gInPc6	stroj
s	s	k7c7	s
dutým	dutý	k2eAgInSc7d1	dutý
vřetenem	vřeten	k1gInSc7	vřeten
<g/>
.	.	kIx.	.
</s>
<s>
Podávací	podávací	k2eAgNnSc1d1	podávací
ústrojí	ústrojí	k1gNnSc1	ústrojí
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
upraveno	upravit	k5eAaPmNgNnS	upravit
na	na	k7c4	na
současné	současný	k2eAgNnSc4d1	současné
přivádění	přivádění	k1gNnSc4	přivádění
několika	několik	k4yIc2	několik
nití	nit	k1gFnPc2	nit
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
<g/>
,	,	kIx,	,
průběžně	průběžně	k6eAd1	průběžně
měnitelnými	měnitelný	k2eAgFnPc7d1	měnitelná
rychlostmi	rychlost	k1gFnPc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
strojích	stroj	k1gInPc6	stroj
je	být	k5eAaImIp3nS	být
instalováno	instalován	k2eAgNnSc4d1	instalováno
i	i	k8xC	i
průtahové	průtahový	k2eAgNnSc4d1	průtahové
ústrojí	ústrojí	k1gNnSc4	ústrojí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přivádí	přivádět	k5eAaImIp3nS	přivádět
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
zákrutu	zákrut	k1gInSc2	zákrut
chomáčky	chomáček	k1gInPc4	chomáček
vláken	vlákno	k1gNnPc2	vlákno
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skaní	skaní	k1gNnSc1	skaní
často	často	k6eAd1	často
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
pasážích	pasáž	k1gFnPc6	pasáž
<g/>
,	,	kIx,	,
hotová	hotový	k2eAgFnSc1d1	hotová
příze	příze	k1gFnSc1	příze
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
navíjí	navíjet	k5eAaImIp3nS	navíjet
na	na	k7c4	na
konickou	konický	k2eAgFnSc4d1	konická
cívku	cívka	k1gFnSc4	cívka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
moderních	moderní	k2eAgInPc6d1	moderní
strojích	stroj	k1gInPc6	stroj
s	s	k7c7	s
dutým	dutý	k2eAgNnSc7d1	duté
vřetenem	vřeteno	k1gNnSc7	vřeteno
probíhá	probíhat	k5eAaImIp3nS	probíhat
skaní	skaní	k1gNnPc4	skaní
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
příze	příze	k1gFnSc1	příze
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
průtahového	průtahový	k2eAgNnSc2d1	průtahové
ústrojí	ústrojí	k1gNnSc2	ústrojí
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
z	z	k7c2	z
cívečnice	cívečnice	k1gFnSc2	cívečnice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
průchodu	průchod	k1gInSc6	průchod
dutinou	dutina	k1gFnSc7	dutina
vřetene	vřeten	k1gInSc5	vřeten
zakrucována	zakrucovat	k5eAaImNgFnS	zakrucovat
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
elementem	element	k1gInSc7	element
a	a	k8xC	a
zákrut	zákrut	k1gInSc1	zákrut
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
fixován	fixovat	k5eAaImNgInS	fixovat
přízí	příz	k1gFnSc7	příz
z	z	k7c2	z
potáče	potáč	k1gInSc2	potáč
nasazeného	nasazený	k2eAgInSc2d1	nasazený
na	na	k7c6	na
vřetenu	vřeten	k1gInSc6	vřeten
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
otáček	otáčka	k1gFnPc2	otáčka
zkrucovacího	zkrucovací	k2eAgNnSc2d1	zkrucovací
ústrojí	ústrojí	k1gNnSc2	ústrojí
a	a	k8xC	a
vřetene	vřeten	k1gInSc5	vřeten
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
značně	značně	k6eAd1	značně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
omak	omak	k1gInSc4	omak
výsledné	výsledný	k2eAgFnSc2d1	výsledná
příze	příz	k1gFnSc2	příz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
kombinace	kombinace	k1gFnPc1	kombinace
s	s	k7c7	s
dutým	dutý	k2eAgInSc7d1	dutý
vřetenem	vřeten	k1gInSc7	vřeten
a	a	k8xC	a
následujícím	následující	k2eAgNnSc7d1	následující
prstencovým	prstencový	k2eAgNnSc7d1	prstencové
skaním	skaní	k1gNnSc7	skaní
<g/>
.	.	kIx.	.
<g/>
Výrobci	výrobce	k1gMnPc1	výrobce
speciálních	speciální	k2eAgInPc2d1	speciální
strojů	stroj	k1gInPc2	stroj
na	na	k7c4	na
zdobné	zdobný	k2eAgFnPc4d1	zdobná
příze	příz	k1gFnPc4	příz
nabízí	nabízet	k5eAaImIp3nS	nabízet
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vzorování	vzorování	k1gNnSc4	vzorování
efektů	efekt	k1gInPc2	efekt
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2000	[number]	k4	2000
variantách	varianta	k1gFnPc6	varianta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kablování	Kablování	k1gNnSc2	Kablování
==	==	k?	==
</s>
</p>
<p>
<s>
Technologie	technologie	k1gFnSc1	technologie
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
dvouzákrutovému	dvouzákrutový	k2eAgNnSc3d1	dvouzákrutový
skaní	skaní	k1gNnSc3	skaní
<g/>
,	,	kIx,	,
příze	příze	k1gFnSc1	příze
zde	zde	k6eAd1	zde
však	však	k9	však
dostává	dostávat	k5eAaImIp3nS	dostávat
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
otáčce	otáčka	k1gFnSc6	otáčka
vřetene	vřeten	k1gInSc5	vřeten
jen	jen	k9	jen
jeden	jeden	k4xCgInSc4	jeden
zákrut	zákrut	k1gInSc4	zákrut
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
nití	nit	k1gFnPc2	nit
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
skaní	skaní	k1gNnSc6	skaní
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
předkládá	předkládat	k5eAaImIp3nS	předkládat
na	na	k7c6	na
cívce	cívka	k1gFnSc6	cívka
nasazené	nasazený	k2eAgFnSc6d1	nasazená
na	na	k7c6	na
vřetenu	vřeten	k1gInSc6	vřeten
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nit	nit	k1gFnSc1	nit
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
z	z	k7c2	z
cívečnice	cívečnice	k1gFnSc2	cívečnice
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
vřetenem	vřeten	k1gInSc7	vřeten
a	a	k8xC	a
obtáčí	obtáčet	k5eAaImIp3nS	obtáčet
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
první	první	k4xOgFnSc2	první
niti	nit	k1gFnSc2	nit
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
vkládá	vkládat	k5eAaImIp3nS	vkládat
do	do	k7c2	do
příze	příz	k1gFnSc2	příz
jen	jen	k9	jen
skací	skací	k2eAgInSc4d1	skací
zákrut	zákrut	k1gInSc4	zákrut
<g/>
,	,	kIx,	,
na	na	k7c4	na
zákrutu	zákruta	k1gFnSc4	zákruta
předkládaných	předkládaný	k2eAgFnPc2d1	Předkládaná
nití	nit	k1gFnPc2	nit
se	se	k3xPyFc4	se
nic	nic	k6eAd1	nic
nemění	měnit	k5eNaImIp3nP	měnit
a	a	k8xC	a
udržují	udržovat	k5eAaImIp3nP	udržovat
si	se	k3xPyFc3	se
původní	původní	k2eAgInSc1d1	původní
objem	objem	k1gInSc1	objem
<g/>
.	.	kIx.	.
<g/>
Technologie	technologie	k1gFnSc1	technologie
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
obzvlášť	obzvlášť	k6eAd1	obzvlášť
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
skaní	skaní	k1gNnSc4	skaní
nezakroucených	zakroucený	k2eNgInPc2d1	zakroucený
tvarovaných	tvarovaný	k2eAgInPc2d1	tvarovaný
filamentů	filament	k1gInPc2	filament
(	(	kIx(	(
<g/>
BCF	BCF	kA	BCF
<g/>
)	)	kIx)	)
na	na	k7c4	na
kobercové	kobercový	k2eAgFnPc4d1	kobercová
příze	příz	k1gFnPc4	příz
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
lepšího	dobrý	k2eAgNnSc2d2	lepší
plošného	plošný	k2eAgNnSc2d1	plošné
krytí	krytí	k1gNnSc2	krytí
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
úspory	úspora	k1gFnSc2	úspora
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Kablovací	Kablovací	k2eAgInPc1d1	Kablovací
stroje	stroj	k1gInPc1	stroj
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
po	po	k7c6	po
snadné	snadný	k2eAgFnSc6d1	snadná
přestavbě	přestavba	k1gFnSc6	přestavba
pracovních	pracovní	k2eAgInPc2d1	pracovní
orgánů	orgán	k1gInPc2	orgán
použít	použít	k5eAaPmF	použít
i	i	k9	i
na	na	k7c4	na
dvouzákrutové	dvouzákrutový	k2eAgNnSc4d1	dvouzákrutový
skaní	skaní	k1gNnSc4	skaní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
:	:	kIx,	:
Tkalcovská	tkalcovský	k2eAgFnSc1d1	tkalcovská
příručka	příručka	k1gFnSc1	příručka
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
263	[number]	k4	263
<g/>
–	–	k?	–
<g/>
271	[number]	k4	271
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
Praha	Praha	k1gFnSc1	Praha
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Schenek	Schenka	k1gFnPc2	Schenka
<g/>
:	:	kIx,	:
Lexikon	lexikon	k1gInSc1	lexikon
Garne	Garn	k1gInSc5	Garn
und	und	k?	und
Zwirne	Zwirn	k1gInSc5	Zwirn
<g/>
,	,	kIx,	,
Deutscher	Deutschra	k1gFnPc2	Deutschra
Fachverlag	Fachverlaga	k1gFnPc2	Fachverlaga
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3871508101	[number]	k4	3871508101
</s>
</p>
