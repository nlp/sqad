<s>
Jestliže	jestliže	k8xS	jestliže
spirála	spirála	k1gFnSc1	spirála
zákrutu	zákruta	k1gFnSc4	zákruta
probíhá	probíhat	k5eAaImIp3nS	probíhat
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
jako	jako	k8xC	jako
prostřední	prostřednět	k5eAaImIp3nP	prostřednět
část	část	k1gFnSc4	část
písmene	písmeno	k1gNnSc2	písmeno
"	"	kIx"	"
<g/>
Z	z	k7c2	z
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přízi	příze	k1gFnSc4	příze
točenou	točený	k2eAgFnSc4d1	točená
k	k	k7c3	k
sobě	se	k3xPyFc3	se
doleva	doleva	k6eAd1	doleva
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgInSc1d1	vytvářející
pravý	pravý	k2eAgInSc1d1	pravý
závit	závit	k1gInSc1	závit
<g/>
.	.	kIx.	.
</s>
