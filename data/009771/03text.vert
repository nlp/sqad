<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Kamil	Kamil	k1gMnSc1	Kamil
Albrecht	Albrecht	k1gMnSc1	Albrecht
Zeman	Zeman	k1gMnSc1	Zeman
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1882	[number]	k4	1882
Semily	Semily	k1gInPc7	Semily
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1952	[number]	k4	1952
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
spisovatel-prozaik	spisovatelrozaik	k1gMnSc1	spisovatel-prozaik
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
německé	německý	k2eAgFnSc2d1	německá
prózy	próza	k1gFnSc2	próza
<g/>
,	,	kIx,	,
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
advokát	advokát	k1gMnSc1	advokát
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Antal	Antal	k1gMnSc1	Antal
Stašek	Stašek	k1gMnSc1	Stašek
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Antonín	Antonín	k1gMnSc1	Antonín
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
velkým	velký	k2eAgNnSc7d1	velké
sociálním	sociální	k2eAgNnSc7d1	sociální
cítěním	cítění	k1gNnSc7	cítění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
přenášel	přenášet	k5eAaImAgMnS	přenášet
i	i	k9	i
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
</s>
</p>
<p>
<s>
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
za	za	k7c4	za
svobodna	svoboden	k2eAgFnSc1d1	svobodna
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
Kamila	Kamila	k1gFnSc1	Kamila
Schönfeldová	Schönfeldová	k1gFnSc1	Schönfeldová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
se	se	k3xPyFc4	se
zřekla	zřeknout	k5eAaPmAgFnS	zřeknout
židovského	židovský	k2eAgNnSc2d1	Židovské
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
přestoupila	přestoupit	k5eAaPmAgFnS	přestoupit
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
víře	víra	k1gFnSc3	víra
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Kamil	Kamil	k1gMnSc1	Kamil
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
ve	v	k7c6	v
Dvoře	Dvůr	k1gInSc6	Dvůr
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
maturoval	maturovat	k5eAaBmAgInS	maturovat
v	v	k7c6	v
r.	r.	kA	r.
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
jako	jako	k8xS	jako
gymnazista	gymnazista	k1gMnSc1	gymnazista
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
své	svůj	k3xOyFgInPc4	svůj
drobné	drobný	k2eAgInPc4d1	drobný
literární	literární	k2eAgInPc4d1	literární
pokusy	pokus	k1gInPc4	pokus
pseudonymem	pseudonym	k1gInSc7	pseudonym
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jeho	jeho	k3xOp3gNnSc2	jeho
vlastního	vlastní	k2eAgNnSc2d1	vlastní
svědectví	svědectví	k1gNnSc2	svědectví
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tento	tento	k3xDgInSc1	tento
pseudonym	pseudonym	k1gInSc1	pseudonym
spojením	spojení	k1gNnSc7	spojení
Kamilova	Kamilův	k2eAgNnSc2d1	Kamilovo
biřmovacího	biřmovací	k2eAgNnSc2d1	biřmovací
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
"	"	kIx"	"
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
druhým	druhý	k4xOgNnSc7	druhý
křestním	křestní	k2eAgNnSc7d1	křestní
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Albrecht	Albrecht	k1gMnSc1	Albrecht
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
obměnil	obměnit	k5eAaPmAgMnS	obměnit
(	(	kIx(	(
<g/>
popolštil	popolštit	k5eAaPmAgMnS	popolštit
<g/>
)	)	kIx)	)
na	na	k7c4	na
"	"	kIx"	"
<g/>
Olbracht	Olbracht	k1gInSc4	Olbracht
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nejprve	nejprve	k6eAd1	nejprve
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
ročníku	ročník	k1gInSc6	ročník
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
obor	obor	k1gInSc4	obor
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
zeměpis	zeměpis	k1gInSc1	zeměpis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
byl	být	k5eAaImAgInS	být
povolán	povolat	k5eAaPmNgInS	povolat
k	k	k7c3	k
jednoroční	jednoroční	k2eAgFnSc3d1	jednoroční
vojenské	vojenský	k2eAgFnSc3d1	vojenská
službě	služba	k1gFnSc3	služba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
vykonával	vykonávat	k5eAaImAgInS	vykonávat
u	u	k7c2	u
94	[number]	k4	94
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
a	a	k8xC	a
v	v	k7c6	v
Terezíně	Terezín	k1gInSc6	Terezín
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
hodnosti	hodnost	k1gFnPc4	hodnost
kadeta-aspiranta	kadetaspirant	k1gMnSc2	kadeta-aspirant
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
v	v	k7c6	v
r.	r.	kA	r.
1905	[number]	k4	1905
byl	být	k5eAaImAgInS	být
degradován	degradovat	k5eAaBmNgInS	degradovat
kvůli	kvůli	k7c3	kvůli
aktivní	aktivní	k2eAgFnSc3d1	aktivní
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
sociálnědemokratických	sociálnědemokratický	k2eAgFnPc6d1	sociálnědemokratická
politických	politický	k2eAgFnPc6d1	politická
akcích	akce	k1gFnPc6	akce
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
opět	opět	k6eAd1	opět
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
<g/>
,	,	kIx,	,
státní	státní	k2eAgFnPc4d1	státní
zkoušky	zkouška	k1gFnPc4	zkouška
v	v	k7c6	v
úplnosti	úplnost	k1gFnSc6	úplnost
neabsolvoval	absolvovat	k5eNaPmAgMnS	absolvovat
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
složil	složit	k5eAaPmAgMnS	složit
jen	jen	k9	jen
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
část	část	k1gFnSc4	část
státní	státní	k2eAgFnSc2d1	státní
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
univerzitních	univerzitní	k2eAgNnPc2d1	univerzitní
studií	studio	k1gNnPc2	studio
zanechal	zanechat	k5eAaPmAgInS	zanechat
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
novinářskému	novinářský	k2eAgNnSc3d1	novinářské
povolání	povolání	k1gNnSc3	povolání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ho	on	k3xPp3gMnSc4	on
lákalo	lákat	k5eAaImAgNnS	lákat
již	již	k6eAd1	již
od	od	k7c2	od
středoškolských	středoškolský	k2eAgFnPc2d1	středoškolská
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
sociálně	sociálně	k6eAd1	sociálně
demokratických	demokratický	k2eAgInPc2d1	demokratický
Dělnických	dělnický	k2eAgInPc2d1	dělnický
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
novinářkou	novinářka	k1gFnSc7	novinářka
a	a	k8xC	a
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
Helenou	Helena	k1gFnSc7	Helena
Malířovou	malířův	k2eAgFnSc7d1	malířova
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gMnPc4	jeho
dlouholetou	dlouholetý	k2eAgFnSc7d1	dlouholetá
družkou	družka	k1gFnSc7	družka
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
sociálně	sociálně	k6eAd1	sociálně
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
pražského	pražský	k2eAgNnSc2d1	Pražské
Práva	právo	k1gNnSc2	právo
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
demokracii	demokracie	k1gFnSc6	demokracie
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
tzv.	tzv.	kA	tzv.
marxistické	marxistický	k2eAgFnSc2d1	marxistická
levice	levice	k1gFnSc2	levice
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
pobýval	pobývat	k5eAaImAgMnS	pobývat
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
sovětském	sovětský	k2eAgNnSc6d1	sovětské
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k9	i
II	II	kA	II
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
internacionály	internacionála	k1gFnSc2	internacionála
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
redaktorem	redaktor	k1gMnSc7	redaktor
Rudého	rudý	k1gMnSc2	rudý
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
byl	být	k5eAaImAgInS	být
dvakrát	dvakrát	k6eAd1	dvakrát
vězněn	vězněn	k2eAgInSc1d1	vězněn
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
příliš	příliš	k6eAd1	příliš
revoluční	revoluční	k2eAgInPc4d1	revoluční
komunistické	komunistický	k2eAgInPc4d1	komunistický
názory	názor	k1gInPc4	názor
–	–	k?	–
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
ve	v	k7c6	v
Slezské	slezský	k2eAgFnSc6d1	Slezská
Ostravě	Ostrava	k1gFnSc6	Ostrava
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
na	na	k7c6	na
Pankráci	Pankrác	k1gMnSc6	Pankrác
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
však	však	k9	však
podepsal	podepsat	k5eAaPmAgMnS	podepsat
tzv.	tzv.	kA	tzv.
manifest	manifest	k1gInSc1	manifest
sedmi	sedm	k4xCc2	sedm
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
protest	protest	k1gInSc4	protest
sedmi	sedm	k4xCc2	sedm
umělců	umělec	k1gMnPc2	umělec
proti	proti	k7c3	proti
nové	nový	k2eAgFnSc3d1	nová
linii	linie	k1gFnSc3	linie
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
V.	V.	kA	V.
sjezdu	sjezd	k1gInSc2	sjezd
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
proto	proto	k6eAd1	proto
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1929	[number]	k4	1929
z	z	k7c2	z
KSČ	KSČ	kA	KSČ
vyloučen	vyloučen	k2eAgInSc1d1	vyloučen
<g/>
.	.	kIx.	.
<g/>
Olbracht	Olbracht	k1gInSc1	Olbracht
se	se	k3xPyFc4	se
na	na	k7c4	na
čas	čas	k1gInSc4	čas
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
z	z	k7c2	z
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
pobýval	pobývat	k5eAaImAgMnS	pobývat
často	často	k6eAd1	často
na	na	k7c6	na
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založil	založit	k5eAaPmAgMnS	založit
Komitét	komitét	k1gInSc4	komitét
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
pracujícího	pracující	k2eAgInSc2d1	pracující
lidu	lid	k1gInSc2	lid
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
mnoho	mnoho	k4c4	mnoho
osobností	osobnost	k1gFnPc2	osobnost
kulturního	kulturní	k2eAgInSc2d1	kulturní
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
např.	např.	kA	např.
František	František	k1gMnSc1	František
Xaver	Xaver	k1gMnSc1	Xaver
Šalda	Šalda	k1gMnSc1	Šalda
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
zde	zde	k6eAd1	zde
i	i	k9	i
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Kraj	kraj	k1gInSc1	kraj
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
nesmírně	smírně	k6eNd1	smírně
zaostalý	zaostalý	k2eAgMnSc1d1	zaostalý
a	a	k8xC	a
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
zde	zde	k6eAd1	zde
stále	stále	k6eAd1	stále
kolovaly	kolovat	k5eAaImAgFnP	kolovat
záhadné	záhadný	k2eAgFnPc1d1	záhadná
legendy	legenda	k1gFnPc1	legenda
o	o	k7c6	o
hrdinech	hrdina	k1gMnPc6	hrdina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
čarodějnicích	čarodějnice	k1gFnPc6	čarodějnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
skrývají	skrývat	k5eAaImIp3nP	skrývat
v	v	k7c6	v
neprostupných	prostupný	k2eNgInPc6d1	neprostupný
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
rozešel	rozejít	k5eAaPmAgInS	rozejít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
družkou	družka	k1gFnSc7	družka
Helenou	Helena	k1gFnSc7	Helena
Malířovou	malířův	k2eAgFnSc7d1	malířova
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
absolventkou	absolventka	k1gFnSc7	absolventka
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
Jaroslavou	Jaroslava	k1gFnSc7	Jaroslava
Kellerovou	Kellerová	k1gFnSc7	Kellerová
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Ivana	Ivan	k1gMnSc2	Ivan
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
žil	žít	k5eAaImAgMnS	žít
i	i	k9	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
vesničce	vesnička	k1gFnSc6	vesnička
Stříbřec	Stříbřec	k1gInSc1	Stříbřec
na	na	k7c6	na
Třeboňsku	Třeboňsko	k1gNnSc6	Třeboňsko
<g/>
.	.	kIx.	.
</s>
<s>
Vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
opět	opět	k6eAd1	opět
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
ilegální	ilegální	k2eAgFnSc2d1	ilegální
komunistické	komunistický	k2eAgFnSc2d1	komunistická
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Ústředního	ústřední	k2eAgInSc2d1	ústřední
výboru	výbor	k1gInSc2	výbor
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
tohoto	tento	k3xDgInSc2	tento
orgánu	orgán	k1gInSc2	orgán
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
i	i	k9	i
na	na	k7c6	na
osmém	osmý	k4xOgInSc6	osmý
a	a	k8xC	a
devátém	devátý	k4xOgInSc6	devátý
sjezdu	sjezd	k1gInSc6	sjezd
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
Ústavodárného	ústavodárný	k2eAgNnSc2d1	Ústavodárné
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
KSČ	KSČ	kA	KSČ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
setrval	setrvat	k5eAaPmAgInS	setrvat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
národním	národní	k2eAgMnSc7d1	národní
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
zastával	zastávat	k5eAaImAgMnS	zastávat
Olbracht	Olbracht	k1gMnSc1	Olbracht
vedoucí	vedoucí	k2eAgFnSc2d1	vedoucí
funkce	funkce	k1gFnSc2	funkce
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
tiskovém	tiskový	k2eAgMnSc6d1	tiskový
(	(	kIx(	(
<g/>
publikačním	publikační	k2eAgInSc6d1	publikační
<g/>
)	)	kIx)	)
odboru	odbor	k1gInSc6	odbor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
–	–	k?	–
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
–	–	k?	–
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
i	i	k8xC	i
knižní	knižní	k2eAgFnSc4d1	knižní
cenzuru	cenzura	k1gFnSc4	cenzura
a	a	k8xC	a
vyřazování	vyřazování	k1gNnSc4	vyřazování
závadné	závadný	k2eAgFnSc2d1	závadná
literatury	literatura	k1gFnSc2	literatura
z	z	k7c2	z
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
.	.	kIx.	.
<g/>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1952	[number]	k4	1952
ve	v	k7c6	v
Státním	státní	k2eAgNnSc6d1	státní
sanatoriu	sanatorium	k1gNnSc6	sanatorium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
sociálními	sociální	k2eAgFnPc7d1	sociální
otázkami	otázka	k1gFnPc7	otázka
<g/>
,	,	kIx,	,
socialismem	socialismus	k1gInSc7	socialismus
a	a	k8xC	a
revoltou	revolta	k1gFnSc7	revolta
proti	proti	k7c3	proti
měšťácké	měšťácký	k2eAgFnSc3d1	měšťácká
společnosti	společnost	k1gFnSc3	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
výrazným	výrazný	k2eAgInSc7d1	výrazný
zlomem	zlom	k1gInSc7	zlom
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
byla	být	k5eAaImAgFnS	být
návštěva	návštěva	k1gFnSc1	návštěva
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
<g/>
,	,	kIx,	,
usadil	usadit	k5eAaPmAgMnS	usadit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Koločava	Koločava	k1gFnSc1	Koločava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
shromažďoval	shromažďovat	k5eAaImAgMnS	shromažďovat
materiál	materiál	k1gInSc4	materiál
k	k	k7c3	k
připravovanému	připravovaný	k2eAgNnSc3d1	připravované
dílu	dílo	k1gNnSc3	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Podkarpatské	podkarpatský	k2eAgFnPc1d1	Podkarpatská
Rusi	Rus	k1gFnPc1	Rus
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
problematice	problematika	k1gFnSc3	problematika
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
celá	celý	k2eAgFnSc1d1	celá
30	[number]	k4	30
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
bývá	bývat	k5eAaImIp3nS	bývat
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
literární	literární	k2eAgFnSc2d1	literární
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnPc3	jeho
postavám	postava	k1gFnPc3	postava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
vypjatých	vypjatý	k2eAgFnPc2d1	vypjatá
situací	situace	k1gFnPc2	situace
a	a	k8xC	a
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jejich	jejich	k3xOp3gFnSc4	jejich
psychiku	psychika	k1gFnSc4	psychika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
také	také	k9	také
k	k	k7c3	k
psychologické	psychologický	k2eAgFnSc3d1	psychologická
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
zlých	zlý	k2eAgMnPc6d1	zlý
samotářích	samotář	k1gMnPc6	samotář
–	–	k?	–
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
tří	tři	k4xCgFnPc2	tři
povídek	povídka	k1gFnPc2	povídka
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
cirkusáků	cirkusák	k1gMnPc2	cirkusák
<g/>
,	,	kIx,	,
tuláků	tulák	k1gMnPc2	tulák
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
bojují	bojovat	k5eAaImIp3nP	bojovat
o	o	k7c4	o
udržení	udržení	k1gNnSc4	udržení
zbytků	zbytek	k1gInPc2	zbytek
své	svůj	k3xOyFgFnSc2	svůj
důstojnosti	důstojnost	k1gFnSc2	důstojnost
a	a	k8xC	a
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
jedinci	jedinec	k1gMnPc1	jedinec
jsou	být	k5eAaImIp3nP	být
zatlačeni	zatlačit	k5eAaPmNgMnP	zatlačit
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc2d2	veliký
izolace	izolace	k1gFnSc2	izolace
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
probouzí	probouzet	k5eAaImIp3nP	probouzet
zlo	zlo	k1gNnSc4	zlo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
těchto	tento	k3xDgMnPc6	tento
samotářích	samotář	k1gMnPc6	samotář
lze	lze	k6eAd1	lze
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
lehce	lehko	k6eAd1	lehko
anarchističtí	anarchistický	k2eAgMnPc1d1	anarchistický
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
jsou	být	k5eAaImIp3nP	být
obdobou	obdoba	k1gFnSc7	obdoba
Gorkého	Gorkého	k2eAgInPc2d1	Gorkého
bosáků	bosák	k1gMnPc2	bosák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Joska	Joska	k6eAd1	Joska
<g/>
,	,	kIx,	,
Forko	forka	k1gFnSc5	forka
a	a	k8xC	a
Pavlínka	Pavlínka	k1gFnSc1	Pavlínka
</s>
</p>
<p>
<s>
Rasík	rasík	k1gMnSc1	rasík
a	a	k8xC	a
pes	pes	k1gMnSc1	pes
</s>
</p>
<p>
<s>
Bratr	bratr	k1gMnSc1	bratr
Žak	Žak	k1gMnSc1	Žak
</s>
</p>
<p>
<s>
Žalář	žalář	k1gInSc1	žalář
nejtemnější	temný	k2eAgFnSc2d3	nejtemnější
–	–	k?	–
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
pensiovaném	pensiovaný	k2eAgMnSc6d1	pensiovaný
policejním	policejní	k2eAgMnSc6d1	policejní
komisaři	komisař	k1gMnSc6	komisař
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
oslepne	oslepnout	k5eAaPmIp3nS	oslepnout
<g/>
.	.	kIx.	.
</s>
<s>
Ústředním	ústřední	k2eAgInSc7d1	ústřední
motivem	motiv	k1gInSc7	motiv
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
paranoická	paranoický	k2eAgFnSc1d1	paranoická
žárlivost	žárlivost	k1gFnSc1	žárlivost
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yRgFnSc3	který
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
hrubý	hrubý	k2eAgInSc1d1	hrubý
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
slepotu	slepota	k1gFnSc4	slepota
velice	velice	k6eAd1	velice
těžce	těžce	k6eAd1	těžce
nese	nést	k5eAaImIp3nS	nést
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
psychicky	psychicky	k6eAd1	psychicky
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ho	on	k3xPp3gMnSc4	on
opouští	opouštět	k5eAaImIp3nS	opouštět
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
myšlen	myslet	k5eAaImNgInS	myslet
jako	jako	k9	jako
žalář	žalář	k1gInSc1	žalář
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
žárlivosti	žárlivost	k1gFnSc2	žárlivost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podivné	podivný	k2eAgNnSc1d1	podivné
přátelství	přátelství	k1gNnSc1	přátelství
herce	herec	k1gMnSc2	herec
Jesenia	Jesenium	k1gNnSc2	Jesenium
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
též	též	k9	též
Přátelství	přátelství	k1gNnSc4	přátelství
Jesenia	Jesenium	k1gNnSc2	Jesenium
<g/>
)	)	kIx)	)
–	–	k?	–
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
psychologické	psychologický	k2eAgNnSc4d1	psychologické
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
líčen	líčen	k2eAgInSc1d1	líčen
život	život	k1gInSc1	život
pražského	pražský	k2eAgMnSc2d1	pražský
umělce	umělec	k1gMnSc2	umělec
Jesenia	Jesenium	k1gNnSc2	Jesenium
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zásadový	zásadový	k2eAgInSc4d1	zásadový
<g/>
,	,	kIx,	,
pracovitý	pracovitý	k2eAgInSc4d1	pracovitý
<g/>
,	,	kIx,	,
vyrovnaný	vyrovnaný	k2eAgInSc4d1	vyrovnaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
talentu	talent	k1gInSc2	talent
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gMnSc1	jeho
konkurent	konkurent	k1gMnSc1	konkurent
herec	herec	k1gMnSc1	herec
Veselý	Veselý	k1gMnSc1	Veselý
je	být	k5eAaImIp3nS	být
lehkomyslný	lehkomyslný	k2eAgMnSc1d1	lehkomyslný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nadaný	nadaný	k2eAgMnSc1d1	nadaný
a	a	k8xC	a
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
na	na	k7c4	na
co	co	k3yRnSc4	co
sáhne	sáhnout	k5eAaPmIp3nS	sáhnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
daří	dařit	k5eAaImIp3nS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Jesenia	Jesenium	k1gNnPc1	Jesenium
velice	velice	k6eAd1	velice
trápí	trápit	k5eAaImIp3nP	trápit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
velice	velice	k6eAd1	velice
snaží	snažit	k5eAaImIp3nS	snažit
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
chybí	chybit	k5eAaPmIp3nS	chybit
talent	talent	k1gInSc1	talent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zamřížované	zamřížovaný	k2eAgNnSc1d1	zamřížované
zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
–	–	k?	–
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
ve	v	k7c6	v
Slezské	slezský	k2eAgFnSc6d1	Slezská
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Devět	devět	k4xCc1	devět
veselých	veselý	k2eAgFnPc2d1	veselá
povídek	povídka	k1gFnPc2	povídka
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
i	i	k8xC	i
republiky	republika	k1gFnSc2	republika
–	–	k?	–
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
upravené	upravený	k2eAgNnSc1d1	upravené
vydání	vydání	k1gNnSc1	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Bejvávalo	Bejvávalo	k1gFnSc2	Bejvávalo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
povídkách	povídka	k1gFnPc6	povídka
se	se	k3xPyFc4	se
humorně	humorně	k6eAd1	humorně
<g/>
,	,	kIx,	,
se	s	k7c7	s
satirickým	satirický	k2eAgInSc7d1	satirický
výsměchem	výsměch	k1gInSc7	výsměch
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
chyby	chyba	k1gFnPc4	chyba
starého	starý	k2eAgNnSc2d1	staré
Rakouska	Rakousko	k1gNnSc2	Rakousko
i	i	k8xC	i
nové	nový	k2eAgFnSc2d1	nová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
především	především	k9	především
cenzuru	cenzura	k1gFnSc4	cenzura
<g/>
,	,	kIx,	,
byrokracii	byrokracie	k1gFnSc4	byrokracie
a	a	k8xC	a
korupci	korupce	k1gFnSc4	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Martin	Martin	k1gMnSc1	Martin
Frič	Frič	k1gMnSc1	Frič
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
film	film	k1gInSc1	film
Prstýnek	prstýnek	k1gInSc4	prstýnek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
proletářka	proletářka	k1gFnSc1	proletářka
–	–	k?	–
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
agitační	agitační	k2eAgInSc1d1	agitační
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
zrání	zrání	k1gNnSc4	zrání
služky	služka	k1gFnSc2	služka
v	v	k7c4	v
uvědomělou	uvědomělý	k2eAgFnSc4d1	uvědomělá
revoluční	revoluční	k2eAgFnSc4d1	revoluční
pracovnici	pracovnice	k1gFnSc4	pracovnice
a	a	k8xC	a
bojovnici	bojovnice	k1gFnSc4	bojovnice
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pokus	pokus	k1gInSc4	pokus
vylíčit	vylíčit	k5eAaPmF	vylíčit
z	z	k7c2	z
třídního	třídní	k2eAgNnSc2d1	třídní
hlediska	hledisko	k1gNnSc2	hledisko
buržoazní	buržoazní	k2eAgNnSc1d1	buržoazní
a	a	k8xC	a
proletářské	proletářský	k2eAgNnSc1d1	proletářské
prostředí	prostředí	k1gNnSc1	prostředí
a	a	k8xC	a
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
pak	pak	k6eAd1	pak
ukázat	ukázat	k5eAaPmF	ukázat
zrod	zrod	k1gInSc4	zrod
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
Lidový	lidový	k2eAgInSc4d1	lidový
dům	dům	k1gInSc4	dům
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
především	především	k9	především
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
–	–	k?	–
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
beletristická	beletristický	k2eAgFnSc1d1	beletristická
adaptace	adaptace	k1gFnSc1	adaptace
díla	dílo	k1gNnSc2	dílo
amerického	americký	k2eAgMnSc2d1	americký
historika	historik	k1gMnSc2	historik
W.	W.	kA	W.
H.	H.	kA	H.
Prescotta	Prescotta	k1gMnSc1	Prescotta
–	–	k?	–
Dějiny	dějiny	k1gFnPc1	dějiny
dobytí	dobytí	k1gNnSc2	dobytí
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
španělském	španělský	k2eAgInSc6d1	španělský
dobyvateli	dobyvatel	k1gMnSc3	dobyvatel
H.	H.	kA	H.
Cortesovi	Cortes	k1gMnSc3	Cortes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnSc2	kniha
z	z	k7c2	z
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
===	===	k?	===
</s>
</p>
<p>
<s>
Země	zem	k1gFnPc1	zem
bez	bez	k7c2	bez
jména	jméno	k1gNnSc2	jméno
–	–	k?	–
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
reportáže	reportáž	k1gFnPc4	reportáž
informující	informující	k2eAgFnPc4d1	informující
o	o	k7c6	o
špatné	špatný	k2eAgFnSc6d1	špatná
sociální	sociální	k2eAgFnSc3d1	sociální
a	a	k8xC	a
národnostní	národnostní	k2eAgFnSc3d1	národnostní
situaci	situace	k1gFnSc3	situace
v	v	k7c6	v
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Zakarpatské	zakarpatský	k2eAgFnSc6d1	Zakarpatská
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
analýza	analýza	k1gFnSc1	analýza
historických	historický	k2eAgFnPc2d1	historická
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
charakteristika	charakteristika	k1gFnSc1	charakteristika
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
rusínských	rusínský	k2eAgMnPc2d1	rusínský
pastevců	pastevec	k1gMnPc2	pastevec
a	a	k8xC	a
dřevorubců	dřevorubec	k1gMnPc2	dřevorubec
<g/>
,	,	kIx,	,
židovských	židovský	k2eAgMnPc2d1	židovský
řemeslníků	řemeslník	k1gMnPc2	řemeslník
a	a	k8xC	a
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
,	,	kIx,	,
Rumunů	Rumun	k1gMnPc2	Rumun
<g/>
,	,	kIx,	,
Cikánů	cikán	k1gMnPc2	cikán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vesnice	vesnice	k1gFnSc1	vesnice
XI	XI	kA	XI
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
–	–	k?	–
autor	autor	k1gMnSc1	autor
čtenáře	čtenář	k1gMnSc2	čtenář
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
Podkarpatska	Podkarpatsko	k1gNnSc2	Podkarpatsko
<g/>
.	.	kIx.	.
</s>
<s>
Poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
drsné	drsný	k2eAgFnPc4d1	drsná
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
zdejší	zdejší	k2eAgMnPc1d1	zdejší
lidé	člověk	k1gMnPc1	člověk
musí	muset	k5eAaImIp3nP	muset
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Půda	půda	k1gFnSc1	půda
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
neúrodná	úrodný	k2eNgFnSc1d1	neúrodná
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
muži	muž	k1gMnPc1	muž
odjíždí	odjíždět	k5eAaImIp3nP	odjíždět
na	na	k7c4	na
lesní	lesní	k2eAgFnPc4d1	lesní
práce	práce	k1gFnPc4	práce
do	do	k7c2	do
Haliče	Halič	k1gFnSc2	Halič
<g/>
,	,	kIx,	,
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
<g/>
,	,	kIx,	,
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
ujímají	ujímat	k5eAaImIp3nP	ujímat
mužské	mužský	k2eAgFnSc2d1	mužská
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
neúprosný	úprosný	k2eNgInSc1d1	neúprosný
a	a	k8xC	a
těžký	těžký	k2eAgInSc1d1	těžký
<g/>
.	.	kIx.	.
</s>
<s>
Přežívají	přežívat	k5eAaImIp3nP	přežívat
jen	jen	k9	jen
nejsilnější	silný	k2eAgMnPc1d3	nejsilnější
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
nemají	mít	k5eNaImIp3nP	mít
co	co	k3yRnSc4	co
jíst	jíst	k5eAaImF	jíst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
církvi	církev	k1gFnSc3	církev
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
musí	muset	k5eAaImIp3nP	muset
odvádět	odvádět	k5eAaImF	odvádět
daně	daň	k1gFnSc2	daň
a	a	k8xC	a
desátky	desátka	k1gFnSc2	desátka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Podkarpatsku	Podkarpatsko	k1gNnSc6	Podkarpatsko
působí	působit	k5eAaImIp3nS	působit
církev	církev	k1gFnSc1	církev
řecko-katolická	řeckoatolický	k2eAgFnSc1d1	řecko-katolická
a	a	k8xC	a
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
náboženstvím	náboženství	k1gNnSc7	náboženství
karpatských	karpatský	k2eAgMnPc2d1	karpatský
pastevců	pastevec	k1gMnPc2	pastevec
a	a	k8xC	a
dřevorubců	dřevorubec	k1gMnPc2	dřevorubec
je	být	k5eAaImIp3nS	být
směsice	směsice	k1gFnSc1	směsice
pohanství	pohanství	k1gNnSc2	pohanství
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Uznávají	uznávat	k5eAaImIp3nP	uznávat
svého	svůj	k3xOyFgMnSc4	svůj
boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
nazývají	nazývat	k5eAaImIp3nP	nazývat
Božko	Božka	k1gFnSc5	Božka
nebo	nebo	k8xC	nebo
Dažboh	Dažboha	k1gFnPc2	Dažboha
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
Rusínů	rusín	k1gInPc2	rusín
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
životem	život	k1gInSc7	život
středověkým	středověký	k2eAgInSc7d1	středověký
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
primitivním	primitivní	k2eAgNnSc7d1	primitivní
hospodářstvím	hospodářství	k1gNnSc7	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Obživu	obživa	k1gFnSc4	obživa
jim	on	k3xPp3gMnPc3	on
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pastevectví	pastevectví	k1gNnSc4	pastevectví
a	a	k8xC	a
klučení	klučení	k1gNnSc4	klučení
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
výroba	výroba	k1gFnSc1	výroba
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
obuvi	obuv	k1gFnSc2	obuv
a	a	k8xC	a
nářadí	nářadí	k1gNnSc2	nářadí
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
bez	bez	k7c2	bez
gramotnosti	gramotnost	k1gFnSc2	gramotnost
<g/>
,	,	kIx,	,
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ti	ty	k3xPp2nSc3	ty
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgInPc6	který
tu	tu	k6eAd1	tu
dříve	dříve	k6eAd2	dříve
nebylo	být	k5eNaImAgNnS	být
slýcháno	slýchat	k5eAaImNgNnS	slýchat
–	–	k?	–
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
reportáži	reportáž	k1gFnSc6	reportáž
se	se	k3xPyFc4	se
čtenář	čtenář	k1gMnSc1	čtenář
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
vliv	vliv	k1gInSc4	vliv
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
Podkarpatské	podkarpatský	k2eAgFnSc6d1	Podkarpatská
Rusi	Rus	k1gFnSc6	Rus
Češi	Čech	k1gMnPc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
podkarpatský	podkarpatský	k2eAgInSc1d1	podkarpatský
národ	národ	k1gInSc1	národ
počeštit	počeštit	k5eAaPmF	počeštit
<g/>
.	.	kIx.	.
</s>
<s>
Zavádí	zavádět	k5eAaImIp3nS	zavádět
zde	zde	k6eAd1	zde
české	český	k2eAgFnPc4d1	Česká
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
obchody	obchod	k1gInPc4	obchod
<g/>
,	,	kIx,	,
chovají	chovat	k5eAaImIp3nP	chovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
kolonizátoři	kolonizátor	k1gMnPc1	kolonizátor
<g/>
.	.	kIx.	.
</s>
<s>
Nezáleží	záležet	k5eNaImIp3nS	záležet
jim	on	k3xPp3gMnPc3	on
na	na	k7c6	na
Rusínech	Rusín	k1gMnPc6	Rusín
<g/>
.	.	kIx.	.
</s>
<s>
Berou	brát	k5eAaImIp3nP	brát
jim	on	k3xPp3gMnPc3	on
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
Rusíni	Rusín	k1gMnPc1	Rusín
musí	muset	k5eAaImIp3nP	muset
živořit	živořit	k5eAaImF	živořit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Židé	Žid	k1gMnPc1	Žid
–	–	k?	–
autor	autor	k1gMnSc1	autor
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
židovské	židovský	k2eAgNnSc4d1	Židovské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	jeho	k3xOp3gFnSc4	jeho
současnou	současný	k2eAgFnSc4d1	současná
chudobu	chudoba	k1gFnSc4	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
banky	banka	k1gFnPc1	banka
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
Židé	Žid	k1gMnPc1	Žid
nemohou	moct	k5eNaImIp3nP	moct
půjčovat	půjčovat	k5eAaImF	půjčovat
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
bohatnout	bohatnout	k5eAaImF	bohatnout
<g/>
.	.	kIx.	.
</s>
<s>
Olbracht	Olbracht	k1gMnSc1	Olbracht
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
obyčejné	obyčejný	k2eAgFnSc6d1	obyčejná
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c4	na
Zakarpatsku	Zakarpatska	k1gFnSc4	Zakarpatska
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
jazyk	jazyk	k1gInSc4	jazyk
–	–	k?	–
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
části	část	k1gFnSc6	část
své	svůj	k3xOyFgFnSc2	svůj
knihy	kniha	k1gFnSc2	kniha
Olbracht	Olbracht	k1gMnSc1	Olbracht
nabádá	nabádat	k5eAaBmIp3nS	nabádat
Rusíny	rusín	k1gInPc4	rusín
k	k	k7c3	k
boji	boj	k1gInSc3	boj
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
národnost	národnost	k1gFnSc4	národnost
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Hledá	hledat	k5eAaImIp3nS	hledat
správné	správný	k2eAgNnSc1d1	správné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Podkarpatska	Podkarpatsko	k1gNnSc2	Podkarpatsko
a	a	k8xC	a
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nejbližší	blízký	k2eAgFnSc4d3	nejbližší
jejich	jejich	k3xOp3gInSc3	jejich
jazyku	jazyk	k1gInSc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Dospěje	dochvít	k5eAaPmIp3nS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ukrajinský	ukrajinský	k2eAgInSc4d1	ukrajinský
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
ukrajinské	ukrajinský	k2eAgMnPc4d1	ukrajinský
intelektuály	intelektuál	k1gMnPc4	intelektuál
a	a	k8xC	a
spisovatele	spisovatel	k1gMnPc4	spisovatel
a	a	k8xC	a
podněcuje	podněcovat	k5eAaImIp3nS	podněcovat
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
tvorbě	tvorba	k1gFnSc3	tvorba
a	a	k8xC	a
boji	boj	k1gInSc6	boj
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
<g/>
Nikola	Nikola	k1gMnSc1	Nikola
Šuhaj	šuhaj	k1gMnSc1	šuhaj
loupežník	loupežník	k1gMnSc1	loupežník
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
–	–	k?	–
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
loupežníkovi	loupežník	k1gMnSc6	loupežník
Nikolovi	Nikola	k1gMnSc6	Nikola
(	(	kIx(	(
<g/>
skutečná	skutečný	k2eAgFnSc1d1	skutečná
postava	postava	k1gFnSc1	postava
<g/>
)	)	kIx)	)
dávajícím	dávající	k2eAgMnPc3d1	dávající
chudým	chudý	k1gMnPc3	chudý
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
bohatým	bohatý	k2eAgMnSc7d1	bohatý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
činnost	činnost	k1gFnSc4	činnost
je	být	k5eAaImIp3nS	být
pronásledován	pronásledován	k2eAgInSc1d1	pronásledován
četníky	četník	k1gMnPc7	četník
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
vypsána	vypsán	k2eAgFnSc1d1	vypsána
velká	velký	k2eAgFnSc1d1	velká
odměna	odměna	k1gFnSc1	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Nikola	Nikola	k1gFnSc1	Nikola
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
v	v	k7c6	v
lesích	les	k1gInPc6	les
a	a	k8xC	a
jen	jen	k9	jen
občas	občas	k6eAd1	občas
schází	scházet	k5eAaImIp3nS	scházet
tajně	tajně	k6eAd1	tajně
do	do	k7c2	do
Koločavy	Koločava	k1gFnSc2	Koločava
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Eržikou	Eržika	k1gFnSc7	Eržika
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ho	on	k3xPp3gNnSc4	on
zabijí	zabít	k5eAaPmIp3nP	zabít
jeho	jeho	k3xOp3gMnPc1	jeho
kamarádi	kamarád	k1gMnPc1	kamarád
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mu	on	k3xPp3gMnSc3	on
nosí	nosit	k5eAaImIp3nP	nosit
zásoby	zásoba	k1gFnPc4	zásoba
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hory	hora	k1gFnPc4	hora
a	a	k8xC	a
staletí	staletí	k1gNnPc4	staletí
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
–	–	k?	–
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Země	zem	k1gFnSc2	zem
beze	beze	k7c2	beze
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kulturní	kulturní	k2eAgFnPc4d1	kulturní
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnPc4d1	sociální
a	a	k8xC	a
zeměpisné	zeměpisný	k2eAgFnPc4d1	zeměpisná
informace	informace	k1gFnPc4	informace
poskládané	poskládaný	k2eAgFnPc4d1	poskládaná
do	do	k7c2	do
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Golet	golet	k1gInSc1	golet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
–	–	k?	–
povídkový	povídkový	k2eAgInSc4d1	povídkový
soubor	soubor	k1gInSc4	soubor
s	s	k7c7	s
židovskou	židovský	k2eAgFnSc7d1	židovská
tematikou	tematika	k1gFnSc7	tematika
(	(	kIx(	(
<g/>
Zázrak	zázrak	k1gInSc1	zázrak
s	s	k7c7	s
Julčou	Julča	k1gFnSc7	Julča
<g/>
,	,	kIx,	,
Událost	událost	k1gFnSc1	událost
v	v	k7c4	v
mikve	mikev	k1gFnPc4	mikev
<g/>
,	,	kIx,	,
O	o	k7c6	o
smutných	smutný	k2eAgNnPc6d1	smutné
očích	oko	k1gNnPc6	oko
Hany	Hana	k1gFnSc2	Hana
Karadžičové	Karadžičové	k2eAgInPc4d1	Karadžičové
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
v	v	k7c6	v
ghettech	ghetto	k1gNnPc6	ghetto
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
golet	golet	k1gInSc1	golet
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ג	ג	k?	ג
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
exil	exil	k1gInSc1	exil
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
použit	použít	k5eAaPmNgInS	použít
coby	coby	k?	coby
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
malou	malý	k2eAgFnSc4d1	malá
židovskou	židovský	k2eAgFnSc4d1	židovská
vesničku	vesnička	k1gFnSc4	vesnička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
smutných	smutný	k2eAgNnPc6d1	smutné
očích	oko	k1gNnPc6	oko
Hany	Hana	k1gFnSc2	Hana
Karadžičové	Karadžičové	k2eAgMnSc7d1	Karadžičové
–	–	k?	–
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
židovské	židovský	k2eAgFnSc6d1	židovská
dívce	dívka	k1gFnSc6	dívka
Hanele	Hanel	k1gInSc2	Hanel
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
vesničky	vesnička	k1gFnSc2	vesnička
Polany	polana	k1gFnSc2	polana
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
goletu	golet	k1gInSc3	golet
<g/>
"	"	kIx"	"
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
cestě	cesta	k1gFnSc6	cesta
ven	ven	k6eAd1	ven
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Odchází	odcházet	k5eAaImIp3nS	odcházet
se	s	k7c7	s
svolením	svolení	k1gNnSc7	svolení
otce	otec	k1gMnSc2	otec
do	do	k7c2	do
Moravské	moravský	k2eAgFnSc2d1	Moravská
Ostravy	Ostrava	k1gFnSc2	Ostrava
<g/>
,	,	kIx,	,
do	do	k7c2	do
sionistického	sionistický	k2eAgNnSc2d1	sionistické
střediska	středisko	k1gNnSc2	středisko
<g/>
,	,	kIx,	,
připravovat	připravovat	k5eAaImF	připravovat
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
Ivem	Ivo	k1gMnSc7	Ivo
Karadžičem	Karadžič	k1gMnSc7	Karadžič
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Izákem	Izák	k1gMnSc7	Izák
Kohnem	Kohn	k1gMnSc7	Kohn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odpadl	odpadnout	k5eAaPmAgInS	odpadnout
od	od	k7c2	od
židovství	židovství	k1gNnSc2	židovství
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	se	k3xPyFc2	se
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
a	a	k8xC	a
Ivo	Ivo	k1gMnSc1	Ivo
jede	jet	k5eAaImIp3nS	jet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hanou	Hana	k1gFnSc7	Hana
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
rodné	rodný	k2eAgFnSc2d1	rodná
vsi	ves	k1gFnSc2	ves
požádat	požádat	k5eAaPmF	požádat
otce	otec	k1gMnSc4	otec
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
znamenat	znamenat	k5eAaImF	znamenat
rozchod	rozchod	k1gInSc4	rozchod
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
lidmi	člověk	k1gMnPc7	člověk
z	z	k7c2	z
přísně	přísně	k6eAd1	přísně
ortodoxního	ortodoxní	k2eAgInSc2d1	ortodoxní
židovského	židovský	k2eAgInSc2d1	židovský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
dramatických	dramatický	k2eAgFnPc2d1	dramatická
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
oběma	dva	k4xCgMnPc7	dva
nakonec	nakonec	k6eAd1	nakonec
podaří	podařit	k5eAaPmIp3nS	podařit
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
ovšem	ovšem	k9	ovšem
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
navždy	navždy	k6eAd1	navždy
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odpadla	odpadnout	k5eAaPmAgFnS	odpadnout
od	od	k7c2	od
víry	víra	k1gFnSc2	víra
svých	svůj	k3xOyFgMnPc2	svůj
předků	předek	k1gMnPc2	předek
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
rodinu	rodina	k1gFnSc4	rodina
i	i	k8xC	i
známé	známá	k1gFnSc2	známá
vlastně	vlastně	k9	vlastně
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
policie	policie	k1gFnSc2	policie
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
ještě	ještě	k9	ještě
uslyší	uslyšet	k5eAaPmIp3nP	uslyšet
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
modlí	modlit	k5eAaImIp3nS	modlit
kaddiš	kaddiš	k1gInSc1	kaddiš
<g/>
,	,	kIx,	,
modlitbu	modlitba	k1gFnSc4	modlitba
za	za	k7c4	za
mrtvé	mrtvý	k1gMnPc4	mrtvý
<g/>
,	,	kIx,	,
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
<g/>
...	...	k?	...
Smutek	smutek	k1gInSc4	smutek
do	do	k7c2	do
jejích	její	k3xOp3gNnPc2	její
očí	oko	k1gNnPc2	oko
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
a	a	k8xC	a
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
úseku	úsek	k1gInSc2	úsek
života	život	k1gInSc2	život
musela	muset	k5eAaImAgFnS	muset
zemřít	zemřít	k5eAaPmF	zemřít
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byli	být	k5eAaImAgMnP	být
celým	celý	k2eAgInSc7d1	celý
jejím	její	k3xOp3gInSc7	její
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnPc1	kniha
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
===	===	k?	===
</s>
</p>
<p>
<s>
Biblické	biblický	k2eAgInPc1d1	biblický
příběhy	příběh	k1gInPc1	příběh
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
–	–	k?	–
příběhy	příběh	k1gInPc1	příběh
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
;	;	kIx,	;
autor	autor	k1gMnSc1	autor
vynechal	vynechat	k5eAaPmAgInS	vynechat
vysloveně	vysloveně	k6eAd1	vysloveně
náboženské	náboženský	k2eAgFnPc1d1	náboženská
pasáže	pasáž	k1gFnPc1	pasáž
<g/>
,	,	kIx,	,
bohoslužebné	bohoslužebný	k2eAgFnPc1d1	bohoslužebná
poučky	poučka	k1gFnPc1	poučka
a	a	k8xC	a
didakticko-náboženské	didaktickoáboženský	k2eAgFnPc1d1	didakticko-náboženský
části	část	k1gFnPc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
kapitoly	kapitola	k1gFnPc4	kapitola
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vyprávějí	vyprávět	k5eAaImIp3nP	vyprávět
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
o	o	k7c6	o
společenských	společenský	k2eAgInPc6d1	společenský
a	a	k8xC	a
politických	politický	k2eAgInPc6d1	politický
poměrech	poměr	k1gInPc6	poměr
<g/>
,	,	kIx,	,
o	o	k7c6	o
politickém	politický	k2eAgNnSc6d1	politické
zřízení	zřízení	k1gNnSc6	zřízení
a	a	k8xC	a
o	o	k7c6	o
způsobu	způsob	k1gInSc6	způsob
myšlení	myšlení	k1gNnSc2	myšlení
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
starověkých	starověký	k2eAgMnPc2d1	starověký
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
utrpení	utrpení	k1gNnSc4	utrpení
a	a	k8xC	a
pronásledování	pronásledování	k1gNnSc4	pronásledování
starožidovského	starožidovský	k2eAgInSc2d1	starožidovský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
okupace	okupace	k1gFnSc2	okupace
Československa	Československo	k1gNnSc2	Československo
nacisty	nacista	k1gMnSc2	nacista
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
výzvou	výzva	k1gFnSc7	výzva
k	k	k7c3	k
odporu	odpor	k1gInSc3	odpor
a	a	k8xC	a
ke	k	k7c3	k
statečnosti	statečnost	k1gFnSc3	statečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
starých	starý	k2eAgInPc2d1	starý
letopisů	letopis	k1gInPc2	letopis
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
–	–	k?	–
zpracování	zpracování	k1gNnSc6	zpracování
starých	starý	k2eAgInPc2d1	starý
českých	český	k2eAgInPc2d1	český
mýtů	mýtus	k1gInPc2	mýtus
a	a	k8xC	a
pověstí	pověst	k1gFnPc2	pověst
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
autor	autor	k1gMnSc1	autor
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
vlastními	vlastní	k2eAgInPc7d1	vlastní
komentáři	komentář	k1gInPc7	komentář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
mudrci	mudrc	k1gMnSc6	mudrc
Bidpajovi	Bidpaj	k1gMnSc6	Bidpaj
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
zvířátkách	zvířátko	k1gNnPc6	zvířátko
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
–	–	k?	–
zpracování	zpracování	k1gNnSc4	zpracování
staroindických	staroindický	k2eAgFnPc2d1	staroindická
bajek	bajka	k1gFnPc2	bajka
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
Pančatántra	Pančatántr	k1gMnSc2	Pančatántr
od	od	k7c2	od
Eduarda	Eduard	k1gMnSc2	Eduard
Valečky	valečka	k1gFnSc2	valečka
<g/>
.	.	kIx.	.
</s>
<s>
Olbracht	Olbracht	k2eAgInSc1d1	Olbracht
text	text	k1gInSc1	text
zaktualizoval	zaktualizovat	k5eAaPmAgInS	zaktualizovat
především	především	k9	především
v	v	k7c6	v
jazykové	jazykový	k2eAgFnSc6d1	jazyková
a	a	k8xC	a
stylistické	stylistický	k2eAgFnSc6d1	stylistická
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
Olbrachtovy	Olbrachtův	k2eAgFnSc2d1	Olbrachtova
válečné	válečný	k2eAgFnSc2d1	válečná
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
myšlenkový	myšlenkový	k2eAgInSc1d1	myšlenkový
přínos	přínos	k1gInSc1	přínos
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
ale	ale	k8xC	ale
nejmenší	malý	k2eAgFnPc1d3	nejmenší
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
prací	práce	k1gFnPc2	práce
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dílo	dílo	k1gNnSc4	dílo
pouze	pouze	k6eAd1	pouze
dovršil	dovršit	k5eAaPmAgInS	dovršit
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
samostatným	samostatný	k2eAgMnSc7d1	samostatný
tvůrcem	tvůrce	k1gMnSc7	tvůrce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Drama	drama	k1gNnSc1	drama
===	===	k?	===
</s>
</p>
<p>
<s>
Pátý	pátý	k4xOgInSc1	pátý
akt	akt	k1gInSc1	akt
–	–	k?	–
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
selské	selský	k2eAgFnSc6d1	selská
rebelii	rebelie	k1gFnSc6	rebelie
</s>
</p>
<p>
<s>
===	===	k?	===
Publicistika	publicistika	k1gFnSc1	publicistika
===	===	k?	===
</s>
</p>
<p>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
několik	několik	k4yIc4	několik
nadšených	nadšený	k2eAgFnPc2d1	nadšená
reportáží	reportáž	k1gFnPc2	reportáž
ze	z	k7c2	z
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
především	především	k6eAd1	především
k	k	k7c3	k
socialismu	socialismus	k1gInSc3	socialismus
přistupoval	přistupovat	k5eAaImAgInS	přistupovat
zcela	zcela	k6eAd1	zcela
nekriticky	kriticky	k6eNd1	kriticky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1952	[number]	k4	1952
v	v	k7c6	v
článku	článek	k1gInSc6	článek
pro	pro	k7c4	pro
Rudé	rudý	k2eAgNnSc4d1	Rudé
právo	právo	k1gNnSc4	právo
ostře	ostro	k6eAd1	ostro
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
činnost	činnost	k1gFnSc4	činnost
tzv.	tzv.	kA	tzv.
protistátního	protistátní	k2eAgNnSc2d1	protistátní
spikleneckého	spiklenecký	k2eAgNnSc2d1	spiklenecké
centra	centrum	k1gNnSc2	centrum
vedeného	vedený	k2eAgNnSc2d1	vedené
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Slánským	Slánský	k1gMnSc7	Slánský
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Texty	text	k1gInPc1	text
v	v	k7c6	v
podobném	podobný	k2eAgInSc6d1	podobný
duchu	duch	k1gMnSc3	duch
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
psala	psát	k5eAaImAgFnS	psát
řada	řada	k1gFnSc1	řada
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
dalších	další	k2eAgFnPc2d1	další
veřejných	veřejný	k2eAgFnPc2d1	veřejná
osobností	osobnost	k1gFnPc2	osobnost
i	i	k8xC	i
"	"	kIx"	"
<g/>
pracujících	pracující	k2eAgMnPc2d1	pracující
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
Díla	dílo	k1gNnPc1	dílo
Ivana	Ivan	k1gMnSc2	Ivan
Olbrachta	Olbracht	k1gMnSc2	Olbracht
posloužila	posloužit	k5eAaPmAgFnS	posloužit
jako	jako	k8xC	jako
náměty	námět	k1gInPc1	námět
filmů	film	k1gInPc2	film
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Marijka-nevěrnice	Marijkaevěrnice	k1gFnSc1	Marijka-nevěrnice
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Nový	Nový	k1gMnSc1	Nový
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
</s>
</p>
<p>
<s>
Prstýnek	prstýnek	k1gInSc1	prstýnek
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Frič	Frič	k1gMnSc1	Frič
</s>
</p>
<p>
<s>
Nikola	Nikola	k1gMnSc1	Nikola
Šuhaj	šuhaj	k1gMnSc1	šuhaj
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Miroslav	Miroslav	k1gMnSc1	Miroslav
Josef	Josef	k1gMnSc1	Josef
Krňanský	Krňanský	k2eAgMnSc1d1	Krňanský
</s>
</p>
<p>
<s>
Anna	Anna	k1gFnSc1	Anna
proletářka	proletářka	k1gFnSc1	proletářka
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Steklý	Steklý	k1gMnSc1	Steklý
a	a	k8xC	a
TV	TV	kA	TV
film	film	k1gInSc4	film
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Komedianti	komediant	k1gMnPc1	komediant
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
dle	dle	k7c2	dle
povídky	povídka	k1gFnSc2	povídka
Bratr	bratr	k1gMnSc1	bratr
Žak	Žak	k1gMnSc1	Žak
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vlček	Vlček	k1gMnSc1	Vlček
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nikola	Nikola	k1gMnSc1	Nikola
Šuhaj	šuhaj	k1gMnSc1	šuhaj
loupežník	loupežník	k1gMnSc1	loupežník
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
TV	TV	kA	TV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Evžen	Evžen	k1gMnSc1	Evžen
Sokolovský	sokolovský	k2eAgMnSc1d1	sokolovský
</s>
</p>
<p>
<s>
Balada	balada	k1gFnSc1	balada
pro	pro	k7c4	pro
banditu	bandita	k1gMnSc4	bandita
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Sís	Sís	k1gMnSc1	Sís
</s>
</p>
<p>
<s>
Golet	golet	k1gInSc1	golet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Zeno	Zeno	k1gMnSc1	Zeno
Dostál	Dostál	k1gMnSc1	Dostál
</s>
</p>
<p>
<s>
Hanele	Hanele	k6eAd1	Hanele
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Kachyňa	Kachyňa	k1gMnSc1	Kachyňa
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Lada	Lada	k1gFnSc1	Lada
Jelínková	Jelínková	k1gFnSc1	Jelínková
</s>
</p>
<p>
<s>
Olbracht	Olbracht	k1gMnSc1	Olbracht
i	i	k8xC	i
Koločava	Koločava	k1gFnSc1	Koločava
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Sergey	Serge	k2eAgInPc1d1	Serge
Gubsky	Gubsek	k1gInPc1	Gubsek
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
</s>
</p>
<p>
<s>
==	==	k?	==
Posmrtné	posmrtný	k2eAgFnPc4d1	posmrtná
připomínky	připomínka	k1gFnPc4	připomínka
==	==	k?	==
</s>
</p>
<p>
<s>
ulice	ulice	k1gFnSc1	ulice
Olbrachtova	Olbrachtův	k2eAgFnSc1d1	Olbrachtova
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
4	[number]	k4	4
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
Chrudimi	Chrudim	k1gFnSc6	Chrudim
a	a	k8xC	a
Chebu	Cheb	k1gInSc6	Cheb
</s>
</p>
<p>
<s>
ulice	ulice	k1gFnPc1	ulice
Ivana	Ivan	k1gMnSc2	Ivan
Olbrachta	Olbracht	k1gMnSc2	Olbracht
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Trutnově	Trutnov	k1gInSc6	Trutnov
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Vlašimi	Vlašim	k1gFnSc6	Vlašim
<g/>
,	,	kIx,	,
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
,	,	kIx,	,
Třebíči	Třebíč	k1gFnSc6	Třebíč
a	a	k8xC	a
Kladně	Kladno	k1gNnSc6	Kladno
</s>
</p>
<p>
<s>
Olbrachtovo	Olbrachtův	k2eAgNnSc1d1	Olbrachtovo
náměstí	náměstí	k1gNnSc1	náměstí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
sousoší	sousoší	k1gNnSc1	sousoší
Ivana	Ivan	k1gMnSc2	Ivan
Olbrachta	Olbracht	k1gMnSc2	Olbracht
a	a	k8xC	a
Antala	Antal	k1gMnSc2	Antal
Staška	Stašek	k1gMnSc2	Stašek
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Semilech	Semily	k1gInPc6	Semily
<g/>
,	,	kIx,	,
pomník	pomník	k1gInSc4	pomník
Ivana	Ivan	k1gMnSc2	Ivan
Olbrachta	Olbracht	k1gMnSc2	Olbracht
v	v	k7c6	v
Koločavě	Koločava	k1gFnSc6	Koločava
<g/>
,	,	kIx,	,
busta	busta	k1gFnSc1	busta
Ivana	Ivan	k1gMnSc2	Ivan
Olbrachta	Olbracht	k1gMnSc2	Olbracht
ve	v	k7c6	v
Stříbřeci	Stříbřece	k1gFnSc6	Stříbřece
</s>
</p>
<p>
<s>
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
Ivana	Ivan	k1gMnSc2	Ivan
Olbrachta	Olbracht	k1gMnSc2	Olbracht
<g/>
,	,	kIx,	,
Antala	Antal	k1gMnSc2	Antal
Staška	Stašek	k1gMnSc2	Stašek
a	a	k8xC	a
Heleny	Helena	k1gFnSc2	Helena
Malířové	Malířové	k?	Malířové
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
4	[number]	k4	4
</s>
</p>
<p>
<s>
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Semilech	Semily	k1gInPc6	Semily
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
na	na	k7c6	na
domě	dům	k1gInSc6	dům
je	být	k5eAaImIp3nS	být
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Antalem	Antal	k1gMnSc7	Antal
Staškem	Stašek	k1gMnSc7	Stašek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
divadlo	divadlo	k1gNnSc1	divadlo
Ivana	Ivan	k1gMnSc2	Ivan
Olbrachta	Olbracht	k1gMnSc2	Olbracht
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
427	[number]	k4	427
<g/>
–	–	k?	–
<g/>
431	[number]	k4	431
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
/	/	kIx~	/
redakce	redakce	k1gFnSc2	redakce
Otakar	Otakar	k1gMnSc1	Otakar
Chaloupka	Chaloupka	k1gMnSc1	Chaloupka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
293	[number]	k4	293
<g/>
–	–	k?	–
<g/>
298	[number]	k4	298
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HNÍZDO	hnízdo	k1gNnSc1	hnízdo
<g/>
,	,	kIx,	,
Vlastislav	Vlastislav	k1gMnSc1	Vlastislav
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dopl	dopl	k1gMnSc1	dopl
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
329	[number]	k4	329
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
407974	[number]	k4	407974
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kulturně-historická	Kulturněistorický	k2eAgFnSc1d1	Kulturně-historická
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
českého	český	k2eAgNnSc2d1	české
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
:	:	kIx,	:
M	M	kA	M
<g/>
–	–	k?	–
<g/>
Ž.	Ž.	kA	Ž.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
regionální	regionální	k2eAgNnPc4d1	regionální
studia	studio	k1gNnPc4	studio
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Ostravské	ostravský	k2eAgFnSc2d1	Ostravská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
575	[number]	k4	575
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7464	[number]	k4	7464
<g/>
-	-	kIx~	-
<g/>
387	[number]	k4	387
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
133	[number]	k4	133
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MUKAŘOVSKÝ	MUKAŘOVSKÝ	kA	MUKAŘOVSKÝ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
IV	IV	kA	IV
<g/>
,	,	kIx,	,
Literatura	literatura	k1gFnSc1	literatura
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Victoria	Victorium	k1gNnSc2	Victorium
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85865	[number]	k4	85865
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
,	,	kIx,	,
s.	s.	k?	s.
554	[number]	k4	554
<g/>
–	–	k?	–
<g/>
571	[number]	k4	571
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MUKAŘOVSKÝ	MUKAŘOVSKÝ	kA	MUKAŘOVSKÝ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
a	a	k8xC	a
slovesnost	slovesnost	k1gFnSc1	slovesnost
<g/>
.	.	kIx.	.
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
123	[number]	k4	123
<g/>
–	–	k?	–
<g/>
134	[number]	k4	134
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OLBRACHT	OLBRACHT	kA	OLBRACHT
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Biblické	biblický	k2eAgInPc1d1	biblický
příběhy	příběh	k1gInPc1	příběh
:	:	kIx,	:
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ilustr	Ilustr	k1gInSc1	Ilustr
<g/>
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
Doré	Dorá	k1gFnSc2	Dorá
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9788000002101	[number]	k4	9788000002101
<g/>
.	.	kIx.	.
</s>
<s>
OCLC	OCLC	kA	OCLC
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
worldcat	worldcat	k5eAaPmF	worldcat
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
39572092	[number]	k4	39572092
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
OLBRACHT	OLBRACHT	kA	OLBRACHT
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc1	zem
bez	bez	k7c2	bez
jména	jméno	k1gNnSc2	jméno
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Otto	Otto	k1gMnSc1	Otto
Girgal	Girgal	k1gMnSc1	Girgal
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
</s>
</p>
<p>
<s>
OLBRACHT	OLBRACHT	kA	OLBRACHT
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rodinné	rodinný	k2eAgFnSc2d1	rodinná
korespondence	korespondence	k1gFnSc2	korespondence
Ivana	Ivan	k1gMnSc2	Ivan
Olbrachta	Olbracht	k1gMnSc2	Olbracht
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
krásné	krásný	k2eAgFnSc2d1	krásná
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
172	[number]	k4	172
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
444033	[number]	k4	444033
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ONDRA	Ondra	k1gMnSc1	Ondra
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
–	–	k?	–
Průřez	průřez	k1gInSc1	průřez
životem	život	k1gInSc7	život
a	a	k8xC	a
dílem	dílo	k1gNnSc7	dílo
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Bruntál	Bruntál	k1gInSc1	Bruntál
<g/>
:	:	kIx,	:
Okresní	okresní	k2eAgFnSc1d1	okresní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
–	–	k?	–
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
506	[number]	k4	506
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OPELÍK	OPELÍK	kA	OPELÍK
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
M	M	kA	M
<g/>
–	–	k?	–
<g/>
O.	O.	kA	O.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
728	[number]	k4	728
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
708	[number]	k4	708
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
665	[number]	k4	665
<g/>
–	–	k?	–
<g/>
671	[number]	k4	671
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PÍŠA	Píša	k1gMnSc1	Píša
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Matěj	Matěj	k1gMnSc1	Matěj
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Vítězslav	Vítězslava	k1gFnPc2	Vítězslava
Rzounek	Rzounka	k1gFnPc2	Rzounka
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
175	[number]	k4	175
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1503	[number]	k4	1503
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POHORSKÝ	pohorský	k2eAgMnSc1d1	pohorský
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
14	[number]	k4	14
s.	s.	k?	s.
</s>
</p>
<p>
<s>
STEJSKAL	Stejskal	k1gMnSc1	Stejskal
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
347	[number]	k4	347
s.	s.	k?	s.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
II	II	kA	II
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
K	k	k7c3	k
<g/>
–	–	k?	–
<g/>
P.	P.	kA	P.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
649	[number]	k4	649
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
487	[number]	k4	487
<g/>
–	–	k?	–
<g/>
488	[number]	k4	488
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	let	k1gInPc6	let
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
</s>
</p>
<p>
<s>
Socialismus	socialismus	k1gInSc1	socialismus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k2eAgMnSc1d1	Olbracht
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Olbracht	Olbracht	k1gMnSc1	Olbracht
v	v	k7c6	v
souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Olbrachtovo	Olbrachtův	k2eAgNnSc1d1	Olbrachtovo
zpracování	zpracování	k1gNnSc1	zpracování
Bidpajových	Bidpajový	k2eAgFnPc2d1	Bidpajový
bajek	bajka	k1gFnPc2	bajka
</s>
</p>
<p>
<s>
Biblické	biblický	k2eAgInPc1d1	biblický
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
I	i	k9	i
</s>
</p>
<p>
<s>
Biblické	biblický	k2eAgInPc1d1	biblický
příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
II	II	kA	II
</s>
</p>
<p>
<s>
K	k	k7c3	k
posudku	posudek	k1gInSc3	posudek
Olbrachtových	Olbrachtův	k2eAgInPc2d1	Olbrachtův
Biblických	biblický	k2eAgInPc2d1	biblický
příběhů	příběh	k1gInPc2	příběh
</s>
</p>
