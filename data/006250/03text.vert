<s>
Hechtova	Hechtův	k2eAgFnSc1d1	Hechtova
vila	vila	k1gFnSc1	vila
je	být	k5eAaImIp3nS	být
klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
rodinný	rodinný	k2eAgInSc4d1	rodinný
dům	dům	k1gInSc4	dům
pro	pro	k7c4	pro
významného	významný	k2eAgMnSc4d1	významný
textilního	textilní	k2eAgMnSc4d1	textilní
podnikatele	podnikatel	k1gMnSc4	podnikatel
Huga	Hugo	k1gMnSc4	Hugo
Hechta	Hecht	k1gMnSc4	Hecht
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Pisárky	Pisárka	k1gFnSc2	Pisárka
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
rakouského	rakouský	k2eAgMnSc2d1	rakouský
architekta	architekt	k1gMnSc2	architekt
Leopolda	Leopold	k1gMnSc2	Leopold
Bauera	Bauer	k1gMnSc2	Bauer
v	v	k7c6	v
letech	let	k1gInPc6	let
1909	[number]	k4	1909
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vile	vila	k1gFnSc6	vila
sídlí	sídlet	k5eAaImIp3nS	sídlet
Generální	generální	k2eAgInSc1d1	generální
konzulát	konzulát	k1gInSc1	konzulát
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
vily	vila	k1gFnSc2	vila
je	být	k5eAaImIp3nS	být
chráněno	chránit	k5eAaImNgNnS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vila	vila	k1gFnSc1	vila
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
velkých	velký	k2eAgInPc2d1	velký
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
budova	budova	k1gFnSc1	budova
včetně	včetně	k7c2	včetně
interiéru	interiér	k1gInSc2	interiér
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
rysy	rys	k1gInPc4	rys
klasicismu	klasicismus	k1gInSc2	klasicismus
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
půdorys	půdorys	k1gInSc1	půdorys
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
obdélníku	obdélník	k1gInSc2	obdélník
a	a	k8xC	a
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vybíhají	vybíhat	k5eAaImIp3nP	vybíhat
dva	dva	k4xCgInPc4	dva
široké	široký	k2eAgInPc4d1	široký
výklenky	výklenek	k1gInPc4	výklenek
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
schodiště	schodiště	k1gNnSc4	schodiště
na	na	k7c4	na
ochoz	ochoz	k1gInSc4	ochoz
a	a	k8xC	a
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
krb	krb	k1gInSc4	krb
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
budovy	budova	k1gFnSc2	budova
zdobí	zdobit	k5eAaImIp3nS	zdobit
stěny	stěna	k1gFnPc4	stěna
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
obklad	obklad	k1gInSc1	obklad
a	a	k8xC	a
okna	okno	k1gNnPc1	okno
jsou	být	k5eAaImIp3nP	být
vyzdobená	vyzdobený	k2eAgNnPc1d1	vyzdobené
vitrážemi	vitráž	k1gFnPc7	vitráž
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
vstupní	vstupní	k2eAgFnSc2d1	vstupní
haly	hala	k1gFnSc2	hala
vedou	vést	k5eAaImIp3nP	vést
dveře	dveře	k1gFnPc1	dveře
do	do	k7c2	do
pánského	pánský	k2eAgInSc2d1	pánský
pokoje	pokoj	k1gInSc2	pokoj
s	s	k7c7	s
verandou	veranda	k1gFnSc7	veranda
<g/>
,	,	kIx,	,
do	do	k7c2	do
obývacího	obývací	k2eAgInSc2d1	obývací
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
konferenčního	konferenční	k2eAgInSc2d1	konferenční
salonu	salon	k1gInSc2	salon
a	a	k8xC	a
jídelny	jídelna	k1gFnSc2	jídelna
<g/>
.	.	kIx.	.
</s>
<s>
Kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
ve	v	k7c6	v
vyšších	vysoký	k2eAgNnPc6d2	vyšší
podlažích	podlaží	k1gNnPc6	podlaží
najdete	najít	k5eAaPmIp2nP	najít
pouze	pouze	k6eAd1	pouze
pokoje	pokoj	k1gInPc4	pokoj
členů	člen	k1gInPc2	člen
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
hostů	host	k1gMnPc2	host
<g/>
.	.	kIx.	.
</s>
<s>
Vila	vila	k1gFnSc1	vila
připomíná	připomínat	k5eAaImIp3nS	připomínat
svým	svůj	k3xOyFgInSc7	svůj
interiérem	interiér	k1gInSc7	interiér
typický	typický	k2eAgInSc1d1	typický
anglický	anglický	k2eAgInSc1d1	anglický
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
už	už	k6eAd1	už
zmíněné	zmíněný	k2eAgNnSc4d1	zmíněné
dřevěné	dřevěný	k2eAgNnSc4d1	dřevěné
schodiště	schodiště	k1gNnSc4	schodiště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
alkovny	alkovna	k1gFnPc1	alkovna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
prostor	prostor	k1gInSc4	prostor
vstupní	vstupní	k2eAgFnSc2d1	vstupní
haly	hala	k1gFnSc2	hala
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
hlavních	hlavní	k2eAgFnPc2d1	hlavní
místností	místnost	k1gFnPc2	místnost
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
<g/>
.	.	kIx.	.
</s>
<s>
Exteriér	exteriér	k1gInSc1	exteriér
a	a	k8xC	a
vnější	vnější	k2eAgFnSc1d1	vnější
podoba	podoba	k1gFnSc1	podoba
vily	vila	k1gFnSc2	vila
vzhledu	vzhled	k1gInSc2	vzhled
"	"	kIx"	"
<g/>
anglického	anglický	k2eAgInSc2d1	anglický
domu	dům	k1gInSc2	dům
<g/>
"	"	kIx"	"
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
je	být	k5eAaImIp3nS	být
vila	vila	k1gFnSc1	vila
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
použitím	použití	k1gNnSc7	použití
sloupů	sloup	k1gInPc2	sloup
v	v	k7c6	v
průčelí	průčelí	k1gNnSc6	průčelí
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
podpírají	podpírat	k5eAaImIp3nP	podpírat
velký	velký	k2eAgInSc4d1	velký
balkon	balkon	k1gInSc4	balkon
<g/>
,	,	kIx,	,
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
obkladem	obklad	k1gInSc7	obklad
z	z	k7c2	z
vápencových	vápencový	k2eAgFnPc2d1	vápencová
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Stejného	stejný	k2eAgInSc2d1	stejný
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
i	i	k9	i
okázalá	okázalý	k2eAgFnSc1d1	okázalá
vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
Hlinky	hlinka	k1gFnSc2	hlinka
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
je	být	k5eAaImIp3nS	být
zařízen	zařídit	k5eAaPmNgInS	zařídit
neobiedermeierovým	obiedermeierův	k2eNgInSc7d1	obiedermeierův
nábytkem	nábytek	k1gInSc7	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
SEDLÁK	Sedlák	k1gMnSc1	Sedlák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Slavné	slavný	k2eAgFnSc2d1	slavná
brněnské	brněnský	k2eAgFnSc2d1	brněnská
vily	vila	k1gFnSc2	vila
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
:	:	kIx,	:
Foibos	Foibos	k1gMnSc1	Foibos
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
903661	[number]	k4	903661
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
