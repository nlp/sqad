<s>
Messier	Messier	k1gInSc1
60	#num#	k4
</s>
<s>
Messier	Messier	k1gInSc1
60	#num#	k4
M60	M60	k1gFnPc2
na	na	k7c6
snímku	snímek	k1gInSc6
z	z	k7c2
Hubbleova	Hubbleův	k2eAgInSc2d1
vesmírného	vesmírný	k2eAgInSc2d1
dalekohleduPozorovací	dalekohleduPozorovací	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000,0	2000,0	k4
<g/>
)	)	kIx)
Typ	typ	k1gInSc1
</s>
<s>
eliptická	eliptický	k2eAgFnSc1d1
galaxie	galaxie	k1gFnSc1
<g/>
,	,	kIx,
astronomický	astronomický	k2eAgInSc1d1
rádiový	rádiový	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
a	a	k8xC
astrophysical	astrophysicat	k5eAaPmAgMnS
X-ray	X-ra	k2eAgMnPc4d1
source	source	k1gMnPc4
Třída	třída	k1gFnSc1
</s>
<s>
E	E	kA
<g/>
2	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Objevitel	objevitel	k1gMnSc1
</s>
<s>
Johann	Johann	k1gMnSc1
Gottfried	Gottfried	k1gMnSc1
Koehler	Koehler	k1gMnSc1
Datum	datum	k1gInSc4
objevu	objev	k1gInSc2
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1779	#num#	k4
Rektascenze	rektascenze	k1gFnSc2
</s>
<s>
12	#num#	k4
<g/>
h	h	k?
43	#num#	k4
<g/>
m	m	kA
40,0	40,0	k4
<g/>
s	s	k7c7
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Deklinace	deklinace	k1gFnSc1
</s>
<s>
11	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Souhvězdí	souhvězdí	k1gNnSc2
</s>
<s>
Panna	Panna	k1gFnSc1
(	(	kIx(
<g/>
lat.	lat.	k?
Vir	vir	k1gInSc1
<g/>
)	)	kIx)
Zdánlivá	zdánlivý	k2eAgFnSc1d1
magnituda	magnituda	k1gFnSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
8,8	8,8	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
10,3	10,3	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
6,67	6,67	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
5,739	5,739	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Úhlová	úhlový	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
7,4	7,4	k4
<g/>
′	′	k?
<g/>
x	x	k?
<g/>
6,0	6,0	k4
<g/>
′	′	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
56	#num#	k4
700	#num#	k4
000	#num#	k4
ly	ly	k?
Plošná	plošný	k2eAgFnSc1d1
jasnost	jasnost	k1gFnSc1
</s>
<s>
13,1	13,1	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Poziční	poziční	k2eAgInSc1d1
úhel	úhel	k1gInSc1
</s>
<s>
105	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Rudý	rudý	k2eAgInSc1d1
posuv	posuv	k1gInSc1
</s>
<s>
0,003	0,003	k4
39	#num#	k4
Fyzikální	fyzikální	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
Poloměr	poloměr	k1gInSc1
</s>
<s>
60	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
ly	ly	k?
Absolutní	absolutní	k2eAgFnSc1d1
magnituda	magnituda	k1gFnSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
−	−	k?
Označení	označení	k1gNnSc1
v	v	k7c6
katalozích	katalog	k1gInPc6
Messierův	Messierův	k2eAgInSc4d1
katalog	katalog	k1gInSc4
</s>
<s>
M	M	kA
60	#num#	k4
New	New	k1gFnSc2
General	General	k1gFnSc1
Catalogue	Catalogue	k1gFnSc1
</s>
<s>
NGC	NGC	kA
4649	#num#	k4
Uppsala	Uppsala	k1gFnSc1
General	General	k1gFnSc1
Catalogue	Catalogue	k1gFnSc1
</s>
<s>
UGC	UGC	kA
7898	#num#	k4
Principal	Principal	k1gFnSc2
Galaxies	Galaxiesa	k1gFnPc2
Catalogue	Catalogu	k1gFnSc2
</s>
<s>
PGC	PGC	kA
42831	#num#	k4
Jiná	jiná	k1gFnSc1
označení	označení	k1gNnSc2
</s>
<s>
M	M	kA
<g/>
60	#num#	k4
<g/>
,	,	kIx,
NGC	NGC	kA
4649	#num#	k4
<g/>
,	,	kIx,
UGC	UGC	kA
7898	#num#	k4
<g/>
,	,	kIx,
PGC	PGC	kA
42831	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Arp	Arp	k1gFnSc1
116	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Komentář	komentář	k1gInSc1
</s>
<s>
objekt	objekt	k1gInSc1
Arp	Arp	k1gFnSc2
<g/>
116	#num#	k4
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
dvěma	dva	k4xCgFnPc7
galaxiemi	galaxie	k1gFnPc7
<g/>
:	:	kIx,
M60	M60	k1gFnSc7
a	a	k8xC
NGC	NGC	kA
4647	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
–	–	k?
měření	měření	k1gNnSc2
provedena	proveden	k2eAgFnSc1d1
ve	v	k7c6
viditelném	viditelný	k2eAgNnSc6d1
světle	světlo	k1gNnSc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Messier	Messier	k1gInSc1
60	#num#	k4
(	(	kIx(
<g/>
také	také	k9
M60	M60	k1gFnSc1
nebo	nebo	k8xC
NGC	NGC	kA
4649	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
eliptická	eliptický	k2eAgFnSc1d1
galaxie	galaxie	k1gFnSc1
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Panny	Panna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevil	objevit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
Johann	Johann	k1gMnSc1
Gottfried	Gottfried	k1gMnSc1
Koehler	Koehler	k1gMnSc1
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1779	#num#	k4
během	během	k7c2
sledování	sledování	k1gNnSc2
komety	kometa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Galaxie	galaxie	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
Země	zem	k1gFnSc2
vzdálená	vzdálený	k2eAgFnSc1d1
přibližně	přibližně	k6eAd1
56,7	56,7	k4
milionů	milion	k4xCgInPc2
ly	ly	k?
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
a	a	k8xC
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Kupy	kupa	k1gFnSc2
galaxií	galaxie	k1gFnPc2
v	v	k7c6
Panně	Panna	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
jejímž	jejíž	k3xOyRp3gInSc7
je	být	k5eAaImIp3nS
třetím	třetí	k4xOgInSc7
nejjasnějším	jasný	k2eAgInSc7d3
členem	člen	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spolu	spolu	k6eAd1
s	s	k7c7
blízkou	blízký	k2eAgFnSc7d1
galaxií	galaxie	k1gFnSc7
NGC	NGC	kA
4647	#num#	k4
ji	on	k3xPp3gFnSc4
Halton	Halton	k1gInSc1
Arp	Arp	k1gMnSc4
zapsal	zapsat	k5eAaPmAgInS
do	do	k7c2
svého	svůj	k3xOyFgInSc2
katalogu	katalog	k1gInSc2
zvláštních	zvláštní	k2eAgFnPc2d1
galaxií	galaxie	k1gFnPc2
(	(	kIx(
<g/>
Atlas	Atlas	k1gInSc1
of	of	k?
Peculiar	Peculiar	k1gInSc1
Galaxies	Galaxies	k1gInSc1
<g/>
)	)	kIx)
pod	pod	k7c7
číslem	číslo	k1gNnSc7
116	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozorování	pozorování	k1gNnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
M	M	kA
60	#num#	k4
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Panny	Panna	k1gFnSc2
</s>
<s>
M60	M60	k4
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
na	na	k7c6
obloze	obloha	k1gFnSc6
snadno	snadno	k6eAd1
vyhledat	vyhledat	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
leží	ležet	k5eAaImIp3nS
4,5	4,5	k4
<g/>
°	°	k?
západně	západně	k6eAd1
od	od	k7c2
hvězdy	hvězda	k1gFnSc2
3	#num#	k4
<g/>
.	.	kIx.
magnitudy	magnitudy	k6eAd1
s	s	k7c7
názvem	název	k1gInSc7
Vindemiatrix	Vindemiatrix	k1gInSc1
(	(	kIx(
<g/>
ε	ε	k?
Virginis	Virginis	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Galaxie	galaxie	k1gFnSc1
je	být	k5eAaImIp3nS
za	za	k7c2
příznivých	příznivý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
viditelná	viditelný	k2eAgFnSc1d1
i	i	k8xC
středně	středně	k6eAd1
velkým	velký	k2eAgInSc7d1
triedrem	triedr	k1gInSc7
<g/>
,	,	kIx,
například	například	k6eAd1
10	#num#	k4
<g/>
x	x	k?
<g/>
50	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
jako	jako	k9
velmi	velmi	k6eAd1
malá	malý	k2eAgFnSc1d1
neostrá	ostrý	k2eNgFnSc1d1
skvrnka	skvrnka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dalekohledu	dalekohled	k1gInSc6
o	o	k7c6
průměru	průměr	k1gInSc6
150	#num#	k4
mm	mm	kA
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
zahlédnout	zahlédnout	k5eAaPmF
některé	některý	k3yIgFnPc4
podrobnosti	podrobnost	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
světlé	světlý	k2eAgNnSc4d1
halo	halo	k1gNnSc4
rozměru	rozměr	k1gInSc2
3	#num#	k4
<g/>
′	′	k?
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
něhož	jenž	k3xRgMnSc2
je	být	k5eAaImIp3nS
zhuštěný	zhuštěný	k2eAgInSc1d1
střed	střed	k1gInSc1
galaxie	galaxie	k1gFnSc2
velmi	velmi	k6eAd1
hustý	hustý	k2eAgInSc1d1
a	a	k8xC
jasný	jasný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severozápadním	severozápadní	k2eAgInSc7d1
směrem	směr	k1gInSc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
spatřit	spatřit	k5eAaPmF
jakýsi	jakýsi	k3yIgInSc4
flíček	flíček	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
ve	v	k7c6
větším	veliký	k2eAgInSc6d2
dalekohledu	dalekohled	k1gInSc6
ukáže	ukázat	k5eAaPmIp3nS
jako	jako	k9
samostatná	samostatný	k2eAgFnSc1d1
galaxie	galaxie	k1gFnSc1
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
NGC	NGC	kA
4647	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatně	ostatně	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
části	část	k1gFnSc6
oblohy	obloha	k1gFnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
najít	najít	k5eAaPmF
mnoho	mnoho	k4c4
dalších	další	k2eAgFnPc2d1
galaxií	galaxie	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
30	#num#	k4
<g/>
′	′	k?
západně	západně	k6eAd1
Messier	Messier	k1gInSc1
59	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galaxii	galaxie	k1gFnSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
snadno	snadno	k6eAd1
pozorovat	pozorovat	k5eAaImF
z	z	k7c2
obou	dva	k4xCgFnPc2
zemských	zemský	k2eAgFnPc2d1
polokoulí	polokoule	k1gFnPc2
a	a	k8xC
ze	z	k7c2
všech	všecek	k3xTgFnPc2
obydlených	obydlený	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
má	mít	k5eAaImIp3nS
pouze	pouze	k6eAd1
mírnou	mírný	k2eAgFnSc4d1
severní	severní	k2eAgFnSc4d1
deklinaci	deklinace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
je	být	k5eAaImIp3nS
na	na	k7c6
severní	severní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
lépe	dobře	k6eAd2
pozorovatelná	pozorovatelný	k2eAgFnSc1d1
a	a	k8xC
během	během	k7c2
jarních	jarní	k2eAgFnPc2d1
nocí	noc	k1gFnPc2
tam	tam	k6eAd1
vychází	vycházet	k5eAaImIp3nS
vysoko	vysoko	k6eAd1
na	na	k7c4
oblohu	obloha	k1gFnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
v	v	k7c6
oblastech	oblast	k1gFnPc6
více	hodně	k6eAd2
vzdálených	vzdálený	k2eAgMnPc2d1
od	od	k7c2
rovníku	rovník	k1gInSc2
zůstává	zůstávat	k5eAaImIp3nS
poněkud	poněkud	k6eAd1
níže	nízce	k6eAd2
nad	nad	k7c7
obzorem	obzor	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Nejvhodnější	vhodný	k2eAgNnPc1d3
období	období	k1gNnPc1
pro	pro	k7c4
její	její	k3xOp3gNnPc4
pozorování	pozorování	k1gNnPc4
na	na	k7c6
večerní	večerní	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
je	být	k5eAaImIp3nS
od	od	k7c2
března	březen	k1gInSc2
do	do	k7c2
července	červenec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
pozorování	pozorování	k1gNnPc2
</s>
<s>
Spolu	spolu	k6eAd1
se	s	k7c7
sousední	sousední	k2eAgFnSc7d1
galaxií	galaxie	k1gFnSc7
Messier	Messier	k1gMnSc1
59	#num#	k4
tuto	tento	k3xDgFnSc4
galaxii	galaxie	k1gFnSc4
objevil	objevit	k5eAaPmAgMnS
Johann	Johann	k1gMnSc1
Gottfried	Gottfried	k1gMnSc1
Koehler	Koehler	k1gMnSc1
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1779	#num#	k4
během	během	k7c2
sledování	sledování	k1gNnSc2
komety	kometa	k1gFnSc2
a	a	k8xC
o	o	k7c4
několik	několik	k4yIc4
dní	den	k1gInPc2
později	pozdě	k6eAd2
obě	dva	k4xCgNnPc4
nezávisle	závisle	k6eNd1
spoluobjevil	spoluobjevit	k5eAaPmAgMnS
Charles	Charles	k1gMnSc1
Messier	Messier	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zároveň	zároveň	k6eAd1
objevil	objevit	k5eAaPmAgMnS
i	i	k9
blízkou	blízký	k2eAgFnSc4d1
galaxii	galaxie	k1gFnSc4
Messier	Messier	k1gInSc1
58	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
tři	tři	k4xCgFnPc1
pak	pak	k6eAd1
Messier	Messier	k1gMnSc1
přidal	přidat	k5eAaPmAgMnS
do	do	k7c2
svého	svůj	k3xOyFgInSc2
katalogu	katalog	k1gInSc2
a	a	k8xC
k	k	k7c3
M60	M60	k1gFnSc3
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
trochu	trochu	k6eAd1
jasnější	jasný	k2eAgFnSc1d2
a	a	k8xC
výraznější	výrazný	k2eAgFnSc1d2
než	než	k8xS
M58	M58	k1gFnSc1
a	a	k8xC
M	M	kA
<g/>
59	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
John	John	k1gMnSc1
Herschel	Herschel	k1gMnSc1
tuto	tento	k3xDgFnSc4
galaxii	galaxie	k1gFnSc4
pozoroval	pozorovat	k5eAaImAgMnS
mnohokrát	mnohokrát	k6eAd1
kvůli	kvůli	k7c3
přítomnosti	přítomnost	k1gFnSc3
blízké	blízký	k2eAgFnSc2d1
galaxie	galaxie	k1gFnSc2
NGC	NGC	kA
4647	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
je	být	k5eAaImIp3nS
popsal	popsat	k5eAaPmAgMnS
jako	jako	k9
dvojitou	dvojitý	k2eAgFnSc4d1
mlhovinu	mlhovina	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
západní	západní	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
slabší	slabý	k2eAgFnSc1d2
než	než	k8xS
východní	východní	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
být	být	k5eAaImF
hlavní	hlavní	k2eAgFnSc7d1
částí	část	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
Heinrich	Heinrich	k1gMnSc1
Louis	Louis	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Arrest	Arrest	k1gMnSc1
ji	on	k3xPp3gFnSc4
pozoroval	pozorovat	k5eAaImAgMnS
opakovaně	opakovaně	k6eAd1
a	a	k8xC
jejím	její	k3xOp3gInSc7
tvarem	tvar	k1gInSc7
byl	být	k5eAaImAgInS
ohromen	ohromit	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
M60	M60	k4
je	být	k5eAaImIp3nS
třetím	třetí	k4xOgInSc7
nejjasnějším	jasný	k2eAgInSc7d3
členem	člen	k1gInSc7
Kupy	kupa	k1gFnSc2
galaxií	galaxie	k1gFnPc2
v	v	k7c6
Panně	Panna	k1gFnSc6
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
největším	veliký	k2eAgMnSc7d3
členem	člen	k1gMnSc7
její	její	k3xOp3gFnSc2
podskupiny	podskupina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
čtyři	čtyři	k4xCgInPc4
členy	člen	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hmotnost	hmotnost	k1gFnSc1
M60	M60	k1gFnSc1
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
tisíc	tisíc	k4xCgInSc4
miliard	miliarda	k4xCgFnPc2
hmotností	hmotnost	k1gFnPc2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
tedy	tedy	k9
několikanásobně	několikanásobně	k6eAd1
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
hmotnost	hmotnost	k1gFnSc1
Mléčné	mléčný	k2eAgFnSc2d1
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
rozměr	rozměr	k1gInSc1
je	být	k5eAaImIp3nS
asi	asi	k9
120	#num#	k4
000	#num#	k4
ly	ly	k?
<g/>
,	,	kIx,
ale	ale	k8xC
její	její	k3xOp3gNnSc1
vnější	vnější	k2eAgNnSc1d1
halo	halo	k1gNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ještě	ještě	k6eAd1
větší	veliký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
absolutní	absolutní	k2eAgFnSc4d1
hvězdnou	hvězdný	k2eAgFnSc4d1
velikost	velikost	k1gFnSc4
-22,3	-22,3	k4
a	a	k8xC
rozsáhlý	rozsáhlý	k2eAgInSc4d1
systém	systém	k1gInSc4
kulových	kulový	k2eAgFnPc2d1
hvězdokup	hvězdokupa	k1gFnPc2
<g/>
,	,	kIx,
kterých	který	k3yRgFnPc2,k3yQgFnPc2,k3yIgFnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
kolem	kolem	k7c2
5	#num#	k4
100	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Obří	obří	k2eAgFnSc1d1
černá	černý	k2eAgFnSc1d1
díra	díra	k1gFnSc1
v	v	k7c6
jejím	její	k3xOp3gNnSc6
jádru	jádro	k1gNnSc6
má	mít	k5eAaImIp3nS
hmotnost	hmotnost	k1gFnSc1
4,5	4,5	k4
miliardy	miliarda	k4xCgFnSc2
hmotností	hmotnost	k1gFnPc2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
těsné	těsný	k2eAgFnSc2d1
blízkosti	blízkost	k1gFnSc2
M60	M60	k1gFnSc2
a	a	k8xC
spirální	spirální	k2eAgFnSc2d1
galaxie	galaxie	k1gFnSc2
NGC	NGC	kA
4647	#num#	k4
by	by	kYmCp3nS
se	se	k3xPyFc4
dalo	dát	k5eAaPmAgNnS
usoudit	usoudit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
spolu	spolu	k6eAd1
splývají	splývat	k5eAaImIp3nP
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
ovšem	ovšem	k9
fotografické	fotografický	k2eAgInPc1d1
snímky	snímek	k1gInPc1
těchto	tento	k3xDgFnPc2
galaxií	galaxie	k1gFnPc2
neukazují	ukazovat	k5eNaImIp3nP
žádné	žádný	k3yNgInPc4
důkazy	důkaz	k1gInPc4
jejich	jejich	k3xOp3gNnSc2
vzájemného	vzájemný	k2eAgNnSc2d1
ovlivňování	ovlivňování	k1gNnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
bývá	bývat	k5eAaImIp3nS
u	u	k7c2
galaxií	galaxie	k1gFnPc2
obvyklé	obvyklý	k2eAgNnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
skutečně	skutečně	k6eAd1
nacházejí	nacházet	k5eAaImIp3nP
tak	tak	k6eAd1
blízko	blízko	k6eAd1
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
usoudit	usoudit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tyto	tento	k3xDgFnPc1
dvě	dva	k4xCgFnPc1
galaxie	galaxie	k1gFnPc1
překrývají	překrývat	k5eAaImIp3nP
pouze	pouze	k6eAd1
z	z	k7c2
při	při	k7c6
pohledu	pohled	k1gInSc6
ze	z	k7c2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
nacházet	nacházet	k5eAaImF
ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
vzájemné	vzájemný	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
může	moct	k5eAaImIp3nS
zdát	zdát	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
ledna	leden	k1gInSc2
2004	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
této	tento	k3xDgFnSc6
galaxii	galaxie	k1gFnSc6
objevena	objeven	k2eAgFnSc1d1
supernova	supernova	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
dostala	dostat	k5eAaPmAgFnS
označení	označení	k1gNnSc6
SN	SN	kA
2004	#num#	k4
<g/>
W.	W.	kA
Měla	mít	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
magnitudu	magnitud	k1gInSc2
18,8	18,8	k4
a	a	k8xC
byla	být	k5eAaImAgFnS
označena	označit	k5eAaPmNgFnS
za	za	k7c4
málo	málo	k6eAd1
svítící	svítící	k2eAgFnSc4d1
supernovu	supernova	k1gFnSc4
typu	typ	k1gInSc2
Ia	ia	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
vybuchla	vybuchnout	k5eAaPmAgFnS
o	o	k7c4
půl	půl	k1xP
roku	rok	k1gInSc2
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
ale	ale	k8xC
nebyla	být	k5eNaImAgFnS
zpozorována	zpozorovat	k5eAaPmNgFnS
<g/>
,	,	kIx,
protože	protože	k8xS
M60	M60	k1gFnSc1
byla	být	k5eAaImAgFnS
blízko	blízko	k7c2
konjunkce	konjunkce	k1gFnSc2
se	s	k7c7
Sluncem	slunce	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
M60	M60	k1gFnSc2
(	(	kIx(
<g/>
astronomia	astronomia	k1gFnSc1
<g/>
)	)	kIx)
na	na	k7c6
italské	italský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
NASA	NASA	kA
<g/>
/	/	kIx~
<g/>
IPAC	IPAC	kA
Extragalactic	Extragalactice	k1gFnPc2
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Results	Resultsa	k1gFnPc2
for	forum	k1gNnPc2
M	M	kA
60	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Messier	Messier	k1gMnSc1
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Margaret	Margareta	k1gFnPc2
Geller	Geller	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Updated	Updated	k1gMnSc1
Zwicky	Zwicka	k1gFnSc2
Catalog	Catalog	k1gMnSc1
(	(	kIx(
<g/>
UZC	UZC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Publications	Publications	k1gInSc1
of	of	k?
the	the	k?
Astronomical	Astronomical	k1gMnSc1
Society	societa	k1gFnSc2
of	of	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
316343.1	316343.1	k4
2	#num#	k4
3	#num#	k4
Thomas	Thomas	k1gMnSc1
Jarrett	Jarrett	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Two	Two	k1gMnSc1
Micron	Micron	k1gMnSc1
All	All	k1gMnSc1
Sky	Sky	k1gMnSc1
Survey	Survea	k1gFnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
MASS	MASS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Astronomical	Astronomical	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
498708.1	498708.1	k4
2	#num#	k4
FROMMERT	FROMMERT	kA
<g/>
,	,	kIx,
Hartmut	Hartmut	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
SEDS	SEDS	kA
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
:	:	kIx,
Revised	Revised	k1gMnSc1
NGC	NGC	kA
Data	datum	k1gNnSc2
for	forum	k1gNnPc2
NGC	NGC	kA
4649	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
SEDS	SEDS	kA
Messier	Messier	k1gInSc1
Objects	Objects	k1gInSc1
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Messier	Messier	k1gInSc1
60	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ARP	ARP	kA
<g/>
,	,	kIx,
Halton	Halton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gMnSc1
of	of	k?
Peculiar	Peculiar	k1gMnSc1
Galaxies	Galaxies	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astrophysical	Astrophysical	k1gMnSc1
Journal	Journal	k1gMnSc1
Supplement	Supplement	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listopad	listopad	k1gInSc1
1966	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
14	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
190147	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1966	#num#	k4
<g/>
ApJS	ApJS	k1gFnPc2
<g/>
..	..	k?
<g/>
.14	.14	k4
<g/>
...	...	k?
<g/>
.1	.1	k4
<g/>
A.	A.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TULLY	TULLY	kA
<g/>
,	,	kIx,
R.	R.	kA
Brent	Brent	k?
<g/>
;	;	kIx,
COURTOIS	COURTOIS	kA
<g/>
,	,	kIx,
Hélè	Hélè	k1gInSc5
M.	M.	kA
<g/>
;	;	kIx,
SORCE	SORCE	kA
<g/>
,	,	kIx,
Jenny	Jenen	k2eAgFnPc4d1
G.	G.	kA
Cosmicflows-	Cosmicflows-	k1gFnSc7
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronomical	Astronomical	k1gFnSc1
Journal	Journal	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srpen	srpen	k1gInSc1
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
152	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.384	10.384	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
6256	#num#	k4
<g/>
/	/	kIx~
<g/>
152	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
2016	#num#	k4
<g/>
AJ	aj	kA
<g/>
...	...	k?
<g/>
.152	.152	k4
<g/>
..	..	k?
<g/>
.50	.50	k4
<g/>
T.	T.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
VILLARD	VILLARD	kA
<g/>
,	,	kIx,
Ray	Ray	k1gMnSc1
<g/>
;	;	kIx,
USHER	USHER	kA
<g/>
,	,	kIx,
Oli	Oli	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odd	odd	kA
galaxy	galax	k1gInPc4
couple	couple	k6eAd1
on	on	k3xPp3gMnSc1
space	space	k1gMnSc1
voyage	voyagat	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NASA	NASA	kA
<g/>
,	,	kIx,
2012-09-06	2012-09-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
MANZINI	MANZINI	kA
<g/>
,	,	kIx,
Federico	Federico	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Il	Il	k1gMnSc1
Catalogo	Catalogo	k1gMnSc1
di	di	k?
Messier	Messier	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nuovo	Nuovo	k1gNnSc1
Orione	orion	k1gInSc5
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Deklinace	deklinace	k1gFnSc1
11	#num#	k4
<g/>
°	°	k?
severním	severní	k2eAgInSc7d1
směrem	směr	k1gInSc7
odpovídá	odpovídat	k5eAaImIp3nS
úhlové	úhlový	k2eAgFnPc4d1
vzdálenosti	vzdálenost	k1gFnPc4
79	#num#	k4
<g/>
°	°	k?
od	od	k7c2
severního	severní	k2eAgInSc2d1
nebeského	nebeský	k2eAgInSc2d1
pólu	pól	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severně	severně	k6eAd1
od	od	k7c2
79	#num#	k4
<g/>
°	°	k?
severní	severní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
je	být	k5eAaImIp3nS
tedy	tedy	k9
tato	tento	k3xDgFnSc1
galaxie	galaxie	k1gFnSc1
cirkumpolární	cirkumpolární	k2eAgFnSc1d1
(	(	kIx(
<g/>
nikdy	nikdy	k6eAd1
nezapadá	zapadat	k5eNaPmIp3nS,k5eNaImIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jižně	jižně	k6eAd1
od	od	k7c2
79	#num#	k4
<g/>
°	°	k?
jižní	jižní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
objekt	objekt	k1gInSc1
vůbec	vůbec	k9
nevychází	vycházet	k5eNaImIp3nS
nad	nad	k7c4
obzor	obzor	k1gInSc4
<g/>
.1	.1	k4
2	#num#	k4
SANDAGE	SANDAGE	kA
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
BEDKE	BEDKE	kA
<g/>
,	,	kIx,
J.	J.	kA
Carnegie	Carnegie	k1gFnSc1
Atlas	Atlas	k1gInSc1
of	of	k?
Galaxies	Galaxies	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
<g/>
:	:	kIx,
Carnegie	Carnegie	k1gFnSc1
Institution	Institution	k1gInSc1
of	of	k?
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
87279	#num#	k4
<g/>
-	-	kIx~
<g/>
667	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Knihy	kniha	k1gFnPc1
</s>
<s>
O	O	kA
<g/>
'	'	kIx"
<g/>
MEARA	MEARA	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgMnSc1d1
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deep	Deep	k1gInSc1
Sky	Sky	k1gFnSc2
Companions	Companionsa	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Messier	Messier	k1gMnSc1
Objects	Objects	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
55332	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Mapy	mapa	k1gFnPc1
hvězdné	hvězdný	k2eAgFnSc2d1
oblohy	obloha	k1gFnSc2
</s>
<s>
Toshimi	Toshi	k1gFnPc7
Taki	Taki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taki	Taki	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
8.5	8.5	k4
Magnitude	Magnitud	k1gInSc5
Star	Star	kA
Atlas	Atlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
-	-	kIx~
Atlas	Atlas	k1gInSc1
hvězdné	hvězdný	k2eAgFnSc2d1
oblohy	obloha	k1gFnSc2
volně	volně	k6eAd1
stažitelný	stažitelný	k2eAgMnSc1d1
ve	v	k7c6
formátu	formát	k1gInSc6
PDF	PDF	kA
<g/>
.	.	kIx.
</s>
<s>
TIRION	TIRION	kA
<g/>
;	;	kIx,
RAPPAPORT	RAPPAPORT	kA
<g/>
;	;	kIx,
LOVI	LOVI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uranometria	Uranometrium	k1gNnSc2
2000.0	2000.0	k4
-	-	kIx~
Volume	volum	k1gInSc5
I	i	k9
-	-	kIx~
The	The	k1gMnSc1
Northern	Northern	k1gMnSc1
Hemisphere	Hemispher	k1gInSc5
to	ten	k3xDgNnSc1
-6	-6	k4
<g/>
°	°	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richmond	Richmonda	k1gFnPc2
<g/>
,	,	kIx,
Virginia	Virginium	k1gNnSc2
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
Willmann-Bell	Willmann-Bell	k1gMnSc1
<g/>
,	,	kIx,
inc	inc	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
943396	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
TIRION	TIRION	kA
<g/>
;	;	kIx,
SINNOTT	SINNOTT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sky	Sky	k1gMnSc1
Atlas	Atlas	k1gMnSc1
2000.0	2000.0	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
933346	#num#	k4
<g/>
-	-	kIx~
<g/>
90	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TIRION	TIRION	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Cambridge	Cambridge	k1gFnSc1
Star	star	k1gFnSc1
Atlas	Atlas	k1gInSc1
2000.0	2000.0	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
521	#num#	k4
<g/>
-	-	kIx~
<g/>
80084	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Messier	Messier	k1gInSc1
60	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
NASA	NASA	kA
<g/>
/	/	kIx~
<g/>
IPAC	IPAC	kA
Extragalactic	Extragalactice	k1gFnPc2
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Results	Resultsa	k1gFnPc2
for	forum	k1gNnPc2
M	M	kA
60	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SIMBAD	SIMBAD	kA
Astronomical	Astronomical	k1gFnSc1
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Results	Resultsa	k1gFnPc2
for	forum	k1gNnPc2
M	M	kA
60	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SEDS	SEDS	kA
Messier	Messier	k1gInSc1
Objects	Objects	k1gInSc1
Database	Databasa	k1gFnSc3
<g/>
:	:	kIx,
Messier	Messier	k1gInSc1
60	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KODRIŠ	KODRIŠ	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průvodce	průvodka	k1gFnSc6
hvězdnou	hvězdný	k2eAgFnSc7d1
oblohou	obloha	k1gFnSc7
<g/>
:	:	kIx,
Panna	Panna	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
NASA	NASA	kA
-	-	kIx~
APOD.	apod.	kA
Astronomický	astronomický	k2eAgInSc1d1
snímek	snímek	k1gInSc1
dne	den	k1gInSc2
-	-	kIx~
Eliptická	eliptický	k2eAgFnSc1d1
galaxie	galaxie	k1gFnSc1
M60	M60	k1gFnSc1
a	a	k8xC
spirální	spirální	k2eAgFnSc1d1
NGC	NGC	kA
4647	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
astro	astra	k1gFnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016-01-28	2016-01-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gMnSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
url	url	k?
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
//	//	k?
<g/>
upload	upload	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikimedia	wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
org	org	k?
<g/>
/	/	kIx~
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
commons	commons	k6eAd1
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
/	/	kIx~
<g/>
Skymap	Skymap	k1gInSc1
<g/>
.	.	kIx.
<g/>
png	png	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
right	right	k2eAgInSc1d1
no-repeat	no-repeat	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
vertical-align	vertical-align	k1gNnSc1
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
{	{	kIx(
<g/>
font-family	font-famit	k5eAaPmAgFnP
<g/>
:	:	kIx,
<g/>
monospace	monospace	k1gFnSc1
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
85	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Objekty	objekt	k1gInPc1
v	v	k7c6
Messierově	Messierův	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
</s>
<s>
M	M	kA
1	#num#	k4
</s>
<s>
M	M	kA
2	#num#	k4
</s>
<s>
M	M	kA
3	#num#	k4
</s>
<s>
M	M	kA
4	#num#	k4
</s>
<s>
M	M	kA
5	#num#	k4
</s>
<s>
M	M	kA
6	#num#	k4
</s>
<s>
M	M	kA
7	#num#	k4
</s>
<s>
M	M	kA
8	#num#	k4
</s>
<s>
M	M	kA
9	#num#	k4
</s>
<s>
M	M	kA
10	#num#	k4
</s>
<s>
M	M	kA
11	#num#	k4
</s>
<s>
M	M	kA
12	#num#	k4
</s>
<s>
M	M	kA
13	#num#	k4
</s>
<s>
M	M	kA
14	#num#	k4
</s>
<s>
M	M	kA
15	#num#	k4
</s>
<s>
M	M	kA
16	#num#	k4
</s>
<s>
M	M	kA
17	#num#	k4
</s>
<s>
M	M	kA
18	#num#	k4
</s>
<s>
M	M	kA
19	#num#	k4
</s>
<s>
M	M	kA
20	#num#	k4
</s>
<s>
M	M	kA
21	#num#	k4
</s>
<s>
M	M	kA
22	#num#	k4
</s>
<s>
M	M	kA
23	#num#	k4
</s>
<s>
M	M	kA
24	#num#	k4
</s>
<s>
M	M	kA
25	#num#	k4
</s>
<s>
M	M	kA
26	#num#	k4
</s>
<s>
M	M	kA
27	#num#	k4
</s>
<s>
M	M	kA
28	#num#	k4
</s>
<s>
M	M	kA
29	#num#	k4
</s>
<s>
M	M	kA
30	#num#	k4
</s>
<s>
M	M	kA
31	#num#	k4
</s>
<s>
M	M	kA
32	#num#	k4
</s>
<s>
M	M	kA
33	#num#	k4
</s>
<s>
M	M	kA
34	#num#	k4
</s>
<s>
M	M	kA
35	#num#	k4
</s>
<s>
M	M	kA
36	#num#	k4
</s>
<s>
M	M	kA
37	#num#	k4
</s>
<s>
M	M	kA
38	#num#	k4
</s>
<s>
M	M	kA
39	#num#	k4
</s>
<s>
M	M	kA
40	#num#	k4
</s>
<s>
M	M	kA
41	#num#	k4
</s>
<s>
M	M	kA
42	#num#	k4
</s>
<s>
M	M	kA
43	#num#	k4
</s>
<s>
M	M	kA
44	#num#	k4
</s>
<s>
M	M	kA
45	#num#	k4
</s>
<s>
M	M	kA
46	#num#	k4
</s>
<s>
M	M	kA
47	#num#	k4
</s>
<s>
M	M	kA
48	#num#	k4
</s>
<s>
M	M	kA
49	#num#	k4
</s>
<s>
M	M	kA
50	#num#	k4
</s>
<s>
M	M	kA
51	#num#	k4
</s>
<s>
M	M	kA
52	#num#	k4
</s>
<s>
M	M	kA
53	#num#	k4
</s>
<s>
M	M	kA
54	#num#	k4
</s>
<s>
M	M	kA
55	#num#	k4
</s>
<s>
M	M	kA
56	#num#	k4
</s>
<s>
M	M	kA
57	#num#	k4
</s>
<s>
M	M	kA
58	#num#	k4
</s>
<s>
M	M	kA
59	#num#	k4
</s>
<s>
M	M	kA
60	#num#	k4
</s>
<s>
M	M	kA
61	#num#	k4
</s>
<s>
M	M	kA
62	#num#	k4
</s>
<s>
M	M	kA
63	#num#	k4
</s>
<s>
M	M	kA
64	#num#	k4
</s>
<s>
M	M	kA
65	#num#	k4
</s>
<s>
M	M	kA
66	#num#	k4
</s>
<s>
M	M	kA
67	#num#	k4
</s>
<s>
M	M	kA
68	#num#	k4
</s>
<s>
M	M	kA
69	#num#	k4
</s>
<s>
M	M	kA
70	#num#	k4
</s>
<s>
M	M	kA
71	#num#	k4
</s>
<s>
M	M	kA
72	#num#	k4
</s>
<s>
M	M	kA
73	#num#	k4
</s>
<s>
M	M	kA
74	#num#	k4
</s>
<s>
M	M	kA
75	#num#	k4
</s>
<s>
M	M	kA
76	#num#	k4
</s>
<s>
M	M	kA
77	#num#	k4
</s>
<s>
M	M	kA
78	#num#	k4
</s>
<s>
M	M	kA
79	#num#	k4
</s>
<s>
M	M	kA
80	#num#	k4
</s>
<s>
M	M	kA
81	#num#	k4
</s>
<s>
M	M	kA
82	#num#	k4
</s>
<s>
M	M	kA
83	#num#	k4
</s>
<s>
M	M	kA
84	#num#	k4
</s>
<s>
M	M	kA
85	#num#	k4
</s>
<s>
M	M	kA
86	#num#	k4
</s>
<s>
M	M	kA
87	#num#	k4
</s>
<s>
M	M	kA
88	#num#	k4
</s>
<s>
M	M	kA
89	#num#	k4
</s>
<s>
M	M	kA
90	#num#	k4
</s>
<s>
M	M	kA
91	#num#	k4
</s>
<s>
M	M	kA
92	#num#	k4
</s>
<s>
M	M	kA
93	#num#	k4
</s>
<s>
M	M	kA
94	#num#	k4
</s>
<s>
M	M	kA
95	#num#	k4
</s>
<s>
M	M	kA
96	#num#	k4
</s>
<s>
M	M	kA
97	#num#	k4
</s>
<s>
M	M	kA
98	#num#	k4
</s>
<s>
M	M	kA
99	#num#	k4
</s>
<s>
M	M	kA
100	#num#	k4
</s>
<s>
M	M	kA
101	#num#	k4
</s>
<s>
M	M	kA
102	#num#	k4
</s>
<s>
M	M	kA
103	#num#	k4
</s>
<s>
M	M	kA
104	#num#	k4
</s>
<s>
M	M	kA
105	#num#	k4
</s>
<s>
M	M	kA
106	#num#	k4
</s>
<s>
M	M	kA
107	#num#	k4
</s>
<s>
M	M	kA
108	#num#	k4
</s>
<s>
M	M	kA
109	#num#	k4
</s>
<s>
M	M	kA
110	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
