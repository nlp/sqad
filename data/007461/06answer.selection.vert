<s>
Vinnetou	Vinneta	k1gFnSc7	Vinneta
je	být	k5eAaImIp3nS	být
smyšlená	smyšlený	k2eAgFnSc1d1	smyšlená
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
předlohou	předloha	k1gFnSc7	předloha
však	však	k9	však
byl	být	k5eAaImAgMnS	být
skutečný	skutečný	k2eAgMnSc1d1	skutečný
apačský	apačský	k2eAgMnSc1d1	apačský
náčelník	náčelník	k1gMnSc1	náčelník
Cochise	Cochise	k1gFnSc2	Cochise
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
letech	let	k1gInPc6	let
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
