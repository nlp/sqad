<s>
Židovská	židovský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Falafel	Falafel	k1gMnSc1
<g/>
,	,	kIx,
Hummus	Hummus	k1gMnSc1
a	a	k8xC
izraelský	izraelský	k2eAgMnSc1d1
salátZákladní	salátZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Typ	typ	k1gInSc1
</s>
<s>
náboženská	náboženský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
Ingredience	ingredience	k1gFnSc2
Suroviny	surovina	k1gFnSc2
</s>
<s>
hovězí	hovězí	k2eAgNnSc1d1
maso	maso	k1gNnSc1
<g/>
,	,	kIx,
skopové	skopový	k2eAgNnSc1d1
maso	maso	k1gNnSc1
<g/>
,	,	kIx,
kuřecí	kuřecí	k2eAgNnSc1d1
maso	maso	k1gNnSc1
<g/>
,	,	kIx,
ryby	ryba	k1gFnPc1
<g/>
,	,	kIx,
luštěniny	luštěnina	k1gFnPc1
<g/>
,	,	kIx,
rýže	rýže	k1gFnSc1
<g/>
,	,	kIx,
nudle	nudle	k1gFnSc1
<g/>
,	,	kIx,
kořenová	kořenový	k2eAgFnSc1d1
zelenina	zelenina	k1gFnSc1
Dochucovadla	Dochucovadlo	k1gNnSc2
</s>
<s>
sezam	sezam	k1gInSc1
<g/>
,	,	kIx,
skořice	skořice	k1gFnSc1
<g/>
,	,	kIx,
máta	máta	k1gFnSc1
<g/>
,	,	kIx,
koriandr	koriandr	k1gInSc1
<g/>
,	,	kIx,
růžový	růžový	k2eAgInSc1d1
olej	olej	k1gInSc1
<g/>
,	,	kIx,
med	med	k1gInSc1
Pokrmy	pokrm	k1gInPc7
Jídla	jídlo	k1gNnSc2
</s>
<s>
Falafel	Falafel	k1gMnSc1
<g/>
,	,	kIx,
Hummus	Hummus	k1gMnSc1
<g/>
,	,	kIx,
Pita	pit	k2eAgFnSc1d1
placka	placka	k1gFnSc1
<g/>
,	,	kIx,
Kuskus	kuskus	k1gInSc1
<g/>
,	,	kIx,
Kebab	kebab	k1gInSc1
<g/>
,	,	kIx,
Šašlik	šašlik	k1gInSc1
<g/>
,	,	kIx,
Švarma	Švarma	k1gNnSc1
Nápoje	nápoj	k1gInSc2
</s>
<s>
káva	káva	k1gFnSc1
<g/>
,	,	kIx,
čaj	čaj	k1gInSc1
<g/>
,	,	kIx,
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
pivo	pivo	k1gNnSc1
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
unikátní	unikátní	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
přitom	přitom	k6eAd1
velmi	velmi	k6eAd1
rozmanitá	rozmanitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
množství	množství	k1gNnSc1
pokrmů	pokrm	k1gInPc2
ovlivněných	ovlivněný	k2eAgFnPc2d1
či	či	k8xC
přejatých	přejatý	k2eAgFnPc2d1
z	z	k7c2
místních	místní	k2eAgFnPc2d1
kuchyní	kuchyně	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
obývá	obývat	k5eAaImIp3nS
židovské	židovský	k2eAgNnSc4d1
etnikum	etnikum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc1d3
komunita	komunita	k1gFnSc1
Židů	Žid	k1gMnPc2
žije	žít	k5eAaImIp3nS
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
židovská	židovský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
téměř	téměř	k6eAd1
identická	identický	k2eAgFnSc1d1
s	s	k7c7
kuchyní	kuchyně	k1gFnSc7
izraelskou	izraelský	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
je	být	k5eAaImIp3nS
neobvyklá	obvyklý	k2eNgFnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
do	do	k7c2
ní	on	k3xPp3gFnSc2
zasahuje	zasahovat	k5eAaImIp3nS
sama	sám	k3xTgNnPc4
filosofie	filosofie	k1gFnSc1
židovského	židovský	k2eAgNnSc2d1
stravování	stravování	k1gNnSc2
a	a	k8xC
prvky	prvek	k1gInPc1
náboženských	náboženský	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
jsou	být	k5eAaImIp3nP
konkrétně	konkrétně	k6eAd1
obsaženy	obsáhnout	k5eAaPmNgInP
především	především	k9
v	v	k7c6
Tóře	tóra	k1gFnSc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
nazývány	nazýván	k2eAgInPc4d1
kašrut	kašrut	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohdy	mnohdy	k6eAd1
tato	tento	k3xDgNnPc1
pravidla	pravidlo	k1gNnPc1
nezasahují	zasahovat	k5eNaImIp3nP
jen	jen	k9
do	do	k7c2
jídla	jídlo	k1gNnSc2
samotného	samotný	k2eAgNnSc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
přípravy	příprava	k1gFnSc2
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
i	i	k9
do	do	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
podávání	podávání	k1gNnSc2
(	(	kIx(
<g/>
kupříkladu	kupříkladu	k6eAd1
času	čas	k1gInSc2
podání	podání	k1gNnPc2
či	či	k8xC
používání	používání	k1gNnSc2
různého	různý	k2eAgInSc2d1
typu	typ	k1gInSc2
nádobí	nádobí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
třeba	třeba	k6eAd1
do	do	k7c2
práce	práce	k1gFnSc2
řezníků	řezník	k1gMnPc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
židovská	židovský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
má	mít	k5eAaImIp3nS
i	i	k9
svá	svůj	k3xOyFgNnPc4
vlastní	vlastní	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
pro	pro	k7c4
usmrcení	usmrcení	k1gNnSc4
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
židovské	židovský	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
též	též	k9
pochází	pocházet	k5eAaImIp3nS
slovo	slovo	k1gNnSc4
košer	košer	k6eAd1
znamenající	znamenající	k2eAgInSc1d1
v	v	k7c6
původním	původní	k2eAgInSc6d1
významu	význam	k1gInSc6
čistý	čistý	k2eAgInSc4d1
<g/>
/	/	kIx~
<g/>
povolený	povolený	k2eAgInSc4d1
a	a	k8xC
odkazující	odkazující	k2eAgInSc4d1
tedy	tedy	k9
na	na	k7c4
rozlišení	rozlišení	k1gNnSc4
potravin	potravina	k1gFnPc2
podle	podle	k7c2
rituální	rituální	k2eAgFnSc2d1
čistoty	čistota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
nemá	mít	k5eNaImIp3nS
všeobecně	všeobecně	k6eAd1
uznávané	uznávaný	k2eAgNnSc4d1
národní	národní	k2eAgNnSc4d1
jídlo	jídlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
židovská	židovský	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
je	být	k5eAaImIp3nS
rozptýlena	rozptýlit	k5eAaPmNgFnS
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Izraeli	Izrael	k1gInSc6
se	se	k3xPyFc4
za	za	k7c4
tradiční	tradiční	k2eAgNnSc4d1
jídlo	jídlo	k1gNnSc4
považují	považovat	k5eAaImIp3nP
smažené	smažený	k2eAgInPc1d1
a	a	k8xC
kořeně	kořenit	k5eAaImSgInS
kuličky	kulička	k1gFnPc4
z	z	k7c2
mleté	mletý	k2eAgFnSc2d1
cizrny	cizrna	k1gFnSc2
-	-	kIx~
falafel	falafel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodávají	prodávat	k5eAaImIp3nP
se	se	k3xPyFc4
celá	celý	k2eAgNnPc1d1
desetiletí	desetiletí	k1gNnPc1
pouličními	pouliční	k2eAgMnPc7d1
prodejci	prodejce	k1gMnPc7
po	po	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populární	populární	k2eAgInSc1d1
je	být	k5eAaImIp3nS
také	také	k9
mezze	mezze	k1gFnSc1
v	v	k7c4
humus-in-pita	humus-in-pit	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověstné	pověstný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
také	také	k9
izraelské	izraelský	k2eAgFnPc4d1
snídaně	snídaně	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
„	„	k?
<g/>
příspěvek	příspěvek	k1gInSc4
židovského	židovský	k2eAgInSc2d1
státu	stát	k1gInSc2
ke	k	k7c3
světové	světový	k2eAgFnSc3d1
kuchyni	kuchyně	k1gFnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Náboženská	náboženský	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
</s>
<s>
Ze	z	k7c2
zvěře	zvěř	k1gFnSc2
je	být	k5eAaImIp3nS
absolutně	absolutně	k6eAd1
zakázáno	zakázat	k5eAaPmNgNnS
konzumovat	konzumovat	k5eAaBmF
vepřové	vepřové	k1gNnSc4
a	a	k8xC
koňské	koňský	k2eAgNnSc4d1
maso	maso	k1gNnSc4
<g/>
,	,	kIx,
úhoře	úhoř	k1gMnSc2
a	a	k8xC
jesetera	jeseter	k1gMnSc2
(	(	kIx(
<g/>
druh	druh	k1gInSc1
ryby	ryba	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
krev	krev	k1gFnSc4
jakéhokoli	jakýkoli	k3yIgNnSc2
zvířete	zvíře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Velmi	velmi	k6eAd1
přísně	přísně	k6eAd1
se	se	k3xPyFc4
také	také	k9
dodržuje	dodržovat	k5eAaImIp3nS
pravidlo	pravidlo	k1gNnSc4
oddělování	oddělování	k1gNnSc2
jídel	jídlo	k1gNnPc2
masitých	masitý	k2eAgNnPc2d1
od	od	k7c2
jídel	jídlo	k1gNnPc2
obsahujících	obsahující	k2eAgFnPc2d1
mléko	mléko	k1gNnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Neuvaříš	uvařit	k5eNaPmIp2nS
kůzle	kůzle	k1gNnSc4
v	v	k7c6
mléce	mléko	k1gNnSc6
jeho	jeho	k3xOp3gFnSc2
matky	matka	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zpracování	zpracování	k1gNnSc6
navzájem	navzájem	k6eAd1
dle	dle	k7c2
pravidel	pravidlo	k1gNnPc2
neslučitelných	slučitelný	k2eNgFnPc2d1
potravin	potravina	k1gFnPc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
různé	různý	k2eAgFnPc1d1
sady	sada	k1gFnPc1
nádobí	nádobí	k1gNnSc2
<g/>
,	,	kIx,
skleničky	sklenička	k1gFnPc4
<g/>
,	,	kIx,
příbory	příbor	k1gInPc4
ale	ale	k8xC
i	i	k9
třeba	třeba	k6eAd1
různé	různý	k2eAgFnPc1d1
utěrky	utěrka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Zachovávají	zachovávat	k5eAaImIp3nP
se	se	k3xPyFc4
rovněž	rovněž	k9
určité	určitý	k2eAgInPc1d1
stanovené	stanovený	k2eAgInPc1d1
odstupy	odstup	k1gInPc1
při	při	k7c6
konzumaci	konzumace	k1gFnSc6
takto	takto	k6eAd1
různých	různý	k2eAgInPc2d1
typů	typ	k1gInPc2
jídel	jídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
po	po	k7c6
konzumaci	konzumace	k1gFnSc6
byť	byť	k8xS
jen	jen	k9
minimálního	minimální	k2eAgNnSc2d1
množství	množství	k1gNnSc2
masa	maso	k1gNnSc2
je	být	k5eAaImIp3nS
po	po	k7c4
dobu	doba	k1gFnSc4
6	#num#	k4
hodin	hodina	k1gFnPc2
zapovězeno	zapovědět	k5eAaPmNgNnS
pít	pít	k5eAaImF
mléka	mléko	k1gNnSc2
a	a	k8xC
jíst	jíst	k5eAaImF
cokoli	cokoli	k3yInSc4
mléčného	mléčný	k2eAgNnSc2d1
či	či	k8xC
mléko	mléko	k1gNnSc4
obsahujícího	obsahující	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
mléčném	mléčný	k2eAgNnSc6d1
jídle	jídlo	k1gNnSc6
naopak	naopak	k6eAd1
není	být	k5eNaImIp3nS
povoleno	povolit	k5eAaPmNgNnS
celou	celá	k1gFnSc4
hodinu	hodina	k1gFnSc4
konzumovat	konzumovat	k5eAaBmF
hovězí	hovězí	k2eAgNnSc4d1
maso	maso	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Zvířata	zvíře	k1gNnPc1
musí	muset	k5eAaImIp3nP
řezník	řezník	k1gMnSc1
usmrcovat	usmrcovat	k5eAaImF
vždy	vždy	k6eAd1
jedním	jeden	k4xCgInSc7
řezem	řez	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
přetne	přetnout	k5eAaPmIp3nS
krční	krční	k2eAgFnSc4d1
tepnu	tepna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
Maca	Maca	k1gFnSc1
-	-	kIx~
nekvašený	kvašený	k2eNgInSc1d1
chléb	chléb	k1gInSc1
podávaný	podávaný	k2eAgMnSc1d1
ve	v	k7c6
dnech	den	k1gInPc6
svátku	svátek	k1gInSc2
Pesach	pesach	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Nejčastější	častý	k2eAgNnPc1d3
jídla	jídlo	k1gNnPc1
</s>
<s>
Polévky	polévka	k1gFnPc1
</s>
<s>
Kuřecí	kuřecí	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
je	být	k5eAaImIp3nS
základem	základ	k1gInSc7
židovské	židovský	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasická	klasický	k2eAgFnSc1d1
kuřecí	kuřecí	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
jako	jako	k9
vývar	vývar	k1gInSc1
z	z	k7c2
kuřecího	kuřecí	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
cibule	cibule	k1gFnSc2
<g/>
,	,	kIx,
mrkve	mrkev	k1gFnSc2
<g/>
,	,	kIx,
celere	celer	k1gInSc5
<g/>
,	,	kIx,
kopru	kopr	k1gInSc2
a	a	k8xC
petržele	petržel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Složitější	složitý	k2eAgFnSc1d2
kuřecí	kuřecí	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
je	být	k5eAaImIp3nS
sephardim	sephardim	k6eAd1
s	s	k7c7
orzo	orzo	k6eAd1
nebo	nebo	k8xC
rýží	rýže	k1gFnSc7
<g/>
,	,	kIx,
také	také	k9
polévka	polévka	k1gFnSc1
ashkenazim	ashkenazim	k6eAd1
s	s	k7c7
nudlemi	nudle	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obzvláště	obzvláště	k6eAd1
populární	populární	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
polévka	polévka	k1gFnSc1
kubba	kubba	k1gFnSc1
připravená	připravený	k2eAgFnSc1d1
z	z	k7c2
bulguru	bulgur	k1gInSc2
nebo	nebo	k8xC
čočková	čočkový	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
s	s	k7c7
masem	maso	k1gNnSc7
a	a	k8xC
koriandrem	koriandr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polévka	polévka	k1gFnSc1
harira	harira	k1gFnSc1
je	být	k5eAaImIp3nS
kořeněná	kořeněný	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
z	z	k7c2
jehněčího	jehněčí	k2eAgNnSc2d1
nebo	nebo	k8xC
kuřecího	kuřecí	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
se	se	k3xPyFc4
často	často	k6eAd1
podává	podávat	k5eAaImIp3nS
polévka	polévka	k1gFnSc1
z	z	k7c2
bílých	bílý	k2eAgFnPc2d1
fazolí	fazole	k1gFnPc2
v	v	k7c6
rajčatové	rajčatový	k2eAgFnSc6d1
omáčce	omáčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ryby	Ryby	k1gFnPc1
</s>
<s>
Tilapie	Tilapie	k1gFnSc1
<g/>
,	,	kIx,
Tilapia	Tilapia	k1gFnSc1
Zilli	Zille	k1gFnSc4
-	-	kIx~
ryba	ryba	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
</s>
<s>
Izrael	Izrael	k1gInSc1
je	být	k5eAaImIp3nS
přímořský	přímořský	k2eAgInSc1d1
stát	stát	k1gInSc1
a	a	k8xC
čerstvé	čerstvý	k2eAgFnPc1d1
ryby	ryba	k1gFnPc1
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
snadno	snadno	k6eAd1
dostupné	dostupný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravují	připravovat	k5eAaImIp3nP
se	se	k3xPyFc4
celé	celý	k2eAgInPc1d1
<g/>
,	,	kIx,
grilované	grilovaný	k2eAgInPc1d1
nebo	nebo	k8xC
smažené	smažený	k2eAgInPc1d1
často	často	k6eAd1
pouze	pouze	k6eAd1
s	s	k7c7
čerstvě	čerstvě	k6eAd1
vymačkanou	vymačkaný	k2eAgFnSc7d1
citronovou	citronový	k2eAgFnSc7d1
šťávou	šťáva	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbený	oblíbený	k2eAgInSc1d1
je	on	k3xPp3gInPc4
pstruh	pstruh	k1gMnSc1
(	(	kIx(
<g/>
forel	forel	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
gilthead	gilthead	k6eAd1
seabream	seabream	k6eAd1
(	(	kIx(
<g/>
denisse	denisse	k6eAd1
)	)	kIx)
a	a	k8xC
mnoho	mnoho	k4c4
dalších	další	k2eAgFnPc2d1
ryb	ryba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ryby	Ryby	k1gFnPc1
se	se	k3xPyFc4
často	často	k6eAd1
dusí	dusit	k5eAaImIp3nS
v	v	k7c6
omáčce	omáčka	k1gFnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
pokrm	pokrm	k1gInSc1
hraime	hraimat	k5eAaPmIp3nS
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
ryba	ryba	k1gFnSc1
v	v	k7c6
omáčce	omáčka	k1gFnSc6
s	s	k7c7
feferonkou	feferonka	k1gFnSc7
a	a	k8xC
jiným	jiný	k2eAgNnSc7d1
kořením	koření	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ryba	ryba	k1gFnSc1
kufta	kufto	k1gNnSc2
se	se	k3xPyFc4
vaří	vařit	k5eAaImIp3nS
v	v	k7c6
rajčatové	rajčatový	k2eAgFnSc6d1
nebo	nebo	k8xC
jogurtové	jogurtový	k2eAgFnSc6d1
omáčce	omáčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tilapie	Tilapie	k1gFnSc1
je	být	k5eAaImIp3nS
ryba	ryba	k1gFnSc1
zapečená	zapečený	k2eAgFnSc1d1
s	s	k7c7
omáčkou	omáčka	k1gFnSc7
tahini	tahin	k2eAgMnPc1d1
a	a	k8xC
přelitá	přelitý	k2eAgNnPc4d1
olivovým	olivový	k2eAgInSc7d1
olejem	olej	k1gInSc7
<g/>
,	,	kIx,
koriandrem	koriandr	k1gInSc7
<g/>
,	,	kIx,
mátou	máta	k1gFnSc7
<g/>
,	,	kIx,
bazalkou	bazalka	k1gFnSc7
a	a	k8xC
piniovými	piniový	k2eAgInPc7d1
oříšky	oříšek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pevné	pevný	k2eAgFnPc1d1
bílé	bílý	k2eAgFnPc1d1
ryby	ryba	k1gFnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
kapr	kapr	k1gMnSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
rozemlety	rozemlít	k5eAaPmNgInP
a	a	k8xC
tvarovány	tvarovat	k5eAaImNgInP
do	do	k7c2
bochníků	bochník	k1gInPc2
nebo	nebo	k8xC
kuliček	kulička	k1gFnPc2
a	a	k8xC
vařené	vařený	k2eAgInPc4d1
v	v	k7c6
rybím	rybí	k2eAgInSc6d1
vývaru	vývar	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Maso	maso	k1gNnSc1
</s>
<s>
Krůtí	krůtí	k2eAgInSc4d1
řízek	řízek	k1gInSc4
s	s	k7c7
těstovinami	těstovina	k1gFnPc7
</s>
<s>
Kuřecí	kuřecí	k2eAgNnSc1d1
a	a	k8xC
krůtí	krůtí	k2eAgNnSc1d1
maso	maso	k1gNnSc1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
na	na	k7c4
mnoho	mnoho	k4c4
způsobů	způsob	k1gInPc2
<g/>
,	,	kIx,
od	od	k7c2
jednoduchého	jednoduchý	k2eAgNnSc2d1
pečeného	pečený	k2eAgNnSc2d1
kuřecího	kuřecí	k2eAgNnSc2d1
masa	maso	k1gNnSc2
až	až	k9
po	po	k7c4
komplikované	komplikovaný	k2eAgInPc4d1
pokrmy	pokrm	k1gInPc4
s	s	k7c7
omáčkami	omáčka	k1gFnPc7
-	-	kIx~
datlový	datlový	k2eAgInSc1d1
sirup	sirup	k1gInSc1
nebo	nebo	k8xC
rajčatová	rajčatový	k2eAgFnSc1d1
omáčka	omáčka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známé	známá	k1gFnPc1
jsou	být	k5eAaImIp3nP
kuřecí	kuřecí	k2eAgFnPc1d1
kuličky	kulička	k1gFnPc1
z	z	k7c2
mletého	mletý	k2eAgNnSc2d1
masa	maso	k1gNnSc2
(	(	kIx(
<g/>
albondigas	albondigas	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
rajčatové	rajčatový	k2eAgFnSc6d1
omáčce	omáčka	k1gFnSc6
nebo	nebo	k8xC
kufta	kufta	k1gFnSc1
z	z	k7c2
mletého	mletý	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
bylin	bylina	k1gFnPc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
,	,	kIx,
vařená	vařený	k2eAgFnSc1d1
s	s	k7c7
rajčatovou	rajčatový	k2eAgFnSc7d1
omáčkou	omáčka	k1gFnSc7
<g/>
,	,	kIx,
datlovým	datlový	k2eAgInSc7d1
sirupem	sirup	k1gInSc7
<g/>
,	,	kIx,
sirupem	sirup	k1gInSc7
z	z	k7c2
granátového	granátový	k2eAgNnSc2d1
jablka	jablko	k1gNnSc2
nebo	nebo	k8xC
tamarindovým	tamarindový	k2eAgInSc7d1
sirupem	sirup	k1gInSc7
se	s	k7c7
zeleninou	zelenina	k1gFnSc7
nebo	nebo	k8xC
fazolemi	fazole	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obalovaný	obalovaný	k2eAgInSc4d1
krůtí	krůtí	k2eAgInSc4d1
řízek	řízek	k1gInSc4
podávaný	podávaný	k2eAgInSc4d1
s	s	k7c7
těstovinami	těstovina	k1gFnPc7
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
oblíbeným	oblíbený	k2eAgNnSc7d1
jídlem	jídlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židé	Žid	k1gMnPc1
maso	maso	k1gNnSc1
rádi	rád	k2eAgMnPc1d1
grilují	grilovat	k5eAaImIp3nP
doma	doma	k6eAd1
i	i	k9
venku	venku	k6eAd1
<g/>
,	,	kIx,
nazývá	nazývat	k5eAaImIp3nS
se	se	k3xPyFc4
pak	pak	k6eAd1
mangal	mangal	k1gInSc1
nebo	nebo	k8xC
al	ala	k1gFnPc2
ha-esh	ha-esha	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
ohni	oheň	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravují	připravovat	k5eAaImIp3nP
tak	tak	k6eAd1
kebab	kebab	k1gInSc4
<g/>
,	,	kIx,
taouk	taouk	k1gInSc4
<g/>
,	,	kIx,
merguez	merguez	k1gInSc4
a	a	k8xC
šašlik	šašlik	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Rýže	rýže	k1gFnPc1
<g/>
,	,	kIx,
luštěniny	luštěnina	k1gFnPc1
a	a	k8xC
těstoviny	těstovina	k1gFnPc1
</s>
<s>
Kuskus	kuskus	k1gMnSc1
</s>
<s>
Rýži	rýže	k1gFnSc4
konzumují	konzumovat	k5eAaBmIp3nP
Židé	Žid	k1gMnPc1
od	od	k7c2
jednoduché	jednoduchý	k2eAgFnSc2d1
dušené	dušený	k2eAgFnSc2d1
bílé	bílý	k2eAgFnSc2d1
až	až	k9
po	po	k7c6
slavnostní	slavnostní	k2eAgFnSc6d1
-	-	kIx~
složitě	složitě	k6eAd1
připravenou	připravený	k2eAgFnSc4d1
s	s	k7c7
mnoha	mnoho	k4c7
ingrediencemi	ingredience	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rýže	rýže	k1gFnSc1
se	se	k3xPyFc4
často	často	k6eAd1
vaří	vařit	k5eAaImIp3nS
s	s	k7c7
kořením	koření	k1gNnSc7
a	a	k8xC
podává	podávat	k5eAaImIp3nS
s	s	k7c7
mandlemi	mandle	k1gFnPc7
a	a	k8xC
piniovými	piniový	k2eAgInPc7d1
oříšky	oříšek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbená	oblíbený	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
zelená	zelený	k2eAgFnSc1d1
rýže	rýže	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
různé	různý	k2eAgFnPc4d1
čerstvě	čerstvě	k6eAd1
nasekané	nasekaný	k2eAgFnPc1d1
bylinky	bylinka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rýžové	rýžový	k2eAgFnPc4d1
mísy	mísa	k1gFnPc4
často	často	k6eAd1
obsahují	obsahovat	k5eAaImIp3nP
i	i	k9
tenké	tenký	k2eAgFnPc4d1
smažené	smažený	k2eAgFnPc4d1
nudle	nudle	k1gFnPc4
nebo	nebo	k8xC
čočku	čočka	k1gFnSc4
(	(	kIx(
<g/>
mujadara	mujadara	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orez	Orez	k1gInSc1
shu	shu	k?
<g/>
'	'	kIx"
<g/>
it	it	k?
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
z	z	k7c2
bílých	bílý	k2eAgFnPc2d1
fazolí	fazole	k1gFnPc2
vařených	vařený	k2eAgFnPc2d1
s	s	k7c7
rajčaty	rajče	k1gNnPc7
a	a	k8xC
podává	podávat	k5eAaImIp3nS
se	se	k3xPyFc4
s	s	k7c7
rýži	rýže	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kuskus	kuskus	k1gInSc1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
z	z	k7c2
krupice	krupice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
pasíruje	pasírovat	k5eAaImIp3nS
přes	přes	k7c4
síto	síto	k1gNnSc4
a	a	k8xC
vaří	vařit	k5eAaImIp3nS
v	v	k7c6
aromatickém	aromatický	k2eAgInSc6d1
nálevu	nálev	k1gInSc6
ve	v	k7c6
speciální	speciální	k2eAgFnSc6d1
parní	parní	k2eAgFnSc6d1
nádobě	nádoba	k1gFnSc6
zvané	zvaný	k2eAgFnSc2d1
couscoussiè	couscoussiè	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dušeném	dušený	k2eAgInSc6d1
kuskusu	kuskus	k1gInSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
podává	podávat	k5eAaImIp3nS
kuřecí	kuřecí	k2eAgNnSc1d1
nebo	nebo	k8xC
jehněčí	jehněčí	k2eAgNnSc1d1
maso	maso	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kuskus	kuskus	k1gMnSc1
se	se	k3xPyFc4
také	také	k9
míchá	míchat	k5eAaImIp3nS
se	s	k7c7
zeleninou	zelenina	k1gFnSc7
vařenou	vařený	k2eAgFnSc7d1
v	v	k7c6
polévce	polévka	k1gFnSc6
ochucené	ochucený	k2eAgFnSc2d1
šafránem	šafrán	k1gInSc7
nebo	nebo	k8xC
kurkumou	kurkuma	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Těstoviny	těstovina	k1gFnPc1
Putim	Putim	k1gFnSc1
<g/>
,	,	kIx,
nazývané	nazývaný	k2eAgFnSc2d1
izraelský	izraelský	k2eAgInSc4d1
kuskus	kuskus	k1gInSc4
<g/>
,	,	kIx,
vznikly	vzniknout	k5eAaPmAgInP
jako	jako	k8xC,k8xS
náhrada	náhrada	k1gFnSc1
rýže	rýže	k1gFnSc2
a	a	k8xC
byly	být	k5eAaImAgFnP
připravovány	připravovat	k5eAaImNgFnP
z	z	k7c2
pšenice	pšenice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
měly	mít	k5eAaImAgFnP
původně	původně	k6eAd1
tvar	tvar	k1gInSc4
rýžových	rýžový	k2eAgNnPc2d1
zrn	zrno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
mají	mít	k5eAaImIp3nP
tvar	tvar	k1gInSc4
perel	perla	k1gFnPc2
<g/>
,	,	kIx,
smyček	smyčka	k1gFnPc2
<g/>
,	,	kIx,
hvězd	hvězda	k1gFnPc2
nebo	nebo	k8xC
srdcí	srdce	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
ostatní	ostatní	k2eAgFnPc1d1
těstoviny	těstovina	k1gFnPc1
jsou	být	k5eAaImIp3nP
dochuceny	dochutit	k5eAaPmNgInP
kořením	koření	k1gNnSc7
<g/>
,	,	kIx,
bylinkami	bylinka	k1gFnPc7
a	a	k8xC
omáčkami	omáčka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
oblíbenou	oblíbený	k2eAgFnSc7d1
těstovinou	těstovina	k1gFnSc7
je	být	k5eAaImIp3nS
bulgur	bulgur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
druh	druh	k1gInSc1
sušené	sušený	k2eAgFnSc2d1
popraskané	popraskaný	k2eAgFnSc2d1
pšenice	pšenice	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
stejně	stejně	k6eAd1
jako	jako	k9
rýže	rýže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Omáčky	omáčka	k1gFnPc1
</s>
<s>
Izraelci	Izraelec	k1gMnPc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
oblibě	obliba	k1gFnSc6
nejrůznější	různý	k2eAgFnSc2d3
pikantní	pikantní	k2eAgFnSc2d1
omáčky	omáčka	k1gFnSc2
a	a	k8xC
koření	koření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podávají	podávat	k5eAaImIp3nP
se	se	k3xPyFc4
především	především	k9
ke	k	k7c3
grilovanému	grilovaný	k2eAgNnSc3d1
masu	maso	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
k	k	k7c3
mnoha	mnoho	k4c3
dalším	další	k2eAgInPc3d1
pokrmům	pokrm	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známá	známá	k1gFnSc1
je	být	k5eAaImIp3nS
červená	červený	k2eAgFnSc1d1
a	a	k8xC
velmi	velmi	k6eAd1
ostrá	ostrý	k2eAgFnSc1d1
omáčka	omáčka	k1gFnSc1
skhug	skhug	k1gMnSc1
malawach	malawach	k1gMnSc1
chilli	chilli	k1gNnSc4
z	z	k7c2
chilli	chilli	k1gNnSc2
papriček	paprička	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Omáčky	omáčka	k1gFnSc2
harissa	harissa	k1gFnSc1
a	a	k8xC
filfel	filfel	k1gInSc4
chuma	chumum	k1gNnSc2
jsou	být	k5eAaImIp3nP
vyrobené	vyrobený	k2eAgFnPc1d1
z	z	k7c2
česneku	česnek	k1gInSc2
a	a	k8xC
chilli	chilli	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amba	ambo	k1gNnSc2
je	být	k5eAaImIp3nS
nakládaná	nakládaný	k2eAgFnSc1d1
mangová	mangový	k2eAgFnSc1d1
omáčka	omáčka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Nápoje	nápoj	k1gInPc1
</s>
<s>
Káva	káva	k1gFnSc1
</s>
<s>
Mezi	mezi	k7c7
židovskou	židovský	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
je	být	k5eAaImIp3nS
pití	pití	k1gNnSc1
kávy	káva	k1gFnSc2
kulturní	kulturní	k2eAgFnSc7d1
záležitostí	záležitost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Káva	káva	k1gFnSc1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
jako	jako	k9
instantní	instantní	k2eAgNnSc1d1
(	(	kIx(
<g/>
nes	nést	k5eAaImRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ledová	ledový	k2eAgFnSc1d1
<g/>
,	,	kIx,
latte	latte	k5eAaPmIp2nP
(	(	kIx(
<g/>
hafuḥ	hafuḥ	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
italské	italský	k2eAgFnSc2d1
espresso	espressa	k1gFnSc5
nebo	nebo	k8xC
turecká	turecký	k2eAgFnSc1d1
káva	káva	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
ochucuje	ochucovat	k5eAaImIp3nS
kardamomem	kardamom	k1gInSc7
(	(	kIx(
<g/>
hel	hel	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Čaj	čaj	k1gInSc1
</s>
<s>
Čaj	čaj	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
velmi	velmi	k6eAd1
oblíbený	oblíbený	k2eAgInSc4d1
a	a	k8xC
konzumovaný	konzumovaný	k2eAgInSc4d1
doma	doma	k6eAd1
i	i	k9
v	v	k7c6
kavárnách	kavárna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židé	Žid	k1gMnPc1
jej	on	k3xPp3gMnSc4
připravují	připravovat	k5eAaImIp3nP
mnoha	mnoho	k4c7
způsoby	způsob	k1gInPc7
<g/>
,	,	kIx,
od	od	k7c2
obyčejného	obyčejný	k2eAgInSc2d1
černého	černý	k2eAgInSc2d1
čaje	čaj	k1gInSc2
v	v	k7c6
ruském	ruský	k2eAgInSc6d1
a	a	k8xC
tureckém	turecký	k2eAgInSc6d1
stylu	styl	k1gInSc6
s	s	k7c7
cukrem	cukr	k1gInSc7
<g/>
,	,	kIx,
po	po	k7c4
čaj	čaj	k1gInSc4
s	s	k7c7
citronem	citron	k1gInSc7
nebo	nebo	k8xC
mlékem	mléko	k1gNnSc7
<g/>
,	,	kIx,
také	také	k9
s	s	k7c7
mátou	máta	k1gFnSc7
(	(	kIx(
<g/>
nana	nana	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Limonáda	limonáda	k1gFnSc1
</s>
<s>
Limonána	Limonán	k2eAgFnSc1d1
<g/>
,	,	kIx,
vyrobená	vyrobený	k2eAgFnSc1d1
z	z	k7c2
čerstvě	čerstvě	k6eAd1
vymačkaných	vymačkaný	k2eAgInPc2d1
citronů	citron	k1gInPc2
a	a	k8xC
máty	máta	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
vynalezena	vynaleznout	k5eAaPmNgFnS
v	v	k7c6
Izraeli	Izrael	k1gInSc6
počátkem	počátkem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rimonana	Rimonana	k1gFnSc1
je	být	k5eAaImIp3nS
vyrobená	vyrobený	k2eAgFnSc1d1
z	z	k7c2
džusu	džus	k1gInSc2
z	z	k7c2
granátového	granátový	k2eAgNnSc2d1
jablka	jablko	k1gNnSc2
a	a	k8xC
máty	máta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Víno	víno	k1gNnSc1
</s>
<s>
Izraelci	Izraelec	k1gMnPc1
víno	víno	k1gNnSc4
pěstují	pěstovat	k5eAaImIp3nP
a	a	k8xC
vypijí	vypít	k5eAaPmIp3nP
asi	asi	k9
6,5	6,5	k4
litru	litr	k1gInSc2
vína	víno	k1gNnSc2
na	na	k7c4
osobu	osoba	k1gFnSc4
a	a	k8xC
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
málo	málo	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
jej	on	k3xPp3gInSc4
pijí	pít	k5eAaImIp3nP
především	především	k9
k	k	k7c3
jídlu	jídlo	k1gNnSc3
a	a	k8xC
s	s	k7c7
mírou	míra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známé	známý	k2eAgFnPc4d1
jsou	být	k5eAaImIp3nP
značky	značka	k1gFnPc1
Carmel	Carmela	k1gFnPc2
<g/>
,	,	kIx,
Tishbi	Tishbi	k1gNnPc2
a	a	k8xC
Golan	Golana	k1gFnPc2
Heights	Heightsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
vína	víno	k1gNnSc2
vyrobeného	vyrobený	k2eAgNnSc2d1
od	od	k7c2
roku	rok	k1gInSc2
1880	#num#	k4
bylo	být	k5eAaImAgNnS
sladké	sladký	k2eAgNnSc1d1
košer	košer	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jsou	být	k5eAaImIp3nP
však	však	k9
oblíbená	oblíbený	k2eAgNnPc4d1
i	i	k8xC
vína	víno	k1gNnPc4
suchá	suchý	k2eAgNnPc4d1
a	a	k8xC
polosuchá	polosuchý	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
pěstovaná	pěstovaný	k2eAgNnPc4d1
košer	košer	k2eAgNnPc4d1
vína	víno	k1gNnPc4
jsou	být	k5eAaImIp3nP
především	především	k9
Cabernet	Cabernet	k1gMnSc1
Sauvignon	Sauvignon	k1gMnSc1
<g/>
,	,	kIx,
Merlot	Merlot	k1gMnSc1
<g/>
,	,	kIx,
Sauvignon	Sauvignon	k1gMnSc1
blanc	blanc	k1gFnSc1
<g/>
,	,	kIx,
Chardonnay	Chardonnaa	k1gFnPc1
<g/>
,	,	kIx,
Pinot	Pinot	k1gMnSc1
noir	noir	k1gMnSc1
<g/>
,	,	kIx,
Ryzlink	ryzlink	k1gInSc1
rýnský	rýnský	k2eAgInSc1d1
a	a	k8xC
Gewürztraminer	Gewürztraminer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Izraeli	Izrael	k1gInSc6
jsou	být	k5eAaImIp3nP
stovky	stovka	k1gFnPc1
malých	malý	k2eAgNnPc2d1
vinařství	vinařství	k1gNnPc2
až	až	k9
po	po	k7c4
velké	velký	k2eAgFnPc4d1
vinařské	vinařský	k2eAgFnPc4d1
společnosti	společnost	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
vyrábějí	vyrábět	k5eAaImIp3nP
deset	deset	k4xCc4
milionů	milion	k4xCgInPc2
lahví	lahev	k1gFnPc2
ročně	ročně	k6eAd1
a	a	k8xC
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
je	být	k5eAaImIp3nS
i	i	k8xC
vyvážejí	vyvážet	k5eAaImIp3nP
do	do	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pivo	pivo	k1gNnSc1
</s>
<s>
Černé	Černé	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
(	(	kIx(
<g/>
Bira	Bir	k2eAgFnSc1d1
Shḥ	Shḥ	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nealkoholický	alkoholický	k2eNgInSc4d1
nápoj	nápoj	k1gInSc4
vyráběný	vyráběný	k2eAgInSc4d1
v	v	k7c6
Izraeli	Izrael	k1gInSc6
už	už	k6eAd1
několik	několik	k4yIc4
století	století	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známá	známá	k1gFnSc1
jsou	být	k5eAaImIp3nP
i	i	k9
piva	pivo	k1gNnPc4
Goldstar	Goldstara	k1gFnPc2
a	a	k8xC
Maccabi	Maccab	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nové	nový	k2eAgFnPc4d1
značky	značka	k1gFnPc4
patří	patřit	k5eAaImIp3nS
Dancing	dancing	k1gInSc1
Camel	Camela	k1gFnPc2
<g/>
,	,	kIx,
Negev	Negva	k1gFnPc2
a	a	k8xC
Can	Can	k1gFnPc2
<g/>
'	'	kIx"
<g/>
an	an	k?
<g/>
.	.	kIx.
</s>
<s>
Tvrdý	tvrdý	k2eAgInSc1d1
alkohol	alkohol	k1gInSc1
</s>
<s>
Arak	arak	k1gInSc1
je	být	k5eAaImIp3nS
alkoholický	alkoholický	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
s	s	k7c7
anýzem	anýz	k1gInSc7
(	(	kIx(
<g/>
40	#num#	k4
<g/>
-	-	kIx~
<g/>
63	#num#	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
čirý	čirý	k2eAgMnSc1d1
<g/>
,	,	kIx,
bezbarvý	bezbarvý	k2eAgMnSc1d1
a	a	k8xC
neslazený	slazený	k2eNgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
podává	podávat	k5eAaImIp3nS
smíchaný	smíchaný	k2eAgInSc1d1
s	s	k7c7
ledem	led	k1gInSc7
a	a	k8xC
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
promění	proměnit	k5eAaPmIp3nS
likér	likér	k1gInSc4
v	v	k7c4
mléčně	mléčně	k6eAd1
bílou	bílý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
grapefruitovým	grapefruitový	k2eAgInSc7d1
džusem	džus	k1gInSc7
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Arak	arak	k1gInSc1
eshkoliyyot	eshkoliyyot	k1gInSc1
(	(	kIx(
<g/>
Arak	arak	k1gInSc1
grapefruit	grapefruit	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejrůznější	různý	k2eAgFnPc1d3
pálenky	pálenka	k1gFnPc1
a	a	k8xC
likéry	likér	k1gInPc1
se	se	k3xPyFc4
produkují	produkovat	k5eAaImIp3nP
po	po	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
JANALÍK	JANALÍK	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
;	;	kIx,
MARHOLD	MARHOLD	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tajemství	tajemství	k1gNnSc1
židovské	židovský	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
<g/>
,	,	kIx,
aneb	aneb	k?
<g/>
,	,	kIx,
270	#num#	k4
tradičních	tradiční	k2eAgInPc2d1
židovských	židovský	k2eAgInPc2d1
receptů	recept	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Plot	plot	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86523	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KREKULOVÁ	KREKULOVÁ	kA
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židovská	židovský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7209	#num#	k4
<g/>
-	-	kIx~
<g/>
369	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
WIRKOWSKI	WIRKOWSKI	kA
<g/>
,	,	kIx,
Eugeniusz	Eugeniusz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židovská	židovský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Práca	Práca	k1gMnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7094	#num#	k4
<g/>
-	-	kIx~
<g/>
289	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Izraelské	izraelský	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Košer	košer	k6eAd1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Židovská	židovský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
|	|	kIx~
Hebraistika	hebraistika	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Asijská	asijský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
Nezávislé	závislý	k2eNgFnSc2d1
státy	stát	k1gInPc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
Myanmar	Myanmar	k1gMnSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
SAE	SAE	kA
•	•	k?
Srí	Srí	k1gMnSc1
Lanka	lanko	k1gNnSc2
•	•	k?
Sýrie	Sýrie	k1gFnSc1
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
•	•	k?
Východní	východní	k2eAgInSc1d1
Timor	Timor	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
<g/>
,	,	kIx,
<g/>
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
adalší	adalšet	k5eAaImIp3nP,k5eAaPmIp3nP
regiony	region	k1gInPc4
</s>
<s>
Hongkong	Hongkong	k1gInSc1
•	•	k?
Jukagirsko	Jukagirsko	k1gNnSc4
•	•	k?
Kašmír	Kašmír	k1gInSc1
•	•	k?
Komandorské	Komandorský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Korea	Korea	k1gFnSc1
•	•	k?
Kuej-čou	Kuej-čá	k1gFnSc4
•	•	k?
Macao	Macao	k1gNnSc1
•	•	k?
Nivchsko	Nivchsko	k1gNnSc1
•	•	k?
Palestina	Palestina	k1gFnSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Tibet	Tibet	k1gInSc1
•	•	k?
Vánoční	vánoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Ujgursko	Ujgursko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85031849	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85031849	#num#	k4
</s>
