<s>
Abundantní	abundantní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Přirozená	přirozený	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
od	od	k7c2
1	#num#	k4
do	do	k7c2
40	#num#	k4
a	a	k8xC
hodnoty	hodnota	k1gFnPc1
součtů	součet	k1gInPc2
jejich	jejich	k3xOp3gInPc2
dělitelů	dělitel	k1gInPc2
s	s	k7c7
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
<g/>
;	;	kIx,
abundantní	abundantní	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
znázorněna	znázorněn	k2eAgNnPc1d1
modře	modro	k6eAd1
</s>
<s>
Abundantní	abundantní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
z	z	k7c2
latiny	latina	k1gFnSc2
abundans	abundans	k1gInSc4
–	–	k?
hojný	hojný	k2eAgMnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
matematice	matematika	k1gFnSc6
takové	takový	k3xDgNnSc1
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
menší	malý	k2eAgFnSc1d2
než	než	k8xS
součet	součet	k1gInSc1
jeho	jeho	k3xOp3gInPc2
vlastních	vlastní	k2eAgInPc2d1
dělitelů	dělitel	k1gInPc2
kromě	kromě	k7c2
sebe	se	k3xPyFc2
<g/>
,	,	kIx,
opakem	opak	k1gInSc7
je	být	k5eAaImIp3nS
deficientní	deficientní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1
(	(	kIx(
<g/>
ekvivalentní	ekvivalentní	k2eAgFnSc1d1
<g/>
)	)	kIx)
definice	definice	k1gFnSc1
abundantního	abundantní	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
abundantní	abundantní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
takové	takový	k3xDgNnSc1
přirozené	přirozený	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
n	n	k0
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
platí	platit	k5eAaImIp3nP
σ	σ	k?
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
>	>	kIx)
2	#num#	k4
<g/>
n.	n.	k?
Kde	kde	k6eAd1
σ	σ	k?
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
součet	součet	k1gInSc1
všech	všecek	k3xTgMnPc2
kladných	kladný	k2eAgMnPc2d1
dělitelů	dělitel	k1gMnPc2
čísla	číslo	k1gNnSc2
n	n	k0
<g/>
,	,	kIx,
včetně	včetně	k7c2
čísla	číslo	k1gNnSc2
samého	samý	k3xTgNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnota	hodnota	k1gFnSc1
σ	σ	k?
<g/>
(	(	kIx(
<g/>
n	n	k0
<g/>
)	)	kIx)
-	-	kIx~
2	#num#	k4
<g/>
n	n	k0
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
abundance	abundance	k1gFnSc1
čísla	číslo	k1gNnSc2
n.	n.	k?
</s>
<s>
Abudantní	Abudantní	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
jsou	být	k5eAaImIp3nP
např.	např.	kA
</s>
<s>
12	#num#	k4
<g/>
,	,	kIx,
18	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
24	#num#	k4
<g/>
,	,	kIx,
30	#num#	k4
<g/>
,	,	kIx,
36	#num#	k4
<g/>
,	,	kIx,
40	#num#	k4
<g/>
,	,	kIx,
42	#num#	k4
<g/>
,	,	kIx,
48	#num#	k4
<g/>
,	,	kIx,
54	#num#	k4
<g/>
,	,	kIx,
56	#num#	k4
<g/>
,	,	kIx,
60	#num#	k4
<g/>
,	,	kIx,
66	#num#	k4
<g/>
,	,	kIx,
70	#num#	k4
<g/>
,	,	kIx,
72	#num#	k4
<g/>
,	,	kIx,
78	#num#	k4
<g/>
,	,	kIx,
80	#num#	k4
<g/>
,	,	kIx,
84	#num#	k4
<g/>
,	,	kIx,
88	#num#	k4
<g/>
,	,	kIx,
90	#num#	k4
<g/>
,	,	kIx,
96	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
102	#num#	k4
<g/>
,	,	kIx,
…	…	k?
</s>
<s>
Vezměme	vzít	k5eAaPmRp1nP
si	se	k3xPyFc3
například	například	k6eAd1
číslo	číslo	k1gNnSc4
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
dělitelé	dělitel	k1gMnPc1
jsou	být	k5eAaImIp3nP
čísla	číslo	k1gNnSc2
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
,	,	kIx,
12	#num#	k4
a	a	k8xC
24	#num#	k4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc1
součet	součet	k1gInSc1
je	být	k5eAaImIp3nS
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
60	#num#	k4
je	být	k5eAaImIp3nS
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
2	#num#	k4
×	×	k?
24	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
číslo	číslo	k1gNnSc4
24	#num#	k4
abundantní	abundantní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
abundance	abundance	k1gFnSc2
je	být	k5eAaImIp3nS
60	#num#	k4
-	-	kIx~
2	#num#	k4
×	×	k?
24	#num#	k4
=	=	kIx~
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Abundantní	abundantní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
každé	každý	k3xTgNnSc4
sudé	sudý	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
není	být	k5eNaImIp3nS
prvočíslo	prvočíslo	k1gNnSc4
<g/>
,	,	kIx,
poloprvočíslo	poloprvočísnout	k5eAaPmAgNnS
nebo	nebo	k8xC
jakákoli	jakýkoli	k3yIgFnSc1
mocnina	mocnina	k1gFnSc1
(	(	kIx(
<g/>
výjimkou	výjimka	k1gFnSc7
jsou	být	k5eAaImIp3nP
lichá	lichý	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
protože	protože	k8xS
i	i	k8xC
většina	většina	k1gFnSc1
lichých	lichý	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
splňujících	splňující	k2eAgInPc2d1
tyto	tento	k3xDgFnPc4
podmínky	podmínka	k1gFnPc4
je	být	k5eAaImIp3nS
kvůli	kvůli	k7c3
nízkému	nízký	k2eAgInSc3d1
součtu	součet	k1gInSc3
svých	svůj	k3xOyFgInPc2
dělitelů	dělitel	k1gInPc2
deficientní	deficientní	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejmenší	malý	k2eAgNnSc1d3
liché	lichý	k2eAgNnSc1d1
abundantní	abundantní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
je	být	k5eAaImIp3nS
945	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Každé	každý	k3xTgNnSc1
celé	celý	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
větší	veliký	k2eAgFnSc2d2
než	než	k8xS
20	#num#	k4
161	#num#	k4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zapsáno	zapsat	k5eAaPmNgNnS
jako	jako	k8xC,k8xS
součet	součet	k1gInSc1
dvou	dva	k4xCgNnPc2
abundantních	abundantní	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dokonalé	dokonalý	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
</s>
<s>
Deficientní	Deficientní	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Mersennovo	Mersennův	k2eAgNnSc1d1
prvočíslo	prvočíslo	k1gNnSc1
</s>
<s>
GIMPS	GIMPS	kA
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
