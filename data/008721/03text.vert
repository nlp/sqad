<p>
<s>
Zbyslav	Zbyslava	k1gFnPc2	Zbyslava
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vykládá	vykládat	k5eAaImIp3nS	vykládat
se	se	k3xPyFc4	se
jako	jako	k9	jako
"	"	kIx"	"
<g/>
posilující	posilující	k2eAgFnSc4d1	posilující
slávu	sláva	k1gFnSc4	sláva
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
také	také	k9	také
jako	jako	k9	jako
"	"	kIx"	"
<g/>
kdo	kdo	k3yQnSc1	kdo
má	mít	k5eAaImIp3nS	mít
hodně	hodně	k6eAd1	hodně
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
komu	kdo	k3yQnSc3	kdo
zbývá	zbývat	k5eAaImIp3nS	zbývat
sláva	sláva	k1gFnSc1	sláva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
variantou	varianta	k1gFnSc7	varianta
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
Zbislav	Zbislav	k1gMnSc1	Zbislav
<g/>
.	.	kIx.	.
</s>
<s>
Ženskou	ženský	k2eAgFnSc7d1	ženská
podobou	podoba	k1gFnSc7	podoba
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
Zbyslava	Zbyslava	k1gFnSc1	Zbyslava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
staršího	starý	k2eAgInSc2d2	starší
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zbyslav	Zbyslava	k1gFnPc2	Zbyslava
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Polsky	Polska	k1gFnPc1	Polska
<g/>
:	:	kIx,	:
Zbysław	Zbysław	k1gFnPc1	Zbysław
</s>
</p>
<p>
<s>
==	==	k?	==
Známí	známý	k2eAgMnPc1d1	známý
nositelé	nositel	k1gMnPc1	nositel
jména	jméno	k1gNnSc2	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Zbyslav	Zbyslava	k1gFnPc2	Zbyslava
z	z	k7c2	z
Třebouně	Třebouna	k1gFnSc3	Třebouna
–	–	k?	–
otec	otec	k1gMnSc1	otec
Viléma	Vilém	k1gMnSc2	Vilém
Zajíce	Zajíc	k1gMnSc2	Zajíc
z	z	k7c2	z
Valdeka	Valdeek	k1gInSc2	Valdeek
</s>
</p>
