<s>
Pulmonické	Pulmonický	k2eAgInPc1d1
hlásky	hlásek	k1gInPc1
vznikají	vznikat	k5eAaImIp3nP
prouděním	proudění	k1gNnSc7
vzduchu	vzduch	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
je	být	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgInS
do	do	k7c2
pohybu	pohyb	k1gInSc2
pomocí	pomocí	k7c2
plic	plíce	k1gFnPc2
<g/>
.	.	kIx.
</s>