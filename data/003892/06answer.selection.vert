<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
řekou	řeka	k1gFnSc7	řeka
Venezuely	Venezuela	k1gFnSc2	Venezuela
je	být	k5eAaImIp3nS	být
Orinoco	Orinoco	k6eAd1	Orinoco
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
pravé	pravý	k2eAgInPc1d1	pravý
přítoky	přítok	k1gInPc1	přítok
tvoří	tvořit	k5eAaImIp3nP	tvořit
vodopády	vodopád	k1gInPc4	vodopád
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opouští	opouštět	k5eAaImIp3nS	opouštět
Guyanskou	Guyanský	k2eAgFnSc4d1	Guyanská
vysočinu	vysočina	k1gFnSc4	vysočina
<g/>
.	.	kIx.	.
</s>
