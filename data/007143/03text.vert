<s>
Přímka	přímka	k1gFnSc1	přímka
je	být	k5eAaImIp3nS	být
jednorozměrný	jednorozměrný	k2eAgInSc1d1	jednorozměrný
základní	základní	k2eAgInSc1d1	základní
geometrický	geometrický	k2eAgInSc1d1	geometrický
útvar	útvar	k1gInSc1	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
popsat	popsat	k5eAaPmF	popsat
jako	jako	k8xS	jako
nekonečně	konečně	k6eNd1	konečně
tenkou	tenký	k2eAgFnSc7d1	tenká
<g/>
,	,	kIx,	,
dvoustranně	dvoustranně	k6eAd1	dvoustranně
nekonečně	konečně	k6eNd1	konečně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
rovnou	rovný	k2eAgFnSc4d1	rovná
křivku	křivka	k1gFnSc4	křivka
(	(	kIx(	(
<g/>
pojem	pojem	k1gInSc4	pojem
křivka	křivka	k1gFnSc1	křivka
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
"	"	kIx"	"
<g/>
rovné	rovný	k2eAgFnPc4d1	rovná
křivky	křivka	k1gFnPc4	křivka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
křivku	křivka	k1gFnSc4	křivka
s	s	k7c7	s
nekonečně	konečně	k6eNd1	konečně
velkým	velký	k2eAgInSc7d1	velký
poloměrem	poloměr	k1gInSc7	poloměr
zakřivení	zakřivení	k1gNnSc2	zakřivení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
euklidovské	euklidovský	k2eAgFnSc6d1	euklidovská
geometrii	geometrie	k1gFnSc6	geometrie
pro	pro	k7c4	pro
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
body	bod	k1gInPc4	bod
existuje	existovat	k5eAaImIp3nS	existovat
právě	právě	k9	právě
jedna	jeden	k4xCgFnSc1	jeden
přímka	přímka	k1gFnSc1	přímka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
oběma	dva	k4xCgMnPc7	dva
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
přímka	přímka	k1gFnSc1	přímka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejkratší	krátký	k2eAgFnSc4d3	nejkratší
spojnici	spojnice	k1gFnSc4	spojnice
mezi	mezi	k7c7	mezi
dotyčnými	dotyčný	k2eAgInPc7d1	dotyčný
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
úsečku	úsečka	k1gFnSc4	úsečka
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
bodu	bod	k1gInSc2	bod
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
přímka	přímka	k1gFnSc1	přímka
trajektorie	trajektorie	k1gFnSc2	trajektorie
fotonu	foton	k1gInSc2	foton
neovlivněného	ovlivněný	k2eNgInSc2d1	neovlivněný
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
přímky	přímka	k1gFnSc2	přímka
je	být	k5eAaImIp3nS	být
osa	osa	k1gFnSc1	osa
<g/>
.	.	kIx.	.
</s>
<s>
Přímka	přímka	k1gFnSc1	přímka
se	se	k3xPyFc4	se
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
rovnou	rovný	k2eAgFnSc7d1	rovná
čarou	čára	k1gFnSc7	čára
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
,	,	kIx,	,
c	c	k0	c
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Přímka	přímka	k1gFnSc1	přímka
procházející	procházející	k2eAgFnSc1d1	procházející
dvěma	dva	k4xCgInPc7	dva
body	bod	k1gInPc7	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
,	,	kIx,	,
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
značena	značen	k2eAgFnSc1d1	značena
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
↔	↔	k?	↔
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overleftrightarrow	overleftrightarrow	k?	overleftrightarrow
{	{	kIx(	{
<g/>
AB	AB	kA	AB
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Znázornění	znázornění	k1gNnPc4	znázornění
<g/>
:	:	kIx,	:
Přímku	přímka	k1gFnSc4	přímka
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
lze	lze	k6eAd1	lze
algebraicky	algebraicky	k6eAd1	algebraicky
popsat	popsat	k5eAaPmF	popsat
pomocí	pomocí	k7c2	pomocí
lineárních	lineární	k2eAgFnPc2d1	lineární
rovnic	rovnice	k1gFnPc2	rovnice
nebo	nebo	k8xC	nebo
lineárních	lineární	k2eAgFnPc2d1	lineární
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
intuitivní	intuitivní	k2eAgInSc1d1	intuitivní
koncept	koncept	k1gInSc1	koncept
přímky	přímka	k1gFnSc2	přímka
lze	lze	k6eAd1	lze
formalizovat	formalizovat	k5eAaBmF	formalizovat
několika	několik	k4yIc2	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
geometrie	geometrie	k1gFnSc1	geometrie
postavena	postaven	k2eAgFnSc1d1	postavena
axiomaticky	axiomaticky	k6eAd1	axiomaticky
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
v	v	k7c6	v
Eukleidových	Eukleidový	k2eAgInPc6d1	Eukleidový
Základech	základ	k1gInPc6	základ
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ve	v	k7c4	v
Foundations	Foundations	k1gInSc4	Foundations
of	of	k?	of
Geometry	geometr	k1gInPc4	geometr
Davida	David	k1gMnSc2	David
Hilberta	Hilbert	k1gMnSc2	Hilbert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
přímky	přímka	k1gFnPc1	přímka
nejsou	být	k5eNaImIp3nP	být
vůbec	vůbec	k9	vůbec
definovány	definován	k2eAgFnPc1d1	definována
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
axiomaticky	axiomaticky	k6eAd1	axiomaticky
charakterizovány	charakterizován	k2eAgFnPc1d1	charakterizována
svými	svůj	k3xOyFgFnPc7	svůj
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
splňuje	splňovat	k5eAaImIp3nS	splňovat
axiomy	axiom	k1gInPc4	axiom
pro	pro	k7c4	pro
přímku	přímka	k1gFnSc4	přímka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přímka	přímka	k1gFnSc1	přímka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zatímco	zatímco	k8xS	zatímco
Eukleidés	Eukleidés	k1gInSc1	Eukleidés
definoval	definovat	k5eAaBmAgInS	definovat
přímku	přímka	k1gFnSc4	přímka
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
délku	délka	k1gFnSc4	délka
bez	bez	k7c2	bez
šířky	šířka	k1gFnSc2	šířka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
pozdějších	pozdní	k2eAgInPc6d2	pozdější
vývodech	vývod	k1gInPc6	vývod
tuto	tento	k3xDgFnSc4	tento
mlhavou	mlhavý	k2eAgFnSc4d1	mlhavá
definici	definice	k1gFnSc4	definice
nepoužíval	používat	k5eNaImAgMnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Eukleidovském	eukleidovský	k2eAgInSc6d1	eukleidovský
prostoru	prostor	k1gInSc6	prostor
Rn	Rn	k1gFnSc2	Rn
(	(	kIx(	(
<g/>
a	a	k8xC	a
analogicky	analogicky	k6eAd1	analogicky
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
ostatních	ostatní	k2eAgInPc6d1	ostatní
vektorových	vektorový	k2eAgInPc6d1	vektorový
prostorech	prostor	k1gInPc6	prostor
<g/>
)	)	kIx)	)
definujeme	definovat	k5eAaBmIp1nP	definovat
přímku	přímka	k1gFnSc4	přímka
L	L	kA	L
jako	jako	k8xS	jako
podmnožinu	podmnožina	k1gFnSc4	podmnožina
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
=	=	kIx~	=
{	{	kIx(	{
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
+	+	kIx~	+
t	t	k?	t
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
∣	∣	k?	∣
t	t	k?	t
∈	∈	k?	∈
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
+	+	kIx~	+
<g/>
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mid	mid	k?	mid
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
}}	}}	k?	}}
:	:	kIx,	:
kde	kde	k6eAd1	kde
a	a	k8xC	a
a	a	k8xC	a
b	b	k?	b
jsou	být	k5eAaImIp3nP	být
vektory	vektor	k1gInPc1	vektor
v	v	k7c6	v
Rn	Rn	k1gMnSc6	Rn
a	a	k8xC	a
b	b	k?	b
je	být	k5eAaImIp3nS	být
nenulové	nulový	k2eNgNnSc1d1	nenulové
<g/>
.	.	kIx.	.
</s>
<s>
Vektor	vektor	k1gInSc1	vektor
b	b	k?	b
udává	udávat	k5eAaImIp3nS	udávat
směr	směr	k1gInSc4	směr
přímky	přímka	k1gFnSc2	přímka
a	a	k8xC	a
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
bod	bod	k1gInSc4	bod
na	na	k7c6	na
přímce	přímka	k1gFnSc6	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Tutéž	týž	k3xTgFnSc4	týž
přímku	přímka	k1gFnSc4	přímka
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
pomocí	pomocí	k7c2	pomocí
různých	různý	k2eAgFnPc2d1	různá
kombinací	kombinace	k1gFnPc2	kombinace
a	a	k8xC	a
a	a	k8xC	a
b.	b.	k?	b.
V	v	k7c6	v
R2	R2	k1gFnSc6	R2
je	být	k5eAaImIp3nS	být
každá	každý	k3xTgFnSc1	každý
přímka	přímka	k1gFnSc1	přímka
L	L	kA	L
popsaná	popsaný	k2eAgFnSc1d1	popsaná
lineární	lineární	k2eAgFnSc7d1	lineární
rovnicí	rovnice	k1gFnSc7	rovnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zadána	zadat	k5eAaPmNgFnS	zadat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
tvarech	tvar	k1gInPc6	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Směrnicová	směrnicový	k2eAgFnSc1d1	směrnicový
rovnice	rovnice	k1gFnSc1	rovnice
přímky	přímka	k1gFnSc2	přímka
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
k	k	k7c3	k
x	x	k?	x
+	+	kIx~	+
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
kx	kx	k?	kx
<g/>
+	+	kIx~	+
<g/>
q	q	k?	q
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
=	=	kIx~	=
tg	tg	kA	tg
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
tg	tg	kA	tg
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
směrnice	směrnice	k1gFnSc1	směrnice
přímky	přímka	k1gFnSc2	přímka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnPc6	varphi
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
orientovaný	orientovaný	k2eAgInSc4d1	orientovaný
úhel	úhel	k1gInSc4	úhel
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
v	v	k7c6	v
průsečíku	průsečík	k1gInSc6	průsečík
přímky	přímka	k1gFnSc2	přímka
a	a	k8xC	a
první	první	k4xOgFnSc2	první
souřadnicové	souřadnicový	k2eAgFnSc2d1	souřadnicová
osy	osa	k1gFnSc2	osa
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
jehož	jehož	k3xOyRp3gInPc4	jehož
rameny	rameno	k1gNnPc7	rameno
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
kladně	kladně	k6eAd1	kladně
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
<g/>
)	)	kIx)	)
první	první	k4xOgFnSc1	první
osa	osa	k1gFnSc1	osa
souřadnicové	souřadnicový	k2eAgFnSc2d1	souřadnicová
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
přímka	přímka	k1gFnSc1	přímka
<g/>
,	,	kIx,	,
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
úsek	úsek	k1gInSc1	úsek
(	(	kIx(	(
<g/>
vytnutý	vytnutý	k2eAgInSc1d1	vytnutý
přímkou	přímka	k1gFnSc7	přímka
<g/>
)	)	kIx)	)
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
souřadnice	souřadnice	k1gFnSc1	souřadnice
průsečíku	průsečík	k1gInSc2	průsečík
přímky	přímka	k1gFnSc2	přímka
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
>	>	kIx)	>
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
představuje	představovat	k5eAaImIp3nS	představovat
rovnice	rovnice	k1gFnSc1	rovnice
přímky	přímka	k1gFnSc2	přímka
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
<	<	kIx(	<
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
<	<	kIx(	<
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
funkci	funkce	k1gFnSc4	funkce
klesající	klesající	k2eAgFnSc1d1	klesající
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
přímka	přímka	k1gFnSc1	přímka
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
přímka	přímka	k1gFnSc1	přímka
prochází	procházet	k5eAaImIp3nS	procházet
počátkem	počátek	k1gInSc7	počátek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
O	O	kA	O
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Přímku	přímka	k1gFnSc4	přímka
rovnoběžnou	rovnoběžný	k2eAgFnSc4d1	rovnoběžná
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
nelze	lze	k6eNd1	lze
směrnicovou	směrnicový	k2eAgFnSc7d1	směrnicový
rovnicí	rovnice	k1gFnSc7	rovnice
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Úseková	úsekový	k2eAgFnSc1d1	úseková
rovnice	rovnice	k1gFnSc1	rovnice
přímky	přímka	k1gFnSc2	přímka
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
q	q	k?	q
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
úsek	úsek	k1gInSc1	úsek
(	(	kIx(	(
<g/>
vytnutý	vytnutý	k2eAgInSc1d1	vytnutý
přímkou	přímka	k1gFnSc7	přímka
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
q	q	k?	q
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
úsek	úsek	k1gInSc1	úsek
(	(	kIx(	(
<g/>
vytnutý	vytnutý	k2eAgInSc1d1	vytnutý
přímkou	přímka	k1gFnSc7	přímka
<g/>
)	)	kIx)	)
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Přímku	přímka	k1gFnSc4	přímka
rovnoběžnou	rovnoběžný	k2eAgFnSc4d1	rovnoběžná
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
nelze	lze	k6eNd1	lze
úsekovou	úsekový	k2eAgFnSc7d1	úseková
rovnicí	rovnice	k1gFnSc7	rovnice
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Normálovou	normálový	k2eAgFnSc4d1	normálová
rovnici	rovnice	k1gFnSc4	rovnice
přímky	přímka	k1gFnSc2	přímka
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
cos	cos	kA	cos
:	:	kIx,	:
ψ	ψ	k?	ψ
+	+	kIx~	+
y	y	k?	y
sin	sin	kA	sin
:	:	kIx,	:
ψ	ψ	k?	ψ
−	−	k?	−
n	n	k0	n
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
+	+	kIx~	+
<g/>
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
-n	-n	k?	-n
<g/>
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
≥	≥	k?	≥
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
}	}	kIx)	}
představuje	představovat	k5eAaImIp3nS	představovat
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
počátku	počátek	k1gInSc2	počátek
soustavy	soustava	k1gFnSc2	soustava
souřadnic	souřadnice	k1gFnPc2	souřadnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
O	O	kA	O
<g/>
}	}	kIx)	}
od	od	k7c2	od
přímky	přímka	k1gFnSc2	přímka
a	a	k8xC	a
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
ψ	ψ	k?	ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc1	velikost
orientovaného	orientovaný	k2eAgInSc2d1	orientovaný
úhlu	úhel	k1gInSc2	úhel
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
rameno	rameno	k1gNnSc4	rameno
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
kladná	kladný	k2eAgFnSc1d1	kladná
poloosa	poloosa	k1gFnSc1	poloosa
souřadné	souřadný	k2eAgFnSc2d1	souřadná
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
rameno	rameno	k1gNnSc4	rameno
je	být	k5eAaImIp3nS	být
polopřímka	polopřímka	k1gFnSc1	polopřímka
s	s	k7c7	s
počátkem	počátek	k1gInSc7	počátek
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
O	o	k7c4	o
<g/>
}	}	kIx)	}
vedená	vedený	k2eAgFnSc1d1	vedená
kolmo	kolmo	k6eAd1	kolmo
k	k	k7c3	k
přímce	přímka	k1gFnSc3	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gInPc1	člen
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
ψ	ψ	k?	ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cos	cos	k3yInSc1	cos
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
ψ	ψ	k?	ψ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sin	sino	k1gNnPc2	sino
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
}	}	kIx)	}
představují	představovat	k5eAaImIp3nP	představovat
složky	složka	k1gFnPc1	složka
jednotkového	jednotkový	k2eAgInSc2d1	jednotkový
vektoru	vektor	k1gInSc2	vektor
kolmého	kolmý	k2eAgInSc2d1	kolmý
k	k	k7c3	k
přímce	přímka	k1gFnSc3	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
rovnice	rovnice	k1gFnSc1	rovnice
přímky	přímka	k1gFnSc2	přímka
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
je	být	k5eAaImIp3nS	být
speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
obecné	obecný	k2eAgFnSc2d1	obecná
rovnice	rovnice	k1gFnSc2	rovnice
nadroviny	nadrovina	k1gFnSc2	nadrovina
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
x	x	k?	x
+	+	kIx~	+
b	b	k?	b
y	y	k?	y
+	+	kIx~	+
c	c	k0	c
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
ax	ax	k?	ax
<g/>
+	+	kIx~	+
<g/>
by	by	kYmCp3nS	by
<g/>
+	+	kIx~	+
<g/>
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
,	,	kIx,	,
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
konstanty	konstanta	k1gFnPc1	konstanta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
přímka	přímka	k1gFnSc1	přímka
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
,	,	kIx,	,
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
přímka	přímka	k1gFnSc1	přímka
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
prochází	procházet	k5eAaImIp3nS	procházet
přímka	přímka	k1gFnSc1	přímka
počátkem	počátek	k1gInSc7	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Porovnáním	porovnání	k1gNnSc7	porovnání
obecné	obecný	k2eAgFnSc2d1	obecná
a	a	k8xC	a
normálové	normálový	k2eAgFnSc2d1	normálová
rovnice	rovnice	k1gFnSc2	rovnice
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
význam	význam	k1gInSc4	význam
konstant	konstanta	k1gFnPc2	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
,	,	kIx,	,
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Konstanty	konstanta	k1gFnPc1	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
určují	určovat	k5eAaImIp3nP	určovat
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
kolmý	kolmý	k2eAgInSc1d1	kolmý
k	k	k7c3	k
přímce	přímka	k1gFnSc3	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Parametr	parametr	k1gInSc1	parametr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
pak	pak	k6eAd1	pak
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
přímky	přímka	k1gFnSc2	přímka
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
souřadné	souřadný	k2eAgFnSc2d1	souřadná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Obecnou	obecný	k2eAgFnSc4d1	obecná
rovnici	rovnice	k1gFnSc4	rovnice
přímky	přímka	k1gFnSc2	přímka
lze	lze	k6eAd1	lze
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
rovnici	rovnice	k1gFnSc4	rovnice
směrnicovou	směrnicový	k2eAgFnSc4d1	směrnicový
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zavedeme	zavést	k5eAaPmIp1nP	zavést
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
q	q	k?	q
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
q	q	k?	q
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Zavedeme	zavést	k5eAaPmIp1nP	zavést
<g/>
-li	i	k?	-li
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
q	q	k?	q
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
q	q	k?	q
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
≠	≠	k?	≠
0	[number]	k4	0
,	,	kIx,	,
b	b	k?	b
≠	≠	k?	≠
0	[number]	k4	0
,	,	kIx,	,
c	c	k0	c
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
obecnou	obecný	k2eAgFnSc4d1	obecná
rovnici	rovnice	k1gFnSc4	rovnice
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
úsekový	úsekový	k2eAgInSc4d1	úsekový
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Převedením	převedení	k1gNnSc7	převedení
obecné	obecný	k2eAgFnSc2d1	obecná
rovnice	rovnice	k1gFnSc2	rovnice
přímky	přímka	k1gFnSc2	přímka
do	do	k7c2	do
normálového	normálový	k2eAgInSc2d1	normálový
tvaru	tvar	k1gInSc2	tvar
získáme	získat	k5eAaPmIp1nP	získat
normálovou	normálový	k2eAgFnSc4d1	normálová
rovnici	rovnice	k1gFnSc4	rovnice
přímky	přímka	k1gFnSc2	přímka
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sgn	sgn	k?	sgn
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sgn	sgn	k?	sgn
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sgn	sgn	k?	sgn
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
sgn	sgn	k?	sgn
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}}	}}}	k?	}}}
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
sgn	sgn	k?	sgn
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}}	}}}	k?	}}}
<g/>
y	y	k?	y
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
sgn	sgn	k?	sgn
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
Důležité	důležitý	k2eAgFnPc1d1	důležitá
vlastnosti	vlastnost	k1gFnPc1	vlastnost
takto	takto	k6eAd1	takto
definovaných	definovaný	k2eAgFnPc2d1	definovaná
přímek	přímka	k1gFnPc2	přímka
jsou	být	k5eAaImIp3nP	být
jejich	jejich	k3xOp3gFnPc2	jejich
sklon	sklona	k1gFnPc2	sklona
<g/>
,	,	kIx,	,
x-intercept	xntercepta	k1gFnPc2	x-intercepta
a	a	k8xC	a
y-intercept	yntercepta	k1gFnPc2	y-intercepta
<g/>
.	.	kIx.	.
</s>
<s>
Excentricita	excentricita	k1gFnSc1	excentricita
přímky	přímka	k1gFnSc2	přímka
je	být	k5eAaImIp3nS	být
nekonečno	nekonečno	k1gNnSc1	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Parametrické	parametrický	k2eAgNnSc4d1	parametrické
vyjádření	vyjádření	k1gNnSc4	vyjádření
přímky	přímka	k1gFnSc2	přímka
je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
vztahem	vztah	k1gInSc7	vztah
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
=	=	kIx~	=
A	A	kA	A
+	+	kIx~	+
u	u	k7c2	u
.	.	kIx.	.
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
=	=	kIx~	=
<g/>
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
u.t	u.t	k?	u.t
<g/>
}	}	kIx)	}
a	a	k8xC	a
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
dáno	dát	k5eAaPmNgNnS	dát
rovnicemi	rovnice	k1gFnPc7	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
y	y	k?	y
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
t	t	k?	t
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
A	a	k8xC	a
=	=	kIx~	=
[	[	kIx(	[
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
=	=	kIx~	=
<g/>
[	[	kIx(	[
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
libovolný	libovolný	k2eAgInSc4d1	libovolný
bod	bod	k1gInSc4	bod
přímky	přímka	k1gFnSc2	přímka
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
jsou	být	k5eAaImIp3nP	být
konstanty	konstanta	k1gFnPc4	konstanta
určující	určující	k2eAgFnPc4d1	určující
<g />
.	.	kIx.	.
</s>
<s hack="1">
směrnici	směrnice	k1gFnSc3	směrnice
přímky	přímka	k1gFnSc2	přímka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vektor	vektor	k1gInSc1	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
směrovým	směrový	k2eAgInSc7d1	směrový
vektorem	vektor	k1gInSc7	vektor
přímky	přímka	k1gFnSc2	přímka
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
∈	∈	k?	∈
(	(	kIx(	(
−	−	k?	−
∞	∞	k?	∞
,	,	kIx,	,
∞	∞	k?	∞
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
proměnný	proměnný	k2eAgInSc1d1	proměnný
parametr	parametr	k1gInSc1	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Alespoň	alespoň	k9	alespoň
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
konstant	konstanta	k1gFnPc2	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nenulová	nulový	k2eNgFnSc1d1	nenulová
<g/>
.	.	kIx.	.
</s>
<s>
Vektorová	vektorový	k2eAgFnSc1d1	vektorová
rovnice	rovnice	k1gFnSc1	rovnice
přímky	přímka	k1gFnSc2	přímka
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
rádiusvektor	rádiusvektor	k1gInSc4	rádiusvektor
procházející	procházející	k2eAgInSc4d1	procházející
všemi	všecek	k3xTgFnPc7	všecek
body	bod	k1gInPc7	bod
přímky	přímka	k1gFnSc2	přímka
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
rádiusvektor	rádiusvektor	k1gInSc1	rádiusvektor
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
bodů	bod	k1gInPc2	bod
přímky	přímka	k1gFnSc2	přímka
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
vektor	vektor	k1gInSc4	vektor
určující	určující	k2eAgInSc4d1	určující
směr	směr	k1gInSc4	směr
přímky	přímka	k1gFnSc2	přímka
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
∈	∈	k?	∈
(	(	kIx(	(
−	−	k?	−
∞	∞	k?	∞
,	,	kIx,	,
∞	∞	k?	∞
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
proměnný	proměnný	k2eAgInSc1d1	proměnný
parametr	parametr	k1gInSc1	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Vektorový	vektorový	k2eAgInSc1d1	vektorový
zápis	zápis	k1gInSc1	zápis
tedy	tedy	k9	tedy
představuje	představovat	k5eAaImIp3nS	představovat
přehlednější	přehlední	k2eAgInSc1d2	přehlední
zápis	zápis	k1gInSc1	zápis
parametrického	parametrický	k2eAgInSc2d1	parametrický
tvaru	tvar	k1gInSc2	tvar
rovnice	rovnice	k1gFnSc2	rovnice
přímky	přímka	k1gFnSc2	přímka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
souřadnicích	souřadnice	k1gFnPc6	souřadnice
lze	lze	k6eAd1	lze
přímku	přímka	k1gFnSc4	přímka
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
ψ	ψ	k?	ψ
−	−	k?	−
φ	φ	k?	φ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
psi	pes	k1gMnPc1	pes
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
varphi	varphi	k6eAd1	varphi
)	)	kIx)	)
<g/>
}}}}	}}}}	k?	}}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
přímky	přímka	k1gFnSc2	přímka
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
O	o	k7c4	o
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varphi	varphi	k1gNnPc6	varphi
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc1	velikost
orientovaného	orientovaný	k2eAgInSc2d1	orientovaný
úhlu	úhel	k1gInSc2	úhel
s	s	k7c7	s
vrcholem	vrchol	k1gInSc7	vrchol
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
první	první	k4xOgNnSc4	první
rameno	rameno	k1gNnSc4	rameno
tvoří	tvořit	k5eAaImIp3nS	tvořit
polární	polární	k2eAgFnSc1d1	polární
osa	osa	k1gFnSc1	osa
a	a	k8xC	a
druhé	druhý	k4xOgNnSc4	druhý
rameno	rameno	k1gNnSc4	rameno
polopřímka	polopřímka	k1gFnSc1	polopřímka
kolmá	kolmý	k2eAgFnSc1d1	kolmá
k	k	k7c3	k
přímce	přímka	k1gFnSc3	přímka
s	s	k7c7	s
počátkem	počátek	k1gInSc7	počátek
v	v	k7c6	v
O.	O.	kA	O.
Rovnice	rovnice	k1gFnSc1	rovnice
přímky	přímka	k1gFnSc2	přímka
<g />
.	.	kIx.	.
</s>
<s hack="1">
se	s	k7c7	s
směrnicí	směrnice	k1gFnSc7	směrnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
procházející	procházející	k2eAgMnPc1d1	procházející
bodem	bod	k1gInSc7	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
k	k	k7c3	k
(	(	kIx(	(
x	x	k?	x
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y-y_	y_	k?	y-y_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
k	k	k7c3	k
<g/>
(	(	kIx(	(
<g/>
x-x_	x_	k?	x-x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Rovnice	rovnice	k1gFnSc1	rovnice
přímky	přímka	k1gFnSc2	přímka
procházející	procházející	k2eAgFnSc1d1	procházející
dvěma	dva	k4xCgInPc7	dva
danými	daný	k2eAgInPc7d1	daný
body	bod	k1gInPc7	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
≠	≠	k?	≠
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y-y_	y_	k?	y-y_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
x-x_	x_	k?	x-x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
-x_	_	k?	-x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
neboli	neboli	k8xC	neboli
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
x	x	k?	x
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y-y_	y_	k?	y-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x-x_	x_	k?	x-x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-x_	_	k?	-x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Předchozí	předchozí	k2eAgFnSc1d1	předchozí
rovnice	rovnice	k1gFnSc1	rovnice
bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
vyjadřována	vyjadřovat	k5eAaImNgFnS	vyjadřovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
determinantu	determinant	k1gInSc2	determinant
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
vmatrix	vmatrix	k1gInSc1	vmatrix
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
&	&	k?	&
<g/>
y	y	k?	y
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
\\	\\	k?	\\
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
\\	\\	k?	\\
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
vmatrix	vmatrix	k1gInSc1	vmatrix
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
:	:	kIx,	:
Tuto	tento	k3xDgFnSc4	tento
rovnici	rovnice	k1gFnSc4	rovnice
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
jako	jako	k8xS	jako
podmínku	podmínka	k1gFnSc4	podmínka
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tři	tři	k4xCgInPc4	tři
body	bod	k1gInPc4	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
,	,	kIx,	,
[	[	kIx(	[
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
[	[	kIx(	[
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
x_	x_	k?	x_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
body	bod	k1gInPc1	bod
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
splněna	splněn	k2eAgFnSc1d1	splněna
podmínka	podmínka	k1gFnSc1	podmínka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
vmatrix	vmatrix	k1gInSc1	vmatrix
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
\\	\\	k?	\\
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g />
.	.	kIx.	.
</s>
<s>
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
\\	\\	k?	\\
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
vmatrix	vmatrix	k1gInSc1	vmatrix
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
Přímkou	přímka	k1gFnSc7	přímka
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
množina	množina	k1gFnSc1	množina
bodů	bod	k1gInPc2	bod
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
rovnici	rovnice	k1gFnSc4	rovnice
přímky	přímka	k1gFnSc2	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Rovnici	rovnice	k1gFnSc4	rovnice
přímky	přímka	k1gFnSc2	přímka
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
R3	R3	k1gFnSc6	R3
lze	lze	k6eAd1	lze
přímku	přímka	k1gFnSc4	přímka
L	L	kA	L
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
průsečík	průsečík	k1gInSc1	průsečík
dvou	dva	k4xCgFnPc2	dva
rovin	rovina	k1gFnPc2	rovina
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
soustavy	soustava	k1gFnSc2	soustava
jejich	jejich	k3xOp3gFnPc2	jejich
lineárních	lineární	k2eAgFnPc2d1	lineární
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
=	=	kIx~	=
{	{	kIx(	{
(	(	kIx(	(
x	x	k?	x
,	,	kIx,	,
y	y	k?	y
,	,	kIx,	,
z	z	k7c2	z
)	)	kIx)	)
∣	∣	k?	∣
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
+	+	kIx~	+
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
c	c	k0	c
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
∧	∧	k?	∧
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
+	+	kIx~	+
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
,	,	kIx,	,
<g/>
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mid	mid	k?	mid
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
y	y	k?	y
<g/>
+	+	kIx~	+
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
d_	d_	k?	d_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
land	land	k1gInSc1	land
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
y	y	k?	y
<g/>
+	+	kIx~	+
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
d_	d_	k?	d_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
:	:	kIx,	:
(	(	kIx(	(
<g/>
definici	definice	k1gFnSc3	definice
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
rozšířit	rozšířit	k5eAaPmF	rozšířit
o	o	k7c4	o
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
koeficienty	koeficient	k1gInPc4	koeficient
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
až	až	k6eAd1	až
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d_	d_	k?	d_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zaručí	zaručit	k5eAaPmIp3nS	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
roviny	rovina	k1gFnPc1	rovina
budou	být	k5eAaImBp3nP	být
různoběžné	různoběžný	k2eAgFnPc1d1	různoběžná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přímka	přímka	k1gFnSc1	přímka
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
řešením	řešení	k1gNnSc7	řešení
soustavy	soustava	k1gFnSc2	soustava
rovnic	rovnice	k1gFnPc2	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
+	+	kIx~	+
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
+	+	kIx~	+
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
y	y	k?	y
<g/>
+	+	kIx~	+
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
z	z	k7c2	z
<g/>
+	+	kIx~	+
<g/>
d_	d_	k?	d_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
+	+	kIx~	+
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
+	+	kIx~	+
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
y	y	k?	y
<g/>
+	+	kIx~	+
<g/>
c_	c_	k?	c_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
z	z	k7c2	z
<g/>
+	+	kIx~	+
<g/>
d_	d_	k?	d_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
:	:	kIx,	:
Ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
případě	případ	k1gInSc6	případ
vyjádříme	vyjádřit	k5eAaPmIp1nP	vyjádřit
přímku	přímka	k1gFnSc4	přímka
jako	jako	k8xC	jako
průsečík	průsečík	k1gInSc4	průsečík
dvou	dva	k4xCgFnPc2	dva
rovin	rovina	k1gFnPc2	rovina
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
kolmá	kolmý	k2eAgFnSc1d1	kolmá
k	k	k7c3	k
některé	některý	k3yIgFnSc3	některý
souřadnicové	souřadnicový	k2eAgFnSc3d1	souřadnicová
rovině	rovina	k1gFnSc3	rovina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
roviny	rovina	k1gFnPc4	rovina
kolmé	kolmý	k2eAgFnPc1d1	kolmá
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
xy	xy	k?	xy
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
xz	xz	k?	xz
<g/>
}	}	kIx)	}
dostaneme	dostat	k5eAaPmIp1nP	dostat
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
m	m	kA	m
x	x	k?	x
+	+	kIx~	+
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
mx	mx	k?	mx
<g/>
+	+	kIx~	+
<g/>
q	q	k?	q
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
n	n	k0	n
x	x	k?	x
+	+	kIx~	+
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
nx	nx	k?	nx
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
:	:	kIx,	:
Parametrické	parametrický	k2eAgFnPc1d1	parametrická
rovnice	rovnice	k1gFnPc1	rovnice
přímky	přímka	k1gFnSc2	přímka
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
t	t	k?	t
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
ta	ten	k3xDgFnSc1	ten
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
t	t	k?	t
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
tb	tb	k?	tb
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
t	t	k?	t
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
tc	tc	k0	tc
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
libovolný	libovolný	k2eAgInSc4d1	libovolný
bod	bod	k1gInSc4	bod
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
přímka	přímka	k1gFnSc1	přímka
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
,	,	kIx,	,
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
konstanty	konstanta	k1gFnPc1	konstanta
určující	určující	k2eAgFnSc1d1	určující
směrnici	směrnice	k1gFnSc4	směrnice
přímky	přímka	k1gFnSc2	přímka
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
∈	∈	k?	∈
(	(	kIx(	(
−	−	k?	−
∞	∞	k?	∞
,	,	kIx,	,
∞	∞	k?	∞
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
parametr	parametr	k1gInSc1	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Konstanty	konstanta	k1gFnPc1	konstanta
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
,	,	kIx,	,
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyjádřeny	vyjádřit	k5eAaPmNgFnP	vyjádřit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
směrových	směrový	k2eAgInPc2d1	směrový
úhlů	úhel	k1gInPc2	úhel
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
,	,	kIx,	,
β	β	k?	β
,	,	kIx,	,
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
}	}	kIx)	}
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
t	t	k?	t
cos	cos	kA	cos
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
t	t	k?	t
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
=	=	kIx~	=
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
t	t	k?	t
cos	cos	kA	cos
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
y	y	k?	y
<g/>
=	=	kIx~	=
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
cos	cos	kA	cos
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
=	=	kIx~	=
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
t	t	k?	t
cos	cos	kA	cos
:	:	kIx,	:
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
z	z	k7c2	z
<g/>
=	=	kIx~	=
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
t	t	k?	t
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
gamma	gamma	k1gNnSc1	gamma
}	}	kIx)	}
:	:	kIx,	:
Směrové	směrový	k2eAgInPc1d1	směrový
úhly	úhel	k1gInPc1	úhel
přitom	přitom	k6eAd1	přitom
splňují	splňovat	k5eAaImIp3nP	splňovat
podmínku	podmínka	k1gFnSc4	podmínka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
+	+	kIx~	+
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
+	+	kIx~	+
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
γ	γ	k?	γ
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
cos	cos	kA	cos
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
:	:	kIx,	:
Rovnici	rovnice	k1gFnSc4	rovnice
přímky	přímka	k1gFnSc2	přímka
<g />
.	.	kIx.	.
</s>
<s hack="1">
procházející	procházející	k2eAgInPc1d1	procházející
body	bod	k1gInPc1	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
,	,	kIx,	,
[	[	kIx(	[
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
−	−	k?	−
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x-x_	x_	k?	x-x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-x_	_	k?	-x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y-y_	y_	k?	y-y_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-y_	_	k?	-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
z-z_	z_	k?	z-z_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-z_	_	k?	-z_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Rovnici	rovnice	k1gFnSc4	rovnice
přímky	přímka	k1gFnSc2	přímka
procházející	procházející	k2eAgMnPc1d1	procházející
bodem	bod	k1gInSc7	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
[	[	kIx(	[
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
]	]	kIx)	]
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
[	[	kIx(	[
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
z_	z_	k?	z_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
]	]	kIx)	]
<g/>
}	}	kIx)	}
se	s	k7c7	s
směrovými	směrový	k2eAgInPc7d1	směrový
úhly	úhel	k1gInPc7	úhel
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
,	,	kIx,	,
β	β	k?	β
,	,	kIx,	,
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
beta	beta	k1gNnSc1	beta
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
gamma	gamma	k1gFnSc1	gamma
}	}	kIx)	}
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
β	β	k?	β
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
−	−	k?	−
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
cos	cos	kA	cos
:	:	kIx,	:
γ	γ	k?	γ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x-x_	x_	k?	x-x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y-y_	y_	k?	y-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
z-z_	z_	k?	z-z_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
cos	cos	kA	cos
\	\	kIx~	\
<g/>
gamma	gamm	k1gMnSc2	gamm
}}}	}}}	k?	}}}
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
místo	místo	k7c2	místo
směrových	směrový	k2eAgInPc2d1	směrový
úhlů	úhel	k1gInPc2	úhel
určíme	určit	k5eAaPmIp1nP	určit
směrnici	směrnice	k1gFnSc4	směrnice
přímky	přímka	k1gFnSc2	přímka
parametry	parametr	k1gInPc1	parametr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
,	,	kIx,	,
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
předchozí	předchozí	k2eAgInSc4d1	předchozí
vztah	vztah	k1gInSc4	vztah
přepsat	přepsat	k5eAaPmF	přepsat
jako	jako	k8xC	jako
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
−	−	k?	−
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
−	−	k?	−
:	:	kIx,	:
z	z	k7c2	z
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
x-x_	x_	k?	x-x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
y-y_	y_	k?	y-y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
z-z_	z_	k?	z-z_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Přímku	přímka	k1gFnSc4	přímka
lze	lze	k6eAd1	lze
zavést	zavést	k5eAaPmF	zavést
také	také	k9	také
v	v	k7c6	v
n-rozměrném	nozměrný	k2eAgInSc6d1	n-rozměrný
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Přímku	přímka	k1gFnSc4	přímka
v	v	k7c6	v
Rn	Rn	k1gFnSc6	Rn
lze	lze	k6eAd1	lze
také	také	k9	také
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
parametricky	parametricky	k6eAd1	parametricky
<g/>
:	:	kIx,	:
přímka	přímka	k1gFnSc1	přímka
procházející	procházející	k2eAgFnSc1d1	procházející
bodem	bod	k1gInSc7	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
;	;	kIx,	;
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
;	;	kIx,	;
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
;	;	kIx,	;
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
;	;	kIx,	;
<g/>
...	...	k?	...
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
se	s	k7c7	s
směrovým	směrový	k2eAgInSc7d1	směrový
vektorem	vektor	k1gInSc7	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
v	v	k7c6	v
(	(	kIx(	(
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
;	;	kIx,	;
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
;	;	kIx,	;
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
;	;	kIx,	;
:	:	kIx,	:
v	v	k7c6	v
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
v	v	k7c6	v
<g/>
(	(	kIx(	(
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
;	;	kIx,	;
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
;	;	kIx,	;
<g/>
...	...	k?	...
<g/>
;	;	kIx,	;
<g/>
v_	v_	k?	v_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
bodů	bod	k1gInPc2	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
;	;	kIx,	;
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
;	;	kIx,	;
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
;	;	kIx,	;
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
;	;	kIx,	;
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
;	;	kIx,	;
<g/>
...	...	k?	...
<g/>
;	;	kIx,	;
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgInPc4	který
existuje	existovat	k5eAaImIp3nS	existovat
skalár	skalár	k1gInSc1	skalár
k	k	k7c3	k
takový	takový	k3xDgInSc4	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
k	k	k7c3	k
:	:	kIx,	:
v	v	k7c4	v
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
k	k	k7c3	k
:	:	kIx,	:
v	v	k7c4	v
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
k	k	k7c3	k
:	:	kIx,	:
v	v	k7c4	v
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
\	\	kIx~	\
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
matrix	matrix	k1gInSc1	matrix
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
kv_	kv_	k?	kv_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\\	\\	k?	\\
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
kv_	kv_	k?	kv_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\\	\\	k?	\\
<g/>
...	...	k?	...
<g/>
\\	\\	k?	\\
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
kv_	kv_	k?	kv_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
matrix	matrix	k1gInSc1	matrix
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Místo	místo	k7c2	místo
předchozího	předchozí	k2eAgNnSc2d1	předchozí
parametrického	parametrický	k2eAgNnSc2d1	parametrické
vyjádření	vyjádření	k1gNnSc2	vyjádření
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
vektorový	vektorový	k2eAgInSc1d1	vektorový
zápis	zápis	k1gInSc1	zápis
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
k	k	k7c3	k
:	:	kIx,	:
v	v	k7c4	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc4	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
+	+	kIx~	+
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
v	v	k7c6	v
<g/>
}	}	kIx)	}
}	}	kIx)	}
:	:	kIx,	:
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
poloha	poloha	k1gFnSc1	poloha
bodu	bod	k1gInSc2	bod
a	a	k8xC	a
přímky	přímka	k1gFnSc2	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgNnPc1	tři
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
téže	tenže	k3xDgFnSc6	tenže
přímce	přímka	k1gFnSc6	přímka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
kolineární	kolineární	k2eAgFnPc1d1	kolineární
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
<g/>
-li	i	k?	-li
tři	tři	k4xCgInPc4	tři
body	bod	k1gInPc4	bod
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vždy	vždy	k6eAd1	vždy
leží	ležet	k5eAaImIp3nS	ležet
právě	právě	k9	právě
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgNnPc7d1	ostatní
dvěma	dva	k4xCgFnPc7	dva
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
<g/>
-li	i	k?	-li
bod	bod	k1gInSc1	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
}	}	kIx)	}
mezi	mezi	k7c7	mezi
body	bod	k1gInPc7	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
bod	bod	k1gInSc4	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B	B	kA	B
<g/>
}	}	kIx)	}
označíme	označit	k5eAaPmIp1nP	označit
jako	jako	k9	jako
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
bod	bod	k1gInSc4	bod
úsečky	úsečka	k1gFnSc2	úsečka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
AC	AC	kA	AC
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Bod	bod	k1gInSc1	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
}	}	kIx)	}
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
přímce	přímka	k1gFnSc6	přímka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
ji	on	k3xPp3gFnSc4	on
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
polopřímky	polopřímka	k1gFnPc4	polopřímka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
bod	bod	k1gInSc1	bod
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	A	kA	A
<g/>
}	}	kIx)	}
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
bodem	bod	k1gInSc7	bod
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
polopřímek	polopřímka	k1gFnPc2	polopřímka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
polopřímku	polopřímka	k1gFnSc4	polopřímka
užíváme	užívat	k5eAaImIp1nP	užívat
značení	značení	k1gNnSc4	značení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
A	A	kA	A
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overrightarrow	overrightarrow	k?	overrightarrow
{	{	kIx(	{
<g/>
XA	XA	kA	XA
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Opačnou	opačný	k2eAgFnSc4d1	opačná
polopřímku	polopřímka	k1gFnSc4	polopřímka
k	k	k7c3	k
polopřímce	polopřímka	k1gFnSc3	polopřímka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
A	A	kA	A
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overrightarrow	overrightarrow	k?	overrightarrow
{	{	kIx(	{
<g/>
XA	XA	kA	XA
<g/>
}}}	}}}	k?	}}}
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
A	A	kA	A
:	:	kIx,	:
←	←	k?	←
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overleftarrow	overleftarrow	k?	overleftarrow
{	{	kIx(	{
<g/>
XA	XA	kA	XA
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
poloha	poloha	k1gFnSc1	poloha
dvou	dva	k4xCgFnPc2	dva
přímek	přímka	k1gFnPc2	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
různé	různý	k2eAgFnPc1d1	různá
přímky	přímka	k1gFnPc1	přímka
ležící	ležící	k2eAgFnPc1d1	ležící
v	v	k7c6	v
téže	tenže	k3xDgFnSc6	tenže
rovině	rovina	k1gFnSc6	rovina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
neprotnout	protnout	k5eNaPmF	protnout
(	(	kIx(	(
<g/>
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
společný	společný	k2eAgInSc4d1	společný
bod	bod	k1gInSc4	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
různoběžné	různoběžný	k2eAgNnSc1d1	různoběžné
a	a	k8xC	a
protnout	protnout	k5eAaPmF	protnout
se	se	k3xPyFc4	se
v	v	k7c6	v
právě	právě	k9	právě
jednom	jeden	k4xCgInSc6	jeden
bodě	bod	k1gInSc6	bod
<g/>
,	,	kIx,	,
průsečíku	průsečík	k1gInSc6	průsečík
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
roviny	rovina	k1gFnPc1	rovina
se	se	k3xPyFc4	se
protínají	protínat	k5eAaImIp3nP	protínat
v	v	k7c6	v
nejvýše	nejvýše	k6eAd1	nejvýše
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
přímce	přímka	k1gFnSc6	přímka
<g/>
,	,	kIx,	,
průsečnici	průsečnice	k1gFnSc3	průsečnice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vícerozměrných	vícerozměrný	k2eAgInPc6d1	vícerozměrný
prostorech	prostor	k1gInPc6	prostor
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nS	muset
ani	ani	k8xC	ani
být	být	k5eAaImF	být
rovnoběžné	rovnoběžný	k2eAgFnPc4d1	rovnoběžná
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
protínat	protínat	k5eAaImF	protínat
<g/>
,	,	kIx,	,
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
mimoběžky	mimoběžka	k1gFnSc2	mimoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
obě	dva	k4xCgFnPc1	dva
přímky	přímka	k1gFnPc1	přímka
rovny	roven	k2eAgFnPc1d1	rovna
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
přímky	přímka	k1gFnPc4	přímka
splývající	splývající	k2eAgFnPc4d1	splývající
(	(	kIx(	(
<g/>
totožné	totožný	k2eAgFnPc4d1	totožná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přímku	přímka	k1gFnSc4	přímka
různoběžnou	různoběžný	k2eAgFnSc4d1	různoběžná
s	s	k7c7	s
rovnoběžkami	rovnoběžka	k1gFnPc7	rovnoběžka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
,	,	kIx,	,
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
,	,	kIx,	,
<g/>
q	q	k?	q
<g/>
}	}	kIx)	}
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
příčku	příčka	k1gFnSc4	příčka
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
,	,	kIx,	,
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
,	,	kIx,	,
<g/>
q	q	k?	q
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Průnik	průnik	k1gInSc1	průnik
dvou	dva	k4xCgFnPc2	dva
polopřímek	polopřímka	k1gFnPc2	polopřímka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overrightarrow	overrightarrow	k?	overrightarrow
{	{	kIx(	{
<g/>
AB	AB	kA	AB
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
A	A	kA	A
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overrightarrow	overrightarrow	k?	overrightarrow
{	{	kIx(	{
<g/>
BA	ba	k9	ba
<g/>
}}}	}}}	k?	}}}
nazýváme	nazývat	k5eAaImIp1nP	nazývat
úsečkou	úsečka	k1gFnSc7	úsečka
a	a	k8xC	a
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
AB	AB	kA	AB
<g/>
}	}	kIx)	}
.	.	kIx.	.
asymptota	asymptota	k1gFnSc1	asymptota
–	–	k?	–
přímka	přímka	k1gFnSc1	přímka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nP	blížit
daná	daný	k2eAgFnSc1d1	daná
křivka	křivka	k1gFnSc1	křivka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
graf	graf	k1gInSc1	graf
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nezávisle	závisle	k6eNd1	závisle
proměnnou	proměnná	k1gFnSc4	proměnná
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
nade	nad	k7c4	nad
všechny	všechen	k3xTgFnPc4	všechen
meze	mez	k1gFnPc4	mez
číselná	číselný	k2eAgFnSc1d1	číselná
osa	osa	k1gFnSc1	osa
–	–	k?	–
přímka	přímka	k1gFnSc1	přímka
s	s	k7c7	s
reálnými	reálný	k2eAgNnPc7d1	reálné
čísly	číslo	k1gNnPc7	číslo
přiřazenými	přiřazený	k2eAgFnPc7d1	přiřazená
každému	každý	k3xTgMnSc3	každý
jejímu	její	k3xOp3gInSc3	její
bodu	bod	k1gInSc3	bod
<g/>
,	,	kIx,	,
užívaná	užívaný	k2eAgFnSc1d1	užívaná
např.	např.	kA	např.
jako	jako	k8xS	jako
<g />
.	.	kIx.	.
</s>
<s>
souřadná	souřadný	k2eAgFnSc1d1	souřadná
osa	osa	k1gFnSc1	osa
osa	osa	k1gFnSc1	osa
rotace	rotace	k1gFnSc1	rotace
–	–	k?	–
přímka	přímka	k1gFnSc1	přímka
<g/>
,	,	kIx,	,
kolem	kolem	k6eAd1	kolem
níž	jenž	k3xRgFnSc3	jenž
rotuje	rotovat	k5eAaImIp3nS	rotovat
(	(	kIx(	(
<g/>
otáčí	otáčet	k5eAaImIp3nS	otáčet
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
dané	daný	k2eAgNnSc1d1	dané
těleso	těleso	k1gNnSc1	těleso
nebo	nebo	k8xC	nebo
vůči	vůči	k7c3	vůči
které	který	k3yRgFnSc3	který
provádíme	provádět	k5eAaImIp1nP	provádět
matematické	matematický	k2eAgNnSc1d1	matematické
otáčení	otáčení	k1gNnSc1	otáčení
tělesa	těleso	k1gNnSc2	těleso
osa	osa	k1gFnSc1	osa
symetrie	symetrie	k1gFnSc1	symetrie
–	–	k?	–
přímka	přímka	k1gFnSc1	přímka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
lze	lze	k6eAd1	lze
zrcadlově	zrcadlově	k6eAd1	zrcadlově
obrátit	obrátit	k5eAaPmF	obrátit
geometrický	geometrický	k2eAgInSc4d1	geometrický
útvar	útvar	k1gInSc4	útvar
a	a	k8xC	a
dostat	dostat	k5eAaPmF	dostat
tak	tak	k9	tak
útvar	útvar	k1gInSc4	útvar
totožný	totožný	k2eAgInSc4d1	totožný
Eulerova	Eulerův	k2eAgFnSc1d1	Eulerova
přímka	přímka	k1gFnSc1	přímka
Simsonova	Simsonův	k2eAgFnSc1d1	Simsonova
přímka	přímka	k1gFnSc1	přímka
tečna	tečna	k1gFnSc1	tečna
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
přímka	přímka	k1gFnSc1	přímka
dotýkající	dotýkající	k2eAgFnSc1d1	dotýkající
se	se	k3xPyFc4	se
křivky	křivka	k1gFnSc2	křivka
nebo	nebo	k8xC	nebo
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
průběžným	průběžný	k2eAgInSc7d1	průběžný
bodem	bod	k1gInSc7	bod
(	(	kIx(	(
<g/>
bodem	bod	k1gInSc7	bod
dotyku	dotyk	k1gInSc2	dotyk
<g/>
)	)	kIx)	)
křivky	křivka	k1gFnPc1	křivka
(	(	kIx(	(
<g/>
plochy	plocha	k1gFnPc1	plocha
<g/>
)	)	kIx)	)
jednostranně	jednostranně	k6eAd1	jednostranně
<g/>
,	,	kIx,	,
neprotíná	protínat	k5eNaImIp3nS	protínat
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
normála	normála	k1gFnSc1	normála
–	–	k?	–
kolmice	kolmice	k1gFnSc2	kolmice
k	k	k7c3	k
tečně	tečna	k1gFnSc3	tečna
v	v	k7c6	v
bodě	bod	k1gInSc6	bod
dotyku	dotyk	k1gInSc2	dotyk
křivky	křivka	k1gFnSc2	křivka
<g/>
,	,	kIx,	,
laicky	laicky	k6eAd1	laicky
"	"	kIx"	"
<g/>
kolmice	kolmice	k1gFnSc1	kolmice
ke	k	k7c3	k
křivce	křivka	k1gFnSc3	křivka
<g/>
"	"	kIx"	"
kolmice	kolmice	k1gFnSc1	kolmice
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
přímka	přímka	k1gFnSc1	přímka
pravoúhle	pravoúhle	k6eAd1	pravoúhle
skloněná	skloněný	k2eAgFnSc1d1	skloněná
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
přímce	přímka	k1gFnSc3	přímka
nebo	nebo	k8xC	nebo
rovině	rovina	k1gFnSc3	rovina
těžnice	těžnice	k1gFnSc2	těžnice
–	–	k?	–
přímka	přímka	k1gFnSc1	přímka
procházející	procházející	k2eAgFnSc1d1	procházející
vrcholkem	vrcholek	k1gInSc7	vrcholek
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
a	a	k8xC	a
středem	střed	k1gInSc7	střed
protilehlé	protilehlý	k2eAgFnSc2d1	protilehlá
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
půlící	půlící	k2eAgFnSc1d1	půlící
jeho	jeho	k3xOp3gFnSc4	jeho
plochu	plocha	k1gFnSc4	plocha
Marcela	Marcela	k1gFnSc1	Marcela
Palková	Palková	k1gFnSc1	Palková
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Průvodce	průvodce	k1gMnSc1	průvodce
matematikou	matematika	k1gFnSc7	matematika
2	[number]	k4	2
<g/>
,	,	kIx,	,
Didaktis	Didaktis	k1gFnSc1	Didaktis
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
7358	[number]	k4	7358
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
8-9	[number]	k4	8-9
Šárka	Šárka	k1gFnSc1	Šárka
Voráčová	Voráčová	k1gFnSc1	Voráčová
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
geometrie	geometrie	k1gFnSc2	geometrie
–	–	k?	–
Geometrie	geometrie	k1gFnSc1	geometrie
krásná	krásný	k2eAgFnSc1d1	krásná
a	a	k8xC	a
užitečná	užitečný	k2eAgFnSc1d1	užitečná
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1575	[number]	k4	1575
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
12	[number]	k4	12
Základní	základní	k2eAgInPc4d1	základní
geometrické	geometrický	k2eAgInPc4d1	geometrický
útvary	útvar	k1gInPc4	útvar
Lineární	lineární	k2eAgInPc4d1	lineární
geometrické	geometrický	k2eAgInPc4d1	geometrický
útvary	útvar	k1gInPc4	útvar
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
poloha	poloha	k1gFnSc1	poloha
přímky	přímka	k1gFnSc2	přímka
a	a	k8xC	a
kružnice	kružnice	k1gFnSc2	kružnice
Výpočet	výpočet	k1gInSc1	výpočet
průsečíku	průsečík	k1gInSc2	průsečík
křivek	křivka	k1gFnPc2	křivka
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
přímka	přímka	k1gFnSc1	přímka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
přímka	přímka	k1gFnSc1	přímka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
