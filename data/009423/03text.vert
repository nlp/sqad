<p>
<s>
Laurence	Laurence	k1gFnSc1	Laurence
van	vana	k1gFnPc2	vana
Cott	Cott	k2eAgInSc1d1	Cott
Niven	Niven	k1gInSc1	Niven
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
autor	autor	k1gMnSc1	autor
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
patrně	patrně	k6eAd1	patrně
nejznámějším	známý	k2eAgInSc7d3	nejznámější
románem	román	k1gInSc7	román
je	být	k5eAaImIp3nS	být
Prstenec	prstenec	k1gInSc1	prstenec
(	(	kIx(	(
<g/>
Ringworld	Ringworld	k1gInSc1	Ringworld
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
oceněný	oceněný	k2eAgInSc4d1	oceněný
Hugem	Hugo	k1gMnSc7	Hugo
<g/>
,	,	kIx,	,
Nebulou	nebula	k1gFnSc7	nebula
a	a	k8xC	a
Locusem	Locus	k1gInSc7	Locus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
z	z	k7c2	z
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
píše	psát	k5eAaImIp3nS	psát
často	často	k6eAd1	často
společně	společně	k6eAd1	společně
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
autory	autor	k1gMnPc7	autor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Jerrym	Jerrym	k1gInSc1	Jerrym
Pournellem	Pournell	k1gInSc7	Pournell
a	a	k8xC	a
Stevenem	Steven	k1gMnSc7	Steven
Barnesem	Barnes	k1gMnSc7	Barnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
otištěnou	otištěný	k2eAgFnSc7d1	otištěná
povídkou	povídka	k1gFnSc7	povídka
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
The	The	k1gMnSc1	The
Coldest	Coldest	k1gMnSc1	Coldest
Place	plac	k1gInSc6	plac
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
If	If	k1gFnSc2	If
o	o	k7c6	o
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Ironií	ironie	k1gFnSc7	ironie
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedlouho	dlouho	k6eNd1	dlouho
před	před	k7c7	před
jejím	její	k3xOp3gNnSc7	její
otištěním	otištění	k1gNnSc7	otištění
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
planeta	planeta	k1gFnSc1	planeta
žádnou	žádný	k3yNgFnSc4	žádný
trvale	trvale	k6eAd1	trvale
přivrácenou	přivrácený	k2eAgFnSc4d1	přivrácená
ani	ani	k8xC	ani
odvrácenou	odvrácený	k2eAgFnSc4d1	odvrácená
stranu	strana	k1gFnSc4	strana
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgInS	získat
prvního	první	k4xOgMnSc4	první
Huga	Hugo	k1gMnSc4	Hugo
za	za	k7c4	za
povídku	povídka	k1gFnSc4	povídka
Neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Niven	Niven	k1gInSc1	Niven
také	také	k9	také
napsal	napsat	k5eAaBmAgInS	napsat
několik	několik	k4yIc4	několik
scénářů	scénář	k1gInPc2	scénář
k	k	k7c3	k
televizním	televizní	k2eAgInPc3d1	televizní
seriálům	seriál	k1gInPc3	seriál
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Land	Landa	k1gFnPc2	Landa
of	of	k?	of
the	the	k?	the
Lost	Losta	k1gFnPc2	Losta
a	a	k8xC	a
animovaného	animovaný	k2eAgInSc2d1	animovaný
Star	Star	kA	Star
Treku	Trek	k1gInSc2	Trek
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
Inconstant	Inconstant	k1gMnSc1	Inconstant
Moon	Moon	k1gMnSc1	Moon
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
adaptována	adaptovat	k5eAaBmNgFnS	adaptovat
pro	pro	k7c4	pro
seriál	seriál	k1gInSc4	seriál
Krajní	krajní	k2eAgFnSc2d1	krajní
meze	mez	k1gFnSc2	mez
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Outer	Outer	k1gMnSc1	Outer
Limits	Limits	k1gInSc1	Limits
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
jeho	jeho	k3xOp3gInPc1	jeho
příběhy	příběh	k1gInPc1	příběh
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
ve	v	k7c6	v
"	"	kIx"	"
<g/>
Známém	známý	k2eAgInSc6d1	známý
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
lidstvo	lidstvo	k1gNnSc1	lidstvo
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tuctem	tucet	k1gInSc7	tucet
mimozemských	mimozemský	k2eAgInPc2d1	mimozemský
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Kzintů	Kzint	k1gMnPc2	Kzint
a	a	k8xC	a
Loutkářů	loutkář	k1gMnPc2	loutkář
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
celý	celý	k2eAgInSc1d1	celý
cyklus	cyklus	k1gInSc1	cyklus
Prstencového	prstencový	k2eAgInSc2d1	prstencový
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
vesmíru	vesmír	k1gInSc6	vesmír
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Příběhy	příběh	k1gInPc7	příběh
ze	z	k7c2	z
Známého	známý	k2eAgInSc2d1	známý
vesmíru	vesmír	k1gInSc2	vesmír
===	===	k?	===
</s>
</p>
<p>
<s>
Svět	svět	k1gInSc1	svět
Ptavvů	Ptavv	k1gMnPc2	Ptavv
<g/>
,	,	kIx,	,
KJV	KJV	kA	KJV
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
World	Worlda	k1gFnPc2	Worlda
of	of	k?	of
Ptavvs	Ptavvsa	k1gFnPc2	Ptavvsa
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
Gift	Gift	k1gMnSc1	Gift
From	From	k1gMnSc1	From
Earth	Earth	k1gMnSc1	Earth
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
</s>
</p>
<p>
<s>
Neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86390-15-2	[number]	k4	80-86390-15-2
(	(	kIx(	(
<g/>
Neutron	neutron	k1gInSc1	neutron
Star	Star	kA	Star
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
All	All	k?	All
the	the	k?	the
Myriad	Myriad	k1gInSc1	Myriad
Ways	Ways	k1gInSc1	Ways
<g/>
,	,	kIx,	,
,	,	kIx,	,
1971	[number]	k4	1971
</s>
</p>
<p>
<s>
Protektor	protektor	k1gInSc1	protektor
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Protector	Protector	k1gInSc1	Protector
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Tales	Tales	k1gMnSc1	Tales
of	of	k?	of
Known	Known	k1gMnSc1	Known
Space	Space	k1gMnSc1	Space
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Universe	Universe	k1gFnSc2	Universe
of	of	k?	of
Larry	Larra	k1gFnSc2	Larra
Niven	Nivna	k1gFnPc2	Nivna
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
The	The	k?	The
Long	Long	k1gMnSc1	Long
ARM	ARM	kA	ARM
of	of	k?	of
Gil	Gil	k1gFnSc1	Gil
Hamilton	Hamilton	k1gInSc1	Hamilton
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Convergent	Convergent	k1gMnSc1	Convergent
Series	Series	k1gMnSc1	Series
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
The	The	k?	The
Patchwork	Patchwork	k1gInSc1	Patchwork
Girl	girl	k1gFnSc1	girl
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
Flotila	flotila	k1gFnSc1	flotila
Světů	svět	k1gInPc2	svět
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
Edward	Edward	k1gMnSc1	Edward
M.	M.	kA	M.
Lerner	Lerner	k1gMnSc1	Lerner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Baronet	baronet	k1gMnSc1	baronet
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7384-213-0	[number]	k4	978-80-7384-213-0
(	(	kIx(	(
<g/>
Fleet	Fleeta	k1gFnPc2	Fleeta
of	of	k?	of
Worlds	Worldsa	k1gFnPc2	Worldsa
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
zákulisí	zákulisí	k1gNnSc6	zákulisí
světů	svět	k1gInPc2	svět
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
Edward	Edward	k1gMnSc1	Edward
M.	M.	kA	M.
Lerner	Lerner	k1gMnSc1	Lerner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Baronet	baronet	k1gMnSc1	baronet
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Juggler	Juggler	k1gInSc1	Juggler
of	of	k?	of
Worlds	Worlds	k1gInSc1	Worlds
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Destroyer	Destroyer	k1gInSc1	Destroyer
of	of	k?	of
Worlds	Worlds	k1gInSc1	Worlds
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
Edward	Edward	k1gMnSc1	Edward
M	M	kA	M
Lerner	Lerner	k1gMnSc1	Lerner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Betrayer	Betrayer	k1gInSc1	Betrayer
of	of	k?	of
Worlds	Worlds	k1gInSc1	Worlds
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
Edward	Edward	k1gMnSc1	Edward
M	M	kA	M
Lerner	Lerner	k1gMnSc1	Lerner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
===	===	k?	===
Prstencový	prstencový	k2eAgInSc1d1	prstencový
svět	svět	k1gInSc1	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Prstenec	prstenec	k1gInSc1	prstenec
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7214	[number]	k4	7214
<g/>
-	-	kIx~	-
<g/>
264	[number]	k4	264
<g/>
-X	-X	k?	-X
<g/>
,	,	kIx,	,
Baronet	baronet	k1gMnSc1	baronet
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
Ringworld	Ringworld	k1gMnSc1	Ringworld
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stavitelé	stavitel	k1gMnPc1	stavitel
prstence	prstenec	k1gInSc2	prstenec
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7214	[number]	k4	7214
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Baronet	baronet	k1gMnSc1	baronet
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
The	The	k1gFnSc6	The
Ringworld	Ringworlda	k1gFnPc2	Ringworlda
Engineers	Engineersa	k1gFnPc2	Engineersa
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pánové	pán	k1gMnPc1	pán
prstence	prstenec	k1gInSc2	prstenec
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7214	[number]	k4	7214
<g/>
-	-	kIx~	-
<g/>
355	[number]	k4	355
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
Baronet	baronet	k1gMnSc1	baronet
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Ringworld	Ringworld	k1gMnSc1	Ringworld
Throne	Thron	k1gInSc5	Thron
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
prstence	prstenec	k1gInSc2	prstenec
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7214	[number]	k4	7214
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
Baronet	baronet	k1gMnSc1	baronet
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Ringworld	Ringworld	k1gInSc1	Ringworld
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Children	Childrna	k1gFnPc2	Childrna
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Války	válka	k1gFnPc1	válka
s	s	k7c7	s
Kzinty	Kzint	k1gMnPc7	Kzint
===	===	k?	===
</s>
</p>
<p>
<s>
Války	válka	k1gFnPc1	válka
s	s	k7c7	s
Kzinty	Kzint	k1gMnPc7	Kzint
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86390	[number]	k4	86390
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
Man-Kzin	Man-Kzin	k2eAgInSc1d1	Man-Kzin
Wars	Wars	k1gInSc1	Wars
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Války	válka	k1gFnPc1	válka
s	s	k7c7	s
Kzinty	Kzint	k1gMnPc7	Kzint
II	II	kA	II
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
Man-Kzin	Man-Kzin	k2eAgInSc1d1	Man-Kzin
Wars	Wars	k1gInSc1	Wars
II	II	kA	II
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Války	válka	k1gFnPc1	válka
s	s	k7c7	s
Kzinty	Kzint	k1gMnPc7	Kzint
III	III	kA	III
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
Man-Kzin	Man-Kzin	k2eAgInSc1d1	Man-Kzin
Wars	Wars	k1gInSc1	Wars
III	III	kA	III
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Války	válka	k1gFnPc1	válka
s	s	k7c7	s
Kzinty	Kzint	k1gMnPc7	Kzint
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
(	(	kIx(	(
<g/>
Man-Kzin	Man-Kzin	k2eAgInSc1d1	Man-Kzin
Wars	Wars	k1gInSc1	Wars
IV	IV	kA	IV
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Man-Kzin	Man-Kzin	k2eAgInSc1d1	Man-Kzin
Wars	Wars	k1gInSc1	Wars
V	V	kA	V
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Man-Kzin	Man-Kzin	k2eAgInSc1d1	Man-Kzin
Wars	Wars	k1gInSc1	Wars
VI	VI	kA	VI
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Man-Kzin	Man-Kzin	k2eAgInSc1d1	Man-Kzin
Wars	Wars	k1gInSc1	Wars
VII	VII	kA	VII
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Man	Man	k1gMnSc1	Man
Kzin	Kzin	k1gMnSc1	Kzin
Wars	Warsa	k1gFnPc2	Warsa
VIII	VIII	kA	VIII
<g/>
:	:	kIx,	:
Choosing	Choosing	k1gInSc1	Choosing
Names	Names	k1gInSc1	Names
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Man-Kzin	Man-Kzin	k2eAgInSc1d1	Man-Kzin
Wars	Wars	k1gInSc1	Wars
IX	IX	kA	IX
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Man-Kzin	Man-Kzin	k2eAgInSc1d1	Man-Kzin
Wars	Wars	k1gInSc1	Wars
X	X	kA	X
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Wunder	Wunder	k1gMnSc1	Wunder
War	War	k1gMnSc1	War
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Man-Kzin	Man-Kzin	k2eAgInSc1d1	Man-Kzin
Wars	Wars	k1gInSc1	Wars
XI	XI	kA	XI
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Man-Kzin	Man-Kzin	k2eAgInSc1d1	Man-Kzin
Wars	Wars	k1gInSc1	Wars
XII	XII	kA	XII
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Kondominium	kondominium	k1gNnSc1	kondominium
===	===	k?	===
</s>
</p>
<p>
<s>
Spoluautor	spoluautor	k1gMnSc1	spoluautor
Jerry	Jerra	k1gFnSc2	Jerra
Pournelle	Pournelle	k1gFnSc2	Pournelle
</s>
</p>
<p>
<s>
Tříska	Tříska	k1gMnSc1	Tříska
v	v	k7c6	v
božím	boží	k2eAgNnSc6d1	boží
oku	oko	k1gNnSc6	oko
<g/>
:	:	kIx,	:
Džin	džin	k1gMnSc1	džin
v	v	k7c6	v
lahvi	lahev	k1gFnSc6	lahev
<g/>
,	,	kIx,	,
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86139-08-5	[number]	k4	80-86139-08-5
</s>
</p>
<p>
<s>
Tříska	Tříska	k1gMnSc1	Tříska
v	v	k7c6	v
božím	boží	k2eAgNnSc6d1	boží
oku	oko	k1gNnSc6	oko
<g/>
:	:	kIx,	:
Přízračný	přízračný	k2eAgInSc1d1	přízračný
křižník	křižník	k1gInSc1	křižník
<g/>
,	,	kIx,	,
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86139-11-5	[number]	k4	80-86139-11-5
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Mote	Mot	k1gMnSc2	Mot
in	in	k?	in
God	God	k1gMnSc2	God
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Eye	Eye	k1gFnSc7	Eye
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Drtící	drtící	k2eAgFnSc1d1	drtící
ruka	ruka	k1gFnSc1	ruka
<g/>
,	,	kIx,	,
Banshies	Banshies	k1gInSc1	Banshies
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86456-19-6	[number]	k4	80-86456-19-6
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Gripping	Gripping	k1gInSc1	Gripping
Hand	Hand	k1gInSc1	Hand
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dream	Dream	k1gInSc1	Dream
Park	park	k1gInSc1	park
===	===	k?	===
</s>
</p>
<p>
<s>
Spoluautor	spoluautor	k1gMnSc1	spoluautor
Steven	Stevna	k1gFnPc2	Stevna
Barnes	Barnes	k1gMnSc1	Barnes
</s>
</p>
<p>
<s>
Dream	Dream	k6eAd1	Dream
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
The	The	k?	The
Barsoom	Barsoom	k1gInSc1	Barsoom
Project	Projecta	k1gFnPc2	Projecta
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Dream	Dream	k1gInSc1	Dream
Park	park	k1gInSc1	park
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Voodoo	Voodoo	k1gMnSc1	Voodoo
Game	game	k1gInSc1	game
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
===	===	k?	===
Heorot	Heorot	k1gInSc4	Heorot
===	===	k?	===
</s>
</p>
<p>
<s>
Spoluautoři	spoluautor	k1gMnPc1	spoluautor
<g/>
:	:	kIx,	:
Jerry	Jerr	k1gInPc1	Jerr
Pournelle	Pournelle	k1gNnPc2	Pournelle
a	a	k8xC	a
Steven	Steven	k2eAgInSc1d1	Steven
Barnes	Barnes	k1gInSc1	Barnes
</s>
</p>
<p>
<s>
Odkaz	odkaz	k1gInSc1	odkaz
Heorotu	Heorot	k1gInSc2	Heorot
<g/>
,	,	kIx,	,
Banshies	Banshies	k1gMnSc1	Banshies
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86456-00-5	[number]	k4	80-86456-00-5
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Legacy	Legaca	k1gFnSc2	Legaca
of	of	k?	of
Heorot	Heorot	k1gMnSc1	Heorot
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Draci	drak	k1gMnPc1	drak
Heorotu	Heorot	k1gInSc2	Heorot
<g/>
,	,	kIx,	,
Banshies	Banshies	k1gMnSc1	Banshies
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86456-06-4	[number]	k4	80-86456-06-4
(	(	kIx(	(
<g/>
Beowulf	Beowulf	k1gInSc1	Beowulf
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Children	Childrna	k1gFnPc2	Childrna
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
také	také	k9	také
jako	jako	k9	jako
The	The	k1gFnPc1	The
Dragons	Dragons	k1gInSc1	Dragons
of	of	k?	of
Heorot	Heorot	k1gInSc1	Heorot
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
Heorotu	Heorot	k1gInSc2	Heorot
<g/>
,	,	kIx,	,
Banshies	Banshies	k1gMnSc1	Banshies
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-86456-07-2	[number]	k4	80-86456-07-2
(	(	kIx(	(
<g/>
Beowulf	Beowulf	k1gInSc1	Beowulf
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Children	Childrna	k1gFnPc2	Childrna
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgInPc1d1	další
romány	román	k1gInPc1	román
===	===	k?	===
</s>
</p>
<p>
<s>
Létající	létající	k2eAgMnPc1d1	létající
čarodějové	čaroděj	k1gMnPc1	čaroděj
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
David	David	k1gMnSc1	David
Gerrold	Gerrold	k1gMnSc1	Gerrold
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Classic	Classic	k1gMnSc1	Classic
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Flying	Flying	k1gInSc1	Flying
Sorcerers	Sorcerers	k1gInSc1	Sorcerers
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Inferno	inferno	k1gNnSc1	inferno
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
–	–	k?	–
spoluautor	spoluautor	k1gMnSc1	spoluautor
Jerry	Jerra	k1gMnSc2	Jerra
Pournelle	Pournelle	k1gNnSc2	Pournelle
</s>
</p>
<p>
<s>
Children	Childrno	k1gNnPc2	Childrno
of	of	k?	of
the	the	k?	the
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Luciferovo	Luciferův	k2eAgNnSc1d1	Luciferův
Kladivo	kladivo	k1gNnSc1	kladivo
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
Jerry	Jerra	k1gMnSc2	Jerra
Pournelle	Pournelle	k1gNnSc2	Pournelle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Banshies	Banshies	k1gMnSc1	Banshies
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
(	(	kIx(	(
<g/>
Lucifer	Lucifer	k1gMnSc1	Lucifer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hammer	Hammer	k1gInSc1	Hammer
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Magic	Magic	k1gMnSc1	Magic
Goes	Goes	k1gInSc4	Goes
Away	Awaa	k1gFnSc2	Awaa
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
</s>
</p>
<p>
<s>
Oath	Oath	k1gMnSc1	Oath
of	of	k?	of
Fealty	Fealt	k1gInPc1	Fealt
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
–	–	k?	–
spoluautor	spoluautor	k1gMnSc1	spoluautor
Jerry	Jerra	k1gMnSc2	Jerra
Pournelle	Pournelle	k1gNnSc2	Pournelle
</s>
</p>
<p>
<s>
The	The	k?	The
Flight	Flight	k1gInSc1	Flight
of	of	k?	of
the	the	k?	the
Horse	Horse	k1gFnSc2	Horse
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
The	The	k?	The
Descent	Descent	k1gInSc1	Descent
of	of	k?	of
Anansi	Ananse	k1gFnSc4	Ananse
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
–	–	k?	–
spoluautor	spoluautor	k1gMnSc1	spoluautor
Steven	Steven	k2eAgMnSc1d1	Steven
Barnes	Barnes	k1gMnSc1	Barnes
</s>
</p>
<p>
<s>
Dream	Dream	k6eAd1	Dream
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
</s>
</p>
<p>
<s>
Footfall	Footfall	k1gMnSc1	Footfall
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
–	–	k?	–
spoluautor	spoluautor	k1gMnSc1	spoluautor
Jerry	Jerra	k1gMnSc2	Jerra
Pournelle	Pournelle	k1gNnSc2	Pournelle
</s>
</p>
<p>
<s>
Limits	Limits	k6eAd1	Limits
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
Gift	Gift	k2eAgMnSc1d1	Gift
from	from	k1gMnSc1	from
Earth	Earth	k1gMnSc1	Earth
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
N-Space	N-Space	k1gFnSc1	N-Space
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
–	–	k?	–
spoluautor	spoluautor	k1gMnSc1	spoluautor
Tom	Tom	k1gMnSc1	Tom
Clancy	Clanca	k1gFnSc2	Clanca
</s>
</p>
<p>
<s>
Fallen	Fallen	k2eAgInSc1d1	Fallen
Angels	Angels	k1gInSc1	Angels
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
–	–	k?	–
spoluautor	spoluautor	k1gMnSc1	spoluautor
Jerry	Jerra	k1gMnSc2	Jerra
Pournelle	Pournelle	k1gFnSc2	Pournelle
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
F.	F.	kA	F.
Flynn	Flynn	k1gMnSc1	Flynn
</s>
</p>
<p>
<s>
Achilles	Achilles	k1gMnSc1	Achilles
<g/>
'	'	kIx"	'
Choice	Choice	k1gFnSc1	Choice
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
–	–	k?	–
spoluautor	spoluautor	k1gMnSc1	spoluautor
Steven	Steven	k2eAgMnSc1d1	Steven
Barnes	Barnes	k1gMnSc1	Barnes
</s>
</p>
<p>
<s>
Convergent	Convergent	k1gMnSc1	Convergent
Series	Series	k1gMnSc1	Series
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Playgrounds	Playgrounds	k1gInSc1	Playgrounds
of	of	k?	of
the	the	k?	the
Mind	Mind	k1gMnSc1	Mind
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
The	The	k?	The
Magic	Magic	k1gMnSc1	Magic
May	May	k1gMnSc1	May
Return	Return	k1gMnSc1	Return
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
–	–	k?	–
spoluautorka	spoluautorka	k1gFnSc1	spoluautorka
Alicia	Alicia	k1gFnSc1	Alicia
Austinová	Austinový	k2eAgFnSc1d1	Austinová
</s>
</p>
<p>
<s>
Destiny	Destina	k1gFnPc1	Destina
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Road	Road	k1gInSc1	Road
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Saturn	Saturn	k1gInSc1	Saturn
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Race	Race	k1gFnSc7	Race
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
–	–	k?	–
spoluautor	spoluautor	k1gMnSc1	spoluautor
Steven	Steven	k2eAgMnSc1d1	Steven
Barnes	Barnes	k1gMnSc1	Barnes
</s>
</p>
<p>
<s>
The	The	k?	The
Burning	Burning	k1gInSc1	Burning
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
–	–	k?	–
spoluautor	spoluautor	k1gMnSc1	spoluautor
Jerry	Jerra	k1gMnSc2	Jerra
Pournelle	Pournelle	k1gNnSc2	Pournelle
</s>
</p>
<p>
<s>
Scatterbrain	Scatterbrain	k1gMnSc1	Scatterbrain
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Burning	Burning	k1gInSc1	Burning
Tower	Towra	k1gFnPc2	Towra
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
–	–	k?	–
spoluautor	spoluautor	k1gMnSc1	spoluautor
Jerry	Jerra	k1gMnSc2	Jerra
Pournelle	Pournelle	k1gNnSc2	Pournelle
</s>
</p>
<p>
<s>
Building	Building	k1gInSc1	Building
Harlequin	Harlequin	k1gInSc1	Harlequin
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Moon	Moon	k1gInSc1	Moon
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
–	–	k?	–
spoluautorka	spoluautorka	k1gFnSc1	spoluautorka
Brenda	Brenda	k1gFnSc1	Brenda
Cooperová	Cooperová	k1gFnSc1	Cooperová
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
cena	cena	k1gFnSc1	cena
Nebula	nebula	k1gFnSc1	nebula
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
román	román	k1gInSc1	román
–	–	k?	–
Prstenec	prstenec	k1gInSc1	prstenec
<g/>
5	[number]	k4	5
cen	cena	k1gFnPc2	cena
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
–	–	k?	–
povídka	povídka	k1gFnSc1	povídka
–	–	k?	–
Neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
–	–	k?	–
román	román	k1gInSc1	román
–	–	k?	–
Prstenec	prstenec	k1gInSc1	prstenec
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
–	–	k?	–
povídka	povídka	k1gFnSc1	povídka
–	–	k?	–
Nestálý	stálý	k2eNgInSc4d1	nestálý
měsíc	měsíc	k1gInSc4	měsíc
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
–	–	k?	–
povídka	povídka	k1gFnSc1	povídka
–	–	k?	–
Hledač	hledač	k1gInSc1	hledač
temných	temný	k2eAgFnPc2d1	temná
hvězd	hvězda	k1gFnPc2	hvězda
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
–	–	k?	–
povídka	povídka	k1gFnSc1	povídka
–	–	k?	–
The	The	k1gFnSc1	The
Borderland	Borderland	k1gInSc1	Borderland
of	of	k?	of
Sol	sol	k1gInSc1	sol
<g/>
4	[number]	k4	4
ceny	cena	k1gFnSc2	cena
Locus	Locus	k1gMnSc1	Locus
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
román	román	k1gInSc1	román
–	–	k?	–
Prstenec	prstenec	k1gInSc1	prstenec
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
–	–	k?	–
The	The	k1gMnSc1	The
Convergent	Convergent	k1gMnSc1	Convergent
Series	Series	k1gMnSc1	Series
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
román	román	k1gInSc1	román
–	–	k?	–
The	The	k1gFnSc2	The
Integral	Integral	k1gMnSc1	Integral
Trees	Trees	k1gMnSc1	Trees
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
povídka	povídka	k1gFnSc1	povídka
–	–	k?	–
The	The	k1gFnSc1	The
Missing	Missing	k1gInSc1	Missing
Mass	Mass	k1gInSc1	Mass
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
Forry	Forro	k1gNnPc7	Forro
Award	Awarda	k1gFnPc2	Awarda
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Larry	Larra	k1gFnSc2	Larra
Niven	Niven	k1gInSc1	Niven
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Larry	Larr	k1gInPc7	Larr
Niven	Nivna	k1gFnPc2	Nivna
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Known	Known	k1gMnSc1	Known
Space	Space	k1gMnSc1	Space
-	-	kIx~	-
The	The	k1gMnSc5	The
Future	Futur	k1gMnSc5	Futur
Worlds	Worlds	k1gInSc4	Worlds
of	of	k?	of
Larry	Larra	k1gFnSc2	Larra
Niven	Nivna	k1gFnPc2	Nivna
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
His	his	k1gNnSc7	his
Own	Own	k1gFnSc2	Own
Biography	Biographa	k1gFnSc2	Biographa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bibliografie	bibliografie	k1gFnSc1	bibliografie
na	na	k7c4	na
SciFan	SciFan	k1gInSc4	SciFan
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bibliografie	bibliografie	k1gFnSc1	bibliografie
na	na	k7c4	na
ISFDB	ISFDB	kA	ISFDB
</s>
</p>
