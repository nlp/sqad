<s>
Laurence	Laurence	k1gFnSc1	Laurence
van	vana	k1gFnPc2	vana
Cott	Cott	k2eAgInSc1d1	Cott
Niven	Niven	k1gInSc1	Niven
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
autor	autor	k1gMnSc1	autor
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
patrně	patrně	k6eAd1	patrně
nejznámějším	známý	k2eAgInSc7d3	nejznámější
románem	román	k1gInSc7	román
je	být	k5eAaImIp3nS	být
Prstenec	prstenec	k1gInSc1	prstenec
(	(	kIx(	(
<g/>
Ringworld	Ringworld	k1gInSc1	Ringworld
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
oceněný	oceněný	k2eAgInSc4d1	oceněný
Hugem	Hugo	k1gMnSc7	Hugo
<g/>
,	,	kIx,	,
Nebulou	nebula	k1gFnSc7	nebula
a	a	k8xC	a
Locusem	Locus	k1gInSc7	Locus
<g/>
.	.	kIx.	.
</s>
