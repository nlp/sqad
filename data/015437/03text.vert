<s>
The	The	k?
Pagan	Pagan	k1gInSc1
Prosperity	prosperita	k1gFnSc2
</s>
<s>
The	The	k?
Pagan	Pagan	k1gMnSc1
ProsperityInterpretOld	ProsperityInterpretOld	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
ChildDruh	ChildDruh	k1gInSc1
albaStudiové	albaStudiový	k2eAgFnPc1d1
albumVydánosrpen	albumVydánosrpen	k2eAgInSc4d1
1997	#num#	k4
<g/>
Nahránočerven	Nahránočerven	k2eAgInSc4d1
1997	#num#	k4
ve	v	k7c6
studiu	studio	k1gNnSc6
StudiomegaŽánrsymfonický	StudiomegaŽánrsymfonický	k2eAgInSc1d1
black	black	k1gInSc1
metalDélka	metalDélka	k1gFnSc1
<g/>
36	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
VydavatelstvíCentury	VydavatelstvíCentura	k1gFnSc2
Media	medium	k1gNnSc2
RecordsProducentOld	RecordsProducentOld	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
ChildProfesionální	ChildProfesionální	k2eAgFnSc7d1
kritika	kritik	k1gMnSc4
</s>
<s>
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Old	Olda	k1gFnPc2
Man	mana	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Child	Child	k1gInSc1
chronologicky	chronologicky	k6eAd1
</s>
<s>
Born	Born	k1gInSc1
of	of	k?
the	the	k?
Flickering	Flickering	k1gInSc1
<g/>
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Pagan	Pagan	k1gInSc1
Prosperity	prosperita	k1gFnSc2
<g/>
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ill-Natured	Ill-Natured	k1gInSc1
Spiritual	Spiritual	k1gInSc1
Invasion	Invasion	k1gInSc1
<g/>
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Pagan	Pagan	k1gInSc1
Prosperity	Prosperity	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
překladu	překlad	k1gInSc6
Pohanská	pohanský	k2eAgFnSc1d1
prosperita	prosperita	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druhé	druhý	k4xOgNnSc1
studiové	studiový	k2eAgNnSc1d1
album	album	k1gNnSc1
norské	norský	k2eAgFnSc2d1
black	black	k6eAd1
metalové	metalový	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Old	Olda	k1gFnPc2
Man	Man	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Child	Child	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
nahráno	nahrát	k5eAaBmNgNnS,k5eAaPmNgNnS
v	v	k7c6
červnu	červen	k1gInSc6
1997	#num#	k4
ve	v	k7c6
studiu	studio	k1gNnSc6
Studiomega	Studiomega	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
"	"	kIx"
<g/>
The	The	k1gFnSc6
Millennium	millennium	k1gNnSc4
King	Kinga	k1gFnPc2
<g/>
"	"	kIx"
–	–	k?
5	#num#	k4
<g/>
:	:	kIx,
<g/>
28	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Behind	Behind	k1gInSc1
the	the	k?
Mask	Mask	k1gInSc1
<g/>
"	"	kIx"
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
58	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Soul	Soul	k1gInSc1
Possessed	Possessed	k1gInSc1
<g/>
"	"	kIx"
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
My	my	k3xPp1nPc1
Demonic	Demonice	k1gFnPc2
Figures	Figures	k1gInSc1
<g/>
"	"	kIx"
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
59	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Doommaker	Doommakero	k1gNnPc2
<g/>
"	"	kIx"
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
39	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
My	my	k3xPp1nPc1
Kingdom	Kingdom	k1gInSc4
Will	Will	k1gInSc1
Come	Come	k1gFnSc1
<g/>
"	"	kIx"
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
35	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
Return	Return	k1gInSc1
of	of	k?
the	the	k?
Night	Night	k1gMnSc1
Creatures	Creatures	k1gMnSc1
<g/>
"	"	kIx"
–	–	k?
5	#num#	k4
<g/>
:	:	kIx,
<g/>
36	#num#	k4
</s>
<s>
"	"	kIx"
<g/>
What	What	k1gInSc1
Malice	Malice	k1gFnSc2
Embrace	Embrace	k1gFnSc2
<g/>
"	"	kIx"
–	–	k?
5	#num#	k4
<g/>
:	:	kIx,
<g/>
13	#num#	k4
</s>
<s>
Sestava	sestava	k1gFnSc1
</s>
<s>
Galder	Galder	k1gMnSc1
–	–	k?
vokály	vokál	k1gInPc1
<g/>
,	,	kIx,
kytary	kytara	k1gFnPc1
<g/>
,	,	kIx,
klávesy	klávesa	k1gFnPc1
</s>
<s>
Jardar	Jardar	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Frode	Frode	k6eAd1
"	"	kIx"
<g/>
Gonde	Gond	k1gInSc5
<g/>
"	"	kIx"
Forsmo	Forsma	k1gFnSc5
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Tony	Tony	k1gMnSc1
Kirkemo	Kirkema	k1gFnSc5
–	–	k?
bicí	bicí	k2eAgMnSc1d1
</s>
<s>
J.	J.	kA
Lohngrin	Lohngrin	k1gInSc1
Cremonese	Cremonese	k1gFnSc1
-	-	kIx~
hostující	hostující	k2eAgInPc1d1
vokály	vokál	k1gInPc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gMnSc1
Pagan	Pagan	k1gMnSc1
Prosperity	prosperita	k1gFnSc2
na	na	k7c6
Allmusic	Allmusic	k1gMnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Album	album	k1gNnSc1
na	na	k7c6
webu	web	k1gInSc6
Encyclopaedia	Encyclopaedium	k1gNnSc2
Metallum	Metallum	k1gNnSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Album	album	k1gNnSc1
na	na	k7c6
webu	web	k1gInSc6
Discogs	Discogsa	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Old	Olda	k1gFnPc2
Man	mana	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Child	Child	k1gInSc1
Současná	současný	k2eAgFnSc1d1
sestava	sestava	k1gFnSc1
<g/>
:	:	kIx,
Galdeřívější	Galdeřívý	k2eAgMnPc1d2
členové	člen	k1gMnPc1
<g/>
:	:	kIx,
Jardar	Jardar	k1gMnSc1
•	•	k?
Tjodalv	Tjodalv	k1gMnSc1
•	•	k?
Gonde	Gond	k1gInSc5
•	•	k?
Brynjard	Brynjard	k1gInSc1
Tristan	Tristan	k1gInSc1
•	•	k?
Stian	Stian	k1gInSc1
Aarstad	Aarstad	k1gInSc1
Studiová	studiový	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
Born	Born	k1gInSc1
of	of	k?
the	the	k?
Flickering	Flickering	k1gInSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gMnSc4
Pagan	Pagan	k1gMnSc1
Prosperity	prosperita	k1gFnSc2
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ill-Natured	Ill-Natured	k1gInSc1
Spiritual	Spiritual	k1gMnSc1
Invasion	Invasion	k1gInSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Revelation	Revelation	k1gInSc1
666	#num#	k4
–	–	k?
The	The	k1gMnSc1
Curse	Curse	k1gFnSc2
of	of	k?
Damnation	Damnation	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
In	In	k1gFnSc1
Defiance	Defiance	k1gFnSc1
of	of	k?
Existence	existence	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vermin	Vermin	k1gInSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slaves	Slaves	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
Demo	demo	k2eAgFnSc2d1
</s>
<s>
In	In	k?
the	the	k?
Shades	Shades	k1gInSc1
of	of	k?
Life	Life	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
Split	Split	k1gInSc1
</s>
<s>
Sons	Sons	k1gInSc1
of	of	k?
Satan	satan	k1gInSc1
Gather	Gathra	k1gFnPc2
for	forum	k1gNnPc2
Attack	Attack	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Kompilace	kompilace	k1gFnSc1
</s>
<s>
The	The	k?
Historical	Historical	k1gFnSc1
Plague	Plague	k1gFnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
