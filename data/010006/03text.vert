<p>
<s>
Račí	račí	k2eAgFnSc1d1	račí
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
Volyně	Volyně	k1gFnSc2	Volyně
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Strakonice	Strakonice	k1gFnPc1	Strakonice
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
2,5	[number]	k4	2,5
km	km	kA	km
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
od	od	k7c2	od
Volyně	Volyně	k1gFnSc2	Volyně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
evidováno	evidovat	k5eAaImNgNnS	evidovat
13	[number]	k4	13
adres	adresa	k1gFnPc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
trvale	trvale	k6eAd1	trvale
žilo	žít	k5eAaImAgNnS	žít
24	[number]	k4	24
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
<g/>
Račí	račí	k2eAgFnSc1d1	račí
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Račí	račí	k2eAgFnSc2d1	račí
u	u	k7c2	u
Nišovic	Nišovice	k1gFnPc2	Nišovice
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
1,7	[number]	k4	1,7
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
Račí	račí	k2eAgMnSc1d1	račí
je	být	k5eAaImIp3nS	být
mj.	mj.	kA	mj.
situována	situován	k2eAgFnSc1d1	situována
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Nišovice	Nišovice	k1gFnSc2	Nišovice
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
198	[number]	k4	198
Strakonice	Strakonice	k1gFnPc4	Strakonice
<g/>
–	–	k?	–
<g/>
Volary	Volara	k1gFnSc2	Volara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místní	místní	k2eAgFnPc1d1	místní
části	část	k1gFnPc1	část
Černětice	Černětice	k1gFnSc1	Černětice
a	a	k8xC	a
Račí	račí	k2eAgFnSc1d1	račí
leží	ležet	k5eAaImIp3nS	ležet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
vlastní	vlastní	k2eAgFnSc2d1	vlastní
Volyně	Volyně	k1gFnSc2	Volyně
a	a	k8xC	a
společně	společně	k6eAd1	společně
tvoří	tvořit	k5eAaImIp3nP	tvořit
exklávu	exkláva	k1gFnSc4	exkláva
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
odděleny	oddělit	k5eAaPmNgInP	oddělit
obcí	obec	k1gFnSc7	obec
Nišovice	Nišovice	k1gFnSc2	Nišovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vesnici	vesnice	k1gFnSc6	vesnice
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Kaple	kaple	k1gFnSc1	kaple
na	na	k7c6	na
návsi	náves	k1gFnSc6	náves
</s>
</p>
<p>
<s>
Usedlost	usedlost	k1gFnSc1	usedlost
čp.	čp.	k?	čp.
1	[number]	k4	1
(	(	kIx(	(
<g/>
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Račí	račí	k2eAgFnPc4d1	račí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Račí	račí	k2eAgNnSc1d1	račí
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
katastru	katastr	k1gInSc2	katastr
Račí	račí	k2eAgFnSc1d1	račí
u	u	k7c2	u
Nišovic	Nišovice	k1gFnPc2	Nišovice
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
</s>
</p>
