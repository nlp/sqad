<s>
Kyselina	kyselina	k1gFnSc1
vinylfosfonová	vinylfosfonová	k1gFnSc1
</s>
<s>
Kyselina	kyselina	k1gFnSc1
vinylfosfonová	vinylfosfonová	k1gFnSc1
</s>
<s>
Strukturní	strukturní	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
kyselina	kyselina	k1gFnSc1
ethenylfosfonová	ethenylfosfonová	k1gFnSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
C2H5O3P	C2H5O3P	k4
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
1746-03-8	1746-03-8	k4
</s>
<s>
PubChem	PubCh	k1gInSc7
</s>
<s>
168725	#num#	k4
</s>
<s>
SMILES	SMILES	kA
</s>
<s>
C	C	kA
<g/>
=	=	kIx~
<g/>
CP	CP	kA
<g/>
(	(	kIx(
<g/>
=	=	kIx~
<g/>
O	o	k7c4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
O	o	k7c4
<g/>
)	)	kIx)
<g/>
O	o	k7c6
</s>
<s>
InChI	InChI	k?
</s>
<s>
1	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
C	C	kA
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
5	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
P	P	kA
<g/>
/	/	kIx~
<g/>
c	c	k0
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
3,4	3,4	k4
<g/>
)	)	kIx)
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
,1	,1	k4
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
<g/>
(	(	kIx(
<g/>
H	H	kA
<g/>
2,3	2,3	k4
<g/>
,4	,4	k4
<g/>
,5	,5	k4
<g/>
)	)	kIx)
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
108,03	108,03	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
36	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
309	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
1,37	1,37	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
GHS	GHS	kA
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
H-věty	H-věta	k1gFnPc1
</s>
<s>
H314	H314	k4
H	H	kA
<g/>
318	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
P-věty	P-věta	k1gFnPc1
</s>
<s>
P260	P260	k4
P264	P264	k1gFnSc1
P280	P280	k1gFnSc2
P	P	kA
<g/>
301	#num#	k4
<g/>
+	+	kIx~
<g/>
330	#num#	k4
<g/>
+	+	kIx~
<g/>
331	#num#	k4
P	P	kA
<g/>
303	#num#	k4
<g/>
+	+	kIx~
<g/>
361	#num#	k4
<g/>
+	+	kIx~
<g/>
353	#num#	k4
P	P	kA
<g/>
304	#num#	k4
<g/>
+	+	kIx~
<g/>
340	#num#	k4
P	P	kA
<g/>
305	#num#	k4
<g/>
+	+	kIx~
<g/>
351	#num#	k4
<g/>
+	+	kIx~
<g/>
338	#num#	k4
P310	P310	k1gMnSc1
P321	P321	k1gMnSc1
P363	P363	k1gMnSc1
P405	P405	k1gMnSc1
P	P	kA
<g/>
501	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kyselina	kyselina	k1gFnSc1
vinylfosfonová	vinylfosfonový	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
organická	organický	k2eAgFnSc1d1
sloučenina	sloučenina	k1gFnSc1
patřící	patřící	k2eAgFnSc1d1
mezi	mezi	k7c4
fosfonové	fosfonová	k2eAgFnPc4d1
kyseliny	kyselina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čisté	čistý	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
je	být	k5eAaImIp3nS
bezbarvou	bezbarvý	k2eAgFnSc7d1
pevnou	pevný	k2eAgFnSc7d1
látkou	látka	k1gFnSc7
(	(	kIx(
<g/>
komerčně	komerčně	k6eAd1
prodávané	prodávaný	k2eAgInPc1d1
vzorky	vzorek	k1gInPc1
bývají	bývat	k5eAaImIp3nP
často	často	k6eAd1
nažloutlé	nažloutlý	k2eAgFnPc1d1
a	a	k8xC
kapalné	kapalný	k2eAgNnSc1d1
<g/>
)	)	kIx)
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
teplotou	teplota	k1gFnSc7
tání	tání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
výrobu	výroba	k1gFnSc4
polymerů	polymer	k1gInPc2
používaných	používaný	k2eAgInPc2d1
do	do	k7c2
lepidel	lepidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jako	jako	k9
jiné	jiný	k2eAgFnSc2d1
fosfonové	fosfonový	k2eAgFnSc2d1
kyseliny	kyselina	k1gFnSc2
má	mít	k5eAaImIp3nS
čtyřstěnnou	čtyřstěnný	k2eAgFnSc4d1
molekulu	molekula	k1gFnSc4
s	s	k7c7
centrem	centr	k1gInSc7
tvořeným	tvořený	k2eAgInSc7d1
atomem	atom	k1gInSc7
fosforu	fosfor	k1gInSc2
<g/>
;	;	kIx,
na	na	k7c4
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
je	být	k5eAaImIp3nS
navázána	navázán	k2eAgFnSc1d1
organická	organický	k2eAgFnSc1d1
funkční	funkční	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
(	(	kIx(
<g/>
zde	zde	k6eAd1
vinyl	vinyl	k1gInSc1
(	(	kIx(
<g/>
ethenyl	ethenyl	k1gInSc1
<g/>
))	))	k?
<g/>
,	,	kIx,
dále	daleko	k6eAd2
dva	dva	k4xCgInPc1
hydroxyly	hydroxyl	k1gInPc1
a	a	k8xC
jeden	jeden	k4xCgInSc1
atom	atom	k1gInSc1
kyslíku	kyslík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
</s>
<s>
Kyselinu	kyselina	k1gFnSc4
vinylfosfonovou	vinylfosfonová	k1gFnSc4
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
několika	několik	k4yIc7
způsoby	způsob	k1gInPc7
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
adice	adice	k1gFnSc1
chloridu	chlorid	k1gInSc2
fosforitého	fosforitý	k2eAgMnSc2d1
na	na	k7c4
acetaldehyd	acetaldehyd	k1gInSc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
PCl	PCl	k?
<g/>
3	#num#	k4
+	+	kIx~
CH3CHO	CH3CHO	k1gFnSc1
→	→	k?
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
CH	Ch	kA
<g/>
(	(	kIx(
<g/>
O	o	k7c6
<g/>
−	−	k?
<g/>
)	)	kIx)
<g/>
PCl	PCl	k1gFnSc1
+3	+3	k4
</s>
<s>
následovaná	následovaný	k2eAgFnSc1d1
reakcí	reakce	k1gFnSc7
vytvořeného	vytvořený	k2eAgInSc2d1
aduktu	adukt	k1gInSc2
s	s	k7c7
kyselinou	kyselina	k1gFnSc7
octovou	octový	k2eAgFnSc7d1
</s>
<s>
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
CH	Ch	kA
<g/>
(	(	kIx(
<g/>
O	o	k7c6
<g/>
−	−	k?
<g/>
)	)	kIx)
<g/>
PCl	PCl	k1gFnSc1
+3	+3	k4
+	+	kIx~
2	#num#	k4
CH3CO2H	CH3CO2H	k1gFnSc2
→	→	k?
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
CH	Ch	kA
<g/>
(	(	kIx(
<g/>
Cl	Cl	k1gMnSc1
<g/>
)	)	kIx)
<g/>
PO	Po	kA
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
+	+	kIx~
2	#num#	k4
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
COCl	COClum	k1gNnPc2
</s>
<s>
a	a	k8xC
dehydrochlorací	dehydrochlorace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
CH	Ch	kA
<g/>
3	#num#	k4
<g/>
CH	Ch	kA
<g/>
(	(	kIx(
<g/>
Cl	Cl	k1gMnSc1
<g/>
)	)	kIx)
<g/>
PO	Po	kA
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
→	→	k?
CH	Ch	kA
<g/>
2	#num#	k4
<g/>
=	=	kIx~
<g/>
CHPO	CHPO	kA
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
+	+	kIx~
HCl	HCl	k1gFnSc1
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Polymerizací	polymerizace	k1gFnSc7
kyseliny	kyselina	k1gFnSc2
vinylfosfonové	vinylfosfonová	k1gFnSc2
vznikají	vznikat	k5eAaImIp3nP
polyvinylfosfonové	polyvinylfosfonový	k2eAgFnPc1d1
kyseliny	kyselina	k1gFnPc1
<g/>
,	,	kIx,
používané	používaný	k2eAgInPc1d1
v	v	k7c6
lepidlech	lepidlo	k1gNnPc6
určených	určený	k2eAgInPc2d1
ke	k	k7c3
spojování	spojování	k1gNnSc3
organických	organický	k2eAgInPc2d1
a	a	k8xC
anorganických	anorganický	k2eAgInPc2d1
povrchů	povrch	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
mezi	mezi	k7c7
nátěry	nátěr	k1gInPc7
a	a	k8xC
natíranými	natíraný	k2eAgInPc7d1
povrchy	povrch	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Homopolymery	Homopolymer	k1gInPc1
i	i	k8xC
kopolymery	kopolymer	k1gInPc1
této	tento	k3xDgFnSc2
kyseliny	kyselina	k1gFnSc2
se	se	k3xPyFc4
také	také	k9
používají	používat	k5eAaImIp3nP
k	k	k7c3
odstraňování	odstraňování	k1gNnSc3
vodního	vodní	k2eAgInSc2d1
kamene	kámen	k1gInSc2
a	a	k8xC
rzi	rez	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Polyvinylfosfonové	Polyvinylfosfonový	k2eAgFnPc1d1
kyseliny	kyselina	k1gFnPc1
jsou	být	k5eAaImIp3nP
složkami	složka	k1gFnPc7
polymerních	polymerní	k2eAgFnPc2d1
elektrolytových	elektrolytový	k2eAgFnPc2d1
membrán	membrána	k1gFnPc2
používaných	používaný	k2eAgFnPc2d1
v	v	k7c6
palivových	palivový	k2eAgInPc6d1
článcích	článek	k1gInPc6
<g/>
,	,	kIx,
hydrogelech	hydrogel	k1gInPc6
sloužících	sloužící	k2eAgInPc6d1
k	k	k7c3
dopravě	doprava	k1gFnSc6
léčiv	léčivo	k1gNnPc2
do	do	k7c2
cílových	cílový	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
a	a	k8xC
v	v	k7c6
biomimetické	biomimetický	k2eAgFnSc6d1
mineralizaci	mineralizace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Vinylphosphonic	Vinylphosphonice	k1gFnPc2
acid	acido	k1gNnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
https://pubchem.ncbi.nlm.nih.gov/compound/1687251	https://pubchem.ncbi.nlm.nih.gov/compound/1687251	k4
2	#num#	k4
M.	M.	kA
Lavinia	Lavinium	k1gNnSc2
<g/>
;	;	kIx,
I.	I.	kA
Gheorghe	Gheorghe	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poly	pola	k1gFnSc2
<g/>
(	(	kIx(
<g/>
vinylphosphonic	vinylphosphonice	k1gFnPc2
acid	acid	k1gInSc1
<g/>
)	)	kIx)
and	and	k?
its	its	k?
derivatives	derivatives	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Progress	Progress	k1gInSc1
in	in	k?
Polymer	polymer	k1gInSc1
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
350	#num#	k4
<g/>
-	-	kIx~
<g/>
365	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
progpolymsci	progpolymsek	k1gMnPc1
<g/>
.2010	.2010	k4
<g/>
.04	.04	k4
<g/>
.001	.001	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
