<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Wien	Wien	k1gNnSc1	Wien
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
také	také	k9	také
statutární	statutární	k2eAgNnSc4d1	statutární
město	město	k1gNnSc4	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1922	[number]	k4	1922
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
obklopená	obklopený	k2eAgFnSc1d1	obklopená
územím	území	k1gNnSc7	území
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Dolní	dolní	k2eAgInPc1d1	dolní
Rakousy	Rakousy	k1gInPc1	Rakousy
<g/>
.	.	kIx.	.
</s>
