<p>
<s>
System	Systo	k1gNnSc7	Systo
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gInSc1	Down
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
alternativně	alternativně	k6eAd1	alternativně
metalová	metalový	k2eAgFnSc1d1	metalová
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
Los	los	k1gInSc1	los
Angeles	Angeles	k1gInSc1	Angeles
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
na	na	k7c6	na
SOAD	SOAD	kA	SOAD
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc4	člen
jsou	být	k5eAaImIp3nP	být
Daron	Daron	k1gMnSc1	Daron
Malakian	Malakian	k1gMnSc1	Malakian
(	(	kIx(	(
<g/>
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Serj	Serj	k1gFnSc1	Serj
Tankian	Tankiana	k1gFnPc2	Tankiana
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInPc1d1	hlavní
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
občasné	občasný	k2eAgInPc1d1	občasný
klávesy	kláves	k1gInPc1	kláves
a	a	k8xC	a
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shavo	Shavo	k1gNnSc1	Shavo
Odadjian	Odadjiana	k1gFnPc2	Odadjiana
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
<g/>
)	)	kIx)	)
a	a	k8xC	a
John	John	k1gMnSc1	John
Dolmayan	Dolmayan	k1gMnSc1	Dolmayan
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sami	sám	k3xTgMnPc1	sám
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
žádné	žádný	k3yNgFnSc2	žádný
hudební	hudební	k2eAgFnSc2d1	hudební
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Malakian	Malakian	k1gMnSc1	Malakian
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
prozatím	prozatím	k6eAd1	prozatím
nesžil	sžít	k5eNaPmAgMnS	sžít
s	s	k7c7	s
nu-metalovým	nuetalův	k2eAgInSc7d1	nu-metalův
zvukem	zvuk	k1gInSc7	zvuk
7	[number]	k4	7
<g/>
-strunné	trunný	k2eAgFnPc1d1	-strunný
kytary	kytara	k1gFnPc1	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
prohlašují	prohlašovat	k5eAaImIp3nP	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostě	prostě	k9	prostě
hrají	hrát	k5eAaImIp3nP	hrát
svůj	svůj	k3xOyFgInSc4	svůj
styl	styl	k1gInSc4	styl
–	–	k?	–
styl	styl	k1gInSc1	styl
System	Systo	k1gNnSc7	Systo
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gInSc1	Down
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
mají	mít	k5eAaImIp3nP	mít
arménské	arménský	k2eAgMnPc4d1	arménský
předky	předek	k1gMnPc4	předek
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
i	i	k9	i
arménskou	arménský	k2eAgFnSc7d1	arménská
genocidou	genocida	k1gFnSc7	genocida
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1915-18	[number]	k4	1915-18
a	a	k8xC	a
válkou	válka	k1gFnSc7	válka
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
v	v	k7c6	v
USA	USA	kA	USA
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
původem	původ	k1gInSc7	původ
Arméni	Armén	k1gMnPc1	Armén
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Serj	Serj	k1gMnSc1	Serj
Tankian	Tankian	k1gMnSc1	Tankian
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
John	John	k1gMnSc1	John
Dolmayan	Dolmayan	k1gMnSc1	Dolmayan
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Daron	Daron	k1gMnSc1	Daron
Malakian	Malakian	k1gMnSc1	Malakian
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Hollywoodu	Hollywood	k1gInSc2	Hollywood
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
baskytarista	baskytarista	k1gMnSc1	baskytarista
Shavo	Shavo	k1gNnSc1	Shavo
Odadjian	Odadjian	k1gMnSc1	Odadjian
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
potkali	potkat	k5eAaPmAgMnP	potkat
Serj	Serj	k1gMnSc1	Serj
Tankian	Tankian	k1gMnSc1	Tankian
a	a	k8xC	a
Daron	Daron	k1gMnSc1	Daron
Malakian	Malakian	k1gMnSc1	Malakian
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
kapelu	kapela	k1gFnSc4	kapela
jménem	jméno	k1gNnSc7	jméno
Soil	Soilum	k1gNnPc2	Soilum
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
<g/>
:	:	kIx,	:
Serj	Serj	k1gMnSc1	Serj
Tankian	Tankian	k1gMnSc1	Tankian
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
a	a	k8xC	a
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
Daron	Daron	k1gMnSc1	Daron
Malakian	Malakian	k1gMnSc1	Malakian
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Hagopyn	Hagopyn	k1gNnSc4	Hagopyn
–	–	k?	–
basa	basa	k1gFnSc1	basa
a	a	k8xC	a
Domingo	Domingo	k1gMnSc1	Domingo
Laranio	Laranio	k1gMnSc1	Laranio
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Serj	Serj	k1gMnSc1	Serj
a	a	k8xC	a
Daron	Daron	k1gMnSc1	Daron
založili	založit	k5eAaPmAgMnP	založit
novou	nový	k2eAgFnSc4d1	nová
skupinu	skupina	k1gFnSc4	skupina
s	s	k7c7	s
názvem	název	k1gInSc7	název
System	Syst	k1gMnSc7	Syst
of	of	k?	of
a	a	k8xC	a
down	downa	k1gFnPc2	downa
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
básně	báseň	k1gFnSc2	báseň
Victim	Victim	k1gInSc1	Victim
of	of	k?	of
a	a	k8xC	a
down	down	k1gMnSc1	down
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Shavo	Shavo	k6eAd1	Shavo
jim	on	k3xPp3gMnPc3	on
dělal	dělat	k5eAaImAgMnS	dělat
manažera	manažer	k1gMnSc2	manažer
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
basu	basa	k1gFnSc4	basa
a	a	k8xC	a
na	na	k7c4	na
bubny	buben	k1gInPc4	buben
Andy	Anda	k1gFnSc2	Anda
Khachaturian	Khachaturiana	k1gFnPc2	Khachaturiana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
bubeník	bubeník	k1gMnSc1	bubeník
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
Johnem	John	k1gMnSc7	John
Dolmayanem	Dolmayan	k1gMnSc7	Dolmayan
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
získala	získat	k5eAaPmAgFnS	získat
své	svůj	k3xOyFgNnSc4	svůj
konečné	konečný	k2eAgNnSc4d1	konečné
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
jmenovalo	jmenovat	k5eAaImAgNnS	jmenovat
jako	jako	k9	jako
kapela	kapela	k1gFnSc1	kapela
–	–	k?	–
tedy	tedy	k8xC	tedy
System	Systo	k1gNnSc7	Systo
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gInSc1	Down
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
písní	píseň	k1gFnPc2	píseň
vydalo	vydat	k5eAaPmAgNnS	vydat
jako	jako	k9	jako
singly	singl	k1gInPc4	singl
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
124	[number]	k4	124
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
oceněno	ocenit	k5eAaPmNgNnS	ocenit
platinovou	platinový	k2eAgFnSc7d1	platinová
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
album	album	k1gNnSc4	album
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
dostalo	dostat	k5eAaPmAgNnS	dostat
jméno	jméno	k1gNnSc4	jméno
Toxicity	toxicita	k1gFnSc2	toxicita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
vyhouplo	vyhoupnout	k5eAaPmAgNnS	vyhoupnout
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
žebříčku	žebříček	k1gInSc2	žebříček
a	a	k8xC	a
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
oceněno	ocenit	k5eAaPmNgNnS	ocenit
čtyřmi	čtyři	k4xCgFnPc7	čtyři
platinovými	platinový	k2eAgFnPc7d1	platinová
deskami	deska	k1gFnPc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
deska	deska	k1gFnSc1	deska
Steal	Steal	k1gInSc4	Steal
This	This	k1gInSc1	This
Album	album	k1gNnSc1	album
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předcházející	předcházející	k2eAgFnSc2d1	předcházející
Toxicity	toxicita	k1gFnSc2	toxicita
propadla	propadnout	k5eAaPmAgFnS	propadnout
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
jejích	její	k3xOp3gFnPc2	její
písní	píseň	k1gFnPc2	píseň
unikla	uniknout	k5eAaPmAgFnS	uniknout
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
v	v	k7c6	v
demo	demo	k2eAgFnPc6d1	demo
verzích	verze	k1gFnPc6	verze
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
písně	píseň	k1gFnPc1	píseň
komponovány	komponovat	k5eAaImNgFnP	komponovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
písněmi	píseň	k1gFnPc7	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
Toxicity	toxicita	k1gFnSc2	toxicita
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
odcizení	odcizení	k1gNnSc4	odcizení
demo	demo	k2eAgFnPc2d1	demo
nahrávek	nahrávka	k1gFnPc2	nahrávka
ji	on	k3xPp3gFnSc4	on
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Steal	Steal	k1gInSc1	Steal
this	this	k6eAd1	this
album	album	k1gNnSc4	album
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
Ukradni	ukradnout	k5eAaPmRp2nS	ukradnout
tohle	tenhle	k3xDgNnSc4	tenhle
album	album	k1gNnSc4	album
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
pořádného	pořádný	k2eAgInSc2d1	pořádný
bookletu	booklet	k1gInSc2	booklet
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
dokonce	dokonce	k9	dokonce
56	[number]	k4	56
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
mnohými	mnohý	k2eAgFnPc7d1	mnohá
fanoušky	fanoušek	k1gMnPc4	fanoušek
hodnocena	hodnocen	k2eAgFnSc1d1	hodnocena
jako	jako	k8xS	jako
deska	deska	k1gFnSc1	deska
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
na	na	k7c6	na
vydaném	vydaný	k2eAgNnSc6d1	vydané
albu	album	k1gNnSc6	album
se	se	k3xPyFc4	se
od	od	k7c2	od
uniklých	uniklý	k2eAgFnPc2d1	uniklá
verzí	verze	k1gFnPc2	verze
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
přepracovány	přepracován	k2eAgInPc1d1	přepracován
texty	text	k1gInPc1	text
a	a	k8xC	a
mnohokrát	mnohokrát	k6eAd1	mnohokrát
dokonce	dokonce	k9	dokonce
i	i	k9	i
samotné	samotný	k2eAgFnPc4d1	samotná
melodie	melodie	k1gFnPc4	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Uniklé	uniklý	k2eAgNnSc1d1	uniklé
demo	demo	k2eAgNnSc1d1	demo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
navíc	navíc	k6eAd1	navíc
ještě	ještě	k9	ještě
dvě	dva	k4xCgFnPc4	dva
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
Steal	Steal	k1gInSc4	Steal
this	this	k6eAd1	this
Album	album	k1gNnSc1	album
nedostaly	dostat	k5eNaPmAgInP	dostat
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
STA	sto	k4xCgNnPc1	sto
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
některé	některý	k3yIgFnPc4	některý
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nenacházely	nacházet	k5eNaImAgFnP	nacházet
na	na	k7c6	na
uniklém	uniklý	k2eAgNnSc6d1	uniklé
demu	demum	k1gNnSc6	demum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kapele	kapela	k1gFnSc3	kapela
vyplatilo	vyplatit	k5eAaPmAgNnS	vyplatit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
své	svůj	k3xOyFgNnSc4	svůj
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Mezmerize	Mezmerize	k1gFnSc1	Mezmerize
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
a	a	k8xC	a
v	v	k7c6	v
umístění	umístění	k1gNnSc6	umístění
na	na	k7c6	na
žebříčcích	žebříček	k1gInPc6	žebříček
prodejnosti	prodejnost	k1gFnSc2	prodejnost
porazilo	porazit	k5eAaPmAgNnS	porazit
album	album	k1gNnSc1	album
Toxicity	toxicita	k1gFnSc2	toxicita
<g/>
.	.	kIx.	.
</s>
<s>
Mezmerize	Mezmerize	k6eAd1	Mezmerize
se	se	k3xPyFc4	se
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
druhém	druhý	k4xOgNnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
napodobilo	napodobit	k5eAaPmAgNnS	napodobit
System	Systo	k1gNnSc7	Systo
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gNnSc1	Down
a	a	k8xC	a
Toxicity	toxicita	k1gFnPc1	toxicita
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
platinovou	platinový	k2eAgFnSc4d1	platinová
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
"	"	kIx"	"
<g/>
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
<g/>
"	"	kIx"	"
album	album	k1gNnSc4	album
Hypnotize	Hypnotize	k1gFnSc2	Hypnotize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
velkou	velký	k2eAgFnSc7d1	velká
součástí	součást	k1gFnSc7	součást
dokumentárního	dokumentární	k2eAgInSc2d1	dokumentární
filmu	film	k1gInSc2	film
Screamers	Screamers	k1gInSc1	Screamers
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přestávka	přestávka	k1gFnSc1	přestávka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
skupina	skupina	k1gFnSc1	skupina
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
přestávku	přestávka	k1gFnSc4	přestávka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
Darona	Daron	k1gMnSc2	Daron
měla	mít	k5eAaImAgFnS	mít
trvat	trvat	k5eAaImF	trvat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
rozpad	rozpad	k1gInSc1	rozpad
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
přestávky	přestávka	k1gFnSc2	přestávka
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
členové	člen	k1gMnPc1	člen
chtějí	chtít	k5eAaImIp3nP	chtít
rozjet	rozjet	k5eAaPmF	rozjet
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
projekty	projekt	k1gInPc4	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Daron	Daron	k1gMnSc1	Daron
založil	založit	k5eAaPmAgMnS	založit
kapelu	kapela	k1gFnSc4	kapela
Scars	Scarsa	k1gFnPc2	Scarsa
on	on	k3xPp3gMnSc1	on
Broadway	Broadwa	k2eAgMnPc4d1	Broadwa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vydala	vydat	k5eAaPmAgFnS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
debut	debut	k1gInSc4	debut
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kapele	kapela	k1gFnSc6	kapela
je	být	k5eAaImIp3nS	být
i	i	k9	i
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Shavo	Shavo	k6eAd1	Shavo
založil	založit	k5eAaPmAgMnS	založit
s	s	k7c7	s
RZA	RZA	kA	RZA
z	z	k7c2	z
Wu-Tang	Wu-Tanga	k1gFnPc2	Wu-Tanga
Clanu	Claen	k2eAgFnSc4d1	Claen
kapelu	kapela	k1gFnSc4	kapela
Achozen	Achozen	k2eAgMnSc1d1	Achozen
<g/>
.	.	kIx.	.
</s>
<s>
Serj	Serj	k1gMnSc1	Serj
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
své	svůj	k3xOyFgNnSc4	svůj
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
Serjical	Serjical	k1gMnPc1	Serjical
Strike	Strik	k1gInSc2	Strik
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
vydal	vydat	k5eAaPmAgMnS	vydat
svoje	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
čistě	čistě	k6eAd1	čistě
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
Elect	Elect	k1gMnSc1	Elect
the	the	k?	the
Dead	Dead	k1gMnSc1	Dead
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
už	už	k6eAd1	už
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
bodovalo	bodovat	k5eAaImAgNnS	bodovat
několik	několik	k4yIc1	několik
songů	song	k1gInPc2	song
a	a	k8xC	a
hudebních	hudební	k2eAgInPc2d1	hudební
klipů	klip	k1gInPc2	klip
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
si	se	k3xPyFc3	se
otevřel	otevřít	k5eAaPmAgMnS	otevřít
online	onlinout	k5eAaPmIp3nS	onlinout
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
komiksy	komiks	k1gInPc7	komiks
Torpedo	Torpedo	k1gNnSc4	Torpedo
Comics	comics	k1gInSc4	comics
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc4d1	poslední
koncert	koncert	k1gInSc4	koncert
odehráli	odehrát	k5eAaPmAgMnP	odehrát
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
na	na	k7c6	na
Ozzfestu	Ozzfest	k1gInSc6	Ozzfest
ve	v	k7c4	v
West	West	k2eAgInSc4d1	West
Palm	Palm	k1gInSc4	Palm
Beach	Beacha	k1gFnPc2	Beacha
<g/>
.	.	kIx.	.
</s>
<s>
Ohledně	ohledně	k7c2	ohledně
návratu	návrat	k1gInSc2	návrat
k	k	k7c3	k
SOAD	SOAD	kA	SOAD
zatím	zatím	k6eAd1	zatím
mluví	mluvit	k5eAaImIp3nS	mluvit
pouze	pouze	k6eAd1	pouze
stylem	styl	k1gInSc7	styl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dveře	dveře	k1gFnPc1	dveře
jsou	být	k5eAaImIp3nP	být
otevřené	otevřený	k2eAgFnPc1d1	otevřená
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Dne	den	k1gInSc2	den
31.10	[number]	k4	31.10
<g/>
.2009	.2009	k4	.2009
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
párty	párty	k1gFnSc4	párty
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
HALLOW	HALLOW	kA	HALLOW
<g/>
9	[number]	k4	9
<g/>
EEN	EEN	kA	EEN
A	A	kA	A
Night	Night	k1gMnSc1	Night
of	of	k?	of
SurpriZes	SurpriZes	k1gMnSc1	SurpriZes
<g/>
"	"	kIx"	"
ve	v	k7c6	v
věhlasném	věhlasný	k2eAgInSc6d1	věhlasný
klubu	klub	k1gInSc6	klub
Roxy	Roxa	k1gFnSc2	Roxa
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mimochodem	mimochodem	k9	mimochodem
odehrála	odehrát	k5eAaPmAgFnS	odehrát
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
show	show	k1gFnSc1	show
SOAD	SOAD	kA	SOAD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
párty	párty	k1gFnSc6	párty
byli	být	k5eAaImAgMnP	být
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
členů	člen	k1gInPc2	člen
SOAD	SOAD	kA	SOAD
<g/>
.	.	kIx.	.
<g/>
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
<g/>
Daron	Daron	k1gMnSc1	Daron
a	a	k8xC	a
Shavo	Shavo	k1gNnSc4	Shavo
zahráli	zahrát	k5eAaPmAgMnP	zahrát
společně	společně	k6eAd1	společně
píseň	píseň	k1gFnSc4	píseň
Suite-pee	Suitee	k1gFnSc2	Suite-pe
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
akce	akce	k1gFnSc1	akce
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
členů	člen	k1gInPc2	člen
byla	být	k5eAaImAgFnS	být
20.11	[number]	k4	20.11
<g/>
.2009	.2009	k4	.2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
benefiční	benefiční	k2eAgInSc1d1	benefiční
koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
basáka	basák	k1gMnSc4	basák
Deftones	Deftones	k1gInSc4	Deftones
<g/>
,	,	kIx,	,
Chi	chi	k0	chi
Chenga	Cheng	k1gMnSc4	Cheng
<g/>
.	.	kIx.	.
</s>
<s>
Zahráli	zahrát	k5eAaPmAgMnP	zahrát
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
Aerials	Aerials	k1gInSc1	Aerials
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Toxicity	toxicita	k1gFnSc2	toxicita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Comeback	Comeback	k1gInSc1	Comeback
2011	[number]	k4	2011
==	==	k?	==
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
zvěstí	zvěstit	k5eAaPmIp3nS	zvěstit
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
,	,	kIx,	,
System	Systo	k1gNnSc7	Systo
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gMnSc1	Down
oficiálně	oficiálně	k6eAd1	oficiálně
oznámili	oznámit	k5eAaPmAgMnP	oznámit
comeback	comeback	k1gInSc4	comeback
<g/>
,	,	kIx,	,
turné	turné	k1gNnSc4	turné
čítající	čítající	k2eAgMnSc1d1	čítající
nad	nad	k7c4	nad
20	[number]	k4	20
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
po	po	k7c6	po
evropských	evropský	k2eAgInPc6d1	evropský
festivalech	festival	k1gInPc6	festival
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
festivaly	festival	k1gInPc4	festival
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
SOAD	SOAD	kA	SOAD
navštívili	navštívit	k5eAaPmAgMnP	navštívit
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Download	Download	k1gInSc1	Download
Festival	festival	k1gInSc1	festival
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
švýcarský	švýcarský	k2eAgInSc1d1	švýcarský
Greenfield	Greenfield	k1gInSc1	Greenfield
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Rock	rock	k1gInSc1	rock
am	am	k?	am
Ring	ring	k1gInSc1	ring
/	/	kIx~	/
Rock	rock	k1gInSc1	rock
im	im	k?	im
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
švédský	švédský	k2eAgInSc1d1	švédský
Metaltown	Metaltown	k1gInSc1	Metaltown
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgInSc1d1	rakouský
Nova	nova	k1gFnSc1	nova
Rock	rock	k1gInSc1	rock
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
finský	finský	k2eAgMnSc1d1	finský
Provinssirock	Provinssirock	k1gMnSc1	Provinssirock
nebo	nebo	k8xC	nebo
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
festivalů	festival	k1gInPc2	festival
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
Rock	rock	k1gInSc1	rock
in	in	k?	in
Rio	Rio	k1gFnSc2	Rio
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dalších	další	k2eAgInPc6d1	další
plánech	plán	k1gInPc6	plán
a	a	k8xC	a
budoucnosti	budoucnost	k1gFnSc6	budoucnost
kapely	kapela	k1gFnSc2	kapela
zatím	zatím	k6eAd1	zatím
nebylo	být	k5eNaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
amerického	americký	k2eAgNnSc2d1	americké
turné	turné	k1gNnSc2	turné
je	být	k5eAaImIp3nS	být
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
kapela	kapela	k1gFnSc1	kapela
Gogol	Gogol	k1gMnSc1	Gogol
Bordello	Bordello	k1gNnSc4	Bordello
jako	jako	k8xS	jako
předskokané	předskokaný	k2eAgInPc4d1	předskokaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
turné	turné	k1gNnSc1	turné
po	po	k7c6	po
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
momentální	momentální	k2eAgFnSc6d1	momentální
době	doba	k1gFnSc6	doba
plánuje	plánovat	k5eAaImIp3nS	plánovat
Serj	Serj	k1gMnSc1	Serj
Tankian	Tankian	k1gMnSc1	Tankian
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
sólo	sólo	k2eAgInPc6d1	sólo
projektech	projekt	k1gInPc6	projekt
a	a	k8xC	a
turné	turné	k1gNnSc6	turné
<g/>
.	.	kIx.	.
</s>
<s>
Daron	Daron	k1gMnSc1	Daron
Malakian	Malakian	k1gMnSc1	Malakian
chystá	chystat	k5eAaImIp3nS	chystat
vydání	vydání	k1gNnSc4	vydání
druhé	druhý	k4xOgFnSc2	druhý
desky	deska	k1gFnSc2	deska
Scars	Scarsa	k1gFnPc2	Scarsa
on	on	k3xPp3gMnSc1	on
Broadway	Broadwa	k2eAgFnPc1d1	Broadwa
na	na	k7c4	na
jaro	jaro	k6eAd1	jaro
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
budoucnost	budoucnost	k1gFnSc1	budoucnost
System	Syst	k1gInSc7	Syst
of	of	k?	of
a	a	k8xC	a
Down	Downo	k1gNnPc2	Downo
tedy	tedy	k9	tedy
nebyla	být	k5eNaImAgFnS	být
konkrétně	konkrétně	k6eAd1	konkrétně
specifikována	specifikován	k2eAgFnSc1d1	specifikována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
zahrají	zahrát	k5eAaPmIp3nP	zahrát
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
koncertech	koncert	k1gInPc6	koncert
a	a	k8xC	a
možná	možná	k9	možná
zapracují	zapracovat	k5eAaPmIp3nP	zapracovat
i	i	k9	i
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
2013	[number]	k4	2013
==	==	k?	==
</s>
</p>
<p>
<s>
SOAD	SOAD	kA	SOAD
představují	představovat	k5eAaImIp3nP	představovat
evropské	evropský	k2eAgInPc1d1	evropský
tour	tour	k1gInSc1	tour
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
(	(	kIx(	(
<g/>
objevili	objevit	k5eAaPmAgMnP	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
po	po	k7c6	po
15	[number]	k4	15
letech	léto	k1gNnPc6	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c6	na
Festivalu	festival	k1gInSc6	festival
Aerodrome	aerodrom	k1gInSc5	aerodrom
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
budou	být	k5eAaImBp3nP	být
například	například	k6eAd1	například
na	na	k7c6	na
švédském	švédský	k2eAgInSc6d1	švédský
Gateway	Gatewa	k2eAgInPc1d1	Gatewa
Rock	rock	k1gInSc4	rock
Festivalu	festival	k1gInSc2	festival
nebo	nebo	k8xC	nebo
také	také	k9	také
francouzském	francouzský	k2eAgInSc6d1	francouzský
Rock	rock	k1gInSc1	rock
En	En	k1gMnSc1	En
Seine	Sein	k1gInSc5	Sein
Festivalu	festival	k1gInSc3	festival
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Spory	spor	k1gInPc1	spor
o	o	k7c4	o
žánr	žánr	k1gInSc4	žánr
==	==	k?	==
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
System	Syst	k1gInSc7	Syst
of	of	k?	of
a	a	k8xC	a
Down	Downa	k1gFnPc2	Downa
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
několikrát	několikrát	k6eAd1	několikrát
označena	označen	k2eAgFnSc1d1	označena
jako	jako	k8xS	jako
nu-metalová	nuetalový	k2eAgFnSc1d1	nu-metalová
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nálepka	nálepka	k1gFnSc1	nálepka
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
přičítána	přičítán	k2eAgFnSc1d1	přičítána
prvnímu	první	k4xOgNnSc3	první
albu	album	k1gNnSc3	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nu-metalového	nuetalový	k2eAgInSc2d1	nu-metalový
rozmachu	rozmach	k1gInSc2	rozmach
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nesouhlasí	souhlasit	k5eNaImIp3nP	souhlasit
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
argumentují	argumentovat	k5eAaImIp3nP	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hudba	hudba	k1gFnSc1	hudba
System	Syst	k1gInSc7	Syst
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gInSc1	Down
nesplňuje	splňovat	k5eNaImIp3nS	splňovat
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
typických	typický	k2eAgInPc2d1	typický
znaků	znak	k1gInPc2	znak
nu-metalu	nuetal	k1gMnSc3	nu-metal
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kytarové	kytarový	k2eAgInPc1d1	kytarový
riffy	riff	k1gInPc1	riff
připomínají	připomínat	k5eAaImIp3nP	připomínat
spíš	spíš	k9	spíš
thrash	thrash	k1gInSc4	thrash
metal	metat	k5eAaImAgMnS	metat
než	než	k8xS	než
nu-metal	nuetat	k5eAaImAgMnS	nu-metat
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
písničkách	písnička	k1gFnPc6	písnička
na	na	k7c6	na
každém	každý	k3xTgNnSc6	každý
albu	album	k1gNnSc6	album
jsou	být	k5eAaImIp3nP	být
kytarová	kytarový	k2eAgNnPc1d1	kytarové
sóla	sólo	k1gNnPc1	sólo
<g/>
,	,	kIx,	,
basa	basa	k1gFnSc1	basa
nehraje	hrát	k5eNaImIp3nS	hrát
tak	tak	k6eAd1	tak
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
jako	jako	k8xC	jako
v	v	k7c6	v
nu-metalu	nuetal	k1gMnSc6	nu-metal
</s>
</p>
<p>
<s>
Odlišné	odlišný	k2eAgNnSc1d1	odlišné
tempo	tempo	k1gNnSc1	tempo
bicích	bicí	k2eAgFnPc2d1	bicí
</s>
</p>
<p>
<s>
Texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
politickými	politický	k2eAgInPc7d1	politický
a	a	k8xC	a
společenskými	společenský	k2eAgInPc7d1	společenský
problémy	problém	k1gInPc7	problém
spíš	spíš	k9	spíš
než	než	k8xS	než
problémy	problém	k1gInPc1	problém
osobnímiBěhem	osobnímiBěh	k1gInSc7	osobnímiBěh
jednoho	jeden	k4xCgInSc2	jeden
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Daron	Daron	k1gMnSc1	Daron
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dřív	dříve	k6eAd2	dříve
nás	my	k3xPp1nPc4	my
nazývali	nazývat	k5eAaImAgMnP	nazývat
nu-metal	nuetat	k5eAaPmAgInS	nu-metat
<g/>
,	,	kIx,	,
teď	teď	k6eAd1	teď
zase	zase	k9	zase
progressive	progressivat	k5eAaPmIp3nS	progressivat
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nás	my	k3xPp1nPc4	my
nazývají	nazývat	k5eAaImIp3nP	nazývat
čímkoli	číkoli	k3xOyIgFnPc3	číkoli
co	co	k9	co
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgMnSc1d1	populární
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k9	ale
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nenávidím	nenávidět	k5eAaImIp1nS	nenávidět
označení	označení	k1gNnPc4	označení
nu-metal	nuetat	k5eAaPmAgInS	nu-metat
pro	pro	k7c4	pro
SOAD	SOAD	kA	SOAD
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
cokoli	cokoli	k3yInSc4	cokoli
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Žánr	žánr	k1gInSc1	žánr
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
označit	označit	k5eAaPmF	označit
pouze	pouze	k6eAd1	pouze
slovem	slovo	k1gNnSc7	slovo
crossover	crossovra	k1gFnPc2	crossovra
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
rychlé	rychlý	k2eAgNnSc4d1	rychlé
přeskakování	přeskakování	k1gNnSc4	přeskakování
mezi	mezi	k7c7	mezi
různými	různý	k2eAgInPc7d1	různý
styly	styl	k1gInPc7	styl
(	(	kIx(	(
<g/>
jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
hardcore	hardcor	k1gMnSc5	hardcor
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
Serj	Serj	k1gMnSc1	Serj
Tankian	Tankian	k1gMnSc1	Tankian
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
kytara	kytara	k1gFnSc1	kytara
</s>
</p>
<p>
<s>
Daron	Daron	k1gMnSc1	Daron
Malakian	Malakian	k1gMnSc1	Malakian
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Shavo	Shavo	k6eAd1	Shavo
Odadjian	Odadjian	k1gInSc1	Odadjian
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Dolmayan	Dolmayan	k1gMnSc1	Dolmayan
–	–	k?	–
bicí	bicí	k2eAgNnSc1d1	bicí
</s>
</p>
<p>
<s>
==	==	k?	==
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
Andy	Anda	k1gFnPc1	Anda
Khachaturian	Khachaturiana	k1gFnPc2	Khachaturiana
–	–	k?	–
bicí	bicí	k2eAgFnSc2d1	bicí
</s>
</p>
<p>
<s>
==	==	k?	==
Alba	album	k1gNnSc2	album
==	==	k?	==
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
System	Systo	k1gNnSc7	Systo
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gMnSc1	Down
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
Toxicity	toxicita	k1gFnSc2	toxicita
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
Steal	Steal	k1gInSc1	Steal
This	This	k1gInSc4	This
Album	album	k1gNnSc1	album
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
Mezmerize	Mezmerize	k1gFnSc1	Mezmerize
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
Hypnotize	Hypnotize	k1gFnSc1	Hypnotize
</s>
</p>
<p>
<s>
==	==	k?	==
Videografie	Videografie	k1gFnSc2	Videografie
==	==	k?	==
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
War	War	k1gFnSc2	War
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
Sugar	Sugar	k1gMnSc1	Sugar
–	–	k?	–
Režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Nathan	Nathan	k1gMnSc1	Nathan
Cox	Cox	k1gMnSc1	Cox
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Spiders	Spidersa	k1gFnPc2	Spidersa
–	–	k?	–
Režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Charlie	Charlie	k1gMnSc1	Charlie
Deaux	Deaux	k1gInSc4	Deaux
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
<g/>
:	:	kIx,	:
Chop	chop	k1gInSc1	chop
Suey	Suea	k1gFnSc2	Suea
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
Režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Marcos	Marcos	k1gMnSc1	Marcos
Siega	Sieg	k1gMnSc2	Sieg
<g/>
/	/	kIx~	/
<g/>
Shavo	Shavo	k1gNnSc1	Shavo
Odadjian	Odadjiana	k1gFnPc2	Odadjiana
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Toxicity	toxicita	k1gFnSc2	toxicita
–	–	k?	–
Režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Shavo	Shavo	k1gNnSc1	Shavo
Odadjian	Odadjiana	k1gFnPc2	Odadjiana
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Aerials	Aerialsa	k1gFnPc2	Aerialsa
–	–	k?	–
Režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Shavo	Shavo	k1gNnSc1	Shavo
Odadjian	Odadjiana	k1gFnPc2	Odadjiana
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Boom	boom	k1gInSc1	boom
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
Režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Michael	Michael	k1gMnSc1	Michael
Moore	Moor	k1gMnSc5	Moor
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
B.Y.O.B.	B.Y.O.B.	k1gMnSc1	B.Y.O.B.
–	–	k?	–
Režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Jake	Jake	k1gFnSc1	Jake
Nava	Nava	k?	Nava
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Question	Question	k1gInSc1	Question
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
Režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Shavo	Shavo	k1gNnSc1	Shavo
Odadjian	Odadjiany	k1gInPc2	Odadjiany
<g/>
/	/	kIx~	/
<g/>
Howard	Howard	k1gMnSc1	Howard
Greenhalgh	Greenhalgh	k1gMnSc1	Greenhalgh
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Hypnotize	Hypnotize	k1gFnSc2	Hypnotize
–	–	k?	–
Režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Shavo	Shavo	k1gNnSc1	Shavo
Odadjian	Odadjiana	k1gFnPc2	Odadjiana
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Lonely	Lonela	k1gFnSc2	Lonela
Day	Day	k1gMnSc1	Day
–	–	k?	–
Režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Josh	Josh	k1gMnSc1	Josh
Melnick	Melnick	k1gMnSc1	Melnick
<g/>
/	/	kIx~	/
<g/>
Xander	Xander	k1gMnSc1	Xander
Charity	charita	k1gFnSc2	charita
</s>
</p>
<p>
<s>
==	==	k?	==
Singly	singl	k1gInPc4	singl
==	==	k?	==
</s>
</p>
<p>
<s>
Sugar	Sugar	k1gInSc1	Sugar
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spiders	Spiders	k1gInSc1	Spiders
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chop	chop	k1gInSc1	chop
Suey	Suea	k1gFnSc2	Suea
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Toxicity	toxicita	k1gFnPc1	toxicita
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aerials	Aerials	k1gInSc1	Aerials
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Boom	boom	k1gInSc1	boom
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
B.Y.O.B.	B.Y.O.B.	k?	B.Y.O.B.
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Question	Question	k1gInSc1	Question
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hypnotize	Hypnotize	k1gFnSc1	Hypnotize
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lonely	Lonel	k1gInPc1	Lonel
Day	Day	k1gFnSc2	Day
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Kerrang	Kerrang	k1gInSc1	Kerrang
–	–	k?	–
Best	Best	k2eAgInSc1d1	Best
Live	Live	k1gInSc1	Live
Band	banda	k1gFnPc2	banda
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
MTV	MTV	kA	MTV
Europe	Europ	k1gInSc5	Europ
Music	Musice	k1gFnPc2	Musice
Award	Award	k1gMnSc1	Award
–	–	k?	–
Best	Best	k1gMnSc1	Best
Alternative	Alternativ	k1gInSc5	Alternativ
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Grammy	Gramma	k1gFnSc2	Gramma
–	–	k?	–
Best	Best	k2eAgInSc4d1	Best
Hard	Hard	k1gInSc4	Hard
Rock	rock	k1gInSc1	rock
Performance	performance	k1gFnSc1	performance
(	(	kIx(	(
<g/>
za	za	k7c4	za
B.Y.O.B.	B.Y.O.B.	k1gFnSc4	B.Y.O.B.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Echo	echo	k1gNnSc1	echo
–	–	k?	–
Best	Best	k1gMnSc1	Best
Alternative	Alternativ	k1gInSc5	Alternativ
International	International	k1gFnSc7	International
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
MTV	MTV	kA	MTV
Good	Gooda	k1gFnPc2	Gooda
Woodie	Woodie	k1gFnSc1	Woodie
Award	Award	k1gInSc1	Award
s	s	k7c7	s
pisní	piseň	k1gFnSc7	piseň
"	"	kIx"	"
<g/>
Question	Question	k1gInSc1	Question
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
System	Systo	k1gNnSc7	Systo
of	of	k?	of
a	a	k8xC	a
Down	Down	k1gInSc1	Down
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
kapely	kapela	k1gFnSc2	kapela
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
české	český	k2eAgFnPc1d1	Česká
fan	fana	k1gFnPc2	fana
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
