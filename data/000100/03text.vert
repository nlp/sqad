<s>
Cena	cena	k1gFnSc1	cena
Emmy	Emma	k1gFnSc2	Emma
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Emmy	Emma	k1gFnPc1	Emma
Award	Awarda	k1gFnPc2	Awarda
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
každoročně	každoročně	k6eAd1	každoročně
udělované	udělovaný	k2eAgNnSc1d1	udělované
americké	americký	k2eAgNnSc1d1	americké
televizní	televizní	k2eAgNnSc1d1	televizní
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc1d1	televizní
obdoba	obdoba	k1gFnSc1	obdoba
filmových	filmový	k2eAgInPc2d1	filmový
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgFnP	vést
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
odvětvích	odvětví	k1gNnPc6	odvětví
amerického	americký	k2eAgInSc2d1	americký
televizního	televizní	k2eAgInSc2d1	televizní
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
předávány	předávat	k5eAaImNgFnP	předávat
na	na	k7c6	na
rozdílných	rozdílný	k2eAgFnPc6d1	rozdílná
každoročních	každoroční	k2eAgFnPc6d1	každoroční
ceremoniích	ceremonie	k1gFnPc6	ceremonie
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějšími	známý	k2eAgNnPc7d3	nejznámější
předáváními	předávání	k1gNnPc7	předávání
jsou	být	k5eAaImIp3nP	být
Primetime	Primetim	k1gInSc5	Primetim
Emmys	Emmys	k1gInSc1	Emmys
a	a	k8xC	a
Daytime	Daytim	k1gInSc5	Daytim
Emmys	Emmysa	k1gFnPc2	Emmysa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
zábavném	zábavný	k2eAgInSc6d1	zábavný
programu	program	k1gInSc6	program
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dne	den	k1gInSc2	den
a	a	k8xC	a
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
známé	známý	k2eAgMnPc4d1	známý
ceremonie	ceremonie	k1gFnSc2	ceremonie
cen	cena	k1gFnPc2	cena
Emmy	Emma	k1gFnSc2	Emma
patří	patřit	k5eAaImIp3nP	patřit
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
sportovní	sportovní	k2eAgNnSc4d1	sportovní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnPc1d1	národní
televizní	televizní	k2eAgFnPc1d1	televizní
zprávy	zpráva	k1gFnPc1	zpráva
a	a	k8xC	a
dokumentární	dokumentární	k2eAgInPc1d1	dokumentární
pořady	pořad	k1gInPc1	pořad
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnPc4d1	národní
pracovní	pracovní	k2eAgFnPc4d1	pracovní
a	a	k8xC	a
finanční	finanční	k2eAgFnPc4d1	finanční
zprávy	zpráva	k1gFnPc4	zpráva
a	a	k8xC	a
technologické	technologický	k2eAgInPc4d1	technologický
a	a	k8xC	a
inženýrské	inženýrský	k2eAgInPc4d1	inženýrský
úspěchy	úspěch	k1gInPc4	úspěch
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oddělené	oddělený	k2eAgFnPc1d1	oddělená
organizace	organizace	k1gFnPc1	organizace
organizují	organizovat	k5eAaBmIp3nP	organizovat
cen	cena	k1gFnPc2	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
:	:	kIx,	:
akademie	akademie	k1gFnSc2	akademie
televizního	televizní	k2eAgNnSc2d1	televizní
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
ATAS	ATAS	kA	ATAS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc1d1	národní
akademie	akademie	k1gFnSc1	akademie
televizního	televizní	k2eAgNnSc2d1	televizní
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
NATAS	NATAS	kA	NATAS
<g/>
)	)	kIx)	)
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
akademie	akademie	k1gFnSc1	akademie
televizního	televizní	k2eAgNnSc2d1	televizní
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
IATAS	IATAS	kA	IATAS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
organizací	organizace	k1gFnPc2	organizace
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
vytváření	vytváření	k1gNnSc4	vytváření
specifické	specifický	k2eAgFnSc2d1	specifická
složky	složka	k1gFnSc2	složka
na	na	k7c4	na
předávání	předávání	k1gNnSc4	předávání
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
cena	cena	k1gFnSc1	cena
Emmy	Emma	k1gFnSc2	Emma
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
při	při	k7c6	při
premiérovém	premiérový	k2eAgInSc6d1	premiérový
slavnostním	slavnostní	k2eAgInSc6d1	slavnostní
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
v	v	k7c4	v
Hollywood	Hollywood	k1gInSc4	Hollywood
Athletic	Athletice	k1gFnPc2	Athletice
Club	club	k1gInSc4	club
Shirley	Shirley	k1gInPc4	Shirley
Dinsdaleové	Dinsdaleové	k2eAgInPc4d1	Dinsdaleové
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
televizní	televizní	k2eAgFnSc1d1	televizní
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Primetime	Primetimat	k5eAaPmIp3nS	Primetimat
Emmys	Emmys	k1gInSc1	Emmys
-	-	kIx~	-
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
herce	herec	k1gMnPc4	herec
<g/>
,	,	kIx,	,
režiséry	režisér	k1gMnPc4	režisér
<g/>
,	,	kIx,	,
scenáristy	scenárista	k1gMnPc4	scenárista
a	a	k8xC	a
pořady	pořad	k1gInPc4	pořad
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
čase	čas	k1gInSc6	čas
amerického	americký	k2eAgNnSc2d1	americké
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Ceremonie	ceremonie	k1gFnSc1	ceremonie
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
měsíci	měsíc	k1gInSc6	měsíc
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Daytime	Daytimat	k5eAaPmIp3nS	Daytimat
Emmys	Emmys	k1gInSc1	Emmys
-	-	kIx~	-
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
pořady	pořad	k1gInPc4	pořad
<g/>
,	,	kIx,	,
herce	herec	k1gMnPc4	herec
<g/>
,	,	kIx,	,
moderátory	moderátor	k1gMnPc4	moderátor
<g/>
,	,	kIx,	,
scenáristy	scenárista	k1gMnPc4	scenárista
<g/>
,	,	kIx,	,
režiséry	režisér	k1gMnPc4	režisér
a	a	k8xC	a
španělské	španělský	k2eAgInPc4d1	španělský
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
vysílané	vysílaný	k2eAgNnSc1d1	vysílané
od	od	k7c2	od
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Sports	Sports	k1gInSc1	Sports
Emmys	Emmys	k1gInSc1	Emmys
-	-	kIx~	-
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
sportovní	sportovní	k2eAgInPc4d1	sportovní
pořady	pořad	k1gInPc4	pořad
<g/>
,	,	kIx,	,
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
studiové	studiový	k2eAgFnPc4d1	studiová
show	show	k1gFnPc4	show
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnPc4d1	sportovní
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
střih	střih	k1gInSc4	střih
<g/>
,	,	kIx,	,
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
studiové	studiový	k2eAgInPc1d1	studiový
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
osobnosti	osobnost	k1gFnPc1	osobnost
sportovního	sportovní	k2eAgNnSc2d1	sportovní
dění	dění	k1gNnSc2	dění
(	(	kIx(	(
<g/>
reportéry	reportér	k1gMnPc4	reportér
<g/>
,	,	kIx,	,
moderátory	moderátor	k1gMnPc4	moderátor
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
News	News	k1gInSc1	News
&	&	k?	&
Documentary	Documentara	k1gFnSc2	Documentara
Emmys	Emmysa	k1gFnPc2	Emmysa
-	-	kIx~	-
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
noviny	novina	k1gFnPc4	novina
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
novináře	novinář	k1gMnPc4	novinář
<g/>
,	,	kIx,	,
historické	historický	k2eAgInPc4d1	historický
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
informativní	informativní	k2eAgInPc4d1	informativní
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
reportáže	reportáž	k1gFnPc4	reportáž
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
dokumenty	dokument	k1gInPc4	dokument
Technology	technolog	k1gMnPc4	technolog
&	&	k?	&
Engineering	Engineering	k1gInSc1	Engineering
Emmys	Emmys	k1gInSc1	Emmys
-	-	kIx~	-
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
vývoj	vývoj	k1gInSc4	vývoj
v	v	k7c4	v
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
individuálně	individuálně	k6eAd1	individuálně
nebo	nebo	k8xC	nebo
za	za	k7c4	za
společnost	společnost	k1gFnSc4	společnost
International	International	k1gFnSc2	International
Emmys	Emmys	k1gInSc1	Emmys
-	-	kIx~	-
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
programy	program	k1gInPc1	program
vysílané	vysílaný	k2eAgInPc1d1	vysílaný
mimo	mimo	k7c4	mimo
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
:	:	kIx,	:
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
herce	herc	k1gInPc4	herc
<g/>
,	,	kIx,	,
seriály	seriál	k1gInPc4	seriál
<g/>
,	,	kIx,	,
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
telenovely	telenovela	k1gFnPc4	telenovela
<g/>
,	,	kIx,	,
TV	TV	kA	TV
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
minisérie	minisérie	k1gFnPc1	minisérie
<g/>
...	...	k?	...
Regional	Regional	k1gFnSc1	Regional
Emmys	Emmys	k1gInSc1	Emmys
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
Regionální	regionální	k2eAgFnSc1d1	regionální
cena	cena	k1gFnSc1	cena
Emmy	Emma	k1gFnSc2	Emma
oceňuje	oceňovat	k5eAaImIp3nS	oceňovat
místní	místní	k2eAgNnSc4d1	místní
vysílané	vysílaný	k2eAgNnSc4d1	vysílané
show	show	k1gNnSc4	show
a	a	k8xC	a
místní	místní	k2eAgFnPc4d1	místní
noviny	novina	k1gFnPc4	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Emmy	Emma	k1gFnSc2	Emma
Award	Awarda	k1gFnPc2	Awarda
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cena	cena	k1gFnSc1	cena
Emmy	Emma	k1gFnSc2	Emma
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Academy	Academ	k1gInPc1	Academ
of	of	k?	of
Television	Television	k1gInSc1	Television
Arts	Arts	k1gInSc1	Arts
and	and	k?	and
Sciences	Sciences	k1gInSc1	Sciences
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Emmy	Emma	k1gFnSc2	Emma
Awards	Awards	k1gInSc1	Awards
na	na	k7c4	na
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc6	Databasa
</s>
