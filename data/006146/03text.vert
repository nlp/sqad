<s>
Joseph	Joseph	k1gInSc1	Joseph
Rudyard	Rudyard	k1gInSc1	Rudyard
Kipling	Kipling	k1gInSc4	Kipling
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
ˈ	ˈ	k?	ˈ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
Bombaj	Bombaj	k1gFnSc1	Bombaj
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
britský	britský	k2eAgMnSc1d1	britský
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Rudyard	Rudyard	k1gInSc1	Rudyard
Kipling	Kipling	k1gInSc1	Kipling
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
indické	indický	k2eAgFnSc6d1	indická
Bombaji	Bombaj	k1gFnSc6	Bombaj
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
malíře	malíř	k1gMnSc2	malíř
pracujícího	pracující	k1gMnSc2	pracující
pro	pro	k7c4	pro
britskou	britský	k2eAgFnSc4d1	britská
koloniální	koloniální	k2eAgFnSc4d1	koloniální
správu	správa	k1gFnSc4	správa
<g/>
,	,	kIx,	,
dětství	dětství	k1gNnSc4	dětství
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
internátní	internátní	k2eAgFnSc2d1	internátní
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
opět	opět	k6eAd1	opět
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vojenská	vojenský	k2eAgFnSc1d1	vojenská
kariéra	kariéra	k1gFnSc1	kariéra
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
pro	pro	k7c4	pro
silnou	silný	k2eAgFnSc4d1	silná
krátkozrakost	krátkozrakost	k1gFnSc4	krátkozrakost
a	a	k8xC	a
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
bylo	být	k5eAaImAgNnS	být
příliš	příliš	k6eAd1	příliš
nákladné	nákladný	k2eAgNnSc1d1	nákladné
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
pomocníkem	pomocník	k1gMnSc7	pomocník
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
anglických	anglický	k2eAgFnPc2d1	anglická
novin	novina	k1gFnPc2	novina
v	v	k7c4	v
Láhaur	Láhaur	k1gInSc4	Láhaur
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
otiskli	otisknout	k5eAaPmAgMnP	otisknout
první	první	k4xOgInPc4	první
verše	verš	k1gInPc4	verš
a	a	k8xC	a
krátké	krátký	k2eAgFnPc4d1	krátká
povídky	povídka	k1gFnPc4	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
pak	pak	k6eAd1	pak
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
strávil	strávit	k5eAaPmAgInS	strávit
pouhých	pouhý	k2eAgInPc2d1	pouhý
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
načerpal	načerpat	k5eAaPmAgMnS	načerpat
tu	tu	k6eAd1	tu
náměty	námět	k1gInPc4	námět
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
prosadil	prosadit	k5eAaPmAgMnS	prosadit
jako	jako	k9	jako
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k9	i
jako	jako	k9	jako
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
.	.	kIx.	.
</s>
<s>
Podnikl	podniknout	k5eAaPmAgMnS	podniknout
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sňatku	sňatek	k1gInSc6	sňatek
s	s	k7c7	s
Američankou	Američanka	k1gFnSc7	Američanka
Caroline	Carolin	k1gInSc5	Carolin
Balestierovou	Balestierová	k1gFnSc7	Balestierová
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
žil	žít	k5eAaImAgMnS	žít
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
ve	v	k7c6	v
Vermontu	Vermont	k1gInSc6	Vermont
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaPmAgInS	napsat
své	svůj	k3xOyFgFnPc4	svůj
slavné	slavný	k2eAgFnPc4d1	slavná
Knihy	kniha	k1gFnPc4	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
své	svůj	k3xOyFgInPc4	svůj
další	další	k2eAgInPc4d1	další
romány	román	k1gInPc4	román
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
psal	psát	k5eAaImAgMnS	psát
i	i	k9	i
poezii	poezie	k1gFnSc3	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vlasti	vlast	k1gFnSc6	vlast
byl	být	k5eAaImAgInS	být
ceněn	ceněn	k2eAgInSc1d1	ceněn
především	především	k9	především
jako	jako	k8xS	jako
básník	básník	k1gMnSc1	básník
opěvující	opěvující	k2eAgFnSc4d1	opěvující
mužnost	mužnost	k1gFnSc4	mužnost
a	a	k8xC	a
vlastenectví	vlastenectví	k1gNnSc4	vlastenectví
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
próza	próza	k1gFnSc1	próza
nebyla	být	k5eNaImAgFnS	být
tak	tak	k6eAd1	tak
vyzdvihována	vyzdvihován	k2eAgFnSc1d1	vyzdvihována
<g/>
.	.	kIx.	.
</s>
<s>
Čtenáře	čtenář	k1gMnPc4	čtenář
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
především	především	k6eAd1	především
cizokrajnou	cizokrajný	k2eAgFnSc7d1	cizokrajná
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
popisem	popis	k1gInSc7	popis
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
zvyků	zvyk	k1gInPc2	zvyk
domorodců	domorodec	k1gMnPc2	domorodec
a	a	k8xC	a
pozoruhodným	pozoruhodný	k2eAgInSc7d1	pozoruhodný
uměleckým	umělecký	k2eAgInSc7d1	umělecký
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Přelom	přelom	k1gInSc1	přelom
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
znamenal	znamenat	k5eAaImAgInS	znamenat
vrchol	vrchol	k1gInSc1	vrchol
jeho	jeho	k3xOp3gInPc2	jeho
literárních	literární	k2eAgInPc2d1	literární
úspěchů	úspěch	k1gInPc2	úspěch
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
pozorovací	pozorovací	k2eAgInSc4d1	pozorovací
talent	talent	k1gInSc4	talent
<g/>
,	,	kIx,	,
originální	originální	k2eAgFnSc4d1	originální
představivost	představivost	k1gFnSc4	představivost
jakož	jakož	k8xC	jakož
i	i	k9	i
mužnou	mužný	k2eAgFnSc4d1	mužná
sílu	síla	k1gFnSc4	síla
idejí	idea	k1gFnPc2	idea
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
kresby	kresba	k1gFnSc2	kresba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
Kipling	Kipling	k1gInSc1	Kipling
psal	psát	k5eAaImAgInS	psát
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
již	již	k6eAd1	již
předchozích	předchozí	k2eAgInPc2d1	předchozí
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
těžce	těžce	k6eAd1	těžce
nesl	nést	k5eAaImAgMnS	nést
smrt	smrt	k1gFnSc4	smrt
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Johna	John	k1gMnSc2	John
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
osud	osud	k1gInSc1	osud
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
film	film	k1gInSc1	film
"	"	kIx"	"
<g/>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
mém	můj	k1gMnSc6	můj
synovi	syn	k1gMnSc6	syn
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
spisovatelově	spisovatelův	k2eAgFnSc6d1	spisovatelova
smrti	smrt	k1gFnSc6	smrt
na	na	k7c4	na
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ještě	ještě	k9	ještě
jeho	jeho	k3xOp3gFnSc1	jeho
autobiografie	autobiografie	k1gFnSc1	autobiografie
Something	Something	k1gInSc4	Something
of	of	k?	of
Myself	Myself	k1gInSc1	Myself
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kipling	Kipling	k1gInSc1	Kipling
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
britské	britský	k2eAgNnSc4d1	Britské
impérium	impérium	k1gNnSc4	impérium
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
síle	síla	k1gFnSc6	síla
a	a	k8xC	a
velikosti	velikost	k1gFnSc6	velikost
a	a	k8xC	a
jako	jako	k8xC	jako
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
vlastenec	vlastenec	k1gMnSc1	vlastenec
a	a	k8xC	a
humanista	humanista	k1gMnSc1	humanista
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
Brity	Brit	k1gMnPc4	Brit
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Indům	Ind	k1gMnPc3	Ind
jako	jako	k8xC	jako
velké	velký	k2eAgMnPc4d1	velký
bílé	bílý	k2eAgMnPc4d1	bílý
bratry	bratr	k1gMnPc4	bratr
s	s	k7c7	s
povinností	povinnost	k1gFnSc7	povinnost
vést	vést	k5eAaImF	vést
je	on	k3xPp3gInPc4	on
a	a	k8xC	a
učit	učit	k5eAaImF	učit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc4	ten
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
básni	báseň	k1gFnSc6	báseň
Břímě	břímě	k1gNnSc1	břímě
bělochů	běloch	k1gMnPc2	běloch
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
White	Whit	k1gMnSc5	Whit
Man	mana	k1gFnPc2	mana
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Burden	Burdna	k1gFnPc2	Burdna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
o	o	k7c6	o
domorodcích	domorodec	k1gMnPc6	domorodec
nevyjadřoval	vyjadřovat	k5eNaImAgMnS	vyjadřovat
ale	ale	k8xC	ale
příliš	příliš	k6eAd1	příliš
vlídně	vlídně	k6eAd1	vlídně
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pohanů	pohan	k1gMnPc2	pohan
blud	blud	k1gInSc1	blud
a	a	k8xC	a
tupost	tupost	k1gFnSc1	tupost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
často	často	k6eAd1	často
označován	označován	k2eAgMnSc1d1	označován
za	za	k7c4	za
imperialistu	imperialista	k1gMnSc4	imperialista
a	a	k8xC	a
horlivého	horlivý	k2eAgMnSc4d1	horlivý
zastánce	zastánce	k1gMnSc4	zastánce
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nemohl	moct	k5eNaImAgInS	moct
upřít	upřít	k5eAaPmF	upřít
talent	talent	k1gInSc4	talent
velkého	velký	k2eAgMnSc2d1	velký
vypravěče	vypravěč	k1gMnSc2	vypravěč
<g/>
.	.	kIx.	.
</s>
<s>
Proslulá	proslulý	k2eAgFnSc1d1	proslulá
je	být	k5eAaImIp3nS	být
také	také	k9	také
jeho	jeho	k3xOp3gFnSc1	jeho
báseň	báseň	k1gFnSc1	báseň
If	If	k1gFnSc1	If
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
přebásnění	přebásnění	k1gNnSc6	přebásnění
Otokara	Otokar	k1gMnSc2	Otokar
Fischera	Fischer	k1gMnSc2	Fischer
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Když	když	k8xS	když
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgInPc4	který
Kipling	Kipling	k1gInSc1	Kipling
vystihl	vystihnout	k5eAaPmAgInS	vystihnout
obraz	obraz	k1gInSc4	obraz
a	a	k8xC	a
ideál	ideál	k1gInSc4	ideál
mužných	mužný	k2eAgFnPc2d1	mužná
vlastností	vlastnost	k1gFnPc2	vlastnost
a	a	k8xC	a
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
statečného	statečný	k2eAgMnSc4d1	statečný
člověka	člověk	k1gMnSc4	člověk
v	v	k7c6	v
životním	životní	k2eAgInSc6d1	životní
zápase	zápas	k1gInSc6	zápas
v	v	k7c6	v
přemáhání	přemáhání	k1gNnSc6	přemáhání
každodenních	každodenní	k2eAgInPc2d1	každodenní
všelidských	všelidský	k2eAgInPc2d1	všelidský
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Departmental	Departmental	k1gMnSc1	Departmental
Ditties	Ditties	k1gMnSc1	Ditties
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
Oddílové	oddílový	k2eAgFnSc2d1	oddílová
popěvky	popěvka	k1gFnSc2	popěvka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Barrack-Room	Barrack-Room	k1gInSc4	Barrack-Room
Ballads	Balladsa	k1gFnPc2	Balladsa
and	and	k?	and
Other	Other	k1gMnSc1	Other
Verses	Verses	k1gMnSc1	Verses
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Kasárenské	kasárenský	k2eAgFnSc2d1	kasárenská
balady	balada	k1gFnSc2	balada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc7	The
Seven	Sevna	k1gFnPc2	Sevna
Seas	Seas	k1gInSc1	Seas
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Sedm	sedm	k4xCc1	sedm
moří	moře	k1gNnPc2	moře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Five	Fiv	k1gFnSc2	Fiv
Nations	Nations	k1gInSc1	Nations
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Pět	pět	k4xCc4	pět
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc7	The
Years	Yearsa	k1gFnPc2	Yearsa
Between	Between	k1gInSc1	Between
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
Plain	Plain	k1gMnSc1	Plain
Tales	Tales	k1gMnSc1	Tales
From	From	k1gMnSc1	From
the	the	k?	the
Hills	Hills	k1gInSc1	Hills
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Prosté	prostý	k2eAgFnPc4d1	prostá
povídky	povídka	k1gFnPc4	povídka
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
krátkých	krátká	k1gFnPc2	krátká
povídek	povídka	k1gFnPc2	povídka
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
Kipling	Kipling	k1gInSc1	Kipling
využil	využít	k5eAaPmAgMnS	využít
společenských	společenský	k2eAgFnPc2d1	společenská
anekdot	anekdota	k1gFnPc2	anekdota
i	i	k8xC	i
skutečných	skutečný	k2eAgInPc2d1	skutečný
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
je	být	k5eAaImIp3nS	být
podat	podat	k5eAaPmF	podat
skutečně	skutečně	k6eAd1	skutečně
prostě	prostě	k6eAd1	prostě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přitom	přitom	k6eAd1	přitom
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
dramatičnost	dramatičnost	k1gFnSc4	dramatičnost
<g/>
,	,	kIx,	,
humornost	humornost	k1gFnSc4	humornost
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
tragiku	tragika	k1gFnSc4	tragika
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
živé	živý	k2eAgInPc4d1	živý
<g/>
,	,	kIx,	,
autentické	autentický	k2eAgInPc4d1	autentický
<g/>
,	,	kIx,	,
pozorovatelsky	pozorovatelsky	k6eAd1	pozorovatelsky
přesné	přesný	k2eAgNnSc4d1	přesné
a	a	k8xC	a
přitažlivé	přitažlivý	k2eAgNnSc4d1	přitažlivé
vyprávění	vyprávění	k1gNnSc4	vyprávění
prokládá	prokládat	k5eAaImIp3nS	prokládat
autor	autor	k1gMnSc1	autor
vtipy	vtip	k1gInPc4	vtip
a	a	k8xC	a
aforismy	aforismus	k1gInPc4	aforismus
a	a	k8xC	a
stupňuje	stupňovat	k5eAaImIp3nS	stupňovat
je	on	k3xPp3gInPc4	on
až	až	k9	až
do	do	k7c2	do
působivé	působivý	k2eAgFnSc2d1	působivá
pointy	pointa	k1gFnSc2	pointa
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
za	za	k7c7	za
demokratičností	demokratičnost	k1gFnSc7	demokratičnost
Kiplingova	Kiplingův	k2eAgInSc2d1	Kiplingův
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
námětů	námět	k1gInPc2	námět
lpí	lpět	k5eAaImIp3nS	lpět
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
jenom	jenom	k9	jenom
britské	britský	k2eAgFnSc2d1	britská
organizační	organizační	k2eAgFnSc2d1	organizační
a	a	k8xC	a
technické	technický	k2eAgFnSc2d1	technická
schopnosti	schopnost	k1gFnSc2	schopnost
mohou	moct	k5eAaImIp3nP	moct
udržet	udržet	k5eAaPmF	udržet
na	na	k7c6	na
uzdě	uzda	k1gFnSc6	uzda
zaostalé	zaostalý	k2eAgNnSc1d1	zaostalé
domorodé	domorodý	k2eAgNnSc1d1	domorodé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Soldiers	Soldiers	k1gInSc1	Soldiers
Three	Thre	k1gFnSc2	Thre
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Tři	tři	k4xCgMnPc1	tři
vojáci	voják	k1gMnPc1	voják
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Tři	tři	k4xCgMnPc1	tři
mušketýři	mušketýr	k1gMnPc1	mušketýr
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc1	některý
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
ze	z	k7c2	z
života	život	k1gInSc2	život
a	a	k8xC	a
činů	čin	k1gInPc2	čin
pěšáků	pěšák	k1gMnPc2	pěšák
Terence	Terence	k1gFnSc2	Terence
Mulvaneyho	Mulvaney	k1gMnSc2	Mulvaney
<g/>
,	,	kIx,	,
Stanleyho	Stanley	k1gMnSc2	Stanley
Ortherise	Ortherise	k1gFnSc2	Ortherise
a	a	k8xC	a
Johna	John	k1gMnSc2	John
Learoyda	Learoyd	k1gMnSc2	Learoyd
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnSc2	sbírka
povídek	povídka	k1gFnPc2	povídka
In	In	k1gMnSc1	In
Black	Black	k1gMnSc1	Black
and	and	k?	and
White	Whit	k1gInSc5	Whit
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Černé	Černé	k2eAgInPc1d1	Černé
na	na	k7c6	na
bílém	bílé	k1gNnSc6	bílé
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Story	story	k1gFnSc2	story
of	of	k?	of
the	the	k?	the
Gadsbys	Gadsbys	k1gInSc1	Gadsbys
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Příběh	příběh	k1gInSc1	příběh
Gadsbyových	Gadsbyová	k1gFnPc2	Gadsbyová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Phantom	Phantom	k1gInSc1	Phantom
Rickshaw	Rickshaw	k1gFnSc1	Rickshaw
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Fantóm	fantóm	k1gInSc4	fantóm
nosítek	nosítka	k1gNnPc2	nosítka
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Přízraky	přízrak	k1gInPc1	přízrak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Under	Under	k1gInSc1	Under
the	the	k?	the
Deodars	Deodars	k1gInSc1	Deodars
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Hrobaři	hrobař	k1gMnSc5	hrobař
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indian	Indiana	k1gFnPc2	Indiana
Tales	Talesa	k1gFnPc2	Talesa
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Obrázky	obrázek	k1gInPc4	obrázek
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Light	Light	k1gMnSc1	Light
That	That	k1gMnSc1	That
Failed	Failed	k1gMnSc1	Failed
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zhaslo	zhasnout	k5eAaPmAgNnS	zhasnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
s	s	k7c7	s
autobiografickými	autobiografický	k2eAgInPc7d1	autobiografický
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
American	American	k1gInSc1	American
Notes	notes	k1gInSc1	notes
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
,	,	kIx,	,
Americké	americký	k2eAgFnPc1d1	americká
poznámky	poznámka	k1gFnPc1	poznámka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Naulahka	Naulahka	k1gMnSc1	Naulahka
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Náhrdelník	náhrdelník	k1gInSc1	náhrdelník
maharadžů	maharadž	k1gMnPc2	maharadž
nebo	nebo	k8xC	nebo
Za	za	k7c7	za
kouzlem	kouzlo	k1gNnSc7	kouzlo
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc4	příběh
Američana	Američan	k1gMnSc4	Američan
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
napsáno	napsat	k5eAaPmNgNnS	napsat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Charlesem	Charles	k1gMnSc7	Charles
Wolcottem	Wolcott	k1gMnSc7	Wolcott
Balestierem	Balestier	k1gMnSc7	Balestier
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
-	-	kIx~	-
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Many	mana	k1gFnPc1	mana
Inventions	Inventionsa	k1gFnPc2	Inventionsa
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Fantasie	fantasie	k1gFnSc1	fantasie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgNnSc4	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
povídka	povídka	k1gFnSc1	povídka
In	In	k1gMnSc1	In
the	the	k?	the
Rukh	Rukh	k1gMnSc1	Rukh
(	(	kIx(	(
<g/>
Na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
džungle	džungle	k1gFnSc2	džungle
nebo	nebo	k8xC	nebo
také	také	k9	také
V	v	k7c6	v
rukhu	rukh	k1gInSc6	rukh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgNnSc7	první
vydaným	vydaný	k2eAgNnSc7d1	vydané
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
chronologicky	chronologicky	k6eAd1	chronologicky
posledním	poslední	k2eAgInSc7d1	poslední
příběhem	příběh	k1gInSc7	příběh
Mauglího	Mauglí	k2eAgNnSc2d1	Mauglí
z	z	k7c2	z
Knih	kniha	k1gFnPc2	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Jungle	Jungle	k1gInSc1	Jungle
Book	Book	k1gInSc1	Book
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gMnSc1	The
Second	Second	k1gMnSc1	Second
Jungle	Jungle	k1gFnSc2	Jungle
Book	Book	k1gMnSc1	Book
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Druhá	druhý	k4xOgFnSc1	druhý
kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
jako	jako	k9	jako
Knihy	kniha	k1gFnPc1	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
,	,	kIx,	,
dvě	dva	k4xCgNnPc4	dva
nejznámější	známý	k2eAgNnPc4d3	nejznámější
autorova	autorův	k2eAgNnPc4d1	autorovo
díla	dílo	k1gNnPc4	dílo
připomínající	připomínající	k2eAgInSc4d1	připomínající
středověký	středověký	k2eAgInSc4d1	středověký
epos	epos	k1gInSc4	epos
o	o	k7c6	o
zvířatech	zvíře	k1gNnPc6	zvíře
s	s	k7c7	s
ústředním	ústřední	k2eAgInSc7d1	ústřední
příběhem	příběh	k1gInSc7	příběh
indického	indický	k2eAgMnSc2d1	indický
chlapce	chlapec	k1gMnSc2	chlapec
Mauglího	Mauglí	k2eAgMnSc2d1	Mauglí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
bezbranné	bezbranný	k2eAgNnSc1d1	bezbranné
batole	batole	k1gNnSc1	batole
octne	octnout	k5eAaPmIp3nS	octnout
v	v	k7c6	v
džungli	džungle	k1gFnSc6	džungle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prožívá	prožívat	k5eAaImIp3nS	prožívat
mnohá	mnohý	k2eAgNnPc4d1	mnohé
dobrodružství	dobrodružství	k1gNnPc4	dobrodružství
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
medvědem	medvěd	k1gMnSc7	medvěd
Balúem	Balú	k1gMnSc7	Balú
<g/>
,	,	kIx,	,
hadem	had	k1gMnSc7	had
Ká	ká	k0	ká
<g/>
,	,	kIx,	,
tygrem	tygr	k1gMnSc7	tygr
Šér	Šér	k1gMnSc7	Šér
Chánem	chán	k1gMnSc7	chán
a	a	k8xC	a
pardálem	pardál	k1gMnSc7	pardál
Baghírou	Baghíra	k1gMnSc7	Baghíra
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
Mauglího	Mauglí	k2eAgInSc2d1	Mauglí
příhody	příhoda	k1gFnPc4	příhoda
vydávány	vydáván	k2eAgFnPc1d1	vydávána
samostatně	samostatně	k6eAd1	samostatně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Captains	Captains	k1gInSc1	Captains
Courageous	Courageous	k1gInSc1	Courageous
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Stateční	statečný	k2eAgMnPc1d1	statečný
kapitáni	kapitán	k1gMnPc1	kapitán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dobrodružný	dobrodružný	k2eAgInSc1d1	dobrodružný
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
líčí	líčit	k5eAaImIp3nS	líčit
příběh	příběh	k1gInSc1	příběh
patnáctiletého	patnáctiletý	k2eAgMnSc2d1	patnáctiletý
syna	syn	k1gMnSc2	syn
amerického	americký	k2eAgMnSc2d1	americký
milionáře	milionář	k1gMnSc2	milionář
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
pohodlný	pohodlný	k2eAgInSc4d1	pohodlný
život	život	k1gInSc4	život
a	a	k8xC	a
peníze	peníz	k1gInPc4	peníz
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
scestí	scestí	k1gNnSc4	scestí
a	a	k8xC	a
kterého	který	k3yIgMnSc4	který
zachrání	zachránit	k5eAaPmIp3nS	zachránit
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
život	život	k1gInSc1	život
plavčíka	plavčík	k1gMnSc2	plavčík
na	na	k7c6	na
rybářském	rybářský	k2eAgInSc6d1	rybářský
škuneru	škuner	k1gInSc6	škuner
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
z	z	k7c2	z
nutnosti	nutnost	k1gFnSc2	nutnost
podřídit	podřídit	k5eAaPmF	podřídit
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
The	The	k1gMnSc1	The
Day	Day	k1gMnSc1	Day
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Work	Worka	k1gFnPc2	Worka
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Denní	denní	k2eAgFnSc1d1	denní
práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
From	From	k1gMnSc1	From
Sea	Sea	k1gMnSc1	Sea
to	ten	k3xDgNnSc4	ten
Sea	Sea	k1gFnSc1	Sea
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Od	od	k7c2	od
moře	moře	k1gNnSc2	moře
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestopisné	cestopisný	k2eAgInPc1d1	cestopisný
obrázky	obrázek	k1gInPc1	obrázek
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc2d1	spojený
<g />
.	.	kIx.	.
</s>
<s>
států	stát	k1gInPc2	stát
a	a	k8xC	a
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Stalky	Stalka	k1gFnSc2	Stalka
&	&	k?	&
Co	co	k9	co
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Stopka	stopka	k1gFnSc1	stopka
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
humorná	humorný	k2eAgFnSc1d1	humorná
knížka	knížka	k1gFnSc1	knížka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
s	s	k7c7	s
pochopením	pochopení	k1gNnSc7	pochopení
líčí	líčit	k5eAaImIp3nS	líčit
bujaré	bujarý	k2eAgFnSc2d1	bujará
příhody	příhoda	k1gFnSc2	příhoda
tří	tři	k4xCgMnPc2	tři
vynalézavých	vynalézavý	k2eAgMnPc2d1	vynalézavý
chlapců	chlapec	k1gMnPc2	chlapec
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
internátní	internátní	k2eAgFnSc6d1	internátní
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
Kim	Kim	k1gFnSc6	Kim
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
špionážní	špionážní	k2eAgInSc1d1	špionážní
román	román	k1gInSc1	román
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
napínavý	napínavý	k2eAgInSc4d1	napínavý
příběh	příběh	k1gInSc4	příběh
irského	irský	k2eAgMnSc2d1	irský
sirotka	sirotek	k1gMnSc2	sirotek
vychovávaného	vychovávaný	k2eAgInSc2d1	vychovávaný
tibetským	tibetský	k2eAgMnSc7d1	tibetský
lámou	láma	k1gMnSc7	láma
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zaplete	zaplést	k5eAaPmIp3nS	zaplést
do	do	k7c2	do
vyzvědačských	vyzvědačský	k2eAgFnPc2d1	vyzvědačská
praktik	praktika	k1gFnPc2	praktika
<g/>
,	,	kIx,	,
Just	just	k6eAd1	just
So	So	kA	So
Stories	Stories	k1gInSc1	Stories
for	forum	k1gNnPc2	forum
Little	Little	k1gFnSc2	Little
Children	Childrno	k1gNnPc2	Childrno
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
Bajky	bajka	k1gFnPc1	bajka
i	i	k8xC	i
nebajky	nebajka	k1gFnPc1	nebajka
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Příběhy	příběh	k1gInPc1	příběh
jen	jen	k6eAd1	jen
tak	tak	k6eAd1	tak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veselé	veselý	k2eAgNnSc4d1	veselé
<g/>
,	,	kIx,	,
hravé	hravý	k2eAgNnSc4d1	hravé
a	a	k8xC	a
vynalézavé	vynalézavý	k2eAgNnSc4d1	vynalézavé
<g />
.	.	kIx.	.
</s>
<s>
krátké	krátký	k2eAgInPc4d1	krátký
exotické	exotický	k2eAgInPc4d1	exotický
příběhy	příběh	k1gInPc4	příběh
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
plné	plný	k2eAgFnPc4d1	plná
slovních	slovní	k2eAgFnPc2d1	slovní
hříček	hříčka	k1gFnPc2	hříčka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
Kipling	Kipling	k1gInSc1	Kipling
doprovodil	doprovodit	k5eAaPmAgInS	doprovodit
vlastními	vlastní	k2eAgFnPc7d1	vlastní
ilustracemi	ilustrace	k1gFnPc7	ilustrace
<g/>
,	,	kIx,	,
svědčícími	svědčící	k2eAgFnPc7d1	svědčící
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
pozoruhodném	pozoruhodný	k2eAgInSc6d1	pozoruhodný
výtvarném	výtvarný	k2eAgInSc6d1	výtvarný
talentu	talent	k1gInSc6	talent
<g/>
,	,	kIx,	,
Puck	Puck	k1gInSc1	Puck
of	of	k?	of
Pook	Pook	k1gInSc1	Pook
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Hill	Hilla	k1gFnPc2	Hilla
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Šotkova	šotkův	k2eAgNnSc2d1	šotkův
kouzla	kouzlo	k1gNnSc2	kouzlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rewards	Rewards	k1gInSc1	Rewards
and	and	k?	and
Fairies	Fairies	k1gInSc1	Fairies
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Nová	nový	k2eAgNnPc1d1	nové
šotkova	šotkův	k2eAgNnPc1d1	šotkův
kouzla	kouzlo	k1gNnPc1	kouzlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohádkově	pohádkově	k6eAd1	pohádkově
laděná	laděný	k2eAgNnPc4d1	laděné
dílka	dílko	k1gNnPc4	dílko
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnSc2	sbírka
povídek	povídka	k1gFnPc2	povídka
s	s	k7c7	s
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
tematikou	tematika	k1gFnSc7	tematika
A	a	k8xC	a
Diversity	Diversit	k1gInPc1	Diversit
of	of	k?	of
Creatures	Creatures	k1gInSc1	Creatures
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Rozličné	rozličný	k2eAgFnSc6d1	rozličná
bytosti	bytost	k1gFnSc6	bytost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Graves	Graves	k1gMnSc1	Graves
of	of	k?	of
the	the	k?	the
Fallen	Fallen	k1gInSc1	Fallen
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Hroby	hrob	k1gInPc1	hrob
padlých	padlý	k1gMnPc2	padlý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Letters	Letters	k1gInSc1	Letters
<g />
.	.	kIx.	.
</s>
<s>
of	of	k?	of
Travel	Travel	k1gInSc1	Travel
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Listy	lista	k1gFnPc1	lista
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
)	)	kIx)	)
Debits	Debits	k1gInSc1	Debits
and	and	k?	and
Credists	Credists	k1gInSc1	Credists
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Dluhy	dluh	k1gInPc4	dluh
a	a	k8xC	a
úvěry	úvěr	k1gInPc4	úvěr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Povídky	povídka	k1gFnSc2	povídka
zednářské	zednářský	k2eAgNnSc4d1	zednářské
lože	lože	k1gNnSc4	lože
<g/>
,	,	kIx,	,
Thy	Thy	k1gFnSc1	Thy
Servant	servanta	k1gFnPc2	servanta
Dog	doga	k1gFnPc2	doga
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Tvůj	tvůj	k3xOp2gMnSc1	tvůj
sluha	sluha	k1gMnSc1	sluha
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Something	Something	k1gInSc1	Something
of	of	k?	of
Myself	Myself	k1gInSc1	Myself
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
Něco	něco	k3yInSc1	něco
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autobiografie	autobiografie	k1gFnSc1	autobiografie
vydaná	vydaný	k2eAgFnSc1d1	vydaná
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Sládek	Sládek	k1gMnSc1	Sládek
<g/>
,	,	kIx,	,
Zhaslé	zhaslý	k2eAgNnSc1d1	zhaslé
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Několik	několik	k4yIc1	několik
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
,	,	kIx,	,
Kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
a	a	k8xC	a
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
,	,	kIx,	,
Edvard	Edvard	k1gMnSc1	Edvard
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
Za	za	k7c7	za
kouzlem	kouzlo	k1gNnSc7	kouzlo
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Alois	Alois	k1gMnSc1	Alois
H.	H.	kA	H.
Šmíd	Šmíd	k1gMnSc1	Šmíd
<g/>
,	,	kIx,	,
Povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
<g/>
,	,	kIx,	,
Kim	Kim	k1gMnSc1	Kim
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
ještě	ještě	k9	ještě
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Josef	Josefa	k1gFnPc2	Josefa
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
a	a	k8xC	a
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Pohádky	pohádka	k1gFnPc1	pohádka
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Aventinum	Aventinum	k1gNnSc1	Aventinum
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
a	a	k8xC	a
zakladatelství	zakladatelství	k1gNnSc4	zakladatelství
Akcent	akcent	k1gInSc1	akcent
v	v	k7c6	v
Třebíči	Třebíč	k1gFnSc6	Třebíč
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Stateční	statečný	k2eAgMnPc1d1	statečný
kapitáni	kapitán	k1gMnPc1	kapitán
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
ještě	ještě	k9	ještě
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
a	a	k8xC	a
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Zpracováno	zpracovat	k5eAaPmNgNnS	zpracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
rozhlasu	rozhlas	k1gInSc6	rozhlas
jako	jako	k8xC	jako
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
úprava	úprava	k1gFnSc1	úprava
Martina	Martina	k1gFnSc1	Martina
Drijverová	Drijverová	k1gFnSc1	Drijverová
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pergl	Pergl	k1gMnSc1	Pergl
<g/>
,	,	kIx,	,
dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
Ivan	Ivan	k1gMnSc1	Ivan
Hubač	hubačit	k5eAaImRp2nS	hubačit
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Maria	Maria	k1gFnSc1	Maria
Křepelková	Křepelková	k1gFnSc1	Křepelková
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Vít	vít	k5eAaImF	vít
Ondračka	Ondračka	k1gFnSc1	Ondračka
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
Bürger	Bürger	k1gMnSc1	Bürger
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Pelzer	Pelzer	k1gMnSc1	Pelzer
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Skopeček	skopeček	k1gMnSc1	skopeček
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Švehlík	Švehlík	k1gMnSc1	Švehlík
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Gübel	Gübel	k1gMnSc1	Gübel
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Trojan	Trojan	k1gMnSc1	Trojan
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
Syslová	Syslová	k1gFnSc1	Syslová
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Županič	Županič	k1gMnSc1	Županič
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Richter	Richter	k1gMnSc1	Richter
<g/>
.	.	kIx.	.
</s>
<s>
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
.	.	kIx.	.
</s>
<s>
Fantom	fantom	k1gInSc1	fantom
nosítek	nosítka	k1gNnPc2	nosítka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Cyril	Cyril	k1gMnSc1	Cyril
Žďárský	Žďárský	k1gMnSc1	Žďárský
<g/>
,	,	kIx,	,
S	s	k7c7	s
pokraje	pokraj	k1gInSc2	pokraj
džunglí	džungle	k1gFnPc2	džungle
<g/>
,	,	kIx,	,
Aloys	Aloysa	k1gFnPc2	Aloysa
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
J.	J.	kA	J.
O.	O.	kA	O.
Hart	Hart	k1gMnSc1	Hart
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dvě	dva	k4xCgFnPc4	dva
povídky	povídka	k1gFnPc4	povídka
<g/>
:	:	kIx,	:
V	v	k7c6	v
rukhu	rukh	k1gInSc6	rukh
a	a	k8xC	a
Podivná	podivný	k2eAgFnSc1d1	podivná
jízda	jízda	k1gFnSc1	jízda
Morrowbie	Morrowbie	k1gFnSc1	Morrowbie
Jukesa	Jukesa	k1gFnSc1	Jukesa
<g/>
.	.	kIx.	.
</s>
<s>
Stopka	stopka	k1gFnSc1	stopka
<g/>
,	,	kIx,	,
Brouk	brouk	k1gMnSc1	brouk
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
ještě	ještě	k9	ještě
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Josef	Josef	k1gMnSc1	Josef
Boš	boš	k1gMnSc1	boš
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Toužimský	Toužimský	k2eAgMnSc1d1	Toužimský
a	a	k8xC	a
Moravec	Moravec	k1gMnSc1	Moravec
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Šotkova	šotkův	k2eAgNnSc2d1	šotkův
kouzla	kouzlo	k1gNnSc2	kouzlo
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
Fantasie	fantasie	k1gFnPc1	fantasie
<g/>
:	:	kIx,	:
smyšlenky	smyšlenka	k1gFnPc1	smyšlenka
rozličné	rozličný	k2eAgFnPc1d1	rozličná
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Julie	Julie	k1gFnSc1	Julie
Novotná	Novotná	k1gFnSc1	Novotná
<g/>
,	,	kIx,	,
Kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnPc2	džungle
a	a	k8xC	a
Druhá	druhý	k4xOgFnSc1	druhý
kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Miloš	Miloš	k1gMnSc1	Miloš
Maixner	Maixner	k1gMnSc1	Maixner
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tyto	tento	k3xDgFnPc4	tento
knihy	kniha	k1gFnPc4	kniha
toto	tento	k3xDgNnSc4	tento
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
a	a	k8xC	a
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Přízraky	přízrak	k1gInPc1	přízrak
<g/>
,	,	kIx,	,
Příběh	příběh	k1gInSc1	příběh
Gadsbyův	Gadsbyův	k2eAgInSc1d1	Gadsbyův
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
Nová	Nová	k1gFnSc1	Nová
šotkova	šotkův	k2eAgNnSc2d1	šotkův
kouzla	kouzlo	k1gNnSc2	kouzlo
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
Bez	bez	k7c2	bez
božího	boží	k2eAgNnSc2d1	boží
požehnání	požehnání	k1gNnSc2	požehnání
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Sládek	Sládek	k1gMnSc1	Sládek
<g/>
,	,	kIx,	,
Prosté	prostý	k2eAgFnPc1d1	prostá
povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
indických	indický	k2eAgFnPc2d1	indická
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Lydia	Lydium	k1gNnSc2	Lydium
Kolátorová	Kolátorový	k2eAgFnSc1d1	Kolátorová
<g/>
,	,	kIx,	,
Naulahka	Naulahka	k1gFnSc1	Naulahka
<g/>
,	,	kIx,	,
náhrdelník	náhrdelník	k1gInSc1	náhrdelník
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Lydia	Lydium	k1gNnSc2	Lydium
Kolátorová	Kolátorový	k2eAgFnSc1d1	Kolátorová
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Náhrdelník	náhrdelník	k1gInSc1	náhrdelník
marahadžů	marahadž	k1gInPc2	marahadž
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
ještě	ještě	k9	ještě
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
a	a	k8xC	a
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
moře	moře	k1gNnSc2	moře
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Miloš	Miloš	k1gMnSc1	Miloš
Maixner	Maixner	k1gMnSc1	Maixner
<g/>
,	,	kIx,	,
Hrobaři	hrobař	k1gMnPc1	hrobař
<g/>
,	,	kIx,	,
Šolc	Šolc	k1gMnSc1	Šolc
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
M.	M.	kA	M.
Satranová	Satranová	k1gFnSc1	Satranová
<g/>
,	,	kIx,	,
Záhady	záhada	k1gFnPc1	záhada
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Šolc	Šolc	k1gMnSc1	Šolc
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vykydal	vykydat	k5eAaPmAgMnS	vykydat
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Světla	světlo	k1gNnPc1	světlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
zhasla	zhasnout	k5eAaPmAgNnP	zhasnout
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
L.	L.	kA	L.
Vojta	vojt	k1gMnSc2	vojt
<g/>
,	,	kIx,	,
Lispeth	Lispeth	k1gInSc1	Lispeth
<g/>
,	,	kIx,	,
Holešov	Holešov	k1gInSc1	Holešov
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Marie	Maria	k1gFnSc2	Maria
Jaminová	Jaminová	k1gFnSc1	Jaminová
<g/>
,	,	kIx,	,
Stavitelé	stavitel	k1gMnPc1	stavitel
mostu	most	k1gInSc2	most
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Boš	boš	k1gMnSc1	boš
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
František	František	k1gMnSc1	František
Kolátor	kolátor	k1gMnSc1	kolátor
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Družstevní	družstevní	k2eAgFnSc2d1	družstevní
práce	práce	k1gFnSc2	práce
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Maltézský	maltézský	k2eAgInSc1d1	maltézský
kocour	kocour	k1gInSc1	kocour
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Boš	boš	k1gMnSc1	boš
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Julie	Julie	k1gFnSc2	Julie
Novotná-Procházková	Novotná-Procházkový	k2eAgFnSc1d1	Novotná-Procházkový
<g/>
,	,	kIx,	,
Prosté	prostý	k2eAgFnPc1d1	prostá
povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Weinfurter	Weinfurter	k1gMnSc1	Weinfurter
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černá	k1gFnPc1	Černá
na	na	k7c6	na
bílém	bílé	k1gNnSc6	bílé
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Elsie	Elsie	k1gFnSc2	Elsie
Havlasová	Havlasová	k1gFnSc1	Havlasová
<g/>
,	,	kIx,	,
Kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnPc2	džungle
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Viktor	Viktor	k1gMnSc1	Viktor
Messi	Messe	k1gFnSc4	Messe
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
a	a	k8xC	a
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnSc7	džungle
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Viktor	Viktor	k1gMnSc1	Viktor
Messi	Messe	k1gFnSc4	Messe
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
a	a	k8xC	a
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
zhaslo	zhasnout	k5eAaPmAgNnS	zhasnout
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ladislav	Ladislav	k1gMnSc1	Ladislav
Vojtig	Vojtig	k1gMnSc1	Vojtig
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Strom	strom	k1gInSc1	strom
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
Šotkova	šotkův	k2eAgNnPc4d1	šotkův
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Welandův	Welandův	k2eAgInSc1d1	Welandův
meč	meč	k1gInSc1	meč
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
Šotkova	šotkův	k2eAgNnPc1d1	šotkův
kouzla	kouzlo	k1gNnPc1	kouzlo
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
zednářské	zednářský	k2eAgFnSc2d1	zednářská
lóže	lóže	k1gFnSc2	lóže
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Alexandr	Alexandr	k1gMnSc1	Alexandr
Fleischr	Fleischr	k1gMnSc1	Fleischr
<g/>
,	,	kIx,	,
Tvůj	tvůj	k3xOp2gMnSc1	tvůj
sluha	sluha	k1gMnSc1	sluha
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Kiplingovy	Kiplingův	k2eAgFnPc1d1	Kiplingova
povídky	povídka	k1gFnPc1	povídka
o	o	k7c6	o
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
Námořní	námořní	k2eAgFnSc1d1	námořní
vzpoura	vzpoura	k1gFnSc1	vzpoura
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Bohumil	Bohumil	k1gMnSc1	Bohumil
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
<g/>
,	,	kIx,	,
Písně	píseň	k1gFnPc1	píseň
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
Zlín	Zlín	k1gInSc1	Zlín
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Otokar	Otokar	k1gMnSc1	Otokar
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
výbor	výbor	k1gInSc1	výbor
básní	báseň	k1gFnPc2	báseň
vyšel	vyjít	k5eAaPmAgInS	vyjít
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Vilém	Vilém	k1gMnSc1	Vilém
Smidt	Smidt	k1gMnSc1	Smidt
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
a	a	k8xC	a
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
v	v	k7c6	v
hodonínském	hodonínský	k2eAgNnSc6d1	hodonínské
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kolařík	Kolařík	k1gMnSc1	Kolařík
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
a	a	k8xC	a
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Primus	primus	k1gInSc1	primus
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
toku	tok	k1gInSc2	tok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k9	když
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Chvála	chvála	k1gFnSc1	chvála
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Otokar	Otokar	k1gMnSc1	Otokar
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
báseň	báseň	k1gFnSc4	báseň
ještě	ještě	k6eAd1	ještě
vydalo	vydat	k5eAaPmAgNnS	vydat
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
překladu	překlad	k1gInSc6	překlad
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
Mrázek	Mrázek	k1gMnSc1	Mrázek
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Štěpaník	Štěpaník	k1gMnSc1	Štěpaník
<g/>
,	,	kIx,	,
Mauglí	Mauglý	k1gMnPc1	Mauglý
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hobzík	Hobzík	k1gMnSc1	Hobzík
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
Knih	kniha	k1gFnPc2	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
indický	indický	k2eAgMnSc1d1	indický
chlapec	chlapec	k1gMnSc1	chlapec
Mauglí	Maugle	k1gFnPc2	Maugle
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnPc2	džungle
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,,	,,	k?	,,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hobzík	Hobzík	k1gMnSc1	Hobzík
<g/>
,	,	kIx,	,
Sloní	sloní	k2eAgNnSc1d1	sloní
mládě	mládě	k1gNnSc1	mládě
<g/>
,	,	kIx,	,
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgFnSc1d1	moudrá
<g/>
,	,	kIx,	,
Stateční	statečný	k2eAgMnPc1d1	statečný
kapitáni	kapitán	k1gMnPc1	kapitán
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hobzík	Hobzík	k1gMnSc1	Hobzík
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
ještě	ještě	k6eAd1	ještě
Lidové	lidový	k2eAgNnSc1d1	lidové
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Svoboda	Svoboda	k1gMnSc1	Svoboda
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
mušketýři	mušketýr	k1gMnPc1	mušketýr
<g/>
,	,	kIx,	,
Toužimský	Toužimský	k2eAgMnSc1d1	Toužimský
a	a	k8xC	a
Moravec	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Viktor	Viktor	k1gMnSc1	Viktor
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Mauglí	Mauglí	k1gNnSc1	Mauglí
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hobzík	Hobzík	k1gMnSc1	Hobzík
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Beránek	Beránek	k1gMnSc1	Beránek
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
z	z	k7c2	z
Knih	kniha	k1gFnPc2	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
indický	indický	k2eAgMnSc1d1	indický
chlapec	chlapec	k1gMnSc1	chlapec
Mauglí	Mauglí	k1gNnSc2	Mauglí
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
ještě	ještě	k9	ještě
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
a	a	k8xC	a
1960	[number]	k4	1960
a	a	k8xC	a
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Alternativa	alternativa	k1gFnSc1	alternativa
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Bajky	bajka	k1gFnPc1	bajka
i	i	k8xC	i
nebajky	nebajka	k1gFnPc1	nebajka
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hobzík	Hobzík	k1gMnSc1	Hobzík
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
ještě	ještě	k9	ještě
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Rikki-tikki-tavi	Rikkiikkiavit	k5eAaPmRp2nS	Rikki-tikki-tavit
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
povídky	povídka	k1gFnPc4	povídka
o	o	k7c6	o
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Wanda	Wanda	k1gFnSc1	Wanda
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
povídek	povídka	k1gFnPc2	povídka
z	z	k7c2	z
Knih	kniha	k1gFnPc2	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Alois	Alois	k1gMnSc1	Alois
a	a	k8xC	a
Hana	Hana	k1gFnSc1	Hana
Skoumalovi	Skoumal	k1gMnSc3	Skoumal
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
toto	tento	k3xDgNnSc1	tento
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
a	a	k8xC	a
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Olympia	Olympia	k1gFnSc1	Olympia
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
KMa	KMa	k1gFnSc2	KMa
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
a	a	k8xC	a
Brio	brio	k1gNnSc1	brio
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Stopka	stopka	k1gFnSc1	stopka
&	&	k?	&
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Tafel	Tafel	k1gMnSc1	Tafel
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
moře	moře	k1gNnSc2	moře
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Dušan	Dušan	k1gMnSc1	Dušan
Zbavitel	zbavitel	k1gMnSc1	zbavitel
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
překladu	překlad	k1gInSc6	překlad
vydalo	vydat	k5eAaPmAgNnS	vydat
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
ještě	ještě	k9	ještě
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
jen	jen	k9	jen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
,	,	kIx,	,
Klamné	klamný	k2eAgNnSc1d1	klamné
svítání	svítání	k1gNnSc1	svítání
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Stanislava	Stanislava	k1gFnSc1	Stanislava
Pošustová	Pošustová	k1gFnSc1	Pošustová
<g/>
,	,	kIx,	,
Marina	Marina	k1gFnSc1	Marina
Castielliová	Castielliový	k2eAgFnSc1d1	Castielliový
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Chalupský	chalupský	k1gMnSc1	chalupský
<g/>
,	,	kIx,	,
Kim	Kim	k1gMnSc1	Kim
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
Moserová	Moserová	k1gFnSc1	Moserová
<g/>
,	,	kIx,	,
Tucet	tucet	k1gInSc1	tucet
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
Pavla	Pavel	k1gMnSc2	Pavel
Moudrá	moudrý	k2eAgNnPc1d1	moudré
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
Gaudore	Gaudor	k1gInSc5	Gaudor
<g/>
,	,	kIx,	,
Knihy	kniha	k1gFnPc4	kniha
džunglí	džungle	k1gFnPc2	džungle
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Martin	Martin	k1gMnSc1	Martin
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Vybrané	vybraný	k2eAgFnPc1d1	vybraná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
zednářské	zednářský	k2eAgFnSc2d1	zednářská
lóže	lóže	k1gFnSc2	lóže
<g/>
,	,	kIx,	,
Machart	Machart	k1gInSc1	Machart
<g/>
,	,	kIx,	,
Beroun	Beroun	k1gInSc1	Beroun
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
překlad	překlad	k1gInSc1	překlad
Alexandra	Alexandr	k1gMnSc2	Alexandr
Fleischra	Fleischra	k1gFnSc1	Fleischra
redigoval	redigovat	k5eAaImAgMnS	redigovat
<g/>
,	,	kIx,	,
jazykově	jazykově	k6eAd1	jazykově
a	a	k8xC	a
stylisticky	stylisticky	k6eAd1	stylisticky
upravil	upravit	k5eAaPmAgMnS	upravit
Jan	Jan	k1gMnSc1	Jan
Velíšek	Velíšek	k1gMnSc1	Velíšek
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
<g/>
,	,	kIx,	,
Petrkov	Petrkov	k1gInSc1	Petrkov
<g/>
,	,	kIx,	,
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Hron	Hron	k1gMnSc1	Hron
<g/>
.	.	kIx.	.
</s>
