<s>
Chinin	chinin	k1gInSc1	chinin
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
něm.	něm.	k?	něm.
Chinin	chinin	k1gInSc4	chinin
a	a	k8xC	a
ital	ital	k1gInSc4	ital
<g/>
.	.	kIx.	.
china	china	k1gFnSc1	china
<g/>
,	,	kIx,	,
chinina	chinin	k2eAgFnSc1d1	chinin
ze	z	k7c2	z
špan.	špan.	k?	špan.
quino	quien	k2eAgNnSc1d1	quien
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
kečuánštiny	kečuánština	k1gFnSc2	kečuánština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
alkaloid	alkaloid	k1gInSc1	alkaloid
využívaný	využívaný	k2eAgInSc1d1	využívaný
jako	jako	k8xS	jako
základní	základní	k2eAgNnSc1d1	základní
antimalarikum	antimalarikum	k1gNnSc1	antimalarikum
(	(	kIx(	(
<g/>
lék	lék	k1gInSc1	lék
proti	proti	k7c3	proti
malárii	malárie	k1gFnSc3	malárie
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
základních	základní	k2eAgNnPc2d1	základní
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
<s>
Chinin	chinin	k1gInSc1	chinin
je	být	k5eAaImIp3nS	být
methoxyderivát	methoxyderivát	k5eAaPmF	methoxyderivát
cinchoninu	cinchonina	k1gFnSc4	cinchonina
<g/>
.	.	kIx.	.
</s>
<s>
Chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
chininu	chinin	k1gInSc2	chinin
je	být	k5eAaImIp3nS	být
C	C	kA	C
<g/>
20	[number]	k4	20
<g/>
H	H	kA	H
<g/>
24	[number]	k4	24
<g/>
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
alkaloid	alkaloid	k1gInSc4	alkaloid
<g/>
.	.	kIx.	.
</s>
<s>
Vzorec	vzorec	k1gInSc1	vzorec
byl	být	k5eAaImAgInS	být
sestaven	sestavit	k5eAaPmNgInS	sestavit
Augustem	August	k1gMnSc7	August
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Hofmanem	Hofman	k1gMnSc7	Hofman
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Chinin	chinin	k1gInSc1	chinin
má	mít	k5eAaImIp3nS	mít
hořkou	hořký	k2eAgFnSc4d1	hořká
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
dávkách	dávka	k1gFnPc6	dávka
povzbuzuje	povzbuzovat	k5eAaImIp3nS	povzbuzovat
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
ultrafialovém	ultrafialový	k2eAgNnSc6d1	ultrafialové
světle	světlo	k1gNnSc6	světlo
fluoreskuje	fluoreskovat	k5eAaImIp3nS	fluoreskovat
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
alkaloid	alkaloid	k1gInSc4	alkaloid
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
chinin	chinin	k1gInSc1	chinin
zásadité	zásaditý	k2eAgFnSc2d1	zásaditá
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
slabé	slabý	k2eAgNnSc1d1	slabé
analgetikum	analgetikum	k1gNnSc1	analgetikum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tlumí	tlumit	k5eAaImIp3nS	tlumit
centrum	centrum	k1gNnSc1	centrum
bolesti	bolest	k1gFnSc2	bolest
v	v	k7c6	v
centrálním	centrální	k2eAgNnSc6d1	centrální
nervstvu	nervstvo	k1gNnSc6	nervstvo
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
jako	jako	k9	jako
mírné	mírný	k2eAgNnSc1d1	mírné
antipyretikum	antipyretikum	k1gNnSc1	antipyretikum
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tlumí	tlumit	k5eAaImIp3nS	tlumit
udržování	udržování	k1gNnSc4	udržování
tělesné	tělesný	k2eAgFnSc2d1	tělesná
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
látkovou	látkový	k2eAgFnSc4d1	látková
přeměnu	přeměna	k1gFnSc4	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Snižuje	snižovat	k5eAaImIp3nS	snižovat
tak	tak	k9	tak
horečku	horečka	k1gFnSc4	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
srdeční	srdeční	k2eAgFnSc4d1	srdeční
akci	akce	k1gFnSc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Snižuje	snižovat	k5eAaImIp3nS	snižovat
tak	tak	k9	tak
tlak	tlak	k1gInSc1	tlak
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
příznivé	příznivý	k2eAgInPc4d1	příznivý
účinky	účinek	k1gInPc4	účinek
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
lupus	lupus	k1gInSc1	lupus
erythematodes	erythematodesa	k1gFnPc2	erythematodesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
humánní	humánní	k2eAgFnSc6d1	humánní
medicíně	medicína	k1gFnSc6	medicína
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
jako	jako	k9	jako
antimalarikum	antimalarikum	k1gNnSc1	antimalarikum
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
lék	lék	k1gInSc1	lék
proti	proti	k7c3	proti
malárii	malárie	k1gFnSc3	malárie
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
parazity	parazit	k1gMnPc4	parazit
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepůsobí	působit	k5eNaImIp3nS	působit
na	na	k7c4	na
parazity	parazit	k1gMnPc4	parazit
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vznikají	vznikat	k5eAaImIp3nP	vznikat
kmeny	kmen	k1gInPc1	kmen
malárie	malárie	k1gFnSc2	malárie
odolné	odolný	k2eAgFnSc2d1	odolná
proti	proti	k7c3	proti
chininu	chinin	k1gInSc3	chinin
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
chinin	chinin	k1gInSc1	chinin
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
relativně	relativně	k6eAd1	relativně
účinný	účinný	k2eAgInSc1d1	účinný
a	a	k8xC	a
využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
jako	jako	k9	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
lék	lék	k1gInSc1	lék
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vážných	vážný	k2eAgInPc2d1	vážný
případů	případ	k1gInPc2	případ
malárie	malárie	k1gFnSc2	malárie
<g/>
.	.	kIx.	.
</s>
<s>
Chinin	chinin	k1gInSc1	chinin
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
významným	významný	k2eAgInSc7d1	významný
lékem	lék	k1gInSc7	lék
proti	proti	k7c3	proti
malárii	malárie	k1gFnSc3	malárie
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
z	z	k7c2	z
části	část	k1gFnSc2	část
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
syntetickými	syntetický	k2eAgInPc7d1	syntetický
léky	lék	k1gInPc7	lék
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
chinin	chinin	k1gInSc1	chinin
podává	podávat	k5eAaImIp3nS	podávat
orálně	orálně	k6eAd1	orálně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vážnějších	vážní	k2eAgInPc6d2	vážnější
případech	případ	k1gInPc6	případ
intravenózně	intravenózně	k6eAd1	intravenózně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgInS	připravovat
lék	lék	k1gInSc1	lék
proti	proti	k7c3	proti
malárii	malárie	k1gFnSc3	malárie
rozdrcením	rozdrcení	k1gNnSc7	rozdrcení
kůry	kůra	k1gFnSc2	kůra
chinovníku	chinovník	k1gInSc2	chinovník
a	a	k8xC	a
rozpuštěním	rozpuštění	k1gNnSc7	rozpuštění
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
prášku	prášek	k1gInSc2	prášek
ve	v	k7c6	v
víně	víno	k1gNnSc6	víno
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
etanolu	etanol	k1gInSc6	etanol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
při	při	k7c6	při
předávkování	předávkování	k1gNnSc6	předávkování
jsou	být	k5eAaImIp3nP	být
nevolnost	nevolnost	k1gFnSc1	nevolnost
<g/>
,	,	kIx,	,
hučení	hučení	k1gNnSc1	hučení
v	v	k7c6	v
uších	ucho	k1gNnPc6	ucho
a	a	k8xC	a
poruchy	porucha	k1gFnPc1	porucha
vidění	vidění	k1gNnSc2	vidění
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
křeče	křeč	k1gFnPc4	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
8-10	[number]	k4	8-10
gramů	gram	k1gInPc2	gram
chininu	chinin	k1gInSc2	chinin
<g/>
.	.	kIx.	.
</s>
<s>
Chinin	chinin	k1gInSc1	chinin
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
různých	různý	k2eAgInPc2d1	různý
likérů	likér	k1gInPc2	likér
<g/>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
také	také	k6eAd1	také
součástí	součást	k1gFnSc7	součást
toniků	tonik	k1gInPc2	tonik
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
výskytu	výskyt	k1gInSc2	výskyt
chininu	chinin	k1gInSc2	chinin
v	v	k7c6	v
tonicích	tonik	k1gInPc6	tonik
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
27,7	[number]	k4	27,7
do	do	k7c2	do
79,7	[number]	k4	79,7
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
chinin	chinin	k1gInSc1	chinin
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
různých	různý	k2eAgInPc2d1	různý
výrobků	výrobek	k1gInPc2	výrobek
<g/>
:	:	kIx,	:
vlasové	vlasový	k2eAgInPc4d1	vlasový
a	a	k8xC	a
opalovací	opalovací	k2eAgInPc4d1	opalovací
oleje	olej	k1gInPc4	olej
<g/>
,	,	kIx,	,
šampóny	šampóny	k?	šampóny
<g/>
,	,	kIx,	,
insekticidy	insekticid	k1gInPc1	insekticid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
gumárenském	gumárenský	k2eAgInSc6d1	gumárenský
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
vulkanizační	vulkanizační	k2eAgInSc4d1	vulkanizační
prostředek	prostředek	k1gInSc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Chinin	chinin	k1gInSc1	chinin
byl	být	k5eAaImAgInS	být
nalezen	naleznout	k5eAaPmNgInS	naleznout
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
chinovníku	chinovník	k1gInSc2	chinovník
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
ho	on	k3xPp3gNnSc4	on
izoloval	izolovat	k5eAaBmAgMnS	izolovat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
chemik	chemik	k1gMnSc1	chemik
Pierre-Joseph	Pierre-Joseph	k1gMnSc1	Pierre-Joseph
Peletiere	Peletier	k1gInSc5	Peletier
<g/>
.	.	kIx.	.
</s>
<s>
Získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
z	z	k7c2	z
druhů	druh	k1gInPc2	druh
chinovník	chinovník	k1gInSc4	chinovník
lékařský	lékařský	k2eAgInSc1d1	lékařský
a	a	k8xC	a
chinovník	chinovník	k1gInSc1	chinovník
pýřitý	pýřitý	k2eAgInSc1d1	pýřitý
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
kůra	kůra	k1gFnSc1	kůra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
procent	procent	k1gInSc4	procent
chininu	chinin	k1gInSc2	chinin
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
chininu	chinin	k1gInSc2	chinin
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
stromů	strom	k1gInPc2	strom
rodu	rod	k1gInSc2	rod
Remijia	Remijius	k1gMnSc2	Remijius
je	být	k5eAaImIp3nS	být
0,5	[number]	k4	0,5
-	-	kIx~	-
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
levnější	levný	k2eAgFnSc1d2	levnější
než	než	k8xS	než
kůra	kůra	k1gFnSc1	kůra
chinovníku	chinovník	k1gInSc2	chinovník
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
výraznou	výrazný	k2eAgFnSc4d1	výrazná
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
toniků	tonik	k1gInPc2	tonik
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
také	také	k9	také
ve	v	k7c6	v
vrbové	vrbový	k2eAgFnSc6d1	vrbová
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Odvar	odvar	k1gInSc1	odvar
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
malárie	malárie	k1gFnSc2	malárie
již	již	k6eAd1	již
Hippokrates	Hippokrates	k1gMnSc1	Hippokrates
<g/>
.	.	kIx.	.
</s>
<s>
Hahnemanova	Hahnemanův	k2eAgFnSc1d1	Hahnemanův
alergie	alergie	k1gFnSc1	alergie
Cinchonin	Cinchonin	k1gInSc4	Cinchonin
Chinidin	Chinidin	k1gInSc1	Chinidin
je	být	k5eAaImIp3nS	být
antiarytmikum	antiarytmikum	k1gNnSc4	antiarytmikum
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
srdeční	srdeční	k2eAgFnSc3d1	srdeční
arytmii	arytmie	k1gFnSc3	arytmie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
antimalarické	antimalarický	k2eAgInPc4d1	antimalarický
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chinin	chinin	k1gInSc1	chinin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
