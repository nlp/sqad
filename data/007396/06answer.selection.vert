<s>
Chinin	chinin	k1gInSc1	chinin
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
něm.	něm.	k?	něm.
Chinin	chinin	k1gInSc4	chinin
a	a	k8xC	a
ital	ital	k1gInSc4	ital
<g/>
.	.	kIx.	.
china	china	k1gFnSc1	china
<g/>
,	,	kIx,	,
chinina	chinin	k2eAgFnSc1d1	chinin
ze	z	k7c2	z
špan.	špan.	k?	špan.
quino	quien	k2eAgNnSc1d1	quien
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
kečuánštiny	kečuánština	k1gFnSc2	kečuánština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
alkaloid	alkaloid	k1gInSc1	alkaloid
využívaný	využívaný	k2eAgInSc1d1	využívaný
jako	jako	k8xC	jako
základní	základní	k2eAgNnSc1d1	základní
antimalarikum	antimalarikum	k1gNnSc1	antimalarikum
(	(	kIx(	(
<g/>
lék	lék	k1gInSc1	lék
proti	proti	k7c3	proti
malárii	malárie	k1gFnSc3	malárie
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
základních	základní	k2eAgNnPc2d1	základní
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
.	.	kIx.	.
</s>
