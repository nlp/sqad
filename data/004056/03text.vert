<s>
Banán	banán	k1gInSc4	banán
je	být	k5eAaImIp3nS	být
protáhlé	protáhlý	k2eAgNnSc1d1	protáhlé
ovoce	ovoce	k1gNnSc1	ovoce
(	(	kIx(	(
<g/>
nesladké	sladký	k2eNgInPc1d1	nesladký
druhy	druh	k1gInPc1	druh
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
obsahem	obsah	k1gInSc7	obsah
škrobu	škrob	k1gInSc2	škrob
(	(	kIx(	(
<g/>
plantain	plantain	k1gInSc1	plantain
<g/>
)	)	kIx)	)
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
)	)	kIx)	)
a	a	k8xC	a
plod	plod	k1gInSc4	plod
banánovníku	banánovník	k1gInSc2	banánovník
(	(	kIx(	(
<g/>
epigeická	epigeický	k2eAgFnSc1d1	epigeický
bobule	bobule	k1gFnSc1	bobule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velice	velice	k6eAd1	velice
žádanou	žádaný	k2eAgFnSc4d1	žádaná
komoditu	komodita	k1gFnSc4	komodita
produkovanou	produkovaný	k2eAgFnSc4d1	produkovaná
zemědělci	zemědělec	k1gMnPc7	zemědělec
tropických	tropický	k2eAgFnPc2d1	tropická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
složku	složka	k1gFnSc4	složka
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Banány	banán	k1gInPc1	banán
mívají	mívat	k5eAaImIp3nP	mívat
obvykle	obvykle	k6eAd1	obvykle
hmotnost	hmotnost	k1gFnSc4	hmotnost
mezi	mezi	k7c7	mezi
125	[number]	k4	125
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
g	g	kA	g
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
významně	významně	k6eAd1	významně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
kultivaru	kultivar	k1gInSc6	kultivar
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
váhy	váha	k1gFnSc2	váha
asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
představuje	představovat	k5eAaImIp3nS	představovat
jedlou	jedlý	k2eAgFnSc4d1	jedlá
část	část	k1gFnSc4	část
a	a	k8xC	a
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
20	[number]	k4	20
%	%	kIx~	%
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
kožovitou	kožovitý	k2eAgFnSc4d1	kožovitá
slupku	slupka	k1gFnSc4	slupka
<g/>
.	.	kIx.	.
</s>
<s>
Banán	banán	k1gInSc1	banán
se	s	k7c7	s
(	(	kIx(	(
<g/>
po	po	k7c6	po
kukuřici	kukuřice	k1gFnSc6	kukuřice
a	a	k8xC	a
rýži	rýže	k1gFnSc6	rýže
<g/>
)	)	kIx)	)
stal	stát	k5eAaPmAgMnS	stát
třetí	třetí	k4xOgFnSc7	třetí
plodinou	plodina	k1gFnSc7	plodina
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
celý	celý	k2eAgInSc1d1	celý
její	její	k3xOp3gInSc1	její
genom	genom	k1gInSc1	genom
<g/>
.	.	kIx.	.
</s>
<s>
Vůně	vůně	k1gFnSc1	vůně
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
a	a	k8xC	a
konzistence	konzistence	k1gFnSc1	konzistence
plodů	plod	k1gInPc2	plod
je	být	k5eAaImIp3nS	být
významně	významně	k6eAd1	významně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
teplotou	teplota	k1gFnSc7	teplota
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
šednou	šednout	k5eAaImIp3nP	šednout
a	a	k8xC	a
kazí	kazit	k5eAaImIp3nP	kazit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Banány	banán	k1gInPc1	banán
dodávané	dodávaný	k2eAgInPc1d1	dodávaný
na	na	k7c4	na
trh	trh	k1gInSc4	trh
mírného	mírný	k2eAgNnSc2d1	mírné
pásma	pásmo	k1gNnSc2	pásmo
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k8xC	i
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
sklízené	sklízený	k2eAgFnPc1d1	sklízená
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
během	během	k7c2	během
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
teploty	teplota	k1gFnSc2	teplota
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
životnost	životnost	k1gFnSc1	životnost
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
negativně	negativně	k6eAd1	negativně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
jejich	jejich	k3xOp3gFnSc4	jejich
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
při	při	k7c6	při
dopravě	doprava	k1gFnSc6	doprava
nikdy	nikdy	k6eAd1	nikdy
nebývají	bývat	k5eNaImIp3nP	bývat
chlazeny	chladit	k5eAaImNgFnP	chladit
pod	pod	k7c4	pod
13,5	[number]	k4	13,5
°	°	k?	°
<g/>
C.	C.	kA	C.
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
ukládat	ukládat	k5eAaImF	ukládat
nakoupené	nakoupený	k2eAgInPc4d1	nakoupený
banány	banán	k1gInPc4	banán
do	do	k7c2	do
lednice	lednice	k1gFnSc2	lednice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
kvalita	kvalita	k1gFnSc1	kvalita
tím	ten	k3xDgNnSc7	ten
značně	značně	k6eAd1	značně
utrpí	utrpět	k5eAaPmIp3nP	utrpět
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
nechávají	nechávat	k5eAaImIp3nP	nechávat
banány	banán	k1gInPc1	banán
dozrávat	dozrávat	k5eAaImF	dozrávat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
tlakových	tlakový	k2eAgFnPc6d1	tlaková
komorách	komora	k1gFnPc6	komora
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
celé	celý	k2eAgInPc1d1	celý
nerozbalené	rozbalený	k2eNgInPc1d1	nerozbalený
kartony	karton	k1gInPc1	karton
s	s	k7c7	s
banánovými	banánový	k2eAgInPc7d1	banánový
plody	plod	k1gInPc7	plod
umístí	umístit	k5eAaPmIp3nS	umístit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tlakové	tlakový	k2eAgFnSc6d1	tlaková
komoře	komora	k1gFnSc6	komora
dojde	dojít	k5eAaPmIp3nS	dojít
"	"	kIx"	"
<g/>
k	k	k7c3	k
omačkání	omačkání	k1gNnSc3	omačkání
plodů	plod	k1gInPc2	plod
<g/>
"	"	kIx"	"
plynem	plyn	k1gInSc7	plyn
(	(	kIx(	(
<g/>
etylen	etylen	k1gInSc1	etylen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
nastartuje	nastartovat	k5eAaPmIp3nS	nastartovat
zrychlený	zrychlený	k2eAgInSc4d1	zrychlený
proces	proces	k1gInSc4	proces
zrání	zrání	k1gNnSc2	zrání
<g/>
.	.	kIx.	.
</s>
<s>
Zralost	zralost	k1gFnSc1	zralost
banánů	banán	k1gInPc2	banán
(	(	kIx(	(
<g/>
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
skladech	sklad	k1gInPc6	sklad
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
speciální	speciální	k2eAgFnSc7d1	speciální
stupnicí	stupnice	k1gFnSc7	stupnice
dle	dle	k7c2	dle
podílu	podíl	k1gInSc2	podíl
zahnědlých	zahnědlý	k2eAgFnPc2d1	zahnědlá
skvrn	skvrna	k1gFnPc2	skvrna
a	a	k8xC	a
ploch	plocha	k1gFnPc2	plocha
na	na	k7c6	na
slupce	slupka	k1gFnSc6	slupka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupujícím	postupující	k2eAgNnSc7d1	postupující
zráním	zrání	k1gNnSc7	zrání
se	se	k3xPyFc4	se
banán	banán	k1gInSc1	banán
také	také	k9	také
zakulacuje	zakulacovat	k5eAaImIp3nS	zakulacovat
<g/>
.	.	kIx.	.
</s>
<s>
Banány	banán	k1gInPc1	banán
rostou	růst	k5eAaImIp3nP	růst
ve	v	k7c6	v
visících	visící	k2eAgInPc6d1	visící
trsech	trs	k1gInPc6	trs
<g/>
,	,	kIx,	,
od	od	k7c2	od
několika	několik	k4yIc2	několik
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
kusů	kus	k1gInPc2	kus
ve	v	k7c6	v
vrstvě	vrstva	k1gFnSc6	vrstva
(	(	kIx(	(
<g/>
zvané	zvaný	k2eAgFnPc1d1	zvaná
též	též	k9	též
ruka	ruka	k1gFnSc1	ruka
<g/>
)	)	kIx)	)
v	v	k7c6	v
mnohovrstevném	mnohovrstevný	k2eAgInSc6d1	mnohovrstevný
svazku	svazek	k1gInSc6	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Celek	celek	k1gInSc1	celek
visících	visící	k2eAgInPc2d1	visící
trsů	trs	k1gInPc2	trs
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kmen	kmen	k1gInSc1	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
celosvětově	celosvětově	k6eAd1	celosvětově
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
12	[number]	k4	12
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
banánů	banán	k1gInPc2	banán
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
,	,	kIx,	,
Kostarika	Kostarika	k1gFnSc1	Kostarika
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
a	a	k8xC	a
Filipíny	Filipíny	k1gFnPc1	Filipíny
exportovaly	exportovat	k5eAaBmAgFnP	exportovat
každý	každý	k3xTgMnSc1	každý
přes	přes	k7c4	přes
1	[number]	k4	1
milión	milión	k4xCgInSc4	milión
tun	tuna	k1gFnPc2	tuna
banánů	banán	k1gInPc2	banán
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejhojněji	hojně	k6eAd3	hojně
pěstovaný	pěstovaný	k2eAgInSc4d1	pěstovaný
kultivar	kultivar	k1gInSc4	kultivar
banánu	banán	k1gInSc2	banán
patří	patřit	k5eAaImIp3nS	patřit
triploidní	triploidní	k2eAgFnSc1d1	triploidní
odrůda	odrůda	k1gFnSc1	odrůda
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
zkřížením	zkřížení	k1gNnSc7	zkřížení
druhů	druh	k1gInPc2	druh
Musa	Musa	k1gFnSc1	Musa
acuminata	acuminata	k1gFnSc1	acuminata
a	a	k8xC	a
Musa	Musa	k1gFnSc1	Musa
balbisiana	balbisiana	k1gFnSc1	balbisiana
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
semen	semeno	k1gNnPc2	semeno
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
konzumaci	konzumace	k1gFnSc3	konzumace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
pouze	pouze	k6eAd1	pouze
vegetativně	vegetativně	k6eAd1	vegetativně
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yQnSc2	což
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
náchylný	náchylný	k2eAgMnSc1d1	náchylný
k	k	k7c3	k
nemocem	nemoc	k1gFnPc3	nemoc
a	a	k8xC	a
různým	různý	k2eAgFnPc3d1	různá
plísním	plíseň	k1gFnPc3	plíseň
<g/>
.	.	kIx.	.
</s>
<s>
Banán	banán	k1gInSc1	banán
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
rostlina	rostlina	k1gFnSc1	rostlina
banánovník	banánovník	k1gInSc1	banánovník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
domestikován	domestikovat	k5eAaBmNgInS	domestikovat
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
rostou	růst	k5eAaImIp3nP	růst
botanické	botanický	k2eAgInPc1d1	botanický
druhy	druh	k1gInPc1	druh
banánovníku	banánovník	k1gInSc2	banánovník
<g/>
)	)	kIx)	)
a	a	k8xC	a
podle	podle	k7c2	podle
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
to	ten	k3xDgNnSc1	ten
mohlo	moct	k5eAaImAgNnS	moct
nejspíše	nejspíše	k9	nejspíše
být	být	k5eAaImF	být
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Papuy-Nové	Papuy-Nové	k2eAgFnSc2d1	Papuy-Nové
Guineje	Guinea	k1gFnSc2	Guinea
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
možná	možná	k9	možná
již	již	k9	již
8000	[number]	k4	8000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
banánech	banán	k1gInPc6	banán
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
buddhistických	buddhistický	k2eAgInPc6d1	buddhistický
textech	text	k1gInPc6	text
z	z	k7c2	z
let	léto	k1gNnPc2	léto
okolo	okolo	k7c2	okolo
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
objevil	objevit	k5eAaPmAgInS	objevit
chuť	chuť	k1gFnSc4	chuť
banánu	banán	k1gInSc2	banán
v	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
Indie	Indie	k1gFnSc2	Indie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
327	[number]	k4	327
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
První	první	k4xOgMnSc1	první
známý	známý	k2eAgInSc1d1	známý
organizovaný	organizovaný	k2eAgInSc1d1	organizovaný
banánový	banánový	k2eAgInSc1d1	banánový
sad	sad	k1gInSc1	sad
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
v	v	k7c6	v
roce	rok	k1gInSc6	rok
200	[number]	k4	200
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
650	[number]	k4	650
islámští	islámský	k2eAgMnPc1d1	islámský
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
přinesli	přinést	k5eAaPmAgMnP	přinést
banán	banán	k1gInSc4	banán
do	do	k7c2	do
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Arabští	arabský	k2eAgMnPc1d1	arabský
kupci	kupec	k1gMnPc1	kupec
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
banány	banán	k1gInPc4	banán
po	po	k7c6	po
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1502	[number]	k4	1502
portugalští	portugalský	k2eAgMnPc1d1	portugalský
kolonisté	kolonista	k1gMnPc1	kolonista
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
první	první	k4xOgFnPc4	první
banánové	banánový	k2eAgFnPc4d1	banánová
sady	sada	k1gFnPc4	sada
v	v	k7c6	v
Karibiku	Karibikum	k1gNnSc6	Karibikum
a	a	k8xC	a
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
syrových	syrový	k2eAgInPc6d1	syrový
banánech	banán	k1gInPc6	banán
<g/>
.	.	kIx.	.
</s>
<s>
Banánovník	banánovník	k1gInSc1	banánovník
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
napaden	napadnout	k5eAaPmNgInS	napadnout
bakterií	bakterie	k1gFnSc7	bakterie
hnědé	hnědý	k2eAgFnSc2d1	hnědá
hniloby	hniloba	k1gFnSc2	hniloba
brambor	brambora	k1gFnPc2	brambora
-	-	kIx~	-
Ralstonia	Ralstonium	k1gNnSc2	Ralstonium
solanacearum	solanacearum	k1gNnSc4	solanacearum
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yQgFnSc4	který
snad	snad	k9	snad
žije	žít	k5eAaImIp3nS	žít
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
symbióze	symbióza	k1gFnSc6	symbióza
a	a	k8xC	a
kterou	který	k3yIgFnSc4	který
známe	znát	k5eAaImIp1nP	znát
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hnědých	hnědý	k2eAgInPc2d1	hnědý
flíčků	flíček	k1gInPc2	flíček
a	a	k8xC	a
ploch	plocha	k1gFnPc2	plocha
na	na	k7c6	na
banánové	banánový	k2eAgFnSc6d1	banánová
slupce	slupka	k1gFnSc6	slupka
<g/>
.	.	kIx.	.
</s>
