<s>
Banány	banán	k1gInPc1	banán
mívají	mívat	k5eAaImIp3nP	mívat
obvykle	obvykle	k6eAd1	obvykle
hmotnost	hmotnost	k1gFnSc4	hmotnost
mezi	mezi	k7c7	mezi
125	[number]	k4	125
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
g	g	kA	g
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
významně	významně	k6eAd1	významně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
kultivaru	kultivar	k1gInSc6	kultivar
<g/>
.	.	kIx.	.
</s>
