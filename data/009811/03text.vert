<p>
<s>
Bankovka	bankovka	k1gFnSc1	bankovka
neboli	neboli	k8xC	neboli
cenný	cenný	k2eAgInSc1d1	cenný
papír	papír	k1gInSc1	papír
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
peněžní	peněžní	k2eAgFnSc7d1	peněžní
částkou	částka	k1gFnSc7	částka
je	být	k5eAaImIp3nS	být
bezúročný	bezúročný	k2eAgInSc1d1	bezúročný
platební	platební	k2eAgInSc1d1	platební
prostředek	prostředek	k1gInSc1	prostředek
<g/>
,	,	kIx,	,
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
centrální	centrální	k2eAgFnSc7d1	centrální
bankou	banka	k1gFnSc7	banka
<g/>
.	.	kIx.	.
</s>
<s>
Bankovkami	bankovka	k1gFnPc7	bankovka
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
sběratelský	sběratelský	k2eAgInSc1d1	sběratelský
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
notafilie	notafilie	k1gFnSc1	notafilie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Bankovky	bankovka	k1gFnPc1	bankovka
se	se	k3xPyFc4	se
historicky	historicky	k6eAd1	historicky
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
obchodních	obchodní	k2eAgFnPc2d1	obchodní
směnek	směnka	k1gFnPc2	směnka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
předložení	předložení	k1gNnSc2	předložení
směnky	směnka	k1gFnSc2	směnka
byla	být	k5eAaImAgFnS	být
původnímu	původní	k2eAgNnSc3d1	původní
majiteli	majitel	k1gMnPc7	majitel
vydána	vydán	k2eAgFnSc1d1	vydána
bankovka	bankovka	k1gFnSc1	bankovka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
banka	banka	k1gFnSc1	banka
zavazovala	zavazovat	k5eAaImAgFnS	zavazovat
vyplatit	vyplatit	k5eAaPmF	vyplatit
při	při	k7c6	při
předložení	předložení	k1gNnSc6	předložení
určité	určitý	k2eAgNnSc4d1	určité
množství	množství	k1gNnSc4	množství
zlata	zlato	k1gNnSc2	zlato
popřípadě	popřípadě	k6eAd1	popřípadě
určitou	určitý	k2eAgFnSc4d1	určitá
sumu	suma	k1gFnSc4	suma
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
bankovkami	bankovka	k1gFnPc7	bankovka
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
pouze	pouze	k6eAd1	pouze
peníze	peníz	k1gInPc1	peníz
z	z	k7c2	z
drahých	drahý	k2eAgInPc2d1	drahý
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
samy	sám	k3xTgInPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
byly	být	k5eAaImAgFnP	být
nositeli	nositel	k1gMnSc3	nositel
hodnoty	hodnota	k1gFnSc2	hodnota
–	–	k?	–
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
bankovek	bankovka	k1gFnPc2	bankovka
–	–	k?	–
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
bance	banka	k1gFnSc6	banka
<g/>
,	,	kIx,	,
státu	stát	k1gInSc3	stát
či	či	k8xC	či
úřední	úřední	k2eAgFnSc3d1	úřední
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInPc1d3	nejčastější
byly	být	k5eAaImAgInP	být
zlaté	zlatý	k1gInPc1	zlatý
mince	mince	k1gFnSc2	mince
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc7	jejich
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
poměrně	poměrně	k6eAd1	poměrně
těžké	těžký	k2eAgFnPc1d1	těžká
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
opotřebovávaly	opotřebovávat	k5eAaImAgInP	opotřebovávat
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
650	[number]	k4	650
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
byly	být	k5eAaImAgInP	být
první	první	k4xOgInPc1	první
papírové	papírový	k2eAgInPc1d1	papírový
peníze	peníz	k1gInPc1	peníz
vydány	vydán	k2eAgInPc1d1	vydán
za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Kao	Kao	k1gMnSc2	Kao
Tsunga	Tsung	k1gMnSc2	Tsung
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
měnu	měna	k1gFnSc4	měna
jsou	být	k5eAaImIp3nP	být
uznány	uznat	k5eAaPmNgFnP	uznat
až	až	k9	až
v	v	k7c4	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
roku	rok	k1gInSc2	rok
1298	[number]	k4	1298
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
bankovky	bankovka	k1gFnPc1	bankovka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
i	i	k8xC	i
v	v	k7c6	v
Persii	Persie	k1gFnSc6	Persie
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nemusely	muset	k5eNaImAgInP	muset
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
pytlíky	pytlík	k1gInPc1	pytlík
mincí	mince	k1gFnPc2	mince
<g/>
,	,	kIx,	,
zavedli	zavést	k5eAaPmAgMnP	zavést
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
evropští	evropský	k2eAgMnPc1d1	evropský
obchodníci	obchodník	k1gMnPc1	obchodník
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
úvěrové	úvěrový	k2eAgInPc4d1	úvěrový
úpisy	úpis	k1gInPc4	úpis
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vydávaly	vydávat	k5eAaPmAgInP	vydávat
takové	takový	k3xDgInPc1	takový
úpisy	úpis	k1gInPc1	úpis
přímo	přímo	k6eAd1	přímo
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
kovová	kovový	k2eAgNnPc1d1	kovové
platidla	platidlo	k1gNnPc1	platidlo
nestačila	stačit	k5eNaBmAgNnP	stačit
pokrývat	pokrývat	k5eAaImF	pokrývat
prudký	prudký	k2eAgInSc1d1	prudký
růst	růst	k1gInSc1	růst
objemu	objem	k1gInSc2	objem
transakcí	transakce	k1gFnPc2	transakce
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
používat	používat	k5eAaImF	používat
směnky	směnka	k1gFnPc4	směnka
jakožto	jakožto	k8xS	jakožto
cenné	cenný	k2eAgInPc4d1	cenný
papíry	papír	k1gInPc4	papír
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc3	jejich
vydavatel	vydavatel	k1gMnSc1	vydavatel
zavazuje	zavazovat	k5eAaImIp3nS	zavazovat
vyplatit	vyplatit	k5eAaPmF	vyplatit
držiteli	držitel	k1gMnSc3	držitel
určitou	určitý	k2eAgFnSc4d1	určitá
částku	částka	k1gFnSc4	částka
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
směnkami	směnka	k1gFnPc7	směnka
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
začalo	začít	k5eAaPmAgNnS	začít
obchodovat	obchodovat	k5eAaImF	obchodovat
<g/>
.	.	kIx.	.
</s>
<s>
Směnky	směnka	k1gFnPc1	směnka
měly	mít	k5eAaImAgFnP	mít
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
několik	několik	k4yIc4	několik
nevýhod	nevýhoda	k1gFnPc2	nevýhoda
–	–	k?	–
byly	být	k5eAaImAgInP	být
vždy	vždy	k6eAd1	vždy
vypsány	vypsat	k5eAaPmNgInP	vypsat
na	na	k7c4	na
pevnou	pevný	k2eAgFnSc4d1	pevná
částku	částka	k1gFnSc4	částka
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
reálná	reálný	k2eAgFnSc1d1	reálná
hodnota	hodnota	k1gFnSc1	hodnota
závisela	záviset	k5eAaImAgFnS	záviset
na	na	k7c6	na
platební	platební	k2eAgFnSc6d1	platební
morálce	morálka	k1gFnSc6	morálka
konkrétního	konkrétní	k2eAgMnSc2d1	konkrétní
vydavatele	vydavatel	k1gMnSc2	vydavatel
(	(	kIx(	(
<g/>
emitenta	emitent	k1gMnSc2	emitent
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Banky	banka	k1gFnPc1	banka
proto	proto	k8xC	proto
začaly	začít	k5eAaPmAgFnP	začít
směnky	směnka	k1gFnPc1	směnka
vykupovat	vykupovat	k5eAaImF	vykupovat
a	a	k8xC	a
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
je	on	k3xPp3gInPc4	on
za	za	k7c2	za
směnky	směnka	k1gFnSc2	směnka
vlastní	vlastní	k2eAgFnSc2d1	vlastní
–	–	k?	–
bankovky	bankovka	k1gFnSc2	bankovka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
kryty	krýt	k5eAaImNgInP	krýt
vykoupenými	vykoupený	k2eAgFnPc7d1	vykoupená
směnkami	směnka	k1gFnPc7	směnka
nebo	nebo	k8xC	nebo
zlatem	zlato	k1gNnSc7	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
systémem	systém	k1gInSc7	systém
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
monopol	monopol	k1gInSc1	monopol
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
bankovek	bankovka	k1gFnPc2	bankovka
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
svěřen	svěřit	k5eAaPmNgMnS	svěřit
centrální	centrální	k2eAgMnSc1d1	centrální
bance	banka	k1gFnSc3	banka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
tak	tak	k6eAd1	tak
smí	smět	k5eAaImIp3nS	smět
bankovky	bankovka	k1gFnPc4	bankovka
vydávat	vydávat	k5eAaPmF	vydávat
pouze	pouze	k6eAd1	pouze
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
banka	banka	k1gFnSc1	banka
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
dáno	dát	k5eAaPmNgNnS	dát
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
bankovky	bankovka	k1gFnPc4	bankovka
ČNB	ČNB	kA	ČNB
(	(	kIx(	(
<g/>
koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
měny	měna	k1gFnPc4	měna
s	s	k7c7	s
nuceným	nucený	k2eAgInSc7d1	nucený
oběhem	oběh	k1gInSc7	oběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
bankovky	bankovka	k1gFnPc1	bankovka
na	na	k7c6	na
území	území	k1gNnSc6	území
rakouské	rakouský	k2eAgFnSc2d1	rakouská
monarchie	monarchie	k1gFnSc2	monarchie
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
vydávat	vydávat	k5eAaPmF	vydávat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vlády	vláda	k1gFnSc2	vláda
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
.	.	kIx.	.
</s>
<s>
Poukazy	poukaz	k1gInPc4	poukaz
začala	začít	k5eAaPmAgFnS	začít
emitovat	emitovat	k5eAaBmF	emitovat
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
nazývat	nazývat	k5eAaImF	nazývat
"	"	kIx"	"
<g/>
Banco-zettel	Bancoettel	k1gInSc1	Banco-zettel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
bankovní	bankovní	k2eAgInSc1d1	bankovní
lístek	lístek	k1gInSc1	lístek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Počeštěním	počeštění	k1gNnSc7	počeštění
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
bankocetle	bankocetle	k1gFnSc1	bankocetle
<g/>
"	"	kIx"	"
a	a	k8xC	a
pro	pro	k7c4	pro
bankovky	bankovka	k1gFnPc4	bankovka
obecně	obecně	k6eAd1	obecně
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
banknóty	banknóta	k1gFnPc1	banknóta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
oficiální	oficiální	k2eAgFnSc1d1	oficiální
"	"	kIx"	"
<g/>
bankovka	bankovka	k1gFnSc1	bankovka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgNnPc1d1	vydané
na	na	k7c4	na
státní	státní	k2eAgInPc4d1	státní
náklady	náklad	k1gInPc4	náklad
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
v	v	k7c6	v
emisi	emise	k1gFnSc6	emise
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1800	[number]	k4	1800
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Lidé	člověk	k1gMnPc1	člověk
však	však	k9	však
tehdy	tehdy	k6eAd1	tehdy
k	k	k7c3	k
novým	nový	k2eAgInPc3d1	nový
kreditním	kreditní	k2eAgInPc3d1	kreditní
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
nehodnotným	hodnotný	k2eNgInPc3d1	nehodnotný
penězům	peníze	k1gInPc3	peníze
neměli	mít	k5eNaImAgMnP	mít
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
historie	historie	k1gFnSc2	historie
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
používání	používání	k1gNnSc6	používání
bankovek	bankovka	k1gFnPc2	bankovka
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
první	první	k4xOgFnPc1	první
bankovky	bankovka	k1gFnPc1	bankovka
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
jsou	být	k5eAaImIp3nP	být
datovány	datovat	k5eAaImNgInP	datovat
rokem	rok	k1gInSc7	rok
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
u	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
první	první	k4xOgFnPc1	první
bankovky	bankovka	k1gFnPc1	bankovka
používaly	používat	k5eAaImAgFnP	používat
výrazně	výrazně	k6eAd1	výrazně
později	pozdě	k6eAd2	pozdě
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
africké	africký	k2eAgFnPc1d1	africká
Lesotho	Lesotha	k1gFnSc5	Lesotha
mělo	mít	k5eAaImAgNnS	mít
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
bankovky	bankovka	k1gFnPc4	bankovka
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tento	tento	k3xDgInSc1	tento
výrazný	výrazný	k2eAgInSc1d1	výrazný
časový	časový	k2eAgInSc1d1	časový
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Afriky	Afrika	k1gFnSc2	Afrika
na	na	k7c6	na
ukončení	ukončení	k1gNnSc6	ukončení
kolonizace	kolonizace	k1gFnSc2	kolonizace
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1961	[number]	k4	1961
-	-	kIx~	-
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
států	stát	k1gInPc2	stát
i	i	k9	i
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Krytí	krytí	k1gNnSc1	krytí
bankovek	bankovka	k1gFnPc2	bankovka
==	==	k?	==
</s>
</p>
<p>
<s>
Bankovky	bankovka	k1gFnPc1	bankovka
některých	některý	k3yIgFnPc2	některý
měn	měna	k1gFnPc2	měna
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
kryty	kryt	k2eAgInPc1d1	kryt
různými	různý	k2eAgFnPc7d1	různá
komoditami	komodita	k1gFnPc7	komodita
<g/>
;	;	kIx,	;
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
časem	časem	k6eAd1	časem
převládlo	převládnout	k5eAaPmAgNnS	převládnout
zlato	zlato	k1gNnSc1	zlato
a	a	k8xC	a
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
měnový	měnový	k2eAgInSc1d1	měnový
konverzní	konverzní	k2eAgInSc1d1	konverzní
poměr	poměr	k1gInSc1	poměr
se	se	k3xPyFc4	se
odvozoval	odvozovat	k5eAaImAgInS	odvozovat
právě	právě	k6eAd1	právě
od	od	k7c2	od
množství	množství	k1gNnSc2	množství
zlatého	zlatý	k2eAgInSc2d1	zlatý
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
kryté	krytý	k2eAgFnPc1d1	krytá
bankovky	bankovka	k1gFnPc1	bankovka
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
kdykoliv	kdykoliv	k6eAd1	kdykoliv
po	po	k7c4	po
předložení	předložení	k1gNnSc4	předložení
směnit	směnit	k5eAaPmF	směnit
za	za	k7c4	za
komoditu	komodita	k1gFnSc4	komodita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měnu	měna	k1gFnSc4	měna
kryla	krýt	k5eAaImAgFnS	krýt
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
komoditní	komoditní	k2eAgNnSc1d1	komoditní
krytí	krytí	k1gNnSc1	krytí
měny	měna	k1gFnSc2	měna
tvořilo	tvořit	k5eAaImAgNnS	tvořit
rovnovážnou	rovnovážný	k2eAgFnSc4d1	rovnovážná
měnovou	měnový	k2eAgFnSc4d1	měnová
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
ručili	ručit	k5eAaImAgMnP	ručit
emitenti	emitent	k1gMnPc1	emitent
bankovek	bankovka	k1gFnPc2	bankovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnešní	dnešní	k2eAgFnPc1d1	dnešní
bankovky	bankovka	k1gFnPc1	bankovka
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
kryty	kryt	k2eAgInPc4d1	kryt
drahými	drahý	k2eAgInPc7d1	drahý
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mnohdy	mnohdy	k6eAd1	mnohdy
zadlužené	zadlužený	k2eAgInPc4d1	zadlužený
státy	stát	k1gInPc4	stát
by	by	kYmCp3nS	by
toto	tento	k3xDgNnSc1	tento
počínání	počínání	k1gNnSc1	počínání
bylo	být	k5eAaImAgNnS	být
nevýhodné	výhodný	k2eNgNnSc1d1	nevýhodné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
bankovek	bankovka	k1gFnPc2	bankovka
dodnes	dodnes	k6eAd1	dodnes
krytých	krytý	k2eAgFnPc2d1	krytá
stříbrem	stříbro	k1gNnSc7	stříbro
a	a	k8xC	a
zlatem	zlato	k1gNnSc7	zlato
lze	lze	k6eAd1	lze
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
najít	najít	k5eAaPmF	najít
dolary	dolar	k1gInPc4	dolar
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
se	s	k7c7	s
žlutou	žlutý	k2eAgFnSc7d1	žlutá
a	a	k8xC	a
modrou	modrý	k2eAgFnSc7d1	modrá
pečetí	pečeť	k1gFnSc7	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
vydávány	vydávat	k5eAaImNgFnP	vydávat
hlavně	hlavně	k9	hlavně
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
emisích	emise	k1gFnPc6	emise
Silver	Silvra	k1gFnPc2	Silvra
Certificate	Certificat	k1gInSc5	Certificat
(	(	kIx(	(
<g/>
s	s	k7c7	s
modrou	modrý	k2eAgFnSc7d1	modrá
pečetí	pečeť	k1gFnSc7	pečeť
pro	pro	k7c4	pro
bankovky	bankovka	k1gFnPc4	bankovka
kryté	krytý	k2eAgFnPc4d1	krytá
stříbrem	stříbro	k1gNnSc7	stříbro
)	)	kIx)	)
a	a	k8xC	a
Yellow	Yellow	k1gMnSc1	Yellow
Seal	Seal	k1gMnSc1	Seal
(	(	kIx(	(
<g/>
se	s	k7c7	s
žlutou	žlutý	k2eAgFnSc7d1	žlutá
pečetí	pečeť	k1gFnSc7	pečeť
pro	pro	k7c4	pro
bankovky	bankovka	k1gFnPc4	bankovka
kryté	krytý	k2eAgFnPc4d1	krytá
zlatem	zlato	k1gNnSc7	zlato
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInPc1d1	dnešní
dolary	dolar	k1gInPc1	dolar
mají	mít	k5eAaImIp3nP	mít
pečeť	pečeť	k1gFnSc4	pečeť
zelenou	zelená	k1gFnSc4	zelená
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejsou	být	k5eNaImIp3nP	být
kryty	kryt	k1gInPc4	kryt
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
jsou	být	k5eAaImIp3nP	být
kreditními	kreditní	k2eAgFnPc7d1	kreditní
bankovkami	bankovka	k1gFnPc7	bankovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochranné	ochranný	k2eAgInPc1d1	ochranný
prvky	prvek	k1gInPc1	prvek
bankovek	bankovka	k1gFnPc2	bankovka
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
bankovkách	bankovka	k1gFnPc6	bankovka
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
řada	řada	k1gFnSc1	řada
technických	technický	k2eAgInPc2d1	technický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
zabránit	zabránit	k5eAaPmF	zabránit
jejich	jejich	k3xOp3gNnSc4	jejich
padělání	padělání	k1gNnSc4	padělání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvky	prvek	k1gInPc1	prvek
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
materiálu	materiál	k1gInSc2	materiál
platidla	platidlo	k1gNnSc2	platidlo
specifického	specifický	k2eAgNnSc2d1	specifické
složení	složení	k1gNnSc2	složení
<g/>
:	:	kIx,	:
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
plast	plast	k1gInSc1	plast
<g/>
,	,	kIx,	,
konfety	konfet	k1gInPc1	konfet
<g/>
,	,	kIx,	,
barevná	barevný	k2eAgNnPc1d1	barevné
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
fluorescentní	fluorescentní	k2eAgNnPc1d1	fluorescentní
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
zapracovaný	zapracovaný	k2eAgInSc1d1	zapracovaný
ochranný	ochranný	k2eAgInSc1d1	ochranný
proužek	proužek	k1gInSc1	proužek
a	a	k8xC	a
vodotisk	vodotisk	k1gInSc1	vodotisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvky	prvek	k1gInPc1	prvek
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
specifických	specifický	k2eAgFnPc2d1	specifická
vlastností	vlastnost	k1gFnPc2	vlastnost
tisku	tisk	k1gInSc2	tisk
<g/>
:	:	kIx,	:
soutisk	soutisk	k1gInSc1	soutisk
<g/>
,	,	kIx,	,
fluorescentní	fluorescentní	k2eAgInSc1d1	fluorescentní
tisk	tisk	k1gInSc1	tisk
<g/>
,	,	kIx,	,
tisk	tisk	k1gInSc1	tisk
metalickými	metalický	k2eAgFnPc7d1	metalická
barvami	barva	k1gFnPc7	barva
<g/>
,	,	kIx,	,
hlubotiskový	hlubotiskový	k2eAgInSc1d1	hlubotiskový
sklopný	sklopný	k2eAgInSc1d1	sklopný
efekt-	efekt-	k?	efekt-
<g/>
3	[number]	k4	3
<g/>
D	D	kA	D
tisk	tisk	k1gInSc1	tisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
prvky	prvek	k1gInPc1	prvek
<g/>
:	:	kIx,	:
hologram	hologram	k1gInSc1	hologram
<g/>
,	,	kIx,	,
kinegram	kinegram	k1gInSc1	kinegram
<g/>
,	,	kIx,	,
číslování	číslování	k1gNnSc1	číslování
bankovek	bankovka	k1gFnPc2	bankovka
<g/>
,	,	kIx,	,
EURion	EURion	k1gInSc1	EURion
konstelace	konstelace	k1gFnSc2	konstelace
<g/>
.	.	kIx.	.
<g/>
Anatomie	anatomie	k1gFnSc2	anatomie
a	a	k8xC	a
identit	identita	k1gFnPc2	identita
bankovek	bankovka	k1gFnPc2	bankovka
<g/>
:	:	kIx,	:
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velice	velice	k6eAd1	velice
podrobný	podrobný	k2eAgInSc4d1	podrobný
popis	popis	k1gInSc4	popis
bankovek	bankovka	k1gFnPc2	bankovka
<g/>
;	;	kIx,	;
anatomie	anatomie	k1gFnSc1	anatomie
bankovek	bankovka	k1gFnPc2	bankovka
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
základní	základní	k2eAgFnPc4d1	základní
součásti	součást	k1gFnPc4	součást
bankovek	bankovka	k1gFnPc2	bankovka
<g/>
,	,	kIx,	,
identit	identita	k1gFnPc2	identita
pak	pak	k6eAd1	pak
specifické	specifický	k2eAgFnPc4d1	specifická
součásti	součást	k1gFnPc4	součást
bankovek	bankovka	k1gFnPc2	bankovka
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bankovky	bankovka	k1gFnPc1	bankovka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Koruna	koruna	k1gFnSc1	koruna
československá	československý	k2eAgFnSc1d1	Československá
(	(	kIx(	(
<g/>
bankovky	bankovka	k1gFnSc2	bankovka
ČSSR	ČSSR	kA	ČSSR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mince	mince	k1gFnSc1	mince
</s>
</p>
<p>
<s>
Státovka	státovka	k1gFnSc1	státovka
</s>
</p>
<p>
<s>
Notafilie	Notafilie	k1gFnSc1	Notafilie
</s>
</p>
<p>
<s>
Numismatika	numismatika	k1gFnSc1	numismatika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bankovka	bankovka	k1gFnSc1	bankovka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bankovka	bankovka	k1gFnSc1	bankovka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Numismatické	numismatický	k2eAgFnPc4d1	numismatická
rady	rada	k1gFnPc4	rada
a	a	k8xC	a
zajímavosti	zajímavost	k1gFnPc4	zajímavost
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
o	o	k7c6	o
českých	český	k2eAgFnPc6d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc6d1	slovenská
bankovkách	bankovka	k1gFnPc6	bankovka
a	a	k8xC	a
papírových	papírový	k2eAgNnPc6d1	papírové
platidlech	platidlo	k1gNnPc6	platidlo
(	(	kIx(	(
<g/>
katalog	katalog	k1gInSc1	katalog
<g/>
,	,	kIx,	,
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
informace	informace	k1gFnPc1	informace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
o	o	k7c4	o
notafilii	notafilie	k1gFnSc4	notafilie
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
špičky	špička	k1gFnSc2	špička
české	český	k2eAgFnSc2d1	Česká
notafilie	notafilie	k1gFnSc2	notafilie
<g/>
,	,	kIx,	,
Miloše	Miloš	k1gMnSc2	Miloš
Kudweise	Kudweise	k1gFnSc2	Kudweise
</s>
</p>
<p>
<s>
Významné	významný	k2eAgInPc1d1	významný
články	článek	k1gInPc1	článek
badatele	badatel	k1gMnPc4	badatel
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
notafilie	notafilie	k1gFnSc2	notafilie
Miloše	Miloš	k1gMnSc2	Miloš
Kudweise	Kudweise	k1gFnSc2	Kudweise
-	-	kIx~	-
http://notafilie.numin.cz/	[url]	k?	http://notafilie.numin.cz/
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ron	ron	k1gInSc1	ron
Wise	Wise	k1gInSc1	Wise
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Geographical	Geographical	k1gFnPc7	Geographical
Directory	Director	k1gInPc4	Director
Of	Of	k1gMnSc1	Of
World	World	k1gMnSc1	World
Paper	Paper	k1gMnSc1	Paper
Money	Monea	k1gFnSc2	Monea
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
a	a	k8xC	a
obrázky	obrázek	k1gInPc1	obrázek
rakousko-uherských	rakouskoherský	k2eAgFnPc2d1	rakousko-uherská
bankovek	bankovka	k1gFnPc2	bankovka
</s>
</p>
<p>
<s>
Images	Images	k1gMnSc1	Images
of	of	k?	of
world	world	k1gMnSc1	world
paper	paper	k1gMnSc1	paper
money	monea	k1gFnSc2	monea
</s>
</p>
<p>
<s>
Fate	Fate	k1gFnSc1	Fate
of	of	k?	of
Paper	Paper	k1gInSc4	Paper
Money	Monea	k1gFnSc2	Monea
</s>
</p>
