<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
přirozených	přirozený	k2eAgFnPc6d1	přirozená
formách	forma	k1gFnPc6	forma
–	–	k?	–
vitamín	vitamín	k1gInSc1	vitamín
A1	A1	k1gFnSc1	A1
(	(	kIx(	(
<g/>
retinol	retinol	k1gInSc1	retinol
<g/>
)	)	kIx)	)
a	a	k8xC	a
vitamín	vitamín	k1gInSc1	vitamín
A2	A2	k1gFnSc2	A2
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-dehydroretin	ehydroretin	k2eAgInSc1d1	-dehydroretin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
