<s>
Vitamín	vitamín	k1gInSc1	vitamín
A	a	k9	a
(	(	kIx(	(
<g/>
axeroftol	axeroftol	k1gInSc1	axeroftol
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
rozpustný	rozpustný	k2eAgInSc4d1	rozpustný
vitamín	vitamín	k1gInSc4	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
přirozených	přirozený	k2eAgFnPc6d1	přirozená
formách	forma	k1gFnPc6	forma
–	–	k?	–
vitamín	vitamín	k1gInSc1	vitamín
A1	A1	k1gFnSc1	A1
(	(	kIx(	(
<g/>
retinol	retinol	k1gInSc1	retinol
<g/>
)	)	kIx)	)
a	a	k8xC	a
vitamín	vitamín	k1gInSc1	vitamín
A2	A2	k1gFnSc2	A2
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
-dehydroretin	ehydroretin	k2eAgInSc1d1	-dehydroretin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
A	a	k9	a
je	být	k5eAaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
rodopsinu	rodopsina	k1gFnSc4	rodopsina
<g/>
,	,	kIx,	,
zrakového	zrakový	k2eAgInSc2d1	zrakový
pigmentu	pigment	k1gInSc2	pigment
používaného	používaný	k2eAgInSc2d1	používaný
za	za	k7c2	za
nízkého	nízký	k2eAgNnSc2d1	nízké
osvětlení	osvětlení	k1gNnSc2	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
proto	proto	k8xC	proto
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
šerosleposti	šeroslepost	k1gFnSc3	šeroslepost
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
A	a	k9	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitý	důležitý	k2eAgInSc4d1	důležitý
antioxidant	antioxidant	k1gInSc4	antioxidant
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
epitelií	epitelie	k1gFnPc2	epitelie
<g/>
,	,	kIx,	,
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
nedostatku	nedostatek	k1gInSc6	nedostatek
buňky	buňka	k1gFnSc2	buňka
rohovatí	rohovatět	k5eAaImIp3nS	rohovatět
(	(	kIx(	(
<g/>
xeróza	xeróza	k1gFnSc1	xeróza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
vitaminu	vitamin	k1gInSc2	vitamin
A	a	k8xC	a
je	být	k5eAaImIp3nS	být
alfa-karoten	alfaaroten	k2eAgMnSc1d1	alfa-karoten
<g/>
,	,	kIx,	,
beta-karoten	betaaroten	k2eAgMnSc1d1	beta-karoten
a	a	k8xC	a
lykopen	lykopen	k2eAgMnSc1d1	lykopen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
oranžová	oranžový	k2eAgNnPc1d1	oranžové
a	a	k8xC	a
červená	červený	k2eAgNnPc1d1	červené
barviva	barvivo	k1gNnPc1	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
A	a	k9	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
provitamínu	provitamín	k1gInSc2	provitamín
A	A	kA	A
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
především	především	k9	především
z	z	k7c2	z
betakarotenu	betakaroten	k1gInSc2	betakaroten
<g/>
.	.	kIx.	.
</s>
<s>
Rybí	rybí	k2eAgInSc1d1	rybí
tuk	tuk	k1gInSc1	tuk
<g/>
,	,	kIx,	,
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
mrkev	mrkev	k1gFnSc1	mrkev
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgInPc1d1	zelený
a	a	k8xC	a
žluté	žlutý	k2eAgInPc1d1	žlutý
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
špenát	špenát	k1gInSc1	špenát
<g/>
,	,	kIx,	,
kapusta	kapusta	k1gFnSc1	kapusta
<g/>
,	,	kIx,	,
petrželová	petrželový	k2eAgFnSc1d1	petrželová
nať	nať	k1gFnSc1	nať
<g/>
,	,	kIx,	,
kedlubnová	kedlubnový	k2eAgFnSc1d1	kedlubnová
nať	nať	k1gFnSc1	nať
<g/>
,	,	kIx,	,
meloun	meloun	k1gInSc1	meloun
<g/>
,	,	kIx,	,
meruňky	meruňka	k1gFnPc1	meruňka
<g/>
,	,	kIx,	,
zelí	zelí	k1gNnSc1	zelí
<g/>
,	,	kIx,	,
brokolice	brokolice	k1gFnSc1	brokolice
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
dýně	dýně	k1gFnSc1	dýně
<g/>
,	,	kIx,	,
máslo	máslo	k1gNnSc1	máslo
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vaječný	vaječný	k2eAgInSc1d1	vaječný
žloutek	žloutek	k1gInSc1	žloutek
<g/>
,	,	kIx,	,
v	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
množství	množství	k1gNnSc6	množství
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
tučné	tučný	k2eAgFnPc1d1	tučná
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
třešně	třešeň	k1gFnPc1	třešeň
aj.	aj.	kA	aj.
Nedostatek	nedostatek	k1gInSc1	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
A	a	k9	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
šeroslepost	šeroslepost	k1gFnSc1	šeroslepost
<g/>
,	,	kIx,	,
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
zánětu	zánět	k1gInSc3	zánět
očních	oční	k2eAgFnPc2d1	oční
spojivek	spojivka	k1gFnPc2	spojivka
a	a	k8xC	a
poškození	poškození	k1gNnSc2	poškození
oční	oční	k2eAgFnSc2d1	oční
sítnice	sítnice	k1gFnSc2	sítnice
<g/>
,	,	kIx,	,
rohovatění	rohovatění	k1gNnSc2	rohovatění
a	a	k8xC	a
šupinatění	šupinatění	k1gNnSc2	šupinatění
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
zpomalení	zpomalení	k1gNnSc1	zpomalení
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc4	snížení
potence	potence	k1gFnSc2	potence
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
imunity	imunita	k1gFnSc2	imunita
<g/>
,	,	kIx,	,
sklon	sklon	k1gInSc1	sklon
k	k	k7c3	k
zánětům	zánět	k1gInPc3	zánět
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
další	další	k2eAgInPc4d1	další
poruchy	poruch	k1gInPc4	poruch
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
hypovitaminóza	hypovitaminóza	k1gFnSc1	hypovitaminóza
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
příznakům	příznak	k1gInPc3	příznak
nedostatku	nedostatek	k1gInSc2	nedostatek
vitamínu	vitamín	k1gInSc2	vitamín
patří	patřit	k5eAaImIp3nS	patřit
šeroslepost	šeroslepost	k1gFnSc4	šeroslepost
a	a	k8xC	a
bolest	bolest	k1gFnSc4	bolest
očí	oko	k1gNnPc2	oko
např.	např.	kA	např.
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
ze	z	k7c2	z
tmy	tma	k1gFnSc2	tma
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
je	být	k5eAaImIp3nS	být
však	však	k9	však
i	i	k8xC	i
přebytek	přebytek	k1gInSc1	přebytek
tohoto	tento	k3xDgInSc2	tento
vitamínu	vitamín	k1gInSc2	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
činí	činit	k5eAaImIp3nS	činit
0,8	[number]	k4	0,8
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
či	či	k8xC	či
4,8	[number]	k4	4,8
mg	mg	kA	mg
beta-karotenu	betaaroten	k1gInSc2	beta-karoten
=	=	kIx~	=
poměr	poměr	k1gInSc1	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
užívání	užívání	k1gNnSc6	užívání
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc1d1	nutná
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
přestávky	přestávka	k1gFnPc1	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
Užívání	užívání	k1gNnSc1	užívání
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
konzultovat	konzultovat	k5eAaImF	konzultovat
s	s	k7c7	s
lékařem	lékař	k1gMnSc7	lékař
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
poškodit	poškodit	k5eAaPmF	poškodit
nervový	nervový	k2eAgInSc4d1	nervový
systém	systém	k1gInSc4	systém
plodu	plod	k1gInSc2	plod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInSc1d1	přírodní
provitamín	provitamín	k1gInSc1	provitamín
A	a	k9	a
(	(	kIx(	(
<g/>
beta-karoten	betaaroten	k2eAgInSc4d1	beta-karoten
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
bez	bez	k7c2	bez
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
účinků	účinek	k1gInPc2	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Diabetes	diabetes	k1gInSc1	diabetes
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
potíže	potíž	k1gFnPc4	potíž
v	v	k7c6	v
přeměně	přeměna	k1gFnSc6	přeměna
betakarotenu	betakaroten	k1gInSc2	betakaroten
na	na	k7c4	na
vitamín	vitamín	k1gInSc4	vitamín
A.	A.	kA	A.
Při	při	k7c6	při
nemocech	nemoc	k1gFnPc6	nemoc
srdce	srdce	k1gNnSc2	srdce
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
užívat	užívat	k5eAaImF	užívat
jej	on	k3xPp3gNnSc4	on
formou	forma	k1gFnSc7	forma
sirupů	sirup	k1gInPc2	sirup
či	či	k8xC	či
prášků	prášek	k1gInPc2	prášek
nejčastěji	často	k6eAd3	často
třikrát	třikrát	k6eAd1	třikrát
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
A	a	k9	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
vitamínu	vitamín	k1gInSc2	vitamín
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
hypervitaminózu	hypervitaminóza	k1gFnSc4	hypervitaminóza
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
onemocnění	onemocnění	k1gNnSc1	onemocnění
z	z	k7c2	z
nadbytku	nadbytek	k1gInSc2	nadbytek
vitamínu	vitamín	k1gInSc2	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Ukládá	ukládat	k5eAaImIp3nS	ukládat
se	se	k3xPyFc4	se
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
může	moct	k5eAaImIp3nS	moct
předávkování	předávkování	k1gNnSc1	předávkování
způsobit	způsobit	k5eAaPmF	způsobit
osteoporózu	osteoporóza	k1gFnSc4	osteoporóza
i	i	k8xC	i
otravu	otrava	k1gFnSc4	otrava
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
naštěstí	naštěstí	k6eAd1	naštěstí
nereálné	reálný	k2eNgNnSc1d1	nereálné
<g/>
.	.	kIx.	.
</s>
<s>
Nadbytek	nadbytek	k1gInSc1	nadbytek
vitamínu	vitamín	k1gInSc2	vitamín
A	a	k9	a
v	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
týden	týden	k1gInSc4	týden
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
způsobit	způsobit	k5eAaPmF	způsobit
rozštěpy	rozštěp	k1gInPc4	rozštěp
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
zeleniny	zelenina	k1gFnSc2	zelenina
se	se	k3xPyFc4	se
nemusíme	muset	k5eNaImIp1nP	muset
bát	bát	k5eAaImF	bát
předávkování	předávkování	k1gNnSc4	předávkování
vitamínem	vitamín	k1gInSc7	vitamín
A	a	k8xC	a
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
přebytek	přebytek	k1gInSc1	přebytek
nás	my	k3xPp1nPc4	my
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volně	volně	k6eAd1	volně
prodejných	prodejný	k2eAgInPc6d1	prodejný
výrobcích	výrobek	k1gInPc6	výrobek
s	s	k7c7	s
kapslemi	kapsle	k1gFnPc7	kapsle
vitamínu	vitamín	k1gInSc2	vitamín
A	a	k8xC	a
je	být	k5eAaImIp3nS	být
ho	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
že	že	k8xS	že
předávkování	předávkování	k1gNnSc1	předávkování
prakticky	prakticky	k6eAd1	prakticky
nehrozí	hrozit	k5eNaImIp3nS	hrozit
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
vysokém	vysoký	k2eAgNnSc6d1	vysoké
množství	množství	k1gNnSc6	množství
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
arktických	arktický	k2eAgNnPc2d1	arktické
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
vlků	vlk	k1gMnPc2	vlk
<g/>
,	,	kIx,	,
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
medvědů	medvěd	k1gMnPc2	medvěd
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
otravě	otrava	k1gFnSc3	otrava
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
fatální	fatální	k2eAgFnSc1d1	fatální
<g/>
)	)	kIx)	)
polárních	polární	k2eAgFnPc2d1	polární
expedic	expedice	k1gFnPc2	expedice
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
došly	dojít	k5eAaPmAgInP	dojít
zásoby	zásoba	k1gFnPc4	zásoba
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
sníst	sníst	k5eAaPmF	sníst
své	svůj	k3xOyFgMnPc4	svůj
psy	pes	k1gMnPc4	pes
nebo	nebo	k8xC	nebo
lovit	lovit	k5eAaImF	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Vitamin	vitamin	k1gInSc1	vitamin
A	a	k9	a
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
kosmetických	kosmetický	k2eAgInPc2d1	kosmetický
přípravků	přípravek	k1gInPc2	přípravek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
těch	ten	k3xDgMnPc2	ten
pro	pro	k7c4	pro
omlazení	omlazení	k1gNnSc4	omlazení
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
německého	německý	k2eAgInSc2d1	německý
Spolkového	spolkový	k2eAgInSc2d1	spolkový
institutu	institut	k1gInSc2	institut
pro	pro	k7c4	pro
hodnocení	hodnocení	k1gNnSc4	hodnocení
rizik	riziko	k1gNnPc2	riziko
by	by	kYmCp3nS	by
však	však	k9	však
jeho	jeho	k3xOp3gNnSc4	jeho
užívání	užívání	k1gNnSc4	užívání
v	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
limitováno	limitovat	k5eAaBmNgNnS	limitovat
v	v	k7c6	v
přípravcích	přípravek	k1gInPc6	přípravek
nanášených	nanášený	k2eAgFnPc2d1	nanášená
na	na	k7c4	na
obličej	obličej	k1gInSc4	obličej
a	a	k8xC	a
na	na	k7c6	na
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
produktech	produkt	k1gInPc6	produkt
nanášených	nanášený	k2eAgInPc6d1	nanášený
na	na	k7c4	na
rty	ret	k1gInPc4	ret
(	(	kIx(	(
<g/>
rtěnky	rtěnka	k1gFnPc4	rtěnka
<g/>
,	,	kIx,	,
balzámy	balzám	k1gInPc4	balzám
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
určených	určený	k2eAgFnPc2d1	určená
k	k	k7c3	k
péči	péče	k1gFnSc3	péče
o	o	k7c4	o
tělo	tělo	k1gNnSc4	tělo
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
neměl	mít	k5eNaImAgInS	mít
používat	používat	k5eAaImF	používat
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
německých	německý	k2eAgMnPc2d1	německý
expertů	expert	k1gMnPc2	expert
máme	mít	k5eAaImIp1nP	mít
vitaminu	vitamin	k1gInSc3	vitamin
A	a	k8xC	a
dostatek	dostatek	k1gInSc4	dostatek
v	v	k7c6	v
jídle	jídlo	k1gNnSc6	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
žen	žena	k1gFnPc2	žena
po	po	k7c6	po
menopauze	menopauza	k1gFnSc6	menopauza
dokonce	dokonce	k9	dokonce
příjem	příjem	k1gInSc1	příjem
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
maximální	maximální	k2eAgFnSc4d1	maximální
doporučenou	doporučený	k2eAgFnSc4d1	Doporučená
dávku	dávka	k1gFnSc4	dávka
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
žádoucí	žádoucí	k2eAgInSc4d1	žádoucí
jeho	jeho	k3xOp3gInSc4	jeho
příjem	příjem	k1gInSc4	příjem
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kosmetiky	kosmetika	k1gFnSc2	kosmetika
<g/>
.	.	kIx.	.
</s>
