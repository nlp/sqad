<s>
Pavouci	pavouk	k1gMnPc1	pavouk
(	(	kIx(	(
<g/>
Araneae	Araneae	k1gInSc1	Araneae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
členovci	členovec	k1gMnPc1	členovec
s	s	k7c7	s
osmi	osm	k4xCc7	osm
nohama	noha	k1gFnPc7	noha
a	a	k8xC	a
chelicerami	chelicera	k1gFnPc7	chelicera
(	(	kIx(	(
<g/>
klepítky	klepítko	k1gNnPc7	klepítko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgInPc2	který
ústí	ústit	k5eAaImIp3nS	ústit
vývod	vývod	k1gInSc1	vývod
jedových	jedový	k2eAgFnPc2d1	jedová
žláz	žláza	k1gFnPc2	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
největším	veliký	k2eAgInSc7d3	veliký
řádem	řád	k1gInSc7	řád
pavoukovců	pavoukovec	k1gMnPc2	pavoukovec
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
druhovou	druhový	k2eAgFnSc7d1	druhová
rozmanitostí	rozmanitost	k1gFnSc7	rozmanitost
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
kontinentu	kontinent	k1gInSc6	kontinent
kromě	kromě	k7c2	kromě
Antarktidy	Antarktida	k1gFnSc2	Antarktida
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
prostředí	prostředí	k1gNnSc6	prostředí
kromě	kromě	k7c2	kromě
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prosinci	prosinec	k1gInSc3	prosinec
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
43	[number]	k4	43
678	[number]	k4	678
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
109	[number]	k4	109
čeledí	čeleď	k1gFnPc2	čeleď
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
;	;	kIx,	;
nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
komunitě	komunita	k1gFnSc6	komunita
panují	panovat	k5eAaImIp3nP	panovat
nejasnosti	nejasnost	k1gFnPc4	nejasnost
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
tyto	tento	k3xDgFnPc4	tento
čeledi	čeleď	k1gFnPc4	čeleď
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
počet	počet	k1gInSc4	počet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
různých	různý	k2eAgFnPc2d1	různá
klasifikací	klasifikace	k1gFnPc2	klasifikace
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
879	[number]	k4	879
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
27	[number]	k4	27
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
v	v	k7c6	v
ČR	ČR	kA	ČR
již	již	k6eAd1	již
vyhynulé	vyhynulý	k2eAgNnSc4d1	vyhynulé
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
483	[number]	k4	483
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
třetího	třetí	k4xOgNnSc2	třetí
vydání	vydání	k1gNnSc2	vydání
Červeného	Červeného	k2eAgInSc2d1	Červeného
seznamu	seznam	k1gInSc2	seznam
pavouků	pavouk	k1gMnPc2	pavouk
ČR	ČR	kA	ČR
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
stupni	stupeň	k1gInSc6	stupeň
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
tvůrců	tvůrce	k1gMnPc2	tvůrce
červeného	červený	k2eAgInSc2d1	červený
seznamu	seznam	k1gInSc2	seznam
o	o	k7c6	o
příčinách	příčina	k1gFnPc6	příčina
jeho	jeho	k3xOp3gFnSc2	jeho
aktualizace	aktualizace	k1gFnSc2	aktualizace
a	a	k8xC	a
diskuse	diskuse	k1gFnSc2	diskuse
proč	proč	k6eAd1	proč
někteří	některý	k3yIgMnPc1	některý
pavouci	pavouk	k1gMnPc1	pavouk
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
přírody	příroda	k1gFnSc2	příroda
mizí	mizet	k5eAaImIp3nS	mizet
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
dostupný	dostupný	k2eAgInSc1d1	dostupný
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
pořad	pořad	k1gInSc1	pořad
Planetárium	planetárium	k1gNnSc4	planetárium
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Anatomicky	anatomicky	k6eAd1	anatomicky
se	se	k3xPyFc4	se
pavouci	pavouk	k1gMnPc1	pavouk
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
členovců	členovec	k1gMnPc2	členovec
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinou	většinou	k6eAd1	většinou
mají	mít	k5eAaImIp3nP	mít
tělo	tělo	k1gNnSc1	tělo
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
tagmata	tagma	k1gNnPc4	tagma
(	(	kIx(	(
<g/>
funkční	funkční	k2eAgInPc1d1	funkční
celky	celek	k1gInPc1	celek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavohruď	hlavohruď	k1gFnSc4	hlavohruď
a	a	k8xC	a
zadeček	zadeček	k1gInSc4	zadeček
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
spojena	spojit	k5eAaPmNgFnS	spojit
malou	malý	k2eAgFnSc7d1	malá
válcovitou	válcovitý	k2eAgFnSc7d1	válcovitá
stopkou	stopka	k1gFnSc7	stopka
(	(	kIx(	(
<g/>
pedicelem	pedicel	k1gInSc7	pedicel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hmyzu	hmyz	k1gInSc2	hmyz
nemají	mít	k5eNaImIp3nP	mít
tykadla	tykadlo	k1gNnPc4	tykadlo
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
nejprimitivnější	primitivní	k2eAgFnSc4d3	nejprimitivnější
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
sklípkoše	sklípkoš	k1gMnPc4	sklípkoš
(	(	kIx(	(
<g/>
Mesothelae	Mesothelae	k1gInSc1	Mesothelae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
pavouci	pavouk	k1gMnPc1	pavouk
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
členovců	členovec	k1gMnPc2	členovec
nejvíce	nejvíce	k6eAd1	nejvíce
centralizovanou	centralizovaný	k2eAgFnSc4d1	centralizovaná
nervovou	nervový	k2eAgFnSc4d1	nervová
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
všechny	všechen	k3xTgInPc1	všechen
jejich	jejich	k3xOp3gInPc1	jejich
gangliony	ganglion	k1gInPc1	ganglion
(	(	kIx(	(
<g/>
nervové	nervový	k2eAgFnPc1d1	nervová
uzliny	uzlina	k1gFnPc1	uzlina
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
hlavohrudi	hlavohruď	k1gFnSc6	hlavohruď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
členovců	členovec	k1gMnPc2	členovec
nemají	mít	k5eNaImIp3nP	mít
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
končetinách	končetina	k1gFnPc6	končetina
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgInPc1	žádný
extenzory	extenzor	k1gInPc1	extenzor
(	(	kIx(	(
<g/>
natahovače	natahovač	k1gInPc1	natahovač
<g/>
)	)	kIx)	)
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
natahují	natahovat	k5eAaImIp3nP	natahovat
hydraulickým	hydraulický	k2eAgInSc7d1	hydraulický
tlakem	tlak	k1gInSc7	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Zadečkové	zadečkový	k2eAgFnPc1d1	zadečková
končetiny	končetina	k1gFnPc1	končetina
pavouků	pavouk	k1gMnPc2	pavouk
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
ve	v	k7c4	v
snovací	snovací	k2eAgFnPc4d1	snovací
bradavky	bradavka	k1gFnPc4	bradavka
produkující	produkující	k2eAgFnSc1d1	produkující
pavučinová	pavučinový	k2eAgFnSc1d1	pavučinová
vlákna	vlákna	k1gFnSc1	vlákna
až	až	k9	až
z	z	k7c2	z
šesti	šest	k4xCc2	šest
druhů	druh	k1gInPc2	druh
snovacích	snovací	k2eAgFnPc2d1	snovací
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
zadečku	zadeček	k1gInSc6	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Pavučiny	pavučina	k1gFnPc1	pavučina
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
použitého	použitý	k2eAgNnSc2d1	Použité
lepkavého	lepkavý	k2eAgNnSc2d1	lepkavé
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
kruhové	kruhový	k2eAgFnPc1d1	kruhová
pavučiny	pavučina	k1gFnPc1	pavučina
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgInPc1	jeden
ze	z	k7c2	z
starších	starý	k2eAgInPc2d2	starší
druhů	druh	k1gInPc2	druh
pavučin	pavučina	k1gFnPc2	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
vytvářející	vytvářející	k2eAgMnSc1d1	vytvářející
pavučinové	pavučinový	k2eAgInPc4d1	pavučinový
vaky	vak	k1gInPc4	vak
jsou	být	k5eAaImIp3nP	být
rozšířenější	rozšířený	k2eAgMnPc1d2	rozšířenější
a	a	k8xC	a
rozmanitější	rozmanitý	k2eAgMnPc1d2	rozmanitější
než	než	k8xS	než
křižákovití	křižákovitý	k2eAgMnPc1d1	křižákovitý
(	(	kIx(	(
<g/>
kteří	který	k3yIgMnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
právě	právě	k9	právě
kruhové	kruhový	k2eAgFnPc1d1	kruhová
pavučiny	pavučina	k1gFnPc1	pavučina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pavoukům	pavouk	k1gMnPc3	pavouk
podobní	podobný	k2eAgMnPc1d1	podobný
pavoukovci	pavoukovec	k1gMnPc1	pavoukovec
s	s	k7c7	s
orgány	orgán	k1gInPc7	orgán
produkujícími	produkující	k2eAgInPc7d1	produkující
pavučinová	pavučinový	k2eAgNnPc4d1	pavučinové
vlákna	vlákno	k1gNnPc4	vlákno
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
devonu	devon	k1gInSc6	devon
někdy	někdy	k6eAd1	někdy
před	před	k7c7	před
386	[number]	k4	386
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
těmto	tento	k3xDgMnPc3	tento
živočichům	živočich	k1gMnPc3	živočich
chyběly	chybět	k5eAaImAgFnP	chybět
snovací	snovací	k2eAgFnPc4d1	snovací
bradavky	bradavka	k1gFnPc4	bradavka
<g/>
.	.	kIx.	.
</s>
<s>
Praví	pravý	k2eAgMnPc1d1	pravý
pavouci	pavouk	k1gMnPc1	pavouk
byli	být	k5eAaImAgMnP	být
nalezeni	nalézt	k5eAaBmNgMnP	nalézt
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
z	z	k7c2	z
karbonu	karbon	k1gInSc2	karbon
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
před	před	k7c7	před
318	[number]	k4	318
až	až	k8xS	až
299	[number]	k4	299
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
podobní	podobný	k2eAgMnPc1d1	podobný
nejprimitivnějšímu	primitivní	k2eAgInSc3d3	nejprimitivnější
žijícímu	žijící	k2eAgInSc3d1	žijící
podřádu	podřád	k1gInSc3	podřád
<g/>
,	,	kIx,	,
sklípkošům	sklípkoš	k1gInPc3	sklípkoš
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
skupiny	skupina	k1gFnPc1	skupina
moderních	moderní	k2eAgMnPc2d1	moderní
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
sklípkani	sklípkan	k1gMnPc5	sklípkan
(	(	kIx(	(
<g/>
Mygalomorphae	Mygalomorphae	k1gNnSc4	Mygalomorphae
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvouplicní	dvouplicní	k2eAgFnSc4d1	dvouplicní
(	(	kIx(	(
<g/>
Araneomorphae	Araneomorphae	k1gFnSc4	Araneomorphae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
triasu	trias	k1gInSc6	trias
před	před	k7c7	před
200	[number]	k4	200
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
potravní	potravní	k2eAgFnSc1d1	potravní
specializace	specializace	k1gFnSc1	specializace
druhu	druh	k1gInSc2	druh
Bagheera	Bagheero	k1gNnSc2	Bagheero
kiplingi	kipling	k1gFnSc2	kipling
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
druhy	druh	k1gInPc4	druh
jsou	být	k5eAaImIp3nP	být
predátoři	predátor	k1gMnPc1	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
ostatními	ostatní	k2eAgMnPc7d1	ostatní
pavouky	pavouk	k1gMnPc7	pavouk
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
několik	několik	k4yIc1	několik
velkých	velký	k2eAgInPc2d1	velký
druhů	druh	k1gInPc2	druh
loví	lovit	k5eAaImIp3nP	lovit
i	i	k9	i
ptáky	pták	k1gMnPc4	pták
a	a	k8xC	a
ještěrky	ještěrka	k1gFnPc4	ještěrka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
lovení	lovení	k1gNnSc3	lovení
kořisti	kořist	k1gFnSc2	kořist
pavouci	pavouk	k1gMnPc1	pavouk
využívají	využívat	k5eAaImIp3nP	využívat
různé	různý	k2eAgFnPc4d1	různá
strategie	strategie	k1gFnPc4	strategie
<g/>
:	:	kIx,	:
chytají	chytat	k5eAaImIp3nP	chytat
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
lepkavých	lepkavý	k2eAgFnPc2d1	lepkavá
pavučin	pavučina	k1gFnPc2	pavučina
<g/>
;	;	kIx,	;
vrhají	vrhat	k5eAaImIp3nP	vrhat
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
lepkavá	lepkavý	k2eAgFnSc1d1	lepkavá
vlákna	vlákna	k1gFnSc1	vlákna
<g/>
;	;	kIx,	;
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
neodhalila	odhalit	k5eNaPmAgFnS	odhalit
<g/>
;	;	kIx,	;
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
uženou	uhnat	k5eAaPmIp3nP	uhnat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
kořist	kořist	k1gFnSc1	kořist
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
pomocí	pomocí	k7c2	pomocí
vibrací	vibrace	k1gFnPc2	vibrace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aktivní	aktivní	k2eAgMnPc1d1	aktivní
lovci	lovec	k1gMnPc1	lovec
mají	mít	k5eAaImIp3nP	mít
dobrý	dobrý	k2eAgInSc4d1	dobrý
zrak	zrak	k1gInSc4	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Lovci	lovec	k1gMnPc1	lovec
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
skákavka	skákavka	k1gFnSc1	skákavka
(	(	kIx(	(
<g/>
Portia	Portia	k1gFnSc1	Portia
<g/>
)	)	kIx)	)
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
znaky	znak	k1gInPc1	znak
inteligentního	inteligentní	k2eAgNnSc2d1	inteligentní
chování	chování	k1gNnSc2	chování
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
taktiky	taktika	k1gFnSc2	taktika
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
ve	v	k7c6	v
schopnosti	schopnost	k1gFnSc6	schopnost
vytvářet	vytvářet	k5eAaImF	vytvářet
nové	nový	k2eAgNnSc4d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Střeva	střevo	k1gNnPc1	střevo
pavouků	pavouk	k1gMnPc2	pavouk
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
úzká	úzký	k2eAgFnSc1d1	úzká
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
přijímat	přijímat	k5eAaImF	přijímat
pevnou	pevný	k2eAgFnSc4d1	pevná
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
rozmělňují	rozmělňovat	k5eAaImIp3nP	rozmělňovat
trávicími	trávicí	k2eAgInPc7d1	trávicí
enzymy	enzym	k1gInPc7	enzym
a	a	k8xC	a
drtí	drtit	k5eAaImIp3nP	drtit
ji	on	k3xPp3gFnSc4	on
makadly	makadlo	k1gNnPc7	makadlo
(	(	kIx(	(
<g/>
pedipalpy	pedipalp	k1gInPc7	pedipalp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemají	mít	k5eNaImIp3nP	mít
pravé	pravý	k2eAgFnPc4d1	pravá
čelisti	čelist	k1gFnPc4	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Samečci	sameček	k1gMnPc1	sameček
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
různými	různý	k2eAgInPc7d1	různý
namlouvacími	namlouvací	k2eAgInPc7d1	namlouvací
rituály	rituál	k1gInPc7	rituál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gNnSc4	on
samičky	samička	k1gFnPc1	samička
nesežraly	sežrat	k5eNaPmAgFnP	sežrat
<g/>
.	.	kIx.	.
</s>
<s>
Samečci	sameček	k1gMnPc1	sameček
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
přežívají	přežívat	k5eAaImIp3nP	přežívat
několik	několik	k4yIc4	několik
páření	páření	k1gNnPc2	páření
a	a	k8xC	a
umírají	umírat	k5eAaImIp3nP	umírat
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
příčin	příčina	k1gFnPc2	příčina
<g/>
,	,	kIx,	,
než	než	k8xS	než
že	že	k8xS	že
by	by	kYmCp3nS	by
je	on	k3xPp3gNnSc4	on
sežrala	sežrat	k5eAaPmAgFnS	sežrat
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc4	samička
z	z	k7c2	z
pavučinových	pavučinový	k2eAgNnPc2d1	pavučinové
vláken	vlákno	k1gNnPc2	vlákno
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
kokony	kokon	k1gInPc1	kokon
na	na	k7c4	na
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
až	až	k6eAd1	až
stovky	stovka	k1gFnPc4	stovka
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc1	samička
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
o	o	k7c4	o
svá	svůj	k3xOyFgNnPc4	svůj
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
nosí	nosit	k5eAaImIp3nP	nosit
s	s	k7c7	s
sebou	se	k3xPyFc7	se
či	či	k8xC	či
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
dělí	dělit	k5eAaImIp3nS	dělit
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
sociální	sociální	k2eAgFnSc1d1	sociální
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
společné	společný	k2eAgFnPc4d1	společná
pavučiny	pavučina	k1gFnPc4	pavučina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
několika	několik	k4yIc7	několik
málo	málo	k4c1	málo
až	až	k6eAd1	až
50	[number]	k4	50
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgNnSc1d1	sociální
chování	chování	k1gNnSc1	chování
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
opatrné	opatrný	k2eAgFnSc2d1	opatrná
tolerance	tolerance	k1gFnSc2	tolerance
až	až	k9	až
ke	k	k7c3	k
společnému	společný	k2eAgNnSc3d1	společné
lovení	lovení	k1gNnSc3	lovení
a	a	k8xC	a
sdílení	sdílení	k1gNnSc4	sdílení
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
pavouků	pavouk	k1gMnPc2	pavouk
dožívá	dožívat	k5eAaImIp3nS	dožívat
maximálně	maximálně	k6eAd1	maximálně
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
sklípkani	sklípkan	k1gMnPc1	sklípkan
se	se	k3xPyFc4	se
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
mohou	moct	k5eAaImIp3nP	moct
dožít	dožít	k5eAaPmF	dožít
až	až	k9	až
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jed	jed	k1gInSc1	jed
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
<g/>
,	,	kIx,	,
vědci	vědec	k1gMnPc1	vědec
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
potenciální	potenciální	k2eAgNnSc1d1	potenciální
využití	využití	k1gNnSc1	využití
pavoučího	pavoučí	k2eAgInSc2d1	pavoučí
jedu	jed	k1gInSc2	jed
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
a	a	k8xC	a
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
pesticid	pesticid	k1gInSc4	pesticid
neznečišťující	znečišťující	k2eNgNnSc4d1	znečišťující
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pavoučí	pavoučí	k2eAgNnSc1d1	pavoučí
vlákno	vlákno	k1gNnSc1	vlákno
představuje	představovat	k5eAaImIp3nS	představovat
lepší	dobrý	k2eAgFnSc4d2	lepší
kombinaci	kombinace	k1gFnSc4	kombinace
lehkosti	lehkost	k1gFnSc2	lehkost
<g/>
,	,	kIx,	,
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
pružnosti	pružnost	k1gFnPc4	pružnost
než	než	k8xS	než
umělé	umělý	k2eAgInPc4d1	umělý
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
a	a	k8xC	a
vědci	vědec	k1gMnPc1	vědec
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
vložit	vložit	k5eAaPmF	vložit
geny	gen	k1gInPc4	gen
produkující	produkující	k2eAgFnSc2d1	produkující
tato	tento	k3xDgNnPc1	tento
vlákna	vlákno	k1gNnPc1	vlákno
do	do	k7c2	do
DNA	dno	k1gNnSc2	dno
různých	různý	k2eAgMnPc2d1	různý
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tyto	tento	k3xDgInPc1	tento
organismy	organismus	k1gInPc1	organismus
mohou	moct	k5eAaImIp3nP	moct
vlákna	vlákno	k1gNnPc4	vlákno
vytvářet	vytvářet	k5eAaImF	vytvářet
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
rozmanitému	rozmanitý	k2eAgNnSc3d1	rozmanité
chování	chování	k1gNnSc3	chování
se	se	k3xPyFc4	se
pavouci	pavouk	k1gMnPc1	pavouk
stali	stát	k5eAaPmAgMnP	stát
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
a	a	k8xC	a
mytologii	mytologie	k1gFnSc6	mytologie
symbolem	symbol	k1gInSc7	symbol
trpělivosti	trpělivost	k1gFnSc2	trpělivost
<g/>
,	,	kIx,	,
krutosti	krutost	k1gFnSc2	krutost
a	a	k8xC	a
kreativity	kreativita	k1gFnSc2	kreativita
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
jsou	být	k5eAaImIp3nP	být
klepítkatci	klepítkatec	k1gMnPc1	klepítkatec
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
členovci	členovec	k1gMnPc1	členovec
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
členovci	členovec	k1gMnPc1	členovec
mají	mít	k5eAaImIp3nP	mít
článkovité	článkovitý	k2eAgNnSc4d1	článkovité
tělo	tělo	k1gNnSc4	tělo
s	s	k7c7	s
párovými	párový	k2eAgFnPc7d1	párová
končetinami	končetina	k1gFnPc7	končetina
<g/>
,	,	kIx,	,
pokryté	pokrytý	k2eAgFnPc1d1	pokrytá
kutikulou	kutikula	k1gFnSc7	kutikula
z	z	k7c2	z
chitinu	chitin	k1gInSc2	chitin
a	a	k8xC	a
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
;	;	kIx,	;
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
několika	několik	k4yIc2	několik
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
splývají	splývat	k5eAaImIp3nP	splývat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
embryonálního	embryonální	k2eAgInSc2d1	embryonální
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
klepítkatci	klepítkatec	k1gMnPc1	klepítkatec
mají	mít	k5eAaImIp3nP	mít
tělo	tělo	k1gNnSc4	tělo
složené	složený	k2eAgNnSc4d1	složené
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
tagmat	tagma	k1gNnPc2	tagma
neboli	neboli	k8xC	neboli
souborů	soubor	k1gInPc2	soubor
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
funkci	funkce	k1gFnSc4	funkce
<g/>
:	:	kIx,	:
přední	přední	k2eAgFnSc1d1	přední
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hlavohruď	hlavohruď	k1gFnSc1	hlavohruď
neboli	neboli	k8xC	neboli
cephalothorax	cephalothorax	k1gInSc1	cephalothorax
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
u	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
dvě	dva	k4xCgNnPc4	dva
různá	různý	k2eAgNnPc4d1	různé
tagmata	tagma	k1gNnPc4	tagma
<g/>
,	,	kIx,	,
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
hruď	hruď	k1gFnSc4	hruď
<g/>
;	;	kIx,	;
zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
zadeček	zadeček	k1gInSc1	zadeček
neboli	neboli	k8xC	neboli
abdomen	abdomen	k1gInSc1	abdomen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pavouků	pavouk	k1gMnPc2	pavouk
jsou	být	k5eAaImIp3nP	být
hlavohruď	hlavohruď	k1gFnSc4	hlavohruď
a	a	k8xC	a
zadeček	zadeček	k1gInSc4	zadeček
spojeny	spojit	k5eAaPmNgFnP	spojit
malou	malý	k2eAgFnSc7d1	malá
válcovitou	válcovitý	k2eAgFnSc7d1	válcovitá
stopkou	stopka	k1gFnSc7	stopka
<g/>
,	,	kIx,	,
pedicelem	pedicel	k1gInSc7	pedicel
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
splývání	splývání	k1gNnSc2	splývání
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yQgFnPc2	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
hlava	hlava	k1gFnSc1	hlava
klepítkatců	klepítkatec	k1gMnPc2	klepítkatec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
členovci	členovec	k1gMnPc7	členovec
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
<g/>
:	:	kIx,	:
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obvykle	obvykle	k6eAd1	obvykle
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
nejpřednější	přední	k2eAgFnSc1d3	nejpřednější
část	část	k1gFnSc1	část
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
zaniká	zanikat	k5eAaImIp3nS	zanikat
v	v	k7c6	v
brzkém	brzký	k2eAgNnSc6d1	brzké
stádiu	stádium	k1gNnSc6	stádium
vývoje	vývoj	k1gInSc2	vývoj
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
nemají	mít	k5eNaImIp3nP	mít
tykadla	tykadlo	k1gNnPc1	tykadlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
členovců	členovec	k1gMnPc2	členovec
typická	typický	k2eAgFnSc1d1	typická
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgFnPc7d1	jediná
končetinami	končetina	k1gFnPc7	končetina
nacházejícími	nacházející	k2eAgFnPc7d1	nacházející
se	se	k3xPyFc4	se
před	před	k7c7	před
ústy	ústa	k1gNnPc7	ústa
je	být	k5eAaImIp3nS	být
pár	pár	k4xCyI	pár
chelicer	chelicero	k1gNnPc2	chelicero
(	(	kIx(	(
<g/>
klepítek	klepítko	k1gNnPc2	klepítko
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
klepítkatci	klepítkatec	k1gMnPc1	klepítkatec
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgFnPc4	žádný
"	"	kIx"	"
<g/>
čelisti	čelist	k1gFnPc1	čelist
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
končetiny	končetina	k1gFnPc1	končetina
za	za	k7c7	za
ústy	ústa	k1gNnPc7	ústa
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
makadla	makadlo	k1gNnPc1	makadlo
a	a	k8xC	a
u	u	k7c2	u
různých	různý	k2eAgFnPc2d1	různá
skupin	skupina	k1gFnPc2	skupina
klepítkatců	klepítkatec	k1gMnPc2	klepítkatec
slouží	sloužit	k5eAaImIp3nS	sloužit
různým	různý	k2eAgInPc3d1	různý
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
štíry	štír	k1gMnPc7	štír
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
skupinami	skupina	k1gFnPc7	skupina
patří	patřit	k5eAaImIp3nP	patřit
pavouci	pavouk	k1gMnPc1	pavouk
mezi	mezi	k7c4	mezi
pavoukovce	pavoukovec	k1gInPc4	pavoukovec
<g/>
.	.	kIx.	.
</s>
<s>
Chelicery	Chelicera	k1gFnPc1	Chelicera
štírů	štír	k1gMnPc2	štír
mají	mít	k5eAaImIp3nP	mít
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přijímání	přijímání	k1gNnSc3	přijímání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Chelicery	Chelicera	k1gFnPc1	Chelicera
pavouků	pavouk	k1gMnPc2	pavouk
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zakončeny	zakončen	k2eAgInPc1d1	zakončen
drápky	drápek	k1gInPc1	drápek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
a	a	k8xC	a
když	když	k8xS	když
je	on	k3xPp3gNnSc4	on
pavouci	pavouk	k1gMnPc1	pavouk
zrovna	zrovna	k6eAd1	zrovna
nepoužívají	používat	k5eNaImIp3nP	používat
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nP	nosit
je	on	k3xPp3gInPc4	on
ohnuté	ohnutý	k2eAgInPc4d1	ohnutý
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnPc1d1	horní
části	část	k1gFnPc1	část
mívají	mívat	k5eAaImIp3nP	mívat
husté	hustý	k2eAgNnSc4d1	husté
ochlupení	ochlupení	k1gNnSc4	ochlupení
tvořící	tvořící	k2eAgNnSc4d1	tvořící
"	"	kIx"	"
<g/>
síto	síto	k1gNnSc4	síto
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nepropouští	propouštět	k5eNaImIp3nP	propouštět
pevné	pevný	k2eAgInPc1d1	pevný
kusy	kus	k1gInPc1	kus
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pavouci	pavouk	k1gMnPc1	pavouk
mohou	moct	k5eAaImIp3nP	moct
přijímat	přijímat	k5eAaImF	přijímat
pouze	pouze	k6eAd1	pouze
tekutou	tekutý	k2eAgFnSc4d1	tekutá
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Makadla	makadlo	k1gNnSc2	makadlo
štírů	štír	k1gMnPc2	štír
většinou	většinou	k6eAd1	většinou
tvoří	tvořit	k5eAaImIp3nS	tvořit
velká	velký	k2eAgNnPc4d1	velké
klepeta	klepeto	k1gNnPc4	klepeto
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
štíři	štír	k1gMnPc1	štír
loví	lovit	k5eAaImIp3nP	lovit
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pavouci	pavouk	k1gMnPc1	pavouk
mají	mít	k5eAaImIp3nP	mít
relativně	relativně	k6eAd1	relativně
malá	malý	k2eAgNnPc4d1	malé
makadla	makadlo	k1gNnPc4	makadlo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
konce	konec	k1gInPc1	konec
ovšem	ovšem	k9	ovšem
také	také	k9	také
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
krmení	krmení	k1gNnSc3	krmení
<g/>
;	;	kIx,	;
navíc	navíc	k6eAd1	navíc
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
konce	konec	k1gInSc2	konec
zvětšené	zvětšený	k2eAgFnSc3d1	zvětšená
a	a	k8xC	a
využívají	využívat	k5eAaImIp3nP	využívat
je	on	k3xPp3gNnSc4	on
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
spermatu	sperma	k1gNnSc2	sperma
<g/>
.	.	kIx.	.
</s>
<s>
Stopka	stopka	k1gFnSc1	stopka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
hlavohruď	hlavohruď	k1gFnSc4	hlavohruď
a	a	k8xC	a
zadeček	zadeček	k1gInSc4	zadeček
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zadečku	zadeček	k1gInSc3	zadeček
nezávislý	závislý	k2eNgInSc1d1	nezávislý
pohyb	pohyb	k1gInSc1	pohyb
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
pavučinových	pavučinový	k2eAgNnPc2d1	pavučinové
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
strana	strana	k1gFnSc1	strana
hlavohrudi	hlavohruď	k1gFnSc2	hlavohruď
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
konvexním	konvexní	k2eAgInSc7d1	konvexní
karapaxem	karapax	k1gInSc7	karapax
a	a	k8xC	a
spodní	spodní	k2eAgFnSc1d1	spodní
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
kryta	kryt	k2eAgFnSc1d1	kryta
dvěma	dva	k4xCgFnPc7	dva
plochými	plochý	k2eAgFnPc7d1	plochá
destičkami	destička	k1gFnPc7	destička
<g/>
.	.	kIx.	.
</s>
<s>
Zadeček	zadeček	k1gInSc1	zadeček
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vejcovitý	vejcovitý	k2eAgInSc4d1	vejcovitý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
sklípkošů	sklípkoš	k1gMnPc2	sklípkoš
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
horní	horní	k2eAgFnSc1d1	horní
strana	strana	k1gFnSc1	strana
členitá	členitý	k2eAgFnSc1d1	členitá
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
jedním	jeden	k4xCgInSc7	jeden
celkem	celek	k1gInSc7	celek
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členovci	členovec	k1gMnPc1	členovec
mají	mít	k5eAaImIp3nP	mít
coelom	coelom	k1gInSc4	coelom
(	(	kIx(	(
<g/>
pravou	pravý	k2eAgFnSc4d1	pravá
tělní	tělní	k2eAgFnSc4d1	tělní
dutinu	dutina	k1gFnSc4	dutina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
oblastech	oblast	k1gFnPc6	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
rozmnožovací	rozmnožovací	k2eAgFnSc2d1	rozmnožovací
a	a	k8xC	a
vylučovací	vylučovací	k2eAgFnSc2d1	vylučovací
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
místo	místo	k1gNnSc1	místo
zabírá	zabírat	k5eAaImIp3nS	zabírat
převážně	převážně	k6eAd1	převážně
dutina	dutina	k1gFnSc1	dutina
probíhající	probíhající	k2eAgFnSc1d1	probíhající
téměř	téměř	k6eAd1	téměř
celým	celý	k2eAgNnSc7d1	celé
tělem	tělo	k1gNnSc7	tělo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mixocoel	mixocoel	k1gInSc1	mixocoel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
proudí	proudit	k5eAaPmIp3nP	proudit
krvomíza	krvomíz	k1gMnSc4	krvomíz
-	-	kIx~	-
hemolymfa	hemolymf	k1gMnSc4	hemolymf
.	.	kIx.	.
</s>
<s>
Srdce	srdce	k1gNnSc1	srdce
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
trubicí	trubice	k1gFnSc7	trubice
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
několika	několik	k4yIc2	několik
polopropustnými	polopropustný	k2eAgFnPc7d1	polopropustná
chlopněmi	chlopeň	k1gFnPc7	chlopeň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vstup	vstup	k1gInSc4	vstup
hemolymfy	hemolymf	k1gInPc1	hemolymf
z	z	k7c2	z
mixocoelu	mixocoel	k1gInSc2	mixocoel
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pavouků	pavouk	k1gMnPc2	pavouk
ale	ale	k9	ale
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
pouze	pouze	k6eAd1	pouze
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
zadečku	zadeček	k1gInSc2	zadeček
a	a	k8xC	a
hemolymfa	hemolymf	k1gMnSc2	hemolymf
je	být	k5eAaImIp3nS	být
vylučována	vylučovat	k5eAaImNgFnS	vylučovat
do	do	k7c2	do
mixocoelu	mixocoel	k1gInSc2	mixocoel
jednou	jednou	k6eAd1	jednou
tepnou	tepna	k1gFnSc7	tepna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
zadečku	zadeček	k1gInSc2	zadeček
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
rozvětvujícími	rozvětvující	k2eAgFnPc7d1	rozvětvující
se	se	k3xPyFc4	se
tepnami	tepna	k1gFnPc7	tepna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
procházejí	procházet	k5eAaImIp3nP	procházet
stopkou	stopka	k1gFnSc7	stopka
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
několika	několik	k4yIc2	několik
částí	část	k1gFnPc2	část
hlavohrudi	hlavohruď	k1gFnSc2	hlavohruď
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
pavouci	pavouk	k1gMnPc1	pavouk
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
oběhovou	oběhový	k2eAgFnSc4d1	oběhová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Hemolymfa	Hemolymf	k1gMnSc2	Hemolymf
mnoha	mnoho	k4c2	mnoho
pavouků	pavouk	k1gMnPc2	pavouk
s	s	k7c7	s
plicními	plicní	k2eAgInPc7d1	plicní
vaky	vak	k1gInPc7	vak
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
respirační	respirační	k2eAgInSc1d1	respirační
pigment	pigment	k1gInSc1	pigment
(	(	kIx(	(
<g/>
dýchací	dýchací	k2eAgNnSc1d1	dýchací
barvivo	barvivo	k1gNnSc1	barvivo
<g/>
)	)	kIx)	)
hemocyanin	hemocyanina	k1gFnPc2	hemocyanina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
efektivnější	efektivní	k2eAgInSc4d2	efektivnější
přenos	přenos	k1gInSc4	přenos
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pavouků	pavouk	k1gMnPc2	pavouk
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
soustav	soustava	k1gFnPc2	soustava
založené	založený	k2eAgFnPc4d1	založená
na	na	k7c6	na
plicních	plicní	k2eAgInPc6d1	plicní
vacích	vak	k1gInPc6	vak
<g/>
,	,	kIx,	,
vzdušnicích	vzdušnice	k1gFnPc6	vzdušnice
(	(	kIx(	(
<g/>
trachejích	trachej	k1gInPc6	trachej
<g/>
)	)	kIx)	)
či	či	k8xC	či
na	na	k7c6	na
kombinaci	kombinace	k1gFnSc6	kombinace
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Sklípkani	sklípkan	k1gMnPc1	sklípkan
a	a	k8xC	a
sklípkoši	sklípkoš	k1gMnPc1	sklípkoš
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgInPc1	dva
páry	pár	k1gInPc1	pár
plicních	plicní	k2eAgInPc2d1	plicní
vaků	vak	k1gInPc2	vak
naplněných	naplněný	k2eAgInPc2d1	naplněný
hemolymfou	hemolymfá	k1gFnSc4	hemolymfá
s	s	k7c7	s
otvory	otvor	k1gInPc7	otvor
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
zadečku	zadeček	k1gInSc2	zadeček
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
dostává	dostávat	k5eAaImIp3nS	dostávat
vzduch	vzduch	k1gInSc1	vzduch
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
vývojově	vývojově	k6eAd1	vývojově
starší	starý	k2eAgInPc4d2	starší
druhy	druh	k1gInPc4	druh
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
dvouplicní	dvouplicní	k2eAgFnSc1d1	dvouplicní
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Hypochilidae	Hypochilida	k1gFnSc2	Hypochilida
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
zástupců	zástupce	k1gMnPc2	zástupce
tohoto	tento	k3xDgInSc2	tento
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
pouze	pouze	k6eAd1	pouze
přední	přední	k2eAgInSc4d1	přední
pár	pár	k1gInSc4	pár
plicních	plicní	k2eAgInPc2d1	plicní
vaků	vak	k1gInPc2	vak
a	a	k8xC	a
zadní	zadní	k2eAgInSc1d1	zadní
pár	pár	k1gInSc1	pár
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
či	či	k8xC	či
úplně	úplně	k6eAd1	úplně
přetvořen	přetvořen	k2eAgInSc1d1	přetvořen
ve	v	k7c4	v
vzdušnice	vzdušnice	k1gFnPc4	vzdušnice
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
kyslík	kyslík	k1gInSc1	kyslík
přenášen	přenášen	k2eAgInSc1d1	přenášen
do	do	k7c2	do
hemolymfy	hemolymf	k1gInPc4	hemolymf
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Dýchací	dýchací	k2eAgFnSc1d1	dýchací
soustava	soustava	k1gFnSc1	soustava
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
vzdušnicích	vzdušnice	k1gFnPc6	vzdušnice
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
u	u	k7c2	u
předků	předek	k1gInPc2	předek
menšího	malý	k2eAgInSc2d2	menší
vzrůstu	vzrůst	k1gInSc2	vzrůst
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
odolávání	odolávání	k1gNnSc4	odolávání
desikaci	desikace	k1gFnSc4	desikace
(	(	kIx(	(
<g/>
vysoušení	vysoušení	k1gNnSc2	vysoušení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzdušnice	vzdušnice	k1gFnPc1	vzdušnice
byly	být	k5eAaImAgFnP	být
původně	původně	k6eAd1	původně
propojeny	propojen	k2eAgInPc1d1	propojen
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
skrze	skrze	k?	skrze
pár	pár	k4xCyI	pár
otvorů	otvor	k1gInPc2	otvor
zvaných	zvaný	k2eAgInPc2d1	zvaný
spiracula	spiracula	k1gFnSc1	spiracula
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
pavouků	pavouk	k1gMnPc2	pavouk
tento	tento	k3xDgInSc1	tento
pár	pár	k1gInSc1	pár
splynul	splynout	k5eAaPmAgInS	splynout
v	v	k7c4	v
jedno	jeden	k4xCgNnSc4	jeden
spiraculum	spiraculum	k1gNnSc4	spiraculum
umístěné	umístěný	k2eAgNnSc4d1	umístěné
uprostřed	uprostřed	k6eAd1	uprostřed
a	a	k8xC	a
posunul	posunout	k5eAaPmAgMnS	posunout
se	se	k3xPyFc4	se
dozadu	dozadu	k6eAd1	dozadu
ke	k	k7c3	k
snovacím	snovací	k2eAgFnPc3d1	snovací
bradavkám	bradavka	k1gFnPc3	bradavka
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
se	s	k7c7	s
vzdušnicemi	vzdušnice	k1gFnPc7	vzdušnice
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
vyšší	vysoký	k2eAgInSc4d2	vyšší
metabolický	metabolický	k2eAgInSc4d1	metabolický
výdej	výdej	k1gInSc4	výdej
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
udržují	udržovat	k5eAaImIp3nP	udržovat
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
klepítkatců	klepítkatec	k1gMnPc2	klepítkatec
jsou	být	k5eAaImIp3nP	být
poslední	poslední	k2eAgInPc1d1	poslední
články	článek	k1gInPc1	článek
pavoučích	pavoučí	k2eAgFnPc2d1	pavoučí
chelicer	chelicra	k1gFnPc2	chelicra
přetvořeny	přetvořen	k2eAgInPc4d1	přetvořen
v	v	k7c4	v
drápky	drápek	k1gInPc4	drápek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
pavouků	pavouk	k1gMnPc2	pavouk
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
vstřikování	vstřikování	k1gNnSc3	vstřikování
jedu	jed	k1gInSc2	jed
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Jed	jed	k1gInSc1	jed
je	být	k5eAaImIp3nS	být
vytvářen	vytvářit	k5eAaPmNgInS	vytvářit
jedovými	jedový	k2eAgFnPc7d1	jedová
žlázami	žláza	k1gFnPc7	žláza
v	v	k7c6	v
kořenech	kořen	k1gInPc6	kořen
chelicer	chelicra	k1gFnPc2	chelicra
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
pakřižákovití	pakřižákovitý	k2eAgMnPc1d1	pakřižákovitý
(	(	kIx(	(
<g/>
Uloboridae	Uloboridae	k1gNnSc3	Uloboridae
<g/>
)	)	kIx)	)
o	o	k7c4	o
jedové	jedový	k2eAgFnPc4d1	jedová
žlázy	žláza	k1gFnPc4	žláza
přišli	přijít	k5eAaPmAgMnP	přijít
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
usmrcují	usmrcovat	k5eAaImIp3nP	usmrcovat
pavoučími	pavoučí	k2eAgNnPc7d1	pavoučí
vlákny	vlákno	k1gNnPc7	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
pavoukovců	pavoukovec	k1gMnPc2	pavoukovec
mají	mít	k5eAaImIp3nP	mít
pavouci	pavouk	k1gMnPc1	pavouk
úzká	úzký	k2eAgNnPc4d1	úzké
střeva	střevo	k1gNnPc4	střevo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
přijímat	přijímat	k5eAaImF	přijímat
pouze	pouze	k6eAd1	pouze
potravu	potrava	k1gFnSc4	potrava
v	v	k7c6	v
tekutém	tekutý	k2eAgInSc6d1	tekutý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
filtrování	filtrování	k1gNnSc3	filtrování
pevné	pevný	k2eAgFnSc2d1	pevná
stravy	strava	k1gFnSc2	strava
slouží	sloužit	k5eAaImIp3nP	sloužit
dvě	dva	k4xCgFnPc1	dva
sady	sada	k1gFnPc1	sada
filtrů	filtr	k1gInPc2	filtr
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
používají	používat	k5eAaImIp3nP	používat
dva	dva	k4xCgInPc4	dva
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
externího	externí	k2eAgNnSc2d1	externí
trávení	trávení	k1gNnSc2	trávení
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
napouštějí	napouštět	k5eAaImIp3nP	napouštět
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
trávicími	trávicí	k2eAgInPc7d1	trávicí
enzymy	enzym	k1gInPc7	enzym
a	a	k8xC	a
poté	poté	k6eAd1	poté
vysávají	vysávat	k5eAaImIp3nP	vysávat
její	její	k3xOp3gFnPc4	její
rozpuštěné	rozpuštěný	k2eAgFnPc4d1	rozpuštěná
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
,	,	kIx,	,
až	až	k9	až
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
pouze	pouze	k6eAd1	pouze
prázdná	prázdný	k2eAgFnSc1d1	prázdná
slupka	slupka	k1gFnSc1	slupka
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
drtí	drtit	k5eAaImIp3nP	drtit
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
na	na	k7c4	na
kaši	kaše	k1gFnSc4	kaše
pomocí	pomocí	k7c2	pomocí
chelicer	chelicra	k1gFnPc2	chelicra
a	a	k8xC	a
makadel	makadlo	k1gNnPc2	makadlo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ji	on	k3xPp3gFnSc4	on
napouštějí	napouštět	k5eAaImIp3nP	napouštět
enzymy	enzym	k1gInPc1	enzym
<g/>
;	;	kIx,	;
u	u	k7c2	u
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
chelicery	chelicera	k1gFnSc2	chelicera
a	a	k8xC	a
makadla	makadlo	k1gNnSc2	makadlo
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
dutinu	dutina	k1gFnSc4	dutina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
drží	držet	k5eAaImIp3nS	držet
zpracovávanou	zpracovávaný	k2eAgFnSc4d1	zpracovávaná
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Žaludek	žaludek	k1gInSc1	žaludek
v	v	k7c6	v
hlavohrudi	hlavohruď	k1gFnSc6	hlavohruď
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
čerpadlo	čerpadlo	k1gNnSc4	čerpadlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
posouvá	posouvat	k5eAaImIp3nS	posouvat
potravu	potrava	k1gFnSc4	potrava
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
trávicí	trávicí	k2eAgFnSc2d1	trávicí
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
mají	mít	k5eAaImIp3nP	mít
několik	několik	k4yIc4	několik
slepých	slepý	k2eAgNnPc2d1	slepé
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
vstřebávají	vstřebávat	k5eAaImIp3nP	vstřebávat
živiny	živina	k1gFnPc1	živina
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
zadečku	zadeček	k1gInSc6	zadeček
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
největší	veliký	k2eAgFnSc7d3	veliký
prostor	prostora	k1gFnPc2	prostora
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
právě	právě	k9	právě
trávicí	trávicí	k2eAgFnSc1d1	trávicí
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
několik	několik	k4yIc1	několik
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
hlavohrudi	hlavohruď	k1gFnSc6	hlavohruď
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pavouků	pavouk	k1gMnPc2	pavouk
přetváří	přetvářet	k5eAaImIp3nS	přetvářet
dusíkaté	dusíkatý	k2eAgFnPc4d1	dusíkatá
odpadní	odpadní	k2eAgFnPc4d1	odpadní
látky	látka	k1gFnPc4	látka
v	v	k7c4	v
kyselinu	kyselina	k1gFnSc4	kyselina
močovou	močový	k2eAgFnSc4d1	močová
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pak	pak	k6eAd1	pak
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
v	v	k7c6	v
suchém	suchý	k2eAgInSc6d1	suchý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Malpighické	Malpighický	k2eAgFnSc2d1	Malpighický
trubice	trubice	k1gFnSc2	trubice
tyto	tento	k3xDgFnPc1	tento
odpadní	odpadní	k2eAgFnPc1d1	odpadní
látky	látka	k1gFnPc1	látka
extrahují	extrahovat	k5eAaBmIp3nP	extrahovat
z	z	k7c2	z
hemolymfy	hemolymf	k1gMnPc7	hemolymf
v	v	k7c6	v
mixocoelu	mixocoel	k1gInSc6	mixocoel
a	a	k8xC	a
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
kloaky	kloaka	k1gFnSc2	kloaka
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jsou	být	k5eAaImIp3nP	být
vyloučeny	vyloučit	k5eAaPmNgFnP	vyloučit
řitním	řitní	k2eAgInSc7d1	řitní
otvorem	otvor	k1gInSc7	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Vytváření	vytváření	k1gNnSc1	vytváření
kyseliny	kyselina	k1gFnSc2	kyselina
močové	močový	k2eAgFnSc2d1	močová
a	a	k8xC	a
její	její	k3xOp3gNnPc4	její
vylučování	vylučování	k1gNnPc4	vylučování
přes	přes	k7c4	přes
Malpighické	Malpighický	k2eAgFnPc4d1	Malpighický
trubice	trubice	k1gFnPc4	trubice
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
udržování	udržování	k1gNnSc3	udržování
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
nezávisle	závisle	k6eNd1	závisle
u	u	k7c2	u
několika	několik	k4yIc2	několik
linií	linie	k1gFnPc2	linie
členovců	členovec	k1gMnPc2	členovec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
žít	žít	k5eAaImF	žít
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
vody	voda	k1gFnSc2	voda
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
trubice	trubice	k1gFnPc4	trubice
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
pavoukovců	pavoukovec	k1gInPc2	pavoukovec
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
naprosto	naprosto	k6eAd1	naprosto
odlišných	odlišný	k2eAgFnPc2d1	odlišná
částí	část	k1gFnPc2	část
embrya	embryo	k1gNnSc2	embryo
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
primitivní	primitivní	k2eAgMnPc1d1	primitivní
pavouci	pavouk	k1gMnPc1	pavouk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
druhy	druh	k1gInPc4	druh
podřádů	podřád	k1gInPc2	podřád
sklípkoši	sklípkoš	k1gMnPc1	sklípkoš
a	a	k8xC	a
sklípkani	sklípkan	k1gMnPc1	sklípkan
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
ale	ale	k9	ale
zachovali	zachovat	k5eAaPmAgMnP	zachovat
nefridie	nefridie	k1gFnPc4	nefridie
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
malé	malý	k2eAgFnPc4d1	malá
ledviny	ledvina	k1gFnPc4	ledvina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
spotřebují	spotřebovat	k5eAaPmIp3nP	spotřebovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
k	k	k7c3	k
vylučování	vylučování	k1gNnSc3	vylučování
dusíkatých	dusíkatý	k2eAgFnPc2d1	dusíkatá
odpadních	odpadní	k2eAgFnPc2d1	odpadní
látek	látka	k1gFnPc2	látka
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
centrální	centrální	k2eAgFnSc1d1	centrální
nervová	nervový	k2eAgFnSc1d1	nervová
soustava	soustava	k1gFnSc1	soustava
členovců	členovec	k1gMnPc2	členovec
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
páru	pár	k1gInSc2	pár
nervových	nervový	k2eAgFnPc2d1	nervová
trubic	trubice	k1gFnPc2	trubice
podbíhajících	podbíhající	k2eAgFnPc2d1	podbíhající
pod	pod	k7c7	pod
střevem	střevo	k1gNnSc7	střevo
a	a	k8xC	a
párových	párový	k2eAgFnPc2d1	párová
ganglií	ganglie	k1gFnPc2	ganglie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
místní	místní	k2eAgNnSc1d1	místní
řídící	řídící	k2eAgNnSc1d1	řídící
centra	centrum	k1gNnPc1	centrum
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
článcích	článek	k1gInPc6	článek
<g/>
;	;	kIx,	;
a	a	k8xC	a
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
splynutím	splynutí	k1gNnSc7	splynutí
ganglií	ganglie	k1gFnPc2	ganglie
hlavových	hlavový	k2eAgInPc2d1	hlavový
článků	článek	k1gInPc2	článek
nacházejících	nacházející	k2eAgInPc2d1	nacházející
se	se	k3xPyFc4	se
před	před	k7c7	před
a	a	k8xC	a
za	za	k7c7	za
ústy	ústa	k1gNnPc7	ústa
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tento	tento	k3xDgInSc1	tento
shluk	shluk	k1gInSc1	shluk
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
jícen	jícen	k1gInSc1	jícen
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
primitivních	primitivní	k2eAgMnPc2d1	primitivní
sklípkošů	sklípkoš	k1gMnPc2	sklípkoš
mají	mít	k5eAaImIp3nP	mít
pavouci	pavouk	k1gMnPc1	pavouk
nerovovou	rovový	k2eNgFnSc4d1	nerovová
soustavu	soustava	k1gFnSc4	soustava
více	hodně	k6eAd2	hodně
centralizovanou	centralizovaný	k2eAgFnSc4d1	centralizovaná
<g/>
:	:	kIx,	:
všechna	všechen	k3xTgNnPc4	všechen
ganglia	ganglion	k1gNnPc4	ganglion
za	za	k7c7	za
jícnem	jícen	k1gInSc7	jícen
splynula	splynout	k5eAaPmAgFnS	splynout
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
hlavohrudi	hlavohruď	k1gFnSc6	hlavohruď
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nervových	nervový	k2eAgFnPc2d1	nervová
tkání	tkáň	k1gFnPc2	tkáň
a	a	k8xC	a
v	v	k7c6	v
zadečku	zadeček	k1gInSc6	zadeček
nejsou	být	k5eNaImIp3nP	být
žádná	žádný	k3yNgNnPc4	žádný
ganglia	ganglion	k1gNnPc4	ganglion
<g/>
;	;	kIx,	;
u	u	k7c2	u
sklípkošů	sklípkoš	k1gMnPc2	sklípkoš
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
ganglia	ganglion	k1gNnPc4	ganglion
zadečku	zadeček	k1gInSc2	zadeček
a	a	k8xC	a
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
hlavohrudi	hlavohruď	k1gFnSc2	hlavohruď
oddělena	oddělit	k5eAaPmNgNnP	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pavouků	pavouk	k1gMnPc2	pavouk
má	mít	k5eAaImIp3nS	mít
vepředu	vepředu	k6eAd1	vepředu
nahoře	nahoře	k6eAd1	nahoře
na	na	k7c6	na
hlavohrudi	hlavohruď	k1gFnSc6	hlavohruď
čtyři	čtyři	k4xCgFnPc4	čtyři
páry	pára	k1gFnPc1	pára
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
rozmístění	rozmístění	k1gNnSc1	rozmístění
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
čeleď	čeleď	k1gFnSc1	čeleď
od	od	k7c2	od
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Čelní	čelní	k2eAgInSc1d1	čelní
pár	pár	k1gInSc1	pár
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
z	z	k7c2	z
takového	takový	k3xDgInSc2	takový
druhu	druh	k1gInSc2	druh
jednoduchých	jednoduchý	k2eAgNnPc2d1	jednoduché
očí	oko	k1gNnPc2	oko
(	(	kIx(	(
<g/>
očí	oko	k1gNnPc2	oko
s	s	k7c7	s
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
čočkou	čočka	k1gFnSc7	čočka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
členovců	členovec	k1gMnPc2	členovec
schopny	schopen	k2eAgFnPc1d1	schopna
rozeznávat	rozeznávat	k5eAaImF	rozeznávat
pouze	pouze	k6eAd1	pouze
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
dopadává	dopadávat	k5eAaImIp3nS	dopadávat
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pavouků	pavouk	k1gMnPc2	pavouk
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
oči	oko	k1gNnPc1	oko
ale	ale	k8xC	ale
schopny	schopen	k2eAgFnPc1d1	schopna
vytvářet	vytvářet	k5eAaImF	vytvářet
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgNnPc2d1	ostatní
očí	oko	k1gNnPc2	oko
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
ze	z	k7c2	z
složených	složený	k2eAgNnPc2d1	složené
očí	oko	k1gNnPc2	oko
klepítkatců	klepítkatec	k1gMnPc2	klepítkatec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
se	se	k3xPyFc4	se
neskládají	skládat	k5eNaImIp3nP	skládat
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
oddělených	oddělený	k2eAgNnPc2d1	oddělené
oček	očko	k1gNnPc2	očko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
složené	složený	k2eAgNnSc4d1	složené
oči	oko	k1gNnPc4	oko
typická	typický	k2eAgFnSc1d1	typická
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
hlavních	hlavní	k2eAgNnPc2d1	hlavní
očí	oko	k1gNnPc2	oko
tyto	tento	k3xDgFnPc4	tento
vedlejší	vedlejší	k2eAgNnPc4d1	vedlejší
oči	oko	k1gNnPc4	oko
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
pavouků	pavouk	k1gMnPc2	pavouk
vnímají	vnímat	k5eAaImIp3nP	vnímat
světlo	světlo	k1gNnSc4	světlo
odražené	odražený	k2eAgNnSc4d1	odražené
od	od	k7c2	od
tapeta	tapeta	k1gFnSc1	tapeta
lucida	lucida	k1gFnSc1	lucida
a	a	k8xC	a
slíďákovité	slíďákovitý	k2eAgInPc1d1	slíďákovitý
lze	lze	k6eAd1	lze
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
rozeznat	rozeznat	k5eAaPmF	rozeznat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
oči	oko	k1gNnPc1	oko
"	"	kIx"	"
<g/>
svítí	svítit	k5eAaImIp3nP	svítit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
například	například	k6eAd1	například
vedlejší	vedlejší	k2eAgNnPc4d1	vedlejší
oči	oko	k1gNnPc4	oko
skákavkovitých	skákavkovitý	k2eAgFnPc2d1	skákavkovitý
tapeta	tapeta	k1gFnSc1	tapeta
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Zraková	zrakový	k2eAgFnSc1d1	zraková
ostrost	ostrost	k1gFnSc1	ostrost
některých	některý	k3yIgFnPc2	některý
skákavkovitých	skákavkovitý	k2eAgFnPc2d1	skákavkovitý
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
až	až	k6eAd1	až
desetinásobně	desetinásobně	k6eAd1	desetinásobně
zrakovou	zrakový	k2eAgFnSc4d1	zraková
ostrost	ostrost	k1gFnSc4	ostrost
šídel	šídlo	k1gNnPc2	šídlo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
mezi	mezi	k7c7	mezi
hmyzem	hmyz	k1gInSc7	hmyz
zdaleka	zdaleka	k6eAd1	zdaleka
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
zrak	zrak	k1gInSc4	zrak
<g/>
;	;	kIx,	;
lidé	člověk	k1gMnPc1	člověk
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
pětkrát	pětkrát	k6eAd1	pětkrát
ostřejší	ostrý	k2eAgInSc4d2	ostřejší
zrak	zrak	k1gInSc4	zrak
než	než	k8xS	než
skákavkovití	skákavkovitý	k2eAgMnPc1d1	skákavkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Dosahují	dosahovat	k5eAaImIp3nP	dosahovat
toho	ten	k3xDgNnSc2	ten
pomocí	pomocí	k7c2	pomocí
čoček	čočka	k1gFnPc2	čočka
poskládaných	poskládaný	k2eAgFnPc2d1	poskládaná
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
teleobjektivu	teleobjektiv	k1gInSc2	teleobjektiv
<g/>
,	,	kIx,	,
čtyřvrstvé	čtyřvrstvý	k2eAgFnSc2d1	čtyřvrstvá
sítnice	sítnice	k1gFnSc2	sítnice
a	a	k8xC	a
schopnosti	schopnost	k1gFnSc2	schopnost
otáčet	otáčet	k5eAaImF	otáčet
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
sjednotit	sjednotit	k5eAaPmF	sjednotit
obrazy	obraz	k1gInPc4	obraz
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
fází	fáze	k1gFnPc2	fáze
snímání	snímání	k1gNnSc4	snímání
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
procesu	proces	k1gInSc2	proces
sjednocování	sjednocování	k1gNnSc2	sjednocování
obrazů	obraz	k1gInPc2	obraz
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
pomalost	pomalost	k1gFnSc1	pomalost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pavouci	pavouk	k1gMnPc1	pavouk
mají	mít	k5eAaImIp3nP	mít
menší	malý	k2eAgInSc4d2	menší
počet	počet	k1gInSc4	počet
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
šest	šest	k4xCc1	šest
(	(	kIx(	(
<g/>
např.	např.	kA	např.
čeleď	čeleď	k1gFnSc4	čeleď
šestiočkovití	šestiočkovitý	k2eAgMnPc1d1	šestiočkovitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
jen	jen	k9	jen
čtyři	čtyři	k4xCgFnPc1	čtyři
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgFnPc1	dva
<g/>
.	.	kIx.	.
</s>
<s>
Jeskynní	jeskynní	k2eAgInPc1d1	jeskynní
druhy	druh	k1gInPc1	druh
mívají	mívat	k5eAaImIp3nP	mívat
oči	oko	k1gNnPc4	oko
zakrnělé	zakrnělý	k2eAgFnPc1d1	zakrnělá
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
členovců	členovec	k1gMnPc2	členovec
by	by	kYmCp3nS	by
pavoučí	pavoučí	k2eAgFnSc1d1	pavoučí
kutikula	kutikula	k1gFnSc1	kutikula
normálně	normálně	k6eAd1	normálně
bránila	bránit	k5eAaImAgFnS	bránit
průniku	průnik	k1gInSc2	průnik
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
členovci	členovec	k1gMnPc1	členovec
ji	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
prostoupenou	prostoupený	k2eAgFnSc7d1	prostoupená
důmyslnou	důmyslný	k2eAgFnSc7d1	důmyslná
sadou	sada	k1gFnSc7	sada
senzorů	senzor	k1gInPc2	senzor
<g/>
.	.	kIx.	.
</s>
<s>
Různá	různý	k2eAgNnPc1d1	různé
dotyková	dotykový	k2eAgNnPc1d1	dotykové
čidla	čidlo	k1gNnPc1	čidlo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
štětiny	štětina	k1gFnPc1	štětina
<g/>
,	,	kIx,	,
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
síly	síla	k1gFnPc4	síla
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
od	od	k7c2	od
silného	silný	k2eAgInSc2d1	silný
dotyku	dotyk	k1gInSc2	dotyk
po	po	k7c4	po
slabé	slabý	k2eAgInPc4d1	slabý
závany	závan	k1gInPc4	závan
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Chemická	chemický	k2eAgNnPc1d1	chemické
čidla	čidlo	k1gNnPc1	čidlo
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
čichu	čich	k1gInSc2	čich
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
skrze	skrze	k?	skrze
štětiny	štětina	k1gFnSc2	štětina
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
v	v	k7c6	v
kloubech	kloub	k1gInPc6	kloub
končetin	končetina	k1gFnPc2	končetina
speciální	speciální	k2eAgInPc1d1	speciální
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c4	na
vibrace	vibrace	k1gFnPc4	vibrace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pavučiny	pavučina	k1gFnPc4	pavučina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
mechanické	mechanický	k2eAgInPc1d1	mechanický
a	a	k8xC	a
chemické	chemický	k2eAgInPc1d1	chemický
senzory	senzor	k1gInPc1	senzor
důležitější	důležitý	k2eAgInPc1d2	Důležitější
než	než	k8xS	než
oči	oko	k1gNnPc1	oko
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
smyslovým	smyslový	k2eAgInSc7d1	smyslový
orgánem	orgán	k1gInSc7	orgán
pro	pro	k7c4	pro
aktivně	aktivně	k6eAd1	aktivně
lovící	lovící	k2eAgMnPc4d1	lovící
pavouky	pavouk	k1gMnPc4	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
členovců	členovec	k1gMnPc2	členovec
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
čidla	čidlo	k1gNnPc1	čidlo
pro	pro	k7c4	pro
vnímání	vnímání	k1gNnSc4	vnímání
stability	stabilita	k1gFnSc2	stabilita
a	a	k8xC	a
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nahoře	nahoře	k6eAd1	nahoře
a	a	k8xC	a
kde	kde	k6eAd1	kde
dole	dole	k6eAd1	dole
<g/>
,	,	kIx,	,
rozpoznávají	rozpoznávat	k5eAaImIp3nP	rozpoznávat
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
Proprioreceptory	Proprioreceptor	k1gInPc1	Proprioreceptor
členovců	členovec	k1gMnPc2	členovec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vnímají	vnímat	k5eAaImIp3nP	vnímat
napětí	napětí	k1gNnSc4	napětí
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
ohyb	ohyb	k1gInSc4	ohyb
kloubů	kloub	k1gInPc2	kloub
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
důkladně	důkladně	k6eAd1	důkladně
prozkoumány	prozkoumat	k5eAaPmNgInP	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
příliš	příliš	k6eAd1	příliš
neví	vědět	k5eNaImIp3nS	vědět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgInPc4	jaký
další	další	k2eAgInPc4d1	další
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
smyslové	smyslový	k2eAgInPc4d1	smyslový
orgány	orgán	k1gInPc4	orgán
pavouci	pavouk	k1gMnPc1	pavouk
a	a	k8xC	a
členovci	členovec	k1gMnPc1	členovec
obecně	obecně	k6eAd1	obecně
mají	mít	k5eAaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
osmi	osm	k4xCc2	osm
nohou	noha	k1gFnPc2	noha
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
tělu	tělo	k1gNnSc3	tělo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kyčel	kyčel	k1gFnSc1	kyčel
(	(	kIx(	(
<g/>
coxa	coxa	k1gFnSc1	coxa
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
další	další	k2eAgFnSc7d1	další
částí	část	k1gFnSc7	část
je	být	k5eAaImIp3nS	být
příkyčlí	příkyčlí	k1gNnSc1	příkyčlí
(	(	kIx(	(
<g/>
trochanter	trochanter	k1gInSc1	trochanter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
pant	pant	k1gInSc1	pant
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
stehno	stehno	k1gNnSc4	stehno
(	(	kIx(	(
<g/>
femur	femur	k1gMnSc1	femur
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
následuje	následovat	k5eAaImIp3nS	následovat
koleno	koleno	k1gNnSc1	koleno
(	(	kIx(	(
<g/>
patella	patella	k1gFnSc1	patella
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
pant	pant	k1gInSc4	pant
pro	pro	k7c4	pro
holeň	holeň	k1gFnSc4	holeň
(	(	kIx(	(
<g/>
tibia	tibia	k1gFnSc1	tibia
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
další	další	k2eAgInSc1d1	další
je	být	k5eAaImIp3nS	být
nárt	nárt	k1gInSc1	nárt
(	(	kIx(	(
<g/>
metatarsus	metatarsus	k1gInSc1	metatarsus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
holeň	holeň	k1gFnSc4	holeň
s	s	k7c7	s
chodidlem	chodidlo	k1gNnSc7	chodidlo
(	(	kIx(	(
<g/>
tarsus	tarsus	k1gMnSc1	tarsus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
chodidlo	chodidlo	k1gNnSc1	chodidlo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
čeledi	čeleď	k1gFnSc6	čeleď
zakončeno	zakončen	k2eAgNnSc1d1	zakončeno
dvěma	dva	k4xCgFnPc7	dva
nebo	nebo	k8xC	nebo
třemi	tři	k4xCgInPc7	tři
drápky	drápek	k1gInPc7	drápek
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
členovci	členovec	k1gMnPc1	členovec
ohýbají	ohýbat	k5eAaImIp3nP	ohýbat
své	svůj	k3xOyFgFnPc4	svůj
končetiny	končetina	k1gFnPc4	končetina
pomocí	pomocí	k7c2	pomocí
svalů	sval	k1gInPc2	sval
připevněných	připevněný	k2eAgInPc2d1	připevněný
na	na	k7c4	na
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
stranu	strana	k1gFnSc4	strana
vnější	vnější	k2eAgFnSc2d1	vnější
kostry	kostra	k1gFnSc2	kostra
<g/>
;	;	kIx,	;
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
natahování	natahování	k1gNnSc3	natahování
využívají	využívat	k5eAaImIp3nP	využívat
pavouci	pavouk	k1gMnPc1	pavouk
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
skupin	skupina	k1gFnPc2	skupina
hydraulický	hydraulický	k2eAgInSc4d1	hydraulický
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
zděděný	zděděný	k2eAgInSc4d1	zděděný
po	po	k7c6	po
předcích	předek	k1gInPc6	předek
členovců	členovec	k1gMnPc2	členovec
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgInPc1d1	jediný
extenzory	extenzor	k1gInPc1	extenzor
(	(	kIx(	(
<g/>
natahovače	natahovač	k1gInPc1	natahovač
<g/>
)	)	kIx)	)
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
pavouků	pavouk	k1gMnPc2	pavouk
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
kyčelních	kyčelní	k2eAgInPc6d1	kyčelní
kloubech	kloub	k1gInPc6	kloub
(	(	kIx(	(
<g/>
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
kyčlí	kyčel	k1gFnSc7	kyčel
a	a	k8xC	a
příkyčlí	příkyčle	k1gFnSc7	příkyčle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
pavouk	pavouk	k1gMnSc1	pavouk
s	s	k7c7	s
probodnutou	probodnutý	k2eAgFnSc7d1	probodnutá
hlavohrudí	hlavohruď	k1gFnSc7	hlavohruď
nemůže	moct	k5eNaImIp3nS	moct
natahovat	natahovat	k5eAaImF	natahovat
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
nohy	noha	k1gFnPc4	noha
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
pavouků	pavouk	k1gMnPc2	pavouk
se	se	k3xPyFc4	se
kroutí	kroutit	k5eAaImIp3nS	kroutit
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
natažení	natažení	k1gNnSc2	natažení
nohou	noha	k1gFnPc2	noha
vyvinout	vyvinout	k5eAaPmF	vyvinout
tlak	tlak	k1gInSc4	tlak
až	až	k9	až
osmkrát	osmkrát	k6eAd1	osmkrát
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
klidovém	klidový	k2eAgInSc6d1	klidový
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
skákavkovití	skákavkovitý	k2eAgMnPc1d1	skákavkovitý
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vyskočit	vyskočit	k5eAaPmF	vyskočit
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
až	až	k8xS	až
padesátinásobku	padesátinásobek	k1gInSc2	padesátinásobek
délky	délka	k1gFnSc2	délka
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
ve	v	k7c6	v
třetím	třetí	k4xOgMnSc6	třetí
či	či	k8xC	či
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
páru	pár	k1gInSc6	pár
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
větší	veliký	k2eAgMnPc1d2	veliký
pavouci	pavouk	k1gMnPc1	pavouk
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
natahování	natahování	k1gNnSc3	natahování
nohou	nohý	k2eAgFnSc4d1	nohá
hydrauliku	hydraulika	k1gFnSc4	hydraulika
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
skákavkovitých	skákavkovitý	k2eAgFnPc2d1	skákavkovitý
ke	k	k7c3	k
skokům	skok	k1gInPc3	skok
využívají	využívat	k5eAaImIp3nP	využívat
flexory	flexor	k1gInPc4	flexor
(	(	kIx(	(
<g/>
ohybače	ohybač	k1gInPc4	ohybač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
aktivně	aktivně	k6eAd1	aktivně
lovících	lovící	k2eAgMnPc2d1	lovící
pavouků	pavouk	k1gMnPc2	pavouk
má	mít	k5eAaImIp3nS	mít
mezi	mezi	k7c4	mezi
drápky	drápek	k1gInPc4	drápek
na	na	k7c6	na
konci	konec	k1gInSc6	konec
nohou	noha	k1gFnPc2	noha
husté	hustý	k2eAgInPc1d1	hustý
chomáče	chomáč	k1gInPc1	chomáč
chloupků	chloupek	k1gInPc2	chloupek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
chomáče	chomáč	k1gInPc1	chomáč
neboli	neboli	k8xC	neboli
skopuly	skopula	k1gFnPc1	skopula
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
chloupky	chloupek	k1gInPc4	chloupek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
konce	konec	k1gInPc1	konec
jsou	být	k5eAaImIp3nP	být
rozvětveny	rozvětven	k2eAgInPc1d1	rozvětven
až	až	k9	až
na	na	k7c4	na
tisíce	tisíc	k4xCgInPc4	tisíc
konečků	koneček	k1gInPc2	koneček
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
pavoukům	pavouk	k1gMnPc3	pavouk
lézt	lézt	k5eAaImF	lézt
po	po	k7c6	po
vertikálně	vertikálně	k6eAd1	vertikálně
položeném	položený	k2eAgNnSc6d1	položené
skle	sklo	k1gNnSc6	sklo
a	a	k8xC	a
vzhůru	vzhůru	k6eAd1	vzhůru
nohama	noha	k1gFnPc7	noha
po	po	k7c6	po
stropě	strop	k1gInSc6	strop
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
přilnavost	přilnavost	k1gFnSc1	přilnavost
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
umožněna	umožnit	k5eAaPmNgFnS	umožnit
díky	díky	k7c3	díky
kontaktu	kontakt	k1gInSc3	kontakt
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
tenkými	tenký	k2eAgFnPc7d1	tenká
vrstvami	vrstva	k1gFnPc7	vrstva
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
površích	povrch	k1gInPc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
či	či	k8xC	či
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
nechávají	nechávat	k5eAaImIp3nP	nechávat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nejméně	málo	k6eAd3	málo
čtyři	čtyři	k4xCgFnPc1	čtyři
nohy	noha	k1gFnPc1	noha
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
pavoukovci	pavoukovec	k1gMnPc1	pavoukovec
<g/>
.	.	kIx.	.
</s>
<s>
Zadeček	zadeček	k1gInSc1	zadeček
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgFnPc4	žádný
končetiny	končetina	k1gFnPc4	končetina
kromě	kromě	k7c2	kromě
těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
krátké	krátká	k1gFnPc1	krátká
pohyblivé	pohyblivý	k2eAgFnSc2d1	pohyblivá
snovací	snovací	k2eAgFnPc4d1	snovací
bradavky	bradavka	k1gFnSc2	bradavka
produkující	produkující	k2eAgFnSc1d1	produkující
pavučinová	pavučinový	k2eAgFnSc1d1	pavučinová
vlákna	vlákna	k1gFnSc1	vlákna
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
jich	on	k3xPp3gFnPc2	on
jeden	jeden	k4xCgMnSc1	jeden
až	až	k9	až
čtyři	čtyři	k4xCgInPc1	čtyři
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejčastěji	často	k6eAd3	často
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
tři	tři	k4xCgInPc1	tři
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
snovací	snovací	k2eAgFnSc1d1	snovací
bradavka	bradavka	k1gFnSc1	bradavka
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
vývodů	vývod	k1gInPc2	vývod
(	(	kIx(	(
<g/>
spigotů	spigot	k1gInPc2	spigot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
každý	každý	k3xTgInSc1	každý
je	být	k5eAaImIp3nS	být
propojen	propojit	k5eAaPmNgInS	propojit
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
žlázou	žláza	k1gFnSc7	žláza
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
alespoň	alespoň	k9	alespoň
šest	šest	k4xCc4	šest
druhů	druh	k1gInPc2	druh
snovacích	snovací	k2eAgFnPc2d1	snovací
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
jiný	jiný	k2eAgInSc4d1	jiný
druh	druh	k1gInSc4	druh
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Sklípkani	sklípkan	k1gMnPc1	sklípkan
mají	mít	k5eAaImIp3nP	mít
snovací	snovací	k2eAgFnPc4d1	snovací
žlázy	žláza	k1gFnPc4	žláza
také	také	k9	také
v	v	k7c6	v
nohou	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Pavučinové	pavučinový	k2eAgNnSc1d1	pavučinové
vlákno	vlákno	k1gNnSc1	vlákno
je	být	k5eAaImIp3nS	být
složeno	složen	k2eAgNnSc1d1	složeno
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
bílkoviny	bílkovina	k1gFnSc2	bílkovina
podobné	podobný	k2eAgFnSc2d1	podobná
té	ten	k3xDgFnSc2	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
hmyzích	hmyzí	k2eAgNnPc6d1	hmyzí
hedvábných	hedvábný	k2eAgNnPc6d1	hedvábné
vláknech	vlákno	k1gNnPc6	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kapalinu	kapalina	k1gFnSc4	kapalina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tvrdne	tvrdnout	k5eAaImIp3nS	tvrdnout
ne	ne	k9	ne
kvůli	kvůli	k7c3	kvůli
kontaktu	kontakt	k1gInSc3	kontakt
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
proces	proces	k1gInSc1	proces
vylučování	vylučování	k1gNnSc2	vylučování
mění	měnit	k5eAaImIp3nS	měnit
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
bílkoviny	bílkovina	k1gFnSc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
pevnost	pevnost	k1gFnSc4	pevnost
v	v	k7c6	v
tahu	tah	k1gInSc6	tah
jako	jako	k8xC	jako
nylon	nylon	k1gInSc1	nylon
a	a	k8xC	a
přírodní	přírodní	k2eAgInPc1d1	přírodní
materiály	materiál	k1gInPc1	materiál
jako	jako	k8xS	jako
chitin	chitin	k1gInSc1	chitin
<g/>
,	,	kIx,	,
kolagen	kolagen	k1gInSc1	kolagen
a	a	k8xC	a
celulóza	celulóza	k1gFnSc1	celulóza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
elastičtější	elastický	k2eAgNnSc1d2	elastičtější
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
více	hodně	k6eAd2	hodně
natahovat	natahovat	k5eAaImF	natahovat
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
přetrhlo	přetrhnout	k5eAaPmAgNnS	přetrhnout
či	či	k8xC	či
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pavouci	pavouk	k1gMnPc1	pavouk
mají	mít	k5eAaImIp3nP	mít
sítko	sítko	k1gNnSc4	sítko
(	(	kIx(	(
<g/>
cribellum	cribellum	k1gInSc1	cribellum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgInSc1d1	speciální
orgán	orgán	k1gInSc1	orgán
s	s	k7c7	s
až	až	k6eAd1	až
40	[number]	k4	40
000	[number]	k4	000
spigoty	spigot	k1gInPc7	spigot
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
každý	každý	k3xTgInSc1	každý
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
jedno	jeden	k4xCgNnSc4	jeden
malé	malý	k2eAgNnSc4d1	malé
vlákno	vlákno	k1gNnSc4	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
rozčesávána	rozčesáván	k2eAgNnPc1d1	rozčesáván
hřebínkem	hřebínek	k1gInSc7	hřebínek
(	(	kIx(	(
<g/>
calamistrum	calamistrum	k1gNnSc1	calamistrum
<g/>
)	)	kIx)	)
a	a	k8xC	a
výsledné	výsledný	k2eAgNnSc1d1	výsledné
vlákno	vlákno	k1gNnSc1	vlákno
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
účinné	účinný	k2eAgNnSc1d1	účinné
při	při	k7c6	při
zachycování	zachycování	k1gNnSc6	zachycování
ochlupených	ochlupený	k2eAgFnPc2d1	ochlupená
hmyzích	hmyzí	k2eAgFnPc2d1	hmyzí
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc1d1	původní
pavouci	pavouk	k1gMnPc1	pavouk
měli	mít	k5eAaImAgMnP	mít
cribella	cribella	k6eAd1	cribella
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
produkovala	produkovat	k5eAaImAgFnS	produkovat
první	první	k4xOgFnSc1	první
vlákna	vlákna	k1gFnSc1	vlákna
schopná	schopný	k2eAgFnSc1d1	schopná
chytat	chytat	k5eAaImF	chytat
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
a	a	k8xC	a
až	až	k9	až
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
žlázy	žláza	k1gFnPc1	žláza
produkující	produkující	k2eAgFnSc2d1	produkující
lepkavé	lepkavý	k2eAgFnSc2d1	lepkavá
kapičky	kapička	k1gFnSc2	kapička
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
novodobých	novodobý	k2eAgFnPc2d1	novodobá
skupin	skupina	k1gFnPc2	skupina
pavouků	pavouk	k1gMnPc2	pavouk
již	již	k6eAd1	již
cribellum	cribellum	k1gInSc4	cribellum
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
pavučiny	pavučina	k1gFnPc4	pavučina
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaImIp3nP	využívat
pavučinová	pavučinový	k2eAgNnPc1d1	pavučinové
vlákna	vlákno	k1gNnPc1	vlákno
několika	několik	k4yIc7	několik
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
jako	jako	k8xS	jako
obal	obal	k1gInSc4	obal
pro	pro	k7c4	pro
sperma	sperma	k1gNnSc4	sperma
a	a	k8xC	a
pro	pro	k7c4	pro
oplodněná	oplodněný	k2eAgNnPc4d1	oplodněné
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
;	;	kIx,	;
jako	jako	k9	jako
"	"	kIx"	"
<g/>
záchranný	záchranný	k2eAgInSc4d1	záchranný
provaz	provaz	k1gInSc4	provaz
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
budování	budování	k1gNnSc4	budování
hnízd	hnízdo	k1gNnPc2	hnízdo
<g/>
;	;	kIx,	;
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
padák	padák	k1gInSc1	padák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
pohlavně	pohlavně	k6eAd1	pohlavně
a	a	k8xC	a
oplodnění	oplodnění	k1gNnSc1	oplodnění
probíhá	probíhat	k5eAaImIp3nS	probíhat
vnitřně	vnitřně	k6eAd1	vnitřně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepřímo	přímo	k6eNd1	přímo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
samec	samec	k1gMnSc1	samec
nevnáší	vnášet	k5eNaImIp3nS	vnášet
sperma	sperma	k1gNnSc4	sperma
do	do	k7c2	do
samičího	samičí	k2eAgNnSc2d1	samičí
těla	tělo	k1gNnSc2	tělo
svým	svůj	k3xOyFgFnPc3	svůj
pohlavním	pohlavní	k2eAgFnPc3d1	pohlavní
ústrojím	ústroj	k1gFnPc3	ústroj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
suchozemských	suchozemský	k2eAgMnPc2d1	suchozemský
členovců	členovec	k1gMnPc2	členovec
samci	samec	k1gMnPc1	samec
pavouků	pavouk	k1gMnPc2	pavouk
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
spermatofory	spermatofor	k1gInPc1	spermatofor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
malé	malý	k2eAgFnPc1d1	malá
pavučiny	pavučina	k1gFnPc1	pavučina
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yIgFnPc2	který
ejakulují	ejakulovat	k5eAaImIp3nP	ejakulovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
sperma	sperma	k1gNnSc4	sperma
přenesou	přenést	k5eAaPmIp3nP	přenést
do	do	k7c2	do
objektů	objekt	k1gInPc2	objekt
podobných	podobný	k2eAgInPc2d1	podobný
injekční	injekční	k2eAgFnSc6d1	injekční
stříkačce	stříkačka	k1gFnSc6	stříkačka
na	na	k7c6	na
konci	konec	k1gInSc6	konec
makadel	makadlo	k1gNnPc2	makadlo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
samec	samec	k1gMnSc1	samec
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
okolí	okolí	k1gNnSc6	okolí
zaznamená	zaznamenat	k5eAaPmIp3nS	zaznamenat
přítomnost	přítomnost	k1gFnSc1	přítomnost
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
samici	samice	k1gFnSc4	samice
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
připravena	připravit	k5eAaPmNgFnS	připravit
na	na	k7c6	na
páření	páření	k1gNnSc6	páření
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
u	u	k7c2	u
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pavučiny	pavučina	k1gFnPc4	pavučina
či	či	k8xC	či
"	"	kIx"	"
<g/>
záchranné	záchranný	k2eAgInPc1d1	záchranný
provazy	provaz	k1gInPc1	provaz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
rozpoznává	rozpoznávat	k5eAaImIp3nS	rozpoznávat
samec	samec	k1gMnSc1	samec
druh	druh	k1gInSc1	druh
a	a	k8xC	a
pohlaví	pohlaví	k1gNnSc1	pohlaví
pomocí	pomocí	k7c2	pomocí
"	"	kIx"	"
<g/>
čichu	čich	k1gInSc2	čich
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Malí	malý	k2eAgMnPc1d1	malý
samci	samec	k1gMnPc1	samec
často	často	k6eAd1	často
využívají	využívat	k5eAaImIp3nP	využívat
složité	složitý	k2eAgInPc4d1	složitý
namlouvací	namlouvací	k2eAgInPc4d1	namlouvací
rituály	rituál	k1gInPc4	rituál
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
velké	velký	k2eAgInPc4d1	velký
samice	samice	k1gFnPc1	samice
nesežraly	sežrat	k5eNaPmAgFnP	sežrat
před	před	k7c7	před
oplodněním	oplodnění	k1gNnSc7	oplodnění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
jsou	být	k5eAaImIp3nP	být
samci	samec	k1gMnPc1	samec
tak	tak	k6eAd1	tak
malí	malý	k2eAgMnPc1d1	malý
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
samicím	samice	k1gFnPc3	samice
stejně	stejně	k9	stejně
nevyplatilo	vyplatit	k5eNaPmAgNnS	vyplatit
je	on	k3xPp3gFnPc4	on
jíst	jíst	k5eAaImF	jíst
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pavouků	pavouk	k1gMnPc2	pavouk
vytvářejících	vytvářející	k2eAgMnPc2d1	vytvářející
pavučiny	pavučina	k1gFnSc2	pavučina
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
rituálu	rituál	k1gInSc2	rituál
důmyslný	důmyslný	k2eAgInSc1d1	důmyslný
systém	systém	k1gInSc1	systém
vibrací	vibrace	k1gFnPc2	vibrace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
aktivně	aktivně	k6eAd1	aktivně
lovících	lovící	k2eAgMnPc2d1	lovící
pavouků	pavouk	k1gMnPc2	pavouk
hrají	hrát	k5eAaImIp3nP	hrát
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
doteky	dotek	k1gInPc4	dotek
samičího	samičí	k2eAgNnSc2d1	samičí
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
samici	samice	k1gFnSc4	samice
"	"	kIx"	"
<g/>
zhypnotizovat	zhypnotizovat	k5eAaPmF	zhypnotizovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
skákavkovité	skákavkovitý	k2eAgMnPc4d1	skákavkovitý
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
dobrý	dobrý	k2eAgInSc4d1	dobrý
zrak	zrak	k1gInSc4	zrak
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc4d1	důležitý
různé	různý	k2eAgInPc4d1	různý
posunky	posunek	k1gInPc4	posunek
a	a	k8xC	a
tance	tanec	k1gInPc4	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
úspěšných	úspěšný	k2eAgFnPc6d1	úspěšná
námluvách	námluva	k1gFnPc6	námluva
samec	samec	k1gMnSc1	samec
vnese	vnést	k5eAaPmIp3nS	vnést
spermie	spermie	k1gFnPc4	spermie
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
makadel	makadlo	k1gNnPc2	makadlo
do	do	k7c2	do
samičího	samičí	k2eAgInSc2d1	samičí
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
otvoru	otvor	k1gInSc2	otvor
skrze	skrze	k?	skrze
pohlavní	pohlavní	k2eAgFnSc4d1	pohlavní
destičku	destička	k1gFnSc4	destička
(	(	kIx(	(
<g/>
epigyne	epigynout	k5eAaPmIp3nS	epigynout
<g/>
)	)	kIx)	)
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
jejího	její	k3xOp3gInSc2	její
zadečku	zadeček	k1gInSc2	zadeček
<g/>
.	.	kIx.	.
</s>
<s>
Samičí	samičí	k2eAgFnSc1d1	samičí
rozmnožovací	rozmnožovací	k2eAgFnSc1d1	rozmnožovací
soustava	soustava	k1gFnSc1	soustava
může	moct	k5eAaImIp3nS	moct
sestávat	sestávat	k5eAaImF	sestávat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
trubic	trubice	k1gFnPc2	trubice
či	či	k8xC	či
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
i	i	k9	i
schránky	schránka	k1gFnPc1	schránka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
samice	samice	k1gFnSc1	samice
skladuje	skladovat	k5eAaImIp3nS	skladovat
spermie	spermie	k1gFnPc4	spermie
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
vhodnější	vhodný	k2eAgFnSc2d2	vhodnější
pro	pro	k7c4	pro
oplodnění	oplodnění	k1gNnSc4	oplodnění
<g/>
.	.	kIx.	.
</s>
<s>
Samcům	samec	k1gMnPc3	samec
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Tidarren	Tidarrno	k1gNnPc2	Tidarrno
před	před	k7c7	před
dospěním	dospění	k1gNnSc7	dospění
upadává	upadávat	k5eAaImIp3nS	upadávat
jedno	jeden	k4xCgNnSc1	jeden
makadlo	makadlo	k1gNnSc1	makadlo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgMnPc2	tento
pavouků	pavouk	k1gMnPc2	pavouk
představují	představovat	k5eAaImIp3nP	představovat
makadla	makadlo	k1gNnPc4	makadlo
20	[number]	k4	20
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
samčího	samčí	k2eAgNnSc2d1	samčí
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zbaví	zbavit	k5eAaPmIp3nP	zbavit
<g/>
,	,	kIx,	,
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jemenského	jemenský	k2eAgInSc2d1	jemenský
druhu	druh	k1gInSc2	druh
Tidarren	Tidarrna	k1gFnPc2	Tidarrna
argo	argo	k6eAd1	argo
zbývající	zbývající	k2eAgNnSc4d1	zbývající
makadlo	makadlo	k1gNnSc4	makadlo
utrhává	utrhávat	k5eAaImIp3nS	utrhávat
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
poté	poté	k6eAd1	poté
asi	asi	k9	asi
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
zůstane	zůstat	k5eAaPmIp3nS	zůstat
zachycené	zachycený	k2eAgNnSc1d1	zachycené
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
pohlavní	pohlavní	k2eAgFnSc6d1	pohlavní
destičce	destička	k1gFnSc6	destička
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
dále	daleko	k6eAd2	daleko
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
samice	samice	k1gFnSc1	samice
samce	samec	k1gInSc2	samec
sežere	sežrat	k5eAaPmIp3nS	sežrat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
%	%	kIx~	%
případech	případ	k1gInPc6	případ
samice	samice	k1gFnSc2	samice
australské	australský	k2eAgFnSc2d1	australská
snovačky	snovačka	k1gFnSc2	snovačka
Hasseltovy	Hasseltův	k2eAgFnSc2d1	Hasseltův
zabije	zabít	k5eAaPmIp3nS	zabít
a	a	k8xC	a
sežere	sežrat	k5eAaPmIp3nS	sežrat
samce	samec	k1gMnSc4	samec
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
pohlavního	pohlavní	k2eAgInSc2d1	pohlavní
otvoru	otvor	k1gInSc2	otvor
vloží	vložit	k5eAaPmIp3nS	vložit
své	svůj	k3xOyFgNnSc1	svůj
druhé	druhý	k4xOgNnSc1	druhý
makadlo	makadlo	k1gNnSc1	makadlo
<g/>
;	;	kIx,	;
samci	samec	k1gMnPc1	samec
jí	on	k3xPp3gFnSc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
dokonce	dokonce	k9	dokonce
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
nabodnout	nabodnout	k5eAaPmF	nabodnout
na	na	k7c4	na
její	její	k3xOp3gInPc4	její
drápky	drápek	k1gInPc4	drápek
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
samců	samec	k1gInPc2	samec
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
totiž	totiž	k9	totiž
nikdy	nikdy	k6eAd1	nikdy
nedostane	dostat	k5eNaPmIp3nS	dostat
možnost	možnost	k1gFnSc4	možnost
se	se	k3xPyFc4	se
pářit	pářit	k5eAaImF	pářit
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
navýšit	navýšit	k5eAaPmF	navýšit
počet	počet	k1gInSc4	počet
mláďat	mládě	k1gNnPc2	mládě
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
samice	samice	k1gFnSc1	samice
dobře	dobře	k6eAd1	dobře
krmena	krmen	k2eAgFnSc1d1	krmena
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
samci	samec	k1gMnPc1	samec
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
přežijí	přežít	k5eAaPmIp3nP	přežít
několik	několik	k4yIc4	několik
páření	páření	k1gNnPc2	páření
a	a	k8xC	a
umírají	umírat	k5eAaImIp3nP	umírat
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
příčin	příčina	k1gFnPc2	příčina
než	než	k8xS	než
usmrcením	usmrcení	k1gNnSc7	usmrcení
samicí	samice	k1gFnSc7	samice
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
dokonce	dokonce	k9	dokonce
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
pavučinách	pavučina	k1gFnPc6	pavučina
svých	svůj	k3xOyFgFnPc2	svůj
partnerek	partnerka	k1gFnPc2	partnerka
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
kladou	klást	k5eAaImIp3nP	klást
až	až	k9	až
3000	[number]	k4	3000
vajíček	vajíčko	k1gNnPc2	vajíčko
do	do	k7c2	do
jednoho	jeden	k4xCgMnSc2	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
kokonů	kokon	k1gInPc2	kokon
z	z	k7c2	z
pavučinových	pavučinový	k2eAgFnPc2d1	pavučinová
vláken	vlákna	k1gFnPc2	vlákna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
udržují	udržovat	k5eAaImIp3nP	udržovat
víceméně	víceméně	k9	víceméně
konstantní	konstantní	k2eAgFnSc1d1	konstantní
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
některých	některý	k3yIgMnPc2	některý
druhů	druh	k1gInPc2	druh
poté	poté	k6eAd1	poté
zemřou	zemřít	k5eAaPmIp3nP	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samice	samice	k1gFnSc1	samice
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
kokony	kokon	k1gInPc4	kokon
chrání	chránit	k5eAaImIp3nS	chránit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
uchytí	uchytit	k5eAaPmIp3nS	uchytit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
pavučině	pavučina	k1gFnSc3	pavučina
<g/>
,	,	kIx,	,
schovají	schovat	k5eAaPmIp3nP	schovat
v	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
v	v	k7c6	v
chelicerách	chelicera	k1gFnPc6	chelicera
nebo	nebo	k8xC	nebo
přichytí	přichytit	k5eAaPmIp3nS	přichytit
ke	k	k7c3	k
snovacím	snovací	k2eAgFnPc3d1	snovací
bradavkám	bradavka	k1gFnPc3	bradavka
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
tráví	trávit	k5eAaImIp3nP	trávit
celé	celá	k1gFnPc1	celá
své	svůj	k3xOyFgNnSc4	svůj
larvální	larvální	k2eAgNnSc4d1	larvální
stadium	stadium	k1gNnSc4	stadium
uvnitř	uvnitř	k7c2	uvnitř
vajíčka	vajíčko	k1gNnSc2	vajíčko
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
podobná	podobný	k2eAgNnPc1d1	podobné
dospělým	dospělí	k1gMnPc3	dospělí
jedincům	jedinec	k1gMnPc3	jedinec
<g/>
,	,	kIx,	,
jen	jen	k9	jen
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pavouci	pavouk	k1gMnPc1	pavouk
o	o	k7c4	o
svá	svůj	k3xOyFgNnPc4	svůj
mláďata	mládě	k1gNnPc4	mládě
pečují	pečovat	k5eAaImIp3nP	pečovat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
samice	samice	k1gFnSc1	samice
slíďákovitých	slíďákovitý	k2eAgMnPc2d1	slíďákovitý
nosí	nosit	k5eAaImIp3nS	nosit
kokon	kokon	k1gInSc4	kokon
s	s	k7c7	s
vajíčky	vajíčko	k1gNnPc7	vajíčko
připevněný	připevněný	k2eAgMnSc1d1	připevněný
na	na	k7c6	na
zadečku	zadeček	k1gInSc6	zadeček
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
reagují	reagovat	k5eAaBmIp3nP	reagovat
na	na	k7c6	na
"	"	kIx"	"
<g/>
škemrání	škemrání	k1gNnSc6	škemrání
<g/>
"	"	kIx"	"
svých	svůj	k3xOyFgNnPc2	svůj
mláďat	mládě	k1gNnPc2	mládě
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
dávají	dávat	k5eAaImIp3nP	dávat
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
vyvrhují	vyvrhovat	k5eAaImIp3nP	vyvrhovat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členovci	členovec	k1gMnPc1	členovec
se	se	k3xPyFc4	se
při	při	k7c6	při
růstu	růst	k1gInSc6	růst
musí	muset	k5eAaImIp3nS	muset
svlékat	svlékat	k5eAaImF	svlékat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
kutikula	kutikula	k1gFnSc1	kutikula
se	se	k3xPyFc4	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
roztáhnout	roztáhnout	k5eAaPmF	roztáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nS	pářit
s	s	k7c7	s
čerstvě	čerstvě	k6eAd1	čerstvě
vysvlečenými	vysvlečený	k2eAgFnPc7d1	vysvlečená
samicemi	samice	k1gFnPc7	samice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
slabé	slabý	k2eAgFnPc1d1	slabá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
byly	být	k5eAaImAgInP	být
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pavouků	pavouk	k1gMnPc2	pavouk
žije	žít	k5eAaImIp3nS	žít
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někteří	některý	k3yIgMnPc1	některý
sklípkani	sklípkan	k1gMnPc1	sklípkan
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
přežít	přežít	k5eAaPmF	přežít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
různorodých	různorodý	k2eAgFnPc2d1	různorodá
velikostí	velikost	k1gFnPc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
těch	ten	k3xDgInPc2	ten
nejmenších	malý	k2eAgInPc2d3	nejmenší
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Patu	pata	k1gFnSc4	pata
digua	digu	k1gInSc2	digu
z	z	k7c2	z
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
0,37	[number]	k4	0,37
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
největších	veliký	k2eAgMnPc2d3	veliký
sklípkanů	sklípkan	k1gMnPc2	sklípkan
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
až	až	k9	až
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
90	[number]	k4	90
milimetrů	milimetr	k1gInPc2	milimetr
a	a	k8xC	a
rozpětí	rozpětí	k1gNnSc2	rozpětí
nohou	noha	k1gFnPc2	noha
mají	mít	k5eAaImIp3nP	mít
až	až	k9	až
250	[number]	k4	250
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
symbiotické	symbiotický	k2eAgInPc4d1	symbiotický
vztahy	vztah	k1gInPc4	vztah
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
metodou	metoda	k1gFnSc7	metoda
lovu	lov	k1gInSc2	lov
kořisti	kořist	k1gFnSc2	kořist
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc4	její
chycení	chycení	k1gNnSc4	chycení
do	do	k7c2	do
lepkavé	lepkavý	k2eAgFnSc2d1	lepkavá
pavučiny	pavučina	k1gFnSc2	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Různá	různý	k2eAgNnPc1d1	různé
umístění	umístění	k1gNnPc1	umístění
pavučiny	pavučina	k1gFnSc2	pavučina
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
různým	různý	k2eAgInPc3d1	různý
druhům	druh	k1gInPc3	druh
pavouků	pavouk	k1gMnPc2	pavouk
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
oblasti	oblast	k1gFnSc6	oblast
chytat	chytat	k5eAaImF	chytat
odlišné	odlišný	k2eAgInPc4d1	odlišný
druhy	druh	k1gInPc4	druh
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
horizontálně	horizontálně	k6eAd1	horizontálně
umístěné	umístěný	k2eAgFnPc1d1	umístěná
sítě	síť	k1gFnPc1	síť
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzlétá	vzlétat	k5eAaImIp3nS	vzlétat
z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
pod	pod	k7c7	pod
nimi	on	k3xPp3gFnPc7	on
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vertikálně	vertikálně	k6eAd1	vertikálně
umístěné	umístěný	k2eAgFnPc1d1	umístěná
sítě	síť	k1gFnPc1	síť
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
hmyz	hmyz	k1gInSc4	hmyz
v	v	k7c6	v
horizontálním	horizontální	k2eAgInSc6d1	horizontální
letu	let	k1gInSc6	let
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
budují	budovat	k5eAaImIp3nP	budovat
pavučiny	pavučina	k1gFnPc4	pavučina
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
špatný	špatný	k2eAgInSc4d1	špatný
zrak	zrak	k1gInSc4	zrak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
citliví	citlivý	k2eAgMnPc1d1	citlivý
na	na	k7c4	na
vibrace	vibrace	k1gFnPc4	vibrace
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc1	samička
vodního	vodní	k2eAgMnSc2d1	vodní
pavouka	pavouk	k1gMnSc2	pavouk
vodoucha	vodouch	k1gMnSc2	vodouch
stříbřitého	stříbřitý	k2eAgMnSc2d1	stříbřitý
(	(	kIx(	(
<g/>
Argyroneta	Argyronet	k1gMnSc2	Argyronet
aquatica	aquaticus	k1gMnSc2	aquaticus
<g/>
)	)	kIx)	)
stavějí	stavět	k5eAaImIp3nP	stavět
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
"	"	kIx"	"
<g/>
zvon	zvon	k1gInSc1	zvon
<g/>
"	"	kIx"	"
z	z	k7c2	z
pavučin	pavučina	k1gFnPc2	pavučina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
plní	plnit	k5eAaImIp3nS	plnit
vzduchem	vzduch	k1gInSc7	vzduch
a	a	k8xC	a
využívají	využívat	k5eAaImIp3nP	využívat
ho	on	k3xPp3gInSc4	on
k	k	k7c3	k
trávení	trávení	k1gNnSc3	trávení
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
svlékání	svlékání	k1gNnSc6	svlékání
<g/>
,	,	kIx,	,
páření	páření	k1gNnSc6	páření
a	a	k8xC	a
kladení	kladení	k1gNnSc6	kladení
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
tráví	trávit	k5eAaImIp3nP	trávit
ve	v	k7c6	v
zvonech	zvon	k1gInPc6	zvon
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc4d1	celý
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
a	a	k8xC	a
vylézají	vylézat	k5eAaImIp3nP	vylézat
jenom	jenom	k9	jenom
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
chycení	chycení	k1gNnSc1	chycení
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zvonu	zvon	k1gInSc2	zvon
nebo	nebo	k8xC	nebo
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
přilehlých	přilehlý	k2eAgNnPc2d1	přilehlé
vláken	vlákno	k1gNnPc2	vlákno
dotkla	dotknout	k5eAaPmAgFnS	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
využívá	využívat	k5eAaPmIp3nS	využívat
hladiny	hladina	k1gFnPc4	hladina
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
rybníků	rybník	k1gInPc2	rybník
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
pavučiny	pavučina	k1gFnPc1	pavučina
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
rozpoznává	rozpoznávat	k5eAaImIp3nS	rozpoznávat
vibrace	vibrace	k1gFnPc4	vibrace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
hmyz	hmyz	k1gInSc4	hmyz
spadlý	spadlý	k2eAgInSc4d1	spadlý
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
snažící	snažící	k2eAgNnSc1d1	snažící
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
loví	lovit	k5eAaImIp3nP	lovit
vrháním	vrhání	k1gNnSc7	vrhání
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgFnPc1d1	malá
pavučiny	pavučina	k1gFnPc1	pavučina
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
následně	následně	k6eAd1	následně
hýbají	hýbat	k5eAaImIp3nP	hýbat
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
pakřižák	pakřižák	k1gInSc1	pakřižák
(	(	kIx(	(
<g/>
Hyptiotes	Hyptiotes	k1gInSc1	Hyptiotes
<g/>
)	)	kIx)	)
a	a	k8xC	a
čeledi	čeleď	k1gFnSc2	čeleď
křižáčkovití	křižáčkovitý	k2eAgMnPc1d1	křižáčkovitý
(	(	kIx(	(
<g/>
Theridiosomatidae	Theridiosomatidae	k1gNnSc7	Theridiosomatidae
<g/>
)	)	kIx)	)
své	svůj	k3xOyFgFnPc4	svůj
pavučiny	pavučina	k1gFnPc4	pavučina
natahují	natahovat	k5eAaImIp3nP	natahovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
kořist	kořist	k1gFnSc1	kořist
dotkne	dotknout	k5eAaPmIp3nS	dotknout
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
je	on	k3xPp3gMnPc4	on
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
vrhačovití	vrhačovitý	k2eAgMnPc1d1	vrhačovitý
(	(	kIx(	(
<g/>
Deinopidae	Deinopidae	k1gNnSc7	Deinopidae
<g/>
)	)	kIx)	)
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
ještě	ještě	k6eAd1	ještě
menší	malý	k2eAgFnPc1d2	menší
pavučiny	pavučina	k1gFnPc1	pavučina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
drží	držet	k5eAaImIp3nP	držet
mezi	mezi	k7c7	mezi
prvními	první	k4xOgInPc7	první
dvěma	dva	k4xCgInPc7	dva
páry	pár	k1gInPc7	pár
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zahlédnou	zahlédnout	k5eAaPmIp3nP	zahlédnout
potenciální	potenciální	k2eAgFnSc4d1	potenciální
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
pavučinu	pavučina	k1gFnSc4	pavučina
roztáhnou	roztáhnout	k5eAaPmIp3nP	roztáhnout
<g/>
,	,	kIx,	,
vyrazí	vyrazit	k5eAaPmIp3nP	vyrazit
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
vpřed	vpřed	k6eAd1	vpřed
a	a	k8xC	a
kořist	kořist	k1gFnSc4	kořist
chytí	chytit	k5eAaPmIp3nS	chytit
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
pavučiny	pavučina	k1gFnSc2	pavučina
se	se	k3xPyFc4	se
tímto	tento	k3xDgNnSc7	tento
roztažením	roztažení	k1gNnSc7	roztažení
může	moct	k5eAaImIp3nS	moct
zvětšit	zvětšit	k5eAaPmF	zvětšit
až	až	k9	až
desetkrát	desetkrát	k6eAd1	desetkrát
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
vrhačovitých	vrhačovití	k1gMnPc2	vrhačovití
využívá	využívat	k5eAaPmIp3nS	využívat
dvě	dva	k4xCgFnPc4	dva
různé	různý	k2eAgFnPc4d1	různá
techniky	technika	k1gFnPc4	technika
lovu	lov	k1gInSc2	lov
<g/>
:	:	kIx,	:
útočí	útočit	k5eAaImIp3nS	útočit
buď	buď	k8xC	buď
nahoru	nahoru	k6eAd1	nahoru
na	na	k7c4	na
létající	létající	k2eAgInSc4d1	létající
hmyz	hmyz	k1gInSc4	hmyz
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
detekuje	detekovat	k5eAaImIp3nS	detekovat
pomocí	pomocí	k7c2	pomocí
vibrací	vibrace	k1gFnPc2	vibrace
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dolu	dol	k1gInSc2	dol
na	na	k7c4	na
hmyz	hmyz	k1gInSc4	hmyz
pohybující	pohybující	k2eAgInSc4d1	pohybující
se	se	k3xPyFc4	se
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
zpozoruje	zpozorovat	k5eAaPmIp3nS	zpozorovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
potravy	potrava	k1gFnSc2	potrava
většiny	většina	k1gFnSc2	většina
vrhačovitých	vrhačovití	k1gMnPc2	vrhačovití
tvoří	tvořit	k5eAaImIp3nS	tvořit
pozemní	pozemní	k2eAgInSc4d1	pozemní
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
i	i	k9	i
populace	populace	k1gFnSc1	populace
druhu	druh	k1gInSc2	druh
Deinopis	Deinopis	k1gInSc1	Deinopis
subrufa	subruf	k1gMnSc2	subruf
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
živila	živit	k5eAaImAgNnP	živit
hlavně	hlavně	k6eAd1	hlavně
létajícími	létající	k2eAgFnPc7d1	létající
tiplicemi	tiplice	k1gFnPc7	tiplice
(	(	kIx(	(
<g/>
Tipulidae	Tipulidae	k1gInSc1	Tipulidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dospělé	dospělý	k2eAgFnPc1d1	dospělá
samice	samice	k1gFnPc1	samice
bolasových	bolasův	k2eAgMnPc2d1	bolasův
pavouků	pavouk	k1gMnPc2	pavouk
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
"	"	kIx"	"
<g/>
pavučiny	pavučina	k1gFnPc1	pavučina
<g/>
"	"	kIx"	"
sestávající	sestávající	k2eAgInPc1d1	sestávající
z	z	k7c2	z
jediného	jediný	k2eAgNnSc2d1	jediné
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
střeží	střežit	k5eAaImIp3nS	střežit
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
pomocí	pomocí	k7c2	pomocí
"	"	kIx"	"
<g/>
lasa	laso	k1gNnSc2	laso
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pavučinové	pavučinový	k2eAgNnSc1d1	pavučinové
vlákno	vlákno	k1gNnSc1	vlákno
zakončené	zakončený	k2eAgNnSc1d1	zakončené
velkou	velký	k2eAgFnSc7d1	velká
lepkavou	lepkavý	k2eAgFnSc7d1	lepkavá
koulí	koule	k1gFnSc7	koule
<g/>
.	.	kIx.	.
</s>
<s>
Vylučují	vylučovat	k5eAaImIp3nP	vylučovat
látky	látka	k1gFnPc4	látka
připomínající	připomínající	k2eAgInSc4d1	připomínající
můří	můří	k2eAgInSc4d1	můří
feromony	feromon	k1gInPc4	feromon
a	a	k8xC	a
poté	poté	k6eAd1	poté
na	na	k7c4	na
můry	můra	k1gFnPc4	můra
vrhají	vrhat	k5eAaImIp3nP	vrhat
svá	svůj	k3xOyFgNnPc4	svůj
lasa	laso	k1gNnPc4	laso
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
asi	asi	k9	asi
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
případů	případ	k1gInPc2	případ
minou	minout	k5eAaImIp3nP	minout
<g/>
,	,	kIx,	,
za	za	k7c4	za
noc	noc	k1gFnSc4	noc
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
ulovit	ulovit	k5eAaPmF	ulovit
hmyz	hmyz	k1gInSc4	hmyz
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
hmotnosti	hmotnost	k1gFnSc6	hmotnost
jako	jako	k9	jako
pavouci	pavouk	k1gMnPc1	pavouk
stejné	stejný	k2eAgFnSc2d1	stejná
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pavučiny	pavučina	k1gFnPc4	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
nepodaří	podařit	k5eNaPmIp3nS	podařit
ulovit	ulovit	k5eAaPmF	ulovit
žádnou	žádný	k3yNgFnSc4	žádný
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
sežerou	sežrat	k5eAaPmIp3nP	sežrat
laso	laso	k1gNnSc4	laso
<g/>
,	,	kIx,	,
chvíli	chvíle	k1gFnSc4	chvíle
si	se	k3xPyFc3	se
odpočinou	odpočinout	k5eAaPmIp3nP	odpočinout
a	a	k8xC	a
poté	poté	k6eAd1	poté
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
nové	nový	k2eAgNnSc1d1	nové
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
a	a	k8xC	a
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgNnPc1d2	menší
a	a	k8xC	a
lasa	laso	k1gNnPc1	laso
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
feromony	feromon	k1gInPc4	feromon
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
lákají	lákat	k5eAaImIp3nP	lákat
koutulovité	koutulovitý	k2eAgFnPc1d1	koutulovitý
(	(	kIx(	(
<g/>
Psychodidae	Psychodida	k1gFnPc1	Psychodida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
molům	mol	k1gMnPc3	mol
podobné	podobný	k2eAgFnPc4d1	podobná
mušky	muška	k1gFnPc4	muška
chytají	chytat	k5eAaImIp3nP	chytat
předními	přední	k2eAgInPc7d1	přední
páry	pár	k1gInPc7	pár
nohou	noha	k1gFnPc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
Sklípkošovití	Sklípkošovití	k1gMnPc1	Sklípkošovití
(	(	kIx(	(
<g/>
Liphistiidae	Liphistiidae	k1gInSc1	Liphistiidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sklípákovití	sklípákovitý	k2eAgMnPc1d1	sklípákovitý
(	(	kIx(	(
<g/>
Ctenizidae	Ctenizidae	k1gNnSc7	Ctenizidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
sklípkanů	sklípkan	k1gMnPc2	sklípkan
přepadává	přepadávat	k5eAaImIp3nS	přepadávat
kořist	kořist	k1gFnSc4	kořist
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
doupěte	doupě	k1gNnSc2	doupě
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
kryto	kryt	k2eAgNnSc4d1	kryto
poklopem	poklop	k1gInSc7	poklop
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
obklopeno	obklopen	k2eAgNnSc1d1	obklopeno
systémem	systém	k1gInSc7	systém
pavučinových	pavučinový	k2eAgNnPc2d1	pavučinové
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
pavouky	pavouk	k1gMnPc4	pavouk
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
potenciální	potenciální	k2eAgFnSc2d1	potenciální
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
včelami	včela	k1gFnPc7	včela
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
nastavit	nastavit	k5eAaPmF	nastavit
odrazovost	odrazovost	k1gFnSc4	odrazovost
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
podobný	podobný	k2eAgInSc1d1	podobný
rostlinám	rostlina	k1gFnPc3	rostlina
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
ukrývají	ukrývat	k5eAaImIp3nP	ukrývat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Včely	včela	k1gFnPc4	včela
vidí	vidět	k5eAaImIp3nS	vidět
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Slíďákovití	Slíďákovitý	k2eAgMnPc1d1	Slíďákovitý
<g/>
,	,	kIx,	,
skákavkovití	skákavkovitý	k2eAgMnPc1d1	skákavkovitý
<g/>
,	,	kIx,	,
běžníkovití	běžníkovitý	k2eAgMnPc1d1	běžníkovitý
a	a	k8xC	a
lovčíci	lovčík	k1gMnPc1	lovčík
(	(	kIx(	(
<g/>
Dolomedes	Dolomedes	k1gMnSc1	Dolomedes
<g/>
)	)	kIx)	)
svou	svůj	k3xOyFgFnSc4	svůj
kořist	kořist	k1gFnSc4	kořist
honí	honit	k5eAaImIp3nP	honit
a	a	k8xC	a
spoléhají	spoléhat	k5eAaImIp3nP	spoléhat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
zrak	zrak	k1gInSc4	zrak
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
pavouci	pavouk	k1gMnPc1	pavouk
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Portia	Portia	k1gFnSc1	Portia
loví	lovit	k5eAaImIp3nS	lovit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
pavouky	pavouk	k1gMnPc4	pavouk
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
náznaky	náznak	k1gInPc1	náznak
inteligentního	inteligentní	k2eAgNnSc2d1	inteligentní
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Laboratorní	laboratorní	k2eAgInPc1d1	laboratorní
výzkumy	výzkum	k1gInPc1	výzkum
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
instinktivní	instinktivní	k2eAgInSc1d1	instinktivní
způsob	způsob	k1gInSc1	způsob
lovu	lov	k1gInSc2	lov
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tyto	tento	k3xDgMnPc4	tento
pavouky	pavouk	k1gMnPc4	pavouk
jen	jen	k9	jen
počátečním	počáteční	k2eAgInSc7d1	počáteční
bodem	bod	k1gInSc7	bod
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
lovící	lovící	k2eAgFnSc4d1	lovící
taktiku	taktika	k1gFnSc4	taktika
pilují	pilovat	k5eAaImIp3nP	pilovat
metodou	metoda	k1gFnSc7	metoda
pokus	pokus	k1gInSc1	pokus
a	a	k8xC	a
omyl	omyl	k1gInSc1	omyl
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
napodobující	napodobující	k2eAgFnSc2d1	napodobující
mravence	mravenec	k1gMnSc2	mravenec
se	se	k3xPyFc4	se
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
pavouků	pavouk	k1gMnPc2	pavouk
liší	lišit	k5eAaImIp3nS	lišit
v	v	k7c6	v
několika	několik	k4yIc6	několik
věcech	věc	k1gFnPc6	věc
<g/>
:	:	kIx,	:
mají	mít	k5eAaImIp3nP	mít
užší	úzký	k2eAgInSc4d2	užší
zadeček	zadeček	k1gInSc4	zadeček
a	a	k8xC	a
umělý	umělý	k2eAgInSc4d1	umělý
"	"	kIx"	"
<g/>
pas	pas	k1gInSc4	pas
<g/>
"	"	kIx"	"
v	v	k7c6	v
hlavohrudi	hlavohruď	k1gFnSc6	hlavohruď
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
napodobili	napodobit	k5eAaPmAgMnP	napodobit
tři	tři	k4xCgNnPc4	tři
oddělená	oddělený	k2eAgNnPc4d1	oddělené
tagmata	tagma	k1gNnPc4	tagma
mravenčích	mravenčí	k2eAgNnPc2d1	mravenčí
těl	tělo	k1gNnPc2	tělo
<g/>
;	;	kIx,	;
prvním	první	k4xOgMnSc6	první
párem	pár	k1gInSc7	pár
nohou	noha	k1gFnPc2	noha
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
tykadla	tykadlo	k1gNnPc1	tykadlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	on	k3xPp3gMnPc4	on
nemají	mít	k5eNaImIp3nP	mít
a	a	k8xC	a
protože	protože	k8xS	protože
mravenci	mravenec	k1gMnPc1	mravenec
mají	mít	k5eAaImIp3nP	mít
<g />
.	.	kIx.	.
</s>
<s>
jen	jen	k9	jen
tři	tři	k4xCgInPc1	tři
páry	pár	k1gInPc1	pár
nohou	noha	k1gFnPc2	noha
<g/>
;	;	kIx,	;
okolo	okolo	k7c2	okolo
jednoho	jeden	k4xCgInSc2	jeden
páru	pár	k1gInSc2	pár
očí	oko	k1gNnPc2	oko
mají	mít	k5eAaImIp3nP	mít
velké	velký	k2eAgFnPc4d1	velká
barevné	barevný	k2eAgFnPc4d1	barevná
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zakryli	zakrýt	k5eAaPmAgMnP	zakrýt
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
osm	osm	k4xCc1	osm
jednoduchých	jednoduchý	k2eAgNnPc2d1	jednoduché
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
mravenci	mravenec	k1gMnPc1	mravenec
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc1	dva
složené	složený	k2eAgFnPc1d1	složená
oči	oko	k1gNnPc1	oko
<g/>
;	;	kIx,	;
a	a	k8xC	a
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
své	svůj	k3xOyFgNnSc4	svůj
tělo	tělo	k1gNnSc4	tělo
chloupky	chloupek	k1gInPc4	chloupek
odrážejícími	odrážející	k2eAgFnPc7d1	odrážející
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
napodobili	napodobit	k5eAaPmAgMnP	napodobit
lesklé	lesklý	k2eAgNnSc4d1	lesklé
tělo	tělo	k1gNnSc4	tělo
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
samec	samec	k1gInSc1	samec
a	a	k8xC	a
samička	samička	k1gFnSc1	samička
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
odlišné	odlišný	k2eAgInPc1d1	odlišný
druhy	druh	k1gInPc1	druh
mravencovitých	mravencovitý	k2eAgFnPc2d1	mravencovitý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
samičky	samička	k1gFnPc1	samička
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc3d2	veliký
než	než	k8xS	než
samci	samec	k1gMnSc3	samec
<g/>
.	.	kIx.	.
</s>
<s>
Imitátoři	imitátor	k1gMnPc1	imitátor
také	také	k9	také
uzpůsobují	uzpůsobovat	k5eAaImIp3nP	uzpůsobovat
své	svůj	k3xOyFgNnSc4	svůj
chování	chování	k1gNnSc4	chování
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připomínalo	připomínat	k5eAaImAgNnS	připomínat
chování	chování	k1gNnSc4	chování
jejich	jejich	k3xOp3gInPc2	jejich
mravenčích	mravenčí	k2eAgInPc2d1	mravenčí
protějšků	protějšek	k1gInPc2	protějšek
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
jejich	jejich	k3xOp3gInSc4	jejich
způsob	způsob	k1gInSc4	způsob
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
se	se	k3xPyFc4	se
skákání	skákání	k1gNnSc2	skákání
<g/>
.	.	kIx.	.
</s>
<s>
Napodobování	napodobování	k1gNnSc1	napodobování
mravenců	mravenec	k1gMnPc2	mravenec
slouží	sloužit	k5eAaImIp3nS	sloužit
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
pavouků	pavouk	k1gMnPc2	pavouk
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
členovců	členovec	k1gMnPc2	členovec
jako	jako	k8xS	jako
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c4	před
predátory	predátor	k1gMnPc4	predátor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
využívají	využívat	k5eAaImIp3nP	využívat
zrak	zrak	k1gInSc4	zrak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
ještěrky	ještěrka	k1gFnPc1	ještěrka
a	a	k8xC	a
pavouci	pavouk	k1gMnPc1	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
ale	ale	k9	ale
živí	živit	k5eAaImIp3nP	živit
mravenci	mravenec	k1gMnPc1	mravenec
či	či	k8xC	či
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
mravenci	mravenec	k1gMnPc1	mravenec
opečovávají	opečovávat	k5eAaImIp3nP	opečovávat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
mšice	mšice	k1gFnPc1	mšice
<g/>
.	.	kIx.	.
</s>
<s>
Pavouk	pavouk	k1gMnSc1	pavouk
rodu	rod	k1gInSc2	rod
Amyciaea	Amyciaea	k1gMnSc1	Amyciaea
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
nevypadá	vypadat	k5eNaImIp3nS	vypadat
jako	jako	k9	jako
mravenec	mravenec	k1gMnSc1	mravenec
rodu	rod	k1gInSc2	rod
Oecophylla	Oecophyllo	k1gNnSc2	Oecophyllo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
loví	lovit	k5eAaImIp3nP	lovit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
chování	chování	k1gNnSc4	chování
umírajícího	umírající	k1gMnSc2	umírající
pavouka	pavouk	k1gMnSc2	pavouk
a	a	k8xC	a
láká	lákat	k5eAaImIp3nS	lákat
tak	tak	k6eAd1	tak
mravenčí	mravenčí	k2eAgFnSc1d1	mravenčí
dělnice	dělnice	k1gFnSc1	dělnice
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
draví	dravý	k2eAgMnPc1d1	dravý
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Bagheera	Bagheera	k1gFnSc1	Bagheera
kiplingi	kipling	k1gFnSc2	kipling
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
skákavkovití	skákavkovitý	k2eAgMnPc1d1	skákavkovitý
přijímá	přijímat	k5eAaImIp3nS	přijímat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
potravy	potrava	k1gFnSc2	potrava
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
produkují	produkovat	k5eAaImIp3nP	produkovat
akácie	akácie	k1gFnPc1	akácie
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vzájemně	vzájemně	k6eAd1	vzájemně
prospěšného	prospěšný	k2eAgInSc2d1	prospěšný
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
mravenci	mravenec	k1gMnPc7	mravenec
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
některých	některý	k3yIgMnPc2	některý
druhů	druh	k1gInPc2	druh
z	z	k7c2	z
čeledí	čeleď	k1gFnPc2	čeleď
šplhavkovití	šplhavkovitý	k2eAgMnPc1d1	šplhavkovitý
(	(	kIx(	(
<g/>
Anyphaenidae	Anyphaenidae	k1gNnSc7	Anyphaenidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlavounovití	hlavounovitý	k2eAgMnPc1d1	hlavounovitý
(	(	kIx(	(
<g/>
Corinnidae	Corinnidae	k1gNnSc7	Corinnidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zápředníkovití	zápředníkovitý	k2eAgMnPc1d1	zápředníkovitý
(	(	kIx(	(
<g/>
Clubionidae	Clubionidae	k1gNnSc7	Clubionidae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžníkovití	běžníkovitý	k2eAgMnPc1d1	běžníkovitý
(	(	kIx(	(
<g/>
Thomisidae	Thomisidae	k1gNnSc7	Thomisidae
<g/>
)	)	kIx)	)
a	a	k8xC	a
skákavkovití	skákavkovitý	k2eAgMnPc1d1	skákavkovitý
(	(	kIx(	(
<g/>
Salticidae	Salticidae	k1gNnSc7	Salticidae
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
nektarem	nektar	k1gInSc7	nektar
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
laboratorních	laboratorní	k2eAgInPc6d1	laboratorní
pokusech	pokus	k1gInPc6	pokus
dávají	dávat	k5eAaImIp3nP	dávat
tito	tento	k3xDgMnPc1	tento
pavouci	pavouk	k1gMnPc1	pavouk
přednost	přednost	k1gFnSc4	přednost
sladkým	sladký	k2eAgInPc3d1	sladký
roztokům	roztok	k1gInPc3	roztok
před	před	k7c7	před
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
živiny	živina	k1gFnPc4	živina
<g/>
.	.	kIx.	.
</s>
<s>
Nektar	nektar	k1gInSc1	nektar
kromě	kromě	k7c2	kromě
cukru	cukr	k1gInSc2	cukr
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
<g/>
,	,	kIx,	,
lipidy	lipid	k1gInPc4	lipid
<g/>
,	,	kIx,	,
vitamíny	vitamín	k1gInPc4	vitamín
a	a	k8xC	a
minerály	minerál	k1gInPc4	minerál
a	a	k8xC	a
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
studií	studie	k1gFnPc2	studie
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
pavouků	pavouk	k1gMnPc2	pavouk
žijí	žít	k5eAaImIp3nP	žít
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Živení	živení	k1gNnPc1	živení
se	se	k3xPyFc4	se
nektarem	nektar	k1gInSc7	nektar
odstraňuje	odstraňovat	k5eAaImIp3nS	odstraňovat
riziko	riziko	k1gNnSc1	riziko
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
lovením	lovení	k1gNnSc7	lovení
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
potřebu	potřeba	k1gFnSc4	potřeba
vytváření	vytváření	k1gNnSc2	vytváření
jedu	jed	k1gInSc2	jed
a	a	k8xC	a
trávicích	trávicí	k2eAgInPc2d1	trávicí
enzymů	enzym	k1gInPc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
mrtvými	mrtvý	k2eAgMnPc7d1	mrtvý
členovci	členovec	k1gMnPc7	členovec
(	(	kIx(	(
<g/>
mrchožroutství	mrchožroutství	k1gNnPc2	mrchožroutství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pavoučími	pavoučí	k2eAgNnPc7d1	pavoučí
vlákny	vlákno	k1gNnPc7	vlákno
nebo	nebo	k8xC	nebo
svou	svůj	k3xOyFgFnSc7	svůj
svléknutou	svléknutý	k2eAgFnSc7d1	svléknutá
kutikulou	kutikula	k1gFnSc7	kutikula
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
žerou	žrát	k5eAaImIp3nP	žrát
i	i	k9	i
pyl	pyl	k1gInSc4	pyl
zachycený	zachycený	k2eAgInSc4d1	zachycený
v	v	k7c6	v
pavučinách	pavučina	k1gFnPc6	pavučina
a	a	k8xC	a
z	z	k7c2	z
pozorování	pozorování	k1gNnSc2	pozorování
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mláďata	mládě	k1gNnPc1	mládě
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
šanci	šance	k1gFnSc4	šance
přežití	přežití	k1gNnSc2	přežití
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
ho	on	k3xPp3gMnSc4	on
žrát	žrát	k5eAaImF	žrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
pavouci	pavouk	k1gMnPc1	pavouk
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
živí	živit	k5eAaImIp3nP	živit
také	také	k9	také
banány	banán	k1gInPc7	banán
<g/>
,	,	kIx,	,
marmeládou	marmeláda	k1gFnSc7	marmeláda
<g/>
,	,	kIx,	,
mlékem	mléko	k1gNnSc7	mléko
<g/>
,	,	kIx,	,
žloutky	žloutek	k1gInPc7	žloutek
či	či	k8xC	či
klobásami	klobása	k1gFnPc7	klobása
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
silné	silný	k2eAgInPc1d1	silný
důkazy	důkaz	k1gInPc1	důkaz
podporující	podporující	k2eAgFnSc4d1	podporující
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
zbarvení	zbarvení	k1gNnSc1	zbarvení
pavouků	pavouk	k1gMnPc2	pavouk
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
maskování	maskování	k1gNnSc1	maskování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
je	být	k5eAaImIp3nS	být
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
hlavními	hlavní	k2eAgMnPc7d1	hlavní
predátory	predátor	k1gMnPc7	predátor
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ptáků	pták	k1gMnPc2	pták
a	a	k8xC	a
parazitoidních	parazitoidní	k2eAgFnPc2d1	parazitoidní
vos	vosa	k1gFnPc2	vosa
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
dobré	dobrý	k2eAgNnSc4d1	dobré
barevné	barevný	k2eAgNnSc4d1	barevné
vidění	vidění	k1gNnSc4	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
pavouků	pavouk	k1gMnPc2	pavouk
je	být	k5eAaImIp3nS	být
zbarveno	zbarvit	k5eAaPmNgNnS	zbarvit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zapadlo	zapadnout	k5eAaPmAgNnS	zapadnout
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
mají	mít	k5eAaImIp3nP	mít
rušivé	rušivý	k2eAgNnSc4d1	rušivé
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
predátorům	predátor	k1gMnPc3	predátor
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
rozeznat	rozeznat	k5eAaPmF	rozeznat
jejich	jejich	k3xOp3gInPc4	jejich
obrysy	obrys	k1gInPc4	obrys
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
několika	několik	k4yIc2	několik
málo	málo	k6eAd1	málo
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Theridion	Theridion	k1gInSc1	Theridion
grallator	grallator	k1gInSc1	grallator
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
zbarvení	zbarvení	k1gNnPc2	zbarvení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
účel	účel	k1gInSc4	účel
zmást	zmást	k5eAaPmF	zmást
predátory	predátor	k1gMnPc4	predátor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
problémy	problém	k1gInPc4	problém
tyto	tento	k3xDgInPc4	tento
druhy	druh	k1gInPc4	druh
rozeznat	rozeznat	k5eAaPmF	rozeznat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pavouků	pavouk	k1gMnPc2	pavouk
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
či	či	k8xC	či
nepoživatelná	poživatelný	k2eNgFnSc1d1	nepoživatelná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gInPc2	on
vyskytovalo	vyskytovat	k5eAaImAgNnS	vyskytovat
varovné	varovný	k2eAgNnSc1d1	varovné
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
jedem	jed	k1gInSc7	jed
či	či	k8xC	či
velkými	velký	k2eAgFnPc7d1	velká
chelicerami	chelicera	k1gFnPc7	chelicera
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
skvrny	skvrna	k1gFnPc4	skvrna
varovných	varovný	k2eAgFnPc2d1	varovná
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nP	cítit
ohroženi	ohrozit	k5eAaPmNgMnP	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zástupců	zástupce	k1gMnPc2	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
Theraphosidae	Theraphosidae	k1gFnSc1	Theraphosidae
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
zadečku	zadeček	k1gInSc6	zadeček
dráždivé	dráždivý	k2eAgInPc4d1	dráždivý
chloupky	chloupek	k1gInPc4	chloupek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
svýma	svůj	k3xOyFgFnPc7	svůj
nohama	noha	k1gFnPc7	noha
hází	házet	k5eAaImIp3nP	házet
na	na	k7c4	na
predátory	predátor	k1gMnPc4	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
chloupky	chloupek	k1gInPc1	chloupek
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
háčky	háček	k1gInPc4	háček
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
silně	silně	k6eAd1	silně
dráždit	dráždit	k5eAaImF	dráždit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádný	žádný	k3yNgInSc1	žádný
jed	jed	k1gInSc1	jed
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
pavouků	pavouk	k1gMnPc2	pavouk
se	se	k3xPyFc4	se
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
vosami	vosa	k1gFnPc7	vosa
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
pavučiny	pavučina	k1gFnSc2	pavučina
zaplétají	zaplétat	k5eAaImIp3nP	zaplétat
zvláště	zvláště	k6eAd1	zvláště
odolná	odolný	k2eAgNnPc1d1	odolné
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
protrhnutím	protrhnutí	k1gNnSc7	protrhnutí
mají	mít	k5eAaImIp3nP	mít
vosy	vosa	k1gFnPc4	vosa
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pavouk	pavouk	k1gMnSc1	pavouk
má	mít	k5eAaImIp3nS	mít
čas	čas	k1gInSc4	čas
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
druhu	druh	k1gInSc2	druh
Carparachne	Carparachne	k1gFnSc1	Carparachne
aureoflava	aureoflava	k1gFnSc1	aureoflava
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
Namib	Namiba	k1gFnPc2	Namiba
unikají	unikat	k5eAaImIp3nP	unikat
parazitoidním	parazitoidní	k2eAgFnPc3d1	parazitoidní
vosám	vosa	k1gFnPc3	vosa
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
převrátí	převrátit	k5eAaPmIp3nS	převrátit
na	na	k7c4	na
bok	bok	k1gInSc4	bok
a	a	k8xC	a
skutálí	skutálet	k5eAaPmIp3nS	skutálet
se	se	k3xPyFc4	se
z	z	k7c2	z
písečných	písečný	k2eAgFnPc2d1	písečná
dun	duna	k1gFnPc2	duna
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
pavučiny	pavučina	k1gFnPc4	pavučina
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
spolu	spolu	k6eAd1	spolu
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
koloniích	kolonie	k1gFnPc6	kolonie
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
sociální	sociální	k2eAgNnSc4d1	sociální
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
tak	tak	k6eAd1	tak
důsledné	důsledný	k2eAgNnSc1d1	důsledné
jako	jako	k8xC	jako
u	u	k7c2	u
sociálního	sociální	k2eAgInSc2d1	sociální
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Anelosimus	Anelosimus	k1gMnSc1	Anelosimus
eximius	eximius	k1gMnSc1	eximius
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
snovačkovití	snovačkovitý	k2eAgMnPc1d1	snovačkovitý
(	(	kIx(	(
<g/>
Theridiidae	Theridiidae	k1gNnSc7	Theridiidae
<g/>
)	)	kIx)	)
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
kolonie	kolonie	k1gFnSc1	kolonie
čítající	čítající	k2eAgFnSc1d1	čítající
až	až	k9	až
50	[number]	k4	50
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodu	rod	k1gInSc6	rod
Anelosimus	Anelosimus	k1gInSc1	Anelosimus
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc1	několik
sociálních	sociální	k2eAgInPc2d1	sociální
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
žijící	žijící	k2eAgInPc1d1	žijící
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
jsou	být	k5eAaImIp3nP	být
sociální	sociální	k2eAgInPc1d1	sociální
a	a	k8xC	a
druhy	druh	k1gInPc1	druh
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
jsou	být	k5eAaImIp3nP	být
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
sociální	sociální	k2eAgNnSc1d1	sociální
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
patřících	patřící	k2eAgInPc2d1	patřící
do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
čeledi	čeleď	k1gFnSc2	čeleď
se	se	k3xPyFc4	se
sociální	sociální	k2eAgNnSc1d1	sociální
chování	chování	k1gNnSc1	chování
může	moct	k5eAaImIp3nS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
druh	druh	k1gInSc4	druh
Theridion	Theridion	k1gInSc1	Theridion
nigroannulatum	nigroannulatum	k1gNnSc1	nigroannulatum
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
žádný	žádný	k3yNgInSc1	žádný
jiný	jiný	k2eAgInSc1d1	jiný
sociální	sociální	k2eAgInSc1d1	sociální
druh	druh	k1gInSc1	druh
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
kolonie	kolonie	k1gFnSc1	kolonie
až	až	k9	až
o	o	k7c6	o
několika	několik	k4yIc6	několik
tisících	tisící	k4xOgMnPc6	tisící
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
sdílejí	sdílet	k5eAaImIp3nP	sdílet
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
sociální	sociální	k2eAgMnPc4d1	sociální
pavouky	pavouk	k1gMnPc4	pavouk
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
Philoponella	Philoponell	k1gMnSc2	Philoponell
(	(	kIx(	(
<g/>
čeleď	čeleď	k1gFnSc1	čeleď
pakřižákovití	pakřižákovitý	k2eAgMnPc1d1	pakřižákovitý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokoutník	pokoutník	k1gMnSc1	pokoutník
společenský	společenský	k2eAgMnSc1d1	společenský
(	(	kIx(	(
<g/>
Agelena	Agelen	k2eAgFnSc1d1	Agelen
consociata	consociata	k1gFnSc1	consociata
<g/>
,	,	kIx,	,
čeleď	čeleď	k1gFnSc1	čeleď
pokoutníkovití	pokoutníkovitý	k2eAgMnPc1d1	pokoutníkovitý
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mallos	Mallos	k1gMnSc1	Mallos
gregalis	gregalis	k1gFnSc2	gregalis
(	(	kIx(	(
<g/>
čeleď	čeleď	k1gFnSc1	čeleď
cedivečkovití	cedivečkovitý	k2eAgMnPc1d1	cedivečkovitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Draví	dravý	k2eAgMnPc1d1	dravý
sociální	sociální	k2eAgMnPc1d1	sociální
pavouci	pavouk	k1gMnPc1	pavouk
musí	muset	k5eAaImIp3nP	muset
chránit	chránit	k5eAaImF	chránit
svou	svůj	k3xOyFgFnSc4	svůj
potravu	potrava	k1gFnSc4	potrava
před	před	k7c7	před
kleptoparazity	kleptoparazit	k1gInPc7	kleptoparazit
a	a	k8xC	a
větší	veliký	k2eAgFnPc1d2	veliký
kolonie	kolonie	k1gFnPc1	kolonie
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
plnění	plnění	k1gNnSc4	plnění
tohoto	tento	k3xDgInSc2	tento
úkolu	úkol	k1gInSc2	úkol
úspěšnější	úspěšný	k2eAgNnPc1d2	úspěšnější
<g/>
.	.	kIx.	.
</s>
<s>
Býložravý	býložravý	k2eAgMnSc1d1	býložravý
pavouk	pavouk	k1gMnSc1	pavouk
Bagheera	Bagheero	k1gNnSc2	Bagheero
kiplingi	kiplingi	k1gNnSc2	kiplingi
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
chrání	chránit	k5eAaImIp3nP	chránit
vajíčka	vajíčko	k1gNnPc4	vajíčko
a	a	k8xC	a
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
snovačky	snovačka	k1gFnPc1	snovačka
(	(	kIx(	(
<g/>
Latrodectus	Latrodectus	k1gInSc1	Latrodectus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
kanibalismem	kanibalismus	k1gInSc7	kanibalismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
malé	malý	k2eAgFnPc1d1	malá
kolonie	kolonie	k1gFnPc1	kolonie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
jedinci	jedinec	k1gMnPc1	jedinec
sdílí	sdílet	k5eAaImIp3nP	sdílet
pavučiny	pavučina	k1gFnPc4	pavučina
a	a	k8xC	a
společně	společně	k6eAd1	společně
žerou	žrát	k5eAaImIp3nP	žrát
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pavučina	pavučina	k1gFnSc1	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc4	žádný
pevný	pevný	k2eAgInSc4d1	pevný
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
klasifikací	klasifikace	k1gFnSc7	klasifikace
pavouků	pavouk	k1gMnPc2	pavouk
a	a	k8xC	a
typem	typ	k1gInSc7	typ
pavučin	pavučina	k1gFnPc2	pavučina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
<g/>
:	:	kIx,	:
druhy	druh	k1gInPc1	druh
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
rodu	rod	k1gInSc6	rod
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnPc4d1	podobná
či	či	k8xC	či
výrazně	výrazně	k6eAd1	výrazně
odlišné	odlišný	k2eAgFnPc1d1	odlišná
pavučiny	pavučina	k1gFnPc1	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
o	o	k7c6	o
chemickém	chemický	k2eAgNnSc6d1	chemické
složení	složení	k1gNnSc6	složení
pavoučích	pavoučí	k2eAgNnPc2d1	pavoučí
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemně	vzájemně	k6eAd1	vzájemně
podobné	podobný	k2eAgFnPc1d1	podobná
techniky	technika	k1gFnPc1	technika
vytváření	vytváření	k1gNnSc2	vytváření
pavučin	pavučina	k1gFnPc2	pavučina
nejčastěji	často	k6eAd3	často
vznikají	vznikat	k5eAaImIp3nP	vznikat
konvergencí	konvergence	k1gFnSc7	konvergence
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pavouků	pavouk	k1gMnPc2	pavouk
snová	snovat	k5eAaImIp3nS	snovat
nekruhové	kruhový	k2eNgFnPc4d1	nekruhová
pavučiny	pavučina	k1gFnPc4	pavučina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kruhové	kruhový	k2eAgFnPc1d1	kruhová
pavučiny	pavučina	k1gFnPc1	pavučina
jsou	být	k5eAaImIp3nP	být
evolučně	evolučně	k6eAd1	evolučně
starší	starý	k2eAgFnPc1d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
potenciální	potenciální	k2eAgFnSc2d1	potenciální
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zachytí	zachytit	k5eAaPmIp3nS	zachytit
do	do	k7c2	do
kruhové	kruhový	k2eAgFnSc2d1	kruhová
pavučiny	pavučina	k1gFnSc2	pavučina
<g/>
,	,	kIx,	,
unikne	uniknout	k5eAaPmIp3nS	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Pavučina	pavučina	k1gFnSc1	pavučina
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
funkce	funkce	k1gFnPc4	funkce
<g/>
:	:	kIx,	:
zachycení	zachycení	k1gNnSc4	zachycení
kořisti	kořist	k1gFnSc2	kořist
<g/>
;	;	kIx,	;
pohlcení	pohlcení	k1gNnSc3	pohlcení
její	její	k3xOp3gFnSc2	její
hybnosti	hybnost	k1gFnSc2	hybnost
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
roztrhla	roztrhnout	k5eAaPmAgFnS	roztrhnout
<g/>
;	;	kIx,	;
a	a	k8xC	a
zadržení	zadržení	k1gNnSc4	zadržení
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
ideální	ideální	k2eAgFnSc1d1	ideální
struktura	struktura	k1gFnSc1	struktura
pavučiny	pavučina	k1gFnSc2	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
mezery	mezera	k1gFnPc1	mezera
mezi	mezi	k7c4	mezi
vlákny	vlákna	k1gFnPc4	vlákna
například	například	k6eAd1	například
zvětšují	zvětšovat	k5eAaImIp3nP	zvětšovat
plochu	plocha	k1gFnSc4	plocha
pavučiny	pavučina	k1gFnSc2	pavučina
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
zachycení	zachycení	k1gNnSc4	zachycení
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ale	ale	k8xC	ale
snižují	snižovat	k5eAaImIp3nP	snižovat
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
zadržení	zadržení	k1gNnSc4	zadržení
<g/>
;	;	kIx,	;
menší	malý	k2eAgFnPc1d2	menší
mezery	mezera	k1gFnPc1	mezera
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnPc1d2	veliký
lepkavé	lepkavý	k2eAgFnPc1d1	lepkavá
kapky	kapka	k1gFnPc1	kapka
a	a	k8xC	a
silnější	silný	k2eAgFnSc1d2	silnější
vlákna	vlákna	k1gFnSc1	vlákna
zase	zase	k9	zase
kořist	kořist	k1gFnSc1	kořist
lépe	dobře	k6eAd2	dobře
zadrží	zadržet	k5eAaPmIp3nS	zadržet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
spíše	spíše	k9	spíše
pavučiny	pavučina	k1gFnSc2	pavučina
všimne	všimnout	k5eAaPmIp3nS	všimnout
a	a	k8xC	a
vyhne	vyhnout	k5eAaPmIp3nS	vyhnout
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgInPc1	žádný
pevné	pevný	k2eAgInPc1d1	pevný
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
kruhovými	kruhový	k2eAgFnPc7d1	kruhová
pavučinami	pavučina	k1gFnPc7	pavučina
používanými	používaný	k2eAgFnPc7d1	používaná
přes	přes	k7c4	přes
den	den	k1gInSc4	den
a	a	k8xC	a
těmi	ten	k3xDgFnPc7	ten
používanými	používaný	k2eAgFnPc7d1	používaná
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pavouci	pavouk	k1gMnPc1	pavouk
číhají	číhat	k5eAaImIp3nP	číhat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
středem	střed	k1gInSc7	střed
pavučiny	pavučina	k1gFnPc1	pavučina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pavouci	pavouk	k1gMnPc1	pavouk
se	se	k3xPyFc4	se
dolů	dolů	k6eAd1	dolů
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
nějaký	nějaký	k3yIgInSc4	nějaký
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
pavouk	pavouk	k1gMnSc1	pavouk
může	moct	k5eAaImIp3nS	moct
uniknout	uniknout	k5eAaPmF	uniknout
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
před	před	k7c7	před
svými	svůj	k3xOyFgMnPc7	svůj
predátory	predátor	k1gMnPc7	predátor
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
vyčkávací	vyčkávací	k2eAgNnSc1d1	vyčkávací
místo	místo	k1gNnSc1	místo
pavouka	pavouk	k1gMnSc2	pavouk
vychýleno	vychýlen	k2eAgNnSc1d1	vychýleno
tímto	tento	k3xDgInSc7	tento
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
běžné	běžný	k2eAgFnPc1d1	běžná
jsou	být	k5eAaImIp3nP	být
horizontální	horizontální	k2eAgFnPc1d1	horizontální
sítě	síť	k1gFnPc1	síť
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
efektivní	efektivní	k2eAgFnPc1d1	efektivní
při	při	k7c6	při
zachycování	zachycování	k1gNnSc6	zachycování
a	a	k8xC	a
zadržování	zadržování	k1gNnSc6	zadržování
kořisti	kořist	k1gFnSc2	kořist
a	a	k8xC	a
více	hodně	k6eAd2	hodně
náchylné	náchylný	k2eAgFnPc1d1	náchylná
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
deštěm	dešť	k1gInSc7	dešť
a	a	k8xC	a
padajícími	padající	k2eAgInPc7d1	padající
objekty	objekt	k1gInPc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
některé	některý	k3yIgFnPc4	některý
výhody	výhoda	k1gFnPc4	výhoda
<g/>
:	:	kIx,	:
nejsou	být	k5eNaImIp3nP	být
tolik	tolik	k6eAd1	tolik
poškozovány	poškozován	k2eAgFnPc1d1	poškozována
větrem	vítr	k1gInSc7	vítr
<g/>
;	;	kIx,	;
kořist	kořist	k1gFnSc1	kořist
letící	letící	k2eAgNnSc4d1	letící
vzhůru	vzhůru	k6eAd1	vzhůru
je	on	k3xPp3gNnSc4	on
nevidí	vidět	k5eNaImIp3nS	vidět
tak	tak	k9	tak
dobře	dobře	k6eAd1	dobře
kvůli	kvůli	k7c3	kvůli
slunci	slunce	k1gNnSc3	slunce
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
;	;	kIx,	;
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
rozkmitat	rozkmitat	k5eAaPmF	rozkmitat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
chytily	chytit	k5eAaPmAgInP	chytit
hmyz	hmyz	k1gInSc4	hmyz
v	v	k7c6	v
pomalém	pomalý	k2eAgInSc6d1	pomalý
horizontálním	horizontální	k2eAgInSc6d1	horizontální
letu	let	k1gInSc6	let
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
často	často	k6eAd1	často
na	na	k7c4	na
pavučiny	pavučina	k1gFnPc4	pavučina
přidělávají	přidělávat	k5eAaImIp3nP	přidělávat
silné	silný	k2eAgInPc1d1	silný
pásky	pásek	k1gInPc1	pásek
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
stabilimenta	stabilimenta	k1gFnSc1	stabilimenta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výzkumníků	výzkumník	k1gMnPc2	výzkumník
se	se	k3xPyFc4	se
v	v	k7c6	v
takto	takto	k6eAd1	takto
ozdobených	ozdobený	k2eAgFnPc6d1	ozdobená
pavučinách	pavučina	k1gFnPc6	pavučina
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
zachytí	zachytit	k5eAaPmIp3nP	zachytit
více	hodně	k6eAd2	hodně
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ukázala	ukázat	k5eAaPmAgFnS	ukázat
laboratorní	laboratorní	k2eAgFnSc1d1	laboratorní
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
když	když	k8xS	když
pavouci	pavouk	k1gMnPc1	pavouk
cítí	cítit	k5eAaImIp3nP	cítit
přítomnost	přítomnost	k1gFnSc4	přítomnost
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
vytváření	vytváření	k1gNnSc4	vytváření
stabiliment	stabiliment	k1gInSc4	stabiliment
omezují	omezovat	k5eAaImIp3nP	omezovat
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
neobvyklých	obvyklý	k2eNgFnPc2d1	neobvyklá
variant	varianta	k1gFnPc2	varianta
kruhových	kruhový	k2eAgFnPc2d1	kruhová
pavučin	pavučina	k1gFnPc2	pavučina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
mnohé	mnohé	k1gNnSc4	mnohé
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
konvergentně	konvergentně	k6eAd1	konvergentně
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
například	například	k6eAd1	například
pavučiny	pavučina	k1gFnSc2	pavučina
připevněné	připevněný	k2eAgFnSc2d1	připevněná
k	k	k7c3	k
vodní	vodní	k2eAgFnSc3d1	vodní
hladině	hladina	k1gFnSc3	hladina
<g/>
,	,	kIx,	,
možná	možná	k9	možná
kvůli	kvůli	k7c3	kvůli
chytání	chytání	k1gNnSc3	chytání
hmyzu	hmyz	k1gInSc2	hmyz
plovoucího	plovoucí	k2eAgInSc2d1	plovoucí
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
;	;	kIx,	;
pavučiny	pavučina	k1gFnPc1	pavučina
s	s	k7c7	s
větvičkami	větvička	k1gFnPc7	větvička
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
pavouk	pavouk	k1gMnSc1	pavouk
může	moct	k5eAaImIp3nS	moct
schovat	schovat	k5eAaPmF	schovat
před	před	k7c7	před
predátory	predátor	k1gMnPc7	predátor
<g/>
;	;	kIx,	;
či	či	k8xC	či
pavučiny	pavučina	k1gFnPc1	pavučina
podobné	podobný	k2eAgFnPc1d1	podobná
žebříku	žebřík	k1gInSc2	žebřík
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
vhodné	vhodný	k2eAgInPc1d1	vhodný
na	na	k7c4	na
chytání	chytání	k1gNnSc4	chytání
můr	můra	k1gFnPc2	můra
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
mnoha	mnoho	k4c2	mnoho
variací	variace	k1gFnPc2	variace
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
vzala	vzít	k5eAaPmAgFnS	vzít
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
Skylab	Skylab	k1gInSc1	Skylab
3	[number]	k4	3
pavouky	pavouk	k1gMnPc7	pavouk
vytvářející	vytvářející	k2eAgFnSc2d1	vytvářející
kruhové	kruhový	k2eAgFnSc2d1	kruhová
sítě	síť	k1gFnSc2	síť
do	do	k7c2	do
kosmického	kosmický	k2eAgInSc2d1	kosmický
prostoru	prostor	k1gInSc2	prostor
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
otestování	otestování	k1gNnSc2	otestování
snovacích	snovací	k2eAgFnPc2d1	snovací
dovedností	dovednost	k1gFnPc2	dovednost
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
beztíže	beztíže	k1gFnSc2	beztíže
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
pavouci	pavouk	k1gMnPc1	pavouk
snovali	snovat	k5eAaImAgMnP	snovat
nekvalitní	kvalitní	k2eNgFnPc4d1	nekvalitní
pavučiny	pavučina	k1gFnPc4	pavučina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
novému	nový	k2eAgNnSc3d1	nové
prostředí	prostředí	k1gNnSc2	prostředí
přizpůsobili	přizpůsobit	k5eAaPmAgMnP	přizpůsobit
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
snovačkovití	snovačkovitý	k2eAgMnPc1d1	snovačkovitý
snovají	snovat	k5eAaImIp3nP	snovat
nepravidelné	pravidelný	k2eNgFnPc1d1	nepravidelná
<g/>
,	,	kIx,	,
propletené	propletený	k2eAgFnPc1d1	propletená
trojrozměrné	trojrozměrný	k2eAgFnPc1d1	trojrozměrná
pavučiny	pavučina	k1gFnPc1	pavučina
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
také	také	k6eAd1	také
říká	říkat	k5eAaImIp3nS	říkat
pavučinové	pavučinový	k2eAgInPc4d1	pavučinový
vaky	vak	k1gInPc4	vak
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
evoluční	evoluční	k2eAgInSc4d1	evoluční
trend	trend	k1gInSc4	trend
směřující	směřující	k2eAgInSc4d1	směřující
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
použitých	použitý	k2eAgNnPc2d1	Použité
lepkavých	lepkavý	k2eAgNnPc2d1	lepkavé
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
až	až	k9	až
v	v	k7c4	v
jejich	jejich	k3xOp3gFnSc4	jejich
úplnou	úplný	k2eAgFnSc4d1	úplná
absenci	absence	k1gFnSc4	absence
<g/>
.	.	kIx.	.
</s>
<s>
Snování	snování	k1gNnSc1	snování
pavučinových	pavučinový	k2eAgInPc2d1	pavučinový
vaků	vak	k1gInPc2	vak
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
stereotypní	stereotypní	k2eAgNnSc1d1	stereotypní
než	než	k8xS	než
snování	snování	k1gNnSc1	snování
kruhových	kruhový	k2eAgFnPc2d1	kruhová
pavučin	pavučina	k1gFnPc2	pavučina
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
až	až	k9	až
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Plachetnatkovití	Plachetnatkovitý	k2eAgMnPc1d1	Plachetnatkovitý
(	(	kIx(	(
<g/>
Linyphiidae	Linyphiidae	k1gNnSc7	Linyphiidae
<g/>
)	)	kIx)	)
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
horizontální	horizontální	k2eAgFnPc4d1	horizontální
hrbolaté	hrbolatý	k2eAgFnPc4d1	hrbolatá
"	"	kIx"	"
<g/>
plachetky	plachetka	k1gFnPc4	plachetka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterými	který	k3yIgFnPc7	který
jsou	být	k5eAaImIp3nP	být
smotaná	smotaný	k2eAgNnPc1d1	smotané
zastavovací	zastavovací	k2eAgNnPc1d1	zastavovací
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
do	do	k7c2	do
těchto	tento	k3xDgNnPc2	tento
vláken	vlákno	k1gNnPc2	vlákno
narazí	narazit	k5eAaPmIp3nS	narazit
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
spadne	spadnout	k5eAaPmIp3nS	spadnout
do	do	k7c2	do
plachetky	plachetka	k1gFnSc2	plachetka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zachytí	zachytit	k5eAaPmIp3nP	zachytit
v	v	k7c6	v
lepkavých	lepkavý	k2eAgNnPc6d1	lepkavé
vláknech	vlákno	k1gNnPc6	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Pavouk	pavouk	k1gMnSc1	pavouk
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
potom	potom	k6eAd1	potom
zaútočí	zaútočit	k5eAaPmIp3nP	zaútočit
zespoda	zespoda	k7c2	zespoda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
fosilní	fosilní	k2eAgInSc1d1	fosilní
záznam	záznam	k1gInSc1	záznam
pavouků	pavouk	k1gMnPc2	pavouk
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
slabý	slabý	k2eAgInSc4d1	slabý
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
fosilií	fosilie	k1gFnPc2	fosilie
popsáno	popsat	k5eAaPmNgNnS	popsat
téměř	téměř	k6eAd1	téměř
1000	[number]	k4	1000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
těla	tělo	k1gNnPc1	tělo
pavouků	pavouk	k1gMnPc2	pavouk
jsou	být	k5eAaImIp3nP	být
křehká	křehký	k2eAgFnSc1d1	křehká
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
fosilizovaných	fosilizovaný	k2eAgMnPc2d1	fosilizovaný
pavouků	pavouk	k1gMnPc2	pavouk
byla	být	k5eAaImAgFnS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
v	v	k7c6	v
jantaru	jantar	k1gInSc6	jantar
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
známá	známý	k2eAgFnSc1d1	známá
fosilie	fosilie	k1gFnSc1	fosilie
členovce	členovec	k1gMnSc2	členovec
v	v	k7c6	v
jantaru	jantar	k1gInSc6	jantar
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rané	raný	k2eAgFnSc2d1	raná
křídy	křída	k1gFnSc2	křída
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
130	[number]	k4	130
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Fosilie	fosilie	k1gFnPc1	fosilie
neslouží	sloužit	k5eNaImIp3nP	sloužit
pouze	pouze	k6eAd1	pouze
ke	k	k7c3	k
zkoumání	zkoumání	k1gNnSc3	zkoumání
anatomie	anatomie	k1gFnSc2	anatomie
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jantaru	jantar	k1gInSc6	jantar
byli	být	k5eAaImAgMnP	být
zachyceni	zachytit	k5eAaPmNgMnP	zachytit
také	také	k9	také
pavouci	pavouk	k1gMnPc1	pavouk
při	při	k7c6	při
páření	páření	k1gNnSc6	páření
<g/>
,	,	kIx,	,
lovení	lovení	k1gNnSc6	lovení
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
,	,	kIx,	,
snování	snování	k1gNnSc1	snování
vláken	vlákno	k1gNnPc2	vlákno
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
při	při	k7c6	při
péči	péče	k1gFnSc6	péče
o	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Zachovaly	zachovat	k5eAaPmAgInP	zachovat
se	se	k3xPyFc4	se
i	i	k9	i
zkameněliny	zkamenělina	k1gFnSc2	zkamenělina
kokonů	kokon	k1gInPc2	kokon
s	s	k7c7	s
vajíčky	vajíčko	k1gNnPc7	vajíčko
a	a	k8xC	a
pavučin	pavučina	k1gFnPc2	pavučina
se	s	k7c7	s
zachycenou	zachycený	k2eAgFnSc7d1	zachycená
kořistí	kořist	k1gFnSc7	kořist
<g/>
;	;	kIx,	;
nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
fosilní	fosilní	k2eAgInSc1d1	fosilní
záznam	záznam	k1gInSc1	záznam
pavučiny	pavučina	k1gFnSc2	pavučina
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgMnSc1d1	starý
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
známý	známý	k2eAgMnSc1d1	známý
pavoukovec	pavoukovec	k1gMnSc1	pavoukovec
je	být	k5eAaImIp3nS	být
Palaeotarbus	Palaeotarbus	k1gInSc4	Palaeotarbus
jerami	jera	k1gFnPc7	jera
z	z	k7c2	z
období	období	k1gNnSc2	období
siluru	silur	k1gInSc2	silur
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
asi	asi	k9	asi
před	před	k7c7	před
420	[number]	k4	420
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
trojúhelníkovou	trojúhelníkový	k2eAgFnSc4d1	trojúhelníková
hlavohruď	hlavohruď	k1gFnSc4	hlavohruď
<g/>
,	,	kIx,	,
členitý	členitý	k2eAgInSc4d1	členitý
zadeček	zadeček	k1gInSc4	zadeček
<g/>
,	,	kIx,	,
osm	osm	k4xCc1	osm
nohou	noha	k1gFnPc2	noha
a	a	k8xC	a
makadla	makadlo	k1gNnSc2	makadlo
<g/>
.	.	kIx.	.
</s>
<s>
Attercopus	Attercopus	k1gInSc1	Attercopus
fimbriunguis	fimbriunguis	k1gFnSc2	fimbriunguis
z	z	k7c2	z
devonu	devon	k1gInSc2	devon
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
asi	asi	k9	asi
před	před	k7c7	před
386	[number]	k4	386
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
má	mít	k5eAaImIp3nS	mít
nejstarší	starý	k2eAgInPc4d3	nejstarší
známé	známý	k2eAgInPc4d1	známý
spigoty	spigot	k1gInPc4	spigot
vytvářející	vytvářející	k2eAgFnSc1d1	vytvářející
pavučinová	pavučinový	k2eAgFnSc1d1	pavučinová
vlákna	vlákna	k1gFnSc1	vlákna
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
tudíž	tudíž	k8xC	tudíž
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c4	za
pavouka	pavouk	k1gMnSc4	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
spigoty	spigot	k1gInPc1	spigot
se	se	k3xPyFc4	se
ale	ale	k9	ale
možná	možná	k9	možná
nacházely	nacházet	k5eAaImAgFnP	nacházet
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
zadečku	zadeček	k1gInSc2	zadeček
namísto	namísto	k7c2	namísto
na	na	k7c6	na
snovacích	snovací	k2eAgFnPc6d1	snovací
bradavkách	bradavka	k1gFnPc6	bradavka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
při	při	k7c6	při
stavění	stavění	k1gNnSc6	stavění
pavučin	pavučina	k1gFnPc2	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Attercopus	Attercopus	k1gInSc4	Attercopus
a	a	k8xC	a
jemu	on	k3xPp3gNnSc3	on
podobný	podobný	k2eAgInSc4d1	podobný
permský	permský	k2eAgInSc4d1	permský
pavoukovec	pavoukovec	k1gInSc4	pavoukovec
Permarachne	Permarachne	k1gFnSc2	Permarachne
možná	možná	k9	možná
nebyli	být	k5eNaImAgMnP	být
pravými	pravý	k2eAgMnPc7d1	pravý
pavouky	pavouk	k1gMnPc7	pavouk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pavučinová	pavučinový	k2eAgNnPc1d1	pavučinové
vlákna	vlákno	k1gNnPc1	vlákno
využívali	využívat	k5eAaPmAgMnP	využívat
spíše	spíše	k9	spíše
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
hnízd	hnízdo	k1gNnPc2	hnízdo
a	a	k8xC	a
kokonů	kokon	k1gInPc2	kokon
než	než	k8xS	než
na	na	k7c4	na
vytváření	vytváření	k1gNnSc4	vytváření
pavučin	pavučina	k1gFnPc2	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
známým	známý	k2eAgMnSc7d1	známý
fosilním	fosilní	k2eAgMnSc7d1	fosilní
pavoukem	pavouk	k1gMnSc7	pavouk
je	být	k5eAaImIp3nS	být
Nephila	Nephila	k1gFnSc1	Nephila
jurassica	jurassica	k1gFnSc1	jurassica
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
asi	asi	k9	asi
před	před	k7c7	před
165	[number]	k4	165
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
ve	v	k7c6	v
Vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
Mongolsku	Mongolsko	k1gNnSc6	Mongolsko
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
téměř	téměř	k6eAd1	téměř
25	[number]	k4	25
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
pavouků	pavouk	k1gMnPc2	pavouk
z	z	k7c2	z
karbonu	karbon	k1gInSc2	karbon
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
primitivního	primitivní	k2eAgInSc2d1	primitivní
podřádu	podřád	k1gInSc2	podřád
sklípkoši	sklípkoš	k1gMnSc3	sklípkoš
(	(	kIx(	(
<g/>
Mesothelae	Mesothela	k1gFnSc2	Mesothela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
přežila	přežít	k5eAaPmAgFnS	přežít
pouze	pouze	k6eAd1	pouze
čeleď	čeleď	k1gFnSc1	čeleď
sklípkošovití	sklípkošovití	k1gMnPc5	sklípkošovití
(	(	kIx(	(
<g/>
Liphistiidae	Liphistiidae	k1gFnSc1	Liphistiidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
období	období	k1gNnSc6	období
permu	perm	k1gInSc2	perm
před	před	k7c7	před
299	[number]	k4	299
až	až	k8xS	až
251	[number]	k4	251
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
probíhala	probíhat	k5eAaImAgFnS	probíhat
rychlá	rychlý	k2eAgFnSc1d1	rychlá
diverzifikace	diverzifikace	k1gFnSc1	diverzifikace
létajícího	létající	k2eAgInSc2d1	létající
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
fosilií	fosilie	k1gFnPc2	fosilie
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
skupiny	skupina	k1gFnPc1	skupina
moderních	moderní	k2eAgMnPc2d1	moderní
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
sklípkani	sklípkan	k1gMnPc5	sklípkan
(	(	kIx(	(
<g/>
Mygalomorphae	Mygalomorphae	k1gNnSc4	Mygalomorphae
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvouplicní	dvouplicní	k2eAgFnSc4d1	dvouplicní
(	(	kIx(	(
<g/>
Araneomorphae	Araneomorphae	k1gFnSc4	Araneomorphae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
triasu	trias	k1gInSc6	trias
před	před	k7c7	před
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
pavouků	pavouk	k1gMnPc2	pavouk
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
být	být	k5eAaImF	být
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
sklípkancovití	sklípkancovitý	k2eAgMnPc1d1	sklípkancovitý
(	(	kIx(	(
<g/>
Hexathelidae	Hexathelidae	k1gNnSc7	Hexathelidae
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
snovací	snovací	k2eAgFnPc1d1	snovací
bradavky	bradavka	k1gFnPc1	bradavka
byly	být	k5eAaImAgFnP	být
zřejmě	zřejmě	k6eAd1	zřejmě
uzpůsobené	uzpůsobený	k2eAgFnPc1d1	uzpůsobená
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
pavučin	pavučina	k1gFnPc2	pavučina
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
trychtýřů	trychtýř	k1gInPc2	trychtýř
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
současné	současný	k2eAgMnPc4d1	současný
zástupce	zástupce	k1gMnPc4	zástupce
této	tento	k3xDgFnSc2	tento
čeledi	čeleď	k1gFnSc2	čeleď
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
známý	známý	k2eAgInSc1d1	známý
sklípkanec	sklípkanec	k1gInSc1	sklípkanec
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
(	(	kIx(	(
<g/>
Atrax	Atrax	k1gInSc1	Atrax
robustus	robustus	k1gInSc1	robustus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvouplicní	dvouplicní	k2eAgFnPc1d1	dvouplicní
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
velkou	velký	k2eAgFnSc4d1	velká
většinu	většina	k1gFnSc4	většina
současných	současný	k2eAgMnPc2d1	současný
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
než	než	k8xS	než
patří	patřit	k5eAaImIp3nP	patřit
pavouci	pavouk	k1gMnPc1	pavouk
snovající	snovající	k2eAgFnSc2d1	snovající
kruhové	kruhový	k2eAgFnSc2d1	kruhová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jury	jura	k1gFnSc2	jura
a	a	k8xC	a
křídy	křída	k1gFnSc2	křída
se	se	k3xPyFc4	se
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
fosilizovaných	fosilizovaný	k2eAgMnPc2d1	fosilizovaný
pavouků	pavouk	k1gMnPc2	pavouk
včetně	včetně	k7c2	včetně
zástupců	zástupce	k1gMnPc2	zástupce
mnoha	mnoho	k4c2	mnoho
současných	současný	k2eAgFnPc2d1	současná
čeledí	čeleď	k1gFnPc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
čeledí	čeleď	k1gFnPc2	čeleď
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
jsou	být	k5eAaImIp3nP	být
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
podřádů	podřád	k1gInPc2	podřád
<g/>
,	,	kIx,	,
sklípkoši	sklípkoš	k1gMnPc1	sklípkoš
a	a	k8xC	a
Opisthothelae	Opisthothela	k1gFnPc1	Opisthothela
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Opisthothelae	Opisthothelae	k1gInSc1	Opisthothelae
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
dva	dva	k4xCgInPc4	dva
infrařády	infrařád	k1gInPc4	infrařád
<g/>
:	:	kIx,	:
sklípkani	sklípkan	k1gMnPc1	sklípkan
a	a	k8xC	a
dvouplicní	dvouplicní	k2eAgMnPc1d1	dvouplicní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
přes	přes	k7c4	přes
40	[number]	k4	40
000	[number]	k4	000
žijících	žijící	k2eAgInPc2d1	žijící
druhů	druh	k1gInPc2	druh
pavouků	pavouk	k1gMnPc2	pavouk
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
arachnologové	arachnolog	k1gMnPc1	arachnolog
rozřazují	rozřazovat	k5eAaImIp3nP	rozřazovat
do	do	k7c2	do
zhruba	zhruba	k6eAd1	zhruba
110	[number]	k4	110
čeledí	čeleď	k1gFnPc2	čeleď
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
3700	[number]	k4	3700
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Jediní	jediný	k2eAgMnPc1d1	jediný
žijící	žijící	k2eAgMnPc1d1	žijící
zástupci	zástupce	k1gMnPc1	zástupce
z	z	k7c2	z
podřádu	podřád	k1gInSc2	podřád
sklípkoši	sklípkoš	k1gMnPc1	sklípkoš
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
sklípkošovití	sklípkošovití	k1gMnPc1	sklípkošovití
a	a	k8xC	a
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Čínské	čínský	k2eAgFnSc3d1	čínská
lidové	lidový	k2eAgFnSc3d1	lidová
republice	republika	k1gFnSc3	republika
a	a	k8xC	a
Japonsku	Japonsko	k1gNnSc3	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
sklípkošů	sklípkoš	k1gMnPc2	sklípkoš
si	se	k3xPyFc3	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
doupata	doupě	k1gNnPc4	doupě
s	s	k7c7	s
poklopem	poklop	k1gInSc7	poklop
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
pavoučími	pavoučí	k2eAgNnPc7d1	pavoučí
vlákny	vlákno	k1gNnPc7	vlákno
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
někteří	některý	k3yIgMnPc1	některý
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Liphistius	Liphistius	k1gInSc1	Liphistius
stavějí	stavět	k5eAaImIp3nP	stavět
zakryté	zakrytý	k2eAgFnPc4d1	zakrytá
pavučinové	pavučinový	k2eAgFnPc4d1	pavučinová
trubice	trubice	k1gFnPc4	trubice
s	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
poklopem	poklop	k1gInSc7	poklop
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
můžou	můžou	k?	můžou
uniknout	uniknout	k5eAaPmF	uniknout
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nouze	nouze	k1gFnSc2	nouze
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Liphistius	Liphistius	k1gInSc1	Liphistius
natahují	natahovat	k5eAaImIp3nP	natahovat
vlákna	vlákno	k1gNnPc1	vlákno
mimo	mimo	k7c4	mimo
tunely	tunel	k1gInPc4	tunel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
lépe	dobře	k6eAd2	dobře
dokázali	dokázat	k5eAaPmAgMnP	dokázat
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
blížící	blížící	k2eAgFnSc4d1	blížící
se	se	k3xPyFc4	se
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
Heptathela	Heptathel	k1gMnSc2	Heptathel
se	se	k3xPyFc4	se
spoléhají	spoléhat	k5eAaImIp3nP	spoléhat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
senzory	senzor	k1gInPc4	senzor
vibrací	vibrace	k1gFnPc2	vibrace
a	a	k8xC	a
vlákna	vlákno	k1gNnSc2	vlákno
nenatahují	natahovat	k5eNaImIp3nP	natahovat
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Heptathela	Heptathel	k1gMnSc2	Heptathel
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgNnSc1	žádný
jedové	jedový	k2eAgFnPc1d1	jedová
žlázy	žláza	k1gFnPc1	žláza
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
drápcích	drápec	k1gInPc6	drápec
mají	mít	k5eAaImIp3nP	mít
jejich	jejich	k3xOp3gInPc1	jejich
vývody	vývod	k1gInPc1	vývod
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sklípkani	sklípkan	k1gMnPc1	sklípkan
<g/>
.	.	kIx.	.
</s>
<s>
Sklípkani	sklípkan	k1gMnPc1	sklípkan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
triasu	trias	k1gInSc6	trias
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
mohutní	mohutný	k2eAgMnPc1d1	mohutný
a	a	k8xC	a
chlupatí	chlupatý	k2eAgMnPc1d1	chlupatý
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
velké	velký	k2eAgFnPc4d1	velká
silné	silný	k2eAgFnPc4d1	silná
chelicery	chelicera	k1gFnPc4	chelicera
a	a	k8xC	a
drápky	drápek	k1gInPc4	drápek
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
tráví	trávit	k5eAaImIp3nP	trávit
většinu	většina	k1gFnSc4	většina
života	život	k1gInSc2	život
v	v	k7c6	v
doupatech	doupě	k1gNnPc6	doupě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někteří	některý	k3yIgMnPc1	některý
snovají	snovat	k5eAaImIp3nP	snovat
pavučiny	pavučina	k1gFnPc4	pavučina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dvouplicních	dvouplicní	k2eAgInPc2d1	dvouplicní
ale	ale	k8xC	ale
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
produkovat	produkovat	k5eAaImF	produkovat
takový	takový	k3xDgInSc4	takový
druh	druh	k1gInSc4	druh
pavoučího	pavoučí	k2eAgNnSc2d1	pavoučí
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
lepí	lepit	k5eAaImIp3nS	lepit
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
vlákna	vlákno	k1gNnPc4	vlákno
či	či	k8xC	či
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
povrchy	povrch	k1gInPc4	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
jim	on	k3xPp3gFnPc3	on
vytváření	vytváření	k1gNnSc3	vytváření
pavučiny	pavučina	k1gFnSc2	pavučina
ztěžuje	ztěžovat	k5eAaImIp3nS	ztěžovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
členovců	členovec	k1gMnPc2	členovec
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
také	také	k9	také
žábami	žába	k1gFnPc7	žába
<g/>
,	,	kIx,	,
ještěrkami	ještěrka	k1gFnPc7	ještěrka
a	a	k8xC	a
šneky	šnek	k1gInPc7	šnek
<g/>
.	.	kIx.	.
</s>
<s>
Dvouplicní	dvouplicní	k2eAgInPc1d1	dvouplicní
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
pavoučích	pavoučí	k2eAgInPc2d1	pavoučí
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
například	například	k6eAd1	například
křižákovití	křižákovitý	k2eAgMnPc1d1	křižákovitý
<g/>
,	,	kIx,	,
slíďákovití	slíďákovitý	k2eAgMnPc1d1	slíďákovitý
a	a	k8xC	a
skákavkovití	skákavkovitý	k2eAgMnPc1d1	skákavkovitý
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
i	i	k9	i
jediný	jediný	k2eAgMnSc1d1	jediný
známý	známý	k2eAgMnSc1d1	známý
býložravý	býložravý	k2eAgMnSc1d1	býložravý
pavouk	pavouk	k1gMnSc1	pavouk
<g/>
,	,	kIx,	,
Bagheera	Bagheera	k1gFnSc1	Bagheera
kiplingi	kipling	k1gFnSc2	kipling
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	s	k7c7	s
drápky	drápek	k1gInPc7	drápek
orientovanými	orientovaný	k2eAgInPc7d1	orientovaný
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
sklípkanů	sklípkan	k1gMnPc2	sklípkan
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
drápky	drápek	k1gInPc1	drápek
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
pavouků	pavouk	k1gMnPc2	pavouk
napadá	napadat	k5eAaBmIp3nS	napadat
lidi	člověk	k1gMnPc4	člověk
jen	jen	k9	jen
při	při	k7c6	při
sebeobraně	sebeobrana	k1gFnSc6	sebeobrana
a	a	k8xC	a
jen	jen	k9	jen
málo	málo	k4c1	málo
kousnutí	kousnutí	k1gNnPc2	kousnutí
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
horší	zlý	k2eAgInPc1d2	horší
následky	následek	k1gInPc1	následek
než	než	k8xS	než
bodnutí	bodnutí	k1gNnSc1	bodnutí
komára	komár	k1gMnSc2	komár
či	či	k8xC	či
včely	včela	k1gFnPc4	včela
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těch	ten	k3xDgMnPc2	ten
se	s	k7c7	s
silnějším	silný	k2eAgInSc7d2	silnější
jedem	jed	k1gInSc7	jed
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
snovačky	snovačka	k1gFnPc4	snovačka
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
které	který	k3yQgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
známá	známý	k2eAgFnSc1d1	známá
snovačka	snovačka	k1gFnSc1	snovačka
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
)	)	kIx)	)
a	a	k8xC	a
pavouci	pavouk	k1gMnPc1	pavouk
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Loxosceles	Loxoscelesa	k1gFnPc2	Loxoscelesa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plachá	plachý	k2eAgFnSc1d1	plachá
a	a	k8xC	a
kousá	kousat	k5eAaImIp3nS	kousat
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
ohrožená	ohrožený	k2eAgFnSc1d1	ohrožená
<g/>
.	.	kIx.	.
</s>
<s>
Sklípkanec	Sklípkanec	k1gMnSc1	Sklípkanec
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
má	mít	k5eAaImIp3nS	mít
agresivní	agresivní	k2eAgFnPc4d1	agresivní
obrannou	obranný	k2eAgFnSc4d1	obranná
taktiku	taktika	k1gFnSc4	taktika
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
jed	jed	k1gInSc1	jed
způsobil	způsobit	k5eAaPmAgInS	způsobit
úmrtí	úmrtí	k1gNnSc4	úmrtí
minimálně	minimálně	k6eAd1	minimálně
13	[number]	k4	13
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
existuje	existovat	k5eAaImIp3nS	existovat
asi	asi	k9	asi
100	[number]	k4	100
věrohodných	věrohodný	k2eAgInPc2d1	věrohodný
nahlášených	nahlášený	k2eAgInPc2d1	nahlášený
případů	případ	k1gInPc2	případ
úmrtí	úmrtí	k1gNnSc2	úmrtí
na	na	k7c4	na
pavoučí	pavoučí	k2eAgNnSc4d1	pavoučí
kousnutí	kousnutí	k1gNnSc4	kousnutí
<g/>
.	.	kIx.	.
</s>
<s>
Vaření	vařený	k2eAgMnPc1d1	vařený
sklípkani	sklípkan	k1gMnPc1	sklípkan
představují	představovat	k5eAaImIp3nP	představovat
pro	pro	k7c4	pro
Kambodžany	Kambodžan	k1gMnPc4	Kambodžan
a	a	k8xC	a
pro	pro	k7c4	pro
indiány	indián	k1gMnPc4	indián
kmene	kmen	k1gInSc2	kmen
Piaroa	Piaroa	k1gMnSc1	Piaroa
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Venezuely	Venezuela	k1gFnSc2	Venezuela
pochoutku	pochoutka	k1gFnSc4	pochoutka
<g/>
.	.	kIx.	.
</s>
<s>
Pavoučí	pavoučí	k2eAgInPc1d1	pavoučí
jedy	jed	k1gInPc1	jed
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
šetrnější	šetrný	k2eAgFnSc7d2	šetrnější
variantou	varianta	k1gFnSc7	varianta
k	k	k7c3	k
běžným	běžný	k2eAgInPc3d1	běžný
pesticidům	pesticid	k1gInPc3	pesticid
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
smrtelné	smrtelný	k2eAgFnPc1d1	smrtelná
pro	pro	k7c4	pro
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
neškodná	škodný	k2eNgFnSc1d1	neškodná
pro	pro	k7c4	pro
obratlovce	obratlovec	k1gMnPc4	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Vhodným	vhodný	k2eAgInSc7d1	vhodný
zdrojem	zdroj	k1gInSc7	zdroj
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
australští	australský	k2eAgMnPc1d1	australský
sklípkanci	sklípkanec	k1gMnPc1	sklípkanec
jedovatí	jedovatět	k5eAaImIp3nP	jedovatět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
většina	většina	k1gFnSc1	většina
hmyzích	hmyzí	k2eAgInPc2d1	hmyzí
druhů	druh	k1gInPc2	druh
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
jed	jed	k1gInSc4	jed
nemá	mít	k5eNaImIp3nS	mít
vybudovanou	vybudovaný	k2eAgFnSc4d1	vybudovaná
imunitu	imunita	k1gFnSc4	imunita
a	a	k8xC	a
sklípkancům	sklípkanec	k1gMnPc3	sklípkanec
jedovatým	jedovatý	k2eAgMnPc3d1	jedovatý
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
a	a	k8xC	a
jed	jed	k1gInSc1	jed
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
lze	lze	k6eAd1	lze
lehce	lehko	k6eAd1	lehko
dostat	dostat	k5eAaPmF	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
škůdce	škůdce	k1gMnSc1	škůdce
by	by	kYmCp3nS	by
možná	možná	k9	možná
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zaměřit	zaměřit	k5eAaPmF	zaměřit
pomocí	pomocí	k7c2	pomocí
genetického	genetický	k2eAgNnSc2d1	genetické
inženýrství	inženýrství	k1gNnSc2	inženýrství
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
geny	gen	k1gInPc1	gen
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
za	za	k7c4	za
tvorbu	tvorba	k1gFnSc4	tvorba
pavoučích	pavoučí	k2eAgInPc2d1	pavoučí
toxinů	toxin	k1gInPc2	toxin
implantovaly	implantovat	k5eAaBmAgFnP	implantovat
do	do	k7c2	do
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
napadají	napadat	k5eAaImIp3nP	napadat
například	například	k6eAd1	například
populace	populace	k1gFnSc2	populace
černopásky	černopásek	k1gInPc1	černopásek
bavlníkové	bavlníkový	k2eAgInPc1d1	bavlníkový
<g/>
.	.	kIx.	.
</s>
<s>
Pavoučí	pavoučí	k2eAgInSc1d1	pavoučí
jed	jed	k1gInSc1	jed
má	mít	k5eAaImIp3nS	mít
potenciální	potenciální	k2eAgMnSc1d1	potenciální
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
například	například	k6eAd1	například
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
srdeční	srdeční	k2eAgFnSc2d1	srdeční
arytmie	arytmie	k1gFnSc2	arytmie
<g/>
,	,	kIx,	,
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1	Alzheimerova
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
mozkové	mozkový	k2eAgFnSc2d1	mozková
mrtvice	mrtvice	k1gFnSc2	mrtvice
a	a	k8xC	a
erektilní	erektilní	k2eAgFnSc2d1	erektilní
dysfunkce	dysfunkce	k1gFnSc2	dysfunkce
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
pavoučí	pavoučí	k2eAgNnSc1d1	pavoučí
hedvábí	hedvábí	k1gNnSc1	hedvábí
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
lehké	lehký	k2eAgNnSc1d1	lehké
a	a	k8xC	a
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
,	,	kIx,	,
probíhají	probíhat	k5eAaImIp3nP	probíhat
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
umělou	umělý	k2eAgFnSc4d1	umělá
syntézu	syntéza	k1gFnSc4	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Geny	gen	k1gInPc1	gen
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
proteinů	protein	k1gInPc2	protein
pavoučích	pavoučí	k2eAgFnPc2d1	pavoučí
vláken	vlákna	k1gFnPc2	vlákna
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
pomocí	pomocí	k7c2	pomocí
metod	metoda	k1gFnPc2	metoda
genetického	genetický	k2eAgInSc2d1	genetický
inženýrství	inženýrství	k1gNnSc6	inženýrství
přenášeny	přenášet	k5eAaImNgInP	přenášet
do	do	k7c2	do
genomu	genom	k1gInSc2	genom
dalších	další	k2eAgMnPc2d1	další
potenciálních	potenciální	k2eAgMnPc2d1	potenciální
producentů	producent	k1gMnPc2	producent
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
některých	některý	k3yIgFnPc2	některý
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
kvasinek	kvasinka	k1gFnPc2	kvasinka
či	či	k8xC	či
koz	koza	k1gFnPc2	koza
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Arachnofobie	Arachnofobie	k1gFnSc2	Arachnofobie
<g/>
.	.	kIx.	.
</s>
<s>
Arachnofobie	Arachnofobie	k1gFnSc1	Arachnofobie
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
fobie	fobie	k1gFnSc1	fobie
-	-	kIx~	-
abnormální	abnormální	k2eAgInSc1d1	abnormální
strach	strach	k1gInSc1	strach
z	z	k7c2	z
pavouků	pavouk	k1gMnPc2	pavouk
nebo	nebo	k8xC	nebo
čehokoliv	cokoliv	k3yInSc2	cokoliv
připomínajícího	připomínající	k2eAgNnSc2d1	připomínající
pavouky	pavouk	k1gMnPc4	pavouk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
pavučiny	pavučina	k1gFnPc1	pavučina
a	a	k8xC	a
pavoučí	pavoučí	k2eAgInPc1d1	pavoučí
tvary	tvar	k1gInPc1	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejběžnějších	běžný	k2eAgFnPc2d3	nejběžnější
specifických	specifický	k2eAgFnPc2d1	specifická
fobií	fobie	k1gFnPc2	fobie
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
statistiky	statistika	k1gFnPc1	statistika
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
u	u	k7c2	u
50	[number]	k4	50
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
přehnanou	přehnaný	k2eAgFnSc4d1	přehnaná
formu	forma	k1gFnSc4	forma
instinktivní	instinktivní	k2eAgFnSc2d1	instinktivní
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pomohla	pomoct	k5eAaPmAgFnS	pomoct
prvním	první	k4xOgMnSc6	první
lidem	člověk	k1gMnPc3	člověk
k	k	k7c3	k
přežití	přežití	k1gNnSc3	přežití
<g/>
,	,	kIx,	,
či	či	k8xC	či
o	o	k7c4	o
kulturní	kulturní	k2eAgInSc4d1	kulturní
jev	jev	k1gInSc4	jev
běžný	běžný	k2eAgInSc4d1	běžný
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
společnostech	společnost	k1gFnPc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Pavouci	pavouk	k1gMnPc1	pavouk
byli	být	k5eAaImAgMnP	být
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
mytologií	mytologie	k1gFnPc2	mytologie
různých	různý	k2eAgFnPc2d1	různá
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
technice	technika	k1gFnSc3	technika
lovu	lov	k1gInSc2	lov
spočívající	spočívající	k2eAgFnSc1d1	spočívající
ve	v	k7c6	v
vytváření	vytváření	k1gNnSc6	vytváření
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
čekání	čekání	k1gNnSc2	čekání
na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
trpělivost	trpělivost	k1gFnSc1	trpělivost
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
kvůli	kvůli	k7c3	kvůli
jedovatým	jedovatý	k2eAgFnPc3d1	jedovatá
(	(	kIx(	(
<g/>
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
smrtelným	smrtelný	k2eAgMnSc7d1	smrtelný
<g/>
)	)	kIx)	)
kousnutím	kousnutí	k1gNnSc7	kousnutí
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
krutost	krutost	k1gFnSc4	krutost
<g/>
.	.	kIx.	.
</s>
<s>
Snování	snování	k1gNnSc1	snování
pavučin	pavučina	k1gFnPc2	pavučina
také	také	k9	také
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
spojení	spojení	k1gNnSc1	spojení
pavouků	pavouk	k1gMnPc2	pavouk
s	s	k7c7	s
mýty	mýtus	k1gInPc7	mýtus
o	o	k7c6	o
stvoření	stvoření	k1gNnSc6	stvoření
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pavouci	pavouk	k1gMnPc1	pavouk
jako	jako	k8xC	jako
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
vlastní	vlastní	k2eAgInPc4d1	vlastní
světy	svět	k1gInPc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Močikové	Močik	k1gMnPc1	Močik
ze	z	k7c2	z
starodávného	starodávný	k2eAgNnSc2d1	starodávné
Peru	Peru	k1gNnSc2	Peru
vzývali	vzývat	k5eAaImAgMnP	vzývat
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
často	často	k6eAd1	často
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
umění	umění	k1gNnSc6	umění
zobrazovali	zobrazovat	k5eAaImAgMnP	zobrazovat
pavouky	pavouk	k1gMnPc4	pavouk
<g/>
.	.	kIx.	.
</s>
