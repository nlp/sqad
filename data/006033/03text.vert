<s>
Slad	slad	k1gInSc1	slad
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
pro	pro	k7c4	pro
naklíčené	naklíčený	k2eAgNnSc4d1	naklíčené
a	a	k8xC	a
usušené	usušený	k2eAgNnSc4d1	usušené
obilné	obilný	k2eAgNnSc4d1	obilné
zrno	zrno	k1gNnSc4	zrno
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
ječmenné	ječmenný	k2eAgFnPc1d1	ječmenná
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
sladu	slad	k1gInSc2	slad
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sladování	sladování	k1gNnSc1	sladování
a	a	k8xC	a
věnují	věnovat	k5eAaImIp3nP	věnovat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
sladovníci	sladovník	k1gMnPc1	sladovník
ve	v	k7c6	v
sladovnách	sladovna	k1gFnPc6	sladovna
<g/>
.	.	kIx.	.
</s>
<s>
Slad	slad	k1gInSc1	slad
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
surovin	surovina	k1gFnPc2	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
whisky	whisky	k1gFnSc2	whisky
<g/>
.	.	kIx.	.
</s>
<s>
Sladování	sladování	k1gNnSc1	sladování
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
rozštěpily	rozštěpit	k5eAaPmAgInP	rozštěpit
v	v	k7c6	v
sladu	slad	k1gInSc6	slad
uložené	uložený	k2eAgInPc1d1	uložený
polysacharidy	polysacharid	k1gInPc1	polysacharid
na	na	k7c4	na
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
sacharidy	sacharid	k1gInPc4	sacharid
vhodné	vhodný	k2eAgInPc4d1	vhodný
ke	k	k7c3	k
kvašení	kvašení	k1gNnSc3	kvašení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Sladování	sladování	k1gNnSc1	sladování
a	a	k8xC	a
Sladovna	sladovna	k1gFnSc1	sladovna
<g/>
.	.	kIx.	.
</s>
<s>
Sladování	sladování	k1gNnSc1	sladování
je	být	k5eAaImIp3nS	být
několikadenní	několikadenní	k2eAgInSc4d1	několikadenní
proces	proces	k1gInSc4	proces
probíhající	probíhající	k2eAgInSc4d1	probíhající
ve	v	k7c6	v
sladovnách	sladovna	k1gFnPc6	sladovna
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
sladuje	sladovat	k5eAaImIp3nS	sladovat
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
poloautomatických	poloautomatický	k2eAgFnPc6d1	poloautomatická
sladovnách	sladovna	k1gFnPc6	sladovna
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
sladovna	sladovna	k1gFnSc1	sladovna
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
pivovaru	pivovar	k1gInSc6	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Zrno	zrno	k1gNnSc1	zrno
se	se	k3xPyFc4	se
během	během	k7c2	během
sladování	sladování	k1gNnSc2	sladování
namáčí	namáčet	k5eAaImIp3nS	namáčet
a	a	k8xC	a
nechá	nechat	k5eAaPmIp3nS	nechat
se	se	k3xPyFc4	se
klíčit	klíčit	k5eAaImF	klíčit
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
uvolnění	uvolnění	k1gNnSc1	uvolnění
cukrotvorných	cukrotvorný	k2eAgInPc2d1	cukrotvorný
enzymů	enzym	k1gInPc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
suší	sušit	k5eAaImIp3nS	sušit
a	a	k8xC	a
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
sušení	sušení	k1gNnSc2	sušení
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivněn	k2eAgFnSc1d1	ovlivněna
barva	barva	k1gFnSc1	barva
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
vlastnosti	vlastnost	k1gFnPc1	vlastnost
sladu	slad	k1gInSc2	slad
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
obracení	obracení	k1gNnSc4	obracení
vrstvy	vrstva	k1gFnSc2	vrstva
zrn	zrno	k1gNnPc2	zrno
(	(	kIx(	(
<g/>
hromady	hromada	k1gFnSc2	hromada
<g/>
)	)	kIx)	)
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
stádiích	stádium	k1gNnPc6	stádium
klíčení	klíčení	k1gNnSc2	klíčení
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
hromada	hromada	k1gFnSc1	hromada
zarovnávala	zarovnávat	k5eAaImAgFnS	zarovnávat
(	(	kIx(	(
<g/>
vydrovala	vydrovat	k5eAaImAgFnS	vydrovat
<g/>
)	)	kIx)	)
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
druhem	druh	k1gInSc7	druh
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
lopaty	lopata	k1gFnSc2	lopata
<g/>
,	,	kIx,	,
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
vydrou	vydra	k1gFnSc7	vydra
<g/>
.	.	kIx.	.
</s>
<s>
Předcházelo	předcházet	k5eAaImAgNnS	předcházet
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
ztrátám	ztráta	k1gFnPc3	ztráta
z	z	k7c2	z
rozptýlených	rozptýlený	k2eAgFnPc2d1	rozptýlená
zrn	zrno	k1gNnPc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nástroj	nástroj	k1gInSc1	nástroj
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
sladoven	sladovna	k1gFnPc2	sladovna
a	a	k8xC	a
pivovarů	pivovar	k1gInPc2	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
sladů	slad	k1gInPc2	slad
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
<g/>
:	:	kIx,	:
světlý	světlý	k2eAgInSc4d1	světlý
–	–	k?	–
neboli	neboli	k8xC	neboli
plzeňský	plzeňský	k2eAgInSc4d1	plzeňský
slad	slad	k1gInSc4	slad
s	s	k7c7	s
nejsvětlejší	světlý	k2eAgFnSc7d3	nejsvětlejší
barvou	barva	k1gFnSc7	barva
určený	určený	k2eAgInSc1d1	určený
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
světlých	světlý	k2eAgNnPc2d1	světlé
piv	pivo	k1gNnPc2	pivo
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
typu	typ	k1gInSc2	typ
bavorský	bavorský	k2eAgMnSc1d1	bavorský
–	–	k?	–
takzvaně	takzvaně	k6eAd1	takzvaně
polotmavý	polotmavý	k2eAgInSc1d1	polotmavý
slad	slad	k1gInSc1	slad
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
mírné	mírný	k2eAgNnSc4d1	mírné
dobarvení	dobarvení	k1gNnSc4	dobarvení
světlých	světlý	k2eAgNnPc2d1	světlé
piv	pivo	k1gNnPc2	pivo
či	či	k8xC	či
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
piv	pivo	k1gNnPc2	pivo
polotmavých	polotmavý	k2eAgFnPc6d1	polotmavá
a	a	k8xC	a
tmavých	tmavý	k2eAgFnPc6d1	tmavá
karamelový	karamelový	k2eAgMnSc1d1	karamelový
–	–	k?	–
velmi	velmi	k6eAd1	velmi
tmavý	tmavý	k2eAgInSc4d1	tmavý
slad	slad	k1gInSc4	slad
-	-	kIx~	-
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
světlý	světlý	k2eAgInSc4d1	světlý
a	a	k8xC	a
tmavý	tmavý	k2eAgInSc4d1	tmavý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
světlého	světlý	k2eAgNnSc2d1	světlé
je	být	k5eAaImIp3nS	být
použita	použit	k2eAgFnSc1d1	použita
teplota	teplota	k1gFnSc1	teplota
sušení	sušení	k1gNnSc1	sušení
120-130	[number]	k4	120-130
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
u	u	k7c2	u
tmavého	tmavý	k2eAgNnSc2d1	tmavé
150-170	[number]	k4	150-170
°	°	k?	°
<g/>
C	C	kA	C
barevný	barevný	k2eAgInSc1d1	barevný
–	–	k?	–
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
pražený	pražený	k2eAgInSc4d1	pražený
slad	slad	k1gInSc4	slad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
přidáván	přidávat	k5eAaImNgInS	přidávat
jen	jen	k9	jen
pro	pro	k7c4	pro
tmavší	tmavý	k2eAgFnSc4d2	tmavší
barvu	barva	k1gFnSc4	barva
výsledného	výsledný	k2eAgInSc2d1	výsledný
moku	mok	k1gInSc2	mok
<g/>
.	.	kIx.	.
</s>
<s>
Pražení	pražení	k1gNnSc1	pražení
probíhá	probíhat	k5eAaImIp3nS	probíhat
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
210-220	[number]	k4	210-220
°	°	k?	°
<g/>
C.	C.	kA	C.
Tento	tento	k3xDgInSc4	tento
slad	slad	k1gInSc4	slad
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgInPc4	žádný
enzymy	enzym	k1gInPc4	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInPc1d1	speciální
slady	slad	k1gInPc1	slad
diastatický	diastatický	k2eAgInSc1d1	diastatický
–	–	k?	–
při	při	k7c6	při
sušení	sušení	k1gNnSc6	sušení
(	(	kIx(	(
<g/>
hvozdění	hvozdění	k1gNnSc1	hvozdění
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
teplota	teplota	k1gFnSc1	teplota
max	max	kA	max
50	[number]	k4	50
°	°	k?	°
<g/>
C.	C.	kA	C.
Slad	slad	k1gInSc1	slad
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
enzymatickou	enzymatický	k2eAgFnSc4d1	enzymatická
účinnost	účinnost	k1gFnSc4	účinnost
a	a	k8xC	a
přidává	přidávat	k5eAaImIp3nS	přidávat
se	se	k3xPyFc4	se
k	k	k7c3	k
sladům	slad	k1gInPc3	slad
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
enzymů	enzym	k1gInPc2	enzym
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
proteolitický	proteolitický	k2eAgInSc1d1	proteolitický
–	–	k?	–
v	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nevyrábí	vyrábět	k5eNaImIp3nS	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
výrobě	výroba	k1gFnSc6	výroba
se	se	k3xPyFc4	se
slad	slad	k1gInSc1	slad
kvasí	kvasit	k5eAaImIp3nS	kvasit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
mléčné	mléčný	k2eAgFnSc2d1	mléčná
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Slad	slad	k1gInSc1	slad
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
kyselost	kyselost	k1gFnSc4	kyselost
a	a	k8xC	a
pěnivost	pěnivost	k1gFnSc4	pěnivost
výsledného	výsledný	k2eAgNnSc2d1	výsledné
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
nakuřovaný	nakuřovaný	k2eAgInSc1d1	nakuřovaný
–	–	k?	–
při	při	k7c6	při
hvozdění	hvozdění	k1gNnSc6	hvozdění
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
dosušování	dosušování	k1gNnSc6	dosušování
kouř	kouř	k1gInSc4	kouř
z	z	k7c2	z
rašeliny	rašelina	k1gFnSc2	rašelina
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
whisky	whisky	k1gFnSc2	whisky
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
piva	pivo	k1gNnSc2	pivo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rittmayer	Rittmayer	k1gInSc1	Rittmayer
Smokey	Smokea	k1gMnSc2	Smokea
George	Georg	k1gMnSc2	Georg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sladu	slad	k1gInSc2	slad
pro	pro	k7c4	pro
pivo	pivo	k1gNnSc4	pivo
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
používá	používat	k5eAaImIp3nS	používat
kouř	kouř	k1gInSc1	kouř
z	z	k7c2	z
bukového	bukový	k2eAgNnSc2d1	bukové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
lihovarský	lihovarský	k2eAgInSc1d1	lihovarský
–	–	k?	–
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
diastatický	diastatický	k2eAgMnSc1d1	diastatický
kyselý	kyselý	k2eAgMnSc1d1	kyselý
-	-	kIx~	-
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
snižování	snižování	k1gNnSc4	snižování
pH	ph	kA	ph
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Kyselost	kyselost	k1gFnSc1	kyselost
dodává	dodávat	k5eAaImIp3nS	dodávat
kyselina	kyselina	k1gFnSc1	kyselina
mléčná	mléčný	k2eAgFnSc1d1	mléčná
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
během	běh	k1gInSc7	běh
sladování	sladování	k1gNnSc2	sladování
<g/>
.	.	kIx.	.
</s>
<s>
Kolbachovo	Kolbachův	k2eAgNnSc1d1	Kolbachovo
číslo	číslo	k1gNnSc1	číslo
Kvas	kvas	k1gInSc4	kvas
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
slad	slad	k1gInSc4	slad
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
slad	slad	k1gInSc4	slad
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
