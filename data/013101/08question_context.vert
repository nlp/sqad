<s>
Ian	Ian	k?	Ian
Ritchie	Ritchie	k1gFnSc1	Ritchie
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
aranžér	aranžér	k1gMnSc1	aranžér
a	a	k8xC	a
saxofonista	saxofonista	k1gMnSc1	saxofonista
<g/>
.	.	kIx.	.
</s>
<s>
Produkoval	produkovat	k5eAaImAgMnS	produkovat
mnoho	mnoho	k4c4	mnoho
nahrávek	nahrávka	k1gFnPc2	nahrávka
včetně	včetně	k7c2	včetně
alb	alba	k1gFnPc2	alba
Radio	radio	k1gNnSc1	radio
K.A.	K.A.	k1gMnSc1	K.A.
<g/>
O.S.	O.S.	k1gMnSc1	O.S.
(	(	kIx(	(
<g/>
Roger	Roger	k1gInSc1	Roger
Waters	Waters	k1gInSc1	Waters
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strange	Strange	k1gInSc1	Strange
Angels	Angels	k1gInSc1	Angels
(	(	kIx(	(
<g/>
Laurie	Laurie	k1gFnSc1	Laurie
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sinful	Sinful	k1gInSc1	Sinful
(	(	kIx(	(
<g/>
Pete	Pete	k1gInSc1	Pete
Wylie	Wylie	k1gFnSc2	Wylie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wolf	Wolf	k1gMnSc1	Wolf
(	(	kIx(	(
<g/>
Hugh	Hugh	k1gMnSc1	Hugh
Cornwell	Cornwell	k1gMnSc1	Cornwell
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Swimmer	Swimmer	k1gMnSc1	Swimmer
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Big	Big	k1gMnSc1	Big
Dish	Dish	k1gMnSc1	Dish
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Boomtown	Boomtown	k1gMnSc1	Boomtown
Rats	Rats	k1gInSc1	Rats
(	(	kIx(	(
<g/>
Bob	Bob	k1gMnSc1	Bob
Geldof	Geldof	k1gMnSc1	Geldof
<g/>
)	)	kIx)	)
či	či	k8xC	či
URUBU	urubat	k5eAaPmIp1nS	urubat
<g/>
.	.	kIx.	.
</s>
