<p>
<s>
Ian	Ian	k?	Ian
Ritchie	Ritchie	k1gFnSc1	Ritchie
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
aranžér	aranžér	k1gMnSc1	aranžér
a	a	k8xC	a
saxofonista	saxofonista	k1gMnSc1	saxofonista
<g/>
.	.	kIx.	.
</s>
<s>
Produkoval	produkovat	k5eAaImAgMnS	produkovat
mnoho	mnoho	k4c4	mnoho
nahrávek	nahrávka	k1gFnPc2	nahrávka
včetně	včetně	k7c2	včetně
alb	alba	k1gFnPc2	alba
Radio	radio	k1gNnSc1	radio
K.A.	K.A.	k1gMnSc1	K.A.
<g/>
O.S.	O.S.	k1gMnSc1	O.S.
(	(	kIx(	(
<g/>
Roger	Roger	k1gInSc1	Roger
Waters	Waters	k1gInSc1	Waters
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Strange	Strange	k1gInSc1	Strange
Angels	Angels	k1gInSc1	Angels
(	(	kIx(	(
<g/>
Laurie	Laurie	k1gFnSc1	Laurie
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sinful	Sinful	k1gInSc1	Sinful
(	(	kIx(	(
<g/>
Pete	Pete	k1gInSc1	Pete
Wylie	Wylie	k1gFnSc2	Wylie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wolf	Wolf	k1gMnSc1	Wolf
(	(	kIx(	(
<g/>
Hugh	Hugh	k1gMnSc1	Hugh
Cornwell	Cornwell	k1gMnSc1	Cornwell
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Swimmer	Swimmer	k1gMnSc1	Swimmer
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Big	Big	k1gMnSc1	Big
Dish	Dish	k1gMnSc1	Dish
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Boomtown	Boomtown	k1gMnSc1	Boomtown
Rats	Rats	k1gInSc1	Rats
(	(	kIx(	(
<g/>
Bob	Bob	k1gMnSc1	Bob
Geldof	Geldof	k1gMnSc1	Geldof
<g/>
)	)	kIx)	)
či	či	k8xC	či
URUBU	urubat	k5eAaPmIp1nS	urubat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
také	také	k9	také
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
cestopisný	cestopisný	k2eAgInSc4d1	cestopisný
pořad	pořad	k1gInSc4	pořad
Globe	globus	k1gInSc5	globus
Trekker	Trekker	k1gInSc4	Trekker
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Lonely	Lonela	k1gFnPc1	Lonela
Planet	planeta	k1gFnPc2	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
rockové	rockový	k2eAgFnSc2d1	rocková
kapely	kapela	k1gFnSc2	kapela
Deaf	Deaf	k1gMnSc1	Deaf
School	School	k1gInSc1	School
a	a	k8xC	a
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vydal	vydat	k5eAaPmAgInS	vydat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Miro	Mira	k1gFnSc5	Mira
Miroe	Miroe	k1gNnSc6	Miroe
několik	několik	k4yIc4	několik
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
saxofonista	saxofonista	k1gMnSc1	saxofonista
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
albu	album	k1gNnSc6	album
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Dee	Dee	k1gFnSc2	Dee
C.	C.	kA	C.
Leeové	Leeová	k1gFnSc2	Leeová
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnSc2	skupina
Wham	Whama	k1gFnPc2	Whama
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
zpěváka	zpěvák	k1gMnSc2	zpěvák
a	a	k8xC	a
kytaristy	kytarista	k1gMnSc2	kytarista
Robbieho	Robbie	k1gMnSc2	Robbie
Nevila	vít	k5eNaImAgFnS	vít
či	či	k8xC	či
The	The	k1gFnSc1	The
Beach	Beacha	k1gFnPc2	Beacha
Boys	boy	k1gMnPc2	boy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vydal	vydat	k5eAaPmAgInS	vydat
jazzové	jazzový	k2eAgNnSc4d1	jazzové
album	album	k1gNnSc4	album
Ian	Ian	k1gFnSc2	Ian
Ritchie	Ritchie	k1gFnSc2	Ritchie
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
SOHO	SOHO	kA	SOHO
Project	Project	k1gInSc1	Project
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
následováno	následován	k2eAgNnSc1d1	následováno
deskou	deska	k1gFnSc7	deska
South	Southa	k1gFnPc2	Southa
of	of	k?	of
Houston	Houston	k1gInSc1	Houston
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
Rogera	Roger	k1gMnSc2	Roger
Waterse	Waterse	k1gFnSc2	Waterse
The	The	k1gMnSc2	The
Dark	Darka	k1gFnPc2	Darka
Side	Sid	k1gMnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gNnSc4	Moon
Live	Liv	k1gFnSc2	Liv
na	na	k7c4	na
saxofon	saxofon	k1gInSc4	saxofon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
boku	bok	k1gInSc6	bok
Rogera	Rogero	k1gNnSc2	Rogero
Waterse	Waterse	k1gFnSc2	Waterse
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
při	při	k7c6	při
celosvětových	celosvětový	k2eAgInPc6d1	celosvětový
benefičních	benefiční	k2eAgInPc6d1	benefiční
koncertech	koncert	k1gInPc6	koncert
Live	Live	k1gFnSc1	Live
Earth	Earth	k1gInSc1	Earth
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
a	a	k8xC	a
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Concert	Concert	k1gInSc1	Concert
for	forum	k1gNnPc2	forum
Sandy	Sanda	k1gFnSc2	Sanda
Relief	Relief	k1gInSc1	Relief
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
australskou	australský	k2eAgFnSc7d1	australská
progresivní	progresivní	k2eAgFnSc7d1	progresivní
rockovou	rockový	k2eAgFnSc7d1	rocková
kapelou	kapela	k1gFnSc7	kapela
Unitopia	Unitopium	k1gNnSc2	Unitopium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
svou	svůj	k3xOyFgFnSc4	svůj
jazzovou	jazzový	k2eAgFnSc4d1	jazzová
tvorbu	tvorba	k1gFnSc4	tvorba
představil	představit	k5eAaPmAgMnS	představit
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
premiérového	premiérový	k2eAgNnSc2d1	premiérové
turné	turné	k1gNnSc2	turné
po	po	k7c6	po
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
Slovensku	Slovensko	k1gNnSc6	Slovensko
po	po	k7c6	po
boku	bok	k1gInSc6	bok
slovenského	slovenský	k2eAgMnSc2d1	slovenský
pianisty	pianista	k1gMnSc2	pianista
Tomáše	Tomáš	k1gMnSc2	Tomáš
Jochmanna	Jochmann	k1gMnSc2	Jochmann
<g/>
,	,	kIx,	,
kontrabasisty	kontrabasista	k1gMnSc2	kontrabasista
Miloše	Miloš	k1gMnSc2	Miloš
Klápštěho	Klápště	k1gMnSc2	Klápště
a	a	k8xC	a
bubeníka	bubeník	k1gMnSc2	bubeník
Olivera	Oliver	k1gMnSc2	Oliver
Lipenského	lipenský	k2eAgMnSc2d1	lipenský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ian	Ian	k1gFnSc2	Ian
Ritchie	Ritchie	k1gFnSc2	Ritchie
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
