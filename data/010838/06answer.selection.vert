<s>
Balneologie	balneologie	k1gFnSc1	balneologie
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
léčivých	léčivý	k2eAgFnPc6d1	léčivá
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
lázních	lázeň	k1gFnPc6	lázeň
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc6	jejich
účincích	účinek	k1gInPc6	účinek
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
terapeutickým	terapeutický	k2eAgInPc3d1	terapeutický
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
