<p>
<s>
Balneologie	balneologie	k1gFnSc1	balneologie
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
léčivých	léčivý	k2eAgFnPc6d1	léčivá
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
lázních	lázeň	k1gFnPc6	lázeň
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc6	jejich
účincích	účinek	k1gInPc6	účinek
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
s	s	k7c7	s
přihlédnutím	přihlédnutí	k1gNnSc7	přihlédnutí
k	k	k7c3	k
terapeutickým	terapeutický	k2eAgInPc3d1	terapeutický
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Balneologický	balneologický	k2eAgInSc1d1	balneologický
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
lidově	lidově	k6eAd1	lidově
řečeno	říct	k5eAaPmNgNnS	říct
rozmočená	rozmočený	k2eAgFnSc1d1	rozmočená
kůže	kůže	k1gFnSc2	kůže
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
známe	znát	k5eAaImIp1nP	znát
rozmočené	rozmočený	k2eAgInPc1d1	rozmočený
konečky	koneček	k1gInPc1	koneček
prstů	prst	k1gInPc2	prst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozmočená	rozmočený	k2eAgFnSc1d1	rozmočená
kůže	kůže	k1gFnSc1	kůže
se	se	k3xPyFc4	se
roztáhne	roztáhnout	k5eAaPmIp3nS	roztáhnout
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
přijímat	přijímat	k5eAaImF	přijímat
více	hodně	k6eAd2	hodně
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
–	–	k?	–
v	v	k7c6	v
bazénu	bazén	k1gInSc6	bazén
například	například	k6eAd1	například
jód	jód	k1gInSc4	jód
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
toxiny	toxin	k1gInPc1	toxin
sinic	sinice	k1gFnPc2	sinice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
české	český	k2eAgFnSc2d1	Česká
balneologie	balneologie	k1gFnSc2	balneologie
je	být	k5eAaImIp3nS	být
docent	docent	k1gMnSc1	docent
Jan	Jan	k1gMnSc1	Jan
Špott	Špott	k1gMnSc1	Špott
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
Časopisu	časopis	k1gInSc2	časopis
lékařů	lékař	k1gMnPc2	lékař
českých	český	k2eAgMnPc2d1	český
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
Repealu	repeal	k1gInSc6	repeal
prošel	projít	k5eAaPmAgInS	projít
rakouskými	rakouský	k2eAgFnPc7d1	rakouská
věznicemi	věznice	k1gFnPc7	věznice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
balneologie	balneologie	k1gFnSc2	balneologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
