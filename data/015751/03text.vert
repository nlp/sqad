<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
<g/>
,	,	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
–	–	k?
Éloi	Élo	k1gFnSc3
Firmin	Firmin	k2eAgInSc1d1
Féron	Féron	k1gInSc1
(	(	kIx(
<g/>
1802	#num#	k4
<g/>
–	–	k?
<g/>
1876	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1191	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Arsuf	Arsuf	k1gMnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
drtivé	drtivý	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
křižáků	křižák	k1gInPc2
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Anglické	anglický	k2eAgNnSc1d1
království	království	k1gNnSc1
a	a	k8xC
Říše	říše	k1gFnSc1
Plantagenetů	Plantagenet	k1gInPc2
Francouzské	francouzský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Burgundské	burgundský	k2eAgNnSc1d1
vévodství	vévodství	k1gNnSc1
</s>
<s>
Hrabství	hrabství	k1gNnSc1
Champagne	Champagn	k1gMnSc5
</s>
<s>
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
Řád	řád	k1gInSc4
johanitů	johanita	k1gMnPc2
Řád	řád	k1gInSc4
templářůkřižáci	templářůkřižák	k1gMnPc1
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
království	království	k1gNnPc2
</s>
<s>
Ajjúbovský	Ajjúbovský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
Hugo	Hugo	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Burgundský	burgundský	k2eAgInSc1d1
Guy	Guy	k1gFnSc7
de	de	k?
Lusignan	Lusignan	k1gMnSc1
Garnier	Garnier	k1gMnSc1
z	z	k7c2
Naplous	Naplous	k1gMnSc1
Robert	Robert	k1gMnSc1
ze	z	k7c2
Sablé	Sablý	k1gMnPc4
Jakub	Jakub	k1gMnSc1
z	z	k7c2
Avesnes	Avesnes	k1gMnSc1
†	†	k?
Jindřich	Jindřich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
ze	z	k7c2
Champagne	Champagn	k1gInSc5
</s>
<s>
Saladin	Saladin	k2eAgMnSc1d1
Al-Ádil	Al-Ádil	k1gMnSc1
I.	I.	kA
Al-Afdal	Al-Afdal	k1gFnSc1
Aladdin	Aladdina	k1gFnPc2
z	z	k7c2
Mosulu	Mosul	k1gInSc2
Musek	Musek	k1gMnSc1
<g/>
,	,	kIx,
velkoemír	velkoemír	k1gMnSc1
Kurdů	Kurd	k1gMnPc2
†	†	k?
Al-Muzaffar	Al-Muzaffara	k1gFnPc2
Umar	Umar	k1gMnSc1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
10	#num#	k4
000	#num#	k4
pěšáků	pěšák	k1gMnPc2
<g/>
,1	,1	k4
200	#num#	k4
těžké	těžký	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
<g/>
,	,	kIx,
<g/>
dohromady	dohromady	k6eAd1
11	#num#	k4
200	#num#	k4
mužů	muž	k1gMnPc2
</s>
<s>
až	až	k9
25	#num#	k4
000	#num#	k4
jezdců	jezdec	k1gMnPc2
<g/>
,	,	kIx,
<g/>
lehké	lehký	k2eAgFnPc1d1
nebo	nebo	k8xC
těžké	těžký	k2eAgFnPc1d1
jízdy	jízda	k1gFnPc1
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
okolo	okolo	k7c2
700	#num#	k4
padlých	padlý	k1gMnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
okolo	okolo	k7c2
7	#num#	k4
000	#num#	k4
padlýchvčetně	padlýchvčetně	k6eAd1
32	#num#	k4
emírů	emír	k1gMnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
(	(	kIx(
<g/>
též	též	k9
bitva	bitva	k1gFnSc1
u	u	k7c2
Arsoufu	Arsouf	k1gInSc2
<g/>
)	)	kIx)
-	-	kIx~
byla	být	k5eAaImAgFnS
významným	významný	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
anglického	anglický	k2eAgMnSc2d1
krále	král	k1gMnSc2
Richarda	Richard	k1gMnSc2
I.	I.	kA
Lví	lví	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
čelních	čelní	k2eAgMnPc2d1
vůdců	vůdce	k1gMnPc2
třetí	třetí	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
nad	nad	k7c7
muslimským	muslimský	k2eAgMnSc7d1
vládcem	vládce	k1gMnSc7
Saladinem	Saladin	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
význam	význam	k1gInSc1
spočívá	spočívat	k5eAaImIp3nS
zejména	zejména	k9
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
první	první	k4xOgNnSc1
velké	velký	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
křižáků	křižák	k1gInPc2
nad	nad	k7c7
Saldinem	Saldin	k1gInSc7
od	od	k7c2
bitvy	bitva	k1gFnSc2
u	u	k7c2
Hattínu	Hattín	k1gInSc2
a	a	k8xC
více	hodně	k6eAd2
méně	málo	k6eAd2
otevřelo	otevřít	k5eAaPmAgNnS
cestu	cesta	k1gFnSc4
pro	pro	k7c4
vyjednávání	vyjednávání	k1gNnSc4
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
kulturami	kultura	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
zpřístupněním	zpřístupnění	k1gNnSc7
Božího	boží	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
západním	západní	k2eAgMnPc3d1
poutníkům	poutník	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
kapitulaci	kapitulace	k1gFnSc6
Akkonu	Akkon	k1gInSc2
v	v	k7c6
červenci	červenec	k1gInSc6
roku	rok	k1gInSc2
1191	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
Richard	Richard	k1gMnSc1
I.	I.	kA
vydal	vydat	k5eAaPmAgMnS
na	na	k7c4
pochod	pochod	k1gInSc4
k	k	k7c3
jihu	jih	k1gInSc3
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nP
mohl	moct	k5eAaImAgInS
znova	znova	k6eAd1
dobýt	dobýt	k5eAaPmF
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
něj	on	k3xPp3gNnSc4
velmi	velmi	k6eAd1
důležité	důležitý	k2eAgNnSc1d1
zmocnit	zmocnit	k5eAaPmF
se	se	k3xPyFc4
přístavního	přístavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Jaffa	Jaffa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saladin	Saladina	k1gFnPc2
<g/>
,	,	kIx,
kterému	který	k3yRgInSc3,k3yIgInSc3,k3yQgInSc3
se	se	k3xPyFc4
právě	právě	k9
podařilo	podařit	k5eAaPmAgNnS
vypudit	vypudit	k5eAaPmF
křižácká	křižácký	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
z	z	k7c2
téměř	téměř	k6eAd1
celého	celý	k2eAgNnSc2d1
území	území	k1gNnSc2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
shromáždil	shromáždit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
hlavní	hlavní	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
moc	moc	k1gFnSc4
u	u	k7c2
města	město	k1gNnSc2
Arsufu	Arsuf	k1gInSc2
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
zde	zde	k6eAd1
svést	svést	k5eAaPmF
s	s	k7c7
křižáky	křižák	k1gMnPc7
rozhodující	rozhodující	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kristovým	Kristův	k2eAgMnSc7d1
vojákům	voják	k1gMnPc3
trvalo	trvat	k5eAaImAgNnS
za	za	k7c4
krajně	krajně	k6eAd1
vysilujících	vysilující	k2eAgInPc2d1
pochodů	pochod	k1gInPc2
a	a	k8xC
stálého	stálý	k2eAgNnSc2d1
atakování	atakování	k1gNnSc2
Saladinovým	Saladinový	k2eAgNnSc7d1
jezdectvem	jezdectvo	k1gNnSc7
dosáhnout	dosáhnout	k5eAaPmF
místa	místo	k1gNnPc4
střetnutí	střetnutí	k1gNnSc2
celých	celý	k2eAgInPc2d1
čtrnáct	čtrnáct	k4xCc4
dní	den	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
přesto	přesto	k6eAd1
byli	být	k5eAaImAgMnP
schopní	schopný	k2eAgMnPc1d1
muslimského	muslimský	k2eAgInSc2d1
panovníka	panovník	k1gMnSc4
drtivě	drtivě	k6eAd1
porazit	porazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Pochod	pochod	k1gInSc1
na	na	k7c4
jih	jih	k1gInSc4
</s>
<s>
Králové	Králové	k2eAgMnSc1d1
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
a	a	k8xC
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
přijímají	přijímat	k5eAaImIp3nP
kapitulaci	kapitulace	k1gFnSc4
Akkonu	Akkon	k1gInSc2
(	(	kIx(
<g/>
Grandes	Grandes	k1gMnSc1
Chroniques	Chroniques	k1gMnSc1
de	de	k?
France	Franc	k1gMnSc4
<g/>
)	)	kIx)
<g/>
Král	Král	k1gMnSc1
Richard	Richard	k1gMnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
výpravy	výprava	k1gFnSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
roku	rok	k1gInSc2
1191	#num#	k4
vyrazil	vyrazit	k5eAaPmAgMnS
Richard	Richard	k1gMnSc1
I.	I.	kA
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
armádou	armáda	k1gFnSc7
po	po	k7c6
pobřežní	pobřežní	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
na	na	k7c4
jih	jih	k1gInSc4
k	k	k7c3
Jaffě	Jaffa	k1gFnSc3
a	a	k8xC
Askalonu	Askalon	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
opuštění	opuštění	k1gNnSc6
Akkonu	Akkon	k1gInSc2
byli	být	k5eAaImAgMnP
jeho	jeho	k3xOp3gMnPc1
muži	muž	k1gMnPc1
soustavně	soustavně	k6eAd1
odstřelováni	odstřelovat	k5eAaImNgMnP
šípy	šíp	k1gInPc7
vystřelenými	vystřelený	k2eAgInPc7d1
zpoza	zpoza	k7c2
pahorků	pahorek	k1gInPc2
nebo	nebo	k8xC
obtěžováni	obtěžovat	k5eAaImNgMnP
drobnými	drobný	k2eAgMnPc7d1
přepady	přepad	k1gInPc1
saladinových	saladinový	k2eAgMnPc2d1
jezdců	jezdec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadní	zadní	k2eAgInPc1d1
voje	voj	k1gInPc1
<g/>
,	,	kIx,
tvořené	tvořený	k2eAgFnPc1d1
francouzskými	francouzský	k2eAgNnPc7d1
rytíři	rytíř	k1gMnSc6
pod	pod	k7c7
vedením	vedení	k1gNnSc7
burgundského	burgundský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
Huga	Hugo	k1gMnSc2
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
dokonce	dokonce	k9
v	v	k7c4
jednu	jeden	k4xCgFnSc4
chvíli	chvíle	k1gFnSc4
obklíčeny	obklíčen	k2eAgFnPc1d1
a	a	k8xC
odříznuty	odříznout	k5eAaPmNgFnP
od	od	k7c2
hlavní	hlavní	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
díky	díky	k7c3
Richardově	Richardův	k2eAgFnSc3d1
včasné	včasný	k2eAgFnSc3d1
pomoci	pomoc	k1gFnSc3
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podařilo	podařit	k5eAaPmAgNnS
prstencem	prstenec	k1gInSc7
záškodníků	záškodník	k1gMnPc2
prorazit	prorazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
přetrvávajícímu	přetrvávající	k2eAgNnSc3d1
nebezpečí	nebezpečí	k1gNnSc3
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
pověřováni	pověřován	k2eAgMnPc1d1
obsazováním	obsazování	k1gNnSc7
nebezpečných	bezpečný	k2eNgFnPc2d1
a	a	k8xC
odpovědných	odpovědný	k2eAgFnPc2d1
pozic	pozice	k1gFnPc2
na	na	k7c6
čele	čelo	k1gNnSc6
vojska	vojsko	k1gNnSc2
a	a	k8xC
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
zadním	zadní	k2eAgInSc6d1
voji	voj	k1gInSc6
pouze	pouze	k6eAd1
členové	člen	k1gMnPc1
rytířských	rytířský	k2eAgInPc2d1
řádů	řád	k1gInPc2
templářů	templář	k1gMnPc2
a	a	k8xC
johanitů	johanita	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
odpovědnost	odpovědnost	k1gFnSc1
za	za	k7c4
obranu	obrana	k1gFnSc4
pochodových	pochodový	k2eAgFnPc2d1
linií	linie	k1gFnPc2
pak	pak	k6eAd1
připadla	připadnout	k5eAaPmAgFnS
pěchotě	pěchota	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těchto	tento	k3xDgNnPc2
zhruba	zhruba	k6eAd1
14	#num#	k4
000	#num#	k4
ozbrojenců	ozbrojenec	k1gMnPc2
bylo	být	k5eAaImAgNnS
rozděleno	rozdělit	k5eAaPmNgNnS
do	do	k7c2
dvou	dva	k4xCgInPc2
pochodových	pochodový	k2eAgInPc2d1
proudů	proud	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
se	se	k3xPyFc4
přesunovala	přesunovat	k5eAaImAgFnS
jízda	jízda	k1gFnSc1
a	a	k8xC
ekvipáže	ekvipáž	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
dlouhé	dlouhý	k2eAgFnSc6d1
linii	linie	k1gFnSc6
byli	být	k5eAaImAgMnP
rozmístěni	rozmístěn	k2eAgMnPc1d1
střelci	střelec	k1gMnPc1
z	z	k7c2
kuší	kuše	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
drželi	držet	k5eAaImAgMnP
v	v	k7c6
šachu	šach	k1gInSc6
neustále	neustále	k6eAd1
dotírajícího	dotírající	k2eAgMnSc2d1
protivníka	protivník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
obě	dva	k4xCgNnPc4
křídla	křídlo	k1gNnPc4
pravidelně	pravidelně	k6eAd1
střídal	střídat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
den	dna	k1gFnPc2
vojáci	voják	k1gMnPc1
odolávali	odolávat	k5eAaImAgMnP
neustálým	neustálý	k2eAgNnSc7d1
útokům	útok	k1gInPc3
muslimů	muslim	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ten	ten	k3xDgMnSc1
druhý	druhý	k4xOgMnSc1
pochodovali	pochodovat	k5eAaImAgMnP
v	v	k7c6
relativním	relativní	k2eAgNnSc6d1
bezpečí	bezpečí	k1gNnSc6
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zodpovědnost	zodpovědnost	k1gFnSc1
za	za	k7c4
zásobování	zásobování	k1gNnSc4
armády	armáda	k1gFnSc2
převzalo	převzít	k5eAaPmAgNnS
silné	silný	k2eAgNnSc1d1
křižácké	křižácký	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
brázdilo	brázdit	k5eAaImAgNnS
Středozemní	středozemní	k2eAgNnSc1d1
moře	moře	k1gNnSc1
po	po	k7c6
pravici	pravice	k1gFnSc6
kristových	kristový	k2eAgMnPc2d1
bojovníků	bojovník	k1gMnPc2
a	a	k8xC
dodávalo	dodávat	k5eAaImAgNnS
jim	on	k3xPp3gMnPc3
potraviny	potravina	k1gFnPc4
a	a	k8xC
vodu	voda	k1gFnSc4
kdekoliv	kdekoliv	k6eAd1
to	ten	k3xDgNnSc4
profil	profil	k1gInSc1
pobřeží	pobřeží	k1gNnSc4
jen	jen	k6eAd1
trochu	trochu	k6eAd1
dovoloval	dovolovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podmínky	podmínka	k1gFnPc1
pochodu	pochod	k1gInSc2
byly	být	k5eAaImAgFnP
mimořádně	mimořádně	k6eAd1
kruté	krutý	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
právě	právě	k9
pro	pro	k7c4
pěší	pěší	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horko	horko	k1gNnSc1
se	se	k3xPyFc4
stávalo	stávat	k5eAaImAgNnS
pro	pro	k7c4
muže	muž	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
dlouhé	dlouhý	k2eAgInPc4d1
hodiny	hodina	k1gFnSc2
vláčeli	vláčet	k5eAaImAgMnP
těžkou	těžký	k2eAgFnSc4d1
zbro	zbra	k1gMnSc5
<g/>
]	]	kIx)
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
nesnesitelným	snesitelný	k2eNgInSc7d1
a	a	k8xC
mnoho	mnoho	k4c1
z	z	k7c2
nich	on	k3xPp3gMnPc2
následkem	následkem	k7c2
přehřátí	přehřátí	k1gNnSc2
a	a	k8xC
žízně	žízeň	k1gFnSc2
zemřelo	zemřít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
jim	on	k3xPp3gMnPc3
Richard	Richard	k1gMnSc1
dopřával	dopřávat	k5eAaImAgInS
během	během	k7c2
dne	den	k1gInSc2
mnoho	mnoho	k4c1
přestávek	přestávka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
střídal	střídat	k5eAaImAgMnS
s	s	k7c7
krátkými	krátký	k2eAgInPc7d1
pochody	pochod	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižákům	křižák	k1gMnPc3
trvalo	trvat	k5eAaImAgNnS
čtyři	čtyři	k4xCgFnPc4
dny	dna	k1gFnPc4
než	než	k8xS
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
města	město	k1gNnSc2
Haify	Haifa	k1gFnSc2
<g/>
,	,	kIx,
vzdáleného	vzdálený	k2eAgInSc2d1
od	od	k7c2
Akkonu	Akkon	k1gInSc2
pouhých	pouhý	k2eAgInPc2d1
16	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Haifě	Haifa	k1gFnSc6
si	se	k3xPyFc3
křižáci	křižák	k1gMnPc1
odpočinuli	odpočinout	k5eAaPmAgMnP
a	a	k8xC
zanechali	zanechat	k5eAaPmAgMnP
zde	zde	k6eAd1
vše	všechen	k3xTgNnSc4
nepotřebné	potřebný	k2eNgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záhy	záhy	k6eAd1
pokračovali	pokračovat	k5eAaImAgMnP
dál	daleko	k6eAd2
k	k	k7c3
městu	město	k1gNnSc3
Caesarea	Caesare	k1gInSc2
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
dorazili	dorazit	k5eAaPmAgMnP
poslední	poslední	k2eAgInSc4d1
den	den	k1gInSc4
měsíce	měsíc	k1gInSc2
srpna	srpen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
sice	sice	k8xC
bývalo	bývat	k5eAaImAgNnS
obehnáno	obehnán	k2eAgNnSc1d1
opevněním	opevnění	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
Saladinovi	Saladinův	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
ho	on	k3xPp3gNnSc4
zcela	zcela	k6eAd1
pobořili	pobořit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křižákům	křižák	k1gInPc3
tak	tak	k6eAd1
nezbylo	zbýt	k5eNaPmAgNnS
nic	nic	k3yNnSc1
jiného	jiný	k2eAgNnSc2d1
<g/>
,	,	kIx,
než	než	k8xS
postupovat	postupovat	k5eAaImF
stále	stále	k6eAd1
kupředu	kupředu	k6eAd1
a	a	k8xC
zastavovat	zastavovat	k5eAaImF
pouze	pouze	k6eAd1
u	u	k7c2
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
tažné	tažný	k2eAgNnSc1d1
zvířectvo	zvířectvo	k1gNnSc1
<g/>
,	,	kIx,
koně	kůň	k1gMnPc1
a	a	k8xC
muži	muž	k1gMnPc1
<g/>
,	,	kIx,
dehydratování	dehydratování	k1gNnSc1
po	po	k7c6
celodenním	celodenní	k2eAgInSc6d1
pochodu	pochod	k1gInSc6
<g/>
,	,	kIx,
mohli	moct	k5eAaImAgMnP
dostatečně	dostatečně	k6eAd1
napojit	napojit	k5eAaPmF
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
se	se	k3xPyFc4
Richard	Richard	k1gMnSc1
I.	I.	kA
setkal	setkat	k5eAaPmAgMnS
se	s	k7c7
Saladinovým	Saladinový	k2eAgMnSc7d1
bratrem	bratr	k1gMnSc7
Malikem	Malik	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
podmínek	podmínka	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
muslimové	muslim	k1gMnPc1
podstoupí	podstoupit	k5eAaPmIp3nP
křižákům	křižák	k1gMnPc3
celou	celý	k2eAgFnSc4d1
Palestinu	Palestina	k1gFnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
ochoten	ochoten	k2eAgMnSc1d1
přistoupit	přistoupit	k5eAaPmF
na	na	k7c4
příměří	příměří	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uražený	uražený	k2eAgInSc1d1
Malik	Malik	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
nedokázal	dokázat	k5eNaPmAgMnS
na	na	k7c4
tuto	tento	k3xDgFnSc4
troufalost	troufalost	k1gFnSc4
odpovědět	odpovědět	k5eAaPmF
<g/>
,	,	kIx,
ihned	ihned	k6eAd1
rokování	rokování	k1gNnSc4
ukončil	ukončit	k5eAaPmAgMnS
a	a	k8xC
odjel	odjet	k5eAaPmAgMnS
zpět	zpět	k6eAd1
k	k	k7c3
bratrovi	bratr	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Když	když	k8xS
křižácké	křižácký	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
dorazilo	dorazit	k5eAaPmAgNnS
na	na	k7c4
dotek	dotek	k1gInSc4
s	s	k7c7
hlavní	hlavní	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
nepřítele	nepřítel	k1gMnSc2
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Saladin	Saladina	k1gFnPc2
snaží	snažit	k5eAaImIp3nS
vynutit	vynutit	k5eAaPmF
si	se	k3xPyFc3
bitvu	bitva	k1gFnSc4
v	v	k7c6
otevřeném	otevřený	k2eAgInSc6d1
terénu	terén	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
vhodný	vhodný	k2eAgInSc1d1
pro	pro	k7c4
jeho	jeho	k3xOp3gInPc4
jízdní	jízdní	k2eAgInPc4d1
oddíly	oddíl	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgFnSc1
krajina	krajina	k1gFnSc1
se	se	k3xPyFc4
rozkládala	rozkládat	k5eAaImAgFnS
právě	právě	k9
poblíž	poblíž	k7c2
Arsufu	Arsuf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
rozprostíral	rozprostírat	k5eAaImAgInS
zalesněný	zalesněný	k2eAgInSc1d1
pobřežní	pobřežní	k2eAgInSc1d1
pás	pás	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
připravit	připravit	k5eAaPmF
křižákům	křižák	k1gInPc3
překvapení	překvapení	k1gNnSc2
buď	buď	k8xC
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
sem	sem	k6eAd1
Saladin	Saladin	k2eAgMnSc1d1
umístil	umístit	k5eAaPmAgMnS
skryté	skrytý	k2eAgInPc4d1
houfy	houf	k1gInPc4
nebo	nebo	k8xC
by	by	kYmCp3nS
les	les	k1gInSc1
<g/>
,	,	kIx,
při	při	k7c6
postupu	postup	k1gInSc6
křižáků	křižák	k1gInPc2
<g/>
,	,	kIx,
mohl	moct	k5eAaImAgInS
zapálit	zapálit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
vším	veš	k1gFnPc3
však	však	k9
Richard	Richard	k1gMnSc1
I.	I.	kA
počítal	počítat	k5eAaImAgMnS
a	a	k8xC
byl	být	k5eAaImAgMnS
pevně	pevně	k6eAd1
rozhodnut	rozhodnout	k5eAaPmNgMnS
bitvu	bitva	k1gFnSc4
přijmout	přijmout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Armády	armáda	k1gFnPc1
</s>
<s>
Jízdní	jízdní	k2eAgMnSc1d1
lučištník	lučištník	k1gMnSc1
ze	z	k7c2
středověké	středověký	k2eAgFnSc2d1
turecké	turecký	k2eAgFnSc2d1
kroniky	kronika	k1gFnSc2
</s>
<s>
Křižáci	křižák	k1gMnPc1
</s>
<s>
Křižácký	křižácký	k2eAgMnSc1d1
pěší	pěší	k1gMnSc1
ozbrojenec	ozbrojenec	k1gMnSc1
byl	být	k5eAaImAgInS
obvykle	obvykle	k6eAd1
vystrojen	vystrojit	k5eAaPmNgInS
ve	v	k7c6
lněném	lněný	k2eAgInSc6d1
kabátci	kabátec	k1gInSc6
a	a	k8xC
na	na	k7c6
hlavě	hlava	k1gFnSc6
nosil	nosit	k5eAaImAgInS
koženou	kožený	k2eAgFnSc4d1
nebo	nebo	k8xC
kovovou	kovový	k2eAgFnSc4d1
přilbu	přilba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jeho	jeho	k3xOp3gInSc3
výzbroji	výzbroj	k1gInSc3
patřilo	patřit	k5eAaImAgNnS
těžké	těžký	k2eAgNnSc1d1
kopí	kopí	k1gNnSc1
<g/>
,	,	kIx,
válečná	válečný	k2eAgFnSc1d1
sekera	sekera	k1gFnSc1
<g/>
,	,	kIx,
kyj	kyj	k1gInSc1
nebo	nebo	k8xC
krátký	krátký	k2eAgInSc1d1
meč	meč	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
boji	boj	k1gInSc6
byla	být	k5eAaImAgFnS
křižácká	křižácký	k2eAgFnSc1d1
pěchota	pěchota	k1gFnSc1
velmi	velmi	k6eAd1
tvrdým	tvrdý	k2eAgMnSc7d1
protivníkem	protivník	k1gMnSc7
schopným	schopný	k2eAgMnPc3d1
klást	klást	k5eAaImF
urputný	urputný	k2eAgInSc4d1
odpor	odpor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
víře	víra	k1gFnSc3
a	a	k8xC
pevnému	pevný	k2eAgNnSc3d1
přesvědčení	přesvědčení	k1gNnSc3
o	o	k7c6
správnosti	správnost	k1gFnSc6
svého	svůj	k3xOyFgNnSc2
poslání	poslání	k1gNnSc2
bojovala	bojovat	k5eAaImAgFnS
s	s	k7c7
neumdlévajícím	umdlévající	k2eNgNnSc7d1
hrdinstvím	hrdinství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
byli	být	k5eAaImAgMnP
pěší	pěší	k2eAgMnPc1d1
bojovníci	bojovník	k1gMnPc1
přinuceni	přinucen	k2eAgMnPc1d1
okolnostmi	okolnost	k1gFnPc7
k	k	k7c3
ústupu	ústup	k1gInSc2
<g/>
,	,	kIx,
nepřítel	nepřítel	k1gMnSc1
je	být	k5eAaImIp3nS
ohrožoval	ohrožovat	k5eAaImAgMnS
z	z	k7c2
týla	týl	k1gInSc2
nebo	nebo	k8xC
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
strategie	strategie	k1gFnSc1
boje	boj	k1gInSc2
vyžadovala	vyžadovat	k5eAaImAgFnS
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
schopni	schopen	k2eAgMnPc1d1
bojovat	bojovat	k5eAaImF
i	i	k8xC
zády	záda	k1gNnPc7
ve	v	k7c6
směru	směr	k1gInSc6
pochodu	pochod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pružnost	pružnost	k1gFnSc1
křižácké	křižácký	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
učinila	učinit	k5eAaImAgFnS,k5eAaPmAgFnS
velký	velký	k2eAgInSc4d1
dojem	dojem	k1gInSc4
i	i	k9
na	na	k7c4
muslimské	muslimský	k2eAgMnPc4d1
kronikáře	kronikář	k1gMnPc4
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1
pěchota	pěchota	k1gFnSc1
seřazená	seřazený	k2eAgFnSc1d1
před	před	k7c7
jízdou	jízda	k1gFnSc7
stála	stát	k5eAaImAgFnS
pevně	pevně	k6eAd1
jako	jako	k8xC,k8xS
zeď	zeď	k1gFnSc1
a	a	k8xC
každý	každý	k3xTgMnSc1
voják	voják	k1gMnSc1
měl	mít	k5eAaImAgMnS
na	na	k7c6
sobě	sebe	k3xPyFc6
polštářovaný	polštářovaný	k2eAgInSc4d1
kabát	kabát	k1gInSc4
a	a	k8xC
kroužkové	kroužkový	k2eAgNnSc4d1
brnění	brnění	k1gNnSc4
tak	tak	k9
silné	silný	k2eAgNnSc1d1
a	a	k8xC
pevné	pevný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
proti	proti	k7c3
němu	on	k3xPp3gInSc3
byly	být	k5eAaImAgInP
naše	náš	k3xOp1gInPc1
šípy	šíp	k1gInPc1
neúčinné	účinný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viděl	vidět	k5eAaImAgMnS
sem	sem	k6eAd1
muže	muž	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
v	v	k7c6
sobě	sebe	k3xPyFc6
měli	mít	k5eAaImAgMnP
jeden	jeden	k4xCgInSc4
až	až	k8xS
deset	deset	k4xCc4
šípů	šíp	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
přitom	přitom	k6eAd1
se	se	k3xPyFc4
plahočili	plahočit	k5eAaImAgMnP
dál	daleko	k6eAd2
spolu	spolu	k6eAd1
s	s	k7c7
jejich	jejich	k3xOp3gFnPc7
kolonami	kolona	k1gFnPc7
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Beha	Beha	k1gMnSc1
al-Din	al-Din	k1gMnSc1
</s>
<s>
Zbroj	zbroj	k1gFnSc1
rytířů	rytíř	k1gMnPc2
na	na	k7c4
koních	kůň	k1gMnPc6
představovala	představovat	k5eAaImAgFnS
zejména	zejména	k9
těžká	těžký	k2eAgFnSc1d1
kroužková	kroužkový	k2eAgFnSc1d1
košile	košile	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
většinou	většinou	k6eAd1
kryla	krýt	k5eAaImAgFnS
celé	celý	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
tímto	tento	k3xDgInSc7
drátěným	drátěný	k2eAgInSc7d1
pancířem	pancíř	k1gInSc7
měli	mít	k5eAaImAgMnP
na	na	k7c6
sobě	se	k3xPyFc3
navlečené	navlečený	k2eAgNnSc4d1
plátěné	plátěný	k2eAgNnSc4d1
prádlo	prádlo	k1gNnSc4
<g/>
,	,	kIx,
koženou	kožený	k2eAgFnSc4d1
kazajku	kazajka	k1gFnSc4
a	a	k8xC
kožené	kožený	k2eAgFnPc1d1
nohavice	nohavice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
zbraním	zbraň	k1gFnPc3
rytíře	rytíř	k1gMnSc4
patřilo	patřit	k5eAaImAgNnS
lehké	lehký	k2eAgNnSc1d1
kopí	kopí	k1gNnSc1
<g/>
,	,	kIx,
dlouhý	dlouhý	k2eAgInSc1d1
dvoubřitý	dvoubřitý	k2eAgInSc1d1
meč	meč	k1gInSc1
a	a	k8xC
těžká	těžký	k2eAgFnSc1d1
válečná	válečný	k2eAgFnSc1d1
sekera	sekera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
sehráli	sehrát	k5eAaPmAgMnP
velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
i	i	k9
rytíři	rytíř	k1gMnPc1
vojenských	vojenský	k2eAgInPc2d1
řádů	řád	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInPc4
morálka	morálka	k1gFnSc1
a	a	k8xC
přísná	přísný	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
dovolila	dovolit	k5eAaPmAgFnS
těžkou	těžký	k2eAgFnSc4d1
jízdu	jízda	k1gFnSc4
využívat	využívat	k5eAaPmF,k5eAaImF
k	k	k7c3
velmi	velmi	k6eAd1
složitým	složitý	k2eAgFnPc3d1
bojovým	bojový	k2eAgFnPc3d1
operacím	operace	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Muslimové	muslim	k1gMnPc1
</s>
<s>
Saladinova	Saladinův	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
velice	velice	k6eAd1
kosmopolitní	kosmopolitní	k2eAgNnSc4d1
a	a	k8xC
bojovali	bojovat	k5eAaImAgMnP
v	v	k7c6
ní	on	k3xPp3gFnSc6
muži	muž	k1gMnPc1
celé	celá	k1gFnSc6
jeho	jeho	k3xOp3gFnSc2
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jádro	jádro	k1gNnSc1
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
vojska	vojsko	k1gNnSc2
tvořila	tvořit	k5eAaImAgFnS
těžká	těžký	k2eAgFnSc1d1
a	a	k8xC
lehká	lehký	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Itinerarium	itinerarium	k1gNnSc1
<g/>
,	,	kIx,
další	další	k2eAgInSc4d1
popis	popis	k1gInSc4
určitého	určitý	k2eAgMnSc2d1
svědka	svědek	k1gMnSc2
<g/>
,	,	kIx,
přibližuje	přibližovat	k5eAaImIp3nS
nebezpečí	nebezpečí	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
představovaly	představovat	k5eAaImAgInP
střely	střel	k1gInPc1
muslimů	muslim	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
</s>
<s>
Stále	stále	k6eAd1
se	se	k3xPyFc4
drželi	držet	k5eAaImAgMnP
vedle	vedle	k7c2
naší	náš	k3xOp1gFnSc2
postupující	postupující	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
snažili	snažit	k5eAaImAgMnP
se	se	k3xPyFc4
nám	my	k3xPp1nPc3
způsobit	způsobit	k5eAaPmF
škody	škoda	k1gFnPc4
<g/>
,	,	kIx,
stříleli	střílet	k5eAaImAgMnP
po	po	k7c6
nás	my	k3xPp1nPc2
šipkami	šipka	k1gFnPc7
a	a	k8xC
šípy	šíp	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
přilétali	přilétat	k5eAaImAgMnP
v	v	k7c6
takovém	takový	k3xDgNnSc6
množství	množství	k1gNnSc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
déšť	déšť	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běda	běda	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
koní	kůň	k1gMnPc2
padlo	padnout	k5eAaPmAgNnS,k5eAaImAgNnS
k	k	k7c3
zemi	zem	k1gFnSc3
zasaženo	zasažen	k2eAgNnSc4d1
střelami	střela	k1gFnPc7
<g/>
,	,	kIx,
mnoho	mnoho	k4c1
jich	on	k3xPp3gFnPc2
bylo	být	k5eAaImAgNnS
vážně	vážně	k6eAd1
zraněno	zranit	k5eAaPmNgNnS
a	a	k8xC
zemřeli	zemřít	k5eAaPmAgMnP
mnohem	mnohem	k6eAd1
později	pozdě	k6eAd2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Množství	množství	k1gNnSc1
šipek	šipka	k1gFnPc2
a	a	k8xC
šípů	šíp	k1gInPc2
bylo	být	k5eAaImAgNnS
takové	takový	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
armáda	armáda	k1gFnSc1
procházela	procházet	k5eAaImAgFnS
<g/>
,	,	kIx,
jste	být	k5eAaImIp2nP
nemohli	moct	k5eNaImAgMnP
najít	najít	k5eAaPmF
místo	místo	k1gNnSc4
o	o	k7c6
velikosti	velikost	k1gFnSc6
čtvereční	čtvereční	k2eAgFnSc2d1
stopy	stopa	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
něhož	jenž	k3xRgInSc2
by	by	kYmCp3nS
nebyl	být	k5eNaImAgMnS
zabodnut	zabodnut	k2eAgInSc4d1
šíp	šíp	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Itinerarium	itinerarium	k1gNnSc1
</s>
<s>
Pěchota	pěchota	k1gFnSc1
v	v	k7c6
Saladinově	Saladinův	k2eAgNnSc6d1
vojsku	vojsko	k1gNnSc6
nebyla	být	k5eNaImAgFnS
příliš	příliš	k6eAd1
zastoupena	zastoupit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příslušníci	příslušník	k1gMnPc1
pěších	pěší	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
účastnící	účastnící	k2eAgMnSc1d1
se	se	k3xPyFc4
bitvy	bitva	k1gFnSc2
byli	být	k5eAaImAgMnP
většinou	většina	k1gFnSc7
naverbovaní	naverbovaný	k2eAgMnPc1d1
žoldnéři	žoldnér	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
však	však	k9
dokázali	dokázat	k5eAaPmAgMnP
na	na	k7c4
protivníka	protivník	k1gMnSc4
tvrdě	tvrdě	k6eAd1
dorážet	dorážet	k5eAaImF
používáním	používání	k1gNnSc7
střelných	střelný	k2eAgFnPc2d1
a	a	k8xC
vrhacích	vrhací	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Střetnutí	střetnutí	k1gNnSc1
</s>
<s>
Blízký	blízký	k2eAgInSc4d1
východ	východ	k1gInSc4
kolem	kolem	k7c2
roku	rok	k1gInSc2
1190	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
armáda	armáda	k1gFnSc1
křižáků	křižák	k1gMnPc2
vynořila	vynořit	k5eAaPmAgFnS
z	z	k7c2
Arsúfského	Arsúfský	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
,	,	kIx,
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
Richard	Richard	k1gMnSc1
mimořádnou	mimořádný	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
jejímu	její	k3xOp3gNnSc3
uspořádání	uspořádání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
předem	předem	k6eAd1
ji	on	k3xPp3gFnSc4
rozdělil	rozdělit	k5eAaPmAgMnS
do	do	k7c2
pěti	pět	k4xCc2
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
čela	čelo	k1gNnSc2
postavil	postavit	k5eAaPmAgMnS
lučištníky	lučištník	k1gMnPc4
a	a	k8xC
teprve	teprve	k6eAd1
za	za	k7c4
ně	on	k3xPp3gMnPc4
rytířskou	rytířský	k2eAgFnSc4d1
jízdu	jízda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Templáři	templář	k1gMnPc1
zaujali	zaujmout	k5eAaPmAgMnP
postavení	postavení	k1gNnSc4
na	na	k7c6
pravém	pravý	k2eAgNnSc6d1
křídle	křídlo	k1gNnSc6
<g/>
,	,	kIx,
vedle	vedle	k7c2
nich	on	k3xPp3gInPc2
stáli	stát	k5eAaImAgMnP
bojovníci	bojovník	k1gMnPc1
z	z	k7c2
Anjou	Anja	k1gFnSc7
a	a	k8xC
oddíly	oddíl	k1gInPc1
Guye	Guy	k1gFnSc2
z	z	k7c2
Lusignanu	Lusignan	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
bratra	bratr	k1gMnSc2
Geoffroie	Geoffroie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Richard	Richard	k1gMnSc1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
Angličany	Angličan	k1gMnPc7
<g/>
,	,	kIx,
střežícími	střežící	k2eAgInPc7d1
Dračí	dračí	k2eAgFnSc4d1
standartu	standarta	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
Normany	Norman	k1gMnPc7
zaujal	zaujmout	k5eAaPmAgInS
postavení	postavení	k1gNnSc4
ve	v	k7c6
středu	střed	k1gInSc6
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlevo	vlevo	k6eAd1
od	od	k7c2
něj	on	k3xPp3gMnSc2
pak	pak	k6eAd1
stáli	stát	k5eAaImAgMnP
vlámští	vlámský	k2eAgMnPc1d1
a	a	k8xC
místní	místní	k2eAgMnPc1d1
baroni	baron	k1gMnPc1
vedení	vedení	k1gNnSc2
Jakubem	Jakub	k1gMnSc7
z	z	k7c2
Avesnes	Avesnesa	k1gFnPc2
a	a	k8xC
Francouzi	Francouz	k1gMnPc1
pod	pod	k7c7
velením	velení	k1gNnSc7
Huga	Hugo	k1gMnSc2
Burgundského	burgundský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špitálníci	špitálník	k1gMnPc1
(	(	kIx(
<g/>
johanité	johanita	k1gMnPc1
<g/>
)	)	kIx)
zaujali	zaujmout	k5eAaPmAgMnP
postavení	postavený	k2eAgMnPc1d1
na	na	k7c6
křídle	křídlo	k1gNnSc6
levém	levý	k2eAgNnSc6d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Obě	dva	k4xCgNnPc4
křídla	křídlo	k1gNnPc4
byla	být	k5eAaImAgFnS
navíc	navíc	k6eAd1
posílena	posílit	k5eAaPmNgFnS
silnými	silný	k2eAgInPc7d1
oddíly	oddíl	k1gInPc7
pěchoty	pěchota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Saladin	Saladin	k2eAgInSc1d1
útočil	útočit	k5eAaImAgInS
po	po	k7c4
celý	celý	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
do	do	k7c2
boje	boj	k1gInSc2
vyslal	vyslat	k5eAaPmAgMnS
pěchotu	pěchota	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
křižáky	křižák	k1gMnPc4
zasypala	zasypat	k5eAaPmAgFnS
střelami	střela	k1gFnPc7
z	z	k7c2
praků	prak	k1gInPc2
i	i	k9
luků	luk	k1gInPc2
a	a	k8xC
lehkými	lehký	k2eAgInPc7d1
oštěpy	oštěp	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masa	masa	k1gFnSc1
jezdectva	jezdectvo	k1gNnSc2
obešla	obejít	k5eAaPmAgFnS
pochodovou	pochodový	k2eAgFnSc4d1
linii	linie	k1gFnSc4
křižáků	křižák	k1gInPc2
zleva	zleva	k6eAd1
a	a	k8xC
neustálými	neustálý	k2eAgInPc7d1
útoky	útok	k1gInPc7
<g/>
,	,	kIx,
zejména	zejména	k9
na	na	k7c4
týl	týl	k1gInSc4
sestavy	sestava	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
pokoušela	pokoušet	k5eAaImAgFnS
narušit	narušit	k5eAaPmF
její	její	k3xOp3gNnSc4
uspořádání	uspořádání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řev	řev	k1gInSc1
trumpet	trumpeta	k1gFnPc2
a	a	k8xC
rachot	rachot	k1gInSc4
bubínků	bubínek	k1gInPc2
naplňoval	naplňovat	k5eAaImAgMnS
křesťany	křesťan	k1gMnPc4
téměř	téměř	k6eAd1
fyzickou	fyzický	k2eAgFnSc7d1
zuřivostí	zuřivost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čas	čas	k1gInSc1
od	od	k7c2
času	čas	k1gInSc2
se	se	k3xPyFc4
přiblížila	přiblížit	k5eAaPmAgFnS
muslimská	muslimský	k2eAgFnSc1d1
obrněná	obrněný	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
a	a	k8xC
dorážela	dorážet	k5eAaImAgFnS
na	na	k7c4
křižáky	křižák	k1gMnPc4
svými	svůj	k3xOyFgInPc7
palcáty	palcát	k1gInPc7
na	na	k7c6
dlouhých	dlouhý	k2eAgFnPc6d1
násadách	násada	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc1d3
tlak	tlak	k1gInSc1
doléhal	doléhat	k5eAaImAgInS
na	na	k7c4
zadní	zadní	k2eAgInSc4d1
voj	voj	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgNnSc6
těžce	těžce	k6eAd1
strádali	strádat	k5eAaImAgMnP
johanitští	johanitský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
ztratili	ztratit	k5eAaPmAgMnP
mnoho	mnoho	k4c4
koní	kůň	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gMnSc1
velmistr	velmistr	k1gMnSc1
<g/>
,	,	kIx,
Garnier	Garnier	k1gMnSc1
de	de	k?
Naplous	Naplous	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
několikrát	několikrát	k6eAd1
obrátil	obrátit	k5eAaPmAgMnS
na	na	k7c4
Richarda	Richard	k1gMnSc4
s	s	k7c7
žádostí	žádost	k1gFnSc7
o	o	k7c4
povolení	povolení	k1gNnSc4
výpadu	výpad	k1gInSc2
a	a	k8xC
rozprášení	rozprášení	k1gNnPc2
muslimů	muslim	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
jeho	jeho	k3xOp3gInPc4
návrhy	návrh	k1gInPc4
prozatím	prozatím	k6eAd1
odmítal	odmítat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čekal	čekat	k5eAaImAgMnS
na	na	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
budou	být	k5eAaImBp3nP
rychlí	rychlý	k2eAgMnPc1d1
koně	kůň	k1gMnPc1
muslimů	muslim	k1gMnPc2
unaveni	unavit	k5eAaPmNgMnP
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
pomalejší	pomalý	k2eAgMnPc1d2
rytíři	rytíř	k1gMnPc1
je	on	k3xPp3gNnSc4
budou	být	k5eAaImBp3nP
schopni	schopen	k2eAgMnPc1d1
dostihnout	dostihnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
směl	smět	k5eAaImAgInS
být	být	k5eAaImF
proveden	provést	k5eAaPmNgInS
až	až	k9
ve	v	k7c6
chvíli	chvíle	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
k	k	k7c3
němu	on	k3xPp3gNnSc3
osobně	osobně	k6eAd1
vydá	vydat	k5eAaPmIp3nS
rozkaz	rozkaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
postupujícím	postupující	k2eAgInSc7d1
dnem	den	k1gInSc7
na	na	k7c4
křižáky	křižák	k1gMnPc4
opět	opět	k6eAd1
stále	stále	k6eAd1
víc	hodně	k6eAd2
doléhala	doléhat	k5eAaImAgFnS
úmornost	úmornost	k1gFnSc1
palestinského	palestinský	k2eAgNnSc2d1
slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvířený	rozvířený	k2eAgInSc1d1
prach	prach	k1gInSc1
a	a	k8xC
neustálý	neustálý	k2eAgInSc1d1
rachot	rachot	k1gInSc1
muslimských	muslimský	k2eAgFnPc2d1
trumpet	trumpeta	k1gFnPc2
a	a	k8xC
bubnů	buben	k1gInPc2
i	i	k8xC
stoupající	stoupající	k2eAgInSc1d1
počet	počet	k1gInSc1
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
především	především	k9
mezi	mezi	k7c7
koňmi	kůň	k1gMnPc7
<g/>
,	,	kIx,
stále	stále	k6eAd1
více	hodně	k6eAd2
nahlodával	nahlodávat	k5eAaImAgInS
trpělivost	trpělivost	k1gFnSc4
křižáků	křižák	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
bojů	boj	k1gInPc2
postoupili	postoupit	k5eAaPmAgMnP
kupředu	kupředu	k6eAd1
pouze	pouze	k6eAd1
o	o	k7c4
necelé	celý	k2eNgInPc4d1
tři	tři	k4xCgInPc4
kilometry	kilometr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Útok	útok	k1gInSc1
</s>
<s>
Král	Král	k1gMnSc1
Richard	Richard	k1gMnSc1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
</s>
<s>
Johanitští	johanitský	k2eAgMnPc1d1
rytíři	rytíř	k1gMnPc1
v	v	k7c6
zadních	zadní	k2eAgFnPc6d1
řadách	řada	k1gFnPc6
se	se	k3xPyFc4
tlačili	tlačit	k5eAaImAgMnP
na	na	k7c4
francouzské	francouzský	k2eAgInPc4d1
oddíly	oddíl	k1gInPc4
před	před	k7c7
nimi	on	k3xPp3gFnPc7
a	a	k8xC
záhy	záhy	k6eAd1
hrozilo	hrozit	k5eAaImAgNnS
porušení	porušení	k1gNnSc1
pochodové	pochodový	k2eAgFnSc2d1
formace	formace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmistr	velmistr	k1gMnSc1
johanitů	johanita	k1gMnPc2
navíc	navíc	k6eAd1
cítil	cítit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
není	být	k5eNaImIp3nS
dále	daleko	k6eAd2
schopen	schopen	k2eAgMnSc1d1
snášet	snášet	k5eAaImF
potupu	potupa	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
znamenalo	znamenat	k5eAaImAgNnS
vyčkávání	vyčkávání	k1gNnSc1
bez	bez	k7c2
možnosti	možnost	k1gFnSc2
opětování	opětování	k1gNnSc1
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
rytířem	rytíř	k1gMnSc7
Baldwinem	Baldwin	k1gMnSc7
z	z	k7c2
Carew	Carew	k1gFnSc2
se	se	k3xPyFc4
tedy	tedy	k9
rozhodl	rozhodnout	k5eAaPmAgInS
neuposlechnout	uposlechnout	k5eNaPmF
Richardova	Richardův	k2eAgInSc2d1
rozkazu	rozkaz	k1gInSc2
a	a	k8xC
zaútočit	zaútočit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
těch	ten	k3xDgInPc2
dvou	dva	k4xCgInPc2
byl	být	k5eAaImAgInS
rytíř	rytíř	k1gMnSc1
<g/>
,	,	kIx,
maršál	maršál	k1gMnSc1
Špitálu	špitál	k1gInSc2
<g/>
,	,	kIx,
druhým	druhý	k4xOgNnSc7
Baldouin	Baldouin	k2eAgMnSc1d1
le	le	k?
Caron	Caron	k1gMnSc1
<g/>
,	,	kIx,
statečný	statečný	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
lev	lev	k1gMnSc1
<g/>
,	,	kIx,
druh	druh	k1gMnSc1
krále	král	k1gMnSc2
anglického	anglický	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
jej	on	k3xPp3gInSc4
přivedl	přivést	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
dva	dva	k4xCgMnPc1
začali	začít	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
útok	útok	k1gInSc4
se	s	k7c7
jménem	jméno	k1gNnSc7
Všemohoucího	Všemohoucí	k1gMnSc2
na	na	k7c6
rtech	ret	k1gInPc6
a	a	k8xC
křičeli	křičet	k5eAaImAgMnP
hlasitě	hlasitě	k6eAd1
Svatý	svatý	k1gMnSc1
Jiří	Jiří	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
Boží	božit	k5eAaImIp3nP
pak	pak	k6eAd1
obrátili	obrátit	k5eAaPmAgMnP
své	svůj	k3xOyFgMnPc4
koně	kůň	k1gMnSc4
proti	proti	k7c3
nepříteli	nepřítel	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Ambroise	Ambroise	k1gFnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zbytek	zbytek	k1gInSc1
Maltézských	maltézský	k2eAgMnPc2d1
rytířů	rytíř	k1gMnPc2
a	a	k8xC
některé	některý	k3yIgInPc1
francouzské	francouzský	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
se	se	k3xPyFc4
hnaly	hnát	k5eAaImAgFnP
za	za	k7c7
nimi	on	k3xPp3gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
akt	akt	k1gInSc4
vzpoury	vzpoura	k1gFnSc2
však	však	k9
nezpůsobil	způsobit	k5eNaPmAgInS
katastrofu	katastrofa	k1gFnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
by	by	kYmCp3nS
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
za	za	k7c2
jiných	jiný	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
stát	stát	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richarda	k1gFnPc2
na	na	k7c4
neposlušnost	neposlušnost	k1gFnSc4
johanitů	johanita	k1gMnPc2
zareagoval	zareagovat	k5eAaPmAgInS
s	s	k7c7
ráznou	rázný	k2eAgFnSc7d1
rozhodností	rozhodnost	k1gFnSc7
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
také	také	k9
docela	docela	k6eAd1
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c4
tuto	tento	k3xDgFnSc4
chvíli	chvíle	k1gFnSc4
se	se	k3xPyFc4
již	již	k9
sám	sám	k3xTgMnSc1
rozhodoval	rozhodovat	k5eAaImAgMnS
o	o	k7c6
provedení	provedení	k1gNnSc6
protiútoku	protiútok	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osobně	osobně	k6eAd1
se	se	k3xPyFc4
postavil	postavit	k5eAaPmAgMnS
do	do	k7c2
čela	čelo	k1gNnSc2
vojska	vojsko	k1gNnSc2
a	a	k8xC
mohutným	mohutný	k2eAgInSc7d1
úderem	úder	k1gInSc7
během	během	k7c2
několika	několik	k4yIc3
minut	minuta	k1gFnPc2
rozprášil	rozprášit	k5eAaPmAgInS
Saladinovu	Saladinův	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
do	do	k7c2
všech	všecek	k3xTgFnPc2
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Sám	sám	k3xTgMnSc1
jsem	být	k5eAaImIp1nS
viděl	vidět	k5eAaImAgMnS
jejich	jejich	k3xOp3gMnPc4
rytíře	rytíř	k1gMnPc4
shromážděné	shromážděný	k2eAgMnPc4d1
uprostřed	uprostřed	k7c2
pěchoty	pěchota	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
v	v	k7c6
rukou	ruka	k1gFnPc6
třímají	třímat	k5eAaImIp3nP
svá	svůj	k3xOyFgNnPc4
kopí	kopí	k1gNnPc4
a	a	k8xC
vydávají	vydávat	k5eAaPmIp3nP,k5eAaImIp3nP
bojový	bojový	k2eAgInSc4d1
pokřik	pokřik	k1gInSc4
jako	jako	k8xS,k8xC
jeden	jeden	k4xCgMnSc1
muž	muž	k1gMnSc1
<g/>
;	;	kIx,
pěchota	pěchota	k1gFnSc1
se	se	k3xPyFc4
rozestoupila	rozestoupit	k5eAaPmAgFnS
a	a	k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
se	se	k3xPyFc4
hnali	hnát	k5eAaImAgMnP
vpřed	vpřed	k6eAd1
v	v	k7c6
jediném	jediný	k2eAgInSc6d1
velkém	velký	k2eAgInSc6d1
útoku	útok	k1gInSc6
všemi	všecek	k3xTgInPc7
směry	směr	k1gInPc7
-	-	kIx~
někteří	některý	k3yIgMnPc1
proti	proti	k7c3
našemu	náš	k3xOp1gInSc3
levému	levý	k2eAgInSc3d1
křídu	křída	k1gFnSc4
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
proti	proti	k7c3
pravému	pravý	k2eAgInSc3d1
a	a	k8xC
někteří	některý	k3yIgMnPc1
proti	proti	k7c3
našemu	náš	k3xOp1gInSc3
středu	střed	k1gInSc3
<g/>
,	,	kIx,
dokud	dokud	k8xS
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
zhroucení	zhroucení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Beha	Beha	k1gMnSc1
al-Din	al-Din	k1gMnSc1
</s>
<s>
Křižáci	křižák	k1gMnPc1
pronásledovali	pronásledovat	k5eAaImAgMnP
prchajícího	prchající	k2eAgMnSc4d1
nepřítele	nepřítel	k1gMnSc4
celé	celý	k2eAgFnSc2d1
míle	míle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
byl	být	k5eAaImAgInS
nejnebezpečnější	bezpečný	k2eNgInSc1d3
okamžik	okamžik	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocitli	ocitnout	k5eAaPmAgMnP
se	se	k3xPyFc4
bez	bez	k7c2
podpory	podpora	k1gFnSc2
pěchoty	pěchota	k1gFnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
taktika	taktika	k1gFnSc1
muslimů	muslim	k1gMnPc2
závisela	záviset	k5eAaImAgFnS
především	především	k6eAd1
na	na	k7c6
principu	princip	k1gInSc6
ústupu	ústup	k1gInSc2
<g/>
,	,	kIx,
rychlého	rychlý	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
a	a	k8xC
opakovaného	opakovaný	k2eAgInSc2d1
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richardovi	Richardův	k2eAgMnPc1d1
se	se	k3xPyFc4
však	však	k9
podařilo	podařit	k5eAaPmAgNnS
vítězstvím	vítězství	k1gNnSc7
opojené	opojený	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
udržet	udržet	k5eAaPmF
na	na	k7c6
uzdě	uzda	k1gFnSc6
a	a	k8xC
nedovolil	dovolit	k5eNaPmAgMnS
jim	on	k3xPp3gMnPc3
příliš	příliš	k6eAd1
se	se	k3xPyFc4
rozptýlit	rozptýlit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Důsledky	důsledek	k1gInPc1
</s>
<s>
Středověké	středověký	k2eAgNnSc4d1
zobrazení	zobrazení	k1gNnSc4
bitvy	bitva	k1gFnSc2
Saladina	Saladina	k1gFnSc1
s	s	k7c7
Richerdem	Richerd	k1gInSc7
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
byla	být	k5eAaImAgFnS
sice	sice	k8xC
velkým	velký	k2eAgInSc7d1
křižáckým	křižácký	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
tento	tento	k3xDgInSc1
úspěch	úspěch	k1gInSc1
nemohl	moct	k5eNaImAgInS
být	být	k5eAaImF
srovnáván	srovnávat	k5eAaImNgInS
s	s	k7c7
drtivým	drtivý	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
Saladina	Saladin	k2eAgNnSc2d1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Hattínu	Hattín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saladinovo	Saladinův	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
i	i	k9
přes	přes	k7c4
ztrátu	ztráta	k1gFnSc4
7000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
stále	stále	k6eAd1
nebylo	být	k5eNaImAgNnS
zničeno	zničit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shromáždilo	shromáždit	k5eAaPmAgNnS
se	se	k3xPyFc4
již	již	k9
druhý	druhý	k4xOgInSc1
den	den	k1gInSc1
po	po	k7c6
bitvě	bitva	k1gFnSc6
a	a	k8xC
pro	pro	k7c4
Saladina	Saladina	k1gFnSc1
nebyl	být	k5eNaImAgInS
ve	v	k7c6
zdejších	zdejší	k2eAgInPc6d1
krajích	kraj	k1gInPc6
problém	problém	k1gInSc4
naverbovat	naverbovat	k5eAaPmF
nové	nový	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přesto	přesto	k8xC
se	se	k3xPyFc4
však	však	k9
muslimové	muslim	k1gMnPc1
soustředili	soustředit	k5eAaPmAgMnP
už	už	k6eAd1
jen	jen	k9
na	na	k7c4
obranu	obrana	k1gFnSc4
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
obě	dva	k4xCgFnPc4
strany	strana	k1gFnPc4
měla	mít	k5eAaImAgFnS
porážka	porážka	k1gFnSc1
a	a	k8xC
vítězství	vítězství	k1gNnSc1
spíš	spát	k5eAaImIp2nS
morální	morální	k2eAgInSc4d1
účinek	účinek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saladin	Saladina	k1gFnPc2
se	se	k3xPyFc4
po	po	k7c6
této	tento	k3xDgFnSc6
zkušenosti	zkušenost	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přišel	přijít	k5eAaPmAgMnS
o	o	k7c4
nimbus	nimbus	k1gInSc4
neporazitelného	porazitelný	k2eNgMnSc2d1
válečníka	válečník	k1gMnSc2
<g/>
,	,	kIx,
již	již	k6eAd1
nikdy	nikdy	k6eAd1
nepokusil	pokusit	k5eNaPmAgMnS
o	o	k7c4
bitvu	bitva	k1gFnSc4
s	s	k7c7
Richardem	Richard	k1gMnSc7
na	na	k7c6
otevřeném	otevřený	k2eAgNnSc6d1
prostranství	prostranství	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Záhy	záhy	k6eAd1
po	po	k7c6
vítězství	vítězství	k1gNnSc6
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
se	se	k3xPyFc4
Richardovi	Richard	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
dobýt	dobýt	k5eAaPmF
Jaffu	Jaffa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
vojskem	vojsko	k1gNnSc7
na	na	k7c4
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
usadil	usadit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toho	ten	k3xDgNnSc2
využil	využít	k5eAaPmAgInS
Saladin	Saladin	k2eAgInSc1d1
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
rozbořil	rozbořit	k5eAaPmAgInS
vzkvétající	vzkvétající	k2eAgInSc1d1
Askalon	Askalon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemohl	moct	k5eNaImAgMnS
si	se	k3xPyFc3
dovolit	dovolit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
toto	tento	k3xDgNnSc1
město	město	k1gNnSc1
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
do	do	k7c2
rukou	ruka	k1gFnPc2
křižáků	křižák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalšímu	další	k2eAgInSc3d1
postupu	postup	k1gInSc3
na	na	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
neměl	mít	k5eNaImAgMnS
Richard	Richard	k1gMnSc1
I.	I.	kA
dost	dost	k6eAd1
sil	síla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muslimský	muslimský	k2eAgInSc1d1
vládce	vládce	k1gMnPc4
při	při	k7c6
dalším	další	k2eAgNnSc6d1
tažení	tažení	k1gNnSc6
křižáků	křižák	k1gInPc2
ke	k	k7c3
Svatému	svatý	k2eAgNnSc3d1
městu	město	k1gNnSc3
důsledně	důsledně	k6eAd1
uskutečňoval	uskutečňovat	k5eAaImAgInS
taktiku	taktika	k1gFnSc4
spálené	spálený	k2eAgFnSc2d1
země	zem	k1gFnSc2
a	a	k8xC
Richard	Richard	k1gMnSc1
dal	dát	k5eAaPmAgMnS
nakonec	nakonec	k6eAd1
přednost	přednost	k1gFnSc4
vyjednávání	vyjednávání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saladin	Saladina	k1gFnPc2
tedy	tedy	k8xC
předložil	předložit	k5eAaPmAgMnS
anglickému	anglický	k2eAgMnSc3d1
králi	král	k1gMnSc3
mírový	mírový	k2eAgInSc4d1
návrh	návrh	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
něhož	jenž	k3xRgMnSc2
zpřístupní	zpřístupnit	k5eAaPmIp3nS
Boží	boží	k2eAgInSc1d1
hrob	hrob	k1gInSc1
západním	západní	k2eAgMnPc3d1
poutníkům	poutník	k1gMnPc3
a	a	k8xC
uzná	uznat	k5eAaPmIp3nS
hranice	hranice	k1gFnSc1
stávajících	stávající	k2eAgFnPc2d1
křižáckých	křižácký	k2eAgFnPc2d1
držav	država	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
tento	tento	k3xDgInSc4
návrh	návrh	k1gInSc4
asi	asi	k9
po	po	k7c6
roce	rok	k1gInSc6
složité	složitý	k2eAgFnSc2d1
diplomacie	diplomacie	k1gFnSc2
přijal	přijmout	k5eAaPmAgMnS
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
se	se	k3xPyFc4
zpět	zpět	k6eAd1
do	do	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
DUPUY	DUPUY	kA
<g/>
,	,	kIx,
Trevor	Trevor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenské	vojenský	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paha	Pahum	k1gNnPc1
<g/>
:	:	kIx,
Forma	forma	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7213	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
335	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
OBERMEIER	OBERMEIER	kA
<g/>
,	,	kIx,
Siegfried	Siegfried	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
–	–	k?
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
rytíř	rytíř	k1gMnSc1
a	a	k8xC
dobrodruh	dobrodruh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paha	Paha	k1gMnSc1
<g/>
:	:	kIx,
Ikar	Ikar	k1gMnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
242	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
39	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
107	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Obermeier	Obermeier	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Obermeier	Obermeira	k1gFnPc2
-	-	kIx~
s.	s.	k?
108	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
NICOLLE	NICOLLE	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
1191	#num#	k4
:	:	kIx,
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc4d1
srdce	srdce	k1gNnSc4
<g/>
,	,	kIx,
Saladin	Saladin	k2eAgInSc4d1
a	a	k8xC
zápas	zápas	k1gInSc4
o	o	k7c4
Jeruzalém	Jeruzalém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
96	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
2382	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
76	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Evans	Evans	k1gInSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
,	,	kIx,
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
<g/>
:	:	kIx,
Střet	střet	k1gInSc1
kříže	kříž	k1gInSc2
a	a	k8xC
půlměsíce	půlměsíc	k1gInSc2
<g/>
,	,	kIx,
Vojenská	vojenský	k2eAgFnSc1d1
historie	historie	k1gFnSc1
<g/>
,	,	kIx,
srpen	srpen	k1gInSc1
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Gillingham	Gillingham	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Lví	lví	k2eAgNnSc1d1
Srdce	srdce	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Nicolle	Nicolle	k1gFnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
,	,	kIx,
Saladin	Saladin	k2eAgMnSc1d1
a	a	k8xC
Saracéni	Saracén	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Smail	Smail	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
<g/>
C.	C.	kA
<g/>
,	,	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
1097	#num#	k4
<g/>
–	–	k?
<g/>
1193	#num#	k4
(	(	kIx(
<g/>
Cambridge	Cambridge	k1gFnSc2
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
nd	nd	k?
ed	ed	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
162	#num#	k4
<g/>
–	–	k?
<g/>
165	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hattínu	Hattín	k1gInSc2
</s>
<s>
Křížové	Křížové	k2eAgFnPc1d1
výpravy	výprava	k1gFnPc1
</s>
<s>
Jeruzalémské	jeruzalémský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Křižácko-ajjúbovské	Křižácko-ajjúbovský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Montgisard	Montgisard	k1gInSc1
(	(	kIx(
<g/>
1177	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mardž	Mardž	k1gFnSc1
Ajjún	Ajjún	k1gMnSc1
(	(	kIx(
<g/>
1179	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jakubův	Jakubův	k2eAgInSc4d1
brod	brod	k1gInSc4
(	(	kIx(
<g/>
1179	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hrad	hrad	k1gInSc1
Belvoir	Belvoir	k1gInSc1
(	(	kIx(
<g/>
1182	#num#	k4
<g/>
)	)	kIx)
•	•	k?
al-Fule	al-Fule	k1gFnSc1
(	(	kIx(
<g/>
1183	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kerak	Kerak	k1gInSc1
(	(	kIx(
<g/>
1183	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cressonské	Cressonský	k2eAgInPc1d1
prameny	pramen	k1gInPc1
(	(	kIx(
<g/>
1187	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Tiberias	Tiberias	k1gInSc1
(	(	kIx(
<g/>
1187	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hattín	Hattín	k1gInSc1
(	(	kIx(
<g/>
1187	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jeruzalém	Jeruzalém	k1gInSc1
(	(	kIx(
<g/>
1187	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tyros	Tyros	k1gInSc1
(	(	kIx(
<g/>
1187	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Akkon	Akkon	k1gInSc1
(	(	kIx(
<g/>
1189	#num#	k4
<g/>
-	-	kIx~
<g/>
1191	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arsuf	Arsuf	k1gInSc1
(	(	kIx(
<g/>
1191	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jaffa	Jaffa	k1gFnSc1
(	(	kIx(
<g/>
1192	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Damietta	Damietta	k1gFnSc1
(	(	kIx(
<g/>
1218	#num#	k4
<g/>
)	)	kIx)
•	•	k?
La	la	k1gNnSc1
Forbie	Forbie	k1gFnSc1
(	(	kIx(
<g/>
1244	#num#	k4
<g/>
)	)	kIx)
•	•	k?
al-Mansúra	al-Mansúra	k1gFnSc1
(	(	kIx(
<g/>
1250	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fariskur	Fariskur	k1gMnSc1
(	(	kIx(
<g/>
1250	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k5eAaPmF
<g/>
:	:	kIx,
<g/>
bold	bold	k6eAd1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc7
<g/>
:	:	kIx,
Křížové	Křížové	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
|	|	kIx~
Středověk	středověk	k1gInSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
