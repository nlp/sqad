<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arsufu	Arsuf	k1gInSc2
(	(	kIx(
<g/>
též	též	k9
bitva	bitva	k1gFnSc1
u	u	k7c2
Arsoufu	Arsouf	k1gInSc2
<g/>
)	)	kIx)
-	-	kIx~
byla	být	k5eAaImAgFnS
významným	významný	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
anglického	anglický	k2eAgMnSc2d1
krále	král	k1gMnSc2
Richarda	Richard	k1gMnSc2
I.	I.	kA
Lví	lví	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
čelních	čelní	k2eAgMnPc2d1
vůdců	vůdce	k1gMnPc2
třetí	třetí	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
,	,	kIx,
nad	nad	k7c7
muslimským	muslimský	k2eAgMnSc7d1
vládcem	vládce	k1gMnSc7
Saladinem	Saladin	k1gMnSc7
<g/>
.	.	kIx.
</s>