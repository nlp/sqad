<s>
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
ilustrační	ilustrační	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1904	#num#	k4
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1905	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
rok	rok	k1gInSc1
<g/>
,	,	kIx,
6	#num#	k4
měsíců	měsíc	k1gInPc2
a	a	k8xC
4	#num#	k4
týdny	týden	k1gInPc7
<g/>
)	)	kIx)
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Korejský	korejský	k2eAgInSc1d1
poloostrov	poloostrov	k1gInSc1
<g/>
,	,	kIx,
Mandžusko	Mandžusko	k1gNnSc1
<g/>
,	,	kIx,
Žluté	žlutý	k2eAgNnSc1d1
moře	moře	k1gNnSc1
</s>
<s>
casus	casus	k1gInSc4
belli	bell	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Spor	spor	k1gInSc1
o	o	k7c4
korejský	korejský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
a	a	k8xC
Mandžusko	Mandžusko	k1gNnSc4
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
</s>
<s>
finanční	finanční	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Černohorské	Černohorské	k2eAgNnSc2d1
knížectví	knížectví	k1gNnSc2
</s>
<s>
finanční	finanční	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Meidži	Meidzat	k5eAaPmIp1nS
</s>
<s>
Iwao	Iwao	k6eAd1
Ójama	Ójama	k1gFnSc1
</s>
<s>
Gentaró	Gentaró	k?
Kodama	Kodama	k1gFnSc1
</s>
<s>
Maresuke	Maresuke	k1gFnSc1
Nogi	Nog	k1gFnSc2
</s>
<s>
Heihačiró	Heihačiró	k?
Tógó	Tógó	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Alexej	Alexej	k1gMnSc1
Kuropatkin	Kuropatkin	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stěpan	Stěpan	k1gInSc1
Makarov	Makarov	k1gInSc1
†	†	k?
</s>
<s>
Zinovij	Zinovít	k5eAaPmRp2nS
Rožestvenskij	Rožestvenskij	k1gFnSc1
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Lekso	Leksa	k1gFnSc5
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
300	#num#	k4
000	#num#	k4
–	–	k?
500	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
</s>
<s>
500	#num#	k4
000	#num#	k4
–	–	k?
1	#num#	k4
000	#num#	k4
000	#num#	k4
vojáků	voják	k1gMnPc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
47	#num#	k4
387	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
173	#num#	k4
425	#num#	k4
zraněných	zraněný	k2eAgInPc2d1
27	#num#	k4
200	#num#	k4
nemocných	nemocný	k1gMnPc2
</s>
<s>
25	#num#	k4
331	#num#	k4
mrtvých	mrtvý	k2eAgInPc2d1
146	#num#	k4
032	#num#	k4
zraněných	zraněný	k2eAgInPc2d1
18	#num#	k4
830	#num#	k4
nemocných	nemocný	k2eAgInPc2d1,k2eNgInPc2d1
74	#num#	k4
369	#num#	k4
zajatých	zajatý	k2eAgInPc2d1
50	#num#	k4
688	#num#	k4
podvyživených	podvyživený	k2eAgMnPc2d1
</s>
<s>
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
byl	být	k5eAaImAgInS
válečný	válečný	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
od	od	k7c2
února	únor	k1gInSc2
1904	#num#	k4
do	do	k7c2
září	září	k1gNnSc2
1905	#num#	k4
na	na	k7c6
Dálném	dálný	k2eAgInSc6d1
východě	východ	k1gInSc6
mezi	mezi	k7c7
carským	carský	k2eAgNnSc7d1
Ruskem	Rusko	k1gNnSc7
a	a	k8xC
Japonským	japonský	k2eAgNnSc7d1
císařstvím	císařství	k1gNnSc7
o	o	k7c4
nadvládu	nadvláda	k1gFnSc4
nad	nad	k7c7
Mandžuskem	Mandžusko	k1gNnSc7
a	a	k8xC
Korejským	korejský	k2eAgInSc7d1
poloostrovem	poloostrov	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojovalo	bojovat	k5eAaImAgNnS
se	s	k7c7
převážně	převážně	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
(	(	kIx(
<g/>
a	a	k8xC
o	o	k7c6
území	území	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
patřilo	patřit	k5eAaImAgNnS
(	(	kIx(
<g/>
ať	ať	k9
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
či	či	k8xC
nepřímo	přímo	k6eNd1
<g/>
)	)	kIx)
slábnoucí	slábnoucí	k2eAgFnSc6d1
čínské	čínský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
dynastie	dynastie	k1gFnSc2
Čching	Čching	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
moři	moře	k1gNnSc6
se	se	k3xPyFc4
Japonci	Japonec	k1gMnPc1
nejprve	nejprve	k6eAd1
snažili	snažit	k5eAaImAgMnP
oslabit	oslabit	k5eAaPmF
a	a	k8xC
zablokovat	zablokovat	k5eAaPmF
jádro	jádro	k1gNnSc4
ruské	ruský	k2eAgFnSc2d1
tichooceánské	tichooceánský	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
v	v	k7c4
Port	port	k1gInSc4
Arthuru	Arthur	k1gInSc2
a	a	k8xC
nedovolit	dovolit	k5eNaPmF
jí	on	k3xPp3gFnSc7
narušit	narušit	k5eAaPmF
japonská	japonský	k2eAgNnPc1d1
vylodění	vylodění	k1gNnPc1
a	a	k8xC
přesuny	přesun	k1gInPc1
vojsk	vojsko	k1gNnPc2
<g/>
,	,	kIx,
či	či	k8xC
uprchnout	uprchnout	k5eAaPmF
do	do	k7c2
Vladivostoku	Vladivostok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
na	na	k7c6
souši	souš	k1gFnSc6
zahájili	zahájit	k5eAaPmAgMnP
obléhání	obléhání	k1gNnSc4
portarturské	portarturský	k2eAgFnSc2d1
pevnosti	pevnost	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
bylo	být	k5eAaImAgNnS
nutné	nutný	k2eAgNnSc1d1
vyřadit	vyřadit	k5eAaPmF
před	před	k7c7
připlutím	připlutí	k1gNnSc7
posil	posila	k1gFnPc2
z	z	k7c2
evropské	evropský	k2eAgFnSc2d1
části	část	k1gFnSc2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Port	porta	k1gFnPc2
Artur	Artur	k1gMnSc1
padl	padnout	k5eAaPmAgMnS,k5eAaImAgMnS
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1905	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonský	japonský	k2eAgInSc1d1
postup	postup	k1gInSc1
do	do	k7c2
Mandžuska	Mandžusko	k1gNnSc2
vyvrcholil	vyvrcholit	k5eAaPmAgInS
porážkou	porážka	k1gFnSc7
ruské	ruský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
v	v	k7c6
devatenáctidenní	devatenáctidenní	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Mukdenu	Mukden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruské	ruský	k2eAgFnPc4d1
snahy	snaha	k1gFnPc4
o	o	k7c4
zvrácení	zvrácení	k1gNnSc4
situace	situace	k1gFnSc2
definitivně	definitivně	k6eAd1
ukončila	ukončit	k5eAaPmAgFnS
porážka	porážka	k1gFnSc1
Rožestvenského	Rožestvenský	k2eAgInSc2d1
námořní	námořní	k2eAgFnSc2d1
evropské	evropský	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Cušimy	Cušima	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Předválečné	předválečný	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
expandovalo	expandovat	k5eAaImAgNnS
na	na	k7c6
celém	celý	k2eAgInSc6d1
Dálném	dálný	k2eAgInSc6d1
východě	východ	k1gInSc6
po	po	k7c6
celé	celá	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusové	Rusové	k2eAgFnSc1d1
zde	zde	k6eAd1
potřebovali	potřebovat	k5eAaImAgMnP
opěrný	opěrný	k2eAgInSc4d1
bod	bod	k1gInSc4
pro	pro	k7c4
své	svůj	k3xOyFgNnSc4
válečné	válečný	k2eAgNnSc4d1
loďstvo	loďstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
ruské	ruský	k2eAgInPc4d1
přístavy	přístav	k1gInPc4
na	na	k7c6
východě	východ	k1gInSc6
ovšem	ovšem	k9
přes	přes	k7c4
zimu	zima	k1gFnSc4
zamrzaly	zamrzat	k5eAaImAgInP
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
Rusové	Rus	k1gMnPc1
obrátili	obrátit	k5eAaPmAgMnP
svoji	svůj	k3xOyFgFnSc4
pozornost	pozornost	k1gFnSc4
k	k	k7c3
přístavům	přístav	k1gInPc3
v	v	k7c6
Mandžusku	Mandžusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
těchto	tento	k3xDgInPc2
přístavů	přístav	k1gInPc2
byl	být	k5eAaImAgInS
Port	port	k1gInSc1
Arthur	Arthura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
přístav	přístav	k1gInSc4
si	se	k3xPyFc3
ruské	ruský	k2eAgNnSc1d1
tichooceánské	tichooceánský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
zvolilo	zvolit	k5eAaPmAgNnS
jako	jako	k9
svůj	svůj	k3xOyFgInSc4
opěrný	opěrný	k2eAgInSc4d1
bod	bod	k1gInSc4
a	a	k8xC
ruská	ruský	k2eAgFnSc1d1
diplomacie	diplomacie	k1gFnSc1
dohodla	dohodnout	k5eAaPmAgFnS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
pronájem	pronájem	k1gInSc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
za	za	k7c4
ruskou	ruský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
se	s	k7c7
splácením	splácení	k1gNnSc7
čínských	čínský	k2eAgFnPc2d1
válečných	válečný	k2eAgFnPc2d1
reparací	reparace	k1gFnPc2
z	z	k7c2
první	první	k4xOgFnSc2
čínsko	čínsko	k6eAd1
<g/>
–	–	k?
<g/>
japonské	japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
mohla	moct	k5eAaImAgFnS
kontrolovat	kontrolovat	k5eAaImF
Žluté	žlutý	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
Korejský	korejský	k2eAgInSc1d1
záliv	záliv	k1gInSc1
a	a	k8xC
čínské	čínský	k2eAgNnSc1d1
pobřeží	pobřeží	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
od	od	k7c2
reforem	reforma	k1gFnPc2
Meidži	Meidž	k1gFnSc6
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
stalo	stát	k5eAaPmAgNnS
asijskou	asijský	k2eAgFnSc7d1
mocností	mocnost	k1gFnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozhodlo	rozhodnout	k5eAaPmAgNnS
vytvořit	vytvořit	k5eAaPmF
vlastní	vlastní	k2eAgNnSc1d1
koloniální	koloniální	k2eAgNnSc1d1
panství	panství	k1gNnSc1
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záměry	záměra	k1gFnSc2
Ruska	Rusko	k1gNnSc2
a	a	k8xC
Japonska	Japonsko	k1gNnSc2
se	se	k3xPyFc4
střetávaly	střetávat	k5eAaImAgFnP
v	v	k7c6
Mandžusku	Mandžusko	k1gNnSc6
a	a	k8xC
Koreji	Korea	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
podporovala	podporovat	k5eAaImAgFnS
Japonsko	Japonsko	k1gNnSc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
finančně	finančně	k6eAd1
<g/>
,	,	kIx,
výcvikem	výcvik	k1gInSc7
japonských	japonský	k2eAgMnPc2d1
námořních	námořní	k2eAgMnPc2d1
důstojníků	důstojník	k1gMnPc2
<g/>
,	,	kIx,
stavbou	stavba	k1gFnSc7
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záměrem	záměr	k1gInSc7
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Japonsko	Japonsko	k1gNnSc1
vybojovalo	vybojovat	k5eAaPmAgNnS
válku	válka	k1gFnSc4
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
za	za	k7c4
Británii	Británie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
se	se	k3xPyFc4
na	na	k7c4
válku	válka	k1gFnSc4
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
připravovalo	připravovat	k5eAaImAgNnS
od	od	k7c2
uzavření	uzavření	k1gNnSc2
míru	míra	k1gFnSc4
mezi	mezi	k7c7
Čínou	Čína	k1gFnSc7
a	a	k8xC
Japonskem	Japonsko	k1gNnSc7
(	(	kIx(
<g/>
program	program	k1gInSc1
výstavby	výstavba	k1gFnSc2
válečného	válečný	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
tzv.	tzv.	kA
6	#num#	k4
+	+	kIx~
6	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
bylo	být	k5eAaImAgNnS
informováno	informovat	k5eAaBmNgNnS
o	o	k7c6
japonských	japonský	k2eAgFnPc6d1
přípravách	příprava	k1gFnPc6
a	a	k8xC
mělo	mít	k5eAaImAgNnS
vlastní	vlastní	k2eAgInSc4d1
program	program	k1gInSc4
„	„	k?
<g/>
výstavby	výstavba	k1gFnSc2
lodí	loď	k1gFnPc2
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
Dálného	dálný	k2eAgInSc2d1
Východu	východ	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
války	válka	k1gFnSc2
</s>
<s>
Ústup	ústup	k1gInSc1
ruských	ruský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Mukdenu	Mukden	k1gInSc2
</s>
<s>
Válka	válka	k1gFnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
únoru	únor	k1gInSc6
1904	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
útokem	útok	k1gInSc7
Japonska	Japonsko	k1gNnSc2
na	na	k7c4
přístavy	přístav	k1gInPc4
Port	porta	k1gFnPc2
Arthur	Arthura	k1gFnPc2
a	a	k8xC
Čemulpcho	Čemulpcha	k1gFnSc5
provedenými	provedený	k2eAgFnPc7d1
ještě	ještě	k9
před	před	k7c7
vyhlášením	vyhlášení	k1gNnSc7
války	válka	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vnější	vnější	k2eAgFnSc6d1
rejdě	rejda	k1gFnSc6
Port	porta	k1gFnPc2
Arturu	Artur	k1gMnSc3
se	se	k3xPyFc4
japonským	japonský	k2eAgMnPc3d1
torpédoborcům	torpédoborec	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
poškodit	poškodit	k5eAaPmF
bitevní	bitevní	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
Cesarevič	Cesarevič	k1gInSc1
a	a	k8xC
Retvizan	Retvizan	k1gInSc1
a	a	k8xC
chráněný	chráněný	k2eAgInSc1d1
křižník	křižník	k1gInSc1
Pallada	Pallada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
boji	boj	k1gInSc6
u	u	k7c2
Čempulcha	Čempulch	k1gMnSc2
byly	být	k5eAaImAgFnP
obě	dva	k4xCgFnPc1
přítomné	přítomný	k2eAgFnPc1d1
ruské	ruský	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
(	(	kIx(
<g/>
chráněný	chráněný	k2eAgInSc4d1
křižník	křižník	k1gInSc4
Varjag	Varjaga	k1gFnPc2
a	a	k8xC
dělový	dělový	k2eAgInSc1d1
člun	člun	k1gInSc1
Korejec	Korejec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
nejprve	nejprve	k6eAd1
poškozeny	poškozen	k2eAgFnPc1d1
a	a	k8xC
poté	poté	k6eAd1
zničeny	zničit	k5eAaPmNgInP
vlastními	vlastní	k2eAgFnPc7d1
posádkami	posádka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS
období	období	k1gNnSc1
<g/>
,	,	kIx,
během	během	k7c2
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
Japonci	Japonec	k1gMnPc1
posilovali	posilovat	k5eAaImAgMnP
svou	svůj	k3xOyFgFnSc4
přítomnost	přítomnost	k1gFnSc4
v	v	k7c6
Koreji	Korea	k1gFnSc6
a	a	k8xC
občas	občas	k6eAd1
se	se	k3xPyFc4
pokoušeli	pokoušet	k5eAaImAgMnP
o	o	k7c4
námořní	námořní	k2eAgNnSc4d1
bombardování	bombardování	k1gNnSc4
a	a	k8xC
zablokování	zablokování	k1gNnSc4
Port	porta	k1gFnPc2
Arturu	Artur	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
flota	flota	k1gFnSc1
byla	být	k5eAaImAgFnS
zpočátku	zpočátku	k6eAd1
pasivní	pasivní	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
změnilo	změnit	k5eAaPmAgNnS
až	až	k9
s	s	k7c7
příjezdem	příjezd	k1gInSc7
nového	nový	k2eAgMnSc2d1
velitele	velitel	k1gMnSc2
–	–	k?
admirála	admirál	k1gMnSc2
Stěpana	Stěpan	k1gMnSc2
Osipoviče	Osipovič	k1gMnSc2
Makarova	Makarův	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
flotě	flotě	k6eAd1
pozvedl	pozvednout	k5eAaPmAgMnS
morálku	morálka	k1gFnSc4
a	a	k8xC
především	především	k9
aktivitu	aktivita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
zavedl	zavést	k5eAaPmAgMnS
pozorovatele	pozorovatel	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
naváděli	navádět	k5eAaImAgMnP
palbu	palba	k1gFnSc4
z	z	k7c2
lodí	loď	k1gFnPc2
v	v	k7c6
portarturské	portarturský	k2eAgFnSc6d1
rejdě	rejda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stěpan	Stěpany	k1gInPc2
Makarov	Makarovo	k1gNnPc2
ale	ale	k8xC
již	již	k6eAd1
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1904	#num#	k4
zahynul	zahynout	k5eAaPmAgMnS
na	na	k7c6
palubě	paluba	k1gFnSc6
bitevní	bitevní	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
Petropavlovsk	Petropavlovsk	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
potopila	potopit	k5eAaPmAgFnS
na	na	k7c6
japonských	japonský	k2eAgFnPc6d1
minách	mina	k1gFnPc6
při	při	k7c6
výpadu	výpad	k1gInSc6
na	na	k7c4
ochranu	ochrana	k1gFnSc4
torpédoborce	torpédoborec	k1gMnSc2
napadeného	napadený	k2eAgMnSc2d1
Japonci	Japonec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkáza	zkáza	k1gFnSc1
válečné	válečný	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
Petropavlovsk	Petropavlovsk	k1gInSc1
byla	být	k5eAaImAgFnS
způsobena	způsobit	k5eAaPmNgFnS
výbuchem	výbuch	k1gInSc7
vlastní	vlastní	k2eAgFnSc2d1
munice	munice	k1gFnSc2
<g/>
,	,	kIx,
iniciovaných	iniciovaný	k2eAgFnPc2d1
výbuchem	výbuch	k1gInSc7
japonské	japonský	k2eAgFnPc1d1
miny	mina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Makarovovi	Makarova	k1gMnSc6
byl	být	k5eAaImAgMnS
velitelem	velitel	k1gMnSc7
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
admirál	admirál	k1gMnSc1
Vitgeft	Vitgeft	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
ale	ale	k8xC
výrazně	výrazně	k6eAd1
pasivní	pasivní	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktivitu	aktivita	k1gFnSc4
nepřineslo	přinést	k5eNaPmAgNnS
ani	ani	k8xC
potopení	potopení	k1gNnSc1
dvou	dva	k4xCgFnPc2
japonských	japonský	k2eAgFnPc2d1
bitevních	bitevní	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
na	na	k7c6
minách	mina	k1gFnPc6
o	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
flota	flota	k1gFnSc1
disponovala	disponovat	k5eAaBmAgFnS
6	#num#	k4
válečnými	válečný	k2eAgFnPc7d1
loďmi	loď	k1gFnPc7
typu	typ	k1gInSc2
predreadnought	predreadnoughta	k1gFnPc2
<g/>
,	,	kIx,
japonská	japonský	k2eAgFnSc1d1
flota	flota	k1gFnSc1
měla	mít	k5eAaImAgFnS
4	#num#	k4
válečné	válečný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
typu	typ	k1gInSc2
predreadnought	predreadnought	k1gInSc1
a	a	k8xC
8	#num#	k4
pancéřových	pancéřový	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
floty	flota	k1gFnPc1
disponovaly	disponovat	k5eAaBmAgFnP
větším	veliký	k2eAgNnSc7d2
množstvím	množství	k1gNnSc7
chráněných	chráněný	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
<g/>
,	,	kIx,
torpédoborců	torpédoborec	k1gInPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
pomocných	pomocný	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
(	(	kIx(
<g/>
avíza	avízo	k1gNnPc4
<g/>
,	,	kIx,
minonosky	minonoska	k1gFnPc4
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Japonci	Japonec	k1gMnPc1
se	se	k3xPyFc4
poté	poté	k6eAd1
bez	bez	k7c2
výraznější	výrazný	k2eAgFnSc2d2
ruské	ruský	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
vylodili	vylodit	k5eAaPmAgMnP
na	na	k7c6
Liaotungském	Liaotungský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
a	a	k8xC
pozvolna	pozvolna	k6eAd1
se	se	k3xPyFc4
probíjeli	probíjet	k5eAaImAgMnP
k	k	k7c3
Port	port	k1gInSc1
Arturu	Artur	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
bojovali	bojovat	k5eAaImAgMnP
proti	proti	k7c3
ruské	ruský	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
v	v	k7c6
Mandžusku	Mandžusko	k1gNnSc6
(	(	kIx(
<g/>
bitva	bitva	k1gFnSc1
na	na	k7c6
řece	řeka	k1gFnSc6
Jalu	Jalus	k1gInSc2
a	a	k8xC
bitva	bitva	k1gFnSc1
na	na	k7c6
řece	řeka	k1gFnSc6
Ša-che	Ša-ch	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1
pokus	pokus	k1gInSc1
portarturské	portarturský	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
o	o	k7c4
únik	únik	k1gInSc4
skončil	skončit	k5eAaPmAgMnS
bitvou	bitva	k1gFnSc7
ve	v	k7c6
Žlutém	žlutý	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
rozhodnut	rozhodnout	k5eAaPmNgInS
šťastným	šťastný	k2eAgInSc7d1
japonským	japonský	k2eAgInSc7d1
výstřelem	výstřel	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
zabil	zabít	k5eAaPmAgMnS
ruského	ruský	k2eAgMnSc4d1
admirála	admirál	k1gMnSc4
Vitgefta	Vitgeft	k1gMnSc4
na	na	k7c6
velitelském	velitelský	k2eAgInSc6d1
můstku	můstek	k1gInSc6
lodě	loď	k1gFnSc2
Cesarevič	Cesarevič	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
už	už	k6eAd1
jedinou	jediný	k2eAgFnSc4d1
aktivitu	aktivita	k1gFnSc4
tichooceánské	tichooceánský	k2eAgFnSc2d1
floty	flota	k1gFnSc2
vyvíjely	vyvíjet	k5eAaImAgInP
tři	tři	k4xCgInPc1
křižníky	křižník	k1gInPc1
(	(	kIx(
<g/>
Gromoboj	Gromoboj	k1gInSc1
<g/>
,	,	kIx,
Rjurik	Rjurik	k1gMnSc1
a	a	k8xC
Rossija	Rossija	k1gMnSc1
<g/>
)	)	kIx)
dislokované	dislokovaný	k2eAgFnSc2d1
ve	v	k7c6
Vladivostoku	Vladivostok	k1gInSc6
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
chvíle	chvíle	k1gFnSc2
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
při	při	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
výpadů	výpad	k1gInPc2
potopen	potopen	k2eAgMnSc1d1
Rjurik	Rjurik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
několika	několik	k4yIc6
předchozích	předchozí	k2eAgInPc6d1
výpadech	výpad	k1gInPc6
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
podařilo	podařit	k5eAaPmAgNnS
potopit	potopit	k5eAaPmF
celkem	celkem	k6eAd1
15	#num#	k4
japonských	japonský	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Port	porta	k1gFnPc2
Arthur	Arthura	k1gFnPc2
kapituloval	kapitulovat	k5eAaBmAgInS
po	po	k7c6
11	#num#	k4
měsíčním	měsíční	k2eAgNnSc6d1
obležení	obležení	k1gNnSc6
a	a	k8xC
těžkých	těžký	k2eAgFnPc6d1
japonských	japonský	k2eAgFnPc6d1
i	i	k8xC
ruských	ruský	k2eAgFnPc6d1
ztrátách	ztráta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Japonci	Japonec	k1gMnPc1
v	v	k7c6
bojích	boj	k1gInPc6
oficiálně	oficiálně	k6eAd1
ztratili	ztratit	k5eAaPmAgMnP
57	#num#	k4
780	#num#	k4
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
názory	názor	k1gInPc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
předčasnou	předčasný	k2eAgFnSc4d1
kapitulaci	kapitulace	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c4
Port	port	k1gInSc4
Arthuru	Arthur	k1gInSc2
byly	být	k5eAaImAgFnP
značné	značný	k2eAgFnPc1d1
zásoby	zásoba	k1gFnPc1
munice	munice	k1gFnSc2
a	a	k8xC
potravin	potravina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
Baltu	Balt	k1gInSc2
byla	být	k5eAaImAgFnS
ještě	ještě	k9
před	před	k7c7
pádem	pád	k1gInSc7
Port	porta	k1gFnPc2
Arturu	Artur	k1gMnSc3
poslána	poslat	k5eAaPmNgFnS
část	část	k1gFnSc1
baltské	baltský	k2eAgFnSc2d1
eskadry	eskadra	k1gFnSc2
na	na	k7c4
pomoc	pomoc	k1gFnSc4
tichomořské	tichomořský	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
ruské	ruský	k2eAgNnSc1d1
válečné	válečný	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
nazývané	nazývaný	k2eAgFnSc2d1
2	#num#	k4
<g/>
.	.	kIx.
tichomořská	tichomořský	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
<g/>
,	,	kIx,
vedené	vedený	k2eAgNnSc1d1
admirálem	admirál	k1gMnSc7
Rožestvenským	Rožestvenský	k2eAgFnPc3d1
bylo	být	k5eAaImAgNnS
poraženo	porazit	k5eAaPmNgNnS
v	v	k7c6
námořní	námořní	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Cušimy	Cušima	k1gFnSc2
v	v	k7c6
květnu	květen	k1gInSc6
1905	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgNnSc1d1
loďstvo	loďstvo	k1gNnSc1
bylo	být	k5eAaImAgNnS
výrazně	výrazně	k6eAd1
silnější	silný	k2eAgMnSc1d2
než	než	k8xS
ruské	ruský	k2eAgNnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
hlavní	hlavní	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
potopení	potopení	k1gNnSc2
nových	nový	k2eAgInPc2d1
ruských	ruský	k2eAgInPc2d1
predreadnoughtů	predreadnought	k1gInPc2
třídy	třída	k1gFnSc2
Borodino	Borodin	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
jejich	jejich	k3xOp3gNnSc1
přetížení	přetížení	k1gNnSc1
naloženým	naložený	k2eAgNnSc7d1
uhlím	uhlí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
pancéřový	pancéřový	k2eAgInSc1d1
pás	pás	k1gInSc1
přetížených	přetížený	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgInS
pod	pod	k7c4
hladinu	hladina	k1gFnSc4
moře	moře	k1gNnSc2
a	a	k8xC
tak	tak	k6eAd1
nemohl	moct	k5eNaImAgMnS
chránit	chránit	k5eAaImF
tyto	tento	k3xDgFnPc4
lodě	loď	k1gFnPc4
před	před	k7c7
japonskými	japonský	k2eAgInPc7d1
granáty	granát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
pozemní	pozemní	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
byla	být	k5eAaImAgFnS
ještě	ještě	k6eAd1
před	před	k7c7
Cušimou	Cušima	k1gFnSc7
definitivně	definitivně	k6eAd1
poražena	poražen	k2eAgFnSc1d1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Mukdenu	Mukden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostřednictvím	prostřednictvím	k7c2
amerického	americký	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Theodora	Theodor	k1gMnSc4
Roosevelta	Roosevelt	k1gMnSc4
byl	být	k5eAaImAgMnS
5	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1905	#num#	k4
v	v	k7c6
Portsmouthu	Portsmouth	k1gInSc6
uzavřen	uzavřít	k5eAaPmNgInS
mír	mír	k1gInSc1
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
stranami	strana	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Důsledky	důsledek	k1gInPc1
války	válka	k1gFnSc2
</s>
<s>
Výsledky	výsledek	k1gInPc1
míru	mír	k1gInSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
přijaty	přijmout	k5eAaPmNgInP
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
nevolí	nevole	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
sice	sice	k8xC
získalo	získat	k5eAaPmAgNnS
území	území	k1gNnSc4
Port	porta	k1gFnPc2
Arthuru	Arthur	k1gInSc2
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
ostrova	ostrov	k1gInSc2
Sachalin	Sachalin	k1gInSc4
a	a	k8xC
Koreu	Korea	k1gFnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
nezískalo	získat	k5eNaPmAgNnS
žádné	žádný	k3yNgFnPc4
válečné	válečný	k2eAgFnPc4d1
reparace	reparace	k1gFnPc4
od	od	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
a	a	k8xC
USA	USA	kA
si	se	k3xPyFc3
nepřály	přát	k5eNaImAgFnP
výrazné	výrazný	k2eAgNnSc4d1
posílení	posílení	k1gNnSc4
Japonska	Japonsko	k1gNnSc2
a	a	k8xC
proto	proto	k8xC
naléhaly	naléhat	k5eAaImAgFnP,k5eAaBmAgFnP
na	na	k7c4
Japonsko	Japonsko	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
uzavřelo	uzavřít	k5eAaPmAgNnS
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
nemělo	mít	k5eNaImAgNnS
další	další	k2eAgInSc4d1
finanční	finanční	k2eAgInSc4d1
prostředky	prostředek	k1gInPc4
pro	pro	k7c4
vedení	vedení	k1gNnSc4
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusku	Rusko	k1gNnSc6
finanční	finanční	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
nechyběly	chybět	k5eNaImAgFnP
<g/>
,	,	kIx,
avšak	avšak	k8xC
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
území	území	k1gNnSc6
probíhaly	probíhat	k5eAaImAgInP
nepokoje	nepokoj	k1gInPc1
a	a	k8xC
bouře	bouř	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
vedly	vést	k5eAaImAgInP
k	k	k7c3
první	první	k4xOgFnSc3
ruské	ruský	k2eAgFnSc3d1
revoluci	revoluce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonsko	Japonsko	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
rozhodující	rozhodující	k2eAgInSc4d1
vliv	vliv	k1gInSc4
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Mandžusku	Mandžusko	k1gNnSc6
a	a	k8xC
hlavní	hlavní	k2eAgInSc1d1
vliv	vliv	k1gInSc1
v	v	k7c6
Koreji	Korea	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korea	Korea	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
Japonskem	Japonsko	k1gNnSc7
anektována	anektován	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Technika	technika	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
ve	v	k7c6
válce	válka	k1gFnSc6
</s>
<s>
Tato	tento	k3xDgFnSc1
válka	válka	k1gFnSc1
měla	mít	k5eAaImAgFnS
některá	některý	k3yIgNnPc4
technická	technický	k2eAgNnPc4d1
specifika	specifikon	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hromadné	hromadný	k2eAgNnSc1d1
používání	používání	k1gNnSc4
kulometů	kulomet	k1gInPc2
a	a	k8xC
použití	použití	k1gNnSc2
ostnatého	ostnatý	k2eAgInSc2d1
drátu	drát	k1gInSc2
při	při	k7c6
obraně	obrana	k1gFnSc6
Port	porta	k1gFnPc2
Arthuru	Arthur	k1gInSc2
předznamenávalo	předznamenávat	k5eAaImAgNnS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
budou	být	k5eAaImBp3nP
vypadat	vypadat	k5eAaPmF,k5eAaImF
o	o	k7c4
10	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
boje	boj	k1gInPc4
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Námořní	námořní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
předchozích	předchozí	k2eAgInPc2d1
větších	veliký	k2eAgInPc2d2
námořních	námořní	k2eAgInPc2d1
střetů	střet	k1gInPc2
ve	v	k7c6
španělsko-americké	španělsko-americký	k2eAgFnSc6d1
válce	válka	k1gFnSc6
vyrovnanější	vyrovnaný	k2eAgInSc1d2
<g/>
.	.	kIx.
</s>
<s>
Plně	plně	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
ukázala	ukázat	k5eAaPmAgFnS
možnost	možnost	k1gFnSc4
minové	minový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námořní	námořní	k2eAgFnPc1d1
miny	mina	k1gFnPc1
dokázaly	dokázat	k5eAaPmAgFnP
během	během	k7c2
války	válka	k1gFnSc2
potopit	potopit	k5eAaPmF
tři	tři	k4xCgFnPc4
bitevní	bitevní	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
a	a	k8xC
napáchaly	napáchat	k5eAaBmAgInP
ještě	ještě	k6eAd1
mnoho	mnoho	k4c4
dalších	další	k2eAgFnPc2d1
škod	škoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměrně	poměrně	k6eAd1
neúčinná	účinný	k2eNgFnSc1d1
se	se	k3xPyFc4
naopak	naopak	k6eAd1
ukázala	ukázat	k5eAaPmAgNnP
torpéda	torpédo	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
kromě	kromě	k7c2
úvodního	úvodní	k2eAgInSc2d1
útoku	útok	k1gInSc2
Japonců	Japonec	k1gMnPc2
na	na	k7c4
Port	port	k1gInSc4
Arthur	Arthura	k1gFnPc2
žádný	žádný	k3yNgInSc4
úspěch	úspěch	k1gInSc4
nezaznamenala	zaznamenat	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
flota	flota	k1gFnSc1
sestávala	sestávat	k5eAaImAgFnS
z	z	k7c2
14	#num#	k4
moderních	moderní	k2eAgFnPc2d1
velkých	velký	k2eAgFnPc2d1
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
(	(	kIx(
<g/>
6	#num#	k4
predreadnoughtů	predreadnought	k1gInPc2
a	a	k8xC
8	#num#	k4
pancéřových	pancéřový	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
10	#num#	k4
lodí	loď	k1gFnPc2
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
v	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
2	#num#	k4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
,	,	kIx,
1	#num#	k4
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
1	#num#	k4
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
straně	strana	k1gFnSc6
Ruska	Rusko	k1gNnSc2
pouze	pouze	k6eAd1
3	#num#	k4
velké	velký	k2eAgFnPc1d1
válečné	válečný	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
byly	být	k5eAaImAgFnP
postaveny	postaven	k2eAgInPc1d1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
,	,	kIx,
1	#num#	k4
v	v	k7c6
USA	USA	kA
a	a	k8xC
2	#num#	k4
v	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
velkých	velký	k2eAgFnPc2d1
válečných	válečný	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
ruskými	ruský	k2eAgFnPc7d1
loděnicemi	loděnice	k1gFnPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc4
technická	technický	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
zaostávala	zaostávat	k5eAaImAgFnS
za	za	k7c7
západními	západní	k2eAgFnPc7d1
velmocemi	velmoc	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
byla	být	k5eAaImAgFnS
hlavní	hlavní	k2eAgFnSc1d1
příčina	příčina	k1gFnSc1
Japonského	japonský	k2eAgNnSc2d1
vítězství	vítězství	k1gNnSc2
ve	v	k7c6
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
Rusko-Japonské	rusko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
byly	být	k5eAaImAgFnP
započaty	započat	k2eAgFnPc1d1
výrazné	výrazný	k2eAgFnPc1d1
reformy	reforma	k1gFnPc1
ve	v	k7c6
vedení	vedení	k1gNnSc6
ruského	ruský	k2eAgNnSc2d1
námořnictva	námořnictvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Velitelská	velitelský	k2eAgFnSc1d1
loď	loď	k1gFnSc1
admirála	admirál	k1gMnSc2
Tógóa	Tógóus	k1gMnSc2
<g/>
,	,	kIx,
predreadnought	predreadnought	k1gInSc1
Mikasa	Mikasa	k1gFnSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
po	po	k7c6
válce	válka	k1gFnSc6
rekonstruována	rekonstruován	k2eAgFnSc1d1
a	a	k8xC
dnes	dnes	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jako	jako	k8xC,k8xS
námořní	námořní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jediný	jediný	k2eAgInSc4d1
zachovaný	zachovaný	k2eAgInSc4d1
predreadnougth	predreadnougth	k1gInSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Velká	velký	k2eAgFnSc1d1
ruská	ruský	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
Р	Р	k?
<g/>
́	́	k?
<g/>
С	С	k?
<g/>
́	́	k?
<g/>
Н	Н	k?
В	В	k?
<g/>
́	́	k?
1904	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
CONNAUGHTON	CONNAUGHTON	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vycházející	vycházející	k2eAgFnSc7d1
slunce	slunce	k1gNnSc4
a	a	k8xC
skolený	skolený	k2eAgMnSc1d1
medvěd	medvěd	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BB	BB	kA
<g/>
/	/	kIx~
<g/>
art	art	k?
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7341	#num#	k4
<g/>
-	-	kIx~
<g/>
371	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
JELÍNEK	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
:	:	kIx,
Port	port	k1gInSc1
Artur	Artur	k1gMnSc1
1904	#num#	k4
<g/>
-	-	kIx~
<g/>
1905	#num#	k4
<g/>
:	:	kIx,
Válka	válka	k1gFnSc1
začala	začít	k5eAaPmAgFnS
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
745	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JELÍNEK	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
:	:	kIx,
Port	port	k1gInSc1
Artur	Artur	k1gMnSc1
1904	#num#	k4
<g/>
-	-	kIx~
<g/>
1905	#num#	k4
<g/>
:	:	kIx,
Porážky	porážka	k1gFnPc4
a	a	k8xC
ústupy	ústup	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
746	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JELÍNEK	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
:	:	kIx,
Port	port	k1gInSc1
Artur	Artur	k1gMnSc1
1904	#num#	k4
<g/>
-	-	kIx~
<g/>
1905	#num#	k4
<g/>
:	:	kIx,
Zánik	zánik	k1gInSc1
eskadry	eskadra	k1gFnSc2
<g/>
,	,	kIx,
pád	pád	k1gInSc4
pevnosti	pevnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
920	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
JELÍNEK	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lodě	loď	k1gFnPc4
války	válka	k1gFnSc2
rusko-japonské	rusko-japonský	k2eAgFnSc2d1
1904	#num#	k4
<g/>
-	-	kIx~
<g/>
1905	#num#	k4
<g/>
,	,	kIx,
Díl	díl	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruské	ruský	k2eAgFnSc2d1
námořní	námořní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
:	:	kIx,
I.	I.	kA
Tichooceánská	tichooceánský	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tichooceánská	tichooceánský	k2eAgFnSc1d1
eskadra	eskadra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
:	:	kIx,
Kanon	kanon	k1gInSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2012	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Námořní	námořní	k2eAgInSc1d1
žurnál	žurnál	k1gInSc1
historický	historický	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
JELÍNEK	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lodě	loď	k1gFnPc4
války	válka	k1gFnSc2
rusko-japonské	rusko-japonský	k2eAgFnSc2d1
1904	#num#	k4
<g/>
-	-	kIx~
<g/>
1905	#num#	k4
<g/>
,	,	kIx,
Díl	díl	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonské	japonský	k2eAgFnSc2d1
námořní	námořní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
:	:	kIx,
Kanon	kanon	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Námořní	námořní	k2eAgInSc1d1
žurnál	žurnál	k1gInSc1
historický	historický	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
KODET	KODET	kA
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Port	porta	k1gFnPc2
Arthur	Arthur	k1gMnSc1
1904	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
16	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
11	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
258	#num#	k4
<g/>
-	-	kIx~
<g/>
270	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
6097	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cušima	Cušima	k1gFnSc1
<g/>
:	:	kIx,
Poslední	poslední	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
rusko-japonské	rusko-japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
919	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
JELÍNEK	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
rusko-japonská	rusko-japonský	k2eAgFnSc1d1
1904	#num#	k4
<g/>
-	-	kIx~
<g/>
1905	#num#	k4
<g/>
:	:	kIx,
První	první	k4xOgFnSc1
válka	válka	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
r-j-valka	r-j-valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
webnode	webnod	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
</s>
<s>
POLÁŠEK	Polášek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opevnění	opevnění	k1gNnSc1
za	za	k7c4
rusko	rusko	k1gNnSc4
japonské	japonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1904	#num#	k4
–	–	k?
1905	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
polni-opevneni	polni-opevnen	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
websnadno	websnadno	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
</s>
<s>
POLÁŠEK	Polášek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
BANNERMAN	BANNERMAN	kA
<g/>
,	,	kIx,
A.	A.	kA
Vývoj	vývoj	k1gInSc1
ruských	ruský	k2eAgInPc2d1
zákopů	zákop	k1gInPc2
u	u	k7c2
Port	porta	k1gFnPc2
Arturu	Artur	k1gMnSc3
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
scribd	scribd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
POLÁŠEK	Polášek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
TULLOCH	TULLOCH	kA
<g/>
,	,	kIx,
J.	J.	kA
W.	W.	kA
G.	G.	kA
Obranná	obranný	k2eAgNnPc4d1
postavení	postavení	k1gNnPc4
2	#num#	k4
<g/>
.	.	kIx.
japonské	japonský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
severně	severně	k6eAd1
od	od	k7c2
Tieh-ling	Tieh-ling	k1gInSc1
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
scribd	scribd	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
SCHLANGE	SCHLANGE	kA
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
1904-05	1904-05	k4
díl	díl	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
valka	valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2003-12-10	2003-12-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
SCHLANGE	SCHLANGE	kA
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
1904-05	1904-05	k4
díl	díl	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
valka	valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2003-12-10	2003-12-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
SCHLANGE	SCHLANGE	kA
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
1904-05	1904-05	k4
díl	díl	k1gInSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
valka	valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2003-12-10	2003-12-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
SCHLANGE	SCHLANGE	kA
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
1904-05	1904-05	k4
díl	díl	k1gInSc1
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
valka	valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2003-12-11	2003-12-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Japan-Russia	Japan-Russia	k1gFnSc1
War	War	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
cityofart	cityofart	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Russo-Japanese	Russo-Japanese	k1gFnSc1
War	War	k1gFnSc2
Research	Researcha	k1gFnPc2
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
russojapanesewar	russojapanesewar	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Obrazový	obrazový	k2eAgInSc1d1
zpravodaj	zpravodaj	k1gInSc1
z	z	k7c2
bojiště	bojiště	k1gNnSc2
<g/>
.	.	kIx.
1904	#num#	k4
<g/>
-	-	kIx~
<g/>
1906	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2336	#num#	k4
<g/>
-	-	kIx~
<g/>
2162	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
–	–	k?
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
Bitvy	bitva	k1gFnSc2
</s>
<s>
Port	port	k1gInSc1
ArthurN	ArthurN	k1gFnSc2
•	•	k?
ČemulpchoN	ČemulpchoN	k1gFnSc2
•	•	k?
Jalu	Jalus	k1gInSc2
•	•	k?
Nanšan	Nanšan	k1gInSc1
•	•	k?
Telisu	Telis	k1gInSc2
•	•	k?
Hitači	Hitač	k1gInSc6
MaruN	Maruna	k1gFnPc2
•	•	k?
Průsmyk	průsmyk	k1gInSc1
Motien	Motien	k1gInSc1
•	•	k?
Ta-š	Ta-š	k1gInSc1
<g/>
'	'	kIx"
<g/>
-čchiao	-čchiao	k6eAd1
•	•	k?
Port	port	k1gInSc1
Arthur	Arthur	k1gMnSc1
•	•	k?
Si-mu-čcheng	Si-mu-čcheng	k1gMnSc1
•	•	k?
Žluté	žlutý	k2eAgFnSc3d1
mořeN	mořen	k2eAgMnSc1d1
•	•	k?
UlsanN	UlsanN	k1gMnSc1
•	•	k?
KorsakovN	KorsakovN	k1gMnSc1
•	•	k?
Liao-jang	Liao-jang	k1gMnSc1
•	•	k?
Ša-che	Ša-che	k1gInSc1
•	•	k?
Sandepu	Sandep	k1gInSc2
•	•	k?
Mukden	Mukdno	k1gNnPc2
•	•	k?
CušimaN	CušimaN	k1gFnSc2
•	•	k?
Sachalin	Sachalin	k1gInSc1
N	N	kA
–	–	k?
námořní	námořní	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rusko	Rusko	k1gNnSc1
|	|	kIx~
Japonsko	Japonsko	k1gNnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
|	|	kIx~
Loďstvo	loďstvo	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
125314	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4129105-0	4129105-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85116039	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85116039	#num#	k4
</s>
