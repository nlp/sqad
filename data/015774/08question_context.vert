<s>
Rusko-japonská	rusko-japonský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
byl	být	k5eAaImAgInS
válečný	válečný	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
od	od	k7c2
února	únor	k1gInSc2
1904	#num#	k4
do	do	k7c2
září	září	k1gNnSc2
1905	#num#	k4
na	na	k7c6
Dálném	dálný	k2eAgInSc6d1
východě	východ	k1gInSc6
mezi	mezi	k7c7
carským	carský	k2eAgNnSc7d1
Ruskem	Rusko	k1gNnSc7
a	a	k8xC
Japonským	japonský	k2eAgNnSc7d1
císařstvím	císařství	k1gNnSc7
o	o	k7c4
nadvládu	nadvláda	k1gFnSc4
nad	nad	k7c7
Mandžuskem	Mandžusko	k1gNnSc7
a	a	k8xC
Korejským	korejský	k2eAgInSc7d1
poloostrovem	poloostrov	k1gInSc7
<g/>
.	.	kIx.
</s>