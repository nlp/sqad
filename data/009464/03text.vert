<p>
<s>
Pinotage	Pinotage	k1gFnSc1	Pinotage
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
odrůda	odrůda	k1gFnSc1	odrůda
révy	réva	k1gFnSc2	réva
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
tam	tam	k6eAd1	tam
vyšlechtěna	vyšlechtit	k5eAaPmNgFnS	vyšlechtit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
jako	jako	k8xC	jako
kříženec	kříženec	k1gMnSc1	kříženec
Pinot	Pinot	k1gMnSc1	Pinot
noir	noir	k1gMnSc1	noir
a	a	k8xC	a
Cinsault	Cinsault	k1gMnSc1	Cinsault
(	(	kIx(	(
<g/>
Cinsault	Cinsault	k1gMnSc1	Cinsault
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Hermitage	Hermitage	k1gInSc1	Hermitage
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Pinotage	Pinotage	k1gFnSc1	Pinotage
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
také	také	k9	také
zdaleka	zdaleka	k6eAd1	zdaleka
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
vín	víno	k1gNnPc2	víno
této	tento	k3xDgFnSc2	tento
odrůdy	odrůda	k1gFnSc2	odrůda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Odrůdu	odrůda	k1gFnSc4	odrůda
Pinotage	Pinotag	k1gFnSc2	Pinotag
vyšlechtil	vyšlechtit	k5eAaPmAgMnS	vyšlechtit
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
Abraham	Abraham	k1gMnSc1	Abraham
Izak	Izak	k1gMnSc1	Izak
Perold	Perold	k1gMnSc1	Perold
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
profesor	profesor	k1gMnSc1	profesor
vinařství	vinařství	k1gNnSc4	vinařství
na	na	k7c4	na
Stellenbosch	Stellenbosch	k1gInSc4	Stellenbosch
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Perold	Perold	k1gInSc1	Perold
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
spojit	spojit	k5eAaPmF	spojit
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
vlastnosti	vlastnost	k1gFnPc4	vlastnost
robustního	robustní	k2eAgInSc2d1	robustní
Hermitage	Hermitag	k1gInSc2	Hermitag
a	a	k8xC	a
Pinot	Pinot	k1gMnSc1	Pinot
noir	noir	k1gMnSc1	noir
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hroznů	hrozen	k1gInPc2	hrozen
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
náročné	náročný	k2eAgFnPc1d1	náročná
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
<g/>
.	.	kIx.	.
</s>
<s>
Perold	Perold	k1gMnSc1	Perold
zasadil	zasadit	k5eAaPmAgMnS	zasadit
čtyři	čtyři	k4xCgFnPc4	čtyři
sazenice	sazenice	k1gFnPc4	sazenice
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
křížení	křížení	k1gNnSc2	křížení
v	v	k7c6	v
zahradě	zahrada	k1gFnSc6	zahrada
na	na	k7c6	na
Welgevallenské	Welgevallenský	k2eAgFnSc6d1	Welgevallenský
experimentální	experimentální	k2eAgFnSc6d1	experimentální
farmě	farma	k1gFnSc6	farma
a	a	k8xC	a
zapomněl	zapomenout	k5eAaPmAgMnS	zapomenout
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
univerzita	univerzita	k1gFnSc1	univerzita
vyslala	vyslat	k5eAaPmAgFnS	vyslat
tým	tým	k1gInSc4	tým
k	k	k7c3	k
zušlechtění	zušlechtění	k1gNnSc3	zušlechtění
a	a	k8xC	a
údržbě	údržba	k1gFnSc3	údržba
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgInSc2	tento
týmu	tým	k1gInSc2	tým
byl	být	k5eAaImAgMnS	být
mladý	mladý	k2eAgMnSc1d1	mladý
asistent	asistent	k1gMnSc1	asistent
Charlie	Charlie	k1gMnSc1	Charlie
Niehaus	Niehaus	k1gMnSc1	Niehaus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
o	o	k7c6	o
sazenicích	sazenice	k1gFnPc6	sazenice
věděl	vědět	k5eAaImAgMnS	vědět
a	a	k8xC	a
zachránil	zachránit	k5eAaPmAgMnS	zachránit
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
likvidací	likvidace	k1gFnSc7	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
rostliny	rostlina	k1gFnPc1	rostlina
byly	být	k5eAaImAgFnP	být
přesazeny	přesadit	k5eAaPmNgFnP	přesadit
do	do	k7c2	do
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
školy	škola	k1gFnSc2	škola
Elsenburg	Elsenburg	k1gInSc1	Elsenburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
Peroldův	Peroldův	k2eAgMnSc1d1	Peroldův
nástupce	nástupce	k1gMnSc1	nástupce
C.	C.	kA	C.
J.	J.	kA	J.
Theron	Theron	k1gMnSc1	Theron
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
pak	pak	k6eAd1	pak
společně	společně	k6eAd1	společně
vybrali	vybrat	k5eAaPmAgMnP	vybrat
vhodné	vhodný	k2eAgInPc4d1	vhodný
klony	klon	k1gInPc4	klon
a	a	k8xC	a
nově	nově	k6eAd1	nově
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
odrůdu	odrůda	k1gFnSc4	odrůda
nazvali	nazvat	k5eAaBmAgMnP	nazvat
pinotage	pinotage	k6eAd1	pinotage
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
víno	víno	k1gNnSc1	víno
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
v	v	k7c6	v
Elsenburgu	Elsenburg	k1gInSc6	Elsenburg
z	z	k7c2	z
první	první	k4xOgFnSc2	první
komerční	komerční	k2eAgFnSc2d1	komerční
výsadby	výsadba	k1gFnSc2	výsadba
v	v	k7c4	v
Myrtle	Myrtle	k1gFnPc4	Myrtle
Grove	Groev	k1gFnSc2	Groev
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
odrůdy	odrůda	k1gFnSc2	odrůda
Abraham	Abraham	k1gMnSc1	Abraham
Perold	Perold	k1gMnSc1	Perold
však	však	k9	však
výsledek	výsledek	k1gInSc4	výsledek
svého	svůj	k3xOyFgNnSc2	svůj
šlechtění	šlechtění	k1gNnSc2	šlechtění
stěží	stěží	k6eAd1	stěží
mohl	moct	k5eAaImAgInS	moct
ochutnat	ochutnat	k5eAaPmF	ochutnat
<g/>
,	,	kIx,	,
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
totiž	totiž	k9	totiž
zemřelPrvní	zemřelPrvní	k2eAgNnSc1d1	zemřelPrvní
uznání	uznání	k1gNnSc1	uznání
přišlo	přijít	k5eAaPmAgNnS	přijít
<g/>
,	,	kIx,	,
když	když	k8xS	když
víno	víno	k1gNnSc1	víno
Bellevue	bellevue	k1gFnSc2	bellevue
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
z	z	k7c2	z
Pinotage	Pinotag	k1gInSc2	Pinotag
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
na	na	k7c4	na
Cape	capat	k5eAaImIp3nS	capat
Wine	Wine	k1gInSc1	Wine
Show	show	k1gFnSc2	show
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
snadné	snadný	k2eAgNnSc4d1	snadné
pěstování	pěstování	k1gNnSc4	pěstování
Pinotage	Pinotag	k1gInPc1	Pinotag
vyvolaly	vyvolat	k5eAaPmAgInP	vyvolat
vlnu	vlna	k1gFnSc4	vlna
výsadby	výsadba	k1gFnSc2	výsadba
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
světové	světový	k2eAgFnSc2d1	světová
výsadby	výsadba	k1gFnSc2	výsadba
Pinotage	Pinotag	k1gFnSc2	Pinotag
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
7000	[number]	k4	7000
ha	ha	kA	ha
(	(	kIx(	(
<g/>
7,4	[number]	k4	7,4
%	%	kIx~	%
plochy	plocha	k1gFnSc2	plocha
vinic	vinice	k1gFnPc2	vinice
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
třetí	třetí	k4xOgFnSc1	třetí
nejpěstovanější	pěstovaný	k2eAgFnSc1d3	nejpěstovanější
modrou	modrý	k2eAgFnSc7d1	modrá
odrůdou	odrůda	k1gFnSc7	odrůda
(	(	kIx(	(
<g/>
za	za	k7c7	za
odrůdami	odrůda	k1gFnPc7	odrůda
Cabernet	Cabernet	k1gMnSc1	Cabernet
Sauvignon	Sauvignon	k1gMnSc1	Sauvignon
a	a	k8xC	a
Syrah	Syrah	k1gMnSc1	Syrah
<g/>
)	)	kIx)	)
a	a	k8xC	a
šestou	šestý	k4xOgFnSc7	šestý
celkem	celkem	k6eAd1	celkem
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
tržně	tržně	k6eAd1	tržně
mimořádně	mimořádně	k6eAd1	mimořádně
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
domácí	domácí	k2eAgNnSc1d1	domácí
křížení	křížení	k1gNnSc1	křížení
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
vinařských	vinařský	k2eAgFnPc2d1	vinařská
tradic	tradice	k1gFnPc2	tradice
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
z	z	k7c2	z
asi	asi	k9	asi
3	[number]	k4	3
milionů	milion	k4xCgInPc2	milion
litrů	litr	k1gInPc2	litr
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
na	na	k7c4	na
5	[number]	k4	5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
litrů	litr	k1gInPc2	litr
prodaných	prodaný	k2eAgInPc2d1	prodaný
doma	doma	k6eAd1	doma
a	a	k8xC	a
19	[number]	k4	19
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
litrů	litr	k1gInPc2	litr
exportovaných	exportovaný	k2eAgInPc2d1	exportovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
Pinotage	Pinotage	k1gFnSc1	Pinotage
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
také	také	k9	také
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandě	Zéland	k1gInSc6	Zéland
je	být	k5eAaImIp3nS	být
94	[number]	k4	94
akrů	akr	k1gInPc2	akr
(	(	kIx(	(
<g/>
38	[number]	k4	38
ha	ha	kA	ha
<g/>
)	)	kIx)	)
Pinotage	Pinotag	k1gMnSc2	Pinotag
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
jsou	být	k5eAaImIp3nP	být
výsadby	výsadba	k1gFnPc1	výsadba
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Víno	víno	k1gNnSc1	víno
==	==	k?	==
</s>
</p>
<p>
<s>
Pinotage	Pinotage	k6eAd1	Pinotage
dává	dávat	k5eAaImIp3nS	dávat
výrazné	výrazný	k2eAgNnSc1d1	výrazné
tmavě	tmavě	k6eAd1	tmavě
rudé	rudý	k2eAgNnSc1d1	Rudé
<g/>
,	,	kIx,	,
zemité	zemitý	k2eAgNnSc1d1	zemité
víno	víno	k1gNnSc1	víno
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
bezcukerný	bezcukerný	k2eAgInSc4d1	bezcukerný
extrakt	extrakt	k1gInSc4	extrakt
a	a	k8xC	a
taniny	tanin	k1gInPc4	tanin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
standardní	standardní	k2eAgFnSc7d1	standardní
součástí	součást	k1gFnSc7	součást
červených	červený	k2eAgFnPc2d1	červená
kupáží	kupáž	k1gFnPc2	kupáž
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
produkce	produkce	k1gFnSc2	produkce
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
trhu	trh	k1gInSc6	trh
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
asi	asi	k9	asi
19	[number]	k4	19
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
litrů	litr	k1gInPc2	litr
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
právě	právě	k9	právě
z	z	k7c2	z
JAR	jaro	k1gNnPc2	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Tržní	tržní	k2eAgInSc1d1	tržní
úspěch	úspěch	k1gInSc1	úspěch
zřejmě	zřejmě	k6eAd1	zřejmě
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odrůda	odrůda	k1gFnSc1	odrůda
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jihoafrických	jihoafrický	k2eAgFnPc6d1	Jihoafrická
podmínkách	podmínka	k1gFnPc6	podmínka
velmi	velmi	k6eAd1	velmi
produktivní	produktivní	k2eAgFnPc1d1	produktivní
a	a	k8xC	a
víno	víno	k1gNnSc1	víno
je	být	k5eAaImIp3nS	být
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
jeho	jeho	k3xOp3gMnPc1	jeho
konkurenti	konkurent	k1gMnPc1	konkurent
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
se	se	k3xPyFc4	se
exportuje	exportovat	k5eAaBmIp3nS	exportovat
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
a	a	k8xC	a
lahvuje	lahvovat	k5eAaImIp3nS	lahvovat
se	se	k3xPyFc4	se
v	v	k7c6	v
cílových	cílový	k2eAgFnPc6d1	cílová
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
se	se	k3xPyFc4	se
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
v	v	k7c6	v
originálních	originální	k2eAgFnPc6d1	originální
lahvích	lahev	k1gFnPc6	lahev
<g/>
.	.	kIx.	.
</s>
<s>
Kritikové	kritik	k1gMnPc1	kritik
pinotáži	pinotáž	k1gFnSc3	pinotáž
vyčítají	vyčítat	k5eAaImIp3nP	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
bývá	bývat	k5eAaImIp3nS	bývat
cítit	cítit	k5eAaImF	cítit
po	po	k7c6	po
amyl-	amyl-	k?	amyl-
nebo	nebo	k8xC	nebo
izoamylacetátu	izoamylacetát	k1gInSc2	izoamylacetát
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
odlakovač	odlakovač	k1gInSc1	odlakovač
nehtů	nehet	k1gInPc2	nehet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	Tom	k1gMnSc3	Tom
se	se	k3xPyFc4	se
však	však	k9	však
lze	lze	k6eAd1	lze
vyhnout	vyhnout	k5eAaPmF	vyhnout
vhodným	vhodný	k2eAgInSc7d1	vhodný
technologickým	technologický	k2eAgInSc7d1	technologický
postupem	postup	k1gInSc7	postup
<g/>
.	.	kIx.	.
</s>
<s>
Pinotage	Pinotage	k6eAd1	Pinotage
se	se	k3xPyFc4	se
hodí	hodit	k5eAaImIp3nS	hodit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
barikového	barikový	k2eAgNnSc2d1	barikové
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
snese	snést	k5eAaPmIp3nS	snést
i	i	k9	i
delší	dlouhý	k2eAgFnSc4d2	delší
archivaci	archivace	k1gFnSc4	archivace
<g/>
.	.	kIx.	.
</s>
<s>
Novějšími	nový	k2eAgInPc7d2	novější
trendem	trend	k1gInSc7	trend
je	být	k5eAaImIp3nS	být
produkce	produkce	k1gFnSc1	produkce
pinotáží	pinotáž	k1gFnPc2	pinotáž
s	s	k7c7	s
tóny	tón	k1gInPc7	tón
kouře	kouř	k1gInSc2	kouř
<g/>
,	,	kIx,	,
čokolády	čokoláda	k1gFnSc2	čokoláda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
