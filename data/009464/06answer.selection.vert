<s>
Odrůdu	odrůda	k1gFnSc4	odrůda
Pinotage	Pinotag	k1gFnSc2	Pinotag
vyšlechtil	vyšlechtit	k5eAaPmAgMnS	vyšlechtit
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
Abraham	Abraham	k1gMnSc1	Abraham
Izak	Izak	k1gMnSc1	Izak
Perold	Perold	k1gMnSc1	Perold
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
profesor	profesor	k1gMnSc1	profesor
vinařství	vinařství	k1gNnSc4	vinařství
na	na	k7c4	na
Stellenbosch	Stellenbosch	k1gInSc4	Stellenbosch
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
