<s>
Systém	systém	k1gInSc1	systém
Elo	Ela	k1gFnSc5	Ela
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
střetávají	střetávat	k5eAaImIp3nP	střetávat
dvojice	dvojice	k1gFnPc1	dvojice
hráčů	hráč	k1gMnPc2	hráč
nebo	nebo	k8xC	nebo
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
