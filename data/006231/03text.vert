<s>
Elo	Ela	k1gFnSc5	Ela
(	(	kIx(	(
<g/>
též	též	k9	též
koeficient	koeficient	k1gInSc1	koeficient
Elo	Ela	k1gFnSc5	Ela
nebo	nebo	k8xC	nebo
rating	rating	k1gInSc1	rating
Elo	Ela	k1gFnSc5	Ela
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statistické	statistický	k2eAgNnSc1d1	statistické
ohodnocení	ohodnocení	k1gNnSc1	ohodnocení
výkonnosti	výkonnost	k1gFnSc2	výkonnost
hráče	hráč	k1gMnSc2	hráč
či	či	k8xC	či
týmu	tým	k1gInSc2	tým
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
jeho	jeho	k3xOp3gFnPc2	jeho
her	hra	k1gFnPc2	hra
podle	podle	k7c2	podle
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Arpad	Arpad	k1gInSc1	Arpad
Elo	Ela	k1gFnSc5	Ela
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
Elo	Ela	k1gFnSc5	Ela
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
šachu	šach	k1gInSc6	šach
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
střetávají	střetávat	k5eAaImIp3nP	střetávat
dvojice	dvojice	k1gFnPc1	dvojice
hráčů	hráč	k1gMnPc2	hráč
nebo	nebo	k8xC	nebo
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
také	také	k9	také
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
hodnocení	hodnocení	k1gNnSc4	hodnocení
obtížnosti	obtížnost	k1gFnSc2	obtížnost
otázek	otázka	k1gFnPc2	otázka
a	a	k8xC	a
schopností	schopnost	k1gFnPc2	schopnost
žáků	žák	k1gMnPc2	žák
při	při	k7c6	při
učení	učení	k1gNnSc6	učení
<g/>
.	.	kIx.	.
</s>
<s>
Elo	Ela	k1gFnSc5	Ela
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
někdy	někdy	k6eAd1	někdy
píše	psát	k5eAaImIp3nS	psát
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
(	(	kIx(	(
<g/>
ELO	Ela	k1gFnSc5	Ela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
zkratka	zkratka	k1gFnSc1	zkratka
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
příjmení	příjmení	k1gNnSc4	příjmení
tvůrce	tvůrce	k1gMnSc2	tvůrce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
psát	psát	k5eAaImF	psát
velké	velký	k2eAgNnSc4d1	velké
jen	jen	k9	jen
první	první	k4xOgNnSc4	první
písmeno	písmeno	k1gNnSc4	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šachu	šach	k1gInSc6	šach
se	se	k3xPyFc4	se
rating	rating	k1gInSc1	rating
zpravidla	zpravidla	k6eAd1	zpravidla
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
Elo	Ela	k1gFnSc5	Ela
národní	národní	k2eAgInPc1d1	národní
(	(	kIx(	(
<g/>
vydávané	vydávaný	k2eAgInPc1d1	vydávaný
národními	národní	k2eAgFnPc7d1	národní
šachovými	šachový	k2eAgFnPc7d1	šachová
federacemi	federace	k1gFnPc7	federace
<g/>
)	)	kIx)	)
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
(	(	kIx(	(
<g/>
vydávané	vydávaný	k2eAgFnSc2d1	vydávaná
FIDE	FIDE	kA	FIDE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
FIDE	FIDE	kA	FIDE
Elo	Ela	k1gFnSc5	Ela
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
nového	nový	k2eAgInSc2d1	nový
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
národní	národní	k2eAgFnPc1d1	národní
Elo	Ela	k1gFnSc5	Ela
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
května	květen	k1gInSc2	květen
a	a	k8xC	a
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
aktualizaci	aktualizace	k1gFnSc6	aktualizace
ratingu	rating	k1gInSc2	rating
Elo	Ela	k1gFnSc5	Ela
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledku	výsledek	k1gInSc2	výsledek
odehraných	odehraný	k2eAgFnPc2d1	odehraná
partií	partie	k1gFnPc2	partie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozdílu	rozdíl	k1gInSc2	rozdíl
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
Elo	Ela	k1gFnSc5	Ela
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgMnPc7	dva
hráči	hráč	k1gMnPc7	hráč
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
stanovit	stanovit	k5eAaPmF	stanovit
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
výsledek	výsledek	k1gInSc4	výsledek
partie	partie	k1gFnSc1	partie
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
výsledek	výsledek	k1gInSc4	výsledek
série	série	k1gFnSc2	série
partií	partie	k1gFnPc2	partie
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
hráči	hráč	k1gMnPc7	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Přepočet	přepočet	k1gInSc1	přepočet
rozdílu	rozdíl	k1gInSc2	rozdíl
na	na	k7c4	na
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
výhry	výhra	k1gFnSc2	výhra
udává	udávat	k5eAaImIp3nS	udávat
následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Například	například	k6eAd1	například
jestliže	jestliže	k8xS	jestliže
jeden	jeden	k4xCgMnSc1	jeden
hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
hodnocení	hodnocení	k1gNnSc4	hodnocení
Elo	Ela	k1gFnSc5	Ela
1300	[number]	k4	1300
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhý	druhý	k4xOgMnSc1	druhý
hráč	hráč	k1gMnSc1	hráč
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
s	s	k7c7	s
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
85	[number]	k4	85
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
očekávání	očekávání	k1gNnSc1	očekávání
naplněno	naplnit	k5eAaPmNgNnS	naplnit
<g/>
,	,	kIx,	,
Elo	Ela	k1gFnSc5	Ela
hráčů	hráč	k1gMnPc2	hráč
se	se	k3xPyFc4	se
nezmění	změnit	k5eNaPmIp3nS	změnit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
naplněno	naplněn	k2eAgNnSc1d1	naplněno
není	být	k5eNaImIp3nS	být
(	(	kIx(	(
<g/>
hráč	hráč	k1gMnSc1	hráč
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
buď	buď	k8xC	buď
vícekrát	vícekrát	k6eAd1	vícekrát
nebo	nebo	k8xC	nebo
méněkrát	méněkrát	k6eAd1	méněkrát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
opravě	oprava	k1gFnSc3	oprava
Elo	Ela	k1gFnSc5	Ela
obou	dva	k4xCgInPc6	dva
hráčů	hráč	k1gMnPc2	hráč
podle	podle	k7c2	podle
vzorce	vzorec	k1gInSc2	vzorec
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
o	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
K	k	k7c3	k
⋅	⋅	k?	⋅
(	(	kIx(	(
W	W	kA	W
−	−	k?	−
:	:	kIx,	:
W	W	kA	W
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
R_	R_	k1gFnSc1	R_
<g/>
{	{	kIx(	{
<g/>
o	o	k7c4	o
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
K	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
W-W_	W-W_	k1gFnSc1	W-W_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgMnSc1d1	nový
Elo	Ela	k1gFnSc5	Ela
rating	rating	k1gInSc1	rating
hráče	hráč	k1gMnSc4	hráč
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
R	R	kA	R
:	:	kIx,	:
o	o	k7c4	o
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnPc7	R_
<g/>
{	{	kIx(	{
<g/>
o	o	k7c4	o
<g/>
}}	}}	k?	}}
původní	původní	k2eAgInSc4d1	původní
Elo	Ela	k1gFnSc5	Ela
rating	rating	k1gInSc1	rating
hráče	hráč	k1gMnSc4	hráč
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
K	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
}	}	kIx)	}
koeficient	koeficient	k1gInSc1	koeficient
rozvoje	rozvoj	k1gInSc2	rozvoj
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
W	W	kA	W
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
W	W	kA	W
<g/>
}	}	kIx)	}
dosažený	dosažený	k2eAgInSc1d1	dosažený
počet	počet	k1gInSc1	počet
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
počítá	počítat	k5eAaImIp3nS	počítat
se	se	k3xPyFc4	se
1	[number]	k4	1
b.	b.	k?	b.
za	za	k7c4	za
výhru	výhra	k1gFnSc4	výhra
<g/>
,	,	kIx,	,
0	[number]	k4	0
b.	b.	k?	b.
za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
a	a	k8xC	a
za	za	k7c4	za
remízu	remíza	k1gFnSc4	remíza
0,5	[number]	k4	0,5
<g/>
b.	b.	k?	b.
<g/>
)	)	kIx)	)
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
W	W	kA	W
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
W_	W_	k1gMnSc1	W_
<g/>
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
jestliže	jestliže	k8xS	jestliže
hráč	hráč	k1gMnSc1	hráč
má	mít	k5eAaImIp3nS	mít
např.	např.	kA	např.
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
získat	získat	k5eAaPmF	získat
1	[number]	k4	1
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
s	s	k7c7	s
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
85	[number]	k4	85
%	%	kIx~	%
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
takové	takový	k3xDgFnSc2	takový
partie	partie	k1gFnSc2	partie
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
bodový	bodový	k2eAgInSc4d1	bodový
zisk	zisk	k1gInSc4	zisk
1	[number]	k4	1
<g/>
·	·	k?	·
<g/>
85	[number]	k4	85
%	%	kIx~	%
=	=	kIx~	=
0,85	[number]	k4	0,85
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
Elo	Ela	k1gFnSc5	Ela
počítá	počítat	k5eAaImIp3nS	počítat
ze	z	k7c2	z
série	série	k1gFnSc2	série
partií	partie	k1gFnPc2	partie
s	s	k7c7	s
různými	různý	k2eAgMnPc7d1	různý
soupeři	soupeř	k1gMnPc7	soupeř
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
určit	určit	k5eAaPmF	určit
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
z	z	k7c2	z
rozdílu	rozdíl	k1gInSc2	rozdíl
Elo	Ela	k1gFnSc5	Ela
hráče	hráč	k1gMnPc4	hráč
a	a	k8xC	a
průměru	průměr	k1gInSc6	průměr
Elo	Ela	k1gFnSc5	Ela
všech	všecek	k3xTgMnPc2	všecek
jeho	jeho	k3xOp3gMnPc2	jeho
soupeřů	soupeř	k1gMnPc2	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
koeficientu	koeficient	k1gInSc2	koeficient
rozvoje	rozvoj	k1gInSc2	rozvoj
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
vliv	vliv	k1gInSc1	vliv
aktuální	aktuální	k2eAgFnSc2d1	aktuální
partie	partie	k1gFnSc2	partie
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
vliv	vliv	k1gInSc4	vliv
starého	starý	k2eAgNnSc2d1	staré
hodnocení	hodnocení	k1gNnSc2	hodnocení
Elo	Ela	k1gFnSc5	Ela
<g/>
)	)	kIx)	)
na	na	k7c4	na
nově	nově	k6eAd1	nově
počítané	počítaný	k2eAgNnSc4d1	počítané
Elo	Ela	k1gFnSc5	Ela
-	-	kIx~	-
čím	co	k3yQnSc7	co
je	být	k5eAaImIp3nS	být
koeficient	koeficient	k1gInSc1	koeficient
vyšší	vysoký	k2eAgInSc1d2	vyšší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgInPc1d2	veliký
vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
výsledné	výsledný	k2eAgNnSc4d1	výsledné
hodnocení	hodnocení	k1gNnSc4	hodnocení
výsledek	výsledek	k1gInSc1	výsledek
nejnovější	nový	k2eAgFnSc2d3	nejnovější
partie	partie	k1gFnSc2	partie
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
nejnovější	nový	k2eAgFnSc1d3	nejnovější
série	série	k1gFnSc1	série
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
Elo	Ela	k1gFnSc5	Ela
právě	právě	k6eAd1	právě
počítáno	počítat	k5eAaImNgNnS	počítat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Elo	Ela	k1gFnSc5	Ela
počítalo	počítat	k5eAaImAgNnS	počítat
jen	jen	k9	jen
z	z	k7c2	z
několika	několik	k4yIc2	několik
posledních	poslední	k2eAgFnPc2d1	poslední
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
by	by	kYmCp3nP	by
přeceněny	přeceněn	k2eAgInPc1d1	přeceněn
výsledky	výsledek	k1gInPc1	výsledek
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
podceněny	podceněn	k2eAgInPc4d1	podceněn
výsledky	výsledek	k1gInPc4	výsledek
neúspěšných	úspěšný	k2eNgMnPc2d1	neúspěšný
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
potřeba	potřeba	k6eAd1	potřeba
započítávat	započítávat	k5eAaImF	započítávat
i	i	k9	i
vliv	vliv	k1gInSc4	vliv
starších	starý	k2eAgFnPc2d2	starší
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
nového	nový	k2eAgInSc2d1	nový
Elo	Ela	k1gFnSc5	Ela
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
již	již	k6eAd1	již
získaného	získaný	k2eAgInSc2d1	získaný
Elo	Ela	k1gFnSc5	Ela
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
přisuzovalo	přisuzovat	k5eAaImAgNnS	přisuzovat
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c1	mnoho
váhy	váha	k1gFnSc2	váha
starému	starý	k2eAgNnSc3d1	staré
hodnocení	hodnocení	k1gNnSc3	hodnocení
<g/>
,	,	kIx,	,
Elo	Ela	k1gFnSc5	Ela
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měnilo	měnit	k5eAaImAgNnS	měnit
příliš	příliš	k6eAd1	příliš
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
by	by	kYmCp3nP	by
schopno	schopen	k2eAgNnSc1d1	schopno
včas	včas	k6eAd1	včas
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
schopností	schopnost	k1gFnPc2	schopnost
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
hodnoty	hodnota	k1gFnPc1	hodnota
koeficientu	koeficient	k1gInSc2	koeficient
rozvoje	rozvoj	k1gInSc2	rozvoj
od	od	k7c2	od
10	[number]	k4	10
do	do	k7c2	do
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
hráče	hráč	k1gMnPc4	hráč
s	s	k7c7	s
Elo	Ela	k1gFnSc5	Ela
vyšším	vysoký	k2eAgInSc6d2	vyšší
než	než	k8xS	než
2399	[number]	k4	2399
užívá	užívat	k5eAaImIp3nS	užívat
koeficient	koeficient	k1gInSc1	koeficient
K	k	k7c3	k
=	=	kIx~	=
10	[number]	k4	10
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnPc4	jejich
hodnocení	hodnocení	k1gNnPc4	hodnocení
již	již	k6eAd1	již
stabilizované	stabilizovaný	k2eAgFnSc2d1	stabilizovaná
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
hráče	hráč	k1gMnPc4	hráč
s	s	k7c7	s
Elo	Ela	k1gFnSc5	Ela
nižším	nízký	k2eAgInSc6d2	nižší
než	než	k8xS	než
2400	[number]	k4	2400
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
koeficient	koeficient	k1gInSc1	koeficient
K	k	k7c3	k
=	=	kIx~	=
15	[number]	k4	15
a	a	k8xC	a
pro	pro	k7c4	pro
hráče	hráč	k1gMnSc4	hráč
mladšího	mladý	k2eAgMnSc4d2	mladší
20	[number]	k4	20
let	léto	k1gNnPc2	léto
s	s	k7c7	s
Elo	Ela	k1gFnSc5	Ela
nižším	nízký	k2eAgInSc6d2	nižší
než	než	k8xS	než
2200	[number]	k4	2200
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
koeficient	koeficient	k1gInSc1	koeficient
K	k	k7c3	k
=	=	kIx~	=
25	[number]	k4	25
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
věkové	věkový	k2eAgFnSc2d1	věková
skupiny	skupina	k1gFnSc2	skupina
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
dynamický	dynamický	k2eAgInSc4d1	dynamický
vývoj	vývoj	k1gInSc4	vývoj
výkonnosti	výkonnost	k1gFnSc2	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
výpočtu	výpočet	k1gInSc2	výpočet
Dejme	dát	k5eAaPmRp1nP	dát
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
16	[number]	k4	16
<g/>
letý	letý	k2eAgMnSc1d1	letý
hráč	hráč	k1gMnSc1	hráč
A	a	k9	a
s	s	k7c7	s
Elo	Ela	k1gFnSc5	Ela
1200	[number]	k4	1200
si	se	k3xPyFc3	se
zahraje	zahrát	k5eAaPmIp3nS	zahrát
partii	partie	k1gFnSc4	partie
s	s	k7c7	s
15	[number]	k4	15
<g/>
letým	letý	k2eAgInSc7d1	letý
hráčem	hráč	k1gMnSc7	hráč
B	B	kA	B
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
Elo	Ela	k1gFnSc5	Ela
činí	činit	k5eAaImIp3nS	činit
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
mají	mít	k5eAaImIp3nP	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
použijeme	použít	k5eAaPmIp1nP	použít
proto	proto	k8xC	proto
koeficient	koeficient	k1gInSc1	koeficient
rozvoje	rozvoj	k1gInSc2	rozvoj
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
Elo	Ela	k1gFnSc5	Ela
činí	činit	k5eAaImIp3nS	činit
200	[number]	k4	200
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
očekávaný	očekávaný	k2eAgInSc1d1	očekávaný
zisk	zisk	k1gInSc1	zisk
hráče	hráč	k1gMnSc2	hráč
A	a	k8xC	a
je	být	k5eAaImIp3nS	být
0,76	[number]	k4	0,76
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
zisk	zisk	k1gInSc4	zisk
hráče	hráč	k1gMnSc2	hráč
B	B	kA	B
je	být	k5eAaImIp3nS	být
0,24	[number]	k4	0,24
bodu	bod	k1gInSc2	bod
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
tabulku	tabulka	k1gFnSc4	tabulka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Můžou	Můžou	k?	Můžou
nastat	nastat	k5eAaPmF	nastat
tyto	tento	k3xDgFnPc4	tento
možnosti	možnost	k1gFnPc4	možnost
<g/>
:	:	kIx,	:
jestliže	jestliže	k8xS	jestliže
hráč	hráč	k1gMnSc1	hráč
A	a	k9	a
skutečně	skutečně	k6eAd1	skutečně
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
1	[number]	k4	1
bod	bod	k1gInSc1	bod
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
očekávanému	očekávaný	k2eAgNnSc3d1	očekávané
0,76	[number]	k4	0,76
<g/>
b.	b.	k?	b.
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
nové	nový	k2eAgNnSc4d1	nové
Elo	Ela	k1gFnSc5	Ela
bude	být	k5eAaImBp3nS	být
o	o	k7c4	o
šest	šest	k4xCc4	šest
bodů	bod	k1gInPc2	bod
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
:	:	kIx,	:
1200	[number]	k4	1200
+	+	kIx~	+
25	[number]	k4	25
<g/>
·	·	k?	·
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0,76	[number]	k4	0,76
<g/>
)	)	kIx)	)
=	=	kIx~	=
1206	[number]	k4	1206
<g/>
.	.	kIx.	.
</s>
<s>
Elo	Ela	k1gFnSc5	Ela
hráče	hráč	k1gMnSc2	hráč
B	B	kA	B
o	o	k7c4	o
těchto	tento	k3xDgInPc2	tento
6	[number]	k4	6
bodů	bod	k1gInPc2	bod
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
<g/>
:	:	kIx,	:
oproti	oproti	k7c3	oproti
očekávanému	očekávaný	k2eAgNnSc3d1	očekávané
0,24	[number]	k4	0,24
<g/>
b.	b.	k?	b.
získal	získat	k5eAaPmAgMnS	získat
0	[number]	k4	0
<g/>
b.	b.	k?	b.
<g/>
,	,	kIx,	,
nové	nový	k2eAgFnSc2d1	nová
Elo	Ela	k1gFnSc5	Ela
proto	proto	k8xC	proto
bude	být	k5eAaImBp3nS	být
1000	[number]	k4	1000
+	+	kIx~	+
25	[number]	k4	25
<g/>
·	·	k?	·
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0,24	[number]	k4	0,24
<g/>
)	)	kIx)	)
=	=	kIx~	=
994	[number]	k4	994
<g/>
.	.	kIx.	.
jestliže	jestliže	k8xS	jestliže
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
hráč	hráč	k1gMnSc1	hráč
B	B	kA	B
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
získá	získat	k5eAaPmIp3nS	získat
1	[number]	k4	1
b.	b.	k?	b.
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
očekávanému	očekávaný	k2eAgNnSc3d1	očekávané
0,24	[number]	k4	0,24
<g/>
b.	b.	k?	b.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
nové	nový	k2eAgInPc4d1	nový
Elo	Ela	k1gFnSc5	Ela
bude	být	k5eAaImBp3nS	být
1000	[number]	k4	1000
+	+	kIx~	+
25	[number]	k4	25
<g/>
·	·	k?	·
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0,24	[number]	k4	0,24
<g/>
)	)	kIx)	)
=	=	kIx~	=
1019	[number]	k4	1019
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
Elo	Ela	k1gFnSc5	Ela
hráče	hráč	k1gMnPc4	hráč
A	a	k9	a
o	o	k7c4	o
těchto	tento	k3xDgInPc2	tento
19	[number]	k4	19
bodů	bod	k1gInPc2	bod
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
<g/>
:	:	kIx,	:
1200	[number]	k4	1200
+	+	kIx~	+
25	[number]	k4	25
<g/>
·	·	k?	·
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0,76	[number]	k4	0,76
<g/>
)	)	kIx)	)
=	=	kIx~	=
1181	[number]	k4	1181
jestliže	jestliže	k8xS	jestliže
hráči	hráč	k1gMnPc1	hráč
remizují	remizovat	k5eAaPmIp3nP	remizovat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
oba	dva	k4xCgMnPc1	dva
získají	získat	k5eAaPmIp3nP	získat
0,5	[number]	k4	0,5
<g/>
b.	b.	k?	b.
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
nové	nový	k2eAgNnSc1d1	nové
Elo	Ela	k1gFnSc5	Ela
u	u	k7c2	u
hráče	hráč	k1gMnSc2	hráč
A	a	k9	a
rovno	roven	k2eAgNnSc4d1	rovno
1200	[number]	k4	1200
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
25	[number]	k4	25
<g/>
·	·	k?	·
<g/>
(	(	kIx(	(
<g/>
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
0,76	[number]	k4	0,76
<g/>
)	)	kIx)	)
=	=	kIx~	=
1193,5	[number]	k4	1193,5
<g/>
,	,	kIx,	,
po	po	k7c6	po
zaokrouhlení	zaokrouhlení	k1gNnSc6	zaokrouhlení
1194	[number]	k4	1194
<g/>
,	,	kIx,	,
a	a	k8xC	a
nové	nový	k2eAgFnPc1d1	nová
Elo	Ela	k1gFnSc5	Ela
hráče	hráč	k1gMnPc4	hráč
B	B	kA	B
bude	být	k5eAaImBp3nS	být
rovno	roven	k2eAgNnSc4d1	rovno
1000	[number]	k4	1000
+	+	kIx~	+
25	[number]	k4	25
<g/>
·	·	k?	·
<g/>
(	(	kIx(	(
<g/>
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
0,24	[number]	k4	0,24
<g/>
)	)	kIx)	)
=	=	kIx~	=
1006,5	[number]	k4	1006,5
<g/>
,	,	kIx,	,
zaokrouhlí	zaokrouhlit	k5eAaPmIp3nS	zaokrouhlit
se	se	k3xPyFc4	se
na	na	k7c4	na
1007	[number]	k4	1007
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
vypočítat	vypočítat	k5eAaPmF	vypočítat
Elo	Ela	k1gFnSc5	Ela
u	u	k7c2	u
nových	nový	k2eAgMnPc2d1	nový
hráčů	hráč	k1gMnPc2	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ještě	ještě	k6eAd1	ještě
hodnocení	hodnocený	k2eAgMnPc1d1	hodnocený
Elo	Ela	k1gFnSc5	Ela
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
periodické	periodický	k2eAgFnSc2d1	periodická
metody	metoda	k1gFnSc2	metoda
se	se	k3xPyFc4	se
vypočítá	vypočítat	k5eAaPmIp3nS	vypočítat
Elo	Ela	k1gFnSc5	Ela
hráče	hráč	k1gMnPc4	hráč
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	jeho	k3xOp3gMnPc2	jeho
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
sérii	série	k1gFnSc6	série
partií	partie	k1gFnPc2	partie
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
znalosti	znalost	k1gFnSc2	znalost
Elo	Ela	k1gFnSc5	Ela
jeho	jeho	k3xOp3gMnPc2	jeho
spoluhráčů	spoluhráč	k1gMnPc2	spoluhráč
podle	podle	k7c2	podle
vzorce	vzorec	k1gInSc2	vzorec
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
D	D	kA	D
(	(	kIx(	(
P	P	kA	P
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gFnSc6	R_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
R_	R_	k1gFnSc1	R_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
D	D	kA	D
<g/>
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gFnSc6	R_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
výkonnost	výkonnost	k1gFnSc1	výkonnost
hráče	hráč	k1gMnPc4	hráč
vyjádřená	vyjádřený	k2eAgFnSc1d1	vyjádřená
v	v	k7c6	v
Elo	Ela	k1gFnSc5	Ela
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
R_	R_	k1gMnSc1	R_
<g/>
{	{	kIx(	{
<g/>
c	c	k0	c
<g/>
}}	}}	k?	}}
průměr	průměr	k1gInSc1	průměr
Elo	Ela	k1gFnSc5	Ela
všech	všecek	k3xTgMnPc2	všecek
jeho	jeho	k3xOp3gMnPc2	jeho
soupeřů	soupeř	k1gMnPc2	soupeř
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
D	D	kA	D
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
P	P	kA	P
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
D	D	kA	D
<g/>
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
oprava	oprava	k1gFnSc1	oprava
podle	podle	k7c2	podle
procentuální	procentuální	k2eAgFnSc2d1	procentuální
úspěšnosti	úspěšnost	k1gFnSc2	úspěšnost
hráče	hráč	k1gMnSc2	hráč
(	(	kIx(	(
<g/>
kolik	kolik	k4yRc1	kolik
procent	procento	k1gNnPc2	procento
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
možného	možný	k2eAgInSc2d1	možný
počtu	počet	k1gInSc2	počet
bodů	bod	k1gInPc2	bod
skutečně	skutečně	k6eAd1	skutečně
získal	získat	k5eAaPmAgInS	získat
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
následující	následující	k2eAgFnSc2d1	následující
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
:	:	kIx,	:
Hráči	hráč	k1gMnPc1	hráč
nehrají	hrát	k5eNaImIp3nP	hrát
pokaždé	pokaždé	k6eAd1	pokaždé
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc4	jejich
aktuální	aktuální	k2eAgInSc4d1	aktuální
výkon	výkon	k1gInSc4	výkon
kolísá	kolísat	k5eAaImIp3nS	kolísat
kolem	kolem	k7c2	kolem
určité	určitý	k2eAgFnSc2d1	určitá
střední	střední	k2eAgFnSc2d1	střední
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
odhadem	odhad	k1gInSc7	odhad
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
Elo	Ela	k1gFnSc5	Ela
<g/>
.	.	kIx.	.
</s>
<s>
Odhad	odhad	k1gInSc1	odhad
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
přesnější	přesný	k2eAgFnSc1d2	přesnější
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
častěji	často	k6eAd2	často
hráč	hráč	k1gMnSc1	hráč
hraje	hrát	k5eAaImIp3nS	hrát
a	a	k8xC	a
čím	co	k3yQnSc7	co
více	hodně	k6eAd2	hodně
her	hra	k1gFnPc2	hra
hráč	hráč	k1gMnSc1	hráč
odehrál	odehrát	k5eAaPmAgMnS	odehrát
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Elo	Ela	k1gFnSc5	Ela
rating	rating	k1gInSc1	rating
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
počítá	počítat	k5eAaImIp3nS	počítat
až	až	k9	až
po	po	k7c6	po
odehrání	odehrání	k1gNnSc6	odehrání
stanoveného	stanovený	k2eAgInSc2d1	stanovený
počtu	počet	k1gInSc2	počet
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
FIDE	FIDE	kA	FIDE
po	po	k7c6	po
9	[number]	k4	9
partiích	partie	k1gFnPc6	partie
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
po	po	k7c6	po
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Elo	Ela	k1gFnSc5	Ela
je	on	k3xPp3gInPc4	on
třeba	třeba	k6eAd1	třeba
pohlížet	pohlížet	k5eAaImF	pohlížet
jako	jako	k9	jako
na	na	k7c4	na
odhad	odhad	k1gInSc4	odhad
relativní	relativní	k2eAgFnSc2d1	relativní
síly	síla	k1gFnSc2	síla
hráče	hráč	k1gMnSc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
utkají	utkat	k5eAaPmIp3nP	utkat
2	[number]	k4	2
hráči	hráč	k1gMnPc7	hráč
s	s	k7c7	s
Elo	Ela	k1gFnSc5	Ela
1200	[number]	k4	1200
a	a	k8xC	a
po	po	k7c6	po
nějaké	nějaký	k3yIgFnSc6	nějaký
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
oba	dva	k4xCgMnPc1	dva
trénovali	trénovat	k5eAaImAgMnP	trénovat
a	a	k8xC	a
u	u	k7c2	u
obou	dva	k4xCgFnPc2	dva
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
podobně	podobně	k6eAd1	podobně
velkému	velký	k2eAgNnSc3d1	velké
zlepšení	zlepšení	k1gNnSc3	zlepšení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
utkají	utkat	k5eAaPmIp3nP	utkat
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
rating	rating	k1gInSc1	rating
Elo	Ela	k1gFnSc5	Ela
tento	tento	k3xDgInSc4	tento
absolutní	absolutní	k2eAgInSc4d1	absolutní
nárůst	nárůst	k1gInSc4	nárůst
výkonnosti	výkonnost	k1gFnSc2	výkonnost
obou	dva	k4xCgMnPc2	dva
hráčů	hráč	k1gMnPc2	hráč
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
zachytit	zachytit	k5eAaPmF	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
omezení	omezení	k1gNnSc1	omezení
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
regionální	regionální	k2eAgFnSc2d1	regionální
podmíněnosti	podmíněnost	k1gFnSc2	podmíněnost
-	-	kIx~	-
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
hodnocení	hodnocení	k1gNnSc1	hodnocení
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
hře	hra	k1gFnSc6	hra
mezi	mezi	k7c7	mezi
stále	stále	k6eAd1	stále
stejnými	stejný	k2eAgMnPc7d1	stejný
hráči	hráč	k1gMnPc7	hráč
(	(	kIx(	(
<g/>
šachový	šachový	k2eAgInSc1d1	šachový
oddíl	oddíl	k1gInSc1	oddíl
<g/>
,	,	kIx,	,
návštěvníci	návštěvník	k1gMnPc1	návštěvník
jednoho	jeden	k4xCgInSc2	jeden
šachového	šachový	k2eAgInSc2d1	šachový
serveru	server	k1gInSc2	server
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
svou	svůj	k3xOyFgFnSc4	svůj
vypovídací	vypovídací	k2eAgFnSc4d1	vypovídací
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
hráči	hráč	k1gMnPc1	hráč
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
různé	různý	k2eAgFnPc4d1	různá
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
různými	různý	k2eAgMnPc7d1	různý
soupeři	soupeř	k1gMnPc7	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
výkonnost	výkonnost	k1gFnSc1	výkonnost
hráče	hráč	k1gMnSc2	hráč
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
Elo	Ela	k1gFnSc5	Ela
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
v	v	k7c6	v
čase	čas	k1gInSc6	čas
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Jev	jev	k1gInSc4	jev
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgMnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
schopnosti	schopnost	k1gFnPc1	schopnost
jeho	jeho	k3xOp3gMnPc2	jeho
soupeřů	soupeř	k1gMnPc2	soupeř
se	se	k3xPyFc4	se
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
časem	časem	k6eAd1	časem
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
šance	šance	k1gFnSc1	šance
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
porazí	porazit	k5eAaPmIp3nS	porazit
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
Elo	Ela	k1gFnSc5	Ela
tedy	tedy	k9	tedy
klesne	klesnout	k5eAaPmIp3nS	klesnout
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
si	se	k3xPyFc3	se
hráč	hráč	k1gMnSc1	hráč
může	moct	k5eAaImIp3nS	moct
zachovávat	zachovávat	k5eAaImF	zachovávat
svou	svůj	k3xOyFgFnSc4	svůj
výkonnost	výkonnost	k1gFnSc4	výkonnost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
šachů	šach	k1gInPc2	šach
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
specifické	specifický	k2eAgInPc1d1	specifický
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
jevy	jev	k1gInPc1	jev
-	-	kIx~	-
např.	např.	kA	např.
bílý	bílý	k1gMnSc1	bílý
je	být	k5eAaImIp3nS	být
zvýhodněný	zvýhodněný	k2eAgMnSc1d1	zvýhodněný
oproti	oproti	k7c3	oproti
černému	černý	k2eAgInSc3d1	černý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
systém	systém	k1gInSc1	systém
Elo	Ela	k1gFnSc5	Ela
zvýhodnění	zvýhodnění	k1gNnPc4	zvýhodnění
začínajícího	začínající	k2eAgMnSc4d1	začínající
nebere	brát	k5eNaImIp3nS	brát
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
<g/>
.	.	kIx.	.
</s>
<s>
Výše	výše	k1gFnSc1	výše
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
i	i	k8xC	i
jiné	jiný	k2eAgFnSc2d1	jiná
nedokonalosti	nedokonalost	k1gFnSc2	nedokonalost
Elo	Ela	k1gFnSc5	Ela
ratingu	rating	k1gInSc3	rating
vedou	vést	k5eAaImIp3nP	vést
různé	různý	k2eAgInPc1d1	různý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
šachové	šachový	k2eAgInPc1d1	šachový
<g/>
)	)	kIx)	)
organizace	organizace	k1gFnSc1	organizace
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
systém	systém	k1gInSc1	systém
Elo	Ela	k1gFnSc5	Ela
implementují	implementovat	k5eAaImIp3nP	implementovat
v	v	k7c6	v
modifikované	modifikovaný	k2eAgFnSc6d1	modifikovaná
podobě	podoba	k1gFnSc6	podoba
(	(	kIx(	(
<g/>
přidávají	přidávat	k5eAaImIp3nP	přidávat
různé	různý	k2eAgInPc4d1	různý
koeficienty	koeficient	k1gInPc4	koeficient
<g/>
,	,	kIx,	,
připočítávají	připočítávat	k5eAaImIp3nP	připočítávat
konstanty	konstanta	k1gFnPc1	konstanta
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
systém	systém	k1gInSc4	systém
Elo	Ela	k1gFnSc5	Ela
zpřesňovat	zpřesňovat	k5eAaImF	zpřesňovat
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
používané	používaný	k2eAgInPc4d1	používaný
způsoby	způsob	k1gInPc4	způsob
výpočtu	výpočet	k1gInSc2	výpočet
Elo	Ela	k1gFnSc5	Ela
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
organizacích	organizace	k1gFnPc6	organizace
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
příslušné	příslušný	k2eAgInPc1d1	příslušný
vzorce	vzorec	k1gInPc1	vzorec
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
závazných	závazný	k2eAgInPc6d1	závazný
dokumentech	dokument	k1gInPc6	dokument
dané	daný	k2eAgFnSc2d1	daná
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
se	se	k3xPyFc4	se
různě	různě	k6eAd1	různě
volí	volit	k5eAaImIp3nP	volit
koeficienty	koeficient	k1gInPc1	koeficient
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
Elo	Ela	k1gFnSc5	Ela
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
různě	různě	k6eAd1	různě
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
druzích	druh	k1gInPc6	druh
turnajů	turnaj	k1gInPc2	turnaj
<g/>
,	,	kIx,	,
počtu	počet	k1gInSc2	počet
odehraných	odehraný	k2eAgFnPc2d1	odehraná
partií	partie	k1gFnPc2	partie
<g/>
,	,	kIx,	,
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
hráčských	hráčský	k2eAgFnPc6d1	hráčská
kategoriích	kategorie	k1gFnPc6	kategorie
atd.	atd.	kA	atd.
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
organizace	organizace	k1gFnPc1	organizace
dávají	dávat	k5eAaImIp3nP	dávat
za	za	k7c4	za
výjimečné	výjimečný	k2eAgInPc4d1	výjimečný
výkony	výkon	k1gInPc4	výkon
extra	extra	k6eAd1	extra
bonusové	bonusový	k2eAgInPc4d1	bonusový
body	bod	k1gInPc4	bod
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
hráči	hráč	k1gMnPc1	hráč
který	který	k3yQgInSc4	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnSc1	jeho
Elo	Ela	k1gFnSc5	Ela
<g />
.	.	kIx.	.
</s>
<s>
bylo	být	k5eAaImAgNnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podceněno	podcenit	k5eAaPmNgNnS	podcenit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
hráči	hráč	k1gMnSc3	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prohrál	prohrát	k5eAaPmAgMnS	prohrát
-	-	kIx~	-
neboť	neboť	k8xC	neboť
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prohrál	prohrát	k5eAaPmAgMnS	prohrát
s	s	k7c7	s
hráčem	hráč	k1gMnSc7	hráč
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
Elo	Ela	k1gFnSc5	Ela
bylo	být	k5eAaImAgNnS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jeho	jeho	k3xOp3gFnPc3	jeho
schopnostem	schopnost	k1gFnPc3	schopnost
<g/>
,	,	kIx,	,
mu	on	k3xPp3gInSc3	on
jeho	jeho	k3xOp3gMnSc1	jeho
Elo	Ela	k1gFnSc5	Ela
pokleslo	poklesnout	k5eAaPmAgNnS	poklesnout
o	o	k7c4	o
nepřiměřeně	přiměřeně	k6eNd1	přiměřeně
mnoho	mnoho	k4c4	mnoho
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
očekávaného	očekávaný	k2eAgInSc2d1	očekávaný
výsledku	výsledek	k1gInSc2	výsledek
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zohlednit	zohlednit	k5eAaPmF	zohlednit
zvýhodnění	zvýhodnění	k1gNnSc4	zvýhodnění
bílého	bílé	k1gNnSc2	bílé
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
dělala	dělat	k5eAaImAgFnS	dělat
např.	např.	kA	např.
PCA	PCA	kA	PCA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
drobné	drobný	k2eAgFnPc1d1	drobná
<g/>
,	,	kIx,	,
např.	např.	kA	např.
USCF	USCF	kA	USCF
nezaokrouhluje	zaokrouhlovat	k5eNaImIp3nS	zaokrouhlovat
matematicky	matematicky	k6eAd1	matematicky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
starého	starý	k2eAgInSc2d1	starý
Elo	Ela	k1gFnSc5	Ela
<g/>
.	.	kIx.	.
</s>
<s>
Elo	Ela	k1gFnSc5	Ela
úplného	úplný	k2eAgMnSc4d1	úplný
začátečníka	začátečník	k1gMnSc4	začátečník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zrovna	zrovna	k6eAd1	zrovna
naučil	naučit	k5eAaPmAgMnS	naučit
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
pohybovat	pohybovat	k5eAaImF	pohybovat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kolem	kolem	k7c2	kolem
hodnoty	hodnota	k1gFnSc2	hodnota
100	[number]	k4	100
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
silný	silný	k2eAgInSc1d1	silný
<g/>
"	"	kIx"	"
sváteční	sváteční	k2eAgMnSc1d1	sváteční
hráč	hráč	k1gMnSc1	hráč
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
Elo	Ela	k1gFnSc5	Ela
někde	někde	k6eAd1	někde
mezi	mezi	k7c7	mezi
800	[number]	k4	800
<g/>
-	-	kIx~	-
<g/>
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgMnSc1d1	průměrný
dospělý	dospělý	k2eAgMnSc1d1	dospělý
člen	člen	k1gMnSc1	člen
Šachového	šachový	k2eAgInSc2d1	šachový
svazu	svaz	k1gInSc2	svaz
USA	USA	kA	USA
(	(	kIx(	(
<g/>
USCF	USCF	kA	USCF
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
Elo	Ela	k1gFnSc5	Ela
1429	[number]	k4	1429
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
každoročně	každoročně	k6eAd1	každoročně
kolísá	kolísat	k5eAaImIp3nS	kolísat
kolem	kolem	k7c2	kolem
hodnoty	hodnota	k1gFnSc2	hodnota
1500	[number]	k4	1500
<g/>
)	)	kIx)	)
a	a	k8xC	a
průměrný	průměrný	k2eAgMnSc1d1	průměrný
žák	žák	k1gMnSc1	žák
USCF	USCF	kA	USCF
měl	mít	k5eAaImAgMnS	mít
Elo	Ela	k1gFnSc5	Ela
608	[number]	k4	608
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
systémy	systém	k1gInPc4	systém
na	na	k7c4	na
porovnávání	porovnávání	k1gNnSc4	porovnávání
výkonnosti	výkonnost	k1gFnSc2	výkonnost
např.	např.	kA	např.
Glicko	Glicko	k1gNnSc4	Glicko
a	a	k8xC	a
TrueSkill	TrueSkill	k1gInSc4	TrueSkill
<g/>
.	.	kIx.	.
</s>
<s>
Chessgraphs	Chessgraphs	k1gInSc1	Chessgraphs
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
-	-	kIx~	-
Compare	Compar	k1gMnSc5	Compar
chess	chessit	k5eAaPmRp2nS	chessit
players	players	k1gInSc1	players
<g/>
'	'	kIx"	'
rating	rating	k1gInSc1	rating
histories	histories	k1gMnSc1	histories
with	with	k1gMnSc1	with
FIDE	FIDE	kA	FIDE
data	datum	k1gNnSc2	datum
back	back	k1gInSc1	back
to	ten	k3xDgNnSc1	ten
1970	[number]	k4	1970
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Listina	listina	k1gFnSc1	listina
osobních	osobní	k2eAgInPc2d1	osobní
koeficientů	koeficient	k1gInPc2	koeficient
LOK	lok	k1gInSc1	lok
ČR	ČR	kA	ČR
Oficiální	oficiální	k2eAgInSc1d1	oficiální
ratingový	ratingový	k2eAgInSc1d1	ratingový
žebříček	žebříček	k1gInSc1	žebříček
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
šachové	šachový	k2eAgFnSc2d1	šachová
organizace	organizace	k1gFnSc2	organizace
FIDE	FIDE	kA	FIDE
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mark	Mark	k1gMnSc1	Mark
Glickman	Glickman	k1gMnSc1	Glickman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
research	research	k1gInSc1	research
page	pag	k1gInPc1	pag
-	-	kIx~	-
odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c4	na
technické	technický	k2eAgInPc4d1	technický
články	článek	k1gInPc4	článek
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
ratingových	ratingový	k2eAgInPc2d1	ratingový
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
