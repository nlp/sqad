<s>
Film	film	k1gInSc4	film
režíroval	režírovat	k5eAaImAgMnS	režírovat
Robert	Robert	k1gMnSc1	Robert
Redford	Redford	k1gMnSc1	Redford
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
také	také	k9	také
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
zaříkávače	zaříkávač	k1gMnSc2	zaříkávač
koní	kůň	k1gMnPc2	kůň
Toma	Tom	k1gMnSc2	Tom
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
i	i	k9	i
spoluproducentem	spoluproducent	k1gMnSc7	spoluproducent
snímku	snímek	k1gInSc2	snímek
<g/>
.	.	kIx.	.
</s>
