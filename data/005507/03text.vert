<s>
Zaříkávač	zaříkávač	k1gMnSc1	zaříkávač
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
v	v	k7c6	v
originálu	originál	k1gInSc6	originál
The	The	k1gMnSc2	The
Horse	Hors	k1gMnSc2	Hors
Whisperer	Whisperer	k1gMnSc1	Whisperer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americké	americký	k2eAgNnSc1d1	americké
romantické	romantický	k2eAgNnSc1d1	romantické
drama	drama	k1gNnSc1	drama
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
společnosti	společnost	k1gFnSc2	společnost
Touchstone	Touchston	k1gInSc5	Touchston
Pictures	Pictures	k1gInSc1	Pictures
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
předlohy	předloha	k1gFnSc2	předloha
Nicholase	Nicholasa	k1gFnSc3	Nicholasa
Evanse	Evansa	k1gFnSc3	Evansa
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
režíroval	režírovat	k5eAaImAgMnS	režírovat
Robert	Robert	k1gMnSc1	Robert
Redford	Redford	k1gMnSc1	Redford
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zde	zde	k6eAd1	zde
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
také	také	k9	také
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
zaříkávače	zaříkávač	k1gMnSc2	zaříkávač
koní	kůň	k1gMnPc2	kůň
Toma	Tom	k1gMnSc2	Tom
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
i	i	k9	i
spoluproducentem	spoluproducent	k1gMnSc7	spoluproducent
snímku	snímek	k1gInSc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
ženskou	ženský	k2eAgFnSc4d1	ženská
roli	role	k1gFnSc4	role
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Kristin	Kristin	k2eAgInSc4d1	Kristin
Scott	Scott	k1gInSc4	Scott
Thomasová	Thomasová	k1gFnSc1	Thomasová
a	a	k8xC	a
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
zahrál	zahrát	k5eAaPmAgMnS	zahrát
i	i	k9	i
novozélandský	novozélandský	k2eAgMnSc1d1	novozélandský
herec	herec	k1gMnSc1	herec
Sam	Sam	k1gMnSc1	Sam
Neill	Neill	k1gMnSc1	Neill
<g/>
.	.	kIx.	.
</s>
<s>
Hrála	hrát	k5eAaImAgFnS	hrát
zde	zde	k6eAd1	zde
také	také	k9	také
mladá	mladá	k1gFnSc1	mladá
Scarlett	Scarletta	k1gFnPc2	Scarletta
Johanssonová	Johanssonová	k1gFnSc1	Johanssonová
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
právě	právě	k9	právě
tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
dostal	dostat	k5eAaPmAgInS	dostat
mezi	mezi	k7c4	mezi
známé	známý	k2eAgFnPc4d1	známá
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
si	se	k3xPyFc3	se
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
zahrála	zahrát	k5eAaPmAgFnS	zahrát
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
menší	malý	k2eAgFnSc4d2	menší
roli	role	k1gFnSc4	role
kamarádky	kamarádka	k1gFnSc2	kamarádka
Grace	Grace	k1gMnSc1	Grace
Judith	Judith	k1gMnSc1	Judith
<g/>
,	,	kIx,	,
mladá	mladý	k2eAgFnSc1d1	mladá
herečka	herečka	k1gFnSc1	herečka
Kate	kat	k1gInSc5	kat
Bosworthová	Bosworthový	k2eAgNnPc4d1	Bosworthový
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
roli	role	k1gFnSc4	role
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
známá	známý	k2eAgFnSc1d1	známá
Natalie	Natalie	k1gFnSc1	Natalie
Portmanová	Portmanová	k1gFnSc1	Portmanová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Broadwayi	Broadway	k1gFnSc2	Broadway
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
píseň	píseň	k1gFnSc4	píseň
A	a	k9	a
Soft	Soft	k?	Soft
place	plac	k1gInSc6	plac
to	ten	k3xDgNnSc4	ten
fall	falnout	k5eAaPmAgMnS	falnout
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
třináctiletá	třináctiletý	k2eAgFnSc1d1	třináctiletá
dívka	dívka	k1gFnSc1	dívka
Grace	Grace	k1gMnSc1	Grace
McLaean	McLaean	k1gMnSc1	McLaean
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
otec	otec	k1gMnSc1	otec
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
maminka	maminka	k1gFnSc1	maminka
Annie	Annie	k1gFnSc1	Annie
McLean	McLean	k1gInSc4	McLean
je	být	k5eAaImIp3nS	být
šéfredaktorkou	šéfredaktorka	k1gFnSc7	šéfredaktorka
významného	významný	k2eAgInSc2d1	významný
časopisu	časopis	k1gInSc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Grace	Grako	k6eAd1	Grako
je	být	k5eAaImIp3nS	být
jedináček	jedináček	k1gMnSc1	jedináček
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ráda	rád	k2eAgMnSc4d1	rád
koně	kůň	k1gMnSc4	kůň
a	a	k8xC	a
věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
jezdectví	jezdectví	k1gNnSc2	jezdectví
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnPc1	její
zámožní	zámožný	k2eAgMnPc1d1	zámožný
rodiče	rodič	k1gMnPc1	rodič
jí	on	k3xPp3gFnSc3	on
pořídili	pořídit	k5eAaPmAgMnP	pořídit
jezdeckého	jezdecký	k2eAgMnSc4d1	jezdecký
koně	kůň	k1gMnSc4	kůň
Pilgrima	Pilgrim	k1gMnSc4	Pilgrim
<g/>
.	.	kIx.	.
</s>
<s>
Grace	Grako	k6eAd1	Grako
si	se	k3xPyFc3	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
společně	společně	k6eAd1	společně
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
kamarádkou	kamarádka	k1gFnSc7	kamarádka
Judith	Juditha	k1gFnPc2	Juditha
vyjede	vyjet	k5eAaPmIp3nS	vyjet
na	na	k7c4	na
zimní	zimní	k2eAgFnSc4d1	zimní
projížďku	projížďka	k1gFnSc4	projížďka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
ale	ale	k8xC	ale
kůň	kůň	k1gMnSc1	kůň
s	s	k7c7	s
Judith	Judith	k1gInSc1	Judith
zničehonic	zničehonic	k6eAd1	zničehonic
uklouzne	uklouznout	k5eAaPmIp3nS	uklouznout
na	na	k7c6	na
zasněženém	zasněžený	k2eAgInSc6d1	zasněžený
svahu	svah	k1gInSc6	svah
<g/>
,	,	kIx,	,
noha	noha	k1gFnSc1	noha
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
zasekne	zaseknout	k5eAaPmIp3nS	zaseknout
ve	v	k7c6	v
třmenu	třmen	k1gInSc6	třmen
a	a	k8xC	a
jezdkyně	jezdkyně	k1gFnSc1	jezdkyně
i	i	k8xC	i
kůň	kůň	k1gMnSc1	kůň
se	se	k3xPyFc4	se
po	po	k7c6	po
zamrzlém	zamrzlý	k2eAgInSc6d1	zamrzlý
svahu	svah	k1gInSc6	svah
svezou	svézt	k5eAaPmIp3nP	svézt
na	na	k7c4	na
blízkou	blízký	k2eAgFnSc4d1	blízká
silnici	silnice	k1gFnSc4	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Grace	Grako	k6eAd1	Grako
se	se	k3xPyFc4	se
Judith	Judith	k1gMnSc1	Judith
marně	marně	k6eAd1	marně
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
vyprostit	vyprostit	k5eAaPmF	vyprostit
ze	z	k7c2	z
třmenu	třmen	k1gInSc2	třmen
a	a	k8xC	a
odtáhnout	odtáhnout	k5eAaPmF	odtáhnout
ze	z	k7c2	z
silnice	silnice	k1gFnSc2	silnice
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
je	být	k5eAaImIp3nS	být
srazí	srazit	k5eAaPmIp3nP	srazit
projíždějící	projíždějící	k2eAgInSc4d1	projíždějící
nákladní	nákladní	k2eAgInSc4d1	nákladní
kamion	kamion	k1gInSc4	kamion
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
namrzlé	namrzlý	k2eAgFnSc6d1	namrzlá
vozovce	vozovka	k1gFnSc6	vozovka
dostane	dostat	k5eAaPmIp3nS	dostat
při	při	k7c6	při
prudkém	prudký	k2eAgNnSc6d1	prudké
brzdění	brzdění	k1gNnSc6	brzdění
smyk	smyk	k1gInSc4	smyk
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
kamarádka	kamarádka	k1gFnSc1	kamarádka
Judith	Judith	k1gMnSc1	Judith
i	i	k8xC	i
její	její	k3xOp3gMnSc1	její
kůň	kůň	k1gMnSc1	kůň
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
společně	společně	k6eAd1	společně
zahynou	zahynout	k5eAaPmIp3nP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Pilgrim	Pilgrim	k6eAd1	Pilgrim
se	se	k3xPyFc4	se
vzepne	vzepnout	k5eAaPmIp3nS	vzepnout
proti	proti	k7c3	proti
kamionu	kamion	k1gInSc3	kamion
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Grace	Grace	k1gMnSc1	Grace
chránil	chránit	k5eAaImAgMnS	chránit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
velmi	velmi	k6eAd1	velmi
těžce	těžce	k6eAd1	těžce
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Vážné	vážný	k2eAgNnSc1d1	vážné
zranění	zranění	k1gNnSc1	zranění
utrpí	utrpět	k5eAaPmIp3nS	utrpět
i	i	k9	i
Grace	Grace	k1gFnSc1	Grace
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
přežijí	přežít	k5eAaPmIp3nP	přežít
<g/>
,	,	kIx,	,
Grace	Grace	k1gFnSc1	Grace
má	mít	k5eAaImIp3nS	mít
zmrzačené	zmrzačený	k2eAgFnPc4d1	zmrzačená
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
pravou	pravý	k2eAgFnSc4d1	pravá
nohu	noha	k1gFnSc4	noha
jí	on	k3xPp3gFnSc2	on
musí	muset	k5eAaImIp3nP	muset
po	po	k7c6	po
převozu	převoz	k1gInSc6	převoz
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
vrtulníkem	vrtulník	k1gInSc7	vrtulník
amputovat	amputovat	k5eAaBmF	amputovat
<g/>
.	.	kIx.	.
</s>
<s>
Těžce	těžce	k6eAd1	těžce
raněného	raněný	k1gMnSc4	raněný
Pilgrima	Pilgrim	k1gMnSc4	Pilgrim
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
podaří	podařit	k5eAaPmIp3nP	podařit
veterinářům	veterinář	k1gMnPc3	veterinář
s	s	k7c7	s
vypětím	vypětí	k1gNnSc7	vypětí
všech	všecek	k3xTgFnPc2	všecek
sil	síla	k1gFnPc2	síla
chytit	chytit	k5eAaPmF	chytit
a	a	k8xC	a
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
vyléčení	vyléčení	k1gNnSc6	vyléčení
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
velké	velký	k2eAgInPc4d1	velký
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Grace	Grace	k1gFnSc1	Grace
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
také	také	k6eAd1	také
psychicky	psychicky	k6eAd1	psychicky
velice	velice	k6eAd1	velice
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
odmítá	odmítat	k5eAaImIp3nS	odmítat
chodit	chodit	k5eAaImF	chodit
mezi	mezi	k7c4	mezi
zdravé	zdravý	k2eAgFnPc4d1	zdravá
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jí	on	k3xPp3gFnSc2	on
těžce	těžce	k6eAd1	těžce
poznamená	poznamenat	k5eAaPmIp3nS	poznamenat
její	její	k3xOp3gNnPc4	její
setkání	setkání	k1gNnPc4	setkání
s	s	k7c7	s
Pilgrimem	Pilgrim	k1gInSc7	Pilgrim
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
zoufalá	zoufalý	k2eAgFnSc1d1	zoufalá
matka	matka	k1gFnSc1	matka
Annie	Annie	k1gFnSc2	Annie
poté	poté	k6eAd1	poté
začne	začít	k5eAaPmIp3nS	začít
studovat	studovat	k5eAaImF	studovat
všechny	všechen	k3xTgInPc4	všechen
dostupné	dostupný	k2eAgInPc4d1	dostupný
materiály	materiál	k1gInPc4	materiál
o	o	k7c6	o
koních	kůň	k1gMnPc6	kůň
<g/>
,	,	kIx,	,
jezdectví	jezdectví	k1gNnSc6	jezdectví
<g/>
,	,	kIx,	,
dostizích	dostih	k1gInPc6	dostih
<g/>
,	,	kIx,	,
léčení	léčení	k1gNnSc1	léčení
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
veterinární	veterinární	k2eAgFnSc6d1	veterinární
medicíně	medicína	k1gFnSc6	medicína
apod.	apod.	kA	apod.
Snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc3	svůj
nemocné	nemocný	k2eAgFnSc3d1	nemocná
dceři	dcera	k1gFnSc3	dcera
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
odborném	odborný	k2eAgInSc6d1	odborný
časopise	časopis	k1gInSc6	časopis
dočte	dočíst	k5eAaPmIp3nS	dočíst
o	o	k7c6	o
zázračném	zázračný	k2eAgMnSc6d1	zázračný
muži	muž	k1gMnSc6	muž
v	v	k7c6	v
Montaně	Montana	k1gFnSc6	Montana
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prý	prý	k9	prý
dovede	dovést	k5eAaPmIp3nS	dovést
koně	kůň	k1gMnSc4	kůň
zkrotit	zkrotit	k5eAaPmF	zkrotit
a	a	k8xC	a
spravit	spravit	k5eAaPmF	spravit
jejich	jejich	k3xOp3gFnSc4	jejich
poraněnou	poraněný	k2eAgFnSc4d1	poraněná
zvířecí	zvířecí	k2eAgFnSc4d1	zvířecí
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
Annie	Annie	k1gFnSc1	Annie
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
snaží	snažit	k5eAaImIp3nS	snažit
Toma	Tom	k1gMnSc4	Tom
telefonicky	telefonicky	k6eAd1	telefonicky
přemluvit	přemluvit	k5eAaPmF	přemluvit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ale	ale	k8xC	ale
Tom	Tom	k1gMnSc1	Tom
kategoricky	kategoricky	k6eAd1	kategoricky
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Nezbývá	zbývat	k5eNaImIp3nS	zbývat
jí	on	k3xPp3gFnSc3	on
než	než	k8xS	než
se	se	k3xPyFc4	se
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
i	i	k8xC	i
s	s	k7c7	s
Pilgrimem	Pilgrim	k1gInSc7	Pilgrim
a	a	k8xC	a
s	s	k7c7	s
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
invalidní	invalidní	k2eAgFnSc7d1	invalidní
dcerou	dcera	k1gFnSc7	dcera
<g/>
,	,	kIx,	,
vydat	vydat	k5eAaPmF	vydat
do	do	k7c2	do
Montany	Montana	k1gFnSc2	Montana
<g/>
.	.	kIx.	.
</s>
<s>
Pilgrima	Pilgrima	k1gFnSc1	Pilgrima
naloží	naložit	k5eAaPmIp3nS	naložit
do	do	k7c2	do
speciálního	speciální	k2eAgInSc2d1	speciální
přívěsu	přívěs	k1gInSc2	přívěs
za	za	k7c4	za
terénní	terénní	k2eAgInSc4d1	terénní
automobil	automobil	k1gInSc4	automobil
a	a	k8xC	a
vydají	vydat	k5eAaPmIp3nP	vydat
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zaříkávač	zaříkávač	k1gMnSc1	zaříkávač
Tom	Tom	k1gMnSc1	Tom
Booker	Booker	k1gMnSc1	Booker
(	(	kIx(	(
<g/>
Robert	Robert	k1gMnSc1	Robert
Redford	Redford	k1gMnSc1	Redford
<g/>
)	)	kIx)	)
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
bratrově	bratrův	k2eAgFnSc6d1	bratrova
rodinné	rodinný	k2eAgFnSc6d1	rodinná
dobytkářské	dobytkářský	k2eAgFnSc6d1	dobytkářská
farmě	farma	k1gFnSc6	farma
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
je	být	k5eAaImIp3nS	být
Pilgrim	Pilgrim	k1gInSc4	Pilgrim
i	i	k9	i
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgInPc4d1	počáteční
problémy	problém	k1gInPc4	problém
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
hodnější	hodný	k2eAgMnSc1d2	hodnější
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
si	se	k3xPyFc3	se
Grace	Grace	k1gMnSc1	Grace
nakloní	naklonit	k5eAaPmIp3nS	naklonit
a	a	k8xC	a
pomůže	pomoct	k5eAaPmIp3nS	pomoct
jí	on	k3xPp3gFnSc7	on
a	a	k8xC	a
ochrání	ochránit	k5eAaPmIp3nS	ochránit
ji	on	k3xPp3gFnSc4	on
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
zahořkla	zahořknout	k5eAaPmAgFnS	zahořknout
<g/>
,	,	kIx,	,
Annie	Annius	k1gMnPc4	Annius
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
musí	muset	k5eAaImIp3nP	muset
také	také	k9	také
vyřešit	vyřešit	k5eAaPmF	vyřešit
svůj	svůj	k3xOyFgInSc4	svůj
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
nemocné	mocný	k2eNgFnSc3d1	mocný
dceři	dcera	k1gFnSc3	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
společně	společně	k6eAd1	společně
s	s	k7c7	s
Annie	Annie	k1gFnPc4	Annie
prožívá	prožívat	k5eAaImIp3nS	prožívat
krásné	krásný	k2eAgFnPc4d1	krásná
chvíle	chvíle	k1gFnPc4	chvíle
<g/>
,	,	kIx,	,
Annie	Annie	k1gFnSc1	Annie
opět	opět	k6eAd1	opět
začne	začít	k5eAaPmIp3nS	začít
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
a	a	k8xC	a
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
krásy	krása	k1gFnSc2	krása
venkovského	venkovský	k2eAgInSc2d1	venkovský
života	život	k1gInSc2	život
a	a	k8xC	a
místní	místní	k2eAgFnSc2d1	místní
divoké	divoký	k2eAgFnSc2d1	divoká
podhorské	podhorský	k2eAgFnSc2d1	podhorská
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
s	s	k7c7	s
Annie	Annie	k1gFnPc1	Annie
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
sblíží	sblížit	k5eAaPmIp3nP	sblížit
a	a	k8xC	a
zamilují	zamilovat	k5eAaPmIp3nP	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Annie	Annie	k1gFnSc1	Annie
i	i	k8xC	i
Grace	Grace	k1gFnSc1	Grace
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
dvě	dva	k4xCgFnPc1	dva
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
spřátelí	spřátelit	k5eAaPmIp3nS	spřátelit
i	i	k9	i
s	s	k7c7	s
Tomovou	Tomův	k2eAgFnSc7d1	Tomova
početnou	početný	k2eAgFnSc7d1	početná
rodinou	rodina	k1gFnSc7	rodina
(	(	kIx(	(
<g/>
3	[number]	k4	3
malí	malý	k2eAgMnPc1d1	malý
synovci	synovec	k1gMnPc1	synovec
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
švagrová	švagrová	k1gFnSc1	švagrová
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
ukáže	ukázat	k5eAaPmIp3nS	ukázat
Annie	Annie	k1gFnPc4	Annie
<g/>
,	,	kIx,	,
navyklé	navyklý	k2eAgInPc4d1	navyklý
na	na	k7c4	na
rušný	rušný	k2eAgInSc4d1	rušný
velkoměstský	velkoměstský	k2eAgInSc4d1	velkoměstský
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
krásy	krása	k1gFnPc4	krása
svobodného	svobodný	k2eAgInSc2d1	svobodný
života	život	k1gInSc2	život
na	na	k7c6	na
podhorské	podhorský	k2eAgFnSc6d1	podhorská
dobytkářské	dobytkářský	k2eAgFnSc6d1	dobytkářská
farmě	farma	k1gFnSc6	farma
<g/>
.	.	kIx.	.
</s>
<s>
Blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
ale	ale	k9	ale
uzdravení	uzdravený	k2eAgMnPc1d1	uzdravený
koně	kůň	k1gMnPc1	kůň
a	a	k8xC	a
na	na	k7c4	na
farmu	farma	k1gFnSc4	farma
v	v	k7c6	v
Montaně	Montana	k1gFnSc6	Montana
nečekaně	nečekaně	k6eAd1	nečekaně
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
Annin	Annin	k2eAgMnSc1d1	Annin
manžel	manžel	k1gMnSc1	manžel
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
Grace	Grace	k1gMnSc1	Grace
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Grace	Grako	k6eAd1	Grako
společně	společně	k6eAd1	společně
s	s	k7c7	s
Tomem	Tom	k1gMnSc7	Tom
Pilgrima	Pilgrimum	k1gNnSc2	Pilgrimum
definitivně	definitivně	k6eAd1	definitivně
vyléčí	vyléčit	k5eAaPmIp3nS	vyléčit
a	a	k8xC	a
Grace	Grace	k1gFnSc1	Grace
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
opět	opět	k6eAd1	opět
začne	začít	k5eAaPmIp3nS	začít
jezdit	jezdit	k5eAaImF	jezdit
<g/>
.	.	kIx.	.
</s>
<s>
Annie	Annie	k1gFnSc1	Annie
ale	ale	k9	ale
musí	muset	k5eAaImIp3nS	muset
volit	volit	k5eAaImF	volit
vážné	vážný	k2eAgNnSc4d1	vážné
životní	životní	k2eAgNnSc4d1	životní
dilema	dilema	k1gNnSc4	dilema
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgMnPc7	dva
milovanými	milovaný	k2eAgMnPc7d1	milovaný
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
aby	aby	kYmCp3nS	aby
udržela	udržet	k5eAaPmAgFnS	udržet
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
šaramantním	šaramantní	k2eAgMnSc7d1	šaramantní
a	a	k8xC	a
laskavým	laskavý	k2eAgMnSc7d1	laskavý
zaříkávačem	zaříkávač	k1gMnSc7	zaříkávač
koní	kůň	k1gMnPc2	kůň
Tomem	Tom	k1gMnSc7	Tom
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
bolí	bolet	k5eAaImIp3nS	bolet
<g/>
,	,	kIx,	,
učiní	učinit	k5eAaImIp3nS	učinit
tak	tak	k6eAd1	tak
co	co	k3yInSc4	co
nejšetrněji	šetrně	k6eAd3	šetrně
aby	aby	k9	aby
Tomovi	Tom	k1gMnSc3	Tom
nezpůsobila	způsobit	k5eNaPmAgFnS	způsobit
bolest	bolest	k1gFnSc1	bolest
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
takto	takto	k6eAd1	takto
přišel	přijít	k5eAaPmAgMnS	přijít
již	již	k6eAd1	již
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
bývalou	bývalý	k2eAgFnSc4d1	bývalá
manželku-violoncellistku	manželkuioloncellistka	k1gFnSc4	manželku-violoncellistka
<g/>
.	.	kIx.	.
</s>
<s>
Navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
mu	on	k3xPp3gMnSc3	on
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
naposledy	naposledy	k6eAd1	naposledy
projeli	projet	k5eAaPmAgMnP	projet
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
Tom	Tom	k1gMnSc1	Tom
koně	kůň	k1gMnSc2	kůň
přivede	přivést	k5eAaPmIp3nS	přivést
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Annie	Annie	k1gFnSc1	Annie
i	i	k9	i
s	s	k7c7	s
Pilgrimem	Pilgrim	k1gInSc7	Pilgrim
pryč	pryč	k6eAd1	pryč
a	a	k8xC	a
Tom	Tom	k1gMnSc1	Tom
vidí	vidět	k5eAaImIp3nS	vidět
v	v	k7c6	v
ujíždějícím	ujíždějící	k2eAgNnSc6d1	ujíždějící
autě	auto	k1gNnSc6	auto
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
tržby	tržba	k1gFnPc1	tržba
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
trojnásobku	trojnásobek	k1gInSc3	trojnásobek
původního	původní	k2eAgMnSc4d1	původní
rozpočtut	rozpočtout	k5eAaPmNgMnS	rozpočtout
<g/>
,	,	kIx,	,
i	i	k8xC	i
tržby	tržba	k1gFnPc1	tržba
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
USA	USA	kA	USA
jej	on	k3xPp3gNnSc4	on
převýšily	převýšit	k5eAaPmAgFnP	převýšit
<g/>
.	.	kIx.	.
</s>
<s>
Rozpočet	rozpočet	k1gInSc1	rozpočet
<g/>
:	:	kIx,	:
60	[number]	k4	60
000	[number]	k4	000
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
Tržby	tržba	k1gFnSc2	tržba
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
:	:	kIx,	:
75	[number]	k4	75
383	[number]	k4	383
563	[number]	k4	563
dolarů	dolar	k1gInPc2	dolar
Celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
tržby	tržba	k1gFnSc2	tržba
<g/>
:	:	kIx,	:
186	[number]	k4	186
883	[number]	k4	883
563	[number]	k4	563
dolarů	dolar	k1gInPc2	dolar
Zaříkávač	zaříkávač	k1gMnSc1	zaříkávač
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
http://www.fdb.cz/film/20908-zarikavac-koni-the-horse-whisperer.html	[url]	k1gInSc1	http://www.fdb.cz/film/20908-zarikavac-koni-the-horse-whisperer.html
Filmová	filmový	k2eAgFnSc1d1	filmová
databáze	databáze	k1gFnSc1	databáze
</s>
