<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Klus	klus	k1gInSc1	klus
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1986	[number]	k4	1986
Třinec	Třinec	k1gInSc1	Třinec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
písničkář	písničkář	k1gMnSc1	písničkář
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
moderní	moderní	k2eAgMnSc1d1	moderní
pětibojař	pětibojař	k1gMnSc1	pětibojař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
získal	získat	k5eAaPmAgMnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
pětiboji	pětiboj	k1gInSc6	pětiboj
(	(	kIx(	(
<g/>
družstva	družstvo	k1gNnSc2	družstvo
<g/>
)	)	kIx)	)
na	na	k7c6	na
dorosteneckém	dorostenecký	k2eAgNnSc6d1	dorostenecké
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
sportu	sport	k1gInSc3	sport
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Dopis	dopis	k1gInSc4	dopis
pěveckou	pěvecký	k2eAgFnSc4d1	pěvecká
soutěž	soutěž	k1gFnSc4	soutěž
CzechTalent	CzechTalent	k1gMnSc1	CzechTalent
Zlín	Zlín	k1gInSc1	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
mu	on	k3xPp3gNnSc3	on
firma	firma	k1gFnSc1	firma
Sony	Sony	kA	Sony
BMG	BMG	kA	BMG
vydala	vydat	k5eAaPmAgFnS	vydat
jeho	jeho	k3xOp3gNnSc4	jeho
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
záhu	záhus	k1gInSc2	záhus
<g/>
(	(	kIx(	(
<g/>
d	d	k?	d
<g/>
)	)	kIx)	)
<g/>
by	by	kYmCp3nS	by
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
napsal	napsat	k5eAaPmAgMnS	napsat
a	a	k8xC	a
nahrál	nahrát	k5eAaPmAgMnS	nahrát
hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Anglické	anglický	k2eAgFnSc2d1	anglická
jahody	jahoda	k1gFnSc2	jahoda
a	a	k8xC	a
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
spoluhráčem	spoluhráč	k1gMnSc7	spoluhráč
Jiřím	Jiří	k1gMnSc7	Jiří
Kučerovským	Kučerovský	k2eAgMnSc7d1	Kučerovský
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
předskokan	předskokan	k1gMnSc1	předskokan
skupiny	skupina	k1gFnSc2	skupina
Chinaski	Chinask	k1gFnSc2	Chinask
na	na	k7c6	na
Chinaski	Chinaski	k1gNnSc6	Chinaski
Space	Space	k1gFnSc2	Space
Tour	Tour	k1gInSc4	Tour
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vydal	vydat	k5eAaPmAgMnS	vydat
druhé	druhý	k4xOgNnSc4	druhý
album	album	k1gNnSc4	album
Hlavní	hlavní	k2eAgInSc1d1	hlavní
uzávěr	uzávěr	k1gInSc1	uzávěr
splínu	splín	k1gInSc2	splín
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
vyšel	vyjít	k5eAaPmAgInS	vyjít
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
zpěvník	zpěvník	k1gInSc1	zpěvník
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
písničkáŘ	písničkář	k1gMnSc1	písničkář
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
prvního	první	k4xOgMnSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgNnSc2	druhý
CD	CD	kA	CD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2012	[number]	k4	2012
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
studium	studium	k1gNnSc4	studium
herectví	herectví	k1gNnPc2	herectví
na	na	k7c6	na
Divadelní	divadelní	k2eAgFnSc6d1	divadelní
fakultě	fakulta	k1gFnSc6	fakulta
Akademie	akademie	k1gFnSc2	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
(	(	kIx(	(
<g/>
DAMU	DAMU	kA	DAMU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
herce	herec	k1gMnSc4	herec
jej	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
vidět	vidět	k5eAaImF	vidět
například	například	k6eAd1	například
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Hop	hop	k0	hop
nebo	nebo	k8xC	nebo
Trop	trop	k2eAgInPc6d1	trop
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Šejdrem	šejdr	k1gInSc7	šejdr
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
prince	princ	k1gMnSc2	princ
Jakuba	Jakub	k1gMnSc2	Jakub
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
ve	v	k7c6	v
vánoční	vánoční	k2eAgFnSc6d1	vánoční
pohádce	pohádka	k1gFnSc6	pohádka
Tajemství	tajemství	k1gNnSc1	tajemství
staré	starý	k2eAgFnSc2d1	stará
bambitky	bambitka	k1gFnSc2	bambitka
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
školního	školní	k2eAgNnSc2d1	školní
divadla	divadlo	k1gNnSc2	divadlo
DISK	disk	k1gInSc1	disk
účinkoval	účinkovat	k5eAaImAgInS	účinkovat
ve	v	k7c4	v
hrách	hrách	k1gInSc4	hrách
Malá	malý	k2eAgFnSc1d1	malá
mořská	mořský	k2eAgFnSc1d1	mořská
víla	víla	k1gFnSc1	víla
<g/>
,	,	kIx,	,
Kazimír	Kazimír	k1gMnSc1	Kazimír
a	a	k8xC	a
Karolína	Karolína	k1gFnSc1	Karolína
<g/>
,	,	kIx,	,
Tartuffe	Tartuffe	k1gMnSc1	Tartuffe
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
Lazarová	Lazarová	k1gFnSc1	Lazarová
a	a	k8xC	a
Racek	racek	k1gMnSc1	racek
<g/>
.	.	kIx.	.
</s>
<s>
Účinkuje	účinkovat	k5eAaImIp3nS	účinkovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Deadline	Deadlin	k1gInSc5	Deadlin
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Divadle	divadlo	k1gNnSc6	divadlo
Ypsilon	ypsilon	k1gNnSc2	ypsilon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zpěvákem	zpěvák	k1gMnSc7	zpěvák
roku	rok	k1gInSc2	rok
hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
TV	TV	kA	TV
stanice	stanice	k1gFnSc2	stanice
Óčko	Óčko	k6eAd1	Óčko
i	i	k9	i
výročních	výroční	k2eAgMnPc2d1	výroční
Andělů	Anděl	k1gMnPc2	Anděl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2011	[number]	k4	2011
a	a	k8xC	a
2012	[number]	k4	2012
získal	získat	k5eAaPmAgInS	získat
stříbrného	stříbrný	k2eAgMnSc4d1	stříbrný
a	a	k8xC	a
zlatého	zlatý	k2eAgMnSc4d1	zlatý
Českého	český	k2eAgMnSc4d1	český
slavíka	slavík	k1gMnSc4	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS	podporovat
také	také	k9	také
charitativní	charitativní	k2eAgInPc4d1	charitativní
projekty	projekt	k1gInPc4	projekt
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
ČT	ČT	kA	ČT
pořad	pořad	k1gInSc4	pořad
Klus	klus	k1gInSc4	klus
pro	pro	k7c4	pro
Kuře	kuře	k1gNnSc4	kuře
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
jako	jako	k8xC	jako
moderátor	moderátor	k1gMnSc1	moderátor
i	i	k9	i
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
interpretů	interpret	k1gMnPc2	interpret
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
vydal	vydat	k5eAaPmAgMnS	vydat
album	album	k1gNnSc4	album
Racek	racek	k1gMnSc1	racek
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
dlouho	dlouho	k6eAd1	dlouho
dopředu	dopředu	k6eAd1	dopředu
vyprodáno	vyprodat	k5eAaPmNgNnS	vyprodat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tří	tři	k4xCgInPc2	tři
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
koncertech	koncert	k1gInPc6	koncert
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
udržet	udržet	k5eAaPmF	udržet
pozornost	pozornost	k1gFnSc4	pozornost
publika	publikum	k1gNnSc2	publikum
běžně	běžně	k6eAd1	běžně
i	i	k9	i
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgFnPc2	tři
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
mají	mít	k5eAaImIp3nP	mít
rovněž	rovněž	k9	rovněž
hudební	hudební	k2eAgFnPc1d1	hudební
aranže	aranže	k1gFnPc1	aranže
i	i	k8xC	i
výrazný	výrazný	k2eAgInSc1d1	výrazný
kytarový	kytarový	k2eAgInSc1d1	kytarový
doprovod	doprovod	k1gInSc1	doprovod
Jiřího	Jiří	k1gMnSc2	Jiří
Kučerovského	Kučerovský	k2eAgMnSc2d1	Kučerovský
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
písničkářskou	písničkářský	k2eAgFnSc4d1	písničkářská
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
tvorbu	tvorba	k1gFnSc4	tvorba
a	a	k8xC	a
interpretaci	interpretace	k1gFnSc4	interpretace
neomezuje	omezovat	k5eNaImIp3nS	omezovat
hudebními	hudební	k2eAgInPc7d1	hudební
žánry	žánr	k1gInPc7	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
tvorbu	tvorba	k1gFnSc4	tvorba
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
text	text	k1gInSc4	text
a	a	k8xC	a
melodii	melodie	k1gFnSc4	melodie
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vlivy	vliv	k1gInPc7	vliv
předchůdců	předchůdce	k1gMnPc2	předchůdce
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
vlastním	vlastní	k2eAgInSc7d1	vlastní
výrazem	výraz	k1gInSc7	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
nebrání	bránit	k5eNaImIp3nS	bránit
komentování	komentování	k1gNnSc1	komentování
situace	situace	k1gFnSc2	situace
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Klus	klus	k1gInSc4	klus
vydal	vydat	k5eAaPmAgMnS	vydat
další	další	k2eAgFnSc4d1	další
desku	deska	k1gFnSc4	deska
Proměnamě	Proměnama	k1gFnSc3	Proměnama
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
proměnou	proměna	k1gFnSc7	proměna
prošel	projít	k5eAaPmAgInS	projít
<g/>
.	.	kIx.	.
</s>
<s>
Uklidnil	uklidnit	k5eAaPmAgMnS	uklidnit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
spokojený	spokojený	k2eAgInSc4d1	spokojený
rodinný	rodinný	k2eAgInSc4d1	rodinný
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
nemá	mít	k5eNaImIp3nS	mít
potřebu	potřeba	k1gFnSc4	potřeba
se	se	k3xPyFc4	se
za	za	k7c7	za
čímkoli	cokoli	k3yInSc7	cokoli
honit	honit	k5eAaImF	honit
a	a	k8xC	a
pro	pro	k7c4	pro
něco	něco	k3yInSc4	něco
se	se	k3xPyFc4	se
stresovat	stresovat	k5eAaImF	stresovat
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
jistotu	jistota	k1gFnSc4	jistota
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
tak	tak	k6eAd1	tak
hledal	hledat	k5eAaImAgInS	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jí	jíst	k5eAaImIp3nS	jíst
manželka	manželka	k1gFnSc1	manželka
Tamara	Tamara	k1gFnSc1	Tamara
Kubová	Kubová	k1gFnSc1	Kubová
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
finalistka	finalistka	k1gFnSc1	finalistka
z	z	k7c2	z
populární	populární	k2eAgFnSc2d1	populární
hudební	hudební	k2eAgFnSc2d1	hudební
soutěže	soutěž	k1gFnSc2	soutěž
Hlas	hlas	k1gInSc1	hlas
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
dcera	dcera	k1gFnSc1	dcera
Josefína	Josefína	k1gFnSc1	Josefína
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
na	na	k7c6	na
cenách	cena	k1gFnPc6	cena
Anděl	Anděla	k1gFnPc2	Anděla
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
jako	jako	k8xC	jako
nejprodávanější	prodávaný	k2eAgNnSc1d3	nejprodávanější
album	album	k1gNnSc1	album
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
přes	přes	k7c4	přes
dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
Tři	tři	k4xCgMnPc4	tři
bratři	bratr	k1gMnPc1	bratr
v	v	k7c6	v
roli	role	k1gFnSc6	role
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
vydá	vydat	k5eAaPmIp3nS	vydat
hledat	hledat	k5eAaImF	hledat
zakletou	zakletý	k2eAgFnSc4d1	zakletá
princeznu	princezna	k1gFnSc4	princezna
Růženku	Růženka	k1gFnSc4	Růženka
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
soundtracku	soundtrack	k1gInSc6	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
opět	opět	k6eAd1	opět
nahrál	nahrát	k5eAaBmAgMnS	nahrát
s	s	k7c7	s
uskupením	uskupení	k1gNnSc7	uskupení
Cílová	cílový	k2eAgFnSc1d1	cílová
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Anat	Anat	k2eAgInSc4d1	Anat
život	život	k1gInSc4	život
není	být	k5eNaImIp3nS	být
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
protiválečnými	protiválečný	k2eAgFnPc7d1	protiválečná
písněmi	píseň	k1gFnPc7	píseň
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
alba	album	k1gNnSc2	album
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
semitskou	semitský	k2eAgFnSc4d1	semitská
bohyni	bohyně	k1gFnSc4	bohyně
války	válka	k1gFnSc2	válka
a	a	k8xC	a
plodnosti	plodnost	k1gFnSc2	plodnost
Anat.	Anat.	k1gFnSc2	Anat.
Singlem	singl	k1gInSc7	singl
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
JdeVoZem	JdeVoZem	k1gInSc1	JdeVoZem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
písničkáŘ	písničkář	k1gMnSc1	písničkář
•	•	k?	•
zpěvník	zpěvník	k1gInSc1	zpěvník
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
JÁ	já	k1gNnSc6	já
písničkáŘ	písničkář	k1gMnSc1	písničkář
II	II	kA	II
•	•	k?	•
zpěvník	zpěvník	k1gInSc1	zpěvník
<g/>
,	,	kIx,	,
Albatros	albatros	k1gMnSc1	albatros
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Tomáše	Tomáš	k1gMnSc2	Tomáš
Kluse	klus	k1gInSc5	klus
<g/>
.	.	kIx.	.
</s>
<s>
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
záhu	záhus	k1gInSc2	záhus
<g/>
(	(	kIx(	(
<g/>
d	d	k?	d
<g/>
)	)	kIx)	)
<g/>
by	by	k9	by
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Hlavní	hlavní	k2eAgInSc1d1	hlavní
uzávěr	uzávěr	k1gInSc1	uzávěr
splínu	splín	k1gInSc2	splín
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Racek	racek	k1gMnSc1	racek
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Proměnamě	Proměnama	k1gFnSc3	Proměnama
s	s	k7c7	s
Jeho	jeho	k3xOp3gFnSc7	jeho
cílovou	cílový	k2eAgFnSc7d1	cílová
skupinou	skupina	k1gFnSc7	skupina
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Anat	Anat	k2eAgInSc1d1	Anat
život	život	k1gInSc1	život
není	být	k5eNaImIp3nS	být
s	s	k7c7	s
Jeho	jeho	k3xOp3gFnSc7	jeho
cílovou	cílový	k2eAgFnSc7d1	cílová
skupinou	skupina	k1gFnSc7	skupina
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
regionální	regionální	k2eAgFnSc2d1	regionální
hudební	hudební	k2eAgFnSc2d1	hudební
soutěže	soutěž	k1gFnSc2	soutěž
mladých	mladý	k2eAgMnPc2d1	mladý
zpěváků	zpěvák	k1gMnPc2	zpěvák
CzechTalent	CzechTalent	k1gMnSc1	CzechTalent
Zlín	Zlín	k1gInSc1	Zlín
<g/>
,	,	kIx,	,
obdržel	obdržet	k5eAaPmAgInS	obdržet
v	v	k7c6	v
r.	r.	kA	r.
2007	[number]	k4	2007
jak	jak	k8xC	jak
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
festivalu	festival	k1gInSc2	festival
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
cenu	cena	k1gFnSc4	cena
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
-	-	kIx~	-
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
Jiné	jiný	k2eAgInPc4d1	jiný
výskyty	výskyt	k1gInPc4	výskyt
V	v	k7c4	v
zastoupení	zastoupení	k1gNnSc4	zastoupení
čtenářů	čtenář	k1gMnPc2	čtenář
webportálu	webportál	k1gInSc2	webportál
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Klus	klus	k1gInSc1	klus
v	v	k7c6	v
r.	r.	kA	r.
2014	[number]	k4	2014
vyhodnocen	vyhodnotit	k5eAaPmNgInS	vyhodnotit
za	za	k7c2	za
vítěze	vítěz	k1gMnSc2	vítěz
ankety	anketa	k1gFnSc2	anketa
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
25	[number]	k4	25
objevů	objev	k1gInPc2	objev
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
kultuře	kultura	k1gFnSc6	kultura
za	za	k7c4	za
čtvrtstoletí	čtvrtstoletí	k1gNnSc4	čtvrtstoletí
uplynulém	uplynulý	k2eAgInSc6d1	uplynulý
od	od	k7c2	od
Sametové	sametový	k2eAgFnSc2d1	sametová
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
