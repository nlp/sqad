<p>
<s>
Minor	minor	k2eAgMnSc1d1	minor
Earth	Earth	k1gMnSc1	Earth
Major	major	k1gMnSc1	major
Sky	Sky	k1gMnSc1	Sky
je	být	k5eAaImIp3nS	být
sedmým	sedmý	k4xOgNnSc7	sedmý
albem	album	k1gNnSc7	album
norské	norský	k2eAgFnSc2d1	norská
skupiny	skupina	k1gFnSc2	skupina
A-ha	A	k1gInSc2	A-h
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
k	k	k7c3	k
titulní	titulní	k2eAgFnSc3d1	titulní
skladbě	skladba	k1gFnSc3	skladba
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgMnS	natáčet
v	v	k7c6	v
pískovně	pískovna	k1gFnSc6	pískovna
nedaleko	nedaleko	k7c2	nedaleko
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Řazení	řazení	k1gNnPc1	řazení
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
Minor	minor	k2eAgMnSc1d1	minor
Earth	Earth	k1gMnSc1	Earth
Major	major	k1gMnSc1	major
Sky	Sky	k1gMnSc1	Sky
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
</s>
</p>
<p>
<s>
Little	Little	k6eAd1	Little
Black	Black	k1gInSc1	Black
Heart	Heart	k1gInSc1	Heart
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
</s>
</p>
<p>
<s>
Velvet	Velvet	k1gInSc1	Velvet
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
21	[number]	k4	21
</s>
</p>
<p>
<s>
Summer	Summer	k1gMnSc1	Summer
Moved	Moved	k1gMnSc1	Moved
On	on	k3xPp3gMnSc1	on
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
38	[number]	k4	38
</s>
</p>
<p>
<s>
The	The	k?	The
Sun	Sun	kA	Sun
Never	Never	k1gInSc1	Never
Shone	shon	k1gInSc5	shon
That	That	k2eAgInSc4d1	That
Day	Day	k1gFnSc7	Day
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
</s>
</p>
<p>
<s>
To	to	k9	to
Let	let	k1gInSc1	let
You	You	k1gMnSc2	You
Win	Win	k1gMnSc2	Win
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
24	[number]	k4	24
</s>
</p>
<p>
<s>
The	The	k?	The
Company	Compana	k1gFnPc1	Compana
Man	mana	k1gFnPc2	mana
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
</s>
</p>
<p>
<s>
Thought	Thought	k2eAgMnSc1d1	Thought
That	That	k1gMnSc1	That
It	It	k1gMnSc1	It
Was	Was	k1gMnSc1	Was
You	You	k1gMnSc1	You
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
</s>
</p>
<p>
<s>
I	i	k9	i
Wish	Wish	k1gInSc1	Wish
I	i	k8xC	i
Cared	Cared	k1gInSc1	Cared
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
23	[number]	k4	23
</s>
</p>
<p>
<s>
Barely	barel	k1gInPc1	barel
Hanging	Hanging	k1gInSc1	Hanging
On	on	k3xPp3gMnSc1	on
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
</s>
</p>
<p>
<s>
You	You	k?	You
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Never	Never	k1gMnSc1	Never
Get	Get	k1gMnSc1	Get
Over	Over	k1gMnSc1	Over
Me	Me	k1gMnSc1	Me
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
</s>
</p>
<p>
<s>
I	i	k9	i
Won	won	k1gInSc1	won
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Forget	Forget	k1gInSc4	Forget
Her	hra	k1gFnPc2	hra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
44	[number]	k4	44
</s>
</p>
<p>
<s>
Mary	Mary	k1gFnSc1	Mary
Ellen	Ellna	k1gFnPc2	Ellna
Makes	Makes	k1gMnSc1	Makes
the	the	k?	the
Moment	moment	k1gInSc1	moment
Count	Count	k1gInSc1	Count
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Morten	Morten	k2eAgInSc1d1	Morten
Harket	Harket	k1gInSc1	Harket
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Waaktaar-Savoy	Waaktaar-Savoa	k1gFnSc2	Waaktaar-Savoa
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Magne	Magnout	k5eAaImIp3nS	Magnout
Furuholmen	Furuholmen	k1gInSc1	Furuholmen
(	(	kIx(	(
<g/>
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Hosté	host	k1gMnPc1	host
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc4	číslo
za	za	k7c7	za
pomlčkou	pomlčka	k1gFnSc7	pomlčka
je	být	k5eAaImIp3nS	být
pořadové	pořadový	k2eAgNnSc4d1	pořadové
číslo	číslo	k1gNnSc4	číslo
skladby	skladba	k1gFnSc2	skladba
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Frode	Frode	k6eAd1	Frode
Unneland	Unneland	k1gInSc1	Unneland
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
-	-	kIx~	-
1	[number]	k4	1
</s>
</p>
<p>
<s>
Sven	Sven	k1gMnSc1	Sven
Lindvall	Lindvall	k1gMnSc1	Lindvall
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
-	-	kIx~	-
2	[number]	k4	2
a	a	k8xC	a
13	[number]	k4	13
</s>
</p>
<p>
<s>
Simone	Simon	k1gMnSc5	Simon
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc4	zpěv
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
</s>
</p>
<p>
<s>
Per	prát	k5eAaImRp2nS	prát
Lindvall	Lindvall	k1gMnSc1	Lindvall
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
,	,	kIx,	,
10	[number]	k4	10
a	a	k8xC	a
13	[number]	k4	13
</s>
</p>
<p>
<s>
Frode	Frode	k6eAd1	Frode
Unneland	Unneland	k1gInSc1	Unneland
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
-	-	kIx~	-
7	[number]	k4	7
</s>
</p>
<p>
<s>
Lauren	Laurna	k1gFnPc2	Laurna
Savoy	Savoa	k1gFnSc2	Savoa
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
-	-	kIx~	-
11	[number]	k4	11
</s>
</p>
<p>
<s>
Per	prát	k5eAaImRp2nS	prát
Hillestad	Hillestad	k1gInSc1	Hillestad
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
-	-	kIx~	-
11	[number]	k4	11
</s>
</p>
<p>
<s>
Vertavo	Vertava	k1gFnSc5	Vertava
Quartet	Quartet	k1gInSc4	Quartet
(	(	kIx(	(
<g/>
smyčce	smyčec	k1gInPc4	smyčec
<g/>
)	)	kIx)	)
-	-	kIx~	-
11	[number]	k4	11
</s>
</p>
<p>
<s>
Johun	Johun	k1gMnSc1	Johun
Bogeberg	Bogeberg	k1gMnSc1	Bogeberg
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
-	-	kIx~	-
11	[number]	k4	11
<g/>
členové	člen	k1gMnPc1	člen
Oslo	Oslo	k1gNnPc2	Oslo
Philharmonic	Philharmonice	k1gFnPc2	Philharmonice
Orchestra	orchestra	k1gFnSc1	orchestra
a	a	k8xC	a
Norwegian	Norwegian	k1gInSc1	Norwegian
Radio	radio	k1gNnSc1	radio
Orchestra	orchestra	k1gFnSc1	orchestra
(	(	kIx(	(
<g/>
smyčce	smyčec	k1gInSc2	smyčec
<g/>
)	)	kIx)	)
</s>
</p>
