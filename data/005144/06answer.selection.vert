<s>
Soustava	soustava	k1gFnSc1	soustava
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pásmo	pásmo	k1gNnSc1	pásmo
komet	kometa	k1gFnPc2	kometa
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
1 000	[number]	k4	1 000
astronomických	astronomický	k2eAgFnPc2d1	astronomická
jednotek	jednotka	k1gFnPc2	jednotka
AU	au	k0	au
<g/>
,	,	kIx,	,
planetární	planetární	k2eAgFnSc1d1	planetární
soustava	soustava	k1gFnSc1	soustava
50	[number]	k4	50
AU	au	k0	au
<g/>
.	.	kIx.	.
</s>
