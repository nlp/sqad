<s>
Tulipán	tulipán	k1gMnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Tulipán	tulipán	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tulipán	tulipán	k1gInSc1
Tulipán	tulipán	k1gInSc1
zahradní	zahradní	k2eAgInSc1d1
(	(	kIx(
<g/>
Tulipa	Tulipa	k1gFnSc1
×	×	k?
gesneriana	gesneriana	k1gFnSc1
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
jednoděložné	jednoděložná	k1gFnPc1
(	(	kIx(
<g/>
Liliopsida	Liliopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
liliotvaré	liliotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Liliales	Liliales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
liliovité	liliovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Liliaceae	Liliacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
tulipán	tulipán	k1gInSc1
(	(	kIx(
<g/>
Tulipa	Tulipa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
L.	L.	kA
<g/>
,	,	kIx,
1753	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tulipánové	tulipánový	k2eAgNnSc1d1
pole	pole	k1gNnSc1
v	v	k7c6
Holandsku	Holandsko	k1gNnSc6
</s>
<s>
Tulipány	tulipán	k1gInPc1
</s>
<s>
Tulipán	tulipán	k1gMnSc1
</s>
<s>
Tulipán	tulipán	k1gInSc1
(	(	kIx(
<g/>
Tulipa	Tulipa	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rozsáhlý	rozsáhlý	k2eAgInSc4d1
rod	rod	k1gInSc4
jednoděložných	jednoděložný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
liliovitých	liliovitý	k2eAgInPc2d1
v	v	k7c6
užším	úzký	k2eAgNnSc6d2
pojetí	pojetí	k1gNnSc6
(	(	kIx(
<g/>
Liliaceae	Liliaceae	k1gFnSc1
s.	s.	k?
str	str	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Tulipány	tulipán	k1gInPc1
jsou	být	k5eAaImIp3nP
vytrvalé	vytrvalý	k2eAgFnPc4d1
cibulovité	cibulovitý	k2eAgFnPc4d1
rostliny	rostlina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
jsou	být	k5eAaImIp3nP
jednoduché	jednoduchý	k2eAgFnPc1d1
<g/>
,	,	kIx,
střídavé	střídavý	k2eAgFnPc1d1
<g/>
,	,	kIx,
horní	horní	k2eAgFnPc1d1
listy	lista	k1gFnPc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
zmenšují	zmenšovat	k5eAaImIp3nP
oproti	oproti	k7c3
spodním	spodní	k2eAgFnPc3d1
<g/>
,	,	kIx,
s	s	k7c7
listovými	listový	k2eAgFnPc7d1
pochvami	pochva	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čepele	čepel	k1gInSc2
listů	list	k1gInPc2
jsou	být	k5eAaImIp3nP
čárkovité	čárkovitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
kopinaté	kopinatý	k2eAgFnPc1d1
až	až	k6eAd1
podlouhlé	podlouhlý	k2eAgFnPc1d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
trochu	trochu	k6eAd1
ztlustlé	ztlustlý	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	s	k7c7
souběžnou	souběžný	k2eAgFnSc7d1
žilnatinou	žilnatina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květy	Květa	k1gFnPc1
jsou	být	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
velké	velký	k2eAgInPc1d1
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
jednotlivé	jednotlivý	k2eAgFnPc1d1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
stonku	stonek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okvětí	okvětí	k1gNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
šesti	šest	k4xCc2
okvětních	okvětní	k2eAgInPc2d1
lístků	lístek	k1gInPc2
ve	v	k7c6
dvou	dva	k4xCgInPc6
přeslenech	přeslen	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyčinek	tyčinka	k1gFnPc2
je	být	k5eAaImIp3nS
šest	šest	k4xCc1
ve	v	k7c6
dvou	dva	k4xCgInPc6
přeslenech	přeslen	k1gInPc6
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
volné	volný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gyneceum	Gyneceum	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgInPc2
plodolistů	plodolist	k1gInPc2
<g/>
,	,	kIx,
je	on	k3xPp3gInPc4
synkarpní	synkarpnit	k5eAaPmIp3nP
<g/>
,	,	kIx,
semeník	semeník	k1gInSc1
je	být	k5eAaImIp3nS
svrchní	svrchní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plodem	plod	k1gInSc7
je	být	k5eAaImIp3nS
tobolka	tobolka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tulipány	tulipán	k1gInPc7
se	se	k3xPyFc4
množí	množit	k5eAaImIp3nP
cibulkami	cibulka	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
oddělují	oddělovat	k5eAaImIp3nP
od	od	k7c2
matečné	matečný	k2eAgFnSc2d1
rostliny	rostlina	k1gFnSc2
(	(	kIx(
<g/>
na	na	k7c4
podzim	podzim	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dělení	dělení	k1gNnSc1
tulipánů	tulipán	k1gInPc2
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP
rané	raný	k2eAgMnPc4d1
<g/>
,	,	kIx,
středně	středně	k6eAd1
rané	raný	k2eAgFnSc2d1
a	a	k8xC
pozdní	pozdní	k2eAgFnSc2d1
odrůdy	odrůda	k1gFnSc2
tulipánů	tulipán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Darwinovy	Darwinův	k2eAgInPc1d1
hybridy	hybrid	k1gInPc1
mají	mít	k5eAaImIp3nP
pevné	pevný	k2eAgInPc4d1
stonky	stonek	k1gInPc4
<g/>
,	,	kIx,
proto	proto	k8xC
jsou	být	k5eAaImIp3nP
vhodné	vhodný	k2eAgFnPc1d1
k	k	k7c3
řezu	řez	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liliokvěté	Liliokvětý	k2eAgInPc1d1
tulipány	tulipán	k1gInPc1
dorůstají	dorůstat	k5eAaImIp3nP
výšky	výška	k1gFnPc1
50	#num#	k4
až	až	k9
70	#num#	k4
centimetrů	centimetr	k1gInPc2
a	a	k8xC
vyznačují	vyznačovat	k5eAaImIp3nP
se	s	k7c7
prohnutými	prohnutý	k2eAgInPc7d1
okvětními	okvětní	k2eAgInPc7d1
lístky	lístek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rembrandtovy	Rembrandtův	k2eAgInPc1d1
tulipány	tulipán	k1gInPc1
okouzlují	okouzlovat	k5eAaImIp3nP
pestře	pestro	k6eAd1
pruhovanými	pruhovaný	k2eAgInPc7d1
nebo	nebo	k8xC
skvrnitými	skvrnitý	k2eAgInPc7d1
květy	květ	k1gInPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
způsobeno	způsobit	k5eAaPmNgNnS
úmyslnou	úmyslný	k2eAgFnSc7d1
<g/>
,	,	kIx,
neškodnou	škodný	k2eNgFnSc7d1
virovou	virový	k2eAgFnSc7d1
infekcí	infekce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tulipány	tulipán	k1gInPc1
Viridiflora	Viridiflora	k1gFnSc1
mají	mít	k5eAaImIp3nP
bílé	bílý	k2eAgInPc1d1
<g/>
,	,	kIx,
žluté	žlutý	k2eAgInPc1d1
nebo	nebo	k8xC
červené	červený	k2eAgInPc4d1
květy	květ	k1gInPc4
se	s	k7c7
zelenými	zelený	k2eAgInPc7d1
proužky	proužek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neobvyklé	obvyklý	k2eNgInPc1d1
papouškovité	papouškovitý	k2eAgInPc1d1
tulipány	tulipán	k1gInPc1
jsou	být	k5eAaImIp3nP
hustě	hustě	k6eAd1
plnokvěté	plnokvětý	k2eAgFnPc1d1
a	a	k8xC
mají	mít	k5eAaImIp3nP
kadeřavé	kadeřavý	k2eAgInPc4d1
okvětní	okvětní	k2eAgInPc4d1
lístky	lístek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třepenité	třepenitý	k2eAgInPc1d1
tulipány	tulipán	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
bývají	bývat	k5eAaImIp3nP
většinou	většinou	k6eAd1
červené	červený	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
jemně	jemně	k6eAd1
roztřepenými	roztřepený	k2eAgInPc7d1
okraji	okraj	k1gInPc7
okvětních	okvětní	k2eAgInPc2d1
lístků	lístek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
tulipánů	tulipán	k1gInPc2
</s>
<s>
jednoduché	jednoduchý	k2eAgNnSc1d1
rané	raný	k2eAgNnSc1d1
</s>
<s>
jednoduché	jednoduchý	k2eAgFnPc1d1
pozdní	pozdní	k2eAgFnPc1d1
</s>
<s>
plné	plný	k2eAgFnPc1d1
rané	raný	k2eAgFnPc1d1
</s>
<s>
triumf	triumf	k1gInSc1
tulipány	tulipán	k1gInPc1
</s>
<s>
Darwin	Darwin	k1gMnSc1
hybridy	hybrida	k1gFnSc2
</s>
<s>
liliokvěté	liliokvětý	k2eAgNnSc1d1
</s>
<s>
třepenité	třepenitý	k2eAgNnSc1d1
</s>
<s>
papouškovité	papouškovitý	k2eAgNnSc1d1
</s>
<s>
Viridiflora	Viridiflora	k1gFnSc1
hybridy	hybrida	k1gFnSc2
</s>
<s>
pivoňkovité	pivoňkovitý	k2eAgNnSc1d1
</s>
<s>
vícekvěté	vícekvětý	k2eAgNnSc1d1
</s>
<s>
exotické	exotický	k2eAgNnSc4d1
</s>
<s>
botanické	botanický	k2eAgFnPc4d1
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
ve	v	k7c6
světě	svět	k1gInSc6
</s>
<s>
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
přibližně	přibližně	k6eAd1
100	#num#	k4
až	až	k9
150	#num#	k4
druhů	druh	k1gInPc2
tulipánů	tulipán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
výskytu	výskyt	k1gInSc2
tulipánů	tulipán	k1gInPc2
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
jižní	jižní	k2eAgInPc1d1
svahy	svah	k1gInPc1
Pamíru	Pamír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
se	se	k3xPyFc4
květina	květina	k1gFnSc1
rozšířila	rozšířit	k5eAaPmAgFnS
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
obchodními	obchodní	k2eAgFnPc7d1
karavanami	karavana	k1gFnPc7
<g/>
)	)	kIx)
do	do	k7c2
Turecka	Turecko	k1gNnSc2
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
Anatolie	Anatolie	k1gFnSc2
<g/>
,	,	kIx,
Íránu	Írán	k1gInSc2
a	a	k8xC
Japonska	Japonsko	k1gNnSc2
na	na	k7c6
východě	východ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
druhů	druh	k1gInPc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
pohoří	pohoří	k1gNnSc2
Pamír	Pamír	k1gInSc1
a	a	k8xC
Hindúkuš	Hindúkuš	k1gInSc1
a	a	k8xC
kazašských	kazašský	k2eAgFnPc2d1
stepí	step	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinde	jinde	k6eAd1
jsou	být	k5eAaImIp3nP
však	však	k9
hojně	hojně	k6eAd1
pěstovány	pěstován	k2eAgFnPc1d1
a	a	k8xC
občas	občas	k6eAd1
i	i	k9
zplaňují	zplaňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tulipány	tulipán	k1gInPc7
byly	být	k5eAaImAgInP
v	v	k7c6
oblibě	obliba	k1gFnSc6
u	u	k7c2
sultánů	sultán	k1gMnPc2
Mohameda	Mohamed	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1451	#num#	k4
<g/>
–	–	k?
<g/>
1481	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Sulejmana	Sulejman	k1gMnSc2
Velikého	veliký	k2eAgInSc2d1
(	(	kIx(
<g/>
1520	#num#	k4
<g/>
–	–	k?
<g/>
1566	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
V	v	k7c6
ČR	ČR	kA
není	být	k5eNaImIp3nS
původní	původní	k2eAgInSc4d1
žádný	žádný	k3yNgInSc4
druh	druh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tulipán	tulipán	k1gInSc1
planý	planý	k2eAgInSc1d1
(	(	kIx(
<g/>
Tulipa	Tulipa	k1gFnSc1
sylvestris	sylvestris	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
tulipán	tulipán	k1gInSc1
zahradní	zahradní	k2eAgInSc1d1
(	(	kIx(
<g/>
Tulipa	Tulipa	k1gFnSc1
×	×	k?
gesneriana	gesneriana	k1gFnSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
hojně	hojně	k6eAd1
pěstované	pěstovaný	k2eAgInPc1d1
druhy	druh	k1gInPc1
a	a	k8xC
v	v	k7c6
okolí	okolí	k1gNnSc6
lidských	lidský	k2eAgNnPc2d1
sídel	sídlo	k1gNnPc2
občas	občas	k6eAd1
i	i	k9
zplaňují	zplaňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Tulipány	tulipán	k1gInPc1
v	v	k7c6
zahradnictví	zahradnictví	k1gNnSc6
</s>
<s>
Tulipány	tulipán	k1gInPc1
se	se	k3xPyFc4
rozdělují	rozdělovat	k5eAaImIp3nP
na	na	k7c4
<g/>
:	:	kIx,
</s>
<s>
rané	raný	k2eAgFnSc2d1
odrůdy	odrůda	k1gFnSc2
–	–	k?
jednoduché	jednoduchý	k2eAgNnSc1d1
rané	raný	k2eAgInPc1d1
tulipány	tulipán	k1gInPc1
<g/>
,	,	kIx,
plné	plný	k2eAgInPc1d1
rané	raný	k2eAgInPc1d1
tulipány	tulipán	k1gInPc1
</s>
<s>
středně	středně	k6eAd1
rané	raný	k2eAgNnSc4d1
–	–	k?
Triumph	Triumph	k1gInSc4
tulipány	tulipán	k1gInPc1
</s>
<s>
pozdní	pozdní	k2eAgInSc1d1
–	–	k?
Darwinovy	Darwinův	k2eAgInPc1d1
tulipány	tulipán	k1gInPc1
<g/>
,	,	kIx,
liliokvěté	liliokvětý	k2eAgNnSc1d1
t.	t.	k?
<g/>
,	,	kIx,
jednoduché	jednoduchý	k2eAgNnSc4d1
pozdní	pozdní	k2eAgNnSc4d1
t.	t.	k?
<g/>
,	,	kIx,
Rembrandtovy	Rembrandtův	k2eAgFnSc2d1
t.	t.	k?
<g/>
,	,	kIx,
papouškovité	papouškovitý	k2eAgFnSc2d1
t.	t.	k?
<g/>
,	,	kIx,
plné	plný	k2eAgFnSc3d1
pozdní	pozdní	k2eAgFnSc3d1
(	(	kIx(
<g/>
pivoňkovité	pivoňkovitý	k2eAgFnSc3d1
<g/>
)	)	kIx)
t.	t.	k?
<g/>
,	,	kIx,
Tulipa	Tulip	k1gMnSc2
kaufmanniana	kaufmannian	k1gMnSc2
<g/>
,	,	kIx,
T.	T.	kA
fosteniana	fosteniana	k1gFnSc1
<g/>
,	,	kIx,
T.	T.	kA
greigii	greigie	k1gFnSc4
</s>
<s>
původní	původní	k2eAgInPc1d1
druhy	druh	k1gInPc1
–	–	k?
Tulipa	Tulip	k1gMnSc4
tarda	tard	k1gMnSc4
<g/>
,	,	kIx,
T.	T.	kA
prestans	prestans	k1gInSc1
<g/>
,	,	kIx,
T.	T.	kA
pulchella	pulchella	k6eAd1
</s>
<s>
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c4
skalky	skalka	k1gFnPc4
<g/>
,	,	kIx,
venkovní	venkovní	k2eAgInPc4d1
záhony	záhon	k1gInPc4
<g/>
,	,	kIx,
trávník	trávník	k1gInSc4
i	i	k9
k	k	k7c3
řezu	řez	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Zástupci	zástupce	k1gMnPc1
</s>
<s>
tulipán	tulipán	k1gInSc1
Greigův	Greigův	k2eAgInSc1d1
(	(	kIx(
<g/>
Tulipa	Tulipa	k1gFnSc1
greigii	greigium	k1gNnPc7
<g/>
)	)	kIx)
</s>
<s>
tulipán	tulipán	k1gInSc1
Kaufmannův	Kaufmannův	k2eAgInSc1d1
(	(	kIx(
<g/>
Tulipa	Tulipa	k1gFnSc1
kaufmannii	kaufmannium	k1gNnPc7
<g/>
)	)	kIx)
</s>
<s>
tulipán	tulipán	k1gInSc4
planý	planý	k2eAgInSc4d1
(	(	kIx(
<g/>
Tulipa	Tulipa	k1gFnSc1
sylvestris	sylvestris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
tulipán	tulipán	k1gMnSc1
lesní	lesní	k2eAgFnSc2d1
</s>
<s>
tulipán	tulipán	k1gInSc1
pozdní	pozdní	k2eAgInSc1d1
(	(	kIx(
<g/>
Tulipa	Tulipa	k1gFnSc1
tarda	tarda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
tulipán	tulipán	k1gMnSc1
turkestánský	turkestánský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Tulipa	Tulip	k1gMnSc4
turkestanica	turkestanicus	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
tulipán	tulipán	k1gInSc1
zahradní	zahradní	k2eAgInSc1d1
(	(	kIx(
<g/>
Tulipa	Tulipa	k1gFnSc1
×	×	k?
gesneriana	gesneriana	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Původ	původ	k1gInSc1
tulipánů	tulipán	k1gInPc2
</s>
<s>
O	o	k7c4
původ	původ	k1gInSc4
tulipánu	tulipán	k1gInSc2
se	se	k3xPyFc4
přeli	přít	k5eAaImAgMnP
Osmané	Osman	k1gMnPc1
a	a	k8xC
Peršané	Peršan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršané	Peršan	k1gMnPc1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
Turci	Turek	k1gMnPc1
jim	on	k3xPp3gMnPc3
tulipány	tulipán	k1gMnPc4
sebrali	sebrat	k5eAaPmAgMnP
při	při	k7c6
výpravách	výprava	k1gFnPc6
<g/>
,	,	kIx,
Turci	Turek	k1gMnPc1
naopak	naopak	k6eAd1
tvrdí	tvrdit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
nic	nic	k3yNnSc4
neukradli	ukradnout	k5eNaPmAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
na	na	k7c4
jejich	jejich	k3xOp3gNnSc4
území	území	k1gNnSc4
tulipány	tulipán	k1gInPc1
rostly	růst	k5eAaImAgInP
už	už	k6eAd1
od	od	k7c2
nepaměti	nepaměť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obou	dva	k4xCgInPc6
státech	stát	k1gInPc6
se	se	k3xPyFc4
velice	velice	k6eAd1
přísně	přísně	k6eAd1
kontroloval	kontrolovat	k5eAaImAgMnS
vývoz	vývoz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peršané	Peršan	k1gMnPc1
vedli	vést	k5eAaImAgMnP
přesné	přesný	k2eAgInPc4d1
záznamy	záznam	k1gInPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
uznávali	uznávat	k5eAaImAgMnP
1588	#num#	k4
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
moderních	moderní	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
odlišovali	odlišovat	k5eAaImAgMnP
druhy	druh	k1gInPc4
pouze	pouze	k6eAd1
podle	podle	k7c2
barvy	barva	k1gFnSc2
a	a	k8xC
velikosti	velikost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tulipány	tulipán	k1gInPc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
V	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byly	být	k5eAaImAgInP
tulipány	tulipán	k1gInPc1
velmi	velmi	k6eAd1
populární	populární	k2eAgInPc1d1
jak	jak	k8xS,k8xC
v	v	k7c6
Osmanské	osmanský	k2eAgFnSc6d1
<g/>
,	,	kIx,
tak	tak	k9
v	v	k7c6
Perské	perský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Turecku	Turecko	k1gNnSc6
dokonce	dokonce	k9
odlišujeme	odlišovat	k5eAaImIp1nP
takzvané	takzvaný	k2eAgNnSc4d1
tulipánové	tulipánový	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
končilo	končit	k5eAaImAgNnS
až	až	k9
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
knize	kniha	k1gFnSc6
Tulipán	tulipán	k1gInSc4
z	z	k7c2
Istanbulu	Istanbul	k1gInSc2
nalezneme	nalézt	k5eAaBmIp1nP,k5eAaPmIp1nP
informaci	informace	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
pěstitelé	pěstitel	k1gMnPc1
květin	květina	k1gFnPc2
z	z	k7c2
Istanbulu	Istanbul	k1gInSc2
dělali	dělat	k5eAaImAgMnP
nové	nový	k2eAgFnPc4d1
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
barevné	barevný	k2eAgFnSc2d1
odrůdy	odrůda	k1gFnSc2
<g/>
,	,	kIx,
různými	různý	k2eAgInPc7d1
bizarními	bizarní	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgInPc6
státech	stát	k1gInPc6
zdobily	zdobit	k5eAaImAgInP
tulipány	tulipán	k1gInPc1
i	i	k8xC
paláce	palác	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
Selim	Selim	k?
II	II	kA
<g/>
.	.	kIx.
nechal	nechat	k5eAaPmAgMnS
do	do	k7c2
paláce	palác	k1gInSc2
Topkapi	Topkap	k1gFnSc2
přivézt	přivézt	k5eAaPmF
z	z	k7c2
Krymu	Krym	k1gInSc2
na	na	k7c4
300	#num#	k4
tisíc	tisíc	k4xCgInSc4
cibulek	cibulka	k1gFnPc2
<g/>
.	.	kIx.
nedaleko	nedaleko	k7c2
města	město	k1gNnSc2
Manisa	Manis	k1gMnSc2
pak	pak	k6eAd1
nechal	nechat	k5eAaPmAgMnS
sultán	sultán	k1gMnSc1
Ahmed	Ahmed	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
vysadit	vysadit	k5eAaPmF
obrovské	obrovský	k2eAgFnPc4d1
tulipánové	tulipánový	k2eAgFnPc4d1
zahrady	zahrada	k1gFnPc4
<g/>
,	,	kIx,
o	o	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
se	se	k3xPyFc4
staralo	starat	k5eAaImAgNnS
přes	přes	k7c4
900	#num#	k4
zahradníků	zahradník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tulipány	tulipán	k1gInPc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Již	již	k6eAd1
roku	rok	k1gInSc2
1554	#num#	k4
zaslal	zaslat	k5eAaPmAgMnS
rakousko-uherský	rakousko-uherský	k2eAgMnSc1d1
velvyslanec	velvyslanec	k1gMnSc1
na	na	k7c6
dvoře	dvůr	k1gInSc6
sultána	sultán	k1gMnSc2
Sulejmana	Sulejman	k1gMnSc2
Nádherného	nádherný	k2eAgMnSc2d1
několik	několik	k4yIc1
cibulek	cibulka	k1gFnPc2
do	do	k7c2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
jiných	jiný	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
se	se	k3xPyFc4
však	však	k9
v	v	k7c6
polovině	polovina	k1gFnSc6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
objevily	objevit	k5eAaPmAgInP
nejprve	nejprve	k6eAd1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
v	v	k7c6
Jelením	jelení	k2eAgInSc6d1
příkopu	příkop	k1gInSc6
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
hradě	hrad	k1gInSc6
<g/>
)	)	kIx)
a	a	k8xC
až	až	k9
odtud	odtud	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
se	se	k3xPyFc4
dostaly	dostat	k5eAaPmAgFnP
do	do	k7c2
Vídně	Vídeň	k1gFnSc2
a	a	k8xC
díky	díky	k7c3
botanikovi	botanik	k1gMnSc3
Charlesi	Charles	k1gMnSc3
de	de	k?
L	L	kA
<g/>
’	’	k?
<g/>
Eclusi	Ecluse	k1gFnSc6
do	do	k7c2
nizozemského	nizozemský	k2eAgInSc2d1
Leidenu	Leiden	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
zasazeny	zasadit	k5eAaPmNgFnP
do	do	k7c2
tamní	tamní	k2eAgFnSc2d1
nově	nově	k6eAd1
založené	založený	k2eAgFnSc2d1
univerzitní	univerzitní	k2eAgFnSc2d1
zahrady	zahrada	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
pak	pak	k6eAd1
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
standardní	standardní	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
velvyslanci	velvyslanec	k1gMnPc1
dostávali	dostávat	k5eAaImAgMnP
cibulky	cibulka	k1gFnPc4
tulipánů	tulipán	k1gInPc2
jako	jako	k8xS,k8xC
dary	dar	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tulipány	tulipán	k1gInPc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
dosáhly	dosáhnout	k5eAaPmAgFnP
ohromné	ohromný	k2eAgFnPc4d1
popularity	popularita	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrady	zahrada	k1gFnPc1
s	s	k7c7
tulipány	tulipán	k1gMnPc7
byly	být	k5eAaImAgFnP
často	často	k6eAd1
vykrádány	vykrádán	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
tulipánu	tulipán	k1gInSc2
stala	stát	k5eAaPmAgFnS
komodita	komodita	k1gFnSc1
<g/>
,	,	kIx,
neboť	neboť	k8xC
cena	cena	k1gFnSc1
cibulky	cibulka	k1gFnSc2
tulipánu	tulipán	k1gInSc2
se	se	k3xPyFc4
vyšplhala	vyšplhat	k5eAaPmAgFnS
až	až	k9
na	na	k7c4
5	#num#	k4
500	#num#	k4
florinů	florin	k1gInPc2
(	(	kIx(
<g/>
100	#num#	k4
litrů	litr	k1gInPc2
piva	pivo	k1gNnSc2
stálo	stát	k5eAaImAgNnS
8	#num#	k4
florintů	florint	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgInSc6
ohromném	ohromný	k2eAgInSc6d1
boomu	boom	k1gInSc6
pak	pak	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
poklesu	pokles	k1gInSc3
ceny	cena	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
zákazníci	zákazník	k1gMnPc1
odmítali	odmítat	k5eAaImAgMnP
platit	platit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
tulipán	tulipán	k1gInSc1
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
tureckého	turecký	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
tülbend	tülbenda	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c6
překladu	překlad	k1gInSc6
znamená	znamenat	k5eAaImIp3nS
turban	turban	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
legendy	legenda	k1gFnSc2
dostal	dostat	k5eAaPmAgMnS
jeden	jeden	k4xCgInSc4
mladík	mladík	k1gInSc4
od	od	k7c2
dívky	dívka	k1gFnSc2
tulipán	tulipán	k1gMnSc1
a	a	k8xC
vložil	vložit	k5eAaPmAgMnS
si	se	k3xPyFc3
ho	on	k3xPp3gNnSc4
do	do	k7c2
turbanu	turban	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
zamířil	zamířit	k5eAaPmAgMnS
do	do	k7c2
čajovny	čajovna	k1gFnSc2
nedaleko	daleko	k6eNd1
od	od	k7c2
Modré	modrý	k2eAgFnSc2d1
mešity	mešita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
seděl	sedět	k5eAaImAgMnS
neznámý	známý	k2eNgMnSc1d1
Evropan	Evropan	k1gMnSc1
a	a	k8xC
zeptal	zeptat	k5eAaPmAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
hlavě	hlava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mladý	mladý	k1gMnSc1
muž	muž	k1gMnSc1
mu	on	k3xPp3gMnSc3
odpověděl	odpovědět	k5eAaPmAgMnS
tülbend	tülbend	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
zapomněl	zapomenout	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
v	v	k7c6
turbanu	turban	k1gInSc6
tulipán	tulipán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
pojmenování	pojmenování	k1gNnSc1
tulipánu	tulipán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInPc1d1
etymologické	etymologický	k2eAgInPc1d1
slovníky	slovník	k1gInPc1
však	však	k9
uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c6
přenesení	přenesení	k1gNnSc6
významu	význam	k1gInSc2
na	na	k7c6
základě	základ	k1gInSc6
podobnosti	podobnost	k1gFnSc2
smotaného	smotaný	k2eAgInSc2d1
turbanu	turban	k1gInSc2
s	s	k7c7
tulipánem	tulipán	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
agenensis	agenensis	k1gFnSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
aucheriana	aucheriana	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
australis	australis	k1gFnSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
bakeri	baker	k1gFnSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
butkovii	butkovie	k1gFnSc4
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
celsiana	celsiana	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
clusiana	clusiana	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
cretica	cretic	k1gInSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
florenskyi	florensky	k1gFnSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
fosteriana	fosteriana	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
gesneriana	gesneriana	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
goulimyi	goulimy	k1gFnSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
greigii	greigie	k1gFnSc4
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
humilis	humilis	k1gFnSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
kaufmanniana	kaufmanniana	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
kuschkensis	kuschkensis	k1gFnSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
linifolia	linifolia	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
orphanidea	orphanide	k1gInSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
platystigma	platystigma	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
polychroma	polychroma	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
praestans	praestans	k6eAd1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
scabriscapa	scabriscapa	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
systola	systola	k1gFnSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
sylvestris	sylvestris	k1gFnSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
sylvestrissubsp	sylvestrissubsp	k1gInSc1
<g/>
.	.	kIx.
australis	australis	k1gInSc1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
taihangshanica	taihangshanic	k1gInSc2
</s>
<s>
Tulipán	tulipán	k1gInSc1
pozdní	pozdní	k2eAgFnSc2d1
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
tschimganica	tschimganic	k1gInSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
turkestanica	turkestanic	k1gInSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
urumiensis	urumiensis	k1gFnSc2
</s>
<s>
Tulipa	Tulipa	k1gFnSc1
vvedenskyi	vvedensky	k1gFnSc2
</s>
<s>
Zámecký	zámecký	k2eAgInSc1d1
park	park	k1gInSc1
Lednice-tulipán	Lednice-tulipán	k2eAgInSc1d1
se	s	k7c7
třemi	tři	k4xCgInPc7
květy	květ	k1gInPc7
rok	rok	k1gInSc1
2016	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
HRABALOVÁ	Hrabalová	k1gFnSc1
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Květina	květina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
dobyla	dobýt	k5eAaPmAgFnS
svět	svět	k1gInSc4
<g/>
.	.	kIx.
100	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
44	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
TOMÁŠOVÁ	TOMÁŠOVÁ	kA
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tulipány	tulipán	k1gInPc4
<g/>
:	:	kIx,
historie	historie	k1gFnSc1
a	a	k8xC
pěstování	pěstování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abeceda	abeceda	k1gFnSc1
zahrady	zahrada	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-02-04	2014-02-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
FOKTOVÁ	FOKTOVÁ	kA
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
<g/>
;	;	kIx,
FOKT	FOKT	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Symbol	symbol	k1gInSc1
Nizozemska	Nizozemsko	k1gNnSc2
přicestoval	přicestovat	k5eAaPmAgInS
z	z	k7c2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
a	a	k8xC
Země	zem	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-01-05	2011-01-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Klíč	klíč	k1gInSc1
ke	k	k7c3
Květeně	květena	k1gFnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kubát	Kubát	k1gMnSc1
K.	K.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
836	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Květena	květena	k1gFnSc1
ČSSR	ČSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostál	Dostál	k1gMnSc1
J.	J.	kA
Praha	Praha	k1gFnSc1
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
95	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
kultivarů	kultivar	k1gInPc2
rodu	rod	k1gInSc2
tulipán	tulipán	k1gInSc1
</s>
<s>
Bylina	bylina	k1gFnSc1
</s>
<s>
Biologie	biologie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
tulipán	tulipán	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Tulipa	Tulip	k1gMnSc2
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
tulipán	tulipán	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Flóra	Flóra	k1gFnSc1
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
</s>
<s>
Flóra	Flóra	k1gFnSc1
Číny	Čína	k1gFnSc2
</s>
<s>
Galerie	galerie	k1gFnSc1
fotografií	fotografia	k1gFnPc2
tulipánů	tulipán	k1gInPc2
mnoha	mnoho	k4c2
odrůd	odrůda	k1gFnPc2
</s>
<s>
Tulipány	tulipán	k1gInPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
hlíznaté	hlíznatý	k2eAgFnPc1d1
okrasné	okrasný	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnPc1
|	|	kIx~
Rostliny	rostlina	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4186422-0	4186422-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85138521	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85138521	#num#	k4
</s>
