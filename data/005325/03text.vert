<s>
Nitra	Nitra	k1gFnSc1	Nitra
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Nyitra	Nyitra	k1gFnSc1	Nyitra
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Neutra	neutrum	k1gNnSc2	neutrum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
krajské	krajský	k2eAgNnSc1d1	krajské
a	a	k8xC	a
okresní	okresní	k2eAgNnSc1d1	okresní
město	město	k1gNnSc1	město
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
na	na	k7c6	na
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
řece	řeka	k1gFnSc6	řeka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
78	[number]	k4	78
033	[number]	k4	033
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pátým	pátý	k4xOgNnSc7	pátý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nitra	Nitra	k1gFnSc1	Nitra
byla	být	k5eAaImAgFnS	být
centrem	centrum	k1gNnSc7	centrum
nitranského	nitranský	k2eAgNnSc2d1	Nitranské
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
knížete	kníže	k1gNnSc4wR	kníže
Pribiny	Pribina	k1gFnSc2	Pribina
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
velkomoravské	velkomoravský	k2eAgFnSc3d1	Velkomoravská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
město	město	k1gNnSc4	město
s	s	k7c7	s
nitranským	nitranský	k2eAgInSc7d1	nitranský
hradem	hrad	k1gInSc7	hrad
udrželo	udržet	k5eAaPmAgNnS	udržet
význam	význam	k1gInSc4	význam
důležitého	důležitý	k2eAgNnSc2d1	důležité
správního	správní	k2eAgNnSc2d1	správní
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Nitry	Nitra	k1gFnSc2	Nitra
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
osídlena	osídlit	k5eAaPmNgFnS	osídlit
Kelty	Kelt	k1gMnPc7	Kelt
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Germány	Germán	k1gMnPc4	Germán
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
Slovany	Slovan	k1gMnPc7	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházela	nacházet	k5eAaImAgFnS	nacházet
bohatě	bohatě	k6eAd1	bohatě
vybavená	vybavený	k2eAgNnPc4d1	vybavené
pohřebiště	pohřebiště	k1gNnPc4	pohřebiště
<g/>
,	,	kIx,	,
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pak	pak	k6eAd1	pak
staromaďarské	staromaďarský	k2eAgInPc1d1	staromaďarský
hroby	hrob	k1gInPc1	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Archeologický	archeologický	k2eAgInSc1d1	archeologický
průzkum	průzkum	k1gInSc1	průzkum
doložil	doložit	k5eAaPmAgInS	doložit
existenci	existence	k1gFnSc4	existence
několika	několik	k4yIc2	několik
románských	románský	k2eAgFnPc2d1	románská
církevních	církevní	k2eAgFnPc2d1	církevní
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
sídlil	sídlit	k5eAaImAgInS	sídlit
kníže	kníže	k1gMnSc1	kníže
Pribina	Pribina	k1gFnSc1	Pribina
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1018	[number]	k4	1018
ho	on	k3xPp3gInSc4	on
drželi	držet	k5eAaImAgMnP	držet
Arpádovci	Arpádovec	k1gMnPc1	Arpádovec
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1083	[number]	k4	1083
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
biskupství	biskupství	k1gNnSc1	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1248	[number]	k4	1248
se	se	k3xPyFc4	se
Nitra	Nitra	k1gFnSc1	Nitra
stala	stát	k5eAaPmAgFnS	stát
královským	královský	k2eAgNnSc7d1	královské
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
o	o	k7c4	o
čtyřicet	čtyřicet	k4xCc4	čtyřicet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
král	král	k1gMnSc1	král
město	město	k1gNnSc4	město
i	i	k8xC	i
hrad	hrad	k1gInSc4	hrad
daroval	darovat	k5eAaPmAgMnS	darovat
nitranským	nitranský	k2eAgMnSc7d1	nitranský
biskupům	biskup	k1gMnPc3	biskup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1633	[number]	k4	1633
-	-	kIx~	-
34	[number]	k4	34
ji	on	k3xPp3gFnSc4	on
okupovali	okupovat	k5eAaBmAgMnP	okupovat
Turci	Turek	k1gMnPc1	Turek
při	při	k7c6	při
svých	svůj	k3xOyFgInPc6	svůj
výbojích	výboj	k1gInPc6	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
Nitranský	nitranský	k2eAgInSc4d1	nitranský
hrad	hrad	k1gInSc4	hrad
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přestavěný	přestavěný	k2eAgMnSc1d1	přestavěný
a	a	k8xC	a
upravovaný	upravovaný	k2eAgMnSc1d1	upravovaný
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Akropole	Akropole	k1gFnSc1	Akropole
nitranská	nitranský	k2eAgFnSc1d1	Nitranská
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
sídlo	sídlo	k1gNnSc1	sídlo
knížete	kníže	k1gMnSc2	kníže
Pribiny	Pribina	k1gFnSc2	Pribina
a	a	k8xC	a
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
králem	král	k1gMnSc7	král
a	a	k8xC	a
vládcem	vládce	k1gMnSc7	vládce
celé	celý	k2eAgFnSc2d1	celá
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgInSc1d1	bývalý
Pribinův	Pribinův	k2eAgInSc1d1	Pribinův
hrad	hrad	k1gInSc1	hrad
<g/>
,	,	kIx,	,
v	v	k7c6	v
soudobých	soudobý	k2eAgInPc6d1	soudobý
pramenech	pramen	k1gInPc6	pramen
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Nitrava	Nitrava	k1gFnSc1	Nitrava
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
období	období	k1gNnSc6	období
sídlem	sídlo	k1gNnSc7	sídlo
údělného	údělný	k2eAgNnSc2d1	údělné
knížectví	knížectví	k1gNnSc2	knížectví
příslušníků	příslušník	k1gMnPc2	příslušník
dynastie	dynastie	k1gFnSc2	dynastie
Mojmírovců	Mojmírovec	k1gMnPc2	Mojmírovec
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
výzkumů	výzkum	k1gInPc2	výzkum
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
hledání	hledání	k1gNnSc2	hledání
zbytků	zbytek	k1gInPc2	zbytek
kostela	kostel	k1gInSc2	kostel
vysvěceného	vysvěcený	k2eAgInSc2d1	vysvěcený
solnohradským	solnohradský	k2eAgInSc7d1	solnohradský
biskupem	biskup	k1gInSc7	biskup
Adalramem	Adalram	k1gInSc7	Adalram
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pod	pod	k7c4	pod
základy	základ	k1gInPc4	základ
mladšího	mladý	k2eAgInSc2d2	mladší
románského	románský	k2eAgInSc2d1	románský
chrámu	chrám	k1gInSc2	chrám
sv.	sv.	kA	sv.
Emmerama	Emmerama	k1gFnSc1	Emmerama
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
dodatečně	dodatečně	k6eAd1	dodatečně
rozšířeným	rozšířený	k2eAgNnSc7d1	rozšířené
patrociniem	patrocinium	k1gNnSc7	patrocinium
i	i	k8xC	i
sv.	sv.	kA	sv.
Benedikta	Benedikt	k1gMnSc4	Benedikt
a	a	k8xC	a
sv.	sv.	kA	sv.
Svorada	Svorada	k1gFnSc1	Svorada
<g/>
.	.	kIx.	.
</s>
<s>
Obrat	obrat	k1gInSc1	obrat
v	v	k7c6	v
dosavadních	dosavadní	k2eAgInPc6d1	dosavadní
názorech	názor	k1gInPc6	názor
historiků	historik	k1gMnPc2	historik
na	na	k7c6	na
Nitru	nitro	k1gNnSc6	nitro
přinesly	přinést	k5eAaPmAgFnP	přinést
výkopové	výkopový	k2eAgFnPc1d1	výkopová
práce	práce	k1gFnPc1	práce
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
akademika	akademik	k1gMnSc2	akademik
Böhma	Böhm	k1gMnSc2	Böhm
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
románský	románský	k2eAgInSc1d1	románský
kostel	kostel	k1gInSc1	kostel
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
pokračovatelem	pokračovatel	k1gMnSc7	pokračovatel
kostela	kostel	k1gInSc2	kostel
knížete	kníže	k1gMnSc2	kníže
Pribiny	Pribina	k1gFnSc2	Pribina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gInPc1	jeho
základy	základ	k1gInPc1	základ
překrývají	překrývat	k5eAaImIp3nP	překrývat
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Počátek	počátek	k1gInSc1	počátek
systematických	systematický	k2eAgInPc2d1	systematický
výzkumů	výzkum	k1gInPc2	výzkum
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Chropovského	Chropovský	k1gMnSc2	Chropovský
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
velkomoravský	velkomoravský	k2eAgInSc1d1	velkomoravský
inventář	inventář	k1gInSc1	inventář
se	se	k3xPyFc4	se
našel	najít	k5eAaPmAgInS	najít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Ladislava	Ladislav	k1gMnSc2	Ladislav
až	až	k9	až
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Pribinovo	Pribinův	k2eAgNnSc1d1	Pribinovo
hradiště	hradiště	k1gNnSc1	hradiště
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nacházelo	nacházet	k5eAaImAgNnS	nacházet
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
středověkého	středověký	k2eAgInSc2d1	středověký
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
výzkumy	výzkum	k1gInPc1	výzkum
vede	vést	k5eAaImIp3nS	vést
Gabriel	Gabriel	k1gMnSc1	Gabriel
Fusek	Fusek	k1gMnSc1	Fusek
<g/>
.	.	kIx.	.
</s>
<s>
Nitranské	nitranský	k2eAgNnSc1d1	Nitranské
velkomoravské	velkomoravský	k2eAgNnSc1d1	velkomoravské
osídlení	osídlení	k1gNnSc1	osídlení
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
shlukem	shluk	k1gInSc7	shluk
ostrožních	ostrožní	k2eAgNnPc2d1	ostrožní
hradisek	hradisko	k1gNnPc2	hradisko
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
obklopovala	obklopovat	k5eAaImAgNnP	obklopovat
centrální	centrální	k2eAgNnPc1d1	centrální
ústředí	ústředí	k1gNnPc1	ústředí
na	na	k7c4	na
návrší	návrší	k1gNnPc4	návrší
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
nitranského	nitranský	k2eAgInSc2d1	nitranský
středověkého	středověký	k2eAgInSc2d1	středověký
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
část	část	k1gFnSc1	část
s	s	k7c7	s
pravděpodobným	pravděpodobný	k2eAgNnSc7d1	pravděpodobné
tržištěm	tržiště	k1gNnSc7	tržiště
<g/>
,	,	kIx,	,
zděnou	zděný	k2eAgFnSc7d1	zděná
stavbou	stavba	k1gFnSc7	stavba
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
palácového	palácový	k2eAgInSc2d1	palácový
typu	typ	k1gInSc2	typ
a	a	k8xC	a
kostelem	kostel	k1gInSc7	kostel
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
chránilo	chránit	k5eAaImAgNnS	chránit
opevnění	opevnění	k1gNnSc1	opevnění
tvořené	tvořený	k2eAgNnSc1d1	tvořené
dvojitým	dvojitý	k2eAgInSc7d1	dvojitý
zemním	zemní	k2eAgInSc7d1	zemní
valem	val	k1gInSc7	val
s	s	k7c7	s
palisádou	palisáda	k1gFnSc7	palisáda
a	a	k8xC	a
příkopem	příkop	k1gInSc7	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hradní	hradní	k2eAgFnSc6d1	hradní
výšině	výšina	k1gFnSc6	výšina
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
zatím	zatím	k6eAd1	zatím
jen	jen	k9	jen
sporadické	sporadický	k2eAgNnSc4d1	sporadické
osídlení	osídlení	k1gNnSc4	osídlení
<g/>
.	.	kIx.	.
</s>
<s>
Důstojnost	důstojnost	k1gFnSc4	důstojnost
sídla	sídlo	k1gNnSc2	sídlo
dotvářel	dotvářet	k5eAaImAgInS	dotvářet
obranný	obranný	k2eAgInSc1d1	obranný
celek	celek	k1gInSc1	celek
opevněných	opevněný	k2eAgNnPc2d1	opevněné
hradisek	hradisko	k1gNnPc2	hradisko
důležitých	důležitý	k2eAgMnPc2d1	důležitý
zejména	zejména	k6eAd1	zejména
s	s	k7c7	s
vojensko-strategického	vojenskotrategický	k2eAgNnSc2d1	vojensko-strategické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
kontrolujících	kontrolující	k2eAgMnPc6d1	kontrolující
přístupové	přístupový	k2eAgFnPc4d1	přístupová
cesty	cesta	k1gFnPc4	cesta
k	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
aglomerace	aglomerace	k1gFnSc2	aglomerace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Martinském	martinský	k2eAgInSc6d1	martinský
vrchu	vrch	k1gInSc6	vrch
je	být	k5eAaImIp3nS	být
doložen	doložen	k2eAgInSc4d1	doložen
kostel	kostel	k1gInSc4	kostel
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
pohřebiště	pohřebiště	k1gNnPc1	pohřebiště
a	a	k8xC	a
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
výroba	výroba	k1gFnSc1	výroba
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
osadách	osada	k1gFnPc6	osada
mimo	mimo	k7c4	mimo
obvod	obvod	k1gInSc4	obvod
valů	val	k1gInPc2	val
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
straně	strana	k1gFnSc6	strana
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
sídliště	sídliště	k1gNnSc4	sídliště
výrazně	výrazně	k6eAd1	výrazně
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
<g/>
,	,	kIx,	,
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
bydleli	bydlet	k5eAaImAgMnP	bydlet
hutníci	hutník	k1gMnPc1	hutník
a	a	k8xC	a
skláři	sklář	k1gMnPc1	sklář
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
kováři	kovář	k1gMnPc1	kovář
<g/>
,	,	kIx,	,
kameníci	kameník	k1gMnPc1	kameník
<g/>
,	,	kIx,	,
kosťaři	kosťař	k1gMnPc1	kosťař
a	a	k8xC	a
šperkaři	šperkař	k1gMnPc1	šperkař
<g/>
.	.	kIx.	.
</s>
<s>
Specializované	specializovaný	k2eAgNnSc1d1	specializované
hrnčířství	hrnčířství	k1gNnSc1	hrnčířství
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
hradisko	hradisko	k1gNnSc4	hradisko
na	na	k7c4	na
Lupce	Lupce	k1gFnPc4	Lupce
<g/>
,	,	kIx,	,
sklářství	sklářství	k1gNnPc4	sklářství
zase	zase	k9	zase
pro	pro	k7c4	pro
hradisko	hradisko	k1gNnSc4	hradisko
Borina	Borino	k1gNnSc2	Borino
na	na	k7c6	na
Šibeničním	šibeniční	k2eAgInSc6d1	šibeniční
vrchu	vrch	k1gInSc6	vrch
i	i	k9	i
pro	pro	k7c4	pro
hradisko	hradisko	k1gNnSc4	hradisko
na	na	k7c6	na
Zoboru	Zobor	k1gInSc6	Zobor
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
názvu	název	k1gInSc3	název
se	se	k3xPyFc4	se
o	o	k7c6	o
lokaci	lokace	k1gFnSc6	lokace
Nitry	Nitra	k1gFnSc2	Nitra
nikdy	nikdy	k6eAd1	nikdy
nepochybovalo	pochybovat	k5eNaImAgNnS	pochybovat
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
ibn	ibn	k?	ibn
Rustova	Rustův	k2eAgFnSc1d1	Rustova
Kniha	kniha	k1gFnSc1	kniha
vzácných	vzácný	k2eAgInPc2d1	vzácný
drahokamů	drahokam	k1gInPc2	drahokam
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
dvě	dva	k4xCgNnPc4	dva
města	město	k1gNnPc4	město
-	-	kIx~	-
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
grad	grad	k1gInSc1	grad
<g/>
)	)	kIx)	)
Nít	Nít	k1gFnSc1	Nít
(	(	kIx(	(
<g/>
Nitra	Nitra	k1gFnSc1	Nitra
<g/>
)	)	kIx)	)
a	a	k8xC	a
Velké	velký	k2eAgFnPc1d1	velká
(	(	kIx(	(
<g/>
vele-	vele-	k?	vele-
<g/>
)	)	kIx)	)
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
grad	grad	k1gInSc1	grad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tedy	tedy	k9	tedy
Nitru	Nitra	k1gFnSc4	Nitra
a	a	k8xC	a
Velehrad	Velehrad	k1gInSc4	Velehrad
<g/>
,	,	kIx,	,
rovnítko	rovnítko	k1gNnSc4	rovnítko
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
můžeme	moct	k5eAaImIp1nP	moct
prakticky	prakticky	k6eAd1	prakticky
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Nitra	Nitra	k1gFnSc1	Nitra
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
celého	celý	k2eAgInSc2d1	celý
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
důležitých	důležitý	k2eAgFnPc2d1	důležitá
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
jako	jako	k9	jako
např.	např.	kA	např.
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Poľnohospodárska	Poľnohospodárska	k1gFnSc1	Poľnohospodárska
Univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Konštantína	Konštantín	k1gMnSc2	Konštantín
Filozofa	filozof	k1gMnSc2	filozof
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Andreja	Andreja	k?	Andreja
Bagara	Bagara	k1gFnSc1	Bagara
<g/>
,	,	kIx,	,
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
poľnohospodárske	poľnohospodársk	k1gFnSc2	poľnohospodársk
múzeum	múzeum	k1gInSc4	múzeum
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc4d1	různý
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
ústavy	ústav	k1gInPc4	ústav
a	a	k8xC	a
výstavní	výstavní	k2eAgInSc4d1	výstavní
areál	areál	k1gInSc4	areál
Agrokomplex	agrokomplex	k1gInSc1	agrokomplex
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupeno	zastoupen	k2eAgNnSc1d1	zastoupeno
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
odvětví	odvětví	k1gNnPc2	odvětví
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
ale	ale	k8xC	ale
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
zemědělskému	zemědělský	k2eAgInSc3d1	zemědělský
charakteru	charakter	k1gInSc3	charakter
okolní	okolní	k2eAgFnSc2d1	okolní
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Nitranský	nitranský	k2eAgInSc1d1	nitranský
hrad	hrad	k1gInSc1	hrad
–	–	k?	–
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
bazilika	bazilika	k1gFnSc1	bazilika
sv.	sv.	kA	sv.
Jimrama	Jimrama	k1gFnSc1	Jimrama
–	–	k?	–
původně	původně	k6eAd1	původně
románský	románský	k2eAgInSc1d1	románský
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přestavován	přestavován	k2eAgMnSc1d1	přestavován
v	v	k7c6	v
období	období	k1gNnSc6	období
gotiky	gotika	k1gFnSc2	gotika
a	a	k8xC	a
baroka	baroko	k1gNnSc2	baroko
horní	horní	k2eAgInSc4d1	horní
biskupský	biskupský	k2eAgInSc4d1	biskupský
katedrální	katedrální	k2eAgInSc4d1	katedrální
kostel	kostel	k1gInSc4	kostel
–	–	k?	–
gotický	gotický	k2eAgMnSc1d1	gotický
<g/>
,	,	kIx,	,
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
hradní	hradní	k2eAgFnSc2d1	hradní
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
raně	raně	k6eAd1	raně
barokně	barokně	k6eAd1	barokně
upraven	upravit	k5eAaPmNgInS	upravit
a	a	k8xC	a
spojen	spojen	k2eAgInSc1d1	spojen
širokým	široký	k2eAgNnSc7d1	široké
schodištěm	schodiště	k1gNnSc7	schodiště
s	s	k7c7	s
dolním	dolní	k2eAgInSc7d1	dolní
kostelem	kostel	k1gInSc7	kostel
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
upraven	upravit	k5eAaPmNgInS	upravit
vrcholně	vrcholně	k6eAd1	vrcholně
<g />
.	.	kIx.	.
</s>
<s>
barokně	barokně	k6eAd1	barokně
(	(	kIx(	(
<g/>
Domenico	Domenico	k6eAd1	Domenico
Martinelli	Martinelle	k1gFnSc4	Martinelle
<g/>
)	)	kIx)	)
dolní	dolní	k2eAgInSc1d1	dolní
kostel	kostel	k1gInSc1	kostel
–	–	k?	–
raně	raně	k6eAd1	raně
barokní	barokní	k2eAgMnSc1d1	barokní
<g/>
,	,	kIx,	,
mramorový	mramorový	k2eAgInSc1d1	mramorový
oltář	oltář	k1gInSc1	oltář
biskupský	biskupský	k2eAgInSc1d1	biskupský
palác	palác	k1gInSc1	palác
–	–	k?	–
vrcholně	vrcholně	k6eAd1	vrcholně
barokní	barokní	k2eAgInPc1d1	barokní
na	na	k7c6	na
gotických	gotický	k2eAgInPc6d1	gotický
základech	základ	k1gInPc6	základ
Horní	horní	k2eAgFnSc1d1	horní
Město	město	k1gNnSc1	město
kostel	kostel	k1gInSc4	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
františkánský	františkánský	k2eAgInSc1d1	františkánský
klášter	klášter	k1gInSc1	klášter
–	–	k?	–
barokní	barokní	k2eAgInSc1d1	barokní
<g/>
,	,	kIx,	,
na	na	k7c6	na
kostele	kostel	k1gInSc6	kostel
reliéfy	reliéf	k1gInPc1	reliéf
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měšťanské	měšťanský	k2eAgInPc1d1	měšťanský
a	a	k8xC	a
kanovnické	kanovnický	k2eAgInPc1d1	kanovnický
domy	dům	k1gInPc1	dům
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Malý	malý	k2eAgMnSc1d1	malý
a	a	k8xC	a
Velký	velký	k2eAgInSc1d1	velký
seminář	seminář	k1gInSc1	seminář
–	–	k?	–
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Pro	pro	k7c4	pro
meritae	meritae	k1gFnSc4	meritae
quieti	quiet	k1gMnPc1	quiet
–	–	k?	–
empírová	empírový	k2eAgFnSc1d1	empírová
budova	budova	k1gFnSc1	budova
Dolní	dolní	k2eAgNnSc1d1	dolní
Město	město	k1gNnSc4	město
Piaristický	piaristický	k2eAgInSc1d1	piaristický
klášter	klášter	k1gInSc1	klášter
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Ladislava	Ladislav	k1gMnSc2	Ladislav
–	–	k?	–
barokní	barokní	k2eAgInSc4d1	barokní
komplex	komplex	k1gInSc4	komplex
klášter	klášter	k1gInSc1	klášter
vincentek	vincentka	k1gFnPc2	vincentka
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
–	–	k?	–
novorománská	novorománský	k2eAgFnSc1d1	novorománská
bazilika	bazilika	k1gFnSc1	bazilika
kostel	kostel	k1gInSc4	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c4	na
Kalvárii	Kalvárie	k1gFnSc4	Kalvárie
–	–	k?	–
poutní	poutní	k2eAgFnSc4d1	poutní
<g/>
,	,	kIx,	,
s	s	k7c7	s
klášterem	klášter	k1gInSc7	klášter
nazarénů	nazarén	k1gMnPc2	nazarén
<g/>
,	,	kIx,	,
křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
kaplí	kaple	k1gFnSc7	kaple
sv.	sv.	kA	sv.
Kříže	Kříž	k1gMnSc2	Kříž
Synagoga	synagoga	k1gFnSc1	synagoga
-	-	kIx~	-
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
eklektický	eklektický	k2eAgInSc4d1	eklektický
sloh	sloh	k1gInSc4	sloh
mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
–	–	k?	–
sloup	sloup	k1gInSc1	sloup
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
od	od	k7c2	od
Martina	Martin	k1gMnSc2	Martin
Vogerleho	Vogerle	k1gMnSc2	Vogerle
–	–	k?	–
součást	součást	k1gFnSc1	součást
prostoru	prostor	k1gInSc2	prostor
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
parkově	parkově	k6eAd1	parkově
upraven	upraven	k2eAgInSc1d1	upraven
a	a	k8xC	a
sochařsky	sochařsky	k6eAd1	sochařsky
<g />
.	.	kIx.	.
</s>
<s>
vyzdoben	vyzdoben	k2eAgMnSc1d1	vyzdoben
ve	v	k7c4	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Moderní	moderní	k2eAgFnSc2d1	moderní
památky	památka	k1gFnSc2	památka
Dělnický	dělnický	k2eAgInSc1d1	dělnický
kulturní	kulturní	k2eAgInSc1d1	kulturní
dům	dům	k1gInSc1	dům
–	–	k?	–
funkcionalistický	funkcionalistický	k2eAgInSc1d1	funkcionalistický
univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
kampus	kampus	k1gInSc1	kampus
–	–	k?	–
oceněné	oceněný	k2eAgNnSc4d1	oceněné
řešení	řešení	k1gNnSc4	řešení
areálu	areál	k1gInSc2	areál
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
ze	z	k7c2	z
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
<s>
V	v	k7c6	v
Nitře	Nitra	k1gFnSc6	Nitra
je	být	k5eAaImIp3nS	být
provozovaná	provozovaný	k2eAgFnSc1d1	provozovaná
městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
společností	společnost	k1gFnSc7	společnost
Arriva	Arrivo	k1gNnPc1	Arrivo
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
ambice	ambice	k1gFnPc4	ambice
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
plánu	plán	k1gInSc2	plán
upustilo	upustit	k5eAaPmAgNnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
92	[number]	k4	92
autobusů	autobus	k1gInPc2	autobus
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
typ	typ	k1gInSc4	typ
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
731	[number]	k4	731
a	a	k8xC	a
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
732	[number]	k4	732
a	a	k8xC	a
kloubové	kloubový	k2eAgInPc4d1	kloubový
vozy	vůz	k1gInPc4	vůz
Ikarus	Ikarus	k1gMnSc1	Ikarus
280	[number]	k4	280
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
jejich	jejich	k3xOp3gNnSc4	jejich
vyřazení	vyřazení	k1gNnSc4	vyřazení
zejména	zejména	k9	zejména
autobusy	autobus	k1gInPc1	autobus
SOR	SOR	kA	SOR
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Van	van	k1gInSc1	van
Hool	Hoola	k1gFnPc2	Hoola
AG	AG	kA	AG
300	[number]	k4	300
(	(	kIx(	(
<g/>
13	[number]	k4	13
vozů	vůz	k1gInPc2	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mercedes-Benz	Mercedes-Benz	k1gMnSc1	Mercedes-Benz
(	(	kIx(	(
<g/>
8	[number]	k4	8
vozů	vůz	k1gInPc2	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Scania	Scanium	k1gNnSc2	Scanium
OmniLink	OmniLink	k1gInSc1	OmniLink
(	(	kIx(	(
<g/>
3	[number]	k4	3
vozy	vůz	k1gInPc4	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Solaris	Solaris	k1gFnSc1	Solaris
Urbino	Urbino	k1gNnSc1	Urbino
12	[number]	k4	12
(	(	kIx(	(
<g/>
2	[number]	k4	2
vozy	vůz	k1gInPc4	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
952	[number]	k4	952
(	(	kIx(	(
<g/>
6	[number]	k4	6
vozů	vůz	k1gInPc2	vůz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
961	[number]	k4	961
<g/>
,	,	kIx,	,
Karosa	karosa	k1gFnSc1	karosa
C	C	kA	C
943	[number]	k4	943
(	(	kIx(	(
<g/>
obě	dva	k4xCgNnPc4	dva
jsou	být	k5eAaImIp3nP	být
kloubové	kloubový	k2eAgInPc4d1	kloubový
záložní	záložní	k2eAgInPc4d1	záložní
vozy	vůz	k1gInPc4	vůz
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
kloubových	kloubový	k2eAgNnPc2d1	kloubové
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
pravidelně	pravidelně	k6eAd1	pravidelně
i	i	k9	i
s	s	k7c7	s
vlastními	vlastní	k2eAgMnPc7d1	vlastní
řidiči	řidič	k1gMnPc7	řidič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karosa	karosa	k1gFnSc1	karosa
C	C	kA	C
934	[number]	k4	934
(	(	kIx(	(
<g/>
1	[number]	k4	1
voz	vozit	k5eAaImRp2nS	vozit
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
na	na	k7c4	na
specifický	specifický	k2eAgInSc4d1	specifický
turnus	turnus	k1gInSc4	turnus
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
svoz	svoz	k1gInSc4	svoz
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
Arrivy	Arriva	k1gFnSc2	Arriva
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
Irisbus	Irisbus	k1gInSc4	Irisbus
Crossway	Crosswaa	k1gFnSc2	Crosswaa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
26	[number]	k4	26
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
sezonní	sezonní	k2eAgInSc1d1	sezonní
cyklobus	cyklobus	k1gInSc1	cyklobus
do	do	k7c2	do
městského	městský	k2eAgInSc2d1	městský
parku	park	k1gInSc2	park
(	(	kIx(	(
<g/>
1	[number]	k4	1
speciálně	speciálně	k6eAd1	speciálně
upravená	upravený	k2eAgFnSc1d1	upravená
Karosa	karosa	k1gFnSc1	karosa
B	B	kA	B
732	[number]	k4	732
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgFnPc1d1	noční
linky	linka	k1gFnPc1	linka
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Nitra	Nitra	k1gFnSc1	Nitra
je	být	k5eAaImIp3nS	být
napojená	napojený	k2eAgFnSc1d1	napojená
na	na	k7c4	na
rychlostní	rychlostní	k2eAgFnSc4d1	rychlostní
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
Bratislavou	Bratislava	k1gFnSc7	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
přes	přes	k7c4	přes
město	město	k1gNnSc4	město
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
za	za	k7c4	za
Rakouska-Uherska	Rakouska-Uhersek	k1gMnSc4	Rakouska-Uhersek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
směřována	směřován	k2eAgFnSc1d1	směřována
od	od	k7c2	od
Budapešti	Budapešť	k1gFnSc2	Budapešť
údolím	údolí	k1gNnSc7	údolí
řeky	řeka	k1gFnSc2	řeka
Nitra	nitro	k1gNnSc2	nitro
do	do	k7c2	do
Prievidze	Prievidza	k1gFnSc2	Prievidza
a	a	k8xC	a
Nitrianskeho	Nitrianske	k1gMnSc2	Nitrianske
Pravna	Pravno	k1gNnSc2	Pravno
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vybudována	vybudován	k2eAgFnSc1d1	vybudována
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Leopoldova	Leopoldův	k2eAgNnSc2d1	Leopoldovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
trať	trať	k1gFnSc4	trať
Žilina	Žilina	k1gFnSc1	Žilina
-	-	kIx~	-
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
nebyla	být	k5eNaImAgFnS	být
elektrifikována	elektrifikován	k2eAgFnSc1d1	elektrifikována
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
brání	bránit	k5eAaImIp3nS	bránit
větším	veliký	k2eAgFnPc3d2	veliký
rychlostem	rychlost	k1gFnPc3	rychlost
vlaků	vlak	k1gInPc2	vlak
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgNnPc7	tento
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
denně	denně	k6eAd1	denně
jezdí	jezdit	k5eAaImIp3nS	jezdit
železniční	železniční	k2eAgMnSc1d1	železniční
dopravce	dopravce	k1gMnSc1	dopravce
Arriva	Arrivo	k1gNnSc2	Arrivo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
linky	linka	k1gFnSc2	linka
<g/>
:	:	kIx,	:
Praha-Olomouc-Trenčín-Nitra	Praha-Olomouc-Trenčín-Nitra	k1gFnSc1	Praha-Olomouc-Trenčín-Nitra
<g/>
.	.	kIx.	.
</s>
<s>
ČFK	ČFK	kA	ČFK
Nitra	Nitra	k1gFnSc1	Nitra
-	-	kIx~	-
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
FC	FC	kA	FC
Nitra	Nitra	k1gFnSc1	Nitra
-	-	kIx~	-
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
HBK	HBK	kA	HBK
Nitrianski	Nitriansk	k1gFnSc2	Nitriansk
rytieri	rytier	k1gFnSc2	rytier
Nitra	Nitra	k1gFnSc1	Nitra
-	-	kIx~	-
hokejbalový	hokejbalový	k2eAgInSc1d1	hokejbalový
klub	klub	k1gInSc1	klub
HK	HK	kA	HK
Nitra	Nitra	k1gFnSc1	Nitra
-	-	kIx~	-
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
Jozef	Jozef	k1gMnSc1	Jozef
Stümpel	Stümpel	k1gMnSc1	Stümpel
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
hokejista	hokejista	k1gMnSc1	hokejista
Jozef	Jozef	k1gMnSc1	Jozef
Dóczy	Dócza	k1gFnSc2	Dócza
–	–	k?	–
herec	herec	k1gMnSc1	herec
Ľubomír	Ľubomíra	k1gFnPc2	Ľubomíra
Kolník	kolník	k1gInSc4	kolník
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
hokejista	hokejista	k1gMnSc1	hokejista
Ľubomír	Ľubomír	k1gMnSc1	Ľubomír
Moravčík	Moravčík	k1gMnSc1	Moravčík
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
fotbalista	fotbalista	k1gMnSc1	fotbalista
Tibor	Tibor	k1gMnSc1	Tibor
Bártfay	Bártfaa	k1gFnSc2	Bártfaa
-	-	kIx~	-
umělec	umělec	k1gMnSc1	umělec
Tomáš	Tomáš	k1gMnSc1	Tomáš
Babiak	Babiak	k1gMnSc1	Babiak
-	-	kIx~	-
atlet	atlet	k1gMnSc1	atlet
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
oštěpař	oštěpař	k1gMnSc1	oštěpař
<g/>
)	)	kIx)	)
Zuzana	Zuzana	k1gFnSc1	Zuzana
Moravčíková	Moravčíková	k1gFnSc1	Moravčíková
-	-	kIx~	-
atletka	atletka	k1gFnSc1	atletka
(	(	kIx(	(
<g/>
běžkyně	běžkyně	k1gFnSc1	běžkyně
<g/>
)	)	kIx)	)
Daniela	Daniela	k1gFnSc1	Daniela
Šinkorová	Šinkorová	k1gFnSc1	Šinkorová
-	-	kIx~	-
herečka	herečka	k1gFnSc1	herečka
Bački	Bačk	k1gFnSc2	Bačk
Petrovac	Petrovac	k1gFnSc1	Petrovac
<g/>
,	,	kIx,	,
Srbsko	Srbsko	k1gNnSc1	Srbsko
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc4	Česko
Spišská	spišský	k2eAgFnSc1d1	Spišská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Osijek	Osijka	k1gFnPc2	Osijka
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc4	Chorvatsko
Zielona	Zielon	k1gMnSc2	Zielon
Góra	Górus	k1gMnSc2	Górus
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Gosford	Gosford	k1gInSc1	Gosford
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Jižní	jižní	k2eAgInSc1d1	jižní
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
Naperville	Naperville	k1gFnSc1	Naperville
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Zoetermeer	Zoetermeer	k1gInSc1	Zoetermeer
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
</s>
