<p>
<s>
Slunečný	slunečný	k2eAgInSc1d1	slunečný
vršek	vršek	k1gInSc1	vršek
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc1d1	nový
sídelní	sídelní	k2eAgInSc1d1	sídelní
útvar	útvar	k1gInSc1	útvar
na	na	k7c6	na
území	území	k1gNnSc6	území
Hostivaře	Hostivař	k1gFnSc2	Hostivař
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Praha	Praha	k1gFnSc1	Praha
15	[number]	k4	15
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
ulicemi	ulice	k1gFnPc7	ulice
K	k	k7c3	k
Horkám	horka	k1gFnPc3	horka
<g/>
,	,	kIx,	,
Doupovská	Doupovský	k2eAgFnSc1d1	Doupovská
a	a	k8xC	a
Přeštická	přeštický	k2eAgFnSc1d1	Přeštická
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
270	[number]	k4	270
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
sídliště	sídliště	k1gNnSc2	sídliště
Košík	košík	k1gInSc1	košík
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
funkční	funkční	k2eAgInSc1d1	funkční
celek	celek	k1gInSc1	celek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
14	[number]	k4	14
bloků	blok	k1gInPc2	blok
<g/>
,	,	kIx,	,
označených	označený	k2eAgInPc2d1	označený
A	a	k8xC	a
<g/>
–	–	k?	–
<g/>
N	N	kA	N
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
asi	asi	k9	asi
1300	[number]	k4	1300
bytových	bytový	k2eAgFnPc2d1	bytová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jejich	jejich	k3xOp3gFnSc2	jejich
výstavby	výstavba	k1gFnSc2	výstavba
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc1d1	nová
ulice	ulice	k1gFnPc1	ulice
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
<g/>
,	,	kIx,	,
Bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
<g/>
,	,	kIx,	,
Rižská	rižský	k2eAgFnSc1d1	Rižská
<g/>
,	,	kIx,	,
Budapešťská	budapešťský	k2eAgFnSc1d1	Budapešťská
a	a	k8xC	a
Athénská	athénský	k2eAgFnSc1d1	Athénská
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
náměstím	náměstí	k1gNnSc7	náměstí
Přátelství	přátelství	k1gNnSc2	přátelství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Hostivařský	hostivařský	k2eAgInSc1d1	hostivařský
lesopark	lesopark	k1gInSc1	lesopark
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
a	a	k8xC	a
také	také	k9	také
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Meandry	meandr	k1gInPc4	meandr
Botiče	Botič	k1gInSc2	Botič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
je	být	k5eAaImIp3nS	být
napojeno	napojen	k2eAgNnSc1d1	napojeno
autobusy	autobus	k1gInPc1	autobus
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
především	především	k9	především
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
metra	metro	k1gNnSc2	metro
Skalka	skalka	k1gFnSc1	skalka
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
A	A	kA	A
a	a	k8xC	a
stanici	stanice	k1gFnSc6	stanice
Opatov	Opatov	k1gInSc1	Opatov
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
C	C	kA	C
<g/>
;	;	kIx,	;
existuje	existovat	k5eAaImIp3nS	existovat
ale	ale	k9	ale
i	i	k9	i
přímé	přímý	k2eAgNnSc4d1	přímé
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
hostivařsko-malešickou	hostivařskoalešický	k2eAgFnSc7d1	hostivařsko-malešický
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
oblastí	oblast	k1gFnSc7	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
Košík	košík	k1gInSc1	košík
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Slunečný	slunečný	k2eAgInSc4d1	slunečný
vršek	vršek	k1gInSc4	vršek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Sídliště	sídliště	k1gNnSc1	sídliště
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
developera	developer	k1gMnSc2	developer
</s>
</p>
