<s>
Slunečný	slunečný	k2eAgInSc1d1	slunečný
vršek	vršek	k1gInSc1	vršek
je	být	k5eAaImIp3nS	být
nový	nový	k2eAgInSc1d1	nový
sídelní	sídelní	k2eAgInSc1d1	sídelní
útvar	útvar	k1gInSc1	útvar
na	na	k7c6	na
území	území	k1gNnSc6	území
Hostivaře	Hostivař	k1gFnSc2	Hostivař
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Praha	Praha	k1gFnSc1	Praha
15	[number]	k4	15
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
ulicemi	ulice	k1gFnPc7	ulice
K	k	k7c3	k
Horkám	horka	k1gFnPc3	horka
<g/>
,	,	kIx,	,
Doupovská	Doupovský	k2eAgFnSc1d1	Doupovská
a	a	k8xC	a
Přeštická	přeštický	k2eAgFnSc1d1	Přeštická
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
asi	asi	k9	asi
270	[number]	k4	270
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
sídliště	sídliště	k1gNnSc2	sídliště
Košík	košík	k1gInSc1	košík
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
funkční	funkční	k2eAgInSc1d1	funkční
celek	celek	k1gInSc1	celek
<g/>
.	.	kIx.	.
</s>
