<s>
Uran	Uran	k1gInSc1	Uran
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Uranus	Uranus	k1gInSc1	Uranus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sedmá	sedmý	k4xOgFnSc1	sedmý
planeta	planeta	k1gFnSc1	planeta
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
planeta	planeta	k1gFnSc1	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
plynné	plynný	k2eAgMnPc4d1	plynný
obry	obr	k1gMnPc4	obr
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Neptunem	Neptun	k1gInSc7	Neptun
i	i	k8xC	i
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
ledové	ledový	k2eAgFnPc4d1	ledová
obry	obr	k1gMnPc7	obr
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
řeckém	řecký	k2eAgMnSc6d1	řecký
bohu	bůh	k1gMnSc6	bůh
Úranovi	Úran	k1gMnSc6	Úran
<g/>
,	,	kIx,	,
bohu	bůh	k1gMnSc6	bůh
nebes	nebesa	k1gNnPc2	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Symboly	symbol	k1gInPc1	symbol
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gInSc1	Uran
jsou	být	k5eAaImIp3nP	být
znak	znak	k1gInSc4	znak
♅	♅	k?	♅
(	(	kIx(	(
<g/>
užívaný	užívaný	k2eAgInSc1d1	užívaný
v	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
užívaný	užívaný	k2eAgInSc1d1	užívaný
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
Uran	Uran	k1gInSc4	Uran
za	za	k7c2	za
příznivých	příznivý	k2eAgFnPc2d1	příznivá
podmínek	podmínka	k1gFnPc2	podmínka
pozorovat	pozorovat	k5eAaImF	pozorovat
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
antickými	antický	k2eAgFnPc7d1	antická
astronomy	astronom	k1gMnPc4	astronom
rozpoznán	rozpoznán	k2eAgInSc4d1	rozpoznán
jako	jako	k8xC	jako
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
hvězdu	hvězda	k1gFnSc4	hvězda
kvůli	kvůli	k7c3	kvůli
pomalé	pomalý	k2eAgFnSc3d1	pomalá
rychlosti	rychlost	k1gFnSc3	rychlost
a	a	k8xC	a
slabé	slabý	k2eAgFnSc3d1	slabá
záři	zář	k1gFnSc3	zář
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
Uranu	Uran	k1gInSc2	Uran
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
William	William	k1gInSc1	William
Herschel	Herschel	k1gInSc1	Herschel
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1781	[number]	k4	1781
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
posunul	posunout	k5eAaPmAgInS	posunout
známé	známý	k2eAgFnPc4d1	známá
hranice	hranice	k1gFnPc4	hranice
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Chemickým	chemický	k2eAgNnSc7d1	chemické
složením	složení	k1gNnSc7	složení
se	se	k3xPyFc4	se
Uran	Uran	k1gInSc1	Uran
podobá	podobat	k5eAaImIp3nS	podobat
Neptunu	Neptun	k1gMnSc3	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
planety	planeta	k1gFnPc1	planeta
mají	mít	k5eAaImIp3nP	mít
rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
zastoupení	zastoupení	k1gNnSc1	zastoupení
plynů	plyn	k1gInPc2	plyn
oproti	oproti	k7c3	oproti
Jupiteru	Jupiter	k1gInSc3	Jupiter
či	či	k8xC	či
Saturnu	Saturn	k1gInSc3	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
atmosféra	atmosféra	k1gFnSc1	atmosféra
Uranu	Uran	k1gInSc2	Uran
složením	složení	k1gNnSc7	složení
podobná	podobný	k2eAgNnPc4d1	podobné
atmosféře	atmosféra	k1gFnSc6	atmosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
či	či	k8xC	či
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
převážně	převážně	k6eAd1	převážně
plynné	plynný	k2eAgFnPc4d1	plynná
formy	forma	k1gFnPc4	forma
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
výrazný	výrazný	k2eAgInSc1d1	výrazný
podíl	podíl	k1gInSc1	podíl
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
čpavku	čpavek	k1gInSc2	čpavek
či	či	k8xC	či
metanu	metan	k1gInSc2	metan
se	s	k7c7	s
stopami	stopa	k1gFnPc7	stopa
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
nejchladnější	chladný	k2eAgFnSc7d3	nejchladnější
atmosférou	atmosféra	k1gFnSc7	atmosféra
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgFnPc1d1	minimální
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
-224	-224	k4	-224
°	°	k?	°
<g/>
C	C	kA	C
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
vrstevnatá	vrstevnatý	k2eAgFnSc1d1	vrstevnatá
<g/>
:	:	kIx,	:
v	v	k7c6	v
nejnižších	nízký	k2eAgNnPc6d3	nejnižší
patrech	patro	k1gNnPc6	patro
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mraky	mrak	k1gInPc1	mrak
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svrchních	svrchní	k2eAgNnPc6d1	svrchní
patrech	patro	k1gNnPc6	patro
mraky	mrak	k1gInPc1	mrak
tvořené	tvořený	k2eAgInPc1d1	tvořený
především	především	k6eAd1	především
metanem	metan	k1gInSc7	metan
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
složena	složit	k5eAaPmNgFnS	složit
především	především	k9	především
z	z	k7c2	z
ledu	led	k1gInSc2	led
a	a	k8xC	a
kamení	kamení	k1gNnSc2	kamení
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
další	další	k2eAgFnPc4d1	další
plynné	plynný	k2eAgFnPc4d1	plynná
planety	planeta	k1gFnPc4	planeta
má	mít	k5eAaImIp3nS	mít
i	i	k8xC	i
Uran	Uran	k1gMnSc1	Uran
planetární	planetární	k2eAgInPc4d1	planetární
prstence	prstenec	k1gInPc4	prstenec
<g/>
,	,	kIx,	,
magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
a	a	k8xC	a
obíhá	obíhat	k5eAaImIp3nS	obíhat
ho	on	k3xPp3gMnSc4	on
řada	řada	k1gFnSc1	řada
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
sklon	sklon	k1gInSc1	sklon
jeho	jeho	k3xOp3gFnSc2	jeho
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
<g/>
:	:	kIx,	:
osa	osa	k1gFnSc1	osa
leží	ležet	k5eAaImIp3nS	ležet
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgMnPc4	který
planeta	planeta	k1gFnSc1	planeta
obíhá	obíhat	k5eAaImIp3nS	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
planet	planeta	k1gFnPc2	planeta
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
pro	pro	k7c4	pro
rovník	rovník	k1gInSc4	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
občas	občas	k6eAd1	občas
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
prstence	prstenec	k1gInPc1	prstenec
Uranu	Uran	k1gInSc2	Uran
jeví	jevit	k5eAaImIp3nP	jevit
jako	jako	k9	jako
terč	terč	k1gInSc4	terč
s	s	k7c7	s
Uranem	Uran	k1gInSc7	Uran
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
kolem	kolem	k7c2	kolem
Uranu	Uran	k1gInSc2	Uran
proletěla	proletět	k5eAaPmAgFnS	proletět
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
nepozorovala	pozorovat	k5eNaImAgFnS	pozorovat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
žádné	žádný	k3yNgNnSc4	žádný
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
mračen	mračno	k1gNnPc2	mračno
a	a	k8xC	a
bouřkových	bouřkový	k2eAgInPc2d1	bouřkový
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
jiné	jiný	k2eAgMnPc4d1	jiný
plynné	plynný	k2eAgMnPc4d1	plynný
obry	obr	k1gMnPc4	obr
<g/>
.	.	kIx.	.
</s>
<s>
Pozemská	pozemský	k2eAgNnPc1d1	pozemské
pozorování	pozorování	k1gNnPc1	pozorování
však	však	k9	však
přinesla	přinést	k5eAaPmAgNnP	přinést
náznaky	náznak	k1gInPc1	náznak
sezónních	sezónní	k2eAgFnPc2d1	sezónní
změn	změna	k1gFnPc2	změna
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
souvisí	souviset	k5eAaImIp3nP	souviset
i	i	k9	i
větry	vítr	k1gInPc1	vítr
vanoucí	vanoucí	k2eAgInPc1d1	vanoucí
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
rychlosti	rychlost	k1gFnSc3	rychlost
až	až	k9	až
900	[number]	k4	900
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Uran	Uran	k1gInSc1	Uran
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
stejným	stejný	k2eAgInSc7d1	stejný
procesem	proces	k1gInSc7	proces
jako	jako	k8xS	jako
Jupiter	Jupiter	k1gMnSc1	Jupiter
z	z	k7c2	z
protoplanetárního	protoplanetární	k2eAgInSc2d1	protoplanetární
disku	disk	k1gInSc2	disk
před	před	k7c7	před
4,6	[number]	k4	4,6
až	až	k9	až
4,7	[number]	k4	4,7
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
mohly	moct	k5eAaImAgFnP	moct
velké	velký	k2eAgFnPc4d1	velká
plynné	plynný	k2eAgFnPc4d1	plynná
planety	planeta	k1gFnPc4	planeta
vzniknout	vzniknout	k5eAaPmF	vzniknout
a	a	k8xC	a
zformovat	zformovat	k5eAaPmF	zformovat
se	se	k3xPyFc4	se
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
<g/>
:	:	kIx,	:
teorie	teorie	k1gFnSc1	teorie
akrece	akrece	k1gFnSc1	akrece
a	a	k8xC	a
teorie	teorie	k1gFnSc1	teorie
gravitačního	gravitační	k2eAgInSc2d1	gravitační
kolapsu	kolaps	k1gInSc2	kolaps
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
akrece	akrece	k1gFnSc2	akrece
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
protoplanetárním	protoplanetární	k2eAgInSc6d1	protoplanetární
disku	disk	k1gInSc6	disk
postupně	postupně	k6eAd1	postupně
slepovaly	slepovat	k5eAaImAgFnP	slepovat
drobné	drobný	k2eAgFnPc1d1	drobná
prachové	prachový	k2eAgFnPc1d1	prachová
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
větší	veliký	k2eAgFnPc1d2	veliký
částice	částice	k1gFnPc1	částice
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
balvany	balvan	k1gInPc4	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Neustálé	neustálý	k2eAgFnPc4d1	neustálá
srážky	srážka	k1gFnPc4	srážka
těles	těleso	k1gNnPc2	těleso
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
narůstání	narůstání	k1gNnSc1	narůstání
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
tělesa	těleso	k1gNnPc1	těleso
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
velká	velký	k2eAgNnPc1d1	velké
železokamenitá	železokamenitý	k2eAgNnPc1d1	železokamenitý
tělesa	těleso	k1gNnPc1	těleso
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zárodky	zárodek	k1gInPc7	zárodek
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobná	podobný	k2eAgNnPc1d1	podobné
tělesa	těleso	k1gNnPc1	těleso
mohla	moct	k5eAaImAgNnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
ve	v	k7c6	v
vzdálenějších	vzdálený	k2eAgFnPc6d2	vzdálenější
oblastech	oblast	k1gFnPc6	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vlivem	vliv	k1gInSc7	vliv
velké	velký	k2eAgFnSc2d1	velká
gravitace	gravitace	k1gFnSc2	gravitace
začala	začít	k5eAaPmAgFnS	začít
strhávat	strhávat	k5eAaImF	strhávat
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začal	začít	k5eAaPmAgInS	začít
nabalovat	nabalovat	k5eAaImF	nabalovat
na	na	k7c4	na
pevné	pevný	k2eAgNnSc4d1	pevné
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
planeta	planeta	k1gFnSc1	planeta
dorostla	dorůst	k5eAaPmAgFnS	dorůst
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
gravitačního	gravitační	k2eAgInSc2d1	gravitační
kolapsu	kolaps	k1gInSc2	kolaps
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgFnPc1d1	velká
planety	planeta	k1gFnPc1	planeta
nevznikaly	vznikat	k5eNaImAgFnP	vznikat
postupným	postupný	k2eAgNnSc7d1	postupné
slepováním	slepování	k1gNnSc7	slepování
drobných	drobný	k2eAgFnPc2d1	drobná
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
rychlým	rychlý	k2eAgNnSc7d1	rychlé
smrštěním	smrštění	k1gNnSc7	smrštění
z	z	k7c2	z
nahuštěného	nahuštěný	k2eAgInSc2d1	nahuštěný
shluku	shluk	k1gInSc2	shluk
v	v	k7c6	v
zárodečném	zárodečný	k2eAgNnSc6d1	zárodečné
disku	disco	k1gNnSc6	disco
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
několika	několik	k4yIc2	několik
gravitačních	gravitační	k2eAgInPc2d1	gravitační
kolapsů	kolaps	k1gInPc2	kolaps
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Alan	Alan	k1gMnSc1	Alan
Boss	boss	k1gMnSc1	boss
z	z	k7c2	z
Carnegie	Carnegie	k1gFnSc2	Carnegie
Institution	Institution	k1gInSc1	Institution
of	of	k?	of
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vznik	vznik	k1gInSc1	vznik
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
krátký	krátký	k2eAgInSc4d1	krátký
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Uranu	Uran	k1gInSc2	Uran
trval	trvat	k5eAaImAgInS	trvat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Uran	Uran	k1gInSc1	Uran
nevznikl	vzniknout	k5eNaPmAgInS	vzniknout
na	na	k7c6	na
současném	současný	k2eAgNnSc6d1	současné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
zřejmě	zřejmě	k6eAd1	zřejmě
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
formování	formování	k1gNnSc2	formování
planet	planeta	k1gFnPc2	planeta
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
zárodečného	zárodečný	k2eAgInSc2d1	zárodečný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
zrod	zrod	k1gInSc1	zrod
(	(	kIx(	(
<g/>
ať	ať	k8xS	ať
akrecí	akrece	k1gFnSc7	akrece
nebo	nebo	k8xC	nebo
gravitačním	gravitační	k2eAgInSc7d1	gravitační
kolapsem	kolaps	k1gInSc7	kolaps
<g/>
)	)	kIx)	)
proto	proto	k8xC	proto
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
blíže	blízce	k6eAd2	blízce
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
Uran	Uran	k1gInSc1	Uran
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
migroval	migrovat	k5eAaImAgMnS	migrovat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
současné	současný	k2eAgFnSc2d1	současná
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
jen	jen	k9	jen
83	[number]	k4	83
%	%	kIx~	%
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
15	[number]	k4	15
%	%	kIx~	%
helia	helium	k1gNnPc4	helium
a	a	k8xC	a
stopová	stopový	k2eAgNnPc4d1	stopové
množství	množství	k1gNnPc4	množství
metanu	metan	k1gInSc2	metan
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gInSc1	Saturn
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Jádra	jádro	k1gNnPc1	jádro
Uranu	Uran	k1gInSc2	Uran
a	a	k8xC	a
Neptunu	Neptun	k1gInSc2	Neptun
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
směrech	směr	k1gInPc6	směr
podobají	podobat	k5eAaImIp3nP	podobat
jádrům	jádro	k1gNnPc3	jádro
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
však	však	k9	však
masívní	masívní	k2eAgFnSc4d1	masívní
obálku	obálka	k1gFnSc4	obálka
tekutého	tekutý	k2eAgInSc2d1	tekutý
kovového	kovový	k2eAgInSc2d1	kovový
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Uran	Uran	k1gInSc1	Uran
nemá	mít	k5eNaImIp3nS	mít
výrazně	výrazně	k6eAd1	výrazně
diferencované	diferencovaný	k2eAgNnSc1d1	diferencované
kamenné	kamenný	k2eAgNnSc1d1	kamenné
jádro	jádro	k1gNnSc1	jádro
jako	jako	k8xS	jako
Jupiter	Jupiter	k1gMnSc1	Jupiter
a	a	k8xC	a
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
materiál	materiál	k1gInSc1	materiál
je	být	k5eAaImIp3nS	být
víceméně	víceméně	k9	víceméně
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
rozložen	rozložen	k2eAgMnSc1d1	rozložen
<g/>
.	.	kIx.	.
</s>
<s>
Uranova	Uranův	k2eAgFnSc1d1	Uranova
modrozelená	modrozelený	k2eAgFnSc1d1	modrozelená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
absorpcí	absorpce	k1gFnSc7	absorpce
červeného	červený	k2eAgNnSc2d1	červené
světla	světlo	k1gNnSc2	světlo
jeho	jeho	k3xOp3gFnSc4	jeho
metanovou	metanová	k1gFnSc4	metanová
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
14,5	[number]	k4	14,5
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgFnSc1d2	hmotnější
než	než	k8xS	než
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
nejlehčí	lehký	k2eAgMnSc1d3	nejlehčí
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
je	být	k5eAaImIp3nS	být
1,27	[number]	k4	1,27
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejmenší	malý	k2eAgFnSc1d3	nejmenší
hodnota	hodnota	k1gFnSc1	hodnota
z	z	k7c2	z
planet	planeta	k1gFnPc2	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
po	po	k7c6	po
Saturnu	Saturn	k1gInSc6	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
málo	málo	k6eAd1	málo
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
průměr	průměr	k1gInSc4	průměr
Neptunu	Neptun	k1gInSc2	Neptun
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
průměr	průměr	k1gInSc4	průměr
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Uran	Uran	k1gInSc1	Uran
je	být	k5eAaImIp3nS	být
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
menší	malý	k2eAgMnSc1d2	menší
Neptun	Neptun	k1gMnSc1	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Nízká	nízký	k2eAgFnSc1d1	nízká
hustota	hustota	k1gFnSc1	hustota
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
složená	složený	k2eAgFnSc1d1	složená
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
lehkých	lehký	k2eAgInPc2d1	lehký
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
čpavku	čpavek	k1gInSc2	čpavek
a	a	k8xC	a
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
ledu	led	k1gInSc2	led
obsaženého	obsažený	k2eAgInSc2d1	obsažený
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
stavbě	stavba	k1gFnSc6	stavba
Uranu	Uran	k1gInSc2	Uran
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známa	znám	k2eAgFnSc1d1	známa
a	a	k8xC	a
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
použitého	použitý	k2eAgInSc2d1	použitý
modelu	model	k1gInSc2	model
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
mezi	mezi	k7c7	mezi
9,3	[number]	k4	9,3
až	až	k9	až
13,5	[number]	k4	13,5
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vodík	vodík	k1gInSc1	vodík
a	a	k8xC	a
hélium	hélium	k1gNnSc1	hélium
tvoří	tvořit	k5eAaImIp3nS	tvořit
pouze	pouze	k6eAd1	pouze
malou	malý	k2eAgFnSc4d1	malá
část	část	k1gFnSc4	část
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
asi	asi	k9	asi
mezi	mezi	k7c7	mezi
0,5	[number]	k4	0,5
až	až	k9	až
1,5	[number]	k4	1,5
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc4	zbytek
materiálu	materiál	k1gInSc2	materiál
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
0,5	[number]	k4	0,5
až	až	k8xS	až
3,7	[number]	k4	3,7
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
kamenný	kamenný	k2eAgInSc4d1	kamenný
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
model	model	k1gInSc1	model
stavby	stavba	k1gFnSc2	stavba
Uranu	Uran	k1gInSc2	Uran
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
tři	tři	k4xCgFnPc4	tři
oddělené	oddělený	k2eAgFnPc4d1	oddělená
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
:	:	kIx,	:
kamenné	kamenný	k2eAgNnSc1d1	kamenné
jádro	jádro	k1gNnSc1	jádro
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
ledový	ledový	k2eAgInSc1d1	ledový
plášť	plášť	k1gInSc1	plášť
a	a	k8xC	a
plynný	plynný	k2eAgInSc1d1	plynný
obal	obal	k1gInSc1	obal
tvořený	tvořený	k2eAgInSc1d1	tvořený
převážně	převážně	k6eAd1	převážně
vodíkem	vodík	k1gInSc7	vodík
a	a	k8xC	a
héliem	hélium	k1gNnSc7	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgNnSc1d1	malé
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
pouze	pouze	k6eAd1	pouze
0,55	[number]	k4	0,55
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
20	[number]	k4	20
%	%	kIx~	%
velikosti	velikost	k1gFnSc6	velikost
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc1	plášť
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
13,4	[number]	k4	13,4
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
60	[number]	k4	60
%	%	kIx~	%
velikosti	velikost	k1gFnSc2	velikost
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
svrchní	svrchní	k2eAgFnSc1d1	svrchní
atmosféra	atmosféra	k1gFnSc1	atmosféra
planety	planeta	k1gFnSc2	planeta
pak	pak	k6eAd1	pak
váží	vážit	k5eAaImIp3nS	vážit
pouze	pouze	k6eAd1	pouze
0,5	[number]	k4	0,5
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zabírá	zabírat	k5eAaImIp3nS	zabírat
zbylých	zbylý	k2eAgNnPc2d1	zbylé
20	[number]	k4	20
%	%	kIx~	%
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
má	mít	k5eAaImIp3nS	mít
hustotu	hustota	k1gFnSc4	hustota
okolo	okolo	k7c2	okolo
9	[number]	k4	9
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
tlak	tlak	k1gInSc1	tlak
zde	zde	k6eAd1	zde
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
8	[number]	k4	8
miliónů	milión	k4xCgInPc2	milión
barů	bar	k1gInPc2	bar
(	(	kIx(	(
<g/>
800	[number]	k4	800
GPa	GPa	k1gFnPc2	GPa
<g/>
)	)	kIx)	)
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
5000	[number]	k4	5000
K.	K.	kA	K.
Ledový	ledový	k2eAgInSc1d1	ledový
plášť	plášť	k1gInSc1	plášť
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
tvořený	tvořený	k2eAgInSc4d1	tvořený
z	z	k7c2	z
pevného	pevný	k2eAgInSc2d1	pevný
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
husté	hustý	k2eAgFnSc2d1	hustá
tekuté	tekutý	k2eAgFnSc2d1	tekutá
kapaliny	kapalina	k1gFnSc2	kapalina
tvořené	tvořený	k2eAgNnSc1d1	tvořené
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
čpavkem	čpavek	k1gInSc7	čpavek
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
lehkými	lehký	k2eAgFnPc7d1	lehká
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
kapalina	kapalina	k1gFnSc1	kapalina
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
elektricky	elektricky	k6eAd1	elektricky
vodivá	vodivý	k2eAgFnSc1d1	vodivá
a	a	k8xC	a
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vodo-čpavkový	vodo-čpavkový	k2eAgInSc1d1	vodo-čpavkový
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
pláště	plášť	k1gInSc2	plášť
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
velice	velice	k6eAd1	velice
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
i	i	k9	i
v	v	k7c6	v
rozdílné	rozdílný	k2eAgFnSc6d1	rozdílná
klasifikaci	klasifikace	k1gFnSc6	klasifikace
Uranu	Uran	k1gInSc2	Uran
a	a	k8xC	a
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
ledové	ledový	k2eAgFnPc4d1	ledová
obry	obr	k1gMnPc7	obr
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
popsaný	popsaný	k2eAgInSc1d1	popsaný
model	model	k1gInSc1	model
není	být	k5eNaImIp3nS	být
ojedinělý	ojedinělý	k2eAgInSc1d1	ojedinělý
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
modely	model	k1gInPc1	model
složení	složení	k1gNnSc2	složení
Uranu	Uran	k1gMnSc3	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
změnit	změnit	k5eAaPmF	změnit
odhad	odhad	k1gInSc4	odhad
zastoupení	zastoupení	k1gNnSc2	zastoupení
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
a	a	k8xC	a
horninového	horninový	k2eAgInSc2d1	horninový
materiálu	materiál	k1gInSc2	materiál
smíchaného	smíchaný	k2eAgMnSc4d1	smíchaný
s	s	k7c7	s
ledem	led	k1gInSc7	led
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
zmenšení	zmenšení	k1gNnSc3	zmenšení
odhadu	odhad	k1gInSc2	odhad
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
ledu	led	k1gInSc2	led
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
stavbě	stavba	k1gFnSc6	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgNnPc1d1	současné
data	datum	k1gNnPc1	datum
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
přesně	přesně	k6eAd1	přesně
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
model	model	k1gInSc1	model
je	být	k5eAaImIp3nS	být
správný	správný	k2eAgInSc1d1	správný
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
modely	model	k1gInPc1	model
se	se	k3xPyFc4	se
však	však	k9	však
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Uran	Uran	k1gInSc1	Uran
nemá	mít	k5eNaImIp3nS	mít
pevný	pevný	k2eAgInSc4d1	pevný
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
že	že	k8xS	že
atmosféra	atmosféra	k1gFnSc1	atmosféra
pozvolna	pozvolna	k6eAd1	pozvolna
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
kapalné	kapalný	k2eAgFnSc2d1	kapalná
celoplanetární	celoplanetární	k2eAgFnSc2d1	celoplanetární
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
rotační	rotační	k2eAgInSc1d1	rotační
elipsoid	elipsoid	k1gInSc1	elipsoid
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yRgInSc2	který
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc1	povrch
uměle	uměle	k6eAd1	uměle
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xC	jako
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
tlak	tlak	k1gInSc1	tlak
roven	roven	k2eAgInSc1d1	roven
1	[number]	k4	1
baru	bar	k1gInSc2	bar
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
rovníkový	rovníkový	k2eAgInSc1d1	rovníkový
poloměr	poloměr	k1gInSc1	poloměr
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
25	[number]	k4	25
559	[number]	k4	559
±	±	k?	±
4	[number]	k4	4
km	km	kA	km
<g/>
,	,	kIx,	,
polární	polární	k2eAgInSc1d1	polární
poloměr	poloměr	k1gInSc1	poloměr
pak	pak	k6eAd1	pak
24	[number]	k4	24
973	[number]	k4	973
±	±	k?	±
20	[number]	k4	20
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
definovaný	definovaný	k2eAgInSc1d1	definovaný
povrch	povrch	k1gInSc1	povrch
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
nulová	nulový	k2eAgFnSc1d1	nulová
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
teplo	teplo	k1gNnSc1	teplo
Uranu	Uran	k1gInSc2	Uran
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
značně	značně	k6eAd1	značně
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
plynné	plynný	k2eAgMnPc4d1	plynný
obry	obr	k1gMnPc4	obr
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
nízkém	nízký	k2eAgInSc6d1	nízký
tepelném	tepelný	k2eAgInSc6d1	tepelný
toku	tok	k1gInSc6	tok
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
teplota	teplota	k1gFnSc1	teplota
Uranu	Uran	k1gInSc2	Uran
tak	tak	k6eAd1	tak
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
stále	stále	k6eAd1	stále
dostatečně	dostatečně	k6eAd1	dostatečně
vysvětleno	vysvětlen	k2eAgNnSc1d1	vysvětleno
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gInSc1	Neptun
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
velikostí	velikost	k1gFnSc7	velikost
a	a	k8xC	a
složením	složení	k1gNnSc7	složení
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
Uranu	Uran	k1gInSc3	Uran
<g/>
,	,	kIx,	,
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
do	do	k7c2	do
okolí	okolí	k1gNnPc2	okolí
2,61	[number]	k4	2,61
<g/>
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
dostává	dostávat	k5eAaImIp3nS	dostávat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
nevyzařuje	vyzařovat	k5eNaImIp3nS	vyzařovat
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
skoro	skoro	k6eAd1	skoro
žádnou	žádný	k3yNgFnSc4	žádný
energii	energie	k1gFnSc4	energie
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
vyzářená	vyzářený	k2eAgFnSc1d1	vyzářená
energie	energie	k1gFnSc1	energie
Uranu	Uran	k1gInSc2	Uran
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
(	(	kIx(	(
<g/>
tepelné	tepelný	k2eAgFnSc6d1	tepelná
<g/>
)	)	kIx)	)
části	část	k1gFnSc3	část
spektra	spektrum	k1gNnSc2	spektrum
je	být	k5eAaImIp3nS	být
1,06	[number]	k4	1,06
±	±	k?	±
0,08	[number]	k4	0,08
násobek	násobek	k1gInSc1	násobek
sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
absorbované	absorbovaný	k2eAgInPc4d1	absorbovaný
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Tepelný	tepelný	k2eAgInSc1d1	tepelný
tok	tok	k1gInSc1	tok
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
0,042	[number]	k4	0,042
±	±	k?	±
0,047	[number]	k4	0,047
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
i	i	k9	i
než	než	k8xS	než
tepelný	tepelný	k2eAgInSc4d1	tepelný
tok	tok	k1gInSc4	tok
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
0,075	[number]	k4	0,075
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
nejmenší	malý	k2eAgFnSc1d3	nejmenší
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
teplota	teplota	k1gFnSc1	teplota
49	[number]	k4	49
K	K	kA	K
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
224	[number]	k4	224
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
v	v	k7c6	v
Uranově	Uranův	k2eAgFnSc6d1	Uranova
tropopauze	tropopauza	k1gFnSc6	tropopauza
dělá	dělat	k5eAaImIp3nS	dělat
z	z	k7c2	z
Uranu	Uran	k1gInSc2	Uran
nejchladnější	chladný	k2eAgFnSc4d3	nejchladnější
planetu	planeta	k1gFnSc4	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Hypotézy	hypotéza	k1gFnPc1	hypotéza
vysvětlující	vysvětlující	k2eAgFnPc1d1	vysvětlující
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
pracují	pracovat	k5eAaImIp3nP	pracovat
například	například	k6eAd1	například
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
superhmotné	superhmotný	k2eAgFnSc2d1	superhmotná
srážky	srážka	k1gFnSc2	srážka
Uranu	Uran	k1gInSc2	Uran
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
tělesem	těleso	k1gNnSc7	těleso
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
měla	mít	k5eAaImAgNnP	mít
za	za	k7c4	za
výsledek	výsledek	k1gInSc4	výsledek
převrácení	převrácení	k1gNnSc2	převrácení
sklonu	sklon	k1gInSc2	sklon
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mohlo	moct	k5eAaImAgNnS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
většiny	většina	k1gFnSc2	většina
primárního	primární	k2eAgNnSc2d1	primární
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
ochlazení	ochlazení	k1gNnSc2	ochlazení
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
hypotéza	hypotéza	k1gFnSc1	hypotéza
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvnitř	uvnitř	k7c2	uvnitř
Uranu	Uran	k1gInSc2	Uran
existuje	existovat	k5eAaImIp3nS	existovat
vrstva	vrstva	k1gFnSc1	vrstva
či	či	k8xC	či
vrstvy	vrstva	k1gFnPc1	vrstva
bránící	bránící	k2eAgNnSc1d1	bránící
proudění	proudění	k1gNnSc1	proudění
tepla	teplo	k1gNnSc2	teplo
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Konvekce	konvekce	k1gFnSc1	konvekce
(	(	kIx(	(
<g/>
přenos	přenos	k1gInSc1	přenos
tepla	teplo	k1gNnSc2	teplo
prouděním	proudění	k1gNnSc7	proudění
hmoty	hmota	k1gFnSc2	hmota
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
probíhala	probíhat	k5eAaImAgFnS	probíhat
mezi	mezi	k7c7	mezi
vrstvami	vrstva	k1gFnPc7	vrstva
různého	různý	k2eAgNnSc2d1	různé
složení	složení	k1gNnSc2	složení
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
účinně	účinně	k6eAd1	účinně
bránily	bránit	k5eAaImAgFnP	bránit
výstupu	výstup	k1gInSc3	výstup
teplého	teplý	k2eAgInSc2d1	teplý
materiálu	materiál	k1gInSc2	materiál
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
je	být	k5eAaImIp3nS	být
nejchladnější	chladný	k2eAgFnSc7d3	nejchladnější
planetou	planeta	k1gFnSc7	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
jeho	jeho	k3xOp3gFnSc2	jeho
atmosféry	atmosféra	k1gFnSc2	atmosféra
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jen	jen	k9	jen
53	[number]	k4	53
K	K	kA	K
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
220	[number]	k4	220
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
teplota	teplota	k1gFnSc1	teplota
změřená	změřený	k2eAgFnSc1d1	změřená
v	v	k7c6	v
tropopauze	tropopauza	k1gFnSc6	tropopauza
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
49	[number]	k4	49
K	K	kA	K
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
činí	činit	k5eAaImIp3nS	činit
Uran	Uran	k1gInSc4	Uran
nejchladnější	chladný	k2eAgFnSc7d3	nejchladnější
planetou	planeta	k1gFnSc7	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
výraznému	výrazný	k2eAgInSc3d1	výrazný
odklonu	odklon	k1gInSc3	odklon
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
přijímají	přijímat	k5eAaImIp3nP	přijímat
polární	polární	k2eAgFnPc4d1	polární
oblasti	oblast	k1gFnPc4	oblast
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
energie	energie	k1gFnSc2	energie
než	než	k8xS	než
rovníkové	rovníkový	k2eAgFnSc2d1	Rovníková
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníku	rovník	k1gInSc2	rovník
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
způsobující	způsobující	k2eAgInSc1d1	způsobující
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
známý	známý	k2eAgMnSc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Ví	vědět	k5eAaImIp3nS	vědět
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vane	vanout	k5eAaImIp3nS	vanout
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
vítr	vítr	k1gInSc1	vítr
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k6eAd1	až
900	[number]	k4	900
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Vzhled	vzhled	k1gInSc4	vzhled
atmosféry	atmosféra	k1gFnSc2	atmosféra
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
jednolitý	jednolitý	k2eAgInSc4d1	jednolitý
bez	bez	k7c2	bez
znatelné	znatelný	k2eAgFnSc2d1	znatelná
struktury	struktura	k1gFnSc2	struktura
jak	jak	k8xC	jak
ve	v	k7c6	v
viditelném	viditelný	k2eAgInSc6d1	viditelný
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
ultrafialovém	ultrafialový	k2eAgNnSc6d1	ultrafialové
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Uran	Uran	k1gInSc1	Uran
nemá	mít	k5eNaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgInPc4	žádný
zdroje	zdroj	k1gInPc4	zdroj
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
tepla	teplo	k1gNnSc2	teplo
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
plynnými	plynný	k2eAgMnPc7d1	plynný
obry	obr	k1gMnPc7	obr
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
dynamika	dynamika	k1gFnSc1	dynamika
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
deset	deset	k4xCc1	deset
nevýrazných	výrazný	k2eNgFnPc2d1	nevýrazná
světlých	světlý	k2eAgFnPc2d1	světlá
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
pozorovány	pozorovat	k5eAaImNgFnP	pozorovat
i	i	k8xC	i
Hubbleovým	Hubbleův	k2eAgInSc7d1	Hubbleův
teleskopem	teleskop	k1gInSc7	teleskop
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
interpretovány	interpretován	k2eAgInPc1d1	interpretován
jako	jako	k9	jako
mračna	mračno	k1gNnPc1	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféru	atmosféra	k1gFnSc4	atmosféra
Uranu	Uran	k1gInSc2	Uran
tvoří	tvořit	k5eAaImIp3nP	tvořit
převážně	převážně	k6eAd1	převážně
molekulární	molekulární	k2eAgInSc4d1	molekulární
vodík	vodík	k1gInSc4	vodík
a	a	k8xC	a
helium	helium	k1gNnSc4	helium
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgInSc1d1	molekulární
podíl	podíl	k1gInSc1	podíl
helia	helium	k1gNnSc2	helium
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
podíl	podíl	k1gInSc4	podíl
počtu	počet	k1gInSc2	počet
atomů	atom	k1gInPc2	atom
helia	helium	k1gNnSc2	helium
vůči	vůči	k7c3	vůči
počtu	počet	k1gInSc3	počet
molekul	molekula	k1gFnPc2	molekula
všech	všecek	k3xTgInPc2	všecek
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
0,15	[number]	k4	0,15
±	±	k?	±
0,03	[number]	k4	0,03
ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
troposféře	troposféra	k1gFnSc6	troposféra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
hmotnostnímu	hmotnostní	k2eAgInSc3d1	hmotnostní
podílu	podíl	k1gInSc3	podíl
helia	helium	k1gNnSc2	helium
0,26	[number]	k4	0,26
±	±	k?	±
0,05	[number]	k4	0,05
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
blízká	blízký	k2eAgNnPc4d1	blízké
množství	množství	k1gNnPc4	množství
hélia	hélium	k1gNnSc2	hélium
v	v	k7c6	v
protohvězdách	protohvězda	k1gFnPc6	protohvězda
(	(	kIx(	(
<g/>
0,275	[number]	k4	0,275
±	±	k?	±
0,01	[number]	k4	0,01
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hélium	hélium	k1gNnSc1	hélium
nesoustředilo	soustředit	k5eNaPmAgNnS	soustředit
do	do	k7c2	do
středu	střed	k1gInSc2	střed
planety	planeta	k1gFnSc2	planeta
jako	jako	k8xS	jako
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
nejčetnější	četný	k2eAgFnSc1d3	nejčetnější
komponenta	komponenta	k1gFnSc1	komponenta
atmosféry	atmosféra	k1gFnSc2	atmosféra
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
metan	metan	k1gInSc1	metan
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
absorpci	absorpce	k1gFnSc4	absorpce
viditelného	viditelný	k2eAgNnSc2d1	viditelné
a	a	k8xC	a
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
světla	světlo	k1gNnSc2	světlo
projevující	projevující	k2eAgFnSc4d1	projevující
se	se	k3xPyFc4	se
typickou	typický	k2eAgFnSc7d1	typická
namodralou	namodralý	k2eAgFnSc7d1	namodralá
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
metanovou	metanový	k2eAgFnSc7d1	metanový
vrstvou	vrstva	k1gFnSc7	vrstva
mraků	mrak	k1gInPc2	mrak
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
tlaku	tlak	k1gInSc2	tlak
1,3	[number]	k4	1,3
bar	bar	k1gInSc1	bar
(	(	kIx(	(
<g/>
130	[number]	k4	130
kPa	kPa	k?	kPa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
molekuly	molekula	k1gFnPc4	molekula
metanu	metan	k1gInSc2	metan
2,3	[number]	k4	2,3
%	%	kIx~	%
molárního	molární	k2eAgInSc2d1	molární
podílu	podíl	k1gInSc2	podíl
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
představuje	představovat	k5eAaImIp3nS	představovat
20	[number]	k4	20
<g/>
krát	krát	k6eAd1	krát
až	až	k9	až
30	[number]	k4	30
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgInSc4d2	veliký
podíl	podíl	k1gInSc4	podíl
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Poměrné	poměrný	k2eAgNnSc1d1	poměrné
zastoupení	zastoupení	k1gNnSc1	zastoupení
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc1d2	nižší
ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
atmosféře	atmosféra	k1gFnSc6	atmosféra
kvůli	kvůli	k7c3	kvůli
extrémně	extrémně	k6eAd1	extrémně
nízké	nízký	k2eAgFnSc3d1	nízká
teplotě	teplota	k1gFnSc3	teplota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
míru	míra	k1gFnSc4	míra
nasycení	nasycení	k1gNnSc2	nasycení
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
mrznutí	mrznutí	k1gNnSc1	mrznutí
nadbytečného	nadbytečný	k2eAgInSc2d1	nadbytečný
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
<s>
Zastoupení	zastoupení	k1gNnSc1	zastoupení
lehkých	lehký	k2eAgFnPc2d1	lehká
těkavých	těkavý	k2eAgFnPc2d1	těkavá
látek	látka	k1gFnPc2	látka
jako	jako	k8xC	jako
čpavku	čpavek	k1gInSc2	čpavek
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
či	či	k8xC	či
sulfanu	sulfan	k1gInSc2	sulfan
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
atmosféře	atmosféra	k1gFnSc6	atmosféra
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
však	však	k9	však
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
taktéž	taktéž	k?	taktéž
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
Slunce	slunce	k1gNnSc4	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
metanu	metan	k1gInSc2	metan
se	se	k3xPyFc4	se
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
Uranu	Uran	k1gInSc2	Uran
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
jako	jako	k9	jako
výsledek	výsledek	k1gInSc4	výsledek
chemického	chemický	k2eAgInSc2d1	chemický
rozkladu	rozklad	k1gInSc2	rozklad
metanu	metan	k1gInSc2	metan
vyvolaného	vyvolaný	k2eAgInSc2d1	vyvolaný
slunečním	sluneční	k2eAgNnSc7d1	sluneční
ultrafialovým	ultrafialový	k2eAgNnSc7d1	ultrafialové
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
látky	látka	k1gFnPc1	látka
jako	jako	k8xC	jako
ethan	ethan	k1gInSc1	ethan
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
acetylén	acetylén	k1gInSc1	acetylén
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
metylacetylén	metylacetylén	k1gInSc1	metylacetylén
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
)	)	kIx)	)
či	či	k8xC	či
diacetylén	diacetylén	k1gInSc1	diacetylén
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
HC	HC	kA	HC
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spektroskopická	spektroskopický	k2eAgNnPc1d1	spektroskopické
měření	měření	k1gNnPc1	měření
taktéž	taktéž	k?	taktéž
detekovala	detekovat	k5eAaImAgFnS	detekovat
stopy	stop	k1gInPc4	stop
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
části	část	k1gFnSc6	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
pocházet	pocházet	k5eAaImF	pocházet
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
externích	externí	k2eAgInPc2d1	externí
zdrojů	zdroj	k1gInPc2	zdroj
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
komety	kometa	k1gFnPc1	kometa
či	či	k8xC	či
meziplanetární	meziplanetární	k2eAgInSc1d1	meziplanetární
prach	prach	k1gInSc1	prach
<g/>
.	.	kIx.	.
</s>
<s>
Troposféra	troposféra	k1gFnSc1	troposféra
je	být	k5eAaImIp3nS	být
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
a	a	k8xC	a
také	také	k9	také
nejhustší	hustý	k2eAgFnSc4d3	nejhustší
část	část	k1gFnSc4	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
poklesem	pokles	k1gInSc7	pokles
teploty	teplota	k1gFnSc2	teplota
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
klesá	klesat	k5eAaImIp3nS	klesat
z	z	k7c2	z
okolo	okolo	k6eAd1	okolo
320	[number]	k4	320
K	K	kA	K
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
hranici	hranice	k1gFnSc6	hranice
troposféry	troposféra	k1gFnSc2	troposféra
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
-	-	kIx~	-
km	km	kA	km
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
53	[number]	k4	53
K	k	k7c3	k
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
50	[number]	k4	50
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
nejchladnější	chladný	k2eAgFnSc6d3	nejchladnější
svrchní	svrchní	k2eAgFnSc6d1	svrchní
oblasti	oblast	k1gFnSc6	oblast
troposféry	troposféra	k1gFnSc2	troposféra
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
tropopauza	tropopauza	k1gFnSc1	tropopauza
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
mezi	mezi	k7c7	mezi
49	[number]	k4	49
až	až	k8xS	až
57	[number]	k4	57
K	k	k7c3	k
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
planetární	planetární	k2eAgFnSc6d1	planetární
šířce	šířka	k1gFnSc6	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
tropopauzy	tropopauza	k1gFnSc2	tropopauza
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
většinu	většina	k1gFnSc4	většina
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
efektivní	efektivní	k2eAgFnSc4d1	efektivní
teplotu	teplota	k1gFnSc4	teplota
59,1	[number]	k4	59,1
±	±	k?	±
0,3	[number]	k4	0,3
K.	K.	kA	K.
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
existují	existovat	k5eAaImIp3nP	existovat
složitá	složitý	k2eAgNnPc1d1	složité
mračna	mračno	k1gNnPc1	mračno
<g/>
;	;	kIx,	;
vodní	vodní	k2eAgInPc1d1	vodní
mraky	mrak	k1gInPc1	mrak
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
hypoteticky	hypoteticky	k6eAd1	hypoteticky
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tlak	tlak	k1gInSc1	tlak
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
50	[number]	k4	50
až	až	k9	až
100	[number]	k4	100
bar	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
mračna	mračna	k1gFnSc1	mračna
hydrosulfidu	hydrosulfid	k1gInSc2	hydrosulfid
amonného	amonný	k2eAgInSc2d1	amonný
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
bar	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
amoniaková	amoniakový	k2eAgFnSc1d1	amoniaková
či	či	k8xC	či
sulfanová	sulfanový	k2eAgFnSc1d1	sulfanový
mračna	mračna	k1gFnSc1	mračna
mezi	mezi	k7c7	mezi
3	[number]	k4	3
až	až	k9	až
10	[number]	k4	10
bar	bar	k1gInSc4	bar
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
detekovaná	detekovaný	k2eAgNnPc1d1	detekované
řídká	řídký	k2eAgNnPc1d1	řídké
metanová	metanový	k2eAgNnPc1d1	metanový
mračna	mračno	k1gNnPc1	mračno
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
1	[number]	k4	1
až	až	k8xS	až
2	[number]	k4	2
bar	bar	k1gInSc1	bar
<g/>
.	.	kIx.	.
</s>
<s>
Troposféra	troposféra	k1gFnSc1	troposféra
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
dynamická	dynamický	k2eAgFnSc1d1	dynamická
oblast	oblast	k1gFnSc1	oblast
se	s	k7c7	s
silnými	silný	k2eAgInPc7d1	silný
větry	vítr	k1gInPc7	vítr
<g/>
,	,	kIx,	,
světlými	světlý	k2eAgNnPc7d1	světlé
mračny	mračno	k1gNnPc7	mračno
a	a	k8xC	a
sezónními	sezónní	k2eAgFnPc7d1	sezónní
změnami	změna	k1gFnPc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
Prostřední	prostřední	k2eAgFnSc1d1	prostřední
vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
stratosféra	stratosféra	k1gFnSc1	stratosféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
teplota	teplota	k1gFnSc1	teplota
obecně	obecně	k6eAd1	obecně
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
z	z	k7c2	z
53	[number]	k4	53
K	k	k7c3	k
v	v	k7c6	v
tropopauze	tropopauza	k1gFnSc6	tropopauza
na	na	k7c4	na
800	[number]	k4	800
až	až	k9	až
850	[number]	k4	850
K	K	kA	K
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
termosféry	termosféra	k1gFnSc2	termosféra
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc1	nárůst
teploty	teplota	k1gFnSc2	teplota
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
absorpcí	absorpce	k1gFnSc7	absorpce
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
a	a	k8xC	a
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
metanem	metan	k1gInSc7	metan
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
uhlovodíky	uhlovodík	k1gInPc7	uhlovodík
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc4	výsledek
fotolýzy	fotolýza	k1gFnSc2	fotolýza
<g/>
.	.	kIx.	.
</s>
<s>
Uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
úzké	úzký	k2eAgFnSc6d1	úzká
vrstvě	vrstva	k1gFnSc6	vrstva
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
mezi	mezi	k7c7	mezi
100	[number]	k4	100
až	až	k9	až
280	[number]	k4	280
km	km	kA	km
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tlak	tlak	k1gInSc1	tlak
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
10	[number]	k4	10
až	až	k9	až
0,1	[number]	k4	0,1
mbar	mbara	k1gFnPc2	mbara
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
až	až	k9	až
10	[number]	k4	10
kPa	kPa	k?	kPa
<g/>
)	)	kIx)	)
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
mezi	mezi	k7c7	mezi
75	[number]	k4	75
až	až	k9	až
170	[number]	k4	170
K.	K.	kA	K.
Nejhojnější	hojný	k2eAgInPc4d3	nejhojnější
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
metan	metan	k1gInSc4	metan
<g/>
,	,	kIx,	,
acetylén	acetylén	k1gInSc4	acetylén
a	a	k8xC	a
ethan	ethan	k1gInSc4	ethan
s	s	k7c7	s
poměrným	poměrný	k2eAgNnSc7d1	poměrné
zastoupením	zastoupení	k1gNnSc7	zastoupení
okolo	okolo	k7c2	okolo
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vodíku	vodík	k1gInSc3	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejné	k1gNnSc1	stejné
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
výškách	výška	k1gFnPc6	výška
i	i	k9	i
poměrné	poměrný	k2eAgNnSc4d1	poměrné
zastoupení	zastoupení	k1gNnSc4	zastoupení
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Těžší	těžký	k2eAgInPc1d2	těžší
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
mají	mít	k5eAaImIp3nP	mít
poměrné	poměrný	k2eAgNnSc4d1	poměrné
zastoupení	zastoupení	k1gNnSc4	zastoupení
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
řády	řád	k1gInPc4	řád
nižší	nízký	k2eAgFnPc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
relativní	relativní	k2eAgInSc1d1	relativní
výskyt	výskyt	k1gInSc1	výskyt
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
7	[number]	k4	7
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Ethan	ethan	k1gInSc1	ethan
a	a	k8xC	a
acetylén	acetylén	k1gInSc1	acetylén
kondenzují	kondenzovat	k5eAaImIp3nP	kondenzovat
v	v	k7c6	v
chladnějších	chladný	k2eAgFnPc6d2	chladnější
částech	část	k1gFnPc6	část
stratosféry	stratosféra	k1gFnSc2	stratosféra
a	a	k8xC	a
tropopauz	tropopauza	k1gFnPc2	tropopauza
(	(	kIx(	(
<g/>
když	když	k8xS	když
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
tlaku	tlak	k1gInSc2	tlak
pod	pod	k7c4	pod
10	[number]	k4	10
mbar	mbara	k1gFnPc2	mbara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
zamlžená	zamlžený	k2eAgFnSc1d1	zamlžená
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
nevýrazný	výrazný	k2eNgInSc4d1	nevýrazný
vzhled	vzhled	k1gInSc4	vzhled
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
uhlovodíku	uhlovodík	k1gInSc2	uhlovodík
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
Uranu	Uran	k1gInSc2	Uran
nad	nad	k7c7	nad
mlžnou	mlžný	k2eAgFnSc7d1	mlžná
vrstvou	vrstva	k1gFnSc7	vrstva
je	být	k5eAaImIp3nS	být
významně	významně	k6eAd1	významně
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
výskyt	výskyt	k1gInSc1	výskyt
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
jiných	jiný	k2eAgMnPc2d1	jiný
plynných	plynný	k2eAgMnPc2d1	plynný
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
Nejzazší	zadní	k2eAgFnSc1d3	nejzazší
vrstva	vrstva	k1gFnSc1	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
termosférou	termosféra	k1gFnSc7	termosféra
a	a	k8xC	a
koronou	korona	k1gFnSc7	korona
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
teplotu	teplota	k1gFnSc4	teplota
mezi	mezi	k7c7	mezi
800	[number]	k4	800
až	až	k9	až
850	[number]	k4	850
K.	K.	kA	K.
Potřebné	potřebný	k2eAgInPc4d1	potřebný
zdroje	zdroj	k1gInPc4	zdroj
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
udržovat	udržovat	k5eAaImF	udržovat
takto	takto	k6eAd1	takto
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
známy	znám	k2eAgFnPc1d1	známa
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ani	ani	k8xC	ani
množství	množství	k1gNnSc1	množství
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
a	a	k8xC	a
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
není	být	k5eNaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
poskytnout	poskytnout	k5eAaPmF	poskytnout
potřebnou	potřebný	k2eAgFnSc4d1	potřebná
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
teplotě	teplota	k1gFnSc3	teplota
přispívá	přispívat	k5eAaImIp3nS	přispívat
i	i	k9	i
slabé	slabý	k2eAgNnSc1d1	slabé
vyzařování	vyzařování	k1gNnSc1	vyzařování
tepla	teplo	k1gNnSc2	teplo
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
vlivem	vlivem	k7c2	vlivem
přikrývky	přikrývka	k1gFnSc2	přikrývka
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
tlaku	tlak	k1gInSc2	tlak
0,1	[number]	k4	0,1
mbar	mbara	k1gFnPc2	mbara
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
molekulárního	molekulární	k2eAgInSc2d1	molekulární
vodíku	vodík	k1gInSc2	vodík
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
termosféra	termosféra	k1gFnSc1	termosféra
a	a	k8xC	a
korona	korona	k1gFnSc1	korona
i	i	k8xC	i
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
volných	volný	k2eAgInPc2d1	volný
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
malá	malý	k2eAgFnSc1d1	malá
hmotnost	hmotnost	k1gFnSc1	hmotnost
společně	společně	k6eAd1	společně
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
teplotou	teplota	k1gFnSc7	teplota
vysvětlují	vysvětlovat	k5eAaImIp3nP	vysvětlovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
tato	tento	k3xDgFnSc1	tento
unikátní	unikátní	k2eAgFnSc1d1	unikátní
korona	korona	k1gFnSc1	korona
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
50	[number]	k4	50
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
dvěma	dva	k4xCgInPc3	dva
poloměrům	poloměr	k1gInPc3	poloměr
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
zcela	zcela	k6eAd1	zcela
atypické	atypický	k2eAgInPc1d1	atypický
<g/>
.	.	kIx.	.
</s>
<s>
Korona	Korona	k1gFnSc1	Korona
odtlačuje	odtlačovat	k5eAaImIp3nS	odtlačovat
pryč	pryč	k6eAd1	pryč
malé	malý	k2eAgFnPc1d1	malá
částice	částice	k1gFnPc1	částice
obíhající	obíhající	k2eAgFnPc1d1	obíhající
kolem	kolem	k7c2	kolem
Uranu	Uran	k1gInSc2	Uran
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
na	na	k7c6	na
prstencích	prstenec	k1gInPc6	prstenec
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Termosféra	Termosféra	k1gFnSc1	Termosféra
planety	planeta	k1gFnSc2	planeta
společně	společně	k6eAd1	společně
se	s	k7c7	s
svrchní	svrchní	k2eAgFnSc7d1	svrchní
stratopauzou	stratopauza	k1gFnSc7	stratopauza
tvoří	tvořit	k5eAaImIp3nS	tvořit
ionosféru	ionosféra	k1gFnSc4	ionosféra
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnPc1	pozorování
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ionosféra	ionosféra	k1gFnSc1	ionosféra
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
mezi	mezi	k7c7	mezi
2	[number]	k4	2
000	[number]	k4	000
až	až	k9	až
10	[number]	k4	10
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Ionosféra	ionosféra	k1gFnSc1	ionosféra
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
hustší	hustý	k2eAgFnSc1d2	hustší
než	než	k8xS	než
ionosféra	ionosféra	k1gFnSc1	ionosféra
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Ionosféra	ionosféra	k1gFnSc1	ionosféra
je	být	k5eAaImIp3nS	být
živena	živit	k5eAaImNgFnS	živit
ultrafialovým	ultrafialový	k2eAgNnSc7d1	ultrafialové
zářením	záření	k1gNnSc7	záření
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
hustota	hustota	k1gFnSc1	hustota
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
sluneční	sluneční	k2eAgFnSc6d1	sluneční
aktivitě	aktivita	k1gFnSc6	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Jupiterem	Jupiter	k1gMnSc7	Jupiter
či	či	k8xC	či
Saturnem	Saturn	k1gMnSc7	Saturn
je	být	k5eAaImIp3nS	být
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
polární	polární	k2eAgFnSc1d1	polární
záře	záře	k1gFnSc1	záře
zcela	zcela	k6eAd1	zcela
zanedbatelná	zanedbatelný	k2eAgFnSc1d1	zanedbatelná
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
přiletěla	přiletět	k5eAaPmAgFnS	přiletět
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyagero	k1gNnPc2	Voyagero
2	[number]	k4	2
k	k	k7c3	k
Uranu	Uran	k1gInSc3	Uran
<g/>
,	,	kIx,	,
neexistovala	existovat	k5eNaImAgFnS	existovat
žádná	žádný	k3yNgNnPc4	žádný
měření	měření	k1gNnPc4	měření
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jeho	on	k3xPp3gInSc4	on
charakter	charakter	k1gInSc4	charakter
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
záhadou	záhada	k1gFnSc7	záhada
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1986	[number]	k4	1986
astronomové	astronom	k1gMnPc1	astronom
očekávali	očekávat	k5eAaImAgMnP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Uranu	Uran	k1gInSc2	Uran
bude	být	k5eAaImBp3nS	být
ležet	ležet	k5eAaImF	ležet
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Uranovo	Uranův	k2eAgNnSc1d1	Uranovo
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgNnSc1d1	zvláštní
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
centrum	centrum	k1gNnSc1	centrum
nenachází	nacházet	k5eNaImIp3nS	nacházet
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vychýleno	vychýlit	k5eAaPmNgNnS	vychýlit
téměř	téměř	k6eAd1	téměř
59	[number]	k4	59
<g/>
°	°	k?	°
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ose	osa	k1gFnSc3	osa
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
posunuto	posunut	k2eAgNnSc1d1	posunuto
mimo	mimo	k7c4	mimo
střed	střed	k1gInSc4	střed
planety	planeta	k1gFnSc2	planeta
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jižnímu	jižní	k2eAgInSc3d1	jižní
rotačnímu	rotační	k2eAgInSc3d1	rotační
pólu	pól	k1gInSc3	pól
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
poloměru	poloměr	k1gInSc2	poloměr
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
je	být	k5eAaImIp3nS	být
vytvářeno	vytvářit	k5eAaPmNgNnS	vytvářit
pohybem	pohyb	k1gInSc7	pohyb
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
mělkých	mělký	k2eAgFnPc6d1	mělká
hloubkách	hloubka	k1gFnPc6	hloubka
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Neptun	Neptun	k1gInSc1	Neptun
má	mít	k5eAaImIp3nS	mít
podobně	podobně	k6eAd1	podobně
umístěné	umístěný	k2eAgNnSc1d1	umístěné
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
není	být	k5eNaImIp3nS	být
následkem	následkem	k7c2	následkem
výchylky	výchylka	k1gFnSc2	výchylka
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
je	být	k5eAaImIp3nS	být
zkroucena	zkroutit	k5eAaPmNgFnS	zkroutit
rotací	rotace	k1gFnSc7	rotace
planety	planeta	k1gFnSc2	planeta
do	do	k7c2	do
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
vývrtkovitého	vývrtkovitý	k2eAgInSc2d1	vývrtkovitý
tvaru	tvar	k1gInSc2	tvar
táhnoucího	táhnoucí	k2eAgInSc2d1	táhnoucí
se	se	k3xPyFc4	se
za	za	k7c7	za
planetou	planeta	k1gFnSc7	planeta
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
miliónů	milión	k4xCgInPc2	milión
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
;	;	kIx,	;
o	o	k7c6	o
dříve	dříve	k6eAd2	dříve
předpokládaném	předpokládaný	k2eAgInSc6d1	předpokládaný
elektricky	elektricky	k6eAd1	elektricky
vodivém	vodivý	k2eAgInSc6d1	vodivý
extrémně	extrémně	k6eAd1	extrémně
stlačeném	stlačený	k2eAgInSc6d1	stlačený
oceánu	oceán	k1gInSc6	oceán
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
amoniaku	amoniak	k1gInSc2	amoniak
mezi	mezi	k7c7	mezi
jádrem	jádro	k1gNnSc7	jádro
a	a	k8xC	a
atmosférou	atmosféra	k1gFnSc7	atmosféra
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Anomální	anomální	k2eAgFnSc1d1	anomální
poloha	poloha	k1gFnSc1	poloha
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
silnou	silný	k2eAgFnSc4d1	silná
asymetrii	asymetrie	k1gFnSc4	asymetrie
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
stranách	strana	k1gFnPc6	strana
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
může	moct	k5eAaImIp3nS	moct
síla	síla	k1gFnSc1	síla
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
při	při	k7c6	při
povrchu	povrch	k1gInSc6	povrch
dosahovat	dosahovat	k5eAaImF	dosahovat
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
μ	μ	k?	μ
<g/>
,	,	kIx,	,
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
110	[number]	k4	110
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
má	mít	k5eAaImIp3nS	mít
pole	pole	k1gNnSc1	pole
sílu	síl	k1gInSc2	síl
okolo	okolo	k7c2	okolo
23	[number]	k4	23
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
silné	silný	k2eAgFnPc1d1	silná
u	u	k7c2	u
obou	dva	k4xCgInPc2	dva
pólů	pól	k1gInPc2	pól
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
"	"	kIx"	"
<g/>
magnetický	magnetický	k2eAgInSc1d1	magnetický
rovník	rovník	k1gInSc1	rovník
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
rovnoběžný	rovnoběžný	k2eAgInSc1d1	rovnoběžný
se	s	k7c7	s
zeměpisným	zeměpisný	k2eAgInSc7d1	zeměpisný
rovníkem	rovník	k1gInSc7	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Magnetický	magnetický	k2eAgInSc1d1	magnetický
dipólový	dipólový	k2eAgInSc1d1	dipólový
moment	moment	k1gInSc1	moment
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Neptun	Neptun	k1gInSc1	Neptun
má	mít	k5eAaImIp3nS	mít
podobně	podobně	k6eAd1	podobně
posunuté	posunutý	k2eAgNnSc1d1	posunuté
a	a	k8xC	a
odkloněné	odkloněný	k2eAgNnSc1d1	odkloněné
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
obecný	obecný	k2eAgInSc4d1	obecný
rys	rys	k1gInSc4	rys
ledových	ledový	k2eAgMnPc2d1	ledový
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
hypotéza	hypotéza	k1gFnSc1	hypotéza
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oproti	oproti	k7c3	oproti
terestrickým	terestrický	k2eAgFnPc3d1	terestrická
planetám	planeta	k1gFnPc3	planeta
a	a	k8xC	a
plynným	plynný	k2eAgMnPc3d1	plynný
obrům	obr	k1gMnPc3	obr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
ledových	ledový	k2eAgMnPc2d1	ledový
obrů	obr	k1gMnPc2	obr
vznik	vznik	k1gInSc4	vznik
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
pohybem	pohyb	k1gInSc7	pohyb
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
hloubce	hloubka	k1gFnSc6	hloubka
-	-	kIx~	-
například	například	k6eAd1	například
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
čpavku	čpavek	k1gInSc2	čpavek
nacházejícím	nacházející	k2eAgFnPc3d1	nacházející
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
jádrem	jádro	k1gNnSc7	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
netypickému	typický	k2eNgNnSc3d1	netypické
magnetickému	magnetický	k2eAgNnSc3d1	magnetické
poli	pole	k1gNnSc3	pole
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
ohledech	ohled	k1gInPc6	ohled
je	být	k5eAaImIp3nS	být
magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k9	jako
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
planet	planeta	k1gFnPc2	planeta
<g/>
:	:	kIx,	:
má	mít	k5eAaImIp3nS	mít
rázovou	rázový	k2eAgFnSc4d1	rázová
vlnu	vlna	k1gFnSc4	vlna
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	s	k7c7	s
23	[number]	k4	23
poloměrů	poloměr	k1gInPc2	poloměr
planety	planeta	k1gFnSc2	planeta
před	před	k7c7	před
Uranem	Uran	k1gInSc7	Uran
<g/>
,	,	kIx,	,
magnetopauzu	magnetopauza	k1gFnSc4	magnetopauza
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
18	[number]	k4	18
poloměrů	poloměr	k1gInPc2	poloměr
Uranu	Uran	k1gInSc2	Uran
<g/>
,	,	kIx,	,
plně	plně	k6eAd1	plně
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
magnetický	magnetický	k2eAgInSc1d1	magnetický
ohon	ohon	k1gInSc1	ohon
a	a	k8xC	a
radiační	radiační	k2eAgInPc1d1	radiační
pásy	pás	k1gInPc1	pás
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
struktura	struktura	k1gFnSc1	struktura
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
spíše	spíše	k9	spíše
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
magnetosféře	magnetosféra	k1gFnSc3	magnetosféra
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Magnetosféra	magnetosféra	k1gFnSc1	magnetosféra
Uranu	Uran	k1gInSc2	Uran
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nabité	nabitý	k2eAgFnSc2d1	nabitá
částice	částice	k1gFnSc2	částice
<g/>
:	:	kIx,	:
protony	proton	k1gInPc1	proton
a	a	k8xC	a
elektrony	elektron	k1gInPc1	elektron
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
iontů	ion	k1gInPc2	ion
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgInPc1	žádný
další	další	k2eAgInPc1d1	další
těžší	těžký	k2eAgInPc1d2	těžší
ionty	ion	k1gInPc1	ion
nebyly	být	k5eNaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
zmiňovaných	zmiňovaný	k2eAgFnPc2d1	zmiňovaná
částic	částice	k1gFnPc2	částice
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
horké	horký	k2eAgFnSc2d1	horká
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
korony	korona	k1gFnSc2	korona
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
iontů	ion	k1gInPc2	ion
a	a	k8xC	a
elektronů	elektron	k1gInPc2	elektron
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
1,2	[number]	k4	1,2
až	až	k9	až
4	[number]	k4	4
megaelektronvoltu	megaelektronvolt	k1gInSc2	megaelektronvolt
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
nízkoenergetických	nízkoenergetický	k2eAgInPc2d1	nízkoenergetický
iontů	ion	k1gInPc2	ion
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
1	[number]	k4	1
kiloelektronvolt	kiloelektronvolta	k1gFnPc2	kiloelektronvolta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
magnetosféře	magnetosféra	k1gFnSc6	magnetosféra
okolo	okolo	k7c2	okolo
2	[number]	k4	2
cm	cm	kA	cm
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
měsíci	měsíc	k1gInPc7	měsíc
Uranu	Uran	k1gInSc2	Uran
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vymetly	vymetnout	k5eAaPmAgFnP	vymetnout
oblasti	oblast	k1gFnPc4	oblast
okolo	okolo	k7c2	okolo
svých	svůj	k3xOyFgFnPc2	svůj
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
tak	tak	k9	tak
mezery	mezera	k1gFnPc1	mezera
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
způsobil	způsobit	k5eAaPmAgMnS	způsobit
ztmavnutí	ztmavnutí	k1gNnSc4	ztmavnutí
měsíčních	měsíční	k2eAgInPc2d1	měsíční
povrchů	povrch	k1gInPc2	povrch
za	za	k7c4	za
astronomicky	astronomicky	k6eAd1	astronomicky
relativně	relativně	k6eAd1	relativně
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
100	[number]	k4	100
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
tmavého	tmavý	k2eAgNnSc2d1	tmavé
zbarvení	zbarvení	k1gNnSc2	zbarvení
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
dobře	dobře	k6eAd1	dobře
vyvinutou	vyvinutý	k2eAgFnSc4d1	vyvinutá
polární	polární	k2eAgFnSc4d1	polární
záři	záře	k1gFnSc4	záře
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
jako	jako	k9	jako
jasný	jasný	k2eAgInSc4d1	jasný
oblouk	oblouk	k1gInSc4	oblouk
okolo	okolo	k7c2	okolo
obou	dva	k4xCgInPc2	dva
magnetických	magnetický	k2eAgInPc2d1	magnetický
pólů	pól	k1gInPc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Jupitera	Jupiter	k1gMnSc2	Jupiter
polární	polární	k2eAgFnSc2d1	polární
záře	zář	k1gFnSc2	zář
Uranu	Uran	k1gInSc2	Uran
výrazně	výrazně	k6eAd1	výrazně
neovlivňují	ovlivňovat	k5eNaImIp3nP	ovlivňovat
energetickou	energetický	k2eAgFnSc4d1	energetická
bilanci	bilance	k1gFnSc4	bilance
termosféry	termosféra	k1gFnSc2	termosféra
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
obíhá	obíhat	k5eAaImIp3nS	obíhat
Slunce	slunce	k1gNnSc4	slunce
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
2	[number]	k4	2
870	[number]	k4	870
972	[number]	k4	972
220	[number]	k4	220
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
se	se	k3xPyFc4	se
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
nejvíce	nejvíce	k6eAd1	nejvíce
na	na	k7c4	na
2	[number]	k4	2
735	[number]	k4	735
555	[number]	k4	555
035	[number]	k4	035
km	km	kA	km
a	a	k8xC	a
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
na	na	k7c4	na
3	[number]	k4	3
006	[number]	k4	006
389	[number]	k4	389
405	[number]	k4	405
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
oběhne	oběhnout	k5eAaPmIp3nS	oběhnout
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
84,07	[number]	k4	84,07
let	léto	k1gNnPc2	léto
a	a	k8xC	a
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
za	za	k7c4	za
17	[number]	k4	17
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
14	[number]	k4	14
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznačnějších	význačný	k2eAgInPc2d3	nejvýznačnější
znaků	znak	k1gInPc2	znak
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
sklon	sklon	k1gInSc1	sklon
rovníku	rovník	k1gInSc2	rovník
Uranu	Uran	k1gInSc2	Uran
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
jeho	jeho	k3xOp3gFnSc2	jeho
dráhy	dráha	k1gFnSc2	dráha
o	o	k7c4	o
97,86	[number]	k4	97,86
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
rotace	rotace	k1gFnSc1	rotace
planety	planeta	k1gFnSc2	planeta
retrográdní	retrográdní	k2eAgFnSc1d1	retrográdní
<g/>
.	.	kIx.	.
</s>
<s>
Rovina	rovina	k1gFnSc1	rovina
oběhu	oběh	k1gInSc2	oběh
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
k	k	k7c3	k
ekliptice	ekliptika	k1gFnSc3	ekliptika
skloněna	skloněn	k2eAgFnSc1d1	skloněna
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
úhlem	úhel	k1gInSc7	úhel
0,769	[number]	k4	0,769
86	[number]	k4	86
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
rotační	rotační	k2eAgFnSc1d1	rotační
osa	osa	k1gFnSc1	osa
leží	ležet	k5eAaImIp3nS	ležet
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
ekliptice	ekliptika	k1gFnSc6	ekliptika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
během	během	k7c2	během
Uranova	Uranův	k2eAgInSc2d1	Uranův
roku	rok	k1gInSc2	rok
svítí	svítit	k5eAaImIp3nS	svítit
Slunce	slunce	k1gNnSc4	slunce
střídavě	střídavě	k6eAd1	střídavě
na	na	k7c4	na
severní	severní	k2eAgInSc4d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgInSc4d1	jižní
pól	pól	k1gInSc4	pól
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
póly	pól	k1gInPc4	pól
postupně	postupně	k6eAd1	postupně
míří	mířit	k5eAaImIp3nP	mířit
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
na	na	k7c6	na
pólu	pól	k1gInSc6	pól
pak	pak	k6eAd1	pak
trvá	trvat	k5eAaImIp3nS	trvat
42	[number]	k4	42
let	léto	k1gNnPc2	léto
a	a	k8xC	a
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
42	[number]	k4	42
let	léto	k1gNnPc2	léto
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
orbity	orbita	k1gFnSc2	orbita
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
natočena	natočen	k2eAgFnSc1d1	natočena
rovníkem	rovník	k1gInSc7	rovník
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
a	a	k8xC	a
zapadá	zapadat	k5eAaPmIp3nS	zapadat
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Prstence	prstenec	k1gInPc4	prstenec
společně	společně	k6eAd1	společně
s	s	k7c7	s
měsíci	měsíc	k1gInPc7	měsíc
pak	pak	k6eAd1	pak
obíhají	obíhat	k5eAaImIp3nP	obíhat
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
Uranova	Uranův	k2eAgInSc2d1	Uranův
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
Uranova	Uranův	k2eAgFnSc1d1	Uranova
soustava	soustava	k1gFnSc1	soustava
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
"	"	kIx"	"
<g/>
valí	valit	k5eAaImIp3nP	valit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
průletu	průlet	k1gInSc2	průlet
Voyageru	Voyager	k1gInSc2	Voyager
2	[number]	k4	2
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
byl	být	k5eAaImAgInS	být
Uranův	Uranův	k2eAgInSc1d1	Uranův
jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
nasměrován	nasměrován	k2eAgInSc1d1	nasměrován
téměř	téměř	k6eAd1	téměř
přesně	přesně	k6eAd1	přesně
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
označení	označení	k1gNnSc1	označení
tohoto	tento	k3xDgInSc2	tento
pólu	pól	k1gInSc2	pól
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
diskuzí	diskuze	k1gFnPc2	diskuze
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Uranu	Uran	k1gInSc2	Uran
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
buď	buď	k8xC	buď
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
odklon	odklon	k1gInSc1	odklon
osy	osa	k1gFnSc2	osa
rotace	rotace	k1gFnSc2	rotace
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
odklon	odklon	k1gInSc1	odklon
osy	osa	k1gFnSc2	osa
rotace	rotace	k1gFnSc2	rotace
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
méně	málo	k6eAd2	málo
než	než	k8xS	než
90	[number]	k4	90
<g/>
°	°	k?	°
a	a	k8xC	a
rotuje	rotovat	k5eAaImIp3nS	rotovat
ve	v	k7c6	v
zpětném	zpětný	k2eAgInSc6d1	zpětný
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
popisy	popis	k1gInPc1	popis
přesně	přesně	k6eAd1	přesně
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
skutečnému	skutečný	k2eAgNnSc3d1	skutečné
chování	chování	k1gNnSc3	chování
planety	planeta	k1gFnSc2	planeta
<g/>
;	;	kIx,	;
výsledkem	výsledek	k1gInSc7	výsledek
odlišných	odlišný	k2eAgFnPc2d1	odlišná
definic	definice	k1gFnPc2	definice
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
určení	určení	k1gNnSc1	určení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pól	pól	k1gInSc1	pól
je	být	k5eAaImIp3nS	být
severní	severní	k2eAgMnSc1d1	severní
a	a	k8xC	a
který	který	k3yRgInSc1	který
jižní	jižní	k2eAgInSc1d1	jižní
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
ale	ale	k8xC	ale
rotační	rotační	k2eAgFnSc1d1	rotační
osa	osa	k1gFnSc1	osa
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
s	s	k7c7	s
rovinou	rovina	k1gFnSc7	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
jeden	jeden	k4xCgInSc1	jeden
pól	pól	k1gInSc1	pól
nad	nad	k7c7	nad
rovinou	rovina	k1gFnSc7	rovina
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
pod	pod	k7c7	pod
rovinou	rovina	k1gFnSc7	rovina
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
pozemské	pozemský	k2eAgInPc4d1	pozemský
póly	pól	k1gInPc4	pól
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
mezinárodní	mezinárodní	k2eAgFnSc3d1	mezinárodní
domluvě	domluva	k1gFnSc3	domluva
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
označení	označení	k1gNnSc4	označení
severní	severní	k2eAgInSc1d1	severní
pól	pól	k1gInSc1	pól
pro	pro	k7c4	pro
ten	ten	k3xDgInSc4	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nad	nad	k7c7	nad
rovinou	rovina	k1gFnSc7	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
otáčí	otáčet	k5eAaImIp3nS	otáčet
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
extrémního	extrémní	k2eAgNnSc2d1	extrémní
vychýlení	vychýlení	k1gNnSc2	vychýlení
Uranovy	Uranův	k2eAgFnSc2d1	Uranova
osy	osa	k1gFnSc2	osa
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
formování	formování	k1gNnSc2	formování
planety	planeta	k1gFnSc2	planeta
došlo	dojít	k5eAaPmAgNnS	dojít
možná	možná	k9	možná
ke	k	k7c3	k
kolizi	kolize	k1gFnSc3	kolize
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
protoplanetou	protoplaneta	k1gFnSc7	protoplaneta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
změnu	změna	k1gFnSc4	změna
orientace	orientace	k1gFnSc2	orientace
<g/>
.	.	kIx.	.
</s>
<s>
Provedené	provedený	k2eAgFnPc1d1	provedená
simulace	simulace	k1gFnPc1	simulace
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
nepotvrzují	potvrzovat	k5eNaImIp3nP	potvrzovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nejde	jít	k5eNaImIp3nS	jít
tím	ten	k3xDgNnSc7	ten
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
nejsou	být	k5eNaImIp3nP	být
odkloněny	odkloněn	k2eAgFnPc1d1	odkloněna
i	i	k8xC	i
osy	osa	k1gFnPc1	osa
jeho	jeho	k3xOp3gInPc2	jeho
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
i	i	k9	i
o	o	k7c6	o
vlivu	vliv	k1gInSc6	vliv
husté	hustý	k2eAgFnSc2d1	hustá
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kvůli	kvůli	k7c3	kvůli
sklonu	sklon	k1gInSc3	sklon
osy	osa	k1gFnSc2	osa
rotace	rotace	k1gFnSc2	rotace
cirkuluje	cirkulovat	k5eAaImIp3nS	cirkulovat
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
uvažovaným	uvažovaný	k2eAgInSc7d1	uvažovaný
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
dočasná	dočasný	k2eAgFnSc1d1	dočasná
přítomnost	přítomnost	k1gFnSc1	přítomnost
velkého	velký	k2eAgInSc2d1	velký
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
provedených	provedený	k2eAgFnPc2d1	provedená
simulací	simulace	k1gFnPc2	simulace
měl	mít	k5eAaImAgInS	mít
Uran	Uran	k1gInSc1	Uran
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
měsíc	měsíc	k1gInSc4	měsíc
o	o	k7c4	o
1	[number]	k4	1
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
těleso	těleso	k1gNnSc1	těleso
srovnatelné	srovnatelný	k2eAgNnSc1d1	srovnatelné
s	s	k7c7	s
velikostí	velikost	k1gFnSc7	velikost
planety	planeta	k1gFnSc2	planeta
Mars	Mars	k1gInSc1	Mars
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
by	by	kYmCp3nS	by
po	po	k7c6	po
2	[number]	k4	2
miliónech	milión	k4xCgInPc6	milión
let	léto	k1gNnPc2	léto
sklonit	sklonit	k5eAaPmF	sklonit
rotační	rotační	k2eAgFnSc4d1	rotační
osu	osa	k1gFnSc4	osa
planety	planeta	k1gFnSc2	planeta
na	na	k7c4	na
současnou	současný	k2eAgFnSc4d1	současná
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
Uranův	Uranův	k2eAgInSc1d1	Uranův
extrémní	extrémní	k2eAgInSc1d1	extrémní
odklon	odklon	k1gInSc1	odklon
osy	osa	k1gFnSc2	osa
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
také	také	k9	také
radikální	radikální	k2eAgInPc4d1	radikální
sezónní	sezónní	k2eAgInPc4d1	sezónní
výkyvy	výkyv	k1gInPc4	výkyv
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
Voyageru	Voyager	k1gInSc2	Voyager
2	[number]	k4	2
byl	být	k5eAaImAgInS	být
pásový	pásový	k2eAgInSc1d1	pásový
vzor	vzor	k1gInSc1	vzor
Uranovy	Uranův	k2eAgFnSc2d1	Uranova
atmosféry	atmosféra	k1gFnSc2	atmosféra
velmi	velmi	k6eAd1	velmi
jemný	jemný	k2eAgInSc1d1	jemný
a	a	k8xC	a
klidný	klidný	k2eAgInSc1d1	klidný
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
Hubblova	Hubblův	k2eAgInSc2d1	Hubblův
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
dalekohledu	dalekohled	k1gInSc2	dalekohled
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
mnohem	mnohem	k6eAd1	mnohem
zřetelnější	zřetelný	k2eAgNnSc4d2	zřetelnější
pásování	pásování	k1gNnSc4	pásování
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Slunce	slunce	k1gNnSc1	slunce
osvětlovalo	osvětlovat	k5eAaImAgNnS	osvětlovat
Uranův	Uranův	k2eAgInSc4d1	Uranův
rovník	rovník	k1gInSc4	rovník
(	(	kIx(	(
<g/>
přesně	přesně	k6eAd1	přesně
nad	nad	k7c7	nad
Uranovým	Uranův	k2eAgInSc7d1	Uranův
rovníkem	rovník	k1gInSc7	rovník
bylo	být	k5eAaImAgNnS	být
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ultrafialové	ultrafialový	k2eAgFnSc6d1	ultrafialová
a	a	k8xC	a
viditelné	viditelný	k2eAgFnSc6d1	viditelná
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
je	být	k5eAaImIp3nS	být
atmosféra	atmosféra	k1gFnSc1	atmosféra
Uranu	Uran	k1gInSc2	Uran
pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
nevýrazná	výrazný	k2eNgFnSc1d1	nevýrazná
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
plynnými	plynný	k2eAgMnPc7d1	plynný
obry	obr	k1gMnPc7	obr
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
včetně	včetně	k7c2	včetně
atmosféry	atmosféra	k1gFnSc2	atmosféra
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jinak	jinak	k6eAd1	jinak
silně	silně	k6eAd1	silně
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
atmosféře	atmosféra	k1gFnSc6	atmosféra
pouze	pouze	k6eAd1	pouze
deset	deset	k4xCc4	deset
mračen	mračno	k1gNnPc2	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
možných	možný	k2eAgNnPc2d1	možné
vysvětlení	vysvětlení	k1gNnPc2	vysvětlení
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Uranu	Uran	k1gInSc6	Uran
atmosféra	atmosféra	k1gFnSc1	atmosféra
takto	takto	k6eAd1	takto
klidná	klidný	k2eAgFnSc1d1	klidná
<g/>
,	,	kIx,	,
zní	znět	k5eAaImIp3nS	znět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
teplo	teplo	k1gNnSc1	teplo
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
výraznější	výrazný	k2eAgMnSc1d2	výraznější
dynamické	dynamický	k2eAgInPc4d1	dynamický
procesy	proces	k1gInPc4	proces
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
objevil	objevit	k5eAaPmAgInS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
viditelná	viditelný	k2eAgFnSc1d1	viditelná
jižní	jižní	k2eAgFnSc1d1	jižní
polokoule	polokoule	k1gFnSc1	polokoule
Uranu	Uran	k1gInSc2	Uran
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
oblasti	oblast	k1gFnPc4	oblast
<g/>
:	:	kIx,	:
světlejší	světlý	k2eAgFnSc4d2	světlejší
polární	polární	k2eAgFnSc4d1	polární
oblast	oblast	k1gFnSc4	oblast
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
polární	polární	k2eAgFnSc1d1	polární
čepička	čepička	k1gFnSc1	čepička
<g/>
)	)	kIx)	)
a	a	k8xC	a
tmavší	tmavý	k2eAgInSc1d2	tmavší
rovníkový	rovníkový	k2eAgInSc1d1	rovníkový
pás	pás	k1gInSc1	pás
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
oblastmi	oblast	k1gFnPc7	oblast
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c6	na
-45	-45	k4	-45
rovnoběžce	rovnoběžka	k1gFnSc6	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úzkém	úzký	k2eAgInSc6d1	úzký
pásu	pás	k1gInSc6	pás
mezi	mezi	k7c7	mezi
-45	-45	k4	-45
<g/>
°	°	k?	°
a	a	k8xC	a
-50	-50	k4	-50
<g/>
°	°	k?	°
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
viditelná	viditelný	k2eAgFnSc1d1	viditelná
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jižní	jižní	k2eAgInSc1d1	jižní
"	"	kIx"	"
<g/>
límec	límec	k1gInSc1	límec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Čepička	čepička	k1gFnSc1	čepička
a	a	k8xC	a
límec	límec	k1gInSc1	límec
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tvořeny	tvořit	k5eAaImNgFnP	tvořit
hustější	hustý	k2eAgFnSc7d2	hustší
oblastí	oblast	k1gFnSc7	oblast
metanových	metanův	k2eAgNnPc2d1	metanův
mračen	mračno	k1gNnPc2	mračno
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
tlaků	tlak	k1gInPc2	tlak
mezi	mezi	k7c7	mezi
1,3	[number]	k4	1,3
až	až	k9	až
2	[number]	k4	2
bar	bar	k1gInSc1	bar
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
poblíž	poblíž	k7c2	poblíž
planety	planeta	k1gFnSc2	planeta
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kulminujícího	kulminující	k2eAgNnSc2d1	kulminující
léta	léto	k1gNnSc2	léto
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
a	a	k8xC	a
nemohla	moct	k5eNaImAgFnS	moct
tedy	tedy	k9	tedy
sledovat	sledovat	k5eAaImF	sledovat
odvrácenou	odvrácený	k2eAgFnSc4d1	odvrácená
severní	severní	k2eAgFnSc4d1	severní
polokouli	polokoule	k1gFnSc4	polokoule
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
zima	zima	k1gFnSc1	zima
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
zkoumána	zkoumat	k5eAaImNgFnS	zkoumat
Hubbleovým	Hubbleův	k2eAgInSc7d1	Hubbleův
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
dalekohledem	dalekohled	k1gInSc7	dalekohled
a	a	k8xC	a
teleskopem	teleskop	k1gInSc7	teleskop
Keck	Kecka	k1gFnPc2	Kecka
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc4	dva
pozorování	pozorování	k1gNnPc4	pozorování
nepřinesla	přinést	k5eNaPmAgFnS	přinést
žádné	žádný	k3yNgInPc4	žádný
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c4	o
přítomnost	přítomnost	k1gFnSc4	přítomnost
polární	polární	k2eAgFnSc2d1	polární
čepičky	čepička	k1gFnSc2	čepička
a	a	k8xC	a
límce	límec	k1gInSc2	límec
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Uran	Uran	k1gInSc1	Uran
je	být	k5eAaImIp3nS	být
asymetrický	asymetrický	k2eAgInSc1d1	asymetrický
<g/>
:	:	kIx,	:
světlý	světlý	k2eAgInSc1d1	světlý
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
a	a	k8xC	a
límce	límec	k1gInSc2	límec
a	a	k8xC	a
tmavý	tmavý	k2eAgMnSc1d1	tmavý
severně	severně	k6eAd1	severně
od	od	k7c2	od
límce	límec	k1gInSc2	límec
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
velkých	velký	k2eAgInPc2d1	velký
útvarů	útvar	k1gInPc2	útvar
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
pásem	pásmo	k1gNnPc2	pásmo
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
deset	deset	k4xCc1	deset
malých	malý	k2eAgFnPc2d1	malá
světlých	světlý	k2eAgFnPc2d1	světlá
mračen	mračna	k1gFnPc2	mračna
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
ležela	ležet	k5eAaImAgFnS	ležet
severně	severně	k6eAd1	severně
od	od	k7c2	od
límce	límec	k1gInSc2	límec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
dalších	další	k2eAgInPc6d1	další
ohledech	ohled	k1gInPc6	ohled
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
Uran	Uran	k1gInSc1	Uran
jevil	jevit	k5eAaImAgInS	jevit
jako	jako	k9	jako
dynamicky	dynamicky	k6eAd1	dynamicky
neaktivní	aktivní	k2eNgFnSc1d1	neaktivní
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
další	další	k2eAgNnSc4d1	další
pozorování	pozorování	k1gNnSc4	pozorování
světlých	světlý	k2eAgNnPc2d1	světlé
mračen	mračno	k1gNnPc2	mračno
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
díky	díky	k7c3	díky
rozvoji	rozvoj	k1gInSc3	rozvoj
pozorovací	pozorovací	k2eAgFnSc2d1	pozorovací
techniky	technika	k1gFnSc2	technika
přinášející	přinášející	k2eAgMnSc1d1	přinášející
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
snímky	snímek	k1gInPc4	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
mračen	mračna	k1gFnPc2	mračna
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
naskytla	naskytnout	k5eAaPmAgFnS	naskytnout
možnost	možnost	k1gFnSc1	možnost
jí	on	k3xPp3gFnSc2	on
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgNnSc1d1	dřívější
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlejší	světlý	k2eAgNnPc4d2	světlejší
mračna	mračno	k1gNnPc4	mračno
je	být	k5eAaImIp3nS	být
snazší	snadný	k2eAgNnSc1d2	snazší
objevit	objevit	k5eAaPmF	objevit
na	na	k7c6	na
tmavších	tmavý	k2eAgFnPc6d2	tmavší
severních	severní	k2eAgFnPc6d1	severní
částech	část	k1gFnPc6	část
planety	planeta	k1gFnSc2	planeta
namísto	namísto	k7c2	namísto
světlejších	světlý	k2eAgFnPc2d2	světlejší
jižních	jižní	k2eAgFnPc2d1	jižní
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
neukázalo	ukázat	k5eNaPmAgNnS	ukázat
jako	jako	k9	jako
správné	správný	k2eAgNnSc1d1	správné
<g/>
,	,	kIx,	,
když	když	k8xS	když
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
skutečnému	skutečný	k2eAgInSc3d1	skutečný
nárůstu	nárůst	k1gInSc3	nárůst
množství	množství	k1gNnSc2	množství
mračen	mračna	k1gFnPc2	mračna
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c4	mezi
mračny	mračna	k1gFnPc4	mračna
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
polokoulích	polokoule	k1gFnPc6	polokoule
Uranu	Uran	k1gInSc2	Uran
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
mračna	mračna	k1gFnSc1	mračna
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnSc4d2	menší
<g/>
,	,	kIx,	,
ostřejší	ostrý	k2eAgFnSc4d2	ostřejší
a	a	k8xC	a
světlejší	světlý	k2eAgFnSc4d2	světlejší
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mračna	mračna	k1gFnSc1	mračna
zřejmě	zřejmě	k6eAd1	zřejmě
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
výšce	výška	k1gFnSc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
životnosti	životnost	k1gFnSc2	životnost
mračen	mračna	k1gFnPc2	mračna
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
několika	několik	k4yIc2	několik
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
malá	malý	k2eAgNnPc1d1	malé
mračna	mračno	k1gNnPc1	mračno
zaniknou	zaniknout	k5eAaPmIp3nP	zaniknout
po	po	k7c6	po
několika	několik	k4yIc6	několik
hodinách	hodina	k1gFnPc6	hodina
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jedno	jeden	k4xCgNnSc1	jeden
mračno	mračno	k1gNnSc1	mračno
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
přeletu	přelet	k1gInSc2	přelet
Voyageru	Voyager	k1gInSc2	Voyager
až	až	k6eAd1	až
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgNnPc4d1	současné
pozorování	pozorování	k1gNnPc4	pozorování
taktéž	taktéž	k?	taktéž
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uranovská	uranovský	k2eAgNnPc4d1	uranovský
mračna	mračno	k1gNnPc4	mračno
mají	mít	k5eAaImIp3nP	mít
mnoho	mnoho	k4c4	mnoho
podobných	podobný	k2eAgFnPc2d1	podobná
charakteristik	charakteristika	k1gFnPc2	charakteristika
s	s	k7c7	s
mraky	mrak	k1gInPc7	mrak
na	na	k7c6	na
Neptunu	Neptun	k1gInSc6	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
tmavé	tmavý	k2eAgFnPc1d1	tmavá
skvrny	skvrna	k1gFnPc1	skvrna
časté	častý	k2eAgFnPc1d1	častá
na	na	k7c6	na
Neptunu	Neptun	k1gInSc6	Neptun
nebyly	být	k5eNaImAgInP	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
pozorovány	pozorován	k2eAgFnPc4d1	pozorována
na	na	k7c6	na
Uranu	Uran	k1gInSc6	Uran
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
prvnímu	první	k4xOgNnSc3	první
pozorování	pozorování	k1gNnSc3	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Uran	Uran	k1gInSc1	Uran
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
více	hodně	k6eAd2	hodně
podobat	podobat	k5eAaImF	podobat
Neptunu	Neptun	k1gInSc2	Neptun
během	během	k7c2	během
přiblížení	přiblížení	k1gNnSc2	přiblížení
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
pohybů	pohyb	k1gInPc2	pohyb
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
mračen	mračna	k1gFnPc2	mračna
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
určit	určit	k5eAaPmF	určit
zonální	zonální	k2eAgNnSc1d1	zonální
proudění	proudění	k1gNnSc1	proudění
ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
části	část	k1gFnSc6	část
troposféry	troposféra	k1gFnSc2	troposféra
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
rovníků	rovník	k1gInPc2	rovník
se	se	k3xPyFc4	se
větry	vítr	k1gInPc7	vítr
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
retrográdně	retrográdně	k6eAd1	retrográdně
<g/>
,	,	kIx,	,
proudí	proudit	k5eAaImIp3nP	proudit
proti	proti	k7c3	proti
rotaci	rotace	k1gFnSc3	rotace
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
rychlost	rychlost	k1gFnSc1	rychlost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
mezi	mezi	k7c7	mezi
50	[number]	k4	50
až	až	k9	až
100	[number]	k4	100
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Rychlost	rychlost	k1gFnSc1	rychlost
větrů	vítr	k1gInPc2	vítr
roste	růst	k5eAaImIp3nS	růst
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
nulové	nulový	k2eAgFnPc1d1	nulová
rychlosti	rychlost	k1gFnPc1	rychlost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kolem	kolem	k7c2	kolem
±	±	k?	±
<g/>
20	[number]	k4	20
<g/>
°	°	k?	°
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
teplota	teplota	k1gFnSc1	teplota
troposféry	troposféra	k1gFnSc2	troposféra
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
<g/>
.	.	kIx.	.
</s>
<s>
Blíže	blíže	k1gFnSc1	blíže
k	k	k7c3	k
pólům	pól	k1gInPc3	pól
se	se	k3xPyFc4	se
směr	směr	k1gInSc1	směr
větrů	vítr	k1gInPc2	vítr
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c6	na
prográdní	prográdní	k2eAgFnSc6d1	prográdní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
mají	mít	k5eAaImIp3nP	mít
stejný	stejný	k2eAgInSc1d1	stejný
směr	směr	k1gInSc1	směr
jako	jako	k8xC	jako
rotace	rotace	k1gFnSc1	rotace
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
proudění	proudění	k1gNnSc2	proudění
zde	zde	k6eAd1	zde
roste	růst	k5eAaImIp3nS	růst
až	až	k9	až
na	na	k7c4	na
maximální	maximální	k2eAgFnPc4d1	maximální
hodnoty	hodnota	k1gFnPc4	hodnota
okolo	okolo	k7c2	okolo
±	±	k?	±
<g/>
60	[number]	k4	60
<g/>
°	°	k?	°
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
pak	pak	k6eAd1	pak
klesá	klesat	k5eAaImIp3nS	klesat
na	na	k7c4	na
nulu	nula	k1gFnSc4	nula
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
-	-	kIx~	-
<g/>
°	°	k?	°
šířky	šířka	k1gFnSc2	šířka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
mezi	mezi	k7c7	mezi
150	[number]	k4	150
až	až	k9	až
200	[number]	k4	200
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
límec	límec	k1gInSc1	límec
je	být	k5eAaImIp3nS	být
rovnoběžný	rovnoběžný	k2eAgInSc1d1	rovnoběžný
s	s	k7c7	s
prouděním	proudění	k1gNnSc7	proudění
větrů	vítr	k1gInPc2	vítr
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
<g/>
,	,	kIx,	,
nedokážeme	dokázat	k5eNaPmIp1nP	dokázat
změřit	změřit	k5eAaPmF	změřit
rychlost	rychlost	k1gFnSc4	rychlost
proudění	proudění	k1gNnSc2	proudění
větrů	vítr	k1gInPc2	vítr
mezi	mezi	k7c7	mezi
tímto	tento	k3xDgInSc7	tento
límcem	límec	k1gInSc7	límec
a	a	k8xC	a
jižním	jižní	k2eAgInSc7d1	jižní
pólem	pól	k1gInSc7	pól
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
je	být	k5eAaImIp3nS	být
proudění	proudění	k1gNnSc1	proudění
možné	možný	k2eAgNnSc1d1	možné
měřit	měřit	k5eAaImF	měřit
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
240	[number]	k4	240
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
okolo	okolo	k7c2	okolo
+50	+50	k4	+50
<g/>
°	°	k?	°
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
mezi	mezi	k7c7	mezi
březnem	březen	k1gInSc7	březen
až	až	k8xS	až
květnem	květen	k1gInSc7	květen
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
mračen	mračno	k1gNnPc2	mračno
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
způsobily	způsobit	k5eAaPmAgInP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Uran	Uran	k1gMnSc1	Uran
podobal	podobat	k5eAaImAgMnS	podobat
více	hodně	k6eAd2	hodně
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
byly	být	k5eAaImAgInP	být
pozorována	pozorován	k2eAgFnSc1d1	pozorována
rychlost	rychlost	k1gFnSc1	rychlost
proudících	proudící	k2eAgInPc2d1	proudící
větrů	vítr	k1gInPc2	vítr
dosahující	dosahující	k2eAgFnSc4d1	dosahující
až	až	k8xS	až
229	[number]	k4	229
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
824	[number]	k4	824
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
a	a	k8xC	a
mohutné	mohutný	k2eAgFnSc2d1	mohutná
bouře	bouř	k1gFnSc2	bouř
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
výzkumníkům	výzkumník	k1gMnPc3	výzkumník
ze	z	k7c2	z
Space	Space	k1gFnSc2	Space
Science	Science	k1gFnSc2	Science
Institute	institut	k1gInSc5	institut
(	(	kIx(	(
<g/>
Boulder	Bouldra	k1gFnPc2	Bouldra
<g/>
,	,	kIx,	,
CO	co	k3yQnSc1	co
<g/>
)	)	kIx)	)
a	a	k8xC	a
University	universita	k1gFnSc2	universita
of	of	k?	of
Wisconsin	Wisconsin	k1gMnSc1	Wisconsin
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Uranu	Uran	k1gInSc2	Uran
pozorovat	pozorovat	k5eAaImF	pozorovat
tmavou	tmavý	k2eAgFnSc4d1	tmavá
skvrnu	skvrna	k1gFnSc4	skvrna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
astronomům	astronom	k1gMnPc3	astronom
příležitost	příležitost	k1gFnSc4	příležitost
lépe	dobře	k6eAd2	dobře
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
jeho	jeho	k3xOp3gFnSc4	jeho
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
náhlému	náhlý	k2eAgInSc3d1	náhlý
vzestupu	vzestup	k1gInSc3	vzestup
aktivity	aktivita	k1gFnSc2	aktivita
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
vysvětleno	vysvětlen	k2eAgNnSc1d1	vysvětleno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výsledek	výsledek	k1gInSc1	výsledek
extrémního	extrémní	k2eAgNnSc2d1	extrémní
axiálního	axiální	k2eAgNnSc2d1	axiální
naklonění	naklonění	k1gNnSc2	naklonění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
sezónní	sezónní	k2eAgFnSc1d1	sezónní
variace	variace	k1gFnSc1	variace
počasí	počasí	k1gNnSc2	počasí
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Určit	určit	k5eAaPmF	určit
přesný	přesný	k2eAgInSc4d1	přesný
důvod	důvod	k1gInSc4	důvod
těchto	tento	k3xDgFnPc2	tento
sezónních	sezónní	k2eAgFnPc2d1	sezónní
změn	změna	k1gFnPc2	změna
je	být	k5eAaImIp3nS	být
složité	složitý	k2eAgNnSc1d1	složité
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vědci	vědec	k1gMnPc1	vědec
zatím	zatím	k6eAd1	zatím
nemají	mít	k5eNaImIp3nP	mít
údaje	údaj	k1gInPc1	údaj
o	o	k7c4	o
chování	chování	k1gNnSc4	chování
atmosféry	atmosféra	k1gFnSc2	atmosféra
planety	planeta	k1gFnSc2	planeta
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
celého	celý	k2eAgInSc2d1	celý
jednoho	jeden	k4xCgMnSc4	jeden
jejího	její	k3xOp3gInSc2	její
oběhu	oběh	k1gInSc2	oběh
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Fotometrie	fotometrie	k1gFnSc1	fotometrie
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
půlky	půlka	k1gFnSc2	půlka
Uranova	Uranův	k2eAgInSc2d1	Uranův
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
počínaje	počínaje	k7c7	počínaje
50	[number]	k4	50
<g/>
.	.	kIx.	.
lety	let	k1gInPc7	let
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
ukázala	ukázat	k5eAaPmAgFnS	ukázat
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
jasu	jas	k1gInSc6	jas
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
spektrálních	spektrální	k2eAgNnPc6d1	spektrální
pásmech	pásmo	k1gNnPc6	pásmo
s	s	k7c7	s
maximy	maximum	k1gNnPc7	maximum
vyskytujícími	vyskytující	k2eAgMnPc7d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
období	období	k1gNnSc6	období
slunovratu	slunovrat	k1gInSc2	slunovrat
a	a	k8xC	a
minimy	minimum	k1gNnPc7	minimum
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
nastávají	nastávat	k5eAaImIp3nP	nastávat
během	během	k7c2	během
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
periodické	periodický	k2eAgFnPc1d1	periodická
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
době	doba	k1gFnSc6	doba
slunovratu	slunovrat	k1gInSc2	slunovrat
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mikrovlnného	mikrovlnný	k2eAgNnSc2d1	mikrovlnné
záření	záření	k1gNnSc2	záření
vycházejícího	vycházející	k2eAgInSc2d1	vycházející
ze	z	k7c2	z
spodních	spodní	k2eAgFnPc2d1	spodní
vrstev	vrstva	k1gFnPc2	vrstva
troposféry	troposféra	k1gFnSc2	troposféra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nP	měřit
od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
teploty	teplota	k1gFnSc2	teplota
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
i	i	k9	i
maximální	maximální	k2eAgFnSc1d1	maximální
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
změřena	změřit	k5eAaPmNgFnS	změřit
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
v	v	k7c6	v
období	období	k1gNnSc6	období
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
náznaků	náznak	k1gInPc2	náznak
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
na	na	k7c6	na
Uranu	Uran	k1gInSc6	Uran
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
sezónním	sezónní	k2eAgFnPc3d1	sezónní
variacím	variace	k1gFnPc3	variace
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
jižní	jižní	k2eAgFnSc1d1	jižní
polokoule	polokoule	k1gFnSc1	polokoule
je	být	k5eAaImIp3nS	být
světlejší	světlý	k2eAgFnSc1d2	světlejší
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
tmavší	tmavý	k2eAgFnSc1d2	tmavší
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
však	však	k9	však
nelze	lze	k6eNd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
modelem	model	k1gInSc7	model
sezónních	sezónní	k2eAgFnPc2d1	sezónní
změn	změna	k1gFnPc2	změna
zmiňovaným	zmiňovaný	k2eAgInSc7d1	zmiňovaný
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
minulého	minulý	k2eAgInSc2d1	minulý
slunovratu	slunovrat	k1gInSc2	slunovrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
jevil	jevit	k5eAaImAgInS	jevit
Uran	Uran	k1gInSc1	Uran
v	v	k7c6	v
několika	několik	k4yIc6	několik
stupních	stupeň	k1gInPc6	stupeň
jasu	jas	k1gInSc2	jas
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
severní	severní	k2eAgFnSc1d1	severní
oblast	oblast	k1gFnSc1	oblast
nebyla	být	k5eNaImAgFnS	být
vždy	vždy	k6eAd1	vždy
tak	tak	k6eAd1	tak
tmavá	tmavý	k2eAgFnSc1d1	tmavá
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
viditelný	viditelný	k2eAgInSc1d1	viditelný
pól	pól	k1gInSc1	pól
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
slunovratu	slunovrat	k1gInSc2	slunovrat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
nejdále	daleko	k6eAd3	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Podrobný	podrobný	k2eAgInSc1d1	podrobný
rozbor	rozbor	k1gInSc1	rozbor
viditelného	viditelný	k2eAgNnSc2d1	viditelné
a	a	k8xC	a
mikrovlnného	mikrovlnný	k2eAgNnSc2d1	mikrovlnné
záření	záření	k1gNnSc2	záření
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
periodické	periodický	k2eAgFnPc1d1	periodická
změny	změna	k1gFnPc1	změna
jasnosti	jasnost	k1gFnSc2	jasnost
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
symetrické	symetrický	k2eAgInPc1d1	symetrický
kolem	kolem	k7c2	kolem
slunovratů	slunovrat	k1gInPc2	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Uran	Uran	k1gMnSc1	Uran
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
době	doba	k1gFnSc6	doba
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
,	,	kIx,	,
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
a	a	k8xC	a
pozemní	pozemní	k2eAgFnPc1d1	pozemní
observatoře	observatoř	k1gFnPc1	observatoř
pozorovaly	pozorovat	k5eAaImAgFnP	pozorovat
malý	malý	k2eAgInSc4d1	malý
pokles	pokles	k1gInSc4	pokles
světlosti	světlost	k1gFnSc2	světlost
jižní	jižní	k2eAgFnSc2d1	jižní
čepičky	čepička	k1gFnSc2	čepička
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
límce	límec	k1gInSc2	límec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zůstal	zůstat	k5eAaPmAgInS	zůstat
stejně	stejně	k6eAd1	stejně
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
naopak	naopak	k6eAd1	naopak
aktivita	aktivita	k1gFnSc1	aktivita
narostla	narůst	k5eAaPmAgFnS	narůst
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
přítomnost	přítomnost	k1gFnSc1	přítomnost
mračen	mračna	k1gFnPc2	mračna
a	a	k8xC	a
zvedla	zvednout	k5eAaPmAgFnS	zvednout
se	se	k3xPyFc4	se
síla	síla	k1gFnSc1	síla
větrného	větrný	k2eAgNnSc2d1	větrné
proudění	proudění	k1gNnSc2	proudění
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
sezónních	sezónní	k2eAgFnPc2d1	sezónní
změn	změna	k1gFnPc2	změna
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
letního	letní	k2eAgInSc2d1	letní
a	a	k8xC	a
zimního	zimní	k2eAgInSc2d1	zimní
slunovratu	slunovrat	k1gInSc2	slunovrat
leží	ležet	k5eAaImIp3nS	ležet
polokoule	polokoule	k1gFnSc1	polokoule
Uranu	Uran	k1gInSc2	Uran
střídavě	střídavě	k6eAd1	střídavě
přivráceny	přivrátit	k5eAaPmNgInP	přivrátit
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
či	či	k8xC	či
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
hlubokého	hluboký	k2eAgInSc2d1	hluboký
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Zjasnění	zjasnění	k1gNnSc1	zjasnění
přivrácené	přivrácený	k2eAgFnSc2d1	přivrácená
polokoule	polokoule	k1gFnSc2	polokoule
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
je	být	k5eAaImIp3nS	být
vysvětlováno	vysvětlován	k2eAgNnSc1d1	vysvětlováno
změnou	změna	k1gFnSc7	změna
tloušťky	tloušťka	k1gFnSc2	tloušťka
metanových	metanův	k2eAgNnPc2d1	metanův
mračen	mračno	k1gNnPc2	mračno
a	a	k8xC	a
mlhové	mlhový	k2eAgFnSc2d1	mlhová
vrstvy	vrstva	k1gFnSc2	vrstva
v	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Světlý	světlý	k2eAgInSc1d1	světlý
límec	límec	k1gInSc1	límec
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
-45	-45	k4	-45
<g/>
°	°	k?	°
šířky	šířka	k1gFnSc2	šířka
je	být	k5eAaImIp3nS	být
taktéž	taktéž	k?	taktéž
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
metanových	metanův	k2eAgNnPc2d1	metanův
mračen	mračno	k1gNnPc2	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
změny	změna	k1gFnPc1	změna
probíhající	probíhající	k2eAgFnPc1d1	probíhající
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vysvětleny	vysvětlit	k5eAaPmNgInP	vysvětlit
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
vrstvách	vrstva	k1gFnPc6	vrstva
mračen	mračna	k1gFnPc2	mračna
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
vyzařování	vyzařování	k1gNnSc2	vyzařování
mikrovlnného	mikrovlnný	k2eAgNnSc2d1	mikrovlnné
záření	záření	k1gNnSc2	záření
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
planety	planeta	k1gFnSc2	planeta
mohou	moct	k5eAaImIp3nP	moct
zapříčiňovat	zapříčiňovat	k5eAaImF	zapříčiňovat
různé	různý	k2eAgNnSc4d1	různé
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
měnit	měnit	k5eAaImF	měnit
cirkulaci	cirkulace	k1gFnSc4	cirkulace
v	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
může	moct	k5eAaImIp3nS	moct
opět	opět	k6eAd1	opět
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
proudění	proudění	k1gNnSc2	proudění
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Prstence	prstenec	k1gInSc2	prstenec
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Uranův	Uranův	k2eAgInSc1d1	Uranův
systém	systém	k1gInSc1	systém
planetárních	planetární	k2eAgInPc2d1	planetární
prstenců	prstenec	k1gInPc2	prstenec
je	být	k5eAaImIp3nS	být
nezřetelný	zřetelný	k2eNgInSc1d1	nezřetelný
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
13	[number]	k4	13
dosud	dosud	k6eAd1	dosud
objevených	objevený	k2eAgInPc2d1	objevený
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
tenké	tenký	k2eAgInPc1d1	tenký
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
z	z	k7c2	z
tmavých	tmavý	k2eAgInPc2d1	tmavý
balvanů	balvan	k1gInPc2	balvan
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
od	od	k7c2	od
10	[number]	k4	10
cm	cm	kA	cm
do	do	k7c2	do
30	[number]	k4	30
m	m	kA	m
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
prstenců	prstenec	k1gInPc2	prstenec
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
tenká	tenký	k2eAgFnSc1d1	tenká
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
nemohly	moct	k5eNaImAgFnP	moct
existovat	existovat	k5eAaImF	existovat
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
tzv.	tzv.	kA	tzv.
pastýřských	pastýřský	k2eAgInPc2d1	pastýřský
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
poblíž	poblíž	k7c2	poblíž
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
částice	částice	k1gFnPc1	částice
v	v	k7c6	v
prstenci	prstenec	k1gInSc6	prstenec
<g/>
,	,	kIx,	,
a	a	k8xC	a
drží	držet	k5eAaImIp3nP	držet
tak	tak	k9	tak
prstence	prstenec	k1gInPc1	prstenec
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
objevit	objevit	k5eAaPmF	objevit
dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
:	:	kIx,	:
Cordelia	Cordelius	k1gMnSc4	Cordelius
a	a	k8xC	a
Ophelia	Ophelius	k1gMnSc4	Ophelius
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
pastýřské	pastýřský	k2eAgInPc1d1	pastýřský
měsíce	měsíc	k1gInPc1	měsíc
jsou	být	k5eAaImIp3nP	být
pravděpodobné	pravděpodobný	k2eAgInPc1d1	pravděpodobný
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
prstence	prstenec	k1gInPc1	prstenec
Uranu	Uran	k1gInSc2	Uran
tvoří	tvořit	k5eAaImIp3nP	tvořit
13	[number]	k4	13
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
prstence	prstenec	k1gInPc1	prstenec
1986	[number]	k4	1986
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
ζ	ζ	k?	ζ
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
α	α	k?	α
<g/>
,	,	kIx,	,
β	β	k?	β
<g/>
,	,	kIx,	,
η	η	k?	η
<g/>
,	,	kIx,	,
γ	γ	k?	γ
<g/>
,	,	kIx,	,
δ	δ	k?	δ
<g/>
,	,	kIx,	,
λ	λ	k?	λ
<g/>
,	,	kIx,	,
ε	ε	k?	ε
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ν	ν	k?	ν
a	a	k8xC	a
μ	μ	k?	μ
Jejich	jejich	k3xOp3gInPc1	jejich
poloměry	poloměr	k1gInPc1	poloměr
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
38	[number]	k4	38
000	[number]	k4	000
km	km	kA	km
u	u	k7c2	u
prstence	prstenec	k1gInSc2	prstenec
1986	[number]	k4	1986
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
ζ	ζ	k?	ζ
až	až	k9	až
k	k	k7c3	k
98	[number]	k4	98
000	[number]	k4	000
km	km	kA	km
u	u	k7c2	u
prstence	prstenec	k1gInSc2	prstenec
μ	μ	k?	μ
Prstence	prstenec	k1gInSc2	prstenec
jsou	být	k5eAaImIp3nP	být
extrémně	extrémně	k6eAd1	extrémně
tmavé	tmavý	k2eAgFnPc1d1	tmavá
a	a	k8xC	a
odrazivost	odrazivost	k1gFnSc1	odrazivost
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
<g/>
,	,	kIx,	,
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
obohaceným	obohacený	k2eAgInSc7d1	obohacený
neznámou	známý	k2eNgFnSc7d1	neznámá
tmavou	tmavý	k2eAgFnSc7d1	tmavá
organickou	organický	k2eAgFnSc7d1	organická
látkou	látka	k1gFnSc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Prstence	prstenec	k1gInPc1	prstenec
Uranu	Uran	k1gInSc2	Uran
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
neprůhledné	průhledný	k2eNgNnSc4d1	neprůhledné
a	a	k8xC	a
široké	široký	k2eAgNnSc4d1	široké
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
části	část	k1gFnSc2	část
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
obvykle	obvykle	k6eAd1	obvykle
decimetrovými	decimetrový	k2eAgInPc7d1	decimetrový
až	až	k8xS	až
metrovými	metrový	k2eAgInPc7d1	metrový
balvany	balvan	k1gInPc7	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prstence	prstenec	k1gInPc1	prstenec
jsou	být	k5eAaImIp3nP	být
opticky	opticky	k6eAd1	opticky
tenčí	tenký	k2eAgFnPc1d2	tenčí
<g/>
:	:	kIx,	:
široké	široký	k2eAgFnPc1d1	široká
a	a	k8xC	a
slabé	slabý	k2eAgInPc1d1	slabý
prstence	prstenec	k1gInPc1	prstenec
1986	[number]	k4	1986
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
ζ	ζ	k?	ζ
<g/>
,	,	kIx,	,
μ	μ	k?	μ
a	a	k8xC	a
ν	ν	k?	ν
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
malými	malý	k2eAgFnPc7d1	malá
prachovými	prachový	k2eAgFnPc7d1	prachová
částicemi	částice	k1gFnPc7	částice
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
úzký	úzký	k2eAgInSc4d1	úzký
prstenec	prstenec	k1gInSc4	prstenec
λ	λ	k?	λ
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
velká	velký	k2eAgNnPc4d1	velké
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Relativní	relativní	k2eAgInSc1d1	relativní
nedostatek	nedostatek	k1gInSc1	nedostatek
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
prstencích	prstenec	k1gInPc6	prstenec
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
aerodynamickým	aerodynamický	k2eAgInSc7d1	aerodynamický
odporem	odpor	k1gInSc7	odpor
částic	částice	k1gFnPc2	částice
vznikajících	vznikající	k2eAgFnPc2d1	vznikající
v	v	k7c6	v
koróně	koróna	k1gFnSc6	koróna
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
prstence	prstenec	k1gInPc1	prstenec
Uranu	Uran	k1gInSc2	Uran
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
mladé	mladý	k2eAgMnPc4d1	mladý
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
stáří	stáří	k1gNnSc4	stáří
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
600	[number]	k4	600
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Prstence	prstenec	k1gInPc1	prstenec
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kolizemi	kolize	k1gFnPc7	kolize
menších	malý	k2eAgInPc2d2	menší
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
kdysi	kdysi	k6eAd1	kdysi
obíhaly	obíhat	k5eAaImAgInP	obíhat
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
srážce	srážka	k1gFnSc6	srážka
se	se	k3xPyFc4	se
měsíce	měsíc	k1gInPc1	měsíc
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rozpadly	rozpadnout	k5eAaPmAgInP	rozpadnout
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
menších	malý	k2eAgFnPc2d2	menší
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
přežily	přežít	k5eAaPmAgFnP	přežít
jako	jako	k9	jako
úzké	úzký	k2eAgFnPc1d1	úzká
a	a	k8xC	a
opticky	opticky	k6eAd1	opticky
husté	hustý	k2eAgInPc1d1	hustý
prstence	prstenec	k1gInPc1	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
prstence	prstenec	k1gInPc1	prstenec
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
náhodou	náhodou	k6eAd1	náhodou
při	při	k7c6	při
zákrytu	zákryt	k1gInSc6	zákryt
hvězdy	hvězda	k1gFnSc2	hvězda
Uranem	Uran	k1gInSc7	Uran
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1977	[number]	k4	1977
Jamesem	James	k1gMnSc7	James
L.	L.	kA	L.
Elliotem	Elliot	k1gMnSc7	Elliot
<g/>
,	,	kIx,	,
Edwardem	Edward	k1gMnSc7	Edward
W.	W.	kA	W.
Dunhamem	Dunham	k1gInSc7	Dunham
a	a	k8xC	a
Douglasem	Douglas	k1gMnSc7	Douglas
J.	J.	kA	J.
Minkem	mink	k1gMnSc7	mink
v	v	k7c6	v
Kuiperově	Kuiperův	k2eAgFnSc6d1	Kuiperova
letadlové	letadlový	k2eAgFnSc6d1	letadlová
observatoři	observatoř	k1gFnSc6	observatoř
(	(	kIx(	(
<g/>
Kuiper	Kuiper	k1gInSc1	Kuiper
Airborne	Airborn	k1gInSc5	Airborn
Observatory	Observator	k1gMnPc4	Observator
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
sondou	sonda	k1gFnSc7	sonda
Voyager	Voyagra	k1gFnPc2	Voyagra
2	[number]	k4	2
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
zpozoroval	zpozorovat	k5eAaPmAgInS	zpozorovat
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2003	[number]	k4	2003
další	další	k2eAgInPc4d1	další
slabé	slabý	k2eAgInPc4d1	slabý
prachové	prachový	k2eAgInPc4d1	prachový
prstence	prstenec	k1gInPc4	prstenec
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2008	[number]	k4	2008
následovaly	následovat	k5eAaImAgFnP	následovat
další	další	k2eAgInSc4d1	další
objevy	objev	k1gInPc7	objev
pozemskými	pozemský	k2eAgInPc7d1	pozemský
teleskopy	teleskop	k1gInPc7	teleskop
i	i	k8xC	i
Hubbleovým	Hubbleův	k2eAgInSc7d1	Hubbleův
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měsíce	měsíc	k1gInSc2	měsíc
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
má	mít	k5eAaImIp3nS	mít
27	[number]	k4	27
známých	známý	k2eAgInPc2d1	známý
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pět	pět	k4xCc4	pět
hlavních	hlavní	k2eAgInPc2d1	hlavní
patří	patřit	k5eAaImIp3nS	patřit
Miranda	Miranda	k1gFnSc1	Miranda
<g/>
,	,	kIx,	,
Ariel	Ariel	k1gInSc1	Ariel
<g/>
,	,	kIx,	,
Umbriel	Umbriel	k1gInSc1	Umbriel
<g/>
,	,	kIx,	,
Titania	Titanium	k1gNnPc1	Titanium
a	a	k8xC	a
Oberon	Oberon	k1gInSc1	Oberon
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
Titania	Titanium	k1gNnPc4	Titanium
a	a	k8xC	a
Oberon	Oberon	k1gInSc4	Oberon
s	s	k7c7	s
průměry	průměr	k1gInPc7	průměr
přes	přes	k7c4	přes
1500	[number]	k4	1500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
měsíce	měsíc	k1gInPc1	měsíc
jsou	být	k5eAaImIp3nP	být
málo	málo	k6eAd1	málo
jasné	jasný	k2eAgFnPc1d1	jasná
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
šlo	jít	k5eAaImAgNnS	jít
pozorovat	pozorovat	k5eAaImF	pozorovat
běžnými	běžný	k2eAgInPc7d1	běžný
dalekohledy	dalekohled	k1gInPc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Měsíce	měsíc	k1gInPc1	měsíc
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nazývají	nazývat	k5eAaImIp3nP	nazývat
rodiny	rodina	k1gFnPc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
měsíce	měsíc	k1gInPc1	měsíc
s	s	k7c7	s
pravidelnými	pravidelný	k2eAgFnPc7d1	pravidelná
drahami	draha	k1gFnPc7	draha
obíhají	obíhat	k5eAaImIp3nP	obíhat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
planety	planeta	k1gFnSc2	planeta
po	po	k7c6	po
kruhových	kruhový	k2eAgFnPc6d1	kruhová
drahách	draha	k1gFnPc6	draha
ležících	ležící	k2eAgFnPc2d1	ležící
téměř	téměř	k6eAd1	téměř
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
rovníku	rovník	k1gInSc2	rovník
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
rodiny	rodina	k1gFnSc2	rodina
patří	patřit	k5eAaImIp3nS	patřit
Ophelia	Ophelia	k1gFnSc1	Ophelia
<g/>
,	,	kIx,	,
Bianca	Bianca	k1gFnSc1	Bianca
<g/>
,	,	kIx,	,
Cressida	Cressida	k1gFnSc1	Cressida
<g/>
,	,	kIx,	,
Desdemona	Desdemona	k1gFnSc1	Desdemona
<g/>
,	,	kIx,	,
Juliet	Juliet	k1gInSc1	Juliet
<g/>
,	,	kIx,	,
Portia	Portia	k1gFnSc1	Portia
<g/>
,	,	kIx,	,
Rosalind	Rosalinda	k1gFnPc2	Rosalinda
<g/>
,	,	kIx,	,
Belinda	Belinda	k1gFnSc1	Belinda
<g/>
,	,	kIx,	,
Puck	Puck	k1gInSc1	Puck
<g/>
,	,	kIx,	,
Perdita	Perdita	k1gFnSc1	Perdita
<g/>
,	,	kIx,	,
Mab	Mab	k1gFnSc1	Mab
a	a	k8xC	a
Cupid	Cupid	k1gInSc1	Cupid
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
leží	ležet	k5eAaImIp3nP	ležet
mezi	mezi	k7c7	mezi
Uranovými	Uranův	k2eAgInPc7d1	Uranův
prstenci	prstenec	k1gInPc7	prstenec
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgInPc1d1	vnější
měsíce	měsíc	k1gInPc1	měsíc
s	s	k7c7	s
pravidelnými	pravidelný	k2eAgFnPc7d1	pravidelná
drahami	draha	k1gFnPc7	draha
obíhají	obíhat	k5eAaImIp3nP	obíhat
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
až	až	k9	až
za	za	k7c7	za
prstenci	prstenec	k1gInPc7	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
dráhy	dráha	k1gFnPc1	dráha
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
prakticky	prakticky	k6eAd1	prakticky
kruhové	kruhový	k2eAgInPc1d1	kruhový
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
téměř	téměř	k6eAd1	téměř
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
rodiny	rodina	k1gFnSc2	rodina
patří	patřit	k5eAaImIp3nP	patřit
největší	veliký	k2eAgInPc4d3	veliký
Uranovy	Uranův	k2eAgInPc4d1	Uranův
měsíce	měsíc	k1gInPc4	měsíc
Miranda	Mirando	k1gNnSc2	Mirando
<g/>
,	,	kIx,	,
Ariel	Ariel	k1gInSc1	Ariel
<g/>
,	,	kIx,	,
Umbriel	Umbriel	k1gInSc1	Umbriel
<g/>
,	,	kIx,	,
Titania	Titanium	k1gNnPc1	Titanium
a	a	k8xC	a
Oberon	Oberon	k1gInSc1	Oberon
<g/>
.	.	kIx.	.
</s>
<s>
Měsíce	měsíc	k1gInPc1	měsíc
s	s	k7c7	s
nepravidelnými	pravidelný	k2eNgFnPc7d1	nepravidelná
drahami	draha	k1gFnPc7	draha
obíhají	obíhat	k5eAaImIp3nP	obíhat
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
po	po	k7c6	po
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
výstředných	výstředný	k2eAgFnPc6d1	výstředná
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
o	o	k7c4	o
zachycená	zachycený	k2eAgNnPc4d1	zachycené
transneptunická	transneptunický	k2eAgNnPc4d1	transneptunické
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
rodiny	rodina	k1gFnSc2	rodina
patří	patřit	k5eAaImIp3nS	patřit
Caliban	Calibany	k1gInPc2	Calibany
<g/>
,	,	kIx,	,
Stephano	Stephana	k1gFnSc5	Stephana
<g/>
,	,	kIx,	,
Trinculo	Trincula	k1gFnSc5	Trincula
<g/>
,	,	kIx,	,
Sycorax	Sycorax	k1gInSc1	Sycorax
<g/>
,	,	kIx,	,
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
,	,	kIx,	,
Prospero	Prospero	k1gNnSc1	Prospero
<g/>
,	,	kIx,	,
Setebos	Setebos	k1gMnSc1	Setebos
<g/>
,	,	kIx,	,
Francisco	Francisco	k1gMnSc1	Francisco
a	a	k8xC	a
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
velkých	velký	k2eAgInPc2d1	velký
Uranových	Uranův	k2eAgInPc2d1	Uranův
měsíců	měsíc	k1gInPc2	měsíc
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xS	jako
vznikaly	vznikat	k5eAaImAgFnP	vznikat
kamenné	kamenný	k2eAgFnPc4d1	kamenná
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
však	však	k9	však
Uran	Uran	k1gInSc1	Uran
velmi	velmi	k6eAd1	velmi
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
z	z	k7c2	z
fází	fáze	k1gFnPc2	fáze
vzniku	vznik	k1gInSc2	vznik
měsíců	měsíc	k1gInPc2	měsíc
nevystoupila	vystoupit	k5eNaPmAgFnS	vystoupit
teplota	teplota	k1gFnSc1	teplota
na	na	k7c4	na
vysoké	vysoký	k2eAgFnPc4d1	vysoká
hodnoty	hodnota	k1gFnPc4	hodnota
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
okolí	okolí	k1gNnSc2	okolí
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
tak	tak	k6eAd1	tak
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
lehce	lehko	k6eAd1	lehko
tavitelných	tavitelný	k2eAgFnPc2d1	tavitelná
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
disku	disk	k1gInSc2	disk
okolo	okolo	k7c2	okolo
vznikající	vznikající	k2eAgFnSc2d1	vznikající
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
malých	malý	k2eAgInPc2d1	malý
měsíců	měsíc	k1gInPc2	měsíc
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
asteroidy	asteroid	k1gInPc4	asteroid
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
Uranem	Uran	k1gInSc7	Uran
zachyceny	zachytit	k5eAaPmNgFnP	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
dva	dva	k4xCgInPc1	dva
měsíce	měsíc	k1gInPc1	měsíc
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
ještě	ještě	k9	ještě
objevitelem	objevitel	k1gMnSc7	objevitel
Uranu	Uran	k1gInSc2	Uran
Williamem	William	k1gInSc7	William
Herschelem	Herschel	k1gMnSc7	Herschel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
jen	jen	k6eAd1	jen
5	[number]	k4	5
největších	veliký	k2eAgInPc2d3	veliký
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
planety	planeta	k1gFnSc2	planeta
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíchž	jejíž	k3xOyRp3gInPc6	jejíž
snímcích	snímek	k1gInPc6	snímek
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
jedenáct	jedenáct	k4xCc1	jedenáct
dalších	další	k2eAgInPc2d1	další
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
měsíce	měsíc	k1gInPc1	měsíc
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
až	až	k6eAd1	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
velmi	velmi	k6eAd1	velmi
dobrých	dobrý	k2eAgFnPc2d1	dobrá
pozorovacích	pozorovací	k2eAgFnPc2d1	pozorovací
podmínek	podmínka	k1gFnPc2	podmínka
na	na	k7c6	na
tmavé	tmavý	k2eAgFnSc6d1	tmavá
obloze	obloha	k1gFnSc6	obloha
a	a	k8xC	a
při	při	k7c6	při
znalosti	znalost	k1gFnSc6	znalost
jeho	jeho	k3xOp3gFnSc2	jeho
polohy	poloha	k1gFnSc2	poloha
lze	lze	k6eAd1	lze
Uran	Uran	k1gInSc4	Uran
pozorovat	pozorovat	k5eAaImF	pozorovat
i	i	k8xC	i
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Jasnost	jasnost	k1gFnSc1	jasnost
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
5,7	[number]	k4	5,7
mag	mag	k?	mag
a	a	k8xC	a
5,9	[number]	k4	5,9
mag	mag	k?	mag
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
v	v	k7c6	v
době	doba	k1gFnSc6	doba
oposice	oposice	k1gFnSc2	oposice
Úhlový	úhlový	k2eAgInSc1d1	úhlový
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
3,4	[number]	k4	3,4
a	a	k8xC	a
3,7	[number]	k4	3,7
obloukové	obloukový	k2eAgFnSc2d1	oblouková
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
.	.	kIx.	.
</s>
<s>
Malými	malý	k2eAgInPc7d1	malý
dalekohledy	dalekohled	k1gInPc7	dalekohled
mezi	mezi	k7c4	mezi
15	[number]	k4	15
až	až	k9	až
23	[number]	k4	23
cm	cm	kA	cm
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
světlý	světlý	k2eAgInSc4d1	světlý
azurový	azurový	k2eAgInSc4d1	azurový
disk	disk	k1gInSc4	disk
s	s	k7c7	s
tmavšími	tmavý	k2eAgFnPc7d2	tmavší
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
dalekohledy	dalekohled	k1gInPc7	dalekohled
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
25	[number]	k4	25
cm	cm	kA	cm
a	a	k8xC	a
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
mračna	mračno	k1gNnPc4	mračno
a	a	k8xC	a
větší	veliký	k2eAgInPc4d2	veliký
měsíce	měsíc	k1gInPc4	měsíc
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Titanii	Titanie	k1gFnSc4	Titanie
a	a	k8xC	a
Oberon	Oberon	k1gInSc4	Oberon
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
Uran	Uran	k1gInSc1	Uran
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Ryb	Ryby	k1gFnPc2	Ryby
<g/>
.	.	kIx.	.
</s>
<s>
Planetu	planeta	k1gFnSc4	planeta
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
pomocí	pomocí	k7c2	pomocí
dalekohledu	dalekohled	k1gInSc2	dalekohled
vlastní	vlastní	k2eAgFnSc2d1	vlastní
výroby	výroba	k1gFnSc2	výroba
anglický	anglický	k2eAgMnSc1d1	anglický
astronom	astronom	k1gMnSc1	astronom
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
William	William	k1gInSc1	William
Herschel	Herschel	k1gInSc1	Herschel
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
byl	být	k5eAaImAgInS	být
první	první	k4xOgFnSc7	první
objevenou	objevený	k2eAgFnSc7d1	objevená
planetou	planeta	k1gFnSc7	planeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k9	již
od	od	k7c2	od
dávných	dávný	k2eAgMnPc2d1	dávný
časů	čas	k1gInPc2	čas
<g/>
,	,	kIx,	,
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
mnoha	mnoho	k4c6	mnoho
příležitostech	příležitost	k1gFnPc6	příležitost
pozorován	pozorovat	k5eAaImNgInS	pozorovat
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
byl	být	k5eAaImAgInS	být
mylně	mylně	k6eAd1	mylně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
další	další	k2eAgFnSc4d1	další
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
zaznamenané	zaznamenaný	k2eAgNnSc1d1	zaznamenané
pozorování	pozorování	k1gNnSc1	pozorování
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
anglický	anglický	k2eAgMnSc1d1	anglický
astronom	astronom	k1gMnSc1	astronom
John	John	k1gMnSc1	John
Flamsteed	Flamsteed	k1gMnSc1	Flamsteed
katalogizoval	katalogizovat	k5eAaImAgMnS	katalogizovat
jako	jako	k9	jako
34	[number]	k4	34
<g/>
.	.	kIx.	.
hvězdu	hvězda	k1gFnSc4	hvězda
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Tauri	Taur	k1gFnSc2	Taur
<g/>
.	.	kIx.	.
</s>
<s>
Herschel	Herschet	k5eAaImAgMnS	Herschet
nově	nově	k6eAd1	nově
objevenou	objevený	k2eAgFnSc4d1	objevená
planetu	planeta	k1gFnSc4	planeta
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
původně	původně	k6eAd1	původně
Georgium	Georgium	k1gNnSc1	Georgium
Sidus	Sidus	k1gInSc1	Sidus
(	(	kIx(	(
<g/>
Hvězda	hvězda	k1gFnSc1	hvězda
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
<g/>
)	)	kIx)	)
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
však	však	k9	však
mimo	mimo	k7c4	mimo
Británii	Británie	k1gFnSc4	Británie
neujalo	ujmout	k5eNaPmAgNnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Jeromeho	Jerome	k1gMnSc2	Jerome
Lalanda	Lalando	k1gNnSc2	Lalando
jej	on	k3xPp3gMnSc4	on
francouzští	francouzský	k2eAgMnPc1d1	francouzský
astronomové	astronom	k1gMnPc1	astronom
začali	začít	k5eAaPmAgMnP	začít
nazývat	nazývat	k5eAaImF	nazývat
Herschel	Herschel	k1gInSc4	Herschel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Němec	Němec	k1gMnSc1	Němec
Johann	Johann	k1gMnSc1	Johann
Bode	bůst	k5eAaImIp3nS	bůst
prosadil	prosadit	k5eAaPmAgMnS	prosadit
jméno	jméno	k1gNnSc4	jméno
Uran	Uran	k1gInSc1	Uran
po	po	k7c6	po
řeckém	řecký	k2eAgMnSc6d1	řecký
bohu	bůh	k1gMnSc6	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
nejstarších	starý	k2eAgNnPc2d3	nejstarší
vydání	vydání	k1gNnPc2	vydání
časopisu	časopis	k1gInSc2	časopis
Monthly	Monthly	k1gFnSc2	Monthly
Notices	Notices	k1gMnSc1	Notices
of	of	k?	of
the	the	k?	the
Royal	Royal	k1gInSc4	Royal
Astronomical	Astronomical	k1gMnSc2	Astronomical
Society	societa	k1gFnSc2	societa
(	(	kIx(	(
<g/>
Měsíční	měsíční	k2eAgFnPc1d1	měsíční
poznámky	poznámka	k1gFnPc1	poznámka
Královské	královský	k2eAgFnSc2d1	královská
astronomické	astronomický	k2eAgFnPc4d1	astronomická
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
minimálně	minimálně	k6eAd1	minimálně
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
a	a	k8xC	a
možná	možná	k9	možná
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
mezi	mezi	k7c7	mezi
britskými	britský	k2eAgMnPc7d1	britský
astronomy	astronom	k1gMnPc7	astronom
název	název	k1gInSc4	název
Uran	Uran	k1gMnSc1	Uran
již	již	k6eAd1	již
běžný	běžný	k2eAgMnSc1d1	běžný
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Georgium	Georgium	k1gNnSc1	Georgium
Sidus	Sidus	k1gMnSc1	Sidus
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
the	the	k?	the
Georgian	Georgian	k1gInSc1	Georgian
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
Brity	Brit	k1gMnPc4	Brit
občas	občas	k6eAd1	občas
užíván	užíván	k2eAgMnSc1d1	užíván
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
se	se	k3xPyFc4	se
udržel	udržet	k5eAaPmAgInS	udržet
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
v	v	k7c6	v
HM	HM	k?	HM
Nautical	Nautical	k1gMnSc1	Nautical
Almanac	Almanac	k1gFnSc4	Almanac
Office	Office	kA	Office
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
název	název	k1gInSc4	název
Uran	Uran	k1gInSc4	Uran
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
kosmických	kosmický	k2eAgInPc2d1	kosmický
letů	let	k1gInPc2	let
zkoumán	zkoumán	k2eAgInSc4d1	zkoumán
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
pouze	pouze	k6eAd1	pouze
jedinou	jediný	k2eAgFnSc7d1	jediná
planetární	planetární	k2eAgFnSc7d1	planetární
sondou	sonda	k1gFnSc7	sonda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tak	tak	k6eAd1	tak
přinesla	přinést	k5eAaPmAgFnS	přinést
většinu	většina	k1gFnSc4	většina
poznatků	poznatek	k1gInPc2	poznatek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
lidstvo	lidstvo	k1gNnSc1	lidstvo
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
planetě	planeta	k1gFnSc6	planeta
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
americká	americký	k2eAgFnSc1d1	americká
planetární	planetární	k2eAgFnSc1d1	planetární
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
okolo	okolo	k7c2	okolo
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgNnSc1d3	nejbližší
přiblížení	přiblížení	k1gNnSc1	přiblížení
k	k	k7c3	k
Uranu	Uran	k1gInSc3	Uran
nastalo	nastat	k5eAaPmAgNnS	nastat
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sonda	sonda	k1gFnSc1	sonda
nacházela	nacházet	k5eAaImAgFnS	nacházet
81	[number]	k4	81
500	[number]	k4	500
km	km	kA	km
nad	nad	k7c7	nad
horní	horní	k2eAgFnSc7d1	horní
vrstvou	vrstva	k1gFnSc7	vrstva
Uranovy	Uranův	k2eAgFnSc2d1	Uranova
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
sonda	sonda	k1gFnSc1	sonda
objevila	objevit	k5eAaPmAgFnS	objevit
10	[number]	k4	10
dříve	dříve	k6eAd2	dříve
neznámých	známý	k2eNgInPc2d1	neznámý
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
studovala	studovat	k5eAaImAgFnS	studovat
unikátní	unikátní	k2eAgFnSc4d1	unikátní
atmosféru	atmosféra	k1gFnSc4	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
prozkoumala	prozkoumat	k5eAaPmAgFnS	prozkoumat
prstence	prstenec	k1gInPc4	prstenec
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
určit	určit	k5eAaPmF	určit
přesnou	přesný	k2eAgFnSc4d1	přesná
rotační	rotační	k2eAgFnSc4d1	rotační
dobu	doba	k1gFnSc4	doba
planety	planeta	k1gFnSc2	planeta
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
sonda	sonda	k1gFnSc1	sonda
odeslala	odeslat	k5eAaPmAgFnS	odeslat
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
okolo	okolo	k7c2	okolo
8	[number]	k4	8
000	[number]	k4	000
fotografií	fotografia	k1gFnPc2	fotografia
Uranu	Uran	k1gInSc2	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
podrobně	podrobně	k6eAd1	podrobně
studoval	studovat	k5eAaImAgMnS	studovat
rotaci	rotace	k1gFnSc3	rotace
třetí	třetí	k4xOgFnSc2	třetí
největší	veliký	k2eAgFnSc2d3	veliký
planety	planeta	k1gFnSc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnPc2	pozorování
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
otočí	otočit	k5eAaPmIp3nS	otočit
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
za	za	k7c4	za
17	[number]	k4	17
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
14	[number]	k4	14
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
že	že	k8xS	že
současně	současně	k6eAd1	současně
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
rotuje	rotovat	k5eAaImIp3nS	rotovat
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
rotace	rotace	k1gFnSc2	rotace
položenou	položená	k1gFnSc4	položená
do	do	k7c2	do
roviny	rovina	k1gFnSc2	rovina
oběhu	oběh	k1gInSc2	oběh
(	(	kIx(	(
<g/>
zdánlivě	zdánlivě	k6eAd1	zdánlivě
tedy	tedy	k9	tedy
planeta	planeta	k1gFnSc1	planeta
"	"	kIx"	"
<g/>
válí	válet	k5eAaImIp3nP	válet
sudy	sud	k1gInPc4	sud
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejpozoruhodnějších	pozoruhodný	k2eAgInPc2d3	nejpozoruhodnější
důsledků	důsledek	k1gInPc2	důsledek
Uranovy	Uranův	k2eAgFnSc2d1	Uranova
polohy	poloha	k1gFnSc2	poloha
na	na	k7c6	na
boku	bok	k1gInSc6	bok
je	být	k5eAaImIp3nS	být
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
ohon	ohon	k1gInSc4	ohon
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
samo	sám	k3xTgNnSc1	sám
skloněno	sklonit	k5eAaPmNgNnS	sklonit
o	o	k7c4	o
60	[number]	k4	60
stupňů	stupeň	k1gInPc2	stupeň
od	od	k7c2	od
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Ohon	ohon	k1gInSc1	ohon
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
je	být	k5eAaImIp3nS	být
zkroucený	zkroucený	k2eAgMnSc1d1	zkroucený
rotací	rotace	k1gFnSc7	rotace
planety	planeta	k1gFnSc2	planeta
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
vývrtky	vývrtka	k1gFnSc2	vývrtka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příletem	přílet	k1gInSc7	přílet
Voyageru	Voyager	k1gInSc2	Voyager
2	[number]	k4	2
nebylo	být	k5eNaImAgNnS	být
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
Uranu	Uran	k1gMnSc3	Uran
nic	nic	k3yNnSc1	nic
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
pozorovala	pozorovat	k5eAaImAgFnS	pozorovat
i	i	k9	i
radiační	radiační	k2eAgInPc4d1	radiační
pásy	pás	k1gInPc4	pás
okolo	okolo	k7c2	okolo
Uranu	Uran	k1gInSc2	Uran
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnPc1d1	podobná
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
pozorovány	pozorovat	k5eAaImNgFnP	pozorovat
u	u	k7c2	u
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
i	i	k9	i
zde	zde	k6eAd1	zde
sonda	sonda	k1gFnSc1	sonda
využila	využít	k5eAaPmAgFnS	využít
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc3	působení
planety	planeta	k1gFnSc2	planeta
pro	pro	k7c4	pro
korekci	korekce	k1gFnSc4	korekce
své	svůj	k3xOyFgFnSc2	svůj
dráhy	dráha	k1gFnSc2	dráha
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Neptunu	Neptun	k1gInSc3	Neptun
a	a	k8xC	a
k	k	k7c3	k
nabrání	nabrání	k1gNnSc3	nabrání
potřebné	potřebný	k2eAgFnSc2d1	potřebná
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Uranu	Uran	k1gInSc6	Uran
se	se	k3xPyFc4	se
určitě	určitě	k6eAd1	určitě
nemůže	moct	k5eNaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
život	život	k1gInSc1	život
podobný	podobný	k2eAgInSc1d1	podobný
pozemskému	pozemský	k2eAgInSc3d1	pozemský
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Uran	Uran	k1gInSc1	Uran
nemá	mít	k5eNaImIp3nS	mít
pevný	pevný	k2eAgInSc4d1	pevný
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Případné	případný	k2eAgInPc1d1	případný
hypotetické	hypotetický	k2eAgInPc1d1	hypotetický
živé	živý	k2eAgInPc1d1	živý
organismy	organismus	k1gInPc1	organismus
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
musely	muset	k5eAaImAgInP	muset
vznášet	vznášet	k5eAaImF	vznášet
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
hlavně	hlavně	k6eAd1	hlavně
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
velice	velice	k6eAd1	velice
nízké	nízký	k2eAgFnPc1d1	nízká
teploty	teplota	k1gFnPc1	teplota
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
případná	případný	k2eAgFnSc1d1	případná
obyvatelná	obyvatelný	k2eAgFnSc1d1	obyvatelná
zóna	zóna	k1gFnSc1	zóna
nacházet	nacházet	k5eAaImF	nacházet
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
vzniku	vznik	k1gInSc3	vznik
života	život	k1gInSc2	život
pak	pak	k6eAd1	pak
hovoří	hovořit	k5eAaImIp3nS	hovořit
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
potenciálně	potenciálně	k6eAd1	potenciálně
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
by	by	kYmCp3nP	by
panovaly	panovat	k5eAaImAgInP	panovat
extrémní	extrémní	k2eAgInPc1d1	extrémní
tlaky	tlak	k1gInPc1	tlak
způsobené	způsobený	k2eAgInPc1d1	způsobený
nadložními	nadložní	k2eAgFnPc7d1	nadložní
vrstvami	vrstva	k1gFnPc7	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
život	život	k1gInSc1	život
musel	muset	k5eAaImAgInS	muset
adaptovat	adaptovat	k5eAaBmF	adaptovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
možnost	možnost	k1gFnSc1	možnost
života	život	k1gInSc2	život
na	na	k7c6	na
Uranu	Uran	k1gInSc6	Uran
jako	jako	k8xS	jako
nereálná	reálný	k2eNgFnSc1d1	nereálná
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgInS	být
Uran	Uran	k1gInSc1	Uran
objeven	objevit	k5eAaPmNgInS	objevit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
astrologii	astrologie	k1gFnSc6	astrologie
svůj	svůj	k3xOyFgInSc4	svůj
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
dodatečně	dodatečně	k6eAd1	dodatečně
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
zakomponování	zakomponování	k1gNnSc4	zakomponování
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
astrologie	astrologie	k1gFnSc2	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
objevení	objevení	k1gNnSc6	objevení
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
astrologové	astrolog	k1gMnPc1	astrolog
začali	začít	k5eAaPmAgMnP	začít
po	po	k7c6	po
významu	význam	k1gInSc6	význam
Uranu	Uran	k1gInSc2	Uran
pídit	pídit	k5eAaImF	pídit
<g/>
.	.	kIx.	.
</s>
<s>
Proslul	proslout	k5eAaPmAgMnS	proslout
tím	ten	k3xDgNnSc7	ten
zejména	zejména	k9	zejména
anglický	anglický	k2eAgMnSc1d1	anglický
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
nadšený	nadšený	k2eAgMnSc1d1	nadšený
astrolog	astrolog	k1gMnSc1	astrolog
John	John	k1gMnSc1	John
Varley	Varlea	k1gMnSc2	Varlea
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sledoval	sledovat	k5eAaImAgMnS	sledovat
působení	působení	k1gNnSc3	působení
Uranu	Uran	k1gInSc2	Uran
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
typech	typ	k1gInPc6	typ
horoskopů	horoskop	k1gInPc2	horoskop
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
historka	historka	k1gFnSc1	historka
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
napjatém	napjatý	k2eAgNnSc6d1	napjaté
očekávání	očekávání	k1gNnSc6	očekávání
<g/>
,	,	kIx,	,
spojeném	spojený	k2eAgNnSc6d1	spojené
s	s	k7c7	s
přechodem	přechod	k1gInSc7	přechod
Uranu	Uran	k1gInSc2	Uran
přes	přes	k7c4	přes
důležitá	důležitý	k2eAgNnPc4d1	důležité
místa	místo	k1gNnPc4	místo
Varleyho	Varley	k1gMnSc2	Varley
horoskopu	horoskop	k1gInSc2	horoskop
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
skončilo	skončit	k5eAaPmAgNnS	skončit
požárem	požár	k1gInSc7	požár
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Varley	Varlea	k1gFnPc1	Varlea
jako	jako	k8xC	jako
jediný	jediný	k2eAgInSc1d1	jediný
měl	mít	k5eAaImAgInS	mít
z	z	k7c2	z
katastrofy	katastrofa	k1gFnSc2	katastrofa
údajně	údajně	k6eAd1	údajně
téměř	téměř	k6eAd1	téměř
radost	radost	k1gFnSc4	radost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
jeho	jeho	k3xOp3gInPc4	jeho
dosavadní	dosavadní	k2eAgInPc4d1	dosavadní
závěry	závěr	k1gInPc4	závěr
-	-	kIx~	-
že	že	k8xS	že
Uran	Uran	k1gInSc1	Uran
zvěstuje	zvěstovat	k5eAaImIp3nS	zvěstovat
náhlé	náhlý	k2eAgNnSc4d1	náhlé
<g/>
,	,	kIx,	,
nečekané	čekaný	k2eNgNnSc4d1	nečekané
a	a	k8xC	a
radikální	radikální	k2eAgNnSc4d1	radikální
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
požáry	požár	k1gInPc7	požár
<g/>
,	,	kIx,	,
úrazy	úraz	k1gInPc1	úraz
apod.	apod.	kA	apod.
Moderní	moderní	k2eAgMnPc1d1	moderní
astrologové	astrolog	k1gMnPc1	astrolog
tento	tento	k3xDgInSc4	tento
Uranův	Uranův	k2eAgInSc4d1	Uranův
význam	význam	k1gInSc4	význam
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
a	a	k8xC	a
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
spojitost	spojitost	k1gFnSc4	spojitost
Uranu	Uran	k1gInSc2	Uran
s	s	k7c7	s
elektřinou	elektřina	k1gFnSc7	elektřina
a	a	k8xC	a
jejími	její	k3xOp3gInPc7	její
projevy	projev	k1gInPc7	projev
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
i	i	k9	i
s	s	k7c7	s
létáním	létání	k1gNnSc7	létání
(	(	kIx(	(
<g/>
a	a	k8xC	a
leteckými	letecký	k2eAgFnPc7d1	letecká
katastrofami	katastrofa	k1gFnPc7	katastrofa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
poněkud	poněkud	k6eAd1	poněkud
zmírnili	zmírnit	k5eAaPmAgMnP	zmírnit
"	"	kIx"	"
<g/>
zlovolnou	zlovolný	k2eAgFnSc4d1	zlovolná
<g/>
"	"	kIx"	"
povahu	povaha	k1gFnSc4	povaha
planety	planeta	k1gFnSc2	planeta
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
prudké	prudký	k2eAgFnPc1d1	prudká
změny	změna	k1gFnPc1	změna
vyvolané	vyvolaný	k2eAgFnPc1d1	vyvolaná
Uranem	Uran	k1gInSc7	Uran
sice	sice	k8xC	sice
postižení	postižený	k1gMnPc1	postižený
nesou	nést	k5eAaImIp3nP	nést
těžce	těžce	k6eAd1	těžce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
posunu	posun	k1gInSc3	posun
a	a	k8xC	a
růstu	růst	k1gInSc3	růst
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
odbourávají	odbourávat	k5eAaImIp3nP	odbourávat
staré	starý	k2eAgFnPc1d1	stará
<g/>
,	,	kIx,	,
nefunkční	funkční	k2eNgFnPc1d1	nefunkční
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
byli	být	k5eAaImAgMnP	být
lidé	člověk	k1gMnPc1	člověk
nemístně	místně	k6eNd1	místně
závislí	závislý	k2eAgMnPc1d1	závislý
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
tématy	téma	k1gNnPc7	téma
spojenými	spojený	k2eAgInPc7d1	spojený
s	s	k7c7	s
astrologickým	astrologický	k2eAgInSc7d1	astrologický
Uranem	Uran	k1gInSc7	Uran
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
moderní	moderní	k2eAgInPc4d1	moderní
vědecké	vědecký	k2eAgInPc4d1	vědecký
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
planetami	planeta	k1gFnPc7	planeta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
již	již	k6eAd1	již
nejsou	být	k5eNaImIp3nP	být
viditelné	viditelný	k2eAgFnSc2d1	viditelná
prostým	prostý	k2eAgNnSc7d1	prosté
okem	oke	k1gNnSc7	oke
a	a	k8xC	a
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
Uran	Uran	k1gInSc1	Uran
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
transpersonální	transpersonální	k2eAgFnSc2d1	transpersonální
<g/>
"	"	kIx"	"
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
připisuje	připisovat	k5eAaImIp3nS	připisovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
tedy	tedy	k9	tedy
v	v	k7c4	v
moderní	moderní	k2eAgFnSc4d1	moderní
astrologii	astrologie	k1gFnSc4	astrologie
spojitost	spojitost	k1gFnSc1	spojitost
s	s	k7c7	s
proměnami	proměna	k1gFnPc7	proměna
celých	celý	k2eAgNnPc2d1	celé
společenství	společenství	k1gNnPc2	společenství
a	a	k8xC	a
s	s	k7c7	s
dlouhodobými	dlouhodobý	k2eAgInPc7d1	dlouhodobý
společenskými	společenský	k2eAgInPc7d1	společenský
procesy	proces	k1gInPc7	proces
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
působením	působení	k1gNnSc7	působení
většinou	většinou	k6eAd1	většinou
vrcholí	vrcholit	k5eAaImIp3nS	vrcholit
náhle	náhle	k6eAd1	náhle
<g/>
,	,	kIx,	,
revolučně	revolučně	k6eAd1	revolučně
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
téma	téma	k1gNnSc4	téma
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
osvobození	osvobození	k1gNnSc2	osvobození
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgMnPc1d1	moderní
astrologové	astrolog	k1gMnPc1	astrolog
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
i	i	k9	i
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Uran	Uran	k1gInSc1	Uran
objeven	objevit	k5eAaPmNgInS	objevit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
příznačně	příznačně	k6eAd1	příznačně
žijící	žijící	k2eAgMnSc1d1	žijící
právě	právě	k6eAd1	právě
tématy	téma	k1gNnPc7	téma
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
systému	systém	k1gInSc6	systém
"	"	kIx"	"
<g/>
vládců	vládce	k1gMnPc2	vládce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každému	každý	k3xTgMnSc3	každý
ze	z	k7c2	z
znamení	znamení	k1gNnSc2	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
vládne	vládnout	k5eAaImIp3nS	vládnout
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
připisují	připisovat	k5eAaImIp3nP	připisovat
moderní	moderní	k2eAgMnPc1d1	moderní
astrologové	astrolog	k1gMnPc1	astrolog
Uranu	Uran	k1gInSc2	Uran
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
znamením	znamení	k1gNnSc7	znamení
Vodnáře	vodnář	k1gMnSc2	vodnář
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
astrologii	astrologie	k1gFnSc6	astrologie
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
planetou	planeta	k1gFnSc7	planeta
Saturn	Saturn	k1gMnSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gMnSc1	Uran
nelákal	lákat	k5eNaImAgMnS	lákat
autory	autor	k1gMnPc4	autor
sci-fi	scii	k1gFnSc2	sci-fi
zdaleka	zdaleka	k6eAd1	zdaleka
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
obří	obří	k2eAgInSc4d1	obří
Jupiter	Jupiter	k1gInSc4	Jupiter
nebo	nebo	k8xC	nebo
Saturn	Saturn	k1gInSc4	Saturn
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
nezůstal	zůstat	k5eNaPmAgMnS	zůstat
stranou	stranou	k6eAd1	stranou
jejich	jejich	k3xOp3gInSc2	jejich
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
však	však	k9	však
jen	jen	k9	jen
epizodní	epizodní	k2eAgFnSc7d1	epizodní
součástí	součást	k1gFnSc7	součást
děje	dít	k5eAaImIp3nS	dít
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
populárním	populární	k2eAgInSc6d1	populární
seriálu	seriál	k1gInSc6	seriál
Doctor	Doctor	k1gMnSc1	Doctor
Who	Who	k1gMnSc1	Who
(	(	kIx(	(
<g/>
produkovaného	produkovaný	k2eAgMnSc2d1	produkovaný
britskou	britský	k2eAgFnSc7d1	britská
televizí	televize	k1gFnSc7	televize
BBC	BBC	kA	BBC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
je	být	k5eAaImIp3nS	být
Uran	Uran	k1gInSc4	Uran
jediné	jediný	k2eAgNnSc1d1	jediné
místo	místo	k1gNnSc1	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
minerál	minerál	k1gInSc1	minerál
taranium	taranium	k1gNnSc1	taranium
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
pravidla	pravidlo	k1gNnSc2	pravidlo
je	být	k5eAaImIp3nS	být
dánsko-americký	dánskomerický	k2eAgInSc1d1	dánsko-americký
film	film	k1gInSc1	film
Journey	Journea	k1gFnSc2	Journea
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Seventh	Seventh	k1gInSc1	Seventh
Planet	planeta	k1gFnPc2	planeta
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
sedmé	sedmý	k4xOgFnSc3	sedmý
planetě	planeta	k1gFnSc3	planeta
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
nehostinného	hostinný	k2eNgInSc2d1	nehostinný
a	a	k8xC	a
studeného	studený	k2eAgInSc2d1	studený
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prší	pršet	k5eAaImIp3nS	pršet
čpavek	čpavek	k1gInSc1	čpavek
a	a	k8xC	a
methan	methan	k1gInSc1	methan
<g/>
,	,	kIx,	,
objeví	objevit	k5eAaPmIp3nP	objevit
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
idylickou	idylický	k2eAgFnSc4d1	idylická
pozemskou	pozemský	k2eAgFnSc4d1	pozemská
krajinu	krajina	k1gFnSc4	krajina
s	s	k7c7	s
bublajícími	bublající	k2eAgInPc7d1	bublající
potůčky	potůček	k1gInPc7	potůček
<g/>
,	,	kIx,	,
krásnými	krásný	k2eAgFnPc7d1	krásná
ženami	žena	k1gFnPc7	žena
<g/>
,	,	kIx,	,
hustými	hustý	k2eAgInPc7d1	hustý
<g/>
,	,	kIx,	,
bujnými	bujný	k2eAgInPc7d1	bujný
lesy	les	k1gInPc7	les
a	a	k8xC	a
stromy	strom	k1gInPc7	strom
obsypanými	obsypaný	k2eAgInPc7d1	obsypaný
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Dozvíme	dozvědět	k5eAaPmIp1nP	dozvědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
členové	člen	k1gMnPc1	člen
posádky	posádka	k1gFnSc2	posádka
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
konfrontováni	konfrontovat	k5eAaBmNgMnP	konfrontovat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
vlastními	vlastní	k2eAgFnPc7d1	vlastní
představami	představa	k1gFnPc7	představa
a	a	k8xC	a
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
rajská	rajský	k2eAgFnSc1d1	rajská
krajina	krajina	k1gFnSc1	krajina
je	být	k5eAaImIp3nS	být
pouhou	pouhý	k2eAgFnSc7d1	pouhá
iluzí	iluze	k1gFnSc7	iluze
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
stojí	stát	k5eAaImIp3nS	stát
tajemná	tajemný	k2eAgFnSc1d1	tajemná
super-bytost	superytost	k1gFnSc1	super-bytost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zhmotňovat	zhmotňovat	k5eAaImF	zhmotňovat
představy	představa	k1gFnPc4	představa
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
Uran	Uran	k1gMnSc1	Uran
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
málo	málo	k1gNnSc4	málo
<g/>
:	:	kIx,	:
např.	např.	kA	např.
v	v	k7c6	v
oceněném	oceněný	k2eAgInSc6d1	oceněný
románu	román	k1gInSc6	román
Larry	Larra	k1gMnSc2	Larra
Nivena	Niven	k1gMnSc2	Niven
A	a	k9	a
World	World	k1gMnSc1	World
Out	Out	k1gFnSc2	Out
of	of	k?	of
Time	Tim	k1gFnSc2	Tim
je	být	k5eAaImIp3nS	být
Uran	Uran	k1gInSc1	Uran
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xC	jako
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odsune	odsunout	k5eAaPmIp3nS	odsunout
Zemi	zem	k1gFnSc4	zem
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
zjasňujícího	zjasňující	k2eAgNnSc2d1	zjasňující
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
místem	místo	k1gNnSc7	místo
děje	děj	k1gInSc2	děj
stávají	stávat	k5eAaImIp3nP	stávat
místo	místo	k7c2	místo
Uranu	Uran	k1gInSc2	Uran
jeho	on	k3xPp3gInSc2	on
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c6	v
románu	román	k1gInSc6	román
a	a	k8xC	a
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
natočeného	natočený	k2eAgInSc2d1	natočený
filmu	film	k1gInSc2	film
ruského	ruský	k2eAgMnSc2d1	ruský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Sergeje	Sergej	k1gMnSc2	Sergej
I.	I.	kA	I.
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
Lunarnaja	Lunarnaj	k2eAgFnSc1d1	Lunarnaj
raduga	raduga	k1gFnSc1	raduga
(	(	kIx(	(
<g/>
Měsíční	měsíční	k2eAgFnSc1d1	měsíční
duha	duha	k1gFnSc1	duha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
kosmonauti	kosmonaut	k1gMnPc1	kosmonaut
na	na	k7c6	na
Oberonu	Oberon	k1gInSc6	Oberon
nakazí	nakazit	k5eAaPmIp3nS	nakazit
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
níž	jenž	k3xRgFnSc3	jenž
získají	získat	k5eAaPmIp3nP	získat
nadpřirozené	nadpřirozený	k2eAgFnPc4d1	nadpřirozená
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
