<s>
Charleyova	Charleyův	k2eAgFnSc1d1	Charleyova
teta	teta	k1gFnSc1	teta
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgFnSc1d1	divadelní
komedie	komedie	k1gFnSc1	komedie
anglického	anglický	k2eAgMnSc2d1	anglický
autora	autor	k1gMnSc2	autor
Brandona	Brandon	k1gMnSc2	Brandon
Thomase	Thomas	k1gMnSc2	Thomas
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
ji	on	k3xPp3gFnSc4	on
uvedlo	uvést	k5eAaPmAgNnS	uvést
poprvé	poprvé	k6eAd1	poprvé
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
má	mít	k5eAaImIp3nS	mít
tuto	tento	k3xDgFnSc4	tento
hru	hra	k1gFnSc4	hra
na	na	k7c6	na
repertoáru	repertoár	k1gInSc6	repertoár
mnoho	mnoho	k4c4	mnoho
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
legendární	legendární	k2eAgFnPc4d1	legendární
české	český	k2eAgFnPc4d1	Česká
inscenace	inscenace	k1gFnPc4	inscenace
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
patří	patřit	k5eAaImIp3nP	patřit
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
také	také	k9	také
ta	ten	k3xDgFnSc1	ten
z	z	k7c2	z
Městských	městský	k2eAgNnPc2d1	Městské
divadel	divadlo	k1gNnPc2	divadlo
pražských	pražský	k2eAgNnPc2d1	Pražské
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
ABC	ABC	kA	ABC
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
ztvárňoval	ztvárňovat	k5eAaImAgMnS	ztvárňovat
Lubomír	Lubomír	k1gMnSc1	Lubomír
Lipský	lipský	k2eAgMnSc1d1	lipský
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
studenti	student	k1gMnPc1	student
Oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
univerzity	univerzita	k1gFnSc2	univerzita
Charley	Charlea	k1gFnSc2	Charlea
a	a	k8xC	a
Jack	Jack	k1gMnSc1	Jack
chtějí	chtít	k5eAaImIp3nP	chtít
požádat	požádat	k5eAaPmF	požádat
své	svůj	k3xOyFgFnPc4	svůj
dívky	dívka	k1gFnPc4	dívka
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doba	doba	k1gFnSc1	doba
si	se	k3xPyFc3	se
žádá	žádat	k5eAaImIp3nS	žádat
doporučení	doporučení	k1gNnSc4	doporučení
solidní	solidní	k2eAgFnSc2d1	solidní
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
vrátit	vrátit	k5eAaPmF	vrátit
velmi	velmi	k6eAd1	velmi
bohatá	bohatý	k2eAgFnSc1d1	bohatá
Dona	dona	k1gFnSc1	dona
Lucie	Lucie	k1gFnSc2	Lucie
Dalvadores	Dalvadoresa	k1gFnPc2	Dalvadoresa
<g/>
,	,	kIx,	,
Charleyova	Charleyův	k2eAgFnSc1d1	Charleyova
teta	teta	k1gFnSc1	teta
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Slečny	slečna	k1gFnPc1	slečna
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
netrpělivé	trpělivý	k2eNgFnPc1d1	netrpělivá
a	a	k8xC	a
teta	teta	k1gFnSc1	teta
stále	stále	k6eAd1	stále
nikde	nikde	k6eAd1	nikde
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c4	za
tetu	teta	k1gFnSc4	teta
převleče	převléct	k5eAaPmIp3nS	převléct
jejich	jejich	k3xOp3gMnSc1	jejich
spolužák	spolužák	k1gMnSc1	spolužák
Babberly	Babberla	k1gFnSc2	Babberla
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
velmi	velmi	k6eAd1	velmi
komické	komický	k2eAgFnPc1d1	komická
situace	situace	k1gFnPc1	situace
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
i	i	k9	i
skutečná	skutečný	k2eAgFnSc1d1	skutečná
teta	teta	k1gFnSc1	teta
<g/>
.	.	kIx.	.
</s>
<s>
Hru	hra	k1gFnSc4	hra
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
na	na	k7c6	na
repertoáru	repertoár	k1gInSc6	repertoár
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
představení	představení	k1gNnPc2	představení
činí	činit	k5eAaImIp3nS	činit
2	[number]	k4	2
h	h	k?	h
55	[number]	k4	55
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
s	s	k7c7	s
jednou	jednou	k6eAd1	jednou
dvaceti	dvacet	k4xCc7	dvacet
minutovou	minutový	k2eAgFnSc7d1	minutová
přestávkou	přestávka	k1gFnSc7	přestávka
<g/>
.	.	kIx.	.
15.10	[number]	k4	15.10
<g/>
.2009	.2009	k4	.2009
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
premiéra	premiéra	k1gFnSc1	premiéra
hry	hra	k1gFnSc2	hra
na	na	k7c6	na
prknech	prkno	k1gNnPc6	prkno
pražského	pražský	k2eAgNnSc2d1	Pražské
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
Jezerce	Jezerka	k1gFnSc6	Jezerka
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Jana	Jan	k1gMnSc2	Jan
Hrušínského	Hrušínský	k2eAgMnSc2d1	Hrušínský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
sluhy	sluha	k1gMnSc2	sluha
Brasseta	Brasset	k1gMnSc2	Brasset
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Hrušínským	Hrušínský	k2eAgMnSc7d1	Hrušínský
a	a	k8xC	a
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Hruškou	Hruška	k1gMnSc7	Hruška
alternoval	alternovat	k5eAaImAgMnS	alternovat
nejslavnější	slavný	k2eAgMnSc1d3	nejslavnější
český	český	k2eAgMnSc1d1	český
představitel	představitel	k1gMnSc1	představitel
titulní	titulní	k2eAgFnSc2d1	titulní
role	role	k1gFnSc2	role
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
Lipský	lipský	k2eAgMnSc1d1	lipský
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
lorda	lord	k1gMnSc2	lord
Babberlyho	Babberly	k1gMnSc2	Babberly
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Radek	Radek	k1gMnSc1	Radek
Holub	Holub	k1gMnSc1	Holub
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
objevuje	objevovat	k5eAaImIp3nS	objevovat
Petr	Petr	k1gMnSc1	Petr
Vacek	Vacek	k1gMnSc1	Vacek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgFnPc6d1	další
postavách	postava	k1gFnPc6	postava
se	se	k3xPyFc4	se
za	za	k7c4	za
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
prostřídalo	prostřídat	k5eAaPmAgNnS	prostřídat
několik	několik	k4yIc4	několik
herců	herc	k1gInPc2	herc
<g/>
.	.	kIx.	.
</s>
<s>
Jacka	Jacka	k1gMnSc1	Jacka
Chesneyho	Chesney	k1gMnSc4	Chesney
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
například	například	k6eAd1	například
Roman	Roman	k1gMnSc1	Roman
Štabrňák	Štabrňák	k1gMnSc1	Štabrňák
<g/>
,	,	kIx,	,
Charleyho	Charley	k1gMnSc2	Charley
pak	pak	k8xC	pak
Lukáš	Lukáš	k1gMnSc1	Lukáš
Langmajer	Langmajer	k1gMnSc1	Langmajer
<g/>
.	.	kIx.	.
</s>
