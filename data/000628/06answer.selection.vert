<s>
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
byla	být	k5eAaImAgFnS	být
svedena	sveden	k2eAgFnSc1d1	svedena
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1278	[number]	k4	1278
(	(	kIx(	(
<g/>
na	na	k7c4	na
den	den	k1gInSc4	den
sv.	sv.	kA	sv.
Rufa	Ruf	k1gInSc2	Ruf
<g/>
)	)	kIx)	)
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
mezi	mezi	k7c7	mezi
vesnicemi	vesnice	k1gFnPc7	vesnice
Dürnkrut	Dürnkrut	k1gMnSc1	Dürnkrut
(	(	kIx(	(
<g/>
Suché	Suché	k2eAgFnSc2d1	Suché
Kruty	kruta	k1gFnSc2	kruta
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jedenspeigen	Jedenspeigen	k1gInSc1	Jedenspeigen
<g/>
,	,	kIx,	,
ležícími	ležící	k2eAgFnPc7d1	ležící
v	v	k7c6	v
Dolních	dolní	k2eAgInPc6d1	dolní
Rakousích	Rakousy	k1gInPc6	Rakousy
30	[number]	k4	30
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Břeclavi	Břeclav	k1gFnSc2	Břeclav
<g/>
.	.	kIx.	.
</s>
