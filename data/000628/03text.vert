<s>
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
byla	být	k5eAaImAgFnS	být
svedena	sveden	k2eAgFnSc1d1	svedena
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1278	[number]	k4	1278
(	(	kIx(	(
<g/>
na	na	k7c4	na
den	den	k1gInSc4	den
sv.	sv.	kA	sv.
Rufa	Ruf	k1gInSc2	Ruf
<g/>
)	)	kIx)	)
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
mezi	mezi	k7c7	mezi
vesnicemi	vesnice	k1gFnPc7	vesnice
Dürnkrut	Dürnkrut	k1gMnSc1	Dürnkrut
(	(	kIx(	(
<g/>
Suché	Suché	k2eAgFnSc2d1	Suché
Kruty	kruta	k1gFnSc2	kruta
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jedenspeigen	Jedenspeigen	k1gInSc1	Jedenspeigen
<g/>
,	,	kIx,	,
ležícími	ležící	k2eAgFnPc7d1	ležící
v	v	k7c6	v
Dolních	dolní	k2eAgInPc6d1	dolní
Rakousích	Rakousy	k1gInPc6	Rakousy
30	[number]	k4	30
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Břeclavi	Břeclav	k1gFnSc2	Břeclav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
stanula	stanout	k5eAaPmAgFnS	stanout
spojená	spojený	k2eAgFnSc1d1	spojená
vojska	vojsko	k1gNnSc2	vojsko
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Rudolfa	Rudolf	k1gMnSc2	Rudolf
I.	I.	kA	I.
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
a	a	k8xC	a
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Ladislava	Ladislav	k1gMnSc2	Ladislav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Kumána	Kumána	k1gFnSc1	Kumána
proti	proti	k7c3	proti
vojsku	vojsko	k1gNnSc3	vojsko
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
šlo	jít	k5eAaImAgNnS	jít
především	především	k9	především
o	o	k7c4	o
babenberské	babenberský	k2eAgNnSc4d1	babenberské
dědictví	dědictví	k1gNnSc4	dědictví
(	(	kIx(	(
<g/>
Rakousy	Rakousy	k1gInPc1	Rakousy
a	a	k8xC	a
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
původně	původně	k6eAd1	původně
získal	získat	k5eAaPmAgMnS	získat
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
něž	jenž	k3xRgInPc4	jenž
usiloval	usilovat	k5eAaImAgMnS	usilovat
také	také	k9	také
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
potomci	potomek	k1gMnPc1	potomek
stali	stát	k5eAaPmAgMnP	stát
říšskými	říšský	k2eAgMnPc7d1	říšský
knížaty	kníže	k1gMnPc7wR	kníže
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgMnPc7	jenž
dosud	dosud	k6eAd1	dosud
nebyli	být	k5eNaImAgMnP	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1276	[number]	k4	1276
byl	být	k5eAaImAgMnS	být
Přemysl	Přemysl	k1gMnSc1	Přemysl
nucen	nutit	k5eAaImNgMnS	nutit
uzavřít	uzavřít	k5eAaPmF	uzavřít
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
mír	mír	k1gInSc4	mír
a	a	k8xC	a
zříci	zříct	k5eAaPmF	zříct
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
všech	všecek	k3xTgFnPc2	všecek
nečeských	český	k2eNgFnPc2d1	nečeská
držav	država	k1gFnPc2	država
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
podmínky	podmínka	k1gFnPc4	podmínka
míru	mír	k1gInSc2	mír
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
a	a	k8xC	a
Habsburk	Habsburk	k1gMnSc1	Habsburk
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
začal	začít	k5eAaPmAgInS	začít
bezprávně	bezprávně	k6eAd1	bezprávně
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
záležitostí	záležitost	k1gFnPc2	záležitost
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
a	a	k8xC	a
podporovat	podporovat	k5eAaImF	podporovat
proti	proti	k7c3	proti
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
odbojnou	odbojný	k2eAgFnSc4d1	odbojná
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
jistě	jistě	k9	jistě
nevybral	vybrat	k5eNaPmAgMnS	vybrat
bojiště	bojiště	k1gNnSc4	bojiště
poblíž	poblíž	k7c2	poblíž
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
náhodně	náhodně	k6eAd1	náhodně
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
rozhodně	rozhodně	k6eAd1	rozhodně
vhodné	vhodný	k2eAgNnSc4d1	vhodné
bitevní	bitevní	k2eAgNnSc4d1	bitevní
pole	pole	k1gNnSc4	pole
a	a	k8xC	a
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgMnS	mít
jistě	jistě	k9	jistě
dobré	dobrý	k2eAgFnPc4d1	dobrá
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
Suchých	Suchých	k2eAgFnPc2d1	Suchých
Krut	kruta	k1gFnPc2	kruta
leží	ležet	k5eAaImIp3nS	ležet
místo	místo	k7c2	místo
jeho	on	k3xPp3gNnSc2	on
největšího	veliký	k2eAgNnSc2d3	veliký
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
,	,	kIx,	,
Kressenbrunn	Kressenbrunna	k1gFnPc2	Kressenbrunna
<g/>
.	.	kIx.	.
</s>
<s>
Bitvy	bitva	k1gFnPc4	bitva
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
dohromady	dohromady	k6eAd1	dohromady
cca	cca	kA	cca
55	[number]	k4	55
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
přesné	přesný	k2eAgInPc1d1	přesný
počty	počet	k1gInPc1	počet
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
střet	střet	k1gInSc1	střet
trval	trvat	k5eAaImAgInS	trvat
přibližně	přibližně	k6eAd1	přibližně
tři	tři	k4xCgFnPc1	tři
hodiny	hodina	k1gFnPc1	hodina
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Za	za	k7c4	za
heslo	heslo	k1gNnSc4	heslo
prohlášeno	prohlášen	k2eAgNnSc4d1	prohlášeno
u	u	k7c2	u
českého	český	k2eAgNnSc2d1	české
vojska	vojsko	k1gNnSc2	vojsko
<g/>
:	:	kIx,	:
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
!	!	kIx.	!
</s>
<s>
U	u	k7c2	u
nepřátel	nepřítel	k1gMnPc2	nepřítel
volalo	volat	k5eAaImAgNnS	volat
se	se	k3xPyFc4	se
<g/>
:	:	kIx,	:
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
V	v	k7c6	v
dlouho	dlouho	k6eAd1	dlouho
vyrovnaném	vyrovnaný	k2eAgInSc6d1	vyrovnaný
boji	boj	k1gInSc6	boj
se	se	k3xPyFc4	se
vítězství	vítězství	k1gNnSc1	vítězství
nakonec	nakonec	k6eAd1	nakonec
přiklonilo	přiklonit	k5eAaPmAgNnS	přiklonit
na	na	k7c4	na
habsburskou	habsburský	k2eAgFnSc4d1	habsburská
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgInSc1d1	habsburský
se	se	k3xPyFc4	se
prý	prý	k9	prý
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zachránil	zachránit	k5eAaPmAgMnS	zachránit
ho	on	k3xPp3gMnSc4	on
rytíř	rytíř	k1gMnSc1	rytíř
Walter	Walter	k1gMnSc1	Walter
z	z	k7c2	z
Ramschwagu	Ramschwag	k1gInSc2	Ramschwag
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
říšského	říšský	k2eAgNnSc2d1	říšské
vojska	vojsko	k1gNnSc2	vojsko
vedena	vést	k5eAaImNgNnP	vést
proti	proti	k7c3	proti
soudobým	soudobý	k2eAgNnPc3d1	soudobé
pravidlům	pravidlo	k1gNnPc3	pravidlo
rytířského	rytířský	k2eAgInSc2d1	rytířský
boje	boj	k1gInSc2	boj
(	(	kIx(	(
<g/>
právě	právě	k9	právě
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
rytířství	rytířství	k1gNnSc2	rytířství
objevuje	objevovat	k5eAaImIp3nS	objevovat
vždy	vždy	k6eAd1	vždy
Přemysl	Přemysl	k1gMnSc1	Přemysl
<g/>
,	,	kIx,	,
ne	ne	k9	ne
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
)	)	kIx)	)
-	-	kIx~	-
nerozhodla	rozhodnout	k5eNaPmAgFnS	rozhodnout
ji	on	k3xPp3gFnSc4	on
proslulá	proslulý	k2eAgFnSc1d1	proslulá
zrada	zrada	k1gFnSc1	zrada
českých	český	k2eAgMnPc2d1	český
pánů	pan	k1gMnPc2	pan
(	(	kIx(	(
<g/>
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většina	většina	k1gFnSc1	většina
ani	ani	k8xC	ani
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
bitvy	bitva	k1gFnSc2	bitva
nedorazila	dorazit	k5eNaPmAgFnS	dorazit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
útok	útok	k1gInSc1	útok
Rudolfova	Rudolfův	k2eAgInSc2d1	Rudolfův
šiku	šik	k1gInSc2	šik
<g/>
,	,	kIx,	,
vedeného	vedený	k2eAgInSc2d1	vedený
Ulrichem	Ulrich	k1gMnSc7	Ulrich
z	z	k7c2	z
Kapellen	Kapellna	k1gFnPc2	Kapellna
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zálohy	záloha	k1gFnSc2	záloha
do	do	k7c2	do
boku	bok	k1gInSc2	bok
české	český	k2eAgFnSc2d1	Česká
sestavy	sestava	k1gFnSc2	sestava
se	s	k7c7	s
smluveným	smluvený	k2eAgInSc7d1	smluvený
pokřikem	pokřik	k1gInSc7	pokřik
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Utíkají	utíkat	k5eAaImIp3nP	utíkat
<g/>
!	!	kIx.	!
</s>
<s>
Utíkají	utíkat	k5eAaImIp3nP	utíkat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k9	což
zmatené	zmatený	k2eAgNnSc1d1	zmatené
české	český	k2eAgNnSc1d1	české
vojsko	vojsko	k1gNnSc1	vojsko
rozložilo	rozložit	k5eAaPmAgNnS	rozložit
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
českých	český	k2eAgFnPc2d1	Česká
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgInSc1d1	habsburský
dle	dle	k7c2	dle
zvyku	zvyk	k1gInSc2	zvyk
doby	doba	k1gFnSc2	doba
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přehnaně	přehnaně	k6eAd1	přehnaně
vyčíslil	vyčíslit	k5eAaPmAgInS	vyčíslit
na	na	k7c4	na
12	[number]	k4	12
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
rozbahněná	rozbahněný	k2eAgFnSc1d1	rozbahněná
řeka	řeka	k1gFnSc1	řeka
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
rytíři	rytíř	k1gMnPc1	rytíř
utopili	utopit	k5eAaPmAgMnP	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Porážka	porážka	k1gFnSc1	porážka
českého	český	k2eAgNnSc2d1	české
vojska	vojsko	k1gNnSc2	vojsko
byla	být	k5eAaImAgFnS	být
natolik	natolik	k6eAd1	natolik
výrazná	výrazný	k2eAgFnSc1d1	výrazná
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rudolf	Rudolf	k1gMnSc1	Rudolf
část	část	k1gFnSc4	část
svých	svůj	k3xOyFgMnPc2	svůj
bojovníků	bojovník	k1gMnPc2	bojovník
odvolal	odvolat	k5eAaPmAgMnS	odvolat
a	a	k8xC	a
podmanil	podmanit	k5eAaPmAgMnS	podmanit
si	se	k3xPyFc3	se
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bitvy	bitva	k1gFnSc2	bitva
bylo	být	k5eAaImAgNnS	být
definitivní	definitivní	k2eAgNnSc1d1	definitivní
potvrzení	potvrzení	k1gNnSc1	potvrzení
nově	nově	k6eAd1	nově
nastoleného	nastolený	k2eAgNnSc2d1	nastolené
habsburského	habsburský	k2eAgNnSc2d1	habsburské
panství	panství	k1gNnSc2	panství
v	v	k7c6	v
Rakousích	Rakousy	k1gInPc6	Rakousy
a	a	k8xC	a
Štýrsku	Štýrsko	k1gNnSc6	Štýrsko
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
tzv.	tzv.	kA	tzv.
dědičnými	dědičný	k2eAgFnPc7d1	dědičná
habsburskými	habsburský	k2eAgFnPc7d1	habsburská
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
daly	dát	k5eAaPmAgFnP	dát
základ	základ	k1gInSc4	základ
geopolitickému	geopolitický	k2eAgNnSc3d1	geopolitické
uspořádání	uspořádání	k1gNnSc3	uspořádání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
téměř	téměř	k6eAd1	téměř
šest	šest	k4xCc1	šest
a	a	k8xC	a
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
nastala	nastat	k5eAaPmAgFnS	nastat
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
velmi	velmi	k6eAd1	velmi
složitá	složitý	k2eAgFnSc1d1	složitá
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
vlastně	vlastně	k9	vlastně
anarchie	anarchie	k1gFnSc1	anarchie
<g/>
)	)	kIx)	)
a	a	k8xC	a
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Poručník	poručník	k1gMnSc1	poručník
kralevice	kralevic	k1gMnSc2	kralevic
Václava	Václav	k1gMnSc2	Václav
Ota	Oto	k1gMnSc2	Oto
V.	V.	kA	V.
Braniborský	braniborský	k2eAgInSc1d1	braniborský
si	se	k3xPyFc3	se
počínal	počínat	k5eAaImAgInS	počínat
jako	jako	k9	jako
v	v	k7c6	v
dobyté	dobytý	k2eAgFnSc6d1	dobytá
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
kořistnicky	kořistnicky	k6eAd1	kořistnicky
se	se	k3xPyFc4	se
chovala	chovat	k5eAaImAgFnS	chovat
i	i	k9	i
část	část	k1gFnSc1	část
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Poměry	poměr	k1gInPc1	poměr
zčásti	zčásti	k6eAd1	zčásti
uklidnilo	uklidnit	k5eAaPmAgNnS	uklidnit
až	až	k9	až
svolání	svolání	k1gNnSc4	svolání
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
sněmu	sněm	k1gInSc2	sněm
v	v	k7c6	v
r.	r.	kA	r.
1281	[number]	k4	1281
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
účastníci	účastník	k1gMnPc1	účastník
se	se	k3xPyFc4	se
zavázali	zavázat	k5eAaPmAgMnP	zavázat
ke	k	k7c3	k
stíhání	stíhání	k1gNnSc3	stíhání
lupičů	lupič	k1gMnPc2	lupič
a	a	k8xC	a
škůdců	škůdce	k1gMnPc2	škůdce
země	zem	k1gFnSc2	zem
a	a	k8xC	a
k	k	k7c3	k
navrácení	navrácení	k1gNnSc3	navrácení
rozkradených	rozkradený	k2eAgFnPc2d1	rozkradená
královských	královský	k2eAgFnPc2d1	královská
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
hradů	hrad	k1gInPc2	hrad
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
poručníka	poručník	k1gMnSc2	poručník
(	(	kIx(	(
<g/>
jakožto	jakožto	k8xS	jakožto
zástupce	zástupce	k1gMnSc1	zástupce
řádné	řádný	k2eAgFnSc2d1	řádná
královy	králův	k2eAgFnSc2d1	králova
vlády	vláda	k1gFnSc2	vláda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
neúroda	neúroda	k1gFnSc1	neúroda
a	a	k8xC	a
mrazivé	mrazivý	k2eAgNnSc1d1	mrazivé
počasí	počasí	k1gNnSc1	počasí
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
situaci	situace	k1gFnSc4	situace
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
zkomplikovaly	zkomplikovat	k5eAaPmAgFnP	zkomplikovat
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
1278	[number]	k4	1278
<g/>
-	-	kIx~	-
<g/>
1283	[number]	k4	1283
bylo	být	k5eAaImAgNnS	být
proto	proto	k8xC	proto
již	již	k6eAd1	již
dobovými	dobový	k2eAgMnPc7d1	dobový
kronikáři	kronikář	k1gMnPc7	kronikář
nazváno	nazvat	k5eAaPmNgNnS	nazvat
"	"	kIx"	"
<g/>
zlá	zlý	k2eAgNnPc4d1	zlé
léta	léto	k1gNnPc4	léto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
poměrně	poměrně	k6eAd1	poměrně
krátkou	krátký	k2eAgFnSc4d1	krátká
bitvu	bitva	k1gFnSc4	bitva
se	se	k3xPyFc4	se
o	o	k7c6	o
boji	boj	k1gInSc6	boj
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
množství	množství	k1gNnSc1	množství
pověr	pověra	k1gFnPc2	pověra
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Záviš	Záviš	k1gMnSc1	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
bitvy	bitva	k1gFnSc2	bitva
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
pomoc	pomoc	k1gFnSc4	pomoc
za	za	k7c4	za
beztrestnost	beztrestnost	k1gFnSc4	beztrestnost
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Falkenštejn	Falkenštejn	k1gMnSc1	Falkenštejn
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
králově	králův	k2eAgFnSc6d1	králova
smrti	smrt	k1gFnSc6	smrt
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vítkovských	vítkovský	k2eAgFnPc2d1	Vítkovská
čet	četa	k1gFnPc2	četa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
znovu	znovu	k6eAd1	znovu
vedly	vést	k5eAaImAgFnP	vést
útok	útok	k1gInSc4	útok
na	na	k7c4	na
klášter	klášter	k1gInSc4	klášter
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
Korunu	koruna	k1gFnSc4	koruna
i	i	k9	i
na	na	k7c4	na
královské	královský	k2eAgNnSc4d1	královské
město	město	k1gNnSc4	město
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
pověst	pověst	k1gFnSc1	pověst
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Milota	milota	k1gFnSc1	milota
z	z	k7c2	z
Dědic	Dědice	k1gFnPc2	Dědice
odjel	odjet	k5eAaPmAgMnS	odjet
uprostřed	uprostřed	k7c2	uprostřed
bitvy	bitva	k1gFnSc2	bitva
z	z	k7c2	z
boje	boj	k1gInSc2	boj
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgMnS	způsobit
prohru	prohra	k1gFnSc4	prohra
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
z	z	k7c2	z
Dědic	Dědice	k1gFnPc2	Dědice
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Přemyslových	Přemyslův	k2eAgMnPc2d1	Přemyslův
nejvěrnějších	věrný	k2eAgMnPc2d3	nejvěrnější
<g/>
.	.	kIx.	.
</s>
<s>
Milota	milota	k1gFnSc1	milota
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zachránit	zachránit	k5eAaPmF	zachránit
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
rytířů	rytíř	k1gMnPc2	rytíř
chtěl	chtít	k5eAaImAgMnS	chtít
novou	nový	k2eAgFnSc4d1	nová
vlnu	vlna	k1gFnSc4	vlna
útočníků	útočník	k1gMnPc2	útočník
zezadu	zezadu	k6eAd1	zezadu
objet	objet	k5eAaPmF	objet
a	a	k8xC	a
vpadnout	vpadnout	k5eAaPmF	vpadnout
jim	on	k3xPp3gMnPc3	on
do	do	k7c2	do
zad	záda	k1gNnPc2	záda
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
vina	vina	k1gFnSc1	vina
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
mnozí	mnohý	k2eAgMnPc1d1	mnohý
pochopili	pochopit	k5eAaPmAgMnP	pochopit
jako	jako	k8xS	jako
ústup	ústup	k1gInSc4	ústup
a	a	k8xC	a
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
nikomu	nikdo	k3yNnSc3	nikdo
nic	nic	k3yNnSc1	nic
nevysvětloval	vysvětlovat	k5eNaImAgMnS	vysvětlovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemusel	muset	k5eNaImAgMnS	muset
<g/>
,	,	kIx,	,
a	a	k8xC	a
zrádce	zrádce	k1gMnSc1	zrádce
se	se	k3xPyFc4	se
z	z	k7c2	z
něho	on	k3xPp3gInSc2	on
stal	stát	k5eAaPmAgInS	stát
až	až	k6eAd1	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
fáma	fáma	k1gFnSc1	fáma
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
osobním	osobní	k2eAgInSc6d1	osobní
souboji	souboj	k1gInSc6	souboj
mezi	mezi	k7c7	mezi
Přemyslem	Přemysl	k1gMnSc7	Přemysl
a	a	k8xC	a
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Habsburským	habsburský	k2eAgMnSc7d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
manévru	manévr	k1gInSc6	manévr
hejtmana	hejtman	k1gMnSc2	hejtman
z	z	k7c2	z
Dědic	Dědice	k1gFnPc2	Dědice
se	se	k3xPyFc4	se
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
snažil	snažit	k5eAaImAgMnS	snažit
zabránit	zabránit	k5eAaPmF	zabránit
panice	panic	k1gMnSc4	panic
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
ve	v	k7c6	v
složité	složitý	k2eAgFnSc6d1	složitá
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
protivník	protivník	k1gMnSc1	protivník
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
osobně	osobně	k6eAd1	osobně
střetl	střetnout	k5eAaPmAgMnS	střetnout
na	na	k7c4	na
meče	meč	k1gInPc4	meč
(	(	kIx(	(
<g/>
a	a	k8xC	a
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jistě	jistě	k9	jistě
by	by	kYmCp3nS	by
o	o	k7c6	o
tom	ten	k3xDgInSc6	ten
nemlčel	mlčet	k5eNaImAgMnS	mlčet
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
pověst	pověst	k1gFnSc1	pověst
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
samotném	samotný	k2eAgInSc6d1	samotný
scénáři	scénář	k1gInSc6	scénář
smrti	smrt	k1gFnSc2	smrt
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Neví	vědět	k5eNaImIp3nS	vědět
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
verzí	verze	k1gFnSc7	verze
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
-	-	kIx~	-
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
ústupu	ústup	k1gInSc6	ústup
zabili	zabít	k5eAaPmAgMnP	zabít
jeho	jeho	k3xOp3gMnPc1	jeho
vlastní	vlastní	k2eAgMnPc1d1	vlastní
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
líčení	líčení	k1gNnSc1	líčení
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
nejčastěji	často	k6eAd3	často
<g/>
.	.	kIx.	.
</s>
<s>
Naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
že	že	k8xS	že
útočníci	útočník	k1gMnPc1	útočník
Přemysla	Přemysl	k1gMnSc4	Přemysl
nepoznali	poznat	k5eNaPmAgMnP	poznat
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
pánů	pan	k1gMnPc2	pan
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
za	za	k7c2	za
královraha	královrah	k1gMnSc2	královrah
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
označil	označit	k5eAaPmAgMnS	označit
Bertold	Bertold	k1gInSc4	Bertold
Schenk	Schenk	k1gMnSc1	Schenk
z	z	k7c2	z
Emerberku	Emerberk	k1gInSc2	Emerberk
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
byl	být	k5eAaImAgMnS	být
předtím	předtím	k6eAd1	předtím
popraven	popravit	k5eAaPmNgMnS	popravit
na	na	k7c4	na
králův	králův	k2eAgInSc4d1	králův
příkaz	příkaz	k1gInSc4	příkaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xS	jak
už	už	k6eAd1	už
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
pan	pan	k1gMnSc1	pan
z	z	k7c2	z
Emerberku	Emerberk	k1gInSc2	Emerberk
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
číšníka	číšník	k1gMnSc2	číšník
a	a	k8xC	a
Přemysla	Přemysl	k1gMnSc2	Přemysl
musel	muset	k5eAaImAgMnS	muset
znát	znát	k5eAaImF	znát
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
Bertoldovi	Bertold	k1gMnSc3	Bertold
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
družině	družina	k1gFnSc3	družina
měl	mít	k5eAaImAgInS	mít
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ti	ten	k3xDgMnPc1	ten
toho	ten	k3xDgMnSc4	ten
nedbali	nedbat	k5eAaImAgMnP	nedbat
a	a	k8xC	a
ubili	ubít	k5eAaPmAgMnP	ubít
ho	on	k3xPp3gMnSc4	on
snad	snad	k9	snad
sedmnácti	sedmnáct	k4xCc7	sedmnáct
ranami	rána	k1gFnPc7	rána
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
mu	on	k3xPp3gMnSc3	on
rozčísla	rozčísnout	k5eAaPmAgFnS	rozčísnout
lebku	lebka	k1gFnSc4	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Tragický	tragický	k2eAgInSc1d1	tragický
konec	konec	k1gInSc1	konec
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
značný	značný	k2eAgInSc1d1	značný
rozruch	rozruch	k1gInSc1	rozruch
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
soudobých	soudobý	k2eAgFnPc2d1	soudobá
i	i	k8xC	i
pozdějších	pozdní	k2eAgFnPc2d2	pozdější
kronik	kronika	k1gFnPc2	kronika
a	a	k8xC	a
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
je	být	k5eAaImIp3nS	být
zmíněn	zmíněn	k2eAgMnSc1d1	zmíněn
i	i	k8xC	i
ve	v	k7c6	v
slavném	slavný	k2eAgNnSc6d1	slavné
Dantově	Dantův	k2eAgNnSc6d1	Dantovo
díle	dílo	k1gNnSc6	dílo
Božská	božská	k1gFnSc1	božská
komedie	komedie	k1gFnSc1	komedie
(	(	kIx(	(
<g/>
v	v	k7c6	v
VII	VII	kA	VII
<g/>
.	.	kIx.	.
zpěvu	zpěv	k1gInSc2	zpěv
II	II	kA	II
<g/>
.	.	kIx.	.
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Očistce	očistec	k1gInPc4	očistec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
ironicky	ironicky	k6eAd1	ironicky
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
milou	milý	k2eAgFnSc4d1	Milá
společnost	společnost	k1gFnSc4	společnost
tvoří	tvořit	k5eAaImIp3nP	tvořit
<g/>
"	"	kIx"	"
Rudolfu	Rudolf	k1gMnSc3	Rudolf
Habsburskému	habsburský	k2eAgMnSc3d1	habsburský
<g/>
...	...	k?	...
<g />
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
Otakarovu	Otakarův	k2eAgInSc3d1	Otakarův
pádu	pád	k1gInSc3	pád
věnoval	věnovat	k5eAaImAgMnS	věnovat
Jindřich	Jindřich	k1gMnSc1	Jindřich
Heimburský	Heimburský	k2eAgMnSc1d1	Heimburský
<g/>
,	,	kIx,	,
kronikář	kronikář	k1gMnSc1	kronikář
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
píšící	píšící	k2eAgInSc1d1	píšící
latinsky	latinsky	k6eAd1	latinsky
<g/>
;	;	kIx,	;
pak	pak	k6eAd1	pak
také	také	k9	také
tzv.	tzv.	kA	tzv.
Colmarský	Colmarský	k2eAgMnSc1d1	Colmarský
dominikán	dominikán	k1gMnSc1	dominikán
<g/>
,	,	kIx,	,
kronikář	kronikář	k1gMnSc1	kronikář
z	z	k7c2	z
alsaského	alsaský	k2eAgNnSc2d1	alsaské
města	město	k1gNnSc2	město
Colmar	Colmar	k1gMnSc1	Colmar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
píseň	píseň	k1gFnSc4	píseň
Wâfen	Wâfen	k2eAgMnSc1d1	Wâfen
iemer	iemer	k1gMnSc1	iemer
mê	mê	k?	mê
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Běda	běda	k6eAd1	běda
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
běda	běda	k6eAd1	běda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svým	svůj	k3xOyFgNnSc7	svůj
vyzněním	vyznění	k1gNnSc7	vyznění
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
velmi	velmi	k6eAd1	velmi
příznivou	příznivý	k2eAgFnSc4d1	příznivá
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
také	také	k9	také
byla	být	k5eAaImAgFnS	být
kladně	kladně	k6eAd1	kladně
přijímána	přijímat	k5eAaImNgFnS	přijímat
a	a	k8xC	a
oceňována	oceňovat	k5eAaImNgFnS	oceňovat
už	už	k6eAd1	už
např.	např.	kA	např.
Františkem	František	k1gMnSc7	František
Palackým	Palacký	k1gMnSc7	Palacký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
zdrojem	zdroj	k1gInSc7	zdroj
informací	informace	k1gFnPc2	informace
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
protichůdných	protichůdný	k2eAgFnPc2d1	protichůdná
<g/>
)	)	kIx)	)
o	o	k7c6	o
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
i	i	k9	i
Štýrská	štýrský	k2eAgFnSc1d1	štýrská
rýmovaná	rýmovaný	k2eAgFnSc1d1	rýmovaná
kronika	kronika	k1gFnSc1	kronika
od	od	k7c2	od
Otakara	Otakar	k1gMnSc2	Otakar
Štýrského	štýrský	k2eAgMnSc2d1	štýrský
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
Přemysl	Přemysl	k1gMnSc1	Přemysl
znázorněn	znázorněn	k2eAgMnSc1d1	znázorněn
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
světské	světský	k2eAgFnSc2d1	světská
pýchy	pýcha	k1gFnSc2	pýcha
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
obviňován	obviňován	k2eAgInSc4d1	obviňován
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
hlavních	hlavní	k2eAgInPc2d1	hlavní
hříchů	hřích	k1gInPc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
Přemyslovy	Přemyslův	k2eAgFnPc1d1	Přemyslova
oběti	oběť	k1gFnPc1	oběť
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
-	-	kIx~	-
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
skutečnostmi	skutečnost	k1gFnPc7	skutečnost
-	-	kIx~	-
vzpomínáni	vzpomínat	k5eAaImNgMnP	vzpomínat
Gertruda	Gertrudo	k1gNnPc4	Gertrudo
Babenberská	babenberský	k2eAgNnPc4d1	babenberské
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
syn	syn	k1gMnSc1	syn
Fridrich	Fridrich	k1gMnSc1	Fridrich
Bádenský	bádenský	k2eAgMnSc1d1	bádenský
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
zástupce	zástupce	k1gMnSc1	zástupce
císařského	císařský	k2eAgInSc2d1	císařský
rodu	rod	k1gInSc2	rod
Štaufů	Štauf	k1gMnPc2	Štauf
Konradin	Konradina	k1gFnPc2	Konradina
<g/>
.	.	kIx.	.
</s>
<s>
Neznámý	známý	k2eNgMnSc1d1	neznámý
básník	básník	k1gMnSc1	básník
z	z	k7c2	z
Porýní	Porýní	k1gNnSc2	Porýní
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
částečně	částečně	k6eAd1	částečně
dochované	dochovaný	k2eAgNnSc4d1	dochované
dílo	dílo	k1gNnSc4	dílo
Die	Die	k1gMnSc1	Die
Böhmerslacht	Böhmerslacht	k1gMnSc1	Böhmerslacht
(	(	kIx(	(
<g/>
Bitva	bitva	k1gFnSc1	bitva
s	s	k7c7	s
Čechy	Čech	k1gMnPc7	Čech
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
popisuje	popisovat	k5eAaImIp3nS	popisovat
přípravy	příprava	k1gFnPc4	příprava
k	k	k7c3	k
boji	boj	k1gInSc3	boj
i	i	k9	i
samotný	samotný	k2eAgInSc4d1	samotný
průběh	průběh	k1gInSc4	průběh
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
se	se	k3xPyFc4	se
dílo	dílo	k1gNnSc1	dílo
nedochovalo	dochovat	k5eNaPmAgNnS	dochovat
celé	celý	k2eAgNnSc1d1	celé
a	a	k8xC	a
krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
není	být	k5eNaImIp3nS	být
podáno	podán	k2eAgNnSc1d1	podáno
dle	dle	k7c2	dle
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stylizovaně	stylizovaně	k6eAd1	stylizovaně
<g/>
.	.	kIx.	.
</s>
<s>
Soudobý	soudobý	k2eAgMnSc1d1	soudobý
autor	autor	k1gMnSc1	autor
Oldřich	Oldřich	k1gMnSc1	Oldřich
Daněk	Daněk	k1gMnSc1	Daněk
napsal	napsat	k5eAaPmAgMnS	napsat
rozhlasovou	rozhlasový	k2eAgFnSc4d1	rozhlasová
hru	hra	k1gFnSc4	hra
"	"	kIx"	"
<g/>
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
formou	forma	k1gFnSc7	forma
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
Zbraslavské	zbraslavský	k2eAgFnSc2d1	Zbraslavská
kroniky	kronika	k1gFnSc2	kronika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
každopádně	každopádně	k6eAd1	každopádně
s	s	k7c7	s
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
loučí	loučit	k5eAaImIp3nS	loučit
dost	dost	k6eAd1	dost
pochmurnými	pochmurný	k2eAgNnPc7d1	pochmurné
slovy	slovo	k1gNnPc7	slovo
<g/>
.	.	kIx.	.
</s>
<s>
BOK	bok	k1gInSc1	bok
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
POKORNÝ	Pokorný	k1gMnSc1	Pokorný
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
</s>
<s>
Moravo	Morava	k1gFnSc5	Morava
<g/>
,	,	kIx,	,
Čechy	Čech	k1gMnPc7	Čech
<g/>
,	,	kIx,	,
radujte	radovat	k5eAaImRp2nP	radovat
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
a	a	k8xC	a
rakouští	rakouský	k2eAgMnPc1d1	rakouský
básníci	básník	k1gMnPc1	básník
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
za	za	k7c2	za
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Aula	aula	k1gFnSc1	aula
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901626	[number]	k4	901626
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
DOPSCH	DOPSCH	kA	DOPSCH
<g/>
,	,	kIx,	,
Heinz	Heinz	k1gMnSc1	Heinz
<g/>
;	;	kIx,	;
BRUNNER	BRUNNER	kA	BRUNNER
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
<g/>
;	;	kIx,	;
WELTIN	WELTIN	kA	WELTIN
<g/>
,	,	kIx,	,
Maximilian	Maximilian	k1gInSc1	Maximilian
<g/>
.	.	kIx.	.
</s>
<s>
Österreichische	Österreichischus	k1gMnSc5	Österreichischus
Geschichte	Geschicht	k1gMnSc5	Geschicht
1122	[number]	k4	1122
<g/>
-	-	kIx~	-
<g/>
1278	[number]	k4	1278
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Länder	Länder	k1gInSc1	Länder
und	und	k?	und
das	das	k?	das
Reich	Reich	k?	Reich
:	:	kIx,	:
der	drát	k5eAaImRp2nS	drát
Ostalpenraum	Ostalpenraum	k1gInSc1	Ostalpenraum
im	im	k?	im
Hochmittelalter	Hochmittelalter	k1gInSc1	Hochmittelalter
<g/>
.	.	kIx.	.
</s>
<s>
Wien	Wien	k1gMnSc1	Wien
:	:	kIx,	:
Ueberreuter	Ueberreuter	k1gMnSc1	Ueberreuter
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
620	[number]	k4	620
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8000	[number]	k4	8000
<g/>
-	-	kIx~	-
<g/>
3532	[number]	k4	3532
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
KOFRÁNKOVÁ	KOFRÁNKOVÁ	kA	KOFRÁNKOVÁ
<g/>
,	,	kIx,	,
Václava	Václava	k1gFnSc1	Václava
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1278	[number]	k4	1278
<g/>
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgNnSc1d1	Moravské
pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
boj	boj	k1gInSc1	boj
zlatého	zlatý	k2eAgMnSc2d1	zlatý
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Havran	Havran	k1gMnSc1	Havran
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
153	[number]	k4	153
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86515	[number]	k4	86515
<g/>
-	-	kIx~	-
<g/>
71	[number]	k4	71
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
KOFRÁNKOVÁ	KOFRÁNKOVÁ	kA	KOFRÁNKOVÁ
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
král	král	k1gMnSc1	král
a	a	k8xC	a
chudý	chudý	k2eAgMnSc1d1	chudý
hrabě	hrabě	k1gMnSc1	hrabě
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
tradici	tradice	k1gFnSc6	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
LIKA	LIKA	kA	LIKA
KLUB	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
158	[number]	k4	158
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86069	[number]	k4	86069
<g/>
-	-	kIx~	-
<g/>
77	[number]	k4	77
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
KUSTERNIG	KUSTERNIG	kA	KUSTERNIG
<g/>
,	,	kIx,	,
Andreas	Andreas	k1gInSc1	Andreas
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
(	(	kIx(	(
<g/>
u	u	k7c2	u
Suchých	Suchých	k2eAgFnPc2d1	Suchých
Krut	kruta	k1gFnPc2	kruta
a	a	k8xC	a
Jedenspeigen	Jedenspeigen	k1gInSc1	Jedenspeigen
<g/>
)	)	kIx)	)
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1278	[number]	k4	1278
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
HLAVÁČEK	Hlaváček	k1gMnSc1	Hlaváček
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Česko-rakouské	českoakouský	k2eAgInPc4d1	česko-rakouský
vztahy	vztah	k1gInPc4	vztah
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
:	:	kIx,	:
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Štýrska	Štýrsko	k1gNnSc2	Štýrsko
<g/>
,	,	kIx,	,
Korutan	Korutany	k1gInPc2	Korutany
a	a	k8xC	a
Kraňska	Kraňsko	k1gNnSc2	Kraňsko
<g/>
)	)	kIx)	)
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
velké	velký	k2eAgFnSc2d1	velká
říše	říš	k1gFnSc2	říš
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
:	:	kIx,	:
sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
ze	z	k7c2	z
symposia	symposion	k1gNnSc2	symposion
konaného	konaný	k2eAgNnSc2d1	konané
26	[number]	k4	26
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1996	[number]	k4	1996
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Rakouský	rakouský	k2eAgInSc1d1	rakouský
kulturní	kulturní	k2eAgInSc1d1	kulturní
institut	institut	k1gInSc1	institut
;	;	kIx,	;
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85899	[number]	k4	85899
<g/>
-	-	kIx~	-
<g/>
41	[number]	k4	41
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
163	[number]	k4	163
<g/>
-	-	kIx~	-
<g/>
189	[number]	k4	189
<g/>
.	.	kIx.	.
</s>
<s>
MIKA	Mik	k1gMnSc4	Mik
<g/>
,	,	kIx,	,
Norbert	Norbert	k1gMnSc1	Norbert
<g/>
.	.	kIx.	.
</s>
<s>
Walka	Walka	k1gFnSc1	Walka
o	o	k7c4	o
spadek	spadek	k1gInSc4	spadek
po	po	k7c4	po
Babenbergach	Babenbergach	k1gInSc4	Babenbergach
1246	[number]	k4	1246
<g/>
-	-	kIx~	-
<g/>
1278	[number]	k4	1278
<g/>
.	.	kIx.	.
</s>
<s>
Racibórz	Racibórz	k1gMnSc1	Racibórz
:	:	kIx,	:
WAW	WAW	kA	WAW
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
136	[number]	k4	136
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
919765	[number]	k4	919765
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
ŠUSTA	Šusta	k1gMnSc1	Šusta
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Soumrak	soumrak	k1gInSc1	soumrak
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
803	[number]	k4	803
s.	s.	k?	s.
VANÍČEK	Vaníček	k1gMnSc1	Vaníček
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
III	III	kA	III
<g/>
.	.	kIx.	.
1250	[number]	k4	1250
<g/>
-	-	kIx~	-
<g/>
1310	[number]	k4	1310
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
760	[number]	k4	760
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
433	[number]	k4	433
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Století	století	k1gNnSc1	století
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
412	[number]	k4	412
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
281	[number]	k4	281
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Bitvy	bitva	k1gFnPc1	bitva
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Bellum	Bellum	k1gInSc1	Bellum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1278	[number]	k4	1278
700	[number]	k4	700
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
od	od	k7c2	od
bitvy	bitva	k1gFnSc2	bitva
na	na	k7c6	na
dopisní	dopisní	k2eAgFnSc6d1	dopisní
známce	známka	k1gFnSc6	známka
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
soudobých	soudobý	k2eAgFnPc2d1	soudobá
kronik	kronika	k1gFnPc2	kronika
1278	[number]	k4	1278
-	-	kIx~	-
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc4d1	historický
magazín	magazín	k1gInSc4	magazín
Debata	debata	k1gFnSc1	debata
o	o	k7c6	o
bitvě	bitva	k1gFnSc6	bitva
</s>
