<s>
Žárovka	žárovka	k1gFnSc1	žárovka
je	být	k5eAaImIp3nS	být
jednoduché	jednoduchý	k2eAgNnSc4d1	jednoduché
zařízení	zařízení	k1gNnSc4	zařízení
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
zahřívání	zahřívání	k1gNnSc2	zahřívání
tenkého	tenký	k2eAgNnSc2d1	tenké
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
wolframového	wolframový	k2eAgInSc2d1	wolframový
vodiče	vodič	k1gInSc2	vodič
elektrickým	elektrický	k2eAgInSc7d1	elektrický
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jím	on	k3xPp3gNnSc7	on
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
teplotě	teplota	k1gFnSc6	teplota
vlákno	vlákno	k1gNnSc1	vlákno
žárovky	žárovka	k1gFnSc2	žárovka
září	září	k1gNnSc2	září
především	především	k9	především
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
přežhavených	přežhavený	k2eAgFnPc2d1	přežhavená
žárovek	žárovka	k1gFnPc2	žárovka
(	(	kIx(	(
<g/>
projekční	projekční	k2eAgInPc1d1	projekční
typy	typ	k1gInPc1	typ
<g/>
,	,	kIx,	,
halogeny	halogen	k1gInPc1	halogen
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
najdeme	najít	k5eAaPmIp1nP	najít
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
i	i	k9	i
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
baňka	baňka	k1gFnSc1	baňka
žárovky	žárovka	k1gFnSc2	žárovka
z	z	k7c2	z
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
skla	sklo	k1gNnSc2	sklo
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
záření	záření	k1gNnSc4	záření
prakticky	prakticky	k6eAd1	prakticky
nepropustná	propustný	k2eNgFnSc1d1	nepropustná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
optického	optický	k2eAgNnSc2d1	optické
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
vlákno	vlákno	k1gNnSc4	vlákno
žárovky	žárovka	k1gFnSc2	žárovka
nechová	chovat	k5eNaImIp3nS	chovat
jako	jako	k9	jako
absolutně	absolutně	k6eAd1	absolutně
černý	černý	k2eAgInSc1d1	černý
zářič	zářič	k1gInSc1	zářič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
kelvinů	kelvin	k1gInPc2	kelvin
teplejší	teplý	k2eAgMnSc1d2	teplejší
(	(	kIx(	(
<g/>
wolfram	wolfram	k1gInSc1	wolfram
je	být	k5eAaImIp3nS	být
selektivní	selektivní	k2eAgInSc4d1	selektivní
zářič	zářič	k1gInSc4	zářič
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
žárovka	žárovka	k1gFnSc1	žárovka
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k9	také
základem	základ	k1gInSc7	základ
většiny	většina	k1gFnSc2	většina
přenosných	přenosný	k2eAgNnPc2d1	přenosné
svítidel	svítidlo	k1gNnPc2	svítidlo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
automobilových	automobilový	k2eAgInPc6d1	automobilový
světlometech	světlomet	k1gInPc6	světlomet
nebo	nebo	k8xC	nebo
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
světlo	světlo	k1gNnSc1	světlo
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
často	často	k6eAd1	často
využívají	využívat	k5eAaImIp3nP	využívat
halogenové	halogenový	k2eAgFnPc4d1	halogenová
žárovky	žárovka	k1gFnPc4	žárovka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
výhody	výhoda	k1gFnPc4	výhoda
žárovky	žárovka	k1gFnSc2	žárovka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
světelného	světelný	k2eAgInSc2d1	světelný
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
vysoce	vysoce	k6eAd1	vysoce
automatizovaná	automatizovaný	k2eAgFnSc1d1	automatizovaná
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgNnSc1d1	vynikající
podání	podání	k1gNnSc1	podání
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
Ra	ra	k0	ra
=	=	kIx~	=
100	[number]	k4	100
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
přímého	přímý	k2eAgNnSc2d1	přímé
napájení	napájení	k1gNnSc2	napájení
z	z	k7c2	z
elektrické	elektrický	k2eAgFnSc2d1	elektrická
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
absence	absence	k1gFnSc1	absence
zdraví	zdraví	k1gNnSc2	zdraví
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
nízká	nízký	k2eAgFnSc1d1	nízká
účinnost	účinnost	k1gFnSc1	účinnost
a	a	k8xC	a
měrný	měrný	k2eAgInSc1d1	měrný
výkon	výkon	k1gInSc1	výkon
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
10	[number]	k4	10
-	-	kIx~	-
15	[number]	k4	15
lm	lm	k?	lm
<g/>
/	/	kIx~	/
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
závislost	závislost	k1gFnSc1	závislost
parametrů	parametr	k1gInPc2	parametr
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
životnosti	životnost	k1gFnSc2	životnost
<g/>
)	)	kIx)	)
na	na	k7c6	na
napájecím	napájecí	k2eAgNnSc6d1	napájecí
napětí	napětí	k1gNnSc6	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Technologicky	technologicky	k6eAd1	technologicky
výrobu	výroba	k1gFnSc4	výroba
žárovky	žárovka	k1gFnSc2	žárovka
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
a	a	k8xC	a
patentoval	patentovat	k5eAaBmAgMnS	patentovat
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
-	-	kIx~	-
první	první	k4xOgFnSc1	první
žárovka	žárovka	k1gFnSc1	žárovka
byla	být	k5eAaImAgFnS	být
rozsvícena	rozsvícen	k2eAgFnSc1d1	rozsvícena
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1879	[number]	k4	1879
a	a	k8xC	a
svítila	svítit	k5eAaImAgFnS	svítit
40	[number]	k4	40
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trh	trh	k1gInSc4	trh
byly	být	k5eAaImAgFnP	být
uvedeny	uveden	k2eAgFnPc1d1	uvedena
žárovky	žárovka	k1gFnPc1	žárovka
v	v	k7c6	v
provedení	provedení	k1gNnSc6	provedení
s	s	k7c7	s
bambusovým	bambusový	k2eAgNnSc7d1	bambusové
vláknem	vlákno	k1gNnSc7	vlákno
a	a	k8xC	a
standardní	standardní	k2eAgFnSc7d1	standardní
šroubovací	šroubovací	k2eAgFnSc7d1	šroubovací
paticí	patice	k1gFnSc7	patice
E	E	kA	E
27	[number]	k4	27
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
<s>
Svítily	svítit	k5eAaImAgFnP	svítit
asi	asi	k9	asi
600	[number]	k4	600
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
drahé	drahá	k1gFnPc1	drahá
-	-	kIx~	-
1	[number]	k4	1
dolar	dolar	k1gInSc4	dolar
a	a	k8xC	a
15	[number]	k4	15
centů	cent	k1gInPc2	cent
<g/>
.	.	kIx.	.
</s>
<s>
Edison	Edison	k1gInSc1	Edison
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
původním	původní	k2eAgMnSc7d1	původní
vynálezcem	vynálezce	k1gMnSc7	vynálezce
žárovky	žárovka	k1gFnSc2	žárovka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
předchůdcem	předchůdce	k1gMnSc7	předchůdce
byl	být	k5eAaImAgMnS	být
Heinrich	Heinrich	k1gMnSc1	Heinrich
Göbel	Göbel	k1gMnSc1	Göbel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
se	s	k7c7	s
žárovkou	žárovka	k1gFnSc7	žárovka
(	(	kIx(	(
<g/>
principiálně	principiálně	k6eAd1	principiálně
vznik	vznik	k1gInSc4	vznik
světla	světlo	k1gNnSc2	světlo
žhavením	žhavení	k1gNnSc7	žhavení
materiálů	materiál	k1gInPc2	materiál
průchodem	průchod	k1gInSc7	průchod
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
datovat	datovat	k5eAaImF	datovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1802	[number]	k4	1802
(	(	kIx(	(
<g/>
Humphry	Humphra	k1gFnSc2	Humphra
Davy	Dav	k1gInPc1	Dav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
datum	datum	k1gNnSc1	datum
jejího	její	k3xOp3gNnSc2	její
vynalezení	vynalezení	k1gNnSc2	vynalezení
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
uváděn	uvádět	k5eAaImNgInS	uvádět
rok	rok	k1gInSc1	rok
1854	[number]	k4	1854
a	a	k8xC	a
jméno	jméno	k1gNnSc4	jméno
Göbel	Göbel	k1gMnSc1	Göbel
(	(	kIx(	(
<g/>
Goebel	Goebel	k1gMnSc1	Goebel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výrobou	výroba	k1gFnSc7	výroba
žárovky	žárovka	k1gFnSc2	žárovka
Edison	Edisona	k1gFnPc2	Edisona
v	v	k7c6	v
soudní	soudní	k2eAgFnSc6d1	soudní
síni	síň	k1gFnSc6	síň
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvenství	prvenství	k1gNnSc1	prvenství
ve	v	k7c6	v
využití	využití	k1gNnSc6	využití
patří	patřit	k5eAaImIp3nS	patřit
jemu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
žárovky	žárovka	k1gFnSc2	žárovka
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
elektronka	elektronka	k1gFnSc1	elektronka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
základem	základ	k1gInSc7	základ
elektronických	elektronický	k2eAgInPc2d1	elektronický
přístrojů	přístroj	k1gInPc2	přístroj
až	až	k6eAd1	až
do	do	k7c2	do
vynálezu	vynález	k1gInSc2	vynález
tranzistoru	tranzistor	k1gInSc2	tranzistor
<g/>
.	.	kIx.	.
</s>
<s>
Efekt	efekt	k1gInSc1	efekt
vyzařování	vyzařování	k1gNnSc2	vyzařování
elektronů	elektron	k1gInPc2	elektron
z	z	k7c2	z
rozžhaveného	rozžhavený	k2eAgNnSc2d1	rozžhavené
vlákna	vlákno	k1gNnSc2	vlákno
objevil	objevit	k5eAaPmAgMnS	objevit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
T.	T.	kA	T.
A.	A.	kA	A.
Edison	Edison	k1gInSc4	Edison
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
doba	doba	k1gFnSc1	doba
života	život	k1gInSc2	život
standardní	standardní	k2eAgFnSc2d1	standardní
žárovky	žárovka	k1gFnSc2	žárovka
je	být	k5eAaImIp3nS	být
800	[number]	k4	800
-	-	kIx~	-
1000	[number]	k4	1000
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Střední	střední	k2eAgFnSc1d1	střední
doba	doba	k1gFnSc1	doba
života	život	k1gInSc2	život
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
uvedené	uvedený	k2eAgFnSc6d1	uvedená
době	doba	k1gFnSc6	doba
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
nejméně	málo	k6eAd3	málo
50	[number]	k4	50
%	%	kIx~	%
původních	původní	k2eAgFnPc2d1	původní
žárovek	žárovka	k1gFnPc2	žárovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
elektrické	elektrický	k2eAgNnSc1d1	elektrické
světlo	světlo	k1gNnSc1	světlo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
zemích	zem	k1gFnPc6	zem
měl	mít	k5eAaImAgInS	mít
Robertův	Robertův	k2eAgInSc1d1	Robertův
cukrovar	cukrovar	k1gInSc1	cukrovar
v	v	k7c6	v
Židlochovicích	Židlochovice	k1gFnPc6	Židlochovice
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejmodernějších	moderní	k2eAgInPc2d3	nejmodernější
podniků	podnik	k1gInPc2	podnik
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
rozsvítila	rozsvítit	k5eAaPmAgFnS	rozsvítit
žárovka	žárovka	k1gFnSc1	žárovka
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
výstavě	výstava	k1gFnSc6	výstava
svítily	svítit	k5eAaImAgFnP	svítit
žárovky	žárovka	k1gFnPc1	žárovka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1881	[number]	k4	1881
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
měla	mít	k5eAaImAgFnS	mít
elektrické	elektrický	k2eAgNnSc4d1	elektrické
osvětlení	osvětlení	k1gNnSc4	osvětlení
Daňkova	Daňkův	k2eAgFnSc1d1	Daňkova
strojírna	strojírna	k1gFnSc1	strojírna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
divadlo	divadlo	k1gNnSc1	divadlo
s	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
osvětlením	osvětlení	k1gNnSc7	osvětlení
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
pevnině	pevnina	k1gFnSc6	pevnina
mělo	mít	k5eAaImAgNnS	mít
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
Mahenovo	Mahenův	k2eAgNnSc1d1	Mahenovo
divadlo	divadlo	k1gNnSc1	divadlo
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgNnSc1d1	elektrické
osvětlení	osvětlení	k1gNnSc1	osvětlení
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
bylo	být	k5eAaImAgNnS	být
zadáno	zadat	k5eAaPmNgNnS	zadat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnPc1d1	původní
Edisonovy	Edisonův	k2eAgFnPc1d1	Edisonova
žárovky	žárovka	k1gFnPc1	žárovka
měly	mít	k5eAaImAgFnP	mít
uhlíkové	uhlíkový	k2eAgNnSc4d1	uhlíkové
vlákno	vlákno	k1gNnSc4	vlákno
(	(	kIx(	(
<g/>
zuhelnatělý	zuhelnatělý	k2eAgInSc1d1	zuhelnatělý
bambus	bambus	k1gInSc1	bambus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
využívá	využívat	k5eAaPmIp3nS	využívat
wolfram	wolfram	k1gInSc1	wolfram
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
lépe	dobře	k6eAd2	dobře
odolává	odolávat	k5eAaImIp3nS	odolávat
vysokým	vysoký	k2eAgFnPc3d1	vysoká
teplotám	teplota	k1gFnPc3	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
vlákno	vlákno	k1gNnSc1	vlákno
neshořelo	shořet	k5eNaPmAgNnS	shořet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
baňce	baňka	k1gFnSc6	baňka
z	z	k7c2	z
obyčejného	obyčejný	k2eAgNnSc2d1	obyčejné
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
vyčerpán	vyčerpán	k2eAgInSc1d1	vyčerpán
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
standardních	standardní	k2eAgFnPc2d1	standardní
žárovek	žárovka	k1gFnPc2	žárovka
do	do	k7c2	do
15	[number]	k4	15
W	W	kA	W
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
baňka	baňka	k1gFnSc1	baňka
vakuovaná	vakuovaný	k2eAgFnSc1d1	vakuovaná
(	(	kIx(	(
<g/>
vzduchoprázdná	vzduchoprázdný	k2eAgFnSc1d1	vzduchoprázdná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
silnějších	silný	k2eAgFnPc2d2	silnější
žárovek	žárovka	k1gFnPc2	žárovka
je	být	k5eAaImIp3nS	být
plněná	plněný	k2eAgFnSc1d1	plněná
směsí	směs	k1gFnSc7	směs
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
argonu	argon	k1gInSc2	argon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řidčeji	řídce	k6eAd2	řídce
také	také	k9	také
kryptonem	krypton	k1gInSc7	krypton
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
xenonem	xenon	k1gInSc7	xenon
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
náplně	náplň	k1gFnPc1	náplň
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vyšší	vysoký	k2eAgFnPc1d2	vyšší
provozní	provozní	k2eAgFnPc1d1	provozní
teploty	teplota	k1gFnPc1	teplota
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
omezují	omezovat	k5eAaImIp3nP	omezovat
jeho	jeho	k3xOp3gNnSc4	jeho
stárnutí	stárnutí	k1gNnSc4	stárnutí
rozprašováním	rozprašování	k1gNnSc7	rozprašování
nebo	nebo	k8xC	nebo
odpařováním	odpařování	k1gNnSc7	odpařování
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
standardních	standardní	k2eAgFnPc2d1	standardní
a	a	k8xC	a
velkých	velký	k2eAgFnPc2d1	velká
žárovek	žárovka	k1gFnPc2	žárovka
je	být	k5eAaImIp3nS	být
náplň	náplň	k1gFnSc1	náplň
volena	volit	k5eAaImNgFnS	volit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
za	za	k7c2	za
provozu	provoz	k1gInSc2	provoz
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
baňce	baňka	k1gFnSc6	baňka
přibližně	přibližně	k6eAd1	přibližně
srovnal	srovnat	k5eAaPmAgMnS	srovnat
s	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
atmosférickým	atmosférický	k2eAgInSc7d1	atmosférický
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
teplotní	teplotní	k2eAgInSc4d1	teplotní
světelný	světelný	k2eAgInSc4d1	světelný
zdroj	zdroj	k1gInSc4	zdroj
dávají	dávat	k5eAaImIp3nP	dávat
stálé	stálý	k2eAgNnSc4d1	stálé
spojité	spojitý	k2eAgNnSc4d1	spojité
světelné	světelný	k2eAgNnSc4d1	světelné
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
do	do	k7c2	do
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
kuchyní	kuchyně	k1gFnPc2	kuchyně
<g/>
,	,	kIx,	,
dílenských	dílenský	k2eAgInPc2d1	dílenský
provozů	provoz	k1gInPc2	provoz
a	a	k8xC	a
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
zachovat	zachovat	k5eAaPmF	zachovat
věrnost	věrnost	k1gFnSc4	věrnost
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
vycházejí	vycházet	k5eAaImIp3nP	vycházet
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
běžné	běžný	k2eAgInPc1d1	běžný
výbojové	výbojový	k2eAgInPc1d1	výbojový
zdroje	zdroj	k1gInPc1	zdroj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
zářivky	zářivka	k1gFnPc1	zářivka
a	a	k8xC	a
výbojky	výbojka	k1gFnPc1	výbojka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
naopak	naopak	k6eAd1	naopak
bývají	bývat	k5eAaImIp3nP	bývat
monochromatické	monochromatický	k2eAgInPc1d1	monochromatický
a	a	k8xC	a
pulsují	pulsovat	k5eAaImIp3nP	pulsovat
s	s	k7c7	s
frekvencí	frekvence	k1gFnSc7	frekvence
elektrorozvodné	elektrorozvodný	k2eAgFnSc2d1	elektrorozvodná
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
ty	ty	k3xPp2nSc1	ty
bez	bez	k7c2	bez
elektronického	elektronický	k2eAgMnSc2d1	elektronický
zapalovače	zapalovač	k1gMnSc2	zapalovač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jas	jas	k1gInSc1	jas
žárovek	žárovka	k1gFnPc2	žárovka
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
plynule	plynule	k6eAd1	plynule
regulovat	regulovat	k5eAaImF	regulovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
vyšší	vysoký	k2eAgInSc4d2	vyšší
světelný	světelný	k2eAgInSc4d1	světelný
tok	tok	k1gInSc4	tok
zvýšením	zvýšení	k1gNnSc7	zvýšení
napájecího	napájecí	k2eAgNnSc2d1	napájecí
napětí	napětí	k1gNnSc2	napětí
<g/>
;	;	kIx,	;
u	u	k7c2	u
běžné	běžný	k2eAgFnSc2d1	běžná
žárovky	žárovka	k1gFnSc2	žárovka
při	při	k7c6	při
užívání	užívání	k1gNnSc6	užívání
o	o	k7c4	o
5	[number]	k4	5
%	%	kIx~	%
vyššího	vysoký	k2eAgNnSc2d2	vyšší
napájecího	napájecí	k2eAgNnSc2d1	napájecí
napětí	napětí	k1gNnSc2	napětí
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
světelný	světelný	k2eAgInSc4d1	světelný
tok	tok	k1gInSc4	tok
asi	asi	k9	asi
o	o	k7c4	o
20	[number]	k4	20
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
životnost	životnost	k1gFnSc1	životnost
takové	takový	k3xDgFnSc2	takový
žárovky	žárovka	k1gFnSc2	žárovka
klesne	klesnout	k5eAaPmIp3nS	klesnout
na	na	k7c4	na
50	[number]	k4	50
%	%	kIx~	%
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
žárovek	žárovka	k1gFnPc2	žárovka
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
<g/>
,	,	kIx,	,
levnější	levný	k2eAgMnSc1d2	levnější
a	a	k8xC	a
energeticky	energeticky	k6eAd1	energeticky
úspornější	úsporný	k2eAgFnSc1d2	úspornější
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukčně	konstrukčně	k6eAd1	konstrukčně
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
<g/>
,	,	kIx,	,
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádný	žádný	k3yNgInSc4	žádný
elektronický	elektronický	k2eAgInSc4d1	elektronický
zapalovač	zapalovač	k1gInSc4	zapalovač
<g/>
.	.	kIx.	.
</s>
<s>
Žárovky	žárovka	k1gFnPc1	žárovka
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
materiálů	materiál	k1gInPc2	materiál
ekologicky	ekologicky	k6eAd1	ekologicky
nezávadné	závadný	k2eNgInPc1d1	nezávadný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
wolfram	wolfram	k1gInSc1	wolfram
<g/>
,	,	kIx,	,
inertní	inertní	k2eAgInSc1d1	inertní
plyn	plyn	k1gInSc1	plyn
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
dusík	dusík	k1gInSc1	dusík
a	a	k8xC	a
argon	argon	k1gInSc1	argon
a	a	k8xC	a
běžný	běžný	k2eAgInSc1d1	běžný
konstrukční	konstrukční	k2eAgInSc1d1	konstrukční
kov	kov	k1gInSc1	kov
na	na	k7c4	na
patici	patice	k1gFnSc4	patice
a	a	k8xC	a
držák	držák	k1gInSc4	držák
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zářivky	zářivka	k1gFnPc1	zářivka
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
složitě	složitě	k6eAd1	složitě
ekologicky	ekologicky	k6eAd1	ekologicky
likvidovat	likvidovat	k5eAaBmF	likvidovat
jako	jako	k9	jako
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
odpad	odpad	k1gInSc4	odpad
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
rtuť	rtuť	k1gFnSc1	rtuť
a	a	k8xC	a
toxický	toxický	k2eAgInSc1d1	toxický
luminofor	luminofor	k1gInSc1	luminofor
a	a	k8xC	a
u	u	k7c2	u
zářivek	zářivka	k1gFnPc2	zářivka
s	s	k7c7	s
elektronickým	elektronický	k2eAgInSc7d1	elektronický
zapalovačem	zapalovač	k1gInSc7	zapalovač
další	další	k2eAgFnPc1d1	další
elektrotechnické	elektrotechnický	k2eAgFnPc1d1	elektrotechnická
součástky	součástka	k1gFnPc1	součástka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
několikanásobně	několikanásobně	k6eAd1	několikanásobně
(	(	kIx(	(
<g/>
5	[number]	k4	5
-	-	kIx~	-
25	[number]	k4	25
x	x	k?	x
<g/>
)	)	kIx)	)
levnější	levný	k2eAgFnSc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
Žárovky	žárovka	k1gFnPc1	žárovka
neemitují	emitovat	k5eNaBmIp3nP	emitovat
žádné	žádný	k3yNgNnSc4	žádný
nebezpečné	bezpečný	k2eNgNnSc4d1	nebezpečné
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Žárovky	žárovka	k1gFnPc1	žárovka
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
spojité	spojitý	k2eAgNnSc4d1	spojité
spektrum	spektrum	k1gNnSc4	spektrum
světla	světlo	k1gNnSc2	světlo
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgMnSc3	ten
u	u	k7c2	u
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Žárovky	žárovka	k1gFnPc1	žárovka
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
kombinovaný	kombinovaný	k2eAgInSc4d1	kombinovaný
světelný	světelný	k2eAgInSc4d1	světelný
a	a	k8xC	a
tepelný	tepelný	k2eAgInSc4d1	tepelný
zdroj	zdroj	k1gInSc4	zdroj
(	(	kIx(	(
<g/>
akvaristika	akvaristika	k1gFnSc1	akvaristika
<g/>
,	,	kIx,	,
teraristika	teraristika	k1gFnSc1	teraristika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žárovky	žárovka	k1gFnPc1	žárovka
téměř	téměř	k6eAd1	téměř
neblikají	blikat	k5eNaImIp3nP	blikat
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
rychle	rychle	k6eAd1	rychle
rotující	rotující	k2eAgInPc1d1	rotující
předměty	předmět	k1gInPc1	předmět
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
dílnách	dílna	k1gFnPc6	dílna
s	s	k7c7	s
obráběcími	obráběcí	k2eAgInPc7d1	obráběcí
stroji	stroj	k1gInPc7	stroj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
energetická	energetický	k2eAgFnSc1d1	energetická
účinnost	účinnost	k1gFnSc1	účinnost
-	-	kIx~	-
většina	většina	k1gFnSc1	většina
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
až	až	k9	až
95	[number]	k4	95
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
teplo	teplo	k1gNnSc4	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
žárovek	žárovka	k1gFnPc2	žárovka
ovšem	ovšem	k9	ovšem
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
příkonem	příkon	k1gInSc7	příkon
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
tabulka	tabulka	k1gFnSc1	tabulka
Světelná	světelný	k2eAgFnSc1d1	světelná
účinnost	účinnost	k1gFnSc1	účinnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokrytí	pokrytí	k1gNnSc1	pokrytí
vlákna	vlákno	k1gNnSc2	vlákno
vrstvou	vrstva	k1gFnSc7	vrstva
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
zvýšit	zvýšit	k5eAaPmF	zvýšit
účinnost	účinnost	k1gFnSc4	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Žárovky	žárovka	k1gFnPc1	žárovka
mají	mít	k5eAaImIp3nP	mít
kratší	krátký	k2eAgFnSc4d2	kratší
životnost	životnost	k1gFnSc4	životnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
nejčastěji	často	k6eAd3	často
kolem	kolem	k7c2	kolem
800	[number]	k4	800
-	-	kIx~	-
1000	[number]	k4	1000
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
kompromis	kompromis	k1gInSc4	kompromis
mezi	mezi	k7c7	mezi
účinností	účinnost	k1gFnSc7	účinnost
a	a	k8xC	a
životností	životnost	k1gFnSc7	životnost
<g/>
;	;	kIx,	;
vysoce	vysoce	k6eAd1	vysoce
účinné	účinný	k2eAgFnSc2d1	účinná
přežhavené	přežhavený	k2eAgFnSc2d1	přežhavená
žárovky	žárovka	k1gFnSc2	žárovka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
kinoprojektorech	kinoprojektor	k1gInPc6	kinoprojektor
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
krátkou	krátký	k2eAgFnSc4d1	krátká
životnost	životnost	k1gFnSc4	životnost
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
tzv.	tzv.	kA	tzv.
podžhavené	podžhavený	k2eAgInPc1d1	podžhavený
(	(	kIx(	(
<g/>
též	též	k9	též
dlouhoživotné	dlouhoživotný	k2eAgFnPc1d1	dlouhoživotný
<g/>
)	)	kIx)	)
žárovky	žárovka	k1gFnPc1	žárovka
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
světelným	světelný	k2eAgInSc7d1	světelný
tokem	tok	k1gInSc7	tok
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
nízkou	nízký	k2eAgFnSc7d1	nízká
účinností	účinnost	k1gFnSc7	účinnost
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
násobně	násobně	k6eAd1	násobně
delší	dlouhý	k2eAgFnSc4d2	delší
životnost	životnost	k1gFnSc4	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
halogenové	halogenový	k2eAgFnPc1d1	halogenová
žárovky	žárovka	k1gFnPc1	žárovka
emitují	emitovat	k5eAaBmIp3nP	emitovat
také	také	k9	také
UV	UV	kA	UV
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
žárovkám	žárovka	k1gFnPc3	žárovka
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
jejich	jejich	k3xOp3gNnSc2	jejich
používání	používání	k1gNnSc2	používání
i	i	k9	i
při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
<g/>
,	,	kIx,	,
než	než	k8xS	než
jmenovitých	jmenovitý	k2eAgNnPc6d1	jmenovité
napětích	napětí	k1gNnPc6	napětí
<g/>
:	:	kIx,	:
příkon	příkon	k1gInSc1	příkon
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
úměrný	úměrný	k2eAgInSc1d1	úměrný
kvadrátu	kvadrát	k1gInSc3	kvadrát
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
produkovaný	produkovaný	k2eAgInSc4d1	produkovaný
světelný	světelný	k2eAgInSc4d1	světelný
zářivý	zářivý	k2eAgInSc4d1	zářivý
výkon	výkon	k1gInSc4	výkon
se	se	k3xPyFc4	se
s	s	k7c7	s
napětím	napětí	k1gNnSc7	napětí
plynule	plynule	k6eAd1	plynule
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
regulaci	regulace	k1gFnSc4	regulace
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
tzv.	tzv.	kA	tzv.
stmívače	stmívač	k1gInPc1	stmívač
<g/>
.	.	kIx.	.
</s>
<s>
Stmívače	Stmívač	k1gInPc1	Stmívač
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
nepoužitelné	použitelný	k2eNgInPc1d1	nepoužitelný
u	u	k7c2	u
obyčejných	obyčejný	k2eAgFnPc2d1	obyčejná
zářivek	zářivka	k1gFnPc2	zářivka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
zářivka	zářivka	k1gFnSc1	zářivka
se	se	k3xPyFc4	se
jím	on	k3xPp3gInSc7	on
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vysoko	vysoko	k6eAd1	vysoko
přežhavených	přežhavený	k2eAgFnPc2d1	přežhavená
žárovek	žárovka	k1gFnPc2	žárovka
(	(	kIx(	(
<g/>
také	také	k9	také
halogenových	halogenový	k2eAgMnPc2d1	halogenový
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
provozní	provozní	k2eAgInSc1d1	provozní
tlak	tlak	k1gInSc1	tlak
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
brát	brát	k5eAaImF	brát
na	na	k7c4	na
zřetel	zřetel	k1gInSc4	zřetel
a	a	k8xC	a
omezit	omezit	k5eAaPmF	omezit
možnost	možnost	k1gFnSc4	možnost
exploze	exploze	k1gFnSc2	exploze
speciálním	speciální	k2eAgNnSc7d1	speciální
sklem	sklo	k1gNnSc7	sklo
<g/>
,	,	kIx,	,
síťkou	síťka	k1gFnSc7	síťka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
optická	optický	k2eAgNnPc4d1	optické
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
reflektory	reflektor	k1gInPc4	reflektor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
rizikem	riziko	k1gNnSc7	riziko
rozlétnutí	rozlétnutí	k1gNnSc2	rozlétnutí
žhavých	žhavý	k2eAgInPc2d1	žhavý
střípků	střípek	k1gInPc2	střípek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
žárovky	žárovka	k1gFnPc1	žárovka
bývají	bývat	k5eAaImIp3nP	bývat
plněny	plnit	k5eAaImNgInP	plnit
halogenidovými	halogenidový	k2eAgFnPc7d1	halogenidová
sloučeninami	sloučenina	k1gFnPc7	sloučenina
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
čistým	čistý	k2eAgInSc7d1	čistý
jódem	jód	k1gInSc7	jód
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
různými	různý	k2eAgFnPc7d1	různá
organickými	organický	k2eAgFnPc7d1	organická
sloučeninami	sloučenina	k1gFnPc7	sloučenina
bromu	brom	k1gInSc2	brom
(	(	kIx(	(
<g/>
brommetan	brommetan	k1gInSc1	brommetan
<g/>
,	,	kIx,	,
bromofosfonitrit	bromofosfonitrit	k1gInSc1	bromofosfonitrit
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Halogenová	halogenový	k2eAgFnSc1d1	halogenová
žárovka	žárovka	k1gFnSc1	žárovka
<g/>
.	.	kIx.	.
</s>
<s>
Halogenová	halogenový	k2eAgFnSc1d1	halogenová
žárovka	žárovka	k1gFnSc1	žárovka
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgInSc4d1	speciální
druh	druh	k1gInSc4	druh
žárovky	žárovka	k1gFnSc2	žárovka
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
vlákna	vlákno	k1gNnSc2	vlákno
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
vyšší	vysoký	k2eAgFnSc2d2	vyšší
světelné	světelný	k2eAgFnSc2d1	světelná
účinnosti	účinnost	k1gFnSc2	účinnost
a	a	k8xC	a
bělejšího	bílý	k2eAgNnSc2d2	bělejší
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
delší	dlouhý	k2eAgFnSc2d2	delší
životnosti	životnost	k1gFnSc2	životnost
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
uvnitř	uvnitř	k7c2	uvnitř
baňky	baňka	k1gFnSc2	baňka
přidá	přidat	k5eAaPmIp3nS	přidat
sloučenina	sloučenina	k1gFnSc1	sloučenina
halového	halový	k2eAgInSc2d1	halový
prvku	prvek	k1gInSc2	prvek
(	(	kIx(	(
<g/>
halogenu	halogen	k1gInSc2	halogen
<g/>
,	,	kIx,	,
např.	např.	kA	např.
bromu	brom	k1gInSc2	brom
nebo	nebo	k8xC	nebo
jódu	jód	k1gInSc2	jód
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žárovce	žárovka	k1gFnSc6	žárovka
probíhá	probíhat	k5eAaImIp3nS	probíhat
tzv.	tzv.	kA	tzv.
halogenidový	halogenidový	k2eAgInSc1d1	halogenidový
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
teplotě	teplota	k1gFnSc6	teplota
vypařující	vypařující	k2eAgInSc1d1	vypařující
wolfram	wolfram	k1gInSc1	wolfram
slučuje	slučovat	k5eAaImIp3nS	slučovat
a	a	k8xC	a
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
např.	např.	kA	např.
s	s	k7c7	s
bromem	brom	k1gInSc7	brom
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tenzi	tenze	k1gFnSc3	tenze
wolframových	wolframový	k2eAgFnPc2d1	wolframová
par	para	k1gFnPc2	para
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vlákna	vlákno	k1gNnSc2	vlákno
se	se	k3xPyFc4	se
omezuje	omezovat	k5eAaImIp3nS	omezovat
jeho	jeho	k3xOp3gNnSc4	jeho
vypařování	vypařování	k1gNnSc4	vypařování
-	-	kIx~	-
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
o	o	k7c4	o
10	[number]	k4	10
%	%	kIx~	%
delší	dlouhý	k2eAgFnSc1d2	delší
životnost	životnost	k1gFnSc1	životnost
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc1	zvýšení
světelného	světelný	k2eAgInSc2d1	světelný
toku	tok	k1gInSc2	tok
(	(	kIx(	(
<g/>
měrný	měrný	k2eAgInSc1d1	měrný
zářivý	zářivý	k2eAgInSc1d1	zářivý
výkon	výkon	k1gInSc1	výkon
až	až	k9	až
20	[number]	k4	20
lm	lm	k?	lm
<g/>
/	/	kIx~	/
<g/>
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nahrazování	nahrazování	k1gNnSc2	nahrazování
žárovek	žárovka	k1gFnPc2	žárovka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
delší	dlouhý	k2eAgFnSc4d2	delší
životnost	životnost	k1gFnSc4	životnost
a	a	k8xC	a
lepší	dobrý	k2eAgFnSc4d2	lepší
energetickou	energetický	k2eAgFnSc4d1	energetická
účinnost	účinnost	k1gFnSc4	účinnost
jsou	být	k5eAaImIp3nP	být
žárovky	žárovka	k1gFnPc1	žárovka
nahrazovány	nahrazován	k2eAgFnPc1d1	nahrazována
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
i	i	k8xC	i
nuceně	nuceně	k6eAd1	nuceně
právními	právní	k2eAgFnPc7d1	právní
regulacemi	regulace	k1gFnPc7	regulace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
zářivkami	zářivka	k1gFnPc7	zářivka
nebo	nebo	k8xC	nebo
výbojkami	výbojka	k1gFnPc7	výbojka
<g/>
.	.	kIx.	.
</s>
<s>
Snahou	snaha	k1gFnSc7	snaha
je	být	k5eAaImIp3nS	být
využít	využít	k5eAaPmF	využít
pro	pro	k7c4	pro
osvětlování	osvětlování	k1gNnSc4	osvětlování
také	také	k9	také
LED	led	k1gInSc4	led
(	(	kIx(	(
<g/>
svítivé	svítivý	k2eAgFnPc1d1	svítivá
diody	dioda	k1gFnPc1	dioda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
svítivých	svítivý	k2eAgFnPc2d1	svítivá
diod	dioda	k1gFnPc2	dioda
je	být	k5eAaImIp3nS	být
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
životnost	životnost	k1gFnSc1	životnost
<g/>
,	,	kIx,	,
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
častému	častý	k2eAgNnSc3d1	časté
spínání	spínání	k1gNnSc3	spínání
a	a	k8xC	a
okamžitý	okamžitý	k2eAgInSc4d1	okamžitý
start	start	k1gInSc4	start
na	na	k7c4	na
plný	plný	k2eAgInSc4d1	plný
světelný	světelný	k2eAgInSc4d1	světelný
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k8xC	i
možnost	možnost	k1gFnSc4	možnost
nastavit	nastavit	k5eAaPmF	nastavit
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
barvu	barva	k1gFnSc4	barva
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
systémy	systém	k1gInPc1	systém
s	s	k7c7	s
trojicí	trojice	k1gFnSc7	trojice
RGB	RGB	kA	RGB
diod	dioda	k1gFnPc2	dioda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
životnost	životnost	k1gFnSc1	životnost
LED	LED	kA	LED
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
50000	[number]	k4	50000
až	až	k9	až
100000	[number]	k4	100000
provozních	provozní	k2eAgFnPc2d1	provozní
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
LED	LED	kA	LED
osvětlení	osvětlení	k1gNnSc1	osvětlení
je	být	k5eAaImIp3nS	být
také	také	k9	také
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
vyšší	vysoký	k2eAgFnSc2d2	vyšší
provozní	provozní	k2eAgFnSc2d1	provozní
účinnosti	účinnost	k1gFnSc2	účinnost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
98	[number]	k4	98
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
klasických	klasický	k2eAgFnPc2d1	klasická
žárovek	žárovka	k1gFnPc2	žárovka
s	s	k7c7	s
wolframovým	wolframový	k2eAgNnSc7d1	wolframové
vláknem	vlákno	k1gNnSc7	vlákno
se	se	k3xPyFc4	se
provozní	provozní	k2eAgFnSc1d1	provozní
účinnost	účinnost	k1gFnSc1	účinnost
vztažená	vztažený	k2eAgFnSc1d1	vztažená
k	k	k7c3	k
citlivosti	citlivost	k1gFnSc3	citlivost
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
pouhých	pouhý	k2eAgNnPc2d1	pouhé
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
srovnávání	srovnávání	k1gNnSc6	srovnávání
LED	LED	kA	LED
žárovek	žárovka	k1gFnPc2	žárovka
a	a	k8xC	a
klasických	klasický	k2eAgFnPc2d1	klasická
žárovek	žárovka	k1gFnPc2	žárovka
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
srovnávat	srovnávat	k5eAaImF	srovnávat
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
wattech	watt	k1gInPc6	watt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
raději	rád	k6eAd2	rád
světelný	světelný	k2eAgInSc4d1	světelný
tok	tok	k1gInSc4	tok
v	v	k7c6	v
lumenech	lumen	k1gInPc6	lumen
<g/>
.	.	kIx.	.
</s>
<s>
Watty	watt	k1gInPc1	watt
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zavádějící	zavádějící	k2eAgMnSc1d1	zavádějící
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
srovnání	srovnání	k1gNnSc1	srovnání
zastarává	zastarávat	k5eAaImIp3nS	zastarávat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
LED	LED	kA	LED
žárovek	žárovka	k1gFnPc2	žárovka
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc7d2	vyšší
účinností	účinnost	k1gFnSc7	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
pro	pro	k7c4	pro
klasickou	klasický	k2eAgFnSc4d1	klasická
žárovku	žárovka	k1gFnSc4	žárovka
na	na	k7c4	na
srovnání	srovnání	k1gNnSc4	srovnání
na	na	k7c4	na
lumeny	lumen	k1gMnPc4	lumen
<g/>
:	:	kIx,	:
</s>
