<s>
Zlotý	zlotý	k1gInSc1
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
złoty	złota	k1gMnSc2
[	[	kIx(
<g/>
zuoty	zuota	k1gMnSc2
<g/>
]	]	kIx)
<g/>
,	,	kIx,
hovorově	hovorově	k6eAd1
złotówka	złotówka	k6eAd1
[	[	kIx(
<g/>
zuotuvka	zuotuvka	k1gFnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
polské	polský	k2eAgFnSc2d1
měny	měna	k1gFnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc4d1
zkratku	zkratka	k1gFnSc4
PLN	PLN	kA
a	a	k8xC
dělí	dělit	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
100	#num#	k4
grošů	groš	k1gInPc2
<g/>
.	.	kIx.
</s>