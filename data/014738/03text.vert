<s>
Zlotý	zlotý	k1gInSc1
</s>
<s>
Polský	polský	k2eAgMnSc1d1
zlotýZemě	zlotýZemě	k6eAd1
</s>
<s>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
PLN	pln	k2eAgMnSc1d1
Inflace	inflace	k1gFnSc2
</s>
<s>
–	–	k?
<g/>
0,9	0,9	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
,	,	kIx,
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
zł	zł	k?
–	–	k?
zlotý	zlotý	k1gInSc1
gr	gr	k?
–	–	k?
groš	groš	k1gInSc1
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
groš	groš	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
grošů	groš	k1gInPc2
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
zlotých	zlotý	k1gInPc2
Bankovky	bankovka	k1gFnSc2
</s>
<s>
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
zlotých	zlotý	k1gInPc2
</s>
<s>
Zlotý	zlotý	k1gInSc1
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
złoty	złota	k1gMnSc2
[	[	kIx(
<g/>
zuoty	zuota	k1gMnSc2
<g/>
]	]	kIx)
<g/>
,	,	kIx,
hovorově	hovorově	k6eAd1
złotówka	złotówka	k6eAd1
[	[	kIx(
<g/>
zuotuvka	zuotuvka	k1gFnSc1
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednotka	jednotka	k1gFnSc1
polské	polský	k2eAgFnSc2d1
měny	měna	k1gFnSc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc4d1
zkratku	zkratka	k1gFnSc4
PLN	PLN	kA
a	a	k8xC
dělí	dělit	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
100	#num#	k4
grošů	groš	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
2017	#num#	k4
kurz	kurz	k1gInSc1
kolísal	kolísat	k5eAaImAgInS
mezi	mezi	k7c4
5,997	5,997	k4
<g/>
-	-	kIx~
<g/>
6,409	6,409	k4
CZK	CZK	kA
za	za	k7c4
1	#num#	k4
PLN	plno	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vznik	vznik	k1gInSc1
</s>
<s>
Do	do	k7c2
oběhu	oběh	k1gInSc2
byl	být	k5eAaImAgInS
nově	nově	k6eAd1
uveden	uvést	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
peněžní	peněžní	k2eAgFnSc7d1
reformou	reforma	k1gFnSc7
ministra	ministr	k1gMnSc2
financí	finance	k1gFnPc2
Władysława	Władysławus	k1gMnSc2
Grabského	Grabský	k2eAgMnSc2d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nahradil	nahradit	k5eAaPmAgMnS
polskou	polský	k2eAgFnSc4d1
marku	marka	k1gFnSc4
<g/>
,	,	kIx,
prozatímní	prozatímní	k2eAgFnSc4d1
měnovou	měnový	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
prošla	projít	k5eAaPmAgFnS
hyperinflací	hyperinflace	k1gFnSc7
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnota	hodnota	k1gFnSc1
nové	nový	k2eAgFnSc2d1
měny	měna	k1gFnSc2
byla	být	k5eAaImAgFnS
stanovena	stanovit	k5eAaPmNgFnS
na	na	k7c4
0,1687	0,1687	k4
gramů	gram	k1gInPc2
zlata	zlato	k1gNnSc2
za	za	k7c4
1	#num#	k4
zlotý	zlotý	k1gInSc1
s	s	k7c7
výměnným	výměnný	k2eAgInSc7d1
kursem	kurs	k1gInSc7
1	#num#	k4
zlotý	zlotý	k1gInSc1
=	=	kIx~
1	#num#	k4
800	#num#	k4
000	#num#	k4
polských	polský	k2eAgFnPc2d1
marek	marka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
navrhovanými	navrhovaný	k2eAgInPc7d1
názvy	název	k1gInPc7
nové	nový	k2eAgFnSc2d1
měny	měna	k1gFnSc2
Druhé	druhý	k4xOgFnSc2
polské	polský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byl	být	k5eAaImAgInS
například	například	k6eAd1
„	„	k?
<g/>
lech	lech	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
pol	pola	k1gFnPc2
<g/>
“	“	k?
i	i	k8xC
„	„	k?
<g/>
zlotý	zlotý	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
nejvíce	hodně	k6eAd3,k6eAd1
smysluplný	smysluplný	k2eAgInSc4d1
název	název	k1gInSc4
<g/>
,	,	kIx,
tj.	tj.	kA
zlotý	zlotý	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
navazoval	navazovat	k5eAaImAgInS
na	na	k7c4
historickou	historický	k2eAgFnSc4d1
peněžní	peněžní	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
Polského	polský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
nová	nový	k2eAgFnSc1d1
měnová	měnový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
byl	být	k5eAaImAgInS
zlotý	zlotý	k1gInSc1
schválen	schválit	k5eAaPmNgInS
Sejmem	Sejm	k1gInSc7
již	již	k6eAd1
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1919	#num#	k4
a	a	k8xC
krátce	krátce	k6eAd1
nato	nato	k6eAd1
byly	být	k5eAaImAgFnP
také	také	k9
vytištěny	vytisknout	k5eAaPmNgFnP
první	první	k4xOgFnPc1
bankovky	bankovka	k1gFnPc1
této	tento	k3xDgFnSc2
měny	měna	k1gFnSc2
(	(	kIx(
<g/>
s	s	k7c7
názvem	název	k1gInSc7
emisního	emisní	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
Bank	bank	k1gInSc1
Polski	Polsk	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ještě	ještě	k6eAd1
neexistoval	existovat	k5eNaImAgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydání	vydání	k1gNnSc1
těchto	tento	k3xDgFnPc2
nových	nový	k2eAgFnPc2d1
bankovek	bankovka	k1gFnPc2
<g/>
,	,	kIx,
zavedení	zavedení	k1gNnSc4
nové	nový	k2eAgFnSc2d1
měny	měna	k1gFnSc2
a	a	k8xC
založení	založení	k1gNnSc2
Banku	bank	k1gInSc2
Polského	polský	k2eAgInSc2d1
bylo	být	k5eAaImAgNnS
však	však	k9
odloženo	odložit	k5eAaPmNgNnS
až	až	k6eAd1
na	na	k7c6
období	období	k1gNnSc6
po	po	k7c6
dosažení	dosažení	k1gNnSc6
určité	určitý	k2eAgFnSc2d1
ekonomické	ekonomický	k2eAgFnSc2d1
a	a	k8xC
finanční	finanční	k2eAgFnSc2d1
stabilizace	stabilizace	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
v	v	k7c6
praxi	praxe	k1gFnSc6
nastoupilo	nastoupit	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zůstala	zůstat	k5eAaPmAgFnS
tedy	tedy	k9
prozatímní	prozatímní	k2eAgFnSc7d1
měnovou	měnový	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
polská	polský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
s	s	k7c7
emisním	emisní	k2eAgInSc7d1
ústavem	ústav	k1gInSc7
Polska	Polsko	k1gNnSc2
Krajowa	Krajowum	k1gNnSc2
Kasa	kasa	k1gFnSc1
Pożyczkowa	Pożyczkowa	k1gFnSc1
(	(	kIx(
<g/>
PKKP	PKKP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Předchůdci	předchůdce	k1gMnPc1
</s>
<s>
Jako	jako	k8xC,k8xS
zloté	zlotý	k1gInPc4
byly	být	k5eAaImAgInP
již	již	k6eAd1
v	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
označovány	označován	k2eAgInPc4d1
zahraniční	zahraniční	k2eAgInPc4d1
zlaté	zlatý	k2eAgInPc4d1
dukáty	dukát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
oběhu	oběh	k1gInSc6
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
zprvu	zprvu	k6eAd1
dělené	dělený	k2eAgInPc1d1
na	na	k7c4
14	#num#	k4
grošů	groš	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodnutím	rozhodnutí	k1gNnSc7
Sejmu	Sejm	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1496	#num#	k4
byl	být	k5eAaImAgInS
pevně	pevně	k6eAd1
stanoven	stanovit	k5eAaPmNgInS
kurz	kurz	k1gInSc1
zlotého	zlotý	k1gInSc2
na	na	k7c4
30	#num#	k4
grošů	groš	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
času	čas	k1gInSc2
nebylo	být	k5eNaImAgNnS
možno	možno	k6eAd1
tento	tento	k3xDgInSc4
kurz	kurz	k1gInSc4
udržet	udržet	k5eAaPmF
<g/>
,	,	kIx,
zejména	zejména	k9
z	z	k7c2
důvodu	důvod	k1gInSc2
změn	změna	k1gFnPc2
parity	parita	k1gFnSc2
ceny	cena	k1gFnSc2
zlata	zlato	k1gNnSc2
a	a	k8xC
stříbra	stříbro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
početní	početní	k2eAgFnSc1d1
peněžní	peněžní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
odpovídala	odpovídat	k5eAaImAgFnS
30	#num#	k4
grošům	groš	k1gInPc3
<g/>
,	,	kIx,
označována	označován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
nebo	nebo	k8xC
floren	florna	k1gFnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
zlaté	zlatý	k2eAgInPc1d1
dukáty	dukát	k1gInPc1
začaly	začít	k5eAaPmAgInP
být	být	k5eAaImF
označovány	označovat	k5eAaImNgInP
názvem	název	k1gInSc7
červený	červený	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
hodnota	hodnota	k1gFnSc1
1	#num#	k4
červeného	červený	k2eAgInSc2d1
zlotého	zlotý	k1gInSc2
rovna	roven	k2eAgFnSc1d1
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
polským	polský	k2eAgInPc3d1
zlotým	zlotý	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
byl	být	k5eAaImAgInS
tedy	tedy	k9
polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
pouze	pouze	k6eAd1
početní	početní	k2eAgFnSc7d1
(	(	kIx(
<g/>
teoretickou	teoretický	k2eAgFnSc7d1
<g/>
)	)	kIx)
měnovou	měnový	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Teprve	teprve	k6eAd1
při	při	k7c6
měnové	měnový	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
krále	král	k1gMnSc2
Stanislava	Stanislav	k1gMnSc2
Augusta	Augusta	k1gMnSc1
roku	rok	k1gInSc2
1766	#num#	k4
se	se	k3xPyFc4
polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
stal	stát	k5eAaPmAgInS
reálnou	reálný	k2eAgFnSc7d1
základní	základní	k2eAgFnSc7d1
měnovou	měnový	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
o	o	k7c6
kurzu	kurz	k1gInSc6
30	#num#	k4
grošů	groš	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
stav	stav	k1gInSc1
přetrval	přetrvat	k5eAaPmAgInS
i	i	k9
v	v	k7c6
historických	historický	k2eAgNnPc6d1
obdobích	období	k1gNnPc6
Varšavského	varšavský	k2eAgNnSc2d1
knížectví	knížectví	k1gNnSc2
a	a	k8xC
Kongresového	kongresový	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varšavská	varšavský	k2eAgFnSc1d1
mincovna	mincovna	k1gFnSc1
razila	razit	k5eAaImAgFnS
zloté	zlotý	k1gInPc4
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1841	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
německé	německý	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
zůstal	zůstat	k5eAaPmAgInS
zlotý	zlotý	k1gInSc4
měnovou	měnový	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
na	na	k7c6
území	území	k1gNnSc6
Generálního	generální	k2eAgInSc2d1
gouvernementu	gouvernement	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reforma	reforma	k1gFnSc1
PLZ	PLZ	kA
>	>	kIx)
PLN	pln	k2eAgInSc1d1
</s>
<s>
Z	z	k7c2
důvodu	důvod	k1gInSc2
vysoké	vysoký	k2eAgFnSc2d1
inflace	inflace	k1gFnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
byla	být	k5eAaImAgFnS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
provedena	provést	k5eAaPmNgFnS
v	v	k7c4
Polsku	Polska	k1gFnSc4
denominace	denominace	k1gFnSc2
zlotého	zlotý	k1gInSc2
o	o	k7c4
čtyři	čtyři	k4xCgFnPc4
nuly	nula	k1gFnPc4
(	(	kIx(
<g/>
1	#num#	k4
PLN	plno	k1gNnPc2
=	=	kIx~
10	#num#	k4
000	#num#	k4
PLZ	PLZ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měna	měna	k1gFnSc1
tehdy	tehdy	k6eAd1
obdržela	obdržet	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
kód	kód	k1gInSc4
ISO	ISO	kA
4217	#num#	k4
-	-	kIx~
PLN	plno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
druhé	druhý	k4xOgFnSc2
republiky	republika	k1gFnSc2
i	i	k9
v	v	k7c6
poválečné	poválečný	k2eAgFnSc6d1
době	doba	k1gFnSc6
nadále	nadále	k6eAd1
razí	razit	k5eAaImIp3nS
mince	mince	k1gFnSc1
Varšavská	varšavský	k2eAgFnSc1d1
mincovna	mincovna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Součet	součet	k1gInSc4
hodnot	hodnota	k1gFnPc2
všech	všecek	k3xTgFnPc2
dnešních	dnešní	k2eAgFnPc2d1
polských	polský	k2eAgFnPc2d1
platných	platný	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
a	a	k8xC
bankovek	bankovka	k1gFnPc2
činí	činit	k5eAaImIp3nS
888,88	888,88	k4
zł	zł	k?
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnSc1
</s>
<s>
V	v	k7c6
polským	polský	k2eAgInPc3d1
oběhu	oběh	k1gInSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
nyní	nyní	k6eAd1
platných	platný	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
devíti	devět	k4xCc2
nominálních	nominální	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
:	:	kIx,
1	#num#	k4
gr	gr	k?
<g/>
,	,	kIx,
2	#num#	k4
gr	gr	k?
<g/>
,	,	kIx,
5	#num#	k4
gr	gr	k?
<g/>
,	,	kIx,
10	#num#	k4
gr	gr	k?
<g/>
,	,	kIx,
20	#num#	k4
gr	gr	k?
<g/>
,	,	kIx,
50	#num#	k4
gr	gr	k?
<g/>
,	,	kIx,
1	#num#	k4
zł	zł	k?
<g/>
,	,	kIx,
2	#num#	k4
zł	zł	k?
a	a	k8xC
5	#num#	k4
zł	zł	k?
<g/>
.	.	kIx.
</s>
<s>
Aversy	avers	k1gInPc4
všech	všecek	k3xTgFnPc2
polských	polský	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
ukazují	ukazovat	k5eAaImIp3nP
státní	státní	k2eAgInSc4d1
znak	znak	k1gInSc4
Polska	Polsko	k1gNnSc2
-	-	kIx~
Bílou	bílý	k2eAgFnSc4d1
orlici	orlice	k1gFnSc4
s	s	k7c7
korunou	koruna	k1gFnSc7
<g/>
,	,	kIx,
polský	polský	k2eAgInSc1d1
název	název	k1gInSc1
státu	stát	k1gInSc2
(	(	kIx(
<g/>
RZECZPOSPOLITA	RZECZPOSPOLITA	kA
POLSKA	Polska	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
rok	rok	k1gInSc4
ražby	ražba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
byl	být	k5eAaImAgInS
vytvořen	vytvořit	k5eAaPmNgInS
nový	nový	k2eAgInSc1d1
vzor	vzor	k1gInSc1
aversu	avers	k1gInSc2
-	-	kIx~
státní	státní	k2eAgInSc1d1
název	název	k1gInSc1
a	a	k8xC
rok	rok	k1gInSc1
pod	pod	k7c4
orlici	orlice	k1gFnSc4
-	-	kIx~
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
u	u	k7c2
mincí	mince	k1gFnPc2
s	s	k7c7
nominální	nominální	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
1	#num#	k4
groš	groš	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
groše	groš	k1gInSc2
a	a	k8xC
5	#num#	k4
grošů	groš	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
objevil	objevit	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
na	na	k7c6
mincích	mince	k1gFnPc6
10	#num#	k4
grošů	groš	k1gInPc2
<g/>
,	,	kIx,
20	#num#	k4
grošů	groš	k1gInPc2
<g/>
,	,	kIx,
50	#num#	k4
grošů	groš	k1gInPc2
a	a	k8xC
1	#num#	k4
zlotý	zlotý	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dřívější	dřívější	k2eAgInSc1d1
vzor	vzor	k1gInSc1
lícové	lícový	k2eAgFnSc2d1
strany	strana	k1gFnSc2
-	-	kIx~
orlice	orlice	k1gFnSc1
obklopená	obklopený	k2eAgFnSc1d1
rokem	rok	k1gInSc7
zdola	zdola	k6eAd1
a	a	k8xC
názvem	název	k1gInSc7
státu	stát	k1gInSc2
z	z	k7c2
jiných	jiný	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
-	-	kIx~
je	být	k5eAaImIp3nS
zachován	zachovat	k5eAaPmNgInS
na	na	k7c6
mincích	mince	k1gFnPc6
2	#num#	k4
zł	zł	k?
a	a	k8xC
5	#num#	k4
zł	zł	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mince	mince	k1gFnSc1
se	s	k7c7
starým	starý	k2eAgInSc7d1
lícem	líc	k1gInSc7
jsou	být	k5eAaImIp3nP
stále	stále	k6eAd1
v	v	k7c6
oběhu	oběh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Polské	polský	k2eAgFnPc4d1
mince	mince	k1gFnPc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Denominace	denominace	k1gFnSc1
</s>
<s>
Ražena	ražen	k2eAgFnSc1d1
od	od	k7c2
</s>
<s>
Vydávaná	vydávaný	k2eAgFnSc1d1
od	od	k7c2
</s>
<s>
Průměr	průměr	k1gInSc1
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
</s>
<s>
Materiál	materiál	k1gInSc1
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
Hrana	hrana	k1gFnSc1
</s>
<s>
Návrh	návrh	k1gInSc1
</s>
<s>
1	#num#	k4
groš	groš	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
gr	gr	k?
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
(	(	kIx(
<g/>
Reedice	reedice	k1gFnSc2
<g/>
:	:	kIx,
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
15,5	15,5	k4
mm	mm	kA
</s>
<s>
1,64	1,64	k4
g	g	kA
</s>
<s>
ocel	ocel	k1gFnSc1
pokovená	pokovený	k2eAgFnSc1d1
mosazí	mosaz	k1gFnSc7
(	(	kIx(
<g/>
do	do	k7c2
2014	#num#	k4
manganová	manganový	k2eAgFnSc1d1
mosaz	mosaz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
hnědá	hnědat	k5eAaImIp3nS
<g/>
/	/	kIx~
<g/>
měděná	měděný	k2eAgNnPc1d1
</s>
<s>
vroubkovaná	vroubkovaný	k2eAgFnSc1d1
</s>
<s>
Avers	avers	k1gInSc1
do	do	k7c2
2014	#num#	k4
<g/>
:	:	kIx,
Stanisława	Stanisław	k1gInSc2
Wątróbska-FrindtAvers	Wątróbska-FrindtAvers	k1gInSc1
od	od	k7c2
2014	#num#	k4
<g/>
:	:	kIx,
Sebastian	Sebastian	k1gMnSc1
MikołajczakRevers	MikołajczakReversa	k1gFnPc2
<g/>
:	:	kIx,
Ewa	Ewa	k1gFnSc1
Tyc-Karpińska	Tyc-Karpińska	k1gFnSc1
</s>
<s>
2	#num#	k4
groše	groš	k1gInSc2
(	(	kIx(
<g/>
2	#num#	k4
gr	gr	k?
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
(	(	kIx(
<g/>
Reedice	reedice	k1gFnSc2
<g/>
:	:	kIx,
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
17,5	17,5	k4
mm	mm	kA
</s>
<s>
2,13	2,13	k4
g	g	kA
</s>
<s>
ocel	ocel	k1gFnSc1
pokovená	pokovený	k2eAgFnSc1d1
mosazí	mosaz	k1gFnSc7
(	(	kIx(
<g/>
do	do	k7c2
2014	#num#	k4
manganová	manganový	k2eAgFnSc1d1
mosaz	mosaz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
hnědá	hnědat	k5eAaImIp3nS
<g/>
/	/	kIx~
<g/>
měděná	měděný	k2eAgNnPc1d1
</s>
<s>
hladká	hladký	k2eAgFnSc1d1
</s>
<s>
Avers	avers	k1gInSc1
do	do	k7c2
2014	#num#	k4
<g/>
:	:	kIx,
Stanisława	Stanisław	k1gInSc2
Wątróbska-FrindtAvers	Wątróbska-FrindtAvers	k1gInSc1
od	od	k7c2
2014	#num#	k4
<g/>
:	:	kIx,
Sebastian	Sebastian	k1gMnSc1
MikołajczakRevers	MikołajczakReversa	k1gFnPc2
<g/>
:	:	kIx,
Ewa	Ewa	k1gFnSc1
Tyc-Karpińska	Tyc-Karpińska	k1gFnSc1
</s>
<s>
5	#num#	k4
grošů	groš	k1gInPc2
(	(	kIx(
<g/>
5	#num#	k4
gr	gr	k?
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
(	(	kIx(
<g/>
Reedice	reedice	k1gFnSc2
<g/>
:	:	kIx,
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
17,5	17,5	k4
mm	mm	kA
</s>
<s>
2,59	2,59	k4
g	g	kA
</s>
<s>
ocel	ocel	k1gFnSc1
pokovená	pokovený	k2eAgFnSc1d1
mosazí	mosaz	k1gFnSc7
(	(	kIx(
<g/>
do	do	k7c2
2014	#num#	k4
manganová	manganový	k2eAgFnSc1d1
mosaz	mosaz	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
hnědá	hnědat	k5eAaImIp3nS
<g/>
/	/	kIx~
<g/>
měděná	měděný	k2eAgNnPc1d1
</s>
<s>
střídavě	střídavě	k6eAd1
hladká	hladký	k2eAgFnSc1d1
a	a	k8xC
vroubkovaná	vroubkovaný	k2eAgFnSc1d1
</s>
<s>
Avers	avers	k1gInSc1
do	do	k7c2
2014	#num#	k4
<g/>
:	:	kIx,
Stanisława	Stanisław	k1gInSc2
Wątróbska-FrindtAvers	Wątróbska-FrindtAvers	k1gInSc1
od	od	k7c2
2014	#num#	k4
<g/>
:	:	kIx,
Sebastian	Sebastian	k1gMnSc1
MikołajczakRevers	MikołajczakReversa	k1gFnPc2
<g/>
:	:	kIx,
Ewa	Ewa	k1gFnSc1
Tyc-Karpińska	Tyc-Karpińska	k1gFnSc1
</s>
<s>
10	#num#	k4
grošů	groš	k1gInPc2
(	(	kIx(
<g/>
10	#num#	k4
gr	gr	k?
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
(	(	kIx(
<g/>
Reedice	reedice	k1gFnSc2
<g/>
:	:	kIx,
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2017	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
16,5	16,5	k4
mm	mm	kA
</s>
<s>
2,51	2,51	k4
g	g	kA
</s>
<s>
ocel	ocel	k1gFnSc1
pokovená	pokovený	k2eAgFnSc1d1
mědí	měď	k1gFnSc7
a	a	k8xC
niklem	nikl	k1gInSc7
(	(	kIx(
<g/>
do	do	k7c2
2020	#num#	k4
mědinikl	mědiniknout	k5eAaPmAgMnS
<g/>
)	)	kIx)
</s>
<s>
stříbrná	stříbrnat	k5eAaImIp3nS
</s>
<s>
střídavě	střídavě	k6eAd1
hladká	hladký	k2eAgFnSc1d1
a	a	k8xC
vroubkovaná	vroubkovaný	k2eAgFnSc1d1
</s>
<s>
Avers	avers	k1gInSc1
do	do	k7c2
2017	#num#	k4
<g/>
:	:	kIx,
Stanisława	Stanisław	k1gInSc2
Wątróbska-FrindtAvers	Wątróbska-FrindtAvers	k1gInSc1
od	od	k7c2
2017	#num#	k4
<g/>
:	:	kIx,
Sebastian	Sebastian	k1gMnSc1
MikołajczakRevers	MikołajczakReversa	k1gFnPc2
<g/>
:	:	kIx,
Ewa	Ewa	k1gFnSc1
Tyc-Karpińska	Tyc-Karpińska	k1gFnSc1
</s>
<s>
20	#num#	k4
grošů	groš	k1gInPc2
(	(	kIx(
<g/>
20	#num#	k4
gr	gr	k?
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
(	(	kIx(
<g/>
Reedice	reedice	k1gFnSc2
<g/>
:	:	kIx,
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
18,5	18,5	k4
mm	mm	kA
</s>
<s>
3,22	3,22	k4
g	g	kA
</s>
<s>
ocel	ocel	k1gFnSc1
pokovená	pokovený	k2eAgFnSc1d1
mědí	měď	k1gFnSc7
a	a	k8xC
niklem	nikl	k1gInSc7
(	(	kIx(
<g/>
do	do	k7c2
2020	#num#	k4
mědinikl	mědiniknout	k5eAaPmAgMnS
<g/>
)	)	kIx)
</s>
<s>
stříbrná	stříbrnat	k5eAaImIp3nS
</s>
<s>
vroubkovaná	vroubkovaný	k2eAgFnSc1d1
</s>
<s>
Avers	avers	k1gInSc1
do	do	k7c2
2017	#num#	k4
<g/>
:	:	kIx,
Stanisława	Stanisław	k1gInSc2
Wątróbska-FrindtAvers	Wątróbska-FrindtAvers	k1gInSc1
od	od	k7c2
2017	#num#	k4
<g/>
:	:	kIx,
Sebastian	Sebastian	k1gMnSc1
MikołajczakRevers	MikołajczakReversa	k1gFnPc2
<g/>
:	:	kIx,
Ewa	Ewa	k1gFnSc1
Tyc-Karpińska	Tyc-Karpińska	k1gFnSc1
</s>
<s>
50	#num#	k4
grošů	groš	k1gInPc2
(	(	kIx(
<g/>
50	#num#	k4
gr	gr	k?
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
(	(	kIx(
<g/>
Reedice	reedice	k1gFnSc2
<g/>
:	:	kIx,
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
20,5	20,5	k4
mm	mm	kA
</s>
<s>
3,94	3,94	k4
g	g	kA
</s>
<s>
ocel	ocel	k1gFnSc1
pokovená	pokovený	k2eAgFnSc1d1
mědí	měď	k1gFnSc7
a	a	k8xC
niklem	nikl	k1gInSc7
(	(	kIx(
<g/>
do	do	k7c2
2020	#num#	k4
mědinikl	mědiniknout	k5eAaPmAgMnS
<g/>
)	)	kIx)
</s>
<s>
stříbrná	stříbrnat	k5eAaImIp3nS
</s>
<s>
vroubkovaná	vroubkovaný	k2eAgFnSc1d1
</s>
<s>
Avers	avers	k1gInSc1
do	do	k7c2
2017	#num#	k4
<g/>
:	:	kIx,
Stanisława	Stanisław	k1gInSc2
Wątróbska-FrindtAvers	Wątróbska-FrindtAvers	k1gInSc1
od	od	k7c2
2017	#num#	k4
<g/>
:	:	kIx,
Sebastian	Sebastian	k1gMnSc1
MikołajczakRevers	MikołajczakReversa	k1gFnPc2
<g/>
:	:	kIx,
Ewa	Ewa	k1gFnSc1
Tyc-Karpińska	Tyc-Karpińska	k1gFnSc1
</s>
<s>
1	#num#	k4
zlotý	zlotý	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
zł	zł	k?
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
(	(	kIx(
<g/>
Reedice	reedice	k1gFnSc2
<g/>
:	:	kIx,
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2017	#num#	k4
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
23	#num#	k4
mm	mm	kA
</s>
<s>
5	#num#	k4
g	g	kA
</s>
<s>
ocel	ocel	k1gFnSc1
pokovená	pokovený	k2eAgFnSc1d1
mědí	měď	k1gFnSc7
a	a	k8xC
niklem	nikl	k1gInSc7
(	(	kIx(
<g/>
do	do	k7c2
2020	#num#	k4
mědinikl	mědiniknout	k5eAaPmAgMnS
<g/>
)	)	kIx)
</s>
<s>
stříbrná	stříbrnat	k5eAaImIp3nS
</s>
<s>
střídavě	střídavě	k6eAd1
hladká	hladký	k2eAgFnSc1d1
a	a	k8xC
vroubkovaná	vroubkovaný	k2eAgFnSc1d1
</s>
<s>
Avers	avers	k1gInSc1
do	do	k7c2
2017	#num#	k4
<g/>
:	:	kIx,
Stanisława	Stanisław	k1gInSc2
Wątróbska-FrindtAvers	Wątróbska-FrindtAvers	k1gInSc1
od	od	k7c2
2017	#num#	k4
<g/>
:	:	kIx,
Sebastian	Sebastian	k1gMnSc1
MikołajczakRevers	MikołajczakReversa	k1gFnPc2
<g/>
:	:	kIx,
Ewa	Ewa	k1gFnSc1
Tyc-Karpińska	Tyc-Karpińska	k1gFnSc1
</s>
<s>
2	#num#	k4
zloté	zlotý	k1gInPc4
(	(	kIx(
<g/>
2	#num#	k4
zł	zł	k?
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
21,5	21,5	k4
mm	mm	kA
(	(	kIx(
<g/>
střed	střed	k1gInSc1
<g/>
:	:	kIx,
12	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
mezikruh	mezikruh	k1gMnSc1
<g/>
:	:	kIx,
9,5	9,5	k4
mm	mm	kA
<g/>
)	)	kIx)
</s>
<s>
5,21	5,21	k4
g	g	kA
</s>
<s>
Mezikruh	Mezikruh	k1gInSc1
<g/>
:	:	kIx,
hliníkový	hliníkový	k2eAgInSc4d1
bronz	bronz	k1gInSc4
Střed	střed	k1gInSc1
<g/>
:	:	kIx,
mědinikl	mědinikl	k1gInSc1
</s>
<s>
Mezikruh	Mezikruh	k1gInSc1
<g/>
:	:	kIx,
zlatá	zlatý	k2eAgFnSc1d1
Střed	střed	k1gInSc1
<g/>
:	:	kIx,
stříbrná	stříbrnat	k5eAaImIp3nS
</s>
<s>
hladká	hladký	k2eAgFnSc1d1
</s>
<s>
Ewa	Ewa	k?
Tyc-Karpińska	Tyc-Karpińska	k1gFnSc1
</s>
<s>
5	#num#	k4
zlotých	zlotý	k1gInPc2
(	(	kIx(
<g/>
5	#num#	k4
zł	zł	k?
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
24	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
střed	střed	k1gInSc1
<g/>
:	:	kIx,
16	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
mezikruh	mezikruh	k1gMnSc1
<g/>
:	:	kIx,
8	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
</s>
<s>
6,54	6,54	k4
g	g	kA
</s>
<s>
Mezikruh	Mezikruh	k1gInSc1
<g/>
:	:	kIx,
mědinikl	mědinikl	k1gInSc1
Střed	střed	k1gInSc1
<g/>
:	:	kIx,
hliníkový	hliníkový	k2eAgInSc4d1
bronz	bronz	k1gInSc4
</s>
<s>
Mezikruh	Mezikruh	k1gInSc1
<g/>
:	:	kIx,
stříbrná	stříbrná	k1gFnSc1
Střed	středa	k1gFnPc2
<g/>
:	:	kIx,
zlatá	zlatý	k2eAgFnSc1d1
</s>
<s>
hlavně	hlavně	k9
vroubkovaná	vroubkovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
hladká	hladký	k2eAgFnSc1d1
</s>
<s>
Ewa	Ewa	k?
Tyc-Karpińska	Tyc-Karpińska	k1gFnSc1
</s>
<s>
Bankovky	bankovka	k1gFnPc1
</s>
<s>
V	v	k7c6
polským	polský	k2eAgInPc3d1
oběhu	oběh	k1gInSc2
se	se	k3xPyFc4
dnes	dnes	k6eAd1
používá	používat	k5eAaImIp3nS
platných	platný	k2eAgFnPc2d1
bankovek	bankovka	k1gFnPc2
šesti	šest	k4xCc2
nominálních	nominální	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
:	:	kIx,
10	#num#	k4
zł	zł	k?
<g/>
,	,	kIx,
20	#num#	k4
zł	zł	k?
<g/>
,	,	kIx,
50	#num#	k4
zł	zł	k?
<g/>
,	,	kIx,
100	#num#	k4
zł	zł	k?
<g/>
,	,	kIx,
200	#num#	k4
zł	zł	k?
a	a	k8xC
500	#num#	k4
zł	zł	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvních	první	k4xOgInPc2
pět	pět	k4xCc4
bylo	být	k5eAaImAgNnS
představeno	představit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
ta	ten	k3xDgNnPc1
poslední	poslední	k2eAgNnPc1d1
pouzle	pouzle	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Polské	polský	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
patrně	patrně	k6eAd1
podléhají	podléhat	k5eAaImIp3nP
autorskému	autorský	k2eAgNnSc3d1
právu	právo	k1gNnSc3
<g/>
,	,	kIx,
proto	proto	k8xC
zde	zde	k6eAd1
nejsou	být	k5eNaImIp3nP
vyobrazeny	vyobrazen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současné	současný	k2eAgFnPc1d1
platné	platný	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
navrženy	navrhnout	k5eAaPmNgInP
Andrzejem	Andrzej	k1gMnSc7
Heydrichem	Heydrich	k1gMnSc7
jsou	být	k5eAaImIp3nP
vydavané	vydavan	k1gMnPc1
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
ukazují	ukazovat	k5eAaImIp3nP
obrazy	obraz	k1gInPc1
polských	polský	k2eAgMnPc2d1
vládců	vládce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Polské	polský	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
od	od	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Denominace	denominace	k1gFnSc1
</s>
<s>
Vydávaná	vydávaný	k2eAgFnSc1d1
od	od	k7c2
</s>
<s>
Rozměry	rozměra	k1gFnPc1
</s>
<s>
Barva	barva	k1gFnSc1
</s>
<s>
Označení	označení	k1gNnSc1
pro	pro	k7c4
nevidomé	nevidomý	k1gMnPc4
</s>
<s>
Vladce	Vladka	k1gFnSc3
</s>
<s>
10	#num#	k4
zlotých	zlotý	k1gInPc2
(	(	kIx(
<g/>
10	#num#	k4
zł	zł	k?
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
120	#num#	k4
×	×	k?
60	#num#	k4
mm	mm	kA
</s>
<s>
hnědá	hnědat	k5eAaImIp3nS
</s>
<s>
čtverec	čtverec	k1gInSc1
</s>
<s>
Měšek	Měšek	k1gMnSc1
I.	I.	kA
</s>
<s>
20	#num#	k4
zlotých	zlotý	k1gInPc2
(	(	kIx(
<g/>
20	#num#	k4
zł	zł	k?
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
126	#num#	k4
×	×	k?
63	#num#	k4
mm	mm	kA
</s>
<s>
nachová	nachovat	k5eAaBmIp3nS,k5eAaPmIp3nS
</s>
<s>
kruh	kruh	k1gInSc1
</s>
<s>
Boleslav	Boleslav	k1gMnSc1
I.	I.	kA
Chrabrý	chrabrý	k2eAgInSc4d1
</s>
<s>
50	#num#	k4
zlotých	zlotý	k1gInPc2
(	(	kIx(
<g/>
50	#num#	k4
zł	zł	k?
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1995	#num#	k4
</s>
<s>
132	#num#	k4
×	×	k?
66	#num#	k4
mm	mm	kA
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
kosočtverec	kosočtverec	k1gInSc1
</s>
<s>
Kazimír	Kazimír	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veliký	veliký	k2eAgInSc1d1
</s>
<s>
100	#num#	k4
zlotých	zlotý	k1gInPc2
(	(	kIx(
<g/>
100	#num#	k4
zł	zł	k?
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1995	#num#	k4
</s>
<s>
138	#num#	k4
×	×	k?
69	#num#	k4
mm	mm	kA
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
</s>
<s>
kříž	kříž	k1gInSc1
</s>
<s>
Vladislav	Vladislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jagello	Jagello	k1gNnSc1
</s>
<s>
200	#num#	k4
zlotých	zlotý	k1gInPc2
(	(	kIx(
<g/>
200	#num#	k4
zł	zł	k?
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1995	#num#	k4
</s>
<s>
144	#num#	k4
×	×	k?
72	#num#	k4
mm	mm	kA
</s>
<s>
žlutá	žlutý	k2eAgFnSc1d1
</s>
<s>
trojúhelník	trojúhelník	k1gInSc1
</s>
<s>
Zikmund	Zikmund	k1gMnSc1
I.	I.	kA
Starý	Starý	k1gMnSc1
</s>
<s>
500	#num#	k4
zlotých	zlotý	k1gInPc2
(	(	kIx(
<g/>
500	#num#	k4
zł	zł	k?
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2017	#num#	k4
</s>
<s>
150	#num#	k4
x	x	k?
75	#num#	k4
mm	mm	kA
</s>
<s>
červená	červený	k2eAgFnSc1d1
a	a	k8xC
hnědá	hnědý	k2eAgFnSc1d1
</s>
<s>
dvě	dva	k4xCgFnPc1
rovnoběžné	rovnoběžný	k2eAgFnPc1d1
vodorovné	vodorovný	k2eAgFnPc1d1
čáry	čára	k1gFnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sobieski	Sobieski	k1gNnSc1
</s>
<s>
Historické	historický	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
</s>
<s>
Bankovka	bankovka	k1gFnSc1
20	#num#	k4
zlotých	zlotý	k1gInPc2
(	(	kIx(
<g/>
bez	bez	k7c2
orla	orel	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vzor	vzor	k1gInSc4
1940	#num#	k4
vydaná	vydaný	k2eAgFnSc1d1
německou	německý	k2eAgFnSc7d1
okupační	okupační	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1553	#num#	k4
<g/>
-	-	kIx~
<g/>
8133	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.kurzy.cz/kurzy-men/grafy/nr/CZK-PLN/od-15.10.2016/	http://www.kurzy.cz/kurzy-men/grafy/nr/CZK-PLN/od-15.10.2016/	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Polské	polský	k2eAgInPc1d1
euromince	eurominec	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
zlotý	zlotý	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
zlotý	zlotý	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Bankovky	bankovka	k1gFnPc1
vydané	vydaný	k2eAgFnPc1d1
Polskou	polský	k2eAgFnSc7d1
národní	národní	k2eAgFnSc7d1
bankou	banka	k1gFnSc7
</s>
<s>
Mince	mince	k1gFnPc4
vydané	vydaný	k2eAgFnPc4d1
Polskou	polský	k2eAgFnSc7d1
národní	národní	k2eAgFnSc7d1
bankou	banka	k1gFnSc7
</s>
<s>
Nová	nový	k2eAgFnSc1d1
bankovka	bankovka	k1gFnSc1
500	#num#	k4
zlotých	zlotý	k1gInPc2
(	(	kIx(
<g/>
video	video	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
bankovka	bankovka	k1gFnSc1
500	#num#	k4
zlotých	zlotý	k1gInPc2
(	(	kIx(
<g/>
text	text	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
bankovka	bankovka	k1gFnSc1
500	#num#	k4
zlotých	zlotý	k1gInPc2
(	(	kIx(
<g/>
fotografie	fotografia	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnSc2
Evropy	Evropa	k1gFnSc2
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1
lev	lev	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Chorvatská	chorvatský	k2eAgFnSc1d1
kuna	kuna	k1gFnSc1
•	•	k?
Euro	euro	k1gNnSc1
•	•	k?
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
•	•	k?
Rumunský	rumunský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Moldavský	moldavský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Podněsterský	podněsterský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
<g/>
)	)	kIx)
Jižní	jižní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánský	albánský	k2eAgInSc1d1
lek	lek	k1gInSc1
•	•	k?
Bosenskohercegovská	bosenskohercegovský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Makedonský	makedonský	k2eAgInSc1d1
denár	denár	k1gInSc1
•	•	k?
Srbský	srbský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
•	•	k?
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Islandská	islandský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Polsko	Polsko	k1gNnSc1
</s>
