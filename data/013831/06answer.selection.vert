<s desamb="1">
Když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
sir	sir	k1gMnSc1
Clemens	Clemens	k1gMnSc1
Markham	Markham	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
Královské	královský	k2eAgFnSc2d1
zeměpisné	zeměpisný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
sehnal	sehnat	k5eAaPmAgMnS
jak	jak	k6eAd1
od	od	k7c2
státu	stát	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k9
od	od	k7c2
bohatých	bohatý	k2eAgMnPc2d1
mecenášů	mecenáš	k1gMnPc2
dostatek	dostatek	k1gInSc4
peněz	peníze	k1gInPc2
a	a	k8xC
prostředků	prostředek	k1gInPc2
<g/>
,	,	kIx,
zbývalo	zbývat	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
pouze	pouze	k6eAd1
jmenovat	jmenovat	k5eAaBmF,k5eAaImF
velitele	velitel	k1gMnSc4
expedice	expedice	k1gFnSc2
<g/>
.	.	kIx.
</s>