<s>
Robert	Robert	k1gMnSc1
Falcon	Falcon	k1gMnSc1
Scott	Scott	k1gMnSc1
</s>
<s>
Robert	Robert	k1gMnSc1
Falcon	Falcon	k1gMnSc1
Scott	Scott	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1868	#num#	k4
<g/>
Plymouth	Plymoutha	k1gFnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1912	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
43	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Rossův	Rossův	k2eAgInSc1d1
šelfový	šelfový	k2eAgInSc1d1
ledovec	ledovec	k1gInSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
podchlazení	podchlazení	k1gNnSc1
a	a	k8xC
Kachexie	kachexie	k1gFnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Stubbington	Stubbington	k1gInSc1
House	house	k1gNnSc1
School	Schoola	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
1880	#num#	k4
<g/>
)	)	kIx)
Povolání	povolání	k1gNnSc2
</s>
<s>
objevitel	objevitel	k1gMnSc1
<g/>
,	,	kIx,
námořní	námořní	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
a	a	k8xC
spisovatel	spisovatel	k1gMnSc1
literatury	literatura	k1gFnSc2
faktu	fakt	k1gInSc2
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Medaile	medaile	k1gFnSc1
patrona	patron	k1gMnSc2
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
<g/>
medaile	medaile	k1gFnSc1
Vega	Vega	k1gMnSc1
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
<g/>
Cullum	Cullum	k1gInSc1
Geographical	Geographical	k1gMnSc1
Medal	Medal	k1gMnSc1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
<g/>
důstojník	důstojník	k1gMnSc1
Řádu	řád	k1gInSc2
čestné	čestný	k2eAgNnSc1d1
legiekomandér	legiekomandér	k1gInSc4
Královského	královský	k2eAgInSc2d1
řádu	řád	k1gInSc2
Viktoriina	Viktoriin	k2eAgInSc2d1
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Kathleen	Kathleen	k1gInSc1
Scottová	Scottová	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1908	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Peter	Peter	k1gMnSc1
Scott	Scott	k1gMnSc1
Rodiče	rodič	k1gMnPc1
</s>
<s>
John	John	k1gMnSc1
Edward	Edward	k1gMnSc1
Scott	Scott	k1gMnSc1
a	a	k8xC
Hannah	Hannah	k1gMnSc1
Cuming	Cuming	k1gInSc4
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Nicola	Nicola	k1gFnSc1
Scott	Scott	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Dafila	Dafila	k1gFnSc1
Kathleen	Kathleen	k1gInSc1
Scott	Scott	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Richard	Richard	k1gMnSc1
Falcon	Falcon	k1gMnSc1
Scott	Scott	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
vnoučata	vnouče	k1gNnPc1
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Herbert	Herbert	k1gInSc1
Ponting	Ponting	k1gInSc1
<g/>
:	:	kIx,
Scott	Scott	k1gMnSc1
na	na	k7c6
základně	základna	k1gFnSc6
Cape	capat	k5eAaImIp3nS
Evans	Evans	k1gInSc4
<g/>
,	,	kIx,
asi	asi	k9
1911	#num#	k4
</s>
<s>
Kapitán	kapitán	k1gMnSc1
Robert	Robert	k1gMnSc1
Falcon	Falcon	k1gMnSc1
Scott	Scott	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1868	#num#	k4
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
nebo	nebo	k8xC
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1912	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
britský	britský	k2eAgMnSc1d1
polárník	polárník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospěl	dochvít	k5eAaPmAgMnS
k	k	k7c3
jižnímu	jižní	k2eAgNnSc3d1
pólu	pólo	k1gNnSc3
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1912	#num#	k4
<g/>
,	,	kIx,
o	o	k7c4
měsíc	měsíc	k1gInSc4
a	a	k8xC
den	den	k1gInSc4
později	pozdě	k6eAd2
než	než	k8xS
Roald	Roald	k1gInSc4
Amundsen	Amundsna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scott	Scott	k2eAgInSc4d1
a	a	k8xC
čtyři	čtyři	k4xCgMnPc1
další	další	k2eAgMnPc1d1
členové	člen	k1gMnPc1
jeho	jeho	k3xOp3gFnSc2
expedice	expedice	k1gFnSc2
zahynuli	zahynout	k5eAaPmAgMnP
na	na	k7c6
cestě	cesta	k1gFnSc6
zpět	zpět	k6eAd1
hladem	hlad	k1gInSc7
<g/>
,	,	kIx,
zimou	zima	k1gFnSc7
a	a	k8xC
vyčerpáním	vyčerpání	k1gNnSc7
v	v	k7c6
mimořádně	mimořádně	k6eAd1
špatném	špatný	k2eAgNnSc6d1
počasí	počasí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stan	stan	k1gInSc1
s	s	k7c7
mrtvými	mrtvý	k2eAgNnPc7d1
těly	tělo	k1gNnPc7
<g/>
,	,	kIx,
deníky	deník	k1gInPc7
a	a	k8xC
sbírkami	sbírka	k1gFnPc7
vědeckého	vědecký	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
našla	najít	k5eAaPmAgFnS
záchranná	záchranný	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
až	až	k9
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1912	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
před	před	k7c7
polárními	polární	k2eAgFnPc7d1
výpravami	výprava	k1gFnPc7
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Plymouthu	Plymouth	k1gInSc6
jako	jako	k9
třetí	třetí	k4xOgMnSc1
ze	z	k7c2
šesti	šest	k4xCc2
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jeho	jeho	k3xOp3gInSc3
budoucímu	budoucí	k2eAgInSc3d1
povolání	povolání	k1gNnSc2
námořního	námořní	k2eAgMnSc4d1
důstojníka	důstojník	k1gMnSc4
ho	on	k3xPp3gNnSc2
předurčovala	předurčovat	k5eAaImAgFnS
rodinná	rodinný	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
<g/>
:	:	kIx,
děd	děd	k1gMnSc1
<g/>
,	,	kIx,
dědovi	dědův	k2eAgMnPc1d1
bratři	bratr	k1gMnPc1
i	i	k8xC
další	další	k2eAgMnPc1d1
předkové	předek	k1gMnPc1
sloužili	sloužit	k5eAaImAgMnP
v	v	k7c6
Královském	královský	k2eAgNnSc6d1
námořnictvu	námořnictvo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
rodině	rodina	k1gFnSc6
civilista	civilista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
námořnictva	námořnictvo	k1gNnSc2
vstoupil	vstoupit	k5eAaPmAgInS
už	už	k6eAd1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
třinácti	třináct	k4xCc6
letech	léto	k1gNnPc6
jako	jako	k9
námořní	námořní	k2eAgMnSc1d1
kadet	kadet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velice	velice	k6eAd1
rychle	rychle	k6eAd1
a	a	k8xC
cílevědomě	cílevědomě	k6eAd1
stoupal	stoupat	k5eAaImAgInS
v	v	k7c6
žebříčku	žebříček	k1gInSc6
hodností	hodnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
vlastních	vlastní	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
byl	být	k5eAaImAgMnS
v	v	k7c6
mládí	mládí	k1gNnSc6
poněkud	poněkud	k6eAd1
líný	líný	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
soustavně	soustavně	k6eAd1
cvičil	cvičit	k5eAaImAgMnS
svou	svůj	k3xOyFgFnSc4
kázeň	kázeň	k1gFnSc4
<g/>
,	,	kIx,
vytrvalost	vytrvalost	k1gFnSc4
a	a	k8xC
vůli	vůle	k1gFnSc4
<g/>
,	,	kIx,
hodně	hodně	k6eAd1
sportoval	sportovat	k5eAaImAgMnS
a	a	k8xC
byl	být	k5eAaImAgMnS
velice	velice	k6eAd1
cílevědomý	cílevědomý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
sloužil	sloužit	k5eAaImAgMnS
na	na	k7c6
bitevní	bitevní	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
Majestic	Majestice	k1gFnPc2
jako	jako	k8xC,k8xS
námořní	námořní	k2eAgMnSc1d1
poručík	poručík	k1gMnSc1
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
velel	velet	k5eAaImAgInS
George	George	k1gInSc1
Egerton	Egerton	k1gInSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
arktický	arktický	k2eAgMnSc1d1
badatel	badatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
v	v	k7c6
něm	on	k3xPp3gInSc6
podnítil	podnítit	k5eAaPmAgInS
zájem	zájem	k1gInSc1
o	o	k7c4
zeměpisné	zeměpisný	k2eAgInPc4d1
výzkumy	výzkum	k1gInPc4
a	a	k8xC
polární	polární	k2eAgInPc1d1
kraje	kraj	k1gInPc1
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
pól	pól	k1gInSc1
</s>
<s>
První	první	k4xOgFnSc1
výprava	výprava	k1gFnSc1
k	k	k7c3
jižnímu	jižní	k2eAgInSc3d1
pólu	pól	k1gInSc3
</s>
<s>
Na	na	k7c6
přelomu	přelom	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
velice	velice	k6eAd1
vzrostl	vzrůst	k5eAaPmAgInS
zájem	zájem	k1gInSc1
o	o	k7c4
nejnepřístupnější	přístupný	k2eNgInPc4d3
kouty	kout	k1gInPc4
naší	náš	k3xOp1gFnSc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Antarktida	Antarktida	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
zájmu	zájem	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Němci	Němec	k1gMnPc7
<g/>
,	,	kIx,
Švédové	Švéd	k1gMnPc1
i	i	k8xC
Skotové	Skot	k1gMnPc1
chystali	chystat	k5eAaImAgMnP
expedici	expedice	k1gFnSc4
na	na	k7c4
tento	tento	k3xDgInSc4
neobydlený	obydlený	k2eNgInSc4d1
kontinent	kontinent	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Británie	Británie	k1gFnSc1
nechtěla	chtít	k5eNaImAgFnS
zůstat	zůstat	k5eAaPmF
pozadu	pozadu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
sir	sir	k1gMnSc1
Clemens	Clemens	k1gMnSc1
Markham	Markham	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
Královské	královský	k2eAgFnSc2d1
zeměpisné	zeměpisný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
sehnal	sehnat	k5eAaPmAgMnS
jak	jak	k6eAd1
od	od	k7c2
státu	stát	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k9
od	od	k7c2
bohatých	bohatý	k2eAgMnPc2d1
mecenášů	mecenáš	k1gMnPc2
dostatek	dostatek	k1gInSc4
peněz	peníze	k1gInPc2
a	a	k8xC
prostředků	prostředek	k1gInPc2
<g/>
,	,	kIx,
zbývalo	zbývat	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
pouze	pouze	k6eAd1
jmenovat	jmenovat	k5eAaBmF,k5eAaImF
velitele	velitel	k1gMnSc4
expedice	expedice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
jím	on	k3xPp3gInSc7
právě	právě	k6eAd1
Robert	Robert	k1gMnSc1
F.	F.	kA
Scott	Scott	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
velice	velice	k6eAd1
brzy	brzy	k6eAd1
energicky	energicky	k6eAd1
ujal	ujmout	k5eAaPmAgMnS
příprav	příprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
expedice	expedice	k1gFnSc1
měla	mít	k5eAaImAgFnS
vůbec	vůbec	k9
jako	jako	k9
první	první	k4xOgFnSc1
podniknout	podniknout	k5eAaPmF
saňové	saňový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
Antarktidy	Antarktida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostředky	prostředek	k1gInPc7
měl	mít	k5eAaImAgMnS
velice	velice	k6eAd1
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
mu	on	k3xPp3gMnSc3
dovolily	dovolit	k5eAaPmAgFnP
opatřit	opatřit	k5eAaPmF
takřka	takřka	k6eAd1
vše	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
si	se	k3xPyFc3
mohl	moct	k5eAaImAgMnS
polární	polární	k2eAgMnSc1d1
výzkumník	výzkumník	k1gMnSc1
v	v	k7c6
těch	ten	k3xDgInPc6
časech	čas	k1gInPc6
přát	přát	k5eAaImF
<g/>
:	:	kIx,
velké	velký	k2eAgFnPc4d1
zásoby	zásoba	k1gFnPc4
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
nejmodernější	moderní	k2eAgFnSc4d3
vědeckou	vědecký	k2eAgFnSc4d1
výzbroj	výzbroj	k1gFnSc4
<g/>
,	,	kIx,
nejmodernější	moderní	k2eAgInPc4d3
oděvy	oděv	k1gInPc4
a	a	k8xC
obuv	obuv	k1gFnSc4
a	a	k8xC
také	také	k9
loď	loď	k1gFnSc4
Discovery	Discovera	k1gFnSc2
<g/>
,	,	kIx,
postavenou	postavený	k2eAgFnSc4d1
přímo	přímo	k6eAd1
na	na	k7c4
tuto	tento	k3xDgFnSc4
cestu	cesta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expedice	expedice	k1gFnSc1
měla	mít	k5eAaImAgFnS
přistát	přistát	k5eAaPmF,k5eAaImF
na	na	k7c4
78	#num#	k4
<g/>
.	.	kIx.
rovnoběžce	rovnoběžka	k1gFnSc6
přímo	přímo	k6eAd1
u	u	k7c2
Velké	velký	k2eAgFnSc2d1
ledové	ledový	k2eAgFnSc2d1
bariéry	bariéra	k1gFnSc2
v	v	k7c6
Rossově	Rossův	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
místo	místo	k1gNnSc4
dorazili	dorazit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1902	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vynikající	vynikající	k2eAgInSc1d1
(	(	kIx(
<g/>
na	na	k7c4
tamní	tamní	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
<g/>
)	)	kIx)
počasí	počasí	k1gNnSc1
jim	on	k3xPp3gMnPc3
umožnilo	umožnit	k5eAaPmAgNnS
prozkoumat	prozkoumat	k5eAaPmF
východní	východní	k2eAgFnPc4d1
části	část	k1gFnPc4
Rossova	Rossův	k2eAgNnSc2d1
moře	moře	k1gNnSc2
a	a	k8xC
objevili	objevit	k5eAaPmAgMnP
hornatou	hornatý	k2eAgFnSc4d1
Zemi	zem	k1gFnSc4
krále	král	k1gMnSc2
Edvarda	Edvard	k1gMnSc2
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
vyzkoušeli	vyzkoušet	k5eAaPmAgMnP
novou	nový	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
výzkumu	výzkum	k1gInSc2
–	–	k?
pomocí	pomocí	k7c2
upoutaného	upoutaný	k2eAgInSc2d1
balónu	balón	k1gInSc2
pozorovali	pozorovat	k5eAaImAgMnP
a	a	k8xC
mapovali	mapovat	k5eAaImAgMnP
z	z	k7c2
výšky	výška	k1gFnSc2
několik	několik	k4yIc4
stovek	stovka	k1gFnPc2
metrů	metr	k1gInPc2
území	území	k1gNnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
až	až	k9
dohlédli	dohlédnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1902	#num#	k4
loď	loď	k1gFnSc1
Discovery	Discovera	k1gFnSc2
zakotvila	zakotvit	k5eAaPmAgFnS
v	v	k7c6
McMurdově	McMurdův	k2eAgInSc6d1
průlivu	průliv	k1gInSc6
u	u	k7c2
výběžku	výběžek	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
díky	díky	k7c3
hluboké	hluboký	k2eAgFnSc3d1
vodě	voda	k1gFnSc3
mohla	moct	k5eAaImAgFnS
kotvit	kotvit	k5eAaImF
až	až	k9
u	u	k7c2
břehu	břeh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomuto	tento	k3xDgInSc3
výběžku	výběžek	k1gInSc3
později	pozdě	k6eAd2
začali	začít	k5eAaPmAgMnP
říkat	říkat	k5eAaImF
Chatový	chatový	k2eAgInSc4d1
(	(	kIx(
<g/>
Hut	Hut	k1gFnSc1
Point	pointa	k1gFnPc2
<g/>
)	)	kIx)
podle	podle	k7c2
chaty	chata	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
zde	zde	k6eAd1
postavili	postavit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgNnPc6
místech	místo	k1gNnPc6
při	při	k7c6
průzkumu	průzkum	k1gInSc6
členitého	členitý	k2eAgInSc2d1
terénu	terén	k1gInSc2
zahynul	zahynout	k5eAaPmAgMnS
námořník	námořník	k1gMnSc1
Vince	Vince	k?
ve	v	k7c6
sněhové	sněhový	k2eAgFnSc6d1
bouři	bouř	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
tragédie	tragédie	k1gFnSc1
Scottem	Scott	k1gInSc7
hluboce	hluboko	k6eAd1
otřásla	otřást	k5eAaPmAgFnS
a	a	k8xC
velice	velice	k6eAd1
dlouho	dlouho	k6eAd1
a	a	k8xC
důkladně	důkladně	k6eAd1
promýšlel	promýšlet	k5eAaImAgMnS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
zabránit	zabránit	k5eAaPmF
takovýmto	takovýto	k3xDgFnPc3
nehodám	nehoda	k1gFnPc3
a	a	k8xC
přitom	přitom	k6eAd1
dosáhnout	dosáhnout	k5eAaPmF
vysokých	vysoký	k2eAgInPc2d1
cílů	cíl	k1gInPc2
expedice	expedice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jaře	jaro	k1gNnSc6
a	a	k8xC
v	v	k7c6
létě	léto	k1gNnSc6
Scott	Scotta	k1gFnPc2
s	s	k7c7
dr	dr	kA
<g/>
.	.	kIx.
Edwardem	Edward	k1gMnSc7
Wilsonem	Wilson	k1gMnSc7
a	a	k8xC
poručíkem	poručík	k1gMnSc7
Ernestem	Ernest	k1gMnSc7
Shackletonem	Shackleton	k1gInSc7
pronikli	proniknout	k5eAaPmAgMnP
až	až	k6eAd1
na	na	k7c4
82	#num#	k4
<g/>
°	°	k?
16	#num#	k4
<g/>
'	'	kIx"
jižní	jižní	k2eAgFnSc2d1
šířky	šířka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestou	cestou	k7c2
zpět	zpět	k6eAd1
Shackleton	Shackleton	k1gInSc4
onemocněl	onemocnět	k5eAaPmAgMnS
kurdějemi	kurděje	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
150	#num#	k4
km	km	kA
cesty	cesta	k1gFnSc2
se	se	k3xPyFc4
dva	dva	k4xCgInPc4
potácející	potácející	k2eAgInPc4d1
se	se	k3xPyFc4
muži	muž	k1gMnPc1
a	a	k8xC
jeden	jeden	k4xCgMnSc1
polomrtvý	polomrtvý	k2eAgMnSc1d1
<g/>
,	,	kIx,
ležící	ležící	k2eAgInSc1d1
na	na	k7c6
saních	saně	k1gFnPc6
<g/>
,	,	kIx,
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
blízkosti	blízkost	k1gFnSc2
své	svůj	k3xOyFgFnSc2
chaty	chata	k1gFnSc2
Discovery	Discovera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počasí	počasí	k1gNnSc1
jim	on	k3xPp3gMnPc3
přálo	přát	k5eAaImAgNnS
a	a	k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
se	se	k3xPyFc4
dovlekli	dovléct	k5eAaPmAgMnP
až	až	k9
na	na	k7c4
základnu	základna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Scott	Scott	k1gMnSc1
velice	velice	k6eAd1
podcenil	podcenit	k5eAaPmAgMnS
možnosti	možnost	k1gFnPc4
a	a	k8xC
výhody	výhoda	k1gFnPc4
plynoucí	plynoucí	k2eAgFnPc4d1
ze	z	k7c2
psích	psí	k2eAgFnPc2d1
spřežení	spřežení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
pouze	pouze	k6eAd1
18	#num#	k4
tažných	tažný	k2eAgMnPc2d1
psů	pes	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potřeboval	potřebovat	k5eAaImAgMnS
by	by	kYmCp3nS
jich	on	k3xPp3gInPc2
nejméně	málo	k6eAd3
pětkrát	pětkrát	k6eAd1
tolik	tolik	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zajistil	zajistit	k5eAaPmAgMnS
tahouny	tahoun	k1gInPc4
pro	pro	k7c4
točnový	točnový	k2eAgInSc4d1
oddíl	oddíl	k1gInSc4
<g/>
,	,	kIx,
pomocná	pomocný	k2eAgFnSc1d1
a	a	k8xC
popřípadě	popřípadě	k6eAd1
i	i	k9
záchranná	záchranný	k2eAgNnPc1d1
družstva	družstvo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zůstal	zůstat	k5eAaPmAgMnS
v	v	k7c6
Antarktidě	Antarktida	k1gFnSc6
další	další	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
točnu	točna	k1gFnSc4
se	se	k3xPyFc4
nepokusil	pokusit	k5eNaPmAgMnS
<g/>
,	,	kIx,
věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
bez	bez	k7c2
zdatných	zdatný	k2eAgInPc2d1
tahounů	tahoun	k1gInPc2
to	ten	k3xDgNnSc1
nepůjde	jít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
expedice	expedice	k1gFnSc1
podnikla	podniknout	k5eAaPmAgFnS
několik	několik	k4yIc4
dalších	další	k2eAgFnPc2d1
výzkumných	výzkumný	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3
byla	být	k5eAaImAgFnS
patrně	patrně	k6eAd1
ta	ten	k3xDgFnSc1
přes	přes	k7c4
Západní	západní	k2eAgFnPc4d1
hory	hora	k1gFnPc4
až	až	k9
do	do	k7c2
nitra	nitro	k1gNnSc2
Viktoriiny	Viktoriin	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
cesta	cesta	k1gFnSc1
dokázala	dokázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Antarktida	Antarktida	k1gFnSc1
je	být	k5eAaImIp3nS
pevnina	pevnina	k1gFnSc1
(	(	kIx(
<g/>
tak	tak	k6eAd1
gigantický	gigantický	k2eAgInSc1d1
ledovec	ledovec	k1gInSc1
musel	muset	k5eAaImAgInS
mít	mít	k5eAaImF
pevný	pevný	k2eAgInSc1d1
základ	základ	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
dodala	dodat	k5eAaPmAgFnS
představu	představa	k1gFnSc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc1
asi	asi	k9
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c6
jižním	jižní	k2eAgNnSc6d1
pólu	pólo	k1gNnSc6
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
i	i	k9
ten	ten	k3xDgMnSc1
leží	ležet	k5eAaImIp3nS
ve	v	k7c6
vnitrozemí	vnitrozemí	k1gNnSc6
<g/>
,	,	kIx,
za	za	k7c7
horskou	horský	k2eAgFnSc7d1
hrází	hráz	k1gFnSc7
podobnou	podobný	k2eAgFnSc4d1
té	ten	k3xDgFnSc2
tvořené	tvořený	k2eAgInPc1d1
Západními	západní	k2eAgFnPc7d1
horami	hora	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
deseti	deset	k4xCc6
týdnech	týden	k1gInPc6
a	a	k8xC
1340	#num#	k4
kilometrech	kilometr	k1gInPc6
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
k	k	k7c3
Discovery	Discovero	k1gNnPc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
už	už	k6eAd1
na	na	k7c4
ně	on	k3xPp3gMnPc4
čekaly	čekat	k5eAaImAgFnP
dvě	dva	k4xCgFnPc4
pomocné	pomocný	k2eAgFnPc4d1
lodi	loď	k1gFnPc4
–	–	k?
Morning	Morning	k1gInSc1
a	a	k8xC
Terra	Terra	k1gFnSc1
Nova	nova	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
této	tento	k3xDgFnSc6
výzkumné	výzkumný	k2eAgFnSc6d1
cestě	cesta	k1gFnSc6
(	(	kIx(
<g/>
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
si	se	k3xPyFc3
svou	svůj	k3xOyFgFnSc7
délkou	délka	k1gFnSc7
a	a	k8xC
náročností	náročnost	k1gFnPc2
nezadala	zadat	k5eNaPmAgFnS
s	s	k7c7
cestou	cesta	k1gFnSc7
k	k	k7c3
pólu	pól	k1gInSc3
<g/>
)	)	kIx)
Scott	Scott	k1gMnSc1
opět	opět	k6eAd1
potvrdil	potvrdit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
mimořádnou	mimořádný	k2eAgFnSc4d1
odvahu	odvaha	k1gFnSc4
<g/>
,	,	kIx,
odolnost	odolnost	k1gFnSc4
a	a	k8xC
vytrvalost	vytrvalost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svým	svůj	k3xOyFgInSc7
charakterem	charakter	k1gInSc7
si	se	k3xPyFc3
dokázal	dokázat	k5eAaPmAgInS
získat	získat	k5eAaPmF
osobní	osobní	k2eAgFnSc4d1
autoritu	autorita	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
uznání	uznání	k1gNnSc4
a	a	k8xC
oblibu	obliba	k1gFnSc4
jak	jak	k6eAd1
mezi	mezi	k7c7
důstojníky	důstojník	k1gMnPc7
<g/>
,	,	kIx,
tak	tak	k9
mezi	mezi	k7c7
mužstvem	mužstvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
některými	některý	k3yIgMnPc7
muži	muž	k1gMnPc7
ho	on	k3xPp3gMnSc2
spojilo	spojit	k5eAaPmAgNnS
i	i	k9
pevné	pevný	k2eAgNnSc1d1
osobní	osobní	k2eAgNnSc1d1
přátelství	přátelství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Expedice	expedice	k1gFnSc1
dorazila	dorazit	k5eAaPmAgFnS
domů	domů	k6eAd1
v	v	k7c6
září	září	k1gNnSc6
1904	#num#	k4
a	a	k8xC
dostalo	dostat	k5eAaPmAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
slavnostního	slavnostní	k2eAgNnSc2d1
přivítání	přivítání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědecký	vědecký	k2eAgInSc1d1
přínos	přínos	k1gInSc1
výpravy	výprava	k1gFnSc2
byl	být	k5eAaImAgInS
obrovský	obrovský	k2eAgInSc1d1
<g/>
:	:	kIx,
získali	získat	k5eAaPmAgMnP
spoustu	spousta	k1gFnSc4
poznatků	poznatek	k1gInPc2
o	o	k7c6
geologii	geologie	k1gFnSc6
<g/>
,	,	kIx,
fauně	fauna	k1gFnSc6
<g/>
,	,	kIx,
podnebí	podnebí	k1gNnSc6
<g/>
,	,	kIx,
zmapovali	zmapovat	k5eAaPmAgMnP
nové	nový	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
…	…	k?
Scott	Scott	k1gMnSc1
však	však	k9
přesto	přesto	k8xC
nebyl	být	k5eNaImAgInS
spokojen	spokojen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toužil	toužit	k5eAaImAgMnS
po	po	k7c6
jediném	jediné	k1gNnSc6
<g/>
:	:	kIx,
vztyčit	vztyčit	k5eAaPmF
britskou	britský	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
na	na	k7c6
jižním	jižní	k2eAgNnSc6d1
pólu	pólo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Shackletonova	Shackletonův	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1908	#num#	k4
<g/>
–	–	k?
<g/>
1909	#num#	k4
</s>
<s>
Ernest	Ernest	k1gMnSc1
Henry	Henry	k1gMnSc1
Shackleton	Shackleton	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
málem	málem	k6eAd1
zahynul	zahynout	k5eAaPmAgInS
při	při	k7c6
Scottově	Scottův	k2eAgFnSc6d1
výpravě	výprava	k1gFnSc6
k	k	k7c3
pólu	pól	k1gInSc3
<g/>
,	,	kIx,
se	se	k3xPyFc4
po	po	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
Anglie	Anglie	k1gFnSc2
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
nedokázal	dokázat	k5eNaPmAgMnS
se	s	k7c7
Scottovou	Scottový	k2eAgFnSc7d1
výpravou	výprava	k1gFnSc7
a	a	k8xC
při	při	k7c6
čem	co	k3yQnSc6,k3yRnSc6,k3yInSc6
málem	málem	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
dokáže	dokázat	k5eAaPmIp3nS
sám	sám	k3xTgMnSc1
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
vlastní	vlastní	k2eAgFnSc7d1
expedicí	expedice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
vrcholilo	vrcholit	k5eAaImAgNnS
úsilí	úsilí	k1gNnSc1
Roberta	Robert	k1gMnSc2
Pearyho	Peary	k1gMnSc2
a	a	k8xC
Fredericka	Fredericko	k1gNnPc1
Cooka	Cooko	k1gNnSc2
o	o	k7c4
Severní	severní	k2eAgInSc4d1
pól	pól	k1gInSc4
(	(	kIx(
<g/>
oba	dva	k4xCgMnPc1
byli	být	k5eAaImAgMnP
Američané	Američan	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jih	jih	k1gInSc1
byl	být	k5eAaImAgInS
dle	dle	k7c2
mínění	mínění	k1gNnSc2
samotných	samotný	k2eAgMnPc2d1
Britů	Brit	k1gMnPc2
jejich	jejich	k3xOp3gFnPc2
záležitostí	záležitost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shackleton	Shackleton	k1gInSc1
chtěl	chtít	k5eAaImAgInS
být	být	k5eAaImF
první	první	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scott	Scott	k1gMnSc1
jen	jen	k9
se	s	k7c7
smíšenými	smíšený	k2eAgInPc7d1
pocity	pocit	k1gInPc7
pozoroval	pozorovat	k5eAaImAgMnS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
„	„	k?
<g/>
chráněnec	chráněnec	k1gMnSc1
<g/>
“	“	k?
a	a	k8xC
přítel	přítel	k1gMnSc1
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yQgMnSc3,k3yRgMnSc3
zachránil	zachránit	k5eAaPmAgInS
život	život	k1gInSc1
<g/>
,	,	kIx,
pouští	pouštět	k5eAaImIp3nS
plnou	plný	k2eAgFnSc7d1
silou	síla	k1gFnSc7
za	za	k7c4
jeho	jeho	k3xOp3gFnSc4
cílem	cíl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Už	už	k6eAd1
z	z	k7c2
této	tento	k3xDgFnSc2
expedice	expedice	k1gFnSc2
lze	lze	k6eAd1
vystopovat	vystopovat	k5eAaPmF
příčiny	příčina	k1gFnPc4
pozdější	pozdní	k2eAgFnSc2d2
tragédie	tragédie	k1gFnSc2
Scottovy	Scottův	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
volba	volba	k1gFnSc1
dopravních	dopravní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shackleton	Shackleton	k1gInSc1
si	se	k3xPyFc3
přivezl	přivézt	k5eAaPmAgInS
9	#num#	k4
psů	pes	k1gMnPc2
<g/>
,	,	kIx,
10	#num#	k4
poníků	poník	k1gMnPc2
a	a	k8xC
čtyřválcový	čtyřválcový	k2eAgInSc1d1
automobil	automobil	k1gInSc1
–	–	k?
minimum	minimum	k1gNnSc1
osvědčených	osvědčený	k2eAgInPc2d1
polárních	polární	k2eAgInPc2d1
tahounů	tahoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spoléhal	spoléhat	k5eAaImAgMnS
hlavně	hlavně	k9
na	na	k7c4
poníky	poník	k1gMnPc4
<g/>
,	,	kIx,
jenže	jenže	k8xC
ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
v	v	k7c6
těžkém	těžký	k2eAgInSc6d1
terénu	terén	k1gInSc6
ploužili	ploužit	k5eAaImAgMnP
a	a	k8xC
byli	být	k5eAaImAgMnP
celkově	celkově	k6eAd1
málo	málo	k6eAd1
odolní	odolný	k2eAgMnPc1d1
vůči	vůči	k7c3
krutému	krutý	k2eAgInSc3d1
mrazu	mráz	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
Shackleton	Shackleton	k1gInSc1
se	s	k7c7
svými	svůj	k3xOyFgInPc7
třemi	tři	k4xCgInPc7
kolegy	kolega	k1gMnPc7
vyrazil	vyrazit	k5eAaPmAgMnS
směrem	směr	k1gInSc7
k	k	k7c3
pólu	pól	k1gInSc3
<g/>
,	,	kIx,
už	už	k6eAd1
v	v	k7c6
polovině	polovina	k1gFnSc6
cesty	cesta	k1gFnSc2
mu	on	k3xPp3gMnSc3
všichni	všechen	k3xTgMnPc1
poníci	poník	k1gMnPc1
uhynuli	uhynout	k5eAaPmAgMnP
a	a	k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
museli	muset	k5eAaImAgMnP
veškerou	veškerý	k3xTgFnSc4
svoji	svůj	k3xOyFgFnSc4
výstroj	výstroj	k1gFnSc4
a	a	k8xC
výzbroj	výzbroj	k1gInSc4
táhnout	táhnout	k5eAaImF
sami	sám	k3xTgMnPc1
zapřaženi	zapřáhnout	k5eAaPmNgMnP
do	do	k7c2
saní	saně	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
pólu	pólo	k1gNnSc3
se	se	k3xPyFc4
přiblížili	přiblížit	k5eAaPmAgMnP
až	až	k9
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
pouhých	pouhý	k2eAgInPc2d1
179	#num#	k4
km	km	kA
<g/>
,	,	kIx,
tam	tam	k6eAd1
je	být	k5eAaImIp3nS
třídenní	třídenní	k2eAgInSc1d1
blizard	blizard	k1gInSc1
a	a	k8xC
nedostatek	nedostatek	k1gInSc1
zásob	zásoba	k1gFnPc2
přinutily	přinutit	k5eAaPmAgFnP
k	k	k7c3
návratu	návrat	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesta	cesta	k1gFnSc1
trvající	trvající	k2eAgFnSc2d1
127	#num#	k4
dní	den	k1gInPc2
málem	málem	k6eAd1
skončila	skončit	k5eAaPmAgFnS
tragicky	tragicky	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
posledních	poslední	k2eAgFnPc2d1
sil	síla	k1gFnPc2
se	se	k3xPyFc4
všichni	všechen	k3xTgMnPc1
4	#num#	k4
muži	muž	k1gMnPc7
úspěšně	úspěšně	k6eAd1
vrátili	vrátit	k5eAaPmAgMnP
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
loď	loď	k1gFnSc4
Nimrod	Nimrod	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ušli	ujít	k5eAaPmAgMnP
2825	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědecký	vědecký	k2eAgInSc1d1
přínos	přínos	k1gInSc1
mise	mise	k1gFnSc2
byl	být	k5eAaImAgInS
také	také	k9
značný	značný	k2eAgInSc1d1
–	–	k?
Západní	západní	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
prof.	prof.	kA
Davida	David	k1gMnSc2
našel	najít	k5eAaPmAgMnS
ve	v	k7c6
Viktoriině	Viktoriin	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
jižní	jižní	k2eAgInSc1d1
magnetický	magnetický	k2eAgInSc1d1
pól	pól	k1gInSc1
a	a	k8xC
několik	několik	k4yIc1
členů	člen	k1gMnPc2
expedice	expedice	k1gFnSc2
uskutečnilo	uskutečnit	k5eAaPmAgNnS
prvovýstup	prvovýstup	k1gInSc4
na	na	k7c4
sopku	sopka	k1gFnSc4
Mount	Mounta	k1gFnPc2
Erebus	Erebus	k1gInSc4
vysokou	vysoká	k1gFnSc4
necelých	celý	k2eNgInPc2d1
4000	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Terra	Terra	k1gFnSc1
Nova	nova	k1gFnSc1
a	a	k8xC
Antarktida	Antarktida	k1gFnSc1
1910	#num#	k4
<g/>
–	–	k?
<g/>
1913	#num#	k4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Expedice	expedice	k1gFnSc2
Terra	Terra	k1gFnSc1
Nova	nova	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Chata	chata	k1gFnSc1
Scottovy	Scottův	k2eAgFnSc2d1
expedice	expedice	k1gFnSc2
v	v	k7c6
dnešních	dnešní	k2eAgInPc6d1
dnech	den	k1gInPc6
</s>
<s>
Během	během	k7c2
plavby	plavba	k1gFnSc2
z	z	k7c2
Anglie	Anglie	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
lodi	loď	k1gFnSc6
Terra	Terro	k1gNnSc2
Nova	novum	k1gNnSc2
objevila	objevit	k5eAaPmAgFnS
trhlina	trhlina	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
musela	muset	k5eAaImAgFnS
na	na	k7c6
Novém	nový	k2eAgInSc6d1
Zélandu	Zéland	k1gInSc6
do	do	k7c2
doku	dok	k1gInSc2
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1910	#num#	k4
znovu	znovu	k6eAd1
vyplula	vyplout	k5eAaPmAgNnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
plavby	plavba	k1gFnSc2
se	se	k3xPyFc4
expedice	expedice	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
velké	velký	k2eAgFnSc2d1
bouřky	bouřka	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
ztratila	ztratit	k5eAaPmAgFnS
několik	několik	k4yIc4
psů	pes	k1gMnPc2
a	a	k8xC
sudů	sud	k1gInPc2
s	s	k7c7
benzinem	benzin	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
několikrát	několikrát	k6eAd1
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
ledového	ledový	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1911	#num#	k4
loď	loď	k1gFnSc1
přistála	přistát	k5eAaPmAgFnS,k5eAaImAgFnS
v	v	k7c6
McMurdově	McMurdův	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
kousek	kousek	k1gInSc4
od	od	k7c2
Chatového	chatový	k2eAgInSc2d1
výběžku	výběžek	k1gInSc2
(	(	kIx(
<g/>
Hut	Hut	k1gFnSc1
Point	pointa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
při	při	k7c6
Scottově	Scottův	k2eAgFnSc6d1
minulé	minulý	k2eAgFnSc6d1
expedici	expedice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
již	již	k9
měli	mít	k5eAaImAgMnP
postavenou	postavený	k2eAgFnSc4d1
a	a	k8xC
plně	plně	k6eAd1
zařízenou	zařízený	k2eAgFnSc4d1
chatu	chata	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
člen	člen	k1gMnSc1
„	„	k?
<g/>
pobřežní	pobřežní	k1gMnSc1
skupiny	skupina	k1gFnSc2
<g/>
“	“	k?
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
na	na	k7c6
expedici	expedice	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
tábořila	tábořit	k5eAaImAgFnS
u	u	k7c2
mysu	mys	k1gInSc2
Evans	Evansa	k1gFnPc2
na	na	k7c6
Rossově	Rossův	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
profesionální	profesionální	k2eAgMnSc1d1
fotograf	fotograf	k1gMnSc1
Herbert	Herbert	k1gMnSc1
Ponting	Ponting	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tábor	tábor	k1gMnSc1
„	„	k?
<g/>
pobřežní	pobřežní	k1gMnSc1
skupiny	skupina	k1gFnSc2
<g/>
“	“	k?
obsahoval	obsahovat	k5eAaImAgInS
malou	malý	k2eAgFnSc4d1
fotografickou	fotografický	k2eAgFnSc4d1
temnou	temný	k2eAgFnSc4d1
komoru	komora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
expedice	expedice	k1gFnSc1
konala	konat	k5eAaImAgFnS
více	hodně	k6eAd2
než	než	k8xS
20	#num#	k4
let	léto	k1gNnPc2
po	po	k7c6
vynálezu	vynález	k1gInSc6
fotografického	fotografický	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
,	,	kIx,
Ponting	Ponting	k1gInSc1
upřednostňoval	upřednostňovat	k5eAaImAgInS
vysoce	vysoce	k6eAd1
kvalitní	kvalitní	k2eAgInPc4d1
a	a	k8xC
komplikovanější	komplikovaný	k2eAgInPc4d2
snímky	snímek	k1gInPc4
pořízené	pořízený	k2eAgInPc4d1
na	na	k7c4
skleněné	skleněný	k2eAgFnPc4d1
fotografické	fotografický	k2eAgFnPc4d1
desky	deska	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Ponting	Ponting	k1gInSc4
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
prvních	první	k4xOgMnPc2
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
v	v	k7c6
Antarktidě	Antarktida	k1gFnSc6
použili	použít	k5eAaPmAgMnP
přenosnou	přenosný	k2eAgFnSc4d1
filmovou	filmový	k2eAgFnSc4d1
kameru	kamera	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Primitivní	primitivní	k2eAgInSc4d1
zařízení	zařízení	k1gNnSc1
zvané	zvaný	k2eAgNnSc1d1
kinematograf	kinematograf	k1gInSc4
mohlo	moct	k5eAaImAgNnS
pořizovat	pořizovat	k5eAaImF
krátké	krátký	k2eAgFnPc4d1
videosekvence	videosekvence	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ponting	Ponting	k1gInSc1
měl	mít	k5eAaImAgInS
s	s	k7c7
sebou	se	k3xPyFc7
také	také	k6eAd1
několik	několik	k4yIc4
autochromových	autochromový	k2eAgFnPc2d1
desek	deska	k1gFnPc2
a	a	k8xC
pořídil	pořídit	k5eAaPmAgMnS
několik	několik	k4yIc4
průkopnických	průkopnický	k2eAgFnPc2d1
barevných	barevný	k2eAgFnPc2d1
fotografií	fotografia	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Expediční	expediční	k2eAgMnPc1d1
vědci	vědec	k1gMnPc1
studovali	studovat	k5eAaImAgMnP
chování	chování	k1gNnSc4
velkých	velká	k1gFnPc2
antarktických	antarktický	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
kosatky	kosatka	k1gFnPc4
dravé	dravý	k2eAgFnPc4d1
<g/>
,	,	kIx,
ploutvonožce	ploutvonožec	k1gMnSc4
(	(	kIx(
<g/>
např.	např.	kA
lachtany	lachtan	k1gMnPc4
<g/>
)	)	kIx)
a	a	k8xC
tučňáky	tučňák	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ponting	Ponting	k1gInSc1
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
dostat	dostat	k5eAaPmF
co	co	k9
nejblíže	blízce	k6eAd3
k	k	k7c3
těmto	tento	k3xDgNnPc3
zvířatům	zvíře	k1gNnPc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
na	na	k7c6
lodi	loď	k1gFnSc6
Terra	Terr	k1gInSc2
Nova	nova	k1gFnSc1
na	na	k7c6
moři	moře	k1gNnSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
na	na	k7c6
ledovci	ledovec	k1gInSc6
a	a	k8xC
později	pozdě	k6eAd2
na	na	k7c6
Rossově	Rossův	k2eAgInSc6d1
ostrově	ostrov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
1911	#num#	k4
ve	v	k7c6
vodách	voda	k1gFnPc6
zálivu	záliv	k1gInSc2
McMurdo	McMurdo	k1gNnSc1
Sound	Sound	k1gInSc1
Herbert	Herbert	k1gMnSc1
Ponting	Ponting	k1gInSc1
jen	jen	k9
těsně	těsně	k6eAd1
unikl	uniknout	k5eAaPmAgMnS
smrti	smrt	k1gFnSc3
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
kvůli	kvůli	k7c3
hejnu	hejno	k1gNnSc6
osmi	osm	k4xCc2
kosatek	kosatka	k1gFnPc2
odlomila	odlomit	k5eAaPmAgFnS
ledová	ledový	k2eAgFnSc1d1
kra	kra	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
zůstal	zůstat	k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
i	i	k8xC
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
kamerou	kamera	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Herbert	Herbert	k1gInSc1
Ponting	Ponting	k1gInSc1
<g/>
:	:	kIx,
Lawrence	Lawrence	k1gFnSc1
'	'	kIx"
<g/>
Titus	Titus	k1gMnSc1
<g/>
'	'	kIx"
Oates	Oates	k1gMnSc1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
koňmi	kůň	k1gMnPc7
a	a	k8xC
psy	pes	k1gMnPc7
<g/>
,	,	kIx,
asi	asi	k9
1911	#num#	k4
</s>
<s>
Herbert	Herbert	k1gInSc1
Ponting	Ponting	k1gInSc1
<g/>
:	:	kIx,
Ledovec	ledovec	k1gInSc1
</s>
<s>
Herbert	Herbert	k1gInSc1
Ponting	Ponting	k1gInSc1
<g/>
:	:	kIx,
Scottova	Scottův	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Terra	Terra	k1gFnSc1
Nova	nova	k1gFnSc1
</s>
<s>
Během	během	k7c2
zimy	zima	k1gFnSc2
1911	#num#	k4
pořídil	pořídit	k5eAaPmAgInS
Ponting	Ponting	k1gInSc1
s	s	k7c7
bleskem	blesk	k1gInSc7
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
fotografií	fotografia	k1gFnPc2
Scotta	Scott	k1gInSc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
členů	člen	k1gMnPc2
expedice	expedice	k1gFnSc2
v	v	k7c6
chatě	chata	k1gFnSc6
Cape	capat	k5eAaImIp3nS
Evans	Evans	k1gInSc1
(	(	kIx(
<g/>
Cape	capat	k5eAaImIp3nS
Evans	Evans	k1gInSc4
hut	hut	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátek	začátek	k1gInSc1
sezóny	sezóna	k1gFnSc2
1911	#num#	k4
<g/>
–	–	k?
<g/>
1912	#num#	k4
Ponting	Ponting	k1gInSc1
fotografoval	fotografovat	k5eAaImAgInS
další	další	k2eAgInPc4d1
členy	člen	k1gInPc4
výpravy	výprava	k1gFnSc2
na	na	k7c4
pobřeží	pobřeží	k1gNnSc4
a	a	k8xC
čekali	čekat	k5eAaImAgMnP
zda	zda	k8xS
trek	trek	k6eAd1
bude	být	k5eAaImBp3nS
úspěšný	úspěšný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
14	#num#	k4
měsících	měsíc	k1gInPc6
na	na	k7c6
mysu	mys	k1gInSc6
Evans	Evansa	k1gFnPc2
nastoupil	nastoupit	k5eAaPmAgInS
Ponting	Ponting	k1gInSc1
spolu	spolu	k6eAd1
s	s	k7c7
osmi	osm	k4xCc7
dalšími	další	k2eAgMnPc7d1
muži	muž	k1gMnPc7
na	na	k7c4
loď	loď	k1gFnSc4
Terra	Terro	k1gNnSc2
Nova	novum	k1gNnSc2
a	a	k8xC
v	v	k7c6
únoru	únor	k1gInSc6
1912	#num#	k4
se	se	k3xPyFc4
rozhodli	rozhodnout	k5eAaPmAgMnP
k	k	k7c3
návratu	návrat	k1gInSc3
do	do	k7c2
civilizace	civilizace	k1gFnSc2
<g/>
,	,	kIx,
zajistit	zajistit	k5eAaPmF
soupis	soupis	k1gInSc4
více	hodně	k6eAd2
než	než	k8xS
1700	#num#	k4
fotografických	fotografický	k2eAgFnPc2d1
desek	deska	k1gFnPc2
a	a	k8xC
sepsat	sepsat	k5eAaPmF
celý	celý	k2eAgInSc4d1
příběh	příběh	k1gInSc4
expedice	expedice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ponting	Ponting	k1gInSc4
ještě	ještě	k9
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
vyprávění	vyprávění	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
očekával	očekávat	k5eAaImAgInS
návrat	návrat	k1gInSc1
kapitána	kapitán	k1gMnSc2
Scotta	Scott	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Expedice	expedice	k1gFnSc1
na	na	k7c4
jižní	jižní	k2eAgInSc4d1
pól	pól	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scottova	Scottův	k2eAgFnSc1d1
(	(	kIx(
<g/>
zelená	zelený	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
Amundsenova	Amundsenův	k2eAgFnSc1d1
(	(	kIx(
<g/>
červená	červený	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Scottova	Scottův	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
však	však	k9
skončila	skončit	k5eAaPmAgFnS
nezdarem	nezdar	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sibiřští	sibiřský	k2eAgMnPc1d1
poníci	poník	k1gMnPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
zvolil	zvolit	k5eAaPmAgInS
jako	jako	k9
tažná	tažný	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
neosvědčili	osvědčit	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brodili	brodit	k5eAaImAgMnP
se	se	k3xPyFc4
po	po	k7c4
břicha	břicho	k1gNnPc4
ve	v	k7c6
sněhu	sníh	k1gInSc6
a	a	k8xC
nakonec	nakonec	k6eAd1
museli	muset	k5eAaImAgMnP
být	být	k5eAaImF
utraceni	utracen	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
200	#num#	k4
mil	míle	k1gFnPc2
od	od	k7c2
pólu	pól	k1gInSc2
se	se	k3xPyFc4
Scott	Scott	k1gMnSc1
vydal	vydat	k5eAaPmAgMnS
na	na	k7c4
další	další	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
sám	sám	k3xTgMnSc1
se	s	k7c7
čtyřmi	čtyři	k4xCgInPc7
druhy	druh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sáně	sáně	k1gFnPc4
museli	muset	k5eAaImAgMnP
táhnout	táhnout	k5eAaImF
sami	sám	k3xTgMnPc1
a	a	k8xC
cesta	cesta	k1gFnSc1
byla	být	k5eAaImAgFnS
tak	tak	k6eAd1
strastiplná	strastiplný	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
někdy	někdy	k6eAd1
urazili	urazit	k5eAaPmAgMnP
jen	jen	k9
16	#num#	k4
km	km	kA
za	za	k7c4
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k6eAd1
dosáhli	dosáhnout	k5eAaPmAgMnP
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1912	#num#	k4
jižního	jižní	k2eAgInSc2d1
pólu	pól	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
však	však	k9
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Roald	Roald	k1gInSc1
Amundsen	Amundsen	k2eAgInSc1d1
tam	tam	k6eAd1
již	již	k6eAd1
byl	být	k5eAaImAgInS
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
týdny	týden	k1gInPc4
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cesta	cesta	k1gFnSc1
zpět	zpět	k6eAd1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
pak	pak	k6eAd1
stala	stát	k5eAaPmAgFnS
osudnou	osudný	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastihla	zastihnout	k5eAaPmAgFnS
je	on	k3xPp3gNnSc4
sněhová	sněhový	k2eAgFnSc1d1
vánice	vánice	k1gFnSc1
a	a	k8xC
došly	dojít	k5eAaPmAgFnP
jim	on	k3xPp3gMnPc3
zásoby	zásoba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyčerpáním	vyčerpání	k1gNnSc7
a	a	k8xC
vysílením	vysílení	k1gNnSc7
zahynuli	zahynout	k5eAaPmAgMnP
29	#num#	k4
<g/>
.	.	kIx.
nebo	nebo	k8xC
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1912	#num#	k4
jen	jen	k9
18	#num#	k4
km	km	kA
od	od	k7c2
velkého	velký	k2eAgInSc2d1
zásobovacího	zásobovací	k2eAgInSc2d1
skladu	sklad	k1gInSc2
potravin	potravina	k1gFnPc2
a	a	k8xC
paliva	palivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
těla	tělo	k1gNnPc1
byla	být	k5eAaImAgNnP
nalezena	nalézt	k5eAaBmNgNnP,k5eAaPmNgNnP
až	až	k6eAd1
o	o	k7c6
osm	osm	k4xCc4
měsíců	měsíc	k1gInPc2
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgNnPc4d1
vydání	vydání	k1gNnPc4
Scottových	Scottův	k2eAgInPc2d1
spisů	spis	k1gInPc2
</s>
<s>
Dosažení	dosažení	k1gNnSc1
jižní	jižní	k2eAgFnSc2d1
točny	točna	k1gFnSc2
(	(	kIx(
<g/>
Scottova	Scottův	k2eAgFnSc1d1
poslední	poslední	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1913	#num#	k4
<g/>
,	,	kIx,
český	český	k2eAgInSc4d1
překlad	překlad	k1gInSc4
redigoval	redigovat	k5eAaImAgMnS
Ludvík	Ludvík	k1gMnSc1
Tošner	Tošner	k1gMnSc1
(	(	kIx(
<g/>
Scott	Scott	k1gMnSc1
není	být	k5eNaImIp3nS
původcem	původce	k1gMnSc7
všech	všecek	k3xTgInPc2
textů	text	k1gInPc2
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
sborník	sborník	k1gInSc4
prací	práce	k1gFnPc2
od	od	k7c2
různých	různý	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dosažení	dosažení	k1gNnSc1
jižní	jižní	k2eAgFnSc2d1
točny	točna	k1gFnSc2
(	(	kIx(
<g/>
deník	deník	k1gInSc1
z	z	k7c2
poslední	poslední	k2eAgFnSc2d1
expedice	expedice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1972	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Stanislav	Stanislav	k1gMnSc1
Bártl	Bártl	k1gMnSc1
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
Scottovy	Scottův	k2eAgInPc4d1
zážitky	zážitek	k1gInPc4
od	od	k7c2
vyplutí	vyplutí	k1gNnSc2
z	z	k7c2
Nového	Nového	k2eAgInSc2d1
Zélandu	Zéland	k1gInSc2
v	v	k7c6
listopadu	listopad	k1gInSc6
1910	#num#	k4
až	až	k6eAd1
po	po	k7c4
poslední	poslední	k2eAgInPc4d1
okamžiky	okamžik	k1gInPc4
v	v	k7c6
březnu	březen	k1gInSc6
1912	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mohl	moct	k5eAaImAgInS
ještě	ještě	k6eAd1
psát	psát	k5eAaImF
<g/>
,	,	kIx,
než	než	k8xS
zahynul	zahynout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Darryl	Darryl	k1gInSc1
Roger	Roger	k1gInSc4
Lundy	Lunda	k1gMnSc2
<g/>
:	:	kIx,
The	The	k1gMnSc2
Peerage	Peerag	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Robert	Robert	k1gMnSc1
Falcon	Falcon	k1gMnSc1
Scott	Scott	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Robert	Robert	k1gMnSc1
Falcon	Falcon	k1gMnSc1
Scott	Scott	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Robert	Robert	k1gMnSc1
Falcon	Falcon	k1gMnSc1
Scott	Scott	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
25091	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118612395	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2137	#num#	k4
1976	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50005591	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
66475494	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50005591	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
