<s>
Good	Good	k6eAd1	Good
Charlotte	Charlott	k1gInSc5	Charlott
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
americké	americký	k2eAgFnSc2d1	americká
pop	pop	k1gMnSc1	pop
punkové	punkový	k2eAgFnSc2d1	punková
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
založila	založit	k5eAaPmAgFnS	založit
dvojčata	dvojče	k1gNnPc1	dvojče
Joel	Joel	k1gMnSc1	Joel
Ryan	Ryan	k1gMnSc1	Ryan
Rueben	Ruebna	k1gFnPc2	Ruebna
Madden	Maddna	k1gFnPc2	Maddna
a	a	k8xC	a
Benji	Benje	k1gFnSc4	Benje
Levi	Lev	k1gFnSc2	Lev
Madden	Maddno	k1gNnPc2	Maddno
a	a	k8xC	a
Billy	Bill	k1gMnPc7	Bill
Martin	Martina	k1gFnPc2	Martina
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Anthony	Anthona	k1gFnSc2	Anthona
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
,	,	kIx,	,
Dean	Dean	k1gMnSc1	Dean
Butterworth	Butterworth	k1gMnSc1	Butterworth
v	v	k7c6	v
Marylandu	Maryland	k1gInSc6	Maryland
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
dětské	dětský	k2eAgFnSc2d1	dětská
knížky	knížka	k1gFnSc2	knížka
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ji	on	k3xPp3gFnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nikdy	nikdy	k6eAd1	nikdy
nečetl	číst	k5eNaImAgInS	číst
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vystoupení	vystoupení	k1gNnSc1	vystoupení
skupiny	skupina	k1gFnSc2	skupina
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
sousedově	sousedův	k2eAgInSc6d1	sousedův
sklepě	sklep	k1gInSc6	sklep
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přišlo	přijít	k5eAaPmAgNnS	přijít
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
k	k	k7c3	k
Benjimu	Benjim	k1gMnSc3	Benjim
<g/>
,	,	kIx,	,
Joelovi	Joel	k1gMnSc3	Joel
a	a	k8xC	a
Paulovi	Paul	k1gMnSc3	Paul
přidal	přidat	k5eAaPmAgMnS	přidat
kytarista	kytarista	k1gMnSc1	kytarista
Billy	Bill	k1gMnPc4	Bill
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
kapela	kapela	k1gFnSc1	kapela
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Good	Gooda	k1gFnPc2	Gooda
Charlotte	Charlott	k1gInSc5	Charlott
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
The	The	k1gMnSc1	The
Young	Young	k1gMnSc1	Young
and	and	k?	and
the	the	k?	the
Hopeless	Hopelessa	k1gFnPc2	Hopelessa
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
CD	CD	kA	CD
The	The	k1gFnPc1	The
Chronicles	Chronicles	k1gMnSc1	Chronicles
of	of	k?	of
Life	Life	k1gInSc1	Life
and	and	k?	and
Death	Death	k1gInSc1	Death
(	(	kIx(	(
<g/>
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
verzích	verze	k1gFnPc6	verze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vyšla	vyjít	k5eAaPmAgFnS	vyjít
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
názvem	název	k1gInSc7	název
Good	Good	k1gInSc1	Good
Morning	Morning	k1gInSc1	Morning
Revival	revival	k1gInSc1	revival
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
vysloužila	vysloužit	k5eAaPmAgFnS	vysloužit
spoustu	spousta	k1gFnSc4	spousta
kritiky	kritika	k1gFnSc2	kritika
jak	jak	k8xS	jak
od	od	k7c2	od
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
tak	tak	k9	tak
od	od	k7c2	od
hudebních	hudební	k2eAgMnPc2d1	hudební
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc4	text
i	i	k8xC	i
hudbu	hudba	k1gFnSc4	hudba
si	se	k3xPyFc3	se
skládají	skládat	k5eAaImIp3nP	skládat
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc4	text
píše	psát	k5eAaImIp3nS	psát
většinou	většina	k1gFnSc7	většina
Joel	Joela	k1gFnPc2	Joela
a	a	k8xC	a
Benji	Benje	k1gFnSc4	Benje
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
největší	veliký	k2eAgInSc4d3	veliký
vzor	vzor	k1gInSc4	vzor
skupinu	skupina	k1gFnSc4	skupina
Rancid	Rancid	k1gInSc4	Rancid
a	a	k8xC	a
Benji	Benje	k1gFnSc4	Benje
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
klipu	klip	k1gInSc6	klip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Fall	Fall	k1gMnSc1	Fall
Back	Back	k1gMnSc1	Back
Down	Down	k1gMnSc1	Down
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2017	[number]	k4	2017
se	se	k3xPyFc4	se
Good	Good	k1gMnSc1	Good
Charlotte	Charlott	k1gInSc5	Charlott
zastavili	zastavit	k5eAaPmAgMnP	zastavit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
vystoupení	vystoupení	k1gNnSc1	vystoupení
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
desce	deska	k1gFnSc3	deska
Youth	Youtha	k1gFnPc2	Youtha
Authority	Authorita	k1gFnSc2	Authorita
<g/>
.	.	kIx.	.
</s>
<s>
Demo	demo	k2eAgNnPc2d1	demo
1	[number]	k4	1
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Demo	demo	k2eAgInPc2d1	demo
2	[number]	k4	2
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Another	Anothra	k1gFnPc2	Anothra
EP	EP	kA	EP
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
GC	GC	kA	GC
EP	EP	kA	EP
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Bootlegs	Bootlegsa	k1gFnPc2	Bootlegsa
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Good	Good	k1gInSc1	Good
Charlotte	Charlott	k1gInSc5	Charlott
Video	video	k1gNnSc1	video
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Live	Live	k1gFnSc1	Live
At	At	k1gMnSc1	At
Brixton	Brixton	k1gInSc1	Brixton
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Fast	Fast	k1gInSc1	Fast
Future	Futur	k1gMnSc5	Futur
Generation	Generation	k1gInSc1	Generation
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Joel	Joel	k1gInSc1	Joel
Madden	Maddna	k1gFnPc2	Maddna
-	-	kIx~	-
vokalista	vokalista	k1gMnSc1	vokalista
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
Benji	Benje	k1gFnSc4	Benje
Madden	Maddna	k1gFnPc2	Maddna
-	-	kIx~	-
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
vokalista	vokalista	k1gMnSc1	vokalista
Billy	Bill	k1gMnPc4	Bill
Martin	Martina	k1gFnPc2	Martina
-	-	kIx~	-
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
vokalista	vokalista	k1gMnSc1	vokalista
Paul	Paul	k1gMnSc1	Paul
Thomas	Thomas	k1gMnSc1	Thomas
-	-	kIx~	-
baskytarista	baskytarista	k1gMnSc1	baskytarista
Dean	Dean	k1gMnSc1	Dean
Butterworth	Butterworth	k1gMnSc1	Butterworth
-	-	kIx~	-
bubeník	bubeník	k1gMnSc1	bubeník
</s>
