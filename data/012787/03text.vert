<p>
<s>
Wintersun	Wintersun	k1gInSc1	Wintersun
je	být	k5eAaImIp3nS	být
finská	finský	k2eAgFnSc1d1	finská
melodic	melodice	k1gInPc2	melodice
deathmetalová	deathmetalový	k2eAgFnSc1d1	deathmetalová
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
zpěvákem	zpěvák	k1gMnSc7	zpěvák
a	a	k8xC	a
kytaristou	kytarista	k1gMnSc7	kytarista
Jarim	Jarim	k1gMnSc1	Jarim
Mäenpäänem	Mäenpään	k1gMnSc7	Mäenpään
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Wintersun	Wintersun	k1gInSc1	Wintersun
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
dva	dva	k4xCgInPc4	dva
různé	různý	k2eAgInPc4d1	různý
elementy	element	k1gInPc4	element
<g/>
;	;	kIx,	;
zimu	zima	k1gFnSc4	zima
a	a	k8xC	a
vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
pod	pod	k7c7	pod
eponymním	eponymní	k2eAgInSc7d1	eponymní
názvem	název	k1gInSc7	název
Wintersun	Wintersun	k1gMnSc1	Wintersun
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
deska	deska	k1gFnSc1	deska
ovšem	ovšem	k9	ovšem
vyšla	vyjít	k5eAaPmAgFnS	vyjít
až	až	k9	až
o	o	k7c4	o
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
konceptuální	konceptuální	k2eAgNnSc4d1	konceptuální
album	album	k1gNnSc4	album
Time	Tim	k1gFnSc2	Tim
I	I	kA	I
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
příběh	příběh	k1gInSc1	příběh
měl	mít	k5eAaImAgInS	mít
pokračovat	pokračovat	k5eAaImF	pokračovat
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
desce	deska	k1gFnSc6	deska
Time	Tim	k1gFnSc2	Tim
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Vydáni	vydán	k2eAgMnPc1d1	vydán
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
neustále	neustále	k6eAd1	neustále
odkládalo	odkládat	k5eAaImAgNnS	odkládat
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Mäenpäänena	Mäenpääneno	k1gNnSc2	Mäenpääneno
především	především	k6eAd1	především
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
absence	absence	k1gFnSc2	absence
vlastního	vlastní	k2eAgNnSc2d1	vlastní
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
kvůli	kvůli	k7c3	kvůli
výstavbě	výstavba	k1gFnSc3	výstavba
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
chtěla	chtít	k5eAaImAgFnS	chtít
uspořádat	uspořádat	k5eAaPmF	uspořádat
crowdfundingovou	crowdfundingový	k2eAgFnSc4d1	crowdfundingový
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
ale	ale	k9	ale
zamítlo	zamítnout	k5eAaPmAgNnS	zamítnout
jejich	jejich	k3xOp3gNnSc1	jejich
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Nuclear	Nuclear	k1gMnSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
deska	deska	k1gFnSc1	deska
tedy	tedy	k9	tedy
nakonec	nakonec	k6eAd1	nakonec
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
ale	ale	k9	ale
o	o	k7c4	o
Time	Time	k1gFnSc4	Time
II	II	kA	II
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c4	o
jiné	jiný	k2eAgNnSc4d1	jiné
konceptuální	konceptuální	k2eAgNnSc4d1	konceptuální
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
inspirováno	inspirován	k2eAgNnSc1d1	inspirováno
Vivaldiho	Vivaldiha	k1gFnSc5	Vivaldiha
concerty	concert	k1gInPc1	concert
grosso	grossa	k1gFnSc5	grossa
Čtvero	čtvero	k4xRgNnSc1	čtvero
ročních	roční	k2eAgFnPc2d1	roční
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
pojmenováno	pojmenován	k2eAgNnSc4d1	pojmenováno
The	The	k1gMnPc7	The
Forest	Forest	k1gFnSc4	Forest
Seasons	Seasonsa	k1gFnPc2	Seasonsa
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sestava	sestava	k1gFnSc1	sestava
==	==	k?	==
</s>
</p>
<p>
<s>
Jari	jar	k1gFnPc1	jar
Mäenpää	Mäenpää	k1gFnSc2	Mäenpää
−	−	k?	−
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
od	od	k7c2	od
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Teemu	Teemat	k5eAaPmIp1nS	Teemat
Mäntysaari	Mäntysaare	k1gFnSc4	Mäntysaare
−	−	k?	−
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
od	od	k7c2	od
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Asim	Asim	k1gMnSc1	Asim
Searah	Searah	k1gMnSc1	Searah
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
od	od	k7c2	od
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jukka	Jukka	k6eAd1	Jukka
Koskinen	Koskinen	k1gInSc1	Koskinen
−	−	k?	−
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
od	od	k7c2	od
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kai	Kai	k?	Kai
Hahto	Hahto	k1gNnSc1	Hahto
−	−	k?	−
bicí	bicí	k2eAgFnPc4d1	bicí
(	(	kIx(	(
<g/>
od	od	k7c2	od
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
<s>
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členovéOliver	členovéOliver	k1gMnSc1	členovéOliver
Fokin	Fokin	k1gMnSc1	Fokin
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Wintersun	Wintersun	k1gNnSc1	Wintersun
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Time	Time	k6eAd1	Time
I	i	k9	i
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Forest	Forest	k1gInSc1	Forest
Seasons	Seasons	k1gInSc1	Seasons
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wintersun	Wintersuna	k1gFnPc2	Wintersuna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
