<s>
Věž	věž	k1gFnSc1
byla	být	k5eAaImAgFnS
vybudována	vybudovat	k5eAaPmNgFnS
ve	v	k7c6
středověku	středověk	k1gInSc6
<g/>
,	,	kIx,
nejspíše	nejspíše	k9
na	na	k7c6
počátku	počátek	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dní	den	k1gInPc2
se	se	k3xPyFc4
nedochovaly	dochovat	k5eNaPmAgFnP
žádné	žádný	k3yNgFnPc1
informace	informace	k1gFnPc1
o	o	k7c6
jejím	její	k3xOp3gInSc6
původu	původ	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>