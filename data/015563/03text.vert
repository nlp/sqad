<s>
Šikmá	šikmý	k2eAgFnSc1d1
věž	věž	k1gFnSc1
(	(	kIx(
<g/>
Ząbkowice	Ząbkowice	k1gFnSc1
Śląskie	Śląskie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
věže	věž	k1gFnPc4
</s>
<s>
Šikmá	šikmý	k2eAgFnSc1d1
věž	věž	k1gFnSc1
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
Krzywa	Krzyw	k2eAgFnSc1d1
wieża	wieża	k1gFnSc1
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
k	k	k7c3
pamětihodnostem	pamětihodnost	k1gFnPc3
dolnoslezského	dolnoslezský	k2eAgNnSc2d1
města	město	k1gNnSc2
Ząbkowice	Ząbkowice	k1gFnSc1
Śląskie	Śląskie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
náměstí	náměstí	k1gNnSc2
(	(	kIx(
<g/>
rynku	rynek	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věž	věž	k1gFnSc1
je	být	k5eAaImIp3nS
vysoká	vysoký	k2eAgFnSc1d1
34	#num#	k4
m	m	kA
a	a	k8xC
od	od	k7c2
svého	svůj	k3xOyFgInSc2
středu	střed	k1gInSc2
se	se	k3xPyFc4
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
vychyluje	vychylovat	k5eAaImIp3nS
o	o	k7c4
2,14	2,14	k4
m.	m.	k?
</s>
<s>
Věž	věž	k1gFnSc1
byla	být	k5eAaImAgFnS
vybudována	vybudovat	k5eAaPmNgFnS
ve	v	k7c6
středověku	středověk	k1gInSc6
<g/>
,	,	kIx,
nejspíše	nejspíše	k9
na	na	k7c6
počátku	počátek	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dní	den	k1gInPc2
se	se	k3xPyFc4
nedochovaly	dochovat	k5eNaPmAgFnP
žádné	žádný	k3yNgFnPc1
informace	informace	k1gFnPc1
o	o	k7c6
jejím	její	k3xOp3gInSc6
původu	původ	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
není	být	k5eNaImIp3nS
přesně	přesně	k6eAd1
známo	znám	k2eAgNnSc1d1
její	její	k3xOp3gNnSc4
stáří	stáří	k1gNnSc4
není	být	k5eNaImIp3nS
ani	ani	k8xC
znám	znám	k2eAgInSc1d1
účel	účel	k1gInSc1
<g/>
,	,	kIx,
za	za	k7c7
jakým	jaký	k3yRgNnSc7,k3yQgNnSc7,k3yIgNnSc7
byla	být	k5eAaImAgFnS
věž	věž	k1gFnSc1
zbudována	zbudovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
možností	možnost	k1gFnPc2
<g/>
;	;	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
součást	součást	k1gFnSc4
zoubkovického	zoubkovický	k2eAgInSc2d1
hradu	hrad	k1gInSc2
-	-	kIx~
tuto	tento	k3xDgFnSc4
možnost	možnost	k1gFnSc4
potvrzuje	potvrzovat	k5eAaImIp3nS
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
zdivo	zdivo	k1gNnSc1
spodní	spodní	k2eAgFnSc2d1
části	část	k1gFnSc2
věže	věž	k1gFnSc2
je	být	k5eAaImIp3nS
zbudováno	zbudován	k2eAgNnSc1d1
z	z	k7c2
kamene	kámen	k1gInSc2
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
z	z	k7c2
cihel	cihla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
spodní	spodní	k2eAgFnSc6d1
části	část	k1gFnSc6
(	(	kIx(
<g/>
do	do	k7c2
10	#num#	k4
m	m	kA
výšky	výška	k1gFnSc2
<g/>
)	)	kIx)
dosahuje	dosahovat	k5eAaImIp3nS
tloušťka	tloušťka	k1gFnSc1
zdí	zeď	k1gFnPc2
navíc	navíc	k6eAd1
až	až	k9
čtyř	čtyři	k4xCgInPc2
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
této	tento	k3xDgFnSc2
existují	existovat	k5eAaImIp3nP
i	i	k9
další	další	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
<g/>
,	,	kIx,
např.	např.	kA
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
původně	původně	k6eAd1
věž	věž	k1gFnSc4
středověkého	středověký	k2eAgNnSc2d1
opevnění	opevnění	k1gNnSc2
(	(	kIx(
<g/>
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
posunu	posun	k1gInSc3
hradeb	hradba	k1gFnPc2
dál	daleko	k6eAd2
od	od	k7c2
náměstí	náměstí	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
jen	jen	k9
a	a	k8xC
pouze	pouze	k6eAd1
o	o	k7c4
městskou	městský	k2eAgFnSc4d1
zvonici	zvonice	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
až	až	k9
do	do	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
plnila	plnit	k5eAaImAgFnS
věž	věž	k1gFnSc1
právě	právě	k9
úlohu	úloha	k1gFnSc4
zvonice	zvonice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
osvobození	osvobození	k1gNnSc4
a	a	k8xC
připojení	připojení	k1gNnSc4
původního	původní	k2eAgNnSc2d1
města	město	k1gNnSc2
Frankenstein	Frankenstein	k1gInSc1
k	k	k7c3
Polsku	Polsko	k1gNnSc3
zůstal	zůstat	k5eAaPmAgInS
objekt	objekt	k1gInSc1
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
opuštěný	opuštěný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
rekonstrukce	rekonstrukce	k1gFnSc1
věže	věž	k1gFnSc2
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
a	a	k8xC
další	další	k2eAgNnSc1d1
o	o	k7c4
několik	několik	k4yIc4
dekád	dekáda	k1gFnPc2
později	pozdě	k6eAd2
(	(	kIx(
<g/>
dokončena	dokončen	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
naklonění	naklonění	k1gNnSc3
věže	věž	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
nejspíše	nejspíše	k9
v	v	k7c6
roce	rok	k1gInSc6
1598	#num#	k4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
buď	buď	k8xC
s	s	k7c7
geologickými	geologický	k2eAgInPc7d1
pohyby	pohyb	k1gInPc7
<g/>
,	,	kIx,
či	či	k8xC
podmáčeným	podmáčený	k2eAgNnSc7d1
podložím	podloží	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
se	se	k3xPyFc4
naklonění	naklonění	k1gNnSc4
věže	věž	k1gFnSc2
neustále	neustále	k6eAd1
zvětšuje	zvětšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1977	#num#	k4
a	a	k8xC
2007	#num#	k4
narostlo	narůst	k5eAaPmAgNnS
o	o	k7c4
15	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
požáru	požár	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
město	město	k1gNnSc4
postihl	postihnout	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1858	#num#	k4
byl	být	k5eAaImAgInS
vršek	vršek	k1gInSc1
věže	věž	k1gFnSc2
opatřen	opatřit	k5eAaPmNgInS
kovovou	kovový	k2eAgFnSc7d1
kopulí	kopule	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
něm	on	k3xPp3gInSc6
nachází	nacházet	k5eAaImIp3nS
cimbuří	cimbuří	k1gNnSc1
a	a	k8xC
vyhlídková	vyhlídkový	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
pro	pro	k7c4
turisty	turist	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
Dolního	dolní	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Šikmá	šikmý	k2eAgFnSc1d1
věž	věž	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
památky	památka	k1gFnSc2
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Polsko	Polsko	k1gNnSc1
</s>
