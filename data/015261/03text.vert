<s>
Nick	Nick	k6eAd1
Oshiro	Oshiro	k1gNnSc1
</s>
<s>
Nick	Nick	k6eAd1
Oshiro	Oshiro	k1gNnSc4
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Narození	narození	k1gNnSc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1978	#num#	k4
(	(	kIx(
<g/>
42	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
Žánry	žánr	k1gInPc4
</s>
<s>
nu	nu	k9
metal	metal	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudebník	hudebník	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
bicí	bicí	k2eAgFnSc1d1
souprava	souprava	k1gFnSc1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Reprise	reprisa	k1gFnSc3
Records	Records	k1gInSc1
Web	web	k1gInSc4
</s>
<s>
www.static-x.com	www.static-x.com	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nick	Nick	k6eAd1
Oshiro	Oshiro	k1gNnSc1
(	(	kIx(
<g/>
*	*	kIx~
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1978	#num#	k4
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bubeník	bubeník	k1gMnSc1
kapely	kapela	k1gFnSc2
Static-X	Static-X	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
původem	původ	k1gInSc7
z	z	k7c2
Jižní	jižní	k2eAgFnSc2d1
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1978	#num#	k4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
jednu	jeden	k4xCgFnSc4
sestru	sestra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
hudbě	hudba	k1gFnSc3
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
k	k	k7c3
bubnování	bubnování	k1gNnSc3
<g/>
,	,	kIx,
ho	on	k3xPp3gInSc4
přivedl	přivést	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
mu	on	k3xPp3gMnSc3
také	také	k6eAd1
udělal	udělat	k5eAaPmAgMnS
jeho	jeho	k3xOp3gInPc4
první	první	k4xOgInPc4
bubny	buben	k1gInPc4
ze	z	k7c2
starého	starý	k2eAgNnSc2d1
nádobí	nádobí	k1gNnSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
asi	asi	k9
v	v	k7c6
devíti	devět	k4xCc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rockovou	rockový	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
stát	stát	k5eAaImF,k5eAaPmF
v	v	k7c6
10	#num#	k4
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
na	na	k7c6
MTV	MTV	kA
viděl	vidět	k5eAaImAgMnS
videoklip	videoklip	k1gInSc4
kapely	kapela	k1gFnSc2
Whitesnake	Whitesnak	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
12	#num#	k4
let	léto	k1gNnPc2
žil	žít	k5eAaImAgMnS
v	v	k7c6
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgMnS
zde	zde	k6eAd1
zaměstnán	zaměstnat	k5eAaPmNgMnS
jako	jako	k8xS,k8xC
elektrikář	elektrikář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
Před	před	k7c7
Static-X	Static-X	k1gFnPc7
působil	působit	k5eAaImAgMnS
v	v	k7c6
jihoafrické	jihoafrický	k2eAgFnSc3d1
kapele	kapela	k1gFnSc3
Seether	Seethra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
Static-X	Static-X	k1gFnSc3
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgMnS
během	během	k7c2
turné	turné	k1gNnSc2
k	k	k7c3
albu	album	k1gNnSc3
Shadow	Shadow	k1gFnSc2
Zone	Zon	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
si	se	k3xPyFc3
ho	on	k3xPp3gNnSc4
vybrala	vybrat	k5eAaPmAgFnS
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
29	#num#	k4
bubeníků	bubeník	k1gMnPc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
třikrát	třikrát	k6eAd1
přijel	přijet	k5eAaPmAgMnS
na	na	k7c4
konkurz	konkurz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podílel	podílet	k5eAaImAgMnS
se	se	k3xPyFc4
teprve	teprve	k6eAd1
na	na	k7c6
nahrávce	nahrávka	k1gFnSc6
Start	start	k1gInSc1
a	a	k8xC
War	War	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wayne	Wayn	k1gInSc5
Static	statice	k1gFnPc2
o	o	k7c6
něm	on	k3xPp3gNnSc6
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
ďábelský	ďábelský	k2eAgMnSc1d1
bubeník	bubeník	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
bubny	buben	k1gInPc1
byly	být	k5eAaImAgInP
na	na	k7c4
albu	alba	k1gFnSc4
Start	start	k1gInSc1
a	a	k8xC
War	War	k1gFnPc1
ponechány	ponechán	k2eAgFnPc1d1
v	v	k7c6
původní	původní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
,	,	kIx,
bez	bez	k7c2
úprav	úprava	k1gFnPc2
počítačem	počítač	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
syrovějšímu	syrový	k2eAgInSc3d2
zvuku	zvuk	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
bubny	buben	k1gInPc4
značky	značka	k1gFnSc2
Tama	Tama	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
turné	turné	k1gNnSc4
k	k	k7c3
desce	deska	k1gFnSc3
Cannibal	Cannibal	k1gInSc4
si	se	k3xPyFc3
zlomil	zlomit	k5eAaPmAgMnS
ruku	ruka	k1gFnSc4
a	a	k8xC
na	na	k7c4
několik	několik	k4yIc4
týdnů	týden	k1gInPc2
ho	on	k3xPp3gMnSc4
museli	muset	k5eAaImAgMnP
vystřídat	vystřídat	k5eAaPmF
jiní	jiný	k2eAgMnPc1d1
bubeníci	bubeník	k1gMnPc1
–	–	k?
Will	Willum	k1gNnPc2
Hunt	hunt	k1gInSc1
a	a	k8xC
Beaven	Beaven	k2eAgInSc1d1
Davis	Davis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
,	,	kIx,
těsně	těsně	k6eAd1
před	před	k7c7
vydáním	vydání	k1gNnSc7
Cult	Culta	k1gFnPc2
Of	Of	k1gFnSc7
Static	statice	k1gFnPc2
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nezúčastní	zúčastnit	k5eNaPmIp3nS
následující	následující	k2eAgInSc1d1
tour	tour	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Static-X	Static-X	k1gMnSc5
Wayne	Wayn	k1gMnSc5
Static	statice	k1gFnPc2
•	•	k?
Tony	Tony	k1gMnSc1
Campos	Campos	k1gMnSc1
•	•	k?
Kóiči	Kóič	k1gFnSc3
Fukuda	Fukuda	k1gMnSc1
•	•	k?
Ken	Ken	k1gMnSc1
Jay	Jay	k1gMnSc1
•	•	k?
Tripp	Tripp	k1gMnSc1
Eisen	Eisen	k2eAgMnSc1d1
•	•	k?
Nick	Nick	k1gMnSc1
Oshiro	Oshiro	k1gNnSc4
Studiová	studiový	k2eAgFnSc1d1
alba	alba	k1gFnSc1
</s>
<s>
Wisconsin	Wisconsin	k2eAgMnSc1d1
Death	Death	k1gMnSc1
Trip	Trip	k1gMnSc1
•	•	k?
Machine	Machin	k1gInSc5
•	•	k?
Shadow	Shadow	k1gMnSc2
Zone	Zon	k1gMnSc2
•	•	k?
Start	start	k1gInSc1
a	a	k8xC
War	War	k1gFnSc1
•	•	k?
Cannibal	Cannibal	k1gInSc1
•	•	k?
Cult	Cult	k1gInSc1
of	of	k?
Static	statice	k1gFnPc2
•	•	k?
Project	Project	k1gInSc1
Regeneration	Regeneration	k1gInSc1
EP	EP	kA
</s>
<s>
The	The	k?
Death	Death	k1gMnSc1
Trip	Trip	k1gMnSc1
Continues	Continues	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
