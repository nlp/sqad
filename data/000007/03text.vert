<s>
Polemic	Polemice	k1gFnPc2	Polemice
je	být	k5eAaImIp3nS	být
osmičlenná	osmičlenný	k2eAgFnSc1d1	osmičlenná
slovenská	slovenský	k2eAgFnSc1d1	slovenská
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
přední	přední	k2eAgFnSc2d1	přední
slovenské	slovenský	k2eAgFnSc2d1	slovenská
skupiny	skupina	k1gFnSc2	skupina
hrající	hrající	k2eAgInPc1d1	hrající
styly	styl	k1gInPc1	styl
ska	ska	k?	ska
a	a	k8xC	a
reggae	regga	k1gFnSc2	regga
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
oblíbení	oblíbení	k1gNnPc1	oblíbení
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
dvacetiletého	dvacetiletý	k2eAgNnSc2d1	dvacetileté
působení	působení	k1gNnSc2	působení
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
i	i	k8xC	i
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Předskakovali	předskakovat	k5eAaImAgMnP	předskakovat
kapelám	kapela	k1gFnPc3	kapela
SKA-P	SKA-P	k1gMnPc2	SKA-P
<g/>
,	,	kIx,	,
The	The	k1gMnPc2	The
Wailers	Wailersa	k1gFnPc2	Wailersa
<g/>
,	,	kIx,	,
UB	UB	kA	UB
<g/>
40	[number]	k4	40
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Selecter	Selecter	k1gMnSc1	Selecter
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Skatalites	Skatalites	k1gMnSc1	Skatalites
<g/>
,	,	kIx,	,
Gentleman	gentleman	k1gMnSc1	gentleman
<g/>
,	,	kIx,	,
Levellers	Levellers	k1gInSc1	Levellers
<g/>
,	,	kIx,	,
Persiana	Persiana	k1gFnSc1	Persiana
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Real	Real	k1gInSc1	Real
McKenzies	McKenzies	k1gInSc1	McKenzies
<g/>
,	,	kIx,	,
Yellow	Yellow	k1gMnSc1	Yellow
Umbrella	Umbrella	k1gMnSc1	Umbrella
nebo	nebo	k8xC	nebo
The	The	k1gMnSc1	The
Locos	Locos	k1gMnSc1	Locos
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Do	do	k7c2	do
SKA	SKA	kA	SKA
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
YAH	YAH	kA	YAH
MAN	mana	k1gFnPc2	mana
<g/>
!	!	kIx.	!
</s>
<s>
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
Gangster-SKA	Gangster-SKA	k1gFnSc1	Gangster-SKA
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Nelám	Nela	k1gFnPc3	Nela
si	se	k3xPyFc3	se
s	s	k7c7	s
tým	tým	k1gInSc1	tým
hlavu	hlava	k1gFnSc4	hlava
<g/>
!	!	kIx.	!
</s>
<s>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Nenudin	Nenudina	k1gFnPc2	Nenudina
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Horúce	Horúce	k1gFnPc1	Horúce
časy	čas	k1gInPc1	čas
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Hey	Hey	k1gFnSc2	Hey
<g/>
!	!	kIx.	!
</s>
<s>
Ba-Ba-Re-Bop	Ba-Ba-Re-Bop	k1gInSc1	Ba-Ba-Re-Bop
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
-	-	kIx~	-
zvukový	zvukový	k2eAgInSc1d1	zvukový
záznam	záznam	k1gInSc1	záznam
vystoupení	vystoupení	k1gNnSc2	vystoupení
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Hodokvas	hodokvasit	k5eAaImRp2nS	hodokvasit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
DVD	DVD	kA	DVD
Live	Live	k1gFnSc1	Live
-	-	kIx~	-
videozáznam	videozáznam	k1gInSc1	videozáznam
vystoupení	vystoupení	k1gNnSc2	vystoupení
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Hodokvas	hodokvasit	k5eAaImRp2nS	hodokvasit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgInPc4	všechen
doposud	doposud	k6eAd1	doposud
natočené	natočený	k2eAgInPc4d1	natočený
klipy	klip	k1gInPc4	klip
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Best	Best	k1gInSc1	Best
of	of	k?	of
1988-2008	[number]	k4	1988-2008
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
Vrana	Vran	k1gInSc2	Vran
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Mesto	Mesto	k1gNnSc4	Mesto
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Slnko	Slnko	k1gNnSc1	Slnko
v	v	k7c6	v
sieti	sieť	k1gFnSc6	sieť
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
skupiny	skupina	k1gFnSc2	skupina
MySpace	MySpace	k1gFnSc1	MySpace
profil	profil	k1gInSc1	profil
</s>
