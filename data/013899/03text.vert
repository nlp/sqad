<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
</s>
<s>
MS	MS	kA
juniorů	junior	k1gMnPc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1
ročník	ročník	k1gInSc1
<g/>
:	:	kIx,
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
2018	#num#	k4
Sport	sport	k1gInSc1
</s>
<s>
sportovní	sportovní	k2eAgNnSc1d1
lezení	lezení	k1gNnSc1
Založeno	založen	k2eAgNnSc1d1
</s>
<s>
1992	#num#	k4
Vlastník	vlastník	k1gMnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
sportovního	sportovní	k2eAgNnSc2d1
lezení	lezení	k1gNnSc2
(	(	kIx(
<g/>
IFSC	IFSC	kA
<g/>
)	)	kIx)
První	první	k4xOgInSc1
ročník	ročník	k1gInSc1
</s>
<s>
MSJ	MSJ	kA
1992	#num#	k4
Poslední	poslední	k2eAgInPc1d1
vítěz	vítěz	k1gMnSc1
</s>
<s>
Jošijuki	Jošijuki	k1gNnSc1
Ogata	Ogat	k1gInSc2
Claire	Clair	k1gInSc5
Buhrfeind	Buhrfeinda	k1gFnPc2
Nejvíce	hodně	k6eAd3,k6eAd1
titulů	titul	k1gInPc2
</s>
<s>
Sean	Sean	k1gMnSc1
McColl	McColl	k1gInSc4
Ašima	Ašimum	k1gNnSc2
Širaiši	Širaiš	k1gMnPc1
TV	TV	kA
partneři	partner	k1gMnPc1
</s>
<s>
Ifscchannel	Ifscchannel	k1gInSc1
youtube	youtubat	k5eAaPmIp3nS
Příbuzné	příbuzný	k2eAgFnPc4d1
soutěže	soutěž	k1gFnPc4
</s>
<s>
MSJ	MSJ	kA
v	v	k7c6
ledolezení	ledolezení	k1gNnSc6
<g/>
;	;	kIx,
Akademické	akademický	k2eAgFnSc2d1
MS	MS	kA
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
horolezecká	horolezecký	k2eAgFnSc1d1
federace	federace	k1gFnSc1
(	(	kIx(
<g/>
UIAA	UIAA	kA
<g/>
)	)	kIx)
Vyšší	vysoký	k2eAgFnSc1d2
soutěž	soutěž	k1gFnSc1
</s>
<s>
MS	MS	kA
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
Nižší	nízký	k2eAgFnSc1d2
soutěž	soutěž	k1gFnSc1
</s>
<s>
MEJ	MEJ	kA
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
Domácí	domácí	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
MČR	MČR	kA
mládeže	mládež	k1gFnSc2
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
Web	web	k1gInSc1
</s>
<s>
www.ifsc-climbing.org	www.ifsc-climbing.org	k1gMnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
IFSC	IFSC	kA
World	Worlda	k1gFnPc2
Youth	Youtha	k1gFnPc2
Championships	Championships	k1gInSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
UIAA	UIAA	kA
World	Worlda	k1gFnPc2
Youth	Youtha	k1gFnPc2
Championships	Championships	k1gInSc1
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
juniorská	juniorský	k2eAgNnPc1d1
mistrovství	mistrovství	k1gNnPc1
světa	svět	k1gInSc2
(	(	kIx(
<g/>
MSJ	MSJ	kA
<g/>
)	)	kIx)
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
juniorů	junior	k1gMnPc2
a	a	k8xC
mládeže	mládež	k1gFnSc2
od	od	k7c2
14	#num#	k4
do	do	k7c2
19	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
pořádané	pořádaný	k2eAgFnPc1d1
zpravidla	zpravidla	k6eAd1
každoročně	každoročně	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
Mezinárodní	mezinárodní	k2eAgFnSc7d1
federací	federace	k1gFnSc7
sportovního	sportovní	k2eAgNnSc2d1
lezení	lezení	k1gNnSc2
(	(	kIx(
<g/>
IFSC	IFSC	kA
<g/>
)	)	kIx)
ve	v	k7c6
třech	tři	k4xCgFnPc6
kategoriích	kategorie	k1gFnPc6
<g/>
,	,	kIx,
nekonala	konat	k5eNaImAgFnS
se	se	k3xPyFc4
pouze	pouze	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1993	#num#	k4
a	a	k8xC
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
určuje	určovat	k5eAaImIp3nS
v	v	k7c6
kategoriích	kategorie	k1gFnPc6
juniorů	junior	k1gMnPc2
a	a	k8xC
mládeže	mládež	k1gFnSc2
mistry	mistr	k1gMnPc7
světa	svět	k1gInSc2
ve	v	k7c6
třech	tři	k4xCgFnPc6
disciplínách	disciplína	k1gFnPc6
sportovního	sportovní	k2eAgInSc2d1
lezení	lezení	k1gNnSc3
<g/>
:	:	kIx,
lezení	lezení	k1gNnSc3
na	na	k7c4
obtížnost	obtížnost	k1gFnSc4
<g/>
,	,	kIx,
lezení	lezení	k1gNnSc4
na	na	k7c4
rychlost	rychlost	k1gFnSc4
a	a	k8xC
bouldering	bouldering	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dalšími	další	k2eAgInPc7d1
juniorskými	juniorský	k2eAgInPc7d1
světovými	světový	k2eAgInPc7d1
závody	závod	k1gInPc7
jsou	být	k5eAaImIp3nP
Mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
a	a	k8xC
Evropský	evropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
juniorů	junior	k1gMnPc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
mohou	moct	k5eAaImIp3nP
v	v	k7c6
každé	každý	k3xTgFnSc6
kategorii	kategorie	k1gFnSc6
závodit	závodit	k5eAaImF
maximálně	maximálně	k6eAd1
čtyři	čtyři	k4xCgMnPc1
závodníci	závodník	k1gMnPc1
z	z	k7c2
každé	každý	k3xTgFnSc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
českou	český	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
určuje	určovat	k5eAaImIp3nS
Český	český	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
ČHS	ČHS	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
také	také	k9
pořádá	pořádat	k5eAaImIp3nS
závody	závod	k1gInPc4
v	v	k7c4
lezení	lezení	k1gNnSc4
jako	jako	k8xS,k8xC
Mistrovství	mistrovství	k1gNnSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
a	a	k8xC
Mistrovství	mistrovství	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
mládeže	mládež	k1gFnSc2
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závodníci	Závodník	k1gMnPc1
od	od	k7c2
šestnácti	šestnáct	k4xCc2
let	léto	k1gNnPc2
mají	mít	k5eAaImIp3nP
také	také	k9
současně	současně	k6eAd1
možnost	možnost	k1gFnSc4
účastnit	účastnit	k5eAaImF
se	se	k3xPyFc4
Mistrovství	mistrovství	k1gNnSc3
světa	svět	k1gInSc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
pro	pro	k7c4
dospělé	dospělí	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
vysokými	vysoký	k2eAgFnPc7d1
školami	škola	k1gFnPc7
probíhá	probíhat	k5eAaImIp3nS
Akademické	akademický	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
včetně	včetně	k7c2
dalších	další	k2eAgInPc2d1
závodů	závod	k1gInPc2
pořádá	pořádat	k5eAaImIp3nS
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
univerzitního	univerzitní	k2eAgInSc2d1
sportu	sport	k1gInSc2
(	(	kIx(
<g/>
FISU	FISU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
horolezecká	horolezecký	k2eAgFnSc1d1
federace	federace	k1gFnSc1
(	(	kIx(
<g/>
UIAA	UIAA	kA
<g/>
)	)	kIx)
pořádá	pořádat	k5eAaImIp3nS
také	také	k9
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
ledolezení	ledolezení	k1gNnSc6
na	na	k7c4
obtížnost	obtížnost	k1gFnSc4
a	a	k8xC
rychlost	rychlost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Juniorské	juniorský	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
disciplínách	disciplína	k1gFnPc6
od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
obtížnost	obtížnost	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
rychlost	rychlost	k1gFnSc4
a	a	k8xC
2015	#num#	k4
bouldering	bouldering	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězové	vítěz	k1gMnPc1
kategorií	kategorie	k1gFnPc2
A	A	kA
a	a	k8xC
juniorů	junior	k1gMnPc2
v	v	k7c6
disciplínách	disciplína	k1gFnPc6
lezení	lezení	k1gNnPc2
na	na	k7c4
obtížnost	obtížnost	k1gFnSc4
a	a	k8xC
rychlost	rychlost	k1gFnSc4
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
nominovali	nominovat	k5eAaBmAgMnP
na	na	k7c4
účast	účast	k1gFnSc4
ve	v	k7c6
Světových	světový	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2013	#num#	k4
v	v	k7c6
Cali	Cal	k1gFnSc6
v	v	k7c6
Kolumbii	Kolumbie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobně	obdobně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
se	se	k3xPyFc4
juniorští	juniorský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
ve	v	k7c6
všech	všecek	k3xTgFnPc6
třech	tři	k4xCgFnPc6
disciplínách	disciplína	k1gFnPc6
nominovali	nominovat	k5eAaBmAgMnP
na	na	k7c4
Světové	světový	k2eAgFnPc4d1
hry	hra	k1gFnPc4
2017	#num#	k4
v	v	k7c6
polské	polský	k2eAgFnSc6d1
Vratislavi	Vratislav	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Junioři	junior	k1gMnPc1
a	a	k8xC
Juniorky	juniorka	k1gFnPc1
(	(	kIx(
<g/>
18	#num#	k4
-	-	kIx~
19	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
chlapci	chlapec	k1gMnPc1
a	a	k8xC
dívky	dívka	k1gFnPc1
kategorie	kategorie	k1gFnSc2
A	a	k9
(	(	kIx(
<g/>
16	#num#	k4
-	-	kIx~
17	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
chlapci	chlapec	k1gMnPc1
a	a	k8xC
dívky	dívka	k1gFnPc1
kategorie	kategorie	k1gFnSc2
B	B	kA
(	(	kIx(
<g/>
14	#num#	k4
-	-	kIx~
15	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
závodů	závod	k1gInPc2
</s>
<s>
RokMístoDatumWebDisciplínyZávodníciZemě	RokMístoDatumWebDisciplínyZávodníciZemě	k6eAd1
</s>
<s>
č.	č.	k?
<g/>
obtížnostrychlostboulderingKombinacejun	obtížnostrychlostboulderingKombinacejun	k1gMnSc1
<g/>
.	.	kIx.
<g/>
kat	kat	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akat	Akata	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bjun	Bjun	k1gMnSc1
<g/>
.	.	kIx.
<g/>
kat	kat	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akat	Akata	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
I	i	k9
<g/>
.1992	.1992	k4
</s>
<s>
Basilej	Basilej	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
</s>
<s>
1	#num#	k4
<g/>
•	•	k?
</s>
<s>
II	II	kA
<g/>
.1994	.1994	k4
</s>
<s>
Lipsko	Lipsko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
</s>
<s>
1	#num#	k4
<g/>
•	•	k?
</s>
<s>
III	III	kA
<g/>
.1995	.1995	k4
</s>
<s>
Laval	Laval	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
IV	IV	kA
<g/>
.1996	.1996	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
<g/>
21	#num#	k4
</s>
<s>
V	v	k7c6
<g/>
.1997	.1997	k4
</s>
<s>
Imst	Imst	k1gInSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
</s>
<s>
1	#num#	k4
<g/>
•	•	k?
</s>
<s>
VI	VI	kA
<g/>
.1998	.1998	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
</s>
<s>
1	#num#	k4
<g/>
•	•	k?
</s>
<s>
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
</s>
<s>
Courmayeur	Courmayeur	k1gMnSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
•	•	k?
</s>
<s>
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
</s>
<s>
Amsterdam	Amsterdam	k1gInSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
•	•	k?
</s>
<s>
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
</s>
<s>
Imst	Imst	k1gInSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
X.	X.	kA
</s>
<s>
2002	#num#	k4
</s>
<s>
Canteleu	Canteleu	k6eAd1
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
XI	XI	kA
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
</s>
<s>
Veliko	Velika	k1gFnSc5
Tarnovo	Tarnův	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
•	•	k?
</s>
<s>
XII	XII	kA
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
</s>
<s>
Edinburgh	Edinburgh	k1gInSc1
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
•	•	k?
</s>
<s>
XIII	XIII	kA
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
</s>
<s>
Peking	Peking	k1gInSc1
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
</s>
<s>
Imst	Imst	k1gInSc1
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
</s>
<s>
Ibarra	Ibarra	k1gFnSc1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
XVI	XVI	kA
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
</s>
<s>
Sydney	Sydney	k1gNnSc1
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
XVII	XVII	kA
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
</s>
<s>
Valence	valence	k1gFnSc1
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
XVIII	XVIII	kA
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
</s>
<s>
Edinburgh	Edinburgh	k1gInSc1
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
XIX	XIX	kA
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
</s>
<s>
Imst	Imst	k1gInSc1
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
XX	XX	kA
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
</s>
<s>
Singapur	Singapur	k1gInSc1
Singapur	Singapur	k1gInSc1
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
XXI	XXI	kA
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
</s>
<s>
Saanich	Saanich	k1gInSc1
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
XXII	XXII	kA
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
</s>
<s>
Nouméa	Nouméa	k1gFnSc1
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
2	#num#	k4
<g/>
•	•	k?
</s>
<s>
XXIII	XXIII	kA
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
</s>
<s>
Arco	Arco	k1gNnSc1
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
-	-	kIx~
6	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
•	•	k?
</s>
<s>
XXIV	XXIV	kA
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
</s>
<s>
Kanton	Kanton	k1gInSc1
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
listopaduwych	listopaduwych	k1gInSc1
<g/>
2016	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
•	•	k?
</s>
<s>
XXV	XXV	kA
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
</s>
<s>
Innsbruck	Innsbruck	k1gInSc1
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
-	-	kIx~
10	#num#	k4
<g/>
.	.	kIx.
záříywch	záříywch	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
<g/>
•	•	k?
</s>
<s>
XXVI	XXVI	kA
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
</s>
<s>
3	#num#	k4
<g/>
•	•	k?
</s>
<s>
Medailisté	medailista	k1gMnPc1
</s>
<s>
Seznamy	seznam	k1gInPc1
medailistů	medailista	k1gMnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
medailistů	medailista	k1gMnPc2
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Česko	Česko	k1gNnSc4
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Medaile	medaile	k1gFnSc1
podle	podle	k7c2
zemí	zem	k1gFnPc2
</s>
<s>
Země	země	k1gFnSc1
<g/>
19929495969798992000010203040506070809201011121314151617	#num#	k4
<g/>
celkem	celkem	k6eAd1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
Rusko	Rusko	k1gNnSc4
Rusko	Rusko	k1gNnSc1
<g/>
44318	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
421199121361099119141112111013196	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
<g/>
864	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
544327674332625341642101	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Rakousko	Rakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
22225211296544674632176	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
USA	USA	kA
USA14	USA14	k1gFnSc2
?	?	kIx.
</s>
<s desamb="1">
<g/>
31221125461143691066	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Japonsko	Japonsko	k1gNnSc1
Japonsko	Japonsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
1122323241691753	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
Ukrajina	Ukrajina	k1gFnSc1
Ukrajina	Ukrajina	k1gFnSc1
<g/>
1	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
2222855411233010244	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
Slovinsko	Slovinsko	k1gNnSc4
Slovinsko	Slovinsko	k1gNnSc1
<g/>
12	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
223311222124125541	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
Itálie	Itálie	k1gFnSc2
Itálie	Itálie	k1gFnSc2
<g/>
1	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
11111113322377237	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
Německo	Německo	k1gNnSc4
Německo	Německo	k1gNnSc1
<g/>
43	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
21121222422129	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
Švýcarsko	Švýcarsko	k1gNnSc4
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
12	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
3211111111111221	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc2
<g/>
31	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
112111131117	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
1113132111116	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
<g/>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
32111221114	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
Španělsko	Španělsko	k1gNnSc4
Španělsko	Španělsko	k1gNnSc1
<g/>
1	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
1111211211	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
Ekvádor	Ekvádor	k1gInSc1
Ekvádor	Ekvádor	k1gInSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
3111219	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
<g/>
Kanada	Kanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
111137	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
<g/>
Venezuela	Venezuela	k1gFnSc1
Venezuela	Venezuela	k1gFnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
1121117	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
1111116	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
111115	#num#	k4
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
1	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
124	#num#	k4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
<g/>
Čína	Čína	k1gFnSc1
Čína	Čína	k1gFnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
123	#num#	k4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
<g/>
Švédsko	Švédsko	k1gNnSc1
Švédsko	Švédsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
123	#num#	k4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
Bulharsko	Bulharsko	k1gNnSc4
Bulharsko	Bulharsko	k1gNnSc1
<g/>
1	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
12	#num#	k4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
112	#num#	k4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
Kazachstán	Kazachstán	k1gInSc1
Kazachstán	Kazachstán	k1gInSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
112	#num#	k4
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
Austrálie	Austrálie	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
Maďarsko	Maďarsko	k1gNnSc1
Maďarsko	Maďarsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
Srbsko	Srbsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
Írán	Írán	k1gInSc1
Írán	Írán	k1gInSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
Chorvatsko	Chorvatsko	k1gNnSc1
Chorvatsko	Chorvatsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
30	#num#	k4
<g/>
celkem	celkem	k6eAd1
<g/>
19191836	#num#	k4
<g/>
*	*	kIx~
<g/>
181818183636181836363636363636363636545454794	#num#	k4
</s>
<s>
medaile	medaile	k1gFnPc1
i	i	k8xC
vítězové	vítěz	k1gMnPc1
1992-2017	1992-2017	k4
(	(	kIx(
<g/>
1996	#num#	k4
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
obtížnost	obtížnost	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vítězové	vítěz	k1gMnPc1
podle	podle	k7c2
zemí	zem	k1gFnPc2
</s>
<s>
Země	země	k1gFnSc1
<g/>
19929495969798992000010203040506070809201011121314151617	#num#	k4
<g/>
celkem	celkem	k6eAd1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
Rusko	Rusko	k1gNnSc4
Rusko	Rusko	k1gNnSc1
<g/>
2	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
71124324443455424364	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
<g/>
152	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
11111113321111112233	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Rakousko	Rakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
12111123323222127	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
USA	USA	kA
USA	USA	kA
<g/>
2	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
111124517	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Slovinsko	Slovinsko	k1gNnSc1
Slovinsko	Slovinsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11211112112216	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Japonsko	Japonsko	k1gNnSc1
Japonsko	Japonsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11212111616	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
Ukrajina	Ukrajina	k1gFnSc1
Ukrajina	Ukrajina	k1gFnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
1211312111115	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
Itálie	Itálie	k1gFnSc2
Itálie	Itálie	k1gFnSc2
?	?	kIx.
</s>
<s desamb="1">
<g/>
11111223214	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
Německo	Německo	k1gNnSc4
Německo	Německo	k1gNnSc1
<g/>
1	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
111219	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
Švýcarsko	Švýcarsko	k1gNnSc4
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
1	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
111111119	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
121111119	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
1111116	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
Belgie	Belgie	k1gFnSc1
Belgie	Belgie	k1gFnSc2
<g/>
31	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
1116	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
Španělsko	Španělsko	k1gNnSc4
Španělsko	Španělsko	k1gNnSc1
<g/>
1	#num#	k4
?	?	kIx.
</s>
<s desamb="1">
<g/>
1125	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
Kanada	Kanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11125	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
<g/>
Venezuela	Venezuela	k1gFnSc1
Venezuela	Venezuela	k1gFnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11114	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
Nizozemsko	Nizozemsko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
112	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
Ekvádor	Ekvádor	k1gInSc1
Ekvádor	Ekvádor	k1gInSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
112	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Kazachstán	Kazachstán	k1gInSc1
Kazachstán	Kazachstán	k1gInSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Srbsko	Srbsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Chorvatsko	Chorvatsko	k1gNnSc1
Chorvatsko	Chorvatsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Švédsko	Švédsko	k1gNnSc1
Švédsko	Švédsko	k1gNnSc1
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Austrálie	Austrálie	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
?	?	kIx.
</s>
<s desamb="1">
<g/>
11	#num#	k4
</s>
<s>
25	#num#	k4
<g/>
celkem	celkem	k6eAd1
<g/>
6712	#num#	k4
<g/>
*	*	kIx~
<g/>
12666612136611	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
121212121212121212181818	#num#	k4
<g/>
XXX	XXX	kA
</s>
<s>
medaile	medaile	k1gFnPc1
i	i	k8xC
vítězové	vítěz	k1gMnPc1
1992-2016	1992-2016	k4
(	(	kIx(
<g/>
1995	#num#	k4
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
rychlost	rychlost	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
1992	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
1994	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
1995	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
Krch	Krch	k1gMnSc1
<g/>
:	:	kIx,
Kak	Kak	k1gMnSc1
těbja	těbja	k1gMnSc1
zavut	zavut	k1gMnSc1
<g/>
;	;	kIx,
Montana	Montana	k1gFnSc1
6	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
<g/>
,	,	kIx,
VI	VI	kA
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
52	#num#	k4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
-	-	kIx~
MSJ	MSJ	kA
1996	#num#	k4
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
1997	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
1998	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
výsledky	výsledek	k1gInPc4
MSJ	MSJ	kA
1999	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2000	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2001	#num#	k4
obtížnost	obtížnost	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2001	#num#	k4
rychlost	rychlost	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2002	#num#	k4
obtížnost	obtížnost	k1gFnSc1
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2002	#num#	k4
rychlost	rychlost	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2003	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2004	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2005	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2006	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2007	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2008	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2009	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2010	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2011	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2012	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2013	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2014	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2015	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2016	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2017	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
IFSC	IFSC	kA
<g/>
:	:	kIx,
výsledky	výsledek	k1gInPc1
MSJ	MSJ	kA
2018	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
sportovního	sportovní	k2eAgNnSc2d1
lezení	lezení	k1gNnSc2
(	(	kIx(
<g/>
IFSC	IFSC	kA
<g/>
)	)	kIx)
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
ledolezení	ledolezení	k1gNnSc6
(	(	kIx(
<g/>
MSJ	MSJ	kA
pro	pro	k7c4
zimní	zimní	k2eAgInPc4d1
discilpíny	discilpín	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Akademické	akademický	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
(	(	kIx(
<g/>
AMS	AMS	kA
<g/>
)	)	kIx)
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
mládeže	mládež	k1gFnSc2
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
(	(	kIx(
<g/>
MČRM	MČRM	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Český	český	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
-	-	kIx~
Soutěžní	soutěžní	k2eAgNnSc1d1
lezení	lezení	k1gNnSc1
</s>
<s>
Kalendář	kalendář	k1gInSc1
mezinárodních	mezinárodní	k2eAgInPc2d1
závodů	závod	k1gInPc2
IFSC	IFSC	kA
</s>
<s>
Závodní	závodní	k1gMnSc1
pravidla	pravidlo	k1gNnSc2
IFSC	IFSC	kA
2013	#num#	k4
</s>
<s>
Závodní	závodní	k1gMnSc1
pravidla	pravidlo	k1gNnSc2
IFSC	IFSC	kA
2015	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
</s>
<s>
Basilej	Basilej	k1gFnSc1
1992	#num#	k4
•	•	k?
Lipsko	Lipsko	k1gNnSc1
1994	#num#	k4
•	•	k?
Laval	Laval	k1gInSc1
1995	#num#	k4
•	•	k?
Moskva	Moskva	k1gFnSc1
1996	#num#	k4
•	•	k?
Imst	Imst	k1gInSc1
1997	#num#	k4
•	•	k?
Moskva	Moskva	k1gFnSc1
1998	#num#	k4
•	•	k?
Courmayeur	Courmayeura	k1gFnPc2
1999	#num#	k4
•	•	k?
Amsterdam	Amsterdam	k1gInSc1
2000	#num#	k4
•	•	k?
Imst	Imst	k1gInSc1
2001	#num#	k4
•	•	k?
Canteleu	Canteleus	k1gInSc2
2002	#num#	k4
•	•	k?
Veliko	Velika	k1gFnSc5
Tarnovo	Tarnův	k2eAgNnSc4d1
2003	#num#	k4
•	•	k?
Edinburgh	Edinburgh	k1gInSc1
2004	#num#	k4
•	•	k?
Peking	Peking	k1gInSc1
2005	#num#	k4
•	•	k?
Imst	Imst	k1gInSc1
2006	#num#	k4
•	•	k?
Ibarra	Ibarra	k1gMnSc1
2007	#num#	k4
•	•	k?
Sydney	Sydney	k1gNnSc1
2008	#num#	k4
•	•	k?
Valence	valence	k1gFnSc2
2009	#num#	k4
•	•	k?
Edinburgh	Edinburgh	k1gInSc1
2010	#num#	k4
•	•	k?
Imst	Imst	k1gInSc1
2011	#num#	k4
•	•	k?
Singapur	Singapur	k1gInSc1
2012	#num#	k4
•	•	k?
Saanich	Saanich	k1gInSc1
2013	#num#	k4
•	•	k?
Noumea	Noumea	k1gMnSc1
2014	#num#	k4
•	•	k?
Arco	Arco	k1gNnSc1
2015	#num#	k4
•	•	k?
Kanton	Kanton	k1gInSc1
2016	#num#	k4
•	•	k?
Innsbruck	Innsbruck	k1gInSc1
2017	#num#	k4
•	•	k?
Moskva	Moskva	k1gFnSc1
2018	#num#	k4
</s>
<s>
seznam	seznam	k1gInSc1
medailistů	medailista	k1gMnPc2
•	•	k?
čeští	český	k2eAgMnPc1d1
medailisté	medailista	k1gMnPc1
</s>
<s>
Závody	závod	k1gInPc1
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
</s>
<s>
Letní	letní	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
•	•	k?
Letní	letní	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
mládeže	mládež	k1gFnSc2
•	•	k?
Světové	světový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
•	•	k?
Asijské	asijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
•	•	k?
Zimní	zimní	k2eAgFnSc2d1
armádní	armádní	k2eAgFnSc2d1
světové	světový	k2eAgFnSc2d1
hry	hra	k1gFnSc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
•	•	k?
Světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Asie	Asie	k1gFnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
<g/>
;	;	kIx,
Amerika	Amerika	k1gFnSc1
<g/>
;	;	kIx,
Oceánie	Oceánie	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc4
Asie	Asie	k1gFnSc2
juniorů	junior	k1gMnPc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
•	•	k?
Evropský	evropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
juniorů	junior	k1gMnPc2
•	•	k?
Rheintal	Rheintal	k1gMnSc1
Cup	cup	k0
</s>
<s>
Akademické	akademický	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
•	•	k?
Akademické	akademický	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
•	•	k?
Akademické	akademický	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
Asie	Asie	k1gFnSc2
•	•	k?
Akademické	akademický	k2eAgFnSc2d1
MČR	MČR	kA
ve	v	k7c6
sportovním	sportovní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
</s>
<s>
Sportroccia	Sportroccia	k1gFnSc1
•	•	k?
Arco	Arco	k1gNnSc1
Rock	rock	k1gInSc1
Master	master	k1gMnSc1
•	•	k?
Melloblocco	Melloblocco	k1gMnSc1
•	•	k?
Adidas	Adidas	k1gInSc1
Rockstars	Rockstars	k1gInSc1
•	•	k?
Salewa	Salew	k1gInSc2
rock	rock	k1gInSc1
show	show	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Francie	Francie	k1gFnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Itálie	Itálie	k1gFnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Německa	Německo	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Polska	Polsko	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Rakouska	Rakousko	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Slovenska	Slovensko	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Španělska	Španělsko	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
Švýcarska	Švýcarsko	k1gNnSc2
•	•	k?
Mistrovství	mistrovství	k1gNnSc1
USA	USA	kA
</s>
<s>
Italský	italský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
MČR	MČR	kA
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
•	•	k?
ČP	ČP	kA
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
•	•	k?
Petrohradské	petrohradský	k2eAgNnSc4d1
Padání	padání	k1gNnSc4
•	•	k?
Mejcup	Mejcup	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
boulderingová	boulderingový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
MČR	MČR	kA
mládeže	mládež	k1gFnSc2
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
•	•	k?
ČP	ČP	kA
mládeže	mládež	k1gFnSc2
v	v	k7c6
soutěžním	soutěžní	k2eAgNnSc6d1
lezení	lezení	k1gNnSc6
•	•	k?
U14	U14	k1gFnSc4
•	•	k?
Překližka	překližka	k1gFnSc1
cup	cup	k1gInSc1
•	•	k?
Moravský	moravský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
mládeže	mládež	k1gFnSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
sportovního	sportovní	k2eAgNnSc2d1
lezení	lezení	k1gNnSc2
(	(	kIx(
<g/>
IFSC	IFSC	kA
<g/>
)	)	kIx)
•	•	k?
Český	český	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
svaz	svaz	k1gInSc1
(	(	kIx(
<g/>
ČHS	ČHS	kA
<g/>
)	)	kIx)
•	•	k?
Slovenský	slovenský	k2eAgInSc1d1
horolezecký	horolezecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
JAMES	JAMES	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
