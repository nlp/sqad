<s>
Alfred	Alfred	k1gMnSc1	Alfred
Bernhard	Bernhard	k1gMnSc1	Bernhard
Nobel	Nobel	k1gMnSc1	Nobel
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc1	výslovnost
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1833	[number]	k4	1833
Stockholm	Stockholm	k1gInSc1	Stockholm
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1896	[number]	k4	1896
Sanremo	Sanrema	k1gFnSc5	Sanrema
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
dynamitu	dynamit	k1gInSc2	dynamit
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	jeho	k3xOp3gFnSc2	jeho
závěti	závěť	k1gFnSc2	závěť
jsou	být	k5eAaImIp3nP	být
každoročně	každoročně	k6eAd1	každoročně
udělovány	udělovat	k5eAaImNgFnP	udělovat
Nobelovy	Nobelův	k2eAgFnPc1d1	Nobelova
ceny	cena	k1gFnPc1	cena
<g/>
.	.	kIx.	.
</s>
