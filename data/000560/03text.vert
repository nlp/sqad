<s>
Duben	duben	k1gInSc1	duben
je	být	k5eAaImIp3nS	být
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
měsíc	měsíc	k1gInSc4	měsíc
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
přechodem	přechod	k1gInSc7	přechod
do	do	k7c2	do
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
kalendáře	kalendář	k1gInSc2	kalendář
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
druhý	druhý	k4xOgInSc1	druhý
měsíc	měsíc	k1gInSc1	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
dub	dub	k1gInSc1	dub
<g/>
.	.	kIx.	.
</s>
<s>
Duben	duben	k1gInSc1	duben
začíná	začínat	k5eAaImIp3nS	začínat
stejným	stejný	k2eAgInSc7d1	stejný
dnem	den	k1gInSc7	den
jako	jako	k8xC	jako
červenec	červenec	k1gInSc4	červenec
a	a	k8xC	a
v	v	k7c6	v
přestupném	přestupný	k2eAgInSc6d1	přestupný
roce	rok	k1gInSc6	rok
jako	jako	k9	jako
leden	leden	k1gInSc4	leden
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
dubnový	dubnový	k2eAgInSc1d1	dubnový
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
apríl	apríl	k1gInSc1	apríl
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
apríl	apríl	k1gInSc1	apríl
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
žertíky	žertík	k1gInPc7	žertík
a	a	k8xC	a
drobnými	drobný	k2eAgFnPc7d1	drobná
zlomyslnostmi	zlomyslnost	k1gFnPc7	zlomyslnost
<g/>
.	.	kIx.	.
</s>
<s>
Aprílové	aprílový	k2eAgNnSc1d1	aprílové
počasí	počasí	k1gNnSc1	počasí
označuje	označovat	k5eAaImIp3nS	označovat
počastí	počastit	k5eAaPmIp3nP	počastit
nestálé	stálý	k2eNgInPc1d1	nestálý
<g/>
,	,	kIx,	,
proměnlivé	proměnlivý	k2eAgInPc1d1	proměnlivý
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
proměnlivém	proměnlivý	k2eAgNnSc6d1	proměnlivé
a	a	k8xC	a
nestálém	stálý	k2eNgNnSc6d1	nestálé
počasí	počasí	k1gNnSc6	počasí
v	v	k7c4	v
jiný	jiný	k2eAgInSc4d1	jiný
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Duben	duben	k1gInSc1	duben
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
tam	tam	k6eAd1	tam
budem	budem	k?	budem
<g/>
.	.	kIx.	.
</s>
<s>
Prší	pršet	k5eAaImIp3nS	pršet
<g/>
-li	i	k?	-li
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
mokrý	mokrý	k2eAgInSc4d1	mokrý
máj	máj	k1gInSc4	máj
<g/>
.	.	kIx.	.
</s>
<s>
Hodně	hodně	k6eAd1	hodně
<g/>
-li	i	k?	-li
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
vítr	vítr	k1gInSc1	vítr
duje	dout	k5eAaImIp3nS	dout
<g/>
,	,	kIx,	,
stodola	stodola	k1gFnSc1	stodola
se	se	k3xPyFc4	se
naplňuje	naplňovat	k5eAaImIp3nS	naplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
mokrý	mokrý	k2eAgInSc4d1	mokrý
duben	duben	k1gInSc4	duben
-	-	kIx~	-
suchý	suchý	k2eAgInSc4d1	suchý
červen	červen	k1gInSc4	červen
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
duben	duben	k1gInSc1	duben
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
duben	duben	k1gInSc1	duben
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
