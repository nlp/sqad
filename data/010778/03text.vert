<p>
<s>
Komárovití	Komárovitý	k2eAgMnPc1d1	Komárovitý
(	(	kIx(	(
<g/>
Culicidae	Culicidae	k1gNnSc7	Culicidae
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čeleď	čeleď	k1gFnSc1	čeleď
dvoukřídlého	dvoukřídlý	k2eAgInSc2d1	dvoukřídlý
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
zástupci	zástupce	k1gMnPc1	zástupce
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
označováni	označován	k2eAgMnPc1d1	označován
jako	jako	k9	jako
komáři	komár	k1gMnPc1	komár
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc1	samička
většiny	většina	k1gFnSc2	většina
z	z	k7c2	z
asi	asi	k9	asi
3000	[number]	k4	3000
druhů	druh	k1gInPc2	druh
sají	sát	k5eAaImIp3nP	sát
krev	krev	k1gFnSc4	krev
teplokrevných	teplokrevný	k2eAgInPc2d1	teplokrevný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
často	často	k6eAd1	často
přenáší	přenášet	k5eAaImIp3nS	přenášet
velmi	velmi	k6eAd1	velmi
nebezpečné	bezpečný	k2eNgFnPc4d1	nebezpečná
nemoci	nemoc	k1gFnPc4	nemoc
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
malárii	malárie	k1gFnSc4	malárie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
každoročně	každoročně	k6eAd1	každoročně
nepřímo	přímo	k6eNd1	přímo
zahubí	zahubit	k5eAaPmIp3nP	zahubit
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
měří	měřit	k5eAaImIp3nS	měřit
přes	přes	k7c4	přes
16	[number]	k4	16
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Komáři	komár	k1gMnPc1	komár
váží	vážit	k5eAaImIp3nP	vážit
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
2	[number]	k4	2
až	až	k9	až
2,5	[number]	k4	2,5
mg	mg	kA	mg
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
noc	noc	k1gFnSc4	noc
dokáží	dokázat	k5eAaPmIp3nP	dokázat
urazit	urazit	k5eAaPmF	urazit
až	až	k9	až
10	[number]	k4	10
km	km	kA	km
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
létat	létat	k5eAaImF	létat
až	až	k9	až
4	[number]	k4	4
hodiny	hodina	k1gFnSc2	hodina
bez	bez	k7c2	bez
přestávky	přestávka	k1gFnSc2	přestávka
rychlostí	rychlost	k1gFnSc7	rychlost
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
a	a	k8xC	a
shání	shánět	k5eAaImIp3nS	shánět
potravu	potrava	k1gFnSc4	potrava
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
večer	večer	k6eAd1	večer
či	či	k8xC	či
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
komárů	komár	k1gMnPc2	komár
schovává	schovávat	k5eAaImIp3nS	schovávat
na	na	k7c6	na
chladných	chladný	k2eAgNnPc6d1	chladné
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Komár	komár	k1gMnSc1	komár
v	v	k7c6	v
letu	let	k1gInSc6	let
vydává	vydávat	k5eAaPmIp3nS	vydávat
hvízdavý	hvízdavý	k2eAgInSc1d1	hvízdavý
tón	tón	k1gInSc1	tón
<g/>
,	,	kIx,	,
způsobený	způsobený	k2eAgInSc1d1	způsobený
chvěním	chvění	k1gNnSc7	chvění
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
také	také	k9	také
hlasivek	hlasivka	k1gFnPc2	hlasivka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
napjaté	napjatý	k2eAgFnPc1d1	napjatá
v	v	k7c6	v
hrudních	hrudní	k2eAgInPc6d1	hrudní
průduších	průduch	k1gInPc6	průduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
komáři	komár	k1gMnPc1	komár
se	se	k3xPyFc4	se
primárně	primárně	k6eAd1	primárně
živí	živit	k5eAaImIp3nS	živit
nektarem	nektar	k1gInSc7	nektar
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samičky	samička	k1gFnPc1	samička
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
schopny	schopen	k2eAgFnPc1d1	schopna
hematofágie	hematofágie	k1gFnPc1	hematofágie
(	(	kIx(	(
<g/>
sání	sání	k1gNnSc1	sání
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
ji	on	k3xPp3gFnSc4	on
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
přežití	přežití	k1gNnSc3	přežití
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
bílkovin	bílkovina	k1gFnPc2	bílkovina
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
komáři	komár	k1gMnPc1	komár
rodu	rod	k1gInSc2	rod
Toxorhynchites	Toxorhynchitesa	k1gFnPc2	Toxorhynchitesa
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
krev	krev	k1gFnSc4	krev
nesají	sát	k5eNaImIp3nP	sát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
rodu	rod	k1gInSc3	rod
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
největší	veliký	k2eAgInPc1d3	veliký
známé	známý	k2eAgInPc1d1	známý
druhy	druh	k1gInPc1	druh
komárů	komár	k1gMnPc2	komár
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
larvy	larva	k1gFnPc1	larva
požírají	požírat	k5eAaImIp3nP	požírat
larvy	larva	k1gFnPc4	larva
jiných	jiný	k2eAgInPc2d1	jiný
druhů	druh	k1gInPc2	druh
komárů	komár	k1gMnPc2	komár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
==	==	k?	==
</s>
</p>
<p>
<s>
Komár	komár	k1gMnSc1	komár
prochází	procházet	k5eAaImIp3nS	procházet
kompletní	kompletní	k2eAgFnSc7d1	kompletní
metamorfózou	metamorfóza	k1gFnSc7	metamorfóza
přes	přes	k7c4	přes
4	[number]	k4	4
vývojová	vývojový	k2eAgNnPc1d1	vývojové
stadia	stadion	k1gNnPc1	stadion
–	–	k?	–
vajíčko	vajíčko	k1gNnSc1	vajíčko
<g/>
,	,	kIx,	,
larva	larva	k1gFnSc1	larva
<g/>
,	,	kIx,	,
kukla	kukla	k1gFnSc1	kukla
a	a	k8xC	a
dospělec	dospělec	k1gMnSc1	dospělec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
popsal	popsat	k5eAaPmAgMnS	popsat
již	již	k6eAd1	již
řecký	řecký	k2eAgMnSc1d1	řecký
filozof	filozof	k1gMnSc1	filozof
Aristotelés	Aristotelésa	k1gFnPc2	Aristotelésa
<g/>
.	.	kIx.	.
</s>
<s>
Larvy	larva	k1gFnPc1	larva
i	i	k8xC	i
kukly	kukla	k1gFnPc1	kukla
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
vlhkém	vlhký	k2eAgNnSc6d1	vlhké
prostředí	prostředí	k1gNnSc6	prostředí
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
stojaté	stojatý	k2eAgFnSc6d1	stojatá
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přenos	přenos	k1gInSc1	přenos
nemocí	nemoc	k1gFnPc2	nemoc
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
probodnutí	probodnutí	k1gNnSc6	probodnutí
kůže	kůže	k1gFnSc2	kůže
samička	samička	k1gFnSc1	samička
vstříkne	vstříknout	k5eAaPmIp3nS	vstříknout
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
své	svůj	k3xOyFgFnPc4	svůj
sliny	slina	k1gFnPc4	slina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
omezují	omezovat	k5eAaImIp3nP	omezovat
srážlivost	srážlivost	k1gFnSc4	srážlivost
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
analgetikum	analgetikum	k1gNnSc1	analgetikum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
postižený	postižený	k2eAgMnSc1d1	postižený
tvor	tvor	k1gMnSc1	tvor
nevnímal	vnímat	k5eNaImAgMnS	vnímat
bodnutí	bodnutí	k1gNnSc4	bodnutí
a	a	k8xC	a
nebránil	bránit	k5eNaImAgMnS	bránit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
se	se	k3xPyFc4	se
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
zvířete	zvíře	k1gNnSc2	zvíře
či	či	k8xC	či
člověka	člověk	k1gMnSc2	člověk
dostanou	dostat	k5eAaPmIp3nP	dostat
choroboplodné	choroboplodný	k2eAgInPc4d1	choroboplodný
zárodky	zárodek	k1gInPc4	zárodek
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
malárie	malárie	k1gFnSc1	malárie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
zimničky	zimnička	k1gFnPc4	zimnička
–	–	k?	–
parazitičtí	parazitický	k2eAgMnPc1d1	parazitický
prvoci	prvok	k1gMnPc1	prvok
rodu	rod	k1gInSc2	rod
Plasmodium	plasmodium	k1gNnSc1	plasmodium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
je	on	k3xPp3gInPc4	on
přenášejí	přenášet	k5eAaImIp3nP	přenášet
výhradně	výhradně	k6eAd1	výhradně
komáři	komár	k1gMnPc1	komár
rodu	rod	k1gInSc2	rod
Anopheles	Anophelesa	k1gFnPc2	Anophelesa
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
druhů	druh	k1gInPc2	druh
ze	z	k7c2	z
200	[number]	k4	200
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
anofeles	anofeles	k1gInSc1	anofeles
čtyřskvrnný	čtyřskvrnný	k2eAgInSc1d1	čtyřskvrnný
(	(	kIx(	(
<g/>
Anopheles	Anopheles	k1gInSc1	Anopheles
maculipennis	maculipennis	k1gFnSc2	maculipennis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
přenašeči	přenašeč	k1gInPc7	přenašeč
chorob	choroba	k1gFnPc2	choroba
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
rody	rod	k1gInPc1	rod
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ptačí	ptačí	k2eAgFnSc4d1	ptačí
malárii	malárie	k1gFnSc4	malárie
běžně	běžně	k6eAd1	běžně
přenášejí	přenášet	k5eAaImIp3nP	přenášet
komáři	komár	k1gMnPc1	komár
rodu	rod	k1gInSc2	rod
Culex	Culex	k1gInSc1	Culex
<g/>
.	.	kIx.	.
</s>
<s>
Komár	komár	k1gMnSc1	komár
Aedes	Aedes	k1gMnSc1	Aedes
aegypti	aegypť	k1gFnSc2	aegypť
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
vektorem	vektor	k1gInSc7	vektor
arboviru	arbovir	k1gInSc2	arbovir
žluté	žlutý	k2eAgFnSc2d1	žlutá
zimnice	zimnice	k1gFnSc2	zimnice
<g/>
,	,	kIx,	,
viru	vir	k1gInSc2	vir
zika	zikum	k1gNnSc2	zikum
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
horečnatých	horečnatý	k2eAgNnPc2d1	horečnaté
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
komárů	komár	k1gMnPc2	komár
vyskytující	vyskytující	k2eAgFnSc4d1	vyskytující
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
mohou	moct	k5eAaImIp3nP	moct
pasivně	pasivně	k6eAd1	pasivně
přenášet	přenášet	k5eAaImF	přenášet
lymeskou	lymeský	k2eAgFnSc4d1	lymeská
borreliózu	borrelióza	k1gFnSc4	borrelióza
<g/>
.	.	kIx.	.
</s>
<s>
Pasivní	pasivní	k2eAgInSc1d1	pasivní
přenos	přenos	k1gInSc1	přenos
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
původce	původce	k1gMnSc1	původce
nemoci	nemoc	k1gFnSc2	nemoc
se	se	k3xPyFc4	se
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
komára	komár	k1gMnSc2	komár
nemnoží	množit	k5eNaImIp3nS	množit
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
byl	být	k5eAaImAgInS	být
přenesen	přenesen	k2eAgInSc1d1	přenesen
s	s	k7c7	s
čerstvou	čerstvý	k2eAgFnSc7d1	čerstvá
krví	krev	k1gFnSc7	krev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
komárovi	komár	k1gMnSc3	komár
ulpěla	ulpět	k5eAaPmAgFnS	ulpět
na	na	k7c6	na
sosáku	sosák	k1gInSc6	sosák
a	a	k8xC	a
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Komár	komár	k1gMnSc1	komár
záplavový	záplavový	k2eAgMnSc1d1	záplavový
přenáší	přenášet	k5eAaImIp3nS	přenášet
virus	virus	k1gInSc1	virus
Ťahyňa	Ťahyň	k1gInSc2	Ťahyň
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
valtickou	valtický	k2eAgFnSc4d1	Valtická
horečku	horečka	k1gFnSc4	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
žádný	žádný	k3yNgInSc1	žádný
případ	případ	k1gInSc1	případ
přenosu	přenos	k1gInSc2	přenos
viru	vir	k1gInSc2	vir
HIV	HIV	kA	HIV
(	(	kIx(	(
<g/>
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nemoc	nemoc	k1gFnSc1	nemoc
AIDS	AIDS	kA	AIDS
<g/>
)	)	kIx)	)
komáry	komár	k1gMnPc4	komár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
žije	žít	k5eAaImIp3nS	žít
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
druhů	druh	k1gInPc2	druh
komárů	komár	k1gMnPc2	komár
ze	z	k7c2	z
6	[number]	k4	6
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
lužních	lužní	k2eAgInPc6d1	lužní
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
lidských	lidský	k2eAgNnPc2d1	lidské
obydlí	obydlí	k1gNnPc2	obydlí
zalétá	zalétat	k5eAaPmIp3nS	zalétat
zejména	zejména	k9	zejména
komár	komár	k1gMnSc1	komár
pisklavý	pisklavý	k2eAgMnSc1d1	pisklavý
(	(	kIx(	(
<g/>
Culex	Culex	k1gInSc1	Culex
pipiens	pipiens	k1gInSc1	pipiens
<g/>
)	)	kIx)	)
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
žije	žít	k5eAaImIp3nS	žít
jeho	on	k3xPp3gInSc4	on
poddruh	poddruh	k1gInSc4	poddruh
komár	komár	k1gMnSc1	komár
obtížný	obtížný	k2eAgMnSc1d1	obtížný
(	(	kIx(	(
<g/>
Culex	Culex	k1gInSc1	Culex
pipiens	pipiens	k1gInSc1	pipiens
molestus	molestus	k1gInSc1	molestus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
londýnského	londýnský	k2eAgMnSc2d1	londýnský
metra	metr	k1gMnSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
povodních	povodeň	k1gFnPc6	povodeň
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
výskyt	výskyt	k1gInSc1	výskyt
komára	komár	k1gMnSc2	komár
záplavového	záplavový	k2eAgMnSc2d1	záplavový
(	(	kIx(	(
<g/>
Aedes	Aedes	k1gInSc1	Aedes
vexans	vexans	k1gInSc1	vexans
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
komáři	komár	k1gMnPc1	komár
rodu	rod	k1gInSc2	rod
Anopheles	Anophelesa	k1gFnPc2	Anophelesa
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
podle	podle	k7c2	podle
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
ještě	ještě	k9	ještě
v	v	k7c4	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sužovali	sužovat	k5eAaImAgMnP	sužovat
malárií	malárie	k1gFnSc7	malárie
jižní	jižní	k2eAgFnSc4d1	jižní
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc4d1	základní
ochranu	ochrana	k1gFnSc4	ochrana
lidí	člověk	k1gMnPc2	člověk
proti	proti	k7c3	proti
bodnutí	bodnutí	k1gNnSc1	bodnutí
komárem	komár	k1gMnSc7	komár
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
moskytiéra	moskytiéra	k1gFnSc1	moskytiéra
–	–	k?	–
jemná	jemný	k2eAgFnSc1d1	jemná
síť	síť	k1gFnSc1	síť
natažená	natažený	k2eAgFnSc1d1	natažená
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
kolem	kolem	k7c2	kolem
lůžka	lůžko	k1gNnSc2	lůžko
či	či	k8xC	či
obydlí	obydlí	k1gNnSc2	obydlí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
napuštěna	napuštěn	k2eAgFnSc1d1	napuštěna
insekticidem	insekticid	k1gInSc7	insekticid
<g/>
,	,	kIx,	,
účinnost	účinnost	k1gFnSc1	účinnost
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
prostředky	prostředek	k1gInPc4	prostředek
patří	patřit	k5eAaImIp3nS	patřit
uvolňování	uvolňování	k1gNnSc1	uvolňování
insekticidů	insekticid	k1gInPc2	insekticid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
syntetický	syntetický	k2eAgInSc1d1	syntetický
pyrethroid	pyrethroid	k1gInSc1	pyrethroid
<g/>
)	)	kIx)	)
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
slouží	sloužit	k5eAaImIp3nP	sloužit
spreje	sprej	k1gInPc1	sprej
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgInPc1d1	elektrický
odpařovače	odpařovač	k1gInPc1	odpařovač
<g/>
,	,	kIx,	,
insekticidní	insekticidní	k2eAgFnPc1d1	insekticidní
spirály	spirála	k1gFnPc1	spirála
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
jsou	být	k5eAaImIp3nP	být
odpuzovače	odpuzovač	k1gInPc1	odpuzovač
hmyzu	hmyz	k1gInSc2	hmyz
chemické	chemický	k2eAgInPc1d1	chemický
(	(	kIx(	(
<g/>
repelenty	repelent	k1gInPc1	repelent
<g/>
)	)	kIx)	)
či	či	k8xC	či
ultrazvukové	ultrazvukový	k2eAgFnPc1d1	ultrazvuková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
poměrně	poměrně	k6eAd1	poměrně
účinné	účinný	k2eAgInPc1d1	účinný
DDT	DDT	kA	DDT
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
toxické	toxický	k2eAgNnSc1d1	toxické
i	i	k9	i
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
vyšších	vysoký	k2eAgMnPc2d2	vyšší
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
negativní	negativní	k2eAgInPc4d1	negativní
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
ekosystém	ekosystém	k1gInSc4	ekosystém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Mosquito	Mosquit	k2eAgNnSc1d1	Mosquito
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
komárovití	komárovitý	k2eAgMnPc1d1	komárovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
komár	komár	k1gMnSc1	komár
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Zachrání	zachránit	k5eAaPmIp3nP	zachránit
mutovaní	mutovaný	k2eAgMnPc1d1	mutovaný
komáři	komár	k1gMnPc1	komár
lidské	lidský	k2eAgFnPc1d1	lidská
životy	život	k1gInPc4	život
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
-	-	kIx~	-
časopis	časopis	k1gInSc1	časopis
ABC	ABC	kA	ABC
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
European	European	k1gMnSc1	European
Mosquito	Mosquit	k2eAgNnSc1d1	Mosquito
Bulletin	bulletin	k1gInSc4	bulletin
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
