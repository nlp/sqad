<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
City	city	k1gNnSc1	city
of	of	k?	of
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
v	v	k7c6	v
běžném	běžný	k2eAgNnSc6d1	běžné
užívání	užívání	k1gNnSc6	užívání
i	i	k8xC	i
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
City	City	k1gFnPc2	City
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejlidnatější	lidnatý	k2eAgNnSc1d3	nejlidnatější
město	město	k1gNnSc1	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
výběžku	výběžek	k1gInSc6	výběžek
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Hudson	Hudsona	k1gFnPc2	Hudsona
do	do	k7c2	do
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
sousedních	sousední	k2eAgInPc2d1	sousední
států	stát	k1gInPc2	stát
a	a	k8xC	a
která	který	k3yRgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejlidnatější	lidnatý	k2eAgInSc4d3	nejlidnatější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Nizozemci	Nizozemec	k1gMnPc7	Nizozemec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1625	[number]	k4	1625
jako	jako	k8xC	jako
Nový	nový	k2eAgInSc1d1	nový
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
<g/>
,	,	kIx,	,
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Angličanů	Angličan	k1gMnPc2	Angličan
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
roku	rok	k1gInSc2	rok
1664	[number]	k4	1664
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
tvoří	tvořit	k5eAaImIp3nS	tvořit
pět	pět	k4xCc4	pět
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Bronx	Bronx	k1gInSc1	Bronx
<g/>
,	,	kIx,	,
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
<g/>
,	,	kIx,	,
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
,	,	kIx,	,
Queens	Queens	k1gInSc1	Queens
a	a	k8xC	a
Staten	Staten	k2eAgInSc1d1	Staten
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1785	[number]	k4	1785
<g/>
-	-	kIx~	-
<g/>
1790	[number]	k4	1790
byl	být	k5eAaImAgInS	být
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
světových	světový	k2eAgNnPc2d1	světové
center	centrum	k1gNnPc2	centrum
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
finančnictví	finančnictví	k1gNnSc2	finančnictví
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
vliv	vliv	k1gInSc1	vliv
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
zábavy	zábava	k1gFnSc2	zábava
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
reklamy	reklama	k1gFnSc2	reklama
a	a	k8xC	a
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
se	se	k3xPyFc4	se
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
prestižního	prestižní	k2eAgInSc2d1	prestižní
žebříčku	žebříček	k1gInSc2	žebříček
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
módních	módní	k2eAgFnPc2d1	módní
metropolí	metropol	k1gFnPc2	metropol
světa	svět	k1gInSc2	svět
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
oficiálně	oficiálně	k6eAd1	oficiálně
používat	používat	k5eAaImF	používat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
módy	móda	k1gFnSc2	móda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
také	také	k9	také
ohniskem	ohnisko	k1gNnSc7	ohnisko
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
diplomacie	diplomacie	k1gFnSc2	diplomacie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
sídlo	sídlo	k1gNnSc4	sídlo
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Yorku	York	k1gInSc2	York
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
celodennímu	celodenní	k2eAgInSc3d1	celodenní
ruchu	ruch	k1gInSc3	ruch
někdy	někdy	k6eAd1	někdy
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nikdy	nikdy	k6eAd1	nikdy
nespí	spát	k5eNaImIp3nS	spát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
City	City	k1gFnSc2	City
That	That	k1gMnSc1	That
Never	Never	k1gMnSc1	Never
Sleeps	Sleepsa	k1gFnPc2	Sleepsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
přezdívkami	přezdívka	k1gFnPc7	přezdívka
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
Velké	velký	k2eAgNnSc1d1	velké
jablko	jablko	k1gNnSc1	jablko
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Big	Big	k1gMnSc3	Big
Apple	Apple	kA	Apple
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Gotham	Gotham	k1gInSc1	Gotham
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>

