<s>
Profesor	profesor	k1gMnSc1	profesor
Antonín	Antonín	k1gMnSc1	Antonín
Benjamin	Benjamin	k1gMnSc1	Benjamin
Svojsík	Svojsík	k1gMnSc1	Svojsík
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1876	[number]	k4	1876
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
pedagogem	pedagog	k1gMnSc7	pedagog
a	a	k8xC	a
zakladatelem	zakladatel	k1gMnSc7	zakladatel
junáctví	junáctví	k1gNnSc2	junáctví
<g/>
,	,	kIx,	,
českého	český	k2eAgNnSc2d1	české
hnutí	hnutí	k1gNnSc2	hnutí
skautingu	skauting	k1gInSc2	skauting
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
propagátorem	propagátor	k1gMnSc7	propagátor
a	a	k8xC	a
organizátorem	organizátor	k1gMnSc7	organizátor
<g/>
.	.	kIx.	.
</s>
