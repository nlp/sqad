<s>
Profesor	profesor	k1gMnSc1	profesor
Antonín	Antonín	k1gMnSc1	Antonín
Benjamin	Benjamin	k1gMnSc1	Benjamin
Svojsík	Svojsík	k1gMnSc1	Svojsík
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1876	[number]	k4	1876
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
pedagogem	pedagog	k1gMnSc7	pedagog
a	a	k8xC	a
zakladatelem	zakladatel	k1gMnSc7	zakladatel
junáctví	junáctví	k1gNnSc2	junáctví
<g/>
,	,	kIx,	,
českého	český	k2eAgNnSc2d1	české
hnutí	hnutí	k1gNnSc2	hnutí
skautingu	skauting	k1gInSc2	skauting
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
propagátorem	propagátor	k1gMnSc7	propagátor
a	a	k8xC	a
organizátorem	organizátor	k1gMnSc7	organizátor
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
Antonín	Antonín	k1gMnSc1	Antonín
František	František	k1gMnSc1	František
Svojsík	Svojsík	k1gMnSc1	Svojsík
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
druhý	druhý	k4xOgMnSc1	druhý
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
synů	syn	k1gMnPc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
Alois	Alois	k1gMnSc1	Alois
Svojsík	Svojsík	k1gMnSc1	Svojsík
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
-	-	kIx~	-
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
publikací	publikace	k1gFnSc7	publikace
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
lid	lid	k1gInSc1	lid
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zdaleka	zdaleka	k6eAd1	zdaleka
nejobsáhlejší	obsáhlý	k2eAgFnSc7d3	nejobsáhlejší
knihou	kniha	k1gFnSc7	kniha
o	o	k7c6	o
Japonsku	Japonsko	k1gNnSc6	Japonsko
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
až	až	k6eAd1	až
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
bydlel	bydlet	k5eAaImAgMnS	bydlet
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
sourozenci	sourozenec	k1gMnPc7	sourozenec
ve	v	k7c6	v
Dvoře	Dvůr	k1gInSc6	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Antonín	Antonín	k1gMnSc1	Antonín
Svojsík	Svojsík	k1gMnSc1	Svojsík
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1835	[number]	k4	1835
Holice	holice	k1gFnSc1	holice
-	-	kIx~	-
1.2	[number]	k4	1.2
<g/>
.1880	.1880	k4	.1880
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
)	)	kIx)	)
soudním	soudní	k2eAgMnSc7d1	soudní
úředníkem	úředník	k1gMnSc7	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
matka	matka	k1gFnSc1	matka
Ludmila	Ludmila	k1gFnSc1	Ludmila
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
byl	být	k5eAaImAgInS	být
pohybově	pohybově	k6eAd1	pohybově
nadaný	nadaný	k2eAgMnSc1d1	nadaný
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
učitelem	učitel	k1gMnSc7	učitel
tělocviku	tělocvik	k1gInSc2	tělocvik
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
splnilo	splnit	k5eAaPmAgNnS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
také	také	k9	také
rád	rád	k6eAd1	rád
zpíval	zpívat	k5eAaImAgMnS	zpívat
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
členem	člen	k1gInSc7	člen
Českého	český	k2eAgNnSc2d1	české
pěveckého	pěvecký	k2eAgNnSc2d1	pěvecké
kvarteta	kvarteto	k1gNnSc2	kvarteto
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
získal	získat	k5eAaPmAgInS	získat
jako	jako	k9	jako
nejmladší	mladý	k2eAgFnSc4d3	nejmladší
přezdívku	přezdívka	k1gFnSc4	přezdívka
Benjamin	Benjamin	k1gMnSc1	Benjamin
<g/>
.	.	kIx.	.
</s>
<s>
Kvarteto	kvarteto	k1gNnSc1	kvarteto
se	se	k3xPyFc4	se
vydalo	vydat	k5eAaPmAgNnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
světě	svět	k1gInSc6	svět
a	a	k8xC	a
sklízelo	sklízet	k5eAaImAgNnS	sklízet
velké	velký	k2eAgInPc4d1	velký
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Svojsík	Svojsík	k1gMnSc1	Svojsík
několikrát	několikrát	k6eAd1	několikrát
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
město	město	k1gNnSc1	město
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
Junáckých	junácký	k2eAgInPc2d1	junácký
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgMnSc3	svůj
staršímu	starý	k2eAgMnSc3d2	starší
bratru	bratr	k1gMnSc3	bratr
Aloisovi	Alois	k1gMnSc3	Alois
a	a	k8xC	a
Josefu	Josef	k1gMnSc3	Josef
Meilbeckovi	Meilbecek	k1gMnSc3	Meilbecek
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
skautingem	skauting	k1gInSc7	skauting
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
o	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
tehdy	tehdy	k6eAd1	tehdy
35	[number]	k4	35
<g/>
letý	letý	k2eAgMnSc1d1	letý
profesor	profesor	k1gMnSc1	profesor
Svojsík	Svojsík	k1gMnSc1	Svojsík
z	z	k7c2	z
žižkovské	žižkovský	k2eAgFnSc2d1	Žižkovská
reálky	reálka	k1gFnSc2	reálka
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
navštívil	navštívit	k5eAaPmAgMnS	navštívit
skautský	skautský	k2eAgInSc4d1	skautský
tábor	tábor	k1gInSc4	tábor
a	a	k8xC	a
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
metodami	metoda	k1gFnPc7	metoda
výchovy	výchova	k1gFnSc2	výchova
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgInS	vrátit
se	se	k3xPyFc4	se
nadšený	nadšený	k2eAgInSc1d1	nadšený
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
překládat	překládat	k5eAaImF	překládat
knihu	kniha	k1gFnSc4	kniha
Scouting	Scouting	k1gInSc1	Scouting
for	forum	k1gNnPc2	forum
boys	boy	k1gMnPc2	boy
od	od	k7c2	od
R.	R.	kA	R.
B.	B.	kA	B.
Powella	Powella	k1gMnSc1	Powella
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
uvědomoval	uvědomovat	k5eAaImAgInS	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přesně	přesně	k6eAd1	přesně
přenést	přenést	k5eAaPmF	přenést
anglický	anglický	k2eAgInSc4d1	anglický
skauting	skauting	k1gInSc4	skauting
do	do	k7c2	do
českých	český	k2eAgInPc2d1	český
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
nakonec	nakonec	k6eAd1	nakonec
nevydal	vydat	k5eNaPmAgMnS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prázdninách	prázdniny	k1gFnPc6	prázdniny
1911	[number]	k4	1911
sestavil	sestavit	k5eAaPmAgInS	sestavit
pokusnou	pokusný	k2eAgFnSc4d1	pokusná
skautskou	skautský	k2eAgFnSc4d1	skautská
družinu	družina	k1gFnSc4	družina
z	z	k7c2	z
žáků	žák	k1gMnPc2	žák
žižkovské	žižkovský	k2eAgFnSc2d1	Žižkovská
reálky	reálka	k1gFnSc2	reálka
<g/>
.	.	kIx.	.
</s>
<s>
Radil	radit	k5eAaImAgMnS	radit
se	se	k3xPyFc4	se
s	s	k7c7	s
významnými	významný	k2eAgMnPc7d1	významný
odborníky	odborník	k1gMnPc7	odborník
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
další	další	k2eAgFnSc2d1	další
známé	známý	k2eAgFnSc2d1	známá
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
profesory	profesor	k1gMnPc4	profesor
Čádu	Čáda	k1gFnSc4	Čáda
<g/>
,	,	kIx,	,
Drtinu	drtina	k1gFnSc4	drtina
<g/>
,	,	kIx,	,
Masaryka	Masaryk	k1gMnSc4	Masaryk
<g/>
,	,	kIx,	,
Kramáře	kramář	k1gMnSc4	kramář
apod.	apod.	kA	apod.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
vydal	vydat	k5eAaPmAgMnS	vydat
knihu	kniha	k1gFnSc4	kniha
"	"	kIx"	"
<g/>
Základy	základ	k1gInPc1	základ
junáctví	junáctví	k1gNnSc1	junáctví
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
přispěla	přispět	k5eAaPmAgFnS	přispět
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
profesorů	profesor	k1gMnPc2	profesor
-	-	kIx~	-
M.	M.	kA	M.
Aleš	Aleš	k1gMnSc1	Aleš
(	(	kIx(	(
<g/>
nakreslil	nakreslit	k5eAaPmAgInS	nakreslit
obálku	obálka	k1gFnSc4	obálka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
K.	K.	kA	K.
V.	V.	kA	V.
Rais	Rais	k1gMnSc1	Rais
<g/>
,	,	kIx,	,
dr	dr	kA	dr
<g/>
.	.	kIx.	.
J.	J.	kA	J.
Guth-Jarkovský	Guth-Jarkovský	k2eAgMnSc1d1	Guth-Jarkovský
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
veřejnosti	veřejnost	k1gFnSc2	veřejnost
byla	být	k5eAaImAgFnS	být
rozporuplná	rozporuplný	k2eAgFnSc1d1	rozporuplná
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Humoristické	humoristický	k2eAgInPc1d1	humoristický
listy	list	k1gInPc1	list
označily	označit	k5eAaPmAgInP	označit
skauty	skaut	k1gMnPc4	skaut
jako	jako	k8xS	jako
moderní	moderní	k2eAgMnPc4d1	moderní
cikány	cikán	k1gMnPc4	cikán
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
zachovávali	zachovávat	k5eAaImAgMnP	zachovávat
zdrženlivý	zdrženlivý	k2eAgInSc4d1	zdrženlivý
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
o	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
vedl	vést	k5eAaImAgInS	vést
první	první	k4xOgInSc1	první
skautský	skautský	k2eAgInSc1d1	skautský
tábor	tábor	k1gInSc1	tábor
nedaleko	nedaleko	k7c2	nedaleko
hradu	hrad	k1gInSc2	hrad
Lipnice	Lipnice	k1gFnSc2	Lipnice
<g/>
.	.	kIx.	.
</s>
<s>
Tábor	Tábor	k1gInSc1	Tábor
měl	mít	k5eAaImAgInS	mít
13	[number]	k4	13
účastníků	účastník	k1gMnPc2	účastník
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
průběh	průběh	k1gInSc4	průběh
Svojsík	Svojsík	k1gMnSc1	Svojsík
vylíčil	vylíčit	k5eAaPmAgMnS	vylíčit
v	v	k7c6	v
brožurce	brožurka	k1gFnSc6	brožurka
Den	den	k1gInSc4	den
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
Junáků	junák	k1gMnPc2	junák
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
také	také	k9	také
Skautský	skautský	k2eAgInSc1d1	skautský
deník	deník	k1gInSc1	deník
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
básník	básník	k1gMnSc1	básník
popisuje	popisovat	k5eAaImIp3nS	popisovat
své	svůj	k3xOyFgInPc4	svůj
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
tábora	tábor	k1gInSc2	tábor
na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
místě	místo	k1gNnSc6	místo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Svojsík	Svojsík	k1gMnSc1	Svojsík
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
pokusil	pokusit	k5eAaPmAgMnS	pokusit
začlenit	začlenit	k5eAaPmF	začlenit
vznikající	vznikající	k2eAgFnSc2d1	vznikající
skautské	skautský	k2eAgFnSc2d1	skautská
družiny	družina	k1gFnSc2	družina
do	do	k7c2	do
Sokola	Sokol	k1gMnSc2	Sokol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k9	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgInS	založit
tedy	tedy	k9	tedy
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
samostatný	samostatný	k2eAgInSc1d1	samostatný
skautský	skautský	k2eAgInSc1d1	skautský
spolek	spolek	k1gInSc1	spolek
Junák	junák	k1gMnSc1	junák
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
skaut	skaut	k1gMnSc1	skaut
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
prvním	první	k4xOgMnSc7	první
starostou	starosta	k1gMnSc7	starosta
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pražský	pražský	k2eAgMnSc1d1	pražský
hygienik	hygienik	k1gMnSc1	hygienik
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Čeněk	Čeněk	k1gMnSc1	Čeněk
Klika	Klika	k1gMnSc1	Klika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
začal	začít	k5eAaPmAgMnS	začít
vydávat	vydávat	k5eAaPmF	vydávat
časopis	časopis	k1gInSc4	časopis
Junák	junák	k1gMnSc1	junák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
udržel	udržet	k5eAaPmAgMnS	udržet
vydávat	vydávat	k5eAaPmF	vydávat
i	i	k9	i
v	v	k7c6	v
těžké	těžký	k2eAgFnSc6d1	těžká
válečné	válečný	k2eAgFnSc6d1	válečná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Skauti	skaut	k1gMnPc1	skaut
pracovali	pracovat	k5eAaImAgMnP	pracovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Červeného	Červeného	k2eAgInSc2d1	Červeného
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyli	být	k5eNaImAgMnP	být
využíváni	využívat	k5eAaImNgMnP	využívat
pro	pro	k7c4	pro
válečné	válečný	k2eAgInPc4d1	válečný
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
asi	asi	k9	asi
300	[number]	k4	300
skautů	skaut	k1gMnPc2	skaut
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
zapojili	zapojit	k5eAaPmAgMnP	zapojit
plně	plně	k6eAd1	plně
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
skautská	skautský	k2eAgFnSc1d1	skautská
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
spolehlivě	spolehlivě	k6eAd1	spolehlivě
starala	starat	k5eAaImAgFnS	starat
o	o	k7c6	o
doručování	doručování	k1gNnSc6	doručování
zásilek	zásilka	k1gFnPc2	zásilka
v	v	k7c6	v
revoluční	revoluční	k2eAgFnSc6d1	revoluční
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgMnPc1d2	starší
skauti	skaut	k1gMnPc1	skaut
působili	působit	k5eAaImAgMnP	působit
jako	jako	k8xS	jako
ostraha	ostraha	k1gFnSc1	ostraha
důležitých	důležitý	k2eAgInPc2d1	důležitý
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
skautky	skautka	k1gFnPc1	skautka
zase	zase	k9	zase
jako	jako	k9	jako
úřednice	úřednice	k1gFnPc1	úřednice
<g/>
,	,	kIx,	,
telefonistky	telefonistka	k1gFnPc1	telefonistka
a	a	k8xC	a
tlumočnice	tlumočnice	k1gFnPc1	tlumočnice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1919	[number]	k4	1919
založil	založit	k5eAaPmAgInS	založit
Svojsík	Svojsík	k1gInSc1	Svojsík
Svaz	svaz	k1gInSc1	svaz
junáků	junák	k1gMnPc2	junák
-	-	kIx~	-
skautů	skaut	k1gMnPc2	skaut
RČS	RČS	kA	RČS
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
náčelníkem	náčelník	k1gMnSc7	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
ústředí	ústředí	k1gNnSc3	ústředí
skautské	skautský	k2eAgFnSc2d1	skautská
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
skauting	skauting	k1gInSc4	skauting
stále	stále	k6eAd1	stále
získával	získávat	k5eAaImAgMnS	získávat
jak	jak	k8xS	jak
nové	nový	k2eAgMnPc4d1	nový
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
na	na	k7c6	na
dobré	dobrý	k2eAgFnSc6d1	dobrá
pověsti	pověst	k1gFnSc6	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Vznikaly	vznikat	k5eAaImAgFnP	vznikat
další	další	k2eAgFnPc1d1	další
skautské	skautský	k2eAgFnPc1d1	skautská
organizace	organizace	k1gFnPc1	organizace
s	s	k7c7	s
mírně	mírně	k6eAd1	mírně
odlišnými	odlišný	k2eAgInPc7d1	odlišný
názory	názor	k1gInPc7	názor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
největší	veliký	k2eAgFnSc1d3	veliký
organizace	organizace	k1gFnSc1	organizace
Sokol	Sokol	k1gMnSc1	Sokol
spatřovala	spatřovat	k5eAaImAgFnS	spatřovat
v	v	k7c6	v
Junáku	junák	k1gMnSc6	junák
určitou	určitý	k2eAgFnSc4d1	určitá
konkurenci	konkurence	k1gFnSc3	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
hrozby	hrozba	k1gFnSc2	hrozba
fašismu	fašismus	k1gInSc2	fašismus
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
podařilo	podařit	k5eAaPmAgNnS	podařit
český	český	k2eAgInSc4d1	český
skauting	skauting	k1gInSc4	skauting
sjednotit	sjednotit	k5eAaPmF	sjednotit
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Svojsíka	Svojsík	k1gMnSc4	Svojsík
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
"	"	kIx"	"
<g/>
Junák	junák	k1gMnSc1	junák
-	-	kIx~	-
svaz	svaz	k1gInSc1	svaz
skautů	skaut	k1gMnPc2	skaut
a	a	k8xC	a
skautek	skautka	k1gFnPc2	skautka
RČS	RČS	kA	RČS
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
1938	[number]	k4	1938
odejel	odejet	k5eAaPmAgMnS	odejet
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
výchovou	výchova	k1gFnSc7	výchova
ruské	ruský	k2eAgFnSc2d1	ruská
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
chtěl	chtít	k5eAaImAgMnS	chtít
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
pohledu	pohled	k1gInSc2	pohled
poznat	poznat	k5eAaPmF	poznat
nové	nový	k2eAgInPc4d1	nový
směry	směr	k1gInPc4	směr
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
tělovýchově	tělovýchova	k1gFnSc6	tělovýchova
<g/>
.	.	kIx.	.
</s>
<s>
Únavu	únava	k1gFnSc4	únava
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
přinášelo	přinášet	k5eAaImAgNnS	přinášet
nepohodlí	nepohodlí	k1gNnSc1	nepohodlí
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
nezvyklou	zvyklý	k2eNgFnSc4d1	nezvyklá
stravu	strava	k1gFnSc4	strava
a	a	k8xC	a
úmorná	úmorný	k2eAgNnPc4d1	úmorné
vedra	vedro	k1gNnPc4	vedro
snášel	snášet	k5eAaImAgMnS	snášet
dost	dost	k6eAd1	dost
těžce	těžce	k6eAd1	těžce
a	a	k8xC	a
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
sebezapřením	sebezapření	k1gNnSc7	sebezapření
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
domů	domů	k6eAd1	domů
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
týden	týden	k1gInSc4	týden
ulehl	ulehnout	k5eAaPmAgMnS	ulehnout
s	s	k7c7	s
horečkami	horečka	k1gFnPc7	horečka
způsobenými	způsobený	k2eAgFnPc7d1	způsobená
streptokokovou	streptokokový	k2eAgFnSc4d1	streptokoková
nákazou	nákaza	k1gFnSc7	nákaza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
stoupaly	stoupat	k5eAaImAgFnP	stoupat
i	i	k9	i
přes	přes	k7c4	přes
všechnu	všechen	k3xTgFnSc4	všechen
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
péči	péče	k1gFnSc4	péče
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c4	v
9	[number]	k4	9
hodin	hodina	k1gFnPc2	hodina
dopoledne	dopoledne	k6eAd1	dopoledne
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
tisíce	tisíc	k4xCgInPc1	tisíc
junáků	junák	k1gMnPc2	junák
doprovodily	doprovodit	k5eAaPmAgInP	doprovodit
svého	svůj	k3xOyFgMnSc2	svůj
náčelníka	náčelník	k1gMnSc2	náčelník
na	na	k7c4	na
památný	památný	k2eAgInSc4d1	památný
Vyšehradský	vyšehradský	k2eAgInSc4d1	vyšehradský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
junáctví	junáctví	k1gNnSc2	junáctví
Umění	umění	k1gNnSc2	umění
pozorovati	pozorovat	k5eAaImF	pozorovat
Táboření	táboření	k1gNnSc4	táboření
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
Výchova	výchova	k1gFnSc1	výchova
občana	občan	k1gMnSc2	občan
republiky	republika	k1gFnSc2	republika
skautováním	skautování	k1gNnSc7	skautování
Táborový	táborový	k2eAgInSc1d1	táborový
deník	deník	k1gInSc1	deník
šestnáctiletého	šestnáctiletý	k2eAgMnSc2d1	šestnáctiletý
skauta	skaut	k1gMnSc2	skaut
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
popud	popud	k1gInSc4	popud
po	po	k7c6	po
zapůjčení	zapůjčení	k1gNnSc6	zapůjčení
od	od	k7c2	od
maminky	maminka	k1gFnSc2	maminka
Jiřího	Jiří	k1gMnSc2	Jiří
Wolkera	Wolker	k1gMnSc2	Wolker
<g/>
.	.	kIx.	.
</s>
