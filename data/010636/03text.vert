<p>
<s>
Motýlek	motýlek	k1gInSc1	motýlek
je	být	k5eAaImIp3nS	být
doplněk	doplněk	k1gInSc4	doplněk
společenského	společenský	k2eAgInSc2d1	společenský
či	či	k8xC	či
formálního	formální	k2eAgInSc2d1	formální
oděvu	oděv	k1gInSc2	oděv
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
se	se	k3xPyFc4	se
uvázaný	uvázaný	k2eAgMnSc1d1	uvázaný
na	na	k7c6	na
krku	krk	k1gInSc6	krk
–	–	k?	–
kolem	kolem	k7c2	kolem
límečku	límeček	k1gInSc2	límeček
košile	košile	k1gFnSc2	košile
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgMnSc1d1	vhodný
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
vhodná	vhodný	k2eAgFnSc1d1	vhodná
kravata	kravata	k1gFnSc1	kravata
<g/>
;	;	kIx,	;
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
méně	málo	k6eAd2	málo
častější	častý	k2eAgNnSc1d2	častější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
byznyse	byznys	k1gInSc6	byznys
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
více	hodně	k6eAd2	hodně
na	na	k7c6	na
kulturních	kulturní	k2eAgFnPc6d1	kulturní
akcích	akce	k1gFnPc6	akce
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
jej	on	k3xPp3gMnSc4	on
nosí	nosit	k5eAaImIp3nP	nosit
už	už	k6eAd1	už
přibližně	přibližně	k6eAd1	přibližně
čtyři	čtyři	k4xCgNnPc1	čtyři
století	století	k1gNnPc1	století
<g/>
.	.	kIx.	.
<g/>
Motýlek	motýlek	k1gInSc1	motýlek
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
doplněk	doplněk	k1gInSc1	doplněk
pánského	pánský	k2eAgNnSc2d1	pánské
oblékání	oblékání	k1gNnSc2	oblékání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
několika	několik	k4yIc7	několik
desetiletími	desetiletí	k1gNnPc7	desetiletí
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
i	i	k9	i
do	do	k7c2	do
dámské	dámský	k2eAgFnSc2d1	dámská
módy	móda	k1gFnSc2	móda
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
však	však	k9	však
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
se	s	k7c7	s
šátky	šátek	k1gInPc7	šátek
a	a	k8xC	a
ascoty	ascot	k1gInPc7	ascot
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
lze	lze	k6eAd1	lze
sehnat	sehnat	k5eAaPmF	sehnat
i	i	k9	i
motýlky	motýlek	k1gInPc4	motýlek
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
délka	délka	k1gFnSc1	délka
motýlku	motýlek	k1gInSc2	motýlek
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
odpovídat	odpovídat	k5eAaImF	odpovídat
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
mezi	mezi	k7c7	mezi
okraji	okraj	k1gInPc7	okraj
obočí	obočí	k1gNnSc2	obočí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příležitosti	příležitost	k1gFnSc3	příležitost
nošení	nošení	k1gNnSc2	nošení
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
večerní	večerní	k2eAgInPc4d1	večerní
formální	formální	k2eAgInPc4d1	formální
obleky	oblek	k1gInPc4	oblek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
frak	frak	k1gInSc4	frak
nebo	nebo	k8xC	nebo
smoking	smoking	k1gInSc4	smoking
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
motýlek	motýlek	k1gMnSc1	motýlek
předepsanou	předepsaný	k2eAgFnSc4d1	předepsaná
součástí	součást	k1gFnSc7	součást
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
události	událost	k1gFnSc2	událost
–	–	k?	–
black	black	k1gMnSc1	black
tie	tie	k?	tie
event	event	k1gMnSc1	event
<g/>
,	,	kIx,	,
white	white	k5eAaPmIp2nP	white
tie	tie	k?	tie
event	event	k1gInSc4	event
<g/>
)	)	kIx)	)
striktně	striktně	k6eAd1	striktně
černé	černý	k2eAgFnPc1d1	černá
nebo	nebo	k8xC	nebo
bílé	bílý	k2eAgFnPc1d1	bílá
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
na	na	k7c4	na
plesy	ples	k1gInPc4	ples
a	a	k8xC	a
taneční	taneční	k2eAgInPc1d1	taneční
kursy	kurs	k1gInPc1	kurs
<g/>
,	,	kIx,	,
či	či	k8xC	či
bývají	bývat	k5eAaImIp3nP	bývat
součástí	součást	k1gFnSc7	součást
pracovních	pracovní	k2eAgFnPc2d1	pracovní
uniforem	uniforma	k1gFnPc2	uniforma
číšníků	číšník	k1gMnPc2	číšník
<g/>
,	,	kIx,	,
krupiérů	krupiér	k1gMnPc2	krupiér
a	a	k8xC	a
podobných	podobný	k2eAgFnPc2d1	podobná
profesí	profes	k1gFnPc2	profes
<g/>
.	.	kIx.	.
</s>
<s>
Častěji	často	k6eAd2	často
jej	on	k3xPp3gMnSc4	on
také	také	k9	také
nosí	nosit	k5eAaImIp3nP	nosit
akademici	akademik	k1gMnPc1	akademik
<g/>
,	,	kIx,	,
uvaděči	uvaděč	k1gMnPc1	uvaděč
<g/>
,	,	kIx,	,
zápasoví	zápasový	k2eAgMnPc1d1	zápasový
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
<g/>
,	,	kIx,	,
operní	operní	k2eAgMnPc1d1	operní
zpěváci	zpěvák	k1gMnPc1	zpěvák
<g/>
,	,	kIx,	,
dirigenti	dirigent	k1gMnPc1	dirigent
<g/>
,	,	kIx,	,
hudebníci	hudebník	k1gMnPc1	hudebník
big	big	k?	big
bandů	band	k1gInPc2	band
a	a	k8xC	a
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnPc2	součást
i	i	k8xC	i
některých	některý	k3yIgFnPc2	některý
uniforem	uniforma	k1gFnPc2	uniforma
<g/>
.	.	kIx.	.
</s>
<s>
Motýlky	motýlek	k1gInPc1	motýlek
(	(	kIx(	(
<g/>
jiných	jiný	k2eAgFnPc2d1	jiná
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
vzorů	vzor	k1gInPc2	vzor
<g/>
,	,	kIx,	,
materiálů	materiál	k1gInPc2	materiál
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
i	i	k8xC	i
poloformálního	poloformální	k2eAgNnSc2d1	poloformální
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
volnočasového	volnočasový	k2eAgNnSc2d1	volnočasové
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
Vázaný	vázaný	k2eAgMnSc1d1	vázaný
motýlek	motýlek	k1gMnSc1	motýlek
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
starší	starý	k2eAgFnSc7d2	starší
<g/>
,	,	kIx,	,
klasičtější	klasický	k2eAgFnSc7d2	klasičtější
a	a	k8xC	a
formálnější	formální	k2eAgFnSc7d2	formálnější
podobou	podoba	k1gFnSc7	podoba
motýlka	motýlek	k1gMnSc2	motýlek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
buď	buď	k8xC	buď
jediným	jediný	k2eAgInSc7d1	jediný
kusem	kus	k1gInSc7	kus
látky	látka	k1gFnSc2	látka
(	(	kIx(	(
<g/>
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
určen	určit	k5eAaPmNgInS	určit
jen	jen	k9	jen
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
obvod	obvod	k1gInSc4	obvod
krku	krk	k1gInSc2	krk
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
obvodu	obvod	k1gInSc6	obvod
opatřen	opatřit	k5eAaPmNgInS	opatřit
sponou	spona	k1gFnSc7	spona
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
níž	jenž	k3xRgFnSc2	jenž
jde	jít	k5eAaImIp3nS	jít
upravit	upravit	k5eAaPmF	upravit
jeho	jeho	k3xOp3gFnSc4	jeho
délku	délka	k1gFnSc4	délka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
symetrický	symetrický	k2eAgInSc1d1	symetrický
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k6eAd1	uprostřed
užší	úzký	k2eAgInSc4d2	užší
pás	pás	k1gInSc4	pás
<g/>
,	,	kIx,	,
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
půl	půl	k1xP	půl
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
vázaný	vázaný	k2eAgInSc1d1	vázaný
motýlek	motýlek	k1gInSc4	motýlek
široký	široký	k2eAgInSc4d1	široký
zhruba	zhruba	k6eAd1	zhruba
7	[number]	k4	7
cm	cm	kA	cm
v	v	k7c6	v
nejširším	široký	k2eAgNnSc6d3	nejširší
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
2-3	[number]	k4	2-3
cm	cm	kA	cm
podél	podél	k7c2	podél
krku	krk	k1gInSc2	krk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
85	[number]	k4	85
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Okraje	okraj	k1gInPc4	okraj
má	mít	k5eAaImIp3nS	mít
nejčastěji	často	k6eAd3	často
buď	buď	k8xC	buď
rovné	rovný	k2eAgFnPc4d1	rovná
<g/>
,	,	kIx,	,
kulaté	kulatý	k2eAgFnPc4d1	kulatá
nebo	nebo	k8xC	nebo
do	do	k7c2	do
mírné	mírný	k2eAgFnSc2d1	mírná
špičky	špička	k1gFnSc2	špička
v	v	k7c6	v
tupém	tupý	k2eAgInSc6d1	tupý
úhlu	úhel	k1gInSc6	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
vázaného	vázané	k1gNnSc2	vázané
motýlka	motýlek	k1gMnSc2	motýlek
bývají	bývat	k5eAaImIp3nP	bývat
stejně	stejně	k6eAd1	stejně
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
po	po	k7c6	po
uvázání	uvázání	k1gNnSc6	uvázání
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
"	"	kIx"	"
<g/>
rubová	rubový	k2eAgFnSc1d1	rubová
<g/>
"	"	kIx"	"
v	v	k7c6	v
určitých	určitý	k2eAgNnPc6d1	určité
místech	místo	k1gNnPc6	místo
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
motýlku	motýlek	k1gInSc2	motýlek
není	být	k5eNaImIp3nS	být
symetrický	symetrický	k2eAgInSc1d1	symetrický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předvázaný	Předvázaný	k2eAgInSc1d1	Předvázaný
motýlek	motýlek	k1gInSc1	motýlek
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
předem	předem	k6eAd1	předem
uvázaným	uvázaný	k2eAgInSc7d1	uvázaný
nebo	nebo	k8xC	nebo
ušitým	ušitý	k2eAgInSc7d1	ušitý
uzlem	uzel	k1gInSc7	uzel
(	(	kIx(	(
<g/>
či	či	k8xC	či
jeho	jeho	k3xOp3gFnSc7	jeho
napodobeninou	napodobenina	k1gFnSc7	napodobenina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
připevněn	připevnit	k5eAaPmNgInS	připevnit
na	na	k7c6	na
pásku	pásek	k1gInSc6	pásek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
materiálu	materiál	k1gInSc2	materiál
jako	jako	k8xS	jako
uzel	uzel	k1gInSc4	uzel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
sponkou	sponka	k1gFnSc7	sponka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
připíná	připínat	k5eAaImIp3nS	připínat
na	na	k7c4	na
košili	košile	k1gFnSc4	košile
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vázaného	vázaný	k2eAgInSc2d1	vázaný
motýlku	motýlek	k1gInSc2	motýlek
též	též	k9	též
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
stejné	stejný	k2eAgFnPc1d1	stejná
konstrukce	konstrukce	k1gFnPc1	konstrukce
–	–	k?	–
tedy	tedy	k8xC	tedy
ne	ne	k9	ne
dvouvrstvý	dvouvrstvý	k2eAgInSc1d1	dvouvrstvý
ale	ale	k8xC	ale
jen	jen	k9	jen
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
látkového	látkový	k2eAgInSc2d1	látkový
obdélníčku	obdélníček	k1gInSc2	obdélníček
uprostřed	uprostřed	k7c2	uprostřed
stáhnutým	stáhnutý	k2eAgFnPc3d1	stáhnutá
stuhou	stuha	k1gFnSc7	stuha
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
předvázaný	předvázaný	k2eAgInSc4d1	předvázaný
motýlek	motýlek	k1gInSc4	motýlek
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
nosit	nosit	k5eAaImF	nosit
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
frakovou	frakový	k2eAgFnSc7d1	fraková
košilí	košile	k1gFnSc7	košile
<g/>
;	;	kIx,	;
též	též	k9	též
pro	pro	k7c4	pro
ty	ten	k3xDgFnPc4	ten
nejslavnostnější	slavnostní	k2eAgFnSc1d3	nejslavnostnější
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
zvolit	zvolit	k5eAaPmF	zvolit
vázaný	vázaný	k2eAgMnSc1d1	vázaný
motýlek	motýlek	k1gMnSc1	motýlek
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
poloformálního	poloformální	k2eAgNnSc2d1	poloformální
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
kreativního	kreativní	k2eAgInSc2d1	kreativní
stylu	styl	k1gInSc2	styl
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
módní	módní	k2eAgInSc4d1	módní
výstřelek	výstřelek	k1gInSc4	výstřelek
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
v	v	k7c6	v
aktuálně	aktuálně	k6eAd1	aktuálně
populární	populární	k2eAgFnSc3d1	populární
jiné-než-látkové	jinéežátkový	k2eAgFnSc3d1	jiné-než-látkový
formě	forma	k1gFnSc3	forma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
mimo	mimo	k7c4	mimo
označení	označení	k1gNnSc4	označení
bow-tie	bowie	k1gFnSc2	bow-tie
jako	jako	k8xC	jako
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
pojem	pojem	k1gInSc1	pojem
používá	používat	k5eAaImIp3nS	používat
ještě	ještě	k9	ještě
slova	slovo	k1gNnSc2	slovo
semi-butterfly	semiutterfnout	k5eAaPmAgInP	semi-butterfnout
pro	pro	k7c4	pro
motýlky	motýlek	k1gInPc4	motýlek
klasických	klasický	k2eAgFnPc2d1	klasická
velikostí	velikost	k1gFnPc2	velikost
<g/>
,	,	kIx,	,
butterfly	butterfly	k1gInPc1	butterfly
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgInPc4d1	vysoký
motýlky	motýlek	k1gInPc4	motýlek
a	a	k8xC	a
bat-wing	bating	k1gInSc4	bat-wing
pro	pro	k7c4	pro
nízké	nízký	k2eAgInPc4d1	nízký
motýlky	motýlek	k1gInPc4	motýlek
nebo	nebo	k8xC	nebo
vázané	vázaný	k2eAgInPc4d1	vázaný
motýlky	motýlek	k1gInPc4	motýlek
bez	bez	k7c2	bez
vlkového	vlkový	k2eAgNnSc2d1	vlkový
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
String	String	k1gInSc1	String
tie	tie	k?	tie
označuje	označovat	k5eAaImIp3nS	označovat
vázaný	vázaný	k2eAgInSc1d1	vázaný
motýlek	motýlek	k1gInSc1	motýlek
s	s	k7c7	s
prodlouženými	prodloužený	k2eAgInPc7d1	prodloužený
konci	konec	k1gInPc7	konec
–	–	k?	–
přecházející	přecházející	k2eAgNnSc4d1	přecházející
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
mašle	mašle	k1gFnSc2	mašle
<g/>
;	;	kIx,	;
ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
jižanské	jižanský	k2eAgInPc4d1	jižanský
státy	stát	k1gInPc4	stát
USA	USA	kA	USA
a	a	k8xC	a
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Materiál	materiál	k1gInSc1	materiál
==	==	k?	==
</s>
</p>
<p>
<s>
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
textilie	textilie	k1gFnPc1	textilie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
polyesterová	polyesterový	k2eAgFnSc1d1	polyesterová
vlákna	vlákna	k1gFnSc1	vlákna
(	(	kIx(	(
<g/>
u	u	k7c2	u
levnějších	levný	k2eAgFnPc2d2	levnější
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
hedvábí	hedvábí	k1gNnSc1	hedvábí
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
doporučený	doporučený	k2eAgInSc1d1	doporučený
materiál	materiál	k1gInSc1	materiál
na	na	k7c6	na
nejslavnostnější	slavnostní	k2eAgFnSc6d3	nejslavnostnější
příležitosti	příležitost	k1gFnSc6	příležitost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
satén	satén	k1gInSc1	satén
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
)	)	kIx)	)
úpletová	úpletový	k2eAgFnSc1d1	úpletová
vlna	vlna	k1gFnSc1	vlna
</s>
</p>
<p>
<s>
dřevo	dřevo	k1gNnSc1	dřevo
(	(	kIx(	(
<g/>
nošený	nošený	k2eAgMnSc1d1	nošený
zejména	zejména	k9	zejména
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sklo	sklo	k1gNnSc1	sklo
</s>
</p>
<p>
<s>
papír	papír	k1gInSc1	papír
</s>
</p>
<p>
<s>
kov	kov	k1gInSc1	kov
nebo	nebo	k8xC	nebo
raritně	raritně	k6eAd1	raritně
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
materiály	materiál	k1gInPc1	materiál
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
motýlek	motýlek	k1gInSc4	motýlek
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
motýlek	motýlek	k1gInSc4	motýlek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
https://web.archive.org/web/20161127085024/http://milkshirts.com/blog/bowties-for-the-holidays-everything-you-need-to-know/	[url]	k4	https://web.archive.org/web/20161127085024/http://milkshirts.com/blog/bowties-for-the-holidays-everything-you-need-to-know/
</s>
</p>
