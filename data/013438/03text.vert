<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
figuralista	figuralista	k1gMnSc1	figuralista
a	a	k8xC	a
krajinář	krajinář	k1gMnSc1	krajinář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
lakýrnictví	lakýrnictví	k1gNnSc2	lakýrnictví
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
i	i	k9	i
Josefův	Josefův	k2eAgMnSc1d1	Josefův
bratr	bratr	k1gMnSc1	bratr
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc1	jejich
bratranec	bratranec	k1gMnSc1	bratranec
byl	být	k5eAaImAgMnS	být
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
próz	próza	k1gFnPc2	próza
Karel	Karel	k1gMnSc1	Karel
Paťha	Paťha	k?	Paťha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc4	dílo
Josefa	Josef	k1gMnSc2	Josef
Krčila	krčit	k5eAaImAgFnS	krčit
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
obecně	obecně	k6eAd1	obecně
známé	známý	k2eAgFnSc2d1	známá
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gNnSc3	jeho
celoživotnímu	celoživotní	k2eAgNnSc3d1	celoživotní
působení	působení	k1gNnSc3	působení
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgNnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
poválečné	poválečný	k2eAgNnSc1d1	poválečné
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
si	se	k3xPyFc3	se
také	také	k9	také
plně	plně	k6eAd1	plně
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
přídomek	přídomek	k1gInSc4	přídomek
''	''	k?	''
<g/>
malíř	malíř	k1gMnSc1	malíř
svého	svůj	k3xOyFgNnSc2	svůj
města	město	k1gNnSc2	město
<g/>
''	''	k?	''
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pochopení	pochopení	k1gNnSc4	pochopení
této	tento	k3xDgFnSc2	tento
umělecké	umělecký	k2eAgFnSc2d1	umělecká
a	a	k8xC	a
životní	životní	k2eAgFnSc2d1	životní
etapy	etapa	k1gFnSc2	etapa
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
důležité	důležitý	k2eAgNnSc1d1	důležité
připomenout	připomenout	k5eAaPmF	připomenout
si	se	k3xPyFc3	se
právě	právě	k6eAd1	právě
počátky	počátek	k1gInPc4	počátek
a	a	k8xC	a
ranou	raný	k2eAgFnSc4d1	raná
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
ovlivňující	ovlivňující	k2eAgFnPc4d1	ovlivňující
dalších	další	k2eAgNnPc2d1	další
40	[number]	k4	40
let	léto	k1gNnPc2	léto
Krčilovy	Krčilův	k2eAgFnSc2d1	Krčilův
umělecké	umělecký	k2eAgFnSc2d1	umělecká
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Shromážděná	shromážděný	k2eAgFnSc1d1	shromážděná
kolekce	kolekce	k1gFnSc1	kolekce
43	[number]	k4	43
kreseb	kresba	k1gFnPc2	kresba
i	i	k8xC	i
obrazů	obraz	k1gInPc2	obraz
pochází	pocházet	k5eAaImIp3nS	pocházet
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
soukromých	soukromý	k2eAgFnPc2d1	soukromá
sbírek	sbírka	k1gFnPc2	sbírka
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
kutnohorské	kutnohorský	k2eAgFnSc2d1	Kutnohorská
pobočky	pobočka	k1gFnSc2	pobočka
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
Česká	český	k2eAgFnSc1d1	Česká
spořitelna	spořitelna	k1gFnSc1	spořitelna
<g/>
,	,	kIx,	,
Okresního	okresní	k2eAgNnSc2d1	okresní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
kutnohorské	kutnohorský	k2eAgFnSc2d1	Kutnohorská
pobočky	pobočka	k1gFnSc2	pobočka
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
Komerční	komerční	k2eAgFnSc1d1	komerční
banka	banka	k1gFnSc1	banka
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1924	[number]	k4	1924
až	až	k9	až
1949	[number]	k4	1949
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
pedagog	pedagog	k1gMnSc1	pedagog
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
umělec	umělec	k1gMnSc1	umělec
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
rektor	rektor	k1gMnSc1	rektor
akademie	akademie	k1gFnSc2	akademie
Jakub	Jakub	k1gMnSc1	Jakub
Obrovský	obrovský	k2eAgMnSc1d1	obrovský
<g/>
.	.	kIx.	.
</s>
<s>
Malířská	malířský	k2eAgFnSc1d1	malířská
<g/>
,	,	kIx,	,
grafická	grafický	k2eAgFnSc1d1	grafická
a	a	k8xC	a
sochařská	sochařský	k2eAgFnSc1d1	sochařská
figurální	figurální	k2eAgFnSc1d1	figurální
tvorba	tvorba	k1gFnSc1	tvorba
profesora	profesor	k1gMnSc2	profesor
Jakuba	Jakub	k1gMnSc2	Jakub
Obrovského	obrovský	k2eAgMnSc2d1	obrovský
charakterizovaná	charakterizovaný	k2eAgNnPc4d1	charakterizované
bezprostředním	bezprostřední	k2eAgInSc7d1	bezprostřední
vztahem	vztah	k1gInSc7	vztah
k	k	k7c3	k
realitě	realita	k1gFnSc3	realita
a	a	k8xC	a
k	k	k7c3	k
senzualismu	senzualismus	k1gInSc3	senzualismus
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
základem	základ	k1gInSc7	základ
<g/>
,	,	kIx,	,
měřítkem	měřítko	k1gNnSc7	měřítko
a	a	k8xC	a
vzorem	vzor	k1gInSc7	vzor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
tento	tento	k3xDgMnSc1	tento
pedagog	pedagog	k1gMnSc1	pedagog
poskytoval	poskytovat	k5eAaImAgMnS	poskytovat
svým	svůj	k3xOyFgMnPc3	svůj
žákům	žák	k1gMnPc3	žák
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
například	například	k6eAd1	například
Jan	Jan	k1gMnSc1	Jan
Úprka	Úprka	k1gMnSc1	Úprka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Koliha	koliha	k1gFnSc1	koliha
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Tikal	Tikal	k1gMnSc1	Tikal
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Černý	černý	k2eAgMnSc1d1	černý
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
českých	český	k2eAgMnPc2d1	český
umělců	umělec	k1gMnPc2	umělec
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
kutnohorský	kutnohorský	k2eAgMnSc1d1	kutnohorský
rodák	rodák	k1gMnSc1	rodák
malíř	malíř	k1gMnSc1	malíř
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
studoval	studovat	k5eAaImAgMnS	studovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
absolutoria	absolutorium	k1gNnSc2	absolutorium
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
období	období	k1gNnSc2	období
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
zde	zde	k6eAd1	zde
vystavený	vystavený	k2eAgInSc1d1	vystavený
soubor	soubor	k1gInSc1	soubor
19	[number]	k4	19
<g/>
uhlových	uhlový	k2eAgFnPc2d1	Uhlová
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
nejvíce	nejvíce	k6eAd1	nejvíce
patrná	patrný	k2eAgFnSc1d1	patrná
Krčilova	Krčilův	k2eAgFnSc1d1	Krčilův
kreslířská	kreslířský	k2eAgFnSc1d1	kreslířská
schopnost	schopnost	k1gFnSc1	schopnost
<g/>
,	,	kIx,	,
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
nadání	nadání	k1gNnSc1	nadání
a	a	k8xC	a
míra	míra	k1gFnSc1	míra
vlivu	vliv	k1gInSc2	vliv
figuralisty	figuralista	k1gMnSc2	figuralista
Jakuba	Jakub	k1gMnSc2	Jakub
Obrovského	obrovský	k2eAgMnSc2d1	obrovský
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
žáky	žák	k1gMnPc4	žák
<g/>
.	.	kIx.	.
</s>
<s>
Kresby	kresba	k1gFnPc1	kresba
Josefa	Josef	k1gMnSc2	Josef
Krčila	krčit	k5eAaImAgFnS	krčit
z	z	k7c2	z
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
zájmem	zájem	k1gInSc7	zájem
o	o	k7c6	o
postižení	postižení	k1gNnSc6	postižení
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
energickou	energický	k2eAgFnSc7d1	energická
linkou	linka	k1gFnSc7	linka
a	a	k8xC	a
sebejistým	sebejistý	k2eAgInSc7d1	sebejistý
rukopisem	rukopis	k1gInSc7	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Modelace	modelace	k1gFnSc1	modelace
je	být	k5eAaImIp3nS	být
utvářena	utvářit	k5eAaPmNgFnS	utvářit
v	v	k7c6	v
detailech	detail	k1gInPc6	detail
typickým	typický	k2eAgNnSc7d1	typické
skládáním	skládání	k1gNnSc7	skládání
objemu	objem	k1gInSc2	objem
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
plošek	ploška	k1gFnPc2	ploška
<g/>
,	,	kIx,	,
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
používaným	používaný	k2eAgMnSc7d1	používaný
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
i	i	k9	i
na	na	k7c6	na
olejomalbách	olejomalba	k1gFnPc6	olejomalba
městských	městský	k2eAgInPc2d1	městský
pohledů	pohled	k1gInPc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
byl	být	k5eAaImAgInS	být
výborným	výborný	k2eAgMnSc7d1	výborný
kreslířem	kreslíř	k1gMnSc7	kreslíř
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
tyto	tento	k3xDgInPc4	tento
své	svůj	k3xOyFgInPc4	svůj
grafické	grafický	k2eAgInPc4d1	grafický
předpoklady	předpoklad	k1gInPc4	předpoklad
zdůrazňované	zdůrazňovaný	k2eAgInPc4d1	zdůrazňovaný
a	a	k8xC	a
připomínané	připomínaný	k2eAgInPc4d1	připomínaný
mu	on	k3xPp3gInSc3	on
již	již	k6eAd1	již
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
bezvýhradně	bezvýhradně	k6eAd1	bezvýhradně
toužil	toužit	k5eAaImAgInS	toužit
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
malířem	malíř	k1gMnSc7	malíř
a	a	k8xC	a
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
malbě	malba	k1gFnSc3	malba
jako	jako	k9	jako
své	svůj	k3xOyFgFnSc3	svůj
hlavní	hlavní	k2eAgFnSc3d1	hlavní
doméně	doména	k1gFnSc3	doména
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
kresebnou	kresebný	k2eAgFnSc4d1	kresebná
jistotu	jistota	k1gFnSc4	jistota
využíval	využívat	k5eAaPmAgInS	využívat
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
prostředek	prostředek	k1gInSc4	prostředek
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
rozvržení	rozvržení	k1gNnSc4	rozvržení
nového	nový	k2eAgInSc2d1	nový
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
kompozici	kompozice	k1gFnSc4	kompozice
získaným	získaný	k2eAgInSc7d1	získaný
během	běh	k1gInSc7	běh
školení	školení	k1gNnSc4	školení
dekorativního	dekorativní	k2eAgNnSc2d1	dekorativní
malířství	malířství	k1gNnSc2	malířství
u	u	k7c2	u
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Černého	Černý	k1gMnSc2	Černý
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
předcházelo	předcházet	k5eAaImAgNnS	předcházet
studiím	studio	k1gNnPc3	studio
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
zabývat	zabývat	k5eAaImF	zabývat
vlastní	vlastní	k2eAgFnSc7d1	vlastní
volnou	volný	k2eAgFnSc7d1	volná
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
,	,	kIx,	,
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
studijní	studijní	k2eAgFnSc4d1	studijní
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
balkánskými	balkánský	k2eAgInPc7d1	balkánský
státy	stát	k1gInPc7	stát
až	až	k9	až
do	do	k7c2	do
Cařihradu	Cařihrad	k1gInSc2	Cařihrad
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pobytu	pobyt	k1gInSc2	pobyt
vytěžil	vytěžit	k5eAaPmAgInS	vytěžit
soubor	soubor	k1gInSc1	soubor
děl	dělo	k1gNnPc2	dělo
orientální	orientální	k2eAgFnSc2d1	orientální
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vystavena	vystaven	k2eAgFnSc1d1	vystavena
kresba	kresba	k1gFnSc1	kresba
tužkou	tužka	k1gFnSc7	tužka
Obchodník	obchodník	k1gMnSc1	obchodník
z	z	k7c2	z
Varny	Varna	k1gFnSc2	Varna
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mnohých	mnohý	k2eAgFnPc2d1	mnohá
letmých	letmý	k2eAgFnPc2d1	letmá
a	a	k8xC	a
svěžích	svěží	k2eAgFnPc2d1	svěží
skic	skica	k1gFnPc2	skica
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
zaznamenával	zaznamenávat	k5eAaImAgMnS	zaznamenávat
zážitky	zážitek	k1gInPc4	zážitek
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
až	až	k9	až
1949	[number]	k4	1949
začal	začít	k5eAaPmAgMnS	začít
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
působit	působit	k5eAaImF	působit
i	i	k9	i
jako	jako	k9	jako
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
učil	učít	k5eAaPmAgInS	učít
výtvarnou	výtvarný	k2eAgFnSc4d1	výtvarná
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
deskriptivu	deskriptiva	k1gFnSc4	deskriptiva
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
učňovské	učňovský	k2eAgFnSc2d1	učňovská
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ho	on	k3xPp3gMnSc4	on
bezesporu	bezesporu	k9	bezesporu
přiměla	přimět	k5eAaPmAgFnS	přimět
nutnost	nutnost	k1gFnSc4	nutnost
hledání	hledání	k1gNnSc2	hledání
stálého	stálý	k2eAgInSc2d1	stálý
zdroje	zdroj	k1gInSc2	zdroj
obživy	obživa	k1gFnSc2	obživa
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
oženil	oženit	k5eAaPmAgMnS	oženit
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1951	[number]	k4	1951
až	až	k8xS	až
1966	[number]	k4	1966
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
restaurátorské	restaurátorský	k2eAgFnSc3d1	restaurátorská
práci	práce	k1gFnSc3	práce
na	na	k7c6	na
obnově	obnova	k1gFnSc6	obnova
nástěnných	nástěnný	k2eAgFnPc2d1	nástěnná
maleb	malba	k1gFnPc2	malba
historických	historický	k2eAgInPc2d1	historický
objektů	objekt	k1gInPc2	objekt
i	i	k8xC	i
když	když	k8xS	když
vlastní	vlastní	k2eAgFnSc3d1	vlastní
volné	volný	k2eAgFnSc3d1	volná
tvorbě	tvorba	k1gFnSc3	tvorba
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
stále	stále	k6eAd1	stále
<g/>
,	,	kIx,	,
nezapomínaje	zapomínat	k5eNaImSgMnS	zapomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
ona	onen	k3xDgFnSc1	onen
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
po	po	k7c6	po
umělci	umělec	k1gMnSc6	umělec
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
dnes	dnes	k6eAd1	dnes
čítají	čítat	k5eAaImIp3nP	čítat
ne	ne	k9	ne
na	na	k7c4	na
desítky	desítka	k1gFnPc4	desítka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
stovky	stovka	k1gFnPc4	stovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
potence	potence	k1gFnSc1	potence
Josefa	Josef	k1gMnSc2	Josef
Krčila	krčit	k5eAaImAgFnS	krčit
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
jak	jak	k6eAd1	jak
v	v	k7c6	v
portrétní	portrétní	k2eAgFnSc6d1	portrétní
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zejména	zejména	k9	zejména
v	v	k7c6	v
cíleném	cílený	k2eAgNnSc6d1	cílené
zachycování	zachycování	k1gNnSc6	zachycování
historické	historický	k2eAgFnSc2d1	historická
architektury	architektura	k1gFnSc2	architektura
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
městských	městský	k2eAgFnPc2d1	městská
výsečí	výseč	k1gFnPc2	výseč
a	a	k8xC	a
zákoutí	zákoutí	k1gNnPc2	zákoutí
<g/>
,	,	kIx,	,
kutnohorských	kutnohorský	k2eAgFnPc2d1	Kutnohorská
uliček	ulička	k1gFnPc2	ulička
známých	známý	k2eAgFnPc2d1	známá
i	i	k9	i
zdánlivě	zdánlivě	k6eAd1	zdánlivě
všedních	všední	k2eAgInPc2d1	všední
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
stylizace	stylizace	k1gFnSc2	stylizace
autorovy	autorův	k2eAgFnSc2d1	autorova
olejomalby	olejomalba	k1gFnSc2	olejomalba
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
postimpresi	postimprese	k1gFnSc4	postimprese
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc1d1	původní
tmavá	tmavý	k2eAgFnSc1d1	tmavá
hutná	hutný	k2eAgFnSc1d1	hutná
barevnost	barevnost	k1gFnSc1	barevnost
uplatňovaná	uplatňovaný	k2eAgFnSc1d1	uplatňovaná
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
<g/>
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
některé	některý	k3yIgInPc4	některý
výjímky	výjímek	k1gInPc4	výjímek
např.	např.	kA	např.
cyklus	cyklus	k1gInSc4	cyklus
Modrý	modrý	k2eAgInSc1d1	modrý
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyjasňuje	vyjasňovat	k5eAaImIp3nS	vyjasňovat
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	let	k1gInSc1	let
třicátých	třicátý	k4xOgNnPc2	třicátý
a	a	k8xC	a
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
a	a	k8xC	a
počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1945	[number]	k4	1945
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc4d1	typický
městské	městský	k2eAgInPc4d1	městský
motivy	motiv	k1gInPc4	motiv
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
podávány	podáván	k2eAgMnPc4d1	podáván
pastózním	pastózní	k2eAgInSc7d1	pastózní
přednesem	přednes	k1gInSc7	přednes
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
zobrazovaný	zobrazovaný	k2eAgInSc4d1	zobrazovaný
materiál	materiál	k1gInSc4	materiál
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
okrů	okr	k1gInPc2	okr
<g/>
,	,	kIx,	,
žlutí	žluť	k1gFnPc2	žluť
a	a	k8xC	a
cihlové	cihlový	k2eAgFnSc2d1	cihlová
červeni	červeň	k1gFnSc2wR	červeň
kombinované	kombinovaný	k2eAgFnSc2d1	kombinovaná
dle	dle	k7c2	dle
volby	volba	k1gFnSc2	volba
motivu	motiv	k1gInSc2	motiv
s	s	k7c7	s
měkkou	měkký	k2eAgFnSc7d1	měkká
zelení	zeleň	k1gFnSc7	zeleň
vegetace	vegetace	k1gFnSc2	vegetace
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
dramatickými	dramatický	k2eAgFnPc7d1	dramatická
liniemi	linie	k1gFnPc7	linie
holých	holý	k2eAgInPc2d1	holý
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstavy	výstava	k1gFnPc1	výstava
==	==	k?	==
</s>
</p>
<p>
<s>
1937	[number]	k4	1937
X.	X.	kA	X.
výstava	výstava	k1gFnSc1	výstava
obrazů	obraz	k1gInPc2	obraz
z	z	k7c2	z
domova	domov	k1gInSc2	domov
i	i	k8xC	i
ciziny	cizina	k1gFnSc2	cizina
Josefa	Josefa	k1gFnSc1	Josefa
Krčila	krčit	k5eAaImAgFnS	krčit
<g/>
,	,	kIx,	,
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu-	listopadu-	k?	listopadu-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1941	[number]	k4	1941
Malíři	malíř	k1gMnPc7	malíř
Čáslavska	Čáslavsko	k1gNnSc2	Čáslavsko
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
s	s	k7c7	s
J.	J.	kA	J.
U.	U.	kA	U.
V.	V.	kA	V.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čáslav	Čáslav	k1gFnSc1	Čáslav
</s>
</p>
<p>
<s>
1946	[number]	k4	1946
Souborná	souborný	k2eAgFnSc1d1	souborná
výstava	výstava	k1gFnSc1	výstava
Josefa	Josef	k1gMnSc2	Josef
Krčila	krčit	k5eAaImAgFnS	krčit
<g/>
,	,	kIx,	,
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
Josefa	Josefa	k1gFnSc1	Josefa
Krčila	krčit	k5eAaImAgFnS	krčit
<g/>
,	,	kIx,	,
Rubešova	Rubešův	k2eAgFnSc1d1	Rubešova
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
Soubor	soubor	k1gInSc1	soubor
obrazů	obraz	k1gInPc2	obraz
Josefa	Josefa	k1gFnSc1	Josefa
Krčila	krčit	k5eAaImAgFnS	krčit
<g/>
,	,	kIx,	,
Výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
Mladé	mladý	k2eAgFnSc2d1	mladá
fronty	fronta	k1gFnSc2	fronta
<g/>
,	,	kIx,	,
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu-	listopadu-	k?	listopadu-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
<g/>
,	,	kIx,	,
Hornická	hornický	k2eAgFnSc1d1	hornická
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu-	listopadu-	k?	listopadu-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1951	[number]	k4	1951
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
Závodní	závodní	k2eAgInSc4d1	závodní
klub	klub	k1gInSc4	klub
Sázavan	Sázavan	k1gMnSc1	Sázavan
<g/>
,	,	kIx,	,
Společenský	společenský	k2eAgInSc1d1	společenský
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
Zruč	Zruč	k1gInSc1	Zruč
n.	n.	k?	n.
Sázavou	Sázava	k1gFnSc7	Sázava
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
v	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
Josefa	Josefa	k1gFnSc1	Josefa
Krčila	krčit	k5eAaImAgFnS	krčit
<g/>
,	,	kIx,	,
Jeneweinova	Jeneweinův	k2eAgFnSc1d1	Jeneweinova
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
Město	město	k1gNnSc1	město
nad	nad	k7c7	nad
Vrchlicí	vrchlice	k1gFnSc7	vrchlice
(	(	kIx(	(
<g/>
obrazy	obraz	k1gInPc4	obraz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tylův	Tylův	k2eAgInSc1d1	Tylův
památník	památník	k1gInSc1	památník
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
<g/>
,	,	kIx,	,
Tylův	Tylův	k2eAgInSc1d1	Tylův
památník	památník	k1gInSc1	památník
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
<g/>
,	,	kIx,	,
Výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
Čáslav	Čáslav	k1gFnSc1	Čáslav
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc4	obraz
a	a	k8xC	a
kresby	kresba	k1gFnPc4	kresba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Felixe	Felix	k1gMnSc2	Felix
Jeneweina	Jenewein	k2eAgNnSc2d1	Jenewein
města	město	k1gNnSc2	město
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
<g/>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
také	také	k9	také
členské	členský	k2eAgFnSc2d1	členská
výstavy	výstava	k1gFnSc2	výstava
J.U.V.	J.U.V.	k1gFnSc2	J.U.V.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
-	-	kIx~	-
Umělci	umělec	k1gMnPc1	umělec
národu	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
Zlínský	zlínský	k2eAgInSc1d1	zlínský
salon	salon	k1gInSc1	salon
<g/>
,	,	kIx,	,
v	v	k7c6	v
Třemošnici	Třemošnice	k1gFnSc6	Třemošnice
<g/>
,	,	kIx,	,
výstava	výstava	k1gFnSc1	výstava
SVUV	SVUV	kA	SVUV
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
a	a	k8xC	a
j.	j.	k?	j.
</s>
</p>
<p>
<s>
===	===	k?	===
Zastoupení	zastoupení	k1gNnPc4	zastoupení
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Felixe	Felix	k1gMnSc2	Felix
Jeneweina	Jenewein	k2eAgNnSc2d1	Jenewein
města	město	k1gNnSc2	město
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Sbírka	sbírka	k1gFnSc1	sbírka
města	město	k1gNnSc2	město
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Okresní	okresní	k2eAgNnSc1d1	okresní
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
Josefa	Josefa	k1gFnSc1	Josefa
Krčila	krčit	k5eAaImAgFnS	krčit
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
Josef	Josef	k1gMnSc1	Josef
Vepřek	Vepřek	k1gInSc1	Vepřek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
březen	březen	k1gInSc1	březen
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matějková	Matějková	k1gFnSc1	Matějková
Eva	Eva	k1gFnSc1	Eva
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
K	k	k7c3	k
nově	nově	k6eAd1	nově
odkrytým	odkrytý	k2eAgFnPc3d1	odkrytá
malbám	malba	k1gFnPc3	malba
v	v	k7c6	v
kutnohorském	kutnohorský	k2eAgInSc6d1	kutnohorský
Hrádku	Hrádok	k1gInSc6	Hrádok
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
památkové	památkový	k2eAgFnSc2d1	památková
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.5	.5	k4	.5
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Katalog	katalog	k1gInSc1	katalog
výstavy	výstava	k1gFnSc2	výstava
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Sejček	Sejček	k?	Sejček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g/>
,	,	kIx,	,
říjen	říjen	k1gInSc1	říjen
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Almanach	almanach	k1gInSc1	almanach
Akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
k	k	k7c3	k
180	[number]	k4	180
<g/>
.	.	kIx.	.
<g/>
výročí	výročí	k1gNnSc3	výročí
založení	založení	k1gNnSc3	založení
(	(	kIx(	(
<g/>
1799	[number]	k4	1799
<g/>
-	-	kIx~	-
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
AVU	AVU	kA	AVU
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Katalog	katalog	k1gInSc1	katalog
výstavy	výstava	k1gFnSc2	výstava
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Sejček	Sejček	k?	Sejček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc4	muzeum
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
-	-	kIx~	-
Tylův	Tylův	k2eAgInSc1d1	Tylův
památník	památník	k1gInSc1	památník
<g/>
,	,	kIx,	,
leden	leden	k1gInSc1	leden
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
slovník	slovník	k1gInSc1	slovník
československých	československý	k2eAgMnPc2d1	československý
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
A-K	A-K	k1gFnSc1	A-K
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
561	[number]	k4	561
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
centrum	centrum	k1gNnSc1	centrum
Chagall	Chagall	k1gInSc1	Chagall
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toman	Toman	k1gMnSc1	Toman
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
slovník	slovník	k1gInSc1	slovník
československých	československý	k2eAgMnPc2d1	československý
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
Dodatky	dodatek	k1gInPc1	dodatek
<g/>
.	.	kIx.	.
</s>
<s>
Str	str	kA	str
<g/>
.	.	kIx.	.
106	[number]	k4	106
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
centrum	centrum	k1gNnSc1	centrum
Chagall	Chagall	k1gInSc1	Chagall
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pavliňák	Pavliňák	k1gMnSc1	Pavliňák
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
<g/>
Signatury	signatura	k1gFnPc1	signatura
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
slovenských	slovenský	k2eAgMnPc2d1	slovenský
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Str	str	kA	str
<g/>
.	.	kIx.	.
96	[number]	k4	96
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
centrum	centrum	k1gNnSc1	centrum
Chagall	Chagall	k1gInSc1	Chagall
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hospes	Hospes	k1gMnSc1	Hospes
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
:	:	kIx,	:
Malíř	malíř	k1gMnSc1	malíř
svého	svůj	k3xOyFgNnSc2	svůj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
1906	[number]	k4	1906
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Obzory	obzor	k1gInPc1	obzor
Kutnohorska	Kutnohorsko	k1gNnSc2	Kutnohorsko
<g/>
.	.	kIx.	.
</s>
<s>
Č.	Č.	kA	Č.
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
<g/>
VII	VII	kA	VII
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úvodní	úvodní	k2eAgInSc1d1	úvodní
list	list	k1gInSc1	list
k	k	k7c3	k
výstavě	výstava	k1gFnSc3	výstava
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgInS	krčit
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
a	a	k8xC	a
kresby	kresba	k1gFnPc1	kresba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1930-1945	[number]	k4	1930-1945
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
Aleš	Aleš	k1gMnSc1	Aleš
Rezler	Rezler	k1gMnSc1	Rezler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Felixe	Felix	k1gMnSc2	Felix
Jeneweina	Jenewein	k2eAgNnSc2d1	Jenewein
města	město	k1gNnSc2	město
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
listopad	listopad	k1gInSc1	listopad
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgMnPc2d1	český
a	a	k8xC	a
slovenských	slovenský	k2eAgMnPc2d1	slovenský
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Kon-Ky	Kon-Ky	k1gInPc1	Kon-Ky
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
252	[number]	k4	252
<g/>
.	.	kIx.	.
<g/>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
centrum	centrum	k1gNnSc1	centrum
Chagall	Chagall	k1gInSc1	Chagall
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
1906-1985	[number]	k4	1906-1985
:	:	kIx,	:
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
kresby	kresba	k1gFnPc1	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Katalog	katalog	k1gInSc1	katalog
výstavy	výstava	k1gFnSc2	výstava
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
Aleš	Aleš	k1gMnSc1	Aleš
Rezler	Rezler	k1gMnSc1	Rezler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Felixe	Felix	k1gMnSc2	Felix
Jeneweina	Jenewein	k2eAgNnSc2d1	Jenewein
města	město	k1gNnSc2	město
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
Vlašský	vlašský	k2eAgInSc1d1	vlašský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
13.4	[number]	k4	13.4
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
18.6	[number]	k4	18.6
<g/>
.2006	.2006	k4	.2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Josef	Josef	k1gMnSc1	Josef
Krčil	krčit	k5eAaImAgMnS	krčit
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
–	–	k?	–
osoba	osoba	k1gFnSc1	osoba
<g/>
:	:	kIx,	:
<g/>
Krčil	krčit	k5eAaImAgMnS	krčit
Josef	Josef	k1gMnSc1	Josef
</s>
</p>
