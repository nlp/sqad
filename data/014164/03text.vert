<s>
Q.E.D.	Q.E.D.	k?
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
matematické	matematický	k2eAgFnSc6d1
zkratce	zkratka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
kvantové	kvantový	k2eAgFnSc6d1
elektrodynamice	elektrodynamika	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
kvantová	kvantový	k2eAgFnSc1d1
elektrodynamika	elektrodynamika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Q.E.D.	Q.E.D.	kA
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
latinského	latinský	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
quod	quod	k3yRnSc1
erat	erat	k5eAaImAgNnS
demonstrandum	demonstrandum	k5eAaPmF
(	(	kIx(
<g/>
česky	česky	k6eAd1
což	což	k3yRnSc1
bylo	být	k5eAaImAgNnS
dokázati	dokázat	k5eAaPmF
či	či	k8xC
což	což	k3yRnSc1
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
dokázáno	dokázán	k2eAgNnSc1d1
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
c.	c.	kA
b.	b.	kA
d.	d.	kA
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původ	původ	k1gInSc4
této	tento	k3xDgFnSc2
zkratky	zkratka	k1gFnSc2
je	být	k5eAaImIp3nS
však	však	k9
v	v	k7c6
řeckém	řecký	k2eAgInSc6d1
ὅ	ὅ	k?
ἔ	ἔ	k?
δ	δ	k?
(	(	kIx(
<g/>
hoper	hoper	k1gMnSc1
edei	ede	k1gFnSc2
deixai	deixa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
překladem	překlad	k1gInSc7
latinský	latinský	k2eAgInSc1d1
termín	termín	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
označení	označení	k1gNnSc1
ukončení	ukončení	k1gNnSc2
matematického	matematický	k2eAgInSc2d1
důkazu	důkaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
řecké	řecký	k2eAgNnSc1d1
označení	označení	k1gNnSc1
používalo	používat	k5eAaImAgNnS
více	hodně	k6eAd2
raných	raný	k2eAgMnPc2d1
řeckých	řecký	k2eAgMnPc2d1
matematiků	matematik	k1gMnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Eukleida	Eukleid	k1gMnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Archiméda	Archimédes	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
novodobé	novodobý	k2eAgFnSc6d1
matematice	matematika	k1gFnSc6
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
současně	současně	k6eAd1
použít	použít	k5eAaPmF
znak	znak	k1gInSc4
∎	∎	k?
<g/>
,	,	kIx,
nazývaný	nazývaný	k2eAgMnSc1d1
prostě	prostě	k9
„	„	k?
<g/>
čtvereček	čtvereček	k1gInSc1
<g/>
“	“	k?
nebo	nebo	k8xC
Halmosův	Halmosův	k2eAgInSc1d1
symbol	symbol	k1gInSc1
na	na	k7c4
počest	počest	k1gFnSc4
Paula	Paul	k1gMnSc4
Halmose	Halmosa	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
jeho	jeho	k3xOp3gNnSc4
používání	používání	k1gNnSc4
zavedl	zavést	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Eukleidovy	Eukleidův	k2eAgInPc1d1
Základy	základ	k1gInPc1
2.5	2.5	k4
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
L.	L.	kA
Heiberg	Heiberg	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
navštíveno	navštíven	k2eAgNnSc4d1
16	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
QED	QED	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
