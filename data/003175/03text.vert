<s>
Trajektorie	trajektorie	k1gFnSc1	trajektorie
(	(	kIx(	(
<g/>
též	též	k9	též
pohybová	pohybový	k2eAgFnSc1d1	pohybová
křivka	křivka	k1gFnSc1	křivka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
geometrická	geometrický	k2eAgFnSc1d1	geometrická
čára	čára	k1gFnSc1	čára
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
hmotný	hmotný	k2eAgInSc1d1	hmotný
bod	bod	k1gInSc1	bod
nebo	nebo	k8xC	nebo
těleso	těleso	k1gNnSc1	těleso
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
opisuje	opisovat	k5eAaImIp3nS	opisovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgFnPc2	všecek
poloh	poloha	k1gFnPc2	poloha
(	(	kIx(	(
<g/>
hmotného	hmotný	k2eAgInSc2d1	hmotný
<g/>
)	)	kIx)	)
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
časových	časový	k2eAgInPc6d1	časový
okamžicích	okamžik	k1gInPc6	okamžik
nacházet	nacházet	k5eAaImF	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Trajektorií	trajektorie	k1gFnSc7	trajektorie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přímka	přímka	k1gFnSc1	přímka
<g/>
,	,	kIx,	,
kružnice	kružnice	k1gFnSc1	kružnice
<g/>
,	,	kIx,	,
elipsa	elipsa	k1gFnSc1	elipsa
či	či	k8xC	či
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
obecná	obecný	k2eAgFnSc1d1	obecná
křivka	křivka	k1gFnSc1	křivka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
trajektorie	trajektorie	k1gFnSc2	trajektorie
dělíme	dělit	k5eAaImIp1nP	dělit
pohyb	pohyb	k1gInSc4	pohyb
na	na	k7c4	na
přímočarý	přímočarý	k2eAgInSc4d1	přímočarý
a	a	k8xC	a
křivočarý	křivočarý	k2eAgInSc4d1	křivočarý
<g/>
.	.	kIx.	.
</s>
<s>
Trajektorii	trajektorie	k1gFnSc4	trajektorie
pohybu	pohyb	k1gInSc2	pohyb
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
pomocí	pomocí	k7c2	pomocí
polohového	polohový	k2eAgInSc2d1	polohový
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
který	který	k3yIgInSc4	který
vyjádříme	vyjádřit	k5eAaPmIp1nP	vyjádřit
jako	jako	k9	jako
funkci	funkce	k1gFnSc4	funkce
času	čas	k1gInSc2	čas
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
t	t	k?	t
<g/>
}	}	kIx)	}
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
trajektorie	trajektorie	k1gFnSc2	trajektorie
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
volbě	volba	k1gFnSc6	volba
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
trajektorie	trajektorie	k1gFnSc2	trajektorie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
hmotný	hmotný	k2eAgInSc1d1	hmotný
bod	bod	k1gInSc1	bod
opíše	opsat	k5eAaPmIp3nS	opsat
za	za	k7c4	za
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
značí	značit	k5eAaImIp3nS	značit
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
s.	s.	k?	s.
Dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
funkcí	funkce	k1gFnSc7	funkce
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
čase	čas	k1gInSc6	čas
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
=	=	kIx~	=
s	s	k7c7	s
(	(	kIx(	(
t	t	k?	t
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
s	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
s	s	k7c7	s
<g/>
(	(	kIx(	(
<g/>
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
např.	např.	kA	např.
bod	bod	k1gInSc4	bod
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
jedoucího	jedoucí	k2eAgNnSc2d1	jedoucí
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Zvolíme	zvolit	k5eAaPmIp1nP	zvolit
<g/>
-li	i	k?	-li
za	za	k7c4	za
vztažnou	vztažný	k2eAgFnSc4d1	vztažná
soustavu	soustava	k1gFnSc4	soustava
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
trajektorií	trajektorie	k1gFnSc7	trajektorie
pohybu	pohyb	k1gInSc2	pohyb
tzv.	tzv.	kA	tzv.
cykloida	cykloida	k1gFnSc1	cykloida
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
zvolíme	zvolit	k5eAaPmIp1nP	zvolit
soustavu	soustava	k1gFnSc4	soustava
spojenou	spojený	k2eAgFnSc4d1	spojená
např.	např.	kA	např.
s	s	k7c7	s
automobilem	automobil	k1gInSc7	automobil
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgNnSc3	který
kolo	kolo	k1gNnSc1	kolo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
bude	být	k5eAaImBp3nS	být
bod	bod	k1gInSc1	bod
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
kola	kolo	k1gNnSc2	kolo
vykonávat	vykonávat	k5eAaImF	vykonávat
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c4	po
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
trajektorií	trajektorie	k1gFnSc7	trajektorie
bude	být	k5eAaImBp3nS	být
kružnice	kružnice	k1gFnSc1	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
Budeme	být	k5eAaImBp1nP	být
<g/>
-li	i	k?	-li
místo	místo	k1gNnSc4	místo
bodu	bod	k1gInSc2	bod
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
sledovat	sledovat	k5eAaImF	sledovat
střed	střed	k1gInSc4	střed
daného	daný	k2eAgNnSc2d1	dané
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
volby	volba	k1gFnSc2	volba
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
spojené	spojený	k2eAgFnSc2d1	spojená
se	se	k3xPyFc4	se
zemí	zem	k1gFnSc7	zem
půjde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pohyb	pohyb	k1gInSc4	pohyb
přímočarý	přímočarý	k2eAgInSc4d1	přímočarý
a	a	k8xC	a
trajektorií	trajektorie	k1gFnPc2	trajektorie
bude	být	k5eAaImBp3nS	být
tedy	tedy	k9	tedy
přímka	přímka	k1gFnSc1	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Mechanický	mechanický	k2eAgInSc1d1	mechanický
pohyb	pohyb	k1gInSc1	pohyb
Kinematika	kinematika	k1gFnSc1	kinematika
Polohový	polohový	k2eAgInSc1d1	polohový
vektor	vektor	k1gInSc1	vektor
</s>
