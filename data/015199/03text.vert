<s>
Generali	Generat	k5eAaPmAgMnP,k5eAaImAgMnP
Ladies	Ladies	k1gMnSc1
Linz	Linz	k1gInSc4
2015	#num#	k4
–	–	k?
ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
</s>
<s>
Generali	Generat	k5eAaPmAgMnP,k5eAaImAgMnP
Ladies	Ladies	k1gMnSc1
Linz	Linz	k1gInSc4
2015	#num#	k4
Vítězka	vítězka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Anastasija	Anastasij	k2eAgFnSc1d1
Pavljučenkovová	Pavljučenkovový	k2eAgFnSc1d1
Finalistka	finalistka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Anna-Lena	Anna-Len	k2eAgFnSc1d1
Friedsamová	Friedsamový	k2eAgFnSc1d1
Výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
Soutěže	soutěž	k1gFnSc2
</s>
<s>
ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
<	<	kIx(
2014	#num#	k4
</s>
<s>
2016	#num#	k4
>	>	kIx)
</s>
<s>
Do	do	k7c2
soutěže	soutěž	k1gFnSc2
ženské	ženský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
na	na	k7c6
tenisovém	tenisový	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
Generali	Generali	k1gMnSc1
Ladies	Ladies	k1gMnSc1
Linz	Linz	k1gInSc4
2015	#num#	k4
nastoupilo	nastoupit	k5eAaPmAgNnS
třicet	třicet	k4xCc1
dva	dva	k4xCgInPc4
hráček	hráčka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obhájkyní	obhájkyně	k1gFnPc2
titulu	titul	k1gInSc2
byla	být	k5eAaImAgFnS
tenistka	tenistka	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Plíšková	plíškový	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zvolila	zvolit	k5eAaPmAgFnS
start	start	k1gInSc4
na	na	k7c6
paralelně	paralelně	k6eAd1
probíhajícím	probíhající	k2eAgInSc6d1
Tianjin	Tianjin	k2eAgMnSc1d1
Open	Open	k1gNnSc4
v	v	k7c6
čínském	čínský	k2eAgInSc6d1
Tchien-ťinu	Tchien-ťin	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýše	vysoce	k6eAd3,k6eAd1
nasazená	nasazený	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
Šafářová	Šafářová	k1gFnSc1
<g/>
,	,	kIx,
vracející	vracející	k2eAgMnSc1d1
se	se	k3xPyFc4
po	po	k7c6
přestávce	přestávka	k1gFnSc6
způsobené	způsobený	k2eAgNnSc1d1
bakteriální	bakteriální	k2eAgFnSc7d1
infekcí	infekce	k1gFnSc7
<g/>
,	,	kIx,
podlehla	podlehnout	k5eAaPmAgFnS
v	v	k7c6
úvodním	úvodní	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
rumunské	rumunský	k2eAgFnPc1d1
hráčce	hráčka	k1gFnSc3
Andreee	Andreee	k1gFnSc7
Mituové	Mituové	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Vítězkou	vítězka	k1gFnSc7
dvouhry	dvouhra	k1gFnSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
sedmá	sedmý	k4xOgFnSc1
nasazená	nasazený	k2eAgFnSc1d1
Ruska	Ruska	k1gFnSc1
Anastasija	Anastasija	k1gFnSc1
Pavljučenkovová	Pavljučenkovová	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
ve	v	k7c6
finále	finála	k1gFnSc6
zdolala	zdolat	k5eAaPmAgFnS
německou	německý	k2eAgFnSc4d1
tenistku	tenistka	k1gFnSc4
Annu-Lenu	Annu-Len	k2eAgFnSc4d1
Friedsamovou	Friedsamový	k2eAgFnSc4d1
po	po	k7c6
dvousetovém	dvousetový	k2eAgInSc6d1
průběhu	průběh	k1gInSc6
6	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
a	a	k8xC
6	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
probíhající	probíhající	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
si	se	k3xPyFc3
připsala	připsat	k5eAaPmAgFnS
premiérové	premiérový	k2eAgNnSc4d1
turnajové	turnajový	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
představovalo	představovat	k5eAaImAgNnS
osmý	osmý	k4xOgInSc4
singlový	singlový	k2eAgInSc4d1
titul	titul	k1gInSc4
na	na	k7c6
okruhu	okruh	k1gInSc6
WTA	WTA	kA
Tour	Tour	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhrála	vyhrát	k5eAaPmAgFnS
tak	tak	k9
třetí	třetí	k4xOgInSc4
halový	halový	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
když	když	k8xS
naposledy	naposledy	k6eAd1
předtím	předtím	k6eAd1
nastoupila	nastoupit	k5eAaPmAgFnS
do	do	k7c2
pařížského	pařížský	k2eAgInSc2d1
Open	Open	k1gInSc1
GDF	GDF	kA
Suez	Suez	k1gInSc4
2014	#num#	k4
a	a	k8xC
moskevského	moskevský	k2eAgInSc2d1
Kremlin	Kremlin	k2eAgInSc1d1
Cupu	cup	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
oba	dva	k4xCgMnPc4
ovládla	ovládnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hale	hala	k1gFnSc6
navýšila	navýšit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
neporazitelnost	neporazitelnost	k1gFnSc4
na	na	k7c4
patnáct	patnáct	k4xCc4
zápasů	zápas	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nasazení	nasazení	k1gNnSc1
hráček	hráčka	k1gFnPc2
</s>
<s>
Čtyři	čtyři	k4xCgMnPc1
nejvýše	nejvýše	k6eAd1,k6eAd3
nasazené	nasazený	k2eAgFnPc1d1
hráčky	hráčka	k1gFnPc1
měly	mít	k5eAaImAgFnP
volný	volný	k2eAgInSc4d1
los	los	k1gInSc4
do	do	k7c2
druhého	druhý	k4xOgNnSc2
kola	kolo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Lucie	Lucie	k1gFnSc1
Šafářová	Šafářová	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Caroline	Carolin	k1gInSc5
Wozniacká	Wozniacký	k2eAgFnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Roberta	Roberta	k1gFnSc1
Vinciová	Vinciový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Andrea	Andrea	k1gFnSc1
Petkovicová	Petkovicový	k2eAgFnSc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Schmiedlová	Schmiedlový	k2eAgFnSc1d1
(	(	kIx(
<g/>
odstoupila	odstoupit	k5eAaPmAgFnS
<g/>
)	)	kIx)
</s>
<s>
Camila	Camila	k1gFnSc1
Giorgiová	Giorgiový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Anastasija	Anastasij	k2eAgFnSc1d1
Pavljučenkovová	Pavljučenkovová	k1gFnSc1
(	(	kIx(
<g/>
vítězka	vítězka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Barbora	Barbora	k1gFnSc1
Strýcová	Strýcová	k1gFnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Pavouk	pavouk	k1gMnSc1
</s>
<s>
Legenda	legenda	k1gFnSc1
</s>
<s>
Q	Q	kA
–	–	k?
kvalifikant	kvalifikant	k1gMnSc1
</s>
<s>
WC	WC	kA
–	–	k?
divoká	divoký	k2eAgFnSc1d1
karta	karta	k1gFnSc1
</s>
<s>
LL	LL	kA
–	–	k?
šťastný	šťastný	k2eAgMnSc1d1
poražený	poražený	k2eAgMnSc1d1
</s>
<s>
Alt	Alt	kA
–	–	k?
náhradník	náhradník	k1gMnSc1
</s>
<s>
SE	s	k7c7
–	–	k?
zvláštní	zvláštní	k2eAgFnSc1d1
výjimka	výjimka	k1gFnSc1
</s>
<s>
PR	pr	k0
–	–	k?
žebříčková	žebříčkový	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
</s>
<s>
w	w	k?
<g/>
/	/	kIx~
<g/>
o	o	k7c6
–	–	k?
bez	bez	k7c2
boje	boj	k1gInSc2
</s>
<s>
r	r	kA
–	–	k?
skreč	skreč	k1gInSc1
</s>
<s>
d	d	k?
–	–	k?
diskvalifikace	diskvalifikace	k1gFnSc1
</s>
<s>
Finálová	finálový	k2eAgFnSc1d1
fáze	fáze	k1gFnSc1
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
Anna-Lena	Anna-Len	k2eAgFnSc1d1
Friedsamová	Friedsamový	k2eAgFnSc1d1
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Johanna	Johanen	k2eAgFnSc1d1
Larssonová	Larssonová	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
Anna-Lena	Anna-Len	k2eAgFnSc1d1
Friedsamová	Friedsamový	k2eAgFnSc1d1
</s>
<s>
4	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Anastasija	Anastasij	k2eAgFnSc1d1
Pavljučenkovová	Pavljučenkovová	k1gFnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Anastasija	Anastasij	k2eAgFnSc1d1
Pavljučenkovová	Pavljučenkovová	k1gFnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Kirsten	Kirsten	k2eAgInSc1d1
Flipkensová	Flipkensový	k2eAgFnSc5d1
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Horní	horní	k2eAgFnSc1d1
polovina	polovina	k1gFnSc1
</s>
<s>
První	první	k4xOgNnSc1
kolo	kolo	k1gNnSc1
</s>
<s>
Druhé	druhý	k4xOgNnSc1
kolo	kolo	k1gNnSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
WC	WC	kA
</s>
<s>
L	L	kA
Šafářová	Šafářová	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
A	a	k9
Mitu	Mita	k1gFnSc4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
A	a	k9
Mitu	Mita	k1gFnSc4
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
M	M	kA
Rybáriková	Rybárikový	k2eAgFnSc1d1
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
A-L	A-L	k?
Friedsam	Friedsam	k1gInSc1
</s>
<s>
7	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
A-L	A-L	k?
Friedsam	Friedsam	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
A-L	A-L	k?
Friedsam	Friedsam	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
M	M	kA
Gasparjan	Gasparjan	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
M	M	kA
Gasparjan	Gasparjan	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
C	C	kA
Witthöft	Witthöft	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
M	M	kA
Gasparjan	Gasparjan	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
J	J	kA
Görges	Görges	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
C	C	kA
Giorgi	Giorgi	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
C	C	kA
Giorgi	Giorgi	k1gNnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
A-L	A-L	k?
Friedsam	Friedsam	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
WC	WC	kA
</s>
<s>
A	a	k9
Petkovic	Petkovice	k1gFnPc2
</s>
<s>
6	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
J	J	kA
Larsson	Larsson	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
J	J	kA
Larsson	Larsson	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
J	J	kA
Larsson	Larsson	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
77	#num#	k4
</s>
<s>
L	L	kA
Hradecká	Hradecká	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
A	a	k9
Van	van	k1gInSc1
Uytvanck	Uytvancka	k1gFnPc2
</s>
<s>
4	#num#	k4
</s>
<s>
62	#num#	k4
</s>
<s>
A	a	k9
Van	van	k1gInSc1
Uytvanck	Uytvancka	k1gFnPc2
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
J	J	kA
Larsson	Larsson	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
78	#num#	k4
</s>
<s>
M	M	kA
Brengle	Brengle	k1gFnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
M	M	kA
Brengle	Brengle	k1gFnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
66	#num#	k4
</s>
<s>
M	M	kA
Doi	Doi	k1gFnSc1
</s>
<s>
0	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
M	M	kA
Brengle	Brengle	k1gFnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
A	a	k9
Beck	Beck	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
LL	LL	kA
</s>
<s>
J	J	kA
Konta	konto	k1gNnSc2
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
LL	LL	kA
</s>
<s>
J	J	kA
Konta	konto	k1gNnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
polovina	polovina	k1gFnSc1
</s>
<s>
První	první	k4xOgNnSc1
kolo	kolo	k1gNnSc1
</s>
<s>
Druhé	druhý	k4xOgNnSc1
kolo	kolo	k1gNnSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
A	a	k9
Pavljučenkova	Pavljučenkův	k2eAgFnSc1d1
</s>
<s>
77	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
K	k	k7c3
Siniaková	Siniakový	k2eAgFnSc5d1
</s>
<s>
64	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
A	a	k9
Pavljučenkova	Pavljučenkův	k2eAgFnSc1d1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
S	s	k7c7
Vögele	Vögel	k1gInPc5
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
S	s	k7c7
Vögele	Vögel	k1gInSc2
</s>
<s>
4	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
WC	WC	kA
</s>
<s>
T	T	kA
Paszek	Paszek	k1gInSc1
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
A	a	k9
Pavljučenkova	Pavljučenkův	k2eAgFnSc1d1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
M	M	kA
Barthel	Barthel	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
A	a	k9
Krunić	Krunić	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
K	k	k7c3
Bertens	Bertens	k1gInSc1
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
M	M	kA
Barthel	Barthel	k1gMnSc1
</s>
<s>
66	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
A	a	k9
Krunić	Krunić	k1gFnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
A	a	k9
Krunić	Krunić	k1gFnSc1
</s>
<s>
78	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
R	R	kA
Vinci	Vinca	k1gMnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
77	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
A	a	k9
Pavljučenkova	Pavljučenkův	k2eAgFnSc1d1
</s>
<s>
6	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
B	B	kA
Strýcová	Strýcová	k1gFnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
65	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
K	k	k7c3
Flipkens	Flipkens	k1gInSc1
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
WC	WC	kA
</s>
<s>
B	B	kA
Haas	Haas	k1gInSc1
</s>
<s>
0	#num#	k4
</s>
<s>
77	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
B	B	kA
Strýcová	Strýcová	k1gFnSc1
</s>
<s>
60	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
K	k	k7c3
Koukalová	Koukalová	k1gFnSc5
</s>
<s>
2	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
D	D	kA
Allertová	Allertová	k1gFnSc1
</s>
<s>
77	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
D	D	kA
Allertová	Allertová	k1gFnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
D	D	kA
Allertová	Allertová	k1gFnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
K	k	k7c3
Flipkens	Flipkens	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
77	#num#	k4
</s>
<s>
K	k	k7c3
Flipkens	Flipkens	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
V	v	k7c6
Lepčenko	Lepčenka	k1gFnSc5
</s>
<s>
3	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
K	k	k7c3
Flipkens	Flipkens	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
M	M	kA
Lučić-Baroni	Lučić-Baron	k1gMnPc5
</s>
<s>
6	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
C	C	kA
Wozniacki	Wozniacki	k1gNnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
C	C	kA
Wozniacki	Wozniacki	k1gNnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
77	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
2015	#num#	k4
Generali	Generali	k1gMnSc1
Ladies	Ladies	k1gMnSc1
Linz	Linz	k1gMnSc1
–	–	k?
Singles	Singles	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
ZABLOUDI	zabloudit	k5eAaPmRp2nS
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linec	Linec	k1gInSc1
<g/>
:	:	kIx,
Pavljučenkovová	Pavljučenkovová	k1gFnSc1
ovládla	ovládnout	k5eAaPmAgFnS
třetí	třetí	k4xOgInSc4
halový	halový	k2eAgInSc4d1
turnaj	turnaj	k1gInSc4
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
TenisPortal	TenisPortal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-10-18	2015-10-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
na	na	k7c6
Generali	Generali	k1gFnSc6
Ladies	Ladies	k1gMnSc1
Linz	Linz	k1gMnSc1
2015	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
WTA	WTA	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Kvalifikace	kvalifikace	k1gFnSc1
ženské	ženský	k2eAgFnSc2d1
dvouhry	dvouhra	k1gFnSc2
na	na	k7c6
Generali	Generali	k1gFnSc6
Ladies	Ladies	k1gMnSc1
Linz	Linz	k1gMnSc1
2015	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
WTA	WTA	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Tenis	tenis	k1gInSc1
</s>
