<s desamb="1">
Obhájkyní	obhájkyně	k1gFnPc2
titulu	titul	k1gInSc2
byla	být	k5eAaImAgFnS
tenistka	tenistka	k1gFnSc1
Karolína	Karolína	k1gFnSc1
Plíšková	plíškový	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zvolila	zvolit	k5eAaPmAgFnS
start	start	k1gInSc4
na	na	k7c6
paralelně	paralelně	k6eAd1
probíhajícím	probíhající	k2eAgInSc6d1
Tianjin	Tianjin	k2eAgMnSc1d1
Open	Open	k1gNnSc4
v	v	k7c6
čínském	čínský	k2eAgInSc6d1
Tchien-ťinu	Tchien-ťin	k1gInSc6
<g/>
.	.	kIx.
</s>