<s>
Mensa	mensa	k1gFnSc1	mensa
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Hanou	Hana	k1gFnSc7	Hana
Drábkovou	Drábková	k1gFnSc7	Drábková
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
učinila	učinit	k5eAaPmAgFnS	učinit
první	první	k4xOgInPc4	první
pokusy	pokus	k1gInPc4	pokus
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
registrována	registrován	k2eAgFnSc1d1	registrována
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
