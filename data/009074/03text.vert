<p>
<s>
Čtečka	čtečka	k1gFnSc1	čtečka
elektronických	elektronický	k2eAgFnPc2d1	elektronická
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
též	též	k9	též
čtečka	čtečka	k1gFnSc1	čtečka
e-knih	eniha	k1gFnPc2	e-kniha
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
e-book	eook	k1gInSc1	e-book
reader	readra	k1gFnPc2	readra
nebo	nebo	k8xC	nebo
e-reader	eeadra	k1gFnPc2	e-readra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc4d1	určený
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
elektronických	elektronický	k2eAgFnPc2d1	elektronická
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
e-knih	enih	k1gInSc1	e-knih
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
elektronických	elektronický	k2eAgNnPc2d1	elektronické
periodik	periodikum	k1gNnPc2	periodikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtečky	čtečka	k1gFnPc1	čtečka
elektronických	elektronický	k2eAgFnPc2d1	elektronická
knih	kniha	k1gFnPc2	kniha
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
jako	jako	k9	jako
alternativa	alternativa	k1gFnSc1	alternativa
ke	k	k7c3	k
klasickým	klasický	k2eAgFnPc3d1	klasická
tištěným	tištěný	k2eAgFnPc3d1	tištěná
knihám	kniha	k1gFnPc3	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
mají	mít	k5eAaImIp3nP	mít
displej	displej	k1gInSc4	displej
založeny	založen	k2eAgMnPc4d1	založen
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
elektronického	elektronický	k2eAgInSc2d1	elektronický
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
svým	svůj	k3xOyFgNnSc7	svůj
zobrazením	zobrazení	k1gNnSc7	zobrazení
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
připomínat	připomínat	k5eAaImF	připomínat
právě	právě	k9	právě
tištěnou	tištěný	k2eAgFnSc4d1	tištěná
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trhu	trh	k1gInSc6	trh
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
čtečky	čtečka	k1gFnPc1	čtečka
s	s	k7c7	s
konvenčním	konvenční	k2eAgNnSc7d1	konvenční
LCD	LCD	kA	LCD
displejem	displej	k1gInSc7	displej
a	a	k8xC	a
přídatnými	přídatný	k2eAgFnPc7d1	přídatná
funkcemi	funkce	k1gFnPc7	funkce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
multimediální	multimediální	k2eAgInSc1d1	multimediální
přehrávač	přehrávač	k1gInSc1	přehrávač
<g/>
,	,	kIx,	,
webový	webový	k2eAgMnSc1d1	webový
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Pierre	Pierr	k1gInSc5	Pierr
Schweitzer	Schweitzer	k1gInSc4	Schweitzer
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
zobrazování	zobrazování	k1gNnSc4	zobrazování
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
@	@	kIx~	@
<g/>
folio	folio	k1gNnSc4	folio
project	project	k5eAaPmF	project
<g/>
.	.	kIx.	.
</s>
<s>
Zařízení	zařízení	k1gNnSc1	zařízení
bylo	být	k5eAaImAgNnS	být
koncipováno	koncipovat	k5eAaBmNgNnS	koncipovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
stahovat	stahovat	k5eAaImF	stahovat
a	a	k8xC	a
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
libovolný	libovolný	k2eAgInSc4d1	libovolný
text	text	k1gInSc4	text
nebo	nebo	k8xC	nebo
ilustrace	ilustrace	k1gFnPc4	ilustrace
z	z	k7c2	z
webu	web	k1gInSc2	web
nebo	nebo	k8xC	nebo
pevného	pevný	k2eAgInSc2d1	pevný
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
libovolném	libovolný	k2eAgInSc6d1	libovolný
formátu	formát	k1gInSc6	formát
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnPc4	technologie
@	@	kIx~	@
<g/>
folio	folio	k1gNnSc4	folio
byla	být	k5eAaImAgFnS	být
nová	nový	k2eAgFnSc1d1	nová
a	a	k8xC	a
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2002	[number]	k4	2002
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
francouzský	francouzský	k2eAgInSc1d1	francouzský
startup	startup	k1gInSc1	startup
iCodex	iCodex	k1gInSc1	iCodex
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
a	a	k8xC	a
propagovat	propagovat	k5eAaImF	propagovat
@	@	kIx~	@
<g/>
folio	folio	k1gNnSc4	folio
project	project	k5eAaPmF	project
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ovšem	ovšem	k9	ovšem
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
prototypu	prototyp	k1gInSc2	prototyp
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
jazykové	jazykový	k2eAgFnSc3d1	jazyková
bariéře	bariéra	k1gFnSc3	bariéra
–	–	k?	–
většina	většina	k1gFnSc1	většina
článků	článek	k1gInPc2	článek
byla	být	k5eAaImAgFnS	být
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
první	první	k4xOgFnPc1	první
čtečky	čtečka	k1gFnPc1	čtečka
elektronických	elektronický	k2eAgFnPc2d1	elektronická
knih	kniha	k1gFnPc2	kniha
Rocket	Rocketa	k1gFnPc2	Rocketa
ebook	ebook	k1gInSc1	ebook
a	a	k8xC	a
SoftBook	SoftBook	k1gInSc1	SoftBook
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnSc1	první
čtečka	čtečka	k1gFnSc1	čtečka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
začala	začít	k5eAaPmAgFnS	začít
prodávat	prodávat	k5eAaImF	prodávat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Cybook	Cybook	k1gInSc4	Cybook
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
vyvinuta	vyvinut	k2eAgFnSc1d1	vyvinuta
francouzská	francouzský	k2eAgFnSc1d1	francouzská
společnost	společnost	k1gFnSc1	společnost
Cytale	Cytala	k1gFnSc3	Cytala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Sony	Sony	kA	Sony
představila	představit	k5eAaPmAgFnS	představit
první	první	k4xOgFnSc1	první
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
Sony	Sony	kA	Sony
Readerů	Reader	k1gInPc2	Reader
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
firma	firma	k1gFnSc1	firma
Amazon	amazona	k1gFnPc2	amazona
začala	začít	k5eAaPmAgFnS	začít
prodávat	prodávat	k5eAaImF	prodávat
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
čtečky	čtečka	k1gFnSc2	čtečka
Kindle	Kindl	k1gMnSc5	Kindl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
čtečka	čtečka	k1gFnSc1	čtečka
e-knih	eniha	k1gFnPc2	e-kniha
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
elektronické	elektronický	k2eAgNnSc4d1	elektronické
zařízení	zařízení	k1gNnSc4	zařízení
umožňující	umožňující	k2eAgNnSc4d1	umožňující
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
obrazovce	obrazovka	k1gFnSc6	obrazovka
zobrazit	zobrazit	k5eAaPmF	zobrazit
text	text	k1gInSc4	text
uložený	uložený	k2eAgInSc4d1	uložený
ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
souborovém	souborový	k2eAgInSc6d1	souborový
formátu	formát	k1gInSc6	formát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
EPUB	EPUB	kA	EPUB
<g/>
,	,	kIx,	,
PDF	PDF	kA	PDF
<g/>
,	,	kIx,	,
MOBI	MOBI	kA	MOBI
nebo	nebo	k8xC	nebo
AZW	AZW	kA	AZW
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zařízení	zařízení	k1gNnSc1	zařízení
specializované	specializovaný	k2eAgNnSc1d1	specializované
jen	jen	k9	jen
na	na	k7c6	na
čtení	čtení	k1gNnSc6	čtení
literatury	literatura	k1gFnSc2	literatura
může	moct	k5eAaImIp3nS	moct
disponovat	disponovat	k5eAaBmF	disponovat
přednostmi	přednost	k1gFnPc7	přednost
jako	jako	k8xC	jako
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
odběr	odběr	k1gInSc1	odběr
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
výdrž	výdrž	k1gFnSc1	výdrž
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnSc4d2	lepší
čitelnost	čitelnost	k1gFnSc4	čitelnost
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
pod	pod	k7c7	pod
přímým	přímý	k2eAgNnSc7d1	přímé
sluncem	slunce	k1gNnSc7	slunce
i	i	k8xC	i
denním	denní	k2eAgNnSc7d1	denní
světlem	světlo	k1gNnSc7	světlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ergonomie	ergonomie	k1gFnSc1	ergonomie
pro	pro	k7c4	pro
pohodlnější	pohodlný	k2eAgNnSc4d2	pohodlnější
čtení	čtení	k1gNnSc4	čtení
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Součástí	součást	k1gFnSc7	součást
čtečky	čtečka	k1gFnSc2	čtečka
e-knih	enih	k1gInSc1	e-knih
je	být	k5eAaImIp3nS	být
paměť	paměť	k1gFnSc4	paměť
pro	pro	k7c4	pro
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uchovávat	uchovávat	k5eAaImF	uchovávat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
tisíce	tisíc	k4xCgInPc1	tisíc
<g/>
)	)	kIx)	)
e-knih	eniha	k1gFnPc2	e-kniha
<g/>
.	.	kIx.	.
</s>
<s>
Čtečka	čtečka	k1gFnSc1	čtečka
může	moct	k5eAaImIp3nS	moct
nabízet	nabízet	k5eAaImF	nabízet
pomocné	pomocný	k2eAgFnPc4d1	pomocná
funkce	funkce	k1gFnPc4	funkce
jako	jako	k8xC	jako
např.	např.	kA	např.
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
v	v	k7c6	v
textu	text	k1gInSc6	text
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
definice	definice	k1gFnSc2	definice
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
vybraného	vybraný	k2eAgInSc2d1	vybraný
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
záložky	záložka	k1gFnSc2	záložka
nebo	nebo	k8xC	nebo
poznámky	poznámka	k1gFnPc4	poznámka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
náročnější	náročný	k2eAgFnPc4d2	náročnější
funkcionality	funkcionalita	k1gFnPc4	funkcionalita
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
bezdrátové	bezdrátový	k2eAgNnSc4d1	bezdrátové
připojení	připojení	k1gNnSc4	připojení
<g/>
,	,	kIx,	,
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
tak	tak	k9	tak
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
internetovému	internetový	k2eAgInSc3d1	internetový
obchodu	obchod	k1gInSc3	obchod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgInPc4d1	možný
e-knihy	enih	k1gInPc4	e-knih
zakoupit	zakoupit	k5eAaPmF	zakoupit
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
čtečky	čtečka	k1gFnSc2	čtečka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Displeje	displej	k1gInPc1	displej
bývají	bývat	k5eAaImIp3nP	bývat
řešeny	řešit	k5eAaImNgInP	řešit
i	i	k9	i
jako	jako	k9	jako
dotykové	dotykový	k2eAgNnSc1d1	dotykové
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
při	při	k7c6	při
umazání	umazání	k1gNnSc6	umazání
nevadí	vadit	k5eNaImIp3nS	vadit
u	u	k7c2	u
LCD	LCD	kA	LCD
-	-	kIx~	-
špínu	špína	k1gMnSc4	špína
prosvítí	prosvítit	k5eAaPmIp3nS	prosvítit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vadí	vadit	k5eAaImIp3nS	vadit
čitelnosti	čitelnost	k1gFnSc3	čitelnost
u	u	k7c2	u
e-inkoustu	enkoust	k1gInSc2	e-inkoust
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
snazší	snadný	k2eAgNnSc4d2	snazší
ovládání	ovládání	k1gNnSc4	ovládání
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
větší	veliký	k2eAgFnSc4d2	veliký
plochu	plocha	k1gFnSc4	plocha
displeje	displej	k1gInSc2	displej
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
minimalizace	minimalizace	k1gFnSc2	minimalizace
počtu	počet	k1gInSc2	počet
ovládacích	ovládací	k2eAgNnPc2d1	ovládací
tlačítek	tlačítko	k1gNnPc2	tlačítko
mimo	mimo	k7c4	mimo
displej	displej	k1gInSc4	displej
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čtečka	čtečka	k1gFnSc1	čtečka
elektronických	elektronický	k2eAgFnPc2d1	elektronická
knih	kniha	k1gFnPc2	kniha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Postup	postup	k1gInSc1	postup
pro	pro	k7c4	pro
vybírání	vybírání	k1gNnSc4	vybírání
čtečky	čtečka	k1gFnSc2	čtečka
elektronických	elektronický	k2eAgFnPc2d1	elektronická
knih	kniha	k1gFnPc2	kniha
</s>
</p>
