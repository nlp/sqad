<s>
Čtečka	čtečka	k1gFnSc1	čtečka
elektronických	elektronický	k2eAgFnPc2d1	elektronická
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
též	též	k9	též
čtečka	čtečka	k1gFnSc1	čtečka
e-knih	eniha	k1gFnPc2	e-kniha
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
e-book	eook	k1gInSc1	e-book
reader	readra	k1gFnPc2	readra
nebo	nebo	k8xC	nebo
e-reader	eeadra	k1gFnPc2	e-readra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc4d1	určený
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
elektronických	elektronický	k2eAgFnPc2d1	elektronická
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
e-knih	enih	k1gInSc1	e-knih
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
elektronických	elektronický	k2eAgNnPc2d1	elektronické
periodik	periodikum	k1gNnPc2	periodikum
<g/>
.	.	kIx.	.
</s>
