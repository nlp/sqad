<p>
<s>
Skřivan	Skřivan	k1gMnSc1	Skřivan
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Lullula	Lullula	k1gMnSc1	Lullula
arborea	arborea	k1gMnSc1	arborea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
pěvce	pěvec	k1gMnSc2	pěvec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
skřivanovitých	skřivanovitý	k2eAgMnPc2d1	skřivanovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velikosti	velikost	k1gFnPc1	velikost
vrabce	vrabec	k1gMnSc2	vrabec
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
13,5	[number]	k4	13,5
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Shora	shora	k6eAd1	shora
je	být	k5eAaImIp3nS	být
hnědý	hnědý	k2eAgInSc1d1	hnědý
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
skvrnitý	skvrnitý	k2eAgMnSc1d1	skvrnitý
<g/>
,	,	kIx,	,
spodinu	spodina	k1gFnSc4	spodina
má	mít	k5eAaImIp3nS	mít
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
,	,	kIx,	,
končetiny	končetina	k1gFnPc1	končetina
žlutavě	žlutavě	k6eAd1	žlutavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
a	a	k8xC	a
slabý	slabý	k2eAgInSc1d1	slabý
zobák	zobák	k1gInSc1	zobák
šedohnědý	šedohnědý	k2eAgInSc1d1	šedohnědý
se	s	k7c7	s
světlejším	světlý	k2eAgInSc7d2	světlejší
kořenem	kořen	k1gInSc7	kořen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
skřivanu	skřivan	k1gMnSc3	skřivan
polnímu	polní	k2eAgMnSc3d1	polní
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
následujícími	následující	k2eAgInPc7d1	následující
znaky	znak	k1gInPc7	znak
<g/>
:	:	kIx,	:
ocas	ocas	k1gInSc1	ocas
má	mít	k5eAaImIp3nS	mít
bílou	bílý	k2eAgFnSc4d1	bílá
špičku	špička	k1gFnSc4	špička
(	(	kIx(	(
<g/>
ne	ne	k9	ne
okraje	okraj	k1gInPc1	okraj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgInSc1d1	zadní
okraj	okraj	k1gInSc1	okraj
křídla	křídlo	k1gNnSc2	křídlo
není	být	k5eNaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
kresba	kresba	k1gFnSc1	kresba
křídla	křídlo	k1gNnSc2	křídlo
je	být	k5eAaImIp3nS	být
kontrastní	kontrastní	k2eAgMnSc1d1	kontrastní
(	(	kIx(	(
<g/>
tmavé	tmavý	k2eAgFnPc1d1	tmavá
ruční	ruční	k2eAgFnPc1d1	ruční
krovky	krovka	k1gFnPc1	krovka
se	s	k7c7	s
světlými	světlý	k2eAgFnPc7d1	světlá
špičkami	špička	k1gFnPc7	špička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
široký	široký	k2eAgInSc1d1	široký
krémový	krémový	k2eAgInSc1d1	krémový
nadoční	nadoční	k2eAgInSc1d1	nadoční
proužek	proužek	k1gInSc1	proužek
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
týla	týl	k1gInSc2	týl
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
<g/>
Zpívá	zpívat	k5eAaImIp3nS	zpívat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
letu	let	k1gInSc6	let
(	(	kIx(	(
<g/>
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
nebo	nebo	k8xC	nebo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
tvoří	tvořit	k5eAaImIp3nS	tvořit
flétnovité	flétnovitý	k2eAgFnSc2d1	flétnovitý
lylylylydadadyldadyldyldyldl	lylylylydadadyldadyldyldyldnout	k5eAaPmAgMnS	lylylylydadadyldadyldyldyldnout
<g/>
,	,	kIx,	,
přednášené	přednášený	k2eAgFnPc1d1	přednášená
nejintenzivněji	intenzivně	k6eAd3	intenzivně
časně	časně	k6eAd1	časně
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běžně	běžně	k6eAd1	běžně
i	i	k9	i
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mnoha	mnoho	k4c2	mnoho
milovníků	milovník	k1gMnPc2	milovník
ptactva	ptactvo	k1gNnSc2	ptactvo
<g/>
,	,	kIx,	,
terénních	terénní	k2eAgMnPc2d1	terénní
ornitologů	ornitolog	k1gMnPc2	ornitolog
i	i	k8xC	i
starých	starý	k2eAgMnPc2d1	starý
ptáčníků	ptáčník	k1gMnPc2	ptáčník
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
náš	náš	k3xOp1gMnSc1	náš
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
ptačí	ptačí	k2eAgMnSc1d1	ptačí
zpěvák	zpěvák	k1gMnSc1	zpěvák
(	(	kIx(	(
<g/>
jakkoliv	jakkoliv	k6eAd1	jakkoliv
je	být	k5eAaImIp3nS	být
krása	krása	k1gFnSc1	krása
zpěvu	zpěv	k1gInSc2	zpěv
velmi	velmi	k6eAd1	velmi
subjektivní	subjektivní	k2eAgFnSc1d1	subjektivní
kategorie	kategorie	k1gFnSc1	kategorie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
zpěvu	zpěv	k1gInSc6	zpěv
lidé	člověk	k1gMnPc1	člověk
cítí	cítit	k5eAaImIp3nP	cítit
lyričnost	lyričnost	k1gFnSc4	lyričnost
a	a	k8xC	a
melancholii	melancholie	k1gFnSc4	melancholie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
ani	ani	k9	ani
téměř	téměř	k6eAd1	téměř
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
slavičí	slavičí	k2eAgInSc1d1	slavičí
zpěv	zpěv	k1gInSc1	zpěv
neobsáhne	obsáhnout	k5eNaPmIp3nS	obsáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
i	i	k9	i
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
mohli	moct	k5eAaImAgMnP	moct
hlasovat	hlasovat	k5eAaImF	hlasovat
všichni	všechen	k3xTgMnPc1	všechen
posluchači	posluchač	k1gMnPc1	posluchač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
roztroušeně	roztroušeně	k6eAd1	roztroušeně
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
početnější	početní	k2eAgMnSc1d2	početnější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
polovině	polovina	k1gFnSc6	polovina
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
po	po	k7c6	po
horní	horní	k2eAgFnSc6d1	horní
hranici	hranice	k1gFnSc6	hranice
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
silném	silný	k2eAgInSc6d1	silný
poklesu	pokles	k1gInSc6	pokles
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dochází	docházet	k5eAaImIp3nS	docházet
místy	místy	k6eAd1	místy
k	k	k7c3	k
opětovnému	opětovný	k2eAgInSc3d1	opětovný
růstu	růst	k1gInSc3	růst
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
početnost	početnost	k1gFnSc1	početnost
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
na	na	k7c4	na
600	[number]	k4	600
<g/>
–	–	k?	–
<g/>
1100	[number]	k4	1100
párů	pár	k1gInPc2	pár
<g/>
.	.	kIx.	.
<g/>
Zvláště	zvláště	k6eAd1	zvláště
chráněný	chráněný	k2eAgInSc1d1	chráněný
jako	jako	k8xS	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
<g/>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
světlých	světlý	k2eAgInPc6d1	světlý
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
především	především	k9	především
borových	borový	k2eAgFnPc2d1	Borová
na	na	k7c6	na
písčitých	písčitý	k2eAgFnPc6d1	písčitá
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
na	na	k7c6	na
vřesovištích	vřesoviště	k1gNnPc6	vřesoviště
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starých	starý	k2eAgInPc6d1	starý
sadech	sad	k1gInPc6	sad
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Potrava	potrava	k1gFnSc1	potrava
je	být	k5eAaImIp3nS	být
smíšená	smíšený	k2eAgFnSc1d1	smíšená
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
složení	složení	k1gNnSc4	složení
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
zjara	zjara	k6eAd1	zjara
převažují	převažovat	k5eAaImIp3nP	převažovat
zelené	zelený	k2eAgFnPc1d1	zelená
části	část	k1gFnPc1	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
drobní	drobný	k2eAgMnPc1d1	drobný
bezobratlí	bezobratlí	k1gMnPc1	bezobratlí
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
semena	semeno	k1gNnSc2	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
hledá	hledat	k5eAaImIp3nS	hledat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
na	na	k7c6	na
nízkých	nízký	k2eAgFnPc6d1	nízká
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
ve	v	k7c6	v
svrchní	svrchní	k2eAgFnSc6d1	svrchní
vrstvě	vrstva	k1gFnSc6	vrstva
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
většinou	většina	k1gFnSc7	většina
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
3	[number]	k4	3
<g/>
×	×	k?	×
ročně	ročně	k6eAd1	ročně
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
až	až	k8xS	až
červenci	červenec	k1gInSc6	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
skryté	skrytý	k2eAgFnPc4d1	skrytá
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Snůška	snůška	k1gFnSc1	snůška
čítá	čítat	k5eAaImIp3nS	čítat
4	[number]	k4	4
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
špinavě	špinavě	k6eAd1	špinavě
bílá	bílý	k2eAgNnPc1d1	bílé
<g/>
,	,	kIx,	,
hustě	hustě	k6eAd1	hustě
jemně	jemně	k6eAd1	jemně
hnědě	hnědě	k6eAd1	hnědě
<g/>
,	,	kIx,	,
šedohnědě	šedohnědě	k6eAd1	šedohnědě
nebo	nebo	k8xC	nebo
šedočerveně	šedočerveně	k6eAd1	šedočerveně
skvrnitá	skvrnitý	k2eAgNnPc1d1	skvrnité
vejce	vejce	k1gNnPc1	vejce
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
21,2	[number]	k4	21,2
x	x	k?	x
15,7	[number]	k4	15,7
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
trvá	trvat	k5eAaImIp3nS	trvat
13-15	[number]	k4	13-15
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedí	sedit	k5eAaImIp3nP	sedit
samotná	samotný	k2eAgFnSc1d1	samotná
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
krmí	krmit	k5eAaImIp3nP	krmit
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
opouští	opouštět	k5eAaImIp3nS	opouštět
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
10-13	[number]	k4	10-13
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
při	při	k7c6	při
vyplašení	vyplašení	k1gNnSc6	vyplašení
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
skřivan	skřivan	k1gMnSc1	skřivan
lesní	lesní	k2eAgMnSc1d1	lesní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
skřivan	skřivan	k1gMnSc1	skřivan
lesní	lesní	k2eAgMnSc1d1	lesní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
skřivan	skřivan	k1gMnSc1	skřivan
lesní	lesní	k2eAgMnSc1d1	lesní
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Lullula	Lullula	k1gMnSc2	Lullula
arborea	arboreus	k1gMnSc2	arboreus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
