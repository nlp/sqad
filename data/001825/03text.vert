<s>
Poetismus	poetismus	k1gInSc1	poetismus
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
literární	literární	k2eAgInSc4d1	literární
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nerozšířil	rozšířit	k5eNaPmAgMnS	rozšířit
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
výlučně	výlučně	k6eAd1	výlučně
český	český	k2eAgInSc4d1	český
avantgardní	avantgardní	k2eAgInSc4d1	avantgardní
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Básně	báseň	k1gFnPc1	báseň
všedního	všední	k2eAgInSc2d1	všední
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
popisují	popisovat	k5eAaImIp3nP	popisovat
obyčejné	obyčejný	k2eAgFnPc4d1	obyčejná
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Poetismus	poetismus	k1gInSc1	poetismus
nechtěl	chtít	k5eNaImAgInS	chtít
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
literárním	literární	k2eAgInSc7d1	literární
programem	program	k1gInSc7	program
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgMnS	mít
ambice	ambice	k1gFnPc4	ambice
stát	stát	k5eAaImF	stát
se	s	k7c7	s
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
dívat	dívat	k5eAaImF	dívat
na	na	k7c4	na
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
básní	báseň	k1gFnSc7	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
poetismu	poetismus	k1gInSc2	poetismus
bylo	být	k5eAaImAgNnS	být
úplné	úplný	k2eAgNnSc4d1	úplné
oproštění	oproštění	k1gNnSc4	oproštění
od	od	k7c2	od
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
optimistický	optimistický	k2eAgInSc4d1	optimistický
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplynul	vyplynout	k5eAaPmAgInS	vyplynout
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
styl	styl	k1gInSc1	styl
rozvíjející	rozvíjející	k2eAgFnSc4d1	rozvíjející
lyriku	lyrika	k1gFnSc4	lyrika
a	a	k8xC	a
hravost	hravost	k1gFnSc4	hravost
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
interpunkce	interpunkce	k1gFnSc1	interpunkce
<g/>
.	.	kIx.	.
</s>
<s>
Soustředil	soustředit	k5eAaPmAgMnS	soustředit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
současnost	současnost	k1gFnSc4	současnost
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
na	na	k7c4	na
radostné	radostný	k2eAgInPc4d1	radostný
okamžiky	okamžik	k1gInPc4	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
hlavními	hlavní	k2eAgMnPc7d1	hlavní
tématy	téma	k1gNnPc7	téma
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
pocity	pocit	k1gInPc1	pocit
štěstí	štěstí	k1gNnSc2	štěstí
a	a	k8xC	a
radosti	radost	k1gFnSc2	radost
<g/>
,	,	kIx,	,
emotivní	emotivní	k2eAgFnSc1d1	emotivní
stránka	stránka	k1gFnSc1	stránka
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
poetismus	poetismus	k1gInSc4	poetismus
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgFnSc1d1	důležitá
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
poetistické	poetistický	k2eAgFnSc2d1	poetistická
filosofie	filosofie	k1gFnSc2	filosofie
byl	být	k5eAaImAgInS	být
problém	problém	k1gInSc1	problém
odcizení	odcizení	k1gNnSc2	odcizení
a	a	k8xC	a
sebeodcizení	sebeodcizení	k1gNnSc2	sebeodcizení
člověka	člověk	k1gMnSc2	člověk
-	-	kIx~	-
tj.	tj.	kA	tj.
mezilidských	mezilidský	k2eAgInPc2d1	mezilidský
vztahů	vztah	k1gInPc2	vztah
<g/>
;	;	kIx,	;
poetisté	poetista	k1gMnPc1	poetista
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
deformaci	deformace	k1gFnSc3	deformace
mezilidských	mezilidský	k2eAgInPc2d1	mezilidský
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
vztahu	vztah	k1gInSc2	vztah
člověka	člověk	k1gMnSc2	člověk
k	k	k7c3	k
okolnímu	okolní	k2eAgInSc3d1	okolní
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
odcizení	odcizení	k1gNnSc2	odcizení
vinili	vinit	k5eAaImAgMnP	vinit
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgNnSc3	svůj
uspořádání	uspořádání	k1gNnSc4	uspořádání
(	(	kIx(	(
<g/>
a	a	k8xC	a
složitosti	složitost	k1gFnSc2	složitost
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přestala	přestat	k5eAaPmAgFnS	přestat
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c6	o
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
štěstí	štěstí	k1gNnSc1	štěstí
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
sama	sám	k3xTgFnSc1	sám
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
poetismu	poetismus	k1gInSc2	poetismus
bylo	být	k5eAaImAgNnS	být
napomáhat	napomáhat	k5eAaImF	napomáhat
nápravě	náprava	k1gFnSc3	náprava
a	a	k8xC	a
překonání	překonání	k1gNnSc3	překonání
tohoto	tento	k3xDgNnSc2	tento
odcizení	odcizení	k1gNnSc2	odcizení
<g/>
.	.	kIx.	.
</s>
<s>
Poetismus	poetismus	k1gInSc1	poetismus
má	mít	k5eAaImIp3nS	mít
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
proletářské	proletářský	k2eAgFnSc6d1	proletářská
poezii	poezie	k1gFnSc6	poezie
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
avantgardní	avantgardní	k2eAgFnSc2d1	avantgardní
skupiny	skupina	k1gFnSc2	skupina
Devětsil	Devětsil	k1gInSc1	Devětsil
<g/>
,	,	kIx,	,
za	za	k7c4	za
rok	rok	k1gInSc4	rok
vzniku	vznik	k1gInSc2	vznik
bývá	bývat	k5eAaImIp3nS	bývat
uváděn	uvádět	k5eAaImNgInS	uvádět
letopočet	letopočet	k1gInSc1	letopočet
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
termín	termín	k1gInSc1	termín
poetismus	poetismus	k1gInSc1	poetismus
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
patrně	patrně	k6eAd1	patrně
použit	použít	k5eAaPmNgInS	použít
až	až	k9	až
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
vydal	vydat	k5eAaPmAgMnS	vydat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
sbírku	sbírka	k1gFnSc4	sbírka
Město	město	k1gNnSc4	město
v	v	k7c6	v
slzách	slza	k1gFnPc6	slza
a	a	k8xC	a
roku	rok	k1gInSc6	rok
1923	[number]	k4	1923
mu	on	k3xPp3gNnSc3	on
vyšla	vyjít	k5eAaPmAgFnS	vyjít
sbírka	sbírka	k1gFnSc1	sbírka
Samá	samý	k3xTgFnSc1	samý
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
předmluvy	předmluva	k1gFnPc1	předmluva
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
sbírkám	sbírka	k1gFnPc3	sbírka
byly	být	k5eAaImAgInP	být
psány	psát	k5eAaImNgInP	psát
jménem	jméno	k1gNnSc7	jméno
Devětsilu	Devětsil	k1gInSc2	Devětsil
a	a	k8xC	a
naznačovaly	naznačovat	k5eAaImAgInP	naznačovat
posun	posun	k1gInSc4	posun
myšlení	myšlení	k1gNnSc2	myšlení
uvnitř	uvnitř	k7c2	uvnitř
tzv.	tzv.	kA	tzv.
socialistické	socialistický	k2eAgFnSc2d1	socialistická
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
proletářské	proletářský	k2eAgFnSc2d1	proletářská
poezie	poezie	k1gFnSc2	poezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
první	první	k4xOgFnSc2	první
předmluvy	předmluva	k1gFnSc2	předmluva
byl	být	k5eAaImAgMnS	být
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zde	zde	k6eAd1	zde
zdůvodnil	zdůvodnit	k5eAaPmAgMnS	zdůvodnit
existenci	existence	k1gFnSc4	existence
proletářské	proletářský	k2eAgFnSc2d1	proletářská
poezie	poezie	k1gFnSc2	poezie
a	a	k8xC	a
jejích	její	k3xOp3gFnPc2	její
zásad	zásada	k1gFnPc2	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Posláním	poslání	k1gNnSc7	poslání
tvorby	tvorba	k1gFnSc2	tvorba
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
duchovní	duchovní	k2eAgFnSc1d1	duchovní
a	a	k8xC	a
citová	citový	k2eAgFnSc1d1	citová
příprava	příprava	k1gFnSc1	příprava
na	na	k7c6	na
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
předmluvu	předmluva	k1gFnSc4	předmluva
napsal	napsat	k5eAaBmAgMnS	napsat
Karel	Karel	k1gMnSc1	Karel
Teige	Teig	k1gMnSc2	Teig
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sice	sice	k8xC	sice
také	také	k6eAd1	také
zastával	zastávat	k5eAaImAgMnS	zastávat
socialistické	socialistický	k2eAgInPc4d1	socialistický
principy	princip	k1gInPc4	princip
a	a	k8xC	a
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
a	a	k8xC	a
moderní	moderní	k2eAgFnSc2d1	moderní
kultury	kultura	k1gFnSc2	kultura
<g/>
)	)	kIx)	)
měly	mít	k5eAaImAgFnP	mít
pronikat	pronikat	k5eAaImF	pronikat
cizí	cizí	k2eAgInPc4d1	cizí
vlivy	vliv	k1gInPc4	vliv
a	a	k8xC	a
že	že	k8xS	že
by	by	kYmCp3nS	by
i	i	k9	i
ona	onen	k3xDgFnSc1	onen
měla	mít	k5eAaImAgFnS	mít
promlouvat	promlouvat	k5eAaImF	promlouvat
a	a	k8xC	a
oslovovat	oslovovat	k5eAaImF	oslovovat
jiné	jiný	k2eAgInPc4d1	jiný
národy	národ	k1gInPc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
v	v	k7c6	v
Devětsilu	Devětsil	k1gInSc6	Devětsil
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
podzim	podzim	k1gInSc4	podzim
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
-	-	kIx~	-
ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Nové	Nové	k2eAgNnSc1d1	Nové
umění	umění	k1gNnSc1	umění
proletářské	proletářský	k2eAgNnSc1d1	proletářské
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
pokládán	pokládat	k5eAaImNgInS	pokládat
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
slovo	slovo	k1gNnSc4	slovo
nové	nový	k2eAgFnSc2d1	nová
tj.	tj.	kA	tj.
nebránící	bránící	k2eNgFnSc2d1	nebránící
novým	nový	k2eAgInPc3d1	nový
experimentům	experiment	k1gInPc3	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
stať	stať	k1gFnSc1	stať
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jiné	jiný	k2eAgFnPc1d1	jiná
úvahy	úvaha	k1gFnPc1	úvaha
K.	K.	kA	K.
Teiga	Teiga	k1gFnSc1	Teiga
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
namířeny	namířit	k5eAaPmNgFnP	namířit
proti	proti	k7c3	proti
Wolkerově	Wolkerův	k2eAgFnSc3d1	Wolkerova
nebo	nebo	k8xC	nebo
Horově	Horův	k2eAgFnSc3d1	Horova
poezii	poezie	k1gFnSc3	poezie
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
K.	K.	kA	K.
Teige	Teig	k1gFnSc2	Teig
v	v	k7c6	v
brněnském	brněnský	k2eAgInSc6d1	brněnský
časopise	časopis	k1gInSc6	časopis
Host	host	k1gMnSc1	host
do	do	k7c2	do
domu	dům	k1gInSc2	dům
první	první	k4xOgInSc4	první
ucelený	ucelený	k2eAgInSc4d1	ucelený
náčrt	náčrt	k1gInSc4	náčrt
poetismu	poetismus	k1gInSc2	poetismus
(	(	kIx(	(
<g/>
stať	stať	k1gFnSc1	stať
poetismus	poetismus	k1gInSc1	poetismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Revui	revue	k1gFnSc6	revue
Devětsilu	Devětsil	k1gInSc2	Devětsil
(	(	kIx(	(
<g/>
č.	č.	k?	č.
9	[number]	k4	9
<g/>
)	)	kIx)	)
vydán	vydat	k5eAaPmNgInS	vydat
Manifest	manifest	k1gInSc1	manifest
poetismu	poetismus	k1gInSc2	poetismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Poetismus	poetismus	k1gInSc1	poetismus
a	a	k8xC	a
skládal	skládat	k5eAaImAgMnS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Kapka	kapka	k1gFnSc1	kapka
inkoustu	inkoust	k1gInSc2	inkoust
-	-	kIx~	-
V.	V.	kA	V.
Nezval	Nezval	k1gMnSc1	Nezval
Ultrafialové	ultrafialový	k2eAgInPc4d1	ultrafialový
obrazy	obraz	k1gInPc4	obraz
čili	čili	k8xC	čili
Artificielismus	Artificielismus	k1gInSc1	Artificielismus
-	-	kIx~	-
K.	K.	kA	K.
Teige	Teige	k1gFnSc1	Teige
Manifest	manifest	k1gInSc1	manifest
Poetismu	poetismus	k1gInSc2	poetismus
-	-	kIx~	-
K.	K.	kA	K.
Teige	Teige	k1gNnSc4	Teige
Druhý	druhý	k4xOgInSc1	druhý
a	a	k8xC	a
poslední	poslední	k2eAgInSc1d1	poslední
manifest	manifest	k1gInSc1	manifest
poetismu	poetismus	k1gInSc2	poetismus
byl	být	k5eAaImAgInS	být
uveřejněn	uveřejněn	k2eAgInSc1d1	uveřejněn
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Host	host	k1gMnSc1	host
do	do	k7c2	do
domu	dům	k1gInSc2	dům
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Papoušek	Papoušek	k1gMnSc1	Papoušek
na	na	k7c6	na
motocyklu	motocykl	k1gInSc6	motocykl
čili	čili	k8xC	čili
O	o	k7c6	o
řemesle	řemeslo	k1gNnSc6	řemeslo
básnickém	básnický	k2eAgNnSc6d1	básnické
od	od	k7c2	od
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Nezvala	Nezval	k1gMnSc2	Nezval
<g/>
.	.	kIx.	.
</s>
<s>
Poetismus	poetismus	k1gInSc1	poetismus
se	se	k3xPyFc4	se
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
literatuře	literatura	k1gFnSc6	literatura
objevoval	objevovat	k5eAaImAgInS	objevovat
až	až	k6eAd1	až
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
myšlenky	myšlenka	k1gFnPc1	myšlenka
poetismu	poetismus	k1gInSc2	poetismus
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
písňové	písňový	k2eAgFnSc6d1	písňová
tvorbě	tvorba	k1gFnSc6	tvorba
dvojice	dvojice	k1gFnSc2	dvojice
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Šlitr	Šlitr	k1gMnSc1	Šlitr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literární	literární	k2eAgFnSc6d1	literární
tvorbě	tvorba	k1gFnSc6	tvorba
zdůrazňovali	zdůrazňovat	k5eAaImAgMnP	zdůrazňovat
řazení	řazení	k1gNnSc4	řazení
básnických	básnický	k2eAgInPc2d1	básnický
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c6	na
logice	logika	k1gFnSc6	logika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
hlavní	hlavní	k2eAgInSc4d1	hlavní
účel	účel	k1gInSc4	účel
básně	báseň	k1gFnSc2	báseň
považovali	považovat	k5eAaImAgMnP	považovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
básní	báseň	k1gFnSc7	báseň
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
hrou	hra	k1gFnSc7	hra
obrazotvornosti	obrazotvornost	k1gFnSc2	obrazotvornost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prioritním	prioritní	k2eAgInSc7d1	prioritní
znakem	znak	k1gInSc7	znak
jejich	jejich	k3xOp3gFnPc2	jejich
básní	báseň	k1gFnPc2	báseň
je	být	k5eAaImIp3nS	být
pěkný	pěkný	k2eAgInSc4d1	pěkný
rým	rým	k1gInSc4	rým
<g/>
.	.	kIx.	.
</s>
<s>
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	zvát	k5eNaImAgMnS	zvát
jeho	jeho	k3xOp3gFnSc4	jeho
kniha	kniha	k1gFnSc1	kniha
Pantomima	pantomima	k1gFnSc1	pantomima
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
knih	kniha	k1gFnPc2	kniha
poetismu	poetismus	k1gInSc2	poetismus
Konstantin	Konstantin	k1gMnSc1	Konstantin
Biebl	Biebl	k1gMnSc1	Biebl
František	František	k1gMnSc1	František
Halas	Halas	k1gMnSc1	Halas
Vladimír	Vladimír	k1gMnSc1	Vladimír
Holan	Holan	k1gMnSc1	Holan
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
Karel	Karel	k1gMnSc1	Karel
Teige	Teige	k1gFnPc2	Teige
Vladislav	Vladislav	k1gMnSc1	Vladislav
Vančura	Vančura	k1gMnSc1	Vančura
Vilém	Vilém	k1gMnSc1	Vilém
Závada	závada	k1gFnSc1	závada
Ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
také	také	k9	také
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poetizaci	poetizace	k1gFnSc3	poetizace
námětu	námět	k1gInSc2	námět
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Štyrský	Štyrský	k2eAgInSc1d1	Štyrský
Toyen	Toyen	k1gInSc1	Toyen
-	-	kIx~	-
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Marie	Maria	k1gFnSc2	Maria
Čermínová	Čermínová	k1gFnSc1	Čermínová
František	František	k1gMnSc1	František
Muzika	muzika	k1gFnSc1	muzika
Na	na	k7c6	na
divadle	divadlo	k1gNnSc6	divadlo
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
prosadil	prosadit	k5eAaPmAgInS	prosadit
v	v	k7c6	v
divadelní	divadelní	k2eAgFnSc6d1	divadelní
režii	režie	k1gFnSc6	režie
J.	J.	kA	J.
Frejky	Frejka	k1gFnSc2	Frejka
a	a	k8xC	a
J.	J.	kA	J.
Honzla	Honzla	k1gFnSc1	Honzla
<g/>
.	.	kIx.	.
nemocnice	nemocnice	k1gFnSc1	nemocnice
v	v	k7c6	v
Mukačevu	Mukačev	k1gInSc6	Mukačev
(	(	kIx(	(
<g/>
Podkarpatská	podkarpatský	k2eAgFnSc1d1	Podkarpatská
Rus	Rus	k1gFnSc1	Rus
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
Svaz	svaz	k1gInSc1	svaz
moderní	moderní	k2eAgFnSc2d1	moderní
kultury	kultura	k1gFnSc2	kultura
Devětsil	Devětsil	k1gInSc1	Devětsil
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
