<s>
Výmarská	výmarský	k2eAgFnSc1d1	Výmarská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Weimarer	Weimarer	k1gInSc1	Weimarer
Republik	republika	k1gFnPc2	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
historický	historický	k2eAgInSc4d1	historický
stát	stát	k1gInSc4	stát
Německa	Německo	k1gNnSc2	Německo
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
monarchií	monarchie	k1gFnPc2	monarchie
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
až	až	k9	až
do	do	k7c2	do
nástupu	nástup	k1gInSc2	nástup
nacistů	nacista	k1gMnPc2	nacista
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
