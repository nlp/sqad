<s>
Výmarská	výmarský	k2eAgFnSc1d1	Výmarská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Weimarer	Weimarer	k1gInSc1	Weimarer
Republik	republika	k1gFnPc2	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
historický	historický	k2eAgInSc4d1	historický
stát	stát	k1gInSc4	stát
Německa	Německo	k1gNnSc2	Německo
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
monarchií	monarchie	k1gFnPc2	monarchie
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
až	až	k9	až
do	do	k7c2	do
nástupu	nástup	k1gInSc2	nástup
nacistů	nacista	k1gMnPc2	nacista
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
Německa	Německo	k1gNnSc2	Německo
i	i	k9	i
nadále	nadále	k6eAd1	nadále
zněl	znět	k5eAaImAgInS	znět
Německá	německý	k2eAgFnSc1d1	německá
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Deutsches	Deutsches	k1gInSc1	Deutsches
Reich	Reich	k?	Reich
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
republikánský	republikánský	k2eAgInSc4d1	republikánský
spolkový	spolkový	k2eAgInSc4d1	spolkový
státní	státní	k2eAgInSc4d1	státní
útvar	útvar	k1gInSc4	útvar
s	s	k7c7	s
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
spolkovou	spolkový	k2eAgFnSc7d1	spolková
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
přijatou	přijatý	k2eAgFnSc4d1	přijatá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c6	po
lednových	lednový	k2eAgFnPc6d1	lednová
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
sešel	sejít	k5eAaPmAgInS	sejít
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1919	[number]	k4	1919
první	první	k4xOgInSc4	první
skutečně	skutečně	k6eAd1	skutečně
demokraticky	demokraticky	k6eAd1	demokraticky
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
zvolený	zvolený	k2eAgInSc1d1	zvolený
parlament	parlament	k1gInSc1	parlament
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
odvozeno	odvozen	k2eAgNnSc4d1	odvozeno
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Německa	Německo	k1gNnSc2	Německo
však	však	k8xC	však
zůstal	zůstat	k5eAaPmAgInS	zůstat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
Berlín	Berlín	k1gInSc1	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgMnS	být
říšský	říšský	k2eAgMnSc1d1	říšský
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
sborem	sbor	k1gInSc7	sbor
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
)	)	kIx)	)
skládající	skládající	k2eAgNnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
Říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
exekutivy	exekutiva	k1gFnSc2	exekutiva
stála	stát	k5eAaImAgFnS	stát
říšská	říšský	k2eAgFnSc1d1	říšská
vláda	vláda	k1gFnSc1	vláda
vedená	vedený	k2eAgFnSc1d1	vedená
říšským	říšský	k2eAgMnSc7d1	říšský
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
říšský	říšský	k2eAgMnSc1d1	říšský
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1919	[number]	k4	1919
podepsali	podepsat	k5eAaPmAgMnP	podepsat
zástupci	zástupce	k1gMnPc1	zástupce
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Versailleskou	versailleský	k2eAgFnSc4d1	Versailleská
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
bylo	být	k5eAaImAgNnS	být
Francii	Francie	k1gFnSc4	Francie
navráceno	navrácen	k2eAgNnSc1d1	navráceno
Alsasko-Lotrinsko	Alsasko-Lotrinsko	k1gNnSc1	Alsasko-Lotrinsko
(	(	kIx(	(
<g/>
14	[number]	k4	14
522	[number]	k4	522
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Belgii	Belgie	k1gFnSc3	Belgie
postoupeno	postoupen	k2eAgNnSc1d1	postoupeno
území	území	k1gNnSc1	území
Moresnetu	Moresnet	k1gInSc2	Moresnet
a	a	k8xC	a
po	po	k7c6	po
plebiscitu	plebiscit	k1gInSc6	plebiscit
i	i	k9	i
Eupen	Eupen	k1gInSc4	Eupen
a	a	k8xC	a
Malmédy	Malméda	k1gFnPc4	Malméda
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
1036	[number]	k4	1036
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Polsku	Polska	k1gFnSc4	Polska
většina	většina	k1gFnSc1	většina
Poznaňska	Poznaňsk	k1gInSc2	Poznaňsk
a	a	k8xC	a
Západního	západní	k2eAgNnSc2d1	západní
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
nepatrná	patrný	k2eNgFnSc1d1	patrný
část	část	k1gFnSc1	část
Východního	východní	k2eAgInSc2d1	východní
<g />
.	.	kIx.	.
</s>
<s>
Pruska	Prusko	k1gNnPc1	Prusko
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
46	[number]	k4	46
142	[number]	k4	142
km2	km2	k4	km2
<g/>
;	;	kIx,	;
Československu	Československo	k1gNnSc3	Československo
Hlučínsko	Hlučínsko	k1gNnSc1	Hlučínsko
(	(	kIx(	(
<g/>
316	[number]	k4	316
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Gdaňsk	Gdaňsk	k1gInSc4	Gdaňsk
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
km2	km2	k4	km2
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
přeměněn	přeměnit	k5eAaPmNgInS	přeměnit
v	v	k7c4	v
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
Svobodné	svobodný	k2eAgNnSc4d1	svobodné
město	město	k1gNnSc4	město
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
;	;	kIx,	;
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Východního	východní	k2eAgNnSc2d1	východní
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
,	,	kIx,	,
Klajpeda	Klajped	k1gMnSc2	Klajped
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
(	(	kIx(	(
<g/>
2416	[number]	k4	2416
km2	km2	k4	km2
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
autonomní	autonomní	k2eAgFnSc7d1	autonomní
částí	část	k1gFnSc7	část
Litvy	Litva	k1gFnSc2	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
získalo	získat	k5eAaPmAgNnS	získat
po	po	k7c6	po
plebiscitu	plebiscit	k1gInSc6	plebiscit
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Šlesvicka	Šlesvicka	k1gFnSc1	Šlesvicka
(	(	kIx(	(
<g/>
moderní	moderní	k2eAgInSc4d1	moderní
dánský	dánský	k2eAgInSc4d1	dánský
okres	okres	k1gInSc4	okres
Jižní	jižní	k2eAgNnSc1d1	jižní
Jutsko	Jutsko	k1gNnSc1	Jutsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
3	[number]	k4	3
938	[number]	k4	938
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Sársko	Sársko	k1gNnSc1	Sársko
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
1910,49	[number]	k4	1910,49
km2	km2	k4	km2
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
15	[number]	k4	15
let	léto	k1gNnPc2	léto
předáno	předán	k2eAgNnSc4d1	předáno
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
výnosy	výnos	k1gInPc1	výnos
jeho	jeho	k3xOp3gInPc2	jeho
dolů	dol	k1gInPc2	dol
postoupeny	postoupen	k2eAgFnPc1d1	postoupena
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Porýní	Porýní	k1gNnSc1	Porýní
bylo	být	k5eAaImAgNnS	být
demilitarizováno	demilitarizovat	k5eAaBmNgNnS	demilitarizovat
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
okupováno	okupovat	k5eAaBmNgNnS	okupovat
vítěznými	vítězný	k2eAgInPc7d1	vítězný
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
Německo	Německo	k1gNnSc1	Německo
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
kolonie	kolonie	k1gFnPc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zmenšilo	zmenšit	k5eAaPmAgNnS	zmenšit
o	o	k7c4	o
70	[number]	k4	70
615	[number]	k4	615
km2	km2	k4	km2
na	na	k7c4	na
468	[number]	k4	468
116,13	[number]	k4	116,13
km2	km2	k4	km2
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
postiženo	postihnout	k5eAaPmNgNnS	postihnout
válečnými	válečný	k2eAgFnPc7d1	válečná
reparacemi	reparace	k1gFnPc7	reparace
<g/>
.	.	kIx.	.
</s>
<s>
Neschopnost	neschopnost	k1gFnSc1	neschopnost
demokratických	demokratický	k2eAgFnPc2d1	demokratická
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
utvořit	utvořit	k5eAaPmF	utvořit
stabilní	stabilní	k2eAgFnSc4d1	stabilní
vládní	vládní	k2eAgFnSc4d1	vládní
koalici	koalice	k1gFnSc4	koalice
přiměla	přimět	k5eAaPmAgFnS	přimět
prezidenta	prezident	k1gMnSc4	prezident
Hindenburga	Hindenburg	k1gMnSc4	Hindenburg
jmenovat	jmenovat	k5eAaBmF	jmenovat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1930	[number]	k4	1930
Heinricha	Heinrich	k1gMnSc2	Heinrich
Brüninga	Brüning	k1gMnSc2	Brüning
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
nahradil	nahradit	k5eAaPmAgInS	nahradit
parlamentní	parlamentní	k2eAgFnSc4d1	parlamentní
demokracii	demokracie	k1gFnSc4	demokracie
autoritativnější	autoritativní	k2eAgFnSc7d2	autoritativnější
formou	forma	k1gFnSc7	forma
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Brüningova	Brüningův	k2eAgFnSc1d1	Brüningův
snaha	snaha	k1gFnSc1	snaha
zastavit	zastavit	k5eAaPmF	zastavit
zostřující	zostřující	k2eAgFnSc4d1	zostřující
se	se	k3xPyFc4	se
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
krizi	krize	k1gFnSc4	krize
selhala	selhat	k5eAaPmAgFnS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Kancléřova	kancléřův	k2eAgFnSc1d1	kancléřova
snaha	snaha	k1gFnSc1	snaha
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
nacistům	nacista	k1gMnPc3	nacista
zákazem	zákaz	k1gInSc7	zákaz
nošení	nošení	k1gNnSc4	nošení
uniforem	uniforma	k1gFnPc2	uniforma
se	se	k3xPyFc4	se
minula	minout	k5eAaImAgFnS	minout
účinkem	účinek	k1gInSc7	účinek
a	a	k8xC	a
nový	nový	k2eAgMnSc1d1	nový
kancléř	kancléř	k1gMnSc1	kancléř
Franz	Franz	k1gMnSc1	Franz
von	von	k1gInSc4	von
Papen	Papen	k2eAgInSc4d1	Papen
již	již	k6eAd1	již
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
kurs	kurs	k1gInSc1	kurs
sblížení	sblížení	k1gNnSc2	sblížení
s	s	k7c7	s
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
Brüningův	Brüningův	k2eAgInSc1d1	Brüningův
zákaz	zákaz	k1gInSc1	zákaz
odvolal	odvolat	k5eAaPmAgInS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Papenova	Papenův	k2eAgFnSc1d1	Papenův
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
však	však	k9	však
ukázala	ukázat	k5eAaPmAgFnS	ukázat
ještě	ještě	k6eAd1	ještě
labilnější	labilní	k2eAgFnSc1d2	labilnější
než	než	k8xS	než
jeho	on	k3xPp3gMnSc2	on
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1932	[number]	k4	1932
stal	stát	k5eAaPmAgInS	stát
kancléřem	kancléř	k1gMnSc7	kancléř
generál	generál	k1gMnSc1	generál
Kurt	Kurt	k1gMnSc1	Kurt
von	von	k1gInSc4	von
Schleicher	Schleichra	k1gFnPc2	Schleichra
<g/>
.	.	kIx.	.
</s>
<s>
Generálova	generálův	k2eAgFnSc1d1	generálova
snaha	snaha	k1gFnSc1	snaha
rozehnat	rozehnat	k5eAaPmF	rozehnat
nacisty	nacista	k1gMnPc4	nacista
pomocí	pomocí	k7c2	pomocí
armády	armáda	k1gFnSc2	armáda
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
odborů	odbor	k1gInPc2	odbor
a	a	k8xC	a
levicového	levicový	k2eAgNnSc2d1	levicové
křídla	křídlo	k1gNnSc2	křídlo
NSDAP	NSDAP	kA	NSDAP
však	však	k9	však
zkrachoval	zkrachovat	k5eAaPmAgInS	zkrachovat
a	a	k8xC	a
za	za	k7c2	za
Papenovy	Papenův	k2eAgFnSc2d1	Papenův
zákulisní	zákulisní	k2eAgFnSc2d1	zákulisní
podpory	podpora	k1gFnSc2	podpora
se	se	k3xPyFc4	se
nových	nový	k2eAgInPc6d1	nový
kancléřem	kancléř	k1gMnSc7	kancléř
stal	stát	k5eAaPmAgInS	stát
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
spolkovým	spolkový	k2eAgInSc7d1	spolkový
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
složeným	složený	k2eAgInSc7d1	složený
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
původních	původní	k2eAgFnPc2d1	původní
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
předchozího	předchozí	k2eAgNnSc2d1	předchozí
Německého	německý	k2eAgNnSc2d1	německé
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
republikami	republika	k1gFnPc7	republika
s	s	k7c7	s
německým	německý	k2eAgNnSc7d1	německé
označením	označení	k1gNnSc7	označení
Svobodný	svobodný	k2eAgInSc4d1	svobodný
stát	stát	k1gInSc4	stát
(	(	kIx(	(
<g/>
Freistaat	Freistaat	k1gInSc1	Freistaat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Lidový	lidový	k2eAgInSc1d1	lidový
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
Volksstaat	Volksstaat	k1gInSc1	Volksstaat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
Svobodný	svobodný	k2eAgInSc1d1	svobodný
lidový	lidový	k2eAgInSc1d1	lidový
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
Freier	Freier	k1gInSc1	Freier
Volksstaat	Volksstaat	k1gInSc1	Volksstaat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zmenšení	zmenšení	k1gNnSc3	zmenšení
počtu	počet	k1gInSc2	počet
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Durynska	Durynsko	k1gNnSc2	Durynsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Gothy	Gotha	k1gFnSc2	Gotha
a	a	k8xC	a
Koburgu	Koburg	k1gInSc2	Koburg
(	(	kIx(	(
<g/>
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
existovaly	existovat	k5eAaImAgInP	existovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
existovaly	existovat	k5eAaImAgInP	existovat
všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
již	již	k9	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
období	období	k1gNnSc4	období
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Svobodný	svobodný	k2eAgInSc1d1	svobodný
stát	stát	k1gInSc1	stát
Waldeck	Waldecko	k1gNnPc2	Waldecko
začleněn	začlenit	k5eAaPmNgInS	začlenit
do	do	k7c2	do
Svobodného	svobodný	k2eAgInSc2d1	svobodný
státu	stát	k1gInSc2	stát
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Sársko	Sársko	k1gNnSc1	Sársko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc3	rok
1919	[number]	k4	1919
vyčleněním	vyčlenění	k1gNnSc7	vyčlenění
částí	část	k1gFnPc2	část
území	území	k1gNnSc2	území
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
a	a	k8xC	a
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
spravováno	spravován	k2eAgNnSc1d1	spravováno
Společností	společnost	k1gFnSc7	společnost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
nemělo	mít	k5eNaImAgNnS	mít
postavení	postavení	k1gNnSc4	postavení
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
mělo	mít	k5eAaImAgNnS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
zemský	zemský	k2eAgInSc4d1	zemský
sněm	sněm	k1gInSc4	sněm
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
zemská	zemský	k2eAgFnSc1d1	zemská
rada	rada	k1gFnSc1	rada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
spolkový	spolkový	k2eAgInSc4d1	spolkový
stát	stát	k1gInSc4	stát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
Pruska	Prusko	k1gNnSc2	Prusko
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1929	[number]	k4	1929
Sloučením	sloučení	k1gNnSc7	sloučení
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
Lidový	lidový	k2eAgInSc1d1	lidový
stát	stát	k1gInSc1	stát
Reuss	Reussa	k1gFnPc2	Reussa
Sloučením	sloučení	k1gNnPc3	sloučení
šesti	šest	k4xCc2	šest
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1920	[number]	k4	1920
Země	zem	k1gFnSc2	zem
Durynsko	Durynsko	k1gNnSc4	Durynsko
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Svobodný	svobodný	k2eAgInSc1d1	svobodný
stát	stát	k1gInSc1	stát
Koburg	Koburg	k1gInSc1	Koburg
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1920	[number]	k4	1920
součástí	součást	k1gFnSc7	součást
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
.	.	kIx.	.
</s>
