<s>
Latinské	latinský	k2eAgNnSc1d1	latinské
písmo	písmo	k1gNnSc1	písmo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
asi	asi	k9	asi
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
odvozením	odvození	k1gNnSc7	odvození
od	od	k7c2	od
řeckého	řecký	k2eAgNnSc2d1	řecké
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
Římané	Říman	k1gMnPc1	Říman
přejali	přejmout	k5eAaPmAgMnP	přejmout
od	od	k7c2	od
Etrusků	Etrusk	k1gMnPc2	Etrusk
<g/>
.	.	kIx.	.
</s>
