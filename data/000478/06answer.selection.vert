<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
afrikánsky	afrikánsky	k6eAd1	afrikánsky
<g/>
:	:	kIx,	:
Kaapstad	Kaapstad	k1gInSc1	Kaapstad
/	/	kIx~	/
<g/>
ˈ	ˈ	k?	ˈ
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gInSc1	Town
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
xhosa	xhosa	k1gFnSc1	xhosa
iKapa	iKapa	k1gFnSc1	iKapa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
provincie	provincie	k1gFnSc2	provincie
Západní	západní	k2eAgNnSc1d1	západní
Kapsko	Kapsko	k1gNnSc1	Kapsko
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
legislativní	legislativní	k2eAgNnSc4d1	legislativní
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
tu	tu	k6eAd1	tu
jihoafrický	jihoafrický	k2eAgInSc1d1	jihoafrický
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
vládní	vládní	k2eAgFnPc1d1	vládní
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
</s>
