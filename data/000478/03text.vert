<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
afrikánsky	afrikánsky	k6eAd1	afrikánsky
<g/>
:	:	kIx,	:
Kaapstad	Kaapstad	k1gInSc1	Kaapstad
/	/	kIx~	/
<g/>
ˈ	ˈ	k?	ˈ
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gInSc1	Town
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
xhosa	xhosa	k1gFnSc1	xhosa
iKapa	iKapa	k1gFnSc1	iKapa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
provincie	provincie	k1gFnSc2	provincie
Západní	západní	k2eAgNnSc1d1	západní
Kapsko	Kapsko	k1gNnSc1	Kapsko
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
legislativní	legislativní	k2eAgNnSc4d1	legislativní
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
tu	tu	k6eAd1	tu
jihoafrický	jihoafrický	k2eAgInSc1d1	jihoafrický
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
vládní	vládní	k2eAgFnPc1d1	vládní
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
přírodním	přírodní	k2eAgFnPc3d1	přírodní
scenériím	scenérie	k1gFnPc3	scenérie
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Stolová	stolový	k2eAgFnSc1d1	stolová
hora	hora	k1gFnSc1	hora
nebo	nebo	k8xC	nebo
mys	mys	k1gInSc1	mys
Dobré	dobrý	k2eAgFnSc2d1	dobrá
naděje	naděje	k1gFnSc2	naděje
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgNnPc2d3	nejkrásnější
měst	město	k1gNnPc2	město
světa	svět	k1gInSc2	svět
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
turistickým	turistický	k2eAgInSc7d1	turistický
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgFnSc7	první
evropskou	evropský	k2eAgFnSc7d1	Evropská
osadou	osada	k1gFnSc7	osada
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	on	k3xPp3gMnPc4	on
jihoafričany	jihoafričan	k1gMnPc4	jihoafričan
často	často	k6eAd1	často
označováno	označován	k2eAgNnSc1d1	označováno
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Mother	Mothra	k1gFnPc2	Mothra
City	City	k1gFnSc2	City
<g/>
"	"	kIx"	"
-	-	kIx~	-
mateřské	mateřský	k2eAgNnSc1d1	mateřské
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Západní	západní	k2eAgNnSc4d1	západní
Kapsko	Kapsko	k1gNnSc4	Kapsko
žili	žít	k5eAaImAgMnP	žít
lidé	člověk	k1gMnPc1	člověk
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
,	,	kIx,	,
nevytvořili	vytvořit	k5eNaPmAgMnP	vytvořit
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
vlastní	vlastní	k2eAgNnSc4d1	vlastní
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
o	o	k7c6	o
prehistorii	prehistorie	k1gFnSc6	prehistorie
těchto	tento	k3xDgNnPc2	tento
míst	místo	k1gNnPc2	místo
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
písemnou	písemný	k2eAgFnSc4d1	písemná
zmínku	zmínka	k1gFnSc4	zmínka
učinil	učinit	k5eAaPmAgMnS	učinit
až	až	k9	až
portugalský	portugalský	k2eAgMnSc1d1	portugalský
objevitel	objevitel	k1gMnSc1	objevitel
Bartolomeo	Bartolomeo	k1gMnSc1	Bartolomeo
Diaz	Diaz	k1gMnSc1	Diaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1486	[number]	k4	1486
<g/>
;	;	kIx,	;
po	po	k7c4	po
dvě	dva	k4xCgFnPc4	dva
následující	následující	k2eAgFnPc4d1	následující
století	století	k1gNnSc1	století
nebyla	být	k5eNaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
Evropany	Evropan	k1gMnPc4	Evropan
trvale	trvale	k6eAd1	trvale
obývána	obýván	k2eAgFnSc1d1	obývána
<g/>
.	.	kIx.	.
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1652	[number]	k4	1652
Jan	Jan	k1gMnSc1	Jan
van	vana	k1gFnPc2	vana
Riebeeck	Riebeeck	k1gMnSc1	Riebeeck
jako	jako	k9	jako
zásobovací	zásobovací	k2eAgFnSc3d1	zásobovací
stanici	stanice	k1gFnSc3	stanice
pro	pro	k7c4	pro
holandské	holandský	k2eAgFnPc4d1	holandská
lodě	loď	k1gFnPc4	loď
plující	plující	k2eAgFnPc4d1	plující
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
;	;	kIx,	;
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
bylo	být	k5eAaImAgNnS	být
ideální	ideální	k2eAgInSc4d1	ideální
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
chránící	chránící	k2eAgFnSc2d1	chránící
lodě	loď	k1gFnSc2	loď
před	před	k7c7	před
rozbouřeným	rozbouřený	k2eAgNnSc7d1	rozbouřené
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
před	před	k7c7	před
domorodým	domorodý	k2eAgNnSc7d1	domorodé
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
postavili	postavit	k5eAaPmAgMnP	postavit
Holanďané	Holanďan	k1gMnPc1	Holanďan
pevnost	pevnost	k1gFnSc4	pevnost
Fort	Fort	k?	Fort
de	de	k?	de
Goede	Goed	k1gInSc5	Goed
Hoop	Hoop	k1gInSc1	Hoop
-	-	kIx~	-
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Castle	Castle	k1gFnSc1	Castle
of	of	k?	of
Good	Good	k1gInSc4	Good
Hope	Hop	k1gFnSc2	Hop
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
ale	ale	k8xC	ale
rostl	růst	k5eAaImAgInS	růst
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
se	s	k7c7	s
vzrůstajícími	vzrůstající	k2eAgInPc7d1	vzrůstající
požadavky	požadavek	k1gInPc7	požadavek
na	na	k7c4	na
pracovní	pracovní	k2eAgFnSc4d1	pracovní
sílu	síla	k1gFnSc4	síla
začala	začít	k5eAaPmAgFnS	začít
holandská	holandský	k2eAgFnSc1d1	holandská
Východoindická	východoindický	k2eAgFnSc1d1	Východoindická
společnost	společnost	k1gFnSc1	společnost
dovážet	dovážet	k5eAaImF	dovážet
otroky	otrok	k1gMnPc4	otrok
z	z	k7c2	z
Madagaskaru	Madagaskar	k1gInSc2	Madagaskar
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc2	Malajsie
a	a	k8xC	a
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
málo	málo	k4c1	málo
evropských	evropský	k2eAgFnPc2d1	Evropská
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
vznikala	vznikat	k5eAaImAgNnP	vznikat
četná	četný	k2eAgNnPc1d1	četné
smíšená	smíšený	k2eAgNnPc1d1	smíšené
manželství	manželství	k1gNnPc1	manželství
mezi	mezi	k7c7	mezi
Evropany	Evropan	k1gMnPc7	Evropan
a	a	k8xC	a
domorodými	domorodý	k2eAgFnPc7d1	domorodá
Afričankami	Afričanka	k1gFnPc7	Afričanka
nebo	nebo	k8xC	nebo
Asiatkami	Asiatka	k1gFnPc7	Asiatka
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc1	jejich
potomci	potomek	k1gMnPc1	potomek
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xC	jako
Cape	capat	k5eAaImIp3nS	capat
Coloureds	Coloureds	k1gInSc4	Coloureds
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
150	[number]	k4	150
let	léto	k1gNnPc2	léto
pod	pod	k7c7	pod
nizozemskou	nizozemský	k2eAgFnSc7d1	nizozemská
správou	správa	k1gFnSc7	správa
se	se	k3xPyFc4	se
z	z	k7c2	z
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
stal	stát	k5eAaPmAgInS	stát
důležitý	důležitý	k2eAgInSc1d1	důležitý
přístav	přístav	k1gInSc1	přístav
poskytující	poskytující	k2eAgInSc4d1	poskytující
odpočinek	odpočinek	k1gInSc4	odpočinek
námořníkům	námořník	k1gMnPc3	námořník
plujícím	plující	k2eAgInPc3d1	plující
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
americké	americký	k2eAgFnSc2d1	americká
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
stáli	stát	k5eAaImAgMnP	stát
Nizozemci	Nizozemec	k1gMnPc1	Nizozemec
společně	společně	k6eAd1	společně
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vojenské	vojenský	k2eAgInPc1d1	vojenský
střety	střet	k1gInPc1	střet
mezi	mezi	k7c7	mezi
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Nizozemím	Nizozemí	k1gNnSc7	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Muizenbergu	Muizenberg	k1gInSc2	Muizenberg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1795	[number]	k4	1795
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Britové	Brit	k1gMnPc1	Brit
dočasně	dočasně	k6eAd1	dočasně
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1803	[number]	k4	1803
ale	ale	k8xC	ale
připadlo	připadnout	k5eAaPmAgNnS	připadnout
zpět	zpět	k6eAd1	zpět
Nizozemcům	Nizozemec	k1gMnPc3	Nizozemec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
další	další	k2eAgFnSc6d1	další
prohrané	prohraný	k2eAgFnSc6d1	prohraná
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Bloubergstrandu	Bloubergstrand	k1gInSc2	Bloubergstrand
(	(	kIx(	(
<g/>
asi	asi	k9	asi
25	[number]	k4	25
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
holandská	holandský	k2eAgFnSc1d1	holandská
Východoindická	východoindický	k2eAgFnSc1d1	Východoindická
společnost	společnost	k1gFnSc1	společnost
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
pokraj	pokraj	k1gInSc4	pokraj
bankrotu	bankrot	k1gInSc2	bankrot
<g/>
,	,	kIx,	,
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
i	i	k9	i
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
kolonií	kolonie	k1gFnSc7	kolonie
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1814	[number]	k4	1814
definitivně	definitivně	k6eAd1	definitivně
Britské	britský	k2eAgFnSc3d1	britská
koruně	koruna	k1gFnSc3	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
nalezišť	naleziště	k1gNnPc2	naleziště
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
diamantů	diamant	k1gInPc2	diamant
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Witwatersrand	Witwatersranda	k1gFnPc2	Witwatersranda
blízko	blízko	k7c2	blízko
Johannesburgu	Johannesburg	k1gInSc2	Johannesburg
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
zlatou	zlatý	k2eAgFnSc4d1	zlatá
horečku	horečka	k1gFnSc4	horečka
<g/>
.	.	kIx.	.
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
následně	následně	k6eAd1	následně
sice	sice	k8xC	sice
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
velice	velice	k6eAd1	velice
profitovalo	profitovat	k5eAaBmAgNnS	profitovat
ze	z	k7c2	z
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
obchodního	obchodní	k2eAgInSc2d1	obchodní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tehdy	tehdy	k6eAd1	tehdy
vydělal	vydělat	k5eAaPmAgMnS	vydělat
jmění	jmění	k1gNnSc4	jmění
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Kapské	kapský	k2eAgFnSc2d1	kapská
kolonie	kolonie	k1gFnSc2	kolonie
Cecil	Cecil	k1gMnSc1	Cecil
Rhodes	Rhodes	k1gMnSc1	Rhodes
<g/>
,	,	kIx,	,
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
žijící	žijící	k2eAgFnPc1d1	žijící
v	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
Městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
majitel	majitel	k1gMnSc1	majitel
těžební	těžební	k2eAgFnSc2d1	těžební
společnosti	společnost	k1gFnSc2	společnost
De	De	k?	De
Beers	Beers	k1gInSc1	Beers
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dodnes	dodnes	k6eAd1	dodnes
těží	těžet	k5eAaImIp3nS	těžet
nejvíce	hodně	k6eAd3	hodně
diamantů	diamant	k1gInPc2	diamant
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
eskalovalo	eskalovat	k5eAaImAgNnS	eskalovat
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
britskou	britský	k2eAgFnSc7d1	britská
koloniální	koloniální	k2eAgFnSc7d1	koloniální
správou	správa	k1gFnSc7	správa
<g/>
,	,	kIx,	,
novými	nový	k2eAgMnPc7d1	nový
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
(	(	kIx(	(
<g/>
nazývanými	nazývaný	k2eAgInPc7d1	nazývaný
"	"	kIx"	"
<g/>
uitlanders	uitlanders	k1gInSc1	uitlanders
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Búry	Búr	k1gMnPc7	Búr
<g/>
,	,	kIx,	,
potomky	potomek	k1gMnPc7	potomek
nizozemských	nizozemský	k2eAgMnPc2d1	nizozemský
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
zakládat	zakládat	k5eAaImF	zakládat
vlastní	vlastní	k2eAgInPc4d1	vlastní
státy	stát	k1gInPc4	stát
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
tohoto	tento	k3xDgNnSc2	tento
napětí	napětí	k1gNnSc2	napětí
byly	být	k5eAaImAgFnP	být
dvě	dva	k4xCgFnPc1	dva
búrské	búrský	k2eAgFnPc1d1	búrská
války	válka	k1gFnPc1	válka
<g/>
;	;	kIx,	;
po	po	k7c4	po
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
střetnutích	střetnutí	k1gNnPc6	střetnutí
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
Britové	Brit	k1gMnPc1	Brit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
Jihoafrickou	jihoafrický	k2eAgFnSc4d1	Jihoafrická
unii	unie	k1gFnSc4	unie
s	s	k7c7	s
Kapským	kapský	k2eAgNnSc7d1	Kapské
Městem	město	k1gNnSc7	město
jako	jako	k8xC	jako
legislativním	legislativní	k2eAgNnSc7d1	legislativní
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Epidemie	epidemie	k1gFnSc1	epidemie
dýmějového	dýmějový	k2eAgInSc2d1	dýmějový
moru	mor	k1gInSc2	mor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
posloužila	posloužit	k5eAaPmAgFnS	posloužit
koloniální	koloniální	k2eAgFnSc1d1	koloniální
správě	správa	k1gFnSc3	správa
jako	jako	k8xS	jako
záminka	záminka	k1gFnSc1	záminka
pro	pro	k7c4	pro
zavedení	zavedení	k1gNnSc4	zavedení
rasové	rasový	k2eAgFnSc2d1	rasová
segregace	segregace	k1gFnSc2	segregace
(	(	kIx(	(
<g/>
známější	známý	k2eAgInSc1d2	známější
pod	pod	k7c7	pod
afrikánským	afrikánský	k2eAgInSc7d1	afrikánský
názvem	název	k1gInSc7	název
apartheid	apartheid	k1gInSc1	apartheid
<g/>
)	)	kIx)	)
-	-	kIx~	-
černošskému	černošský	k2eAgNnSc3d1	černošské
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
byly	být	k5eAaImAgFnP	být
vyhrazeny	vyhrazen	k2eAgFnPc1d1	vyhrazena
dvě	dva	k4xCgFnPc1	dva
městské	městský	k2eAgFnPc1d1	městská
oblasti	oblast	k1gFnPc1	oblast
oddělené	oddělený	k2eAgFnPc1d1	oddělená
od	od	k7c2	od
ostatní	ostatní	k2eAgFnSc2d1	ostatní
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
volby	volba	k1gFnPc4	volba
National	National	k1gFnSc2	National
Party	party	k1gFnSc4	party
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slibovala	slibovat	k5eAaImAgFnS	slibovat
zákony	zákon	k1gInPc4	zákon
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
podporu	podpora	k1gFnSc4	podpora
rasové	rasový	k2eAgFnSc2d1	rasová
segregace	segregace	k1gFnSc2	segregace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgNnP	být
všechna	všechen	k3xTgNnPc1	všechen
městská	městský	k2eAgNnPc1d1	Městské
území	území	k1gNnPc1	území
rozčleněna	rozčleněn	k2eAgNnPc1d1	rozčleněno
na	na	k7c6	na
rasových	rasový	k2eAgInPc6d1	rasový
základech	základ	k1gInPc6	základ
<g/>
,	,	kIx,	,
z	z	k7c2	z
některých	některý	k3yIgNnPc2	některý
předměstí	předměstí	k1gNnPc2	předměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dříve	dříve	k6eAd2	dříve
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
žili	žít	k5eAaImAgMnP	žít
příslušníci	příslušník	k1gMnPc1	příslušník
více	hodně	k6eAd2	hodně
ras	rasa	k1gFnPc2	rasa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
černí	černý	k2eAgMnPc1d1	černý
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
bývalá	bývalý	k2eAgNnPc4d1	bývalé
obydlí	obydlí	k1gNnPc4	obydlí
byla	být	k5eAaImAgFnS	být
nezřídka	nezřídka	k6eAd1	nezřídka
určena	určit	k5eAaPmNgFnS	určit
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
District	District	k1gMnSc1	District
Six	Six	k1gMnSc1	Six
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
zdemolován	zdemolovat	k5eAaPmNgInS	zdemolovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
jako	jako	k8xC	jako
okrsek	okrsek	k1gInSc1	okrsek
pro	pro	k7c4	pro
bílé	bílé	k1gNnSc4	bílé
<g/>
;	;	kIx,	;
následně	následně	k6eAd1	následně
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
bylo	být	k5eAaImAgNnS	být
nuceně	nuceně	k6eAd1	nuceně
vystěhováno	vystěhovat	k5eAaPmNgNnS	vystěhovat
60	[number]	k4	60
000	[number]	k4	000
černochů	černoch	k1gMnPc2	černoch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
Městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
mnoho	mnoho	k4c1	mnoho
odpůrců	odpůrce	k1gMnPc2	odpůrce
apartheidu	apartheid	k1gInSc2	apartheid
<g/>
;	;	kIx,	;
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
internováni	internován	k2eAgMnPc1d1	internován
jako	jako	k8xC	jako
političtí	politický	k2eAgMnPc1d1	politický
vězni	vězeň	k1gMnPc1	vězeň
na	na	k7c4	na
Robben	Robben	k2eAgInSc4d1	Robben
Island	Island	k1gInSc4	Island
<g/>
,	,	kIx,	,
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
cca	cca	kA	cca
10	[number]	k4	10
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Nelson	Nelson	k1gMnSc1	Nelson
Mandela	Mandel	k1gMnSc2	Mandel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
z	z	k7c2	z
balkónu	balkón	k1gInSc2	balkón
radnice	radnice	k1gFnSc2	radnice
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
pronesl	pronést	k5eAaPmAgInS	pronést
svoji	svůj	k3xOyFgFnSc4	svůj
slavnou	slavný	k2eAgFnSc4d1	slavná
řeč	řeč	k1gFnSc4	řeč
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
předurčil	předurčit	k5eAaPmAgMnS	předurčit
novou	nový	k2eAgFnSc4d1	nová
etapu	etapa	k1gFnSc4	etapa
vývoje	vývoj	k1gInSc2	vývoj
Jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pádu	pád	k1gInSc2	pád
apartheidu	apartheid	k1gInSc2	apartheid
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
věcech	věc	k1gFnPc6	věc
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
:	:	kIx,	:
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
bezpečnější	bezpečný	k2eAgNnSc1d2	bezpečnější
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
razantnímu	razantní	k2eAgInSc3d1	razantní
se	se	k3xPyFc4	se
zvýšení	zvýšení	k1gNnSc4	zvýšení
ceny	cena	k1gFnSc2	cena
pozemků	pozemek	k1gInPc2	pozemek
a	a	k8xC	a
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
mnoho	mnoho	k4c1	mnoho
programů	program	k1gInPc2	program
pro	pro	k7c4	pro
revitalizaci	revitalizace	k1gFnSc4	revitalizace
příměstských	příměstský	k2eAgFnPc2d1	příměstská
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
městských	městský	k2eAgFnPc6d1	městská
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgInPc4d1	stejný
příjmy	příjem	k1gInPc4	příjem
a	a	k8xC	a
sociální	sociální	k2eAgNnSc4d1	sociální
postavení	postavení	k1gNnSc4	postavení
jako	jako	k8xS	jako
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
apartheidu	apartheid	k1gInSc2	apartheid
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
věci	věc	k1gFnPc1	věc
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
<g/>
:	:	kIx,	:
město	město	k1gNnSc1	město
bojuje	bojovat	k5eAaImIp3nS	bojovat
s	s	k7c7	s
epidemií	epidemie	k1gFnSc7	epidemie
AIDS	AIDS	kA	AIDS
a	a	k8xC	a
nárůstem	nárůst	k1gInSc7	nárůst
především	především	k6eAd1	především
drogové	drogový	k2eAgFnSc2d1	drogová
kriminality	kriminalita	k1gFnSc2	kriminalita
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ale	ale	k9	ale
dnes	dnes	k6eAd1	dnes
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
zažívá	zažívat	k5eAaImIp3nS	zažívat
nebývalý	nebývalý	k2eAgInSc4d1	nebývalý
turistický	turistický	k2eAgInSc4d1	turistický
boom	boom	k1gInSc4	boom
<g/>
,	,	kIx,	,
možná	možná	k9	možná
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
bezpečnější	bezpečný	k2eAgFnSc1d2	bezpečnější
než	než	k8xS	než
ostatní	ostatní	k2eAgNnPc1d1	ostatní
jihoafrická	jihoafrický	k2eAgNnPc1d1	jihoafrické
velkoměsta	velkoměsto	k1gNnPc1	velkoměsto
jako	jako	k8xC	jako
Johannesburg	Johannesburg	k1gInSc1	Johannesburg
<g/>
,	,	kIx,	,
Pretoria	Pretorium	k1gNnPc1	Pretorium
nebo	nebo	k8xC	nebo
Durban	Durban	k1gInSc1	Durban
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
prosince	prosinec	k1gInSc2	prosinec
1967	[number]	k4	1967
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
nemocnici	nemocnice	k1gFnSc6	nemocnice
Groote	Groot	k1gInSc5	Groot
Schuur	Schuura	k1gFnPc2	Schuura
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
tým	tým	k1gInSc1	tým
profesora	profesor	k1gMnSc2	profesor
Christiaana	Christiaana	k1gFnSc1	Christiaana
Barnarda	Barnarda	k1gFnSc1	Barnarda
první	první	k4xOgFnSc4	první
transplantaci	transplantace	k1gFnSc4	transplantace
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
konci	konec	k1gInSc6	konec
hornatého	hornatý	k2eAgInSc2d1	hornatý
Kapského	kapský	k2eAgInSc2d1	kapský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zabíhá	zabíhat	k5eAaImIp3nS	zabíhat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
mysem	mys	k1gInSc7	mys
Dobré	dobrý	k2eAgFnPc4d1	dobrá
naděje	naděje	k1gFnPc4	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
asi	asi	k9	asi
kilometr	kilometr	k1gInSc1	kilometr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
Stolová	stolový	k2eAgFnSc1d1	stolová
hora	hora	k1gFnSc1	hora
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
strmými	strmý	k2eAgInPc7d1	strmý
srázy	sráz	k1gInPc4	sráz
Devil	Devil	k1gInSc1	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Peak	Peak	k1gInSc1	Peak
a	a	k8xC	a
Lion	Lion	k1gMnSc1	Lion
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Head	Heada	k1gFnPc2	Heada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
mnoho	mnoho	k4c1	mnoho
vrcholů	vrchol	k1gInPc2	vrchol
vyšších	vysoký	k2eAgInPc2d2	vyšší
než	než	k8xS	než
300	[number]	k4	300
m	m	kA	m
<g/>
;	;	kIx,	;
mnohá	mnohý	k2eAgNnPc1d1	mnohé
předměstí	předměstí	k1gNnPc1	předměstí
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
planině	planina	k1gFnSc6	planina
Cape	capat	k5eAaImIp3nS	capat
Flats	Flats	k1gInSc1	Flats
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
poloostrov	poloostrov	k1gInSc4	poloostrov
s	s	k7c7	s
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
subtropickém	subtropický	k2eAgInSc6d1	subtropický
pásu	pás	k1gInSc6	pás
s	s	k7c7	s
dobře	dobře	k6eAd1	dobře
rozeznatelnými	rozeznatelný	k2eAgFnPc7d1	rozeznatelná
čtyřmi	čtyři	k4xCgNnPc7	čtyři
ročními	roční	k2eAgNnPc7d1	roční
obdobími	období	k1gNnPc7	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
převažují	převažovat	k5eAaImIp3nP	převažovat
severozápadní	severozápadní	k2eAgInPc1d1	severozápadní
větry	vítr	k1gInPc1	vítr
z	z	k7c2	z
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgInPc4d1	zimní
měsíce	měsíc	k1gInPc4	měsíc
jsou	být	k5eAaImIp3nP	být
chladné	chladný	k2eAgInPc1d1	chladný
<g/>
,	,	kIx,	,
s	s	k7c7	s
nejnižšími	nízký	k2eAgFnPc7d3	nejnižší
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
teplotami	teplota	k1gFnPc7	teplota
okolo	okolo	k7c2	okolo
7	[number]	k4	7
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
spadne	spadnout	k5eAaPmIp3nS	spadnout
nejvíce	nejvíce	k6eAd1	nejvíce
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
značně	značně	k6eAd1	značně
členitého	členitý	k2eAgInSc2d1	členitý
terénu	terén	k1gInSc2	terén
jsou	být	k5eAaImIp3nP	být
srážkové	srážkový	k2eAgInPc1d1	srážkový
úhrny	úhrn	k1gInPc1	úhrn
značně	značně	k6eAd1	značně
územně	územně	k6eAd1	územně
diferencované	diferencovaný	k2eAgNnSc1d1	diferencované
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
a	a	k8xC	a
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
jsou	být	k5eAaImIp3nP	být
průměrné	průměrný	k2eAgFnPc1d1	průměrná
srážky	srážka	k1gFnPc1	srážka
515	[number]	k4	515
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
1500	[number]	k4	1500
mm	mm	kA	mm
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgFnSc1d1	trvající
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
teplé	teplý	k2eAgNnSc1d1	teplé
a	a	k8xC	a
suché	suchý	k2eAgNnSc1d1	suché
<g/>
.	.	kIx.	.
</s>
<s>
Převažují	převažovat	k5eAaImIp3nP	převažovat
jihovýchodní	jihovýchodní	k2eAgInPc1d1	jihovýchodní
větry	vítr	k1gInPc1	vítr
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Cape	capat	k5eAaImIp3nS	capat
Doctor	Doctor	k1gMnSc1	Doctor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
díky	díky	k7c3	díky
nim	on	k3xPp3gMnPc3	on
se	se	k3xPyFc4	se
rozptýlí	rozptýlit	k5eAaPmIp3nS	rozptýlit
zplodiny	zplodina	k1gFnPc1	zplodina
z	z	k7c2	z
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
průměrné	průměrný	k2eAgFnPc1d1	průměrná
letní	letní	k2eAgFnPc1d1	letní
teploty	teplota	k1gFnPc1	teplota
nepřesahují	přesahovat	k5eNaImIp3nP	přesahovat
26	[number]	k4	26
°	°	k?	°
<g/>
C.	C.	kA	C.
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
mělo	mít	k5eAaImAgNnS	mít
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
2	[number]	k4	2
893	[number]	k4	893
251	[number]	k4	251
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Coloured	Coloured	k1gInSc1	Coloured
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
míšenci	míšenec	k1gMnPc1	míšenec
evropských	evropský	k2eAgInPc2d1	evropský
a	a	k8xC	a
asijských	asijský	k2eAgMnPc2d1	asijský
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
48,13	[number]	k4	48,13
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
černí	černý	k2eAgMnPc1d1	černý
Afričané	Afričan	k1gMnPc1	Afričan
31	[number]	k4	31
%	%	kIx~	%
<g/>
,	,	kIx,	,
běloši	běloch	k1gMnPc1	běloch
18,75	[number]	k4	18,75
%	%	kIx~	%
a	a	k8xC	a
Asiaté	Asiat	k1gMnPc1	Asiat
1,43	[number]	k4	1,43
%	%	kIx~	%
<g/>
.	.	kIx.	.
46,6	[number]	k4	46,6
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
mladší	mladý	k2eAgFnSc1d2	mladší
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jen	jen	k9	jen
5	[number]	k4	5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgFnSc1d2	starší
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Medián	medián	k1gInSc1	medián
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
)	)	kIx)	)
věkové	věkový	k2eAgFnSc2d1	věková
struktury	struktura	k1gFnSc2	struktura
je	být	k5eAaImIp3nS	být
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
na	na	k7c4	na
každých	každý	k3xTgFnPc2	každý
100	[number]	k4	100
žen	žena	k1gFnPc2	žena
připadá	připadat	k5eAaImIp3nS	připadat
92,4	[number]	k4	92,4
mužů	muž	k1gMnPc2	muž
<g/>
;	;	kIx,	;
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
19,4	[number]	k4	19,4
%	%	kIx~	%
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
-	-	kIx~	-
58,3	[number]	k4	58,3
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
černoši	černoch	k1gMnPc1	černoch
<g/>
,	,	kIx,	,
38,1	[number]	k4	38,1
%	%	kIx~	%
Coloured	Coloured	k1gMnSc1	Coloured
<g/>
,	,	kIx,	,
3,1	[number]	k4	3,1
%	%	kIx~	%
běloši	běloch	k1gMnPc1	běloch
a	a	k8xC	a
0,5	[number]	k4	0,5
%	%	kIx~	%
Asiaté	Asiat	k1gMnPc1	Asiat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
41,4	[number]	k4	41,4
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
mluví	mluvit	k5eAaImIp3nS	mluvit
doma	doma	k6eAd1	doma
afrikánsky	afrikánsky	k6eAd1	afrikánsky
<g/>
,	,	kIx,	,
28,7	[number]	k4	28,7
%	%	kIx~	%
jazykem	jazyk	k1gInSc7	jazyk
Xhosa	Xhos	k1gMnSc2	Xhos
<g/>
,	,	kIx,	,
27,9	[number]	k4	27,9
%	%	kIx~	%
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
0,7	[number]	k4	0,7
%	%	kIx~	%
jazykem	jazyk	k1gInSc7	jazyk
Sesotho	Sesot	k1gMnSc2	Sesot
<g/>
,	,	kIx,	,
0,3	[number]	k4	0,3
%	%	kIx~	%
jazykem	jazyk	k1gInSc7	jazyk
Zulu	Zulu	k1gMnSc1	Zulu
<g/>
,	,	kIx,	,
0,1	[number]	k4	0,1
%	%	kIx~	%
jazykem	jazyk	k1gInSc7	jazyk
Setswana	Setswan	k1gMnSc2	Setswan
a	a	k8xC	a
0,7	[number]	k4	0,7
%	%	kIx~	%
mluví	mluvit	k5eAaImIp3nS	mluvit
doma	doma	k6eAd1	doma
některým	některý	k3yIgFnPc3	některý
z	z	k7c2	z
neoficiálních	oficiální	k2eNgInPc2d1	neoficiální
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
76,6	[number]	k4	76,6
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
10,7	[number]	k4	10,7
%	%	kIx~	%
ateisté	ateista	k1gMnPc1	ateista
<g/>
,	,	kIx,	,
9,7	[number]	k4	9,7
%	%	kIx~	%
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
0,5	[number]	k4	0,5
%	%	kIx~	%
židé	žid	k1gMnPc1	žid
<g/>
,	,	kIx,	,
0,2	[number]	k4	0,2
%	%	kIx~	%
hinduisté	hinduista	k1gMnPc1	hinduista
a	a	k8xC	a
2,3	[number]	k4	2,3
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
další	další	k2eAgNnSc1d1	další
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
4,2	[number]	k4	4,2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
starších	starý	k2eAgMnPc2d2	starší
20	[number]	k4	20
let	léto	k1gNnPc2	léto
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgNnSc1	žádný
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
18,9	[number]	k4	18,9
%	%	kIx~	%
má	mít	k5eAaImIp3nS	mít
základní	základní	k2eAgNnSc1d1	základní
vzdělání	vzdělání	k1gNnSc1	vzdělání
<g/>
,	,	kIx,	,
64,3	[number]	k4	64,3
%	%	kIx~	%
střední	střední	k2eAgNnSc4d1	střední
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
12,6	[number]	k4	12,6
%	%	kIx~	%
vyšší	vysoký	k2eAgNnPc4d2	vyšší
než	než	k8xS	než
střední	střední	k2eAgNnSc4d1	střední
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Medián	medián	k1gInSc1	medián
ročních	roční	k2eAgInPc2d1	roční
příjmů	příjem	k1gInPc2	příjem
pracujících	pracující	k2eAgMnPc2d1	pracující
občanů	občan	k1gMnPc2	občan
ve	v	k7c6	v
věkovém	věkový	k2eAgNnSc6d1	věkové
rozmezí	rozmezí	k1gNnSc6	rozmezí
15-65	[number]	k4	15-65
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
774	[number]	k4	774
jihoafrických	jihoafrický	k2eAgInPc2d1	jihoafrický
randů	rand	k1gInPc2	rand
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
82	[number]	k4	82
000	[number]	k4	000
českých	český	k2eAgFnPc2d1	Česká
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
centrem	centr	k1gInSc7	centr
provincie	provincie	k1gFnSc2	provincie
Západní	západní	k2eAgNnSc4d1	západní
Kapsko	Kapsko	k1gNnSc4	Kapsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
důležitý	důležitý	k2eAgInSc1d1	důležitý
přístav	přístav	k1gInSc1	přístav
a	a	k8xC	a
letiště	letiště	k1gNnSc1	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
důležité	důležitý	k2eAgFnPc1d1	důležitá
vládní	vládní	k2eAgFnPc1d1	vládní
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
jsou	být	k5eAaImIp3nP	být
závislá	závislý	k2eAgNnPc4d1	závislé
mnohá	mnohý	k2eAgNnPc4d1	mnohé
odvětví	odvětví	k1gNnPc4	odvětví
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
také	také	k9	také
hostuje	hostovat	k5eAaImIp3nS	hostovat
mnoho	mnoho	k4c4	mnoho
konferencí	konference	k1gFnPc2	konference
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c4	v
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gInSc1	Town
International	International	k1gFnPc2	International
Convention	Convention	k1gInSc1	Convention
Centre	centr	k1gInSc5	centr
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zažívá	zažívat	k5eAaImIp3nS	zažívat
prudký	prudký	k2eAgInSc1d1	prudký
rozvoj	rozvoj	k1gInSc1	rozvoj
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
realitami	realita	k1gFnPc7	realita
-	-	kIx~	-
mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
staví	stavit	k5eAaImIp3nS	stavit
letní	letní	k2eAgInPc4d1	letní
domky	domek	k1gInPc4	domek
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
na	na	k7c4	na
stálo	stát	k5eAaImAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Prudký	prudký	k2eAgInSc1d1	prudký
rozvoj	rozvoj	k1gInSc1	rozvoj
také	také	k9	také
zažívá	zažívat	k5eAaImIp3nS	zažívat
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Investory	investor	k1gMnPc4	investor
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
láká	lákat	k5eAaImIp3nS	lákat
především	především	k9	především
relativně	relativně	k6eAd1	relativně
kvalifikovaná	kvalifikovaný	k2eAgFnSc1d1	kvalifikovaná
pracovní	pracovní	k2eAgFnSc1d1	pracovní
síla	síla	k1gFnSc1	síla
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
relativně	relativně	k6eAd1	relativně
hodně	hodně	k6eAd1	hodně
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
středoškolským	středoškolský	k2eAgNnSc7d1	středoškolské
vzděláním	vzdělání	k1gNnSc7	vzdělání
nebo	nebo	k8xC	nebo
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávanými	uznávaný	k2eAgInPc7d1	uznávaný
diplomy	diplom	k1gInPc7	diplom
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnSc1	provincie
Západní	západní	k2eAgNnSc1d1	západní
Kapsko	Kapsko	k1gNnSc1	Kapsko
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
produkce	produkce	k1gFnSc2	produkce
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
výrobků	výrobek	k1gInPc2	výrobek
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
víno	víno	k1gNnSc4	víno
a	a	k8xC	a
květiny	květina	k1gFnPc4	květina
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
provincii	provincie	k1gFnSc6	provincie
se	se	k3xPyFc4	se
také	také	k9	také
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
celkových	celkový	k2eAgInPc2d1	celkový
jihoafrického	jihoafrický	k2eAgInSc2d1	jihoafrický
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
produkce	produkce	k1gFnSc2	produkce
se	se	k3xPyFc4	se
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
přes	přes	k7c4	přes
Port	port	k1gInSc4	port
of	of	k?	of
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gInSc4	Town
nebo	nebo	k8xC	nebo
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gNnSc4	Town
International	International	k1gMnSc2	International
Airport	Airport	k1gInSc4	Airport
<g/>
,	,	kIx,	,
v	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
Městě	město	k1gNnSc6	město
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgInPc4	svůj
závody	závod	k1gInPc4	závod
mnohé	mnohý	k2eAgFnSc2d1	mnohá
loďařské	loďařský	k2eAgFnSc2d1	loďařská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
provincie	provincie	k1gFnSc1	provincie
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitým	důležitý	k2eAgNnSc7d1	důležité
energetickým	energetický	k2eAgNnSc7d1	energetické
centrem	centrum	k1gNnSc7	centrum
země	zem	k1gFnSc2	zem
<g/>
;	;	kIx,	;
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
objevena	objeven	k2eAgNnPc1d1	objeveno
ložiska	ložisko	k1gNnPc1	ložisko
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
důležitá	důležitý	k2eAgNnPc1d1	důležité
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
textilní	textilní	k2eAgInSc4d1	textilní
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
informační	informační	k2eAgFnPc4d1	informační
technologie	technologie	k1gFnPc4	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnSc1	provincie
Západní	západní	k2eAgNnSc1d1	západní
Kapsko	Kapsko	k1gNnSc1	Kapsko
je	být	k5eAaImIp3nS	být
také	také	k9	také
důležitým	důležitý	k2eAgInSc7d1	důležitý
turistickým	turistický	k2eAgInSc7d1	turistický
regionem	region	k1gInSc7	region
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
turistika	turistika	k1gFnSc1	turistika
představuje	představovat	k5eAaImIp3nS	představovat
9,8	[number]	k4	9,8
%	%	kIx~	%
HDP	HDP	kA	HDP
provincie	provincie	k1gFnSc2	provincie
a	a	k8xC	a
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
9,6	[number]	k4	9,6
%	%	kIx~	%
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
příjemnému	příjemný	k2eAgNnSc3d1	příjemné
klimatu	klima	k1gNnSc3	klima
<g/>
,	,	kIx,	,
krásným	krásný	k2eAgFnPc3d1	krásná
přírodním	přírodní	k2eAgFnPc3d1	přírodní
scenériím	scenérie	k1gFnPc3	scenérie
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
dobře	dobře	k6eAd1	dobře
rozvinuté	rozvinutý	k2eAgFnSc6d1	rozvinutá
infrastruktuře	infrastruktura	k1gFnSc6	infrastruktura
se	se	k3xPyFc4	se
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejoblíbenější	oblíbený	k2eAgInPc4d3	nejoblíbenější
turistické	turistický	k2eAgInPc4d1	turistický
cíle	cíl	k1gInPc4	cíl
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
přírodních	přírodní	k2eAgFnPc2d1	přírodní
pozoruhodností	pozoruhodnost	k1gFnPc2	pozoruhodnost
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Stolová	stolový	k2eAgFnSc1d1	stolová
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
Table	tablo	k1gNnSc6	tablo
Mountain	Mountain	k2eAgMnSc1d1	Mountain
National	National	k1gMnSc1	National
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Úchvatnou	úchvatný	k2eAgFnSc4d1	úchvatná
scenérii	scenérie	k1gFnSc4	scenérie
také	také	k9	také
skýtá	skýtat	k5eAaImIp3nS	skýtat
hornatý	hornatý	k2eAgInSc1d1	hornatý
Kapský	kapský	k2eAgInSc1d1	kapský
poloostrov	poloostrov	k1gInSc1	poloostrov
zakončený	zakončený	k2eAgInSc1d1	zakončený
mysem	mys	k1gInSc7	mys
Dobré	dobrá	k1gFnSc2	dobrá
naděje	naděje	k1gFnSc2	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
krásnému	krásný	k2eAgInSc3d1	krásný
výhledu	výhled	k1gInSc3	výhled
na	na	k7c4	na
Atlantik	Atlantik	k1gInSc4	Atlantik
a	a	k8xC	a
blízká	blízký	k2eAgNnPc4d1	blízké
pohoří	pohoří	k1gNnPc4	pohoří
též	též	k9	též
mnoho	mnoho	k4c4	mnoho
turistů	turist	k1gMnPc2	turist
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
projíždí	projíždět	k5eAaImIp3nS	projíždět
úzkou	úzký	k2eAgFnSc7d1	úzká
silnicí	silnice	k1gFnSc7	silnice
Chapman	Chapman	k1gMnSc1	Chapman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Peak	Peak	k1gInSc1	Peak
Drive	drive	k1gInSc4	drive
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgNnPc1d1	spojující
města	město	k1gNnPc1	město
Noordhoek	Noordhoky	k1gFnPc2	Noordhoky
a	a	k8xC	a
Hout	Houta	k1gFnPc2	Houta
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
unikátní	unikátní	k2eAgFnSc3d1	unikátní
poloze	poloha	k1gFnSc3	poloha
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
za	za	k7c4	za
pouhý	pouhý	k2eAgInSc4d1	pouhý
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
navštívit	navštívit	k5eAaPmF	navštívit
několik	několik	k4yIc4	několik
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
pláží	pláž	k1gFnPc2	pláž
<g/>
,	,	kIx,	,
zasazených	zasazený	k2eAgFnPc2d1	zasazená
v	v	k7c6	v
odlišném	odlišný	k2eAgNnSc6d1	odlišné
přírodním	přírodní	k2eAgNnSc6d1	přírodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Pláže	pláž	k1gFnPc1	pláž
při	při	k7c6	při
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
chladnější	chladný	k2eAgFnSc4d2	chladnější
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
False	Fals	k1gMnSc2	Fals
Bay	Bay	k1gMnSc2	Bay
má	mít	k5eAaImIp3nS	mít
teplotu	teplota	k1gFnSc4	teplota
vyšší	vysoký	k2eAgFnSc4d2	vyšší
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
o	o	k7c4	o
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
Všechny	všechen	k3xTgFnPc1	všechen
pláže	pláž	k1gFnPc1	pláž
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
Atlantském	atlantský	k2eAgNnSc6d1	Atlantské
pobřeží	pobřeží	k1gNnSc6	pobřeží
existuje	existovat	k5eAaImIp3nS	existovat
rozvinutější	rozvinutý	k2eAgFnSc1d2	rozvinutější
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
a	a	k8xC	a
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
kaváren	kavárna	k1gFnPc2	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
kapská	kapský	k2eAgFnSc1d1	kapská
pláž	pláž	k1gFnSc1	pláž
Boulders	Bouldersa	k1gFnPc2	Bouldersa
Beach	Beach	k1gInSc4	Beach
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kolonii	kolonie	k1gFnSc4	kolonie
afrických	africký	k2eAgMnPc2d1	africký
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
Blízká	blízký	k2eAgNnPc1d1	blízké
města	město	k1gNnPc1	město
Stellenbosch	Stellenboscha	k1gFnPc2	Stellenboscha
<g/>
,	,	kIx,	,
Paarl	Paarla	k1gFnPc2	Paarla
a	a	k8xC	a
Franschhoek	Franschhoky	k1gFnPc2	Franschhoky
jsou	být	k5eAaImIp3nP	být
oblíbenými	oblíbený	k2eAgInPc7d1	oblíbený
cíli	cíl	k1gInPc7	cíl
milovníků	milovník	k1gMnPc2	milovník
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
velryby	velryba	k1gFnPc4	velryba
a	a	k8xC	a
delfíny	delfín	k1gMnPc4	delfín
<g/>
,	,	kIx,	,
město	město	k1gNnSc4	město
Hermanus	Hermanus	k1gInSc4	Hermanus
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
velrybím	velrybí	k2eAgInSc7d1	velrybí
festivalem	festival	k1gInSc7	festival
<g/>
.	.	kIx.	.
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
jedinečné	jedinečný	k2eAgNnSc1d1	jedinečné
svým	svůj	k3xOyFgNnSc7	svůj
architektonickým	architektonický	k2eAgNnSc7d1	architektonické
dědictvím	dědictví	k1gNnSc7	dědictví
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
četnými	četný	k2eAgFnPc7d1	četná
památkami	památka	k1gFnPc7	památka
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Cape	capat	k5eAaImIp3nS	capat
Dutch	Dutch	k1gInSc1	Dutch
-	-	kIx~	-
tento	tento	k3xDgInSc1	tento
styl	styl	k1gInSc1	styl
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
francouzské	francouzský	k2eAgInPc4d1	francouzský
<g/>
,	,	kIx,	,
nizozemské	nizozemský	k2eAgInPc4d1	nizozemský
a	a	k8xC	a
německé	německý	k2eAgInPc4d1	německý
architektonické	architektonický	k2eAgInPc4d1	architektonický
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
památky	památka	k1gFnPc1	památka
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
Central	Central	k1gFnSc6	Central
Business	business	k1gInSc1	business
District	Districta	k1gFnPc2	Districta
(	(	kIx(	(
<g/>
staré	starý	k2eAgFnSc2d1	stará
vládní	vládní	k2eAgFnSc2d1	vládní
budovy	budova	k1gFnSc2	budova
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Long	Long	k1gMnSc1	Long
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgFnPc2d3	nejoblíbenější
městských	městský	k2eAgFnPc2d1	městská
nákupních	nákupní	k2eAgFnPc2d1	nákupní
zón	zóna	k1gFnPc2	zóna
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
viktoriánský	viktoriánský	k2eAgInSc1d1	viktoriánský
komplex	komplex	k1gInSc1	komplex
Victoria	Victorium	k1gNnSc2	Victorium
and	and	k?	and
Alfred	Alfred	k1gMnSc1	Alfred
Waterfront	Waterfront	k1gMnSc1	Waterfront
<g/>
,	,	kIx,	,
vystavený	vystavený	k2eAgMnSc1d1	vystavený
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
doků	dok	k1gInPc2	dok
<g/>
;	;	kIx,	;
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
akvárium	akvárium	k1gNnSc4	akvárium
Two	Two	k1gFnSc2	Two
Oceans	Oceansa	k1gFnPc2	Oceansa
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
osobitého	osobitý	k2eAgNnSc2d1	osobité
kouzla	kouzlo	k1gNnSc2	kouzlo
V	v	k7c6	v
<g/>
&	&	k?	&
<g/>
A	A	kA	A
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
zkratkou	zkratka	k1gFnSc7	zkratka
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
starý	starý	k2eAgInSc1d1	starý
přístav	přístav	k1gInSc1	přístav
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
návštěvníci	návštěvník	k1gMnPc1	návštěvník
mohou	moct	k5eAaImIp3nP	moct
nerušeně	nerušeně	k6eAd1	nerušeně
pozorovat	pozorovat	k5eAaImF	pozorovat
přístavní	přístavní	k2eAgInSc4d1	přístavní
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
navštívit	navštívit	k5eAaPmF	navštívit
další	další	k2eAgNnPc4d1	další
zajímavá	zajímavý	k2eAgNnPc4d1	zajímavé
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
nejstarší	starý	k2eAgFnSc4d3	nejstarší
budovu	budova	k1gFnSc4	budova
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
-	-	kIx~	-
Castle	Castle	k1gFnSc1	Castle
of	of	k?	of
Good	Good	k1gInSc1	Good
Hope	Hope	k1gInSc1	Hope
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgInPc1d1	barokní
pevnost	pevnost	k1gFnSc4	pevnost
postavenou	postavený	k2eAgFnSc4d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1666	[number]	k4	1666
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
architektonicky	architektonicky	k6eAd1	architektonicky
zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
objekty	objekt	k1gInPc1	objekt
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
městská	městský	k2eAgFnSc1d1	městská
radnice	radnice	k1gFnSc1	radnice
(	(	kIx(	(
<g/>
City	city	k1gNnSc1	city
Hall	Halla	k1gFnPc2	Halla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
St.	st.	kA	st.
Georges	Georgesa	k1gFnPc2	Georgesa
Cathedral	Cathedral	k1gFnSc1	Cathedral
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
anglikánského	anglikánský	k2eAgMnSc2d1	anglikánský
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
;	;	kIx,	;
National	National	k1gMnSc1	National
Gallery	Galler	k1gInPc4	Galler
a	a	k8xC	a
National	National	k1gFnSc2	National
Museum	museum	k1gNnSc1	museum
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
je	být	k5eAaImIp3nS	být
Long	Long	k1gInSc4	Long
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
kaváren	kavárna	k1gFnPc2	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Turisticky	turisticky	k6eAd1	turisticky
velice	velice	k6eAd1	velice
atraktivní	atraktivní	k2eAgMnSc1d1	atraktivní
je	být	k5eAaImIp3nS	být
Robben	Robben	k2eAgInSc1d1	Robben
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
afrikánsky	afrikánsky	k6eAd1	afrikánsky
<g/>
:	:	kIx,	:
Robbeneiland	Robbeneiland	k1gInSc1	Robbeneiland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zapsaný	zapsaný	k2eAgInSc1d1	zapsaný
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
doby	doba	k1gFnSc2	doba
apartheidu	apartheid	k1gInSc2	apartheid
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
tábor	tábor	k1gInSc1	tábor
pro	pro	k7c4	pro
politické	politický	k2eAgMnPc4d1	politický
vězně	vězeň	k1gMnPc4	vězeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pobýval	pobývat	k5eAaImAgMnS	pobývat
i	i	k9	i
bývalý	bývalý	k2eAgMnSc1d1	bývalý
jihoafrický	jihoafrický	k2eAgMnSc1d1	jihoafrický
prezident	prezident	k1gMnSc1	prezident
Nelson	Nelson	k1gMnSc1	Nelson
Mandela	Mandela	k1gFnSc1	Mandela
nebo	nebo	k8xC	nebo
Govan	Govan	k1gInSc1	Govan
Mbeki	Mbeki	k1gNnSc2	Mbeki
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
jihoafrického	jihoafrický	k2eAgMnSc2d1	jihoafrický
prezidenta	prezident	k1gMnSc2	prezident
Thabo	Thaba	k1gMnSc5	Thaba
Mbeki	Mbek	k1gMnSc5	Mbek
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
se	s	k7c7	s
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
v	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
Městě	město	k1gNnSc6	město
koná	konat	k5eAaImIp3nS	konat
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gInSc1	Town
Minstrel	minstrel	k1gMnSc1	minstrel
Carnival	Carnival	k1gMnSc1	Carnival
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
pod	pod	k7c7	pod
afrikánským	afrikánský	k2eAgInSc7d1	afrikánský
názvem	název	k1gInSc7	název
Kaapse	Kaaps	k1gMnSc2	Kaaps
Klose	Klos	k1gMnSc2	Klos
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžící	soutěžící	k2eAgFnPc1d1	soutěžící
skupinky	skupinka	k1gFnPc1	skupinka
minstrelů	minstrel	k1gMnPc2	minstrel
v	v	k7c6	v
pestrobarevných	pestrobarevný	k2eAgInPc6d1	pestrobarevný
kostýmech	kostým	k1gInPc6	kostým
promenují	promenovat	k5eAaImIp3nP	promenovat
městem	město	k1gNnSc7	město
a	a	k8xC	a
hrají	hrát	k5eAaImIp3nP	hrát
na	na	k7c4	na
rozličné	rozličný	k2eAgInPc4d1	rozličný
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
také	také	k9	také
bezesporu	bezesporu	k9	bezesporu
stojí	stát	k5eAaImIp3nS	stát
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
Kirstenbosch	Kirstenboscha	k1gFnPc2	Kirstenboscha
při	při	k7c6	při
jižním	jižní	k2eAgNnSc6d1	jižní
úpatí	úpatí	k1gNnSc6	úpatí
Stolové	stolový	k2eAgFnSc2d1	stolová
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
věnoval	věnovat	k5eAaPmAgMnS	věnovat
Kapskému	kapský	k2eAgNnSc3d1	Kapské
Městu	město	k1gNnSc3	město
Cecil	Cecil	k1gMnSc1	Cecil
Rhodes	Rhodes	k1gInSc1	Rhodes
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
ohrazením	ohrazení	k1gNnSc7	ohrazení
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
vzácné	vzácný	k2eAgInPc1d1	vzácný
druhy	druh	k1gInPc1	druh
rostlin	rostlina	k1gFnPc2	rostlina
-	-	kIx~	-
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
nebyla	být	k5eNaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
postavená	postavený	k2eAgFnSc1d1	postavená
žádná	žádný	k3yNgFnSc1	žádný
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
populárním	populární	k2eAgInSc7d1	populární
sportem	sport	k1gInSc7	sport
surfování	surfování	k1gNnSc2	surfování
<g/>
;	;	kIx,	;
město	město	k1gNnSc1	město
každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
soutěž	soutěž	k1gFnSc4	soutěž
Red	Red	k1gFnPc2	Red
Bull	bulla	k1gFnPc2	bulla
Big	Big	k1gMnSc2	Big
Wave	Wav	k1gMnSc2	Wav
Africa	Africus	k1gMnSc2	Africus
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
populární	populární	k2eAgInPc1d1	populární
sporty	sport	k1gInPc1	sport
jsou	být	k5eAaImIp3nP	být
kriket	kriket	k1gInSc1	kriket
<g/>
,	,	kIx,	,
plavání	plavání	k1gNnSc1	plavání
<g/>
,	,	kIx,	,
fotbal	fotbal	k1gInSc1	fotbal
a	a	k8xC	a
ragby	ragby	k1gNnSc1	ragby
<g/>
.	.	kIx.	.
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
často	často	k6eAd1	často
hostuje	hostovat	k5eAaImIp3nS	hostovat
zápasy	zápas	k1gInPc4	zápas
národního	národní	k2eAgInSc2d1	národní
ragbyového	ragbyový	k2eAgInSc2d1	ragbyový
týmu	tým	k1gInSc2	tým
-	-	kIx~	-
"	"	kIx"	"
<g/>
Springboks	Springboks	k1gInSc1	Springboks
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hrály	hrát	k5eAaImAgInP	hrát
některé	některý	k3yIgInPc1	některý
zápasy	zápas	k1gInPc1	zápas
ragbyového	ragbyový	k2eAgNnSc2d1	ragbyové
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ragby	ragby	k1gNnSc6	ragby
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
semifinále	semifinále	k1gNnSc2	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
velice	velice	k6eAd1	velice
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
ragbyový	ragbyový	k2eAgInSc1d1	ragbyový
klub	klub	k1gInSc1	klub
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Stormers	Stormersa	k1gFnPc2	Stormersa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
první	první	k4xOgFnSc6	první
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
lize	liga	k1gFnSc6	liga
hrají	hrát	k5eAaImIp3nP	hrát
dva	dva	k4xCgInPc4	dva
fotbalové	fotbalový	k2eAgInPc4d1	fotbalový
kluby	klub	k1gInPc4	klub
z	z	k7c2	z
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
Ajax	Ajax	k1gInSc1	Ajax
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gMnSc1	Town
a	a	k8xC	a
Santos	Santos	k1gMnSc1	Santos
Football	Football	k1gMnSc1	Football
Club	club	k1gInSc4	club
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
Městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
také	také	k6eAd1	také
hrálo	hrát	k5eAaImAgNnS	hrát
několik	několik	k4yIc4	několik
zápasů	zápas	k1gInPc2	zápas
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gInSc4	Town
International	International	k1gFnSc1	International
Airport	Airport	k1gInSc1	Airport
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
letiště	letiště	k1gNnSc4	letiště
na	na	k7c6	na
území	území	k1gNnSc6	území
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
letiště	letiště	k1gNnSc1	letiště
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
přímé	přímý	k2eAgNnSc1d1	přímé
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
větších	veliký	k2eAgNnPc2d2	veliký
měst	město	k1gNnPc2	město
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
městy	město	k1gNnPc7	město
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
blížící	blížící	k2eAgNnSc4d1	blížící
se	se	k3xPyFc4	se
datum	datum	k1gNnSc4	datum
zahájení	zahájení	k1gNnSc2	zahájení
fotbalového	fotbalový	k2eAgNnSc2d1	fotbalové
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
prochází	procházet	k5eAaImIp3nS	procházet
letiště	letiště	k1gNnSc4	letiště
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
renovací	renovace	k1gFnSc7	renovace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
stavbu	stavba	k1gFnSc4	stavba
nových	nový	k2eAgFnPc2d1	nová
vysokokapacitních	vysokokapacitní	k2eAgFnPc2d1	vysokokapacitní
garáží	garáž	k1gFnPc2	garáž
<g/>
,	,	kIx,	,
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
terminálu	terminál	k1gInSc2	terminál
pro	pro	k7c4	pro
vnitrostátní	vnitrostátní	k2eAgInPc4d1	vnitrostátní
lety	let	k1gInPc4	let
a	a	k8xC	a
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgInSc2d1	nový
terminálu	terminál	k1gInSc2	terminál
pro	pro	k7c4	pro
mezistátní	mezistátní	k2eAgInPc4d1	mezistátní
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Průběžně	průběžně	k6eAd1	průběžně
se	se	k3xPyFc4	se
také	také	k9	také
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
nákladní	nákladní	k2eAgFnSc1d1	nákladní
kapacita	kapacita	k1gFnSc1	kapacita
letiště	letiště	k1gNnSc2	letiště
<g/>
,	,	kIx,	,
na	na	k7c6	na
okolních	okolní	k2eAgFnPc6d1	okolní
volných	volný	k2eAgFnPc6d1	volná
plochách	plocha	k1gFnPc6	plocha
se	se	k3xPyFc4	se
staví	stavit	k5eAaPmIp3nS	stavit
nové	nový	k2eAgFnPc4d1	nová
kancelářské	kancelářský	k2eAgFnPc4d1	kancelářská
prostory	prostora	k1gFnPc4	prostora
a	a	k8xC	a
hotely	hotel	k1gInPc4	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
jako	jako	k9	jako
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
situováno	situován	k2eAgNnSc1d1	situováno
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
námořních	námořní	k2eAgFnPc2d1	námořní
tras	trasa	k1gFnPc2	trasa
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Přístav	přístav	k1gInSc1	přístav
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Central	Central	k1gFnSc2	Central
Business	business	k1gInSc1	business
District	District	k1gInSc1	District
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
Table	tablo	k1gNnSc6	tablo
Bay	Bay	k1gFnSc2	Bay
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
přístav	přístav	k1gInSc1	přístav
odbavil	odbavit	k5eAaPmAgInS	odbavit
3	[number]	k4	3
161	[number]	k4	161
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
9,2	[number]	k4	9,2
miliónů	milión	k4xCgInPc2	milión
tun	tuna	k1gFnPc2	tuna
zboží	zboží	k1gNnSc2	zboží
<g/>
;	;	kIx,	;
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
odbaveného	odbavený	k2eAgNnSc2d1	odbavené
zboží	zboží	k1gNnSc2	zboží
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
po	po	k7c6	po
durbanském	durbanský	k2eAgInSc6d1	durbanský
přístavu	přístav	k1gInSc6	přístav
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgMnSc1d3	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
základnou	základna	k1gFnSc7	základna
jihoafrického	jihoafrický	k2eAgNnSc2d1	jihoafrické
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
je	být	k5eAaImIp3nS	být
blízký	blízký	k2eAgMnSc1d1	blízký
Simon	Simon	k1gMnSc1	Simon
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Town	Town	k1gMnSc1	Town
Harbour	Harbour	k1gMnSc1	Harbour
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
zátoce	zátoka	k1gFnSc6	zátoka
False	Fals	k1gMnSc2	Fals
Bay	Bay	k1gMnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
provozovatelem	provozovatel	k1gMnSc7	provozovatel
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
firma	firma	k1gFnSc1	firma
Spoornet	Spoornet	k1gInSc1	Spoornet
<g/>
,	,	kIx,	,
dálkové	dálkový	k2eAgInPc4d1	dálkový
osobní	osobní	k2eAgInPc4d1	osobní
spoje	spoj	k1gInPc4	spoj
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
divize	divize	k1gFnSc2	divize
Shosholoza	Shosholoz	k1gMnSc4	Shosholoz
Meyl	Meyl	k1gMnSc1	Meyl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
provozuje	provozovat	k5eAaImIp3nS	provozovat
denní	denní	k2eAgInPc4d1	denní
spoje	spoj	k1gInPc4	spoj
do	do	k7c2	do
Johannesburgu	Johannesburg	k1gInSc2	Johannesburg
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Kimberley	Kimberley	k1gInPc4	Kimberley
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
týdně	týdně	k6eAd1	týdně
do	do	k7c2	do
Durbanu	Durban	k1gInSc2	Durban
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Kimberley	Kimberley	k1gInPc4	Kimberley
<g/>
,	,	kIx,	,
Bloemfontein	Bloemfontein	k1gMnSc1	Bloemfontein
a	a	k8xC	a
Pietermaritzburg	Pietermaritzburg	k1gMnSc1	Pietermaritzburg
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
směr	směr	k1gInSc4	směr
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
-	-	kIx~	-
Durban	Durban	k1gMnSc1	Durban
každé	každý	k3xTgNnSc4	každý
pondělí	pondělí	k1gNnSc4	pondělí
a	a	k8xC	a
směr	směr	k1gInSc4	směr
Durban	Durban	k1gInSc1	Durban
-	-	kIx~	-
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
každou	každý	k3xTgFnSc4	každý
středu	středa	k1gFnSc4	středa
<g/>
.	.	kIx.	.
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
konečná	konečný	k2eAgFnSc1d1	konečná
stanice	stanice	k1gFnSc1	stanice
luxusního	luxusní	k2eAgInSc2d1	luxusní
vlaku	vlak	k1gInSc2	vlak
Blue	Blu	k1gFnSc2	Blu
Train	Traina	k1gFnPc2	Traina
<g/>
.	.	kIx.	.
</s>
<s>
Příměstskou	příměstský	k2eAgFnSc4d1	příměstská
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
provozuje	provozovat	k5eAaImIp3nS	provozovat
společnost	společnost	k1gFnSc1	společnost
Metrorail	Metroraila	k1gFnPc2	Metroraila
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
celkem	celkem	k6eAd1	celkem
96	[number]	k4	96
zastávek	zastávka	k1gFnPc2	zastávka
příměstských	příměstský	k2eAgInPc2d1	příměstský
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
vedou	vést	k5eAaImIp3nP	vést
tři	tři	k4xCgFnPc1	tři
důležité	důležitý	k2eAgFnPc1d1	důležitá
dálnice	dálnice	k1gFnPc1	dálnice
(	(	kIx(	(
<g/>
national	nationat	k5eAaImAgInS	nationat
road	road	k6eAd1	road
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
N1	N1	k1gFnSc1	N1
směr	směr	k1gInSc1	směr
Bloemfontein	Bloemfontein	k1gInSc1	Bloemfontein
<g/>
,	,	kIx,	,
Johannesburg	Johannesburg	k1gInSc1	Johannesburg
<g/>
,	,	kIx,	,
Pretoria	Pretorium	k1gNnPc1	Pretorium
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Zimbabwe	Zimbabw	k1gInSc2	Zimbabw
<g/>
;	;	kIx,	;
N2	N2	k1gFnSc1	N2
směr	směr	k1gInSc4	směr
Port	port	k1gInSc1	port
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
<g/>
,	,	kIx,	,
East	East	k1gMnSc1	East
London	London	k1gMnSc1	London
a	a	k8xC	a
Durban	Durban	k1gMnSc1	Durban
a	a	k8xC	a
N7	N7	k1gFnSc4	N7
spojující	spojující	k2eAgFnSc4d1	spojující
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
s	s	k7c7	s
provincií	provincie	k1gFnSc7	provincie
Northern	Northerno	k1gNnPc2	Northerno
Cape	capat	k5eAaImIp3nS	capat
a	a	k8xC	a
Namibií	Namibie	k1gFnSc7	Namibie
<g/>
.	.	kIx.	.
</s>
<s>
N1	N1	k4	N1
a	a	k8xC	a
N2	N2	k1gFnPc1	N2
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
Central	Central	k1gFnSc6	Central
Business	business	k1gInSc1	business
District	Districta	k1gFnPc2	Districta
<g/>
,	,	kIx,	,
N1	N1	k1gFnPc2	N1
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
N2	N2	k1gFnSc1	N2
jihovýchodním	jihovýchodní	k2eAgMnSc7d1	jihovýchodní
okolo	okolo	k6eAd1	okolo
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gMnSc1	Town
International	International	k1gFnSc2	International
Airport	Airport	k1gInSc1	Airport
<g/>
.	.	kIx.	.
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
vlastní	vlastní	k2eAgFnSc4d1	vlastní
síť	síť	k1gFnSc4	síť
dálnic	dálnice	k1gFnPc2	dálnice
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
M-roads	Moads	k1gInSc1	M-roads
<g/>
)	)	kIx)	)
propojujících	propojující	k2eAgInPc2d1	propojující
různé	různý	k2eAgFnPc4d1	různá
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Městskou	městský	k2eAgFnSc4d1	městská
autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
Městě	město	k1gNnSc6	město
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
společnost	společnost	k1gFnSc1	společnost
Golden	Goldna	k1gFnPc2	Goldna
Arrow	Arrow	k1gFnSc1	Arrow
Bus	bus	k1gInSc1	bus
Services	Services	k1gInSc1	Services
<g/>
,	,	kIx,	,
dálkové	dálkový	k2eAgFnSc2d1	dálková
meziměstské	meziměstský	k2eAgFnSc2d1	meziměstská
linky	linka	k1gFnSc2	linka
provozuje	provozovat	k5eAaImIp3nS	provozovat
vícero	vícero	k1gNnSc1	vícero
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgNnPc2d1	ostatní
měst	město	k1gNnPc2	město
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
Městě	město	k1gNnSc6	město
povoleno	povolit	k5eAaPmNgNnS	povolit
taxikářům	taxikář	k1gMnPc3	taxikář
jezdit	jezdit	k5eAaImF	jezdit
volně	volně	k6eAd1	volně
po	po	k7c6	po
městě	město	k1gNnSc6	město
a	a	k8xC	a
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
pasažéry	pasažér	k1gMnPc4	pasažér
<g/>
,	,	kIx,	,
smějí	smát	k5eAaImIp3nP	smát
pouze	pouze	k6eAd1	pouze
přijet	přijet	k5eAaPmF	přijet
na	na	k7c4	na
předem	předem	k6eAd1	předem
sjednané	sjednaný	k2eAgNnSc4d1	sjednané
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
proto	proto	k8xC	proto
vzkvétá	vzkvétat	k5eAaImIp3nS	vzkvétat
systém	systém	k1gInSc4	systém
minibusových	minibusový	k2eAgNnPc2d1	minibusový
taxi	taxi	k1gNnPc2	taxi
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jezdí	jezdit	k5eAaImIp3nP	jezdit
po	po	k7c6	po
určených	určený	k2eAgFnPc6d1	určená
trasách	trasa	k1gFnPc6	trasa
a	a	k8xC	a
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
zastavit	zastavit	k5eAaPmF	zastavit
pokynem	pokyn	k1gInSc7	pokyn
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k8xC	ale
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přináší	přinášet	k5eAaImIp3nS	přinášet
jisté	jistý	k2eAgFnPc4d1	jistá
potíže	potíž	k1gFnPc4	potíž
-	-	kIx~	-
řidiči	řidič	k1gMnPc1	řidič
za	za	k7c7	za
minibusem	minibus	k1gInSc7	minibus
často	často	k6eAd1	často
nestačí	stačit	k5eNaBmIp3nS	stačit
zareagovat	zareagovat	k5eAaPmF	zareagovat
a	a	k8xC	a
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
vzniká	vznikat	k5eAaImIp3nS	vznikat
množství	množství	k1gNnSc1	množství
dopravních	dopravní	k2eAgFnPc2d1	dopravní
nehod	nehoda	k1gFnPc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Minibusy	minibus	k1gInPc1	minibus
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
přeplněné	přeplněný	k2eAgFnPc1d1	přeplněná
a	a	k8xC	a
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
technickém	technický	k2eAgInSc6d1	technický
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc2d1	dopravní
nehody	nehoda	k1gFnSc2	nehoda
těchto	tento	k3xDgNnPc2	tento
vozidel	vozidlo	k1gNnPc2	vozidlo
bývají	bývat	k5eAaImIp3nP	bývat
proto	proto	k8xC	proto
tragické	tragický	k2eAgNnSc1d1	tragické
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
dopravy	doprava	k1gFnSc2	doprava
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
využívá	využívat	k5eAaImIp3nS	využívat
mnoho	mnoho	k4c1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Majitelé	majitel	k1gMnPc1	majitel
většinou	většinou	k6eAd1	většinou
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
a	a	k8xC	a
provozují	provozovat	k5eAaImIp3nP	provozovat
více	hodně	k6eAd2	hodně
minibusů	minibus	k1gInPc2	minibus
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vypukají	vypukat	k5eAaImIp3nP	vypukat
rozepře	rozepře	k1gFnPc1	rozepře
kvůli	kvůli	k7c3	kvůli
obsluze	obsluha	k1gFnSc3	obsluha
nejlukrativnějších	lukrativní	k2eAgFnPc2d3	nejlukrativnější
tras	trasa	k1gFnPc2	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Johannesburgem	Johannesburg	k1gInSc7	Johannesburg
a	a	k8xC	a
Pretorií	Pretorie	k1gFnSc7	Pretorie
patří	patřit	k5eAaImIp3nS	patřit
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
k	k	k7c3	k
nejdůležitějším	důležitý	k2eAgInPc3d3	nejdůležitější
univerzitním	univerzitní	k2eAgInPc3d1	univerzitní
centrům	centr	k1gInPc3	centr
Jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitě	kvalita	k1gFnSc3	kvalita
univerzitního	univerzitní	k2eAgNnSc2d1	univerzitní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
relativně	relativně	k6eAd1	relativně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
úroveň	úroveň	k1gFnSc1	úroveň
financování	financování	k1gNnSc2	financování
z	z	k7c2	z
veřejných	veřejný	k2eAgInPc2d1	veřejný
i	i	k8xC	i
soukromých	soukromý	k2eAgInPc2d1	soukromý
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
apartheidu	apartheid	k1gInSc2	apartheid
prošlo	projít	k5eAaPmAgNnS	projít
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
restrukturalizací	restrukturalizace	k1gFnPc2	restrukturalizace
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
profitovaly	profitovat	k5eAaBmAgInP	profitovat
zvláště	zvláště	k9	zvláště
"	"	kIx"	"
<g/>
nebělošské	bělošský	k2eNgFnPc4d1	bělošský
<g/>
"	"	kIx"	"
školy	škola	k1gFnPc4	škola
jako	jako	k8xC	jako
University	universita	k1gFnPc4	universita
of	of	k?	of
the	the	k?	the
Western	Western	kA	Western
Cape	capat	k5eAaImIp3nS	capat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejstarší	starý	k2eAgFnSc7d3	nejstarší
jihoafrickou	jihoafrický	k2eAgFnSc7d1	Jihoafrická
univerzitou	univerzita	k1gFnSc7	univerzita
je	být	k5eAaImIp3nS	být
University	universita	k1gFnSc2	universita
of	of	k?	of
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gInSc1	Town
(	(	kIx(	(
<g/>
UCT	UCT	kA	UCT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
tehdy	tehdy	k6eAd1	tehdy
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
South	South	k1gInSc1	South
African	Africana	k1gFnPc2	Africana
College	Colleg	k1gInSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
hlavní	hlavní	k2eAgInSc1d1	hlavní
kampus	kampus	k1gInSc1	kampus
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Stolové	stolový	k2eAgFnSc2d1	stolová
hory	hora	k1gFnSc2	hora
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Devil	Devila	k1gFnPc2	Devila
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Peak	Peaka	k1gFnPc2	Peaka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
fakult	fakulta	k1gFnPc2	fakulta
<g/>
:	:	kIx,	:
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
(	(	kIx(	(
<g/>
Commerce	Commerka	k1gFnSc6	Commerka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
technickou	technický	k2eAgFnSc7d1	technická
(	(	kIx(	(
<g/>
Engineering	Engineering	k1gInSc4	Engineering
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékařskou	lékařský	k2eAgFnSc7d1	lékařská
(	(	kIx(	(
<g/>
Health	Healtha	k1gFnPc2	Healtha
Sciences	Sciences	k1gInSc1	Sciences
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
humanitní	humanitní	k2eAgMnSc1d1	humanitní
(	(	kIx(	(
<g/>
Humanities	Humanities	k1gMnSc1	Humanities
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právnickou	právnický	k2eAgFnSc7d1	právnická
(	(	kIx(	(
<g/>
Law	Law	k1gFnSc7	Law
<g/>
)	)	kIx)	)
a	a	k8xC	a
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
(	(	kIx(	(
<g/>
Science	Science	k1gFnSc1	Science
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
zapsaných	zapsaný	k2eAgInPc2d1	zapsaný
21	[number]	k4	21
713	[number]	k4	713
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
bývalé	bývalý	k2eAgMnPc4d1	bývalý
absolventy	absolvent	k1gMnPc4	absolvent
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
tři	tři	k4xCgMnPc1	tři
držitelé	držitel	k1gMnPc1	držitel
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
<g/>
:	:	kIx,	:
Aaron	Aaron	k1gMnSc1	Aaron
Klug	Klug	k1gMnSc1	Klug
(	(	kIx(	(
<g/>
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Allan	Allan	k1gMnSc1	Allan
McLeod	McLeoda	k1gFnPc2	McLeoda
Cormack	Cormack	k1gMnSc1	Cormack
(	(	kIx(	(
<g/>
lékařství	lékařství	k1gNnSc1	lékařství
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
a	a	k8xC	a
J.	J.	kA	J.
M.	M.	kA	M.
Coetzee	Coetzee	k1gFnSc1	Coetzee
(	(	kIx(	(
<g/>
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
současné	současný	k2eAgMnPc4d1	současný
profesory	profesor	k1gMnPc4	profesor
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
univerzitě	univerzita	k1gFnSc6	univerzita
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
například	například	k6eAd1	například
George	George	k1gFnSc1	George
Ellis	Ellis	k1gFnSc1	Ellis
<g/>
,	,	kIx,	,
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
Stephena	Stephen	k1gMnSc2	Stephen
Hawkinga	Hawking	k1gMnSc2	Hawking
<g/>
.	.	kIx.	.
</s>
<s>
University	universita	k1gFnPc4	universita
of	of	k?	of
Cape	capat	k5eAaImIp3nS	capat
Town	Town	k1gInSc1	Town
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
university	universita	k1gFnPc4	universita
na	na	k7c6	na
africkém	africký	k2eAgInSc6d1	africký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
Městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
nachází	nacházet	k5eAaImIp3nS	nacházet
University	universita	k1gFnPc4	universita
of	of	k?	of
the	the	k?	the
Western	Western	kA	Western
Cape	capat	k5eAaImIp3nS	capat
(	(	kIx(	(
<g/>
UWC	UWC	kA	UWC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cape	capat	k5eAaImIp3nS	capat
Peninsula	Peninsula	k1gFnSc1	Peninsula
University	universita	k1gFnSc2	universita
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
(	(	kIx(	(
<g/>
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
sloučením	sloučení	k1gNnSc7	sloučení
Cape	capat	k5eAaImIp3nS	capat
Technikon	Technikon	k1gNnSc1	Technikon
s	s	k7c7	s
Peninsula	Peninsul	k1gMnSc2	Peninsul
Technikon	Technikona	k1gFnPc2	Technikona
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stellenbosch	Stellenbosch	k1gInSc4	Stellenbosch
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
její	její	k3xOp3gInPc1	její
campusy	campus	k1gInPc1	campus
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
blíže	blízce	k6eAd2	blízce
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Tygerberg	Tygerberg	k1gInSc1	Tygerberg
Faculty	Facult	k1gInPc1	Facult
of	of	k?	of
Health	Health	k1gInSc1	Health
Sciences	Sciences	k1gInSc1	Sciences
a	a	k8xC	a
Bellville	Bellville	k1gFnSc1	Bellville
Business	business	k1gInSc1	business
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgFnSc7d1	kvalitní
obchodní	obchodní	k2eAgFnSc7d1	obchodní
školou	škola	k1gFnSc7	škola
(	(	kIx(	(
<g/>
business	business	k1gInSc1	business
school	school	k1gInSc1	school
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
Graduate	Graduat	k1gInSc5	Graduat
School	School	k1gInSc4	School
of	of	k?	of
Business	business	k1gInSc1	business
při	při	k7c6	při
University	universita	k1gFnPc1	universita
of	of	k?	of
the	the	k?	the
Western	Western	kA	Western
Cape	capat	k5eAaImIp3nS	capat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
komplexu	komplex	k1gInSc2	komplex
Victoria	Victorium	k1gNnSc2	Victorium
&	&	k?	&
Alfred	Alfred	k1gMnSc1	Alfred
Waterfront	Waterfront	k1gMnSc1	Waterfront
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
anglické	anglický	k2eAgFnSc2d1	anglická
jazykové	jazykový	k2eAgFnSc2d1	jazyková
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
Language	language	k1gFnSc1	language
Schools	Schoolsa	k1gFnPc2	Schoolsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nP	dařit
přilákat	přilákat	k5eAaPmF	přilákat
zájemce	zájemce	k1gMnPc4	zájemce
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
díky	díky	k7c3	díky
důrazu	důraz	k1gInSc2	důraz
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
výuky	výuka	k1gFnSc2	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
angličtiny	angličtina	k1gFnSc2	angličtina
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
velice	velice	k6eAd1	velice
výnosným	výnosný	k2eAgNnPc3d1	výnosné
odvětvím	odvětví	k1gNnPc3	odvětví
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přispívá	přispívat	k5eAaImIp3nS	přispívat
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Antverpy	Antverpy	k1gFnPc1	Antverpy
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Cáchy	Cáchy	k1gFnPc1	Cáchy
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Funchal	Funchal	k1gFnSc1	Funchal
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Haifa	Haifa	k1gFnSc1	Haifa
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Chang-čou	Chang-čou	k1gNnSc1	Chang-čou
<g/>
,	,	kIx,	,
China	China	k1gFnSc1	China
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Luanda	Luanda	k1gFnSc1	Luanda
<g/>
,	,	kIx,	,
Angola	Angola	k1gFnSc1	Angola
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Maputo	Maput	k2eAgNnSc1d1	Maputo
<g/>
,	,	kIx,	,
Mosambik	Mosambik	k1gInSc1	Mosambik
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Nice	Nice	k1gFnSc1	Nice
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kapské	kapský	k2eAgFnSc2d1	kapská
Město	město	k1gNnSc4	město
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Galerie	galerie	k1gFnSc2	galerie
Kapské	kapský	k2eAgNnSc1d1	Kapské
Město	město	k1gNnSc1	město
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Generální	generální	k2eAgInSc1d1	generální
konzulát	konzulát	k1gInSc1	konzulát
ČR	ČR	kA	ČR
v	v	k7c6	v
Kapském	kapský	k2eAgNnSc6d1	Kapské
Městě	město	k1gNnSc6	město
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Kapského	kapský	k2eAgNnSc2d1	Kapské
Města	město	k1gNnSc2	město
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
provincie	provincie	k1gFnSc2	provincie
Západní	západní	k2eAgInSc4d1	západní
Kapsko	Kapsko	k1gNnSc4	Kapsko
</s>
