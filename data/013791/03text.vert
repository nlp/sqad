<s>
Lineární	lineární	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
B	B	kA
</s>
<s>
Hliněná	hliněný	k2eAgFnSc1d1
destička	destička	k1gFnSc1
s	s	k7c7
lineárním	lineární	k2eAgNnSc7d1
písmem	písmo	k1gNnSc7
B	B	kA
</s>
<s>
Lineární	lineární	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
B	B	kA
je	být	k5eAaImIp3nS
písmo	písmo	k1gNnSc1
užívané	užívaný	k2eAgNnSc1d1
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
pro	pro	k7c4
psaní	psaní	k1gNnSc4
v	v	k7c6
pozdní	pozdní	k2eAgFnSc6d1
mykénské	mykénský	k2eAgFnSc6d1
civilizaci	civilizace	k1gFnSc6
v	v	k7c6
rané	raný	k2eAgFnSc6d1
formě	forma	k1gFnSc6
řečtiny	řečtina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
tabulek	tabulka	k1gFnPc2
napsaných	napsaný	k2eAgFnPc2d1
v	v	k7c6
lineárním	lineární	k2eAgNnSc6d1
písmu	písmo	k1gNnSc6
B	B	kA
byla	být	k5eAaImAgFnS
nalezena	nalézt	k5eAaBmNgFnS,k5eAaPmNgFnS
v	v	k7c6
Knóssu	Knóss	k1gInSc6
<g/>
,	,	kIx,
Kydonii	Kydonie	k1gFnSc6
<g/>
,	,	kIx,
Pylu	pyl	k1gInSc6
<g/>
,	,	kIx,
Thébách	Théby	k1gFnPc6
a	a	k8xC
Mykénách	Mykény	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časově	časově	k6eAd1
předcházelo	předcházet	k5eAaImAgNnS
řecké	řecký	k2eAgFnSc3d1
alfabetě	alfabeta	k1gFnSc3
o	o	k7c6
několik	několik	k4yIc4
století	století	k1gNnPc2
<g/>
,	,	kIx,
vymizelo	vymizet	k5eAaPmAgNnS
s	s	k7c7
pádem	pád	k1gInSc7
mykénské	mykénský	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
a	a	k8xC
bylo	být	k5eAaImAgNnS
zcela	zcela	k6eAd1
zapomenuto	zapomnět	k5eAaImNgNnS,k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
znám	znám	k2eAgInSc4d1
žádný	žádný	k3yNgInSc4
důkaz	důkaz	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
obyvatelé	obyvatel	k1gMnPc1
Řecka	Řecko	k1gNnSc2
v	v	k7c6
období	období	k1gNnSc6
od	od	k7c2
pádu	pád	k1gInSc2
mykénské	mykénský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
až	až	k9
do	do	k7c2
vzniku	vznik	k1gInSc2
alfabety	alfabeta	k1gFnSc2
užívali	užívat	k5eAaImAgMnP
nějakého	nějaký	k3yIgNnSc2
písma	písmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vztah	vztah	k1gInSc1
k	k	k7c3
dalším	další	k2eAgFnPc3d1
egejským	egejský	k2eAgFnPc3d1
písmům	písmo	k1gNnPc3
</s>
<s>
Lineární	lineární	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
B	B	kA
bylo	být	k5eAaImAgNnS
jedno	jeden	k4xCgNnSc1
z	z	k7c2
pěti	pět	k4xCc2
písem	písmo	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
v	v	k7c6
egejské	egejský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
existovaly	existovat	k5eAaImAgFnP
před	před	k7c7
rozšířením	rozšíření	k1gNnSc7
řecké	řecký	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgInSc1d3
bylo	být	k5eAaImAgNnS
Krétské	krétský	k2eAgNnSc4d1
hieroglyfické	hieroglyfický	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yRgNnSc2,k3yQgNnSc2,k3yIgNnSc2
vyšlo	vyjít	k5eAaPmAgNnS
lineární	lineární	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
A	A	kA
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
vývojová	vývojový	k2eAgFnSc1d1
linie	linie	k1gFnSc1
rozdvojuje	rozdvojovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západní	západní	k2eAgFnSc6d1
větvi	větev	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
přechodu	přechod	k1gInSc3
k	k	k7c3
lineárnímu	lineární	k2eAgNnSc3d1
písmu	písmo	k1gNnSc3
B	B	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
je	být	k5eAaImIp3nS
doloženo	doložit	k5eAaPmNgNnS
na	na	k7c6
Krétě	Kréta	k1gFnSc6
i	i	k8xC
v	v	k7c6
pevninském	pevninský	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
Kypru	Kypr	k1gInSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
rozvíjí	rozvíjet	k5eAaImIp3nS
písmo	písmo	k1gNnSc4
kypersko-mínojské	kypersko-mínojský	k2eAgNnSc4d1
a	a	k8xC
později	pozdě	k6eAd2
kyperské	kyperský	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stranou	stranou	k6eAd1
celé	celá	k1gFnSc2
této	tento	k3xDgFnSc2
vývojové	vývojový	k2eAgFnSc2d1
linie	linie	k1gFnSc2
stojí	stát	k5eAaImIp3nS
disk	disk	k1gInSc4
z	z	k7c2
paláce	palác	k1gInSc2
ve	v	k7c6
Faistu	Faist	k1gInSc6
<g/>
,	,	kIx,
popsaný	popsaný	k2eAgInSc1d1
po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
zvláštním	zvláštní	k2eAgNnSc7d1
písmem	písmo	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
nemá	mít	k5eNaImIp3nS
nikde	nikde	k6eAd1
jinde	jinde	k6eAd1
obdobu	obdoba	k1gFnSc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lineární	lineární	k2eAgInSc4d1
písmo	písmo	k1gNnSc1
B	B	kA
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
lineárního	lineární	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
A	a	k9
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
částečně	částečně	k6eAd1
přizpůsobené	přizpůsobený	k2eAgNnSc1d1
pro	pro	k7c4
zápis	zápis	k1gInSc4
řečtiny	řečtina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozluštění	rozluštění	k1gNnSc1
písma	písmo	k1gNnSc2
</s>
<s>
Základ	základ	k1gInSc1
písma	písmo	k1gNnSc2
rozluštil	rozluštit	k5eAaPmAgInS
jazykově	jazykově	k6eAd1
nadaný	nadaný	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
se	se	k3xPyFc4
zájmem	zájem	k1gInSc7
o	o	k7c4
egejskou	egejský	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
Michael	Michael	k1gMnSc1
Ventris	Ventris	k1gMnSc1
v	v	k7c6
letech	let	k1gInPc6
1950	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ventris	Ventris	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
slabičnou	slabičný	k2eAgFnSc4d1
mřížku	mřížka	k1gFnSc4
a	a	k8xC
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
fonetické	fonetický	k2eAgNnSc4d1
slabičné	slabičný	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
rozluštil	rozluštit	k5eAaPmAgMnS
první	první	k4xOgInPc4
znaky	znak	k1gInPc4
písma	písmo	k1gNnSc2
<g/>
,	,	kIx,
překvapeně	překvapeně	k6eAd1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
formu	forma	k1gFnSc4
starořečtiny	starořečtina	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
dokázal	dokázat	k5eAaPmAgMnS
Waceovu	Waceův	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
mykénské	mykénský	k2eAgFnSc2d1
expanze	expanze	k1gFnSc2
na	na	k7c4
Krétu	Kréta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
dalším	další	k2eAgNnSc6d1
luštění	luštění	k1gNnSc6
písma	písmo	k1gNnSc2
spolupracoval	spolupracovat	k5eAaImAgInS
s	s	k7c7
mladým	mladý	k2eAgMnSc7d1
filologem	filolog	k1gMnSc7
Johnem	John	k1gMnSc7
Chadwickem	Chadwick	k1gInSc7
v	v	k7c6
letech	let	k1gInPc6
1952	#num#	k4
<g/>
–	–	k?
<g/>
1955	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
závěry	závěr	k1gInPc4
dešifrování	dešifrování	k1gNnSc2
lineárního	lineární	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
B	B	kA
byly	být	k5eAaImAgInP
nezávisle	závisle	k6eNd1
potvrzeny	potvrdit	k5eAaPmNgInP
americkým	americký	k2eAgInSc7d1
archeologem	archeolog	k1gMnSc7
Blegenem	Blegen	k1gInSc7
na	na	k7c6
základě	základ	k1gInSc6
výkladu	výklad	k1gInSc2
hliněné	hliněný	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
nalezené	nalezený	k2eAgFnSc2d1
v	v	k7c6
Pylu	pyl	k1gInSc6
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
kompletního	kompletní	k2eAgInSc2d1
čtecího	čtecí	k2eAgInSc2d1
klíče	klíč	k1gInSc2
pro	pro	k7c4
toto	tento	k3xDgNnSc4
písmo	písmo	k1gNnSc4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
byl	být	k5eAaImAgInS
významným	významný	k2eAgMnSc7d1
odborníkem	odborník	k1gMnSc7
a	a	k8xC
znalcem	znalec	k1gMnSc7
lineárního	lineární	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
B	B	kA
profesor	profesor	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Bartoněk	Bartoněk	k1gMnSc1
působící	působící	k2eAgMnSc1d1
na	na	k7c6
Filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Lineární	lineární	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
B	B	kA
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
87	#num#	k4
znaků	znak	k1gInPc2
pro	pro	k7c4
slabiky	slabika	k1gFnPc4
a	a	k8xC
mnoha	mnoho	k4c2
sémantických	sémantický	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
sémantické	sémantický	k2eAgInPc4d1
(	(	kIx(
<g/>
významové	významový	k2eAgInPc4d1
<g/>
)	)	kIx)
znaky	znak	k1gInPc4
(	(	kIx(
<g/>
logogramy	logogram	k1gInPc4
<g/>
)	)	kIx)
zastupují	zastupovat	k5eAaImIp3nP
objekty	objekt	k1gInPc4
nebo	nebo	k8xC
zboží	zboží	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
neměly	mít	k5eNaImAgInP
fonetickou	fonetický	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
a	a	k8xC
nikdy	nikdy	k6eAd1
nebyly	být	k5eNaImAgInP
užívány	užívat	k5eAaImNgInP
jako	jako	k8xC,k8xS
slova	slovo	k1gNnPc1
ve	v	k7c6
větách	věta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řečtina	řečtina	k1gFnSc1
lineárního	lineární	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
B	B	kA
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
zvláštní	zvláštní	k2eAgMnSc1d1
<g/>
,	,	kIx,
Ventris	Ventris	k1gFnSc1
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
užil	užít	k5eAaPmAgMnS
označení	označení	k1gNnSc1
mykénština	mykénština	k1gFnSc1
<g/>
,	,	kIx,
protože	protože	k8xS
ač	ač	k8xS
je	být	k5eAaImIp3nS
nepochybně	pochybně	k6eNd1
řeckým	řecký	k2eAgInSc7d1
dialektem	dialekt	k1gInSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
podobná	podobný	k2eAgFnSc1d1
arkádo-kyperským	arkádo-kyperský	k2eAgInPc3d1
dialektům	dialekt	k1gInPc3
<g/>
,	,	kIx,
s	s	k7c7
prvky	prvek	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
v	v	k7c6
pozdějších	pozdní	k2eAgNnPc6d2
Homérových	Homérův	k2eAgNnPc6d1
dílech	dílo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Rozpoznané	rozpoznaný	k2eAgInPc1d1
znaky	znak	k1gInPc1
typu	typ	k1gInSc2
V	V	kA
<g/>
,	,	kIx,
CV	CV	kA
<g/>
[	[	kIx(
<g/>
Pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
-a	-a	k?
</s>
<s>
-e	-e	k?
</s>
<s>
-i	-i	k?
</s>
<s>
-o	-o	k?
</s>
<s>
-u	-u	k?
</s>
<s>
𐀀	𐀀	k?
</s>
<s>
a	a	k8xC
</s>
<s>
*	*	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
𐀁	𐀁	k?
</s>
<s>
e	e	k0
</s>
<s>
*	*	kIx~
<g/>
38	#num#	k4
</s>
<s>
𐀂	𐀂	k?
</s>
<s>
i	i	k9
</s>
<s>
*	*	kIx~
<g/>
28	#num#	k4
</s>
<s>
𐀃	𐀃	k?
</s>
<s>
o	o	k7c6
</s>
<s>
*	*	kIx~
<g/>
61	#num#	k4
</s>
<s>
𐀄	𐀄	k?
</s>
<s>
u	u	k7c2
</s>
<s>
*	*	kIx~
<g/>
10	#num#	k4
</s>
<s>
d-	d-	k?
</s>
<s>
𐀅	𐀅	k?
</s>
<s>
da	da	k?
</s>
<s>
*	*	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
𐀆	𐀆	k?
</s>
<s>
de	de	k?
</s>
<s>
*	*	kIx~
<g/>
45	#num#	k4
</s>
<s>
𐀇	𐀇	k?
</s>
<s>
di	di	k?
</s>
<s>
*	*	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
𐀈	𐀈	k?
</s>
<s>
do	do	k7c2
</s>
<s>
*	*	kIx~
<g/>
14	#num#	k4
</s>
<s>
𐀉	𐀉	k?
</s>
<s>
du	du	k?
</s>
<s>
*	*	kIx~
<g/>
51	#num#	k4
</s>
<s>
j-	j-	k?
</s>
<s>
𐀊	𐀊	k?
</s>
<s>
ja	ja	k?
</s>
<s>
*	*	kIx~
<g/>
57	#num#	k4
</s>
<s>
𐀋	𐀋	k?
</s>
<s>
je	být	k5eAaImIp3nS
</s>
<s>
*	*	kIx~
<g/>
46	#num#	k4
</s>
<s>
𐀍	𐀍	k?
</s>
<s>
jo	jo	k9
</s>
<s>
*	*	kIx~
<g/>
36	#num#	k4
</s>
<s>
k-	k-	k?
</s>
<s>
𐀏	𐀏	k?
</s>
<s>
ka	ka	k?
</s>
<s>
*	*	kIx~
<g/>
77	#num#	k4
</s>
<s>
𐀐	𐀐	k?
</s>
<s>
ke	k	k7c3
</s>
<s>
*	*	kIx~
<g/>
44	#num#	k4
</s>
<s>
𐀑	𐀑	k?
</s>
<s>
ki	ki	k?
</s>
<s>
*	*	kIx~
<g/>
67	#num#	k4
</s>
<s>
𐀒	𐀒	k?
</s>
<s>
ko	ko	k?
</s>
<s>
*	*	kIx~
<g/>
70	#num#	k4
</s>
<s>
𐀓	𐀓	k?
</s>
<s>
ku	k	k7c3
</s>
<s>
*	*	kIx~
<g/>
81	#num#	k4
</s>
<s>
m-	m-	k?
</s>
<s>
𐀔	𐀔	k?
</s>
<s>
ma	ma	k?
</s>
<s>
*	*	kIx~
<g/>
80	#num#	k4
</s>
<s>
𐀕	𐀕	k?
</s>
<s>
me	me	k?
</s>
<s>
*	*	kIx~
<g/>
13	#num#	k4
</s>
<s>
𐀖	𐀖	k?
</s>
<s>
mi	já	k3xPp1nSc3
</s>
<s>
*	*	kIx~
<g/>
73	#num#	k4
</s>
<s>
𐀗	𐀗	k?
</s>
<s>
mo	mo	k?
</s>
<s>
*	*	kIx~
<g/>
15	#num#	k4
</s>
<s>
𐀘	𐀘	k?
</s>
<s>
mu	on	k3xPp3gMnSc3
</s>
<s>
*	*	kIx~
<g/>
23	#num#	k4
</s>
<s>
n-	n-	k?
</s>
<s>
𐀙	𐀙	k?
</s>
<s>
na	na	k7c6
</s>
<s>
*	*	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
𐀚	𐀚	k?
</s>
<s>
ne	ne	k9
</s>
<s>
*	*	kIx~
<g/>
24	#num#	k4
</s>
<s>
𐀛	𐀛	k?
</s>
<s>
ni	on	k3xPp3gFnSc4
</s>
<s>
*	*	kIx~
<g/>
30	#num#	k4
</s>
<s>
𐀜	𐀜	k?
</s>
<s>
no	no	k9
</s>
<s>
*	*	kIx~
<g/>
52	#num#	k4
</s>
<s>
𐀝	𐀝	k?
</s>
<s>
nu	nu	k9
</s>
<s>
*	*	kIx~
<g/>
55	#num#	k4
</s>
<s>
p-	p-	k?
</s>
<s>
𐀞	𐀞	k?
</s>
<s>
pa	pa	k0
</s>
<s>
*	*	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
𐀟	𐀟	k?
</s>
<s>
pe	pe	k?
</s>
<s>
*	*	kIx~
<g/>
72	#num#	k4
</s>
<s>
𐀠	𐀠	k?
</s>
<s>
pi	pi	k0
</s>
<s>
*	*	kIx~
<g/>
39	#num#	k4
</s>
<s>
𐀡	𐀡	k?
</s>
<s>
po	po	k7c6
</s>
<s>
*	*	kIx~
<g/>
11	#num#	k4
</s>
<s>
𐀢	𐀢	k?
</s>
<s>
pu	pu	k?
</s>
<s>
*	*	kIx~
<g/>
50	#num#	k4
</s>
<s>
q-	q-	k?
</s>
<s>
𐀣	𐀣	k?
</s>
<s>
qa	qa	k?
</s>
<s>
*	*	kIx~
<g/>
16	#num#	k4
</s>
<s>
𐀤	𐀤	k?
</s>
<s>
qe	qe	k?
</s>
<s>
*	*	kIx~
<g/>
78	#num#	k4
</s>
<s>
𐀥	𐀥	k?
</s>
<s>
qi	qi	k?
</s>
<s>
*	*	kIx~
<g/>
21	#num#	k4
</s>
<s>
𐀦	𐀦	k?
</s>
<s>
qo	qo	k?
</s>
<s>
*	*	kIx~
<g/>
32	#num#	k4
</s>
<s>
r-	r-	k?
</s>
<s>
𐀨	𐀨	k?
</s>
<s>
ra	ra	k0
</s>
<s>
*	*	kIx~
<g/>
60	#num#	k4
</s>
<s>
𐀩	𐀩	k?
</s>
<s>
re	re	k9
</s>
<s>
*	*	kIx~
<g/>
27	#num#	k4
</s>
<s>
𐀪	𐀪	k?
</s>
<s>
ri	ri	k?
</s>
<s>
*	*	kIx~
<g/>
53	#num#	k4
</s>
<s>
𐀫	𐀫	k?
</s>
<s>
ro	ro	k?
</s>
<s>
*	*	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
𐀬	𐀬	k?
</s>
<s>
ru	ru	k?
</s>
<s>
*	*	kIx~
<g/>
26	#num#	k4
</s>
<s>
s-	s-	k?
</s>
<s>
𐀭	𐀭	k?
</s>
<s>
sa	sa	k?
</s>
<s>
*	*	kIx~
<g/>
31	#num#	k4
</s>
<s>
𐀮	𐀮	k?
</s>
<s>
se	s	k7c7
</s>
<s>
*	*	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
𐀯	𐀯	k?
</s>
<s>
si	se	k3xPyFc3
</s>
<s>
*	*	kIx~
<g/>
41	#num#	k4
</s>
<s>
𐀰	𐀰	k?
</s>
<s>
so	so	k?
</s>
<s>
*	*	kIx~
<g/>
12	#num#	k4
</s>
<s>
𐀱	𐀱	k?
</s>
<s>
su	su	k?
</s>
<s>
*	*	kIx~
<g/>
58	#num#	k4
</s>
<s>
t-	t-	k?
</s>
<s>
𐀲	𐀲	k?
</s>
<s>
ta	ten	k3xDgFnSc1
</s>
<s>
*	*	kIx~
<g/>
59	#num#	k4
</s>
<s>
𐀳	𐀳	k?
</s>
<s>
te	te	k?
</s>
<s>
*	*	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
𐀴	𐀴	k?
</s>
<s>
ti	ten	k3xDgMnPc1
</s>
<s>
*	*	kIx~
<g/>
37	#num#	k4
</s>
<s>
𐀵	𐀵	k?
</s>
<s>
to	ten	k3xDgNnSc1
</s>
<s>
*	*	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
𐀶	𐀶	k?
</s>
<s>
tu	tu	k6eAd1
</s>
<s>
*	*	kIx~
<g/>
69	#num#	k4
</s>
<s>
w-	w-	k?
</s>
<s>
𐀷	𐀷	k?
</s>
<s>
wa	wa	k?
</s>
<s>
*	*	kIx~
<g/>
54	#num#	k4
</s>
<s>
𐀸	𐀸	k?
</s>
<s>
we	we	k?
</s>
<s>
*	*	kIx~
<g/>
75	#num#	k4
</s>
<s>
𐀹	𐀹	k?
</s>
<s>
wi	wi	k?
</s>
<s>
*	*	kIx~
<g/>
40	#num#	k4
</s>
<s>
𐀺	𐀺	k?
</s>
<s>
wo	wo	k?
</s>
<s>
*	*	kIx~
<g/>
42	#num#	k4
</s>
<s>
z-	z-	k?
</s>
<s>
𐀼	𐀼	k?
</s>
<s>
za	za	k7c4
</s>
<s>
*	*	kIx~
<g/>
17	#num#	k4
</s>
<s>
𐀽	𐀽	k?
</s>
<s>
ze	z	k7c2
</s>
<s>
*	*	kIx~
<g/>
74	#num#	k4
</s>
<s>
𐀿	𐀿	k?
</s>
<s>
zo	zo	k?
</s>
<s>
*	*	kIx~
<g/>
20	#num#	k4
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Lineární	lineární	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
B	B	kA
bylo	být	k5eAaImAgNnS
používáno	používán	k2eAgNnSc1d1
pouze	pouze	k6eAd1
pro	pro	k7c4
byrokracii	byrokracie	k1gFnSc4
a	a	k8xC
inventarizaci	inventarizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavé	zajímavý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
objevených	objevený	k2eAgFnPc6d1
tabulkách	tabulka	k1gFnPc6
(	(	kIx(
<g/>
necelých	celý	k2eNgNnPc6d1
šest	šest	k4xCc4
tisíc	tisíc	k4xCgInSc4
<g/>
)	)	kIx)
pracovalo	pracovat	k5eAaImAgNnS
pravděpodobně	pravděpodobně	k6eAd1
jen	jen	k9
málo	málo	k4c1
písařů	písař	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
45	#num#	k4
v	v	k7c6
Pylu	pyl	k1gInSc6
(	(	kIx(
<g/>
západní	západní	k2eAgNnSc1d1
pobřeží	pobřeží	k1gNnSc1
Peloponéského	peloponéský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
66	#num#	k4
v	v	k7c6
Knóssu	Knóss	k1gInSc6
(	(	kIx(
<g/>
na	na	k7c6
Krétě	Kréta	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
historici	historik	k1gMnPc1
z	z	k7c2
tohoto	tento	k3xDgInSc2
faktu	fakt	k1gInSc2
usuzují	usuzovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
písmo	písmo	k1gNnSc1
bylo	být	k5eAaImAgNnS
užíváno	užívat	k5eAaImNgNnS
nějakým	nějaký	k3yIgInSc7
druhem	druh	k1gInSc7
spolku	spolek	k1gInSc2
profesionálních	profesionální	k2eAgMnPc2d1
písařů	písař	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
sloužili	sloužit	k5eAaImAgMnP
centrálním	centrální	k2eAgMnPc3d1
palácům	palác	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byly	být	k5eAaImAgInP
paláce	palác	k1gInPc1
zničeny	zničen	k2eAgInPc1d1
<g/>
,	,	kIx,
písmo	písmo	k1gNnSc1
zaniklo	zaniknout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
V	v	k7c6
jazykovědě	jazykověda	k1gFnSc6
C	C	kA
a	a	k8xC
V	V	kA
v	v	k7c6
tomto	tento	k3xDgInSc6
kontextu	kontext	k1gInSc6
zastupuje	zastupovat	k5eAaImIp3nS
souhlásku	souhláska	k1gFnSc4
(	(	kIx(
<g/>
consonant	consonant	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
samohlásku	samohláska	k1gFnSc4
(	(	kIx(
<g/>
vowel	vowel	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Linear	Lineara	k1gFnPc2
B	B	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
BATONĚK	BATONĚK	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlatá	zlatý	k2eAgFnSc1d1
Egeis	Egeis	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Lineární	lineární	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
A	a	k9
</s>
<s>
Michael	Michael	k1gMnSc1
Ventris	Ventris	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mínojská	Mínojský	k2eAgFnSc1d1
civilizace	civilizace	k1gFnSc1
mínojské	mínojský	k2eAgFnSc2d1
paláce	palác	k1gInSc2
</s>
<s>
Faistos	Faistos	k1gMnSc1
•	•	k?
Gortyn	Gortyn	k1gMnSc1
•	•	k?
Hagia	Hagia	k1gFnSc1
Triada	Triada	k1gFnSc1
•	•	k?
Kató	Kató	k1gFnSc2
Zakros	Zakrosa	k1gFnPc2
•	•	k?
Knóssos	Knóssos	k1gMnSc1
•	•	k?
Malia	Malium	k1gNnSc2
ostatní	ostatní	k2eAgFnSc2d1
mínojské	mínojský	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
</s>
<s>
Vila	vila	k1gFnSc1
lilií	lilie	k1gFnPc2
v	v	k7c6
Amnisosu	Amnisos	k1gInSc6
mínojská	mínojský	k2eAgFnSc1d1
písma	písmo	k1gNnSc2
</s>
<s>
Lineární	lineární	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
A	a	k8xC
•	•	k?
Lineární	lineární	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
B	B	kA
Mínóův	Mínóův	k2eAgInSc4d1
labyrint	labyrint	k1gInSc4
</s>
<s>
Labrys	Labrys	k6eAd1
•	•	k?
Mínótauros	Mínótaurosa	k1gFnPc2
•	•	k?
Mínós	Mínós	k1gInSc1
ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Disk	disk	k1gInSc1
z	z	k7c2
Faistu	Faist	k1gInSc2
•	•	k?
Střední	střední	k2eAgNnSc1d1
období	období	k1gNnSc1
mínojské	mínojský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
•	•	k?
Mínojština	Mínojština	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
písemných	písemný	k2eAgInPc2d1
systémů	systém	k1gInPc2
</s>
<s>
Obecně	obecně	k6eAd1
</s>
<s>
Historie	historie	k1gFnSc1
písma	písmo	k1gNnSc2
</s>
<s>
Jednotka	jednotka	k1gFnSc1
písma	písmo	k1gNnSc2
</s>
<s>
Kaligrafie	kaligrafie	k1gFnSc1
</s>
<s>
Počítačová	počítačový	k2eAgFnSc1d1
standardizace	standardizace	k1gFnSc1
</s>
<s>
Typografie	typografie	k1gFnSc1
Seznamy	seznam	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
písem	písmo	k1gNnPc2
</s>
<s>
nerozluštěná	rozluštěný	k2eNgFnSc1d1
</s>
<s>
tvůrci	tvůrce	k1gMnPc1
</s>
<s>
Jazyky	jazyk	k1gInPc1
dle	dle	k7c2
písma	písmo	k1gNnSc2
/	/	kIx~
dle	dle	k7c2
prvního	první	k4xOgInSc2
písemného	písemný	k2eAgInSc2d1
dokladu	doklad	k1gInSc2
</s>
<s>
Seznam	seznam	k1gInSc4
písem	písmo	k1gNnPc2
dle	dle	k7c2
řezu	řez	k1gInSc2
</s>
<s>
Typypísem	Typypís	k1gInSc7
</s>
<s>
Ideogramy	ideogram	k1gInPc1
/	/	kIx~
Piktogramy	piktogram	k1gInPc1
(	(	kIx(
<g/>
není	být	k5eNaImIp3nS
skutečné	skutečný	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Aztécké	aztécký	k2eAgNnSc1d1
</s>
<s>
Bliss	Bliss	k6eAd1
systém	systém	k1gInSc1
</s>
<s>
Dongba	Dongba	k1gFnSc1
</s>
<s>
Kaidā	Kaidā	k?
</s>
<s>
Míkmaq	Míkmaq	k?
</s>
<s>
Nsibidi	Nsibid	k1gMnPc1
Logogramy	Logogram	k1gInPc5
</s>
<s>
Čínské	čínský	k2eAgInPc1d1
znaky	znak	k1gInPc1
</s>
<s>
tradiční	tradiční	k2eAgInSc1d1
</s>
<s>
zjednodušené	zjednodušený	k2eAgNnSc4d1
</s>
<s>
Hanča	Hanča	k1gFnSc1
</s>
<s>
Idu	Ida	k1gFnSc4
</s>
<s>
Kandži	Kandzat	k5eAaPmIp1nS
</s>
<s>
Chữ	Chữ	k?
nôm	nôm	k?
Připodobněno	připodobnit	k5eAaPmNgNnS
čínským	čínský	k2eAgInSc7d1
</s>
<s>
Džurčenské	Džurčenský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Kitanské	Kitanský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Tangutské	Tangutský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
Klínopis	Klínopis	k1gInSc1
</s>
<s>
Akkadský	Akkadský	k2eAgInSc1d1
</s>
<s>
Asyrský	asyrský	k2eAgInSc1d1
</s>
<s>
Chetitský	chetitský	k2eAgInSc1d1
</s>
<s>
Luvijský	Luvijský	k2eAgInSc1d1
</s>
<s>
Sumerský	sumerský	k2eAgMnSc1d1
Ostatní	ostatní	k2eAgMnSc1d1
logosylabické	logosylabická	k1gFnPc1
</s>
<s>
Anatolské	anatolský	k2eAgInPc1d1
hieroglyfy	hieroglyf	k1gInPc1
</s>
<s>
Krétské	krétský	k2eAgInPc1d1
hieroglyfy	hieroglyf	k1gInPc1
</s>
<s>
Mayské	mayský	k2eAgInPc1d1
hieroglyfy	hieroglyf	k1gInPc1
Logokonsonantické	Logokonsonantický	k2eAgInPc1d1
</s>
<s>
Démotické	démotický	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Hieratické	hieratický	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Egyptské	egyptský	k2eAgInPc1d1
hieroglyfy	hieroglyf	k1gInPc1
Číslice	číslice	k1gFnSc2
</s>
<s>
Západoarabské	Západoarabský	k2eAgFnSc3d1
(	(	kIx(
<g/>
Evropské	evropský	k2eAgFnSc3d1
<g/>
)	)	kIx)
</s>
<s>
Východoarabské	Východoarabský	k2eAgInPc4d1
(	(	kIx(
<g/>
Indické	indický	k2eAgInPc4d1
<g/>
)	)	kIx)
</s>
<s>
Řecké	řecký	k2eAgNnSc1d1
</s>
<s>
Římské	římský	k2eAgInPc1d1
</s>
<s>
Slabičná	slabičný	k2eAgFnSc1d1
</s>
<s>
Bybloské	Bybloský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Čerokézské	Čerokézský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Kyperské	kyperský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Kypersko-mínojské	Kypersko-mínojský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
</s>
<s>
Hiragana	Hiragana	k1gFnSc1
</s>
<s>
Katakana	Katakana	k1gFnSc1
</s>
<s>
Lineární	lineární	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
B	B	kA
</s>
<s>
Man	Man	k1gMnSc1
<g/>
'	'	kIx"
<g/>
jógana	jógana	k1gFnSc1
</s>
<s>
Nüshu	Nüsha	k1gFnSc4
</s>
<s>
Staroperský	staroperský	k2eAgInSc1d1
klínopis	klínopis	k1gInSc1
Částečně	částečně	k6eAd1
slabičná	slabičný	k2eAgFnSc1d1
</s>
<s>
Plná	plný	k2eAgFnSc1d1
</s>
<s>
Keltiberské	Keltiberský	k2eAgFnPc1d1
S	s	k7c7
redundancí	redundance	k1gFnSc7
</s>
<s>
Kitanské	Kitanský	k2eAgNnSc1d1
malé	malý	k2eAgNnSc1d1
pečetní	pečetní	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Jihozápadní	jihozápadní	k2eAgNnSc1d1
paleohispánské	paleohispánský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Ču-jin	Ču-jin	k1gMnSc1
fu-chao	fu-chao	k1gMnSc1
</s>
<s>
Abugidy	Abugida	k1gFnPc1
<g/>
(	(	kIx(
<g/>
abecedně-slabičná	abecedně-slabičný	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Bráhmí	Bráhmit	k5eAaPmIp3nS
</s>
<s>
Severní	severní	k2eAgFnSc1d1
</s>
<s>
Bengalská	Bengalský	k2eAgFnSc1d1
</s>
<s>
Brā	Brā	k?
</s>
<s>
Devanā	Devanā	k?
</s>
<s>
Gudžarátská	Gudžarátský	k2eAgFnSc1d1
</s>
<s>
Gupta	Gupta	k1gFnSc1
</s>
<s>
Gurmukhī	Gurmukhī	k?
</s>
<s>
Kaithi	Kaithi	k6eAd1
</s>
<s>
Nā	Nā	k?
</s>
<s>
'	'	kIx"
<g/>
Phags-pa	Phags-p	k1gMnSc4
</s>
<s>
Siddhaṃ	Siddhaṃ	k?
</s>
<s>
Sojombo	Sojomba	k1gMnSc5
</s>
<s>
Tibetská	tibetský	k2eAgFnSc1d1
</s>
<s>
Tocharská	tocharský	k2eAgFnSc1d1
Jižní	jižní	k2eAgFnSc1d1
</s>
<s>
Barmská	barmský	k2eAgFnSc1d1
</s>
<s>
Kannada	Kannada	k1gFnSc1
</s>
<s>
Khmerská	khmerský	k2eAgFnSc1d1
</s>
<s>
Malajalamská	Malajalamský	k2eAgFnSc1d1
</s>
<s>
Sinhala	Sinhat	k5eAaImAgFnS,k5eAaPmAgFnS,k5eAaBmAgFnS
</s>
<s>
Tai	Tai	k?
Viet	Viet	k1gInSc1
</s>
<s>
Tamilská	tamilský	k2eAgFnSc1d1
</s>
<s>
Telugská	Telugskat	k5eAaPmIp3nS
</s>
<s>
Thajská	thajský	k2eAgFnSc1d1
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Kanadská	kanadský	k2eAgNnPc1d1
slabičná	slabičný	k2eAgNnPc1d1
písma	písmo	k1gNnPc1
(	(	kIx(
<g/>
Inuitské	Inuitský	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
,	,	kIx,
Kríjské	Kríjský	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Ge	Ge	k?
<g/>
'	'	kIx"
<g/>
ez	ez	k?
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1
Braillovo	Braillův	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Kharóšthí	Kharóšthí	k6eAd1
</s>
<s>
Merojské	Merojský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Tā	Tā	k6eAd1
</s>
<s>
Souhlásková	souhláskový	k2eAgFnSc1d1
(	(	kIx(
<g/>
abdžady	abdžad	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
číslice	číslice	k1gFnSc1
</s>
<s>
Aramejský	aramejský	k2eAgInSc1d1
</s>
<s>
Arabský	arabský	k2eAgInSc1d1
</s>
<s>
Hebrejský	hebrejský	k2eAgInSc1d1
</s>
<s>
Tifinagh	Tifinagh	k1gMnSc1
</s>
<s>
Manichejský	manichejský	k2eAgInSc1d1
</s>
<s>
Nabatajský	Nabatajský	k2eAgInSc1d1
</s>
<s>
Pahlaví	Pahlavit	k5eAaPmIp3nS
</s>
<s>
Paleohebrejský	Paleohebrejský	k2eAgInSc1d1
</s>
<s>
Pegon	Pegon	k1gMnSc1
</s>
<s>
Fénický	fénický	k2eAgInSc1d1
</s>
<s>
Psalter	Psalter	k1gMnSc1
</s>
<s>
Samaritánský	samaritánský	k2eAgInSc1d1
</s>
<s>
Jihoarabský	Jihoarabský	k2eAgInSc1d1
</s>
<s>
Sogdský	Sogdský	k2eAgInSc1d1
</s>
<s>
Syrský	syrský	k2eAgInSc1d1
</s>
<s>
Ugaritský	Ugaritský	k2eAgMnSc1d1
Abecedy	abeceda	k1gFnSc2
</s>
<s>
Lineární	lineární	k2eAgFnSc1d1
</s>
<s>
Anglosaský	anglosaský	k2eAgInSc1d1
futhork	futhork	k1gInSc1
</s>
<s>
Arménská	arménský	k2eAgFnSc1d1
</s>
<s>
Avestánská	Avestánský	k2eAgFnSc1d1
</s>
<s>
Kárská	Kárskat	k5eAaPmIp3nS
</s>
<s>
Koptská	koptský	k2eAgFnSc1d1
</s>
<s>
Cyrilice	cyrilice	k1gFnSc1
</s>
<s>
Starší	starý	k2eAgInSc1d2
futhark	futhark	k1gInSc1
</s>
<s>
Etruská	etruský	k2eAgFnSc1d1
</s>
<s>
Gruzínská	gruzínský	k2eAgFnSc1d1
</s>
<s>
Hlaholice	hlaholice	k1gFnSc1
</s>
<s>
Gótská	gótský	k2eAgFnSc1d1
</s>
<s>
Řecké	řecký	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Řecko-Iberská	řecko-iberský	k2eAgFnSc1d1
</s>
<s>
Hangul	Hangul	k1gInSc1
</s>
<s>
IPA	IPA	kA
</s>
<s>
Latinka	latinka	k1gFnSc1
</s>
<s>
Beneventána	Beneventán	k2eAgFnSc1d1
</s>
<s>
Gotické	gotický	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Karolina	Karolinum	k1gNnPc1
</s>
<s>
Fraktura	fraktura	k1gFnSc1
</s>
<s>
Cló	Cló	k?
Gaelach	Gaelach	k1gInSc1
</s>
<s>
Iroskotské	Iroskotský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Kurent	kurent	k1gInSc1
</s>
<s>
Merovejské	merovejský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Sütterlin	Sütterlin	k1gInSc1
</s>
<s>
Vizigótské	vizigótský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Lýkská	Lýkskat	k5eAaPmIp3nS
</s>
<s>
Lydská	Lydskat	k5eAaPmIp3nS
</s>
<s>
Mandžuská	mandžuský	k2eAgFnSc1d1
</s>
<s>
Mongolská	mongolský	k2eAgFnSc1d1
</s>
<s>
Neo-Tifinagh	Neo-Tifinagh	k1gMnSc1
</s>
<s>
N	N	kA
<g/>
'	'	kIx"
<g/>
Ko	Ko	k1gMnSc1
</s>
<s>
Ogham	Ogham	k6eAd1
</s>
<s>
Rovas	Rovas	k1gMnSc1
</s>
<s>
Orchonské	Orchonský	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Staroujgurská	Staroujgurský	k2eAgFnSc1d1
</s>
<s>
Runy	Runa	k1gFnPc1
</s>
<s>
Tifinagh	Tifinagh	k1gMnSc1
</s>
<s>
Visible	Visible	k6eAd1
Speech	speech	k1gInSc1
</s>
<s>
Mladší	mladý	k2eAgInSc1d2
futhark	futhark	k1gInSc1
Nelineární	lineární	k2eNgFnSc2d1
</s>
<s>
Braillovo	Braillův	k2eAgNnSc1d1
písmo	písmo	k1gNnSc1
</s>
<s>
Námořní	námořní	k2eAgFnSc1d1
vlajková	vlajkový	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
</s>
<s>
Morseova	Morseův	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
</s>
<s>
Chappeův	Chappeův	k2eAgInSc1d1
telegraf	telegraf	k1gInSc1
</s>
<s>
Semafor	Semafor	k1gInSc1
(	(	kIx(
<g/>
abeceda	abeceda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Moon	Moon	k1gMnSc1
type	typ	k1gInSc5
</s>
<s>
Fiktivní	fiktivní	k2eAgNnPc1d1
písma	písmo	k1gNnPc1
</s>
<s>
Aurebesh	Aurebesh	k1gMnSc1
</s>
<s>
Cirth	Cirth	k1gMnSc1
</s>
<s>
pIqaD	pIqaD	k?
</s>
<s>
Tengwar	Tengwar	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
381201	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Starověk	starověk	k1gInSc1
</s>
