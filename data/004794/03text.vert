<s>
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
Acquired	Acquired	k1gInSc1	Acquired
Immune	Immun	k1gInSc5	Immun
Deficiency	Deficiency	k1gInPc1	Deficiency
Syndrome	syndrom	k1gInSc5	syndrom
nebo	nebo	k8xC	nebo
též	též	k9	též
Acquired	Acquired	k1gInSc1	Acquired
Immunodeficiency	Immunodeficienca	k1gMnSc2	Immunodeficienca
Syndrome	syndrom	k1gInSc5	syndrom
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Syndrom	syndrom	k1gInSc1	syndrom
získaného	získaný	k2eAgNnSc2d1	získané
selhání	selhání	k1gNnSc2	selhání
imunity	imunita	k1gFnSc2	imunita
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Syndrome	syndrom	k1gInSc5	syndrom
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
immunodéficience	immunodéficience	k1gFnSc1	immunodéficience
acquise	acquise	k1gFnSc1	acquise
<g/>
,	,	kIx,	,
SIDA	SIDA	kA	SIDA
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc1	soubor
příznaků	příznak	k1gInPc2	příznak
a	a	k8xC	a
infekcí	infekce	k1gFnPc2	infekce
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
následkem	následkem	k7c2	následkem
poškození	poškození	k1gNnSc2	poškození
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
člověka	člověk	k1gMnSc2	člověk
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
nakazily	nakazit	k5eAaPmAgFnP	nakazit
desítky	desítka	k1gFnPc4	desítka
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
nejhůře	zle	k6eAd3	zle
postižené	postižený	k2eAgInPc1d1	postižený
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc1	některý
africké	africký	k2eAgInPc1d1	africký
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
léky	lék	k1gInPc1	lék
na	na	k7c4	na
tlumení	tlumení	k1gNnSc4	tlumení
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
neustále	neustále	k6eAd1	neustále
zdokonalují	zdokonalovat	k5eAaImIp3nP	zdokonalovat
<g/>
,	,	kIx,	,
nemoc	nemoc	k1gFnSc1	nemoc
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
nevyléčitelnou	vyléčitelný	k2eNgFnSc7d1	nevyléčitelná
a	a	k8xC	a
smrtelnou	smrtelný	k2eAgFnSc7d1	smrtelná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
definice	definice	k1gFnSc1	definice
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doplněná	doplněný	k2eAgFnSc1d1	doplněná
o	o	k7c4	o
několik	několik	k4yIc4	několik
referenčních	referenční	k2eAgFnPc2d1	referenční
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
definice	definice	k1gFnSc2	definice
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
starší	starý	k2eAgMnSc1d2	starší
dvanácti	dvanáct	k4xCc2	dvanáct
let	léto	k1gNnPc2	léto
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
tzv.	tzv.	kA	tzv.
test	test	k1gInSc1	test
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
protilátek	protilátka	k1gFnPc2	protilátka
HIV	HIV	kA	HIV
(	(	kIx(	(
<g/>
popularizován	popularizovat	k5eAaImNgInS	popularizovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
AIDS-test	AIDSest	k1gFnSc1	AIDS-test
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vykázal	vykázat	k5eAaPmAgInS	vykázat
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
výsledek	výsledek	k1gInSc1	výsledek
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
splněna	splnit	k5eAaPmNgFnS	splnit
aspoň	aspoň	k9	aspoň
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
následujících	následující	k2eAgFnPc2d1	následující
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
:	:	kIx,	:
alespoň	alespoň	k9	alespoň
10	[number]	k4	10
<g/>
%	%	kIx~	%
ztráta	ztráta	k1gFnSc1	ztráta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g />
.	.	kIx.	.
</s>
<s>
nebo	nebo	k8xC	nebo
kachexie	kachexie	k1gFnSc1	kachexie
(	(	kIx(	(
<g/>
celková	celkový	k2eAgFnSc1d1	celková
tělesná	tělesný	k2eAgFnSc1d1	tělesná
sešlost	sešlost	k1gFnSc1	sešlost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
průjmy	průjem	k1gInPc7	průjem
<g/>
,	,	kIx,	,
horečkami	horečka	k1gFnPc7	horečka
<g/>
,	,	kIx,	,
trvajícími	trvající	k2eAgFnPc7d1	trvající
přerušovaně	přerušovaně	k6eAd1	přerušovaně
nebo	nebo	k8xC	nebo
nepřetržitě	přetržitě	k6eNd1	přetržitě
nejméně	málo	k6eAd3	málo
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
známa	znám	k2eAgFnSc1d1	známa
jejich	jejich	k3xOp3gFnSc1	jejich
příčina	příčina	k1gFnSc1	příčina
nesouvisející	související	k2eNgFnSc1d1	nesouvisející
s	s	k7c7	s
infekcí	infekce	k1gFnSc7	infekce
HIV	HIV	kA	HIV
kryptokoková	kryptokokový	k2eAgFnSc1d1	kryptokokový
meningitida	meningitida	k1gFnSc1	meningitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
mozkových	mozkový	k2eAgInPc2d1	mozkový
obalů	obal	k1gInPc2	obal
způsobený	způsobený	k2eAgInSc1d1	způsobený
infekcí	infekce	k1gFnPc2	infekce
kvasinkou	kvasinka	k1gFnSc7	kvasinka
Cryptococcus	Cryptococcus	k1gMnSc1	Cryptococcus
neoformans	neoformansa	k1gFnPc2	neoformansa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
plic	plíce	k1gFnPc2	plíce
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
orgánů	orgán	k1gInPc2	orgán
Kaposiho	Kaposi	k1gMnSc2	Kaposi
sarkom	sarkom	k1gInSc4	sarkom
neurologické	neurologický	k2eAgNnSc1d1	neurologický
poškození	poškození	k1gNnSc1	poškození
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
nezávislým	závislý	k2eNgFnPc3d1	nezávislá
každodenním	každodenní	k2eAgFnPc3d1	každodenní
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
známa	znám	k2eAgFnSc1d1	známa
jeho	jeho	k3xOp3gFnSc1	jeho
příčina	příčina	k1gFnSc1	příčina
nesouvisející	související	k2eNgFnSc1d1	nesouvisející
s	s	k7c7	s
infekcí	infekce	k1gFnSc7	infekce
HIV	HIV	kA	HIV
(	(	kIx(	(
<g/>
např.	např.	kA	např.
úraz	úraz	k1gInSc1	úraz
nebo	nebo	k8xC	nebo
cévní	cévní	k2eAgFnSc1d1	cévní
mozková	mozkový	k2eAgFnSc1d1	mozková
příhoda	příhoda	k1gFnSc1	příhoda
<g/>
)	)	kIx)	)
kandidóza	kandidóza	k1gFnSc1	kandidóza
jícnu	jícen	k1gInSc2	jícen
(	(	kIx(	(
<g/>
přemnožení	přemnožení	k1gNnSc4	přemnožení
kvasinek	kvasinka	k1gFnPc2	kvasinka
rodu	rod	k1gInSc2	rod
Candida	Candida	k1gFnSc1	Candida
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
často	často	k6eAd1	často
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přítomnosti	přítomnost	k1gFnSc2	přítomnost
orální	orální	k2eAgFnSc2d1	orální
kandidózy	kandidóza	k1gFnSc2	kandidóza
doprovázené	doprovázený	k2eAgFnSc2d1	doprovázená
poruchou	porucha	k1gFnSc7	porucha
polykání	polykání	k1gNnSc2	polykání
klinicky	klinicky	k6eAd1	klinicky
diagnostikovaný	diagnostikovaný	k2eAgInSc1d1	diagnostikovaný
život	život	k1gInSc4	život
ohrožující	ohrožující	k2eAgInSc4d1	ohrožující
nebo	nebo	k8xC	nebo
opakovaný	opakovaný	k2eAgInSc4d1	opakovaný
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
i	i	k9	i
bez	bez	k7c2	bez
etiologického	etiologický	k2eAgNnSc2d1	etiologické
potvrzení	potvrzení	k1gNnSc2	potvrzení
V	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
používaná	používaný	k2eAgFnSc1d1	používaná
definice	definice	k1gFnSc1	definice
CDC	CDC	kA	CDC
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
seznam	seznam	k1gInSc4	seznam
platných	platný	k2eAgInPc2d1	platný
příznaků	příznak	k1gInPc2	příznak
o	o	k7c4	o
počet	počet	k1gInSc4	počet
CD	CD	kA	CD
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
lymfocytů	lymfocyt	k1gInPc2	lymfocyt
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
200	[number]	k4	200
<g/>
/	/	kIx~	/
<g/>
mikrolitr	mikrolitr	k1gMnSc1	mikrolitr
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
chudších	chudý	k2eAgFnPc2d2	chudší
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
pro	pro	k7c4	pro
diagnostiku	diagnostika	k1gFnSc4	diagnostika
AIDS	AIDS	kA	AIDS
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
definice	definice	k1gFnSc1	definice
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
test	test	k1gInSc4	test
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
protilátek	protilátka	k1gFnPc2	protilátka
a	a	k8xC	a
definuje	definovat	k5eAaBmIp3nS	definovat
AIDS	aids	k1gInSc4	aids
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
člověk	člověk	k1gMnSc1	člověk
nejméně	málo	k6eAd3	málo
dva	dva	k4xCgInPc1	dva
hlavní	hlavní	k2eAgInPc1d1	hlavní
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
aspoň	aspoň	k9	aspoň
jedním	jeden	k4xCgInSc7	jeden
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
příznakem	příznak	k1gInSc7	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
alespoň	alespoň	k9	alespoň
20	[number]	k4	20
<g/>
%	%	kIx~	%
ztráta	ztráta	k1gFnSc1	ztráta
tělesné	tělesný	k2eAgFnSc2d1	tělesná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
chronický	chronický	k2eAgInSc1d1	chronický
průjem	průjem	k1gInSc1	průjem
trvající	trvající	k2eAgInSc1d1	trvající
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
1	[number]	k4	1
měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
horečky	horečka	k1gFnPc4	horečka
trvající	trvající	k2eAgFnPc4d1	trvající
stále	stále	k6eAd1	stále
nebo	nebo	k8xC	nebo
přerušovaně	přerušovaně	k6eAd1	přerušovaně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
měsíc	měsíc	k1gInSc1	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
trvalý	trvalý	k2eAgInSc1d1	trvalý
kašel	kašel	k1gInSc1	kašel
trvající	trvající	k2eAgInSc1d1	trvající
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
1	[number]	k4	1
měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
celkové	celkový	k2eAgNnSc1d1	celkové
svědivé	svědivý	k2eAgNnSc1d1	svědivé
zánětlivé	zánětlivý	k2eAgNnSc1d1	zánětlivé
onemocnění	onemocnění	k1gNnSc1	onemocnění
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
anamnéza	anamnéza	k1gFnSc1	anamnéza
pásového	pásový	k2eAgInSc2d1	pásový
oparu	opar	k1gInSc2	opar
<g/>
,	,	kIx,	,
kandidóza	kandidóza	k1gFnSc1	kandidóza
ústní	ústní	k2eAgFnSc2d1	ústní
části	část	k1gFnSc2	část
hltanu	hltan	k1gInSc2	hltan
<g/>
,	,	kIx,	,
chronická	chronický	k2eAgFnSc1d1	chronická
pokročilá	pokročilý	k2eAgFnSc1d1	pokročilá
roztroušená	roztroušený	k2eAgFnSc1d1	roztroušená
infekce	infekce	k1gFnSc1	infekce
prostého	prostý	k2eAgInSc2d1	prostý
oparu	opar	k1gInSc2	opar
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
onemocnění	onemocnění	k1gNnSc4	onemocnění
mízních	mízní	k2eAgFnPc2d1	mízní
uzlin	uzlina	k1gFnPc2	uzlina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
přítomnost	přítomnost	k1gFnSc1	přítomnost
celkového	celkový	k2eAgMnSc2d1	celkový
Kaposiho	Kaposi	k1gMnSc2	Kaposi
sarkomu	sarkom	k1gInSc2	sarkom
nebo	nebo	k8xC	nebo
kryptokokální	kryptokokální	k2eAgFnSc2d1	kryptokokální
meningitidy	meningitida	k1gFnSc2	meningitida
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
definice	definice	k1gFnSc2	definice
dostatečná	dostatečný	k2eAgFnSc1d1	dostatečná
pro	pro	k7c4	pro
diagnózu	diagnóza	k1gFnSc4	diagnóza
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
přenosu	přenos	k1gInSc2	přenos
viru	vir	k1gInSc2	vir
HIV	HIV	kA	HIV
nejnebezpečnější	bezpečný	k2eNgFnSc7d3	nejnebezpečnější
tekutinou	tekutina	k1gFnSc7	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Rizikové	rizikový	k2eAgInPc1d1	rizikový
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
použité	použitý	k2eAgFnPc1d1	použitá
injekční	injekční	k2eAgFnPc1d1	injekční
jehly	jehla	k1gFnPc1	jehla
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
sdílené	sdílený	k2eAgFnPc1d1	sdílená
mezi	mezi	k7c4	mezi
narkomany	narkoman	k1gMnPc4	narkoman
nebo	nebo	k8xC	nebo
opakovaně	opakovaně	k6eAd1	opakovaně
používané	používaný	k2eAgNnSc1d1	používané
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
se	s	k7c7	s
zaostalým	zaostalý	k2eAgNnSc7d1	zaostalé
zdravotnictvím	zdravotnictví	k1gNnSc7	zdravotnictví
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
přenosu	přenos	k1gInSc2	přenos
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
2	[number]	k4	2
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Důsledné	důsledný	k2eAgNnSc1d1	důsledné
používání	používání	k1gNnSc1	používání
jednorázových	jednorázový	k2eAgFnPc2d1	jednorázová
jehel	jehla	k1gFnPc2	jehla
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
prevence	prevence	k1gFnSc2	prevence
nutností	nutnost	k1gFnPc2	nutnost
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc4	přenos
krevní	krevní	k2eAgFnSc7d1	krevní
transfúzí	transfúze	k1gFnSc7	transfúze
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
díky	díky	k7c3	díky
testování	testování	k1gNnSc3	testování
krve	krev	k1gFnSc2	krev
vzácný	vzácný	k2eAgInSc4d1	vzácný
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
však	však	k9	však
občas	občas	k6eAd1	občas
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc4	riziko
představují	představovat	k5eAaImIp3nP	představovat
i	i	k9	i
orgánové	orgánový	k2eAgFnPc4d1	orgánová
transplantace	transplantace	k1gFnPc4	transplantace
<g/>
.	.	kIx.	.
</s>
<s>
Velkému	velký	k2eAgNnSc3d1	velké
riziku	riziko	k1gNnSc3	riziko
je	být	k5eAaImIp3nS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
zdravotnický	zdravotnický	k2eAgInSc1d1	zdravotnický
personál	personál	k1gInSc1	personál
manipulující	manipulující	k2eAgInSc1d1	manipulující
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Hmyzí	hmyzí	k2eAgNnSc4d1	hmyzí
kousnutí	kousnutí	k1gNnSc4	kousnutí
riziko	riziko	k1gNnSc1	riziko
přenosu	přenos	k1gInSc2	přenos
nepředstavuje	představovat	k5eNaImIp3nS	představovat
<g/>
.	.	kIx.	.
</s>
<s>
Sexuální	sexuální	k2eAgInSc1d1	sexuální
přenos	přenos	k1gInSc1	přenos
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
z	z	k7c2	z
ženy	žena	k1gFnSc2	žena
na	na	k7c4	na
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
muže	muž	k1gMnSc2	muž
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
muže	muž	k1gMnSc2	muž
na	na	k7c4	na
muže	muž	k1gMnPc4	muž
i	i	k8xC	i
ženy	žena	k1gFnPc4	žena
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
při	při	k7c6	při
análním	anální	k2eAgMnSc6d1	anální
<g/>
,	,	kIx,	,
vaginálním	vaginální	k2eAgMnSc6d1	vaginální
i	i	k8xC	i
orálním	orální	k2eAgInSc6d1	orální
styku	styk	k1gInSc6	styk
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
jiné	jiný	k2eAgFnSc3d1	jiná
praktice	praktika	k1gFnSc3	praktika
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yIgFnSc2	který
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
styku	styk	k1gInSc3	styk
sliznic	sliznice	k1gFnPc2	sliznice
se	s	k7c7	s
sexuálními	sexuální	k2eAgInPc7d1	sexuální
sekrety	sekret	k1gInPc7	sekret
či	či	k8xC	či
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Anální	anální	k2eAgInSc1d1	anální
styk	styk	k1gInSc1	styk
je	být	k5eAaImIp3nS	být
významně	významně	k6eAd1	významně
nebezpečnější	bezpečný	k2eNgInSc4d2	nebezpečnější
než	než	k8xS	než
vaginální	vaginální	k2eAgInSc4d1	vaginální
<g/>
,	,	kIx,	,
orální	orální	k2eAgInSc4d1	orální
styk	styk	k1gInSc4	styk
je	být	k5eAaImIp3nS	být
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
nejméně	málo	k6eAd3	málo
<g/>
.	.	kIx.	.
</s>
<s>
Vaginální	vaginální	k2eAgInSc1d1	vaginální
styk	styk	k1gInSc1	styk
představuje	představovat	k5eAaImIp3nS	představovat
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
přenosu	přenos	k1gInSc2	přenos
z	z	k7c2	z
muže	muž	k1gMnSc2	muž
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
než	než	k8xS	než
naopak	naopak	k6eAd1	naopak
(	(	kIx(	(
<g/>
žena	žena	k1gFnSc1	žena
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
většímu	veliký	k2eAgNnSc3d2	veliký
množství	množství	k1gNnSc3	množství
tekutin	tekutina	k1gFnPc2	tekutina
partnera	partner	k1gMnSc4	partner
než	než	k8xS	než
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
sperma	sperma	k1gNnSc1	sperma
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
viru	vir	k1gInSc2	vir
než	než	k8xS	než
vaginální	vaginální	k2eAgInSc1d1	vaginální
sekret	sekret	k1gInSc1	sekret
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
je	být	k5eAaImIp3nS	být
tekutinám	tekutina	k1gFnPc3	tekutina
vystavena	vystavit	k5eAaPmNgFnS	vystavit
déle	dlouho	k6eAd2	dlouho
a	a	k8xC	a
větší	veliký	k2eAgFnSc7d2	veliký
plochou	plocha	k1gFnSc7	plocha
sliznice	sliznice	k1gFnSc2	sliznice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
análního	anální	k2eAgInSc2d1	anální
styku	styk	k1gInSc2	styk
je	být	k5eAaImIp3nS	být
většímu	veliký	k2eAgNnSc3d2	veliký
riziku	riziko	k1gNnSc3	riziko
vystaven	vystavit	k5eAaPmNgInS	vystavit
příjemce	příjemka	k1gFnSc3	příjemka
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
přenosu	přenos	k1gInSc2	přenos
HIV	HIV	kA	HIV
infekce	infekce	k1gFnSc2	infekce
při	při	k7c6	při
jednotlivém	jednotlivý	k2eAgInSc6d1	jednotlivý
nechráněném	chráněný	k2eNgInSc6d1	nechráněný
sexuálním	sexuální	k2eAgInSc6d1	sexuální
styku	styk	k1gInSc6	styk
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
styku	styk	k1gInSc2	styk
<g/>
,	,	kIx,	,
aktuální	aktuální	k2eAgNnSc1d1	aktuální
množství	množství	k1gNnSc1	množství
viru	vir	k1gInSc2	vir
v	v	k7c6	v
nakaženém	nakažený	k2eAgMnSc6d1	nakažený
partnerovi	partner	k1gMnSc6	partner
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
nákaza	nákaza	k1gFnSc1	nákaza
další	další	k2eAgFnSc1d1	další
pohlavní	pohlavní	k2eAgFnSc7d1	pohlavní
chorobou	choroba	k1gFnSc7	choroba
(	(	kIx(	(
<g/>
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
přenosu	přenos	k1gInSc2	přenos
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
styk	styk	k1gInSc4	styk
během	během	k7c2	během
menstruace	menstruace	k1gFnSc2	menstruace
je	být	k5eAaImIp3nS	být
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
přítomnosti	přítomnost	k1gFnSc3	přítomnost
krve	krev	k1gFnSc2	krev
rizikovější	rizikový	k2eAgFnSc2d2	rizikovější
<g/>
.	.	kIx.	.
</s>
<s>
Líbání	líbání	k1gNnSc1	líbání
rizikové	rizikový	k2eAgNnSc1d1	rizikové
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
že	že	k8xS	že
ústa	ústa	k1gNnPc1	ústa
nejsou	být	k5eNaImIp3nP	být
poraněná	poraněný	k2eAgNnPc1d1	poraněné
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
nákazy	nákaza	k1gFnSc2	nákaza
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
střídání	střídání	k1gNnSc1	střídání
partnerů	partner	k1gMnPc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
nakažení	nakažení	k1gNnSc2	nakažení
významně	významně	k6eAd1	významně
snižuje	snižovat	k5eAaImIp3nS	snižovat
užívání	užívání	k1gNnSc1	užívání
kondomu	kondom	k1gInSc2	kondom
<g/>
,	,	kIx,	,
Waller	Waller	k1gInSc1	Waller
a	a	k8xC	a
Davis	Davis	k1gFnSc1	Davis
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
metaanalýze	metaanalýza	k1gFnSc6	metaanalýza
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
ukázali	ukázat	k5eAaPmAgMnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
užívání	užívání	k1gNnSc2	užívání
kondomu	kondom	k1gInSc2	kondom
snižuje	snižovat	k5eAaImIp3nS	snižovat
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
nepoužíváním	nepoužívání	k1gNnSc7	nepoužívání
kondomu	kondom	k1gInSc2	kondom
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
riziko	riziko	k1gNnSc1	riziko
sérokonverze	sérokonverze	k1gFnSc2	sérokonverze
o	o	k7c4	o
80	[number]	k4	80
%	%	kIx~	%
<g/>
;	;	kIx,	;
autoři	autor	k1gMnPc1	autor
dodávají	dodávat	k5eAaImIp3nP	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
použité	použitý	k2eAgInPc1d1	použitý
prameny	pramen	k1gInPc1	pramen
nehodnotily	hodnotit	k5eNaImAgInP	hodnotit
správnost	správnost	k1gFnSc4	správnost
použití	použití	k1gNnSc2	použití
kondomů	kondom	k1gInPc2	kondom
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
formy	forma	k1gFnPc1	forma
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
hormonální	hormonální	k2eAgFnSc6d1	hormonální
<g/>
)	)	kIx)	)
při	pře	k1gFnSc6	pře
styku	styk	k1gInSc2	styk
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
nákazou	nákaza	k1gFnSc7	nákaza
neposkytují	poskytovat	k5eNaImIp3nP	poskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nechráněném	chráněný	k2eNgInSc6d1	nechráněný
styku	styk	k1gInSc6	styk
s	s	k7c7	s
neznámým	známý	k2eNgMnSc7d1	neznámý
partnerem	partner	k1gMnSc7	partner
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
dostatečném	dostatečný	k2eAgInSc6d1	dostatečný
časovém	časový	k2eAgInSc6d1	časový
odstupu	odstup	k1gInSc6	odstup
nutném	nutný	k2eAgInSc6d1	nutný
pro	pro	k7c4	pro
správnou	správný	k2eAgFnSc4d1	správná
diagnózu	diagnóza	k1gFnSc4	diagnóza
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
u	u	k7c2	u
lékaře	lékař	k1gMnSc2	lékař
podstoupit	podstoupit	k5eAaPmF	podstoupit
krevní	krevní	k2eAgInPc4d1	krevní
testy	test	k1gInPc4	test
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
viru	vir	k1gInSc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
Nákaze	nákaza	k1gFnSc3	nákaza
tak	tak	k9	tak
již	již	k6eAd1	již
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nelze	lze	k6eNd1	lze
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
zabránit	zabránit	k5eAaPmF	zabránit
nákaze	nákaza	k1gFnSc3	nákaza
dalších	další	k2eAgMnPc2d1	další
partnerů	partner	k1gMnPc2	partner
a	a	k8xC	a
včasnou	včasný	k2eAgFnSc7d1	včasná
léčbou	léčba	k1gFnSc7	léčba
zlepšit	zlepšit	k5eAaPmF	zlepšit
prognózu	prognóza	k1gFnSc4	prognóza
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
léčebné	léčebný	k2eAgInPc1d1	léčebný
postupy	postup	k1gInPc1	postup
mají	mít	k5eAaImIp3nP	mít
slibné	slibný	k2eAgInPc1d1	slibný
výsledky	výsledek	k1gInPc1	výsledek
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
zpomalení	zpomalení	k1gNnSc1	zpomalení
postupu	postup	k1gInSc2	postup
onemocnění	onemocnění	k1gNnSc2	onemocnění
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejúčinnější	účinný	k2eAgFnSc7d3	nejúčinnější
metodou	metoda	k1gFnSc7	metoda
proti	proti	k7c3	proti
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
epidemii	epidemie	k1gFnSc3	epidemie
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
prevence	prevence	k1gFnSc1	prevence
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
doporučení	doporučení	k1gNnSc4	doporučení
pro	pro	k7c4	pro
prevenci	prevence	k1gFnSc4	prevence
sexuálního	sexuální	k2eAgInSc2d1	sexuální
přenosu	přenos	k1gInSc2	přenos
HIV	HIV	kA	HIV
patří	patřit	k5eAaImIp3nP	patřit
sexuální	sexuální	k2eAgFnPc1d1	sexuální
abstinence	abstinence	k1gFnPc1	abstinence
<g/>
,	,	kIx,	,
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
monogamní	monogamní	k2eAgInSc4d1	monogamní
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
nenakaženým	nakažený	k2eNgMnSc7d1	nenakažený
partnerem	partner	k1gMnSc7	partner
<g/>
,	,	kIx,	,
omezený	omezený	k2eAgInSc1d1	omezený
počet	počet	k1gInSc1	počet
životních	životní	k2eAgMnPc2d1	životní
partnerů	partner	k1gMnPc2	partner
a	a	k8xC	a
použití	použití	k1gNnSc4	použití
kondomu	kondom	k1gInSc2	kondom
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
jeden	jeden	k4xCgInSc4	jeden
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
styk	styk	k1gInSc4	styk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
viru	vir	k1gInSc2	vir
HIV	HIV	kA	HIV
z	z	k7c2	z
matky	matka	k1gFnSc2	matka
na	na	k7c4	na
dítě	dítě	k1gNnSc4	dítě
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
jak	jak	k6eAd1	jak
během	během	k7c2	během
vlastního	vlastní	k2eAgNnSc2d1	vlastní
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
během	během	k7c2	během
porodu	porod	k1gInSc2	porod
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
15	[number]	k4	15
–	–	k?	–
30	[number]	k4	30
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnou	vhodný	k2eAgFnSc7d1	vhodná
antivirovou	antivirový	k2eAgFnSc7d1	antivirová
léčbou	léčba	k1gFnSc7	léčba
během	během	k7c2	během
těhotenství	těhotenství	k1gNnSc2	těhotenství
a	a	k8xC	a
porodem	porod	k1gInSc7	porod
císařským	císařský	k2eAgInSc7d1	císařský
řezem	řez	k1gInSc7	řez
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
toto	tento	k3xDgNnSc4	tento
riziko	riziko	k1gNnSc4	riziko
významně	významně	k6eAd1	významně
snížit	snížit	k5eAaPmF	snížit
(	(	kIx(	(
<g/>
1	[number]	k4	1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
riziko	riziko	k1gNnSc1	riziko
přenosu	přenos	k1gInSc2	přenos
představuje	představovat	k5eAaImIp3nS	představovat
kojení	kojení	k1gNnSc4	kojení
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
HIV	HIV	kA	HIV
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
matky	matka	k1gFnPc4	matka
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
<g/>
-li	i	k?	-li
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
vyvarovat	vyvarovat	k5eAaPmF	vyvarovat
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
2-6	[number]	k4	2-6
týdnů	týden	k1gInPc2	týden
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
nespecifické	specifický	k2eNgInPc1d1	nespecifický
příznaky	příznak	k1gInPc1	příznak
akutní	akutní	k2eAgFnSc2d1	akutní
infekce	infekce	k1gFnSc2	infekce
HIV	HIV	kA	HIV
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnSc3d1	podobná
chřipce	chřipka	k1gFnSc3	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odeznění	odeznění	k1gNnSc6	odeznění
přechází	přecházet	k5eAaImIp3nS	přecházet
nemoc	nemoc	k1gFnSc1	nemoc
do	do	k7c2	do
latentního	latentní	k2eAgNnSc2d1	latentní
stadia	stadion	k1gNnSc2	stadion
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nesprávně	správně	k6eNd1	správně
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
<g/>
)	)	kIx)	)
během	během	k7c2	během
kterého	který	k3yRgInSc2	který
nemocný	nemocný	k1gMnSc1	nemocný
nepociťuje	pociťovat	k5eNaImIp3nS	pociťovat
žádné	žádný	k3yNgFnPc4	žádný
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
následuje	následovat	k5eAaImIp3nS	následovat
zhroucení	zhroucení	k1gNnSc3	zhroucení
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
–	–	k?	–
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgFnSc1d1	střední
doba	doba	k1gFnSc1	doba
mezi	mezi	k7c7	mezi
nákazou	nákaza	k1gFnSc7	nákaza
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
a	a	k8xC	a
propuknutím	propuknutí	k1gNnSc7	propuknutí
nemoci	nemoc	k1gFnSc2	nemoc
AIDS	AIDS	kA	AIDS
přibližně	přibližně	k6eAd1	přibližně
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
ale	ale	k9	ale
trvat	trvat	k5eAaImF	trvat
kratší	krátký	k2eAgFnSc4d2	kratší
i	i	k8xC	i
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
podíl	podíl	k1gInSc1	podíl
nemocných	nemocný	k1gMnPc2	nemocný
narůstá	narůstat	k5eAaImIp3nS	narůstat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
pět	pět	k4xCc4	pět
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Kongenitálně	kongenitálně	k6eAd1	kongenitálně
nebo	nebo	k8xC	nebo
perinatálně	perinatálně	k6eAd1	perinatálně
infikované	infikovaný	k2eAgFnPc1d1	infikovaná
děti	dítě	k1gFnPc1	dítě
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
většinou	většina	k1gFnSc7	většina
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
či	či	k8xC	či
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Testy	testa	k1gFnSc2	testa
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
HIV	HIV	kA	HIV
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
lze	lze	k6eAd1	lze
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
pomocí	pomocí	k7c2	pomocí
krevních	krevní	k2eAgInPc2d1	krevní
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
protilátek	protilátka	k1gFnPc2	protilátka
proti	proti	k7c3	proti
viru	vir	k1gInSc3	vir
HIV	HIV	kA	HIV
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
výsledek	výsledek	k1gInSc1	výsledek
testu	test	k1gInSc2	test
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
organismus	organismus	k1gInSc1	organismus
proti	proti	k7c3	proti
viru	vir	k1gInSc3	vir
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
protilátky	protilátka	k1gFnPc4	protilátka
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
jím	on	k3xPp3gMnSc7	on
tedy	tedy	k9	tedy
infikován	infikován	k2eAgInSc4d1	infikován
<g/>
.	.	kIx.	.
</s>
<s>
Protilátky	protilátka	k1gFnPc1	protilátka
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
dostatečném	dostatečný	k2eAgNnSc6d1	dostatečné
množství	množství	k1gNnSc6	množství
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
až	až	k9	až
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
po	po	k7c6	po
nákaze	nákaza	k1gFnSc6	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
doba	doba	k1gFnSc1	doba
do	do	k7c2	do
nástupu	nástup	k1gInSc2	nástup
protilátek	protilátka	k1gFnPc2	protilátka
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
týdnech	týden	k1gInPc6	týden
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
přítomnost	přítomnost	k1gFnSc1	přítomnost
HIV	HIV	kA	HIV
verifikovat	verifikovat	k5eAaBmF	verifikovat
průkazem	průkaz	k1gInSc7	průkaz
antigenu	antigen	k1gInSc2	antigen
p	p	k?	p
<g/>
24	[number]	k4	24
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c6	po
týdnu	týden	k1gInSc6	týden
po	po	k7c6	po
akvírování	akvírování	k?	akvírování
infekce	infekce	k1gFnSc1	infekce
je	být	k5eAaImIp3nS	být
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
reakce	reakce	k1gFnPc4	reakce
PCR	PCR	kA	PCR
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
lékařská	lékařský	k2eAgFnSc1d1	lékařská
věda	věda	k1gFnSc1	věda
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
AIDS	AIDS	kA	AIDS
vyléčit	vyléčit	k5eAaPmF	vyléčit
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
ale	ale	k8xC	ale
oddálit	oddálit	k5eAaPmF	oddálit
jeho	jeho	k3xOp3gNnSc4	jeho
propuknutí	propuknutí	k1gNnSc4	propuknutí
a	a	k8xC	a
zpomalit	zpomalit	k5eAaPmF	zpomalit
jeho	on	k3xPp3gInSc4	on
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Nejúčinnější	účinný	k2eAgFnSc1d3	nejúčinnější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
použití	použití	k1gNnSc4	použití
antiretrovirových	antiretrovirův	k2eAgFnPc2d1	antiretrovirův
virostatik	virostatika	k1gFnPc2	virostatika
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
tzv.	tzv.	kA	tzv.
kombinované	kombinovaný	k2eAgFnSc2d1	kombinovaná
antiretrovirové	antiretrovirový	k2eAgFnSc2d1	antiretrovirová
terapie	terapie	k1gFnSc2	terapie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stadiu	stadion	k1gNnSc6	stadion
AIDS	AIDS	kA	AIDS
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
léčby	léčba	k1gFnSc2	léčba
také	také	k9	také
potlačování	potlačování	k1gNnSc4	potlačování
oportunních	oportunní	k2eAgFnPc2d1	oportunní
infekcí	infekce	k1gFnPc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
v	v	k7c6	v
chudých	chudý	k2eAgFnPc6d1	chudá
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
nedostupnost	nedostupnost	k1gFnSc1	nedostupnost
příliš	příliš	k6eAd1	příliš
drahých	drahý	k2eAgInPc2d1	drahý
léků	lék	k1gInPc2	lék
a	a	k8xC	a
nepříjemné	příjemný	k2eNgInPc1d1	nepříjemný
vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
účinky	účinek	k1gInPc1	účinek
některých	některý	k3yIgInPc2	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Prognóza	prognóza	k1gFnSc1	prognóza
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
rozvinutosti	rozvinutost	k1gFnSc6	rozvinutost
infekce	infekce	k1gFnSc2	infekce
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
-li	i	k?	-li
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
nakazil	nakazit	k5eAaPmAgMnS	nakazit
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
dostupné	dostupný	k2eAgInPc4d1	dostupný
vhodné	vhodný	k2eAgInPc4d1	vhodný
léky	lék	k1gInPc4	lék
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
prožít	prožít	k5eAaPmF	prožít
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
AIDS	aids	k1gInSc1	aids
vůbec	vůbec	k9	vůbec
projevil	projevit	k5eAaPmAgInS	projevit
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
vedený	vedený	k2eAgInSc1d1	vedený
Kristinou	Kristina	k1gFnSc7	Kristina
Allers	Allersa	k1gFnPc2	Allersa
z	z	k7c2	z
Charité	Charitý	k2eAgFnSc2d1	Charitý
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
nález	nález	k1gInSc1	nález
pravděpodobného	pravděpodobný	k2eAgNnSc2d1	pravděpodobné
odstranění	odstranění	k1gNnSc2	odstranění
(	(	kIx(	(
<g/>
3,5	[number]	k4	3,5
roku	rok	k1gInSc2	rok
po	po	k7c6	po
terapii	terapie	k1gFnSc6	terapie
je	být	k5eAaImIp3nS	být
pacient	pacient	k1gMnSc1	pacient
bez	bez	k7c2	bez
známek	známka	k1gFnPc2	známka
přítomnosti	přítomnost	k1gFnSc2	přítomnost
viru	vir	k1gInSc2	vir
<g/>
)	)	kIx)	)
viru	vir	k1gInSc2	vir
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
efekt	efekt	k1gInSc4	efekt
terapie	terapie	k1gFnSc2	terapie
akutní	akutní	k2eAgFnSc2d1	akutní
myeloidní	myeloidní	k2eAgFnSc2d1	myeloidní
leukémie	leukémie	k1gFnSc2	leukémie
spočívající	spočívající	k2eAgFnSc2d1	spočívající
v	v	k7c6	v
transplantaci	transplantace	k1gFnSc6	transplantace
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
<g/>
..	..	k?	..
Roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
uveřejněna	uveřejněn	k2eAgFnSc1d1	uveřejněna
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
lékařům	lékař	k1gMnPc3	lékař
v	v	k7c6	v
USA	USA	kA	USA
podařilo	podařit	k5eAaPmAgNnS	podařit
vyléčit	vyléčit	k5eAaPmF	vyléčit
HIV	HIV	kA	HIV
pozitivní	pozitivní	k2eAgNnSc1d1	pozitivní
batole	batole	k1gNnSc1	batole
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
vyléčení	vyléčení	k1gNnSc6	vyléčení
dalšího	další	k2eAgNnSc2d1	další
HIV	HIV	kA	HIV
pozitivního	pozitivní	k2eAgNnSc2d1	pozitivní
dítěte	dítě	k1gNnSc2	dítě
přišla	přijít	k5eAaPmAgFnS	přijít
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
Šíření	šíření	k1gNnSc4	šíření
pandemie	pandemie	k1gFnSc2	pandemie
AIDS	AIDS	kA	AIDS
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
problémů	problém	k1gInPc2	problém
řady	řada	k1gFnSc2	řada
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
okolo	okolo	k7c2	okolo
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
nakažených	nakažený	k2eAgMnPc2d1	nakažený
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
afrických	africký	k2eAgFnPc6d1	africká
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
nakažena	nakazit	k5eAaPmNgFnS	nakazit
i	i	k9	i
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
postižena	postižen	k2eAgFnSc1d1	postižena
je	být	k5eAaImIp3nS	být
také	také	k9	také
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pak	pak	k6eAd1	pak
země	zem	k1gFnSc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
nakažených	nakažený	k2eAgMnPc2d1	nakažený
HIV	HIV	kA	HIV
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Nigerii	Nigerie	k1gFnSc6	Nigerie
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
postižení	postižený	k1gMnPc1	postižený
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
mladí	mladý	k2eAgMnPc1d1	mladý
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
produktivním	produktivní	k2eAgInSc6d1	produktivní
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
šíření	šíření	k1gNnSc3	šíření
AIDS	AIDS	kA	AIDS
zásadní	zásadní	k2eAgInPc4d1	zásadní
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Studie	studie	k1gFnSc1	studie
Světové	světový	k2eAgFnSc2d1	světová
banky	banka	k1gFnSc2	banka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
postihne	postihnout	k5eAaPmIp3nS	postihnout
virus	virus	k1gInSc1	virus
HIV	HIV	kA	HIV
8	[number]	k4	8
%	%	kIx~	%
a	a	k8xC	a
více	hodně	k6eAd2	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
růst	růst	k1gInSc1	růst
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
zpomalovat	zpomalovat	k5eAaImF	zpomalovat
<g/>
,	,	kIx,	,
pracovní	pracovní	k2eAgFnPc1d1	pracovní
síly	síla	k1gFnPc1	síla
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
ubývá	ubývat	k5eAaImIp3nS	ubývat
a	a	k8xC	a
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
a	a	k8xC	a
sociální	sociální	k2eAgInSc1d1	sociální
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
přetížen	přetížen	k2eAgInSc1d1	přetížen
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důsledků	důsledek	k1gInPc2	důsledek
pandemie	pandemie	k1gFnSc2	pandemie
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
miliony	milion	k4xCgInPc1	milion
dětí	dítě	k1gFnPc2	dítě
bez	bez	k7c2	bez
rodičů	rodič	k1gMnPc2	rodič
–	–	k?	–
tzv.	tzv.	kA	tzv.
sirotci	sirotek	k1gMnPc1	sirotek
AIDS	AIDS	kA	AIDS
(	(	kIx(	(
<g/>
AIDS	AIDS	kA	AIDS
orphans	orphans	k1gInSc1	orphans
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejohroženější	ohrožený	k2eAgFnPc4d3	nejohroženější
skupiny	skupina	k1gFnPc4	skupina
patří	patřit	k5eAaImIp3nS	patřit
mladé	mladý	k2eAgFnPc4d1	mladá
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
dívky	dívka	k1gFnPc4	dívka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nakazí	nakazit	k5eAaPmIp3nP	nakazit
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
manželů	manžel	k1gMnPc2	manžel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
společensky	společensky	k6eAd1	společensky
neúnosné	únosný	k2eNgNnSc4d1	neúnosné
stigma	stigma	k1gNnSc4	stigma
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
lidé	člověk	k1gMnPc1	člověk
svou	svůj	k3xOyFgFnSc4	svůj
nemoc	nemoc	k1gFnSc4	nemoc
tají	tajit	k5eAaImIp3nP	tajit
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
řešení	řešení	k1gNnSc6	řešení
problému	problém	k1gInSc2	problém
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
přístup	přístup	k1gInSc1	přístup
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
rozběhly	rozběhnout	k5eAaPmAgFnP	rozběhnout
vládní	vládní	k2eAgInPc4d1	vládní
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
proti	proti	k7c3	proti
pandemii	pandemie	k1gFnSc3	pandemie
účinně	účinně	k6eAd1	účinně
bojovat	bojovat	k5eAaImF	bojovat
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
se	se	k3xPyFc4	se
na	na	k7c4	na
informační	informační	k2eAgFnPc4d1	informační
kampaně	kampaň	k1gFnPc4	kampaň
<g/>
,	,	kIx,	,
nabízejí	nabízet	k5eAaImIp3nP	nabízet
testování	testování	k1gNnSc4	testování
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
těhotné	těhotný	k2eAgFnPc1d1	těhotná
ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
někde	někde	k6eAd1	někde
testují	testovat	k5eAaImIp3nP	testovat
automaticky	automaticky	k6eAd1	automaticky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
již	již	k6eAd1	již
nemocní	nemocný	k1gMnPc1	nemocný
mohou	moct	k5eAaImIp3nP	moct
získat	získat	k5eAaPmF	získat
zdarma	zdarma	k6eAd1	zdarma
i	i	k9	i
antiretrovirální	antiretrovirální	k2eAgInPc1d1	antiretrovirální
léky	lék	k1gInPc1	lék
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zpomalují	zpomalovat	k5eAaImIp3nP	zpomalovat
postup	postup	k1gInSc4	postup
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnPc1	vláda
nejchudších	chudý	k2eAgFnPc2d3	nejchudší
zemí	zem	k1gFnPc2	zem
však	však	k9	však
na	na	k7c4	na
problém	problém	k1gInSc4	problém
samy	sám	k3xTgMnPc4	sám
nestačí	stačit	k5eNaBmIp3nS	stačit
<g/>
.	.	kIx.	.
</s>
<s>
Antiretrovirální	Antiretrovirální	k2eAgInPc1d1	Antiretrovirální
léky	lék	k1gInPc1	lék
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
drahé	drahý	k2eAgInPc1d1	drahý
<g/>
:	:	kIx,	:
léčba	léčba	k1gFnSc1	léčba
jednoho	jeden	k4xCgMnSc2	jeden
nemocného	nemocný	k2eAgMnSc2d1	nemocný
vyjde	vyjít	k5eAaPmIp3nS	vyjít
nejméně	málo	k6eAd3	málo
na	na	k7c4	na
200	[number]	k4	200
dolarů	dolar	k1gInPc2	dolar
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
například	například	k6eAd1	například
v	v	k7c6	v
Zambii	Zambie	k1gFnSc6	Zambie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nakaženo	nakazit	k5eAaPmNgNnS	nakazit
16,5	[number]	k4	16,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
HND	HND	kA	HND
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
380	[number]	k4	380
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Možným	možný	k2eAgNnSc7d1	možné
řešením	řešení	k1gNnSc7	řešení
jsou	být	k5eAaImIp3nP	být
generika	generik	k1gMnSc2	generik
a	a	k8xC	a
také	také	k9	také
podpora	podpora	k1gFnSc1	podpora
od	od	k7c2	od
vyspělých	vyspělý	k2eAgFnPc2d1	vyspělá
zemí	zem	k1gFnPc2	zem
-	-	kIx~	-
ať	ať	k8xS	ať
už	už	k6eAd1	už
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
různých	různý	k2eAgInPc2d1	různý
programů	program	k1gInPc2	program
velkých	velký	k2eAgFnPc2d1	velká
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
nebo	nebo	k8xC	nebo
skrze	skrze	k?	skrze
menší	malý	k2eAgInPc4d2	menší
projekty	projekt	k1gInPc4	projekt
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Zastavit	zastavit	k5eAaPmF	zastavit
a	a	k8xC	a
zvrátit	zvrátit	k5eAaPmF	zvrátit
pandemii	pandemie	k1gFnSc4	pandemie
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
Rozvojových	rozvojový	k2eAgInPc2d1	rozvojový
cílů	cíl	k1gInPc2	cíl
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
,	,	kIx,	,
programu	program	k1gInSc2	program
OSN	OSN	kA	OSN
na	na	k7c6	na
odstranění	odstranění	k1gNnSc6	odstranění
největších	veliký	k2eAgInPc2d3	veliký
problémů	problém	k1gInPc2	problém
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
masové	masový	k2eAgFnSc3d1	masová
imigraci	imigrace	k1gFnSc3	imigrace
se	se	k3xPyFc4	se
epidemie	epidemie	k1gFnPc1	epidemie
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
šíří	šířit	k5eAaImIp3nP	šířit
i	i	k8xC	i
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
nárůstu	nárůst	k1gInSc2	nárůst
počtu	počet	k1gInSc2	počet
nakažených	nakažený	k2eAgInPc2d1	nakažený
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
jsou	být	k5eAaImIp3nP	být
afričtí	africký	k2eAgMnPc1d1	africký
imigranti	imigrant	k1gMnPc1	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
nemocní	mocnit	k5eNaImIp3nP	mocnit
AIDS	aids	k1gInSc4	aids
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
však	však	k9	však
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
museli	muset	k5eAaImAgMnP	muset
často	často	k6eAd1	často
potýkat	potýkat	k5eAaImF	potýkat
vedle	vedle	k7c2	vedle
své	svůj	k3xOyFgFnSc2	svůj
nemoci	nemoc	k1gFnSc2	nemoc
také	také	k9	také
se	s	k7c7	s
společenskými	společenský	k2eAgInPc7d1	společenský
předsudky	předsudek	k1gInPc7	předsudek
či	či	k8xC	či
s	s	k7c7	s
vyčleněním	vyčlenění	k1gNnSc7	vyčlenění
z	z	k7c2	z
normálního	normální	k2eAgInSc2d1	normální
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
tendence	tendence	k1gFnPc1	tendence
vrcholily	vrcholit	k5eAaImAgFnP	vrcholit
koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
nesměli	smět	k5eNaImAgMnP	smět
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
cestovat	cestovat	k5eAaImF	cestovat
lidé	člověk	k1gMnPc1	člověk
nakažení	nakažení	k1gNnSc2	nakažení
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
znemožňoval	znemožňovat	k5eAaImAgInS	znemožňovat
cestu	cesta	k1gFnSc4	cesta
studentům	student	k1gMnPc3	student
či	či	k8xC	či
turistům	turist	k1gMnPc3	turist
<g/>
,	,	kIx,	,
komplikoval	komplikovat	k5eAaBmAgInS	komplikovat
adopci	adopce	k1gFnSc4	adopce
dětí	dítě	k1gFnPc2	dítě
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
také	také	k9	také
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
nemohl	moct	k5eNaImAgInS	moct
konat	konat	k5eAaImF	konat
žádný	žádný	k3yNgInSc1	žádný
významný	významný	k2eAgInSc1d1	významný
světový	světový	k2eAgInSc1d1	světový
summit	summit	k1gInSc1	summit
o	o	k7c4	o
nemoci	nemoc	k1gFnPc4	nemoc
AIDS	AIDS	kA	AIDS
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
významných	významný	k2eAgMnPc2d1	významný
nakažených	nakažený	k2eAgMnPc2d1	nakažený
vědců	vědec	k1gMnPc2	vědec
nebo	nebo	k8xC	nebo
aktivistů	aktivista	k1gMnPc2	aktivista
se	se	k3xPyFc4	se
nemohla	moct	k5eNaImAgFnS	moct
dostavit	dostavit	k5eAaPmF	dostavit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kontroverzní	kontroverzní	k2eAgInSc1d1	kontroverzní
zákon	zákon	k1gInSc1	zákon
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
USA	USA	kA	USA
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
neumožňovaly	umožňovat	k5eNaImAgFnP	umožňovat
vstup	vstup	k1gInSc4	vstup
lidem	lido	k1gNnSc7	lido
s	s	k7c7	s
HIV	HIV	kA	HIV
-	-	kIx~	-
těmito	tento	k3xDgFnPc7	tento
zeměmi	zem	k1gFnPc7	zem
stále	stále	k6eAd1	stále
jsou	být	k5eAaImIp3nP	být
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Brunej	Brunej	k1gFnSc1	Brunej
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc1	Irák
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
<g/>
,	,	kIx,	,
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
,	,	kIx,	,
Fidži	Fidž	k1gFnSc6	Fidž
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
Arménie	Arménie	k1gFnSc1	Arménie
<g/>
,	,	kIx,	,
a	a	k8xC	a
Súdán	Súdán	k1gInSc1	Súdán
<g/>
.	.	kIx.	.
</s>
