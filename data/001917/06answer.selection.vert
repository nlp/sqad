<s>
Cytoplazmatická	Cytoplazmatický	k2eAgFnSc1d1	Cytoplazmatická
membrána	membrána	k1gFnSc1	membrána
(	(	kIx(	(
<g/>
též	též	k9	též
plazmalema	plazmalema	k1gNnSc1	plazmalema
nebo	nebo	k8xC	nebo
plasmalema	plasmalema	k1gNnSc1	plasmalema
<g/>
;	;	kIx,	;
plazmatická	plazmatický	k2eAgFnSc1d1	plazmatická
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
zkr.	zkr.	kA	zkr.
PM	PM	kA	PM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
semipermeabilní	semipermeabilní	k2eAgFnSc1d1	semipermeabilní
(	(	kIx(	(
<g/>
polopropustná	polopropustný	k2eAgFnSc1d1	polopropustná
<g/>
)	)	kIx)	)
membrána	membrána	k1gFnSc1	membrána
uzavírající	uzavírající	k2eAgFnSc1d1	uzavírající
obsah	obsah	k1gInSc4	obsah
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
