<s>
Prvními	první	k4xOgFnPc7
opatřeními	opatření	k1gNnPc7
její	její	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
tedy	tedy	k9
bylo	být	k5eAaImAgNnS
snižování	snižování	k1gNnSc1
vládních	vládní	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
<g/>
,	,	kIx,
snížení	snížení	k1gNnSc1
<g/>
,	,	kIx,
či	či	k8xC
zastavení	zastavení	k1gNnSc1
podpory	podpora	k1gFnSc2
krachujícím	krachující	k2eAgInPc3d1
podnikům	podnik	k1gInPc3
<g/>
,	,	kIx,
privatizace	privatizace	k1gFnSc2
podniků	podnik	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
ještě	ještě	k9
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
zachránit	zachránit	k5eAaPmF
a	a	k8xC
stabilizace	stabilizace	k1gFnSc2
cen	cena	k1gFnPc2
<g/>
.	.	kIx.
</s>