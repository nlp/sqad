<s>
Okamžitým	okamžitý	k2eAgInSc7d1	okamžitý
výsledkem	výsledek	k1gInSc7	výsledek
těchto	tento	k3xDgNnPc2	tento
opatření	opatření	k1gNnPc2	opatření
bylo	být	k5eAaImAgNnS	být
prudké	prudký	k2eAgNnSc1d1	prudké
zvýšení	zvýšení	k1gNnSc1	zvýšení
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
z	z	k7c2	z
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
z	z	k7c2	z
necelých	celý	k2eNgNnPc2d1	necelé
5	[number]	k4	5
%	%	kIx~	%
na	na	k7c6	na
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
12	[number]	k4	12
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
nebyla	být	k5eNaImAgFnS	být
Británie	Británie	k1gFnSc1	Británie
ani	ani	k8xC	ani
zvyklá	zvyklý	k2eAgFnSc1d1	zvyklá
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
připravená	připravený	k2eAgFnSc1d1	připravená
<g/>
.	.	kIx.	.
</s>
