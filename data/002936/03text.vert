<s>
Oliver	Oliver	k1gInSc1	Oliver
Twist	twist	k1gInSc1	twist
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
Charlese	Charles	k1gMnSc2	Charles
Dickense	Dickens	k1gMnSc2	Dickens
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Chapman	Chapman	k1gMnSc1	Chapman
&	&	k?	&
Hall	Hall	k1gMnSc1	Hall
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
také	také	k9	také
mnohokrát	mnohokrát	k6eAd1	mnohokrát
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
ho	on	k3xPp3gMnSc4	on
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
polský	polský	k2eAgMnSc1d1	polský
režisér	režisér	k1gMnSc1	režisér
Roman	Roman	k1gMnSc1	Roman
Polanski	Polanski	k1gNnPc2	Polanski
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
dosud	dosud	k6eAd1	dosud
nejznámější	známý	k2eAgNnPc1d3	nejznámější
a	a	k8xC	a
také	také	k9	také
nejkvalitnější	kvalitní	k2eAgNnSc1d3	nejkvalitnější
filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
Olivera	Oliver	k1gMnSc2	Oliver
Twista	Twist	k1gMnSc2	Twist
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
režisér	režisér	k1gMnSc1	režisér
David	David	k1gMnSc1	David
Lean	Lean	k1gMnSc1	Lean
<g/>
.	.	kIx.	.
</s>
<s>
Oliverovi	Oliverův	k2eAgMnPc1d1	Oliverův
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
narození	narození	k1gNnSc6	narození
v	v	k7c6	v
místním	místní	k2eAgInSc6d1	místní
chudobinci	chudobinec	k1gInSc6	chudobinec
<g/>
/	/	kIx~	/
<g/>
sirotčinci	sirotčinec	k1gInSc6	sirotčinec
umírá	umírat	k5eAaImIp3nS	umírat
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
není	být	k5eNaImIp3nS	být
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dítěte	dítě	k1gNnSc2	dítě
ujal	ujmout	k5eAaPmAgMnS	ujmout
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
jeho	jeho	k3xOp3gMnSc7	jeho
"	"	kIx"	"
<g/>
zajatcem	zajatec	k1gMnSc7	zajatec
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
jméno	jméno	k1gNnSc1	jméno
mu	on	k3xPp3gMnSc3	on
tehdy	tehdy	k6eAd1	tehdy
určila	určit	k5eAaPmAgFnS	určit
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Sirotek	Sirotek	k1gMnSc1	Sirotek
Oliver	Oliver	k1gMnSc1	Oliver
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
v	v	k7c6	v
chudobinci	chudobinec	k1gInSc6	chudobinec
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
krutého	krutý	k2eAgMnSc2d1	krutý
a	a	k8xC	a
nelidského	lidský	k2eNgMnSc2d1	nelidský
pana	pan	k1gMnSc2	pan
Bumbla	Bumbla	k1gMnSc2	Bumbla
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
všem	všecek	k3xTgMnPc3	všecek
na	na	k7c4	na
obtíž	obtíž	k1gFnSc4	obtíž
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
lepší	dobrý	k2eAgNnSc1d2	lepší
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
raději	rád	k6eAd2	rád
umřel	umřít	k5eAaPmAgMnS	umřít
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
v	v	k7c6	v
sirotčinci	sirotčinec	k1gInSc6	sirotčinec
trpí	trpět	k5eAaImIp3nS	trpět
podvýživou	podvýživa	k1gFnSc7	podvýživa
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
Oliver	Oliver	k1gMnSc1	Oliver
požádá	požádat	k5eAaPmIp3nS	požádat
o	o	k7c4	o
jídlo	jídlo	k1gNnSc4	jídlo
navíc	navíc	k6eAd1	navíc
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nakonec	nakonec	k6eAd1	nakonec
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
vyhazovu	vyhazov	k1gInSc3	vyhazov
ze	z	k7c2	z
sirotčince	sirotčinec	k1gInSc2	sirotčinec
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
sobě	se	k3xPyFc3	se
jako	jako	k9	jako
učedníka	učedník	k1gMnSc4	učedník
vezme	vzít	k5eAaPmIp3nS	vzít
pan	pan	k1gMnSc1	pan
Sowerberry	Sowerberra	k1gFnSc2	Sowerberra
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
pohřební	pohřební	k2eAgInSc4d1	pohřební
ústav	ústav	k1gInSc4	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Oliver	Oliver	k1gMnSc1	Oliver
se	se	k3xPyFc4	se
panu	pan	k1gMnSc3	pan
Sowerberrymu	Sowerberrym	k1gInSc3	Sowerberrym
velmi	velmi	k6eAd1	velmi
zamlouvá	zamlouvat	k5eAaImIp3nS	zamlouvat
<g/>
;	;	kIx,	;
bohužel	bohužel	k9	bohužel
vše	všechen	k3xTgNnSc1	všechen
narušuje	narušovat	k5eAaImIp3nS	narušovat
Sowerberryho	Sowerberry	k1gMnSc4	Sowerberry
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
naopak	naopak	k6eAd1	naopak
Olivera	Olivera	k1gFnSc1	Olivera
nenávidí	návidět	k5eNaImIp3nS	návidět
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
vypudit	vypudit	k5eAaPmF	vypudit
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
bez	bez	k7c2	bez
přičinění	přičinění	k1gNnSc2	přičinění
manželky	manželka	k1gFnSc2	manželka
pana	pan	k1gMnSc2	pan
Sowerberryho	Sowerberry	k1gMnSc2	Sowerberry
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Oliverovi	Oliverův	k2eAgMnPc1d1	Oliverův
vůbec	vůbec	k9	vůbec
nelíbí	líbit	k5eNaImIp3nP	líbit
a	a	k8xC	a
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
se	se	k3xPyFc4	se
o	o	k7c4	o
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
posléze	posléze	k6eAd1	posléze
přece	přece	k8xC	přece
jen	jen	k6eAd1	jen
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Oliver	Oliver	k1gMnSc1	Oliver
se	se	k3xPyFc4	se
útěkem	útěk	k1gInSc7	útěk
od	od	k7c2	od
pana	pan	k1gMnSc2	pan
Sowerberryho	Sowerberry	k1gMnSc2	Sowerberry
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nP	připojit
k	k	k7c3	k
bandě	banda	k1gFnSc3	banda
mladých	mladý	k2eAgMnPc2d1	mladý
zlodějíčků	zlodějíček	k1gMnPc2	zlodějíček
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vede	vést	k5eAaImIp3nS	vést
starý	starý	k2eAgMnSc1d1	starý
a	a	k8xC	a
prohnaný	prohnaný	k2eAgMnSc1d1	prohnaný
Žid	Žid	k1gMnSc1	Žid
Fagin	Fagin	k1gMnSc1	Fagin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
zlodějíčkům	zlodějíček	k1gMnPc3	zlodějíček
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
za	za	k7c4	za
protislužby	protislužba	k1gFnPc4	protislužba
přístřeší	přístřeší	k1gNnSc4	přístřeší
<g/>
,	,	kIx,	,
jídlo	jídlo	k1gNnSc4	jídlo
a	a	k8xC	a
rady	rada	k1gFnPc4	rada
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
koho	kdo	k3yInSc4	kdo
okrást	okrást	k5eAaPmF	okrást
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zloději	zloděj	k1gMnPc7	zloděj
v	v	k7c6	v
bandě	banda	k1gFnSc6	banda
vyniká	vynikat	k5eAaImIp3nS	vynikat
John	John	k1gMnSc1	John
Dawkins	Dawkins	k1gInSc1	Dawkins
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
nikdo	nikdo	k3yNnSc1	nikdo
neřekne	říct	k5eNaPmIp3nS	říct
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
Ferina	ferina	k1gMnSc1	ferina
Lišák	lišák	k1gMnSc1	lišák
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
Oliverovým	Oliverův	k2eAgMnSc7d1	Oliverův
učitelem	učitel	k1gMnSc7	učitel
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
krádeže	krádež	k1gFnPc4	krádež
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
i	i	k9	i
lupič	lupič	k1gMnSc1	lupič
Bill	Bill	k1gMnSc1	Bill
Sikes	Sikes	k1gMnSc1	Sikes
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
společnicí	společnice	k1gFnSc7	společnice
Nancy	Nancy	k1gFnSc7	Nancy
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Billa	Bill	k1gMnSc4	Bill
miluje	milovat	k5eAaImIp3nS	milovat
a	a	k8xC	a
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gInSc3	on
opustit	opustit	k5eAaPmF	opustit
bandu	banda	k1gFnSc4	banda
starého	starý	k2eAgNnSc2d1	staré
Fagina	Fagino	k1gNnSc2	Fagino
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Oliver	Oliver	k1gInSc1	Oliver
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
pobude	pobýt	k5eAaPmIp3nS	pobýt
<g/>
;	;	kIx,	;
neřeší	řešit	k5eNaImIp3nS	řešit
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
činí	činit	k5eAaImIp3nS	činit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
střechu	střecha	k1gFnSc4	střecha
nad	nad	k7c7	nad
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
jídlo	jídlo	k1gNnSc4	jídlo
a	a	k8xC	a
místo	místo	k7c2	místo
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
k	k	k7c3	k
večeru	večer	k1gInSc3	večer
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
krádeží	krádež	k1gFnPc2	krádež
je	být	k5eAaImIp3nS	být
však	však	k9	však
Oliver	Oliver	k1gInSc1	Oliver
náhle	náhle	k6eAd1	náhle
chycen	chytit	k5eAaPmNgInS	chytit
za	za	k7c4	za
zločin	zločin	k1gInSc4	zločin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nespáchal	spáchat	k5eNaPmAgInS	spáchat
<g/>
,	,	kIx,	,
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
ho	on	k3xPp3gNnSc4	on
před	před	k7c7	před
rozsudkem	rozsudek	k1gInSc7	rozsudek
soudce	soudce	k1gMnSc1	soudce
zachrání	zachránit	k5eAaPmIp3nS	zachránit
dobrotivý	dobrotivý	k2eAgMnSc1d1	dobrotivý
starý	starý	k2eAgMnSc1d1	starý
pan	pan	k1gMnSc1	pan
Brownlow	Brownlow	k1gMnSc1	Brownlow
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Oliverovi	Oliver	k1gMnSc3	Oliver
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
nový	nový	k2eAgInSc4d1	nový
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
pan	pan	k1gMnSc1	pan
Brownlow	Brownlow	k1gMnSc1	Brownlow
chce	chtít	k5eAaImIp3nS	chtít
zjistit	zjistit	k5eAaPmF	zjistit
Oliverovu	Oliverův	k2eAgFnSc4d1	Oliverova
oddanost	oddanost	k1gFnSc4	oddanost
a	a	k8xC	a
posílá	posílat	k5eAaImIp3nS	posílat
jej	on	k3xPp3gInSc4	on
s	s	k7c7	s
knihami	kniha	k1gFnPc7	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
Oliverovi	Oliver	k1gMnSc6	Oliver
skutečně	skutečně	k6eAd1	skutečně
nepřeje	přát	k5eNaImIp3nS	přát
a	a	k8xC	a
potkává	potkávat	k5eAaImIp3nS	potkávat
svou	svůj	k3xOyFgFnSc4	svůj
starou	starý	k2eAgFnSc4d1	stará
bandu	banda	k1gFnSc4	banda
zlodějíčků	zlodějíček	k1gMnPc2	zlodějíček
od	od	k7c2	od
starého	starý	k1gMnSc2	starý
žida	žid	k1gMnSc2	žid
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
jej	on	k3xPp3gNnSc4	on
unesou	unést	k5eAaPmIp3nP	unést
zpět	zpět	k6eAd1	zpět
<g/>
;	;	kIx,	;
za	za	k7c4	za
žádnou	žádný	k3yNgFnSc4	žádný
cenu	cena	k1gFnSc4	cena
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
obětního	obětní	k2eAgMnSc2d1	obětní
beránka	beránek	k1gMnSc2	beránek
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Oliver	Oliver	k1gInSc1	Oliver
nechtějí	chtít	k5eNaImIp3nP	chtít
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
bandě	banda	k1gFnSc6	banda
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
Edward	Edward	k1gMnSc1	Edward
Leeford	Leeford	k1gMnSc1	Leeford
alias	alias	k9	alias
Monks	Monks	k1gInSc4	Monks
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Oliverova	Oliverův	k2eAgMnSc4d1	Oliverův
nevlastního	vlastní	k2eNgMnSc4d1	nevlastní
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
prioritou	priorita	k1gFnSc7	priorita
je	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
Olivera	Oliver	k1gMnSc2	Oliver
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
dědit	dědit	k5eAaImF	dědit
po	po	k7c6	po
Oliverově	Oliverův	k2eAgMnSc6d1	Oliverův
otci	otec	k1gMnSc6	otec
<g/>
;	;	kIx,	;
jako	jako	k8xS	jako
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
syn	syn	k1gMnSc1	syn
by	by	kYmCp3nS	by
v	v	k7c6	v
případě	případ	k1gInSc6	případ
živého	živý	k2eAgMnSc2d1	živý
Olivera	Oliver	k1gMnSc2	Oliver
dostal	dostat	k5eAaPmAgMnS	dostat
pouze	pouze	k6eAd1	pouze
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
ba	ba	k9	ba
možná	možná	k9	možná
že	že	k8xS	že
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Oliver	Oliver	k1gInSc1	Oliver
pochopitelně	pochopitelně	k6eAd1	pochopitelně
nic	nic	k3yNnSc1	nic
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
netuší	tušit	k5eNaImIp3nS	tušit
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Oliverovi	Oliver	k1gMnSc3	Oliver
nezbývá	zbývat	k5eNaImIp3nS	zbývat
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
nechat	nechat	k5eAaPmF	nechat
unést	unést	k5eAaPmF	unést
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
krádeži	krádež	k1gFnSc6	krádež
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Maylieových	Maylieův	k2eAgFnPc2d1	Maylieův
je	být	k5eAaImIp3nS	být
vážně	vážně	k6eAd1	vážně
postřelen	postřelen	k2eAgMnSc1d1	postřelen
<g/>
.	.	kIx.	.
</s>
<s>
Olivera	Olivera	k1gFnSc1	Olivera
tehdy	tehdy	k6eAd1	tehdy
potká	potkat	k5eAaPmIp3nS	potkat
další	další	k2eAgNnSc1d1	další
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
:	:	kIx,	:
paní	paní	k1gFnSc1	paní
Maylieová	Maylieový	k2eAgFnSc1d1	Maylieový
se	se	k3xPyFc4	se
postřeleného	postřelený	k2eAgNnSc2d1	postřelené
Olivera	Olivero	k1gNnSc2	Olivero
ujímá	ujímat	k5eAaImIp3nS	ujímat
-	-	kIx~	-
alespoň	alespoň	k9	alespoň
do	do	k7c2	do
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
uzdraví	uzdravit	k5eAaPmIp3nP	uzdravit
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
její	její	k3xOp3gFnSc1	její
společnice	společnice	k1gFnSc1	společnice
Róza	Róza	k1gFnSc1	Róza
Maylieová	Maylieová	k1gFnSc1	Maylieová
<g/>
,	,	kIx,	,
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
paní	paní	k1gFnSc1	paní
Maylieová	Maylieová	k1gFnSc1	Maylieová
adoptovala	adoptovat	k5eAaPmAgFnS	adoptovat
<g/>
.	.	kIx.	.
</s>
<s>
Oliver	Oliver	k1gInSc1	Oliver
šťastně	šťastně	k6eAd1	šťastně
žije	žít	k5eAaImIp3nS	žít
u	u	k7c2	u
Maylieových	Maylieův	k2eAgInPc2d1	Maylieův
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
to	ten	k3xDgNnSc1	ten
netuší	tušit	k5eNaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
chudobinci	chudobinec	k1gInSc6	chudobinec
panu	pan	k1gMnSc3	pan
Bumbleovi	Bumbleus	k1gMnSc3	Bumbleus
svěří	svěřit	k5eAaPmIp3nS	svěřit
umírající	umírající	k2eAgFnSc1d1	umírající
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
okradla	okrást	k5eAaPmAgFnS	okrást
Oliverovu	Oliverův	k2eAgFnSc4d1	Oliverova
matku	matka	k1gFnSc4	matka
o	o	k7c4	o
malý	malý	k2eAgInSc4d1	malý
přívěsek	přívěsek	k1gInSc4	přívěsek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
Oliverův	Oliverův	k2eAgInSc4d1	Oliverův
původ	původ	k1gInSc4	původ
a	a	k8xC	a
vazby	vazba	k1gFnPc4	vazba
na	na	k7c6	na
Monkse	Monksa	k1gFnSc6	Monksa
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Bumble	Bumble	k1gFnSc2	Bumble
situace	situace	k1gFnSc1	situace
využije	využít	k5eAaPmIp3nS	využít
a	a	k8xC	a
prodá	prodat	k5eAaPmIp3nS	prodat
přívěsek	přívěsek	k1gInSc1	přívěsek
Edwardu	Edward	k1gMnSc3	Edward
Leefordovi	Leeforda	k1gMnSc3	Leeforda
alias	alias	k9	alias
Monksovi	Monks	k1gMnSc3	Monks
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
chopí	chopit	k5eAaPmIp3nP	chopit
se	se	k3xPyFc4	se
šance	šance	k1gFnSc2	šance
a	a	k8xC	a
přívěsku	přívěsek	k1gInSc2	přívěsek
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
členka	členka	k1gFnSc1	členka
bandy	bandy	k1gNnSc2	bandy
zlodějíčků	zlodějíček	k1gMnPc2	zlodějíček
pana	pan	k1gMnSc2	pan
Fagina	Fagin	k1gMnSc2	Fagin
<g/>
,	,	kIx,	,
Nancy	Nancy	k1gFnSc2	Nancy
<g/>
,	,	kIx,	,
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
pravý	pravý	k2eAgInSc4d1	pravý
Oliverův	Oliverův	k2eAgInSc4d1	Oliverův
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
nechtěně	chtěně	k6eNd1	chtěně
vyslechne	vyslechnout	k5eAaPmIp3nS	vyslechnout
rozhovor	rozhovor	k1gInSc1	rozhovor
starého	starý	k2eAgMnSc2d1	starý
žida	žid	k1gMnSc2	žid
s	s	k7c7	s
Monksem	Monks	k1gMnSc7	Monks
alias	alias	k9	alias
Edwardem	Edward	k1gMnSc7	Edward
Leefordem	Leeford	k1gInSc7	Leeford
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zmatená	zmatený	k2eAgFnSc1d1	zmatená
a	a	k8xC	a
jaksi	jaksi	k6eAd1	jaksi
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
dělat	dělat	k5eAaImF	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
vyhledá	vyhledat	k5eAaPmIp3nS	vyhledat
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
Olivera	Oliver	k1gMnSc4	Oliver
právě	právě	k6eAd1	právě
stará	starý	k2eAgFnSc1d1	stará
-	-	kIx~	-
Rózu	Róza	k1gFnSc4	Róza
Maylieovou	Maylieův	k2eAgFnSc7d1	Maylieův
a	a	k8xC	a
se	s	k7c7	s
vším	všecek	k3xTgNnSc7	všecek
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
svěří	svěřit	k5eAaPmIp3nS	svěřit
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
celý	celý	k2eAgInSc1d1	celý
její	její	k3xOp3gInSc1	její
příběh	příběh	k1gInSc1	příběh
dával	dávat	k5eAaImAgInS	dávat
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
i	i	k9	i
okolnosti	okolnost	k1gFnPc1	okolnost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
vším	všecek	k3xTgMnSc7	všecek
spjaté	spjatý	k2eAgFnPc1d1	spjatá
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
ji	on	k3xPp3gFnSc4	on
Róza	Róza	k1gFnSc1	Róza
nabízí	nabízet	k5eAaImIp3nS	nabízet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
od	od	k7c2	od
bandy	bandy	k1gNnSc2	bandy
zlodějíčků	zlodějíček	k1gMnPc2	zlodějíček
pana	pan	k1gMnSc2	pan
Fagina	Fagin	k1gMnSc2	Fagin
odešla	odejít	k5eAaPmAgFnS	odejít
<g/>
,	,	kIx,	,
odmítá	odmítat	k5eAaImIp3nS	odmítat
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
a	a	k8xC	a
možná	možná	k9	možná
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
Billovi	Bill	k1gMnSc3	Bill
Sikesovi	Sikes	k1gMnSc3	Sikes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
dobročinnost	dobročinnost	k1gFnSc4	dobročinnost
ale	ale	k8xC	ale
Nancy	Nancy	k1gFnSc4	Nancy
doplácí	doplácet	k5eAaImIp3nS	doplácet
<g/>
:	:	kIx,	:
Sikes	Sikes	k1gInSc1	Sikes
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
co	co	k8xS	co
vše	všechen	k3xTgNnSc1	všechen
prozradila	prozradit	k5eAaPmAgFnS	prozradit
a	a	k8xC	a
zabije	zabít	k5eAaPmIp3nS	zabít
ji	on	k3xPp3gFnSc4	on
<g/>
;	;	kIx,	;
zhnusen	zhnusen	k2eAgInSc1d1	zhnusen
svým	svůj	k3xOyFgInSc7	svůj
činem	čin	k1gInSc7	čin
utíká	utíkat	k5eAaImIp3nS	utíkat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
policií	policie	k1gFnSc7	policie
již	již	k6eAd1	již
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
<g/>
.	.	kIx.	.
</s>
<s>
Oliver	Oliver	k1gMnSc1	Oliver
se	se	k3xPyFc4	se
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Rózou	Róza	k1gFnSc7	Róza
Maylieovou	Maylieův	k2eAgFnSc7d1	Maylieův
dostává	dostávat	k5eAaImIp3nS	dostávat
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
minulosti	minulost	k1gFnSc2	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odkázán	odkázat	k5eAaPmNgMnS	odkázat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Pana	Pan	k1gMnSc4	Pan
Brownlowa	Brownlowus	k1gMnSc4	Brownlowus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
také	také	k9	také
navštíví	navštívit	k5eAaPmIp3nS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
šokovaný	šokovaný	k2eAgInSc1d1	šokovaný
Oliver	Oliver	k1gInSc1	Oliver
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
bohatý	bohatý	k2eAgMnSc1d1	bohatý
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
příteli	přítel	k1gMnSc6	přítel
Brownlowovi	Brownlowa	k1gMnSc6	Brownlowa
zanechal	zanechat	k5eAaPmAgMnS	zanechat
své	svůj	k3xOyFgInPc4	svůj
peníze	peníz	k1gInPc4	peníz
svým	svůj	k3xOyFgNnSc7	svůj
dvěma	dva	k4xCgMnPc3	dva
synům	syn	k1gMnPc3	syn
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
polovinou	polovina	k1gFnSc7	polovina
pro	pro	k7c4	pro
oba	dva	k4xCgMnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
dědictví	dědictví	k1gNnSc1	dědictví
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
a	a	k8xC	a
Monks	Monks	k1gInSc1	Monks
alias	alias	k9	alias
Edward	Edward	k1gMnSc1	Edward
Leeford	Leeford	k1gMnSc1	Leeford
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
peníze	peníz	k1gInPc1	peníz
rychle	rychle	k6eAd1	rychle
utratí	utratit	k5eAaPmIp3nP	utratit
a	a	k8xC	a
zemře	zemřít	k5eAaPmIp3nS	zemřít
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Fagin	Fagin	k1gMnSc1	Fagin
je	být	k5eAaImIp3nS	být
dopaden	dopadnout	k5eAaPmNgMnS	dopadnout
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
oběšením	oběšení	k1gNnPc3	oběšení
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
Bumblovi	Bumblův	k2eAgMnPc1d1	Bumblův
jsou	být	k5eAaImIp3nP	být
zbaveni	zbaven	k2eAgMnPc1d1	zbaven
svého	svůj	k3xOyFgNnSc2	svůj
úředního	úřední	k2eAgNnSc2d1	úřední
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
upadají	upadat	k5eAaPmIp3nP	upadat
do	do	k7c2	do
bídy	bída	k1gFnSc2	bída
a	a	k8xC	a
nouze	nouze	k1gFnSc2	nouze
a	a	k8xC	a
skončí	skončit	k5eAaPmIp3nP	skončit
jako	jako	k9	jako
obecní	obecní	k2eAgMnPc1d1	obecní
chovanci	chovanec	k1gMnPc1	chovanec
ve	v	k7c6	v
špitále	špitál	k1gInSc6	špitál
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
dříve	dříve	k6eAd2	dříve
"	"	kIx"	"
<g/>
vládli	vládnout	k5eAaImAgMnP	vládnout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Róza	Róza	k1gFnSc1	Róza
Maylieová	Maylieový	k2eAgFnSc1d1	Maylieový
je	být	k5eAaImIp3nS	být
sestra	sestra	k1gFnSc1	sestra
matky	matka	k1gFnSc2	matka
Olivera	Olivera	k1gFnSc1	Olivera
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jeho	jeho	k3xOp3gFnSc1	jeho
teta	teta	k1gFnSc1	teta
<g/>
;	;	kIx,	;
bere	brát	k5eAaImIp3nS	brát
si	se	k3xPyFc3	se
za	za	k7c2	za
muže	muž	k1gMnSc2	muž
syna	syn	k1gMnSc2	syn
paní	paní	k1gFnSc2	paní
Maylieové	Maylieová	k1gFnSc2	Maylieová
<g/>
,	,	kIx,	,
Harryho	Harry	k1gMnSc2	Harry
Mayliena	Maylien	k1gMnSc2	Maylien
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Oliver	Oliver	k1gInSc1	Oliver
je	být	k5eAaImIp3nS	být
adoptován	adoptovat	k5eAaPmNgInS	adoptovat
panem	pan	k1gMnSc7	pan
Brownlowen	Brownlowen	k1gInSc4	Brownlowen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
Oliverem	Oliver	k1gInSc7	Oliver
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
domu	dům	k1gInSc2	dům
Maylieových	Maylieův	k2eAgFnPc2d1	Maylieův
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Oliver	Oliver	k1gMnSc1	Oliver
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
životě	život	k1gInSc6	život
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
životního	životní	k2eAgInSc2d1	životní
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
vyrovnanosti	vyrovnanost	k1gFnSc2	vyrovnanost
a	a	k8xC	a
pochopení	pochopení	k1gNnSc2	pochopení
<g/>
.	.	kIx.	.
</s>
<s>
Oliver	Oliver	k1gInSc1	Oliver
Twist	twist	k1gInSc1	twist
-	-	kIx~	-
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
sirotek	sirotek	k1gMnSc1	sirotek
vyrůstající	vyrůstající	k2eAgMnSc1d1	vyrůstající
v	v	k7c6	v
chudobinci	chudobinec	k1gInSc6	chudobinec
Fagin	Fagin	k1gMnSc1	Fagin
-	-	kIx~	-
starý	starý	k2eAgMnSc1d1	starý
protřelý	protřelý	k2eAgMnSc1d1	protřelý
Žid	Žid	k1gMnSc1	Žid
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vede	vést	k5eAaImIp3nS	vést
skupinu	skupina	k1gFnSc4	skupina
mladých	mladý	k2eAgMnPc2d1	mladý
zlodějů	zloděj	k1gMnPc2	zloděj
Bill	Bill	k1gMnSc1	Bill
Sikes	Sikes	k1gMnSc1	Sikes
-	-	kIx~	-
brutální	brutální	k2eAgMnSc1d1	brutální
zloděj	zloděj	k1gMnSc1	zloděj
a	a	k8xC	a
surovec	surovec	k1gMnSc1	surovec
<g/>
,	,	kIx,	,
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
Nancy	Nancy	k1gFnSc4	Nancy
John	John	k1gMnSc1	John
Dawkins	Dawkinsa	k1gFnPc2	Dawkinsa
alias	alias	k9	alias
Ferina	ferina	k1gMnSc1	ferina
Lišák	lišák	k1gMnSc1	lišák
-	-	kIx~	-
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mladých	mladý	k2eAgMnPc2d1	mladý
zlodějů	zloděj	k1gMnPc2	zloděj
<g/>
,	,	kIx,	,
zaučuje	zaučovat	k5eAaImIp3nS	zaučovat
Olivera	Olivera	k1gFnSc1	Olivera
Čódl	Čódl	k1gMnSc1	Čódl
Bates	Bates	k1gMnSc1	Bates
-	-	kIx~	-
veselý	veselý	k2eAgMnSc1d1	veselý
zlodějíček	zlodějíček	k1gMnSc1	zlodějíček
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Faginových	Faginových	k2eAgFnSc2d1	Faginových
Nancy	Nancy	k1gFnSc2	Nancy
-	-	kIx~	-
Billova	Billův	k2eAgFnSc1d1	Billova
společnice	společnice	k1gFnSc1	společnice
<g/>
,	,	kIx,	,
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
Oliverův	Oliverův	k2eAgInSc4d1	Oliverův
původ	původ	k1gInSc4	původ
Noe	Noe	k1gMnSc1	Noe
Claypole	Claypole	k1gFnSc2	Claypole
-	-	kIx~	-
učedník	učedník	k1gMnSc1	učedník
Sowerberryho	Sowerberry	k1gMnSc2	Sowerberry
<g/>
,	,	kIx,	,
nesnáší	snášet	k5eNaImIp3nS	snášet
Olivera	Olivera	k1gFnSc1	Olivera
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
ho	on	k3xPp3gMnSc4	on
dostat	dostat	k5eAaPmF	dostat
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Sowerberryho	Sowerberry	k1gMnSc4	Sowerberry
okrade	okrást	k5eAaPmIp3nS	okrást
a	a	k8xC	a
přidá	přidat	k5eAaPmIp3nS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
Faginovi	Fagin	k1gMnSc3	Fagin
jako	jako	k8xS	jako
špeh	špeh	k1gInSc4	špeh
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Brownlow	Brownlow	k1gMnSc1	Brownlow
-	-	kIx~	-
dobrodušný	dobrodušný	k2eAgMnSc1d1	dobrodušný
starý	starý	k2eAgMnSc1d1	starý
pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
Oliverův	Oliverův	k2eAgMnSc1d1	Oliverův
zachránce	zachránce	k1gMnSc1	zachránce
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ho	on	k3xPp3gMnSc4	on
adoptuje	adoptovat	k5eAaPmIp3nS	adoptovat
Edward	Edward	k1gMnSc1	Edward
Leeford	Leeford	k1gMnSc1	Leeford
alias	alias	k9	alias
Monks	Monks	k1gInSc4	Monks
-	-	kIx~	-
Oliverův	Oliverův	k2eAgMnSc1d1	Oliverův
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
zničit	zničit	k5eAaPmF	zničit
Olivera	Oliver	k1gMnSc4	Oliver
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
tak	tak	k9	tak
dědictví	dědictví	k1gNnSc4	dědictví
Pan	Pan	k1gMnSc1	Pan
Losberne	Losbern	k1gInSc5	Losbern
-	-	kIx~	-
milý	milý	k2eAgMnSc1d1	milý
doktor	doktor	k1gMnSc1	doktor
a	a	k8xC	a
přítel	přítel	k1gMnSc1	přítel
rodiny	rodina	k1gFnSc2	rodina
Mayliů	Mayli	k1gMnPc2	Mayli
s	s	k7c7	s
prudkou	prudký	k2eAgFnSc7d1	prudká
povahou	povaha	k1gFnSc7	povaha
Paní	paní	k1gFnSc1	paní
Maylieová	Maylieová	k1gFnSc1	Maylieová
-	-	kIx~	-
hodná	hodný	k2eAgFnSc1d1	hodná
stará	starý	k2eAgFnSc1d1	stará
paní	paní	k1gFnSc1	paní
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
adoptuje	adoptovat	k5eAaPmIp3nS	adoptovat
Rózu	Róza	k1gFnSc4	Róza
<g/>
.	.	kIx.	.
</s>
<s>
Pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
Olivera	Oliver	k1gMnSc4	Oliver
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
domě	dům	k1gInSc6	dům
při	při	k7c6	při
krádeži	krádež	k1gFnSc6	krádež
postřelen	postřelit	k5eAaPmNgInS	postřelit
Róza	Róza	k1gFnSc1	Róza
Maylieová	Maylieový	k2eAgFnSc1d1	Maylieový
-	-	kIx~	-
citlivá	citlivý	k2eAgFnSc1d1	citlivá
a	a	k8xC	a
laskavá	laskavý	k2eAgFnSc1d1	laskavá
dívka	dívka	k1gFnSc1	dívka
adoptovaná	adoptovaný	k2eAgFnSc1d1	adoptovaná
paní	paní	k1gFnSc7	paní
Mayliovou	Mayliový	k2eAgFnSc7d1	Mayliový
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zjistí	zjistit	k5eAaPmIp3nS	zjistit
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Oliverova	Oliverův	k2eAgFnSc1d1	Oliverova
teta	teta	k1gFnSc1	teta
Pan	Pan	k1gMnSc1	Pan
Bumble	Bumble	k1gMnSc1	Bumble
-	-	kIx~	-
nafoukaný	nafoukaný	k2eAgInSc1d1	nafoukaný
obecní	obecní	k2eAgInSc1d1	obecní
serbus	serbus	k0	serbus
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vedoucí	vedoucí	k1gMnSc1	vedoucí
špitálu	špitál	k1gInSc2	špitál
<g/>
,	,	kIx,	,
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
chudobinci	chudobinec	k1gInSc6	chudobinec
Pan	Pan	k1gMnSc1	Pan
<g />
.	.	kIx.	.
</s>
<s>
Sowerberry	Sowerberr	k1gInPc1	Sowerberr
-	-	kIx~	-
hodný	hodný	k2eAgMnSc1d1	hodný
majitel	majitel	k1gMnSc1	majitel
pohřebního	pohřební	k2eAgInSc2d1	pohřební
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
nechá	nechat	k5eAaPmIp3nS	nechat
se	se	k3xPyFc4	se
však	však	k9	však
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Paní	paní	k1gFnSc1	paní
Sowerberryová	Sowerberryová	k1gFnSc1	Sowerberryová
-	-	kIx~	-
manželka	manželka	k1gFnSc1	manželka
Sowerberryho	Sowerberry	k1gMnSc2	Sowerberry
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Noa	Noa	k1gFnSc2	Noa
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
Olivera	Olivera	k1gFnSc1	Olivera
vypudit	vypudit	k5eAaPmF	vypudit
Paní	paní	k1gFnSc1	paní
Bedwinová	Bedwinový	k2eAgFnSc1d1	Bedwinový
-	-	kIx~	-
milá	milý	k2eAgFnSc1d1	Milá
hospodyně	hospodyně	k1gFnSc1	hospodyně
pana	pan	k1gMnSc2	pan
Brownlowa	Brownlowus	k1gMnSc2	Brownlowus
Pan	Pan	k1gMnSc1	Pan
Grimwig	Grimwig	k1gMnSc1	Grimwig
-	-	kIx~	-
přítel	přítel	k1gMnSc1	přítel
pana	pan	k1gMnSc2	pan
Brownlowa	Brownlowum	k1gNnSc2	Brownlowum
<g/>
,	,	kIx,	,
nevěří	věřit	k5eNaImIp3nS	věřit
v	v	k7c4	v
Oliverovu	Oliverův	k2eAgFnSc4d1	Oliverova
poctivost	poctivost	k1gFnSc4	poctivost
Karolína	Karolína	k1gFnSc1	Karolína
-	-	kIx~	-
služka	služka	k1gFnSc1	služka
<g/>
,	,	kIx,	,
kuchařka	kuchařka	k1gFnSc1	kuchařka
u	u	k7c2	u
paní	paní	k1gFnSc2	paní
Sowerberryové	Sowerberryová	k1gFnSc2	Sowerberryová
<g/>
,	,	kIx,	,
miluje	milovat	k5eAaImIp3nS	milovat
Noea	Noea	k1gMnSc1	Noea
Claypolea	Claypolea	k1gMnSc1	Claypolea
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
1	[number]	k4	1
<g/>
/	/	kIx~	/
A-	A-	k1gMnPc2	A-
<g/>
L.	L.	kA	L.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
948	[number]	k4	948
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
475	[number]	k4	475
<g/>
.	.	kIx.	.
</s>
