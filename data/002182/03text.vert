<s>
Providence	providence	k1gFnSc1	providence
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
a	a	k8xC	a
nejlidnatější	lidnatý	k2eAgNnSc4d3	nejlidnatější
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejdříve	dříve	k6eAd3	dříve
založených	založený	k2eAgNnPc2d1	založené
měst	město	k1gNnPc2	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
třetí	třetí	k4xOgNnSc1	třetí
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Providence	providence	k1gFnSc2	providence
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1636	[number]	k4	1636
Rogerem	Roger	k1gMnSc7	Roger
Williamsem	Williams	k1gMnSc7	Williams
<g/>
,	,	kIx,	,
anglickým	anglický	k2eAgMnSc7d1	anglický
teologem	teolog	k1gMnSc7	teolog
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
známé	známá	k1gFnPc4	známá
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
šperkovní	šperkovní	k2eAgInSc4d1	šperkovní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c4	v
Providence	providence	k1gFnPc4	providence
sídlí	sídlet	k5eAaImIp3nS	sídlet
osm	osm	k4xCc4	osm
nemocnic	nemocnice	k1gFnPc2	nemocnice
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
institucí	instituce	k1gFnPc2	instituce
vyššího	vysoký	k2eAgNnSc2d2	vyšší
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
téměř	téměř	k6eAd1	téměř
30	[number]	k4	30
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Providence	providence	k1gFnSc1	providence
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
53,2	[number]	k4	53,2
km2	km2	k4	km2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
47,8	[number]	k4	47,8
km2	km2	k4	km2
pevnina	pevnina	k1gFnSc1	pevnina
a	a	k8xC	a
5,3	[number]	k4	5,3
km2	km2	k4	km2
(	(	kIx(	(
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
178	[number]	k4	178
042	[number]	k4	042
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlilo	sídlit	k5eAaImAgNnS	sídlit
173	[number]	k4	173
618	[number]	k4	618
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
62	[number]	k4	62
389	[number]	k4	389
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
35	[number]	k4	35
859	[number]	k4	859
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
3	[number]	k4	3
629,4	[number]	k4	629,4
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
49,8	[number]	k4	49,8
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
16,0	[number]	k4	16,0
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
1,4	[number]	k4	1,4
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
6,4	[number]	k4	6,4
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
19,8	[number]	k4	19,8
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
6,5	[number]	k4	6,5
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
38,1	[number]	k4	38,1
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Aldrich	Aldrich	k1gMnSc1	Aldrich
(	(	kIx(	(
<g/>
1863	[number]	k4	1863
-	-	kIx~	-
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
Howard	Howard	k1gMnSc1	Howard
Phillips	Phillipsa	k1gFnPc2	Phillipsa
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
-	-	kIx~	-
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
amatérský	amatérský	k2eAgMnSc1d1	amatérský
žurnalista	žurnalista	k1gMnSc1	žurnalista
a	a	k8xC	a
autodidakt	autodidakt	k1gMnSc1	autodidakt
Clara	Clara	k1gFnSc1	Clara
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
-	-	kIx~	-
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
psychoanalytička	psychoanalytička	k1gFnSc1	psychoanalytička
<g />
.	.	kIx.	.
</s>
<s>
Aaron	Aaron	k1gMnSc1	Aaron
Siskind	Siskind	k1gMnSc1	Siskind
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
-	-	kIx~	-
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
abstraktní	abstraktní	k2eAgMnSc1d1	abstraktní
expresionistický	expresionistický	k2eAgMnSc1d1	expresionistický
fotograf	fotograf	k1gMnSc1	fotograf
Galway	Galwaa	k1gFnSc2	Galwaa
Kinnell	Kinnell	k1gMnSc1	Kinnell
(	(	kIx(	(
<g/>
*	*	kIx~	*
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
editor	editor	k1gMnSc1	editor
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
Jill	Jill	k1gMnSc1	Jill
Craybasová	Craybasová	k1gFnSc1	Craybasová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
Josh	Josh	k1gMnSc1	Josh
Schwartz	Schwartz	k1gMnSc1	Schwartz
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
televizní	televizní	k2eAgMnSc1d1	televizní
producent	producent	k1gMnSc1	producent
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
Phnompenh	Phnompenha	k1gFnPc2	Phnompenha
<g/>
,	,	kIx,	,
Kambodža	Kambodža	k1gFnSc1	Kambodža
Riga	Riga	k1gFnSc1	Riga
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k1gNnSc1	Domingo
<g/>
,	,	kIx,	,
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Providence	providence	k1gFnSc2	providence
<g/>
,	,	kIx,	,
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Providence	providence	k1gFnSc2	providence
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
