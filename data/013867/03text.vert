<s>
Miroslav	Miroslav	k1gMnSc1
Jůza	Jůza	k1gMnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Jůza	Jůza	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1943	#num#	k4
<g/>
Čebín	Čebína	k1gFnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2012	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
68	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Evropské	evropský	k2eAgFnPc1d1
halové	halový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
EHH	EHH	kA
1968	#num#	k4
</s>
<s>
3	#num#	k4
x	x	k?
1000	#num#	k4
m	m	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
ČSSR	ČSSR	kA
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MČSSR	MČSSR	kA
1962	#num#	k4
</s>
<s>
běh	běh	k1gInSc1
na	na	k7c4
1500	#num#	k4
m	m	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MČSSR	MČSSR	kA
1964	#num#	k4
</s>
<s>
běh	běh	k1gInSc1
na	na	k7c4
1500	#num#	k4
m	m	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MČSSR	MČSSR	kA
1965	#num#	k4
</s>
<s>
běh	běh	k1gInSc1
na	na	k7c4
1500	#num#	k4
m	m	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MČSSR	MČSSR	kA
1966	#num#	k4
</s>
<s>
běh	běh	k1gInSc1
na	na	k7c4
1500	#num#	k4
m	m	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MČSSR	MČSSR	kA
1967	#num#	k4
</s>
<s>
běh	běh	k1gInSc1
na	na	k7c4
5000	#num#	k4
m	m	kA
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Jůza	Jůza	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1943	#num#	k4
<g/>
,	,	kIx,
Čebín	Čebín	k1gInSc1
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
československý	československý	k2eAgMnSc1d1
atlet	atlet	k1gMnSc1
<g/>
,	,	kIx,
běžec	běžec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgInS,k5eAaPmAgInS
středním	střední	k2eAgFnPc3d1
tratím	trať	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
získal	získat	k5eAaPmAgMnS
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
na	na	k7c6
třetím	třetí	k4xOgInSc6
ročníku	ročník	k1gInSc6
evropských	evropský	k2eAgFnPc2d1
halových	halový	k2eAgFnPc2d1
her	hra	k1gFnPc2
v	v	k7c6
Madridu	Madrid	k1gInSc6
ve	v	k7c6
štafetovém	štafetový	k2eAgInSc6d1
závodě	závod	k1gInSc6
na	na	k7c4
3	#num#	k4
x	x	k?
1000	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třikrát	třikrát	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
vítězem	vítěz	k1gMnSc7
mezinárodního	mezinárodní	k2eAgInSc2d1
závodu	závod	k1gInSc2
Běh	běh	k1gInSc1
Lužánkami	Lužánka	k1gFnPc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
v	v	k7c6
Brně	Brno	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
atletické	atletický	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
(	(	kIx(
<g/>
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
ukončil	ukončit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
oddílu	oddíl	k1gInSc2
Zbrojovka	zbrojovka	k1gFnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
trenérem	trenér	k1gMnSc7
byl	být	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
Šrámek	Šrámek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Profil	profil	k1gInSc1
na	na	k7c4
atletickytrenink	atletickytrenink	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.atletickytrenink.cz	www.atletickytrenink.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČESTNÁ	čestný	k2eAgFnSc1d1
LISTINA	listina	k1gFnSc1
VÍTĚZŮ	vítěz	k1gMnPc2
HLAVNÍHO	hlavní	k2eAgInSc2d1
ZÁVODU	závod	k1gInSc2
BĚHU	běh	k1gInSc2
LUŽÁNKAMI	LUŽÁNKAMI	kA
<g/>
.	.	kIx.
www.behluzankami.cz	www.behluzankami.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
