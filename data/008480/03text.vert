<p>
<s>
Náchod	Náchod	k1gInSc1	Náchod
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Nachod	Nachod	k1gInSc1	Nachod
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
turistickém	turistický	k2eAgInSc6d1	turistický
regionu	region	k1gInSc6	region
Kladské	kladský	k2eAgNnSc4d1	Kladské
pomezí	pomezí	k1gNnSc4	pomezí
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rytířem	rytíř	k1gMnSc7	rytíř
Hronem	Hron	k1gMnSc7	Hron
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Načeradiců	Načeradiec	k1gInPc2	Načeradiec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
založil	založit	k5eAaPmAgMnS	založit
hrad	hrad	k1gInSc4	hrad
na	na	k7c6	na
strategickém	strategický	k2eAgNnSc6d1	strategické
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zemská	zemský	k2eAgFnSc1d1	zemská
stezka	stezka	k1gFnSc1	stezka
zužuje	zužovat	k5eAaImIp3nS	zužovat
do	do	k7c2	do
průsmyku	průsmyk	k1gInSc2	průsmyk
zvaného	zvaný	k2eAgInSc2d1	zvaný
Branka	branka	k1gFnSc1	branka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1254	[number]	k4	1254
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
Náchod	Náchod	k1gInSc4	Náchod
chodilo	chodit	k5eAaImAgNnS	chodit
do	do	k7c2	do
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hraniční	hraniční	k2eAgInSc1d1	hraniční
přechod	přechod	k1gInSc1	přechod
Náchod-Běloves	Náchod-Běloves	k1gInSc1	Náchod-Běloves
<g/>
/	/	kIx~	/
<g/>
Kudowa	Kudowa	k1gFnSc1	Kudowa
Słone	Słon	k1gInSc5	Słon
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
trase	trasa	k1gFnSc6	trasa
Praha	Praha	k1gFnSc1	Praha
<g/>
–	–	k?	–
<g/>
Varšava	Varšava	k1gFnSc1	Varšava
(	(	kIx(	(
<g/>
silnice	silnice	k1gFnSc1	silnice
E	E	kA	E
<g/>
67	[number]	k4	67
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náchodem	Náchod	k1gInSc7	Náchod
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Metuje	Metuje	k1gFnSc1	Metuje
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
Lázní	lázeň	k1gFnPc2	lázeň
Běloves	Běloves	k1gInSc4	Běloves
je	být	k5eAaImIp3nS	být
Náchod	Náchod	k1gInSc4	Náchod
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
jediným	jediný	k2eAgNnSc7d1	jediné
českým	český	k2eAgNnSc7d1	české
lázeňským	lázeňský	k2eAgNnSc7d1	lázeňské
městem	město	k1gNnSc7	město
bez	bez	k7c2	bez
fungujících	fungující	k2eAgFnPc2d1	fungující
lázní	lázeň	k1gFnPc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
významným	významný	k2eAgNnSc7d1	významné
textilním	textilní	k2eAgNnSc7d1	textilní
centrem	centrum	k1gNnSc7	centrum
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Manchester	Manchester	k1gInSc1	Manchester
východu	východ	k1gInSc2	východ
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
založil	založit	k5eAaPmAgMnS	založit
Hron	Hron	k1gMnSc1	Hron
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Načeradiců	Načeradiec	k1gInPc2	Načeradiec
hrad	hrad	k1gInSc4	hrad
sloužící	sloužící	k1gFnSc2	sloužící
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
procházela	procházet	k5eAaImAgFnS	procházet
stará	starý	k2eAgFnSc1d1	stará
obchodní	obchodní	k2eAgFnSc1d1	obchodní
zemská	zemský	k2eAgFnSc1d1	zemská
cesta	cesta	k1gFnSc1	cesta
spojující	spojující	k2eAgFnSc4d1	spojující
Prahu	Praha	k1gFnSc4	Praha
s	s	k7c7	s
Kladskem	Kladsko	k1gNnSc7	Kladsko
<g/>
/	/	kIx~	/
<g/>
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgInS	nacházet
na	na	k7c6	na
strategicky	strategicky	k6eAd1	strategicky
důležitém	důležitý	k2eAgNnSc6d1	důležité
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zemská	zemský	k2eAgFnSc1d1	zemská
stezka	stezka	k1gFnSc1	stezka
zužovala	zužovat	k5eAaImAgFnS	zužovat
do	do	k7c2	do
průsmyku	průsmyk	k1gInSc2	průsmyk
nazvaného	nazvaný	k2eAgInSc2d1	nazvaný
Branka	branka	k1gFnSc1	branka
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Náchod	Náchod	k1gInSc1	Náchod
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
zdokumentováno	zdokumentovat	k5eAaPmNgNnS	zdokumentovat
v	v	k7c6	v
písemných	písemný	k2eAgInPc6d1	písemný
pramenech	pramen	k1gInPc6	pramen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1254	[number]	k4	1254
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k9	jako
označení	označení	k1gNnSc1	označení
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
chodí	chodit	k5eAaImIp3nS	chodit
nebo	nebo	k8xC	nebo
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
.	.	kIx.	.
<g/>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
trhová	trhový	k2eAgFnSc1d1	trhová
ves	ves	k1gFnSc1	ves
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
význam	význam	k1gInSc1	význam
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
Náchod	Náchod	k1gInSc1	Náchod
chráněno	chránit	k5eAaImNgNnS	chránit
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
navazovaly	navazovat	k5eAaImAgFnP	navazovat
dvě	dva	k4xCgFnPc1	dva
další	další	k2eAgFnPc1d1	další
linie	linie	k1gFnPc1	linie
hradeb	hradba	k1gFnPc2	hradba
táhnoucí	táhnoucí	k2eAgFnSc2d1	táhnoucí
se	se	k3xPyFc4	se
nahoru	nahoru	k6eAd1	nahoru
k	k	k7c3	k
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
začala	začít	k5eAaPmAgFnS	začít
též	též	k9	též
vznikat	vznikat	k5eAaImF	vznikat
i	i	k9	i
dvě	dva	k4xCgNnPc1	dva
nejvýznamnější	významný	k2eAgNnPc1d3	nejvýznamnější
předměstí	předměstí	k1gNnPc1	předměstí
–	–	k?	–
Horské	Horské	k2eAgNnSc4d1	Horské
a	a	k8xC	a
Krajské	krajský	k2eAgNnSc4d1	krajské
před	před	k7c7	před
oběma	dva	k4xCgFnPc7	dva
branami	brána	k1gFnPc7	brána
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
se	se	k3xPyFc4	se
majitelé	majitel	k1gMnPc1	majitel
hradu	hrad	k1gInSc2	hrad
střídali	střídat	k5eAaImAgMnP	střídat
–	–	k?	–
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1437	[number]	k4	1437
sirotčí	sirotčí	k2eAgMnPc1d1	sirotčí
hejtman	hejtman	k1gMnSc1	hejtman
Jan	Jan	k1gMnSc1	Jan
Kolda	Kolda	k1gMnSc1	Kolda
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Žampachu	Žampach	k1gInSc2	Žampach
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
změnám	změna	k1gFnPc3	změna
docházelo	docházet	k5eAaImAgNnS	docházet
i	i	k9	i
ve	v	k7c6	v
vzhledu	vzhled	k1gInSc6	vzhled
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
opevnění	opevnění	k1gNnSc2	opevnění
v	v	k7c6	v
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
místě	místo	k1gNnSc6	místo
ostrohu	ostroh	k1gInSc2	ostroh
se	se	k3xPyFc4	se
zvětšil	zvětšit	k5eAaPmAgInS	zvětšit
do	do	k7c2	do
velkého	velký	k2eAgInSc2d1	velký
fortifikačního	fortifikační	k2eAgInSc2d1	fortifikační
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1544	[number]	k4	1544
Náchod	Náchod	k1gInSc1	Náchod
získal	získat	k5eAaPmAgInS	získat
mocný	mocný	k2eAgInSc4d1	mocný
a	a	k8xC	a
bohatý	bohatý	k2eAgInSc4d1	bohatý
rod	rod	k1gInSc4	rod
Smiřických	smiřický	k2eAgInPc2d1	smiřický
ze	z	k7c2	z
Smiřic	Smiřice	k1gFnPc2	Smiřice
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc3	jehož
nárokům	nárok	k1gInPc3	nárok
středověké	středověký	k2eAgFnSc2d1	středověká
sídlo	sídlo	k1gNnSc1	sídlo
nevyhovovalo	vyhovovat	k5eNaImAgNnS	vyhovovat
–	–	k?	–
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1566	[number]	k4	1566
a	a	k8xC	a
1614	[number]	k4	1614
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
pohodlný	pohodlný	k2eAgInSc4d1	pohodlný
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
nastala	nastat	k5eAaPmAgFnS	nastat
doba	doba	k1gFnSc1	doba
největšího	veliký	k2eAgInSc2d3	veliký
kulturního	kulturní	k2eAgInSc2d1	kulturní
i	i	k8xC	i
hmotného	hmotný	k2eAgInSc2d1	hmotný
rozmachu	rozmach	k1gInSc2	rozmach
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Náchod	Náchod	k1gInSc1	Náchod
byl	být	k5eAaImAgInS	být
nadán	nadat	k5eAaPmNgInS	nadat
výsadami	výsada	k1gFnPc7	výsada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
rovnaly	rovnat	k5eAaImAgInP	rovnat
postavení	postavení	k1gNnSc4	postavení
měst	město	k1gNnPc2	město
královských	královský	k2eAgInPc6d1	královský
<g/>
.	.	kIx.	.
1601	[number]	k4	1601
koupilo	koupit	k5eAaPmAgNnS	koupit
město	město	k1gNnSc1	město
Náchod	Náchod	k1gInSc1	Náchod
rytířský	rytířský	k2eAgInSc4d1	rytířský
statek	statek	k1gInSc4	statek
v	v	k7c6	v
Slaným	Slaný	k1gInPc3	Slaný
(	(	kIx(	(
<g/>
Slaney	Slanea	k1gFnSc2	Slanea
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Schlaney	Schlane	k2eAgInPc1d1	Schlane
<g/>
,	,	kIx,	,
od	od	k7c2	od
1945	[number]	k4	1945
polsky	polsky	k6eAd1	polsky
Słone	Słon	k1gInSc5	Słon
<g/>
)	)	kIx)	)
v	v	k7c6	v
kladském	kladský	k2eAgNnSc6d1	Kladské
hrabství	hrabství	k1gNnSc6	hrabství
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
1945	[number]	k4	1945
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
ovšem	ovšem	k9	ovšem
přerušila	přerušit	k5eAaPmAgFnS	přerušit
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Statky	statek	k1gInPc1	statek
Smiřických	smiřický	k2eAgFnPc2d1	Smiřická
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
oddanosti	oddanost	k1gFnSc2	oddanost
rodu	rod	k1gInSc2	rod
králi	král	k1gMnSc3	král
Fridrichu	Fridrich	k1gMnSc3	Fridrich
Falckému	falcký	k2eAgMnSc3d1	falcký
propadly	propadlo	k1gNnPc7	propadlo
císařskému	císařský	k2eAgInSc3d1	císařský
fisku	fiskus	k1gInSc3	fiskus
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1623	[number]	k4	1623
prodány	prodat	k5eAaPmNgFnP	prodat
Trčkům	Trčka	k1gMnPc3	Trčka
z	z	k7c2	z
Lípy	lípa	k1gFnSc2	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavraždění	zavraždění	k1gNnSc6	zavraždění
Adama	Adam	k1gMnSc2	Adam
Erdmana	Erdman	k1gMnSc2	Erdman
Trčky	Trčka	k1gMnSc2	Trčka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
švagrem	švagr	k1gMnSc7	švagr
Albrechtem	Albrecht	k1gMnSc7	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
roku	rok	k1gInSc2	rok
1634	[number]	k4	1634
byly	být	k5eAaImAgInP	být
majetky	majetek	k1gInPc1	majetek
opět	opět	k6eAd1	opět
zabaveny	zabaven	k2eAgInPc1d1	zabaven
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
darem	dar	k1gInSc7	dar
generál	generál	k1gMnSc1	generál
Ottavio	Ottavio	k1gMnSc1	Ottavio
Piccolomini	Piccolomin	k2eAgMnPc1d1	Piccolomin
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Amalfi	Amalf	k1gFnSc2	Amalf
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
Náchod	Náchod	k1gInSc1	Náchod
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
italského	italský	k2eAgInSc2d1	italský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jehož	k3xOyRp3gFnSc2	jehož
vlády	vláda	k1gFnSc2	vláda
trpěl	trpět	k5eAaImAgMnS	trpět
válečnými	válečný	k2eAgFnPc7d1	válečná
událostmi	událost	k1gFnPc7	událost
a	a	k8xC	a
násilnou	násilný	k2eAgFnSc7d1	násilná
rekatolizací	rekatolizace	k1gFnSc7	rekatolizace
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
ale	ale	k9	ale
i	i	k9	i
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
barokní	barokní	k2eAgFnSc3d1	barokní
přestavbě	přestavba	k1gFnSc3	přestavba
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
letech	let	k1gInPc6	let
1650	[number]	k4	1650
<g/>
–	–	k?	–
<g/>
1659	[number]	k4	1659
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
byly	být	k5eAaImAgFnP	být
také	také	k9	také
prováděny	prováděn	k2eAgFnPc1d1	prováděna
stavební	stavební	k2eAgFnPc1d1	stavební
práce	práce	k1gFnPc1	práce
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1638	[number]	k4	1638
byla	být	k5eAaImAgFnS	být
vydlážděna	vydláždit	k5eAaPmNgFnS	vydláždit
první	první	k4xOgFnSc1	první
ulice	ulice	k1gFnSc1	ulice
–	–	k?	–
dodnes	dodnes	k6eAd1	dodnes
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Kamenice	Kamenice	k1gFnSc1	Kamenice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
města	město	k1gNnSc2	město
roku	rok	k1gInSc2	rok
1663	[number]	k4	1663
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
barokní	barokní	k2eAgFnSc1d1	barokní
stará	starý	k2eAgFnSc1d1	stará
radnice	radnice	k1gFnSc1	radnice
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
též	též	k9	též
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Měšťané	měšťan	k1gMnPc1	měšťan
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
četná	četný	k2eAgNnPc4d1	četné
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
vymření	vymření	k1gNnSc6	vymření
rodu	rod	k1gInSc2	rod
Piccolomini	Piccolomin	k2eAgMnPc1d1	Piccolomin
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Náchod	Náchod	k1gInSc1	Náchod
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
Desfoursové	Desfoursové	k2eAgMnPc2d1	Desfoursové
<g/>
,	,	kIx,	,
od	od	k7c2	od
nichž	jenž	k3xRgMnPc2	jenž
jej	on	k3xPp3gNnSc4	on
koupil	koupit	k5eAaPmAgMnS	koupit
známý	známý	k2eAgMnSc1d1	známý
milovník	milovník	k1gMnSc1	milovník
umění	umění	k1gNnSc2	umění
vévoda	vévoda	k1gMnSc1	vévoda
Petr	Petr	k1gMnSc1	Petr
Kuronský	Kuronský	k2eAgMnSc1d1	Kuronský
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Petra	Petr	k1gMnSc2	Petr
Kuronského	Kuronský	k2eAgMnSc2d1	Kuronský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
patřil	patřit	k5eAaImAgInS	patřit
Náchod	Náchod	k1gInSc1	Náchod
jeho	jeho	k3xOp3gFnSc3	jeho
dceři	dcera	k1gFnSc3	dcera
vévodkyni	vévodkyně	k1gFnSc3	vévodkyně
Kateřině	Kateřina	k1gFnSc3	Kateřina
Vilemíně	Vilemína	k1gFnSc3	Vilemína
Zaháňské	zaháňský	k2eAgFnSc3d1	Zaháňská
a	a	k8xC	a
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
německému	německý	k2eAgNnSc3d1	německé
rodu	rod	k1gInSc2	rod
Schaumburg-Lippe	Schaumburg-Lipp	k1gInSc5	Schaumburg-Lipp
<g/>
.	.	kIx.	.
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přinesla	přinést	k5eAaPmAgFnS	přinést
změny	změna	k1gFnPc4	změna
do	do	k7c2	do
života	život	k1gInSc2	život
malého	malý	k2eAgNnSc2d1	malé
poddanského	poddanský	k2eAgNnSc2d1	poddanské
městečka	městečko	k1gNnSc2	městečko
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
městská	městský	k2eAgFnSc1d1	městská
samospráva	samospráva	k1gFnSc1	samospráva
<g/>
,	,	kIx,	,
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
se	se	k3xPyFc4	se
školství	školství	k1gNnSc1	školství
<g/>
,	,	kIx,	,
prudce	prudko	k6eAd1	prudko
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgInSc1d1	stavební
rozmach	rozmach	k1gInSc1	rozmach
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
určil	určit	k5eAaPmAgInS	určit
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
středu	středa	k1gFnSc4	středa
města	město	k1gNnSc2	město
–	–	k?	–
např.	např.	kA	např.
pseudorenesanční	pseudorenesanční	k2eAgFnSc1d1	pseudorenesanční
nová	nový	k2eAgFnSc1d1	nová
radnice	radnice	k1gFnSc1	radnice
a	a	k8xC	a
secesní	secesní	k2eAgNnSc1d1	secesní
městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
národního	národní	k2eAgNnSc2d1	národní
uvědomění	uvědomění	k1gNnSc2	uvědomění
utvářela	utvářet	k5eAaImAgFnS	utvářet
již	již	k6eAd1	již
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
z	z	k7c2	z
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
hlediska	hledisko	k1gNnSc2	hledisko
však	však	k9	však
Náchod	Náchod	k1gInSc4	Náchod
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
dlouho	dlouho	k6eAd1	dlouho
za	za	k7c7	za
sousedními	sousední	k2eAgNnPc7d1	sousední
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
<g/>
Vedle	vedle	k7c2	vedle
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
soustředěných	soustředěný	k2eAgFnPc2d1	soustředěná
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
cechovních	cechovní	k2eAgFnPc6d1	cechovní
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
společenstevních	společenstevní	k2eAgFnPc6d1	společenstevní
organizacích	organizace	k1gFnPc6	organizace
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
i	i	k8xC	i
celém	celý	k2eAgNnSc6d1	celé
okolí	okolí	k1gNnSc6	okolí
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
činitelem	činitel	k1gInSc7	činitel
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
organizována	organizovat	k5eAaBmNgFnS	organizovat
faktorskými	faktorský	k2eAgFnPc7d1	faktorská
firmami	firma	k1gFnPc7	firma
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
však	však	k9	však
nemohly	moct	k5eNaImAgFnP	moct
zajistit	zajistit	k5eAaPmF	zajistit
dostatečně	dostatečně	k6eAd1	dostatečně
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgInP	postavit
první	první	k4xOgFnSc6	první
dvě	dva	k4xCgFnPc1	dva
malé	malý	k2eAgFnPc1d1	malá
mechanické	mechanický	k2eAgFnPc1d1	mechanická
tkalcovny	tkalcovna	k1gFnPc1	tkalcovna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
odstartovaly	odstartovat	k5eAaPmAgFnP	odstartovat
rychlý	rychlý	k2eAgInSc4d1	rychlý
růst	růst	k1gInSc4	růst
textilního	textilní	k2eAgInSc2d1	textilní
průmyslu	průmysl	k1gInSc2	průmysl
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Náchod	Náchod	k1gInSc1	Náchod
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
bavlnářských	bavlnářský	k2eAgNnPc2d1	bavlnářský
center	centrum	k1gNnPc2	centrum
Rakousko-Uherska	Rakousko-Uherska	k1gFnSc1	Rakousko-Uherska
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
zváno	zván	k2eAgNnSc1d1	zváno
"	"	kIx"	"
<g/>
Manchester	Manchester	k1gInSc1	Manchester
východu	východ	k1gInSc2	východ
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
hrál	hrát	k5eAaImAgMnS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
životě	život	k1gInSc6	život
města	město	k1gNnSc2	město
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
proletariát	proletariát	k1gInSc4	proletariát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
dělnické	dělnický	k2eAgInPc1d1	dělnický
spolky	spolek	k1gInPc1	spolek
doplňovaly	doplňovat	k5eAaImAgInP	doplňovat
četné	četný	k2eAgInPc1d1	četný
spolky	spolek	k1gInPc1	spolek
občanské	občanský	k2eAgFnSc2d1	občanská
<g/>
.	.	kIx.	.
<g/>
V	v	k7c4	v
pondělí	pondělí	k1gNnSc4	pondělí
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1926	[number]	k4	1926
navštívil	navštívit	k5eAaPmAgMnS	navštívit
město	město	k1gNnSc4	město
oficiálně	oficiálně	k6eAd1	oficiálně
prezident	prezident	k1gMnSc1	prezident
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
T.	T.	kA	T.
G.	G.	kA	G.
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jehož	jenž	k3xRgNnSc4	jenž
přivítání	přivítání	k1gNnSc4	přivítání
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Kašparáku	Kašparák	k1gInSc6	Kašparák
<g/>
"	"	kIx"	"
postavena	postaven	k2eAgFnSc1d1	postavena
velká	velký	k2eAgFnSc1d1	velká
pseudogotická	pseudogotický	k2eAgFnSc1d1	pseudogotická
slavobrána	slavobrána	k1gFnSc1	slavobrána
<g/>
,	,	kIx,	,
zachycená	zachycený	k2eAgFnSc1d1	zachycená
i	i	k9	i
na	na	k7c6	na
filmových	filmový	k2eAgInPc6d1	filmový
záběrech	záběr	k1gInPc6	záběr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Náchod	Náchod	k1gInSc1	Náchod
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
10	[number]	k4	10
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
téměř	téměř	k6eAd1	téměř
shodných	shodný	k2eAgFnPc2d1	shodná
s	s	k7c7	s
10	[number]	k4	10
katastrálními	katastrální	k2eAgNnPc7d1	katastrální
územími	území	k1gNnPc7	území
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Babí	babí	k2eAgFnSc1d1	babí
–	–	k?	–
k.	k.	k?	k.
ú.	ú.	k?	ú.
Babí	babit	k5eAaImIp3nS	babit
u	u	k7c2	u
Náchoda	Náchod	k1gInSc2	Náchod
a	a	k8xC	a
Náchod	Náchod	k1gInSc1	Náchod
</s>
</p>
<p>
<s>
Běloves	Běloves	k1gInSc1	Běloves
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
také	také	k9	také
Bílá	bílý	k2eAgFnSc1d1	bílá
Ves	ves	k1gFnSc1	ves
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bražec	Bražec	k1gMnSc1	Bražec
</s>
</p>
<p>
<s>
Dobrošov	Dobrošov	k1gInSc1	Dobrošov
</s>
</p>
<p>
<s>
Jizbice	jizbice	k1gFnSc1	jizbice
–	–	k?	–
k.	k.	k?	k.
ú.	ú.	k?	ú.
Jizbice	jizbice	k1gFnSc2	jizbice
u	u	k7c2	u
Náchoda	Náchod	k1gInSc2	Náchod
</s>
</p>
<p>
<s>
Lipí	Lipí	k1gFnSc1	Lipí
–	–	k?	–
k.	k.	k?	k.
ú.	ú.	k?	ú.
Lipí	Lipí	k1gFnSc2	Lipí
u	u	k7c2	u
Náchoda	Náchod	k1gInSc2	Náchod
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
Poříčí	Poříčí	k1gNnSc4	Poříčí
</s>
</p>
<p>
<s>
Náchod	Náchod	k1gInSc1	Náchod
</s>
</p>
<p>
<s>
Pavlišov	Pavlišov	k1gInSc1	Pavlišov
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
nad	nad	k7c4	nad
MetujíDříve	MetujíDříev	k1gFnPc4	MetujíDříev
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
také	také	k9	také
dnes	dnes	k6eAd1	dnes
samostatné	samostatný	k2eAgFnPc1d1	samostatná
obce	obec	k1gFnPc1	obec
Kramolna	Kramolna	k1gFnSc1	Kramolna
a	a	k8xC	a
Vysokov	Vysokov	k1gInSc1	Vysokov
<g/>
.	.	kIx.	.
<g/>
Dále	daleko	k6eAd2	daleko
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
Metuje	Metuje	k1gFnSc2	Metuje
se	se	k3xPyFc4	se
na	na	k7c4	na
Staré	Staré	k2eAgNnSc4d1	Staré
Město	město	k1gNnSc4	město
napojuje	napojovat	k5eAaImIp3nS	napojovat
vesnička	vesnička	k1gFnSc1	vesnička
Bražec	Bražec	k1gInSc1	Bražec
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
u	u	k7c2	u
čistírny	čistírna	k1gFnSc2	čistírna
odpadních	odpadní	k2eAgFnPc2d1	odpadní
vod	voda	k1gFnPc2	voda
začíná	začínat	k5eAaImIp3nS	začínat
hluboké	hluboký	k2eAgNnSc4d1	hluboké
údolí	údolí	k1gNnSc4	údolí
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
přírodní	přírodní	k2eAgFnSc7d1	přírodní
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Čarokrásným	čarokrásný	k2eAgNnSc7d1	čarokrásné
údolím	údolí	k1gNnSc7	údolí
dojdeme	dojít	k5eAaPmIp1nP	dojít
po	po	k7c6	po
novém	nový	k2eAgInSc6d1	nový
povrchu	povrch	k1gInSc6	povrch
cyklostezky	cyklostezka	k1gFnSc2	cyklostezka
po	po	k7c6	po
pár	pár	k4xCyI	pár
kilometrech	kilometr	k1gInPc6	kilometr
do	do	k7c2	do
osady	osada	k1gFnSc2	osada
Ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
občerstvit	občerstvit	k5eAaPmF	občerstvit
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokračovat	pokračovat	k5eAaImF	pokračovat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
nové	nový	k2eAgFnPc4d1	nová
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
lávky	lávka	k1gFnPc4	lávka
do	do	k7c2	do
Pekla	peklo	k1gNnSc2	peklo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pekle	peklo	k1gNnSc6	peklo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zastávka	zastávka	k1gFnSc1	zastávka
sezónního	sezónní	k2eAgInSc2d1	sezónní
autobusu	autobus	k1gInSc2	autobus
směr	směr	k1gInSc4	směr
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
a	a	k8xC	a
rozcestník	rozcestník	k1gInSc1	rozcestník
poukazující	poukazující	k2eAgInSc1d1	poukazující
například	například	k6eAd1	například
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
na	na	k7c6	na
Lipí	Lipí	k1gFnSc6	Lipí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Pekla	peklo	k1gNnSc2	peklo
lze	lze	k6eAd1	lze
pokračovat	pokračovat	k5eAaImF	pokračovat
podél	podél	k7c2	podél
levostranného	levostranný	k2eAgInSc2d1	levostranný
přítoku	přítok	k1gInSc2	přítok
řeky	řeka	k1gFnSc2	řeka
Metuje	Metuje	k1gFnSc2	Metuje
<g/>
,	,	kIx,	,
říčky	říčka	k1gFnSc2	říčka
jménem	jméno	k1gNnSc7	jméno
Olešenka	Olešenka	k1gFnSc1	Olešenka
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
hlubší	hluboký	k2eAgFnSc1d2	hlubší
a	a	k8xC	a
uzavřenější	uzavřený	k2eAgFnSc1d2	uzavřenější
údolí	údolí	k1gNnSc2	údolí
nás	my	k3xPp1nPc4	my
dovede	dovést	k5eAaPmIp3nS	dovést
do	do	k7c2	do
Dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
části	část	k1gFnSc6	část
obce	obec	k1gFnSc2	obec
Nový	nový	k2eAgInSc1d1	nový
Hrádek	hrádek	k1gInSc1	hrádek
<g/>
.	.	kIx.	.
</s>
<s>
Odbočkami	odbočka	k1gFnPc7	odbočka
z	z	k7c2	z
údolí	údolí	k1gNnSc2	údolí
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dostat	dostat	k5eAaPmF	dostat
i	i	k9	i
na	na	k7c4	na
Českou	český	k2eAgFnSc4d1	Česká
Čermnou	Čermný	k2eAgFnSc4d1	Čermná
<g/>
,	,	kIx,	,
Borovou	borový	k2eAgFnSc4d1	Borová
či	či	k8xC	či
Krahulčí	krahulčí	k2eAgFnSc4d1	krahulčí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgNnPc1d1	správní
území	území	k1gNnPc1	území
==	==	k?	==
</s>
</p>
<p>
<s>
Náchod	Náchod	k1gInSc1	Náchod
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
pověřeným	pověřený	k2eAgInSc7d1	pověřený
obecním	obecní	k2eAgInSc7d1	obecní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Okres	okres	k1gInSc1	okres
Náchod	Náchod	k1gInSc1	Náchod
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
78	[number]	k4	78
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
ORP	ORP	kA	ORP
z	z	k7c2	z
36	[number]	k4	36
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
779	[number]	k4	779
domech	dům	k1gInPc6	dům
10	[number]	k4	10
825	[number]	k4	825
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
5	[number]	k4	5
863	[number]	k4	863
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
10	[number]	k4	10
510	[number]	k4	510
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
151	[number]	k4	151
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
94	[number]	k4	94
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
5	[number]	k4	5
157	[number]	k4	157
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
281	[number]	k4	281
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
3	[number]	k4	3
249	[number]	k4	249
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
362	[number]	k4	362
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
1	[number]	k4	1
071	[number]	k4	071
domech	dům	k1gInPc6	dům
11	[number]	k4	11
811	[number]	k4	811
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
11	[number]	k4	11
381	[number]	k4	381
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
248	[number]	k4	248
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
5	[number]	k4	5
166	[number]	k4	166
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
323	[number]	k4	323
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
4	[number]	k4	4
168	[number]	k4	168
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
283	[number]	k4	283
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Náchod	Náchod	k1gInSc4	Náchod
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
Učitelský	učitelský	k2eAgInSc4d1	učitelský
spolek	spolek	k1gInSc4	spolek
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
reálce	reálka	k1gFnSc6	reálka
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
první	první	k4xOgFnSc4	první
výstavu	výstava	k1gFnSc4	výstava
soch	socha	k1gFnPc2	socha
a	a	k8xC	a
obrazů	obraz	k1gInPc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
činnost	činnost	k1gFnSc4	činnost
navázal	navázat	k5eAaPmAgInS	navázat
pořádáním	pořádání	k1gNnSc7	pořádání
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
výstav	výstava	k1gFnPc2	výstava
studentský	studentský	k2eAgInSc1d1	studentský
spolek	spolek	k1gInSc1	spolek
BOR	bor	k1gInSc1	bor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
jednala	jednat	k5eAaImAgFnS	jednat
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
o	o	k7c4	o
zřízení	zřízení	k1gNnSc4	zřízení
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
městské	městský	k2eAgFnSc2d1	městská
dvorany	dvorana	k1gFnSc2	dvorana
za	za	k7c7	za
hotelem	hotel	k1gInSc7	hotel
Beránek	Beránek	k1gMnSc1	Beránek
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
město	město	k1gNnSc1	město
zakoupilo	zakoupit	k5eAaPmAgNnS	zakoupit
i	i	k9	i
sousední	sousední	k2eAgInSc4d1	sousední
hotel	hotel	k1gInSc4	hotel
Letzel	Letzel	k1gFnSc2	Letzel
a	a	k8xC	a
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnPc4d1	nová
budovy	budova	k1gFnPc4	budova
Městského	městský	k2eAgNnSc2d1	Městské
divadla	divadlo	k1gNnSc2	divadlo
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
významného	významný	k2eAgMnSc2d1	významný
architekta	architekt	k1gMnSc2	architekt
Aloise	Alois	k1gMnSc4	Alois
Čenského	Čenský	k1gMnSc4	Čenský
(	(	kIx(	(
<g/>
projektoval	projektovat	k5eAaBmAgMnS	projektovat
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
i	i	k9	i
výstavní	výstavní	k2eAgFnSc1d1	výstavní
galerie	galerie	k1gFnSc1	galerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
zřídila	zřídit	k5eAaPmAgFnS	zřídit
náchodská	náchodský	k2eAgFnSc1d1	Náchodská
Muzejní	muzejní	k2eAgFnSc1d1	muzejní
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
školy	škola	k1gFnSc2	škola
na	na	k7c6	na
Karlově	Karlův	k2eAgNnSc6d1	Karlovo
náměstí	náměstí	k1gNnSc6	náměstí
Městskou	městský	k2eAgFnSc4d1	městská
obrazárnu	obrazárna	k1gFnSc4	obrazárna
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgMnPc4	který
byly	být	k5eAaImAgInP	být
soustředěny	soustředěn	k2eAgInPc1d1	soustředěn
obrazy	obraz	k1gInPc1	obraz
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
města	město	k1gNnSc2	město
Náchod	Náchod	k1gInSc1	Náchod
<g/>
,	,	kIx,	,
Městské	městský	k2eAgFnSc2d1	městská
spořitelny	spořitelna	k1gFnSc2	spořitelna
a	a	k8xC	a
zapůjčená	zapůjčený	k2eAgNnPc4d1	zapůjčené
díla	dílo	k1gNnPc4	dílo
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
velkoobchodníka	velkoobchodník	k1gMnSc2	velkoobchodník
Lea	Leo	k1gMnSc2	Leo
Strasse	Strass	k1gMnSc2	Strass
a	a	k8xC	a
nakupovala	nakupovat	k5eAaBmAgFnS	nakupovat
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
pravidelně	pravidelně	k6eAd1	pravidelně
obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
výstav	výstava	k1gFnPc2	výstava
pořádaných	pořádaný	k2eAgFnPc2d1	pořádaná
spolkem	spolek	k1gInSc7	spolek
BOR	bor	k1gInSc1	bor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
obrazárna	obrazárna	k1gFnSc1	obrazárna
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
sbírky	sbírka	k1gFnPc1	sbírka
rozptýleny	rozptýlit	k5eAaPmNgFnP	rozptýlit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
obrazárna	obrazárna	k1gFnSc1	obrazárna
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Městského	městský	k2eAgNnSc2d1	Městské
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
část	část	k1gFnSc4	část
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
na	na	k7c6	na
náchodském	náchodský	k2eAgInSc6d1	náchodský
zámku	zámek	k1gInSc6	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
byly	být	k5eAaImAgFnP	být
sbírky	sbírka	k1gFnPc1	sbírka
součástí	součást	k1gFnPc2	součást
Okresního	okresní	k2eAgNnSc2d1	okresní
vlastivědného	vlastivědný	k2eAgNnSc2d1	Vlastivědné
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc2	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
<g/>
.	.	kIx.	.
</s>
<s>
Sbírky	sbírka	k1gFnPc1	sbírka
okresní	okresní	k2eAgFnSc2d1	okresní
galerie	galerie	k1gFnSc2	galerie
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
převedeny	převést	k5eAaPmNgFnP	převést
do	do	k7c2	do
nově	nově	k6eAd1	nově
založené	založený	k2eAgFnSc2d1	založená
Galerie	galerie	k1gFnSc2	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
zde	zde	k6eAd1	zde
natáčel	natáčet	k5eAaImAgInS	natáčet
štáb	štáb	k1gInSc1	štáb
Dušana	Dušan	k1gMnSc2	Dušan
Kleina	Klein	k1gMnSc2	Klein
kriminální	kriminální	k2eAgInSc1d1	kriminální
film	film	k1gInSc1	film
Případ	případ	k1gInSc1	případ
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
spolužáků	spolužák	k1gMnPc2	spolužák
a	a	k8xC	a
Náchod	Náchod	k1gInSc1	Náchod
byl	být	k5eAaImAgInS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
dle	dle	k7c2	dle
scénáře	scénář	k1gInSc2	scénář
jako	jako	k8xS	jako
Kostelec	Kostelec	k1gInSc4	Kostelec
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
filmu	film	k1gInSc2	film
nevyhnul	vyhnout	k5eNaPmAgInS	vyhnout
ještě	ještě	k9	ještě
po	po	k7c4	po
dvakráte	dvakráte	k6eAd1	dvakráte
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Kostelec	Kostelec	k1gInSc1	Kostelec
<g/>
"	"	kIx"	"
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Náchod	Náchod	k1gInSc4	Náchod
i	i	k9	i
ve	v	k7c4	v
Škvoreckého	Škvorecký	k2eAgMnSc4d1	Škvorecký
próze	próza	k1gFnSc3	próza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
pak	pak	k6eAd1	pak
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
další	další	k2eAgInSc1d1	další
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
Pamětnice	pamětnice	k1gFnSc2	pamětnice
<g/>
.	.	kIx.	.
</s>
<s>
Hereckou	herecký	k2eAgFnSc4d1	herecká
elitu	elita	k1gFnSc4	elita
do	do	k7c2	do
filmu	film	k1gInSc2	film
Pamětnice	pamětnice	k1gFnSc2	pamětnice
přivedl	přivést	k5eAaPmAgMnS	přivést
autor	autor	k1gMnSc1	autor
scénáře	scénář	k1gInSc2	scénář
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
filmu	film	k1gInSc2	film
Tomáš	Tomáš	k1gMnSc1	Tomáš
Magnusek	Magnusek	k1gMnSc1	Magnusek
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
moderátor	moderátor	k1gMnSc1	moderátor
pořadu	pořad	k1gInSc2	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Černé	Černé	k2eAgFnSc2d1	Černé
ovce	ovce	k1gFnSc2	ovce
Vlado	Vlado	k1gNnSc1	Vlado
Štancel	Štancel	k1gMnPc2	Štancel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Náchodský	náchodský	k2eAgInSc1d1	náchodský
zámeček	zámeček	k1gInSc1	zámeček
je	být	k5eAaImIp3nS	být
zmiňován	zmiňovat	k5eAaImNgInS	zmiňovat
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
lidové	lidový	k2eAgFnSc6d1	lidová
písni	píseň	k1gFnSc6	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
se	se	k3xPyFc4	se
točila	točit	k5eAaImAgFnS	točit
trilogie	trilogie	k1gFnSc1	trilogie
Bastardi	bastard	k1gMnPc1	bastard
<g/>
,	,	kIx,	,
producentem	producent	k1gMnSc7	producent
filmu	film	k1gInSc2	film
byl	být	k5eAaImAgMnS	být
Tomáš	Tomáš	k1gMnSc1	Tomáš
Magnusek	Magnusek	k1gMnSc1	Magnusek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
obchvat	obchvat	k1gInSc1	obchvat
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Centrem	centr	k1gInSc7	centr
města	město	k1gNnSc2	město
Náchoda	Náchod	k1gInSc2	Náchod
prochází	procházet	k5eAaImIp3nS	procházet
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
silnice	silnice	k1gFnSc1	silnice
1	[number]	k4	1
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
33	[number]	k4	33
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
67	[number]	k4	67
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
tahem	tah	k1gInSc7	tah
tranzitní	tranzitní	k2eAgFnSc2d1	tranzitní
nákladní	nákladní	k2eAgFnSc2d1	nákladní
dopravy	doprava	k1gFnSc2	doprava
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
přilehlého	přilehlý	k2eAgNnSc2d1	přilehlé
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
projede	projet	k5eAaPmIp3nS	projet
denně	denně	k6eAd1	denně
několik	několik	k4yIc1	několik
tisíc	tisíc	k4xCgInSc1	tisíc
nákladních	nákladní	k2eAgNnPc2d1	nákladní
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
v	v	k7c6	v
dopravní	dopravní	k2eAgFnSc6d1	dopravní
špičce	špička	k1gFnSc6	špička
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
tvoří	tvořit	k5eAaImIp3nS	tvořit
dopravní	dopravní	k2eAgFnSc1d1	dopravní
zácpa	zácpa	k1gFnSc1	zácpa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mnohdy	mnohdy	k6eAd1	mnohdy
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
za	za	k7c2	za
hranice	hranice	k1gFnSc2	hranice
města	město	k1gNnSc2	město
samotného	samotný	k2eAgNnSc2d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
dopravní	dopravní	k2eAgFnSc4d1	dopravní
situaci	situace	k1gFnSc4	situace
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
vyřešit	vyřešit	k5eAaPmF	vyřešit
obchvat	obchvat	k1gInSc1	obchvat
města	město	k1gNnSc2	město
Náchoda	Náchod	k1gInSc2	Náchod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
vyprojektován	vyprojektován	k2eAgInSc1d1	vyprojektován
a	a	k8xC	a
momentálně	momentálně	k6eAd1	momentálně
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
před	před	k7c7	před
územním	územní	k2eAgNnSc7d1	územní
řízením	řízení	k1gNnSc7	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
stále	stále	k6eAd1	stále
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
budoucí	budoucí	k2eAgInSc1d1	budoucí
obchvat	obchvat	k1gInSc1	obchvat
městu	město	k1gNnSc3	město
pomůže	pomoct	k5eAaPmIp3nS	pomoct
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
tranzitní	tranzitní	k2eAgFnSc4d1	tranzitní
dopravu	doprava	k1gFnSc4	doprava
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
odvede	odvést	k5eAaPmIp3nS	odvést
připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
projektu	projekt	k1gInSc2	projekt
železniční	železniční	k2eAgInPc1d1	železniční
propojení	propojení	k1gNnSc4	propojení
Náchoda	Náchod	k1gInSc2	Náchod
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
Skalicí	Skalice	k1gFnSc7	Skalice
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
železniční	železniční	k2eAgInSc1d1	železniční
tunel	tunel	k1gInSc1	tunel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nS	by
dopravě	doprava	k1gFnSc3	doprava
<g />
.	.	kIx.	.
</s>
<s>
pomohl	pomoct	k5eAaPmAgInS	pomoct
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
minimálně	minimálně	k6eAd1	minimálně
srovnatelně	srovnatelně	k6eAd1	srovnatelně
jako	jako	k8xC	jako
obchvat	obchvat	k1gInSc4	obchvat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
železnici	železnice	k1gFnSc4	železnice
k	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
cestám	cesta	k1gFnPc3	cesta
využívají	využívat	k5eAaPmIp3nP	využívat
jak	jak	k6eAd1	jak
lidé	člověk	k1gMnPc1	člověk
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
aktuální	aktuální	k2eAgInSc1d1	aktuální
stav	stav	k1gInSc1	stav
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
poptávce	poptávka	k1gFnSc3	poptávka
po	po	k7c6	po
dopravním	dopravní	k2eAgNnSc6d1	dopravní
spojení	spojení	k1gNnSc6	spojení
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nP	jezdit
po	po	k7c6	po
tratích	trať	k1gFnPc6	trať
vybudovaných	vybudovaný	k2eAgMnPc2d1	vybudovaný
ve	v	k7c6	v
století	století	k1gNnSc6	století
devatenáctém	devatenáctý	k4xOgNnSc6	devatenáctý
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
variantou	varianta	k1gFnSc7	varianta
železničního	železniční	k2eAgNnSc2d1	železniční
spojení	spojení	k1gNnSc2	spojení
Náchoda	Náchod	k1gInSc2	Náchod
na	na	k7c4	na
západ	západ	k1gInSc4	západ
je	být	k5eAaImIp3nS	být
uvažovaná	uvažovaný	k2eAgFnSc1d1	uvažovaná
levnější	levný	k2eAgFnSc1d2	levnější
varianta	varianta	k1gFnSc1	varianta
–	–	k?	–
trať	trať	k1gFnSc1	trať
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
–	–	k?	–
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významní	významný	k2eAgMnPc1d1	významný
rodáci	rodák	k1gMnPc1	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Bělobrádek	Bělobrádka	k1gFnPc2	Bělobrádka
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
předseda	předseda	k1gMnSc1	předseda
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Birke	Birk	k1gFnSc2	Birk
(	(	kIx(	(
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Náchoda	Náchod	k1gInSc2	Náchod
</s>
</p>
<p>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
Blažek	Blažek	k1gMnSc1	Blažek
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
filmový	filmový	k2eAgMnSc1d1	filmový
scenárista	scenárista	k1gMnSc1	scenárista
</s>
</p>
<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Bohadlová	Bohadlový	k2eAgFnSc1d1	Bohadlová
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překladatelka	překladatelka	k1gFnSc1	překladatelka
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
teoretička	teoretička	k1gFnSc1	teoretička
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Burdych	Burdych	k1gMnSc1	Burdych
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
houslový	houslový	k2eAgMnSc1d1	houslový
virtuos	virtuos	k1gMnSc1	virtuos
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Cita	Cita	k1gMnSc1	Cita
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Vysokov	Vysokov	k1gInSc1	Vysokov
u	u	k7c2	u
Náchoda	Náchod	k1gInSc2	Náchod
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
</s>
</p>
<p>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Erben	Erben	k1gMnSc1	Erben
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Erben	Erben	k1gMnSc1	Erben
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
zejména	zejména	k9	zejména
svými	svůj	k3xOyFgInPc7	svůj
detektivními	detektivní	k2eAgInPc7d1	detektivní
příběhy	příběh	k1gInPc7	příběh
s	s	k7c7	s
kapitánem	kapitán	k1gMnSc7	kapitán
Exnerem	Exner	k1gMnSc7	Exner
</s>
</p>
<p>
<s>
Vendula	Vendula	k1gFnSc1	Vendula
Frintová	Frintový	k2eAgFnSc1d1	Frintová
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
triatlonistka	triatlonistka	k1gFnSc1	triatlonistka
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Grym	Grym	k1gMnSc1	Grym
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Herman	Herman	k1gMnSc1	Herman
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fagotista	fagotista	k1gMnSc1	fagotista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
</s>
</p>
<p>
<s>
Zdeňka	Zdeňka	k1gFnSc1	Zdeňka
Horníková	Horníková	k1gFnSc1	Horníková
(	(	kIx(	(
<g/>
*	*	kIx~	*
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
Hurdálek	Hurdálek	k1gMnSc1	Hurdálek
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1747	[number]	k4	1747
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
1833	[number]	k4	1833
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
litoměřický	litoměřický	k2eAgMnSc1d1	litoměřický
biskup	biskup	k1gMnSc1	biskup
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Letzel	Letzel	k1gMnSc1	Letzel
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Vratislav	Vratislav	k1gFnSc1	Vratislav
Lokvenc	Lokvenc	k1gFnSc1	Lokvenc
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Menzel	Menzel	k1gMnSc1	Menzel
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Mrázek	Mrázek	k1gMnSc1	Mrázek
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
československý	československý	k2eAgMnSc1d1	československý
letec	letec	k1gMnSc1	letec
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1942	[number]	k4	1942
a	a	k8xC	a
1943	[number]	k4	1943
velitel	velitel	k1gMnSc1	velitel
čs	čs	kA	čs
<g/>
.	.	kIx.	.
stíhacího	stíhací	k2eAgNnSc2d1	stíhací
křídla	křídlo	k1gNnSc2	křídlo
v	v	k7c6	v
RAF	raf	k0	raf
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Pinkava	Pinkava	k?	Pinkava
(	(	kIx(	(
<g/>
*	*	kIx~	*
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Přibyl	Přibyl	k1gMnSc1	Přibyl
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operní	operní	k2eAgMnSc1d1	operní
pěvec	pěvec	k1gMnSc1	pěvec
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Raichl	Raichl	k1gMnSc1	Raichl
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Roth	Roth	k1gMnSc1	Roth
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
nebo	nebo	k8xC	nebo
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kameraman	kameraman	k1gMnSc1	kameraman
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Samko	Samko	k1gNnSc4	Samko
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
ČT	ČT	kA	ČT
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Sivek	sivka	k1gFnPc2	sivka
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lední	lední	k2eAgMnSc1d1	lední
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Luba	Lub	k2eAgFnSc1d1	Luba
Skořepová	Skořepová	k1gFnSc1	Skořepová
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Libuše	Libuše	k1gFnSc2	Libuše
Skořepová	Skořepová	k1gFnSc1	Skořepová
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
-	-	kIx~	-
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Souček	Souček	k1gMnSc1	Souček
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filolog	filolog	k1gMnSc1	filolog
<g/>
,	,	kIx,	,
bohemista	bohemista	k1gMnSc1	bohemista
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
historik	historik	k1gMnSc1	historik
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Strnad	Strnad	k1gMnSc1	Strnad
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1746	[number]	k4	1746
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
Sazená	sazený	k2eAgFnSc1d1	Sazená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
meteorolog	meteorolog	k1gMnSc1	meteorolog
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
rektor	rektor	k1gMnSc1	rektor
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
v	v	k7c6	v
Klementinu	Klementinum	k1gNnSc6	Klementinum
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
vícenásobný	vícenásobný	k2eAgMnSc1d1	vícenásobný
mistr	mistr	k1gMnSc1	mistr
ČR	ČR	kA	ČR
a	a	k8xC	a
světa	svět	k1gInSc2	svět
v	v	k7c6	v
trialu	trial	k1gInSc6	trial
a	a	k8xC	a
bike	bike	k1gNnSc6	bike
trialu	trial	k1gInSc2	trial
</s>
</p>
<p>
<s>
Oskar	Oskar	k1gMnSc1	Oskar
Sýkora	Sýkora	k1gMnSc1	Sýkora
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
Halifax	Halifax	k1gInSc1	Halifax
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
stomatologie	stomatologie	k1gFnSc2	stomatologie
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Šafář	Šafář	k1gMnSc1	Šafář
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1921	[number]	k4	1921
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
kronikář	kronikář	k1gMnSc1	kronikář
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Škvorecký	Škvorecký	k2eAgMnSc1d1	Škvorecký
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
nakladatel	nakladatel	k1gMnSc1	nakladatel
</s>
</p>
<p>
<s>
Bohumír	Bohumír	k1gMnSc1	Bohumír
Španiel	Španiel	k1gMnSc1	Španiel
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Špáta	Špáta	k1gMnSc1	Špáta
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
dokumentarista	dokumentarista	k1gMnSc1	dokumentarista
<g/>
,	,	kIx,	,
kameraman	kameraman	k1gMnSc1	kameraman
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potápěč	potápěč	k1gMnSc1	potápěč
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Tošovský	Tošovský	k1gMnSc1	Tošovský
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bankéř	bankéř	k1gMnSc1	bankéř
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
premiér	premiér	k1gMnSc1	premiér
přechodné	přechodný	k2eAgFnSc2d1	přechodná
vlády	vláda	k1gFnSc2	vláda
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Všetička	Všetička	k1gFnSc1	Všetička
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Plötzensee	Plötzensee	k1gFnSc1	Plötzensee
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
</s>
</p>
<p>
<s>
Sláva	Sláva	k1gFnSc1	Sláva
Vorlová	Vorlová	k1gFnSc1	Vorlová
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladatelka	skladatelka	k1gFnSc1	skladatelka
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
státem	stát	k1gInSc7	stát
evidovaných	evidovaný	k2eAgFnPc2d1	evidovaná
památek	památka	k1gFnPc2	památka
==	==	k?	==
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
zámek	zámek	k1gInSc1	zámek
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
Ul	ul	kA	ul
<g/>
.	.	kIx.	.
Smiřických	smiřický	k2eAgFnPc6d1	Smiřická
(	(	kIx(	(
<g/>
Národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
evidenčního	evidenční	k2eAgInSc2d1	evidenční
areálu	areál	k1gInSc2	areál
1462	[number]	k4	1462
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
–	–	k?	–
Nám.	Nám.	k1gFnSc2	Nám.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
čp.	čp.	k?	čp.
37	[number]	k4	37
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1464	[number]	k4	1464
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pranýř	pranýř	k1gInSc1	pranýř
–	–	k?	–
Nám.	Nám.	k1gFnSc2	Nám.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc4	Masaryk
vedle	vedle	k7c2	vedle
Staré	Staré	k2eAgFnSc2d1	Staré
radnice	radnice	k1gFnSc2	radnice
čp.	čp.	k?	čp.
1	[number]	k4	1
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1478	[number]	k4	1478
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Historická	historický	k2eAgFnSc1d1	historická
budova	budova	k1gFnSc1	budova
radnice	radnice	k1gFnSc2	radnice
–	–	k?	–
Nám.	Nám.	k1gFnSc2	Nám.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
čp.	čp.	k?	čp.
1	[number]	k4	1
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1466	[number]	k4	1466
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
radnice	radnice	k1gFnSc1	radnice
–	–	k?	–
Nám.	Nám.	k1gFnSc2	Nám.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
čp.	čp.	k?	čp.
40	[number]	k4	40
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
18739	[number]	k4	18739
(	(	kIx(	(
<g/>
1467	[number]	k4	1467
<g/>
))	))	k?	))
</s>
</p>
<p>
<s>
Hotel	hotel	k1gInSc1	hotel
a	a	k8xC	a
divadlo	divadlo	k1gNnSc1	divadlo
Beránek	Beránek	k1gMnSc1	Beránek
–	–	k?	–
Severní	severní	k2eAgFnSc1d1	severní
strana	strana	k1gFnSc1	strana
nám.	nám.	k?	nám.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
čp.	čp.	k?	čp.
74	[number]	k4	74
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1482	[number]	k4	1482
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
děkanství	děkanství	k1gNnSc2	děkanství
–	–	k?	–
Severní	severní	k2eAgFnSc1d1	severní
strana	strana	k1gFnSc1	strana
nám.	nám.	k?	nám.
T.G.	T.G.	k1gMnSc2	T.G.
<g/>
Masaryka	Masaryk	k1gMnSc2	Masaryk
čp.	čp.	k?	čp.
75	[number]	k4	75
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1465	[number]	k4	1465
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Historická	historický	k2eAgFnSc1d1	historická
kašna	kašna	k1gFnSc1	kašna
velká	velký	k2eAgFnSc1d1	velká
–	–	k?	–
Před	před	k7c7	před
poštovním	poštovní	k2eAgInSc7d1	poštovní
úřadem	úřad	k1gInSc7	úřad
čp.	čp.	k?	čp.
43	[number]	k4	43
na	na	k7c4	na
nám.	nám.	k?	nám.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
čp.	čp.	k?	čp.
40	[number]	k4	40
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1475	[number]	k4	1475
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Trojice	trojice	k1gFnSc1	trojice
–	–	k?	–
Severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
strana	strana	k1gFnSc1	strana
nám.	nám.	k?	nám.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
čp.	čp.	k?	čp.
75	[number]	k4	75
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1472	[number]	k4	1472
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
–	–	k?	–
Nám.	Nám.	k1gFnSc2	Nám.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
vedle	vedle	k7c2	vedle
kostela	kostel	k1gInSc2	kostel
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1473	[number]	k4	1473
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sousoší	sousoší	k1gNnSc1	sousoší
Kalvarie	Kalvarie	k1gFnSc2	Kalvarie
–	–	k?	–
Nám.	Nám.	k1gFnSc2	Nám.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
za	za	k7c7	za
kněžištěm	kněžiště	k1gNnSc7	kněžiště
kostela	kostel	k1gInSc2	kostel
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1464	[number]	k4	1464
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
–	–	k?	–
Nám.	Nám.	k1gFnSc2	Nám.
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
na	na	k7c6	na
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1471	[number]	k4	1471
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Podstavec	podstavec	k1gInSc1	podstavec
pro	pro	k7c4	pro
sochu	socha	k1gFnSc4	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
–	–	k?	–
U	u	k7c2	u
spočinku	spočinek	k1gInSc2	spočinek
zámeckého	zámecký	k2eAgNnSc2d1	zámecké
schodiště	schodiště	k1gNnSc2	schodiště
v	v	k7c6	v
Regnerových	Regnerův	k2eAgInPc6d1	Regnerův
sadech	sad	k1gInPc6	sad
pod	pod	k7c7	pod
zámkem	zámek	k1gInSc7	zámek
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1462	[number]	k4	1462
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nájemní	nájemní	k2eAgInSc1d1	nájemní
dům	dům	k1gInSc1	dům
s	s	k7c7	s
barokní	barokní	k2eAgFnSc7d1	barokní
sochou	socha	k1gFnSc7	socha
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
Bolestné	bolestný	k2eAgFnSc2d1	bolestná
–	–	k?	–
Riegrova	Riegrov	k1gInSc2	Riegrov
ulice	ulice	k1gFnSc2	ulice
čp.	čp.	k?	čp.
866	[number]	k4	866
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1477	[number]	k4	1477
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
středověkého	středověký	k2eAgNnSc2d1	středověké
opevnění	opevnění	k1gNnSc2	opevnění
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgNnSc1d1	postavené
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1325	[number]	k4	1325
–	–	k?	–
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc4	číslo
areálu	areál	k1gInSc2	areál
1481	[number]	k4	1481
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
Cyrila	Cyril	k1gMnSc2	Cyril
Bartoně	Bartoň	k1gMnSc2	Bartoň
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
knihovna	knihovna	k1gFnSc1	knihovna
–	–	k?	–
Ul	ul	kA	ul
<g/>
.	.	kIx.	.
Kamenice	Kamenice	k1gFnSc1	Kamenice
čp.	čp.	k?	čp.
105	[number]	k4	105
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1483	[number]	k4	1483
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Michaela	Michael	k1gMnSc2	Michael
–	–	k?	–
Komenského	Komenský	k1gMnSc2	Komenský
ul	ul	kA	ul
<g/>
.	.	kIx.	.
čp.	čp.	k?	čp.
38	[number]	k4	38
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1468	[number]	k4	1468
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
–	–	k?	–
Komenského	Komenský	k1gMnSc2	Komenský
ul	ul	kA	ul
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
čelně	čelně	k6eAd1	čelně
vpravo	vpravo	k6eAd1	vpravo
kostelíka	kostelík	k1gInSc2	kostelík
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc4	Michal
poblíže	poblíže	k7c2	poblíže
školy	škola	k1gFnSc2	škola
čp.	čp.	k?	čp.
425	[number]	k4	425
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1476	[number]	k4	1476
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hřbitovní	hřbitovní	k2eAgInSc1d1	hřbitovní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
–	–	k?	–
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
,	,	kIx,	,
Hřbitovní	hřbitovní	k2eAgFnSc1d1	hřbitovní
ul	ul	kA	ul
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1469	[number]	k4	1469
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Městský	městský	k2eAgInSc1d1	městský
hřbitov	hřbitov	k1gInSc1	hřbitov
–	–	k?	–
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
n.	n.	k?	n.
Met	Mety	k1gFnPc2	Mety
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Hřbitovní	hřbitovní	k2eAgFnSc1d1	hřbitovní
ul	ul	kA	ul
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1469	[number]	k4	1469
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Socha	Socha	k1gMnSc1	Socha
Ukřižování	ukřižování	k1gNnSc2	ukřižování
–	–	k?	–
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
n.	n.	k?	n.
Met	Mety	k1gFnPc2	Mety
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Českoskalická	českoskalický	k2eAgFnSc1d1	Českoskalická
ul	ul	kA	ul
<g/>
.	.	kIx.	.
u	u	k7c2	u
hřbitova	hřbitov	k1gInSc2	hřbitov
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
6013	[number]	k4	6013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Socha	Socha	k1gMnSc1	Socha
Ukřižování	ukřižování	k1gNnSc2	ukřižování
–	–	k?	–
Na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
ulic	ulice	k1gFnPc2	ulice
B.	B.	kA	B.
Němcové	Němcová	k1gFnPc1	Němcová
<g/>
,	,	kIx,	,
Purkyňovy	Purkyňův	k2eAgFnPc1d1	Purkyňova
a	a	k8xC	a
Bartoňovy	Bartoňův	k2eAgFnPc1d1	Bartoňova
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
u	u	k7c2	u
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
465	[number]	k4	465
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
6016	[number]	k4	6016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Socha	Socha	k1gMnSc1	Socha
Ukřižování	ukřižování	k1gNnSc2	ukřižování
–	–	k?	–
Na	na	k7c6	na
rozcestí	rozcestí	k1gNnSc6	rozcestí
ulice	ulice	k1gFnSc2	ulice
Purkyňovy	Purkyňův	k2eAgFnSc2d1	Purkyňova
a	a	k8xC	a
Dobrošovské	Dobrošovská	k1gFnSc2	Dobrošovská
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
opěrné	opěrný	k2eAgFnSc2d1	opěrná
zdi	zeď	k1gFnSc2	zeď
zahrady	zahrada	k1gFnSc2	zahrada
hotelu	hotel	k1gInSc2	hotel
Hron	Hron	k1gInSc1	Hron
čp.	čp.	k?	čp.
436	[number]	k4	436
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
6014	[number]	k4	6014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kaple	kaple	k1gFnSc1	kaple
Zvěstování	zvěstování	k1gNnSc2	zvěstování
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
–	–	k?	–
Plhov	Plhov	k1gInSc1	Plhov
<g/>
,	,	kIx,	,
Na	na	k7c4	na
Hrobku	hrobka	k1gFnSc4	hrobka
<g/>
,	,	kIx,	,
Tylova	Tylův	k2eAgFnSc1d1	Tylova
ul	ul	kA	ul
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1470	[number]	k4	1470
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zámecká	zámecký	k2eAgFnSc1d1	zámecká
jízdárna	jízdárna	k1gFnSc1	jízdárna
–	–	k?	–
Galerie	galerie	k1gFnSc1	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Smiřických	smiřický	k2eAgInPc2d1	smiřický
čp.	čp.	k?	čp.
272	[number]	k4	272
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1462	[number]	k4	1462
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
–	–	k?	–
Alej	alej	k1gFnSc4	alej
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Zaháňské	zaháňský	k2eAgFnPc1d1	Zaháňská
<g/>
,	,	kIx,	,
asi	asi	k9	asi
140	[number]	k4	140
m	m	kA	m
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
zámku	zámek	k1gInSc2	zámek
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1462	[number]	k4	1462
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pamětní	pamětní	k2eAgInSc1d1	pamětní
kříž	kříž	k1gInSc1	kříž
ze	z	k7c2	z
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
–	–	k?	–
V	v	k7c6	v
Aleji	alej	k1gFnSc6	alej
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Zaháňské	zaháňský	k2eAgFnSc2d1	Zaháňská
na	na	k7c6	na
vzdálenějším	vzdálený	k2eAgInSc6d2	vzdálenější
konci	konec	k1gInSc6	konec
od	od	k7c2	od
zámku	zámek	k1gInSc2	zámek
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1485	[number]	k4	1485
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
hřbitov	hřbitov	k1gInSc1	hřbitov
z	z	k7c2	z
války	válka	k1gFnSc2	válka
1866	[number]	k4	1866
–	–	k?	–
Galerie	galerie	k1gFnSc1	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Smiřických	smiřický	k2eAgInPc2d1	smiřický
čp.	čp.	k?	čp.
272	[number]	k4	272
<g/>
,	,	kIx,	,
v	v	k7c6	v
Aleji	alej	k1gFnSc6	alej
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Zaháňské	zaháňský	k2eAgFnSc2d1	Zaháňská
na	na	k7c6	na
vzdálenějším	vzdálený	k2eAgInSc6d2	vzdálenější
konci	konec	k1gInSc6	konec
od	od	k7c2	od
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
hřbitov	hřbitov	k1gInSc4	hřbitov
založený	založený	k2eAgInSc4d1	založený
pro	pro	k7c4	pro
zemřelé	zemřelý	k2eAgMnPc4d1	zemřelý
císařské	císařský	k2eAgMnPc4d1	císařský
vojáky	voják	k1gMnPc4	voják
ze	z	k7c2	z
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
roku	rok	k1gInSc2	rok
1762	[number]	k4	1762
<g/>
,	,	kIx,	,
upraven	upravit	k5eAaPmNgInS	upravit
jako	jako	k8xS	jako
romantická	romantický	k2eAgFnSc1d1	romantická
zřícenina	zřícenina	k1gFnSc1	zřícenina
gotického	gotický	k2eAgInSc2d1	gotický
chrámu	chrám	k1gInSc2	chrám
pro	pro	k7c4	pro
vojáky	voják	k1gMnPc4	voják
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
zemřelé	zemřelá	k1gFnSc2	zemřelá
na	na	k7c4	na
následky	následek	k1gInPc4	následek
bojů	boj	k1gInPc2	boj
u	u	k7c2	u
Náchoda	Náchod	k1gInSc2	Náchod
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
Skalice	Skalice	k1gFnSc2	Skalice
<g/>
,	,	kIx,	,
Svinšťan	Svinšťan	k1gMnSc1	Svinšťan
i	i	k8xC	i
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
1485	[number]	k4	1485
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kolonie	kolonie	k1gFnSc1	kolonie
rodinných	rodinný	k2eAgInPc2d1	rodinný
dělnických	dělnický	k2eAgInPc2d1	dělnický
domků	domek	k1gInPc2	domek
–	–	k?	–
Babí	babit	k5eAaImIp3nS	babit
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
<g/>
,	,	kIx,	,
čp.	čp.	k?	čp.
154	[number]	k4	154
<g/>
–	–	k?	–
<g/>
161	[number]	k4	161
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
4780	[number]	k4	4780
<g/>
)	)	kIx)	)
–	–	k?	–
architekt	architekt	k1gMnSc1	architekt
Adolf	Adolf	k1gMnSc1	Adolf
Loos	Loos	k1gInSc4	Loos
</s>
</p>
<p>
<s>
Socha	Socha	k1gMnSc1	Socha
Ukřižování	ukřižování	k1gNnSc2	ukřižování
–	–	k?	–
Běloves	Běloves	k1gMnSc1	Běloves
<g/>
,	,	kIx,	,
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Na	na	k7c6	na
Horním	horní	k2eAgInSc6d1	horní
Konci	konec	k1gInSc6	konec
na	na	k7c6	na
zahradě	zahrada	k1gFnSc6	zahrada
domu	dům	k1gInSc2	dům
čp.	čp.	k?	čp.
127	[number]	k4	127
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
5955	[number]	k4	5955
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Socha	Socha	k1gMnSc1	Socha
Ukřižování	ukřižování	k1gNnSc2	ukřižování
–	–	k?	–
Běloves	Běloves	k1gMnSc1	Běloves
<g/>
,	,	kIx,	,
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Kladská	kladský	k2eAgFnSc1d1	Kladská
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
čp.	čp.	k?	čp.
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Rozcestí	rozcestí	k1gNnSc1	rozcestí
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Kladské	kladský	k2eAgFnSc2d1	Kladská
a	a	k8xC	a
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Na	na	k7c6	na
Koletově	Koletův	k2eAgFnSc6d1	Koletova
(	(	kIx(	(
<g/>
Evidovaná	evidovaný	k2eAgFnSc1d1	evidovaná
památka	památka	k1gFnSc1	památka
číslo	číslo	k1gNnSc1	číslo
5962	[number]	k4	5962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
lokality	lokalita	k1gFnPc1	lokalita
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c4	v
Náchodězámek	Náchodězámek	k1gInSc4	Náchodězámek
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
příkopu	příkop	k1gInSc6	příkop
žijí	žít	k5eAaImIp3nP	žít
medvědi	medvěd	k1gMnPc1	medvěd
Ludvík	Ludvík	k1gMnSc1	Ludvík
a	a	k8xC	a
Dáša	Dáša	k1gFnSc1	Dáša
</s>
</p>
<p>
<s>
Bartoňova	Bartoňův	k2eAgFnSc1d1	Bartoňova
vila	vila	k1gFnSc1	vila
architekta	architekt	k1gMnSc2	architekt
Pavla	Pavel	k1gMnSc2	Pavel
Janáka	Janák	k1gMnSc2	Janák
</s>
</p>
<p>
<s>
pěchotní	pěchotní	k2eAgInSc1d1	pěchotní
srub	srub	k1gInSc1	srub
Březinka	březinka	k1gFnSc1	březinka
</s>
</p>
<p>
<s>
zrušené	zrušený	k2eAgFnPc1d1	zrušená
lázně	lázeň	k1gFnPc1	lázeň
Běloves	Běloves	k1gInSc1	Běloves
<g/>
,	,	kIx,	,
minerální	minerální	k2eAgInSc1d1	minerální
pramen	pramen	k1gInSc1	pramen
Ida	ido	k1gNnSc2	ido
</s>
</p>
<p>
<s>
lyžařské	lyžařský	k2eAgFnSc2d1	lyžařská
sjezdovky	sjezdovka	k1gFnSc2	sjezdovka
Brabák	Brabák	k1gInSc1	Brabák
a	a	k8xC	a
Maliňák	Maliňák	k1gInSc1	Maliňák
s	s	k7c7	s
moderními	moderní	k2eAgInPc7d1	moderní
vleky	vlek	k1gInPc7	vlek
na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
úbočí	úbočí	k1gNnSc6	úbočí
hřebenu	hřeben	k1gInSc2	hřeben
DobrošovaV	DobrošovaV	k1gFnSc2	DobrošovaV
okolí	okolí	k1gNnSc2	okolí
do	do	k7c2	do
patnácti	patnáct	k4xCc2	patnáct
kilometrůúdolí	kilometrůúdolí	k1gNnPc2	kilometrůúdolí
řeky	řeka	k1gFnSc2	řeka
Metuje	Metuje	k1gFnSc2	Metuje
(	(	kIx(	(
<g/>
přírodní	přírodní	k2eAgFnPc1d1	přírodní
rezervace	rezervace	k1gFnPc1	rezervace
<g/>
)	)	kIx)	)
s	s	k7c7	s
osadou	osada	k1gFnSc7	osada
Ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
Peklo	peklo	k1gNnSc1	peklo
–	–	k?	–
4	[number]	k4	4
km	km	kA	km
</s>
</p>
<p>
<s>
Jiráskova	Jiráskův	k2eAgFnSc1d1	Jiráskova
chata	chata	k1gFnSc1	chata
na	na	k7c6	na
Dobrošově	Dobrošův	k2eAgFnSc6d1	Dobrošova
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
(	(	kIx(	(
<g/>
622	[number]	k4	622
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
s	s	k7c7	s
vyhlídkovou	vyhlídkový	k2eAgFnSc7d1	vyhlídková
věží	věž	k1gFnSc7	věž
po	po	k7c6	po
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
–	–	k?	–
4	[number]	k4	4
km	km	kA	km
</s>
</p>
<p>
<s>
pevnost	pevnost	k1gFnSc1	pevnost
Dobrošov	Dobrošov	k1gInSc1	Dobrošov
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
–	–	k?	–
4	[number]	k4	4
km	km	kA	km
</s>
</p>
<p>
<s>
kóta	kóta	k1gFnSc1	kóta
Maliňák	Maliňák	k1gMnSc1	Maliňák
(	(	kIx(	(
<g/>
cca	cca	kA	cca
615	[number]	k4	615
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
s	s	k7c7	s
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
dělostřelecké	dělostřelecký	k2eAgFnSc2d1	dělostřelecká
výsuvné	výsuvný	k2eAgFnSc2d1	výsuvná
věže	věž	k1gFnSc2	věž
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
pevnosti	pevnost	k1gFnSc2	pevnost
Dobrošov	Dobrošov	k1gInSc1	Dobrošov
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozhledem	rozhled	k1gInSc7	rozhled
od	od	k7c2	od
Orlických	orlický	k2eAgFnPc2d1	Orlická
hor	hora	k1gFnPc2	hora
přes	přes	k7c4	přes
Šumavu	Šumava	k1gFnSc4	Šumava
po	po	k7c4	po
Krkonoše	Krkonoše	k1gFnPc4	Krkonoše
–	–	k?	–
4	[number]	k4	4
km	km	kA	km
</s>
</p>
<p>
<s>
hraniční	hraniční	k2eAgInSc4d1	hraniční
přechod	přechod	k1gInSc4	přechod
Náchod-Běloves	Náchod-Běloves	k1gMnSc1	Náchod-Běloves
<g/>
/	/	kIx~	/
<g/>
Kudowa	Kudowa	k1gMnSc1	Kudowa
Słone	Słon	k1gInSc5	Słon
–	–	k?	–
4	[number]	k4	4
km	km	kA	km
</s>
</p>
<p>
<s>
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Rozkoš	rozkoš	k1gFnSc1	rozkoš
–	–	k?	–
8	[number]	k4	8
km	km	kA	km
</s>
</p>
<p>
<s>
Babiččino	babiččin	k2eAgNnSc1d1	Babiččino
údolí	údolí	k1gNnSc1	údolí
(	(	kIx(	(
<g/>
Staré	Staré	k2eAgNnSc1d1	Staré
bělidlo	bělidlo	k1gNnSc1	bělidlo
<g/>
,	,	kIx,	,
Ratibořický	ratibořický	k2eAgInSc4d1	ratibořický
zámek	zámek	k1gInSc4	zámek
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
8	[number]	k4	8
km	km	kA	km
</s>
</p>
<p>
<s>
rodný	rodný	k2eAgInSc1d1	rodný
domek	domek	k1gInSc1	domek
A.	A.	kA	A.
Jiráska	Jirásek	k1gMnSc2	Jirásek
v	v	k7c6	v
Hronově	Hronov	k1gInSc6	Hronov
–	–	k?	–
8	[number]	k4	8
km	km	kA	km
</s>
</p>
<p>
<s>
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
"	"	kIx"	"
<g/>
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
Vysokov	Vysokov	k1gInSc1	Vysokov
–	–	k?	–
Václavice	Václavice	k1gFnSc2	Václavice
1866	[number]	k4	1866
<g/>
"	"	kIx"	"
–	–	k?	–
8,5	[number]	k4	8,5
km	km	kA	km
</s>
</p>
<p>
<s>
hora	hora	k1gFnSc1	hora
Turov	Turov	k1gInSc1	Turov
(	(	kIx(	(
<g/>
602	[number]	k4	602
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
–	–	k?	–
10	[number]	k4	10
<g/>
km	km	kA	km
</s>
</p>
<p>
<s>
náměstí	náměstí	k1gNnSc1	náměstí
a	a	k8xC	a
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
–	–	k?	–
10	[number]	k4	10
km	km	kA	km
</s>
</p>
<p>
<s>
dům	dům	k1gInSc1	dům
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Skalici	Skalice	k1gFnSc6	Skalice
–	–	k?	–
10	[number]	k4	10
kmNad	kmNad	k6eAd1	kmNad
15	[number]	k4	15
kilometrůPřírodní	kilometrůPřírodní	k2eAgFnSc2d1	kilometrůPřírodní
rezervace	rezervace	k1gFnSc2	rezervace
Ostaš	Ostaš	k1gInSc1	Ostaš
–	–	k?	–
20	[number]	k4	20
km	km	kA	km
</s>
</p>
<p>
<s>
Orlické	orlický	k2eAgFnSc2d1	Orlická
hory	hora	k1gFnSc2	hora
–	–	k?	–
Deštné	deštný	k2eAgNnSc1d1	Deštné
v	v	k7c6	v
O.h.	O.h.	k1gFnSc6	O.h.
–	–	k?	–
35	[number]	k4	35
km	km	kA	km
</s>
</p>
<p>
<s>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
(	(	kIx(	(
<g/>
KRNAP	KRNAP	kA	KRNAP
<g/>
)	)	kIx)	)
–	–	k?	–
70	[number]	k4	70
km	km	kA	km
</s>
</p>
<p>
<s>
Adršpašsko-teplické	adršpašskoeplický	k2eAgFnSc2d1	adršpašsko-teplický
skály	skála	k1gFnSc2	skála
–	–	k?	–
32	[number]	k4	32
km	km	kA	km
</s>
</p>
<p>
<s>
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
–	–	k?	–
pevnost	pevnost	k1gFnSc1	pevnost
Josefov	Josefov	k1gInSc1	Josefov
–	–	k?	–
cca	cca	kA	cca
25	[number]	k4	25
km	km	kA	km
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgFnPc1d1	Malé
Svatoňovice	Svatoňovice	k1gFnPc1	Svatoňovice
–	–	k?	–
muzeum	muzeum	k1gNnSc1	muzeum
bratří	bratr	k1gMnPc2	bratr
Čapků	Čapek	k1gMnPc2	Čapek
–	–	k?	–
cca	cca	kA	cca
25	[number]	k4	25
km	km	kA	km
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Halberstadt	Halberstadt	k1gInSc1	Halberstadt
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Kladsko	Kladsko	k1gNnSc1	Kladsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
Kudowa-Zdrój	Kudowa-Zdrój	k1gFnSc1	Kudowa-Zdrój
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
Warrington	Warrington	k1gInSc1	Warrington
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
</s>
</p>
<p>
<s>
Bauska	Bauska	k1gFnSc1	Bauska
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAŠTECKÁ	baštecký	k2eAgFnSc1d1	Baštecká
<g/>
,	,	kIx,	,
Lýdie	Lýdie	k1gFnSc1	Lýdie
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Náchod	Náchod	k1gInSc1	Náchod
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
344	[number]	k4	344
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
674	[number]	k4	674
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sborník	sborník	k1gInSc1	sborník
prací	práce	k1gFnPc2	práce
historických	historický	k2eAgFnPc2d1	historická
<g/>
:	:	kIx,	:
k	k	k7c3	k
šedesátým	šedesátý	k4xOgFnPc3	šedesátý
narozeninám	narozeniny	k1gFnPc3	narozeniny
dvor	dvor	k1gInSc4	dvor
<g/>
.	.	kIx.	.
rady	rada	k1gFnSc2	rada
prof.	prof.	kA	prof.
dra	dřít	k5eAaImSgInS	dřít
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Golla	Goll	k1gMnSc2	Goll
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Friedrich	Friedrich	k1gMnSc1	Friedrich
Gustav	Gustav	k1gMnSc1	Gustav
<g/>
,	,	kIx,	,
Kamil	Kamil	k1gMnSc1	Kamil
Krofta	Krofta	k1gMnSc1	Krofta
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Bidlo	bidlo	k1gNnSc1	bidlo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Hist.	Hist.	k1gFnSc1	Hist.
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
388	[number]	k4	388
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
MACHÁT	MACHÁT	kA	MACHÁT
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
:	:	kIx,	:
Příspěvek	příspěvek	k1gInSc1	příspěvek
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
města	město	k1gNnSc2	město
v	v	k7c6	v
XVII	XVII	kA	XVII
<g/>
.	.	kIx.	.
a	a	k8xC	a
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
s.	s.	k?	s.
298	[number]	k4	298
-	-	kIx~	-
308	[number]	k4	308
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
</s>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Náchod	Náchod	k1gInSc1	Náchod
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Náchod	Náchod	k1gInSc1	Náchod
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Náchod	Náchod	k1gInSc1	Náchod
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Náchod	Náchod	k1gInSc1	Náchod
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Náchod	Náchod	k1gInSc1	Náchod
</s>
</p>
<p>
<s>
Turistické	turistický	k2eAgFnSc2d1	turistická
informace	informace	k1gFnSc2	informace
a	a	k8xC	a
nové	nový	k2eAgFnSc2d1	nová
HD	HD	kA	HD
webkamery	webkamera	k1gFnSc2	webkamera
na	na	k7c6	na
Pražské	pražský	k2eAgFnSc6d1	Pražská
ulici	ulice	k1gFnSc6	ulice
</s>
</p>
<p>
<s>
Meteostanice	Meteostanice	k1gFnSc1	Meteostanice
na	na	k7c6	na
Pražské	pražský	k2eAgFnSc6d1	Pražská
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
+	+	kIx~	+
HD	HD	kA	HD
webkamery	webkamera	k1gFnSc2	webkamera
</s>
</p>
<p>
<s>
Meteostanice	Meteostanice	k1gFnSc1	Meteostanice
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
<g/>
,	,	kIx,	,
Bělovsi	Bělovse	k1gFnSc6	Bělovse
+	+	kIx~	+
HD	HD	kA	HD
webkamera	webkamera	k1gFnSc1	webkamera
</s>
</p>
<p>
<s>
====	====	k?	====
O	o	k7c6	o
náchodském	náchodský	k2eAgInSc6d1	náchodský
zámku	zámek	k1gInSc6	zámek
====	====	k?	====
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
náchodském	náchodský	k2eAgInSc6d1	náchodský
zámku	zámek	k1gInSc6	zámek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
zamky-hrady	zamkyrada	k1gFnSc2	zamky-hrada
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Náchodský	náchodský	k2eAgInSc1d1	náchodský
zámek	zámek	k1gInSc1	zámek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
pruvodce	pruvodce	k1gMnSc2	pruvodce
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
