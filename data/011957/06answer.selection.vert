<s>
Luminofor	luminofor	k1gInSc1	luminofor
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
schopná	schopný	k2eAgFnSc1d1	schopná
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
následně	následně	k6eAd1	následně
ji	on	k3xPp3gFnSc4	on
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
luminiscence	luminiscence	k1gFnSc2	luminiscence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
