<s>
IEEE	IEEE	kA
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Institute	institut	k1gInSc5
of	of	k?
Electrical	Electrical	k1gFnSc2
and	and	k?
Electronics	Electronics	k1gInSc1
Engineers	Engineers	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
„	„	k?
<g/>
Institut	institut	k1gInSc1
pro	pro	k7c4
elektrotechnické	elektrotechnický	k2eAgNnSc4d1
a	a	k8xC
elektronické	elektronický	k2eAgNnSc4d1
inženýrství	inženýrství	k1gNnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
nezisková	ziskový	k2eNgFnSc1d1
profesní	profesní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
usilující	usilující	k2eAgFnSc1d1
o	o	k7c4
vzestup	vzestup	k1gInSc4
technologie	technologie	k1gFnSc2
související	související	k2eAgFnPc4d1
s	s	k7c7
elektrotechnikou	elektrotechnika	k1gFnSc7
<g/>
.	.	kIx.
</s>