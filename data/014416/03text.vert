<s>
IEEE	IEEE	kA
</s>
<s>
IEEE	IEEE	kA
Zkratka	zkratka	k1gFnSc1
</s>
<s>
IEEE	IEEE	kA
Vznik	vznik	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1963	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
Profesní	profesní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Park	park	k1gInSc1
Avenue	avenue	k1gFnSc1
3	#num#	k4
<g/>
,	,	kIx,
Manhattan	Manhattan	k1gInSc1
<g/>
,	,	kIx,
100	#num#	k4
16	#num#	k4
<g/>
-	-	kIx~
<g/>
5997	#num#	k4
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
(	(	kIx(
<g/>
3	#num#	k4
Park	park	k1gInSc1
Avenue	avenue	k1gFnSc2
<g/>
)	)	kIx)
Místo	místo	k7c2
</s>
<s>
Piscataway	Piscatawa	k2eAgFnPc1d1
Souřadnice	souřadnice	k1gFnPc1
</s>
<s>
40	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
73	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
52	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Působnost	působnost	k1gFnSc1
</s>
<s>
Celosvětová	celosvětový	k2eAgFnSc1d1
Prezident	prezident	k1gMnSc1
</s>
<s>
Susan	Susan	k1gInSc1
"	"	kIx"
<g/>
Kathy	Katha	k1gFnPc1
<g/>
"	"	kIx"
Land	Land	k1gInSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Ayrton	Ayrton	k1gInSc1
Prize	Prize	k1gFnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.ieee.org	www.ieee.org	k1gInSc1
Dřívější	dřívější	k2eAgInSc4d1
název	název	k1gInSc4
</s>
<s>
Institut	institut	k1gInSc1
pro	pro	k7c4
elektrotechnické	elektrotechnický	k2eAgNnSc4d1
a	a	k8xC
elektronické	elektronický	k2eAgNnSc4d1
inženýrství	inženýrství	k1gNnSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
IEEE	IEEE	kA
<g/>
,	,	kIx,
plným	plný	k2eAgInSc7d1
názvem	název	k1gInSc7
Institute	institut	k1gInSc5
of	of	k?
Electrical	Electrical	k1gFnSc2
and	and	k?
Electronics	Electronics	k1gInSc1
Engineers	Engineers	k1gInSc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
„	„	k?
<g/>
Institut	institut	k1gInSc1
pro	pro	k7c4
elektrotechnické	elektrotechnický	k2eAgNnSc4d1
a	a	k8xC
elektronické	elektronický	k2eAgNnSc4d1
inženýrství	inženýrství	k1gNnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
nezisková	ziskový	k2eNgFnSc1d1
profesní	profesní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
usilující	usilující	k2eAgFnSc1d1
o	o	k7c4
vzestup	vzestup	k1gInSc4
technologie	technologie	k1gFnSc2
související	související	k2eAgFnPc4d1
s	s	k7c7
elektrotechnikou	elektrotechnika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
institut	institut	k1gInSc4
čítá	čítat	k5eAaImIp3nS
nejvíce	nejvíce	k6eAd1,k6eAd3
členů	člen	k1gMnPc2
technické	technický	k2eAgFnSc2d1
profese	profes	k1gFnSc2
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
přes	přes	k7c4
360	#num#	k4
000	#num#	k4
ve	v	k7c6
175	#num#	k4
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
USA	USA	kA
ve	v	k7c6
státě	stát	k1gInSc6
New	New	k1gFnSc2
Jersey	Jersea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
IEEE	IEEE	kA
je	být	k5eAaImIp3nS
zapsáno	zapsat	k5eAaPmNgNnS
ve	v	k7c6
státu	stát	k1gInSc6
New	New	k1gFnSc2
Jersey	Jersea	k1gFnSc2
<g/>
,	,	kIx,
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
roku	rok	k1gInSc2
1963	#num#	k4
sloučením	sloučení	k1gNnSc7
Institute	institut	k1gInSc5
of	of	k?
Radio	radio	k1gNnSc4
Engineers	Engineers	k1gInSc4
(	(	kIx(
<g/>
IRE	Ir	k1gMnSc5
<g/>
,	,	kIx,
založen	založit	k5eAaPmNgInS
1912	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
American	American	k1gMnSc1
Institute	institut	k1gInSc5
of	of	k?
Electrical	Electrical	k1gFnPc1
Engineers	Engineersa	k1gFnPc2
(	(	kIx(
<g/>
AIEE	AIEE	kA
<g/>
,	,	kIx,
založen	založen	k2eAgMnSc1d1
1884	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1
zájmy	zájem	k1gInPc7
AIEE	AIEE	kA
byly	být	k5eAaImAgFnP
drátové	drátový	k2eAgFnPc4d1
komunikace	komunikace	k1gFnPc4
(	(	kIx(
<g/>
telegrafní	telegrafní	k2eAgInPc4d1
a	a	k8xC
telefonní	telefonní	k2eAgInPc4d1
<g/>
)	)	kIx)
a	a	k8xC
elektrické	elektrický	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IRE	Ir	k1gMnSc5
se	se	k3xPyFc4
týkalo	týkat	k5eAaImAgNnS
hlavně	hlavně	k9
radiotechniky	radiotechnika	k1gFnPc1
a	a	k8xC
vytvořilo	vytvořit	k5eAaPmAgNnS
se	se	k3xPyFc4
ze	z	k7c2
dvou	dva	k4xCgFnPc2
menších	malý	k2eAgFnPc2d2
organizací	organizace	k1gFnPc2
–	–	k?
Society	societa	k1gFnSc2
of	of	k?
Wireless	Wireless	k1gInSc1
and	and	k?
Telegraph	Telegraph	k1gInSc1
Engineers	Engineersa	k1gFnPc2
a	a	k8xC
Wireless	Wirelessa	k1gFnPc2
Institute	institut	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
vzestupem	vzestup	k1gInSc7
elektroniky	elektronika	k1gFnSc2
ve	v	k7c6
třicátých	třicátý	k4xOgNnPc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
elektroinženýři	elektroinženýr	k1gMnPc1
obvykle	obvykle	k6eAd1
stávali	stávat	k5eAaImAgMnP
členy	člen	k1gMnPc4
IRE	Ir	k1gMnSc5
<g/>
,	,	kIx,
používání	používání	k1gNnSc6
elektronek	elektronka	k1gFnPc2
se	se	k3xPyFc4
ale	ale	k9
natolik	natolik	k6eAd1
rozšířilo	rozšířit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
obtížné	obtížný	k2eAgNnSc1d1
rozpoznat	rozpoznat	k5eAaPmF
hranice	hranice	k1gFnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
techniky	technika	k1gFnSc2
rozlišující	rozlišující	k2eAgFnSc2d1
IRE	Ir	k1gMnSc5
od	od	k7c2
AIEE	AIEE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
si	se	k3xPyFc3
tyto	tento	k3xDgFnPc4
dvě	dva	k4xCgFnPc4
organizace	organizace	k1gFnPc4
stále	stále	k6eAd1
více	hodně	k6eAd2
konkurovaly	konkurovat	k5eAaImAgFnP
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
je	být	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnSc2
vedení	vedení	k1gNnSc2
rozhodla	rozhodnout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1961	#num#	k4
spojit	spojit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
dvě	dva	k4xCgFnPc1
organizace	organizace	k1gFnPc1
byly	být	k5eAaImAgFnP
formálně	formálně	k6eAd1
sloučeny	sloučit	k5eAaPmNgInP
v	v	k7c6
IEEE	IEEE	kA
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1963	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
významné	významný	k2eAgMnPc4d1
představitele	představitel	k1gMnPc4
IEEE	IEEE	kA
a	a	k8xC
jeho	jeho	k3xOp3gFnPc2
zakládajících	zakládající	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Elihu	Eliha	k1gMnSc4
Thomson	Thomson	k1gMnSc1
(	(	kIx(
<g/>
AIEE	AIEE	kA
<g/>
,	,	kIx,
1889	#num#	k4
<g/>
–	–	k?
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Graham	Graham	k1gMnSc1
Bell	bell	k1gInSc1
(	(	kIx(
<g/>
AIEE	AIEE	kA
<g/>
,	,	kIx,
1891	#num#	k4
<g/>
–	–	k?
<g/>
1892	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Charles	Charles	k1gMnSc1
Proteus	Proteus	k1gMnSc1
Steinmetz	Steinmetz	k1gMnSc1
(	(	kIx(
<g/>
AIEE	AIEE	kA
<g/>
,	,	kIx,
1901	#num#	k4
<g/>
–	–	k?
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lee	Lea	k1gFnSc3
De	De	k?
Forest	Forest	k1gInSc4
(	(	kIx(
<g/>
IRE	Ir	k1gMnSc5
<g/>
,	,	kIx,
1930	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Frederick	Frederick	k1gMnSc1
E.	E.	kA
Terman	Terman	k1gMnSc1
(	(	kIx(
<g/>
IRE	Ir	k1gMnSc5
<g/>
,	,	kIx,
1941	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
William	William	k1gInSc1
R.	R.	kA
Hewlett	Hewlett	kA
(	(	kIx(
<g/>
IRE	Ir	k1gMnSc5
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ernst	Ernst	k1gMnSc1
Weber	Weber	k1gMnSc1
(	(	kIx(
<g/>
IRE	Ir	k1gMnSc5
<g/>
,	,	kIx,
1959	#num#	k4
<g/>
;	;	kIx,
IEEE	IEEE	kA
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivan	Ivan	k1gMnSc1
Getting	Getting	k1gInSc1
(	(	kIx(
<g/>
IEEE	IEEE	kA
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Současnými	současný	k2eAgMnPc7d1
prezidenty	prezident	k1gMnPc7
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
Susan	Susan	k1gInSc4
"	"	kIx"
<g/>
Kathy	Katha	k1gMnSc2
<g/>
"	"	kIx"
Land	Land	k1gMnSc1
jako	jako	k8xS,k8xC
prezident	prezident	k1gMnSc1
a	a	k8xC
K.	K.	kA
J.	J.	kA
Ray	Ray	k1gMnSc1
Liu	Liu	k1gMnSc1
jako	jako	k8xS,k8xC
nově	nově	k6eAd1
zvolený	zvolený	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
pro	pro	k7c4
IEEE	IEEE	kA
a	a	k8xC
Katherine	Katherin	k1gInSc5
J.	J.	kA
Duncan	Duncany	k1gInPc2
pro	pro	k7c4
IEEE-USA	IEEE-USA	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
IEEE	IEEE	kA
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Na	na	k7c6
našem	náš	k3xOp1gInSc6
území	území	k1gNnSc6
vyvíjí	vyvíjet	k5eAaImIp3nS
IEEE	IEEE	kA
činnost	činnost	k1gFnSc4
od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
prostřednictvím	prostřednictvím	k7c2
Československé	československý	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
IEEE	IEEE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
prvým	prvý	k4xOgNnSc7
předsedou	předseda	k1gMnSc7
byl	být	k5eAaImAgMnS
Vratislav	Vratislav	k1gMnSc1
Štěpař	štěpař	k1gMnSc1
<g/>
,	,	kIx,
současným	současný	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
Matej	Matej	k1gInSc4
Pacha	Pach	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československá	československý	k2eAgFnSc1d1
sekce	sekce	k1gFnSc1
pořádá	pořádat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
konferencí	konference	k1gFnPc2
a	a	k8xC
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
časopis	časopis	k1gInSc1
Slaboproudý	slaboproudý	k2eAgInSc1d1
obzor	obzor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
</s>
<s>
Stanovy	stanova	k1gFnPc1
IEEE	IEEE	kA
formulují	formulovat	k5eAaImIp3nP
cíle	cíl	k1gInPc1
organizace	organizace	k1gFnSc2
jako	jako	k8xC,k8xS
“	“	k?
<g/>
vědecké	vědecký	k2eAgFnSc2d1
a	a	k8xC
vzdělávací	vzdělávací	k2eAgFnSc2d1
<g/>
,	,	kIx,
směřující	směřující	k2eAgFnSc2d1
k	k	k7c3
pokroku	pokrok	k1gInSc3
v	v	k7c6
oblasti	oblast	k1gFnSc6
teorie	teorie	k1gFnSc2
a	a	k8xC
praxe	praxe	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
elektrotechnice	elektrotechnika	k1gFnSc6
<g/>
,	,	kIx,
elektronice	elektronika	k1gFnSc6
<g/>
,	,	kIx,
komunikacích	komunikace	k1gFnPc6
a	a	k8xC
výpočetní	výpočetní	k2eAgFnSc6d1
technice	technika	k1gFnSc6
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
v	v	k7c6
informatice	informatika	k1gFnSc6
a	a	k8xC
příbuzných	příbuzný	k2eAgNnPc6d1
odvětvích	odvětví	k1gNnPc6
techniky	technika	k1gFnSc2
a	a	k8xC
souvisejících	související	k2eAgInPc6d1
vědních	vědní	k2eAgInPc6d1
oborech	obor	k1gInPc6
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
je	být	k5eAaImIp3nS
leadrem	leadr	k1gMnSc7
ve	v	k7c6
vývoji	vývoj	k1gInSc6
průmyslových	průmyslový	k2eAgInPc2d1
standardů	standard	k1gInPc2
(	(	kIx(
<g/>
má	mít	k5eAaImIp3nS
vyvinuto	vyvinout	k5eAaPmNgNnS
přes	přes	k7c4
900	#num#	k4
platných	platný	k2eAgInPc2d1
průmyslových	průmyslový	k2eAgInPc2d1
standardů	standard	k1gInPc2
<g/>
)	)	kIx)
v	v	k7c6
rozsáhlé	rozsáhlý	k2eAgFnSc6d1
řadě	řada	k1gFnSc6
disciplín	disciplína	k1gFnPc2
<g/>
,	,	kIx,
zahrnujících	zahrnující	k2eAgMnPc2d1
elektrickou	elektrický	k2eAgFnSc4d1
energii	energie	k1gFnSc4
<g/>
,	,	kIx,
lékařské	lékařský	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
<g/>
,	,	kIx,
zdravotní	zdravotní	k2eAgFnSc4d1
péči	péče	k1gFnSc4
<g/>
,	,	kIx,
informační	informační	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
<g/>
,	,	kIx,
telekomunikace	telekomunikace	k1gFnPc4
<g/>
,	,	kIx,
spotřebitelskou	spotřebitelský	k2eAgFnSc4d1
elektroniku	elektronika	k1gFnSc4
<g/>
,	,	kIx,
dopravu	doprava	k1gFnSc4
<g/>
,	,	kIx,
letectví	letectví	k1gNnSc4
a	a	k8xC
nanotechnologie	nanotechnologie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
vyvinul	vyvinout	k5eAaPmAgInS
a	a	k8xC
podílel	podílet	k5eAaImAgInS
se	se	k3xPyFc4
na	na	k7c6
</s>
<s>
vzdělávacích	vzdělávací	k2eAgFnPc6d1
aktivitách	aktivita	k1gFnPc6
jako	jako	k8xC,k8xS
třeba	třeba	k6eAd1
schválení	schválení	k1gNnSc1
elektrotechnických	elektrotechnický	k2eAgInPc2d1
programů	program	k1gInPc2
v	v	k7c6
institutech	institut	k1gInPc6
vyššího	vysoký	k2eAgNnSc2d2
vzdělávání	vzdělávání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
sponzorem	sponzor	k1gMnSc7
nebo	nebo	k8xC
spolupořadatelem	spolupořadatel	k1gMnSc7
více	hodně	k6eAd2
než	než	k8xS
300	#num#	k4
mezinárodních	mezinárodní	k2eAgFnPc2d1
technických	technický	k2eAgFnPc2d1
konferencí	konference	k1gFnPc2
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
řadu	řada	k1gFnSc4
špičkových	špičkový	k2eAgInPc2d1
vědeckých	vědecký	k2eAgInPc2d1
časopisů	časopis	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
IEEE	IEEE	kA
Transactions	Transactionsa	k1gFnPc2
on	on	k3xPp3gMnSc1
Microwave	Microwav	k1gInSc5
Theory	Theor	k1gMnPc7
and	and	k?
Technique	Techniqu	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
IEEE	IEEE	kA
má	mít	k5eAaImIp3nS
dvojí	dvojí	k4xRgFnSc4
pomocnou	pomocný	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
(	(	kIx(
<g/>
oblastní	oblastní	k2eAgFnSc4d1
a	a	k8xC
technickou	technický	k2eAgFnSc4d1
<g/>
)	)	kIx)
s	s	k7c7
organizačními	organizační	k2eAgInPc7d1
celky	celek	k1gInPc7
založenými	založený	k2eAgInPc7d1
na	na	k7c6
geografii	geografie	k1gFnSc6
(	(	kIx(
<g/>
například	například	k6eAd1
sekce	sekce	k1gFnSc1
IEEE	IEEE	kA
ve	v	k7c6
Philadelphii	Philadelphia	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
technickém	technický	k2eAgNnSc6d1
zaměření	zaměření	k1gNnSc6
(	(	kIx(
<g/>
například	například	k6eAd1
IEEE	IEEE	kA
Computer	computer	k1gInSc4
Society	societa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
řídí	řídit	k5eAaImIp3nS
oddělenou	oddělený	k2eAgFnSc4d1
organizační	organizační	k2eAgFnSc4d1
jednotku	jednotka	k1gFnSc4
IEEE-USA	IEEE-USA	k1gFnSc2
s	s	k7c7
doporučením	doporučení	k1gNnSc7
politiků	politik	k1gMnPc2
a	a	k8xC
realizací	realizace	k1gFnPc2
programů	program	k1gInPc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
členů	člen	k1gInPc2
(	(	kIx(
<g/>
tj.	tj.	kA
veřejnost	veřejnost	k1gFnSc4
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
39	#num#	k4
spolků	spolek	k1gInPc2
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
300	#num#	k4
místními	místní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
pravidelně	pravidelně	k6eAd1
scházejí	scházet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
Standards	Standards	k1gInSc1
Association	Association	k1gInSc1
(	(	kIx(
<g/>
IEEE-SA	IEEE-SA	k1gFnSc1
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
na	na	k7c4
starosti	starost	k1gFnPc4
standardizační	standardizační	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
IEEE	IEEE	kA
<g/>
.	.	kIx.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
sedm	sedm	k4xCc1
kroků	krok	k1gInPc2
standardizačního	standardizační	k2eAgNnSc2d1
nastavení	nastavení	k1gNnSc2
procesu	proces	k1gInSc2
(	(	kIx(
<g/>
do	do	k7c2
zdárného	zdárný	k2eAgNnSc2d1
ukončení	ukončení	k1gNnSc2
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
obvykle	obvykle	k6eAd1
18	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
zajištění	zajištění	k1gNnSc1
sponzorů	sponzor	k1gMnPc2
</s>
<s>
zažádání	zažádání	k1gNnSc1
o	o	k7c6
autorizaci	autorizace	k1gFnSc6
projektu	projekt	k1gInSc2
</s>
<s>
sestavení	sestavení	k1gNnSc1
pracovní	pracovní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
</s>
<s>
návrh	návrh	k1gInSc1
standardů	standard	k1gInPc2
</s>
<s>
tajné	tajný	k2eAgNnSc1d1
hlasování	hlasování	k1gNnSc1
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
potřeba	potřeba	k1gFnSc1
¾	¾	k?
většiny	většina	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
zhodnocení	zhodnocení	k1gNnSc1
komise	komise	k1gFnSc2
</s>
<s>
finální	finální	k2eAgFnSc1d1
volba	volba	k1gFnSc1
</s>
<s>
Standardy	standard	k1gInPc1
a	a	k8xC
standardizačně-vývojový	standardizačně-vývojový	k2eAgInSc1d1
proces	proces	k1gInSc1
IEEE	IEEE	kA
</s>
<s>
IEEE	IEEE	kA
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
předních	přední	k2eAgFnPc2d1
standardizačně-vývojových	standardizačně-vývojový	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vykonává	vykonávat	k5eAaImIp3nS
vývojové	vývojový	k2eAgNnSc4d1
a	a	k8xC
údržbové	údržbový	k2eAgNnSc4d1
funkce	funkce	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
skrze	skrze	k?
IEEE	IEEE	kA
Standards	Standards	k1gInSc1
Association	Association	k1gInSc1
(	(	kIx(
<g/>
IEEE-SA	IEEE-SA	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
IEEE	IEEE	kA
uzavřelo	uzavřít	k5eAaPmAgNnS
na	na	k7c4
devět	devět	k4xCc4
set	set	k1gInSc4
platných	platný	k2eAgInPc2d1
standardů	standard	k1gInPc2
<g/>
,	,	kIx,
s	s	k7c7
pěti	pět	k4xCc7
sty	sto	k4xCgNnPc7
standardy	standard	k1gInPc7
ve	v	k7c6
vývoji	vývoj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
důležitých	důležitý	k2eAgInPc2d1
standardů	standard	k1gInPc2
IEEE	IEEE	kA
je	být	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
standardů	standard	k1gInPc2
IEEE	IEEE	kA
802	#num#	k4
LAN	lano	k1gNnPc2
<g/>
/	/	kIx~
<g/>
WAN	WAN	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
IEEE	IEEE	kA
802.3	802.3	k4
Ethernet	Ethernet	k1gInSc4
a	a	k8xC
IEEE	IEEE	kA
802.11	802.11	k4
Wireless	Wireless	k1gInSc1
Networking	Networking	k1gInSc4
standardy	standard	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
standardizačních	standardizační	k2eAgInPc2d1
procesů	proces	k1gInPc2
IEEE	IEEE	kA
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
rozdělen	rozdělit	k5eAaPmNgInS
do	do	k7c2
sedmi	sedm	k4xCc2
následujících	následující	k2eAgInPc2d1
kroků	krok	k1gInPc2
<g/>
:	:	kIx,
</s>
<s>
Zajištění	zajištění	k1gNnSc1
sponzorů	sponzor	k1gMnPc2
<g/>
:	:	kIx,
Organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
schválená	schválený	k2eAgFnSc1d1
institutem	institut	k1gInSc7
IEEE	IEEE	kA
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
sponzorovat	sponzorovat	k5eAaImF
standard	standard	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sponzorující	sponzorující	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c4
starost	starost	k1gFnSc4
koordinaci	koordinace	k1gFnSc4
a	a	k8xC
dohled	dohled	k1gInSc4
nad	nad	k7c7
vývojem	vývoj	k1gInSc7
standardu	standard	k1gInSc2
od	od	k7c2
začátku	začátek	k1gInSc2
do	do	k7c2
konce	konec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesionální	profesionální	k2eAgFnPc4d1
společnosti	společnost	k1gFnPc4
uvnitř	uvnitř	k7c2
IEEE	IEEE	kA
slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
přirozený	přirozený	k2eAgMnSc1d1
sponzor	sponzor	k1gMnSc1
pro	pro	k7c4
mnoho	mnoho	k4c4
standardů	standard	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Požadavek	požadavek	k1gInSc1
autorizace	autorizace	k1gFnSc2
projektu	projekt	k1gInSc2
(	(	kIx(
<g/>
PAR	para	k1gFnPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
K	k	k7c3
obdržení	obdržení	k1gNnSc3
autorizace	autorizace	k1gFnSc2
pro	pro	k7c4
standard	standard	k1gInSc4
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
zažádat	zažádat	k5eAaPmF
IEEE-SA	IEEE-SA	k1gFnSc1
Standards	Standards	k1gInSc4
Board	Board	k1gInSc1
(	(	kIx(
<g/>
výbor	výbor	k1gInSc1
pro	pro	k7c4
standardy	standard	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komise	komise	k1gFnSc1
pro	pro	k7c4
nové	nový	k2eAgInPc4d1
standardy	standard	k1gInPc4
(	(	kIx(
<g/>
New	New	k1gFnSc1
Standards	Standards	k1gInSc1
Committee	Committee	k1gFnSc1
–	–	k?
NesCom	NesCom	k1gInSc1
<g/>
)	)	kIx)
z	z	k7c2
IEEE-SA	IEEE-SA	k1gFnSc2
Standard	standard	k1gInSc1
Board	Board	k1gMnSc1
posoudí	posoudit	k5eAaPmIp3nS
požadavek	požadavek	k1gInSc4
autorizace	autorizace	k1gFnSc2
projektu	projekt	k1gInSc2
(	(	kIx(
<g/>
PAR	para	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
vydá	vydat	k5eAaPmIp3nS
doporučení	doporučení	k1gNnSc4
o	o	k7c6
jejím	její	k3xOp3gNnSc6
schválení	schválení	k1gNnSc6
výboru	výbor	k1gInSc2
pro	pro	k7c4
standardy	standard	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
schválení	schválení	k1gNnSc6
požadavku	požadavek	k1gInSc2
autorizace	autorizace	k1gFnSc2
je	být	k5eAaImIp3nS
pracovní	pracovní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
sestavena	sestavit	k5eAaPmNgFnS
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
standardu	standard	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE-SA	IEEE-SA	k1gFnSc1
dohlíží	dohlížet	k5eAaImIp3nS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
všechny	všechen	k3xTgFnPc1
pracovní	pracovní	k2eAgFnPc1d1
schůzky	schůzka	k1gFnPc1
skupin	skupina	k1gFnPc2
byly	být	k5eAaImAgFnP
veřejné	veřejný	k2eAgFnPc1d1
a	a	k8xC
aby	aby	kYmCp3nP
na	na	k7c6
nich	on	k3xPp3gMnPc6
měl	mít	k5eAaImAgMnS
každý	každý	k3xTgMnSc1
právo	právo	k1gNnSc4
účasti	účast	k1gFnSc2
a	a	k8xC
příspěvku	příspěvek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Návrh	návrh	k1gInSc1
standardů	standard	k1gInPc2
<g/>
:	:	kIx,
Pracovní	pracovní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
připraví	připravit	k5eAaPmIp3nS
koncept	koncept	k1gInSc4
navrhovaného	navrhovaný	k2eAgInSc2d1
standardu	standard	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpravidla	zpravidla	k6eAd1
návrh	návrh	k1gInSc4
doprovází	doprovázet	k5eAaImIp3nS
manuál	manuál	k1gInSc1
standardizačního	standardizační	k2eAgInSc2d1
způsobu	způsob	k1gInSc2
IEEE	IEEE	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
nastaví	nastavět	k5eAaBmIp3nS,k5eAaPmIp3nS
“	“	k?
<g/>
pokyn	pokyn	k1gInSc1
<g/>
”	”	k?
pro	pro	k7c4
klauzule	klauzule	k1gFnPc4
a	a	k8xC
formát	formát	k1gInSc4
dokumentu	dokument	k1gInSc2
standardu	standard	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tajné	tajný	k2eAgNnSc1d1
hlasování	hlasování	k1gNnSc1
<g/>
:	:	kIx,
Jakmile	jakmile	k8xS
je	být	k5eAaImIp3nS
návrh	návrh	k1gInSc4
standardu	standard	k1gInSc2
v	v	k7c6
pracovní	pracovní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
dokončen	dokončen	k2eAgInSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
předložen	předložit	k5eAaPmNgInS
ke	k	k7c3
schválení	schválení	k1gNnSc3
v	v	k7c6
tajné	tajný	k2eAgFnSc6d1
volbě	volba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oddělení	oddělení	k1gNnSc1
standardů	standard	k1gInPc2
IEEE	IEEE	kA
pošle	poslat	k5eAaPmIp3nS
pozvánku	pozvánka	k1gFnSc4
k	k	k7c3
volbě	volba	k1gFnSc3
každému	každý	k3xTgMnSc3
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
vyjádřil	vyjádřit	k5eAaPmAgMnS
zájem	zájem	k1gInSc4
o	o	k7c4
věcnou	věcný	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
standardu	standard	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
požaduje	požadovat	k5eAaImIp3nS
pro	pro	k7c4
schválení	schválení	k1gNnSc4
návrhu	návrh	k1gInSc2
standardu	standard	k1gInSc2
alespoň	alespoň	k9
75	#num#	k4
<g/>
%	%	kIx~
většinu	většina	k1gFnSc4
kladných	kladný	k2eAgInPc2d1
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
návrh	návrh	k1gInSc1
neprojde	projít	k5eNaPmIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vrácen	vrátit	k5eAaPmNgInS
k	k	k7c3
přepracování	přepracování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Zhodnocení	zhodnocení	k1gNnSc1
komise	komise	k1gFnSc2
<g/>
:	:	kIx,
Po	po	k7c6
obdržení	obdržení	k1gNnSc6
75	#num#	k4
<g/>
%	%	kIx~
doporučení	doporučení	k1gNnPc2
je	být	k5eAaImIp3nS
předložen	předložit	k5eAaPmNgInS
návrh	návrh	k1gInSc1
standardu	standard	k1gInSc2
dál	daleko	k6eAd2
spolu	spolu	k6eAd1
s	s	k7c7
poznámkami	poznámka	k1gFnPc7
(	(	kIx(
<g/>
voličů	volič	k1gMnPc2
tajné	tajný	k2eAgFnSc2d1
volby	volba	k1gFnSc2
<g/>
)	)	kIx)
instituci	instituce	k1gFnSc4
IEEE-SA	IEEE-SA	k1gFnSc2
Standards	Standardsa	k1gFnPc2
Board	Board	k1gInSc1
Review	Review	k1gMnSc1
Committee	Committee	k1gInSc1
(	(	kIx(
<g/>
RevCom	RevCom	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
RevCom	RevCom	k1gInSc1
recenzují	recenzovat	k5eAaImIp3nP
navržený	navržený	k2eAgInSc4d1
koncept	koncept	k1gInSc4
standardu	standard	k1gInSc2
proti	proti	k7c3
IEEE-SA	IEEE-SA	k1gFnPc3
Standards	Standardsa	k1gFnPc2
Board	Boarda	k1gFnPc2
Bylaws	Bylaws	k1gInSc1
a	a	k8xC
dohody	dohoda	k1gFnPc1
obsažené	obsažený	k2eAgFnPc1d1
v	v	k7c6
IEEE-SA	IEEE-SA	k1gFnSc6
Standards	Standards	k1gInSc1
Board	Boarda	k1gFnPc2
Operations	Operationsa	k1gFnPc2
Manual	Manual	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
RevCom	RevCom	k1gInSc1
poté	poté	k6eAd1
podá	podat	k5eAaPmIp3nS
doporučení	doporučení	k1gNnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
schválit	schválit	k5eAaPmF
předložený	předložený	k2eAgInSc4d1
návrh	návrh	k1gInSc4
standardizačního	standardizační	k2eAgInSc2d1
dokumentu	dokument	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Finální	finální	k2eAgFnSc1d1
volba	volba	k1gFnSc1
<g/>
:	:	kIx,
Každý	každý	k3xTgMnSc1
člen	člen	k1gMnSc1
IEEE-SA	IEEE-SA	k1gMnSc1
Standards	Standardsa	k1gFnPc2
Board	Board	k1gMnSc1
umístí	umístit	k5eAaPmIp3nS
konečný	konečný	k2eAgInSc4d1
hlas	hlas	k1gInSc4
na	na	k7c4
předložený	předložený	k2eAgInSc4d1
standardizační	standardizační	k2eAgInSc4d1
dokument	dokument	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
získání	získání	k1gNnSc3
konečného	konečný	k2eAgNnSc2d1
schválení	schválení	k1gNnSc2
standardu	standard	k1gInSc2
je	být	k5eAaImIp3nS
potřeba	potřeba	k6eAd1
většina	většina	k1gFnSc1
hlasů	hlas	k1gInPc2
Standardizačního	standardizační	k2eAgInSc2d1
výboru	výbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
když	když	k8xS
RevCom	RevCom	k1gInSc1
doporučí	doporučit	k5eAaPmIp3nS
schválení	schválení	k1gNnSc4
standardu	standard	k1gInSc2
<g/>
,	,	kIx,
standardizační	standardizační	k2eAgInSc1d1
výbor	výbor	k1gInSc1
jej	on	k3xPp3gMnSc4
uzná	uznat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Institute	institut	k1gInSc5
of	of	k?
Electrical	Electrical	k1gFnSc2
and	and	k?
Electronics	Electronics	k1gInSc1
Engineers	Engineers	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
IEEE	IEEE	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
české	český	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Součásti	součást	k1gFnPc4
organizace	organizace	k1gFnSc2
IETF	IETF	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
nlk	nlk	k?
<g/>
20050163649	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1692-5	1692-5	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2106	#num#	k4
3391	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79053217	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
123926298	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79053217	#num#	k4
</s>
