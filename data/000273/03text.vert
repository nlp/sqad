<s>
Rady	rada	k1gFnPc1	rada
ptáka	pták	k1gMnSc2	pták
Loskutáka	loskuták	k1gMnSc2	loskuták
je	být	k5eAaImIp3nS	být
televizní	televizní	k2eAgInSc1d1	televizní
pořad	pořad	k1gInSc1	pořad
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Moderátorem	moderátor	k1gInSc7	moderátor
pořadu	pořad	k1gInSc2	pořad
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
Petr	Petr	k1gMnSc1	Petr
Rychlý	Rychlý	k1gMnSc1	Rychlý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jej	on	k3xPp3gMnSc4	on
moderovali	moderovat	k5eAaBmAgMnP	moderovat
Dalibor	Dalibor	k1gMnSc1	Dalibor
Gondík	Gondík	k1gMnSc1	Gondík
<g/>
,	,	kIx,	,
Adéla	Adéla	k1gFnSc1	Adéla
Gondíková	Gondíková	k1gFnSc1	Gondíková
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Vodochodský	vodochodský	k2eAgMnSc1d1	vodochodský
a	a	k8xC	a
Stanislav	Stanislav	k1gMnSc1	Stanislav
Berkovec	Berkovec	k1gMnSc1	Berkovec
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc1	pořad
probírá	probírat	k5eAaImIp3nS	probírat
témata	téma	k1gNnPc4	téma
o	o	k7c6	o
chovatelství	chovatelství	k1gNnSc6	chovatelství
<g/>
,	,	kIx,	,
kutilství	kutilství	k1gNnSc6	kutilství
<g/>
,	,	kIx,	,
stavbě	stavba	k1gFnSc6	stavba
<g/>
,	,	kIx,	,
receptech	recept	k1gInPc6	recept
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
tématech	téma	k1gNnPc6	téma
<g/>
.	.	kIx.	.
</s>
<s>
Stavění	stavění	k1gNnSc1	stavění
velkého	velký	k2eAgNnSc2d1	velké
vlakového	vlakový	k2eAgNnSc2d1	vlakové
kolejiště	kolejiště	k1gNnSc2	kolejiště
Stavění	stavění	k1gNnSc2	stavění
skleníku	skleník	k1gInSc2	skleník
Stavba	stavba	k1gFnSc1	stavba
a	a	k8xC	a
úpravy	úprava	k1gFnPc1	úprava
rodinného	rodinný	k2eAgInSc2d1	rodinný
domu	dům	k1gInSc2	dům
Úpravy	úprava	k1gFnSc2	úprava
a	a	k8xC	a
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
bytu	byt	k1gInSc2	byt
1	[number]	k4	1
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
-	-	kIx~	-
2014	[number]	k4	2014
Dalibor	Dalibor	k1gMnSc1	Dalibor
Gondík	Gondík	k1gMnSc1	Gondík
<g/>
,	,	kIx,	,
Adéla	Adéla	k1gFnSc1	Adéla
Gondíková	Gondíková	k1gFnSc1	Gondíková
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
Vodochodský	vodochodský	k2eAgMnSc1d1	vodochodský
a	a	k8xC	a
Stanislav	Stanislav	k1gMnSc1	Stanislav
Berkovec	Berkovec	k1gMnSc1	Berkovec
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
-	-	kIx~	-
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
Petr	Petr	k1gMnSc1	Petr
Rychlý	Rychlý	k1gMnSc1	Rychlý
</s>
