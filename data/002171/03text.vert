<s>
Juneau	Juneau	k6eAd1	Juneau
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Aljaška	Aljaška	k1gFnSc1	Aljaška
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
státy	stát	k1gInPc1	stát
Rhode	Rhodos	k1gInSc5	Rhodos
Island	Island	k1gInSc1	Island
a	a	k8xC	a
Delaware	Delawar	k1gMnSc5	Delawar
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
tak	tak	k6eAd1	tak
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
oba	dva	k4xCgInPc4	dva
tyto	tento	k3xDgInPc4	tento
státy	stát	k1gInPc4	stát
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
podle	podle	k7c2	podle
prospektora	prospektor	k1gMnSc2	prospektor
Joa	Joa	k1gMnSc2	Joa
Juneaua	Juneauus	k1gMnSc2	Juneauus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
městě	město	k1gNnSc6	město
hledal	hledat	k5eAaImAgInS	hledat
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
tímto	tento	k3xDgInSc7	tento
se	se	k3xPyFc4	se
městu	město	k1gNnSc3	město
říkalo	říkat	k5eAaImAgNnS	říkat
Rockwell	Rockwell	k1gInSc4	Rockwell
nebo	nebo	k8xC	nebo
Harrisburg	Harrisburg	k1gInSc4	Harrisburg
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
přibližně	přibližně	k6eAd1	přibližně
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
města	město	k1gNnSc2	město
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
dvěma	dva	k4xCgMnPc7	dva
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
založili	založit	k5eAaPmAgMnP	založit
důlní	důlní	k2eAgNnPc4d1	důlní
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
z	z	k7c2	z
dočasného	dočasný	k2eAgInSc2d1	dočasný
tábora	tábor	k1gInSc2	tábor
stalo	stát	k5eAaPmAgNnS	stát
první	první	k4xOgNnSc1	první
město	město	k1gNnSc1	město
založené	založený	k2eAgNnSc1d1	založené
po	po	k7c6	po
koupi	koupě	k1gFnSc6	koupě
Aljašky	Aljaška	k1gFnSc2	Aljaška
Američany	Američan	k1gMnPc4	Američan
od	od	k7c2	od
Rusů	Rus	k1gMnPc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
31	[number]	k4	31
275	[number]	k4	275
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
69,7	[number]	k4	69,7
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
0,9	[number]	k4	0,9
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
11,8	[number]	k4	11,8
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
6,1	[number]	k4	6,1
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
1,2	[number]	k4	1,2
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
9,5	[number]	k4	9,5
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
5,1	[number]	k4	5,1
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Whitehorse	Whitehorse	k1gFnSc1	Whitehorse
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Chiayi	Chiayi	k1gNnSc2	Chiayi
<g/>
,	,	kIx,	,
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
Vladivostok	Vladivostok	k1gInSc1	Vladivostok
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Mishan	Mishana	k1gFnPc2	Mishana
<g/>
,	,	kIx,	,
Heilongjiang	Heilongjianga	k1gFnPc2	Heilongjianga
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Juneau	Juneaus	k1gInSc2	Juneaus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
