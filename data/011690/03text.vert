<p>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
špalda	špalda	k1gFnSc1	špalda
či	či	k8xC	či
grünkern	grünkern	k1gInSc1	grünkern
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
zelená	zelený	k2eAgNnPc1d1	zelené
zrna	zrno	k1gNnPc1	zrno
či	či	k8xC	či
zelený	zelený	k2eAgInSc1d1	zelený
kaviár	kaviár	k1gInSc1	kaviár
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
špalda	špalda	k1gFnSc1	špalda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
sklizena	sklidit	k5eAaPmNgFnS	sklidit
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
dozráním	dozrání	k1gNnSc7	dozrání
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
voskové	voskový	k2eAgFnSc2d1	vosková
zralosti	zralost	k1gFnSc2	zralost
zrn	zrno	k1gNnPc2	zrno
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
uměle	uměle	k6eAd1	uměle
usušena	usušen	k2eAgFnSc1d1	usušena
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
podle	podle	k7c2	podle
zelené	zelený	k2eAgFnSc2d1	zelená
barvy	barva	k1gFnSc2	barva
zrn	zrno	k1gNnPc2	zrno
dostal	dostat	k5eAaPmAgMnS	dostat
grünkern	grünkern	k1gMnSc1	grünkern
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Sklizené	sklizený	k2eAgNnSc1d1	sklizené
zelené	zelený	k2eAgNnSc1d1	zelené
zrní	zrní	k1gNnSc1	zrní
se	se	k3xPyFc4	se
suší	sušit	k5eAaImIp3nS	sušit
asi	asi	k9	asi
při	při	k7c6	při
120	[number]	k4	120
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vzniká	vznikat	k5eAaImIp3nS	vznikat
ono	onen	k3xDgNnSc1	onen
nezaměnitelné	zaměnitelný	k2eNgNnSc1d1	nezaměnitelné
aroma	aroma	k1gNnSc1	aroma
zelené	zelený	k2eAgFnSc2d1	zelená
špaldy	špalda	k1gFnSc2	špalda
<g/>
.	.	kIx.	.
</s>
<s>
Špalda	špalda	k1gFnSc1	špalda
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
zejména	zejména	k9	zejména
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
Boxberg	Boxberg	k1gInSc1	Boxberg
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
100	[number]	k4	100
gramů	gram	k1gInPc2	gram
zrn	zrno	k1gNnPc2	zrno
zelené	zelený	k2eAgFnSc2d1	zelená
špaldy	špalda	k1gFnSc2	špalda
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
průměrně	průměrně	k6eAd1	průměrně
11,6	[number]	k4	11,6
%	%	kIx~	%
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
63,2	[number]	k4	63,2
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
2,7	[number]	k4	2,7
%	%	kIx~	%
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
8,8	[number]	k4	8,8
%	%	kIx~	%
vlákniny	vláknina	k1gFnPc4	vláknina
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
především	především	k9	především
fosfor	fosfor	k1gInSc1	fosfor
(	(	kIx(	(
<g/>
0,4	[number]	k4	0,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
(	(	kIx(	(
<g/>
0,13	[number]	k4	0,13
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
(	(	kIx(	(
<g/>
0,45	[number]	k4	0,45
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
(	(	kIx(	(
<g/>
0,004	[number]	k4	0,004
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
sodík	sodík	k1gInSc1	sodík
(	(	kIx(	(
<g/>
asi	asi	k9	asi
0,003	[number]	k4	0,003
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c4	o
grünkernu	grünkerna	k1gFnSc4	grünkerna
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1660	[number]	k4	1660
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
panoval	panovat	k5eAaImAgInS	panovat
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
hladomor	hladomor	k1gInSc1	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
silným	silný	k2eAgFnPc3d1	silná
bouřkám	bouřka	k1gFnPc3	bouřka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
často	často	k6eAd1	často
doprovázelo	doprovázet	k5eAaImAgNnS	doprovázet
krupobití	krupobití	k1gNnSc3	krupobití
<g/>
,	,	kIx,	,
sklízeli	sklízet	k5eAaImAgMnP	sklízet
rolníci	rolník	k1gMnPc1	rolník
pšenici	pšenice	k1gFnSc4	pšenice
raději	rád	k6eAd2	rád
nezralou	zralý	k2eNgFnSc4d1	nezralá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
zachovali	zachovat	k5eAaPmAgMnP	zachovat
alespoň	alespoň	k9	alespoň
část	část	k1gFnSc4	část
úrody	úroda	k1gFnSc2	úroda
<g/>
.	.	kIx.	.
</s>
<s>
Nezralé	zralý	k2eNgNnSc4d1	nezralé
zrno	zrno	k1gNnSc4	zrno
pak	pak	k6eAd1	pak
sušili	sušit	k5eAaImAgMnP	sušit
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
prodloužila	prodloužit	k5eAaPmAgFnS	prodloužit
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yQgFnSc4	který
je	on	k3xPp3gFnPc4	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
skladovat	skladovat	k5eAaImF	skladovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
grünkern	grünkern	k1gInSc1	grünkern
sušil	sušit	k5eAaImAgInS	sušit
jen	jen	k9	jen
pro	pro	k7c4	pro
domácí	domácí	k2eAgFnSc4d1	domácí
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
zelená	zelený	k2eAgFnSc1d1	zelená
špalda	špalda	k1gFnSc1	špalda
využívat	využívat	k5eAaImF	využívat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
,	,	kIx,	,
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
například	například	k6eAd1	například
slad	slad	k1gInSc4	slad
pro	pro	k7c4	pro
pivovary	pivovar	k1gInPc4	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
začala	začít	k5eAaPmAgFnS	začít
výroba	výroba	k1gFnSc1	výroba
tohoto	tento	k3xDgNnSc2	tento
zeleného	zelený	k2eAgNnSc2d1	zelené
jádra	jádro	k1gNnSc2	jádro
vzrůstat	vzrůstat	k5eAaImF	vzrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Rozkvět	rozkvět	k1gInSc4	rozkvět
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
zejména	zejména	k9	zejména
v	v	k7c6	v
období	období	k1gNnSc6	období
obou	dva	k4xCgFnPc2	dva
světových	světový	k2eAgFnPc2d1	světová
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
produkce	produkce	k1gFnSc1	produkce
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
až	až	k9	až
na	na	k7c4	na
6000	[number]	k4	6000
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
způsob	způsob	k1gInSc1	způsob
přípravy	příprava	k1gFnSc2	příprava
zelené	zelený	k2eAgFnSc2d1	zelená
špaldy	špalda	k1gFnSc2	špalda
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
asi	asi	k9	asi
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nezralá	zralý	k2eNgFnSc1d1	nezralá
špalda	špalda	k1gFnSc1	špalda
se	se	k3xPyFc4	se
posekala	posekat	k5eAaPmAgFnS	posekat
<g/>
,	,	kIx,	,
stonky	stonka	k1gFnPc1	stonka
se	se	k3xPyFc4	se
svázaly	svázat	k5eAaPmAgFnP	svázat
do	do	k7c2	do
svazků	svazek	k1gInPc2	svazek
<g/>
,	,	kIx,	,
od	od	k7c2	od
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
železných	železný	k2eAgInPc2d1	železný
hřebenů	hřeben	k1gInPc2	hřeben
oddělovaly	oddělovat	k5eAaImAgFnP	oddělovat
klasy	klasa	k1gFnPc1	klasa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
sušily	sušit	k5eAaImAgFnP	sušit
v	v	k7c6	v
sušárnách	sušárna	k1gFnPc6	sušárna
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
umístěných	umístěný	k2eAgInPc2d1	umístěný
kvůli	kvůli	k7c3	kvůli
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
požárů	požár	k1gInPc2	požár
mimo	mimo	k7c4	mimo
města	město	k1gNnPc4	město
či	či	k8xC	či
obce	obec	k1gFnPc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc4d1	centrální
část	část	k1gFnSc4	část
sušárny	sušárna	k1gFnSc2	sušárna
zabíral	zabírat	k5eAaImAgInS	zabírat
rošt	rošt	k1gInSc1	rošt
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
topilo	topit	k5eAaImAgNnS	topit
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
se	se	k3xPyFc4	se
rozložily	rozložit	k5eAaPmAgFnP	rozložit
klasy	klasa	k1gFnPc1	klasa
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
sušily	sušit	k5eAaImAgFnP	sušit
se	se	k3xPyFc4	se
a	a	k8xC	a
pražily	pražit	k5eAaImAgFnP	pražit
za	za	k7c2	za
teploty	teplota	k1gFnSc2	teplota
120	[number]	k4	120
<g/>
–	–	k?	–
<g/>
180	[number]	k4	180
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
byl	být	k5eAaImAgInS	být
obsah	obsah	k1gInSc1	obsah
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
zrnech	zrno	k1gNnPc6	zrno
snížen	snížit	k5eAaPmNgInS	snížit
až	až	k9	až
na	na	k7c4	na
13	[number]	k4	13
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Špaldová	špaldový	k2eAgNnPc4d1	špaldové
zrna	zrno	k1gNnPc4	zrno
získala	získat	k5eAaPmAgFnS	získat
pražením	pražení	k1gNnPc3	pražení
typickou	typický	k2eAgFnSc4d1	typická
chuť	chuť	k1gFnSc4	chuť
a	a	k8xC	a
vůni	vůně	k1gFnSc4	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
použití	použití	k1gNnSc4	použití
se	se	k3xPyFc4	se
zrna	zrno	k1gNnSc2	zrno
musela	muset	k5eAaImAgFnS	muset
zbavit	zbavit	k5eAaPmF	zbavit
slupek	slupka	k1gFnPc2	slupka
a	a	k8xC	a
případně	případně	k6eAd1	případně
semlít	semlít	k5eAaPmF	semlít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Grünkern	Grünkerno	k1gNnPc2	Grünkerno
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Grünkern	Grünkern	k1gInSc4	Grünkern
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
