<s>
Rozkvět	rozkvět	k1gInSc4	rozkvět
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
zejména	zejména	k9	zejména
v	v	k7c6	v
období	období	k1gNnSc6	období
obou	dva	k4xCgFnPc2	dva
světových	světový	k2eAgFnPc2d1	světová
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
produkce	produkce	k1gFnSc1	produkce
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
až	až	k9	až
na	na	k7c4	na
6000	[number]	k4	6000
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
