<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Zvole	Zvole	k1gFnSc2	Zvole
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1468	[number]	k4	1468
poblíž	poblíž	k7c2	poblíž
obce	obec	k1gFnSc2	obec
Zvole	Zvole	k1gFnSc2	Zvole
přesněji	přesně	k6eAd2	přesně
mezi	mezi	k7c7	mezi
obcemi	obec	k1gFnPc7	obec
Rájec	Rájec	k1gInSc1	Rájec
a	a	k8xC	a
Zvole	Zvole	k1gInSc1	Zvole
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
JV	JV	kA	JV
od	od	k7c2	od
Zábřeha	Zábřeh	k1gInSc2	Zábřeh
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
vojsko	vojsko	k1gNnSc1	vojsko
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
bylo	být	k5eAaImAgNnS	být
poraženo	porazit	k5eAaPmNgNnS	porazit
uherským	uherský	k2eAgNnSc7d1	Uherské
vojskem	vojsko	k1gNnSc7	vojsko
tzv.	tzv.	kA	tzv.
černou	černý	k2eAgFnSc7d1	černá
rotou	rota	k1gFnSc7	rota
Matyáše	Matyáš	k1gMnSc2	Matyáš
Korvína	Korvín	k1gInSc2	Korvín
vedenou	vedený	k2eAgFnSc4d1	vedená
hejtmanem	hejtman	k1gMnSc7	hejtman
Františkem	František	k1gMnSc7	František
z	z	k7c2	z
Háje	háj	k1gInSc2	háj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
kontext	kontext	k1gInSc1	kontext
==	==	k?	==
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
svěřil	svěřit	k5eAaPmAgInS	svěřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1464	[number]	k4	1464
hrad	hrad	k1gInSc4	hrad
Bouzov	Bouzov	k1gInSc4	Bouzov
s	s	k7c7	s
panstvím	panství	k1gNnSc7	panství
svému	svůj	k3xOyFgMnSc3	svůj
věrnému	věrný	k2eAgMnSc3d1	věrný
stoupenci	stoupenec	k1gMnSc3	stoupenec
Zdeňku	Zdeněk	k1gMnSc3	Zdeněk
Kostkovi	Kostka	k1gMnSc3	Kostka
z	z	k7c2	z
Postupic	Postupice	k1gFnPc2	Postupice
a	a	k8xC	a
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
jej	on	k3xPp3gMnSc4	on
velitelem	velitel	k1gMnSc7	velitel
svých	svůj	k3xOyFgNnPc2	svůj
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
i	i	k9	i
blízkou	blízký	k2eAgFnSc7d1	blízká
Moravskou	moravský	k2eAgFnSc7d1	Moravská
Třebovou	Třebová	k1gFnSc7	Třebová
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
hejtmanem	hejtman	k1gMnSc7	hejtman
chrudimského	chrudimský	k2eAgInSc2d1	chrudimský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
nato	nato	k6eAd1	nato
<g/>
,	,	kIx,	,
za	za	k7c2	za
česko-uherských	českoherský	k2eAgFnPc2d1	česko-uherská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
bouzovské	bouzovský	k2eAgNnSc1d1	bouzovský
panství	panství	k1gNnSc1	panství
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
Moravy	Morava	k1gFnSc2	Morava
popleněno	poplenit	k5eAaPmNgNnS	poplenit
uherskými	uherský	k2eAgNnPc7d1	Uherské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Rokem	rok	k1gInSc7	rok
1468	[number]	k4	1468
začíná	začínat	k5eAaImIp3nS	začínat
období	období	k1gNnSc4	období
pro	pro	k7c4	pro
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
Slezsko	Slezsko	k1gNnSc1	Slezsko
poměrně	poměrně	k6eAd1	poměrně
tragické	tragický	k2eAgNnSc1d1	tragické
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
1468	[number]	k4	1468
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
českému	český	k2eAgMnSc3d1	český
králi	král	k1gMnSc3	král
Jiřímu	Jiří	k1gMnSc3	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
válku	válek	k1gInSc2	válek
<g/>
.	.	kIx.	.
</s>
<s>
Vojevůdcem	vojevůdce	k1gMnSc7	vojevůdce
uherských	uherský	k2eAgInPc2d1	uherský
vojsk	vojsko	k1gNnPc2	vojsko
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Korvín	Korvín	k1gInSc4	Korvín
hejtmana	hejtman	k1gMnSc2	hejtman
France	Franc	k1gMnSc2	Franc
z	z	k7c2	z
Háje	háj	k1gInSc2	háj
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
vojskem	vojsko	k1gNnSc7	vojsko
i	i	k8xC	i
lidem	lid	k1gInSc7	lid
zvaný	zvaný	k2eAgMnSc1d1	zvaný
"	"	kIx"	"
<g/>
Strašný	strašný	k2eAgMnSc1d1	strašný
Franc	Franc	k1gMnSc1	Franc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Čech	Čech	k1gMnSc1	Čech
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
Bratří	bratřit	k5eAaImIp3nS	bratřit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
června	červen	k1gInSc2	červen
r.	r.	kA	r.
1468	[number]	k4	1468
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
k	k	k7c3	k
Olomouci	Olomouc	k1gFnSc3	Olomouc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
Hradisku	hradisko	k1gNnSc6	hradisko
oblehl	oblehnout	k5eAaPmAgInS	oblehnout
umístěnou	umístěný	k2eAgFnSc4d1	umístěná
českou	český	k2eAgFnSc4d1	Česká
posádku	posádka	k1gFnSc4	posádka
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Vaňka	Vaněk	k1gMnSc2	Vaněk
Šatného	šatný	k2eAgMnSc2d1	šatný
<g/>
.	.	kIx.	.
</s>
<s>
Dvouměsíčním	dvouměsíční	k2eAgNnSc7d1	dvouměsíční
obléháním	obléhání	k1gNnSc7	obléhání
hrozilo	hrozit	k5eAaImAgNnS	hrozit
posádce	posádka	k1gFnSc3	posádka
vyhladovění	vyhladovění	k1gNnSc6	vyhladovění
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strategického	strategický	k2eAgNnSc2d1	strategické
hlediska	hledisko	k1gNnSc2	hledisko
velmi	velmi	k6eAd1	velmi
záleželo	záležet	k5eAaImAgNnS	záležet
králi	král	k1gMnSc3	král
Jiřímu	Jiří	k1gMnSc3	Jiří
na	na	k7c6	na
udržení	udržení	k1gNnSc6	udržení
této	tento	k3xDgFnSc2	tento
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
obležené	obležený	k2eAgMnPc4d1	obležený
vysvobodit	vysvobodit	k5eAaPmF	vysvobodit
a	a	k8xC	a
doplnit	doplnit	k5eAaPmF	doplnit
jejich	jejich	k3xOp3gFnPc4	jejich
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
bitvy	bitva	k1gFnSc2	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
Konvoj	konvoj	k1gInSc1	konvoj
záchranné	záchranný	k2eAgFnSc2d1	záchranná
výpravy	výprava	k1gFnSc2	výprava
čítající	čítající	k2eAgFnSc1d1	čítající
přes	přes	k7c4	přes
180	[number]	k4	180
vozů	vůz	k1gInPc2	vůz
se	s	k7c7	s
spíží	spíž	k1gFnSc7	spíž
a	a	k8xC	a
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
v	v	k7c6	v
síle	síla	k1gFnSc6	síla
přes	přes	k7c4	přes
5000	[number]	k4	5000
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Kostky	Kostka	k1gMnSc2	Kostka
z	z	k7c2	z
Postupic	Postupice	k1gFnPc2	Postupice
přesouval	přesouvat	k5eAaImAgInS	přesouvat
od	od	k7c2	od
Litomyšle	Litomyšl	k1gFnSc2	Litomyšl
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
Moravské	moravský	k2eAgFnSc2d1	Moravská
Sázavy	Sázava	k1gFnSc2	Sázava
k	k	k7c3	k
Zábřehu	Zábřeh	k1gInSc3	Zábřeh
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
září	září	k1gNnSc2	září
dospěla	dochvít	k5eAaPmAgFnS	dochvít
výprava	výprava	k1gFnSc1	výprava
až	až	k9	až
do	do	k7c2	do
Zábřeha	Zábřeh	k1gInSc2	Zábřeh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgNnSc7	svůj
vojskem	vojsko	k1gNnSc7	vojsko
připojili	připojit	k5eAaPmAgMnP	připojit
bratři	bratr	k1gMnPc1	bratr
Tunklové	Tunklová	k1gFnSc2	Tunklová
–	–	k?	–
Jiří	Jiří	k1gMnSc1	Jiří
Tunkl	Tunkl	k1gInSc1	Tunkl
ze	z	k7c2	z
Zábřeha	Zábřeh	k1gInSc2	Zábřeh
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Tunkl	Tunkl	k1gInSc4	Tunkl
z	z	k7c2	z
Brníčka	Brníček	k1gInSc2	Brníček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
Jiřím	Jiří	k1gMnSc7	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
a	a	k8xC	a
Matyášem	Matyáš	k1gMnSc7	Matyáš
Korvínem	Korvín	k1gMnSc7	Korvín
<g/>
,	,	kIx,	,
stáli	stát	k5eAaImAgMnP	stát
oba	dva	k4xCgMnPc1	dva
Tunklové	Tunkl	k1gMnPc1	Tunkl
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Jiřího	Jiří	k1gMnSc2	Jiří
a	a	k8xC	a
zapletli	zaplést	k5eAaPmAgMnP	zaplést
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
sporů	spor	k1gInPc2	spor
s	s	k7c7	s
pány	pan	k1gMnPc7	pan
ze	z	k7c2	z
Zvole	Zvole	k1gNnSc2	Zvole
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
podporovali	podporovat	k5eAaImAgMnP	podporovat
Korvína	Korvín	k1gMnSc4	Korvín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
Zábřeha	Zábřeh	k1gInSc2	Zábřeh
měla	mít	k5eAaImAgFnS	mít
výprava	výprava	k1gFnSc1	výprava
namířeno	namířen	k2eAgNnSc4d1	namířeno
již	již	k6eAd1	již
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
Olomouci	Olomouc	k1gFnSc3	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kostka	Kostka	k1gMnSc1	Kostka
však	však	k9	však
pro	pro	k7c4	pro
pokračování	pokračování	k1gNnSc4	pokračování
výpravy	výprava	k1gFnSc2	výprava
stál	stát	k5eAaImAgMnS	stát
před	před	k7c7	před
dilematem	dilema	k1gNnSc7	dilema
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
volit	volit	k5eAaImF	volit
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
variant	varianta	k1gFnPc2	varianta
pokračování	pokračování	k1gNnSc2	pokračování
cesty	cesta	k1gFnPc4	cesta
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
byly	být	k5eAaImAgFnP	být
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
volil	volit	k5eAaImAgMnS	volit
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
u	u	k7c2	u
vsi	ves	k1gFnSc2	ves
Leština	Leština	k1gFnSc1	Leština
výprava	výprava	k1gFnSc1	výprava
složitě	složitě	k6eAd1	složitě
přebrodit	přebrodit	k5eAaPmF	přebrodit
nebezpečně	bezpečně	k6eNd1	bezpečně
rozvodněnou	rozvodněný	k2eAgFnSc4d1	rozvodněná
řeku	řeka	k1gFnSc4	řeka
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
probíjet	probíjet	k5eAaImF	probíjet
územím	území	k1gNnSc7	území
drženým	držený	k2eAgMnPc3d1	držený
a	a	k8xC	a
ovládaným	ovládaný	k2eAgMnSc7d1	ovládaný
úsovským	úsovský	k2eAgMnSc7d1	úsovský
pánem	pán	k1gMnSc7	pán
Karlem	Karel	k1gMnSc7	Karel
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
<g/>
,	,	kIx,	,
přívržencem	přívrženec	k1gMnSc7	přívrženec
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
padlo	padnout	k5eAaImAgNnS	padnout
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Kostkou	Kostka	k1gMnSc7	Kostka
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
pro	pro	k7c4	pro
postup	postup	k1gInSc4	postup
po	po	k7c6	po
pravém	pravý	k2eAgInSc6d1	pravý
širším	široký	k2eAgInSc6d2	širší
a	a	k8xC	a
schůdnějším	schůdný	k2eAgInSc6d2	schůdnější
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zde	zde	k6eAd1	zde
číhalo	číhat	k5eAaImAgNnS	číhat
trojí	trojí	k4xRgNnSc1	trojí
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
překážkou	překážka	k1gFnSc7	překážka
byla	být	k5eAaImAgFnS	být
tvrz	tvrz	k1gFnSc1	tvrz
v	v	k7c6	v
Rájci	Rájec	k1gInSc6	Rájec
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
a	a	k8xC	a
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
biskupského	biskupský	k2eAgMnSc2d1	biskupský
mana	man	k1gMnSc2	man
Jana	Jan	k1gMnSc2	Jan
Rájeckého	Rájecký	k2eAgMnSc2d1	Rájecký
z	z	k7c2	z
Mírova	Mírův	k2eAgInSc2d1	Mírův
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
a	a	k8xC	a
překážkou	překážka	k1gFnSc7	překážka
byla	být	k5eAaImAgFnS	být
poměrně	poměrně	k6eAd1	poměrně
pevná	pevný	k2eAgFnSc1d1	pevná
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
vyzbrojená	vyzbrojený	k2eAgFnSc1d1	vyzbrojená
tvrz	tvrz	k1gFnSc1	tvrz
ve	v	k7c6	v
Zvoli	Zvole	k1gFnSc6	Zvole
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
posádce	posádka	k1gFnSc3	posádka
velel	velet	k5eAaImAgMnS	velet
další	další	k2eAgMnSc1d1	další
biskupský	biskupský	k2eAgMnSc1d1	biskupský
man	man	k1gMnSc1	man
–	–	k?	–
rytíř	rytíř	k1gMnSc1	rytíř
Zikmund	Zikmund	k1gMnSc1	Zikmund
ze	z	k7c2	z
Zvole	Zvole	k1gFnSc2	Zvole
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
třetí	třetí	k4xOgInPc4	třetí
a	a	k8xC	a
také	také	k9	také
největší	veliký	k2eAgFnSc7d3	veliký
komplikací	komplikace	k1gFnSc7	komplikace
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
stojící	stojící	k2eAgNnSc1d1	stojící
biskupské	biskupský	k2eAgNnSc1d1	Biskupské
město	město	k1gNnSc1	město
Mohelnice	Mohelnice	k1gFnSc2	Mohelnice
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
opevněné	opevněný	k2eAgFnPc1d1	opevněná
a	a	k8xC	a
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
blízkého	blízký	k2eAgNnSc2d1	blízké
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
silně	silně	k6eAd1	silně
opevněného	opevněný	k2eAgInSc2d1	opevněný
hradu	hrad	k1gInSc2	hrad
Mírova	Mírův	k2eAgInSc2d1	Mírův
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
početnou	početný	k2eAgFnSc7d1	početná
dobře	dobře	k6eAd1	dobře
vyzbrojeno	vyzbrojen	k2eAgNnSc1d1	vyzbrojeno
a	a	k8xC	a
vycvičenou	vycvičený	k2eAgFnSc7d1	vycvičená
posádkou	posádka	k1gFnSc7	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
přívrženci	přívrženec	k1gMnPc7	přívrženec
krále	král	k1gMnSc2	král
Matyáše	Matyáš	k1gMnSc2	Matyáš
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tato	tento	k3xDgNnPc4	tento
rizika	riziko	k1gNnPc4	riziko
se	se	k3xPyFc4	se
České	český	k2eAgNnSc1d1	české
vojsko	vojsko	k1gNnSc1	vojsko
vydalo	vydat	k5eAaPmAgNnS	vydat
cestou	cesta	k1gFnSc7	cesta
po	po	k7c6	po
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kostka	Kostka	k1gMnSc1	Kostka
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
a	a	k8xC	a
očekával	očekávat	k5eAaImAgMnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
v	v	k7c4	v
ústrety	ústreta	k1gFnPc4	ústreta
přijede	přijet	k5eAaPmIp3nS	přijet
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
posádka	posádka	k1gFnSc1	posádka
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
Bouzova	Bouzův	k2eAgNnSc2d1	Bouzův
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zpevnění	zpevnění	k1gNnSc3	zpevnění
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
posílení	posílení	k1gNnSc2	posílení
mužstva	mužstvo	k1gNnSc2	mužstvo
vyslala	vyslat	k5eAaPmAgFnS	vyslat
mírovská	mírovský	k2eAgFnSc1d1	Mírovská
posádka	posádka	k1gFnSc1	posádka
značný	značný	k2eAgInSc1d1	značný
počet	počet	k1gInSc1	počet
vojska	vojsko	k1gNnSc2	vojsko
na	na	k7c4	na
tvrze	tvrz	k1gFnPc4	tvrz
v	v	k7c6	v
Rájci	Rájec	k1gInSc6	Rájec
a	a	k8xC	a
ve	v	k7c6	v
Zvoli	Zvole	k1gFnSc6	Zvole
<g/>
.	.	kIx.	.
</s>
<s>
Rájecká	Rájecký	k2eAgFnSc1d1	Rájecká
posádka	posádka	k1gFnSc1	posádka
však	však	k9	však
neprovokovala	provokovat	k5eNaImAgFnS	provokovat
a	a	k8xC	a
nevyvolávala	vyvolávat	k5eNaImAgFnS	vyvolávat
střety	střet	k1gInPc4	střet
a	a	k8xC	a
volně	volně	k6eAd1	volně
ponechala	ponechat	k5eAaPmAgFnS	ponechat
české	český	k2eAgNnSc4d1	české
vojsko	vojsko	k1gNnSc4	vojsko
bez	bez	k7c2	bez
napadení	napadení	k1gNnSc2	napadení
projít	projít	k5eAaPmF	projít
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Zvolí	zvolit	k5eAaPmIp3nS	zvolit
se	se	k3xPyFc4	se
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kostka	Kostka	k1gMnSc1	Kostka
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
tuto	tento	k3xDgFnSc4	tento
obejít	obejít	k5eAaPmF	obejít
a	a	k8xC	a
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
tak	tak	k8xC	tak
dalšímu	další	k2eAgInSc3d1	další
možnému	možný	k2eAgInSc3d1	možný
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
tedy	tedy	k9	tedy
odbočila	odbočit	k5eAaPmAgFnS	odbočit
vpravo	vpravo	k6eAd1	vpravo
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
staré	starý	k2eAgFnSc3d1	stará
cestě	cesta	k1gFnSc3	cesta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
však	však	k9	však
střetla	střetnout	k5eAaPmAgFnS	střetnout
s	s	k7c7	s
překážkou	překážka	k1gFnSc7	překážka
–	–	k?	–
hluboký	hluboký	k2eAgInSc1d1	hluboký
úvoz	úvoz	k1gInSc1	úvoz
cesty	cesta	k1gFnSc2	cesta
nad	nad	k7c7	nad
vsí	ves	k1gFnSc7	ves
byl	být	k5eAaImAgMnS	být
zatarasen	zatarasit	k5eAaPmNgMnS	zatarasit
zásekem	zásek	k1gInSc7	zásek
<g/>
.	.	kIx.	.
</s>
<s>
Postupovat	postupovat	k5eAaImF	postupovat
bylo	být	k5eAaImAgNnS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
cesty	cesta	k1gFnPc1	cesta
byly	být	k5eAaImAgFnP	být
připraveny	připravit	k5eAaPmNgFnP	připravit
protivníkovy	protivníkův	k2eAgFnPc1d1	protivníkova
zálohy	záloha	k1gFnPc1	záloha
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
proto	proto	k8xC	proto
jiného	jiný	k2eAgNnSc2d1	jiné
řešení	řešení	k1gNnSc2	řešení
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
před	před	k7c4	před
osadu	osada	k1gFnSc4	osada
Zvoli	Zvole	k1gFnSc4	Zvole
<g/>
,	,	kIx,	,
vybudovat	vybudovat	k5eAaPmF	vybudovat
tam	tam	k6eAd1	tam
ležení	ležení	k1gNnSc4	ležení
<g/>
,	,	kIx,	,
tábor	tábor	k1gInSc4	tábor
opevnit	opevnit	k5eAaPmF	opevnit
z	z	k7c2	z
vozové	vozový	k2eAgFnSc2d1	vozová
hradby	hradba	k1gFnSc2	hradba
a	a	k8xC	a
vyčkat	vyčkat	k5eAaPmF	vyčkat
příhodné	příhodný	k2eAgFnSc2d1	příhodná
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
momentu	moment	k1gInSc2	moment
překvapení	překvapení	k1gNnSc2	překvapení
při	při	k7c6	při
prudkém	prudký	k2eAgInSc6d1	prudký
výpadu	výpad	k1gInSc6	výpad
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
setmění	setmění	k1gNnSc6	setmění
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
lehká	lehký	k2eAgFnSc1d1	lehká
uherská	uherský	k2eAgFnSc1d1	uherská
jízda	jízda	k1gFnSc1	jízda
několikrát	několikrát	k6eAd1	několikrát
atakovat	atakovat	k5eAaBmF	atakovat
tábor	tábor	k1gInSc4	tábor
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bez	bez	k7c2	bez
většího	veliký	k2eAgInSc2d2	veliký
efektu	efekt	k1gInSc2	efekt
a	a	k8xC	a
škod	škoda	k1gFnPc2	škoda
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
1468	[number]	k4	1468
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
deštivý	deštivý	k2eAgInSc1d1	deštivý
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
ležení	ležení	k1gNnSc1	ležení
rozložené	rozložený	k2eAgNnSc1d1	rozložené
na	na	k7c6	na
bahnité	bahnitý	k2eAgFnSc6d1	bahnitá
rozbředlé	rozbředlý	k2eAgFnSc6d1	rozbředlá
půdě	půda	k1gFnSc6	půda
velmi	velmi	k6eAd1	velmi
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
<g/>
.	.	kIx.	.
</s>
<s>
Velení	velení	k1gNnSc1	velení
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
přemístit	přemístit	k5eAaPmF	přemístit
tábor	tábor	k1gInSc4	tábor
na	na	k7c4	na
sušší	suchý	k2eAgNnSc4d2	sušší
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
manévru	manévr	k1gInSc6	manévr
a	a	k8xC	a
pokusu	pokus	k1gInSc6	pokus
přemísťování	přemísťování	k1gNnSc2	přemísťování
provedly	provést	k5eAaPmAgFnP	provést
uherské	uherský	k2eAgFnPc1d1	uherská
setniny	setnina	k1gFnPc1	setnina
nájezdy	nájezd	k1gInPc7	nájezd
na	na	k7c4	na
vozové	vozový	k2eAgFnPc4d1	vozová
ohrady	ohrada	k1gFnPc4	ohrada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
bez	bez	k7c2	bez
valných	valný	k2eAgFnPc2d1	valná
škod	škoda	k1gFnPc2	škoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohroma	pohroma	k1gFnSc1	pohroma
a	a	k8xC	a
katastrofa	katastrofa	k1gFnSc1	katastrofa
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
úplné	úplný	k2eAgFnSc2d1	úplná
porážky	porážka	k1gFnSc2	porážka
českého	český	k2eAgNnSc2d1	české
vojska	vojsko	k1gNnSc2	vojsko
se	se	k3xPyFc4	se
však	však	k9	však
dostavila	dostavit	k5eAaPmAgFnS	dostavit
velmi	velmi	k6eAd1	velmi
záhy	záhy	k6eAd1	záhy
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Olomouce	Olomouc	k1gFnSc2	Olomouc
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
ke	k	k7c3	k
Zvoli	Zvole	k1gFnSc3	Zvole
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
uherským	uherský	k2eAgInSc7d1	uherský
vojem	voj	k1gInSc7	voj
čítajícím	čítající	k2eAgInSc7d1	čítající
bezmála	bezmála	k6eAd1	bezmála
8000	[number]	k4	8000
mužů	muž	k1gMnPc2	muž
František	Františka	k1gFnPc2	Františka
z	z	k7c2	z
Háje	háj	k1gInSc2	háj
a	a	k8xC	a
český	český	k2eAgInSc1d1	český
tábor	tábor	k1gInSc1	tábor
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
přepadl	přepadnout	k5eAaPmAgMnS	přepadnout
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
bitva	bitva	k1gFnSc1	bitva
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
vojsko	vojsko	k1gNnSc1	vojsko
mělo	mít	k5eAaImAgNnS	mít
jedinou	jediný	k2eAgFnSc4d1	jediná
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Zábřeh	Zábřeh	k1gInSc4	Zábřeh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rájecké	rájecký	k2eAgFnSc2d1	rájecká
tvrze	tvrz	k1gFnSc2	tvrz
provedla	provést	k5eAaPmAgFnS	provést
výpad	výpad	k1gInSc4	výpad
na	na	k7c4	na
ustupující	ustupující	k2eAgNnSc4d1	ustupující
vojsko	vojsko	k1gNnSc4	vojsko
silná	silný	k2eAgFnSc1d1	silná
posádka	posádka	k1gFnSc1	posádka
a	a	k8xC	a
české	český	k2eAgNnSc1d1	české
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
obklíčení	obklíčení	k1gNnSc2	obklíčení
a	a	k8xC	a
v	v	k7c6	v
nastalé	nastalý	k2eAgFnSc6d1	nastalá
kruté	krutý	k2eAgFnSc6d1	krutá
bitvě	bitva	k1gFnSc6	bitva
české	český	k2eAgNnSc1d1	české
vojsko	vojsko	k1gNnSc1	vojsko
podlehlo	podlehnout	k5eAaPmAgNnS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
celkových	celkový	k2eAgFnPc2d1	celková
ztrát	ztráta	k1gFnPc2	ztráta
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kronikách	kronika	k1gFnPc6	kronika
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
straně	strana	k1gFnSc6	strana
až	až	k9	až
600	[number]	k4	600
usmrcených	usmrcený	k2eAgFnPc2d1	usmrcená
mečem	meč	k1gInSc7	meč
a	a	k8xC	a
kopím	kopí	k1gNnSc7	kopí
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
utopených	utopený	k1gMnPc2	utopený
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Sázavě	Sázava	k1gFnSc6	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
vojska	vojsko	k1gNnSc2	vojsko
se	se	k3xPyFc4	se
probila	probít	k5eAaPmAgFnS	probít
zpět	zpět	k6eAd1	zpět
za	za	k7c4	za
hradby	hradba	k1gFnPc4	hradba
Zábřeha	Zábřeh	k1gInSc2	Zábřeh
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
těžce	těžce	k6eAd1	těžce
raněn	raněn	k2eAgInSc1d1	raněn
(	(	kIx(	(
<g/>
údajně	údajně	k6eAd1	údajně
Janem	Jan	k1gMnSc7	Jan
z	z	k7c2	z
Vrbna	Vrbna	k1gFnSc1	Vrbna
<g/>
)	)	kIx)	)
velitel	velitel	k1gMnSc1	velitel
Poděbradova	Poděbradův	k2eAgNnSc2d1	Poděbradovo
vojska	vojsko	k1gNnSc2	vojsko
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kostka	Kostka	k1gMnSc1	Kostka
z	z	k7c2	z
Postupic	Postupice	k1gFnPc2	Postupice
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
večer	večer	k6eAd1	večer
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c6	na
Tunklově	Tunklův	k2eAgNnSc6d1	Tunklův
sídle	sídlo	k1gNnSc6	sídlo
v	v	k7c6	v
Zábřeze	Zábřeh	k1gInSc6wO	Zábřeh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
dal	dát	k5eAaPmAgMnS	dát
podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gInSc4	Korvín
vypálit	vypálit	k5eAaPmF	vypálit
kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgInPc2d1	jiný
i	i	k8xC	i
hrad	hrad	k1gInSc1	hrad
Brníčko	Brníčko	k1gNnSc1	Brníčko
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
spojenec	spojenec	k1gMnSc1	spojenec
Poděbradův	Poděbradův	k2eAgMnSc1d1	Poděbradův
Jiří	Jiří	k1gMnSc1	Jiří
st.	st.	kA	st.
Tunkl	Tunkl	k1gInSc1	Tunkl
se	se	k3xPyFc4	se
raději	rád	k6eAd2	rád
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hejtman	hejtman	k1gMnSc1	hejtman
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1474	[number]	k4	1474
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Ivanovic	Ivanovice	k1gFnPc2	Ivanovice
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Matyášově	Matyášův	k2eAgFnSc6d1	Matyášova
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
udobřený	udobřený	k2eAgMnSc1d1	udobřený
král	král	k1gMnSc1	král
Matyáš	Matyáš	k1gMnSc1	Matyáš
se	se	k3xPyFc4	se
přimluvil	přimluvit	k5eAaPmAgMnS	přimluvit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
moravští	moravský	k2eAgMnPc1d1	moravský
páni	pan	k1gMnPc1	pan
přijali	přijmout	k5eAaPmAgMnP	přijmout
Jiřího	Jiří	k1gMnSc4	Jiří
Tunkla	Tunkla	k1gMnSc4	Tunkla
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
potomky	potomek	k1gMnPc7	potomek
do	do	k7c2	do
panského	panský	k2eAgInSc2d1	panský
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1480	[number]	k4	1480
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
umírá	umírat	k5eAaImIp3nS	umírat
Jiřího	Jiří	k1gMnSc4	Jiří
bratr	bratr	k1gMnSc1	bratr
Jan	Jan	k1gMnSc1	Jan
Tunkl	Tunkl	k1gInSc4	Tunkl
z	z	k7c2	z
Brníčka	Brníček	k1gInSc2	Brníček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důsledky	důsledek	k1gInPc1	důsledek
==	==	k?	==
</s>
</p>
<p>
<s>
Porážka	porážka	k1gFnSc1	porážka
českého	český	k2eAgNnSc2d1	české
vojska	vojsko	k1gNnSc2	vojsko
u	u	k7c2	u
Zvole	Zvole	k1gNnSc2	Zvole
měla	mít	k5eAaImAgFnS	mít
fatální	fatální	k2eAgInPc4d1	fatální
následky	následek	k1gInPc4	následek
pro	pro	k7c4	pro
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelská	přátelský	k2eNgNnPc1d1	nepřátelské
uherská	uherský	k2eAgNnPc1d1	Uherské
vojska	vojsko	k1gNnPc1	vojsko
ničila	ničit	k5eAaImAgFnS	ničit
hrady	hrad	k1gInPc4	hrad
<g/>
,	,	kIx,	,
tvrze	tvrz	k1gFnPc4	tvrz
a	a	k8xC	a
pustošila	pustošit	k5eAaImAgFnS	pustošit
osady	osada	k1gFnPc4	osada
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
úplně	úplně	k6eAd1	úplně
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
lokalizace	lokalizace	k1gFnSc1	lokalizace
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
současných	současný	k2eAgInPc2d1	současný
archeologických	archeologický	k2eAgInPc2d1	archeologický
výzkumů	výzkum	k1gInPc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
mnohými	mnohý	k2eAgMnPc7d1	mnohý
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1471	[number]	k4	1471
zničen	zničit	k5eAaPmNgInS	zničit
i	i	k8xC	i
hrad	hrad	k1gInSc1	hrad
Brníčko	Brníčko	k1gNnSc4	Brníčko
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1513	[number]	k4	1513
uvádí	uvádět	k5eAaImIp3nS	uvádět
již	již	k6eAd1	již
jako	jako	k9	jako
zřícenina	zřícenina	k1gFnSc1	zřícenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
marná	marný	k2eAgFnSc1d1	marná
i	i	k8xC	i
snaha	snaha	k1gFnSc1	snaha
Ctibora	Ctibor	k1gMnSc2	Ctibor
Tovačovského	Tovačovský	k1gMnSc2	Tovačovský
z	z	k7c2	z
Cimburka	Cimburek	k1gMnSc2	Cimburek
dobýt	dobýt	k5eAaPmF	dobýt
Olomouc	Olomouc	k1gFnSc4	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
citelnými	citelný	k2eAgFnPc7d1	citelná
ztrátami	ztráta	k1gFnPc7	ztráta
byl	být	k5eAaImAgInS	být
odražen	odražen	k2eAgInSc1d1	odražen
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
knížeti	kníže	k1gMnSc3	kníže
Viktorinovi	Viktorin	k1gMnSc3	Viktorin
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1468	[number]	k4	1468
dobýt	dobýt	k5eAaPmF	dobýt
Uherské	uherský	k2eAgNnSc1d1	Uherské
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
posádka	posádka	k1gFnSc1	posádka
na	na	k7c6	na
brněnském	brněnský	k2eAgInSc6d1	brněnský
Špilberku	Špilberk	k1gInSc6	Špilberk
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
po	po	k7c6	po
vyhladovění	vyhladovění	k1gNnSc6	vyhladovění
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
celá	celý	k2eAgFnSc1d1	celá
Morava	Morava	k1gFnSc1	Morava
a	a	k8xC	a
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Uherského	uherský	k2eAgNnSc2d1	Uherské
Hradiště	Hradiště	k1gNnSc2	Hradiště
<g/>
,	,	kIx,	,
Opavy	Opava	k1gFnSc2	Opava
a	a	k8xC	a
Lužice	Lužice	k1gFnSc2	Lužice
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
uherských	uherský	k2eAgFnPc6d1	uherská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
bitev	bitva	k1gFnPc2	bitva
</s>
</p>
