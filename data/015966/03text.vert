<s>
Bělověžský	Bělověžský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuBělověžský	infoboxuBělověžský	k2eAgInSc1d1
národní	národní	k2eAgFnPc4d1
parkIUCN	parkIUCN	k?
kategorie	kategorie	k1gFnPc4
II	II	kA
(	(	kIx(
<g/>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
)	)	kIx)
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Vyhlášení	vyhlášení	k1gNnSc2
</s>
<s>
1932	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
105,17	105,17	k4
km²	km²	k?
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
52	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
23	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Bělověžský	Bělověžský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc4
</s>
<s>
www.bpn.com.pl	www.bpn.com.pnout	k5eAaPmAgMnS
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Bělověžský	Bělověžský	k2eAgMnSc1d1
pralesSvětové	pralesSvět	k1gMnPc1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
Logo	logo	k1gNnSc1
polské	polský	k2eAgFnSc6d1
částiSmluvní	částiSmluvný	k2eAgMnPc5d1
stát	stát	k1gInSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
Bělorusko	Bělorusko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
Polsko	Polsko	k1gNnSc1
Typ	typ	k1gInSc1
</s>
<s>
přírodní	přírodní	k2eAgNnSc1d1
dědictví	dědictví	k1gNnSc1
Kritérium	kritérium	k1gNnSc1
</s>
<s>
ix	ix	k?
<g/>
,	,	kIx,
x	x	k?
Odkaz	odkaz	k1gInSc1
</s>
<s>
33	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zařazení	zařazení	k1gNnSc1
do	do	k7c2
seznamu	seznam	k1gInSc2
Zařazení	zařazení	k1gNnSc1
</s>
<s>
1979	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc1
<g/>
)	)	kIx)
Změny	změna	k1gFnPc1
</s>
<s>
1992	#num#	k4
–	–	k?
rozšíření	rozšíření	k1gNnSc1
<g/>
2014	#num#	k4
-	-	kIx~
rozšíření	rozšíření	k1gNnSc4
</s>
<s>
Močál	močál	k1gInSc1
v	v	k7c6
Bělověžském	Bělověžský	k2eAgInSc6d1
národním	národní	k2eAgInSc6d1
parku	park	k1gInSc6
s	s	k7c7
ostrůvky	ostrůvek	k1gInPc7
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Bělověžský	Bělověžský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
(	(	kIx(
<g/>
bělorusky	bělorusky	k6eAd1
Н	Н	k?
п	п	k?
Б	Б	k?
п	п	k?
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Białowieski	Białowieske	k1gFnSc4
Park	park	k1gInSc4
Narodowy	Narodowa	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
běloruský	běloruský	k2eAgMnSc1d1
a	a	k8xC
polský	polský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
polské	polský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
v	v	k7c6
Podleském	podleský	k2eAgNnSc6d1
vojvodství	vojvodství	k1gNnSc6
a	a	k8xC
na	na	k7c6
běloruské	běloruský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
v	v	k7c6
Brestské	brestský	k2eAgFnSc6d1
a	a	k8xC
Hrodenské	Hrodenský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc4
části	část	k1gFnPc1
na	na	k7c4
sebe	sebe	k3xPyFc4
plynule	plynule	k6eAd1
navazují	navazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
hranice	hranice	k1gFnPc1
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
je	být	k5eAaImIp3nS
pro	pro	k7c4
lidi	člověk	k1gMnPc4
i	i	k8xC
větší	veliký	k2eAgFnSc1d2
zvěř	zvěř	k1gFnSc1
neprostupná	prostupný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
tomto	tento	k3xDgInSc6
parku	park	k1gInSc6
původní	původní	k2eAgInSc1d1
temperátní	temperátní	k2eAgInSc1d1
Bělověžský	Bělověžský	k2eAgInSc1d1
prales	prales	k1gInSc1
a	a	k8xC
populace	populace	k1gFnSc1
zubra	zubr	k1gMnSc2
evropského	evropský	k2eAgMnSc2d1
a	a	k8xC
jiných	jiný	k2eAgNnPc2d1
divokých	divoký	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Park	park	k1gInSc1
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
biosférickou	biosférický	k2eAgFnSc7d1
rezervací	rezervace	k1gFnSc7
UNESCO	Unesco	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Park	park	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
nížině	nížina	k1gFnSc6
na	na	k7c6
řekách	řeka	k1gFnPc6
Narewka	Narewko	k1gNnSc2
<g/>
,	,	kIx,
Hwoźna	Hwoźno	k1gNnSc2
a	a	k8xC
Łutownia	Łutownium	k1gNnSc2
<g/>
,	,	kIx,
územím	území	k1gNnSc7
též	též	k6eAd1
protékají	protékat	k5eAaImIp3nP
menší	malý	k2eAgFnPc1d2
říčky	říčka	k1gFnPc1
Przedzielna	Przedzielna	k1gFnSc1
<g/>
,	,	kIx,
Sirota	sirota	k1gFnSc1
a	a	k8xC
Orłowka	Orłowka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občas	občas	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
močály	močál	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půdy	půda	k1gFnPc1
jsou	být	k5eAaImIp3nP
propustné	propustný	k2eAgFnPc1d1
písčité	písčitý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prales	prales	k1gInSc1
je	být	k5eAaImIp3nS
smíšený	smíšený	k2eAgInSc1d1
s	s	k7c7
převahou	převaha	k1gFnSc7
listnáčů	listnáč	k1gInPc2
<g/>
,	,	kIx,
pozoruhodné	pozoruhodný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
staleté	staletý	k2eAgInPc4d1
duby	dub	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rezervaci	rezervace	k1gFnSc6
volně	volně	k6eAd1
žije	žít	k5eAaImIp3nS
mnoho	mnoho	k4c1
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
nejzajímavější	zajímavý	k2eAgNnSc4d3
patří	patřit	k5eAaImIp3nS
ještě	ještě	k9
nedávno	nedávno	k6eAd1
značně	značně	k6eAd1
ohrožení	ohrožený	k2eAgMnPc1d1
evropští	evropský	k2eAgMnPc1d1
zubři	zubr	k1gMnPc1
nebo	nebo	k8xC
kdysi	kdysi	k6eAd1
vyhynulí	vyhynulý	k2eAgMnPc1d1
zpětně	zpětně	k6eAd1
vyšlechtění	vyšlechtěný	k2eAgMnPc1d1
tarpani	tarpan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turisté	turist	k1gMnPc1
se	se	k3xPyFc4
smějí	smát	k5eAaImIp3nP
pohybovat	pohybovat	k5eAaImF
jen	jen	k9
po	po	k7c6
vyznačených	vyznačený	k2eAgFnPc6d1
cestách	cesta	k1gFnPc6
<g/>
,	,	kIx,
případně	případně	k6eAd1
po	po	k7c6
lesní	lesní	k2eAgFnSc6d1
turistické	turistický	k2eAgFnSc6d1
úzkokolejce	úzkokolejka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Okolní	okolní	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
</s>
<s>
Z	z	k7c2
okolních	okolní	k2eAgFnPc2d1
vesnic	vesnice	k1gFnPc2
je	být	k5eAaImIp3nS
nejdůležitější	důležitý	k2eAgFnSc1d3
Bělověž	Bělověž	k1gFnSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
Pogorzelce	Pogorzelec	k1gInPc1
<g/>
,	,	kIx,
Teremiski	Teremiski	k1gNnPc1
a	a	k8xC
Zamosze	Zamosze	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Vstupní	vstupní	k2eAgFnPc1d1
brány	brána	k1gFnPc1
do	do	k7c2
parku	park	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Bělověžský	Bělověžský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
v	v	k7c6
květnu	květen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Pohled	pohled	k1gInSc1
do	do	k7c2
slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Mrtvé	mrtvý	k2eAgNnSc1d1
tělo	tělo	k1gNnSc1
staletého	staletý	k2eAgInSc2d1
dubu	dub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Častým	častý	k2eAgInSc7d1
jevem	jev	k1gInSc7
jsou	být	k5eAaImIp3nP
vyvrácené	vyvrácený	k2eAgInPc1d1
mělcekořenící	mělcekořenící	k2eAgInPc1d1
stromy	strom	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
zůstávají	zůstávat	k5eAaImIp3nP
ležet	ležet	k5eAaImF
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ležící	ležící	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
jsou	být	k5eAaImIp3nP
nechány	nechat	k5eAaPmNgInP
na	na	k7c4
pospas	pospas	k1gInSc4
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kmeny	kmen	k1gInPc1
zetlí	zetlít	k5eAaPmIp3nP
asi	asi	k9
za	za	k7c4
70	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Močál	močál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Evropský	evropský	k2eAgMnSc1d1
zubr	zubr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Novodobí	novodobý	k2eAgMnPc1d1
tarpani	tarpan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Turistický	turistický	k2eAgInSc4d1
úzkorozchodný	úzkorozchodný	k2eAgInSc4d1
vláček	vláček	k1gInSc4
na	na	k7c6
trase	trasa	k1gFnSc6
Hajnovka-Topiło	Hajnovka-Topiło	k6eAd1
v	v	k7c6
polské	polský	k2eAgFnSc6d1
části	část	k1gFnSc6
parku	park	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
úzkorozchodných	úzkorozchodný	k2eAgFnPc2d1
železnic	železnice	k1gFnPc2
v	v	k7c6
polské	polský	k2eAgFnSc6d1
části	část	k1gFnSc6
parku	park	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Bělorusko	Bělorusko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Polsko	Polsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
260731	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4496801-2	4496801-2	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1016	#num#	k4
0890	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
148302686	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Národní	národní	k2eAgInPc1d1
parky	park	k1gInPc1
v	v	k7c6
Polsku	Polsko	k1gNnSc6
</s>
<s>
Babia	Babia	k1gFnSc1
Góra	Gór	k1gInSc2
•	•	k?
Bělověžský	Bělověžský	k2eAgInSc1d1
•	•	k?
Biebrzanský	Biebrzanský	k2eAgInSc1d1
•	•	k?
Bieszczadský	Bieszczadský	k2eAgInSc1d1
•	•	k?
Drawenský	Drawenský	k2eAgInSc1d1
•	•	k?
Gorczaňský	Gorczaňský	k2eAgInSc1d1
•	•	k?
Kampinoský	Kampinoský	k2eAgInSc1d1
•	•	k?
Krkonošský	krkonošský	k2eAgInSc1d1
•	•	k?
Magurský	Magurský	k2eAgInSc1d1
•	•	k?
Narwiaňský	Narwiaňský	k2eAgInSc1d1
•	•	k?
Ojcowský	Ojcowský	k2eAgInSc1d1
•	•	k?
Pieninský	Pieninský	k2eAgInSc1d1
•	•	k?
Poleský	Poleský	k2eAgInSc1d1
•	•	k?
Roztoczanský	Roztoczanský	k2eAgInSc1d1
•	•	k?
Sloviňský	Sloviňský	k2eAgInSc1d1
•	•	k?
Stolové	stolový	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Svatokřížský	Svatokřížský	k2eAgInSc1d1
•	•	k?
Tatranský	tatranský	k2eAgInSc1d1
•	•	k?
Tucholské	Tucholský	k2eAgInPc1d1
bory	bor	k1gInPc1
•	•	k?
Ústí	ústí	k1gNnSc1
Warty	Warta	k1gFnSc2
•	•	k?
Velkopolský	velkopolský	k2eAgInSc1d1
•	•	k?
Wigerský	Wigerský	k2eAgInSc1d1
•	•	k?
Wolinský	Wolinský	k2eAgInSc1d1
</s>
