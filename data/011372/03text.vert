<p>
<s>
Oblouk	oblouk	k1gInSc1	oblouk
je	být	k5eAaImIp3nS	být
architektonická	architektonický	k2eAgFnSc1d1	architektonická
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
překlenuje	překlenovat	k5eAaImIp3nS	překlenovat
otvor	otvor	k1gInSc4	otvor
ve	v	k7c6	v
zdivu	zdivo	k1gNnSc6	zdivo
<g/>
.	.	kIx.	.
</s>
<s>
Obloukové	obloukový	k2eAgFnPc1d1	oblouková
klenby	klenba	k1gFnPc1	klenba
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
i	i	k9	i
nosnou	nosný	k2eAgFnSc4d1	nosná
konstrukci	konstrukce	k1gFnSc4	konstrukce
stropů	strop	k1gInPc2	strop
<g/>
,	,	kIx,	,
střech	střecha	k1gFnPc2	střecha
velkých	velký	k2eAgNnPc2d1	velké
rozpětí	rozpětí	k1gNnPc2	rozpětí
a	a	k8xC	a
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Smysl	smysl	k1gInSc1	smysl
zakřivení	zakřivení	k1gNnSc2	zakřivení
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tlak	tlak	k1gInSc1	tlak
zdiva	zdivo	k1gNnSc2	zdivo
rozvádí	rozvádět	k5eAaImIp3nS	rozvádět
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
smykové	smykový	k2eAgFnPc4d1	smyková
<g/>
,	,	kIx,	,
ohybové	ohybový	k2eAgFnPc4d1	ohybová
atd.	atd.	kA	atd.
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Oblouk	oblouk	k1gInSc1	oblouk
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dvourozměrná	dvourozměrný	k2eAgFnSc1d1	dvourozměrná
<g/>
"	"	kIx"	"
klenba	klenba	k1gFnSc1	klenba
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
součástí	součást	k1gFnSc7	součást
kleneb	klenba	k1gFnPc2	klenba
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
žebra	žebro	k1gNnPc1	žebro
<g/>
,	,	kIx,	,
pásy	pás	k1gInPc1	pás
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
mnoho	mnoho	k6eAd1	mnoho
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
a	a	k8xC	a
funkce	funkce	k1gFnSc1	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
konstrukce	konstrukce	k1gFnSc1	konstrukce
nadpraží	nadpraží	k1gNnSc2	nadpraží
otvoru	otvor	k1gInSc2	otvor
<g/>
,	,	kIx,	,
vodorovný	vodorovný	k2eAgInSc1d1	vodorovný
nosník	nosník	k1gInSc1	nosník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
namáhán	namáhat	k5eAaImNgInS	namáhat
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
polovině	polovina	k1gFnSc6	polovina
průřezu	průřez	k1gInSc2	průřez
tlakem	tlak	k1gInSc7	tlak
a	a	k8xC	a
v	v	k7c4	v
dolní	dolní	k2eAgFnSc4d1	dolní
tahem	tah	k1gInSc7	tah
<g/>
.	.	kIx.	.
</s>
<s>
Cihla	cihla	k1gFnSc1	cihla
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
i	i	k8xC	i
zdivo	zdivo	k1gNnSc1	zdivo
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoká	k1gFnSc7	vysoká
pevnost	pevnost	k1gFnSc4	pevnost
v	v	k7c6	v
tlaku	tlak	k1gInSc6	tlak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
malou	malý	k2eAgFnSc4d1	malá
v	v	k7c6	v
tahu	tah	k1gInSc6	tah
<g/>
.	.	kIx.	.
</s>
<s>
Vodorovný	vodorovný	k2eAgInSc1d1	vodorovný
kamenný	kamenný	k2eAgInSc1d1	kamenný
nosník	nosník	k1gInSc1	nosník
(	(	kIx(	(
<g/>
architráv	architráv	k1gInSc1	architráv
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
proto	proto	k6eAd1	proto
použitelný	použitelný	k2eAgInSc1d1	použitelný
jen	jen	k9	jen
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc4d1	malá
rozpony	rozpona	k1gFnPc4	rozpona
<g/>
.	.	kIx.	.
</s>
<s>
Poměry	poměr	k1gInPc1	poměr
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
nosníky	nosník	k1gInPc1	nosník
opřou	opřít	k5eAaPmIp3nP	opřít
o	o	k7c4	o
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
trojúhelníkovou	trojúhelníkový	k2eAgFnSc4d1	trojúhelníková
klenbu	klenba	k1gFnSc4	klenba
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Druhy	druh	k1gMnPc4	druh
kleneb	klenba	k1gFnPc2	klenba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
působení	působení	k1gNnSc1	působení
sil	síla	k1gFnPc2	síla
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
ideální	ideální	k2eAgInSc4d1	ideální
klenební	klenební	k2eAgInSc4d1	klenební
oblouk	oblouk	k1gInSc4	oblouk
představit	představit	k5eAaPmF	představit
jako	jako	k8xC	jako
obrácené	obrácený	k2eAgNnSc1d1	obrácené
lano	lano	k1gNnSc1	lano
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
místo	místo	k7c2	místo
tahu	tah	k1gInSc2	tah
působí	působit	k5eAaImIp3nP	působit
pouze	pouze	k6eAd1	pouze
tlaky	tlak	k1gInPc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
volně	volně	k6eAd1	volně
visící	visící	k2eAgNnSc4d1	visící
lano	lano	k1gNnSc4	lano
<g/>
,	,	kIx,	,
také	také	k9	také
ideální	ideální	k2eAgInSc1d1	ideální
oblouk	oblouk	k1gInSc1	oblouk
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
řetězovky	řetězovka	k1gFnSc2	řetězovka
(	(	kIx(	(
<g/>
Gateway	Gatewaa	k1gFnSc2	Gatewaa
Arch	arch	k1gInSc1	arch
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Louis	Louis	k1gMnSc1	Louis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
paraboly	parabola	k1gFnSc2	parabola
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zděný	zděný	k2eAgInSc4d1	zděný
oblouk	oblouk	k1gInSc4	oblouk
paradoxně	paradoxně	k6eAd1	paradoxně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pevný	pevný	k2eAgInSc1d1	pevný
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
stálém	stálý	k2eAgNnSc6d1	stálé
zatížení	zatížení	k1gNnSc6	zatížení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
brání	bránit	k5eAaImIp3nS	bránit
vzájemným	vzájemný	k2eAgInPc3d1	vzájemný
posunům	posun	k1gInPc3	posun
kamenů	kámen	k1gInPc2	kámen
nebo	nebo	k8xC	nebo
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mostních	mostní	k2eAgInPc2d1	mostní
oblouků	oblouk	k1gInPc2	oblouk
a	a	k8xC	a
ocelových	ocelový	k2eAgInPc2d1	ocelový
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
železobetonových	železobetonový	k2eAgFnPc2d1	železobetonová
konstrukcí	konstrukce	k1gFnPc2	konstrukce
vůbec	vůbec	k9	vůbec
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
také	také	k9	také
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
konce	konec	k1gInSc2	konec
oblouku	oblouk	k1gInSc2	oblouk
pevně	pevně	k6eAd1	pevně
vetknuty	vetknout	k5eAaPmNgInP	vetknout
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
zda	zda	k8xS	zda
mají	mít	k5eAaImIp3nP	mít
dva	dva	k4xCgMnPc1	dva
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
více	hodně	k6eAd2	hodně
kloubů	kloub	k1gInPc2	kloub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
klenby	klenba	k1gFnPc1	klenba
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
používaly	používat	k5eAaImAgInP	používat
se	se	k3xPyFc4	se
ale	ale	k9	ale
jen	jen	k9	jen
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
rozměrech	rozměr	k1gInPc6	rozměr
a	a	k8xC	a
pro	pro	k7c4	pro
podzemní	podzemní	k2eAgFnPc4d1	podzemní
stavby	stavba	k1gFnPc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
oblouky	oblouk	k1gInPc7	oblouk
používali	používat	k5eAaImAgMnP	používat
výhradně	výhradně	k6eAd1	výhradně
u	u	k7c2	u
užitných	užitný	k2eAgFnPc2d1	užitná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
šířkou	šířka	k1gFnSc7	šířka
oblouku	oblouk	k1gInSc2	oblouk
1-3	[number]	k4	1-3
metry	metr	k1gInPc4	metr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sýpky	sýpka	k1gFnSc2	sýpka
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Ramese	Ramese	k1gFnSc2	Ramese
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
příslušníka	příslušník	k1gMnSc2	příslušník
19	[number]	k4	19
<g/>
.	.	kIx.	.
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
součásti	součást	k1gFnPc1	součást
tzv.	tzv.	kA	tzv.
Ramesea	Ramesea	k1gFnSc1	Ramesea
v	v	k7c6	v
Thébách	Théby	k1gFnPc6	Théby
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
několik	několik	k4yIc1	několik
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
byly	být	k5eAaImAgFnP	být
vzájemně	vzájemně	k6eAd1	vzájemně
bočně	bočně	k6eAd1	bočně
opřeny	opřít	k5eAaPmNgInP	opřít
o	o	k7c4	o
společnou	společný	k2eAgFnSc4d1	společná
zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
techniku	technika	k1gFnSc4	technika
ale	ale	k8xC	ale
nepoužívali	používat	k5eNaImAgMnP	používat
u	u	k7c2	u
kamených	kamený	k2eAgInPc2d1	kamený
chrámů	chrám	k1gInPc2	chrám
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
namísto	namísto	k7c2	namísto
oblouků	oblouk	k1gInPc2	oblouk
vždy	vždy	k6eAd1	vždy
používaly	používat	k5eAaImAgInP	používat
kamenné	kamenný	k2eAgInPc1d1	kamenný
překlady	překlad	k1gInPc1	překlad
(	(	kIx(	(
<g/>
snad	snad	k9	snad
pro	pro	k7c4	pro
delší	dlouhý	k2eAgFnSc4d2	delší
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
životnost	životnost	k1gFnSc4	životnost
takových	takový	k3xDgFnPc2	takový
staveb	stavba	k1gFnPc2	stavba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgInPc1d1	řecký
chrámy	chrám	k1gInPc1	chrám
oblouky	oblouk	k1gInPc1	oblouk
většinou	většina	k1gFnSc7	většina
nepoužívaly	používat	k5eNaImAgInP	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
stavět	stavět	k5eAaImF	stavět
monumentální	monumentální	k2eAgInPc1d1	monumentální
kamenné	kamenný	k2eAgInPc1d1	kamenný
oblouky	oblouk	k1gInPc1	oblouk
<g/>
,	,	kIx,	,
brány	brána	k1gFnPc1	brána
<g/>
,	,	kIx,	,
mosty	most	k1gInPc1	most
a	a	k8xC	a
akvadukty	akvadukt	k1gInPc1	akvadukt
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
(	(	kIx(	(
<g/>
půlkruhovou	půlkruhový	k2eAgFnSc7d1	půlkruhová
<g/>
)	)	kIx)	)
klenbou	klenba	k1gFnSc7	klenba
<g/>
.	.	kIx.	.
</s>
<s>
Kamenné	kamenný	k2eAgInPc1d1	kamenný
a	a	k8xC	a
cihlové	cihlový	k2eAgInPc1d1	cihlový
oblouky	oblouk	k1gInPc1	oblouk
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
užívaly	užívat	k5eAaImAgFnP	užívat
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Masivní	masivní	k2eAgInPc1d1	masivní
plné	plný	k2eAgInPc1d1	plný
oblouky	oblouk	k1gInPc1	oblouk
kleneb	klenba	k1gFnPc2	klenba
valených	valený	k2eAgFnPc2d1	valená
románského	románský	k2eAgInSc2d1	románský
slohu	sloh	k1gInSc2	sloh
vystřídaly	vystřídat	k5eAaPmAgInP	vystřídat
daleko	daleko	k6eAd1	daleko
lehčí	lehký	k2eAgInPc4d2	lehčí
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
bohatě	bohatě	k6eAd1	bohatě
profilované	profilovaný	k2eAgFnPc1d1	profilovaná
konstrukce	konstrukce	k1gFnPc1	konstrukce
kleneb	klenba	k1gFnPc2	klenba
gotických	gotický	k2eAgFnPc2d1	gotická
<g/>
.	.	kIx.	.
</s>
<s>
Renesance	renesance	k1gFnSc1	renesance
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
římským	římský	k2eAgInPc3d1	římský
vzorům	vzor	k1gInPc3	vzor
<g/>
,	,	kIx,	,
okna	okno	k1gNnPc1	okno
i	i	k8xC	i
dveře	dveře	k1gFnPc1	dveře
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
obdélníkové	obdélníkový	k2eAgFnPc1d1	obdélníková
<g/>
,	,	kIx,	,
s	s	k7c7	s
kamenným	kamenný	k2eAgNnSc7d1	kamenné
ostěním	ostění	k1gNnSc7	ostění
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
sloh	sloh	k1gInSc1	sloh
si	se	k3xPyFc3	se
liboval	libovat	k5eAaImAgInS	libovat
v	v	k7c6	v
rozmanitých	rozmanitý	k2eAgInPc6d1	rozmanitý
tvarech	tvar	k1gInPc6	tvar
kleneb	klenba	k1gFnPc2	klenba
od	od	k7c2	od
kleneb	klenba	k1gFnPc2	klenba
obloukových	obloukový	k2eAgFnPc2d1	oblouková
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
klenby	klenba	k1gFnPc4	klenba
sférické	sférický	k2eAgFnPc4d1	sférická
po	po	k7c6	po
užití	užití	k1gNnSc6	užití
nových	nový	k2eAgFnPc2d1	nová
zborcených	zborcený	k2eAgFnPc2d1	zborcená
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
poměrně	poměrně	k6eAd1	poměrně
plochých	plochý	k2eAgFnPc2d1	plochá
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
lehčí	lehčit	k5eAaImIp3nS	lehčit
cihlové	cihlový	k2eAgNnSc1d1	cihlové
zdivo	zdivo	k1gNnSc1	zdivo
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
řemeslnou	řemeslný	k2eAgFnSc7d1	řemeslná
zručností	zručnost	k1gFnSc7	zručnost
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
stavitelů	stavitel	k1gMnPc2	stavitel
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
období	období	k1gNnSc6	období
historismu	historismus	k1gInSc2	historismus
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgInPc1d1	hlavní
umělecké	umělecký	k2eAgInPc1d1	umělecký
slohy	sloh	k1gInPc1	sloh
rychle	rychle	k6eAd1	rychle
vystřídaly	vystřídat	k5eAaPmAgInP	vystřídat
<g/>
,	,	kIx,	,
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
objevovat	objevovat	k5eAaImF	objevovat
možnosti	možnost	k1gFnPc4	možnost
oblouků	oblouk	k1gInPc2	oblouk
železobetonových	železobetonový	k2eAgInPc2d1	železobetonový
a	a	k8xC	a
ocelových	ocelový	k2eAgInPc2d1	ocelový
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
i	i	k9	i
dnes	dnes	k6eAd1	dnes
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
u	u	k7c2	u
reprezentativních	reprezentativní	k2eAgFnPc2d1	reprezentativní
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
vytvářet	vytvářet	k5eAaImF	vytvářet
jedinečné	jedinečný	k2eAgInPc4d1	jedinečný
geometrické	geometrický	k2eAgInPc4d1	geometrický
tvary	tvar	k1gInPc4	tvar
hal	hala	k1gFnPc2	hala
<g/>
,	,	kIx,	,
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
továren	továrna	k1gFnPc2	továrna
<g/>
,	,	kIx,	,
kostelů	kostel	k1gInPc2	kostel
i	i	k8xC	i
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
klenebních	klenební	k2eAgInPc2d1	klenební
oblouků	oblouk	k1gInPc2	oblouk
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
následující	následující	k2eAgInPc4d1	následující
druhy	druh	k1gInPc4	druh
klenebních	klenební	k2eAgInPc2d1	klenební
oblouků	oblouk	k1gInPc2	oblouk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
galerii	galerie	k1gFnSc6	galerie
nejsou	být	k5eNaImIp3nP	být
uvedené	uvedený	k2eAgInPc1d1	uvedený
následující	následující	k2eAgInPc1d1	následující
typy	typ	k1gInPc1	typ
oblouků	oblouk	k1gInPc2	oblouk
<g/>
:	:	kIx,	:
prostý	prostý	k2eAgInSc1d1	prostý
přímý	přímý	k2eAgInSc1d1	přímý
<g/>
,	,	kIx,	,
přímý	přímý	k2eAgInSc1d1	přímý
se	s	k7c7	s
zaoblenými	zaoblený	k2eAgInPc7d1	zaoblený
rohy	roh	k1gInPc7	roh
<g/>
,	,	kIx,	,
převýšený	převýšený	k2eAgInSc1d1	převýšený
<g/>
,	,	kIx,	,
eliptický	eliptický	k2eAgInSc1d1	eliptický
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
kobylí	kobylí	k2eAgFnSc1d1	kobylí
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
kýlový	kýlový	k2eAgInSc1d1	kýlový
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
hvězdový	hvězdový	k2eAgInSc1d1	hvězdový
<g/>
,	,	kIx,	,
myurský	myurský	k2eAgInSc1d1	myurský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukční	konstrukční	k2eAgInPc4d1	konstrukční
problémy	problém	k1gInPc4	problém
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tvaru	tvar	k1gInSc6	tvar
budou	být	k5eAaImBp3nP	být
konce	konec	k1gInSc2	konec
oblouku	oblouk	k1gInSc2	oblouk
působit	působit	k5eAaImF	působit
na	na	k7c4	na
své	své	k1gNnSc4	své
podpory	podpora	k1gFnSc2	podpora
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
lze	lze	k6eAd1	lze
rozložit	rozložit	k5eAaPmF	rozložit
na	na	k7c4	na
svislou	svislý	k2eAgFnSc4d1	svislá
a	a	k8xC	a
vodorovnou	vodorovný	k2eAgFnSc4d1	vodorovná
složku	složka	k1gFnSc4	složka
<g/>
,	,	kIx,	,
směřující	směřující	k2eAgInSc4d1	směřující
vnějším	vnější	k2eAgInSc7d1	vnější
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
složka	složka	k1gFnSc1	složka
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
je	být	k5eAaImIp3nS	být
klenební	klenební	k2eAgInSc1d1	klenební
oblouk	oblouk	k1gInSc1	oblouk
plošší	plochý	k2eAgInSc1d2	plošší
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
klenební	klenební	k2eAgInSc4d1	klenební
oblouk	oblouk	k1gInSc4	oblouk
ve	v	k7c6	v
zdivu	zdivo	k1gNnSc6	zdivo
(	(	kIx(	(
<g/>
okno	okno	k1gNnSc1	okno
<g/>
,	,	kIx,	,
dveře	dveře	k1gFnPc1	dveře
<g/>
,	,	kIx,	,
portál	portál	k1gInSc1	portál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdivo	zdivo	k1gNnSc1	zdivo
obě	dva	k4xCgFnPc1	dva
síly	síla	k1gFnPc1	síla
zachytí	zachytit	k5eAaPmIp3nP	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mostní	mostní	k2eAgInPc1d1	mostní
oblouky	oblouk	k1gInPc1	oblouk
se	se	k3xPyFc4	se
opírají	opírat	k5eAaImIp3nP	opírat
o	o	k7c4	o
sebe	sebe	k3xPyFc4	sebe
navzájem	navzájem	k6eAd1	navzájem
a	a	k8xC	a
stranové	stranový	k2eAgInPc1d1	stranový
tlaky	tlak	k1gInPc1	tlak
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ruší	rušit	k5eAaImIp3nS	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
však	však	k9	však
klenební	klenební	k2eAgInSc1d1	klenební
oblouk	oblouk	k1gInSc1	oblouk
opřen	opřít	k5eAaPmNgInS	opřít
o	o	k7c4	o
vnější	vnější	k2eAgFnPc4d1	vnější
stěny	stěna	k1gFnPc4	stěna
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
stěny	stěna	k1gFnPc4	stěna
tlačit	tlačit	k5eAaImF	tlačit
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
větší	veliký	k2eAgFnSc1d2	veliký
je	být	k5eAaImIp3nS	být
rozpon	rozpon	k1gInSc4	rozpon
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
těžší	těžký	k2eAgFnSc1d2	těžší
je	být	k5eAaImIp3nS	být
konstrukce	konstrukce	k1gFnSc1	konstrukce
klenby	klenba	k1gFnSc2	klenba
a	a	k8xC	a
čím	čí	k3xOyQgNnSc7	čí
vyšší	vysoký	k2eAgInPc1d2	vyšší
jsou	být	k5eAaImIp3nP	být
stěny	stěna	k1gFnPc1	stěna
nebo	nebo	k8xC	nebo
sloupy	sloup	k1gInPc1	sloup
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
je	být	k5eAaImIp3nS	být
klenba	klenba	k1gFnSc1	klenba
postavena	postaven	k2eAgFnSc1d1	postavena
<g/>
.	.	kIx.	.
</s>
<s>
Stavitelé	stavitel	k1gMnPc1	stavitel
kamenných	kamenný	k2eAgInPc2d1	kamenný
a	a	k8xC	a
cihlových	cihlový	k2eAgInPc2d1	cihlový
chrámů	chrám	k1gInPc2	chrám
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
vyrovnávali	vyrovnávat	k5eAaImAgMnP	vyrovnávat
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
obdobích	období	k1gNnPc6	období
různě	různě	k6eAd1	různě
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
románský	románský	k2eAgInSc1d1	románský
sloh	sloh	k1gInSc1	sloh
<g/>
:	:	kIx,	:
nosné	nosný	k2eAgFnPc1d1	nosná
stěny	stěna	k1gFnPc1	stěna
mají	mít	k5eAaImIp3nP	mít
velkou	velký	k2eAgFnSc4d1	velká
tloušťku	tloušťka	k1gFnSc4	tloušťka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
boční	boční	k2eAgFnSc1d1	boční
složka	složka	k1gFnSc1	složka
je	být	k5eAaImIp3nS	být
eliminována	eliminovat	k5eAaBmNgFnS	eliminovat
v	v	k7c6	v
tloušťce	tloušťka	k1gFnSc6	tloušťka
stěny	stěna	k1gFnSc2	stěna
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
gotický	gotický	k2eAgInSc4d1	gotický
sloh	sloh	k1gInSc4	sloh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
lomený	lomený	k2eAgInSc1d1	lomený
oblouk	oblouk	k1gInSc1	oblouk
má	mít	k5eAaImIp3nS	mít
obecně	obecně	k6eAd1	obecně
menší	malý	k2eAgFnSc4d2	menší
složku	složka	k1gFnSc4	složka
boční	boční	k2eAgFnSc4d1	boční
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
složku	složka	k1gFnSc4	složka
svislou	svislý	k2eAgFnSc4d1	svislá
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
třeba	třeba	k6eAd1	třeba
tak	tak	k6eAd1	tak
silných	silný	k2eAgFnPc2d1	silná
stěn	stěna	k1gFnPc2	stěna
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
stěny	stěna	k1gFnPc1	stěna
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
zesilovány	zesilován	k2eAgInPc4d1	zesilován
opěrnými	opěrný	k2eAgInPc7d1	opěrný
pilíři	pilíř	k1gInPc7	pilíř
(	(	kIx(	(
<g/>
opěráky	opěrák	k1gInPc7	opěrák
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
složitějším	složitý	k2eAgInSc7d2	složitější
opěrným	opěrný	k2eAgInSc7d1	opěrný
systémem	systém	k1gInSc7	systém
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
vícelodních	vícelodní	k2eAgFnPc2d1	vícelodní
staveb	stavba	k1gFnPc2	stavba
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
lodi	loď	k1gFnSc2	loď
zhruba	zhruba	k6eAd1	zhruba
poloviční	poloviční	k2eAgFnPc4d1	poloviční
výšky	výška	k1gFnPc4	výška
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
stranové	stranový	k2eAgFnPc1d1	stranová
síly	síla	k1gFnPc1	síla
pomáhaly	pomáhat	k5eAaImAgFnP	pomáhat
zachytit	zachytit	k5eAaPmF	zachytit
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
renesanční	renesanční	k2eAgInSc4d1	renesanční
sloh	sloh	k1gInSc4	sloh
<g/>
:	:	kIx,	:
boční	boční	k2eAgFnPc1d1	boční
síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
eliminovány	eliminovat	k5eAaBmNgFnP	eliminovat
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
nebo	nebo	k8xC	nebo
ocelovým	ocelový	k2eAgNnSc7d1	ocelové
táhlem	táhlo	k1gNnSc7	táhlo
v	v	k7c6	v
patě	pata	k1gFnSc6	pata
klenby	klenba	k1gFnSc2	klenba
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
barokní	barokní	k2eAgInSc4d1	barokní
sloh	sloh	k1gInSc4	sloh
<g/>
:	:	kIx,	:
táhla	táhlo	k1gNnPc1	táhlo
jsou	být	k5eAaImIp3nP	být
přesunuta	přesunout	k5eAaPmNgNnP	přesunout
na	na	k7c4	na
rub	rub	k1gInSc4	rub
klenby	klenba	k1gFnSc2	klenba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nerušila	rušit	k5eNaImAgFnS	rušit
vizuální	vizuální	k2eAgInSc4d1	vizuální
dojem	dojem	k1gInSc4	dojem
zaklenovaného	zaklenovaný	k2eAgInSc2d1	zaklenovaný
prostoru	prostor	k1gInSc2	prostor
<g/>
;	;	kIx,	;
<g/>
Hotová	hotový	k2eAgFnSc1d1	hotová
klenba	klenba	k1gFnSc1	klenba
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
velmi	velmi	k6eAd1	velmi
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
během	během	k7c2	během
stavby	stavba	k1gFnSc2	stavba
je	být	k5eAaImIp3nS	být
však	však	k9	však
většinou	většinou	k6eAd1	většinou
podpírána	podpírat	k5eAaImNgFnS	podpírat
přesným	přesný	k2eAgNnSc7d1	přesné
tvarovým	tvarový	k2eAgNnSc7d1	tvarové
bedněním	bednění	k1gNnSc7	bednění
<g/>
.	.	kIx.	.
</s>
<s>
Kamenné	kamenný	k2eAgInPc4d1	kamenný
oblouky	oblouk	k1gInPc4	oblouk
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
neobyčejně	obyčejně	k6eNd1	obyčejně
přesné	přesný	k2eAgNnSc4d1	přesné
zpracování	zpracování	k1gNnSc4	zpracování
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
příliš	příliš	k6eAd1	příliš
upravovat	upravovat	k5eAaImF	upravovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozkreslování	rozkreslování	k1gNnSc1	rozkreslování
klenebního	klenební	k2eAgInSc2d1	klenební
oblouku	oblouk	k1gInSc2	oblouk
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
kameny	kámen	k1gInPc4	kámen
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kamenořez	kamenořez	k1gInSc1	kamenořez
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
kameny	kámen	k1gInPc1	kámen
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
proti	proti	k7c3	proti
posunutí	posunutí	k1gNnSc1	posunutí
kovovými	kovový	k2eAgFnPc7d1	kovová
sponami	spona	k1gFnPc7	spona
<g/>
,	,	kIx,	,
drážkami	drážka	k1gFnPc7	drážka
na	na	k7c6	na
styčných	styčný	k2eAgFnPc6d1	styčná
plochách	plocha	k1gFnPc6	plocha
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
nalévá	nalévat	k5eAaImIp3nS	nalévat
malta	malta	k1gFnSc1	malta
nebo	nebo	k8xC	nebo
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
i	i	k8xC	i
provléknutými	provléknutý	k2eAgNnPc7d1	provléknutý
ocelovými	ocelový	k2eAgNnPc7d1	ocelové
lany	lano	k1gNnPc7	lano
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Arch	archa	k1gFnPc2	archa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Bogen	Bogen	k1gInSc4	Bogen
(	(	kIx(	(
<g/>
Architektur	architektura	k1gFnPc2	architektura
<g/>
)	)	kIx)	)
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Eduarda	Eduard	k1gMnSc2	Eduard
Lipanská	lipanský	k2eAgFnSc1d1	Lipanská
<g/>
:	:	kIx,	:
Historické	historický	k2eAgFnPc1d1	historická
klenby	klenba	k1gFnPc1	klenba
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
EL	Ela	k1gFnPc2	Ela
Consult	Consulta	k1gFnPc2	Consulta
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-902076-1-8	[number]	k4	80-902076-1-8
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
J.	J.	kA	J.
Blažíček	Blažíček	k1gMnSc1	Blažíček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Kropáček	Kropáček	k1gMnSc1	Kropáček
<g/>
:	:	kIx,	:
Slovník	slovník	k1gInSc1	slovník
pojmů	pojem	k1gInPc2	pojem
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-207-0246-6	[number]	k4	80-207-0246-6
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Herout	Herout	k1gMnSc1	Herout
<g/>
:	:	kIx,	:
Slabikář	slabikář	k1gInSc1	slabikář
návštěvníků	návštěvník	k1gMnPc2	návštěvník
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
SSPPOP	SSPPOP	kA	SSPPOP
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římský	římský	k2eAgInSc1d1	římský
oblouk	oblouk	k1gInSc1	oblouk
</s>
</p>
<p>
<s>
Vítězný	vítězný	k2eAgInSc1d1	vítězný
oblouk	oblouk	k1gInSc1	oblouk
</s>
</p>
<p>
<s>
Klenba	klenba	k1gFnSc1	klenba
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
oblouk	oblouk	k1gInSc1	oblouk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
oblouk	oblouk	k1gInSc1	oblouk
foto	foto	k1gNnSc1	foto
amerického	americký	k2eAgMnSc2d1	americký
vojáka	voják	k1gMnSc2	voják
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
DIYinfo	DIYinfo	k6eAd1	DIYinfo
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
:	:	kIx,	:
Jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
postavit	postavit	k5eAaPmF	postavit
cihlové	cihlový	k2eAgInPc1d1	cihlový
oblouky	oblouk	k1gInPc1	oblouk
okolo	okolo	k7c2	okolo
domu	dům	k1gInSc2	dům
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
DIYinfo	DIYinfo	k6eAd1	DIYinfo
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
:	:	kIx,	:
Jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
postavit	postavit	k5eAaPmF	postavit
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
oblouky	oblouk	k1gInPc1	oblouk
okolo	okolo	k7c2	okolo
domu	dům	k1gInSc2	dům
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Interaktivní	interaktivní	k2eAgFnPc4d1	interaktivní
stránky	stránka	k1gFnPc4	stránka
s	s	k7c7	s
vynikajícím	vynikající	k2eAgInSc7d1	vynikající
výkladem	výklad	k1gInSc7	výklad
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
oblouku	oblouk	k1gInSc6	oblouk
</s>
</p>
