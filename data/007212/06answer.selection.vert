<s>
Hédonismus	hédonismus	k1gInSc1	hédonismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
hédoné	hédoná	k1gFnSc2	hédoná
<g/>
,	,	kIx,	,
potěšení	potěšení	k1gNnSc4	potěšení
<g/>
,	,	kIx,	,
slast	slast	k1gFnSc1	slast
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
filosofické	filosofický	k2eAgNnSc1d1	filosofické
učení	učení	k1gNnSc1	učení
o	o	k7c6	o
slasti	slast	k1gFnSc6	slast
jako	jako	k8xS	jako
hlavním	hlavní	k2eAgInSc6d1	hlavní
motivu	motiv	k1gInSc6	motiv
lidského	lidský	k2eAgNnSc2d1	lidské
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
dvě	dva	k4xCgFnPc4	dva
formy	forma	k1gFnPc4	forma
<g/>
.	.	kIx.	.
</s>
