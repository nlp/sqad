<s>
Hédonismus	hédonismus	k1gInSc1	hédonismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
řec.	řec.	k?	řec.
hédoné	hédoná	k1gFnSc2	hédoná
<g/>
,	,	kIx,	,
potěšení	potěšení	k1gNnSc4	potěšení
<g/>
,	,	kIx,	,
slast	slast	k1gFnSc1	slast
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
filosofické	filosofický	k2eAgNnSc1d1	filosofické
učení	učení	k1gNnSc1	učení
o	o	k7c6	o
slasti	slast	k1gFnSc6	slast
jako	jako	k8xC	jako
hlavním	hlavní	k2eAgInSc6d1	hlavní
motivu	motiv	k1gInSc6	motiv
lidského	lidský	k2eAgNnSc2d1	lidské
jednání	jednání	k1gNnSc2	jednání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
dvě	dva	k4xCgFnPc4	dva
formy	forma	k1gFnPc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Psychologický	psychologický	k2eAgInSc1d1	psychologický
hédonismus	hédonismus	k1gInSc1	hédonismus
je	být	k5eAaImIp3nS	být
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
fakticky	fakticky	k6eAd1	fakticky
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
slast	slast	k1gFnSc4	slast
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
slast	slast	k1gFnSc1	slast
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgNnSc1	veškerý
jednání	jednání	k1gNnSc1	jednání
směřuje	směřovat	k5eAaImIp3nS	směřovat
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
k	k	k7c3	k
dosahování	dosahování	k1gNnSc3	dosahování
slasti	slast	k1gFnSc2	slast
a	a	k8xC	a
vyhýbání	vyhýbání	k1gNnSc2	vyhýbání
se	se	k3xPyFc4	se
strasti	strast	k1gFnSc3	strast
<g/>
.	.	kIx.	.
</s>
<s>
Etický	etický	k2eAgInSc1d1	etický
hédonismus	hédonismus	k1gInSc1	hédonismus
navíc	navíc	k6eAd1	navíc
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slast	slast	k1gFnSc1	slast
je	být	k5eAaImIp3nS	být
také	také	k9	také
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
dobro	dobro	k1gNnSc1	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc4	takový
stanovisko	stanovisko	k1gNnSc4	stanovisko
zastával	zastávat	k5eAaImAgMnS	zastávat
údajně	údajně	k6eAd1	údajně
Eudoxos	Eudoxos	k1gMnSc1	Eudoxos
z	z	k7c2	z
Knidu	Knid	k1gInSc2	Knid
<g/>
,	,	kIx,	,
slavný	slavný	k2eAgMnSc1d1	slavný
řecký	řecký	k2eAgMnSc1d1	řecký
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
starší	starý	k2eAgMnSc1d2	starší
současník	současník	k1gMnSc1	současník
Aristotelův	Aristotelův	k2eAgMnSc1d1	Aristotelův
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eudoxos	Eudoxos	k1gMnSc1	Eudoxos
považoval	považovat	k5eAaImAgMnS	považovat
slast	slast	k1gFnSc4	slast
za	za	k7c4	za
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
dobro	dobro	k1gNnSc4	dobro
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechno	všechen	k3xTgNnSc1	všechen
rozumné	rozumný	k2eAgNnSc1d1	rozumné
i	i	k8xC	i
nerozumné	rozumný	k2eNgNnSc1d1	nerozumné
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
směřuje	směřovat	k5eAaImIp3nS	směřovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
Eudoxos	Eudoxos	k1gMnSc1	Eudoxos
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dobro	dobro	k1gNnSc4	dobro
<g/>
,	,	kIx,	,
a	a	k8xC	a
čím	co	k3yRnSc7	co
více	hodně	k6eAd2	hodně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
žádoucí	žádoucí	k2eAgNnSc1d1	žádoucí
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
přitahováni	přitahovat	k5eAaImNgMnP	přitahovat
ke	k	k7c3	k
stejné	stejný	k2eAgFnSc3d1	stejná
věci	věc	k1gFnSc3	věc
<g/>
,	,	kIx,	,
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
věc	věc	k1gFnSc1	věc
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
;	;	kIx,	;
neboť	neboť	k8xC	neboť
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Eudoxos	Eudoxos	k1gMnSc1	Eudoxos
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
poznává	poznávat	k5eAaImIp3nS	poznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
samo	sám	k3xTgNnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
jako	jako	k9	jako
třeba	třeba	k6eAd1	třeba
vhodná	vhodný	k2eAgFnSc1d1	vhodná
strava	strava	k1gFnSc1	strava
<g/>
,	,	kIx,	,
a	a	k8xC	a
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
dobré	dobrý	k2eAgNnSc1d1	dobré
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
cílem	cíl	k1gInSc7	cíl
snažení	snažení	k1gNnSc2	snažení
všech	všecek	k3xTgInPc2	všecek
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
dobrem	dobro	k1gNnSc7	dobro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
s	s	k7c7	s
hédonismem	hédonismus	k1gInSc7	hédonismus
pracuje	pracovat	k5eAaImIp3nS	pracovat
utilitarismus	utilitarismus	k1gInSc1	utilitarismus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
John	John	k1gMnSc1	John
Stuart	Stuarta	k1gFnPc2	Stuarta
Mill	Milla	k1gFnPc2	Milla
nebo	nebo	k8xC	nebo
Jeremy	Jerema	k1gFnSc2	Jerema
Bentham	Bentham	k1gInSc1	Bentham
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
následovníci	následovník	k1gMnPc1	následovník
<g/>
.	.	kIx.	.
</s>
