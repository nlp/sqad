<p>
<s>
Čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
(	(	kIx(	(
<g/>
Triturus	Triturus	k1gInSc1	Triturus
carnifex	carnifex	k1gInSc1	carnifex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
čolek	čolek	k1gMnSc1	čolek
jihoevropský	jihoevropský	k2eAgMnSc1d1	jihoevropský
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mlokovitý	mlokovitý	k1gMnSc1	mlokovitý
obojživelník	obojživelník	k1gMnSc1	obojživelník
obývající	obývající	k2eAgFnSc4d1	obývající
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
severozápad	severozápad	k1gInSc4	severozápad
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
jih	jih	k1gInSc1	jih
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Rakousko	Rakousko	k1gNnSc1	Rakousko
a	a	k8xC	a
západ	západ	k1gInSc1	západ
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
při	při	k7c6	při
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Dyje	Dyje	k1gFnSc2	Dyje
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
Evropy	Evropa	k1gFnSc2	Evropa
byl	být	k5eAaImAgInS	být
introdukován	introdukovat	k5eAaImNgInS	introdukovat
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
ztrátou	ztráta	k1gFnSc7	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
druhem	druh	k1gInSc7	druh
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
a	a	k8xC	a
měří	měřit	k5eAaImIp3nP	měřit
celkově	celkově	k6eAd1	celkově
asi	asi	k9	asi
180	[number]	k4	180
mm	mm	kA	mm
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
velikosti	velikost	k1gFnSc3	velikost
okolo	okolo	k7c2	okolo
150	[number]	k4	150
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
populace	populace	k1gFnPc1	populace
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
lokalitách	lokalita	k1gFnPc6	lokalita
odlišné	odlišný	k2eAgInPc1d1	odlišný
<g/>
.	.	kIx.	.
</s>
<s>
Biologie	biologie	k1gFnSc1	biologie
čolka	čolek	k1gMnSc2	čolek
dravého	dravý	k2eAgMnSc2d1	dravý
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
biologii	biologie	k1gFnSc4	biologie
čolka	čolek	k1gMnSc2	čolek
velkého	velký	k2eAgMnSc2d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Dospělci	dospělec	k1gMnPc1	dospělec
se	se	k3xPyFc4	se
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
živí	živit	k5eAaImIp3nP	živit
žížalami	žížala	k1gFnPc7	žížala
<g/>
,	,	kIx,	,
stejnonožci	stejnonožec	k1gMnPc1	stejnonožec
či	či	k8xC	či
měkkýši	měkkýš	k1gMnPc1	měkkýš
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
potravu	potrava	k1gFnSc4	potrava
menší	malý	k2eAgFnSc4d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
stojatých	stojatý	k2eAgFnPc6d1	stojatá
až	až	k8xS	až
mírně	mírně	k6eAd1	mírně
průtočných	průtočný	k2eAgFnPc6d1	průtočná
vodách	voda	k1gFnPc6	voda
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
června	červen	k1gInSc2	červen
a	a	k8xC	a
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
žijí	žít	k5eAaImIp3nP	žít
čolci	čolek	k1gMnPc1	čolek
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
jednotlivě	jednotlivě	k6eAd1	jednotlivě
naklade	naklást	k5eAaPmIp3nS	naklást
až	až	k9	až
250	[number]	k4	250
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
asi	asi	k9	asi
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
larvy	larva	k1gFnPc1	larva
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
měří	měřit	k5eAaImIp3nS	měřit
8,7	[number]	k4	8,7
až	až	k9	až
12	[number]	k4	12
mm	mm	kA	mm
a	a	k8xC	a
dospělci	dospělec	k1gMnPc1	dospělec
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
stávají	stávat	k5eAaImIp3nP	stávat
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
s	s	k7c7	s
nedospělými	dospělý	k2eNgMnPc7d1	nedospělý
jedinci	jedinec	k1gMnPc7	jedinec
někdy	někdy	k6eAd1	někdy
i	i	k9	i
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
==	==	k?	==
</s>
</p>
<p>
<s>
Čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
mlokovití	mlokovití	k1gMnPc1	mlokovití
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
Triturus	Triturus	k1gMnSc1	Triturus
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
hybridy	hybrid	k1gInPc4	hybrid
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
čolky	čolek	k1gMnPc7	čolek
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
s	s	k7c7	s
čolkem	čolek	k1gMnSc7	čolek
velkým	velký	k2eAgNnSc7d1	velké
(	(	kIx(	(
<g/>
Triturus	Triturus	k1gInSc1	Triturus
cristatus	cristatus	k1gInSc1	cristatus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nP	křížit
například	například	k6eAd1	například
severně	severně	k6eAd1	severně
od	od	k7c2	od
pohoří	pohoří	k1gNnSc2	pohoří
Alp	Alpy	k1gFnPc2	Alpy
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k8xC	ale
též	též	k9	též
kříženci	kříženec	k1gMnPc1	kříženec
například	například	k6eAd1	například
s	s	k7c7	s
čolkem	čolek	k1gMnSc7	čolek
dunajským	dunajský	k2eAgNnSc7d1	Dunajské
(	(	kIx(	(
<g/>
Triturus	Triturus	k1gInSc1	Triturus
dobrogicus	dobrogicus	k1gInSc1	dobrogicus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Balkánu	Balkán	k1gInSc2	Balkán
žije	žít	k5eAaImIp3nS	žít
oddělená	oddělený	k2eAgFnSc1d1	oddělená
populace	populace	k1gFnSc1	populace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
vedena	vést	k5eAaImNgFnS	vést
jako	jako	k9	jako
poddruh	poddruh	k1gInSc1	poddruh
Triturus	Triturus	k1gInSc1	Triturus
carnifex	carnifex	k1gInSc1	carnifex
macedonicus	macedonicus	k1gInSc1	macedonicus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
roce	rok	k1gInSc6	rok
však	však	k9	však
byla	být	k5eAaImAgFnS	být
kvůli	kvůli	k7c3	kvůli
neobjasněné	objasněný	k2eNgFnSc3d1	neobjasněná
fylogenezi	fylogeneze	k1gFnSc3	fylogeneze
rodu	rod	k1gInSc2	rod
Triturus	Triturus	k1gInSc1	Triturus
provedena	proveden	k2eAgFnSc1d1	provedena
molekulární	molekulární	k2eAgFnSc1d1	molekulární
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tento	tento	k3xDgInSc4	tento
poddruh	poddruh	k1gInSc4	poddruh
vyčlenila	vyčlenit	k5eAaPmAgFnS	vyčlenit
jako	jako	k9	jako
samostatný	samostatný	k2eAgInSc4d1	samostatný
druh	druh	k1gInSc4	druh
Triturus	Triturus	k1gMnSc1	Triturus
macedonicus	macedonicus	k1gMnSc1	macedonicus
<g/>
.	.	kIx.	.
<g/>
Druh	druh	k1gMnSc1	druh
popsal	popsat	k5eAaPmAgMnS	popsat
Joseph	Joseph	k1gMnSc1	Joseph
Nicolaus	Nicolaus	k1gMnSc1	Nicolaus
Laurenti	Laurent	k1gMnPc1	Laurent
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1768	[number]	k4	1768
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
synonym	synonymum	k1gNnPc2	synonymum
<g/>
:	:	kIx,	:
Molge	Molge	k1gFnSc1	Molge
cristatus	cristatus	k1gMnSc1	cristatus
carnifex	carnifex	k1gInSc1	carnifex
var.	var.	k?	var.
neapolitana	neapolitana	k1gFnSc1	neapolitana
<g/>
,	,	kIx,	,
Triton	triton	k1gInSc1	triton
(	(	kIx(	(
<g/>
Alethotriton	Alethotriton	k1gInSc1	Alethotriton
<g/>
)	)	kIx)	)
cristatus	cristatus	k1gMnSc1	cristatus
platycephalus	platycephalus	k1gMnSc1	platycephalus
<g/>
,	,	kIx,	,
Triton	triton	k1gMnSc1	triton
(	(	kIx(	(
<g/>
Neotriton	Neotriton	k1gInSc1	Neotriton
<g/>
)	)	kIx)	)
carnifex	carnifex	k1gInSc1	carnifex
<g/>
,	,	kIx,	,
Triton	triton	k1gMnSc1	triton
cristatus	cristatus	k1gMnSc1	cristatus
carnifex	carnifex	k1gInSc4	carnifex
forma	forma	k1gFnSc1	forma
alpina	alpinum	k1gNnSc2	alpinum
<g/>
,	,	kIx,	,
Triton	triton	k1gMnSc1	triton
cristatus	cristatus	k1gMnSc1	cristatus
var.	var.	k?	var.
carnifex	carnifex	k1gInSc1	carnifex
<g/>
,	,	kIx,	,
Triturus	Triturus	k1gInSc1	Triturus
carnifex	carnifex	k1gInSc1	carnifex
carnifex	carnifex	k1gInSc1	carnifex
<g/>
,	,	kIx,	,
Triturus	Triturus	k1gInSc1	Triturus
cristatus	cristatus	k1gMnSc1	cristatus
carnifex	carnifex	k1gInSc1	carnifex
a	a	k8xC	a
Triturus	Triturus	k1gMnSc1	Triturus
cristatus	cristatus	k1gMnSc1	cristatus
platycauda	platycauda	k1gMnSc1	platycauda
<g/>
.	.	kIx.	.
</s>
<s>
Alternativní	alternativní	k2eAgInSc1d1	alternativní
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
pro	pro	k7c4	pro
druh	druh	k1gInSc4	druh
je	být	k5eAaImIp3nS	být
čolek	čolek	k1gMnSc1	čolek
jihoevropský	jihoevropský	k2eAgMnSc1d1	jihoevropský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
obývá	obývat	k5eAaImIp3nS	obývat
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
severozápad	severozápad	k1gInSc4	severozápad
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
,	,	kIx,	,
jih	jih	k1gInSc1	jih
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Rakousko	Rakousko	k1gNnSc1	Rakousko
a	a	k8xC	a
západ	západ	k1gInSc1	západ
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
žije	žít	k5eAaImIp3nS	žít
při	při	k7c6	při
povodí	povodí	k1gNnSc6	povodí
řeky	řeka	k1gFnSc2	řeka
Dyje	Dyje	k1gFnSc2	Dyje
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
možný	možný	k2eAgInSc1d1	možný
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
)	)	kIx)	)
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
rakouských	rakouský	k2eAgFnPc2d1	rakouská
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
objevitelem	objevitel	k1gMnSc7	objevitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Antonín	Antonín	k1gMnSc1	Antonín
Reiter	Reiter	k1gMnSc1	Reiter
<g/>
,	,	kIx,	,
zoolog	zoolog	k1gMnSc1	zoolog
pracující	pracující	k1gMnSc1	pracující
v	v	k7c6	v
městském	městský	k2eAgNnSc6d1	Městské
muzeu	muzeum	k1gNnSc6	muzeum
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
nebyl	být	k5eNaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
zjištěn	zjistit	k5eAaPmNgInS	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Introdukován	Introdukován	k2eAgInSc1d1	Introdukován
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
do	do	k7c2	do
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
či	či	k8xC	či
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
o	o	k7c4	o
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
utekla	utéct	k5eAaPmAgFnS	utéct
z	z	k7c2	z
terárií	terárium	k1gNnPc2	terárium
<g/>
.	.	kIx.	.
</s>
<s>
Nepůvodní	původní	k2eNgFnSc1d1	nepůvodní
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ženevského	ženevský	k2eAgNnSc2d1	Ženevské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
čolci	čolek	k1gMnPc1	čolek
zavlečeni	zavlečen	k2eAgMnPc1d1	zavlečen
buďto	buďto	k8xC	buďto
začátkem	začátkem	k7c2	začátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
Azorách	Azory	k1gFnPc6	Azory
<g/>
.	.	kIx.	.
<g/>
Čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
obývá	obývat	k5eAaImIp3nS	obývat
řadu	řada	k1gFnSc4	řada
biotopů	biotop	k1gInPc2	biotop
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
například	například	k6eAd1	například
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
travnatých	travnatý	k2eAgInPc2d1	travnatý
porostů	porost	k1gInPc2	porost
či	či	k8xC	či
křovin	křovina	k1gFnPc2	křovina
<g/>
,	,	kIx,	,
nevadí	vadit	k5eNaImIp3nP	vadit
mu	on	k3xPp3gMnSc3	on
ani	ani	k8xC	ani
suché	suchý	k2eAgFnSc6d1	suchá
oblasti	oblast	k1gFnSc6	oblast
s	s	k7c7	s
bukovým	bukový	k2eAgInSc7d1	bukový
porostem	porost	k1gInSc7	porost
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
přizpůsobil	přizpůsobit	k5eAaPmAgMnS	přizpůsobit
životu	život	k1gInSc2	život
na	na	k7c6	na
rýžových	rýžový	k2eAgNnPc6d1	rýžové
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sladkovodní	sladkovodní	k2eAgMnSc1d1	sladkovodní
a	a	k8xC	a
během	během	k7c2	během
období	období	k1gNnSc2	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
stojaté	stojatý	k2eAgFnPc4d1	stojatá
vody	voda	k1gFnPc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
žít	žít	k5eAaImF	žít
až	až	k9	až
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
2	[number]	k4	2
140	[number]	k4	140
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
obývá	obývat	k5eAaImIp3nS	obývat
nejčastěji	často	k6eAd3	často
nížinaté	nížinatý	k2eAgFnPc4d1	nížinatá
oblasti	oblast	k1gFnPc4	oblast
mezi	mezi	k7c7	mezi
200	[number]	k4	200
až	až	k9	až
250	[number]	k4	250
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
druhem	druh	k1gInSc7	druh
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
robustní	robustní	k2eAgFnSc4d1	robustní
postavu	postava	k1gFnSc4	postava
se	s	k7c7	s
širokým	široký	k2eAgInSc7d1	široký
hřbetem	hřbet	k1gInSc7	hřbet
a	a	k8xC	a
zaobleným	zaoblený	k2eAgInSc7d1	zaoblený
čenichem	čenich	k1gInSc7	čenich
<g/>
,	,	kIx,	,
končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgFnPc1d1	krátká
a	a	k8xC	a
silně	silně	k6eAd1	silně
stavěné	stavěný	k2eAgNnSc1d1	stavěné
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
měří	měřit	k5eAaImIp3nP	měřit
od	od	k7c2	od
čenichu	čenich	k1gInSc2	čenich
po	po	k7c4	po
kloaku	kloaka	k1gFnSc4	kloaka
55	[number]	k4	55
až	až	k9	až
88	[number]	k4	88
mm	mm	kA	mm
(	(	kIx(	(
<g/>
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
činí	činit	k5eAaImIp3nS	činit
zhruba	zhruba	k6eAd1	zhruba
180	[number]	k4	180
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
samců	samec	k1gMnPc2	samec
mezi	mezi	k7c7	mezi
čenichem	čenich	k1gInSc7	čenich
a	a	k8xC	a
kloakou	kloaka	k1gFnSc7	kloaka
činí	činit	k5eAaImIp3nS	činit
54	[number]	k4	54
až	až	k9	až
80	[number]	k4	80
mm	mm	kA	mm
(	(	kIx(	(
<g/>
celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
asi	asi	k9	asi
150	[number]	k4	150
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
větší	veliký	k2eAgMnPc1d2	veliký
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
populace	populace	k1gFnPc1	populace
potom	potom	k6eAd1	potom
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
odlišných	odlišný	k2eAgFnPc2d1	odlišná
velikostí	velikost	k1gFnPc2	velikost
(	(	kIx(	(
<g/>
jedinci	jedinec	k1gMnPc1	jedinec
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
chladných	chladný	k2eAgFnPc6d1	chladná
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
úhrnem	úhrn	k1gInSc7	úhrn
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
krátké	krátký	k2eAgNnSc4d1	krátké
období	období	k1gNnSc4	období
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
větší	veliký	k2eAgInPc1d2	veliký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
variabilní	variabilní	k2eAgInSc1d1	variabilní
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
,	,	kIx,	,
čolci	čolek	k1gMnPc1	čolek
draví	dravý	k2eAgMnPc1d1	dravý
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
oblastí	oblast	k1gFnPc2	oblast
mohou	moct	k5eAaImIp3nP	moct
vypadat	vypadat	k5eAaImF	vypadat
zcela	zcela	k6eAd1	zcela
odlišně	odlišně	k6eAd1	odlišně
<g/>
;	;	kIx,	;
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
hnědaví	hnědavý	k2eAgMnPc1d1	hnědavý
a	a	k8xC	a
kaštanoví	kaštanový	k2eAgMnPc1d1	kaštanový
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
i	i	k9	i
do	do	k7c2	do
žlutohněda	žlutohnědo	k1gNnSc2	žlutohnědo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
sytě	sytě	k6eAd1	sytě
oranžovočerveným	oranžovočervený	k2eAgNnSc7d1	oranžovočervené
břichem	břicho	k1gNnSc7	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Vzácné	vzácný	k2eAgNnSc1d1	vzácné
je	být	k5eAaImIp3nS	být
namodralé	namodralý	k2eAgNnSc1d1	namodralé
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
například	například	k6eAd1	například
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
nebo	nebo	k8xC	nebo
Itálii	Itálie	k1gFnSc6	Itálie
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
jedinci	jedinec	k1gMnPc1	jedinec
zbarvení	zbarvení	k1gNnSc4	zbarvení
modrošedě	modrošedě	k6eAd1	modrošedě
<g/>
.	.	kIx.	.
</s>
<s>
Spodek	spodek	k1gInSc1	spodek
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
poset	posít	k5eAaPmNgInS	posít
černými	černý	k2eAgFnPc7d1	černá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgInPc4d1	různý
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
do	do	k7c2	do
sebe	se	k3xPyFc2	se
splývat	splývat	k5eAaImF	splývat
<g/>
.	.	kIx.	.
</s>
<s>
Zády	záda	k1gNnPc7	záda
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
oranžovohnědá	oranžovohnědý	k2eAgFnSc1d1	oranžovohnědá
až	až	k9	až
světle	světle	k6eAd1	světle
oranžová	oranžový	k2eAgFnSc1d1	oranžová
páteřní	páteřní	k2eAgFnSc1d1	páteřní
linka	linka	k1gFnSc1	linka
<g/>
,	,	kIx,	,
vyvinout	vyvinout	k5eAaPmF	vyvinout
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
neúplná	úplný	k2eNgFnSc1d1	neúplná
anebo	anebo	k8xC	anebo
vůbec	vůbec	k9	vůbec
žádná	žádný	k3yNgFnSc1	žádný
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
ji	on	k3xPp3gFnSc4	on
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnPc1	samice
a	a	k8xC	a
mladí	mladý	k2eAgMnPc1d1	mladý
čolci	čolek	k1gMnPc1	čolek
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
šedě	šedě	k6eAd1	šedě
lemovanými	lemovaný	k2eAgFnPc7d1	lemovaná
skvrnami	skvrna	k1gFnPc7	skvrna
černého	černé	k1gNnSc2	černé
zabarvení	zabarvení	k1gNnSc2	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Hrdlo	hrdlo	k1gNnSc1	hrdlo
má	mít	k5eAaImIp3nS	mít
zbarvení	zbarvení	k1gNnSc4	zbarvení
žlutooranžové	žlutooranžový	k2eAgNnSc4d1	žlutooranžové
<g/>
,	,	kIx,	,
naoranžovělé	naoranžovělý	k2eAgNnSc4d1	naoranžovělé
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
barevné	barevný	k2eAgFnPc1d1	barevná
varianty	varianta	k1gFnPc1	varianta
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
doplněny	doplnit	k5eAaPmNgFnP	doplnit
o	o	k7c4	o
nahnědlý	nahnědlý	k2eAgInSc4d1	nahnědlý
nádech	nádech	k1gInSc4	nádech
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
hnědočerné	hnědočerný	k2eAgInPc1d1	hnědočerný
<g/>
,	,	kIx,	,
šedočerné	šedočerný	k2eAgInPc1d1	šedočerný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
poseté	posetý	k2eAgFnPc1d1	posetá
tečkami	tečka	k1gFnPc7	tečka
nebo	nebo	k8xC	nebo
mramorovaným	mramorovaný	k2eAgInSc7d1	mramorovaný
vzorem	vzor	k1gInSc7	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
zbarveny	zbarven	k2eAgInPc1d1	zbarven
méně	málo	k6eAd2	málo
pestrými	pestrý	k2eAgFnPc7d1	pestrá
barvami	barva	k1gFnPc7	barva
než	než	k8xS	než
samci	samec	k1gMnPc1	samec
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
zvýrazní	zvýraznit	k5eAaPmIp3nS	zvýraznit
jejich	jejich	k3xOp3gInSc1	jejich
ploutevní	ploutevní	k2eAgInSc1d1	ploutevní
lem	lem	k1gInSc1	lem
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
vlnitý	vlnitý	k2eAgInSc4d1	vlnitý
a	a	k8xC	a
hladký	hladký	k2eAgInSc4d1	hladký
<g/>
.	.	kIx.	.
</s>
<s>
Kůží	kůže	k1gFnPc2	kůže
čolci	čolek	k1gMnPc1	čolek
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
toxický	toxický	k2eAgInSc4d1	toxický
sekret	sekret	k1gInSc4	sekret
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nP	tvořit
potravu	potrava	k1gFnSc4	potrava
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
predátorů	predátor	k1gMnPc2	predátor
<g/>
.	.	kIx.	.
<g/>
Ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
rodu	rod	k1gInSc2	rod
Triturus	Triturus	k1gInSc1	Triturus
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
podobá	podobat	k5eAaImIp3nS	podobat
čolkovi	čolek	k1gMnSc3	čolek
velkému	velký	k2eAgInSc3d1	velký
(	(	kIx(	(
<g/>
Triturus	Triturus	k1gMnSc1	Triturus
cristatus	cristatus	k1gMnSc1	cristatus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
hladším	hladký	k2eAgInSc7d2	hladší
povrchem	povrch	k1gInSc7	povrch
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
většími	veliký	k2eAgFnPc7d2	veliký
končetinami	končetina	k1gFnPc7	končetina
a	a	k8xC	a
kořenem	kořen	k1gInSc7	kořen
ocasu	ocas	k1gInSc2	ocas
a	a	k8xC	a
také	také	k9	také
počtem	počet	k1gInSc7	počet
hrudních	hrudní	k2eAgInPc2d1	hrudní
obratlů	obratel	k1gInPc2	obratel
<g/>
;	;	kIx,	;
čolek	čolek	k1gMnSc1	čolek
velký	velký	k2eAgMnSc1d1	velký
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Biologie	biologie	k1gFnSc1	biologie
čolka	čolek	k1gMnSc2	čolek
dravého	dravý	k2eAgMnSc2d1	dravý
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
biologii	biologie	k1gFnSc4	biologie
čolka	čolek	k1gMnSc2	čolek
velkého	velký	k2eAgMnSc2d1	velký
<g/>
,	,	kIx,	,
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
druhy	druh	k1gInPc7	druh
jsou	být	k5eAaImIp3nP	být
zkoumány	zkoumán	k2eAgInPc1d1	zkoumán
<g/>
.	.	kIx.	.
</s>
<s>
Aktivitu	aktivita	k1gFnSc4	aktivita
projevuje	projevovat	k5eAaImIp3nS	projevovat
čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pozemním	pozemní	k2eAgInSc7d1	pozemní
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
období	období	k1gNnSc2	období
páření	páření	k1gNnSc2	páření
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Útočiště	útočiště	k1gNnSc1	útočiště
hledá	hledat	k5eAaImIp3nS	hledat
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
a	a	k8xC	a
preferuje	preferovat	k5eAaImIp3nS	preferovat
pórovitý	pórovitý	k2eAgInSc4d1	pórovitý
terén	terén	k1gInSc4	terén
<g/>
,	,	kIx,	,
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
na	na	k7c6	na
propustných	propustný	k2eAgInPc6d1	propustný
svazích	svah	k1gInPc6	svah
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kameny	kámen	k1gInPc7	kámen
či	či	k8xC	či
usazeným	usazený	k2eAgNnSc7d1	usazené
bahnem	bahno	k1gNnSc7	bahno
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
tráví	trávit	k5eAaImIp3nP	trávit
i	i	k9	i
zimní	zimní	k2eAgInPc4d1	zimní
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Probouzí	probouzet	k5eAaImIp3nS	probouzet
se	se	k3xPyFc4	se
během	během	k7c2	během
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
podle	podle	k7c2	podle
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
čolci	čolek	k1gMnPc1	čolek
páří	pářit	k5eAaImIp3nP	pářit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
období	období	k1gNnSc2	období
rozmnožování	rozmnožování	k1gNnPc1	rozmnožování
samci	samec	k1gMnPc1	samec
opouštějí	opouštět	k5eAaImIp3nP	opouštět
vodní	vodní	k2eAgMnPc1d1	vodní
prostředí	prostředí	k1gNnSc3	prostředí
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
samice	samice	k1gFnSc1	samice
s	s	k7c7	s
nedospělými	dospělý	k2eNgMnPc7d1	nedospělý
jedinci	jedinec	k1gMnPc7	jedinec
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
až	až	k9	až
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
zde	zde	k6eAd1	zde
potom	potom	k6eAd1	potom
přezimují	přezimovat	k5eAaBmIp3nP	přezimovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
pro	pro	k7c4	pro
sledování	sledování	k1gNnSc4	sledování
čolka	čolek	k1gMnSc2	čolek
dravého	dravý	k2eAgNnSc2d1	dravé
období	období	k1gNnSc2	období
mezi	mezi	k7c7	mezi
koncem	konec	k1gInSc7	konec
března	březen	k1gInSc2	březen
a	a	k8xC	a
polovinou	polovina	k1gFnSc7	polovina
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Potrava	potrava	k1gFnSc1	potrava
===	===	k?	===
</s>
</p>
<p>
<s>
Larvy	larva	k1gFnPc1	larva
čolků	čolek	k1gMnPc2	čolek
dravých	dravý	k2eAgMnPc2d1	dravý
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
vodními	vodní	k2eAgMnPc7d1	vodní
bezobratlými	bezobratlí	k1gMnPc7	bezobratlí
<g/>
,	,	kIx,	,
jinými	jiný	k2eAgFnPc7d1	jiná
larvami	larva	k1gFnPc7	larva
či	či	k8xC	či
pulci	pulec	k1gMnPc7	pulec
<g/>
.	.	kIx.	.
</s>
<s>
Dospělci	dospělec	k1gMnPc1	dospělec
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
loví	lovit	k5eAaImIp3nP	lovit
kořist	kořist	k1gFnSc4	kořist
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
žížaly	žížala	k1gFnPc4	žížala
<g/>
,	,	kIx,	,
stejnonožce	stejnonožec	k1gMnPc4	stejnonožec
a	a	k8xC	a
měkkýše	měkkýš	k1gMnPc4	měkkýš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
drobné	drobný	k2eAgMnPc4d1	drobný
obratlovce	obratlovec	k1gMnPc4	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
pak	pak	k6eAd1	pak
potravu	potrava	k1gFnSc4	potrava
tvoří	tvořit	k5eAaImIp3nP	tvořit
menší	malý	k2eAgMnPc1d2	menší
bezobratlí	bezobratlí	k1gMnPc1	bezobratlí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
larvy	larva	k1gFnPc4	larva
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
zde	zde	k6eAd1	zde
však	však	k9	však
i	i	k9	i
mladé	mladý	k2eAgMnPc4d1	mladý
čolky	čolek	k1gMnPc4	čolek
a	a	k8xC	a
pulce	pulec	k1gMnPc4	pulec
<g/>
.	.	kIx.	.
</s>
<s>
Kanibalismus	kanibalismus	k1gInSc1	kanibalismus
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
jak	jak	k8xS	jak
u	u	k7c2	u
dospělců	dospělec	k1gMnPc2	dospělec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
u	u	k7c2	u
larev	larva	k1gFnPc2	larva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozmnožování	rozmnožování	k1gNnPc1	rozmnožování
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
Rozmnožování	rozmnožování	k1gNnSc4	rozmnožování
u	u	k7c2	u
čolků	čolek	k1gMnPc2	čolek
dravých	dravý	k2eAgFnPc2d1	dravá
probíhá	probíhat	k5eAaImIp3nS	probíhat
především	především	k6eAd1	především
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
však	však	k9	však
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
náhradní	náhradní	k2eAgFnPc4d1	náhradní
snůšky	snůška	k1gFnPc4	snůška
vajíček	vajíčko	k1gNnPc2	vajíčko
i	i	k8xC	i
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
samici	samice	k1gFnSc3	samice
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
hmotnost	hmotnost	k1gFnSc1	hmotnost
vaječníků	vaječník	k1gInPc2	vaječník
a	a	k8xC	a
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
u	u	k7c2	u
samců	samec	k1gInPc2	samec
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
varlata	varle	k1gNnPc1	varle
největší	veliký	k2eAgFnSc2d3	veliký
hmotnosti	hmotnost	k1gFnSc2	hmotnost
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
játra	játra	k1gNnPc1	játra
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
období	období	k1gNnSc6	období
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
čolci	čolek	k1gMnPc1	čolek
těžší	těžký	k2eAgMnPc1d2	těžší
<g/>
.	.	kIx.	.
</s>
<s>
Páření	páření	k1gNnSc1	páření
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
neznečistěných	znečistěný	k2eNgFnPc6d1	znečistěný
stojatých	stojatý	k2eAgFnPc6d1	stojatá
či	či	k8xC	či
mírně	mírně	k6eAd1	mírně
průtočných	průtočný	k2eAgFnPc6d1	průtočná
vodách	voda	k1gFnPc6	voda
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
v	v	k7c6	v
uměle	uměle	k6eAd1	uměle
vytvořených	vytvořený	k2eAgNnPc6d1	vytvořené
prostředích	prostředí	k1gNnPc6	prostředí
s	s	k7c7	s
vegetačním	vegetační	k2eAgInSc7d1	vegetační
krytem	kryt	k1gInSc7	kryt
<g/>
,	,	kIx,	,
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
místem	místo	k1gNnSc7	místo
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
pískovny	pískovna	k1gFnPc1	pískovna
či	či	k8xC	či
menší	malý	k2eAgInPc1d2	menší
rybníky	rybník	k1gInPc1	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Preferovaná	preferovaný	k2eAgFnSc1d1	preferovaná
hloubka	hloubka	k1gFnSc1	hloubka
u	u	k7c2	u
stojatých	stojatý	k2eAgFnPc2d1	stojatá
vod	voda	k1gFnPc2	voda
činí	činit	k5eAaImIp3nS	činit
40	[number]	k4	40
až	až	k9	až
80	[number]	k4	80
cm	cm	kA	cm
<g/>
,	,	kIx,	,
u	u	k7c2	u
průtočných	průtočný	k2eAgMnPc2d1	průtočný
pak	pak	k6eAd1	pak
minimálně	minimálně	k6eAd1	minimálně
20	[number]	k4	20
cm	cm	kA	cm
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
hrozícího	hrozící	k2eAgNnSc2d1	hrozící
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
ukryli	ukrýt	k5eAaPmAgMnP	ukrýt
v	v	k7c6	v
hlubině	hlubina	k1gFnSc6	hlubina
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
samotným	samotný	k2eAgNnSc7d1	samotné
pářením	páření	k1gNnSc7	páření
čolci	čolek	k1gMnPc1	čolek
provádějí	provádět	k5eAaImIp3nP	provádět
složité	složitý	k2eAgInPc4d1	složitý
svatební	svatební	k2eAgInPc4d1	svatební
tance	tanec	k1gInPc4	tanec
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
samice	samice	k1gFnSc1	samice
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
ocasu	ocas	k1gInSc2	ocas
samečka	sameček	k1gMnSc2	sameček
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
následně	následně	k6eAd1	následně
odloží	odložit	k5eAaPmIp3nS	odložit
své	svůj	k3xOyFgFnPc4	svůj
spermie	spermie	k1gFnPc4	spermie
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
zvednutím	zvednutí	k1gNnSc7	zvednutí
kloaky	kloaka	k1gFnSc2	kloaka
oplodní	oplodnit	k5eAaPmIp3nP	oplodnit
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
naklade	naklást	k5eAaPmIp3nS	naklást
asi	asi	k9	asi
250	[number]	k4	250
vajíček	vajíčko	k1gNnPc2	vajíčko
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
je	být	k5eAaImIp3nS	být
velké	velký	k2eAgNnSc4d1	velké
4,9	[number]	k4	4,9
<g/>
−	−	k?	−
<g/>
5,6	[number]	k4	5,6
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
ukládá	ukládat	k5eAaImIp3nS	ukládat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c4	na
lodyhy	lodyha	k1gFnPc4	lodyha
listů	list	k1gInPc2	list
či	či	k8xC	či
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
předměty	předmět	k1gInPc4	předmět
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
průtočných	průtočný	k2eAgFnPc6d1	průtočná
vodách	voda	k1gFnPc6	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
za	za	k7c4	za
14	[number]	k4	14
dní	den	k1gInPc2	den
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
larvy	larva	k1gFnPc1	larva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
letální	letální	k2eAgFnSc2d1	letální
mutace	mutace	k1gFnSc2	mutace
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
chromozomu	chromozom	k1gInSc6	chromozom
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
polovina	polovina	k1gFnSc1	polovina
snůšky	snůška	k1gFnSc2	snůška
zničí	zničit	k5eAaPmIp3nS	zničit
ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
<g/>
Larva	larva	k1gFnSc1	larva
měří	měřit	k5eAaImIp3nS	měřit
asi	asi	k9	asi
8,7	[number]	k4	8,7
až	až	k9	až
12	[number]	k4	12
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
ploutevní	ploutevní	k2eAgInSc1d1	ploutevní
lem	lem	k1gInSc1	lem
s	s	k7c7	s
šedočernými	šedočerný	k2eAgFnPc7d1	šedočerná
až	až	k8xS	až
černými	černý	k2eAgFnPc7d1	černá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
celého	celý	k2eAgNnSc2d1	celé
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
šedožluté	šedožlutý	k2eAgNnSc1d1	šedožluté
až	až	k6eAd1	až
běložluté	běložlutý	k2eAgNnSc1d1	běložluté
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
boku	bok	k1gInSc6	bok
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
obvykle	obvykle	k6eAd1	obvykle
táhne	táhnout	k5eAaImIp3nS	táhnout
skvrna	skvrna	k1gFnSc1	skvrna
oranžové	oranžový	k2eAgFnSc2d1	oranžová
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
špinavě	špinavě	k6eAd1	špinavě
žluté	žlutý	k2eAgFnPc4d1	žlutá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
zasahovat	zasahovat	k5eAaImF	zasahovat
až	až	k9	až
na	na	k7c4	na
břicho	břicho	k1gNnSc4	břicho
<g/>
;	;	kIx,	;
pomocí	pomocí	k7c2	pomocí
tohoto	tento	k3xDgInSc2	tento
znaku	znak	k1gInSc2	znak
lze	lze	k6eAd1	lze
larvu	larva	k1gFnSc4	larva
čolka	čolek	k1gMnSc2	čolek
dravého	dravý	k2eAgMnSc2d1	dravý
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
larev	larva	k1gFnPc2	larva
ostatních	ostatní	k2eAgMnPc2d1	ostatní
čolků	čolek	k1gMnPc2	čolek
–	–	k?	–
dunajského	dunajský	k2eAgNnSc2d1	Dunajské
a	a	k8xC	a
velkého	velký	k2eAgNnSc2d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
larvy	larva	k1gFnPc1	larva
proměňují	proměňovat	k5eAaImIp3nP	proměňovat
v	v	k7c4	v
čolky	čolek	k1gMnPc4	čolek
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
v	v	k7c6	v
3,6	[number]	k4	3,6
letech	léto	k1gNnPc6	léto
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
±	±	k?	±
0,1	[number]	k4	0,1
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
v	v	k7c6	v
3,8	[number]	k4	3,8
letech	léto	k1gNnPc6	léto
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
±	±	k?	±
0,1	[number]	k4	0,1
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dlouhověký	dlouhověký	k2eAgInSc1d1	dlouhověký
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
věku	věk	k1gInSc2	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pro	pro	k7c4	pro
čolka	čolek	k1gMnSc4	čolek
dravého	dravý	k2eAgNnSc2d1	dravé
představuje	představovat	k5eAaImIp3nS	představovat
ničení	ničení	k1gNnSc4	ničení
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
citlivý	citlivý	k2eAgInSc1d1	citlivý
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
přesouvat	přesouvat	k5eAaImF	přesouvat
na	na	k7c4	na
větší	veliký	k2eAgFnPc4d2	veliký
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
a	a	k8xC	a
louky	louka	k1gFnPc1	louka
jsou	být	k5eAaImIp3nP	být
odvodňovány	odvodňovat	k5eAaImNgFnP	odvodňovat
či	či	k8xC	či
přeměněny	přeměnit	k5eAaPmNgFnP	přeměnit
na	na	k7c4	na
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Čolci	čolek	k1gMnPc1	čolek
přicházejí	přicházet	k5eAaImIp3nP	přicházet
o	o	k7c4	o
vodní	vodní	k2eAgFnPc4d1	vodní
plochy	plocha	k1gFnPc4	plocha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
často	často	k6eAd1	často
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
znehodnocení	znehodnocení	k1gNnSc3	znehodnocení
ploch	plocha	k1gFnPc2	plocha
dochází	docházet	k5eAaImIp3nS	docházet
například	například	k6eAd1	například
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
eutrofizace	eutrofizace	k1gFnSc2	eutrofizace
nebo	nebo	k8xC	nebo
desikace	desikace	k1gFnSc2	desikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
turismem	turismus	k1gInSc7	turismus
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
maďarského	maďarský	k2eAgInSc2d1	maďarský
skanzenu	skanzen	k1gInSc2	skanzen
Népi	Nép	k1gFnSc2	Nép
műemlékegyüttes	műemlékegyüttesa	k1gFnPc2	műemlékegyüttesa
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
vysazování	vysazování	k1gNnSc1	vysazování
dravých	dravý	k2eAgFnPc2d1	dravá
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
čolky	čolek	k1gMnPc7	čolek
či	či	k8xC	či
jejich	jejich	k3xOp3gFnPc7	jejich
larvami	larva	k1gFnPc7	larva
živí	živit	k5eAaImIp3nS	živit
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc1	Ryby
živící	živící	k2eAgFnPc1d1	živící
se	s	k7c7	s
planktonem	plankton	k1gInSc7	plankton
čolky	čolek	k1gMnPc7	čolek
obírají	obírat	k5eAaImIp3nP	obírat
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Stavy	stav	k1gInPc1	stav
druhu	druh	k1gInSc2	druh
snižuje	snižovat	k5eAaImIp3nS	snižovat
také	také	k9	také
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
autoprovoz	autoprovoz	k1gInSc1	autoprovoz
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
obojživelníky	obojživelník	k1gMnPc7	obojživelník
žijícími	žijící	k2eAgMnPc7d1	žijící
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
patří	patřit	k5eAaImIp3nS	patřit
čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
k	k	k7c3	k
zástupcům	zástupce	k1gMnPc3	zástupce
s	s	k7c7	s
nejvíce	hodně	k6eAd3	hodně
klesající	klesající	k2eAgFnSc7d1	klesající
populací	populace	k1gFnSc7	populace
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
však	však	k9	však
počet	počet	k1gInSc1	počet
jedinců	jedinec	k1gMnPc2	jedinec
velký	velký	k2eAgMnSc1d1	velký
a	a	k8xC	a
druh	druh	k1gInSc1	druh
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
se	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
stanovištím	stanoviště	k1gNnPc3	stanoviště
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
svaz	svaz	k1gInSc1	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
IUCN	IUCN	kA	IUCN
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc4d1	dotčený
<g/>
.	.	kIx.	.
</s>
<s>
Chráněn	chráněn	k2eAgMnSc1d1	chráněn
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
Směrnice	směrnice	k1gFnSc2	směrnice
o	o	k7c6	o
stanovištích	stanoviště	k1gNnPc6	stanoviště
a	a	k8xC	a
zapsán	zapsán	k2eAgMnSc1d1	zapsán
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
příloze	příloha	k1gFnSc6	příloha
Úmluvy	úmluva	k1gFnSc2	úmluva
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
evropských	evropský	k2eAgFnPc2d1	Evropská
planě	planě	k6eAd1	planě
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
přírodních	přírodní	k2eAgNnPc2d1	přírodní
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejvzácnější	vzácný	k2eAgMnPc4d3	nejvzácnější
obojživelníky	obojživelník	k1gMnPc4	obojživelník
a	a	k8xC	a
Červený	červený	k2eAgInSc1d1	červený
seznam	seznam	k1gInSc1	seznam
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jej	on	k3xPp3gMnSc4	on
vede	vést	k5eAaImIp3nS	vést
jako	jako	k8xC	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
zachování	zachování	k1gNnSc4	zachování
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
ochrana	ochrana	k1gFnSc1	ochrana
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
druh	druh	k1gInSc1	druh
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytváření	vytváření	k1gNnSc1	vytváření
nových	nový	k2eAgFnPc2d1	nová
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
.	.	kIx.	.
<g/>
Čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
chráněných	chráněný	k2eAgFnPc2d1	chráněná
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ZWACH	ZWACH	kA	ZWACH
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Obojživelníci	obojživelník	k1gMnPc1	obojživelník
a	a	k8xC	a
plazi	plaz	k1gMnPc1	plaz
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
496	[number]	k4	496
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
2509	[number]	k4	2509
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
87	[number]	k4	87
<g/>
−	−	k?	−
<g/>
92	[number]	k4	92
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čolek	čolek	k1gMnSc1	čolek
dravý	dravý	k2eAgMnSc1d1	dravý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Triturus	Triturus	k1gInSc1	Triturus
carnifex	carnifex	k1gInSc1	carnifex
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
WHITTAKER	WHITTAKER	kA	WHITTAKER
<g/>
,	,	kIx,	,
Kellie	Kellie	k1gFnSc1	Kellie
<g/>
;	;	kIx,	;
TSAI	TSAI	kA	TSAI
<g/>
,	,	kIx,	,
Gary	Gary	k1gInPc7	Gary
<g/>
.	.	kIx.	.
</s>
<s>
Triturus	Triturus	k1gInSc1	Triturus
carnifex	carnifex	k1gInSc1	carnifex
<g/>
.	.	kIx.	.
amphibiaweb	amphibiawba	k1gFnPc2	amphibiawba
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
University	universita	k1gFnPc1	universita
of	of	k?	of
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
,	,	kIx,	,
California	Californium	k1gNnSc2	Californium
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
