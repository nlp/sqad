<s>
Země	země	k1gFnSc1	země
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
plochu	plocha	k1gFnSc4	plocha
1566500	[number]	k4	1566500
km2	km2	k4	km2
(	(	kIx(	(
<g/>
dvacetkrát	dvacetkrát	k6eAd1	dvacetkrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
pouze	pouze	k6eAd1	pouze
2,7	[number]	k4	2,7
milionům	milion	k4xCgInPc3	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
státem	stát	k1gInSc7	stát
s	s	k7c7	s
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
hustotou	hustota	k1gFnSc7	hustota
zalidnění	zalidnění	k1gNnPc2	zalidnění
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
