<s>
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Stát	stát	k5eAaPmF	stát
Mongolsko	Mongolsko	k1gNnSc4	Mongolsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
hraničící	hraničící	k2eAgFnSc6d1	hraničící
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Čínskou	čínský	k2eAgFnSc7d1	čínská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
plochu	plocha	k1gFnSc4	plocha
1566500	[number]	k4	1566500
km2	km2	k4	km2
(	(	kIx(	(
<g/>
dvacetkrát	dvacetkrát	k6eAd1	dvacetkrát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
pouze	pouze	k6eAd1	pouze
2,7	[number]	k4	2,7
milionům	milion	k4xCgInPc3	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
státem	stát	k1gInSc7	stát
s	s	k7c7	s
nejnižší	nízký	k2eAgFnSc7d3	nejnižší
hustotou	hustota	k1gFnSc7	hustota
zalidnění	zalidnění	k1gNnPc2	zalidnění
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>

