<s>
Kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgInSc3	tento
periodickému	periodický	k2eAgInSc3d1	periodický
ději	děj	k1gInSc3	děj
(	(	kIx(	(
<g/>
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
k	k	k7c3	k
významné	významný	k2eAgFnSc3d1	významná
změně	změna	k1gFnSc3	změna
tlaku	tlak	k1gInSc2	tlak
během	během	k7c2	během
roku	rok	k1gInSc2	rok
až	až	k9	až
o	o	k7c4	o
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
