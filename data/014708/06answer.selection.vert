<s>
Cer	cer	k1gInSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Ce	Ce	kA
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Cerium	Cerium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
šedavě	šedavě	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
vnitřně	vnitřně	k6eAd1
přechodný	přechodný	k2eAgInSc1d1
kovový	kovový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
člen	člen	k1gInSc1
skupiny	skupina	k1gFnSc2
lanthanoidů	lanthanoid	k1gInPc2
<g/>
.	.	kIx.
</s>