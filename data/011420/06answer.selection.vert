<s>
Komín	Komín	k1gInSc1	Komín
nebo	nebo	k8xC	nebo
také	také	k9	také
spalinová	spalinový	k2eAgFnSc1d1	spalinová
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
svislá	svislý	k2eAgFnSc1d1	svislá
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
stavby	stavba	k1gFnSc2	stavba
či	či	k8xC	či
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
systém	systém	k1gInSc1	systém
sloužící	sloužící	k2eAgInSc1d1	sloužící
pro	pro	k7c4	pro
odvod	odvod	k1gInSc4	odvod
spalin	spaliny	k1gFnPc2	spaliny
z	z	k7c2	z
topeniště	topeniště	k1gNnSc2	topeniště
<g/>
.	.	kIx.	.
</s>
