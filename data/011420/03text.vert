<p>
<s>
Komín	Komín	k1gInSc1	Komín
nebo	nebo	k8xC	nebo
také	také	k9	také
spalinová	spalinový	k2eAgFnSc1d1	spalinová
cesta	cesta	k1gFnSc1	cesta
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
svislá	svislý	k2eAgFnSc1d1	svislá
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
stavby	stavba	k1gFnSc2	stavba
či	či	k8xC	či
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
systém	systém	k1gInSc1	systém
sloužící	sloužící	k2eAgInSc1d1	sloužící
pro	pro	k7c4	pro
odvod	odvod	k1gInSc4	odvod
spalin	spaliny	k1gFnPc2	spaliny
z	z	k7c2	z
topeniště	topeniště	k1gNnSc2	topeniště
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
spalinovou	spalinový	k2eAgFnSc4d1	spalinová
cestu	cesta	k1gFnSc4	cesta
se	se	k3xPyFc4	se
nepovažuje	považovat	k5eNaImIp3nS	považovat
odvod	odvod	k1gInSc1	odvod
spalin	spaliny	k1gFnPc2	spaliny
z	z	k7c2	z
lokálních	lokální	k2eAgNnPc2d1	lokální
podokenních	podokenní	k2eAgNnPc2d1	podokenní
topidel	topidlo	k1gNnPc2	topidlo
o	o	k7c6	o
jmenovitém	jmenovitý	k2eAgInSc6d1	jmenovitý
výkonu	výkon	k1gInSc6	výkon
do	do	k7c2	do
7	[number]	k4	7
kW	kW	kA	kW
s	s	k7c7	s
vývodem	vývod	k1gInSc7	vývod
přes	přes	k7c4	přes
fasádu	fasáda	k1gFnSc4	fasáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Předstupněm	předstupeň	k1gInSc7	předstupeň
komína	komín	k1gInSc2	komín
byl	být	k5eAaImAgInS	být
kouřový	kouřový	k2eAgInSc1d1	kouřový
otvor	otvor	k1gInSc1	otvor
ve	v	k7c6	v
střeše	střecha	k1gFnSc6	střecha
nebo	nebo	k8xC	nebo
u	u	k7c2	u
střechy	střecha	k1gFnSc2	střecha
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
stříškou	stříška	k1gFnSc7	stříška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarším	starý	k2eAgNnSc7d3	nejstarší
zařízením	zařízení	k1gNnSc7	zařízení
sloužícím	sloužící	k2eAgNnSc7d1	sloužící
k	k	k7c3	k
odvodu	odvod	k1gInSc3	odvod
kouře	kouř	k1gInSc2	kouř
ven	ven	k6eAd1	ven
z	z	k7c2	z
domu	dům	k1gInSc2	dům
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
jizby	jizba	k1gFnSc2	jizba
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
tzv.	tzv.	kA	tzv.
dymník	dymník	k1gInSc4	dymník
odvádějící	odvádějící	k2eAgInSc1d1	odvádějící
kouř	kouř	k1gInSc1	kouř
do	do	k7c2	do
podkrovního	podkrovní	k2eAgInSc2d1	podkrovní
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevují	objevovat	k5eAaImIp3nP	objevovat
již	již	k6eAd1	již
komíny	komín	k1gInPc4	komín
sahající	sahající	k2eAgInPc4d1	sahající
až	až	k9	až
nad	nad	k7c4	nad
samotnou	samotný	k2eAgFnSc4d1	samotná
střechu	střecha	k1gFnSc4	střecha
a	a	k8xC	a
odvádějící	odvádějící	k2eAgInSc4d1	odvádějící
tak	tak	k9	tak
kouř	kouř	k1gInSc4	kouř
zcela	zcela	k6eAd1	zcela
mimo	mimo	k7c4	mimo
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
komíny	komín	k1gInPc1	komín
byly	být	k5eAaImAgInP	být
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
či	či	k8xC	či
hlinitodřevěné	hlinitodřevěný	k2eAgInPc1d1	hlinitodřevěný
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
protipožární	protipožární	k2eAgFnSc2d1	protipožární
prevence	prevence	k1gFnSc2	prevence
přecházelo	přecházet	k5eAaImAgNnS	přecházet
na	na	k7c4	na
komíny	komín	k1gInPc4	komín
zděné	zděný	k2eAgInPc4d1	zděný
a	a	k8xC	a
tedy	tedy	k9	tedy
nehořlavé	hořlavý	k2eNgFnPc1d1	nehořlavá
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
průduchy	průduch	k1gInPc1	průduch
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
svedeny	svést	k5eAaPmNgInP	svést
až	až	k6eAd1	až
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
a	a	k8xC	a
využívaly	využívat	k5eAaImAgInP	využívat
tak	tak	k9	tak
k	k	k7c3	k
odvodu	odvod	k1gInSc3	odvod
kouře	kouř	k1gInSc2	kouř
umělého	umělý	k2eAgInSc2d1	umělý
tahu	tah	k1gInSc2	tah
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Funkce	funkce	k1gFnSc1	funkce
komínu	komín	k1gInSc2	komín
-	-	kIx~	-
spalinové	spalinový	k2eAgFnPc1d1	spalinová
cesty	cesta	k1gFnPc1	cesta
==	==	k?	==
</s>
</p>
<p>
<s>
Komín	Komín	k1gInSc1	Komín
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
největší	veliký	k2eAgFnSc6d3	veliký
míře	míra	k1gFnSc6	míra
využíván	využívat	k5eAaImNgInS	využívat
k	k	k7c3	k
odvádění	odvádění	k1gNnSc3	odvádění
spalin	spaliny	k1gFnPc2	spaliny
z	z	k7c2	z
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Napojují	napojovat	k5eAaImIp3nP	napojovat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
kamna	kamna	k1gNnPc4	kamna
<g/>
,	,	kIx,	,
pece	pec	k1gFnPc4	pec
<g/>
,	,	kIx,	,
krby	krb	k1gInPc4	krb
nebo	nebo	k8xC	nebo
kotle	kotel	k1gInPc4	kotel
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
spotřebičů	spotřebič	k1gInPc2	spotřebič
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
paliv	palivo	k1gNnPc2	palivo
u	u	k7c2	u
kterých	který	k3yIgMnPc2	který
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
odvod	odvod	k1gInSc4	odvod
spalin	spaliny	k1gFnPc2	spaliny
z	z	k7c2	z
topeniště	topeniště	k1gNnSc2	topeniště
<g/>
.	.	kIx.	.
</s>
<s>
Horké	Horké	k2eAgFnPc1d1	Horké
spaliny	spaliny	k1gFnPc1	spaliny
v	v	k7c6	v
komíně	komín	k1gInSc6	komín
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnSc4d2	nižší
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
venkovní	venkovní	k2eAgInSc4d1	venkovní
neohřátý	ohřátý	k2eNgInSc4d1	neohřátý
vzduch	vzduch	k1gInSc4	vzduch
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
vytlačovány	vytlačován	k2eAgFnPc1d1	vytlačována
a	a	k8xC	a
stoupají	stoupat	k5eAaImIp3nP	stoupat
komínem	komín	k1gInSc7	komín
vzhůru	vzhůru	k6eAd1	vzhůru
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
je	být	k5eAaImIp3nS	být
vytlačován	vytlačován	k2eAgMnSc1d1	vytlačován
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nasáván	nasáván	k2eAgMnSc1d1	nasáván
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
vzduch	vzduch	k1gInSc1	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tlaku	tlak	k1gInSc3	tlak
<g/>
,	,	kIx,	,
vyvolanému	vyvolaný	k2eAgMnSc3d1	vyvolaný
rozdílem	rozdíl	k1gInSc7	rozdíl
hustot	hustota	k1gFnPc2	hustota
spalin	spaliny	k1gFnPc2	spaliny
a	a	k8xC	a
okolního	okolní	k2eAgInSc2d1	okolní
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
žene	hnát	k5eAaImIp3nS	hnát
vzduch	vzduch	k1gInSc4	vzduch
komínem	komín	k1gInSc7	komín
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
tah	tah	k1gInSc1	tah
komína	komín	k1gInSc2	komín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tah	tah	k1gInSc1	tah
komína	komín	k1gInSc2	komín
je	být	k5eAaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
kromě	kromě	k7c2	kromě
poměru	poměr	k1gInSc2	poměr
hustot	hustota	k1gFnPc2	hustota
ohřátých	ohřátý	k2eAgInPc2d1	ohřátý
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
i	i	k9	i
na	na	k7c6	na
objemu	objem	k1gInSc6	objem
spalin	spaliny	k1gFnPc2	spaliny
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jeho	jeho	k3xOp3gFnSc3	jeho
výšce	výška	k1gFnSc3	výška
a	a	k8xC	a
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
provozech	provoz	k1gInPc6	provoz
stavěny	stavěn	k2eAgInPc4d1	stavěn
vysoké	vysoký	k2eAgInPc4d1	vysoký
komíny	komín	k1gInPc4	komín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nemožné	možný	k2eNgNnSc1d1	nemožné
stavět	stavět	k5eAaImF	stavět
vysoký	vysoký	k2eAgInSc4d1	vysoký
komín	komín	k1gInSc4	komín
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
komín	komín	k1gInSc4	komín
odvádět	odvádět	k5eAaImF	odvádět
studené	studený	k2eAgInPc4d1	studený
plyny	plyn	k1gInPc4	plyn
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
montována	montován	k2eAgNnPc4d1	montováno
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
umělý	umělý	k2eAgInSc4d1	umělý
tah	tah	k1gInSc4	tah
–	–	k?	–
tahové	tahový	k2eAgInPc4d1	tahový
ventilátory	ventilátor	k1gInPc4	ventilátor
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dyšna	dyšna	k1gFnSc1	dyšna
parních	parní	k2eAgFnPc2d1	parní
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
nebo	nebo	k8xC	nebo
ventilátor	ventilátor	k1gInSc4	ventilátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Údržba	údržba	k1gFnSc1	údržba
-	-	kIx~	-
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
,	,	kIx,	,
čištění	čištění	k1gNnSc1	čištění
a	a	k8xC	a
revize	revize	k1gFnSc1	revize
komínů	komín	k1gInPc2	komín
(	(	kIx(	(
<g/>
spalinových	spalinový	k2eAgFnPc2d1	spalinová
cest	cesta	k1gFnPc2	cesta
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Čištění	čištění	k1gNnSc2	čištění
===	===	k?	===
</s>
</p>
<p>
<s>
Čištění	čištění	k1gNnSc1	čištění
komínů	komín	k1gInPc2	komín
provádí	provádět	k5eAaImIp3nS	provádět
pouze	pouze	k6eAd1	pouze
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
osoba	osoba	k1gFnSc1	osoba
podle	podle	k7c2	podle
Zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
133	[number]	k4	133
<g/>
/	/	kIx~	/
<g/>
1985	[number]	k4	1985
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
a	a	k8xC	a
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
tak	tak	k9	tak
že	že	k8xS	že
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
odstraní	odstranit	k5eAaPmIp3nS	odstranit
saze	saze	k1gFnSc1	saze
ze	z	k7c2	z
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
komponentů	komponent	k1gInPc2	komponent
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
vyberou	vybrat	k5eAaPmIp3nP	vybrat
pevné	pevný	k2eAgFnPc4d1	pevná
části	část	k1gFnPc4	část
spalin	spaliny	k1gFnPc2	spaliny
z	z	k7c2	z
půdice	půdice	k?	půdice
(	(	kIx(	(
<g/>
dno	dno	k1gNnSc4	dno
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyberou	vybrat	k5eAaPmIp3nP	vybrat
se	se	k3xPyFc4	se
také	také	k9	také
kondenzáty	kondenzát	k1gInPc1	kondenzát
ze	z	k7c2	z
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Čištění	čištění	k1gNnSc1	čištění
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
napojené	napojený	k2eAgFnSc2d1	napojená
na	na	k7c4	na
topidlo	topidlo	k1gNnSc4	topidlo
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
do	do	k7c2	do
50	[number]	k4	50
kW	kW	kA	kW
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rodinné	rodinný	k2eAgInPc1d1	rodinný
domky	domek	k1gInPc1	domek
<g/>
)	)	kIx)	)
a	a	k8xC	a
náhradní	náhradní	k2eAgInPc4d1	náhradní
zdroje	zdroj	k1gInPc4	zdroj
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
dieselagregáty	dieselagregát	k1gInPc1	dieselagregát
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
provést	provést	k5eAaPmF	provést
sám	sám	k3xTgMnSc1	sám
majitel	majitel	k1gMnSc1	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Čištění	čištění	k1gNnSc1	čištění
je	být	k5eAaImIp3nS	být
také	také	k9	také
vypálení	vypálení	k1gNnSc4	vypálení
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
odolné	odolný	k2eAgFnSc2d1	odolná
proti	proti	k7c3	proti
vyhoření	vyhoření	k1gNnSc3	vyhoření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
může	moct	k5eAaImIp3nS	moct
provést	provést	k5eAaPmF	provést
jen	jen	k9	jen
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Frekvence	frekvence	k1gFnSc1	frekvence
čištění	čištění	k1gNnSc2	čištění
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
frekvence	frekvence	k1gFnSc1	frekvence
provozu	provoz	k1gInSc2	provoz
(	(	kIx(	(
<g/>
celoroční	celoroční	k2eAgInPc4d1	celoroční
nebo	nebo	k8xC	nebo
sezónní	sezónní	k2eAgInPc4d1	sezónní
<g/>
)	)	kIx)	)
a	a	k8xC	a
podle	podle	k7c2	podle
výkonu	výkon	k1gInSc2	výkon
topidla	topidlo	k1gNnSc2	topidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
pro	pro	k7c4	pro
rodinnou	rodinný	k2eAgFnSc4d1	rodinná
rekreaci	rekreace	k1gFnSc4	rekreace
se	se	k3xPyFc4	se
čištění	čištění	k1gNnSc1	čištění
provádí	provádět	k5eAaImIp3nS	provádět
nejméně	málo	k6eAd3	málo
jedenkrát	jedenkrát	k6eAd1	jedenkrát
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
kontrola	kontrola	k1gFnSc1	kontrola
nejméně	málo	k6eAd3	málo
jedenkrát	jedenkrát	k6eAd1	jedenkrát
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
čištění	čištění	k1gNnSc1	čištění
neprovede	provést	k5eNaPmIp3nS	provést
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
zapálení	zapálení	k1gNnSc3	zapálení
sazí	saze	k1gFnPc2	saze
a	a	k8xC	a
následnému	následný	k2eAgInSc3d1	následný
komínovému	komínový	k2eAgInSc3d1	komínový
požáru	požár	k1gInSc3	požár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kontrola	kontrola	k1gFnSc1	kontrola
===	===	k?	===
</s>
</p>
<p>
<s>
Kontrolu	kontrola	k1gFnSc4	kontrola
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
provést	provést	k5eAaPmF	provést
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
133	[number]	k4	133
<g/>
/	/	kIx~	/
<g/>
1985	[number]	k4	1985
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
a	a	k8xC	a
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
minimálně	minimálně	k6eAd1	minimálně
jednou	jeden	k4xCgFnSc7	jeden
ročně	ročně	k6eAd1	ročně
od	od	k7c2	od
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
spotřebičů	spotřebič	k1gInPc2	spotřebič
<g/>
:	:	kIx,	:
krby	krb	k1gInPc1	krb
<g/>
,	,	kIx,	,
kamna	kamna	k1gNnPc1	kamna
<g/>
,	,	kIx,	,
ohřívače	ohřívač	k1gInPc1	ohřívač
vody	voda	k1gFnSc2	voda
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
kotle	kotel	k1gInPc1	kotel
<g/>
,	,	kIx,	,
sálavá	sálavý	k2eAgNnPc1d1	sálavé
topidla	topidlo	k1gNnPc1	topidlo
<g/>
,	,	kIx,	,
ohřívače	ohřívač	k1gInPc1	ohřívač
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
všechny	všechen	k3xTgInPc4	všechen
spotřebiče	spotřebič	k1gInPc4	spotřebič
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
spalují	spalovat	k5eAaImIp3nP	spalovat
plynná	plynný	k2eAgNnPc1d1	plynné
<g/>
,	,	kIx,	,
kapalná	kapalný	k2eAgNnPc1d1	kapalné
nebo	nebo	k8xC	nebo
pevná	pevný	k2eAgNnPc1d1	pevné
paliva	palivo	k1gNnPc1	palivo
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
připojena	připojit	k5eAaPmNgNnP	připojit
na	na	k7c4	na
spalinovou	spalinový	k2eAgFnSc4d1	spalinová
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
i	i	k9	i
tzv.	tzv.	kA	tzv.
turbo	turba	k1gFnSc5	turba
kotle	kotel	k1gInPc1	kotel
a	a	k8xC	a
kondenzační	kondenzační	k2eAgInPc1d1	kondenzační
kotle	kotel	k1gInPc1	kotel
vyústěné	vyústěný	k2eAgInPc1d1	vyústěný
přes	přes	k7c4	přes
fasádu	fasáda	k1gFnSc4	fasáda
budovy	budova	k1gFnSc2	budova
nebo	nebo	k8xC	nebo
vedené	vedený	k2eAgFnSc2d1	vedená
přes	přes	k7c4	přes
střechu	střecha	k1gFnSc4	střecha
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
spalinová	spalinový	k2eAgFnSc1d1	spalinová
cesta	cesta	k1gFnSc1	cesta
napojená	napojený	k2eAgFnSc1d1	napojená
na	na	k7c4	na
topidlo	topidlo	k1gNnSc4	topidlo
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
nad	nad	k7c7	nad
50	[number]	k4	50
kW	kW	kA	kW
(	(	kIx(	(
<g/>
např.	např.	kA	např.
topidlo	topidlo	k1gNnSc4	topidlo
teplárny	teplárna	k1gFnSc2	teplárna
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
desítek	desítka	k1gFnPc2	desítka
MW	MW	kA	MW
<g/>
)	)	kIx)	)
a	a	k8xC	a
využívá	využívat	k5eAaImIp3nS	využívat
pevný	pevný	k2eAgInSc4d1	pevný
druh	druh	k1gInSc4	druh
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kontrola	kontrola	k1gFnSc1	kontrola
provádí	provádět	k5eAaImIp3nS	provádět
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kontrolu	kontrola	k1gFnSc4	kontrola
může	moct	k5eAaImIp3nS	moct
provádět	provádět	k5eAaImF	provádět
pouze	pouze	k6eAd1	pouze
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
živnostenského	živnostenský	k2eAgNnSc2d1	živnostenské
oprávnění	oprávnění	k1gNnSc2	oprávnění
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
kominictví	kominictví	k1gNnSc2	kominictví
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kontrole	kontrola	k1gFnSc6	kontrola
kominíci	kominík	k1gMnPc1	kominík
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
spalinová	spalinový	k2eAgFnSc1d1	spalinová
cesta	cesta	k1gFnSc1	cesta
bezpečná	bezpečný	k2eAgFnSc1d1	bezpečná
<g/>
,	,	kIx,	,
neohrožuje	ohrožovat	k5eNaImIp3nS	ohrožovat
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
nařídí	nařídit	k5eAaPmIp3nS	nařídit
odstranění	odstranění	k1gNnSc1	odstranění
překážek	překážka	k1gFnPc2	překážka
a	a	k8xC	a
závad	závada	k1gFnPc2	závada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Revize	revize	k1gFnSc2	revize
===	===	k?	===
</s>
</p>
<p>
<s>
Revizi	revize	k1gFnSc4	revize
provádí	provádět	k5eAaImIp3nS	provádět
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
živnostenského	živnostenský	k2eAgNnSc2d1	živnostenské
oprávnění	oprávnění	k1gNnSc2	oprávnění
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
kominictví	kominictví	k1gNnSc2	kominictví
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
revizním	revizní	k2eAgNnSc7d1	revizní
technikem	technikum	k1gNnSc7	technikum
spalinových	spalinový	k2eAgFnPc2d1	spalinová
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Revize	revize	k1gFnSc1	revize
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
před	před	k7c7	před
uvedením	uvedení	k1gNnSc7	uvedení
nové	nový	k2eAgFnSc2d1	nová
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
nebo	nebo	k8xC	nebo
po	po	k7c6	po
každé	každý	k3xTgFnSc6	každý
stavební	stavební	k2eAgFnSc6d1	stavební
úpravě	úprava	k1gFnSc6	úprava
komínu	komín	k1gInSc2	komín
<g/>
,	,	kIx,	,
<g/>
při	při	k7c6	při
změně	změna	k1gFnSc6	změna
druhu	druh	k1gInSc2	druh
paliva	palivo	k1gNnSc2	palivo
připojeného	připojený	k2eAgInSc2d1	připojený
spotřebiče	spotřebič	k1gInSc2	spotřebič
paliv	palivo	k1gNnPc2	palivo
<g/>
,	,	kIx,	,
<g/>
před	před	k7c7	před
připojením	připojení	k1gNnSc7	připojení
spotřebiče	spotřebič	k1gInPc1	spotřebič
paliv	palivo	k1gNnPc2	palivo
do	do	k7c2	do
nepoužívané	používaný	k2eNgFnSc2d1	nepoužívaná
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
<g/>
před	před	k7c7	před
výměnou	výměna	k1gFnSc7	výměna
spotřebiče	spotřebič	k1gInSc2	spotřebič
paliv	palivo	k1gNnPc2	palivo
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
výměny	výměna	k1gFnSc2	výměna
spotřebiče	spotřebič	k1gInSc2	spotřebič
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
typu	typ	k1gInSc2	typ
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
provedení	provedení	k1gNnSc6	provedení
a	a	k8xC	a
výkonu	výkon	k1gInSc6	výkon
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
způsobilost	způsobilost	k1gFnSc4	způsobilost
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
je	být	k5eAaImIp3nS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c4	o
provedení	provedení	k1gNnSc4	provedení
čištění	čištění	k1gNnSc2	čištění
a	a	k8xC	a
kontroly	kontrola	k1gFnSc2	kontrola
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
<g/>
po	po	k7c6	po
komínovém	komínový	k2eAgInSc6d1	komínový
požáru	požár	k1gInSc6	požár
<g/>
,	,	kIx,	,
nebopři	nebopř	k1gFnSc6	nebopř
vzniku	vznik	k1gInSc2	vznik
trhlin	trhlina	k1gFnPc2	trhlina
u	u	k7c2	u
používané	používaný	k2eAgFnSc2d1	používaná
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
při	při	k7c6	při
důvodném	důvodný	k2eAgNnSc6d1	důvodné
podezření	podezření	k1gNnSc6	podezření
na	na	k7c4	na
výskyt	výskyt	k1gInSc4	výskyt
trhlin	trhlina	k1gFnPc2	trhlina
u	u	k7c2	u
používané	používaný	k2eAgFnSc2d1	používaná
spalinové	spalinový	k2eAgFnSc2d1	spalinová
<g />
.	.	kIx.	.
</s>
<s>
cesty	cesta	k1gFnPc1	cesta
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
kominík	kominík	k1gMnSc1	kominík
nebo	nebo	k8xC	nebo
revizní	revizní	k2eAgMnSc1d1	revizní
technik	technik	k1gMnSc1	technik
spalinových	spalinový	k2eAgFnPc2d1	spalinová
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zjistí	zjistit	k5eAaPmIp3nS	zjistit
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
bezprostředně	bezprostředně	k6eAd1	bezprostředně
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
život	život	k1gInSc4	život
nebo	nebo	k8xC	nebo
majetek	majetek	k1gInSc4	majetek
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
hlásit	hlásit	k5eAaImF	hlásit
tyto	tento	k3xDgFnPc4	tento
závady	závada	k1gFnPc4	závada
stavebnímu	stavební	k2eAgInSc3d1	stavební
úřadu	úřad	k1gInSc3	úřad
při	při	k7c6	při
technických	technický	k2eAgFnPc6d1	technická
závadách	závada	k1gFnPc6	závada
a	a	k8xC	a
orgánu	orgán	k1gInSc6	orgán
státního	státní	k2eAgInSc2d1	státní
požárního	požární	k2eAgInSc2d1	požární
dozoru	dozor	k1gInSc2	dozor
při	při	k7c6	při
nedodržení	nedodržení	k1gNnSc6	nedodržení
požární	požární	k2eAgFnSc2d1	požární
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
provedeném	provedený	k2eAgNnSc6d1	provedené
čištění	čištění	k1gNnSc6	čištění
<g/>
,	,	kIx,	,
kontrole	kontrola	k1gFnSc3	kontrola
nebo	nebo	k8xC	nebo
revizi	revize	k1gFnSc3	revize
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
čištění	čištění	k1gNnSc6	čištění
<g/>
,	,	kIx,	,
kontrole	kontrola	k1gFnSc3	kontrola
nebo	nebo	k8xC	nebo
revizi	revize	k1gFnSc3	revize
je	být	k5eAaImIp3nS	být
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
osoba	osoba	k1gFnSc1	osoba
předat	předat	k5eAaPmF	předat
objednateli	objednatel	k1gMnSc3	objednatel
písemnou	písemný	k2eAgFnSc4d1	písemná
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
provedeném	provedený	k2eAgNnSc6d1	provedené
čištění	čištění	k1gNnSc6	čištění
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
10	[number]	k4	10
pracovních	pracovní	k2eAgInPc2d1	pracovní
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
právnická	právnický	k2eAgFnSc1d1	právnická
nebo	nebo	k8xC	nebo
podnikající	podnikající	k2eAgFnSc1d1	podnikající
fyzická	fyzický	k2eAgFnSc1d1	fyzická
osoba	osoba	k1gFnSc1	osoba
vyčistí	vyčistit	k5eAaPmIp3nS	vyčistit
komín	komín	k1gInSc4	komín
svépomocí	svépomoc	k1gFnPc2	svépomoc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povinna	povinen	k2eAgFnSc1d1	povinna
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
učinit	učinit	k5eAaImF	učinit
písemný	písemný	k2eAgInSc4d1	písemný
záznam	záznam	k1gInSc4	záznam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pokuty	pokuta	k1gFnSc2	pokuta
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
majitel	majitel	k1gMnSc1	majitel
poruší	porušit	k5eAaPmIp3nS	porušit
zásady	zásada	k1gFnSc2	zásada
bezpečného	bezpečný	k2eAgInSc2d1	bezpečný
provozu	provoz	k1gInSc2	provoz
komínu	komín	k1gInSc2	komín
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gInSc4	on
provozuje	provozovat	k5eAaImIp3nS	provozovat
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
133	[number]	k4	133
<g/>
/	/	kIx~	/
<g/>
1985	[number]	k4	1985
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
pokutu	pokuta	k1gFnSc4	pokuta
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
10	[number]	k4	10
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
právnická	právnický	k2eAgFnSc1d1	právnická
nebo	nebo	k8xC	nebo
podnikající	podnikající	k2eAgFnSc1d1	podnikající
fyzická	fyzický	k2eAgFnSc1d1	fyzická
osoba	osoba	k1gFnSc1	osoba
provozuje	provozovat	k5eAaImIp3nS	provozovat
spalinovou	spalinový	k2eAgFnSc4d1	spalinová
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
133	[number]	k4	133
<g/>
/	/	kIx~	/
<g/>
1985	[number]	k4	1985
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
,	,	kIx,	,
<g/>
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
pokutu	pokuta	k1gFnSc4	pokuta
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
100	[number]	k4	100
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
osoba	osoba	k1gFnSc1	osoba
provede	provést	k5eAaPmIp3nS	provést
čištění	čištění	k1gNnSc4	čištění
<g/>
,	,	kIx,	,
kontrolu	kontrola	k1gFnSc4	kontrola
nebo	nebo	k8xC	nebo
revizi	revize	k1gFnSc4	revize
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
133	[number]	k4	133
<g/>
/	/	kIx~	/
<g/>
1985	[number]	k4	1985
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
nevydá	vydat	k5eNaPmIp3nS	vydat
písemnou	písemný	k2eAgFnSc4d1	písemná
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
provedeném	provedený	k2eAgNnSc6d1	provedené
čištění	čištění	k1gNnSc6	čištění
<g/>
,	,	kIx,	,
kontrole	kontrola	k1gFnSc3	kontrola
nebo	nebo	k8xC	nebo
revizi	revize	k1gFnSc3	revize
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
pokutu	pokuta	k1gFnSc4	pokuta
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
50	[number]	k4	50
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vložkování	Vložkování	k1gNnPc2	Vložkování
komínů	komín	k1gInPc2	komín
==	==	k?	==
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
komín	komín	k1gInSc1	komín
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
životnost	životnost	k1gFnSc4	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
komínový	komínový	k2eAgInSc1d1	komínový
průduch	průduch	k1gInSc1	průduch
z	z	k7c2	z
jakýchkoli	jakýkoli	k3yIgInPc2	jakýkoli
důvodů	důvod	k1gInPc2	důvod
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
platným	platný	k2eAgInPc3d1	platný
předpisům	předpis	k1gInPc3	předpis
<g/>
,	,	kIx,	,
přestavba	přestavba	k1gFnSc1	přestavba
komínu	komín	k1gInSc2	komín
je	být	k5eAaImIp3nS	být
nevyhnutelná	vyhnutelný	k2eNgFnSc1d1	nevyhnutelná
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
komínů	komín	k1gInPc2	komín
pomocí	pomocí	k7c2	pomocí
vložkování	vložkování	k1gNnSc2	vložkování
je	být	k5eAaImIp3nS	být
šetrný	šetrný	k2eAgInSc4d1	šetrný
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
lze	lze	k6eAd1	lze
prodloužit	prodloužit	k5eAaPmF	prodloužit
životnost	životnost	k1gFnSc4	životnost
každého	každý	k3xTgInSc2	každý
komínu	komín	k1gInSc2	komín
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
předchází	předcházet	k5eAaImIp3nS	předcházet
vložkování	vložkování	k1gNnSc4	vložkování
i	i	k8xC	i
frézování	frézování	k1gNnSc4	frézování
komína	komín	k1gInSc2	komín
v	v	k7c6	v
případě	případ	k1gInSc6	případ
spotřebičů	spotřebič	k1gInPc2	spotřebič
na	na	k7c4	na
pevná	pevný	k2eAgNnPc4d1	pevné
paliva	palivo	k1gNnPc4	palivo
především	především	k6eAd1	především
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
spotřebičů	spotřebič	k1gInPc2	spotřebič
na	na	k7c4	na
plyn	plyn	k1gInSc4	plyn
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
časté	častý	k2eAgNnSc1d1	časté
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Děje	dít	k5eAaImIp3nS	dít
se	se	k3xPyFc4	se
tak	tak	k9	tak
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
průduch	průduch	k1gInSc1	průduch
komína	komín	k1gInSc2	komín
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zvětšit	zvětšit	k5eAaPmF	zvětšit
a	a	k8xC	a
po	po	k7c6	po
vyfrézování	vyfrézování	k1gNnSc6	vyfrézování
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
komín	komín	k1gInSc4	komín
vyvložkovat	vyvložkovat	k5eAaBmF	vyvložkovat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
ztenčení	ztenčení	k1gNnSc2	ztenčení
cihelné	cihelný	k2eAgFnSc2d1	cihelná
vyzdívky	vyzdívka	k1gFnSc2	vyzdívka
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tímto	tento	k3xDgInSc7	tento
již	již	k6eAd1	již
neplní	plnit	k5eNaImIp3nP	plnit
funkci	funkce	k1gFnSc4	funkce
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgNnSc1d1	následné
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
vložkování	vložkování	k1gNnSc1	vložkování
pomocí	pomocí	k7c2	pomocí
vyhovujících	vyhovující	k2eAgInPc2d1	vyhovující
a	a	k8xC	a
bezpečných	bezpečný	k2eAgInPc2d1	bezpečný
materiálů	materiál	k1gInPc2	materiál
zajistí	zajistit	k5eAaPmIp3nS	zajistit
dobrý	dobrý	k2eAgInSc1d1	dobrý
tah	tah	k1gInSc1	tah
při	při	k7c6	při
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
druhu	druh	k1gInSc6	druh
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
odolnost	odolnost	k1gFnSc1	odolnost
vůči	vůči	k7c3	vůči
nasákavosti	nasákavost	k1gFnSc3	nasákavost
a	a	k8xC	a
vlhkosti	vlhkost	k1gFnSc3	vlhkost
<g/>
,	,	kIx,	,
čisté	čistý	k2eAgNnSc1d1	čisté
ovzduší	ovzduší	k1gNnSc1	ovzduší
bez	bez	k7c2	bez
kouřového	kouřový	k2eAgInSc2d1	kouřový
zápachu	zápach	k1gInSc2	zápach
spalin	spaliny	k1gFnPc2	spaliny
v	v	k7c6	v
domě	dům	k1gInSc6	dům
a	a	k8xC	a
také	také	k9	také
bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
připojení	připojení	k1gNnSc1	připojení
moderního	moderní	k2eAgInSc2d1	moderní
spotřebiče	spotřebič	k1gInSc2	spotřebič
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
úprava	úprava	k1gFnSc1	úprava
nebo	nebo	k8xC	nebo
zásah	zásah	k1gInSc1	zásah
do	do	k7c2	do
komína	komín	k1gInSc2	komín
-	-	kIx~	-
spalinové	spalinový	k2eAgFnPc1d1	spalinová
cesty	cesta	k1gFnPc1	cesta
musí	muset	k5eAaImIp3nP	muset
odpovídat	odpovídat	k5eAaImF	odpovídat
kominické	kominický	k2eAgFnSc3d1	kominická
normě	norma	k1gFnSc3	norma
ČSN	ČSN	kA	ČSN
73	[number]	k4	73
4200	[number]	k4	4200
a	a	k8xC	a
ČSN	ČSN	kA	ČSN
73	[number]	k4	73
4201	[number]	k4	4201
a	a	k8xC	a
být	být	k5eAaImF	být
schváleno	schválit	k5eAaPmNgNnS	schválit
revizním	revizní	k2eAgNnSc7d1	revizní
technikem	technikum	k1gNnSc7	technikum
spalinových	spalinový	k2eAgFnPc2d1	spalinová
cest	cesta	k1gFnPc2	cesta
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
34	[number]	k4	34
<g/>
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
Sb	sb	kA	sb
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
komínu	komín	k1gInSc2	komín
-	-	kIx~	-
spalinové	spalinový	k2eAgFnPc1d1	spalinová
cesty	cesta	k1gFnPc1	cesta
==	==	k?	==
</s>
</p>
<p>
<s>
kouřovod	kouřovod	k1gInSc1	kouřovod
-	-	kIx~	-
průchozí	průchozí	k2eAgFnSc1d1	průchozí
část	část	k1gFnSc1	část
komínu	komín	k1gInSc2	komín
sloužící	sloužící	k1gFnSc2	sloužící
k	k	k7c3	k
odvodu	odvod	k1gInSc3	odvod
spalin	spaliny	k1gFnPc2	spaliny
z	z	k7c2	z
topidla	topidlo	k1gNnSc2	topidlo
<g/>
;	;	kIx,	;
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
čtvercový	čtvercový	k2eAgInSc4d1	čtvercový
<g/>
,	,	kIx,	,
obdélníkový	obdélníkový	k2eAgInSc4d1	obdélníkový
či	či	k8xC	či
kruhový	kruhový	k2eAgInSc4d1	kruhový
průřez	průřez	k1gInSc4	průřez
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
z	z	k7c2	z
plechu	plech	k1gInSc2	plech
nebo	nebo	k8xC	nebo
keramiky	keramika	k1gFnSc2	keramika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
šamot	šamot	k1gInSc1	šamot
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
komínový	komínový	k2eAgInSc1d1	komínový
plášť	plášť	k1gInSc1	plášť
nebo	nebo	k8xC	nebo
komínové	komínový	k2eAgNnSc1d1	komínové
těleso	těleso	k1gNnSc1	těleso
–	–	k?	–
vnější	vnější	k2eAgFnSc1d1	vnější
vrstva	vrstva	k1gFnSc1	vrstva
komínu	komín	k1gInSc2	komín
(	(	kIx(	(
<g/>
u	u	k7c2	u
rodinných	rodinný	k2eAgInPc2d1	rodinný
domků	domek	k1gInPc2	domek
zjm	zjm	k?	zjm
<g/>
.	.	kIx.	.
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
a	a	k8xC	a
malty	malta	k1gFnSc2	malta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
půdice	půdice	k?	půdice
–	–	k?	–
dno	dno	k1gNnSc4	dno
spalinové	spalinový	k2eAgFnSc2d1	spalinová
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
odebírají	odebírat	k5eAaImIp3nP	odebírat
pevné	pevný	k2eAgFnSc3d1	pevná
části	část	k1gFnSc3	část
spalin	spaliny	k1gFnPc2	spaliny
</s>
</p>
<p>
<s>
vybírací	vybírací	k2eAgInSc1d1	vybírací
otvor	otvor	k1gInSc1	otvor
–	–	k?	–
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
části	část	k1gFnSc6	část
komína	komín	k1gInSc2	komín
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
ve	v	k7c6	v
sklepě	sklep	k1gInSc6	sklep
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vybírání	vybírání	k1gNnSc3	vybírání
popílku	popílek	k1gInSc2	popílek
a	a	k8xC	a
sazí	saze	k1gFnPc2	saze
z	z	k7c2	z
půdice	půdice	k?	půdice
</s>
</p>
<p>
<s>
sopouch	sopouch	k1gInSc1	sopouch
–	–	k?	–
otvor	otvor	k1gInSc1	otvor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
topidlo	topidlo	k1gNnSc4	topidlo
se	s	k7c7	s
spalinovou	spalinový	k2eAgFnSc7d1	spalinová
cestou	cesta	k1gFnSc7	cesta
</s>
</p>
<p>
<s>
vymetací	vymetací	k2eAgInSc1d1	vymetací
otvor	otvor	k1gInSc1	otvor
–	–	k?	–
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
se	se	k3xPyFc4	se
na	na	k7c6	na
půdách	půda	k1gFnPc6	půda
jestliže	jestliže	k8xS	jestliže
nelze	lze	k6eNd1	lze
vymetat	vymetat	k5eAaImF	vymetat
komín	komín	k1gInSc4	komín
z	z	k7c2	z
lávky	lávka	k1gFnSc2	lávka
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
</s>
</p>
<p>
<s>
komínová	komínový	k2eAgFnSc1d1	komínová
hlava	hlava	k1gFnSc1	hlava
–	–	k?	–
poslední	poslední	k2eAgFnSc4d1	poslední
část	část	k1gFnSc4	část
komínu	komín	k1gInSc2	komín
nad	nad	k7c7	nad
střechou	střecha	k1gFnSc7	střecha
</s>
</p>
<p>
<s>
komínová	komínový	k2eAgFnSc1d1	komínová
stříška	stříška	k1gFnSc1	stříška
–	–	k?	–
kryje	krýt	k5eAaImIp3nS	krýt
komínovou	komínový	k2eAgFnSc4d1	komínová
hlavu	hlava	k1gFnSc4	hlava
</s>
</p>
<p>
<s>
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
otvor	otvor	k1gInSc4	otvor
–	–	k?	–
otvor	otvor	k1gInSc1	otvor
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
kompletní	kompletní	k2eAgNnSc4d1	kompletní
čištění	čištění	k1gNnSc4	čištění
komínu	komín	k1gInSc2	komín
ze	z	k7c2	z
střechy	střecha	k1gFnSc2	střecha
nemožné	nemožná	k1gFnSc2	nemožná
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
komín	komín	k1gInSc1	komín
příliš	příliš	k6eAd1	příliš
vysoký	vysoký	k2eAgInSc1d1	vysoký
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
stavební	stavební	k2eAgFnSc4d1	stavební
konstrukci	konstrukce	k1gFnSc4	konstrukce
(	(	kIx(	(
<g/>
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
umístí	umístit	k5eAaPmIp3nS	umístit
pod	pod	k7c4	pod
ohyb	ohyb	k1gInSc4	ohyb
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
komínů	komín	k1gInPc2	komín
==	==	k?	==
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
konstrukčního	konstrukční	k2eAgNnSc2d1	konstrukční
uspořádání	uspořádání	k1gNnSc2	uspořádání
dělíme	dělit	k5eAaImIp1nP	dělit
komíny	komín	k1gInPc4	komín
na	na	k7c6	na
jednovrstvé	jednovrstvá	k1gFnSc6	jednovrstvá
–	–	k?	–
komínový	komínový	k2eAgInSc1d1	komínový
průduch	průduch	k1gInSc1	průduch
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
komínovým	komínový	k2eAgInSc7d1	komínový
pláštěm	plášť	k1gInSc7	plášť
a	a	k8xC	a
na	na	k7c6	na
vícevrstvé	vícevrstvý	k2eAgFnSc6d1	vícevrstvá
–	–	k?	–
komínový	komínový	k2eAgInSc4d1	komínový
průduch	průduch	k1gInSc4	průduch
tvoří	tvořit	k5eAaImIp3nP	tvořit
vložka	vložka	k1gFnSc1	vložka
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
izolační	izolační	k2eAgFnSc7d1	izolační
vrstvou	vrstva	k1gFnSc7	vrstva
a	a	k8xC	a
až	až	k6eAd1	až
potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
plášť	plášť	k1gInSc1	plášť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
průduchů	průduch	k1gInPc2	průduch
dělíme	dělit	k5eAaImIp1nP	dělit
komíny	komín	k1gInPc1	komín
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
–	–	k?	–
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
průduchem	průduch	k1gInSc7	průduch
a	a	k8xC	a
sdružené	sdružený	k2eAgNnSc1d1	sdružené
–	–	k?	–
s	s	k7c7	s
více	hodně	k6eAd2	hodně
průduchy	průduch	k1gInPc7	průduch
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
řadách	řada	k1gFnPc6	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
uspořádání	uspořádání	k1gNnSc2	uspořádání
průduchů	průduch	k1gInPc2	průduch
komíny	komín	k1gInPc7	komín
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c6	na
průběžné	průběžný	k2eAgFnSc6d1	průběžná
–	–	k?	–
všechny	všechen	k3xTgInPc4	všechen
průduchy	průduch	k1gInPc4	průduch
mají	mít	k5eAaImIp3nP	mít
půdice	půdice	k?	půdice
v	v	k7c6	v
nejnižším	nízký	k2eAgNnSc6d3	nejnižší
podlaží	podlaží	k1gNnSc6	podlaží
a	a	k8xC	a
podlažní	podlažní	k2eAgInPc1d1	podlažní
–	–	k?	–
průduchy	průduch	k1gInPc1	průduch
mají	mít	k5eAaImIp3nP	mít
půdice	půdice	k?	půdice
v	v	k7c6	v
těch	ten	k3xDgNnPc6	ten
podlažích	podlaží	k1gNnPc6	podlaží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
připojeny	připojen	k2eAgInPc4d1	připojen
spotřebiče	spotřebič	k1gInPc4	spotřebič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
připojování	připojování	k1gNnSc2	připojování
spotřebičů	spotřebič	k1gInPc2	spotřebič
komíny	komín	k1gInPc7	komín
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
samostatné	samostatný	k2eAgFnPc4d1	samostatná
–	–	k?	–
spaliny	spaliny	k1gFnPc4	spaliny
se	se	k3xPyFc4	se
odvádí	odvádět	k5eAaImIp3nS	odvádět
jedním	jeden	k4xCgInSc7	jeden
průduchem	průduch	k1gInSc7	průduch
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
podlaží	podlaží	k1gNnSc2	podlaží
a	a	k8xC	a
společné	společný	k2eAgFnSc2d1	společná
–	–	k?	–
jedním	jeden	k4xCgInSc7	jeden
průduchem	průduch	k1gInSc7	průduch
se	se	k3xPyFc4	se
odvádí	odvádět	k5eAaImIp3nP	odvádět
spaliny	spaliny	k1gFnPc1	spaliny
z	z	k7c2	z
více	hodně	k6eAd2	hodně
podlaží	podlaží	k1gNnSc2	podlaží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
průběhu	průběh	k1gInSc2	průběh
podélné	podélný	k2eAgFnSc2d1	podélná
osy	osa	k1gFnSc2	osa
dělíme	dělit	k5eAaImIp1nP	dělit
komíny	komín	k1gInPc1	komín
na	na	k7c4	na
přímé	přímý	k2eAgNnSc4d1	přímé
–	–	k?	–
osa	osa	k1gFnSc1	osa
je	být	k5eAaImIp3nS	být
svislá	svislý	k2eAgFnSc1d1	svislá
a	a	k8xC	a
nepřímé	přímý	k2eNgNnSc1d1	nepřímé
–	–	k?	–
osa	osa	k1gFnSc1	osa
průduchu	průduch	k1gInSc2	průduch
se	se	k3xPyFc4	se
odklání	odklánět	k5eAaImIp3nS	odklánět
od	od	k7c2	od
svislice	svislice	k1gFnSc2	svislice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
provedení	provedení	k1gNnSc2	provedení
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c6	na
zděné	zděný	k2eAgFnSc6d1	zděná
–	–	k?	–
ze	z	k7c2	z
speciálních	speciální	k2eAgFnPc2d1	speciální
tvarovek	tvarovka	k1gFnPc2	tvarovka
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
na	na	k7c6	na
montované	montovaný	k2eAgFnSc6d1	montovaná
a	a	k8xC	a
jednolité	jednolitý	k2eAgFnSc6d1	jednolitá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
podle	podle	k7c2	podle
vztahu	vztah	k1gInSc2	vztah
ke	k	k7c3	k
svislým	svislý	k2eAgFnPc3d1	svislá
konstrukcím	konstrukce	k1gFnPc3	konstrukce
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
vestavěné	vestavěný	k2eAgNnSc4d1	vestavěné
<g/>
,	,	kIx,	,
přistavěné	přistavěný	k2eAgNnSc4d1	přistavěné
a	a	k8xC	a
samostatně	samostatně	k6eAd1	samostatně
stojící	stojící	k2eAgFnPc4d1	stojící
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Milovníci	milovník	k1gMnPc1	milovník
lezení	lezený	k2eAgMnPc1d1	lezený
na	na	k7c4	na
komíny	komín	k1gInPc4	komín
si	se	k3xPyFc3	se
říkají	říkat	k5eAaImIp3nP	říkat
komínáři	komínář	k1gMnPc1	komínář
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
sdružení	sdružení	k1gNnSc4	sdružení
<g/>
,	,	kIx,	,
pravidla	pravidlo	k1gNnPc1	pravidlo
a	a	k8xC	a
specifický	specifický	k2eAgInSc1d1	specifický
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
komíny	komín	k1gInPc4	komín
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
mají	mít	k5eAaImIp3nP	mít
nejvyšší	vysoký	k2eAgInPc1d3	Nejvyšší
komíny	komín	k1gInPc1	komín
elektrárna	elektrárna	k1gFnSc1	elektrárna
Chvaletice	Chvaletice	k1gFnPc1	Chvaletice
a	a	k8xC	a
elektrárna	elektrárna	k1gFnSc1	elektrárna
Prunéřov	Prunéřov	k1gInSc1	Prunéřov
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
300	[number]	k4	300
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
elektrárna	elektrárna	k1gFnSc1	elektrárna
Mělník	Mělník	k1gInSc1	Mělník
–	–	k?	–
Horní	horní	k2eAgFnSc4d1	horní
Počáply	Počáply	k1gFnSc4	Počáply
(	(	kIx(	(
<g/>
270	[number]	k4	270
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
komín	komín	k1gInSc1	komín
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
tepelné	tepelný	k2eAgFnSc2d1	tepelná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Trbovlje	Trbovlj	k1gFnSc2	Trbovlj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgMnSc1d1	vysoký
360	[number]	k4	360
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
komín	komín	k1gInSc1	komín
na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Ekibastuz	Ekibastuza	k1gFnPc2	Ekibastuza
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
420	[number]	k4	420
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
komínové	komínový	k2eAgFnSc2d1	komínová
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
nadstřešní	nadstřešní	k2eAgFnSc1d1	nadstřešní
část	část	k1gFnSc1	část
komína	komín	k1gInSc2	komín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
státní	státní	k2eAgFnSc7d1	státní
normou	norma	k1gFnSc7	norma
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
komín	komín	k1gInSc1	komín
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
hřebene	hřeben	k1gInSc2	hřeben
střechy	střecha	k1gFnSc2	střecha
musí	muset	k5eAaImIp3nS	muset
přesahovat	přesahovat	k5eAaImF	přesahovat
tečnu	tečna	k1gFnSc4	tečna
vedenou	vedený	k2eAgFnSc4d1	vedená
hřebenem	hřeben	k1gInSc7	hřeben
střechy	střecha	k1gFnSc2	střecha
o	o	k7c4	o
650	[number]	k4	650
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
připojování	připojování	k1gNnSc6	připojování
spotřebičů	spotřebič	k1gInPc2	spotřebič
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
teplotou	teplota	k1gFnSc7	teplota
spalin	spaliny	k1gFnPc2	spaliny
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
dilatačně	dilatačně	k6eAd1	dilatačně
oddělit	oddělit	k5eAaPmF	oddělit
kovový	kovový	k2eAgInSc4d1	kovový
kouřovod	kouřovod	k1gInSc4	kouřovod
od	od	k7c2	od
keramického	keramický	k2eAgNnSc2d1	keramické
hrdla	hrdlo	k1gNnSc2	hrdlo
sopouchu	sopouch	k1gInSc2	sopouch
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spoj	spoj	k1gInSc1	spoj
těsnil	těsnit	k5eAaImAgInS	těsnit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
aby	aby	kYmCp3nS	aby
zároveň	zároveň	k6eAd1	zároveň
eliminoval	eliminovat	k5eAaBmAgMnS	eliminovat
rozdílné	rozdílný	k2eAgFnPc4d1	rozdílná
rozměrové	rozměrový	k2eAgFnPc4d1	rozměrová
změny	změna	k1gFnPc4	změna
obou	dva	k4xCgInPc2	dva
materiálů	materiál	k1gInPc2	materiál
při	při	k7c6	při
změnách	změna	k1gFnPc6	změna
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
docílit	docílit	k5eAaPmF	docílit
napojovacími	napojovací	k2eAgInPc7d1	napojovací
adaptéry	adaptér	k1gInPc7	adaptér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lokomotivní	lokomotivní	k2eAgInSc1d1	lokomotivní
komín	komín	k1gInSc1	komín
==	==	k?	==
</s>
</p>
<p>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
kapitolu	kapitola	k1gFnSc4	kapitola
tvoří	tvořit	k5eAaImIp3nS	tvořit
komín	komín	k1gInSc1	komín
parních	parní	k2eAgInPc2d1	parní
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
parních	parní	k2eAgFnPc2d1	parní
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Lokomotivní	lokomotivní	k2eAgInSc1d1	lokomotivní
parní	parní	k2eAgInSc1d1	parní
kotel	kotel	k1gInSc1	kotel
je	být	k5eAaImIp3nS	být
konstruován	konstruován	k2eAgInSc1d1	konstruován
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgNnSc4d1	vysoké
plošné	plošný	k2eAgNnSc4d1	plošné
tepelné	tepelný	k2eAgNnSc4d1	tepelné
zatížení	zatížení	k1gNnSc4	zatížení
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
malý	malý	k2eAgInSc1d1	malý
lokomotivní	lokomotivní	k2eAgInSc1d1	lokomotivní
parní	parní	k2eAgInSc1d1	parní
kotel	kotel	k1gInSc1	kotel
musí	muset	k5eAaImIp3nS	muset
dodávat	dodávat	k5eAaImF	dodávat
tepelný	tepelný	k2eAgInSc4d1	tepelný
výkon	výkon	k1gInSc4	výkon
až	až	k9	až
v	v	k7c6	v
řádech	řád	k1gInPc6	řád
megawattů	megawatt	k1gInPc2	megawatt
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
komína	komín	k1gInSc2	komín
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
omezena	omezit	k5eAaPmNgFnS	omezit
průjezdným	průjezdný	k2eAgInSc7d1	průjezdný
profilem	profil	k1gInSc7	profil
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
z	z	k7c2	z
relativně	relativně	k6eAd1	relativně
krátkého	krátký	k2eAgInSc2d1	krátký
komína	komín	k1gInSc2	komín
neodlétaly	odlétat	k5eNaImAgFnP	odlétat
jiskry	jiskra	k1gFnPc1	jiskra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
způsobit	způsobit	k5eAaPmF	způsobit
požár	požár	k1gInSc4	požár
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
vyvinuta	vyvinut	k2eAgFnSc1d1	vyvinuta
řada	řada	k1gFnSc1	řada
komínů	komín	k1gInPc2	komín
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tyto	tento	k3xDgInPc1	tento
protichůdné	protichůdný	k2eAgInPc1d1	protichůdný
požadavky	požadavek	k1gInPc1	požadavek
splňovaly	splňovat	k5eAaImAgInP	splňovat
<g/>
.	.	kIx.	.
</s>
<s>
Dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
průchod	průchod	k1gInSc1	průchod
spalin	spaliny	k1gFnPc2	spaliny
nízkým	nízký	k2eAgInSc7d1	nízký
komínem	komín	k1gInSc7	komín
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
už	už	k6eAd1	už
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
vývoje	vývoj	k1gInSc2	vývoj
parních	parní	k2eAgFnPc2d1	parní
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
dyšna	dyšna	k1gFnSc1	dyšna
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
komíny	komín	k1gInPc1	komín
nízkých	nízký	k2eAgFnPc2d1	nízká
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
byly	být	k5eAaImAgInP	být
vysoké	vysoký	k2eAgInPc1d1	vysoký
válce	válec	k1gInPc1	válec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vzhůru	vzhůru	k6eAd1	vzhůru
se	se	k3xPyFc4	se
rozšiřující	rozšiřující	k2eAgInPc1d1	rozšiřující
kužely	kužel	k1gInPc1	kužel
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
komín	komín	k1gInSc4	komín
se	se	k3xPyFc4	se
často	často	k6eAd1	často
montovalo	montovat	k5eAaImAgNnS	montovat
síto	síto	k1gNnSc1	síto
pro	pro	k7c4	pro
zachycení	zachycení	k1gNnSc4	zachycení
sazí	saze	k1gFnPc2	saze
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
několik	několik	k4yIc1	několik
typů	typ	k1gInPc2	typ
baňatých	baňatý	k2eAgInPc2d1	baňatý
komínů	komín	k1gInPc2	komín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
komínáři	komínář	k1gMnPc1	komínář
</s>
</p>
<p>
<s>
dymník	dymník	k1gInSc1	dymník
</s>
</p>
<p>
<s>
komínový	komínový	k2eAgInSc1d1	komínový
efekt	efekt	k1gInSc1	efekt
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
komín	komín	k1gInSc1	komín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
komín	komín	k1gInSc1	komín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
továrních	tovární	k2eAgInPc2d1	tovární
komínů	komín	k1gInPc2	komín
Galerie	galerie	k1gFnSc2	galerie
komín	komín	k1gInSc4	komín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
