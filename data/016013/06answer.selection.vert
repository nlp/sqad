<s>
Technecium	technecium	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Tc	tc	k0
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Technetium	Technetium	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejlehčí	lehký	k2eAgInSc1d3
prvek	prvek	k1gInSc1
v	v	k7c6
periodické	periodický	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nemá	mít	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
stabilní	stabilní	k2eAgInSc4d1
izotop	izotop	k1gInSc4
<g/>
.	.	kIx.
</s>