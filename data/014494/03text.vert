<s>
Krzysztof	Krzysztof	k1gInSc1
Rutkowski	Rutkowsk	k1gFnSc2
</s>
<s>
Krzysztof	Krzysztof	k1gInSc1
Rutkowski	Rutkowsk	k1gFnSc2
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Stronnictwo	Stronnictwo	k1gNnSc1
Gospodarcze	Gospodarcze	k1gFnSc2
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1960	#num#	k4
(	(	kIx(
<g/>
61	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Teresin	Teresin	k1gInSc1
Profese	profes	k1gFnSc2
</s>
<s>
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
herec	herec	k1gMnSc1
a	a	k8xC
detektiv	detektiv	k1gMnSc1
Commons	Commonsa	k1gFnPc2
</s>
<s>
Krzysztof	Krzysztof	k1gInSc1
Rutkowski	Rutkowsk	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Krzysztof	Krzysztof	k1gInSc1
Rutkowski	Rutkowsk	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1960	#num#	k4
v	v	k7c6
Teresině	Teresina	k1gFnSc6
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
polský	polský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
soukromý	soukromý	k2eAgMnSc1d1
detektiv	detektiv	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2001-2005	2001-2005	k4
byl	být	k5eAaImAgMnS
poslancem	poslanec	k1gMnSc7
Sejmu	Sejm	k1gInSc2
za	za	k7c4
Sebeobranu	sebeobrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podniká	podnikat	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
soukromých	soukromý	k2eAgFnPc2d1
detektivních	detektivní	k2eAgFnPc2d1
agentur	agentura	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgFnPc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nebojí	bát	k5eNaImIp3nP
provádět	provádět	k5eAaImF
i	i	k9
nebezpečné	bezpečný	k2eNgFnPc4d1
<g/>
,	,	kIx,
kontroverzní	kontroverzní	k2eAgFnPc4d1
a	a	k8xC
nezákonné	zákonný	k2eNgFnPc4d1
akce	akce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
kontroverzní	kontroverzní	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
a	a	k8xC
účast	účast	k1gFnSc4
na	na	k7c6
finančních	finanční	k2eAgFnPc6d1
machinacích	machinace	k1gFnPc6
mu	on	k3xPp3gMnSc3
vynesly	vynést	k5eAaPmAgInP
několik	několik	k4yIc4
menších	malý	k2eAgNnPc2d2
odsouzení	odsouzení	k1gNnPc2
a	a	k8xC
trestů	trest	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Barnevern	Barnevern	k1gMnSc1
</s>
<s>
Mezi	mezi	k7c7
jeho	jeho	k3xOp3gFnSc7
nejznámější	známý	k2eAgFnSc1d3
akce	akce	k1gFnSc1
patří	patřit	k5eAaImIp3nS
organizace	organizace	k1gFnSc1
útěků	útěk	k1gInPc2
dětí	dítě	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
návrat	návrat	k1gInSc1
zahraničním	zahraniční	k2eAgMnPc3d1
rodičům	rodič	k1gMnPc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
připravil	připravit	k5eAaPmAgInS
a	a	k8xC
vedl	vést	k5eAaImAgInS
v	v	k7c6
Norsku	Norsko	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
útěk	útěk	k1gInSc4
matky	matka	k1gFnSc2
s	s	k7c7
dětmi	dítě	k1gFnPc7
z	z	k7c2
Tuniska	Tunisko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
vzbudil	vzbudit	k5eAaPmAgInS
především	především	k6eAd1
případ	případ	k1gInSc1
polské	polský	k2eAgFnSc2d1
dívky	dívka	k1gFnSc2
Nikolky	Nikolka	k1gFnSc2
Rybkové	Rybková	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
pomohl	pomoct	k5eAaPmAgMnS
utéci	utéct	k5eAaPmF
z	z	k7c2
péče	péče	k1gFnSc2
kontroverzní	kontroverzní	k2eAgFnSc2d1
norské	norský	k2eAgFnSc2d1
sociálky	sociálka	k1gFnSc2
Barnevernetu	Barnevernet	k1gInSc2
a	a	k8xC
vrátil	vrátit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
rodičům	rodič	k1gMnPc3
do	do	k7c2
Polska	Polsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
(	(	kIx(
<g/>
žádost	žádost	k1gFnSc4
norských	norský	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
o	o	k7c4
opětovné	opětovný	k2eAgNnSc4d1
odebrání	odebrání	k1gNnSc4
dívky	dívka	k1gFnSc2
rodičům	rodič	k1gMnPc3
a	a	k8xC
její	její	k3xOp3gNnSc4
navrácení	navrácení	k1gNnSc4
do	do	k7c2
Norska	Norsko	k1gNnSc2
polské	polský	k2eAgInPc1d1
soudy	soud	k1gInPc1
odmítly	odmítnout	k5eAaPmAgInP
jako	jako	k9
zcela	zcela	k6eAd1
neopodstatněnou	opodstatněný	k2eNgFnSc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
polskou	polský	k2eAgFnSc7d1
televizí	televize	k1gFnSc7
TVN	TVN	kA
natočil	natočit	k5eAaBmAgMnS
seriál	seriál	k1gInSc4
Detektyw	Detektyw	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
některých	některý	k3yIgFnPc6
akcích	akce	k1gFnPc6
jeho	jeho	k3xOp3gFnPc2
agentur	agentura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
js	js	k?
<g/>
2014805073	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1067	#num#	k4
3370	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2006110848	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
63772804	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2006110848	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Polsko	Polsko	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
