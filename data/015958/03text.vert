<s>
3D	3D	k4
</s>
<s>
Trojrozměrné	trojrozměrný	k2eAgFnPc1d1
kartézské	kartézský	k2eAgFnPc1d1
souřadnice	souřadnice	k1gFnPc1
s	s	k7c7
osami	osa	k1gFnPc7
x	x	k?
<g/>
,	,	kIx,
y	y	k?
a	a	k8xC
z	z	k7c2
</s>
<s>
3D	3D	k4
či	či	k8xC
3-D	3-D	k4
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
výrazu	výraz	k1gInSc3
„	„	k?
<g/>
trojdimenzionální	trojdimenzionální	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
trojrozměrný	trojrozměrný	k2eAgInSc4d1
<g/>
“	“	k?
a	a	k8xC
označuje	označovat	k5eAaImIp3nS
svět	svět	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
popsat	popsat	k5eAaPmF
třemi	tři	k4xCgInPc7
rozměry	rozměr	k1gInPc7
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
kartézská	kartézský	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
souřadnic	souřadnice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
předměty	předmět	k1gInPc1
ve	v	k7c6
trojrozměrném	trojrozměrný	k2eAgInSc6d1
světě	svět	k1gInSc6
mají	mít	k5eAaImIp3nP
objem	objem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Zkratka	zkratka	k1gFnSc1
3D	3D	k4
často	často	k6eAd1
označuje	označovat	k5eAaImIp3nS
techniky	technika	k1gFnSc2
používané	používaný	k2eAgFnSc2d1
pro	pro	k7c4
zobrazení	zobrazení	k1gNnSc4
či	či	k8xC
prohlížení	prohlížení	k1gNnSc4
zdánlivě	zdánlivě	k6eAd1
trojrozměrných	trojrozměrný	k2eAgInPc2d1
objektů	objekt	k1gInPc2
na	na	k7c6
plochém	plochý	k2eAgInSc6d1
(	(	kIx(
<g/>
dvojrozměrném	dvojrozměrný	k2eAgMnSc6d1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
D	D	kA
<g/>
)	)	kIx)
médiu	médium	k1gNnSc6
(	(	kIx(
<g/>
na	na	k7c6
papíře	papír	k1gInSc6
<g/>
,	,	kIx,
filmovém	filmový	k2eAgNnSc6d1
plátnu	plátno	k1gNnSc6
<g/>
,	,	kIx,
počítačové	počítačový	k2eAgFnSc6d1
obrazovce	obrazovka	k1gFnSc6
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
matematice	matematika	k1gFnSc6
se	se	k3xPyFc4
často	často	k6eAd1
používá	používat	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
n-D	n-D	k?
<g/>
,	,	kIx,
nebo	nebo	k8xC
nP	nP	k?
pro	pro	k7c4
n-rozměrný	n-rozměrný	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
3D	3D	k4
film	film	k1gInSc1
</s>
<s>
3D	3D	k4
televize	televize	k1gFnSc1
</s>
<s>
3D	3D	k4
tkanina	tkanina	k1gFnSc1
</s>
<s>
Dimenze	dimenze	k1gFnSc1
</s>
<s>
Počítačová	počítačový	k2eAgFnSc1d1
grafika	grafika	k1gFnSc1
</s>
<s>
Počítačová	počítačový	k2eAgFnSc1d1
3D	3D	k4
grafika	grafik	k1gMnSc2
</s>
<s>
Prostor	prostor	k1gInSc1
(	(	kIx(
<g/>
geometrie	geometrie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
počítačem	počítač	k1gInSc7
animovaných	animovaný	k2eAgInPc2d1
filmů	film	k1gInPc2
</s>
<s>
Stereoskopie	stereoskopie	k1gFnSc1
</s>
<s>
4D	4D	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
PSH	PSH	kA
<g/>
:	:	kIx,
13622	#num#	k4
</s>
