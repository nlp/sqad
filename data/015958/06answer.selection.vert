<s>
3D	3D	k4
či	či	k8xC
3-D	3-D	k4
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
výrazu	výraz	k1gInSc3
„	„	k?
<g/>
trojdimenzionální	trojdimenzionální	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
trojrozměrný	trojrozměrný	k2eAgInSc4d1
<g/>
“	“	k?
a	a	k8xC
označuje	označovat	k5eAaImIp3nS
svět	svět	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
popsat	popsat	k5eAaPmF
třemi	tři	k4xCgInPc7
rozměry	rozměr	k1gInPc7
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
kartézská	kartézský	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
souřadnic	souřadnice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
předměty	předmět	k1gInPc1
ve	v	k7c6
trojrozměrném	trojrozměrný	k2eAgInSc6d1
světě	svět	k1gInSc6
mají	mít	k5eAaImIp3nP
objem	objem	k1gInSc4
<g/>
.	.	kIx.
</s>