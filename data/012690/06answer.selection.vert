<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
kariéru	kariéra	k1gFnSc4	kariéra
protknulo	protknout	k5eAaPmAgNnS	protknout
několik	několik	k4yIc1	několik
kontroverzních	kontroverzní	k2eAgInPc2d1	kontroverzní
momentů	moment	k1gInPc2	moment
<g/>
,	,	kIx,	,
v	v	k7c6	v
závěrečných	závěrečný	k2eAgInPc6d1	závěrečný
závodech	závod	k1gInPc6	závod
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
o	o	k7c4	o
titul	titul	k1gInSc4	titul
kolidoval	kolidovat	k5eAaImAgMnS	kolidovat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
soupeři	soupeř	k1gMnPc7	soupeř
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
s	s	k7c7	s
Damonem	Damon	k1gInSc7	Damon
Hillem	Hill	k1gInSc7	Hill
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
s	s	k7c7	s
Jacquesem	Jacques	k1gMnSc7	Jacques
Villeneuvem	Villeneuv	k1gMnSc7	Villeneuv
<g/>
,	,	kIx,	,
za	za	k7c4	za
druhý	druhý	k4xOgInSc4	druhý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
incidentů	incident	k1gInPc2	incident
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
ze	z	k7c2	z
šampionátu	šampionát	k1gInSc2	šampionát
(	(	kIx(	(
<g/>
body	bod	k1gInPc4	bod
a	a	k8xC	a
vítězství	vítězství	k1gNnSc4	vítězství
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
nicméně	nicméně	k8xC	nicméně
ponechány	ponechán	k2eAgFnPc1d1	ponechána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
byl	být	k5eAaImAgMnS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
za	za	k7c4	za
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
jízdu	jízda	k1gFnSc4	jízda
a	a	k8xC	a
týmovou	týmový	k2eAgFnSc4d1	týmová
režii	režie	k1gFnSc4	režie
(	(	kIx(	(
<g/>
trest	trest	k1gInSc4	trest
udělen	udělen	k2eAgInSc4d1	udělen
stáji	stáj	k1gFnSc3	stáj
Ferrari	ferrari	k1gNnSc1	ferrari
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
vedení	vedení	k1gNnSc1	vedení
týmovou	týmový	k2eAgFnSc4d1	týmová
režii	režie	k1gFnSc4	režie
řídilo	řídit	k5eAaImAgNnS	řídit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
