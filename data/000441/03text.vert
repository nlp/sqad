<p>
<s>
Merkur	Merkur	k1gMnSc1	Merkur
je	být	k5eAaImIp3nS	být
Slunci	slunce	k1gNnSc3	slunce
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
a	a	k8xC	a
současně	současně	k6eAd1	současně
i	i	k9	i
nejmenší	malý	k2eAgFnSc7d3	nejmenší
planetou	planeta	k1gFnSc7	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
větší	veliký	k2eAgFnSc2d2	veliký
velikosti	velikost	k1gFnSc2	velikost
než	než	k8xS	než
pozemský	pozemský	k2eAgInSc4d1	pozemský
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
menší	malý	k2eAgInSc4d2	menší
než	než	k8xS	než
Jupiterův	Jupiterův	k2eAgInSc4d1	Jupiterův
měsíc	měsíc	k1gInSc4	měsíc
Ganymed	Ganymed	k1gMnSc1	Ganymed
a	a	k8xC	a
Saturnův	Saturnův	k2eAgInSc1d1	Saturnův
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
Merkur	Merkur	k1gInSc1	Merkur
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
nejblíže	blízce	k6eAd3	blízce
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
oběh	oběh	k1gInSc4	oběh
trvá	trvat	k5eAaImIp3nS	trvat
pouze	pouze	k6eAd1	pouze
87,969	[number]	k4	87,969
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
Merkuru	Merkur	k1gInSc2	Merkur
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgFnSc1d3	veliký
výstřednost	výstřednost	k1gFnSc1	výstřednost
dráhy	dráha	k1gFnSc2	dráha
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
planet	planeta	k1gFnPc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
nejmenší	malý	k2eAgInSc4d3	nejmenší
sklon	sklon	k1gInSc4	sklon
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
oběhů	oběh	k1gInPc2	oběh
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
třem	tři	k4xCgInPc3	tři
otočením	otočení	k1gNnSc7	otočení
kolem	kolem	k7c2	kolem
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Perihelium	perihelium	k1gNnSc1	perihelium
jeho	jeho	k3xOp3gFnSc2	jeho
dráhy	dráha	k1gFnSc2	dráha
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
o	o	k7c4	o
43	[number]	k4	43
vteřin	vteřina	k1gFnPc2	vteřina
za	za	k7c4	za
století	století	k1gNnSc4	století
<g/>
;	;	kIx,	;
fenomén	fenomén	k1gInSc1	fenomén
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
obecnou	obecná	k1gFnSc4	obecná
teorií	teorie	k1gFnPc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
Merkur	Merkur	k1gInSc1	Merkur
jasnosti	jasnost	k1gFnSc2	jasnost
mezi	mezi	k7c7	mezi
-2,0	-2,0	k4	-2,0
až	až	k9	až
5,5	[number]	k4	5,5
<g/>
m	m	kA	m
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
i	i	k9	i
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevzdaluje	vzdalovat	k5eNaImIp3nS	vzdalovat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
na	na	k7c4	na
28,3	[number]	k4	28,3
<g/>
°	°	k?	°
je	být	k5eAaImIp3nS	být
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
těžko	těžko	k6eAd1	těžko
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
podmínky	podmínka	k1gFnPc1	podmínka
tak	tak	k6eAd1	tak
nastávají	nastávat	k5eAaImIp3nP	nastávat
při	při	k7c6	při
soumraku	soumrak	k1gInSc6	soumrak
či	či	k8xC	či
úsvitu	úsvit	k1gInSc6	úsvit
<g/>
,	,	kIx,	,
než	než	k8xS	než
vyjde	vyjít	k5eAaPmIp3nS	vyjít
Slunce	slunce	k1gNnSc1	slunce
nad	nad	k7c4	nad
horizont	horizont	k1gInSc4	horizont
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
planety	planeta	k1gFnSc2	planeta
pozemskými	pozemský	k2eAgInPc7d1	pozemský
teleskopy	teleskop	k1gInPc7	teleskop
je	být	k5eAaImIp3nS	být
složité	složitý	k2eAgNnSc1d1	složité
kvůli	kvůli	k7c3	kvůli
blízkosti	blízkost	k1gFnSc3	blízkost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Detailnější	detailní	k2eAgFnPc4d2	detailnější
znalosti	znalost	k1gFnPc4	znalost
přinesla	přinést	k5eAaPmAgFnS	přinést
až	až	k9	až
dvojice	dvojice	k1gFnSc1	dvojice
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
sondou	sonda	k1gFnSc7	sonda
u	u	k7c2	u
Merkuru	Merkur	k1gInSc2	Merkur
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nasnímala	nasnímat	k5eAaPmAgNnP	nasnímat
přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
%	%	kIx~	%
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
dorazila	dorazit	k5eAaPmAgFnS	dorazit
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
další	další	k2eAgFnSc1d1	další
sonda	sonda	k1gFnSc1	sonda
MESSENGER	MESSENGER	kA	MESSENGER
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
provedla	provést	k5eAaPmAgFnS	provést
tři	tři	k4xCgInPc4	tři
průlety	průlet	k1gInPc4	průlet
kolem	kolem	k7c2	kolem
Merkuru	Merkur	k1gInSc2	Merkur
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
definitivně	definitivně	k6eAd1	definitivně
úspěšně	úspěšně	k6eAd1	úspěšně
navedena	navést	k5eAaPmNgFnS	navést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
sond	sonda	k1gFnPc2	sonda
umožnily	umožnit	k5eAaPmAgInP	umožnit
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
silně	silně	k6eAd1	silně
připomíná	připomínat	k5eAaImIp3nS	připomínat
měsíční	měsíční	k2eAgFnSc4d1	měsíční
krajinu	krajina	k1gFnSc4	krajina
plnou	plný	k2eAgFnSc4d1	plná
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
nízkých	nízký	k2eAgNnPc2d1	nízké
pohoří	pohoří	k1gNnPc2	pohoří
a	a	k8xC	a
lávových	lávový	k2eAgFnPc2d1	lávová
planin	planina	k1gFnPc2	planina
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vlivem	k7c2	vlivem
neustálých	neustálý	k2eAgInPc2d1	neustálý
dopadů	dopad	k1gInPc2	dopad
těles	těleso	k1gNnPc2	těleso
všech	všecek	k3xTgFnPc2	všecek
velikostí	velikost	k1gFnPc2	velikost
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Merkuru	Merkur	k1gInSc2	Merkur
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
povrchu	povrch	k1gInSc2	povrch
erodována	erodovat	k5eAaImNgFnS	erodovat
drobnými	drobný	k2eAgInPc7d1	drobný
krátery	kráter	k1gInPc7	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
vlivem	vliv	k1gInSc7	vliv
smršťování	smršťování	k1gNnSc2	smršťování
planety	planeta	k1gFnSc2	planeta
rozpraskán	rozpraskán	k2eAgInSc4d1	rozpraskán
množstvím	množství	k1gNnSc7	množství
útesových	útesový	k2eAgInPc2d1	útesový
zlomů	zlom	k1gInPc2	zlom
dosahujících	dosahující	k2eAgFnPc2d1	dosahující
výšky	výška	k1gFnPc1	výška
několika	několik	k4yIc2	několik
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
stovek	stovka	k1gFnPc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
neustále	neustále	k6eAd1	neustále
bombardován	bombardován	k2eAgInSc4d1	bombardován
fotony	foton	k1gInPc4	foton
i	i	k8xC	i
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
–	–	k?	–
proudem	proud	k1gInSc7	proud
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
směřujících	směřující	k2eAgFnPc2d1	směřující
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rychlostí	rychlost	k1gFnSc7	rychlost
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
velkých	velký	k2eAgInPc2d1	velký
rozdílů	rozdíl	k1gInPc2	rozdíl
teplot	teplota	k1gFnPc2	teplota
mezi	mezi	k7c7	mezi
osvětlenou	osvětlený	k2eAgFnSc7d1	osvětlená
a	a	k8xC	a
neosvětlenou	osvětlený	k2eNgFnSc7d1	neosvětlená
polokoulí	polokoule	k1gFnSc7	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
přes	přes	k7c4	přes
600	[number]	k4	600
°	°	k?	°
<g/>
C.	C.	kA	C.
Na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
může	moct	k5eAaImIp3nS	moct
teplota	teplota	k1gFnSc1	teplota
vystoupit	vystoupit	k5eAaPmF	vystoupit
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
430	[number]	k4	430
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
na	na	k7c4	na
odvrácené	odvrácený	k2eAgFnPc4d1	odvrácená
panuje	panovat	k5eAaImIp3nS	panovat
mráz	mráz	k1gInSc1	mráz
až	až	k8xS	až
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
doložené	doložený	k2eAgNnSc1d1	doložené
pozorování	pozorování	k1gNnSc1	pozorování
Merkuru	Merkur	k1gInSc2	Merkur
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
4	[number]	k4	4
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
Merkur	Merkur	k1gInSc4	Merkur
řečtí	řecký	k2eAgMnPc1d1	řecký
astronomové	astronom	k1gMnPc1	astronom
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
však	však	k9	však
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
samostatná	samostatný	k2eAgNnPc4d1	samostatné
tělesa	těleso	k1gNnPc4	těleso
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
Apollo	Apollo	k1gNnSc4	Apollo
pro	pro	k7c4	pro
hvězdu	hvězda	k1gFnSc4	hvězda
při	při	k7c6	při
východu	východ	k1gInSc6	východ
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Hermés	Hermésa	k1gFnPc2	Hermésa
při	při	k7c6	při
západu	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
pojmenování	pojmenování	k1gNnSc1	pojmenování
planety	planeta	k1gFnSc2	planeta
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
římské	římský	k2eAgFnSc2d1	římská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Merkur	Merkur	k1gInSc1	Merkur
bylo	být	k5eAaImAgNnS	být
jméno	jméno	k1gNnSc4	jméno
pro	pro	k7c4	pro
posla	posel	k1gMnSc4	posel
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
jménem	jméno	k1gNnSc7	jméno
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
Hermovi	Hermův	k2eAgMnPc1d1	Hermův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Merkur	Merkur	k1gInSc1	Merkur
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
solárního	solární	k2eAgInSc2d1	solární
systému	systém	k1gInSc2	systém
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
4,5	[number]	k4	4,5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
akrecí	akrece	k1gFnPc2	akrece
z	z	k7c2	z
pracho-plynného	pracholynný	k2eAgInSc2d1	pracho-plynný
disku	disk	k1gInSc2	disk
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obíhal	obíhat	k5eAaImAgMnS	obíhat
kolem	kolem	k7c2	kolem
rodící	rodící	k2eAgFnSc2d1	rodící
se	se	k3xPyFc4	se
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Srážkami	srážka	k1gFnPc7	srážka
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgNnP	začít
formovat	formovat	k5eAaImF	formovat
malá	malý	k2eAgNnPc1d1	malé
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
gravitací	gravitace	k1gFnSc7	gravitace
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
další	další	k2eAgFnSc1d1	další
částice	částice	k1gFnSc1	částice
a	a	k8xC	a
okolní	okolní	k2eAgInSc1d1	okolní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tak	tak	k9	tak
první	první	k4xOgFnPc1	první
planetesimály	planetesimála	k1gFnPc1	planetesimála
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
srážely	srážet	k5eAaImAgInP	srážet
a	a	k8xC	a
formovaly	formovat	k5eAaImAgInP	formovat
větší	veliký	k2eAgFnPc4d2	veliký
tělesa	těleso	k1gNnPc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
čtyři	čtyři	k4xCgInPc1	čtyři
terestrické	terestrický	k2eAgInPc1d1	terestrický
protoplanety	protoplanet	k1gInPc1	protoplanet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zformování	zformování	k1gNnSc1	zformování
protoplanety	protoplanet	k1gInPc7	protoplanet
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
masivnímu	masivní	k2eAgNnSc3d1	masivní
bombardování	bombardování	k1gNnSc3	bombardování
povrchu	povrch	k1gInSc3	povrch
materiálem	materiál	k1gInSc7	materiál
zbylým	zbylý	k2eAgInSc7d1	zbylý
ze	z	k7c2	z
vzniku	vznik	k1gInSc2	vznik
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jeho	jeho	k3xOp3gInSc2	jeho
neustálé	neustálý	k2eAgNnSc1d1	neustálé
přetváření	přetváření	k1gNnSc1	přetváření
a	a	k8xC	a
přetavování	přetavování	k1gNnSc1	přetavování
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
Merkur	Merkur	k1gInSc1	Merkur
netypicky	typicky	k6eNd1	typicky
velké	velký	k2eAgNnSc4d1	velké
jádro	jádro	k1gNnSc4	jádro
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
plášti	plášť	k1gInSc3	plášť
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
kolize	kolize	k1gFnSc1	kolize
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
planetisimálou	planetisimála	k1gFnSc7	planetisimála
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
část	část	k1gFnSc1	část
pláště	plášť	k1gInSc2	plášť
odpařila	odpařit	k5eAaPmAgFnS	odpařit
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc1d1	celý
povrch	povrch	k1gInSc1	povrch
byl	být	k5eAaImAgInS	být
roztaven	roztavit	k5eAaPmNgInS	roztavit
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
tzv.	tzv.	kA	tzv.
magmatického	magmatický	k2eAgInSc2d1	magmatický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
tepelná	tepelný	k2eAgFnSc1d1	tepelná
energie	energie	k1gFnSc1	energie
společně	společně	k6eAd1	společně
s	s	k7c7	s
teplem	teplo	k1gNnSc7	teplo
uvolněným	uvolněný	k2eAgNnSc7d1	uvolněné
diferenciací	diferenciace	k1gFnSc7	diferenciace
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
kumulována	kumulován	k2eAgFnSc1d1	kumulována
v	v	k7c6	v
nitru	nitro	k1gNnSc6	nitro
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
primární	primární	k2eAgFnSc2d1	primární
kůry	kůra	k1gFnSc2	kůra
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
stále	stále	k6eAd1	stále
nacházely	nacházet	k5eAaImAgFnP	nacházet
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
oblasti	oblast	k1gFnPc4	oblast
žhavé	žhavý	k2eAgFnSc2d1	žhavá
roztavené	roztavený	k2eAgFnSc2d1	roztavená
lávy	láva	k1gFnSc2	láva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nejspíše	nejspíše	k9	nejspíše
vyplnila	vyplnit	k5eAaPmAgFnS	vyplnit
některé	některý	k3yIgFnPc4	některý
starší	starý	k2eAgFnPc4d2	starší
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ztuhnutí	ztuhnutí	k1gNnSc6	ztuhnutí
lávy	láva	k1gFnSc2	láva
nastalo	nastat	k5eAaPmAgNnS	nastat
pro	pro	k7c4	pro
Merkur	Merkur	k1gInSc4	Merkur
klidnější	klidný	k2eAgNnSc4d2	klidnější
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
nedopadalo	dopadat	k5eNaImAgNnS	dopadat
tolik	tolik	k4xDc4	tolik
těles	těleso	k1gNnPc2	těleso
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
mohly	moct	k5eAaImAgFnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
mezikráterové	mezikráterový	k2eAgFnPc4d1	mezikráterový
planiny	planina	k1gFnPc4	planina
<g/>
.	.	kIx.	.
</s>
<s>
Merkur	Merkur	k1gInSc1	Merkur
i	i	k9	i
nadále	nadále	k6eAd1	nadále
postupně	postupně	k6eAd1	postupně
chladl	chladnout	k5eAaImAgMnS	chladnout
a	a	k8xC	a
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
zmenšování	zmenšování	k1gNnSc3	zmenšování
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
projevilo	projevit	k5eAaPmAgNnS	projevit
rozpraskáním	rozpraskání	k1gNnSc7	rozpraskání
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
stovky	stovka	k1gFnSc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
zlomů	zlom	k1gInPc2	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpraskání	rozpraskání	k1gNnSc6	rozpraskání
kůry	kůra	k1gFnSc2	kůra
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
objevily	objevit	k5eAaPmAgFnP	objevit
další	další	k2eAgFnPc1d1	další
velké	velký	k2eAgFnPc1d1	velká
lávové	lávový	k2eAgFnPc1d1	lávová
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
opět	opět	k6eAd1	opět
překryly	překrýt	k5eAaPmAgFnP	překrýt
část	část	k1gFnSc4	část
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
umožnily	umožnit	k5eAaPmAgFnP	umožnit
vznik	vznik	k1gInSc4	vznik
hladkých	hladký	k2eAgFnPc2d1	hladká
planin	planina	k1gFnPc2	planina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
žádná	žádný	k3yNgFnSc1	žádný
větší	veliký	k2eAgFnSc1d2	veliký
lávová	lávový	k2eAgFnSc1d1	lávová
plocha	plocha	k1gFnSc1	plocha
neobjevila	objevit	k5eNaPmAgFnS	objevit
a	a	k8xC	a
vzhled	vzhled	k1gInSc1	vzhled
povrchu	povrch	k1gInSc2	povrch
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
utvářet	utvářet	k5eAaImF	utvářet
dopady	dopad	k1gInPc4	dopad
meteoritů	meteorit	k1gInPc2	meteorit
a	a	k8xC	a
mikrometeoritů	mikrometeorit	k1gInPc2	mikrometeorit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
vznikem	vznik	k1gInSc7	vznik
drobného	drobný	k2eAgInSc2d1	drobný
prachu	prach	k1gInSc2	prach
rozšířeného	rozšířený	k2eAgInSc2d1	rozšířený
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
nazývaného	nazývaný	k2eAgMnSc4d1	nazývaný
regolit	regolit	k1gInSc4	regolit
<g/>
.	.	kIx.	.
<g/>
Mimo	mimo	k7c4	mimo
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
srážce	srážka	k1gFnSc6	srážka
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
abnormální	abnormální	k2eAgFnSc4d1	abnormální
velikost	velikost	k1gFnSc4	velikost
jádra	jádro	k1gNnSc2	jádro
vůči	vůči	k7c3	vůči
zbytku	zbytek	k1gInSc3	zbytek
planety	planeta	k1gFnSc2	planeta
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
planeta	planeta	k1gFnSc1	planeta
ještě	ještě	k6eAd1	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
zářivý	zářivý	k2eAgInSc1d1	zářivý
výkon	výkon	k1gInSc1	výkon
Slunce	slunce	k1gNnSc2	slunce
stabilizoval	stabilizovat	k5eAaBmAgInS	stabilizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
jejího	její	k3xOp3gInSc2	její
pláště	plášť	k1gInSc2	plášť
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
odpařena	odpařit	k5eAaPmNgFnS	odpařit
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
vesmíru	vesmír	k1gInSc2	vesmír
při	při	k7c6	při
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
obřích	obří	k2eAgFnPc2d1	obří
protoslunečních	protosluneční	k2eAgFnPc2d1	protosluneční
erupcí	erupce	k1gFnPc2	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
možné	možný	k2eAgNnSc4d1	možné
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
domněnku	domněnka	k1gFnSc4	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
lehčích	lehký	k2eAgInPc2d2	lehčí
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
formujících	formující	k2eAgFnPc2d1	formující
obvykle	obvykle	k6eAd1	obvykle
plášť	plášť	k1gInSc4	plášť
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vytlačena	vytlačit	k5eAaPmNgFnS	vytlačit
mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
vzniku	vznik	k1gInSc2	vznik
Merkuru	Merkur	k1gInSc3	Merkur
silným	silný	k2eAgInSc7d1	silný
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
charakteristika	charakteristika	k1gFnSc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Merkur	Merkur	k1gInSc1	Merkur
je	být	k5eAaImIp3nS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
s	s	k7c7	s
rovníkovým	rovníkový	k2eAgInSc7d1	rovníkový
poloměrem	poloměr	k1gInSc7	poloměr
2439,7	[number]	k4	2439,7
km	km	kA	km
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
38	[number]	k4	38
%	%	kIx~	%
průměru	průměr	k1gInSc2	průměr
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
1,4	[number]	k4	1,4
krát	krát	k6eAd1	krát
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
pozemský	pozemský	k2eAgInSc4d1	pozemský
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxně	paradoxně	k6eAd1	paradoxně
je	být	k5eAaImIp3nS	být
Merkur	Merkur	k1gMnSc1	Merkur
menší	malý	k2eAgInPc4d2	menší
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
největší	veliký	k2eAgInPc4d3	veliký
měsíce	měsíc	k1gInPc4	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
Ganymed	Ganymed	k1gMnSc1	Ganymed
a	a	k8xC	a
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
pevný	pevný	k2eAgInSc4d1	pevný
kamenitý	kamenitý	k2eAgInSc4d1	kamenitý
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
přibližně	přibližně	k6eAd1	přibližně
ze	z	k7c2	z
70	[number]	k4	70
%	%	kIx~	%
z	z	k7c2	z
kovových	kovový	k2eAgInPc2d1	kovový
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
30	[number]	k4	30
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
křemičitany	křemičitan	k1gInPc4	křemičitan
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
velkého	velký	k2eAgNnSc2d1	velké
zastoupení	zastoupení	k1gNnSc2	zastoupení
kovů	kov	k1gInPc2	kov
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
je	být	k5eAaImIp3nS	být
Merkur	Merkur	k1gMnSc1	Merkur
druhou	druhý	k4xOgFnSc7	druhý
nejhustší	hustý	k2eAgFnSc7d3	nejhustší
planetou	planeta	k1gFnSc7	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
o	o	k7c6	o
hustotě	hustota	k1gFnSc6	hustota
5,427	[number]	k4	5,427
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Venuše	Venuše	k1gFnSc2	Venuše
téměř	téměř	k6eAd1	téměř
dokonale	dokonale	k6eAd1	dokonale
kulový	kulový	k2eAgInSc1d1	kulový
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc1d1	malé
zploštění	zploštění	k1gNnSc1	zploštění
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
<g/>
.	.	kIx.	.
<g/>
Merkur	Merkur	k1gInSc1	Merkur
zblízka	zblízka	k6eAd1	zblízka
zkoumaly	zkoumat	k5eAaImAgInP	zkoumat
dvě	dva	k4xCgFnPc4	dva
americké	americký	k2eAgFnPc4d1	americká
sondy	sonda	k1gFnPc4	sonda
<g/>
:	:	kIx,	:
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
letech	let	k1gInPc6	let
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
zmapoval	zmapovat	k5eAaPmAgInS	zmapovat
přibližně	přibližně	k6eAd1	přibližně
45	[number]	k4	45
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
MESSENGER	MESSENGER	kA	MESSENGER
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zatím	zatím	k6eAd1	zatím
při	při	k7c6	při
třech	tři	k4xCgInPc6	tři
průletech	průlet	k1gInPc6	průlet
v	v	k7c6	v
roku	rok	k1gInSc6	rok
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
studoval	studovat	k5eAaImAgInS	studovat
kromě	kromě	k7c2	kromě
atmosféry	atmosféra	k1gFnSc2	atmosféra
i	i	k8xC	i
složení	složení	k1gNnSc2	složení
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Sondy	sonda	k1gFnPc1	sonda
zjistily	zjistit	k5eAaPmAgFnP	zjistit
velmi	velmi	k6eAd1	velmi
slabé	slabý	k2eAgFnPc1d1	slabá
stopy	stopa	k1gFnPc1	stopa
plynného	plynný	k2eAgInSc2d1	plynný
obalu	obal	k1gInSc2	obal
<g/>
,	,	kIx,	,
obsahujícího	obsahující	k2eAgNnSc2d1	obsahující
především	především	k9	především
atomy	atom	k1gInPc1	atom
pocházející	pocházející	k2eAgInPc1d1	pocházející
ze	z	k7c2	z
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
převážně	převážně	k6eAd1	převážně
helium	helium	k1gNnSc4	helium
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
Merkurovy	Merkurův	k2eAgFnSc2d1	Merkurova
atmosféry	atmosféra	k1gFnSc2	atmosféra
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Geologické	geologický	k2eAgNnSc4d1	geologické
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
Merkuru	Merkur	k1gInSc2	Merkur
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
značně	značně	k6eAd1	značně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
dosahující	dosahující	k2eAgFnPc1d1	dosahující
asi	asi	k9	asi
5400	[number]	k4	5400
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
silné	silný	k2eAgNnSc1d1	silné
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
zemského	zemský	k2eAgInSc2d1	zemský
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
je	být	k5eAaImIp3nS	být
vysvětlován	vysvětlován	k2eAgInSc1d1	vysvětlován
vysokým	vysoký	k2eAgNnSc7d1	vysoké
zastoupením	zastoupení	k1gNnSc7	zastoupení
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
niklu	nikl	k1gInSc2	nikl
uvnitř	uvnitř	k7c2	uvnitř
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
masivním	masivní	k2eAgNnSc7d1	masivní
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
kůrou	kůra	k1gFnSc7	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
důkaz	důkaz	k1gInSc1	důkaz
velkých	velký	k2eAgInPc2d1	velký
rozměrů	rozměr	k1gInPc2	rozměr
jádra	jádro	k1gNnSc2	jádro
slouží	sloužit	k5eAaImIp3nP	sloužit
přítomnost	přítomnost	k1gFnSc4	přítomnost
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
bylo	být	k5eAaImAgNnS	být
jádro	jádro	k1gNnSc1	jádro
jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
pomalá	pomalý	k2eAgFnSc1d1	pomalá
rotace	rotace	k1gFnSc1	rotace
planety	planeta	k1gFnSc2	planeta
by	by	kYmCp3nS	by
nestačila	stačit	k5eNaBmAgFnS	stačit
ke	k	k7c3	k
generování	generování	k1gNnSc3	generování
silného	silný	k2eAgNnSc2d1	silné
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
akumulace	akumulace	k1gFnSc1	akumulace
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
společně	společně	k6eAd1	společně
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
masivní	masivní	k2eAgFnSc7d1	masivní
velikostí	velikost	k1gFnSc7	velikost
zabírající	zabírající	k2eAgInSc4d1	zabírající
až	až	k6eAd1	až
75	[number]	k4	75
%	%	kIx~	%
průměru	průměr	k1gInSc6	průměr
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vědce	vědec	k1gMnPc4	vědec
zatím	zatím	k6eAd1	zatím
záhadou	záhada	k1gFnSc7	záhada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
<g/>
Geologové	geolog	k1gMnPc1	geolog
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
planety	planeta	k1gFnSc2	planeta
zabírá	zabírat	k5eAaImIp3nS	zabírat
okolo	okolo	k7c2	okolo
42	[number]	k4	42
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
poloměru	poloměr	k1gInSc2	poloměr
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
17	[number]	k4	17
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
výzkumy	výzkum	k1gInPc1	výzkum
napovídají	napovídat	k5eAaBmIp3nP	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
Merkuru	Merkur	k1gInSc2	Merkur
je	být	k5eAaImIp3nS	být
tekuté	tekutý	k2eAgNnSc1d1	tekuté
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
500	[number]	k4	500
až	až	k9	až
700	[number]	k4	700
km	km	kA	km
silný	silný	k2eAgInSc4d1	silný
plášť	plášť	k1gInSc4	plášť
tvořený	tvořený	k2eAgInSc4d1	tvořený
silikáty	silikát	k1gInPc4	silikát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
kůra	kůra	k1gFnSc1	kůra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
dle	dle	k7c2	dle
měření	měření	k1gNnSc2	měření
sondy	sonda	k1gFnSc2	sonda
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
a	a	k8xC	a
pozemských	pozemský	k2eAgInPc2d1	pozemský
teleskopů	teleskop	k1gInPc2	teleskop
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
100	[number]	k4	100
až	až	k9	až
300	[number]	k4	300
km	km	kA	km
silná	silný	k2eAgFnSc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
70	[number]	k4	70
%	%	kIx~	%
tvořena	tvořen	k2eAgFnSc1d1	tvořena
kovy	kov	k1gInPc7	kov
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
30	[number]	k4	30
%	%	kIx~	%
silikátovým	silikátový	k2eAgInSc7d1	silikátový
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
<g/>
Nejvíce	hodně	k6eAd3	hodně
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
teorie	teorie	k1gFnSc1	teorie
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Merkur	Merkur	k1gInSc1	Merkur
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
poměr	poměr	k1gInSc1	poměr
železa	železo	k1gNnSc2	železo
ku	k	k7c3	k
křemičitanům	křemičitan	k1gInPc3	křemičitan
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
chondrity	chondrita	k1gFnPc1	chondrita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgFnSc7d1	základní
stavební	stavební	k2eAgFnSc7d1	stavební
jednotkou	jednotka	k1gFnSc7	jednotka
většiny	většina	k1gFnSc2	většina
těles	těleso	k1gNnPc2	těleso
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
a	a	k8xC	a
že	že	k8xS	že
celá	celý	k2eAgFnSc1d1	celá
planeta	planeta	k1gFnSc1	planeta
byla	být	k5eAaImAgFnS	být
přibližně	přibližně	k6eAd1	přibližně
2,25	[number]	k4	2,25
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgFnSc1d2	hmotnější
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
historii	historie	k1gFnSc6	historie
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
srážce	srážka	k1gFnSc3	srážka
Merkuru	Merkur	k1gInSc2	Merkur
s	s	k7c7	s
planetisimálou	planetisimála	k1gFnSc7	planetisimála
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
Merkuru	Merkur	k1gInSc2	Merkur
a	a	k8xC	a
velikosti	velikost	k1gFnSc2	velikost
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
odpaření	odpaření	k1gNnSc3	odpaření
většiny	většina	k1gFnSc2	většina
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
pláště	plášť	k1gInSc2	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
srážce	srážka	k1gFnSc6	srážka
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
stalo	stát	k5eAaPmAgNnS	stát
dominantní	dominantní	k2eAgNnSc1d1	dominantní
komponentou	komponenta	k1gFnSc7	komponenta
celé	celá	k1gFnSc2	celá
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
nejspíše	nejspíše	k9	nejspíše
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
době	doba	k1gFnSc6	doba
zformování	zformování	k1gNnSc2	zformování
pozemského	pozemský	k2eAgInSc2d1	pozemský
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
oblast	oblast	k1gFnSc4	oblast
hypotetické	hypotetický	k2eAgFnPc1d1	hypotetická
srážky	srážka	k1gFnPc1	srážka
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
považuje	považovat	k5eAaImIp3nS	považovat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
oblast	oblast	k1gFnSc1	oblast
Caloris	Caloris	k1gFnSc2	Caloris
Basin	Basina	k1gFnPc2	Basina
<g/>
.	.	kIx.	.
<g/>
Jiné	jiný	k2eAgFnPc1d1	jiná
teorie	teorie	k1gFnPc1	teorie
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Merkur	Merkur	k1gMnSc1	Merkur
se	se	k3xPyFc4	se
zformoval	zformovat	k5eAaPmAgMnS	zformovat
jako	jako	k8xS	jako
protoplaneta	protoplaneta	k1gFnSc1	protoplaneta
v	v	k7c6	v
planetární	planetární	k2eAgFnSc6d1	planetární
mlhovině	mlhovina	k1gFnSc6	mlhovina
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
ustálilo	ustálit	k5eAaPmAgNnS	ustálit
a	a	k8xC	a
stabilizovalo	stabilizovat	k5eAaBmAgNnS	stabilizovat
svůj	svůj	k3xOyFgInSc4	svůj
energetický	energetický	k2eAgInSc4d1	energetický
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tak	tak	k9	tak
protoplaneta	protoplaneta	k1gFnSc1	protoplaneta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc4d2	veliký
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
následně	následně	k6eAd1	následně
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kontrakci	kontrakce	k1gFnSc3	kontrakce
protoslunce	protoslunka	k1gFnSc3	protoslunka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Merkuru	Merkur	k1gInSc2	Merkur
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
teplotu	teplota	k1gFnSc4	teplota
mezi	mezi	k7c7	mezi
2500	[number]	k4	2500
až	až	k9	až
3500	[number]	k4	3500
K	K	kA	K
(	(	kIx(	(
<g/>
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
až	až	k9	až
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
K	K	kA	K
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
těchto	tento	k3xDgFnPc2	tento
teplot	teplota	k1gFnPc2	teplota
se	se	k3xPyFc4	se
vypařila	vypařit	k5eAaPmAgFnS	vypařit
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
pláště	plášť	k1gInSc2	plášť
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
atmosféry	atmosféra	k1gFnSc2	atmosféra
Merkuru	Merkur	k1gInSc2	Merkur
tvořené	tvořený	k2eAgNnSc4d1	tvořené
z	z	k7c2	z
plynů	plyn	k1gInPc2	plyn
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
z	z	k7c2	z
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemné	vzájemný	k2eAgFnPc1d1	vzájemná
interakce	interakce	k1gFnPc1	interakce
se	s	k7c7	s
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
následně	následně	k6eAd1	následně
měly	mít	k5eAaImAgFnP	mít
odvát	odvát	k5eAaPmF	odvát
celou	celý	k2eAgFnSc4d1	celá
atmosféru	atmosféra	k1gFnSc4	atmosféra
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
<g/>
Třetí	třetí	k4xOgFnSc1	třetí
hypotéza	hypotéza	k1gFnSc1	hypotéza
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sluneční	sluneční	k2eAgFnSc1d1	sluneční
mlhovina	mlhovina	k1gFnSc1	mlhovina
byla	být	k5eAaImAgFnS	být
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
středu	střed	k1gInSc2	střed
vlivem	vliv	k1gInSc7	vliv
počínající	počínající	k2eAgFnSc2d1	počínající
akrece	akrece	k1gFnSc2	akrece
hustší	hustý	k2eAgFnSc1d2	hustší
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
lehčí	lehký	k2eAgFnPc1d2	lehčí
částice	částice	k1gFnPc1	částice
vytlačovány	vytlačovat	k5eAaImNgFnP	vytlačovat
mimo	mimo	k7c4	mimo
oblast	oblast	k1gFnSc4	oblast
blízkou	blízký	k2eAgFnSc4d1	blízká
k	k	k7c3	k
budoucímu	budoucí	k2eAgNnSc3d1	budoucí
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
materiál	materiál	k1gInSc1	materiál
tvořící	tvořící	k2eAgInSc1d1	tvořící
později	pozdě	k6eAd2	pozdě
Merkur	Merkur	k1gInSc4	Merkur
prodírat	prodírat	k5eAaImF	prodírat
nahuštěným	nahuštěný	k2eAgInSc7d1	nahuštěný
plynem	plyn	k1gInSc7	plyn
<g/>
,	,	kIx,	,
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
vzniku	vznik	k1gInSc2	vznik
Merkuru	Merkur	k1gInSc2	Merkur
tak	tak	k6eAd1	tak
zůstávaly	zůstávat	k5eAaImAgInP	zůstávat
převážně	převážně	k6eAd1	převážně
těžší	těžký	k2eAgInPc4d2	těžší
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
je	být	k5eAaImIp3nS	být
Merkur	Merkur	k1gInSc1	Merkur
nyní	nyní	k6eAd1	nyní
složen	složit	k5eAaPmNgInS	složit
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
předkládaných	předkládaný	k2eAgFnPc2d1	Předkládaná
hypotéz	hypotéza	k1gFnPc2	hypotéza
ale	ale	k8xC	ale
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
jiné	jiný	k2eAgNnSc4d1	jiné
složení	složení	k1gNnSc4	složení
povrchových	povrchový	k2eAgFnPc2d1	povrchová
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
využít	využít	k5eAaPmF	využít
během	během	k7c2	během
experimentů	experiment	k1gInPc2	experiment
sond	sonda	k1gFnPc2	sonda
MESSENGER	MESSENGER	kA	MESSENGER
a	a	k8xC	a
BepiColombo	BepiColomba	k1gMnSc5	BepiColomba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
závěry	závěra	k1gFnPc1	závěra
těchto	tento	k3xDgFnPc2	tento
hypotéz	hypotéza	k1gFnPc2	hypotéza
potvrdit	potvrdit	k5eAaPmF	potvrdit
či	či	k8xC	či
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Povrch	povrch	k1gInSc1	povrch
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Povrch	povrch	k1gInSc1	povrch
Merkuru	Merkur	k1gInSc2	Merkur
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
povrchu	povrch	k1gInSc3	povrch
pozemského	pozemský	k2eAgInSc2d1	pozemský
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
ukázaly	ukázat	k5eAaPmAgInP	ukázat
první	první	k4xOgInPc1	první
detailnější	detailní	k2eAgInPc1d2	detailnější
snímky	snímek	k1gInPc1	snímek
pořízené	pořízený	k2eAgInPc1d1	pořízený
americkou	americký	k2eAgFnSc7d1	americká
sondou	sonda	k1gFnSc7	sonda
Mariner	Mariner	k1gMnSc1	Mariner
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
ho	on	k3xPp3gNnSc4	on
především	především	k6eAd1	především
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
kráterů	kráter	k1gInPc2	kráter
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
srážkou	srážka	k1gFnSc7	srážka
s	s	k7c7	s
meteority	meteorit	k1gInPc7	meteorit
a	a	k8xC	a
planetkami	planetka	k1gFnPc7	planetka
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
velikostí	velikost	k1gFnPc2	velikost
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
impaktní	impaktní	k2eAgInPc1d1	impaktní
krátery	kráter	k1gInPc1	kráter
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Merkuru	Merkur	k1gInSc2	Merkur
nachází	nacházet	k5eAaImIp3nS	nacházet
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
lávové	lávový	k2eAgFnPc4d1	lávová
pláně	pláň	k1gFnPc4	pláň
vyplňující	vyplňující	k2eAgFnPc1d1	vyplňující
některé	některý	k3yIgFnPc1	některý
velké	velký	k2eAgFnPc1d1	velká
impaktní	impaktní	k2eAgFnPc1d1	impaktní
pánve	pánev	k1gFnPc1	pánev
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
Měsíci	měsíc	k1gInSc3	měsíc
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
Merkuru	Merkur	k1gInSc2	Merkur
mnohem	mnohem	k6eAd1	mnohem
tmavší	tmavý	k2eAgInSc1d2	tmavší
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
minerálů	minerál	k1gInPc2	minerál
obsahujících	obsahující	k2eAgInPc2d1	obsahující
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
za	za	k7c4	za
tmavost	tmavost	k1gFnSc4	tmavost
povrchu	povrch	k1gInSc2	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Merkuru	Merkur	k1gInSc2	Merkur
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
tmavost	tmavost	k1gFnSc4	tmavost
jiný	jiný	k2eAgInSc4d1	jiný
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
uhlík	uhlík	k1gInSc4	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
dostal	dostat	k5eAaPmAgMnS	dostat
během	během	k7c2	během
impaktů	impakt	k1gInPc2	impakt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obnažily	obnažit	k5eAaPmAgInP	obnažit
starší	starý	k2eAgFnSc4d2	starší
kůru	kůra	k1gFnSc4	kůra
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
utuhnutím	utuhnutí	k1gNnSc7	utuhnutí
magmatického	magmatický	k2eAgInSc2d1	magmatický
oceánu	oceán	k1gInSc2	oceán
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
vývoje	vývoj	k1gInSc2	vývoj
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Grafit	grafit	k1gInSc1	grafit
<g/>
,	,	kIx,	,
minerál	minerál	k1gInSc1	minerál
tvořený	tvořený	k2eAgInSc1d1	tvořený
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
dostatečně	dostatečně	k6eAd1	dostatečně
nízkou	nízký	k2eAgFnSc4d1	nízká
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vznášet	vznášet	k5eAaImF	vznášet
při	při	k7c6	při
hladině	hladina	k1gFnSc6	hladina
magmatického	magmatický	k2eAgInSc2d1	magmatický
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
povrchovým	povrchový	k2eAgInSc7d1	povrchový
útvarem	útvar	k1gInSc7	útvar
Merkuru	Merkur	k1gInSc2	Merkur
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
1400	[number]	k4	1400
km	km	kA	km
se	se	k3xPyFc4	se
táhnoucí	táhnoucí	k2eAgFnSc1d1	táhnoucí
prohlubeň	prohlubeň	k1gFnSc1	prohlubeň
Caloris	Caloris	k1gFnSc2	Caloris
Basin	Basina	k1gFnPc2	Basina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
kráter	kráter	k1gInSc4	kráter
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Planetu	planeta	k1gFnSc4	planeta
navštívily	navštívit	k5eAaPmAgFnP	navštívit
zatím	zatím	k6eAd1	zatím
pouze	pouze	k6eAd1	pouze
sondy	sonda	k1gFnPc4	sonda
<g/>
:	:	kIx,	:
nejdříve	dříve	k6eAd3	dříve
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
sonda	sonda	k1gFnSc1	sonda
MESSENGER	MESSENGER	kA	MESSENGER
<g/>
.	.	kIx.	.
</s>
<s>
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
prolétl	prolétnout	k5eAaPmAgInS	prolétnout
kolem	kolem	k7c2	kolem
Merkuru	Merkur	k1gInSc2	Merkur
celkem	celkem	k6eAd1	celkem
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
odeslal	odeslat	k5eAaPmAgMnS	odeslat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
přes	přes	k7c4	přes
2700	[number]	k4	2700
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
pokryly	pokrýt	k5eAaPmAgFnP	pokrýt
povrch	povrch	k1gInSc4	povrch
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
45	[number]	k4	45
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
planety	planeta	k1gFnSc2	planeta
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
zmapován	zmapovat	k5eAaPmNgInS	zmapovat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
menší	malý	k2eAgFnSc2d2	menší
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímka	k1gFnPc1	snímka
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
svět	svět	k1gInSc4	svět
podobný	podobný	k2eAgInSc4d1	podobný
Měsíci	měsíc	k1gInSc3	měsíc
s	s	k7c7	s
velice	velice	k6eAd1	velice
starou	starý	k2eAgFnSc7d1	stará
kůrou	kůra	k1gFnSc7	kůra
pokrytou	pokrytý	k2eAgFnSc4d1	pokrytá
značným	značný	k2eAgNnSc7d1	značné
množstvím	množství	k1gNnSc7	množství
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
od	od	k7c2	od
velikosti	velikost	k1gFnSc2	velikost
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
metrů	metr	k1gInPc2	metr
až	až	k6eAd1	až
po	po	k7c4	po
1300	[number]	k4	1300
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Merkuru	Merkur	k1gInSc6	Merkur
nebyl	být	k5eNaImAgInS	být
identifikován	identifikovat	k5eAaBmNgInS	identifikovat
žádný	žádný	k3yNgInSc1	žádný
geologický	geologický	k2eAgInSc1d1	geologický
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
krátery	kráter	k1gInPc4	kráter
omlazovat	omlazovat	k5eAaImF	omlazovat
<g/>
,	,	kIx,	,
jako	jako	k9	jako
např.	např.	kA	např.
desková	deskový	k2eAgFnSc1d1	desková
tektonika	tektonika	k1gFnSc1	tektonika
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
či	či	k8xC	či
lávové	lávový	k2eAgInPc4d1	lávový
výlevy	výlev	k1gInPc4	výlev
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
kráterů	kráter	k1gInPc2	kráter
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
stupni	stupeň	k1gInSc6	stupeň
eroze	eroze	k1gFnSc1	eroze
<g/>
.	.	kIx.	.
</s>
<s>
Merkur	Merkur	k1gInSc1	Merkur
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ale	ale	k8xC	ale
jeví	jevit	k5eAaImIp3nP	jevit
značné	značný	k2eAgFnPc4d1	značná
známky	známka	k1gFnPc4	známka
rozrušení	rozrušení	k1gNnSc2	rozrušení
impakty	impakt	k1gInPc1	impakt
jiných	jiný	k2eAgNnPc2d1	jiné
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Horstva	horstvo	k1gNnPc1	horstvo
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
menšinovým	menšinový	k2eAgInSc7d1	menšinový
činitelem	činitel	k1gInSc7	činitel
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
povrchu	povrch	k1gInSc2	povrch
nejspíše	nejspíše	k9	nejspíše
zabírají	zabírat	k5eAaImIp3nP	zabírat
lávové	lávový	k2eAgFnPc4d1	lávová
planiny	planina	k1gFnPc4	planina
dvojího	dvojí	k4xRgInSc2	dvojí
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
mezikráterové	mezikráterový	k2eAgFnPc1d1	mezikráterový
a	a	k8xC	a
hladké	hladký	k2eAgFnPc1d1	hladká
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
četnosti	četnost	k1gFnSc6	četnost
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
hladké	hladký	k2eAgFnPc1d1	hladká
planiny	planina	k1gFnPc1	planina
jsou	být	k5eAaImIp3nP	být
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
.	.	kIx.	.
<g/>
Oproti	oproti	k7c3	oproti
povrchu	povrch	k1gInSc3	povrch
Měsíce	měsíc	k1gInSc2	měsíc
či	či	k8xC	či
Marsu	Mars	k1gInSc2	Mars
chyběly	chybět	k5eAaImAgInP	chybět
u	u	k7c2	u
Merkuru	Merkur	k1gInSc2	Merkur
doklady	doklad	k1gInPc4	doklad
sopečné	sopečný	k2eAgFnPc1d1	sopečná
aktivity	aktivita	k1gFnSc2	aktivita
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tam	tam	k6eAd1	tam
sonda	sonda	k1gFnSc1	sonda
MESSENGER	MESSENGER	kA	MESSENGER
objevila	objevit	k5eAaPmAgFnS	objevit
sopky	sopka	k1gFnPc4	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
kontrakce	kontrakce	k1gFnSc2	kontrakce
obvodu	obvod	k1gInSc2	obvod
Merkuru	Merkur	k1gInSc2	Merkur
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
planeta	planeta	k1gFnSc1	planeta
zmenšit	zmenšit	k5eAaPmF	zmenšit
až	až	k9	až
o	o	k7c4	o
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
masivním	masivní	k2eAgNnSc7d1	masivní
zvrásněním	zvrásnění	k1gNnSc7	zvrásnění
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
popraskáním	popraskání	k1gNnSc7	popraskání
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
tak	tak	k6eAd1	tak
útesové	útesový	k2eAgInPc1d1	útesový
zlomy	zlom	k1gInPc1	zlom
vysoké	vysoký	k2eAgInPc1d1	vysoký
několik	několik	k4yIc1	několik
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
až	až	k9	až
stovky	stovka	k1gFnPc1	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
chladnutí	chladnutí	k1gNnSc3	chladnutí
planety	planeta	k1gFnSc2	planeta
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
3,8	[number]	k4	3,8
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
její	její	k3xOp3gInSc1	její
průměr	průměr	k1gInSc1	průměr
o	o	k7c4	o
14	[number]	k4	14
km	km	kA	km
na	na	k7c4	na
nynějších	nynější	k2eAgInPc2d1	nynější
4879	[number]	k4	4879
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
povedlo	povést	k5eAaPmAgNnS	povést
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Merkuru	Merkur	k1gInSc2	Merkur
identifikovat	identifikovat	k5eAaBmF	identifikovat
jak	jak	k6eAd1	jak
projevy	projev	k1gInPc4	projev
efuzivního	efuzivní	k2eAgInSc2d1	efuzivní
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
lávových	lávový	k2eAgInPc2d1	lávový
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
explozivního	explozivní	k2eAgInSc2d1	explozivní
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
uloženin	uloženina	k1gFnPc2	uloženina
pyroklastického	pyroklastický	k2eAgInSc2d1	pyroklastický
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
efuzivní	efuzivní	k2eAgInSc1d1	efuzivní
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
byl	být	k5eAaImAgInS	být
aktivní	aktivní	k2eAgInSc1d1	aktivní
převážně	převážně	k6eAd1	převážně
před	před	k7c7	před
~	~	kIx~	~
<g/>
4,1	[number]	k4	4,1
až	až	k9	až
3,55	[number]	k4	3,55
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
explozivní	explozivní	k2eAgMnSc1d1	explozivní
se	se	k3xPyFc4	se
odehrával	odehrávat	k5eAaImAgMnS	odehrávat
od	od	k7c2	od
3,9	[number]	k4	3,9
po	po	k7c4	po
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Atmosféra	atmosféra	k1gFnSc1	atmosféra
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Merkur	Merkur	k1gInSc1	Merkur
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
tenkou	tenký	k2eAgFnSc4d1	tenká
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
atomy	atom	k1gInPc4	atom
vyražené	vyražený	k2eAgNnSc1d1	vyražené
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
povrchu	povrch	k1gInSc2	povrch
slunečním	sluneční	k2eAgInSc7d1	sluneční
větrem	vítr	k1gInSc7	vítr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
důsledek	důsledek	k1gInSc4	důsledek
slabého	slabý	k2eAgNnSc2d1	slabé
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
poměrně	poměrně	k6eAd1	poměrně
lehké	lehký	k2eAgFnPc1d1	lehká
planety	planeta	k1gFnPc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
Merkuru	Merkur	k1gInSc2	Merkur
velmi	velmi	k6eAd1	velmi
horký	horký	k2eAgInSc1d1	horký
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
atomy	atom	k1gInPc1	atom
rychle	rychle	k6eAd1	rychle
unikají	unikat	k5eAaImIp3nP	unikat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
–	–	k?	–
oproti	oproti	k7c3	oproti
Zemi	zem	k1gFnSc3	zem
nebo	nebo	k8xC	nebo
Venuši	Venuše	k1gFnSc6	Venuše
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
atmosféry	atmosféra	k1gFnPc1	atmosféra
jsou	být	k5eAaImIp3nP	být
stabilní	stabilní	k2eAgFnSc4d1	stabilní
–	–	k?	–
atmosféra	atmosféra	k1gFnSc1	atmosféra
Merkuru	Merkur	k1gInSc2	Merkur
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
neustále	neustále	k6eAd1	neustále
doplňována	doplňovat	k5eAaImNgFnS	doplňovat
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
atmosféry	atmosféra	k1gFnSc2	atmosféra
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
10	[number]	k4	10
Pa	Pa	kA	Pa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
pozemském	pozemský	k2eAgNnSc6d1	pozemské
měřítku	měřítko	k1gNnSc6	měřítko
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
tlaku	tlak	k1gInSc3	tlak
ultra-nízkému	ultraízký	k2eAgInSc3d1	ultra-nízký
(	(	kIx(	(
<g/>
daleko	daleko	k6eAd1	daleko
vyšší	vysoký	k2eAgInSc1d2	vyšší
tlak	tlak	k1gInSc1	tlak
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
vakuum	vakuum	k1gNnSc1	vakuum
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
žárovce	žárovka	k1gFnSc6	žárovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
planeta	planeta	k1gFnSc1	planeta
nemá	mít	k5eNaImIp3nS	mít
silnější	silný	k2eAgFnSc4d2	silnější
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
obloha	obloha	k1gFnSc1	obloha
na	na	k7c6	na
Merkuru	Merkur	k1gInSc6	Merkur
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
především	především	k6eAd1	především
z	z	k7c2	z
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
sodíku	sodík	k1gInSc2	sodík
<g/>
,	,	kIx,	,
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Helium	helium	k1gNnSc1	helium
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ze	z	k7c2	z
slunečního	sluneční	k2eAgInSc2d1	sluneční
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
část	část	k1gFnSc1	část
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
také	také	k9	také
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgInPc1d1	ostatní
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
uvolňovány	uvolňovat	k5eAaImNgInP	uvolňovat
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
doneseného	donesený	k2eAgInSc2d1	donesený
meteoritického	meteoritický	k2eAgInSc2d1	meteoritický
materiálu	materiál	k1gInSc2	materiál
fotoionizací	fotoionizace	k1gFnPc2	fotoionizace
dopadajícím	dopadající	k2eAgNnSc7d1	dopadající
slunečním	sluneční	k2eAgNnSc7d1	sluneční
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
byly	být	k5eAaImAgFnP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
i	i	k8xC	i
nízké	nízký	k2eAgInPc1d1	nízký
obsahy	obsah	k1gInPc1	obsah
molekul	molekula	k1gFnPc2	molekula
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
sopečnou	sopečný	k2eAgFnSc4d1	sopečná
aktivitu	aktivita	k1gFnSc4	aktivita
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Merkurova	Merkurův	k2eAgFnSc1d1	Merkurova
atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
řídká	řídký	k2eAgFnSc1d1	řídká
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomy	atom	k1gInPc1	atom
plynů	plyn	k1gInPc2	plyn
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
balistických	balistický	k2eAgFnPc6d1	balistická
drahách	draha	k1gFnPc6	draha
a	a	k8xC	a
daleko	daleko	k6eAd1	daleko
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
srážejí	srážet	k5eAaImIp3nP	srážet
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
planety	planeta	k1gFnSc2	planeta
než	než	k8xS	než
samy	sám	k3xTgInPc4	sám
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
<g/>
Vlivem	vliv	k1gInSc7	vliv
velice	velice	k6eAd1	velice
nízké	nízký	k2eAgFnSc2d1	nízká
hustoty	hustota	k1gFnSc2	hustota
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
vakuum	vakuum	k1gNnSc4	vakuum
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
Merkurově	Merkurův	k2eAgFnSc6d1	Merkurova
atmosféře	atmosféra	k1gFnSc6	atmosféra
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
žádné	žádný	k3yNgInPc4	žádný
meteorologické	meteorologický	k2eAgInPc4d1	meteorologický
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Teplota	teplota	k1gFnSc1	teplota
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
planeta	planeta	k1gFnSc1	planeta
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
žádnou	žádný	k3yNgFnSc4	žádný
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
dokázala	dokázat	k5eAaPmAgFnS	dokázat
udržet	udržet	k5eAaPmF	udržet
stabilní	stabilní	k2eAgFnSc4d1	stabilní
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
k	k	k7c3	k
silným	silný	k2eAgInPc3d1	silný
teplotním	teplotní	k2eAgInPc3d1	teplotní
výkyvům	výkyv	k1gInPc3	výkyv
<g/>
,	,	kIx,	,
největším	veliký	k2eAgNnSc6d3	veliký
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
klesá	klesat	k5eAaImIp3nS	klesat
až	až	k9	až
k	k	k7c3	k
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
narůstá	narůstat	k5eAaImIp3nS	narůstat
až	až	k6eAd1	až
k	k	k7c3	k
430	[number]	k4	430
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
osluněné	osluněný	k2eAgFnSc6d1	osluněná
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
diametrální	diametrální	k2eAgFnPc1d1	diametrální
změny	změna	k1gFnPc1	změna
teploty	teplota	k1gFnSc2	teplota
během	během	k7c2	během
dne	den	k1gInSc2	den
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
několika	několik	k4yIc2	několik
faktory	faktor	k1gInPc7	faktor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
rotace	rotace	k1gFnSc1	rotace
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
Merkuru	Merkur	k1gInSc2	Merkur
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
176	[number]	k4	176
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Voda	voda	k1gFnSc1	voda
===	===	k?	===
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Merkuru	Merkur	k1gInSc2	Merkur
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nemůže	moct	k5eNaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
blízkost	blízkost	k1gFnSc4	blízkost
Slunce	slunce	k1gNnSc2	slunce
totiž	totiž	k9	totiž
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
výskytu	výskyt	k1gInSc3	výskyt
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Výkonné	výkonný	k2eAgInPc4d1	výkonný
radioteleskopy	radioteleskop	k1gInPc4	radioteleskop
i	i	k8xC	i
měření	měření	k1gNnSc4	měření
sondy	sonda	k1gFnSc2	sonda
Mariner	Mariner	k1gInSc4	Mariner
10	[number]	k4	10
však	však	k9	však
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
navzdory	navzdory	k7c3	navzdory
obrovským	obrovský	k2eAgFnPc3d1	obrovská
povrchovým	povrchový	k2eAgFnPc3d1	povrchová
teplotám	teplota	k1gFnPc3	teplota
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
na	na	k7c6	na
Merkuru	Merkur	k1gInSc6	Merkur
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
</s>
<s>
Rotační	rotační	k2eAgFnSc1d1	rotační
osa	osa	k1gFnSc1	osa
Merkuru	Merkur	k1gInSc2	Merkur
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
téměř	téměř	k6eAd1	téměř
kolmá	kolmý	k2eAgFnSc1d1	kolmá
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
velkých	velký	k2eAgInPc2d1	velký
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
pólů	pól	k1gInPc2	pól
nikdy	nikdy	k6eAd1	nikdy
nesvítí	svítit	k5eNaImIp3nS	svítit
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
soustavně	soustavně	k6eAd1	soustavně
drží	držet	k5eAaImIp3nS	držet
na	na	k7c4	na
−	−	k?	−
°	°	k?	°
<g/>
C.	C.	kA	C.
Tato	tento	k3xDgFnSc1	tento
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
na	na	k7c4	na
Merkur	Merkur	k1gInSc4	Merkur
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dostala	dostat	k5eAaPmAgFnS	dostat
při	při	k7c6	při
srážkách	srážka	k1gFnPc6	srážka
s	s	k7c7	s
jádry	jádro	k1gNnPc7	jádro
komet	kometa	k1gFnPc2	kometa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
komety	kometa	k1gFnSc2	kometa
mohla	moct	k5eAaImAgFnS	moct
dostat	dostat	k5eAaPmF	dostat
pod	pod	k7c4	pod
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
překrytého	překrytý	k2eAgInSc2d1	překrytý
jemným	jemný	k2eAgInSc7d1	jemný
prachem	prach	k1gInSc7	prach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
tepelná	tepelný	k2eAgFnSc1d1	tepelná
izolace	izolace	k1gFnSc1	izolace
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
planetologové	planetolog	k1gMnPc1	planetolog
Duane	Duan	k1gInSc5	Duan
Mulhelm	Mulhelm	k1gMnSc1	Mulhelm
a	a	k8xC	a
Bryan	Bryan	k1gMnSc1	Bryan
Butler	Butler	k1gMnSc1	Butler
studovali	studovat	k5eAaImAgMnP	studovat
nezmapované	zmapovaný	k2eNgFnSc3d1	nezmapovaná
oblasti	oblast	k1gFnSc3	oblast
Merkuru	Merkur	k1gInSc2	Merkur
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
sedmdesátimetrové	sedmdesátimetrový	k2eAgFnSc2d1	sedmdesátimetrová
antény	anténa	k1gFnSc2	anténa
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
Goldstone	Goldston	k1gInSc5	Goldston
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
překvapení	překvapení	k1gNnSc3	překvapení
naměřili	naměřit	k5eAaBmAgMnP	naměřit
silný	silný	k2eAgInSc4d1	silný
odraz	odraz	k1gInSc4	odraz
vysílaného	vysílaný	k2eAgInSc2d1	vysílaný
radarového	radarový	k2eAgInSc2d1	radarový
signálu	signál	k1gInSc2	signál
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
podobal	podobat	k5eAaImAgMnS	podobat
odrazům	odraz	k1gInPc3	odraz
zjištěným	zjištěný	k2eAgInSc7d1	zjištěný
u	u	k7c2	u
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
stejné	stejný	k2eAgNnSc1d1	stejné
pozorování	pozorování	k1gNnSc1	pozorování
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přineslo	přinést	k5eAaPmAgNnS	přinést
podobné	podobný	k2eAgInPc4d1	podobný
výsledky	výsledek	k1gInPc4	výsledek
i	i	k9	i
pro	pro	k7c4	pro
oblasti	oblast	k1gFnPc4	oblast
okolo	okolo	k7c2	okolo
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
a	a	k8xC	a
radiace	radiace	k1gFnSc1	radiace
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
detekovala	detekovat	k5eAaImAgFnS	detekovat
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
Merkuru	Merkur	k1gInSc2	Merkur
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
asi	asi	k9	asi
1	[number]	k4	1
%	%	kIx~	%
zemského	zemský	k2eAgInSc2d1	zemský
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
vědecký	vědecký	k2eAgInSc4d1	vědecký
svět	svět	k1gInSc4	svět
překvapením	překvapení	k1gNnSc7	překvapení
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
malá	malý	k2eAgFnSc1d1	malá
planeta	planeta	k1gFnSc1	planeta
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
pevné	pevný	k2eAgNnSc1d1	pevné
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
vychladlo	vychladnout	k5eAaPmAgNnS	vychladnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nevytváří	vytvářet	k5eNaImIp3nS	vytvářet
magnetismus	magnetismus	k1gInSc4	magnetismus
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
sondy	sonda	k1gFnSc2	sonda
znamenal	znamenat	k5eAaImAgInS	znamenat
přehodnocení	přehodnocení	k1gNnSc4	přehodnocení
tohoto	tento	k3xDgInSc2	tento
předpokladu	předpoklad	k1gInSc2	předpoklad
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
existence	existence	k1gFnSc2	existence
většího	veliký	k2eAgInSc2d2	veliký
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
roztaveného	roztavený	k2eAgNnSc2d1	roztavené
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	k9	by
rozdílnou	rozdílný	k2eAgFnSc7d1	rozdílná
rychlostí	rychlost	k1gFnSc7	rychlost
rotace	rotace	k1gFnSc2	rotace
mohlo	moct	k5eAaImAgNnS	moct
generovat	generovat	k5eAaImF	generovat
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
planety	planeta	k1gFnSc2	planeta
na	na	k7c6	na
principu	princip	k1gInSc6	princip
dynama	dynamo	k1gNnSc2	dynamo
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
vůči	vůči	k7c3	vůči
rotační	rotační	k2eAgFnSc3d1	rotační
ose	osa	k1gFnSc3	osa
Merkuru	Merkur	k1gInSc2	Merkur
skloněné	skloněný	k2eAgFnPc4d1	skloněná
o	o	k7c4	o
7	[number]	k4	7
stupňů	stupeň	k1gInPc2	stupeň
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
silné	silný	k2eAgNnSc1d1	silné
<g/>
,	,	kIx,	,
že	že	k8xS	že
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vznik	vznik	k1gInSc4	vznik
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
okolo	okolo	k7c2	okolo
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odklání	odklánět	k5eAaImIp3nS	odklánět
sluneční	sluneční	k2eAgInSc4d1	sluneční
vítr	vítr	k1gInSc4	vítr
<g/>
.	.	kIx.	.
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiná	jiný	k2eAgNnPc1d1	jiné
vysvětlení	vysvětlení	k1gNnPc1	vysvětlení
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
obejdou	obejít	k5eAaPmIp3nP	obejít
bez	bez	k7c2	bez
hypotézy	hypotéza	k1gFnSc2	hypotéza
jejího	její	k3xOp3gNnSc2	její
většího	veliký	k2eAgNnSc2d2	veliký
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vybuzováno	vybuzovat	k5eAaImNgNnS	vybuzovat
remanentní	remanentní	k2eAgFnSc7d1	remanentní
magnetizací	magnetizace	k1gFnSc7	magnetizace
hornin	hornina	k1gFnPc2	hornina
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
zmagnetizovány	zmagnetizovat	k5eAaPmNgInP	zmagnetizovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
<g/>
Jak	jak	k6eAd1	jak
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
měření	měření	k1gNnSc3	měření
další	další	k2eAgFnSc2d1	další
sondy	sonda	k1gFnSc2	sonda
MESSENGER	MESSENGER	kA	MESSENGER
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
průletů	průlet	k1gInPc2	průlet
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
Merkuru	Merkur	k1gInSc2	Merkur
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgNnSc1d2	menší
než	než	k8xS	než
pozemské	pozemský	k2eAgNnSc1d1	pozemské
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
počítačové	počítačový	k2eAgFnPc4d1	počítačová
modelace	modelace	k1gFnPc4	modelace
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
výpočetní	výpočetní	k2eAgFnSc1d1	výpočetní
kapacita	kapacita	k1gFnSc1	kapacita
počítačů	počítač	k1gMnPc2	počítač
nestačí	stačit	k5eNaBmIp3nS	stačit
na	na	k7c4	na
numerické	numerický	k2eAgNnSc4d1	numerické
modelování	modelování	k1gNnSc4	modelování
pozemského	pozemský	k2eAgNnSc2d1	pozemské
magnetického	magnetický	k2eAgNnSc2d1	magnetické
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
modeluje	modelovat	k5eAaImIp3nS	modelovat
snadnější	snadný	k2eAgNnSc4d2	snazší
pole	pole	k1gNnSc4	pole
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
snahu	snaha	k1gFnSc4	snaha
objasnit	objasnit	k5eAaPmF	objasnit
tajemství	tajemství	k1gNnSc4	tajemství
spojená	spojený	k2eAgNnPc4d1	spojené
s	s	k7c7	s
pozemským	pozemský	k2eAgNnSc7d1	pozemské
polem	pole	k1gNnSc7	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Merkur	Merkur	k1gInSc1	Merkur
obíhá	obíhat	k5eAaImIp3nS	obíhat
Slunce	slunce	k1gNnSc4	slunce
po	po	k7c6	po
eliptické	eliptický	k2eAgFnSc6d1	eliptická
dráze	dráha	k1gFnSc6	dráha
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
velkou	velký	k2eAgFnSc7d1	velká
excentricitou	excentricita	k1gFnSc7	excentricita
dosahující	dosahující	k2eAgInSc1d1	dosahující
0,2056	[number]	k4	0,2056
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
výstřednost	výstřednost	k1gFnSc1	výstřednost
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
perihélia	perihélium	k1gNnSc2	perihélium
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
24	[number]	k4	24
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
blíže	blízce	k6eAd2	blízce
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
než	než	k8xS	než
v	v	k7c6	v
době	doba	k1gFnSc6	doba
afélia	afélium	k1gNnSc2	afélium
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
centrální	centrální	k2eAgFnSc2d1	centrální
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
57,9	[number]	k4	57,9
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
planeta	planeta	k1gFnSc1	planeta
urazí	urazit	k5eAaPmIp3nS	urazit
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
87,969	[number]	k4	87,969
dne	den	k1gInSc2	den
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
rychlostí	rychlost	k1gFnSc7	rychlost
oběhu	oběh	k1gInSc2	oběh
47,87	[number]	k4	47,87
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
<g/>
Vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
a	a	k8xC	a
oddaluje	oddalovat	k5eAaImIp3nS	oddalovat
s	s	k7c7	s
maximálním	maximální	k2eAgInSc7d1	maximální
rozdílem	rozdíl	k1gInSc7	rozdíl
145	[number]	k4	145
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
na	na	k7c6	na
pozorovatelnosti	pozorovatelnost	k1gFnSc6	pozorovatelnost
planety	planeta	k1gFnSc2	planeta
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kolísá	kolísat	k5eAaImIp3nS	kolísat
její	její	k3xOp3gFnSc1	její
úhlová	úhlový	k2eAgFnSc1d1	úhlová
velikost	velikost	k1gFnSc1	velikost
mezi	mezi	k7c7	mezi
5	[number]	k4	5
<g/>
"	"	kIx"	"
až	až	k9	až
15	[number]	k4	15
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
Merkur	Merkur	k1gInSc1	Merkur
mnohem	mnohem	k6eAd1	mnohem
blíže	blízce	k6eAd2	blízce
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
jeví	jevit	k5eAaImIp3nS	jevit
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
obloze	obloha	k1gFnSc6	obloha
až	až	k9	až
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
krát	krát	k6eAd1	krát
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
na	na	k7c4	na
pozemské	pozemský	k2eAgNnSc4d1	pozemské
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Stáčení	stáčení	k1gNnPc2	stáčení
perihelia	perihelium	k1gNnSc2	perihelium
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Měření	měření	k1gNnSc1	měření
parametrů	parametr	k1gInPc2	parametr
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Merkuru	Merkur	k1gInSc2	Merkur
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
důkazů	důkaz	k1gInPc2	důkaz
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Merkur	Merkur	k1gInSc1	Merkur
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
výstřednou	výstředný	k2eAgFnSc4d1	výstředná
dráhu	dráha	k1gFnSc4	dráha
a	a	k8xC	a
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
perihelium	perihelium	k1gNnSc1	perihelium
jeho	jeho	k3xOp3gFnSc2	jeho
dráhy	dráha	k1gFnSc2	dráha
stáčí	stáčet	k5eAaImIp3nS	stáčet
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
stupeň	stupeň	k1gInSc4	stupeň
za	za	k7c4	za
6	[number]	k4	6
pozemských	pozemský	k2eAgNnPc2d1	pozemské
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
stáčení	stáčení	k1gNnSc1	stáčení
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
plně	plně	k6eAd1	plně
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
působením	působení	k1gNnSc7	působení
ostatních	ostatní	k2eAgFnPc2d1	ostatní
planet	planeta	k1gFnPc2	planeta
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Newtonových	Newtonových	k2eAgInPc2d1	Newtonových
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
započtení	započtení	k1gNnSc6	započtení
všech	všecek	k3xTgInPc2	všecek
vlivů	vliv	k1gInPc2	vliv
zbývala	zbývat	k5eAaImAgFnS	zbývat
nevysvětlená	vysvětlený	k2eNgFnSc1d1	nevysvětlená
odchylka	odchylka	k1gFnSc1	odchylka
43	[number]	k4	43
obloukových	obloukový	k2eAgFnPc2d1	oblouková
vteřin	vteřina	k1gFnPc2	vteřina
za	za	k7c2	za
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
astronomové	astronom	k1gMnPc1	astronom
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
působí	působit	k5eAaImIp3nS	působit
další	další	k2eAgNnSc1d1	další
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
neznámá	známý	k2eNgFnSc1d1	neznámá
planeta	planeta	k1gFnSc1	planeta
(	(	kIx(	(
<g/>
které	který	k3yQgFnPc4	který
říkali	říkat	k5eAaImAgMnP	říkat
Vulkan	Vulkan	k1gMnSc1	Vulkan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
Einsteinova	Einsteinův	k2eAgFnSc1d1	Einsteinova
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
důvod	důvod	k1gInSc1	důvod
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
plně	plně	k6eAd1	plně
vysvětlila	vysvětlit	k5eAaPmAgFnS	vysvětlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rotace	rotace	k1gFnSc2	rotace
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
spolehlivě	spolehlivě	k6eAd1	spolehlivě
určit	určit	k5eAaPmF	určit
rotační	rotační	k2eAgFnSc4d1	rotační
dobu	doba	k1gFnSc4	doba
planety	planeta	k1gFnSc2	planeta
na	na	k7c4	na
59	[number]	k4	59
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pomocí	pomocí	k7c2	pomocí
nových	nový	k2eAgInPc2d1	nový
výkonných	výkonný	k2eAgInPc2d1	výkonný
radioteleskopů	radioteleskop	k1gInPc2	radioteleskop
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Merkur	Merkur	k1gInSc1	Merkur
má	mít	k5eAaImIp3nS	mít
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
přivrácena	přivrácen	k2eAgFnSc1d1	přivrácen
stále	stále	k6eAd1	stále
stejná	stejný	k2eAgFnSc1d1	stejná
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
vystavována	vystavovat	k5eAaImNgFnS	vystavovat
extrémnímu	extrémní	k2eAgInSc3d1	extrémní
žáru	žár	k1gInSc6	žár
a	a	k8xC	a
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
chladu	chlad	k1gInSc3	chlad
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
teleskopů	teleskop	k1gInPc2	teleskop
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
rotuje	rotovat	k5eAaImIp3nS	rotovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
velice	velice	k6eAd1	velice
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jedna	jeden	k4xCgFnSc1	jeden
otočka	otočka	k1gFnSc1	otočka
zabírá	zabírat	k5eAaImIp3nS	zabírat
58,646	[number]	k4	58,646
pozemského	pozemský	k2eAgInSc2d1	pozemský
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
pomalé	pomalý	k2eAgFnSc2d1	pomalá
rotace	rotace	k1gFnSc2	rotace
trvá	trvat	k5eAaImIp3nS	trvat
jeden	jeden	k4xCgMnSc1	jeden
sluneční	sluneční	k2eAgInSc1d1	sluneční
den	den	k1gInSc1	den
na	na	k7c6	na
Merkuru	Merkur	k1gInSc6	Merkur
176	[number]	k4	176
pozemských	pozemský	k2eAgInPc2d1	pozemský
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
Merkur	Merkur	k1gInSc1	Merkur
točit	točit	k5eAaImF	točit
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
a	a	k8xC	a
že	že	k8xS	že
den	den	k1gInSc1	den
mohl	moct	k5eAaImAgInS	moct
trvat	trvat	k5eAaImF	trvat
pouhých	pouhý	k2eAgFnPc2d1	pouhá
osm	osm	k4xCc1	osm
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
Slunce	slunce	k1gNnSc2	slunce
ale	ale	k8xC	ale
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prodloužení	prodloužení	k1gNnSc3	prodloužení
dne	den	k1gInSc2	den
až	až	k9	až
na	na	k7c4	na
dnešní	dnešní	k2eAgFnPc4d1	dnešní
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
rotační	rotační	k2eAgFnSc1d1	rotační
osa	osa	k1gFnSc1	osa
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
kolmá	kolmý	k2eAgFnSc1d1	kolmá
na	na	k7c4	na
rovinu	rovina	k1gFnSc4	rovina
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgInPc4	všechen
body	bod	k1gInPc4	bod
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Merkuru	Merkur	k1gInSc2	Merkur
stejně	stejně	k6eAd1	stejně
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
den	den	k1gInSc1	den
<g/>
.	.	kIx.	.
</s>
<s>
Merkur	Merkur	k1gInSc1	Merkur
má	mít	k5eAaImIp3nS	mít
vůči	vůči	k7c3	vůči
Slunci	slunce	k1gNnSc3	slunce
tzv.	tzv.	kA	tzv.
rotační	rotační	k2eAgFnSc4d1	rotační
rezonanci	rezonance	k1gFnSc4	rezonance
s	s	k7c7	s
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dobu	doba	k1gFnSc4	doba
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
důsledek	důsledek	k1gInSc4	důsledek
hypotetické	hypotetický	k2eAgFnSc2d1	hypotetická
srážky	srážka	k1gFnSc2	srážka
s	s	k7c7	s
velikou	veliký	k2eAgFnSc7d1	veliká
planetisimálou	planetisimála	k1gFnSc7	planetisimála
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
historii	historie	k1gFnSc6	historie
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
srážky	srážka	k1gFnSc2	srážka
a	a	k8xC	a
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
složení	složení	k1gNnSc2	složení
mohla	moct	k5eAaImAgFnS	moct
planeta	planeta	k1gFnSc1	planeta
ztratit	ztratit	k5eAaPmF	ztratit
sférickou	sférický	k2eAgFnSc4d1	sférická
symetrii	symetrie	k1gFnSc4	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
Slunce	slunce	k1gNnSc2	slunce
však	však	k9	však
přesto	přesto	k8xC	přesto
nebyly	být	k5eNaImAgFnP	být
schopny	schopen	k2eAgFnPc1d1	schopna
vytvořit	vytvořit	k5eAaPmF	vytvořit
z	z	k7c2	z
Merkuru	Merkur	k1gInSc2	Merkur
těleso	těleso	k1gNnSc1	těleso
s	s	k7c7	s
vázanou	vázaný	k2eAgFnSc7d1	vázaná
rotací	rotace	k1gFnSc7	rotace
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
například	například	k6eAd1	například
pozemský	pozemský	k2eAgInSc4d1	pozemský
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nastala	nastat	k5eAaPmAgFnS	nastat
tak	tak	k9	tak
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
perihéliu	perihélium	k1gNnSc6	perihélium
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
střídavě	střídavě	k6eAd1	střídavě
přivrácena	přivrácen	k2eAgFnSc1d1	přivrácen
a	a	k8xC	a
odvrácena	odvrácen	k2eAgFnSc1d1	odvrácena
hmotnější	hmotný	k2eAgFnSc1d2	hmotnější
část	část	k1gFnSc1	část
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Přechod	přechod	k1gInSc1	přechod
Merkuru	Merkur	k1gInSc2	Merkur
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Merkur	Merkur	k1gInSc1	Merkur
se	se	k3xPyFc4	se
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
cyklech	cyklus	k1gInPc6	cyklus
ocitá	ocitat	k5eAaImIp3nS	ocitat
mezi	mezi	k7c7	mezi
Sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
sledovat	sledovat	k5eAaImF	sledovat
jeho	on	k3xPp3gInSc4	on
přechod	přechod	k1gInSc4	přechod
přes	přes	k7c4	přes
sluneční	sluneční	k2eAgInSc4d1	sluneční
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Úkaz	úkaz	k1gInSc1	úkaz
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
Merkur	Merkur	k1gInSc1	Merkur
přejde	přejít	k5eAaPmIp3nS	přejít
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
okraji	okraj	k1gInSc6	okraj
slunečního	sluneční	k2eAgInSc2d1	sluneční
kotouče	kotouč	k1gInSc2	kotouč
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
částečný	částečný	k2eAgInSc1d1	částečný
přechod	přechod	k1gInSc1	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Úplné	úplný	k2eAgInPc1d1	úplný
přechody	přechod	k1gInPc1	přechod
Merkuru	Merkur	k1gInSc2	Merkur
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
třináctkrát	třináctkrát	k6eAd1	třináctkrát
či	či	k8xC	či
čtrnáctkrát	čtrnáctkrát	k6eAd1	čtrnáctkrát
za	za	k7c2	za
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
nebo	nebo	k8xC	nebo
listopadu	listopad	k1gInSc6	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Květnové	květnový	k2eAgInPc1d1	květnový
přechody	přechod	k1gInPc1	přechod
nastávají	nastávat	k5eAaImIp3nP	nastávat
asi	asi	k9	asi
měsíc	měsíc	k1gInSc4	měsíc
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Merkur	Merkur	k1gInSc1	Merkur
projde	projít	k5eAaPmIp3nS	projít
svým	svůj	k3xOyFgNnSc7	svůj
aféliem	afélium	k1gNnSc7	afélium
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdáleny	vzdálen	k2eAgInPc4d1	vzdálen
13	[number]	k4	13
nebo	nebo	k8xC	nebo
33	[number]	k4	33
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Listopadové	listopadový	k2eAgInPc1d1	listopadový
přechody	přechod	k1gInPc1	přechod
nastávají	nastávat	k5eAaImIp3nP	nastávat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
Merkur	Merkur	k1gInSc1	Merkur
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
svého	svůj	k3xOyFgNnSc2	svůj
perihélia	perihélium	k1gNnSc2	perihélium
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
dvakrát	dvakrát	k6eAd1	dvakrát
častější	častý	k2eAgFnPc1d2	častější
než	než	k8xS	než
květnové	květnový	k2eAgFnPc1d1	květnová
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
od	od	k7c2	od
sebe	se	k3xPyFc2	se
vzdáleny	vzdálit	k5eAaPmNgInP	vzdálit
7	[number]	k4	7
<g/>
,	,	kIx,	,
13	[number]	k4	13
nebo	nebo	k8xC	nebo
33	[number]	k4	33
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Pozorování	pozorování	k1gNnSc2	pozorování
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Merkur	Merkur	k1gInSc1	Merkur
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
přinejmenším	přinejmenším	k6eAd1	přinejmenším
již	již	k9	již
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
starověkých	starověký	k2eAgInPc2d1	starověký
Sumerů	Sumer	k1gInPc2	Sumer
asi	asi	k9	asi
3	[number]	k4	3
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Jejich	jejich	k3xOp3gNnSc4	jejich
pozorování	pozorování	k1gNnSc4	pozorování
jsou	být	k5eAaImIp3nP	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
klínovým	klínový	k2eAgNnSc7d1	klínové
písmem	písmo	k1gNnSc7	písmo
na	na	k7c6	na
hliněných	hliněný	k2eAgFnPc6d1	hliněná
tabulkách	tabulka	k1gFnPc6	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
Merkur	Merkur	k1gInSc1	Merkur
pozorován	pozorovat	k5eAaImNgInS	pozorovat
ve	v	k7c6	v
starověkém	starověký	k2eAgNnSc6d1	starověké
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
vžily	vžít	k5eAaPmAgInP	vžít
názvy	název	k1gInPc1	název
Σ	Σ	k?	Σ
(	(	kIx(	(
<g/>
Stilbón	Stilbón	k1gMnSc1	Stilbón
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ἑ	Ἑ	k?	Ἑ
(	(	kIx(	(
<g/>
Hermaón	Hermaón	k1gMnSc1	Hermaón
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
Řeky	řeka	k1gFnSc2	řeka
zván	zván	k2eAgMnSc1d1	zván
Apollón	Apollón	k1gMnSc1	Apollón
po	po	k7c6	po
stejnojmenném	stejnojmenný	k2eAgMnSc6d1	stejnojmenný
bohu	bůh	k1gMnSc6	bůh
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
viděn	vidět	k5eAaImNgMnS	vidět
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
či	či	k8xC	či
Hermés	Hermés	k1gInSc1	Hermés
v	v	k7c6	v
době	doba	k1gFnSc6	doba
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
ale	ale	k8xC	ale
antičtí	antický	k2eAgMnPc1d1	antický
astronomové	astronom	k1gMnPc1	astronom
poznali	poznat	k5eAaPmAgMnP	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgNnPc4	tento
dvě	dva	k4xCgNnPc4	dva
tělesa	těleso	k1gNnPc4	těleso
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
těleso	těleso	k1gNnSc1	těleso
jedno	jeden	k4xCgNnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
později	pozdě	k6eAd2	pozdě
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
planetu	planeta	k1gFnSc4	planeta
po	po	k7c6	po
poslovi	posel	k1gMnSc6	posel
bohů	bůh	k1gMnPc2	bůh
Merkurovi	Merkurův	k2eAgMnPc1d1	Merkurův
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
se	se	k3xPyFc4	se
Merkur	Merkur	k1gInSc1	Merkur
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
planety	planeta	k1gFnPc1	planeta
<g/>
.	.	kIx.	.
<g/>
Vhodné	vhodný	k2eAgFnPc1d1	vhodná
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
pozorování	pozorování	k1gNnSc4	pozorování
Merkuru	Merkur	k1gInSc2	Merkur
nastávají	nastávat	k5eAaImIp3nP	nastávat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
východu	východ	k1gInSc2	východ
nebo	nebo	k8xC	nebo
západu	západ	k1gInSc2	západ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnPc4	jeho
pozorování	pozorování	k1gNnPc4	pozorování
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Merkur	Merkur	k1gInSc1	Merkur
právě	právě	k9	právě
Slunce	slunce	k1gNnSc1	slunce
dobíhá	dobíhat	k5eAaImIp3nS	dobíhat
nebo	nebo	k8xC	nebo
předbíhá	předbíhat	k5eAaImIp3nS	předbíhat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
Slunce	slunce	k1gNnSc1	slunce
dobíhá	dobíhat	k5eAaImIp3nS	dobíhat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
po	po	k7c4	po
několik	několik	k4yIc4	několik
minut	minuta	k1gFnPc2	minuta
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Slunce	slunce	k1gNnSc1	slunce
zapadlo	zapadnout	k5eAaPmAgNnS	zapadnout
za	za	k7c4	za
horizont	horizont	k1gInSc4	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
Slunce	slunce	k1gNnSc1	slunce
předbíhá	předbíhat	k5eAaImIp3nS	předbíhat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
planetu	planeta	k1gFnSc4	planeta
chvíli	chvíle	k1gFnSc3	chvíle
před	před	k7c7	před
východem	východ	k1gInSc7	východ
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
než	než	k8xS	než
zanikne	zaniknout	k5eAaPmIp3nS	zaniknout
v	v	k7c6	v
narůstajícím	narůstající	k2eAgNnSc6d1	narůstající
slunečním	sluneční	k2eAgNnSc6d1	sluneční
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Planetu	planeta	k1gFnSc4	planeta
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
či	či	k8xC	či
triedrem	triedr	k1gInSc7	triedr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Merkur	Merkur	k1gInSc1	Merkur
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevzdaluje	vzdalovat	k5eNaImIp3nS	vzdalovat
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
dále	daleko	k6eAd2	daleko
než	než	k8xS	než
na	na	k7c4	na
28	[number]	k4	28
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
jeho	jeho	k3xOp3gNnSc4	jeho
přímé	přímý	k2eAgNnSc4d1	přímé
pozorování	pozorování	k1gNnSc4	pozorování
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
s	s	k7c7	s
větším	veliký	k2eAgNnSc7d2	veliký
rozlišením	rozlišení	k1gNnSc7	rozlišení
a	a	k8xC	a
přesností	přesnost	k1gFnSc7	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Jasnost	jasnost	k1gFnSc1	jasnost
Merkuru	Merkur	k1gInSc2	Merkur
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
od	od	k7c2	od
1,7	[number]	k4	1,7
do	do	k7c2	do
−	−	k?	−
magnitudy	magnituda	k1gFnSc2	magnituda
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
Měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
u	u	k7c2	u
Merkuru	Merkur	k1gInSc2	Merkur
pozorovat	pozorovat	k5eAaImF	pozorovat
měnící	měnící	k2eAgFnSc4d1	měnící
se	se	k3xPyFc4	se
fáze	fáze	k1gFnSc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
pozorování	pozorování	k1gNnPc4	pozorování
Merkuru	Merkur	k1gInSc2	Merkur
pomocí	pomocí	k7c2	pomocí
dalekohledu	dalekohled	k1gInSc2	dalekohled
provedl	provést	k5eAaPmAgMnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
italský	italský	k2eAgInSc1d1	italský
astronom	astronom	k1gMnSc1	astronom
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnPc6	Galilei
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
21	[number]	k4	21
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
pozorován	pozorovat	k5eAaImNgInS	pozorovat
přechod	přechod	k1gInSc1	přechod
Merkuru	Merkur	k1gInSc2	Merkur
přes	přes	k7c4	přes
sluneční	sluneční	k2eAgInSc4d1	sluneční
disk	disk	k1gInSc4	disk
Francouzem	Francouz	k1gMnSc7	Francouz
Pierrem	Pierr	k1gMnSc7	Pierr
Gassendimem	Gassendim	k1gMnSc7	Gassendim
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
italský	italský	k2eAgMnSc1d1	italský
astronom	astronom	k1gMnSc1	astronom
Giovanni	Giovann	k1gMnPc1	Giovann
Battista	Battista	k1gMnSc1	Battista
Zupi	Zupi	k1gNnPc2	Zupi
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1639	[number]	k4	1639
fáze	fáze	k1gFnSc1	fáze
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
nezvratný	zvratný	k2eNgInSc4d1	nezvratný
důkaz	důkaz	k1gInSc4	důkaz
obíhání	obíhání	k1gNnSc2	obíhání
Merkuru	Merkur	k1gInSc2	Merkur
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
německý	německý	k2eAgMnSc1d1	německý
astronom	astronom	k1gMnSc1	astronom
Johann	Johann	k1gMnSc1	Johann
Franz	Franz	k1gMnSc1	Franz
Encke	Encke	k1gInSc4	Encke
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
první	první	k4xOgInSc4	první
reálný	reálný	k2eAgInSc4d1	reálný
odhad	odhad	k1gInSc4	odhad
hmotnosti	hmotnost	k1gFnSc2	hmotnost
planety	planeta	k1gFnSc2	planeta
podle	podle	k7c2	podle
ovlivňování	ovlivňování	k1gNnSc2	ovlivňování
dráhy	dráha	k1gFnSc2	dráha
komety	kometa	k1gFnSc2	kometa
Encke	Enck	k1gFnSc2	Enck
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
oběžné	oběžný	k2eAgFnPc4d1	oběžná
poruchy	porucha	k1gFnPc4	porucha
v	v	k7c6	v
dráze	dráha	k1gFnSc6	dráha
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
pak	pak	k6eAd1	pak
vést	vést	k5eAaImF	vést
některé	některý	k3yIgMnPc4	některý
astronomy	astronom	k1gMnPc4	astronom
k	k	k7c3	k
úvaze	úvaha	k1gFnSc3	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Merkurem	Merkur	k1gInSc7	Merkur
a	a	k8xC	a
Sluncem	slunce	k1gNnSc7	slunce
mohla	moct	k5eAaImAgFnS	moct
nacházet	nacházet	k5eAaImF	nacházet
ještě	ještě	k9	ještě
jedna	jeden	k4xCgFnSc1	jeden
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
začali	začít	k5eAaPmAgMnP	začít
nazývat	nazývat	k5eAaImF	nazývat
Vulkán	vulkán	k1gInSc4	vulkán
<g/>
.	.	kIx.	.
</s>
<s>
Vulkán	vulkán	k1gInSc1	vulkán
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
pozorován	pozorovat	k5eAaImNgInS	pozorovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Merkur	Merkur	k1gInSc1	Merkur
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
atmosféru	atmosféra	k1gFnSc4	atmosféra
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
Johnem	John	k1gMnSc7	John
Flamsteedem	Flamsteed	k1gMnSc7	Flamsteed
a	a	k8xC	a
Johannem	Johann	k1gMnSc7	Johann
Schröterem	Schröter	k1gMnSc7	Schröter
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
Merkuru	Merkur	k1gInSc2	Merkur
přes	přes	k7c4	přes
Slunce	slunce	k1gNnSc4	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
kontrast	kontrast	k1gInSc4	kontrast
mezi	mezi	k7c7	mezi
Sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
planetou	planeta	k1gFnSc7	planeta
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
první	první	k4xOgFnSc1	první
snaha	snaha	k1gFnSc1	snaha
odhadnout	odhadnout	k5eAaPmF	odhadnout
délku	délka	k1gFnSc4	délka
dne	den	k1gInSc2	den
na	na	k7c6	na
Merkuru	Merkur	k1gInSc6	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
astronom	astronom	k1gMnSc1	astronom
Schröter	Schröter	k1gMnSc1	Schröter
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnSc2	pozorování
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
trvá	trvat	k5eAaImIp3nS	trvat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Bessel	Bessel	k1gMnSc1	Bessel
určil	určit	k5eAaPmAgMnS	určit
průměr	průměr	k1gInSc4	průměr
planety	planeta	k1gFnSc2	planeta
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
na	na	k7c4	na
4855	[number]	k4	4855
km	km	kA	km
(	(	kIx(	(
<g/>
současná	současný	k2eAgFnSc1d1	současná
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
4879,4	[number]	k4	4879,4
km	km	kA	km
<g/>
)	)	kIx)	)
O	o	k7c6	o
mnoho	mnoho	k6eAd1	mnoho
později	pozdě	k6eAd2	pozdě
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
Giovanni	Giovaneň	k1gFnSc6	Giovaneň
Schiaparelli	Schiaparelle	k1gFnSc4	Schiaparelle
určil	určit	k5eAaPmAgInS	určit
rotaci	rotace	k1gFnSc4	rotace
planety	planeta	k1gFnSc2	planeta
na	na	k7c4	na
88	[number]	k4	88
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
shodovalo	shodovat	k5eAaImAgNnS	shodovat
s	s	k7c7	s
oběžnou	oběžný	k2eAgFnSc7d1	oběžná
dobou	doba	k1gFnSc7	doba
planety	planeta	k1gFnSc2	planeta
okolo	okolo	k7c2	okolo
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
má	mít	k5eAaImIp3nS	mít
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
stále	stále	k6eAd1	stále
přivrácena	přivrátit	k5eAaPmNgNnP	přivrátit
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
první	první	k4xOgFnSc1	první
mapa	mapa	k1gFnSc1	mapa
povrchu	povrch	k1gInSc2	povrch
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vyhotovil	vyhotovit	k5eAaPmAgInS	vyhotovit
Giovanni	Giovaneň	k1gFnSc3	Giovaneň
Schiaparelli	Schiaparelle	k1gFnSc4	Schiaparelle
a	a	k8xC	a
který	který	k3yRgInSc4	který
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
rotačně	rotačně	k6eAd1	rotačně
svázán	svázán	k2eAgInSc4d1	svázán
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
slapovými	slapový	k2eAgInPc7d1	slapový
jevy	jev	k1gInPc7	jev
způsobovanými	způsobovaný	k2eAgInPc7d1	způsobovaný
centrální	centrální	k2eAgNnSc4d1	centrální
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
byla	být	k5eAaImAgFnS	být
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
strana	strana	k1gFnSc1	strana
Merkuru	Merkur	k1gInSc2	Merkur
zkoumána	zkoumat	k5eAaImNgFnS	zkoumat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
radioastronomie	radioastronomie	k1gFnSc2	radioastronomie
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc4	měření
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
odvrácená	odvrácený	k2eAgFnSc1d1	odvrácená
strana	strana	k1gFnSc1	strana
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
horká	horký	k2eAgFnSc1d1	horká
<g/>
,	,	kIx,	,
očekávaly	očekávat	k5eAaImAgFnP	očekávat
se	se	k3xPyFc4	se
teploty	teplota	k1gFnPc1	teplota
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnPc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
změřili	změřit	k5eAaPmAgMnP	změřit
Američané	Američan	k1gMnPc1	Američan
Pettengil	Pettengil	k1gMnSc1	Pettengil
a	a	k8xC	a
Dyce	Dyce	k1gInSc1	Dyce
rotační	rotační	k2eAgInSc1d1	rotační
dobu	doba	k1gFnSc4	doba
planety	planeta	k1gFnSc2	planeta
na	na	k7c4	na
59	[number]	k4	59
<g/>
±	±	k?	±
<g/>
5	[number]	k4	5
dní	den	k1gInPc2	den
na	na	k7c6	na
základě	základ	k1gInSc6	základ
radarového	radarový	k2eAgNnSc2d1	radarové
pozorování	pozorování	k1gNnSc2	pozorování
Dopplerova	Dopplerův	k2eAgInSc2d1	Dopplerův
posunu	posun	k1gInSc2	posun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
Američan	Američan	k1gMnSc1	Američan
Goldstein	Goldstein	k1gMnSc1	Goldstein
upřesnil	upřesnit	k5eAaPmAgMnS	upřesnit
rotační	rotační	k2eAgFnSc4d1	rotační
dobu	doba	k1gFnSc4	doba
na	na	k7c4	na
58,65	[number]	k4	58,65
<g/>
±	±	k?	±
<g/>
0,25	[number]	k4	0,25
dne	den	k1gInSc2	den
pomocí	pomocí	k7c2	pomocí
stejné	stejný	k2eAgFnSc2d1	stejná
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
povrch	povrch	k1gInSc1	povrch
planety	planeta	k1gFnSc2	planeta
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
části	část	k1gFnSc2	část
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
45	[number]	k4	45
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
nachází	nacházet	k5eAaImIp3nS	nacházet
příliš	příliš	k6eAd1	příliš
blízko	blízko	k6eAd1	blízko
Slunci	slunce	k1gNnSc3	slunce
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
pozorování	pozorování	k1gNnSc4	pozorování
použít	použít	k5eAaPmF	použít
Hubbleův	Hubbleův	k2eAgInSc4d1	Hubbleův
kosmický	kosmický	k2eAgInSc4d1	kosmický
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Merkur	Merkur	k1gInSc1	Merkur
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevzdaluje	vzdalovat	k5eNaImIp3nS	vzdalovat
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
planetu	planeta	k1gFnSc4	planeta
pozorovat	pozorovat	k5eAaImF	pozorovat
pouze	pouze	k6eAd1	pouze
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
východu	východ	k1gInSc6	východ
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
chvíli	chvíle	k1gFnSc4	chvíle
před	před	k7c7	před
západem	západ	k1gInSc7	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
odražené	odražený	k2eAgNnSc1d1	odražené
světlo	světlo	k1gNnSc1	světlo
od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
procházet	procházet	k5eAaImF	procházet
silnější	silný	k2eAgMnSc1d2	silnější
vrstvou	vrstva	k1gFnSc7	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
až	až	k9	až
desetkrát	desetkrát	k6eAd1	desetkrát
snižují	snižovat	k5eAaImIp3nP	snižovat
rozlišovací	rozlišovací	k2eAgFnSc4d1	rozlišovací
schopnost	schopnost	k1gFnSc4	schopnost
teleskopů	teleskop	k1gInPc2	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc1	povrch
Merkuru	Merkur	k1gInSc2	Merkur
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
celý	celý	k2eAgInSc4d1	celý
díky	díky	k7c3	díky
působení	působení	k1gNnSc3	působení
americké	americký	k2eAgFnSc2d1	americká
planetární	planetární	k2eAgFnSc2d1	planetární
sondy	sonda	k1gFnSc2	sonda
MESSENGER	MESSENGER	kA	MESSENGER
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Merkuru	Merkur	k1gInSc2	Merkur
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
2008	[number]	k4	2008
až	až	k9	až
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výzkum	výzkum	k1gInSc1	výzkum
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Merkur	Merkur	k1gInSc1	Merkur
byl	být	k5eAaImAgInS	být
poslední	poslední	k2eAgFnSc7d1	poslední
zkoumanou	zkoumaný	k2eAgFnSc7d1	zkoumaná
terestrickou	terestrický	k2eAgFnSc7d1	terestrická
planetou	planeta	k1gFnSc7	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
blízkých	blízký	k2eAgFnPc2d1	blízká
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Donedávna	donedávna	k6eAd1	donedávna
byl	být	k5eAaImAgInS	být
zblízka	zblízka	k6eAd1	zblízka
prozkoumán	prozkoumán	k2eAgInSc1d1	prozkoumán
pouze	pouze	k6eAd1	pouze
jedinou	jediný	k2eAgFnSc7d1	jediná
sondou	sonda	k1gFnSc7	sonda
–	–	k?	–
americkou	americký	k2eAgFnSc7d1	americká
sondou	sonda	k1gFnSc7	sonda
Mariner	Mariner	k1gMnSc1	Mariner
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
výzkumem	výzkum	k1gInSc7	výzkum
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
planety	planeta	k1gFnSc2	planeta
Slunce	slunce	k1gNnSc2	slunce
zabývá	zabývat	k5eAaImIp3nS	zabývat
další	další	k2eAgFnSc1d1	další
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
MESSENGER	MESSENGER	kA	MESSENGER
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
tři	tři	k4xCgInPc4	tři
průlety	průlet	k1gInPc4	průlet
okolo	okolo	k7c2	okolo
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
navedena	navést	k5eAaPmNgFnS	navést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Minulost	minulost	k1gFnSc1	minulost
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Před	před	k7c7	před
letem	let	k1gInSc7	let
Marineru	Mariner	k1gMnSc3	Mariner
10	[number]	k4	10
byly	být	k5eAaImAgInP	být
poznatky	poznatek	k1gInPc1	poznatek
o	o	k7c6	o
Merkuru	Merkur	k1gInSc6	Merkur
velice	velice	k6eAd1	velice
kusé	kusý	k2eAgNnSc1d1	kusé
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
planetu	planeta	k1gFnSc4	planeta
jde	jít	k5eAaImIp3nS	jít
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
pozorovat	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Mariner	Mariner	k1gInSc1	Mariner
10	[number]	k4	10
proletěl	proletět	k5eAaPmAgInS	proletět
kolem	kolem	k7c2	kolem
Merkuru	Merkur	k1gInSc2	Merkur
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1974	[number]	k4	1974
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
705	[number]	k4	705
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
prolétl	prolétnout	k5eAaPmAgMnS	prolétnout
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1974	[number]	k4	1974
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1975	[number]	k4	1975
pak	pak	k6eAd1	pak
potřetí	potřetí	k4xO	potřetí
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
těchto	tento	k3xDgInPc2	tento
třech	tři	k4xCgInPc2	tři
průletů	průlet	k1gInPc2	průlet
pořídil	pořídit	k5eAaPmAgInS	pořídit
a	a	k8xC	a
odeslal	odeslat	k5eAaPmAgInS	odeslat
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
přes	přes	k7c4	přes
2700	[number]	k4	2700
použitelných	použitelný	k2eAgFnPc2d1	použitelná
fotografií	fotografia	k1gFnPc2	fotografia
o	o	k7c6	o
různém	různý	k2eAgNnSc6d1	různé
rozlišení	rozlišení	k1gNnSc6	rozlišení
mezi	mezi	k7c7	mezi
4	[number]	k4	4
km	km	kA	km
až	až	k9	až
100	[number]	k4	100
m	m	kA	m
na	na	k7c4	na
pixel	pixel	k1gInSc4	pixel
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
celkově	celkově	k6eAd1	celkově
pokryly	pokrýt	k5eAaPmAgFnP	pokrýt
45	[number]	k4	45
%	%	kIx~	%
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
obrovských	obrovský	k2eAgNnPc2d1	obrovské
překvapení	překvapení	k1gNnPc2	překvapení
mise	mise	k1gFnSc2	mise
bylo	být	k5eAaImAgNnS	být
objevení	objevení	k1gNnSc1	objevení
a	a	k8xC	a
změření	změření	k1gNnSc1	změření
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
změnilo	změnit	k5eAaPmAgNnS	změnit
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
stavbu	stavba	k1gFnSc4	stavba
planety	planeta	k1gFnSc2	planeta
<g/>
.3	.3	k4	.3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2004	[number]	k4	2004
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
k	k	k7c3	k
Merkuru	Merkur	k1gInSc3	Merkur
další	další	k2eAgFnSc1d1	další
sonda	sonda	k1gFnSc1	sonda
americké	americký	k2eAgFnSc2d1	americká
kosmické	kosmický	k2eAgFnSc2d1	kosmická
agentury	agentura	k1gFnSc2	agentura
NASA	NASA	kA	NASA
–	–	k?	–
MESSENGER	MESSENGER	kA	MESSENGER
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
byla	být	k5eAaImAgFnS	být
navedena	naveden	k2eAgFnSc1d1	navedena
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
byly	být	k5eAaImAgInP	být
provedeny	proveden	k2eAgInPc1d1	proveden
první	první	k4xOgInPc1	první
dva	dva	k4xCgInPc1	dva
průlety	průlet	k1gInPc1	průlet
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
zaměření	zaměření	k1gNnSc3	zaměření
(	(	kIx(	(
<g/>
výzkum	výzkum	k1gInSc4	výzkum
planety	planeta	k1gFnSc2	planeta
samotné	samotný	k2eAgNnSc1d1	samotné
i	i	k9	i
její	její	k3xOp3gFnSc2	její
magnetosféry	magnetosféra	k1gFnSc2	magnetosféra
<g/>
)	)	kIx)	)
sonda	sonda	k1gFnSc1	sonda
obíhala	obíhat	k5eAaImAgFnS	obíhat
kolem	kolem	k7c2	kolem
Merkuru	Merkur	k1gInSc2	Merkur
po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
eliptické	eliptický	k2eAgFnSc6d1	eliptická
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
planety	planeta	k1gFnSc2	planeta
přibližovala	přibližovat	k5eAaImAgFnS	přibližovat
vždy	vždy	k6eAd1	vždy
u	u	k7c2	u
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
misi	mise	k1gFnSc4	mise
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Budoucnost	budoucnost	k1gFnSc1	budoucnost
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rok	rok	k1gInSc4	rok
2017	[number]	k4	2017
plánovala	plánovat	k5eAaImAgFnS	plánovat
Evropská	evropský	k2eAgFnSc1d1	Evropská
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
ESA	eso	k1gNnSc2	eso
start	start	k1gInSc4	start
družice	družice	k1gFnSc2	družice
BepiColombo	BepiColomba	k1gMnSc5	BepiColomba
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
se	se	k3xPyFc4	se
potýkal	potýkat	k5eAaImAgInS	potýkat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
potížemi	potíž	k1gFnPc7	potíž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
ministerské	ministerský	k2eAgFnSc6d1	ministerská
konferenci	konference	k1gFnSc6	konference
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
ESA	eso	k1gNnSc2	eso
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2005	[number]	k4	2005
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
financování	financování	k1gNnSc1	financování
je	být	k5eAaImIp3nS	být
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
samostatné	samostatný	k2eAgFnPc4d1	samostatná
sondy	sonda	k1gFnPc4	sonda
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
doplňovat	doplňovat	k5eAaImF	doplňovat
<g/>
:	:	kIx,	:
MPO	MPO	kA	MPO
(	(	kIx(	(
<g/>
Mercury	Mercur	k1gInPc1	Mercur
Planetary	Planetara	k1gFnSc2	Planetara
Orbiter	Orbiter	k1gMnSc1	Orbiter
–	–	k?	–
sonda	sonda	k1gFnSc1	sonda
pozorující	pozorující	k2eAgFnSc4d1	pozorující
planetu	planeta	k1gFnSc4	planeta
<g/>
)	)	kIx)	)
a	a	k8xC	a
MMO	MMO	kA	MMO
(	(	kIx(	(
<g/>
Mercury	Mercura	k1gFnSc2	Mercura
Magnetospheric	Magnetospheric	k1gMnSc1	Magnetospheric
Orbiter	Orbiter	k1gMnSc1	Orbiter
–	–	k?	–
sonda	sonda	k1gFnSc1	sonda
studující	studující	k2eAgFnSc4d1	studující
magnetosféru	magnetosféra	k1gFnSc4	magnetosféra
<g/>
)	)	kIx)	)
dodaná	dodaný	k2eAgFnSc1d1	dodaná
japonskou	japonský	k2eAgFnSc7d1	japonská
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
agenturou	agentura	k1gFnSc7	agentura
JAXA	JAXA	kA	JAXA
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Merkuru	Merkur	k1gMnSc3	Merkur
měly	mít	k5eAaImAgInP	mít
obě	dva	k4xCgFnPc4	dva
sondy	sonda	k1gFnPc4	sonda
dorazit	dorazit	k5eAaPmF	dorazit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
obíhat	obíhat	k5eAaImF	obíhat
Merkur	Merkur	k1gInSc4	Merkur
po	po	k7c6	po
eliptických	eliptický	k2eAgFnPc6d1	eliptická
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dráha	dráha	k1gFnSc1	dráha
MPO	MPO	kA	MPO
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
menší	malý	k2eAgFnSc1d2	menší
výstřednost	výstřednost	k1gFnSc1	výstřednost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Merkur	Merkur	k1gInSc1	Merkur
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jméno	jméno	k1gNnSc1	jméno
planety	planeta	k1gFnSc2	planeta
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
nesl	nést	k5eAaImAgInS	nést
Merkur	Merkur	k1gInSc4	Merkur
dvojí	dvojit	k5eAaImIp3nS	dvojit
označení	označení	k1gNnSc1	označení
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
antičtí	antický	k2eAgMnPc1d1	antický
astronomové	astronom	k1gMnPc1	astronom
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
a	a	k8xC	a
totéž	týž	k3xTgNnSc4	týž
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc1	dva
jména	jméno	k1gNnPc1	jméno
byla	být	k5eAaImAgNnP	být
důsledkem	důsledek	k1gInSc7	důsledek
ranního	ranní	k2eAgNnSc2d1	ranní
a	a	k8xC	a
večerního	večerní	k2eAgNnSc2d1	večerní
objevování	objevování	k1gNnSc2	objevování
se	se	k3xPyFc4	se
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ranní	ranní	k2eAgFnSc4d1	ranní
hvězdu	hvězda	k1gFnSc4	hvězda
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgInS	vžít
název	název	k1gInSc1	název
Apollo	Apollo	k1gMnSc1	Apollo
a	a	k8xC	a
pro	pro	k7c4	pro
večerní	večerní	k2eAgFnSc4d1	večerní
hvězdu	hvězda	k1gFnSc4	hvězda
pak	pak	k8xC	pak
jméno	jméno	k1gNnSc4	jméno
Hermés	Hermésa	k1gFnPc2	Hermésa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antických	antický	k2eAgFnPc6d1	antická
dobách	doba	k1gFnPc6	doba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
Hérakleita	Hérakleitum	k1gNnSc2	Hérakleitum
<g/>
)	)	kIx)	)
objevoval	objevovat	k5eAaImAgInS	objevovat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
<g/>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
název	název	k1gInSc1	název
planety	planeta	k1gFnSc2	planeta
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
římské	římský	k2eAgFnSc2d1	římská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Mercurius	Mercurius	k1gInSc4	Mercurius
bylo	být	k5eAaImAgNnS	být
jméno	jméno	k1gNnSc1	jméno
římského	římský	k2eAgMnSc2d1	římský
boha	bůh	k1gMnSc2	bůh
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
cestování	cestování	k1gNnSc2	cestování
a	a	k8xC	a
šikovných	šikovný	k2eAgMnPc2d1	šikovný
zlodějů	zloděj	k1gMnPc2	zloděj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
protějškem	protějšek	k1gInSc7	protějšek
řeckého	řecký	k2eAgMnSc4d1	řecký
boha	bůh	k1gMnSc4	bůh
Herma	Hermes	k1gMnSc4	Hermes
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
Hermés	Hermésa	k1gFnPc2	Hermésa
byl	být	k5eAaImAgMnS	být
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
jako	jako	k8xC	jako
okřídlený	okřídlený	k2eAgMnSc1d1	okřídlený
boží	boží	k2eAgMnSc1d1	boží
posel	posel	k1gMnSc1	posel
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
planety	planeta	k1gFnSc2	planeta
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
také	také	k9	také
díky	díky	k7c3	díky
jejímu	její	k3xOp3gInSc3	její
rychlému	rychlý	k2eAgInSc3d1	rychlý
pohybu	pohyb	k1gInSc3	pohyb
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Hermés	Hermés	k1gInSc1	Hermés
byl	být	k5eAaImAgInS	být
zpravidla	zpravidla	k6eAd1	zpravidla
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
syna	syn	k1gMnSc4	syn
boha	bůh	k1gMnSc4	bůh
nebes	nebesa	k1gNnPc2	nebesa
Caela	Caelo	k1gNnSc2	Caelo
a	a	k8xC	a
uctívání	uctívání	k1gNnSc2	uctívání
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostávalo	dostávat	k5eAaImAgNnS	dostávat
převážně	převážně	k6eAd1	převážně
od	od	k7c2	od
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
pak	pak	k6eAd1	pak
počeštěno	počeštit	k5eAaPmNgNnS	počeštit
na	na	k7c4	na
dnešní	dnešní	k2eAgInSc4d1	dnešní
Merkur	Merkur	k1gInSc4	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
slova	slovo	k1gNnSc2	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
"	"	kIx"	"
<g/>
merx	merx	k1gInSc1	merx
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
mzda	mzda	k1gFnSc1	mzda
či	či	k8xC	či
odměna	odměna	k1gFnSc1	odměna
<g/>
)	)	kIx)	)
anebo	anebo	k8xC	anebo
"	"	kIx"	"
<g/>
mercor	mercor	k1gInSc1	mercor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kupujeme	kupovat	k5eAaImIp1nP	kupovat
<g/>
,	,	kIx,	,
obchodujeme	obchodovat	k5eAaImIp1nP	obchodovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Česka	Česko	k1gNnSc2	Česko
používaly	používat	k5eAaImAgInP	používat
slovanské	slovanský	k2eAgInPc1d1	slovanský
kmeny	kmen	k1gInPc1	kmen
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
mytologii	mytologie	k1gFnSc6	mytologie
označení	označení	k1gNnPc2	označení
Dobropán	Dobropán	k1gMnSc1	Dobropán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
Šuej-sing	Šuejing	k1gInSc1	Šuej-sing
a	a	k8xC	a
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
planeta	planeta	k1gFnSc1	planeta
vody	voda	k1gFnSc2	voda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Severská	severský	k2eAgFnSc1d1	severská
mytologie	mytologie	k1gFnSc1	mytologie
spojovala	spojovat	k5eAaImAgFnS	spojovat
Merkur	Merkur	k1gInSc4	Merkur
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
Ódinem	Ódin	k1gMnSc7	Ódin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sci-fi	scii	k1gNnPc3	sci-fi
===	===	k?	===
</s>
</p>
<p>
<s>
Merkur	Merkur	k1gInSc1	Merkur
posloužil	posloužit	k5eAaPmAgInS	posloužit
jako	jako	k9	jako
námět	námět	k1gInSc1	námět
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
sci-fi	scii	k1gFnSc2	sci-fi
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
téma	téma	k1gNnSc1	téma
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
extrémní	extrémní	k2eAgFnSc7d1	extrémní
radiací	radiace	k1gFnSc7	radiace
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
či	či	k8xC	či
pomalou	pomalý	k2eAgFnSc7d1	pomalá
rotací	rotace	k1gFnSc7	rotace
a	a	k8xC	a
přechodem	přechod	k1gInSc7	přechod
dne	den	k1gInSc2	den
a	a	k8xC	a
noci	noc	k1gFnSc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
hlavně	hlavně	k9	hlavně
dva	dva	k4xCgInPc1	dva
obrazy	obraz	k1gInPc1	obraz
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Planety	planeta	k1gFnPc1	planeta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
chladná	chladný	k2eAgNnPc1d1	chladné
a	a	k8xC	a
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
žhavá	žhavý	k2eAgFnSc5d1	žhavá
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
představa	představa	k1gFnSc1	představa
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
psaných	psaný	k2eAgInPc6d1	psaný
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
pomalu	pomalu	k6eAd1	pomalu
rotuje	rotovat	k5eAaImIp3nS	rotovat
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
modernější	moderní	k2eAgInSc1d2	modernější
obraz	obraz	k1gInSc1	obraz
planety	planeta	k1gFnSc2	planeta
odráží	odrážet	k5eAaImIp3nS	odrážet
novější	nový	k2eAgInPc4d2	novější
vědecké	vědecký	k2eAgInPc4d1	vědecký
poznatky	poznatek	k1gInPc4	poznatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
starší	starý	k2eAgNnPc4d2	starší
díla	dílo	k1gNnPc4	dílo
popisující	popisující	k2eAgInSc4d1	popisující
Merkur	Merkur	k1gInSc4	Merkur
jako	jako	k8xC	jako
nerotující	rotující	k2eNgNnSc4d1	rotující
těleso	těleso	k1gNnSc4	těleso
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
kniha	kniha	k1gFnSc1	kniha
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
Erica	Ericus	k1gMnSc2	Ericus
Rückera	Rücker	k1gMnSc2	Rücker
Eddisona	Eddison	k1gMnSc2	Eddison
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gFnSc2	The
Worm	Wor	k1gNnSc7	Wor
Ouroboros	Ouroborosa	k1gFnPc2	Ouroborosa
pojednávající	pojednávající	k2eAgFnPc4d1	pojednávající
o	o	k7c6	o
věčném	věčný	k2eAgInSc6d1	věčný
boji	boj	k1gInSc6	boj
dvou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
nejsou	být	k5eNaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
žádné	žádný	k3yNgInPc1	žádný
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
známé	známý	k2eAgFnSc2d1	známá
znalosti	znalost	k1gFnSc2	znalost
o	o	k7c6	o
Merkuru	Merkur	k1gInSc6	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
autory	autor	k1gMnPc4	autor
sci-fi	scii	k1gFnSc2	sci-fi
píšící	píšící	k2eAgFnSc2d1	píšící
o	o	k7c6	o
Merkuru	Merkur	k1gInSc6	Merkur
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Isaac	Isaac	k1gInSc1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
situoval	situovat	k5eAaBmAgMnS	situovat
několik	několik	k4yIc4	několik
svých	svůj	k3xOyFgFnPc2	svůj
povídek	povídka	k1gFnPc2	povídka
(	(	kIx(	(
<g/>
Runaround	Runaround	k1gMnSc1	Runaround
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Dying	Dying	k1gMnSc1	Dying
Night	Night	k1gMnSc1	Night
<g/>
,	,	kIx,	,
Lucky	lucky	k6eAd1	lucky
Starr	Starr	k1gMnSc1	Starr
and	and	k?	and
the	the	k?	the
Big	Big	k1gFnSc2	Big
Sun	Sun	kA	Sun
of	of	k?	of
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
byly	být	k5eAaImAgFnP	být
napsány	napsán	k2eAgInPc4d1	napsán
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
astronomové	astronom	k1gMnPc1	astronom
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
planeta	planeta	k1gFnSc1	planeta
nemá	mít	k5eNaImIp3nS	mít
vázanou	vázaný	k2eAgFnSc4d1	vázaná
rotaci	rotace	k1gFnSc4	rotace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
námět	námět	k1gInSc4	námět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
často	často	k6eAd1	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
spadají	spadat	k5eAaImIp3nP	spadat
i	i	k9	i
některá	některý	k3yIgNnPc4	některý
díla	dílo	k1gNnPc4	dílo
od	od	k7c2	od
Arthura	Arthur	k1gMnSc2	Arthur
C.	C.	kA	C.
Clarka	Clarek	k1gMnSc2	Clarek
(	(	kIx(	(
<g/>
Islands	Islands	k1gInSc1	Islands
in	in	k?	in
the	the	k?	the
Sky	Sky	k1gFnSc2	Sky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Larryho	Larry	k1gMnSc2	Larry
Nivena	Niven	k1gMnSc2	Niven
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Coldest	Coldest	k1gMnSc1	Coldest
Place	plac	k1gInSc6	plac
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hugha	Hugha	k1gFnSc1	Hugha
Walterse	Walterse	k1gFnSc1	Walterse
(	(	kIx(	(
<g/>
Mission	Mission	k1gInSc1	Mission
to	ten	k3xDgNnSc1	ten
Mercury	Mercura	k1gFnPc1	Mercura
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohá	mnohé	k1gNnPc1	mnohé
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
koncept	koncept	k1gInSc1	koncept
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
Merkur	Merkur	k1gMnSc1	Merkur
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
objevovat	objevovat	k5eAaImF	objevovat
jako	jako	k9	jako
rotující	rotující	k2eAgFnSc1d1	rotující
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
díla	dílo	k1gNnSc2	dílo
Arthura	Arthur	k1gMnSc2	Arthur
C.	C.	kA	C.
Clarka	Clarek	k1gMnSc2	Clarek
v	v	k7c6	v
díle	díl	k1gInSc6	díl
Setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
Rámou	Ráma	k1gMnSc7	Ráma
vydaném	vydaný	k2eAgInSc6d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
mimozemské	mimozemský	k2eAgFnSc6d1	mimozemská
lodi	loď	k1gFnSc6	loď
prolétávající	prolétávající	k2eAgFnSc7d1	prolétávající
sluneční	sluneční	k2eAgFnSc7d1	sluneční
soustavou	soustava	k1gFnSc7	soustava
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
průletu	průlet	k1gInSc2	průlet
přistanou	přistat	k5eAaPmIp3nP	přistat
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
palubě	paluba	k1gFnSc6	paluba
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
začnou	začít	k5eAaPmIp3nP	začít
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
vnitřek	vnitřek	k1gInSc4	vnitřek
prozkoumávat	prozkoumávat	k5eAaImF	prozkoumávat
a	a	k8xC	a
sledovat	sledovat	k5eAaImF	sledovat
oživování	oživování	k1gNnSc4	oživování
celé	celý	k2eAgFnSc2d1	celá
lodě	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
vycházejí	vycházet	k5eAaImIp3nP	vycházet
novely	novela	k1gFnPc1	novela
a	a	k8xC	a
povídky	povídka	k1gFnPc1	povídka
amerického	americký	k2eAgMnSc2d1	americký
autora	autor	k1gMnSc2	autor
Kima	Kim	k1gInSc2	Kim
Kim	Kim	k1gMnSc4	Kim
Stanleyho	Stanley	k1gMnSc4	Stanley
Robinsona	Robinson	k1gMnSc4	Robinson
zabývající	zabývající	k2eAgFnSc4d1	zabývající
se	se	k3xPyFc4	se
Merkurem	Merkur	k1gMnSc7	Merkur
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
Mercurial	Mercurial	k1gInSc1	Mercurial
<g/>
)	)	kIx)	)
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
románu	román	k1gInSc6	román
Modrý	modrý	k2eAgInSc1d1	modrý
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povídce	povídka	k1gFnSc6	povídka
Mercurial	Mercurial	k1gInSc1	Mercurial
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
motivem	motiv	k1gInSc7	motiv
město	město	k1gNnSc1	město
neustále	neustále	k6eAd1	neustále
pojíždějící	pojíždějící	k2eAgMnSc1d1	pojíždějící
po	po	k7c6	po
rovníku	rovník	k1gInSc6	rovník
planety	planeta	k1gFnSc2	planeta
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
a	a	k8xC	a
unikající	unikající	k2eAgInSc4d1	unikající
před	před	k7c7	před
smrtící	smrtící	k2eAgFnSc7d1	smrtící
radiací	radiace	k1gFnSc7	radiace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
přivrácenou	přivrácený	k2eAgFnSc4d1	přivrácená
stranu	strana	k1gFnSc4	strana
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Celému	celý	k2eAgNnSc3d1	celé
městu	město	k1gNnSc3	město
vládne	vládnout	k5eAaImIp3nS	vládnout
autokratický	autokratický	k2eAgMnSc1d1	autokratický
tyran	tyran	k1gMnSc1	tyran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Merkur	Merkur	k1gInSc1	Merkur
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
taktéž	taktéž	k?	taktéž
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Futurama	Futurama	k?	Futurama
či	či	k8xC	či
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Sunshine	Sunshin	k1gInSc5	Sunshin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Mercury	Mercura	k1gFnSc2	Mercura
in	in	k?	in
fiction	fiction	k1gInSc1	fiction
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Mercury	Mercura	k1gFnSc2	Mercura
(	(	kIx(	(
<g/>
planet	planeta	k1gFnPc2	planeta
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Přechod	přechod	k1gInSc1	přechod
Merkuru	Merkur	k1gInSc2	Merkur
</s>
</p>
<p>
<s>
Astrofyzika	astrofyzika	k1gFnSc1	astrofyzika
</s>
</p>
<p>
<s>
Planety	planeta	k1gFnPc1	planeta
</s>
</p>
<p>
<s>
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Merkur	Merkur	k1gInSc1	Merkur
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Merkur	Merkur	k1gInSc1	Merkur
(	(	kIx(	(
<g/>
Dobropán	Dobropán	k1gMnSc1	Dobropán
<g/>
)	)	kIx)	)
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Merkur	Merkur	k1gInSc1	Merkur
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Český	český	k2eAgMnSc1d1	český
vědec	vědec	k1gMnSc1	vědec
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zkoumat	zkoumat	k5eAaImF	zkoumat
okolí	okolí	k1gNnSc2	okolí
planety	planeta	k1gFnSc2	planeta
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
Aktualne	Aktualn	k1gMnSc5	Aktualn
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
