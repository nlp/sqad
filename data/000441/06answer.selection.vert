<s>
Merkur	Merkur	k1gMnSc1	Merkur
je	být	k5eAaImIp3nS	být
Slunci	slunce	k1gNnSc3	slunce
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
a	a	k8xC	a
současně	současně	k6eAd1	současně
i	i	k9	i
nejmenší	malý	k2eAgFnSc7d3	nejmenší
planetou	planeta	k1gFnSc7	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
větší	veliký	k2eAgFnSc2d2	veliký
velikosti	velikost	k1gFnSc2	velikost
než	než	k8xS	než
pozemský	pozemský	k2eAgInSc4d1	pozemský
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
menší	malý	k2eAgInSc4d2	menší
než	než	k8xS	než
Jupiterův	Jupiterův	k2eAgInSc4d1	Jupiterův
měsíc	měsíc	k1gInSc4	měsíc
Ganymed	Ganymed	k1gMnSc1	Ganymed
a	a	k8xC	a
Saturnův	Saturnův	k2eAgInSc1d1	Saturnův
Titan	titan	k1gInSc1	titan
<g/>
.	.	kIx.	.
</s>
