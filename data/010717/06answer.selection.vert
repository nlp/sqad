<s>
Titan	titan	k1gInSc1	titan
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
planeta	planeta	k1gFnSc1	planeta
Merkur	Merkur	k1gInSc1	Merkur
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgMnS	obklopit
vlastní	vlastní	k2eAgFnSc7d1	vlastní
hustou	hustý	k2eAgFnSc7d1	hustá
atmosférou	atmosféra	k1gFnSc7	atmosféra
složenou	složený	k2eAgFnSc4d1	složená
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
molekulárního	molekulární	k2eAgInSc2d1	molekulární
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
