<s>
Alex	Alex	k1gMnSc1
&	&	k?
spol	spol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Alex	Alex	k1gMnSc1
&	&	k?
spol	spol	k1gInSc1
Základní	základní	k2eAgInSc1d1
informace	informace	k1gFnSc2
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Alex	Alex	k1gMnSc1
&	&	k?
co	co	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žánr	žánr	k1gInSc1
</s>
<s>
sitcom	sitcom	k1gInSc1
Režie	režie	k1gFnSc2
</s>
<s>
Claudio	Claudio	k6eAd1
Norza	Norz	k1gMnSc4
Hrají	hrát	k5eAaImIp3nP
</s>
<s>
Leonardo	Leonardo	k1gMnSc1
CecchiEleonora	CecchiEleonor	k1gMnSc2
GaggeroBeatrice	GaggeroBeatrika	k1gFnSc3
VendraminSaul	VendraminSaul	k1gInSc1
NanniFederico	NanniFederico	k6eAd1
Russo	Russa	k1gFnSc5
Úvodní	úvodní	k2eAgFnSc4d1
znělka	znělka	k1gFnSc1
</s>
<s>
"	"	kIx"
<g/>
Music	Musice	k1gFnPc2
Speaks	Speaks	k1gInSc1
<g/>
"	"	kIx"
Země	země	k1gFnSc1
původu	původ	k1gInSc2
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
Jazyk	jazyk	k1gInSc1
</s>
<s>
italština	italština	k1gFnSc1
Počet	počet	k1gInSc1
řad	řad	k1gInSc1
</s>
<s>
3	#num#	k4
+	+	kIx~
Film	film	k1gInSc1
Počet	počet	k1gInSc1
dílů	díl	k1gInPc2
</s>
<s>
55	#num#	k4
Obvyklá	obvyklý	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
23	#num#	k4
minut	minuta	k1gFnPc2
Produkce	produkce	k1gFnSc1
a	a	k8xC
štáb	štáb	k1gInSc1
Kamera	kamera	k1gFnSc1
</s>
<s>
Mauro	Mauro	k1gNnSc1
Marchetti	Marchetti	k1gNnSc7
Hudba	hudba	k1gFnSc1
</s>
<s>
Federica	Federica	k1gMnSc1
Camba	Camba	k1gMnSc1
Premiérové	premiérový	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
Stanice	stanice	k1gFnSc2
</s>
<s>
Disney	Disnea	k1gFnSc2
Channel	Channela	k1gFnPc2
Formát	formát	k1gInSc1
obrazu	obraz	k1gInSc2
</s>
<s>
16	#num#	k4
:	:	kIx,
9	#num#	k4
Vysíláno	vysílán	k2eAgNnSc4d1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2015	#num#	k4
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
Posloupnost	posloupnost	k1gFnSc1
Následující	následující	k2eAgFnSc2d1
</s>
<s>
Penny	penny	k1gInSc1
z	z	k7c2
M.	M.	kA
<g/>
A.	A.	kA
<g/>
R.	R.	kA
<g/>
Su	Su	k?
Alex	Alex	k1gMnSc1
&	&	k?
spol	spol	k1gInSc1
na	na	k7c4
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
SZ	SZ	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alex	Alex	k1gMnSc1
&	&	k?
spol	spol	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
v	v	k7c6
italském	italský	k2eAgInSc6d1
originále	originál	k1gInSc6
Alex	Alex	k1gMnSc1
&	&	k?
Co	co	k9
<g/>
.	.	kIx.
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
italský	italský	k2eAgInSc1d1
televizní	televizní	k2eAgInSc1d1
sitcom	sitcom	k1gInSc1
stanice	stanice	k1gFnSc2
Disney	Disnea	k1gFnSc2
Channel	Channela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
úvodní	úvodní	k2eAgInSc1d1
díl	díl	k1gInSc1
měl	mít	k5eAaImAgInS
v	v	k7c4
Itálii	Itálie	k1gFnSc4
premiéru	premiéra	k1gFnSc4
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seriál	seriál	k1gInSc1
vypráví	vyprávět	k5eAaImIp3nS
příběh	příběh	k1gInSc4
o	o	k7c6
pěti	pět	k4xCc6
kamarádech	kamarád	k1gMnPc6
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
touží	toužit	k5eAaImIp3nP
změnit	změnit	k5eAaPmF
svět	svět	k1gInSc4
a	a	k8xC
promlouvat	promlouvat	k5eAaImF
lidmi	člověk	k1gMnPc7
jejich	jejich	k3xOp3gFnSc7
vlastní	vlastní	k2eAgFnSc7d1
hudbou	hudba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Setkávají	setkávat	k5eAaImIp3nP
se	se	k3xPyFc4
také	také	k9
na	na	k7c6
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
setká	setkat	k5eAaPmIp3nS
celá	celý	k2eAgFnSc1d1
jejich	jejich	k3xOp3gFnSc1
parta	parta	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
stanou	stanout	k5eAaPmIp3nP
nerozlučnými	rozlučný	k2eNgMnPc7d1
přáteli	přítel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
založí	založit	k5eAaPmIp3nP
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
Sound	Sound	k1gMnSc1
Alound	Alound	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
řadě	řada	k1gFnSc6
seriálu	seriál	k1gInSc2
kapelu	kapela	k1gFnSc4
přejmenují	přejmenovat	k5eAaPmIp3nP
na	na	k7c4
Alex	Alex	k1gMnSc1
&	&	k?
spol	spol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
ŘadyPo	ŘadyPo	k6eAd1
</s>
<s>
ŘadaDílyPremiéra	ŘadaDílyPremiéra	k1gFnSc1
v	v	k7c4
ItáliiPremiéra	ItáliiPremiér	k1gMnSc4
v	v	k7c6
ČR	ČR	kA
</s>
<s>
První	první	k4xOgMnPc1
dílPoslední	dílPosledný	k2eAgMnPc1d1
dílPrvní	dílPrvnit	k5eAaPmIp3nP
dílPoslední	dílPosledný	k2eAgMnPc1d1
díl	díl	k1gInSc4
</s>
<s>
1	#num#	k4
</s>
<s>
1320150511	#num#	k4
<g/>
a	a	k8xC
<g/>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
201520150527	#num#	k4
<g/>
a	a	k8xC
<g/>
27	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
201520160206	#num#	k4
<g/>
a	a	k8xC
<g/>
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
201620160319	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2016	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1820150927	#num#	k4
<g/>
a	a	k8xC
<g/>
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
201520151129	#num#	k4
<g/>
a	a	k8xC
<g/>
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
201520161010	#num#	k4
<g/>
a	a	k8xC
<g/>
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
201620161108	#num#	k4
<g/>
a	a	k8xC
<g/>
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2020160924	#num#	k4
<g/>
a	a	k8xC
<g/>
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
201620170629	#num#	k4
<g/>
a	a	k8xC
<g/>
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
201720170424	#num#	k4
<g/>
a	a	k8xC
<g/>
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
201720170915	#num#	k4
<g/>
a	a	k8xC
<g/>
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2017	#num#	k4
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Alex	Alex	k1gMnSc1
&	&	k?
Spol	spol	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Jak	jak	k6eAd1
dospět	dospět	k5eAaPmF
navzdory	navzdory	k7c3
rodičům	rodič	k1gMnPc3
–	–	k?
20170728	#num#	k4
<g/>
a	a	k8xC
<g/>
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
</s>
<s>
Postavy	postava	k1gFnPc1
</s>
<s>
Postava	postava	k1gFnSc1
</s>
<s>
Herec	herec	k1gMnSc1
</s>
<s>
Dabing	dabing	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
série	série	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Dabing	dabing	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
série	série	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Dabing	dabing	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
série	série	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Alex	Alex	k1gMnSc1
Leoni	Leoň	k1gMnSc3
</s>
<s>
Leonardo	Leonardo	k1gMnSc1
Cecchi	Cecch	k1gFnSc2
</s>
<s>
David	David	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
</s>
<s>
Nicole	Nicole	k1gFnSc1
De	De	k?
Ponteová	Ponteová	k1gFnSc1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Gaggero	Gaggero	k1gFnSc1
</s>
<s>
Ivana	Ivana	k1gFnSc1
Korolová	Korolový	k2eAgFnSc1d1
</s>
<s>
Emma	Emma	k1gFnSc1
Ferrariová	Ferrariový	k2eAgFnSc1d1
</s>
<s>
Beatrice	Beatrice	k1gFnSc1
Vendramin	Vendramin	k1gInSc1
</s>
<s>
Kristýna	Kristýna	k1gFnSc1
Skružná	Skružný	k2eAgFnSc1d1
</s>
<s>
Terezie	Terezie	k1gFnSc1
Taberyová	Taberyová	k1gFnSc1
</s>
<s>
Christian	Christian	k1gMnSc1
Alessi	Aless	k1gMnSc3
</s>
<s>
Saul	Saul	k1gMnSc1
Nanni	Nanň	k1gMnSc3
</s>
<s>
Robin	robin	k2eAgInSc1d1
Pařík	Pařík	k1gInSc1
</s>
<s>
Samuel	Samuel	k1gMnSc1
Sam	Sam	k1gMnSc1
Costa	Costa	k1gMnSc1
</s>
<s>
Federico	Federica	k1gFnSc5
Russo	Russa	k1gFnSc5
</s>
<s>
Jan	Jan	k1gMnSc1
Battěk	Battěk	k1gMnSc1
</s>
<s>
Linda	Linda	k1gFnSc1
Rossettiová	Rossettiový	k2eAgFnSc1d1
</s>
<s>
Lucrezia	Lucrezia	k1gFnSc1
Di	Di	k1gFnSc2
Michele	Michel	k1gInSc2
</s>
<s>
Klára	Klára	k1gFnSc1
Nováková	Nováková	k1gFnSc1
</s>
<s>
Rebecca	Rebecc	k2eAgFnSc1d1
Guglielminová	Guglielminová	k1gFnSc1
</s>
<s>
Giulia	Giulium	k1gNnPc1
Guerrini	Guerrin	k1gMnPc1
</s>
<s>
Rozita	Rozita	k1gFnSc1
Erbanová	Erbanová	k1gFnSc1
</s>
<s>
Kristýna	Kristýna	k1gFnSc1
Skružná	Skružný	k2eAgFnSc1d1
</s>
<s>
Samantha	Samantha	k1gFnSc1
Ferriová	Ferriový	k2eAgFnSc1d1
</s>
<s>
Asia	Asia	k6eAd1
Corvino	Corvin	k2eAgNnSc1d1
</s>
<s>
Adéla	Adéla	k1gFnSc1
Nováková	Nováková	k1gFnSc1
</s>
<s>
Nina	Nina	k1gFnSc1
</s>
<s>
Debora	Debora	k1gFnSc1
Villa	Villa	k1gFnSc1
</s>
<s>
René	René	k1gFnSc1
Slováčková	Slováčková	k1gFnSc1
</s>
<s>
Augusto	Augusta	k1gMnSc5
Ferrari	Ferrari	k1gMnSc5
</s>
<s>
Roberto	Roberta	k1gFnSc5
Citran	citran	k1gInSc4
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Král	Král	k1gMnSc1
(	(	kIx(
<g/>
starší	starší	k1gMnSc1
epizody	epizoda	k1gFnSc2
<g/>
)	)	kIx)
/	/	kIx~
Jiří	Jiří	k1gMnSc1
Valšuba	Valšuba	k1gMnSc1
(	(	kIx(
<g/>
novější	nový	k2eAgFnSc2d2
epizody	epizoda	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Valšuba	Valšuba	k1gMnSc1
</s>
<s>
Giovanni	Giovanen	k2eAgMnPc1d1
Belli	Bell	k1gMnPc1
</s>
<s>
Michele	Michele	k6eAd1
Cesari	Cesar	k1gMnPc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Vondrák	Vondrák	k1gMnSc1
</s>
<s>
profesor	profesor	k1gMnSc1
Škropión	Škropión	k1gMnSc1
</s>
<s>
Nicola	Nicola	k1gFnSc1
Stravalaci	Stravalace	k1gFnSc4
</s>
<s>
Michal	Michal	k1gMnSc1
Michálek	Michálek	k1gMnSc1
</s>
<s>
Victoria	Victorium	k1gNnPc1
Williamsová	Williamsový	k2eAgNnPc1d1
</s>
<s>
Chiara	Chiara	k1gFnSc1
Iezzi	Iezze	k1gFnSc4
Cohen	Cohen	k2eAgInSc4d1
</s>
<s>
Martina	Martina	k1gFnSc1
Kechnerová	Kechnerová	k1gFnSc1
</s>
<s>
Clio	Clio	k6eAd1
Pintová	pintový	k2eAgFnSc1d1
</s>
<s>
Miriam	Miriam	k1gFnSc1
Dossena	Dossen	k2eAgFnSc1d1
</s>
<s>
Martina	Martina	k1gFnSc1
Kechnerová	Kechnerová	k1gFnSc1
</s>
<s>
Diana	Diana	k1gFnSc1
</s>
<s>
Sara	Sara	k6eAd1
Borsarelli	Borsarell	k1gMnPc1
</s>
<s>
Regina	Regina	k1gFnSc1
Řandová	Řandová	k1gFnSc1
</s>
<s>
Sara	Sara	k6eAd1
</s>
<s>
Entica	Entica	k6eAd1
Pintone	Pinton	k1gInSc5
</s>
<s>
Marika	Marika	k1gFnSc1
Šoposká	Šoposká	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
