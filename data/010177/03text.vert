<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
národní	národní	k2eAgFnSc1d1	národní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
pinyin	pinyin	k2eAgInSc1d1	pinyin
'	'	kIx"	'
<g/>
Zhō	Zhō	k1gFnSc1	Zhō
guójiā	guójiā	k?	guójiā
hángtiā	hángtiā	k?	hángtiā
jú	jú	k0	jú
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
中	中	k?	中
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Čung-kuo	Čunguo	k1gNnSc1	Čung-kuo
kuo-ťia	kuo-ťium	k1gNnSc2	kuo-ťium
chang-tchien	changchina	k1gFnPc2	chang-tchina
ťü	ťü	k?	ťü
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
CNSA	CNSA	kA	CNSA
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
China	China	k1gFnSc1	China
National	National	k1gFnSc3	National
Space	Spaec	k1gInSc2	Spaec
Administration	Administration	k1gInSc1	Administration
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
civilní	civilní	k2eAgFnSc1d1	civilní
organizace	organizace	k1gFnSc1	organizace
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
postup	postup	k1gInSc4	postup
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
vesmírném	vesmírný	k2eAgInSc6d1	vesmírný
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
organizace	organizace	k1gFnSc1	organizace
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
rozdělením	rozdělení	k1gNnSc7	rozdělení
čínského	čínský	k2eAgNnSc2d1	čínské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
pro	pro	k7c4	pro
letecký	letecký	k2eAgInSc4d1	letecký
a	a	k8xC	a
vesmírný	vesmírný	k2eAgInSc4d1	vesmírný
průmysl	průmysl	k1gInSc4	průmysl
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
CNSA	CNSA	kA	CNSA
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
letectví	letectví	k1gNnSc4	letectví
a	a	k8xC	a
vesmír	vesmír	k1gInSc1	vesmír
(	(	kIx(	(
<g/>
China	China	k1gFnSc1	China
Aerospace	Aerospace	k1gFnSc1	Aerospace
Corporation-CASC	Corporation-CASC	k1gFnSc1	Corporation-CASC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
masové	masový	k2eAgFnSc2d1	masová
restrukturalizace	restrukturalizace	k1gFnSc2	restrukturalizace
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
CASC	CASC	kA	CASC
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
státních	státní	k2eAgFnPc2d1	státní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
<g/>
CNSA	CNSA	kA	CNSA
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
předchozího	předchozí	k2eAgNnSc2d1	předchozí
kosmického	kosmický	k2eAgNnSc2d1	kosmické
(	(	kIx(	(
<g/>
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
raketového	raketový	k2eAgInSc2d1	raketový
<g/>
)	)	kIx)	)
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
snahou	snaha	k1gFnSc7	snaha
Číny	Čína	k1gFnSc2	Čína
o	o	k7c4	o
vlastní	vlastní	k2eAgInSc4d1	vlastní
raketový	raketový	k2eAgInSc4d1	raketový
nosič	nosič	k1gInSc4	nosič
jaderných	jaderný	k2eAgFnPc2d1	jaderná
hlavic	hlavice	k1gFnPc2	hlavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
družice	družice	k1gFnSc1	družice
Dong	dong	k1gInSc4	dong
Fang	Fang	k1gInSc1	Fang
Hong	Hong	k1gInSc1	Hong
1	[number]	k4	1
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Vážila	vážit	k5eAaImAgFnS	vážit
173	[number]	k4	173
kg	kg	kA	kg
a	a	k8xC	a
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
měla	mít	k5eAaImAgFnS	mít
radiový	radiový	k2eAgInSc4d1	radiový
vysílač	vysílač	k1gInSc4	vysílač
který	který	k3yIgMnSc1	který
vysílal	vysílat	k5eAaImAgInS	vysílat
budovatelskou	budovatelský	k2eAgFnSc4d1	budovatelská
píseň	píseň	k1gFnSc4	píseň
Východ	východ	k1gInSc1	východ
je	být	k5eAaImIp3nS	být
rudý	rudý	k2eAgInSc1d1	rudý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
letech	let	k1gInPc6	let
Čína	Čína	k1gFnSc1	Čína
vyslala	vyslat	k5eAaPmAgFnS	vyslat
několik	několik	k4yIc4	několik
telekomunikačních	telekomunikační	k2eAgInPc2d1	telekomunikační
satelitů	satelit	k1gInPc2	satelit
<g/>
,	,	kIx,	,
meteorologických	meteorologický	k2eAgFnPc2d1	meteorologická
družic	družice	k1gFnPc2	družice
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
špionážní	špionážní	k2eAgInPc1d1	špionážní
satelity	satelit	k1gInPc1	satelit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
úspěchu	úspěch	k1gInSc2	úspěch
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Čína	Čína	k1gFnSc1	Čína
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vypuštění	vypuštění	k1gNnSc3	vypuštění
kosmické	kosmický	k2eAgFnSc2d1	kosmická
lodi	loď	k1gFnSc2	loď
Šen-čou	Šen-ča	k1gFnSc7	Šen-ča
(	(	kIx(	(
<g/>
nebeská	nebeský	k2eAgFnSc1d1	nebeská
loď	loď	k1gFnSc1	loď
<g/>
)	)	kIx)	)
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
posádkou	posádka	k1gFnSc7	posádka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vypuštění	vypuštění	k1gNnSc3	vypuštění
lodi	loď	k1gFnSc2	loď
byla	být	k5eAaImAgFnS	být
použita	použit	k2eAgFnSc1d1	použita
raketa	raketa	k1gFnSc1	raketa
Čchang-čeng	Čchang-čeng	k1gInSc1	Čchang-čeng
CZ-	CZ-	k1gFnSc1	CZ-
<g/>
2	[number]	k4	2
<g/>
F.	F.	kA	F.
V	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Číně	Čína	k1gFnSc3	Čína
podařilo	podařit	k5eAaPmAgNnS	podařit
vyslat	vyslat	k5eAaPmF	vyslat
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
stanici	stanice	k1gFnSc4	stanice
Tchien-kung	Tchienung	k1gInSc1	Tchien-kung
1	[number]	k4	1
jako	jako	k8xC	jako
odrazový	odrazový	k2eAgInSc1d1	odrazový
můstek	můstek	k1gInSc1	můstek
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
cesty	cesta	k1gFnPc4	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
Mars	Mars	k1gInSc4	Mars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
vynesla	vynést	k5eAaPmAgFnS	vynést
nosná	nosný	k2eAgFnSc1d1	nosná
raketa	raketa	k1gFnSc1	raketa
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
pochod	pochod	k1gInSc4	pochod
2F	[number]	k4	2F
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
stanici	stanice	k1gFnSc4	stanice
Tchien-kung	Tchienung	k1gInSc1	Tchien-kung
2	[number]	k4	2
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
připojila	připojit	k5eAaPmAgFnS	připojit
kosmická	kosmický	k2eAgFnSc1d1	kosmická
loď	loď	k1gFnSc1	loď
Šen-čou	Šen-čá	k1gFnSc4	Šen-čá
11	[number]	k4	11
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
kosmonauty	kosmonaut	k1gMnPc7	kosmonaut
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současný	současný	k2eAgMnSc1d1	současný
předseda	předseda	k1gMnSc1	předseda
CNSA	CNSA	kA	CNSA
je	být	k5eAaImIp3nS	být
Sun	sun	k1gInSc4	sun
Laj-jen	Lajna	k1gFnPc2	Laj-jna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budoucnost	budoucnost	k1gFnSc1	budoucnost
==	==	k?	==
</s>
</p>
<p>
<s>
CNSA	CNSA	kA	CNSA
chce	chtít	k5eAaImIp3nS	chtít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
vyslat	vyslat	k5eAaPmF	vyslat
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
další	další	k2eAgFnSc4d1	další
zdokonalenou	zdokonalený	k2eAgFnSc4d1	zdokonalená
verzi	verze	k1gFnSc4	verze
orbitální	orbitální	k2eAgFnSc2d1	orbitální
stanice	stanice	k1gFnSc2	stanice
Tchien-kung	Tchienung	k1gInSc4	Tchien-kung
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
China	China	k1gFnSc1	China
National	National	k1gMnSc2	National
Space	Space	k1gMnSc2	Space
Administration	Administration	k1gInSc4	Administration
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
CNSA	CNSA	kA	CNSA
na	na	k7c4	na
Techblog	Techblog	k1gInSc4	Techblog
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Seminář	seminář	k1gInSc1	seminář
CNSA	CNSA	kA	CNSA
</s>
</p>
