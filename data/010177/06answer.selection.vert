<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
národní	národní	k2eAgFnSc1d1	národní
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
agentura	agentura	k1gFnSc1	agentura
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
pinyin	pinyin	k2eAgInSc1d1	pinyin
'	'	kIx"	'
<g/>
Zhō	Zhō	k1gFnSc1	Zhō
guójiā	guójiā	k?	guójiā
hángtiā	hángtiā	k?	hángtiā
jú	jú	k0	jú
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
中	中	k?	中
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Čung-kuo	Čunguo	k1gNnSc1	Čung-kuo
kuo-ťia	kuo-ťium	k1gNnSc2	kuo-ťium
chang-tchien	changchina	k1gFnPc2	chang-tchina
ťü	ťü	k?	ťü
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
CNSA	CNSA	kA	CNSA
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
China	China	k1gFnSc1	China
National	National	k1gFnSc3	National
Space	Spaec	k1gInSc2	Spaec
Administration	Administration	k1gInSc1	Administration
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
civilní	civilní	k2eAgFnSc1d1	civilní
organizace	organizace	k1gFnSc1	organizace
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
postup	postup	k1gInSc4	postup
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
vesmírném	vesmírný	k2eAgInSc6d1	vesmírný
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
