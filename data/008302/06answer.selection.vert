<s>
Klapzubova	Klapzubův	k2eAgFnSc1d1	Klapzubova
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
humoristická	humoristický	k2eAgFnSc1d1	humoristická
povídka	povídka	k1gFnSc1	povídka
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Eduarda	Eduard	k1gMnSc2	Eduard
Basse	Bass	k1gMnSc2	Bass
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
ze	z	k7c2	z
sportovního	sportovní	k2eAgNnSc2d1	sportovní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
