<p>
<s>
Klapzubova	Klapzubův	k2eAgFnSc1d1	Klapzubova
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
humoristická	humoristický	k2eAgFnSc1d1	humoristická
povídka	povídka	k1gFnSc1	povídka
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Eduarda	Eduard	k1gMnSc2	Eduard
Basse	Bass	k1gMnSc2	Bass
s	s	k7c7	s
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Josefa	Josef	k1gMnSc2	Josef
Čapka	Čapek	k1gMnSc2	Čapek
ze	z	k7c2	z
sportovního	sportovní	k2eAgNnSc2d1	sportovní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
jsou	být	k5eAaImIp3nP	být
členové	člen	k1gMnPc1	člen
rodiny	rodina	k1gFnSc2	rodina
Klapzubů	Klapzub	k1gInPc2	Klapzub
z	z	k7c2	z
Dolních	dolní	k2eAgFnPc2d1	dolní
Bukviček	bukvička	k1gFnPc2	bukvička
u	u	k7c2	u
Kouřimě	Kouřim	k1gFnSc2	Kouřim
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
kompletní	kompletní	k2eAgInSc4d1	kompletní
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
tým	tým	k1gInSc4	tým
S.	S.	kA	S.
K.	K.	kA	K.
KLAPZUBOVA	KLAPZUBOVA	kA	KLAPZUBOVA
JEDENÁCTKA	jedenáctka	k1gFnSc1	jedenáctka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
je	být	k5eAaImIp3nS	být
napsána	napsat	k5eAaBmNgFnS	napsat
ve	v	k7c6	v
spisovné	spisovný	k2eAgFnSc6d1	spisovná
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
občas	občas	k6eAd1	občas
je	být	k5eAaImIp3nS	být
užit	užit	k2eAgInSc4d1	užit
slang	slang	k1gInSc4	slang
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
knihy	kniha	k1gFnSc2	kniha
jako	jako	k8xC	jako
vypravěč	vypravěč	k1gMnSc1	vypravěč
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
snaží	snažit	k5eAaImIp3nS	snažit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
hlavní	hlavní	k2eAgNnSc4d1	hlavní
poselství	poselství	k1gNnSc4	poselství
knihy	kniha	k1gFnSc2	kniha
<g/>
:	:	kIx,	:
oslavu	oslava	k1gFnSc4	oslava
ryzího	ryzí	k2eAgNnSc2d1	ryzí
bratrství	bratrství	k1gNnSc2	bratrství
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
poctivou	poctivý	k2eAgFnSc7d1	poctivá
prací	práce	k1gFnSc7	práce
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
proslulý	proslulý	k2eAgMnSc1d1	proslulý
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
zfilmován	zfilmovat	k5eAaPmNgMnS	zfilmovat
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
námětem	námět	k1gInSc7	námět
několika	několik	k4yIc2	několik
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
povídky	povídka	k1gFnPc4	povídka
jsou	být	k5eAaImIp3nP	být
máma	máma	k1gFnSc1	máma
Klapzubová	Klapzubová	k1gFnSc1	Klapzubová
(	(	kIx(	(
<g/>
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Klapzubka	Klapzubka	k1gFnSc1	Klapzubka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
táta	táta	k1gMnSc1	táta
Klapzuba	Klapzuba	k1gMnSc1	Klapzuba
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neustále	neustále	k6eAd1	neustále
kouří	kouřit	k5eAaImIp3nS	kouřit
fajfku	fajfka	k1gFnSc4	fajfka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
zná	znát	k5eAaImIp3nS	znát
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
a	a	k8xC	a
odvrací	odvracet	k5eAaImIp3nP	odvracet
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
překážky	překážka	k1gFnPc4	překážka
a	a	k8xC	a
nečestné	čestný	k2eNgFnPc4d1	nečestná
praktiky	praktika	k1gFnPc4	praktika
<g/>
,	,	kIx,	,
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
jejich	jejich	k3xOp3gMnPc2	jejich
jedenáct	jedenáct	k4xCc1	jedenáct
synů	syn	k1gMnPc2	syn
tvořících	tvořící	k2eAgMnPc2d1	tvořící
Klapzubovu	Klapzubův	k2eAgFnSc4d1	Klapzubova
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
jedenáctku	jedenáctka	k1gFnSc4	jedenáctka
<g/>
.	.	kIx.	.
</s>
<s>
Synové	syn	k1gMnPc1	syn
drží	držet	k5eAaImIp3nP	držet
vždy	vždy	k6eAd1	vždy
při	při	k7c6	při
sobě	sebe	k3xPyFc6	sebe
<g/>
,	,	kIx,	,
navzájem	navzájem	k6eAd1	navzájem
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nezradili	zradit	k5eNaPmAgMnP	zradit
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
je	on	k3xPp3gFnPc4	on
nezmohla	zmoct	k5eNaPmAgFnS	zmoct
pýcha	pýcha	k1gFnSc1	pýcha
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
slavní	slavný	k2eAgMnPc1d1	slavný
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
má	mít	k5eAaImIp3nS	mít
dvacet	dvacet	k4xCc4	dvacet
kapitol	kapitola	k1gFnPc2	kapitola
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
chudý	chudý	k2eAgMnSc1d1	chudý
chalupník	chalupník	k1gMnSc1	chalupník
Klapzuba	Klapzuba	k1gMnSc1	Klapzuba
učí	učit	k5eAaImIp3nS	učit
svých	svůj	k3xOyFgMnPc2	svůj
jedenáct	jedenáct	k4xCc1	jedenáct
synů	syn	k1gMnPc2	syn
hrát	hrát	k5eAaImF	hrát
fotbal	fotbal	k1gInSc4	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
přísným	přísný	k2eAgInPc3d1	přísný
tréninkům	trénink	k1gInPc3	trénink
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
takřka	takřka	k6eAd1	takřka
hned	hned	k6eAd1	hned
do	do	k7c2	do
první	první	k4xOgFnSc2	první
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
stanou	stanout	k5eAaPmIp3nP	stanout
se	se	k3xPyFc4	se
mistry	mistr	k1gMnPc7	mistr
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
s	s	k7c7	s
celkovým	celkový	k2eAgNnSc7d1	celkové
skóre	skóre	k1gNnSc7	skóre
122	[number]	k4	122
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
porazí	porazit	k5eAaPmIp3nS	porazit
Spartu	Sparta	k1gFnSc4	Sparta
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Slavii	slavie	k1gFnSc4	slavie
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jedou	jet	k5eAaImIp3nP	jet
dobývat	dobývat	k5eAaImF	dobývat
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
chce	chtít	k5eAaImIp3nS	chtít
španělský	španělský	k2eAgInSc1d1	španělský
tým	tým	k1gInSc1	tým
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
Klapzubovy	Klapzubův	k2eAgMnPc4d1	Klapzubův
hochy	hoch	k1gMnPc4	hoch
zmrzačit	zmrzačit	k5eAaPmF	zmrzačit
<g/>
,	,	kIx,	,
navlékne	navléknout	k5eAaPmIp3nS	navléknout
je	on	k3xPp3gNnSc4	on
táta	táta	k1gMnSc1	táta
Klapzuba	Klapzuba	k1gMnSc1	Klapzuba
do	do	k7c2	do
kaučukového	kaučukový	k2eAgInSc2d1	kaučukový
krunýře	krunýř	k1gInSc2	krunýř
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgMnSc2	jenž
byl	být	k5eAaImAgInS	být
napumpován	napumpován	k2eAgInSc1d1	napumpován
vzduch	vzduch	k1gInSc1	vzduch
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
31	[number]	k4	31
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
porazili	porazit	k5eAaPmAgMnP	porazit
Huddersfield	Huddersfield	k1gInSc4	Huddersfield
F.	F.	kA	F.
C.	C.	kA	C.
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
mistry	mistr	k1gMnPc7	mistr
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
přijali	přijmout	k5eAaPmAgMnP	přijmout
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
prince	princ	k1gMnSc2	princ
Waleského	waleský	k2eAgMnSc2d1	waleský
jako	jako	k8xS	jako
náhradníka	náhradník	k1gMnSc2	náhradník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pak	pak	k6eAd1	pak
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
hraje	hrát	k5eAaImIp3nS	hrát
a	a	k8xC	a
trénuje	trénovat	k5eAaImIp3nS	trénovat
v	v	k7c6	v
Dolních	dolní	k2eAgFnPc6d1	dolní
Bukvičkách	bukvička	k1gFnPc6	bukvička
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osmá	osmý	k4xOgFnSc1	osmý
kapitola	kapitola	k1gFnSc1	kapitola
je	být	k5eAaImIp3nS	být
pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
kouzelné	kouzelný	k2eAgFnSc6d1	kouzelná
píšťalce	píšťalka	k1gFnSc6	píšťalka
dědečka	dědeček	k1gMnSc2	dědeček
nynějších	nynější	k2eAgMnPc2d1	nynější
Klapzubů	Klapzuba	k1gMnPc2	Klapzuba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
vymyslil	vymyslit	k5eAaPmAgMnS	vymyslit
jeden	jeden	k4xCgMnSc1	jeden
oregonský	oregonský	k2eAgMnSc1d1	oregonský
dědeček	dědeček	k1gMnSc1	dědeček
pro	pro	k7c4	pro
americké	americký	k2eAgMnPc4d1	americký
kluky	kluk	k1gMnPc4	kluk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
už	už	k6eAd1	už
nevěří	věřit	k5eNaImIp3nP	věřit
klasickým	klasický	k2eAgFnPc3d1	klasická
pohádkám	pohádka	k1gFnPc3	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
další	další	k2eAgFnSc2d1	další
kapitoly	kapitola	k1gFnSc2	kapitola
je	být	k5eAaImIp3nS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
hymna	hymna	k1gFnSc1	hymna
nestora	nestor	k1gMnSc2	nestor
českých	český	k2eAgMnPc2d1	český
básníků	básník	k1gMnPc2	básník
Vincence	Vincenc	k1gMnSc2	Vincenc
Kabrny	Kabrna	k1gFnSc2	Kabrna
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
Klapzubově	Klapzubův	k2eAgFnSc3d1	Klapzubova
jedenáctce	jedenáctka	k1gFnSc3	jedenáctka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Na	na	k7c6	na
zelené	zelený	k2eAgFnSc6d1	zelená
trávě	tráva	k1gFnSc6	tráva
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
svítí	svítit	k5eAaImIp3nP	svítit
bílé	bílý	k2eAgFnPc1d1	bílá
linie	linie	k1gFnPc1	linie
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kdo	kdo	k3yQnSc1	kdo
che	che	k0	che
s	s	k7c7	s
námi	my	k3xPp1nPc7	my
hráti	hrát	k5eAaImF	hrát
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ten	ten	k3xDgMnSc1	ten
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
vždy	vždy	k6eAd1	vždy
vypije	vypít	k5eAaPmIp3nS	vypít
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Nežli	nežli	k8xS	nežli
cizí	cizí	k2eAgMnSc1d1	cizí
brankář	brankář	k1gMnSc1	brankář
</s>
</p>
<p>
<s>
natáhne	natáhnout	k5eAaPmIp3nS	natáhnout
svá	svůj	k3xOyFgNnPc4	svůj
klepeta	klepeto	k1gNnPc4	klepeto
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zazvoní	zazvonit	k5eAaPmIp3nS	zazvonit
už	už	k9	už
rána	rána	k1gFnSc1	rána
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
míč	míč	k1gInSc1	míč
se	se	k3xPyFc4	se
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
třepetá	třepetat	k5eAaImIp3nS	třepetat
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
jim	on	k3xPp3gMnPc3	on
jeden	jeden	k4xCgMnSc1	jeden
pihovatý	pihovatý	k2eAgMnSc1d1	pihovatý
kluk	kluk	k1gMnSc1	kluk
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
nemůže	moct	k5eNaImIp3nS	moct
zahrát	zahrát	k5eAaPmF	zahrát
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
on	on	k3xPp3gMnSc1	on
hraje	hrát	k5eAaImIp3nS	hrát
pro	pro	k7c4	pro
čest	čest	k1gFnSc4	čest
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
oni	onen	k3xDgMnPc1	onen
pro	pro	k7c4	pro
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
hra	hra	k1gFnSc1	hra
Klapzubovy	Klapzubův	k2eAgFnSc2d1	Klapzubova
jedenáctky	jedenáctka	k1gFnSc2	jedenáctka
úplně	úplně	k6eAd1	úplně
změní	změnit	k5eAaPmIp3nS	změnit
<g/>
,	,	kIx,	,
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
vytratí	vytratit	k5eAaPmIp3nS	vytratit
vášeň	vášeň	k1gFnSc1	vášeň
<g/>
,	,	kIx,	,
kluci	kluk	k1gMnPc1	kluk
jsou	být	k5eAaImIp3nP	být
zamlklí	zamlklý	k2eAgMnPc1d1	zamlklý
<g/>
,	,	kIx,	,
neveselí	veselý	k2eNgMnPc1d1	neveselý
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
si	se	k3xPyFc3	se
toho	ten	k3xDgNnSc2	ten
všimnou	všimnout	k5eAaPmIp3nP	všimnout
sportovní	sportovní	k2eAgMnPc1d1	sportovní
zpravodajové	zpravodaj	k1gMnPc1	zpravodaj
i	i	k8xC	i
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Táta	táta	k1gMnSc1	táta
si	se	k3xPyFc3	se
s	s	k7c7	s
chlapci	chlapec	k1gMnPc7	chlapec
promluví	promluvit	k5eAaPmIp3nS	promluvit
a	a	k8xC	a
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
hraním	hraní	k1gNnSc7	hraní
skončí	skončit	k5eAaPmIp3nP	skončit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
slávou	sláva	k1gFnSc7	sláva
zaorají	zaorat	k5eAaImIp3nP	zaorat
hřiště	hřiště	k1gNnSc4	hřiště
a	a	k8xC	a
zasejí	zasít	k5eAaPmIp3nP	zasít
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
poté	poté	k6eAd1	poté
ještě	ještě	k6eAd1	ještě
jedou	jet	k5eAaImIp3nP	jet
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
mistry	mistr	k1gMnPc7	mistr
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
v	v	k7c6	v
první	první	k4xOgFnSc6	první
půli	půle	k1gFnSc6	půle
dostanou	dostat	k5eAaPmIp3nP	dostat
gól	gól	k1gInSc4	gól
(	(	kIx(	(
<g/>
první	první	k4xOgFnPc4	první
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
otec	otec	k1gMnSc1	otec
Klapzuba	Klapzuba	k1gMnSc1	Klapzuba
překousne	překousnout	k5eAaPmIp3nS	překousnout
svou	svůj	k3xOyFgFnSc4	svůj
dýmku	dýmka	k1gFnSc4	dýmka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
domluvě	domluva	k1gFnSc6	domluva
se	se	k3xPyFc4	se
ale	ale	k9	ale
hoši	hoch	k1gMnPc1	hoch
vzchopí	vzchopit	k5eAaPmIp3nP	vzchopit
a	a	k8xC	a
zápas	zápas	k1gInSc4	zápas
vyhrají	vyhrát	k5eAaPmIp3nP	vyhrát
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
domů	dům	k1gInPc2	dům
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
rovníku	rovník	k1gInSc2	rovník
chytne	chytnout	k5eAaPmIp3nS	chytnout
tropická	tropický	k2eAgFnSc1d1	tropická
bouře	bouře	k1gFnSc1	bouře
a	a	k8xC	a
parník	parník	k1gInSc1	parník
Timor	Timora	k1gFnPc2	Timora
s	s	k7c7	s
Klapzuby	Klapzub	k1gInPc7	Klapzub
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
se	se	k3xPyFc4	se
rozlomí	rozlomit	k5eAaPmIp3nS	rozlomit
a	a	k8xC	a
potopí	potopit	k5eAaPmIp3nS	potopit
<g/>
.	.	kIx.	.
</s>
<s>
Poštovní	poštovní	k2eAgInSc4d1	poštovní
parník	parník	k1gInSc4	parník
Argo	Argo	k1gMnSc1	Argo
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jede	jet	k5eAaImIp3nS	jet
Timoru	Timora	k1gFnSc4	Timora
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
přijede	přijet	k5eAaPmIp3nS	přijet
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
zachrání	zachránit	k5eAaPmIp3nP	zachránit
sice	sice	k8xC	sice
celou	celý	k2eAgFnSc4d1	celá
posádku	posádka	k1gFnSc4	posádka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Klapzuby	Klapzuba	k1gFnPc4	Klapzuba
už	už	k6eAd1	už
nenajde	najít	k5eNaPmIp3nS	najít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klapzubové	Klapzuba	k1gMnPc1	Klapzuba
mezitím	mezitím	k6eAd1	mezitím
v	v	k7c6	v
bezpečí	bezpečí	k1gNnSc6	bezpečí
uniknou	uniknout	k5eAaPmIp3nP	uniknout
z	z	k7c2	z
potápějící	potápějící	k2eAgFnSc2d1	potápějící
se	se	k3xPyFc4	se
lodi	loď	k1gFnSc2	loď
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
pryžových	pryžový	k2eAgInPc6d1	pryžový
oblecích	oblek	k1gInPc6	oblek
plněných	plněný	k2eAgInPc6d1	plněný
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
užili	užít	k5eAaPmAgMnP	užít
jen	jen	k6eAd1	jen
jednou	jeden	k4xCgFnSc7	jeden
při	při	k7c6	při
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Barcelonou	Barcelona	k1gFnSc7	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Doplavou	doplavat	k5eAaPmIp3nP	doplavat
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
ostrovu	ostrov	k1gInSc3	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gMnPc4	on
hned	hned	k6eAd1	hned
zajmou	zajmout	k5eAaPmIp3nP	zajmout
lidožrouti	lidožrout	k1gMnPc1	lidožrout
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
zahrají	zahrát	k5eAaPmIp3nP	zahrát
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
Klapzubové	Klapzuba	k1gMnPc1	Klapzuba
prohrají	prohrát	k5eAaPmIp3nP	prohrát
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
budou	být	k5eAaImBp3nP	být
snědeni	sníst	k5eAaPmNgMnP	sníst
<g/>
,	,	kIx,	,
a	a	k8xC	a
jestliže	jestliže	k8xS	jestliže
vyhrají	vyhrát	k5eAaPmIp3nP	vyhrát
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
budou	být	k5eAaImBp3nP	být
propuštěni	propustit	k5eAaPmNgMnP	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kopání	kopání	k1gNnSc6	kopání
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
se	se	k3xPyFc4	se
kope	kopat	k5eAaImIp3nS	kopat
do	do	k7c2	do
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
nosu	nos	k1gInSc2	nos
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
A	a	k9	a
tak	tak	k6eAd1	tak
táta	táta	k1gMnSc1	táta
své	svůj	k3xOyFgMnPc4	svůj
syny	syn	k1gMnPc4	syn
naučí	naučit	k5eAaPmIp3nS	naučit
různým	různý	k2eAgFnPc3d1	různá
surovostem	surovost	k1gFnPc3	surovost
a	a	k8xC	a
oblékne	obléknout	k5eAaPmIp3nS	obléknout
je	on	k3xPp3gInPc4	on
zase	zase	k9	zase
do	do	k7c2	do
těch	ten	k3xDgFnPc2	ten
pryžových	pryžový	k2eAgFnPc2d1	pryžová
koulí	koule	k1gFnPc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Těch	ten	k3xDgFnPc6	ten
se	se	k3xPyFc4	se
lidožrouti	lidožrout	k1gMnPc1	lidožrout
bojí	bát	k5eAaImIp3nP	bát
a	a	k8xC	a
před	před	k7c7	před
bratry	bratr	k1gMnPc7	bratr
utečou	utéct	k5eAaPmIp3nP	utéct
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Klapzubové	Klapzuba	k1gMnPc1	Klapzuba
vyhrají	vyhrát	k5eAaPmIp3nP	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
běhu	běh	k1gInSc2	běh
utečou	utéct	k5eAaPmIp3nP	utéct
a	a	k8xC	a
ujedou	ujet	k5eAaPmIp3nP	ujet
na	na	k7c6	na
náčelníkově	náčelníkův	k2eAgFnSc6d1	náčelníkova
loďce	loďka	k1gFnSc6	loďka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
je	on	k3xPp3gInPc4	on
zachrání	zachránit	k5eAaPmIp3nS	zachránit
parník	parník	k1gInSc1	parník
Jellicoe	Jellico	k1gInSc2	Jellico
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	on	k3xPp3gNnSc4	on
najde	najít	k5eAaPmIp3nS	najít
uprostřed	uprostřed	k7c2	uprostřed
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
hrají	hrát	k5eAaImIp3nP	hrát
vodní	vodní	k2eAgNnSc4d1	vodní
pólo	pólo	k1gNnSc4	pólo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nevyšli	vyjít	k5eNaPmAgMnP	vyjít
ze	z	k7c2	z
cviku	cvik	k1gInSc2	cvik
<g/>
.	.	kIx.	.
</s>
<s>
Čímž	což	k3yRnSc7	což
velká	velký	k2eAgFnSc1d1	velká
historie	historie	k1gFnSc1	historie
Klapzubovy	Klapzubův	k2eAgFnSc2d1	Klapzubova
jedenáctky	jedenáctka	k1gFnSc2	jedenáctka
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Klapzubova	Klapzubův	k2eAgFnSc1d1	Klapzubova
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Ladislav	Ladislav	k1gMnSc1	Ladislav
Brom	brom	k1gInSc1	brom
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Theodor	Theodor	k1gMnSc1	Theodor
Pištěk	Pištěk	k1gMnSc1	Pištěk
<g/>
,	,	kIx,	,
Antonie	Antonie	k1gFnSc1	Antonie
Nedošinská	Nedošinský	k2eAgFnSc1d1	Nedošinská
<g/>
,	,	kIx,	,
Fanda	Fanda	k1gMnSc1	Fanda
Mrázek	Mrázek	k1gMnSc1	Mrázek
<g/>
,	,	kIx,	,
Raoul	Raoul	k1gInSc1	Raoul
Schránil	schránit	k5eAaBmAgInS	schránit
a	a	k8xC	a
další	další	k2eAgInSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klapzubova	Klapzubův	k2eAgFnSc1d1	Klapzubova
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Eduard	Eduard	k1gMnSc1	Eduard
Hofman	Hofman	k1gMnSc1	Hofman
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Jiří	Jiří	k1gMnSc1	Jiří
Sovák	Sovák	k1gMnSc1	Sovák
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Chramostová	Chramostová	k1gFnSc1	Chramostová
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Hlinomaz	hlinomaz	k1gMnSc1	hlinomaz
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
,	,	kIx,	,
Jiřina	Jiřina	k1gFnSc1	Jiřina
Bohdalová	Bohdalová	k1gFnSc1	Bohdalová
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Ustinov	Ustinov	k1gInSc1	Ustinov
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
http://www.kodovky.cz/kniha/2	[url]	k4	http://www.kodovky.cz/kniha/2
</s>
</p>
<p>
<s>
Klapzubova	Klapzubův	k2eAgFnSc1d1	Klapzubova
jedenáctka	jedenáctka	k1gFnSc1	jedenáctka
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
