<s>
Jako	jako	k8xS	jako
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
německý	německý	k2eAgInSc1d1	německý
stát	stát	k1gInSc1	stát
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
nacistické	nacistický	k2eAgFnSc2d1	nacistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
(	(	kIx(	(
<g/>
jmenování	jmenování	k1gNnSc4	jmenování
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
německým	německý	k2eAgMnSc7d1	německý
kancléřem	kancléř	k1gMnSc7	kancléř
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
a	a	k8xC	a
diktátorské	diktátorský	k2eAgFnSc2d1	diktátorská
pravomoci	pravomoc	k1gFnSc2	pravomoc
Hitler	Hitler	k1gMnSc1	Hitler
převzal	převzít	k5eAaPmAgMnS	převzít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
do	do	k7c2	do
kapitulace	kapitulace	k1gFnSc2	kapitulace
Německa	Německo	k1gNnSc2	Německo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
byl	být	k5eAaImAgInS	být
Německá	německý	k2eAgFnSc1d1	německá
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Deutsches	Deutsches	k1gInSc1	Deutsches
Reich	Reich	k?	Reich
<g/>
)	)	kIx)	)
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
a	a	k8xC	a
Velkoněmecká	velkoněmecký	k2eAgFnSc1d1	Velkoněmecká
říše	říše	k1gFnSc1	říše
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Großdeutsches	Großdeutsches	k1gInSc1	Großdeutsches
Reich	Reich	k?	Reich
<g/>
)	)	kIx)	)
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
zániku	zánik	k1gInSc2	zánik
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgNnSc6d1	další
dnes	dnes	k6eAd1	dnes
používaným	používaný	k2eAgNnSc7d1	používané
označením	označení	k1gNnSc7	označení
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
patří	patřit	k5eAaImIp3nS	patřit
pojem	pojem	k1gInSc4	pojem
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
národně-socialistický	národněocialistický	k2eAgInSc1d1	národně-socialistický
stát	stát	k1gInSc1	stát
či	či	k8xC	či
vůdcovský	vůdcovský	k2eAgInSc1d1	vůdcovský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
se	s	k7c7	s
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
dostal	dostat	k5eAaPmAgInS	dostat
legálně	legálně	k6eAd1	legálně
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
dosazen	dosadit	k5eAaPmNgMnS	dosadit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
říšského	říšský	k2eAgMnSc2d1	říšský
kancléře	kancléř	k1gMnSc2	kancléř
prezidentem	prezident	k1gMnSc7	prezident
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Německa	Německo	k1gNnSc2	Německo
Paulem	Paul	k1gMnSc7	Paul
von	von	k1gInSc1	von
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
předsedal	předsedat	k5eAaImAgMnS	předsedat
koaličnímu	koaliční	k2eAgInSc3d1	koaliční
kabinetu	kabinet	k1gInSc3	kabinet
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
poměrně	poměrně	k6eAd1	poměrně
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
od	od	k7c2	od
vlády	vláda	k1gFnSc2	vláda
odstavit	odstavit	k5eAaPmF	odstavit
ty	ten	k3xDgMnPc4	ten
členy	člen	k1gMnPc4	člen
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
členy	člen	k1gMnPc7	člen
nacistické	nacistický	k2eAgFnSc2d1	nacistická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uchopení	uchopení	k1gNnSc6	uchopení
moci	moc	k1gFnSc2	moc
se	se	k3xPyFc4	se
povedlo	povést	k5eAaPmAgNnS	povést
nacistickému	nacistický	k2eAgInSc3d1	nacistický
režimu	režim	k1gInSc3	režim
opět	opět	k6eAd1	opět
postavit	postavit	k5eAaPmF	postavit
ekonomiku	ekonomika	k1gFnSc4	ekonomika
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
ukončit	ukončit	k5eAaPmF	ukončit
vysokou	vysoký	k2eAgFnSc4d1	vysoká
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
pomocí	pomocí	k7c2	pomocí
masových	masový	k2eAgFnPc2d1	masová
investic	investice	k1gFnPc2	investice
do	do	k7c2	do
zbrojení	zbrojení	k1gNnSc2	zbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
prosperity	prosperita	k1gFnSc2	prosperita
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
popularity	popularita	k1gFnSc2	popularita
režimu	režim	k1gInSc2	režim
mezi	mezi	k7c7	mezi
německým	německý	k2eAgNnSc7d1	německé
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
oddaností	oddanost	k1gFnSc7	oddanost
Němců	Němec	k1gMnPc2	Němec
jeho	jeho	k3xOp3gFnSc3	jeho
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
opozice	opozice	k1gFnSc1	opozice
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
liberálů	liberál	k1gMnPc2	liberál
<g/>
,	,	kIx,	,
socialistů	socialist	k1gMnPc2	socialist
a	a	k8xC	a
komunistů	komunista	k1gMnPc2	komunista
byla	být	k5eAaImAgFnS	být
tvrdě	tvrdě	k6eAd1	tvrdě
potlačována	potlačovat	k5eAaImNgFnS	potlačovat
tajnou	tajný	k2eAgFnSc7d1	tajná
státní	státní	k2eAgFnSc7d1	státní
policií	policie	k1gFnSc7	policie
gestapem	gestapo	k1gNnSc7	gestapo
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
Heinricha	Heinrich	k1gMnSc2	Heinrich
Himmlera	Himmler	k1gMnSc2	Himmler
<g/>
.	.	kIx.	.
</s>
<s>
Nacistická	nacistický	k2eAgFnSc1d1	nacistická
strana	strana	k1gFnSc1	strana
postupně	postupně	k6eAd1	postupně
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
soudy	soud	k1gInPc4	soud
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnPc4d1	místní
vlády	vláda	k1gFnPc4	vláda
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
občanské	občanský	k2eAgFnPc4d1	občanská
organizace	organizace	k1gFnPc4	organizace
vyjma	vyjma	k7c2	vyjma
protestantských	protestantský	k2eAgMnPc2d1	protestantský
a	a	k8xC	a
katolických	katolický	k2eAgMnPc2d1	katolický
<g/>
.	.	kIx.	.
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
utvářelo	utvářet	k5eAaImAgNnS	utvářet
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
propagandy	propaganda	k1gFnSc2	propaganda
vedené	vedený	k2eAgFnSc2d1	vedená
Josefem	Josef	k1gMnSc7	Josef
Goebbelsem	Goebbels	k1gMnSc7	Goebbels
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
k	k	k7c3	k
tomu	ten	k3xDgMnSc3	ten
využíval	využívat	k5eAaImAgMnS	využívat
masmédia	masmédium	k1gNnSc2	masmédium
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
a	a	k8xC	a
uhrančivé	uhrančivý	k2eAgInPc1d1	uhrančivý
Hitlerovy	Hitlerův	k2eAgInPc1d1	Hitlerův
projevy	projev	k1gInPc1	projev
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
aparát	aparát	k1gInSc1	aparát
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
vykreslit	vykreslit	k5eAaPmF	vykreslit
Hitlera	Hitler	k1gMnSc4	Hitler
jako	jako	k8xS	jako
vůdce	vůdce	k1gMnSc4	vůdce
německého	německý	k2eAgInSc2d1	německý
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
Führer	Führer	k1gInSc1	Führer
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
účelu	účel	k1gInSc2	účel
mu	on	k3xPp3gMnSc3	on
předával	předávat	k5eAaImAgMnS	předávat
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
veškerou	veškerý	k3xTgFnSc4	veškerý
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
existence	existence	k1gFnSc2	existence
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
Hitler	Hitler	k1gMnSc1	Hitler
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
strachu	strach	k1gInSc2	strach
a	a	k8xC	a
vydírání	vydírání	k1gNnSc2	vydírání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznášel	vznášet	k5eAaImAgInS	vznášet
zdánlivě	zdánlivě	k6eAd1	zdánlivě
důvodné	důvodný	k2eAgInPc4d1	důvodný
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
své	svůj	k3xOyFgMnPc4	svůj
sousedy	soused	k1gMnPc4	soused
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
v	v	k7c6	v
případě	případ	k1gInSc6	případ
neplnění	neplnění	k1gNnSc1	neplnění
vyhrožoval	vyhrožovat	k5eAaImAgMnS	vyhrožovat
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
pokusili	pokusit	k5eAaPmAgMnP	pokusit
jeho	jeho	k3xOp3gMnPc1	jeho
protivníci	protivník	k1gMnPc1	protivník
uklidnit	uklidnit	k5eAaPmF	uklidnit
ústupky	ústupek	k1gInPc4	ústupek
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc4	ten
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
se	se	k3xPyFc4	se
zas	zas	k6eAd1	zas
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
postupem	postup	k1gInSc7	postup
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
značné	značný	k2eAgNnSc1d1	značné
území	území	k1gNnSc1	území
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
odstoupení	odstoupení	k1gNnSc2	odstoupení
československého	československý	k2eAgNnSc2d1	Československé
pohraničí	pohraničí	k1gNnSc2	pohraničí
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Agresivní	agresivní	k2eAgFnSc1d1	agresivní
politika	politika	k1gFnSc1	politika
Německa	Německo	k1gNnSc2	Německo
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vypovědění	vypovědění	k1gNnSc3	vypovědění
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
se	se	k3xPyFc4	se
Německo	Německo	k1gNnSc1	Německo
začalo	začít	k5eAaPmAgNnS	začít
znovuvyzbrojovat	znovuvyzbrojovat	k5eAaImF	znovuvyzbrojovat
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
si	se	k3xPyFc3	se
opětovně	opětovně	k6eAd1	opětovně
připojilo	připojit	k5eAaPmAgNnS	připojit
Sársko	Sársko	k1gNnSc1	Sársko
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
remilitarizovalo	remilitarizovat	k5eAaBmAgNnS	remilitarizovat
Porýní	Porýní	k1gNnSc3	Porýní
<g/>
,	,	kIx,	,
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
spojenectví	spojenectví	k1gNnSc4	spojenectví
-	-	kIx~	-
Osu	osa	k1gFnSc4	osa
s	s	k7c7	s
fašistickou	fašistický	k2eAgFnSc7d1	fašistická
Itálií	Itálie	k1gFnSc7	Itálie
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Benita	Benit	k1gMnSc2	Benit
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
<g/>
,	,	kIx,	,
poslalo	poslat	k5eAaPmAgNnS	poslat
pomoc	pomoc	k1gFnSc1	pomoc
Frankovu	Frankův	k2eAgNnSc3d1	Frankovo
fašistickému	fašistický	k2eAgNnSc3d1	fašistické
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
,	,	kIx,	,
anektovalo	anektovat	k5eAaBmAgNnS	anektovat
Rakousko	Rakousko	k1gNnSc1	Rakousko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
a	a	k8xC	a
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
již	již	k6eAd1	již
zmiňované	zmiňovaný	k2eAgNnSc4d1	zmiňované
československé	československý	k2eAgNnSc4d1	Československé
pohraničí	pohraničí	k1gNnSc4	pohraničí
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
zbytek	zbytek	k1gInSc4	zbytek
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Neustále	neustále	k6eAd1	neustále
rostoucí	rostoucí	k2eAgInPc1d1	rostoucí
územní	územní	k2eAgInPc1d1	územní
nároky	nárok	k1gInPc1	nárok
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
invazí	invaze	k1gFnPc2	invaze
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
vyhlásily	vyhlásit	k5eAaPmAgInP	vyhlásit
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
válku	válek	k1gInSc2	válek
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
oficiálně	oficiálně	k6eAd1	oficiálně
začala	začít	k5eAaPmAgFnS	začít
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
čekalo	čekat	k5eAaImAgNnS	čekat
či	či	k8xC	či
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
připraveno	připravit	k5eAaPmNgNnS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
podařilo	podařit	k5eAaPmAgNnS	podařit
obsadit	obsadit	k5eAaPmF	obsadit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Evropy	Evropa	k1gFnSc2	Evropa
od	od	k7c2	od
Francie	Francie	k1gFnSc2	Francie
až	až	k9	až
téměř	téměř	k6eAd1	téměř
po	po	k7c4	po
Moskvu	Moskva	k1gFnSc4	Moskva
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
Norska	Norsko	k1gNnSc2	Norsko
na	na	k7c6	na
severu	sever	k1gInSc6	sever
až	až	k9	až
po	po	k7c4	po
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
pouštní	pouštní	k2eAgFnPc4d1	pouštní
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
většině	většina	k1gFnSc6	většina
okupovaných	okupovaný	k2eAgNnPc2d1	okupované
území	území	k1gNnPc2	území
se	se	k3xPyFc4	se
nacisté	nacista	k1gMnPc1	nacista
pokusili	pokusit	k5eAaPmAgMnP	pokusit
zavést	zavést	k5eAaPmF	zavést
"	"	kIx"	"
<g/>
Nový	nový	k2eAgInSc4d1	nový
pořádek	pořádek	k1gInSc4	pořádek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
masovým	masový	k2eAgFnPc3d1	masová
persekucím	persekuce	k1gFnPc3	persekuce
neněmeckého	německý	k2eNgNnSc2d1	neněmecké
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
vyvražďování	vyvražďování	k1gNnSc1	vyvražďování
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
cikánů	cikán	k1gMnPc2	cikán
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgInS	vžít
termín	termín	k1gInSc1	termín
holokaust	holokaust	k1gInSc1	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
zemřely	zemřít	k5eAaPmAgFnP	zemřít
miliony	milion	k4xCgInPc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
situace	situace	k1gFnSc1	situace
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
obracet	obracet	k5eAaImF	obracet
v	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
porážce	porážka	k1gFnSc3	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
skončila	skončit	k5eAaPmAgFnS	skončit
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
kapitulací	kapitulace	k1gFnSc7	kapitulace
Německa	Německo	k1gNnSc2	Německo
západním	západní	k2eAgMnPc3d1	západní
spojencům	spojenec	k1gMnPc3	spojenec
a	a	k8xC	a
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
na	na	k7c6	na
základech	základ	k1gInPc6	základ
národního	národní	k2eAgNnSc2d1	národní
ponížení	ponížení	k1gNnSc2	ponížení
<g/>
,	,	kIx,	,
hanby	hanba	k1gFnSc2	hanba
a	a	k8xC	a
vzteku	vztek	k1gInSc2	vztek
vycházející	vycházející	k2eAgFnSc1d1	vycházející
ze	z	k7c2	z
znění	znění	k1gNnSc2	znění
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
Německu	Německo	k1gNnSc6	Německo
vnutily	vnutit	k5eAaPmAgFnP	vnutit
vítězné	vítězný	k2eAgFnPc1d1	vítězná
mocnosti	mocnost	k1gFnPc1	mocnost
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
smlouva	smlouva	k1gFnSc1	smlouva
vnucovala	vnucovat	k5eAaImAgFnS	vnucovat
Němcům	Němec	k1gMnPc3	Němec
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c2	za
rozpoutání	rozpoutání	k1gNnSc2	rozpoutání
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
trvalou	trvalý	k2eAgFnSc4d1	trvalá
ztrátu	ztráta	k1gFnSc4	ztráta
či	či	k8xC	či
demilitarizaci	demilitarizace	k1gFnSc4	demilitarizace
některých	některý	k3yIgNnPc2	některý
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
nutnost	nutnost	k1gFnSc4	nutnost
platit	platit	k5eAaImF	platit
reparace	reparace	k1gFnPc4	reparace
v	v	k7c6	v
penězích	peníze	k1gInPc6	peníze
a	a	k8xC	a
ve	v	k7c6	v
zboží	zboží	k1gNnSc6	zboží
a	a	k8xC	a
příkazu	příkaz	k1gInSc6	příkaz
na	na	k7c4	na
redukci	redukce	k1gFnSc4	redukce
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
příčiny	příčina	k1gFnPc4	příčina
nárůstu	nárůst	k1gInSc2	nárůst
popularity	popularita	k1gFnSc2	popularita
nacistické	nacistický	k2eAgFnSc2d1	nacistická
ideologie	ideologie	k1gFnSc2	ideologie
patřily	patřit	k5eAaImAgFnP	patřit
nárůst	nárůst	k1gInSc4	nárůst
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
populaci	populace	k1gFnSc6	populace
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc4	myšlenka
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
"	"	kIx"	"
<g/>
Velkoněmecké	velkoněmecký	k2eAgFnSc2d1	Velkoněmecká
říše	říš	k1gFnSc2	říš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
trvat	trvat	k5eAaImF	trvat
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
občanské	občanský	k2eAgInPc1d1	občanský
nepokoje	nepokoj	k1gInPc1	nepokoj
a	a	k8xC	a
komunistické	komunistický	k2eAgInPc1d1	komunistický
puče	puč	k1gInPc1	puč
<g/>
,	,	kIx,	,
hyperinflace	hyperinflace	k1gFnPc1	hyperinflace
ve	v	k7c6	v
Výmarské	výmarský	k2eAgFnSc6d1	Výmarská
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
probíhající	probíhající	k2eAgFnSc1d1	probíhající
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
nárůst	nárůst	k1gInSc1	nárůst
síly	síla	k1gFnSc2	síla
komunismu	komunismus	k1gInSc2	komunismus
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
důsledky	důsledek	k1gInPc1	důsledek
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
voliči	volič	k1gMnPc1	volič
frustrovaní	frustrovaný	k2eAgMnPc1d1	frustrovaný
prací	práce	k1gFnSc7	práce
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
začali	začít	k5eAaPmAgMnP	začít
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
volit	volit	k5eAaImF	volit
strany	strana	k1gFnPc4	strana
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
či	či	k8xC	či
pravém	pravý	k2eAgInSc6d1	pravý
okraji	okraj	k1gInSc6	okraj
politického	politický	k2eAgNnSc2d1	politické
spektra	spektrum	k1gNnSc2	spektrum
-	-	kIx~	-
extrémisty	extrémista	k1gMnSc2	extrémista
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
nacistickou	nacistický	k2eAgFnSc4d1	nacistická
stranu	strana	k1gFnSc4	strana
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
slibovali	slibovat	k5eAaImAgMnP	slibovat
silnou	silný	k2eAgFnSc4d1	silná
<g/>
,	,	kIx,	,
autoritativní	autoritativní	k2eAgFnSc4d1	autoritativní
vládu	vláda	k1gFnSc4	vláda
namísto	namísto	k7c2	namísto
neefektivních	efektivní	k2eNgMnPc2d1	neefektivní
parlamentních	parlamentní	k2eAgMnPc2d1	parlamentní
republikánů	republikán	k1gMnPc2	republikán
<g/>
,	,	kIx,	,
občanský	občanský	k2eAgInSc1d1	občanský
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
radikální	radikální	k2eAgInPc1d1	radikální
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
politiku	politika	k1gFnSc4	politika
včele	včela	k1gFnSc3	včela
s	s	k7c7	s
plnou	plný	k2eAgFnSc7d1	plná
zaměstnaností	zaměstnanost	k1gFnSc7	zaměstnanost
<g/>
,	,	kIx,	,
navrácení	navrácení	k1gNnSc4	navrácení
tradičních	tradiční	k2eAgFnPc2d1	tradiční
hodnot	hodnota	k1gFnPc2	hodnota
a	a	k8xC	a
hrdosti	hrdost	k1gFnSc2	hrdost
a	a	k8xC	a
rasovou	rasový	k2eAgFnSc4d1	rasová
čistotu	čistota	k1gFnSc4	čistota
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
dosaženou	dosažený	k2eAgFnSc4d1	dosažená
represemi	represe	k1gFnPc7	represe
proti	proti	k7c3	proti
Židům	Žid	k1gMnPc3	Žid
a	a	k8xC	a
marxistům	marxista	k1gMnPc3	marxista
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
všechno	všechen	k3xTgNnSc1	všechen
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
a	a	k8xC	a
solidarity	solidarita	k1gFnSc2	solidarita
namísto	namísto	k7c2	namísto
rozdrobené	rozdrobený	k2eAgFnSc2d1	rozdrobená
demokratické	demokratický	k2eAgFnSc2d1	demokratická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
za	za	k7c4	za
znovuvyzbrojení	znovuvyzbrojení	k1gNnSc4	znovuvyzbrojení
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
reparací	reparace	k1gFnPc2	reparace
a	a	k8xC	a
znovuobsazení	znovuobsazení	k1gNnPc2	znovuobsazení
území	území	k1gNnPc2	území
ztracených	ztracený	k2eAgNnPc2d1	ztracené
vlivem	vlivem	k7c2	vlivem
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
propagovali	propagovat	k5eAaImAgMnP	propagovat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
zrada	zrada	k1gFnSc1	zrada
způsobená	způsobený	k2eAgFnSc1d1	způsobená
liberální	liberální	k2eAgFnSc7d1	liberální
demokracií	demokracie	k1gFnSc7	demokracie
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
připravila	připravit	k5eAaPmAgFnS	připravit
Německo	Německo	k1gNnSc4	Německo
o	o	k7c4	o
národní	národní	k2eAgFnSc4d1	národní
hrdost	hrdost	k1gFnSc4	hrdost
vlivem	vlivem	k7c2	vlivem
konspirace	konspirace	k1gFnSc2	konspirace
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
jejíchž	jejíž	k3xOyRp3gMnPc2	jejíž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
docílit	docílit	k5eAaPmF	docílit
národní	národní	k2eAgFnPc4d1	národní
nejednotnosti	nejednotnost	k1gFnPc4	nejednotnost
a	a	k8xC	a
otrávení	otrávení	k1gNnPc4	otrávení
německé	německý	k2eAgFnSc2d1	německá
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
cíli	cíl	k1gInSc3	cíl
využívala	využívat	k5eAaPmAgFnS	využívat
nacistická	nacistický	k2eAgFnSc1d1	nacistická
propaganda	propaganda	k1gFnSc1	propaganda
Dolchstoßlegende	Dolchstoßlegend	k1gInSc5	Dolchstoßlegend
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hlásala	hlásat	k5eAaImAgFnS	hlásat
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc4	první
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
neprohrála	prohrát	k5eNaPmAgFnS	prohrát
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
porážka	porážka	k1gFnSc1	porážka
byla	být	k5eAaImAgFnS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
zradou	zrada	k1gFnSc7	zrada
civilistů	civilista	k1gMnPc2	civilista
v	v	k7c6	v
zázemí	zázemí	k1gNnSc6	zázemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
německá	německý	k2eAgFnSc1d1	německá
vláda	vláda	k1gFnSc1	vláda
změnila	změnit	k5eAaPmAgFnS	změnit
z	z	k7c2	z
demokratické	demokratický	k2eAgFnSc2d1	demokratická
na	na	k7c6	na
"	"	kIx"	"
<g/>
de	de	k?	de
facto	facto	k1gNnSc1	facto
<g/>
"	"	kIx"	"
konzervativně-nacionalistickou	konzervativněacionalistický	k2eAgFnSc4d1	konzervativně-nacionalistický
autoritativní	autoritativní	k2eAgFnSc4d1	autoritativní
vládu	vláda	k1gFnSc4	vláda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
válečného	válečný	k2eAgMnSc2d1	válečný
hrdiny	hrdina	k1gMnSc2	hrdina
z	z	k7c2	z
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Paula	Paula	k1gFnSc1	Paula
von	von	k1gInSc1	von
Hindenburga	Hindenburga	k1gFnSc1	Hindenburga
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
nelíbil	líbit	k5eNaImAgInS	líbit
liberálně	liberálně	k6eAd1	liberálně
demokratický	demokratický	k2eAgInSc1d1	demokratický
stát	stát	k1gInSc1	stát
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
který	který	k3yQgMnSc1	který
chtěl	chtít	k5eAaImAgMnS	chtít
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
vytvořit	vytvořit	k5eAaPmF	vytvořit
autoritativní	autoritativní	k2eAgInSc1d1	autoritativní
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeným	přirozený	k2eAgMnSc7d1	přirozený
spojencem	spojenec	k1gMnSc7	spojenec
Hindenburga	Hindenburg	k1gMnSc2	Hindenburg
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
autoritativní	autoritativní	k2eAgFnSc2d1	autoritativní
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
Německá	německý	k2eAgFnSc1d1	německá
národně-lidová	národněidový	k2eAgFnSc1d1	národně-lidový
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
Deutschnationale	Deutschnationale	k1gFnSc1	Deutschnationale
Volkspartei	Volksparte	k1gFnSc2	Volksparte
<g/>
,	,	kIx,	,
DNVP	DNVP	kA	DNVP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zastávala	zastávat	k5eAaImAgFnS	zastávat
pozici	pozice	k1gFnSc4	pozice
nacionalistů	nacionalista	k1gMnPc2	nacionalista
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
vlivem	vlivem	k7c2	vlivem
nastupující	nastupující	k2eAgFnSc2d1	nastupující
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
krize	krize	k1gFnSc2	krize
začala	začít	k5eAaPmAgFnS	začít
čelit	čelit	k5eAaImF	čelit
odlivu	odliv	k1gInSc3	odliv
mladých	mladý	k1gMnPc2	mladý
a	a	k8xC	a
více	hodně	k6eAd2	hodně
radikálních	radikální	k2eAgMnPc2d1	radikální
sympatizantů	sympatizant	k1gMnPc2	sympatizant
k	k	k7c3	k
myšlence	myšlenka	k1gFnSc3	myšlenka
revoluce	revoluce	k1gFnSc2	revoluce
zosobňovanou	zosobňovaný	k2eAgFnSc4d1	zosobňovaná
NSDAP	NSDAP	kA	NSDAP
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
strana	strana	k1gFnSc1	strana
taktéž	taktéž	k?	taktéž
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
protiváha	protiváha	k1gFnSc1	protiváha
narůstající	narůstající	k2eAgFnSc6d1	narůstající
popularitě	popularita	k1gFnSc3	popularita
komunismu	komunismus	k1gInSc2	komunismus
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
všemu	všecek	k3xTgInSc3	všecek
středoví	středový	k2eAgMnPc1d1	středový
politici	politik	k1gMnPc1	politik
ztráceli	ztrácet	k5eAaImAgMnP	ztrácet
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
voliči	volič	k1gMnPc1	volič
přecházeli	přecházet	k5eAaImAgMnP	přecházet
k	k	k7c3	k
extrémní	extrémní	k2eAgFnSc3d1	extrémní
levici	levice	k1gFnSc3	levice
či	či	k8xC	či
pravici	pravice	k1gFnSc3	pravice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
ztěžovalo	ztěžovat	k5eAaImAgNnS	ztěžovat
vytvořit	vytvořit	k5eAaPmF	vytvořit
většinovou	většinový	k2eAgFnSc4d1	většinová
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
po	po	k7c6	po
odeznění	odeznění	k1gNnSc6	odeznění
hyperinflace	hyperinflace	k1gFnSc2	hyperinflace
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1922	[number]	k4	1922
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
spolkové	spolkový	k2eAgFnSc2d1	spolková
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
nacisté	nacista	k1gMnPc1	nacista
získali	získat	k5eAaPmAgMnP	získat
pouze	pouze	k6eAd1	pouze
12	[number]	k4	12
křesel	křeslo	k1gNnPc2	křeslo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
1,9	[number]	k4	1,9
%	%	kIx~	%
poslaneckých	poslanecký	k2eAgInPc2d1	poslanecký
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
spolkových	spolkový	k2eAgFnPc6d1	spolková
volbách	volba	k1gFnPc6	volba
vyhlášených	vyhlášený	k2eAgNnPc2d1	vyhlášené
po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
kancléře	kancléř	k1gMnSc2	kancléř
Heinricha	Heinrich	k1gMnSc2	Heinrich
Brüninga	Brüning	k1gMnSc2	Brüning
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1930	[number]	k4	1930
a	a	k8xC	a
krachu	krach	k1gInSc6	krach
akciových	akciový	k2eAgInPc2d1	akciový
trhů	trh	k1gInPc2	trh
v	v	k7c6	v
USA	USA	kA	USA
dostali	dostat	k5eAaPmAgMnP	dostat
nacisté	nacista	k1gMnPc1	nacista
již	již	k6eAd1	již
6	[number]	k4	6
400	[number]	k4	400
000	[number]	k4	000
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
17,5	[number]	k4	17,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
107	[number]	k4	107
poslaneckých	poslanecký	k2eAgInPc2d1	poslanecký
mandátů	mandát	k1gInPc2	mandát
a	a	k8xC	a
NSDAP	NSDAP	kA	NSDAP
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
druhou	druhý	k4xOgFnSc7	druhý
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
v	v	k7c6	v
Reichstagu	Reichstag	k1gInSc6	Reichstag
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1932	[number]	k4	1932
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
další	další	k2eAgFnPc1d1	další
spolkové	spolkový	k2eAgFnPc1d1	spolková
volby	volba	k1gFnPc1	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
nacisté	nacista	k1gMnPc1	nacista
získali	získat	k5eAaPmAgMnP	získat
230	[number]	k4	230
křesel	křeslo	k1gNnPc2	křeslo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
politickou	politický	k2eAgFnSc7d1	politická
stranou	strana	k1gFnSc7	strana
působící	působící	k2eAgFnSc7d1	působící
v	v	k7c4	v
Reichstagu	Reichstaga	k1gFnSc4	Reichstaga
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Hindenburg	Hindenburg	k1gMnSc1	Hindenburg
se	se	k3xPyFc4	se
zdráhal	zdráhat	k5eAaImAgMnS	zdráhat
předat	předat	k5eAaPmF	předat
moc	moc	k6eAd1	moc
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
dřívější	dřívější	k2eAgMnSc1d1	dřívější
kancléř	kancléř	k1gMnSc1	kancléř
Franz	Franz	k1gMnSc1	Franz
von	von	k1gInSc4	von
Papen	Papen	k1gInSc1	Papen
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
dohodu	dohoda	k1gFnSc4	dohoda
mezi	mezi	k7c7	mezi
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
DNVP	DNVP	kA	DNVP
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
se	se	k3xPyFc4	se
stát	stát	k5eAaPmF	stát
říšským	říšský	k2eAgMnSc7d1	říšský
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Hindenburg	Hindenburg	k1gMnSc1	Hindenburg
Hitlera	Hitler	k1gMnSc2	Hitler
kancléřem	kancléř	k1gMnSc7	kancléř
Německa	Německo	k1gNnSc2	Německo
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
skončila	skončit	k5eAaPmAgFnS	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
snaha	snaha	k1gFnSc1	snaha
generála	generál	k1gMnSc2	generál
Kurta	Kurt	k1gMnSc2	Kurt
von	von	k1gInSc4	von
Schleichera	Schleichero	k1gNnSc2	Schleichero
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
stabilní	stabilní	k2eAgFnSc2d1	stabilní
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c4	na
Hindenburga	Hindenburg	k1gMnSc4	Hindenburg
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
nátlak	nátlak	k1gInSc4	nátlak
skrze	skrze	k?	skrze
jeho	jeho	k3xOp3gMnSc4	jeho
syna	syn	k1gMnSc4	syn
Oskara	Oskar	k1gMnSc4	Oskar
von	von	k1gInSc4	von
Hindenburga	Hindenburg	k1gMnSc2	Hindenburg
a	a	k8xC	a
intrik	intrika	k1gFnPc2	intrika
s	s	k7c7	s
Papenem	Papen	k1gInSc7	Papen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sledoval	sledovat	k5eAaImAgInS	sledovat
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Von	von	k1gInSc1	von
Papen	Papen	k1gInSc1	Papen
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
vicekancléře	vicekancléř	k1gMnSc2	vicekancléř
udrží	udržet	k5eAaPmIp3nS	udržet
nacisty	nacista	k1gMnPc7	nacista
jako	jako	k8xC	jako
minoritní	minoritní	k2eAgFnSc4d1	minoritní
část	část	k1gFnSc4	část
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k8xC	i
když	když	k8xS	když
NSDAP	NSDAP	kA	NSDAP
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
oboje	oboj	k1gFnPc4	oboj
volby	volba	k1gFnSc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nezískala	získat	k5eNaPmAgFnS	získat
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
jí	on	k3xPp3gFnSc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
vládu	vláda	k1gFnSc4	vláda
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
DNVP	DNVP	kA	DNVP
<g/>
.	.	kIx.	.
</s>
<s>
Hitlerovým	Hitlerův	k2eAgInSc7d1	Hitlerův
nástupem	nástup	k1gInSc7	nástup
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
započala	započnout	k5eAaPmAgFnS	započnout
etapa	etapa	k1gFnSc1	etapa
nacionálně	nacionálně	k6eAd1	nacionálně
socialistického	socialistický	k2eAgNnSc2d1	socialistické
uchopení	uchopení	k1gNnSc2	uchopení
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1933	[number]	k4	1933
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
budovy	budova	k1gFnSc2	budova
Říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
Hitler	Hitler	k1gMnSc1	Hitler
obvinil	obvinit	k5eAaPmAgMnS	obvinit
komunisty	komunista	k1gMnPc4	komunista
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
nařízení	nařízení	k1gNnSc1	nařízení
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
byla	být	k5eAaImAgFnS	být
zrušena	zrušen	k2eAgFnSc1d1	zrušena
základní	základní	k2eAgFnSc1d1	základní
lidská	lidský	k2eAgFnSc1d1	lidská
práva	práv	k2eAgFnSc1d1	práva
obsažená	obsažený	k2eAgFnSc1d1	obsažená
ve	v	k7c6	v
výmarské	výmarský	k2eAgFnSc6d1	Výmarská
ústavě	ústava	k1gFnSc6	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
agitace	agitace	k1gFnSc1	agitace
byla	být	k5eAaImAgFnS	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
a	a	k8xC	a
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
zanedlouho	zanedlouho	k6eAd1	zanedlouho
postavena	postaven	k2eAgFnSc1d1	postavena
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
sociálních	sociální	k2eAgMnPc2d1	sociální
demokratů	demokrat	k1gMnPc2	demokrat
a	a	k8xC	a
komunistů	komunista	k1gMnPc2	komunista
bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
a	a	k8xC	a
uvězněno	uvězněn	k2eAgNnSc1d1	uvězněno
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
zmocňovacího	zmocňovací	k2eAgInSc2d1	zmocňovací
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
března	březen	k1gInSc2	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
převzala	převzít	k5eAaPmAgFnS	převzít
vláda	vláda	k1gFnSc1	vláda
veškerou	veškerý	k3xTgFnSc4	veškerý
politickou	politický	k2eAgFnSc4d1	politická
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Zmocňovacím	zmocňovací	k2eAgInSc7d1	zmocňovací
zákonem	zákon	k1gInSc7	zákon
Hitler	Hitler	k1gMnSc1	Hitler
legálním	legální	k2eAgInSc7d1	legální
způsobem	způsob	k1gInSc7	způsob
nastolil	nastolit	k5eAaPmAgMnS	nastolit
nacistickou	nacistický	k2eAgFnSc4d1	nacistická
totalitní	totalitní	k2eAgFnSc4d1	totalitní
diktaturu	diktatura	k1gFnSc4	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
krokem	krok	k1gInSc7	krok
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
Německa	Německo	k1gNnSc2	Německo
k	k	k7c3	k
přeměně	přeměna	k1gFnSc3	přeměna
v	v	k7c4	v
totalitní	totalitní	k2eAgInSc4d1	totalitní
stát	stát	k1gInSc4	stát
bylo	být	k5eAaImAgNnS	být
zglajchšaltování	zglajchšaltování	k1gNnSc1	zglajchšaltování
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
usměrnění	usměrnění	k1gNnSc1	usměrnění
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
celého	celý	k2eAgInSc2d1	celý
politického	politický	k2eAgInSc2d1	politický
a	a	k8xC	a
správního	správní	k2eAgInSc2d1	správní
aparátu	aparát	k1gInSc2	aparát
a	a	k8xC	a
všech	všecek	k3xTgFnPc2	všecek
oblastí	oblast	k1gFnPc2	oblast
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
<g/>
,	,	kIx,	,
sociálního	sociální	k2eAgInSc2d1	sociální
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
sloučily	sloučit	k5eAaPmAgInP	sloučit
s	s	k7c7	s
NSDAP	NSDAP	kA	NSDAP
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgFnP	být
zakázány	zakázat	k5eAaPmNgFnP	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
osud	osud	k1gInSc1	osud
stihl	stihnout	k5eAaPmAgInS	stihnout
také	také	k9	také
odbory	odbor	k1gInPc4	odbor
a	a	k8xC	a
různé	různý	k2eAgFnPc4d1	různá
profesní	profesní	k2eAgFnPc4d1	profesní
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
pro	pro	k7c4	pro
Hitlera	Hitler	k1gMnSc4	Hitler
byla	být	k5eAaImAgFnS	být
SA	SA	kA	SA
<g/>
,	,	kIx,	,
paramilitární	paramilitární	k2eAgFnSc2d1	paramilitární
organizace	organizace	k1gFnSc2	organizace
NSDAP	NSDAP	kA	NSDAP
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
SA	SA	kA	SA
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Röhm	Röhm	k1gMnSc1	Röhm
<g/>
,	,	kIx,	,
toužil	toužit	k5eAaImAgInS	toužit
začlenit	začlenit	k5eAaPmF	začlenit
své	svůj	k3xOyFgMnPc4	svůj
SA-many	SAan	k1gMnPc4	SA-man
do	do	k7c2	do
řad	řada	k1gFnPc2	řada
reichswehru	reichswehra	k1gFnSc4	reichswehra
a	a	k8xC	a
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
narušovalo	narušovat	k5eAaImAgNnS	narušovat
Hitlerovy	Hitlerův	k2eAgFnPc4d1	Hitlerova
záměry	záměra	k1gFnPc4	záměra
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1934	[number]	k4	1934
proto	proto	k8xC	proto
Hitler	Hitler	k1gMnSc1	Hitler
nařídil	nařídit	k5eAaPmAgMnS	nařídit
Röhma	Röhma	k1gNnSc4	Röhma
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
podřízené	podřízený	k1gMnPc4	podřízený
zatknout	zatknout	k5eAaPmF	zatknout
a	a	k8xC	a
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
noc	noc	k1gFnSc4	noc
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
nožů	nůž	k1gInPc2	nůž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
posílila	posílit	k5eAaPmAgFnS	posílit
své	své	k1gNnSc4	své
postavení	postavení	k1gNnSc2	postavení
SS	SS	kA	SS
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Reichsführera	Reichsführer	k1gMnSc2	Reichsführer
SS	SS	kA	SS
Heinricha	Heinrich	k1gMnSc2	Heinrich
Himmlera	Himmler	k1gMnSc2	Himmler
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
záhy	záhy	k6eAd1	záhy
podléhalo	podléhat	k5eAaImAgNnS	podléhat
gestapo	gestapo	k1gNnSc1	gestapo
(	(	kIx(	(
<g/>
tajná	tajný	k2eAgFnSc1d1	tajná
státní	státní	k2eAgFnSc1d1	státní
policie	policie	k1gFnSc1	policie
<g/>
)	)	kIx)	)
a	a	k8xC	a
nově	nově	k6eAd1	nově
zřízené	zřízený	k2eAgInPc4d1	zřízený
koncentrační	koncentrační	k2eAgInPc4d1	koncentrační
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byly	být	k5eAaImAgInP	být
umisťovány	umisťovat	k5eAaImNgInP	umisťovat
politicky	politicky	k6eAd1	politicky
(	(	kIx(	(
<g/>
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
,	,	kIx,	,
komunisté	komunista	k1gMnPc1	komunista
<g/>
)	)	kIx)	)
a	a	k8xC	a
rasově	rasově	k6eAd1	rasově
(	(	kIx(	(
<g/>
Židé	Žid	k1gMnPc1	Žid
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
nežádoucí	žádoucí	k2eNgFnPc1d1	nežádoucí
osoby	osoba	k1gFnPc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
spadala	spadat	k5eAaImAgNnP	spadat
do	do	k7c2	do
Himmlerovy	Himmlerův	k2eAgFnSc2d1	Himmlerova
kompetence	kompetence	k1gFnSc2	kompetence
veškerá	veškerý	k3xTgFnSc1	veškerý
policie	policie	k1gFnSc1	policie
v	v	k7c6	v
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
tak	tak	k6eAd1	tak
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
možnou	možný	k2eAgFnSc4d1	možná
opozici	opozice	k1gFnSc4	opozice
vně	vně	k6eAd1	vně
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
NSDAP	NSDAP	kA	NSDAP
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Hindenburgově	Hindenburgův	k2eAgFnSc6d1	Hindenburgova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1934	[number]	k4	1934
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
nechal	nechat	k5eAaPmAgMnS	nechat
jmenovat	jmenovat	k5eAaImF	jmenovat
vůdcem	vůdce	k1gMnSc7	vůdce
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
"	"	kIx"	"
<g/>
Führer	Führer	k1gInSc1	Führer
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vyšších	vysoký	k2eAgMnPc2d2	vyšší
důstojníků	důstojník	k1gMnPc2	důstojník
reichswehru	reichswehra	k1gFnSc4	reichswehra
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prodělali	prodělat	k5eAaPmAgMnP	prodělat
výcvik	výcvik	k1gInSc4	výcvik
ještě	ještě	k9	ještě
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nesmířila	smířit	k5eNaPmAgFnS	smířit
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
důstojníci	důstojník	k1gMnPc1	důstojník
spatřovali	spatřovat	k5eAaImAgMnP	spatřovat
v	v	k7c6	v
Hitlerovi	Hitler	k1gMnSc6	Hitler
obhájce	obhájce	k1gMnSc1	obhájce
svých	svůj	k3xOyFgMnPc2	svůj
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
opětovně	opětovně	k6eAd1	opětovně
vyzbrojí	vyzbrojit	k5eAaPmIp3nS	vyzbrojit
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
zasadí	zasadit	k5eAaPmIp3nS	zasadit
se	se	k3xPyFc4	se
o	o	k7c4	o
revizi	revize	k1gFnSc4	revize
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
znovu	znovu	k6eAd1	znovu
vybuduje	vybudovat	k5eAaPmIp3nS	vybudovat
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
velmoc	velmoc	k1gFnSc4	velmoc
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgInSc1d1	generální
štáb	štáb	k1gInSc1	štáb
reichswehru	reichswehra	k1gFnSc4	reichswehra
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
Hitler	Hitler	k1gMnSc1	Hitler
naklonil	naklonit	k5eAaPmAgMnS	naklonit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
když	když	k8xS	když
při	při	k7c6	při
noci	noc	k1gFnSc6	noc
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
nožů	nůž	k1gInPc2	nůž
provedl	provést	k5eAaPmAgInS	provést
čistku	čistka	k1gFnSc4	čistka
v	v	k7c6	v
SA	SA	kA	SA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
prezidenta	prezident	k1gMnSc2	prezident
Paula	Paul	k1gMnSc2	Paul
von	von	k1gInSc1	von
Hindenburga	Hindenburga	k1gFnSc1	Hindenburga
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Hitler	Hitler	k1gMnSc1	Hitler
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
a	a	k8xC	a
změnil	změnit	k5eAaPmAgInS	změnit
text	text	k1gInSc1	text
vojenské	vojenský	k2eAgFnSc2d1	vojenská
přísahy	přísaha	k1gFnSc2	přísaha
–	–	k?	–
zatímco	zatímco	k8xS	zatímco
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
přísahali	přísahat	k5eAaImAgMnP	přísahat
vojáci	voják	k1gMnPc1	voják
věrnost	věrnost	k1gFnSc4	věrnost
národu	národ	k1gInSc2	národ
a	a	k8xC	a
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
přísahali	přísahat	k5eAaImAgMnP	přísahat
věrnost	věrnost	k1gFnSc4	věrnost
vůdci	vůdce	k1gMnSc3	vůdce
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
národa	národ	k1gInSc2	národ
Adolfu	Adolf	k1gMnSc3	Adolf
Hitlerovi	Hitler	k1gMnSc3	Hitler
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
armáda	armáda	k1gFnSc1	armáda
44	[number]	k4	44
generálů	generál	k1gMnPc2	generál
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
261	[number]	k4	261
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1935	[number]	k4	1935
zavedli	zavést	k5eAaPmAgMnP	zavést
nacisté	nacista	k1gMnPc1	nacista
brannou	branný	k2eAgFnSc4d1	Branná
povinnost	povinnost	k1gFnSc4	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
postupně	postupně	k6eAd1	postupně
opět	opět	k6eAd1	opět
získala	získat	k5eAaPmAgFnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
prestiž	prestiž	k1gFnSc4	prestiž
a	a	k8xC	a
nacistický	nacistický	k2eAgInSc4d1	nacistický
režim	režim	k1gInSc4	režim
navíc	navíc	k6eAd1	navíc
přikládal	přikládat	k5eAaImAgMnS	přikládat
armádním	armádní	k2eAgMnSc7d1	armádní
záležitostem	záležitost	k1gFnPc3	záležitost
mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
důležitost	důležitost	k1gFnSc4	důležitost
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1937	[number]	k4	1937
a	a	k8xC	a
1938	[number]	k4	1938
objevily	objevit	k5eAaPmAgFnP	objevit
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
první	první	k4xOgInPc1	první
náznaky	náznak	k1gInPc1	náznak
opozice	opozice	k1gFnSc2	opozice
vůči	vůči	k7c3	vůči
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgMnPc4	některý
důstojníky	důstojník	k1gMnPc4	důstojník
znepokojil	znepokojit	k5eAaPmAgMnS	znepokojit
především	především	k6eAd1	především
Hitlerův	Hitlerův	k2eAgInSc4d1	Hitlerův
plán	plán	k1gInSc4	plán
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1937	[number]	k4	1937
na	na	k7c6	na
napadení	napadení	k1gNnSc6	napadení
Československa	Československo	k1gNnSc2	Československo
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
von	von	k1gInSc4	von
Blomberg	Blomberg	k1gMnSc1	Blomberg
a	a	k8xC	a
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
pozemního	pozemní	k2eAgNnSc2d1	pozemní
vojska	vojsko	k1gNnSc2	vojsko
generálplukovník	generálplukovník	k1gMnSc1	generálplukovník
von	von	k1gInSc1	von
Fritsch	Fritscha	k1gFnPc2	Fritscha
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
nad	nad	k7c7	nad
plánem	plán	k1gInSc7	plán
pochybnosti	pochybnost	k1gFnSc2	pochybnost
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
zbaveni	zbavit	k5eAaPmNgMnP	zbavit
svých	svůj	k3xOyFgFnPc2	svůj
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
z	z	k7c2	z
armády	armáda	k1gFnSc2	armáda
odstranit	odstranit	k5eAaPmF	odstranit
i	i	k9	i
zbytek	zbytek	k1gInSc4	zbytek
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
staré	starý	k2eAgFnSc2d1	stará
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
Hitler	Hitler	k1gMnSc1	Hitler
zcela	zcela	k6eAd1	zcela
ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
německou	německý	k2eAgFnSc4d1	německá
brannou	branný	k2eAgFnSc4d1	Branná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
i	i	k9	i
nadále	nadále	k6eAd1	nadále
objevovaly	objevovat	k5eAaImAgFnP	objevovat
opoziční	opoziční	k2eAgFnPc1d1	opoziční
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
opakovaně	opakovaně	k6eAd1	opakovaně
snažily	snažit	k5eAaImAgFnP	snažit
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
odstranit	odstranit	k5eAaPmF	odstranit
–	–	k?	–
např.	např.	kA	např.
skupina	skupina	k1gFnSc1	skupina
spiklenců	spiklenec	k1gMnPc2	spiklenec
kolem	kolem	k7c2	kolem
náčelníka	náčelník	k1gMnSc2	náčelník
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
Ludwiga	Ludwiga	k1gFnSc1	Ludwiga
Becka	Becka	k1gFnSc1	Becka
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
kolem	kolem	k7c2	kolem
admirála	admirál	k1gMnSc2	admirál
Wilhelma	Wilhelm	k1gMnSc2	Wilhelm
Canarise	Canarise	k1gFnSc2	Canarise
a	a	k8xC	a
generálmajora	generálmajor	k1gMnSc2	generálmajor
Hanse	Hans	k1gMnSc2	Hans
Ostera	Ostera	k1gFnSc1	Ostera
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
atentátníků	atentátník	k1gMnPc2	atentátník
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
generálmajorem	generálmajor	k1gMnSc7	generálmajor
Henningem	Henning	k1gInSc7	Henning
von	von	k1gInSc1	von
Tresckow	Tresckow	k1gFnPc2	Tresckow
a	a	k8xC	a
atentátníci	atentátník	k1gMnPc1	atentátník
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Postoj	postoj	k1gInSc1	postoj
nacistů	nacista	k1gMnPc2	nacista
k	k	k7c3	k
církvím	církev	k1gFnPc3	církev
nebyl	být	k5eNaImAgInS	být
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nacistů	nacista	k1gMnPc2	nacista
převažovalo	převažovat	k5eAaImAgNnS	převažovat
spíš	spíš	k9	spíš
hluboké	hluboký	k2eAgNnSc1d1	hluboké
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
tvrdé	tvrdý	k2eAgNnSc4d1	tvrdé
pronásledování	pronásledování	k1gNnSc4	pronásledování
.	.	kIx.	.
</s>
<s>
Německu	Německo	k1gNnSc3	Německo
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
hlásilo	hlásit	k5eAaImAgNnS	hlásit
68	[number]	k4	68
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
ke	k	k7c3	k
křesťanské	křesťanský	k2eAgFnSc3d1	křesťanská
víře	víra	k1gFnSc3	víra
(	(	kIx(	(
<g/>
62,7	[number]	k4	62,7
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
protestantismu	protestantismus	k1gInSc3	protestantismus
a	a	k8xC	a
32,4	[number]	k4	32,4
<g/>
%	%	kIx~	%
ke	k	k7c3	k
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Protestanti	protestant	k1gMnPc1	protestant
i	i	k8xC	i
katolíci	katolík	k1gMnPc1	katolík
zpočátku	zpočátku	k6eAd1	zpočátku
vesměs	vesměs	k6eAd1	vesměs
uvítali	uvítat	k5eAaPmAgMnP	uvítat
nástup	nástup	k1gInSc4	nástup
nacismu	nacismus	k1gInSc2	nacismus
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
od	od	k7c2	od
nacismu	nacismus	k1gInSc2	nacismus
očekávali	očekávat	k5eAaImAgMnP	očekávat
obnovu	obnova	k1gFnSc4	obnova
starých	starý	k2eAgFnPc2d1	stará
tradičních	tradiční	k2eAgFnPc2d1	tradiční
hodnot	hodnota	k1gFnPc2	hodnota
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
za	za	k7c2	za
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
upadaly	upadat	k5eAaImAgFnP	upadat
.	.	kIx.	.
</s>
<s>
Vysoce	vysoce	k6eAd1	vysoce
postaveným	postavený	k2eAgMnSc7d1	postavený
duchovním	duchovní	k1gMnSc7	duchovní
imponovaly	imponovat	k5eAaImAgInP	imponovat
sociální	sociální	k2eAgInPc1d1	sociální
a	a	k8xC	a
politické	politický	k2eAgInPc1d1	politický
cíle	cíl	k1gInPc1	cíl
nacistů	nacista	k1gMnPc2	nacista
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
pastýřský	pastýřský	k2eAgInSc1d1	pastýřský
list	list	k1gInSc1	list
bavorských	bavorský	k2eAgMnPc2d1	bavorský
biskupů	biskup	k1gMnPc2	biskup
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
záchraně	záchrana	k1gFnSc6	záchrana
Německa	Německo	k1gNnSc2	Německo
před	před	k7c7	před
bolševismem	bolševismus	k1gInSc7	bolševismus
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
Německu	Německo	k1gNnSc6	Německo
stanovena	stanoven	k2eAgFnSc1d1	stanovena
jasná	jasný	k2eAgFnSc1d1	jasná
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
záležitostmi	záležitost	k1gFnPc7	záležitost
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
nemá	mít	k5eNaImIp3nS	mít
plést	plést	k5eAaImF	plést
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
záležitostmi	záležitost	k1gFnPc7	záležitost
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
nemají	mít	k5eNaImIp3nP	mít
plést	plést	k5eAaImF	plést
církve	církev	k1gFnPc4	církev
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1933	[number]	k4	1933
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
konkordát	konkordát	k1gInSc1	konkordát
se	se	k3xPyFc4	se
Svatým	svatý	k2eAgInSc7d1	svatý
stolcem	stolec	k1gInSc7	stolec
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
uzavření	uzavření	k1gNnSc4	uzavření
konkordátu	konkordát	k1gInSc2	konkordát
postupovali	postupovat	k5eAaImAgMnP	postupovat
nacisté	nacista	k1gMnPc1	nacista
vůči	vůči	k7c3	vůči
katolické	katolický	k2eAgFnSc3d1	katolická
církvi	církev	k1gFnSc3	církev
tvrdě	tvrdě	k6eAd1	tvrdě
–	–	k?	–
katolický	katolický	k2eAgInSc4d1	katolický
tisk	tisk	k1gInSc4	tisk
byl	být	k5eAaImAgInS	být
cenzurován	cenzurován	k2eAgMnSc1d1	cenzurován
<g/>
,	,	kIx,	,
úřady	úřad	k1gInPc1	úřad
měly	mít	k5eAaImAgInP	mít
právo	právo	k1gNnSc4	právo
nepovolit	povolit	k5eNaPmF	povolit
procesí	procesí	k1gNnSc4	procesí
nebo	nebo	k8xC	nebo
uzavřít	uzavřít	k5eAaPmF	uzavřít
klášterní	klášterní	k2eAgFnPc4d1	klášterní
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Pius	Pius	k1gMnSc1	Pius
XI	XI	kA	XI
<g/>
.	.	kIx.	.
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
zareagoval	zareagovat	k5eAaPmAgInS	zareagovat
encyklikou	encyklika	k1gFnSc7	encyklika
S	s	k7c7	s
palčivou	palčivý	k2eAgFnSc7d1	palčivá
starostí	starost	k1gFnSc7	starost
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
šíření	šířený	k2eAgMnPc1d1	šířený
nacisté	nacista	k1gMnPc1	nacista
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
zakázali	zakázat	k5eAaPmAgMnP	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Encyklika	encyklika	k1gFnSc1	encyklika
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
jen	jen	k6eAd1	jen
ojedinělým	ojedinělý	k2eAgInSc7d1	ojedinělý
případem	případ	k1gInSc7	případ
vzdoru	vzdor	k1gInSc2	vzdor
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
Vatikánem	Vatikán	k1gInSc7	Vatikán
a	a	k8xC	a
Třetí	třetí	k4xOgFnSc7	třetí
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kritiky	kritika	k1gFnSc2	kritika
režimu	režim	k1gInSc2	režim
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
nadále	nadále	k6eAd1	nadále
pouštěli	pouštět	k5eAaImAgMnP	pouštět
někteří	některý	k3yIgMnPc1	některý
domácí	domácí	k2eAgMnPc1d1	domácí
duchovní	duchovní	k1gMnPc1	duchovní
–	–	k?	–
např.	např.	kA	např.
biskup	biskup	k1gMnSc1	biskup
Clemens	Clemensa	k1gFnPc2	Clemensa
August	August	k1gMnSc1	August
hrabě	hrabě	k1gMnSc1	hrabě
von	von	k1gInSc4	von
Galen	Galen	k2eAgInSc4d1	Galen
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
opozice	opozice	k1gFnSc2	opozice
vůči	vůči	k7c3	vůči
nacismu	nacismus	k1gInSc3	nacismus
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
protestantská	protestantský	k2eAgFnSc1d1	protestantská
Vyznávající	vyznávající	k2eAgFnSc1d1	vyznávající
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
Ochranné	ochranný	k2eAgNnSc1d1	ochranné
sdružení	sdružení	k1gNnSc1	sdružení
farářů	farář	k1gMnPc2	farář
a	a	k8xC	a
jmenovitě	jmenovitě	k6eAd1	jmenovitě
pak	pak	k6eAd1	pak
např.	např.	kA	např.
evangeličtí	evangelický	k2eAgMnPc1d1	evangelický
duchovní	duchovní	k1gMnPc1	duchovní
Dietrich	Dietrich	k1gMnSc1	Dietrich
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
nebo	nebo	k8xC	nebo
Friedrich	Friedrich	k1gMnSc1	Friedrich
Justus	Justus	k1gMnSc1	Justus
Perels	Perels	k1gInSc4	Perels
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
protinacistického	protinacistický	k2eAgInSc2d1	protinacistický
odporu	odpor	k1gInSc2	odpor
v	v	k7c6	v
církvích	církev	k1gFnPc6	církev
však	však	k9	však
byly	být	k5eAaImAgInP	být
izolované	izolovaný	k2eAgInPc1d1	izolovaný
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
jich	on	k3xPp3gMnPc2	on
mnoho	mnoho	k4c4	mnoho
.	.	kIx.	.
</s>
<s>
Církve	církev	k1gFnPc1	církev
vystupovaly	vystupovat	k5eAaImAgFnP	vystupovat
otevřeně	otevřeně	k6eAd1	otevřeně
proti	proti	k7c3	proti
nacismu	nacismus	k1gInSc2	nacismus
jen	jen	k6eAd1	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ohrožoval	ohrožovat	k5eAaImAgInS	ohrožovat
jejich	jejich	k3xOp3gFnSc4	jejich
teologickou	teologický	k2eAgFnSc4d1	teologická
nebo	nebo	k8xC	nebo
morální	morální	k2eAgFnSc4d1	morální
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
kladla	klást	k5eAaImAgFnS	klást
rozhodný	rozhodný	k2eAgInSc4d1	rozhodný
a	a	k8xC	a
vytrvalý	vytrvalý	k2eAgInSc4d1	vytrvalý
odpor	odpor	k1gInSc4	odpor
nacismu	nacismus	k1gInSc2	nacismus
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
mimo	mimo	k7c4	mimo
zákon	zákon	k1gInSc4	zákon
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
nacisté	nacista	k1gMnPc1	nacista
převzali	převzít	k5eAaPmAgMnP	převzít
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
konzervativně	konzervativně	k6eAd1	konzervativně
smýšlejících	smýšlející	k2eAgMnPc2d1	smýšlející
soudců	soudce	k1gMnPc2	soudce
a	a	k8xC	a
právníků	právník	k1gMnPc2	právník
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neztotožnila	ztotožnit	k5eNaPmAgFnS	ztotožnit
s	s	k7c7	s
liberálními	liberální	k2eAgFnPc7d1	liberální
myšlenkami	myšlenka	k1gFnPc7	myšlenka
Výmarské	výmarský	k2eAgFnSc2d1	Výmarská
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
v	v	k7c6	v
nastolení	nastolení	k1gNnSc6	nastolení
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
spatřovali	spatřovat	k5eAaImAgMnP	spatřovat
návrat	návrat	k1gInSc4	návrat
ke	k	k7c3	k
starým	starý	k2eAgFnPc3d1	stará
hodnotám	hodnota	k1gFnPc3	hodnota
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
autoritářského	autoritářský	k2eAgNnSc2d1	autoritářské
císařství	císařství	k1gNnSc2	císařství
.	.	kIx.	.
</s>
<s>
Justice	justice	k1gFnSc1	justice
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
bezvýhradně	bezvýhradně	k6eAd1	bezvýhradně
podvolila	podvolit	k5eAaPmAgFnS	podvolit
Hitlerově	Hitlerův	k2eAgFnSc3d1	Hitlerova
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
důležitým	důležitý	k2eAgInSc7d1	důležitý
nástrojem	nástroj	k1gInSc7	nástroj
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
občanském	občanský	k2eAgInSc6d1	občanský
zákoníku	zákoník	k1gInSc6	zákoník
provedli	provést	k5eAaPmAgMnP	provést
nacisté	nacista	k1gMnPc1	nacista
jen	jen	k9	jen
minimum	minimum	k1gNnSc4	minimum
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
trestní	trestní	k2eAgNnSc4d1	trestní
právo	právo	k1gNnSc4	právo
zneužili	zneužít	k5eAaPmAgMnP	zneužít
k	k	k7c3	k
vlastním	vlastní	k2eAgInPc3d1	vlastní
cílům	cíl	k1gInPc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Destrukce	destrukce	k1gFnSc1	destrukce
právního	právní	k2eAgInSc2d1	právní
systému	systém	k1gInSc2	systém
nastala	nastat	k5eAaPmAgFnS	nastat
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
Říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
Nařízením	nařízení	k1gNnSc7	nařízení
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
národa	národ	k1gInSc2	národ
a	a	k8xC	a
státu	stát	k1gInSc2	stát
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
zneužito	zneužít	k5eAaPmNgNnS	zneužít
k	k	k7c3	k
neomezenému	omezený	k2eNgNnSc3d1	neomezené
zatýkání	zatýkání	k1gNnSc3	zatýkání
politických	politický	k2eAgMnPc2d1	politický
oponentů	oponent	k1gMnPc2	oponent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
268	[number]	k4	268
procesů	proces	k1gInPc2	proces
velezrady	velezrada	k1gFnSc2	velezrada
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
11	[number]	k4	11
000	[number]	k4	000
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
síť	síť	k1gFnSc1	síť
mimořádných	mimořádný	k2eAgInPc2d1	mimořádný
soudů	soud	k1gInPc2	soud
bez	bez	k7c2	bez
poroty	porota	k1gFnSc2	porota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
instituce	instituce	k1gFnSc1	instituce
Lidového	lidový	k2eAgInSc2d1	lidový
soudu	soud	k1gInSc2	soud
(	(	kIx(	(
<g/>
Volksgerichtshof	Volksgerichtshof	k1gInSc1	Volksgerichtshof
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
projednávat	projednávat	k5eAaImF	projednávat
případy	případ	k1gInPc4	případ
velezrady	velezrada	k1gFnSc2	velezrada
<g/>
.	.	kIx.	.
</s>
<s>
Hitlerova	Hitlerův	k2eAgNnPc1d1	Hitlerovo
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
stávala	stávat	k5eAaImAgFnS	stávat
výnosem	výnos	k1gInSc7	výnos
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
soudci	soudce	k1gMnPc1	soudce
neměli	mít	k5eNaImAgMnP	mít
právo	právo	k1gNnSc4	právo
tato	tento	k3xDgNnPc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
zpochybňovat	zpochybňovat	k5eAaImF	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
zákonem	zákon	k1gInSc7	zákon
pak	pak	k6eAd1	pak
stálo	stát	k5eAaImAgNnS	stát
např.	např.	kA	např.
gestapo	gestapo	k1gNnSc1	gestapo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
jednalo	jednat	k5eAaImAgNnS	jednat
rovněž	rovněž	k9	rovněž
dle	dle	k7c2	dle
Hitlerovy	Hitlerův	k2eAgFnSc2d1	Hitlerova
vůle	vůle	k1gFnSc2	vůle
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Velkogermánská	velkogermánský	k2eAgFnSc1d1	velkogermánský
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
před	před	k7c7	před
uchopením	uchopení	k1gNnSc7	uchopení
moci	moc	k1gFnSc2	moc
projevovali	projevovat	k5eAaImAgMnP	projevovat
nacisté	nacista	k1gMnPc1	nacista
nenávist	nenávist	k1gFnSc4	nenávist
k	k	k7c3	k
některým	některý	k3yIgNnPc3	některý
etnikům	etnikum	k1gNnPc3	etnikum
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vůči	vůči	k7c3	vůči
Lužickým	lužický	k2eAgMnPc3d1	lužický
Srbům	Srb	k1gMnPc3	Srb
<g/>
,	,	kIx,	,
Židům	Žid	k1gMnPc3	Žid
či	či	k8xC	či
Romům	Rom	k1gMnPc3	Rom
<g/>
.	.	kIx.	.
</s>
<s>
Diskriminace	diskriminace	k1gFnPc1	diskriminace
a	a	k8xC	a
perzekuce	perzekuce	k1gFnPc1	perzekuce
Židů	Žid	k1gMnPc2	Žid
byly	být	k5eAaImAgFnP	být
zahájeny	zahájit	k5eAaPmNgFnP	zahájit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1933	[number]	k4	1933
vypuzením	vypuzení	k1gNnSc7	vypuzení
židovských	židovská	k1gFnPc2	židovská
úředníků	úředník	k1gMnPc2	úředník
ze	z	k7c2	z
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1935	[number]	k4	1935
schválil	schválit	k5eAaPmAgInS	schválit
říšský	říšský	k2eAgInSc1d1	říšský
sněm	sněm	k1gInSc1	sněm
norimberské	norimberský	k2eAgInPc4d1	norimberský
rasové	rasový	k2eAgInPc4d1	rasový
zákony	zákon	k1gInPc4	zákon
namířené	namířený	k2eAgInPc4d1	namířený
proti	proti	k7c3	proti
Židům	Žid	k1gMnPc3	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
byli	být	k5eAaImAgMnP	být
zbaveni	zbavit	k5eAaPmNgMnP	zbavit
německého	německý	k2eAgNnSc2d1	německé
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
vstupovat	vstupovat	k5eAaImF	vstupovat
do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Norimberskými	norimberský	k2eAgInPc7d1	norimberský
zákony	zákon	k1gInPc7	zákon
bylo	být	k5eAaImAgNnS	být
postiženo	postižen	k2eAgNnSc1d1	postiženo
kolem	kolem	k7c2	kolem
500	[number]	k4	500
tisíc	tisíc	k4xCgInSc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1938	[number]	k4	1938
zinscenovali	zinscenovat	k5eAaPmAgMnP	zinscenovat
nacisté	nacista	k1gMnPc1	nacista
"	"	kIx"	"
<g/>
křišťálovou	křišťálový	k2eAgFnSc4d1	Křišťálová
noc	noc	k1gFnSc4	noc
<g/>
"	"	kIx"	"
–	–	k?	–
pogrom	pogrom	k1gInSc1	pogrom
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgMnSc2	jenž
shořelo	shořet	k5eAaPmAgNnS	shořet
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
synagog	synagoga	k1gFnPc2	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
SS	SS	kA	SS
přitom	přitom	k6eAd1	přitom
před	před	k7c7	před
zraky	zrak	k1gInPc7	zrak
nečinné	činný	k2eNgFnSc2d1	nečinná
policie	policie	k1gFnSc2	policie
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
mnoho	mnoho	k4c4	mnoho
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgNnPc1d1	ekonomické
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
např.	např.	kA	např.
výstavba	výstavba	k1gFnSc1	výstavba
dálniční	dálniční	k2eAgFnSc2d1	dálniční
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
Autobahnen	Autobahnen	k1gInSc1	Autobahnen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
připravena	připravit	k5eAaPmNgFnS	připravit
již	již	k6eAd1	již
předchozími	předchozí	k2eAgFnPc7d1	předchozí
vládami	vláda	k1gFnPc7	vláda
<g/>
,	,	kIx,	,
odstranila	odstranit	k5eAaPmAgFnS	odstranit
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečná	k1gFnPc1	válečná
přípravy	příprava	k1gFnSc2	příprava
zabezpečily	zabezpečit	k5eAaPmAgFnP	zabezpečit
hospodářské	hospodářský	k2eAgNnSc4d1	hospodářské
oživení	oživení	k1gNnSc4	oživení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
bylo	být	k5eAaImAgNnS	být
Sársko	Sársko	k1gNnSc1	Sársko
opět	opět	k6eAd1	opět
integrováno	integrovat	k5eAaBmNgNnS	integrovat
do	do	k7c2	do
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
zavedena	zaveden	k2eAgFnSc1d1	zavedena
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
následně	následně	k6eAd1	následně
obnovil	obnovit	k5eAaPmAgMnS	obnovit
německou	německý	k2eAgFnSc4d1	německá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
flagrantně	flagrantně	k6eAd1	flagrantně
porušena	porušit	k5eAaPmNgFnS	porušit
Versailleská	versailleský	k2eAgFnSc1d1	Versailleská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
se	se	k3xPyFc4	se
omezily	omezit	k5eAaPmAgFnP	omezit
na	na	k7c4	na
protesty	protest	k1gInPc4	protest
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
o	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
wehrmacht	wehrmacht	k1gFnSc7	wehrmacht
remilitarizoval	remilitarizovat	k5eAaBmAgMnS	remilitarizovat
Porýní	Porýní	k1gNnSc4	Porýní
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc2d1	západní
mocnosti	mocnost	k1gFnSc2	mocnost
proti	proti	k7c3	proti
tomu	ten	k3xDgMnSc3	ten
nijak	nijak	k6eAd1	nijak
nezakročily	zakročit	k5eNaPmAgInP	zakročit
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
činem	čin	k1gInSc7	čin
Hitler	Hitler	k1gMnSc1	Hitler
výrazně	výrazně	k6eAd1	výrazně
posílil	posílit	k5eAaPmAgMnS	posílit
svoje	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1936	[number]	k4	1936
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgInP	ukázat
být	být	k5eAaImF	být
dalším	další	k2eAgMnSc7d1	další
velkolepým	velkolepý	k2eAgInSc7d1	velkolepý
propagandistickým	propagandistický	k2eAgInSc7d1	propagandistický
úspěchem	úspěch	k1gInSc7	úspěch
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ustavení	ustavení	k1gNnSc6	ustavení
"	"	kIx"	"
<g/>
osy	osa	k1gFnSc2	osa
Berlín-Řím	Berlín-Ří	k1gMnPc3	Berlín-Ří
<g/>
"	"	kIx"	"
s	s	k7c7	s
Mussolinim	Mussolini	k1gNnSc7	Mussolini
a	a	k8xC	a
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
paktu	pakt	k1gInSc2	pakt
proti	proti	k7c3	proti
Kominterně	Kominterna	k1gFnSc3	Kominterna
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
cítil	cítit	k5eAaImAgMnS	cítit
být	být	k5eAaImF	být
dost	dost	k6eAd1	dost
silný	silný	k2eAgInSc1d1	silný
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přešel	přejít	k5eAaPmAgMnS	přejít
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
do	do	k7c2	do
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1938	[number]	k4	1938
wehrmacht	wehrmacht	k1gFnSc2	wehrmacht
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Hitler	Hitler	k1gMnSc1	Hitler
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
bouřlivě	bouřlivě	k6eAd1	bouřlivě
pozdravován	pozdravovat	k5eAaImNgInS	pozdravovat
nadšeným	nadšený	k2eAgInSc7d1	nadšený
davem	dav	k1gInSc7	dav
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
se	s	k7c7	s
99	[number]	k4	99
%	%	kIx~	%
Rakušanů	Rakušan	k1gMnPc2	Rakušan
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
(	(	kIx(	(
<g/>
anšlus	anšlus	k1gInSc1	anšlus
<g/>
)	)	kIx)	)
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Hitlerovi	Hitler	k1gMnSc3	Hitler
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
podařilo	podařit	k5eAaPmAgNnS	podařit
naplnit	naplnit	k5eAaPmF	naplnit
ideu	idea	k1gFnSc4	idea
vytvoření	vytvoření	k1gNnSc2	vytvoření
Velkého	velký	k2eAgNnSc2d1	velké
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Bismarck	Bismarck	k1gMnSc1	Bismarck
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
zavrhl	zavrhnout	k5eAaPmAgMnS	zavrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
anexe	anexe	k1gFnSc1	anexe
Rakouska	Rakousko	k1gNnSc2	Rakousko
odporovala	odporovat	k5eAaImAgFnS	odporovat
Versailleské	versailleský	k2eAgFnSc3d1	Versailleská
smlouvě	smlouva	k1gFnSc3	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
výslovně	výslovně	k6eAd1	výslovně
zakazovala	zakazovat	k5eAaImAgFnS	zakazovat
sjednocení	sjednocení	k1gNnSc4	sjednocení
Rakouska	Rakousko	k1gNnSc2	Rakousko
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnPc1d1	západní
mocnosti	mocnost	k1gFnPc1	mocnost
opět	opět	k6eAd1	opět
nezasáhly	zasáhnout	k5eNaPmAgFnP	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Rakousku	Rakousko	k1gNnSc6	Rakousko
obrátil	obrátit	k5eAaPmAgMnS	obrátit
Hitler	Hitler	k1gMnSc1	Hitler
svůj	svůj	k3xOyFgInSc4	svůj
zrak	zrak	k1gInSc4	zrak
na	na	k7c4	na
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
3,3	[number]	k4	3,3
<g/>
milionová	milionový	k2eAgFnSc1d1	milionová
menšina	menšina	k1gFnSc1	menšina
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
dožadovala	dožadovat	k5eAaImAgFnS	dožadovat
autonomie	autonomie	k1gFnSc1	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1938	[number]	k4	1938
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
Benito	Benit	k2eAgNnSc1d1	Benito
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
<g/>
,	,	kIx,	,
Neville	Neville	k1gFnSc1	Neville
Chamberlain	Chamberlaina	k1gFnPc2	Chamberlaina
a	a	k8xC	a
Edouard	Edouarda	k1gFnPc2	Edouarda
Daladier	Daladiero	k1gNnPc2	Daladiero
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
na	na	k7c4	na
odstoupení	odstoupení	k1gNnSc4	odstoupení
československého	československý	k2eAgNnSc2d1	Československé
pohraničí	pohraničí	k1gNnSc2	pohraničí
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
Hitler	Hitler	k1gMnSc1	Hitler
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškeré	veškerý	k3xTgInPc1	veškerý
německé	německý	k2eAgInPc1d1	německý
územní	územní	k2eAgInPc1d1	územní
nároky	nárok	k1gInPc1	nárok
byly	být	k5eAaImAgInP	být
uspokojeny	uspokojen	k2eAgInPc4d1	uspokojen
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jen	jen	k9	jen
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
využil	využít	k5eAaPmAgInS	využít
rozporů	rozpor	k1gInPc2	rozpor
mezi	mezi	k7c4	mezi
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Slováky	Slovák	k1gMnPc4	Slovák
jako	jako	k8xC	jako
záminky	záminka	k1gFnPc4	záminka
k	k	k7c3	k
obsazení	obsazení	k1gNnSc3	obsazení
zbytku	zbytek	k1gInSc2	zbytek
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
přeměně	přeměna	k1gFnSc3	přeměna
v	v	k7c4	v
protektorát	protektorát	k1gInSc4	protektorát
Čechy	Čechy	k1gFnPc4	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
měsíci	měsíc	k1gInSc6	měsíc
obsadila	obsadit	k5eAaPmAgFnS	obsadit
německá	německý	k2eAgFnSc1d1	německá
vojska	vojsko	k1gNnPc4	vojsko
také	také	k9	také
přístav	přístav	k1gInSc1	přístav
Memel	Memela	k1gFnPc2	Memela
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnSc1d1	západní
politika	politika	k1gFnSc1	politika
appeasementu	appeasement	k1gInSc2	appeasement
tak	tak	k6eAd1	tak
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
německým	německý	k2eAgNnSc7d1	německé
přepadením	přepadení	k1gNnSc7	přepadení
Polska	Polsko	k1gNnSc2	Polsko
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
vyhlásily	vyhlásit	k5eAaPmAgInP	vyhlásit
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
Německu	Německo	k1gNnSc6	Německo
válku	válek	k1gInSc3	válek
<g/>
.	.	kIx.	.
</s>
<s>
Poláci	Polák	k1gMnPc1	Polák
se	se	k3xPyFc4	se
statečně	statečně	k6eAd1	statečně
bránili	bránit	k5eAaImAgMnP	bránit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
proti	proti	k7c3	proti
německé	německý	k2eAgFnSc3d1	německá
bleskové	bleskový	k2eAgFnSc3d1	blesková
válce	válka	k1gFnSc3	válka
se	se	k3xPyFc4	se
ukázali	ukázat	k5eAaPmAgMnP	ukázat
zcela	zcela	k6eAd1	zcela
bezmocní	bezmocný	k2eAgMnPc1d1	bezmocný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pouhém	pouhý	k2eAgInSc6d1	pouhý
měsíci	měsíc	k1gInSc6	měsíc
válčení	válčení	k1gNnSc2	válčení
bylo	být	k5eAaImAgNnS	být
Polsko	Polsko	k1gNnSc1	Polsko
na	na	k7c6	na
kolenou	koleno	k1gNnPc6	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc4d1	východní
polovinu	polovina	k1gFnSc4	polovina
země	zem	k1gFnSc2	zem
zabral	zabrat	k5eAaPmAgMnS	zabrat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
paktem	pakt	k1gInSc7	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
oběťmi	oběť	k1gFnPc7	oběť
německé	německý	k2eAgFnSc2d1	německá
agrese	agrese	k1gFnSc2	agrese
se	se	k3xPyFc4	se
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1940	[number]	k4	1940
staly	stát	k5eAaPmAgFnP	stát
Dánsko	Dánsko	k1gNnSc4	Dánsko
a	a	k8xC	a
Norsko	Norsko	k1gNnSc4	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
zahájila	zahájit	k5eAaPmAgFnS	zahájit
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
napadením	napadení	k1gNnSc7	napadení
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
Nizozemska	Nizozemsko	k1gNnSc2	Nizozemsko
a	a	k8xC	a
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
válku	válek	k1gInSc2	válek
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Německým	německý	k2eAgFnPc3d1	německá
pancéřovým	pancéřový	k2eAgFnPc3d1	pancéřová
divizím	divize	k1gFnPc3	divize
se	se	k3xPyFc4	se
během	běh	k1gInSc7	běh
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
podařilo	podařit	k5eAaPmAgNnS	podařit
rozetnout	rozetnout	k5eAaPmF	rozetnout
spojenecké	spojenecký	k2eAgNnSc1d1	spojenecké
vojsko	vojsko	k1gNnSc1	vojsko
ve	v	k7c6	v
dví	dví	k1xP	dví
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgInSc1d1	britský
expediční	expediční	k2eAgInSc1d1	expediční
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
obklíčený	obklíčený	k2eAgInSc1d1	obklíčený
u	u	k7c2	u
Dunkerque	Dunkerqu	k1gInSc2	Dunkerqu
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
evakuován	evakuován	k2eAgMnSc1d1	evakuován
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
wehrmacht	wehrmacht	k1gInSc1	wehrmacht
triumfálně	triumfálně	k6eAd1	triumfálně
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
týdnu	týden	k1gInSc6	týden
bojů	boj	k1gInPc2	boj
Francie	Francie	k1gFnSc2	Francie
kapitulovala	kapitulovat	k5eAaBmAgFnS	kapitulovat
a	a	k8xC	a
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
příměří	příměří	k1gNnSc4	příměří
u	u	k7c2	u
Compiégne	Compiégn	k1gInSc5	Compiégn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
letecké	letecký	k2eAgFnSc6d1	letecká
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c6	o
Británii	Británie	k1gFnSc6	Británie
musel	muset	k5eAaImAgMnS	muset
však	však	k9	však
Hitler	Hitler	k1gMnSc1	Hitler
strpět	strpět	k5eAaPmF	strpět
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1941	[number]	k4	1941
okupovala	okupovat	k5eAaBmAgFnS	okupovat
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
a	a	k8xC	a
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
také	také	k9	také
Jugoslávii	Jugoslávie	k1gFnSc4	Jugoslávie
a	a	k8xC	a
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Značných	značný	k2eAgInPc2d1	značný
úspěchů	úspěch	k1gInPc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
rovněž	rovněž	k9	rovněž
Afrikakorps	Afrikakorps	k1gInSc1	Afrikakorps
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Erwina	Erwin	k1gMnSc2	Erwin
Rommela	Rommel	k1gMnSc2	Rommel
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
Německo	Německo	k1gNnSc1	Německo
na	na	k7c4	na
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
tohoto	tento	k3xDgNnSc2	tento
mohutného	mohutný	k2eAgNnSc2d1	mohutné
tažení	tažení	k1gNnSc2	tažení
padlo	padnout	k5eAaPmAgNnS	padnout
do	do	k7c2	do
německých	německý	k2eAgFnPc2d1	německá
rukou	ruka	k1gFnPc2	ruka
celé	celý	k2eAgNnSc4d1	celé
západní	západní	k2eAgNnSc4d1	západní
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
a	a	k8xC	a
Pobaltí	Pobaltí	k1gNnSc1	Pobaltí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1941	[number]	k4	1941
<g/>
/	/	kIx~	/
<g/>
1942	[number]	k4	1942
ale	ale	k8xC	ale
německý	německý	k2eAgInSc1d1	německý
postup	postup	k1gInSc1	postup
zamrzl	zamrznout	k5eAaPmAgInS	zamrznout
před	před	k7c7	před
Moskvou	Moskva	k1gFnSc7	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1941	[number]	k4	1941
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
USA	USA	kA	USA
a	a	k8xC	a
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
osobně	osobně	k6eAd1	osobně
schválil	schválit	k5eAaPmAgMnS	schválit
přípravu	příprava	k1gFnSc4	příprava
sabotážních	sabotážní	k2eAgInPc2d1	sabotážní
a	a	k8xC	a
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
provedeny	provést	k5eAaPmNgFnP	provést
na	na	k7c4	na
území	území	k1gNnSc4	území
USA	USA	kA	USA
(	(	kIx(	(
<g/>
Operace	operace	k1gFnSc1	operace
Pastorius	Pastorius	k1gMnSc1	Pastorius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
nové	nový	k2eAgFnSc2d1	nová
německé	německý	k2eAgFnSc2d1	německá
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
na	na	k7c6	na
východě	východ	k1gInSc6	východ
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1942	[number]	k4	1942
pronikl	proniknout	k5eAaPmAgInS	proniknout
wehrmacht	wehrmacht	k1gInSc4	wehrmacht
až	až	k9	až
na	na	k7c4	na
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
a	a	k8xC	a
k	k	k7c3	k
Volze	Volha	k1gFnSc3	Volha
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
však	však	k9	však
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
1942	[number]	k4	1942
<g/>
/	/	kIx~	/
<g/>
1943	[number]	k4	1943
zničující	zničující	k2eAgFnSc4d1	zničující
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Stalingradu	Stalingrad	k1gInSc2	Stalingrad
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
bod	bod	k1gInSc4	bod
obratu	obrat	k1gInSc2	obrat
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byla	být	k5eAaImAgFnS	být
nacisty	nacista	k1gMnPc4	nacista
uvedena	uveden	k2eAgFnSc1d1	uvedena
v	v	k7c4	v
chod	chod	k1gInSc4	chod
genocida	genocida	k1gFnSc1	genocida
především	především	k9	především
židovského	židovský	k2eAgNnSc2d1	Židovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
holokaust	holokaust	k1gInSc1	holokaust
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1942	[number]	k4	1942
uspořádaly	uspořádat	k5eAaPmAgFnP	uspořádat
nacistické	nacistický	k2eAgFnPc1d1	nacistická
špičky	špička	k1gFnPc1	špička
konferenci	konference	k1gFnSc4	konference
ve	v	k7c6	v
Wannsee	Wannsee	k1gFnSc6	Wannsee
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
konečném	konečný	k2eAgNnSc6d1	konečné
řešení	řešení	k1gNnSc6	řešení
židovské	židovský	k2eAgFnSc2d1	židovská
otázky	otázka	k1gFnSc2	otázka
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Endlösung	Endlösung	k1gInSc1	Endlösung
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
okupované	okupovaný	k2eAgFnSc2d1	okupovaná
Evropy	Evropa	k1gFnSc2	Evropa
byli	být	k5eAaImAgMnP	být
odvezeni	odvézt	k5eAaPmNgMnP	odvézt
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
a	a	k8xC	a
vyhlazovacích	vyhlazovací	k2eAgInPc2d1	vyhlazovací
táborů	tábor	k1gInPc2	tábor
v	v	k7c6	v
obsazeném	obsazený	k2eAgNnSc6d1	obsazené
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
<g/>
,	,	kIx,	,
Treblince	Treblinka	k1gFnSc6	Treblinka
<g/>
,	,	kIx,	,
Majdanku	Majdanka	k1gFnSc4	Majdanka
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
vyvražděno	vyvraždit	k5eAaPmNgNnS	vyvraždit
kolem	kolem	k7c2	kolem
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
evropských	evropský	k2eAgMnPc2d1	evropský
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobný	podobný	k2eAgInSc4d1	podobný
úděl	úděl	k1gInSc4	úděl
chystali	chystat	k5eAaImAgMnP	chystat
nacisté	nacista	k1gMnPc1	nacista
také	také	k6eAd1	také
dalším	další	k2eAgFnPc3d1	další
etnickým	etnický	k2eAgFnPc3d1	etnická
a	a	k8xC	a
národnostním	národnostní	k2eAgFnPc3d1	národnostní
skupinám	skupina	k1gFnPc3	skupina
označeným	označený	k2eAgFnPc3d1	označená
za	za	k7c7	za
podřadné	podřadný	k2eAgMnPc4d1	podřadný
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Slovanům	Slovan	k1gInPc3	Slovan
a	a	k8xC	a
Romům	Rom	k1gMnPc3	Rom
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
uvolněné	uvolněný	k2eAgNnSc4d1	uvolněné
teritorium	teritorium	k1gNnSc4	teritorium
na	na	k7c6	na
východě	východ	k1gInSc6	východ
mělo	mít	k5eAaImAgNnS	mít
posloužit	posloužit	k5eAaPmF	posloužit
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
životního	životní	k2eAgInSc2d1	životní
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
Lebensraum	Lebensraum	k1gInSc1	Lebensraum
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
árijskou	árijský	k2eAgFnSc4d1	árijská
rasu	rasa	k1gFnSc4	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Zlikvidovány	zlikvidován	k2eAgFnPc1d1	zlikvidována
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
rovněž	rovněž	k9	rovněž
fyzicky	fyzicky	k6eAd1	fyzicky
nebo	nebo	k8xC	nebo
mentálně	mentálně	k6eAd1	mentálně
postižené	postižený	k2eAgFnPc4d1	postižená
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
u	u	k7c2	u
El-Alamejnu	El-Alamejn	k1gInSc2	El-Alamejn
síly	síla	k1gFnSc2	síla
Osy	osa	k1gFnSc2	osa
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1943	[number]	k4	1943
kapitulovaly	kapitulovat	k5eAaBmAgFnP	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kurska	Kursk	k1gInSc2	Kursk
dobyla	dobýt	k5eAaPmAgFnS	dobýt
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
zpět	zpět	k6eAd1	zpět
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
obsazeného	obsazený	k2eAgNnSc2d1	obsazené
sovětského	sovětský	k2eAgNnSc2d1	sovětské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
se	se	k3xPyFc4	se
anglo-americká	anglomerický	k2eAgNnPc1d1	anglo-americké
vojska	vojsko	k1gNnPc1	vojsko
vylodila	vylodit	k5eAaPmAgNnP	vylodit
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
Spojenci	spojenec	k1gMnSc3	spojenec
osvobodili	osvobodit	k5eAaPmAgMnP	osvobodit
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Sověti	Sovět	k1gMnPc1	Sovět
mezitím	mezitím	k6eAd1	mezitím
pronikli	proniknout	k5eAaPmAgMnP	proniknout
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
a	a	k8xC	a
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1944	[number]	k4	1944
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
Němci	Němec	k1gMnPc1	Němec
pokusili	pokusit	k5eAaPmAgMnP	pokusit
ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
chopit	chopit	k5eAaPmF	chopit
iniciativy	iniciativa	k1gFnPc4	iniciativa
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Ardenách	Ardeny	k1gFnPc6	Ardeny
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
osud	osud	k1gInSc1	osud
Německa	Německo	k1gNnSc2	Německo
zpečetěn	zpečetit	k5eAaPmNgMnS	zpečetit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1945	[number]	k4	1945
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Berlín	Berlín	k1gInSc4	Berlín
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
spáchal	spáchat	k5eAaPmAgMnS	spáchat
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
v	v	k7c6	v
bunkru	bunkr	k1gInSc6	bunkr
pod	pod	k7c7	pod
říšským	říšský	k2eAgNnSc7d1	říšské
kancléřstvím	kancléřství	k1gNnSc7	kancléřství
sebevraždu	sebevražda	k1gFnSc4	sebevražda
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
posádka	posádka	k1gFnSc1	posádka
Berlína	Berlín	k1gInSc2	Berlín
vzdala	vzdát	k5eAaPmAgFnS	vzdát
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
skončila	skončit	k5eAaPmAgFnS	skončit
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
bezpodmínečná	bezpodmínečný	k2eAgFnSc1d1	bezpodmínečná
kapitulace	kapitulace	k1gFnSc1	kapitulace
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Evropy	Evropa	k1gFnSc2	Evropa
ležela	ležet	k5eAaImAgFnS	ležet
v	v	k7c6	v
ruinách	ruina	k1gFnPc6	ruina
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Politická	politický	k2eAgFnSc1d1	politická
a	a	k8xC	a
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
Německa	Německo	k1gNnSc2	Německo
byla	být	k5eAaImAgFnS	být
totálně	totálně	k6eAd1	totálně
zničena	zničit	k5eAaPmNgFnS	zničit
a	a	k8xC	a
z	z	k7c2	z
někdejších	někdejší	k2eAgNnPc2d1	někdejší
německých	německý	k2eAgNnPc2d1	německé
území	území	k1gNnPc2	území
na	na	k7c6	na
východě	východ	k1gInSc6	východ
bylo	být	k5eAaImAgNnS	být
odsunuto	odsunout	k5eAaPmNgNnS	odsunout
kolem	kolem	k7c2	kolem
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Německý	německý	k2eAgInSc4d1	německý
odboj	odboj	k1gInSc4	odboj
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
vůči	vůči	k7c3	vůči
nacismu	nacismus	k1gInSc2	nacismus
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
roztříštěná	roztříštěný	k2eAgFnSc1d1	roztříštěná
a	a	k8xC	a
nejednotná	jednotný	k2eNgFnSc1d1	nejednotná
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgMnPc1d1	aktivní
odpůrci	odpůrce	k1gMnPc1	odpůrce
režimu	režim	k1gInSc2	režim
nebyli	být	k5eNaImAgMnP	být
početní	početní	k2eAgMnPc1d1	početní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zastupovali	zastupovat	k5eAaImAgMnP	zastupovat
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
společnosti	společnost	k1gFnSc2	společnost
–	–	k?	–
patřili	patřit	k5eAaImAgMnP	patřit
sem	sem	k6eAd1	sem
šlechtici	šlechtic	k1gMnPc1	šlechtic
i	i	k8xC	i
dělníci	dělník	k1gMnPc1	dělník
<g/>
,	,	kIx,	,
političtí	politický	k2eAgMnPc1d1	politický
oponenti	oponent	k1gMnPc1	oponent
<g/>
,	,	kIx,	,
církevní	církevní	k2eAgMnPc1d1	církevní
činitelé	činitel	k1gMnPc1	činitel
<g/>
,	,	kIx,	,
generalita	generalita	k1gFnSc1	generalita
<g/>
,	,	kIx,	,
mládež	mládež	k1gFnSc1	mládež
i	i	k8xC	i
státní	státní	k2eAgMnPc1d1	státní
úředníci	úředník	k1gMnPc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Opoziční	opoziční	k2eAgFnPc1d1	opoziční
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
dopouštěly	dopouštět	k5eAaImAgFnP	dopouštět
široké	široký	k2eAgFnPc1d1	široká
škály	škála	k1gFnPc1	škála
činů	čin	k1gInPc2	čin
od	od	k7c2	od
šíření	šíření	k1gNnSc2	šíření
letáků	leták	k1gInPc2	leták
a	a	k8xC	a
sabotáží	sabotáž	k1gFnPc2	sabotáž
přes	přes	k7c4	přes
kázání	kázání	k1gNnSc4	kázání
duchovních	duchovní	k1gMnPc2	duchovní
až	až	k9	až
po	po	k7c4	po
přípravu	příprava	k1gFnSc4	příprava
státního	státní	k2eAgInSc2d1	státní
převratu	převrat	k1gInSc2	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
však	však	k9	však
nacismus	nacismus	k1gInSc1	nacismus
těšil	těšit	k5eAaImAgInS	těšit
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
silné	silný	k2eAgFnSc2d1	silná
podpoře	podpora	k1gFnSc3	podpora
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
upozorňoval	upozorňovat	k5eAaImAgMnS	upozorňovat
Adam	Adam	k1gMnSc1	Adam
von	von	k1gInSc4	von
Trott	Trott	k2eAgInSc4d1	Trott
zu	zu	k?	zu
Solz	Solz	k1gInSc4	Solz
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
memorandu	memorandum	k1gNnSc6	memorandum
na	na	k7c4	na
celkovou	celkový	k2eAgFnSc4d1	celková
pasivitu	pasivita	k1gFnSc4	pasivita
dělnictva	dělnictvo	k1gNnSc2	dělnictvo
a	a	k8xC	a
pramalou	pramalý	k2eAgFnSc4d1	pramalá
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
případný	případný	k2eAgInSc1d1	případný
protihitlerovský	protihitlerovský	k2eAgInSc1d1	protihitlerovský
puč	puč	k1gInSc1	puč
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
masovou	masový	k2eAgFnSc7d1	masová
podporou	podpora	k1gFnSc7	podpora
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
špatné	špatný	k2eAgFnSc3d1	špatná
koordinaci	koordinace	k1gFnSc3	koordinace
a	a	k8xC	a
chabým	chabý	k2eAgInPc3d1	chabý
výsledkům	výsledek	k1gInPc3	výsledek
však	však	k9	však
protinacistické	protinacistický	k2eAgNnSc1d1	protinacistické
hnutí	hnutí	k1gNnSc1	hnutí
neupadlo	upadnout	k5eNaPmAgNnS	upadnout
do	do	k7c2	do
zapomnění	zapomnění	k1gNnSc2	zapomnění
a	a	k8xC	a
v	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
Německu	Německo	k1gNnSc6	Německo
existence	existence	k1gFnSc2	existence
odboje	odboj	k1gInSc2	odboj
přispěla	přispět	k5eAaPmAgFnS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
národ	národ	k1gInSc1	národ
dokázal	dokázat	k5eAaPmAgInS	dokázat
lépe	dobře	k6eAd2	dobře
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
minulostí	minulost	k1gFnSc7	minulost
.	.	kIx.	.
</s>
