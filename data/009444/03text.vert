<p>
<s>
Slartibartfast	Slartibartfast	k1gFnSc1	Slartibartfast
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
Stopařův	stopařův	k2eAgMnSc1d1	stopařův
průvodce	průvodce	k1gMnSc1	průvodce
po	po	k7c6	po
Galaxii	galaxie	k1gFnSc6	galaxie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
jméno	jméno	k1gNnSc4	jméno
není	být	k5eNaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
sám	sám	k3xTgMnSc1	sám
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
návrhářů	návrhář	k1gMnPc2	návrhář
a	a	k8xC	a
stavitelů	stavitel	k1gMnPc2	stavitel
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
specialitou	specialita	k1gFnSc7	specialita
jsou	být	k5eAaImIp3nP	být
fjordy	fjord	k1gInPc1	fjord
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
za	za	k7c4	za
ně	on	k3xPp3gNnPc4	on
dostal	dostat	k5eAaPmAgMnS	dostat
i	i	k9	i
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Magrathea	Magrathe	k1gInSc2	Magrathe
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
Magrathei	Magrathee	k1gFnSc4	Magrathee
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Stopařův	stopařův	k2eAgMnSc1d1	stopařův
průvodce	průvodce	k1gMnSc1	průvodce
po	po	k7c6	po
Galaxii	galaxie	k1gFnSc6	galaxie
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
opět	opět	k6eAd1	opět
dala	dát	k5eAaPmAgFnS	dát
do	do	k7c2	do
chodu	chod	k1gInSc2	chod
po	po	k7c6	po
vesmírné	vesmírný	k2eAgFnSc6d1	vesmírná
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
krizi	krize	k1gFnSc6	krize
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
vesmírní	vesmírný	k2eAgMnPc1d1	vesmírný
magnáti	magnát	k1gMnPc1	magnát
už	už	k6eAd1	už
nechtěli	chtít	k5eNaImAgMnP	chtít
mrhat	mrhat	k5eAaImF	mrhat
penězi	peníze	k1gInPc7	peníze
za	za	k7c2	za
planety	planeta	k1gFnSc2	planeta
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
tvaru	tvar	k1gInSc2	tvar
kvádru	kvádr	k1gInSc2	kvádr
apod.	apod.	kA	apod.
Svými	svůj	k3xOyFgInPc7	svůj
fjordy	fjord	k1gInPc7	fjord
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
tak	tak	k6eAd1	tak
posedlý	posedlý	k2eAgMnSc1d1	posedlý
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
si	se	k3xPyFc3	se
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
,	,	kIx,	,
jistí	jistit	k5eAaImIp3nP	jistit
pandimenzionální	pandimenzionální	k2eAgMnPc1d1	pandimenzionální
tvorové	tvor	k1gMnPc1	tvor
objednají	objednat	k5eAaPmIp3nP	objednat
Zemi	zem	k1gFnSc4	zem
č.	č.	k?	č.
2	[number]	k4	2
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
na	na	k7c4	na
Afriku	Afrika	k1gFnSc4	Afrika
nahrnout	nahrnout	k5eAaPmF	nahrnout
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
knihách	kniha	k1gFnPc6	kniha
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
jako	jako	k9	jako
člen	člen	k1gInSc1	člen
společenství	společenství	k1gNnSc2	společenství
za	za	k7c4	za
normální	normální	k2eAgInSc4d1	normální
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
kvůli	kvůli	k7c3	kvůli
historickým	historický	k2eAgInPc3d1	historický
paradoxům	paradox	k1gInPc3	paradox
vzniklým	vzniklý	k2eAgNnSc7d1	vzniklé
díky	dík	k1gInPc7	dík
cestování	cestování	k1gNnSc2	cestování
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
příběhu	příběh	k1gInSc2	příběh
opět	opět	k6eAd1	opět
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
v	v	k7c6	v
lodi	loď	k1gFnSc6	loď
vypadající	vypadající	k2eAgNnSc1d1	vypadající
jako	jako	k8xS	jako
italské	italský	k2eAgNnSc1d1	italské
bistro	bistro	k1gNnSc1	bistro
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
cestovat	cestovat	k5eAaImF	cestovat
díky	díky	k7c3	díky
bistromatice	bistromatika	k1gFnSc3	bistromatika
<g/>
,	,	kIx,	,
způsobu	způsob	k1gInSc6	způsob
vypočítávání	vypočítávání	k1gNnSc2	vypočítávání
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c6	na
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
cokoliv	cokoliv	k3yInSc4	cokoliv
napsané	napsaný	k2eAgFnPc4d1	napsaná
na	na	k7c6	na
účtence	účtenka	k1gFnSc6	účtenka
si	se	k3xPyFc3	se
řídí	řídit	k5eAaImIp3nS	řídit
jinými	jiný	k2eAgInPc7d1	jiný
zákony	zákon	k1gInPc7	zákon
než	než	k8xS	než
čísla	číslo	k1gNnSc2	číslo
na	na	k7c6	na
jakýchkoliv	jakýkoliv	k3yIgInPc6	jakýkoliv
jiných	jiný	k2eAgInPc6d1	jiný
kusech	kus	k1gInPc6	kus
papíru	papír	k1gInSc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Fordem	ford	k1gInSc7	ford
Prefectem	Prefect	k1gInSc7	Prefect
a	a	k8xC	a
Arthurem	Arthur	k1gMnSc7	Arthur
Dentem	Dent	k1gMnSc7	Dent
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
pokusí	pokusit	k5eAaPmIp3nS	pokusit
zachránit	zachránit	k5eAaPmF	zachránit
vesmír	vesmír	k1gInSc1	vesmír
od	od	k7c2	od
invaze	invaze	k1gFnSc2	invaze
planety	planeta	k1gFnPc4	planeta
skryté	skrytý	k2eAgFnPc4d1	skrytá
v	v	k7c6	v
oblačnu	oblačno	k1gNnSc6	oblačno
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
</p>
