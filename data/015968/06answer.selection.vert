<s>
Nejstarší	starý	k2eAgInPc1d3
archeologické	archeologický	k2eAgInPc1d1
nálezy	nález	k1gInPc1
se	se	k3xPyFc4
datují	datovat	k5eAaImIp3nP
až	až	k9
do	do	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
280	#num#	k4
000	#num#	k4
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgInPc4d2
nálezy	nález	k1gInPc4
jsou	být	k5eAaImIp3nP
staré	starý	k2eAgInPc4d1
zhruba	zhruba	k6eAd1
120	#num#	k4
000	#num#	k4
až	až	k9
10	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>