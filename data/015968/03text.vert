<s>
Essen	Essen	k1gInSc1
</s>
<s>
Essen	Essen	k1gInSc1
Essen	Essen	k1gInSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
3	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
116	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc4
Spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Severní	severní	k2eAgNnSc1d1
Porýní-Vestfálsko	Porýní-Vestfálsko	k1gNnSc1
</s>
<s>
Essen	Essen	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
210,32	210,32	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
583	#num#	k4
109	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
2	#num#	k4
787	#num#	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Thomas	Thomas	k1gMnSc1
Kufen	Kufna	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
2015	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.essen.de	www.essen.de	k6eAd1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
0201	#num#	k4
a	a	k8xC
02054	#num#	k4
PSČ	PSČ	kA
</s>
<s>
45359	#num#	k4
a	a	k8xC
45001	#num#	k4
Označení	označení	k1gNnPc2
vozidel	vozidlo	k1gNnPc2
</s>
<s>
E	E	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Essen	Essen	k1gInSc1
[	[	kIx(
<g/>
ˈ	ˈ	k1gInSc1
<g/>
̩	̩	k?
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
v	v	k7c6
Porúří	Porúří	k1gNnSc6
v	v	k7c6
německé	německý	k2eAgFnSc6d1
spolkové	spolkový	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
Severní	severní	k2eAgNnSc4d1
Porýní-Vestfálsko	Porýní-Vestfálsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Ruhr	Ruhra	k1gFnPc2
a	a	k8xC
žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
583	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ho	on	k3xPp3gMnSc4
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
deset	deset	k4xCc4
největších	veliký	k2eAgNnPc2d3
měst	město	k1gNnPc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
Essen	Essen	k1gInSc1
stal	stát	k5eAaPmAgInS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
nejdůležitějších	důležitý	k2eAgNnPc2d3
center	centrum	k1gNnPc2
těžby	těžba	k1gFnSc2
uhlí	uhlí	k1gNnSc2
a	a	k8xC
výroby	výroba	k1gFnSc2
oceli	ocel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
ekonomika	ekonomika	k1gFnSc1
postavena	postaven	k2eAgFnSc1d1
na	na	k7c6
jiných	jiný	k2eAgInPc6d1
průmyslových	průmyslový	k2eAgInPc6d1
odvětvích	odvětví	k1gNnPc6
a	a	k8xC
na	na	k7c6
službách	služba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
sídlem	sídlo	k1gNnSc7
třinácti	třináct	k4xCc3
ze	z	k7c2
sta	sto	k4xCgNnPc4
největších	veliký	k2eAgFnPc2d3
německých	německý	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Essenská	essenský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1972	#num#	k4
a	a	k8xC
roku	rok	k1gInSc2
2003	#num#	k4
se	se	k3xPyFc4
sloučila	sloučit	k5eAaPmAgFnS
s	s	k7c7
Duisburskou	duisburský	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
vznikla	vzniknout	k5eAaPmAgFnS
současná	současný	k2eAgFnSc1d1
Duisbursko-Essenská	duisbursko-essenský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Essen	Essen	k1gInSc1
na	na	k7c6
rytině	rytina	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1647	#num#	k4
</s>
<s>
Navzdory	navzdory	k7c3
poměrně	poměrně	k6eAd1
rozšířenému	rozšířený	k2eAgInSc3d1
názoru	názor	k1gInSc3
nemá	mít	k5eNaImIp3nS
název	název	k1gInSc1
Essen	Essen	k1gInSc1
nic	nic	k3yNnSc1
společného	společný	k2eAgNnSc2d1
s	s	k7c7
jídlem	jídlo	k1gNnSc7
(	(	kIx(
<g/>
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
němčině	němčina	k1gFnSc6
rovněž	rovněž	k9
Essen	Essen	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
zřejmě	zřejmě	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
velmi	velmi	k6eAd1
starého	starý	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
Asnithi	Asnithi	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
označovalo	označovat	k5eAaImAgNnS
území	území	k1gNnSc4
na	na	k7c6
východě	východ	k1gInSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
území	území	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
rostlo	růst	k5eAaImAgNnS
mnoho	mnoho	k4c1
jasanů	jasan	k1gInPc2
(	(	kIx(
<g/>
Esche	Esche	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3
archeologické	archeologický	k2eAgInPc1d1
nálezy	nález	k1gInPc1
se	se	k3xPyFc4
datují	datovat	k5eAaImIp3nP
až	až	k9
do	do	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
280	#num#	k4
000	#num#	k4
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgInPc4d2
nálezy	nález	k1gInPc4
jsou	být	k5eAaImIp3nP
staré	starý	k2eAgInPc4d1
zhruba	zhruba	k6eAd1
120	#num#	k4
000	#num#	k4
až	až	k9
10	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Essen	Essen	k1gInSc1
byl	být	k5eAaImAgInS
sídlištěm	sídliště	k1gNnSc7
mnoha	mnoho	k4c2
germánských	germánský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
ale	ale	k9
velmi	velmi	k6eAd1
těžké	těžký	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
rozlišovat	rozlišovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Alteburg	Alteburg	k1gInSc1
je	být	k5eAaImIp3nS
hrad	hrad	k1gInSc4
na	na	k7c6
jihu	jih	k1gInSc6
Essenu	Essen	k1gInSc2
datovaný	datovaný	k2eAgInSc4d1
do	do	k7c2
dob	doba	k1gFnPc2
Keltů	Kelt	k1gMnPc2
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
okolo	okolo	k7c2
počátku	počátek	k1gInSc2
našeho	náš	k3xOp1gInSc2
letopočtu	letopočet	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
druhého	druhý	k4xOgInSc2
hradu	hrad	k1gInSc2
Herrenburg	Herrenburg	k1gMnSc1
sahá	sahat	k5eAaImIp3nS
do	do	k7c2
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Zárodkem	zárodek	k1gInSc7
města	město	k1gNnSc2
Essen	Essen	k1gInSc1
byl	být	k5eAaImAgInS
ženský	ženský	k2eAgInSc4d1
klášter	klášter	k1gInSc4
kanovnic	kanovnice	k1gFnPc2
založený	založený	k2eAgInSc1d1
kolem	kolem	k7c2
roku	rok	k1gInSc2
845	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejdůležitějších	důležitý	k2eAgInPc2d3
klášterů	klášter	k1gInPc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
abatyše	abatyše	k1gFnSc2
patřily	patřit	k5eAaImAgFnP
mezi	mezi	k7c4
říšské	říšský	k2eAgInPc4d1
preláty	prelát	k1gInPc4
s	s	k7c7
knížecím	knížecí	k2eAgInSc7d1
titulem	titul	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
stála	stát	k5eAaImAgFnS
Matylda	Matylda	k1gFnSc1
<g/>
,	,	kIx,
vnučka	vnuček	k1gMnSc2
císaře	císař	k1gMnSc2
Otta	Otta	k1gMnSc1
I.	I.	kA
<g/>
,	,	kIx,
a	a	k8xC
po	po	k7c6
ní	on	k3xPp3gFnSc6
dvě	dva	k4xCgFnPc1
další	další	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
také	také	k9
příbuzné	příbuzná	k1gFnPc1
císařů	císař	k1gMnPc2
z	z	k7c2
této	tento	k3xDgFnSc2
dynastie	dynastie	k1gFnSc2
<g/>
:	:	kIx,
Sophia	Sophia	k1gFnSc1
<g/>
,	,	kIx,
dcera	dcera	k1gFnSc1
Otty	Otta	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
a	a	k8xC
Theophanu	Theophana	k1gFnSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
vnučka	vnučka	k1gFnSc1
(	(	kIx(
<g/>
dcera	dcera	k1gFnSc1
Matyldy	Matylda	k1gFnSc2
<g/>
,	,	kIx,
dcery	dcera	k1gFnPc1
Otty	Otta	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
se	se	k3xPyFc4
z	z	k7c2
Essenu	Essen	k1gInSc2
až	až	k6eAd1
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
nevýznamné	významný	k2eNgNnSc1d1
zemědělské	zemědělský	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrušený	zrušený	k2eAgInSc1d1
klášter	klášter	k1gInSc1
připomínají	připomínat	k5eAaImIp3nP
v	v	k7c6
současnosti	současnost	k1gFnSc6
jeho	jeho	k3xOp3gInSc1
původní	původní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
(	(	kIx(
<g/>
Münster	Münster	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
biskupská	biskupský	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
zámek	zámek	k1gInSc1
Borbeck	Borbecka	k1gFnPc2
s	s	k7c7
rozsáhlým	rozsáhlý	k2eAgInSc7d1
parkem	park	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc1
města	město	k1gNnSc2
</s>
<s>
Znak	znak	k1gInSc1
Essenu	Essen	k1gInSc2
</s>
<s>
Hotel	hotel	k1gInSc1
Handelshof	Handelshof	k1gInSc1
s	s	k7c7
upraveným	upravený	k2eAgInSc7d1
znakem	znak	k1gInSc7
a	a	k8xC
neoficiálním	oficiální	k2eNgNnSc7d1,k2eAgNnSc7d1
mottem	motto	k1gNnSc7
města	město	k1gNnSc2
</s>
<s>
Znak	znak	k1gInSc1
města	město	k1gNnSc2
Essen	Essen	k1gInSc1
je	být	k5eAaImIp3nS
heraldická	heraldický	k2eAgFnSc1d1
zvláštnost	zvláštnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
ho	on	k3xPp3gMnSc4
získalo	získat	k5eAaPmAgNnS
roku	rok	k1gInSc2
1886	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sestává	sestávat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgInPc2
oddělených	oddělený	k2eAgInPc2d1
štítů	štít	k1gInPc2
pod	pod	k7c7
společnou	společný	k2eAgFnSc7d1
korunou	koruna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koruna	koruna	k1gFnSc1
nesymbolizuje	symbolizovat	k5eNaImIp3nS
přímo	přímo	k6eAd1
město	město	k1gNnSc1
Essen	Essen	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
sekularizovanou	sekularizovaný	k2eAgFnSc4d1
(	(	kIx(
<g/>
zesvětštělou	zesvětštělý	k2eAgFnSc4d1
<g/>
)	)	kIx)
církevní	církevní	k2eAgFnSc4d1
radu	rada	k1gFnSc4
Essenu	Essen	k1gInSc2
pod	pod	k7c7
vládou	vláda	k1gFnSc7
kněžny-abatyše	kněžny-abatyše	k1gFnSc2
místního	místní	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
štítě	štít	k1gInSc6
nalevo	nalevo	k6eAd1
je	být	k5eAaImIp3nS
vyobrazen	vyobrazen	k2eAgMnSc1d1
dvouhlavý	dvouhlavý	k2eAgMnSc1d1
císařský	císařský	k2eAgMnSc1d1
orel	orel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
štít	štít	k1gInSc1
byl	být	k5eAaImAgInS
městu	město	k1gNnSc3
dán	dán	k2eAgInSc1d1
roku	rok	k1gInSc2
1623	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
pravém	pravý	k2eAgInSc6d1
štítě	štít	k1gInSc6
je	být	k5eAaImIp3nS
vyobrazen	vyobrazen	k2eAgInSc1d1
meč	meč	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yQgInSc7,k3yIgInSc7
podle	podle	k7c2
pověsti	pověst	k1gFnPc1
byly	být	k5eAaImAgFnP
setnuty	setnut	k2eAgFnPc4d1
hlavy	hlava	k1gFnPc4
patronů	patron	k1gMnPc2
města	město	k1gNnSc2
<g/>
,	,	kIx,
svatých	svatý	k1gMnPc2
Kosmy	Kosma	k1gMnSc2
a	a	k8xC
Damiána	Damián	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meč	meč	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
s	s	k7c7
událostí	událost	k1gFnSc7
spojován	spojovat	k5eAaImNgMnS
a	a	k8xC
je	být	k5eAaImIp3nS
uložen	uložit	k5eAaPmNgInS
v	v	k7c6
městské	městský	k2eAgFnSc6d1
pokladnici	pokladnice	k1gFnSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
pozdějšího	pozdní	k2eAgNnSc2d2
data	datum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lehce	lehko	k6eAd1
upravená	upravený	k2eAgFnSc1d1
a	a	k8xC
heraldicky	heraldicky	k6eAd1
správnější	správní	k2eAgFnSc1d2
verze	verze	k1gFnSc1
znaku	znak	k1gInSc2
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
na	na	k7c6
střeše	střecha	k1gFnSc6
hotelu	hotel	k1gInSc2
Handelshof	Handelshof	k1gInSc4
nedaleko	nedaleko	k7c2
hlavního	hlavní	k2eAgNnSc2d1
nádraží	nádraží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Essen	Essen	k1gInSc1
leží	ležet	k5eAaImIp3nS
uprostřed	uprostřed	k7c2
Porúří	Porúří	k1gNnSc2
severně	severně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Ruhr	Ruhr	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ve	v	k7c6
čtvrtích	čtvrt	k1gFnPc6
Kupferdreh	Kupferdreh	k1gInSc1
<g/>
,	,	kIx,
Heisingen	Heisingen	k1gInSc1
a	a	k8xC
Werden	Werdna	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nS
umělé	umělý	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
Baldeney	Baldenea	k1gFnSc2
(	(	kIx(
<g/>
Baldeney-See	Baldeney-See	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezero	jezero	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
mezi	mezi	k7c7
lety	let	k1gInPc7
1931	#num#	k4
až	až	k9
1933	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ho	on	k3xPp3gMnSc4
výměnou	výměna	k1gFnSc7
za	za	k7c4
chléb	chléb	k1gInSc4
a	a	k8xC
pivo	pivo	k1gNnSc4
vykopalo	vykopat	k5eAaPmAgNnS
10	#num#	k4
000	#num#	k4
nezaměstnaných	zaměstnaný	k2eNgMnPc2d1
dělníků	dělník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnižší	nízký	k2eAgInSc4d3
bod	bod	k1gInSc4
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
26,5	26,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
ve	v	k7c6
čtvrti	čtvrt	k1gFnSc6
Karnap	Karnap	k1gInSc4
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc4d3
202,5	202,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
pak	pak	k6eAd1
ve	v	k7c6
čtvrti	čtvrt	k1gFnSc6
Heidhausen	Heidhausna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
116	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Město	město	k1gNnSc1
se	se	k3xPyFc4
rozprostírá	rozprostírat	k5eAaImIp3nS
přes	přes	k7c4
21	#num#	k4
km	km	kA
od	od	k7c2
severu	sever	k1gInSc2
k	k	k7c3
jihu	jih	k1gInSc2
a	a	k8xC
17	#num#	k4
km	km	kA
od	od	k7c2
západu	západ	k1gInSc2
k	k	k7c3
východu	východ	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
Essenem	Essen	k1gInSc7
sousedí	sousedit	k5eAaImIp3nS
ve	v	k7c6
směru	směr	k1gInSc6
běhu	běh	k1gInSc2
hodinových	hodinový	k2eAgFnPc2d1
ručiček	ručička	k1gFnPc2
následující	následující	k2eAgFnSc2d1
obce	obec	k1gFnSc2
<g/>
:	:	kIx,
Gelsenkirchen	Gelsenkirchen	k1gInSc1
<g/>
,	,	kIx,
Bochum	Bochum	k1gInSc1
<g/>
,	,	kIx,
Hattingen	Hattingen	k1gInSc1
<g/>
,	,	kIx,
Velbert	Velbert	k1gInSc1
<g/>
,	,	kIx,
Heiligenhaus	Heiligenhaus	k1gInSc1
<g/>
,	,	kIx,
Ratingen	Ratingen	k1gInSc1
<g/>
,	,	kIx,
Oberhausen	Oberhausen	k1gInSc1
<g/>
,	,	kIx,
Mülheim	Mülheim	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Ruhr	Ruhr	k1gInSc1
<g/>
,	,	kIx,
Bottrop	Bottrop	k1gInSc1
a	a	k8xC
Gladbeck	Gladbeck	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Počasí	počasí	k1gNnSc1
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
ve	v	k7c6
městě	město	k1gNnSc6
je	být	k5eAaImIp3nS
9,6	9,6	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
za	za	k7c4
rok	rok	k1gInSc4
zde	zde	k6eAd1
průměrně	průměrně	k6eAd1
naprší	napršet	k5eAaPmIp3nS
829	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejchladnějším	chladný	k2eAgInSc7d3
měsícem	měsíc	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
je	být	k5eAaImIp3nS
leden	leden	k1gInSc1
s	s	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
teplotou	teplota	k1gFnSc7
1,5	1,5	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
nejteplejším	teplý	k2eAgInSc7d3
pak	pak	k6eAd1
červenec	červenec	k1gInSc1
se	s	k7c7
17,5	17,5	k4
°	°	k?
<g/>
C.	C.	kA
Nejvíce	hodně	k6eAd3,k6eAd1
vody	voda	k1gFnPc1
zde	zde	k6eAd1
naprší	napršet	k5eAaPmIp3nP
v	v	k7c6
srpnu	srpen	k1gInSc6
<g/>
,	,	kIx,
90	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s>
Městské	městský	k2eAgFnSc3d1
části	část	k1gFnSc3
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
střed	střed	k1gInSc4
essenské	essenský	k2eAgFnSc2d1
čtvrti	čtvrt	k1gFnSc2
Kettwig	Kettwiga	k1gFnPc2
z	z	k7c2
Rúrského	rúrský	k2eAgInSc2d1
mostu	most	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
popředí	popředí	k1gNnSc6
je	být	k5eAaImIp3nS
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
znova	znova	k6eAd1
vybudovaný	vybudovaný	k2eAgInSc1d1
Uferpalais	Uferpalais	k1gInSc1
(	(	kIx(
<g/>
Nábřežní	nábřežní	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Essen	Essen	k1gInSc1
je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
19	#num#	k4
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
má	mít	k5eAaImIp3nS
vlastní	vlastní	k2eAgNnSc1d1
devatenáctičlenné	devatenáctičlenný	k2eAgNnSc1d1
zastupitelstvo	zastupitelstvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
má	mít	k5eAaImIp3nS
omezené	omezený	k2eAgFnPc4d1
pravomoci	pravomoc	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městské	městský	k2eAgFnPc4d1
části	část	k1gFnPc4
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
pojmenovány	pojmenován	k2eAgFnPc1d1
podle	podle	k7c2
čtvrtí	čtvrt	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yRgNnSc4,k3yQgNnSc4
je	být	k5eAaImIp3nS
tvoří	tvořit	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
označují	označovat	k5eAaImIp3nP
se	se	k3xPyFc4
také	také	k9
pomocí	pomocí	k7c2
římských	římský	k2eAgFnPc2d1
číslic	číslice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Essen	Essen	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
celkem	celkem	k6eAd1
padesáti	padesát	k4xCc2
čtvrtí	čtvrt	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
mnoho	mnoho	k4c1
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
samostatnými	samostatný	k2eAgFnPc7d1
obcemi	obec	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
byly	být	k5eAaImAgFnP
do	do	k7c2
města	město	k1gNnSc2
začleněny	začleněn	k2eAgInPc4d1
mezi	mezi	k7c7
lety	let	k1gInPc7
1901	#num#	k4
až	až	k9
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomalé	pomalý	k2eAgInPc1d1
rozrůstání	rozrůstání	k1gNnSc4
města	město	k1gNnSc2
má	mít	k5eAaImIp3nS
za	za	k7c4
následek	následek	k1gInSc4
i	i	k9
dvě	dva	k4xCgFnPc4
zajímavosti	zajímavost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrť	čtvrť	k1gFnSc1
Kettwig	Kettwiga	k1gFnPc2
ležící	ležící	k2eAgInSc1d1
jižně	jižně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Ruhr	Ruhra	k1gFnPc2
byla	být	k5eAaImAgFnS
začleněna	začleněn	k2eAgFnSc1d1
roku	rok	k1gInSc2
1975	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
má	mít	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnSc4d1
telefonní	telefonní	k2eAgFnSc4d1
předvolbu	předvolba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
Kettwig	Kettwig	k1gInSc1
patří	patřit	k5eAaImIp3nS
přímo	přímo	k6eAd1
pod	pod	k7c4
Kolínské	kolínský	k2eAgNnSc4d1
arcibiskupství	arcibiskupství	k1gNnSc4
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
zbytek	zbytek	k1gInSc1
města	město	k1gNnSc2
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
Essenské	essenský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1
</s>
<s>
Těžba	těžba	k1gFnSc1
uhlí	uhlí	k1gNnSc2
a	a	k8xC
rudy	ruda	k1gFnSc2
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
prudkému	prudký	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
města	město	k1gNnSc2
a	a	k8xC
celého	celý	k2eAgNnSc2d1
Porúří	Porúří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Essenu	Essen	k1gInSc2
pocházející	pocházející	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
Kruppů	Krupp	k1gMnPc2
zde	zde	k6eAd1
roku	rok	k1gInSc2
1811	#num#	k4
založila	založit	k5eAaPmAgFnS
podnik	podnik	k1gInSc4
na	na	k7c4
výrobu	výroba	k1gFnSc4
oceli	ocel	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
Essen	Essen	k1gInSc1
prošel	projít	k5eAaPmAgInS
velkými	velký	k2eAgFnPc7d1
hospodářskými	hospodářský	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
útlumu	útlum	k1gInSc3
těžby	těžba	k1gFnSc2
a	a	k8xC
rozvoji	rozvoj	k1gInSc3
moderních	moderní	k2eAgFnPc2d1
technologicky	technologicky	k6eAd1
náročných	náročný	k2eAgFnPc2d1
odvětví	odvětví	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Silnice	silnice	k1gFnSc1
</s>
<s>
Dálnice	dálnice	k1gFnSc1
A40	A40	k1gFnSc2
–	–	k?
Ruhrschnellweg	Ruhrschnellweg	k1gMnSc1
</s>
<s>
Dálnice	dálnice	k1gFnSc1
A	a	k9
<g/>
40	#num#	k4
<g/>
,	,	kIx,
přezdívaná	přezdívaný	k2eAgFnSc1d1
Ruhrschnellweg	Ruhrschnellweg	k1gInSc4
probíhá	probíhat	k5eAaImIp3nS
středem	střed	k1gInSc7
města	město	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
dělí	dělit	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
na	na	k7c4
dvě	dva	k4xCgFnPc4
poloviny	polovina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
centru	centrum	k1gNnSc6
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
dálnice	dálnice	k1gFnSc1
skrytá	skrytý	k2eAgFnSc1d1
v	v	k7c6
tunelu	tunel	k1gInSc6
vybudovaném	vybudovaný	k2eAgInSc6d1
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrajů	okraj	k1gInPc2
města	město	k1gNnSc2
se	se	k3xPyFc4
také	také	k9
dotýkají	dotýkat	k5eAaImIp3nP
dálnice	dálnice	k1gFnSc1
A	a	k8xC
52	#num#	k4
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
A42	A42	k1gFnSc6
na	na	k7c6
severu	sever	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
centrum	centrum	k1gNnSc4
Essenu	Essen	k1gInSc2
z	z	k7c2
Tetraederu	Tetraeder	k1gInSc2
v	v	k7c6
sousedním	sousední	k2eAgNnSc6d1
městě	město	k1gNnSc6
Bottropu	Bottrop	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozadí	pozadí	k1gNnSc6
jsou	být	k5eAaImIp3nP
Porúrské	porúrský	k2eAgFnPc4d1
výšiny	výšina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
linka	linka	k1gFnSc1
přes	přes	k7c4
území	území	k1gNnSc4
dnešního	dnešní	k2eAgInSc2d1
Essenu	Essen	k1gInSc2
<g/>
,	,	kIx,
železniční	železniční	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
z	z	k7c2
Kolína	Kolín	k1gInSc2
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
do	do	k7c2
Mindenu	Minden	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
otevřena	otevřít	k5eAaPmNgFnS
mezi	mezi	k7c7
lety	let	k1gInPc7
1845	#num#	k4
až	až	k9
1847	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měla	mít	k5eAaImAgFnS
zastávku	zastávka	k1gFnSc4
v	v	k7c4
Altenessenu	Altenessen	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dráha	dráha	k1gFnSc1
z	z	k7c2
Mülheimu	Mülheim	k1gInSc2
an	an	k?
der	drát	k5eAaImRp2nS
Ruhr	Ruhr	k1gInSc1
do	do	k7c2
Bochumi	Bochu	k1gFnPc7
byla	být	k5eAaImAgFnS
otevřena	otevřen	k2eAgFnSc1d1
o	o	k7c4
pár	pár	k4xCyI
desítek	desítka	k1gFnPc2
let	léto	k1gNnPc2
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
železniční	železniční	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
nyní	nyní	k6eAd1
stojí	stát	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc1d1
nádraží	nádraží	k1gNnSc1
Essen	Essen	k1gInSc1
(	(	kIx(
<g/>
Essen-Hauptbahnhof	Essen-Hauptbahnhof	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
staví	stavit	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
jak	jak	k6eAd1
regionální	regionální	k2eAgFnSc1d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
dálkové	dálkový	k2eAgInPc4d1
vlaky	vlak	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Místní	místní	k2eAgFnSc4d1
veřejnou	veřejný	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
zajišťuje	zajišťovat	k5eAaImIp3nS
Essener	Essener	k1gMnSc1
Verkehrs-AG	Verkehrs-AG	k1gMnSc1
<g/>
,	,	kIx,
veřejná	veřejný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
o	o	k7c4
tři	tři	k4xCgInPc4
příměstské	příměstský	k2eAgInPc4d1
vlakové	vlakový	k2eAgInPc4d1
<g/>
,	,	kIx,
několik	několik	k4yIc4
tramvajových	tramvajový	k2eAgFnPc2d1
a	a	k8xC
autobusových	autobusový	k2eAgFnPc2d1
linek	linka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštností	zvláštnost	k1gFnPc2
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
Spurbus	Spurbus	k1gInSc1
<g/>
,	,	kIx,
vedený	vedený	k2eAgInSc1d1
autobus	autobus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
může	moct	k5eAaImIp3nS
jezdit	jezdit	k5eAaImF
klasicky	klasicky	k6eAd1
jako	jako	k8xC,k8xS
autobus	autobus	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
trolejbus	trolejbus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
má	mít	k5eAaImIp3nS
také	také	k9
síť	síť	k1gFnSc4
linek	linka	k1gFnPc2
metra	metro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
místní	místní	k2eAgInPc1d1
a	a	k8xC
regionální	regionální	k2eAgInPc1d1
spoje	spoj	k1gInPc1
mají	mít	k5eAaImIp3nP
tarifní	tarifní	k2eAgInSc4d1
systém	systém	k1gInSc4
dopravního	dopravní	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
Verkehrsverbund	Verkehrsverbund	k1gMnSc1
Rhein-Ruhr	Rhein-Ruhr	k1gMnSc1
(	(	kIx(
<g/>
VRR	VRR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Grenoble	Grenoble	k1gInSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
1974	#num#	k4
</s>
<s>
Nižnij	Nižnít	k5eAaPmRp2nS
Novgorod	Novgorod	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
</s>
<s>
Sunderland	Sunderland	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
1949	#num#	k4
</s>
<s>
Tampere	Tamprat	k5eAaPmIp3nS
<g/>
,	,	kIx,
Finsko	Finsko	k1gNnSc1
<g/>
,	,	kIx,
1960	#num#	k4
</s>
<s>
Tel	tel	kA
Aviv-Jaffa	Aviv-Jaff	k1gMnSc4
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
1991	#num#	k4
</s>
<s>
Významní	významný	k2eAgMnPc1d1
rodáci	rodák	k1gMnPc1
</s>
<s>
Alfred	Alfred	k1gMnSc1
Krupp	Krupp	k1gMnSc1
(	(	kIx(
<g/>
1812	#num#	k4
<g/>
–	–	k?
<g/>
1887	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
průmyslník	průmyslník	k1gMnSc1
a	a	k8xC
vynálezce	vynálezce	k1gMnSc1
</s>
<s>
Bertha	Bertha	k1gMnSc1
Krupp	Krupp	k1gMnSc1
von	von	k1gInSc4
Bohlen	Bohlen	k2eAgInSc4d1
und	und	k?
Halbach	Halbach	k1gInSc1
(	(	kIx(
<g/>
1886	#num#	k4
<g/>
–	–	k?
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dědička	dědička	k1gFnSc1
ocelářského	ocelářský	k2eAgNnSc2d1
a	a	k8xC
zbrojařského	zbrojařský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
rodu	rod	k1gInSc2
Kruppových	Kruppův	k2eAgMnPc2d1
</s>
<s>
Jörg	Jörg	k1gInSc1
Meuthen	Meuthen	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
1961	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
německý	německý	k2eAgMnSc1d1
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
AfD	AfD	k1gMnSc1
</s>
<s>
Marion	Marion	k1gInSc1
Poschmann	Poschmann	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
1969	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spisovatelka	spisovatelka	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
města	město	k1gNnSc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Essen	Essen	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Města	město	k1gNnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
podle	podle	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnSc1
1	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Berlín	Berlín	k1gInSc1
•	•	k?
Hamburk	Hamburk	k1gInSc1
•	•	k?
Mnichov	Mnichov	k1gInSc1
•	•	k?
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
500	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Brémy	Brémy	k1gFnPc1
•	•	k?
Dortmund	Dortmund	k1gInSc1
•	•	k?
Drážďany	Drážďany	k1gInPc1
•	•	k?
Düsseldorf	Düsseldorf	k1gInSc1
•	•	k?
Essen	Essen	k1gInSc1
•	•	k?
Frankfurt	Frankfurt	k1gInSc1
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
•	•	k?
Hannover	Hannover	k1gInSc1
•	•	k?
Lipsko	Lipsko	k1gNnSc4
•	•	k?
Stuttgart	Stuttgart	k1gInSc1
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Augsburg	Augsburg	k1gMnSc1
•	•	k?
Bielefeld	Bielefeld	k1gMnSc1
•	•	k?
Bochum	Bochum	k1gInSc1
•	•	k?
Bonn	Bonn	k1gInSc1
•	•	k?
Braunschweig	Braunschweig	k1gInSc1
•	•	k?
Cáchy	Cáchy	k1gFnPc4
•	•	k?
Duisburg	Duisburg	k1gInSc1
•	•	k?
Erfurt	Erfurt	k1gInSc1
•	•	k?
Freiburg	Freiburg	k1gInSc1
im	im	k?
Breisgau	Breisgaus	k1gInSc2
•	•	k?
Gelsenkirchen	Gelsenkirchen	k2eAgInSc1d1
•	•	k?
Halle	Halle	k1gInSc1
(	(	kIx(
<g/>
Saale	Saale	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Karlsruhe	Karlsruhe	k1gInSc1
•	•	k?
Kiel	Kiel	k1gInSc1
•	•	k?
Krefeld	Krefeld	k1gInSc1
•	•	k?
Lübeck	Lübeck	k1gInSc1
•	•	k?
Magdeburg	Magdeburg	k1gInSc1
•	•	k?
Mannheim	Mannheim	k1gInSc1
•	•	k?
Münster	Münster	k1gInSc1
•	•	k?
Mohuč	Mohuč	k1gFnSc1
•	•	k?
Mönchengladbach	Mönchengladbach	k1gInSc1
•	•	k?
Norimberk	Norimberk	k1gInSc1
•	•	k?
Oberhausen	Oberhausen	k1gInSc1
•	•	k?
Saská	saský	k2eAgFnSc1d1
Kamenice	Kamenice	k1gFnSc1
•	•	k?
Rostock	Rostock	k1gInSc1
•	•	k?
Wiesbaden	Wiesbaden	k1gInSc1
•	•	k?
Wuppertal	Wuppertal	k1gFnSc2
100	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Bergisch	Bergisch	k1gMnSc1
Gladbach	Gladbach	k1gMnSc1
•	•	k?
Bottrop	Bottrop	k1gInSc1
•	•	k?
Bremerhaven	Bremerhaven	k2eAgInSc1d1
•	•	k?
Darmstadt	Darmstadt	k1gInSc1
•	•	k?
Erlangen	Erlangen	k1gInSc1
•	•	k?
Fürth	Fürth	k1gInSc1
•	•	k?
Göttingen	Göttingen	k1gInSc1
•	•	k?
Hagen	Hagen	k1gInSc1
•	•	k?
Hamm	Hamm	k1gInSc1
•	•	k?
Heidelberg	Heidelberg	k1gInSc1
•	•	k?
Heilbronn	Heilbronn	k1gInSc1
•	•	k?
Herne	Hern	k1gInSc5
•	•	k?
Ingolstadt	Ingolstadt	k1gInSc1
•	•	k?
Jena	Jen	k1gInSc2
•	•	k?
Kassel	Kassel	k1gInSc1
•	•	k?
Koblenz	Koblenz	k1gInSc1
•	•	k?
Leverkusen	Leverkusen	k1gInSc1
•	•	k?
Ludwigshafen	Ludwigshafen	k1gInSc1
•	•	k?
Moers	Moers	k1gInSc1
•	•	k?
Mülheim	Mülheim	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Ruhr	Ruhr	k1gInSc1
•	•	k?
Neuss	Neuss	k1gInSc1
•	•	k?
Offenbach	Offenbach	k1gInSc1
am	am	k?
Main	Main	k1gInSc1
•	•	k?
Oldenburg	Oldenburg	k1gInSc1
•	•	k?
Osnabrück	Osnabrück	k1gInSc1
•	•	k?
Paderborn	Paderborn	k1gInSc1
•	•	k?
Pforzheim	Pforzheim	k1gInSc1
•	•	k?
Postupim	Postupim	k1gFnSc1
•	•	k?
Recklinghausen	Recklinghausen	k1gInSc1
•	•	k?
Remscheid	Remscheid	k1gInSc1
•	•	k?
Reutlingen	Reutlingen	k1gInSc1
•	•	k?
Řezno	Řezno	k1gNnSc4
•	•	k?
Saarbrücken	Saarbrücken	k1gInSc1
•	•	k?
Salzgitter	Salzgitter	k1gInSc1
•	•	k?
Solingen	Solingen	k1gInSc1
•	•	k?
Trevír	Trevír	k1gInSc1
•	•	k?
Ulm	Ulm	k1gFnSc2
•	•	k?
Wolfsburg	Wolfsburg	k1gInSc1
•	•	k?
Würzburg	Würzburg	k1gInSc1
</s>
<s>
Městské	městský	k2eAgInPc1d1
a	a	k8xC
Zemské	zemský	k2eAgInPc1d1
okresy	okres	k1gInPc1
Severního	severní	k2eAgNnSc2d1
Porýní-Vestfálska	Porýní-Vestfálsko	k1gNnSc2
Městské	městský	k2eAgInPc1d1
okresy	okres	k1gInPc1
</s>
<s>
Bielefeld	Bielefeld	k1gInSc1
•	•	k?
Bochum	Bochum	k1gInSc1
•	•	k?
Bonn	Bonn	k1gInSc1
•	•	k?
Bottrop	Bottrop	k1gInSc1
•	•	k?
Dortmund	Dortmund	k1gInSc1
•	•	k?
Duisburg	Duisburg	k1gInSc1
•	•	k?
Düsseldorf	Düsseldorf	k1gInSc1
•	•	k?
Essen	Essen	k1gInSc1
•	•	k?
Gelsenkirchen	Gelsenkirchen	k1gInSc1
•	•	k?
Hagen	Hagen	k1gInSc1
•	•	k?
Hamm	Hamm	k1gInSc1
•	•	k?
Herne	Hern	k1gInSc5
•	•	k?
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
•	•	k?
Krefeld	Krefeld	k1gInSc1
•	•	k?
Leverkusen	Leverkusen	k1gInSc1
•	•	k?
Mönchengladbach	Mönchengladbach	k1gInSc1
•	•	k?
Mülheim	Mülheim	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Ruhr	Ruhr	k1gInSc1
•	•	k?
Münster	Münster	k1gInSc1
•	•	k?
Oberhausen	Oberhausen	k1gInSc1
•	•	k?
Remscheid	Remscheid	k1gInSc1
•	•	k?
Solingen	Solingen	k1gInSc1
•	•	k?
Wuppertal	Wuppertal	k1gFnSc2
Zemské	zemský	k2eAgFnSc2d1
okresy	okres	k1gInPc1
</s>
<s>
Městský	městský	k2eAgInSc4d1
region	region	k1gInSc4
Cáchy	Cáchy	k1gFnPc4
(	(	kIx(
<g/>
Städteregion	Städteregion	k1gInSc4
Aachen	Aachen	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Borken	Borken	k1gInSc1
•	•	k?
Coesfeld	Coesfeld	k1gInSc1
•	•	k?
Düren	Düren	k1gInSc1
•	•	k?
Ennepe-Ruhr-Kreis	Ennepe-Ruhr-Kreis	k1gInSc1
•	•	k?
Euskirchen	Euskirchen	k1gInSc1
•	•	k?
Gütersloh	Gütersloh	k1gInSc1
•	•	k?
Heinsberg	Heinsberg	k1gInSc1
•	•	k?
Herford	Herford	k1gInSc1
•	•	k?
Hochsauerlandkreis	Hochsauerlandkreis	k1gInSc1
•	•	k?
Höxter	Höxter	k1gInSc1
•	•	k?
Kleve	Kleev	k1gFnSc2
•	•	k?
Lippe	Lipp	k1gInSc5
•	•	k?
Märkischer	Märkischra	k1gFnPc2
Kreis	Kreis	k1gFnSc1
•	•	k?
Mettmann	Mettmann	k1gInSc1
•	•	k?
Minden-Lübbecke	Minden-Lübbecke	k1gInSc1
•	•	k?
Oberbergischer	Oberbergischra	k1gFnPc2
Kreis	Kreis	k1gFnSc1
•	•	k?
Olpe	Olpe	k1gInSc1
•	•	k?
Paderborn	Paderborn	k1gInSc1
•	•	k?
Recklinghausen	Recklinghausen	k1gInSc1
•	•	k?
Rhein-Erft-Kreis	Rhein-Erft-Kreis	k1gInSc1
•	•	k?
Rheinisch-Bergischer	Rheinisch-Bergischra	k1gFnPc2
Kreis	Kreis	k1gFnSc1
•	•	k?
Rhein-Kreis	Rhein-Kreis	k1gInSc1
Neuss	Neuss	k1gInSc1
•	•	k?
Rhein-Sieg-Kreis	Rhein-Sieg-Kreis	k1gInSc1
•	•	k?
Siegen-Wittgenstein	Siegen-Wittgenstein	k1gInSc1
•	•	k?
Soest	Soest	k1gInSc1
•	•	k?
Steinfurt	Steinfurt	k1gInSc1
•	•	k?
Unna	Unn	k1gInSc2
•	•	k?
Viersen	Viersen	k2eAgInSc1d1
•	•	k?
Warendorf	Warendorf	k1gInSc1
•	•	k?
Wesel	Wesel	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
139202	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4015557-2	4015557-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80089415	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
125488872	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80089415	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Německo	Německo	k1gNnSc1
</s>
