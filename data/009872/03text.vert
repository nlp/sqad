<p>
<s>
Letiště	letiště	k1gNnSc1	letiště
Gatwick	Gatwicka	k1gFnPc2	Gatwicka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Gatwick	Gatwick	k1gInSc1	Gatwick
Airport	Airport	k1gInSc1	Airport
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc1	druhý
nejrušnější	rušný	k2eAgNnSc1d3	nejrušnější
letiště	letiště	k1gNnSc1	letiště
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
odbavených	odbavený	k2eAgMnPc2d1	odbavený
cestujících	cestující	k1gMnPc2	cestující
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
West	West	k2eAgInSc1d1	West
Sussex	Sussex	k1gInSc1	Sussex
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
Brightonu	Brighton	k1gInSc2	Brighton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Gatwick	Gatwicka	k1gFnPc2	Gatwicka
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1241	[number]	k4	1241
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
jméno	jméno	k1gNnSc1	jméno
panství	panství	k1gNnSc2	panství
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
současného	současný	k2eAgNnSc2d1	současné
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
dostihová	dostihový	k2eAgFnSc1d1	dostihová
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
konaly	konat	k5eAaImAgInP	konat
dostihové	dostihový	k2eAgInPc1d1	dostihový
závody	závod	k1gInPc1	závod
Aintree	Aintre	k1gFnSc2	Aintre
Grand	grand	k1gMnSc1	grand
National	National	k1gMnSc1	National
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
sem	sem	k6eAd1	sem
byl	být	k5eAaImAgInS	být
přesunut	přesunut	k2eAgInSc4d1	přesunut
Surrey	Surrey	k1gInPc7	Surrey
Aero	aero	k1gNnSc4	aero
Club	club	k1gInSc1	club
a	a	k8xC	a
piloti	pilot	k1gMnPc1	pilot
používali	používat	k5eAaImAgMnP	používat
dostihovou	dostihový	k2eAgFnSc4d1	dostihová
dráhu	dráha	k1gFnSc4	dráha
pro	pro	k7c4	pro
vzlety	vzlet	k1gInPc4	vzlet
a	a	k8xC	a
přistání	přistání	k1gNnSc4	přistání
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
závodiště	závodiště	k1gNnSc2	závodiště
koupil	koupit	k5eAaPmAgMnS	koupit
zahraniční	zahraniční	k2eAgMnSc1d1	zahraniční
investor	investor	k1gMnSc1	investor
a	a	k8xC	a
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
povolil	povolit	k5eAaPmAgMnS	povolit
komerční	komerční	k2eAgInPc4d1	komerční
lety	let	k1gInPc4	let
z	z	k7c2	z
Gatwicku	Gatwick	k1gInSc2	Gatwick
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
odsud	odsud	k6eAd1	odsud
odlétalo	odlétat	k5eAaPmAgNnS	odlétat
i	i	k9	i
několik	několik	k4yIc1	několik
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
linek	linka	k1gFnPc2	linka
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
kruhová	kruhový	k2eAgFnSc1d1	kruhová
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
hala	hala	k1gFnSc1	hala
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
The	The	k1gFnSc1	The
Beehive	Beehiev	k1gFnSc2	Beehiev
(	(	kIx(	(
<g/>
včelín	včelín	k1gInSc1	včelín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
podzemní	podzemní	k2eAgNnSc4d1	podzemní
vlakové	vlakový	k2eAgNnSc4d1	vlakové
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
cestující	cestující	k1gMnPc1	cestující
mohli	moct	k5eAaImAgMnP	moct
cestovat	cestovat	k5eAaImF	cestovat
z	z	k7c2	z
nádraží	nádraží	k1gNnSc2	nádraží
Victoria	Victorium	k1gNnSc2	Victorium
až	až	k8xS	až
k	k	k7c3	k
letadlům	letadlo	k1gNnPc3	letadlo
bez	bez	k7c2	bez
přestupu	přestup	k1gInSc2	přestup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
Gatwick	Gatwick	k1gInSc1	Gatwick
rekonstruován	rekonstruovat	k5eAaBmNgInS	rekonstruovat
jako	jako	k8xC	jako
náhradní	náhradní	k2eAgNnSc1d1	náhradní
letiště	letiště	k1gNnSc1	letiště
za	za	k7c4	za
Heathrow	Heathrow	k1gFnSc4	Heathrow
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
této	tento	k3xDgFnSc2	tento
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
náklady	náklad	k1gInPc4	náklad
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
7,8	[number]	k4	7,8
miliónů	milión	k4xCgInPc2	milión
liber	libra	k1gFnPc2	libra
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Gatwick	Gatwick	k1gInSc1	Gatwick
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1956	[number]	k4	1956
až	až	k6eAd1	až
1958	[number]	k4	1958
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstruovaný	rekonstruovaný	k2eAgInSc1d1	rekonstruovaný
Gatwick	Gatwick	k1gInSc1	Gatwick
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgNnSc7	první
letištěm	letiště	k1gNnSc7	letiště
s	s	k7c7	s
přímým	přímý	k2eAgNnSc7d1	přímé
železničním	železniční	k2eAgNnSc7d1	železniční
spojením	spojení	k1gNnSc7	spojení
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
s	s	k7c7	s
uzavřeným	uzavřený	k2eAgInSc7d1	uzavřený
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
letadlům	letadlo	k1gNnPc3	letadlo
<g/>
;	;	kIx,	;
přístupové	přístupový	k2eAgInPc1d1	přístupový
zastřešené	zastřešený	k2eAgInPc1d1	zastřešený
nástupní	nástupní	k2eAgInPc1d1	nástupní
koridory	koridor	k1gInPc1	koridor
spojují	spojovat	k5eAaImIp3nP	spojovat
odbavovací	odbavovací	k2eAgFnSc4d1	odbavovací
halu	hala	k1gFnSc4	hala
s	s	k7c7	s
nástupními	nástupní	k2eAgInPc7d1	nástupní
můstky	můstek	k1gInPc7	můstek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Gatwick	Gatwick	k6eAd1	Gatwick
je	být	k5eAaImIp3nS	být
nejrušnějším	rušný	k2eAgNnSc7d3	nejrušnější
letištěm	letiště	k1gNnSc7	letiště
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
přistávací	přistávací	k2eAgFnSc7d1	přistávací
drahou	draha	k1gFnSc7	draha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
odbaví	odbavit	k5eAaPmIp3nS	odbavit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
45	[number]	k4	45
miliónů	milión	k4xCgInPc2	milión
pasažérů	pasažér	k1gMnPc2	pasažér
cestujících	cestující	k1gMnPc2	cestující
do	do	k7c2	do
asi	asi	k9	asi
200	[number]	k4	200
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Charterové	charterový	k2eAgInPc4d1	charterový
lety	let	k1gInPc4	let
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
většinou	většinou	k6eAd1	většinou
nemohou	moct	k5eNaImIp3nP	moct
využívat	využívat	k5eAaPmF	využívat
Heathrow	Heathrow	k1gFnSc4	Heathrow
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaPmIp3nP	využívat
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
Gatwick	Gatwick	k1gInSc1	Gatwick
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
spojů	spoj	k1gInPc2	spoj
do	do	k7c2	do
a	a	k8xC	a
z	z	k7c2	z
USA	USA	kA	USA
využívá	využívat	k5eAaPmIp3nS	využívat
Gatwick	Gatwick	k1gInSc4	Gatwick
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Heathrow	Heathrow	k1gFnSc1	Heathrow
nepovoluje	povolovat	k5eNaImIp3nS	povolovat
transatlantické	transatlantický	k2eAgNnSc4d1	transatlantické
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Gatwick	Gatwick	k6eAd1	Gatwick
je	být	k5eAaImIp3nS	být
také	také	k9	také
záložním	záložní	k2eAgNnSc7d1	záložní
letištěm	letiště	k1gNnSc7	letiště
společností	společnost	k1gFnPc2	společnost
British	Britisha	k1gFnPc2	Britisha
Airways	Airways	k1gInSc1	Airways
a	a	k8xC	a
Virgin	Virgin	k1gInSc1	Virgin
Atlantic	Atlantice	k1gFnPc2	Atlantice
Airways	Airwaysa	k1gFnPc2	Airwaysa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
parkoviště	parkoviště	k1gNnSc4	parkoviště
pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
přeplněná	přeplněný	k2eAgNnPc1d1	přeplněné
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
omezení	omezení	k1gNnSc1	omezení
stavebních	stavební	k2eAgFnPc2d1	stavební
prací	práce	k1gFnPc2	práce
dané	daný	k2eAgNnSc1d1	dané
územním	územní	k2eAgInSc7d1	územní
plánem	plán	k1gInSc7	plán
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
letadel	letadlo	k1gNnPc2	letadlo
využívajících	využívající	k2eAgFnPc2d1	využívající
Gatwick	Gatwicka	k1gFnPc2	Gatwicka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budoucnost	budoucnost	k1gFnSc1	budoucnost
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
velké	velký	k2eAgFnSc6d1	velká
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
úřady	úřada	k1gMnPc7	úřada
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
se	se	k3xPyFc4	se
letiště	letiště	k1gNnSc2	letiště
nebude	být	k5eNaImBp3nS	být
rozšiřovat	rozšiřovat	k5eAaImF	rozšiřovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
hlučnosti	hlučnost	k1gFnSc2	hlučnost
<g/>
,	,	kIx,	,
znečištění	znečištění	k1gNnSc2	znečištění
a	a	k8xC	a
nutnosti	nutnost	k1gFnSc2	nutnost
demolice	demolice	k1gFnSc2	demolice
některých	některý	k3yIgFnPc2	některý
okolních	okolní	k2eAgFnPc2d1	okolní
vesnic	vesnice	k1gFnPc2	vesnice
vláda	vláda	k1gFnSc1	vláda
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
místo	místo	k7c2	místo
vybudování	vybudování	k1gNnSc2	vybudování
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
druhé	druhý	k4xOgFnSc2	druhý
přistávací	přistávací	k2eAgFnSc2d1	přistávací
dráhy	dráha	k1gFnSc2	dráha
bude	být	k5eAaImBp3nS	být
podporovat	podporovat	k5eAaImF	podporovat
rozšíření	rozšíření	k1gNnSc4	rozšíření
Heathrow	Heathrow	k1gMnSc3	Heathrow
a	a	k8xC	a
Stanstedu	Stansted	k1gMnSc3	Stansted
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlastník	vlastník	k1gMnSc1	vlastník
letiště	letiště	k1gNnSc2	letiště
Gatwick	Gatwick	k1gMnSc1	Gatwick
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
BAA	BAA	kA	BAA
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
předložila	předložit	k5eAaPmAgFnS	předložit
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
druhé	druhý	k4xOgFnSc2	druhý
přistávací	přistávací	k2eAgFnSc2d1	přistávací
dráhy	dráha	k1gFnSc2	dráha
jižněji	jižně	k6eAd2	jižně
od	od	k7c2	od
letiště	letiště	k1gNnSc2	letiště
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
vesnice	vesnice	k1gFnSc1	vesnice
Charlwood	Charlwood	k1gInSc4	Charlwood
a	a	k8xC	a
Hookwood	Hookwood	k1gInSc4	Hookwood
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
dráhy	dráha	k1gFnSc2	dráha
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
demolice	demolice	k1gFnSc2	demolice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odbavovací	odbavovací	k2eAgFnSc2d1	odbavovací
haly	hala	k1gFnSc2	hala
==	==	k?	==
</s>
</p>
<p>
<s>
Letiště	letiště	k1gNnSc1	letiště
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
odbavovací	odbavovací	k2eAgFnPc4d1	odbavovací
haly	hala	k1gFnPc4	hala
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
automatickým	automatický	k2eAgInSc7d1	automatický
dopravním	dopravní	k2eAgInSc7d1	dopravní
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
(	(	kIx(	(
<g/>
North	North	k1gMnSc1	North
Terminal	Terminal	k1gMnSc1	Terminal
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgFnSc1d3	veliký
stavební	stavební	k2eAgFnSc1d1	stavební
akce	akce	k1gFnSc1	akce
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
v	v	k7c4	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hala	hala	k1gFnSc1	hala
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
(	(	kIx(	(
<g/>
South	South	k1gMnSc1	South
Terminal	Terminal	k1gMnSc1	Terminal
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
byly	být	k5eAaImAgInP	být
vybudovány	vybudován	k2eAgInPc1d1	vybudován
dva	dva	k4xCgInPc1	dva
nové	nový	k2eAgInPc1d1	nový
nástupní	nástupní	k2eAgInPc1d1	nástupní
koridory	koridor	k1gInPc1	koridor
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
původního	původní	k2eAgInSc2d1	původní
nástupního	nástupní	k2eAgInSc2d1	nástupní
koridoru	koridor	k1gInSc2	koridor
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
a	a	k8xC	a
původní	původní	k2eAgFnSc2d1	původní
odbavovací	odbavovací	k2eAgFnSc2d1	odbavovací
haly	hala	k1gFnSc2	hala
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
==	==	k?	==
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
hala	hala	k1gFnSc1	hala
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
železniční	železniční	k2eAgFnSc7d1	železniční
stanicí	stanice	k1gFnSc7	stanice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
pravidelně	pravidelně	k6eAd1	pravidelně
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
vlaky	vlak	k1gInPc1	vlak
do	do	k7c2	do
londýnské	londýnský	k2eAgFnSc2d1	londýnská
stanice	stanice	k1gFnSc2	stanice
Victoria	Victorium	k1gNnSc2	Victorium
nebo	nebo	k8xC	nebo
do	do	k7c2	do
Brightonu	Brighton	k1gInSc2	Brighton
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgNnSc7d3	nejznámější
spojením	spojení	k1gNnSc7	spojení
je	být	k5eAaImIp3nS	být
Gatwick	Gatwick	k1gInSc1	Gatwick
Express	express	k1gInSc1	express
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
i	i	k9	i
jiní	jiný	k2eAgMnPc1d1	jiný
dopravci	dopravce	k1gMnPc1	dopravce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Southern	Southern	k1gInSc1	Southern
<g/>
,	,	kIx,	,
Thameslink	Thameslink	k1gInSc1	Thameslink
a	a	k8xC	a
Virgin	Virgin	k2eAgInSc1d1	Virgin
Trains	Trains	k1gInSc1	Trains
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Thameslink	Thameslink	k1gInSc1	Thameslink
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
přímé	přímý	k2eAgNnSc4d1	přímé
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
letištěm	letiště	k1gNnSc7	letiště
Luton	Lutona	k1gFnPc2	Lutona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
–	–	k?	–
spoje	spoj	k1gInPc1	spoj
X26	X26	k1gFnSc1	X26
Express	express	k1gInSc1	express
Bus	bus	k1gInSc1	bus
z	z	k7c2	z
východního	východní	k2eAgInSc2d1	východní
Croydonu	Croydon	k1gInSc2	Croydon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
linky	linka	k1gFnPc1	linka
společnosti	společnost	k1gFnSc2	společnost
National	National	k1gFnPc2	National
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
spojují	spojovat	k5eAaImIp3nP	spojovat
Gatwick	Gatwick	k1gInSc4	Gatwick
s	s	k7c7	s
Heathrow	Heathrow	k1gFnSc7	Heathrow
i	i	k8xC	i
Stansteadem	Stanstead	k1gInSc7	Stanstead
a	a	k8xC	a
okolními	okolní	k2eAgFnPc7d1	okolní
menšími	malý	k2eAgFnPc7d2	menší
městy	město	k1gNnPc7	město
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Gatwicku	Gatwick	k1gInSc2	Gatwick
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letiště	letiště	k1gNnSc1	letiště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poblíž	poblíž	k7c2	poblíž
odbočky	odbočka	k1gFnSc2	odbočka
z	z	k7c2	z
dálnice	dálnice	k1gFnSc2	dálnice
M	M	kA	M
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
silnice	silnice	k1gFnSc1	silnice
A23	A23	k1gFnSc2	A23
a	a	k8xC	a
jižního	jižní	k2eAgInSc2d1	jižní
konce	konec	k1gInSc2	konec
A	a	k9	a
<g/>
217	[number]	k4	217
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
letiště	letiště	k1gNnSc2	letiště
jsou	být	k5eAaImIp3nP	být
vybudovaná	vybudovaný	k2eAgNnPc4d1	vybudované
parkoviště	parkoviště	k1gNnPc4	parkoviště
(	(	kIx(	(
<g/>
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
často	často	k6eAd1	často
přeplněná	přeplněný	k2eAgFnSc1d1	přeplněná
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
krátkodobé	krátkodobý	k2eAgInPc4d1	krátkodobý
i	i	k8xC	i
dlouhodobé	dlouhodobý	k2eAgNnSc4d1	dlouhodobé
parkování	parkování	k1gNnSc4	parkování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Letiště	letiště	k1gNnSc2	letiště
London	London	k1gMnSc1	London
Gatwick	Gatwick	k1gMnSc1	Gatwick
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
