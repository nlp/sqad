<p>
<s>
Biatlon	biatlon	k1gInSc1	biatlon
je	být	k5eAaImIp3nS	být
zimní	zimní	k2eAgInSc1d1	zimní
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
kombinující	kombinující	k2eAgInSc1d1	kombinující
běh	běh	k1gInSc1	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
a	a	k8xC	a
střelbu	střelba	k1gFnSc4	střelba
z	z	k7c2	z
malorážné	malorážný	k2eAgFnSc2d1	malorážná
pušky	puška	k1gFnSc2	puška
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
letní	letní	k2eAgInSc4d1	letní
biatlon	biatlon	k1gInSc4	biatlon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
běh	běh	k1gInSc1	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
běžný	běžný	k2eAgInSc1d1	běžný
běh	běh	k1gInSc1	běh
<g/>
,	,	kIx,	,
jízda	jízda	k1gFnSc1	jízda
na	na	k7c6	na
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
lyžích	lyže	k1gFnPc6	lyže
apod.	apod.	kA	apod.
Třetím	třetí	k4xOgMnSc7	třetí
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
méně	málo	k6eAd2	málo
obvyklým	obvyklý	k2eAgNnPc3d1	obvyklé
odvětvím	odvětví	k1gNnPc3	odvětví
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
skiarc	skiarc	k1gFnSc4	skiarc
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
lukostřelecký	lukostřelecký	k2eAgInSc1d1	lukostřelecký
biatlon	biatlon	k1gInSc1	biatlon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
střelba	střelba	k1gFnSc1	střelba
z	z	k7c2	z
malorážné	malorážný	k2eAgFnSc2d1	malorážná
zbraně	zbraň	k1gFnSc2	zbraň
zaměněna	zaměnit	k5eAaPmNgFnS	zaměnit
za	za	k7c4	za
střelbu	střelba	k1gFnSc4	střelba
z	z	k7c2	z
luku	luk	k1gInSc2	luk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Biatlon	biatlon	k1gInSc1	biatlon
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
provozuje	provozovat	k5eAaImIp3nS	provozovat
v	v	k7c4	v
asi	asi	k9	asi
dvou	dva	k4xCgFnPc6	dva
desítkách	desítka	k1gFnPc6	desítka
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
světového	světový	k2eAgInSc2d1	světový
žebříčku	žebříček	k1gInSc2	žebříček
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Norové	Nor	k1gMnPc1	Nor
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
aktivních	aktivní	k2eAgMnPc2d1	aktivní
biatlonistů	biatlonista	k1gMnPc2	biatlonista
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
získali	získat	k5eAaPmAgMnP	získat
nejvíce	hodně	k6eAd3	hodně
úspěchů	úspěch	k1gInPc2	úspěch
Ole	Ola	k1gFnSc6	Ola
Einar	Einar	k1gMnSc1	Einar
Bjø	Bjø	k1gMnSc1	Bjø
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Fourcade	Fourcad	k1gInSc5	Fourcad
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Hegle	Hegle	k1gFnSc2	Hegle
Svendsen	Svendsen	k1gInSc1	Svendsen
a	a	k8xC	a
Johannes	Johannes	k1gMnSc1	Johannes
Thingnes	Thingnes	k1gMnSc1	Thingnes
Bø	Bø	k1gMnSc1	Bø
<g/>
,	,	kIx,	,
v	v	k7c6	v
ženách	žena	k1gFnPc6	žena
pak	pak	k6eAd1	pak
Darja	Darja	k1gFnSc1	Darja
Domračevová	Domračevová	k1gFnSc1	Domračevová
<g/>
,	,	kIx,	,
Kaisa	Kaisa	k1gFnSc1	Kaisa
Mäkäräinenová	Mäkäräinenová	k1gFnSc1	Mäkäräinenová
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
Koukalová	Koukalová	k1gFnSc1	Koukalová
a	a	k8xC	a
Laura	Laura	k1gFnSc1	Laura
Dahlmeierová	Dahlmeierová	k1gFnSc1	Dahlmeierová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
biatlonu	biatlon	k1gInSc2	biatlon
==	==	k?	==
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc1	počátek
biatlonu	biatlon	k1gInSc2	biatlon
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
však	však	k9	však
jednalo	jednat	k5eAaImAgNnS	jednat
spíše	spíše	k9	spíše
o	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
<g/>
,	,	kIx,	,
než	než	k8xS	než
o	o	k7c4	o
sportovní	sportovní	k2eAgNnSc4d1	sportovní
vyžití	vyžití	k1gNnSc4	vyžití
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
nyní	nyní	k6eAd1	nyní
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
5000	[number]	k4	5000
lety	léto	k1gNnPc7	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
řeckých	řecký	k2eAgMnPc2d1	řecký
<g/>
,	,	kIx,	,
římských	římský	k2eAgMnPc2d1	římský
a	a	k8xC	a
čínských	čínský	k2eAgFnPc2d1	čínská
provincií	provincie	k1gFnPc2	provincie
lovilo	lovit	k5eAaImAgNnS	lovit
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
lyží	lyže	k1gFnPc2	lyže
a	a	k8xC	a
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
biatlon	biatlon	k1gInSc1	biatlon
využíval	využívat	k5eAaPmAgInS	využívat
především	především	k9	především
na	na	k7c4	na
vojenské	vojenský	k2eAgInPc4d1	vojenský
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Důkazem	důkaz	k1gInSc7	důkaz
toho	ten	k3xDgNnSc2	ten
budiž	budiž	k9	budiž
první	první	k4xOgInSc4	první
závod	závod	k1gInSc4	závod
uskutečněný	uskutečněný	k2eAgInSc4d1	uskutečněný
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
roku	rok	k1gInSc2	rok
1767	[number]	k4	1767
mezi	mezi	k7c7	mezi
tamějšími	tamější	k2eAgMnPc7d1	tamější
strážci	strážce	k1gMnPc7	strážce
hranice	hranice	k1gFnSc2	hranice
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
tohoto	tento	k3xDgInSc2	tento
závodu	závod	k1gInSc2	závod
se	se	k3xPyFc4	se
udržela	udržet	k5eAaPmAgFnS	udržet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
závod	závod	k1gInSc1	závod
"	"	kIx"	"
<g/>
běhu	běh	k1gInSc2	běh
vojenských	vojenský	k2eAgFnPc2d1	vojenská
hlídek	hlídka	k1gFnPc2	hlídka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
biatlonu	biatlon	k1gInSc2	biatlon
<g/>
)	)	kIx)	)
mimo	mimo	k7c4	mimo
Norsko	Norsko	k1gNnSc4	Norsko
byl	být	k5eAaImAgInS	být
uskutečněn	uskutečněn	k2eAgInSc1d1	uskutečněn
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Běh	běh	k1gInSc1	běh
vojenských	vojenský	k2eAgFnPc2d1	vojenská
hlídek	hlídka	k1gFnPc2	hlídka
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
zařazen	zařadit	k5eAaPmNgMnS	zařadit
na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
jako	jako	k8xS	jako
ukázková	ukázkový	k2eAgFnSc1d1	ukázková
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
byla	být	k5eAaImAgNnP	být
sepsána	sepsán	k2eAgNnPc1d1	sepsáno
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
pojem	pojem	k1gInSc1	pojem
biatlon	biatlon	k1gInSc1	biatlon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
vzala	vzít	k5eAaPmAgFnS	vzít
pod	pod	k7c4	pod
svá	svůj	k3xOyFgNnPc4	svůj
křídla	křídlo	k1gNnPc4	křídlo
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
unie	unie	k1gFnSc2	unie
moderního	moderní	k2eAgInSc2d1	moderní
pětiboje	pětiboj	k1gInSc2	pětiboj
(	(	kIx(	(
<g/>
UIPM	UIPM	kA	UIPM
<g/>
)	)	kIx)	)
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
UIPMB	UIPMB	kA	UIPMB
–	–	k?	–
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
unie	unie	k1gFnSc2	unie
moderního	moderní	k2eAgInSc2d1	moderní
pětiboje	pětiboj	k1gInSc2	pětiboj
a	a	k8xC	a
biatlonu	biatlon	k1gInSc2	biatlon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nic	nic	k3yNnSc1	nic
tak	tak	k9	tak
již	již	k6eAd1	již
nebránilo	bránit	k5eNaImAgNnS	bránit
uspořádání	uspořádání	k1gNnSc1	uspořádání
prvního	první	k4xOgNnSc2	první
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
v	v	k7c6	v
Saalfeldenu	Saalfelden	k1gInSc6	Saalfelden
(	(	kIx(	(
<g/>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
25	[number]	k4	25
sportovců	sportovec	k1gMnPc2	sportovec
ze	z	k7c2	z
7	[number]	k4	7
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
20	[number]	k4	20
km	km	kA	km
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
byl	být	k5eAaImAgMnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
střelbou	střelba	k1gFnSc7	střelba
z	z	k7c2	z
"	"	kIx"	"
<g/>
velkorážky	velkorážka	k1gFnSc2	velkorážka
<g/>
"	"	kIx"	"
na	na	k7c4	na
papírové	papírový	k2eAgInPc4d1	papírový
terče	terč	k1gInPc4	terč
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
různých	různý	k2eAgFnPc2d1	různá
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
–	–	k?	–
250	[number]	k4	250
m	m	kA	m
<g/>
,	,	kIx,	,
200	[number]	k4	200
m	m	kA	m
a	a	k8xC	a
150	[number]	k4	150
m	m	kA	m
v	v	k7c6	v
leže	leže	k6eAd1	leže
a	a	k8xC	a
100	[number]	k4	100
m	m	kA	m
ve	v	k7c4	v
stoje	stoj	k1gInPc4	stoj
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
netrefenou	trefený	k2eNgFnSc4d1	netrefená
ránu	rána	k1gFnSc4	rána
se	se	k3xPyFc4	se
připočítávala	připočítávat	k5eAaImAgFnS	připočítávat
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Závodník	Závodník	k1gMnSc1	Závodník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
ani	ani	k9	ani
jeden	jeden	k4xCgInSc4	jeden
terč	terč	k1gInSc4	terč
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
mohl	moct	k5eAaImAgMnS	moct
dostat	dostat	k5eAaPmF	dostat
penalizaci	penalizace	k1gFnSc4	penalizace
40	[number]	k4	40
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
malou	malý	k2eAgFnSc4d1	malá
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
MS	MS	kA	MS
se	se	k3xPyFc4	se
biatlon	biatlon	k1gInSc1	biatlon
stával	stávat	k5eAaImAgInS	stávat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podkládá	podkládat	k5eAaImIp3nS	podkládat
zařazení	zařazení	k1gNnSc4	zařazení
do	do	k7c2	do
olympijského	olympijský	k2eAgInSc2d1	olympijský
programu	program	k1gInSc2	program
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Squaw	squaw	k1gFnSc6	squaw
Valley	Vallea	k1gFnSc2	Vallea
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
jelo	jet	k5eAaImAgNnS	jet
20	[number]	k4	20
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
se	se	k3xPyFc4	se
biatlon	biatlon	k1gInSc1	biatlon
stále	stále	k6eAd1	stále
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
a	a	k8xC	a
z	z	k7c2	z
programu	program	k1gInSc2	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
již	již	k6eAd1	již
nevypadl	vypadnout	k5eNaPmAgMnS	vypadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Biatlon	biatlon	k1gInSc1	biatlon
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
stával	stávat	k5eAaImAgInS	stávat
atraktivnější	atraktivní	k2eAgInSc1d2	atraktivnější
pro	pro	k7c4	pro
oko	oko	k1gNnSc4	oko
diváka	divák	k1gMnSc2	divák
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
přispívalo	přispívat	k5eAaImAgNnS	přispívat
neustálé	neustálý	k2eAgNnSc1d1	neustálé
zmenšování	zmenšování	k1gNnSc1	zmenšování
terčů	terč	k1gInPc2	terč
a	a	k8xC	a
přidávání	přidávání	k1gNnSc2	přidávání
atraktivnějších	atraktivní	k2eAgInPc2d2	atraktivnější
závodů	závod	k1gInPc2	závod
(	(	kIx(	(
<g/>
hromadný	hromadný	k2eAgInSc4d1	hromadný
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
stíhací	stíhací	k2eAgInSc4d1	stíhací
závod	závod	k1gInSc4	závod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
do	do	k7c2	do
programu	program	k1gInSc2	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
program	program	k1gInSc4	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
zařazen	zařadit	k5eAaPmNgInS	zařadit
i	i	k9	i
závod	závod	k1gInSc1	závod
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
závod	závod	k1gInSc1	závod
žen	žena	k1gFnPc2	žena
byl	být	k5eAaImAgInS	být
uspořádán	uspořádat	k5eAaPmNgInS	uspořádat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
<g/>
)	)	kIx)	)
a	a	k8xC	a
bruslařský	bruslařský	k2eAgInSc4d1	bruslařský
styl	styl	k1gInSc4	styl
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
přinesl	přinést	k5eAaPmAgInS	přinést
do	do	k7c2	do
závodů	závod	k1gInPc2	závod
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
nebránilo	bránit	k5eNaImAgNnS	bránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
především	především	k6eAd1	především
televizní	televizní	k2eAgInPc4d1	televizní
přenosy	přenos	k1gInPc4	přenos
zpopularizovaly	zpopularizovat	k5eAaPmAgFnP	zpopularizovat
tento	tento	k3xDgInSc4	tento
sport	sport	k1gInSc4	sport
až	až	k9	až
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Biatlon	biatlon	k1gInSc1	biatlon
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
klasického	klasický	k2eAgNnSc2d1	klasické
lyžování	lyžování	k1gNnSc2	lyžování
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
z	z	k7c2	z
klasického	klasický	k2eAgNnSc2d1	klasické
lyžování	lyžování	k1gNnSc2	lyžování
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
také	také	k9	také
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc1d1	vlastní
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Disciplíny	disciplína	k1gFnSc2	disciplína
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vytrvalostní	vytrvalostní	k2eAgInSc4d1	vytrvalostní
závod	závod	k1gInSc4	závod
(	(	kIx(	(
<g/>
tradičně	tradičně	k6eAd1	tradičně
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
individuální	individuální	k2eAgInSc1d1	individuální
závod	závod	k1gInSc1	závod
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
závod	závod	k1gInSc4	závod
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
tradicí	tradice	k1gFnSc7	tradice
<g/>
,	,	kIx,	,
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
od	od	k7c2	od
1958	[number]	k4	1958
a	a	k8xC	a
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
na	na	k7c4	na
1984	[number]	k4	1984
MS	MS	kA	MS
a	a	k8xC	a
OH	OH	kA	OH
1992	[number]	k4	1992
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muži	muž	k1gMnPc1	muž
jedou	jet	k5eAaImIp3nP	jet
20	[number]	k4	20
km	km	kA	km
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
15	[number]	k4	15
km	km	kA	km
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
startují	startovat	k5eAaBmIp3nP	startovat
v	v	k7c6	v
třicetisekundových	třicetisekundový	k2eAgInPc6d1	třicetisekundový
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jede	jet	k5eAaImIp3nS	jet
se	s	k7c7	s
5	[number]	k4	5
okruhů	okruh	k1gInPc2	okruh
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
střílí	střílet	k5eAaImIp3nS	střílet
L	L	kA	L
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
S	s	k7c7	s
(	(	kIx(	(
<g/>
L	L	kA	L
–	–	k?	–
v	v	k7c6	v
leže	leže	k6eAd1	leže
<g/>
,	,	kIx,	,
S	s	k7c7	s
–	–	k?	–
ve	v	k7c6	v
stoje	stoje	k6eAd1	stoje
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
netrefenou	trefený	k2eNgFnSc4d1	netrefená
ránu	rána	k1gFnSc4	rána
je	být	k5eAaImIp3nS	být
přirážka	přirážka	k1gFnSc1	přirážka
1	[number]	k4	1
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
přičte	přičíst	k5eAaPmIp3nS	přičíst
k	k	k7c3	k
výslednému	výsledný	k2eAgInSc3d1	výsledný
běžeckému	běžecký	k2eAgInSc3d1	běžecký
času	čas	k1gInSc3	čas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sprint	sprint	k1gInSc4	sprint
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
od	od	k7c2	od
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
od	od	k7c2	od
1980	[number]	k4	1980
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
od	od	k7c2	od
1984	[number]	k4	1984
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
a	a	k8xC	a
1992	[number]	k4	1992
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muži	muž	k1gMnPc1	muž
jedou	jet	k5eAaImIp3nP	jet
10	[number]	k4	10
km	km	kA	km
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
7,5	[number]	k4	7,5
km	km	kA	km
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
opět	opět	k6eAd1	opět
startují	startovat	k5eAaBmIp3nP	startovat
v	v	k7c6	v
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedou	jet	k5eAaImIp3nP	jet
se	se	k3xPyFc4	se
3	[number]	k4	3
okruhy	okruh	k1gInPc7	okruh
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
střílí	střílet	k5eAaImIp3nS	střílet
pouze	pouze	k6eAd1	pouze
jednou	jednou	k9	jednou
L	L	kA	L
a	a	k8xC	a
jednou	jednou	k6eAd1	jednou
S.	S.	kA	S.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
netrefenou	trefený	k2eNgFnSc4d1	netrefená
ránu	rána	k1gFnSc4	rána
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
proběhnout	proběhnout	k5eAaPmF	proběhnout
navíc	navíc	k6eAd1	navíc
jedno	jeden	k4xCgNnSc1	jeden
trestné	trestný	k2eAgNnSc1d1	trestné
kolo	kolo	k1gNnSc1	kolo
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
<g/>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
<g/>
výsledek	výsledek	k1gInSc1	výsledek
závodu	závod	k1gInSc2	závod
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc1	vliv
na	na	k7c6	na
pořadí	pořadí	k1gNnSc6	pořadí
ve	v	k7c6	v
stíhacím	stíhací	k2eAgInSc6d1	stíhací
závodu	závod	k1gInSc6	závod
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stíhací	stíhací	k2eAgInSc4d1	stíhací
závod	závod	k1gInSc4	závod
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
od	od	k7c2	od
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
od	od	k7c2	od
2002	[number]	k4	2002
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
i	i	k8xC	i
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muži	muž	k1gMnPc1	muž
jedou	jet	k5eAaImIp3nP	jet
12,5	[number]	k4	12,5
km	km	kA	km
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
10	[number]	k4	10
km	km	kA	km
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
startují	startovat	k5eAaBmIp3nP	startovat
tzv.	tzv.	kA	tzv.
Gundersenovou	Gundersenový	k2eAgFnSc7d1	Gundersenový
metodou	metoda	k1gFnSc7	metoda
–	–	k?	–
startují	startovat	k5eAaBmIp3nP	startovat
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
daném	daný	k2eAgNnSc6d1	dané
výsledkovou	výsledkový	k2eAgFnSc7d1	výsledková
listinou	listina	k1gFnSc7	listina
předchozího	předchozí	k2eAgInSc2d1	předchozí
závodu	závod	k1gInSc2	závod
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
časovou	časový	k2eAgFnSc7d1	časová
ztrátou	ztráta	k1gFnSc7	ztráta
na	na	k7c4	na
prvního	první	k4xOgMnSc4	první
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yRgFnSc7	jaký
dojeli	dojet	k5eAaPmAgMnP	dojet
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
platí	platit	k5eAaImIp3nS	platit
v	v	k7c6	v
obvyklém	obvyklý	k2eAgInSc6d1	obvyklý
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
předchozím	předchozí	k2eAgInSc7d1	předchozí
závodem	závod	k1gInSc7	závod
sprint	sprint	k1gInSc1	sprint
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgInS	být
předchozím	předchozí	k2eAgInSc7d1	předchozí
závodem	závod	k1gInSc7	závod
vytrvalostní	vytrvalostní	k2eAgInSc1d1	vytrvalostní
závod	závod	k1gInSc1	závod
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
časový	časový	k2eAgInSc4d1	časový
odstup	odstup	k1gInSc4	odstup
poloviční	poloviční	k2eAgInSc4d1	poloviční
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pěti	pět	k4xCc6	pět
okruzích	okruh	k1gInPc6	okruh
se	s	k7c7	s
střílí	stříle	k1gFnSc7	stříle
L	L	kA	L
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
S.	S.	kA	S.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
netrefenou	trefený	k2eNgFnSc4d1	netrefená
ránu	rána	k1gFnSc4	rána
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
proběhnou	proběhnout	k5eAaPmIp3nP	proběhnout
navíc	navíc	k6eAd1	navíc
jedno	jeden	k4xCgNnSc1	jeden
trestné	trestný	k2eAgNnSc1d1	trestné
kolo	kolo	k1gNnSc1	kolo
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Závod	závod	k1gInSc4	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
a	a	k8xC	a
od	od	k7c2	od
2006	[number]	k4	2006
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
i	i	k8xC	i
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Muži	muž	k1gMnPc1	muž
jedou	jet	k5eAaImIp3nP	jet
15	[number]	k4	15
km	km	kA	km
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
12,5	[number]	k4	12,5
km	km	kA	km
<g/>
;	;	kIx,	;
startují	startovat	k5eAaBmIp3nP	startovat
hromadně	hromadně	k6eAd1	hromadně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pěti	pět	k4xCc6	pět
okruzích	okruh	k1gInPc6	okruh
se	s	k7c7	s
střílí	stříle	k1gFnSc7	stříle
L	L	kA	L
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
S.	S.	kA	S.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
netrefenou	trefený	k2eNgFnSc4d1	netrefená
ránu	rána	k1gFnSc4	rána
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
proběhnou	proběhnout	k5eAaPmIp3nP	proběhnout
navíc	navíc	k6eAd1	navíc
jedno	jeden	k4xCgNnSc1	jeden
trestné	trestný	k2eAgNnSc1d1	trestné
kolo	kolo	k1gNnSc1	kolo
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
závodě	závod	k1gInSc6	závod
startuje	startovat	k5eAaBmIp3nS	startovat
25	[number]	k4	25
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
závodníků	závodník	k1gMnPc2	závodník
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
5	[number]	k4	5
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
nejúspěšnější	úspěšný	k2eAgMnPc1d3	nejúspěšnější
v	v	k7c6	v
předchozích	předchozí	k2eAgInPc6d1	předchozí
závodech	závod	k1gInPc6	závod
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
podniku	podnik	k1gInSc2	podnik
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
startuje	startovat	k5eAaBmIp3nS	startovat
15	[number]	k4	15
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
závodníků	závodník	k1gMnPc2	závodník
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
medailisté	medailista	k1gMnPc1	medailista
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
;	;	kIx,	;
tyto	tento	k3xDgMnPc4	tento
biatlonisty	biatlonista	k1gMnPc4	biatlonista
pak	pak	k6eAd1	pak
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
30	[number]	k4	30
doplní	doplnit	k5eAaPmIp3nS	doplnit
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
závodníci	závodník	k1gMnPc1	závodník
tohoto	tento	k3xDgNnSc2	tento
mistrovství	mistrovství	k1gNnSc2	mistrovství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Štafeta	štafeta	k1gFnSc1	štafeta
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
od	od	k7c2	od
1968	[number]	k4	1968
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
a	a	k8xC	a
od	od	k7c2	od
1984	[number]	k4	1984
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
a	a	k8xC	a
1992	[number]	k4	1992
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každou	každý	k3xTgFnSc4	každý
zemi	zem	k1gFnSc4	zem
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
jedna	jeden	k4xCgFnSc1	jeden
čtyřčlenná	čtyřčlenný	k2eAgFnSc1d1	čtyřčlenná
štafeta	štafeta	k1gFnSc1	štafeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
běží	běžet	k5eAaImIp3nP	běžet
7,5	[number]	k4	7,5
km	km	kA	km
(	(	kIx(	(
<g/>
muži	muž	k1gMnSc6	muž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
6	[number]	k4	6
km	km	kA	km
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
i	i	k8xC	i
smíšená	smíšený	k2eAgFnSc1d1	smíšená
2	[number]	k4	2
ženy	žena	k1gFnPc1	žena
(	(	kIx(	(
<g/>
6	[number]	k4	6
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
muži	muž	k1gMnPc7	muž
(	(	kIx(	(
<g/>
7,5	[number]	k4	7,5
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
okruhy	okruh	k1gInPc1	okruh
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
střílí	střílet	k5eAaImIp3nS	střílet
L	L	kA	L
<g/>
,	,	kIx,	,
S.	S.	kA	S.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
závodník	závodník	k1gMnSc1	závodník
netrefí	trefit	k5eNaPmIp3nS	trefit
všech	všecek	k3xTgInPc2	všecek
pět	pět	k4xCc4	pět
terčů	terč	k1gInPc2	terč
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
disposici	disposice	k1gFnSc3	disposice
3	[number]	k4	3
náhradní	náhradní	k2eAgInPc1d1	náhradní
náboje	náboj	k1gInPc1	náboj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
musí	muset	k5eAaImIp3nP	muset
po	po	k7c6	po
jednom	jeden	k4xCgNnSc6	jeden
dobít	dobít	k5eAaPmF	dobít
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
pokud	pokud	k8xS	pokud
netrefí	trefit	k5eNaPmIp3nS	trefit
ani	ani	k8xC	ani
po	po	k7c6	po
dobití	dobití	k1gNnSc6	dobití
<g/>
,	,	kIx,	,
jede	jet	k5eAaImIp3nS	jet
navíc	navíc	k6eAd1	navíc
tolik	tolik	k4yIc4	tolik
trestných	trestný	k2eAgNnPc2d1	trestné
kol	kolo	k1gNnPc2	kolo
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
netrefil	trefit	k5eNaPmAgMnS	trefit
terčů	terč	k1gInPc2	terč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Závod	závod	k1gInSc1	závod
smíšených	smíšený	k2eAgFnPc2d1	smíšená
dvojic	dvojice	k1gFnPc2	dvojice
===	===	k?	===
</s>
</p>
<p>
<s>
Štafeta	štafeta	k1gFnSc1	štafeta
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
ženy	žena	k1gFnSc2	žena
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgInPc4	čtyři
úseky	úsek	k1gInPc4	úsek
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
závodníci	závodník	k1gMnPc1	závodník
střídají	střídat	k5eAaImIp3nP	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Start	start	k1gInSc1	start
je	být	k5eAaImIp3nS	být
hromadný	hromadný	k2eAgInSc1d1	hromadný
<g/>
,	,	kIx,	,
začínají	začínat	k5eAaImIp3nP	začínat
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
běží	běžet	k5eAaImIp3nP	běžet
na	na	k7c6	na
prvním	první	k4xOgMnSc6	první
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
úseku	úsek	k1gInSc6	úsek
štafety	štafeta	k1gFnSc2	štafeta
dva	dva	k4xCgInPc4	dva
okruhy	okruh	k1gInPc4	okruh
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
1,5	[number]	k4	1,5
km	km	kA	km
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
6	[number]	k4	6
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
běží	běžet	k5eAaImIp3nP	běžet
druhý	druhý	k4xOgInSc4	druhý
úsek	úsek	k1gInSc4	úsek
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
dva	dva	k4xCgInPc1	dva
okruhy	okruh	k1gInPc1	okruh
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
úsek	úsek	k1gInSc1	úsek
štafety	štafeta	k1gFnSc2	štafeta
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
tři	tři	k4xCgInPc1	tři
okruhy	okruh	k1gInPc1	okruh
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
7,5	[number]	k4	7,5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
okruhu	okruh	k1gInSc6	okruh
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
posledního	poslední	k2eAgMnSc2d1	poslední
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
běží	běžet	k5eAaImIp3nS	běžet
muži	muž	k1gMnPc1	muž
<g/>
)	)	kIx)	)
probíhá	probíhat	k5eAaImIp3nS	probíhat
jedna	jeden	k4xCgFnSc1	jeden
střelba	střelba	k1gFnSc1	střelba
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
úseku	úsek	k1gInSc2	úsek
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
střelbou	střelba	k1gFnSc7	střelba
vleže	vleže	k6eAd1	vleže
a	a	k8xC	a
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
úseku	úsek	k1gInSc2	úsek
se	se	k3xPyFc4	se
střílí	střílet	k5eAaImIp3nS	střílet
ve	v	k7c4	v
stoje	stoj	k1gInPc4	stoj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
střelbě	střelba	k1gFnSc6	střelba
lze	lze	k6eAd1	lze
dobíjet	dobíjet	k5eAaImF	dobíjet
až	až	k9	až
tři	tři	k4xCgInPc1	tři
náboje	náboj	k1gInPc1	náboj
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
položce	položka	k1gFnSc6	položka
<g/>
,	,	kIx,	,
za	za	k7c4	za
nesestřelený	sestřelený	k2eNgInSc4d1	sestřelený
terč	terč	k1gInSc4	terč
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
absolvovat	absolvovat	k5eAaPmF	absolvovat
trestné	trestný	k2eAgNnSc4d1	trestné
kolo	kolo	k1gNnSc4	kolo
s	s	k7c7	s
poloviční	poloviční	k2eAgFnSc7d1	poloviční
délkou	délka	k1gFnSc7	délka
oproti	oproti	k7c3	oproti
stíhacímu	stíhací	k2eAgInSc3d1	stíhací
závodu	závod	k1gInSc3	závod
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
75	[number]	k4	75
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Účastníci	účastník	k1gMnPc1	účastník
závodu	závod	k1gInSc2	závod
smíšených	smíšený	k2eAgFnPc2d1	smíšená
dvojic	dvojice	k1gFnPc2	dvojice
nesmí	smět	k5eNaImIp3nS	smět
tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
startovat	startovat	k5eAaBmF	startovat
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
štafetovém	štafetový	k2eAgInSc6d1	štafetový
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Materiálové	materiálový	k2eAgNnSc4d1	materiálové
vybavení	vybavení	k1gNnSc4	vybavení
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Biatlonová	biatlonový	k2eAgFnSc1d1	biatlonová
puška	puška	k1gFnSc1	puška
-	-	kIx~	-
malorážka	malorážka	k1gFnSc1	malorážka
===	===	k?	===
</s>
</p>
<p>
<s>
Ráže	ráže	k1gFnSc1	ráže
hlavně	hlavně	k9	hlavně
je	být	k5eAaImIp3nS	být
5,6	[number]	k4	5,6
mm	mm	kA	mm
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
.22	.22	k4	.22
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
long	long	k1gMnSc1	long
rifle	rifle	k1gFnPc1	rifle
</s>
</p>
<p>
<s>
Odpor	odpor	k1gInSc1	odpor
spouště	spoušť	k1gFnSc2	spoušť
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
minimálně	minimálně	k6eAd1	minimálně
0,5	[number]	k4	0,5
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Puška	puška	k1gFnSc1	puška
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
zásobníků	zásobník	k1gInPc2	zásobník
a	a	k8xC	a
střeliva	střelivo	k1gNnSc2	střelivo
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgFnSc4d1	minimální
hmotnost	hmotnost	k1gFnSc4	hmotnost
3,5	[number]	k4	3,5
kg	kg	kA	kg
</s>
</p>
<p>
<s>
===	===	k?	===
Terčové	terčový	k2eAgNnSc4d1	terčové
zařízení	zařízení	k1gNnSc4	zařízení
===	===	k?	===
</s>
</p>
<p>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
mezi	mezi	k7c7	mezi
přední	přední	k2eAgFnSc7d1	přední
hranou	hrana	k1gFnSc7	hrana
střelecké	střelecký	k2eAgFnSc2d1	střelecká
rampy	rampa	k1gFnSc2	rampa
a	a	k8xC	a
lícem	líc	k1gInSc7	líc
terčů	terč	k1gInPc2	terč
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
m	m	kA	m
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
1	[number]	k4	1
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměry	průměr	k1gInPc1	průměr
černých	černý	k2eAgInPc2d1	černý
kruhů	kruh	k1gInPc2	kruh
pro	pro	k7c4	pro
míření	míření	k1gNnSc4	míření
a	a	k8xC	a
zásahy	zásah	k1gInPc4	zásah
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
poloha	poloha	k1gFnSc1	poloha
vstoje	vstoje	k6eAd1	vstoje
</s>
</p>
<p>
<s>
záměrný	záměrný	k2eAgInSc1d1	záměrný
kruh	kruh	k1gInSc1	kruh
115	[number]	k4	115
mm	mm	kA	mm
</s>
</p>
<p>
<s>
kruh	kruh	k1gInSc1	kruh
pro	pro	k7c4	pro
zásah	zásah	k1gInSc4	zásah
115	[number]	k4	115
mm	mm	kA	mm
(	(	kIx(	(
<g/>
na	na	k7c6	na
papírovém	papírový	k2eAgInSc6d1	papírový
terči	terč	k1gInSc6	terč
110	[number]	k4	110
mm	mm	kA	mm
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
poloha	poloha	k1gFnSc1	poloha
vleže	vleže	k6eAd1	vleže
</s>
</p>
<p>
<s>
záměrný	záměrný	k2eAgInSc1d1	záměrný
kruh	kruh	k1gInSc1	kruh
115	[number]	k4	115
mm	mm	kA	mm
</s>
</p>
<p>
<s>
kruh	kruh	k1gInSc1	kruh
pro	pro	k7c4	pro
zásah	zásah	k1gInSc4	zásah
45	[number]	k4	45
mm	mm	kA	mm
(	(	kIx(	(
<g/>
na	na	k7c6	na
papírovém	papírový	k2eAgInSc6d1	papírový
terči	terč	k1gInSc6	terč
40	[number]	k4	40
mm	mm	kA	mm
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
olympijských	olympijský	k2eAgMnPc2d1	olympijský
medailistů	medailista	k1gMnPc2	medailista
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
</s>
</p>
<p>
<s>
Biatlon	biatlon	k1gInSc1	biatlon
na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
biatlonu	biatlon	k1gInSc6	biatlon
</s>
</p>
<p>
<s>
IBU	IBU	kA	IBU
Cup	cup	k1gInSc1	cup
</s>
</p>
<p>
<s>
Biatlon	biatlon	k1gInSc1	biatlon
na	na	k7c6	na
Zimní	zimní	k2eAgFnSc6d1	zimní
univerziádě	univerziáda	k1gFnSc6	univerziáda
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
biatlon	biatlon	k1gInSc1	biatlon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
biatlon	biatlon	k1gInSc1	biatlon
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
KB	kb	kA	kb
Jilemnice	Jilemnice	k1gFnSc1	Jilemnice
</s>
</p>
<p>
<s>
OEZ	OEZ	kA	OEZ
Biatlon	biatlon	k1gInSc1	biatlon
</s>
</p>
<p>
<s>
SKP	SKP	kA	SKP
Jablonex	jablonex	k1gInSc1	jablonex
</s>
</p>
<p>
<s>
BK	BK	kA	BK
Elán	elán	k1gInSc1	elán
Zruč	Zruč	k1gFnSc1	Zruč
(	(	kIx(	(
<g/>
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KB	kb	kA	kb
Střelka	střelka	k1gFnSc1	střelka
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
SK	Sk	kA	Sk
Rover	rover	k1gMnSc1	rover
-	-	kIx~	-
biatlon	biatlon	k1gInSc1	biatlon
</s>
</p>
<p>
<s>
BK	BK	kA	BK
Beroun	Beroun	k1gInSc1	Beroun
</s>
</p>
<p>
<s>
KB	kb	kA	kb
Kapslovna	kapslovna	k1gFnSc1	kapslovna
</s>
</p>
<p>
<s>
KB	kb	kA	kb
Jílové	Jílové	k1gNnSc1	Jílové
</s>
</p>
<p>
<s>
KB	kb	kA	kb
Trefa	trefa	k1gFnSc1	trefa
</s>
</p>
