<s>
oblast	oblast	k1gFnSc1	oblast
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
únorajul	únorajout	k5eAaPmAgInS	únorajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1852	[number]	k4	1852
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
ruského	ruský	k2eAgInSc2d1	ruský
romantismu	romantismus	k1gInSc2	romantismus
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
silnými	silný	k2eAgInPc7d1	silný
prvky	prvek	k1gInPc7	prvek
kritického	kritický	k2eAgInSc2d1	kritický
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
za	za	k7c4	za
jehož	jenž	k3xRgMnSc4	jenž
zakladatele	zakladatel	k1gMnSc4	zakladatel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
literatuře	literatura	k1gFnSc6	literatura
považován	považován	k2eAgInSc1d1	považován
<g/>
.	.	kIx.	.
</s>
