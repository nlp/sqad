<s>
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
filmový	filmový	k2eAgInSc1d1	filmový
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Podskalský	podskalský	k2eAgMnSc1d1	podskalský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
na	na	k7c4	na
námět	námět	k1gInSc4	námět
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc4	film
vyrobilo	vyrobit	k5eAaPmAgNnS	vyrobit
Filmové	filmový	k2eAgNnSc1d1	filmové
studio	studio	k1gNnSc1	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
obsazení	obsazení	k1gNnSc2	obsazení
filmu	film	k1gInSc6	film
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
normalizace	normalizace	k1gFnSc1	normalizace
<g/>
,	,	kIx,	,
nesměli	smět	k5eNaImAgMnP	smět
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
již	již	k6eAd1	již
objevit	objevit	k5eAaPmF	objevit
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
(	(	kIx(	(
<g/>
purkrabí	purkrabí	k1gMnSc1	purkrabí
<g/>
)	)	kIx)	)
či	či	k8xC	či
Jiřina	Jiřina	k1gFnSc1	Jiřina
Jirásková	Jirásková	k1gFnSc1	Jirásková
(	(	kIx(	(
<g/>
paní	paní	k1gFnSc1	paní
Ofka	Ofka	k1gMnSc1	Ofka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgInSc7d1	poslední
snímkem	snímek	k1gInSc7	snímek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgMnSc6	který
hrál	hrát	k5eAaImAgMnS	hrát
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Marvan	Marvan	k1gMnSc1	Marvan
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
filmu	film	k1gInSc2	film
pocházejí	pocházet	k5eAaImIp3nP	pocházet
některé	některý	k3yIgInPc4	některý
velmi	velmi	k6eAd1	velmi
známé	známý	k2eAgFnPc1d1	známá
písně	píseň	k1gFnPc1	píseň
skladatele	skladatel	k1gMnSc2	skladatel
Karla	Karel	k1gMnSc2	Karel
Svobody	Svoboda	k1gMnSc2	Svoboda
a	a	k8xC	a
textaře	textař	k1gMnSc2	textař
Jiřího	Jiří	k1gMnSc2	Jiří
Štaidla	Štaidlo	k1gNnSc2	Štaidlo
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
Waldemara	Waldemar	k1gMnSc2	Waldemar
Matušky	Matuška	k1gMnSc2	Matuška
a	a	k8xC	a
Heleny	Helena	k1gFnSc2	Helena
Vondráčkové	Vondráčková	k1gFnSc2	Vondráčková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
oslav	oslava	k1gFnPc2	oslava
40	[number]	k4	40
výročí	výročí	k1gNnSc3	výročí
natočení	natočení	k1gNnSc2	natočení
filmu	film	k1gInSc2	film
se	s	k7c7	s
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
hradu	hrad	k1gInSc2	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
koncert	koncert	k1gInSc1	koncert
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
připomněl	připomnět	k5eAaPmAgMnS	připomnět
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
muzikálu	muzikál	k1gInSc2	muzikál
v	v	k7c6	v
provedení	provedení	k1gNnSc6	provedení
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
Janise	Janise	k1gFnSc2	Janise
Sidovského	Sidovský	k1gMnSc2	Sidovský
zde	zde	k6eAd1	zde
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Dyk	Dyk	k?	Dyk
<g/>
,	,	kIx,	,
Matěj	Matěj	k1gMnSc1	Matěj
Ruppert	Ruppert	k1gMnSc1	Ruppert
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Stivínová	Stivínová	k1gFnSc1	Stivínová
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Budař	Budař	k1gMnSc1	Budař
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zpívali	zpívat	k5eAaImAgMnP	zpívat
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
Českého	český	k2eAgInSc2d1	český
národního	národní	k2eAgInSc2d1	národní
symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc4	záznam
natočila	natočit	k5eAaBmAgFnS	natočit
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
hrad	hrad	k1gInSc1	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
nebudou	být	k5eNaImBp3nP	být
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
žádné	žádný	k3yNgFnPc4	žádný
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
věnovat	věnovat	k5eAaImF	věnovat
mužským	mužský	k2eAgFnPc3d1	mužská
záležitostem	záležitost	k1gFnPc3	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákaz	zákaz	k1gInSc1	zákaz
však	však	k9	však
naráz	naráz	k6eAd1	naráz
poruší	porušit	k5eAaPmIp3nS	porušit
mladá	mladý	k2eAgFnSc1d1	mladá
dívka	dívka	k1gFnSc1	dívka
Alena	Alena	k1gFnSc1	Alena
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
Eliška	Eliška	k1gFnSc1	Eliška
Pomořanská	pomořanský	k2eAgFnSc1d1	Pomořanská
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
pobytu	pobyt	k1gInSc2	pobyt
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
filmu	film	k1gInSc2	film
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
,	,	kIx,	,
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
císařem	císař	k1gMnSc7	císař
Karlem	Karel	k1gMnSc7	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
vstoupit	vstoupit	k5eAaPmF	vstoupit
žádná	žádný	k3yNgFnSc1	žádný
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
císařovny	císařovna	k1gFnSc2	císařovna
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
příkaz	příkaz	k1gInSc1	příkaz
měl	mít	k5eAaImAgInS	mít
posloužit	posloužit	k5eAaPmF	posloužit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
mohl	moct	k5eAaImAgMnS	moct
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
věnovat	věnovat	k5eAaPmF	věnovat
mužským	mužský	k2eAgFnPc3d1	mužská
záležitostem	záležitost	k1gFnPc3	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákaz	zákaz	k1gInSc1	zákaz
však	však	k9	však
byl	být	k5eAaImAgInS	být
jedné	jeden	k4xCgFnSc2	jeden
noci	noc	k1gFnSc2	noc
porušen	porušen	k2eAgInSc4d1	porušen
hned	hned	k6eAd1	hned
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
se	se	k3xPyFc4	se
ocitly	ocitnout	k5eAaPmAgFnP	ocitnout
dvě	dva	k4xCgFnPc1	dva
ženy	žena	k1gFnPc1	žena
–	–	k?	–
první	první	k4xOgFnSc1	první
byla	být	k5eAaImAgFnS	být
Alena	Alena	k1gFnSc1	Alena
<g/>
,	,	kIx,	,
neteř	neteř	k1gFnSc1	neteř
purkrabího	purkrabí	k1gMnSc2	purkrabí
(	(	kIx(	(
<g/>
=	=	kIx~	=
správce	správce	k1gMnSc2	správce
hradu	hrad	k1gInSc2	hrad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
opilý	opilý	k2eAgInSc1d1	opilý
<g/>
,	,	kIx,	,
vsadila	vsadit	k5eAaPmAgFnS	vsadit
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
vzít	vzít	k5eAaPmF	vzít
za	za	k7c4	za
muže	muž	k1gMnSc4	muž
císařského	císařský	k2eAgMnSc4d1	císařský
šenka	šenk	k1gMnSc4	šenk
jménem	jméno	k1gNnSc7	jméno
Pešek	Pešek	k1gMnSc1	Pešek
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
stráví	strávit	k5eAaPmIp3nP	strávit
noc	noc	k1gFnSc4	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
ji	on	k3xPp3gFnSc4	on
nikdo	nikdo	k3yNnSc1	nikdo
nechytí	chytit	k5eNaPmIp3nS	chytit
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
byla	být	k5eAaImAgFnS	být
sama	sám	k3xTgFnSc1	sám
císařovna	císařovna	k1gFnSc1	císařovna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
muži	muž	k1gMnSc6	muž
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
milovala	milovat	k5eAaImAgFnS	milovat
<g/>
,	,	kIx,	,
natolik	natolik	k6eAd1	natolik
stýskalo	stýskat	k5eAaImAgNnS	stýskat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebrala	brát	k5eNaImAgFnS	brát
jeho	on	k3xPp3gInSc4	on
zákaz	zákaz	k1gInSc4	zákaz
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
hrad	hrad	k1gInSc4	hrad
Karlík	Karlík	k1gMnSc1	Karlík
se	se	k3xPyFc4	se
potají	potají	k6eAd1	potají
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
Karlštejn	Karlštejn	k1gInSc4	Karlštejn
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
o	o	k7c6	o
císařovnině	císařovnin	k2eAgFnSc6d1	Císařovnina
přítomnosti	přítomnost	k1gFnSc6	přítomnost
dověděl	dovědět	k5eAaPmAgMnS	dovědět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Arnošt	Arnošt	k1gMnSc1	Arnošt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
císařovně	císařovna	k1gFnSc6	císařovna
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pomoci	pomoct	k5eAaPmF	pomoct
a	a	k8xC	a
převlékl	převléknout	k5eAaPmAgMnS	převléknout
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
oděvu	oděv	k1gInSc2	oděv
šenka	šenk	k1gMnSc2	šenk
Peška	Pešek	k1gMnSc2	Pešek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
přestrojení	přestrojení	k1gNnSc6	přestrojení
měla	mít	k5eAaImAgFnS	mít
strávit	strávit	k5eAaPmF	strávit
noc	noc	k1gFnSc4	noc
v	v	k7c6	v
předpokojí	předpokojí	k1gNnSc6	předpokojí
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
být	být	k5eAaImF	být
mu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
službám	služba	k1gFnPc3	služba
<g/>
.	.	kIx.	.
</s>
<s>
Arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Arnošt	Arnošt	k1gMnSc1	Arnošt
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
šenka	šenk	k1gMnSc4	šenk
Peška	Pešek	k1gMnSc4	Pešek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
předstíral	předstírat	k5eAaImAgMnS	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nemocný	nemocný	k2eAgMnSc1d1	nemocný
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
nevycházel	vycházet	k5eNaImAgMnS	vycházet
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
pokoje	pokoj	k1gInSc2	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Pešek	Pešek	k1gMnSc1	Pešek
nesmí	smět	k5eNaImIp3nS	smět
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
pokoje	pokoj	k1gInSc2	pokoj
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nelíbilo	líbit	k5eNaImAgNnS	líbit
jeho	jeho	k3xOp3gFnSc4	jeho
nastávající	nastávající	k1gFnSc4	nastávající
Aleně	Alena	k1gFnSc3	Alena
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
mu	on	k3xPp3gMnSc3	on
dělala	dělat	k5eAaImAgFnS	dělat
různé	různý	k2eAgInPc4d1	různý
naschvály	naschvál	k1gInPc4	naschvál
<g/>
,	,	kIx,	,
bavila	bavit	k5eAaImAgFnS	bavit
se	se	k3xPyFc4	se
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
muži	muž	k1gMnPc7	muž
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
sotva	sotva	k6eAd1	sotva
prohodila	prohodit	k5eAaPmAgFnS	prohodit
pár	pár	k4xCyI	pár
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
nezvaných	nezvaný	k2eAgMnPc2d1	nezvaný
hostů	host	k1gMnPc2	host
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
i	i	k9	i
hosté	host	k1gMnPc1	host
zvaní	zvaní	k1gNnSc2	zvaní
–	–	k?	–
s	s	k7c7	s
diplomatickým	diplomatický	k2eAgNnSc7d1	diplomatické
posláním	poslání	k1gNnSc7	poslání
vévoda	vévoda	k1gMnSc1	vévoda
Štěpán	Štěpán	k1gMnSc1	Štěpán
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
a	a	k8xC	a
cyperský	cyperský	k2eAgMnSc1d1	cyperský
král	král	k1gMnSc1	král
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nezamlouval	zamlouvat	k5eNaImAgMnS	zamlouvat
zákon	zákon	k1gInSc4	zákon
zakazující	zakazující	k2eAgInSc4d1	zakazující
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
přítomnost	přítomnost	k1gFnSc1	přítomnost
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
vážení	vážený	k2eAgMnPc1d1	vážený
hosté	host	k1gMnPc1	host
si	se	k3xPyFc3	se
ale	ale	k9	ale
Aleny	Alena	k1gFnPc1	Alena
všimnou	všimnout	k5eAaPmIp3nP	všimnout
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
pátrat	pátrat	k5eAaImF	pátrat
<g/>
.	.	kIx.	.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
má	mít	k5eAaImIp3nS	mít
noční	noční	k2eAgNnSc1d1	noční
slyšení	slyšení	k1gNnSc1	slyšení
u	u	k7c2	u
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
si	se	k3xPyFc3	se
všimne	všimnout	k5eAaPmIp3nS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
předpokojí	předpokojí	k1gNnSc6	předpokojí
hlídá	hlídat	k5eAaImIp3nS	hlídat
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sama	sám	k3xTgFnSc1	sám
císařovna	císařovna	k1gFnSc1	císařovna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podle	podle	k7c2	podle
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Arnoštem	Arnošt	k1gMnSc7	Arnošt
hlídá	hlídat	k5eAaImIp3nS	hlídat
císařův	císařův	k2eAgInSc1d1	císařův
pokoj	pokoj	k1gInSc1	pokoj
<g/>
,	,	kIx,	,
v	v	k7c6	v
převlečení	převlečení	k1gNnSc6	převlečení
za	za	k7c2	za
Peška	pešek	k1gInSc2	pešek
<g/>
.	.	kIx.	.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
netuší	tušit	k5eNaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
než	než	k8xS	než
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
císařovnu	císařovna	k1gFnSc4	císařovna
splete	splést	k5eAaPmIp3nS	splést
s	s	k7c7	s
Alenou	Alena	k1gFnSc7	Alena
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Petr	Petr	k1gMnSc1	Petr
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
pustí	pustit	k5eAaPmIp3nP	pustit
do	do	k7c2	do
souboje	souboj	k1gInSc2	souboj
<g/>
,	,	kIx,	,
císařovna	císařovna	k1gFnSc1	císařovna
ho	on	k3xPp3gNnSc4	on
přemůže	přemoct	k5eAaPmIp3nS	přemoct
a	a	k8xC	a
zlomí	zlomit	k5eAaPmIp3nS	zlomit
mu	on	k3xPp3gMnSc3	on
meč	meč	k1gInSc4	meč
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Karel	Karel	k1gMnSc1	Karel
ji	on	k3xPp3gFnSc4	on
okamžitě	okamžitě	k6eAd1	okamžitě
pozná	poznat	k5eAaPmIp3nS	poznat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jen	jen	k9	jen
jediná	jediný	k2eAgFnSc1d1	jediná
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umí	umět	k5eAaImIp3nS	umět
lámat	lámat	k5eAaImF	lámat
meče	meč	k1gInPc4	meč
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
(	(	kIx(	(
<g/>
císařovna	císařovna	k1gFnSc1	císařovna
Eliška	Eliška	k1gFnSc1	Eliška
Pomořanská	pomořanský	k2eAgFnSc1d1	Pomořanská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
císařovnino	císařovnin	k2eAgNnSc1d1	císařovnino
tajemství	tajemství	k1gNnSc1	tajemství
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
,	,	kIx,	,
řekne	říct	k5eAaPmIp3nS	říct
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
císaři	císař	k1gMnSc3	císař
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Císařští	císařský	k2eAgMnPc1d1	císařský
manželé	manžel	k1gMnPc1	manžel
se	se	k3xPyFc4	se
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
tu	ten	k3xDgFnSc4	ten
noc	noc	k1gFnSc4	noc
tajně	tajně	k6eAd1	tajně
opustí	opustit	k5eAaPmIp3nP	opustit
hrad	hrad	k1gInSc4	hrad
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
nepodaří	podařit	k5eNaPmIp3nS	podařit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Štěpán	Štěpán	k1gMnSc1	Štěpán
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
vyvolají	vyvolat	k5eAaPmIp3nP	vyvolat
poplach	poplach	k1gInSc4	poplach
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
okamžitě	okamžitě	k6eAd1	okamžitě
onu	onen	k3xDgFnSc4	onen
ženu	žena	k1gFnSc4	žena
hledají	hledat	k5eAaImIp3nP	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Štěpán	Štěpán	k1gMnSc1	Štěpán
Alenu	Alena	k1gFnSc4	Alena
nalezne	nalézt	k5eAaBmIp3nS	nalézt
na	na	k7c6	na
strážnici	strážnice	k1gFnSc6	strážnice
a	a	k8xC	a
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
do	do	k7c2	do
souboje	souboj	k1gInSc2	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
však	však	k9	však
zastaví	zastavit	k5eAaPmIp3nS	zastavit
nejen	nejen	k6eAd1	nejen
souboj	souboj	k1gInSc4	souboj
ale	ale	k8xC	ale
i	i	k9	i
celé	celý	k2eAgNnSc4d1	celé
prohledávání	prohledávání	k1gNnSc4	prohledávání
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
převlékne	převléknout	k5eAaPmIp3nS	převléknout
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
šatů	šat	k1gInPc2	šat
a	a	k8xC	a
tajně	tajně	k6eAd1	tajně
opustí	opustit	k5eAaPmIp3nP	opustit
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
brána	brána	k1gFnSc1	brána
hradu	hrad	k1gInSc2	hrad
otevírá	otevírat	k5eAaImIp3nS	otevírat
a	a	k8xC	a
císařovna	císařovna	k1gFnSc1	císařovna
předstírá	předstírat	k5eAaImIp3nS	předstírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
náhodou	náhodou	k6eAd1	náhodou
dorazila	dorazit	k5eAaPmAgFnS	dorazit
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
hrad	hrad	k1gInSc4	hrad
Karlík	Karlík	k1gMnSc1	Karlík
na	na	k7c4	na
Karlštejn	Karlštejn	k1gInSc4	Karlštejn
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zruší	zrušit	k5eAaPmIp3nS	zrušit
svůj	svůj	k3xOyFgInSc4	svůj
zákaz	zákaz	k1gInSc4	zákaz
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
přítomnosti	přítomnost	k1gFnSc2	přítomnost
žen	žena	k1gFnPc2	žena
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
jsou	být	k5eAaImIp3nP	být
ženy	žena	k1gFnPc1	žena
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
vítány	vítat	k5eAaImNgFnP	vítat
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
snímek	snímek	k1gInSc4	snímek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
svoji	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
filmovou	filmový	k2eAgFnSc4d1	filmová
roli	role	k1gFnSc4	role
zahrál	zahrát	k5eAaPmAgMnS	zahrát
herec	herec	k1gMnSc1	herec
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Marvan	Marvan	k1gMnSc1	Marvan
(	(	kIx(	(
<g/>
purkrabí	purkrabí	k1gMnSc1	purkrabí
Ješek	Ješek	k1gMnSc1	Ješek
z	z	k7c2	z
Vartenberka	Vartenberka	k1gFnSc1	Vartenberka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
filmu	film	k1gInSc3	film
složil	složit	k5eAaPmAgMnS	složit
Jiří	Jiří	k1gMnSc1	Jiří
Štaidl	Štaidl	k1gMnSc1	Štaidl
své	svůj	k3xOyFgFnPc4	svůj
poslední	poslední	k2eAgFnPc4d1	poslední
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
natáčení	natáčení	k1gNnSc2	natáčení
scény	scéna	k1gFnSc2	scéna
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Kříže	Kříž	k1gMnSc2	Kříž
při	při	k7c6	při
osvětlení	osvětlení	k1gNnSc6	osvětlení
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
hroty	hrot	k1gInPc4	hrot
nacházející	nacházející	k2eAgInPc4d1	nacházející
se	se	k3xPyFc4	se
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
místnosti	místnost	k1gFnPc4	místnost
nabodány	nabodán	k2eAgFnPc4d1	nabodán
a	a	k8xC	a
zapáleny	zapálen	k2eAgFnPc4d1	zapálena
svíčky	svíčka	k1gFnPc4	svíčka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
poškození	poškození	k1gNnSc3	poškození
výzdoby	výzdoba	k1gFnSc2	výzdoba
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
šetrně	šetrně	k6eAd1	šetrně
napravit	napravit	k5eAaPmF	napravit
<g/>
.	.	kIx.	.
</s>
<s>
Filmaři	filmař	k1gMnPc1	filmař
zřejmě	zřejmě	k6eAd1	zřejmě
vycházeli	vycházet	k5eAaImAgMnP	vycházet
z	z	k7c2	z
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
předpokládaly	předpokládat	k5eAaImAgFnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hroty	hrot	k1gInPc4	hrot
se	s	k7c7	s
svícemi	svíce	k1gFnPc7	svíce
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
k	k	k7c3	k
osvětlení	osvětlení	k1gNnSc3	osvětlení
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
absenci	absence	k1gFnSc3	absence
nádobek	nádobka	k1gFnPc2	nádobka
na	na	k7c4	na
drahý	drahý	k2eAgInSc4d1	drahý
včelí	včelí	k2eAgInSc4d1	včelí
vosk	vosk	k1gInSc4	vosk
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nyní	nyní	k6eAd1	nyní
interpretovány	interpretován	k2eAgFnPc1d1	interpretována
jako	jako	k8xS	jako
zpodobnění	zpodobnění	k1gNnSc1	zpodobnění
trnové	trnový	k2eAgFnSc2d1	Trnová
koruny	koruna	k1gFnSc2	koruna
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgInPc6d1	jiný
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
i	i	k8xC	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc1	několik
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
historické	historický	k2eAgFnPc4d1	historická
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc4d1	jiná
filmařské	filmařský	k2eAgFnPc4d1	filmařská
<g/>
:	:	kIx,	:
V	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
scéně	scéna	k1gFnSc6	scéna
projde	projít	k5eAaPmIp3nS	projít
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
jízdním	jízdní	k2eAgNnSc7d1	jízdní
kolem	kolo	k1gNnSc7	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
během	během	k7c2	během
záběrů	záběr	k1gInPc2	záběr
střídají	střídat	k5eAaImIp3nP	střídat
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
scéně	scéna	k1gFnSc6	scéna
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Peškově	Peškův	k2eAgInSc6d1	Peškův
kostýmu	kostým	k1gInSc6	kostým
vidět	vidět	k5eAaImF	vidět
zdrhovadlo	zdrhovadlo	k1gNnSc4	zdrhovadlo
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
recituje	recitovat	k5eAaImIp3nS	recitovat
Nerudovy	Nerudův	k2eAgInPc4d1	Nerudův
verše	verš	k1gInPc4	verš
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
postavený	postavený	k2eAgInSc1d1	postavený
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
má	mít	k5eAaImIp3nS	mít
oprýskanou	oprýskaný	k2eAgFnSc4d1	oprýskaná
omítku	omítka	k1gFnSc4	omítka
a	a	k8xC	a
hromosvod	hromosvod	k1gInSc4	hromosvod
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
písně	píseň	k1gFnSc2	píseň
Muži	muž	k1gMnPc7	muž
nejlíp	dobře	k6eAd3	dobře
sluší	slušet	k5eAaImIp3nS	slušet
sólo	sólo	k1gNnSc1	sólo
vytahují	vytahovat	k5eAaImIp3nP	vytahovat
z	z	k7c2	z
hradní	hradní	k2eAgFnSc2d1	hradní
studny	studna	k1gFnSc2	studna
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
vytáhnou	vytáhnout	k5eAaPmIp3nP	vytáhnout
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
ve	v	k7c6	v
vědrech	vědro	k1gNnPc6	vědro
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
dokončen	dokončen	k2eAgInSc1d1	dokončen
<g/>
,	,	kIx,	,
vypadal	vypadat	k5eAaImAgInS	vypadat
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
hrad	hrad	k1gInSc1	hrad
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
po	po	k7c6	po
Mockerově	Mockerův	k2eAgFnSc6d1	Mockerova
přestavbě	přestavba	k1gFnSc6	přestavba
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
zákazu	zákaz	k1gInSc6	zákaz
pobytu	pobyt	k1gInSc2	pobyt
žen	žena	k1gFnPc2	žena
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
základ	základ	k1gInSc4	základ
ve	v	k7c6	v
skutečných	skutečný	k2eAgNnPc6d1	skutečné
omezeních	omezení	k1gNnPc6	omezení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgNnP	týkat
pobytu	pobyt	k1gInSc3	pobyt
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
manželského	manželský	k2eAgInSc2d1	manželský
života	život	k1gInSc2	život
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
,	,	kIx,	,
týkala	týkat	k5eAaImAgFnS	týkat
se	se	k3xPyFc4	se
však	však	k9	však
pouze	pouze	k6eAd1	pouze
některých	některý	k3yIgFnPc2	některý
jeho	jeho	k3xOp3gFnPc2	jeho
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
dialogů	dialog	k1gInPc2	dialog
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
na	na	k7c6	na
gramofonové	gramofonový	k2eAgFnSc6d1	gramofonová
desce	deska	k1gFnSc6	deska
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
uvádí	uvádět	k5eAaImIp3nS	uvádět
Hudební	hudební	k2eAgNnSc1d1	hudební
divadlo	divadlo	k1gNnSc1	divadlo
Karlín	Karlín	k1gInSc1	Karlín
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Petra	Petr	k1gMnSc2	Petr
Novotného	Novotný	k1gMnSc2	Novotný
muzikál	muzikál	k1gInSc1	muzikál
Noc	noc	k1gFnSc4	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
s	s	k7c7	s
písněmi	píseň	k1gFnPc7	píseň
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
–	–	k?	–
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Brodský	Brodský	k1gMnSc1	Brodský
Alžběta	Alžběta	k1gFnSc1	Alžběta
Pomořanská	pomořanský	k2eAgFnSc1d1	Pomořanská
–	–	k?	–
Jana	Jana	k1gFnSc1	Jana
Brejchová	Brejchová	k1gFnSc1	Brejchová
Arnošt	Arnošt	k1gMnSc1	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
–	–	k?	–
Karel	Karel	k1gMnSc1	Karel
Höger	Höger	k1gMnSc1	Höger
Petr	Petr	k1gMnSc1	Petr
Cyperský	cyperský	k2eAgMnSc1d1	cyperský
–	–	k?	–
Waldemar	Waldemar	k1gMnSc1	Waldemar
Matuška	Matuška	k1gMnSc1	Matuška
Štěpán	Štěpán	k1gMnSc1	Štěpán
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
–	–	k?	–
Miloš	Miloš	k1gMnSc1	Miloš
Kopecký	Kopecký	k1gMnSc1	Kopecký
purkrabí	purkrabí	k1gMnSc1	purkrabí
–	–	k?	–
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Marvan	Marvan	k1gMnSc1	Marvan
paní	paní	k1gFnSc2	paní
Ofka	Ofka	k1gMnSc1	Ofka
–	–	k?	–
Slávka	Slávka	k1gFnSc1	Slávka
Budínová	Budínová	k1gFnSc1	Budínová
Alena	Alena	k1gFnSc1	Alena
–	–	k?	–
Daniela	Daniela	k1gFnSc1	Daniela
Kolářová	Kolářová	k1gFnSc1	Kolářová
Pešek	Pešek	k1gMnSc1	Pešek
–	–	k?	–
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hanzlík	Hanzlík	k1gMnSc1	Hanzlík
</s>
