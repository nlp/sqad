<s>
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
filmový	filmový	k2eAgInSc1d1	filmový
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Podskalský	podskalský	k2eAgMnSc1d1	podskalský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
na	na	k7c4	na
námět	námět	k1gInSc4	námět
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
<g/>
.	.	kIx.	.
</s>
