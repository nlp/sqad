<s>
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Ф	Ф	k?	Ф
М	М	k?	М
Д	Д	k?	Д
<g/>
;	;	kIx,	;
30	[number]	k4	30
<g/>
.	.	kIx.	.
říjnajul	říjnajout	k5eAaPmAgInS	říjnajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1821	[number]	k4	1821
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
Moskva	Moskva	k1gFnSc1	Moskva
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
lednajul	lednajout	k5eAaPmAgInS	lednajout
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1881	[number]	k4	1881
<g/>
greg	grega	k1gFnPc2	grega
<g/>
.	.	kIx.	.
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
světových	světový	k2eAgMnPc2d1	světový
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
vrcholný	vrcholný	k2eAgMnSc1d1	vrcholný
představitel	představitel	k1gMnSc1	představitel
ruského	ruský	k2eAgInSc2d1	ruský
realismu	realismus	k1gInSc2	realismus
a	a	k8xC	a
současně	současně	k6eAd1	současně
předchůdce	předchůdce	k1gMnSc1	předchůdce
moderní	moderní	k2eAgFnSc2d1	moderní
psychologické	psychologický	k2eAgFnSc2d1	psychologická
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
