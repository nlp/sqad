<s>
Eduard	Eduard	k1gMnSc1	Eduard
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1442	[number]	k4	1442
Rouen	Rouna	k1gFnPc2	Rouna
<g/>
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1483	[number]	k4	1483
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
králem	král	k1gMnSc7	král
Anglie	Anglie	k1gFnSc2	Anglie
v	v	k7c6	v
období	období	k1gNnSc6	období
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1461	[number]	k4	1461
až	až	k9	až
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1470	[number]	k4	1470
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1471	[number]	k4	1471
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Yorků	York	k1gInPc2	York
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
Rouen	Rouen	k1gInSc4	Rouen
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
druhým	druhý	k4xOgMnSc7	druhý
synem	syn	k1gMnSc7	syn
Richarda	Richard	k1gMnSc2	Richard
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
činil	činit	k5eAaImAgInS	činit
nárok	nárok	k1gInSc4	nárok
v	v	k7c6	v
nástupnictví	nástupnictví	k1gNnSc6	nástupnictví
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
a	a	k8xC	a
Cecílie	Cecílie	k1gFnSc1	Cecílie
Nevillové	Nevillová	k1gFnSc2	Nevillová
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nejstarším	starý	k2eAgInPc3d3	nejstarší
ze	z	k7c2	z
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
dospělosti	dospělost	k1gFnPc4	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Uplatňování	uplatňování	k1gNnSc4	uplatňování
nároku	nárok	k1gInSc2	nárok
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
roku	rok	k1gInSc2	rok
1460	[number]	k4	1460
byla	být	k5eAaImAgFnS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
příčina	příčina	k1gFnSc1	příčina
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
konfliktu	konflikt	k1gInSc2	konflikt
známého	známý	k1gMnSc2	známý
jako	jako	k8xS	jako
války	válka	k1gFnSc2	válka
růží	růž	k1gFnPc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Wakefieldu	Wakefield	k1gInSc2	Wakefield
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgMnS	přejít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
na	na	k7c4	na
Eduarda	Eduard	k1gMnSc4	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Richarda	Richard	k1gMnSc2	Richard
Nevilleho	Neville	k1gMnSc2	Neville
<g/>
,	,	kIx,	,
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Warwicku	Warwick	k1gInSc2	Warwick
<g/>
,	,	kIx,	,
porazil	porazit	k5eAaPmAgMnS	porazit
Eduard	Eduard	k1gMnSc1	Eduard
Lancastery	Lancaster	k1gMnPc7	Lancaster
v	v	k7c6	v
několika	několik	k4yIc6	několik
bitvách	bitva	k1gFnPc6	bitva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
Jindřich	Jindřich	k1gMnSc1	Jindřich
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
na	na	k7c6	na
vojenském	vojenský	k2eAgNnSc6d1	vojenské
tažení	tažení	k1gNnSc6	tažení
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Warwick	Warwick	k1gInSc1	Warwick
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1461	[number]	k4	1461
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
Eduarda	Eduard	k1gMnSc2	Eduard
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
podpořil	podpořit	k5eAaPmAgMnS	podpořit
svůj	svůj	k3xOyFgInSc4	svůj
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
rozhodným	rozhodný	k2eAgNnSc7d1	rozhodné
vítězstvím	vítězství	k1gNnSc7	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Towtonu	Towton	k1gInSc2	Towton
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Eduardovi	Eduardův	k2eAgMnPc1d1	Eduardův
pouze	pouze	k6eAd1	pouze
devatenáct	devatenáct	k4xCc4	devatenáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
schopným	schopný	k2eAgMnSc7d1	schopný
vojenským	vojenský	k2eAgMnSc7d1	vojenský
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Warwick	Warwick	k1gMnSc1	Warwick
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eduardovým	Eduardův	k2eAgNnSc7d1	Eduardovo
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
bude	být	k5eAaImBp3nS	být
dál	daleko	k6eAd2	daleko
ovládat	ovládat	k5eAaImF	ovládat
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
přiměje	přimět	k5eAaPmIp3nS	přimět
ke	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
vytvářejícím	vytvářející	k2eAgInSc7d1	vytvářející
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
důležitou	důležitý	k2eAgFnSc7d1	důležitá
mocností	mocnost	k1gFnSc7	mocnost
na	na	k7c6	na
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
bez	bez	k7c2	bez
jeho	jeho	k3xOp3gNnSc2	jeho
vědomí	vědomí	k1gNnSc2	vědomí
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Alžbětou	Alžběta	k1gFnSc7	Alžběta
z	z	k7c2	z
Woodvile	Woodvila	k1gFnSc3	Woodvila
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
sice	sice	k8xC	sice
nepříliš	příliš	k6eNd1	příliš
bohatými	bohatý	k2eAgMnPc7d1	bohatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zato	zato	k6eAd1	zato
ambiciózními	ambiciózní	k2eAgFnPc7d1	ambiciózní
lancasterskými	lancasterský	k2eAgFnPc7d1	lancasterský
příbuznými	příbuzná	k1gFnPc7	příbuzná
<g/>
.	.	kIx.	.
</s>
<s>
Warwick	Warwick	k1gMnSc1	Warwick
nelibě	libě	k6eNd1	libě
nesl	nést	k5eAaImAgMnS	nést
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
lidí	člověk	k1gMnPc2	člověk
na	na	k7c4	na
Eduarda	Eduard	k1gMnSc4	Eduard
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Jiřího	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Clarence	Clarence	k1gFnSc2	Clarence
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
vojenský	vojenský	k2eAgInSc4d1	vojenský
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
Eduardovi	Eduard	k1gMnSc3	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
Eduardova	Eduardův	k2eAgNnSc2d1	Eduardovo
vojska	vojsko	k1gNnSc2	vojsko
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
něho	on	k3xPp3gNnSc2	on
samotného	samotný	k2eAgNnSc2d1	samotné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
poražena	poražen	k2eAgFnSc1d1	poražena
roku	rok	k1gInSc2	rok
1469	[number]	k4	1469
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Edgecote	Edgecot	k1gInSc5	Edgecot
Moor	Moor	k1gMnSc1	Moor
a	a	k8xC	a
Eduard	Eduard	k1gMnSc1	Eduard
byl	být	k5eAaImAgMnS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
zajat	zajmout	k5eAaPmNgInS	zajmout
u	u	k7c2	u
Olney	Olnea	k1gFnSc2	Olnea
<g/>
.	.	kIx.	.
</s>
<s>
Warwick	Warwick	k1gMnSc1	Warwick
pak	pak	k6eAd1	pak
vládl	vládnout	k5eAaImAgMnS	vládnout
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
příznivci	příznivec	k1gMnPc7	příznivec
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hrozilo	hrozit	k5eAaImAgNnS	hrozit
vzpourou	vzpoura	k1gFnSc7	vzpoura
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
musel	muset	k5eAaImAgInS	muset
Warwick	Warwick	k1gInSc1	Warwick
propustit	propustit	k5eAaPmF	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
s	s	k7c7	s
Warwickem	Warwick	k1gInSc7	Warwick
a	a	k8xC	a
Clarencem	Clarenec	k1gInSc7	Clarenec
usmířit	usmířit	k5eAaPmF	usmířit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1470	[number]	k4	1470
se	se	k3xPyFc4	se
Warwick	Warwick	k1gMnSc1	Warwick
a	a	k8xC	a
Clarence	Clarenec	k1gMnSc4	Clarenec
vzbouřili	vzbouřit	k5eAaPmAgMnP	vzbouřit
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
poraženi	porazit	k5eAaPmNgMnP	porazit
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
utéci	utéct	k5eAaPmF	utéct
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
spojenectví	spojenectví	k1gNnSc4	spojenectví
a	a	k8xC	a
Markétou	Markéta	k1gFnSc7	Markéta
z	z	k7c2	z
Anjou	Anja	k1gFnSc7	Anja
<g/>
,	,	kIx,	,
manželkou	manželka	k1gFnSc7	manželka
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
Jindřichova	Jindřichův	k2eAgInSc2d1	Jindřichův
návratu	návrat	k1gInSc2	návrat
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Podporovaní	podporovaný	k2eAgMnPc1d1	podporovaný
Francií	Francie	k1gFnSc7	Francie
vylodili	vylodit	k5eAaPmAgMnP	vylodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1470	[number]	k4	1470
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Lancasterů	Lancaster	k1gInPc2	Lancaster
přidal	přidat	k5eAaPmAgMnS	přidat
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Neville	Neville	k1gFnSc2	Neville
<g/>
,	,	kIx,	,
odjet	odjet	k5eAaPmF	odjet
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1470	[number]	k4	1470
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
Eduard	Eduard	k1gMnSc1	Eduard
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
do	do	k7c2	do
Burgundska	Burgundsko	k1gNnSc2	Burgundsko
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Richarda	Richard	k1gMnSc2	Richard
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Gloucesteru	Gloucester	k1gInSc2	Gloucester
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Burgundsku	Burgundsko	k1gNnSc6	Burgundsko
vládl	vládnout	k5eAaImAgMnS	vládnout
jeho	jeho	k3xOp3gMnSc4	jeho
švagr	švagr	k1gMnSc1	švagr
Karel	Karel	k1gMnSc1	Karel
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
burgundský	burgundský	k2eAgMnSc1d1	burgundský
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
Karel	Karel	k1gMnSc1	Karel
nechtěl	chtít	k5eNaImAgMnS	chtít
angažovat	angažovat	k5eAaBmF	angažovat
na	na	k7c6	na
Eduardově	Eduardův	k2eAgFnSc6d1	Eduardova
straně	strana	k1gFnSc6	strana
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Francie	Francie	k1gFnSc1	Francie
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Burgundsku	Burgundsko	k1gNnSc3	Burgundsko
válku	válek	k1gInSc2	válek
<g/>
,	,	kIx,	,
změnil	změnit	k5eAaPmAgInS	změnit
názor	názor	k1gInSc4	názor
a	a	k8xC	a
sestavil	sestavit	k5eAaPmAgMnS	sestavit
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Eduardovi	Eduard	k1gMnSc3	Eduard
pomohl	pomoct	k5eAaPmAgMnS	pomoct
získat	získat	k5eAaPmF	získat
zpět	zpět	k6eAd1	zpět
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
domů	domů	k6eAd1	domů
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
malým	malý	k2eAgNnSc7d1	malé
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
většímu	veliký	k2eAgInSc3d2	veliký
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
uplatnit	uplatnit	k5eAaPmF	uplatnit
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
vévodství	vévodství	k1gNnSc4	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
York	York	k1gInSc1	York
ale	ale	k8xC	ale
zavřelo	zavřít	k5eAaPmAgNnS	zavřít
před	před	k7c7	před
jeho	jeho	k3xOp3gNnSc7	jeho
vojskem	vojsko	k1gNnSc7	vojsko
své	svůj	k3xOyFgFnSc2	svůj
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Eduard	Eduard	k1gMnSc1	Eduard
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
získávat	získávat	k5eAaImF	získávat
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Přidal	přidat	k5eAaPmAgMnS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Clarence	Clarence	k1gFnSc2	Clarence
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
porazili	porazit	k5eAaPmAgMnP	porazit
Warwika	Warwik	k1gMnSc4	Warwik
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Barnetu	Barnet	k1gInSc2	Barnet
a	a	k8xC	a
poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
Warwick	Warwick	k1gMnSc1	Warwick
padl	padnout	k5eAaImAgMnS	padnout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
porážka	porážka	k1gFnSc1	porážka
zbytku	zbytek	k1gInSc2	zbytek
lancasterských	lancasterský	k2eAgFnPc2d1	lancasterský
sil	síla	k1gFnPc2	síla
roku	rok	k1gInSc2	rok
1471	[number]	k4	1471
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Tewkesbury	Tewkesbura	k1gFnSc2	Tewkesbura
již	již	k6eAd1	již
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
nebo	nebo	k8xC	nebo
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
i	i	k9	i
poslední	poslední	k2eAgMnSc1d1	poslední
lancasterský	lancasterský	k2eAgMnSc1d1	lancasterský
dědic	dědic	k1gMnSc1	dědic
Eduard	Eduard	k1gMnSc1	Eduard
z	z	k7c2	z
Westminsteru	Westminster	k1gInSc2	Westminster
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
o	o	k7c6	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
Jindřicha	Jindřich	k1gMnSc4	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byli	být	k5eAaImAgMnP	být
Lancasterové	Lancaster	k1gMnPc1	Lancaster
zcela	zcela	k6eAd1	zcela
poraženi	poražen	k2eAgMnPc1d1	poražen
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
Eduardovi	Eduardův	k2eAgMnPc1d1	Eduardův
mladší	mladý	k2eAgMnPc1d2	mladší
bratři	bratr	k1gMnPc1	bratr
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Clarence	Clarence	k1gFnSc2	Clarence
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Gloucesteru	Gloucester	k1gInSc2	Gloucester
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
král	král	k1gMnSc1	král
Richard	Richard	k1gMnSc1	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
oženili	oženit	k5eAaPmAgMnP	oženit
s	s	k7c7	s
Izabelou	Izabela	k1gFnSc7	Izabela
a	a	k8xC	a
Annou	Anna	k1gFnSc7	Anna
Nevillovou	Nevillová	k1gFnSc7	Nevillová
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
byly	být	k5eAaImAgFnP	být
dcery	dcera	k1gFnPc1	dcera
Warwicka	Warwicko	k1gNnSc2	Warwicko
a	a	k8xC	a
Anny	Anna	k1gFnSc2	Anna
Beauchampové	Beauchampová	k1gFnSc2	Beauchampová
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
soupeři	soupeř	k1gMnSc3	soupeř
v	v	k7c6	v
nástupnictví	nástupnictví	k1gNnSc6	nástupnictví
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
osnování	osnování	k1gNnSc2	osnování
vzpoury	vzpoura	k1gFnSc2	vzpoura
proti	proti	k7c3	proti
Eduardovi	Eduard	k1gMnSc3	Eduard
<g/>
,	,	kIx,	,
uvězněn	uvěznit	k5eAaPmNgInS	uvěznit
v	v	k7c6	v
Toweru	Towero	k1gNnSc6	Towero
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1478	[number]	k4	1478
soukromě	soukromě	k6eAd1	soukromě
popraven	popravit	k5eAaPmNgInS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
nemusel	muset	k5eNaImAgMnS	muset
do	do	k7c2	do
konce	konec	k1gInSc2	konec
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
řešit	řešit	k5eAaImF	řešit
žádnou	žádný	k3yNgFnSc4	žádný
vzpouru	vzpoura	k1gFnSc4	vzpoura
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rod	rod	k1gInSc1	rod
Lancasterů	Lancaster	k1gInPc2	Lancaster
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
a	a	k8xC	a
jediný	jediný	k2eAgMnSc1d1	jediný
jeho	jeho	k3xOp3gMnSc1	jeho
soupeř	soupeř	k1gMnSc1	soupeř
Jindřich	Jindřich	k1gMnSc1	Jindřich
Tudor	tudor	k1gInSc4	tudor
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1475	[number]	k4	1475
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
válku	válka	k1gFnSc4	válka
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
mírová	mírový	k2eAgFnSc1d1	mírová
dohoda	dohoda	k1gFnSc1	dohoda
z	z	k7c2	z
Picquigny	Picquigna	k1gFnSc2	Picquigna
mu	on	k3xPp3gMnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
jednorázovou	jednorázový	k2eAgFnSc4d1	jednorázová
platbu	platba	k1gFnSc4	platba
75	[number]	k4	75
000	[number]	k4	000
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
roční	roční	k2eAgFnSc4d1	roční
rentu	renta	k1gFnSc4	renta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
50	[number]	k4	50
000	[number]	k4	000
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
také	také	k9	také
roku	rok	k1gInSc2	rok
1482	[number]	k4	1482
odrazit	odrazit	k5eAaPmF	odrazit
pokus	pokus	k1gInSc4	pokus
Alexandra	Alexandr	k1gMnSc2	Alexandr
Stewarta	Stewart	k1gMnSc2	Stewart
<g/>
,	,	kIx,	,
bratra	bratr	k1gMnSc2	bratr
skotského	skotský	k1gInSc2	skotský
krále	král	k1gMnSc2	král
Jakuba	Jakub	k1gMnSc2	Jakub
III	III	kA	III
<g/>
.	.	kIx.	.
zmocnit	zmocnit	k5eAaPmF	zmocnit
se	se	k3xPyFc4	se
skotského	skotský	k2eAgInSc2d1	skotský
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Eduardovo	Eduardův	k2eAgNnSc4d1	Eduardovo
zdraví	zdraví	k1gNnSc4	zdraví
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
i	i	k8xC	i
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
jeho	jeho	k3xOp3gInSc2	jeho
nezdravého	zdravý	k2eNgInSc2d1	nezdravý
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
zhoršovalo	zhoršovat	k5eAaImAgNnS	zhoršovat
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
ho	on	k3xPp3gMnSc4	on
postihovaly	postihovat	k5eAaImAgFnP	postihovat
různé	různý	k2eAgFnPc4d1	různá
choroby	choroba	k1gFnPc4	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
roku	rok	k1gInSc2	rok
1483	[number]	k4	1483
ho	on	k3xPp3gInSc4	on
postihla	postihnout	k5eAaPmAgFnS	postihnout
vážná	vážný	k2eAgFnSc1d1	vážná
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgMnS	být
schopen	schopit	k5eAaPmNgMnS	schopit
jmenovat	jmenovat	k5eAaImF	jmenovat
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Richarda	Richard	k1gMnSc4	Richard
jako	jako	k8xC	jako
protektora	protektor	k1gMnSc4	protektor
<g/>
,	,	kIx,	,
než	než	k8xS	než
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1483	[number]	k4	1483
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
Svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
ve	v	k7c6	v
Windsorském	windsorský	k2eAgInSc6d1	windsorský
hradu	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Eduardovým	Eduardův	k2eAgMnSc7d1	Eduardův
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
dvanáctiletý	dvanáctiletý	k2eAgMnSc1d1	dvanáctiletý
syn	syn	k1gMnSc1	syn
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
byl	být	k5eAaImAgMnS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
schopným	schopný	k2eAgMnSc7d1	schopný
vojenským	vojenský	k2eAgMnSc7d1	vojenský
velitelem	velitel	k1gMnSc7	velitel
<g/>
,	,	kIx,	,
porazil	porazit	k5eAaPmAgInS	porazit
a	a	k8xC	a
zničil	zničit	k5eAaPmAgInS	zničit
rod	rod	k1gInSc4	rod
Lancasterů	Lancaster	k1gMnPc2	Lancaster
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
a	a	k8xC	a
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
znamenalo	znamenat	k5eAaImAgNnS	znamenat
zpočátku	zpočátku	k6eAd1	zpočátku
obnovení	obnovení	k1gNnSc1	obnovení
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
pořádku	pořádek	k1gInSc2	pořádek
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
objevovat	objevovat	k5eAaImF	objevovat
více	hodně	k6eAd2	hodně
pirátství	pirátství	k1gNnSc4	pirátství
a	a	k8xC	a
banditů	bandita	k1gMnPc2	bandita
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
obchodníkem	obchodník	k1gMnSc7	obchodník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
investoval	investovat	k5eAaBmAgMnS	investovat
do	do	k7c2	do
několika	několik	k4yIc2	několik
společností	společnost	k1gFnPc2	společnost
v	v	k7c6	v
City	city	k1gNnSc6	city
<g/>
.	.	kIx.	.
</s>
<s>
Eduardův	Eduardův	k2eAgInSc1d1	Eduardův
rod	rod	k1gInSc1	rod
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
na	na	k7c6	na
anglickém	anglický	k2eAgInSc6d1	anglický
trůnu	trůn	k1gInSc6	trůn
udržel	udržet	k5eAaPmAgInS	udržet
jen	jen	k9	jen
něco	něco	k6eAd1	něco
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
mužských	mužský	k2eAgInPc2d1	mužský
členů	člen	k1gInPc2	člen
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
smrtí	smrt	k1gFnSc7	smrt
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
i	i	k8xC	i
bratr	bratr	k1gMnSc1	bratr
padli	padnout	k5eAaPmAgMnP	padnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
<g/>
,	,	kIx,	,
dědeček	dědeček	k1gMnSc1	dědeček
i	i	k8xC	i
jiný	jiný	k2eAgMnSc1d1	jiný
bratr	bratr	k1gMnSc1	bratr
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eduardovu	Eduardův	k2eAgMnSc3d1	Eduardův
nejstaršímu	starý	k2eAgMnSc3d3	nejstarší
synovi	syn	k1gMnSc3	syn
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
titul	titul	k1gInSc1	titul
Princ	princa	k1gFnPc2	princa
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
sedmi	sedm	k4xCc2	sedm
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
mladý	mladý	k2eAgMnSc1d1	mladý
Eduard	Eduard	k1gMnSc1	Eduard
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
sesazen	sesadit	k5eAaPmNgMnS	sesadit
a	a	k8xC	a
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Richardem	Richard	k1gMnSc7	Richard
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
padl	padnout	k5eAaPmAgInS	padnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Bosworthu	Bosworth	k1gInSc2	Bosworth
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
Eduardova	Eduardův	k2eAgFnSc1d1	Eduardova
dcera	dcera	k1gFnSc1	dcera
Alžběta	Alžběta	k1gFnSc1	Alžběta
z	z	k7c2	z
Yorku	York	k1gInSc2	York
manželkou	manželka	k1gFnSc7	manželka
krále	král	k1gMnSc2	král
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Tudorovců	Tudorovec	k1gMnPc2	Tudorovec
Jindřicha	Jindřich	k1gMnSc2	Jindřich
a	a	k8xC	a
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
osobě	osoba	k1gFnSc6	osoba
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
vliv	vliv	k1gInSc1	vliv
Yorků	York	k1gInPc2	York
na	na	k7c6	na
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
IV	IV	kA	IV
<g/>
.	.	kIx.	.
měl	mít	k5eAaImAgMnS	mít
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Alžbětou	Alžběta	k1gFnSc7	Alžběta
Woodville	Woodville	k1gNnSc2	Woodville
celkem	celkem	k6eAd1	celkem
deset	deset	k4xCc4	deset
dětí	dítě	k1gFnPc2	dítě
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
sedm	sedm	k4xCc4	sedm
se	se	k3xPyFc4	se
dožilo	dožít	k5eAaPmAgNnS	dožít
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
Parlament	parlament	k1gInSc4	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1483	[number]	k4	1483
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
nelegitimní	legitimní	k2eNgNnSc4d1	nelegitimní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Edwardův	Edwardův	k2eAgMnSc1d1	Edwardův
bratr	bratr	k1gMnSc1	bratr
Richard	Richard	k1gMnSc1	Richard
mohl	moct	k5eAaImAgMnS	moct
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
z	z	k7c2	z
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
1466	[number]	k4	1466
<g/>
-	-	kIx~	-
<g/>
1503	[number]	k4	1503
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudora	tudor	k1gMnSc2	tudor
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Tudora	tudor	k1gMnSc4	tudor
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
z	z	k7c2	z
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
1467	[number]	k4	1467
<g/>
-	-	kIx~	-
<g/>
1482	[number]	k4	1482
<g/>
)	)	kIx)	)
Cecilie	Cecilie	k1gFnSc2	Cecilie
z	z	k7c2	z
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
1469	[number]	k4	1469
<g/>
-	-	kIx~	-
<g/>
1507	[number]	k4	1507
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c4	za
Johna	John	k1gMnSc4	John
Wellese	Wellesa	k1gFnSc6	Wellesa
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Vikomta	vikomt	k1gMnSc2	vikomt
Welles	Welles	k1gInSc4	Welles
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c4	za
Thomase	Thomas	k1gMnSc4	Thomas
Kyme	Kym	k1gMnSc4	Kym
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
Keme	Kem	k1gMnSc2	Kem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Eduard	Eduard	k1gMnSc1	Eduard
V.	V.	kA	V.
(	(	kIx(	(
<g/>
1470	[number]	k4	1470
<g/>
-	-	kIx~	-
<g/>
1483	[number]	k4	1483
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
anglickým	anglický	k2eAgMnSc7d1	anglický
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
starším	starý	k2eAgInPc3d2	starší
z	z	k7c2	z
bratrů	bratr	k1gMnPc2	bratr
přezdívaných	přezdívaný	k2eAgMnPc2d1	přezdívaný
Princové	princ	k1gMnPc1	princ
z	z	k7c2	z
Toweru	Tower	k1gInSc2	Tower
<g/>
.	.	kIx.	.
</s>
<s>
Markéta	Markéta	k1gFnSc1	Markéta
z	z	k7c2	z
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
1472	[number]	k4	1472
<g/>
-	-	kIx~	-
<g/>
1472	[number]	k4	1472
<g/>
)	)	kIx)	)
Richard	Richard	k1gMnSc1	Richard
ze	z	k7c2	z
Shrewsbury	Shrewsbura	k1gFnSc2	Shrewsbura
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
1473	[number]	k4	1473
<g/>
-	-	kIx~	-
<g/>
1483	[number]	k4	1483
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
z	z	k7c2	z
Princů	princ	k1gMnPc2	princ
z	z	k7c2	z
Toweru	Tower	k1gInSc2	Tower
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
z	z	k7c2	z
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
1475	[number]	k4	1475
<g/>
-	-	kIx~	-
<g/>
1511	[number]	k4	1511
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c2	za
Thomase	Thomas	k1gMnSc2	Thomas
Howarda	Howard	k1gMnSc2	Howard
<g/>
,	,	kIx,	,
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
3	[number]	k4	3
<g/>
.	.	kIx.	.
vévodu	vévoda	k1gMnSc4	vévoda
z	z	k7c2	z
Norfolku	Norfolek	k1gInSc2	Norfolek
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k1gFnSc1	George
Plantagenet	Plantageneta	k1gFnPc2	Plantageneta
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Bedfordu	Bedford	k1gInSc2	Bedford
(	(	kIx(	(
<g/>
1477	[number]	k4	1477
<g/>
-	-	kIx~	-
<g/>
1479	[number]	k4	1479
<g/>
)	)	kIx)	)
Kateřina	Kateřina	k1gFnSc1	Kateřina
z	z	k7c2	z
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
1479	[number]	k4	1479
<g/>
-	-	kIx~	-
<g/>
1527	[number]	k4	1527
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c4	za
Williama	William	k1gMnSc4	William
Courtenaye	Courtenay	k1gInSc2	Courtenay
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Devonu	devon	k1gInSc2	devon
<g/>
.	.	kIx.	.
</s>
<s>
Bridget	Bridget	k1gInSc1	Bridget
z	z	k7c2	z
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
1480	[number]	k4	1480
<g/>
-	-	kIx~	-
<g/>
1517	[number]	k4	1517
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeptiška	jeptiška	k1gFnSc1	jeptiška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Edward	Edward	k1gMnSc1	Edward
IV	Iva	k1gFnPc2	Iva
of	of	k?	of
England	Englanda	k1gFnPc2	Englanda
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
NEILLANDS	NEILLANDS	kA	NEILLANDS
<g/>
,	,	kIx,	,
Robin	robin	k2eAgInSc1d1	robin
<g/>
.	.	kIx.	.
</s>
<s>
Války	válek	k1gInPc1	válek
růží	růž	k1gFnPc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
187	[number]	k4	187
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
206	[number]	k4	206
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
620	[number]	k4	620
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Lancasterové	Lancasterové	k2eAgInPc7d1	Lancasterové
a	a	k8xC	a
Yorkové	Yorkové	k2eAgInPc7d1	Yorkové
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Eduard	Eduard	k1gMnSc1	Eduard
IV	IV	kA	IV
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
