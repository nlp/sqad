<p>
<s>
Moučník	moučník	k1gInSc1	moučník
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
jídla	jídlo	k1gNnSc2	jídlo
upečený	upečený	k2eAgInSc1d1	upečený
z	z	k7c2	z
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
moučník	moučník	k1gInSc1	moučník
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
sladké	sladký	k2eAgNnSc4d1	sladké
pečivo	pečivo	k1gNnSc4	pečivo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
moučníky	moučník	k1gInPc1	moučník
slané	slaný	k2eAgInPc1d1	slaný
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
některé	některý	k3yIgInPc1	některý
dezerty	dezert	k1gInPc1	dezert
<g/>
,	,	kIx,	,
koláče	koláč	k1gInPc1	koláč
<g/>
,	,	kIx,	,
buchty	buchta	k1gFnPc1	buchta
<g/>
,	,	kIx,	,
zkrátka	zkrátka	k6eAd1	zkrátka
pečivo	pečivo	k1gNnSc4	pečivo
z	z	k7c2	z
mouky	mouka	k1gFnSc2	mouka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
serveru	server	k1gInSc2	server
Žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
moučníky	moučník	k1gInPc1	moučník
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
pekařské	pekařský	k2eAgInPc4d1	pekařský
<g/>
,	,	kIx,	,
cukrářské	cukrářský	k2eAgInPc4d1	cukrářský
i	i	k8xC	i
jiné	jiný	k2eAgInPc4d1	jiný
kuchařské	kuchařský	k2eAgInPc4d1	kuchařský
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
konzumovat	konzumovat	k5eAaBmF	konzumovat
buď	buď	k8xC	buď
samostatně	samostatně	k6eAd1	samostatně
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
menu	menu	k1gNnSc2	menu
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
či	či	k8xC	či
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
výrobky	výrobek	k1gInPc4	výrobek
tepelně	tepelně	k6eAd1	tepelně
zpracované	zpracovaný	k2eAgInPc4d1	zpracovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ty	ten	k3xDgFnPc4	ten
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
za	za	k7c2	za
studena	studeno	k1gNnSc2	studeno
<g/>
,	,	kIx,	,
sladké	sladký	k2eAgFnSc2d1	sladká
i	i	k8xC	i
slané	slaný	k2eAgFnSc2d1	slaná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kuchařská	kuchařský	k2eAgFnSc1d1	kuchařská
terminologie	terminologie	k1gFnSc1	terminologie
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
zažitému	zažitý	k2eAgNnSc3d1	zažité
užívání	užívání	k1gNnSc3	užívání
termínu	termín	k1gInSc2	termín
moučník	moučník	k1gInSc1	moučník
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
pečené	pečený	k2eAgInPc4d1	pečený
výrobky	výrobek	k1gInPc4	výrobek
z	z	k7c2	z
mouky	mouka	k1gFnSc2	mouka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Zákusek	zákusek	k1gInSc1	zákusek
</s>
</p>
<p>
<s>
Dezert	dezert	k1gInSc1	dezert
</s>
</p>
