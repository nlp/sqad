<s>
Eugenika	eugenika	k1gFnSc1	eugenika
je	být	k5eAaImIp3nS	být
sociálně-filosofický	sociálněilosofický	k2eAgInSc4d1	sociálně-filosofický
směr	směr	k1gInSc4	směr
zaměřený	zaměřený	k2eAgInSc4d1	zaměřený
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
povedou	povést	k5eAaPmIp3nP	povést
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
co	co	k9	co
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
genetického	genetický	k2eAgInSc2d1	genetický
fondu	fond	k1gInSc2	fond
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
eugenika	eugenik	k1gMnSc2	eugenik
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
britským	britský	k2eAgMnSc7d1	britský
matematikem	matematik	k1gMnSc7	matematik
a	a	k8xC	a
vědcem	vědec	k1gMnSc7	vědec
Francisem	Francis	k1gInSc7	Francis
Galtonem	Galton	k1gInSc7	Galton
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pozitivní	pozitivní	k2eAgFnSc7d1	pozitivní
eugenikou	eugenika	k1gFnSc7	eugenika
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
žádaných	žádaný	k2eAgInPc2d1	žádaný
znaků	znak	k1gInPc2	znak
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
negativní	negativní	k2eAgFnSc1d1	negativní
eugenika	eugenika	k1gFnSc1	eugenika
bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
vymýcení	vymýcení	k1gNnSc4	vymýcení
z	z	k7c2	z
populace	populace	k1gFnSc2	populace
znaků	znak	k1gInPc2	znak
nežádaných	žádaný	k2eNgInPc2d1	nežádaný
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
již	již	k6eAd1	již
Platón	Platón	k1gMnSc1	Platón
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
základní	základní	k2eAgFnSc4d1	základní
myšlenku	myšlenka	k1gFnSc4	myšlenka
pozitivní	pozitivní	k2eAgFnSc2d1	pozitivní
eugeniky	eugenika	k1gFnSc2	eugenika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ideální	ideální	k2eAgNnSc4d1	ideální
potomstvo	potomstvo	k1gNnSc4	potomstvo
vzejde	vzejít	k5eAaPmIp3nS	vzejít
z	z	k7c2	z
"	"	kIx"	"
<g/>
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
<g/>
"	"	kIx"	"
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
vešla	vejít	k5eAaPmAgFnS	vejít
eugenika	eugenika	k1gFnSc1	eugenika
do	do	k7c2	do
lidské	lidský	k2eAgFnSc2d1	lidská
historie	historie	k1gFnSc2	historie
spíše	spíše	k9	spíše
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
negativní	negativní	k2eAgFnSc3d1	negativní
formě	forma	k1gFnSc3	forma
v	v	k7c6	v
ideologii	ideologie	k1gFnSc6	ideologie
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
nedobrovolných	dobrovolný	k2eNgFnPc6d1	nedobrovolná
sterilizacích	sterilizace	k1gFnPc6	sterilizace
mentálně	mentálně	k6eAd1	mentálně
postižených	postižený	k1gMnPc2	postižený
<g/>
,	,	kIx,	,
epileptiků	epileptik	k1gMnPc2	epileptik
a	a	k8xC	a
vězňů	vězeň	k1gMnPc2	vězeň
coby	coby	k?	coby
nositelů	nositel	k1gMnPc2	nositel
"	"	kIx"	"
<g/>
defektních	defektní	k2eAgMnPc2d1	defektní
<g/>
"	"	kIx"	"
a	a	k8xC	a
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
dědičných	dědičný	k2eAgInPc2d1	dědičný
znaků	znak	k1gInPc2	znak
v	v	k7c6	v
USA	USA	kA	USA
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1902	[number]	k4	1902
až	až	k9	až
1964	[number]	k4	1964
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
63	[number]	k4	63
000	[number]	k4	000
sterilizací	sterilizace	k1gFnPc2	sterilizace
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kritikům	kritik	k1gMnPc3	kritik
eugeniky	eugenika	k1gFnSc2	eugenika
se	se	k3xPyFc4	se
nelíbí	líbit	k5eNaImIp3nS	líbit
představa	představa	k1gFnSc1	představa
cílevědomého	cílevědomý	k2eAgNnSc2d1	cílevědomé
šlechtění	šlechtění	k1gNnSc2	šlechtění
lidského	lidský	k2eAgInSc2d1	lidský
genofondu	genofond	k1gInSc2	genofond
<g/>
.	.	kIx.	.
</s>
<s>
Předně	předně	k6eAd1	předně
z	z	k7c2	z
politických	politický	k2eAgInPc2d1	politický
a	a	k8xC	a
hodnotových	hodnotový	k2eAgInPc2d1	hodnotový
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
genotypem	genotyp	k1gInSc7	genotyp
a	a	k8xC	a
fenotypem	fenotyp	k1gInSc7	fenotyp
není	být	k5eNaImIp3nS	být
–	–	k?	–
až	až	k9	až
na	na	k7c4	na
určité	určitý	k2eAgFnPc4d1	určitá
výjimky	výjimka	k1gFnPc4	výjimka
–	–	k?	–
tak	tak	k8xC	tak
přímočará	přímočarý	k2eAgFnSc1d1	přímočará
a	a	k8xC	a
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
soudilo	soudit	k5eAaImAgNnS	soudit
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
též	též	k9	též
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
určovat	určovat	k5eAaImF	určovat
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc4	jaký
lidské	lidský	k2eAgFnPc4d1	lidská
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgMnPc4	ten
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Etické	etický	k2eAgFnPc1d1	etická
otázky	otázka	k1gFnPc1	otázka
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
eugenikou	eugenika	k1gFnSc7	eugenika
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
objevují	objevovat	k5eAaImIp3nP	objevovat
především	především	k9	především
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
in	in	k?	in
vitro	vitro	k6eAd1	vitro
fertilizaci	fertilizace	k1gFnSc4	fertilizace
<g/>
,	,	kIx,	,
preimplantačnímu	preimplantační	k2eAgNnSc3d1	preimplantační
genetickému	genetický	k2eAgNnSc3d1	genetické
testování	testování	k1gNnSc3	testování
embryí	embryo	k1gNnPc2	embryo
a	a	k8xC	a
možnému	možný	k2eAgInSc3d1	možný
rozvoji	rozvoj	k1gInSc3	rozvoj
genové	genový	k2eAgFnSc2d1	genová
terapie	terapie	k1gFnSc2	terapie
<g/>
.	.	kIx.	.
</s>
<s>
Společenské	společenský	k2eAgInPc1d1	společenský
vlivy	vliv	k1gInPc1	vliv
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
zrodu	zrod	k1gInSc6	zrod
eugeniky	eugenika	k1gFnSc2	eugenika
a	a	k8xC	a
na	na	k7c4	na
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
popud	popud	k1gInSc4	popud
eugenika	eugenika	k1gFnSc1	eugenika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jsou	být	k5eAaImIp3nP	být
rasismus	rasismus	k1gInSc1	rasismus
<g/>
,	,	kIx,	,
antisemitismus	antisemitismus	k1gInSc1	antisemitismus
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Vzniku	vznik	k1gInSc3	vznik
eugeniky	eugenika	k1gFnSc2	eugenika
předcházela	předcházet	k5eAaImAgFnS	předcházet
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
polemika	polemika	k1gFnSc1	polemika
o	o	k7c6	o
rozdílnosti	rozdílnost	k1gFnSc6	rozdílnost
lidských	lidský	k2eAgFnPc2d1	lidská
ras	rasa	k1gFnPc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hrabě	hrabě	k1gMnSc1	hrabě
Arthur	Arthur	k1gMnSc1	Arthur
de	de	k?	de
Gobineau	Gobineaa	k1gFnSc4	Gobineaa
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Eseji	esej	k1gFnSc6	esej
o	o	k7c6	o
nerovnosti	nerovnost	k1gFnSc6	nerovnost
lidských	lidský	k2eAgNnPc2d1	lidské
plemen	plemeno	k1gNnPc2	plemeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hybnou	hybný	k2eAgFnSc7d1	hybná
silou	síla	k1gFnSc7	síla
dějin	dějiny	k1gFnPc2	dějiny
byly	být	k5eAaImAgInP	být
vždy	vždy	k6eAd1	vždy
zděděné	zděděný	k2eAgFnPc4d1	zděděná
rasové	rasový	k2eAgFnPc4d1	rasová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
rasa	rasa	k1gFnSc1	rasa
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
jasně	jasně	k6eAd1	jasně
nadřazena	nadřazen	k2eAgFnSc1d1	nadřazena
všem	všecek	k3xTgMnPc3	všecek
ostatním	ostatní	k2eAgMnPc3d1	ostatní
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
fyzicky	fyzicky	k6eAd1	fyzicky
i	i	k8xC	i
duševně	duševně	k6eAd1	duševně
nejhodnotnější	hodnotný	k2eAgFnSc1d3	nejhodnotnější
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
odpor	odpor	k1gInSc1	odpor
vůči	vůči	k7c3	vůči
rasovému	rasový	k2eAgNnSc3d1	rasové
míšení	míšení	k1gNnSc3	míšení
je	být	k5eAaImIp3nS	být
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
starší	starý	k2eAgMnSc1d2	starší
a	a	k8xC	a
hrál	hrát	k5eAaImAgMnS	hrát
roli	role	k1gFnSc4	role
i	i	k9	i
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
a	a	k8xC	a
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
římský	římský	k2eAgMnSc1d1	římský
básník	básník	k1gMnSc1	básník
Claudius	Claudius	k1gMnSc1	Claudius
Claudianus	Claudianus	k1gMnSc1	Claudianus
měl	mít	k5eAaImAgMnS	mít
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
silně	silně	k6eAd1	silně
negativní	negativní	k2eAgInSc1d1	negativní
vztah	vztah	k1gInSc1	vztah
a	a	k8xC	a
napsal	napsat	k5eAaBmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesmí	smět	k5eNaImIp3nS	smět
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
s	s	k7c7	s
africkými	africký	k2eAgMnPc7d1	africký
barbary	barbar	k1gMnPc7	barbar
a	a	k8xC	a
barevný	barevný	k2eAgMnSc1d1	barevný
bastard	bastard	k1gMnSc1	bastard
pošpiní	pošpinit	k5eAaPmIp3nS	pošpinit
kolébku	kolébka	k1gFnSc4	kolébka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
vyšla	vyjít	k5eAaPmAgFnS	vyjít
Darwinova	Darwinův	k2eAgFnSc1d1	Darwinova
kniha	kniha	k1gFnSc1	kniha
O	o	k7c6	o
původu	původ	k1gInSc6	původ
druhů	druh	k1gInPc2	druh
přírodním	přírodní	k2eAgInSc7d1	přírodní
výběrem	výběr	k1gInSc7	výběr
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
"	"	kIx"	"
<g/>
Zachování	zachování	k1gNnSc1	zachování
prospěšných	prospěšný	k2eAgNnPc2d1	prospěšné
plemen	plemeno	k1gNnPc2	plemeno
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
"	"	kIx"	"
a	a	k8xC	a
o	o	k7c6	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
vyšla	vyjít	k5eAaPmAgFnS	vyjít
navazující	navazující	k2eAgFnPc4d1	navazující
práce	práce	k1gFnPc4	práce
O	o	k7c6	o
původu	původ	k1gInSc6	původ
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
původ	původ	k1gInSc4	původ
druhů	druh	k1gMnPc2	druh
aplikovala	aplikovat	k5eAaBmAgFnS	aplikovat
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Některých	některý	k3yIgFnPc2	některý
myšlenek	myšlenka	k1gFnPc2	myšlenka
z	z	k7c2	z
Darwinových	Darwinových	k2eAgFnPc2d1	Darwinových
prací	práce	k1gFnPc2	práce
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
skupina	skupina	k1gFnSc1	skupina
evropských	evropský	k2eAgMnPc2d1	evropský
myslitelů	myslitel	k1gMnPc2	myslitel
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
-	-	kIx~	-
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
učenců	učenec	k1gMnPc2	učenec
známa	znám	k2eAgFnSc1d1	známa
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
sociální	sociální	k2eAgMnPc1d1	sociální
darwinisté	darwinista	k1gMnPc1	darwinista
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
úprava	úprava	k1gFnSc1	úprava
Darwinových	Darwinových	k2eAgFnPc2d1	Darwinových
myšlenek	myšlenka	k1gFnPc2	myšlenka
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používala	používat	k5eAaImAgFnS	používat
k	k	k7c3	k
obhajobě	obhajoba	k1gFnSc3	obhajoba
rasového	rasový	k2eAgNnSc2d1	rasové
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Darwin	Darwin	k1gMnSc1	Darwin
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
tyto	tento	k3xDgMnPc4	tento
své	svůj	k3xOyFgMnPc4	svůj
obdivovatele	obdivovatel	k1gMnPc4	obdivovatel
utvrdil	utvrdit	k5eAaPmAgMnS	utvrdit
v	v	k7c4	v
jejich	jejich	k3xOp3gNnSc4	jejich
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
O	o	k7c6	o
původu	původ	k1gInSc6	původ
člověka	člověk	k1gMnSc2	člověk
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
nebudou	být	k5eNaImBp3nP	být
velké	velký	k2eAgInPc1d1	velký
pokroky	pokrok	k1gInPc1	pokrok
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
vyváženy	vyvážen	k2eAgInPc1d1	vyvážen
promyšlenou	promyšlený	k2eAgFnSc7d1	promyšlená
kontrolou	kontrola	k1gFnSc7	kontrola
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
naruší	narušit	k5eAaPmIp3nS	narušit
to	ten	k3xDgNnSc1	ten
přírodní	přírodní	k2eAgInSc1d1	přírodní
výběr	výběr	k1gInSc1	výběr
a	a	k8xC	a
neschopní	schopný	k2eNgMnPc1d1	neschopný
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
stejné	stejný	k2eAgFnPc4d1	stejná
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
přežití	přežití	k1gNnSc4	přežití
jako	jako	k8xS	jako
ti	ten	k3xDgMnPc1	ten
silní	silný	k2eAgMnPc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Darwin	Darwin	k1gMnSc1	Darwin
sám	sám	k3xTgMnSc1	sám
také	také	k9	také
varuje	varovat	k5eAaImIp3nS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
"	"	kIx"	"
<g/>
nezabráníme	zabránit	k5eNaPmIp1nP	zabránit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
bezohledných	bezohledný	k2eAgFnPc2d1	bezohledná
<g/>
,	,	kIx,	,
ničemných	ničemný	k2eAgFnPc2d1	ničemná
a	a	k8xC	a
jinak	jinak	k6eAd1	jinak
podřadných	podřadný	k2eAgInPc2d1	podřadný
členů	člen	k1gInPc2	člen
společnosti	společnost	k1gFnSc2	společnost
zvyšoval	zvyšovat	k5eAaImAgInS	zvyšovat
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
než	než	k8xS	než
lepší	dobrý	k2eAgFnSc1d2	lepší
třída	třída	k1gFnSc1	třída
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
národ	národ	k1gInSc1	národ
bude	být	k5eAaImBp3nS	být
degenerovat	degenerovat	k5eAaBmF	degenerovat
<g/>
"	"	kIx"	"
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
několika	několik	k4yIc2	několik
staletí	staletí	k1gNnPc2	staletí
civilizované	civilizovaný	k2eAgFnSc2d1	civilizovaná
rasy	rasa	k1gFnSc2	rasa
vyhladí	vyhladit	k5eAaPmIp3nP	vyhladit
a	a	k8xC	a
nahradí	nahradit	k5eAaPmIp3nP	nahradit
rasy	rasa	k1gFnPc1	rasa
divošské	divošský	k2eAgFnPc1d1	divošská
Darwinova	Darwinův	k2eAgFnSc1d1	Darwinova
kniha	kniha	k1gFnSc1	kniha
způsobila	způsobit	k5eAaPmAgFnS	způsobit
také	také	k9	také
rozkvět	rozkvět	k1gInSc4	rozkvět
rasové	rasový	k2eAgFnSc2d1	rasová
antropologie	antropologie	k1gFnSc2	antropologie
–	–	k?	–
vědci	vědec	k1gMnPc7	vědec
začali	začít	k5eAaPmAgMnP	začít
vážit	vážit	k5eAaImF	vážit
lebky	lebka	k1gFnPc4	lebka
a	a	k8xC	a
měřit	měřit	k5eAaImF	měřit
nosy	nos	k1gInPc4	nos
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
části	část	k1gFnPc4	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
zaznamenávat	zaznamenávat	k5eAaImF	zaznamenávat
barvu	barva	k1gFnSc4	barva
vlasů	vlas	k1gInPc2	vlas
a	a	k8xC	a
očí	oko	k1gNnPc2	oko
u	u	k7c2	u
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
zjišťovala	zjišťovat	k5eAaImAgFnS	zjišťovat
Německá	německý	k2eAgFnSc1d1	německá
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
antropologii	antropologie	k1gFnSc4	antropologie
rasové	rasový	k2eAgNnSc1d1	rasové
složení	složení	k1gNnSc1	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
německých	německý	k2eAgInPc2d1	německý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
a	a	k8xC	a
nejhorlivějších	horlivý	k2eAgMnPc2d3	nejhorlivější
propagátorů	propagátor	k1gMnPc2	propagátor
sociálního	sociální	k2eAgInSc2d1	sociální
darwinismu	darwinismus	k1gInSc2	darwinismus
byl	být	k5eAaImAgMnS	být
Darwinův	Darwinův	k2eAgMnSc1d1	Darwinův
bratranec	bratranec	k1gMnSc1	bratranec
Francis	Francis	k1gFnSc2	Francis
Galton	Galton	k1gInSc1	Galton
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
termín	termín	k1gInSc4	termín
eugenika	eugenik	k1gMnSc2	eugenik
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
název	název	k1gInSc1	název
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
pomocí	pomocí	k7c2	pomocí
genetiky	genetika	k1gFnSc2	genetika
zlepšit	zlepšit	k5eAaPmF	zlepšit
kvalitu	kvalita	k1gFnSc4	kvalita
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
založil	založit	k5eAaPmAgInS	založit
Galton	Galton	k1gInSc1	Galton
Národní	národní	k2eAgFnSc4d1	národní
eugenickou	eugenický	k2eAgFnSc4d1	eugenická
laboratoř	laboratoř	k1gFnSc4	laboratoř
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
i	i	k9	i
Společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
eugenickou	eugenický	k2eAgFnSc4d1	eugenická
osvětu	osvěta	k1gFnSc4	osvěta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
si	se	k3xPyFc3	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
kladla	klást	k5eAaImAgFnS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
sterilizaci	sterilizace	k1gFnSc4	sterilizace
duševně	duševně	k6eAd1	duševně
nemocných	nemocný	k1gMnPc2	nemocný
a	a	k8xC	a
neduživých	duživý	k2eNgMnPc2d1	neduživý
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
instituce	instituce	k1gFnPc1	instituce
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
objevily	objevit	k5eAaPmAgFnP	objevit
také	také	k9	také
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Dokumentační	dokumentační	k2eAgFnSc1d1	dokumentační
kancelář	kancelář	k1gFnSc1	kancelář
pro	pro	k7c4	pro
eugeniku	eugenika	k1gFnSc4	eugenika
financovaná	financovaný	k2eAgFnSc1d1	financovaná
z	z	k7c2	z
vysoce	vysoce	k6eAd1	vysoce
vážených	vážený	k2eAgFnPc2d1	Vážená
nadací	nadace	k1gFnPc2	nadace
–	–	k?	–
Rockefellerovy	Rockefellerův	k2eAgFnSc2d1	Rockefellerova
a	a	k8xC	a
Carnegieho	Carnegie	k1gMnSc2	Carnegie
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
eugenických	eugenický	k2eAgInPc2d1	eugenický
principů	princip	k1gInPc2	princip
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
v	v	k7c6	v
USA	USA	kA	USA
prováděla	provádět	k5eAaImAgFnS	provádět
rasová	rasový	k2eAgFnSc1d1	rasová
selekce	selekce	k1gFnSc1	selekce
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Přelomovým	přelomový	k2eAgInSc7d1	přelomový
rokem	rok	k1gInSc7	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pro	pro	k7c4	pro
eugeniky	eugenik	k1gMnPc4	eugenik
rok	rok	k1gInSc4	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
univerzita	univerzita	k1gFnSc1	univerzita
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
první	první	k4xOgInSc4	první
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
eugenický	eugenický	k2eAgInSc4d1	eugenický
kongres	kongres	k1gInSc4	kongres
<g/>
,	,	kIx,	,
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
tři	tři	k4xCgNnPc1	tři
sta	sto	k4xCgNnPc1	sto
přívrženců	přívrženec	k1gMnPc2	přívrženec
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k8xC	i
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
účastníky	účastník	k1gMnPc7	účastník
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k9	i
vynálezce	vynálezce	k1gMnSc1	vynálezce
telefonu	telefon	k1gInSc2	telefon
Alexander	Alexandra	k1gFnPc2	Alexandra
Graham	Graham	k1gMnSc1	Graham
Bell	bell	k1gInSc1	bell
<g/>
,	,	kIx,	,
rektoři	rektor	k1gMnPc1	rektor
Harvardovy	Harvardův	k2eAgFnSc2d1	Harvardova
i	i	k8xC	i
Stanfordovy	Stanfordův	k2eAgFnSc2d1	Stanfordova
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
také	také	k9	také
Winston	Winston	k1gInSc4	Winston
Churchill	Churchilla	k1gFnPc2	Churchilla
<g/>
.	.	kIx.	.
</s>
<s>
Debatovalo	debatovat	k5eAaImAgNnS	debatovat
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zabránit	zabránit	k5eAaPmF	zabránit
méněcenné	méněcenný	k2eAgFnSc3d1	méněcenná
populaci	populace	k1gFnSc3	populace
v	v	k7c6	v
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
a	a	k8xC	a
jak	jak	k6eAd1	jak
podpořit	podpořit	k5eAaPmF	podpořit
porodnost	porodnost	k1gFnSc4	porodnost
u	u	k7c2	u
rasově	rasově	k6eAd1	rasově
kvalitních	kvalitní	k2eAgMnPc2d1	kvalitní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
sklidily	sklidit	k5eAaPmAgInP	sklidit
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
velký	velký	k2eAgInSc4d1	velký
obdiv	obdiv	k1gInSc4	obdiv
účastníků	účastník	k1gMnPc2	účastník
jako	jako	k8xC	jako
vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
světová	světový	k2eAgFnSc1d1	světová
mocnost	mocnost	k1gFnSc1	mocnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
sterilizace	sterilizace	k1gFnSc2	sterilizace
mentálně	mentálně	k6eAd1	mentálně
postižených	postižený	k1gMnPc2	postižený
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
eugenice	eugenika	k1gFnSc3	eugenika
se	se	k3xPyFc4	se
ostře	ostro	k6eAd1	ostro
postavila	postavit	k5eAaPmAgFnS	postavit
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
XI	XI	kA	XI
<g/>
.	.	kIx.	.
ji	on	k3xPp3gFnSc4	on
explicitně	explicitně	k6eAd1	explicitně
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
včetně	včetně	k7c2	včetně
její	její	k3xOp3gFnSc2	její
aplikace	aplikace	k1gFnSc2	aplikace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
v	v	k7c6	v
encyklice	encyklika	k1gFnSc6	encyklika
Casti	Casti	k1gNnSc2	Casti
connubii	connubie	k1gFnSc3	connubie
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
myšlenka	myšlenka	k1gFnSc1	myšlenka
eugeniky	eugenika	k1gFnSc2	eugenika
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
kvalitách	kvalita	k1gFnPc6	kvalita
různých	různý	k2eAgFnPc2d1	různá
ras	rasa	k1gFnPc2	rasa
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
přijímána	přijímat	k5eAaImNgNnP	přijímat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
pravidla	pravidlo	k1gNnPc1	pravidlo
eugeniky	eugenika	k1gFnSc2	eugenika
začala	začít	k5eAaPmAgNnP	začít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nacismu	nacismus	k1gInSc2	nacismus
uvádět	uvádět	k5eAaImF	uvádět
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
následkem	následkem	k7c2	následkem
tohoto	tento	k3xDgInSc2	tento
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejen	nejen	k6eAd1	nejen
projekt	projekt	k1gInSc1	projekt
Lebensborn	Lebensborna	k1gFnPc2	Lebensborna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nucené	nucený	k2eAgFnPc4d1	nucená
sterilizace	sterilizace	k1gFnPc4	sterilizace
postižených	postižený	k1gMnPc2	postižený
či	či	k8xC	či
jejich	jejich	k3xOp3gNnPc2	jejich
usmrcování	usmrcování	k1gNnPc2	usmrcování
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
euthanasii	euthanasie	k1gFnSc6	euthanasie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ovšem	ovšem	k9	ovšem
nemělo	mít	k5eNaImAgNnS	mít
pouze	pouze	k6eAd1	pouze
eugenickou	eugenický	k2eAgFnSc4d1	eugenická
motivaci	motivace	k1gFnSc4	motivace
<g/>
,	,	kIx,	,
usmrcováni	usmrcovat	k5eAaImNgMnP	usmrcovat
byli	být	k5eAaImAgMnP	být
i	i	k9	i
neplodní	plodní	k2eNgMnPc1d1	neplodní
a	a	k8xC	a
sterilizovaní	sterilizovaný	k2eAgMnPc1d1	sterilizovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
snahu	snaha	k1gFnSc4	snaha
se	se	k3xPyFc4	se
zbavit	zbavit	k5eAaPmF	zbavit
"	"	kIx"	"
<g/>
nepotřebých	potřebý	k2eNgInPc2d1	potřebý
<g/>
"	"	kIx"	"
v	v	k7c6	v
době	doba	k1gFnSc6	doba
začátku	začátek	k1gInSc2	začátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
sterilizaci	sterilizace	k1gFnSc6	sterilizace
méněcenných	méněcenný	k2eAgInPc2d1	méněcenný
a	a	k8xC	a
asociálních	asociální	k2eAgInPc2d1	asociální
živlů	živel	k1gInPc2	živel
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
sterilizovat	sterilizovat	k5eAaImF	sterilizovat
zatvrzelé	zatvrzelý	k2eAgMnPc4d1	zatvrzelý
zločince	zločinec	k1gMnPc4	zločinec
a	a	k8xC	a
duševně	duševně	k6eAd1	duševně
choré	chorý	k2eAgFnPc1d1	chorá
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
psychiatrů	psychiatr	k1gMnPc2	psychiatr
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
na	na	k7c4	na
zloděje	zloděj	k1gMnPc4	zloděj
recidivisty	recidivista	k1gMnPc4	recidivista
a	a	k8xC	a
alkoholiky	alkoholik	k1gMnPc4	alkoholik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
byl	být	k5eAaImAgInS	být
sterilizační	sterilizační	k2eAgInSc1d1	sterilizační
zákon	zákon	k1gInSc1	zákon
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vlády	vláda	k1gFnPc1	vláda
ujali	ujmout	k5eAaPmAgMnP	ujmout
nacisté	nacista	k1gMnPc1	nacista
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
přívrženců	přívrženec	k1gMnPc2	přívrženec
sociálního	sociální	k2eAgInSc2d1	sociální
darwinismu	darwinismus	k1gInSc2	darwinismus
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesor	profesor	k1gMnSc1	profesor
zoologie	zoologie	k1gFnSc2	zoologie
Ernst	Ernst	k1gMnSc1	Ernst
Haeckel	Haeckel	k1gMnSc1	Haeckel
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
profesor	profesor	k1gMnSc1	profesor
antropologie	antropologie	k1gFnSc2	antropologie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Jeně	Jena	k1gFnSc6	Jena
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
germánské	germánský	k2eAgInPc1d1	germánský
národy	národ	k1gInPc1	národ
jsou	být	k5eAaImIp3nP	být
nadřazeny	nadřadit	k5eAaPmNgInP	nadřadit
všem	všecek	k3xTgMnPc3	všecek
ostatním	ostatní	k2eAgMnPc3d1	ostatní
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Galton	Galton	k1gInSc4	Galton
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
systematickou	systematický	k2eAgFnSc4d1	systematická
likvidaci	likvidace	k1gFnSc4	likvidace
slabých	slabý	k2eAgMnPc2d1	slabý
a	a	k8xC	a
nemocných	nemocný	k2eAgMnPc2d1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Haeckelovo	Haeckelův	k2eAgNnSc4d1	Haeckelovo
dílo	dílo	k1gNnSc4	dílo
dále	daleko	k6eAd2	daleko
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
jeho	jeho	k3xOp3gMnSc4	jeho
nástupce	nástupce	k1gMnSc4	nástupce
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
sociální	sociální	k2eAgFnSc2d1	sociální
antropologie	antropologie	k1gFnSc2	antropologie
Alfred	Alfred	k1gMnSc1	Alfred
Ploetz	Ploetz	k1gMnSc1	Ploetz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
výraz	výraz	k1gInSc4	výraz
"	"	kIx"	"
<g/>
rasová	rasový	k2eAgFnSc1d1	rasová
hygiena	hygiena	k1gFnSc1	hygiena
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgInPc6d2	pozdější
pracích	prak	k1gInPc6	prak
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
vypracovat	vypracovat	k5eAaPmF	vypracovat
program	program	k1gInSc4	program
populační	populační	k2eAgFnSc2d1	populační
kontroly	kontrola	k1gFnSc2	kontrola
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nS	by
zajistil	zajistit	k5eAaPmAgInS	zajistit
přežití	přežití	k1gNnSc3	přežití
jen	jen	k9	jen
těch	ten	k3xDgFnPc2	ten
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
rasových	rasový	k2eAgFnPc2d1	rasová
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgInS	být
Ploetz	Ploetz	k1gInSc1	Ploetz
mezi	mezi	k7c7	mezi
zakladateli	zakladatel	k1gMnPc7	zakladatel
Spolku	spolek	k1gInSc2	spolek
pro	pro	k7c4	pro
rasovou	rasový	k2eAgFnSc4d1	rasová
hygienu	hygiena	k1gFnSc4	hygiena
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
hlásili	hlásit	k5eAaImAgMnP	hlásit
k	k	k7c3	k
eugenice	eugenika	k1gFnSc3	eugenika
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
radikálnější	radikální	k2eAgInPc4d2	radikálnější
názory	názor	k1gInPc4	názor
než	než	k8xS	než
Ploetz	Ploetz	k1gInSc4	Ploetz
měl	mít	k5eAaImAgMnS	mít
Fritz	Fritz	k1gMnSc1	Fritz
Lenz	Lenz	k1gMnSc1	Lenz
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
prvním	první	k4xOgMnSc6	první
německým	německý	k2eAgMnSc7d1	německý
profesorem	profesor	k1gMnSc7	profesor
rasové	rasový	k2eAgFnSc2d1	rasová
hygieny	hygiena	k1gFnSc2	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Lenz	Lenz	k1gMnSc1	Lenz
podporoval	podporovat	k5eAaImAgMnS	podporovat
nejen	nejen	k6eAd1	nejen
sterilizace	sterilizace	k1gFnSc2	sterilizace
"	"	kIx"	"
<g/>
méněcenných	méněcenný	k2eAgFnPc2d1	méněcenná
<g/>
"	"	kIx"	"
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
kvalitnější	kvalitní	k2eAgMnSc1d2	kvalitnější
<g/>
"	"	kIx"	"
vrstvy	vrstva	k1gFnPc1	vrstva
společnosti	společnost	k1gFnSc2	společnost
musí	muset	k5eAaImIp3nP	muset
plodit	plodit	k5eAaImF	plodit
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
dětí	dítě	k1gFnPc2	dítě
než	než	k8xS	než
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
<s>
Porovnával	porovnávat	k5eAaImAgMnS	porovnávat
také	také	k9	také
rasy	rasa	k1gFnSc2	rasa
a	a	k8xC	a
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
černoši	černoch	k1gMnPc1	černoch
i	i	k8xC	i
Židé	Žid	k1gMnPc1	Žid
jsou	být	k5eAaImIp3nP	být
nekvalitní	kvalitní	k2eNgFnSc7d1	nekvalitní
rasou	rasa	k1gFnSc7	rasa
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nordická	nordický	k2eAgFnSc1d1	nordická
rasa	rasa	k1gFnSc1	rasa
je	být	k5eAaImIp3nS	být
tou	ten	k3xDgFnSc7	ten
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
genetikem	genetik	k1gMnSc7	genetik
Erwinem	Erwin	k1gMnSc7	Erwin
Bauerem	Bauer	k1gMnSc7	Bauer
a	a	k8xC	a
antropologem	antropolog	k1gMnSc7	antropolog
Eugenem	Eugen	k1gMnSc7	Eugen
Fischerem	Fischer	k1gMnSc7	Fischer
sepsal	sepsat	k5eAaPmAgMnS	sepsat
učebnici	učebnice	k1gFnSc4	učebnice
o	o	k7c6	o
rasových	rasový	k2eAgFnPc6d1	rasová
otázkách	otázka	k1gFnPc6	otázka
Nástin	nástin	k1gInSc4	nástin
genetiky	genetika	k1gFnSc2	genetika
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
rasové	rasový	k2eAgFnSc2d1	rasová
hygieny	hygiena	k1gFnSc2	hygiena
(	(	kIx(	(
<g/>
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
kladně	kladně	k6eAd1	kladně
přijata	přijmout	k5eAaPmNgNnP	přijmout
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgMnSc7d1	velký
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
Fritze	Fritze	k1gFnSc2	Fritze
Lenze	Lenze	k1gFnSc1	Lenze
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
nepříliš	příliš	k6eNd1	příliš
známý	známý	k2eAgMnSc1d1	známý
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Lenzova	Lenzův	k2eAgFnSc1d1	Lenzův
kniha	kniha	k1gFnSc1	kniha
Nástin	nástin	k1gInSc4	nástin
genetiky	genetika	k1gFnSc2	genetika
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
rasové	rasový	k2eAgFnSc2d1	rasová
hygieny	hygiena	k1gFnSc2	hygiena
byla	být	k5eAaImAgFnS	být
nadšeně	nadšeně	k6eAd1	nadšeně
přijata	přijmout	k5eAaPmNgFnS	přijmout
řadou	řada	k1gFnSc7	řada
vědců	vědec	k1gMnPc2	vědec
také	také	k9	také
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
pochvalně	pochvalně	k6eAd1	pochvalně
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
třeba	třeba	k8wRxS	třeba
spisovatel	spisovatel	k1gMnSc1	spisovatel
L.	L.	kA	L.
A.	A.	kA	A.
G.	G.	kA	G.
Strong	Strong	k1gMnSc1	Strong
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
v	v	k7c6	v
periodiku	periodikum	k1gNnSc6	periodikum
New	New	k1gFnSc2	New
Statesman	Statesman	k1gMnSc1	Statesman
and	and	k?	and
Nation	Nation	k1gInSc1	Nation
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
myšlenkám	myšlenka	k1gFnPc3	myšlenka
eugeniky	eugenika	k1gFnSc2	eugenika
se	se	k3xPyFc4	se
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
hlásilo	hlásit	k5eAaImAgNnS	hlásit
rovněž	rovněž	k9	rovněž
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
osob	osoba	k1gFnPc2	osoba
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
inteligence	inteligence	k1gFnSc2	inteligence
a	a	k8xC	a
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejhorlivějším	horlivý	k2eAgMnSc7d3	nejhorlivější
zastáncem	zastánce	k1gMnSc7	zastánce
eugeniky	eugenika	k1gFnSc2	eugenika
byl	být	k5eAaImAgMnS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
H.	H.	kA	H.
G.	G.	kA	G.
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dal	dát	k5eAaPmAgInS	dát
průchod	průchod	k1gInSc4	průchod
svým	svůj	k3xOyFgInSc7	svůj
sympatiím	sympatie	k1gFnPc3	sympatie
k	k	k7c3	k
eugenice	eugenika	k1gFnSc3	eugenika
především	především	k9	především
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Tušení	tušení	k1gNnPc2	tušení
(	(	kIx(	(
<g/>
Anticipations	Anticipations	k1gInSc1	Anticipations
<g/>
,	,	kIx,	,
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
bez	bez	k7c2	bez
váhání	váhání	k1gNnSc2	váhání
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
rasistickou	rasistický	k2eAgFnSc4d1	rasistická
a	a	k8xC	a
antisemitskou	antisemitský	k2eAgFnSc4d1	antisemitská
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgInPc1	jaký
jsou	být	k5eAaImIp3nP	být
obsaženy	obsažen	k2eAgInPc1d1	obsažen
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
tehdy	tehdy	k6eAd1	tehdy
nebyly	být	k5eNaImAgFnP	být
zcela	zcela	k6eAd1	zcela
cizí	cizí	k2eAgFnPc1d1	cizí
ani	ani	k8xC	ani
dalším	další	k2eAgFnPc3d1	další
známým	známý	k2eAgFnPc3d1	známá
osobnostem	osobnost	k1gFnPc3	osobnost
-	-	kIx~	-
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
alespoň	alespoň	k9	alespoň
některé	některý	k3yIgFnSc2	některý
<g/>
:	:	kIx,	:
Virginia	Virginium	k1gNnPc4	Virginium
Woolfová	Woolfový	k2eAgNnPc4d1	Woolfové
<g/>
,	,	kIx,	,
T.	T.	kA	T.
S.	S.	kA	S.
Eliot	Eliot	k1gMnSc1	Eliot
<g/>
,	,	kIx,	,
G.	G.	kA	G.
B.	B.	kA	B.
Shaw	Shaw	k1gFnPc2	Shaw
a	a	k8xC	a
především	především	k9	především
Rudyard	Rudyard	k1gInSc1	Rudyard
Kipling	Kipling	k1gInSc1	Kipling
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
protižidovská	protižidovský	k2eAgFnSc1d1	protižidovská
báseň	báseň	k1gFnSc4	báseň
Gehazi	Gehaze	k1gFnSc4	Gehaze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
panovalo	panovat	k5eAaImAgNnS	panovat
zcela	zcela	k6eAd1	zcela
jiné	jiný	k2eAgNnSc4d1	jiné
myšlenkové	myšlenkový	k2eAgNnSc4d1	myšlenkové
klima	klima	k1gNnSc4	klima
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
někteří	některý	k3yIgMnPc1	některý
umělci	umělec	k1gMnPc1	umělec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
také	také	k9	také
někteří	některý	k3yIgMnPc1	některý
filosofové	filosof	k1gMnPc1	filosof
a	a	k8xC	a
vědci	vědec	k1gMnPc1	vědec
zastávali	zastávat	k5eAaImAgMnP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
nově	nově	k6eAd1	nově
uspořádat	uspořádat	k5eAaPmF	uspořádat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
civilizace	civilizace	k1gFnSc1	civilizace
se	s	k7c7	s
vlivem	vliv	k1gInSc7	vliv
překotného	překotný	k2eAgInSc2d1	překotný
rozvoje	rozvoj	k1gInSc2	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
nadměrné	nadměrný	k2eAgFnSc2d1	nadměrná
koncentrace	koncentrace	k1gFnSc2	koncentrace
chudých	chudý	k2eAgMnPc2d1	chudý
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
nerovnoměrného	rovnoměrný	k2eNgNnSc2d1	nerovnoměrné
rozdělování	rozdělování	k1gNnSc2	rozdělování
světového	světový	k2eAgNnSc2d1	světové
bohatství	bohatství	k1gNnSc2	bohatství
řítí	řítit	k5eAaImIp3nS	řítit
do	do	k7c2	do
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
řešení	řešení	k1gNnPc2	řešení
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
problému	problém	k1gInSc3	problém
se	se	k3xPyFc4	se
zdála	zdát	k5eAaImAgFnS	zdát
eugenika	eugenika	k1gFnSc1	eugenika
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
neměli	mít	k5eNaImAgMnP	mít
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
přijetím	přijetí	k1gNnSc7	přijetí
větší	veliký	k2eAgInSc4d2	veliký
problémy	problém	k1gInPc4	problém
i	i	k8xC	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
zcela	zcela	k6eAd1	zcela
promyšleně	promyšleně	k6eAd1	promyšleně
provozovali	provozovat	k5eAaImAgMnP	provozovat
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
dost	dost	k6eAd1	dost
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
a	a	k8xC	a
rasisticky	rasisticky	k6eAd1	rasisticky
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
politiku	politika	k1gFnSc4	politika
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
svých	svůj	k3xOyFgFnPc2	svůj
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
na	na	k7c6	na
okupovaných	okupovaný	k2eAgNnPc6d1	okupované
územích	území	k1gNnPc6	území
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
naprosto	naprosto	k6eAd1	naprosto
neschvalovali	schvalovat	k5eNaImAgMnP	schvalovat
mísení	mísení	k1gNnSc3	mísení
své	svůj	k3xOyFgFnSc2	svůj
rasy	rasa	k1gFnSc2	rasa
s	s	k7c7	s
domorodci	domorodec	k1gMnPc7	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
eugeniky	eugenika	k1gFnSc2	eugenika
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
Angličan	Angličan	k1gMnSc1	Angličan
–	–	k?	–
Houston	Houston	k1gInSc1	Houston
Stewart	Stewart	k1gMnSc1	Stewart
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
<g/>
,	,	kIx,	,
zeť	zeť	k1gMnSc1	zeť
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
.	.	kIx.	.
</s>
<s>
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgMnS	usadit
na	na	k7c6	na
kontinentě	kontinent	k1gInSc6	kontinent
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
knihu	kniha	k1gFnSc4	kniha
Základy	základ	k1gInPc4	základ
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
obdivoval	obdivovat	k5eAaImAgInS	obdivovat
jak	jak	k8xC	jak
H.	H.	kA	H.
G.	G.	kA	G.
Wells	Wells	k1gInSc4	Wells
tak	tak	k6eAd1	tak
i	i	k9	i
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
.	.	kIx.	.
</s>
<s>
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
příslušníci	příslušník	k1gMnPc1	příslušník
vyšších	vysoký	k2eAgFnPc2d2	vyšší
ras	rasa	k1gFnPc2	rasa
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
mísit	mísit	k5eAaImF	mísit
s	s	k7c7	s
příslušníky	příslušník	k1gMnPc7	příslušník
jiného	jiný	k2eAgNnSc2d1	jiné
plemene	plemeno	k1gNnSc2	plemeno
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
li	li	k8xS	li
rasa	rasa	k1gFnSc1	rasa
ztratit	ztratit	k5eAaPmF	ztratit
své	svůj	k3xOyFgFnPc4	svůj
kvality	kvalita	k1gFnPc4	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
antické	antický	k2eAgNnSc1d1	antické
Řecko	Řecko	k1gNnSc1	Řecko
i	i	k8xC	i
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
jen	jen	k6eAd1	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
jejich	jejich	k3xOp3gMnPc2	jejich
obyvatel	obyvatel	k1gMnPc2	obyvatel
došlo	dojít	k5eAaPmAgNnS	dojít
mísením	mísení	k1gNnPc3	mísení
ras	rasa	k1gFnPc2	rasa
k	k	k7c3	k
rasové	rasový	k2eAgFnSc3d1	rasová
degeneraci	degenerace	k1gFnSc3	degenerace
<g/>
.	.	kIx.	.
</s>
<s>
Obvinil	obvinit	k5eAaPmAgMnS	obvinit
také	také	k9	také
Židy	Žid	k1gMnPc4	Žid
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
cíleně	cíleně	k6eAd1	cíleně
mísit	mísit	k5eAaImF	mísit
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
rasou	rasa	k1gFnSc7	rasa
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ji	on	k3xPp3gFnSc4	on
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
tyto	tento	k3xDgFnPc1	tento
teorie	teorie	k1gFnPc1	teorie
fascinovaly	fascinovat	k5eAaBmAgFnP	fascinovat
a	a	k8xC	a
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
Chamberlainových	Chamberlainův	k2eAgFnPc2d1	Chamberlainova
myšlenek	myšlenka	k1gFnPc2	myšlenka
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
i	i	k9	i
v	v	k7c6	v
Hitlerově	Hitlerův	k2eAgFnSc6d1	Hitlerova
knize	kniha	k1gFnSc6	kniha
Mein	Mein	k1gMnSc1	Mein
Kampf	Kampf	k1gMnSc1	Kampf
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Eugenika	eugenik	k1gMnSc2	eugenik
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zabránit	zabránit	k5eAaPmF	zabránit
zvyšování	zvyšování	k1gNnSc4	zvyšování
počtu	počet	k1gInSc2	počet
"	"	kIx"	"
<g/>
podřadných	podřadný	k2eAgInPc2d1	podřadný
členů	člen	k1gInPc2	člen
společnosti	společnost	k1gFnSc2	společnost
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
ujala	ujmout	k5eAaPmAgFnS	ujmout
myšlenky	myšlenka	k1gFnPc4	myšlenka
eugeniky	eugenika	k1gFnSc2	eugenika
i	i	k8xC	i
Američanka	Američanka	k1gFnSc1	Američanka
Margaret	Margareta	k1gFnPc2	Margareta
Sangerová	Sangerová	k1gFnSc1	Sangerová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podporovala	podporovat	k5eAaImAgFnS	podporovat
negativní	negativní	k2eAgFnSc4d1	negativní
eugeniku	eugenika	k1gFnSc4	eugenika
<g/>
,	,	kIx,	,
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidské	lidský	k2eAgFnPc1d1	lidská
dědičné	dědičný	k2eAgFnPc1d1	dědičná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zlepšeny	zlepšit	k5eAaPmNgFnP	zlepšit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
sociální	sociální	k2eAgFnSc2d1	sociální
intervence	intervence	k1gFnSc2	intervence
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
přístupu	přístup	k1gInSc6	přístup
používala	používat	k5eAaImAgFnS	používat
vylučovací	vylučovací	k2eAgFnSc4d1	vylučovací
imigrační	imigrační	k2eAgFnSc4d1	imigrační
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
volného	volný	k2eAgInSc2d1	volný
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
metodám	metoda	k1gFnPc3	metoda
kontroly	kontrola	k1gFnSc2	kontrola
porodnosti	porodnost	k1gFnSc2	porodnost
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
volné	volný	k2eAgNnSc4d1	volné
plánování	plánování	k1gNnSc4	plánování
rodičovství	rodičovství	k1gNnSc2	rodičovství
pro	pro	k7c4	pro
mentálně	mentálně	k6eAd1	mentálně
zdatné	zdatný	k2eAgFnPc4d1	zdatná
a	a	k8xC	a
povinné	povinný	k2eAgFnPc4d1	povinná
segregace	segregace	k1gFnPc4	segregace
nebo	nebo	k8xC	nebo
sterilizace	sterilizace	k1gFnPc4	sterilizace
pro	pro	k7c4	pro
hluboce	hluboko	k6eAd1	hluboko
retardované	retardovaný	k2eAgFnPc4d1	retardovaná
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Československá	československý	k2eAgFnSc1d1	Československá
<g/>
)	)	kIx)	)
eugenická	eugenický	k2eAgFnSc1d1	eugenická
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
ČES	ČES	kA	ČES
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
František	František	k1gMnSc1	František
Čáda	Čáda	k1gMnSc1	Čáda
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
a	a	k8xC	a
mezi	mezi	k7c7	mezi
jejími	její	k3xOp3gMnPc7	její
prvorepublikovými	prvorepublikový	k2eAgMnPc7d1	prvorepublikový
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Ladislav	Ladislav	k1gMnSc1	Ladislav
Haškovec	Haškovec	k1gMnSc1	Haškovec
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
-	-	kIx~	-
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladislav	Vladislav	k1gMnSc1	Vladislav
Růžička	Růžička	k1gMnSc1	Růžička
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Herfort	Herfort	k1gInSc1	Herfort
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kříženecký	kříženecký	k2eAgMnSc1d1	kříženecký
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Drachovský	Drachovský	k2eAgMnSc1d1	Drachovský
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
či	či	k8xC	či
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
ČES	ČES	kA	ČES
neúspěšně	úspěšně	k6eNd1	úspěšně
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
v	v	k7c6	v
legislativě	legislativa	k1gFnSc6	legislativa
např.	např.	kA	např.
povinné	povinný	k2eAgNnSc1d1	povinné
lékařské	lékařský	k2eAgNnSc1d1	lékařské
vysvědčení	vysvědčení	k1gNnSc1	vysvědčení
před	před	k7c7	před
sňatkem	sňatek	k1gInSc7	sňatek
a	a	k8xC	a
zavedení	zavedení	k1gNnSc1	zavedení
možnosti	možnost	k1gFnSc2	možnost
přerušení	přerušení	k1gNnSc2	přerušení
těhotenství	těhotenství	k1gNnSc2	těhotenství
na	na	k7c6	na
základě	základ	k1gInSc6	základ
eugenické	eugenický	k2eAgFnSc2d1	eugenická
indikace	indikace	k1gFnSc2	indikace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
založen	založen	k2eAgInSc1d1	založen
výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
a	a	k8xC	a
propagační	propagační	k2eAgInSc1d1	propagační
Československý	československý	k2eAgInSc1d1	československý
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
národní	národní	k2eAgFnSc4d1	národní
eugeniku	eugenika	k1gFnSc4	eugenika
<g/>
.	.	kIx.	.
</s>
<s>
ČES	ČES	kA	ČES
dále	daleko	k6eAd2	daleko
provozoval	provozovat	k5eAaImAgMnS	provozovat
při	při	k7c6	při
univerzitní	univerzitní	k2eAgFnSc6d1	univerzitní
poliklinice	poliklinika	k1gFnSc6	poliklinika
Poradnu	poradna	k1gFnSc4	poradna
pro	pro	k7c4	pro
dědičné	dědičný	k2eAgFnPc4d1	dědičná
choroby	choroba	k1gFnPc4	choroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zavedením	zavedení	k1gNnSc7	zavedení
sterilizačního	sterilizační	k2eAgInSc2d1	sterilizační
zákona	zákon	k1gInSc2	zákon
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
obnovena	obnoven	k2eAgFnSc1d1	obnovena
odborná	odborný	k2eAgFnSc1d1	odborná
debata	debata	k1gFnSc1	debata
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
zavést	zavést	k5eAaPmF	zavést
podobný	podobný	k2eAgInSc4d1	podobný
zákon	zákon	k1gInSc4	zákon
i	i	k9	i
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
byl	být	k5eAaImAgInS	být
připravován	připravován	k2eAgInSc1d1	připravován
návrh	návrh	k1gInSc1	návrh
samostatného	samostatný	k2eAgInSc2d1	samostatný
československého	československý	k2eAgInSc2d1	československý
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
eugenické	eugenický	k2eAgFnSc6d1	eugenická
sterilizaci	sterilizace	k1gFnSc6	sterilizace
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgMnSc6	jenž
pracovali	pracovat	k5eAaImAgMnP	pracovat
zejména	zejména	k9	zejména
Bohumil	Bohumil	k1gMnSc1	Bohumil
Sekla	seknout	k5eAaImAgFnS	seknout
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bergauer	Bergauer	k1gMnSc1	Bergauer
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
-	-	kIx~	-
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
a	a	k8xC	a
právnička	právnička	k1gFnSc1	právnička
Jarmila	Jarmila	k1gFnSc1	Jarmila
Veselá	Veselá	k1gFnSc1	Veselá
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
-	-	kIx~	-
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
vykonání	vykonání	k1gNnSc4	vykonání
sterilizačního	sterilizační	k2eAgInSc2d1	sterilizační
zákroku	zákrok	k1gInSc2	zákrok
na	na	k7c6	na
základě	základ	k1gInSc6	základ
svolení	svolení	k1gNnSc2	svolení
dané	daný	k2eAgFnSc2d1	daná
osoby	osoba	k1gFnSc2	osoba
za	za	k7c2	za
dohledu	dohled	k1gInSc2	dohled
odborníků	odborník	k1gMnPc2	odborník
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
lékařů-eugeniků	lékařůugenik	k1gMnPc2	lékařů-eugenik
a	a	k8xC	a
sociálních	sociální	k2eAgMnPc2d1	sociální
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
chirurgické	chirurgický	k2eAgInPc1d1	chirurgický
zákroky	zákrok	k1gInPc1	zákrok
byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
vasoligatura	vasoligatura	k1gFnSc1	vasoligatura
a	a	k8xC	a
vasectomie	vasectomie	k1gFnSc1	vasectomie
(	(	kIx(	(
<g/>
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
a	a	k8xC	a
salpingektomie	salpingektomie	k1gFnSc1	salpingektomie
(	(	kIx(	(
<g/>
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nedostal	dostat	k5eNaPmAgInS	dostat
ani	ani	k8xC	ani
do	do	k7c2	do
paragrafované	paragrafovaný	k2eAgFnSc2d1	paragrafovaná
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
zabývajících	zabývající	k2eAgMnPc2d1	zabývající
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
eugenikou	eugenika	k1gFnSc7	eugenika
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
odborníky	odborník	k1gMnPc4	odborník
populační	populační	k2eAgFnSc2d1	populační
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
genetiky	genetik	k1gMnPc4	genetik
člověka	člověk	k1gMnSc4	člověk
či	či	k8xC	či
sociobiology	sociobiolog	k1gMnPc4	sociobiolog
<g/>
.	.	kIx.	.
</s>
<s>
Přejmenovány	přejmenován	k2eAgInPc1d1	přejmenován
byly	být	k5eAaImAgInP	být
i	i	k9	i
odborné	odborný	k2eAgInPc1d1	odborný
časopisy	časopis	k1gInPc1	časopis
a	a	k8xC	a
organizace	organizace	k1gFnPc1	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Annals	Annalsa	k1gFnPc2	Annalsa
of	of	k?	of
Eugenics	Eugenicsa	k1gFnPc2	Eugenicsa
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
Annals	Annals	k1gInSc4	Annals
of	of	k?	of
Human	Human	k1gInSc1	Human
Genetics	Genetics	k1gInSc1	Genetics
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
Dokumentační	dokumentační	k2eAgFnSc1d1	dokumentační
kancelář	kancelář	k1gFnSc1	kancelář
pro	pro	k7c4	pro
eugeniku	eugenika	k1gFnSc4	eugenika
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
laboratoř	laboratoř	k1gFnSc4	laboratoř
Cold	Colda	k1gFnPc2	Colda
Spring	Spring	k1gInSc1	Spring
Harbor	Harbor	k1gMnSc1	Harbor
a	a	k8xC	a
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
Dokumentační	dokumentační	k2eAgFnSc1d1	dokumentační
kancelář	kancelář	k1gFnSc1	kancelář
pro	pro	k7c4	pro
eugeniku	eugenika	k1gFnSc4	eugenika
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Galtonovu	Galtonův	k2eAgFnSc4d1	Galtonova
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Eugenická	eugenický	k2eAgFnSc1d1	eugenická
společnost	společnost	k1gFnSc1	společnost
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
Galtonovým	Galtonový	k2eAgInSc7d1	Galtonový
institutem	institut	k1gInSc7	institut
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
nacistickém	nacistický	k2eAgNnSc6d1	nacistické
Německu	Německo	k1gNnSc6	Německo
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
rasově	rasově	k6eAd1	rasově
motivovaných	motivovaný	k2eAgInPc6d1	motivovaný
pokusech	pokus	k1gInPc6	pokus
na	na	k7c6	na
lidech	člověk	k1gMnPc6	člověk
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgFnP	moct
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
výzkumech	výzkum	k1gInPc6	výzkum
pokračovat	pokračovat	k5eAaImF	pokračovat
i	i	k8xC	i
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
už	už	k6eAd1	už
opravdu	opravdu	k6eAd1	opravdu
vědeckými	vědecký	k2eAgFnPc7d1	vědecká
metodami	metoda	k1gFnPc7	metoda
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nacistické	nacistický	k2eAgFnPc4d1	nacistická
eugeniky	eugenika	k1gFnPc4	eugenika
patřil	patřit	k5eAaImAgMnS	patřit
také	také	k9	také
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Robert	Robert	k1gMnSc1	Robert
Ritter	Ritter	k1gMnSc1	Ritter
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
ředitel	ředitel	k1gMnSc1	ředitel
Centra	centrum	k1gNnSc2	centrum
pro	pro	k7c4	pro
rasovou	rasový	k2eAgFnSc4d1	rasová
hygienu	hygiena	k1gFnSc4	hygiena
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
dědičnosti	dědičnost	k1gFnSc2	dědičnost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
jako	jako	k8xS	jako
dětský	dětský	k2eAgMnSc1d1	dětský
psycholog	psycholog	k1gMnSc1	psycholog
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
nacistických	nacistický	k2eAgMnPc2d1	nacistický
eugeniků	eugenik	k1gMnPc2	eugenik
–	–	k?	–
třeba	třeba	k6eAd1	třeba
Fritz	Fritz	k1gMnSc1	Fritz
Lenz	Lenz	k1gMnSc1	Lenz
nebo	nebo	k8xC	nebo
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schade	Schad	k1gInSc5	Schad
–	–	k?	–
získala	získat	k5eAaPmAgFnS	získat
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
znovu	znovu	k6eAd1	znovu
profesury	profesura	k1gFnSc2	profesura
na	na	k7c6	na
německých	německý	k2eAgFnPc6d1	německá
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
genetiky	genetika	k1gFnSc2	genetika
člověka	člověk	k1gMnSc2	člověk
nebo	nebo	k8xC	nebo
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
již	již	k6eAd1	již
rasové	rasový	k2eAgFnSc2d1	rasová
hygieny	hygiena	k1gFnSc2	hygiena
a	a	k8xC	a
eugeniky	eugenika	k1gFnSc2	eugenika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rasových	rasový	k2eAgFnPc2d1	rasová
teorií	teorie	k1gFnPc2	teorie
a	a	k8xC	a
přístupů	přístup	k1gInPc2	přístup
vypracovaných	vypracovaný	k2eAgFnPc2d1	vypracovaná
nacistickými	nacistický	k2eAgMnPc7d1	nacistický
příznivci	příznivec	k1gMnPc7	příznivec
eugeniky	eugenika	k1gFnSc2	eugenika
využívalo	využívat	k5eAaImAgNnS	využívat
dokonce	dokonce	k9	dokonce
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
příznivci	příznivec	k1gMnPc1	příznivec
eugeniky	eugenika	k1gFnSc2	eugenika
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
vědců	vědec	k1gMnPc2	vědec
již	již	k6eAd1	již
nevyvolávají	vyvolávat	k5eNaImIp3nP	vyvolávat
příliš	příliš	k6eAd1	příliš
kontroverzní	kontroverzní	k2eAgNnPc4d1	kontroverzní
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tak	tak	k9	tak
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
základů	základ	k1gInPc2	základ
eugeniky	eugenika	k1gFnSc2	eugenika
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
například	například	k6eAd1	například
vědecky	vědecky	k6eAd1	vědecky
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
inteligence	inteligence	k1gFnSc1	inteligence
jedince	jedinec	k1gMnSc2	jedinec
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
dědičností	dědičnost	k1gFnSc7	dědičnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
někteří	některý	k3yIgMnPc1	některý
jejich	jejich	k3xOp3gMnPc1	jejich
oponenti	oponent	k1gMnPc1	oponent
nesouhlasí	souhlasit	k5eNaImIp3nP	souhlasit
a	a	k8xC	a
kladou	klást	k5eAaImIp3nP	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
roli	role	k1gFnSc4	role
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
eugeniky	eugenika	k1gFnSc2	eugenika
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
částečně	částečně	k6eAd1	částečně
přesunuly	přesunout	k5eAaPmAgInP	přesunout
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
oblastí	oblast	k1gFnPc2	oblast
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
především	především	k9	především
genetiky	genetika	k1gFnSc2	genetika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
občas	občas	k6eAd1	občas
k	k	k7c3	k
vědeckým	vědecký	k2eAgInPc3d1	vědecký
sporům	spor	k1gInPc3	spor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
společenský	společenský	k2eAgInSc4d1	společenský
podtext	podtext	k1gInSc4	podtext
<g/>
:	:	kIx,	:
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
hledá	hledat	k5eAaImIp3nS	hledat
gen	gen	k1gInSc1	gen
zločinnosti	zločinnost	k1gFnSc2	zločinnost
<g/>
,	,	kIx,	,
gen	gen	k1gInSc4	gen
agresivity	agresivita	k1gFnSc2	agresivita
nebo	nebo	k8xC	nebo
gen	gen	k1gInSc1	gen
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dnes	dnes	k6eAd1	dnes
ještě	ještě	k6eAd1	ještě
vědecká	vědecký	k2eAgFnSc1d1	vědecká
a	a	k8xC	a
eticky	eticky	k6eAd1	eticky
kontrolovatelná	kontrolovatelný	k2eAgFnSc1d1	kontrolovatelná
snaha	snaha	k1gFnSc1	snaha
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
čistě	čistě	k6eAd1	čistě
teoreticky	teoreticky	k6eAd1	teoreticky
použita	použít	k5eAaPmNgFnS	použít
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zbavit	zbavit	k5eAaPmF	zbavit
lidstvo	lidstvo	k1gNnSc4	lidstvo
těch	ten	k3xDgMnPc2	ten
"	"	kIx"	"
<g/>
nesprávných	správný	k2eNgInPc2d1	nesprávný
<g/>
"	"	kIx"	"
genů	gen	k1gInPc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
eugenika	eugenika	k1gFnSc1	eugenika
v	v	k7c6	v
civilizovaných	civilizovaný	k2eAgFnPc6d1	civilizovaná
zemích	zem	k1gFnPc6	zem
omezuje	omezovat	k5eAaImIp3nS	omezovat
zpravidla	zpravidla	k6eAd1	zpravidla
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
těhotenství	těhotenství	k1gNnSc6	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
skupina	skupina	k1gFnSc1	skupina
metod	metoda	k1gFnPc2	metoda
používá	používat	k5eAaImIp3nS	používat
diagnostiku	diagnostika	k1gFnSc4	diagnostika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
dědičné	dědičný	k2eAgFnPc4d1	dědičná
choroby	choroba	k1gFnPc4	choroba
a	a	k8xC	a
vady	vada	k1gFnPc4	vada
u	u	k7c2	u
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Vadné	vadný	k2eAgInPc1d1	vadný
plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
doporučovány	doporučovat	k5eAaImNgFnP	doporučovat
k	k	k7c3	k
interrupci	interrupce	k1gFnSc3	interrupce
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
zatím	zatím	k6eAd1	zatím
spíše	spíše	k9	spíše
věcí	věc	k1gFnSc7	věc
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
cílené	cílený	k2eAgNnSc1d1	cílené
"	"	kIx"	"
<g/>
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
"	"	kIx"	"
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
umělým	umělý	k2eAgNnSc7d1	umělé
oplodněním	oplodnění	k1gNnSc7	oplodnění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zatím	zatím	k6eAd1	zatím
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
výběru	výběr	k1gInSc2	výběr
vhodného	vhodný	k2eAgMnSc2d1	vhodný
dárce	dárce	k1gMnSc2	dárce
vajíček	vajíčko	k1gNnPc2	vajíčko
či	či	k8xC	či
spermatu	sperma	k1gNnSc2	sperma
a	a	k8xC	a
případně	případně	k6eAd1	případně
testování	testování	k1gNnSc2	testování
a	a	k8xC	a
výběru	výběr	k1gInSc2	výběr
z	z	k7c2	z
většího	veliký	k2eAgInSc2d2	veliký
počtu	počet	k1gInSc2	počet
embryí	embryo	k1gNnPc2	embryo
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
umělým	umělý	k2eAgInSc7d1	umělý
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
výhledově	výhledově	k6eAd1	výhledově
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
rozvoje	rozvoj	k1gInSc2	rozvoj
genetiky	genetika	k1gFnSc2	genetika
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
i	i	k9	i
zásahy	zásah	k1gInPc1	zásah
do	do	k7c2	do
genomu	genom	k1gInSc2	genom
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
,	,	kIx,	,
embryí	embryo	k1gNnPc2	embryo
či	či	k8xC	či
zygot	zygota	k1gFnPc2	zygota
<g/>
,	,	kIx,	,
minimálně	minimálně	k6eAd1	minimálně
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
odstranění	odstranění	k1gNnSc4	odstranění
genetických	genetický	k2eAgFnPc2d1	genetická
vad	vada	k1gFnPc2	vada
a	a	k8xC	a
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
oblastí	oblast	k1gFnSc7	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
projevuje	projevovat	k5eAaImIp3nS	projevovat
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
eugenickým	eugenický	k2eAgInSc7d1	eugenický
charakterem	charakter	k1gInSc7	charakter
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sterilizace	sterilizace	k1gFnPc4	sterilizace
mentálně	mentálně	k6eAd1	mentálně
postižených	postižený	k1gMnPc2	postižený
či	či	k8xC	či
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
jen	jen	k9	jen
omezeně	omezeně	k6eAd1	omezeně
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
legislativním	legislativní	k2eAgInSc7d1	legislativní
důvodem	důvod	k1gInSc7	důvod
omezení	omezení	k1gNnSc2	omezení
je	být	k5eAaImIp3nS	být
Úmluva	úmluva	k1gFnSc1	úmluva
o	o	k7c6	o
lidských	lidský	k2eAgNnPc6d1	lidské
právech	právo	k1gNnPc6	právo
a	a	k8xC	a
biomedicíně	biomedicína	k1gFnSc6	biomedicína
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejíhož	jejíž	k3xOyRp3gInSc2	jejíž
článku	článek	k1gInSc2	článek
5	[number]	k4	5
jednoznačně	jednoznačně	k6eAd1	jednoznačně
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nelze	lze	k6eNd1	lze
sterilizovat	sterilizovat	k5eAaImF	sterilizovat
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
:	:	kIx,	:
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-204-0460-0	[number]	k4	80-204-0460-0
Ivo	Ivo	k1gMnSc1	Ivo
T.	T.	kA	T.
Budil	Budil	k1gMnSc1	Budil
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Blažek	Blažek	k1gMnSc1	Blažek
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Sládek	Sládek	k1gMnSc1	Sládek
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
<g/>
,	,	kIx,	,
rasa	rasa	k1gFnSc1	rasa
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Králová	Králová	k1gFnSc1	Králová
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-903412-4-1	[number]	k4	80-903412-4-1
<g />
.	.	kIx.	.
</s>
<s>
Stephen	Stephen	k2eAgMnSc1d1	Stephen
Jay	Jay	k1gMnSc1	Jay
Gould	Gould	k1gMnSc1	Gould
<g/>
:	:	kIx,	:
Jak	jak	k8xC	jak
neměřit	měřit	k5eNaImF	měřit
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7106-168-9	[number]	k4	80-7106-168-9
Catrine	Catrin	k1gInSc5	Catrin
Clayová	Clayová	k1gFnSc5	Clayová
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Leapman	Leapman	k1gMnSc1	Leapman
<g/>
:	:	kIx,	:
Panská	panský	k2eAgFnSc1d1	Panská
rasa	rasa	k1gFnSc1	rasa
-	-	kIx~	-
Nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
experiment	experiment	k1gInSc1	experiment
Lebensborn	Lebensborna	k1gFnPc2	Lebensborna
<g/>
,	,	kIx,	,
Columbus	Columbus	k1gInSc1	Columbus
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85928-43-4	[number]	k4	80-85928-43-4
Dědičnost	dědičnost	k1gFnSc1	dědičnost
Dědičnost	dědičnost	k1gFnSc1	dědičnost
proti	proti	k7c3	proti
prostředí	prostředí	k1gNnSc3	prostředí
Kasta	kasta	k1gFnSc1	kasta
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
eugenika	eugenik	k1gMnSc2	eugenik
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Eugenika	eugenika	k1gFnSc1	eugenika
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
