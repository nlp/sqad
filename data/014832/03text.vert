<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
(	(	kIx(
<g/>
Brod	Brod	k1gInSc1
nad	nad	k7c7
Dyjí	Dyje	k1gFnSc7
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckéhov	Nepomuckéhov	k1gInSc4
Brodu	Brod	k1gInSc2
nad	nad	k7c7
Dyjí	Dyje	k1gFnSc7
kostel	kostel	k1gInSc4
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
NepomuckéhoMísto	NepomuckéhoMísta	k1gMnSc5
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Břeclav	Břeclav	k1gFnSc1
Obec	obec	k1gFnSc1
</s>
<s>
Brod	Brod	k1gInSc1
nad	nad	k7c7
Dyjí	Dyje	k1gFnSc7
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
32,93	32,93	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
16,93	16,93	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
moravská	moravský	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
brněnská	brněnský	k2eAgFnSc1d1
Děkanát	děkanát	k1gInSc1
</s>
<s>
mikulovský	mikulovský	k2eAgInSc1d1
Farnost	farnost	k1gFnSc1
</s>
<s>
Brod	Brod	k1gInSc1
nad	nad	k7c7
Dyjí	Dyje	k1gFnSc7
Užívání	užívání	k1gNnSc1
</s>
<s>
farní	farní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
Datum	datum	k1gNnSc4
posvěcení	posvěcení	k1gNnSc2
</s>
<s>
1781	#num#	k4
Další	další	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Kód	kód	k1gInSc4
památky	památka	k1gFnSc2
</s>
<s>
18265	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
1151	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
je	být	k5eAaImIp3nS
římskokatolický	římskokatolický	k2eAgInSc1d1
chrám	chrám	k1gInSc1
v	v	k7c6
obci	obec	k1gFnSc6
Brod	Brod	k1gInSc4
nad	nad	k7c7
Dyjí	Dyje	k1gFnSc7
v	v	k7c6
okrese	okres	k1gInSc6
Břeclav	Břeclav	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xC,k8xS
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
vystavěn	vystavěn	k2eAgInSc1d1
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
osmnáctého	osmnáctý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
věži	věž	k1gFnSc6
je	být	k5eAaImIp3nS
letopočet	letopočet	k1gInSc4
1781	#num#	k4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
o	o	k7c6
tom	ten	k3xDgNnSc6
svědčí	svědčit	k5eAaImIp3nS
letopočet	letopočet	k1gInSc1
na	na	k7c6
věži	věž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
dvacátém	dvacátý	k4xOgInSc6
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
rozšířena	rozšířit	k5eAaPmNgFnS
hudební	hudební	k2eAgFnSc1d1
kruchta	kruchta	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jednolodní	jednolodní	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
s	s	k7c7
odsazeným	odsazený	k2eAgNnSc7d1
kněžištěm	kněžiště	k1gNnSc7
<g/>
,	,	kIx,
k	k	k7c3
jehož	jehož	k3xOyRp3gFnSc3
severní	severní	k2eAgFnSc3d1
zdi	zeď	k1gFnSc3
přiléhá	přiléhat	k5eAaImIp3nS
čtyřboká	čtyřboký	k2eAgFnSc1d1
sakristie	sakristie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fasády	fasáda	k1gFnPc1
člení	členit	k5eAaImIp3nP
hluboké	hluboký	k2eAgFnPc1d1
vpadlé	vpadlý	k2eAgFnPc1d1
výplně	výplň	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgFnPc6
jsou	být	k5eAaImIp3nP
prolomena	prolomit	k5eAaPmNgNnP
okna	okno	k1gNnPc1
se	s	k7c7
segmentovým	segmentový	k2eAgInSc7d1
záklenkem	záklenek	k1gInSc7
<g/>
.	.	kIx.
zařízení	zařízení	k1gNnSc1
je	být	k5eAaImIp3nS
klasicistní	klasicistní	k2eAgNnSc1d1
z	z	k7c2
doby	doba	k1gFnSc2
vzniku	vznik	k1gInSc2
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
farní	farní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
farnosti	farnost	k1gFnSc2
Brod	Brod	k1gInSc4
nad	nad	k7c7
Dyjí	Dyje	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
128923	#num#	k4
:	:	kIx,
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
1	#num#	k4
2	#num#	k4
SAMEK	Samek	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umělecké	umělecký	k2eAgFnPc4d1
památky	památka	k1gFnPc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
(	(	kIx(
<g/>
A	a	k9
<g/>
–	–	k?
<g/>
I	I	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
474	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
252	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Brod	Brod	k1gInSc1
nad	nad	k7c7
Dyjí	Dyje	k1gFnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biskupstvi	Biskupstev	k1gFnSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
</s>
