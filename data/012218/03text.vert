<p>
<s>
Wilsonova	Wilsonův	k2eAgFnSc1d1	Wilsonova
mlžná	mlžný	k2eAgFnSc1d1	mlžná
komora	komora	k1gFnSc1	komora
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
přístroj	přístroj	k1gInSc1	přístroj
umožňující	umožňující	k2eAgInSc1d1	umožňující
pozorovat	pozorovat	k5eAaImF	pozorovat
dráhy	dráha	k1gFnPc4	dráha
elektricky	elektricky	k6eAd1	elektricky
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
prolétávající	prolétávající	k2eAgFnSc1d1	prolétávající
vzduchem	vzduch	k1gInSc7	vzduch
obsahujícím	obsahující	k2eAgMnSc7d1	obsahující
podchlazené	podchlazený	k2eAgFnSc2d1	podchlazená
páry	pára	k1gFnPc1	pára
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zanechávají	zanechávat	k5eAaImIp3nP	zanechávat
stopu	stopa	k1gFnSc4	stopa
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vysrážených	vysrážený	k2eAgFnPc2d1	vysrážená
kapiček	kapička	k1gFnPc2	kapička
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
stopy	stopa	k1gFnPc1	stopa
lze	lze	k6eAd1	lze
následně	následně	k6eAd1	následně
vyfotografovat	vyfotografovat	k5eAaPmF	vyfotografovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Charles	Charles	k1gMnSc1	Charles
Thomson	Thomson	k1gMnSc1	Thomson
Rees	Reesa	k1gFnPc2	Reesa
Wilson	Wilson	k1gInSc4	Wilson
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
pozorování	pozorování	k1gNnSc4	pozorování
jím	on	k3xPp3gMnSc7	on
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
mlžná	mlžný	k2eAgFnSc1d1	mlžná
komora	komora	k1gFnSc1	komora
postupně	postupně	k6eAd1	postupně
vytlačena	vytlačit	k5eAaPmNgFnS	vytlačit
dokonalejší	dokonalý	k2eAgFnSc7d2	dokonalejší
bublinkovou	bublinkový	k2eAgFnSc7d1	bublinková
komorou	komora	k1gFnSc7	komora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Wilsonova	Wilsonův	k2eAgNnSc2d1	Wilsonovo
vyjádření	vyjádření	k1gNnSc2	vyjádření
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
popud	popud	k1gInSc4	popud
k	k	k7c3	k
vynálezu	vynález	k1gInSc3	vynález
při	pře	k1gFnSc4	pře
jeho	on	k3xPp3gInSc2	on
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
observatoři	observatoř	k1gFnSc6	observatoř
na	na	k7c6	na
skotském	skotský	k2eAgInSc6d1	skotský
vrchu	vrch	k1gInSc6	vrch
Ben	Ben	k1gInSc1	Ben
Nevis	viset	k5eNaImRp2nS	viset
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
</s>
<s>
Pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
tam	tam	k6eAd1	tam
různé	různý	k2eAgInPc4d1	různý
světelné	světelný	k2eAgInPc4d1	světelný
jevy	jev	k1gInPc4	jev
na	na	k7c6	na
mlze	mlha	k1gFnSc6	mlha
a	a	k8xC	a
mracích	mrak	k1gInPc6	mrak
<g/>
,	,	kIx,	,
obklopujících	obklopující	k2eAgInPc2d1	obklopující
často	často	k6eAd1	často
observatoř	observatoř	k1gFnSc1	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Cambridge	Cambridge	k1gFnSc2	Cambridge
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
jevy	jev	k1gInPc7	jev
uměle	uměle	k6eAd1	uměle
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
sestavil	sestavit	k5eAaPmAgMnS	sestavit
svou	svůj	k3xOyFgFnSc4	svůj
aparaturu	aparatura	k1gFnSc4	aparatura
z	z	k7c2	z
několika	několik	k4yIc2	několik
lahví	lahev	k1gFnPc2	lahev
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
během	během	k7c2	během
krátkého	krátký	k2eAgInSc2d1	krátký
okamžiku	okamžik	k1gInSc2	okamžik
snížit	snížit	k5eAaPmF	snížit
tlak	tlak	k1gInSc4	tlak
vzduchu	vzduch	k1gInSc2	vzduch
o	o	k7c4	o
desítky	desítka	k1gFnPc4	desítka
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
expanzi	expanze	k1gFnSc3	expanze
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
teplota	teplota	k1gFnSc1	teplota
sytých	sytý	k2eAgFnPc2d1	sytá
par	para	k1gFnPc2	para
pod	pod	k7c4	pod
rosný	rosný	k2eAgInSc4d1	rosný
bod	bod	k1gInSc4	bod
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
mlha	mlha	k1gFnSc1	mlha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
prací	práce	k1gFnPc2	práce
Johna	John	k1gMnSc2	John
Aitkena	Aitken	k1gMnSc2	Aitken
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
podchlazené	podchlazený	k2eAgInPc1d1	podchlazený
páry	pár	k1gInPc1	pár
se	se	k3xPyFc4	se
srážejí	srážet	k5eAaImIp3nP	srážet
na	na	k7c6	na
kondenzačních	kondenzační	k2eAgNnPc6d1	kondenzační
jádrech	jádro	k1gNnPc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
pokusů	pokus	k1gInPc2	pokus
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
vysrážení	vysrážení	k1gNnSc3	vysrážení
vody	voda	k1gFnSc2	voda
kolem	kolem	k7c2	kolem
částic	částice	k1gFnPc2	částice
prachu	prach	k1gInSc2	prach
tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
smyly	smýt	k5eAaPmAgFnP	smýt
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
mlha	mlha	k1gFnSc1	mlha
dále	daleko	k6eAd2	daleko
při	při	k7c6	při
poklesu	pokles	k1gInSc6	pokles
tlaku	tlak	k1gInSc2	tlak
nevznikala	vznikat	k5eNaImAgFnS	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Wilson	Wilson	k1gMnSc1	Wilson
dále	daleko	k6eAd2	daleko
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
po	po	k7c6	po
vyčištění	vyčištění	k1gNnSc6	vyčištění
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
při	při	k7c6	při
expanzi	expanze	k1gFnSc6	expanze
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
%	%	kIx~	%
začala	začít	k5eAaPmAgFnS	začít
mlha	mlha	k1gFnSc1	mlha
opět	opět	k6eAd1	opět
tvořit	tvořit	k5eAaImF	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
expanzích	expanze	k1gFnPc6	expanze
ještě	ještě	k9	ještě
větších	veliký	k2eAgInPc2d2	veliký
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dokonce	dokonce	k9	dokonce
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
mlhu	mlha	k1gFnSc4	mlha
podobných	podobný	k2eAgFnPc2d1	podobná
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c4	na
Ben	Ben	k1gInSc4	Ben
Nevisu	Nevis	k1gInSc2	Nevis
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výzkum	výzkum	k1gInSc1	výzkum
jej	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
nezaujal	zaujmout	k5eNaPmAgMnS	zaujmout
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vznik	vznik	k1gInSc1	vznik
mlhy	mlha	k1gFnSc2	mlha
při	při	k7c6	při
nízkých	nízký	k2eAgInPc6d1	nízký
podtlacích	podtlak	k1gInPc6	podtlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
požádal	požádat	k5eAaPmAgInS	požádat
Wilson	Wilson	k1gInSc1	Wilson
kolegu	kolega	k1gMnSc4	kolega
J.	J.	kA	J.
Thompsona	Thompson	k1gMnSc4	Thompson
o	o	k7c4	o
zapůjčení	zapůjčení	k1gNnSc4	zapůjčení
aparatury	aparatura	k1gFnSc2	aparatura
produkující	produkující	k2eAgNnSc4d1	produkující
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
výzkum	výzkum	k1gInSc1	výzkum
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
právě	právě	k6eAd1	právě
započal	započnout	k5eAaPmAgInS	započnout
<g/>
.	.	kIx.	.
</s>
<s>
Pokusem	pokus	k1gInSc7	pokus
ověřil	ověřit	k5eAaPmAgMnS	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
záření	záření	k1gNnSc1	záření
zajistí	zajistit	k5eAaPmIp3nS	zajistit
vznik	vznik	k1gInSc4	vznik
kondenzačních	kondenzační	k2eAgNnPc2d1	kondenzační
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
publikoval	publikovat	k5eAaBmAgInS	publikovat
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
Wilson	Wilson	k1gInSc4	Wilson
svou	svůj	k3xOyFgFnSc4	svůj
aparaturu	aparatura	k1gFnSc4	aparatura
zdokonaloval	zdokonalovat	k5eAaImAgMnS	zdokonalovat
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dráhy	dráha	k1gFnPc4	dráha
částic	částice	k1gFnPc2	částice
zachytit	zachytit	k5eAaPmF	zachytit
během	během	k7c2	během
krátkého	krátký	k2eAgInSc2d1	krátký
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
svou	svůj	k3xOyFgFnSc4	svůj
aparaturu	aparatura	k1gFnSc4	aparatura
vybavil	vybavit	k5eAaPmAgInS	vybavit
obloukovou	obloukový	k2eAgFnSc7d1	oblouková
lampou	lampa	k1gFnSc7	lampa
s	s	k7c7	s
mohutným	mohutný	k2eAgInSc7d1	mohutný
kondenzorem	kondenzor	k1gInSc7	kondenzor
<g/>
,	,	kIx,	,
mechanikou	mechanika	k1gFnSc7	mechanika
pro	pro	k7c4	pro
rychlý	rychlý	k2eAgInSc4d1	rychlý
pokles	pokles	k1gInSc4	pokles
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
pozorování	pozorování	k1gNnSc1	pozorování
tímto	tento	k3xDgInSc7	tento
zdokonaleným	zdokonalený	k2eAgInSc7d1	zdokonalený
typem	typ	k1gInSc7	typ
komory	komora	k1gFnSc2	komora
provedl	provést	k5eAaPmAgInS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
letech	let	k1gInPc6	let
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
objev	objev	k1gInSc1	objev
ale	ale	k9	ale
nezískal	získat	k5eNaPmAgInS	získat
širší	široký	k2eAgInSc1d2	širší
ohlas	ohlas	k1gInSc1	ohlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgInSc4d2	veliký
rozvoj	rozvoj	k1gInSc4	rozvoj
výzkumů	výzkum	k1gInPc2	výzkum
pomocí	pomocí	k7c2	pomocí
Wilsonovy	Wilsonův	k2eAgFnSc2d1	Wilsonova
komory	komora	k1gFnSc2	komora
pozvolna	pozvolna	k6eAd1	pozvolna
začal	začít	k5eAaPmAgInS	začít
až	až	k9	až
po	po	k7c4	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
zdokonalením	zdokonalení	k1gNnSc7	zdokonalení
bylo	být	k5eAaImAgNnS	být
vložení	vložení	k1gNnSc1	vložení
komory	komora	k1gFnSc2	komora
mezi	mezi	k7c4	mezi
póly	pól	k1gInPc4	pól
magnetu	magnet	k1gInSc2	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
studovat	studovat	k5eAaImF	studovat
elektrické	elektrický	k2eAgFnPc4d1	elektrická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
částic	částice	k1gFnPc2	částice
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zakřivení	zakřivení	k1gNnSc2	zakřivení
jejich	jejich	k3xOp3gFnSc2	jejich
dráhy	dráha	k1gFnSc2	dráha
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
<g/>
.	.	kIx.	.
</s>
<s>
Komora	komora	k1gFnSc1	komora
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
dále	daleko	k6eAd2	daleko
zdokonalována	zdokonalovat	k5eAaImNgFnS	zdokonalovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
vytlačena	vytlačit	k5eAaPmNgFnS	vytlačit
dokonalejší	dokonalý	k2eAgFnSc7d2	dokonalejší
bublinkovou	bublinkový	k2eAgFnSc7d1	bublinková
komorou	komora	k1gFnSc7	komora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
mlžné	mlžný	k2eAgFnPc1d1	mlžná
komory	komora	k1gFnPc1	komora
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
využívány	využívat	k5eAaPmNgInP	využívat
a	a	k8xC	a
vyvíjeny	vyvíjet	k5eAaImNgInP	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
nahrazeny	nahrazen	k2eAgInPc4d1	nahrazen
elektronickými	elektronický	k2eAgInPc7d1	elektronický
přístroji	přístroj	k1gInPc7	přístroj
<g/>
,	,	kIx,	,
názornost	názornost	k1gFnSc1	názornost
mlžné	mlžný	k2eAgFnSc2d1	mlžná
komory	komora	k1gFnSc2	komora
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nepřekonána	překonán	k2eNgFnSc1d1	nepřekonána
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
technologie	technologie	k1gFnPc1	technologie
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
zobrazování	zobrazování	k1gNnSc4	zobrazování
mlžných	mlžný	k2eAgFnPc2d1	mlžná
stop	stopa	k1gFnPc2	stopa
v	v	k7c6	v
nevídaném	vídaný	k2eNgInSc6d1	nevídaný
kontrastu	kontrast	k1gInSc6	kontrast
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
využívány	využívat	k5eAaPmNgInP	využívat
především	především	k9	především
jako	jako	k9	jako
učební	učební	k2eAgFnPc4d1	učební
pomůcky	pomůcka	k1gFnPc4	pomůcka
a	a	k8xC	a
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
popularizaci	popularizace	k1gFnSc4	popularizace
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejmodernějších	moderní	k2eAgFnPc2d3	nejmodernější
mlžných	mlžný	k2eAgFnPc2d1	mlžná
komor	komora	k1gFnPc2	komora
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
Nuledo	Nuledo	k1gNnSc1	Nuledo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgMnPc4	svůj
klienty	klient	k1gMnPc4	klient
řadí	řadit	k5eAaImIp3nS	řadit
například	například	k6eAd1	například
i	i	k8xC	i
CERN	CERN	kA	CERN
ve	v	k7c6	v
Švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
mlžná	mlžný	k2eAgFnSc1d1	mlžná
komora	komora	k1gFnSc1	komora
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
Microcosm	Microcosma	k1gFnPc2	Microcosma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wilsonova	Wilsonův	k2eAgFnSc1d1	Wilsonova
mlžná	mlžný	k2eAgFnSc1d1	mlžná
komora	komora	k1gFnSc1	komora
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
<g/>
:	:	kIx,	:
Mlžná	mlžný	k2eAgFnSc1d1	mlžná
komora	komora	k1gFnSc1	komora
Gymnázia	gymnázium	k1gNnSc2	gymnázium
Opatov	Opatovo	k1gNnPc2	Opatovo
o.s.	o.s.	k?	o.s.
</s>
</p>
<p>
<s>
Detailní	detailní	k2eAgInSc4d1	detailní
popis	popis	k1gInSc4	popis
mlžných	mlžný	k2eAgFnPc2d1	mlžná
komor	komora	k1gFnPc2	komora
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgInPc2	všecek
pozorovatelných	pozorovatelný	k2eAgInPc2d1	pozorovatelný
jevů	jev	k1gInPc2	jev
</s>
</p>
