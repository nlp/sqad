<s>
Owen	Owen	k1gMnSc1	Owen
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
,	,	kIx,	,
Kalifornia	kalifornium	k1gNnSc2	kalifornium
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
výzkumem	výzkum	k1gInSc7	výzkum
srážek	srážka	k1gFnPc2	srážka
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
návrhy	návrh	k1gInPc4	návrh
detektorů	detektor	k1gInPc2	detektor
částic	částice	k1gFnPc2	částice
přispěl	přispět	k5eAaPmAgInS	přispět
nemalou	malý	k2eNgFnSc4d1	nemalá
měrou	míra	k1gFnSc7wR	míra
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
experimentální	experimentální	k2eAgFnSc2d1	experimentální
částicové	částicový	k2eAgFnSc2d1	částicová
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
objevem	objev	k1gInSc7	objev
antiprotonu	antiproton	k1gInSc2	antiproton
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
obdržel	obdržet	k5eAaPmAgMnS	obdržet
společně	společně	k6eAd1	společně
s	s	k7c7	s
Emiliem	Emilium	k1gNnSc7	Emilium
Segrè	Segrè	k1gFnSc4	Segrè
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
fyziky	fyzika	k1gFnSc2	fyzika
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Berkeley	Berkele	k1gMnPc4	Berkele
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
emeritním	emeritní	k2eAgMnSc7d1	emeritní
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Americké	americký	k2eAgFnSc2d1	americká
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
Fellow	Fellow	k1gFnSc1	Fellow
of	of	k?	of
the	the	k?	the
American	American	k1gInSc4	American
Physical	Physical	k1gMnSc2	Physical
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
a	a	k8xC	a
americké	americký	k2eAgFnSc2d1	americká
Národní	národní	k2eAgFnSc2d1	národní
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
National	National	k1gFnSc1	National
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Sciences	Sciences	k1gInSc1	Sciences
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
získal	získat	k5eAaPmAgInS	získat
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Owen	Owen	k1gMnSc1	Owen
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1920	[number]	k4	1920
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
W.	W.	kA	W.
Edward	Edward	k1gMnSc1	Edward
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
byl	být	k5eAaImAgMnS	být
radiolog	radiolog	k1gMnSc1	radiolog
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
za	za	k7c4	za
svobodna	svoboden	k2eAgMnSc4d1	svoboden
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Genevieve	Genevieev	k1gFnSc2	Genevieev
Lucinda	Lucind	k1gMnSc4	Lucind
Owen	Owen	k1gMnSc1	Owen
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
fyziku	fyzika	k1gFnSc4	fyzika
na	na	k7c4	na
Dartmouth	Dartmouth	k1gInSc4	Dartmouth
College	Colleg	k1gFnSc2	Colleg
v	v	k7c4	v
Hanoveru	Hanovera	k1gFnSc4	Hanovera
(	(	kIx(	(
<g/>
New	New	k1gMnSc5	New
Hampshire	Hampshir	k1gMnSc5	Hampshir
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgInS	získat
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
(	(	kIx(	(
<g/>
A.B.	A.B.	k1gFnSc1	A.B.
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
Kalifornskou	kalifornský	k2eAgFnSc4d1	kalifornská
univerzitu	univerzita	k1gFnSc4	univerzita
do	do	k7c2	do
Berkeley	Berkelea	k1gFnSc2	Berkelea
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	on	k3xPp3gNnSc2	on
studia	studio	k1gNnSc2	studio
byla	být	k5eAaImAgFnS	být
přerušena	přerušit	k5eAaPmNgFnS	přerušit
zapojením	zapojení	k1gNnSc7	zapojení
se	se	k3xPyFc4	se
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
se	se	k3xPyFc4	se
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
tajného	tajný	k2eAgInSc2d1	tajný
projektu	projekt	k1gInSc2	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
v	v	k7c4	v
Los	los	k1gInSc4	los
Alamos	Alamos	k1gMnSc1	Alamos
(	(	kIx(	(
<g/>
New	New	k1gMnSc1	New
Mexico	Mexico	k1gMnSc1	Mexico
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vyrobit	vyrobit	k5eAaPmF	vyrobit
atomovou	atomový	k2eAgFnSc4d1	atomová
bombu	bomba	k1gFnSc4	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Chamberlainovým	Chamberlainův	k2eAgInSc7d1	Chamberlainův
úkolem	úkol	k1gInSc7	úkol
byl	být	k5eAaImAgInS	být
výzkum	výzkum	k1gInSc1	výzkum
jaderných	jaderný	k2eAgFnPc2d1	jaderná
srážek	srážka	k1gFnPc2	srážka
se	s	k7c7	s
středně	středně	k6eAd1	středně
energetickými	energetický	k2eAgInPc7d1	energetický
neutrony	neutron	k1gInPc7	neutron
a	a	k8xC	a
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
štěpení	štěpení	k1gNnSc2	štěpení
těžkých	těžký	k2eAgInPc2d1	těžký
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
projektu	projekt	k1gInSc6	projekt
pracoval	pracovat	k5eAaImAgMnS	pracovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Emiliom	Emiliom	k1gInSc1	Emiliom
Segrè	Segrè	k1gFnPc1	Segrè
jak	jak	k8xC	jak
v	v	k7c4	v
Los	los	k1gInSc4	los
Alamos	Alamosa	k1gFnPc2	Alamosa
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c4	v
Berkeley	Berkele	k1gMnPc4	Berkele
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
postgraduálním	postgraduální	k2eAgNnSc6d1	postgraduální
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
Chicagské	chicagský	k2eAgFnSc6d1	Chicagská
univerzitě	univerzita	k1gFnSc6	univerzita
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
slavného	slavný	k2eAgMnSc2d1	slavný
fyzika	fyzik	k1gMnSc2	fyzik
Enrica	Enricus	k1gMnSc2	Enricus
Fermiho	Fermi	k1gMnSc2	Fermi
<g/>
.	.	kIx.	.
</s>
<s>
Fermi	Fer	k1gFnPc7	Fer
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnSc4d1	budoucí
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
dráhu	dráha	k1gFnSc4	dráha
Chamberlaina	Chamberlaino	k1gNnSc2	Chamberlaino
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ho	on	k3xPp3gMnSc4	on
motivoval	motivovat	k5eAaBmAgMnS	motivovat
a	a	k8xC	a
podpořil	podpořit	k5eAaPmAgMnS	podpořit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
namísto	namísto	k7c2	namísto
prestižní	prestižní	k2eAgFnSc2d1	prestižní
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
experimentální	experimentální	k2eAgFnSc7d1	experimentální
fyzikou	fyzika	k1gFnSc7	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
doktorské	doktorský	k2eAgFnSc6d1	doktorská
práci	práce	k1gFnSc6	práce
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
difrakcí	difrakce	k1gFnSc7	difrakce
pomalých	pomalý	k2eAgInPc2d1	pomalý
neutronů	neutron	k1gInPc2	neutron
a	a	k8xC	a
titul	titul	k1gInSc4	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
obdržel	obdržet	k5eAaPmAgMnS	obdržet
oficiálně	oficiálně	k6eAd1	oficiálně
od	od	k7c2	od
Chicagské	chicagský	k2eAgFnSc2d1	Chicagská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
přijal	přijmout	k5eAaPmAgMnS	přijmout
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
učitelské	učitelský	k2eAgNnSc4d1	učitelské
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
Kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Berkeley	Berkeley	k1gInPc4	Berkeley
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
Emiliem	Emilium	k1gNnSc7	Emilium
Segrè	Segrè	k1gMnSc7	Segrè
<g/>
,	,	kIx,	,
Clydem	Clyd	k1gMnSc7	Clyd
Wiegandem	Wiegando	k1gNnSc7	Wiegando
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
se	se	k3xPyFc4	se
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
zabýval	zabývat	k5eAaImAgInS	zabývat
protonovými	protonový	k2eAgFnPc7d1	protonová
srážkami	srážka	k1gFnPc7	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
jejich	jejich	k3xOp3gInPc2	jejich
experimentů	experiment	k1gInPc2	experiment
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
k	k	k7c3	k
objevu	objev	k1gInSc3	objev
antiprotonu	antiproton	k1gInSc2	antiproton
(	(	kIx(	(
<g/>
záporně	záporně	k6eAd1	záporně
nabitého	nabitý	k2eAgInSc2d1	nabitý
protonu	proton	k1gInSc2	proton
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
pozitronu	pozitron	k1gInSc6	pozitron
druhou	druhý	k4xOgFnSc7	druhý
nalezenou	nalezený	k2eAgFnSc7d1	nalezená
antičásticí	antičástice	k1gFnSc7	antičástice
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
existenci	existence	k1gFnSc4	existence
teoreticky	teoreticky	k6eAd1	teoreticky
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
Paul	Paul	k1gMnSc1	Paul
Dirac	Dirac	k1gInSc4	Dirac
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
kolegy	kolega	k1gMnPc7	kolega
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
interakcí	interakce	k1gFnPc2	interakce
antiprotonů	antiproton	k1gInPc2	antiproton
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
částicemi	částice	k1gFnPc7	částice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
výzkumu	výzkum	k1gInSc2	výzkum
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
další	další	k2eAgFnSc1d1	další
antičástice	antičástice	k1gFnSc1	antičástice
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
antineutron	antineutron	k1gInSc1	antineutron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
společně	společně	k6eAd1	společně
s	s	k7c7	s
Carsonem	Carson	k1gMnSc7	Carson
Jeffriesem	Jeffries	k1gMnSc7	Jeffries
a	a	k8xC	a
Gilbertem	Gilbert	k1gMnSc7	Gilbert
Shapirem	Shapir	k1gMnSc7	Shapir
průkopníkem	průkopník	k1gMnSc7	průkopník
použití	použití	k1gNnSc2	použití
polarizovaných	polarizovaný	k2eAgInPc2d1	polarizovaný
protonů	proton	k1gInPc2	proton
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
spinové	spinový	k2eAgFnSc2d1	spinová
závislosti	závislost	k1gFnSc2	závislost
vysokoenergetických	vysokoenergetický	k2eAgInPc2d1	vysokoenergetický
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
parity	parita	k1gFnSc2	parita
hyperonů	hyperon	k1gInPc2	hyperon
či	či	k8xC	či
k	k	k7c3	k
testům	test	k1gInPc3	test
časové	časový	k2eAgFnSc2d1	časová
symetrie	symetrie	k1gFnSc2	symetrie
v	v	k7c6	v
elektron-protonových	elektronrotonový	k2eAgFnPc6d1	elektron-protonový
srážkách	srážka	k1gFnPc6	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
výzkumu	výzkum	k1gInSc3	výzkum
interakcí	interakce	k1gFnPc2	interakce
urychlených	urychlený	k2eAgNnPc2d1	urychlené
lehkých	lehký	k2eAgNnPc2d1	lehké
jader	jádro	k1gNnPc2	jádro
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
atomovými	atomový	k2eAgFnPc7d1	atomová
jádry	jádro	k1gNnPc7	jádro
na	na	k7c6	na
urychlovači	urychlovač	k1gInSc6	urychlovač
Bevalac	Bevalac	k1gInSc1	Bevalac
v	v	k7c6	v
Lawrence	Lawrenka	k1gFnSc6	Lawrenka
Berkeley	Berkelea	k1gFnSc2	Berkelea
National	National	k1gFnSc2	National
Laboratory	Laborator	k1gInPc1	Laborator
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
své	svůj	k3xOyFgFnSc2	svůj
vědecké	vědecký	k2eAgFnSc2d1	vědecká
kariéry	kariéra	k1gFnSc2	kariéra
pracoval	pracovat	k5eAaImAgMnS	pracovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Nygrenem	Nygren	k1gMnSc7	Nygren
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
tzv.	tzv.	kA	tzv.
časově	časově	k6eAd1	časově
projekční	projekční	k2eAgFnSc1d1	projekční
komory	komora	k1gFnSc2	komora
(	(	kIx(	(
<g/>
Time	Tim	k1gInSc2	Tim
projection	projection	k1gInSc1	projection
chamber	chamber	k1gInSc1	chamber
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
detektoru	detektor	k1gInSc2	detektor
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
úspěšně	úspěšně	k6eAd1	úspěšně
použit	použít	k5eAaPmNgInS	použít
k	k	k7c3	k
výzkumu	výzkum	k1gInSc3	výzkum
elektron-pozitronových	elektronozitronův	k2eAgFnPc2d1	elektron-pozitronův
srážek	srážka	k1gFnPc2	srážka
na	na	k7c6	na
lineárním	lineární	k2eAgInSc6d1	lineární
urychlovači	urychlovač	k1gInSc6	urychlovač
ve	v	k7c6	v
Stanfordu	Stanford	k1gInSc6	Stanford
(	(	kIx(	(
<g/>
SLAC	SLAC	kA	SLAC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
se	se	k3xPyFc4	se
též	též	k9	též
aktivně	aktivně	k6eAd1	aktivně
účastnil	účastnit	k5eAaImAgMnS	účastnit
řady	řada	k1gFnPc4	řada
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
či	či	k8xC	či
svobodu	svoboda	k1gFnSc4	svoboda
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
např.	např.	kA	např.
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
významným	významný	k2eAgInSc7d1	významný
členem	člen	k1gInSc7	člen
hnutí	hnutí	k1gNnSc2	hnutí
Scientists	Scientists	k1gInSc1	Scientists
for	forum	k1gNnPc2	forum
Sakharov	Sakharov	k1gInSc1	Sakharov
<g/>
,	,	kIx,	,
Orlov	Orlov	k1gInSc1	Orlov
and	and	k?	and
Shcharansky	Shcharansky	k1gFnSc2	Shcharansky
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
ruských	ruský	k2eAgMnPc2d1	ruský
vědců	vědec	k1gMnPc2	vědec
vězněných	vězněný	k2eAgMnPc2d1	vězněný
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
politická	politický	k2eAgNnPc4d1	politické
přesvědčení	přesvědčení	k1gNnPc4	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
otevřeně	otevřeně	k6eAd1	otevřeně
postavil	postavit	k5eAaPmAgMnS	postavit
za	za	k7c4	za
ukončení	ukončení	k1gNnSc4	ukončení
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
výroby	výroba	k1gFnSc2	výroba
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
se	se	k3xPyFc4	se
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Beatrice	Beatrice	k1gFnPc1	Beatrice
Babette	Babett	k1gInSc5	Babett
Copper	Coppra	k1gFnPc2	Coppra
(	(	kIx(	(
<g/>
†	†	k?	†
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
ještě	ještě	k9	ještě
dvakrát	dvakrát	k6eAd1	dvakrát
-	-	kIx~	-
s	s	k7c7	s
June	jun	k1gMnSc5	jun
Steingart	Steingart	k1gInSc4	Steingart
Greenfield	Greenfield	k1gInSc4	Greenfield
(	(	kIx(	(
<g/>
†	†	k?	†
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sentou	Senta	k1gFnSc7	Senta
Pugh	Pugh	k1gMnSc1	Pugh
Gaiser	Gaiser	k1gMnSc1	Gaiser
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
něho	on	k3xPp3gNnSc2	on
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
Parkinsonova	Parkinsonův	k2eAgFnSc1d1	Parkinsonova
nemoc	nemoc	k1gFnSc1	nemoc
a	a	k8xC	a
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
ukončil	ukončit	k5eAaPmAgInS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
kariéru	kariéra	k1gFnSc4	kariéra
učitele	učitel	k1gMnSc2	učitel
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Berkeley	Berkele	k1gMnPc4	Berkele
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
následky	následek	k1gInPc4	následek
komplikací	komplikace	k1gFnPc2	komplikace
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
nemocí	nemoc	k1gFnSc7	nemoc
zemřel	zemřít	k5eAaPmAgMnS	zemřít
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
v	v	k7c4	v
Berkeley	Berkeley	k1gInPc4	Berkeley
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
85	[number]	k4	85
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
