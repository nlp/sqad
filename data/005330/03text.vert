<s>
Rovnátka	rovnátko	k1gNnPc1	rovnátko
(	(	kIx(	(
<g/>
též	též	k9	též
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
ortodontický	ortodontický	k2eAgInSc1d1	ortodontický
aparát	aparát	k1gInSc1	aparát
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
druh	druh	k1gInSc4	druh
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
pomůcky	pomůcka	k1gFnSc2	pomůcka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
narovnání	narovnání	k1gNnSc3	narovnání
<g/>
,	,	kIx,	,
napravení	napravení	k1gNnSc3	napravení
<g/>
,	,	kIx,	,
či	či	k8xC	či
usměrnění	usměrnění	k1gNnSc1	usměrnění
růstu	růst	k1gInSc2	růst
zubů	zub	k1gInPc2	zub
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
nandávat	nandávat	k5eAaImF	nandávat
jak	jak	k6eAd1	jak
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
dolní	dolní	k2eAgFnSc4d1	dolní
čelist	čelist	k1gFnSc4	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Fixní	fixní	k2eAgNnPc1d1	fixní
rovnátka	rovnátko	k1gNnPc1	rovnátko
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ústní	ústní	k2eAgFnSc6d1	ústní
dutině	dutina	k1gFnSc6	dutina
nepřetržitě	přetržitě	k6eNd1	přetržitě
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
léčby	léčba	k1gFnSc2	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nalepena	nalepen	k2eAgFnSc1d1	nalepena
buď	buď	k8xC	buď
z	z	k7c2	z
tvářové	tvářový	k2eAgFnSc2d1	tvářová
či	či	k8xC	či
jazykové	jazykový	k2eAgFnSc2d1	jazyková
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
lingvální	lingvální	k2eAgInPc1d1	lingvální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Častějším	častý	k2eAgInSc7d2	častější
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
aplikace	aplikace	k1gFnSc1	aplikace
z	z	k7c2	z
tvářové	tvářový	k2eAgFnSc2d1	tvářová
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgFnSc3	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
linguální	linguální	k2eAgNnPc1d1	linguální
rovnátka	rovnátko	k1gNnPc1	rovnátko
jsou	být	k5eAaImIp3nP	být
dražší	drahý	k2eAgInPc1d2	dražší
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
zavedení	zavedení	k1gNnSc1	zavedení
je	být	k5eAaImIp3nS	být
náročnější	náročný	k2eAgNnSc1d2	náročnější
a	a	k8xC	a
klade	klást	k5eAaImIp3nS	klást
větší	veliký	k2eAgInPc4d2	veliký
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
lékaře	lékař	k1gMnSc4	lékař
i	i	k8xC	i
pacienta	pacient	k1gMnSc4	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Snímací	snímací	k2eAgNnPc1d1	snímací
rovnátka	rovnátko	k1gNnPc1	rovnátko
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
vyjmout	vyjmout	k5eAaPmF	vyjmout
z	z	k7c2	z
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
méně	málo	k6eAd2	málo
závažné	závažný	k2eAgFnPc4d1	závažná
zubní	zubní	k2eAgFnPc4d1	zubní
anomálie	anomálie	k1gFnPc4	anomálie
a	a	k8xC	a
vady	vada	k1gFnPc4	vada
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
dočasný	dočasný	k2eAgInSc4d1	dočasný
a	a	k8xC	a
smíšený	smíšený	k2eAgInSc4d1	smíšený
chrup	chrup	k1gInSc4	chrup
<g/>
.	.	kIx.	.
</s>
<s>
Fóliová	fóliový	k2eAgNnPc1d1	fóliové
rovnátka	rovnátko	k1gNnPc1	rovnátko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
neviditelná	viditelný	k2eNgNnPc1d1	neviditelné
rovnátka	rovnátko	k1gNnPc1	rovnátko
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
měkké	měkký	k2eAgFnPc4d1	měkká
plastové	plastový	k2eAgFnPc4d1	plastová
fólie	fólie	k1gFnPc4	fólie
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
pacientu	pacient	k1gMnSc3	pacient
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
podle	podle	k7c2	podle
otisku	otisk	k1gInSc2	otisk
čelistí	čelist	k1gFnPc2	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nosiče	nosič	k1gInPc1	nosič
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
léčby	léčba	k1gFnSc2	léčba
obměňují	obměňovat	k5eAaImIp3nP	obměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
o	o	k7c4	o
speciální	speciální	k2eAgInSc4d1	speciální
druh	druh	k1gInSc4	druh
snímacích	snímací	k2eAgNnPc2d1	snímací
rovnátek	rovnátko	k1gNnPc2	rovnátko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
kdykoliv	kdykoliv	k6eAd1	kdykoliv
vyjmout	vyjmout	k5eAaPmF	vyjmout
<g/>
.	.	kIx.	.
</s>
<s>
Neviditelná	viditelný	k2eNgNnPc1d1	neviditelné
rovnátka	rovnátko	k1gNnPc1	rovnátko
jsou	být	k5eAaImIp3nP	být
americkým	americký	k2eAgInSc7d1	americký
patentem	patent	k1gInSc7	patent
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Invisalign	Invisaligna	k1gFnPc2	Invisaligna
<g/>
.	.	kIx.	.
</s>
<s>
Fixní	fixní	k2eAgInSc1d1	fixní
aparát	aparát	k1gInSc1	aparát
klade	klást	k5eAaImIp3nS	klást
větší	veliký	k2eAgInPc4d2	veliký
požadavky	požadavek	k1gInPc4	požadavek
na	na	k7c4	na
ústní	ústní	k2eAgFnSc4d1	ústní
hygienu	hygiena	k1gFnSc4	hygiena
pacienta	pacient	k1gMnSc2	pacient
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
bylo	být	k5eAaImAgNnS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvorba	tvorba	k1gFnSc1	tvorba
plaku	plak	k1gInSc2	plak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
nošení	nošení	k1gNnSc2	nošení
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
rovnátek	rovnátko	k1gNnPc2	rovnátko
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Pacientům	pacient	k1gMnPc3	pacient
s	s	k7c7	s
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
ústní	ústní	k2eAgFnSc7d1	ústní
hygienou	hygiena	k1gFnSc7	hygiena
se	se	k3xPyFc4	se
fixní	fixní	k2eAgInSc1d1	fixní
aparát	aparát	k1gInSc1	aparát
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
či	či	k8xC	či
mu	on	k3xPp3gMnSc3	on
přímo	přímo	k6eAd1	přímo
není	být	k5eNaImIp3nS	být
umožněn	umožnit	k5eAaPmNgInS	umožnit
<g/>
.	.	kIx.	.
</s>
<s>
Fixním	fixní	k2eAgInSc7d1	fixní
aparátem	aparát	k1gInSc7	aparát
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
lepších	dobrý	k2eAgInPc2d2	lepší
výsledků	výsledek	k1gInPc2	výsledek
než	než	k8xS	než
snímacím	snímací	k2eAgMnSc7d1	snímací
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
spíše	spíše	k9	spíše
u	u	k7c2	u
závažnějších	závažný	k2eAgFnPc2d2	závažnější
zubních	zubní	k2eAgFnPc2d1	zubní
anomálií	anomálie	k1gFnPc2	anomálie
<g/>
.	.	kIx.	.
</s>
<s>
Snímací	snímací	k2eAgInSc1d1	snímací
aparát	aparát	k1gInSc1	aparát
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
náročnější	náročný	k2eAgMnSc1d2	náročnější
na	na	k7c4	na
hygienu	hygiena	k1gFnSc4	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
kdykoliv	kdykoliv	k6eAd1	kdykoliv
sejmout	sejmout	k5eAaPmF	sejmout
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
nevýhoda	nevýhoda	k1gFnSc1	nevýhoda
-	-	kIx~	-
pacient	pacient	k1gMnSc1	pacient
není	být	k5eNaImIp3nS	být
nucen	nutit	k5eAaImNgMnS	nutit
nosit	nosit	k5eAaImF	nosit
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgInSc1d1	vstupní
pohovor	pohovor	k1gInSc1	pohovor
<g/>
,	,	kIx,	,
vyšetření	vyšetření	k1gNnSc1	vyšetření
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
zadokumentování	zadokumentování	k1gNnSc4	zadokumentování
<g/>
.	.	kIx.	.
</s>
<s>
Ortodontista	Ortodontista	k1gMnSc1	Ortodontista
pacienta	pacient	k1gMnSc2	pacient
seznámí	seznámit	k5eAaPmIp3nS	seznámit
o	o	k7c6	o
výsledcích	výsledek	k1gInPc6	výsledek
vyšetření	vyšetření	k1gNnPc2	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
léčebného	léčebný	k2eAgInSc2d1	léčebný
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
schválení	schválení	k1gNnSc4	schválení
pacientem	pacient	k1gMnSc7	pacient
<g/>
,	,	kIx,	,
zubní	zubní	k2eAgFnSc1d1	zubní
otisky	otisk	k1gInPc4	otisk
<g/>
,	,	kIx,	,
rentgenové	rentgenový	k2eAgInPc4d1	rentgenový
snímky	snímek	k1gInPc4	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Aktivní	aktivní	k2eAgFnSc1d1	aktivní
léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
samotný	samotný	k2eAgInSc1d1	samotný
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
vést	vést	k5eAaImF	vést
k	k	k7c3	k
nápravě	náprava	k1gFnSc3	náprava
chrupu	chrup	k1gInSc2	chrup
a	a	k8xC	a
estetiky	estetika	k1gFnSc2	estetika
obličeje	obličej	k1gInSc2	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Retenční	retenční	k2eAgFnSc1d1	retenční
fáze	fáze	k1gFnSc1	fáze
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
aktivní	aktivní	k2eAgFnSc6d1	aktivní
léčbě	léčba	k1gFnSc6	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
udržet	udržet	k5eAaPmF	udržet
výsledky	výsledek	k1gInPc4	výsledek
ortodontické	ortodontický	k2eAgFnSc2d1	ortodontická
léčby	léčba	k1gFnSc2	léčba
co	co	k3yInSc4	co
nejdéle	dlouho	k6eAd3	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zanedbána	zanedbat	k5eAaPmNgFnS	zanedbat
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
částečný	částečný	k2eAgInSc4d1	částečný
či	či	k8xC	či
celkový	celkový	k2eAgInSc4d1	celkový
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
stavu	stav	k1gInSc3	stav
chrupu	chrup	k1gInSc2	chrup
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
typem	typ	k1gInSc7	typ
rovnátek	rovnátko	k1gNnPc2	rovnátko
je	být	k5eAaImIp3nS	být
fixní	fixní	k2eAgInSc1d1	fixní
aparát	aparát	k1gInSc1	aparát
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
právě	právě	k9	právě
jeho	jeho	k3xOp3gFnSc1	jeho
skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
rozebrána	rozebrán	k2eAgMnSc4d1	rozebrán
<g/>
:	:	kIx,	:
Ortodontický	ortodontický	k2eAgInSc4d1	ortodontický
drát	drát	k1gInSc4	drát
(	(	kIx(	(
<g/>
označovaný	označovaný	k2eAgMnSc1d1	označovaný
též	též	k9	též
jako	jako	k9	jako
oblouk	oblouk	k1gInSc1	oblouk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
speciální	speciální	k2eAgInSc1d1	speciální
typ	typ	k1gInSc1	typ
drátu	drát	k1gInSc2	drát
užívaný	užívaný	k2eAgInSc1d1	užívaný
v	v	k7c6	v
ortodoncii	ortodoncie	k1gFnSc6	ortodoncie
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
posunování	posunování	k1gNnSc3	posunování
zubu	zub	k1gInSc2	zub
<g/>
/	/	kIx~	/
<g/>
ů.	ů.	k?	ů.
Drát	drát	k1gInSc1	drát
je	být	k5eAaImIp3nS	být
fixován	fixovat	k5eAaImNgInS	fixovat
do	do	k7c2	do
zámečků	zámeček	k1gInPc2	zámeček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
požádované	požádovaný	k2eAgFnSc2d1	požádovaný
změny	změna	k1gFnSc2	změna
pozice	pozice	k1gFnSc2	pozice
zubů	zub	k1gInPc2	zub
pak	pak	k6eAd1	pak
mírně	mírně	k6eAd1	mírně
ohnutý	ohnutý	k2eAgInSc1d1	ohnutý
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
vlastnostem	vlastnost	k1gFnPc3	vlastnost
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
tvarové	tvarový	k2eAgFnSc2d1	tvarová
paměti	paměť	k1gFnSc2	paměť
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
ohybu	ohyb	k1gInSc2	ohyb
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
rovnat	rovnat	k5eAaImF	rovnat
(	(	kIx(	(
<g/>
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
původní	původní	k2eAgFnSc2d1	původní
polohy	poloha	k1gFnSc2	poloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tlačí	tlačit	k5eAaImIp3nP	tlačit
na	na	k7c4	na
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Stomatolog	stomatolog	k1gMnSc1	stomatolog
několikrát	několikrát	k6eAd1	několikrát
během	během	k7c2	během
aktivní	aktivní	k2eAgFnSc2d1	aktivní
fáze	fáze	k1gFnSc2	fáze
léčby	léčba	k1gFnSc2	léčba
drát	drát	k1gInSc1	drát
vyměňuje	vyměňovat	k5eAaImIp3nS	vyměňovat
za	za	k7c4	za
jiný	jiný	k2eAgInSc4d1	jiný
s	s	k7c7	s
odlišnými	odlišný	k2eAgInPc7d1	odlišný
parametry	parametr	k1gInPc7	parametr
(	(	kIx(	(
<g/>
jiná	jiný	k2eAgFnSc1d1	jiná
tloušťka	tloušťka	k1gFnSc1	tloušťka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Separační	separační	k2eAgFnPc1d1	separační
gumičky	gumička	k1gFnPc1	gumička
jsou	být	k5eAaImIp3nP	být
gumová	gumový	k2eAgNnPc4d1	gumové
kolečka	kolečko	k1gNnPc4	kolečko
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zuby	zub	k1gInPc7	zub
separovat	separovat	k5eAaBmF	separovat
-	-	kIx~	-
vytvořit	vytvořit	k5eAaPmF	vytvořit
mezizubní	mezizubní	k2eAgInSc4d1	mezizubní
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
usazení	usazení	k1gNnSc4	usazení
kroužku	kroužek	k1gInSc2	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Instalují	instalovat	k5eAaBmIp3nP	instalovat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
1	[number]	k4	1
a	a	k8xC	a
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
před	před	k7c7	před
vlepením	vlepení	k1gNnSc7	vlepení
fixního	fixní	k2eAgInSc2d1	fixní
aparátu	aparát	k1gInSc2	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Kovové	kovový	k2eAgInPc1d1	kovový
kroužky	kroužek	k1gInPc1	kroužek
obepínají	obepínat	k5eAaImIp3nP	obepínat
celý	celý	k2eAgInSc4d1	celý
zub	zub	k1gInSc4	zub
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
jsou	být	k5eAaImIp3nP	být
pevně	pevně	k6eAd1	pevně
spojeny	spojit	k5eAaPmNgInP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
krajních	krajní	k2eAgInPc6d1	krajní
zubech	zub	k1gInPc6	zub
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
molár	molár	k1gInSc1	molár
<g/>
)	)	kIx)	)
a	a	k8xC	a
uzavírají	uzavírat	k5eAaPmIp3nP	uzavírat
tak	tak	k6eAd1	tak
celý	celý	k2eAgInSc4d1	celý
aparát	aparát	k1gInSc4	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
jejich	jejich	k3xOp3gNnSc2	jejich
užití	užití	k1gNnSc2	užití
místo	místo	k7c2	místo
zámečku	zámeček	k1gInSc2	zámeček
je	být	k5eAaImIp3nS	být
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
kvalitní	kvalitní	k2eAgFnSc4d1	kvalitní
fixaci	fixace	k1gFnSc4	fixace
-	-	kIx~	-
nehrozí	hrozit	k5eNaImIp3nP	hrozit
odlepení	odlepení	k1gNnSc4	odlepení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kroužku	kroužek	k1gInSc6	kroužek
je	být	k5eAaImIp3nS	být
připevněna	připevněn	k2eAgFnSc1d1	připevněna
kanyla	kanyla	k1gFnSc1	kanyla
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
zasunut	zasunut	k2eAgInSc1d1	zasunut
konec	konec	k1gInSc1	konec
drátu	drát	k1gInSc2	drát
(	(	kIx(	(
<g/>
oblouku	oblouk	k1gInSc2	oblouk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
"	"	kIx"	"
<g/>
vodící	vodící	k2eAgFnSc4d1	vodící
lištu	lišta	k1gFnSc4	lišta
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
drát	drát	k1gInSc4	drát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
nich	on	k3xPp3gFnPc2	on
připevňuje	připevňovat	k5eAaImIp3nS	připevňovat
<g/>
.	.	kIx.	.
</s>
<s>
Zámečky	zámeček	k1gInPc1	zámeček
jsou	být	k5eAaImIp3nP	být
napevno	napevno	k6eAd1	napevno
přilepeny	přilepit	k5eAaPmNgInP	přilepit
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
zubům	zub	k1gInPc3	zub
<g/>
.	.	kIx.	.
</s>
<s>
Zámečky	zámeček	k1gInPc1	zámeček
jsou	být	k5eAaImIp3nP	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
nejčastěji	často	k6eAd3	často
prozradí	prozradit	k5eAaPmIp3nS	prozradit
přítomnost	přítomnost	k1gFnSc4	přítomnost
rovnátek	rovnátko	k1gNnPc2	rovnátko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
okem	oke	k1gNnSc7	oke
viditelných	viditelný	k2eAgNnPc6d1	viditelné
kovových	kovový	k2eAgNnPc6d1	kovové
lze	lze	k6eAd1	lze
nalepit	nalepit	k5eAaPmF	nalepit
i	i	k9	i
částečně	částečně	k6eAd1	částečně
průhledné	průhledný	k2eAgInPc1d1	průhledný
-	-	kIx~	-
např.	např.	kA	např.
keramické	keramický	k2eAgFnPc1d1	keramická
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
barvou	barva	k1gFnSc7	barva
připomínají	připomínat	k5eAaImIp3nP	připomínat
zub	zub	k1gInSc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
obemknuty	obemknout	k5eAaPmNgInP	obemknout
gumičkou	gumička	k1gFnSc7	gumička
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
drží	držet	k5eAaImIp3nS	držet
drát	drát	k5eAaImF	drát
vně	vně	k7c2	vně
drážky	drážka	k1gFnSc2	drážka
v	v	k7c6	v
zámečku	zámeček	k1gInSc6	zámeček
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnSc2	barva
gumiček	gumička	k1gFnPc2	gumička
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
pacient	pacient	k1gMnSc1	pacient
obvykle	obvykle	k6eAd1	obvykle
vybrat	vybrat	k5eAaPmF	vybrat
z	z	k7c2	z
několika	několik	k4yIc2	několik
variant	varianta	k1gFnPc2	varianta
včetně	včetně	k7c2	včetně
průhledných	průhledný	k2eAgInPc2d1	průhledný
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
součást	součást	k1gFnSc1	součást
může	moct	k5eAaImIp3nS	moct
doplnit	doplnit	k5eAaPmF	doplnit
aparátek	aparátek	k1gInSc4	aparátek
v	v	k7c6	v
případě	případ	k1gInSc6	případ
hlubokého	hluboký	k2eAgInSc2d1	hluboký
skusu	skus	k1gInSc2	skus
pacienta	pacient	k1gMnSc2	pacient
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
častý	častý	k2eAgInSc1d1	častý
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
skusu	skus	k1gInSc6	skus
k	k	k7c3	k
překrytí	překrytí	k1gNnSc3	překrytí
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
ploch	plocha	k1gFnPc2	plocha
dolních	dolní	k2eAgInPc2d1	dolní
řezáků	řezák	k1gInPc2	řezák
<g/>
.	.	kIx.	.
</s>
<s>
Nákusná	Nákusný	k2eAgFnSc1d1	Nákusná
destička	destička	k1gFnSc1	destička
je	být	k5eAaImIp3nS	být
součást	součást	k1gFnSc4	součást
z	z	k7c2	z
průhledné	průhledný	k2eAgFnSc2d1	průhledná
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
umístěné	umístěný	k2eAgFnPc4d1	umístěná
za	za	k7c7	za
předními	přední	k2eAgInPc7d1	přední
zuby	zub	k1gInPc7	zub
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
připíná	připínat	k5eAaImIp3nS	připínat
ke	k	k7c3	k
kroužkům	kroužek	k1gInPc3	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
skus	skus	k1gInSc1	skus
pacienta	pacient	k1gMnSc2	pacient
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nasazení	nasazení	k1gNnSc1	nasazení
jak	jak	k6eAd1	jak
horního	horní	k2eAgInSc2d1	horní
tak	tak	k8xC	tak
spodního	spodní	k2eAgInSc2d1	spodní
aparátku	aparátek	k1gInSc2	aparátek
zároveň	zároveň	k6eAd1	zároveň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
léčbu	léčba	k1gFnSc4	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Kamínek	kamínek	k1gInSc1	kamínek
<g/>
:	:	kIx,	:
Současné	současný	k2eAgInPc1d1	současný
fixní	fixní	k2eAgInPc1d1	fixní
ortodontické	ortodontický	k2eAgInPc1d1	ortodontický
aparáty	aparát	k1gInPc1	aparát
<g/>
,	,	kIx,	,
Avicenum	Avicenum	k1gNnSc1	Avicenum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1976	[number]	k4	1976
Zubní	zubní	k2eAgInSc1d1	zubní
implantát	implantát	k1gInSc1	implantát
Mezizubní	mezizubní	k2eAgInSc1d1	mezizubní
kartáček	kartáček	k1gInSc1	kartáček
Zubní	zubní	k2eAgFnSc1d1	zubní
pasta	pasta	k1gFnSc1	pasta
Zubní	zubní	k2eAgFnSc1d1	zubní
kartáček	kartáček	k1gInSc4	kartáček
Zubní	zubní	k2eAgFnSc1d1	zubní
nit	nit	k1gFnSc1	nit
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rovnátka	rovnátko	k1gNnSc2	rovnátko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
