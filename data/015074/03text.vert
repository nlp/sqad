<s>
Český	český	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
korpus	korpus	k1gInSc1
</s>
<s>
Český	český	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
korpus	korpus	k1gInSc1
(	(	kIx(
<g/>
ČNK	ČNK	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
soubor	soubor	k1gInSc1
jazykových	jazykový	k2eAgInPc2d1
korpusů	korpus	k1gInPc2
<g/>
,	,	kIx,
různě	různě	k6eAd1
vybraných	vybraný	k2eAgFnPc2d1
a	a	k8xC
uspořádaných	uspořádaný	k2eAgFnPc2d1
sbírek	sbírka	k1gFnPc2
elektronicky	elektronicky	k6eAd1
zaznamenaných	zaznamenaný	k2eAgInPc2d1
textů	text	k1gInPc2
pro	pro	k7c4
češtinu	čeština	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slouží	sloužit	k5eAaImIp3nS
jako	jako	k9
datová	datový	k2eAgFnSc1d1
základna	základna	k1gFnSc1
pro	pro	k7c4
vědecké	vědecký	k2eAgNnSc4d1
studium	studium	k1gNnSc4
psané	psaný	k2eAgFnSc2d1
i	i	k8xC
mluvené	mluvený	k2eAgFnSc2d1
češtiny	čeština	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
jazykových	jazykový	k2eAgInPc2d1
slovníků	slovník	k1gInPc2
<g/>
,	,	kIx,
počítačových	počítačový	k2eAgInPc2d1
překladačů	překladač	k1gInPc2
a	a	k8xC
korektorů	korektor	k1gInPc2
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČNK	ČNK	kA
buduje	budovat	k5eAaImIp3nS
a	a	k8xC
spravuje	spravovat	k5eAaImIp3nS
Ústav	ústav	k1gInSc1
Českého	český	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
korpusu	korpus	k1gInSc2
při	při	k7c6
Filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
UK	UK	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Ředitelem	ředitel	k1gMnSc7
ústavu	ústav	k1gInSc2
je	být	k5eAaImIp3nS
Mgr.	Mgr.	kA
Michal	Michal	k1gMnSc1
Křen	křen	k1gInSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
</s>
<s>
Software	software	k1gInSc1
</s>
<s>
Jako	jako	k8xS,k8xC
korpusový	korpusový	k2eAgMnSc1d1
manažer	manažer	k1gMnSc1
je	být	k5eAaImIp3nS
užíván	užíván	k2eAgInSc4d1
otevřený	otevřený	k2eAgInSc4d1
software	software	k1gInSc4
NoSketch	NoSketcha	k1gFnPc2
Engine	Engin	k1gInSc5
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
docent	docent	k1gMnSc1
Pavel	Pavel	k1gMnSc1
Rychlý	Rychlý	k1gMnSc1
z	z	k7c2
Centra	centrum	k1gNnSc2
zpracování	zpracování	k1gNnSc2
přirozeného	přirozený	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
na	na	k7c6
Fakultě	fakulta	k1gFnSc6
informatiky	informatika	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
software	software	k1gInSc1
dovoluje	dovolovat	k5eAaImIp3nS
prohledávání	prohledávání	k1gNnSc4
a	a	k8xC
správu	správa	k1gFnSc4
korpusů	korpus	k1gInPc2
a	a	k8xC
skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
ze	z	k7c2
dvou	dva	k4xCgInPc2
hlavních	hlavní	k2eAgInPc2d1
modulů	modul	k1gInPc2
–	–	k?
korpusového	korpusový	k2eAgInSc2d1
manažeru	manažer	k1gInSc2
Manatee	Manate	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
webového	webový	k2eAgNnSc2d1
grafického	grafický	k2eAgNnSc2d1
rozhraní	rozhraní	k1gNnSc2
Bonito	bonita	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
ČNK	ČNK	kA
ovšem	ovšem	k9
místo	místo	k1gNnSc4
Bonita	bonita	k1gFnSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
vlastní	vlastní	k2eAgInSc4d1
software	software	k1gInSc4
KonText	kontext	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
původně	původně	k6eAd1
odvozený	odvozený	k2eAgMnSc1d1
od	od	k7c2
Bonita	bonita	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://wiki.korpus.cz/doku.php/cnk:uvod#kdo_tvori_cesky_narodni_korpus	https://wiki.korpus.cz/doku.php/cnk:uvod#kdo_tvori_cesky_narodni_korpus	k1gMnSc1
<g/>
↑	↑	k?
RYCHLÝ	Rychlý	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manatee	Manatee	k1gNnSc1
<g/>
/	/	kIx~
<g/>
Bonito	bonita	k1gFnSc5
-	-	kIx~
A	a	k9
Modular	Modular	k1gMnSc1
Corpus	corpus	k1gInSc2
Manager	manager	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
st	st	kA
Workshop	workshop	k1gInSc4
on	on	k3xPp3gMnSc1
Recent	Recent	k1gMnSc1
Advances	Advancesa	k1gFnPc2
in	in	k?
Slavonic	Slavonice	k1gFnPc2
Natural	Natural	k?
Language	language	k1gFnPc2
Processing	Processing	k1gInSc1
(	(	kIx(
<g/>
RASLAN	RASLAN	kA
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
<g/>
,	,	kIx,
s.	s.	k?
65	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
210	#num#	k4
<g/>
-	-	kIx~
<g/>
4471	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ZÁCHOVÁ	ZÁCHOVÁ	kA
<g/>
,	,	kIx,
Kristina	Kristina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korpusy	korpus	k1gInPc4
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
využití	využití	k1gNnPc4
ve	v	k7c6
výuce	výuka	k1gFnSc6
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
na	na	k7c6
ZŠ	ZŠ	kA
a	a	k8xC
SŠ	SŠ	kA
<g/>
.	.	kIx.
,	,	kIx,
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
75	#num#	k4
s.	s.	k?
bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Palackého	Palackého	k2eAgMnPc2d1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k1gFnSc1
práce	práce	k1gFnSc2
PhDr.	PhDr.	kA
Petr	Petr	k1gMnSc1
Pořízka	Pořízek	k1gMnSc2
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
.	.	kIx.
s.	s.	k?
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Jazykový	jazykový	k2eAgInSc1d1
korpus	korpus	k1gInSc1
</s>
<s>
Korpusová	korpusový	k2eAgFnSc1d1
lingvistika	lingvistika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Vstupní	vstupní	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
ČNK	ČNK	kA
</s>
<s>
NoSketch	NoSketch	k1gMnSc1
Engine	Engin	k1gInSc5
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
