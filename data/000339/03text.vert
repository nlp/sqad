<s>
Polárka	Polárka	k1gFnSc1	Polárka
(	(	kIx(	(
<g/>
α	α	k?	α
UMi	UMi	k1gMnSc1	UMi
<g/>
,	,	kIx,	,
α	α	k?	α
Ursae	Ursae	k1gFnSc1	Ursae
Minoris	Minoris	k1gFnSc1	Minoris
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Polaris	Polaris	k1gFnSc1	Polaris
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Malého	Malý	k1gMnSc2	Malý
medvěda	medvěd	k1gMnSc2	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
severnímu	severní	k2eAgInSc3d1	severní
nebeskému	nebeský	k2eAgInSc3d1	nebeský
pólu	pól	k1gInSc3	pól
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
dělá	dělat	k5eAaImIp3nS	dělat
severní	severní	k2eAgFnSc4d1	severní
polární	polární	k2eAgFnSc4d1	polární
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Polárka	Polárka	k1gFnSc1	Polárka
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
názvů	název	k1gInPc2	název
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
nejpoužívanější	používaný	k2eAgFnSc1d3	nejpoužívanější
Polárka	Polárka	k1gFnSc1	Polárka
nebo	nebo	k8xC	nebo
Severka	Severka	k1gFnSc1	Severka
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
polaris	polaris	k1gInSc1	polaris
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
Stella	Stella	k1gFnSc1	Stella
Polaris	Polaris	k1gFnPc1	Polaris
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
Polární	polární	k2eAgFnSc1d1	polární
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgNnSc1d1	řecké
jméno	jméno	k1gNnSc1	jméno
Cynosura	Cynosura	k1gFnSc1	Cynosura
(	(	kIx(	(
<g/>
Κ	Κ	k?	Κ
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
ocas	ocas	k1gInSc4	ocas
psa	pes	k1gMnSc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Alrucaba	Alrucaba	k1gFnSc1	Alrucaba
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
Alfa	alfa	k1gFnSc1	alfa
Ursae	Ursae	k1gFnSc1	Ursae
Minoris	Minoris	k1gFnSc1	Minoris
(	(	kIx(	(
<g/>
α	α	k?	α
UMi	UMi	k1gFnSc1	UMi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Flamsteedovo	Flamsteedův	k2eAgNnSc1d1	Flamsteedovo
označení	označení	k1gNnSc1	označení
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
Ursae	Ursae	k1gNnPc2	Ursae
Minoris	Minoris	k1gFnPc2	Minoris
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
číslo	číslo	k1gNnSc1	číslo
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
SAO	SAO	kA	SAO
je	být	k5eAaImIp3nS	být
SAO	SAO	kA	SAO
308	[number]	k4	308
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
HD	HD	kA	HD
má	mít	k5eAaImIp3nS	mít
označení	označení	k1gNnSc4	označení
HD	HD	kA	HD
8890	[number]	k4	8890
<g/>
.	.	kIx.	.
</s>
<s>
Polárka	Polárka	k1gFnSc1	Polárka
leží	ležet	k5eAaImIp3nS	ležet
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
přímé	přímý	k2eAgFnSc6d1	přímá
linii	linie	k1gFnSc6	linie
s	s	k7c7	s
osou	osa	k1gFnSc7	osa
otáčení	otáčení	k1gNnSc2	otáčení
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
nebeském	nebeský	k2eAgInSc6d1	nebeský
pólu	pól	k1gInSc6	pól
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
zdát	zdát	k5eAaPmF	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Polárka	Polárka	k1gFnSc1	Polárka
"	"	kIx"	"
<g/>
stojí	stát	k5eAaImIp3nS	stát
<g/>
"	"	kIx"	"
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc4d1	ostatní
hvězdy	hvězda	k1gFnPc4	hvězda
se	se	k3xPyFc4	se
otáčí	otáčet	k5eAaImIp3nS	otáčet
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
současností	současnost	k1gFnSc7	současnost
je	být	k5eAaImIp3nS	být
Polárka	Polárka	k1gFnSc1	Polárka
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
<g/>
°	°	k?	°
od	od	k7c2	od
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
nebeského	nebeský	k2eAgInSc2d1	nebeský
pólu	pól	k1gInSc2	pól
otáčí	otáčet	k5eAaImIp3nS	otáčet
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
kruzích	kruh	k1gInPc6	kruh
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
2	[number]	k4	2
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
dvakrát	dvakrát	k6eAd1	dvakrát
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
Polárka	Polárka	k1gFnSc1	Polárka
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
severním	severní	k2eAgInSc6d1	severní
azimutu	azimut	k1gInSc6	azimut
<g/>
.	.	kIx.	.
</s>
<s>
Polárka	Polárka	k1gFnSc1	Polárka
nebude	být	k5eNaImBp3nS	být
vždy	vždy	k6eAd1	vždy
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
severní	severní	k2eAgInSc4d1	severní
nebeský	nebeský	k2eAgInSc4d1	nebeský
pól	pól	k1gInSc4	pól
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
precese	precese	k1gFnSc2	precese
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
za	za	k7c4	za
tisíce	tisíc	k4xCgInPc4	tisíc
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
hvězdy	hvězda	k1gFnPc1	hvězda
taktéž	taktéž	k?	taktéž
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
polárními	polární	k2eAgFnPc7d1	polární
hvězdami	hvězda	k1gFnPc7	hvězda
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
jimi	on	k3xPp3gMnPc7	on
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
budou	být	k5eAaImBp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
hvězdy	hvězda	k1gFnPc1	hvězda
Thuban	Thubany	k1gInPc2	Thubany
ze	z	k7c2	z
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Draka	drak	k1gMnSc4	drak
<g/>
,	,	kIx,	,
Vega	Vegus	k1gMnSc4	Vegus
ze	z	k7c2	z
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Lyry	lyra	k1gFnSc2	lyra
a	a	k8xC	a
Deneb	Denba	k1gFnPc2	Denba
z	z	k7c2	z
Labutě	labuť	k1gFnSc2	labuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
Polárky	Polárka	k1gFnSc2	Polárka
od	od	k7c2	od
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejmenší	malý	k2eAgFnPc4d3	nejmenší
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
(	(	kIx(	(
<g/>
0,5	[number]	k4	0,5
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2100	[number]	k4	2100
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
vzdalovat	vzdalovat	k5eAaImF	vzdalovat
<g/>
.	.	kIx.	.
</s>
<s>
Polárka	Polárka	k1gFnSc1	Polárka
je	být	k5eAaImIp3nS	být
pětinásobná	pětinásobný	k2eAgFnSc1d1	pětinásobná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
vidíme	vidět	k5eAaImIp1nP	vidět
jen	jen	k9	jen
největšího	veliký	k2eAgMnSc2d3	veliký
a	a	k8xC	a
nejjasnějšího	jasný	k2eAgMnSc2d3	nejjasnější
člena	člen	k1gMnSc2	člen
soustavy	soustava	k1gFnSc2	soustava
-	-	kIx~	-
žlutého	žlutý	k2eAgMnSc2d1	žlutý
veleobra	veleobr	k1gMnSc2	veleobr
(	(	kIx(	(
<g/>
Polárka	Polárka	k1gFnSc1	Polárka
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
třicetkrát	třicetkrát	k6eAd1	třicetkrát
větší	veliký	k2eAgInSc1d2	veliký
průměr	průměr	k1gInSc1	průměr
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
zářivost	zářivost	k1gFnSc1	zářivost
5000	[number]	k4	5000
Sluncí	slunce	k1gNnPc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
absolutní	absolutní	k2eAgFnSc1d1	absolutní
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
-5,1	-5,1	k4	-5,1
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pulzující	pulzující	k2eAgFnSc1d1	pulzující
proměnná	proměnný	k2eAgFnSc1d1	proměnná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
cefeidu	cefeida	k1gFnSc4	cefeida
<g/>
.	.	kIx.	.
</s>
<s>
Jasnost	jasnost	k1gFnSc1	jasnost
slabě	slabě	k6eAd1	slabě
kolísá	kolísat	k5eAaImIp3nS	kolísat
z	z	k7c2	z
1,9	[number]	k4	1,9
na	na	k7c4	na
2,1	[number]	k4	2,1
mag	mag	k?	mag
s	s	k7c7	s
periodou	perioda	k1gFnSc7	perioda
3,97	[number]	k4	3,97
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
jasnosti	jasnost	k1gFnSc2	jasnost
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgFnPc1d1	malá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgInP	dát
pozorovat	pozorovat	k5eAaImF	pozorovat
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Amplituda	amplituda	k1gFnSc1	amplituda
pulzů	pulz	k1gInPc2	pulz
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
rocích	rok	k1gInPc6	rok
stále	stále	k6eAd1	stále
zmenšovala	zmenšovat	k5eAaImAgFnS	zmenšovat
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gFnPc4	on
zjistit	zjistit	k5eAaPmF	zjistit
jen	jen	k9	jen
fotometrem	fotometr	k1gInSc7	fotometr
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
hlavní	hlavní	k2eAgFnSc2d1	hlavní
složky	složka	k1gFnSc2	složka
obíhá	obíhat	k5eAaImIp3nS	obíhat
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
slabý	slabý	k2eAgMnSc1d1	slabý
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
.	.	kIx.	.
</s>
<s>
Nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
v	v	k7c6	v
dalekohledu	dalekohled	k1gInSc6	dalekohled
vidět	vidět	k5eAaImF	vidět
odděleně	odděleně	k6eAd1	odděleně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
posunem	posun	k1gInSc7	posun
čar	čára	k1gFnPc2	čára
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
Polárky	Polárka	k1gFnSc2	Polárka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
složka	složka	k1gFnSc1	složka
(	(	kIx(	(
<g/>
Polárka	Polárka	k1gFnSc1	Polárka
A	a	k9	a
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
spektroskopická	spektroskopický	k2eAgFnSc1d1	spektroskopická
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
cefeidy	cefeida	k1gFnSc2	cefeida
a	a	k8xC	a
spektroskopických	spektroskopický	k2eAgMnPc2d1	spektroskopický
průvodců	průvodce	k1gMnPc2	průvodce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
obíhají	obíhat	k5eAaImIp3nP	obíhat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
složku	složka	k1gFnSc4	složka
tři	tři	k4xCgMnPc1	tři
další	další	k2eAgMnPc1d1	další
průvodci	průvodce	k1gMnPc1	průvodce
(	(	kIx(	(
<g/>
Polárka	Polárka	k1gFnSc1	Polárka
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
a	a	k8xC	a
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
vzdálení	vzdálený	k2eAgMnPc1d1	vzdálený
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
pozemské	pozemský	k2eAgInPc4d1	pozemský
dalekohledy	dalekohled	k1gInPc4	dalekohled
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
zachytit	zachytit	k5eAaPmF	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Polárka	Polárka	k1gFnSc1	Polárka
B	B	kA	B
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
spektrální	spektrální	k2eAgFnSc2d1	spektrální
třídy	třída	k1gFnSc2	třída
F3V	F3V	k1gFnSc1	F3V
jasná	jasný	k2eAgFnSc1d1	jasná
8,8	[number]	k4	8,8
mag	mag	k?	mag
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
vidět	vidět	k5eAaImF	vidět
pomocí	pomocí	k7c2	pomocí
menšího	malý	k2eAgInSc2d2	menší
hvězdářského	hvězdářský	k2eAgInSc2d1	hvězdářský
dalekohledu	dalekohled	k1gInSc2	dalekohled
(	(	kIx(	(
<g/>
alespoň	alespoň	k9	alespoň
7,5	[number]	k4	7,5
cm	cm	kA	cm
<g/>
)	)	kIx)	)
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
18	[number]	k4	18
<g/>
"	"	kIx"	"
od	od	k7c2	od
Polárky	Polárka	k1gFnSc2	Polárka
A.	A.	kA	A.
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jen	jen	k9	jen
optická	optický	k2eAgFnSc1d1	optická
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
<g/>
.	.	kIx.	.
</s>
<s>
Silným	silný	k2eAgInSc7d1	silný
dalekohledem	dalekohled	k1gInSc7	dalekohled
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uvidět	uvidět	k5eAaPmF	uvidět
dvě	dva	k4xCgFnPc4	dva
slabé	slabý	k2eAgFnPc4d1	slabá
složky	složka	k1gFnPc4	složka
-	-	kIx~	-
Polárku	Polárka	k1gFnSc4	Polárka
C	C	kA	C
(	(	kIx(	(
<g/>
13	[number]	k4	13
mag	mag	k?	mag
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
43	[number]	k4	43
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Polárku	Polárka	k1gFnSc4	Polárka
D	D	kA	D
(	(	kIx(	(
<g/>
12	[number]	k4	12
mag	mag	k?	mag
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
83	[number]	k4	83
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
432	[number]	k4	432
ly	ly	k?	ly
a	a	k8xC	a
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
se	se	k3xPyFc4	se
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
rychlostí	rychlost	k1gFnSc7	rychlost
17	[number]	k4	17
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Polárka	Polárka	k1gFnSc1	Polárka
je	být	k5eAaImIp3nS	být
také	také	k9	také
členem	člen	k1gInSc7	člen
málo	málo	k6eAd1	málo
známého	známý	k2eAgInSc2d1	známý
asterismu	asterismus	k1gInSc2	asterismus
Snubní	snubní	k2eAgInSc4d1	snubní
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
představuje	představovat	k5eAaImIp3nS	představovat
diamant	diamant	k1gInSc4	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Polárka	Polárka	k1gFnSc1	Polárka
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
směřuje	směřovat	k5eAaImIp3nS	směřovat
zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
(	(	kIx(	(
<g/>
epocha	epocha	k1gFnSc1	epocha
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
těsně	těsně	k6eAd1	těsně
vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
leží	ležet	k5eAaImIp3nS	ležet
severní	severní	k2eAgInSc1d1	severní
nebeský	nebeský	k2eAgInSc1d1	nebeský
pól	pól	k1gInSc1	pól
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
odchylka	odchylka	k1gFnSc1	odchylka
Polárky	Polárka	k1gFnSc2	Polárka
od	od	k7c2	od
nebeského	nebeský	k2eAgInSc2d1	nebeský
pólu	pól	k1gInSc2	pól
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
1	[number]	k4	1
úhlový	úhlový	k2eAgInSc1d1	úhlový
stupeň	stupeň	k1gInSc1	stupeň
-	-	kIx~	-
58	[number]	k4	58
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
precese	precese	k1gFnSc2	precese
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
tato	tento	k3xDgFnSc1	tento
hodnota	hodnota	k1gFnSc1	hodnota
stále	stále	k6eAd1	stále
zmenšovat	zmenšovat	k5eAaImF	zmenšovat
a	a	k8xC	a
minima	minimum	k1gNnPc4	minimum
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2100	[number]	k4	2100
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
však	však	k9	však
severní	severní	k2eAgInSc1d1	severní
nebeský	nebeský	k2eAgInSc1d1	nebeský
pól	pól	k1gInSc1	pól
bude	být	k5eAaImBp3nS	být
vzdalovat	vzdalovat	k5eAaImF	vzdalovat
a	a	k8xC	a
k	k	k7c3	k
Polárce	Polárka	k1gFnSc3	Polárka
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
až	až	k9	až
za	za	k7c4	za
25	[number]	k4	25
800	[number]	k4	800
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
platónský	platónský	k2eAgInSc4d1	platónský
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
ji	on	k3xPp3gFnSc4	on
omylem	omylem	k6eAd1	omylem
nazývají	nazývat	k5eAaImIp3nP	nazývat
večernice	večernice	k1gFnPc1	večernice
nebo	nebo	k8xC	nebo
jitřenka	jitřenka	k1gFnSc1	jitřenka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
patří	patřit	k5eAaImIp3nP	patřit
planetám	planeta	k1gFnPc3	planeta
Venuši	Venuše	k1gFnSc6	Venuše
či	či	k8xC	či
Merkuru	Merkur	k1gInSc6	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Polárka	Polárka	k1gFnSc1	Polárka
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
také	také	k9	také
na	na	k7c4	na
vlajku	vlajka	k1gFnSc4	vlajka
státu	stát	k1gInSc2	stát
Aljaška	Aljaška	k1gFnSc1	Aljaška
a	a	k8xC	a
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Polárka	Polárka	k1gFnSc1	Polárka
A	A	kA	A
(	(	kIx(	(
<g/>
1,97	[number]	k4	1,97
mag	mag	k?	mag
<g/>
)	)	kIx)	)
Polárka	Polárka	k1gFnSc1	Polárka
Ab	Ab	k1gFnSc1	Ab
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
2,5	[number]	k4	2,5
<g/>
"	"	kIx"	"
od	od	k7c2	od
Polárky	Polárka	k1gFnSc2	Polárka
A	A	kA	A
<g/>
)	)	kIx)	)
Polárka	Polárka	k1gFnSc1	Polárka
B	B	kA	B
(	(	kIx(	(
<g/>
8,8	[number]	k4	8,8
mag	mag	k?	mag
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
"	"	kIx"	"
od	od	k7c2	od
Polárky	Polárka	k1gFnSc2	Polárka
A	A	kA	A
<g/>
)	)	kIx)	)
Polárka	Polárka	k1gFnSc1	Polárka
C	C	kA	C
(	(	kIx(	(
<g/>
13	[number]	k4	13
mag	mag	k?	mag
<g/>
,	,	kIx,	,
43	[number]	k4	43
<g/>
"	"	kIx"	"
od	od	k7c2	od
Polárky	Polárka	k1gFnSc2	Polárka
A	A	kA	A
<g/>
)	)	kIx)	)
Polárka	Polárka	k1gFnSc1	Polárka
D	D	kA	D
(	(	kIx(	(
<g/>
12	[number]	k4	12
mag	mag	k?	mag
<g/>
,	,	kIx,	,
83	[number]	k4	83
<g/>
"	"	kIx"	"
od	od	k7c2	od
Polárky	Polárka	k1gFnSc2	Polárka
A	a	k9	a
<g/>
)	)	kIx)	)
Zemská	zemský	k2eAgFnSc1d1	zemská
precese	precese	k1gFnSc1	precese
Malý	Malý	k1gMnSc1	Malý
medvěd	medvěd	k1gMnSc1	medvěd
Nebeský	nebeský	k2eAgInSc1d1	nebeský
pól	pól	k1gInSc1	pól
Kleczek	Kleczka	k1gFnPc2	Kleczka
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Velká	velký	k2eAgFnSc1d1	velká
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
582	[number]	k4	582
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80-200-0906-X	[number]	k4	80-200-0906-X
Hubblesite	Hubblesit	k1gInSc5	Hubblesit
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
Hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
Valašské	valašský	k2eAgFnPc1d1	Valašská
Meziříčí	Meziříčí	k1gNnSc2	Meziříčí
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Polárka	Polárka	k1gFnSc1	Polárka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Polárka	Polárka	k1gFnSc1	Polárka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
