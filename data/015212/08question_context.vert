<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
Frederika	Frederik	k1gMnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
zákonitým	zákonitý	k2eAgMnSc7d1
dědicem	dědic	k1gMnSc7
britského	britský	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
princem	princ	k1gMnSc7
z	z	k7c2
Walesu	Wales	k1gInSc2
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1761	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Šarlotou	Šarlota	k1gFnSc7
von	von	k1gInSc4
Mecklenburg-Strelitz	Mecklenburg-Strelitza	k1gFnPc2
a	a	k8xC
o	o	k7c4
dva	dva	k4xCgInPc4
týdny	týden	k1gInPc4
později	pozdě	k6eAd2
byli	být	k5eAaImAgMnP
oba	dva	k4xCgMnPc1
korunováni	korunován	k2eAgMnPc1d1
ve	v	k7c6
Westminsterském	Westminsterský	k2eAgNnSc6d1
opatství	opatství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>