<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
britská	britský	k2eAgFnSc1d1	britská
kapela	kapela	k1gFnSc1	kapela
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
hrající	hrající	k2eAgInSc4d1	hrající
alternativní	alternativní	k2eAgInSc4d1	alternativní
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
?	?	kIx.	?
</s>
