<s>
Chrám	chrám	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
letech	léto	k1gNnPc6
1837	#num#	k4
až	až	k9
1840	#num#	k4
v	v	k7c6
klasicistním	klasicistní	k2eAgInSc6d1
architektonickém	architektonický	k2eAgInSc6d1
stylu	styl	k1gInSc6
s	s	k7c7
prvky	prvek	k1gInPc7
baroka	baroko	k1gNnSc2
na	na	k7c4
rozkaz	rozkaz	k1gInSc4
knížete	kníže	k1gMnSc2
Miloše	Miloš	k1gMnSc2
Obrenoviće	Obrenović	k1gMnSc2
<g/>
.	.	kIx.
</s>