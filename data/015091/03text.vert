<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Michaela	Michael	k1gMnSc2
archanděla	archanděl	k1gMnSc2
(	(	kIx(
<g/>
Bělehrad	Bělehrad	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Chrám	chrám	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Michala	Michal	k1gMnSc2
Archanděla	archanděl	k1gMnSc2
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
klasicistní	klasicistní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1841	#num#	k4
Materiál	materiál	k1gInSc1
</s>
<s>
cihla	cihla	k1gFnSc1
Pojmenováno	pojmenován	k2eAgNnSc1d1
po	po	k7c6
</s>
<s>
Michael	Michael	k1gMnSc1
archanděl	archanděl	k1gMnSc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Bělehrad	Bělehrad	k1gInSc1
<g/>
,	,	kIx,
Srbsko	Srbsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
44	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
5,03	5,03	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
7,72	7,72	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Michaela	Michael	k1gMnSc2
archanděla	archanděl	k1gMnSc2
(	(	kIx(
<g/>
srbsky	srbsky	k6eAd1
v	v	k7c6
cyrilici	cyrilice	k1gFnSc6
С	С	k?
ц	ц	k?
С	С	k?
А	А	k?
М	М	k?
a	a	k8xC
v	v	k7c6
latince	latinka	k1gFnSc6
Saborna	Saborno	k1gNnSc2
crkva	crkvo	k1gNnSc2
Svetog	Svetog	k1gMnSc1
Arhanđela	Arhanđel	k1gMnSc2
Mihaila	Mihail	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
srbským	srbský	k2eAgMnSc7d1
pravoslavný	pravoslavný	k2eAgInSc1d1
chrám	chrám	k1gInSc1
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Srbska	Srbsko	k1gNnSc2
Bělehradě	Bělehrad	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c4
Kosančićevom	Kosančićevom	k1gInSc4
vencu	vencus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Chrám	chrám	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
letech	léto	k1gNnPc6
1837	#num#	k4
až	až	k9
1840	#num#	k4
v	v	k7c6
klasicistním	klasicistní	k2eAgInSc6d1
architektonickém	architektonický	k2eAgInSc6d1
stylu	styl	k1gInSc6
s	s	k7c7
prvky	prvek	k1gInPc7
baroka	baroko	k1gNnSc2
na	na	k7c4
rozkaz	rozkaz	k1gInSc4
knížete	kníže	k1gMnSc2
Miloše	Miloš	k1gMnSc2
Obrenoviće	Obrenović	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
hlavním	hlavní	k2eAgMnSc7d1
architektem	architekt	k1gMnSc7
byl	být	k5eAaImAgMnS
Adam	Adam	k1gMnSc1
Fridrih	Fridrih	k1gMnSc1
Kverfeld	Kverfeld	k1gMnSc1
<g/>
,	,	kIx,
vzorem	vzor	k1gInSc7
byl	být	k5eAaImAgMnS
podobný	podobný	k2eAgInSc4d1
kostel	kostel	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
Sremských	Sremský	k2eAgInPc6d1
Karlovcích	Karlovac	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
chrám	chrám	k1gInSc1
stal	stát	k5eAaPmAgInS
největším	veliký	k2eAgInSc7d3
kostelem	kostel	k1gInSc7
<g/>
,	,	kIx,
zbudovaným	zbudovaný	k2eAgMnSc7d1
na	na	k7c6
srbském	srbský	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byl	být	k5eAaImAgInS
také	také	k6eAd1
hlavním	hlavní	k2eAgInSc7d1
pravoslavným	pravoslavný	k2eAgInSc7d1
chrámem	chrám	k1gInSc7
Srbska	Srbsko	k1gNnSc2
<g/>
,	,	kIx,
probíhaly	probíhat	k5eAaImAgFnP
v	v	k7c6
něm	on	k3xPp3gNnSc6
korunovace	korunovace	k1gFnSc1
srbských	srbský	k2eAgMnPc2d1
panovníků	panovník	k1gMnPc2
a	a	k8xC
intronizace	intronizace	k1gFnSc1
řady	řada	k1gFnSc2
srbských	srbský	k2eAgMnPc2d1
patriarchů	patriarcha	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
uskutečnila	uskutečnit	k5eAaPmAgFnS
korunovace	korunovace	k1gFnSc1
srbského	srbský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Petra	Petr	k1gMnSc2
I.	I.	kA
Karađorđeviće	Karađorđević	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Architektura	architektura	k1gFnSc1
</s>
<s>
Klasicistní	klasicistní	k2eAgFnSc4d1
stavbu	stavba	k1gFnSc4
doplňují	doplňovat	k5eAaImIp3nP
barokní	barokní	k2eAgInPc1d1
prvky	prvek	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portál	portál	k1gInSc1
chrámu	chrám	k1gInSc2
zdobí	zdobit	k5eAaImIp3nS
mozaiky	mozaika	k1gFnPc4
Přesvaté	přesvatý	k2eAgFnSc2d1
bohorodičky	bohorodička	k1gFnSc2
<g/>
,	,	kIx,
Svaté	svatý	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
a	a	k8xC
archandělů	archanděl	k1gMnPc2
Michaela	Michael	k1gMnSc2
a	a	k8xC
Gabriela	Gabriel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ikonostas	ikonostas	k1gInSc1
sestavil	sestavit	k5eAaPmAgMnS
sochař	sochař	k1gMnSc1
Dimitrije	Dimitrije	k1gMnSc1
Petrović	Petrović	k1gMnSc1
a	a	k8xC
ikony	ikona	k1gFnPc1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
dílny	dílna	k1gFnSc2
A.	A.	kA
Avramoviće	Avramović	k1gFnSc2
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
krytu	kryt	k1gInSc6
chrámu	chrám	k1gInSc2
jsou	být	k5eAaImIp3nP
uloženy	uložen	k2eAgInPc1d1
ostatky	ostatek	k1gInPc1
Štěpána	Štěpán	k1gMnSc2
Uroše	Uroše	k1gFnSc2
V.	V.	kA
(	(	kIx(
<g/>
1337	#num#	k4
<g/>
–	–	k?
<g/>
1371	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
a	a	k8xC
Štěpána	Štěpána	k1gFnSc1
Štiljanoviće	Štiljanoviće	k1gFnSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
několika	několik	k4yIc2
srbských	srbský	k2eAgMnPc2d1
patriarchů	patriarcha	k1gMnPc2
a	a	k8xC
také	také	k9
národních	národní	k2eAgMnPc2d1
buditelů	buditel	k1gMnPc2
(	(	kIx(
<g/>
Vuk	Vuk	k1gMnSc1
Stefanović	Stefanović	k1gMnSc1
Karadžić	Karadžić	k1gMnSc1
<g/>
,	,	kIx,
Dositej	Dositej	k1gMnSc1
Obradović	Obradović	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Katedrálny	Katedrálna	k1gFnSc2
chrám	chrám	k1gInSc1
svätého	svätý	k2eAgMnSc2d1
Michala	Michal	k1gMnSc2
Archanjela	Archanjela	k1gMnSc2
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Chrám	chrám	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Michala	Michal	k1gMnSc2
Archanděla	archanděl	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
98107846	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
141200127	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
98107846	#num#	k4
</s>
