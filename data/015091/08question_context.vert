<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Michaela	Michael	k1gMnSc2
archanděla	archanděl	k1gMnSc2
(	(	kIx(
<g/>
srbsky	srbsky	k6eAd1
v	v	k7c6
cyrilici	cyrilice	k1gFnSc6
С	С	k?
ц	ц	k?
С	С	k?
А	А	k?
М	М	k?
a	a	k8xC
v	v	k7c6
latince	latinka	k1gFnSc6
Saborna	Saborno	k1gNnSc2
crkva	crkvo	k1gNnSc2
Svetog	Svetog	k1gMnSc1
Arhanđela	Arhanđel	k1gMnSc2
Mihaila	Mihail	k1gMnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
srbským	srbský	k2eAgMnSc7d1
pravoslavný	pravoslavný	k2eAgInSc1d1
chrám	chrám	k1gInSc1
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Srbska	Srbsko	k1gNnSc2
Bělehradě	Bělehrad	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c4
Kosančićevom	Kosančićevom	k1gInSc4
vencu	vencus	k1gInSc2
<g/>
.	.	kIx.
</s>