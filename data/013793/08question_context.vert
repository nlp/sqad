<s desamb="1">
Pátral	pátrat	k5eAaImAgMnS
na	na	k7c6
severozápadě	severozápad	k1gInSc6
Malé	Malé	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
a	a	k8xC
podle	podle	k7c2
popisů	popis	k1gInPc2
krajiny	krajina	k1gFnSc2
v	v	k7c6
Homérově	Homérův	k2eAgInSc6d1
eposu	epos	k1gInSc6
Ilias	Ilias	k1gFnSc1
(	(	kIx(
<g/>
Iliada	Iliada	k1gFnSc1
<g/>
)	)	kIx)
nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
legendární	legendární	k2eAgNnSc4d1
město	město	k1gNnSc4
Trója	Trója	k1gFnSc1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
Mykény	Mykény	k1gFnPc4
<g/>
.	.	kIx.
</s>