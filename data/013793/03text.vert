<s>
Heinrich	Heinrich	k1gMnSc1
Schliemann	Schliemann	k1gMnSc1
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
Schliemann	Schliemann	k1gMnSc1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Johan	Johan	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
Julius	Julius	k1gMnSc1
Schliemann	Schliemann	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1822	#num#	k4
<g/>
Neubukow	Neubukow	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1890	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
68	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Neapol	Neapol	k1gFnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
meningitida	meningitida	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Próto	Próto	k1gNnSc1
Nekrotafeío	Nekrotafeío	k6eAd1
Athinón	Athinón	k1gInSc1
Bydliště	bydliště	k1gNnSc2
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
(	(	kIx(
<g/>
1846	#num#	k4
<g/>
–	–	k?
<g/>
1917	#num#	k4
<g/>
)	)	kIx)
<g/>
AthényPetrohradIliou	AthényPetrohradIlia	k1gFnSc7
Melathron	Melathron	k1gInSc4
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
1866	#num#	k4
<g/>
–	–	k?
<g/>
1868	#num#	k4
<g/>
)	)	kIx)
<g/>
Rostocká	Rostocký	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
obchodník	obchodník	k1gMnSc1
<g/>
,	,	kIx,
antropolog	antropolog	k1gMnSc1
<g/>
,	,	kIx,
kunsthistorik	kunsthistorik	k1gMnSc1
<g/>
,	,	kIx,
archeolog	archeolog	k1gMnSc1
<g/>
,	,	kIx,
polyglot	polyglot	k1gMnSc1
<g/>
,	,	kIx,
cestovatel	cestovatel	k1gMnSc1
a	a	k8xC
podnikatel	podnikatel	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Royal	Royal	k1gMnSc1
Gold	Gold	k1gMnSc1
Medal	Medal	k1gMnSc1
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
)	)	kIx)
<g/>
Čestný	čestný	k2eAgMnSc1d1
občan	občan	k1gMnSc1
Berlína	Berlín	k1gInSc2
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Sophia	Sophia	k1gFnSc1
Schliemannová	Schliemannová	k1gFnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Agamemnon	Agamemnon	k1gInSc1
SchliemannAndromache	SchliemannAndromache	k1gInSc1
Schliemannová	Schliemannová	k1gFnSc1
Rodiče	rodič	k1gMnSc2
</s>
<s>
Luise	Luisa	k1gFnSc3
Therese	Therese	k1gFnSc2
Sophie	Sophie	k1gFnSc2
Schliemann	Schliemanna	k1gFnPc2
Příbuzní	příbuzný	k2eAgMnPc1d1
</s>
<s>
Vadim	Vadim	k?
Nikolajevič	Nikolajevič	k1gMnSc1
Andrusov	Andrusov	k1gInSc1
(	(	kIx(
<g/>
vnuk	vnuk	k1gMnSc1
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Heinrich	Heinrich	k1gMnSc1
Schliemann	Schliemann	k1gMnSc1
<g/>
,	,	kIx,
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Johann	Johann	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
Julius	Julius	k1gMnSc1
Schliemann	Schliemann	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1822	#num#	k4
<g/>
,	,	kIx,
Neubukow	Neubukow	k1gFnSc1
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1890	#num#	k4
<g/>
,	,	kIx,
Neapol	Neapol	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
obchodník	obchodník	k1gMnSc1
a	a	k8xC
archeolog	archeolog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pátral	pátrat	k5eAaImAgMnS
na	na	k7c6
severozápadě	severozápad	k1gInSc6
Malé	Malé	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
a	a	k8xC
podle	podle	k7c2
popisů	popis	k1gInPc2
krajiny	krajina	k1gFnSc2
v	v	k7c6
Homérově	Homérův	k2eAgInSc6d1
eposu	epos	k1gInSc6
Ilias	Ilias	k1gFnSc1
(	(	kIx(
<g/>
Iliada	Iliada	k1gFnSc1
<g/>
)	)	kIx)
nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
legendární	legendární	k2eAgNnSc4d1
město	město	k1gNnSc4
Trója	Trója	k1gFnSc1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
Mykény	Mykény	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Dětství	dětství	k1gNnSc1
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
chudý	chudý	k2eAgMnSc1d1
evangelický	evangelický	k2eAgMnSc1d1
pastor	pastor	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
něm	on	k3xPp3gInSc6
vymyšlenými	vymyšlený	k2eAgInPc7d1
příběhy	příběh	k1gInPc7
probudil	probudit	k5eAaPmAgInS
víru	víra	k1gFnSc4
v	v	k7c4
reálný	reálný	k2eAgInSc4d1
základ	základ	k1gInSc4
vyprávění	vyprávění	k1gNnSc2
a	a	k8xC
popisů	popis	k1gInPc2
v	v	k7c6
Homérových	Homérův	k2eAgInPc6d1
eposech	epos	k1gInPc6
a	a	k8xC
také	také	k9
touhu	touha	k1gFnSc4
jednou	jednou	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
bájnou	bájný	k2eAgFnSc4d1
Tróju	Trója	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
Heinrichovy	Heinrichův	k2eAgFnSc2d1
matky	matka	k1gFnSc2
(	(	kIx(
<g/>
když	když	k8xS
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
devět	devět	k4xCc4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
ho	on	k3xPp3gMnSc4
již	již	k6eAd1
vychovával	vychovávat	k5eAaImAgMnS
jen	jen	k9
on	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
začal	začít	k5eAaPmAgMnS
pít	pít	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
celá	celý	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
žila	žít	k5eAaImAgFnS
v	v	k7c6
chudobě	chudoba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1833	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1836	#num#	k4
chodil	chodit	k5eAaImAgMnS
Schliemann	Schliemann	k1gMnSc1
do	do	k7c2
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Neusterlitzu	Neusterlitz	k1gInSc6
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1836	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1841	#num#	k4
pak	pak	k6eAd1
do	do	k7c2
knihkupeckého	knihkupecký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
poprvé	poprvé	k6eAd1
slyšel	slyšet	k5eAaImAgMnS
Homéra	Homér	k1gMnSc4
v	v	k7c6
řečtině	řečtina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Kvůli	kvůli	k7c3
nemoci	nemoc	k1gFnSc3
odešel	odejít	k5eAaPmAgInS
z	z	k7c2
knihkupeckého	knihkupecký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
se	se	k3xPyFc4
najmout	najmout	k5eAaPmF
na	na	k7c4
loď	loď	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
plula	plout	k5eAaImAgFnS
z	z	k7c2
Hamburku	Hamburk	k1gInSc2
do	do	k7c2
Venezuely	Venezuela	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
ztroskotala	ztroskotat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
účetním	účetní	k2eAgMnSc7d1
a	a	k8xC
úředníkem	úředník	k1gMnSc7
v	v	k7c6
Amsterdamské	amsterdamský	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věnoval	věnovat	k5eAaImAgInS,k5eAaPmAgInS
se	se	k3xPyFc4
učení	učení	k1gNnSc2
různých	různý	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgMnS
si	se	k3xPyFc3
na	na	k7c4
to	ten	k3xDgNnSc4
vlastní	vlastnit	k5eAaImIp3nS
metodu	metoda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1846	#num#	k4
firmou	firma	k1gFnSc7
přeložen	přeložit	k5eAaPmNgInS
do	do	k7c2
ruského	ruský	k2eAgInSc2d1
Petrohradu	Petrohrad	k1gInSc2
<g/>
,	,	kIx,
založil	založit	k5eAaPmAgMnS
si	se	k3xPyFc3
zde	zde	k6eAd1
vlastní	vlastní	k2eAgFnSc4d1
firmu	firma	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodoval	obchodovat	k5eAaImAgMnS
ale	ale	k9
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1850	#num#	k4
na	na	k7c4
čas	čas	k1gInSc4
podnikal	podnikat	k5eAaImAgMnS
i	i	k9
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c6
jejím	její	k3xOp3gInSc6
připojením	připojení	k1gNnSc7
ke	k	k7c3
Spojeným	spojený	k2eAgInPc3d1
státům	stát	k1gInPc3
americkým	americký	k2eAgInPc3d1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
automaticky	automaticky	k6eAd1
jejich	jejich	k3xOp3gMnSc7
občanem	občan	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
obchodně	obchodně	k6eAd1
profitoval	profitovat	k5eAaBmAgMnS
z	z	k7c2
kalifornské	kalifornský	k2eAgFnSc2d1
zlaté	zlatý	k2eAgFnSc2d1
horečky	horečka	k1gFnSc2
<g/>
,	,	kIx,
brzy	brzy	k6eAd1
se	se	k3xPyFc4
však	však	k9
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Petrohradu	Petrohrad	k1gInSc2
a	a	k8xC
našel	najít	k5eAaPmAgMnS
další	další	k2eAgFnPc4d1
obchodní	obchodní	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
v	v	k7c6
průběhu	průběh	k1gInSc6
Krymské	krymský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schliemann	Schliemann	k1gMnSc1
se	se	k3xPyFc4
vypracoval	vypracovat	k5eAaPmAgMnS
ve	v	k7c4
váženého	vážený	k2eAgMnSc4d1
petrohradského	petrohradský	k2eAgMnSc4d1
obchodníka	obchodník	k1gMnSc4
<g/>
,	,	kIx,
soudce	soudce	k1gMnSc4
tamního	tamní	k2eAgInSc2d1
Obchodního	obchodní	k2eAgInSc2d1
soudu	soud	k1gInSc2
a	a	k8xC
ředitele	ředitel	k1gMnSc2
zdejší	zdejší	k2eAgFnSc2d1
Carské	carský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Splněný	splněný	k2eAgInSc1d1
sen	sen	k1gInSc1
</s>
<s>
Když	když	k8xS
usoudil	usoudit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
vydělal	vydělat	k5eAaPmAgInS
dostatek	dostatek	k1gInSc1
peněz	peníze	k1gInPc2
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
studovat	studovat	k5eAaImF
historii	historie	k1gFnSc4
starého	starý	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohl	moct	k5eAaImAgInS
odjet	odjet	k5eAaPmF
pátrat	pátrat	k5eAaImF
po	po	k7c4
bájné	bájný	k2eAgFnPc4d1
a	a	k8xC
jím	on	k3xPp3gNnSc7
tolik	tolik	k6eAd1
obdivované	obdivovaný	k2eAgFnSc6d1
Tróji	Trója	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodnutí	rozhodnutí	k1gNnSc1
úspěšného	úspěšný	k2eAgMnSc2d1
obchodníka	obchodník	k1gMnSc2
nikdo	nikdo	k3yNnSc1
nechápal	chápat	k5eNaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
on	on	k3xPp3gMnSc1
na	na	k7c6
něm	on	k3xPp3gInSc6
trval	trvat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navštívil	navštívit	k5eAaPmAgMnS
při	při	k7c6
studiu	studio	k1gNnSc6
archeologie	archeologie	k1gFnSc2
řadu	řad	k1gInSc2
míst	místo	k1gNnPc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
i	i	k9
mimo	mimo	k7c4
Evropu	Evropa	k1gFnSc4
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Skandinávie	Skandinávie	k1gFnSc1
<g/>
,	,	kIx,
Sýrie	Sýrie	k1gFnSc1
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydal	vydat	k5eAaPmAgMnS
také	také	k9
knihu	kniha	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
popisoval	popisovat	k5eAaImAgMnS
<g/>
,	,	kIx,
kde	kde	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
Trója	Trója	k1gFnSc1
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
měla	mít	k5eAaImAgFnS
nacházet	nacházet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
podle	podle	k7c2
Homérova	Homérův	k2eAgNnSc2d1
vyprávění	vyprávění	k1gNnSc2
nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
polohu	poloha	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
kdysi	kdysi	k6eAd1
mělo	mít	k5eAaImAgNnS
město	město	k1gNnSc1
stát	stát	k5eAaPmF,k5eAaImF
a	a	k8xC
na	na	k7c6
kopci	kopec	k1gInSc6
Hisarlik	Hisarlika	k1gFnPc2
v	v	k7c6
říjnu	říjen	k1gInSc6
1871	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
výkopy	výkop	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožnily	umožnit	k5eAaPmAgFnP
jeho	jeho	k3xOp3gFnPc1
známosti	známost	k1gFnPc1
s	s	k7c7
diplomaty	diplomat	k1gMnPc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
sultánovi	sultán	k1gMnSc3
(	(	kIx(
<g/>
kterému	který	k3yIgMnSc3,k3yQgMnSc3,k3yRgMnSc3
půda	půda	k1gFnSc1
patřila	patřit	k5eAaImAgFnS
<g/>
)	)	kIx)
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnSc3
archeologické	archeologický	k2eAgInPc1d1
záměry	záměr	k1gInPc1
vůbec	vůbec	k9
nelíbily	líbit	k5eNaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tróju	Trója	k1gFnSc4
skutečně	skutečně	k6eAd1
objevil	objevit	k5eAaPmAgMnS
a	a	k8xC
po	po	k7c6
hledání	hledání	k1gNnSc6
v	v	k7c6
Mykénách	Mykény	k1gFnPc6
se	se	k3xPyFc4
k	k	k7c3
ní	on	k3xPp3gFnSc3
ještě	ještě	k6eAd1
vrátil	vrátit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
vykopávkách	vykopávka	k1gFnPc6
v	v	k7c6
Mykénách	Mykény	k1gFnPc6
se	se	k3xPyFc4
řídil	řídit	k5eAaImAgMnS
díly	díl	k1gInPc4
dramatiků	dramatik	k1gMnPc2
Aischyla	Aischyla	k1gFnSc2
a	a	k8xC
Euripida	Euripid	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nalezl	naleznout	k5eAaPmAgMnS,k5eAaBmAgMnS
například	například	k6eAd1
několik	několik	k4yIc4
hrobů	hrob	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
byli	být	k5eAaImAgMnP
mrtví	mrtvý	k1gMnPc1
obloženi	obložen	k2eAgMnPc1d1
zlatem	zlato	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
považoval	považovat	k5eAaImAgMnS
nálezy	nález	k1gInPc4
mykénské	mykénský	k2eAgFnPc1d1
kultury	kultura	k1gFnSc2
za	za	k7c4
nálezy	nález	k1gInPc4
kultury	kultura	k1gFnSc2
milétské	milétský	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
ho	on	k3xPp3gMnSc4
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
<g/>
,	,	kIx,
pro	pro	k7c4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
chudý	chudý	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
a	a	k8xC
ze	z	k7c2
strachu	strach	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
léčbu	léčba	k1gFnSc4
nezaplatí	zaplatit	k5eNaPmIp3nS
<g/>
,	,	kIx,
odmítli	odmítnout	k5eAaPmAgMnP
ošetřit	ošetřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
pak	pak	k6eAd1
přišlo	přijít	k5eAaPmAgNnS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
pochován	pochován	k2eAgMnSc1d1
v	v	k7c6
Athénách	Athéna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Byl	být	k5eAaImAgMnS
velmi	velmi	k6eAd1
nadaný	nadaný	k2eAgMnSc1d1
na	na	k7c4
jazyky	jazyk	k1gInPc4
a	a	k8xC
měl	mít	k5eAaImAgInS
vynikající	vynikající	k2eAgFnSc4d1
paměť	paměť	k1gFnSc4
<g/>
:	:	kIx,
Dorozuměl	dorozumět	k5eAaPmAgMnS
se	se	k3xPyFc4
až	až	k9
v	v	k7c6
13	#num#	k4
jazycích	jazyk	k1gInPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
nizozemsky	nizozemsky	k6eAd1
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
<g/>
,	,	kIx,
španělsky	španělsky	k6eAd1
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
<g/>
,	,	kIx,
portugalsky	portugalsky	k6eAd1
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
<g/>
,	,	kIx,
starou	starý	k2eAgFnSc7d1
a	a	k8xC
moderní	moderní	k2eAgFnSc7d1
řečtinou	řečtina	k1gFnSc7
<g/>
...	...	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učil	učít	k5eAaPmAgMnS,k5eAaImAgMnS
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
25	#num#	k4
během	během	k7c2
celého	celý	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
talent	talent	k1gInSc1
vždy	vždy	k6eAd1
zapíral	zapírat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
učení	učení	k1gNnSc4
si	se	k3xPyFc3
vytvořil	vytvořit	k5eAaPmAgMnS
vlastní	vlastní	k2eAgFnSc4d1
metodu	metoda	k1gFnSc4
<g/>
,	,	kIx,
díky	díky	k7c3
které	který	k3yRgFnSc3,k3yQgFnSc3,k3yIgFnSc3
se	se	k3xPyFc4
zvládl	zvládnout	k5eAaPmAgInS
naučit	naučit	k5eAaPmF
jazyk	jazyk	k1gInSc4
za	za	k7c4
6	#num#	k4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Během	během	k7c2
svých	svůj	k3xOyFgNnPc2
studií	studio	k1gNnPc2
ruštiny	ruština	k1gFnSc2
dostal	dostat	k5eAaPmAgInS
Schliemann	Schliemann	k1gInSc1
dvakrát	dvakrát	k6eAd1
výpověď	výpověď	k1gFnSc4
z	z	k7c2
bytu	byt	k1gInSc2
pro	pro	k7c4
rušení	rušení	k1gNnSc4
sousedů	soused	k1gMnPc2
řečmi	řeč	k1gFnPc7
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
nikdo	nikdo	k3yNnSc1
nerozumí	rozumět	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Své	svůj	k3xOyFgInPc1
deníky	deník	k1gInPc1
na	na	k7c6
cestách	cesta	k1gFnPc6
psal	psát	k5eAaImAgMnS
různými	různý	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
podle	podle	k7c2
jazyku	jazyk	k1gInSc3
země	zem	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
zrovna	zrovna	k6eAd1
nacházel	nacházet	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejspíš	nejspíš	k9
byl	být	k5eAaImAgInS
dítětem	dítě	k1gNnSc7
štěstěny	štěstěna	k1gFnSc2
<g/>
:	:	kIx,
Když	když	k8xS
u	u	k7c2
holandských	holandský	k2eAgInPc2d1
břehů	břeh	k1gInPc2
ztroskotala	ztroskotat	k5eAaPmAgFnS
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
jedině	jedině	k6eAd1
jeho	jeho	k3xOp3gMnSc3
zavazadlo	zavazadlo	k1gNnSc1
doplavalo	doplavat	k5eAaPmAgNnS
téměř	téměř	k6eAd1
neporušené	porušený	k2eNgNnSc1d1
na	na	k7c4
pláž	pláž	k1gFnSc4
<g/>
;	;	kIx,
jindy	jindy	k6eAd1
požár	požár	k1gInSc1
zničil	zničit	k5eAaPmAgInS
celé	celý	k2eAgNnSc4d1
dřevěné	dřevěný	k2eAgNnSc4d1
město	město	k1gNnSc4
krom	krom	k7c2
pár	pár	k4xCyI
domů	dům	k1gInPc2
kamenného	kamenný	k2eAgInSc2d1
hotelu	hotel	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Schliemann	Schliemann	k1gMnSc1
nocoval	nocovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
jel	jet	k5eAaImAgMnS
do	do	k7c2
Ameriky	Amerika	k1gFnSc2
pátrat	pátrat	k5eAaImF
po	po	k7c6
nezvěstném	zvěstný	k2eNgInSc6d1
bratrovi	bratr	k1gMnSc3
a	a	k8xC
dozvěděl	dozvědět	k5eAaPmAgInS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
již	již	k6eAd1
mrtev	mrtev	k2eAgMnSc1d1
<g/>
,	,	kIx,
nedalo	dát	k5eNaPmAgNnS
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
a	a	k8xC
přítomnosti	přítomnost	k1gFnPc1
na	na	k7c6
kontinentu	kontinent	k1gInSc6
využil	využít	k5eAaPmAgMnS
k	k	k7c3
návštěvě	návštěva	k1gFnSc3
amerického	americký	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
krásou	krása	k1gFnSc7
neoplývající	oplývající	k2eNgFnSc7d1
Kateřinou	Kateřina	k1gFnSc7
Petrovnou	Petrovný	k2eAgFnSc4d1
Lyšinovou	Lyšinová	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
neúspěch	neúspěch	k1gInSc1
v	v	k7c6
osobním	osobní	k2eAgInSc6d1
životě	život	k1gInSc6
kompenzoval	kompenzovat	k5eAaBmAgMnS
úspěchy	úspěch	k1gInPc4
v	v	k7c6
obchodě	obchod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc7
poslední	poslední	k2eAgFnSc7d1
manželkou	manželka	k1gFnSc7
však	však	k9
byla	být	k5eAaImAgNnP
naopak	naopak	k6eAd1
velmi	velmi	k6eAd1
krásná	krásný	k2eAgFnSc1d1
<g/>
,	,	kIx,
vzdělaná	vzdělaný	k2eAgFnSc1d1
a	a	k8xC
inteligentní	inteligentní	k2eAgFnSc1d1
Řekyně	Řekyně	k1gFnSc1
Sophia	Sophia	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zúčastnila	zúčastnit	k5eAaPmAgFnS
i	i	k9
jeho	jeho	k3xOp3gFnPc2
vykopávek	vykopávka	k1gFnPc2
nejen	nejen	k6eAd1
v	v	k7c6
Tróji	Trója	k1gFnSc6
</s>
<s>
Měl	mít	k5eAaImAgInS
talent	talent	k1gInSc1
a	a	k8xC
možná	možná	k9
jistou	jistý	k2eAgFnSc4d1
chladnokrevnou	chladnokrevný	k2eAgFnSc4d1
schopnost	schopnost	k1gFnSc4
využít	využít	k5eAaPmF
ku	k	k7c3
svému	svůj	k3xOyFgInSc3
prospěchu	prospěch	k1gInSc3
všechno	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
namanulo	namanout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
při	při	k7c6
požáru	požár	k1gInSc6
nakoupil	nakoupit	k5eAaPmAgMnS
suroviny	surovina	k1gFnPc4
<g/>
,	,	kIx,
po	po	k7c6
kterých	který	k3yQgMnPc6,k3yRgMnPc6,k3yIgMnPc6
byla	být	k5eAaImAgFnS
druhý	druhý	k4xOgInSc4
den	den	k1gInSc4
maximální	maximální	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
otázka	otázka	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
státní	státní	k2eAgFnSc2d1
příslušnosti	příslušnost	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
byl	být	k5eAaImAgMnS
poddaným	poddaný	k1gMnSc7
ruského	ruský	k2eAgMnSc2d1
cara	car	k1gMnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
občanem	občan	k1gMnSc7
carského	carský	k2eAgNnSc2d1
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
zároveň	zároveň	k6eAd1
byl	být	k5eAaImAgInS
i	i	k9
právoplatným	právoplatný	k2eAgMnSc7d1
občanem	občan	k1gMnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
a	a	k8xC
ke	k	k7c3
všemu	všecek	k3xTgNnSc3
i	i	k9
Němec	Němec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ceram	Ceram	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
35	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
<g/>
↑	↑	k?
Ceram	Ceram	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
38	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
C.	C.	kA
W.	W.	kA
Ceram	Ceram	k1gInSc1
<g/>
:	:	kIx,
Bohové	bůh	k1gMnPc1
<g/>
,	,	kIx,
hroby	hrob	k1gInPc1
a	a	k8xC
učenci	učenec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Román	román	k1gInSc1
o	o	k7c4
archeologii	archeologie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
4	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VYKOUPIL	vykoupit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecce	Ecc	k1gInSc2
homo	homo	k6eAd1
:	:	kIx,
z	z	k7c2
rozhlasových	rozhlasový	k2eAgInPc2d1
fejetonů	fejeton	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Julius	Julius	k1gMnSc1
Zirkus	Zirkus	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
312	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
903377	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Heinrich	Heinrich	k1gMnSc1
Schliemann	Schliemann	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Heinrich	Heinrich	k1gMnSc1
Schliemann	Schliemann	k1gMnSc1
</s>
<s>
SCHWICHTENBERG	SCHWICHTENBERG	kA
<g/>
,	,	kIx,
Holly	Holl	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heinrich	Heinrich	k1gMnSc1
Schliemann	Schliemann	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minnesota	Minnesota	k1gFnSc1
State	status	k1gInSc5
University	universita	k1gFnPc1
<g/>
,	,	kIx,
Mankato	Mankat	k2eAgNnSc1d1
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
EMuseum	EMuseum	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990007630	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118608215	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2122	#num#	k4
2321	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50001384	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500065958	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
17229333	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50001384	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Historie	historie	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
