<p>
<s>
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1944	[number]	k4	1944
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
sci-fi	scii	k1gFnSc2	sci-fi
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
počítačový	počítačový	k2eAgMnSc1d1	počítačový
technik	technik	k1gMnSc1	technik
<g/>
,	,	kIx,	,
plastikový	plastikový	k2eAgMnSc1d1	plastikový
modelář	modelář	k1gMnSc1	modelář
a	a	k8xC	a
jachtař	jachtař	k1gMnSc1	jachtař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
získal	získat	k5eAaPmAgInS	získat
středoškolské	středoškolský	k2eAgNnSc4d1	středoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
na	na	k7c6	na
dnešním	dnešní	k2eAgNnSc6d1	dnešní
gymnáziu	gymnázium	k1gNnSc6	gymnázium
Na	na	k7c6	na
Vídeňce	Vídeňka	k1gFnSc6	Vídeňka
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
obor	obor	k1gInSc1	obor
technická	technický	k2eAgFnSc1d1	technická
kybernetika	kybernetika	k1gFnSc1	kybernetika
na	na	k7c6	na
elektrotechnické	elektrotechnický	k2eAgFnSc6d1	elektrotechnická
fakultě	fakulta	k1gFnSc6	fakulta
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
technik	technik	k1gMnSc1	technik
komplexních	komplexní	k2eAgFnPc2d1	komplexní
zkoušek	zkouška	k1gFnPc2	zkouška
do	do	k7c2	do
národního	národní	k2eAgInSc2d1	národní
podniku	podnik	k1gInSc2	podnik
Chemont	Chemonta	k1gFnPc2	Chemonta
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
štace	štace	k1gFnPc4	štace
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k9	i
blok	blok	k1gInSc4	blok
A1	A1	k1gMnPc3	A1
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
Jaslovských	Jaslovský	k2eAgFnPc6d1	Jaslovský
Bohunicích	Bohunice	k1gFnPc6	Bohunice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prověrkách	prověrka	k1gFnPc6	prověrka
technicko-hospodářských	technickoospodářský	k2eAgMnPc2d1	technicko-hospodářský
pracovníků	pracovník	k1gMnPc2	pracovník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
nesouhlasil	souhlasit	k5eNaImAgInS	souhlasit
s	s	k7c7	s
okupací	okupace	k1gFnSc7	okupace
Československa	Československo	k1gNnSc2	Československo
Sovětskou	sovětský	k2eAgFnSc7d1	sovětská
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1971	[number]	k4	1971
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
bez	bez	k7c2	bez
práce	práce	k1gFnSc2	práce
získal	získat	k5eAaPmAgMnS	získat
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
u	u	k7c2	u
Kancelářských	kancelářský	k2eAgInPc2d1	kancelářský
strojů	stroj	k1gInPc2	stroj
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
detašovaném	detašovaný	k2eAgNnSc6d1	detašované
pracovišti	pracoviště	k1gNnSc6	pracoviště
v	v	k7c6	v
Adamovských	Adamovských	k2eAgFnPc6d1	Adamovských
strojírnách	strojírna	k1gFnPc6	strojírna
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
24	[number]	k4	24
let	léto	k1gNnPc2	léto
dojížděl	dojíždět	k5eAaImAgInS	dojíždět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
řadového	řadový	k2eAgMnSc2d1	řadový
technika	technik	k1gMnSc2	technik
sálového	sálový	k2eAgInSc2d1	sálový
počítače	počítač	k1gInSc2	počítač
ZPA	ZPA	kA	ZPA
600	[number]	k4	600
se	se	k3xPyFc4	se
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
až	až	k9	až
na	na	k7c4	na
vedoucího	vedoucí	k1gMnSc4	vedoucí
střediska	středisko	k1gNnSc2	středisko
technické	technický	k2eAgFnSc2d1	technická
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
volnou	volný	k2eAgFnSc4d1	volná
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Napůl	napůl	k6eAd1	napůl
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
psaním	psaní	k1gNnSc7	psaní
<g/>
,	,	kIx,	,
napůl	napůl	k6eAd1	napůl
jako	jako	k9	jako
profesionální	profesionální	k2eAgMnPc1d1	profesionální
kapitán	kapitán	k1gMnSc1	kapitán
námořních	námořní	k2eAgFnPc2d1	námořní
plachetnic	plachetnice	k1gFnPc2	plachetnice
<g/>
.	.	kIx.	.
</s>
<s>
Napřed	napřed	k6eAd1	napřed
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
katamaranu	katamaran	k1gInSc6	katamaran
vydavatele	vydavatel	k1gMnSc2	vydavatel
Annonce	Annonec	k1gMnSc2	Annonec
Josefa	Josef	k1gMnSc2	Josef
Kudláčka	Kudláček	k1gMnSc2	Kudláček
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
na	na	k7c6	na
dvoustěžňové	dvoustěžňový	k2eAgFnSc6d1	dvoustěžňová
jachtě	jachta	k1gFnSc6	jachta
Freelord	Freelorda	k1gFnPc2	Freelorda
patřící	patřící	k2eAgFnSc3d1	patřící
česko-japonské	českoaponský	k2eAgFnSc3d1	česko-japonská
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
také	také	k9	také
s	s	k7c7	s
agenturou	agentura	k1gFnSc7	agentura
Moravyacht	Moravyachta	k1gFnPc2	Moravyachta
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
a	a	k8xC	a
1997	[number]	k4	1997
přepravil	přepravit	k5eAaPmAgInS	přepravit
jachtu	jachta	k1gFnSc4	jachta
z	z	k7c2	z
Floridy	Florida	k1gFnSc2	Florida
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
do	do	k7c2	do
Gibraltaru	Gibraltar	k1gInSc2	Gibraltar
a	a	k8xC	a
po	po	k7c4	po
druhé	druhý	k4xOgFnPc4	druhý
na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
Freelord	Freelord	k1gMnSc1	Freelord
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
a	a	k8xC	a
s	s	k7c7	s
posádkou	posádka	k1gFnSc7	posádka
studentů	student	k1gMnPc2	student
stal	stát	k5eAaPmAgMnS	stát
vítězem	vítěz	k1gMnSc7	vítěz
Jachetního	jachetní	k2eAgNnSc2d1	jachetní
rallye	rallye	k1gNnSc2	rallye
východního	východní	k2eAgNnSc2d1	východní
Středomoří	středomoří	k1gNnSc2	středomoří
(	(	kIx(	(
<g/>
E.	E.	kA	E.
<g/>
M.	M.	kA	M.
<g/>
Y.	Y.	kA	Y.
<g/>
R.	R.	kA	R.
<g/>
)	)	kIx)	)
pořádaného	pořádaný	k2eAgInSc2d1	pořádaný
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
,	,	kIx,	,
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
Egyptem	Egypt	k1gInSc7	Egypt
jako	jako	k8xC	jako
pokus	pokus	k1gInSc4	pokus
zlepšit	zlepšit	k5eAaPmF	zlepšit
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
vztahy	vztah	k1gInPc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Trasa	trasa	k1gFnSc1	trasa
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
Istanbulu	Istanbul	k1gInSc2	Istanbul
přes	přes	k7c4	přes
Kypr	Kypr	k1gInSc4	Kypr
a	a	k8xC	a
Izrael	Izrael	k1gInSc4	Izrael
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
v	v	k7c6	v
Ismailii	Ismailie	k1gFnSc6	Ismailie
uprostřed	uprostřed	k7c2	uprostřed
Suezského	suezský	k2eAgInSc2d1	suezský
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
jachta	jachta	k1gFnSc1	jachta
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
do	do	k7c2	do
Hurghady	Hurghada	k1gFnSc2	Hurghada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
strávil	strávit	k5eAaPmAgMnS	strávit
s	s	k7c7	s
lodí	loď	k1gFnSc7	loď
potápěčskou	potápěčský	k2eAgFnSc4d1	potápěčská
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
práci	práce	k1gFnSc3	práce
František	František	k1gMnSc1	František
využil	využít	k5eAaPmAgMnS	využít
své	svůj	k3xOyFgFnPc4	svůj
jachtařské	jachtařský	k2eAgFnPc4d1	jachtařská
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
brněnského	brněnský	k2eAgInSc2d1	brněnský
Jachtklubu	Jachtklub	k1gInSc2	Jachtklub
vyrostl	vyrůst	k5eAaPmAgInS	vyrůst
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Řídit	řídit	k5eAaImF	řídit
plachetnici	plachetnice	k1gFnSc4	plachetnice
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
v	v	k7c6	v
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
od	od	k7c2	od
patnácti	patnáct	k4xCc2	patnáct
pak	pak	k6eAd1	pak
začal	začít	k5eAaPmAgInS	začít
závodit	závodit	k5eAaImF	závodit
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
Pirát	pirát	k1gMnSc1	pirát
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
získal	získat	k5eAaPmAgInS	získat
mistrovský	mistrovský	k2eAgInSc1d1	mistrovský
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
přitáhlo	přitáhnout	k5eAaPmAgNnS	přitáhnout
moře	moře	k1gNnSc1	moře
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dohody	dohoda	k1gFnSc2	dohoda
ČSTV	ČSTV	kA	ČSTV
s	s	k7c7	s
polským	polský	k2eAgInSc7d1	polský
Jachtařským	jachtařský	k2eAgInSc7d1	jachtařský
svazem	svaz	k1gInSc7	svaz
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
polského	polský	k2eAgInSc2d1	polský
škuneru	škuner	k1gInSc2	škuner
Zew	Zew	k1gMnSc4	Zew
Morza	Morz	k1gMnSc4	Morz
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
námořního	námořní	k2eAgMnSc2d1	námořní
jachtaře	jachtař	k1gMnSc2	jachtař
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
vykonal	vykonat	k5eAaPmAgMnS	vykonat
na	na	k7c4	na
malých	malý	k2eAgInPc2d1	malý
<g/>
,	,	kIx,	,
amatérsky	amatérsky	k6eAd1	amatérsky
postavených	postavený	k2eAgFnPc6d1	postavená
námořních	námořní	k2eAgFnPc6d1	námořní
plachetnicích	plachetnice	k1gFnPc6	plachetnice
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
plavby	plavba	k1gFnSc2	plavba
v	v	k7c6	v
Dánských	dánský	k2eAgFnPc6d1	dánská
úžinách	úžina	k1gFnPc6	úžina
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
Oslo	Oslo	k1gNnSc2	Oslo
a	a	k8xC	a
na	na	k7c6	na
Černém	černý	k2eAgNnSc6d1	černé
a	a	k8xC	a
Egejském	egejský	k2eAgNnSc6d1	Egejské
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
na	na	k7c6	na
Jadranu	Jadran	k1gInSc6	Jadran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
však	však	k9	však
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
soustředit	soustředit	k5eAaPmF	soustředit
se	se	k3xPyFc4	se
jen	jen	k9	jen
na	na	k7c4	na
psaní	psaní	k1gNnSc4	psaní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
také	také	k9	také
k	k	k7c3	k
překládání	překládání	k1gNnSc3	překládání
militarií	militarie	k1gFnPc2	militarie
a	a	k8xC	a
námořních	námořní	k2eAgFnPc2d1	námořní
encyklopedií	encyklopedie	k1gFnPc2	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
mu	on	k3xPp3gMnSc3	on
do	do	k7c2	do
rozpočtu	rozpočet	k1gInSc2	rozpočet
přispívá	přispívat	k5eAaImIp3nS	přispívat
penze	penze	k1gFnSc1	penze
a	a	k8xC	a
na	na	k7c4	na
moře	moře	k1gNnSc4	moře
vyplouvá	vyplouvat	k5eAaImIp3nS	vyplouvat
jen	jen	k9	jen
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgNnSc4d1	vlastní
potěšení	potěšení	k1gNnSc4	potěšení
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
a	a	k8xC	a
po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
svých	svůj	k3xOyFgMnPc2	svůj
oblíbených	oblíbený	k2eAgMnPc2d1	oblíbený
autorů	autor	k1gMnPc2	autor
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Československý	československý	k2eAgInSc1d1	československý
fandom	fandom	k1gInSc1	fandom
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
fandomu	fandom	k1gInSc2	fandom
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
SF	SF	kA	SF
klubů	klub	k1gInPc2	klub
Nyx	Nyx	k1gFnSc2	Nyx
<g/>
,	,	kIx,	,
Nonparcorán	Nonparcorán	k1gInSc4	Nonparcorán
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
na	na	k7c6	na
Draconech	Dracon	k1gInPc6	Dracon
a	a	k8xC	a
zúčastňuje	zúčastňovat	k5eAaImIp3nS	zúčastňovat
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
Parconů	Parcon	k1gInPc2	Parcon
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
účastník	účastník	k1gMnSc1	účastník
i	i	k9	i
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
populárních	populární	k2eAgFnPc2d1	populární
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgFnSc4d1	literární
činnost	činnost	k1gFnSc4	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
spisovatelům	spisovatel	k1gMnPc3	spisovatel
sci-fi	scii	k1gFnSc2	sci-fi
z	z	k7c2	z
okruhu	okruh	k1gInSc2	okruh
kolem	kolem	k7c2	kolem
Ceny	cena	k1gFnSc2	cena
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
literární	literární	k2eAgFnSc2d1	literární
soutěže	soutěž	k1gFnSc2	soutěž
pořádané	pořádaný	k2eAgFnPc4d1	pořádaná
čerstvě	čerstvě	k6eAd1	čerstvě
založeným	založený	k2eAgInSc7d1	založený
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
fandomem	fandom	k1gMnSc7	fandom
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
s	s	k7c7	s
povídkou	povídka	k1gFnSc7	povídka
"	"	kIx"	"
<g/>
Přednáška	přednáška	k1gFnSc1	přednáška
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaPmNgFnS	napsat
jednou	jednou	k6eAd1	jednou
větou	věta	k1gFnSc7	věta
a	a	k8xC	a
pojednávala	pojednávat	k5eAaImAgFnS	pojednávat
o	o	k7c6	o
hnutí	hnutí	k1gNnSc6	hnutí
"	"	kIx"	"
<g/>
Tří	tři	k4xCgInPc2	tři
bradatých	bradatý	k2eAgInPc2d1	bradatý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
byli	být	k5eAaImAgMnP	být
míněni	míněn	k2eAgMnPc1d1	míněn
Marx	Marx	k1gMnSc1	Marx
<g/>
,	,	kIx,	,
Engels	Engels	k1gMnSc1	Engels
a	a	k8xC	a
Lenin	Lenin	k1gMnSc1	Lenin
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
obsahu	obsah	k1gInSc3	obsah
nedostala	dostat	k5eNaPmAgFnS	dostat
povídka	povídka	k1gFnSc1	povídka
žádnou	žádný	k3yNgFnSc4	žádný
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
autor	autor	k1gMnSc1	autor
vešel	vejít	k5eAaPmAgMnS	vejít
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
hodnotitelů	hodnotitel	k1gMnPc2	hodnotitel
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
získal	získat	k5eAaPmAgInS	získat
za	za	k7c4	za
poému	poéma	k1gFnSc4	poéma
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Madoně	Madona	k1gFnSc6	Madona
z	z	k7c2	z
Vrakoviště	vrakoviště	k1gNnSc2	vrakoviště
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
svého	svůj	k3xOyFgMnSc2	svůj
prvního	první	k4xOgMnSc2	první
Mloka	mlok	k1gMnSc2	mlok
<g/>
.	.	kIx.	.
</s>
<s>
Zaměření	zaměření	k1gNnSc1	zaměření
dalších	další	k2eAgFnPc2d1	další
povídek	povídka	k1gFnPc2	povídka
i	i	k8xC	i
větších	veliký	k2eAgFnPc2d2	veliký
prací	práce	k1gFnPc2	práce
zpočátku	zpočátku	k6eAd1	zpočátku
ovlivňovalo	ovlivňovat	k5eAaImAgNnS	ovlivňovat
technické	technický	k2eAgNnSc1d1	technické
vzdělání	vzdělání	k1gNnSc1	vzdělání
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
dvorním	dvorní	k2eAgNnSc7d1	dvorní
tématem	téma	k1gNnSc7	téma
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
roboti	robot	k1gMnPc1	robot
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
společenské	společenský	k2eAgNnSc1d1	společenské
uvědomování	uvědomování	k1gNnSc1	uvědomování
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
a	a	k8xC	a
úvahy	úvaha	k1gFnPc4	úvaha
psal	psát	k5eAaImAgMnS	psát
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Ikarie	Ikarie	k1gFnSc2	Ikarie
<g/>
,	,	kIx,	,
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
antologií	antologie	k1gFnPc2	antologie
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
Interkomu	Interkom	k1gInSc2	Interkom
<g/>
.	.	kIx.	.
</s>
<s>
Knižního	knižní	k2eAgInSc2d1	knižní
debutu	debut	k1gInSc2	debut
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgMnS	dočkat
v	v	k7c6	v
antologii	antologie	k1gFnSc6	antologie
Návrat	návrat	k1gInSc1	návrat
na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
Zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyšla	vyjít	k5eAaPmAgFnS	vyjít
povídka	povídka	k1gFnSc1	povídka
"	"	kIx"	"
<g/>
Pojďme	jít	k5eAaImRp1nP	jít
si	se	k3xPyFc3	se
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
hoňku	hoňka	k1gFnSc4	hoňka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
mu	on	k3xPp3gMnSc3	on
pak	pak	k6eAd1	pak
vyšla	vyjít	k5eAaPmAgFnS	vyjít
první	první	k4xOgFnSc1	první
samostatná	samostatný	k2eAgFnSc1d1	samostatná
knížka	knížka	k1gFnSc1	knížka
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
Nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
přistání	přistání	k1gNnSc4	přistání
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
cenzurou	cenzura	k1gFnSc7	cenzura
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
spisovatel	spisovatel	k1gMnSc1	spisovatel
o	o	k7c4	o
třetinu	třetina	k1gFnSc4	třetina
zkrácená	zkrácený	k2eAgFnSc1d1	zkrácená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
příval	příval	k1gInSc4	příval
fantasy	fantas	k1gInPc7	fantas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
československých	československý	k2eAgNnPc6d1	Československé
knihkupectvích	knihkupectví	k1gNnPc6	knihkupectví
objevil	objevit	k5eAaPmAgInS	objevit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
zareagoval	zareagovat	k5eAaPmAgInS	zareagovat
parodicky	parodicky	k6eAd1	parodicky
míněným	míněný	k2eAgInSc7d1	míněný
románem	román	k1gInSc7	román
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
den	den	k1gInSc1	den
Valhaly	Valhala	k1gFnSc2	Valhala
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vydal	vydat	k5eAaPmAgInS	vydat
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Frank	Frank	k1gMnSc1	Frank
N.	N.	kA	N.
Skipper	Skipper	k1gMnSc1	Skipper
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
téma	téma	k1gNnSc1	téma
ho	on	k3xPp3gInSc2	on
pohltilo	pohltit	k5eAaPmAgNnS	pohltit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
psaní	psaní	k1gNnSc2	psaní
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
zcela	zcela	k6eAd1	zcela
vážně	vážně	k6eAd1	vážně
míněných	míněný	k2eAgInPc2d1	míněný
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
během	během	k7c2	během
15	[number]	k4	15
let	léto	k1gNnPc2	léto
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
šestidílný	šestidílný	k2eAgInSc1d1	šestidílný
románový	románový	k2eAgInSc1d1	románový
cyklus	cyklus	k1gInSc1	cyklus
Valhala	Valhala	k1gFnSc1	Valhala
<g/>
,	,	kIx,	,
nejrozsáhlejší	rozsáhlý	k2eAgNnSc1d3	nejrozsáhlejší
české	český	k2eAgNnSc1d1	české
dílo	dílo	k1gNnSc1	dílo
pojednávající	pojednávající	k2eAgNnSc1d1	pojednávající
o	o	k7c6	o
alternativním	alternativní	k2eAgInSc6d1	alternativní
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
odskočil	odskočit	k5eAaPmAgMnS	odskočit
k	k	k7c3	k
literatuře	literatura	k1gFnSc3	literatura
faktu	fakt	k1gInSc2	fakt
a	a	k8xC	a
k	k	k7c3	k
beletrii	beletrie	k1gFnSc3	beletrie
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
románem	román	k1gInSc7	román
Prsten	prsten	k1gInSc1	prsten
od	od	k7c2	od
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bizarní	bizarní	k2eAgInSc4d1	bizarní
příběh	příběh	k1gInSc4	příběh
bývalého	bývalý	k2eAgMnSc2d1	bývalý
estébáka	estébáka	k?	estébáka
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
si	se	k3xPyFc3	se
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přitáhne	přitáhnout	k5eAaPmIp3nS	přitáhnout
magický	magický	k2eAgInSc4d1	magický
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
dostala	dostat	k5eAaPmAgFnS	dostat
Božena	Božena	k1gFnSc1	Božena
Němcová	Němcová	k1gFnSc1	Němcová
od	od	k7c2	od
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Zaháňské	zaháňský	k2eAgFnSc2d1	Zaháňská
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
zahájil	zahájit	k5eAaPmAgInS	zahájit
edici	edice	k1gFnSc4	edice
Kroniky	kronika	k1gFnSc2	kronika
karmínových	karmínový	k2eAgInPc2d1	karmínový
kamenů	kámen	k1gInPc2	kámen
věnovanou	věnovaný	k2eAgFnSc4d1	věnovaná
příběhům	příběh	k1gInPc3	příběh
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgMnPc6	jenž
však	však	k9	však
nesmí	smět	k5eNaImIp3nP	smět
chybět	chybět	k5eAaImF	chybět
kousek	kousek	k1gInSc4	kousek
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2012	[number]	k4	2012
pak	pak	k6eAd1	pak
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
dopsal	dopsat	k5eAaPmAgMnS	dopsat
zatím	zatím	k6eAd1	zatím
svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
román	román	k1gInSc4	román
s	s	k7c7	s
názvem	název	k1gInSc7	název
Křižník	křižník	k1gInSc1	křižník
Thor	Thora	k1gFnPc2	Thora
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
napůl	napůl	k6eAd1	napůl
korzárský	korzárský	k2eAgInSc4d1	korzárský
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
napůl	napůl	k6eAd1	napůl
polit-fiction	politiction	k1gInSc1	polit-fiction
z	z	k7c2	z
nedaleké	daleký	k2eNgFnSc2d1	nedaleká
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povídky	povídka	k1gFnPc1	povídka
===	===	k?	===
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
další	další	k2eAgInPc1d1	další
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Madoně	Madona	k1gFnSc6	Madona
z	z	k7c2	z
Vrakoviště	vrakoviště	k1gNnSc2	vrakoviště
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
ni	on	k3xPp3gFnSc4	on
získal	získat	k5eAaPmAgInS	získat
Cenu	cena	k1gFnSc4	cena
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
</s>
</p>
<p>
<s>
Pojďme	jít	k5eAaImRp1nP	jít
si	se	k3xPyFc3	se
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
hoňku	hoňka	k1gFnSc4	hoňka
(	(	kIx(	(
<g/>
Návrat	návrat	k1gInSc1	návrat
na	na	k7c4	na
planetu	planeta	k1gFnSc4	planeta
Zemi	zem	k1gFnSc4	zem
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
René	René	k1gMnSc1	René
nosí	nosit	k5eAaImIp3nS	nosit
šaty	šat	k1gInPc4	šat
(	(	kIx(	(
<g/>
Kočas	Kočas	k1gInSc1	Kočas
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Příšpulek	Příšpulka	k1gFnPc2	Příšpulka
do	do	k7c2	do
různého	různý	k2eAgMnSc2d1	různý
(	(	kIx(	(
<g/>
Kočas	Kočas	k1gInSc1	Kočas
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dítě	dítě	k1gNnSc1	dítě
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jedelského	Jedelský	k2eAgMnSc2d1	Jedelský
(	(	kIx(	(
<g/>
Skandál	skandál	k1gInSc1	skandál
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
snů	sen	k1gInPc2	sen
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Královna	královna	k1gFnSc1	královna
Šumavy	Šumava	k1gFnSc2	Šumava
(	(	kIx(	(
<g/>
Lovci	lovec	k1gMnPc1	lovec
zlatých	zlatá	k1gFnPc2	zlatá
mloků	mlok	k1gMnPc2	mlok
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
čím	čí	k3xOyRgNnSc7	čí
jste	být	k5eAaImIp2nP	být
vy	vy	k3xPp2nPc1	vy
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
přistání	přistání	k1gNnSc4	přistání
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
pro	pro	k7c4	pro
planetu	planeta	k1gFnSc4	planeta
(	(	kIx(	(
<g/>
Nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
přistání	přistání	k1gNnSc4	přistání
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Človíčku	človíček	k1gMnSc5	človíček
<g/>
,	,	kIx,	,
chci	chtít	k5eAaImIp1nS	chtít
tě	ty	k3xPp2nSc4	ty
ještě	ještě	k9	ještě
slyšet	slyšet	k5eAaImF	slyšet
se	se	k3xPyFc4	se
smát	smát	k5eAaImF	smát
(	(	kIx(	(
<g/>
Nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
přistání	přistání	k1gNnSc4	přistání
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koně	kůň	k1gMnPc1	kůň
se	se	k3xPyFc4	se
také	také	k9	také
střílejí	střílet	k5eAaImIp3nP	střílet
(	(	kIx(	(
<g/>
Nešťastné	šťastný	k2eNgNnSc1d1	nešťastné
přistání	přistání	k1gNnSc1	přistání
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Venuše	Venuše	k1gFnSc1	Venuše
kallipygos	kallipygosa	k1gFnPc2	kallipygosa
(	(	kIx(	(
<g/>
Nešťastné	šťastný	k2eNgNnSc1d1	nešťastné
přistání	přistání	k1gNnSc1	přistání
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jsem	být	k5eAaImIp1nS	být
hodný	hodný	k2eAgInSc4d1	hodný
<g/>
,	,	kIx,	,
tichý	tichý	k2eAgInSc4d1	tichý
chlapec	chlapec	k1gMnSc1	chlapec
(	(	kIx(	(
<g/>
Nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
přistání	přistání	k1gNnSc4	přistání
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
přistání	přistání	k1gNnSc4	přistání
(	(	kIx(	(
<g/>
Nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
přistání	přistání	k1gNnSc4	přistání
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bradburyho	Bradburyze	k6eAd1	Bradburyze
stín	stín	k1gInSc1	stín
(	(	kIx(	(
<g/>
Černá	černý	k2eAgFnSc1d1	černá
skříňka	skříňka	k1gFnSc1	skříňka
<g/>
:	:	kIx,	:
Kočas	Kočas	k1gInSc1	Kočas
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jak	jak	k8xC	jak
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
pořídili	pořídit	k5eAaPmAgMnP	pořídit
auto	auto	k1gNnSc4	auto
aneb	aneb	k?	aneb
Slohová	slohový	k2eAgFnSc1d1	slohová
práce	práce	k1gFnSc1	práce
z	z	k7c2	z
alternativního	alternativní	k2eAgInSc2d1	alternativní
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Černá	černý	k2eAgFnSc1d1	černá
skříňka	skříňka	k1gFnSc1	skříňka
<g/>
:	:	kIx,	:
Kočas	Kočas	k1gInSc1	Kočas
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hvězdné	hvězdný	k2eAgFnPc1d1	hvězdná
hry	hra	k1gFnPc1	hra
(	(	kIx(	(
<g/>
Lovci	lovec	k1gMnPc7	lovec
černých	černý	k2eAgMnPc2d1	černý
mloků	mlok	k1gMnPc2	mlok
III	III	kA	III
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
dětství	dětství	k1gNnSc2	dětství
(	(	kIx(	(
<g/>
Lety	let	k1gInPc4	let
zakázanou	zakázaný	k2eAgFnSc7d1	zakázaná
rychlostí	rychlost	k1gFnSc7	rychlost
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Opus	opus	k1gInSc1	opus
dei	dei	k?	dei
(	(	kIx(	(
<g/>
Vesmírní	vesmírný	k2eAgMnPc1d1	vesmírný
diplomaté	diplomat	k1gMnPc1	diplomat
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sonda	sonda	k1gFnSc1	sonda
do	do	k7c2	do
myslí	mysl	k1gFnPc2	mysl
předků	předek	k1gMnPc2	předek
(	(	kIx(	(
<g/>
Kočas	Kočas	k1gInSc1	Kočas
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Modlitba	modlitba	k1gFnSc1	modlitba
ateisty	ateista	k1gMnSc2	ateista
(	(	kIx(	(
<g/>
Bradburyho	Bradbury	k1gMnSc2	Bradbury
stín	stín	k1gInSc1	stín
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Světadárce	Světadárka	k1gFnSc3	Světadárka
(	(	kIx(	(
<g/>
Bradburyho	Bradbury	k1gMnSc4	Bradbury
stín	stín	k1gInSc4	stín
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Záhada	záhada	k1gFnSc1	záhada
linky	linka	k1gFnSc2	linka
Paradise	Paradise	k1gFnSc2	Paradise
(	(	kIx(	(
<g/>
Bradburyho	Bradbury	k1gMnSc2	Bradbury
stín	stín	k1gInSc1	stín
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oni	onen	k3xDgMnPc1	onen
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nP	muset
vymyslet	vymyslet	k5eAaPmF	vymyslet
vždy	vždy	k6eAd1	vždy
něco	něco	k3yInSc1	něco
zvláštního	zvláštní	k2eAgMnSc2d1	zvláštní
(	(	kIx(	(
<g/>
Bradburyho	Bradbury	k1gMnSc2	Bradbury
stín	stín	k1gInSc1	stín
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ramax	Ramax	k1gInSc1	Ramax
(	(	kIx(	(
<g/>
Kočas	Kočas	k1gInSc1	Kočas
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Růže	růže	k1gFnSc1	růže
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
(	(	kIx(	(
<g/>
Ramax	Ramax	k1gInSc1	Ramax
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Andros	Androsa	k1gFnPc2	Androsa
(	(	kIx(	(
<g/>
Ramax	Ramax	k1gInSc1	Ramax
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
(	(	kIx(	(
<g/>
Let	let	k1gInSc1	let
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Askerův	Askerův	k2eAgInSc1d1	Askerův
objev	objev	k1gInSc1	objev
(	(	kIx(	(
<g/>
Ikarie	Ikarie	k1gFnSc1	Ikarie
1994	[number]	k4	1994
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
No	no	k9	no
Difference	Difference	k1gFnSc1	Difference
(	(	kIx(	(
<g/>
Nemesis	Nemesis	k1gFnSc1	Nemesis
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Břitvoruký	Břitvoruký	k2eAgMnSc1d1	Břitvoruký
Jack	Jack	k1gMnSc1	Jack
(	(	kIx(	(
<g/>
Nemesis	Nemesis	k1gFnSc1	Nemesis
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tělesné	tělesný	k2eAgInPc1d1	tělesný
předpoklady	předpoklad	k1gInPc1	předpoklad
(	(	kIx(	(
<g/>
Rigor	Rigor	k1gInSc1	Rigor
Mortis	Mortis	k1gFnSc2	Mortis
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Skládanka	skládanka	k1gFnSc1	skládanka
(	(	kIx(	(
<g/>
Ikarie	Ikarie	k1gFnSc1	Ikarie
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
a	a	k8xC	a
chrám	chrám	k1gInSc1	chrám
(	(	kIx(	(
<g/>
Je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
být	být	k5eAaImF	být
mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Knihy	kniha	k1gFnSc2	kniha
===	===	k?	===
</s>
</p>
<p>
<s>
kniha	kniha	k1gFnSc1	kniha
povídek	povídka	k1gFnPc2	povídka
Nešťastné	šťastný	k2eNgNnSc4d1	nešťastné
přistání	přistání	k1gNnSc4	přistání
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
cenzurováno	cenzurován	k2eAgNnSc1d1	cenzurováno
</s>
</p>
<p>
<s>
kniha	kniha	k1gFnSc1	kniha
povídek	povídka	k1gFnPc2	povídka
Bradburyho	Bradbury	k1gMnSc2	Bradbury
stín	stín	k1gInSc1	stín
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kniha	kniha	k1gFnSc1	kniha
povídek	povídka	k1gFnPc2	povídka
Ramax	Ramax	k1gInSc1	Ramax
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
Cena	cena	k1gFnSc1	cena
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
</s>
</p>
<p>
<s>
fantastický	fantastický	k2eAgInSc1d1	fantastický
román	román	k1gInSc1	román
Prsten	prsten	k1gInSc1	prsten
od	od	k7c2	od
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
ze	z	k7c2	z
série	série	k1gFnSc2	série
Kroniky	kronika	k1gFnSc2	kronika
karmínových	karmínový	k2eAgInPc2d1	karmínový
kamenů	kámen	k1gInPc2	kámen
</s>
</p>
<p>
<s>
román	román	k1gInSc1	román
Křižník	křižník	k1gInSc1	křižník
Thor	Thor	k1gInSc1	Thor
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Šestidílný	šestidílný	k2eAgInSc1d1	šestidílný
románový	románový	k2eAgInSc1d1	románový
cyklus	cyklus	k1gInSc1	cyklus
Valhala	Valhala	k1gFnSc1	Valhala
</s>
</p>
<p>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
den	den	k1gInSc1	den
Valhaly	Valhala	k1gFnSc2	Valhala
<g/>
:	:	kIx,	:
Meče	meč	k1gInSc2	meč
a	a	k8xC	a
prsten	prsten	k1gInSc4	prsten
</s>
</p>
<p>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
den	den	k1gInSc1	den
Valhaly	Valhala	k1gFnSc2	Valhala
<g/>
:	:	kIx,	:
<g/>
Vetřelci	vetřelec	k1gMnPc1	vetřelec
(	(	kIx(	(
<g/>
Meče	meč	k1gInPc1	meč
a	a	k8xC	a
prsten	prsten	k1gInSc1	prsten
a	a	k8xC	a
Vetřelci	vetřelec	k1gMnPc1	vetřelec
původně	původně	k6eAd1	původně
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
r.	r.	kA	r.
1994	[number]	k4	1994
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
jako	jako	k8xC	jako
jediná	jediný	k2eAgFnSc1d1	jediná
kniha	kniha	k1gFnSc1	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
den	den	k1gInSc1	den
Valhaly	Valhala	k1gFnSc2	Valhala
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
den	den	k1gInSc1	den
Valhaly	Valhala	k1gFnSc2	Valhala
<g/>
:	:	kIx,	:
Dopoledne	dopoledne	k1gNnSc1	dopoledne
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
den	den	k1gInSc1	den
Valhaly	Valhala	k1gFnSc2	Valhala
<g/>
:	:	kIx,	:
Odpoledne	odpoledne	k6eAd1	odpoledne
</s>
</p>
<p>
<s>
Konečný	konečný	k2eAgInSc4d1	konečný
den	den	k1gInSc4	den
Valhaly	Valhala	k1gFnSc2	Valhala
<g/>
:	:	kIx,	:
Chlapec	chlapec	k1gMnSc1	chlapec
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
</s>
</p>
<p>
<s>
Konečný	konečný	k2eAgInSc4d1	konečný
den	den	k1gInSc4	den
Valhaly	Valhala	k1gFnSc2	Valhala
<g/>
:	:	kIx,	:
Mezi	mezi	k7c4	mezi
světyvšech	světyvšech	k1gInSc4	světyvšech
6	[number]	k4	6
dílů	díl	k1gInPc2	díl
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
svazku	svazek	k1gInSc6	svazek
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Valhala	Valhala	k1gFnSc1	Valhala
</s>
</p>
<p>
<s>
==	==	k?	==
Publicistika	publicistika	k1gFnSc1	publicistika
==	==	k?	==
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
před	před	k7c7	před
trvalým	trvalý	k2eAgNnSc7d1	trvalé
přesídlením	přesídlení	k1gNnSc7	přesídlení
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
začal	začít	k5eAaPmAgMnS	začít
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
redakcí	redakce	k1gFnSc7	redakce
časopisu	časopis	k1gInSc2	časopis
Yacht	Yacht	k1gMnSc1	Yacht
jako	jako	k8xS	jako
externí	externí	k2eAgMnSc1d1	externí
redaktor	redaktor	k1gMnSc1	redaktor
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
spolupráce	spolupráce	k1gFnSc1	spolupráce
trvá	trvat	k5eAaImIp3nS	trvat
stále	stále	k6eAd1	stále
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
příspěvky	příspěvek	k1gInPc1	příspěvek
netvoří	tvořit	k5eNaImIp3nP	tvořit
jen	jen	k9	jen
recenze	recenze	k1gFnPc1	recenze
a	a	k8xC	a
představování	představování	k1gNnSc1	představování
nových	nový	k2eAgFnPc2d1	nová
jachet	jachta	k1gFnPc2	jachta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
články	článek	k1gInPc4	článek
o	o	k7c6	o
údržbě	údržba	k1gFnSc6	údržba
a	a	k8xC	a
výstroji	výstroj	k1gFnSc6	výstroj
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obsahu	obsah	k1gInSc3	obsah
časopisu	časopis	k1gInSc2	časopis
též	též	k9	též
přispěl	přispět	k5eAaPmAgMnS	přispět
zavedením	zavedení	k1gNnSc7	zavedení
několika	několik	k4yIc2	několik
rubrik	rubrika	k1gFnPc2	rubrika
<g/>
;	;	kIx,	;
s	s	k7c7	s
již	již	k6eAd1	již
zesnulým	zesnulý	k2eAgInSc7d1	zesnulý
Ing.	ing.	kA	ing.
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Pešlem	Pešl	k1gMnSc7	Pešl
uveřejnili	uveřejnit	k5eAaPmAgMnP	uveřejnit
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Žízeň	žízeň	k1gFnSc1	žízeň
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
sérii	série	k1gFnSc4	série
článků	článek	k1gInPc2	článek
o	o	k7c6	o
průkopnických	průkopnický	k2eAgFnPc6d1	průkopnická
plavbách	plavba	k1gFnPc6	plavba
československých	československý	k2eAgMnPc2d1	československý
námořních	námořní	k2eAgMnPc2d1	námořní
jachtařů	jachtař	k1gMnPc2	jachtař
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
obohatil	obohatit	k5eAaPmAgMnS	obohatit
časopis	časopis	k1gInSc4	časopis
Yacht	Yacht	k2eAgInSc4d1	Yacht
o	o	k7c4	o
rubriku	rubrika	k1gFnSc4	rubrika
"	"	kIx"	"
<g/>
Toulky	toulka	k1gFnSc2	toulka
námořní	námořní	k2eAgFnSc2d1	námořní
minulostí	minulost	k1gFnSc7	minulost
<g/>
"	"	kIx"	"
věnovanou	věnovaný	k2eAgFnSc4d1	věnovaná
historii	historie	k1gFnSc4	historie
jachtingu	jachting	k1gInSc2	jachting
a	a	k8xC	a
mořeplavby	mořeplavba	k1gFnSc2	mořeplavba
a	a	k8xC	a
o	o	k7c4	o
sérii	série	k1gFnSc4	série
medailónků	medailónek	k1gInPc2	medailónek
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
průkopníků	průkopník	k1gMnPc2	průkopník
československého	československý	k2eAgInSc2d1	československý
jachtingu	jachting	k1gInSc2	jachting
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
přispíval	přispívat	k5eAaImAgMnS	přispívat
do	do	k7c2	do
internetového	internetový	k2eAgInSc2d1	internetový
deníku	deník	k1gInSc2	deník
Neviditelný	viditelný	k2eNgMnSc1d1	Neviditelný
pes	pes	k1gMnSc1	pes
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Neffa	Neff	k1gMnSc2	Neff
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
úvahy	úvaha	k1gFnPc4	úvaha
a	a	k8xC	a
eseje	esej	k1gInPc4	esej
na	na	k7c4	na
všemožná	všemožný	k2eAgNnPc4d1	všemožné
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
od	od	k7c2	od
politických	politický	k2eAgMnPc2d1	politický
přes	přes	k7c4	přes
kulturní	kulturní	k2eAgNnSc4d1	kulturní
až	až	k9	až
k	k	k7c3	k
technicko-	technicko-	k?	technicko-
a	a	k8xC	a
vojensko-historickým	vojenskoistorický	k2eAgInSc7d1	vojensko-historický
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
vlastní	vlastní	k2eAgFnSc4d1	vlastní
rubriku	rubrika	k1gFnSc4	rubrika
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nazval	nazvat	k5eAaPmAgMnS	nazvat
Mrožoviny	Mrožovina	k1gFnSc2	Mrožovina
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
reflexí	reflexe	k1gFnSc7	reflexe
na	na	k7c4	na
básničku	básnička	k1gFnSc4	básnička
"	"	kIx"	"
<g/>
Mrož	mrož	k1gMnSc1	mrož
a	a	k8xC	a
tesař	tesař	k1gMnSc1	tesař
<g/>
"	"	kIx"	"
z	z	k7c2	z
knížky	knížka	k1gFnSc2	knížka
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Mrož	mrož	k1gMnSc1	mrož
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mluvme	mluvit	k5eAaImRp1nP	mluvit
o	o	k7c6	o
jiných	jiný	k2eAgFnPc6d1	jiná
věcechO	věcechO	k?	věcechO
botách	bota	k1gFnPc6	bota
<g/>
,	,	kIx,	,
lodičkách	lodička	k1gFnPc6	lodička
a	a	k8xC	a
pečetním	pečetní	k2eAgFnPc3d1	pečetní
voskuO	voskuO	k?	voskuO
lenoších	lenoch	k1gInPc6	lenoch
a	a	k8xC	a
králechA	králechA	k?	králechA
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
moře	moře	k1gNnSc4	moře
tak	tak	k6eAd1	tak
horkéA	horkéA	k?	horkéA
zda	zda	k8xS	zda
mají	mít	k5eAaImIp3nP	mít
prasata	prase	k1gNnPc1	prase
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
čtyřverší	čtyřverší	k1gNnSc1	čtyřverší
pak	pak	k6eAd1	pak
tvořilo	tvořit	k5eAaImAgNnS	tvořit
moto	moto	k1gNnSc1	moto
rubriky	rubrika	k1gFnSc2	rubrika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
přesně	přesně	k6eAd1	přesně
vystihovalo	vystihovat	k5eAaImAgNnS	vystihovat
její	její	k3xOp3gInSc4	její
rozsah	rozsah	k1gInSc4	rozsah
a	a	k8xC	a
pestrost	pestrost	k1gFnSc4	pestrost
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
Mrožovinách	Mrožovina	k1gFnPc6	Mrožovina
na	na	k7c4	na
400	[number]	k4	400
statí	stať	k1gFnPc2	stať
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
vydalo	vydat	k5eAaPmAgNnS	vydat
na	na	k7c4	na
hodně	hodně	k6eAd1	hodně
silnou	silný	k2eAgFnSc4d1	silná
knížku	knížka	k1gFnSc4	knížka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neméně	málo	k6eNd2	málo
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
byla	být	k5eAaImAgFnS	být
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
pracoval	pracovat	k5eAaImAgMnS	pracovat
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
jako	jako	k8xS	jako
externí	externí	k2eAgMnSc1d1	externí
moderátor	moderátor	k1gMnSc1	moderátor
a	a	k8xC	a
redaktor	redaktor	k1gMnSc1	redaktor
pro	pro	k7c4	pro
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
kanál	kanál	k1gInSc4	kanál
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
moderátorů	moderátor	k1gMnPc2	moderátor
pořadu	pořad	k1gInSc6	pořad
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
práci	práce	k1gFnSc3	práce
nepatřilo	patřit	k5eNaImAgNnS	patřit
jen	jen	k9	jen
pořad	pořad	k1gInSc4	pořad
odmoderovat	odmoderovat	k5eAaBmF	odmoderovat
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
hosty	host	k1gMnPc7	host
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
dramaturgem	dramaturg	k1gMnSc7	dramaturg
zvolit	zvolit	k5eAaPmF	zvolit
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
napsat	napsat	k5eAaPmF	napsat
scénář	scénář	k1gInSc4	scénář
<g/>
,	,	kIx,	,
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
natáčení	natáčení	k1gNnSc4	natáčení
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
rozhovory	rozhovor	k1gInPc1	rozhovor
s	s	k7c7	s
pamětníky	pamětník	k1gInPc7	pamětník
či	či	k8xC	či
návštěvy	návštěva	k1gFnPc4	návštěva
památných	památný	k2eAgNnPc2d1	památné
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
hradů	hrad	k1gInPc2	hrad
<g/>
,	,	kIx,	,
zámků	zámek	k1gInPc2	zámek
<g/>
,	,	kIx,	,
památníků	památník	k1gInPc2	památník
a	a	k8xC	a
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
a	a	k8xC	a
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
předtáček	předtáček	k1gInSc1	předtáček
ve	v	k7c6	v
střižně	střižna	k1gFnSc6	střižna
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
napsal	napsat	k5eAaBmAgMnS	napsat
a	a	k8xC	a
odmoderoval	odmoderovat	k5eAaImAgMnS	odmoderovat
čtyřicet	čtyřicet	k4xCc4	čtyřicet
vydání	vydání	k1gNnPc2	vydání
Historického	historický	k2eAgInSc2d1	historický
magazínu	magazín	k1gInSc2	magazín
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
dohledat	dohledat	k5eAaPmF	dohledat
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
ve	v	k7c6	v
videoarchivu	videoarchiv	k1gInSc6	videoarchiv
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c4	po
celou	celá	k1gFnSc4	celá
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
samozřejmě	samozřejmě	k6eAd1	samozřejmě
publikoval	publikovat	k5eAaBmAgMnS	publikovat
i	i	k9	i
ve	v	k7c6	v
scifistických	scifistický	k2eAgInPc6d1	scifistický
časopisech	časopis	k1gInPc6	časopis
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Magazínu	magazín	k1gInSc6	magazín
Fantasy	fantas	k1gInPc4	fantas
&	&	k?	&
Science	Science	k1gFnSc1	Science
Fiction	Fiction	k1gInSc1	Fiction
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uveřejňoval	uveřejňovat	k5eAaImAgMnS	uveřejňovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
recenze	recenze	k1gFnPc4	recenze
a	a	k8xC	a
literární	literární	k2eAgFnPc4d1	literární
analýzy	analýza	k1gFnPc4	analýza
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
románů	román	k1gInPc2	román
Roberta	Roberta	k1gFnSc1	Roberta
Holdstocka	Holdstocka	k1gFnSc1	Holdstocka
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
pak	pak	k6eAd1	pak
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2012	[number]	k4	2012
vyšel	vyjít	k5eAaPmAgInS	vyjít
seriál	seriál	k1gInSc1	seriál
"	"	kIx"	"
<g/>
Magické	magický	k2eAgInPc1d1	magický
základy	základ	k1gInPc1	základ
Hitlerovy	Hitlerův	k2eAgFnSc2d1	Hitlerova
říše	říš	k1gFnSc2	říš
<g/>
"	"	kIx"	"
o	o	k7c6	o
okultních	okultní	k2eAgInPc6d1	okultní
kořenech	kořen	k1gInPc6	kořen
nacismu	nacismus	k1gInSc2	nacismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
také	také	k9	také
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
uveřejňoval	uveřejňovat	k5eAaImAgMnS	uveřejňovat
minirecenze	minirecenze	k6eAd1	minirecenze
scifi	scifi	k1gFnSc4	scifi
knížek	knížka	k1gFnPc2	knížka
v	v	k7c6	v
Lidových	lidový	k2eAgFnPc6d1	lidová
novinách	novina	k1gFnPc6	novina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
faktu	fakt	k1gInSc2	fakt
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
sci-fi	scii	k1gFnSc2	sci-fi
se	se	k3xPyFc4	se
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
zabývá	zabývat	k5eAaImIp3nS	zabývat
literaturou	literatura	k1gFnSc7	literatura
faktu	fakt	k1gInSc2	fakt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
mu	on	k3xPp3gNnSc3	on
vyšla	vyjít	k5eAaPmAgFnS	vyjít
knížka	knížka	k1gFnSc1	knížka
"	"	kIx"	"
<g/>
Veleobři	veleobr	k1gMnPc1	veleobr
oceánů	oceán	k1gInPc2	oceán
<g/>
"	"	kIx"	"
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
bitevním	bitevní	k2eAgFnPc3d1	bitevní
lodím	loď	k1gFnPc3	loď
a	a	k8xC	a
bitevním	bitevní	k2eAgInPc3d1	bitevní
křižníkům	křižník	k1gInPc3	křižník
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejen	nejen	k6eAd1	nejen
podrobné	podrobný	k2eAgInPc4d1	podrobný
popisy	popis	k1gInPc4	popis
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
osudy	osud	k1gInPc1	osud
19	[number]	k4	19
největších	veliký	k2eAgFnPc2d3	veliký
válečných	válečný	k2eAgFnPc2d1	válečná
lodí	loď	k1gFnPc2	loď
námořních	námořní	k2eAgFnPc2d1	námořní
mocností	mocnost	k1gFnPc2	mocnost
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
následovala	následovat	k5eAaImAgFnS	následovat
knížka	knížka	k1gFnSc1	knížka
"	"	kIx"	"
<g/>
Plachty	plachta	k1gFnPc1	plachta
v	v	k7c6	v
ohni	oheň	k1gInSc6	oheň
<g/>
"	"	kIx"	"
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
předchozí	předchozí	k2eAgFnSc4d1	předchozí
epochu	epocha	k1gFnSc4	epocha
válečných	válečný	k2eAgFnPc2d1	válečná
plachetnic	plachetnice	k1gFnPc2	plachetnice
<g/>
.	.	kIx.	.
</s>
<s>
Zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
nejen	nejen	k6eAd1	nejen
vývoj	vývoj	k1gInSc4	vývoj
konstrukce	konstrukce	k1gFnSc2	konstrukce
od	od	k7c2	od
vikinských	vikinský	k2eAgFnPc2d1	vikinská
lodic	lodice	k1gFnPc2	lodice
až	až	k9	až
po	po	k7c4	po
mohutné	mohutný	k2eAgMnPc4d1	mohutný
trojpalubníky	trojpalubník	k1gMnPc4	trojpalubník
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
děly	dělo	k1gNnPc7	dělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vývoj	vývoj	k1gInSc4	vývoj
námořní	námořní	k2eAgFnSc2d1	námořní
strategie	strategie	k1gFnSc2	strategie
a	a	k8xC	a
taktiky	taktika	k1gFnSc2	taktika
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c6	na
bitvách	bitva	k1gFnPc6	bitva
mezi	mezi	k7c7	mezi
Francouzi	Francouz	k1gMnPc7	Francouz
a	a	k8xC	a
Angličany	Angličan	k1gMnPc7	Angličan
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
za	za	k7c2	za
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
literatuře	literatura	k1gFnSc3	literatura
faktu	fakt	k1gInSc2	fakt
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
jachtařská	jachtařský	k2eAgFnSc1d1	jachtařská
příručka	příručka	k1gFnSc1	příručka
"	"	kIx"	"
<g/>
Za	za	k7c7	za
kormidlem	kormidlo	k1gNnSc7	kormidlo
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
Jadranu	Jadran	k1gInSc6	Jadran
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
a	a	k8xC	a
rady	rada	k1gFnPc1	rada
začínajícím	začínající	k2eAgMnPc3d1	začínající
námořním	námořní	k2eAgMnPc3d1	námořní
jachtařům	jachtař	k1gMnPc3	jachtař
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
vlastními	vlastní	k2eAgInPc7d1	vlastní
autorovými	autorův	k2eAgInPc7d1	autorův
zážitky	zážitek	k1gInPc7	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
chystá	chystat	k5eAaImIp3nS	chystat
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
ještě	ještě	k6eAd1	ještě
pozoruhodnější	pozoruhodný	k2eAgFnSc4d2	pozoruhodnější
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
plavbách	plavba	k1gFnPc6	plavba
po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
britského	britský	k2eAgMnSc2d1	britský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Arthura	Arthur	k1gMnSc2	Arthur
Ransoma	Ransom	k1gMnSc2	Ransom
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc2	tvůrce
slavných	slavný	k2eAgFnPc2d1	slavná
Vlaštovek	vlaštovka	k1gFnPc2	vlaštovka
a	a	k8xC	a
Amazonek	Amazonka	k1gFnPc2	Amazonka
<g/>
.	.	kIx.	.
</s>
<s>
Knížka	knížka	k1gFnSc1	knížka
"	"	kIx"	"
<g/>
Za	za	k7c7	za
tajemstvím	tajemství	k1gNnSc7	tajemství
Vlaštovek	vlaštovka	k1gFnPc2	vlaštovka
a	a	k8xC	a
Amazonek	Amazonka	k1gFnPc2	Amazonka
aneb	aneb	k?	aneb
jachtařské	jachtařský	k2eAgFnSc2d1	jachtařská
expedice	expedice	k1gFnSc2	expedice
po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
Arthura	Arthur	k1gMnSc2	Arthur
Ransoma	Ransom	k1gMnSc2	Ransom
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
literárních	literární	k2eAgMnPc2d1	literární
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejen	nejen	k6eAd1	nejen
popisy	popis	k1gInPc4	popis
plaveb	plavba	k1gFnPc2	plavba
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Ransomovy	Ransomův	k2eAgInPc1d1	Ransomův
romány	román	k1gInPc1	román
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
odkrývá	odkrývat	k5eAaImIp3nS	odkrývat
tajemství	tajemství	k1gNnSc1	tajemství
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
autorovi	autor	k1gMnSc6	autor
posloužil	posloužit	k5eAaPmAgMnS	posloužit
jako	jako	k9	jako
předlohy	předloha	k1gFnPc4	předloha
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnPc4	jeho
literární	literární	k2eAgFnPc4d1	literární
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
jak	jak	k6eAd1	jak
komplikované	komplikovaný	k2eAgInPc1d1	komplikovaný
vztahy	vztah	k1gInPc1	vztah
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
měl	mít	k5eAaImAgInS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Veleobři	veleobr	k1gMnPc1	veleobr
oceánů	oceán	k1gInPc2	oceán
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Plachty	plachta	k1gFnPc1	plachta
v	v	k7c6	v
ohni	oheň	k1gInSc6	oheň
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Za	za	k7c7	za
kormidlem	kormidlo	k1gNnSc7	kormidlo
nejen	nejen	k6eAd1	nejen
na	na	k7c6	na
Jadranu	Jadran	k1gInSc6	Jadran
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Překladatelská	překladatelský	k2eAgFnSc1d1	překladatelská
činnost	činnost	k1gFnSc1	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
znalosti	znalost	k1gFnPc4	znalost
námořní	námořní	k2eAgFnSc2d1	námořní
tematiky	tematika	k1gFnSc2	tematika
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc4d1	vlastní
jachtařskou	jachtařský	k2eAgFnSc4d1	jachtařská
praxi	praxe	k1gFnSc4	praxe
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
při	při	k7c6	při
překladech	překlad	k1gInPc6	překlad
námořních	námořní	k2eAgFnPc2d1	námořní
a	a	k8xC	a
vojenských	vojenský	k2eAgFnPc2d1	vojenská
encyklopedií	encyklopedie	k1gFnPc2	encyklopedie
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
knížek	knížka	k1gFnPc2	knížka
<g/>
.	.	kIx.	.
</s>
<s>
Zredigoval	zredigovat	k5eAaPmAgMnS	zredigovat
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
přeložil	přeložit	k5eAaPmAgMnS	přeložit
třídílnou	třídílný	k2eAgFnSc4d1	třídílná
"	"	kIx"	"
<g/>
Encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
konfliktů	konflikt	k1gInPc2	konflikt
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
překladu	překlad	k1gInSc6	překlad
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
"	"	kIx"	"
<g/>
Lodě	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
5000	[number]	k4	5000
let	léto	k1gNnPc2	léto
námořních	námořní	k2eAgMnPc2d1	námořní
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
pak	pak	k6eAd1	pak
přeložil	přeložit	k5eAaPmAgMnS	přeložit
encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
"	"	kIx"	"
<g/>
Námořní	námořní	k2eAgFnPc1d1	námořní
bitvy	bitva	k1gFnPc1	bitva
<g/>
"	"	kIx"	"
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
Oxfordského	oxfordský	k2eAgMnSc4d1	oxfordský
průvodce	průvodce	k1gMnSc4	průvodce
o	o	k7c6	o
lodích	loď	k1gFnPc6	loď
a	a	k8xC	a
moři	moře	k1gNnSc6	moře
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Oxford	Oxford	k1gInSc1	Oxford
Companion	Companion	k1gInSc1	Companion
to	ten	k3xDgNnSc1	ten
Ships	Ships	k1gInSc1	Ships
and	and	k?	and
the	the	k?	the
Sea	Sea	k1gFnSc2	Sea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
pod	pod	k7c7	pod
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Velký	velký	k2eAgInSc1d1	velký
slovník	slovník	k1gInSc1	slovník
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
moře	moře	k1gNnSc2	moře
<g/>
"	"	kIx"	"
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
jako	jako	k8xC	jako
referenční	referenční	k2eAgFnSc1d1	referenční
pomůcka	pomůcka	k1gFnSc1	pomůcka
čtenářům	čtenář	k1gMnPc3	čtenář
námořní	námořní	k2eAgFnSc2d1	námořní
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k6eAd1	též
přeložil	přeložit	k5eAaPmAgMnS	přeložit
knihu	kniha	k1gFnSc4	kniha
"	"	kIx"	"
<g/>
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
Vikingové	Viking	k1gMnPc1	Viking
<g/>
"	"	kIx"	"
Jonathana	Jonathan	k1gMnSc2	Jonathan
Trigga	Trigga	k1gFnSc1	Trigga
o	o	k7c6	o
skandinávských	skandinávský	k2eAgMnPc6d1	skandinávský
dobrovolnících	dobrovolník	k1gMnPc6	dobrovolník
SS	SS	kA	SS
a	a	k8xC	a
brožury	brožura	k1gFnSc2	brožura
"	"	kIx"	"
<g/>
Záliv	záliv	k1gInSc1	záliv
Leyte	Leyt	k1gInSc5	Leyt
1944	[number]	k4	1944
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Iwodžima	Iwodžima	k1gFnSc1	Iwodžima
1945	[number]	k4	1945
<g/>
"	"	kIx"	"
z	z	k7c2	z
edice	edice	k1gFnSc2	edice
Velké	velký	k2eAgFnSc2d1	velká
bitvy	bitva	k1gFnSc2	bitva
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
plastikového	plastikový	k2eAgNnSc2d1	plastikové
modelářství	modelářství	k1gNnSc2	modelářství
pak	pak	k6eAd1	pak
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
při	při	k7c6	při
překladech	překlad	k1gInPc6	překlad
modelářských	modelářský	k2eAgFnPc2d1	modelářská
příruček	příručka	k1gFnPc2	příručka
"	"	kIx"	"
<g/>
Modelování	modelování	k1gNnSc1	modelování
terénu	terén	k1gInSc2	terén
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Modelujeme	modelovat	k5eAaImIp1nP	modelovat
bojovou	bojový	k2eAgFnSc4d1	bojová
techniku	technika	k1gFnSc4	technika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Jak	jak	k8xS	jak
na	na	k7c4	na
modely	model	k1gInPc4	model
technikou	technika	k1gFnSc7	technika
Airbrush	Airbrush	k1gMnSc1	Airbrush
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Jak	jak	k8xC	jak
na	na	k7c4	na
modely	model	k1gInPc4	model
letadel	letadlo	k1gNnPc2	letadlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
konfliktů	konflikt	k1gInPc2	konflikt
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
5000	[number]	k4	5000
let	léto	k1gNnPc2	léto
námořních	námořní	k2eAgMnPc2d1	námořní
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
</s>
</p>
<p>
<s>
Námořní	námořní	k2eAgFnPc1d1	námořní
bitvy	bitva	k1gFnPc1	bitva
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
slovník	slovník	k1gInSc1	slovník
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
moře	moře	k1gNnSc2	moře
</s>
</p>
<p>
<s>
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
Vikingové	Viking	k1gMnPc1	Viking
</s>
</p>
<p>
<s>
Záliv	záliv	k1gInSc1	záliv
Leyte	Leyt	k1gMnSc5	Leyt
1944	[number]	k4	1944
</s>
</p>
<p>
<s>
Iwodžima	Iwodžima	k1gFnSc1	Iwodžima
1945	[number]	k4	1945
</s>
</p>
<p>
<s>
Modelování	modelování	k1gNnSc1	modelování
terénu	terén	k1gInSc2	terén
</s>
</p>
<p>
<s>
Modelujeme	modelovat	k5eAaImIp1nP	modelovat
bojovou	bojový	k2eAgFnSc4d1	bojová
techniku	technika	k1gFnSc4	technika
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
na	na	k7c4	na
modely	model	k1gInPc4	model
technikou	technika	k1gFnSc7	technika
Airbrush	Airbrusha	k1gFnPc2	Airbrusha
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
na	na	k7c4	na
modely	model	k1gInPc4	model
letadel	letadlo	k1gNnPc2	letadlo
</s>
</p>
<p>
<s>
==	==	k?	==
Plastikové	plastikový	k2eAgNnSc1d1	plastikové
modelářství	modelářství	k1gNnSc1	modelářství
==	==	k?	==
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
koníčkům	koníček	k1gInPc3	koníček
Františka	František	k1gMnSc4	František
Novotného	Novotný	k1gMnSc4	Novotný
patří	patřit	k5eAaImIp3nS	patřit
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
i	i	k9	i
plastikové	plastikový	k2eAgNnSc1d1	plastikové
modelářství	modelářství	k1gNnSc1	modelářství
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
začátky	začátek	k1gInPc4	začátek
a	a	k8xC	a
celoživotní	celoživotní	k2eAgInSc4d1	celoživotní
koníček	koníček	k1gInSc4	koníček
sepsal	sepsat	k5eAaPmAgMnS	sepsat
ve	v	k7c6	v
vyprávění	vyprávění	k1gNnSc6	vyprávění
Můj	můj	k3xOp1gInSc1	můj
život	život	k1gInSc1	život
s	s	k7c7	s
kity	kit	k1gInPc7	kit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc4d1	krátký
náhled	náhled	k1gInSc4	náhled
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
K	k	k7c3	k
modelářství	modelářství	k1gNnSc3	modelářství
jsem	být	k5eAaImIp1nS	být
inklinoval	inklinovat	k5eAaImAgMnS	inklinovat
již	již	k6eAd1	již
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
jsem	být	k5eAaImIp1nS	být
na	na	k7c6	na
brněnském	brněnský	k2eAgInSc6d1	brněnský
jachtklubu	jachtklub	k1gInSc6	jachtklub
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsme	být	k5eAaImIp1nP	být
se	s	k7c7	s
stejně	stejně	k6eAd1	stejně
starými	starý	k2eAgMnPc7d1	starý
vrstevníky	vrstevník	k1gMnPc7	vrstevník
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
klukovskou	klukovský	k2eAgFnSc4d1	klukovská
partu	parta	k1gFnSc4	parta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
naší	náš	k3xOp1gFnSc3	náš
oblíbené	oblíbený	k2eAgFnSc3d1	oblíbená
zábavě	zábava	k1gFnSc3	zábava
patřilo	patřit	k5eAaImAgNnS	patřit
vyřezávání	vyřezávání	k1gNnSc1	vyřezávání
lodiček	lodička	k1gFnPc2	lodička
z	z	k7c2	z
borové	borový	k2eAgFnSc2d1	Borová
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
vypravovali	vypravovat	k5eAaImAgMnP	vypravovat
do	do	k7c2	do
okolních	okolní	k2eAgInPc2d1	okolní
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
jsme	být	k5eAaImIp1nP	být
"	"	kIx"	"
<g/>
koráby	koráb	k1gInPc4	koráb
<g/>
"	"	kIx"	"
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
kliprů	klipr	k1gInPc2	klipr
–	–	k?	–
s	s	k7c7	s
několika	několik	k4yIc7	několik
stěžni	stěžeň	k1gInPc7	stěžeň
ze	z	k7c2	z
špejlí	špejle	k1gFnPc2	špejle
a	a	k8xC	a
patry	patro	k1gNnPc7	patro
papírových	papírový	k2eAgFnPc2d1	papírová
plachet	plachta	k1gFnPc2	plachta
napíchnutých	napíchnutý	k2eAgFnPc2d1	napíchnutá
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Kýly	kýl	k1gInPc4	kýl
jsme	být	k5eAaImIp1nP	být
vystřihovali	vystřihovat	k5eAaImAgMnP	vystřihovat
z	z	k7c2	z
konzervového	konzervový	k2eAgInSc2d1	konzervový
plechu	plech	k1gInSc2	plech
a	a	k8xC	a
opatřovali	opatřovat	k5eAaImAgMnP	opatřovat
zátěží	zátěž	k1gFnSc7	zátěž
z	z	k7c2	z
rybářských	rybářský	k2eAgFnPc2d1	rybářská
olůvek	olůvko	k1gNnPc2	olůvko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
lodičkami	lodička	k1gFnPc7	lodička
jsme	být	k5eAaImIp1nP	být
pak	pak	k6eAd1	pak
pořádali	pořádat	k5eAaImAgMnP	pořádat
závody	závod	k1gInPc4	závod
mezi	mezi	k7c7	mezi
moly	mol	k1gMnPc7	mol
jachtklubu	jachtklub	k1gInSc2	jachtklub
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
modely	model	k1gInPc1	model
byly	být	k5eAaImAgInP	být
tak	tak	k6eAd1	tak
výkonné	výkonný	k2eAgInPc1d1	výkonný
<g/>
,	,	kIx,	,
že	že	k8xS	že
našim	náš	k3xOp1gMnPc3	náš
závodům	závod	k1gInPc3	závod
přihlíželi	přihlížet	k5eAaImAgMnP	přihlížet
i	i	k9	i
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Mimochodem	mimochod	k1gInSc7	mimochod
<g/>
,	,	kIx,	,
na	na	k7c6	na
těch	ten	k3xDgFnPc6	ten
kůrových	kůrový	k2eAgFnPc6d1	Kůrová
lodičkách	lodička	k1gFnPc6	lodička
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
nejlépe	dobře	k6eAd3	dobře
naučil	naučit	k5eAaPmAgMnS	naučit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
plochou	plocha	k1gFnSc7	plocha
plachet	plachta	k1gFnPc2	plachta
vyvážit	vyvážit	k5eAaPmF	vyvážit
plachetnici	plachetnice	k1gFnSc4	plachetnice
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
směru	směr	k1gInSc3	směr
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
pak	pak	k6eAd1	pak
hodilo	hodit	k5eAaImAgNnS	hodit
v	v	k7c6	v
dospělém	dospělý	k2eAgInSc6d1	dospělý
jachtařském	jachtařský	k2eAgInSc6d1	jachtařský
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
František	František	k1gMnSc1	František
Novotný	Novotný	k1gMnSc1	Novotný
(	(	kIx(	(
<g/>
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Příspěvky	příspěvek	k1gInPc1	příspěvek
v	v	k7c6	v
Ikarii	Ikarie	k1gFnSc6	Ikarie
</s>
</p>
<p>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
na	na	k7c6	na
Legii	legie	k1gFnSc6	legie
</s>
</p>
<p>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
na	na	k7c6	na
webu	web	k1gInSc6	web
Fantasie	fantasie	k1gFnSc2	fantasie
</s>
</p>
