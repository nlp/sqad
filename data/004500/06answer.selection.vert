<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1887	[number]	k4	1887
až	až	k9	až
1889	[number]	k4	1889
a	a	k8xC	a
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
dostavěn	dostavěn	k2eAgInSc1d1	dostavěn
Chrysler	Chrysler	k1gInSc1	Chrysler
Building	Building	k1gInSc4	Building
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
300,65	[number]	k4	300,65
metru	metr	k1gInSc2	metr
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
