<s>
Helena	Helena	k1gFnSc1	Helena
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1947	[number]	k4	1947
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
českých	český	k2eAgFnPc2d1	Česká
zpěvaček	zpěvačka	k1gFnPc2	zpěvačka
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
herce	herec	k1gMnSc2	herec
a	a	k8xC	a
zpěváka	zpěvák	k1gMnSc2	zpěvák
Jiřího	Jiří	k1gMnSc2	Jiří
Vondráčka	Vondráček	k1gMnSc2	Vondráček
a	a	k8xC	a
teta	teta	k1gFnSc1	teta
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
a	a	k8xC	a
herečky	herečka	k1gFnSc2	herečka
Lucie	Lucie	k1gFnSc2	Lucie
Vondráčkové	Vondráčková	k1gFnSc2	Vondráčková
<g/>
.	.	kIx.	.
</s>
<s>
Helena	Helena	k1gFnSc1	Helena
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1947	[number]	k4	1947
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Blaženě	blaženě	k6eAd1	blaženě
a	a	k8xC	a
Jiřímu	Jiří	k1gMnSc3	Jiří
Vondráčkovým	Vondráčkův	k2eAgNnPc3d1	Vondráčkovo
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
prožila	prožít	k5eAaPmAgFnS	prožít
ve	v	k7c6	v
východočeských	východočeský	k2eAgInPc6d1	východočeský
Slatiňanech	Slatiňany	k1gInPc6	Slatiňany
s	s	k7c7	s
bratrem	bratr	k1gMnSc7	bratr
Jiřím	Jiří	k1gMnSc7	Jiří
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
Zdenou	Zdena	k1gFnSc7	Zdena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
předchozího	předchozí	k2eAgNnSc2d1	předchozí
manželství	manželství	k1gNnSc2	manželství
maminky	maminka	k1gFnSc2	maminka
Blaženy	Blažena	k1gFnSc2	Blažena
Osudový	osudový	k2eAgInSc4d1	osudový
zlom	zlom	k1gInSc4	zlom
pro	pro	k7c4	pro
Helenu	Helena	k1gFnSc4	Helena
Vondráčkovou	Vondráčková	k1gFnSc4	Vondráčková
nastal	nastat	k5eAaPmAgMnS	nastat
27	[number]	k4	27
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Lucerna	lucerna	k1gFnSc1	lucerna
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
pěveckou	pěvecký	k2eAgFnSc4d1	pěvecká
soutěž	soutěž	k1gFnSc4	soutěž
Hledáme	hledat	k5eAaImIp1nP	hledat
nové	nový	k2eAgInPc1d1	nový
talenty	talent	k1gInPc1	talent
s	s	k7c7	s
písněmi	píseň	k1gFnPc7	píseň
George	Georg	k1gMnSc2	Georg
Gershwina	Gershwin	k1gMnSc2	Gershwin
Summertime	Summertim	k1gInSc5	Summertim
a	a	k8xC	a
The	The	k1gMnPc3	The
Man	mana	k1gFnPc2	mana
I	i	k9	i
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
hitům	hit	k1gInPc3	hit
Červená	červenat	k5eAaImIp3nS	červenat
řeka	řeka	k1gFnSc1	řeka
a	a	k8xC	a
Pátá	pátá	k1gFnSc1	pátá
stala	stát	k5eAaPmAgFnS	stát
Zlatou	zlatý	k2eAgFnSc7d1	zlatá
slavicí	slavice	k1gFnSc7	slavice
a	a	k8xC	a
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
do	do	k7c2	do
angažmá	angažmá	k1gNnSc2	angažmá
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Rokoko	rokoko	k1gNnSc1	rokoko
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
s	s	k7c7	s
Martou	Marta	k1gFnSc7	Marta
Kubišovou	Kubišová	k1gFnSc7	Kubišová
a	a	k8xC	a
Václavem	Václav	k1gMnSc7	Václav
Neckářem	Neckář	k1gMnSc7	Neckář
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
popové	popový	k2eAgNnSc4d1	popové
trio	trio	k1gNnSc4	trio
Golden	Goldna	k1gFnPc2	Goldna
Kids	Kidsa	k1gFnPc2	Kidsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
hrála	hrát	k5eAaImAgFnS	hrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
pohádce	pohádka	k1gFnSc6	pohádka
Šíleně	šíleně	k6eAd1	šíleně
smutná	smutný	k2eAgFnSc1d1	smutná
princezna	princezna	k1gFnSc1	princezna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
postavu	postava	k1gFnSc4	postava
princezny	princezna	k1gFnSc2	princezna
Helenky	Helenka	k1gFnSc2	Helenka
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
nuceným	nucený	k2eAgInSc7d1	nucený
rozpadem	rozpad	k1gInSc7	rozpad
Golden	Goldna	k1gFnPc2	Goldna
Kids	Kidsa	k1gFnPc2	Kidsa
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
režim	režim	k1gInSc1	režim
zakázal	zakázat	k5eAaPmAgInS	zakázat
činnost	činnost	k1gFnSc4	činnost
Martě	Marta	k1gFnSc3	Marta
Kubišové	Kubišová	k1gFnSc3	Kubišová
a	a	k8xC	a
zbývající	zbývající	k2eAgMnPc1d1	zbývající
dva	dva	k4xCgMnPc1	dva
členové	člen	k1gMnPc1	člen
souboru	soubor	k1gInSc2	soubor
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
sólové	sólový	k2eAgFnPc4d1	sólová
dráhy	dráha	k1gFnPc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejexportovanější	exportovaný	k2eAgFnSc7d3	nejexportovanější
českou	český	k2eAgFnSc7d1	Česká
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Točila	točit	k5eAaImAgFnS	točit
alba	album	k1gNnPc4	album
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
společnosti	společnost	k1gFnPc4	společnost
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Západní	západní	k2eAgNnSc1d1	západní
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
festivalech	festival	k1gInPc6	festival
a	a	k8xC	a
pódiích	pódium	k1gNnPc6	pódium
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
měla	mít	k5eAaImAgFnS	mít
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Malovaný	malovaný	k2eAgInSc1d1	malovaný
džbánku	džbánek	k1gInSc2	džbánek
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
v	v	k7c6	v
polských	polský	k2eAgInPc6d1	polský
Sopotech	sopot	k1gInPc6	sopot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
dalšího	další	k2eAgInSc2d1	další
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
filmu	film	k1gInSc2	film
-	-	kIx~	-
komedii	komedie	k1gFnSc4	komedie
Jen	jen	k9	jen
ho	on	k3xPp3gInSc4	on
nechte	nechat	k5eAaPmRp2nP	nechat
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
podpis	podpis	k1gInSc1	podpis
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
signatářů	signatář	k1gMnPc2	signatář
Anticharty	anticharta	k1gFnSc2	anticharta
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zveřejnilo	zveřejnit	k5eAaPmAgNnS	zveřejnit
Rudé	rudý	k2eAgNnSc1d1	Rudé
právo	právo	k1gNnSc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Antichartu	anticharta	k1gFnSc4	anticharta
nepodepsala	podepsat	k5eNaPmAgFnS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
soustřeďovala	soustřeďovat	k5eAaImAgFnS	soustřeďovat
na	na	k7c4	na
domácí	domácí	k2eAgNnSc4d1	domácí
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Natáčela	natáčet	k5eAaImAgFnS	natáčet
pravidelně	pravidelně	k6eAd1	pravidelně
dlouhohrající	dlouhohrající	k2eAgFnPc4d1	dlouhohrající
desky	deska	k1gFnPc4	deska
a	a	k8xC	a
podnikala	podnikat	k5eAaImAgFnS	podnikat
koncertní	koncertní	k2eAgFnPc4d1	koncertní
šňůry	šňůra	k1gFnPc4	šňůra
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
Orchestrem	orchestr	k1gInSc7	orchestr
Gustava	Gustav	k1gMnSc2	Gustav
Broma	Brom	k1gMnSc2	Brom
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Kornem	Korn	k1gMnSc7	Korn
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
měla	mít	k5eAaImAgFnS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
program	program	k1gInSc4	program
Velká	velký	k2eAgFnSc1d1	velká
neónová	neónový	k2eAgFnSc1d1	neónová
láska	láska	k1gFnSc1	láska
s	s	k7c7	s
doprovodnou	doprovodný	k2eAgFnSc7d1	doprovodná
skupinou	skupina	k1gFnSc7	skupina
Bacily	bacit	k5eAaPmAgFnP	bacit
<g/>
.	.	kIx.	.
</s>
<s>
Moderovala	moderovat	k5eAaBmAgFnS	moderovat
televizní	televizní	k2eAgInPc4d1	televizní
pořad	pořad	k1gInSc4	pořad
Sejdeme	sejít	k5eAaPmIp1nP	sejít
se	se	k3xPyFc4	se
na	na	k7c6	na
výsluní	výsluní	k1gNnSc6	výsluní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
převzala	převzít	k5eAaPmAgFnS	převzít
titul	titul	k1gInSc4	titul
zasloužilá	zasloužilý	k2eAgFnSc1d1	zasloužilá
umělkyně	umělkyně	k1gFnSc1	umělkyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
německého	německý	k2eAgMnSc4d1	německý
hudebníka	hudebník	k1gMnSc4	hudebník
Helmuta	Helmut	k1gMnSc2	Helmut
Sickela	Sickel	k1gMnSc2	Sickel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
řadu	řada	k1gFnSc4	řada
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
hitů	hit	k1gInPc2	hit
Čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
nám	my	k3xPp1nPc3	my
a	a	k8xC	a
Sprint	sprint	k1gInSc4	sprint
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
objevovala	objevovat	k5eAaImAgFnS	objevovat
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
nebo	nebo	k8xC	nebo
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Zlatý	zlatý	k2eAgInSc1d1	zlatý
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
obsazovala	obsazovat	k5eAaImAgFnS	obsazovat
nižší	nízký	k2eAgFnPc4d2	nižší
příčky	příčka	k1gFnPc4	příčka
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
však	však	k9	však
patřila	patřit	k5eAaImAgFnS	patřit
mezi	mezi	k7c4	mezi
nejžádanější	žádaný	k2eAgMnPc4d3	nejžádanější
interprety	interpret	k1gMnPc4	interpret
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
útlum	útlum	k1gInSc1	útlum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
začátkem	začátkem	k7c2	začátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
změnil	změnit	k5eAaPmAgInS	změnit
příchod	příchod	k1gInSc1	příchod
muzikálů	muzikál	k1gInPc2	muzikál
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
obsazena	obsadit	k5eAaPmNgNnP	obsadit
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
ženské	ženský	k2eAgFnSc2d1	ženská
role	role	k1gFnSc2	role
hned	hned	k6eAd1	hned
toho	ten	k3xDgNnSc2	ten
prvního	první	k4xOgNnSc2	první
-	-	kIx~	-
Bídníci	bídník	k1gMnPc1	bídník
<g/>
.	.	kIx.	.
</s>
<s>
Vydala	vydat	k5eAaPmAgFnS	vydat
Broadway	Broadwaa	k1gFnSc2	Broadwaa
album	album	k1gNnSc4	album
plné	plný	k2eAgFnSc2d1	plná
známých	známý	k2eAgFnPc2d1	známá
muzikálových	muzikálový	k2eAgFnPc2d1	muzikálová
melodií	melodie	k1gFnPc2	melodie
<g/>
,	,	kIx,	,
dvě	dva	k4xCgNnPc4	dva
vánoční	vánoční	k2eAgNnPc4d1	vánoční
alba	album	k1gNnPc4	album
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
výběr	výběr	k1gInSc1	výběr
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Helena	Helena	k1gFnSc1	Helena
s	s	k7c7	s
hitem	hit	k1gInSc7	hit
Já	já	k3xPp1nSc1	já
půjdu	jít	k5eAaImIp1nS	jít
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
milníkem	milník	k1gInSc7	milník
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
kariéře	kariéra	k1gFnSc6	kariéra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
rok	rok	k1gInSc1	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
Vodopád	vodopád	k1gInSc1	vodopád
<g/>
,	,	kIx,	,
plné	plný	k2eAgInPc1d1	plný
moderních	moderní	k2eAgFnPc2d1	moderní
převážně	převážně	k6eAd1	převážně
tanečních	taneční	k2eAgFnPc2d1	taneční
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
nejprodávanější	prodávaný	k2eAgFnSc7d3	nejprodávanější
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
Dlouhá	Dlouhá	k1gFnSc1	Dlouhá
noc	noc	k1gFnSc1	noc
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
hitem	hit	k1gInSc7	hit
roku	rok	k1gInSc2	rok
a	a	k8xC	a
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
opět	opět	k6eAd1	opět
objevuje	objevovat	k5eAaImIp3nS	objevovat
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
místech	místo	k1gNnPc6	místo
ankety	anketa	k1gFnSc2	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2001	[number]	k4	2001
přinesl	přinést	k5eAaPmAgInS	přinést
další	další	k2eAgInPc4d1	další
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
rozvod	rozvod	k1gInSc1	rozvod
s	s	k7c7	s
Helmutem	Helmut	k1gMnSc7	Helmut
Sickelem	Sickel	k1gMnSc7	Sickel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
jí	on	k3xPp3gFnSc2	on
Akademie	akademie	k1gFnSc2	akademie
české	český	k2eAgFnSc2d1	Česká
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
zvolila	zvolit	k5eAaPmAgFnS	zvolit
Zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2003	[number]	k4	2003
se	se	k3xPyFc4	se
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
Martina	Martin	k1gMnSc4	Martin
Michala	Michal	k1gMnSc4	Michal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
v	v	k7c4	v
prestižní	prestižní	k2eAgFnPc4d1	prestižní
Carnegie	Carnegie	k1gFnPc4	Carnegie
Hall	Halla	k1gFnPc2	Halla
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
již	již	k9	již
před	před	k7c4	před
pěty	pět	k2eAgInPc4d1	pět
lety	let	k1gInPc4	let
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
host	host	k1gMnSc1	host
Karla	Karel	k1gMnSc2	Karel
Gotta	Gotto	k1gNnSc2	Gotto
<g/>
,	,	kIx,	,
ztvárňuje	ztvárňovat	k5eAaImIp3nS	ztvárňovat
roli	role	k1gFnSc4	role
Grizabelly	Grizabella	k1gFnSc2	Grizabella
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
verzi	verze	k1gFnSc6	verze
muzikálu	muzikál	k1gInSc2	muzikál
Cats	Cats	k1gInSc1	Cats
(	(	kIx(	(
<g/>
Kočky	kočka	k1gFnPc1	kočka
<g/>
)	)	kIx)	)
a	a	k8xC	a
slaví	slavit	k5eAaImIp3nP	slavit
40	[number]	k4	40
let	léto	k1gNnPc2	léto
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
vydáním	vydání	k1gNnSc7	vydání
profilového	profilový	k2eAgInSc2d1	profilový
4-CD	[number]	k4	4-CD
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
kolekce	kolekce	k1gFnSc1	kolekce
a	a	k8xC	a
velkou	velká	k1gFnSc7	velká
televizní	televizní	k2eAgFnSc1d1	televizní
show	show	k1gFnSc1	show
Holka	holka	k1gFnSc1	holka
od	od	k7c2	od
Červený	Červený	k1gMnSc1	Červený
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2006	[number]	k4	2006
jí	jíst	k5eAaImIp3nS	jíst
přinesl	přinést	k5eAaPmAgInS	přinést
velkou	velká	k1gFnSc4	velká
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
mnohých	mnohý	k2eAgMnPc2d1	mnohý
životní	životní	k2eAgFnSc4d1	životní
roli	role	k1gFnSc4	role
<g/>
:	:	kIx,	:
na	na	k7c6	na
bratislavské	bratislavský	k2eAgFnSc6d1	Bratislavská
Nové	Nové	k2eAgFnSc6d1	Nové
scéně	scéna	k1gFnSc6	scéna
hrála	hrát	k5eAaImAgFnS	hrát
<g/>
,	,	kIx,	,
zpívala	zpívat	k5eAaImAgFnS	zpívat
a	a	k8xC	a
tančila	tančit	k5eAaImAgFnS	tančit
vynikajícím	vynikající	k2eAgInSc7d1	vynikající
způsobem	způsob	k1gInSc7	způsob
Dolly	Dolla	k1gFnSc2	Dolla
Leviovou	Leviový	k2eAgFnSc7d1	Leviová
ve	v	k7c6	v
známém	známý	k2eAgInSc6d1	známý
muzikálu	muzikál	k1gInSc6	muzikál
Hello	Hello	k1gNnSc1	Hello
<g/>
,	,	kIx,	,
Dolly	Doll	k1gInPc1	Doll
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
vydala	vydat	k5eAaPmAgFnS	vydat
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
novinkové	novinkový	k2eAgNnSc1d1	novinkové
album	album	k1gNnSc1	album
Zastav	zastavit	k5eAaPmRp2nS	zastavit
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
a	a	k8xC	a
poslouchej	poslouchat	k5eAaImRp2nS	poslouchat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
je	být	k5eAaImIp3nS	být
cover	cover	k1gInSc1	cover
verze	verze	k1gFnSc2	verze
slavného	slavný	k2eAgInSc2d1	slavný
hitu	hit	k1gInSc2	hit
It	It	k1gFnPc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Raining	Raining	k1gInSc1	Raining
Men	Men	k1gFnPc4	Men
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
názvem	název	k1gInSc7	název
Já	já	k3xPp1nSc1	já
vítám	vítat	k5eAaImIp1nS	vítat
déšť	déšť	k1gInSc1	déšť
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
novinky	novinka	k1gFnPc1	novinka
i	i	k8xC	i
starší	starý	k2eAgInPc1d2	starší
hity	hit	k1gInPc1	hit
představila	představit	k5eAaPmAgFnS	představit
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
na	na	k7c6	na
vyprodaném	vyprodaný	k2eAgInSc6d1	vyprodaný
koncertu	koncert	k1gInSc6	koncert
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
vzpomínání	vzpomínání	k1gNnSc2	vzpomínání
i	i	k8xC	i
nových	nový	k2eAgInPc2d1	nový
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
televizi	televize	k1gFnSc6	televize
Prima	prima	k6eAd1	prima
moderovala	moderovat	k5eAaBmAgFnS	moderovat
hudební	hudební	k2eAgInSc4d1	hudební
pořad	pořad	k1gInSc4	pořad
Hvězdy	hvězda	k1gFnSc2	hvězda
u	u	k7c2	u
Piana	piano	k1gNnSc2	piano
<g/>
,	,	kIx,	,
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
kompilační	kompilační	k2eAgNnSc1d1	kompilační
album	album	k1gNnSc1	album
duetů	duet	k1gInPc2	duet
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Kornem	Korn	k1gMnSc7	Korn
Těch	ten	k3xDgInPc2	ten
pár	pár	k4xCyI	pár
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
oslavila	oslavit	k5eAaPmAgFnS	oslavit
své	svůj	k3xOyFgFnPc4	svůj
kulaté	kulatý	k2eAgFnPc4d1	kulatá
narozeniny	narozeniny	k1gFnPc4	narozeniny
velkým	velký	k2eAgInSc7d1	velký
vzpomínkovým	vzpomínkový	k2eAgInSc7d1	vzpomínkový
koncertem	koncert	k1gInSc7	koncert
na	na	k7c6	na
Žofíně	Žofín	k1gInSc6	Žofín
<g/>
.	.	kIx.	.
</s>
<s>
Podzim	podzim	k1gInSc1	podzim
přinesl	přinést	k5eAaPmAgInS	přinést
bilanční	bilanční	k2eAgInSc4d1	bilanční
2-CD	[number]	k4	2-CD
Jsem	být	k5eAaImIp1nS	být
jaká	jaký	k3yRgFnSc1	jaký
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
Těch	ten	k3xDgInPc2	ten
pár	pár	k4xCyI	pár
dnů	den	k1gInPc2	den
a	a	k8xC	a
veleúspěšný	veleúspěšný	k2eAgInSc4d1	veleúspěšný
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Lucerně	lucerna	k1gFnSc6	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samém	samý	k3xTgInSc6	samý
závěru	závěr	k1gInSc6	závěr
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Karla	Karel	k1gMnSc2	Karel
Gotta	Gott	k1gMnSc2	Gott
ve	v	k7c6	v
finálové	finálový	k2eAgFnSc6d1	finálová
silvestrovské	silvestrovský	k2eAgFnSc6d1	silvestrovská
show	show	k1gFnSc6	show
Mejdan	mejdan	k1gInSc4	mejdan
roku	rok	k1gInSc2	rok
z	z	k7c2	z
Václaváku	Václavák	k1gInSc2	Václavák
a	a	k8xC	a
společně	společně	k6eAd1	společně
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
hosty	host	k1gMnPc4	host
i	i	k8xC	i
novoročního	novoroční	k2eAgNnSc2d1	novoroční
vydání	vydání	k1gNnSc2	vydání
hudebního	hudební	k2eAgInSc2d1	hudební
pořadu	pořad	k1gInSc2	pořad
Eso	eso	k1gNnSc1	eso
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
rok	rok	k1gInSc1	rok
2008	[number]	k4	2008
vyšel	vyjít	k5eAaPmAgInS	vyjít
retrospektivní	retrospektivní	k2eAgInSc1d1	retrospektivní
výběr	výběr	k1gInSc1	výběr
hitů	hit	k1gInPc2	hit
tria	trio	k1gNnSc2	trio
Golden	Goldna	k1gFnPc2	Goldna
Kids	Kidsa	k1gFnPc2	Kidsa
24	[number]	k4	24
Golden	Goldna	k1gFnPc2	Goldna
Hits	Hitsa	k1gFnPc2	Hitsa
a	a	k8xC	a
pro	pro	k7c4	pro
její	její	k3xOp3gMnPc4	její
fanoušky	fanoušek	k1gMnPc4	fanoušek
pak	pak	k6eAd1	pak
kolekce	kolekce	k1gFnSc1	kolekce
raritních	raritní	k2eAgFnPc2d1	raritní
nahrávek	nahrávka	k1gFnPc2	nahrávka
Blázen	blázen	k1gMnSc1	blázen
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
lásky	láska	k1gFnSc2	láska
zříká	zříkat	k5eAaImIp3nS	zříkat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
natočila	natočit	k5eAaBmAgFnS	natočit
v	v	k7c6	v
Lucerně	lucerna	k1gFnSc6	lucerna
DVD	DVD	kA	DVD
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
velkého	velký	k2eAgInSc2d1	velký
muzikálového	muzikálový	k2eAgInSc2d1	muzikálový
projektu	projekt	k1gInSc2	projekt
Mona	Mona	k1gFnSc1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
divadle	divadlo	k1gNnSc6	divadlo
Broadway	Broadwaa	k1gFnSc2	Broadwaa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
alternaci	alternace	k1gFnSc6	alternace
s	s	k7c7	s
Hanou	Hana	k1gFnSc7	Hana
Zagorovou	Zagorová	k1gFnSc7	Zagorová
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
roli	role	k1gFnSc4	role
matky	matka	k1gFnSc2	matka
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hrdinky	hrdinka	k1gFnSc2	hrdinka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
vydala	vydat	k5eAaPmAgFnS	vydat
dlouho	dlouho	k6eAd1	dlouho
očekávané	očekávaný	k2eAgNnSc4d1	očekávané
album	album	k1gNnSc4	album
nových	nový	k2eAgFnPc2d1	nová
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
Zůstáváš	zůstávat	k5eAaImIp2nS	zůstávat
tu	tu	k6eAd1	tu
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
Helena	Helena	k1gFnSc1	Helena
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
a	a	k8xC	a
manažer	manažer	k1gMnSc1	manažer
Martin	Martin	k1gMnSc1	Martin
Michal	Michal	k1gMnSc1	Michal
prodali	prodat	k5eAaPmAgMnP	prodat
svoji	svůj	k3xOyFgFnSc4	svůj
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
agenturu	agentura	k1gFnSc4	agentura
MM	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Koupil	koupit	k5eAaPmAgInS	koupit
ji	on	k3xPp3gFnSc4	on
floridský	floridský	k2eAgMnSc1d1	floridský
právník	právník	k1gMnSc1	právník
Michael	Michael	k1gMnSc1	Michael
J.	J.	kA	J.
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
Vondráčková	Vondráčková	k1gFnSc1	Vondráčková
tak	tak	k6eAd1	tak
učinila	učinit	k5eAaImAgFnS	učinit
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
mohla	moct	k5eAaImAgFnS	moct
vést	vést	k5eAaImF	vést
arbitráž	arbitráž	k1gFnSc4	arbitráž
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Heleny	Helena	k1gFnSc2	Helena
Vondráčkové	Vondráčková	k1gFnSc2	Vondráčková
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
písní	píseň	k1gFnPc2	píseň
Heleny	Helena	k1gFnSc2	Helena
Vondráčkové	Vondráčková	k1gFnSc2	Vondráčková
<g/>
.	.	kIx.	.
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
:	:	kIx,	:
Červená	červený	k2eAgFnSc1d1	červená
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
Pátá	pátý	k4xOgFnSc1	pátý
<g/>
,	,	kIx,	,
Chytila	chytit	k5eAaPmAgFnS	chytit
jsem	být	k5eAaImIp1nS	být
na	na	k7c6	na
pasece	paseka	k1gFnSc6	paseka
motýlka	motýlek	k1gMnSc2	motýlek
<g/>
,	,	kIx,	,
Vzdálený	vzdálený	k2eAgInSc1d1	vzdálený
hlas	hlas	k1gInSc1	hlas
<g/>
,	,	kIx,	,
Růže	růž	k1gInPc1	růž
kvetou	kvést	k5eAaImIp3nP	kvést
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
Mám	mít	k5eAaImIp1nS	mít
ráda	rád	k2eAgFnSc1d1	ráda
cestu	cesta	k1gFnSc4	cesta
lesní	lesní	k2eAgFnSc2d1	lesní
<g/>
,	,	kIx,	,
Přejdi	přejít	k5eAaPmRp2nS	přejít
Jordán	Jordán	k1gInSc1	Jordán
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nikdo	nikdo	k3yNnSc1	nikdo
nedoví	dovědět	k5eNaPmIp3nS	dovědět
<g/>
,	,	kIx,	,
Hříbě	hříbě	k1gNnSc1	hříbě
<g/>
,	,	kIx,	,
Hej	hej	k0	hej
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
zajíci	zajíc	k1gMnSc5	zajíc
<g/>
,	,	kIx,	,
Oh	oh	k0	oh
<g/>
,	,	kIx,	,
baby	baba	k1gFnPc1	baba
<g/>
,	,	kIx,	,
baby	baba	k1gFnPc1	baba
<g/>
,	,	kIx,	,
Časy	čas	k1gInPc1	čas
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
<g/>
,	,	kIx,	,
Stín	stín	k1gInSc1	stín
katedrál	katedrála	k1gFnPc2	katedrála
<g/>
,	,	kIx,	,
Tvá	tvůj	k3xOp2gFnSc1	tvůj
malá	malý	k2eAgFnSc1d1	malá
Jane	Jan	k1gMnSc5	Jan
<g/>
,	,	kIx,	,
Proč	proč	k6eAd1	proč
mě	já	k3xPp1nSc4	já
nikdo	nikdo	k3yNnSc1	nikdo
nemá	mít	k5eNaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
Slza	slza	k1gFnSc1	slza
z	z	k7c2	z
tváře	tvář	k1gFnSc2	tvář
<g />
.	.	kIx.	.
</s>
<s>
padá	padat	k5eAaImIp3nS	padat
1970	[number]	k4	1970
<g/>
-	-	kIx~	-
<g/>
1979	[number]	k4	1979
<g/>
:	:	kIx,	:
Miláčku	miláček	k1gMnSc5	miláček
<g/>
,	,	kIx,	,
Fanfán	Fanfán	k2eAgInSc4d1	Fanfán
<g/>
,	,	kIx,	,
Kam	kam	k6eAd1	kam
zmizel	zmizet	k5eAaPmAgInS	zmizet
ten	ten	k3xDgInSc1	ten
starý	starý	k2eAgInSc1d1	starý
song	song	k1gInSc1	song
<g/>
,	,	kIx,	,
Archiméde	Archimédes	k1gMnSc5	Archimédes
<g/>
,	,	kIx,	,
Znala	znát	k5eAaImAgFnS	znát
panna	panna	k1gFnSc1	panna
pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
Kvítek	kvítek	k1gInSc4	kvítek
mandragory	mandragora	k1gFnSc2	mandragora
<g/>
,	,	kIx,	,
Lásko	láska	k1gFnSc5	láska
má	můj	k3xOp1gFnSc1	můj
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
stůňu	stonat	k5eAaImIp1nS	stonat
<g/>
,	,	kIx,	,
Malovaný	malovaný	k2eAgMnSc1d1	malovaný
džbánku	džbánek	k1gInSc2	džbánek
<g/>
,	,	kIx,	,
Dvě	dva	k4xCgNnPc1	dva
malá	malý	k2eAgNnPc1d1	malé
křídla	křídlo	k1gNnPc1	křídlo
tu	tu	k6eAd1	tu
nejsou	být	k5eNaImIp3nP	být
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Ptačí	ptačí	k2eAgNnPc4d1	ptačí
hnízda	hnízdo	k1gNnPc4	hnízdo
<g/>
,	,	kIx,	,
Já	já	k3xPp1nSc1	já
půjdu	jít	k5eAaImIp1nS	jít
tam	tam	k6eAd1	tam
a	a	k8xC	a
ty	ten	k3xDgFnPc4	ten
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
Málo	málo	k6eAd1	málo
mám	mít	k5eAaImIp1nS	mít
lásky	láska	k1gFnSc2	láska
tvé	tvůj	k3xOp2gFnPc1	tvůj
<g/>
,	,	kIx,	,
Tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
budu	být	k5eAaImBp1nS	být
smát	smát	k5eAaImF	smát
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Vzhůru	vzhůru	k6eAd1	vzhůru
k	k	k7c3	k
výškám	výška	k1gFnPc3	výška
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
Léto	léto	k1gNnSc1	léto
je	být	k5eAaImIp3nS	být
léto	léto	k1gNnSc4	léto
<g/>
,	,	kIx,	,
Když	když	k8xS	když
zabloudíš	zabloudit	k5eAaPmIp2nS	zabloudit
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
tak	tak	k8xS	tak
zavolej	zavolat	k5eAaPmRp2nS	zavolat
<g/>
,	,	kIx,	,
Múzy	Múza	k1gFnPc1	Múza
<g/>
,	,	kIx,	,
A	a	k8xC	a
ty	ten	k3xDgInPc4	ten
se	se	k3xPyFc4	se
ptáš	ptat	k5eAaImIp2nS	ptat
<g/>
,	,	kIx,	,
co	co	k9	co
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Jarním	jarní	k2eAgFnPc3d1	jarní
loukám	louka	k1gFnPc3	louka
<g/>
,	,	kIx,	,
Sblížení	sblížení	k1gNnSc2	sblížení
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
Každá	každý	k3xTgFnSc1	každý
trampota	trampota	k1gFnSc1	trampota
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
mez	mez	k1gFnSc4	mez
<g/>
,	,	kIx,	,
Nač	nač	k6eAd1	nač
vlastně	vlastně	k9	vlastně
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
vzdávat	vzdávat	k5eAaImF	vzdávat
mač	mač	k1gInSc4	mač
<g/>
,	,	kIx,	,
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
malý	malý	k2eAgInSc1d1	malý
svět	svět	k1gInSc1	svět
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Jsem	být	k5eAaImIp1nS	být
jaká	jaký	k3yIgFnSc1	jaký
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
Sladké	Sladké	k2eAgNnSc2d1	Sladké
mámení	mámení	k1gNnSc2	mámení
<g/>
,	,	kIx,	,
Čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
nám	my	k3xPp1nPc3	my
<g/>
,	,	kIx,	,
To	ten	k3xDgNnSc4	ten
pan	pan	k1gMnSc1	pan
Chopin	Chopin	k1gMnSc1	Chopin
<g/>
,	,	kIx,	,
Náhodný	náhodný	k2eAgMnSc1d1	náhodný
známý	známý	k1gMnSc1	známý
<g/>
,	,	kIx,	,
Nahrávám	nahrávat	k5eAaImIp1nS	nahrávat
<g/>
,	,	kIx,	,
Sprint	sprint	k1gInSc1	sprint
<g/>
,	,	kIx,	,
Ještě	ještě	k6eAd1	ještě
světu	svět	k1gInSc3	svět
šanci	šance	k1gFnSc4	šance
dej	dát	k5eAaPmRp2nS	dát
<g/>
,	,	kIx,	,
Svou	svůj	k3xOyFgFnSc4	svůj
partu	parta	k1gFnSc4	parta
přátel	přítel	k1gMnPc2	přítel
ještě	ještě	k6eAd1	ještě
naštěstí	naštěstí	k6eAd1	naštěstí
mám	mít	k5eAaImIp1nS	mít
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Song	song	k1gInSc1	song
hrál	hrát	k5eAaImAgInS	hrát
nám	my	k3xPp1nPc3	my
ten	ten	k3xDgMnSc1	ten
ďábel	ďábel	k1gMnSc1	ďábel
saxofon	saxofon	k1gInSc1	saxofon
<g/>
,	,	kIx,	,
Copacabana	Copacabana	k1gFnSc1	Copacabana
<g/>
,	,	kIx,	,
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsi	být	k5eAaImIp2nS	být
ty	ty	k3xPp2nSc1	ty
<g/>
,	,	kIx,	,
Nevzdám	vzdát	k5eNaPmIp1nS	vzdát
se	se	k3xPyFc4	se
hvězdám	hvězda	k1gFnPc3	hvězda
<g/>
,	,	kIx,	,
Já	já	k3xPp1nSc1	já
půjdu	jít	k5eAaImIp1nS	jít
dál	daleko	k6eAd2	daleko
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
To	ten	k3xDgNnSc1	ten
tehdy	tehdy	k6eAd1	tehdy
padal	padat	k5eAaImAgInS	padat
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
Tvou	tvůj	k3xOp2gFnSc4	tvůj
vůni	vůně	k1gFnSc4	vůně
cítím	cítit	k5eAaImIp1nS	cítit
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
Neuč	učit	k5eNaImRp2nS	učit
slunce	slunce	k1gNnSc2	slunce
hřát	hřát	k5eAaImNgInS	hřát
<g/>
,	,	kIx,	,
Karneval	karneval	k1gInSc1	karneval
<g/>
,	,	kIx,	,
Déja	Déja	k1gFnSc1	Déja
vu	vu	k?	vu
<g/>
,	,	kIx,	,
Sundej	sundat	k5eAaPmRp2nS	sundat
kravatu	kravata	k1gFnSc4	kravata
<g/>
,	,	kIx,	,
Hádej	hádat	k5eAaImRp2nS	hádat
<g/>
...	...	k?	...
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Nebudeme	být	k5eNaImBp1nP	být
sedět	sedět	k5eAaImF	sedět
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
Já	já	k3xPp1nSc1	já
vítám	vítat	k5eAaImIp1nS	vítat
déšť	déšť	k1gInSc4	déšť
1967	[number]	k4	1967
Píseň	píseň	k1gFnSc1	píseň
pro	pro	k7c4	pro
Rudolfa	Rudolf	k1gMnSc4	Rudolf
III	III	kA	III
(	(	kIx(	(
<g/>
Tv	Tv	kA	Tv
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
-	-	kIx~	-
Helena	Helena	k1gFnSc1	Helena
1968	[number]	k4	1968
Šíleně	šíleně	k6eAd1	šíleně
smutná	smutný	k2eAgFnSc1d1	smutná
princezna	princezna	k1gFnSc1	princezna
-	-	kIx~	-
princezna	princezna	k1gFnSc1	princezna
Helena	Helena	k1gFnSc1	Helena
1969	[number]	k4	1969
Revue	revue	k1gFnPc2	revue
pro	pro	k7c4	pro
následníka	následník	k1gMnSc4	následník
trůnu	trůn	k1gInSc2	trůn
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
-	-	kIx~	-
Helena	Helena	k1gFnSc1	Helena
1972	[number]	k4	1972
Zpívající	zpívající	k2eAgInSc4d1	zpívající
film	film	k1gInSc4	film
1972	[number]	k4	1972
Příliš	příliš	k6eAd1	příliš
krásná	krásný	k2eAgFnSc1d1	krásná
dívka	dívka	k1gFnSc1	dívka
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
-	-	kIx~	-
Ludmila	Ludmila	k1gFnSc1	Ludmila
1975	[number]	k4	1975
Letní	letní	k2eAgFnPc1d1	letní
romance	romance	k1gFnPc1	romance
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
-	-	kIx~	-
stopařka	stopařka	k1gFnSc1	stopařka
1977	[number]	k4	1977
Jen	jen	k9	jen
ho	on	k3xPp3gInSc4	on
nechte	nechat	k5eAaPmRp2nP	nechat
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
-	-	kIx~	-
Marcela	Marcela	k1gFnSc1	Marcela
1980	[number]	k4	1980
Pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
Honzíkovi	Honzík	k1gMnSc6	Honzík
a	a	k8xC	a
Mařence	Mařenka	k1gFnSc6	Mařenka
1981	[number]	k4	1981
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
bratří	bratr	k1gMnPc2	bratr
Mánesů	Mánes	k1gMnPc2	Mánes
(	(	kIx(	(
<g/>
TV	TV	kA	TV
inscenace	inscenace	k1gFnSc2	inscenace
<g/>
)	)	kIx)	)
1982	[number]	k4	1982
Zpěváci	Zpěvák	k1gMnPc1	Zpěvák
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s>
kraji	kraj	k1gInPc7	kraj
nemocnice	nemocnice	k1gFnSc2	nemocnice
1982	[number]	k4	1982
Revue	revue	k1gFnPc2	revue
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
-	-	kIx~	-
Šeherezáda	Šeherezáda	k1gFnSc1	Šeherezáda
1984	[number]	k4	1984
Barrandovké	Barrandovká	k1gFnSc6	Barrandovká
nocturno	nocturno	k1gNnSc1	nocturno
aneb	aneb	k?	aneb
aneb	aneb	k?	aneb
jak	jak	k8xS	jak
film	film	k1gInSc1	film
tančil	tančit	k5eAaImAgInS	tančit
aneb	aneb	k?	aneb
zpíval	zpívat	k5eAaImAgMnS	zpívat
1988	[number]	k4	1988
Láska	láska	k1gFnSc1	láska
na	na	k7c4	na
inzerát	inzerát	k1gInSc4	inzerát
1992	[number]	k4	1992
R	R	kA	R
jako	jako	k8xS	jako
Helena	Helena	k1gFnSc1	Helena
1992	[number]	k4	1992
Trhala	trhat	k5eAaImAgFnS	trhat
fialky	fialka	k1gFnPc4	fialka
dynamitem	dynamit	k1gInSc7	dynamit
-	-	kIx~	-
zmrzlinářka	zmrzlinářka	k1gFnSc1	zmrzlinářka
2004	[number]	k4	2004
Kameňák	Kameňák	k?	Kameňák
2	[number]	k4	2
-	-	kIx~	-
Doktorka	doktorka	k1gFnSc1	doktorka
Helena	Helena	k1gFnSc1	Helena
2006	[number]	k4	2006
Susedia	Susedium	k1gNnSc2	Susedium
(	(	kIx(	(
<g/>
slovenský	slovenský	k2eAgMnSc1d1	slovenský
TV	TV	kA	TV
seriál	seriál	k1gInSc4	seriál
<g/>
)	)	kIx)	)
-	-	kIx~	-
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g />
.	.	kIx.	.
</s>
<s>
2009	[number]	k4	2009
Comeback	Comeback	k1gInSc1	Comeback
...	...	k?	...
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
2010	[number]	k4	2010
Na	na	k7c6	na
vlásku	vlásek	k1gInSc6	vlásek
-	-	kIx~	-
dabing	dabing	k1gInSc4	dabing
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
Gothel	Gothel	k1gInSc4	Gothel
2013	[number]	k4	2013
Gympl	gympl	k1gInSc1	gympl
s	s	k7c7	s
(	(	kIx(	(
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
učením	učení	k1gNnSc7	učení
omezeným	omezený	k2eAgNnSc7d1	omezené
(	(	kIx(	(
<g/>
český	český	k2eAgMnSc1d1	český
TV	TV	kA	TV
seriál	seriál	k1gInSc4	seriál
<g/>
)	)	kIx)	)
-	-	kIx~	-
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
2014	[number]	k4	2014
G.	G.	kA	G.
<g/>
Jedna	jeden	k4xCgFnSc1	jeden
<g/>
:	:	kIx,	:
Prvni	Prvn	k1gMnPc1	Prvn
stávka	stávka	k1gFnSc1	stávka
-	-	kIx~	-
vypravěč	vypravěč	k1gMnSc1	vypravěč
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
dabing	dabing	k1gInSc1	dabing
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
<g />
.	.	kIx.	.
</s>
<s>
Alenka	Alenka	k1gFnSc1	Alenka
-	-	kIx~	-
dívka	dívka	k1gFnSc1	dívka
ktéra	ktéra	k1gFnSc1	ktéra
se	se	k3xPyFc4	se
nestane	stanout	k5eNaPmIp3nS	stanout
-	-	kIx~	-
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc4	ten
tehdy	tehdy	k6eAd1	tehdy
padal	padat	k5eAaImAgInS	padat
déšť	déšť	k1gInSc1	déšť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlas	hlas	k1gInSc4	hlas
hrát	hrát	k5eAaImF	hrát
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
dabing	dabing	k1gInSc1	dabing
<g/>
)	)	kIx)	)
1992	[number]	k4	1992
Bídníci	bídník	k1gMnPc1	bídník
...	...	k?	...
Fantine	Fantin	k1gInSc5	Fantin
2005	[number]	k4	2005
Cats	Cats	k1gInSc1	Cats
...	...	k?	...
Grizabella	Grizabella	k1gFnSc1	Grizabella
2006	[number]	k4	2006
Hello	Hello	k1gNnSc4	Hello
Dolly	Dolla	k1gFnSc2	Dolla
...	...	k?	...
<g/>
Dolly	Dolla	k1gFnSc2	Dolla
Leviová	Leviový	k2eAgFnSc1d1	Leviová
2009	[number]	k4	2009
Mona	Mon	k2eAgFnSc1d1	Mona
Lisa	Lisa	k1gFnSc1	Lisa
...	...	k?	...
<g/>
Caterina	Caterina	k1gFnSc1	Caterina
de	de	k?	de
<g/>
́	́	k?	́
Gherardini	Gherardin	k2eAgMnPc1d1	Gherardin
-	-	kIx~	-
matka	matka	k1gFnSc1	matka
Mony	Mona	k1gFnSc2	Mona
Lisy	Lisa	k1gFnSc2	Lisa
2010	[number]	k4	2010
Baron	baron	k1gMnSc1	baron
Prášil	Prášil	k1gMnSc1	Prášil
...	...	k?	...
<g/>
Evelína	Evelína	k1gFnSc1	Evelína
2015	[number]	k4	2015
Mamma	Mamma	k1gFnSc1	Mamma
Mia	Mia	k1gFnSc1	Mia
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
Donna	donna	k1gFnSc1	donna
</s>
