<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
International	International	k1gFnSc1	International
Ice	Ice	k1gFnSc2	Ice
Hockey	Hockea	k1gFnSc2	Hockea
Federation	Federation	k1gInSc1	Federation
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
IIHF	IIHF	kA	IIHF
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
federace	federace	k1gFnSc1	federace
národních	národní	k2eAgFnPc2d1	národní
hokejových	hokejový	k2eAgFnPc2d1	hokejová
asociací	asociace	k1gFnPc2	asociace
řídící	řídící	k2eAgNnSc4d1	řídící
sportovní	sportovní	k2eAgNnSc4d1	sportovní
odvětví	odvětví	k1gNnSc4	odvětví
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
a	a	k8xC	a
inline	inlinout	k5eAaPmIp3nS	inlinout
hokej	hokej	k1gInSc4	hokej
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
i	i	k8xC	i
ženy	žena	k1gFnPc4	žena
<g/>
,	,	kIx,	,
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Curychu	Curych	k1gInSc6	Curych
<g/>
.	.	kIx.	.
</s>
