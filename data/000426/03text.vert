<s>
Martina	Martina	k1gFnSc1	Martina
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1987	[number]	k4	1987
Nové	Nová	k1gFnSc2	Nová
Město	město	k1gNnSc1	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
rychlobruslařka	rychlobruslařka	k1gFnSc1	rychlobruslařka
specializující	specializující	k2eAgFnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
tratě	trať	k1gFnPc4	trať
<g/>
,	,	kIx,	,
trojnásobná	trojnásobný	k2eAgFnSc1d1	trojnásobná
olympijská	olympijský	k2eAgFnSc1d1	olympijská
vítězka	vítězka	k1gFnSc1	vítězka
a	a	k8xC	a
několikanásobná	několikanásobný	k2eAgFnSc1d1	několikanásobná
mistryně	mistryně	k1gFnSc1	mistryně
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
i	i	k8xC	i
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
držitelka	držitelka	k1gFnSc1	držitelka
několika	několik	k4yIc2	několik
juniorských	juniorský	k2eAgMnPc2d1	juniorský
i	i	k8xC	i
seniorských	seniorský	k2eAgMnPc2d1	seniorský
světových	světový	k2eAgMnPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Úspěchů	úspěch	k1gInPc2	úspěch
také	také	k9	také
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
cyklistice	cyklistika	k1gFnSc6	cyklistika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
15	[number]	k4	15
medailí	medaile	k1gFnPc2	medaile
z	z	k7c2	z
mistrovství	mistrovství	k1gNnSc2	mistrovství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
získala	získat	k5eAaPmAgFnS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
hrávala	hrávat	k5eAaImAgFnS	hrávat
basketbal	basketbal	k1gInSc4	basketbal
<g/>
,	,	kIx,	,
k	k	k7c3	k
rychlobruslení	rychlobruslení	k1gNnSc3	rychlobruslení
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
11	[number]	k4	11
letech	léto	k1gNnPc6	léto
přivedl	přivést	k5eAaPmAgMnS	přivést
trenér	trenér	k1gMnSc1	trenér
Petr	Petr	k1gMnSc1	Petr
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
svěřenkyní	svěřenkyně	k1gFnSc7	svěřenkyně
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
scéně	scéna	k1gFnSc6	scéna
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
mistrovství	mistrovství	k1gNnSc3	mistrovství
světa	svět	k1gInSc2	svět
juniorů	junior	k1gMnPc2	junior
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zajela	zajet	k5eAaPmAgFnS	zajet
první	první	k4xOgInPc4	první
závody	závod	k1gInPc4	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
;	;	kIx,	;
do	do	k7c2	do
elitní	elitní	k2eAgFnSc2d1	elitní
divize	divize	k1gFnSc2	divize
poháru	pohár	k1gInSc2	pohár
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Přelomem	přelom	k1gInSc7	přelom
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
kariéře	kariéra	k1gFnSc6	kariéra
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
sezóna	sezóna	k1gFnSc1	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
Zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
5000	[number]	k4	5000
m	m	kA	m
skončila	skončit	k5eAaPmAgFnS	skončit
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
juniorů	junior	k1gMnPc2	junior
získala	získat	k5eAaPmAgFnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
její	její	k3xOp3gNnSc1	její
sbírka	sbírka	k1gFnSc1	sbírka
medailí	medaile	k1gFnPc2	medaile
výrazně	výrazně	k6eAd1	výrazně
narostla	narůst	k5eAaPmAgFnS	narůst
-	-	kIx~	-
z	z	k7c2	z
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
si	se	k3xPyFc3	se
postupně	postupně	k6eAd1	postupně
přivezla	přivézt	k5eAaPmAgFnS	přivézt
pět	pět	k4xCc4	pět
zlatých	zlatý	k2eAgFnPc2d1	zlatá
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
stříbrné	stříbrná	k1gFnPc4	stříbrná
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
bronzové	bronzový	k2eAgFnPc4d1	bronzová
<g/>
,	,	kIx,	,
na	na	k7c6	na
mistrovstvích	mistrovství	k1gNnPc6	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tratích	trať	k1gFnPc6	trať
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
dvanáct	dvanáct	k4xCc4	dvanáct
zlatých	zlatý	k2eAgFnPc2d1	zlatá
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
stříbrné	stříbrná	k1gFnPc4	stříbrná
<g/>
,	,	kIx,	,
na	na	k7c6	na
mistrovstvích	mistrovství	k1gNnPc6	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
tři	tři	k4xCgInPc4	tři
zlaté	zlatý	k1gInPc4	zlatý
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
stříbrnou	stříbrná	k1gFnSc4	stříbrná
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
3	[number]	k4	3
a	a	k8xC	a
5	[number]	k4	5
km	km	kA	km
<g/>
,	,	kIx,	,
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
1500	[number]	k4	1500
m	m	kA	m
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgFnSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
dobruslila	dobruslit	k5eAaImAgFnS	dobruslit
na	na	k7c4	na
distanci	distance	k1gFnSc4	distance
3000	[number]	k4	3000
m	m	kA	m
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
<g/>
,	,	kIx,	,
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
obhájila	obhájit	k5eAaPmAgFnS	obhájit
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
v	v	k7c6	v
nejdelším	dlouhý	k2eAgMnSc6d3	nejdelší
<g/>
,	,	kIx,	,
pětikilometrovém	pětikilometrový	k2eAgInSc6d1	pětikilometrový
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
rovněž	rovněž	k9	rovněž
vládne	vládnout	k5eAaImIp3nS	vládnout
Světovému	světový	k2eAgInSc3d1	světový
poháru	pohár	k1gInSc3	pohár
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
celkové	celkový	k2eAgNnSc1d1	celkové
pořadí	pořadí	k1gNnSc1	pořadí
již	již	k6eAd1	již
desetkrát	desetkrát	k6eAd1	desetkrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
a	a	k8xC	a
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
českým	český	k2eAgMnSc7d1	český
Sportovcem	sportovec	k1gMnSc7	sportovec
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
Oscara	Oscar	k1gMnSc4	Oscar
Mathisena	Mathisen	k2eAgFnSc1d1	Mathisena
<g/>
.	.	kIx.	.
</s>
<s>
Sportu	sport	k1gInSc2	sport
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
hrála	hrát	k5eAaImAgFnS	hrát
basketbal	basketbal	k1gInSc4	basketbal
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
BK	BK	kA	BK
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
věnovat	věnovat	k5eAaImF	věnovat
rychlobruslení	rychlobruslení	k1gNnSc4	rychlobruslení
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
trenér	trenér	k1gMnSc1	trenér
Petr	Petr	k1gMnSc1	Petr
Novák	Novák	k1gMnSc1	Novák
dohodl	dohodnout	k5eAaPmAgMnS	dohodnout
s	s	k7c7	s
několika	několik	k4yIc7	několik
rodiči	rodič	k1gMnPc7	rodič
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
vlastního	vlastní	k2eAgInSc2d1	vlastní
Bruslařského	bruslařský	k2eAgInSc2d1	bruslařský
klubu	klub	k1gInSc2	klub
Žďár	Žďár	k1gInSc1	Žďár
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
pozdější	pozdní	k2eAgInSc1d2	pozdější
Novákův	Novákův	k2eAgInSc1d1	Novákův
NOVIS	NOVIS	kA	NOVIS
Team	team	k1gInSc1	team
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
svěřenkyní	svěřenkyně	k1gFnSc7	svěřenkyně
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
11	[number]	k4	11
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc4	její
matka	matka	k1gFnSc1	matka
Eva	Eva	k1gFnSc1	Eva
spravovala	spravovat	k5eAaImAgFnS	spravovat
pro	pro	k7c4	pro
Novákovu	Novákův	k2eAgFnSc4d1	Novákova
manželku	manželka	k1gFnSc4	manželka
učetnictví	učetnictví	k1gNnSc2	učetnictví
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
dětmi	dítě	k1gFnPc7	dítě
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
20	[number]	k4	20
<g/>
)	)	kIx)	)
začala	začít	k5eAaPmAgFnS	začít
trénovat	trénovat	k5eAaImF	trénovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
na	na	k7c6	na
ledě	led	k1gInSc6	led
dráhy	dráha	k1gFnSc2	dráha
ve	v	k7c6	v
Svratce	Svratka	k1gFnSc6	Svratka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
a	a	k8xC	a
na	na	k7c6	na
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
bruslích	brusle	k1gFnPc6	brusle
<g/>
.	.	kIx.	.
</s>
<s>
Pomohly	pomoct	k5eAaPmAgFnP	pomoct
také	také	k9	také
netradiční	tradiční	k2eNgFnPc1d1	netradiční
Novákovy	Novákův	k2eAgFnPc1d1	Novákova
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
speciální	speciální	k2eAgNnSc1d1	speciální
prkno	prkno	k1gNnSc1	prkno
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
Milanem	Milan	k1gMnSc7	Milan
trénovala	trénovat	k5eAaImAgFnS	trénovat
doma	doma	k6eAd1	doma
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
(	(	kIx(	(
<g/>
klouzání	klouzání	k1gNnSc1	klouzání
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
k	k	k7c3	k
druhé	druhý	k4xOgFnSc2	druhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2002	[number]	k4	2002
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
juniorů	junior	k1gMnPc2	junior
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
na	na	k7c6	na
otevřené	otevřený	k2eAgFnSc6d1	otevřená
dráze	dráha	k1gFnSc6	dráha
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Klobensteinu	Klobenstein	k1gInSc6	Klobenstein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mini-čtyřboji	mini-čtyřboj	k1gInSc6	mini-čtyřboj
zde	zde	k6eAd1	zde
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
z	z	k7c2	z
50	[number]	k4	50
závodnic	závodnice	k1gFnPc2	závodnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Marcelou	Marcela	k1gFnSc7	Marcela
Kramárovou	Kramárův	k2eAgFnSc7d1	Kramárův
a	a	k8xC	a
Lucií	Lucie	k1gFnSc7	Lucie
Haselbergerovou	Haselbergerův	k2eAgFnSc7d1	Haselbergerův
jela	jet	k5eAaImAgFnS	jet
také	také	k9	také
ve	v	k7c6	v
stíhacím	stíhací	k2eAgInSc6d1	stíhací
závodě	závod	k1gInSc6	závod
družstev	družstvo	k1gNnPc2	družstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
skončily	skončit	k5eAaPmAgInP	skončit
poslední	poslední	k2eAgInPc4d1	poslední
<g/>
,	,	kIx,	,
dvanácté	dvanáctý	k4xOgFnSc6	dvanáctý
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
představila	představit	k5eAaPmAgFnS	představit
na	na	k7c6	na
závodech	závod	k1gInPc6	závod
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2002	[number]	k4	2002
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Erfurtu	Erfurt	k1gInSc6	Erfurt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odjela	odjet	k5eAaPmAgFnS	odjet
všechny	všechen	k3xTgInPc4	všechen
závody	závod	k1gInPc4	závod
čtyřboje	čtyřboj	k1gInSc2	čtyřboj
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
36	[number]	k4	36
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgNnSc2	první
seniorského	seniorský	k2eAgNnSc2d1	seniorské
mistrovství	mistrovství	k1gNnSc2	mistrovství
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
Heerenveenu	Heerenveen	k1gInSc6	Heerenveen
konalo	konat	k5eAaImAgNnS	konat
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
odjela	odjet	k5eAaPmAgFnS	odjet
pouze	pouze	k6eAd1	pouze
první	první	k4xOgInSc4	první
závod	závod	k1gInSc4	závod
na	na	k7c4	na
500	[number]	k4	500
m	m	kA	m
<g/>
,	,	kIx,	,
k	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
nenastoupila	nastoupit	k5eNaPmAgNnP	nastoupit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
na	na	k7c6	na
juniorském	juniorský	k2eAgInSc6d1	juniorský
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
(	(	kIx(	(
<g/>
34	[number]	k4	34
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
finále	finále	k1gNnSc6	finále
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
závodila	závodit	k5eAaImAgFnS	závodit
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
divizi	divize	k1gFnSc6	divize
B	B	kA	B
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
se	se	k3xPyFc4	se
také	také	k9	také
seniorského	seniorský	k2eAgNnSc2d1	seniorské
Mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
juniorského	juniorský	k2eAgNnSc2d1	juniorské
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2004	[number]	k4	2004
ale	ale	k8xC	ale
již	již	k6eAd1	již
startovala	startovat	k5eAaBmAgFnS	startovat
v	v	k7c6	v
elitní	elitní	k2eAgFnSc6d1	elitní
divizi	divize	k1gFnSc6	divize
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
3	[number]	k4	3
km	km	kA	km
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
5000	[number]	k4	5000
m	m	kA	m
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
divizi	divize	k1gFnSc6	divize
B	B	kA	B
třetí	třetí	k4xOgFnSc3	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
úspěšnost	úspěšnost	k1gFnSc1	úspěšnost
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
se	se	k3xPyFc4	se
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
i	i	k9	i
na	na	k7c6	na
seniorském	seniorský	k2eAgInSc6d1	seniorský
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončila	skončit	k5eAaPmAgFnS	skončit
šestnáctá	šestnáctý	k4xOgFnSc1	šestnáctý
(	(	kIx(	(
<g/>
3	[number]	k4	3
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
dvanáctá	dvanáctý	k4xOgFnSc1	dvanáctý
(	(	kIx(	(
<g/>
5	[number]	k4	5
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
své	svůj	k3xOyFgMnPc4	svůj
zlepšující	zlepšující	k2eAgFnSc1d1	zlepšující
se	se	k3xPyFc4	se
výkony	výkon	k1gInPc1	výkon
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
3	[number]	k4	3
km	km	kA	km
již	již	k6eAd1	již
pravidelně	pravidelně	k6eAd1	pravidelně
startovala	startovat	k5eAaBmAgFnS	startovat
v	v	k7c6	v
elitní	elitní	k2eAgFnSc6d1	elitní
skupině	skupina	k1gFnSc6	skupina
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
5000	[number]	k4	5000
m	m	kA	m
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
sedmé	sedmý	k4xOgFnSc6	sedmý
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Přelomovým	přelomový	k2eAgNnSc7d1	přelomové
obdobím	období	k1gNnSc7	období
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
stala	stát	k5eAaPmAgFnS	stát
sezóna	sezóna	k1gFnSc1	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodech	závod	k1gInPc6	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
se	se	k3xPyFc4	se
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
pravidelně	pravidelně	k6eAd1	pravidelně
umísťovala	umísťovat	k5eAaImAgFnS	umísťovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2006	[number]	k4	2006
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pětikilometrovou	pětikilometrový	k2eAgFnSc4d1	pětikilometrová
trať	trať	k1gFnSc4	trať
zajela	zajet	k5eAaPmAgFnS	zajet
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
závodnic	závodnice	k1gFnPc2	závodnice
nejrychleji	rychle	k6eAd3	rychle
a	a	k8xC	a
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
3000	[number]	k4	3000
m	m	kA	m
skončila	skončit	k5eAaPmAgFnS	skončit
třetí	třetí	k4xOgFnSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2006	[number]	k4	2006
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
poprvé	poprvé	k6eAd1	poprvé
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
Zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
k	k	k7c3	k
medailím	medaile	k1gFnPc3	medaile
ale	ale	k8xC	ale
měla	mít	k5eAaImAgNnP	mít
velmi	velmi	k6eAd1	velmi
blízko	blízko	k7c2	blízko
<g/>
:	:	kIx,	:
3	[number]	k4	3
km	km	kA	km
-	-	kIx~	-
sedmé	sedmý	k4xOgNnSc4	sedmý
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
5	[number]	k4	5
km	km	kA	km
-	-	kIx~	-
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
medaili	medaile	k1gFnSc4	medaile
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
juniorském	juniorský	k2eAgInSc6d1	juniorský
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
této	tento	k3xDgFnSc2	tento
sezóny	sezóna	k1gFnSc2	sezóna
rovněž	rovněž	k9	rovněž
zajela	zajet	k5eAaPmAgFnS	zajet
několik	několik	k4yIc4	několik
světových	světový	k2eAgInPc2d1	světový
juniorských	juniorský	k2eAgInPc2d1	juniorský
rekordů	rekord	k1gInPc2	rekord
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
držitelkou	držitelka	k1gFnSc7	držitelka
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
turínském	turínský	k2eAgInSc6d1	turínský
úspěchu	úspěch	k1gInSc6	úspěch
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
postavení	postavení	k1gNnSc4	postavení
rychlobruslařské	rychlobruslařský	k2eAgFnSc2d1	rychlobruslařská
haly	hala	k1gFnSc2	hala
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgNnP	být
zmiňována	zmiňován	k2eAgNnPc1d1	zmiňováno
místa	místo	k1gNnPc1	místo
jako	jako	k8xC	jako
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
či	či	k8xC	či
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
plánů	plán	k1gInPc2	plán
měl	mít	k5eAaImAgInS	mít
ovál	ovál	k1gInSc1	ovál
vyrůst	vyrůst	k5eAaPmF	vyrůst
ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
Oseku	Osek	k1gInSc6	Osek
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
i	i	k8xC	i
trenér	trenér	k1gMnSc1	trenér
Novák	Novák	k1gMnSc1	Novák
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
však	však	k9	však
byl	být	k5eAaImAgInS	být
dlouho	dlouho	k6eAd1	dlouho
odkládán	odkládat	k5eAaImNgInS	odkládat
<g/>
,	,	kIx,	,
až	až	k9	až
nové	nový	k2eAgNnSc4d1	nové
vedení	vedení	k1gNnSc4	vedení
obce	obec	k1gFnSc2	obec
zvolené	zvolený	k2eAgFnSc2d1	zvolená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
jej	on	k3xPp3gMnSc4	on
zcela	zcela	k6eAd1	zcela
zastavilo	zastavit	k5eAaPmAgNnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
již	již	k9	již
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
Světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
<g/>
,	,	kIx,	,
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
z	z	k7c2	z
šesti	šest	k4xCc2	šest
závodů	závod	k1gInPc2	závod
a	a	k8xC	a
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
hodnocení	hodnocení	k1gNnSc6	hodnocení
jej	on	k3xPp3gNnSc2	on
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Přišly	přijít	k5eAaPmAgFnP	přijít
ještě	ještě	k9	ještě
další	další	k2eAgInPc4d1	další
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
:	:	kIx,	:
získala	získat	k5eAaPmAgFnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
medaile	medaile	k1gFnPc4	medaile
z	z	k7c2	z
téhož	týž	k3xTgInSc2	týž
kovu	kov	k1gInSc2	kov
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
3	[number]	k4	3
a	a	k8xC	a
5	[number]	k4	5
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2007	[number]	k4	2007
navíc	navíc	k6eAd1	navíc
překonala	překonat	k5eAaPmAgFnS	překonat
seniorské	seniorský	k2eAgInPc4d1	seniorský
světové	světový	k2eAgInPc4d1	světový
rekordy	rekord	k1gInPc4	rekord
na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
5	[number]	k4	5
a	a	k8xC	a
10	[number]	k4	10
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Medailová	medailový	k2eAgFnSc1d1	medailová
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
"	"	kIx"	"
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
předních	přední	k2eAgNnPc2d1	přední
umístění	umístění	k1gNnSc2	umístění
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
zlatá	zlatý	k2eAgFnSc1d1	zlatá
medaile	medaile	k1gFnSc1	medaile
na	na	k7c4	na
5000	[number]	k4	5000
m	m	kA	m
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
a	a	k8xC	a
stříbrná	stříbrnat	k5eAaImIp3nS	stříbrnat
na	na	k7c4	na
3000	[number]	k4	3000
m	m	kA	m
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
medaile	medaile	k1gFnPc4	medaile
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
zlatou	zlatá	k1gFnSc4	zlatá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
díky	díky	k7c3	díky
vítězství	vítězství	k1gNnSc3	vítězství
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
závodu	závod	k1gInSc6	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
vyhrálo	vyhrát	k5eAaPmAgNnS	vyhrát
družstvo	družstvo	k1gNnSc1	družstvo
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
-	-	kIx~	-
Jirků	Jirků	k1gFnSc1	Jirků
-	-	kIx~	-
Erbanová	Erbanová	k1gFnSc1	Erbanová
celkové	celkový	k2eAgNnSc1d1	celkové
hodnocení	hodnocení	k1gNnSc1	hodnocení
soutěže	soutěž	k1gFnSc2	soutěž
ve	v	k7c6	v
stíhacím	stíhací	k2eAgInSc6d1	stíhací
závodě	závod	k1gInSc6	závod
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
poprvé	poprvé	k6eAd1	poprvé
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pro	pro	k7c4	pro
ten	ten	k3xDgInSc4	ten
rok	rok	k1gInSc4	rok
nejvšestranější	všestraný	k2eAgFnSc7d3	všestraný
rychlobruslařkou	rychlobruslařka	k1gFnSc7	rychlobruslařka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
několikrát	několikrát	k6eAd1	několikrát
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
příčkách	příčka	k1gFnPc6	příčka
i	i	k8xC	i
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
1500	[number]	k4	1500
m	m	kA	m
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
příliš	příliš	k6eAd1	příliš
neprosazovala	prosazovat	k5eNaImAgFnS	prosazovat
a	a	k8xC	a
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
distanci	distance	k1gFnSc6	distance
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
také	také	k9	také
na	na	k7c6	na
příležitostných	příležitostný	k2eAgNnPc6d1	příležitostné
mistrovstvích	mistrovství	k1gNnPc6	mistrovství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
-	-	kIx~	-
v	v	k7c6	v
ročnících	ročník	k1gInPc6	ročník
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
nádrž	nádrž	k1gFnSc1	nádrž
Pilská	Pilská	k1gFnSc1	Pilská
u	u	k7c2	u
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
)	)	kIx)	)
a	a	k8xC	a
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
šampionát	šampionát	k1gInSc1	šampionát
konal	konat	k5eAaImAgInS	konat
na	na	k7c6	na
rybníku	rybník	k1gInSc6	rybník
Černý	Černý	k1gMnSc1	Černý
nedaleko	nedaleko	k7c2	nedaleko
Hlinska	Hlinsko	k1gNnSc2	Hlinsko
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
výsledky	výsledek	k1gInPc1	výsledek
ale	ale	k9	ale
byly	být	k5eAaImAgInP	být
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
očekávání	očekávání	k1gNnSc4	očekávání
fanoušků	fanoušek	k1gMnPc2	fanoušek
i	i	k8xC	i
odborníků	odborník	k1gMnPc2	odborník
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2010	[number]	k4	2010
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
akci	akce	k1gFnSc4	akce
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
připravovala	připravovat	k5eAaImAgFnS	připravovat
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
v	v	k7c6	v
Klobensteinu	Klobenstein	k1gInSc6	Klobenstein
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Calgary	Calgary	k1gNnSc6	Calgary
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
tlak	tlak	k1gInSc4	tlak
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
hned	hned	k6eAd1	hned
první	první	k4xOgInSc4	první
závod	závod	k1gInSc4	závod
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
a	a	k8xC	a
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
3000	[number]	k4	3000
m	m	kA	m
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
několika	několik	k4yIc3	několik
možným	možný	k2eAgFnPc3d1	možná
favoritkám	favoritka	k1gFnPc3	favoritka
(	(	kIx(	(
<g/>
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
distanci	distance	k1gFnSc6	distance
několikrát	několikrát	k6eAd1	několikrát
porazila	porazit	k5eAaPmAgFnS	porazit
Stephanie	Stephanie	k1gFnSc1	Stephanie
Beckertová	Beckertová	k1gFnSc1	Beckertová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
1500	[number]	k4	1500
m	m	kA	m
<g/>
,	,	kIx,	,
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepatřila	patřit	k5eNaImAgFnS	patřit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
adeptkám	adeptka	k1gFnPc3	adeptka
na	na	k7c4	na
cenný	cenný	k2eAgInSc4d1	cenný
kov	kov	k1gInSc4	kov
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
trati	trať	k1gFnSc6	trať
5000	[number]	k4	5000
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
byla	být	k5eAaImAgFnS	být
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
suverénní	suverénní	k2eAgInSc1d1	suverénní
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nejočekávanější	očekávaný	k2eAgFnSc7d3	nejočekávanější
favoritkou	favoritka	k1gFnSc7	favoritka
<g/>
,	,	kIx,	,
situaci	situace	k1gFnSc3	situace
ale	ale	k8xC	ale
rychlými	rychlý	k2eAgInPc7d1	rychlý
časy	čas	k1gInPc7	čas
zkomplikovaly	zkomplikovat	k5eAaPmAgInP	zkomplikovat
Hughesová	Hughesový	k2eAgFnSc1d1	Hughesová
a	a	k8xC	a
Beckertová	Beckertový	k2eAgFnSc1d1	Beckertová
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
česká	český	k2eAgFnSc1d1	Česká
závodnice	závodnice	k1gFnSc1	závodnice
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
jízdě	jízda	k1gFnSc6	jízda
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
druhou	druhý	k4xOgFnSc4	druhý
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
dvou	dva	k4xCgFnPc2	dva
zlatých	zlatý	k2eAgFnPc2d1	zlatá
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
bronzové	bronzový	k2eAgFnSc2d1	bronzová
olympijské	olympijský	k2eAgFnSc2d1	olympijská
medaile	medaile	k1gFnSc2	medaile
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
nejúspěšnější	úspěšný	k2eAgFnSc7d3	nejúspěšnější
českou	český	k2eAgFnSc7d1	Česká
sportovkyní	sportovkyně	k1gFnSc7	sportovkyně
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ZOH	ZOH	kA	ZOH
a	a	k8xC	a
Jacques	Jacques	k1gMnSc1	Jacques
Rogge	Rogge	k1gFnSc1	Rogge
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
MOV	MOV	kA	MOV
<g/>
,	,	kIx,	,
ji	on	k3xPp3gFnSc4	on
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
královnou	královna	k1gFnSc7	královna
rychlobruslení	rychlobruslení	k1gNnSc2	rychlobruslení
s	s	k7c7	s
elegantním	elegantní	k2eAgInSc7d1	elegantní
stylem	styl	k1gInSc7	styl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
se	se	k3xPyFc4	se
také	také	k9	také
prosadila	prosadit	k5eAaPmAgFnS	prosadit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obhájila	obhájit	k5eAaPmAgFnS	obhájit
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
trenérem	trenér	k1gMnSc7	trenér
Petrem	Petr	k1gMnSc7	Petr
Novákem	Novák	k1gMnSc7	Novák
opustili	opustit	k5eAaPmAgMnP	opustit
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc6	duben
2010	[number]	k4	2010
armádní	armádní	k2eAgInSc1d1	armádní
klub	klub	k1gInSc1	klub
Dukla	Dukla	k1gFnSc1	Dukla
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působili	působit	k5eAaImAgMnP	působit
šest	šest	k4xCc1	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
odešli	odejít	k5eAaPmAgMnP	odejít
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
rychlobruslaři	rychlobruslař	k1gMnPc1	rychlobruslař
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Karolína	Karolína	k1gFnSc1	Karolína
Erbanová	Erbanová	k1gFnSc1	Erbanová
nebo	nebo	k8xC	nebo
Milan	Milan	k1gMnSc1	Milan
Sáblík	Sáblík	k1gMnSc1	Sáblík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
začali	začít	k5eAaPmAgMnP	začít
pracovat	pracovat	k5eAaImF	pracovat
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Centra	centrum	k1gNnSc2	centrum
sportu	sport	k1gInSc2	sport
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
mítinku	mítink	k1gInSc6	mítink
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
v	v	k7c4	v
Heerenvenu	Heerenven	k2eAgFnSc4d1	Heerenven
nemohla	moct	k5eNaImAgFnS	moct
nastoupit	nastoupit	k5eAaPmF	nastoupit
kvůli	kvůli	k7c3	kvůli
zdravotním	zdravotní	k2eAgFnPc3d1	zdravotní
potížím	potíž	k1gFnPc3	potíž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dvou	dva	k4xCgInPc6	dva
závodech	závod	k1gInPc6	závod
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
medailových	medailový	k2eAgFnPc6d1	medailová
pozicích	pozice	k1gFnPc6	pozice
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ani	ani	k8xC	ani
jednou	jednou	k6eAd1	jednou
nevyhrála	vyhrát	k5eNaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
obhájila	obhájit	k5eAaPmAgFnS	obhájit
na	na	k7c6	na
otevřené	otevřený	k2eAgFnSc6d1	otevřená
dráze	dráha	k1gFnSc6	dráha
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Klobensteinu	Klobenstein	k1gInSc6	Klobenstein
titul	titul	k1gInSc1	titul
mistryně	mistryně	k1gFnSc2	mistryně
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
<g/>
,	,	kIx,	,
když	když	k8xS	když
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
3000	[number]	k4	3000
a	a	k8xC	a
5000	[number]	k4	5000
m.	m.	k?	m.
První	první	k4xOgInSc4	první
triumf	triumf	k1gInSc4	triumf
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
si	se	k3xPyFc3	se
připsala	připsat	k5eAaPmAgFnS	připsat
na	na	k7c6	na
mítinku	mítink	k1gInSc6	mítink
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
tříkilometrové	tříkilometrový	k2eAgFnSc6d1	tříkilometrová
trati	trať	k1gFnSc6	trať
<g/>
,	,	kIx,	,
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
1500	[number]	k4	1500
m	m	kA	m
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
2011	[number]	k4	2011
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Calgary	Calgary	k1gNnSc6	Calgary
sice	sice	k8xC	sice
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
závod	závod	k1gInSc4	závod
na	na	k7c4	na
3000	[number]	k4	3000
m	m	kA	m
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
distancí	distance	k1gFnSc7	distance
5	[number]	k4	5
km	km	kA	km
měla	mít	k5eAaImAgFnS	mít
ztrátu	ztráta	k1gFnSc4	ztráta
15	[number]	k4	15
s	s	k7c7	s
na	na	k7c4	na
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
Nizozemku	Nizozemka	k1gFnSc4	Nizozemka
Ireen	Ireno	k1gNnPc2	Ireno
Wüstovou	Wüstová	k1gFnSc7	Wüstová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
Sáblíková	Sáblíkový	k2eAgNnPc4d1	Sáblíkové
čtyři	čtyři	k4xCgNnPc4	čtyři
kola	kolo	k1gNnPc4	kolo
před	před	k7c7	před
cílem	cíl	k1gInSc7	cíl
upadla	upadnout	k5eAaPmAgFnS	upadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k6eAd1	přesto
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
závodě	závod	k1gInSc6	závod
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
na	na	k7c4	na
5000	[number]	k4	5000
m	m	kA	m
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gFnSc2	Lak
City	city	k1gNnSc1	city
překonala	překonat	k5eAaPmAgFnS	překonat
vlastní	vlastní	k2eAgInPc4d1	vlastní
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
starý	starý	k2eAgInSc4d1	starý
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
tři	tři	k4xCgFnPc4	tři
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
prvnímu	první	k4xOgInSc3	první
místu	místo	k1gNnSc3	místo
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
3	[number]	k4	3
km	km	kA	km
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
finále	finále	k1gNnSc6	finále
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
v	v	k7c6	v
Heerenveenu	Heerenveen	k1gInSc6	Heerenveen
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
popáté	popáté	k4xO	popáté
za	za	k7c7	za
sebou	se	k3xPyFc7	se
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
umístění	umístění	k1gNnSc6	umístění
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
3000	[number]	k4	3000
a	a	k8xC	a
5000	[number]	k4	5000
m	m	kA	m
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
podařilo	podařit	k5eAaPmAgNnS	podařit
pouze	pouze	k6eAd1	pouze
Němce	Němka	k1gFnSc6	Němka
Gundě	Gunda	k1gFnSc6	Gunda
Niemannové-Stirnemannové	Niemannové-Stirnemannová	k1gFnSc2	Niemannové-Stirnemannová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
akci	akce	k1gFnSc6	akce
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tratích	trať	k1gFnPc6	trať
2011	[number]	k4	2011
v	v	k7c6	v
Inzellu	Inzello	k1gNnSc6	Inzello
<g/>
,	,	kIx,	,
obhájila	obhájit	k5eAaPmAgFnS	obhájit
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
šampionátu	šampionát	k1gInSc2	šampionát
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
3000	[number]	k4	3000
m	m	kA	m
<g/>
,	,	kIx,	,
když	když	k8xS	když
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
Ireen	Ireno	k1gNnPc2	Ireno
Wüstovou	Wüstová	k1gFnSc4	Wüstová
o	o	k7c4	o
51	[number]	k4	51
setin	setina	k1gFnPc2	setina
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
i	i	k9	i
na	na	k7c6	na
distanci	distance	k1gFnSc6	distance
5000	[number]	k4	5000
m	m	kA	m
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
již	již	k6eAd1	již
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
zahájila	zahájit	k5eAaPmAgFnS	zahájit
vítězstvím	vítězství	k1gNnSc7	vítězství
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
3	[number]	k4	3
km	km	kA	km
a	a	k8xC	a
dvanáctým	dvanáctý	k4xOgNnSc7	dvanáctý
místem	místo	k1gNnSc7	místo
na	na	k7c4	na
1500	[number]	k4	1500
m	m	kA	m
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
mítinku	mítink	k1gInSc6	mítink
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
Čeljabinsku	Čeljabinsk	k1gInSc6	Čeljabinsk
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
z	z	k7c2	z
tříkilometrové	tříkilometrový	k2eAgFnSc2d1	tříkilometrová
dráhy	dráha	k1gFnSc2	dráha
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
i	i	k9	i
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
mítinku	mítink	k1gInSc6	mítink
SP	SP	kA	SP
v	v	k7c6	v
Astaně	Astana	k1gFnSc6	Astana
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
na	na	k7c6	na
poloviční	poloviční	k2eAgFnSc6d1	poloviční
trati	trať	k1gFnSc6	trať
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončila	skončit	k5eAaPmAgFnS	skončit
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
závody	závod	k1gInPc1	závod
SP	SP	kA	SP
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgInP	odehrát
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
v	v	k7c6	v
Heerenveenu	Heerenveen	k1gInSc6	Heerenveen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
posedmé	posedmé	k4xO	posedmé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
mítincích	mítink	k1gInPc6	mítink
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
5	[number]	k4	5
km	km	kA	km
<g/>
,	,	kIx,	,
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
dojela	dojet	k5eAaPmAgFnS	dojet
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
1500	[number]	k4	1500
m	m	kA	m
jako	jako	k8xC	jako
pátá	pátá	k1gFnSc1	pátá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
ledna	leden	k1gInSc2	leden
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
na	na	k7c6	na
otevřené	otevřený	k2eAgFnSc6d1	otevřená
dráze	dráha	k1gFnSc6	dráha
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
potřetí	potřetí	k4xO	potřetí
za	za	k7c4	za
sebou	se	k3xPyFc7	se
třetí	třetí	k4xOgFnSc4	třetí
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
z	z	k7c2	z
evropských	evropský	k2eAgInPc2d1	evropský
šampionátů	šampionát	k1gInPc2	šampionát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zde	zde	k6eAd1	zde
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
na	na	k7c4	na
3000	[number]	k4	3000
<g/>
,	,	kIx,	,
1500	[number]	k4	1500
i	i	k9	i
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetí	třetí	k4xOgFnSc4	třetí
tříkilometrovou	tříkilometrový	k2eAgFnSc4d1	tříkilometrová
distanci	distance	k1gFnSc4	distance
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
na	na	k7c6	na
následujícím	následující	k2eAgInSc6d1	následující
mítinku	mítink	k1gInSc6	mítink
v	v	k7c6	v
Hamaru	Hamar	k1gInSc6	Hamar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
startovala	startovat	k5eAaBmAgNnP	startovat
i	i	k9	i
na	na	k7c6	na
poloviční	poloviční	k2eAgFnSc6d1	poloviční
trati	trať	k1gFnSc6	trať
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
dokončila	dokončit	k5eAaPmAgFnS	dokončit
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
součtu	součet	k1gInSc6	součet
bodů	bod	k1gInPc2	bod
dokázala	dokázat	k5eAaPmAgFnS	dokázat
porazit	porazit	k5eAaPmF	porazit
pouze	pouze	k6eAd1	pouze
Ireen	Ireen	k1gInSc4	Ireen
Wüstová	Wüstová	k1gFnSc1	Wüstová
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
následujícím	následující	k2eAgInSc6d1	následující
mítinku	mítink	k1gInSc6	mítink
v	v	k7c6	v
Heerenveenu	Heerenveen	k1gInSc6	Heerenveen
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
pětikilometrovou	pětikilometrový	k2eAgFnSc4d1	pětikilometrová
trať	trať	k1gFnSc4	trať
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
si	se	k3xPyFc3	se
s	s	k7c7	s
předstihem	předstih	k1gInSc7	předstih
zajistila	zajistit	k5eAaPmAgFnS	zajistit
šesté	šestý	k4xOgNnSc1	šestý
celkové	celkový	k2eAgNnSc1d1	celkové
vítězství	vítězství	k1gNnSc1	vítězství
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
finále	finále	k1gNnSc2	finále
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
další	další	k2eAgInSc4d1	další
víkend	víkend	k1gInSc4	víkend
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
jak	jak	k6eAd1	jak
závodu	závod	k1gInSc2	závod
na	na	k7c4	na
3000	[number]	k4	3000
m	m	kA	m
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
závodu	závod	k1gInSc2	závod
na	na	k7c6	na
poloviční	poloviční	k2eAgFnSc6d1	poloviční
trati	trať	k1gFnSc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
dojela	dojet	k5eAaPmAgFnS	dojet
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc4	její
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
výsledek	výsledek	k1gInSc4	výsledek
na	na	k7c6	na
distanci	distance	k1gFnSc6	distance
1500	[number]	k4	1500
m	m	kA	m
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
se	se	k3xPyFc4	se
celkově	celkově	k6eAd1	celkově
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
velkou	velký	k2eAgFnSc7d1	velká
akcí	akce	k1gFnSc7	akce
sezóny	sezóna	k1gFnSc2	sezóna
tradičně	tradičně	k6eAd1	tradičně
bylo	být	k5eAaImAgNnS	být
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tratích	trať	k1gFnPc6	trať
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
nizozemském	nizozemský	k2eAgInSc6d1	nizozemský
Heerenveenu	Heerenveen	k1gInSc6	Heerenveen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
3000	[number]	k4	3000
m	m	kA	m
porazila	porazit	k5eAaPmAgFnS	porazit
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
své	svůj	k3xOyFgFnPc4	svůj
tři	tři	k4xCgFnPc4	tři
největší	veliký	k2eAgFnPc4d3	veliký
soupeřky	soupeřka	k1gFnPc4	soupeřka
Beckertovou	Beckertová	k1gFnSc4	Beckertová
<g/>
,	,	kIx,	,
Wüstovou	Wüstová	k1gFnSc4	Wüstová
a	a	k8xC	a
Pechsteinovou	Pechsteinová	k1gFnSc4	Pechsteinová
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tak	tak	k6eAd1	tak
druhou	druhý	k4xOgFnSc4	druhý
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
trati	trať	k1gFnSc2	trať
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
šampionátech	šampionát	k1gInPc6	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
patnáctistovku	patnáctistovka	k1gFnSc4	patnáctistovka
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
závod	závod	k1gInSc4	závod
ale	ale	k8xC	ale
vynechala	vynechat	k5eAaPmAgFnS	vynechat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c6	na
nejdelší	dlouhý	k2eAgFnSc6d3	nejdelší
distanci	distance	k1gFnSc6	distance
5000	[number]	k4	5000
m.	m.	k?	m.
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
posledním	poslední	k2eAgInSc6d1	poslední
startu	start	k1gInSc6	start
sezóny	sezóna	k1gFnSc2	sezóna
porazila	porazit	k5eAaPmAgFnS	porazit
na	na	k7c6	na
pětikilometrové	pětikilometrový	k2eAgFnSc6d1	pětikilometrová
trati	trať	k1gFnSc6	trať
všechny	všechen	k3xTgFnPc4	všechen
soupeřky	soupeřka	k1gFnPc4	soupeřka
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tak	tak	k6eAd1	tak
na	na	k7c6	na
heerenveenském	heerenveenský	k2eAgInSc6d1	heerenveenský
šampionátu	šampionát	k1gInSc6	šampionát
druhé	druhý	k4xOgNnSc4	druhý
zlato	zlato	k1gNnSc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rychlobruslařská	rychlobruslařský	k2eAgFnSc1d1	rychlobruslařská
sezóna	sezóna	k1gFnSc1	sezóna
Martiny	Martina	k1gFnSc2	Martina
Sáblíkové	Sáblíková	k1gFnSc2	Sáblíková
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvních	první	k4xOgInPc6	první
třech	tři	k4xCgInPc6	tři
mítincích	mítink	k1gInPc6	mítink
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
průběžné	průběžný	k2eAgNnSc4d1	průběžné
pořadí	pořadí	k1gNnSc4	pořadí
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
(	(	kIx(	(
<g/>
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
závod	závod	k1gInSc4	závod
na	na	k7c4	na
5000	[number]	k4	5000
m	m	kA	m
v	v	k7c6	v
Astaně	Astana	k1gFnSc6	Astana
a	a	k8xC	a
umístila	umístit	k5eAaPmAgFnS	umístit
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
3000	[number]	k4	3000
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
rovněž	rovněž	k9	rovněž
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
patnáctistovce	patnáctistovka	k1gFnSc6	patnáctistovka
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
průběžném	průběžný	k2eAgNnSc6d1	průběžné
pořadí	pořadí	k1gNnSc6	pořadí
zůstala	zůstat	k5eAaPmAgFnS	zůstat
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
příčce	příčka	k1gFnSc6	příčka
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgNnSc6	jeden
pátém	pátý	k4xOgInSc6	pátý
a	a	k8xC	a
jednom	jeden	k4xCgInSc6	jeden
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Vánocemi	Vánoce	k1gFnPc7	Vánoce
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
starty	start	k1gInPc4	start
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
Akademického	akademický	k2eAgNnSc2d1	akademické
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Zakopanem	Zakopan	k1gInSc7	Zakopan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
všechny	všechen	k3xTgInPc4	všechen
tři	tři	k4xCgInPc4	tři
závody	závod	k1gInPc4	závod
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgFnPc2	jenž
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
-	-	kIx~	-
1500	[number]	k4	1500
m	m	kA	m
<g/>
,	,	kIx,	,
3000	[number]	k4	3000
m	m	kA	m
a	a	k8xC	a
5000	[number]	k4	5000
m.	m.	k?	m.
Kvůli	kvůli	k7c3	kvůli
zranění	zranění	k1gNnSc3	zranění
zad	záda	k1gNnPc2	záda
z	z	k7c2	z
tréninku	trénink	k1gInSc2	trénink
byl	být	k5eAaImAgInS	být
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
její	její	k3xOp3gInSc1	její
start	start	k1gInSc1	start
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgFnPc4	tento
komplikace	komplikace	k1gFnPc4	komplikace
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
na	na	k7c6	na
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
příčce	příčka	k1gFnSc6	příčka
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
ale	ale	k8xC	ale
skončila	skončit	k5eAaPmAgFnS	skončit
bez	bez	k7c2	bez
medaile	medaile	k1gFnSc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Start	start	k1gInSc1	start
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
2013	[number]	k4	2013
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
odřekla	odřeknout	k5eAaPmAgFnS	odřeknout
<g/>
,	,	kIx,	,
na	na	k7c6	na
následujících	následující	k2eAgInPc6d1	následující
dvou	dva	k4xCgInPc6	dva
mítincích	mítink	k1gInPc6	mítink
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
závodila	závodit	k5eAaImAgFnS	závodit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
Erfurtu	Erfurt	k1gInSc6	Erfurt
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
5000	[number]	k4	5000
m.	m.	k?	m.
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
Heerenveenu	Heerenveen	k1gInSc6	Heerenveen
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2013	[number]	k4	2013
zakončila	zakončit	k5eAaPmAgFnS	zakončit
seriál	seriál	k1gInSc4	seriál
šestou	šestý	k4xOgFnSc7	šestý
příčkou	příčka	k1gFnSc7	příčka
na	na	k7c4	na
3000	[number]	k4	3000
m	m	kA	m
a	a	k8xC	a
devátým	devátý	k4xOgNnSc7	devátý
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
poloviční	poloviční	k2eAgFnSc6d1	poloviční
distanci	distance	k1gFnSc6	distance
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
udržela	udržet	k5eAaPmAgFnS	udržet
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
po	po	k7c6	po
sedmé	sedmý	k4xOgFnSc6	sedmý
za	za	k7c2	za
sebou	se	k3xPyFc7	se
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
patnáctistovce	patnáctistovka	k1gFnSc6	patnáctistovka
byla	být	k5eAaImAgFnS	být
desátá	desátá	k1gFnSc1	desátá
a	a	k8xC	a
v	v	k7c6	v
Grand	grand	k1gMnSc1	grand
World	World	k1gInSc4	World
Cupu	cup	k1gInSc2	cup
šestá	šestý	k4xOgFnSc1	šestý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
akci	akce	k1gFnSc6	akce
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tratích	trať	k1gFnPc6	trať
<g/>
,	,	kIx,	,
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
tříkilometrové	tříkilometrový	k2eAgFnSc6d1	tříkilometrová
trati	trať	k1gFnSc6	trať
a	a	k8xC	a
obhájila	obhájit	k5eAaPmAgFnS	obhájit
zlato	zlato	k1gNnSc4	zlato
na	na	k7c6	na
pětikilometrové	pětikilometrový	k2eAgFnSc6d1	pětikilometrová
distanci	distance	k1gFnSc6	distance
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
zahájila	zahájit	k5eAaPmAgFnS	zahájit
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
mítinkem	mítink	k1gInSc7	mítink
Světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
v	v	k7c6	v
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
v	v	k7c6	v
Calgary	Calgary	k1gNnSc6	Calgary
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
na	na	k7c6	na
tříkilometrové	tříkilometrový	k2eAgFnSc6d1	tříkilometrová
trati	trať	k1gFnSc6	trať
a	a	k8xC	a
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
na	na	k7c4	na
poloviční	poloviční	k2eAgFnSc4d1	poloviční
distanci	distance	k1gFnSc4	distance
(	(	kIx(	(
<g/>
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
rekordu	rekord	k1gInSc6	rekord
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
podniku	podnik	k1gInSc6	podnik
SP	SP	kA	SP
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lake	k1gFnPc7	Lake
City	City	k1gFnSc2	City
sice	sice	k8xC	sice
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
závod	závod	k1gInSc4	závod
na	na	k7c4	na
3000	[number]	k4	3000
m	m	kA	m
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
patnáctistovce	patnáctistovka	k1gFnSc6	patnáctistovka
si	se	k3xPyFc3	se
přivodila	přivodit	k5eAaBmAgFnS	přivodit
zranění	zranění	k1gNnSc4	zranění
třísla	tříslo	k1gNnSc2	tříslo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohla	moct	k5eAaImAgFnS	moct
závodit	závodit	k5eAaImF	závodit
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgInPc6d1	další
dvou	dva	k4xCgInPc6	dva
mítincích	mítink	k1gInPc6	mítink
SP	SP	kA	SP
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oba	dva	k4xCgInPc1	dva
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
×	×	k?	×
3000	[number]	k4	3000
m	m	kA	m
a	a	k8xC	a
1	[number]	k4	1
<g/>
×	×	k?	×
5000	[number]	k4	5000
m	m	kA	m
<g/>
,	,	kIx,	,
dokázala	dokázat	k5eAaPmAgFnS	dokázat
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
,	,	kIx,	,
kratší	krátký	k2eAgFnPc1d2	kratší
tratě	trať	k1gFnPc1	trať
však	však	k9	však
raději	rád	k6eAd2	rád
neabsolvovala	absolvovat	k5eNaPmAgFnS	absolvovat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Vánocemi	Vánoce	k1gFnPc7	Vánoce
poprvé	poprvé	k6eAd1	poprvé
startovala	startovat	k5eAaBmAgNnP	startovat
na	na	k7c6	na
Zimní	zimní	k2eAgFnSc6d1	zimní
univerziádě	univerziáda	k1gFnSc6	univerziáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
otevřeném	otevřený	k2eAgInSc6d1	otevřený
oválu	ovál	k1gInSc6	ovál
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
tratě	trať	k1gFnPc4	trať
3	[number]	k4	3
km	km	kA	km
a	a	k8xC	a
5	[number]	k4	5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Novém	nový	k2eAgInSc6d1	nový
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
předolympijské	předolympijský	k2eAgFnSc2d1	předolympijská
přípravy	příprava	k1gFnSc2	příprava
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
startovala	startovat	k5eAaBmAgFnS	startovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
třetí	třetí	k4xOgFnSc6	třetí
olympiádě	olympiáda	k1gFnSc6	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
3000	[number]	k4	3000
m	m	kA	m
sice	sice	k8xC	sice
vítězství	vítězství	k1gNnSc2	vítězství
neobhájila	obhájit	k5eNaPmAgFnS	obhájit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
rychlému	rychlý	k2eAgInSc3d1	rychlý
času	čas	k1gInSc3	čas
získala	získat	k5eAaPmAgFnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Překonala	překonat	k5eAaPmAgFnS	překonat
ji	on	k3xPp3gFnSc4	on
pouze	pouze	k6eAd1	pouze
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
připravená	připravený	k2eAgFnSc1d1	připravená
Nizozemka	Nizozemka	k1gFnSc1	Nizozemka
Wüstová	Wüstová	k1gFnSc1	Wüstová
<g/>
,	,	kIx,	,
vítězka	vítězka	k1gFnSc1	vítězka
této	tento	k3xDgFnSc2	tento
distance	distance	k1gFnSc2	distance
na	na	k7c4	na
ZOH	ZOH	kA	ZOH
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
sezóně	sezóna	k1gFnSc6	sezóna
prokazovala	prokazovat	k5eAaImAgFnS	prokazovat
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc4d1	dobrá
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
o	o	k7c6	o
startu	start	k1gInSc6	start
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
1500	[number]	k4	1500
m	m	kA	m
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
obavám	obava	k1gFnPc3	obava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
neobnovila	obnovit	k5eNaPmAgFnS	obnovit
zranění	zranění	k1gNnSc4	zranění
třísla	tříslo	k1gNnSc2	tříslo
z	z	k7c2	z
podzimu	podzim	k1gInSc2	podzim
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
závodu	závod	k1gInSc2	závod
odhlásila	odhlásit	k5eAaPmAgFnS	odhlásit
a	a	k8xC	a
soustředila	soustředit	k5eAaPmAgFnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
<g/>
,	,	kIx,	,
pětikilometrovou	pětikilometrový	k2eAgFnSc4d1	pětikilometrová
trať	trať	k1gFnSc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
dokázala	dokázat	k5eAaPmAgFnS	dokázat
obhájit	obhájit	k5eAaPmF	obhájit
vítězství	vítězství	k1gNnSc4	vítězství
z	z	k7c2	z
předchozích	předchozí	k2eAgInPc2d1	předchozí
ZOH	ZOH	kA	ZOH
ve	v	k7c4	v
Vancouveru	Vancouvera	k1gFnSc4	Vancouvera
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
po	po	k7c6	po
prvenství	prvenství	k1gNnSc6	prvenství
snowboardistky	snowboardistka	k1gFnSc2	snowboardistka
Samkové	Samková	k1gFnSc2	Samková
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
výpravu	výprava	k1gFnSc4	výprava
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
celkově	celkově	k6eAd1	celkově
druhou	druhý	k4xOgFnSc4	druhý
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvním	první	k4xOgInSc6	první
poolympijském	poolympijský	k2eAgInSc6d1	poolympijský
mítinku	mítink	k1gInSc6	mítink
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
Inzellu	Inzell	k1gInSc6	Inzell
zajela	zajet	k5eAaPmAgFnS	zajet
šesté	šestý	k4xOgNnSc1	šestý
místo	místo	k1gNnSc1	místo
na	na	k7c6	na
distanci	distance	k1gFnSc6	distance
1500	[number]	k4	1500
m	m	kA	m
<g/>
,	,	kIx,	,
na	na	k7c6	na
dvojnásobné	dvojnásobný	k2eAgFnSc6d1	dvojnásobná
trati	trať	k1gFnSc6	trať
obsadila	obsadit	k5eAaPmAgFnS	obsadit
druhou	druhý	k4xOgFnSc4	druhý
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
SP	SP	kA	SP
v	v	k7c6	v
Heerenveenu	Heerenveen	k1gInSc6	Heerenveen
již	jenž	k3xRgFnSc4	jenž
patnáctistovku	patnáctistovka	k1gFnSc4	patnáctistovka
kvůli	kvůli	k7c3	kvůli
únavě	únava	k1gFnSc3	únava
vynechala	vynechat	k5eAaPmAgFnS	vynechat
a	a	k8xC	a
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
závodu	závod	k1gInSc6	závod
na	na	k7c4	na
3	[number]	k4	3
km	km	kA	km
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
díky	díky	k7c3	díky
čtvrtému	čtvrtý	k4xOgInSc3	čtvrtý
místu	místo	k1gNnSc3	místo
zajistila	zajistit	k5eAaPmAgFnS	zajistit
osmé	osmý	k4xOgNnSc4	osmý
celkové	celkový	k2eAgNnSc4d1	celkové
prvenství	prvenství	k1gNnSc4	prvenství
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
náročné	náročný	k2eAgFnSc6d1	náročná
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
závěrečném	závěrečný	k2eAgNnSc6d1	závěrečné
Mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
nebude	být	k5eNaImBp3nS	být
startovat	startovat	k5eAaBmF	startovat
<g/>
.	.	kIx.	.
</s>
<s>
Ročník	ročník	k1gInSc1	ročník
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
dvěma	dva	k4xCgInPc7	dva
mítinky	mítink	k1gInPc7	mítink
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
Obihiru	Obihir	k1gInSc6	Obihir
skončila	skončit	k5eAaPmAgFnS	skončit
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
v	v	k7c6	v
úvodním	úvodní	k2eAgInSc6d1	úvodní
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
3000	[number]	k4	3000
m	m	kA	m
vinou	vinou	k7c2	vinou
chyby	chyba	k1gFnSc2	chyba
při	při	k7c6	při
střídání	střídání	k1gNnSc6	střídání
drah	draha	k1gFnPc2	draha
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
kole	kolo	k1gNnSc6	kolo
těsně	těsně	k6eAd1	těsně
druhá	druhý	k4xOgFnSc1	druhý
za	za	k7c4	za
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
Wüstovou	Wüstová	k1gFnSc4	Wüstová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
ještě	ještě	k9	ještě
stíhací	stíhací	k2eAgInSc4d1	stíhací
závod	závod	k1gInSc4	závod
družstev	družstvo	k1gNnPc2	družstvo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
dojela	dojet	k5eAaPmAgFnS	dojet
s	s	k7c7	s
mladými	mladý	k2eAgFnPc7d1	mladá
závodnicemi	závodnice	k1gFnPc7	závodnice
Zdráhalovou	Zdráhalová	k1gFnSc7	Zdráhalová
a	a	k8xC	a
Kerschbaummayr	Kerschbaummayr	k1gMnSc1	Kerschbaummayr
na	na	k7c6	na
předposledním	předposlední	k2eAgInSc6d1	předposlední
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
distanci	distance	k1gFnSc6	distance
1500	[number]	k4	1500
m	m	kA	m
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
také	také	k6eAd1	také
svůj	svůj	k3xOyFgInSc4	svůj
premiérový	premiérový	k2eAgInSc4d1	premiérový
start	start	k1gInSc4	start
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
víkend	víkend	k1gInSc4	víkend
se	se	k3xPyFc4	se
závodilo	závodit	k5eAaImAgNnS	závodit
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
5000	[number]	k4	5000
m	m	kA	m
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
nejlepším	dobrý	k2eAgInSc6d3	nejlepší
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
když	když	k8xS	když
rychleji	rychle	k6eAd2	rychle
zajet	zajet	k5eAaPmF	zajet
tuto	tento	k3xDgFnSc4	tento
distanci	distance	k1gFnSc4	distance
zvládla	zvládnout	k5eAaPmAgFnS	zvládnout
pouze	pouze	k6eAd1	pouze
veteránka	veteránka	k1gFnSc1	veteránka
Claudia	Claudia	k1gFnSc1	Claudia
Pechsteinová	Pechsteinová	k1gFnSc1	Pechsteinová
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
traťovém	traťový	k2eAgInSc6d1	traťový
rekordu	rekord	k1gInSc6	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
ale	ale	k9	ale
nebyla	být	k5eNaImAgFnS	být
spokojena	spokojen	k2eAgFnSc1d1	spokojena
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
předvedeným	předvedený	k2eAgInSc7d1	předvedený
výkonem	výkon	k1gInSc7	výkon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
znamenal	znamenat	k5eAaImAgMnS	znamenat
teprve	teprve	k6eAd1	teprve
druhou	druhý	k4xOgFnSc4	druhý
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
nejdelším	dlouhý	k2eAgInSc6d3	nejdelší
závodu	závod	k1gInSc6	závod
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
se	se	k3xPyFc4	se
představila	představit	k5eAaPmAgFnS	představit
ještě	ještě	k9	ještě
na	na	k7c6	na
distanci	distance	k1gFnSc6	distance
1500	[number]	k4	1500
m	m	kA	m
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
také	také	k9	také
v	v	k7c6	v
hromadném	hromadný	k2eAgInSc6d1	hromadný
závodě	závod	k1gInSc6	závod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
díky	díky	k7c3	díky
úniku	únik	k1gInSc3	únik
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
polovině	polovina	k1gFnSc6	polovina
poprvé	poprvé	k6eAd1	poprvé
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
po	po	k7c6	po
přesunu	přesun	k1gInSc6	přesun
závodů	závod	k1gInPc2	závod
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
nevyhrála	vyhrát	k5eNaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
obsadila	obsadit	k5eAaPmAgFnS	obsadit
na	na	k7c6	na
tříkilometrové	tříkilometrový	k2eAgFnSc6d1	tříkilometrová
trati	trať	k1gFnSc6	trať
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Průběžné	průběžný	k2eAgNnSc4d1	průběžné
prvenství	prvenství	k1gNnSc4	prvenství
ve	v	k7c6	v
Světovém	světový	k2eAgInSc6d1	světový
poháru	pohár	k1gInSc6	pohár
však	však	k9	však
uhájila	uhájit	k5eAaPmAgFnS	uhájit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
netradičně	tradičně	k6eNd1	tradičně
startovala	startovat	k5eAaBmAgNnP	startovat
i	i	k9	i
na	na	k7c6	na
sprinterské	sprinterský	k2eAgFnSc6d1	sprinterská
distanci	distance	k1gFnSc6	distance
1000	[number]	k4	1000
m	m	kA	m
(	(	kIx(	(
<g/>
v	v	k7c6	v
divizi	divize	k1gFnSc6	divize
B	B	kA	B
sedmé	sedmý	k4xOgNnSc4	sedmý
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plánovaný	plánovaný	k2eAgInSc1d1	plánovaný
stíhací	stíhací	k2eAgInSc1d1	stíhací
závod	závod	k1gInSc1	závod
družstev	družstvo	k1gNnPc2	družstvo
ale	ale	k8xC	ale
český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
kvůli	kvůli	k7c3	kvůli
odpočinku	odpočinek	k1gInSc3	odpočinek
vynechal	vynechat	k5eAaPmAgInS	vynechat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
dvakrát	dvakrát	k6eAd1	dvakrát
umístila	umístit	k5eAaPmAgFnS	umístit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
desítce	desítka	k1gFnSc6	desítka
(	(	kIx(	(
<g/>
1500	[number]	k4	1500
m	m	kA	m
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
hromadný	hromadný	k2eAgInSc4d1	hromadný
start	start	k1gInSc4	start
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Heerenveenu	Heerenveen	k1gInSc6	Heerenveen
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
klasický	klasický	k2eAgInSc4d1	klasický
individuální	individuální	k2eAgInSc4d1	individuální
závod	závod	k1gInSc4	závod
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
3000	[number]	k4	3000
m	m	kA	m
porazila	porazit	k5eAaPmAgFnS	porazit
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
souboji	souboj	k1gInSc6	souboj
o	o	k7c4	o
30	[number]	k4	30
setin	setina	k1gFnPc2	setina
Wüstovou	Wüstová	k1gFnSc4	Wüstová
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemskou	nizozemský	k2eAgFnSc4d1	nizozemská
anabázi	anabáze	k1gFnSc4	anabáze
i	i	k8xC	i
sportovní	sportovní	k2eAgInSc4d1	sportovní
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
zakončila	zakončit	k5eAaPmAgFnS	zakončit
pátou	pátý	k4xOgFnSc7	pátý
příčkou	příčka	k1gFnSc7	příčka
na	na	k7c6	na
patnáctistovce	patnáctistovka	k1gFnSc6	patnáctistovka
a	a	k8xC	a
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
místem	místo	k1gNnSc7	místo
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
evropského	evropský	k2eAgInSc2d1	evropský
šampionátu	šampionát	k1gInSc2	šampionát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
v	v	k7c6	v
Čeljabinsku	Čeljabinsk	k1gInSc6	Čeljabinsk
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
přivezla	přivézt	k5eAaPmAgFnS	přivézt
stříbrnou	stříbrná	k1gFnSc4	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
nejdelší	dlouhý	k2eAgFnPc4d3	nejdelší
tratě	trať	k1gFnPc4	trať
3	[number]	k4	3
a	a	k8xC	a
5	[number]	k4	5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
mítink	mítink	k1gInSc1	mítink
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
ledna	leden	k1gInSc2	leden
a	a	k8xC	a
února	únor	k1gInSc2	únor
v	v	k7c6	v
Hamaru	Hamar	k1gInSc6	Hamar
<g/>
.	.	kIx.	.
</s>
<s>
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
zde	zde	k6eAd1	zde
dojela	dojet	k5eAaPmAgFnS	dojet
závod	závod	k1gInSc4	závod
na	na	k7c4	na
1500	[number]	k4	1500
m	m	kA	m
na	na	k7c6	na
šesté	šestý	k4xOgFnSc6	šestý
příčce	příčka	k1gFnSc6	příčka
a	a	k8xC	a
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
vítězství	vítězství	k1gNnPc4	vítězství
na	na	k7c6	na
tříkilometrové	tříkilometrový	k2eAgFnSc6d1	tříkilometrová
distanci	distance	k1gFnSc6	distance
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
vynechala	vynechat	k5eAaPmAgFnS	vynechat
kvůli	kvůli	k7c3	kvůli
nachlazení	nachlazení	k1gNnSc3	nachlazení
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tratích	trať	k1gFnPc6	trať
2015	[number]	k4	2015
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
února	únor	k1gInSc2	únor
v	v	k7c6	v
Heerenveenu	Heerenveen	k1gInSc6	Heerenveen
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
v	v	k7c4	v
úvodní	úvodní	k2eAgInSc4d1	úvodní
den	den	k1gInSc4	den
získala	získat	k5eAaPmAgFnS	získat
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
3000	[number]	k4	3000
m	m	kA	m
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
rozjížďce	rozjížďka	k1gFnSc6	rozjížďka
porazila	porazit	k5eAaPmAgFnS	porazit
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
souboji	souboj	k1gInSc6	souboj
svoji	svůj	k3xOyFgFnSc4	svůj
největší	veliký	k2eAgFnSc4d3	veliký
konkurentku	konkurentka	k1gFnSc4	konkurentka
Wüstovou	Wüstový	k2eAgFnSc4d1	Wüstová
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
světový	světový	k2eAgInSc4d1	světový
titul	titul	k1gInSc4	titul
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
i	i	k9	i
na	na	k7c6	na
nejdelší	dlouhý	k2eAgFnSc6d3	nejdelší
distanci	distance	k1gFnSc6	distance
5	[number]	k4	5
km	km	kA	km
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
musela	muset	k5eAaImAgFnS	muset
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
nečekaně	nečekaně	k6eAd1	nečekaně
velmi	velmi	k6eAd1	velmi
rychlý	rychlý	k2eAgInSc1d1	rychlý
čas	čas	k1gInSc1	čas
Carlijn	Carlijna	k1gFnPc2	Carlijna
Achtereekteové	Achtereekteová	k1gFnSc2	Achtereekteová
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
cenného	cenný	k2eAgInSc2d1	cenný
kovu	kov	k1gInSc2	kov
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
1500	[number]	k4	1500
m	m	kA	m
dělilo	dělit	k5eAaImAgNnS	dělit
jen	jen	k6eAd1	jen
pět	pět	k4xCc1	pět
setin	setina	k1gFnPc2	setina
(	(	kIx(	(
<g/>
umístila	umístit	k5eAaPmAgFnS	umístit
se	se	k3xPyFc4	se
tak	tak	k9	tak
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
při	při	k7c6	při
premiéře	premiéra	k1gFnSc6	premiéra
závodu	závod	k1gInSc2	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
skončila	skončit	k5eAaPmAgFnS	skončit
šestnáctá	šestnáctý	k4xOgFnSc1	šestnáctý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
její	její	k3xOp3gInSc1	její
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
únik	únik	k1gInSc4	únik
nebyl	být	k5eNaImAgInS	být
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvouleté	dvouletý	k2eAgFnSc6d1	dvouletá
přestávce	přestávka	k1gFnSc6	přestávka
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
představila	představit	k5eAaPmAgFnS	představit
i	i	k9	i
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
víceboji	víceboj	k1gInSc6	víceboj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
března	březen	k1gInSc2	březen
na	na	k7c6	na
vysokohorské	vysokohorský	k2eAgFnSc6d1	vysokohorská
dráze	dráha	k1gFnSc6	dráha
Olympic	Olympice	k1gFnPc2	Olympice
Oval	ovalit	k5eAaPmRp2nS	ovalit
v	v	k7c6	v
Calgary	Calgary	k1gNnSc6	Calgary
<g/>
.	.	kIx.	.
</s>
<s>
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
během	během	k7c2	během
šampionátu	šampionát	k1gInSc2	šampionát
dokázala	dokázat	k5eAaPmAgFnS	dokázat
vylepšit	vylepšit	k5eAaPmF	vylepšit
osobní	osobní	k2eAgInPc4d1	osobní
rekordy	rekord	k1gInPc4	rekord
na	na	k7c6	na
tratích	trať	k1gFnPc6	trať
500	[number]	k4	500
m	m	kA	m
<g/>
,	,	kIx,	,
1500	[number]	k4	1500
m	m	kA	m
a	a	k8xC	a
3000	[number]	k4	3000
m	m	kA	m
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
distance	distance	k1gFnSc1	distance
3000	[number]	k4	3000
m	m	kA	m
a	a	k8xC	a
5000	[number]	k4	5000
m	m	kA	m
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
předvedený	předvedený	k2eAgInSc1d1	předvedený
výkon	výkon	k1gInSc1	výkon
i	i	k8xC	i
významná	významný	k2eAgFnSc1d1	významná
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
dvanáctisekundová	dvanáctisekundový	k2eAgFnSc1d1	dvanáctisekundový
převaha	převaha	k1gFnSc1	převaha
na	na	k7c6	na
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
pětikilometrové	pětikilometrový	k2eAgFnSc6d1	pětikilometrová
trati	trať	k1gFnSc6	trať
znamenaly	znamenat	k5eAaImAgFnP	znamenat
pro	pro	k7c4	pro
Češku	Češka	k1gFnSc4	Češka
třetí	třetí	k4xOgInSc1	třetí
světový	světový	k2eAgInSc1d1	světový
vícebojařský	vícebojařský	k2eAgInSc1d1	vícebojařský
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Finále	finále	k1gNnSc1	finále
Světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
poslední	poslední	k2eAgFnSc1d1	poslední
akce	akce	k1gFnSc1	akce
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
uspořádáno	uspořádat	k5eAaPmNgNnS	uspořádat
koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
v	v	k7c6	v
Erfurtu	Erfurt	k1gInSc6	Erfurt
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
skončila	skončit	k5eAaPmAgFnS	skončit
třetí	třetí	k4xOgFnSc1	třetí
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
1500	[number]	k4	1500
m	m	kA	m
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
hodnocení	hodnocení	k1gNnSc6	hodnocení
disciplíny	disciplína	k1gFnSc2	disciplína
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
páté	pátý	k4xOgFnSc6	pátý
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
den	den	k1gInSc4	den
dvakrát	dvakrát	k6eAd1	dvakrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
:	:	kIx,	:
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
tříkilometrové	tříkilometrový	k2eAgFnSc6d1	tříkilometrová
distanci	distance	k1gFnSc6	distance
a	a	k8xC	a
o	o	k7c4	o
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
později	pozdě	k6eAd2	pozdě
také	také	k9	také
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
před	před	k7c7	před
startem	start	k1gInSc7	start
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
zajistila	zajistit	k5eAaPmAgFnS	zajistit
deváté	devátý	k4xOgNnSc4	devátý
celkové	celkový	k2eAgNnSc4d1	celkové
prvenství	prvenství	k1gNnSc4	prvenství
na	na	k7c6	na
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
tratích	trať	k1gFnPc6	trať
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
její	její	k3xOp3gFnSc1	její
jediná	jediný	k2eAgFnSc1d1	jediná
možná	možný	k2eAgFnSc1d1	možná
soupeřka	soupeřka	k1gFnSc1	soupeřka
Ireen	Irena	k1gFnPc2	Irena
Wüstová	Wüstová	k1gFnSc1	Wüstová
se	se	k3xPyFc4	se
podniku	podnik	k1gInSc3	podnik
v	v	k7c4	v
Erfurtu	Erfurta	k1gFnSc4	Erfurta
nezúčastnila	zúčastnit	k5eNaPmAgFnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hodnocení	hodnocení	k1gNnSc6	hodnocení
hromadných	hromadný	k2eAgInPc2d1	hromadný
závodů	závod	k1gInPc2	závod
byla	být	k5eAaImAgFnS	být
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
celkově	celkově	k6eAd1	celkově
třetí	třetí	k4xOgFnSc7	třetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komplexním	komplexní	k2eAgNnSc6d1	komplexní
bodování	bodování	k1gNnSc6	bodování
tzv.	tzv.	kA	tzv.
Grand	grand	k1gMnSc1	grand
World	World	k1gMnSc1	World
Cupu	cup	k1gInSc2	cup
byla	být	k5eAaImAgFnS	být
druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vyrovnala	vyrovnat	k5eAaBmAgFnS	vyrovnat
svoje	svůj	k3xOyFgNnSc4	svůj
umístění	umístění	k1gNnSc4	umístění
ze	z	k7c2	z
sezóny	sezóna	k1gFnSc2	sezóna
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
letní	letní	k2eAgFnSc2d1	letní
přípravy	příprava	k1gFnSc2	příprava
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
začala	začít	k5eAaPmAgFnS	začít
účastnit	účastnit	k5eAaImF	účastnit
i	i	k9	i
některých	některý	k3yIgInPc2	některý
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2007	[number]	k4	2007
poprvé	poprvé	k6eAd1	poprvé
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
záměr	záměr	k1gInSc4	záměr
startovat	startovat	k5eAaBmF	startovat
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2008	[number]	k4	2008
v	v	k7c6	v
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
cyklistických	cyklistický	k2eAgFnPc2d1	cyklistická
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
ale	ale	k9	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Obdobný	obdobný	k2eAgInSc4d1	obdobný
cíl	cíl	k1gInSc4	cíl
startu	start	k1gInSc2	start
na	na	k7c6	na
letní	letní	k2eAgFnSc6d1	letní
olympiádě	olympiáda	k1gFnSc6	olympiáda
2012	[number]	k4	2012
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
trenér	trenér	k1gMnSc1	trenér
Petr	Petr	k1gMnSc1	Petr
Novák	Novák	k1gMnSc1	Novák
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
ale	ale	k9	ale
nekvalifikovala	kvalifikovat	k5eNaBmAgFnS	kvalifikovat
a	a	k8xC	a
podle	podle	k7c2	podle
podmínek	podmínka	k1gFnPc2	podmínka
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
ani	ani	k8xC	ani
nemohla	moct	k5eNaImAgFnS	moct
dostat	dostat	k5eAaPmF	dostat
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
<g/>
.	.	kIx.	.
</s>
<s>
Potřetí	potřetí	k4xO	potřetí
se	se	k3xPyFc4	se
o	o	k7c4	o
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
LOH	LOH	kA	LOH
ucházela	ucházet	k5eAaImAgFnS	ucházet
v	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
,	,	kIx,	,
veškeré	veškerý	k3xTgFnPc4	veškerý
zprávy	zpráva	k1gFnPc4	zpráva
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
uváděly	uvádět	k5eAaImAgFnP	uvádět
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
cyklistiky	cyklistika	k1gFnSc2	cyklistika
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dokázala	dokázat	k5eAaPmAgFnS	dokázat
na	na	k7c4	na
Hry	hra	k1gFnPc4	hra
nominovat	nominovat	k5eAaBmF	nominovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
ale	ale	k8xC	ale
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
cyklistická	cyklistický	k2eAgFnSc1d1	cyklistická
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
UCI	UCI	kA	UCI
<g/>
)	)	kIx)	)
vydala	vydat	k5eAaPmAgFnS	vydat
seznam	seznam	k1gInSc4	seznam
zemí	zem	k1gFnPc2	zem
kvalifikovaných	kvalifikovaný	k2eAgFnPc2d1	kvalifikovaná
na	na	k7c6	na
LOH	LOH	kA	LOH
2016	[number]	k4	2016
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
de	de	k?	de
Janeiru	Janeir	k1gInSc2	Janeir
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
Česko	Česko	k1gNnSc4	Česko
chybělo	chybět	k5eAaImAgNnS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
nominační	nominační	k2eAgNnPc1d1	nominační
kritéria	kritérion	k1gNnPc4	kritérion
UCI	UCI	kA	UCI
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
byl	být	k5eAaImAgInS	být
start	start	k1gInSc4	start
v	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
podmíněn	podmínit	k5eAaPmNgInS	podmínit
kvalifikací	kvalifikace	k1gFnSc7	kvalifikace
do	do	k7c2	do
silničního	silniční	k2eAgInSc2d1	silniční
závodu	závod	k1gInSc2	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Českým	český	k2eAgInSc7d1	český
olympijským	olympijský	k2eAgInSc7d1	olympijský
výborem	výbor	k1gInSc7	výbor
a	a	k8xC	a
právníkem	právník	k1gMnSc7	právník
se	se	k3xPyFc4	se
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
UCI	UCI	kA	UCI
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
zvrátit	zvrátit	k5eAaPmF	zvrátit
a	a	k8xC	a
hledala	hledat	k5eAaImAgFnS	hledat
náhradní	náhradní	k2eAgNnSc4d1	náhradní
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Ria	Ria	k1gFnSc2	Ria
dokonce	dokonce	k9	dokonce
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
týmem	tým	k1gInSc7	tým
a	a	k8xC	a
trenérem	trenér	k1gMnSc7	trenér
Novákem	Novák	k1gMnSc7	Novák
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
a	a	k8xC	a
trénovala	trénovat	k5eAaImAgFnS	trénovat
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
tamního	tamní	k2eAgNnSc2d1	tamní
zasedání	zasedání	k1gNnSc2	zasedání
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
sportovní	sportovní	k2eAgFnSc2d1	sportovní
arbitráže	arbitráž	k1gFnSc2	arbitráž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovšem	ovšem	k9	ovšem
vynesla	vynést	k5eAaPmAgFnS	vynést
definitivní	definitivní	k2eAgInSc4d1	definitivní
verdikt	verdikt	k1gInSc4	verdikt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
na	na	k7c6	na
LOH	LOH	kA	LOH
nekvalifikovala	kvalifikovat	k5eNaBmAgFnS	kvalifikovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
žen	žena	k1gFnPc2	žena
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
zajela	zajet	k5eAaPmAgFnS	zajet
třetí	třetí	k4xOgInSc4	třetí
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
čas	čas	k1gInSc4	čas
<g/>
;	;	kIx,	;
závod	závod	k1gInSc1	závod
však	však	k9	však
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
neregulérnosti	neregulérnost	k1gFnSc3	neregulérnost
anulován	anulován	k2eAgInSc1d1	anulován
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
jel	jet	k5eAaImAgMnS	jet
za	za	k7c2	za
plného	plný	k2eAgInSc2d1	plný
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Opakovaný	opakovaný	k2eAgInSc1d1	opakovaný
závod	závod	k1gInSc1	závod
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
a	a	k8xC	a
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
svůj	svůj	k3xOyFgInSc4	svůj
výsledek	výsledek	k1gInSc4	výsledek
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgInPc4d1	obdobný
výsledky	výsledek	k1gInPc4	výsledek
z	z	k7c2	z
mistrovství	mistrovství	k1gNnSc2	mistrovství
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
zajela	zajet	k5eAaPmAgFnS	zajet
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
-	-	kIx~	-
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
s	s	k7c7	s
Martinou	Martina	k1gFnSc7	Martina
Růžičkovou	růžičkový	k2eAgFnSc4d1	růžičková
časovku	časovka	k1gFnSc4	časovka
dvojic	dvojice	k1gFnPc2	dvojice
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
triumf	triumf	k1gInSc4	triumf
v	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
dvojic	dvojice	k1gFnPc2	dvojice
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
s	s	k7c7	s
Růžičkovou	Růžičková	k1gFnSc7	Růžičková
<g/>
)	)	kIx)	)
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
<g/>
,	,	kIx,	,
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
navíc	navíc	k6eAd1	navíc
i	i	k8xC	i
časovku	časovka	k1gFnSc4	časovka
jednotlivkyň	jednotlivkyně	k1gFnPc2	jednotlivkyně
a	a	k8xC	a
závod	závod	k1gInSc1	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
obhájila	obhájit	k5eAaPmAgFnS	obhájit
tituly	titul	k1gInPc4	titul
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
i	i	k9	i
v	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
(	(	kIx(	(
<g/>
časovka	časovka	k1gFnSc1	časovka
dvojic	dvojice	k1gFnPc2	dvojice
se	se	k3xPyFc4	se
nejela	jet	k5eNaImAgFnS	jet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
společném	společný	k2eAgNnSc6d1	společné
Mistrovství	mistrovství	k1gNnSc6	mistrovství
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
příčce	příčka	k1gFnSc6	příčka
<g/>
,	,	kIx,	,
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
s	s	k7c7	s
hromadným	hromadný	k2eAgInSc7d1	hromadný
startem	start	k1gInSc7	start
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgFnSc1	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
časovku	časovka	k1gFnSc4	časovka
i	i	k8xC	i
silniční	silniční	k2eAgInSc4d1	silniční
závod	závod	k1gInSc4	závod
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podařilo	podařit	k5eAaPmAgNnS	podařit
také	také	k9	také
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
a	a	k8xC	a
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
i	i	k9	i
mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
23	[number]	k4	23
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
sedmém	sedmý	k4xOgInSc6	sedmý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mistrovství	mistrovství	k1gNnSc2	mistrovství
republiky	republika	k1gFnSc2	republika
i	i	k8xC	i
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
také	také	k9	také
několika	několik	k4yIc2	několik
závodů	závod	k1gInPc2	závod
cyklistického	cyklistický	k2eAgInSc2d1	cyklistický
Českého	český	k2eAgInSc2d1	český
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
cyklistice	cyklistika	k1gFnSc6	cyklistika
2010	[number]	k4	2010
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
startovat	startovat	k5eAaBmF	startovat
kvůli	kvůli	k7c3	kvůli
vzdálenému	vzdálený	k2eAgNnSc3d1	vzdálené
místu	místo	k1gNnSc3	místo
konání	konání	k1gNnSc2	konání
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
narušilo	narušit	k5eAaPmAgNnS	narušit
přípravu	příprava	k1gFnSc4	příprava
na	na	k7c4	na
rychlobruslařskou	rychlobruslařský	k2eAgFnSc4d1	rychlobruslařská
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
šampionátu	šampionát	k1gInSc2	šampionát
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
se	se	k3xPyFc4	se
již	již	k6eAd1	již
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2011	[number]	k4	2011
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
časovky	časovka	k1gFnSc2	časovka
<g/>
,	,	kIx,	,
z	z	k7c2	z
51	[number]	k4	51
závodnic	závodnice	k1gFnPc2	závodnice
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
ztrátou	ztráta	k1gFnSc7	ztráta
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
47,43	[number]	k4	47,43
min	mina	k1gFnPc2	mina
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c4	na
Letní	letní	k2eAgFnPc4d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
kvalifikovala	kvalifikovat	k5eAaBmAgFnS	kvalifikovat
pouze	pouze	k6eAd1	pouze
první	první	k4xOgFnSc1	první
desítka	desítka	k1gFnSc1	desítka
závodnic	závodnice	k1gFnPc2	závodnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
startovala	startovat	k5eAaBmAgFnS	startovat
na	na	k7c4	na
Tour	Tour	k1gInSc4	Tour
de	de	k?	de
Feminin	femininum	k1gNnPc2	femininum
<g/>
,	,	kIx,	,
svém	svůj	k3xOyFgMnSc6	svůj
prvním	první	k4xOgMnSc6	první
etapovém	etapový	k2eAgInSc6d1	etapový
závodě	závod	k1gInSc6	závod
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgNnSc2	který
ale	ale	k9	ale
ve	v	k7c6	v
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
etapě	etapa	k1gFnSc6	etapa
po	po	k7c6	po
bouřce	bouřka	k1gFnSc6	bouřka
a	a	k8xC	a
přívalovém	přívalový	k2eAgInSc6d1	přívalový
dešti	dešť	k1gInSc6	dešť
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neriskovala	riskovat	k5eNaBmAgFnS	riskovat
zranění	zranění	k1gNnSc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
cyklistice	cyklistika	k1gFnSc6	cyklistika
2012	[number]	k4	2012
v	v	k7c6	v
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
se	se	k3xPyFc4	se
v	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
devátém	devátý	k4xOgNnSc6	devátý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
silničního	silniční	k2eAgInSc2d1	silniční
závodu	závod	k1gInSc2	závod
po	po	k7c6	po
hromadném	hromadný	k2eAgInSc6d1	hromadný
pádu	pád	k1gInSc6	pád
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
dostala	dostat	k5eAaPmAgFnS	dostat
pozvánku	pozvánka	k1gFnSc4	pozvánka
do	do	k7c2	do
cyklistické	cyklistický	k2eAgFnSc2d1	cyklistická
stáje	stáj	k1gFnSc2	stáj
Etixx-iHNed	EtixxHNed	k1gInSc1	Etixx-iHNed
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Bakala	Bakal	k1gMnSc2	Bakal
jako	jako	k8xS	jako
farmářský	farmářský	k2eAgInSc1d1	farmářský
tým	tým	k1gInSc1	tým
pro	pro	k7c4	pro
elitní	elitní	k2eAgFnSc4d1	elitní
stáj	stáj	k1gFnSc4	stáj
Omega	omega	k1gFnSc1	omega
Pharma-Quick	Pharma-Quick	k1gMnSc1	Pharma-Quick
Step	step	k1gInSc1	step
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
se	se	k3xPyFc4	se
Tour	Tour	k1gMnSc1	Tour
de	de	k?	de
Feminin	femininum	k1gNnPc2	femininum
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
jednu	jeden	k4xCgFnSc4	jeden
časovkovou	časovkový	k2eAgFnSc4d1	časovková
etapu	etapa	k1gFnSc4	etapa
<g/>
,	,	kIx,	,
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
pořadí	pořadí	k1gNnSc6	pořadí
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
vrchařek	vrchařka	k1gFnPc2	vrchařka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
startovala	startovat	k5eAaBmAgFnS	startovat
na	na	k7c6	na
cyklistickém	cyklistický	k2eAgInSc6d1	cyklistický
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
2014	[number]	k4	2014
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
silniční	silniční	k2eAgInSc1d1	silniční
závod	závod	k1gInSc1	závod
po	po	k7c6	po
hromadném	hromadný	k2eAgInSc6d1	hromadný
pádu	pád	k1gInSc6	pád
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
nedokončila	dokončit	k5eNaPmAgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
cyklistice	cyklistika	k1gFnSc6	cyklistika
2015	[number]	k4	2015
se	se	k3xPyFc4	se
v	v	k7c6	v
časovce	časovka	k1gFnSc6	časovka
chtěla	chtít	k5eAaImAgFnS	chtít
kvalifikovat	kvalifikovat	k5eAaBmF	kvalifikovat
na	na	k7c4	na
Letní	letní	k2eAgFnPc4d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
díky	díky	k7c3	díky
dvanácté	dvanáctý	k4xOgFnSc6	dvanáctý
příčce	příčka	k1gFnSc6	příčka
zdánlivě	zdánlivě	k6eAd1	zdánlivě
podařilo	podařit	k5eAaPmAgNnS	podařit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
kvůli	kvůli	k7c3	kvůli
špatnému	špatný	k2eAgInSc3d1	špatný
výkladu	výklad	k1gInSc3	výklad
kritérií	kritérion	k1gNnPc2	kritérion
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
cyklistické	cyklistický	k2eAgFnSc2d1	cyklistická
unie	unie	k1gFnSc2	unie
na	na	k7c4	na
Hrách	hrách	k1gInSc4	hrách
startovat	startovat	k5eAaBmF	startovat
nemohla	moct	k5eNaImAgFnS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgInSc1d1	silniční
závod	závod	k1gInSc1	závod
na	na	k7c4	na
MS	MS	kA	MS
2015	[number]	k4	2015
vynechala	vynechat	k5eAaPmAgFnS	vynechat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
letním	letní	k2eAgInSc7d1	letní
sportem	sport	k1gInSc7	sport
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
inline	inlinout	k5eAaPmIp3nS	inlinout
bruslení	bruslení	k1gNnSc1	bruslení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
Mistrovství	mistrovství	k1gNnSc3	mistrovství
světa	svět	k1gInSc2	svět
juniorů	junior	k1gMnPc2	junior
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
20	[number]	k4	20
km	km	kA	km
skončila	skončit	k5eAaPmAgFnS	skončit
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
Mistrovství	mistrovství	k1gNnSc6	mistrovství
v	v	k7c6	v
maratonu	maraton	k1gInSc6	maraton
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
i	i	k9	i
první	první	k4xOgInSc4	první
ročník	ročník	k1gInSc4	ročník
Českého	český	k2eAgInSc2d1	český
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Triumf	triumf	k1gInSc1	triumf
v	v	k7c6	v
maratonu	maraton	k1gInSc6	maraton
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
i	i	k9	i
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
letní	letní	k2eAgFnSc2d1	letní
přípravy	příprava	k1gFnSc2	příprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
závodu	závod	k1gInSc3	závod
Olympic	Olympic	k1gMnSc1	Olympic
Hopes	Hopes	k1gMnSc1	Hopes
-	-	kIx~	-
Visegrad	Visegrad	k1gInSc1	Visegrad
Cup	cup	k1gInSc1	cup
na	na	k7c6	na
inline	inlin	k1gInSc5	inlin
bruslích	brusle	k1gFnPc6	brusle
na	na	k7c6	na
betonovém	betonový	k2eAgInSc6d1	betonový
oválu	ovál	k1gInSc6	ovál
ve	v	k7c6	v
Žďáru	Žďár	k1gInSc6	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
akci	akce	k1gFnSc6	akce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
jako	jako	k9	jako
první	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
Mistrovství	mistrovství	k1gNnSc2	mistrovství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
rychlobruslení	rychlobruslení	k1gNnSc6	rychlobruslení
<g/>
,	,	kIx,	,
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
tratích	trať	k1gFnPc6	trať
1000	[number]	k4	1000
m	m	kA	m
a	a	k8xC	a
3000	[number]	k4	3000
m.	m.	k?	m.
Celkem	celkem	k6eAd1	celkem
třikrát	třikrát	k6eAd1	třikrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
novinářskou	novinářský	k2eAgFnSc4d1	novinářská
anketu	anketa	k1gFnSc4	anketa
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
ale	ale	k8xC	ale
kategorii	kategorie	k1gFnSc4	kategorie
juniorů	junior	k1gMnPc2	junior
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
skončila	skončit	k5eAaPmAgFnS	skončit
pátá	pátý	k4xOgFnSc1	pátý
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
třetí	třetí	k4xOgInSc1	třetí
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
třináctá	třináctý	k4xOgFnSc1	třináctý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ročníku	ročník	k1gInSc6	ročník
2013	[number]	k4	2013
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
top	topit	k5eAaImRp2nS	topit
ten	ten	k3xDgMnSc1	ten
a	a	k8xC	a
umístila	umístit	k5eAaPmAgFnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
deváté	devátý	k4xOgFnSc6	devátý
příčce	příčka	k1gFnSc6	příčka
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
opětovně	opětovně	k6eAd1	opětovně
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
pomyslné	pomyslný	k2eAgInPc4d1	pomyslný
stupně	stupeň	k1gInPc4	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
:	:	kIx,	:
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
třetí	třetí	k4xOgFnSc6	třetí
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
druhá	druhý	k4xOgFnSc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
šestém	šestý	k4xOgInSc6	šestý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Sportovec	sportovec	k1gMnSc1	sportovec
Evropy	Evropa	k1gFnSc2	Evropa
2007	[number]	k4	2007
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
18	[number]	k4	18
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
získala	získat	k5eAaPmAgFnS	získat
jako	jako	k9	jako
pátá	pátý	k4xOgFnSc1	pátý
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
cenu	cena	k1gFnSc4	cena
Oscara	Oscar	k1gMnSc4	Oscar
Mathiasena	Mathiasen	k2eAgMnSc4d1	Mathiasen
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
uděluje	udělovat	k5eAaImIp3nS	udělovat
bruslařský	bruslařský	k2eAgInSc1d1	bruslařský
klub	klub	k1gInSc1	klub
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
obdržela	obdržet	k5eAaPmAgFnS	obdržet
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
přijetí	přijetí	k1gNnSc6	přijetí
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
sportovců	sportovec	k1gMnPc2	sportovec
armádního	armádní	k2eAgNnSc2d1	armádní
sportovního	sportovní	k2eAgNnSc2d1	sportovní
centra	centrum	k1gNnSc2	centrum
Dukla	Dukla	k1gFnSc1	Dukla
od	od	k7c2	od
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
Martina	Martin	k1gMnSc2	Martin
Bartáka	Barták	k1gMnSc2	Barták
vysoké	vysoká	k1gFnSc2	vysoká
armádní	armádní	k2eAgNnSc4d1	armádní
ocenění	ocenění	k1gNnSc4	ocenění
Armádní	armádní	k2eAgInSc1d1	armádní
kříž	kříž	k1gInSc1	kříž
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každoroční	každoroční	k2eAgFnSc6d1	každoroční
anketě	anketa	k1gFnSc6	anketa
Armádní	armádní	k2eAgMnSc1d1	armádní
sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
vítězkou	vítězka	k1gFnSc7	vítězka
ankety	anketa	k1gFnSc2	anketa
Sportovec	sportovec	k1gMnSc1	sportovec
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2010	[number]	k4	2010
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
Českým	český	k2eAgInSc7d1	český
olympijským	olympijský	k2eAgInSc7d1	olympijský
výborem	výbor	k1gInSc7	výbor
udělena	udělen	k2eAgFnSc1d1	udělena
Cena	cena	k1gFnSc1	cena
Jiřího	Jiří	k1gMnSc2	Jiří
Gutha-Jarkovského	Gutha-Jarkovský	k2eAgMnSc2d1	Gutha-Jarkovský
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Cenu	cena	k1gFnSc4	cena
města	město	k1gNnSc2	město
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgMnSc7	první
laureátem	laureát	k1gMnSc7	laureát
tohoto	tento	k3xDgNnSc2	tento
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	s	k7c7	s
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1987	[number]	k4	1987
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Žďáru	Žďár	k1gInSc6	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
Eva	Eva	k1gFnSc1	Eva
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
účetní	účetní	k1gMnPc4	účetní
pro	pro	k7c4	pro
Petra	Petr	k1gMnSc4	Petr
Nováka	Novák	k1gMnSc4	Novák
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
k	k	k7c3	k
rychlobruslení	rychlobruslení	k1gNnSc3	rychlobruslení
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Milan	Milan	k1gMnSc1	Milan
naopak	naopak	k6eAd1	naopak
hrával	hrávat	k5eAaImAgMnS	hrávat
basketbal	basketbal	k1gInSc4	basketbal
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
jako	jako	k8xS	jako
dítě	dítě	k1gNnSc4	dítě
začínala	začínat	k5eAaImAgFnS	začínat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Milan	Milan	k1gMnSc1	Milan
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
věnoval	věnovat	k5eAaImAgMnS	věnovat
rychlobruslení	rychlobruslení	k1gNnSc4	rychlobruslení
pod	pod	k7c7	pod
trenérem	trenér	k1gMnSc7	trenér
Novákem	Novák	k1gMnSc7	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
jsou	být	k5eAaImIp3nP	být
rozvedení	rozvedení	k1gNnSc4	rozvedení
<g/>
,	,	kIx,	,
rozešli	rozejít	k5eAaPmAgMnP	rozejít
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
otcova	otcův	k2eAgInSc2d1	otcův
dalšího	další	k2eAgInSc2d1	další
vztahu	vztah	k1gInSc2	vztah
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
polorodé	polorodý	k2eAgMnPc4d1	polorodý
sourozence	sourozenec	k1gMnPc4	sourozenec
<g/>
,	,	kIx,	,
dvojčata	dvojče	k1gNnPc4	dvojče
Martina	Martin	k2eAgNnPc4d1	Martino
a	a	k8xC	a
Barboru	Barbora	k1gFnSc4	Barbora
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
Základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
Švermova	Švermův	k2eAgInSc2d1	Švermův
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
ZŠ	ZŠ	kA	ZŠ
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Žďáru	Žďár	k1gInSc6	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Vincence	Vincenc	k1gMnSc2	Vincenc
Makovského	Makovský	k1gMnSc2	Makovský
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
se	s	k7c7	s
sportovním	sportovní	k2eAgNnSc7d1	sportovní
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Obyčejné	obyčejný	k2eAgNnSc4d1	obyčejné
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
studium	studium	k1gNnSc4	studium
si	se	k3xPyFc3	se
díky	díky	k7c3	díky
vedení	vedení	k1gNnSc3	vedení
školy	škola	k1gFnSc2	škola
rozložila	rozložit	k5eAaPmAgFnS	rozložit
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
úspěšně	úspěšně	k6eAd1	úspěšně
odmaturovala	odmaturovat	k5eAaPmAgNnP	odmaturovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
soukromé	soukromý	k2eAgFnSc6d1	soukromá
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
tělesné	tělesný	k2eAgFnSc2d1	tělesná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
sportu	sport	k1gInSc2	sport
Palestra	palestra	k1gFnSc1	palestra
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
domu	dům	k1gInSc2	dům
ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
Oseku	Osek	k1gInSc6	Osek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
vyrůst	vyrůst	k5eAaPmF	vyrůst
první	první	k4xOgFnSc1	první
rychlobruslařská	rychlobruslařský	k2eAgFnSc1d1	rychlobruslařská
hala	hala	k1gFnSc1	hala
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
tyto	tento	k3xDgInPc1	tento
plány	plán	k1gInPc1	plán
nebyly	být	k5eNaImAgInP	být
realizovány	realizovat	k5eAaBmNgInP	realizovat
<g/>
,	,	kIx,	,
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ZOH	ZOH	kA	ZOH
v	v	k7c6	v
Soči	Soči	k1gNnSc6	Soči
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
ve	v	k7c6	v
Žďáru	Žďár	k1gInSc6	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
darem	dar	k1gInSc7	dar
od	od	k7c2	od
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
parcelu	parcela	k1gFnSc4	parcela
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
rodinného	rodinný	k2eAgInSc2d1	rodinný
domu	dům	k1gInSc2	dům
<g/>
;	;	kIx,	;
pozemek	pozemek	k1gInSc1	pozemek
na	na	k7c6	na
rozšiřujícím	rozšiřující	k2eAgNnSc6d1	rozšiřující
se	s	k7c7	s
sídlišti	sídliště	k1gNnPc7	sídliště
Klafar	Klafar	k1gMnSc1	Klafar
si	se	k3xPyFc3	se
sama	sám	k3xTgFnSc1	sám
vybrala	vybrat	k5eAaPmAgFnS	vybrat
<g/>
.	.	kIx.	.
</s>
<s>
Martina	Martin	k2eAgFnSc1d1	Martina
Sáblíková	Sáblíková	k1gFnSc1	Sáblíková
měří	měřit	k5eAaImIp3nS	měřit
171	[number]	k4	171
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
53	[number]	k4	53
kg	kg	kA	kg
<g/>
.	.	kIx.	.
1	[number]	k4	1
český	český	k2eAgInSc4d1	český
rekord	rekord	k1gInSc4	rekord
<g/>
2	[number]	k4	2
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
na	na	k7c6	na
otevřených	otevřený	k2eAgFnPc6d1	otevřená
drahách	draha	k1gFnPc6	draha
(	(	kIx(	(
<g/>
OD	od	k7c2	od
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
<g/>
4	[number]	k4	4
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
světový	světový	k2eAgInSc4d1	světový
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
10	[number]	k4	10
000	[number]	k4	000
m	m	kA	m
se	se	k3xPyFc4	se
ženské	ženský	k2eAgInPc1d1	ženský
závody	závod	k1gInPc1	závod
nekonají	konat	k5eNaImIp3nP	konat
<g/>
)	)	kIx)	)
1	[number]	k4	1
dosud	dosud	k6eAd1	dosud
<g />
.	.	kIx.	.
</s>
<s>
platné	platný	k2eAgInPc4d1	platný
světové	světový	k2eAgInPc4d1	světový
rekordy	rekord	k1gInPc4	rekord
<g/>
2	[number]	k4	2
čas	čas	k1gInSc1	čas
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
8,28	[number]	k4	8,28
zajela	zajet	k5eAaPmAgFnS	zajet
jako	jako	k9	jako
juniorka	juniorka	k1gFnSc1	juniorka
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
ale	ale	k9	ale
o	o	k7c4	o
celkově	celkově	k6eAd1	celkově
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
čas	čas	k1gInSc4	čas
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
ženských	ženský	k2eAgFnPc6d1	ženská
kategoriích	kategorie	k1gFnPc6	kategorie
<g/>
3	[number]	k4	3
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
světový	světový	k2eAgInSc4d1	světový
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
závody	závod	k1gInPc1	závod
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
10	[number]	k4	10
000	[number]	k4	000
m	m	kA	m
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
běžně	běžně	k6eAd1	běžně
nepořádají	pořádat	k5eNaImIp3nP	pořádat
<g/>
)	)	kIx)	)
Zdrojem	zdroj	k1gInSc7	zdroj
pro	pro	k7c4	pro
tabulku	tabulka	k1gFnSc4	tabulka
jsou	být	k5eAaImIp3nP	být
oficiální	oficiální	k2eAgInPc1d1	oficiální
výsledky	výsledek	k1gInPc1	výsledek
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
bruslařské	bruslařský	k2eAgFnSc2d1	bruslařská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
