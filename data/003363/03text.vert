<s>
Anthrax	Anthrax	k1gInSc1	Anthrax
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
thrashmetalová	thrashmetalový	k2eAgFnSc1d1	thrashmetalová
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
thrashmetalové	thrashmetalový	k2eAgFnSc6d1	thrashmetalová
scéně	scéna	k1gFnSc6	scéna
byli	být	k5eAaImAgMnP	být
Anthrax	Anthrax	k1gInSc4	Anthrax
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
skupin	skupina	k1gFnPc2	skupina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
známá	známá	k1gFnSc1	známá
kombinováním	kombinování	k1gNnSc7	kombinování
metalové	metalový	k2eAgFnSc2d1	metalová
muziky	muzika	k1gFnSc2	muzika
s	s	k7c7	s
rapem	rape	k1gNnSc7	rape
<g/>
,	,	kIx,	,
hardcorem	hardcor	k1gInSc7	hardcor
a	a	k8xC	a
alternativním	alternativní	k2eAgInSc7d1	alternativní
rockem	rock	k1gInSc7	rock
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
"	"	kIx"	"
<g/>
Velké	velký	k2eAgFnSc2d1	velká
čtyřky	čtyřka	k1gFnSc2	čtyřka
<g/>
"	"	kIx"	"
thrash	thrash	k1gInSc1	thrash
metalu	metal	k1gInSc2	metal
–	–	k?	–
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
Metallica	Metallica	k1gMnSc1	Metallica
<g/>
,	,	kIx,	,
Slayer	Slayer	k1gMnSc1	Slayer
a	a	k8xC	a
Megadeth	Megadeth	k1gMnSc1	Megadeth
<g/>
.	.	kIx.	.
</s>
<s>
Scott	Scott	k1gMnSc1	Scott
Ian	Ian	k1gMnSc1	Ian
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Jon	Jon	k1gFnSc1	Jon
Donais	Donais	k1gFnSc1	Donais
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Frank	Frank	k1gMnSc1	Frank
Bello	Bello	k1gNnSc1	Bello
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
Charlie	Charlie	k1gMnSc5	Charlie
Benante	Benant	k1gMnSc5	Benant
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgFnSc6d1	bicí
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Joey	Joea	k1gFnSc2	Joea
Belladonna	Belladonna	k1gFnSc1	Belladonna
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Jason	Jason	k1gMnSc1	Jason
Rosenfeld	Rosenfeld	k1gMnSc1	Rosenfeld
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Dirk	Dirk	k1gInSc1	Dirk
Kennedy	Kenneda	k1gMnSc2	Kenneda
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Connelly	Connella	k1gMnSc2	Connella
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Neil	Neil	k1gMnSc1	Neil
<g />
.	.	kIx.	.
</s>
<s>
Turbin	turbina	k1gFnPc2	turbina
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Matt	Matt	k2eAgInSc1d1	Matt
Fallon	Fallon	k1gInSc1	Fallon
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
John	John	k1gMnSc1	John
Bush	Bush	k1gMnSc1	Bush
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Dan	Dan	k1gMnSc1	Dan
Nelson	Nelson	k1gMnSc1	Nelson
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Kenny	Kenna	k1gFnSc2	Kenna
Kushner	Kushner	k1gInSc1	Kushner
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Greg	Greg	k1gInSc1	Greg
Walls	Walls	k1gInSc1	Walls
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Paul	Paula	k1gFnPc2	Paula
Crook	Crook	k1gInSc1	Crook
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Rob	roba	k1gFnPc2	roba
Caggiano	Caggiana	k1gFnSc5	Caggiana
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Paul	Paul	k1gMnSc1	Paul
Kahn	Kahn	k1gMnSc1	Kahn
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Dan	Dan	k1gMnSc1	Dan
Lilker	Lilker	k1gMnSc1	Lilker
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Joey	Joea	k1gFnSc2	Joea
Vera	Ver	k1gInSc2	Ver
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Dave	Dav	k1gInSc5	Dav
Weiss	Weiss	k1gMnSc1	Weiss
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Greg	Greg	k1gInSc1	Greg
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Angelo	Angela	k1gFnSc5	Angela
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Jason	Jason	k1gInSc1	Jason
Bittner	Bittner	k1gMnSc1	Bittner
Fistful	Fistful	k1gInSc1	Fistful
of	of	k?	of
Metal	metal	k1gInSc1	metal
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Spreading	Spreading	k1gInSc1	Spreading
the	the	k?	the
Disease	Diseas	k1gInSc6	Diseas
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Among	Among	k1gInSc1	Among
the	the	k?	the
Living	Living	k1gInSc1	Living
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
State	status	k1gInSc5	status
of	of	k?	of
Euphoria	Euphorium	k1gNnPc1	Euphorium
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Persistence	persistence	k1gFnSc1	persistence
of	of	k?	of
Time	Time	k1gFnSc1	Time
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Sound	Sound	k1gInSc1	Sound
of	of	k?	of
White	Whit	k1gMnSc5	Whit
Noise	Nois	k1gMnSc5	Nois
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Stomp	Stomp	k1gInSc1	Stomp
442	[number]	k4	442
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Volume	volum	k1gInSc5	volum
8	[number]	k4	8
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
The	The	k1gFnPc2	The
Threat	Threat	k1gInSc1	Threat
Is	Is	k1gMnSc1	Is
Real	Real	k1gInSc1	Real
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
We	We	k1gFnSc2	We
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Come	Come	k1gFnSc6	Come
for	forum	k1gNnPc2	forum
You	You	k1gMnSc2	You
All	All	k1gMnSc2	All
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Worship	Worship	k1gMnSc1	Worship
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Anthems	Anthemsa	k1gFnPc2	Anthemsa
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
For	forum	k1gNnPc2	forum
All	All	k1gFnPc2	All
Kings	Kingsa	k1gFnPc2	Kingsa
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Island	Island	k1gInSc1	Island
Years	Years	k1gInSc1	Years
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Music	Music	k1gMnSc1	Music
of	of	k?	of
Mass	Mass	k1gInSc1	Mass
Destruction	Destruction	k1gInSc1	Destruction
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Alive	Aliev	k1gFnSc2	Aliev
2	[number]	k4	2
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Fistful	Fistful	k1gInSc1	Fistful
of	of	k?	of
Anthrax	Anthrax	k1gInSc1	Anthrax
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Attack	Attack	k1gInSc1	Attack
of	of	k?	of
the	the	k?	the
Killer	Killer	k1gInSc1	Killer
B	B	kA	B
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Moshers	Moshersa	k1gFnPc2	Moshersa
<g/>
:	:	kIx,	:
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Return	Return	k1gMnSc1	Return
of	of	k?	of
The	The	k1gMnSc1	The
Killer	Killer	k1gMnSc1	Killer
A	A	kA	A
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Madhouse	Madhouse	k1gFnSc2	Madhouse
–	–	k?	–
The	The	k1gMnSc1	The
Very	Vera	k1gFnSc2	Vera
Best	Best	k1gMnSc1	Best
of	of	k?	of
Anthrax	Anthrax	k1gInSc1	Anthrax
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Universal	Universal	k1gFnSc1	Universal
Masters	Masters	k1gInSc1	Masters
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Greater	Greater	k1gMnSc1	Greater
of	of	k?	of
Two	Two	k1gMnSc1	Two
<g />
.	.	kIx.	.
</s>
<s>
Evils	Evils	k1gInSc1	Evils
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Anthrology	Anthrolog	k1gMnPc7	Anthrolog
<g/>
:	:	kIx,	:
No	no	k9	no
Hit	hit	k1gInSc1	hit
Wonders	Wondersa	k1gFnPc2	Wondersa
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Armed	Armed	k1gInSc1	Armed
and	and	k?	and
Dangerous	Dangerous	k1gInSc1	Dangerous
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
the	the	k?	the
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Penikufesin	Penikufesina	k1gFnPc2	Penikufesina
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Free	Fre	k1gInSc2	Fre
B	B	kA	B
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Inside	Insid	k1gMnSc5	Insid
Out	Out	k1gMnSc5	Out
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Summer	Summer	k1gInSc1	Summer
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Anthrax	Anthrax	k1gInSc1	Anthrax
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
Světový	světový	k2eAgInSc1d1	světový
fanklub	fanklub	k1gInSc1	fanklub
</s>
