<s>
Berenika	Berenik	k1gMnSc4	Berenik
Čecháková	Čecháková	k1gFnSc1	Čecháková
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Kohoutová	Kohoutová	k1gFnSc1	Kohoutová
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
.	.	kIx.	.
</s>
<s>
Studuje	studovat	k5eAaImIp3nS	studovat
herectví	herectví	k1gNnSc1	herectví
na	na	k7c6	na
Pražské	pražský	k2eAgFnSc6d1	Pražská
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgNnP	být
členkou	členka	k1gFnSc7	členka
Bambini	Bambin	k2eAgMnPc1d1	Bambin
di	di	k?	di
Praga	Praga	k1gFnSc1	Praga
<g/>
,	,	kIx,	,
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
přešla	přejít	k5eAaPmAgFnS	přejít
do	do	k7c2	do
Dismanova	Dismanův	k2eAgInSc2d1	Dismanův
rozhlasového	rozhlasový	k2eAgInSc2d1	rozhlasový
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
její	její	k3xOp3gInPc4	její
kroky	krok	k1gInPc4	krok
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
umělecké	umělecký	k2eAgFnSc3d1	umělecká
skupině	skupina	k1gFnSc3	skupina
OLDstars	OLDstars	k1gInSc4	OLDstars
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dětských	dětský	k2eAgNnPc2d1	dětské
let	léto	k1gNnPc2	léto
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
debutovala	debutovat	k5eAaBmAgFnS	debutovat
před	před	k7c7	před
kamerou	kamera	k1gFnSc7	kamera
v	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
pohádce	pohádka	k1gFnSc6	pohádka
Tři	tři	k4xCgNnPc4	tři
srdce	srdce	k1gNnPc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
babička	babička	k1gFnSc1	babička
Anna	Anna	k1gFnSc1	Anna
Žídková	Žídková	k1gFnSc1	Žídková
má	mít	k5eAaImIp3nS	mít
židovské	židovský	k2eAgInPc4d1	židovský
kořeny	kořen	k1gInPc4	kořen
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
je	být	k5eAaImIp3nS	být
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Irena	Irena	k1gFnSc1	Irena
Obermannová	Obermannová	k1gFnSc1	Obermannová
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
hudebník	hudebník	k1gMnSc1	hudebník
Daniel	Daniel	k1gMnSc1	Daniel
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
Rozálie	Rozálie	k1gFnSc1	Rozálie
Kohoutová	kohoutový	k2eAgFnSc1d1	Kohoutová
je	být	k5eAaImIp3nS	být
režisérka	režisérka	k1gFnSc1	režisérka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
zpívala	zpívat	k5eAaImAgFnS	zpívat
ve	v	k7c6	v
ska	ska	k?	ska
skupině	skupina	k1gFnSc3	skupina
DiscoBalls	DiscoBalls	k1gInSc4	DiscoBalls
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
měla	mít	k5eAaImAgFnS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
projekt	projekt	k1gInSc4	projekt
Femme	Femm	k1gMnSc5	Femm
Plastique	Plastiquus	k1gMnSc5	Plastiquus
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
mj.	mj.	kA	mj.
Emil	Emil	k1gMnSc1	Emil
Viklický	Viklický	k2eAgMnSc1d1	Viklický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vydala	vydat	k5eAaPmAgFnS	vydat
s	s	k7c7	s
Viklickým	Viklický	k2eAgInSc7d1	Viklický
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
hosty	host	k1gMnPc7	host
desku	deska	k1gFnSc4	deska
Berenika	Berenik	k1gMnSc2	Berenik
Meets	Meets	k1gInSc1	Meets
Jazz	jazz	k1gInSc1	jazz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
tváří	tvář	k1gFnPc2	tvář
Jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
společně	společně	k6eAd1	společně
<g/>
,	,	kIx,	,
kampaně	kampaň	k1gFnSc2	kampaň
iniciativy	iniciativa	k1gFnSc2	iniciativa
HateFree	HateFreus	k1gMnSc5	HateFreus
Culture	Cultur	k1gMnSc5	Cultur
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Marikou	Marika	k1gFnSc7	Marika
Šoposkou	Šoposka	k1gFnSc7	Šoposka
píše	psát	k5eAaImIp3nS	psát
blog	blog	k1gInSc1	blog
Sedmilhářky	sedmilhářka	k1gFnSc2	sedmilhářka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vyšly	vyjít	k5eAaPmAgInP	vyjít
zápisy	zápis	k1gInPc1	zápis
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
i	i	k9	i
knižně	knižně	k6eAd1	knižně
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
-	-	kIx~	-
mladá	mladý	k2eAgFnSc1d1	mladá
Marta	Marta	k1gFnSc1	Marta
Můj	můj	k3xOp1gInSc4	můj
vysvlečenej	vysvlečenej	k?	vysvlečenej
deník	deník	k1gInSc1	deník
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
-	-	kIx~	-
Kasandra	Kasandra	k1gFnSc1	Kasandra
Nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
romance	romance	k1gFnSc1	romance
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
-	-	kIx~	-
Luisa	Luisa	k1gFnSc1	Luisa
Fair	fair	k6eAd1	fair
play	play	k0	play
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
-	-	kIx~	-
Anna	Anna	k1gFnSc1	Anna
[	[	kIx(	[
<g/>
dabing	dabing	k1gInSc1	dabing
při	při	k7c6	při
postsynchronu	postsynchron	k1gInSc6	postsynchron
za	za	k7c4	za
Judit	Judit	k1gFnSc4	Judit
Bárdos	Bárdosa	k1gFnPc2	Bárdosa
<g/>
]	]	kIx)	]
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Olga	Olga	k1gFnSc1	Olga
Hepnarová	Hepnarová	k1gFnSc1	Hepnarová
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
Tři	tři	k4xCgNnPc4	tři
srdce	srdce	k1gNnPc4	srdce
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
-	-	kIx~	-
princezna	princezna	k1gFnSc1	princezna
Jasna	jasno	k1gNnSc2	jasno
Tatínkova	tatínkův	k2eAgFnSc1d1	tatínkova
holčička	holčička	k1gFnSc1	holčička
<g/>
,	,	kIx,	,
epizoda	epizoda	k1gFnSc1	epizoda
cyklu	cyklus	k1gInSc2	cyklus
Soukromé	soukromý	k2eAgFnSc2d1	soukromá
pasti	past	k1gFnSc2	past
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
-	-	kIx~	-
Veronika	Veronika	k1gFnSc1	Veronika
Rytmus	rytmus	k1gInSc1	rytmus
v	v	k7c6	v
patách	pata	k1gFnPc6	pata
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
-	-	kIx~	-
Marcela	Marcela	k1gFnSc1	Marcela
Razumowská	Razumowský	k2eAgFnSc1d1	Razumowský
Santiniho	Santini	k1gMnSc4	Santini
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
-	-	kIx~	-
recepční	recepční	k2eAgFnSc2d1	recepční
Ordinace	ordinace	k1gFnSc2	ordinace
v	v	k7c6	v
růžové	růžový	k2eAgFnSc6d1	růžová
zahradě	zahrada	k1gFnSc6	zahrada
Terapie	terapie	k1gFnSc2	terapie
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
Klára	Klára	k1gFnSc1	Klára
Poštová	Poštová	k1gFnSc1	Poštová
Ulice	ulice	k1gFnSc1	ulice
-	-	kIx~	-
Markéta	Markéta	k1gFnSc1	Markéta
Havránková	Havránková	k1gFnSc1	Havránková
Život	život	k1gInSc4	život
je	být	k5eAaImIp3nS	být
ples	ples	k1gInSc1	ples
Místo	místo	k7c2	místo
zločinu	zločin	k1gInSc2	zločin
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
Trapný	trapný	k2eAgInSc1d1	trapný
padesátky	padesátka	k1gFnSc2	padesátka
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
-	-	kIx~	-
chystaný	chystaný	k2eAgInSc1d1	chystaný
seriál	seriál	k1gInSc1	seriál
R.	R.	kA	R.
P.	P.	kA	P.
Music	Musice	k1gFnPc2	Musice
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Jiřího	Jiří	k1gMnSc2	Jiří
Grossmanna	Grossmann	k1gMnSc2	Grossmann
<g/>
:	:	kIx,	:
Babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
-	-	kIx~	-
Barunka	Barunka	k1gFnSc1	Barunka
Dismanův	Dismanův	k2eAgInSc1d1	Dismanův
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
dětský	dětský	k2eAgInSc4d1	dětský
soubor	soubor	k1gInSc4	soubor
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
Mlejn	Mlejn	k1gInSc1	Mlejn
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Noc	noc	k1gFnSc1	noc
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
-	-	kIx~	-
Alena	Alena	k1gFnSc1	Alena
Divadlo	divadlo	k1gNnSc1	divadlo
Na	na	k7c6	na
Fidlovačce	Fidlovačka	k1gFnSc6	Fidlovačka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Divotvorný	divotvorný	k2eAgInSc1d1	divotvorný
hrnec	hrnec	k1gInSc1	hrnec
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
dětí	dítě	k1gFnPc2	dítě
Divadlo	divadlo	k1gNnSc1	divadlo
Komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Hořké	hořký	k2eAgFnSc2d1	hořká
slzy	slza	k1gFnSc2	slza
Petry	Petra	k1gFnSc2	Petra
von	von	k1gInSc1	von
Kantové	Kantová	k1gFnSc2	Kantová
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
OLDstars	OLDstarsa	k1gFnPc2	OLDstarsa
<g/>
:	:	kIx,	:
Slyšet	slyšet	k5eAaImF	slyšet
hlasy	hlas	k1gInPc4	hlas
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Divadlo	divadlo	k1gNnSc4	divadlo
Hybernia	Hybernium	k1gNnSc2	Hybernium
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Hello	Hello	k1gNnSc1	Hello
<g/>
,	,	kIx,	,
Dolly	Doll	k1gInPc1	Doll
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
-	-	kIx~	-
Minnie	Minnie	k1gFnSc1	Minnie
Fayová	Fayová	k1gFnSc1	Fayová
Divadlo	divadlo	k1gNnSc1	divadlo
Viola	Viola	k1gFnSc1	Viola
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Století	století	k1gNnSc1	století
hitů	hit	k1gInPc2	hit
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
Studio	studio	k1gNnSc1	studio
DVA	dva	k4xCgInPc4	dva
<g/>
:	:	kIx,	:
Šíleně	šíleně	k6eAd1	šíleně
smutná	smutný	k2eAgFnSc1d1	smutná
princezna	princezna	k1gFnSc1	princezna
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
-	-	kIx~	-
princezna	princezna	k1gFnSc1	princezna
Vlčí	vlčí	k2eAgFnSc1d1	vlčí
ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
-	-	kIx~	-
Líza	Líza	k1gFnSc1	Líza
Gaelová	Gaelový	k2eAgFnSc1d1	Gaelový
Děvčátko	děvčátko	k1gNnSc4	děvčátko
Momo	Momo	k6eAd1	Momo
a	a	k8xC	a
ztracený	ztracený	k2eAgInSc4d1	ztracený
čas	čas	k1gInSc4	čas
Irena	Irena	k1gFnSc1	Irena
Obermannová	Obermannová	k1gFnSc1	Obermannová
<g/>
:	:	kIx,	:
Matky	matka	k1gFnPc1	matka
to	ten	k3xDgNnSc4	ten
chtěj	chtěj	k6eAd1	chtěj
<g />
.	.	kIx.	.
</s>
<s>
taky	taky	k6eAd1	taky
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
DiscoBalls	DiscoBallsa	k1gFnPc2	DiscoBallsa
<g/>
:	:	kIx,	:
DiscoVery	DiscoVera	k1gFnPc1	DiscoVera
Channel	Channlo	k1gNnPc2	Channlo
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
Rytmus	rytmus	k1gInSc1	rytmus
v	v	k7c6	v
patách	pata	k1gFnPc6	pata
<g/>
,	,	kIx,	,
soundtrack	soundtrack	k1gInSc1	soundtrack
k	k	k7c3	k
televiznímu	televizní	k2eAgInSc3d1	televizní
filmu	film	k1gInSc3	film
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
Berenika	Berenik	k1gMnSc2	Berenik
Meets	Meets	k1gInSc4	Meets
Jazz	jazz	k1gInSc1	jazz
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Berenika	Berenik	k1gMnSc2	Berenik
Kohoutová	Kohoutová	k1gFnSc1	Kohoutová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Berenika	Berenik	k1gMnSc2	Berenik
Kohoutová	Kohoutová	k1gFnSc1	Kohoutová
Berenika	Berenik	k1gMnSc2	Berenik
Kohoutová	Kohoutová	k1gFnSc1	Kohoutová
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Berenika	Berenik	k1gMnSc2	Berenik
Kohoutová	Kohoutová	k1gFnSc1	Kohoutová
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
