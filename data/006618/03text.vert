<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc4	stav
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
nebo	nebo	k8xC	nebo
ochotna	ochoten	k2eAgFnSc1d1	ochotna
najít	najít	k5eAaPmF	najít
si	se	k3xPyFc3	se
placené	placený	k2eAgNnSc4d1	placené
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
metodiky	metodika	k1gFnSc2	metodika
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
za	za	k7c2	za
nezaměstnaného	nezaměstnaný	k1gMnSc2	nezaměstnaný
považuje	považovat	k5eAaImIp3nS	považovat
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgMnSc1d2	starší
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
aktivně	aktivně	k6eAd1	aktivně
hledá	hledat	k5eAaImIp3nS	hledat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
připravena	připravit	k5eAaPmNgFnS	připravit
k	k	k7c3	k
nástupu	nástup	k1gInSc3	nástup
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
do	do	k7c2	do
14	[number]	k4	14
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vážný	vážný	k2eAgInSc4d1	vážný
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
problém	problém	k1gInSc4	problém
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nezaměstnaný	zaměstnaný	k2eNgMnSc1d1	nezaměstnaný
nemá	mít	k5eNaImIp3nS	mít
práci	práce	k1gFnSc4	práce
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
1	[number]	k4	1
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těžký	těžký	k2eAgInSc4d1	těžký
návrat	návrat	k1gInSc4	návrat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
podíl	podíl	k1gInSc4	podíl
nezaměstnaných	nezaměstnaný	k1gMnPc2	nezaměstnaný
ke	k	k7c3	k
všem	všecek	k3xTgFnPc3	všecek
osobám	osoba	k1gFnPc3	osoba
schopným	schopný	k2eAgMnPc3d1	schopný
pracovat	pracovat	k5eAaImF	pracovat
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
i	i	k9	i
zaměstnaným	zaměstnaný	k1gMnSc7	zaměstnaný
a	a	k8xC	a
nezaměstnaným	nezaměstnaný	k1gMnSc7	nezaměstnaný
<g/>
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
E	E	kA	E
+	+	kIx~	+
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
<g />
.	.	kIx.	.
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
E	E	kA	E
<g/>
+	+	kIx~	+
<g/>
U	U	kA	U
<g/>
}}}	}}}	k?	}}}
neboli	neboli	k8xC	neboli
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
<g/>
L	L	kA	L
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
}	}	kIx)	}
–	–	k?	–
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
(	(	kIx(	(
<g/>
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
často	často	k6eAd1	často
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hodnota	hodnota	k1gFnSc1	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
u	u	k7c2	u
⋅	⋅	k?	⋅
100	[number]	k4	100
:	:	kIx,	:
%	%	kIx~	%
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
.	.	kIx.	.
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
100	[number]	k4	100
<g/>
~	~	kIx~	~
<g/>
\	\	kIx~	\
<g/>
%	%	kIx~	%
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
U	u	k7c2	u
<g/>
}	}	kIx)	}
–	–	k?	–
počet	počet	k1gInSc4	počet
lidí	člověk	k1gMnPc2	člověk
bez	bez	k7c2	bez
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
E	E	kA	E
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
E	E	kA	E
<g/>
}	}	kIx)	}
–	–	k?	–
počet	počet	k1gInSc4	počet
zaměstnaných	zaměstnaný	k2eAgMnPc2d1	zaměstnaný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
}	}	kIx)	}
–	–	k?	–
celkový	celkový	k2eAgInSc4d1	celkový
počet	počet	k1gInSc4	počet
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
=	=	kIx~	=
E	E	kA	E
+	+	kIx~	+
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
=	=	kIx~	=
<g/>
E	E	kA	E
<g/>
+	+	kIx~	+
<g/>
U	u	k7c2	u
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
přesnost	přesnost	k1gFnSc1	přesnost
takového	takový	k3xDgNnSc2	takový
měření	měření	k1gNnSc2	měření
těžko	těžko	k6eAd1	těžko
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
metoda	metoda	k1gFnSc1	metoda
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
nedostatky	nedostatek	k1gInPc4	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
zjišťovány	zjišťovat	k5eAaImNgInP	zjišťovat
dva	dva	k4xCgInPc1	dva
ukazatelé	ukazatel	k1gInPc1	ukazatel
míry	míra	k1gFnSc2	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
-	-	kIx~	-
obecná	obecná	k1gFnSc1	obecná
a	a	k8xC	a
registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ukazatel	ukazatel	k1gInSc4	ukazatel
míry	míra	k1gFnSc2	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
zjišťovaný	zjišťovaný	k2eAgInSc4d1	zjišťovaný
Českým	český	k2eAgInSc7d1	český
statistickým	statistický	k2eAgInSc7d1	statistický
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
míra	míra	k1gFnSc1	míra
je	být	k5eAaImIp3nS	být
vypočítána	vypočítat	k5eAaPmNgFnS	vypočítat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výběrového	výběrový	k2eAgNnSc2d1	výběrové
šetření	šetření	k1gNnSc2	šetření
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
-	-	kIx~	-
vychází	vycházet	k5eAaImIp3nS	vycházet
tedy	tedy	k9	tedy
z	z	k7c2	z
reprezentativního	reprezentativní	k2eAgInSc2d1	reprezentativní
vzorku	vzorek	k1gInSc2	vzorek
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ukazatel	ukazatel	k1gInSc1	ukazatel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
kompetenci	kompetence	k1gFnSc6	kompetence
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vypočítáván	vypočítávat	k5eAaImNgInS	vypočítávat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
údajů	údaj	k1gInPc2	údaj
úřadů	úřada	k1gMnPc2	úřada
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
rozlišována	rozlišovat	k5eAaImNgFnS	rozlišovat
jako	jako	k9	jako
<g/>
:	:	kIx,	:
dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
–	–	k?	–
osoba	osoba	k1gFnSc1	osoba
setrvává	setrvávat	k5eAaImIp3nS	setrvávat
dobrovolně	dobrovolně	k6eAd1	dobrovolně
nezaměstnaná	zaměstnaný	k2eNgFnSc1d1	nezaměstnaná
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
přijmout	přijmout	k5eAaPmF	přijmout
práci	práce	k1gFnSc4	práce
za	za	k7c4	za
nabízenou	nabízený	k2eAgFnSc4d1	nabízená
mzdu	mzda	k1gFnSc4	mzda
(	(	kIx(	(
<g/>
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
se	se	k3xPyFc4	se
do	do	k7c2	do
statistik	statistika	k1gFnPc2	statistika
nezaměstnaných	zaměstnaný	k2eNgMnPc2d1	nezaměstnaný
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
výše	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
frikční	frikční	k2eAgFnSc1d1	frikční
(	(	kIx(	(
<g/>
dočasná	dočasný	k2eAgFnSc1d1	dočasná
<g/>
)	)	kIx)	)
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
krátkodobou	krátkodobý	k2eAgFnSc4d1	krátkodobá
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
spojenou	spojený	k2eAgFnSc4d1	spojená
zpravidla	zpravidla	k6eAd1	zpravidla
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
obdobím	období	k1gNnSc7	období
mezi	mezi	k7c7	mezi
ukončením	ukončení	k1gNnSc7	ukončení
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
a	a	k8xC	a
nalezením	nalezení	k1gNnSc7	nalezení
nového	nový	k2eAgNnSc2d1	nové
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
<g/>
;	;	kIx,	;
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejčastější	častý	k2eAgInSc4d3	nejčastější
typ	typ	k1gInSc4	typ
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ekonomiku	ekonomika	k1gFnSc4	ekonomika
neznamená	znamenat	k5eNaImIp3nS	znamenat
závažnější	závažný	k2eAgInSc1d2	závažnější
problém	problém	k1gInSc1	problém
systémová	systémový	k2eAgFnSc1d1	systémová
(	(	kIx(	(
<g/>
strukturální	strukturální	k2eAgFnSc1d1	strukturální
<g/>
)	)	kIx)	)
–	–	k?	–
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
způsobená	způsobený	k2eAgFnSc1d1	způsobená
změnou	změna	k1gFnSc7	změna
struktury	struktura	k1gFnSc2	struktura
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
;	;	kIx,	;
nezaměstnaný	nezaměstnaný	k1gMnSc1	nezaměstnaný
nemůže	moct	k5eNaImIp3nS	moct
sehnat	sehnat	k5eAaPmF	sehnat
práci	práce	k1gFnSc4	práce
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
oboru	obor	k1gInSc6	obor
<g/>
,	,	kIx,	,
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
nesouladem	nesoulad	k1gInSc7	nesoulad
nabídky	nabídka	k1gFnSc2	nabídka
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
poptávky	poptávka	k1gFnSc2	poptávka
po	po	k7c6	po
práci	práce	k1gFnSc6	práce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nadbytek	nadbytek	k1gInSc1	nadbytek
horníků	horník	k1gMnPc2	horník
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
dolů	dol	k1gInPc2	dol
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řešením	řešení	k1gNnSc7	řešení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rekvalifikace	rekvalifikace	k1gFnSc1	rekvalifikace
<g/>
.	.	kIx.	.
cyklická	cyklický	k2eAgFnSc1d1	cyklická
–	–	k?	–
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
průběhem	průběh	k1gInSc7	průběh
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
recesi	recese	k1gFnSc6	recese
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
méně	málo	k6eAd2	málo
lidí	člověk	k1gMnPc2	člověk
než	než	k8xS	než
v	v	k7c6	v
době	doba	k1gFnSc6	doba
konjunktury	konjunktura	k1gFnSc2	konjunktura
<g/>
.	.	kIx.	.
sezónní	sezónní	k2eAgMnSc1d1	sezónní
–	–	k?	–
souvisí	souviset	k5eAaImIp3nS	souviset
např.	např.	kA	např.
s	s	k7c7	s
ročním	roční	k2eAgNnSc7d1	roční
obdobím	období	k1gNnSc7	období
–	–	k?	–
třeba	třeba	k9	třeba
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
více	hodně	k6eAd2	hodně
správců	správce	k1gMnPc2	správce
lyžařských	lyžařský	k2eAgInPc2d1	lyžařský
vleků	vlek	k1gInPc2	vlek
než	než	k8xS	než
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
nezaměstnaných	zaměstnaný	k2eNgMnPc2d1	nezaměstnaný
stavařů	stavař	k1gMnPc2	stavař
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
část	část	k1gFnSc4	část
frikční	frikční	k2eAgFnSc2d1	frikční
nebo	nebo	k8xC	nebo
strukturální	strukturální	k2eAgFnSc2d1	strukturální
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Přirozená	přirozený	k2eAgFnSc1d1	přirozená
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
je	být	k5eAaImIp3nS	být
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
udržitelná	udržitelný	k2eAgFnSc1d1	udržitelná
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
odpovídající	odpovídající	k2eAgInSc1d1	odpovídající
potenciálnímu	potenciální	k2eAgInSc3d1	potenciální
produktu	produkt	k1gInSc3	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
výše	výše	k1gFnSc1	výše
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
ekonomiku	ekonomika	k1gFnSc4	ekonomika
jiná	jiný	k2eAgFnSc1d1	jiná
a	a	k8xC	a
nelze	lze	k6eNd1	lze
ani	ani	k9	ani
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tato	tento	k3xDgFnSc1	tento
míra	míra	k1gFnSc1	míra
byla	být	k5eAaImAgFnS	být
žádoucí	žádoucí	k2eAgFnSc1d1	žádoucí
–	–	k?	–
je	být	k5eAaImIp3nS	být
prostě	prostě	k9	prostě
přirozená	přirozený	k2eAgFnSc1d1	přirozená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přirozené	přirozený	k2eAgFnSc6d1	přirozená
míře	míra	k1gFnSc6	míra
tedy	tedy	k9	tedy
není	být	k5eNaImIp3nS	být
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
cyklická	cyklický	k2eAgFnSc1d1	cyklická
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
fluktuuje	fluktuovat	k5eAaImIp3nS	fluktuovat
okolo	okolo	k7c2	okolo
přirozené	přirozený	k2eAgFnSc2d1	přirozená
míry	míra	k1gFnSc2	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
díky	díky	k7c3	díky
hospodářským	hospodářský	k2eAgInPc3d1	hospodářský
cyklům	cyklus	k1gInPc3	cyklus
(	(	kIx(	(
<g/>
v	v	k7c4	v
recesi	recese	k1gFnSc4	recese
je	být	k5eAaImIp3nS	být
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
přirozená	přirozený	k2eAgFnSc1d1	přirozená
míra	míra	k1gFnSc1	míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
minimální	minimální	k2eAgFnSc6d1	minimální
mzdě	mzda	k1gFnSc6	mzda
–	–	k?	–
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgFnSc1d1	minimální
mzda	mzda	k1gFnSc1	mzda
umístěna	umístit	k5eAaPmNgFnS	umístit
nad	nad	k7c7	nad
tržní	tržní	k2eAgFnSc7d1	tržní
cenou	cena	k1gFnSc7	cena
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
přebytek	přebytek	k1gInSc1	přebytek
nabídky	nabídka	k1gFnSc2	nabídka
nad	nad	k7c7	nad
poptávkou	poptávka	k1gFnSc7	poptávka
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
někteří	některý	k3yIgMnPc1	některý
nezaměstnaní	nezaměstnaný	k1gMnPc1	nezaměstnaný
nezískají	získat	k5eNaPmIp3nP	získat
pracovní	pracovní	k2eAgNnPc4d1	pracovní
místa	místo	k1gNnPc4	místo
odbory	odbor	k1gInPc1	odbor
a	a	k8xC	a
kolektivní	kolektivní	k2eAgNnSc1d1	kolektivní
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
(	(	kIx(	(
<g/>
forma	forma	k1gFnSc1	forma
kartelu	kartel	k1gInSc2	kartel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
síle	síla	k1gFnSc3	síla
může	moct	k5eAaImIp3nS	moct
donutit	donutit	k5eAaPmF	donutit
zaměstnavatele	zaměstnavatel	k1gMnPc4	zaměstnavatel
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
mezd	mzda	k1gFnPc2	mzda
<g/>
)	)	kIx)	)
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
propouštění	propouštění	k1gNnSc3	propouštění
některých	některý	k3yIgMnPc2	některý
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
pro	pro	k7c4	pro
pokrytí	pokrytí	k1gNnSc4	pokrytí
zvýšených	zvýšený	k2eAgFnPc2d1	zvýšená
mezd	mzda	k1gFnPc2	mzda
mzdová	mzdový	k2eAgFnSc1d1	mzdová
rigidita	rigidita	k1gFnSc1	rigidita
mzdová	mzdový	k2eAgFnSc1d1	mzdová
renta	renta	k1gFnSc1	renta
–	–	k?	–
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
více	hodně	k6eAd2	hodně
než	než	k8xS	než
musí	muset	k5eAaImIp3nS	muset
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
více	hodně	k6eAd2	hodně
nežli	nežli	k8xS	nežli
je	být	k5eAaImIp3nS	být
transferový	transferový	k2eAgInSc1d1	transferový
výdělek	výdělek	k1gInSc1	výdělek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
je	být	k5eAaImIp3nS	být
určitým	určitý	k2eAgInSc7d1	určitý
způsobem	způsob	k1gInSc7	způsob
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
(	(	kIx(	(
<g/>
jeho	on	k3xPp3gInSc4	on
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
ztrátou	ztráta	k1gFnSc7	ztráta
<g/>
)	)	kIx)	)
náhrada	náhrada	k1gFnSc1	náhrada
lidí	člověk	k1gMnPc2	člověk
za	za	k7c4	za
stroje	stroj	k1gInPc4	stroj
-	-	kIx~	-
stroje	stroj	k1gInPc4	stroj
jsou	být	k5eAaImIp3nP	být
levnější	levný	k2eAgInPc1d2	levnější
a	a	k8xC	a
modernější	moderní	k2eAgFnSc1d2	modernější
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
jako	jako	k8xS	jako
taková	takový	k3xDgFnSc1	takový
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
tržních	tržní	k2eAgFnPc6d1	tržní
ekonomikách	ekonomika	k1gFnPc6	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
feudalismu	feudalismus	k1gInSc2	feudalismus
měli	mít	k5eAaImAgMnP	mít
všichni	všechen	k3xTgMnPc1	všechen
nevolníci	nevolník	k1gMnPc1	nevolník
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
půdě	půda	k1gFnSc3	půda
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
ne	ne	k9	ne
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
<g/>
)	)	kIx)	)
a	a	k8xC	a
potřebným	potřebný	k2eAgInPc3d1	potřebný
nástrojům	nástroj	k1gInPc3	nástroj
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohli	moct	k5eAaImAgMnP	moct
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
ve	v	k7c6	v
starověkých	starověký	k2eAgFnPc6d1	starověká
i	i	k8xC	i
novějších	nový	k2eAgFnPc6d2	novější
formách	forma	k1gFnPc6	forma
otrokářství	otrokářství	k1gNnSc2	otrokářství
nenechávali	nechávat	k5eNaImAgMnP	nechávat
majitelé	majitel	k1gMnPc1	majitel
otroků	otrok	k1gMnPc2	otrok
své	svůj	k3xOyFgMnPc4	svůj
otroky	otrok	k1gMnPc4	otrok
typicky	typicky	k6eAd1	typicky
zahálet	zahálet	k5eAaImF	zahálet
(	(	kIx(	(
<g/>
přinejhorším	přinejhorším	k6eAd1	přinejhorším
je	on	k3xPp3gNnPc4	on
mohli	moct	k5eAaImAgMnP	moct
prodat	prodat	k5eAaPmF	prodat
někomu	někdo	k3yInSc3	někdo
jinému	jiný	k2eAgMnSc3d1	jiný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Centrálně	centrálně	k6eAd1	centrálně
řízené	řízený	k2eAgFnSc2d1	řízená
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc6	jeho
satelitech	satelit	k1gInPc6	satelit
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
práci	práce	k1gFnSc4	práce
každému	každý	k3xTgMnSc3	každý
občanovi	občan	k1gMnSc3	občan
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
přezaměstnanosti	přezaměstnanost	k1gFnSc2	přezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČSSR	ČSSR	kA	ČSSR
existovala	existovat	k5eAaImAgFnS	existovat
pracovní	pracovní	k2eAgFnSc4d1	pracovní
povinnost	povinnost	k1gFnSc4	povinnost
a	a	k8xC	a
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
příživnictví	příživnictví	k1gNnSc2	příživnictví
<g/>
,	,	kIx,	,
trestající	trestající	k2eAgFnPc1d1	trestající
osoby	osoba	k1gFnPc1	osoba
"	"	kIx"	"
<g/>
soustavně	soustavně	k6eAd1	soustavně
se	se	k3xPyFc4	se
vyhýbající	vyhýbající	k2eAgFnSc3d1	vyhýbající
poctivé	poctivý	k2eAgFnSc3d1	poctivá
práci	práce	k1gFnSc3	práce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
umělé	umělý	k2eAgFnSc2d1	umělá
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nezaměstnaní	nezaměstnaný	k1gMnPc1	nezaměstnaný
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
registrováni	registrovat	k5eAaBmNgMnP	registrovat
u	u	k7c2	u
vládních	vládní	k2eAgInPc2d1	vládní
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgFnPc1d1	oficiální
statistiky	statistika	k1gFnPc1	statistika
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgNnSc1d1	nízké
i	i	k9	i
v	v	k7c6	v
kapitalistických	kapitalistický	k2eAgInPc6d1	kapitalistický
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
chudších	chudý	k2eAgFnPc2d2	chudší
zemí	zem	k1gFnPc2	zem
postrádá	postrádat	k5eAaImIp3nS	postrádat
moderní	moderní	k2eAgNnSc1d1	moderní
sociální	sociální	k2eAgNnSc1d1	sociální
zabezpečení	zabezpečení	k1gNnSc1	zabezpečení
včetně	včetně	k7c2	včetně
podpory	podpora	k1gFnSc2	podpora
v	v	k7c6	v
nezaměstnanosti	nezaměstnanost	k1gFnSc6	nezaměstnanost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
může	moct	k5eAaImIp3nS	moct
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
dovolit	dovolit	k5eAaPmF	dovolit
málokterý	málokterý	k3yIgMnSc1	málokterý
z	z	k7c2	z
jejich	jejich	k3xOp3gMnPc2	jejich
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
v	v	k7c6	v
bohatých	bohatý	k2eAgFnPc6d1	bohatá
zemích	zem	k1gFnPc6	zem
zůstali	zůstat	k5eAaPmAgMnP	zůstat
bez	bez	k7c2	bez
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
na	na	k7c4	na
méně	málo	k6eAd2	málo
kvalifikované	kvalifikovaný	k2eAgFnPc4d1	kvalifikovaná
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nezaměstnané	nezaměstnaný	k1gMnPc4	nezaměstnaný
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nezapočítají	započítat	k5eNaPmIp3nP	započítat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vysoká	vysoký	k2eAgFnSc1d1	vysoká
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
–	–	k?	–
historicky	historicky	k6eAd1	historicky
nový	nový	k2eAgInSc4d1	nový
jev	jev	k1gInSc4	jev
–	–	k?	–
stává	stávat	k5eAaImIp3nS	stávat
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jižních	jižní	k2eAgInPc6d1	jižní
státech	stát	k1gInPc6	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
nejen	nejen	k6eAd1	nejen
hospodářským	hospodářský	k2eAgInSc7d1	hospodářský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sociálním	sociální	k2eAgInSc7d1	sociální
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
další	další	k2eAgInSc1d1	další
ukazatel	ukazatel	k1gInSc1	ukazatel
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
i	i	k9	i
zaměstnanost	zaměstnanost	k1gFnSc4	zaměstnanost
dané	daný	k2eAgFnSc2d1	daná
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xS	jako
podíl	podíl	k1gInSc1	podíl
zaměstnaných	zaměstnaný	k1gMnPc2	zaměstnaný
ku	k	k7c3	k
všem	všecek	k3xTgFnPc3	všecek
osobám	osoba	k1gFnPc3	osoba
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
ve	v	k7c6	v
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
20	[number]	k4	20
až	až	k9	až
64	[number]	k4	64
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
EU	EU	kA	EU
přibližně	přibližně	k6eAd1	přibližně
68	[number]	k4	68
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
muži	muž	k1gMnSc3	muž
74	[number]	k4	74
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
62	[number]	k4	62
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
75	[number]	k4	75
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
EU	EU	kA	EU
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
zaměstnanost	zaměstnanost	k1gFnSc4	zaměstnanost
84	[number]	k4	84
<g/>
%	%	kIx~	%
při	při	k7c6	při
dokončeném	dokončený	k2eAgNnSc6d1	dokončené
terciárním	terciární	k2eAgNnSc6d1	terciární
vzdělání	vzdělání	k1gNnSc6	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnaných	zaměstnaný	k2eAgNnPc2d1	zaměstnané
na	na	k7c4	na
plný	plný	k2eAgInSc4d1	plný
pracovní	pracovní	k2eAgInSc4d1	pracovní
úvazek	úvazek	k1gInSc4	úvazek
či	či	k8xC	či
jen	jen	k9	jen
částečný	částečný	k2eAgMnSc1d1	částečný
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
mírně	mírně	k6eAd1	mírně
přes	přes	k7c4	přes
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
ČR	ČR	kA	ČR
nepracuje	pracovat	k5eNaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
krize	krize	k1gFnSc1	krize
Index	index	k1gInSc4	index
mizerie	mizerie	k1gFnSc2	mizerie
Inflace	inflace	k1gFnSc1	inflace
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
zaměstnanosti	zaměstnanost	k1gFnSc6	zaměstnanost
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Český	český	k2eAgInSc1d1	český
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věcí	věc	k1gFnPc2	věc
Vývoj	vývoj	k1gInSc1	vývoj
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
v	v	k7c6	v
ČR	ČR	kA	ČR
on-line	onin	k1gInSc5	on-lin
-	-	kIx~	-
Bluenomics	Bluenomics	k1gInSc4	Bluenomics
</s>
