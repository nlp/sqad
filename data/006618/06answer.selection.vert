<s>
Za	za	k7c4	za
vážný	vážný	k2eAgInSc4d1	vážný
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
problém	problém	k1gInSc4	problém
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nezaměstnaný	zaměstnaný	k2eNgMnSc1d1	nezaměstnaný
nemá	mít	k5eNaImIp3nS	mít
práci	práce	k1gFnSc4	práce
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
1	[number]	k4	1
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
