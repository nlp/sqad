<s>
Priština	Priština	k1gFnSc1	Priština
<g/>
,	,	kIx,	,
také	také	k9	také
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Pristina	Pristina	k1gFnSc1	Pristina
(	(	kIx(	(
<g/>
srbsky	srbsky	k6eAd1	srbsky
<g/>
:	:	kIx,	:
П	П	k?	П
nebo	nebo	k8xC	nebo
Priština	Priština	k1gFnSc1	Priština
<g/>
,	,	kIx,	,
albánsky	albánsky	k6eAd1	albánsky
<g/>
:	:	kIx,	:
Prishtinë	Prishtinë	k1gFnSc1	Prishtinë
nebo	nebo	k8xC	nebo
Prishtina	Prishtina	k1gFnSc1	Prishtina
<g/>
,	,	kIx,	,
audio	audio	k2eAgFnSc1d1	audio
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Kosova	Kosův	k2eAgNnSc2d1	Kosovo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
oficiálně	oficiálně	k6eAd1	oficiálně
prohlášeným	prohlášený	k2eAgInSc7d1	prohlášený
nezávislým	závislý	k2eNgInSc7d1	nezávislý
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
autonomní	autonomní	k2eAgFnSc7d1	autonomní
jednotkou	jednotka	k1gFnSc7	jednotka
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
dob	doba	k1gFnPc2	doba
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
existovalo	existovat	k5eAaImAgNnS	existovat
15	[number]	k4	15
km	km	kA	km
od	od	k7c2	od
Prištiny	Priština	k1gFnSc2	Priština
město	město	k1gNnSc1	město
Ulpiana	Ulpiana	k1gFnSc1	Ulpiana
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
za	za	k7c2	za
dob	doba	k1gFnPc2	doba
císaře	císař	k1gMnSc2	císař
Justiniána	Justinián	k1gMnSc2	Justinián
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
starého	starý	k1gMnSc2	starý
římského	římský	k2eAgNnSc2d1	římské
města	město	k1gNnSc2	město
lze	lze	k6eAd1	lze
stále	stále	k6eAd1	stále
spatřit	spatřit	k5eAaPmF	spatřit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Říma	Řím	k1gInSc2	Řím
se	se	k3xPyFc4	se
Priština	Priština	k1gFnSc1	Priština
dostala	dostat	k5eAaPmAgFnS	dostat
z	z	k7c2	z
ruin	ruina	k1gFnPc2	ruina
bývalého	bývalý	k2eAgNnSc2d1	bývalé
římského	římský	k2eAgNnSc2d1	římské
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
silnic	silnice	k1gFnPc2	silnice
vedoucích	vedoucí	k1gMnPc2	vedoucí
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
směrů	směr	k1gInPc2	směr
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
Priština	Priština	k1gFnSc1	Priština
stala	stát	k5eAaPmAgFnS	stát
důležitým	důležitý	k2eAgNnSc7d1	důležité
obchodním	obchodní	k2eAgNnSc7d1	obchodní
centrem	centrum	k1gNnSc7	centrum
na	na	k7c6	na
hlavních	hlavní	k2eAgFnPc6d1	hlavní
obchodních	obchodní	k2eAgFnPc6d1	obchodní
cestách	cesta	k1gFnPc6	cesta
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Kosově	Kosův	k2eAgNnSc6d1	Kosovo
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
Priština	Priština	k1gFnSc1	Priština
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
Kosova	Kosův	k2eAgFnSc1d1	Kosova
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
Albánie	Albánie	k1gFnSc2	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
spojila	spojit	k5eAaPmAgFnS	spojit
se	s	k7c7	s
Srbským	srbský	k2eAgNnSc7d1	srbské
královstvím	království	k1gNnSc7	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
nově	nově	k6eAd1	nově
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
Priština	Priština	k1gFnSc1	Priština
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
autonomního	autonomní	k2eAgInSc2d1	autonomní
státu	stát	k1gInSc2	stát
Kosova	Kosův	k2eAgInSc2d1	Kosův
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
24	[number]	k4	24
000	[number]	k4	000
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
nárůst	nárůst	k1gInSc1	nárůst
nastal	nastat	k5eAaPmAgInS	nastat
u	u	k7c2	u
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
albánského	albánský	k2eAgNnSc2d1	albánské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
z	z	k7c2	z
horských	horský	k2eAgFnPc2d1	horská
pastvin	pastvina	k1gFnPc2	pastvina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
rurálních	rurální	k2eAgFnPc2d1	rurální
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celé	celý	k2eAgFnSc2d1	celá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
;	;	kIx,	;
sestěhovávání	sestěhovávání	k1gNnSc1	sestěhovávání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
měst	město	k1gNnPc2	město
tak	tak	k6eAd1	tak
znamenalo	znamenat	k5eAaImAgNnS	znamenat
bouřlivý	bouřlivý	k2eAgInSc4d1	bouřlivý
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
zbudována	zbudován	k2eAgNnPc1d1	zbudováno
v	v	k7c6	v
Prištině	Priština	k1gFnSc6	Priština
pobočka	pobočka	k1gFnSc1	pobočka
Bělehradské	bělehradský	k2eAgFnSc2d1	Bělehradská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
protestech	protest	k1gInPc6	protest
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1968	[number]	k4	1968
postupně	postupně	k6eAd1	postupně
osamostatnila	osamostatnit	k5eAaPmAgNnP	osamostatnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osamostatnění	osamostatnění	k1gNnSc6	osamostatnění
a	a	k8xC	a
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
Republiky	republika	k1gFnSc2	republika
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Priština	Priština	k1gFnSc1	Priština
jejím	její	k3xOp3gNnSc7	její
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Priština	Priština	k1gFnSc1	Priština
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
572	[number]	k4	572
km	km	kA	km
<g/>
2	[number]	k4	2
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
652	[number]	k4	652
m.	m.	k?	m.
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c4	v
severovýchodní	severovýchodní	k2eAgFnPc4d1	severovýchodní
části	část	k1gFnPc4	část
Kosova	Kosův	k2eAgInSc2d1	Kosův
<g/>
.	.	kIx.	.
250	[number]	k4	250
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
srbského	srbský	k2eAgInSc2d1	srbský
Bělehradu	Bělehrad	k1gInSc2	Bělehrad
<g/>
,	,	kIx,	,
180	[number]	k4	180
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
bulharské	bulharský	k2eAgFnSc2d1	bulharská
Sofie	Sofia	k1gFnSc2	Sofia
a	a	k8xC	a
70	[number]	k4	70
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
makedonské	makedonský	k2eAgFnSc2d1	makedonská
Skopje	Skopje	k1gFnSc2	Skopje
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Prištiny	Priština	k1gFnSc2	Priština
je	být	k5eAaImIp3nS	být
dobrý	dobrý	k2eAgInSc4d1	dobrý
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
pohoří	pohoří	k1gNnSc4	pohoří
Šar	Šar	k1gFnSc2	Šar
planinu	planina	k1gFnSc4	planina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Prištiny	Priština	k1gFnSc2	Priština
leží	ležet	k5eAaImIp3nS	ležet
dvě	dva	k4xCgNnPc4	dva
další	další	k2eAgNnPc4d1	další
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
Obilić	Obilić	k1gFnPc4	Obilić
a	a	k8xC	a
Kosovo	Kosův	k2eAgNnSc4d1	Kosovo
Polje	Polj	k1gFnPc4	Polj
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
předměstími	předměstí	k1gNnPc7	předměstí
Prištiny	Priština	k1gFnSc2	Priština
<g/>
.	.	kIx.	.
</s>
<s>
Středem	středem	k7c2	středem
Prištiny	Priština	k1gFnSc2	Priština
protéká	protékat	k5eAaImIp3nS	protékat
jediná	jediný	k2eAgFnSc1d1	jediná
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
svedena	svést	k5eAaPmNgFnS	svést
do	do	k7c2	do
podzemních	podzemní	k2eAgInPc2d1	podzemní
tunelů	tunel	k1gInPc2	tunel
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
až	až	k9	až
za	za	k7c7	za
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
198	[number]	k4	198
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
tvoří	tvořit	k5eAaImIp3nP	tvořit
Albánci	Albánec	k1gMnPc1	Albánec
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
Srbové	Srb	k1gMnPc1	Srb
<g/>
,	,	kIx,	,
Bosňáci	Bosňáci	k?	Bosňáci
<g/>
,	,	kIx,	,
Romové	Rom	k1gMnPc1	Rom
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
srbské	srbský	k2eAgFnSc2d1	Srbská
menšiny	menšina	k1gFnSc2	menšina
ale	ale	k8xC	ale
od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
silně	silně	k6eAd1	silně
klesal	klesat	k5eAaImAgInS	klesat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
po	po	k7c6	po
kosovské	kosovský	k2eAgFnSc6d1	Kosovská
válce	válka	k1gFnSc6	válka
většina	většina	k1gFnSc1	většina
prištinských	prištinský	k2eAgMnPc2d1	prištinský
Srbů	Srb	k1gMnPc2	Srb
opustila	opustit	k5eAaPmAgFnS	opustit
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
nebo	nebo	k8xC	nebo
utekla	utéct	k5eAaPmAgFnS	utéct
do	do	k7c2	do
srbské	srbský	k2eAgFnSc2d1	Srbská
enklávy	enkláva	k1gFnSc2	enkláva
Gračanica	Gračanic	k1gInSc2	Gračanic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Prištině	Priština	k1gFnSc6	Priština
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
kosovská	kosovský	k2eAgFnSc1d1	Kosovská
vláda	vláda	k1gFnSc1	vláda
i	i	k8xC	i
Dočasná	dočasný	k2eAgFnSc1d1	dočasná
správní	správní	k2eAgFnSc1d1	správní
mise	mise	k1gFnSc1	mise
OSN	OSN	kA	OSN
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
registrovaných	registrovaný	k2eAgInPc2d1	registrovaný
podniků	podnik	k1gInPc2	podnik
v	v	k7c6	v
Prištině	Priština	k1gFnSc6	Priština
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
8725	[number]	k4	8725
<g/>
,	,	kIx,	,
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
75	[number]	k4	75
089	[number]	k4	089
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Prištinou	Priština	k1gFnSc7	Priština
a	a	k8xC	a
městem	město	k1gNnSc7	město
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
Polje	Polj	k1gInPc4	Polj
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
větší	veliký	k2eAgFnSc1d2	veliký
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
nádraží	nádraží	k1gNnSc1	nádraží
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Kosovo	Kosův	k2eAgNnSc1d1	Kosovo
Polje	Polj	k1gMnSc2	Polj
7	[number]	k4	7
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Prištiny	Priština	k1gFnSc2	Priština
<g/>
.	.	kIx.	.
</s>
<s>
Nádraží	nádraží	k1gNnSc1	nádraží
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Prištině	Priština	k1gFnSc6	Priština
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
jen	jen	k9	jen
pro	pro	k7c4	pro
místní	místní	k2eAgFnSc4d1	místní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
(	(	kIx(	(
<g/>
cca	cca	kA	cca
19	[number]	k4	19
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Priština	Priština	k1gFnSc1	Priština
<g/>
.	.	kIx.	.
</s>
<s>
Kosovské	kosovský	k2eAgNnSc1d1	kosovské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Prištině	Priština	k1gFnSc6	Priština
má	mít	k5eAaImIp3nS	mít
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
archeologické	archeologický	k2eAgFnPc4d1	archeologická
sbírky	sbírka	k1gFnPc4	sbírka
a	a	k8xC	a
etnologické	etnologický	k2eAgInPc4d1	etnologický
artefakty	artefakt	k1gInPc4	artefakt
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
Hodinové	hodinový	k2eAgFnSc2d1	hodinová
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Sahat	sahat	k5eAaImF	sahat
kula	kula	k1gFnSc1	kula
<g/>
,	,	kIx,	,
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k6eAd1	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
byla	být	k5eAaImAgFnS	být
věž	věž	k1gFnSc1	věž
rekonstruována	rekonstruován	k2eAgFnSc1d1	rekonstruována
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
zvon	zvon	k1gInSc1	zvon
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
výroby	výroba	k1gFnSc2	výroba
1764	[number]	k4	1764
byl	být	k5eAaImAgInS	být
dovezen	dovézt	k5eAaPmNgInS	dovézt
z	z	k7c2	z
Moldavska	Moldavsko	k1gNnSc2	Moldavsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
starý	starý	k2eAgInSc1d1	starý
hodinový	hodinový	k2eAgInSc1d1	hodinový
stroj	stroj	k1gInSc1	stroj
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
elektrickým	elektrický	k2eAgInSc7d1	elektrický
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
správní	správní	k2eAgNnSc4d1	správní
<g/>
,	,	kIx,	,
vzdělávací	vzdělávací	k2eAgNnSc4d1	vzdělávací
a	a	k8xC	a
kulturní	kulturní	k2eAgNnSc4d1	kulturní
centrum	centrum	k1gNnSc4	centrum
celého	celý	k2eAgInSc2d1	celý
Kosova	Kosův	k2eAgInSc2d1	Kosův
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
Prištinská	Prištinský	k2eAgFnSc1d1	Prištinský
univerzita	univerzita	k1gFnSc1	univerzita
(	(	kIx(	(
<g/>
Universiteti	Universitet	k1gMnPc1	Universitet
i	i	k8xC	i
Prishtinës	Prishtinës	k1gInSc1	Prishtinës
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
ještě	ještě	k6eAd1	ještě
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
vysokým	vysoký	k2eAgFnPc3d1	vysoká
školám	škola	k1gFnPc3	škola
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
parků	park	k1gInPc2	park
a	a	k8xC	a
městských	městský	k2eAgInPc2d1	městský
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Parku	park	k1gInSc2	park
i	i	k9	i
Qytetit	Qytetit	k1gFnSc1	Qytetit
(	(	kIx(	(
<g/>
Městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
jediným	jediný	k2eAgNnPc3d1	jediné
skutečným	skutečný	k2eAgNnPc3d1	skutečné
zeleným	zelený	k2eAgNnPc3d1	zelené
místům	místo	k1gNnPc3	místo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
Gërmia	Gërmia	k1gFnSc1	Gërmia
park	park	k1gInSc1	park
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vybudování	vybudování	k1gNnSc6	vybudování
nového	nový	k2eAgNnSc2d1	nové
náměstí	náměstí	k1gNnSc2	náměstí
Matky	matka	k1gFnSc2	matka
Terezy	Tereza	k1gFnSc2	Tereza
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
vysázeno	vysázet	k5eAaPmNgNnS	vysázet
mnoho	mnoho	k4c1	mnoho
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
květin	květina	k1gFnPc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Basketbal	basketbal	k1gInSc1	basketbal
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgInPc2d3	nejpopulárnější
sportů	sport	k1gInPc2	sport
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sport	sport	k1gInSc1	sport
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Prištině	Priština	k1gFnSc6	Priština
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
dvěma	dva	k4xCgInPc7	dva
týmy	tým	k1gInPc7	tým
<g/>
.	.	kIx.	.
</s>
<s>
Fotbal	fotbal	k1gInSc1	fotbal
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
FC	FC	kA	FC
Prishtina	Prishtina	k1gFnSc1	Prishtina
hraje	hrát	k5eAaImIp3nS	hrát
své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
na	na	k7c6	na
městském	městský	k2eAgInSc6d1	městský
stadionu	stadion	k1gInSc6	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
sportem	sport	k1gInSc7	sport
ještě	ještě	k9	ještě
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
házená	házený	k2eAgFnSc1d1	házená
<g/>
.	.	kIx.	.
</s>
<s>
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Tirana	Tirana	k1gFnSc1	Tirana
<g/>
,	,	kIx,	,
Albánie	Albánie	k1gFnSc1	Albánie
Podgorica	Podgorica	k1gFnSc1	Podgorica
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
Ankara	Ankara	k1gFnSc1	Ankara
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Priština	Priština	k1gFnSc1	Priština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Galerie	galerie	k1gFnSc1	galerie
Priština	Priština	k1gFnSc1	Priština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Priština	Priština	k1gFnSc1	Priština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
