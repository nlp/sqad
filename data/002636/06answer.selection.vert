<s>
Priština	Priština	k1gFnSc1	Priština
<g/>
,	,	kIx,	,
také	také	k9	také
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Pristina	Pristina	k1gFnSc1	Pristina
(	(	kIx(	(
<g/>
srbsky	srbsky	k6eAd1	srbsky
<g/>
:	:	kIx,	:
П	П	k?	П
nebo	nebo	k8xC	nebo
Priština	Priština	k1gFnSc1	Priština
<g/>
,	,	kIx,	,
albánsky	albánsky	k6eAd1	albánsky
<g/>
:	:	kIx,	:
Prishtinë	Prishtinë	k1gFnSc1	Prishtinë
nebo	nebo	k8xC	nebo
Prishtina	Prishtina	k1gFnSc1	Prishtina
<g/>
,	,	kIx,	,
audio	audio	k2eAgFnSc1d1	audio
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Kosova	Kosův	k2eAgNnSc2d1	Kosovo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
oficiálně	oficiálně	k6eAd1	oficiálně
prohlášeným	prohlášený	k2eAgInSc7d1	prohlášený
nezávislým	závislý	k2eNgInSc7d1	nezávislý
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
autonomní	autonomní	k2eAgFnSc7d1	autonomní
jednotkou	jednotka	k1gFnSc7	jednotka
Srbska	Srbsko	k1gNnSc2	Srbsko
<g/>
.	.	kIx.	.
</s>
