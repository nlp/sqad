<s>
Arménské	arménský	k2eAgNnSc1d1	arménské
písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořen	k2eAgNnSc1d1	vytvořeno
Mesropem	Mesrop	k1gMnSc7	Mesrop
Maštocem	Maštoce	k1gMnSc7	Maštoce
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
405	[number]	k4	405
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
napsat	napsat	k5eAaPmF	napsat
arménský	arménský	k2eAgInSc4d1	arménský
překlad	překlad	k1gInSc4	překlad
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
písmen	písmeno	k1gNnPc2	písmeno
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
abecedy	abeceda	k1gFnSc2	abeceda
řecké	řecký	k2eAgFnSc2d1	řecká
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
oba	dva	k4xCgInPc1	dva
dnešní	dnešní	k2eAgInPc1d1	dnešní
dialekty	dialekt	k1gInPc1	dialekt
arménštiny	arménština	k1gFnSc2	arménština
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
i	i	k8xC	i
východní	východní	k2eAgFnSc1d1	východní
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
stejnou	stejný	k2eAgFnSc4d1	stejná
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
,	,	kIx,	,
výslovnost	výslovnost	k1gFnSc4	výslovnost
některých	některý	k3yIgNnPc2	některý
písmen	písmeno	k1gNnPc2	písmeno
je	být	k5eAaImIp3nS	být
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
nejen	nejen	k6eAd1	nejen
pořadí	pořadí	k1gNnSc1	pořadí
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
nových	nový	k2eAgNnPc2d1	nové
písmen	písmeno	k1gNnPc2	písmeno
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
mohly	moct	k5eAaImAgFnP	moct
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
cizí	cizí	k2eAgFnPc1d1	cizí
hlásky	hláska	k1gFnPc1	hláska
<g/>
.	.	kIx.	.
</s>
<s>
Znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
i	i	k9	i
na	na	k7c4	na
zápis	zápis	k1gInSc4	zápis
arménských	arménský	k2eAgFnPc2d1	arménská
číslic	číslice	k1gFnPc2	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Arménským	arménský	k2eAgNnSc7d1	arménské
písmem	písmo	k1gNnSc7	písmo
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
psalo	psát	k5eAaImAgNnS	psát
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
kurdština	kurdština	k1gFnSc1	kurdština
během	během	k7c2	během
sovětské	sovětský	k2eAgFnSc2d1	sovětská
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
lomavrenština	lomavrenština	k1gFnSc1	lomavrenština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Arménština	arménština	k1gFnSc1	arménština
Arménské	arménský	k2eAgFnSc2d1	arménská
číslice	číslice	k1gFnSc2	číslice
Gruzínské	gruzínský	k2eAgNnSc4d1	gruzínské
písmo	písmo	k1gNnSc4	písmo
</s>
