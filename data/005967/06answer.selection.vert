<s>
Arménské	arménský	k2eAgNnSc1d1	arménské
písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořen	k2eAgNnSc1d1	vytvořeno
Mesropem	Mesrop	k1gMnSc7	Mesrop
Maštocem	Maštoce	k1gMnSc7	Maštoce
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
405	[number]	k4	405
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
napsat	napsat	k5eAaPmF	napsat
arménský	arménský	k2eAgInSc4d1	arménský
překlad	překlad	k1gInSc4	překlad
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
