<s>
Laser	laser	k1gInSc1
game	game	k1gInSc1
</s>
<s>
Typická	typický	k2eAgFnSc1d1
vesta	vesta	k1gFnSc1
pro	pro	k7c4
laser	laser	k1gInSc4
game	game	k1gInSc4
se	s	k7c7
čtyřmi	čtyři	k4xCgInPc7
senzory	senzor	k1gInPc7
</s>
<s>
Laser	laser	k1gInSc1
game	game	k1gInSc1
je	být	k5eAaImIp3nS
týmová	týmový	k2eAgFnSc1d1
sportovně	sportovně	k6eAd1
společenská	společenský	k2eAgFnSc1d1
hra	hra	k1gFnSc1
využívající	využívající	k2eAgFnSc1d1
moderní	moderní	k2eAgFnSc4d1
technologie	technologie	k1gFnSc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
laseru	laser	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
prvky	prvek	k1gInPc7
sci-fi	sci-fi	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
hře	hra	k1gFnSc6
se	se	k3xPyFc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
laserových	laserový	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
hráči	hráč	k1gMnPc1
snaží	snažit	k5eAaImIp3nP
získat	získat	k5eAaPmF
co	co	k9
nejvíce	hodně	k6eAd3,k6eAd1
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
plní	plnit	k5eAaImIp3nP
jiné	jiný	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
(	(	kIx(
<g/>
dobytí	dobytí	k1gNnSc2
základny	základna	k1gFnSc2
<g/>
,	,	kIx,
odcizení	odcizení	k1gNnSc1
standarty	standarta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
jak	jak	k6eAd1
v	v	k7c6
uzavřených	uzavřený	k2eAgInPc6d1
prostorech	prostor	k1gInPc6
tzv.	tzv.	kA
arénách	aréna	k1gFnPc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
venku	venku	k6eAd1
na	na	k7c6
volném	volný	k2eAgNnSc6d1
prostranství	prostranství	k1gNnSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
mnoho	mnoho	k4c4
společností	společnost	k1gFnPc2
zabývající	zabývající	k2eAgFnSc2d1
se	se	k3xPyFc4
výrobou	výroba	k1gFnSc7
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc1
historie	historie	k1gFnSc2
hry	hra	k1gFnSc2
se	se	k3xPyFc4
vztahují	vztahovat	k5eAaImIp3nP
do	do	k7c2
roku	rok	k1gInSc2
1979	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
na	na	k7c4
trh	trh	k1gInSc4
hračka	hračka	k1gFnSc1
Star	star	k1gInSc1
Trek	Trek	k1gMnSc1
Electronic	Electronice	k1gFnPc2
Phaser	Phaser	k1gInSc4
od	od	k7c2
společnosti	společnost	k1gFnSc2
South	South	k1gInSc1
end	end	k?
Electronics	Electronics	k1gInSc1
Miltona	Milton	k1gMnSc2
Bradleyho	Bradley	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
odlišný	odlišný	k2eAgInSc1d1
název	název	k1gInSc1
(	(	kIx(
<g/>
laser	laser	k1gInSc1
game	game	k1gInSc1
<g/>
,	,	kIx,
laser	laser	k1gInSc1
tag	tag	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Laser	laser	k1gInSc1
game	game	k1gInSc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
hra	hra	k1gFnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
volnočasové	volnočasový	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
na	na	k7c6
komerční	komerční	k2eAgFnSc6d1
bázi	báze	k1gFnSc6
a	a	k8xC
aktuálně	aktuálně	k6eAd1
neexistuje	existovat	k5eNaImIp3nS
žádná	žádný	k3yNgFnSc1
asociace	asociace	k1gFnSc1
sdružující	sdružující	k2eAgFnSc2d1
arény	aréna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalitou	kvalita	k1gFnSc7
se	se	k3xPyFc4
různé	různý	k2eAgFnPc1d1
arény	aréna	k1gFnPc1
liší	lišit	k5eAaImIp3nP
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
použitým	použitý	k2eAgNnSc7d1
vybavením	vybavení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
Aréna	aréna	k1gFnSc1
byla	být	k5eAaImAgFnS
postavena	postavit	k5eAaPmNgFnS
na	na	k7c6
Národní	národní	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
však	však	k9
již	již	k6eAd1
nefunguje	fungovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
Laser	laser	k1gInSc1
Aréna	aréna	k1gFnSc1
dnešního	dnešní	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
moderním	moderní	k2eAgInSc7d1
systémem	systém	k1gInSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
realizována	realizovat	k5eAaBmNgFnS
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Česka	Česko	k1gNnSc2
a	a	k8xC
Slovenska	Slovensko	k1gNnSc2
v	v	k7c4
Laser	laser	k1gInSc4
Game	game	k1gInSc4
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
probíhá	probíhat	k5eAaImIp3nS
každoročně	každoročně	k6eAd1
v	v	k7c6
16	#num#	k4
Arénách	aréna	k1gFnPc6
Mistrovství	mistrovství	k1gNnSc2
Česka	Česko	k1gNnSc2
a	a	k8xC
Slovenska	Slovensko	k1gNnSc2
v	v	k7c4
Laser	laser	k1gInSc4
Game	game	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
odehrávají	odehrávat	k5eAaImIp3nP
kvalifikační	kvalifikační	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
přihlášených	přihlášený	k2eAgInPc2d1
týmů	tým	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězné	vítězný	k2eAgInPc1d1
týmy	tým	k1gInPc1
ze	z	k7c2
všech	všecek	k3xTgFnPc2
Arén	aréna	k1gFnPc2
se	se	k3xPyFc4
utkají	utkat	k5eAaPmIp3nP
proti	proti	k7c3
sobě	se	k3xPyFc3
na	na	k7c6
dvoudenním	dvoudenní	k2eAgNnSc6d1
Mistrovství	mistrovství	k1gNnSc6
v	v	k7c6
Košicích	Košice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
ročník	ročník	k1gInSc4
se	se	k3xPyFc4
nesl	nést	k5eAaImAgMnS
více	hodně	k6eAd2
v	v	k7c6
komunitním	komunitní	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
ročník	ročník	k1gInSc1
již	již	k6eAd1
slibuje	slibovat	k5eAaImIp3nS
vítězi	vítěz	k1gMnSc3
celého	celý	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
odměnu	odměna	k1gFnSc4
100	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Registrace	registrace	k1gFnSc1
týmů	tým	k1gInPc2
běží	běžet	k5eAaImIp3nS
většinou	většinou	k6eAd1
do	do	k7c2
května	květen	k1gInSc2
<g/>
,	,	kIx,
do	do	k7c2
srpna	srpen	k1gInSc2
probíhají	probíhat	k5eAaImIp3nP
kvalifikační	kvalifikační	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
a	a	k8xC
ke	k	k7c3
konci	konec	k1gInSc3
září	září	k1gNnSc2
se	se	k3xPyFc4
koná	konat	k5eAaImIp3nS
dvoudenní	dvoudenní	k2eAgNnSc1d1
Mistrovství	mistrovství	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
výrobců	výrobce	k1gMnPc2
laserového	laserový	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
–	–	k?
v	v	k7c6
Evropě	Evropa	k1gFnSc6
jsou	být	k5eAaImIp3nP
nejběžnější	běžný	k2eAgInSc1d3
holandský	holandský	k2eAgInSc1d1
LaserMaxx	LaserMaxx	k1gInSc1
<g/>
,	,	kIx,
australský	australský	k2eAgInSc1d1
Laser	laser	k1gInSc1
Force	force	k1gFnSc2
nebo	nebo	k8xC
Deltra	Deltrum	k1gNnSc2
Strike	Strik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
vývoji	vývoj	k1gInPc7
na	na	k7c6
poli	pole	k1gNnSc6
Laser	laser	k1gInSc1
Game	game	k1gInSc1
systémů	systém	k1gInPc2
věnuje	věnovat	k5eAaPmIp3nS,k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
jediná	jediný	k2eAgFnSc1d1
firma	firma	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Arény	aréna	k1gFnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Aktuálně	aktuálně	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
37	#num#	k4
Laser	laser	k1gInSc4
Arén	aréna	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
22	#num#	k4
je	být	k5eAaImIp3nS
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://lasertagmuseum.com	http://lasertagmuseum.com	k1gInSc1
<g/>
↑	↑	k?
https://www.mcslg.cz/	https://www.mcslg.cz/	k?
<g/>
↑	↑	k?
http://lasergameareny.cz/	http://lasergameareny.cz/	k?
<g/>
↑	↑	k?
http://areny.lasergameareny.cz/	http://areny.lasergameareny.cz/	k?
</s>
