<s>
Calvin	Calvina	k1gFnPc2	Calvina
Klein	Klein	k1gMnSc1	Klein
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1942	[number]	k4	1942
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
módní	módní	k2eAgMnSc1d1	módní
návrhář	návrhář	k1gMnSc1	návrhář
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
maďarského	maďarský	k2eAgMnSc2d1	maďarský
emigranta	emigrant	k1gMnSc2	emigrant
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
minimalista	minimalista	k1gMnSc1	minimalista
a	a	k8xC	a
mistr	mistr	k1gMnSc1	mistr
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
také	také	k9	také
proslavený	proslavený	k2eAgInSc4d1	proslavený
svými	svůj	k3xOyFgInPc7	svůj
provokativními	provokativní	k2eAgInPc7d1	provokativní
návrhy	návrh	k1gInPc7	návrh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
založil	založit	k5eAaPmAgInS	založit
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Barrym	Barrym	k1gInSc4	Barrym
Schwartzem	Schwartz	k1gInSc7	Schwartz
společnost	společnost	k1gFnSc1	společnost
Calvin	Calvina	k1gFnPc2	Calvina
Klein	Klein	k1gMnSc1	Klein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
specializovala	specializovat	k5eAaBmAgFnS	specializovat
na	na	k7c4	na
džínové	džínový	k2eAgInPc4d1	džínový
oděvy	oděv	k1gInPc4	oděv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
skandál	skandál	k1gInSc4	skandál
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
herečka	herečka	k1gFnSc1	herečka
Brooke	Brook	k1gFnSc2	Brook
Shields	Shieldsa	k1gFnPc2	Shieldsa
na	na	k7c6	na
plakátech	plakát	k1gInPc6	plakát
ukázala	ukázat	k5eAaPmAgNnP	ukázat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
kusu	kus	k1gInSc6	kus
oblečení	oblečení	k1gNnSc2	oblečení
–	–	k?	–
džínách	džíny	k1gFnPc6	džíny
Calvin	Calvina	k1gFnPc2	Calvina
Klein	Klein	k1gMnSc1	Klein
<g/>
.	.	kIx.	.
</s>
<s>
Kolekci	kolekce	k1gFnSc4	kolekce
elastického	elastický	k2eAgNnSc2d1	elastické
spodního	spodní	k2eAgNnSc2d1	spodní
prádla	prádlo	k1gNnSc2	prádlo
CK	CK	kA	CK
předváděla	předvádět	k5eAaImAgFnS	předvádět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
modelka	modelka	k1gFnSc1	modelka
Kate	kat	k1gInSc5	kat
Moss	Mossa	k1gFnPc2	Mossa
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
CK	CK	kA	CK
jsou	být	k5eAaImIp3nP	být
unisex	unisex	k1gInSc4	unisex
–	–	k?	–
jeho	on	k3xPp3gNnSc2	on
trička	tričko	k1gNnSc2	tričko
i	i	k9	i
džíny	džíny	k1gFnPc1	džíny
mohou	moct	k5eAaImIp3nP	moct
nosit	nosit	k5eAaImF	nosit
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
prodal	prodat	k5eAaPmAgMnS	prodat
svoji	svůj	k3xOyFgFnSc4	svůj
společnost	společnost	k1gFnSc4	společnost
koncernu	koncern	k1gInSc2	koncern
Phillips-VanHeusen	Phillips-VanHeusna	k1gFnPc2	Phillips-VanHeusna
za	za	k7c4	za
430	[number]	k4	430
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
módní	módní	k2eAgInSc4d1	módní
dům	dům	k1gInSc4	dům
Calvin	Calvina	k1gFnPc2	Calvina
Klein	Klein	k1gMnSc1	Klein
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
kolekce	kolekce	k1gFnSc1	kolekce
známí	známý	k2eAgMnPc1d1	známý
designéři	designér	k1gMnPc1	designér
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgFnPc7	jaký
jsou	být	k5eAaImIp3nP	být
Jil	Jil	k1gFnPc1	Jil
Sander	Sandra	k1gFnPc2	Sandra
<g/>
,	,	kIx,	,
Italo	Italo	k1gNnSc4	Italo
Zucchelli	Zucchelle	k1gFnSc4	Zucchelle
a	a	k8xC	a
Romeo	Romeo	k1gMnSc1	Romeo
Gigli	Gigl	k1gMnPc1	Gigl
<g/>
.	.	kIx.	.
</s>
<s>
Tvářemi	tvář	k1gFnPc7	tvář
kolekcí	kolekce	k1gFnPc2	kolekce
se	se	k3xPyFc4	se
stávaly	stávat	k5eAaImAgFnP	stávat
slavné	slavný	k2eAgFnPc1d1	slavná
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Marky	marka	k1gFnSc2	marka
Mark	Mark	k1gMnSc1	Mark
nebo	nebo	k8xC	nebo
Kate	kat	k1gInSc5	kat
Moss	Moss	k1gInSc4	Moss
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
tváří	tvář	k1gFnSc7	tvář
parfému	parfém	k1gInSc2	parfém
Obsession	Obsession	k1gInSc1	Obsession
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Natalia	Natalia	k1gFnSc1	Natalia
Vodianova	Vodianův	k2eAgFnSc1d1	Vodianova
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Beckham	Beckham	k1gInSc1	Beckham
nebo	nebo	k8xC	nebo
švédský	švédský	k2eAgMnSc1d1	švédský
fotbalista	fotbalista	k1gMnSc1	fotbalista
Freddie	Freddie	k1gFnSc2	Freddie
Ljungberg	Ljungberg	k1gMnSc1	Ljungberg
<g/>
.	.	kIx.	.
</s>
<s>
Calvin	Calvina	k1gFnPc2	Calvina
Klein	Klein	k1gMnSc1	Klein
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
Flore	Flor	k1gInSc5	Flor
a	a	k8xC	a
Lea	Lea	k1gFnSc1	Lea
Kleinových	Kleinových	k2eAgFnSc1d1	Kleinových
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
přistěhovalec	přistěhovalec	k1gMnSc1	přistěhovalec
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
dcera	dcera	k1gFnSc1	dcera
Rakušanky	Rakušanka	k1gFnSc2	Rakušanka
a	a	k8xC	a
Američana	Američan	k1gMnSc4	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
střední	střední	k2eAgFnSc4d1	střední
i	i	k8xC	i
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
designu	design	k1gInSc2	design
<g/>
.	.	kIx.	.
</s>
<s>
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
neúspěchu	neúspěch	k1gInSc3	neúspěch
ukončil	ukončit	k5eAaPmAgMnS	ukončit
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
na	na	k7c6	na
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Fashion	Fashion	k1gInSc1	Fashion
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
(	(	kIx(	(
<g/>
Newyorský	newyorský	k2eAgInSc4d1	newyorský
institut	institut	k1gInSc4	institut
módy	móda	k1gFnSc2	móda
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studie	k1gFnPc6	studie
strávil	strávit	k5eAaPmAgInS	strávit
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
navrhováním	navrhování	k1gNnSc7	navrhování
oblečení	oblečení	k1gNnSc4	oblečení
do	do	k7c2	do
newyorských	newyorský	k2eAgInPc2d1	newyorský
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
založil	založit	k5eAaPmAgInS	založit
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
Barrym	Barrym	k1gInSc4	Barrym
Schwartzem	Schwartz	k1gInSc7	Schwartz
společnost	společnost	k1gFnSc1	společnost
Calvin	Calvina	k1gFnPc2	Calvina
Klein	Klein	k1gMnSc1	Klein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
se	se	k3xPyFc4	se
Calvin	Calvina	k1gFnPc2	Calvina
Klein	Klein	k1gMnSc1	Klein
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
návrhářkou	návrhářka	k1gFnSc7	návrhářka
Jayne	Jayn	k1gInSc5	Jayn
Centre	centr	k1gInSc5	centr
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
měli	mít	k5eAaImAgMnP	mít
pouze	pouze	k6eAd1	pouze
dceru	dcera	k1gFnSc4	dcera
Marci	Marek	k1gMnPc1	Marek
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
producentkou	producentka	k1gFnSc7	producentka
Saturday	Saturdaa	k1gFnSc2	Saturdaa
Night	Night	k2eAgInSc4d1	Night
Live	Live	k1gInSc4	Live
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
Calvin	Calvina	k1gFnPc2	Calvina
a	a	k8xC	a
Jayne	Jayn	k1gInSc5	Jayn
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
Klein	Klein	k1gMnSc1	Klein
znovu	znovu	k6eAd1	znovu
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
asistentkou	asistentka	k1gFnSc7	asistentka
Kelly	Kella	k1gMnSc2	Kella
Rectorovou	Rectorův	k2eAgFnSc7d1	Rectorův
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
druhé	druhý	k4xOgNnSc1	druhý
manželství	manželství	k1gNnSc1	manželství
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
déle	dlouho	k6eAd2	dlouho
<g/>
,	,	kIx,	,
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
Jayne	Jayn	k1gMnSc5	Jayn
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
také	také	k9	také
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Calvin	Calvina	k1gFnPc2	Calvina
Klein	Klein	k1gMnSc1	Klein
(	(	kIx(	(
<g/>
fashion	fashion	k1gInSc1	fashion
designer	designer	k1gInSc1	designer
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
