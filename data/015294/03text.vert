<s>
Iga	Iga	k?
(	(	kIx(
<g/>
provincie	provincie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Mapa	mapa	k1gFnSc1
japonských	japonský	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
se	s	k7c7
zvýrazněnou	zvýrazněný	k2eAgFnSc7d1
provincií	provincie	k1gFnSc7
Iga	Iga	k1gFnSc2
</s>
<s>
Hrad	hrad	k1gInSc1
Ueno	Ueno	k6eAd1
</s>
<s>
Provincie	provincie	k1gFnSc1
Iga	Iga	k1gFnSc2
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
伊	伊	k?
<g/>
;	;	kIx,
Iga	Iga	k1gMnSc1
no	no	k9
kuni	kuni	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rovněž	rovněž	k9
nazývaná	nazývaný	k2eAgFnSc1d1
Išú	Išú	k1gFnSc1
(	(	kIx(
<g/>
伊	伊	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
stará	starý	k2eAgFnSc1d1
japonská	japonský	k2eAgFnSc1d1
provincie	provincie	k1gFnSc1
rozkládající	rozkládající	k2eAgFnSc1d1
se	se	k3xPyFc4
na	na	k7c6
místě	místo	k1gNnSc6
západní	západní	k2eAgFnSc2d1
části	část	k1gFnSc2
dnešní	dnešní	k2eAgFnSc2d1
prefektury	prefektura	k1gFnSc2
Mie	Mie	k1gFnSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Honšú	Honšú	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iga	Iga	k1gFnSc1
sousedila	sousedit	k5eAaImAgFnS
s	s	k7c7
provinciemi	provincie	k1gFnPc7
Ise	Ise	k1gFnSc1
<g/>
,	,	kIx,
Jamaširo	Jamaširo	k1gNnSc1
<g/>
,	,	kIx,
Jamato	Jamat	k2eAgNnSc1d1
a	a	k8xC
Ómi	Ómi	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Provincii	provincie	k1gFnSc3
Iga	Iga	k1gFnSc1
byla	být	k5eAaImAgFnS
proslulá	proslulý	k2eAgFnSc1d1
svými	svůj	k3xOyFgMnPc7
klany	klan	k1gInPc7
nindžů	nindža	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
nejslavnějším	slavný	k2eAgMnSc7d3
představitelem	představitel	k1gMnSc7
byl	být	k5eAaImAgMnS
Hattori	Hattore	k1gFnSc4
Hanzó	Hanzó	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iga	Iga	k1gFnSc1
byla	být	k5eAaImAgFnS
rovněž	rovněž	k9
rodištěm	rodiště	k1gNnSc7
slavného	slavný	k2eAgMnSc2d1
básníka	básník	k1gMnSc2
haiku	haik	k1gInSc2
Macuo	Macuo	k1gMnSc1
Bašóa	Bašóa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
provincie	provincie	k1gFnSc2
bylo	být	k5eAaImAgNnS
hradní	hradní	k2eAgNnSc1d1
město	město	k1gNnSc1
Ueno	Ueno	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Iga	Iga	k1gFnSc2
Province	province	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Staré	Staré	k2eAgFnSc2d1
japonské	japonský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
</s>
<s>
Aki	Aki	k?
·	·	k?
Awa	Awa	k1gFnSc1
(	(	kIx(
<g/>
Kantó	Kantó	k1gFnSc1
<g/>
)	)	kIx)
·	·	k?
Awa	Awa	k1gFnSc1
(	(	kIx(
<g/>
Šikoku	Šikok	k1gInSc2
<g/>
)	)	kIx)
·	·	k?
Awadži	Awadž	k1gFnSc6
·	·	k?
Biččú	Biččú	k1gFnPc2
·	·	k?
Bingo	bingo	k1gNnSc4
·	·	k?
Bizen	Bizen	k1gInSc1
·	·	k?
Bungo	Bungo	k6eAd1
·	·	k?
Buzen	buzen	k2eAgInSc1d1
·	·	k?
Cušima	Cušima	k1gFnSc1
·	·	k?
Čikugo	Čikugo	k6eAd1
·	·	k?
Čikuzen	Čikuzen	k2eAgInSc1d1
·	·	k?
Čišima	Čišima	k1gFnSc1
·	·	k?
Dewa	Dewa	k1gMnSc1
·	·	k?
Eččú	Eččú	k1gMnSc1
·	·	k?
Ečigo	Ečigo	k6eAd1
·	·	k?
Ečizen	Ečizna	k1gFnPc2
·	·	k?
Harima	Harim	k1gMnSc2
·	·	k?
Hida	Hidus	k1gMnSc2
·	·	k?
Hidaka	Hidak	k1gMnSc2
·	·	k?
Higo	Higo	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
·	·	k?
Hitači	Hitač	k1gInSc6
·	·	k?
Hizen	Hizen	k1gInSc1
·	·	k?
Hjúga	Hjúga	k1gFnSc1
·	·	k?
Hóki	Hók	k1gFnSc2
·	·	k?
Iburi	Ibur	k1gFnSc2
·	·	k?
Iga	Iga	k1gMnSc1
·	·	k?
Ijo	Ijo	k1gMnSc1
·	·	k?
Iki	Iki	k1gMnSc1
·	·	k?
Inaba	Inaba	k1gMnSc1
·	·	k?
Ise	Ise	k1gMnSc1
·	·	k?
Išikari	Išikar	k1gFnSc2
·	·	k?
Iwami	Iwa	k1gFnPc7
·	·	k?
Izu	Izu	k1gMnSc3
·	·	k?
Izumi	Izu	k1gFnPc7
·	·	k?
Izumo	Izuma	k1gFnSc5
·	·	k?
Jamaširo	Jamaširo	k1gNnSc4
·	·	k?
Jamato	Jamat	k2eAgNnSc1d1
·	·	k?
Kaga	Kagus	k1gMnSc2
·	·	k?
Kai	Kai	k1gFnSc4
·	·	k?
Kawači	Kawač	k1gInSc3
·	·	k?
Kazusa	Kazus	k1gMnSc2
·	·	k?
Kii	Kii	k1gMnSc2
·	·	k?
Kitami	Kita	k1gFnPc7
·	·	k?
Kózuke	Kózuk	k1gInSc2
·	·	k?
Kuširo	Kuširo	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
·	·	k?
Mikawa	Mikaw	k1gInSc2
·	·	k?
Mimasaka	Mimasak	k1gMnSc2
·	·	k?
Mino	mina	k1gFnSc5
·	·	k?
Mucu	Mucum	k1gNnSc6
·	·	k?
Musaši	Musaše	k1gFnSc4
·	·	k?
Nagato	Nagat	k2eAgNnSc1d1
·	·	k?
Nemuro	Nemura	k1gFnSc5
·	·	k?
Noto	nota	k1gFnSc5
·	·	k?
Oki	Oki	k1gMnSc2
·	·	k?
Ómi	Ómi	k1gMnSc2
·	·	k?
Ošima	Ošim	k1gMnSc2
·	·	k?
Ósumi	Ósu	k1gFnPc7
·	·	k?
Owari	Owar	k1gInSc3
·	·	k?
Sacuma	Sacum	k1gMnSc2
·	·	k?
Sado	sada	k1gFnSc5
·	·	k?
Sagami	Saga	k1gFnPc7
·	·	k?
Sanuki	Sanuki	k1gNnPc6
·	·	k?
Seccu	secco	k1gNnSc6
·	·	k?
Suó	Suó	k1gMnSc2
·	·	k?
Suruga	Surug	k1gMnSc2
·	·	k?
Šima	Šimus	k1gMnSc2
·	·	k?
Šimocuke	Šimocuk	k1gMnSc2
·	·	k?
Šimósa	Šimós	k1gMnSc2
·	·	k?
Šinano	Šinana	k1gFnSc5
·	·	k?
Širibeši	Širibech	k1gMnPc1
·	·	k?
Tadžima	Tadžima	k1gFnSc1
·	·	k?
Tanba	Tanba	k1gFnSc1
·	·	k?
Tango	tango	k1gNnSc1
·	·	k?
Tešio	Tešio	k6eAd1
·	·	k?
Tokači	Tokač	k1gMnSc3
·	·	k?
Tosa	Tosa	k1gMnSc1
·	·	k?
Tótómi	Tótó	k1gFnPc7
·	·	k?
Wakasa	Wakasa	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
