<p>
<s>
Sopečné	sopečný	k2eAgNnSc1d1	sopečné
jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
má	mít	k5eAaImIp3nS	mít
kulatý	kulatý	k2eAgInSc4d1	kulatý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc4d2	menší
rozlohu	rozloha	k1gFnSc4	rozloha
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
hluboké	hluboký	k2eAgNnSc1d1	hluboké
<g/>
.	.	kIx.	.
</s>
<s>
Napájené	napájený	k2eAgNnSc1d1	napájené
je	být	k5eAaImIp3nS	být
atmosférickými	atmosférický	k2eAgFnPc7d1	atmosférická
srážkami	srážka	k1gFnPc7	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
jezerem	jezero	k1gNnSc7	jezero
stále	stále	k6eAd1	stále
aktivní	aktivní	k2eAgInSc1d1	aktivní
vulkanismus	vulkanismus	k1gInSc1	vulkanismus
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
oblast	oblast	k1gFnSc4	oblast
typické	typický	k2eAgNnSc1d1	typické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
nasycena	nasytit	k5eAaPmNgFnS	nasytit
sopečnými	sopečný	k2eAgInPc7d1	sopečný
plyny	plyn	k1gInPc7	plyn
či	či	k8xC	či
v	v	k7c6	v
extrémním	extrémní	k2eAgInSc6d1	extrémní
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
vody	voda	k1gFnSc2	voda
mohou	moct	k5eAaImIp3nP	moct
objevovat	objevovat	k5eAaImF	objevovat
slabé	slabý	k2eAgFnPc1d1	slabá
nebo	nebo	k8xC	nebo
silné	silný	k2eAgFnPc1d1	silná
kyseliny	kyselina	k1gFnPc1	kyselina
a	a	k8xC	a
jezero	jezero	k1gNnSc1	jezero
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
například	například	k6eAd1	například
slabě	slabě	k6eAd1	slabě
koncentrovanou	koncentrovaný	k2eAgFnSc7d1	koncentrovaná
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
vody	voda	k1gFnSc2	voda
jezera	jezero	k1gNnSc2	jezero
dává	dávat	k5eAaImIp3nS	dávat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mohou	moct	k5eAaImIp3nP	moct
vulkanologové	vulkanolog	k1gMnPc1	vulkanolog
získat	získat	k5eAaPmF	získat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
magmatu	magma	k1gNnSc6	magma
v	v	k7c6	v
podloží	podloží	k1gNnSc6	podloží
či	či	k8xC	či
o	o	k7c6	o
případné	případný	k2eAgFnSc6d1	případná
nadcházející	nadcházející	k2eAgFnSc6d1	nadcházející
sopečné	sopečný	k2eAgFnSc6d1	sopečná
erupci	erupce	k1gFnSc6	erupce
<g/>
.	.	kIx.	.
<g/>
Některá	některý	k3yIgNnPc1	některý
sopečná	sopečný	k2eAgNnPc1d1	sopečné
jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
schopna	schopen	k2eAgNnPc1d1	schopno
zadržovat	zadržovat	k5eAaImF	zadržovat
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
dně	dno	k1gNnSc6	dno
sopečné	sopečný	k2eAgInPc1d1	sopečný
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tak	tak	k6eAd1	tak
nemohou	moct	k5eNaImIp3nP	moct
unikat	unikat	k5eAaImF	unikat
pozvolna	pozvolna	k6eAd1	pozvolna
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
náhlé	náhlý	k2eAgFnSc6d1	náhlá
změně	změna	k1gFnSc6	změna
hydrostatického	hydrostatický	k2eAgInSc2d1	hydrostatický
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vlivem	vliv	k1gInSc7	vliv
deště	dešť	k1gInSc2	dešť
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
plyn	plyn	k1gInSc1	plyn
může	moct	k5eAaImIp3nS	moct
náhle	náhle	k6eAd1	náhle
uvolnit	uvolnit	k5eAaPmF	uvolnit
a	a	k8xC	a
expandovat	expandovat	k5eAaImF	expandovat
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
ze	z	k7c2	z
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
jedovaté	jedovatý	k2eAgNnSc1d1	jedovaté
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
tak	tak	k6eAd1	tak
způsobit	způsobit	k5eAaPmF	způsobit
katastrofu	katastrofa	k1gFnSc4	katastrofa
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
stalo	stát	k5eAaPmAgNnS	stát
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
Nyos	Nyosa	k1gFnPc2	Nyosa
v	v	k7c6	v
Kamerunu	Kamerun	k1gInSc6	Kamerun
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
jiných	jiný	k2eAgNnPc2d1	jiné
jezer	jezero	k1gNnPc2	jezero
hrozí	hrozit	k5eAaImIp3nS	hrozit
akutní	akutní	k2eAgNnSc1d1	akutní
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
opětovné	opětovný	k2eAgFnSc2d1	opětovná
sopečné	sopečný	k2eAgFnSc2d1	sopečná
exploze	exploze	k1gFnSc2	exploze
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
smíšení	smíšení	k1gNnSc3	smíšení
zadržované	zadržovaný	k2eAgFnSc2d1	zadržovaná
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
úlomků	úlomek	k1gInPc2	úlomek
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
ničivých	ničivý	k2eAgInPc2d1	ničivý
laharů	lahar	k1gInPc2	lahar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
==	==	k?	==
</s>
</p>
<p>
<s>
kráterová	kráterový	k2eAgFnSc1d1	kráterová
</s>
</p>
<p>
<s>
v	v	k7c6	v
kráteru	kráter	k1gInSc6	kráter
</s>
</p>
<p>
<s>
v	v	k7c6	v
kaldeře	kaldera	k1gFnSc6	kaldera
</s>
</p>
<p>
<s>
v	v	k7c6	v
maaru	maar	k1gInSc6	maar
</s>
</p>
<p>
<s>
hrazená	hrazený	k2eAgFnSc1d1	hrazená
lávovými	lávový	k2eAgFnPc7d1	lávová
proudy	proud	k1gInPc7	proud
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Katalog	katalog	k1gInSc1	katalog
aktivních	aktivní	k2eAgFnPc2d1	aktivní
sopek	sopka	k1gFnPc2	sopka
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Catalog	Catalog	k1gInSc1	Catalog
of	of	k?	of
Active	Actiev	k1gFnSc2	Actiev
Volcanoes	Volcanoes	k1gMnSc1	Volcanoes
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sopečná	sopečný	k2eAgNnPc1d1	sopečné
jezera	jezero	k1gNnPc1	jezero
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
16	[number]	k4	16
%	%	kIx~	%
ze	z	k7c2	z
714	[number]	k4	714
holocénních	holocénní	k2eAgFnPc2d1	holocénní
sopek	sopka	k1gFnPc2	sopka
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příklady	příklad	k1gInPc1	příklad
===	===	k?	===
</s>
</p>
<p>
<s>
Albano	Albana	k1gFnSc5	Albana
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cuicocha	Cuicoch	k1gMnSc4	Cuicoch
(	(	kIx(	(
<g/>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kerið	Kerið	k?	Kerið
(	(	kIx(	(
<g/>
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kráterové	kráterový	k2eAgNnSc1d1	kráterové
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
Oregon	Oregon	k1gNnSc1	Oregon
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nyos	Nyos	k1gInSc1	Nyos
(	(	kIx(	(
<g/>
Kamerun	Kamerun	k1gInSc1	Kamerun
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
(	(	kIx(	(
<g/>
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Taupo	Taupa	k1gFnSc5	Taupa
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Toba	Toba	k1gFnSc1	Toba
(	(	kIx(	(
<g/>
Sumatra	Sumatra	k1gFnSc1	Sumatra
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tazawa	Tazawa	k1gFnSc1	Tazawa
<g/>
,	,	kIx,	,
Towada	Towada	k1gFnSc1	Towada
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
kráteru	kráter	k1gInSc6	kráter
Baekdu	Baekd	k1gInSc2	Baekd
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
ČLR	ČLR	kA	ČLR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
kráteru	kráter	k1gInSc6	kráter
Kelut	Kelut	k1gInSc1	Kelut
(	(	kIx(	(
<g/>
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
kráteru	kráter	k1gInSc6	kráter
Irazú	Irazú	k1gFnSc1	Irazú
(	(	kIx(	(
<g/>
Kostarika	Kostarika	k1gFnSc1	Kostarika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
kráteru	kráter	k1gInSc6	kráter
Pinatubo	Pinatuba	k1gFnSc5	Pinatuba
(	(	kIx(	(
<g/>
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
kráteru	kráter	k1gInSc6	kráter
Keli	Kel	k1gFnSc2	Kel
Mutu	Mutu	k?	Mutu
(	(	kIx(	(
<g/>
Indonésie	Indonésie	k1gFnSc1	Indonésie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
kráteru	kráter	k1gInSc6	kráter
Rano	Rana	k1gFnSc5	Rana
Kau	Kau	k1gFnSc3	Kau
(	(	kIx(	(
<g/>
Velikonoční	velikonoční	k2eAgInSc4d1	velikonoční
ostrov	ostrov	k1gInSc4	ostrov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
kráteru	kráter	k1gInSc6	kráter
Rano	Rana	k1gFnSc5	Rana
Raraku	Rarak	k1gInSc3	Rarak
(	(	kIx(	(
<g/>
Velikonoční	velikonoční	k2eAgInSc1d1	velikonoční
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
kaldeře	kaldera	k1gFnSc6	kaldera
Coatepeque	Coatepequ	k1gFnSc2	Coatepequ
(	(	kIx(	(
<g/>
Salvador	Salvador	k1gInSc1	Salvador
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kaldera	kaldera	k1gFnSc1	kaldera
Narugo	Narugo	k1gMnSc1	Narugo
<g/>
,	,	kIx,	,
pH	ph	kA	ph
1,6	[number]	k4	1,6
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
jezer	jezero	k1gNnPc2	jezero
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
mívá	mívat	k5eAaImIp3nS	mívat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
teplotu	teplota	k1gFnSc4	teplota
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
okolního	okolní	k2eAgInSc2d1	okolní
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
jen	jen	k9	jen
několik	několik	k4yIc4	několik
jezer	jezero	k1gNnPc2	jezero
na	na	k7c6	na
světě	svět	k1gInSc6	svět
mívá	mívat	k5eAaImIp3nS	mívat
teplotu	teplota	k1gFnSc4	teplota
alespoň	alespoň	k9	alespoň
po	po	k7c4	po
část	část	k1gFnSc4	část
roku	rok	k1gInSc2	rok
nad	nad	k7c4	nad
~	~	kIx~	~
<g/>
45	[number]	k4	45
až	až	k9	až
50	[number]	k4	50
°	°	k?	°
<g/>
C.	C.	kA	C.
Bylo	být	k5eAaImAgNnS	být
taktéž	taktéž	k?	taktéž
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
teplejší	teplý	k2eAgFnSc1d2	teplejší
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
taková	takový	k3xDgNnPc1	takový
jezera	jezero	k1gNnPc1	jezero
teplotně	teplotně	k6eAd1	teplotně
homogenní	homogenní	k2eAgNnPc1d1	homogenní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
studená	studený	k2eAgFnSc1d1	studená
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
stratifikováno	stratifikován	k2eAgNnSc1d1	stratifikováno
<g/>
.	.	kIx.	.
<g/>
Sopečná	sopečný	k2eAgNnPc1d1	sopečné
jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
taktéž	taktéž	k?	taktéž
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
různou	různý	k2eAgFnSc7d1	různá
barvou	barva	k1gFnSc7	barva
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
nachází	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
jezero	jezero	k1gNnSc1	jezero
Crater	Cratra	k1gFnPc2	Cratra
Lake	Lak	k1gInSc2	Lak
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Oregonu	Oregon	k1gInSc6	Oregon
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
modrou	modrý	k2eAgFnSc7d1	modrá
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
Kawah	Kawah	k1gMnSc1	Kawah
Ijen	Ijen	k1gMnSc1	Ijen
má	mít	k5eAaImIp3nS	mít
tyrkysově	tyrkysově	k6eAd1	tyrkysově
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
či	či	k8xC	či
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
Oyunuma	Oyunumum	k1gNnSc2	Oyunumum
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
není	být	k5eNaImIp3nS	být
barva	barva	k1gFnSc1	barva
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
čase	čas	k1gInSc6	čas
konstantní	konstantní	k2eAgFnSc1d1	konstantní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
vzrůstající	vzrůstající	k2eAgFnSc6d1	vzrůstající
či	či	k8xC	či
klesající	klesající	k2eAgFnSc6d1	klesající
sopečné	sopečný	k2eAgFnSc6d1	sopečná
aktivitě	aktivita	k1gFnSc6	aktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Jezioro	Jeziora	k1gFnSc5	Jeziora
wulkaniczne	wulkanicznout	k5eAaPmIp3nS	wulkanicznout
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SIGURDSSON	SIGURDSSON	kA	SIGURDSSON
<g/>
,	,	kIx,	,
Haraldur	Haraldur	k1gMnSc1	Haraldur
<g/>
;	;	kIx,	;
DELMELLE	DELMELLE	kA	DELMELLE
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gMnSc5	Pierr
<g/>
;	;	kIx,	;
BERNARD	Bernard	k1gMnSc1	Bernard
<g/>
,	,	kIx,	,
Alain	Alain	k1gMnSc1	Alain
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Volcanoes	Volcanoes	k1gMnSc1	Volcanoes
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Academic	Academic	k1gMnSc1	Academic
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
643140	[number]	k4	643140
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Volcanic	Volcanice	k1gFnPc2	Volcanice
Lakes	Lakes	k1gMnSc1	Lakes
<g/>
,	,	kIx,	,
s.	s.	k?	s.
877	[number]	k4	877
<g/>
-	-	kIx~	-
<g/>
895	[number]	k4	895
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Lávové	lávový	k2eAgNnSc1d1	lávové
jezero	jezero	k1gNnSc1	jezero
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
