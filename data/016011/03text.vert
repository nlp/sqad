<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
</s>
<s>
46	#num#	k4
<g/>
.	.	kIx.
prezident	prezident	k1gMnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
Úřadující	úřadující	k2eAgFnSc4d1
</s>
<s>
Ve	v	k7c4
funkci	funkce	k1gFnSc4
od	od	k7c2
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
Viceprezidentka	viceprezidentka	k1gFnSc1
</s>
<s>
Kamala	Kamala	k1gFnSc1
Harrisová	Harrisový	k2eAgFnSc1d1
Předchůdce	předchůdce	k1gMnSc4
</s>
<s>
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
</s>
<s>
47	#num#	k4
<g/>
.	.	kIx.
viceprezident	viceprezident	k1gMnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
Prezident	prezident	k1gMnSc1
</s>
<s>
Barack	Barack	k1gMnSc1
Obama	Obama	k?
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Dick	Dick	k1gMnSc1
Cheney	Chenea	k1gFnSc2
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Mike	Mike	k1gFnSc1
Pence	pence	k1gFnSc2
</s>
<s>
Senátor	senátor	k1gMnSc1
Senátu	senát	k1gInSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
americkýchza	americkýchza	k1gFnSc1
stát	stát	k1gInSc1
Delaware	Delawar	k1gInSc5
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1973	#num#	k4
–	–	k?
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
J.	J.	kA
Caleb	Calba	k1gFnPc2
Boggs	Boggsa	k1gFnPc2
Nástupce	nástupce	k1gMnSc2
</s>
<s>
Ted	Ted	k1gMnSc1
Kaufman	Kaufman	k1gMnSc1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1942	#num#	k4
(	(	kIx(
<g/>
78	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Scranton	Scranton	k1gInSc1
<g/>
,	,	kIx,
Pensylvánie	Pensylvánie	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
USA	USA	kA
Národnost	národnost	k1gFnSc1
</s>
<s>
Američan	Američan	k1gMnSc1
Choť	choť	k1gMnSc1
</s>
<s>
Neilie	Neilie	k1gFnSc1
Hunterová	Hunterová	k1gFnSc1
Bidenová	Bidenová	k1gFnSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
Jill	Jill	k1gInSc1
Bidenová	Bidenová	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1977	#num#	k4
<g/>
)	)	kIx)
Rodiče	rodič	k1gMnPc1
</s>
<s>
Joseph	Joseph	k1gMnSc1
Robinette	Robinett	k1gInSc5
Biden	Biden	k2eAgMnSc1d1
a	a	k8xC
Jean	Jean	k1gMnSc1
Bidenová	Bidenová	k1gFnSc1
Děti	dítě	k1gFnPc1
</s>
<s>
Beau	Beau	k5eAaPmIp1nS
BidenNaomi	BidenNao	k1gFnPc7
BidenováHunter	BidenováHunter	k1gMnSc1
BidenAshley	BidenAshlea	k1gFnSc2
Bidenová	Bidenová	k1gFnSc1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Valerie	Valerie	k1gFnSc1
Bidenová	Bidenová	k1gFnSc1
Owensová	Owensová	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
James	James	k1gMnSc1
Biden	Biden	k1gInSc1
a	a	k8xC
Francis	Francis	k1gFnSc1
Biden	Bidna	k1gFnPc2
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
Natalie	Natalie	k1gFnSc1
Bidenová	Bidenová	k1gFnSc1
a	a	k8xC
Robert	Robert	k1gMnSc1
Biden	Bidna	k1gFnPc2
II	II	kA
(	(	kIx(
<g/>
vnoučata	vnouče	k1gNnPc1
<g/>
)	)	kIx)
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Claymont	Claymont	k1gInSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
<g/>
Arden	Ardeny	k1gFnPc2
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
–	–	k?
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
Wilmington	Wilmington	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
Wilmington	Wilmington	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
Bílý	bílý	k2eAgInSc1d1
dům	dům	k1gInSc1
(	(	kIx(
<g/>
od	od	k7c2
2021	#num#	k4
<g/>
)	)	kIx)
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Archmerská	Archmerský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
1961	#num#	k4
<g/>
)	)	kIx)
<g/>
University	universita	k1gFnSc2
of	of	k?
Delaware	Delawar	k1gMnSc5
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
–	–	k?
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
<g/>
Syracuská	Syracuský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
1965	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
Profese	profese	k1gFnSc1
</s>
<s>
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
advokát	advokát	k1gMnSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
Náboženství	náboženství	k1gNnSc2
</s>
<s>
římskokatolické	římskokatolický	k2eAgNnSc1d1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Hilal-e-Pakistan	Hilal-e-Pakistan	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
Medaile	medaile	k1gFnSc1
Laetare	Laetar	k1gMnSc5
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
velkokříž	velkokříž	k1gInSc1
Řádu	řád	k1gInSc2
Boyacá	Boyacá	k1gFnPc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
Prezidentská	prezidentský	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
svobody	svoboda	k1gFnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
velkodůstojník	velkodůstojník	k1gInSc1
Řádu	řád	k1gInSc2
tří	tři	k4xCgFnPc2
hvězd	hvězda	k1gFnPc2
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Podpis	podpis	k1gInSc4
</s>
<s>
Webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
joebiden	joebidna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
Commons	Commons	k1gInSc1
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Joseph	Joseph	k1gMnSc1
„	„	k?
<g/>
Joe	Joe	k1gMnSc1
<g/>
“	“	k?
Robinette	Robinette	k1gMnSc1
Biden	Biden	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
ˈ	ˈ	k1gMnSc1
rɒ	rɒ	k1gMnSc1
ˈ	ˈ	k1gMnSc1
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1942	#num#	k4
Scranton	Scranton	k1gInSc1
<g/>
,	,	kIx,
Pensylvánie	Pensylvánie	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
politik	politik	k1gMnSc1
za	za	k7c4
Demokratickou	demokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
46	#num#	k4
<g/>
.	.	kIx.
prezident	prezident	k1gMnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2009	#num#	k4
až	až	k9
2017	#num#	k4
sloužil	sloužit	k5eAaImAgInS
jako	jako	k9
47	#num#	k4
<g/>
.	.	kIx.
viceprezident	viceprezident	k1gMnSc1
USA	USA	kA
ve	v	k7c6
vládě	vláda	k1gFnSc6
Baracka	Baracko	k1gNnSc2
Obamy	Obama	k1gFnSc2
a	a	k8xC
mezi	mezi	k7c7
roky	rok	k1gInPc7
1973	#num#	k4
až	až	k9
2009	#num#	k4
byl	být	k5eAaImAgInS
americkým	americký	k2eAgMnSc7d1
senátorem	senátor	k1gMnSc7
za	za	k7c4
stát	stát	k1gInSc4
Delaware	Delawar	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
Prezidentskou	prezidentský	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
svobody	svoboda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vystudovaný	vystudovaný	k2eAgMnSc1d1
právník	právník	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
29	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
šestý	šestý	k4xOgMnSc1
nejmladší	mladý	k2eAgMnSc1d3
v	v	k7c6
americké	americký	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
zvolen	zvolen	k2eAgMnSc1d1
za	za	k7c4
Demokratickou	demokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
senátorem	senátor	k1gMnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
za	za	k7c4
stát	stát	k1gInSc4
Delaware	Delawar	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měsíc	měsíc	k1gInSc1
nato	nato	k6eAd1
prožil	prožít	k5eAaPmAgInS
rodinnou	rodinný	k2eAgFnSc4d1
tragédii	tragédie	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
při	při	k7c6
autonehodě	autonehoda	k1gFnSc6
zemřela	zemřít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Neilia	Neilia	k1gFnSc1
a	a	k8xC
dcera	dcera	k1gFnSc1
Naomi	Nao	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biden	Biden	k1gInSc1
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
po	po	k7c4
téměř	téměř	k6eAd1
čtyřicet	čtyřicet	k4xCc4
let	léto	k1gNnPc2
dojížděl	dojíždět	k5eAaImAgInS
do	do	k7c2
Senátu	senát	k1gInSc2
vlakem	vlak	k1gInSc7
<g/>
,	,	kIx,
protože	protože	k8xS
nechtěl	chtít	k5eNaImAgMnS
používat	používat	k5eAaImF
automobil	automobil	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joe	Joe	k1gFnPc2
Biden	Bidna	k1gFnPc2
sloužil	sloužit	k5eAaImAgInS
v	v	k7c6
Senátu	senát	k1gInSc6
36	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
podpořil	podpořit	k5eAaPmAgInS
například	například	k6eAd1
zásadní	zásadní	k2eAgFnSc2d1
Violence	Violence	k1gFnSc2
Against	Against	k1gMnSc1
Women	Womna	k1gFnPc2
Act	Act	k1gMnSc1
(	(	kIx(
<g/>
Zákon	zákon	k1gInSc1
proti	proti	k7c3
násilí	násilí	k1gNnSc3
na	na	k7c6
ženách	žena	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vojenskou	vojenský	k2eAgFnSc4d1
intervenci	intervence	k1gFnSc4
v	v	k7c6
Jugoslávii	Jugoslávie	k1gFnSc6
či	či	k8xC
válku	válka	k1gFnSc4
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
si	se	k3xPyFc3
Bidena	Bidena	k1gFnSc1
vybral	vybrat	k5eAaPmAgMnS
kandidát	kandidát	k1gMnSc1
na	na	k7c4
prezidenta	prezident	k1gMnSc2
Barack	Baracka	k1gFnPc2
Obama	Obama	k?
jako	jako	k8xC,k8xS
svého	svůj	k3xOyFgMnSc2
viceprezidenta	viceprezident	k1gMnSc2
<g/>
;	;	kIx,
společně	společně	k6eAd1
pak	pak	k6eAd1
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
znovuzvoleni	znovuzvolit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
47	#num#	k4
<g/>
.	.	kIx.
viceprezident	viceprezident	k1gMnSc1
USA	USA	kA
Biden	Biden	k1gInSc1
podpořil	podpořit	k5eAaPmAgInS
důležité	důležitý	k2eAgInPc4d1
daňové	daňový	k2eAgInPc4d1
zákony	zákon	k1gInPc4
nebo	nebo	k8xC
stažení	stažení	k1gNnSc4
vojáků	voják	k1gMnPc2
z	z	k7c2
Iráku	Irák	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Biden	Biden	k1gInSc1
neúspěšně	úspěšně	k6eNd1
usiloval	usilovat	k5eAaImAgInS
o	o	k7c4
funkci	funkce	k1gFnSc4
prezidenta	prezident	k1gMnSc2
USA	USA	kA
v	v	k7c6
letech	léto	k1gNnPc6
1988	#num#	k4
a	a	k8xC
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potřetí	potřetí	k4xO
kandidoval	kandidovat	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
po	po	k7c6
boku	bok	k1gInSc6
senátorky	senátorka	k1gFnSc2
Kamaly	Kamala	k1gFnSc2
Harrisové	Harrisový	k2eAgFnSc2d1
jako	jako	k8xS,k8xC
kandidátky	kandidátka	k1gFnSc2
na	na	k7c4
viceprezidentku	viceprezidentka	k1gFnSc4
a	a	k8xC
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
46	#num#	k4
<g/>
.	.	kIx.
prezidentem	prezident	k1gMnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgInS
hned	hned	k6eAd1
několika	několik	k4yIc2
historických	historický	k2eAgInPc2d1
milníků	milník	k1gInPc2
<g/>
:	:	kIx,
Po	po	k7c6
Johnu	John	k1gMnSc6
Fitzgeraldovi	Fitzgerald	k1gMnSc6
Kennedym	Kennedym	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
teprve	teprve	k6eAd1
druhým	druhý	k4xOgMnSc7
katolíkem	katolík	k1gMnSc7
zvoleným	zvolený	k2eAgFnPc3d1
do	do	k7c2
funkce	funkce	k1gFnSc2
prezidenta	prezident	k1gMnSc2
USA	USA	kA
<g/>
;	;	kIx,
s	s	k7c7
81	#num#	k4
miliony	milion	k4xCgInPc7
získanými	získaný	k2eAgInPc7d1
hlasy	hlas	k1gInPc7
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
historicky	historicky	k6eAd1
největším	veliký	k2eAgNnSc7d3
množstvím	množství	k1gNnSc7
voličů	volič	k1gMnPc2
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
78	#num#	k4
letech	léto	k1gNnPc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejstarším	starý	k2eAgMnSc7d3
americkým	americký	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
své	svůj	k3xOyFgFnSc6
inauguraci	inaugurace	k1gFnSc6
dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
složil	složit	k5eAaPmAgInS
ústavou	ústava	k1gFnSc7
předepsanou	předepsaný	k2eAgFnSc4d1
přísahu	přísaha	k1gFnSc4
do	do	k7c2
rukou	ruka	k1gFnPc2
předsedy	předseda	k1gMnSc2
Nejvyššího	vysoký	k2eAgInSc2d3
soudu	soud	k1gInSc2
Johna	John	k1gMnSc2
G.	G.	kA
Robertse	Roberts	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
46	#num#	k4
<g/>
.	.	kIx.
prezidentem	prezident	k1gMnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
a	a	k8xC
vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Narození	narození	k1gNnSc1
a	a	k8xC
původ	původ	k1gInSc1
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
ve	v	k7c6
Scrantonu	Scranton	k1gInSc6
v	v	k7c6
Pensylvánii	Pensylvánie	k1gFnSc6
v	v	k7c6
katolické	katolický	k2eAgFnSc6d1
rodině	rodina	k1gFnSc6
jako	jako	k8xC,k8xS
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
sestru	sestra	k1gFnSc4
a	a	k8xC
dva	dva	k4xCgMnPc4
bratry	bratr	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Otec	otec	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Robinette	Robinett	k1gInSc5
Biden	Biden	k2eAgMnSc1d1
Sr	Sr	k1gFnPc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rodák	rodák	k1gMnSc1
z	z	k7c2
Baltimoru	Baltimore	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Příjmení	příjmení	k1gNnSc4
Biden	Bidna	k1gFnPc2
zdědil	zdědit	k5eAaPmAgInS
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
pradědy	praděd	k1gMnPc7
z	z	k7c2
otcovy	otcův	k2eAgFnSc2d1
linie	linie	k1gFnSc2
<g/>
,	,	kIx,
Williama	William	k1gMnSc2
Bidena	Biden	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
přistěhoval	přistěhovat	k5eAaPmAgMnS
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
z	z	k7c2
Anglie	Anglie	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Druhé	druhý	k4xOgNnSc1
příjmení	příjmení	k1gNnSc2
Robinette	Robinett	k1gInSc5
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
jiného	jiný	k2eAgNnSc2d1
Joeova	Joeovo	k1gNnSc2
pradědy	praděd	k1gMnPc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
francouzské	francouzský	k2eAgInPc4d1
kořeny	kořen	k1gInPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
zatímco	zatímco	k8xS
rodiče	rodič	k1gMnPc1
manželky	manželka	k1gFnSc2
tohoto	tento	k3xDgInSc2
pradědy	praděd	k1gMnPc4
se	se	k3xPyFc4
narodili	narodit	k5eAaPmAgMnP
v	v	k7c6
Irsku	Irsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Matka	matka	k1gFnSc1
Catherine	Catherin	k1gInSc5
Eugenia	Eugenium	k1gNnPc4
roz	roz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finnegan	Finnegan	k1gInSc1
(	(	kIx(
<g/>
1917	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
ve	v	k7c6
Scrantonu	Scranton	k1gInSc6
v	v	k7c6
Pensylvánii	Pensylvánie	k1gFnSc6
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc4
předkové	předek	k1gMnPc1
byli	být	k5eAaImAgMnP
irského	irský	k2eAgMnSc4d1
původu	původa	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Otec	otec	k1gMnSc1
pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
relativně	relativně	k6eAd1
zámožné	zámožný	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
narození	narození	k1gNnSc4
nejstaršího	starý	k2eAgMnSc2d3
syna	syn	k1gMnSc2
Joea	Joeus	k1gMnSc2
se	se	k3xPyFc4
však	však	k9
rodině	rodina	k1gFnSc3
finančně	finančně	k6eAd1
nedařilo	dařit	k5eNaImAgNnS
<g/>
,	,	kIx,
proto	proto	k8xC
bydleli	bydlet	k5eAaImAgMnP
u	u	k7c2
rodiny	rodina	k1gFnSc2
Finneganovy	Finneganův	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
tehdy	tehdy	k6eAd1
nemohl	moct	k5eNaImAgMnS
najít	najít	k5eAaPmF
stabilní	stabilní	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1953	#num#	k4
se	se	k3xPyFc4
rodina	rodina	k1gFnSc1
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
státu	stát	k1gInSc2
Delaware	Delawar	k1gMnSc5
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
do	do	k7c2
Claymontu	Claymont	k1gInSc2
a	a	k8xC
poté	poté	k6eAd1
do	do	k7c2
Wilmingtonu	Wilmington	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Otec	otec	k1gMnSc1
začal	začít	k5eAaPmAgMnS
úspěšně	úspěšně	k6eAd1
prodávat	prodávat	k5eAaImF
použité	použitý	k2eAgInPc1d1
automobily	automobil	k1gInPc1
a	a	k8xC
rodina	rodina	k1gFnSc1
se	se	k3xPyFc4
zařadila	zařadit	k5eAaPmAgFnS
do	do	k7c2
střední	střední	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Studoval	studovat	k5eAaImAgInS
na	na	k7c6
katolické	katolický	k2eAgFnSc6d1
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
Archmere	Archmer	k1gInSc5
Academy	Academ	k1gMnPc7
v	v	k7c6
Claymontu	Claymont	k1gInSc6
<g/>
,	,	kIx,
hrál	hrát	k5eAaImAgMnS
ve	v	k7c6
školním	školní	k2eAgInSc6d1
týmu	tým	k1gInSc6
amerického	americký	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
podprůměrný	podprůměrný	k2eAgMnSc1d1
student	student	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
přirozený	přirozený	k2eAgMnSc1d1
vůdce	vůdce	k1gMnSc1
a	a	k8xC
byl	být	k5eAaImAgMnS
předsedou	předseda	k1gMnSc7
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
i	i	k8xC
přes	přes	k7c4
své	svůj	k3xOyFgInPc4
problémy	problém	k1gInPc4
s	s	k7c7
koktavostí	koktavost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Školu	škola	k1gFnSc4
dokončil	dokončit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1961	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
</s>
<s>
Poté	poté	k6eAd1
vystudoval	vystudovat	k5eAaPmAgMnS
na	na	k7c4
University	universita	k1gFnPc4
of	of	k?
Delaware	Delawar	k1gMnSc5
v	v	k7c6
Newarku	Newark	k1gInSc6
historii	historie	k1gFnSc4
a	a	k8xC
politické	politický	k2eAgFnPc4d1
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc4
studijní	studijní	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
byly	být	k5eAaImAgInP
podprůměrné	podprůměrný	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
získání	získání	k1gNnSc6
bakalářského	bakalářský	k2eAgInSc2d1
titulu	titul	k1gInSc2
studoval	studovat	k5eAaImAgMnS
práva	právo	k1gNnSc2
na	na	k7c6
Syracuse	Syracus	k1gInSc6
University	universita	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
získal	získat	k5eAaPmAgMnS
titul	titul	k1gInSc4
doktora	doktor	k1gMnSc2
práv	právo	k1gNnPc2
(	(	kIx(
<g/>
Juris	Juris	k1gFnSc1
Doctor	Doctor	k1gMnSc1
–	–	k?
J.D.	J.D.	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rodinný	rodinný	k2eAgInSc1d1
a	a	k8xC
soukromý	soukromý	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Během	během	k7c2
studií	studie	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
oženil	oženit	k5eAaPmAgInS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
spolužačkou	spolužačka	k1gFnSc7
Neilií	Neilie	k1gFnPc2
Hunter	Huntra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželé	manžel	k1gMnPc1
měli	mít	k5eAaImAgMnP
spolu	spolu	k6eAd1
tři	tři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Syn	syn	k1gMnSc1
Beau	Beaus	k1gInSc2
Biden	Biden	k1gInSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
Hunter	Hunter	k1gMnSc1
Biden	Biden	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
a	a	k8xC
dcera	dcera	k1gFnSc1
Naomi	Nao	k1gFnPc7
Christina	Christin	k2eAgNnPc1d1
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Krátce	krátce	k6eAd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
zvolen	zvolit	k5eAaPmNgInS
senátorem	senátor	k1gMnSc7
<g/>
,	,	kIx,
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
žena	žena	k1gFnSc1
v	v	k7c6
prosinci	prosinec	k1gInSc6
těžce	těžce	k6eAd1
havarovala	havarovat	k5eAaPmAgFnS
<g/>
,	,	kIx,
při	při	k7c6
vánočních	vánoční	k2eAgInPc6d1
nákupech	nákup	k1gInPc6
nedala	dát	k5eNaPmAgFnS
přednost	přednost	k1gFnSc1
při	při	k7c6
vjezdu	vjezd	k1gInSc6
na	na	k7c4
hlavní	hlavní	k2eAgFnSc4d1
silnici	silnice	k1gFnSc4
nákladnímu	nákladní	k2eAgInSc3d1
vozu	vůz	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
jejich	jejich	k3xOp3gNnSc4
auto	auto	k1gNnSc4
srazil	srazit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nehodě	nehoda	k1gFnSc6
zahynula	zahynout	k5eAaPmAgFnS
spolu	spolu	k6eAd1
s	s	k7c7
dcerou	dcera	k1gFnSc7
<g/>
,	,	kIx,
oba	dva	k4xCgMnPc1
synové	syn	k1gMnPc1
byli	být	k5eAaImAgMnP
těžce	těžce	k6eAd1
zraněni	zranit	k5eAaPmNgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Zvažoval	zvažovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mandátu	mandát	k1gInSc2
vzdá	vzdát	k5eAaPmIp3nS
<g/>
,	,	kIx,
senátor	senátor	k1gMnSc1
Mike	Mik	k1gFnSc2
Mansfield	Mansfield	k1gInSc1
ho	on	k3xPp3gMnSc4
však	však	k9
přesvědčil	přesvědčit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
politické	politický	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
nevzdával	vzdávat	k5eNaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgNnSc2
působení	působení	k1gNnSc2
v	v	k7c6
Senátu	senát	k1gInSc6
USA	USA	kA
<g/>
,	,	kIx,
tedy	tedy	k9
36	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
dojížděl	dojíždět	k5eAaImAgMnS
do	do	k7c2
Washingtonu	Washington	k1gInSc2
více	hodně	k6eAd2
než	než	k8xS
hodinu	hodina	k1gFnSc4
vlakem	vlak	k1gInSc7
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
domu	dům	k1gInSc2
ve	v	k7c6
Wilmingtonu	Wilmington	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
schůzce	schůzka	k1gFnSc6
naslepo	naslepo	k6eAd1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
zorganizoval	zorganizovat	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
seznámil	seznámit	k5eAaPmAgMnS
s	s	k7c7
Jill	Jill	k1gInSc1
Tracy	Traca	k1gFnSc2
Jacobs	Jacobsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
oženil	oženit	k5eAaPmAgMnS
a	a	k8xC
v	v	k7c6
manželství	manželství	k1gNnSc6
s	s	k7c7
Jill	Jill	k1gMnSc1
Tracy	Traca	k1gMnSc2
Jacobsovou	Jacobsová	k1gFnSc7
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
dcera	dcera	k1gFnSc1
Ashley	Ashlea	k1gFnSc2
(	(	kIx(
<g/>
*	*	kIx~
1981	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Starší	starší	k1gMnSc1
syn	syn	k1gMnSc1
z	z	k7c2
prvního	první	k4xOgNnSc2
manželství	manželství	k1gNnSc2
Beau	Beaus	k1gInSc2
Biden	Bidna	k1gFnPc2
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
právník	právník	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
v	v	k7c6
letech	let	k1gInPc6
2007	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
z	z	k7c2
titulu	titul	k1gInSc2
generálního	generální	k2eAgMnSc2d1
prokurátora	prokurátor	k1gMnSc2
zastával	zastávat	k5eAaImAgMnS
úřad	úřad	k1gInSc4
ministra	ministr	k1gMnSc2
spravedlnosti	spravedlnost	k1gFnSc2
státu	stát	k1gInSc2
Delaware	Delawar	k1gMnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Mladší	mladý	k2eAgMnSc1d2
syn	syn	k1gMnSc1
Hunter	Hunter	k1gMnSc1
Biden	Biden	k2eAgMnSc1d1
(	(	kIx(
<g/>
*	*	kIx~
1970	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
právník	právník	k1gMnSc1
a	a	k8xC
lobbista	lobbista	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biden	Biden	k1gInSc1
nepije	pít	k5eNaImIp3nS
alkohol	alkohol	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
jeho	jeho	k3xOp3gNnPc2
vyjádření	vyjádření	k1gNnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
rodině	rodina	k1gFnSc6
alkoholiků	alkoholik	k1gMnPc2
už	už	k6eAd1
dost	dost	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Politické	politický	k2eAgNnSc1d1
působení	působení	k1gNnSc1
</s>
<s>
Počátky	počátek	k1gInPc1
politické	politický	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
</s>
<s>
Senátor	senátor	k1gMnSc1
Joe	Joe	k1gMnSc1
Biden	Biden	k2eAgMnSc1d1
a	a	k8xC
prezident	prezident	k1gMnSc1
Jimmy	Jimma	k1gFnSc2
Carter	Carter	k1gMnSc1
<g/>
,	,	kIx,
věnováni	věnován	k2eAgMnPc1d1
od	od	k7c2
CarteraPrezident	CarteraPrezident	k1gMnSc1
Ronald	Ronald	k1gMnSc1
Reagan	Reagan	k1gMnSc1
a	a	k8xC
Joe	Joe	k1gMnSc1
Biden	Bidna	k1gFnPc2
<g/>
,	,	kIx,
1984	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
začal	začít	k5eAaPmAgMnS
pracovat	pracovat	k5eAaImF
jako	jako	k9
advokátní	advokátní	k2eAgMnSc1d1
koncipient	koncipient	k1gMnSc1
ve	v	k7c6
Wilmingtonu	Wilmington	k1gInSc6
<g/>
,	,	kIx,
šéf	šéf	k1gMnSc1
jeho	jeho	k3xOp3gFnSc2
kanceláře	kancelář	k1gFnSc2
byl	být	k5eAaImAgMnS
republikán	republikán	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biden	Biden	k1gInSc1
nepodporoval	podporovat	k5eNaImAgInS
demokratického	demokratický	k2eAgMnSc4d1
guvernéra	guvernér	k1gMnSc4
kvůli	kvůli	k7c3
jeho	jeho	k3xOp3gFnSc3
konzervativní	konzervativní	k2eAgFnSc3d1
rasové	rasový	k2eAgFnSc3d1
politice	politika	k1gFnSc3
a	a	k8xC
podporoval	podporovat	k5eAaImAgMnS
jeho	on	k3xPp3gMnSc4,k3xOp3gMnSc4
republikánského	republikánský	k2eAgMnSc4d1
rivala	rival	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Richarda	Richard	k1gMnSc4
Nixona	Nixon	k1gMnSc4
jako	jako	k8xC,k8xS
prezidentského	prezidentský	k2eAgMnSc4d1
kandidáta	kandidát	k1gMnSc4
však	však	k9
nepodporoval	podporovat	k5eNaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
začal	začít	k5eAaPmAgMnS
pracovat	pracovat	k5eAaImF
jako	jako	k9
advokát	advokát	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
nadřízený	nadřízený	k1gMnSc1
byl	být	k5eAaImAgMnS
aktivní	aktivní	k2eAgMnSc1d1
demokrat	demokrat	k1gMnSc1
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
ho	on	k3xPp3gMnSc4
přesvědčil	přesvědčit	k5eAaPmAgMnS
registrovat	registrovat	k5eAaBmF
se	se	k3xPyFc4
jako	jako	k8xS,k8xC
demokrata	demokrat	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
byl	být	k5eAaImAgInS
již	již	k6eAd1
jako	jako	k8xC,k8xS
demokrat	demokrat	k1gMnSc1
zvolen	zvolit	k5eAaPmNgMnS
do	do	k7c2
okresní	okresní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
v	v	k7c6
New	New	k1gFnSc6
Castle	Castle	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
do	do	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
se	se	k3xPyFc4
svou	svůj	k3xOyFgFnSc7
advokátní	advokátní	k2eAgFnSc7d1
praxí	praxe	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senátor	senátor	k1gMnSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
uspěl	uspět	k5eAaPmAgMnS
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Senátu	senát	k1gInSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
ve	v	k7c6
státě	stát	k1gInSc6
Delaware	Delawar	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
mandát	mandát	k1gInSc4
nastoupil	nastoupit	k5eAaPmAgMnS
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1973	#num#	k4
<g/>
;	;	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
pátým	pátý	k4xOgNnSc7
nejmladším	mladý	k2eAgMnSc7d3
senátorem	senátor	k1gMnSc7
v	v	k7c6
celé	celý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
porazil	porazit	k5eAaPmAgInS
své	své	k1gNnSc4
republikánské	republikánský	k2eAgFnSc2d1
vyzyvatele	vyzyvatele	k?
ve	v	k7c6
všech	všecek	k3xTgFnPc6
dalších	další	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
letech	léto	k1gNnPc6
1978	#num#	k4
(	(	kIx(
<g/>
James	James	k1gMnSc1
H.	H.	kA
Baxter	Baxter	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1984	#num#	k4
(	(	kIx(
<g/>
John	John	k1gMnSc1
M.	M.	kA
Burris	Burris	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1990	#num#	k4
(	(	kIx(
<g/>
M.	M.	kA
Jane	Jan	k1gMnSc5
Brady	brada	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1996	#num#	k4
a	a	k8xC
2002	#num#	k4
(	(	kIx(
<g/>
Raymond	Raymond	k1gInSc1
J.	J.	kA
Clatworthy	Clatworth	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
2008	#num#	k4
(	(	kIx(
<g/>
Christine	Christin	k1gInSc5
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Donnellová	Donnellová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
roku	rok	k1gInSc3
2009	#num#	k4
byl	být	k5eAaImAgInS
šestým	šestý	k4xOgMnSc7
nejdéle	dlouho	k6eAd3
sloužícím	sloužící	k2eAgMnSc7d1
senátorem	senátor	k1gMnSc7
v	v	k7c6
Kongresu	kongres	k1gInSc6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členem	člen	k1gMnSc7
Senátu	senát	k1gInSc2
byl	být	k5eAaImAgMnS
šest	šest	k4xCc4
volebních	volební	k2eAgNnPc2d1
období	období	k1gNnPc2
<g/>
,	,	kIx,
nepřetržitě	přetržitě	k6eNd1
od	od	k7c2
ledna	leden	k1gInSc2
1973	#num#	k4
do	do	k7c2
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
na	na	k7c6
funkci	funkce	k1gFnSc6
rezignoval	rezignovat	k5eAaBmAgMnS
jako	jako	k9
zvolený	zvolený	k2eAgMnSc1d1
viceprezident	viceprezident	k1gMnSc1
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
také	také	k9
předsedou	předseda	k1gMnSc7
vlivného	vlivný	k2eAgInSc2d1
senátního	senátní	k2eAgInSc2d1
Výboru	výbor	k1gInSc2
pro	pro	k7c4
zahraniční	zahraniční	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
(	(	kIx(
<g/>
Foreign	Foreigna	k1gFnPc2
Relations	Relations	k1gInSc1
Committee	Committe	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
Výboru	výbor	k1gInSc2
pro	pro	k7c4
soudnictví	soudnictví	k1gNnSc4
(	(	kIx(
<g/>
Judiciary	Judiciar	k1gInPc1
Committee	Committe	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Delawareská	Delawareský	k2eAgFnSc1d1
guvernérka	guvernérka	k1gFnSc1
Ruth	Ruth	k1gFnSc1
Ann	Ann	k1gFnSc1
Minnerová	Minnerová	k1gFnSc1
na	na	k7c4
uprázdněné	uprázdněný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
jmenovala	jmenovat	k5eAaImAgFnS,k5eAaBmAgFnS
dlouholetého	dlouholetý	k2eAgMnSc4d1
člena	člen	k1gMnSc4
senátu	senát	k1gInSc2
tohoto	tento	k3xDgInSc2
státu	stát	k1gInSc2
Teda	Ted	k1gMnSc4
Kaufmana	Kaufman	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
doplňovacích	doplňovací	k2eAgFnPc6d1
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
listopadu	listopad	k1gInSc6
2010	#num#	k4
byl	být	k5eAaImAgMnS
pak	pak	k6eAd1
novým	nový	k2eAgMnSc7d1
senátorem	senátor	k1gMnSc7
zvolen	zvolen	k2eAgMnSc1d1
demokrat	demokrat	k1gMnSc1
Chris	Chris	k1gFnSc2
Coons	Coons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Viceprezident	viceprezident	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
neúspěšně	úspěšně	k6eNd1
usiloval	usilovat	k5eAaImAgInS
o	o	k7c4
prezidentskou	prezidentský	k2eAgFnSc4d1
kandidaturu	kandidatura	k1gFnSc4
za	za	k7c4
Demokratickou	demokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
svou	svůj	k3xOyFgFnSc4
prezidentskou	prezidentský	k2eAgFnSc4d1
kandidaturu	kandidatura	k1gFnSc4
stáhl	stáhnout	k5eAaPmAgMnS
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
po	po	k7c6
primárkách	primárky	k1gFnPc6
v	v	k7c6
státě	stát	k1gInSc6
Iowa	Iow	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
prezidentský	prezidentský	k2eAgMnSc1d1
kandidát	kandidát	k1gMnSc1
Barack	Barack	k1gMnSc1
Obama	Obama	k?
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
kandidáta	kandidát	k1gMnSc4
na	na	k7c4
místo	místo	k1gNnSc4
viceprezidenta	viceprezident	k1gMnSc2
za	za	k7c7
demokraty	demokrat	k1gMnPc7
si	se	k3xPyFc3
vybral	vybrat	k5eAaPmAgInS
Joe	Joe	k1gMnSc4
Bidena	Biden	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
mj.	mj.	kA
v	v	k7c6
Senátu	senát	k1gInSc6
zastával	zastávat	k5eAaImAgMnS
post	post	k1gInSc4
předsedy	předseda	k1gMnSc2
Výboru	výbor	k1gInSc2
pro	pro	k7c4
zahraniční	zahraniční	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biden	Biden	k1gInSc1
skládá	skládat	k5eAaImIp3nS
přísahu	přísaha	k1gFnSc4
při	při	k7c6
uvedení	uvedení	k1gNnSc6
do	do	k7c2
úřadu	úřad	k1gInSc2
viceprezidenta	viceprezident	k1gMnSc2
USA	USA	kA
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
</s>
<s>
Večer	večer	k6eAd1
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
absolvoval	absolvovat	k5eAaPmAgInS
Biden	Biden	k1gInSc1
jedinou	jediný	k2eAgFnSc4d1
televizní	televizní	k2eAgFnSc4d1
diskusi	diskuse	k1gFnSc4
s	s	k7c7
viceprezidentskou	viceprezidentský	k2eAgFnSc7d1
protikandidátkou	protikandidátka	k1gFnSc7
Sarah	Sarah	k1gFnSc7
Palinovou	Palinová	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diskuse	diskuse	k1gFnSc1
byla	být	k5eAaImAgFnS
klidná	klidný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
nepřesvědčivá	přesvědčivý	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
výsledků	výsledek	k1gInPc2
průzkumů	průzkum	k1gInPc2
veřejného	veřejný	k2eAgNnSc2d1
mínění	mínění	k1gNnSc2
zadaných	zadaný	k2eAgFnPc2d1
americkými	americký	k2eAgFnPc7d1
televizemi	televize	k1gFnPc7
CNN	CNN	kA
a	a	k8xC
CBS	CBS	kA
<g/>
,	,	kIx,
provedených	provedený	k2eAgInPc2d1
po	po	k7c6
duelu	duel	k1gInSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
prohlášen	prohlásit	k5eAaPmNgMnS
vítězem	vítěz	k1gMnSc7
debaty	debata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
ale	ale	k8xC
podle	podle	k7c2
velké	velký	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
diváků	divák	k1gMnPc2
si	se	k3xPyFc3
Palinová	Palinová	k1gFnSc1
vedla	vést	k5eAaImAgFnS
lépe	dobře	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
očekávali	očekávat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prezident	prezident	k1gMnSc1
Barack	Barack	k1gMnSc1
Obama	Obama	k?
a	a	k8xC
viceprezident	viceprezident	k1gMnSc1
Joe	Joe	k1gMnSc1
Biden	Biden	k1gInSc4
s	s	k7c7
vojáky	voják	k1gMnPc7
na	na	k7c6
základně	základna	k1gFnSc6
Fort	Fort	k?
Campbell	Campbella	k1gFnPc2
6	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2011	#num#	k4
</s>
<s>
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2008	#num#	k4
zvítězil	zvítězit	k5eAaPmAgMnS
Barack	Barack	k1gMnSc1
Obama	Obama	k?
v	v	k7c6
prezidentských	prezidentský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biden	Biden	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
viceprezidentem	viceprezident	k1gMnSc7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
složil	složit	k5eAaPmAgMnS
opět	opět	k6eAd1
senátorský	senátorský	k2eAgInSc4d1
slib	slib	k1gInSc4
do	do	k7c2
rukou	ruka	k1gFnPc2
viceprezidenta	viceprezident	k1gMnSc2
Cheneyho	Cheney	k1gMnSc2
a	a	k8xC
mandátu	mandát	k1gInSc2
se	se	k3xPyFc4
vzdal	vzdát	k5eAaPmAgMnS
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
guvernér	guvernér	k1gMnSc1
státu	stát	k1gInSc2
Delaware	Delawar	k1gMnSc5
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
nástupce	nástupce	k1gMnSc4
Teda	Ted	k1gMnSc4
Kaufmana	Kaufman	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zaujal	zaujmout	k5eAaPmAgInS
místo	místo	k1gNnSc4
v	v	k7c6
Senátu	senát	k1gInSc6
USA	USA	kA
na	na	k7c4
další	další	k2eAgInPc4d1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
než	než	k8xS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
předčasným	předčasný	k2eAgFnPc3d1
senátním	senátní	k2eAgFnPc3d1
volbám	volba	k1gFnPc3
na	na	k7c4
uvolněný	uvolněný	k2eAgInSc4d1
post	post	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Viceprezidentem	viceprezident	k1gMnSc7
USA	USA	kA
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
po	po	k7c6
složení	složení	k1gNnSc6
slavnostního	slavnostní	k2eAgInSc2d1
slibu	slib	k1gInSc2
v	v	k7c4
poledne	poledne	k1gNnSc4
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
krátce	krátce	k6eAd1
před	před	k7c7
slibem	slib	k1gInSc7
prezidenta	prezident	k1gMnSc2
Obamy	Obama	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biden	Bidna	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
politikem	politik	k1gMnSc7
státu	stát	k1gInSc2
Delaware	Delawar	k1gMnSc5
ve	v	k7c6
funkci	funkce	k1gFnSc6
viceprezidenta	viceprezident	k1gMnSc2
USA	USA	kA
a	a	k8xC
také	také	k9
prvním	první	k4xOgMnSc7
římským	římský	k2eAgMnSc7d1
katolíkem	katolík	k1gMnSc7
v	v	k7c6
tomto	tento	k3xDgInSc6
úřadu	úřad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejoblíbenějším	oblíbený	k2eAgMnPc3d3
americkým	americký	k2eAgMnPc3d1
politikům	politik	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biden	Biden	k1gInSc1
<g/>
,	,	kIx,
ruský	ruský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Dmitrij	Dmitrij	k1gMnSc1
Medveděv	Medveděv	k1gMnSc1
a	a	k8xC
italský	italský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Silvio	Silvio	k6eAd1
Berlusconi	Berluscoň	k1gFnSc6
v	v	k7c6
Římě	Řím	k1gInSc6
v	v	k7c6
červnu	červen	k1gInSc6
2011	#num#	k4
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
dlouholetý	dlouholetý	k2eAgMnSc1d1
kritik	kritik	k1gMnSc1
programu	program	k1gInSc2
Národní	národní	k2eAgFnSc2d1
raketové	raketový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
odvádí	odvádět	k5eAaImIp3nS
pozornost	pozornost	k1gFnSc1
a	a	k8xC
prostředky	prostředek	k1gInPc1
od	od	k7c2
mnohem	mnohem	k6eAd1
reálnějších	reální	k2eAgFnPc2d2
hrozeb	hrozba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nestavěl	stavět	k5eNaImAgMnS
se	se	k3xPyFc4
proti	proti	k7c3
výzkumu	výzkum	k1gInSc3
protiraketových	protiraketový	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
červnu	červen	k1gInSc6
2002	#num#	k4
odsoudil	odsoudit	k5eAaPmAgInS
prezidenta	prezident	k1gMnSc4
George	Georg	k1gMnSc4
W.	W.	kA
Bushe	Bush	k1gMnSc4
za	za	k7c4
jednostranné	jednostranný	k2eAgNnSc4d1
vypovězení	vypovězení	k1gNnSc4
smlouvy	smlouva	k1gFnSc2
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
o	o	k7c6
omezení	omezení	k1gNnSc6
systémů	systém	k1gInPc2
protiraketové	protiraketový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
(	(	kIx(
<g/>
smlouva	smlouva	k1gFnSc1
ABM	ABM	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Bidena	Bideno	k1gNnSc2
šlo	jít	k5eAaImAgNnS
o	o	k7c4
krok	krok	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
podkopal	podkopat	k5eAaPmAgInS
snahy	snaha	k1gFnPc4
USA	USA	kA
o	o	k7c4
omezení	omezení	k1gNnSc4
šíření	šíření	k1gNnSc2
jaderných	jaderný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obamova	Obamův	k2eAgFnSc1d1
administrativa	administrativa	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
Biden	Biden	k2eAgInSc1d1
sloužil	sloužit	k5eAaImAgInS
jako	jako	k9
viceprezident	viceprezident	k1gMnSc1
<g/>
,	,	kIx,
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
budování	budování	k1gNnSc6
protiraketové	protiraketový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
v	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
a	a	k8xC
Polsku	Polsko	k1gNnSc6
navzdory	navzdory	k7c3
ostrým	ostrý	k2eAgInPc3d1
protestům	protest	k1gInPc3
ze	z	k7c2
strany	strana	k1gFnSc2
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
americkou	americký	k2eAgFnSc4d1
protiraketovou	protiraketový	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
označilo	označit	k5eAaPmAgNnS
za	za	k7c4
porušení	porušení	k1gNnSc4
Smlouvy	smlouva	k1gFnSc2
o	o	k7c4
likvidaci	likvidace	k1gFnSc4
raket	raketa	k1gFnPc2
středního	střední	k2eAgNnSc2d1
a	a	k8xC
krátkého	krátký	k2eAgNnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
doletu	dolet	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1987	#num#	k4
<g/>
,	,	kIx,
protože	protože	k8xS
systém	systém	k1gInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
využit	využit	k2eAgInSc1d1
i	i	k9
k	k	k7c3
odpálení	odpálení	k1gNnSc3
jaderných	jaderný	k2eAgFnPc2d1
střel	střela	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
vedl	vést	k5eAaImAgInS
úspěšné	úspěšný	k2eAgNnSc4d1
administrativní	administrativní	k2eAgNnSc4d1
úsilí	úsilí	k1gNnSc4
k	k	k7c3
senátnímu	senátní	k2eAgNnSc3d1
odsouhlasení	odsouhlasení	k1gNnSc3
nové	nový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
START	start	k1gInSc4
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
o	o	k7c6
snížení	snížení	k1gNnSc6
počtu	počet	k1gInSc2
strategických	strategický	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
2020	#num#	k4
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
<g/>
,	,	kIx,
Barack	Barack	k1gInSc1
Obama	Obama	k?
a	a	k8xC
prezident	prezident	k1gMnSc1
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
<g/>
,	,	kIx,
2017	#num#	k4
</s>
<s>
Během	během	k7c2
kampaně	kampaň	k1gFnSc2
před	před	k7c7
volbou	volba	k1gFnSc7
prezidenta	prezident	k1gMnSc2
USA	USA	kA
2020	#num#	k4
se	se	k3xPyFc4
dopustil	dopustit	k5eAaPmAgInS
mnoha	mnoho	k4c2
slovních	slovní	k2eAgInPc2d1
přešlapů	přešlap	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současný	současný	k2eAgInSc1d1
prezident	prezident	k1gMnSc1
USA	USA	kA
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
o	o	k7c6
něm	on	k3xPp3gInSc6
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Opravdu	opravdu	k6eAd1
někdo	někdo	k3yInSc1
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
mentálně	mentálně	k6eAd1
fit	fit	k2eAgFnSc1d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgMnS
prezidentem	prezident	k1gMnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Sám	sám	k3xTgInSc4
Biden	Biden	k1gInSc4
připustil	připustit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
„	„	k?
<g/>
stroj	stroj	k1gInSc1
na	na	k7c4
přežblepty	přežblepta	k1gFnPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Biden	Biden	k1gInSc1
byl	být	k5eAaImAgInS
během	během	k7c2
úvodních	úvodní	k2eAgInPc2d1
měsíců	měsíc	k1gInPc2
kampaně	kampaň	k1gFnSc2
považován	považován	k2eAgMnSc1d1
za	za	k7c4
nadějného	nadějný	k2eAgMnSc4d1
uchazeče	uchazeč	k1gMnSc4
o	o	k7c4
kandidáta	kandidát	k1gMnSc4
vybraného	vybraný	k2eAgMnSc4d1
z	z	k7c2
předvoleb	předvolba	k1gFnPc2
Demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
podpora	podpora	k1gFnSc1
však	však	k9
začala	začít	k5eAaPmAgFnS
postupně	postupně	k6eAd1
opadat	opadat	k5eAaBmF
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
v	v	k7c6
listopadu	listopad	k1gInSc6
2019	#num#	k4
kandidovat	kandidovat	k5eAaImF
newyorský	newyorský	k2eAgMnSc1d1
miliardář	miliardář	k1gMnSc1
Michael	Michael	k1gMnSc1
Bloomberg	Bloomberg	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Biden	Biden	k1gInSc1
spouští	spouštět	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
prezidentskou	prezidentský	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
<g/>
,	,	kIx,
2019	#num#	k4
</s>
<s>
Během	během	k7c2
hlasování	hlasování	k1gNnSc2
v	v	k7c6
demokratických	demokratický	k2eAgFnPc6d1
primárkách	primárky	k1gFnPc6
v	v	k7c6
několika	několik	k4yIc6
prvních	první	k4xOgInPc6
státech	stát	k1gInPc6
dosáhl	dosáhnout	k5eAaPmAgInS
Biden	Biden	k1gInSc1
špatných	špatný	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
Iowě	Iowa	k1gFnSc6
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
New	New	k1gFnSc6
Hampshiru	Hampshir	k1gInSc2
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
s	s	k7c7
velkým	velký	k2eAgInSc7d1
odstupem	odstup	k1gInSc7
za	za	k7c7
Berniem	Bernium	k1gNnSc7
Sandersem	Sanders	k1gInSc7
v	v	k7c6
Nevadě	Nevada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
kampaň	kampaň	k1gFnSc4
revitalizovalo	revitalizovat	k5eAaImAgNnS
přesvědčivé	přesvědčivý	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Karolíně	Karolína	k1gFnSc6
v	v	k7c4
sobotu	sobota	k1gFnSc4
29	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
neděli	neděle	k1gFnSc6
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
ukončil	ukončit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
kandidaturu	kandidatura	k1gFnSc4
a	a	k8xC
podpořil	podpořit	k5eAaPmAgMnS
Bidena	Biden	k2eAgMnSc4d1
Pete	Pet	k1gMnSc4
Buttigieg	Buttigieg	k1gInSc4
<g/>
,	,	kIx,
o	o	k7c4
den	den	k1gInSc4
později	pozdě	k6eAd2
udělala	udělat	k5eAaPmAgFnS
totéž	týž	k3xTgNnSc1
Amy	Amy	k1gFnSc1
Klobucharová	Klobucharový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
předvečer	předvečer	k1gInSc4
volebního	volební	k2eAgInSc2d1
superúterý	superúterý	k2eAgInSc1d1
Bidena	Biden	k1gMnSc2
podpořil	podpořit	k5eAaPmAgInS
také	také	k9
Beto	Beto	k1gNnSc4
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Rourke	Rourke	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
prezidentskou	prezidentský	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
ukončil	ukončit	k5eAaPmAgMnS
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
Bidenově	Bidenův	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
v	v	k7c6
10	#num#	k4
ze	z	k7c2
14	#num#	k4
států	stát	k1gInPc2
v	v	k7c4
úterý	úterý	k1gNnSc4
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
a	a	k8xC
celkovém	celkový	k2eAgNnSc6d1
vedení	vedení	k1gNnSc6
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
počtu	počet	k1gInSc2
volitelů	volitel	k1gMnPc2
(	(	kIx(
<g/>
před	před	k7c7
druhým	druhý	k4xOgMnSc7
Sandersem	Sanders	k1gMnSc7
<g/>
)	)	kIx)
ukončil	ukončit	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc4
kampaň	kampaň	k1gFnSc4
a	a	k8xC
vyjádřil	vyjádřit	k5eAaPmAgMnS
podporu	podpora	k1gFnSc4
Bidenovi	Biden	k1gMnSc3
také	také	k9
newyorský	newyorský	k2eAgMnSc1d1
mediální	mediální	k2eAgMnSc1d1
magnát	magnát	k1gMnSc1
Michael	Michael	k1gMnSc1
Bloomberg	Bloomberg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
9	#num#	k4
<g/>
.	.	kIx.
březnu	březen	k1gInSc6
2020	#num#	k4
podpořili	podpořit	k5eAaPmAgMnP
Bidena	Biden	k1gMnSc4
další	další	k2eAgMnPc1d1
bývalí	bývalý	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
Cory	Cora	k1gFnSc2
Booker	Booker	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Delaney	Delanea	k1gFnSc2
<g/>
,	,	kIx,
Kamala	Kamala	k1gFnSc1
Harris	Harris	k1gFnSc1
<g/>
,	,	kIx,
Deval	Deval	k1gMnSc1
Patrick	Patrick	k1gMnSc1
a	a	k8xC
Tim	Tim	k?
Ryan	Ryan	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
volebního	volební	k2eAgInSc2d1
modelu	model	k1gInSc2
zpravodajského	zpravodajský	k2eAgInSc2d1
webu	web	k1gInSc2
FiveThirtyEight	FiveThirtyEight	k1gMnSc1
byl	být	k5eAaImAgMnS
Joe	Joe	k1gMnSc3
Biden	Biden	k1gInSc4
po	po	k7c4
tzv.	tzv.	kA
superúterý	superúterý	k2eAgInSc4d1
již	již	k9
jasným	jasný	k2eAgInSc7d1
favoritem	favorit	k1gInSc7
primárních	primární	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
Demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
odstoupení	odstoupení	k1gNnSc6
senátora	senátor	k1gMnSc2
Bernieho	Bernie	k1gMnSc2
Sanderse	Sanderse	k1gFnSc2
z	z	k7c2
primárek	primárky	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
jediným	jediný	k2eAgMnSc7d1
vážným	vážný	k2eAgMnSc7d1
uchazečem	uchazeč	k1gMnSc7
o	o	k7c4
kandidaturu	kandidatura	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
Demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
kampaň	kampaň	k1gFnSc4
však	však	k9
byla	být	k5eAaImAgFnS
brzděna	brzdit	k5eAaImNgFnS
koronavirovou	koronavirův	k2eAgFnSc7d1
krizí	krize	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zavázal	Zavázal	k1gMnSc1
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
jako	jako	k9
kandidáta	kandidát	k1gMnSc4
na	na	k7c4
viceprezidenta	viceprezident	k1gMnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
vybere	vybrat	k5eAaPmIp3nS
ženu	žena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2020	#num#	k4
oznámil	oznámit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
takovou	takový	k3xDgFnSc7
spolukandidátkou	spolukandidátka	k1gFnSc7
stala	stát	k5eAaPmAgFnS
americká	americký	k2eAgFnSc1d1
senátorka	senátorka	k1gFnSc1
z	z	k7c2
Kalifornie	Kalifornie	k1gFnSc2
Kamala	Kamala	k1gFnSc1
Harrisová	Harrisový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2020	#num#	k4
byl	být	k5eAaImAgInS
Joe	Joe	k1gFnSc2
Biden	Biden	k1gInSc1
oficiálně	oficiálně	k6eAd1
nominován	nominován	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
prezidentský	prezidentský	k2eAgMnSc1d1
kandidát	kandidát	k1gMnSc1
za	za	k7c4
Demokratickou	demokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
Bidenova	Bidenův	k2eAgFnSc1d1
manželka	manželka	k1gFnSc1
Jill	Jill	k1gInSc1
<g/>
,	,	kIx,
nastupující	nastupující	k2eAgFnSc1d1
první	první	k4xOgFnSc1
dáma	dáma	k1gFnSc1
USA	USA	kA
</s>
<s>
V	v	k7c6
počátečních	počáteční	k2eAgFnPc6d1
fázích	fáze	k1gFnPc6
kampaně	kampaň	k1gFnSc2
byl	být	k5eAaImAgInS
Biden	Biden	k1gInSc1
několikrát	několikrát	k6eAd1
obviněn	obviněn	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nevhodně	vhodně	k6eNd1
dotýká	dotýkat	k5eAaImIp3nS
cizích	cizí	k2eAgFnPc2d1
žen	žena	k1gFnPc2
a	a	k8xC
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Sám	sám	k3xTgInSc1
sebe	sebe	k3xPyFc4
označil	označit	k5eAaPmAgInS
za	za	k7c7
„	„	k?
<g/>
dotykového	dotykový	k2eAgNnSc2d1
politika	politikum	k1gNnSc2
<g/>
“	“	k?
a	a	k8xC
připustil	připustit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
přináší	přinášet	k5eAaImIp3nS
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgFnSc2
postižené	postižený	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
si	se	k3xPyFc3
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
chování	chování	k1gNnSc4
stěžovaly	stěžovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedkyně	předsedkyně	k1gFnSc1
Sněmovny	sněmovna	k1gFnSc2
reprezentantů	reprezentant	k1gMnPc2
Nancy	Nancy	k1gFnSc1
Pelosiová	Pelosiový	k2eAgFnSc1d1
v	v	k7c6
dubnu	duben	k1gInSc6
2019	#num#	k4
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Biden	Biden	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
pochopit	pochopit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
osobní	osobní	k2eAgInSc1d1
prostor	prostor	k1gInSc1
je	být	k5eAaImIp3nS
důležitý	důležitý	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
březnu	březen	k1gInSc6
2019	#num#	k4
bývalá	bývalý	k2eAgFnSc1d1
členka	členka	k1gFnSc1
nevadského	nevadský	k2eAgNnSc2d1
státního	státní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
<g/>
,	,	kIx,
Lucy	Luca	k1gFnSc2
Flores	Floresa	k1gFnPc2
<g/>
,	,	kIx,
obvinila	obvinit	k5eAaPmAgFnS
Bidena	Bidena	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
během	během	k7c2
jednoho	jeden	k4xCgNnSc2
kampaňového	kampaňový	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
v	v	k7c6
Las	laso	k1gNnPc2
Vegas	Vegasa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
k	k	k7c3
ní	on	k3xPp3gFnSc3
Biden	Biden	k1gInSc1
přišel	přijít	k5eAaPmAgInS
zezadu	zezadu	k6eAd1
<g/>
,	,	kIx,
položil	položit	k5eAaPmAgMnS
ruce	ruka	k1gFnPc4
na	na	k7c4
její	její	k3xOp3gNnPc4
ramena	rameno	k1gNnPc4
<g/>
,	,	kIx,
přičichl	přičichnout	k5eAaPmAgMnS
si	se	k3xPyFc3
k	k	k7c3
jejím	její	k3xOp3gInPc3
vlasům	vlas	k1gInPc3
a	a	k8xC
políbil	políbit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
bez	bez	k7c2
jejího	její	k3xOp3gInSc2
souhlasu	souhlas	k1gInSc2
na	na	k7c4
temeno	temeno	k1gNnSc4
hlavy	hlava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Amy	Amy	k1gMnSc1
Lapos	Lapos	k1gMnSc1
<g/>
,	,	kIx,
bývalá	bývalý	k2eAgFnSc1d1
poradkyně	poradkyně	k1gFnSc1
kongresmana	kongresman	k1gMnSc2
Jima	Jima	k1gFnSc1
Himese	Himese	k1gFnSc1
<g/>
,	,	kIx,
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc2
Biden	Biden	k1gInSc1
dotkl	dotknout	k5eAaPmAgMnS
nesexuálním	sexuální	k2eNgMnSc7d1
<g/>
,	,	kIx,
leč	leč	k8xC,k8xS
nepatřičným	patřičný	k2eNgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
když	když	k8xS
jí	on	k3xPp3gFnSc3
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
přidržel	přidržet	k5eAaPmAgMnS
hlavu	hlava	k1gFnSc4
<g/>
,	,	kIx,
přiblížil	přiblížit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
obličej	obličej	k1gInSc4
k	k	k7c3
jejímu	její	k3xOp3gMnSc3
a	a	k8xC
otřel	otřít	k5eAaPmAgMnS
se	s	k7c7
svým	svůj	k3xOyFgInSc7
nosem	nos	k1gInSc7
o	o	k7c4
její	její	k3xOp3gInSc4
nos	nos	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Krátce	krátce	k6eAd1
nato	nato	k6eAd1
přišly	přijít	k5eAaPmAgFnP
další	další	k2eAgInPc1d1
dvě	dva	k4xCgFnPc1
ženy	žena	k1gFnPc1
s	s	k7c7
obviněními	obvinění	k1gNnPc7
nevhodného	vhodný	k2eNgNnSc2d1
a	a	k8xC
sexuálně	sexuálně	k6eAd1
sugestivního	sugestivní	k2eAgNnSc2d1
chování	chování	k1gNnSc2
Joea	Joe	k2eAgFnSc1d1
Bidena	Bidena	k1gFnSc1
vůči	vůči	k7c3
nim	on	k3xPp3gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
D.	D.	kA
J.	J.	kA
Hillová	Hillová	k1gFnSc1
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
jí	on	k3xPp3gFnSc3
Biden	Biden	k1gInSc1
přejížděl	přejíždět	k5eAaImAgInS
rukou	ruka	k1gFnSc7
z	z	k7c2
ramen	rameno	k1gNnPc2
dolů	dol	k1gInPc2
po	po	k7c6
zádech	záda	k1gNnPc6
a	a	k8xC
Caitlin	Caitlina	k1gFnPc2
Caruso	Carusa	k1gFnSc5
vypověděla	vypovědět	k5eAaPmAgNnP
<g/>
,	,	kIx,
že	že	k8xS
jí	on	k3xPp3gFnSc3
Biden	Biden	k1gInSc1
položil	položit	k5eAaPmAgInS
ruku	ruka	k1gFnSc4
na	na	k7c4
stehno	stehno	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
začátku	začátek	k1gInSc6
dubna	duben	k1gInSc2
2019	#num#	k4
se	se	k3xPyFc4
ozvaly	ozvat	k5eAaPmAgFnP
další	další	k2eAgInPc1d1
tři	tři	k4xCgFnPc1
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
vypověděly	vypovědět	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
v	v	k7c6
minulosti	minulost	k1gFnSc6
Joe	Joe	k1gFnSc2
Biden	Biden	k1gInSc1
dotýkal	dotýkat	k5eAaImAgInS
nevhodným	vhodný	k2eNgInSc7d1
způsobem	způsob	k1gInSc7
a	a	k8xC
prokazoval	prokazovat	k5eAaImAgMnS
jim	on	k3xPp3gNnPc3
„	„	k?
<g/>
nechtěné	chtěný	k2eNgNnSc1d1
zalíbení	zalíbení	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
prohlásila	prohlásit	k5eAaPmAgFnS
Tara	Tar	k2eAgFnSc1d1
Readeová	Readeová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
praktikantkou	praktikantka	k1gFnSc7
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
týmu	tým	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jí	jíst	k5eAaImIp3nS
Biden	Biden	k1gInSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
několikrát	několikrát	k6eAd1
nevhodně	vhodně	k6eNd1
dotýkal	dotýkat	k5eAaImAgMnS
na	na	k7c6
ramenou	rameno	k1gNnPc6
a	a	k8xC
krku	krk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
navíc	navíc	k6eAd1
připojila	připojit	k5eAaPmAgFnS
tvrzení	tvrzení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
ni	on	k3xPp3gFnSc4
Biden	Biden	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
v	v	k7c6
prázdné	prázdný	k2eAgFnSc6d1
místnosti	místnost	k1gFnSc6
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
sexuálně	sexuálně	k6eAd1
zaútočil	zaútočit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Biden	Biden	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
vyjadřoval	vyjadřovat	k5eAaImAgMnS
podporu	podpora	k1gFnSc4
hnutí	hnutí	k1gNnSc2
#	#	kIx~
<g/>
MeToo	MeToo	k6eAd1
a	a	k8xC
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
obviněním	obvinění	k1gNnSc7
soudce	soudce	k1gMnSc2
Nejvyššího	vysoký	k2eAgInSc2d3
soudu	soud	k1gInSc2
Bretta	Bretta	k1gMnSc1
Kavanaugha	Kavanaugha	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
napadeným	napadený	k2eAgFnPc3d1
ženám	žena	k1gFnPc3
mělo	mít	k5eAaImAgNnS
věřit	věřit	k5eAaImF
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
toto	tento	k3xDgNnSc4
obvinění	obvinění	k1gNnSc4
vždy	vždy	k6eAd1
popíral	popírat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Readeová	Readeová	k1gFnSc1
je	být	k5eAaImIp3nS
Bidenovými	Bidenův	k2eAgMnPc7d1
stoupenci	stoupenec	k1gMnPc7
považována	považován	k2eAgFnSc1d1
za	za	k7c4
nespolehlivého	spolehlivý	k2eNgMnSc4d1
svědka	svědek	k1gMnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
minulosti	minulost	k1gFnSc6
nepravdivě	pravdivě	k6eNd1
vypovídala	vypovídat	k5eAaPmAgFnS,k5eAaImAgFnS
o	o	k7c6
své	svůj	k3xOyFgFnSc6
minulosti	minulost	k1gFnSc6
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
podezřelá	podezřelý	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
lhala	lhát	k5eAaImAgFnS
pod	pod	k7c7
přísahou	přísaha	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Několik	několik	k4yIc4
svědků	svědek	k1gMnPc2
ale	ale	k8xC
potvrdilo	potvrdit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
nimi	on	k3xPp3gMnPc7
Readeová	Readeová	k1gFnSc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
o	o	k7c6
sexuálním	sexuální	k2eAgInSc6d1
útoku	útok	k1gInSc6
mluvila	mluvit	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prezident	prezident	k1gMnSc1
USA	USA	kA
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vstřícná	vstřícný	k2eAgFnSc1d1
imigrační	imigrační	k2eAgFnSc1d1
politika	politika	k1gFnSc1
Bidenovy	Bidenův	k2eAgFnSc2d1
administrativy	administrativa	k1gFnSc2
způsobila	způsobit	k5eAaPmAgFnS
podle	podle	k7c2
některých	některý	k3yIgMnPc2
kritiků	kritik	k1gMnPc2
nárůst	nárůst	k1gInSc4
nelegální	legální	k2eNgFnSc2d1
migrace	migrace	k1gFnSc2
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
krizi	krize	k1gFnSc4
na	na	k7c6
americko-mexické	americko-mexický	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Prezident	prezident	k1gMnSc1
Biden	Biden	k1gInSc4
pověřil	pověřit	k5eAaPmAgMnS
v	v	k7c6
březnu	březen	k1gInSc6
2021	#num#	k4
řešením	řešení	k1gNnSc7
migrační	migrační	k2eAgFnSc2d1
krize	krize	k1gFnSc2
viceprezidentku	viceprezidentka	k1gFnSc4
Kamalu	Kamala	k1gFnSc4
Harrisovou	Harrisový	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biden	Biden	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
jadernou	jaderný	k2eAgFnSc4d1
energii	energie	k1gFnSc4
jako	jako	k8xC,k8xS
jeden	jeden	k4xCgInSc4
z	z	k7c2
pilířů	pilíř	k1gInPc2
bezemisní	bezemisní	k2eAgFnSc2d1
energetiky	energetika	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
rozvoj	rozvoj	k1gInSc4
pomůže	pomoct	k5eAaPmIp3nS
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
klimatickým	klimatický	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
odpůrcem	odpůrce	k1gMnSc7
stavby	stavba	k1gFnSc2
plynovodu	plynovod	k1gInSc2
Nord	Nord	k1gInSc1
Stream	Stream	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
přivádět	přivádět	k5eAaImF
zemní	zemní	k2eAgInSc1d1
plyn	plyn	k1gInSc1
z	z	k7c2
Ruska	Rusko	k1gNnSc2
do	do	k7c2
Německa	Německo	k1gNnSc2
po	po	k7c6
dně	dno	k1gNnSc6
Baltského	baltský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
zvažuje	zvažovat	k5eAaImIp3nS
uvalení	uvalení	k1gNnSc1
sankcí	sankce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
by	by	kYmCp3nP
zabránily	zabránit	k5eAaPmAgFnP
jeho	jeho	k3xOp3gNnSc4
dokončení	dokončení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2021	#num#	k4
</s>
<s>
Leden	leden	k1gInSc1
</s>
<s>
Dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
slavnostní	slavnostní	k2eAgFnSc1d1
inaugurace	inaugurace	k1gFnSc1
Joea	Joeus	k1gMnSc2
Bidena	Biden	k1gMnSc2
jako	jako	k9
46	#num#	k4
<g/>
.	.	kIx.
prezidenta	prezident	k1gMnSc4
USA	USA	kA
před	před	k7c7
budovou	budova	k1gFnSc7
Kapitolu	Kapitol	k1gInSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc6
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Kvůli	kvůli	k7c3
pandemii	pandemie	k1gFnSc3
covidu-	covidu-	k?
<g/>
19	#num#	k4
se	se	k3xPyFc4
akce	akce	k1gFnPc4
nemohlo	moct	k5eNaImAgNnS
zúčastnit	zúčastnit	k5eAaPmF
mnoho	mnoho	k4c4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
přítomní	přítomný	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
museli	muset	k5eAaImAgMnP
mít	mít	k5eAaImF
zakryté	zakrytý	k2eAgFnPc4d1
dýchací	dýchací	k2eAgFnPc4d1
cesty	cesta	k1gFnPc4
a	a	k8xC
dodržovat	dodržovat	k5eAaImF
rozestupy	rozestup	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inaugurace	inaugurace	k1gFnSc1
se	se	k3xPyFc4
nezúčastnil	zúčastnit	k5eNaPmAgMnS
dosavadní	dosavadní	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
poprvé	poprvé	k6eAd1
za	za	k7c4
152	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
úřadující	úřadující	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
nezúčastnil	zúčastnit	k5eNaPmAgMnS
inaugurace	inaugurace	k1gFnPc4
svého	svůj	k3xOyFgMnSc2
nástupce	nástupce	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stejného	stejný	k2eAgInSc2d1
dne	den	k1gInSc2
podepsal	podepsat	k5eAaPmAgInS
15	#num#	k4
nových	nový	k2eAgFnPc2d1
exekutivnívh	exekutivnívha	k1gFnPc2
případů	případ	k1gInPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
například	například	k6eAd1
návrat	návrat	k1gInSc1
k	k	k7c3
pařížské	pařížský	k2eAgFnSc3d1
dohodě	dohoda	k1gFnSc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
zastavil	zastavit	k5eAaPmAgInS
stavbu	stavba	k1gFnSc4
zdi	zeď	k1gFnSc3
na	na	k7c6
hranici	hranice	k1gFnSc6
s	s	k7c7
Mexikem	Mexiko	k1gNnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
zrušil	zrušit	k5eAaPmAgInS
zákaz	zákaz	k1gInSc1
cestování	cestování	k1gNnSc2
do	do	k7c2
USA	USA	kA
z	z	k7c2
několika	několik	k4yIc2
muslimských	muslimský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
zastavil	zastavit	k5eAaPmAgMnS
odchod	odchod	k1gInSc4
USA	USA	kA
ze	z	k7c2
Světové	světový	k2eAgFnSc2d1
zdravotnické	zdravotnický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
zrušil	zrušit	k5eAaPmAgMnS
nařízení	nařízení	k1gNnSc4
prezidenta	prezident	k1gMnSc2
Donalda	Donald	k1gMnSc2
Trumpa	Trump	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
zakazovalo	zakazovat	k5eAaImAgNnS
transgenderovým	transgenderův	k2eAgFnPc3d1
osobám	osoba	k1gFnPc3
se	s	k7c7
změněným	změněný	k2eAgNnSc7d1
pohlavím	pohlaví	k1gNnSc7
sloužit	sloužit	k5eAaImF
v	v	k7c6
americké	americký	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biden	Biden	k1gInSc1
na	na	k7c4
online	onlinout	k5eAaPmIp3nS
summitu	summit	k1gInSc2
G7	G7	k1gMnSc2
</s>
<s>
Únor	únor	k1gInSc1
</s>
<s>
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
podepsal	podepsat	k5eAaPmAgMnS
příkaz	příkaz	k1gInSc4
o	o	k7c4
obnovení	obnovení	k1gNnSc4
programu	program	k1gInSc2
přijímání	přijímání	k1gNnPc2
uprchlíků	uprchlík	k1gMnPc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
zvýší	zvýšit	k5eAaPmIp3nS
jejich	jejich	k3xOp3gFnSc4
roční	roční	k2eAgFnSc4d1
kvótu	kvóta	k1gFnSc4
na	na	k7c4
125	#num#	k4
tisíc	tisíc	k4xCgInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
také	také	k9
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
nebudou	být	k5eNaImBp3nP
podporovat	podporovat	k5eAaImF
vojenské	vojenský	k2eAgFnPc4d1
operace	operace	k1gFnPc4
Saúdské	saúdský	k2eAgFnSc2d1
Arábie	Arábie	k1gFnSc2
v	v	k7c6
Jemenu	Jemen	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
poprvé	poprvé	k6eAd1
telefonoval	telefonovat	k5eAaImAgMnS
s	s	k7c7
čínským	čínský	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Si	se	k3xPyFc3
Ťin-pchingem	Ťin-pching	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biden	Biden	k1gInSc1
v	v	k7c6
telefonátu	telefonát	k1gInSc6
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
dělá	dělat	k5eAaImIp3nS
starosti	starost	k1gFnPc4
kvůli	kvůli	k7c3
porušování	porušování	k1gNnSc3
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
v	v	k7c6
provincii	provincie	k1gFnSc6
Sin-ťiang	Sin-ťianga	k1gFnPc2
<g/>
,	,	kIx,
opatření	opatření	k1gNnPc1
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
či	či	k8xC
chování	chování	k1gNnSc6
Číny	Čína	k1gFnSc2
vůči	vůči	k7c3
Tchaj-wanu	Tchaj-wan	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
oznámil	oznámit	k5eAaPmAgMnS
uvalení	uvalení	k1gNnSc4
sankcí	sankce	k1gFnPc2
na	na	k7c4
organizátory	organizátor	k1gMnPc4
vojenského	vojenský	k2eAgInSc2d1
převratu	převrat	k1gInSc2
v	v	k7c6
Myanmaru	Myanmar	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
ve	v	k7c6
funkci	funkce	k1gFnSc6
prezidenta	prezident	k1gMnSc2
zúčastnil	zúčastnit	k5eAaPmAgMnS
summitu	summit	k1gInSc6
G	G	kA
<g/>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Březen	březen	k1gInSc1
</s>
<s>
Dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
Biden	Biden	k1gInSc1
udělil	udělit	k5eAaPmAgInS
dočasné	dočasný	k2eAgNnSc4d1
povolení	povolení	k1gNnSc4
k	k	k7c3
pobytu	pobyt	k1gInSc3
občanům	občan	k1gMnPc3
Venezuely	Venezuela	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
uprchli	uprchnout	k5eAaPmAgMnP
před	před	k7c7
krizí	krize	k1gFnSc7
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
Kongres	kongres	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
schválil	schválit	k5eAaPmAgInS
1,9	1,9	k4
bilionu	bilion	k4xCgInSc2
dolarů	dolar	k1gInPc2
na	na	k7c4
podporu	podpora	k1gFnSc4
ekonomiky	ekonomika	k1gFnSc2
zasažené	zasažený	k2eAgFnPc4d1
pandemií	pandemie	k1gFnSc7
koronaviru	koronavir	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
velvyslankyně	velvyslankyně	k1gFnSc2
při	při	k7c6
OSN	OSN	kA
Linda	Linda	k1gFnSc1
Thomasová-Greenfieldová	Thomasová-Greenfieldová	k1gFnSc1
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Biden	Biden	k1gInSc1
obnovuje	obnovovat	k5eAaImIp3nS
programy	program	k1gInPc4
na	na	k7c4
podporu	podpora	k1gFnSc4
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Politické	politický	k2eAgInPc1d1
názory	názor	k1gInPc1
</s>
<s>
Biden	Biden	k2eAgMnSc1d1
a	a	k8xC
egyptský	egyptský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Sádát	Sádát	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
s	s	k7c7
manželkou	manželka	k1gFnSc7
Jill	Jilla	k1gFnPc2
při	při	k7c6
setkání	setkání	k1gNnSc6
s	s	k7c7
papežem	papež	k1gMnSc7
Janem	Jan	k1gMnSc7
Pavlem	Pavel	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1980	#num#	k4
</s>
<s>
Biden	Biden	k2eAgMnSc1d1
a	a	k8xC
kosovský	kosovský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Hashim	Hashim	k1gMnSc1
Thaçi	Thaçe	k1gFnSc4
s	s	k7c7
Deklarací	deklarace	k1gFnSc7
nezávislosti	nezávislost	k1gFnSc2
Kosova	Kosův	k2eAgMnSc2d1
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
federální	federální	k2eAgNnSc4d1
financování	financování	k1gNnSc4
výzkumu	výzkum	k1gInSc2
embryonálních	embryonální	k2eAgFnPc2d1
kmenových	kmenový	k2eAgFnPc2d1
buněk	buňka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
hlasoval	hlasovat	k5eAaImAgMnS
proti	proti	k7c3
zákazu	zákaz	k1gInSc3
klonování	klonování	k1gNnSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Podporuje	podporovat	k5eAaImIp3nS
a	a	k8xC
rozšířil	rozšířit	k5eAaPmAgInS
by	by	kYmCp3nS
tzv.	tzv.	kA
Obamacare	Obamacar	k1gMnSc5
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
zvyšoval	zvyšovat	k5eAaImAgInS
dostupnost	dostupnost	k1gFnSc4
zdravotního	zdravotní	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
pro	pro	k7c4
chudší	chudý	k2eAgFnPc4d2
vrstvy	vrstva	k1gFnPc4
obyvatel	obyvatel	k1gMnSc1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vyplácení	vyplácení	k1gNnSc3
státních	státní	k2eAgFnPc2d1
dotací	dotace	k1gFnPc2
na	na	k7c4
zakoupení	zakoupení	k1gNnSc4
pojištění	pojištění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokuta	pokuta	k1gFnSc1
za	za	k7c4
porušení	porušení	k1gNnSc4
zákona	zákon	k1gInSc2
pro	pro	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
administrace	administrace	k1gFnPc4
byla	být	k5eAaImAgFnS
k	k	k7c3
roku	rok	k1gInSc3
2019	#num#	k4
zrušena	zrušit	k5eAaPmNgFnS
Donaldem	Donald	k1gMnSc7
Trumpem	Trump	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Biden	Biden	k1gInSc4
byl	být	k5eAaImAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
jako	jako	k9
první	první	k4xOgFnSc7
promluvil	promluvit	k5eAaPmAgMnS
o	o	k7c4
manželství	manželství	k1gNnSc4
homosexuálů	homosexuál	k1gMnPc2
během	během	k7c2
kampaně	kampaň	k1gFnSc2
za	za	k7c4
lidská	lidský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporuje	podporovat	k5eAaImIp3nS
také	také	k9
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
by	by	kYmCp3nS
zakazoval	zakazovat	k5eAaImAgInS
propouštět	propouštět	k5eAaImF
lidi	člověk	k1gMnPc4
kvůli	kvůli	k7c3
jejich	jejich	k3xOp3gFnSc3
sexuální	sexuální	k2eAgFnSc3d1
orientaci	orientace	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Nesouhlasí	souhlasit	k5eNaImIp3nS
s	s	k7c7
trestem	trest	k1gInSc7
smrti	smrt	k1gFnSc2
za	za	k7c4
homosexuální	homosexuální	k2eAgInSc4d1
pohlavní	pohlavní	k2eAgInSc4d1
styk	styk	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
od	od	k7c2
dubna	duben	k1gInSc2
2019	#num#	k4
po	po	k7c6
zavedení	zavedení	k1gNnSc6
islámského	islámský	k2eAgNnSc2d1
práva	právo	k1gNnSc2
šaría	šaría	k6eAd1
uplatňuje	uplatňovat	k5eAaImIp3nS
v	v	k7c6
Sultanátu	sultanát	k1gInSc6
Brunej	Brunej	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
kamenovat	kamenovat	k5eAaImF
lidi	člověk	k1gMnPc4
za	za	k7c4
homosexualitu	homosexualita	k1gFnSc4
nebo	nebo	k8xC
nevěru	nevěra	k1gFnSc4
je	být	k5eAaImIp3nS
otřesné	otřesný	k2eAgNnSc1d1
a	a	k8xC
nemorální	morální	k2eNgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
prezidentské	prezidentský	k2eAgFnSc2d1
televizní	televizní	k2eAgFnSc2d1
debaty	debata	k1gFnSc2
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
senátorka	senátorka	k1gFnSc1
Kamala	Kamala	k1gFnSc1
Harrisová	Harrisový	k2eAgFnSc1d1
vyčetla	vyčíst	k5eAaPmAgFnS
Bidenovi	Biden	k1gMnSc3
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
minulosti	minulost	k1gFnSc6
spolupracoval	spolupracovat	k5eAaImAgInS
s	s	k7c7
předními	přední	k2eAgMnPc7d1
segregacionistickými	segregacionistický	k2eAgMnPc7d1
politiky	politik	k1gMnPc7
a	a	k8xC
byl	být	k5eAaImAgInS
proti	proti	k7c3
rozvážení	rozvážení	k1gNnSc3
černošských	černošský	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
autobusy	autobus	k1gInPc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
desegregation	desegregation	k1gInSc1
busing	busing	k1gInSc1
<g/>
)	)	kIx)
do	do	k7c2
veřejných	veřejný	k2eAgFnPc2d1
škol	škola	k1gFnPc2
s	s	k7c7
převahou	převaha	k1gFnSc7
bílých	bílý	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
politiky	politika	k1gFnSc2
desegregace	desegregace	k1gFnSc2
škol	škola	k1gFnPc2
nařízené	nařízený	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
Nejvyšším	vysoký	k2eAgInSc7d3
soudem	soud	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
stanovil	stanovit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
federální	federální	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
může	moct	k5eAaImIp3nS
zavést	zavést	k5eAaPmF
povinnou	povinný	k2eAgFnSc4d1
školní	školní	k2eAgFnSc4d1
autobusovou	autobusový	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
zajištěna	zajištěn	k2eAgFnSc1d1
rasová	rasový	k2eAgFnSc1d1
rovnováha	rovnováha	k1gFnSc1
na	na	k7c6
amerických	americký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Biden	Bidno	k1gNnPc2
se	se	k3xPyFc4
hájil	hájit	k5eAaImAgMnS
tím	ten	k3xDgNnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
že	že	k8xS
podporoval	podporovat	k5eAaImAgMnS
desegregaci	desegregace	k1gFnSc4
škol	škola	k1gFnPc2
prováděnou	prováděný	k2eAgFnSc4d1
na	na	k7c6
místní	místní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
proti	proti	k7c3
prosazování	prosazování	k1gNnSc3
takové	takový	k3xDgFnSc2
politiky	politika	k1gFnSc2
federální	federální	k2eAgFnSc2d1
vládou	vláda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Podporuje	podporovat	k5eAaImIp3nS
pozitivní	pozitivní	k2eAgFnSc4d1
diskriminaci	diskriminace	k1gFnSc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
affirmative	affirmativ	k1gInSc5
action	action	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
ze	z	k7c2
které	který	k3yQgFnSc2,k3yIgFnSc2,k3yRgFnSc2
benefitují	benefitovat	k5eAaImIp3nP,k5eAaBmIp3nP,k5eAaPmIp3nP
historicky	historicky	k6eAd1
znevýhodněné	znevýhodněný	k2eAgFnPc4d1
etnické	etnický	k2eAgFnPc4d1
a	a	k8xC
rasové	rasový	k2eAgFnPc4d1
menšiny	menšina	k1gFnPc4
(	(	kIx(
<g/>
zvláště	zvláště	k6eAd1
Afroameričané	Afroameričan	k1gMnPc1
a	a	k8xC
Hispánci	Hispánek	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS
imigraci	imigrace	k1gFnSc4
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
chce	chtít	k5eAaImIp3nS
nabídnout	nabídnout	k5eAaPmF
americké	americký	k2eAgNnSc4d1
občanství	občanství	k1gNnSc4
jedenácti	jedenáct	k4xCc3
milionům	milion	k4xCgInPc3
ilegálních	ilegální	k2eAgMnPc2d1
imigrantů	imigrant	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Zvýšil	zvýšit	k5eAaPmAgInS
by	by	k9
limity	limit	k1gInPc4
pro	pro	k7c4
přijímání	přijímání	k1gNnSc4
přistěhovalců	přistěhovalec	k1gMnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
aby	aby	kYmCp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
ročně	ročně	k6eAd1
přijímaly	přijímat	k5eAaImAgInP
125	#num#	k4
000	#num#	k4
uprchlíků	uprchlík	k1gMnPc2
a	a	k8xC
žadatelů	žadatel	k1gMnPc2
o	o	k7c4
azyl	azyl	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
Slíbil	slíbit	k5eAaPmAgInS
zastavení	zastavení	k1gNnSc4
výstavby	výstavba	k1gFnSc2
zdi	zeď	k1gFnSc2
na	na	k7c6
hranici	hranice	k1gFnSc6
s	s	k7c7
Mexikem	Mexiko	k1gNnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
zabránit	zabránit	k5eAaPmF
nelegální	legální	k2eNgFnSc4d1
migraci	migrace	k1gFnSc4
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc4
slib	slib	k1gInSc4
ihned	ihned	k6eAd1
po	po	k7c6
své	svůj	k3xOyFgFnSc6
prezidentské	prezidentský	k2eAgFnSc6d1
inauguraci	inaugurace	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
splnil	splnit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
2020	#num#	k4
Biden	Biden	k1gInSc1
uvedl	uvést	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Jestli	jestli	k8xS
máte	mít	k5eAaImIp2nP
problém	problém	k1gInSc4
rozhodnout	rozhodnout	k5eAaPmF
se	se	k3xPyFc4
<g/>
,	,	kIx,
zda	zda	k8xS
volit	volit	k5eAaImF
mne	já	k3xPp1nSc4
nebo	nebo	k8xC
Trumpa	Trump	k1gMnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
nejste	být	k5eNaImIp2nP
černoch	černoch	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrok	výrok	k1gInSc1
zkritizovala	zkritizovat	k5eAaPmAgFnS
poradkyně	poradkyně	k1gFnPc4
prezidenta	prezident	k1gMnSc2
Donalda	Donald	k1gMnSc2
Trumpa	Trump	k1gMnSc2
Katrina	Katrina	k1gFnSc1
Piersonová	Piersonová	k1gFnSc1
<g/>
,	,	kIx,
uvedla	uvést	k5eAaPmAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
On	on	k3xPp3gMnSc1
si	se	k3xPyFc3
opravdu	opravdu	k6eAd1
myslí	myslet	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
on	on	k3xPp3gMnSc1
<g/>
,	,	kIx,
77	#num#	k4
<g/>
letý	letý	k2eAgMnSc1d1
běloch	běloch	k1gMnSc1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
diktovat	diktovat	k5eAaImF
černochům	černoch	k1gMnPc3
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
mají	mít	k5eAaImIp3nP
chovat	chovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
“	“	k?
Výrok	výrok	k1gInSc1
byl	být	k5eAaImAgInS
také	také	k9
kritizován	kritizovat	k5eAaImNgInS
z	z	k7c2
řad	řada	k1gFnPc2
afroameričanů	afroameričan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černošský	černošský	k2eAgInSc1d1
senátor	senátor	k1gMnSc1
za	za	k7c4
Republikánskou	republikánský	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Tim	Tim	k?
Scott	Scott	k1gMnSc1
z	z	k7c2
Jižní	jižní	k2eAgFnSc2d1
Karolíny	Karolína	k1gFnSc2
uvedl	uvést	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Něco	něco	k3yInSc4
tak	tak	k6eAd1
arogantního	arogantní	k2eAgNnSc2d1
a	a	k8xC
blahosklonného	blahosklonný	k2eAgNnSc2d1
jsem	být	k5eAaImIp1nS
už	už	k6eAd1
dlouho	dlouho	k6eAd1
neslyšel	slyšet	k5eNaImAgMnS
<g/>
“	“	k?
a	a	k8xC
dále	daleko	k6eAd2
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Říká	říkat	k5eAaImIp3nS
1,3	1,3	k4
milionu	milion	k4xCgInSc2
amerických	americký	k2eAgInPc2d1
černochů	černoch	k1gMnPc2
<g/>
,	,	kIx,
že	že	k8xS
nejsou	být	k5eNaImIp3nP
černoši	černoch	k1gMnPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
si	se	k3xPyFc3
sakra	sakra	k0
myslí	myslet	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
smrt	smrt	k1gFnSc4
George	Georg	k1gMnSc2
Floyda	Floyd	k1gMnSc2
<g/>
,	,	kIx,
s	s	k7c7
jehož	jehož	k3xOyRp3gFnSc7
rodinou	rodina	k1gFnSc7
se	se	k3xPyFc4
osobně	osobně	k6eAd1
setkal	setkat	k5eAaPmAgMnS
<g/>
,	,	kIx,
Biden	Biden	k1gInSc4
v	v	k7c6
projevu	projev	k1gInSc6
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
pohřbu	pohřeb	k1gInSc6
volal	volat	k5eAaImAgMnS
po	po	k7c6
spravedlnosti	spravedlnost	k1gFnSc6
pro	pro	k7c4
George	Georg	k1gMnSc4
Floyda	Floyd	k1gMnSc4
i	i	k9
po	po	k7c6
„	„	k?
<g/>
rasové	rasový	k2eAgFnSc3d1
spravedlnosti	spravedlnost	k1gFnSc3
<g/>
“	“	k?
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Staví	stavit	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
pozitivně	pozitivně	k6eAd1
k	k	k7c3
hnutí	hnutí	k1gNnSc3
Black	Blacka	k1gFnPc2
Lives	Lives	k1gMnSc1
Matter	Matter	k1gMnSc1
a	a	k8xC
odsuzuje	odsuzovat	k5eAaImIp3nS
údajný	údajný	k2eAgInSc4d1
systémový	systémový	k2eAgInSc4d1
rasismus	rasismus	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
panuje	panovat	k5eAaImIp3nS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
budou	být	k5eAaImBp3nP
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
nadřazenosti	nadřazenost	k1gFnSc3
bílé	bílý	k2eAgFnSc2d1
rasy	rasa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
chce	chtít	k5eAaImIp3nS
ukončit	ukončit	k5eAaPmF
plány	plán	k1gInPc4
na	na	k7c4
vrty	vrt	k1gInPc4
pro	pro	k7c4
ropu	ropa	k1gFnSc4
a	a	k8xC
zemní	zemní	k2eAgInSc4d1
plyn	plyn	k1gInSc4
v	v	k7c6
severních	severní	k2eAgFnPc6d1
arktických	arktický	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
Aljašky	Aljaška	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
v	v	k7c4
tu	ten	k3xDgFnSc4
dobu	doba	k1gFnSc4
dokončovala	dokončovat	k5eAaImAgFnS
Trumpova	Trumpův	k2eAgFnSc1d1
administrativa	administrativa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
toto	tento	k3xDgNnSc4
předsevzetí	předsevzetí	k1gNnSc4
již	již	k6eAd1
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
po	po	k7c6
své	svůj	k3xOyFgFnSc6
inauguraci	inaugurace	k1gFnSc6
uskutečnil	uskutečnit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biden	Biden	k1gInSc1
by	by	kYmCp3nP
chtěl	chtít	k5eAaImAgInS
výrazně	výrazně	k6eAd1
omezit	omezit	k5eAaPmF
emise	emise	k1gFnPc4
oxidu	oxid	k1gInSc2
uhličitého	uhličitý	k2eAgInSc2d1
do	do	k7c2
roku	rok	k1gInSc2
2050	#num#	k4
<g/>
,	,	kIx,
USA	USA	kA
by	by	kYmCp3nS
měly	mít	k5eAaImAgFnP
postupně	postupně	k6eAd1
omezit	omezit	k5eAaPmF
emise	emise	k1gFnPc4
na	na	k7c4
nulu	nula	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biden	Biden	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
zvýšení	zvýšení	k1gNnSc4
daní	daň	k1gFnPc2
pro	pro	k7c4
firmy	firma	k1gFnPc4
a	a	k8xC
zpřísnění	zpřísnění	k1gNnSc4
regulace	regulace	k1gFnSc2
podnikání	podnikání	k1gNnSc2
oproti	oproti	k7c3
stavu	stav	k1gInSc3
na	na	k7c6
konci	konec	k1gInSc6
volebního	volební	k2eAgNnSc2d1
období	období	k1gNnSc2
prezidenta	prezident	k1gMnSc2
Trumpa	Trump	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ohledně	ohledně	k7c2
pandemie	pandemie	k1gFnSc2
onemocnění	onemocnění	k1gNnSc2
covid-	covid-	k?
<g/>
19	#num#	k4
je	být	k5eAaImIp3nS
podporovatelem	podporovatel	k1gMnSc7
nošení	nošení	k1gNnSc2
roušek	rouška	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c4
trasování	trasování	k1gNnSc4
kontaktů	kontakt	k1gInPc2
by	by	kYmCp3nS
najal	najmout	k5eAaPmAgMnS
navíc	navíc	k6eAd1
až	až	k9
100	#num#	k4
000	#num#	k4
pracovníků	pracovník	k1gMnPc2
a	a	k8xC
kritizoval	kritizovat	k5eAaImAgMnS
Trumpa	Trump	k1gMnSc4
ze	z	k7c2
snižování	snižování	k1gNnSc2
závažnosti	závažnost	k1gFnSc2
pandemie	pandemie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Biden	Biden	k2eAgMnSc1d1
a	a	k8xC
izraelský	izraelský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Benjamin	Benjamin	k1gMnSc1
Netanjahu	Netanjaha	k1gFnSc4
<g/>
,	,	kIx,
2016	#num#	k4
</s>
<s>
Biden	Biden	k1gInSc1
připíjí	připíjet	k5eAaImIp3nS
na	na	k7c4
počest	počest	k1gFnSc4
čínského	čínský	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Si	se	k3xPyFc3
Ťin-pchinga	Ťin-pchinga	k1gFnSc1
během	během	k7c2
jeho	jeho	k3xOp3gFnSc2
návštěvy	návštěva	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
podpořil	podpořit	k5eAaPmAgMnS
nálety	nálet	k1gInPc4
NATO	NATO	kA
na	na	k7c4
Srbsko	Srbsko	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
předseda	předseda	k1gMnSc1
zahraničního	zahraniční	k2eAgInSc2d1
výboru	výbor	k1gInSc2
amerického	americký	k2eAgInSc2d1
Senátu	senát	k1gInSc2
prohlásil	prohlásit	k5eAaPmAgInS
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2002	#num#	k4
<g/>
,	,	kIx,
že	že	k8xS
Irák	Irák	k1gInSc1
vlastní	vlastní	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
hromadného	hromadný	k2eAgNnSc2d1
ničení	ničení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
2002	#num#	k4
Biden	Bidno	k1gNnPc2
hlasoval	hlasovat	k5eAaImAgMnS
pro	pro	k7c4
válku	válka	k1gFnSc4
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
podpořil	podpořit	k5eAaPmAgMnS
vojenskou	vojenský	k2eAgFnSc4d1
intervenci	intervence	k1gFnSc4
NATO	NATO	kA
v	v	k7c6
Libyi	Libye	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
<g/>
,	,	kIx,
obhajoval	obhajovat	k5eAaImAgMnS
vyzbrojování	vyzbrojování	k1gNnSc3
protivládních	protivládní	k2eAgMnPc2d1
povstalců	povstalec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
2014	#num#	k4
obvinil	obvinit	k5eAaPmAgInS
Turecko	Turecko	k1gNnSc4
<g/>
,	,	kIx,
Saúdskou	saúdský	k2eAgFnSc4d1
Arábii	Arábie	k1gFnSc4
a	a	k8xC
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
syrské	syrský	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
podporovaly	podporovat	k5eAaImAgFnP
teroristické	teroristický	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
An-Nusrá	An-Nusrý	k2eAgFnSc1d1
a	a	k8xC
Al-Káida	Al-Káida	k1gFnSc1
i	i	k8xC
další	další	k2eAgInPc1d1
džihádistické	džihádistický	k2eAgInPc1d1
radikály	radikál	k1gInPc1
proudící	proudící	k2eAgInPc1d1
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turecký	turecký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Recep	Recep	k1gMnSc1
Tayyip	Tayyip	k1gMnSc1
Erdoğ	Erdoğ	k1gMnSc1
požadoval	požadovat	k5eAaImAgMnS
od	od	k7c2
Bidena	Bideno	k1gNnSc2
omluvu	omluva	k1gFnSc4
a	a	k8xC
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
se	se	k3xPyFc4
neomluví	omluvit	k5eNaPmIp3nS
<g/>
,	,	kIx,
stane	stanout	k5eAaPmIp3nS
se	se	k3xPyFc4
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
Biden	Biden	k2eAgInSc1d1
minulostí	minulost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
Biden	Biden	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
označil	označit	k5eAaPmAgMnS
Erdoğ	Erdoğ	k1gMnSc4
za	za	k7c2
svého	svůj	k3xOyFgInSc2
„	„	k?
<g/>
starého	starý	k2eAgMnSc2d1
přítele	přítel	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
se	se	k3xPyFc4
<g />
.	.	kIx.
</s>
<s hack="1">
následně	následně	k6eAd1
za	za	k7c4
svá	svůj	k3xOyFgNnPc4
slova	slovo	k1gNnPc4
o	o	k7c6
turecké	turecký	k2eAgFnSc6d1
podpoře	podpora	k1gFnSc6
islamistů	islamista	k1gMnPc2
Erdoğ	Erdoğ	k2eAgMnPc1d1
omluvil	omluvit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
označil	označit	k5eAaPmAgInS
Kurdskou	kurdský	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
PKK	PKK	kA
<g/>
,	,	kIx,
bojující	bojující	k2eAgMnSc1d1
proti	proti	k7c3
Turecku	Turecko	k1gNnSc3
za	za	k7c4
vytvoření	vytvoření	k1gNnSc4
nezávislého	závislý	k2eNgInSc2d1
kurdského	kurdský	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c4
„	„	k?
<g/>
teroristickou	teroristický	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
99	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xS,k8xC
kandidát	kandidát	k1gMnSc1
na	na	k7c4
prezidenta	prezident	k1gMnSc4
v	v	k7c6
září	září	k1gNnSc6
2020	#num#	k4
vyzval	vyzvat	k5eAaPmAgMnS
Turecko	Turecko	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
nezapojovalo	zapojovat	k5eNaImAgNnS
do	do	k7c2
války	válka	k1gFnSc2
o	o	k7c4
Náhorní	náhorní	k2eAgInSc4d1
Karabach	Karabach	k1gInSc4
mezi	mezi	k7c7
Arménií	Arménie	k1gFnSc7
podporovanými	podporovaný	k2eAgMnPc7d1
separatisty	separatista	k1gMnPc7
a	a	k8xC
Ázerbájdžánem	Ázerbájdžán	k1gInSc7
o	o	k7c4
území	území	k1gNnSc4
Armény	Armén	k1gMnPc4
osídlené	osídlený	k2eAgFnSc2d1
Náhorno-karabašské	Náhorno-karabašský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
v	v	k7c6
době	doba	k1gFnSc6
rozpadu	rozpad	k1gInSc2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
s	s	k7c7
podporou	podpora	k1gFnSc7
Arménie	Arménie	k1gFnSc2
odtrhla	odtrhnout	k5eAaPmAgFnS
od	od	k7c2
Ázerbájdžánu	Ázerbájdžán	k1gInSc2
a	a	k8xC
vyhlásila	vyhlásit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bidenova	Bidenův	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
přivítali	přivítat	k5eAaPmAgMnP
zástupci	zástupce	k1gMnPc1
početné	početný	k2eAgFnSc2d1
arménské	arménský	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bidenův	Bidenův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
Hunter	Hunter	k1gMnSc1
Biden	Biden	k1gInSc4
byl	být	k5eAaImAgMnS
krátce	krátce	k6eAd1
po	po	k7c6
Euromajdanu	Euromajdan	k1gInSc6
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
přijat	přijmout	k5eAaPmNgInS
jako	jako	k8xS,k8xC
člen	člen	k1gInSc1
představenstva	představenstvo	k1gNnSc2
ukrajinské	ukrajinský	k2eAgFnSc2d1
plynařské	plynařský	k2eAgFnSc2d1
firmy	firma	k1gFnSc2
Burisma	Burism	k1gMnSc2
Holdings	Holdingsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
Firma	firma	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
vyšetřována	vyšetřovat	k5eAaImNgFnS
pro	pro	k7c4
podezření	podezření	k1gNnSc4
z	z	k7c2
korupce	korupce	k1gFnSc2
a	a	k8xC
tehdejší	tehdejší	k2eAgMnSc1d1
ukrajinský	ukrajinský	k2eAgMnSc1d1
generální	generální	k2eAgMnSc1d1
prokurátor	prokurátor	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
korupci	korupce	k1gFnSc6
vyšetřoval	vyšetřovat	k5eAaImAgMnS
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
po	po	k7c6
údajném	údajný	k2eAgInSc6d1
nátlaku	nátlak	k1gInSc6
viceprezidenta	viceprezident	k1gMnSc2
Bidena	Biden	k1gMnSc2
a	a	k8xC
ministra	ministr	k1gMnSc2
zahraničí	zahraničí	k1gNnSc2
Johna	John	k1gMnSc2
Kerryho	Kerry	k1gMnSc2
na	na	k7c4
vládu	vláda	k1gFnSc4
prezidenta	prezident	k1gMnSc2
Petra	Petra	k1gFnSc1
Porošenka	Porošenka	k1gFnSc1
z	z	k7c2
důvodu	důvod	k1gInSc2
očisty	očista	k1gFnSc2
prokuratury	prokuratura	k1gFnSc2
odvolán	odvolat	k5eAaPmNgMnS
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
Vyšetřování	vyšetřování	k1gNnSc1
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
kvůli	kvůli	k7c3
nedostatku	nedostatek	k1gInSc3
důkazů	důkaz	k1gInPc2
britským	britský	k2eAgInSc7d1
soudem	soud	k1gInSc7
zastaveno	zastavit	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americké	americký	k2eAgNnSc1d1
velvyslanectví	velvyslanectví	k1gNnSc1
v	v	k7c6
Izraeli	Izrael	k1gInSc6
by	by	kYmCp3nS
ponechal	ponechat	k5eAaPmAgMnS
v	v	k7c6
Jeruzalémě	Jeruzalém	k1gInSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
bylo	být	k5eAaImAgNnS
přesunuto	přesunout	k5eAaPmNgNnS
za	za	k7c2
Trumpovy	Trumpův	k2eAgFnSc2d1
administrativy	administrativa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Bidena	Biden	k1gMnSc2
by	by	kYmCp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
mohly	moct	k5eAaImAgInP
obnovit	obnovit	k5eAaPmF
jadernou	jaderný	k2eAgFnSc4d1
dohodu	dohoda	k1gFnSc4
s	s	k7c7
Íránem	Írán	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Biden	Biden	k1gInSc1
ostře	ostro	k6eAd1
odsoudil	odsoudit	k5eAaPmAgInS
hnutí	hnutí	k1gNnPc4
BDS	BDS	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
zvýšit	zvýšit	k5eAaPmF
ekonomický	ekonomický	k2eAgInSc4d1
a	a	k8xC
politický	politický	k2eAgInSc4d1
tlak	tlak	k1gInSc4
na	na	k7c4
Izrael	Izrael	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bidenův	Bidenův	k2eAgMnSc1d1
zahraničně-politický	zahraničně-politický	k2eAgMnSc1d1
poradce	poradce	k1gMnSc1
Tony	Tony	k1gMnSc1
Blinken	Blinken	k1gInSc4
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Biden	Biden	k1gInSc1
nezastaví	zastavit	k5eNaPmIp3nS
americkou	americký	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
Izraeli	Izrael	k1gMnSc3
<g/>
,	,	kIx,
ani	ani	k8xC
kdyby	kdyby	kYmCp3nS
Izrael	Izrael	k1gInSc1
anektoval	anektovat	k5eAaBmAgInS
část	část	k1gFnSc4
okupovaného	okupovaný	k2eAgInSc2d1
Západního	západní	k2eAgInSc2d1
břehu	břeh	k1gInSc2
Jordánu	Jordán	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
jádrem	jádro	k1gNnSc7
budoucího	budoucí	k2eAgInSc2d1
státu	stát	k1gInSc2
Palestina	Palestina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
Bidena	Bideno	k1gNnSc2
se	se	k3xPyFc4
očekává	očekávat	k5eAaImIp3nS
<g/>
[	[	kIx(
<g/>
kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
ním	on	k3xPp3gMnSc7
půjde	jít	k5eAaImIp3nS
snadněji	snadno	k6eAd2
vyjednat	vyjednat	k5eAaPmF
dohody	dohoda	k1gFnPc4
s	s	k7c7
Čínou	Čína	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
jako	jako	k8xS,k8xC
senátor	senátor	k1gMnSc1
podporoval	podporovat	k5eAaImAgMnS
vstup	vstup	k1gInSc4
Číny	Čína	k1gFnSc2
do	do	k7c2
Světové	světový	k2eAgFnSc2d1
obchodní	obchodní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
(	(	kIx(
<g/>
WTO	WTO	kA
<g/>
)	)	kIx)
a	a	k8xC
její	její	k3xOp3gNnSc1
otevření	otevření	k1gNnSc1
se	se	k3xPyFc4
mezinárodnímu	mezinárodní	k2eAgInSc3d1
trhu	trh	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
Zrušil	zrušit	k5eAaPmAgMnS
by	by	kYmCp3nP
jednostranná	jednostranný	k2eAgNnPc1d1
cla	clo	k1gNnPc1
na	na	k7c6
Čínu	Čína	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prosinci	prosinec	k1gInSc6
2013	#num#	k4
odletěl	odletět	k5eAaPmAgMnS
na	na	k7c4
oficiální	oficiální	k2eAgFnSc4d1
návštěvu	návštěva	k1gFnSc4
do	do	k7c2
Číny	Čína	k1gFnSc2
na	na	k7c6
palubě	paluba	k1gFnSc6
vládního	vládní	k2eAgNnSc2d1
letadla	letadlo	k1gNnSc2
Air	Air	k1gFnSc1
Force	force	k1gFnSc1
Two	Two	k1gFnSc1
společně	společně	k6eAd1
s	s	k7c7
Joe	Joe	k1gMnSc7
Bidenem	Biden	k1gMnSc7
jeho	jeho	k3xOp3gNnSc4
syn	syn	k1gMnSc1
Hunter	Hunter	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
spoluzakladatelem	spoluzakladatel	k1gMnSc7
BHR	BHR	kA
Partners	Partnersa	k1gFnPc2
<g/>
,	,	kIx,
čínsko-americké	čínsko-americký	k2eAgFnPc1d1
investiční	investiční	k2eAgFnPc1d1
firmy	firma	k1gFnPc1
mající	mající	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
státem	stát	k1gInSc7
vlastněné	vlastněný	k2eAgFnSc2d1
Bank	bank	k1gInSc4
of	of	k?
China	China	k1gFnSc1
a	a	k8xC
investující	investující	k2eAgInSc1d1
v	v	k7c6
mnoha	mnoho	k4c6
čínsko-amerických	čínsko-americký	k2eAgInPc6d1
projektech	projekt	k1gInPc6
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
někteří	některý	k3yIgMnPc1
kritici	kritik	k1gMnPc1
upozorňovali	upozorňovat	k5eAaImAgMnP
na	na	k7c4
možný	možný	k2eAgInSc4d1
střet	střet	k1gInSc4
zájmů	zájem	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2019	#num#	k4
čelil	čelit	k5eAaImAgInS
kritice	kritika	k1gFnSc3
ze	z	k7c2
strany	strana	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
republikánů	republikán	k1gMnPc2
i	i	k8xC
některých	některý	k3yIgMnPc2
demokratů	demokrat	k1gMnPc2
<g/>
,	,	kIx,
když	když	k8xS
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Čína	Čína	k1gFnSc1
pro	pro	k7c4
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
nepředstavuje	představovat	k5eNaImIp3nS
hrozbu	hrozba	k1gFnSc4
a	a	k8xC
konkurenci	konkurence	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
později	pozdě	k6eAd2
během	během	k7c2
kampaně	kampaň	k1gFnSc2
před	před	k7c7
volbou	volba	k1gFnSc7
prezidenta	prezident	k1gMnSc2
USA	USA	kA
2020	#num#	k4
byl	být	k5eAaImAgMnS
vůči	vůči	k7c3
Číně	Čína	k1gFnSc3
kritičtější	kritický	k2eAgFnSc3d2
a	a	k8xC
snahu	snaha	k1gFnSc4
čínské	čínský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
o	o	k7c4
kulturní	kulturní	k2eAgFnSc4d1
převýchovu	převýchova	k1gFnSc4
Ujgurů	Ujgur	k1gInPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
muslimských	muslimský	k2eAgFnPc2d1
menšin	menšina	k1gFnPc2
v	v	k7c6
čínském	čínský	k2eAgInSc6d1
Sin-ťiangu	Sin-ťiang	k1gInSc6
označil	označit	k5eAaPmAgMnS
za	za	k7c4
genocidu	genocida	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biden	Biden	k1gInSc1
a	a	k8xC
Česko	Česko	k1gNnSc1
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
navštívil	navštívit	k5eAaPmAgInS
Prahu	Praha	k1gFnSc4
v	v	k7c6
srpnu	srpen	k1gInSc6
1977	#num#	k4
jako	jako	k8xC,k8xS
člen	člen	k1gMnSc1
senátního	senátní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
zahraniční	zahraniční	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
Společně	společně	k6eAd1
s	s	k7c7
pěti	pět	k4xCc7
kongresmany	kongresman	k1gMnPc7
Sněmovny	sněmovna	k1gFnSc2
reprezentantů	reprezentant	k1gMnPc2
navštívil	navštívit	k5eAaPmAgInS
Památník	památník	k1gInSc4
Lidice	Lidice	k1gInPc4
<g/>
,	,	kIx,
Památník	památník	k1gInSc1
Terezín	Terezín	k1gInSc1
a	a	k8xC
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
místopředsedou	místopředseda	k1gMnSc7
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
Jánem	Ján	k1gMnSc7
Markem	Marek	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
viceprezidenta	viceprezident	k1gMnSc2
přijel	přijet	k5eAaPmAgMnS
na	na	k7c4
návštěvu	návštěva	k1gFnSc4
Česka	Česko	k1gNnSc2
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
Setkal	setkat	k5eAaPmAgInS
se	se	k3xPyFc4
s	s	k7c7
prezidentem	prezident	k1gMnSc7
Václavem	Václav	k1gMnSc7
Klausem	Klaus	k1gMnSc7
a	a	k8xC
premiérem	premiér	k1gMnSc7
Janem	Jan	k1gMnSc7
Fischerem	Fischer	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návštěva	návštěva	k1gFnSc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
měsíc	měsíc	k1gInSc4
po	po	k7c4
oznámení	oznámení	k1gNnSc4
americké	americký	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
odstoupila	odstoupit	k5eAaPmAgFnS
od	od	k7c2
záměru	záměr	k1gInSc2
umístit	umístit	k5eAaPmF
vojenský	vojenský	k2eAgInSc4d1
radar	radar	k1gInSc4
v	v	k7c6
Brdech	Brdy	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
Biden	Biden	k1gInSc1
a	a	k8xC
Fischer	Fischer	k1gMnSc1
po	po	k7c6
společném	společný	k2eAgNnSc6d1
jednání	jednání	k1gNnSc6
oznámili	oznámit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
Česko	Česko	k1gNnSc1
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
podílet	podílet	k5eAaImF
na	na	k7c6
nové	nový	k2eAgFnSc6d1
koncepci	koncepce	k1gFnSc6
americké	americký	k2eAgFnSc2d1
protiraketové	protiraketový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
Bývalý	bývalý	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Mirek	Mirek	k1gMnSc1
Topolánek	Topolánek	k1gMnSc1
s	s	k7c7
Bidenem	Biden	k1gMnSc7
jednal	jednat	k5eAaImAgMnS
o	o	k7c6
české	český	k2eAgFnSc6d1
účasti	účast	k1gFnSc6
ve	v	k7c6
válce	válka	k1gFnSc6
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
a	a	k8xC
o	o	k7c6
česko-amerických	česko-americký	k2eAgFnPc6d1
smlouvách	smlouva	k1gFnPc6
ve	v	k7c6
vědě	věda	k1gFnSc6
a	a	k8xC
výzkumu	výzkum	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Řád	řád	k1gInSc1
kříže	kříž	k1gInSc2
země	zem	k1gFnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
I.	I.	kA
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
Estonsko	Estonsko	k1gNnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
získává	získávat	k5eAaImIp3nS
od	od	k7c2
prezidenta	prezident	k1gMnSc2
Obamy	Obama	k1gFnSc2
Prezidentskou	prezidentský	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
svobody	svoboda	k1gFnSc2
s	s	k7c7
vyznamenáním	vyznamenání	k1gNnSc7
<g/>
,	,	kIx,
2017	#num#	k4
Vítězný	vítězný	k2eAgInSc1d1
řád	řád	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Jiří	Jiří	k1gMnSc2
(	(	kIx(
<g/>
Gruzie	Gruzie	k1gFnSc1
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
velkodůstojník	velkodůstojník	k1gInSc1
Řádu	řád	k1gInSc2
tří	tři	k4xCgFnPc2
hvězd	hvězda	k1gFnPc2
(	(	kIx(
<g/>
Lotyšsko	Lotyšsko	k1gNnSc1
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řád	řád	k1gInSc1
Pákistánu	Pákistán	k1gInSc2
(	(	kIx(
<g/>
Pákistán	Pákistán	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Řád	řád	k1gInSc1
svobody	svoboda	k1gFnSc2
(	(	kIx(
<g/>
Ukrajina	Ukrajina	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prezidentská	prezidentský	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
svobody	svoboda	k1gFnSc2
s	s	k7c7
vyznamenáním	vyznamenání	k1gNnSc7
(	(	kIx(
<g/>
USA	USA	kA
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Biden	Biden	k1gInSc1
překonal	překonat	k5eAaPmAgInS
věkový	věkový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
svého	svůj	k3xOyFgMnSc2
předchůdce	předchůdce	k1gMnSc2
v	v	k7c6
úřadu	úřad	k1gInSc6
Donalda	Donald	k1gMnSc2
Trumpa	Trump	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
při	při	k7c6
svém	svůj	k3xOyFgInSc6
nástupu	nástup	k1gInSc6
do	do	k7c2
úřadu	úřad	k1gInSc2
70	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Joe	Joe	k1gFnSc2
Biden	Bidna	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
John	John	k1gMnSc1
M.	M.	kA
Broder	Broder	k1gMnSc1
<g/>
:	:	kIx,
Father	Fathra	k1gFnPc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Tough	Tough	k1gMnSc1
Life	Lif	k1gFnSc2
an	an	k?
Inspiration	Inspiration	k1gInSc1
for	forum	k1gNnPc2
Biden	Bidna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Obamův	Obamův	k2eAgInSc1d1
rekordní	rekordní	k2eAgInSc1d1
počet	počet	k1gInSc1
hlasů	hlas	k1gInPc2
překonal	překonat	k5eAaPmAgInS
i	i	k9
Trump	Trump	k1gInSc1
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
WITCOVER	WITCOVER	kA
<g/>
,	,	kIx,
Jules	Jules	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joe	Joe	k1gFnSc1
Biden	Bidna	k1gFnPc2
:	:	kIx,
a	a	k8xC
life	life	k6eAd1
of	of	k?
trial	trial	k1gInSc1
and	and	k?
redemption	redemption	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
st	st	kA
ed	ed	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
William	William	k1gInSc1
Morrow	Morrow	k1gFnSc2
<g/>
/	/	kIx~
<g/>
HarperCollins	HarperCollins	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
536	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
179198	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
179198	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
526057595	#num#	k4
↑	↑	k?
US	US	kA
presidential	presidentiat	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
candidate	candidat	k1gInSc5
Joe	Joe	k1gFnPc2
Biden	Biden	k2eAgInSc1d1
<g/>
’	’	k?
<g/>
s	s	k7c7
family	famil	k1gInPc7
tree	tre	k1gFnSc2
has	hasit	k5eAaImRp2nS
roots	roots	k1gInSc1
in	in	k?
Sussex	Sussex	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Argus	Argus	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Did	Did	k1gFnSc1
Joe	Joe	k1gMnSc1
Biden	Biden	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Great-Grandfather	Great-Grandfathra	k1gFnPc2
Own	Own	k1gMnSc1
Slaves	Slaves	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snopes	Snopes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://www.auburntownship.org/COMMENTARY2/BIDEN%20ANCESTRY%20PART%201_UPDATE.pdf1	https://www.auburntownship.org/COMMENTARY2/BIDEN%20ANCESTRY%20PART%201_UPDATE.pdf1	k4
2	#num#	k4
HAYES	HAYES	kA
<g/>
,	,	kIx,
Isabel	Isabela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Experts	Experts	k1gInSc1
have	hav	k1gFnSc2
delved	delved	k1gMnSc1
into	into	k1gMnSc1
Joe	Joe	k1gMnSc1
Biden	Biden	k2eAgMnSc1d1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Irish	Irish	k1gInSc1
roots	roots	k1gInSc4
-	-	kIx~
and	and	k?
they	thea	k1gFnSc2
go	go	k?
back	back	k1gInSc1
a	a	k8xC
while	while	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
TheJournal	TheJournal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
ie	ie	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
BRODER	BRODER	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
M.	M.	kA
Father	Fathra	k1gFnPc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Tough	Tough	k1gMnSc1
Life	Lif	k1gFnSc2
an	an	k?
Inspiration	Inspiration	k1gInSc1
for	forum	k1gNnPc2
Biden	Biden	k1gInSc1
(	(	kIx(
<g/>
Published	Published	k1gInSc1
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Biden	Bidna	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
regular	regular	k1gMnSc1
Joe	Joe	k1gMnSc7
side	sid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-08-24	2008-08-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DICKENSON	DICKENSON	kA
<g/>
,	,	kIx,
James	James	k1gInSc4
R.	R.	kA
Biden	Biden	k2eAgMnSc1d1
Academic	Academic	k1gMnSc1
Claims	Claimsa	k1gFnPc2
'	'	kIx"
<g/>
Inaccurate	Inaccurat	k1gInSc5
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
Post	post	k1gInSc4
<g/>
.	.	kIx.
1987	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
190	#num#	k4
<g/>
-	-	kIx~
<g/>
8286	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DELAWARE	DELAWARE	kA
DEPARTMENT	department	k1gInSc4
OF	OF	kA
ELECTIONS	ELECTIONS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
State	status	k1gInSc5
of	of	k?
Delaware	Delawar	k1gMnSc5
2006	#num#	k4
Election	Election	k1gInSc1
Results	Results	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Delaware	Delawar	k1gMnSc5
Department	department	k1gInSc4
of	of	k?
Elections	Elections	k1gInSc1
<g/>
,	,	kIx,
November	November	k1gInSc1
7	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ENTOUS	ENTOUS	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Will	Will	k1gMnSc1
Hunter	Hunter	k1gMnSc1
Biden	Biden	k1gInSc4
Jeopardize	Jeopardize	k1gFnSc2
His	his	k1gNnSc2
Father	Fathra	k1gFnPc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Campaign	Campaigna	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
New	New	k1gMnSc1
Yorker	Yorker	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BARRETT	BARRETT	kA
<g/>
,	,	kIx,
Laurence	Laurence	k1gFnSc2
I.	I.	kA
Campaign	Campaign	k1gMnSc1
Portrait	Portrait	k1gMnSc1
<g/>
,	,	kIx,
Joe	Joe	k1gMnSc1
Biden	Biden	k2eAgMnSc1d1
<g/>
:	:	kIx,
Orator	Orator	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
Next	Next	k2eAgInSc4d1
Generation	Generation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Time	Time	k1gNnPc1
<g/>
.	.	kIx.
1987	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
40	#num#	k4
<g/>
-	-	kIx~
<g/>
781	#num#	k4
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Barack	Barack	k1gMnSc1
Obama	Obama	k?
vybral	vybrat	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc4
kandidáta	kandidát	k1gMnSc4
na	na	k7c4
viceprezidenta	viceprezident	k1gMnSc4
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
↑	↑	k?
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
mají	mít	k5eAaImIp3nP
za	za	k7c7
sebou	se	k3xPyFc7
ostře	ostro	k6eAd1
sledovanou	sledovaný	k2eAgFnSc4d1
debatu	debata	k1gFnSc4
kandidátů	kandidát	k1gMnPc2
na	na	k7c4
post	post	k1gInSc4
viceprezidenta	viceprezident	k1gMnSc2
<g/>
↑	↑	k?
Debata	debata	k1gFnSc1
Palinová-Biden	Palinová-Bidno	k1gNnPc2
skončila	skončit	k5eAaPmAgFnS
bez	bez	k7c2
osobních	osobní	k2eAgInPc2d1
útoků	útok	k1gInPc2
<g/>
↑	↑	k?
viceprezidentský	viceprezidentský	k2eAgInSc1d1
duel	duel	k1gInSc1
<g/>
:	:	kIx,
Palinová	Palinová	k1gFnSc1
překvapila	překvapit	k5eAaPmAgFnS
<g/>
,	,	kIx,
Biden	Biden	k1gInSc1
zvítězil	zvítězit	k5eAaPmAgInS
<g/>
↑	↑	k?
Joe	Joe	k1gFnSc3
Biden	Biden	k2eAgInSc1d1
<g/>
:	:	kIx,
The	The	k1gMnSc1
likeable	likeable	k6eAd1
joe	joe	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-03-13	2010-03-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Obama	Obama	k?
si	se	k3xPyFc3
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
spolukandidáta	spolukandidát	k1gMnSc4
zvolil	zvolit	k5eAaPmAgMnS
silného	silný	k2eAgMnSc4d1
kritika	kritik	k1gMnSc4
protiraketové	protiraketový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
www.greenpeace.org	www.greenpeace.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Rusko	Rusko	k1gNnSc1
chce	chtít	k5eAaImIp3nS
s	s	k7c7
postsovětskými	postsovětský	k2eAgMnPc7d1
spojenci	spojenec	k1gMnPc7
vybudovat	vybudovat	k5eAaPmF
vlastní	vlastní	k2eAgInSc4d1
protiraketový	protiraketový	k2eAgInSc4d1
štít	štít	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Putin	putin	k2eAgInSc1d1
o	o	k7c6
americkém	americký	k2eAgInSc6d1
štítu	štít	k1gInSc6
<g/>
:	:	kIx,
Nenecháme	nechat	k5eNaPmIp1nP
se	se	k3xPyFc4
zatáhnout	zatáhnout	k5eAaPmF
do	do	k7c2
nového	nový	k2eAgInSc2d1
závodu	závod	k1gInSc2
ve	v	k7c6
zbrojení	zbrojení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
As	as	k1gInSc1
the	the	k?
Ground	Ground	k1gInSc1
Shifts	Shifts	k1gInSc1
<g/>
,	,	kIx,
Biden	Biden	k2eAgInSc1d1
Plays	Plays	k1gInSc1
a	a	k8xC
Bigger	Bigger	k1gInSc1
Role	role	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Joe	Joe	k1gFnSc1
Biden	Bidno	k1gNnPc2
sází	sázet	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
přežblept	přežblept	k1gInSc4
za	za	k7c7
druhým	druhý	k4xOgInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nemá	mít	k5eNaImIp3nS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgMnS
prezident	prezident	k1gMnSc1
<g/>
,	,	kIx,
útočí	útočit	k5eAaImIp3nS
Trump	Trump	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PACE	Paka	k1gFnSc6
<g/>
,	,	kIx,
Julie	Julie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analysis	Analysis	k1gInSc1
<g/>
:	:	kIx,
Biden	Biden	k2eAgInSc1d1
positions	positions	k1gInSc1
himself	himself	k1gInSc4
as	as	k9
leading	leading	k1gInSc4
moderate	moderat	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Associated	Associated	k1gInSc1
Press	Press	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-03-01	2020-03-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PEOPLES	PEOPLES	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
Super	super	k2eAgFnPc3d1
Tuesday	Tuesda	k2eAgFnPc4d1
eve	eve	k?
<g/>
,	,	kIx,
Biden	Biden	k2eAgMnSc1d1
gets	gets	k6eAd1
boost	boost	k1gMnSc1
from	from	k1gMnSc1
former	former	k1gMnSc1
rivals	rivals	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Associated	Associated	k1gInSc1
Press	Press	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-03-03	2020-03-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NIQUETTE	NIQUETTE	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Michael	Michael	k1gMnSc1
Bloomberg	Bloomberg	k1gMnSc1
Ends	Endsa	k1gFnPc2
Presidential	Presidential	k1gMnSc1
Bid	Bid	k1gMnSc1
<g/>
,	,	kIx,
Endorses	Endorses	k1gMnSc1
Joe	Joe	k1gFnSc2
Biden	Biden	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloomberg	Bloomberg	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-03-04	2020-03-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Booker	Booker	k1gInSc1
endorses	endorses	k1gMnSc1
Biden	Biden	k1gInSc1
<g/>
,	,	kIx,
says	says	k6eAd1
he	he	k0
<g/>
’	’	k?
<g/>
ll	ll	k?
‘	‘	k?
<g/>
restore	restor	k1gMnSc5
honor	honor	k1gInSc1
<g/>
’	’	k?
to	ten	k3xDgNnSc1
office	office	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Associated	Associated	k1gInSc1
Press	Press	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-03-09	2020-03-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Who	Who	k1gMnSc1
Will	Will	k1gMnSc1
Win	Win	k1gMnSc1
The	The	k1gMnSc1
2020	#num#	k4
Democratic	Democratice	k1gFnPc2
Primary	Primara	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FiveThirtyEight	FiveThirtyEight	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Democratic	Democratice	k1gFnPc2
National	National	k1gFnSc1
Convention	Convention	k1gInSc1
Democratic	Democratice	k1gFnPc2
Party	party	k1gFnPc2
Streams	Streams	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wisconsin	Wisconsin	k1gInSc1
<g/>
:	:	kIx,
2020	#num#	k4
Democratic	Democratice	k1gFnPc2
National	National	k1gFnPc2
Convention	Convention	k1gInSc1
<g/>
,	,	kIx,
2020-08-18	2020-08-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MCGANN	MCGANN	kA
<g/>
,	,	kIx,
Laura	Laura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lucy	Luca	k1gFnSc2
Flores	Floresa	k1gFnPc2
isn	isn	k?
<g/>
’	’	k?
<g/>
t	t	k?
alone	alonout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joe	Joe	k1gFnSc1
Biden	Bidna	k1gFnPc2
<g/>
’	’	k?
<g/>
s	s	k7c7
got	got	k?
a	a	k8xC
long	long	k1gInSc1
history	histor	k1gInPc1
of	of	k?
touching	touching	k1gInSc1
women	womna	k1gFnPc2
inappropriately	inappropriatela	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
FACEBOOK	FACEBOOK	kA
<g/>
;	;	kIx,
TWITTER	TWITTER	kA
<g/>
;	;	kIx,
OPTIONS	OPTIONS	kA
<g/>
,	,	kIx,
Show	show	k1gFnSc1
more	mor	k1gInSc5
sharing	sharing	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevada	Nevada	k1gFnSc1
Democrat	Democrat	k1gMnSc1
accuses	accuses	k1gMnSc1
Joe	Joe	k1gMnSc1
Biden	Biden	k2eAgMnSc1d1
of	of	k?
touching	touching	k1gInSc1
and	and	k?
kissing	kissing	k1gInSc1
her	hra	k1gFnPc2
without	without	k5eAaImF,k5eAaPmF
consent	consent	k1gInSc4
at	at	k?
2014	#num#	k4
event.	event.	k?
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-30	2019-03-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Příliš	příliš	k6eAd1
náklonnosti	náklonnost	k1gFnSc2
škodí	škodit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Joe	Joe	k1gFnSc1
Biden	Bidna	k1gFnPc2
čelí	čelit	k5eAaImIp3nS
nařčení	nařčení	k1gNnSc1
z	z	k7c2
nevhodného	vhodný	k2eNgNnSc2d1
chování	chování	k1gNnSc2
vůči	vůči	k7c3
ženám	žena	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
O	O	kA
<g/>
'	'	kIx"
<g/>
CONNORS	CONNORS	kA
<g/>
,	,	kIx,
Lydia	Lydium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ex-Nevada	Ex-Nevada	k1gFnSc1
Assemblywoman	Assemblywoman	k1gMnSc1
Says	Says	k1gInSc1
Joe	Joe	k1gMnSc1
Biden	Bidna	k1gFnPc2
Inappropriately	Inappropriatela	k1gFnSc2
Kissed	Kissed	k1gMnSc1
Her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Huff	Huff	k1gInSc1
Post	post	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-29	2019-03-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NEIL	NEIL	kA
<g/>
,	,	kIx,
Vigdor	Vigdor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Connecticut	Connecticut	k2eAgInSc1d1
woman	woman	k1gInSc1
says	saysa	k1gFnPc2
then-Vice	then-Vice	k1gFnSc2
President	president	k1gMnSc1
Joe	Joe	k1gMnSc1
Biden	Biden	k2eAgInSc4d1
touched	touched	k1gInSc4
her	hra	k1gFnPc2
inappropriately	inappropriatela	k1gFnSc2
at	at	k?
a	a	k8xC
Greenwich	Greenwich	k1gInSc1
fundraiser	fundraiser	k1gInSc1
in	in	k?
2009	#num#	k4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hartford	Hartford	k1gInSc1
Courant	Courant	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hartford	Hartford	k1gInSc1
Courant	Courant	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://www.nytimes.com/2019/04/02/us/politics/joe-biden-women-me-too.html	https://www.nytimes.com/2019/04/02/us/politics/joe-biden-women-me-too.html	k1gInSc1
<g/>
↑	↑	k?
https://thehill.com/homenews/campaign/437069-two-more-women-accuse-biden-of-inappropriate-touching	https://thehill.com/homenews/campaign/437069-two-more-women-accuse-biden-of-inappropriate-touching	k1gInSc1
<g/>
↑	↑	k?
https://www.washingtonpost.com/politics/biden-says-hell-adjust-his-physical-behavior-as-three-more-women-come-forward/2019/04/03/94a2ed2c-5622-11e9-8ef3-fbd41a2ce4d5_story.html	https://www.washingtonpost.com/politics/biden-says-hell-adjust-his-physical-behavior-as-three-more-women-come-forward/2019/04/03/94a2ed2c-5622-11e9-8ef3-fbd41a2ce4d5_story.html	k1gInSc1
<g/>
↑	↑	k?
https://web.archive.org/web/20200401210312/https://www.theunion.com/news/nevada-county-woman-says-joe-biden-inappropriately-touched-her-while-working-in-his-u-s-senate-office/	https://web.archive.org/web/20200401210312/https://www.theunion.com/news/nevada-county-woman-says-joe-biden-inappropriately-touched-her-while-working-in-his-u-s-senate-office/	k4
<g/>
↑	↑	k?
https://web.archive.org/web/20200414043025/https://www.nytimes.com/2020/04/12/us/politics/joe-biden-tara-reade-sexual-assault-complaint.html	https://web.archive.org/web/20200414043025/https://www.nytimes.com/2020/04/12/us/politics/joe-biden-tara-reade-sexual-assault-complaint.html	k1gMnSc1
<g/>
↑	↑	k?
https://www.washingtonpost.com/politics/sexual-assault-allegation-by-former-biden-senate-aide-emerges-in-campaign-draws-denial/2020/04/12/bc070d66-7067-11ea-b148-e4ce3fbd85b5_story.html	https://www.washingtonpost.com/politics/sexual-assault-allegation-by-former-biden-senate-aide-emerges-in-campaign-draws-denial/2020/04/12/bc070d66-7067-11ea-b148-e4ce3fbd85b5_story.html	k1gMnSc1
<g/>
↑	↑	k?
https://apnews.com/d922da60baa91121f4529fe51a0fd55a	https://apnews.com/d922da60baa91121f4529fe51a0fd55a	k1gMnSc1
<g/>
↑	↑	k?
https://web.archive.org/web/20200414043025/https://www.nytimes.com/2020/04/12/us/politics/joe-biden-tara-reade-sexual-assault-complaint.html	https://web.archive.org/web/20200414043025/https://www.nytimes.com/2020/04/12/us/politics/joe-biden-tara-reade-sexual-assault-complaint.html	k1gMnSc1
<g/>
↑	↑	k?
https://www.npr.org/2020/04/19/837966525/on-the-record-a-former-biden-staffers-sexual-assault-allegation	https://www.npr.org/2020/04/19/837966525/on-the-record-a-former-biden-staffers-sexual-assault-allegation	k1gInSc1
<g/>
↑	↑	k?
Joe	Joe	k1gMnSc2
Biden	Biden	k2eAgInSc4d1
Said	Said	k1gInSc4
He	he	k0
Believes	Believesa	k1gFnPc2
All	All	k1gFnSc7
Women	Womna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Does	Does	k1gInSc1
He	he	k0
Believe	Believ	k1gInSc5
Tara	Tarum	k1gNnPc1
Reade	Read	k1gMnSc5
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reason	Reason	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LERER	LERER	kA
<g/>
,	,	kIx,
Lisa	Lisa	k1gFnSc1
<g/>
;	;	kIx,
RUTENBERG	RUTENBERG	kA
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
<g/>
;	;	kIx,
SAUL	Saul	k1gMnSc1
<g/>
,	,	kIx,
Stephanie	Stephanie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tara	Tar	k1gInSc2
Reade	Read	k1gInSc5
Is	Is	k1gFnSc4
Dropped	Dropped	k1gMnSc1
as	as	k9
Client	Client	k1gInSc4
by	by	kYmCp3nS
a	a	k8xC
Leading	Leading	k1gInSc1
#	#	kIx~
<g/>
MeToo	MeToo	k1gMnSc1
Lawyer	Lawyer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
NEWS	NEWS	kA
<g/>
,	,	kIx,
A.	A.	kA
B.	B.	kA
C.	C.	kA
Prosecutors	Prosecutors	k1gInSc1
open	open	k1gNnSc1
probe	probat	k5eAaPmIp3nS
into	into	k6eAd1
whether	whethra	k1gFnPc2
Biden	Biden	k2eAgMnSc1d1
accuser	accuser	k1gMnSc1
Tara	Tara	k1gMnSc1
Reade	Read	k1gInSc5
lied	lied	k1gMnSc1
under	undra	k1gFnPc2
oath	oath	k1gMnSc1
in	in	k?
court	court	k1gInSc1
appearances	appearances	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ABC	ABC	kA
News	Newsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Report	report	k1gInSc1
<g/>
:	:	kIx,
Two	Two	k1gMnSc1
People	People	k1gMnSc1
Recall	Recall	k1gMnSc1
Tara	Tara	k1gMnSc1
Reade	Read	k1gInSc5
Describing	Describing	k1gInSc4
Assault	Assault	k2eAgInSc4d1
or	or	k?
Harassment	Harassment	k1gInSc4
by	by	kYmCp3nS
Joe	Joe	k1gFnSc1
Biden	Bidna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slate	Slat	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bidenova	Bidenův	k2eAgFnSc1d1
migrační	migrační	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstřícnost	vstřícnost	k1gFnSc1
vábí	vábit	k5eAaImIp3nS
k	k	k7c3
hranici	hranice	k1gFnSc3
USA	USA	kA
čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
dál	daleko	k6eAd2
víc	hodně	k6eAd2
rodin	rodina	k1gFnPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Někteří	některý	k3yIgMnPc1
pašeráci	pašerák	k1gMnPc1
házejí	házet	k5eAaImIp3nP
děti	dítě	k1gFnPc4
přes	přes	k7c4
plot	plot	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
čekají	čekat	k5eAaImIp3nP
největší	veliký	k2eAgFnSc4d3
vlnu	vlna	k1gFnSc4
migrantů	migrant	k1gMnPc2
za	za	k7c2
20	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Harrisová	Harrisový	k2eAgFnSc1d1
dostala	dostat	k5eAaPmAgFnS
na	na	k7c4
starost	starost	k1gFnSc4
zmírnění	zmírnění	k1gNnSc4
imigrace	imigrace	k1gFnSc2
ze	z	k7c2
Střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČTK	ČTK	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bidena	Bidena	k1gFnSc1
tíží	tížit	k5eAaImIp3nS
krize	krize	k1gFnSc1
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Migrace	migrace	k1gFnSc1
z	z	k7c2
Mexika	Mexiko	k1gNnSc2
může	moct	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
pomoci	pomoct	k5eAaPmF
republikánům	republikán	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
USA	USA	kA
provozuje	provozovat	k5eAaImIp3nS
94	#num#	k4
jaderných	jaderný	k2eAgInPc2d1
reaktorů	reaktor	k1gInPc2
a	a	k8xC
Biden	Bidna	k1gFnPc2
s	s	k7c7
jádrem	jádro	k1gNnSc7
počítá	počítat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kurzy	kurz	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Evropa	Evropa	k1gFnSc1
propadne	propadnout	k5eAaPmIp3nS
ruskému	ruský	k2eAgInSc3d1
plynu	plyn	k1gInSc3
<g/>
,	,	kIx,
kritizuje	kritizovat	k5eAaImIp3nS
Biden	Biden	k2eAgInSc1d1
projekt	projekt	k1gInSc1
Nord	Nord	k1gInSc1
Stream	Stream	k1gInSc1
2	#num#	k4
a	a	k8xC
zvažuje	zvažovat	k5eAaImIp3nS
sankce	sankce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
E15	E15	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biden	Biden	k1gInSc1
a	a	k8xC
Harrisová	Harrisový	k2eAgFnSc1d1
složili	složit	k5eAaPmAgMnP
přísahu	přísaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkcí	funkce	k1gFnPc2
se	se	k3xPyFc4
ujali	ujmout	k5eAaPmAgMnP
i	i	k9
noví	nový	k2eAgMnPc1d1
senátoři	senátor	k1gMnPc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zajistilo	zajistit	k5eAaPmAgNnS
demokratům	demokrat	k1gMnPc3
většinu	většina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
OBRAZEM	obraz	k1gInSc7
<g/>
:	:	kIx,
Od	od	k7c2
Trumpova	Trumpův	k2eAgInSc2d1
odletu	odlet	k1gInSc2
po	po	k7c4
Bidenovu	Bidenův	k2eAgFnSc4d1
přísahu	přísaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
vypadal	vypadat	k5eAaImAgInS,k5eAaPmAgInS
den	den	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
měnil	měnit	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bidenova	Bidenův	k2eAgFnSc1d1
inaugurace	inaugurace	k1gFnSc1
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zazpívá	zazpívat	k5eAaPmIp3nS
Lady	lady	k1gFnSc1
Gaga	Gaga	k1gFnSc1
<g/>
,	,	kIx,
Trump	Trump	k1gInSc1
natruc	natruc	k6eAd1
nepřijde	přijít	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biden	Biden	k1gInSc1
začal	začít	k5eAaPmAgInS
úřadovat	úřadovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
první	první	k4xOgInSc4
den	den	k1gInSc4
ve	v	k7c6
funkci	funkce	k1gFnSc6
nařídil	nařídit	k5eAaPmAgMnS
návrat	návrat	k1gInSc4
k	k	k7c3
pařížské	pařížský	k2eAgFnSc3d1
dohodě	dohoda	k1gFnSc3
<g/>
,	,	kIx,
udělal	udělat	k5eAaPmAgInS
i	i	k9
další	další	k2eAgNnSc4d1
zásadní	zásadní	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-01-20	2021-01-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Biden	Biden	k1gInSc1
hned	hned	k6eAd1
první	první	k4xOgInSc1
den	den	k1gInSc1
nařídil	nařídit	k5eAaPmAgInS
nosit	nosit	k5eAaImF
roušky	rouška	k1gFnPc4
a	a	k8xC
zastavil	zastavit	k5eAaPmAgInS
stavbu	stavba	k1gFnSc4
Trumpovy	Trumpův	k2eAgFnSc2d1
zdi	zeď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
USA	USA	kA
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
ke	k	k7c3
klimatické	klimatický	k2eAgFnSc3d1
dohodě	dohoda	k1gFnSc3
<g/>
,	,	kIx,
končí	končit	k5eAaImIp3nS
i	i	k9
stavba	stavba	k1gFnSc1
hraniční	hraniční	k2eAgFnSc2d1
zdi	zeď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biden	Biden	k1gInSc1
se	se	k3xPyFc4
vrhl	vrhnout	k5eAaImAgInS,k5eAaPmAgInS
do	do	k7c2
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bidena	Bidena	k1gFnSc1
nečeká	čekat	k5eNaImIp3nS
jeho	jeho	k3xOp3gFnSc1
„	„	k?
<g/>
první	první	k4xOgNnSc4
rodeo	rodeo	k1gNnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
problémy	problém	k1gInPc4
ale	ale	k8xC
bude	být	k5eAaImBp3nS
muset	muset	k5eAaImF
řešit	řešit	k5eAaImF
jako	jako	k9
první	první	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-01-21	2021-01-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Biden	Biden	k1gInSc4
nařídil	nařídit	k5eAaPmAgMnS
návrat	návrat	k1gInSc4
USA	USA	kA
k	k	k7c3
Pařížské	pařížský	k2eAgFnSc3d1
dohodě	dohoda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američany	Američan	k1gMnPc4
vyzval	vyzvat	k5eAaPmAgMnS
k	k	k7c3
jednotě	jednota	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Transgender	Transgender	k1gInSc1
osoby	osoba	k1gFnSc2
se	se	k3xPyFc4
opět	opět	k6eAd1
mohou	moct	k5eAaImIp3nP
připojit	připojit	k5eAaPmF
k	k	k7c3
americké	americký	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biden	Biden	k1gInSc1
zrušil	zrušit	k5eAaPmAgInS
předchozí	předchozí	k2eAgInSc1d1
zákaz	zákaz	k1gInSc1
Trumpa	Trumpa	k1gFnSc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biden	Biden	k1gInSc1
opět	opět	k6eAd1
umožnil	umožnit	k5eAaPmAgInS
transgenderovým	transgenderův	k2eAgFnPc3d1
osobám	osoba	k1gFnPc3
jít	jít	k5eAaImF
do	do	k7c2
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
USA	USA	kA
jsou	být	k5eAaImIp3nP
připraveny	připravit	k5eAaPmNgFnP
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
Čínou	Čína	k1gFnSc7
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
Biden	Biden	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozastaví	pozastavit	k5eAaPmIp3nS
stahování	stahování	k1gNnSc4
americké	americký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
z	z	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Biden	Bidna	k1gFnPc2
poprvé	poprvé	k6eAd1
telefonoval	telefonovat	k5eAaImAgMnS
s	s	k7c7
čínským	čínský	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritizoval	kritizovat	k5eAaImAgMnS
opatření	opatření	k1gNnSc3
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
i	i	k9
obchodní	obchodní	k2eAgFnPc4d1
praktiky	praktika	k1gFnPc4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Starosti	starost	k1gFnPc1
o	o	k7c4
Hongkong	Hongkong	k1gInSc4
i	i	k8xC
Ujgury	Ujgura	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biden	Biden	k1gInSc1
Si	se	k3xPyFc3
Ťin-pchingovi	Ťin-pchingův	k2eAgMnPc1d1
vytkl	vytknout	k5eAaPmAgMnS
chování	chování	k1gNnSc1
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-02-11	2021-02-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
‚	‚	k?
<g/>
Pryč	pryč	k6eAd1
s	s	k7c7
diktaturou	diktatura	k1gFnSc7
<g/>
.	.	kIx.
<g/>
‘	‘	k?
Barmská	barmský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
vyslala	vyslat	k5eAaPmAgFnS
do	do	k7c2
ulic	ulice	k1gFnPc2
obrněná	obrněný	k2eAgNnPc4d1
vozidla	vozidlo	k1gNnPc4
<g/>
,	,	kIx,
demonstranty	demonstrant	k1gMnPc7
to	ten	k3xDgNnSc1
neodradilo	odradit	k5eNaPmAgNnS
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Biden	Biden	k1gInSc1
assures	assures	k1gMnSc1
US	US	kA
allies	allies	k1gMnSc1
he	he	k0
will	wilnout	k5eAaPmAgInS
reverse	reverse	k1gFnSc2
Trump	Trump	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
policies	policies	k1gInSc1
and	and	k?
legacy	legaca	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-02-19	2021-02-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
USA	USA	kA
dočasně	dočasně	k6eAd1
přijmou	přijmout	k5eAaPmIp3nP
uprchlé	uprchlý	k2eAgMnPc4d1
Venezuelany	Venezuelan	k1gMnPc4
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc1
domovina	domovina	k1gFnSc1
není	být	k5eNaImIp3nS
bezpečná	bezpečný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Biden	Biden	k1gInSc1
gives	gives	k1gMnSc1
Venezuelans	Venezuelans	k1gInSc1
in	in	k?
U.	U.	kA
<g/>
S.	S.	kA
chance	chanka	k1gFnSc6
for	forum	k1gNnPc2
Temporary	Temporara	k1gFnSc2
Protected	Protected	k1gMnSc1
Status	status	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NBC	NBC	kA
News	Newsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
První	první	k4xOgMnSc1
Bidenův	Bidenův	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kongres	kongres	k1gInSc1
definitivně	definitivně	k6eAd1
schválil	schválit	k5eAaPmAgInS
krizový	krizový	k2eAgInSc1d1
balíček	balíček	k1gInSc1
za	za	k7c4
téměř	téměř	k6eAd1
dva	dva	k4xCgInPc4
biliony	bilion	k4xCgInPc4
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bidenovo	Bidenův	k2eAgNnSc4d1
první	první	k4xOgNnSc4
vítězství	vítězství	k1gNnSc4
<g/>
:	:	kIx,
Senát	senát	k1gInSc1
USA	USA	kA
schválil	schválit	k5eAaPmAgInS
pomoc	pomoc	k1gFnSc4
ekonomice	ekonomika	k1gFnSc3
za	za	k7c4
1,9	1,9	k4
bilionu	bilion	k4xCgInSc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-06	2021-03-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bidenova	Bidenův	k2eAgFnSc1d1
administrativa	administrativa	k1gFnSc1
obnovila	obnovit	k5eAaPmAgFnS
finanční	finanční	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
pro	pro	k7c4
Palestinu	Palestina	k1gFnSc4
<g/>
,	,	kIx,
pošle	poslat	k5eAaPmIp3nS
15	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
Voter	Voter	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Self	Self	k1gInSc1
Defense	defense	k1gFnSc1
System	Syst	k1gInSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
justfacts	justfacts	k1gInSc1
<g/>
.	.	kIx.
<g/>
votesmart	votesmart	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Politika	politika	k1gFnSc1
Joea	Joea	k1gFnSc1
Bidena	Bidena	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Přinášíme	přinášet	k5eAaImIp1nP
vám	vy	k3xPp2nPc3
výběr	výběr	k1gInSc4
Bidenových	Bidenův	k2eAgInPc2d1
názorů	názor	k1gInPc2
na	na	k7c4
často	často	k6eAd1
diskutovaná	diskutovaný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
dnešní	dnešní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VP	VP	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
LGBT	LGBT	kA
comments	comments	k6eAd1
raise	raise	k6eAd1
eyebrows	eyebrows	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politico	Politico	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Brunei	Brune	k1gFnSc2
defends	defendsa	k1gFnPc2
tough	tough	k1gMnSc1
new	new	k?
Islamic	Islamic	k1gMnSc1
laws	laws	k6eAd1
against	against	k5eAaPmF
growing	growing	k1gInSc4
backlash	backlash	k1gInSc1
<g/>
.	.	kIx.
www.reuters.com	www.reuters.com	k1gInSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
OLIPHANT	OLIPHANT	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harris	Harris	k1gFnSc1
challenges	challengesa	k1gFnPc2
Biden	Bidna	k1gFnPc2
in	in	k?
breakout	breakout	k1gMnSc1
U.	U.	kA
<g/>
S.	S.	kA
debate	debat	k1gInSc5
performance	performance	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Joe	Joe	k1gFnPc2
Biden	Bidna	k1gFnPc2
<g/>
’	’	k?
<g/>
s	s	k7c7
record	record	k6eAd1
on	on	k3xPp3gInSc1
school	school	k1gInSc1
desegregation	desegregation	k1gInSc1
busing	busing	k1gInSc1
<g/>
,	,	kIx,
explained	explained	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vox	Vox	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Joe	Joe	k1gFnSc1
Biden	Biden	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
dicey	dice	k1gMnPc7
past	past	k1gFnSc4
on	on	k3xPp3gMnSc1
racial	racial	k1gMnSc1
issues	issues	k1gMnSc1
could	could	k1gMnSc1
come	comat	k5eAaPmIp3nS
back	back	k6eAd1
to	ten	k3xDgNnSc4
bite	bit	k1gInSc5
him	him	k?
in	in	k?
the	the	k?
2020	#num#	k4
Democratic	Democratice	k1gFnPc2
primaries	primariesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Business	business	k1gInSc1
Insider	insider	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Affirmative	Affirmativ	k1gInSc5
action	action	k1gInSc1
opponents	opponents	k1gInSc4
ask	ask	k?
U.	U.	kA
<g/>
S.	S.	kA
Supreme	Suprem	k1gInSc5
Court	Court	k1gInSc1
to	ten	k3xDgNnSc4
take	takat	k5eAaPmIp3nS
up	up	k?
Harvard	Harvard	k1gInSc1
case	case	k1gFnSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
yahoo	yahoo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reuters	k1gInSc1
<g/>
,	,	kIx,
Yahoo	Yahoo	k1gNnSc1
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
2021-02-25	2021-02-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Nicnedělající	Nicnedělající	k2eAgInSc4d1
Trump	Trump	k1gInSc4
versus	versus	k7c1
pekingský	pekingský	k2eAgInSc4d1
Biden	Biden	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrá	ostrý	k2eAgFnSc1d1
americká	americký	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
vrcholí	vrcholit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Trump	Trump	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
And	Anda	k1gFnPc2
Biden	Biden	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Plans	Plans	k1gInSc1
On	on	k3xPp3gInSc1
Immigration	Immigration	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NPR	NPR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biden	Biden	k2eAgInSc4d1
says	says	k1gInSc4
border	border	k1gMnSc1
wall	wall	k1gMnSc1
construction	construction	k1gInSc4
will	wilnout	k5eAaPmAgMnS
stop	stop	k2eAgMnSc1d1
if	if	k?
he	he	k0
<g/>
’	’	k?
<g/>
s	s	k7c7
elected	elected	k1gMnSc1
president	president	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dallas	Dallas	k1gInSc1
News	News	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
chce	chtít	k5eAaImIp3nS
volit	volit	k5eAaImF
Trumpa	Trumpa	k1gFnSc1
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
černoch	černoch	k1gMnSc1
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
Biden	Biden	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
výrok	výrok	k1gInSc4
se	se	k3xPyFc4
omluvil	omluvit	k5eAaPmAgMnS
Zdroj	zdroj	k1gInSc4
<g/>
:	:	kIx,
https://www.idnes.cz/zpravy/zahranicni/joe-biden-donal-trump-kandidat-prezidentske-volby-usa-vyrok-omluva.A200523_114824_zahranicni_aug	https://www.idnes.cz/zpravy/zahranicni/joe-biden-donal-trump-kandidat-prezidentske-volby-usa-vyrok-omluva.A200523_114824_zahranicni_aug	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idnes	Idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biden	Biden	k2eAgInSc1d1
calls	calls	k1gInSc1
for	forum	k1gNnPc2
'	'	kIx"
<g/>
racial	racial	k1gInSc1
justice	justice	k1gFnSc2
<g/>
'	'	kIx"
during	during	k1gInSc1
emotional	emotionat	k5eAaPmAgInS,k5eAaImAgInS
George	Georg	k1gInPc4
Floyd	Floyda	k1gFnPc2
funeral	funerat	k5eAaImAgInS,k5eAaPmAgInS
speech	speech	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NBC	NBC	kA
News	Newsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biden	Biden	k1gInSc1
ťal	tít	k5eAaPmAgInS
do	do	k7c2
živého	živý	k2eAgMnSc2d1
<g/>
:	:	kIx,
Nadřazenost	nadřazenost	k1gFnSc1
bílých	bílý	k1gMnPc2
musí	muset	k5eAaImIp3nS
skončit	skončit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Trump	Trump	k1gInSc1
and	and	k?
Biden	Biden	k2eAgInSc1d1
wrangle	wrangle	k1gInSc1
over	overa	k1gFnPc2
climate	climat	k1gInSc5
change	change	k1gFnPc4
at	at	k?
first	first	k1gMnSc1
debate	debat	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vyšší	vysoký	k2eAgFnSc2d2
daně	daň	k1gFnSc2
a	a	k8xC
větší	veliký	k2eAgFnSc2d2
regulace	regulace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Investoři	investor	k1gMnPc1
se	se	k3xPyFc4
připravují	připravovat	k5eAaImIp3nP
na	na	k7c4
příchod	příchod	k1gInSc4
Bidena	Bideno	k1gNnSc2
do	do	k7c2
Bílého	bílý	k2eAgInSc2d1
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
(	(	kIx(
<g/>
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Joe	Joe	k1gMnSc1
Biden	Biden	k2eAgMnSc1d1
pledges	pledges	k1gMnSc1
to	ten	k3xDgNnSc4
roll	rolnout	k5eAaPmAgMnS
back	back	k1gMnSc1
Trump	Trump	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
corporate	corporat	k1gInSc5
tax	taxa	k1gFnPc2
cuts	cuts	k6eAd1
on	on	k3xPp3gMnSc1
'	'	kIx"
<g/>
day	day	k?
one	one	k?
<g/>
,	,	kIx,
<g/>
'	'	kIx"
saying	saying	k1gInSc1
it	it	k?
won	won	k1gInSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
hurt	hurt	k?
businesses	businesses	k1gInSc1
<g/>
'	'	kIx"
ability	abilita	k1gFnSc2
to	ten	k3xDgNnSc4
hire	hire	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Business	business	k1gInSc1
Insider	insider	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biden	Biden	k2eAgMnSc1d1
pledges	pledges	k1gMnSc1
to	ten	k3xDgNnSc4
roll	rolnout	k5eAaPmAgMnS
back	back	k1gMnSc1
Trump	Trump	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
tax	taxa	k1gFnPc2
cuts	cuts	k1gInSc1
<g/>
:	:	kIx,
'	'	kIx"
<g/>
A	a	k9
lot	lot	k1gInSc1
of	of	k?
you	you	k?
may	may	k?
not	nota	k1gFnPc2
like	likat	k5eAaPmIp3nS
that	that	k1gInSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
FOXBusiness	FOXBusiness	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Joe	Joe	k1gMnSc1
Biden	Biden	k2eAgMnSc1d1
hits	hits	k6eAd1
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
on	on	k3xPp3gMnSc1
COVID-19	COVID-19	k1gMnSc1
response	response	k1gFnSc2
ahead	ahead	k1gInSc1
of	of	k?
the	the	k?
president	president	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Janesville	Janesville	k1gNnSc7
rally	ralla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milwaukee	Milwauke	k1gInSc2
Journal	Journal	k1gFnSc2
Sentinel	sentinel	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Gordon	Gordon	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
R.	R.	kA
(	(	kIx(
<g/>
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
In	In	k1gFnSc1
Biden	Biden	k1gInSc1
<g/>
,	,	kIx,
Obama	Obama	k?
chooses	chooses	k1gMnSc1
a	a	k8xC
foreign	foreign	k1gMnSc1
policy	polica	k1gFnSc2
adherent	adherent	k1gMnSc1
of	of	k?
diplomacy	diplomaca	k1gFnSc2
before	befor	k1gInSc5
force	force	k1gFnSc3
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Richter	Richter	k1gMnSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
;	;	kIx,
Levey	Levea	k1gFnPc1
<g/>
,	,	kIx,
Noam	Noam	k1gMnSc1
N.	N.	kA
Joe	Joe	k1gMnSc1
Biden	Biden	k2eAgMnSc1d1
respected	respected	k1gMnSc1
–	–	k?
if	if	k?
not	nota	k1gFnPc2
always	always	k1gInSc1
popular	popular	k1gMnSc1
–	–	k?
for	forum	k1gNnPc2
foreign	foreign	k1gMnSc1
policy	polica	k1gFnSc2
record	record	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MEMOLI	MEMOLI	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
A.	A.	kA
LA	la	k1gNnSc1
Times	Times	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kadafi	Kadafi	k1gNnSc1
death	deatha	k1gFnPc2
<g/>
:	:	kIx,
Joe	Joe	k1gFnSc1
Biden	Biden	k1gInSc1
says	says	k1gInSc1
'	'	kIx"
<g/>
NATO	NATO	kA
got	got	k?
it	it	k?
right	right	k1gInSc1
<g/>
'	'	kIx"
in	in	k?
Libya	Libya	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Who	Who	k1gFnSc1
to	ten	k3xDgNnSc4
Blame	Blam	k1gInSc5
If	If	k1gFnPc3
Arming	Arming	k1gInSc1
the	the	k?
Syrian	Syrian	k1gInSc1
Rebels	Rebels	k1gInSc1
Goes	Goesa	k1gFnPc2
Wrong	Wronga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Atlantic	Atlantice	k1gFnPc2
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CARTER	CARTER	kA
<g/>
,	,	kIx,
Chelsea	Chelse	k2eAgFnSc1d1
J.	J.	kA
<g/>
;	;	kIx,
BRUMFIELD	BRUMFIELD	kA
<g/>
,	,	kIx,
Ben	Ben	k1gInSc1
<g/>
;	;	kIx,
MAZLOUMSAKI	MAZLOUMSAKI	kA
<g/>
,	,	kIx,
Sara	Sarum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vice	vika	k1gFnSc6
President	president	k1gMnSc1
Joe	Joe	k1gMnSc1
Biden	Biden	k2eAgInSc4d1
apologises	apologises	k1gInSc4
to	ten	k3xDgNnSc1
Turkey	Turkey	k1gInPc1
<g/>
,	,	kIx,
UAE	UAE	kA
<g/>
.	.	kIx.
edition	edition	k1gInSc1
<g/>
.	.	kIx.
<g/>
cnn	cnn	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Joe	Joe	k1gFnSc1
Biden	Bidno	k1gNnPc2
Is	Is	k1gFnSc2
the	the	k?
Only	Onla	k1gFnSc2
Honest	Honest	k1gMnSc1
Man	Man	k1gMnSc1
in	in	k?
Washington	Washington	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Foreign	Foreign	k1gInSc1
Policy	Polica	k1gFnSc2
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Biden	Biden	k2eAgInSc4d1
says	says	k1gInSc4
Kurdish	Kurdisha	k1gFnPc2
PKK	PKK	kA
is	is	k?
a	a	k8xC
'	'	kIx"
<g/>
terror	terror	k1gMnSc1
group	group	k1gMnSc1
plain	plain	k1gMnSc1
and	and	k?
simple	simple	k6eAd1
<g/>
'	'	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deutsche	Deutsche	k1gFnSc1
Welle	Welle	k1gFnSc1
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Biden	Biden	k2eAgInSc4d1
says	says	k1gInSc4
Turkey	Turkea	k1gMnSc2
must	musta	k1gFnPc2
stay	staa	k1gMnSc2
out	out	k?
of	of	k?
Azerbaijan-Armenia	Azerbaijan-Armenium	k1gNnSc2
conflict	conflicta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Hill	Hill	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VOGEL	Vogel	k1gMnSc1
<g/>
,	,	kIx,
Kenneth	Kenneth	k1gMnSc1
P.	P.	kA
<g/>
;	;	kIx,
MENDEL	Mendel	k1gMnSc1
<g/>
,	,	kIx,
Iuliia	Iuliia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biden	Biden	k2eAgInSc1d1
Faces	Faces	k1gInSc1
Conflict	Conflict	k2eAgInSc1d1
of	of	k?
Interest	Interest	k1gInSc1
Questions	Questions	k1gInSc1
That	That	k2eAgInSc1d1
Are	ar	k1gInSc5
Being	Being	k1gInSc1
Promoted	Promoted	k1gInSc4
by	by	kYmCp3nS
Trump	Trump	k1gMnSc1
and	and	k?
Allies	Allies	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
362	#num#	k4
<g/>
-	-	kIx~
<g/>
4331	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Úder	úder	k1gInSc4
proti	proti	k7c3
Bidenovi	Biden	k1gMnSc3
přichází	přicházet	k5eAaImIp3nS
z	z	k7c2
Kyjeva	Kyjev	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Trump	Trump	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Ukraine	Ukrain	k1gInSc5
controversy	controversa	k1gFnSc2
cast	cast	k2eAgInSc4d1
spotlight	spotlight	k1gInSc4
on	on	k3xPp3gMnSc1
Hunter	Hunter	k1gMnSc1
Biden	Bidna	k1gFnPc2
<g/>
’	’	k?
<g/>
s	s	k7c7
business	business	k1gInSc4
dealings	dealingsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fox	fox	k1gInSc1
News	News	k1gInSc4
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Politika	politika	k1gFnSc1
Joea	Joea	k1gFnSc1
Bidena	Bidena	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Přinášíme	přinášet	k5eAaImIp1nP
vám	vy	k3xPp2nPc3
výběr	výběr	k1gInSc4
Bidenových	Bidenův	k2eAgInPc2d1
názorů	názor	k1gInPc2
na	na	k7c4
často	často	k6eAd1
diskutovaná	diskutovaný	k2eAgNnPc4d1
témata	téma	k1gNnPc4
dnešní	dnešní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biden	Biden	k2eAgInSc4d1
blasts	blasts	k1gInSc4
BDS	BDS	kA
<g/>
:	:	kIx,
Why	Why	k1gMnSc1
it	it	k?
matters	matters	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Jerusalem	Jerusalem	k1gInSc4
Post	post	k1gInSc1
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Biden	Bidna	k1gFnPc2
'	'	kIx"
<g/>
smoother	smoothra	k1gFnPc2
<g/>
'	'	kIx"
to	ten	k3xDgNnSc1
deal	deanout	k5eAaPmAgInS
with	with	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Globaltimes	Globaltimes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Edward	Edward	k1gMnSc1
Wong	Wong	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Crowley	Crowlea	k1gFnSc2
&	&	k?
Ana	Ana	k1gMnSc1
Swanson	Swanson	k1gMnSc1
<g/>
,	,	kIx,
Joe	Joe	k1gMnSc1
Biden	Biden	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
China	China	k1gFnSc1
Journey	Journea	k1gFnPc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Author	Author	k1gInSc1
Alleges	Alleges	k1gMnSc1
China	China	k1gFnSc1
Used	Used	k1gInSc4
Business	business	k1gInSc1
Deals	Deals	k1gInSc1
to	ten	k3xDgNnSc4
Influence	influence	k1gFnPc1
Families	Familiesa	k1gFnPc2
of	of	k?
Mitch	Mitch	k1gMnSc1
McConnell	McConnell	k1gMnSc1
<g/>
,	,	kIx,
Joe	Joe	k1gMnSc1
Biden	Biden	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Wall	Wall	k1gMnSc1
Street	Street	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chinese	Chinese	k1gFnSc1
Fund	fund	k1gInSc1
Backed	Backed	k1gInSc1
by	by	kYmCp3nS
Hunter	Hunter	k1gInSc4
Biden	Bidna	k1gFnPc2
Invested	Invested	k1gMnSc1
in	in	k?
Technology	technolog	k1gMnPc4
Used	Used	k1gInSc1
to	ten	k3xDgNnSc1
Surveil	Surveil	k1gMnSc1
Muslims	Muslims	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Intercept	Intercept	k1gMnSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Will	Will	k1gInSc1
Hunter	Hunter	k1gInSc1
Biden	Biden	k1gInSc4
Jeopardize	Jeopardize	k1gFnSc2
His	his	k1gNnSc2
Father	Fathra	k1gFnPc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Campaign	Campaigna	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
New	New	k1gMnSc1
Yorker	Yorker	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Grassley	Grasslea	k1gFnSc2
Raises	Raises	k1gMnSc1
Concerns	Concernsa	k1gFnPc2
Over	Over	k1gMnSc1
Obama	Obama	k?
Admin	Admin	k1gMnSc1
Approval	Approval	k1gFnSc2
of	of	k?
U.	U.	kA
<g/>
S.	S.	kA
Tech	Tech	k?
Company	Compana	k1gFnSc2
Joint	Joint	k1gMnSc1
Sale	Sal	k1gFnSc2
to	ten	k3xDgNnSc4
Chinese	Chinese	k1gFnPc1
Government	Government	k1gMnSc1
and	and	k?
Investment	Investment	k1gMnSc1
Firm	Firm	k1gMnSc1
Linked	Linked	k1gMnSc1
to	ten	k3xDgNnSc1
Biden	Biden	k1gInSc4
<g/>
,	,	kIx,
Kerry	Kerr	k1gMnPc4
Families	Familiesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
Senate	Senat	k1gInSc5
Committee	Committeus	k1gInSc5
on	on	k3xPp3gMnSc1
Finance	finance	k1gFnPc1
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Senator	Senator	k1gInSc1
wants	wantsa	k1gFnPc2
inquiry	inquira	k1gFnSc2
into	into	k1gMnSc1
whether	whethra	k1gFnPc2
‘	‘	k?
<g/>
conflict	conflict	k1gMnSc1
of	of	k?
interest	interest	k1gMnSc1
<g/>
’	’	k?
led	led	k1gInSc1
to	ten	k3xDgNnSc1
US	US	kA
approval	approvat	k5eAaBmAgInS,k5eAaPmAgInS,k5eAaImAgInS
of	of	k?
deal	deal	k1gInSc1
involving	involving	k1gInSc1
Chinese	Chinese	k1gFnSc1
state	status	k1gInSc5
company	compan	k1gMnPc4
and	and	k?
Joe	Joe	k1gFnSc1
Biden	Biden	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
son	son	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yahoo	Yahoo	k1gNnSc1
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Biden	Biden	k2eAgInSc4d1
downplays	downplays	k1gInSc4
Chinese	Chinese	k1gFnSc2
economic	economice	k1gFnPc2
competition	competition	k1gInSc1
<g/>
,	,	kIx,
drawing	drawing	k1gInSc1
criticism	criticism	k6eAd1
from	from	k6eAd1
Republicans	Republicans	k1gInSc1
and	and	k?
Sanders	Sanders	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Biden	Biden	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
:	:	kIx,
učitel	učitel	k1gMnSc1
dějepisu	dějepis	k1gInSc2
na	na	k7c4
usmiřovací	usmiřovací	k2eAgFnSc4d1
misi	mise	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
TÝDEN	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-10-23	2009-10-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Mezinárodní	mezinárodní	k2eAgInPc4d1
styky	styk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rudé	rudý	k2eAgNnSc1d1
právo	právo	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1977-08-20	1977-08-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
57	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
196	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
V	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
viceprezident	viceprezident	k1gMnSc1
USA	USA	kA
<g/>
,	,	kIx,
jednat	jednat	k5eAaImF
bude	být	k5eAaImBp3nS
v	v	k7c4
pátek	pátek	k1gInSc4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2009-10-22	2009-10-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nová	nový	k2eAgFnSc1d1
protiraketová	protiraketový	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
bude	být	k5eAaImBp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
Severoatlantické	severoatlantický	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2009-09-17	2009-09-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Česko	Česko	k1gNnSc1
se	se	k3xPyFc4
chce	chtít	k5eAaImIp3nS
podílet	podílet	k5eAaImF
na	na	k7c6
systému	systém	k1gInSc6
americké	americký	k2eAgFnSc2d1
protiraketové	protiraketový	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2009-10-23	2009-10-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Viceprezident	viceprezident	k1gMnSc1
USA	USA	kA
bude	být	k5eAaImBp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
mluvit	mluvit	k5eAaImF
o	o	k7c6
protiraketové	protiraketový	k2eAgFnSc6d1
obraně	obrana	k1gFnSc6
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2009-10-23	2009-10-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vabariigi	Vabariigi	k1gNnSc1
President	president	k1gMnSc1
<g/>
.	.	kIx.
www.president.ee	www.president.ee	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ჯ	ჯ	k?
ბ	ბ	k?
თ	თ	k?
<g/>
.	.	kIx.
რ	რ	k?
თ	თ	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
gruzínsky	gruzínsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VESTNESIS	VESTNESIS	kA
<g/>
.	.	kIx.
<g/>
LV	LV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Par	para	k1gFnPc2
Triju	Triju	k1gFnSc1
Zvaigžņ	Zvaigžņ	k1gFnSc4
ordeņ	ordeņ	k1gInSc2
piešķ	piešķ	k1gInSc2
-	-	kIx~
Latvijas	Latvijas	k1gInSc1
Vē	Vē	k1gInSc1
<g/>
.	.	kIx.
www.vestnesis.lv	www.vestnesis.lv	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
lotyšsky	lotyšsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
У	У	k?
П	П	k?
У	У	k?
№	№	k?
<g/>
250	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
.	.	kIx.
О	О	k?
і	і	k?
П	П	k?
У	У	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
ua	ua	k?
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PERKNEROVÁ	PERKNEROVÁ	kA
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hynek	Hynek	k1gMnSc1
Kmoníček	Kmoníček	k1gMnSc1
<g/>
:	:	kIx,
Biden	Biden	k1gInSc1
začne	začít	k5eAaPmIp3nS
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
s	s	k7c7
Obamou	Obama	k1gFnSc7
skončil	skončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražský	pražský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vltava	Vltava	k1gFnSc1
Labe	Labe	k1gNnSc2
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
12,13	12,13	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rubrika	rubrika	k1gFnSc1
Páteční	páteční	k2eAgFnSc2d1
rozhovor	rozhovor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
569	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vláda	vláda	k1gFnSc1
Joea	Joea	k1gFnSc1
Bidena	Bidena	k1gFnSc1
</s>
<s>
Prezident	prezident	k1gMnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Viceprezident	viceprezident	k1gMnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Barack	Barack	k1gInSc1
Obama	Obama	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Joe	Joe	k1gMnSc1
Biden	Biden	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Joe	Joe	k1gFnSc1
Biden	Biden	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Joe	Joe	k1gFnSc2
Biden	Bidna	k1gFnPc2
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
Osoba	osoba	k1gFnSc1
Joe	Joe	k1gFnSc2
Biden	Bidna	k1gFnPc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Prezidenti	prezident	k1gMnPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
George	George	k1gInSc1
Washington	Washington	k1gInSc1
(	(	kIx(
<g/>
bezp	bezp	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
F	F	kA
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Adams	Adams	k1gInSc1
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
•	•	k?
Thomas	Thomas	k1gMnSc1
Jefferson	Jefferson	k1gMnSc1
(	(	kIx(
<g/>
SR	SR	kA
<g/>
)	)	kIx)
•	•	k?
James	James	k1gMnSc1
Madison	Madison	k1gMnSc1
(	(	kIx(
<g/>
SR	SR	kA
<g/>
)	)	kIx)
•	•	k?
James	James	k1gInSc1
Monroe	Monroe	k1gFnSc1
(	(	kIx(
<g/>
SR	SR	kA
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Quincy	Quinca	k1gFnSc2
Adams	Adams	k1gInSc1
(	(	kIx(
<g/>
SR	SR	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Andrew	Andrew	k1gMnSc1
Jackson	Jackson	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
Martin	Martin	k1gInSc1
Van	van	k1gInSc1
Buren	Buren	k1gInSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
William	William	k1gInSc1
Henry	Henry	k1gMnSc1
Harrison	Harrison	k1gMnSc1
†	†	k?
(	(	kIx(
<g/>
W	W	kA
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Tyler	Tyler	k1gMnSc1
(	(	kIx(
<g/>
W	W	kA
<g/>
,	,	kIx,
bezp	bezp	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
•	•	k?
James	James	k1gMnSc1
K.	K.	kA
Polk	Polk	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Zachary	Zachara	k1gFnSc2
Taylor	Taylor	k1gMnSc1
†	†	k?
(	(	kIx(
<g/>
W	W	kA
<g/>
)	)	kIx)
•	•	k?
Millard	Millard	k1gInSc1
Fillmore	Fillmor	k1gInSc5
(	(	kIx(
<g/>
W	W	kA
<g/>
)	)	kIx)
•	•	k?
Franklin	Franklin	k1gInSc1
Pierce	Pierce	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
James	James	k1gMnSc1
Buchanan	Buchanan	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
Abraham	Abraham	k1gMnSc1
Lincoln	Lincoln	k1gMnSc1
†	†	k?
<g/>
*	*	kIx~
(	(	kIx(
<g/>
R	R	kA
<g/>
,	,	kIx,
<g/>
U	u	k7c2
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Andrew	Andrew	k1gMnSc1
Johnson	Johnson	k1gMnSc1
(	(	kIx(
<g/>
U	u	k7c2
<g/>
)	)	kIx)
•	•	k?
Ulysses	Ulysses	k1gInSc1
S.	S.	kA
Grant	grant	k1gInSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Rutherford	Rutherford	k1gMnSc1
B.	B.	kA
Hayes	Hayes	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
James	James	k1gMnSc1
A.	A.	kA
Garfield	Garfield	k1gMnSc1
†	†	k?
<g/>
*	*	kIx~
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Chester	Chester	k1gMnSc1
A.	A.	kA
Arthur	Arthur	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Grover	Grover	k1gInSc1
Cleveland	Cleveland	k1gInSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
Benjamin	Benjamin	k1gMnSc1
Harrison	Harrison	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Grover	Grover	k1gInSc1
Cleveland	Cleveland	k1gInSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
William	William	k1gInSc1
McKinley	McKinlea	k1gMnSc2
†	†	k?
<g/>
*	*	kIx~
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Theodore	Theodor	k1gMnSc5
Roosevelt	Roosevelt	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
William	William	k1gInSc1
Howard	Howard	k1gMnSc1
Taft	taft	k1gInSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Woodrow	Woodrow	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Wilson	Wilson	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
Warren	Warrna	k1gFnPc2
G.	G.	kA
Harding	Harding	k1gInSc1
†	†	k?
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Calvin	Calvina	k1gFnPc2
Coolidge	Coolidge	k1gFnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Herbert	Herbert	k1gInSc1
Clark	Clark	k1gInSc1
Hoover	Hoover	k1gInSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Franklin	Franklin	k1gInSc1
Delano	Delana	k1gFnSc5
Roosevelt	Roosevelt	k1gMnSc1
†	†	k?
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
Harry	Harra	k1gFnSc2
S.	S.	kA
Truman	Truman	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Dwight	Dwight	k1gMnSc1
D.	D.	kA
Eisenhower	Eisenhower	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Fitzgerald	Fitzgerald	k1gMnSc1
Kennedy	Kenneda	k1gMnSc2
†	†	k?
<g/>
*	*	kIx~
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
Lyndon	Lyndon	k1gMnSc1
B.	B.	kA
Johnson	Johnson	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
Richard	Richard	k1gMnSc1
Nixon	Nixon	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Gerald	Gerald	k1gInSc1
Ford	ford	k1gInSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Jimmy	Jimma	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Carter	Carter	k1gInSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
Ronald	Ronald	k1gMnSc1
Reagan	Reagan	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
George	George	k1gNnPc2
H.	H.	kA
W.	W.	kA
Bush	Bush	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Bill	Bill	k1gMnSc1
Clinton	Clinton	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
George	Georg	k1gFnSc2
W.	W.	kA
Bush	Bush	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Barack	Barack	k1gInSc1
Obama	Obama	k?
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
•	•	k?
Joe	Joe	k1gFnSc1
Biden	Biden	k1gInSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
–	–	k?
federalista	federalista	k1gMnSc1
(	(	kIx(
<g/>
SR	SR	kA
<g/>
)	)	kIx)
–	–	k?
„	„	k?
<g/>
starorepublikán	starorepublikán	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
W	W	kA
<g/>
)	)	kIx)
–	–	k?
whig	whig	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
–	–	k?
demokrat	demokrat	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
–	–	k?
republikán	republikán	k1gMnSc1
(	(	kIx(
<g/>
U	u	k7c2
<g/>
)	)	kIx)
–	–	k?
unionista	unionista	k1gMnSc1
(	(	kIx(
<g/>
bezp	bezp	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
–	–	k?
bezpartijní	bezpartijní	k1gMnSc1
†	†	k?
–	–	k?
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
úřadě	úřad	k1gInSc6
*	*	kIx~
–	–	k?
zavražděn	zavražděn	k2eAgInSc1d1
v	v	k7c6
úřadě	úřad	k1gInSc6
Seznam	seznam	k1gInSc1
prezidentů	prezident	k1gMnPc2
</s>
<s>
Viceprezidenti	viceprezident	k1gMnPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
John	John	k1gMnSc1
Adams	Adams	k1gInSc1
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
·	·	k?
Thomas	Thomas	k1gMnSc1
Jefferson	Jefferson	k1gMnSc1
(	(	kIx(
<g/>
SR	SR	kA
<g/>
)	)	kIx)
·	·	k?
Aaron	Aaron	k1gMnSc1
Burr	Burr	k1gMnSc1
(	(	kIx(
<g/>
SR	SR	kA
<g/>
)	)	kIx)
·	·	k?
George	Georg	k1gMnSc4
Clinton	Clinton	k1gMnSc1
(	(	kIx(
<g/>
SR	SR	kA
<g/>
)	)	kIx)
·	·	k?
Elbridge	Elbridg	k1gMnSc2
Gerry	Gerra	k1gMnSc2
(	(	kIx(
<g/>
SR	SR	kA
<g/>
)	)	kIx)
·	·	k?
Daniel	Daniel	k1gMnSc1
D.	D.	kA
Tompkins	Tompkins	k1gInSc1
(	(	kIx(
<g/>
SR	SR	kA
<g/>
)	)	kIx)
·	·	k?
John	John	k1gMnSc1
C.	C.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
Calhoun	Calhoun	k1gInSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Martin	Martin	k1gInSc1
Van	van	k1gInSc1
Buren	Buren	k1gInSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Richard	Richard	k1gMnSc1
Mentor	mentor	k1gMnSc1
Johnson	Johnson	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
John	John	k1gMnSc1
Tyler	Tyler	k1gMnSc1
(	(	kIx(
<g/>
W	W	kA
<g/>
)	)	kIx)
·	·	k?
George	George	k1gInSc1
M.	M.	kA
Dallas	Dallas	k1gInSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Millard	Millard	k1gInSc1
Fillmore	Fillmor	k1gInSc5
(	(	kIx(
<g/>
W	W	kA
<g/>
)	)	kIx)
·	·	k?
<g />
.	.	kIx.
</s>
<s hack="1">
William	William	k1gInSc1
R.	R.	kA
King	King	k1gInSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
John	John	k1gMnSc1
C.	C.	kA
Breckinridge	Breckinridge	k1gFnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Hannibal	Hannibal	k1gInSc1
Hamlin	Hamlin	k1gInSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Andrew	Andrew	k1gMnSc1
Johnson	Johnson	k1gMnSc1
(	(	kIx(
<g/>
U	u	k7c2
<g/>
)	)	kIx)
·	·	k?
Schuyler	Schuyler	k1gInSc1
Colfax	Colfax	k1gInSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Henry	Henry	k1gMnSc1
Wilson	Wilson	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
<g />
.	.	kIx.
</s>
<s hack="1">
William	William	k1gInSc1
A.	A.	kA
Wheeler	Wheeler	k1gInSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Chester	Chester	k1gMnSc1
A.	A.	kA
Arthur	Arthur	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Thomas	Thomas	k1gMnSc1
A.	A.	kA
Hendricks	Hendricks	k1gInSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Levi	Lev	k1gFnSc2
P.	P.	kA
Morton	Morton	k1gInSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Adlai	Adla	k1gFnSc2
E.	E.	kA
Stevenson	Stevenson	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Garret	Garret	k1gInSc1
Hobart	Hobart	k1gInSc1
(	(	kIx(
<g/>
R	R	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
·	·	k?
Theodore	Theodor	k1gMnSc5
Roosevelt	Roosevelt	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Charles	Charles	k1gMnSc1
W.	W.	kA
Fairbanks	Fairbanks	k1gInSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
James	James	k1gMnSc1
S.	S.	kA
Sherman	Sherman	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Thomas	Thomas	k1gMnSc1
R.	R.	kA
Marshall	Marshall	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Calvin	Calvina	k1gFnPc2
Coolidge	Coolidge	k1gFnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Charles	Charles	k1gMnSc1
G.	G.	kA
Dawes	Dawes	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Charles	Charles	k1gMnSc1
Curtis	Curtis	k1gFnSc2
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
John	John	k1gMnSc1
Nance	Nanka	k1gFnSc3
Garner	Garner	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Henry	Henry	k1gMnSc1
A.	A.	kA
Wallace	Wallace	k1gFnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Harry	Harra	k1gFnSc2
S.	S.	kA
Truman	Truman	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Alben	Albena	k1gFnPc2
W.	W.	kA
Barkley	Barklea	k1gFnSc2
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Richard	Richard	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Nixon	Nixon	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Lyndon	Lyndon	k1gMnSc1
B.	B.	kA
Johnson	Johnson	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Hubert	Hubert	k1gMnSc1
Humphrey	Humphrea	k1gMnSc2
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Spiro	Spiro	k1gNnSc1
Agnew	Agnew	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Gerald	Gerald	k1gInSc1
Ford	ford	k1gInSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Nelson	Nelson	k1gMnSc1
Rockefeller	Rockefeller	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Walter	Walter	k1gMnSc1
Mondale	Mondala	k1gFnSc3
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
George	George	k1gNnPc2
H.	H.	kA
W.	W.	kA
Bush	Bush	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Dan	Dan	k1gMnSc1
Quayle	Quayl	k1gInSc5
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Al	ala	k1gFnPc2
Gore	Gore	k1gFnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Dick	Dick	k1gInSc1
Cheney	Chenea	k1gFnSc2
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Joe	Joe	k1gFnSc1
Biden	Biden	k1gInSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
·	·	k?
Mike	Mike	k1gInSc1
Pence	pence	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
·	·	k?
Kamala	Kamala	k1gFnSc1
Harrisová	Harrisový	k2eAgFnSc1d1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
–	–	k?
federalista	federalista	k1gMnSc1
(	(	kIx(
<g/>
SR	SR	kA
<g/>
)	)	kIx)
–	–	k?
„	„	k?
<g/>
starorepublikán	starorepublikán	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
W	W	kA
<g/>
)	)	kIx)
–	–	k?
whig	whig	k1gMnSc1
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
–	–	k?
demokrat	demokrat	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
–	–	k?
republikán	republikán	k1gMnSc1
(	(	kIx(
<g/>
U	u	k7c2
<g/>
)	)	kIx)
–	–	k?
unionista	unionista	k1gMnSc1
</s>
<s>
Vláda	vláda	k1gFnSc1
prezidenta	prezident	k1gMnSc2
Baracka	Baracka	k1gFnSc1
Obamy	Obamo	k1gNnPc7
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Členové	člen	k1gMnPc1
vlády	vláda	k1gFnSc2
(	(	kIx(
<g/>
zastupující	zastupující	k2eAgFnSc1d1
kurzívou	kurzíva	k1gFnSc7
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
zahraničí	zahraničí	k1gNnSc2
</s>
<s>
Hillary	Hillar	k1gInPc1
Clintonová	Clintonová	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Kerry	Kerra	k1gMnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
</s>
<s>
Timothy	Timoth	k1gInPc1
Geithner	Geithnra	k1gFnPc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Neal	Neal	k1gMnSc1
Wolin	Wolin	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jack	Jack	k1gMnSc1
Lew	Lew	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
obrany	obrana	k1gFnSc2
</s>
<s>
Robert	Robert	k1gMnSc1
Gates	Gates	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leon	Leona	k1gFnPc2
Panetta	Panetta	k1gFnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chuck	Chuck	k1gMnSc1
Hagel	Hagel	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ashton	Ashton	k1gInSc1
Carter	Carter	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
spravedlnosti	spravedlnost	k1gFnSc2
</s>
<s>
Eric	Eric	k1gFnSc1
Holder	Holdra	k1gFnPc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Loretta	Loretta	k1gFnSc1
Lynchová	Lynchová	k1gFnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
</s>
<s>
Ken	Ken	k?
Salazar	Salazar	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sally	Sall	k1gInPc1
Jewellová	Jewellová	k1gFnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
zemědělství	zemědělství	k1gNnSc2
</s>
<s>
Tom	Tom	k1gMnSc1
Vilsack	Vilsack	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michael	Michael	k1gMnSc1
Scuse	Scus	k1gMnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
obchodu	obchod	k1gInSc2
</s>
<s>
Gary	Gar	k2eAgFnPc1d1
Locke	Lock	k1gFnPc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rebecca	Rebecca	k1gMnSc1
Blank	Blank	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Bryson	Bryson	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rebecca	Rebecca	k1gMnSc1
Blank	Blank	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cameron	Cameron	k1gInSc1
Kerry	Kerra	k1gMnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Penny	penny	k1gInSc1
Pritzker	Pritzker	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
práce	práce	k1gFnSc2
</s>
<s>
Hilda	Hilda	k1gFnSc1
Solisová	Solisový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Seth	Seth	k1gInSc1
Harris	Harris	k1gFnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Thomas	Thomas	k1gMnSc1
Perez	Perez	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
zdravotnictví	zdravotnictví	k1gNnSc2
</s>
<s>
Kathleen	Kathlena	k1gFnPc2
Sebeliusová	Sebeliusový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sylvia	Sylvium	k1gNnSc2
Mathews	Mathewsa	k1gFnPc2
Burwell	Burwella	k1gFnPc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
školství	školství	k1gNnSc2
</s>
<s>
Arne	Arne	k1gMnSc1
Duncan	Duncan	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
King	King	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
bytové	bytový	k2eAgFnSc2d1
výstavby	výstavba	k1gFnSc2
a	a	k8xC
rozvoje	rozvoj	k1gInSc2
měst	město	k1gNnPc2
</s>
<s>
Shaun	Shaun	k1gMnSc1
Donovan	Donovan	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Julian	Julian	k1gMnSc1
Castro	Castro	k1gNnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
dopravy	doprava	k1gFnSc2
</s>
<s>
Ray	Ray	k?
LaHood	LaHood	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Anthony	Anthon	k1gInPc1
Foxx	Foxx	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
energetiky	energetika	k1gFnSc2
</s>
<s>
Steven	Steven	k2eAgMnSc1d1
Chu	Chu	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Daniel	Daniel	k1gMnSc1
Poneman	Poneman	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ernest	Ernest	k1gMnSc1
Moniz	Moniz	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
pro	pro	k7c4
záležitosti	záležitost	k1gFnPc4
veteránů	veterán	k1gMnPc2
</s>
<s>
Eric	Eric	k1gFnSc1
Shinseki	Shinsek	k1gFnSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sloan	Sloan	k1gMnSc1
D.	D.	kA
Gibson	Gibson	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bob	Bob	k1gMnSc1
McDonald	McDonald	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ministr	ministr	k1gMnSc1
vnitřní	vnitřní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
</s>
<s>
Janet	Janeta	k1gFnPc2
Napolitanová	Napolitanový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jeh	jho	k1gNnPc2
Johnson	Johnson	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgMnPc1d1
úředníci	úředník	k1gMnPc1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
vlády	vláda	k1gFnSc2
Viceprezident	viceprezident	k1gMnSc1
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ředitel	ředitel	k1gMnSc1
Kanceláře	kancelář	k1gFnSc2
Bílého	bílý	k2eAgInSc2d1
domu	dům	k1gInSc2
</s>
<s>
Rahm	Rahm	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
William	William	k1gInSc1
Daley	Dalea	k1gMnSc2
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jack	Jack	k1gMnSc1
Lew	Lew	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Denis	Denisa	k1gFnPc2
McDonough	McDonough	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ředitel	ředitel	k1gMnSc1
Agentury	agentura	k1gFnSc2
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
</s>
<s>
Lisa	Lisa	k1gFnSc1
Jacksonová	Jacksonová	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gina	Ginus	k1gMnSc2
McCarthy	McCartha	k1gMnSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ředitel	ředitel	k1gMnSc1
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
správu	správa	k1gFnSc4
a	a	k8xC
rozpočet	rozpočet	k1gInSc4
</s>
<s>
Peter	Peter	k1gMnSc1
Orszag	Orszag	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jack	Jack	k1gMnSc1
Lew	Lew	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sylvia	Sylvium	k1gNnSc2
Mathews	Mathewsa	k1gFnPc2
Burwell	Burwella	k1gFnPc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Shaun	Shaun	k1gMnSc1
Donovan	Donovan	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Obchodní	obchodní	k2eAgMnSc1d1
zástupce	zástupce	k1gMnSc1
</s>
<s>
Ron	Ron	k1gMnSc1
Kirk	Kirk	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michael	Michael	k1gMnSc1
Froman	Froman	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Velvyslanec	velvyslanec	k1gMnSc1
při	při	k7c6
Organizaci	organizace	k1gFnSc6
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
</s>
<s>
Susan	Susan	k1gInSc1
Riceová	Riceová	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Samantha	Samantha	k1gFnSc1
Powerová	Powerová	k1gFnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Předseda	předseda	k1gMnSc1
Rady	rada	k1gFnSc2
ekonomických	ekonomický	k2eAgMnPc2d1
poradců	poradce	k1gMnPc2
</s>
<s>
Christina	Christin	k2eAgFnSc1d1
Romerová	Romerová	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Austan	Austan	k1gInSc1
Goolsbee	Goolsbee	k1gFnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alan	Alan	k1gMnSc1
Krueger	Krueger	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
Ředitel	ředitel	k1gMnSc1
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
drobné	drobný	k2eAgNnSc4d1
podnikání	podnikání	k1gNnSc4
</s>
<s>
Karen	Karen	k1gInSc1
Millsová	Millsová	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
úřadu	úřad	k1gInSc6
od	od	k7c2
r.	r.	kA
2009	#num#	k4
<g/>
,	,	kIx,
členka	členka	k1gFnSc1
kabinetu	kabinet	k1gInSc2
<g/>
:	:	kIx,
2012	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
roku	rok	k1gInSc2
časopisu	časopis	k1gInSc2
Time	Time	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rudolph	Rudolph	k1gInSc4
Giuliani	Giulian	k1gMnPc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
whistlebloweři	whistleblower	k1gMnPc1
<g/>
:	:	kIx,
Cynthia	Cynthia	k1gFnSc1
Cooperová	Cooperová	k1gFnSc1
/	/	kIx~
Coleen	Coleen	k1gInSc1
Rowleyová	Rowleyová	k1gFnSc1
/	/	kIx~
Sherron	Sherron	k1gInSc1
Watkinsová	Watkinsová	k1gFnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
americký	americký	k2eAgMnSc1d1
voják	voják	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
George	George	k1gFnSc7
W.	W.	kA
Bush	Bush	k1gMnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
dobří	dobrý	k2eAgMnPc1d1
Samaritáni	Samaritán	k1gMnPc1
<g/>
:	:	kIx,
Bono	bona	k1gFnSc5
/	/	kIx~
Bill	Bill	k1gMnSc1
Gates	Gates	k1gMnSc1
/	/	kIx~
Melinda	Melinda	k1gFnSc1
Gatesová	Gatesový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
ty	ten	k3xDgFnPc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
Vladimir	Vladimir	k1gMnSc1
Putin	putin	k2eAgMnSc1d1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
Barack	Barack	k1gInSc1
Obama	Obama	k?
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
Ben	Ben	k1gInSc1
Bernanke	Bernank	k1gFnSc2
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
Mark	Mark	k1gMnSc1
Zuckerberg	Zuckerberg	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
Protestující	protestující	k2eAgFnSc1d1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
Barack	Barack	k1gInSc1
Obama	Obama	k?
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
papež	papež	k1gMnSc1
František	František	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
bojovníci	bojovník	k1gMnPc1
proti	proti	k7c3
ebole	ebola	k1gFnSc3
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
Angela	Angela	k1gFnSc1
Merkelová	Merkelová	k1gFnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
Donald	Donald	k1gMnSc1
Trump	Trump	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
prolomili	prolomit	k5eAaPmAgMnP
mlčení	mlčený	k2eAgMnPc1d1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
Ochránci	ochránce	k1gMnPc1
<g/>
:	:	kIx,
Džamál	Džamál	k1gInSc1
Chášukdží	Chášukdž	k1gFnPc2
<g/>
,	,	kIx,
Maria	Maria	k1gFnSc1
Ressaová	Ressaová	k1gFnSc1
<g/>
,	,	kIx,
Wa	Wa	k1gFnSc1
Lone	Lone	k1gFnSc1
a	a	k8xC
Ťjo	Ťjo	k1gFnSc1
Sou	sou	k1gInSc2
U	U	kA
<g/>
,	,	kIx,
zaměstnanci	zaměstnanec	k1gMnPc1
The	The	k1gMnSc1
Capital	Capital	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
Greta	Greta	k1gFnSc1
Thunbergová	Thunbergový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
a	a	k8xC
Kamala	Kamala	k1gFnSc1
Harrisová	Harrisový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
·	·	k?
</s>
<s>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
·	·	k?
1951	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
·	·	k?
1976	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
·	·	k?
2001	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
124301649	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1444	#num#	k4
6482	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80016125	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
55438660	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80016125	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
