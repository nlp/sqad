<s>
Coupéville	Coupéville	k6eAd1
</s>
<s>
Coupéville	Coupéville	k1gFnSc1
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
37	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
°	°	k?
<g/>
37	#num#	k4
<g/>
′	′	k?
<g/>
35	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
106-214	106-214	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
region	region	k1gInSc1
</s>
<s>
Grand	grand	k1gMnSc1
Est	Est	k1gMnSc1
departement	departement	k1gInSc4
</s>
<s>
Marne	Marnout	k5eAaPmIp3nS,k5eAaImIp3nS
arrondissement	arrondissement	k1gInSc1
</s>
<s>
Châlons-en-Champagne	Châlons-en-Champagnout	k5eAaImIp3nS,k5eAaPmIp3nS
kanton	kanton	k1gInSc1
</s>
<s>
Châlons-en-Champagne-	Châlons-en-Champagne-	k?
<g/>
3	#num#	k4
</s>
<s>
Coupéville	Coupéville	k6eAd1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
30,42	30,42	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
199	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
6,5	6,5	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
PSČ	PSČ	kA
</s>
<s>
51240	#num#	k4
INSEE	INSEE	kA
</s>
<s>
51179	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Coupéville	Coupéville	k1gFnSc1
je	být	k5eAaImIp3nS
francouzská	francouzský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
v	v	k7c6
departementu	departement	k1gInSc6
Marne	Marn	k1gInSc5
v	v	k7c6
regionu	region	k1gInSc6
Grand	grand	k1gMnSc1
Est	Est	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
199	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sousední	sousední	k2eAgFnPc1d1
obce	obec	k1gFnPc1
</s>
<s>
Le	Le	k?
Fresne	Fresne	k1gFnSc1
<g/>
,	,	kIx,
Marson	Marson	k1gInSc1
<g/>
,	,	kIx,
Poix	Poix	k1gInSc1
<g/>
,	,	kIx,
Vanault-le-Châtel	Vanault-le-Châtel	k1gInSc1
<g/>
,	,	kIx,
Saint-Jean-sur-Moivre	Saint-Jean-sur-Moivr	k1gInSc5
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INSEE	INSEE	kA
<g/>
↑	↑	k?
INSEE	INSEE	kA
(	(	kIx(
<g/>
ZIP	zip	k1gInSc1
2,2	2,2	k4
MB	MB	kA
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
departementu	departement	k1gInSc6
Marne	Marn	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Coupéville	Coupéville	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Obce	obec	k1gFnSc2
kantonu	kanton	k1gInSc2
Châlons-en-Champagne-	Châlons-en-Champagne-	k1gFnSc2
<g/>
3	#num#	k4
</s>
<s>
Breuvery-sur-Coole	Breuvery-sur-Coole	k1gFnSc1
•	•	k?
Bussy-Lettrée	Bussy-Lettrée	k1gInSc1
•	•	k?
Cernon	Cernon	k1gInSc1
•	•	k?
Châlons-en-Champagne	Châlons-en-Champagn	k1gInSc5
•	•	k?
Cheniers	Cheniers	k1gInSc1
•	•	k?
Cheppes-la-Prairie	Cheppes-la-Prairie	k1gFnSc2
•	•	k?
Chepy	Chepa	k1gFnSc2
•	•	k?
Coupetz	Coupetz	k1gInSc1
•	•	k?
Coupéville	Coupéville	k1gInSc1
•	•	k?
Dampierre-sur-Moivre	Dampierre-sur-Moivr	k1gInSc5
•	•	k?
Dommartin-Lettrée	Dommartin-Lettré	k1gInPc4
•	•	k?
Écury-sur-Coole	Écury-sur-Coole	k1gFnSc2
•	•	k?
L	L	kA
<g/>
'	'	kIx"
<g/>
Épine	Épin	k1gInSc5
•	•	k?
Faux-Vésigneul	Faux-Vésigneul	k1gInSc1
•	•	k?
Francheville	Francheville	k1gFnSc2
•	•	k?
Le	Le	k1gMnSc1
Fresne	Fresne	k1gMnSc1
•	•	k?
Haussimont	Haussimont	k1gMnSc1
•	•	k?
Lenharrée	Lenharrée	k1gInSc1
•	•	k?
Mairy-sur-Marne	Mairy-sur-Marn	k1gInSc5
•	•	k?
Marson	Marson	k1gInSc1
•	•	k?
Moivre	Moivr	k1gInSc5
•	•	k?
Moncetz-Longevas	Moncetz-Longevas	k1gInSc1
•	•	k?
Montépreux	Montépreux	k1gInSc1
•	•	k?
Nuisement-sur-Coole	Nuisement-sur-Coole	k1gFnSc2
•	•	k?
Omey	Omea	k1gMnSc2
•	•	k?
Pogny	Pogna	k1gMnSc2
•	•	k?
Saint-Étienne-au-Temple	Saint-Étienne-au-Temple	k1gMnSc2
•	•	k?
Saint-Germain-la-Ville	Saint-Germain-la-Vill	k1gMnSc2
•	•	k?
Saint-Jean-sur-Moivre	Saint-Jean-sur-Moivr	k1gInSc5
•	•	k?
Saint-Martin-aux-Champs	Saint-Martin-aux-Champs	k1gInSc1
•	•	k?
Saint-Memmie	Saint-Memmie	k1gFnSc2
•	•	k?
Saint-Quentin-sur-Coole	Saint-Quentin-sur-Coole	k1gFnSc2
•	•	k?
Sarry	Sarra	k1gFnSc2
•	•	k?
Sogny-aux-Moulins	Sogny-aux-Moulins	k1gInSc1
•	•	k?
Sommesous	Sommesous	k1gInSc4
•	•	k?
Soudé	Soudý	k2eAgFnSc2d1
•	•	k?
Soudron	Soudron	k1gMnSc1
•	•	k?
Togny-aux-Bœ	Togny-aux-Bœ	k1gInSc1
•	•	k?
Vassimont-et-Chapelaine	Vassimont-et-Chapelain	k1gInSc5
•	•	k?
Vatry	vatra	k1gFnSc2
•	•	k?
Vésigneul-sur-Marne	Vésigneul-sur-Marn	k1gInSc5
•	•	k?
Vitry-la-Ville	Vitry-la-Ville	k1gNnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
