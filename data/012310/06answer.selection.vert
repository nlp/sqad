<s>
International	Internationat	k5eAaPmAgInS	Internationat
Business	business	k1gInSc1	business
Machines	Machines	k1gMnSc1	Machines
Corporation	Corporation	k1gInSc1	Corporation
(	(	kIx(	(
<g/>
IBM	IBM	kA	IBM
<g/>
)	)	kIx)	)
–	–	k?	–
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
Big	Big	k1gFnSc1	Big
Blue	Blue	k1gFnSc1	Blue
neboli	neboli	k8xC	neboli
Velká	velká	k1gFnSc1	velká
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgFnSc1d1	fungující
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1911	[number]	k4	1911
–	–	k?	–
je	být	k5eAaImIp3nS	být
přední	přední	k2eAgFnSc1d1	přední
světová	světový	k2eAgFnSc1d1	světová
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
