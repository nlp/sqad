<s>
Meloun	meloun	k1gInSc4
vodní	vodní	k2eAgInSc4d1
je	být	k5eAaImIp3nS
tykvovitý	tykvovitý	k2eAgInSc4d1
plod	plod	k1gInSc4
lubenice	lubenice	k1gFnSc2
obecné	obecná	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Citrullus	Citrullus	k1gMnSc1
lanatus	lanatus	k1gMnSc1
tento	tento	k3xDgInSc4
druh	druh	k1gInSc4
melounu	meloun	k1gInSc2
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
kultivarech	kultivar	k1gInPc6
<g/>
.	.	kIx.
</s>