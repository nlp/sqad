<s>
Vodní	vodní	k2eAgInSc1d1
meloun	meloun	k1gInSc1
</s>
<s>
Meloun	meloun	k1gInSc4
vodní	vodní	k2eAgInSc4d1
je	být	k5eAaImIp3nS
tykvovitý	tykvovitý	k2eAgInSc4d1
plod	plod	k1gInSc4
lubenice	lubenice	k1gFnSc2
obecné	obecná	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Citrullus	Citrullus	k1gMnSc1
lanatus	lanatus	k1gMnSc1
tento	tento	k3xDgInSc4
druh	druh	k1gInSc4
melounu	meloun	k1gInSc2
se	se	k3xPyFc4
pěstuje	pěstovat	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
kultivarech	kultivar	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
běžně	běžně	k6eAd1
bývá	bývat	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c4
ovoce	ovoce	k1gNnSc4
<g/>
,	,	kIx,
z	z	k7c2
pěstitelského	pěstitelský	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
plodovou	plodový	k2eAgFnSc4d1
zeleninu	zelenina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Typicky	typicky	k6eAd1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
načervenalou	načervenalý	k2eAgFnSc7d1
vodnatou	vodnatý	k2eAgFnSc7d1
dužninou	dužnina	k1gFnSc7
(	(	kIx(
<g/>
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
meloun	meloun	k1gInSc1
zralý	zralý	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
zelenou	zelený	k2eAgFnSc7d1
kůrkou	kůrka	k1gFnSc7
se	s	k7c7
světlými	světlý	k2eAgInPc7d1
a	a	k8xC
tmavými	tmavý	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
odrůdě	odrůda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vnitřku	vnitřek	k1gInSc6
dužniny	dužnina	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
rovnoměrně	rovnoměrně	k6eAd1
rozmístěná	rozmístěný	k2eAgNnPc4d1
semena	semeno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
vypadal	vypadat	k5eAaImAgInS,k5eAaPmAgInS
jinak	jinak	k6eAd1
a	a	k8xC
dnešní	dnešní	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
je	být	k5eAaImIp3nS
výsledkem	výsledek	k1gInSc7
šlechtění	šlechtění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
dosahovat	dosahovat	k5eAaImF
až	až	k9
váhy	váha	k1gFnPc4
okolo	okolo	k7c2
15	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
ale	ale	k8xC
uvádí	uvádět	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
nejlepší	dobrý	k2eAgInPc1d3
plody	plod	k1gInPc1
ke	k	k7c3
konzumaci	konzumace	k1gFnSc3
mají	mít	k5eAaImIp3nP
hmotnost	hmotnost	k1gFnSc4
okolo	okolo	k7c2
2	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nutriční	nutriční	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
</s>
<s>
Vodní	vodní	k2eAgInPc1d1
melouny	meloun	k1gInPc1
obsahují	obsahovat	k5eAaImIp3nP
v	v	k7c6
průměru	průměr	k1gInSc6
91	#num#	k4
%	%	kIx~
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	on	k3xPp3gNnSc4
činí	činit	k5eAaImIp3nS
vítaným	vítané	k1gNnSc7
zavodňujícími	zavodňující	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
pro	pro	k7c4
organismus	organismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjma	vyjma	k7c2
vody	voda	k1gFnSc2
obsahují	obsahovat	k5eAaImIp3nP
především	především	k9
cukr	cukr	k1gInSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
fruktózy	fruktóza	k1gFnSc2
(	(	kIx(
<g/>
6	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nutriční	nutriční	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
melounů	meloun	k1gInPc2
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
jen	jen	k9
minimum	minimum	k1gNnSc4
tuků	tuk	k1gInPc2
<g/>
,	,	kIx,
bílkovin	bílkovina	k1gFnPc2
<g/>
,	,	kIx,
vlákniny	vláknina	k1gFnSc2
a	a	k8xC
minerálů	minerál	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
jsou	být	k5eAaImIp3nP
nicméně	nicméně	k8xC
určitým	určitý	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
vitamínu	vitamín	k1gInSc2
C	C	kA
(	(	kIx(
<g/>
ve	v	k7c6
100	#num#	k4
gramech	gram	k1gInPc6
10	#num#	k4
%	%	kIx~
doporučené	doporučený	k2eAgFnSc2d1
denní	denní	k2eAgFnSc2d1
dávky	dávka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pěstování	pěstování	k1gNnSc1
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS
se	se	k3xPyFc4
hlavně	hlavně	k9
v	v	k7c6
teplejších	teplý	k2eAgFnPc6d2
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Amerika	Amerika	k1gFnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Asie	Asie	k1gFnSc1
a	a	k8xC
Evropa	Evropa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
ČR	ČR	kA
se	se	k3xPyFc4
dováží	dovážet	k5eAaImIp3nP,k5eAaPmIp3nP
většinou	většina	k1gFnSc7
z	z	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
Španělska	Španělsko	k1gNnSc2
<g/>
,	,	kIx,
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
Kypru	Kypr	k1gInSc2
a	a	k8xC
Izraele	Izrael	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
</s>
<s>
Původem	původ	k1gInSc7
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
oblastí	oblast	k1gFnPc2
Afriky	Afrika	k1gFnSc2
(	(	kIx(
<g/>
z	z	k7c2
Botswany	Botswana	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
hojně	hojně	k6eAd1
pěstován	pěstován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Nejtěžší	těžký	k2eAgInSc1d3
známý	známý	k2eAgInSc1d1
meloun	meloun	k1gInSc1
na	na	k7c6
světě	svět	k1gInSc6
vážil	vážit	k5eAaImAgMnS
159	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
melouny	meloun	k1gInPc1
okolo	okolo	k7c2
100	#num#	k4
kg	kg	kA
nejsou	být	k5eNaImIp3nP
výjimkou	výjimka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
meloun	meloun	k1gInSc1
vodní	vodní	k2eAgInSc1d1
<g/>
,	,	kIx,
lubenice	lubenice	k1gFnSc1
obecná	obecná	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
květin	květina	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://abecedazahrady.dama.cz/clanek/je-meloun-ovoce-nebo-zelenina-plus-jak-poznat-ze-je-opravdu-zraly	https://abecedazahrady.dama.cz/clanek/je-meloun-ovoce-nebo-zelenina-plus-jak-poznat-ze-je-opravdu-zrat	k5eAaPmAgFnP,k5eAaImAgFnP
<g/>
↑	↑	k?
Meloun	meloun	k1gInSc1
-	-	kIx~
Vodní	vodní	k2eAgInSc1d1
(	(	kIx(
<g/>
Citrullus	Citrullus	k1gInSc1
lanatus	lanatus	k1gInSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vegetarian	Vegetarian	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Watermelon	Watermelon	k1gInSc1
<g/>
,	,	kIx,
raw	raw	k?
Nutrition	Nutrition	k1gInSc1
Facts	Facts	k1gInSc1
&	&	k?
Calories	Calories	k1gInSc1
<g/>
.	.	kIx.
nutritiondata	nutritiondata	k1gFnSc1
<g/>
.	.	kIx.
<g/>
self	self	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Heaviest	Heaviest	k1gInSc1
watermelon	watermelon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guinness	Guinnessa	k1gFnPc2
World	Worlda	k1gFnPc2
Records	Records	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
vodní	vodní	k2eAgInSc1d1
meloun	meloun	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
vodní	vodní	k2eAgInSc4d1
meloun	meloun	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Zelenina	zelenina	k1gFnSc1
cibulová	cibulový	k2eAgFnSc1d1
</s>
<s>
cibule	cibule	k1gFnSc1
•	•	k?
česnek	česnek	k1gInSc1
•	•	k?
pažitka	pažitka	k1gFnSc1
•	•	k?
pórek	pórek	k1gInSc1
kořenová	kořenový	k2eAgNnPc4d1
</s>
<s>
celer	celer	k1gInSc1
•	•	k?
černý	černý	k2eAgInSc1d1
kořen	kořen	k1gInSc1
•	•	k?
křen	křen	k1gInSc1
•	•	k?
mrkev	mrkev	k1gFnSc1
•	•	k?
pastinák	pastinák	k1gInSc1
•	•	k?
petržel	petržel	k1gFnSc1
•	•	k?
ředkev	ředkev	k1gFnSc1
•	•	k?
ředkvička	ředkvička	k1gFnSc1
•	•	k?
řepa	řepa	k1gFnSc1
•	•	k?
tuřín	tuřín	k1gInSc1
•	•	k?
vodnice	vodnice	k1gFnSc1
hlíznatá	hlíznatý	k2eAgFnSc1d1
</s>
<s>
batáty	batáty	k1gInPc1
•	•	k?
brambory	brambora	k1gFnSc2
•	•	k?
maniok	maniok	k1gInSc1
•	•	k?
topinambur	topinambur	k1gInSc1
</s>
<s>
košťálová	košťálový	k2eAgFnSc1d1
</s>
<s>
artyčok	artyčok	k1gInSc1
•	•	k?
brokolice	brokolice	k1gFnSc2
•	•	k?
hlávková	hlávkový	k2eAgFnSc1d1
kapusta	kapusta	k1gFnSc1
•	•	k?
kedluben	kedluben	k1gInSc1
•	•	k?
květák	květák	k1gInSc1
•	•	k?
romanesco	romanesco	k6eAd1
•	•	k?
růžičková	růžičkový	k2eAgFnSc1d1
kapusta	kapusta	k1gFnSc1
•	•	k?
zelí	zelí	k1gNnSc1
(	(	kIx(
<g/>
čínské	čínský	k2eAgFnPc1d1
•	•	k?
hlávkové	hlávkový	k2eAgFnPc1d1
•	•	k?
pekingské	pekingský	k2eAgFnPc1d1
<g/>
)	)	kIx)
listová	listový	k2eAgFnSc1d1
</s>
<s>
celerová	celerový	k2eAgFnSc1d1
nať	nať	k1gFnSc1
•	•	k?
čekanka	čekanka	k1gFnSc1
•	•	k?
hlávkový	hlávkový	k2eAgInSc4d1
salát	salát	k1gInSc4
•	•	k?
chřest	chřest	k1gInSc1
•	•	k?
mangold	mangold	k1gInSc1
•	•	k?
petrželová	petrželový	k2eAgFnSc1d1
nať	nať	k1gFnSc1
•	•	k?
rebarbora	rebarbora	k1gFnSc1
•	•	k?
roketa	roket	k2eAgFnSc1d1
•	•	k?
řeřicha	řeřicha	k1gFnSc1
•	•	k?
římský	římský	k2eAgInSc1d1
salát	salát	k1gInSc1
•	•	k?
špenát	špenát	k1gInSc1
lusková	luskový	k2eAgNnPc5d1
</s>
<s>
bob	bob	k1gInSc1
•	•	k?
cizrna	cizrna	k1gFnSc1
•	•	k?
čočka	čočka	k1gFnSc1
•	•	k?
fazol	fazol	k1gInSc1
•	•	k?
hrách	hrách	k1gInSc1
•	•	k?
sója	sója	k1gFnSc1
plodová	plodový	k2eAgFnSc1d1
</s>
<s>
baklažán	baklažán	k1gInSc1
•	•	k?
cuketa	cuketa	k1gFnSc1
•	•	k?
dýně	dýně	k1gFnSc2
•	•	k?
kiwano	kiwana	k1gFnSc5
•	•	k?
meloun	meloun	k1gInSc1
•	•	k?
okurka	okurka	k1gFnSc1
•	•	k?
paprika	paprika	k1gMnSc1
•	•	k?
patizon	patizon	k1gMnSc1
•	•	k?
rajče	rajče	k1gNnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Rostliny	rostlina	k1gFnPc1
|	|	kIx~
Gastronomie	gastronomie	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4189227-6	4189227-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85145743	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85145743	#num#	k4
</s>
