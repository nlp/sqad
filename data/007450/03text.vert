<s>
Hausbót	hausbót	k1gInSc1	hausbót
(	(	kIx(	(
<g/>
z	z	k7c2	z
německého	německý	k2eAgMnSc2d1	německý
Hausboot	Hausboot	k1gInSc4	Hausboot
–	–	k?	–
Haus	Haus	k1gInSc1	Haus
=	=	kIx~	=
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
Boot	Boot	k1gInSc1	Boot
=	=	kIx~	=
loď	loď	k1gFnSc1	loď
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obydlí	obydlí	k1gNnSc1	obydlí
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
<g/>
.	.	kIx.	.
</s>
<s>
Hausbót	hausbót	k1gInSc1	hausbót
není	být	k5eNaImIp3nS	být
většinou	většina	k1gFnSc7	většina
vybaven	vybavit	k5eAaPmNgInS	vybavit
vlastním	vlastní	k2eAgInSc7d1	vlastní
pohonem	pohon	k1gInSc7	pohon
<g/>
,	,	kIx,	,
kotví	kotvit	k5eAaImIp3nS	kotvit
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
či	či	k8xC	či
přehradách	přehrada	k1gFnPc6	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
místy	místo	k1gNnPc7	místo
kotvení	kotvení	k1gNnSc1	kotvení
se	se	k3xPyFc4	se
dopravuje	dopravovat	k5eAaImIp3nS	dopravovat
tažením	tažení	k1gNnSc7	tažení
za	za	k7c7	za
vlečnou	vlečný	k2eAgFnSc7d1	vlečná
lodí	loď	k1gFnSc7	loď
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
typy	typa	k1gFnSc2	typa
hausbótů	hausbót	k1gInPc2	hausbót
tlačením	tlačení	k1gNnSc7	tlačení
remorkérem	remorkér	k1gInSc7	remorkér
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
hausbóty	hausbót	k1gInPc1	hausbót
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
jako	jako	k9	jako
restaurace	restaurace	k1gFnPc4	restaurace
nebo	nebo	k8xC	nebo
hotely	hotel	k1gInPc4	hotel
(	(	kIx(	(
<g/>
těm	ten	k3xDgMnPc3	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
říká	říkat	k5eAaImIp3nS	říkat
botely	botel	k1gInPc4	botel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Karavan	karavan	k1gInSc1	karavan
Maringotka	maringotka	k1gFnSc1	maringotka
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hausbót	hausbót	k1gInSc4	hausbót
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Hausboty	Hausbota	k1gFnSc2	Hausbota
–	–	k?	–
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
hausbóty	hausbót	k1gInPc7	hausbót
a	a	k8xC	a
obytnými	obytný	k2eAgFnPc7d1	obytná
motorovými	motorový	k2eAgFnPc7d1	motorová
loděmi	loď	k1gFnPc7	loď
</s>
