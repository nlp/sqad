<p>
<s>
Neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
astronomické	astronomický	k2eAgNnSc1d1	astronomické
těleso	těleso	k1gNnSc1	těleso
tvořené	tvořený	k2eAgNnSc1d1	tvořené
převážně	převážně	k6eAd1	převážně
neutrony	neutron	k1gInPc7	neutron
udržovanými	udržovaný	k2eAgInPc7d1	udržovaný
pohromadě	pohromadě	k6eAd1	pohromadě
gravitační	gravitační	k2eAgFnSc7d1	gravitační
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
závěrečným	závěrečný	k2eAgNnSc7d1	závěrečné
stádiem	stádium	k1gNnSc7	stádium
vývoje	vývoj	k1gInSc2	vývoj
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
supernovy	supernova	k1gFnSc2	supernova
typu	typ	k1gInSc2	typ
Ib	Ib	k1gFnSc2	Ib
<g/>
,	,	kIx,	,
Ic	Ic	k1gFnSc2	Ic
nebo	nebo	k8xC	nebo
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Neutronové	neutronový	k2eAgFnPc1d1	neutronová
hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
hvězd	hvězda	k1gFnPc2	hvězda
jako	jako	k8xC	jako
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
právě	právě	k9	právě
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
plazmatu	plazma	k1gNnSc2	plazma
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
jádru	jádro	k1gNnSc6	jádro
probíhají	probíhat	k5eAaImIp3nP	probíhat
termonukleární	termonukleární	k2eAgFnPc4d1	termonukleární
reakce	reakce	k1gFnPc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Gravitace	gravitace	k1gFnSc1	gravitace
je	být	k5eAaImIp3nS	být
kompenzována	kompenzovat	k5eAaBmNgFnS	kompenzovat
tlakem	tlak	k1gInSc7	tlak
plazmatu	plazma	k1gNnSc2	plazma
při	při	k7c6	při
vysoké	vysoký	k2eAgFnSc6d1	vysoká
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
během	během	k7c2	během
vzniku	vznik	k1gInSc2	vznik
neutronové	neutronový	k2eAgFnSc2d1	neutronová
hvězdy	hvězda	k1gFnSc2	hvězda
jsou	být	k5eAaImIp3nP	být
pod	pod	k7c7	pod
velkým	velký	k2eAgInSc7d1	velký
tlakem	tlak	k1gInSc7	tlak
elektrony	elektron	k1gInPc4	elektron
vmáčknuty	vmáčknut	k2eAgMnPc4d1	vmáčknut
do	do	k7c2	do
jader	jádro	k1gNnPc2	jádro
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
protony	proton	k1gInPc7	proton
v	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
neutrony	neutron	k1gInPc4	neutron
(	(	kIx(	(
<g/>
za	za	k7c4	za
vyzáření	vyzáření	k1gNnSc4	vyzáření
příslušného	příslušný	k2eAgInSc2d1	příslušný
počtu	počet	k1gInSc2	počet
neutrin	neutrino	k1gNnPc2	neutrino
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
tzv.	tzv.	kA	tzv.
neutronový	neutronový	k2eAgInSc1d1	neutronový
degenerovaný	degenerovaný	k2eAgInSc1d1	degenerovaný
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
neutronizace	neutronizace	k1gFnSc1	neutronizace
<g/>
.	.	kIx.	.
</s>
<s>
Neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
samých	samý	k3xTgMnPc2	samý
neutronů	neutron	k1gInPc2	neutron
a	a	k8xC	a
gravitace	gravitace	k1gFnSc1	gravitace
je	být	k5eAaImIp3nS	být
kompenzována	kompenzovat	k5eAaBmNgFnS	kompenzovat
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c4	v
Pauliho	Pauli	k1gMnSc4	Pauli
vylučovacím	vylučovací	k2eAgInSc6d1	vylučovací
principu	princip	k1gInSc6	princip
(	(	kIx(	(
<g/>
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
"	"	kIx"	"
<g/>
nechuť	nechuť	k1gFnSc1	nechuť
<g/>
"	"	kIx"	"
částic	částice	k1gFnPc2	částice
jako	jako	k8xC	jako
neutrony	neutron	k1gInPc4	neutron
(	(	kIx(	(
<g/>
obecněji	obecně	k6eAd2	obecně
fermionů	fermion	k1gInPc2	fermion
<g/>
)	)	kIx)	)
sdílet	sdílet	k5eAaImF	sdílet
stejný	stejný	k2eAgInSc4d1	stejný
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
bránící	bránící	k2eAgNnSc1d1	bránící
dalšímu	další	k2eAgNnSc3d1	další
smršťování	smršťování	k1gNnSc3	smršťování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hmotnost	hmotnost	k1gFnSc1	hmotnost
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
===	===	k?	===
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
typických	typický	k2eAgFnPc2d1	typická
neutronových	neutronový	k2eAgFnPc2d1	neutronová
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
1,35	[number]	k4	1,35
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hmot	hmota	k1gFnPc2	hmota
do	do	k7c2	do
2,1	[number]	k4	2,1
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hmot	hmota	k1gFnPc2	hmota
(	(	kIx(	(
<g/>
teoreticky	teoreticky	k6eAd1	teoreticky
až	až	k9	až
3-5	[number]	k4	3-5
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Tolman-Oppenheimer-Volkoffova	Tolman-Oppenheimer-Volkoffův	k2eAgFnSc1d1	Tolman-Oppenheimer-Volkoffův
mez	mez	k1gFnSc1	mez
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
mez	mez	k1gFnSc4	mez
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
složené	složený	k2eAgNnSc1d1	složené
z	z	k7c2	z
degenerovaného	degenerovaný	k2eAgInSc2d1	degenerovaný
neutronového	neutronový	k2eAgInSc2d1	neutronový
plynu	plyn	k1gInSc2	plyn
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
na	na	k7c4	na
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c4	na
kvarkovou	kvarkový	k2eAgFnSc4d1	kvarková
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozměry	rozměr	k1gInPc1	rozměr
neutronové	neutronový	k2eAgFnSc2d1	neutronová
hvězdy	hvězda	k1gFnSc2	hvězda
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
jen	jen	k9	jen
mezi	mezi	k7c7	mezi
10	[number]	k4	10
a	a	k8xC	a
20	[number]	k4	20
km	km	kA	km
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jako	jako	k8xC	jako
Deimos	Deimos	k1gInSc4	Deimos
(	(	kIx(	(
<g/>
měsíc	měsíc	k1gInSc4	měsíc
<g/>
))	))	k?	))
<g/>
;	;	kIx,	;
neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
hmotností	hmotnost	k1gFnSc7	hmotnost
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgInSc4d2	menší
poloměr	poloměr	k1gInSc4	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
hustotám	hustota	k1gFnPc3	hustota
8	[number]	k4	8
<g/>
×	×	k?	×
<g/>
1013	[number]	k4	1013
až	až	k9	až
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
1015	[number]	k4	1015
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
za	za	k7c2	za
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
látka	látka	k1gFnSc1	látka
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
degenerovaný	degenerovaný	k2eAgInSc1d1	degenerovaný
neutronový	neutronový	k2eAgInSc1d1	neutronový
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Intenzivní	intenzivní	k2eAgNnSc1d1	intenzivní
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
neutronové	neutronový	k2eAgFnSc2d1	neutronová
hvězdy	hvězda	k1gFnSc2	hvězda
dokáže	dokázat	k5eAaPmIp3nS	dokázat
přitáhnout	přitáhnout	k5eAaPmF	přitáhnout
vše	všechen	k3xTgNnSc4	všechen
hmotné	hmotný	k2eAgNnSc4d1	hmotné
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
gravitačnímu	gravitační	k2eAgNnSc3d1	gravitační
centru	centrum	k1gNnSc3	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Případné	případný	k2eAgFnPc1d1	případná
srážky	srážka	k1gFnPc1	srážka
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
vesmírnými	vesmírný	k2eAgInPc7d1	vesmírný
tělesy	těleso	k1gNnPc7	těleso
jsou	být	k5eAaImIp3nP	být
doprovázeny	doprovázet	k5eAaImNgInP	doprovázet
uvolněním	uvolnění	k1gNnSc7	uvolnění
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
indikovaným	indikovaný	k2eAgInSc7d1	indikovaný
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
emisí	emise	k1gFnSc7	emise
záření	záření	k1gNnSc2	záření
gama	gama	k1gNnSc2	gama
–	–	k?	–
gama	gama	k1gNnSc1	gama
záblesk	záblesk	k1gInSc4	záblesk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
hvězd	hvězda	k1gFnPc2	hvězda
má	mít	k5eAaImIp3nS	mít
magnetická	magnetický	k2eAgFnSc1d1	magnetická
pole	pole	k1gFnSc1	pole
zamrzlá	zamrzlý	k2eAgFnSc1d1	zamrzlá
v	v	k7c6	v
hvězdném	hvězdný	k2eAgNnSc6d1	Hvězdné
plazmatu	plazma	k1gNnSc6	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zhroucení	zhroucení	k1gNnSc6	zhroucení
hvězdy	hvězda	k1gFnSc2	hvězda
např.	např.	kA	např.
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
2,5	[number]	k4	2,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
do	do	k7c2	do
neutronové	neutronový	k2eAgFnSc2d1	neutronová
hvězdy	hvězda	k1gFnSc2	hvězda
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
25	[number]	k4	25
km	km	kA	km
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
průměr	průměr	k1gInSc1	průměr
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
o	o	k7c4	o
několik	několik	k4yIc4	několik
řádů	řád	k1gInPc2	řád
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
i	i	k9	i
objem	objem	k1gInSc4	objem
a	a	k8xC	a
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
hustota	hustota	k1gFnSc1	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Siločáry	siločára	k1gFnPc1	siločára
mag	mag	k?	mag
<g/>
.	.	kIx.	.
pole	pole	k1gFnPc1	pole
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
všechny	všechen	k3xTgFnPc1	všechen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
hustota	hustota	k1gFnSc1	hustota
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
také	také	k9	také
intenzita	intenzita	k1gFnSc1	intenzita
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
obyčejné	obyčejný	k2eAgFnSc2d1	obyčejná
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
setin	setina	k1gFnPc2	setina
tesla	tesla	k1gFnSc1	tesla
<g/>
,	,	kIx,	,
na	na	k7c6	na
neutronových	neutronový	k2eAgFnPc6d1	neutronová
hvězdách	hvězda	k1gFnPc6	hvězda
jsou	být	k5eAaImIp3nP	být
magnetická	magnetický	k2eAgNnPc1d1	magnetické
pole	pole	k1gNnPc1	pole
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgNnPc1d1	silné
<g/>
,	,	kIx,	,
od	od	k7c2	od
106	[number]	k4	106
až	až	k9	až
do	do	k7c2	do
109	[number]	k4	109
T.	T.	kA	T.
A	a	k8xC	a
právě	právě	k9	právě
tato	tento	k3xDgNnPc1	tento
silná	silný	k2eAgNnPc1d1	silné
magnetická	magnetický	k2eAgNnPc1d1	magnetické
pole	pole	k1gNnPc1	pole
dělají	dělat	k5eAaImIp3nP	dělat
z	z	k7c2	z
neutronových	neutronový	k2eAgFnPc2d1	neutronová
hvězd	hvězda	k1gFnPc2	hvězda
pulsary	pulsar	k1gInPc4	pulsar
<g/>
,	,	kIx,	,
zdroje	zdroj	k1gInPc4	zdroj
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
opakujících	opakující	k2eAgInPc2d1	opakující
záblesků	záblesk	k1gInPc2	záblesk
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
pulsary	pulsar	k1gInPc1	pulsar
jsou	být	k5eAaImIp3nP	být
neutronové	neutronový	k2eAgFnPc4d1	neutronová
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinu	většina	k1gFnSc4	většina
neutronových	neutronový	k2eAgFnPc2d1	neutronová
hvězd	hvězda	k1gFnPc2	hvězda
jako	jako	k8xC	jako
pulsary	pulsar	k1gInPc4	pulsar
nepozorujeme	pozorovat	k5eNaImIp1nP	pozorovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pulzy	pulz	k1gInPc1	pulz
jejich	jejich	k3xOp3gNnSc2	jejich
záření	záření	k1gNnSc2	záření
míjejí	míjet	k5eAaImIp3nP	míjet
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozorování	pozorování	k1gNnSc4	pozorování
neutronových	neutronový	k2eAgFnPc2d1	neutronová
hvězd	hvězda	k1gFnPc2	hvězda
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
málo	málo	k4c1	málo
pozorovaných	pozorovaný	k2eAgFnPc2d1	pozorovaná
osamocených	osamocený	k2eAgFnPc2d1	osamocená
neutronových	neutronový	k2eAgFnPc2d1	neutronová
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
pozorování	pozorování	k1gNnSc1	pozorování
osamocené	osamocený	k2eAgFnSc2d1	osamocená
neutronové	neutronový	k2eAgFnSc2d1	neutronová
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
nesnadné	snadný	k2eNgNnSc1d1	nesnadné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
slabě	slabě	k6eAd1	slabě
zářící	zářící	k2eAgInPc4d1	zářící
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gInSc1	jejich
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vysoké	vysoký	k2eAgFnSc3d1	vysoká
povrchové	povrchový	k2eAgFnSc3d1	povrchová
teplotě	teplota	k1gFnSc3	teplota
vysílají	vysílat	k5eAaImIp3nP	vysílat
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
a	a	k8xC	a
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Častěji	často	k6eAd2	často
neutronové	neutronový	k2eAgFnPc4d1	neutronová
hvězdy	hvězda	k1gFnPc4	hvězda
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
jako	jako	k9	jako
neutronové	neutronový	k2eAgFnPc4d1	neutronová
dvojhvězdy	dvojhvězda	k1gFnPc4	dvojhvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neutronové	neutronový	k2eAgFnPc1d1	neutronová
hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
pozorované	pozorovaný	k2eAgInPc1d1	pozorovaný
jako	jako	k8xC	jako
pulsary	pulsar	k1gInPc1	pulsar
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
jako	jako	k9	jako
magnetary	magnetara	k1gFnSc2	magnetara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Struktura	struktura	k1gFnSc1	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
současných	současný	k2eAgInPc2d1	současný
matematických	matematický	k2eAgInPc2d1	matematický
modelů	model	k1gInPc2	model
soudíme	soudit	k5eAaImIp1nP	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc4	povrch
neutronové	neutronový	k2eAgFnSc2d1	neutronová
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
klasických	klasický	k2eAgNnPc2d1	klasické
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
a	a	k8xC	a
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
"	"	kIx"	"
neutronové	neutronový	k2eAgFnSc2d1	neutronová
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
1	[number]	k4	1
m	m	kA	m
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
je	být	k5eAaImIp3nS	být
pevná	pevný	k2eAgFnSc1d1	pevná
"	"	kIx"	"
<g/>
kůra	kůra	k1gFnSc1	kůra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hlouběji	hluboko	k6eAd2	hluboko
jsou	být	k5eAaImIp3nP	být
atomy	atom	k1gInPc1	atom
stále	stále	k6eAd1	stále
těžší	těžký	k2eAgMnSc1d2	těžší
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
neutrony	neutron	k1gInPc1	neutron
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
izotopy	izotop	k1gInPc1	izotop
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
v	v	k7c6	v
pozemských	pozemský	k2eAgFnPc6d1	pozemská
podmínkách	podmínka	k1gFnPc6	podmínka
dávno	dávno	k6eAd1	dávno
rozpadly	rozpadnout	k5eAaPmAgInP	rozpadnout
<g/>
,	,	kIx,	,
za	za	k7c2	za
takto	takto	k6eAd1	takto
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlouběji	hluboko	k6eAd2	hluboko
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
tzv.	tzv.	kA	tzv.
neutronová	neutronový	k2eAgFnSc1d1	neutronová
mez	mez	k1gFnSc1	mez
přesycenosti	přesycenost	k1gFnSc2	přesycenost
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
neutron	neutron	k1gInSc1	neutron
drip	drip	k1gInSc1	drip
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
mez	mez	k1gFnSc1	mez
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
jak	jak	k6eAd1	jak
samotné	samotný	k2eAgInPc1d1	samotný
volné	volný	k2eAgInPc1d1	volný
neutrony	neutron	k1gInPc1	neutron
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
atomová	atomový	k2eAgNnPc1d1	atomové
jádra	jádro	k1gNnPc1	jádro
s	s	k7c7	s
elektrony	elektron	k1gInPc7	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
jader	jádro	k1gNnPc2	jádro
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
ubývá	ubývat	k5eAaImIp3nS	ubývat
a	a	k8xC	a
přibývají	přibývat	k5eAaImIp3nP	přibývat
volné	volný	k2eAgInPc1d1	volný
neutrony	neutron	k1gInPc1	neutron
–	–	k?	–
v	v	k7c6	v
dostatečné	dostatečný	k2eAgFnSc6d1	dostatečná
hloubce	hloubka	k1gFnSc6	hloubka
již	již	k6eAd1	již
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
jen	jen	k9	jen
neutrony	neutron	k1gInPc1	neutron
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
podstata	podstata	k1gFnSc1	podstata
superhusté	superhustý	k2eAgFnSc2d1	superhustá
hmoty	hmota	k1gFnSc2	hmota
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
není	být	k5eNaImIp3nS	být
ještě	ještě	k9	ještě
přesně	přesně	k6eAd1	přesně
známá	známý	k2eAgFnSc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
neutronový	neutronový	k2eAgInSc1d1	neutronový
degenerovaný	degenerovaný	k2eAgInSc1d1	degenerovaný
plyn	plyn	k1gInSc1	plyn
-	-	kIx~	-
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
<g/>
)	)	kIx)	)
supratekutou	supratekutý	k2eAgFnSc4d1	supratekutá
směs	směs	k1gFnSc4	směs
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
malého	malý	k2eAgInSc2d1	malý
podílu	podíl	k1gInSc2	podíl
<g/>
)	)	kIx)	)
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
elektronů	elektron	k1gInPc2	elektron
a	a	k8xC	a
(	(	kIx(	(
<g/>
s	s	k7c7	s
přibývající	přibývající	k2eAgFnSc7d1	přibývající
hustotou	hustota	k1gFnSc7	hustota
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zastoupených	zastoupený	k2eAgInPc2d1	zastoupený
<g/>
)	)	kIx)	)
pionů	pion	k1gInPc2	pion
<g/>
,	,	kIx,	,
kaonů	kaon	k1gInPc2	kaon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
podivné	podivný	k2eAgFnSc2d1	podivná
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
orig	orig	k1gInSc1	orig
<g/>
.	.	kIx.	.
strange	strangat	k5eAaPmIp3nS	strangat
matter	matter	k1gInSc1	matter
<g/>
,	,	kIx,	,
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
od	od	k7c2	od
kvantové	kvantový	k2eAgFnSc2d1	kvantová
vlastnosti	vlastnost	k1gFnSc2	vlastnost
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
jako	jako	k8xS	jako
podivnost	podivnost	k1gFnSc4	podivnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
směs	směs	k1gFnSc4	směs
kvarků	kvark	k1gInPc2	kvark
těžších	těžký	k2eAgInPc2d2	těžší
než	než	k8xS	než
up	up	k?	up
a	a	k8xC	a
down	down	k1gInSc1	down
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
by	by	kYmCp3nP	by
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
hmotu	hmota	k1gFnSc4	hmota
složenou	složený	k2eAgFnSc4d1	složená
z	z	k7c2	z
kvarků	kvark	k1gInPc2	kvark
nevázaných	vázaný	k2eNgInPc2d1	nevázaný
do	do	k7c2	do
hadronů	hadron	k1gInPc2	hadron
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
v	v	k7c6	v
centrech	centr	k1gInPc6	centr
neutronových	neutronový	k2eAgFnPc2d1	neutronová
hvězd	hvězda	k1gFnPc2	hvězda
mohla	moct	k5eAaImAgFnS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
QCD	QCD	kA	QCD
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
jiné	jiný	k2eAgInPc1d1	jiný
hypotetické	hypotetický	k2eAgInPc1d1	hypotetický
materiály	materiál	k1gInPc1	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Chandrasekharova	Chandrasekharův	k2eAgFnSc1d1	Chandrasekharova
mez	mez	k1gFnSc1	mez
<g/>
,	,	kIx,	,
Tolman-Oppenheimer-Volkoffova	Tolman-Oppenheimer-Volkoffův	k2eAgFnSc1d1	Tolman-Oppenheimer-Volkoffův
mez	mez	k1gFnSc1	mez
</s>
</p>
<p>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
,	,	kIx,	,
elektronový	elektronový	k2eAgInSc1d1	elektronový
degenerovaný	degenerovaný	k2eAgInSc1d1	degenerovaný
plyn	plyn	k1gInSc1	plyn
</s>
</p>
<p>
<s>
Neutron	neutron	k1gInSc1	neutron
<g/>
,	,	kIx,	,
neutronový	neutronový	k2eAgInSc1d1	neutronový
degenerovaný	degenerovaný	k2eAgInSc1d1	degenerovaný
plyn	plyn	k1gInSc1	plyn
</s>
</p>
<p>
<s>
Kvarková	Kvarkový	k2eAgFnSc1d1	Kvarkový
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
kvarková	kvarkový	k2eAgFnSc1d1	kvarková
degenerovaná	degenerovaný	k2eAgFnSc1d1	degenerovaná
hmota	hmota	k1gFnSc1	hmota
</s>
</p>
<p>
<s>
Preonová	Preonový	k2eAgFnSc1d1	Preonový
hvězda	hvězda	k1gFnSc1	hvězda
</s>
</p>
<p>
<s>
Pulsar	pulsar	k1gInSc1	pulsar
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Virtual	Virtual	k1gInSc1	Virtual
Trips	Tripsa	k1gFnPc2	Tripsa
to	ten	k3xDgNnSc4	ten
Black	Black	k1gMnSc1	Black
Holes	Holes	k1gMnSc1	Holes
and	and	k?	and
Neutron	neutron	k1gInSc1	neutron
Stars	Stars	k1gInSc1	Stars
</s>
</p>
<p>
<s>
Astronomia	Astronomia	k1gFnSc1	Astronomia
-	-	kIx~	-
astronomický	astronomický	k2eAgInSc1d1	astronomický
server	server	k1gInSc1	server
Fakulty	fakulta	k1gFnSc2	fakulta
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
ZČU	ZČU	kA	ZČU
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
</s>
</p>
