<s>
Neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
astronomické	astronomický	k2eAgNnSc1d1	astronomické
těleso	těleso	k1gNnSc1	těleso
tvořené	tvořený	k2eAgNnSc1d1	tvořené
převážně	převážně	k6eAd1	převážně
neutrony	neutron	k1gInPc7	neutron
udržovanými	udržovaný	k2eAgInPc7d1	udržovaný
pohromadě	pohromadě	k6eAd1	pohromadě
gravitační	gravitační	k2eAgFnSc7d1	gravitační
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
