<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
UK	UK	kA
<g/>
,	,	kIx,
latinský	latinský	k2eAgInSc1d1
název	název	k1gInSc1
Universitas	Universitas	k1gInSc1
Carolina	Carolina	k1gFnSc1
<g/>
)	)	kIx)
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
je	být	k5eAaImIp3nS
nejvýznamnější	významný	k2eAgFnSc1d3
česká	český	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
evropských	evropský	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
<g/>
,	,	kIx,
nejstarší	starý	k2eAgFnSc1d3
severně	severně	k6eAd1
od	od	k7c2
Itálie	Itálie	k1gFnSc2
a	a	k8xC
východně	východně	k6eAd1
od	od	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
.	.	kIx.
</s>