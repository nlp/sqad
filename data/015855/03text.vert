<s>
Andrea	Andrea	k1gFnSc1
Casiraghi	Casiragh	k1gFnSc2
</s>
<s>
Andrea	Andrea	k1gFnSc1
Casiraghi	Casiragh	k1gFnSc2
Andrea	Andrea	k1gFnSc1
Casiraghi	Casiragh	k1gFnSc2
Narození	narození	k1gNnSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1984	#num#	k4
(	(	kIx(
<g/>
36	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Monte	Mont	k1gMnSc5
Carlo	Carla	k1gMnSc5
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
American	American	k1gMnSc1
University	universita	k1gFnSc2
of	of	k?
ParisThe	ParisTh	k1gMnSc2
New	New	k1gMnSc2
School	School	k1gInSc4
Povolání	povolání	k1gNnSc2
</s>
<s>
aristokrat	aristokrat	k1gMnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
katolicismus	katolicismus	k1gInSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Tatiana	Tatian	k1gMnSc2
Santo	Santo	k1gNnSc1
Domingo	Domingo	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2013	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Sacha	Sacha	k1gFnSc1
CasiraghiIndia	CasiraghiIndium	k1gNnSc2
CasiraghiMaximilian	CasiraghiMaximiliana	k1gFnPc2
Casiraghi	Casiragh	k1gFnSc2
Rodiče	rodič	k1gMnPc4
</s>
<s>
Stefano	Stefana	k1gFnSc5
Casiraghi	Casiragh	k1gFnSc5
a	a	k8xC
Caroline	Carolin	k1gInSc5
<g/>
,	,	kIx,
hanoverská	hanoverský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
Rod	rod	k1gInSc1
</s>
<s>
Grimaldiové	Grimaldius	k1gMnPc1
Příbuzní	příbuzný	k2eAgMnPc1d1
</s>
<s>
Charlotte	Charlott	k1gMnSc5
Casiraghi	Casiragh	k1gMnSc5
<g/>
,	,	kIx,
Pierre	Pierr	k1gMnSc5
Casiraghi	Casiragh	k1gMnSc5
a	a	k8xC
Alexandra	Alexandr	k1gMnSc2
<g/>
,	,	kIx,
hanoverská	hanoverský	k2eAgFnSc1d1
princezna	princezna	k1gFnSc1
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
Ernst	Ernst	k1gMnSc1
August	August	k1gMnSc1
Prince	princ	k1gMnSc2
of	of	k?
Hanover	Hanover	k1gInSc1
(	(	kIx(
<g/>
nevlastní	vlastnit	k5eNaImIp3nS
bratr	bratr	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Prince	princ	k1gMnSc2
Christian	Christian	k1gMnSc1
of	of	k?
Hanover	Hanover	k1gMnSc1
(	(	kIx(
<g/>
nevlastní	vlastní	k2eNgMnSc1d1
bratr	bratr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Andrea	Andrea	k1gFnSc1
Albert	Alberta	k1gFnPc2
Pierre	Pierr	k1gInSc5
Casiraghi	Casiraghi	k1gNnPc7
(	(	kIx(
<g/>
*	*	kIx~
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1984	#num#	k4
<g/>
,	,	kIx,
Monte	Mont	k1gMnSc5
Carlo	Carla	k1gMnSc5
<g/>
,	,	kIx,
Monako	Monako	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
monacké	monacký	k2eAgFnSc2d1
princezny	princezna	k1gFnSc2
Caroline	Carolin	k1gInSc5
a	a	k8xC
italského	italský	k2eAgMnSc2d1
sportovce	sportovec	k1gMnSc2
a	a	k8xC
obchodníka	obchodník	k1gMnSc2
Stefana	Stefan	k1gMnSc2
Casiraghiho	Casiraghi	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
matce	matka	k1gFnSc6
je	být	k5eAaImIp3nS
druhý	druhý	k4xOgMnSc1
v	v	k7c6
pořadí	pořadí	k1gNnSc6
(	(	kIx(
<g/>
celkově	celkově	k6eAd1
čtvrtý	čtvrtý	k4xOgMnSc1
<g/>
)	)	kIx)
na	na	k7c4
monacký	monacký	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Dětství	dětství	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1984	#num#	k4
ve	v	k7c4
22	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
Princezny	princezna	k1gFnSc2
Grace	Grace	k1gFnSc2
v	v	k7c6
Monte	Mont	k1gInSc5
Carlu	Carla	k1gFnSc4
a	a	k8xC
jméno	jméno	k1gNnSc4
dostal	dostat	k5eAaPmAgMnS
po	po	k7c6
příteli	přítel	k1gMnSc3
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
kmotry	kmotra	k1gFnPc4
byla	být	k5eAaImAgFnS
teta	teta	k1gFnSc1
princezna	princezna	k1gFnSc1
Stéphanie	Stéphanie	k1gFnSc1
a	a	k8xC
strýc	strýc	k1gMnSc1
Marco	Marco	k1gMnSc1
Casiraghi	Casiraghi	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
dva	dva	k4xCgMnPc4
mladší	mladý	k2eAgMnPc4d2
sourozence	sourozenec	k1gMnPc4
Charlotte	Charlott	k1gInSc5
a	a	k8xC
Pierra	Pierr	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
mladší	mladý	k2eAgFnSc4d2
nevlastní	vlastní	k2eNgFnSc4d1
sestru	sestra	k1gFnSc4
<g/>
,	,	kIx,
princeznu	princezna	k1gFnSc4
Alexandru	Alexandra	k1gFnSc4
Hanoverskou	Hanoverský	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
zahynul	zahynout	k5eAaPmAgMnS
při	při	k7c6
lodním	lodní	k2eAgNnSc6d1
neštěstí	neštěstí	k1gNnSc6
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
šest	šest	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS
mezinárodní	mezinárodní	k2eAgFnSc4d1
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
pokračoval	pokračovat	k5eAaImAgInS
ve	v	k7c6
studiu	studio	k1gNnSc6
na	na	k7c4
American	American	k1gInSc4
University	universita	k1gFnSc2
of	of	k?
Paris	Paris	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
bakalářský	bakalářský	k2eAgInSc1d1
titul	titul	k1gInSc1
v	v	k7c6
oborech	obor	k1gInPc6
výtvarné	výtvarný	k2eAgFnSc2d1
umění	umění	k1gNnSc3
a	a	k8xC
mezinárodní	mezinárodní	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mluví	mluvit	k5eAaImIp3nS
plynule	plynule	k6eAd1
francouzsky	francouzsky	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
a	a	k8xC
německy	německy	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Soukromý	soukromý	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
žije	žít	k5eAaImIp3nS
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volném	volný	k2eAgInSc6d1
čase	čas	k1gInSc6
jezdí	jezdit	k5eAaImIp3nS
na	na	k7c6
koni	kůň	k1gMnSc6
<g/>
,	,	kIx,
lyžuje	lyžovat	k5eAaImIp3nS
<g/>
,	,	kIx,
hraje	hrát	k5eAaImIp3nS
fotbal	fotbal	k1gInSc4
a	a	k8xC
také	také	k9
na	na	k7c4
kytaru	kytara	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
byl	být	k5eAaImAgInS
zařazen	zařadit	k5eAaPmNgInS
na	na	k7c4
seznam	seznam	k1gInSc4
50	#num#	k4
nejkrásnějších	krásný	k2eAgMnPc2d3
lidí	člověk	k1gMnPc2
světa	svět	k1gInSc2
podle	podle	k7c2
časopisu	časopis	k1gInSc2
People	People	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnPc2
přítelkyní	přítelkyně	k1gFnPc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
Tatiana	Tatiana	k1gFnSc1
Santo	Sant	k2eAgNnSc4d1
Domingo	Domingo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznámili	seznámit	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c6
lyceu	lyceum	k1gNnSc6
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
Tatiana	Tatiana	k1gFnSc1
Santo	Sant	k2eAgNnSc4d1
Domingo	Domingo	k1gNnSc4
porodila	porodit	k5eAaPmAgFnS
Casiraghiho	Casiraghi	k1gMnSc4
syna	syn	k1gMnSc4
Sashu	Sasha	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
září	září	k1gNnSc6
2013	#num#	k4
se	se	k3xPyFc4
vzali	vzít	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Sasha	Sasha	k1gMnSc1
se	se	k3xPyFc4
díky	díky	k7c3
sňatku	sňatek	k1gInSc3
dostal	dostat	k5eAaPmAgMnS
na	na	k7c4
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
linii	linie	k1gFnSc6
následnictví	následnictví	k1gNnSc2
monackého	monacký	k2eAgInSc2d1
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
hned	hned	k6eAd1
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
otce	otec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Tatiana	Tatiana	k1gFnSc1
Santo	Sant	k2eAgNnSc4d1
Domingo	Domingo	k1gNnSc4
gives	gives	k1gMnSc1
birth	birth	k1gMnSc1
to	ten	k3xDgNnSc4
a	a	k8xC
baby	baby	k1gNnSc4
boy	boa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hello	Hello	k1gNnSc1
<g/>
.	.	kIx.
22	#num#	k4
March	March	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
23	#num#	k4
March	March	k1gInSc1
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Caroline	Carolin	k1gInSc5
di	di	k?
Monaco	Monaco	k1gNnSc1
<g/>
,	,	kIx,
nonna	nonen	k2eAgFnSc1d1
meraviglia	meraviglia	k1gFnSc1
a	a	k8xC
Saint	Saint	k1gMnSc1
Tropez	Tropez	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oggi	Oggi	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
monacké	monacký	k2eAgFnSc2d1
princezny	princezna	k1gFnSc2
Caroliny	Carolina	k1gFnSc2
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-01	2013-09-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
210921631	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
</s>
