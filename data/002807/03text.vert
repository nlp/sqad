<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Menšík	Menšík	k1gMnSc1	Menšík
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1929	[number]	k4	1929
Ivančice	Ivančice	k1gFnPc1	Ivančice
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1988	[number]	k4	1988
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
,	,	kIx,	,
lidový	lidový	k2eAgMnSc1d1	lidový
vypravěč	vypravěč	k1gMnSc1	vypravěč
a	a	k8xC	a
bavič	bavič	k1gMnSc1	bavič
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
ocenění	ocenění	k1gNnSc2	ocenění
národní	národní	k2eAgMnSc1d1	národní
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
herečky	herečka	k1gFnSc2	herečka
a	a	k8xC	a
moderátorky	moderátorka	k1gFnSc2	moderátorka
Martiny	Martina	k1gFnSc2	Martina
Menšíkové	Menšíková	k1gFnSc2	Menšíková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
hrál	hrát	k5eAaImAgMnS	hrát
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k1gNnSc4	málo
-	-	kIx~	-
většinu	většina	k1gFnSc4	většina
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
150	[number]	k4	150
rolí	role	k1gFnPc2	role
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
a	a	k8xC	a
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
moravských	moravský	k2eAgFnPc2d1	Moravská
Ivančic	Ivančice	k1gFnPc2	Ivančice
přičichl	přičichnout	k5eAaPmAgInS	přičichnout
k	k	k7c3	k
divadlu	divadlo	k1gNnSc3	divadlo
v	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
souboru	soubor	k1gInSc6	soubor
Adolfa	Adolf	k1gMnSc2	Adolf
Pištěláka	Pištělák	k1gMnSc2	Pištělák
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Brněnských	brněnský	k2eAgFnPc6d1	brněnská
strojírnách	strojírna	k1gFnPc6	strojírna
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
brněnskou	brněnský	k2eAgFnSc4d1	brněnská
JAMU	jam	k1gInSc6	jam
(	(	kIx(	(
<g/>
Janáčkova	Janáčkův	k2eAgFnSc1d1	Janáčkova
akademie	akademie	k1gFnSc1	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
pokus	pokus	k1gInSc4	pokus
podařilo	podařit	k5eAaPmAgNnS	podařit
a	a	k8xC	a
po	po	k7c6	po
absolutoriu	absolutorium	k1gNnSc6	absolutorium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
Vesnického	vesnický	k2eAgNnSc2d1	vesnické
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gInSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
(	(	kIx(	(
<g/>
v	v	k7c6	v
divadelní	divadelní	k2eAgFnSc6d1	divadelní
hře	hra	k1gFnSc6	hra
Hledač	hledač	k1gInSc4	hledač
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
objevil	objevit	k5eAaPmAgMnS	objevit
E.	E.	kA	E.
<g/>
F.	F.	kA	F.
<g/>
Burian	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gNnSc4	on
také	také	k9	také
angažoval	angažovat	k5eAaBmAgMnS	angažovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
dostal	dostat	k5eAaPmAgMnS	dostat
spousty	spousta	k1gFnPc4	spousta
příležitostí	příležitost	k1gFnPc2	příležitost
a	a	k8xC	a
přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
divadlem	divadlo	k1gNnSc7	divadlo
zcela	zcela	k6eAd1	zcela
pohlcen	pohlcen	k2eAgMnSc1d1	pohlcen
<g/>
,	,	kIx,	,
toužil	toužit	k5eAaImAgMnS	toužit
zahrát	zahrát	k5eAaPmF	zahrát
si	se	k3xPyFc3	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vůbec	vůbec	k9	vůbec
první	první	k4xOgNnSc1	první
účinkování	účinkování	k1gNnSc1	účinkování
před	před	k7c7	před
kamerou	kamera	k1gFnSc7	kamera
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Velká	velký	k2eAgFnSc1d1	velká
příležitost	příležitost	k1gFnSc1	příležitost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
víceméně	víceméně	k9	víceméně
coby	coby	k?	coby
komparsista	komparsista	k1gMnSc1	komparsista
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Dědeček	dědeček	k1gMnSc1	dědeček
automobil	automobil	k1gInSc1	automobil
režiséra	režisér	k1gMnSc2	režisér
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
italského	italský	k2eAgMnSc4d1	italský
mechanika	mechanik	k1gMnSc4	mechanik
<g/>
,	,	kIx,	,
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
výrazněji	výrazně	k6eAd2	výrazně
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
práci	práce	k1gFnSc3	práce
u	u	k7c2	u
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
E.	E.	kA	E.
F.	F.	kA	F.
Burianem	Burian	k1gMnSc7	Burian
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
film	film	k1gInSc4	film
neměl	mít	k5eNaImAgMnS	mít
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Filmovým	filmový	k2eAgNnSc7d1	filmové
studiem	studio	k1gNnSc7	studio
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
divadlu	divadlo	k1gNnSc3	divadlo
se	se	k3xPyFc4	se
vracel	vracet	k5eAaImAgMnS	vracet
už	už	k6eAd1	už
víceméně	víceméně	k9	víceméně
ojediněle	ojediněle	k6eAd1	ojediněle
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
naplno	naplno	k6eAd1	naplno
rozjela	rozjet	k5eAaPmAgFnS	rozjet
jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
u	u	k7c2	u
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
jen	jen	k9	jen
komediálním	komediální	k2eAgMnSc7d1	komediální
hercem	herec	k1gMnSc7	herec
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
bravurně	bravurně	k6eAd1	bravurně
ztvárnit	ztvárnit	k5eAaPmF	ztvárnit
i	i	k9	i
tragikomické	tragikomický	k2eAgNnSc1d1	tragikomické
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
dramatické	dramatický	k2eAgFnSc2d1	dramatická
role	role	k1gFnSc2	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
již	již	k6eAd1	již
objevoval	objevovat	k5eAaImAgInS	objevovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
komediálních	komediální	k2eAgFnPc6d1	komediální
rolích	role	k1gFnPc6	role
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
ve	v	k7c6	v
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
a	a	k8xC	a
seriálech	seriál	k1gInPc6	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velkého	velký	k2eAgMnSc4d1	velký
baviče	bavič	k1gMnSc4	bavič
<g/>
,	,	kIx,	,
konferenciéra	konferenciér	k1gMnSc4	konferenciér
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
excelentního	excelentní	k2eAgMnSc2d1	excelentní
vypravěče	vypravěč	k1gMnSc2	vypravěč
<g/>
.	.	kIx.	.
</s>
<s>
Vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
doslova	doslova	k6eAd1	doslova
ve	v	k7c6	v
stovkách	stovka	k1gFnPc6	stovka
televizních	televizní	k2eAgInPc2d1	televizní
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
ve	v	k7c6	v
filmu	film	k1gInSc6	film
a	a	k8xC	a
televizi	televize	k1gFnSc6	televize
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
přes	přes	k7c4	přes
150	[number]	k4	150
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nP	stát
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
pořad	pořad	k1gInSc1	pořad
Bakaláři	bakalář	k1gMnPc1	bakalář
a	a	k8xC	a
nezapomenutelné	zapomenutelný	k2eNgNnSc1d1	nezapomenutelné
konferování	konferování	k1gNnSc1	konferování
silvestrovských	silvestrovský	k2eAgFnPc2d1	silvestrovská
estrád	estráda	k1gFnPc2	estráda
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgNnPc1d1	celé
dlouhá	dlouhý	k2eAgNnPc1d1	dlouhé
léta	léto	k1gNnPc1	léto
trpěl	trpět	k5eAaImAgInS	trpět
těžkým	těžký	k2eAgNnSc7d1	těžké
astmatem	astma	k1gNnSc7	astma
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
silný	silný	k2eAgInSc1d1	silný
kuřák	kuřák	k1gInSc1	kuřák
a	a	k8xC	a
také	také	k9	také
často	často	k6eAd1	často
pil	pít	k5eAaImAgMnS	pít
alkohol	alkohol	k1gInSc4	alkohol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
pitím	pití	k1gNnSc7	pití
a	a	k8xC	a
kouřením	kouření	k1gNnSc7	kouření
přestal	přestat	k5eAaPmAgMnS	přestat
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
stál	stát	k5eAaImAgInS	stát
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
seděl	sedět	k5eAaImAgMnS	sedět
před	před	k7c7	před
kamerou	kamera	k1gFnSc7	kamera
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1988	[number]	k4	1988
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
ABECEDA	abeceda	k1gFnSc1	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pražských	pražský	k2eAgInPc6d1	pražský
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Menšík	Menšík	k1gMnSc1	Menšík
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
první	první	k4xOgFnSc7	první
manželkou	manželka	k1gFnSc7	manželka
Věrou	Věra	k1gFnSc7	Věra
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
-	-	kIx~	-
syna	syn	k1gMnSc2	syn
Petra	Petr	k1gMnSc2	Petr
(	(	kIx(	(
<g/>
*	*	kIx~	*
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Vladimíru	Vladimíra	k1gFnSc4	Vladimíra
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
Olgou	Olga	k1gFnSc7	Olga
měl	mít	k5eAaImAgInS	mít
také	také	k9	také
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
-	-	kIx~	-
syna	syn	k1gMnSc2	syn
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Martinu	Martina	k1gFnSc4	Martina
(	(	kIx(	(
<g/>
*	*	kIx~	*
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
dětí	dítě	k1gFnPc2	dítě
stala	stát	k5eAaPmAgFnS	stát
herečkou	herečka	k1gFnSc7	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
zpráva	zpráva	k1gFnSc1	zpráva
ČTK	ČTK	kA	ČTK
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1988	[number]	k4	1988
<g/>
:	:	kIx,	:
Jeho	jeho	k3xOp3gInSc1	jeho
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Ivančicích	Ivančice	k1gFnPc6	Ivančice
zdobí	zdobit	k5eAaImIp3nS	zdobit
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkém	blízký	k2eAgInSc6d1	blízký
Památníku	památník	k1gInSc6	památník
Alfonse	Alfons	k1gMnSc2	Alfons
Muchy	Mucha	k1gMnSc2	Mucha
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
stálá	stálý	k2eAgFnSc1d1	stálá
výstava	výstava	k1gFnSc1	výstava
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Menšíka	Menšík	k1gMnSc2	Menšík
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
Rozhledna	rozhledna	k1gFnSc1	rozhledna
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Menšíka	Menšík	k1gMnSc2	Menšík
na	na	k7c6	na
Hlíně	hlína	k1gFnSc6	hlína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
Polici	police	k1gFnSc6	police
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
Žandova	Žandův	k2eAgFnSc1d1	Žandova
na	na	k7c6	na
Českolipsku	Českolipsko	k1gNnSc6	Českolipsko
<g/>
)	)	kIx)	)
měli	mít	k5eAaImAgMnP	mít
Menšíkovi	Menšíkův	k2eAgMnPc1d1	Menšíkův
v	v	k7c6	v
č.	č.	k?	č.
<g/>
p.	p.	k?	p.
6	[number]	k4	6
řadu	řad	k1gInSc2	řad
let	let	k1gInSc1	let
chalupu	chalupa	k1gFnSc4	chalupa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
Olga	Olga	k1gFnSc1	Olga
Menšíková	Menšíková	k1gFnSc1	Menšíková
část	část	k1gFnSc1	část
vybavení	vybavení	k1gNnSc2	vybavení
z	z	k7c2	z
chalupy	chalupa	k1gFnSc2	chalupa
Vlastivědnému	vlastivědný	k2eAgNnSc3d1	Vlastivědné
muzeu	muzeum	k1gNnSc3	muzeum
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Lípě	lípa	k1gFnSc6	lípa
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
hlavní	hlavní	k2eAgFnSc2d1	hlavní
budovy	budova	k1gFnSc2	budova
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
místnost	místnost	k1gFnSc4	místnost
<g/>
,	,	kIx,	,
expozice	expozice	k1gFnSc1	expozice
je	být	k5eAaImIp3nS	být
nazvána	nazván	k2eAgFnSc1d1	nazvána
Na	na	k7c6	na
chalupě	chalupa	k1gFnSc6	chalupa
u	u	k7c2	u
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Menšíka	Menšík	k1gMnSc2	Menšík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
existovala	existovat	k5eAaImAgFnS	existovat
kavárna	kavárna	k1gFnSc1	kavárna
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Menšíka	Menšík	k1gMnSc2	Menšík
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Menšík	Menšík	k1gMnSc1	Menšík
uměl	umět	k5eAaImAgMnS	umět
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
piano	piano	k1gNnSc4	piano
a	a	k8xC	a
mandolínu	mandolína	k1gFnSc4	mandolína
Hrál	hrát	k5eAaImAgInS	hrát
závodně	závodně	k6eAd1	závodně
házenou	házená	k1gFnSc4	házená
<g/>
,	,	kIx,	,
rekreačně	rekreačně	k6eAd1	rekreačně
fotbal	fotbal	k1gInSc1	fotbal
Když	když	k8xS	když
měl	mít	k5eAaImAgInS	mít
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
první	první	k4xOgFnSc4	první
zkoušku	zkouška	k1gFnSc4	zkouška
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Těžká	těžkat	k5eAaImIp3nS	těžkat
Barbora	Barbora	k1gFnSc1	Barbora
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
<g/>
,	,	kIx,	,
zaspal	zaspat	k5eAaPmAgMnS	zaspat
a	a	k8xC	a
Werich	Werich	k1gMnSc1	Werich
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
druhou	druhý	k4xOgFnSc4	druhý
šanci	šance	k1gFnSc4	šance
nedal	dát	k5eNaPmAgMnS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Menšíka	Menšík	k1gMnSc2	Menšík
bylo	být	k5eAaImAgNnS	být
opravdu	opravdu	k6eAd1	opravdu
hektické	hektický	k2eAgNnSc1d1	hektické
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
dělal	dělat	k5eAaImAgMnS	dělat
až	až	k9	až
tři	tři	k4xCgFnPc4	tři
besedy	beseda	k1gFnPc4	beseda
společně	společně	k6eAd1	společně
s	s	k7c7	s
několika	několik	k4yIc7	několik
kolegy	kolega	k1gMnPc7	kolega
<g/>
,	,	kIx,	,
a	a	k8xC	a
třeba	třeba	k6eAd1	třeba
i	i	k9	i
dvacetkrát	dvacetkrát	k6eAd1	dvacetkrát
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
pak	pak	k6eAd1	pak
zpravidla	zpravidla	k6eAd1	zpravidla
točil	točit	k5eAaImAgMnS	točit
filmy	film	k1gInPc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vozil	vozit	k5eAaImAgMnS	vozit
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nebyly	být	k5eNaImAgFnP	být
ani	ani	k8xC	ani
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
dostupné	dostupný	k2eAgInPc1d1	dostupný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
hektickému	hektický	k2eAgNnSc3d1	hektické
pracovnímu	pracovní	k2eAgNnSc3d1	pracovní
nasazení	nasazení	k1gNnSc3	nasazení
měl	mít	k5eAaImAgInS	mít
hned	hned	k6eAd1	hned
několik	několik	k4yIc4	několik
důvodů	důvod	k1gInPc2	důvod
<g/>
:	:	kIx,	:
jednak	jednak	k8xC	jednak
miloval	milovat	k5eAaImAgMnS	milovat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
finančně	finančně	k6eAd1	finančně
podporoval	podporovat	k5eAaImAgMnS	podporovat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
10	[number]	k4	10
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
pasivitu	pasivita	k1gFnSc4	pasivita
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
,	,	kIx,	,
z	z	k7c2	z
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
akcí	akce	k1gFnPc2	akce
KSČ	KSČ	kA	KSČ
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
dařilo	dařit	k5eAaImAgNnS	dařit
"	"	kIx"	"
<g/>
vykrucovat	vykrucovat	k5eAaImF	vykrucovat
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c4	na
národního	národní	k2eAgMnSc4d1	národní
umělce	umělec	k1gMnSc4	umělec
jej	on	k3xPp3gInSc2	on
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Gustav	Gustav	k1gMnSc1	Gustav
Husák	Husák	k1gMnSc1	Husák
Neměl	mít	k5eNaImAgMnS	mít
rád	rád	k6eAd1	rád
natáčení	natáčení	k1gNnSc4	natáčení
Bakalářů	bakalář	k1gMnPc2	bakalář
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgMnS	považovat
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
"	"	kIx"	"
<g/>
kouli	koule	k1gFnSc4	koule
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
uzoufání	uzoufání	k1gNnSc3	uzoufání
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
a	a	k8xC	a
nudná	nudný	k2eAgFnSc1d1	nudná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
sevřen	sevřít	k5eAaPmNgInS	sevřít
banálním	banální	k2eAgInSc7d1	banální
textem	text	k1gInSc7	text
<g/>
"	"	kIx"	"
Raději	rád	k6eAd2	rád
než	než	k8xS	než
Bakaláře	bakalář	k1gMnPc4	bakalář
měl	mít	k5eAaImAgMnS	mít
rád	rád	k6eAd1	rád
natáčení	natáčení	k1gNnSc4	natáčení
pořadu	pořad	k1gInSc2	pořad
Křeslo	křeslo	k1gNnSc4	křeslo
pro	pro	k7c4	pro
hosta	host	k1gMnSc4	host
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgNnSc2	který
si	se	k3xPyFc3	se
zval	zvát	k5eAaImAgMnS	zvát
především	především	k9	především
vědce	vědec	k1gMnSc4	vědec
a	a	k8xC	a
lékaře	lékař	k1gMnSc4	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
byl	být	k5eAaImAgInS	být
však	však	k9	však
nucen	nutit	k5eAaImNgMnS	nutit
zvát	zvát	k5eAaImF	zvát
i	i	k9	i
komunistické	komunistický	k2eAgMnPc4d1	komunistický
funkcionáře	funkcionář	k1gMnPc4	funkcionář
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
z	z	k7c2	z
pořadu	pořad	k1gInSc2	pořad
vyvázat	vyvázat	k5eAaPmF	vyvázat
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
pohádal	pohádat	k5eAaPmAgMnS	pohádat
i	i	k9	i
s	s	k7c7	s
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
ředitelem	ředitel	k1gMnSc7	ředitel
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
-	-	kIx~	-
Janem	Jan	k1gMnSc7	Jan
Zelenkou	Zelenka	k1gMnSc7	Zelenka
<g/>
.	.	kIx.	.
</s>
<s>
Účinkování	účinkování	k1gNnSc1	účinkování
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
zbavil	zbavit	k5eAaPmAgInS	zbavit
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
natáčení	natáčení	k1gNnSc2	natáčení
přišel	přijít	k5eAaPmAgMnS	přijít
opilý	opilý	k2eAgMnSc1d1	opilý
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
nejoblíbenějšího	oblíbený	k2eAgMnSc2d3	nejoblíbenější
spisovatele	spisovatel	k1gMnSc2	spisovatel
označil	označit	k5eAaPmAgMnS	označit
Liona	Lion	k1gMnSc4	Lion
Feuchtwangera	Feuchtwanger	k1gMnSc4	Feuchtwanger
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
měl	mít	k5eAaImAgInS	mít
rád	rád	k2eAgInSc4d1	rád
např.	např.	kA	např.
válečnou	válečný	k2eAgFnSc4d1	válečná
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
byl	být	k5eAaImAgMnS	být
Vladislav	Vladislav	k1gMnSc1	Vladislav
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prý	prý	k9	prý
na	na	k7c4	na
přání	přání	k1gNnSc4	přání
E.	E.	kA	E.
<g/>
F.	F.	kA	F.
<g/>
Buriana	Burian	k1gMnSc2	Burian
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Chtěl	chtít	k5eAaImAgMnS	chtít
odejít	odejít	k5eAaPmF	odejít
dříve	dříve	k6eAd2	dříve
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
využil	využít	k5eAaPmAgMnS	využít
chyby	chyba	k1gFnPc4	chyba
úřednice	úřednice	k1gFnPc4	úřednice
a	a	k8xC	a
všude	všude	k6eAd1	všude
uváděl	uvádět	k5eAaImAgMnS	uvádět
své	svůj	k3xOyFgNnSc4	svůj
datum	datum	k1gNnSc4	datum
narození	narození	k1gNnSc1	narození
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
dělal	dělat	k5eAaImAgMnS	dělat
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
starším	starý	k2eAgNnSc7d2	starší
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
–	–	k?	–
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
1971	[number]	k4	1971
-	-	kIx~	-
Kam	kam	k6eAd1	kam
slunce	slunce	k1gNnSc1	slunce
nechodí	chodit	k5eNaImIp3nS	chodit
[	[	kIx(	[
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
]	]	kIx)	]
1967	[number]	k4	1967
-	-	kIx~	-
Klec	klec	k1gFnSc1	klec
pro	pro	k7c4	pro
dva	dva	k4xCgInPc4	dva
1968	[number]	k4	1968
-	-	kIx~	-
Ach	ach	k0	ach
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
vojna	vojna	k1gFnSc1	vojna
[	[	kIx(	[
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
]	]	kIx)	]
1960	[number]	k4	1960
-	-	kIx~	-
Práče	práč	k1gInSc2	práč
(	(	kIx(	(
<g/>
Letěla	letět	k5eAaImAgFnS	letět
husička	husička	k1gFnSc1	husička
<g/>
;	;	kIx,	;
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
malý	malý	k2eAgMnSc1d1	malý
mysliveček	mysliveček	k1gMnSc1	mysliveček
<g/>
)	)	kIx)	)
1959	[number]	k4	1959
-	-	kIx~	-
Dařbuján	Dařbuján	k1gMnSc1	Dařbuján
a	a	k8xC	a
Pandrhola	Pandrhola	k?	Pandrhola
(	(	kIx(	(
<g/>
My	my	k3xPp1nPc1	my
jsme	být	k5eAaImIp1nP	být
tady	tady	k6eAd1	tady
dva	dva	k4xCgInPc1	dva
<g/>
)	)	kIx)	)
1958	[number]	k4	1958
-	-	kIx~	-
Hvězda	hvězda	k1gFnSc1	hvězda
jede	jet	k5eAaImIp3nS	jet
na	na	k7c4	na
jih	jih	k1gInSc4	jih
(	(	kIx(	(
<g/>
Ach	ach	k0	ach
synku	synek	k1gMnSc5	synek
<g/>
,	,	kIx,	,
synku	synek	k1gMnSc5	synek
<g/>
)	)	kIx)	)
</s>
