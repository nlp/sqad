<s>
Secesní	secesní	k2eAgFnSc1d1	secesní
stavba	stavba	k1gFnSc1	stavba
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
prostoru	prostor	k1gInSc6	prostor
skrývá	skrývat	k5eAaImIp3nS	skrývat
řadu	řada	k1gFnSc4	řada
pozoruhodných	pozoruhodný	k2eAgInPc2d1	pozoruhodný
uměleckých	umělecký	k2eAgInPc2d1	umělecký
výtvorů	výtvor	k1gInPc2	výtvor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
interiér	interiér	k1gInSc1	interiér
obřadní	obřadní	k2eAgFnSc2d1	obřadní
síně	síň	k1gFnSc2	síň
<g/>
,	,	kIx,	,
okenní	okenní	k2eAgFnSc2d1	okenní
vitráže	vitráž	k1gFnSc2	vitráž
nebo	nebo	k8xC	nebo
malou	malý	k2eAgFnSc4d1	malá
kašnu	kašna	k1gFnSc4	kašna
v	v	k7c6	v
přístupovém	přístupový	k2eAgInSc6d1	přístupový
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
