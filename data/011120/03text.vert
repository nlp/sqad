<p>
<s>
Krematorium	krematorium	k1gNnSc1	krematorium
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Liberec	Liberec	k1gInSc1	Liberec
IV-Perštýn	IV-Perštýna	k1gFnPc2	IV-Perštýna
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
U	u	k7c2	u
Krematoria	krematorium	k1gNnSc2	krematorium
460	[number]	k4	460
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Secesní	secesní	k2eAgFnSc1d1	secesní
stavba	stavba	k1gFnSc1	stavba
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
prostoru	prostor	k1gInSc6	prostor
skrývá	skrývat	k5eAaImIp3nS	skrývat
řadu	řada	k1gFnSc4	řada
pozoruhodných	pozoruhodný	k2eAgInPc2d1	pozoruhodný
uměleckých	umělecký	k2eAgInPc2d1	umělecký
výtvorů	výtvor	k1gInPc2	výtvor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
interiér	interiér	k1gInSc1	interiér
obřadní	obřadní	k2eAgFnSc2d1	obřadní
síně	síň	k1gFnSc2	síň
<g/>
,	,	kIx,	,
okenní	okenní	k2eAgFnSc2d1	okenní
vitráže	vitráž	k1gFnSc2	vitráž
nebo	nebo	k8xC	nebo
malou	malý	k2eAgFnSc4d1	malá
kašnu	kašna	k1gFnSc4	kašna
v	v	k7c6	v
přístupovém	přístupový	k2eAgInSc6d1	přístupový
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zpopelněno	zpopelnit	k5eAaPmNgNnS	zpopelnit
179757	[number]	k4	179757
zemřelých	zemřelý	k1gMnPc2	zemřelý
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k9	jako
nemovitá	movitý	k2eNgFnSc1d1	nemovitá
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
se	se	k3xPyFc4	se
myšlenka	myšlenka	k1gFnSc1	myšlenka
pohřbívání	pohřbívání	k1gNnSc2	pohřbívání
žehem	žeh	k1gInSc7	žeh
dostala	dostat	k5eAaPmAgFnS	dostat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
přes	přes	k7c4	přes
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Propagátorem	propagátor	k1gMnSc7	propagátor
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
spolek	spolek	k1gInSc4	spolek
přátel	přítel	k1gMnPc2	přítel
žehu	žeh	k1gInSc2	žeh
"	"	kIx"	"
<g/>
Die	Die	k1gMnSc1	Die
Flamme	Flamme	k1gMnSc1	Flamme
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6	Rakousku-Uhersek
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
pohřbívání	pohřbívání	k1gNnSc2	pohřbívání
nebyl	být	k5eNaImAgInS	být
legislativně	legislativně	k6eAd1	legislativně
schválen	schválit	k5eAaPmNgInS	schválit
a	a	k8xC	a
státní	státní	k2eAgInPc1d1	státní
úřady	úřad	k1gInPc1	úřad
jej	on	k3xPp3gNnSc4	on
nepovolovaly	povolovat	k5eNaImAgFnP	povolovat
<g/>
.	.	kIx.	.
</s>
<s>
Zemřelí	zemřelý	k2eAgMnPc1d1	zemřelý
propagátoři	propagátor	k1gMnPc1	propagátor
kremace	kremace	k1gFnSc2	kremace
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
Rakousko-Uherska	Rakousko-Uhersko	k1gNnPc4	Rakousko-Uhersko
byli	být	k5eAaImAgMnP	být
proto	proto	k8xC	proto
zpopelňováni	zpopelňován	k2eAgMnPc1d1	zpopelňován
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
v	v	k7c6	v
žitavském	žitavský	k2eAgNnSc6d1	žitavské
a	a	k8xC	a
dráždanském	dráždanský	k2eAgNnSc6d1	dráždanský
krematoriu	krematorium	k1gNnSc6	krematorium
<g/>
.	.	kIx.	.
<g/>
Libereckými	liberecký	k2eAgFnPc7d1	liberecká
osobnostmi	osobnost	k1gFnPc7	osobnost
propagujícími	propagující	k2eAgFnPc7d1	propagující
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
pohřbívání	pohřbívání	k1gNnPc2	pohřbívání
byli	být	k5eAaImAgMnP	být
MUDr.	MUDr.	kA	MUDr.
Václav	Václav	k1gMnSc1	Václav
Šamánek	Šamánek	k1gMnSc1	Šamánek
st.	st.	kA	st.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
švagr	švagr	k1gMnSc1	švagr
MUDr.	MUDr.	kA	MUDr.
Franz	Franz	k1gMnSc1	Franz
Bayer	Bayer	k1gMnSc1	Bayer
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1893	[number]	k4	1893
-	-	kIx~	-
1929	[number]	k4	1929
liberecký	liberecký	k2eAgMnSc1d1	liberecký
purkmistr	purkmistr	k1gMnSc1	purkmistr
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
prosadil	prosadit	k5eAaPmAgMnS	prosadit
vydání	vydání	k1gNnSc4	vydání
stavebního	stavební	k2eAgNnSc2d1	stavební
povolení	povolení	k1gNnSc2	povolení
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
krematoria	krematorium	k1gNnSc2	krematorium
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc4	vydání
povolení	povolení	k1gNnSc2	povolení
předcházel	předcházet	k5eAaImAgInS	předcházet
soudní	soudní	k2eAgInSc1d1	soudní
spor	spor	k1gInSc1	spor
u	u	k7c2	u
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Soudním	soudní	k2eAgNnSc7d1	soudní
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
krematorium	krematorium	k1gNnSc4	krematorium
postavit	postavit	k5eAaPmF	postavit
může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpopelňovat	zpopelňovat	k5eAaImF	zpopelňovat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nesmí	smět	k5eNaImIp3nS	smět
<g/>
.	.	kIx.	.
<g/>
Místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
krematorium	krematorium	k1gNnSc4	krematorium
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
Monstrančním	Monstranční	k2eAgInPc3d1	Monstranční
vrchu	vrch	k1gInSc2	vrch
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
za	za	k7c7	za
libereckou	liberecký	k2eAgFnSc7d1	liberecká
Střelnicí	střelnice	k1gFnSc7	střelnice
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
návrhy	návrh	k1gInPc7	návrh
bylo	být	k5eAaImAgNnS	být
umístit	umístit	k5eAaPmF	umístit
jej	on	k3xPp3gNnSc4	on
na	na	k7c4	na
Ještědský	ještědský	k2eAgInSc4d1	ještědský
hřeben	hřeben	k1gInSc4	hřeben
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1915	[number]	k4	1915
a	a	k8xC	a
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
drážďanského	drážďanský	k2eAgMnSc2d1	drážďanský
architekta	architekt	k1gMnSc2	architekt
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Bitzana	Bitzana	k1gFnSc1	Bitzana
jeho	jeho	k3xOp3gFnSc4	jeho
stavbu	stavba	k1gFnSc4	stavba
provedla	provést	k5eAaPmAgFnS	provést
liberecká	liberecký	k2eAgFnSc1d1	liberecká
firma	firma	k1gFnSc1	firma
Gustav	Gustav	k1gMnSc1	Gustav
Sacher	Sachra	k1gFnPc2	Sachra
a	a	k8xC	a
synové	syn	k1gMnPc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Dokončeno	dokončen	k2eAgNnSc1d1	dokončeno
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
a	a	k8xC	a
již	již	k6eAd1	již
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zkušebně	zkušebně	k6eAd1	zkušebně
zpopelněna	zpopelněn	k2eAgFnSc1d1	zpopelněna
dvě	dva	k4xCgNnPc4	dva
mrtvá	mrtvý	k2eAgNnPc4d1	mrtvé
zvířecí	zvířecí	k2eAgNnPc4d1	zvířecí
těla	tělo	k1gNnPc4	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostně	slavnostně	k6eAd1	slavnostně
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
Zazněla	zaznít	k5eAaPmAgFnS	zaznít
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
předehra	předehra	k1gFnSc1	předehra
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
Parsifal	Parsifal	k1gMnSc2	Parsifal
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
zranění	zranění	k1gNnSc4	zranění
a	a	k8xC	a
nemoce	moko	k6eNd1	moko
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgFnP	být
kremace	kremace	k1gFnPc1	kremace
povoleny	povolen	k2eAgFnPc1d1	povolena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
monarchie	monarchie	k1gFnSc2	monarchie
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
kremace	kremace	k1gFnSc1	kremace
provedena	proveden	k2eAgFnSc1d1	provedena
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
libereckého	liberecký	k2eAgMnSc2d1	liberecký
měšťana	měšťan	k1gMnSc2	měšťan
Roberta	Robert	k1gMnSc2	Robert
Jahna	Jahn	k1gMnSc2	Jahn
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
zpopelněno	zpopelnit	k5eAaPmNgNnS	zpopelnit
dalších	další	k2eAgMnPc2d1	další
82	[number]	k4	82
zemřelých	zemřelý	k1gMnPc2	zemřelý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
zpopelněných	zpopelněný	k2eAgFnPc2d1	zpopelněná
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
mezi	mezi	k7c7	mezi
500	[number]	k4	500
až	až	k9	až
600	[number]	k4	600
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Krematorium	krematorium	k1gNnSc1	krematorium
sloužilo	sloužit	k5eAaImAgNnS	sloužit
pro	pro	k7c4	pro
zemřelé	zemřelá	k1gFnPc4	zemřelá
z	z	k7c2	z
Liberce	Liberec	k1gInSc2	Liberec
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
urny	urna	k1gFnPc1	urna
byly	být	k5eAaImAgFnP	být
ukládány	ukládat	k5eAaImNgFnP	ukládat
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
libereckém	liberecký	k2eAgInSc6d1	liberecký
ústředním	ústřední	k2eAgInSc6d1	ústřední
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
Ruprechticích	Ruprechtice	k1gFnPc6	Ruprechtice
a	a	k8xC	a
na	na	k7c6	na
libereckém	liberecký	k2eAgNnSc6d1	liberecké
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
urnový	urnový	k2eAgInSc1d1	urnový
háj	háj	k1gInSc1	háj
s	s	k7c7	s
parkovou	parkový	k2eAgFnSc7d1	parková
architekturou	architektura	k1gFnSc7	architektura
navrhnutou	navrhnutý	k2eAgFnSc7d1	navrhnutá
architektem	architekt	k1gMnSc7	architekt
Karlem	Karel	k1gMnSc7	Karel
Kerlem	Kerl	k1gMnSc7	Kerl
a	a	k8xC	a
již	již	k9	již
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
kapacitních	kapacitní	k2eAgInPc2d1	kapacitní
důvodů	důvod	k1gInPc2	důvod
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
zpopelněni	zpopelněn	k2eAgMnPc1d1	zpopelněn
studenti	student	k1gMnPc1	student
popravení	popravený	k2eAgMnPc1d1	popravený
po	po	k7c6	po
demonstracích	demonstrace	k1gFnPc6	demonstrace
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
zemřelí	zemřelý	k2eAgMnPc1d1	zemřelý
vězni	vězeň	k1gMnPc1	vězeň
z	z	k7c2	z
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
u	u	k7c2	u
Jablonce	Jablonec	k1gInSc2	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
a	a	k8xC	a
oběti	oběť	k1gFnSc2	oběť
popravené	popravený	k2eAgFnSc2d1	popravená
libereckým	liberecký	k2eAgNnSc7d1	liberecké
gestapem	gestapo	k1gNnSc7	gestapo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konaly	konat	k5eAaImAgInP	konat
dvě	dva	k4xCgFnPc4	dva
velké	velký	k2eAgFnPc4d1	velká
tryzny	tryzna	k1gFnPc4	tryzna
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
srpnové	srpnový	k2eAgFnSc2d1	srpnová
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
tryzen	tryzna	k1gFnPc2	tryzna
promluvili	promluvit	k5eAaPmAgMnP	promluvit
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Tříska	Tříska	k1gMnSc1	Tříska
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
přestavba	přestavba	k1gFnSc1	přestavba
budov	budova	k1gFnPc2	budova
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Poříze	poříz	k1gMnSc5	poříz
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zaskleno	zasklen	k2eAgNnSc1d1	zaskleno
podloubí	podloubí	k1gNnSc1	podloubí
obou	dva	k4xCgFnPc2	dva
křídel	křídlo	k1gNnPc2	křídlo
historické	historický	k2eAgFnSc2d1	historická
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
upravena	upraven	k2eAgFnSc1d1	upravena
obřadní	obřadní	k2eAgFnSc1d1	obřadní
síň	síň	k1gFnSc1	síň
<g/>
.	.	kIx.	.
</s>
<s>
Úpravou	úprava	k1gFnSc7	úprava
prošel	projít	k5eAaPmAgInS	projít
i	i	k9	i
urnový	urnový	k2eAgInSc1d1	urnový
háj	háj	k1gInSc1	háj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Janaty	Janata	k1gMnPc4	Janata
byly	být	k5eAaImAgFnP	být
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
přistavěny	přistavěn	k2eAgFnPc1d1	přistavěna
další	další	k2eAgFnPc1d1	další
budovy	budova	k1gFnPc1	budova
pro	pro	k7c4	pro
nové	nový	k2eAgFnPc4d1	nová
technologie	technologie	k1gFnPc4	technologie
a	a	k8xC	a
otevřeny	otevřen	k2eAgFnPc4d1	otevřena
prodejny	prodejna	k1gFnPc4	prodejna
v	v	k7c6	v
podloubí	podloubí	k1gNnSc6	podloubí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Krematorium	krematorium	k1gNnSc1	krematorium
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Krematorium	krematorium	k1gNnSc1	krematorium
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
historie	historie	k1gFnPc1	historie
posloužily	posloužit	k5eAaPmAgFnP	posloužit
jako	jako	k9	jako
ústřední	ústřední	k2eAgInSc4d1	ústřední
námět	námět	k1gInSc4	námět
pro	pro	k7c4	pro
německy	německy	k6eAd1	německy
psaný	psaný	k2eAgInSc4d1	psaný
román	román	k1gInSc4	román
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Rudiše	Rudiš	k1gMnSc2	Rudiš
Winterbergova	Winterbergův	k2eAgFnSc1d1	Winterbergův
poslední	poslední	k2eAgFnSc1d1	poslední
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
FRAJEROVÁ	FRAJEROVÁ	kA	FRAJEROVÁ
<g/>
,	,	kIx,	,
Blanka	Blanka	k1gFnSc1	Blanka
<g/>
.	.	kIx.	.
</s>
<s>
Klíč	klíč	k1gInSc1	klíč
k	k	k7c3	k
českým	český	k2eAgInPc3d1	český
hřbitovům	hřbitov	k1gInPc3	hřbitov
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
343	[number]	k4	343
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
2984	[number]	k4	2984
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Po	po	k7c6	po
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
–	–	k?	–
Okres	okres	k1gInSc1	okres
Liberec	Liberec	k1gInSc1	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
235	[number]	k4	235
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SVOBODOVÁ	Svobodová	k1gFnSc1	Svobodová
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
.	.	kIx.	.
</s>
<s>
Krematorium	krematorium	k1gNnSc1	krematorium
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
sekularizace	sekularizace	k1gFnSc2	sekularizace
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
ideové	ideový	k2eAgFnSc2d1	ideová
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgFnSc2d1	stavební
a	a	k8xC	a
typologické	typologický	k2eAgFnSc2d1	typologická
proměny	proměna	k1gFnSc2	proměna
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Artefactum	Artefactum	k1gNnSc1	Artefactum
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
182	[number]	k4	182
s.	s.	k?	s.
Epigraphica	Epigraphic	k2eAgFnSc1d1	Epigraphic
&	&	k?	&
sepulcralia	sepulcralia	k1gFnSc1	sepulcralia
<g/>
.	.	kIx.	.
</s>
<s>
Monographica	Monographica	k1gFnSc1	Monographica
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86890	[number]	k4	86890
<g/>
-	-	kIx~	-
<g/>
51	[number]	k4	51
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
36	[number]	k4	36
-	-	kIx~	-
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Krematorium	krematorium	k1gNnSc1	krematorium
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Krematorium	krematorium	k1gNnSc1	krematorium
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
