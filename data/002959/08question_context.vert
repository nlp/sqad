<s>
Vrchlabí	Vrchlabí	k1gNnSc1	Vrchlabí
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Hohenelbe	Hohenelb	k1gMnSc5	Hohenelb
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Albipolis	Albipolis	k1gFnSc1	Albipolis
<g/>
,	,	kIx,	,
krkonošským	krkonošský	k2eAgNnSc7d1	Krkonošské
nářečím	nářečí	k1gNnSc7	nářečí
Verchláb	Verchlába	k1gFnPc2	Verchlába
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc1d1	české
město	město	k1gNnSc1	město
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Nazýváno	nazýván	k2eAgNnSc1d1	nazýváno
bývá	bývat	k5eAaImIp3nS	bývat
jako	jako	k9	jako
Vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
<g/>
.	.	kIx.	.
</s>

