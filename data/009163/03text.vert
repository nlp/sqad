<p>
<s>
Petrus	Petrus	k1gMnSc1	Petrus
de	de	k?	de
Cruce	Cruce	k1gMnSc1	Cruce
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
la	la	k1gNnPc7	la
Croix	Croix	k1gInSc1	Croix
<g/>
,	,	kIx,	,
1270	[number]	k4	1270
<g/>
?	?	kIx.	?
</s>
<s>
Amiens	Amiens	k1gInSc1	Amiens
–	–	k?	–
před	před	k7c4	před
1347	[number]	k4	1347
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
středověký	středověký	k2eAgMnSc1d1	středověký
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
teoretik	teoretik	k1gMnSc1	teoretik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Petrus	Petrus	k1gMnSc1	Petrus
de	de	k?	de
Cruce	Cruce	k1gMnSc1	Cruce
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1298	[number]	k4	1298
jej	on	k3xPp3gMnSc4	on
král	král	k1gMnSc1	král
Filip	Filip	k1gMnSc1	Filip
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
pověřil	pověřit	k5eAaPmAgMnS	pověřit
zkomponováním	zkomponování	k1gNnPc3	zkomponování
oficia	oficium	k1gNnPc4	oficium
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
svatého	svatý	k2eAgMnSc2d1	svatý
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
žákem	žák	k1gMnSc7	žák
Jakuba	Jakub	k1gMnSc2	Jakub
z	z	k7c2	z
Lutychu	Lutych	k1gInSc2	Lutych
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gNnSc4	on
vedle	vedle	k7c2	vedle
Franka	Frank	k1gMnSc2	Frank
Kolínského	kolínský	k2eAgMnSc2d1	kolínský
a	a	k8xC	a
Lamberta	Lambert	k1gMnSc2	Lambert
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c2	za
zástupce	zástupce	k1gMnSc2	zástupce
starého	starý	k2eAgNnSc2d1	staré
umění	umění	k1gNnSc2	umění
-	-	kIx~	-
ars	ars	k?	ars
veterum	veterum	k1gInSc1	veterum
a	a	k8xC	a
jako	jako	k8xS	jako
skladby	skladba	k1gFnSc2	skladba
Petra	Petr	k1gMnSc2	Petr
de	de	k?	de
Cruce	Cruce	k1gMnSc1	Cruce
uvádí	uvádět	k5eAaImIp3nS	uvádět
tříhlasá	tříhlasý	k2eAgNnPc4d1	tříhlasé
moteta	moteto	k1gNnPc4	moteto
Au	au	k0	au
renouveler	renouveler	k1gMnSc1	renouveler
a	a	k8xC	a
Aucun	Aucun	k1gMnSc1	Aucun
ont	ont	k?	ont
trouvé	trouvá	k1gFnSc2	trouvá
chant	chanto	k1gNnPc2	chanto
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
poznamenána	poznamenán	k2eAgNnPc1d1	poznamenáno
zesíleně	zesíleně	k6eAd1	zesíleně
vystupující	vystupující	k2eAgFnSc7d1	vystupující
melismatikou	melismatika	k1gFnSc7	melismatika
v	v	k7c6	v
duplu	dupl	k1gInSc6	dupl
<g/>
,	,	kIx,	,
zdůraznění	zdůraznění	k1gNnSc3	zdůraznění
terciových	terciový	k2eAgInPc2d1	terciový
a	a	k8xC	a
sextových	sextový	k2eAgInPc2d1	sextový
souzvuků	souzvuk	k1gInPc2	souzvuk
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
perfectia	perfectium	k1gNnSc2	perfectium
a	a	k8xC	a
převažující	převažující	k2eAgFnSc1d1	převažující
role	role	k1gFnSc1	role
tripla	tripnout	k5eAaPmAgFnS	tripnout
sestávajícího	sestávající	k2eAgMnSc4d1	sestávající
z	z	k7c2	z
rytmicky	rytmicky	k6eAd1	rytmicky
volně	volně	k6eAd1	volně
zasazených	zasazený	k2eAgFnPc2d1	zasazená
not	nota	k1gFnPc2	nota
malých	malý	k2eAgFnPc2d1	malá
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Hojné	hojný	k2eAgNnSc1d1	hojné
užití	užití	k1gNnSc1	užití
krátkých	krátký	k2eAgFnPc2d1	krátká
not	nota	k1gFnPc2	nota
sebou	se	k3xPyFc7	se
neslo	nést	k5eAaImAgNnS	nést
následné	následný	k2eAgNnSc4d1	následné
prodloužení	prodloužení	k1gNnSc4	prodloužení
trvání	trvání	k1gNnSc2	trvání
noty	nota	k1gFnSc2	nota
brevis	brevis	k1gFnSc2	brevis
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
stylu	styl	k1gInSc2	styl
Petra	Petr	k1gMnSc4	Petr
de	de	k?	de
Cruce	Cruce	k1gMnSc4	Cruce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
zpomalení	zpomalení	k1gNnSc1	zpomalení
tempa	tempo	k1gNnSc2	tempo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
poli	pole	k1gNnSc6	pole
hudební	hudební	k2eAgFnSc2d1	hudební
teorie	teorie	k1gFnSc2	teorie
představují	představovat	k5eAaImIp3nP	představovat
spisy	spis	k1gInPc4	spis
Petra	Petr	k1gMnSc2	Petr
de	de	k?	de
Cruce	Cruce	k1gMnSc2	Cruce
o	o	k7c6	o
mensurální	mensurální	k2eAgFnSc6d1	mensurální
notaci	notace	k1gFnSc6	notace
přechod	přechod	k1gInSc4	přechod
mezi	mezi	k7c7	mezi
Frankem	frank	k1gInSc7	frank
Kolínským	kolínský	k2eAgInSc7d1	kolínský
a	a	k8xC	a
Filipem	Filip	k1gMnSc7	Filip
z	z	k7c2	z
Vitry	Vitra	k1gFnSc2	Vitra
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
brevis	brevis	k1gFnSc1	brevis
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
2	[number]	k4	2
až	až	k9	až
9	[number]	k4	9
semibreves	semibrevesa	k1gFnPc2	semibrevesa
a	a	k8xC	a
k	k	k7c3	k
jasnému	jasný	k2eAgNnSc3d1	jasné
vymezení	vymezení	k1gNnSc3	vymezení
skupinek	skupinka	k1gFnPc2	skupinka
not	nota	k1gFnPc2	nota
používal	používat	k5eAaImAgInS	používat
dělící	dělící	k2eAgNnSc4d1	dělící
znaménko	znaménko	k1gNnSc4	znaménko
punctus	punctus	k1gMnSc1	punctus
divisionis	divisionis	k1gFnSc1	divisionis
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
novinka	novinka	k1gFnSc1	novinka
znamenala	znamenat	k5eAaImAgFnS	znamenat
milník	milník	k1gInSc4	milník
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
od	od	k7c2	od
modální	modální	k2eAgFnSc2d1	modální
k	k	k7c3	k
mensurální	mensurální	k2eAgFnSc3d1	mensurální
rytmice	rytmika	k1gFnSc3	rytmika
italského	italský	k2eAgNnSc2d1	italské
trecenta	trecento	k1gNnSc2	trecento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Edice	edice	k1gFnSc2	edice
==	==	k?	==
</s>
</p>
<p>
<s>
Yvonne	Yvonnout	k5eAaImIp3nS	Yvonnout
Rokseth	Rokseth	k1gInSc1	Rokseth
(	(	kIx(	(
<g/>
Hrg	Hrg	k1gFnSc1	Hrg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Polyphonies	Polyphonies	k1gInSc1	Polyphonies
du	du	k?	du
XIIIe	XIIIe	k1gInSc1	XIIIe
siè	siè	k?	siè
<g/>
,	,	kIx,	,
4	[number]	k4	4
sv.	sv.	kA	sv.
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
1935	[number]	k4	1935
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
Codex	Codex	k1gInSc1	Codex
Montpellier	Montpellier	k1gInSc1	Montpellier
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A.	A.	kA	A.
Auda	Auda	k1gFnSc1	Auda
(	(	kIx(	(
<g/>
Hrg	Hrg	k1gFnSc1	Hrg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Les	les	k1gInSc1	les
"	"	kIx"	"
<g/>
Motets	Motets	k1gInSc1	Motets
wallons	wallons	k1gInSc1	wallons
<g/>
"	"	kIx"	"
du	du	k?	du
ms.	ms.	k?	ms.
de	de	k?	de
Turin	Turin	k1gInSc1	Turin
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
Brusel	Brusel	k1gInSc1	Brusel
1953	[number]	k4	1953
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Willi	Wille	k1gFnSc3	Wille
Apel	apel	k1gInSc1	apel
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Notation	Notation	k1gInSc1	Notation
of	of	k?	of
Polyphonic	Polyphonice	k1gFnPc2	Polyphonice
Music	Musice	k1gInPc2	Musice
<g/>
,	,	kIx,	,
900	[number]	k4	900
<g/>
–	–	k?	–
<g/>
1600	[number]	k4	1600
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc1	Cambridge
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
Leipzig	Leipzig	k1gInSc1	Leipzig
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
S.	S.	kA	S.
B.	B.	kA	B.
Patrick	Patrick	k1gMnSc1	Patrick
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Definition	Definition	k1gInSc1	Definition
<g/>
,	,	kIx,	,
Dissemination	Dissemination	k1gInSc1	Dissemination
and	and	k?	and
Description	Description	k1gInSc1	Description
of	of	k?	of
Petronian	Petronian	k1gInSc1	Petronian
Notation	Notation	k1gInSc1	Notation
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
North	North	k1gInSc1	North
Carolina	Carolina	k1gFnSc1	Carolina
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
</s>
</p>
<p>
<s>
Medieval	medieval	k1gInSc1	medieval
France	Franc	k1gMnSc2	Franc
<g/>
:	:	kIx,	:
an	an	k?	an
encyclopedia	encyclopedium	k1gNnPc1	encyclopedium
Von	von	k1gInSc1	von
William	William	k1gInSc4	William
W.	W.	kA	W.
Kibler	Kibler	k1gInSc1	Kibler
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
737	[number]	k4	737
google	google	k1gNnPc2	google
books	booksa	k1gFnPc2	booksa
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Literatura	literatura	k1gFnSc1	literatura
od	od	k7c2	od
a	a	k8xC	a
o	o	k7c4	o
Petrovi	Petr	k1gMnSc3	Petr
de	de	k?	de
Cruce	Cruce	k1gMnSc1	Cruce
<g/>
,	,	kIx,	,
Deutsche	Deutsche	k1gFnSc1	Deutsche
Nationalbibliothek	Nationalbibliothek	k6eAd1	Nationalbibliothek
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Petrus	Petrus	k1gMnSc1	Petrus
de	de	k?	de
Cruce	Cruce	k1gMnSc1	Cruce
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
