<s>
Jeřáb	jeřáb	k1gInSc1	jeřáb
oskeruše	oskeruše	k1gFnSc2	oskeruše
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
oskoruše	oskoruše	k1gFnSc1	oskoruše
<g/>
,	,	kIx,	,
nářečně	nářečně	k6eAd1	nářečně
oskoruša	oskoruša	k6eAd1	oskoruša
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Sorbus	Sorbus	k1gMnSc1	Sorbus
domestica	domestica	k1gMnSc1	domestica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
opadavý	opadavý	k2eAgMnSc1d1	opadavý
<g/>
,	,	kIx,	,
listnatý	listnatý	k2eAgInSc1d1	listnatý
ovocný	ovocný	k2eAgInSc1d1	ovocný
strom	strom	k1gInSc1	strom
dorůstající	dorůstající	k2eAgFnSc2d1	dorůstající
výšky	výška	k1gFnSc2	výška
až	až	k9	až
15	[number]	k4	15
m	m	kA	m
jako	jako	k8xS	jako
solitér	solitér	k1gInSc4	solitér
nebo	nebo	k8xC	nebo
až	až	k9	až
30	[number]	k4	30
m	m	kA	m
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
podmínkách	podmínka	k1gFnPc6	podmínka
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
300	[number]	k4	300
až	až	k9	až
500	[number]	k4	500
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
až	až	k9	až
600	[number]	k4	600
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
dřevina	dřevina	k1gFnSc1	dřevina
vzácná	vzácný	k2eAgFnSc1d1	vzácná
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
oskeruše	oskeruše	k1gFnSc2	oskeruše
je	být	k5eAaImIp3nS	být
všeslovanský	všeslovanský	k2eAgMnSc1d1	všeslovanský
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejasného	jasný	k2eNgMnSc4d1	nejasný
<g/>
,	,	kIx,	,
asi	asi	k9	asi
již	již	k6eAd1	již
praevropského	praevropský	k2eAgInSc2d1	praevropský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Oskeruše	oskeruše	k1gFnSc1	oskeruše
má	mít	k5eAaImIp3nS	mít
lysé	lysý	k2eAgInPc4d1	lysý
a	a	k8xC	a
lepkavé	lepkavý	k2eAgInPc4d1	lepkavý
pupeny	pupen	k1gInPc4	pupen
hnědé	hnědý	k2eAgInPc4d1	hnědý
až	až	k9	až
hnědozelené	hnědozelený	k2eAgFnSc2d1	hnědozelená
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
střídavé	střídavý	k2eAgMnPc4d1	střídavý
<g/>
,	,	kIx,	,
lichospeřené	lichospeřený	k2eAgMnPc4d1	lichospeřený
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
13	[number]	k4	13
až	až	k9	až
25	[number]	k4	25
cm	cm	kA	cm
a	a	k8xC	a
skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
13	[number]	k4	13
až	až	k9	až
21	[number]	k4	21
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
lístku	lístek	k1gInSc2	lístek
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
až	až	k9	až
6	[number]	k4	6
cm	cm	kA	cm
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
cm	cm	kA	cm
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
podlouhle	podlouhle	k6eAd1	podlouhle
eliptický	eliptický	k2eAgInSc1d1	eliptický
nebo	nebo	k8xC	nebo
oválný	oválný	k2eAgInSc1d1	oválný
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
jednoduše	jednoduše	k6eAd1	jednoduše
pilovitá	pilovitý	k2eAgFnSc1d1	pilovitá
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
celokrajná	celokrajný	k2eAgFnSc1d1	celokrajná
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgInPc1d1	mladý
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
bíle	bíle	k6eAd1	bíle
ochlupené	ochlupený	k2eAgInPc1d1	ochlupený
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
ochlupení	ochlupení	k1gNnSc1	ochlupení
později	pozdě	k6eAd2	pozdě
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
.	.	kIx.	.
</s>
<s>
Květenstvím	květenství	k1gNnSc7	květenství
je	být	k5eAaImIp3nS	být
chocholík	chocholík	k1gInSc4	chocholík
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
růžové	růžový	k2eAgFnPc1d1	růžová
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Kvete	kvést	k5eAaImIp3nS	kvést
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k8xS	až
červnu	červen	k1gInSc3	červen
<g/>
,	,	kIx,	,
květy	květ	k1gInPc4	květ
odkvetou	odkvést	k5eAaPmIp3nP	odkvést
již	již	k6eAd1	již
za	za	k7c4	za
14	[number]	k4	14
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mladých	mladý	k2eAgInPc2d1	mladý
stromků	stromek	k1gInPc2	stromek
je	být	k5eAaImIp3nS	být
kůra	kůra	k1gFnSc1	kůra
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
po	po	k7c6	po
7	[number]	k4	7
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
šupinatá	šupinatý	k2eAgFnSc1d1	šupinatá
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
brázditá	brázditý	k2eAgFnSc1d1	brázditá
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
kulovitá	kulovitý	k2eAgFnSc1d1	kulovitá
<g/>
,	,	kIx,	,
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
většinou	většinou	k6eAd1	většinou
vodorovně	vodorovně	k6eAd1	vodorovně
rostoucími	rostoucí	k2eAgFnPc7d1	rostoucí
nebo	nebo	k8xC	nebo
lehce	lehko	k6eAd1	lehko
vzhůru	vzhůru	k6eAd1	vzhůru
směřujícími	směřující	k2eAgFnPc7d1	směřující
větvemi	větev	k1gFnPc7	větev
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
koruny	koruna	k1gFnSc2	koruna
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
strom	strom	k1gInSc4	strom
solitér	solitéra	k1gFnPc2	solitéra
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
solitéru	solitér	k1gInSc2	solitér
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
její	její	k3xOp3gFnSc1	její
šířka	šířka	k1gFnSc1	šířka
až	až	k9	až
20	[number]	k4	20
m	m	kA	m
<g/>
,	,	kIx,	,
u	u	k7c2	u
stromů	strom	k1gInPc2	strom
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
v	v	k7c6	v
lese	les	k1gInSc6	les
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
podmínkách	podmínka	k1gFnPc6	podmínka
se	se	k3xPyFc4	se
oskeruše	oskeruše	k1gFnSc1	oskeruše
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
dubovém	dubový	k2eAgInSc6d1	dubový
a	a	k8xC	a
bukodubovém	bukodubový	k2eAgInSc6d1	bukodubový
vegetačním	vegetační	k2eAgInSc6d1	vegetační
stupni	stupeň	k1gInSc6	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Běžný	běžný	k2eAgInSc1d1	běžný
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
od	od	k7c2	od
160	[number]	k4	160
do	do	k7c2	do
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Oskeruše	oskeruše	k1gFnSc1	oskeruše
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
jako	jako	k9	jako
solitér	solitér	k1gInSc4	solitér
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
krajině	krajina	k1gFnSc6	krajina
–	–	k?	–
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
výšku	výška	k1gFnSc4	výška
asi	asi	k9	asi
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
koruny	koruna	k1gFnSc2	koruna
až	až	k9	až
20	[number]	k4	20
m	m	kA	m
–	–	k?	–
nebo	nebo	k8xC	nebo
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
krajině	krajina	k1gFnSc6	krajina
–	–	k?	–
její	její	k3xOp3gInSc4	její
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
okolních	okolní	k2eAgInPc2d1	okolní
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc7d1	další
odlišností	odlišnost	k1gFnSc7	odlišnost
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Oskeruše	oskeruše	k1gFnSc1	oskeruše
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
teplomilná	teplomilný	k2eAgFnSc1d1	teplomilná
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
vzrostlé	vzrostlý	k2eAgInPc1d1	vzrostlý
stromy	strom	k1gInPc1	strom
jsou	být	k5eAaImIp3nP	být
mrazuodolné	mrazuodolný	k2eAgInPc1d1	mrazuodolný
(	(	kIx(	(
<g/>
vydrží	vydržet	k5eAaPmIp3nP	vydržet
teploty	teplota	k1gFnPc1	teplota
do	do	k7c2	do
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
prosvětleným	prosvětlený	k2eAgInPc3d1	prosvětlený
suchým	suchý	k2eAgInPc3d1	suchý
a	a	k8xC	a
málo	málo	k6eAd1	málo
vlhkým	vlhký	k2eAgNnPc3d1	vlhké
stanovištím	stanoviště	k1gNnPc3	stanoviště
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
bohatá	bohatý	k2eAgNnPc1d1	bohaté
na	na	k7c4	na
živiny	živina	k1gFnPc4	živina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odolná	odolný	k2eAgFnSc1d1	odolná
proti	proti	k7c3	proti
exhalacím	exhalace	k1gFnPc3	exhalace
a	a	k8xC	a
smogu	smog	k1gInSc6	smog
<g/>
.	.	kIx.	.
</s>
<s>
Oskeruše	oskeruše	k1gFnSc1	oskeruše
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
Balkán	Balkán	k1gInSc1	Balkán
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
středním	střední	k2eAgNnSc6d1	střední
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nejsevernější	severní	k2eAgInSc1d3	nejsevernější
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Sasku-Anhaltsku	Sasku-Anhaltsek	k1gInSc6	Sasku-Anhaltsek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
několika	několik	k4yIc6	několik
lokalitách	lokalita	k1gFnPc6	lokalita
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
oskeruši	oskeruše	k1gFnSc4	oskeruše
i	i	k9	i
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
oskeruše	oskeruše	k1gFnSc1	oskeruše
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
a	a	k8xC	a
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovácku	Slovácko	k1gNnSc6	Slovácko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
od	od	k7c2	od
Pálavských	pálavský	k2eAgInPc2d1	pálavský
vrchů	vrch	k1gInPc2	vrch
po	po	k7c4	po
Vizovickou	vizovický	k2eAgFnSc4d1	Vizovická
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Původnost	původnost	k1gFnSc1	původnost
druhu	druh	k1gInSc2	druh
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
,	,	kIx,	,
snad	snad	k9	snad
jen	jen	k9	jen
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zčásti	zčásti	k6eAd1	zčásti
původní	původní	k2eAgMnSc1d1	původní
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
výskytů	výskyt	k1gInPc2	výskyt
je	být	k5eAaImIp3nS	být
však	však	k9	však
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
starých	starý	k2eAgFnPc2d1	stará
výsadeb	výsadba	k1gFnPc2	výsadba
<g/>
.	.	kIx.	.
</s>
<s>
Oskeruše	oskeruše	k1gFnSc1	oskeruše
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
dřevin	dřevina	k1gFnPc2	dřevina
–	–	k?	–
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
není	být	k5eNaImIp3nS	být
ani	ani	k9	ani
800	[number]	k4	800
vzrostlých	vzrostlý	k2eAgInPc2d1	vzrostlý
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
vysazují	vysazovat	k5eAaImIp3nP	vysazovat
tisíce	tisíc	k4xCgInPc1	tisíc
mladých	mladý	k2eAgMnPc2d1	mladý
stromků	stromek	k1gInPc2	stromek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plodného	plodný	k2eAgInSc2d1	plodný
věku	věk	k1gInSc2	věk
(	(	kIx(	(
<g/>
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dožije	dožít	k5eAaPmIp3nS	dožít
jen	jen	k9	jen
asi	asi	k9	asi
desetina	desetina	k1gFnSc1	desetina
<g/>
,	,	kIx,	,
a	a	k8xC	a
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
dožijí	dožít	k5eAaPmIp3nP	dožít
100	[number]	k4	100
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
Evropy	Evropa	k1gFnSc2	Evropa
–	–	k?	–
i	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
areál	areál	k1gInSc4	areál
oskeruše	oskeruše	k1gFnSc2	oskeruše
velký	velký	k2eAgInSc4d1	velký
<g/>
,	,	kIx,	,
nikde	nikde	k6eAd1	nikde
neexistuje	existovat	k5eNaImIp3nS	existovat
souvislý	souvislý	k2eAgInSc4d1	souvislý
porost	porost	k1gInSc4	porost
<g/>
,	,	kIx,	,
pohromadě	pohromadě	k6eAd1	pohromadě
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
jen	jen	k9	jen
skupinku	skupinka	k1gFnSc4	skupinka
několika	několik	k4yIc2	několik
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
oskeruše	oskeruše	k1gFnSc1	oskeruše
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
je	být	k5eAaImIp3nS	být
Adamcova	Adamcův	k2eAgFnSc1d1	Adamcova
oskeruše	oskeruše	k1gFnSc1	oskeruše
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
kopcem	kopec	k1gInSc7	kopec
Žerotín	Žerotína	k1gFnPc2	Žerotína
asi	asi	k9	asi
3	[number]	k4	3
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Strážnice	Strážnice	k1gFnSc2	Strážnice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obvodem	obvod	k1gInSc7	obvod
kmene	kmen	k1gInSc2	kmen
462	[number]	k4	462
cm	cm	kA	cm
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
největší	veliký	k2eAgFnSc4d3	veliký
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
vysoká	vysoká	k1gFnSc1	vysoká
11	[number]	k4	11
a	a	k8xC	a
široká	široký	k2eAgFnSc1d1	široká
18	[number]	k4	18
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc1	stáří
je	být	k5eAaImIp3nS	být
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
na	na	k7c4	na
400	[number]	k4	400
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
roste	růst	k5eAaImIp3nS	růst
největší	veliký	k2eAgFnSc1d3	veliký
oskeruše	oskeruše	k1gFnSc1	oskeruše
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Jenčice	Jenčice	k1gFnSc2	Jenčice
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
obvod	obvod	k1gInSc1	obvod
kmene	kmen	k1gInSc2	kmen
390	[number]	k4	390
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
má	mít	k5eAaImIp3nS	mít
ratolesti	ratolest	k1gFnPc4	ratolest
oskeruše	oskeruše	k1gFnPc4	oskeruše
i	i	k9	i
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Oskeruše	oskeruše	k1gFnSc1	oskeruše
je	být	k5eAaImIp3nS	být
jednodomá	jednodomý	k2eAgFnSc1d1	jednodomá
dřevina	dřevina	k1gFnSc1	dřevina
schopná	schopný	k2eAgFnSc1d1	schopná
samoopylení	samoopylení	k1gNnPc2	samoopylení
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
semena	semeno	k1gNnPc1	semeno
mohla	moct	k5eAaImAgNnP	moct
vyklíčit	vyklíčit	k5eAaPmF	vyklíčit
<g/>
,	,	kIx,	,
musejí	muset	k5eAaImIp3nP	muset
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
projít	projít	k5eAaPmF	projít
zažívacím	zažívací	k2eAgNnSc7d1	zažívací
ústrojím	ústrojí	k1gNnSc7	ústrojí
ptáků	pták	k1gMnPc2	pták
nebo	nebo	k8xC	nebo
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Oskeruši	oskeruše	k1gFnSc4	oskeruše
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozmnožovat	rozmnožovat	k5eAaImF	rozmnožovat
i	i	k9	i
uměle	uměle	k6eAd1	uměle
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
metod	metoda	k1gFnPc2	metoda
umělého	umělý	k2eAgNnSc2d1	umělé
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
můžeme	moct	k5eAaImIp1nP	moct
uvést	uvést	k5eAaPmF	uvést
vegetativní	vegetativní	k2eAgFnPc4d1	vegetativní
metody	metoda	k1gFnPc4	metoda
roubování	roubování	k1gNnSc2	roubování
a	a	k8xC	a
odběr	odběr	k1gInSc1	odběr
kořenových	kořenový	k2eAgFnPc2d1	kořenová
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
bylinných	bylinný	k2eAgInPc2d1	bylinný
řízků	řízek	k1gInPc2	řízek
<g/>
.	.	kIx.	.
</s>
<s>
Nepříliš	příliš	k6eNd1	příliš
vhodný	vhodný	k2eAgMnSc1d1	vhodný
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
oskeruše	oskeruše	k1gFnSc2	oskeruše
inbreeding	inbreeding	k1gInSc1	inbreeding
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
menší	malý	k2eAgFnSc3d2	menší
životnosti	životnost	k1gFnSc3	životnost
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc3d2	veliký
náchylnosti	náchylnost	k1gFnSc3	náchylnost
na	na	k7c6	na
houbové	houbová	k1gFnSc6	houbová
choroby	choroba	k1gFnSc2	choroba
a	a	k8xC	a
menšímu	malý	k2eAgInSc3d2	menší
vzrůstu	vzrůst	k1gInSc3	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
přes	přes	k7c4	přes
malý	malý	k2eAgInSc4d1	malý
počet	počet	k1gInSc4	počet
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
malou	malý	k2eAgFnSc4d1	malá
hustotu	hustota	k1gFnSc4	hustota
rozšíření	rozšíření	k1gNnSc2	rozšíření
je	být	k5eAaImIp3nS	být
i	i	k9	i
u	u	k7c2	u
skupinek	skupinka	k1gFnPc2	skupinka
stromů	strom	k1gInPc2	strom
jejich	jejich	k3xOp3gFnSc1	jejich
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
genetická	genetický	k2eAgFnSc1d1	genetická
odlišnost	odlišnost	k1gFnSc1	odlišnost
celkem	celkem	k6eAd1	celkem
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Oskeruše	oskeruše	k1gFnSc1	oskeruše
začíná	začínat	k5eAaImIp3nS	začínat
plodit	plodit	k5eAaImF	plodit
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
většinou	většinou	k6eAd1	většinou
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
a	a	k8xC	a
opadávají	opadávat	k5eAaImIp3nP	opadávat
od	od	k7c2	od
září	září	k1gNnSc2	září
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
ovoce	ovoce	k1gNnSc2	ovoce
je	být	k5eAaImIp3nS	být
různé	různý	k2eAgNnSc1d1	různé
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vzrostlého	vzrostlý	k2eAgInSc2d1	vzrostlý
stromu	strom	k1gInSc2	strom
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
hodnoty	hodnota	k1gFnPc1	hodnota
od	od	k7c2	od
300	[number]	k4	300
do	do	k7c2	do
1200	[number]	k4	1200
kg	kg	kA	kg
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
solitérní	solitérní	k2eAgInPc1d1	solitérní
stromy	strom	k1gInPc1	strom
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
velké	velký	k2eAgFnSc3d1	velká
koruně	koruna	k1gFnSc3	koruna
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
úrodu	úroda	k1gFnSc4	úroda
bohatší	bohatý	k2eAgMnSc1d2	bohatší
než	než	k8xS	než
stromy	strom	k1gInPc1	strom
rostoucí	rostoucí	k2eAgInPc1d1	rostoucí
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
úroda	úroda	k1gFnSc1	úroda
ovoce	ovoce	k1gNnPc4	ovoce
však	však	k9	však
nebývá	bývat	k5eNaImIp3nS	bývat
každoroční	každoroční	k2eAgNnSc1d1	každoroční
<g/>
,	,	kIx,	,
některý	některý	k3yIgInSc4	některý
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
neurodí	urodit	k5eNaPmIp3nS	urodit
nic	nic	k3yNnSc1	nic
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
neúrodou	neúroda	k1gFnSc7	neúroda
oskeruší	oskeruše	k1gFnPc2	oskeruše
se	se	k3xPyFc4	se
často	často	k6eAd1	často
traduje	tradovat	k5eAaImIp3nS	tradovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
květ	květ	k1gInSc1	květ
oskeruše	oskeruše	k1gFnSc2	oskeruše
ozářen	ozářit	k5eAaPmNgInS	ozářit
bleskem	blesk	k1gInSc7	blesk
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
opadne	opadnout	k5eAaPmIp3nS	opadnout
a	a	k8xC	a
nedojde	dojít	k5eNaPmIp3nS	dojít
ke	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
byly	být	k5eAaImAgInP	být
spatřeny	spatřen	k2eAgInPc1d1	spatřen
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
byly	být	k5eAaImAgFnP	být
plody	plod	k1gInPc4	plod
jen	jen	k9	jen
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnPc1	druhý
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ozáření	ozáření	k1gNnSc2	ozáření
květů	květ	k1gInPc2	květ
bleskem	blesk	k1gInSc7	blesk
nebyly	být	k5eNaImAgInP	být
plody	plod	k1gInPc1	plod
žádné	žádný	k3yNgFnPc4	žádný
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
Vít	Vít	k1gMnSc1	Vít
Hrdoušek	Hrdoušek	k1gMnSc1	Hrdoušek
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Nutno	nutno	k6eAd1	nutno
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
samozřejmě	samozřejmě	k6eAd1	samozřejmě
o	o	k7c4	o
pověru	pověra	k1gFnSc4	pověra
nezakládající	zakládající	k2eNgFnSc4d1	nezakládající
se	se	k3xPyFc4	se
na	na	k7c6	na
pravdě	pravda	k1gFnSc6	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
oskeruše	oskeruše	k1gFnSc2	oskeruše
je	být	k5eAaImIp3nS	být
malvice	malvice	k1gFnSc1	malvice
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
plodů	plod	k1gInPc2	plod
je	být	k5eAaImIp3nS	být
různý	různý	k2eAgInSc1d1	různý
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kulovitý	kulovitý	k2eAgInSc1d1	kulovitý
<g/>
,	,	kIx,	,
hruškovitý	hruškovitý	k2eAgInSc1d1	hruškovitý
nebo	nebo	k8xC	nebo
i	i	k9	i
vejčitý	vejčitý	k2eAgMnSc1d1	vejčitý
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
žlutozelená	žlutozelený	k2eAgFnSc1d1	žlutozelená
až	až	k6eAd1	až
hnědozelená	hnědozelený	k2eAgFnSc1d1	hnědozelená
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
ke	k	k7c3	k
slunci	slunce	k1gNnSc3	slunce
oranžová	oranžový	k2eAgFnSc1d1	oranžová
až	až	k6eAd1	až
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
plodu	plod	k1gInSc2	plod
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
6	[number]	k4	6
až	až	k9	až
15	[number]	k4	15
g	g	kA	g
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc4	průměr
plodu	plod	k1gInSc2	plod
2,5	[number]	k4	2,5
až	až	k9	až
3,0	[number]	k4	3,0
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
plody	plod	k1gInPc1	plod
větší	veliký	k2eAgInPc1d2	veliký
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
až	až	k9	až
5	[number]	k4	5
cm	cm	kA	cm
a	a	k8xC	a
váhou	váha	k1gFnSc7	váha
přes	přes	k7c4	přes
25	[number]	k4	25
g.	g.	k?	g.
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
sladké	sladký	k2eAgInPc1d1	sladký
<g/>
,	,	kIx,	,
šťavnaté	šťavnatý	k2eAgInPc1d1	šťavnatý
a	a	k8xC	a
aromatické	aromatický	k2eAgInPc1d1	aromatický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
konzumaci	konzumace	k1gFnSc4	konzumace
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	on	k3xPp3gInPc4	on
nechat	nechat	k5eAaPmF	nechat
uležet	uležet	k5eAaPmF	uležet
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
zhnědnou	zhnědnout	k5eAaPmIp3nP	zhnědnout
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
zhniličkovatí	zhniličkovatět	k5eAaImIp3nP	zhniličkovatět
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
:	:	kIx,	:
cukry	cukr	k1gInPc1	cukr
bílkoviny	bílkovina	k1gFnSc2	bílkovina
pektiny	pektin	k1gInPc1	pektin
vitaminy	vitamin	k1gInPc1	vitamin
–	–	k?	–
např.	např.	kA	např.
vitamín	vitamín	k1gInSc4	vitamín
A	A	kA	A
<g/>
,	,	kIx,	,
C	C	kA	C
organické	organický	k2eAgFnSc2d1	organická
kyseliny	kyselina	k1gFnSc2	kyselina
minerály	minerál	k1gInPc1	minerál
tříslovinu	tříslovina	k1gFnSc4	tříslovina
(	(	kIx(	(
<g/>
tanin	tanin	k1gInSc4	tanin
<g/>
)	)	kIx)	)
Plody	plod	k1gInPc4	plod
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
po	po	k7c6	po
uležení	uležení	k1gNnSc6	uležení
konzumovat	konzumovat	k5eAaBmF	konzumovat
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
další	další	k2eAgFnSc2d1	další
úpravy	úprava	k1gFnSc2	úprava
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
chuťově	chuťově	k6eAd1	chuťově
výborné	výborný	k2eAgFnPc1d1	výborná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dělat	dělat	k5eAaImF	dělat
kompoty	kompot	k1gInPc4	kompot
<g/>
,	,	kIx,	,
marmelády	marmeláda	k1gFnPc4	marmeláda
či	či	k8xC	či
je	on	k3xPp3gFnPc4	on
usušit	usušit	k5eAaPmF	usušit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
umletí	umletí	k1gNnSc6	umletí
sušených	sušený	k2eAgFnPc2d1	sušená
oskeruší	oskeruše	k1gFnPc2	oskeruše
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
prášek	prášek	k1gInSc4	prášek
používat	používat	k5eAaImF	používat
k	k	k7c3	k
ochucování	ochucování	k1gNnSc3	ochucování
jídel	jídlo	k1gNnPc2	jídlo
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
příchuť	příchuť	k1gFnSc4	příchuť
skořice	skořice	k1gFnSc2	skořice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
konzervování	konzervování	k1gNnSc3	konzervování
jablečného	jablečný	k2eAgInSc2d1	jablečný
moštu	mošt	k1gInSc2	mošt
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
ovocných	ovocný	k2eAgFnPc2d1	ovocná
šťáv	šťáva	k1gFnPc2	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
výroba	výroba	k1gFnSc1	výroba
likérů	likér	k1gInPc2	likér
či	či	k8xC	či
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgFnPc1d1	kvalitní
pálenky	pálenka	k1gFnPc1	pálenka
–	–	k?	–
oskerušovice	oskerušovice	k1gFnSc1	oskerušovice
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
léčivé	léčivý	k2eAgInPc1d1	léčivý
účinky	účinek	k1gInPc1	účinek
<g/>
:	:	kIx,	:
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
střevních	střevní	k2eAgFnPc6d1	střevní
potížích	potíž	k1gFnPc6	potíž
(	(	kIx(	(
<g/>
sušené	sušený	k2eAgNnSc1d1	sušené
ovoce	ovoce	k1gNnSc1	ovoce
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
statikum	statikum	k?	statikum
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
syrové	syrový	k2eAgNnSc1d1	syrové
má	mít	k5eAaImIp3nS	mít
projímavé	projímavý	k2eAgInPc4d1	projímavý
účinky	účinek	k1gInPc4	účinek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
revma	revma	k1gNnSc6	revma
a	a	k8xC	a
při	při	k7c6	při
horečce	horečka	k1gFnSc6	horečka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
smísení	smísení	k1gNnSc2	smísení
oskerušovice	oskerušovice	k1gFnSc2	oskerušovice
s	s	k7c7	s
bylinkami	bylinka	k1gFnPc7	bylinka
a	a	k8xC	a
medem	med	k1gInSc7	med
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
léčit	léčit	k5eAaImF	léčit
i	i	k9	i
nachlazení	nachlazení	k1gNnSc4	nachlazení
<g/>
.	.	kIx.	.
</s>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
oskeruše	oskeruše	k1gFnSc2	oskeruše
je	být	k5eAaImIp3nS	být
nejtěžší	těžký	k2eAgNnSc1d3	nejtěžší
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pevné	pevný	k2eAgNnSc1d1	pevné
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
pěknou	pěkný	k2eAgFnSc4d1	pěkná
barvu	barva	k1gFnSc4	barva
i	i	k8xC	i
kresbu	kresba	k1gFnSc4	kresba
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
v	v	k7c6	v
truhlářství	truhlářství	k1gNnSc6	truhlářství
<g/>
,	,	kIx,	,
řezbářství	řezbářství	k1gNnSc4	řezbářství
<g/>
,	,	kIx,	,
používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
vinné	vinný	k2eAgInPc1d1	vinný
lisy	lis	k1gInPc1	lis
a	a	k8xC	a
mechanické	mechanický	k2eAgInPc1d1	mechanický
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
jako	jako	k9	jako
materiál	materiál	k1gInSc4	materiál
na	na	k7c4	na
ozdobné	ozdobný	k2eAgNnSc4d1	ozdobné
vykládání	vykládání	k1gNnSc4	vykládání
nábytku	nábytek	k1gInSc2	nábytek
(	(	kIx(	(
<g/>
intarzie	intarzie	k1gFnSc1	intarzie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
oskerušové	oskerušový	k2eAgNnSc1d1	oskerušový
dřevo	dřevo	k1gNnSc1	dřevo
zpracováváno	zpracováván	k2eAgNnSc1d1	zpracováváno
jen	jen	k9	jen
několika	několik	k4yIc2	několik
málo	málo	k4c4	málo
podniky	podnik	k1gInPc1	podnik
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
vzácností	vzácnost	k1gFnSc7	vzácnost
oskeruše	oskeruše	k1gFnSc2	oskeruše
<g/>
.	.	kIx.	.
</s>
<s>
Každopádně	každopádně	k6eAd1	každopádně
oskerušové	oskerušový	k2eAgNnSc1d1	oskerušový
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgMnSc1d1	kvalitní
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
pěstování	pěstování	k1gNnSc2	pěstování
oskeruší	oskeruše	k1gFnPc2	oskeruše
a	a	k8xC	a
využívání	využívání	k1gNnSc2	využívání
jejich	jejich	k3xOp3gNnSc2	jejich
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Oskeruše	oskeruše	k1gFnSc1	oskeruše
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
zčásti	zčásti	k6eAd1	zčásti
původním	původní	k2eAgInSc7d1	původní
druhem	druh	k1gInSc7	druh
teplých	teplý	k2eAgFnPc2d1	teplá
doubrav	doubrava	k1gFnPc2	doubrava
a	a	k8xC	a
dubohabřin	dubohabřina	k1gFnPc2	dubohabřina
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
Slovácku	Slovácko	k1gNnSc6	Slovácko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
její	její	k3xOp3gInSc1	její
výskyt	výskyt	k1gInSc1	výskyt
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
Rakousko	Rakousko	k1gNnSc4	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
s	s	k7c7	s
vinnou	vinný	k2eAgFnSc7d1	vinná
révou	réva	k1gFnSc7	réva
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
spoustu	spousta	k1gFnSc4	spousta
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
oskeruše	oskeruše	k1gFnSc1	oskeruše
dále	daleko	k6eAd2	daleko
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
moravské	moravský	k2eAgNnSc4d1	Moravské
a	a	k8xC	a
české	český	k2eAgNnSc4d1	české
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
lidové	lidový	k2eAgInPc4d1	lidový
názory	názor	k1gInPc4	názor
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
původ	původ	k1gInSc4	původ
patří	patřit	k5eAaImIp3nS	patřit
přinesení	přinesení	k1gNnSc1	přinesení
rostliny	rostlina	k1gFnSc2	rostlina
v	v	k7c6	v
době	doba	k1gFnSc6	doba
tureckých	turecký	k2eAgFnPc2d1	turecká
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
donesena	donést	k5eAaPmNgFnS	donést
z	z	k7c2	z
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
názorem	názor	k1gInSc7	názor
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
sem	sem	k6eAd1	sem
zanesli	zanést	k5eAaPmAgMnP	zanést
už	už	k6eAd1	už
staří	starý	k2eAgMnPc1d1	starý
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
vojska	vojsko	k1gNnPc4	vojsko
ji	on	k3xPp3gFnSc4	on
vysazovala	vysazovat	k5eAaImAgFnS	vysazovat
na	na	k7c6	na
dobytém	dobytý	k2eAgNnSc6d1	dobyté
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
sloužila	sloužit	k5eAaImAgFnS	sloužit
i	i	k9	i
jako	jako	k9	jako
obdoba	obdoba	k1gFnSc1	obdoba
hraničních	hraniční	k2eAgInPc2d1	hraniční
kamenů	kámen	k1gInPc2	kámen
(	(	kIx(	(
<g/>
Limes	Limes	k1gMnSc1	Limes
Romanus	Romanus	k1gMnSc1	Romanus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
nejstarší	starý	k2eAgFnSc1d3	nejstarší
oskeruše	oskeruše	k1gFnSc1	oskeruše
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
památnými	památný	k2eAgInPc7d1	památný
stromy	strom	k1gInPc7	strom
v	v	k7c6	v
CHKO	CHKO	kA	CHKO
Bílé	bílý	k2eAgInPc4d1	bílý
Karpaty	Karpaty	k1gInPc4	Karpaty
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
následující	následující	k2eAgFnPc4d1	následující
čtyři	čtyři	k4xCgFnPc4	čtyři
oskeruše	oskeruše	k1gFnPc4	oskeruše
<g/>
.	.	kIx.	.
</s>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
obvod	obvod	k1gInSc1	obvod
kmene	kmen	k1gInSc2	kmen
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1,3	[number]	k4	1,3
m	m	kA	m
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Adamcova	Adamcův	k2eAgFnSc1d1	Adamcova
oskeruše	oskeruše	k1gFnSc1	oskeruše
<g/>
.	.	kIx.	.
</s>
<s>
Katastr	katastr	k1gInSc1	katastr
<g/>
:	:	kIx,	:
Strážnice	Strážnice	k1gFnSc2	Strážnice
<g/>
,	,	kIx,	,
vinice	vinice	k1gFnSc2	vinice
Žerotín	Žerotína	k1gFnPc2	Žerotína
Obvod	obvod	k1gInSc1	obvod
kmene	kmen	k1gInSc2	kmen
<g/>
:	:	kIx,	:
458	[number]	k4	458
cm	cm	kA	cm
Stáří	stáří	k1gNnSc1	stáří
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
400	[number]	k4	400
let	léto	k1gNnPc2	léto
Výška	výška	k1gFnSc1	výška
koruny	koruna	k1gFnSc2	koruna
<g/>
:	:	kIx,	:
11	[number]	k4	11
m	m	kA	m
Šířka	šířka	k1gFnSc1	šířka
koruny	koruna	k1gFnSc2	koruna
<g/>
:	:	kIx,	:
18	[number]	k4	18
m	m	kA	m
Adamcova	Adamcův	k2eAgFnSc1d1	Adamcova
oskeruše	oskeruše	k1gFnSc1	oskeruše
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
oskeruší	oskeruše	k1gFnSc7	oskeruše
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
<g/>
.	.	kIx.	.
</s>
<s>
Katastr	katastr	k1gInSc1	katastr
<g/>
:	:	kIx,	:
Strážnice	Strážnice	k1gFnSc1	Strážnice
Obvod	obvod	k1gInSc1	obvod
kmene	kmen	k1gInSc2	kmen
<g/>
:	:	kIx,	:
337	[number]	k4	337
cm	cm	kA	cm
Stáří	stáří	k1gNnSc1	stáří
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
300	[number]	k4	300
let	léto	k1gNnPc2	léto
Katastr	katastr	k1gInSc1	katastr
<g/>
:	:	kIx,	:
Tvarožná	Tvarožný	k2eAgFnSc1d1	Tvarožná
Lhota	Lhota	k1gFnSc1	Lhota
Obvod	obvod	k1gInSc1	obvod
kmene	kmen	k1gInSc2	kmen
<g/>
:	:	kIx,	:
370	[number]	k4	370
cm	cm	kA	cm
Stáří	stáří	k1gNnSc1	stáří
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
300	[number]	k4	300
let	léto	k1gNnPc2	léto
Katastr	katastr	k1gInSc1	katastr
<g/>
:	:	kIx,	:
Kněždub	Kněždub	k1gMnSc1	Kněždub
Obvod	obvod	k1gInSc4	obvod
kmene	kmen	k1gInSc2	kmen
<g/>
:	:	kIx,	:
290	[number]	k4	290
cm	cm	kA	cm
Stáří	stáří	k1gNnSc1	stáří
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
250	[number]	k4	250
let	léto	k1gNnPc2	léto
Katastr	katastr	k1gInSc1	katastr
<g/>
:	:	kIx,	:
Polichno	Polichno	k6eAd1	Polichno
Obvod	obvod	k1gInSc1	obvod
kmene	kmen	k1gInSc2	kmen
<g/>
:	:	kIx,	:
328	[number]	k4	328
cm	cm	kA	cm
Stáří	stáří	k1gNnSc1	stáří
(	(	kIx(	(
<g/>
odhad	odhad	k1gInSc1	odhad
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
275	[number]	k4	275
let	léto	k1gNnPc2	léto
</s>
