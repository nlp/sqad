<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ačkoli	ačkoli	k8xS	ačkoli
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
bude	být	k5eAaImBp3nS	být
vydán	vydat	k5eAaPmNgInS	vydat
až	až	k9	až
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2115	[number]	k4	2115
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
100	[number]	k4	100
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
?	?	kIx.	?
</s>
