<p>
<s>
100	[number]	k4	100
Years	Years	k1gInSc1	Years
je	být	k5eAaImIp3nS	být
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
francouzský	francouzský	k2eAgInSc1d1	francouzský
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Robert	Robert	k1gMnSc1	Robert
Rodriguez	Rodriguez	k1gMnSc1	Rodriguez
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Johna	John	k1gMnSc2	John
Malkoviche	Malkovich	k1gMnSc2	Malkovich
<g/>
.	.	kIx.	.
</s>
<s>
Vydán	vydán	k2eAgMnSc1d1	vydán
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2115	[number]	k4	2115
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Malkovich	Malkovich	k1gMnSc1	Malkovich
jako	jako	k8xS	jako
hrdina	hrdina	k1gMnSc1	hrdina
</s>
</p>
<p>
<s>
Shuya	Shuya	k1gMnSc1	Shuya
Chang	Chang	k1gMnSc1	Chang
jako	jako	k8xS	jako
hrdinka	hrdinka	k1gFnSc1	hrdinka
</s>
</p>
<p>
<s>
Marko	Marko	k1gMnSc1	Marko
Zaror	Zaror	k1gMnSc1	Zaror
jako	jako	k8xS	jako
padouch	padouch	k1gMnSc1	padouch
</s>
</p>
<p>
<s>
==	==	k?	==
Produkce	produkce	k1gFnSc1	produkce
a	a	k8xC	a
vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
oznámili	oznámit	k5eAaPmAgMnP	oznámit
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
John	John	k1gMnSc1	John
Malkovich	Malkovich	k1gMnSc1	Malkovich
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
Robert	Robert	k1gMnSc1	Robert
Rodriguez	Rodriguez	k1gMnSc1	Rodriguez
<g/>
,	,	kIx,	,
že	že	k8xS	že
společně	společně	k6eAd1	společně
se	s	k7c7	s
značkou	značka	k1gFnSc7	značka
koňaku	koňak	k1gInSc2	koňak
Louis	Louis	k1gMnSc1	Louis
XIII	XIII	kA	XIII
<g/>
,	,	kIx,	,
vlastněnou	vlastněný	k2eAgFnSc7d1	vlastněná
společností	společnost	k1gFnSc7	společnost
Rémy	Réma	k1gFnSc2	Réma
Martin	Martin	k1gInSc1	Martin
<g/>
,	,	kIx,	,
natočili	natočit	k5eAaBmAgMnP	natočit
film	film	k1gInSc4	film
inspirovaný	inspirovaný	k2eAgInSc4d1	inspirovaný
stovkou	stovka	k1gFnSc7	stovka
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
trvá	trvat	k5eAaImIp3nS	trvat
zrání	zrání	k1gNnSc4	zrání
jedné	jeden	k4xCgFnSc2	jeden
láhve	láhev	k1gFnSc2	láhev
tohoto	tento	k3xDgInSc2	tento
nápoje	nápoj	k1gInSc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc4	děj
snímku	snímek	k1gInSc2	snímek
zůstal	zůstat	k5eAaPmAgMnS	zůstat
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
vydali	vydat	k5eAaPmAgMnP	vydat
autoři	autor	k1gMnPc1	autor
filmu	film	k1gInSc6	film
tři	tři	k4xCgFnPc1	tři
teasery	teasera	k1gFnPc1	teasera
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
jednu	jeden	k4xCgFnSc4	jeden
scénu	scéna	k1gFnSc4	scéna
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
možných	možný	k2eAgFnPc6d1	možná
budoucnostech	budoucnost	k1gFnPc6	budoucnost
<g/>
,	,	kIx,	,
od	od	k7c2	od
dystopické	dystopický	k2eAgFnSc2d1	dystopická
pustiny	pustina	k1gFnSc2	pustina
po	po	k7c4	po
technologický	technologický	k2eAgInSc4d1	technologický
ráj	ráj	k1gInSc4	ráj
<g/>
.	.	kIx.	.
<g/>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
trezoru	trezor	k1gInSc2	trezor
s	s	k7c7	s
neprůstřelným	průstřelný	k2eNgNnSc7d1	neprůstřelné
sklem	sklo	k1gNnSc7	sklo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
automaticky	automaticky	k6eAd1	automaticky
otevřít	otevřít	k5eAaPmF	otevřít
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2115	[number]	k4	2115
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stane	stanout	k5eAaPmIp3nS	stanout
dnem	den	k1gInSc7	den
premiéry	premiéra	k1gFnSc2	premiéra
snímku	snímek	k1gInSc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Pozvánku	pozvánka	k1gFnSc4	pozvánka
na	na	k7c4	na
premiérové	premiérový	k2eAgNnSc4d1	premiérové
promítání	promítání	k1gNnSc4	promítání
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
i	i	k9	i
pro	pro	k7c4	pro
potomky	potomek	k1gMnPc4	potomek
<g/>
,	,	kIx,	,
dostalo	dostat	k5eAaPmAgNnS	dostat
1000	[number]	k4	1000
osob	osoba	k1gFnPc2	osoba
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Rodrigueze	Rodrigueze	k1gFnSc2	Rodrigueze
a	a	k8xC	a
Malkoviche	Malkovich	k1gFnSc2	Malkovich
<g/>
.	.	kIx.	.
</s>
<s>
Sejf	sejf	k1gInSc1	sejf
s	s	k7c7	s
filmem	film	k1gInSc7	film
byl	být	k5eAaImAgInS	být
ukázán	ukázat	k5eAaPmNgInS	ukázat
v	v	k7c6	v
několika	několik	k4yIc6	několik
různých	různý	k2eAgInPc6d1	různý
městech	město	k1gNnPc6	město
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
sklepů	sklep	k1gInPc2	sklep
značky	značka	k1gFnSc2	značka
Louis	Louis	k1gMnSc1	Louis
XIII	XIII	kA	XIII
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Cognacu	Cognacus	k1gInSc6	Cognacus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
100	[number]	k4	100
Years	Years	k1gInSc1	Years
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
100	[number]	k4	100
Years	Years	k1gInSc1	Years
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
100	[number]	k4	100
Years	Years	k1gInSc1	Years
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
