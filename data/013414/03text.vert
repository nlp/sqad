<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1946	[number]	k4	1946
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
hokejový	hokejový	k2eAgMnSc1d1	hokejový
útočník	útočník	k1gMnSc1	útočník
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
trenér	trenér	k1gMnSc1	trenér
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hráč	hráč	k1gMnSc1	hráč
získal	získat	k5eAaPmAgMnS	získat
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
Duklou	Dukla	k1gFnSc7	Dukla
Jihlava	Jihlava	k1gFnSc1	Jihlava
osmkrát	osmkrát	k6eAd1	osmkrát
titul	titul	k1gInSc4	titul
mistra	mistr	k1gMnSc2	mistr
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Úspěchy	úspěch	k1gInPc4	úspěch
slavil	slavit	k5eAaImAgInS	slavit
i	i	k9	i
jako	jako	k9	jako
trenér	trenér	k1gMnSc1	trenér
–	–	k?	–
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
soutěži	soutěž	k1gFnSc6	soutěž
dovedl	dovést	k5eAaPmAgMnS	dovést
k	k	k7c3	k
titulu	titul	k1gInSc3	titul
Jihlavu	Jihlava	k1gFnSc4	Jihlava
a	a	k8xC	a
Olomouc	Olomouc	k1gFnSc4	Olomouc
<g/>
,	,	kIx,	,
s	s	k7c7	s
reprezentačním	reprezentační	k2eAgInSc7d1	reprezentační
celkem	celek	k1gInSc7	celek
pak	pak	k6eAd1	pak
získal	získat	k5eAaPmAgMnS	získat
dvě	dva	k4xCgFnPc4	dva
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
kouč	kouč	k1gMnSc1	kouč
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
trenéra	trenér	k1gMnSc2	trenér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
českého	český	k2eAgInSc2d1	český
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráčská	hráčský	k2eAgFnSc1d1	hráčská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Dukle	Dukla	k1gFnSc6	Dukla
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
získal	získat	k5eAaPmAgMnS	získat
mistrovské	mistrovský	k2eAgInPc4d1	mistrovský
tituly	titul	k1gInPc4	titul
v	v	k7c6	v
letech	let	k1gInPc6	let
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
a	a	k8xC	a
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
československou	československý	k2eAgFnSc4d1	Československá
reprezentaci	reprezentace	k1gFnSc4	reprezentace
hrál	hrát	k5eAaImAgInS	hrát
pravidelně	pravidelně	k6eAd1	pravidelně
od	od	k7c2	od
konce	konec	k1gInSc2	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
do	do	k7c2	do
konce	konec	k1gInSc2	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pomohl	pomoct	k5eAaPmAgMnS	pomoct
vybojovat	vybojovat	k5eAaPmF	vybojovat
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
šampionátech	šampionát	k1gInPc6	šampionát
tři	tři	k4xCgFnPc4	tři
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Startoval	startovat	k5eAaBmAgMnS	startovat
také	také	k9	také
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
národní	národní	k2eAgInSc1d1	národní
tým	tým	k1gInSc1	tým
získal	získat	k5eAaPmAgInS	získat
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
ukončil	ukončit	k5eAaPmAgMnS	ukončit
studia	studio	k1gNnSc2	studio
oboru	obor	k1gInSc2	obor
Učitelství	učitelství	k1gNnSc2	učitelství
všeobecně	všeobecně	k6eAd1	všeobecně
vzdělávacích	vzdělávací	k2eAgInPc2d1	vzdělávací
předmětů	předmět	k1gInPc2	předmět
zeměpis-tělesná	zeměpisělesný	k2eAgFnSc1d1	zeměpis-tělesný
výchova	výchova	k1gFnSc1	výchova
na	na	k7c6	na
Pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
fakultě	fakulta	k1gFnSc6	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Trenérská	trenérský	k2eAgFnSc1d1	trenérská
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
hráčské	hráčský	k2eAgFnSc2d1	hráčská
kariéry	kariéra	k1gFnSc2	kariéra
byl	být	k5eAaImAgInS	být
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
trenérem	trenér	k1gMnSc7	trenér
Dukly	Dukla	k1gFnSc2	Dukla
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yIgFnSc4	který
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
i	i	k8xC	i
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Zkušenosti	zkušenost	k1gFnPc4	zkušenost
prokázal	prokázat	k5eAaPmAgMnS	prokázat
i	i	k9	i
v	v	k7c6	v
první	první	k4xOgFnSc6	první
extraligové	extraligový	k2eAgFnSc6d1	extraligová
sezoně	sezona	k1gFnSc6	sezona
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
kde	kde	k6eAd1	kde
společně	společně	k6eAd1	společně
s	s	k7c7	s
Milošem	Miloš	k1gMnSc7	Miloš
Říhou	Říha	k1gMnSc7	Říha
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
"	"	kIx"	"
<g/>
federálním	federální	k2eAgInSc6d1	federální
ročníku	ročník	k1gInSc6	ročník
<g/>
"	"	kIx"	"
obsadili	obsadit	k5eAaPmAgMnP	obsadit
střed	střed	k1gInSc4	střed
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
českou	český	k2eAgFnSc4d1	Česká
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
sezonu	sezona	k1gFnSc4	sezona
1993	[number]	k4	1993
<g/>
/	/	kIx~	/
<g/>
1994	[number]	k4	1994
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
a	a	k8xC	a
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
průběhu	průběh	k1gInSc6	průběh
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
HC	HC	kA	HC
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Hanáky	Hanák	k1gMnPc4	Hanák
tehdy	tehdy	k6eAd1	tehdy
vyvedl	vyvést	k5eAaPmAgMnS	vyvést
z	z	k7c2	z
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
je	být	k5eAaImIp3nS	být
i	i	k9	i
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
play	play	k0	play
off	off	k?	off
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těch	ten	k3xDgNnPc6	ten
postupně	postupně	k6eAd1	postupně
porazili	porazit	k5eAaPmAgMnP	porazit
České	český	k2eAgInPc4d1	český
Budějovice	Budějovice	k1gInPc4	Budějovice
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vítěze	vítěz	k1gMnSc4	vítěz
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
extraligy	extraliga	k1gFnSc2	extraliga
Poldi	Poldi	k1gFnSc2	Poldi
Kladno	Kladno	k1gNnSc1	Kladno
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
tak	tak	k8xC	tak
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
o	o	k7c4	o
překvapivě	překvapivě	k6eAd1	překvapivě
dovedl	dovést	k5eAaPmAgMnS	dovést
k	k	k7c3	k
mistrovskému	mistrovský	k2eAgInSc3d1	mistrovský
titulu	titul	k1gInSc3	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
trenér	trenér	k1gMnSc1	trenér
vedl	vést	k5eAaImAgMnS	vést
českou	český	k2eAgFnSc4d1	Česká
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
reprezentaci	reprezentace	k1gFnSc4	reprezentace
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
společně	společně	k6eAd1	společně
s	s	k7c7	s
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Martincem	Martinec	k1gMnSc7	Martinec
asistoval	asistovat	k5eAaImAgMnS	asistovat
Ivanu	Ivan	k1gMnSc3	Ivan
Hlinkovi	Hlinka	k1gMnSc3	Hlinka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
hlavního	hlavní	k2eAgMnSc2d1	hlavní
trenéra	trenér	k1gMnSc2	trenér
působil	působit	k5eAaImAgMnS	působit
u	u	k7c2	u
českého	český	k2eAgInSc2d1	český
národního	národní	k2eAgInSc2d1	národní
týmu	tým	k1gInSc2	tým
na	na	k7c6	na
mistrovstvích	mistrovství	k1gNnPc6	mistrovství
světa	svět	k1gInSc2	svět
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
a	a	k8xC	a
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
koučem	kouč	k1gMnSc7	kouč
byl	být	k5eAaImAgMnS	být
i	i	k9	i
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Zlaté	zlatý	k2eAgFnPc1d1	zlatá
medaile	medaile	k1gFnPc1	medaile
s	s	k7c7	s
reprezentací	reprezentace	k1gFnSc7	reprezentace
získal	získat	k5eAaPmAgInS	získat
na	na	k7c4	na
MS	MS	kA	MS
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
a	a	k8xC	a
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2015	[number]	k4	2015
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
rakovina	rakovina	k1gFnSc1	rakovina
slinivky	slinivka	k1gFnSc2	slinivka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
70	[number]	k4	70
let	léto	k1gNnPc2	léto
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yQnSc1	kdo
=	=	kIx~	=
Who	Who	k1gMnPc1	Who
is	is	k?	is
who	who	k?	who
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc4	osobnost
české	český	k2eAgFnSc2d1	Česká
současnosti	současnost	k1gFnSc2	současnost
:	:	kIx,	:
5000	[number]	k4	5000
životopisů	životopis	k1gInPc2	životopis
/	/	kIx~	/
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Třeštík	Třeštík	k1gMnSc1	Třeštík
editor	editor	k1gMnSc1	editor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yInSc1	kdo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
775	[number]	k4	775
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902586	[number]	k4	902586
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
I.	I.	kA	I.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
A	a	k9	a
<g/>
–	–	k?	–
<g/>
J.	J.	kA	J.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
634	[number]	k4	634
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
245	[number]	k4	245
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VRBECKÝ	VRBECKÝ	kA	VRBECKÝ
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Dukla	Dukla	k1gFnSc1	Dukla
Jihlava	Jihlava	k1gFnSc1	Jihlava
1956	[number]	k4	1956
<g/>
-	-	kIx~	-
<g/>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Půl	půl	k6eAd1	půl
století	století	k1gNnSc1	století
legendy	legenda	k1gFnSc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Jihlava	Jihlava	k1gFnSc1	Jihlava
:	:	kIx,	:
Parola	parola	k1gFnSc1	parola
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
(	(	kIx(	(
<g/>
lední	lední	k2eAgMnSc1d1	lední
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
)	)	kIx)	)
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Hockeydb	Hockeydb	k1gInSc4	Hockeydb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
(	(	kIx(	(
<g/>
lední	lední	k2eAgMnSc1d1	lední
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
)	)	kIx)	)
–	–	k?	–
statistiky	statistika	k1gFnSc2	statistika
na	na	k7c4	na
Eliteprospects	Eliteprospects	k1gInSc4	Eliteprospects
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
