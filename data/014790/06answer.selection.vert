<s desamb="1">
Znakem	znak	k1gInSc7
Audi	Audi	k1gNnSc2
jsou	být	k5eAaImIp3nP
4	#num#	k4
propojené	propojený	k2eAgInPc1d1
kruhy	kruh	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
symbolizují	symbolizovat	k5eAaImIp3nP
čtyři	čtyři	k4xCgInPc4
různé	různý	k2eAgInPc4d1
výrobce	výrobce	k1gMnSc2
automobilů	automobil	k1gInPc2
(	(	kIx(
<g/>
Audi	Audi	k1gNnSc1
<g/>
,	,	kIx,
DKW	DKW	kA
<g/>
,	,	kIx,
Horch	Horch	k1gMnSc1
a	a	k8xC
Wanderer	Wanderer	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
jejichž	jejichž	k3xOyRp3gFnSc3
fúzi	fúze	k1gFnSc3
do	do	k7c2
koncernu	koncern	k1gInSc2
Auto	auto	k1gNnSc1
Union	union	k1gInSc4
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
<g/>
.	.	kIx.
</s>