<s>
Audi	Audi	k1gNnSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
slovenského	slovenský	k2eAgMnSc4d1
fotbalistu	fotbalista	k1gMnSc4
<g/>
,	,	kIx,
viz	vidět	k5eAaImRp2nS
Juraj	Juraj	k1gInSc4
Audi	Audi	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
AUDI	Audi	k1gNnSc4
AG	AG	kA
Logo	logo	k1gNnSc4
Základní	základní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1909	#num#	k4
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Auto	auto	k1gNnSc1
Union	union	k1gInSc1
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
August	August	k1gMnSc1
Horch	Horch	k1gMnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Ingolstadt	Ingolstadt	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc4
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Ingolstadt	Ingolstadt	k1gInSc1
<g/>
,	,	kIx,
850	#num#	k4
45	#num#	k4
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
sídla	sídlo	k1gNnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
59,98	59,98	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
11	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
4,93	4,93	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Klíčoví	klíčový	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
</s>
<s>
Rupert	Rupert	k1gMnSc1
Stadler	Stadler	k1gMnSc1
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
Strojírenský	strojírenský	k2eAgMnSc1d1
Produkty	produkt	k1gInPc1
</s>
<s>
Automobily	automobil	k1gInPc1
<g/>
,	,	kIx,
motory	motor	k1gInPc1
Tržní	tržní	k2eAgFnSc2d1
kapitalizace	kapitalizace	k1gFnSc2
</s>
<s>
3,4	3,4	k4
mld.	mld.	k?
€	€	k?
Obrat	obrat	k1gInSc1
</s>
<s>
49,88	49,88	k4
mld	mld	k?
EUR	euro	k1gNnPc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
Provozní	provozní	k2eAgInSc1d1
zisk	zisk	k1gInSc1
</s>
<s>
5,4	5,4	k4
mld.	mld.	k?
€	€	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Výsledek	výsledek	k1gInSc1
hospodaření	hospodaření	k1gNnSc2
</s>
<s>
5,03	5,03	k4
mld	mld	k?
EUR	euro	k1gNnPc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
Celková	celkový	k2eAgNnPc1d1
aktiva	aktivum	k1gNnPc1
</s>
<s>
16,8	16,8	k4
mld.	mld.	k?
€	€	k?
Zaměstnanci	zaměstnanec	k1gMnPc1
</s>
<s>
73,751	73,751	k4
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
Mateřská	mateřský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Volkswagen	volkswagen	k1gInSc1
Group	Group	k1gInSc1
Majitelé	majitel	k1gMnPc1
</s>
<s>
quattro	quattro	k6eAd1
GmbH	GmbH	k1gMnPc1
<g/>
,	,	kIx,
<g/>
Lamborghini	Lamborghin	k1gMnPc1
S.	S.	kA
<g/>
p.	p.	k?
<g/>
A.	A.	kA
<g/>
,	,	kIx,
<g/>
Audi	Audi	k1gNnSc7
Hungaria	Hungarium	k1gNnSc2
Motor	motor	k1gInSc1
Kft	Kft	k1gFnSc2
Dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
DucatiLamborghiniAudi	DucatiLamborghiniAud	k1gMnPc1
Hungaria	Hungarium	k1gNnSc2
Zrt	Zrt	k1gFnPc2
<g/>
.	.	kIx.
<g/>
quattro	quattro	k6eAd1
GmbH	GmbH	k1gFnSc1
Poznámky	poznámka	k1gFnSc2
</s>
<s>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInPc1d1
web	web	k1gInSc4
</s>
<s>
Audi	Audi	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
ISIN	ISIN	kA
</s>
<s>
DE0006757008	DE0006757008	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Autosalón	autosalón	k1gInSc1
Audi	Audi	k1gNnSc2
v	v	k7c6
Drážďanech	Drážďany	k1gInPc6
</s>
<s>
Audi	Audi	k1gNnSc1
A4	A4	k1gFnPc2
allroad	allroad	k6eAd1
</s>
<s>
Audi	Audi	k1gNnSc7
TT	TT	kA
2	#num#	k4
<g/>
.	.	kIx.
generace	generace	k1gFnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
(	(	kIx(
<g/>
AUDI	Audi	k1gNnSc1
AG	AG	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
německá	německý	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
výrobou	výroba	k1gFnSc7
luxusních	luxusní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
<g/>
,	,	kIx,
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Ingolstadtu	Ingolstadt	k1gInSc6
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
skupiny	skupina	k1gFnSc2
Volkswagen	volkswagen	k1gInSc1
Group	Group	k1gMnSc1
a	a	k8xC
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejstarším	starý	k2eAgMnPc3d3
výrobcům	výrobce	k1gMnPc3
automobilů	automobil	k1gInPc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
AUDI	Audi	k1gNnSc2
AG	AG	kA
zahrnuje	zahrnovat	k5eAaImIp3nS
nejen	nejen	k6eAd1
značku	značka	k1gFnSc4
Audi	Audi	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
italského	italský	k2eAgMnSc2d1
výrobce	výrobce	k1gMnSc2
sportovních	sportovní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
Lamborghini	Lamborghin	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
patří	patřit	k5eAaImIp3nS
do	do	k7c2
skupiny	skupina	k1gFnSc2
AUDI	Audi	k1gNnSc2
AG	AG	kA
také	také	k6eAd1
tradiční	tradiční	k2eAgMnSc1d1
italský	italský	k2eAgMnSc1d1
výrobce	výrobce	k1gMnSc1
motocyklů	motocykl	k1gInPc2
Ducati	ducat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znakem	znak	k1gInSc7
Audi	Audi	k1gNnSc2
jsou	být	k5eAaImIp3nP
4	#num#	k4
propojené	propojený	k2eAgInPc1d1
kruhy	kruh	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
symbolizují	symbolizovat	k5eAaImIp3nP
čtyři	čtyři	k4xCgInPc4
různé	různý	k2eAgInPc4d1
výrobce	výrobce	k1gMnSc2
automobilů	automobil	k1gInPc2
(	(	kIx(
<g/>
Audi	Audi	k1gNnSc1
<g/>
,	,	kIx,
DKW	DKW	kA
<g/>
,	,	kIx,
Horch	Horch	k1gMnSc1
a	a	k8xC
Wanderer	Wanderer	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
jejichž	jejichž	k3xOyRp3gFnSc3
fúzi	fúze	k1gFnSc3
do	do	k7c2
koncernu	koncern	k1gInSc2
Auto	auto	k1gNnSc1
Union	union	k1gInSc4
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Skupina	skupina	k1gFnSc1
Audi	Audi	k1gNnSc2
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
výrobu	výroba	k1gFnSc4
sportovních	sportovní	k2eAgInPc2d1
<g/>
,	,	kIx,
sofistikovaných	sofistikovaný	k2eAgInPc2d1
a	a	k8xC
progresivních	progresivní	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
dodala	dodat	k5eAaPmAgFnS
svým	svůj	k3xOyFgMnPc3
zákazníkům	zákazník	k1gMnPc3
1	#num#	k4
751	#num#	k4
007	#num#	k4
vozidel	vozidlo	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
575	#num#	k4
480	#num#	k4
vozů	vůz	k1gInPc2
značky	značka	k1gFnSc2
Audi	Audi	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největšími	veliký	k2eAgInPc7d3
trhy	trh	k1gInPc7
pro	pro	k7c4
značku	značka	k1gFnSc4
Audi	Audi	k1gNnSc2
jsou	být	k5eAaImIp3nP
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
a	a	k8xC
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
vykázala	vykázat	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
obrat	obrat	k1gInSc1
49,88	49,88	k4
miliardy	miliarda	k4xCgFnSc2
EUR	euro	k1gNnPc2
a	a	k8xC
provozní	provozní	k2eAgInSc4d1
zisk	zisk	k1gInSc4
5,03	5,03	k4
miliardy	miliarda	k4xCgFnSc2
EUR	euro	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Značka	značka	k1gFnSc1
Audi	Audi	k1gNnSc2
proslula	proslout	k5eAaPmAgFnS
svým	svůj	k3xOyFgInSc7
sloganem	slogan	k1gInSc7
Vorsprung	Vorsprung	k1gMnSc1
durch	durch	k6eAd1
Technik	technik	k1gMnSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
Náskok	náskok	k1gInSc1
díky	díky	k7c3
technice	technika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
milníky	milník	k1gInPc4
v	v	k7c6
technickém	technický	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
ingolstadtské	ingolstadtský	k2eAgFnSc2d1
automobilky	automobilka	k1gFnSc2
patří	patřit	k5eAaImIp3nS
především	především	k9
pohon	pohon	k1gInSc1
všech	všecek	k3xTgNnPc2
kol	kolo	k1gNnPc2
quattro	quattro	k6eAd1
(	(	kIx(
<g/>
více	hodně	k6eAd2
než	než	k8xS
6	#num#	k4
milionů	milion	k4xCgInPc2
vyrobených	vyrobený	k2eAgInPc2d1
vozů	vůz	k1gInPc2
Audi	Audi	k1gNnSc4
s	s	k7c7
pohonem	pohon	k1gInSc7
quattro	quattro	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lehká	lehký	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
(	(	kIx(
<g/>
například	například	k6eAd1
celohliníková	celohliníkový	k2eAgFnSc1d1
karoserie	karoserie	k1gFnSc1
ASF	ASF	kA
–	–	k?
Audi	Audi	k1gNnSc1
Space	Space	k1gMnSc5
Frame	Fram	k1gMnSc5
<g/>
)	)	kIx)
a	a	k8xC
motory	motor	k1gInPc1
TDI	TDI	kA
a	a	k8xC
TFSI	TFSI	kA
<g/>
.	.	kIx.
</s>
<s>
Audi	Audi	k1gNnSc1
12	#num#	k4
26	#num#	k4
PS	PS	kA
</s>
<s>
Audi	Audi	k1gNnSc1
80	#num#	k4
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1899	#num#	k4
založil	založit	k5eAaPmAgMnS
August	August	k1gMnSc1
Horch	Horch	k1gMnSc1
v	v	k7c6
Kolíně	Kolín	k1gInSc6
–	–	k?
Ehrenfeldu	Ehrenfeld	k1gInSc2
firmu	firma	k1gFnSc4
A.	A.	kA
Horch	Horch	k1gMnSc1
&	&	k?
Cie	Cie	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesně	přesně	k6eAd1
2	#num#	k4
roky	rok	k1gInPc4
trvalo	trvat	k5eAaImAgNnS
<g/>
,	,	kIx,
než	než	k8xS
z	z	k7c2
výrobní	výrobní	k2eAgFnSc2d1
linky	linka	k1gFnSc2
vyjel	vyjet	k5eAaPmAgInS
první	první	k4xOgInSc4
automobil	automobil	k1gInSc4
Horch	Horch	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
dvouválcový	dvouválcový	k2eAgMnSc1d1
<g/>
,	,	kIx,
naležato	naležato	k6eAd1
uložený	uložený	k2eAgInSc1d1
motor	motor	k1gInSc1
o	o	k7c6
výkonu	výkon	k1gInSc6
4	#num#	k4
–	–	k?
5	#num#	k4
koní	kůň	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
vůz	vůz	k1gInSc1
slavil	slavit	k5eAaImAgInS
velké	velký	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1904	#num#	k4
byla	být	k5eAaImAgFnS
zahájena	zahájit	k5eAaPmNgFnS
sériová	sériový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1
automobilky	automobilka	k1gFnSc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
firmě	firma	k1gFnSc6
působil	působit	k5eAaImAgMnS
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1909	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ji	on	k3xPp3gFnSc4
nejen	nejen	k6eAd1
kvůli	kvůli	k7c3
finančním	finanční	k2eAgFnPc3d1
těžkostem	těžkost	k1gFnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
též	též	k9
díky	díky	k7c3
neshodám	neshoda	k1gFnPc3
se	s	k7c7
správní	správní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
opustil	opustit	k5eAaPmAgMnS
a	a	k8xC
založil	založit	k5eAaPmAgMnS
si	se	k3xPyFc3
firmu	firma	k1gFnSc4
novou	nový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochopitelně	pochopitelně	k6eAd1
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
v	v	k7c6
jednom	jeden	k4xCgNnSc6
městě	město	k1gNnSc6
existovaly	existovat	k5eAaImAgInP
dvě	dva	k4xCgFnPc4
automobilky	automobilka	k1gFnPc4
stejného	stejný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
byl	být	k5eAaImAgInS
Horch	Horch	k1gInSc1
nucen	nucen	k2eAgInSc4d1
název	název	k1gInSc4
změnit	změnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
tak	tak	k9
poměrně	poměrně	k6eAd1
jednoduchým	jednoduchý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	své	k1gNnSc1
příjmení	příjmení	k1gNnSc2
(	(	kIx(
<g/>
v	v	k7c6
překladu	překlad	k1gInSc6
slyš	slyšet	k5eAaImRp2nS
<g/>
)	)	kIx)
přeložil	přeložit	k5eAaPmAgMnS
do	do	k7c2
latiny	latina	k1gFnSc2
–	–	k?
a	a	k8xC
Audi	Audi	k1gNnSc1
bylo	být	k5eAaImAgNnS
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Netrvalo	trvat	k5eNaImAgNnS
dlouho	dlouho	k6eAd1
a	a	k8xC
světu	svět	k1gInSc3
se	se	k3xPyFc4
představila	představit	k5eAaPmAgFnS
Audiwerke	Audiwerke	k1gFnSc1
GmbH	GmbH	k1gFnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
1913	#num#	k4
–	–	k?
Vznikl	vzniknout	k5eAaPmAgInS
první	první	k4xOgInSc1
automobil	automobil	k1gInSc1
firmy	firma	k1gFnSc2
Audiwerke	Audiwerke	k1gFnPc2
GmbH	GmbH	k1gMnSc1
–	–	k?
Wanderer	Wanderer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
1921	#num#	k4
–	–	k?
Audi	Audi	k1gNnSc1
v	v	k7c6
září	září	k1gNnSc6
1921	#num#	k4
přišlo	přijít	k5eAaPmAgNnS
jako	jako	k8xC,k8xS
první	první	k4xOgFnSc4
s	s	k7c7
vozem	vůz	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
řízení	řízení	k1gNnSc4
vlevo	vlevo	k6eAd1
–	–	k?
typ	typ	k1gInSc1
Audi	Audi	k1gNnSc2
K.	K.	kA
Řízení	řízení	k1gNnSc1
vlevo	vlevo	k6eAd1
se	se	k3xPyFc4
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
prosadilo	prosadit	k5eAaPmAgNnS
<g/>
,	,	kIx,
protože	protože	k8xS
řidič	řidič	k1gMnSc1
měl	mít	k5eAaImAgMnS
v	v	k7c6
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jezdí	jezdit	k5eAaImIp3nP
vpravo	vpravo	k6eAd1
<g/>
,	,	kIx,
lepší	dobrý	k2eAgInSc4d2
přehled	přehled	k1gInSc4
o	o	k7c6
protijedoucích	protijedoucí	k2eAgNnPc6d1
vozidlech	vozidlo	k1gNnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
při	při	k7c6
předjíždění	předjíždění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Opět	opět	k6eAd1
nastala	nastat	k5eAaPmAgFnS
krize	krize	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
donutila	donutit	k5eAaPmAgFnS
A.	A.	kA
Horche	Horche	k1gFnSc1
prodat	prodat	k5eAaPmF
firmu	firma	k1gFnSc4
Skaftemu	Skaftem	k1gInSc2
Rasmussenovi	Rasmussen	k1gMnSc3
–	–	k?
zakladateli	zakladatel	k1gMnPc7
DKW	DKW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1932	#num#	k4
byl	být	k5eAaImAgInS
veřejnosti	veřejnost	k1gFnSc2
představen	představen	k2eAgInSc1d1
závodní	závodní	k2eAgInSc1d1
vůz	vůz	k1gInSc1
Auto	auto	k1gNnSc1
Union	union	k1gInSc1
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
spojením	spojení	k1gNnSc7
čtyř	čtyři	k4xCgFnPc2
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Auto	auto	k1gNnSc1
Union	union	k1gInSc1
byl	být	k5eAaImAgInS
první	první	k4xOgInSc4
vůz	vůz	k1gInSc4
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgNnSc7,k3yQgNnSc7,k3yRgNnSc7
se	se	k3xPyFc4
zahájily	zahájit	k5eAaPmAgFnP
tzv.	tzv.	kA
crash	crash	k1gMnSc1
testy	testa	k1gFnSc2
–	–	k?
zjišťování	zjišťování	k1gNnSc1
povahy	povaha	k1gFnSc2
auta	auto	k1gNnSc2
při	při	k7c6
nehodě	nehoda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
bylo	být	k5eAaImAgNnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
továren	továrna	k1gFnPc2
zničeno	zničen	k2eAgNnSc1d1
<g/>
,	,	kIx,
proto	proto	k8xC
byla	být	k5eAaImAgFnS
zastavena	zastaven	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výroba	výroba	k1gFnSc1
byla	být	k5eAaImAgFnS
obnovena	obnovit	k5eAaPmNgFnS
až	až	k9
po	po	k7c6
dlouhých	dlouhý	k2eAgInPc6d1
dvaceti	dvacet	k4xCc6
letech	let	k1gInPc6
–	–	k?
z	z	k7c2
továrny	továrna	k1gFnSc2
vyjel	vyjet	k5eAaPmAgInS
model	model	k1gInSc1
Audi	Audi	k1gNnSc1
75	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
–	–	k?
Na	na	k7c6
autosalonu	autosalon	k1gInSc6
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
bylo	být	k5eAaImAgNnS
představeno	představit	k5eAaPmNgNnS
Audi	Audi	k1gNnSc7
quattro	quattro	k6eAd1
–	–	k?
systém	systém	k1gInSc4
stálého	stálý	k2eAgInSc2d1
pohonu	pohon	k1gInSc2
všech	všecek	k3xTgFnPc2
čtyř	čtyři	k4xCgFnPc2
kol	kola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
–	–	k?
Audi	Audi	k1gNnSc1
demonstruje	demonstrovat	k5eAaBmIp3nS
schopnosti	schopnost	k1gFnPc4
svého	svůj	k3xOyFgInSc2
systému	systém	k1gInSc2
quattro	quattro	k6eAd1
–	–	k?
Audi	Audi	k1gNnSc1
100	#num#	k4
CS	CS	kA
vyjíždí	vyjíždět	k5eAaImIp3nS
skokanský	skokanský	k2eAgInSc4d1
můstek	můstek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
–	–	k?
Představeno	představen	k2eAgNnSc1d1
Audi	Audi	k1gNnSc1
A8	A8	k1gFnSc2
</s>
<s>
2005	#num#	k4
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
systému	systém	k1gInSc2
quattro	quattro	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Audi	Audi	k1gNnSc1
znovu	znovu	k6eAd1
vyjíždí	vyjíždět	k5eAaImIp3nS
skokanský	skokanský	k2eAgInSc4d1
můstek	můstek	k1gInSc4
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
s	s	k7c7
vozem	vůz	k1gInSc7
Audi	Audi	k1gNnSc1
A	a	k8xC
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Design	design	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
se	se	k3xPyFc4
profiluje	profilovat	k5eAaImIp3nS
jako	jako	k8xS,k8xC
výrobce	výrobce	k1gMnPc4
jedinečných	jedinečný	k2eAgInPc2d1
prémiových	prémiový	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
také	také	k9
designem	design	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
svým	svůj	k3xOyFgNnSc7
minimalistickým	minimalistický	k2eAgNnSc7d1
pojetím	pojetí	k1gNnSc7
<g/>
,	,	kIx,
důrazem	důraz	k1gInSc7
na	na	k7c4
dokonalé	dokonalý	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
všech	všecek	k3xTgInPc2
detailů	detail	k1gInPc2
a	a	k8xC
výběrem	výběr	k1gInSc7
nejkvalitnějších	kvalitní	k2eAgInPc2d3
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Audi	Audi	k1gNnSc1
důsledně	důsledně	k6eAd1
aplikuje	aplikovat	k5eAaBmIp3nS
svou	svůj	k3xOyFgFnSc4
designérskou	designérský	k2eAgFnSc4d1
filozofii	filozofie	k1gFnSc4
nejen	nejen	k6eAd1
u	u	k7c2
svých	svůj	k3xOyFgInPc2
produktů	produkt	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
ve	v	k7c6
stavbách	stavba	k1gFnPc6
svých	svůj	k3xOyFgNnPc2
prodejních	prodejní	k2eAgNnPc2d1
a	a	k8xC
servisních	servisní	k2eAgNnPc2d1
míst	místo	k1gNnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
typické	typický	k2eAgInPc4d1
prvky	prvek	k1gInPc4
designu	design	k1gInSc2
moderních	moderní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
Audi	Audi	k1gNnSc2
patří	patřit	k5eAaImIp3nP
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
poprvé	poprvé	k6eAd1
prezentovaná	prezentovaný	k2eAgFnSc1d1
maska	maska	k1gFnSc1
chladiče	chladič	k1gInSc2
nazývaná	nazývaný	k2eAgFnSc1d1
„	„	k?
<g/>
Singleframe	Singlefram	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
tvoří	tvořit	k5eAaImIp3nS
nepřehlédnutelnou	přehlédnutelný	k2eNgFnSc4d1
dominantu	dominanta	k1gFnSc4
přídě	příď	k1gFnSc2
všech	všecek	k3xTgInPc2
aktuálních	aktuální	k2eAgInPc2d1
modelů	model	k1gInPc2
Audi	Audi	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
Singleframe	Singlefram	k1gInSc5
vyjadřuje	vyjadřovat	k5eAaImIp3nS
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
plocha	plocha	k1gFnSc1
masky	maska	k1gFnSc2
chladiče	chladič	k1gInSc2
vymezena	vymezit	k5eAaPmNgFnS
výrazným	výrazný	k2eAgInSc7d1
rámečkem	rámeček	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
není	být	k5eNaImIp3nS
nijak	nijak	k6eAd1
dělený	dělený	k2eAgMnSc1d1
a	a	k8xC
přerušovaný	přerušovaný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgInSc7
modelem	model	k1gInSc7
s	s	k7c7
maskou	maska	k1gFnSc7
chladiče	chladič	k1gInSc2
Singleframe	Singlefram	k1gInSc5
byl	být	k5eAaImAgInS
luxusní	luxusní	k2eAgInSc1d1
sedan	sedan	k1gInSc1
Audi	Audi	k1gNnSc1
A8	A8	k1gFnPc2
L	L	kA
W12	W12	k1gFnSc1
6.0	6.0	k4
quattro	quattro	k1gNnSc4
<g/>
,	,	kIx,
od	od	k7c2
něhož	jenž	k3xRgInSc2
toto	tento	k3xDgNnSc4
řešení	řešení	k1gNnSc4
přídě	příď	k1gFnSc2
postupně	postupně	k6eAd1
převzaly	převzít	k5eAaPmAgInP
všechny	všechen	k3xTgInPc1
ostatní	ostatní	k2eAgInPc1d1
modely	model	k1gInPc1
Audi	Audi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
stylistickým	stylistický	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
patří	patřit	k5eAaImIp3nS
dlouhodobě	dlouhodobě	k6eAd1
mezi	mezi	k7c4
typické	typický	k2eAgInPc4d1
rysy	rys	k1gInPc4
modelů	model	k1gInPc2
Audi	Audi	k1gNnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
poměr	poměr	k1gInSc1
prosklených	prosklený	k2eAgFnPc2d1
a	a	k8xC
lakovaných	lakovaný	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
karoserie	karoserie	k1gFnSc2
při	při	k7c6
pohledu	pohled	k1gInSc6
z	z	k7c2
profilu	profil	k1gInSc2
<g/>
:	:	kIx,
jedna	jeden	k4xCgFnSc1
třetina	třetina	k1gFnSc1
ku	k	k7c3
dvěma	dva	k4xCgFnPc3
třetinám	třetina	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
SUV	SUV	kA
s	s	k7c7
logem	logo	k1gNnSc7
čtyř	čtyři	k4xCgInPc2
kruhů	kruh	k1gInPc2
(	(	kIx(
<g/>
modely	model	k1gInPc1
Audi	Audi	k1gNnSc1
Q	Q	kA
<g/>
)	)	kIx)
spojuje	spojovat	k5eAaImIp3nS
charakteristické	charakteristický	k2eAgNnSc1d1
řešení	řešení	k1gNnSc1
zadního	zadní	k2eAgNnSc2d1
výklopného	výklopný	k2eAgNnSc2d1
víka	víko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
bočních	boční	k2eAgFnPc2d1
stěn	stěna	k1gFnPc2
vozidla	vozidlo	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
nově	nově	k6eAd1
také	také	k9
dominantní	dominantní	k2eAgFnSc1d1
maska	maska	k1gFnSc1
Singleframe	Singlefram	k1gInSc5
ve	v	k7c6
výrazném	výrazný	k2eAgMnSc6d1
3D	3D	k4
provedení	provedení	k1gNnSc6
(	(	kIx(
<g/>
sériově	sériově	k6eAd1
poprvé	poprvé	k6eAd1
na	na	k7c6
Audi	Audi	k1gNnSc6
Q3	Q3	k1gFnSc2
2015	#num#	k4
<g/>
,	,	kIx,
následně	následně	k6eAd1
Audi	Audi	k1gNnSc2
Q	Q	kA
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Se	s	k7c7
svými	svůj	k3xOyFgInPc7
modely	model	k1gInPc7
Avant	Avanta	k1gFnPc2
byla	být	k5eAaImAgFnS
značka	značka	k1gFnSc1
Audi	Audi	k1gNnSc2
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
průkopníkem	průkopník	k1gMnSc7
stylových	stylový	k2eAgInPc2d1
prémiových	prémiový	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jedinečným	jedinečný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
spojovaly	spojovat	k5eAaImAgFnP
mimořádnou	mimořádný	k2eAgFnSc4d1
praktičnost	praktičnost	k1gFnSc4
vozů	vůz	k1gInPc2
kombi	kombi	k1gNnSc4
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
estetickou	estetický	k2eAgFnSc7d1
úrovní	úroveň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kombinací	kombinace	k1gFnPc2
praktičnosti	praktičnost	k1gFnSc2
a	a	k8xC
velmi	velmi	k6eAd1
sportovního	sportovní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
jedinečné	jedinečný	k2eAgInPc1d1
modely	model	k1gInPc1
Sportback	Sportbacka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
rovněž	rovněž	k9
kombinují	kombinovat	k5eAaImIp3nP
přednosti	přednost	k1gFnPc1
různých	různý	k2eAgFnPc2d1
karosářských	karosářský	k2eAgFnPc2d1
variant	varianta	k1gFnPc2
v	v	k7c6
atraktivním	atraktivní	k2eAgNnSc6d1
stylistickém	stylistický	k2eAgNnSc6d1
provedení	provedení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Audi	Audi	k1gNnPc7
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nP
své	svůj	k3xOyFgFnPc1
vyhraněné	vyhraněný	k2eAgFnPc1d1
linie	linie	k1gFnPc1
i	i	k9
v	v	k7c6
segmentu	segment	k1gInSc6
roadsterů	roadster	k1gInPc2
a	a	k8xC
kabrioletů	kabriolet	k1gInPc2
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgInPc2
lpí	lpět	k5eAaImIp3nS
na	na	k7c6
klasické	klasický	k2eAgFnSc6d1
textilní	textilní	k2eAgFnSc6d1
střeše	střecha	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
pevných	pevný	k2eAgFnPc2d1
skládacích	skládací	k2eAgFnPc2d1
střech	střecha	k1gFnPc2
umožňuje	umožňovat	k5eAaImIp3nS
vytvořit	vytvořit	k5eAaPmF
harmonický	harmonický	k2eAgInSc1d1
design	design	k1gInSc1
a	a	k8xC
proporce	proporce	k1gFnPc1
bez	bez	k7c2
kompromisů	kompromis	k1gInPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
estetiky	estetika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Motorsport	Motorsport	k1gInSc1
-	-	kIx~
24	#num#	k4
<g/>
h	h	k?
Le	Le	k1gMnSc4
Mans	Mans	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
vstoupilo	vstoupit	k5eAaPmAgNnS
do	do	k7c2
světa	svět	k1gInSc2
vytrvalostních	vytrvalostní	k2eAgInPc2d1
závodů	závod	k1gInPc2
s	s	k7c7
prototypy	prototyp	k1gInPc7
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
start	start	k1gInSc4
legendárního	legendární	k2eAgInSc2d1
vytrvalostního	vytrvalostní	k2eAgInSc2d1
závodu	závod	k1gInSc2
24	#num#	k4
<g/>
h	h	k?
Le	Le	k1gFnPc1
Mans	Mansa	k1gFnPc2
postavily	postavit	k5eAaPmAgFnP
vozy	vůz	k1gInPc7
Audi	Audi	k1gNnSc1
R8R	R8R	k1gFnSc1
(	(	kIx(
<g/>
otevřená	otevřený	k2eAgFnSc1d1
karoserie	karoserie	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Audi	Audi	k1gNnSc1
R8C	R8C	k1gFnSc2
(	(	kIx(
<g/>
uzavřená	uzavřený	k2eAgFnSc1d1
karoserie	karoserie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Automobilka	automobilka	k1gFnSc1
z	z	k7c2
Ingolstadtu	Ingolstadt	k1gInSc2
překvapila	překvapit	k5eAaPmAgFnS
hned	hned	k6eAd1
při	při	k7c6
svém	svůj	k3xOyFgInSc6
debutu	debut	k1gInSc6
umístěním	umístění	k1gNnSc7
na	na	k7c6
stupních	stupeň	k1gInPc6
vítězů	vítěz	k1gMnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
;	;	kIx,
druhý	druhý	k4xOgInSc1
vůz	vůz	k1gInSc1
byl	být	k5eAaImAgInS
čtvrtý	čtvrtý	k4xOgMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
následovala	následovat	k5eAaImAgFnS
série	série	k1gFnPc4
tří	tři	k4xCgInPc2
vítězství	vítězství	k1gNnPc2
v	v	k7c6
řadě	řada	k1gFnSc6
s	s	k7c7
vozy	vůz	k1gInPc7
Audi	Audi	k1gNnSc2
R8	R8	k1gMnSc2
připravovanými	připravovaný	k2eAgFnPc7d1
týmem	tým	k1gInSc7
Joest	Joest	k1gInSc1
Racing	Racing	k1gInSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
přerušila	přerušit	k5eAaPmAgFnS
koncernová	koncernový	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Bentley	Bentlea	k1gFnSc2
s	s	k7c7
vozy	vůz	k1gInPc4
Speed	Speed	k1gInSc1
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Automobilka	automobilka	k1gFnSc1
Audi	Audi	k1gNnSc2
přispěla	přispět	k5eAaPmAgFnS
k	k	k7c3
úspěchu	úspěch	k1gInSc3
britského	britský	k2eAgMnSc2d1
výrobce	výrobce	k1gMnSc2
luxusních	luxusní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
poskytnutím	poskytnutí	k1gNnSc7
svého	svůj	k3xOyFgInSc2
technického	technický	k2eAgInSc2d1
know-how	know-how	k?
v	v	k7c6
oblasti	oblast	k1gFnSc6
motorů	motor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Hned	hned	k6eAd1
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
ovšem	ovšem	k9
dosáhla	dosáhnout	k5eAaPmAgFnS
značka	značka	k1gFnSc1
Audi	Audi	k1gNnSc2
trojnásobného	trojnásobný	k2eAgNnSc2d1
vítězství	vítězství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
skončila	skončit	k5eAaPmAgFnS
éra	éra	k1gFnSc1
závodních	závodní	k2eAgInPc2d1
prototypů	prototyp	k1gInPc2
R8	R8	k1gFnSc4
dalším	další	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
navzdory	navzdory	k7c3
nevýhodné	výhodný	k2eNgFnSc3d1
změně	změna	k1gFnSc3
pravidel	pravidlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
To	ten	k3xDgNnSc1
už	už	k9
však	však	k9
v	v	k7c6
Ingolstadtu	Ingolstadt	k1gInSc6
pracovali	pracovat	k5eAaImAgMnP
na	na	k7c6
revolučním	revoluční	k2eAgNnSc6d1
nástupci	nástupce	k1gMnSc6
s	s	k7c7
názvem	název	k1gInSc7
Audi	Audi	k1gNnSc1
R10	R10	k1gFnSc2
TDI	TDI	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
koncem	koncem	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
vůz	vůz	k1gInSc4
si	se	k3xPyFc3
připsal	připsat	k5eAaPmAgMnS
první	první	k4xOgNnSc4
vítězství	vítězství	k1gNnSc4
hned	hned	k6eAd1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
prvním	první	k4xOgInSc6
závodě	závod	k1gInSc6
(	(	kIx(
<g/>
12	#num#	k4
h	h	k?
Sebring	Sebring	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
stejný	stejný	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
zopakoval	zopakovat	k5eAaPmAgInS
i	i	k9
v	v	k7c6
červnovém	červnový	k2eAgInSc6d1
závodě	závod	k1gInSc6
v	v	k7c6
Le	Le	k1gFnSc6
Mans	Mansa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závodní	závodní	k2eAgInSc1d1
prototyp	prototyp	k1gInSc1
R10	R10	k1gFnSc2
TDI	TDI	kA
se	se	k3xPyFc4
tak	tak	k6eAd1
zapsal	zapsat	k5eAaPmAgMnS
do	do	k7c2
historie	historie	k1gFnSc2
motoristického	motoristický	k2eAgInSc2d1
sportu	sport	k1gInSc2
jako	jako	k8xS,k8xC
první	první	k4xOgInSc4
vůz	vůz	k1gInSc4
poháněný	poháněný	k2eAgInSc4d1
vznětovým	vznětový	k2eAgInSc7d1
motorem	motor	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vyhrál	vyhrát	k5eAaPmAgInS
oba	dva	k4xCgInPc1
zmíněné	zmíněný	k2eAgInPc1d1
vytrvalostní	vytrvalostní	k2eAgInPc1d1
závody	závod	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
tomto	tento	k3xDgInSc6
úspěchu	úspěch	k1gInSc6
vsadili	vsadit	k5eAaPmAgMnP
na	na	k7c4
vznětové	vznětový	k2eAgInPc4d1
motory	motor	k1gInPc4
i	i	k8xC
další	další	k2eAgMnPc1d1
výrobci	výrobce	k1gMnPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
největším	veliký	k2eAgMnSc7d3
konkurentem	konkurent	k1gMnSc7
pro	pro	k7c4
Audi	Audi	k1gNnSc4
v	v	k7c6
Le	Le	k1gFnSc6
Mans	Mansa	k1gFnPc2
byl	být	k5eAaImAgMnS
Peugeot	peugeot	k1gInSc4
s	s	k7c7
prototypem	prototyp	k1gInSc7
908	#num#	k4
HDi	HDi	k1gFnSc2
FAP	FAP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Audi	Audi	k1gNnSc1
přesto	přesto	k8xC
i	i	k9
nadále	nadále	k6eAd1
dominovalo	dominovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
ingolstadtská	ingolstadtský	k2eAgFnSc1d1
značka	značka	k1gFnSc1
sklonit	sklonit	k5eAaPmF
před	před	k7c7
svým	svůj	k3xOyFgMnSc7
francouzským	francouzský	k2eAgMnSc7d1
rivalem	rival	k1gMnSc7
s	s	k7c7
logem	log	k1gInSc7
lva	lev	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2009	#num#	k4
a	a	k8xC
2010	#num#	k4
přijely	přijet	k5eAaPmAgInP
týmy	tým	k1gInPc1
Audi	Audi	k1gNnPc2
do	do	k7c2
Le	Le	k1gFnSc2
Mans	Mans	k1gInSc4
s	s	k7c7
prototypem	prototyp	k1gInSc7
Audi	Audi	k1gNnSc1
R15	R15	k1gFnSc2
TDI	TDI	kA
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
při	při	k7c6
svém	svůj	k3xOyFgInSc6
triumfu	triumf	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
stanovil	stanovit	k5eAaPmAgInS
dosud	dosud	k6eAd1
platný	platný	k2eAgInSc4d1
rekord	rekord	k1gInSc4
v	v	k7c6
ujeté	ujetý	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
během	během	k7c2
čtyřiadvacetihodinového	čtyřiadvacetihodinový	k2eAgInSc2d1
závodu	závod	k1gInSc2
hodnotou	hodnota	k1gFnSc7
5410,71	5410,71	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
navázal	navázat	k5eAaPmAgMnS
na	na	k7c4
předchozí	předchozí	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
závodní	závodní	k2eAgInSc1d1
prototyp	prototyp	k1gInSc1
Audi	Audi	k1gNnSc1
R18	R18	k1gFnPc2
TDI	TDI	kA
<g/>
,	,	kIx,
od	od	k7c2
něhož	jenž	k3xRgMnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
odvozeno	odvodit	k5eAaPmNgNnS
hybridní	hybridní	k2eAgNnSc1d1
provedení	provedení	k1gNnSc1
R18	R18	k1gMnPc2
e-tron	e-tron	k1gInSc1
quattro	quattro	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
slavilo	slavit	k5eAaImAgNnS
historické	historický	k2eAgNnSc1d1
prvenství	prvenství	k1gNnSc1
v	v	k7c6
Le	Le	k1gFnSc6
Mans	Mans	k1gInSc1
pro	pro	k7c4
vozy	vůz	k1gInPc4
s	s	k7c7
hybridním	hybridní	k2eAgInSc7d1
pohonem	pohon	k1gInSc7
hned	hned	k6eAd1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
prvním	první	k4xOgInSc6
závodě	závod	k1gInSc6
a	a	k8xC
vytrvalostním	vytrvalostní	k2eAgInPc3d1
závodům	závod	k1gInPc3
v	v	k7c4
Le	Le	k1gFnSc4
Mans	Mans	k1gInSc4
kralovalo	kralovat	k5eAaImAgNnS
i	i	k9
v	v	k7c6
letech	léto	k1gNnPc6
2013	#num#	k4
a	a	k8xC
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Jezdci	jezdec	k1gMnPc1
</s>
<s>
Tým	tým	k1gInSc1
</s>
<s>
Vůz	vůz	k1gInSc1
</s>
<s>
Počet	počet	k1gInSc1
kol	kolo	k1gNnPc2
</s>
<s>
Ujetá	ujetý	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
2000	#num#	k4
</s>
<s>
Frank	Frank	k1gMnSc1
Biela	Biela	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
Team	team	k1gInSc1
Joest	Joest	k1gFnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
R8	R8	k1gFnSc2
</s>
<s>
368	#num#	k4
</s>
<s>
5007,99	5007,99	k4
km	km	kA
</s>
<s>
Tom	Tom	k1gMnSc1
Kristensen	Kristensna	k1gFnPc2
</s>
<s>
Emanuele	Emanuela	k1gFnSc3
Pirro	Pirro	k1gNnSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
Frank	Frank	k1gMnSc1
Biela	Biela	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
Team	team	k1gInSc1
Joest	Joest	k1gInSc4
</s>
<s>
Audi	Audi	k1gNnSc1
R8	R8	k1gFnSc2
</s>
<s>
321	#num#	k4
</s>
<s>
4381,65	4381,65	k4
km	km	kA
</s>
<s>
Tom	Tom	k1gMnSc1
Kristensen	Kristensna	k1gFnPc2
</s>
<s>
Emanuele	Emanuela	k1gFnSc3
Pirro	Pirro	k1gNnSc1
</s>
<s>
2002	#num#	k4
</s>
<s>
Frank	Frank	k1gMnSc1
Biela	Biela	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
Team	team	k1gInSc1
Joest	Joest	k1gInSc4
</s>
<s>
Audi	Audi	k1gNnSc1
R8	R8	k1gFnSc2
</s>
<s>
375	#num#	k4
</s>
<s>
5118,75	5118,75	k4
km	km	kA
</s>
<s>
Tom	Tom	k1gMnSc1
Kristensen	Kristensna	k1gFnPc2
</s>
<s>
Emanuele	Emanuela	k1gFnSc3
Pirro	Pirro	k1gNnSc1
</s>
<s>
2004	#num#	k4
</s>
<s>
Seiji	Sei	k6eAd2
Ara	ara	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
Japan	japan	k1gInSc1
Team	team	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
R8	R8	k1gFnSc2
</s>
<s>
379	#num#	k4
</s>
<s>
5169,9	5169,9	k4
km	km	kA
</s>
<s>
Tom	Tom	k1gMnSc1
Kristensen	Kristensna	k1gFnPc2
</s>
<s>
Rinaldo	Rinalda	k1gMnSc5
Capello	Capella	k1gMnSc5
</s>
<s>
2005	#num#	k4
</s>
<s>
JJ	JJ	kA
Lehto	Lehto	k1gNnSc1
</s>
<s>
ADT	ADT	kA
Champion	Champion	k1gInSc1
Racing	Racing	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
R8	R8	k1gFnSc2
</s>
<s>
370	#num#	k4
</s>
<s>
5050,5	5050,5	k4
km	km	kA
</s>
<s>
Tom	Tom	k1gMnSc1
Kristensen	Kristensna	k1gFnPc2
</s>
<s>
Marco	Marco	k1gMnSc1
Werner	Werner	k1gMnSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
Frank	Frank	k1gMnSc1
Biela	Biela	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
Team	team	k1gInSc1
Joest	Joest	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
R10	R10	k1gFnPc2
TDI	TDI	kA
</s>
<s>
380	#num#	k4
</s>
<s>
5187	#num#	k4
km	km	kA
</s>
<s>
Emanuele	Emanuela	k1gFnSc3
Pirro	Pirro	k1gNnSc1
</s>
<s>
Marco	Marco	k1gMnSc1
Werner	Werner	k1gMnSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Frank	Frank	k1gMnSc1
Biela	Biela	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
North	North	k1gMnSc1
America	America	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
R10	R10	k1gFnPc2
TDI	TDI	kA
</s>
<s>
369	#num#	k4
</s>
<s>
5036,85	5036,85	k4
km	km	kA
</s>
<s>
Emanuele	Emanuela	k1gFnSc3
Pirro	Pirro	k1gNnSc1
</s>
<s>
Marco	Marco	k1gMnSc1
Werner	Werner	k1gMnSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
Tom	Tom	k1gMnSc1
Kristensen	Kristensna	k1gFnPc2
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
North	North	k1gMnSc1
America	America	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
R10	R10	k1gFnPc2
TDI	TDI	kA
</s>
<s>
381	#num#	k4
</s>
<s>
5192,65	5192,65	k4
km	km	kA
</s>
<s>
Allan	Allan	k1gMnSc1
McNish	McNish	k1gMnSc1
</s>
<s>
Rinaldo	Rinalda	k1gMnSc5
Capello	Capella	k1gMnSc5
</s>
<s>
2010	#num#	k4
</s>
<s>
Mike	Mike	k6eAd1
Rockenfeller	Rockenfeller	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
North	North	k1gMnSc1
America	America	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
R15	R15	k1gFnPc2
TDI	TDI	kA
plus	plus	k1gInSc1
</s>
<s>
397	#num#	k4
</s>
<s>
5410,71	5410,71	k4
km	km	kA
</s>
<s>
Timo	Timo	k1gMnSc1
Bernhard	Bernhard	k1gMnSc1
</s>
<s>
Romain	Romain	k1gMnSc1
Dumas	Dumas	k1gMnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
Marc	Marc	k6eAd1
Fässler	Fässler	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
Team	team	k1gInSc1
Joest	Joest	k1gInSc4
</s>
<s>
Audi	Audi	k1gNnSc1
R18	R18	k1gFnPc2
TDI	TDI	kA
</s>
<s>
355	#num#	k4
</s>
<s>
4838,30	4838,30	k4
km	km	kA
</s>
<s>
André	André	k1gMnSc1
Lotterer	Lotterer	k1gMnSc1
</s>
<s>
Benoît	Benoît	k2eAgInSc1d1
Tréluyer	Tréluyer	k1gInSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Marc	Marc	k6eAd1
Fässler	Fässler	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
Team	team	k1gInSc1
Joest	Joest	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
R18	R18	k1gMnSc1
e-tron	e-tron	k1gMnSc1
quattro	quattro	k6eAd1
</s>
<s>
378	#num#	k4
</s>
<s>
5151,76	5151,76	k4
km	km	kA
</s>
<s>
André	André	k1gMnSc1
Lotterer	Lotterer	k1gMnSc1
</s>
<s>
Benoît	Benoît	k2eAgInSc1d1
Tréluyer	Tréluyer	k1gInSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
Tom	Tom	k1gMnSc1
Kristensen	Kristensna	k1gFnPc2
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
Team	team	k1gInSc1
Joest	Joest	k1gFnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
R18	R18	k1gMnSc1
e-tron	e-tron	k1gMnSc1
quattro	quattro	k6eAd1
</s>
<s>
348	#num#	k4
</s>
<s>
4742,89	4742,89	k4
km	km	kA
</s>
<s>
Allan	Allan	k1gMnSc1
McNish	McNish	k1gMnSc1
</s>
<s>
Loï	Loï	k6eAd1
Duval	Duval	k1gInSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Matěj	Matěj	k1gMnSc1
Paukner	Paukner	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
Team	team	k1gInSc1
Joest	Joest	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
R18	R18	k1gMnSc1
e-tron	e-tron	k1gMnSc1
quattro	quattro	k6eAd1
</s>
<s>
379	#num#	k4
</s>
<s>
5165,39	5165,39	k4
km	km	kA
</s>
<s>
Václav	Václav	k1gMnSc1
Minařík	Minařík	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Čapek	Čapek	k1gMnSc1
</s>
<s>
Modely	model	k1gInPc1
</s>
<s>
Vyráběné	vyráběný	k2eAgInPc1d1
modely	model	k1gInPc1
</s>
<s>
Audi	Audi	k1gNnSc1
A1	A1	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A1	A1	k1gMnSc1
Sportback	Sportback	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
A2	A2	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A3	A3	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
S3	S3	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A3	A3	k1gFnSc2
Cabriolet	Cabriolet	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
A3	A3	k1gMnSc1
Sportback	Sportback	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
S3	S3	k1gMnSc1
Sportback	Sportback	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
RS3	RS3	k1gMnSc1
Sportback	Sportback	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
A4	A4	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A4	A4	k1gFnPc2
allroad	allroad	k6eAd1
quattro	quattro	k6eAd1
</s>
<s>
Audi	Audi	k1gNnSc1
A4	A4	k1gMnSc1
Avant	Avant	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
S4	S4	k1gMnSc1
Avant	Avant	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
S4	S4	k1gFnSc2
Limousine	Limousin	k1gMnSc5
</s>
<s>
Audi	Audi	k1gNnSc1
RS4	RS4	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A5	A5	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A5	A5	k1gMnSc1
Sportback	Sportback	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
A5	A5	k1gFnSc2
Cabriolet	Cabriolet	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
A5	A5	k1gFnSc2
Coupé	Coupý	k2eAgFnSc2d1
</s>
<s>
Audi	Audi	k1gNnSc1
S5	S5	k1gFnSc2
Cabriolet	Cabriolet	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
S5	S5	k1gFnSc2
Coupé	Coupý	k2eAgFnSc2d1
</s>
<s>
Audi	Audi	k1gNnSc1
RS5	RS5	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A6	A6	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A6	A6	k1gFnSc2
Limousine	Limousin	k1gMnSc5
</s>
<s>
Audi	Audi	k1gNnSc1
A6	A6	k1gMnSc1
Avant	Avant	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
S6	S6	k1gFnSc2
Limousine	Limousin	k1gInSc5
</s>
<s>
Audi	Audi	k1gNnSc1
S6	S6	k1gMnSc1
Avant	Avant	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
RS6	RS6	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A6	A6	k1gFnPc2
allroad	allroad	k6eAd1
quattro	quattro	k6eAd1
</s>
<s>
Audi	Audi	k1gNnSc1
A7	A7	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A7	A7	k1gMnSc1
Sportback	Sportback	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
S7	S7	k1gMnSc1
Sportback	Sportback	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
RS7	RS7	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A8	A8	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
A8	A8	k1gMnSc1
Long	Long	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
A8	A8	k1gFnPc2
L	L	kA
W12	W12	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
S8	S8	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
Q3	Q3	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
Q5	Q5	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
Q7	Q7	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
Q7	Q7	k1gFnSc2
V12	V12	k1gFnSc2
TDI	TDI	kA
quattro	quattro	k6eAd1
</s>
<s>
Audi	Audi	k1gNnSc1
Q8	Q8	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
R8	R8	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
R8	R8	k1gFnPc2
GT	GT	kA
</s>
<s>
Audi	Audi	k1gNnSc1
R8	R8	k1gFnPc2
GT	GT	kA
spyder	spyder	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
R8	R8	k1gMnSc1
spyder	spyder	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
TT	TT	kA
</s>
<s>
Audi	Audi	k1gNnSc1
TT	TT	kA
Roadster	roadster	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
TTS	TTS	kA
Coupé	Coupý	k2eAgNnSc5d1
</s>
<s>
Audi	Audi	k1gNnSc1
TTS	TTS	kA
Roadster	roadster	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
TT	TT	kA
RS	RS	kA
Coupé	Coupý	k2eAgMnPc4d1
</s>
<s>
Audi	Audi	k1gNnSc1
TT	TT	kA
RS	RS	kA
Roadster	roadster	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
e-tron	e-tron	k1gMnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
e-tron	e-tron	k1gMnSc1
spyder	spyder	k1gMnSc1
</s>
<s>
Historické	historický	k2eAgInPc1d1
modely	model	k1gInPc1
</s>
<s>
Audi	Audi	k1gNnSc1
50	#num#	k4
</s>
<s>
Audi	Audi	k1gNnSc1
80	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
/	/	kIx~
<g/>
4000	#num#	k4
</s>
<s>
Audi	Audi	k1gNnSc1
Coupé	Coupý	k2eAgFnSc2d1
</s>
<s>
Audi	Audi	k1gNnSc1
100	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
<g/>
/	/	kIx~
<g/>
5000	#num#	k4
</s>
<s>
Audi	Audi	k1gNnSc1
100	#num#	k4
S	s	k7c7
Coupé	Coupý	k2eAgInPc1d1
</s>
<s>
Audi	Audi	k1gNnSc1
Quattro	Quattro	k1gNnSc1
</s>
<s>
Audi	Audi	k1gNnSc1
V8	V8	k1gFnSc2
</s>
<s>
Audi	Audi	k1gNnSc1
UrS	UrS	k1gFnSc2
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
S	s	k7c7
<g/>
6	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
</s>
<s>
•	•	k?
kz	kz	k?
•	•	k?
d	d	k?
•	•	k?
e	e	k0
Automobily	automobil	k1gInPc1
značky	značka	k1gFnPc1
Audi	Audi	k1gNnSc1
1980	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
1980	#num#	k4
</s>
<s>
199020002010	#num#	k4
</s>
<s>
0123456789012345678901234567890123456789	#num#	k4
</s>
<s>
Malé	Malé	k2eAgInPc1d1
automobily	automobil	k1gInPc1
</s>
<s>
A2	A2	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
Z	Z	kA
<g/>
)	)	kIx)
</s>
<s>
A1	A1	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
X	X	kA
<g/>
)	)	kIx)
</s>
<s>
A1	A1	k4
(	(	kIx(
<g/>
GB	GB	kA
<g/>
)	)	kIx)
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2
střední	střední	k2eAgFnSc1d1
</s>
<s>
A3	A3	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
A3	A3	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
A3	A3	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
Střední	střední	k2eAgFnSc1d1
</s>
<s>
80	#num#	k4
(	(	kIx(
<g/>
B	B	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
80	#num#	k4
(	(	kIx(
<g/>
B	B	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
80	#num#	k4
(	(	kIx(
<g/>
B	B	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A4	A4	k4
(	(	kIx(
<g/>
B	B	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A4	A4	k4
(	(	kIx(
<g/>
B	B	kA
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A4	A4	k4
(	(	kIx(
<g/>
B	B	kA
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A4	A4	k4
(	(	kIx(
<g/>
B	B	kA
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A4	A4	k4
(	(	kIx(
<g/>
B	B	kA
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2
střední	střední	k2eAgFnSc1d1
</s>
<s>
100	#num#	k4
(	(	kIx(
<g/>
C	C	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
100	#num#	k4
(	(	kIx(
<g/>
C	C	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
100	#num#	k4
(	(	kIx(
<g/>
C	C	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A6	A6	k4
(	(	kIx(
<g/>
C	C	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A6	A6	k4
(	(	kIx(
<g/>
C	C	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A6	A6	k4
(	(	kIx(
<g/>
C	C	kA
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A6	A6	k4
(	(	kIx(
<g/>
C	C	kA
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A6	A6	k4
(	(	kIx(
<g/>
C	C	kA
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Luxusní	luxusní	k2eAgFnSc1d1
</s>
<s>
V8	V8	k4
(	(	kIx(
<g/>
D	D	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A8	A8	k4
(	(	kIx(
<g/>
D	D	kA
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A8	A8	k4
(	(	kIx(
<g/>
D	D	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A8	A8	k4
(	(	kIx(
<g/>
D	D	kA
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A8	A8	k4
(	(	kIx(
<g/>
D	D	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kupé	kupé	k1gNnSc1
</s>
<s>
A5	A5	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
T	T	kA
<g/>
)	)	kIx)
</s>
<s>
A5	A5	k4
(	(	kIx(
<g/>
F	F	kA
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
A7	A7	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
G	G	kA
<g/>
)	)	kIx)
</s>
<s>
A7	A7	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1
</s>
<s>
TT	TT	kA
(	(	kIx(
<g/>
8	#num#	k4
<g/>
N	N	kA
<g/>
)	)	kIx)
</s>
<s>
TT	TT	kA
(	(	kIx(
<g/>
8	#num#	k4
<g/>
J	J	kA
<g/>
)	)	kIx)
</s>
<s>
TT	TT	kA
(	(	kIx(
<g/>
8	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
SUV	SUV	kA
</s>
<s>
Q3	Q3	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
U	U	kA
<g/>
)	)	kIx)
</s>
<s>
Q3	Q3	k4
(	(	kIx(
<g/>
F	F	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Q5	Q5	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
R	R	kA
<g/>
)	)	kIx)
</s>
<s>
Q5	Q5	k4
(	(	kIx(
<g/>
80	#num#	k4
<g/>
A	A	kA
<g/>
)	)	kIx)
</s>
<s>
Q7	Q7	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Q7	Q7	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
</s>
<s>
Q8	Q8	k4
</s>
<s>
e-tron	e-tron	k1gMnSc1
SUV	SUV	kA
</s>
<s>
Supersport	supersport	k1gInSc1
</s>
<s>
R8	R8	k4
(	(	kIx(
<g/>
42	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
R8	R8	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
Koncern	koncern	k1gInSc1
Audi	Audi	k1gNnSc1
vyrábí	vyrábět	k5eAaImIp3nS
podle	podle	k7c2
potřeb	potřeba	k1gFnPc2
a	a	k8xC
požadavků	požadavek	k1gInPc2
kupujících	kupující	k2eAgInPc2d1
výrobky	výrobek	k1gInPc7
vysoké	vysoký	k2eAgFnSc2d1
kvality	kvalita	k1gFnSc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gInSc1
odbyt	odbyt	k1gInSc1
a	a	k8xC
péče	péče	k1gFnSc1
o	o	k7c4
ně	on	k3xPp3gMnPc4
jsou	být	k5eAaImIp3nP
v	v	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
zajišťovány	zajišťovat	k5eAaImNgInP
sítí	síť	k1gFnSc7
autorizovaných	autorizovaný	k2eAgInPc2d1
dealerů	dealer	k1gMnPc2
<g/>
,	,	kIx,
za	za	k7c7
účelem	účel	k1gInSc7
dosažení	dosažení	k1gNnSc2
vysoké	vysoký	k2eAgFnSc2d1
spokojenosti	spokojenost	k1gFnSc2
zákazníků	zákazník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
autorizovaní	autorizovaný	k2eAgMnPc1d1
dealeři	dealer	k1gMnPc1
Audi	Audi	k1gNnSc1
<g/>
,	,	kIx,
kterých	který	k3yQgMnPc2,k3yIgMnPc2,k3yRgMnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
v	v	k7c6
ČR	ČR	kA
19	#num#	k4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
splňovat	splňovat	k5eAaImF
kvalitativní	kvalitativní	k2eAgInPc4d1
standardy	standard	k1gInPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
jednotný	jednotný	k2eAgInSc4d1
design	design	k1gInSc4
autosalónů	autosalón	k1gInPc2
<g/>
,	,	kIx,
Corporate	Corporat	k1gInSc5
design	design	k1gInSc1
<g/>
,	,	kIx,
povinný	povinný	k2eAgInSc1d1
počet	počet	k1gInSc1
předváděcích	předváděcí	k2eAgInPc2d1
a	a	k8xC
skladových	skladový	k2eAgInPc2d1
vozů	vůz	k1gInPc2
a	a	k8xC
apod.	apod.	kA
</s>
<s>
Tímto	tento	k3xDgNnSc7
si	se	k3xPyFc3
koncern	koncern	k1gInSc1
Audi	Audi	k1gNnSc1
udržuje	udržovat	k5eAaImIp3nS
kontrolu	kontrola	k1gFnSc4
a	a	k8xC
dohlíží	dohlížet	k5eAaImIp3nS
na	na	k7c4
kvalitu	kvalita	k1gFnSc4
služeb	služba	k1gFnPc2
poskytovanou	poskytovaný	k2eAgFnSc4d1
všem	všecek	k3xTgMnPc3
stávajícím	stávající	k2eAgMnPc3d1
i	i	k8xC
novým	nový	k2eAgMnPc3d1
zákazníkům	zákazník	k1gMnPc3
koncernu	koncern	k1gInSc2
Audi	Audi	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Výrobní	výrobní	k2eAgInPc1d1
závody	závod	k1gInPc1
</s>
<s>
StátMěstoVyráběné	StátMěstoVyráběný	k2eAgInPc1d1
modely	model	k1gInPc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NěmeckoIngolstadtA	NěmeckoIngolstadtA	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
3	#num#	k4
<g/>
,	,	kIx,
RS	RS	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
4	#num#	k4
<g/>
,	,	kIx,
RS	RS	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
5	#num#	k4
<g/>
,	,	kIx,
RS	RS	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
Q	Q	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
SQ5	SQ5	k1gFnSc1
</s>
<s>
NěmeckoNeckarsulmA	NěmeckoNeckarsulmA	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
5	#num#	k4
<g/>
,	,	kIx,
RS	RS	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
6	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
6	#num#	k4
<g/>
,	,	kIx,
RS	RS	kA
<g/>
6	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
7	#num#	k4
<g/>
,	,	kIx,
S	s	k7c7
<g/>
7	#num#	k4
<g/>
,	,	kIx,
RS	RS	kA
<g/>
7	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
8	#num#	k4
<g/>
,	,	kIx,
R	R	kA
<g/>
8	#num#	k4
<g/>
,	,	kIx,
S8	S8	k1gFnSc1
</s>
<s>
BelgieBruselA	BelgieBruselA	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
S1	S1	k1gFnSc1
</s>
<s>
MaďarskoGyőrA	MaďarskoGyőrA	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
TT	TT	kA
<g/>
,	,	kIx,
TTS	TTS	kA
</s>
<s>
SlovenskoBratislavaQ	SlovenskoBratislavaQ	k?
<g/>
7	#num#	k4
<g/>
,	,	kIx,
Q8	Q8	k1gFnSc1
</s>
<s>
ŠpanělskoMartorellQ	ŠpanělskoMartorellQ	k?
<g/>
3	#num#	k4
</s>
<s>
RuskoKalugaA	RuskoKalugaA	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
7	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
8	#num#	k4
<g/>
,	,	kIx,
Q	Q	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
Q7	Q7	k1gFnSc1
</s>
<s>
IndieAurangabádA	IndieAurangabádA	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
4	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
6	#num#	k4
<g/>
,	,	kIx,
Q	Q	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
Q	Q	kA
<g/>
5	#num#	k4
<g/>
,	,	kIx,
Q7	Q7	k1gFnSc1
</s>
<s>
ČínaFo-šanA	ČínaFo-šanA	k?
<g/>
3	#num#	k4
</s>
<s>
ČínaČchang-čchunA	ČínaČchang-čchunA	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
A	A	kA
<g/>
6	#num#	k4
<g/>
,	,	kIx,
Q	Q	kA
<g/>
3	#num#	k4
<g/>
,	,	kIx,
Q5	Q5	k1gFnSc1
</s>
<s>
Kromě	kromě	k7c2
toho	ten	k3xDgNnSc2
je	být	k5eAaImIp3nS
malé	malý	k2eAgNnSc1d1
množství	množství	k1gNnSc1
aut	aut	k1gInSc4
montováno	montován	k2eAgNnSc1d1
v	v	k7c6
indonéské	indonéský	k2eAgFnSc6d1
Jakartě	Jakarta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Motorsport	Motorsport	k1gInSc1
</s>
<s>
Audi	Audi	k1gNnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejúspěšnější	úspěšný	k2eAgFnPc4d3
automobilové	automobilový	k2eAgFnPc4d1
značky	značka	k1gFnPc4
ve	v	k7c6
světě	svět	k1gInSc6
motoristického	motoristický	k2eAgInSc2d1
sportu	sport	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
sbíraly	sbírat	k5eAaImAgFnP
vavříny	vavřín	k1gInPc7
slavné	slavný	k2eAgNnSc1d1
závodní	závodní	k2eAgInPc1d1
vozy	vůz	k1gInPc1
Velkých	velký	k2eAgFnPc2d1
cen	cena	k1gFnPc2
značky	značka	k1gFnSc2
Auto	auto	k1gNnSc1
Union	union	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
novodobé	novodobý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
použila	použít	k5eAaPmAgFnS
značka	značka	k1gFnSc1
Audi	Audi	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
výrobce	výrobce	k1gMnSc1
úspěšně	úspěšně	k6eAd1
pohon	pohon	k1gInSc4
všech	všecek	k3xTgNnPc2
kol	kolo	k1gNnPc2
quattro	quattro	k6eAd1
v	v	k7c6
automobilových	automobilový	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
technickou	technický	k2eAgFnSc7d1
novinkou	novinka	k1gFnSc7
soutěžního	soutěžní	k2eAgInSc2d1
vozu	vůz	k1gInSc2
Audi	Audi	k1gNnSc2
quattro	quattro	k6eAd1
byl	být	k5eAaImAgInS
motor	motor	k1gInSc1
přeplňovaný	přeplňovaný	k2eAgInSc1d1
turbodmychadlem	turbodmychadlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
touto	tento	k3xDgFnSc7
technikou	technika	k1gFnSc7
byla	být	k5eAaImAgFnS
značka	značka	k1gFnSc1
Audi	Audi	k1gNnSc2
dominantní	dominantní	k2eAgFnSc7d1
silou	síla	k1gFnSc7
ve	v	k7c6
světových	světový	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1984	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1982	#num#	k4
a	a	k8xC
1984	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
v	v	k7c6
rallye	rallye	k1gNnSc6
v	v	k7c6
hodnocení	hodnocení	k1gNnSc6
značek	značka	k1gFnPc2
(	(	kIx(
<g/>
skupina	skupina	k1gFnSc1
B	B	kA
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1983	#num#	k4
a	a	k8xC
1984	#num#	k4
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
mistry	mistr	k1gMnPc7
světa	svět	k1gInSc2
se	s	k7c7
soutěžními	soutěžní	k2eAgInPc7d1
vozy	vůz	k1gInPc7
Audi	Audi	k1gNnSc1
quattro	quattro	k1gNnSc1
<g/>
,	,	kIx,
resp.	resp.	kA
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
quattro	quattro	k6eAd1
jezdci	jezdec	k1gMnPc1
Hannu	Hann	k1gInSc2
Mikkola	Mikkola	k1gFnSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Stig	Stig	k1gMnSc1
Blomqvist	Blomqvist	k1gMnSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozy	vůz	k1gInPc1
Audi	Audi	k1gNnSc1
Sport	sport	k1gInSc1
quattro	quattro	k6eAd1
vyhrály	vyhrát	k5eAaPmAgFnP
v	v	k7c6
letech	léto	k1gNnPc6
1985	#num#	k4
až	až	k9
1987	#num#	k4
také	také	k9
slavný	slavný	k2eAgInSc1d1
závod	závod	k1gInSc1
do	do	k7c2
vrchu	vrch	k1gInSc2
na	na	k7c4
Pikes	Pikes	k1gInSc4
Peak	Peak	k1gInSc1
(	(	kIx(
<g/>
Michè	Michè	k1gFnSc1
Mouton	Mouton	k1gInSc1
<g/>
,	,	kIx,
Bobby	Bobb	k1gInPc1
Unser	Unser	k1gInSc1
a	a	k8xC
Walter	Walter	k1gMnSc1
Röhrl	Röhrl	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pohon	pohon	k1gInSc1
všech	všecek	k3xTgNnPc2
kol	kolo	k1gNnPc2
quattro	quattro	k6eAd1
slavil	slavit	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
velké	velký	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
také	také	k9
v	v	k7c6
závodech	závod	k1gInPc6
cestovních	cestovní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
na	na	k7c6
okruzích	okruh	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
ovládlo	ovládnout	k5eAaPmAgNnS
Audi	Audi	k1gNnSc1
americký	americký	k2eAgInSc1d1
seriál	seriál	k1gInSc4
TransAm	TransAmo	k1gNnPc2
s	s	k7c7
vozem	vůz	k1gInSc7
Audi	Audi	k1gNnPc2
200	#num#	k4
quattro	quattro	k6eAd1
TransAm	TransAm	k1gMnSc1
(	(	kIx(
<g/>
jezdec	jezdec	k1gMnSc1
<g/>
:	:	kIx,
Harley	harley	k1gInSc1
Haywood	Haywood	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1990	#num#	k4
a	a	k8xC
1991	#num#	k4
dominovaly	dominovat	k5eAaImAgInP
německému	německý	k2eAgNnSc3d1
mistrovství	mistrovství	k1gNnSc3
cestovních	cestovní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
vozy	vůz	k1gInPc7
Audi	Audi	k1gNnSc1
V8	V8	k1gFnSc1
quattro	quattro	k1gNnSc1
DTM	DTM	kA
(	(	kIx(
<g/>
Hans-Joachim	Hans-Joachim	k1gMnSc1
Stuck	Stuck	k1gMnSc1
a	a	k8xC
Frank	Frank	k1gMnSc1
Biela	Biela	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
získaly	získat	k5eAaPmAgFnP
řadu	řada	k1gFnSc4
úspěchů	úspěch	k1gInPc2
v	v	k7c6
závodech	závod	k1gInPc6
cestovních	cestovní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
jezdci	jezdec	k1gMnPc1
s	s	k7c7
vozy	vůz	k1gInPc7
Audi	Audi	k1gNnSc2
80	#num#	k4
quattro	quattro	k1gNnSc1
a	a	k8xC
Audi	Audi	k1gNnSc1
A4	A4	k1gFnSc2
Supertouring	Supertouring	k1gInSc1
quattro	quattro	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
vítězství	vítězství	k1gNnSc3
v	v	k7c6
prestižním	prestižní	k2eAgInSc6d1
seriálu	seriál	k1gInSc6
DTM	DTM	kA
(	(	kIx(
<g/>
Deutsche	Deutsche	k1gFnSc1
Tourenwagen-Masters	Tourenwagen-Mastersa	k1gFnPc2
<g/>
)	)	kIx)
patřila	patřit	k5eAaImAgFnS
značce	značka	k1gFnSc3
Audi	Audi	k1gNnSc7
také	také	k9
v	v	k7c6
letech	léto	k1gNnPc6
2004	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
a	a	k8xC
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
dominuje	dominovat	k5eAaImIp3nS
Audi	Audi	k1gNnSc2
vytrvalostnímu	vytrvalostní	k2eAgInSc3d1
závodu	závod	k1gInSc3
24	#num#	k4
h	h	k?
Le	Le	k1gMnSc4
Mans	Mans	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
mnohá	mnohý	k2eAgNnPc4d1
prvenství	prvenství	k1gNnPc4
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
první	první	k4xOgNnSc1
vítězství	vítězství	k1gNnSc1
vozu	vůz	k1gInSc2
poháněného	poháněný	k2eAgInSc2d1
vznětovým	vznětový	k2eAgInSc7d1
motorem	motor	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
(	(	kIx(
<g/>
Audi	Audi	k1gNnSc1
R10	R10	k1gFnPc2
TDI	TDI	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
první	první	k4xOgNnSc4
vítězství	vítězství	k1gNnSc4
vozu	vůz	k1gInSc2
s	s	k7c7
hybridním	hybridní	k2eAgInSc7d1
pohonem	pohon	k1gInSc7
(	(	kIx(
<g/>
R	R	kA
<g/>
18	#num#	k4
e-tron	e-tron	k1gInSc1
quattro	quattro	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2000	#num#	k4
až	až	k9
2014	#num#	k4
vyhrály	vyhrát	k5eAaPmAgFnP
závodní	závodní	k2eAgInSc4d1
vozy	vůz	k1gInPc4
Audi	Audi	k1gNnSc2
legendární	legendární	k2eAgInSc4d1
závod	závod	k1gInSc4
24	#num#	k4
h	h	k?
Le	Le	k1gFnSc2
Mans	Mansa	k1gFnPc2
třináctkrát	třináctkrát	k6eAd1
a	a	k8xC
na	na	k7c4
historicky	historicky	k6eAd1
nejúspěšnější	úspěšný	k2eAgFnSc4d3
značku	značka	k1gFnSc4
Porsche	Porsche	k1gNnSc2
s	s	k7c7
16	#num#	k4
vítězstvími	vítězství	k1gNnPc7
ztrácí	ztrácet	k5eAaImIp3nP
Audi	Audi	k1gNnPc4
už	už	k6eAd1
jen	jen	k6eAd1
pouhé	pouhý	k2eAgMnPc4d1
tři	tři	k4xCgInPc4
triumfy	triumf	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Audi	Audi	k1gNnSc1
R15	R15	k1gFnPc2
TDI	TDI	kA
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
zvítězilo	zvítězit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
držitelem	držitel	k1gMnSc7
rekordu	rekord	k1gInSc2
v	v	k7c6
ujeté	ujetý	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
během	během	k7c2
čtyřiadvacetihodinového	čtyřiadvacetihodinový	k2eAgInSc2d1
závodu	závod	k1gInSc2
<g/>
:	:	kIx,
5410,71	5410,71	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
značek	značka	k1gFnPc2
automobilů	automobil	k1gInPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
.1	.1	k4
2	#num#	k4
Audi	Audi	k1gNnPc2
2014	#num#	k4
Annual	Annual	k1gInSc1
Report	report	k1gInSc4
<g/>
.	.	kIx.
www.volkswagenag.com	www.volkswagenag.com	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.audi.com/corporate/en/company/audi-at-a-glance.html	http://www.audi.com/corporate/en/company/audi-at-a-glance.html	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Článek	článek	k1gInSc4
k	k	k7c3
100	#num#	k4
<g/>
-mu	-mu	k?
výročí	výročí	k1gNnSc4
značky	značka	k1gFnSc2
audi	audi	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Audi	Audi	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
české	český	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Klub	klub	k1gInSc1
příznivců	příznivec	k1gMnPc2
značky	značka	k1gFnSc2
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
současnost	současnost	k1gFnSc1
automobilky	automobilka	k1gFnSc2
v	v	k7c6
časové	časový	k2eAgFnSc6d1
ose	osa	k1gFnSc6
(	(	kIx(
<g/>
audiklub	audiklub	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
</s>
<s>
Stránky	stránka	k1gFnPc1
majitelů	majitel	k1gMnPc2
a	a	k8xC
příznivců	příznivec	k1gMnPc2
vozů	vůz	k1gInPc2
značky	značka	k1gFnSc2
</s>
<s>
Volkswagen	volkswagen	k1gInSc1
AG	AG	kA
Group	Group	k1gInSc4
Osobní	osobní	k2eAgInPc4d1
automobily	automobil	k1gInPc4
</s>
<s>
Audi	Audi	k1gNnSc1
•	•	k?
Bentley	Bentlea	k1gMnSc2
•	•	k?
Bugatti	Bugatť	k1gFnSc2
•	•	k?
Lamborghini	Lamborghin	k1gMnPc1
•	•	k?
SEAT	SEAT	kA
•	•	k?
Porsche	Porsche	k1gNnSc1
•	•	k?
Škoda	Škoda	k1gMnSc1
•	•	k?
Volkswagen	volkswagen	k1gInSc1
Nákladní	nákladní	k2eAgInPc1d1
automobily	automobil	k1gInPc1
a	a	k8xC
další	další	k2eAgNnPc1d1
</s>
<s>
Volkswagen	volkswagen	k1gInSc1
Užitkové	užitkový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
•	•	k?
Scania	Scanium	k1gNnSc2
•	•	k?
MAN	Man	k1gMnSc1
(	(	kIx(
<g/>
podíl	podíl	k1gInSc1
75,03	75,03	k4
%	%	kIx~
<g/>
)	)	kIx)
•	•	k?
Ducati	ducat	k5eAaImF
(	(	kIx(
<g/>
motocykly	motocykl	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Německé	německý	k2eAgFnPc1d1
automobilky	automobilka	k1gFnPc1
do	do	k7c2
roku	rok	k1gInSc2
1918	#num#	k4
</s>
<s>
Aachener	Aachener	k1gInSc1
•	•	k?
AAG	AAG	kA
•	•	k?
ABAM	ABAM	kA
•	•	k?
Adler	Adler	k1gMnSc1
•	•	k?
Aegir	Aegir	k1gMnSc1
•	•	k?
Alliance	Alliance	k1gFnSc2
•	•	k?
Allright	Allright	k1gMnSc1
•	•	k?
Altmann	Altmann	k1gMnSc1
•	•	k?
AMG	AMG	kA
•	•	k?
Andreas	Andreas	k1gMnSc1
•	•	k?
Anker	Anker	k1gMnSc1
•	•	k?
Ansbach	Ansbach	k1gMnSc1
•	•	k?
Apollo	Apollo	k1gMnSc1
•	•	k?
Argus	Argus	k1gMnSc1
•	•	k?
Asdomobil	Asdomobil	k1gMnSc1
•	•	k?
Äskulap	Äskulap	k1gMnSc1
•	•	k?
Attila	Attila	k1gMnSc1
•	•	k?
Audi	Audi	k1gNnSc4
•	•	k?
Autognom	Autognom	k1gInSc1
•	•	k?
Beckmann	Beckmann	k1gInSc1
•	•	k?
B.E.	B.E.	k1gFnSc2
<g/>
F.	F.	kA
•	•	k?
Behr	Behr	k1gMnSc1
•	•	k?
Benz	Benz	k1gMnSc1
•	•	k?
Berger	Berger	k1gMnSc1
•	•	k?
Bergmann	Bergmann	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Metallurgique	Metallurgique	k1gFnSc1
•	•	k?
Berolina	Berolina	k1gFnSc1
•	•	k?
Blitz	Blitz	k1gInSc1
•	•	k?
BMF	BMF	kA
•	•	k?
Boes	Boes	k1gInSc1
•	•	k?
Brandt	Brandt	k1gMnSc1
•	•	k?
Braun	Braun	k1gMnSc1
•	•	k?
Brennabor	Brennabor	k1gMnSc1
•	•	k?
Bugatti	Bugatť	k1gFnSc2
•	•	k?
C.	C.	kA
Benz	Benz	k1gInSc1
Söhne	Söhn	k1gInSc5
•	•	k?
Chatel-Jeannin	Chatel-Jeannina	k1gFnPc2
•	•	k?
Cito	Cito	k1gMnSc1
•	•	k?
Cloumobil	Cloumobil	k1gMnSc1
•	•	k?
Colibri	Colibr	k1gFnSc2
•	•	k?
Corona	Corona	k1gFnSc1
•	•	k?
COS	cos	kA
•	•	k?
Coswiga	Coswiga	k1gFnSc1
•	•	k?
Cudell	Cudell	k1gInSc1
•	•	k?
Cyklon	cyklon	k1gInSc1
•	•	k?
Daimler	Daimler	k1gInSc1
•	•	k?
De	De	k?
Dietrich	Dietrich	k1gInSc1
•	•	k?
Deka	deka	k1gFnSc1
•	•	k?
Dessauer	Dessauer	k1gInSc1
•	•	k?
Dessavia	Dessavia	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Deutschland	Deutschland	k1gInSc1
•	•	k?
Deutz	Deutz	k1gInSc1
•	•	k?
Dixi	Dix	k1gFnSc2
•	•	k?
Dreyhaupt	Dreyhaupt	k1gMnSc1
•	•	k?
Ducommun	Ducommun	k1gMnSc1
•	•	k?
Dürkopp	Dürkopp	k1gMnSc1
•	•	k?
Dux	Dux	k1gMnSc1
•	•	k?
Dynamobil	Dynamobil	k1gMnSc1
•	•	k?
EBM	EBM	kA
•	•	k?
Ehrhardt	Ehrhardt	k1gInSc1
•	•	k?
Ehrhardt-Decauville	Ehrhardt-Decauville	k1gInSc1
•	•	k?
Einrad	Einrad	k1gInSc1
•	•	k?
Ekamobil	Ekamobil	k1gMnSc2
•	•	k?
Electra	Electr	k1gMnSc2
•	•	k?
Elite	Elit	k1gInSc5
•	•	k?
EMW	EMW	kA
•	•	k?
Engelhardt	Engelhardt	k1gMnSc1
•	•	k?
Erdmann	Erdmann	k1gMnSc1
•	•	k?
Excelsior-Mascot	Excelsior-Mascot	k1gMnSc1
•	•	k?
Express	express	k1gInSc1
•	•	k?
FAF	FAF	kA
•	•	k?
Fafnir	Fafnir	k1gInSc1
•	•	k?
Falke	Falke	k1gInSc1
•	•	k?
Favorit	favorit	k1gInSc1
•	•	k?
F.	F.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
E.G.	E.G.	k1gMnSc1
•	•	k?
Feldmann	Feldmann	k1gMnSc1
•	•	k?
Fiedler	Fiedler	k1gMnSc1
•	•	k?
Fischer	Fischer	k1gMnSc1
(	(	kIx(
<g/>
1902	#num#	k4
<g/>
–	–	k?
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fischer	Fischer	k1gMnSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
–	–	k?
<g/>
1913	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Flocken	Flocken	k1gInSc1
•	•	k?
Foth	Foth	k1gInSc1
•	•	k?
Fulgura	Fulgura	k1gFnSc1
•	•	k?
Fulmina	Fulmin	k2eAgFnSc1d1
•	•	k?
Gaggenau	Gaggena	k1gMnSc3
•	•	k?
Geha	Geha	k1gMnSc1
•	•	k?
Göricke	Göricke	k1gInSc1
•	•	k?
Gottschalk	Gottschalk	k1gInSc1
•	•	k?
Hammonia	Hammonium	k1gNnSc2
•	•	k?
Hansa	hansa	k1gFnSc1
•	•	k?
Hartmann	Hartmann	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Heilbronn	Heilbronn	k1gInSc1
•	•	k?
Heinle	Heinle	k1gInSc1
&	&	k?
Wegelin	Wegelin	k2eAgInSc1d1
•	•	k?
Helios	Helios	k1gInSc1
•	•	k?
Henschel	Henschel	k1gInSc1
•	•	k?
Hermes-Simplex	Hermes-Simplex	k1gInSc1
•	•	k?
Hexe	Hexe	k1gInSc1
•	•	k?
Hille	Hille	k1gInSc1
•	•	k?
Horch	Horch	k1gInSc1
•	•	k?
Hüttis	Hüttis	k1gFnSc2
&	&	k?
Hardebeck	Hardebeck	k1gMnSc1
•	•	k?
Immermobil	Immermobil	k1gMnSc1
•	•	k?
Imperator	Imperator	k1gMnSc1
•	•	k?
Kämper	Kämper	k1gMnSc1
•	•	k?
KAW	KAW	kA
•	•	k?
Kayser	Kayser	k1gInSc1
•	•	k?
Kempten	Kempten	k2eAgInSc1d1
•	•	k?
KEW	KEW	kA
•	•	k?
Kliemt	Kliemt	k1gMnSc1
•	•	k?
Klingenberg	Klingenberg	k1gMnSc1
•	•	k?
Kölner	Kölner	k1gInSc1
Motorwagen	Motorwagen	k1gInSc1
•	•	k?
Komnick	Komnick	k1gMnSc1
•	•	k?
Kondor	kondor	k1gMnSc1
•	•	k?
Kriéger	Kriéger	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Krupkar	Krupkar	k1gMnSc1
•	•	k?
Kruse	Kruse	k1gFnSc2
•	•	k?
Kühlstein	Kühlstein	k1gMnSc1
•	•	k?
Lerche	Lerch	k1gFnSc2
•	•	k?
Liliput	liliput	k1gMnSc1
•	•	k?
Lloyd	Lloyd	k1gMnSc1
•	•	k?
Loreley	Lorelea	k1gFnSc2
•	•	k?
Loutzky	Loutzka	k1gFnSc2
•	•	k?
LUC	LUC	kA
•	•	k?
Lueders	Lueders	k1gInSc1
•	•	k?
Lutzmann	Lutzmann	k1gMnSc1
•	•	k?
Lux	Lux	k1gMnSc1
•	•	k?
MAF	MAF	kA
•	•	k?
Magdeburger	Magdeburger	k1gInSc1
•	•	k?
Magnet	magnet	k1gInSc1
•	•	k?
Mars	Mars	k1gInSc1
•	•	k?
Martinot	Martinota	k1gFnPc2
et	et	k?
Galland	Galland	k1gInSc1
•	•	k?
Mathis	Mathis	k1gInSc1
•	•	k?
Maurer	Maurer	k1gInSc1
Union	union	k1gInSc1
•	•	k?
Maxwerke	Maxwerk	k1gFnSc2
•	•	k?
Mayer	Mayer	k1gMnSc1
•	•	k?
Mercedes	mercedes	k1gInSc1
•	•	k?
Meyrel	Meyrel	k1gInSc1
•	•	k?
Miele	Miele	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
MMB	MMB	kA
•	•	k?
Mono	mono	k2eAgFnSc1d1
•	•	k?
MWD	MWD	kA
•	•	k?
Nacke	Nacke	k1gInSc1
•	•	k?
N.	N.	kA
<g/>
A.G.	A.G.	k1gMnSc3
•	•	k?
NAIG	NAIG	kA
•	•	k?
Neuss	Neuss	k1gInSc1
•	•	k?
Noris	Noris	k1gFnSc2
•	•	k?
NSU	NSU	kA
•	•	k?
Opel	opel	k1gInSc1
•	•	k?
Orient	Orient	k1gInSc1
Express	express	k1gInSc1
•	•	k?
Oryx	Oryx	k1gInSc1
•	•	k?
Panther	Panthra	k1gFnPc2
•	•	k?
Pasing	Pasing	k1gInSc1
•	•	k?
Patria	Patrium	k1gNnSc2
•	•	k?
Pekrun	Pekrun	k1gMnSc1
•	•	k?
Pflüger	Pflüger	k1gInSc1
•	•	k?
Phänomen	Phänomen	k2eAgInSc1d1
•	•	k?
Piccolo	Piccola	k1gFnSc5
•	•	k?
Planet	planeta	k1gFnPc2
•	•	k?
Podeus	Podeus	k1gMnSc1
•	•	k?
Polymobil	Polymobil	k1gMnSc1
•	•	k?
Presto	presto	k6eAd1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Priamus	Priamus	k1gMnSc1
•	•	k?
Primus	primus	k1gMnSc1
•	•	k?
Progress	Progress	k1gInSc1
•	•	k?
Protos	Protos	k1gInSc1
•	•	k?
Rabe-Mobil	Rabe-Mobil	k1gFnSc2
•	•	k?
RAW	RAW	kA
•	•	k?
Record	Record	k1gMnSc1
•	•	k?
Regent	regent	k1gMnSc1
•	•	k?
Reissig	Reissig	k1gMnSc1
•	•	k?
Rex-Simplex	Rex-Simplex	k1gInSc1
•	•	k?
SAF	SAF	kA
•	•	k?
Scheele	Scheel	k1gInSc2
•	•	k?
Scheibler	Scheibler	k1gMnSc1
•	•	k?
Schilling	schilling	k1gInSc1
•	•	k?
Schuckert	Schuckert	k1gMnSc1
•	•	k?
Schulz	Schulz	k1gMnSc1
•	•	k?
Seidel	Seidel	k1gMnSc1
&	&	k?
Naumann	Naumann	k1gMnSc1
•	•	k?
Sekurus	Sekurus	k1gMnSc1
•	•	k?
Siegel	Siegel	k1gMnSc1
•	•	k?
Siegfried	Siegfried	k1gMnSc1
•	•	k?
Siemens-Schuckert	Siemens-Schuckert	k1gMnSc1
•	•	k?
Simson	Simson	k1gMnSc1
•	•	k?
Solidor	Solidor	k1gMnSc1
•	•	k?
Sperber	Sperber	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Standard	standard	k1gInSc1
•	•	k?
Stella	Stella	k1gFnSc1
•	•	k?
Steudel	Steudlo	k1gNnPc2
•	•	k?
Stoewer	Stoewer	k1gMnSc1
•	•	k?
Sun	Sun	kA
•	•	k?
Superior	superior	k1gMnSc1
•	•	k?
Taifun	Taifun	k1gMnSc1
•	•	k?
Taunus	Taunus	k1gMnSc1
•	•	k?
Tempelhof	Tempelhof	k1gMnSc1
•	•	k?
Thüringer	Thüringer	k1gMnSc1
Motorwagenfabrik	Motorwagenfabrik	k1gMnSc1
•	•	k?
Tippmann	Tippmann	k1gMnSc1
•	•	k?
Tourist	Tourist	k1gMnSc1
•	•	k?
Treskow	Treskow	k1gMnSc1
•	•	k?
Triomobil	Triomobil	k1gMnSc1
•	•	k?
Ulmann	Ulmann	k1gMnSc1
•	•	k?
Ultramobile	Ultramobila	k1gFnSc6
•	•	k?
Utermöhle	Utermöhle	k1gFnSc2
•	•	k?
VCS	VCS	kA
•	•	k?
Velomobil	Velomobil	k1gMnSc1
•	•	k?
Vesuv	Vesuv	k1gInSc1
•	•	k?
Victoria	Victorium	k1gNnSc2
•	•	k?
Vindelica	Vindelica	k1gMnSc1
•	•	k?
Vogtland	Vogtland	k1gInSc1
•	•	k?
Vulkan	Vulkan	k1gInSc1
•	•	k?
Wanderer	Wanderer	k1gInSc1
•	•	k?
Wartburg	Wartburg	k1gInSc1
•	•	k?
Weichelt	Weichelt	k1gMnSc1
•	•	k?
Weiss	Weiss	k1gMnSc1
•	•	k?
Wenkelmobil	Wenkelmobil	k1gMnSc1
•	•	k?
Wesen	Wesen	k1gInSc1
•	•	k?
Westfalia	Westfalius	k1gMnSc2
•	•	k?
Windhoff	Windhoff	k1gMnSc1
•	•	k?
Wunderlich	Wunderlich	k1gMnSc1
•	•	k?
Zentralmobil	Zentralmobil	k1gMnSc1
</s>
<s>
Německé	německý	k2eAgFnSc2d1
automobilové	automobilový	k2eAgFnSc2d1
značky	značka	k1gFnSc2
1918	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
</s>
<s>
AAA	AAA	kA
•	•	k?
ABC	ABC	kA
•	•	k?
Adler	Adler	k1gMnSc1
•	•	k?
AGA	aga	k1gMnSc1
•	•	k?
Alan	Alan	k1gMnSc1
•	•	k?
Alfi	Alf	k1gFnSc2
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alfi	Alfi	k1gNnSc4
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AMBAG	AMBAG	kA
•	•	k?
Amor	Amor	k1gMnSc1
•	•	k?
Anker	Anker	k1gMnSc1
•	•	k?
Apollo	Apollo	k1gMnSc1
•	•	k?
Argeo	Argeo	k1gNnSc4
•	•	k?
Arimofa	Arimof	k1gMnSc2
•	•	k?
Atlantic	Atlantice	k1gFnPc2
•	•	k?
Audi	Audi	k1gNnSc4
•	•	k?
Auto-Ell	Auto-Ell	k1gInSc1
•	•	k?
Badenia	Badenium	k1gNnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Baer	Baer	k1gMnSc1
•	•	k?
BAW	BAW	kA
•	•	k?
BEB	BEB	kA
•	•	k?
Beckmann	Beckmann	k1gMnSc1
•	•	k?
Benz	Benz	k1gMnSc1
•	•	k?
Bergmann	Bergmann	k1gMnSc1
•	•	k?
Bergo	Bergo	k1gMnSc1
•	•	k?
BF	BF	kA
•	•	k?
Biene	Bien	k1gInSc5
•	•	k?
Bleichert	Bleichert	k1gInSc1
•	•	k?
BMW	BMW	kA
•	•	k?
Bob	Bob	k1gMnSc1
•	•	k?
Borcharding	Borcharding	k1gInSc1
•	•	k?
Borgward	Borgward	k1gMnSc1
•	•	k?
Bravo	bravo	k1gMnSc1
•	•	k?
Brennabor	Brennabor	k1gMnSc1
•	•	k?
Bufag	Bufag	k1gMnSc1
•	•	k?
Bully	bulla	k1gFnSc2
•	•	k?
Butz	Butz	k1gMnSc1
•	•	k?
BZ	bz	k0
•	•	k?
C.	C.	kA
Benz	Benz	k1gInSc1
Söhne	Söhn	k1gInSc5
•	•	k?
Certus	Certus	k1gInSc1
•	•	k?
Club	club	k1gInSc1
•	•	k?
Cockerell	Cockerell	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Combi	Comb	k1gFnSc2
•	•	k?
Cyklon	cyklon	k1gInSc1
•	•	k?
Davidl	Davidl	k1gFnSc2
•	•	k?
Dehn	Dehn	k1gInSc1
•	•	k?
DEW	DEW	kA
•	•	k?
Diabolo	diabolo	k1gNnSc1
•	•	k?
Diana	Diana	k1gFnSc1
•	•	k?
Dinos	Dinos	k1gInSc1
•	•	k?
Dixi	Dix	k1gFnSc2
•	•	k?
DKW	DKW	kA
•	•	k?
Dorner	Dorner	k1gMnSc1
•	•	k?
Dürkopp	Dürkopp	k1gMnSc1
•	•	k?
Dux	Dux	k1gMnSc1
•	•	k?
D-Wagen	D-Wagen	k1gInSc1
•	•	k?
EBS	EBS	kA
•	•	k?
Ego	ego	k1gNnSc2
•	•	k?
Ehrhardt	Ehrhardt	k1gMnSc1
•	•	k?
Ehrhardt-Szawe	Ehrhardt-Szawe	k1gInSc1
•	•	k?
Eibach	Eibach	k1gInSc1
•	•	k?
Electra	Electr	k1gMnSc2
•	•	k?
Elektric	Elektric	k1gMnSc1
•	•	k?
Elite	Elit	k1gInSc5
•	•	k?
Elitewagen	Elitewagen	k1gInSc1
•	•	k?
Eos	Eos	k1gFnSc2
•	•	k?
Erco	Erco	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Espenlaub	Espenlaub	k1gInSc1
•	•	k?
Eubu	Eubus	k1gInSc2
•	•	k?
Exor	Exor	k1gMnSc1
•	•	k?
Fadag	Fadag	k1gMnSc1
•	•	k?
Fafag	Fafag	k1gMnSc1
•	•	k?
Fafnir	Fafnir	k1gMnSc1
•	•	k?
Falcon	Falcon	k1gMnSc1
•	•	k?
Fama	Fama	k?
•	•	k?
Faun	fauna	k1gFnPc2
•	•	k?
Ferbedo	Ferbedo	k1gNnSc4
•	•	k?
Ford	ford	k1gInSc1
•	•	k?
Fox	fox	k1gInSc1
•	•	k?
Framo	Frama	k1gFnSc5
•	•	k?
Freia	Freium	k1gNnSc2
•	•	k?
Fulmina	Fulmin	k2eAgFnSc1d1
•	•	k?
Garbaty	Garbat	k1gInPc4
•	•	k?
Gasi	Gas	k1gFnSc2
•	•	k?
Goliath	Goliath	k1gInSc1
•	•	k?
Görke	Görke	k1gInSc1
•	•	k?
Grade	grad	k1gInSc5
•	•	k?
Gridi	Grid	k1gMnPc1
•	•	k?
Gries	Gries	k1gMnSc1
•	•	k?
Habag	Habag	k1gMnSc1
•	•	k?
HAG	HAG	kA
•	•	k?
HAG-Gastell	HAG-Gastell	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Hagea-Moto	Hagea-Mota	k1gFnSc5
•	•	k?
Hanomag	Hanomag	k1gInSc1
•	•	k?
Hansa	hansa	k1gFnSc1
•	•	k?
Hansa-Lloyd	Hansa-Lloyd	k1gInSc1
•	•	k?
Hascho	Hascha	k1gFnSc5
•	•	k?
Hataz	Hataz	k1gInSc1
•	•	k?
Hawa	Haw	k1gInSc2
•	•	k?
Heim	Heim	k1gMnSc1
•	•	k?
Helios	Helios	k1gMnSc1
•	•	k?
Helo	Hela	k1gFnSc5
•	•	k?
Hercules	Hercules	k1gMnSc1
•	•	k?
Hero	Hero	k1gMnSc1
•	•	k?
Hildebrand	Hildebrand	k1gInSc1
•	•	k?
Hiller	Hiller	k1gInSc1
•	•	k?
Horch	Horch	k1gInSc1
•	•	k?
HT	HT	kA
•	•	k?
Imperia	Imperium	k1gNnSc2
•	•	k?
Induhag	Induhag	k1gMnSc1
•	•	k?
Ipe	Ipe	k1gMnSc1
•	•	k?
Joswin	Joswin	k1gMnSc1
•	•	k?
Juhö	Juhö	k1gMnSc1
•	•	k?
Kaha	Kaha	k1gMnSc1
•	•	k?
Kaiser	Kaiser	k1gMnSc1
•	•	k?
Keitel	Keitel	k1gMnSc1
•	•	k?
Kenter	Kenter	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Kico	Kico	k1gMnSc1
•	•	k?
Kieling	Kieling	k1gInSc1
•	•	k?
Knöllner	Knöllner	k1gMnSc1
•	•	k?
Kobold	kobold	k1gMnSc1
•	•	k?
Koco	Koco	k6eAd1
•	•	k?
Komet	kometa	k1gFnPc2
•	•	k?
Komnick	Komnick	k1gMnSc1
•	•	k?
Körting	Körting	k1gInSc1
•	•	k?
Kühn	Kühn	k1gInSc1
•	•	k?
Landgrebe	Landgreb	k1gInSc5
•	•	k?
Lauer	Lauer	k1gMnSc1
•	•	k?
Leichtauto	Leichtaut	k2eAgNnSc1d1
•	•	k?
Leifa	Leif	k1gMnSc2
•	•	k?
Lesshaft	Lesshaft	k1gMnSc1
•	•	k?
Ley	Lea	k1gFnSc2
•	•	k?
Libelle	Libelle	k1gFnSc2
•	•	k?
Lindcar	Lindcar	k1gMnSc1
•	•	k?
Lipsia	Lipsia	k1gFnSc1
•	•	k?
Loeb	Loeb	k1gInSc1
•	•	k?
Luther	Luthra	k1gFnPc2
&	&	k?
Heyer	Heyer	k1gMnSc1
•	•	k?
LuWe	LuW	k1gFnSc2
•	•	k?
Luwo	Luwo	k1gMnSc1
•	•	k?
Lux	Lux	k1gMnSc1
•	•	k?
Macu	Maca	k1gFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
MAF	MAF	kA
•	•	k?
Magnet	magnet	k1gInSc1
•	•	k?
Maier	Maier	k1gInSc1
•	•	k?
Maja	Maja	k1gFnSc1
•	•	k?
Mannesmann	Mannesmann	k1gInSc1
•	•	k?
Martinette	Martinett	k1gInSc5
•	•	k?
Maurer	Maurer	k1gMnSc1
•	•	k?
Mauser	Mauser	k1gMnSc1
•	•	k?
Maybach	Maybach	k1gMnSc1
•	•	k?
Mayrette	Mayrett	k1gInSc5
•	•	k?
Mercedes	mercedes	k1gInSc1
•	•	k?
Mercedes-Benz	Mercedes-Benz	k1gInSc1
•	•	k?
MFB	MFB	kA
•	•	k?
Mikromobil	Mikromobil	k1gMnSc1
•	•	k?
Minimus	Minimus	k1gMnSc1
•	•	k?
Möckwagen	Möckwagen	k1gInSc1
•	•	k?
Mölkamp	Mölkamp	k1gInSc1
•	•	k?
Moll	moll	k1gNnSc2
•	•	k?
Monos	Monos	k1gMnSc1
•	•	k?
Mops	mops	k1gMnSc1
•	•	k?
Morgan	morgan	k1gMnSc1
•	•	k?
Motobil	Motobil	k1gMnSc1
•	•	k?
Motrix	Motrix	k1gInSc1
•	•	k?
Muvo	Muvo	k6eAd1
•	•	k?
Nafa	Nafa	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
NAG	NAG	kA
•	•	k?
NAG-Presto	NAG-Presta	k1gMnSc5
•	•	k?
NAG-Protos	NAG-Protos	k1gMnSc1
•	•	k?
Nawa	Nawa	k1gMnSc1
•	•	k?
Neander	Neander	k1gMnSc1
•	•	k?
Neiman	Neiman	k1gMnSc1
•	•	k?
Nemalette	Nemalett	k1gInSc5
•	•	k?
Nenndorf	Nenndorf	k1gMnSc1
•	•	k?
Nowa	Nowa	k1gMnSc1
•	•	k?
NSU	NSU	kA
•	•	k?
NSU-Fiat	NSU-Fiat	k1gInSc1
•	•	k?
Nufmobil	Nufmobil	k1gFnSc2
•	•	k?
Nug	Nug	k1gMnSc1
•	•	k?
Omega	omega	k1gFnSc1
•	•	k?
Omikron	omikron	k1gInSc1
•	•	k?
Omnobil	Omnobil	k1gFnSc2
•	•	k?
Onnasch	Onnasch	k1gInSc1
•	•	k?
Opel	opel	k1gInSc1
•	•	k?
Otto	Otto	k1gMnSc1
•	•	k?
Pawi	Paw	k1gFnSc2
•	•	k?
Pe-Ka	Pe-Ka	k1gMnSc1
•	•	k?
Peer	peer	k1gMnSc1
Gynt	Gynt	k1gMnSc1
•	•	k?
Pelikan	Pelikan	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
&	&	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Moritz	moritz	k1gInSc1
•	•	k?
Pfeil	Pfeil	k1gInSc1
•	•	k?
Phänomen	Phänomen	k2eAgInSc1d1
•	•	k?
Pilot	pilot	k1gInSc1
•	•	k?
Pluto	Pluto	k1gMnSc1
•	•	k?
Presto	presto	k6eAd1
•	•	k?
Priamus	Priamus	k1gMnSc1
•	•	k?
Protos	Protos	k1gMnSc1
•	•	k?
Rabag	Rabag	k1gMnSc1
•	•	k?
Remag	Remag	k1gMnSc1
•	•	k?
Renfert	Renfert	k1gMnSc1
•	•	k?
Rex-Simplex	Rex-Simplex	k1gInSc1
•	•	k?
Rhemag	Rhemag	k1gMnSc1
•	•	k?
Rikas	Rikas	k1gMnSc1
•	•	k?
Rivo	Rivo	k1gMnSc1
•	•	k?
Roland	Roland	k1gInSc1
•	•	k?
Röhr	Röhr	k1gInSc1
•	•	k?
Rollfix	Rollfix	k1gInSc1
•	•	k?
Rumpler	Rumpler	k1gInSc1
•	•	k?
Rüttger	Rüttger	k1gInSc1
•	•	k?
RWN	RWN	kA
•	•	k?
Sablatnig-Beuchelt	Sablatnig-Beuchelt	k1gInSc1
•	•	k?
Sauer	Sauer	k1gInSc1
•	•	k?
SB	sb	kA
•	•	k?
Schebera	Schebera	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Schönnagel	Schönnagel	k1gMnSc1
•	•	k?
Schuricht	Schuricht	k1gMnSc1
•	•	k?
Schütte-Lanz	Schütte-Lanz	k1gMnSc1
•	•	k?
Seidel-Arop	Seidel-Arop	k1gInSc1
•	•	k?
Selve	Selev	k1gFnSc2
•	•	k?
SHW	SHW	kA
•	•	k?
Simson	Simson	k1gMnSc1
•	•	k?
Slaby-Beringer	Slaby-Beringer	k1gMnSc1
•	•	k?
Slevogt	Slevogt	k1gMnSc1
•	•	k?
Solomobil	Solomobil	k1gMnSc1
•	•	k?
Sperber	Sperber	k1gMnSc1
•	•	k?
Sphinx	Sphinx	k1gInSc1
•	•	k?
Spinell	Spinell	k1gInSc1
•	•	k?
Staiger	Staiger	k1gInSc1
•	•	k?
Standard	standard	k1gInSc1
•	•	k?
Steiger	Steiger	k1gInSc1
•	•	k?
Stoewer	Stoewer	k1gInSc1
•	•	k?
Stolle	Stolle	k1gFnSc2
•	•	k?
Sun	Sun	kA
•	•	k?
Szawe	Szawe	k1gInSc1
•	•	k?
Tamag	Tamag	k1gInSc1
•	•	k?
Tamm	Tamm	k1gInSc1
•	•	k?
Tatra	Tatra	k1gFnSc1
•	•	k?
Teco	Teco	k6eAd1
•	•	k?
Tempo	tempo	k1gNnSc4
•	•	k?
Theis	Theis	k1gInSc1
•	•	k?
Tornax	Tornax	k1gInSc1
•	•	k?
Tourist	Tourist	k1gInSc1
•	•	k?
Traeger	Traeger	k1gInSc1
•	•	k?
Trinks	Trinks	k1gInSc1
•	•	k?
Trippel	Trippel	k1gInSc1
•	•	k?
Triumph	Triumph	k1gInSc1
•	•	k?
Turbo	turba	k1gFnSc5
Utilitas	Utilitas	k1gInSc4
•	•	k?
VL	VL	kA
•	•	k?
Voran	Voran	k1gInSc1
•	•	k?
Volkswagen	volkswagen	k1gInSc1
•	•	k?
Walmobil	Walmobil	k1gFnSc2
•	•	k?
Wanderer	Wanderer	k1gMnSc1
•	•	k?
Wegmann	Wegmann	k1gMnSc1
•	•	k?
Weise	Weise	k1gFnSc2
•	•	k?
Wesnigk	Wesnigk	k1gInSc1
•	•	k?
Westfalia	Westfalius	k1gMnSc2
•	•	k?
Winkler	Winkler	k1gMnSc1
•	•	k?
Wittekind	Wittekind	k1gMnSc1
•	•	k?
York	York	k1gInSc1
•	•	k?
Zetgelette	Zetgelett	k1gInSc5
•	•	k?
Zündapp	Zündapp	k1gMnSc1
•	•	k?
Zwerg	Zwerg	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
277575-X	277575-X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0229	#num#	k4
7838	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
90636453	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
152573613	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
90636453	#num#	k4
</s>
