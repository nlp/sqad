<p>
<s>
Faber-Castell	Faber-Castell	k1gMnSc1	Faber-Castell
je	být	k5eAaImIp3nS	být
německý	německý	k2eAgMnSc1d1	německý
výrobce	výrobce	k1gMnSc1	výrobce
psacích	psací	k2eAgFnPc2d1	psací
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
čítající	čítající	k2eAgInPc4d1	čítající
asi	asi	k9	asi
2	[number]	k4	2
mld.	mld.	k?	mld.
tužek	tužka	k1gFnPc2	tužka
ročně	ročně	k6eAd1	ročně
je	být	k5eAaImIp3nS	být
Faber-Castell	Faber-Castell	k1gInSc1	Faber-Castell
největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
tužek	tužka	k1gFnPc2	tužka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podnik	podnik	k1gInSc1	podnik
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
podle	podle	k7c2	podle
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
informací	informace	k1gFnPc2	informace
na	na	k7c4	na
7000	[number]	k4	7000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
v	v	k7c6	v
15	[number]	k4	15
dílnách	dílna	k1gFnPc6	dílna
a	a	k8xC	a
23	[number]	k4	23
prodejnách	prodejna	k1gFnPc6	prodejna
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
zastoupení	zastoupení	k1gNnSc4	zastoupení
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
120	[number]	k4	120
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
brazilská	brazilský	k2eAgFnSc1d1	brazilská
filiálka	filiálka	k1gFnSc1	filiálka
v	v	k7c6	v
Sao	Sao	k1gFnSc6	Sao
Carlos	Carlos	k1gMnSc1	Carlos
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
1,5	[number]	k4	1,5
miliardami	miliarda	k4xCgFnPc7	miliarda
vyrobených	vyrobený	k2eAgFnPc2d1	vyrobená
pastelek	pastelka	k1gFnPc2	pastelka
ročně	ročně	k6eAd1	ročně
jejich	jejich	k3xOp3gMnSc7	jejich
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
výrobcem	výrobce	k1gMnSc7	výrobce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
firmy	firma	k1gFnSc2	firma
==	==	k?	==
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
sídlí	sídlet	k5eAaImIp3nS	sídlet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1719	[number]	k4	1719
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Stein	Stein	k1gMnSc1	Stein
u	u	k7c2	u
Norimberku	Norimberk	k1gInSc2	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
se	se	k3xPyFc4	se
truhlář	truhlář	k1gMnSc1	truhlář
Kaspar	Kaspar	k1gMnSc1	Kaspar
Faber	Faber	k1gMnSc1	Faber
pustil	pustit	k5eAaPmAgMnS	pustit
i	i	k9	i
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
tužek	tužka	k1gFnPc2	tužka
<g/>
.	.	kIx.	.
</s>
<s>
Tužky	tužka	k1gFnPc1	tužka
byly	být	k5eAaImAgFnP	být
prodávány	prodávat	k5eAaImNgFnP	prodávat
na	na	k7c6	na
norimberském	norimberský	k2eAgInSc6d1	norimberský
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
norma	norma	k1gFnSc1	norma
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
tužek	tužka	k1gFnPc2	tužka
Kasparovým	Kasparův	k2eAgMnSc7d1	Kasparův
nástupcem	nástupce	k1gMnSc7	nástupce
Lotharem	Lothar	k1gMnSc7	Lothar
von	von	k1gInSc4	von
Faberem	Faber	k1gInSc7	Faber
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
směrodatná	směrodatný	k2eAgFnSc1d1	směrodatná
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
průmysl	průmysl	k1gInSc4	průmysl
výroby	výroba	k1gFnSc2	výroba
tužek	tužka	k1gFnPc2	tužka
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
stému	stý	k4xOgNnSc3	stý
výročí	výročí	k1gNnSc3	výročí
existence	existence	k1gFnSc2	existence
firmy	firma	k1gFnSc2	firma
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
pobočný	pobočný	k2eAgInSc1d1	pobočný
závod	závod	k1gInSc1	závod
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Geroldsgrün	Geroldsgrüna	k1gFnPc2	Geroldsgrüna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
původně	původně	k6eAd1	původně
sloužila	sloužit	k5eAaImAgFnS	sloužit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
břidlicových	břidlicový	k2eAgFnPc2d1	břidlicová
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Lothar	Lothar	k1gMnSc1	Lothar
Faber	Faber	k1gMnSc1	Faber
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
průkopníkem	průkopník	k1gMnSc7	průkopník
zákonů	zákon	k1gInPc2	zákon
o	o	k7c6	o
ochranné	ochranný	k2eAgFnSc6d1	ochranná
známce	známka	k1gFnSc6	známka
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1875	[number]	k4	1875
přinesl	přinést	k5eAaPmAgInS	přinést
do	do	k7c2	do
říšského	říšský	k2eAgInSc2d1	říšský
sněmu	sněm	k1gInSc2	sněm
petici	petice	k1gFnSc4	petice
"	"	kIx"	"
<g/>
K	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
ochranné	ochranný	k2eAgFnSc6d1	ochranná
známce	známka	k1gFnSc6	známka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
"	"	kIx"	"
<g/>
CASTELL	CASTELL	kA	CASTELL
<g/>
"	"	kIx"	"
od	od	k7c2	od
hraběte	hrabě	k1gMnSc2	hrabě
Alexandra	Alexandr	k1gMnSc2	Alexandr
Faber-Castell	Faber-Castell	k1gInSc4	Faber-Castell
<g/>
.	.	kIx.	.
</s>
<s>
Tužky	tužka	k1gFnPc1	tužka
si	se	k3xPyFc3	se
vysloužily	vysloužit	k5eAaPmAgFnP	vysloužit
pověst	pověst	k1gFnSc4	pověst
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
vysoce	vysoce	k6eAd1	vysoce
jakostní	jakostní	k2eAgInSc1d1	jakostní
<g/>
"	"	kIx"	"
výrobek	výrobek	k1gInSc1	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1931	[number]	k4	1931
-	-	kIx~	-
1932	[number]	k4	1932
byla	být	k5eAaImAgFnS	být
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
tužky	tužka	k1gFnPc4	tužka
patřící	patřící	k2eAgFnSc4d1	patřící
Johannu	Johanna	k1gFnSc4	Johanna
Faberovi	Faberův	k2eAgMnPc1d1	Faberův
převzata	převzat	k2eAgMnSc4d1	převzat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
kým	kdo	k3yRnSc7	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Do	do	k7c2	do
fabriky	fabrika	k1gFnSc2	fabrika
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgInP	dostat
nové	nový	k2eAgInPc1d1	nový
prostředky	prostředek	k1gInPc1	prostředek
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
spolupodílnictví	spolupodílnictví	k1gNnSc2	spolupodílnictví
a	a	k8xC	a
továrna	továrna	k1gFnSc1	továrna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
tužek	tužka	k1gFnPc2	tužka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
Lapis	lapis	k1gInSc1	lapis
Johann	Johann	k1gNnSc1	Johann
Faber	Faber	k1gInSc1	Faber
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
podnik	podnik	k1gInSc1	podnik
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
-	-	kIx~	-
podílelo	podílet	k5eAaImAgNnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
především	především	k9	především
zavedení	zavedení	k1gNnSc4	zavedení
tzv.	tzv.	kA	tzv.
TK-Stifts	TK-Stiftsa	k1gFnPc2	TK-Stiftsa
-	-	kIx~	-
tužek	tužka	k1gFnPc2	tužka
používaných	používaný	k2eAgInPc2d1	používaný
pro	pro	k7c4	pro
technické	technický	k2eAgNnSc4d1	technické
kreslení	kreslení	k1gNnSc4	kreslení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
výroba	výroba	k1gFnSc1	výroba
textilních	textilní	k2eAgInPc2d1	textilní
popisovačů	popisovač	k1gInPc2	popisovač
a	a	k8xC	a
mikrotužek	mikrotužka	k1gFnPc2	mikrotužka
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Faber-Castel	Faber-Castel	k1gInSc1	Faber-Castel
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
do	do	k7c2	do
podniku	podnik	k1gInSc2	podnik
ctící	ctící	k2eAgFnSc2d1	ctící
zásady	zásada	k1gFnSc2	zásada
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
-	-	kIx~	-
zavádí	zavádět	k5eAaImIp3nS	zavádět
se	se	k3xPyFc4	se
technologie	technologie	k1gFnSc1	technologie
pro	pro	k7c4	pro
šetření	šetření	k1gNnSc4	šetření
vodou	voda	k1gFnSc7	voda
u	u	k7c2	u
linek	linka	k1gFnPc2	linka
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
tužek	tužka	k1gFnPc2	tužka
i	i	k8xC	i
pastelek	pastelka	k1gFnPc2	pastelka
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
spolkový	spolkový	k2eAgMnSc1d1	spolkový
ministr	ministr	k1gMnSc1	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
Klaus	Klaus	k1gMnSc1	Klaus
Töpfer	Töpfer	k1gMnSc1	Töpfer
požehnává	požehnávat	k5eAaImIp3nS	požehnávat
celosvětově	celosvětově	k6eAd1	celosvětově
prvnímu	první	k4xOgNnSc3	první
přístroji	přístroj	k1gInPc7	přístroj
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
tužek	tužka	k1gFnPc2	tužka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
šetrný	šetrný	k2eAgInSc1d1	šetrný
k	k	k7c3	k
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Faber-Castell	Faber-Castella	k1gFnPc2	Faber-Castella
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Faber-Castell	Faber-Castella	k1gFnPc2	Faber-Castella
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
internetová	internetový	k2eAgFnSc1d1	internetová
stránka	stránka	k1gFnSc1	stránka
Faber-Castell	Faber-Castella	k1gFnPc2	Faber-Castella
</s>
</p>
