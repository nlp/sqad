<s>
Zemský	zemský	k2eAgInSc1d1	zemský
plášť	plášť	k1gInSc1	plášť
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vrstev	vrstva	k1gFnPc2	vrstva
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
shora	shora	k6eAd1	shora
vymezená	vymezený	k2eAgFnSc1d1	vymezená
zemskou	zemský	k2eAgFnSc7d1	zemská
kůrou	kůra	k1gFnSc7	kůra
a	a	k8xC	a
zespodu	zespodu	k6eAd1	zespodu
zemským	zemský	k2eAgNnSc7d1	zemské
jádrem	jádro	k1gNnSc7	jádro
<g/>
,	,	kIx,	,
odděleným	oddělený	k2eAgInSc7d1	oddělený
Gutenbergovou	Gutenbergův	k2eAgFnSc7d1	Gutenbergova
diskontinuitou	diskontinuita	k1gFnSc7	diskontinuita
<g/>
.	.	kIx.	.
</s>
