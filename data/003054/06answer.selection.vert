<s>
Sběrnice	sběrnice	k1gFnSc1	sběrnice
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
bus	bus	k1gInSc1	bus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
signálových	signálový	k2eAgInPc2d1	signálový
vodičů	vodič	k1gInPc2	vodič
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
skupiny	skupina	k1gFnPc4	skupina
řídicích	řídicí	k2eAgInPc2d1	řídicí
<g/>
,	,	kIx,	,
adresových	adresový	k2eAgInPc2d1	adresový
a	a	k8xC	a
datových	datový	k2eAgInPc2d1	datový
vodičů	vodič	k1gInPc2	vodič
v	v	k7c6	v
případě	případ	k1gInSc6	případ
paralelní	paralelní	k2eAgFnSc2d1	paralelní
sběrnice	sběrnice	k1gFnSc2	sběrnice
nebo	nebo	k8xC	nebo
sdílení	sdílení	k1gNnSc4	sdílení
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
na	na	k7c6	na
společném	společný	k2eAgInSc6d1	společný
vodiči	vodič	k1gInSc6	vodič
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
vodičích	vodič	k1gInPc6	vodič
<g/>
)	)	kIx)	)
u	u	k7c2	u
sériových	sériový	k2eAgFnPc2d1	sériová
sběrnic	sběrnice	k1gFnPc2	sběrnice
<g/>
.	.	kIx.	.
</s>
