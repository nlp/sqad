<s>
Sběrnice	sběrnice	k1gFnSc1	sběrnice
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
bus	bus	k1gInSc1	bus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
signálových	signálový	k2eAgInPc2d1	signálový
vodičů	vodič	k1gInPc2	vodič
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
skupiny	skupina	k1gFnPc4	skupina
řídicích	řídicí	k2eAgInPc2d1	řídicí
<g/>
,	,	kIx,	,
adresových	adresový	k2eAgInPc2d1	adresový
a	a	k8xC	a
datových	datový	k2eAgInPc2d1	datový
vodičů	vodič	k1gInPc2	vodič
v	v	k7c6	v
případě	případ	k1gInSc6	případ
paralelní	paralelní	k2eAgFnSc2d1	paralelní
sběrnice	sběrnice	k1gFnSc2	sběrnice
nebo	nebo	k8xC	nebo
sdílení	sdílení	k1gNnSc4	sdílení
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
na	na	k7c6	na
společném	společný	k2eAgInSc6d1	společný
vodiči	vodič	k1gInSc6	vodič
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
vodičích	vodič	k1gInPc6	vodič
<g/>
)	)	kIx)	)
u	u	k7c2	u
sériových	sériový	k2eAgFnPc2d1	sériová
sběrnic	sběrnice	k1gFnPc2	sběrnice
<g/>
.	.	kIx.	.
</s>
<s>
Sběrnice	sběrnice	k1gFnSc1	sběrnice
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
účel	účel	k1gInSc4	účel
zajistit	zajistit	k5eAaPmF	zajistit
přenos	přenos	k1gInSc4	přenos
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
řídicích	řídicí	k2eAgInPc2d1	řídicí
povelů	povel	k1gInPc2	povel
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
a	a	k8xC	a
více	hodně	k6eAd2	hodně
elektronickými	elektronický	k2eAgNnPc7d1	elektronické
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
dat	datum	k1gNnPc2	datum
na	na	k7c6	na
sběrnici	sběrnice	k1gFnSc6	sběrnice
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
stanoveným	stanovený	k2eAgInSc7d1	stanovený
protokolem	protokol	k1gInSc7	protokol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
modulární	modulární	k2eAgFnSc2d1	modulární
architektury	architektura	k1gFnSc2	architektura
elektronického	elektronický	k2eAgNnSc2d1	elektronické
zařízení	zařízení	k1gNnSc2	zařízení
nebo	nebo	k8xC	nebo
počítače	počítač	k1gInSc2	počítač
je	být	k5eAaImIp3nS	být
sběrnice	sběrnice	k1gFnSc1	sběrnice
po	po	k7c6	po
mechanické	mechanický	k2eAgFnSc6d1	mechanická
stránce	stránka	k1gFnSc6	stránka
vybavena	vybavit	k5eAaPmNgNnP	vybavit
konektory	konektor	k1gInPc7	konektor
uzpůsobenými	uzpůsobený	k2eAgFnPc7d1	uzpůsobená
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
modulů	modul	k1gInPc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
ISA	ISA	kA	ISA
-	-	kIx~	-
starší	starý	k2eAgInSc1d2	starší
typ	typ	k1gInSc1	typ
pasivní	pasivní	k2eAgFnSc2d1	pasivní
sběrnice	sběrnice	k1gFnSc2	sběrnice
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
8	[number]	k4	8
nebo	nebo	k8xC	nebo
16	[number]	k4	16
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
<	<	kIx(	<
8	[number]	k4	8
MB	MB	kA	MB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
PCI	PCI	kA	PCI
-	-	kIx~	-
novější	nový	k2eAgInSc4d2	novější
typ	typ	k1gInSc4	typ
"	"	kIx"	"
<g/>
inteligentní	inteligentní	k2eAgFnSc2d1	inteligentní
<g/>
"	"	kIx"	"
sběrnice	sběrnice	k1gFnSc2	sběrnice
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
32	[number]	k4	32
nebo	nebo	k8xC	nebo
64	[number]	k4	64
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
burst	burst	k1gFnSc1	burst
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
<	<	kIx(	<
<g />
.	.	kIx.	.
</s>
<s>
130	[number]	k4	130
MB	MB	kA	MB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
260	[number]	k4	260
MB	MB	kA	MB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
AGP	AGP	kA	AGP
-	-	kIx~	-
jednoúčelová	jednoúčelový	k2eAgFnSc1d1	jednoúčelová
sběrnice	sběrnice	k1gFnSc1	sběrnice
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
připojeni	připojen	k2eAgMnPc1d1	připojen
grafického	grafický	k2eAgNnSc2d1	grafické
rozhraní	rozhraní	k1gNnSc2	rozhraní
(	(	kIx(	(
<g/>
karty	karta	k1gFnSc2	karta
<g/>
)	)	kIx)	)
k	k	k7c3	k
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
260	[number]	k4	260
MB	MB	kA	MB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
-	-	kIx~	-
2	[number]	k4	2
GB	GB	kA	GB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
PCI-X	PCI-X	k1gFnSc7	PCI-X
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
zpětně	zpětně	k6eAd1	zpětně
kompatibilní	kompatibilní	k2eAgNnSc1d1	kompatibilní
rozšíření	rozšíření	k1gNnSc1	rozšíření
sběrnice	sběrnice	k1gFnSc2	sběrnice
PCI	PCI	kA	PCI
PCI-Express	PCI-Express	k1gInSc1	PCI-Express
(	(	kIx(	(
<g/>
PCIe	PCIe	k1gFnSc1	PCIe
<g/>
)	)	kIx)	)
-	-	kIx~	-
nová	nový	k2eAgFnSc1d1	nová
sériová	sériový	k2eAgFnSc1d1	sériová
implementace	implementace	k1gFnSc1	implementace
sběrnice	sběrnice	k1gFnSc2	sběrnice
PCI	PCI	kA	PCI
USB	USB	kA	USB
-	-	kIx~	-
sériová	sériový	k2eAgFnSc1d1	sériová
polyfunkční	polyfunkční	k2eAgFnSc1d1	polyfunkční
sběrnice	sběrnice	k1gFnSc1	sběrnice
<g/>
,	,	kIx,	,
2	[number]	k4	2
diferenciální	diferenciální	k2eAgInPc4d1	diferenciální
datové	datový	k2eAgInPc4d1	datový
vodiče	vodič	k1gInPc4	vodič
+	+	kIx~	+
2	[number]	k4	2
napájecí	napájecí	k2eAgInSc1d1	napájecí
vodiče	vodič	k1gInSc2	vodič
5	[number]	k4	5
V	V	kA	V
<g/>
/	/	kIx~	/
<g/>
500	[number]	k4	500
mA	mA	k?	mA
<g/>
,	,	kIx,	,
široké	široký	k2eAgNnSc1d1	široké
použití	použití	k1gNnSc1	použití
<g/>
,	,	kIx,	,
verze	verze	k1gFnSc1	verze
1.1	[number]	k4	1.1
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
<g />
.	.	kIx.	.
</s>
<s>
12	[number]	k4	12
Mb	Mb	k1gFnSc1	Mb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
1,43	[number]	k4	1,43
MB	MB	kA	MB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2.0	[number]	k4	2.0
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
480	[number]	k4	480
Mb	Mb	k1gFnPc2	Mb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
57	[number]	k4	57
MB	MB	kA	MB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3.0	[number]	k4	3.0
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
4800	[number]	k4	4800
Mb	Mb	k1gFnPc2	Mb
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
572	[number]	k4	572
MB	MB	kA	MB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
FireWire	FireWir	k1gInSc5	FireWir
-	-	kIx~	-
sériová	sériový	k2eAgFnSc1d1	sériová
polyfunkční	polyfunkční	k2eAgFnSc1d1	polyfunkční
sběrnice	sběrnice	k1gFnSc1	sběrnice
<g/>
,	,	kIx,	,
široké	široký	k2eAgNnSc1d1	široké
použití	použití	k1gNnSc1	použití
<g/>
,	,	kIx,	,
50	[number]	k4	50
MB	MB	kA	MB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
RS-485	RS-485	k1gFnSc7	RS-485
-	-	kIx~	-
sériová	sériový	k2eAgFnSc1d1	sériová
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
sběrnice	sběrnice	k1gFnSc1	sběrnice
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
jako	jako	k8xS	jako
proudová	proudový	k2eAgFnSc1d1	proudová
smyčka	smyčka	k1gFnSc1	smyčka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
prostor	prostora	k1gFnPc2	prostora
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
elektromagnetickým	elektromagnetický	k2eAgNnSc7d1	elektromagnetické
rušením	rušení	k1gNnSc7	rušení
I2	I2	k1gFnSc2	I2
-	-	kIx~	-
sériová	sériový	k2eAgFnSc1d1	sériová
sběrnice	sběrnice	k1gFnSc1	sběrnice
<g/>
,	,	kIx,	,
<	<	kIx(	<
100	[number]	k4	100
kb	kb	kA	kb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
adresace	adresace	k1gFnSc1	adresace
128	[number]	k4	128
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
komunikace	komunikace	k1gFnSc2	komunikace
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
v	v	k7c6	v
elektronických	elektronický	k2eAgInPc6d1	elektronický
zařízení	zařízení	k1gNnPc1	zařízení
</s>
