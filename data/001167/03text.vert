<s>
Bylo	být	k5eAaImAgNnS	být
nás	my	k3xPp1nPc4	my
pět	pět	k4xCc1	pět
je	být	k5eAaImIp3nS	být
humoristický	humoristický	k2eAgInSc1d1	humoristický
román	román	k1gInSc1	román
Karla	Karel	k1gMnSc2	Karel
Poláčka	Poláček	k1gMnSc2	Poláček
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
autor	autor	k1gMnSc1	autor
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
dětství	dětství	k1gNnSc4	dětství
v	v	k7c6	v
Rychnově	Rychnov	k1gInSc6	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěčem	vypravěč	k1gMnSc7	vypravěč
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
školák	školák	k1gMnSc1	školák
Petr	Petr	k1gMnSc1	Petr
Bajza	Bajza	k1gMnSc1	Bajza
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
volným	volný	k2eAgInSc7d1	volný
sledem	sled	k1gInSc7	sled
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
příhod	příhoda	k1gFnPc2	příhoda
a	a	k8xC	a
dobrodružství	dobrodružství	k1gNnPc2	dobrodružství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
potkávají	potkávat	k5eAaImIp3nP	potkávat
vypravěče	vypravěč	k1gMnPc4	vypravěč
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
čtyři	čtyři	k4xCgMnPc4	čtyři
kamarády	kamarád	k1gMnPc4	kamarád
<g/>
:	:	kIx,	:
Čeňka	Čeněk	k1gMnSc4	Čeněk
Jirsáka	Jirsák	k1gMnSc4	Jirsák
<g/>
,	,	kIx,	,
Antonína	Antonín	k1gMnSc4	Antonín
Bejvala	Bejvala	k1gMnSc4	Bejvala
<g/>
,	,	kIx,	,
Édu	Édu	k1gFnSc1	Édu
Kemlinka	Kemlinka	k1gFnSc1	Kemlinka
a	a	k8xC	a
Josefa	Josefa	k1gFnSc1	Josefa
Zilvara	Zilvara	k1gFnSc1	Zilvara
z	z	k7c2	z
chudobince	chudobinec	k1gInSc2	chudobinec
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc4	román
napsal	napsat	k5eAaPmAgMnS	napsat
Poláček	Poláček	k1gMnSc1	Poláček
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
odchodem	odchod	k1gInSc7	odchod
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vyšel	vyjít	k5eAaPmAgInS	vyjít
rok	rok	k1gInSc1	rok
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
bylo	být	k5eAaImAgNnS	být
dílo	dílo	k1gNnSc1	dílo
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
jako	jako	k8xC	jako
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
šestidílný	šestidílný	k2eAgInSc1d1	šestidílný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
používá	používat	k5eAaImIp3nS	používat
hovorový	hovorový	k2eAgInSc4d1	hovorový
i	i	k8xC	i
spisovný	spisovný	k2eAgInSc4d1	spisovný
jazyk	jazyk	k1gInSc4	jazyk
(	(	kIx(	(
<g/>
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
kontrast	kontrast	k1gInSc1	kontrast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
přímá	přímý	k2eAgFnSc1d1	přímá
řeč	řeč	k1gFnSc1	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc1	zobrazení
světa	svět	k1gInSc2	svět
dospělých	dospělí	k1gMnPc2	dospělí
na	na	k7c6	na
malém	malý	k2eAgNnSc6d1	malé
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
viděno	vidět	k5eAaImNgNnS	vidět
dětskýma	dětský	k2eAgNnPc7d1	dětské
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
proto	proto	k8xC	proto
působí	působit	k5eAaImIp3nS	působit
komicky	komicky	k6eAd1	komicky
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Péťova	Péťův	k2eAgFnSc1d1	Péťova
snaha	snaha	k1gFnSc1	snaha
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
korektně	korektně	k6eAd1	korektně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěčem	vypravěč	k1gMnSc7	vypravěč
je	být	k5eAaImIp3nS	být
Petr	Petr	k1gMnSc1	Petr
Bajza	Bajza	k1gMnSc1	Bajza
<g/>
,	,	kIx,	,
veselý	veselý	k2eAgMnSc1d1	veselý
<g/>
,	,	kIx,	,
rošťák	rošťák	k1gMnSc1	rošťák
<g/>
,	,	kIx,	,
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
představivostí	představivost	k1gFnSc7	představivost
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
majitele	majitel	k1gMnSc2	majitel
obchodu	obchod	k1gInSc2	obchod
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
době	doba	k1gFnSc3	doba
vzniku	vznik	k1gInSc2	vznik
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
skutečné	skutečný	k2eAgFnSc2d1	skutečná
Poláčkovy	Poláčkův	k2eAgFnSc2d1	Poláčkova
rodiny	rodina	k1gFnSc2	rodina
židovská	židovská	k1gFnSc1	židovská
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
příběhu	příběh	k1gInSc6	příběh
pochopitelně	pochopitelně	k6eAd1	pochopitelně
žádní	žádný	k3yNgMnPc1	žádný
Židé	Žid	k1gMnPc1	Žid
nevystupují	vystupovat	k5eNaImIp3nP	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Bejval	Bejval	k1gMnSc1	Bejval
-	-	kIx~	-
Petrův	Petrův	k2eAgMnSc1d1	Petrův
spolužák	spolužák	k1gMnSc1	spolužák
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
různé	různý	k2eAgInPc4d1	různý
bláznivé	bláznivý	k2eAgInPc4d1	bláznivý
nápady	nápad	k1gInPc4	nápad
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
si	se	k3xPyFc3	se
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
lakomý	lakomý	k2eAgInSc1d1	lakomý
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
majitele	majitel	k1gMnSc2	majitel
povoznictví	povoznictví	k1gNnSc2	povoznictví
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
uhlím	uhlí	k1gNnSc7	uhlí
Čeněk	Čeněk	k1gMnSc1	Čeněk
Jirsák	Jirsák	k1gMnSc1	Jirsák
-	-	kIx~	-
kamarád	kamarád	k1gMnSc1	kamarád
Petra	Petr	k1gMnSc2	Petr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pobožný	pobožný	k2eAgMnSc1d1	pobožný
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
koníčkem	koníček	k1gMnSc7	koníček
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
sbírání	sbírání	k1gNnSc1	sbírání
hříchů	hřích	k1gInPc2	hřích
ke	k	k7c3	k
svaté	svatý	k2eAgFnSc3d1	svatá
zpovědi	zpověď	k1gFnSc3	zpověď
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
ješitný	ješitný	k2eAgMnSc1d1	ješitný
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Krakonoš	Krakonoš	k1gMnSc1	Krakonoš
Éda	Éda	k1gFnSc2	Éda
Kemlink	Kemlink	k1gInSc1	Kemlink
-	-	kIx~	-
další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
kamarádů	kamarád	k1gMnPc2	kamarád
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
úředníka	úředník	k1gMnSc2	úředník
z	z	k7c2	z
berního	berní	k2eAgInSc2d1	berní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
chytrý	chytrý	k2eAgInSc1d1	chytrý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
Refaelu	Refaela	k1gFnSc4	Refaela
a	a	k8xC	a
Viktorku	Viktorka	k1gFnSc4	Viktorka
a	a	k8xC	a
psa	pes	k1gMnSc4	pes
Pajdu	Pajdu	k?	Pajdu
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
psem	pes	k1gMnSc7	pes
<g />
.	.	kIx.	.
</s>
<s>
Amorem	Amor	k1gMnSc7	Amor
krade	krást	k5eAaImIp3nS	krást
v	v	k7c6	v
řeznictví	řeznictví	k1gNnSc6	řeznictví
buřty	buřty	k?	buřty
Josef	Josef	k1gMnSc1	Josef
Zilvar	Zilvar	k1gInSc1	Zilvar
-	-	kIx~	-
z	z	k7c2	z
chudobince	chudobinec	k1gInSc2	chudobinec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgMnPc1d2	starší
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
hoši	hoch	k1gMnPc1	hoch
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
propadl	propadnout	k5eAaPmAgInS	propadnout
<g/>
,	,	kIx,	,
kouří	kouřit	k5eAaImIp3nS	kouřit
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
dospělé	dospělí	k1gMnPc4	dospělí
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
statečný	statečný	k2eAgMnSc1d1	statečný
<g/>
,	,	kIx,	,
semtam	semtam	k6eAd1	semtam
se	se	k3xPyFc4	se
spustí	spustit	k5eAaPmIp3nS	spustit
s	s	k7c7	s
Habrováky	Habrovák	k1gInPc7	Habrovák
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
válečného	válečný	k2eAgMnSc2d1	válečný
invalidy	invalida	k1gMnSc2	invalida
a	a	k8xC	a
"	"	kIx"	"
<g/>
vrchního	vrchní	k2eAgMnSc2d1	vrchní
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
žebráka	žebrák	k1gMnSc2	žebrák
tatínek	tatínek	k1gMnSc1	tatínek
Bajza	Bajza	k1gMnSc1	Bajza
-	-	kIx~	-
majitel	majitel	k1gMnSc1	majitel
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
přísný	přísný	k2eAgMnSc1d1	přísný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
laskavý	laskavý	k2eAgMnSc1d1	laskavý
<g/>
,	,	kIx,	,
semtam	semtam	k6eAd1	semtam
poněkud	poněkud	k6eAd1	poněkud
opatrný	opatrný	k2eAgMnSc1d1	opatrný
na	na	k7c4	na
peníze	peníz	k1gInPc4	peníz
maminka	maminka	k1gFnSc1	maminka
Bajzová	Bajzová	k1gFnSc1	Bajzová
-	-	kIx~	-
hodná	hodný	k2eAgFnSc1d1	hodná
<g/>
,	,	kIx,	,
rozumná	rozumný	k2eAgFnSc1d1	rozumná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
tatínkovu	tatínkův	k2eAgFnSc4d1	tatínkova
přísnost	přísnost	k1gFnSc4	přísnost
Kristýna	Kristýna	k1gFnSc1	Kristýna
-	-	kIx~	-
služka	služka	k1gFnSc1	služka
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
horské	horský	k2eAgFnSc2d1	horská
vesnice	vesnice	k1gFnSc2	vesnice
Rampuše	Rampuše	k1gFnSc2	Rampuše
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
odtud	odtud	k6eAd1	odtud
Rampepurda	Rampepurda	k1gFnSc1	Rampepurda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
holka	holka	k1gFnSc1	holka
<g/>
,	,	kIx,	,
všemu	všecek	k3xTgNnSc3	všecek
se	se	k3xPyFc4	se
směje	smát	k5eAaImIp3nS	smát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
veliký	veliký	k2eAgInSc4d1	veliký
strach	strach	k1gInSc4	strach
z	z	k7c2	z
myší	myš	k1gFnPc2	myš
Eva	Eva	k1gFnSc1	Eva
Svobodová	Svobodová	k1gFnSc1	Svobodová
-	-	kIx~	-
dcera	dcera	k1gFnSc1	dcera
cukráře	cukrář	k1gMnSc2	cukrář
<g/>
,	,	kIx,	,
Petrova	Petrův	k2eAgFnSc1d1	Petrova
favoritka	favoritka	k1gFnSc1	favoritka
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nS	nosit
Petrovi	Petr	k1gMnSc3	Petr
z	z	k7c2	z
cukrářství	cukrářství	k1gNnSc2	cukrářství
dobroty	dobrota	k1gFnSc2	dobrota
pan	pan	k1gMnSc1	pan
Fajst	Fajst	k1gMnSc1	Fajst
-	-	kIx~	-
místní	místní	k2eAgMnSc1d1	místní
občan	občan	k1gMnSc1	občan
<g/>
,	,	kIx,	,
starý	starý	k2eAgMnSc1d1	starý
mládenec	mládenec	k1gMnSc1	mládenec
<g/>
,	,	kIx,	,
zapšklý	zapšklý	k2eAgMnSc1d1	zapšklý
moralista	moralista	k1gMnSc1	moralista
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
milovník	milovník	k1gMnSc1	milovník
starých	starý	k2eAgInPc2d1	starý
mravů	mrav	k1gInPc2	mrav
Vařekovi	Vařekův	k2eAgMnPc1d1	Vařekův
-	-	kIx~	-
příbuzní	příbuzný	k1gMnPc1	příbuzný
Bajzů	Bajza	k1gMnPc2	Bajza
<g/>
,	,	kIx,	,
lakomci	lakomec	k1gMnPc5	lakomec
<g/>
,	,	kIx,	,
závistivci	závistivec	k1gMnPc5	závistivec
<g/>
,	,	kIx,	,
nikomu	nikdo	k3yNnSc3	nikdo
nic	nic	k3yNnSc1	nic
nepřejí	přejíst	k5eNaPmIp3nS	přejíst
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgInSc1d1	vlastní
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
galanterií	galanterie	k1gFnSc7	galanterie
pan	pan	k1gMnSc1	pan
Zilvar	Zilvar	k1gInSc1	Zilvar
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
Josefa	Josef	k1gMnSc2	Josef
Zilvara	Zilvar	k1gMnSc2	Zilvar
<g/>
,	,	kIx,	,
bojoval	bojovat	k5eAaImAgMnS	bojovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
chudobinci	chudobinec	k1gInSc6	chudobinec
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnSc1d1	vrchní
městský	městský	k2eAgMnSc1d1	městský
žebrák	žebrák	k1gMnSc1	žebrák
<g />
.	.	kIx.	.
</s>
<s>
Otakárek	Otakárek	k1gMnSc1	Otakárek
Soumarů	soumar	k1gMnPc2	soumar
-	-	kIx~	-
syn	syn	k1gMnSc1	syn
továrníka	továrník	k1gMnSc2	továrník
<g/>
,	,	kIx,	,
osamělý	osamělý	k2eAgMnSc1d1	osamělý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zálibu	záliba	k1gFnSc4	záliba
ve	v	k7c6	v
čtení	čtení	k1gNnSc6	čtení
mayovek	mayovka	k1gFnPc2	mayovka
<g/>
,	,	kIx,	,
verneovek	verneovka	k1gFnPc2	verneovka
a	a	k8xC	a
podobné	podobný	k2eAgFnSc2d1	podobná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
obézní	obézní	k2eAgMnSc1d1	obézní
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
vychovatelku	vychovatelka	k1gFnSc4	vychovatelka
pan	pan	k1gMnSc1	pan
Potůček	Potůček	k1gMnSc1	Potůček
-	-	kIx~	-
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
fotografoval	fotografovat	k5eAaImAgInS	fotografovat
celou	celý	k2eAgFnSc4d1	celá
třídu	třída	k1gFnSc4	třída
na	na	k7c6	na
školním	školní	k2eAgInSc6d1	školní
dvoře	dvůr	k1gInSc6	dvůr
Alfons	Alfons	k1gMnSc1	Alfons
Kasalický	kasalický	k2eAgMnSc1d1	kasalický
-	-	kIx~	-
artista	artista	k1gMnSc1	artista
z	z	k7c2	z
cirkusu	cirkus	k1gInSc2	cirkus
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ve	v	k7c6	v
školních	školní	k2eAgInPc6d1	školní
předmětech	předmět	k1gInPc6	předmět
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
tělocviku	tělocvik	k1gInSc2	tělocvik
<g/>
)	)	kIx)	)
příliš	příliš	k6eAd1	příliš
neprospívá	prospívat	k5eNaImIp3nS	prospívat
Karel	Karel	k1gMnSc1	Karel
Páta	Páta	k1gMnSc1	Páta
-	-	kIx~	-
šprt	šprt	k1gMnSc1	šprt
<g/>
,	,	kIx,	,
nepopulární	populární	k2eNgMnSc1d1	nepopulární
Jakub	Jakub	k1gMnSc1	Jakub
-	-	kIx~	-
pracuje	pracovat	k5eAaImIp3nS	pracovat
u	u	k7c2	u
Bejvalů	Bejval	k1gMnPc2	Bejval
v	v	k7c6	v
povoznictví	povoznictví	k1gNnSc6	povoznictví
<g/>
,	,	kIx,	,
silný	silný	k2eAgMnSc1d1	silný
chlap	chlap	k1gMnSc1	chlap
<g/>
,	,	kIx,	,
moc	moc	k1gFnSc1	moc
řečí	řeč	k1gFnPc2	řeč
nenadělá	nadělat	k5eNaBmIp3nS	nadělat
<g/>
,	,	kIx,	,
chlapci	chlapec	k1gMnPc1	chlapec
ho	on	k3xPp3gMnSc4	on
obdivují	obdivovat	k5eAaImIp3nP	obdivovat
pan	pan	k1gMnSc1	pan
Svoboda	Svoboda	k1gMnSc1	Svoboda
-	-	kIx~	-
majitel	majitel	k1gMnSc1	majitel
cukrářství	cukrářství	k1gNnSc2	cukrářství
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Evy	Eva	k1gFnSc2	Eva
<g />
.	.	kIx.	.
</s>
<s>
Svobodové	Svobodové	k2eAgMnSc1d1	Svobodové
pan	pan	k1gMnSc1	pan
Jirsák	Jirsák	k1gMnSc1	Jirsák
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
Čeňka	Čeněk	k1gMnSc2	Čeněk
Jirsáka	Jirsák	k1gMnSc2	Jirsák
<g/>
,	,	kIx,	,
živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
výrobou	výroba	k1gFnSc7	výroba
čepic	čepice	k1gFnPc2	čepice
a	a	k8xC	a
papučí	papuč	k1gFnPc2	papuč
<g/>
,	,	kIx,	,
chlapci	chlapec	k1gMnPc1	chlapec
mu	on	k3xPp3gMnSc3	on
přezdívají	přezdívat	k5eAaImIp3nP	přezdívat
Krakonoš	Krakonoš	k1gMnSc1	Krakonoš
a	a	k8xC	a
paní	paní	k1gFnSc1	paní
Jirsákové	Jirsáková	k1gFnSc2	Jirsáková
Krakonoška	Krakonoška	k1gFnSc1	Krakonoška
Láďa	Láďa	k1gMnSc1	Láďa
Bajza	Bajza	k1gMnSc1	Bajza
-	-	kIx~	-
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Petra	Petr	k1gMnSc2	Petr
Bajzy	Bajza	k1gMnSc2	Bajza
<g/>
,	,	kIx,	,
učí	učit	k5eAaImIp3nS	učit
se	se	k3xPyFc4	se
na	na	k7c4	na
kupce	kupec	k1gMnSc4	kupec
v	v	k7c6	v
Mostě	most	k1gInSc6	most
Mančinka	Mančinka	k1gFnSc1	Mančinka
-	-	kIx~	-
mladší	mladý	k2eAgFnSc1d2	mladší
sestřička	sestřička	k1gFnSc1	sestřička
(	(	kIx(	(
<g/>
batole	batole	k1gNnSc1	batole
<g/>
)	)	kIx)	)
Petra	Petr	k1gMnSc2	Petr
Bajzy	Bajza	k1gMnSc2	Bajza
pan	pan	k1gMnSc1	pan
Kemlink	Kemlink	k1gInSc1	Kemlink
-	-	kIx~	-
otec	otec	k1gMnSc1	otec
Édy	Édy	k1gMnSc1	Édy
Kemlinka	Kemlinka	k1gFnSc1	Kemlinka
<g/>
,	,	kIx,	,
úředník	úředník	k1gMnSc1	úředník
z	z	k7c2	z
berního	berní	k2eAgInSc2d1	berní
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
mají	mít	k5eAaImIp3nP	mít
respekt	respekt	k1gInSc4	respekt
a	a	k8xC	a
zdraví	zdravit	k5eAaImIp3nS	zdravit
ho	on	k3xPp3gNnSc4	on
Petr	Petr	k1gMnSc1	Petr
<g/>
.....	.....	k?	.....
má	mít	k5eAaImIp3nS	mít
malou	malý	k2eAgFnSc4d1	malá
sestru	sestra	k1gFnSc4	sestra
Mančinku	Mančinka	k1gFnSc4	Mančinka
a	a	k8xC	a
staršího	starší	k1gMnSc4	starší
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
už	už	k6eAd1	už
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
nebydlí	bydlet	k5eNaImIp3nS	bydlet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
Sudetech	Sudety	k1gInPc6	Sudety
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
se	se	k3xPyFc4	se
často	často	k6eAd1	často
z	z	k7c2	z
legrace	legrace	k1gFnSc2	legrace
pere	prát	k5eAaImIp3nS	prát
se	s	k7c7	s
služkou	služka	k1gFnSc7	služka
Kristýnou	Kristýna	k1gFnSc7	Kristýna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
říká	říkat	k5eAaImIp3nS	říkat
Rampepurda	Rampepurda	k1gFnSc1	Rampepurda
<g/>
.	.	kIx.	.
</s>
<s>
Dělá	dělat	k5eAaImIp3nS	dělat
jí	on	k3xPp3gFnSc3	on
naschvály	naschvál	k1gInPc1	naschvál
-	-	kIx~	-
například	například	k6eAd1	například
dá	dát	k5eAaPmIp3nS	dát
Kristýně	Kristýna	k1gFnSc3	Kristýna
do	do	k7c2	do
kufru	kufr	k1gInSc2	kufr
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
myš	myš	k1gFnSc4	myš
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
lekla	leknout	k5eAaPmAgFnS	leknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potom	potom	k6eAd1	potom
dostane	dostat	k5eAaPmIp3nS	dostat
vynadáno	vynadán	k2eAgNnSc1d1	vynadáno
a	a	k8xC	a
brečí	brečet	k5eAaImIp3nS	brečet
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
čtyři	čtyři	k4xCgMnPc1	čtyři
kamarádi	kamarád	k1gMnPc1	kamarád
prožívají	prožívat	k5eAaImIp3nP	prožívat
různá	různý	k2eAgNnPc1d1	různé
klukovská	klukovský	k2eAgNnPc1d1	klukovské
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
biograf	biograf	k1gInSc1	biograf
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
kluci	kluk	k1gMnPc1	kluk
rádi	rád	k2eAgMnPc1d1	rád
chodí	chodit	k5eAaImIp3nP	chodit
<g/>
.	.	kIx.	.
</s>
<s>
Lezou	lézt	k5eAaImIp3nP	lézt
tam	tam	k6eAd1	tam
tajně	tajně	k6eAd1	tajně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nemuseli	muset	k5eNaImAgMnP	muset
platit	platit	k5eAaImF	platit
<g/>
.	.	kIx.	.
</s>
<s>
Chlapci	chlapec	k1gMnPc1	chlapec
si	se	k3xPyFc3	se
také	také	k9	také
chtějí	chtít	k5eAaImIp3nP	chtít
ochočit	ochočit	k5eAaPmF	ochočit
vosy	vosa	k1gFnPc4	vosa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepodaří	podařit	k5eNaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Čtou	číst	k5eAaImIp3nP	číst
dobrodružné	dobrodružný	k2eAgFnPc4d1	dobrodružná
knížky	knížka	k1gFnPc4	knížka
a	a	k8xC	a
chtěli	chtít	k5eAaImAgMnP	chtít
by	by	kYmCp3nP	by
cestovat	cestovat	k5eAaImF	cestovat
<g/>
,	,	kIx,	,
plánují	plánovat	k5eAaImIp3nP	plánovat
si	se	k3xPyFc3	se
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ovšem	ovšem	k9	ovšem
neuskuteční	uskutečnit	k5eNaPmIp3nS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Chodí	chodit	k5eAaImIp3nP	chodit
se	se	k3xPyFc4	se
koupat	koupat	k5eAaImF	koupat
k	k	k7c3	k
rybníku	rybník	k1gInSc3	rybník
<g/>
,	,	kIx,	,
Zilvar	Zilvar	k1gInSc4	Zilvar
zachrání	zachránit	k5eAaPmIp3nP	zachránit
tonoucího	tonoucí	k2eAgMnSc4d1	tonoucí
chlapce	chlapec	k1gMnSc4	chlapec
Vénu	Véna	k1gMnSc4	Véna
<g/>
.	.	kIx.	.
</s>
<s>
Hoši	hoch	k1gMnPc1	hoch
se	se	k3xPyFc4	se
perou	prát	k5eAaImIp3nP	prát
s	s	k7c7	s
nepřátelskými	přátelský	k2eNgInPc7d1	nepřátelský
Ješiňáky	Ješiňák	k1gInPc7	Ješiňák
a	a	k8xC	a
Habrováky	Habrovák	k1gInPc7	Habrovák
<g/>
.	.	kIx.	.
</s>
<s>
Dělají	dělat	k5eAaImIp3nP	dělat
si	se	k3xPyFc3	se
legraci	legrace	k1gFnSc4	legrace
z	z	k7c2	z
pana	pan	k1gMnSc2	pan
Fajsta	Fajst	k1gMnSc2	Fajst
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
věčně	věčně	k6eAd1	věčně
pomlouvá	pomlouvat	k5eAaImIp3nS	pomlouvat
mládež	mládež	k1gFnSc1	mládež
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
nezdraví	zdravit	k5eNaImIp3nS	zdravit
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
byl	být	k5eAaImAgMnS	být
také	také	k9	také
několikrát	několikrát	k6eAd1	několikrát
navštívit	navštívit	k5eAaPmF	navštívit
Otakárka	Otakárek	k1gMnSc4	Otakárek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
chce	chtít	k5eAaImIp3nS	chtít
kamarády	kamarád	k1gMnPc4	kamarád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
bohatý	bohatý	k2eAgMnSc1d1	bohatý
a	a	k8xC	a
nesmí	smět	k5eNaImIp3nS	smět
se	se	k3xPyFc4	se
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
dětmi	dítě	k1gFnPc7	dítě
kamarádit	kamarádit	k5eAaImF	kamarádit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepochytil	pochytit	k5eNaPmAgInS	pochytit
špatné	špatný	k2eAgInPc4d1	špatný
způsoby	způsob	k1gInPc4	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
slíbil	slíbit	k5eAaPmAgMnS	slíbit
mamince	maminka	k1gFnSc3	maminka
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
paní	paní	k1gFnSc1	paní
Soumarové	soumarový	k2eAgNnSc1d1	soumarový
říkat	říkat	k5eAaImF	říkat
"	"	kIx"	"
<g/>
rukulíbám	rukulíbat	k5eAaImIp1nS	rukulíbat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ukázal	ukázat	k5eAaPmAgInS	ukázat
svoje	svůj	k3xOyFgNnSc4	svůj
dobré	dobrý	k2eAgNnSc4d1	dobré
vychování	vychování	k1gNnSc4	vychování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stydí	stydět	k5eAaImIp3nP	stydět
se	se	k3xPyFc4	se
a	a	k8xC	a
neříká	říkat	k5eNaImIp3nS	říkat
to	ten	k3xDgNnSc1	ten
<g/>
.	.	kIx.	.
</s>
<s>
Půjčuje	půjčovat	k5eAaImIp3nS	půjčovat
si	se	k3xPyFc3	se
od	od	k7c2	od
Otakárka	Otakárek	k1gMnSc2	Otakárek
knížky	knížka	k1gFnSc2	knížka
<g/>
.	.	kIx.	.
</s>
<s>
Otakárkově	Otakárkův	k2eAgFnSc3d1	Otakárkův
vychovatelce	vychovatelka	k1gFnSc3	vychovatelka
však	však	k8xC	však
malý	malý	k2eAgMnSc1d1	malý
Bajza	Bajza	k1gMnSc1	Bajza
není	být	k5eNaImIp3nS	být
po	po	k7c6	po
chuti	chuť	k1gFnSc6	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Příbuzní	příbuzný	k1gMnPc1	příbuzný
Vařekovi	Vařekův	k2eAgMnPc1d1	Vařekův
jdou	jít	k5eAaImIp3nP	jít
občas	občas	k6eAd1	občas
Bajzovy	Bajzův	k2eAgInPc4d1	Bajzův
navštívit	navštívit	k5eAaPmF	navštívit
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
skrývají	skrývat	k5eAaImIp3nP	skrývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
obědu	oběd	k1gInSc3	oběd
husu	husa	k1gFnSc4	husa
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
je	on	k3xPp3gNnSc4	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
Vařekovi	Vařeek	k1gMnSc3	Vařeek
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
diví	divit	k5eAaImIp3nS	divit
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ty	ten	k3xDgInPc4	ten
peníze	peníz	k1gInPc4	peníz
přijdou	přijít	k5eAaPmIp3nP	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
do	do	k7c2	do
města	město	k1gNnSc2	město
přijede	přijet	k5eAaPmIp3nS	přijet
cirkus	cirkus	k1gInSc1	cirkus
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
touží	toužit	k5eAaImIp3nS	toužit
vidět	vidět	k5eAaImF	vidět
cizokrajná	cizokrajný	k2eAgNnPc4d1	cizokrajné
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hodný	hodný	k2eAgInSc4d1	hodný
<g/>
,	,	kIx,	,
pozorný	pozorný	k2eAgInSc4d1	pozorný
<g/>
,	,	kIx,	,
poslušný	poslušný	k2eAgInSc4d1	poslušný
<g/>
,	,	kIx,	,
nápomocný	nápomocný	k2eAgInSc4d1	nápomocný
a	a	k8xC	a
vzorný	vzorný	k2eAgInSc4d1	vzorný
<g/>
,	,	kIx,	,
až	až	k9	až
to	ten	k3xDgNnSc1	ten
rodičům	rodič	k1gMnPc3	rodič
připadá	připadat	k5eAaPmIp3nS	připadat
podezřelé	podezřelý	k2eAgNnSc1d1	podezřelé
a	a	k8xC	a
myslí	myslet	k5eAaImIp3nS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Učí	učit	k5eAaImIp3nS	učit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
nechodí	chodit	k5eNaImIp3nP	chodit
ven	ven	k6eAd1	ven
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
<g/>
,	,	kIx,	,
hlídá	hlídat	k5eAaImIp3nS	hlídat
Mančinku	Mančinka	k1gFnSc4	Mančinka
a	a	k8xC	a
nepere	prát	k5eNaImIp3nS	prát
se	se	k3xPyFc4	se
s	s	k7c7	s
Rampepurdou	Rampepurda	k1gFnSc7	Rampepurda
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
chlapec	chlapec	k1gMnSc1	chlapec
z	z	k7c2	z
cirkusu	cirkus	k1gInSc2	cirkus
<g/>
,	,	kIx,	,
Alfons	Alfons	k1gMnSc1	Alfons
Kasalický	kasalický	k2eAgMnSc1d1	kasalický
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nP	chodit
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Kluci	kluk	k1gMnPc1	kluk
jsou	být	k5eAaImIp3nP	být
hrdí	hrdý	k2eAgMnPc1d1	hrdý
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
mohou	moct	k5eAaImIp3nP	moct
kamarádit	kamarádit	k5eAaImF	kamarádit
<g/>
.	.	kIx.	.
</s>
<s>
Petřík	Petřík	k1gMnSc1	Petřík
nakonec	nakonec	k6eAd1	nakonec
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
rodiče	rodič	k1gMnPc4	rodič
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
šli	jít	k5eAaImAgMnP	jít
společně	společně	k6eAd1	společně
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
představení	představení	k1gNnSc4	představení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cirkuse	cirkus	k1gInSc6	cirkus
je	být	k5eAaImIp3nS	být
i	i	k9	i
Zilvar	Zilvar	k1gInSc1	Zilvar
z	z	k7c2	z
chudobince	chudobinec	k1gInSc2	chudobinec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tam	tam	k6eAd1	tam
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
představení	představení	k1gNnSc4	představení
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
bez	bez	k7c2	bez
placení	placení	k1gNnSc2	placení
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
kluci	kluk	k1gMnPc1	kluk
chtějí	chtít	k5eAaImIp3nP	chtít
v	v	k7c6	v
cirkuse	cirkus	k1gInSc6	cirkus
taky	taky	k6eAd1	taky
vypomáhat	vypomáhat	k5eAaImF	vypomáhat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nosí	nosit	k5eAaImIp3nS	nosit
zvířatům	zvíře	k1gNnPc3	zvíře
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Přijde	přijít	k5eAaPmIp3nS	přijít
za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
Otakárek	Otakárek	k1gMnSc1	Otakárek
a	a	k8xC	a
taky	taky	k6eAd1	taky
jim	on	k3xPp3gMnPc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jeho	jeho	k3xOp3gFnSc1	jeho
vychovatelka	vychovatelka	k1gFnSc1	vychovatelka
udělá	udělat	k5eAaPmIp3nS	udělat
hysterickou	hysterický	k2eAgFnSc4d1	hysterická
scénu	scéna	k1gFnSc4	scéna
<g/>
,	,	kIx,	,
že	že	k8xS	že
Otakárek	Otakárek	k1gMnSc1	Otakárek
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
bohaté	bohatý	k2eAgFnSc2d1	bohatá
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
nebude	být	k5eNaImBp3nS	být
dělat	dělat	k5eAaImF	dělat
takovouhle	takovýhle	k3xDgFnSc4	takovýhle
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
cirkusu	cirkus	k1gInSc2	cirkus
poté	poté	k6eAd1	poté
vyhodí	vyhodit	k5eAaPmIp3nS	vyhodit
všechny	všechen	k3xTgMnPc4	všechen
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
mu	on	k3xPp3gNnSc3	on
Čeněk	Čeněk	k1gMnSc1	Čeněk
Jirsák	Jirsák	k1gMnSc1	Jirsák
vynadá	vynadat	k5eAaPmIp3nS	vynadat
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
originální	originální	k2eAgInSc4d1	originální
hřích	hřích	k1gInSc4	hřích
ke	k	k7c3	k
zpovědi	zpověď	k1gFnSc3	zpověď
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
vynadal	vynadat	k5eAaPmAgMnS	vynadat
jsem	být	k5eAaImIp1nS	být
cirkusákovi	cirkusák	k1gMnSc6	cirkusák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
spálou	spála	k1gFnSc7	spála
<g/>
.	.	kIx.	.
</s>
<s>
Zdají	zdát	k5eAaImIp3nP	zdát
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dobrodružné	dobrodružný	k2eAgInPc1d1	dobrodružný
sny	sen	k1gInPc1	sen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snu	sen	k1gInSc6	sen
mu	on	k3xPp3gNnSc3	on
tatínek	tatínek	k1gMnSc1	tatínek
koupí	koupě	k1gFnPc2	koupě
slona	slon	k1gMnSc2	slon
indického	indický	k2eAgMnSc2d1	indický
-	-	kIx~	-
Jumba	Jumb	k1gMnSc2	Jumb
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
umí	umět	k5eAaImIp3nS	umět
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Slon	slon	k1gMnSc1	slon
je	být	k5eAaImIp3nS	být
obdivován	obdivován	k2eAgInSc1d1	obdivován
celým	celý	k2eAgNnSc7d1	celé
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jumbem	Jumb	k1gInSc7	Jumb
<g/>
,	,	kIx,	,
Evou	Eva	k1gFnSc7	Eva
a	a	k8xC	a
ostatními	ostatní	k2eAgMnPc7d1	ostatní
kluky	kluk	k1gMnPc7	kluk
se	se	k3xPyFc4	se
vypraví	vypravit	k5eAaPmIp3nS	vypravit
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Navštíví	navštívit	k5eAaPmIp3nS	navštívit
krejčího	krejčí	k1gMnSc2	krejčí
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
vypraví	vypravit	k5eAaPmIp3nS	vypravit
za	za	k7c2	za
maharádžou	maharádzat	k5eAaPmIp3nP	maharádzat
<g/>
.	.	kIx.	.
</s>
<s>
Účastní	účastnit	k5eAaImIp3nP	účastnit
se	se	k3xPyFc4	se
lovu	lov	k1gInSc3	lov
tygrů	tygr	k1gMnPc2	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Zilvar	Zilvar	k1gInSc1	Zilvar
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
maharádžovy	maharádžův	k2eAgFnSc2d1	maharádžův
dcery	dcera	k1gFnSc2	dcera
a	a	k8xC	a
požádá	požádat	k5eAaPmIp3nS	požádat
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
dán	dát	k5eAaPmNgInS	dát
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Princezna	princezna	k1gFnSc1	princezna
steskem	stesk	k1gInSc7	stesk
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
má	mít	k5eAaImIp3nS	mít
o	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
něco	něco	k3yInSc4	něco
neudělala	udělat	k5eNaPmAgFnS	udělat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
maharádžu	maharádzat	k5eAaPmIp1nS	maharádzat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dal	dát	k5eAaPmAgInS	dát
Pepkovi	Pepek	k1gMnSc3	Pepek
Zilvarovi	Zilvar	k1gMnSc3	Zilvar
šanci	šance	k1gFnSc4	šance
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
vyveden	vyvést	k5eAaPmNgMnS	vyvést
z	z	k7c2	z
kobky	kobka	k1gFnSc2	kobka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gMnSc3	on
dány	dát	k5eAaPmNgFnP	dát
tři	tři	k4xCgFnPc1	tři
otázky	otázka	k1gFnPc1	otázka
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
je	on	k3xPp3gInPc4	on
zodpoví	zodpovědět	k5eAaPmIp3nS	zodpovědět
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
princeznu	princezna	k1gFnSc4	princezna
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
ne	ne	k9	ne
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
o	o	k7c4	o
hlavu	hlava	k1gFnSc4	hlava
kratší	krátký	k2eAgFnSc4d2	kratší
<g/>
.	.	kIx.	.
</s>
<s>
Zilvar	Zilvar	k1gInSc1	Zilvar
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
chytrý	chytrý	k2eAgInSc1d1	chytrý
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
pohotové	pohotový	k2eAgFnSc3d1	pohotová
odpovědi	odpověď	k1gFnSc3	odpověď
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
svatba	svatba	k1gFnSc1	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
to	ten	k3xDgNnSc1	ten
Pepkovi	Pepkův	k2eAgMnPc1d1	Pepkův
všechno	všechen	k3xTgNnSc4	všechen
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
už	už	k6eAd1	už
nemá	mít	k5eNaImIp3nS	mít
o	o	k7c4	o
své	svůj	k3xOyFgMnPc4	svůj
kamarády	kamarád	k1gMnPc4	kamarád
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
mu	on	k3xPp3gMnSc3	on
pomstít	pomstít	k5eAaPmF	pomstít
a	a	k8xC	a
Tonda	Tonda	k1gMnSc1	Tonda
Bejval	Bejval	k1gMnSc1	Bejval
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
hodí	hodit	k5eAaPmIp3nP	hodit
během	během	k7c2	během
svatebního	svatební	k2eAgInSc2d1	svatební
obřadu	obřad	k1gInSc2	obřad
zkažené	zkažený	k2eAgNnSc1d1	zkažené
pštrosí	pštrosí	k2eAgNnSc1d1	pštrosí
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Zilvar	Zilvar	k1gInSc1	Zilvar
se	se	k3xPyFc4	se
vrhne	vrhnout	k5eAaImIp3nS	vrhnout
na	na	k7c4	na
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
princezna	princezna	k1gFnSc1	princezna
na	na	k7c4	na
Evu	Eva	k1gFnSc4	Eva
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
strhne	strhnout	k5eAaPmIp3nS	strhnout
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
mela	mlít	k5eAaImSgMnS	mlít
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
Péťa	Péťa	k?	Péťa
probere	probrat	k5eAaPmIp3nS	probrat
a	a	k8xC	a
zvolna	zvolna	k6eAd1	zvolna
mu	on	k3xPp3gMnSc3	on
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
všechno	všechen	k3xTgNnSc1	všechen
jen	jen	k9	jen
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
natočil	natočit	k5eAaBmAgMnS	natočit
český	český	k2eAgMnSc1d1	český
režisér	režisér	k1gMnSc1	režisér
Karel	Karel	k1gMnSc1	Karel
Smyczek	Smyczek	k1gInSc4	Smyczek
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
tohoto	tento	k3xDgInSc2	tento
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
hlavních	hlavní	k2eAgInPc2d1	hlavní
rolích	role	k1gFnPc6	role
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Adama	Adam	k1gMnSc4	Adam
Nováka	Novák	k1gMnSc4	Novák
<g/>
,	,	kIx,	,
Sandru	Sandra	k1gFnSc4	Sandra
Novákovou	Nováková	k1gFnSc4	Nováková
<g/>
,	,	kIx,	,
Barboru	Barbora	k1gFnSc4	Barbora
Srncovou	Srncová	k1gFnSc4	Srncová
<g/>
,	,	kIx,	,
Gabrielu	Gabriela	k1gFnSc4	Gabriela
Wilhelmovou	Wilhelmová	k1gFnSc4	Wilhelmová
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc4	Dagmar
Veškrnovou	Veškrnová	k1gFnSc4	Veškrnová
<g/>
,	,	kIx,	,
Oldřicha	Oldřich	k1gMnSc4	Oldřich
Navrátila	Navrátil	k1gMnSc4	Navrátil
<g/>
,	,	kIx,	,
<g/>
Valentinu	Valentina	k1gFnSc4	Valentina
Thielovou	Thielová	k1gFnSc4	Thielová
<g/>
,	,	kIx,	,
Lenku	Lenka	k1gFnSc4	Lenka
Termerovou	Termerová	k1gFnSc4	Termerová
<g/>
,	,	kIx,	,
Ljubu	Ljuba	k1gFnSc4	Ljuba
Krbovou	krbový	k2eAgFnSc4d1	krbová
<g/>
,	,	kIx,	,
Naďu	Naďa	k1gFnSc4	Naďa
Konvalinkovou	Konvalinková	k1gFnSc4	Konvalinková
<g/>
,	,	kIx,	,
Jiřinu	Jiřina	k1gFnSc4	Jiřina
Jiráskovou	Jirásková	k1gFnSc4	Jirásková
<g/>
,	,	kIx,	,
Marii	Maria	k1gFnSc3	Maria
Durnovou	Durnová	k1gFnSc4	Durnová
<g/>
,	,	kIx,	,
Nelu	Nela	k1gFnSc4	Nela
Boudovou	Boudová	k1gFnSc4	Boudová
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
Nárožného	Nárožný	k2eAgMnSc2d1	Nárožný
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc2	Jiří
Stracha	Strach	k1gMnSc2	Strach
aj.	aj.	kA	aj.
Román	Román	k1gMnSc1	Román
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
proslavil	proslavit	k5eAaPmAgInS	proslavit
František	františek	k1gInSc1	františek
Filipovský	Filipovský	k2eAgInSc1d1	Filipovský
rozhlasovou	rozhlasový	k2eAgFnSc7d1	rozhlasová
četbou	četba	k1gFnSc7	četba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
POLÁČEK	Poláček	k1gMnSc1	Poláček
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nás	my	k3xPp1nPc4	my
pět	pět	k4xCc1	pět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SNKL	SNKL	kA	SNKL
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
230	[number]	k4	230
s.	s.	k?	s.
</s>
