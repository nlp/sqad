<s>
Claude	Claude	k6eAd1	Claude
Bernard	Bernard	k1gMnSc1	Bernard
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1813	[number]	k4	1813
Saint-Julien	Saint-Julina	k1gFnPc2	Saint-Julina
u	u	k7c2	u
Villefranche-sur-Saône	Villefrancheur-Saôn	k1gInSc5	Villefranche-sur-Saôn
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1878	[number]	k4	1878
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
experimentální	experimentální	k2eAgFnSc2d1	experimentální
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
fyziologie	fyziologie	k1gFnSc2	fyziologie
<g/>
.	.	kIx.	.
</s>
