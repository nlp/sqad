<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
iPhone	iPhon	k1gMnSc5	iPhon
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
mobilních	mobilní	k2eAgNnPc6d1	mobilní
zařízeních	zařízení	k1gNnPc6	zařízení
této	tento	k3xDgFnSc2	tento
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
iPod	iPod	k1gMnSc1	iPod
Touch	Touch	k1gMnSc1	Touch
<g/>
,	,	kIx,	,
iPad	iPad	k1gMnSc1	iPad
a	a	k8xC	a
nejnověji	nově	k6eAd3	nově
Apple	Apple	kA	Apple
TV	TV	kA	TV
<g/>
.	.	kIx.	.
</s>
