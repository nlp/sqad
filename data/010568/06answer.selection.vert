<s>
Mao	Mao	k?	Mao
Ce-tung	Ceung	k1gInSc1	Ce-tung
(	(	kIx(	(
<g/>
tradiční	tradiční	k2eAgFnSc1d1	tradiční
čínština	čínština	k1gFnSc1	čínština
<g/>
:	:	kIx,	:
毛	毛	k?	毛
<g/>
,	,	kIx,	,
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
čínština	čínština	k1gFnSc1	čínština
<g/>
:	:	kIx,	:
毛	毛	k?	毛
<g/>
,	,	kIx,	,
pī	pī	k?	pī
<g/>
:	:	kIx,	:
Máo	Máo	k1gMnSc1	Máo
Zédō	Zédō	k1gMnSc1	Zédō
<g/>
;	;	kIx,	;
nové	nový	k2eAgNnSc1d1	nové
jméno	jméno	k1gNnSc1	jméno
<g/>
:	:	kIx,	:
Rù	Rù	k1gFnSc1	Rù
润	润	k?	润
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1893	[number]	k4	1893
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1935	[number]	k4	1935
tajemníkem	tajemník	k1gMnSc7	tajemník
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
předsedou	předseda	k1gMnSc7	předseda
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
