<p>
<s>
Mao	Mao	k?	Mao
Ce-tung	Ceung	k1gInSc1	Ce-tung
(	(	kIx(	(
<g/>
tradiční	tradiční	k2eAgFnSc1d1	tradiční
čínština	čínština	k1gFnSc1	čínština
<g/>
:	:	kIx,	:
毛	毛	k?	毛
<g/>
,	,	kIx,	,
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
čínština	čínština	k1gFnSc1	čínština
<g/>
:	:	kIx,	:
毛	毛	k?	毛
<g/>
,	,	kIx,	,
pī	pī	k?	pī
<g/>
:	:	kIx,	:
Máo	Máo	k1gMnSc1	Máo
Zédō	Zédō	k1gMnSc1	Zédō
<g/>
;	;	kIx,	;
nové	nový	k2eAgNnSc1d1	nové
jméno	jméno	k1gNnSc1	jméno
<g/>
:	:	kIx,	:
Rù	Rù	k1gFnSc1	Rù
润	润	k?	润
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1893	[number]	k4	1893
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1935	[number]	k4	1935
tajemníkem	tajemník	k1gMnSc7	tajemník
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
předsedou	předseda	k1gMnSc7	předseda
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
nacionalisty	nacionalista	k1gMnPc7	nacionalista
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
byl	být	k5eAaImAgInS	být
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
osobností	osobnost	k1gFnSc7	osobnost
v	v	k7c6	v
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc6d1	vzniklá
komunistické	komunistický	k2eAgFnSc6d1	komunistická
Čínské	čínský	k2eAgFnSc6d1	čínská
lidové	lidový	k2eAgFnSc6d1	lidová
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
oficiální	oficiální	k2eAgFnSc4d1	oficiální
přezdívku	přezdívka	k1gFnSc4	přezdívka
Velký	velký	k2eAgMnSc1d1	velký
kormidelník	kormidelník	k1gMnSc1	kormidelník
(	(	kIx(	(
<g/>
伟	伟	k?	伟
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Josifem	Josif	k1gMnSc7	Josif
Stalinem	Stalin	k1gMnSc7	Stalin
a	a	k8xC	a
Adolfem	Adolf	k1gMnSc7	Adolf
Hitlerem	Hitler	k1gMnSc7	Hitler
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
třem	tři	k4xCgMnPc3	tři
nejkrutějším	krutý	k2eAgMnPc3d3	nejkrutější
diktátorům	diktátor	k1gMnPc3	diktátor
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
připisuje	připisovat	k5eAaImIp3nS	připisovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
milionů	milion	k4xCgInPc2	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
však	však	k9	však
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
těší	těšit	k5eAaImIp3nS	těšit
úctě	úcta	k1gFnSc3	úcta
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
dosud	dosud	k6eAd1	dosud
vyobrazen	vyobrazit	k5eAaPmNgInS	vyobrazit
na	na	k7c6	na
čínských	čínský	k2eAgFnPc6d1	čínská
bankovkách	bankovka	k1gFnPc6	bankovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc4	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Šao-šan	Šao-šany	k1gInPc2	Šao-šany
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
bohaté	bohatý	k2eAgFnSc3d1	bohatá
rolnické	rolnický	k2eAgFnSc3d1	rolnická
rodině	rodina	k1gFnSc3	rodina
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Chu-nan	Chuany	k1gInPc2	Chu-nany
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gMnPc3	jeho
rodičům	rodič	k1gMnPc3	rodič
patřilo	patřit	k5eAaImAgNnS	patřit
asi	asi	k9	asi
1,2	[number]	k4	1,2
hektarů	hektar	k1gInPc2	hektar
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Mao	Mao	k?	Mao
měl	mít	k5eAaImAgMnS	mít
šest	šest	k4xCc4	šest
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
on	on	k3xPp3gMnSc1	on
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
překonali	překonat	k5eAaPmAgMnP	překonat
dětské	dětský	k2eAgFnPc4d1	dětská
nemoci	nemoc	k1gFnPc4	nemoc
a	a	k8xC	a
dožili	dožít	k5eAaPmAgMnP	dožít
se	se	k3xPyFc4	se
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Mao	Mao	k?	Mao
měl	mít	k5eAaImAgInS	mít
citově	citově	k6eAd1	citově
blízko	blízko	k6eAd1	blízko
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
oddané	oddaný	k2eAgFnSc3d1	oddaná
buddhistce	buddhistka	k1gFnSc3	buddhistka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
později	pozdě	k6eAd2	pozdě
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
objímala	objímat	k5eAaImAgFnS	objímat
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
sahala	sahat	k5eAaImAgFnS	sahat
daleko	daleko	k6eAd1	daleko
i	i	k8xC	i
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Z	z	k7c2	z
Maova	Maovo	k1gNnSc2	Maovo
dětství	dětství	k1gNnSc2	dětství
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
pár	pár	k4xCyI	pár
kusých	kusý	k2eAgFnPc2d1	kusá
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
otcem	otec	k1gMnSc7	otec
moc	moc	k6eAd1	moc
blízké	blízký	k2eAgInPc4d1	blízký
vztahy	vztah	k1gInPc4	vztah
neměl	mít	k5eNaImAgInS	mít
a	a	k8xC	a
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinu	většina	k1gFnSc4	většina
dětství	dětství	k1gNnSc2	dětství
prožil	prožít	k5eAaPmAgMnS	prožít
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
prarodičů	prarodič	k1gMnPc2	prarodič
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
sloužil	sloužit	k5eAaImAgMnS	sloužit
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
jako	jako	k8xS	jako
voják	voják	k1gMnSc1	voják
provinční	provinční	k2eAgFnSc2d1	provinční
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
býval	bývat	k5eAaImAgInS	bývat
často	často	k6eAd1	často
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
čtrnácti	čtrnáct	k4xCc6	čtrnáct
letech	let	k1gInPc6	let
Mao	Mao	k1gFnSc2	Mao
–	–	k?	–
který	který	k3yRgMnSc1	který
teď	teď	k6eAd1	teď
dokázal	dokázat	k5eAaPmAgMnS	dokázat
číst	číst	k5eAaImF	číst
a	a	k8xC	a
používat	používat	k5eAaImF	používat
počítadlo	počítadlo	k1gNnSc4	počítadlo
–	–	k?	–
odešel	odejít	k5eAaPmAgMnS	odejít
ze	z	k7c2	z
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
otci	otec	k1gMnSc3	otec
staral	starat	k5eAaImAgInS	starat
o	o	k7c6	o
účetnictví	účetnictví	k1gNnSc6	účetnictví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevycházeli	vycházet	k5eNaImAgMnP	vycházet
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
První	první	k4xOgMnSc1	první
kapitalista	kapitalista	k1gMnSc1	kapitalista
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterému	který	k3yRgNnSc3	který
jsem	být	k5eAaImIp1nS	být
bojoval	bojovat	k5eAaImAgMnS	bojovat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
před	před	k7c7	před
americkým	americký	k2eAgMnSc7d1	americký
žurnalistou	žurnalista	k1gMnSc7	žurnalista
Edgarem	Edgar	k1gMnSc7	Edgar
Snowem	Snow	k1gMnSc7	Snow
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
byl	být	k5eAaImAgMnS	být
můj	můj	k1gMnSc1	můj
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mao	Mao	k1gFnSc1	Mao
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
oženil	oženit	k5eAaPmAgMnS	oženit
a	a	k8xC	a
vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
devatenáctiletou	devatenáctiletý	k2eAgFnSc4d1	devatenáctiletá
dívku	dívka	k1gFnSc4	dívka
Luo-š	Luo-š	k1gInSc1	Luo-š
<g/>
'	'	kIx"	'
ze	z	k7c2	z
sousedního	sousední	k2eAgInSc2d1	sousední
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
trvalo	trvat	k5eAaImAgNnS	trvat
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
až	až	k9	až
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
smrti	smrt	k1gFnSc2	smrt
z	z	k7c2	z
neznámých	známý	k2eNgFnPc2d1	neznámá
příčin	příčina	k1gFnPc2	příčina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
jednadvaceti	jednadvacet	k4xCc2	jednadvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Vzdělání	vzdělání	k1gNnPc2	vzdělání
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
v	v	k7c4	v
Čchang-ša	Čchang-šum	k1gNnPc4	Čchang-šum
<g/>
.	.	kIx.	.
</s>
<s>
Mao	Mao	k?	Mao
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
věnoval	věnovat	k5eAaImAgMnS	věnovat
vyučování	vyučování	k1gNnSc4	vyučování
a	a	k8xC	a
novinařině	novinařina	k1gFnSc6	novinařina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
Pekingské	pekingský	k2eAgFnSc2d1	Pekingská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
současníků	současník	k1gMnPc2	současník
nevyužil	využít	k5eNaPmAgMnS	využít
možnosti	možnost	k1gFnPc4	možnost
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
myšlenkami	myšlenka	k1gFnPc7	myšlenka
marxismu	marxismus	k1gInSc2	marxismus
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
budovat	budovat	k5eAaImF	budovat
Komunistickou	komunistický	k2eAgFnSc4d1	komunistická
stranu	strana	k1gFnSc4	strana
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Revolucionář	revolucionář	k1gMnSc1	revolucionář
<g/>
,	,	kIx,	,
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
maoismu	maoismus	k1gInSc2	maoismus
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
revolucionář	revolucionář	k1gMnSc1	revolucionář
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
organizování	organizování	k1gNnPc4	organizování
rolníků	rolník	k1gMnPc2	rolník
v	v	k7c4	v
politickou	politický	k2eAgFnSc4d1	politická
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gInSc6	jehož
neúspěchu	neúspěch	k1gInSc6	neúspěch
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
do	do	k7c2	do
sovětu	sovět	k1gInSc2	sovět
v	v	k7c6	v
Ťiang-si	Ťiange	k1gFnSc6	Ťiang-se
<g/>
.	.	kIx.	.
</s>
<s>
Obklíčení	obklíčení	k1gNnSc1	obklíčení
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
Čankajškovými	Čankajškův	k2eAgFnPc7d1	Čankajškova
silami	síla	k1gFnPc7	síla
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mao	Mao	k1gFnSc1	Mao
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
soudruhy	soudruh	k1gMnPc7	soudruh
zahájili	zahájit	k5eAaPmAgMnP	zahájit
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
Dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
pochod	pochod	k1gInSc4	pochod
–	–	k?	–
právě	právě	k9	právě
během	během	k7c2	během
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
Mao	Mao	k1gMnSc1	Mao
prosadil	prosadit	k5eAaPmAgMnS	prosadit
jako	jako	k9	jako
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gFnPc1	jeho
zdecimované	zdecimovaný	k2eAgFnPc1d1	zdecimovaná
síly	síla	k1gFnPc1	síla
dorazily	dorazit	k5eAaPmAgFnP	dorazit
do	do	k7c2	do
Jen-anu	Jenn	k1gInSc2	Jen-an
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
Mao	Mao	k1gFnSc2	Mao
schopen	schopen	k2eAgInSc1d1	schopen
toho	ten	k3xDgNnSc2	ten
bránit	bránit	k5eAaImF	bránit
se	se	k3xPyFc4	se
Stalinovu	Stalinův	k2eAgInSc3d1	Stalinův
tlaku	tlak	k1gInSc3	tlak
na	na	k7c4	na
dosazení	dosazení	k1gNnSc4	dosazení
promoskevského	promoskevský	k2eAgNnSc2d1	promoskevské
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
kult	kult	k1gInSc4	kult
osobnosti	osobnost	k1gFnSc2	osobnost
a	a	k8xC	a
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
i	i	k9	i
vlastní	vlastní	k2eAgFnSc4d1	vlastní
politickou	politický	k2eAgFnSc4d1	politická
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vešla	vejít	k5eAaPmAgFnS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
jako	jako	k8xC	jako
Mao	Mao	k1gFnSc4	Mao
Ce-tungovo	Ceungův	k2eAgNnSc1d1	Ce-tungovo
myšlení	myšlení	k1gNnSc1	myšlení
(	(	kIx(	(
<g/>
maoismus	maoismus	k1gInSc1	maoismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojenectví	spojenectví	k1gNnSc1	spojenectví
s	s	k7c7	s
Kuomintangem	Kuomintang	k1gInSc7	Kuomintang
během	během	k7c2	během
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Japoncům	Japonec	k1gMnPc3	Japonec
a	a	k8xC	a
pomoc	pomoc	k1gFnSc1	pomoc
Moskvy	Moskva	k1gFnSc2	Moskva
mu	on	k3xPp3gMnSc3	on
umožnily	umožnit	k5eAaPmAgInP	umožnit
vybudovat	vybudovat	k5eAaPmF	vybudovat
značnou	značný	k2eAgFnSc4d1	značná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
rozšířit	rozšířit	k5eAaPmF	rozšířit
síť	síť	k1gFnSc4	síť
guerillových	guerillový	k2eAgFnPc2d1	guerillová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
udržet	udržet	k5eAaPmF	udržet
stranu	strana	k1gFnSc4	strana
při	při	k7c6	při
životě	život	k1gInSc6	život
i	i	k8xC	i
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
nejtěžších	těžký	k2eAgFnPc6d3	nejtěžší
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
rozhodná	rozhodný	k2eAgNnPc1d1	rozhodné
vítězství	vítězství	k1gNnPc1	vítězství
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
mu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
vytvoření	vytvoření	k1gNnSc6	vytvoření
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
přinesla	přinést	k5eAaPmAgFnS	přinést
obrovský	obrovský	k2eAgInSc4d1	obrovský
věhlas	věhlas	k1gInSc4	věhlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vláda	vláda	k1gFnSc1	vláda
Velkého	velký	k2eAgMnSc2d1	velký
kormidelníka	kormidelník	k1gMnSc2	kormidelník
===	===	k?	===
</s>
</p>
<p>
<s>
Maovi	Maoev	k1gFnSc3	Maoev
systém	systém	k1gInSc4	systém
sovětského	sovětský	k2eAgInSc2d1	sovětský
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
nevyhovoval	vyhovovat	k5eNaImAgMnS	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
přistoupit	přistoupit	k5eAaPmF	přistoupit
k	k	k7c3	k
Velkému	velký	k2eAgInSc3d1	velký
skoku	skok	k1gInSc3	skok
vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgInS	mít
zemi	zem	k1gFnSc4	zem
rychle	rychle	k6eAd1	rychle
přiblížit	přiblížit	k5eAaPmF	přiblížit
komunistické	komunistický	k2eAgFnSc3d1	komunistická
Utopii	utopie	k1gFnSc3	utopie
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
následný	následný	k2eAgInSc1d1	následný
hladomor	hladomor	k1gInSc1	hladomor
přinesl	přinést	k5eAaPmAgInS	přinést
smrt	smrt	k1gFnSc4	smrt
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
milionům	milion	k4xCgInPc3	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Totální	totální	k2eAgNnSc4d1	totální
fiasko	fiasko	k1gNnSc4	fiasko
Velkého	velký	k2eAgInSc2d1	velký
skoku	skok	k1gInSc2	skok
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Maa	Maa	k1gFnSc4	Maa
těžkou	těžký	k2eAgFnSc7d1	těžká
ranou	rána	k1gFnSc7	rána
a	a	k8xC	a
ponížením	ponížení	k1gNnSc7	ponížení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
obnovit	obnovit	k5eAaPmF	obnovit
svou	svůj	k3xOyFgFnSc4	svůj
prestiž	prestiž	k1gFnSc4	prestiž
a	a	k8xC	a
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
s	s	k7c7	s
účelem	účel	k1gInSc7	účel
eliminovat	eliminovat	k5eAaBmF	eliminovat
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
Kulturní	kulturní	k2eAgFnSc4d1	kulturní
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
byli	být	k5eAaImAgMnP	být
postiženi	postižen	k2eAgMnPc1d1	postižen
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
podezříval	podezřívat	k5eAaImAgInS	podezřívat
z	z	k7c2	z
neposlušnosti	neposlušnost	k1gFnSc2	neposlušnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
Mao	Mao	k1gFnSc2	Mao
narušil	narušit	k5eAaPmAgMnS	narušit
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Čínu	Čína	k1gFnSc4	Čína
odřízl	odříznout	k5eAaPmAgMnS	odříznout
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
zemí	zem	k1gFnPc2	zem
komunistického	komunistický	k2eAgInSc2d1	komunistický
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neodvratně	odvratně	k6eNd1	odvratně
blíží	blížit	k5eAaImIp3nS	blížit
atomová	atomový	k2eAgFnSc1d1	atomová
válka	válka	k1gFnSc1	válka
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zemi	zem	k1gFnSc3	zem
uvrhl	uvrhnout	k5eAaPmAgInS	uvrhnout
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
horečného	horečný	k2eAgNnSc2d1	horečné
zbrojení	zbrojení	k1gNnSc2	zbrojení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
zruinovalo	zruinovat	k5eAaPmAgNnS	zruinovat
hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Ochlazení	ochlazení	k1gNnSc1	ochlazení
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
napomohla	napomoct	k5eAaPmAgFnS	napomoct
také	také	k9	také
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Mao	Mao	k1gFnSc4	Mao
ostře	ostro	k6eAd1	ostro
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
též	též	k9	též
několik	několik	k4yIc1	několik
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
potyček	potyčka	k1gFnPc2	potyčka
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
se	se	k3xPyFc4	se
SSSR	SSSR	kA	SSSR
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Ussuri	Ussur	k1gFnSc2	Ussur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rudý	rudý	k2eAgInSc1d1	rudý
teror	teror	k1gInSc1	teror
===	===	k?	===
</s>
</p>
<p>
<s>
Mao	Mao	k?	Mao
Ce-tung	Ceung	k1gInSc1	Ce-tung
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
40	[number]	k4	40
až	až	k9	až
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
hladu	hlad	k1gInSc2	hlad
<g/>
,	,	kIx,	,
nucených	nucený	k2eAgFnPc2d1	nucená
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
poprav	poprava	k1gFnPc2	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
zavedl	zavést	k5eAaPmAgInS	zavést
politiku	politika	k1gFnSc4	politika
vybrat	vybrat	k5eAaPmF	vybrat
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
popravu	poprava	k1gFnSc4	poprava
alespoň	alespoň	k9	alespoň
jednoho	jeden	k4xCgMnSc4	jeden
statkáře	statkář	k1gMnSc4	statkář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
několik	několik	k4yIc1	několik
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
čínské	čínský	k2eAgFnSc6d1	čínská
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
odhadem	odhad	k1gInSc7	odhad
2	[number]	k4	2
až	až	k8xS	až
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
vlády	vláda	k1gFnSc2	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
devátý	devátý	k4xOgInSc4	devátý
sjezd	sjezd	k1gInSc4	sjezd
strany	strana	k1gFnSc2	strana
Mao	Mao	k1gFnSc1	Mao
Ce-tunga	Ceunga	k1gFnSc1	Ce-tunga
neomezeným	omezený	k2eNgMnSc7d1	neomezený
vůdcem	vůdce	k1gMnSc7	vůdce
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Ťiang	Ťiang	k1gMnSc1	Ťiang
Čching	Čching	k1gInSc1	Čching
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
členem	člen	k1gInSc7	člen
politbyra	politbyro	k1gNnSc2	politbyro
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Desátý	desátý	k4xOgInSc4	desátý
sjezd	sjezd	k1gInSc4	sjezd
strany	strana	k1gFnSc2	strana
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
postavení	postavení	k1gNnSc4	postavení
Maa	Maa	k1gFnSc2	Maa
a	a	k8xC	a
Ťiang	Ťiang	k1gMnSc1	Ťiang
Čching	Čching	k1gInSc4	Čching
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Mao	Mao	k1gMnSc1	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
už	už	k6eAd1	už
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
věkem	věk	k1gInSc7	věk
pomalu	pomalu	k6eAd1	pomalu
ztrácel	ztrácet	k5eAaImAgInS	ztrácet
svůj	svůj	k3xOyFgInSc4	svůj
mocenský	mocenský	k2eAgInSc4d1	mocenský
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
politiku	politika	k1gFnSc4	politika
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nestaral	starat	k5eNaImAgMnS	starat
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
trávil	trávit	k5eAaImAgMnS	trávit
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
mladými	mladý	k2eAgFnPc7d1	mladá
milenkami	milenka	k1gFnPc7	milenka
<g/>
.	.	kIx.	.
</s>
<s>
Mao	Mao	k?	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
zemřel	zemřít	k5eAaPmAgMnS	zemřít
9	[number]	k4	9
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1976	[number]	k4	1976
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
82	[number]	k4	82
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zlepšily	zlepšit	k5eAaPmAgInP	zlepšit
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
boj	boj	k1gInSc1	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
mezi	mezi	k7c7	mezi
zastánci	zastánce	k1gMnPc7	zastánce
pragmatického	pragmatický	k2eAgInSc2d1	pragmatický
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
radikální	radikální	k2eAgFnSc7d1	radikální
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
známou	známá	k1gFnSc7	známá
jako	jako	k9	jako
gang	gang	k1gInSc1	gang
čtyř	čtyři	k4xCgFnPc2	čtyři
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
kolem	kolem	k7c2	kolem
vdovy	vdova	k1gFnSc2	vdova
Ťiang	Ťiang	k1gMnSc1	Ťiang
Čching	Čching	k1gInSc1	Čching
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
odsouzena	odsoudit	k5eAaPmNgFnS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
dostala	dostat	k5eAaPmAgFnS	dostat
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgInPc1d1	uložen
v	v	k7c6	v
mauzoleu	mauzoleum	k1gNnSc6	mauzoleum
na	na	k7c6	na
centrálním	centrální	k2eAgNnSc6d1	centrální
náměstí	náměstí	k1gNnSc6	náměstí
Tchien-an-men	Tchiennen	k1gInSc1	Tchien-an-men
(	(	kIx(	(
<g/>
též	též	k9	též
Náměstí	náměstí	k1gNnSc1	náměstí
nebeského	nebeský	k2eAgInSc2d1	nebeský
klidu	klid	k1gInSc2	klid
<g/>
)	)	kIx)	)
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
náměstí	náměstí	k1gNnSc6	náměstí
na	na	k7c6	na
bráně	brána	k1gFnSc6	brána
Nebeského	nebeský	k2eAgInSc2d1	nebeský
klidu	klid	k1gInSc2	klid
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
i	i	k9	i
obří	obří	k2eAgFnSc1d1	obří
Maova	Maova	k1gFnSc1	Maova
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
85	[number]	k4	85
%	%	kIx~	%
Číňanů	Číňan	k1gMnPc2	Číňan
Maovy	Maova	k1gFnSc2	Maova
zásluhy	zásluha	k1gFnSc2	zásluha
převažují	převažovat	k5eAaImIp3nP	převažovat
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gFnPc7	jeho
chybami	chyba	k1gFnPc7	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
náhled	náhled	k1gInSc1	náhled
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
současné	současný	k2eAgNnSc1d1	současné
vedení	vedení	k1gNnSc1	vedení
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Maoismus	maoismus	k1gInSc1	maoismus
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
skok	skok	k1gInSc1	skok
vpřed	vpřed	k6eAd1	vpřed
</s>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
revoluce	revoluce	k1gFnSc1	revoluce
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mao	Mao	k1gFnSc2	Mao
Ce-tung	Ceung	k1gInSc1	Ce-tung
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Mao	Mao	k1gMnPc2	Mao
Ce-tung	Ceunga	k1gFnPc2	Ce-tunga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Mao	Mao	k1gFnSc2	Mao
Ce-tung	Ceunga	k1gFnPc2	Ce-tunga
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Mao	Mao	k1gFnSc1	Mao
Ce-tungSeznam	CeungSeznam	k1gInSc4	Ce-tungSeznam
maoistických	maoistický	k2eAgFnPc2d1	maoistická
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
EN	EN	kA	EN
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
komunismus	komunismus	k1gInSc1	komunismus
(	(	kIx(	(
<g/>
CZ	CZ	kA	CZ
<g/>
/	/	kIx~	/
<g/>
EN	EN	kA	EN
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Předseda	předseda	k1gMnSc1	předseda
Mao	Mao	k1gMnSc1	Mao
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
/	/	kIx~	/
<g/>
EN	EN	kA	EN
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Předměty	předmět	k1gInPc1	předmět
z	z	k7c2	z
období	období	k1gNnSc2	období
kulturní	kulturní	k2eAgFnSc2d1	kulturní
revoluce	revoluce	k1gFnSc2	revoluce
na	na	k7c4	na
culturegems	culturegems	k1gInSc4	culturegems
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
EN	EN	kA	EN
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Maova	Maova	k1gFnSc1	Maova
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
EN	EN	kA	EN
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
-	-	kIx~	-
čínské	čínský	k2eAgInPc1d1	čínský
plakáty	plakát	k1gInPc1	plakát
Maa	Maa	k1gFnSc2	Maa
</s>
</p>
<p>
<s>
Rozhovory	rozhovor	k1gInPc1	rozhovor
o	o	k7c6	o
Maovi	Maoev	k1gFnSc6	Maoev
na	na	k7c4	na
BBC	BBC	kA	BBC
-	-	kIx~	-
中	中	k?	中
<g/>
(	(	kIx(	(
<g/>
上	上	k?	上
<g/>
)	)	kIx)	)
a	a	k8xC	a
中	中	k?	中
<g/>
(	(	kIx(	(
<g/>
下	下	k?	下
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
,	,	kIx,	,
zjednoduš	zjednodušit	k5eAaPmRp2nS	zjednodušit
<g/>
.	.	kIx.	.
zápis	zápis	k1gInSc1	zápis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začala	začít	k5eAaPmAgFnS	začít
čínská	čínský	k2eAgFnSc1d1	čínská
kulturní	kulturní	k2eAgFnSc1d1	kulturní
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
pořad	pořad	k1gInSc1	pořad
Slavné	slavný	k2eAgFnSc2d1	slavná
dny	dna	k1gFnSc2	dna
na	na	k7c4	na
Stream	Stream	k1gInSc4	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
