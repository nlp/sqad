<s>
František	František	k1gMnSc1	František
Kupka	Kupka	k1gMnSc1	Kupka
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
Opočno	Opočno	k1gNnSc1	Opočno
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Puteaux	Puteaux	k1gInSc1	Puteaux
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
moderního	moderní	k2eAgNnSc2d1	moderní
abstraktního	abstraktní	k2eAgNnSc2d1	abstraktní
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
a	a	k8xC	a
realizaci	realizace	k1gFnSc3	realizace
dalšího	další	k2eAgInSc2d1	další
nového	nový	k2eAgInSc2d1	nový
výtvarného	výtvarný	k2eAgInSc2d1	výtvarný
směru	směr	k1gInSc2	směr
-	-	kIx~	-
orfismu	orfismus	k1gInSc2	orfismus
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
přibližujícímu	přibližující	k2eAgNnSc3d1	přibližující
malířství	malířství	k1gNnSc3	malířství
jiným	jiný	k2eAgNnSc7d1	jiné
uměním	umění	k1gNnSc7	umění
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
poezie	poezie	k1gFnSc1	poezie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
života	život	k1gInSc2	život
žil	žít	k5eAaImAgInS	žít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Kupka	Kupka	k1gMnSc1	Kupka
byl	být	k5eAaImAgMnS	být
též	též	k9	též
i	i	k9	i
grafikem	grafikon	k1gNnSc7	grafikon
-	-	kIx~	-
ilustrátorem	ilustrátor	k1gMnSc7	ilustrátor
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
se	se	k3xPyFc4	se
nesmazatelně	smazatelně	k6eNd1	smazatelně
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
české	český	k2eAgFnSc2d1	Česká
i	i	k8xC	i
světové	světový	k2eAgFnSc2d1	světová
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Kupka	Kupka	k1gMnSc1	Kupka
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
pěti	pět	k4xCc2	pět
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
notářského	notářský	k2eAgMnSc2d1	notářský
úředníka	úředník	k1gMnSc2	úředník
v	v	k7c6	v
Opočně	Opočno	k1gNnSc6	Opočno
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
mládí	mládí	k1gNnSc4	mládí
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
Dobrušce	Dobruška	k1gFnSc6	Dobruška
pod	pod	k7c7	pod
Orlickými	orlický	k2eAgFnPc7d1	Orlická
horami	hora	k1gFnPc7	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
sedlářskému	sedlářský	k2eAgNnSc3d1	sedlářské
řemeslu	řemeslo	k1gNnSc3	řemeslo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
maloval	malovat	k5eAaImAgMnS	malovat
vývěsní	vývěsní	k2eAgInPc4d1	vývěsní
štíty	štít	k1gInPc4	štít
a	a	k8xC	a
obrázky	obrázek	k1gInPc4	obrázek
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Projevoval	projevovat	k5eAaImAgInS	projevovat
velký	velký	k2eAgInSc1d1	velký
malířský	malířský	k2eAgInSc1d1	malířský
talent	talent	k1gInSc1	talent
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
rodinného	rodinný	k2eAgMnSc2d1	rodinný
přítele	přítel	k1gMnSc2	přítel
nebyl	být	k5eNaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
věnovat	věnovat	k5eAaImF	věnovat
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgNnSc3	svůj
řemeslu	řemeslo	k1gNnSc3	řemeslo
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
řemeslnické	řemeslnický	k2eAgFnSc2d1	řemeslnická
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Jaroměři	Jaroměř	k1gFnSc6	Jaroměř
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
Akademii	akademie	k1gFnSc4	akademie
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
studoval	studovat	k5eAaImAgMnS	studovat
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
pro	pro	k7c4	pro
historickou	historický	k2eAgFnSc4d1	historická
a	a	k8xC	a
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
malbu	malba	k1gFnSc4	malba
jako	jako	k8xC	jako
žák	žák	k1gMnSc1	žák
profesora	profesor	k1gMnSc2	profesor
Sequense	Sequens	k1gMnSc2	Sequens
až	až	k9	až
do	do	k7c2	do
absolutoria	absolutorium	k1gNnSc2	absolutorium
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
jako	jako	k8xS	jako
stipendista	stipendista	k1gMnSc1	stipendista
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
cestoval	cestovat	k5eAaImAgMnS	cestovat
oklikou	oklika	k1gFnSc7	oklika
přes	přes	k7c4	přes
severní	severní	k2eAgFnSc4d1	severní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
École	Écol	k1gInSc6	Écol
des	des	k1gNnSc2	des
Beaux-Arts	Beaux-Arts	k1gInSc1	Beaux-Arts
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgInS	živit
malováním	malování	k1gNnSc7	malování
plakátů	plakát	k1gInPc2	plakát
<g/>
,	,	kIx,	,
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
náboženství	náboženství	k1gNnSc4	náboženství
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
spiritistické	spiritistický	k2eAgNnSc4d1	spiritistické
médium	médium	k1gNnSc4	médium
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
bez	bez	k7c2	bez
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kupka	Kupka	k1gMnSc1	Kupka
byl	být	k5eAaImAgMnS	být
příznivcem	příznivec	k1gMnSc7	příznivec
teosofie	teosofie	k1gFnSc2	teosofie
<g/>
,	,	kIx,	,
východních	východní	k2eAgFnPc2d1	východní
filosofií	filosofie	k1gFnPc2	filosofie
a	a	k8xC	a
mysticismu	mysticismus	k1gInSc2	mysticismus
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
mezinárodně	mezinárodně	k6eAd1	mezinárodně
proslavil	proslavit	k5eAaPmAgMnS	proslavit
publikací	publikace	k1gFnPc2	publikace
svých	svůj	k3xOyFgFnPc2	svůj
satirických	satirický	k2eAgFnPc2d1	satirická
kreseb	kresba	k1gFnPc2	kresba
v	v	k7c6	v
pařížském	pařížský	k2eAgInSc6d1	pařížský
časopisu	časopis	k1gInSc6	časopis
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Assiette	Assiett	k1gInSc5	Assiett
au	au	k0	au
Beurre	Beurr	k1gMnSc5	Beurr
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
přispíval	přispívat	k5eAaImAgMnS	přispívat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1901	[number]	k4	1901
<g/>
-	-	kIx~	-
<g/>
1907	[number]	k4	1907
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
i	i	k9	i
tři	tři	k4xCgInPc4	tři
svá	svůj	k3xOyFgNnPc4	svůj
samostatná	samostatný	k2eAgNnPc4d1	samostatné
čísla	číslo	k1gNnPc4	číslo
-	-	kIx~	-
alba	alba	k1gFnSc1	alba
Peníze	peníz	k1gInSc2	peníz
<g/>
,	,	kIx,	,
Náboženství	náboženství	k1gNnSc4	náboženství
a	a	k8xC	a
Mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
odešel	odejít	k5eAaPmAgMnS	odejít
dobrovolně	dobrovolně	k6eAd1	dobrovolně
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
roty	rota	k1gFnSc2	rota
Nazdar	nazdar	k0	nazdar
<g/>
,	,	kIx,	,
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Aisne	Aisn	k1gInSc5	Aisn
byl	být	k5eAaImAgMnS	být
raněn	ranit	k5eAaPmNgMnS	ranit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
Řádem	řád	k1gInSc7	řád
důstojníka	důstojník	k1gMnSc4	důstojník
Čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
a	a	k8xC	a
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
zásluhy	zásluha	k1gFnPc4	zásluha
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
získal	získat	k5eAaPmAgMnS	získat
hodnost	hodnost	k1gFnSc4	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
organizovat	organizovat	k5eAaBmF	organizovat
československé	československý	k2eAgFnSc2d1	Československá
legie	legie	k1gFnSc2	legie
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
založil	založit	k5eAaPmAgMnS	založit
tzv.	tzv.	kA	tzv.
Českou	český	k2eAgFnSc4d1	Česká
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
pražské	pražský	k2eAgFnSc2d1	Pražská
Akademie	akademie	k1gFnSc2	akademie
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
také	také	k9	také
přednášel	přednášet	k5eAaImAgMnS	přednášet
<g/>
.	.	kIx.	.
</s>
<s>
Přednáškovou	přednáškový	k2eAgFnSc4d1	přednášková
činnost	činnost	k1gFnSc4	činnost
pořádal	pořádat	k5eAaImAgInS	pořádat
později	pozdě	k6eAd2	pozdě
i	i	k9	i
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
československé	československý	k2eAgMnPc4d1	československý
stipendisty	stipendista	k1gMnPc4	stipendista
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
mnoho	mnoho	k4c4	mnoho
výstav	výstava	k1gFnPc2	výstava
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
mnohá	mnohý	k2eAgNnPc4d1	mnohé
ocenění	ocenění	k1gNnPc4	ocenění
za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1919	[number]	k4	1919
až	až	k6eAd1	až
1938	[number]	k4	1938
byl	být	k5eAaImAgMnS	být
Kupka	Kupka	k1gMnSc1	Kupka
podporován	podporovat	k5eAaImNgMnS	podporovat
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Waldesem	Waldes	k1gMnSc7	Waldes
<g/>
,	,	kIx,	,
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
pražským	pražský	k2eAgMnSc7d1	pražský
podnikatelem	podnikatel	k1gMnSc7	podnikatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kupoval	kupovat	k5eAaImAgMnS	kupovat
jeho	jeho	k3xOp3gInPc4	jeho
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
pařížském	pařížský	k2eAgNnSc6d1	pařížské
předměstí	předměstí	k1gNnSc6	předměstí
Puteaux	Puteaux	k1gInSc4	Puteaux
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1957	[number]	k4	1957
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Pere	prát	k5eAaImIp3nS	prát
Lachaise	Lachaise	k1gFnSc1	Lachaise
<g/>
,	,	kIx,	,
v	v	k7c6	v
kolumbáriu	kolumbárium	k1gNnSc6	kolumbárium
<g/>
,	,	kIx,	,
oddělení	oddělení	k1gNnSc1	oddělení
87	[number]	k4	87
<g/>
,	,	kIx,	,
schránka	schránka	k1gFnSc1	schránka
č.	č.	k?	č.
22696	[number]	k4	22696
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
dílo	dílo	k1gNnSc1	dílo
Františka	František	k1gMnSc2	František
Kupky	Kupka	k1gMnSc2	Kupka
se	se	k3xPyFc4	se
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
konečné	konečný	k2eAgFnSc2d1	konečná
podoby	podoba	k1gFnSc2	podoba
utvářelo	utvářet	k5eAaImAgNnS	utvářet
od	od	k7c2	od
prvopočátečního	prvopočáteční	k2eAgInSc2d1	prvopočáteční
popisného	popisný	k2eAgInSc2d1	popisný
realismu	realismus	k1gInSc2	realismus
přes	přes	k7c4	přes
vlivy	vliv	k1gInPc4	vliv
doznívajících	doznívající	k2eAgFnPc2d1	doznívající
malířských	malířský	k2eAgFnPc2d1	malířská
tendencí	tendence	k1gFnPc2	tendence
a	a	k8xC	a
směrů	směr	k1gInPc2	směr
až	až	k9	až
ke	k	k7c3	k
konečnému	konečný	k2eAgNnSc3d1	konečné
vyústění	vyústění	k1gNnSc3	vyústění
do	do	k7c2	do
abstrakce	abstrakce	k1gFnSc2	abstrakce
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
Amorpha	Amorph	k1gMnSc2	Amorph
<g/>
.	.	kIx.	.
</s>
<s>
Fugue	Fugue	k1gFnSc1	Fugue
à	à	k?	à
deux	deux	k1gInSc1	deux
couleurs	couleursa	k1gFnPc2	couleursa
<g/>
,	,	kIx,	,
vystavil	vystavit	k5eAaPmAgMnS	vystavit
Kupka	Kupka	k1gMnSc1	Kupka
na	na	k7c6	na
Podzimním	podzimní	k2eAgInSc6d1	podzimní
salonu	salon	k1gInSc6	salon
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
abstraktní	abstraktní	k2eAgFnSc1d1	abstraktní
tvorba	tvorba	k1gFnSc1	tvorba
Františka	František	k1gMnSc2	František
Kupky	Kupka	k1gMnSc2	Kupka
měla	mít	k5eAaImAgFnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
specifický	specifický	k2eAgInSc4d1	specifický
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
zákonitosti	zákonitost	k1gFnPc4	zákonitost
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
zde	zde	k6eAd1	zde
o	o	k7c4	o
zpracování	zpracování	k1gNnSc4	zpracování
tématu	téma	k1gNnSc2	téma
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
byly	být	k5eAaImAgFnP	být
základem	základ	k1gInSc7	základ
jeho	jeho	k3xOp3gInPc2	jeho
nefigurativních	figurativní	k2eNgInPc2d1	nefigurativní
obrazů	obraz	k1gInPc2	obraz
realistické	realistický	k2eAgInPc4d1	realistický
podklady	podklad	k1gInPc4	podklad
<g/>
,	,	kIx,	,
vrcholné	vrcholný	k2eAgNnSc1d1	vrcholné
období	období	k1gNnSc1	období
Kupkovy	Kupkův	k2eAgFnSc2d1	Kupkova
tvorby	tvorba	k1gFnSc2	tvorba
však	však	k9	však
představuje	představovat	k5eAaImIp3nS	představovat
tzv.	tzv.	kA	tzv.
čistá	čistý	k2eAgFnSc1d1	čistá
abstrakce	abstrakce	k1gFnSc1	abstrakce
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
také	také	k9	také
sehrál	sehrát	k5eAaPmAgMnS	sehrát
Kupkův	Kupkův	k2eAgInSc4d1	Kupkův
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
k	k	k7c3	k
rozvinutí	rozvinutí	k1gNnSc3	rozvinutí
výtvarného	výtvarný	k2eAgInSc2d1	výtvarný
směru	směr	k1gInSc2	směr
-	-	kIx~	-
orfismu	orfismus	k1gInSc2	orfismus
<g/>
.	.	kIx.	.
</s>
<s>
Díla	dílo	k1gNnPc1	dílo
Františka	František	k1gMnSc2	František
Kupky	Kupka	k1gMnSc2	Kupka
byla	být	k5eAaImAgFnS	být
vystavována	vystavovat	k5eAaImNgFnS	vystavovat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
výstavách	výstava	k1gFnPc6	výstava
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgFnSc7d3	veliký
přehlídkou	přehlídka	k1gFnSc7	přehlídka
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
byla	být	k5eAaImAgFnS	být
výstava	výstava	k1gFnSc1	výstava
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
uskutečněna	uskutečnit	k5eAaPmNgFnS	uskutečnit
poslední	poslední	k2eAgFnSc1d1	poslední
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
domácí	domácí	k2eAgFnSc1d1	domácí
výstava	výstava	k1gFnSc1	výstava
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2012	[number]	k4	2012
a	a	k8xC	a
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Salmovském	Salmovský	k2eAgInSc6d1	Salmovský
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Hradčanech	Hradčany	k1gInPc6	Hradčany
uspořádána	uspořádán	k2eAgFnSc1d1	uspořádána
výstava	výstava	k1gFnSc1	výstava
Cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
Amorfě	Amorfa	k1gFnSc3	Amorfa
ukazující	ukazující	k2eAgFnSc1d1	ukazující
Kupkova	Kupkův	k2eAgFnSc1d1	Kupkova
díla	dílo	k1gNnSc2	dílo
zapůjčená	zapůjčený	k2eAgFnSc1d1	zapůjčená
od	od	k7c2	od
významných	významný	k2eAgFnPc2d1	významná
světových	světový	k2eAgFnPc2d1	světová
institucí	instituce	k1gFnPc2	instituce
i	i	k8xC	i
soukromých	soukromý	k2eAgMnPc2d1	soukromý
sběratelů	sběratel	k1gMnPc2	sběratel
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
sbírky	sbírka	k1gFnPc1	sbírka
Kupkových	Kupkových	k2eAgInPc2d1	Kupkových
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
studií	studio	k1gNnPc2	studio
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Museu	museum	k1gNnSc6	museum
Kampa	Kampa	k1gFnSc1	Kampa
a	a	k8xC	a
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Amorfa	Amorf	k1gMnSc2	Amorf
-	-	kIx~	-
Dvoubarevná	dvoubarevný	k2eAgFnSc1d1	dvoubarevná
fuga	fuga	k1gFnSc1	fuga
Studie	studie	k1gFnSc1	studie
Horká	Horká	k1gFnSc1	Horká
a	a	k8xC	a
Studená	studený	k2eAgFnSc1d1	studená
chromatika	chromatika	k1gFnSc1	chromatika
Cyklus	cyklus	k1gInSc1	cyklus
Vertikální	vertikální	k2eAgFnSc2d1	vertikální
kresby	kresba	k1gFnSc2	kresba
Cyklus	cyklus	k1gInSc1	cyklus
Pohyblivé	pohyblivý	k2eAgFnSc2d1	pohyblivá
grafiky	grafika	k1gFnSc2	grafika
Kolem	kolem	k7c2	kolem
jednoho	jeden	k4xCgInSc2	jeden
bodu	bod	k1gInSc2	bod
Cyklus	cyklus	k1gInSc1	cyklus
Katedrála	katedrála	k1gFnSc1	katedrála
Velká	velká	k1gFnSc1	velká
nahota	nahota	k1gFnSc1	nahota
</s>
