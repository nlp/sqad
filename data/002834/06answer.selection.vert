<s>
František	František	k1gMnSc1	František
Kupka	Kupka	k1gMnSc1	Kupka
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
Opočno	Opočno	k1gNnSc1	Opočno
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Puteaux	Puteaux	k1gInSc1	Puteaux
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
moderního	moderní	k2eAgNnSc2d1	moderní
abstraktního	abstraktní	k2eAgNnSc2d1	abstraktní
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
