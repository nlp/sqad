<s>
Sever	sever	k1gInSc1	sever
<g/>
,	,	kIx,	,
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
češtině	čeština	k1gFnSc6	čeština
též	též	k9	též
půlnoc	půlnoc	k1gFnSc4	půlnoc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
světových	světový	k2eAgFnPc2d1	světová
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
primární	primární	k2eAgFnSc4d1	primární
světovou	světový	k2eAgFnSc4d1	světová
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Zkratkou	zkratka	k1gFnSc7	zkratka
je	být	k5eAaImIp3nS	být
písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
N	N	kA	N
<g/>
"	"	kIx"	"
z	z	k7c2	z
německého	německý	k2eAgNnSc2d1	německé
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
Nord	Nord	k1gInSc1	Nord
<g/>
"	"	kIx"	"
či	či	k8xC	či
anglického	anglický	k2eAgMnSc2d1	anglický
"	"	kIx"	"
<g/>
North	North	k1gInSc1	North
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
sever	sever	k1gInSc1	sever
je	být	k5eAaImIp3nS	být
směr	směr	k1gInSc4	směr
k	k	k7c3	k
severnímu	severní	k2eAgNnSc3d1	severní
pólu	pólo	k1gNnSc3	pólo
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
i	i	k9	i
jiného	jiný	k2eAgNnSc2d1	jiné
rotujícího	rotující	k2eAgNnSc2d1	rotující
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
vaší	váš	k3xOp2gFnSc6	váš
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
pokud	pokud	k8xS	pokud
stojíte	stát	k5eAaImIp2nP	stát
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
otáčení	otáčení	k1gNnSc2	otáčení
<g/>
.	.	kIx.	.
</s>
<s>
Směr	směr	k1gInSc1	směr
k	k	k7c3	k
severnímu	severní	k2eAgNnSc3d1	severní
magnetickému	magnetický	k2eAgNnSc3d1	magnetické
pólu	pólo	k1gNnSc3	pólo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kam	kam	k6eAd1	kam
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
střelka	střelka	k1gFnSc1	střelka
kompasu	kompas	k1gInSc2	kompas
<g/>
.	.	kIx.	.
</s>
<s>
Odchylka	odchylka	k1gFnSc1	odchylka
mezi	mezi	k7c7	mezi
zeměpisným	zeměpisný	k2eAgInSc7d1	zeměpisný
a	a	k8xC	a
magnetickým	magnetický	k2eAgInSc7d1	magnetický
severem	sever	k1gInSc7	sever
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
magnetická	magnetický	k2eAgFnSc1d1	magnetická
deklinace	deklinace	k1gFnSc1	deklinace
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
osa	osa	k1gFnSc1	osa
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
otáčejí	otáčet	k5eAaImIp3nP	otáčet
hvězdy	hvězda	k1gFnPc4	hvězda
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Země	zem	k1gFnSc2	zem
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
národů	národ	k1gInPc2	národ
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
uznává	uznávat	k5eAaImIp3nS	uznávat
sever	sever	k1gInSc4	sever
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc4d1	hlavní
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
:	:	kIx,	:
Směr	směr	k1gInSc4	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
většinou	většinou	k6eAd1	většinou
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Globy	globus	k1gInPc1	globus
jsou	být	k5eAaImIp3nP	být
orientovány	orientovat	k5eAaBmNgInP	orientovat
severem	sever	k1gInSc7	sever
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
zkratka	zkratka	k1gFnSc1	zkratka
N	N	kA	N
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
na	na	k7c6	na
směrových	směrový	k2eAgFnPc6d1	směrová
růžicích	růžice	k1gFnPc6	růžice
označen	označit	k5eAaPmNgInS	označit
zdobněji	zdobně	k6eAd2	zdobně
nebo	nebo	k8xC	nebo
zvýrazněně	zvýrazněně	k6eAd1	zvýrazněně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
magnetický	magnetický	k2eAgInSc4d1	magnetický
sever	sever	k1gInSc4	sever
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
střelka	střelka	k1gFnSc1	střelka
kompasu	kompas	k1gInSc2	kompas
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
mnoha	mnoho	k4c7	mnoho
způsoby	způsob	k1gInPc7	způsob
Podle	podle	k7c2	podle
kompasu	kompas	k1gInSc2	kompas
či	či	k8xC	či
buzoly	buzola	k1gFnSc2	buzola
(	(	kIx(	(
<g/>
magnetický	magnetický	k2eAgInSc1d1	magnetický
sever	sever	k1gInSc1	sever
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
gyrokompasu	gyrokompasa	k1gFnSc4	gyrokompasa
(	(	kIx(	(
<g/>
opravdový	opravdový	k2eAgInSc4d1	opravdový
sever	sever	k1gInSc4	sever
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
:	:	kIx,	:
Hodinky	hodinka	k1gFnPc1	hodinka
se	se	k3xPyFc4	se
natočí	natočit	k5eAaBmIp3nP	natočit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hodinová	hodinový	k2eAgFnSc1d1	hodinová
(	(	kIx(	(
<g/>
malá	malý	k2eAgFnSc1d1	malá
<g/>
)	)	kIx)	)
ručička	ručička	k1gFnSc1	ručička
směřovala	směřovat	k5eAaImAgFnS	směřovat
na	na	k7c4	na
slunce	slunce	k1gNnSc4	slunce
(	(	kIx(	(
<g/>
Pozor	pozor	k1gInSc1	pozor
na	na	k7c4	na
letní	letní	k2eAgInSc4d1	letní
čas	čas	k1gInSc4	čas
<g/>
)	)	kIx)	)
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
úhlu	úhel	k1gInSc2	úhel
mezi	mezi	k7c7	mezi
ručičkou	ručička	k1gFnSc7	ručička
(	(	kIx(	(
<g/>
sluncem	slunce	k1gNnSc7	slunce
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvanáctkou	dvanáctka	k1gFnSc7	dvanáctka
(	(	kIx(	(
<g/>
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
jedničkou	jednička	k1gFnSc7	jednička
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
Jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
rovníku	rovník	k1gInSc2	rovník
nelze	lze	k6eNd1	lze
sever	sever	k1gInSc1	sever
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
spolehlivě	spolehlivě	k6eAd1	spolehlivě
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
staveb	stavba	k1gFnPc2	stavba
(	(	kIx(	(
<g/>
kostely	kostel	k1gInPc1	kostel
mívají	mívat	k5eAaImIp3nP	mívat
často	často	k6eAd1	často
oltář	oltář	k1gInSc4	oltář
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
<g/>
)	)	kIx)	)
Podle	podle	k7c2	podle
přírodních	přírodní	k2eAgInPc2d1	přírodní
jevů	jev	k1gInPc2	jev
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
)	)	kIx)	)
rovník	rovník	k1gInSc1	rovník
poledník	poledník	k1gInSc1	poledník
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
jih	jih	k1gInSc1	jih
východ	východ	k1gInSc1	východ
západ	západ	k1gInSc4	západ
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
sever	sever	k1gInSc4	sever
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
