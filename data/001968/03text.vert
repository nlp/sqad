<s>
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k1gNnSc1	Domingo
de	de	k?	de
Guzmán	Guzmán	k2eAgMnSc1d1	Guzmán
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k1gNnSc1	Domingo
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
města	město	k1gNnSc2	město
žije	žít	k5eAaImIp3nS	žít
okolo	okolo	k7c2	okolo
3	[number]	k4	3
850	[number]	k4	850
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k1gNnSc1	Domingo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
ostrova	ostrov	k1gInSc2	ostrov
Hispaniola	Hispaniola	k1gFnSc1	Hispaniola
omývaném	omývaný	k2eAgInSc6d1	omývaný
Karibským	karibský	k2eAgNnSc7d1	Karibské
mořem	moře	k1gNnSc7	moře
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Ozama	Ozamum	k1gNnSc2	Ozamum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1936	[number]	k4	1936
a	a	k8xC	a
1961	[number]	k4	1961
město	město	k1gNnSc1	město
neslo	nést	k5eAaImAgNnS	nést
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Cuidad	Cuidad	k1gInSc1	Cuidad
Trujillo	Trujillo	k1gNnSc1	Trujillo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odkazoval	odkazovat	k5eAaImAgMnS	odkazovat
na	na	k7c4	na
zdejšího	zdejší	k2eAgMnSc4d1	zdejší
diktátora	diktátor	k1gMnSc4	diktátor
Rafaela	Rafael	k1gMnSc4	Rafael
Leonidasa	Leonidas	k1gMnSc4	Leonidas
Trujilla	Trujill	k1gMnSc4	Trujill
Molinu	molino	k1gNnSc6	molino
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Santo	Santo	k1gNnSc4	Santo
Domingo	Domingo	k6eAd1	Domingo
druhým	druhý	k4xOgMnSc7	druhý
nejlidnatějším	lidnatý	k2eAgMnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
Karibiku	Karibik	k1gInSc2	Karibik
hned	hned	k6eAd1	hned
po	po	k7c6	po
kubánské	kubánský	k2eAgFnSc6d1	kubánská
Havaně	Havana	k1gFnSc6	Havana
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kulturní	kulturní	k2eAgNnSc4d1	kulturní
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgNnSc4d1	obchodní
i	i	k8xC	i
vzdělanostní	vzdělanostní	k2eAgNnSc4d1	vzdělanostní
centrum	centrum	k1gNnSc4	centrum
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1496	[number]	k4	1496
Bartolomějem	Bartoloměj	k1gMnSc7	Bartoloměj
Kolumbem	Kolumbus	k1gMnSc7	Kolumbus
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
břehu	břeh	k1gInSc6	břeh
ústí	ústit	k5eAaImIp3nP	ústit
řeky	řeka	k1gFnPc1	řeka
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
La	la	k1gNnSc2	la
Nueva	Nuevo	k1gNnSc2	Nuevo
Isabela	Isabela	k1gFnSc1	Isabela
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Santo	Sant	k2eAgNnSc4d1	Santo
Domingo	Domingo	k1gNnSc4	Domingo
jako	jako	k8xC	jako
pocta	pocta	k1gFnSc1	pocta
svatému	svatý	k1gMnSc3	svatý
Dominiku	Dominik	k1gMnSc3	Dominik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1502	[number]	k4	1502
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
hurikánem	hurikán	k1gInSc7	hurikán
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
nový	nový	k2eAgMnSc1d1	nový
guvernér	guvernér	k1gMnSc1	guvernér
Nicolás	Nicolás	k1gInSc1	Nicolás
de	de	k?	de
Ovando	Ovando	k6eAd1	Ovando
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
osadu	osada	k1gFnSc4	osada
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
na	na	k7c4	na
západní	západní	k2eAgInSc4d1	západní
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xC	jako
první	první	k4xOgNnSc1	první
město	město	k1gNnSc1	město
založené	založený	k2eAgInPc1d1	založený
Španěly	Španěly	k1gInPc1	Španěly
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
a	a	k8xC	a
první	první	k4xOgNnSc1	první
sídlo	sídlo	k1gNnSc1	sídlo
španělské	španělský	k2eAgFnSc2d1	španělská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
zvané	zvaný	k2eAgNnSc1d1	zvané
"	"	kIx"	"
<g/>
Zona	Zona	k1gFnSc1	Zona
colonial	colonial	k1gMnSc1	colonial
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
koloniální	koloniální	k2eAgFnSc6d1	koloniální
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
stavby	stavba	k1gFnPc1	stavba
jako	jako	k8xC	jako
nejstarší	starý	k2eAgFnSc1d3	nejstarší
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc1	pevnost
(	(	kIx(	(
<g/>
Fortaleza	Fortaleza	k1gFnSc1	Fortaleza
Ozama	Ozama	k1gFnSc1	Ozama
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
univerzita	univerzita	k1gFnSc1	univerzita
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
dům	dům	k1gInSc4	dům
Kolumbovy	Kolumbův	k2eAgFnSc2d1	Kolumbova
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
několik	několik	k4yIc4	několik
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
a	a	k8xC	a
paláců	palác	k1gInPc2	palác
ze	z	k7c2	z
začátků	začátek	k1gInPc2	začátek
evropské	evropský	k2eAgFnSc2d1	Evropská
kolonizace	kolonizace	k1gFnSc2	kolonizace
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Městu	město	k1gNnSc3	město
se	se	k3xPyFc4	se
během	během	k7c2	během
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
období	období	k1gNnSc2	období
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
"	"	kIx"	"
<g/>
Vstupní	vstupní	k2eAgFnSc1d1	vstupní
brána	brána	k1gFnSc1	brána
do	do	k7c2	do
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
výprav	výprava	k1gFnPc2	výprava
španělských	španělský	k2eAgMnPc2d1	španělský
conquistadorů	conquistador	k1gMnPc2	conquistador
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
kontinent	kontinent	k1gInSc4	kontinent
začínala	začínat	k5eAaImAgFnS	začínat
právě	právě	k9	právě
ze	z	k7c2	z
Santa	Sant	k1gMnSc2	Sant
Dominga	Doming	k1gMnSc2	Doming
<g/>
.	.	kIx.	.
</s>
<s>
Hernán	Hernán	k2eAgInSc1d1	Hernán
Cortés	Cortés	k1gInSc1	Cortés
odsud	odsud	k6eAd1	odsud
vyplul	vyplout	k5eAaPmAgInS	vyplout
do	do	k7c2	do
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dobyl	dobýt	k5eAaPmAgInS	dobýt
Aztéckou	aztécký	k2eAgFnSc4d1	aztécká
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Santa	Sant	k1gMnSc2	Sant
Dominga	Doming	k1gMnSc2	Doming
vyplul	vyplout	k5eAaPmAgInS	vyplout
i	i	k9	i
např.	např.	kA	např.
Vasco	Vasco	k1gMnSc1	Vasco
Núñ	Núñ	k1gMnSc1	Núñ
de	de	k?	de
Balboa	Balboa	k1gMnSc1	Balboa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
z	z	k7c2	z
amerického	americký	k2eAgNnSc2d1	americké
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1586	[number]	k4	1586
město	město	k1gNnSc1	město
dobyl	dobýt	k5eAaPmAgMnS	dobýt
anglický	anglický	k2eAgMnSc1d1	anglický
korzár	korzár	k1gMnSc1	korzár
Francis	Francis	k1gFnSc2	Francis
Drake	Drak	k1gFnSc2	Drak
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1795	[number]	k4	1795
a	a	k8xC	a
1822	[number]	k4	1822
město	město	k1gNnSc1	město
několikrát	několikrát	k6eAd1	několikrát
změnilo	změnit	k5eAaPmAgNnS	změnit
svého	svůj	k3xOyFgMnSc4	svůj
správce	správce	k1gMnSc4	správce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1795	[number]	k4	1795
přešlo	přejít	k5eAaPmAgNnS	přejít
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
bylo	být	k5eAaImAgNnS	být
okupováno	okupován	k2eAgNnSc1d1	okupováno
haitskými	haitský	k2eAgMnPc7d1	haitský
otroky	otrok	k1gMnPc7	otrok
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1802	[number]	k4	1802
znovu	znovu	k6eAd1	znovu
převzala	převzít	k5eAaPmAgFnS	převzít
správu	správa	k1gFnSc4	správa
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
město	město	k1gNnSc1	město
zpátky	zpátky	k6eAd1	zpátky
získalo	získat	k5eAaPmAgNnS	získat
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
2	[number]	k4	2
měsíce	měsíc	k1gInSc2	měsíc
potom	potom	k6eAd1	potom
nezávislý	závislý	k2eNgInSc1d1	nezávislý
stát	stát	k1gInSc1	stát
Haiti	Haiti	k1gNnSc2	Haiti
ji	on	k3xPp3gFnSc4	on
obsadil	obsadit	k5eAaPmAgMnS	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
na	na	k7c4	na
Haity	Hait	k1gMnPc4	Hait
osamostatnila	osamostatnit	k5eAaPmAgFnS	osamostatnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
a	a	k8xC	a
Santo	Sant	k2eAgNnSc4d1	Santo
Domingo	Domingo	k1gNnSc4	Domingo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jejím	její	k3xOp3gNnSc7	její
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k1gNnSc1	Domingo
je	být	k5eAaImIp3nS	být
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vnitrostátní	vnitrostátní	k2eAgFnSc2d1	vnitrostátní
dopravy	doprava	k1gFnSc2	doprava
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
2	[number]	k4	2
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
(	(	kIx(	(
<g/>
Las	laso	k1gNnPc2	laso
Américas	Américasa	k1gFnPc2	Américasa
International	International	k1gFnPc2	International
Airport	Airport	k1gInSc1	Airport
a	a	k8xC	a
La	la	k1gNnSc2	la
Isabela	Isabela	k1gFnSc1	Isabela
International	International	k1gFnSc1	International
Airport	Airport	k1gInSc1	Airport
<g/>
)	)	kIx)	)
Přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
Santu	Sant	k1gInSc6	Sant
Domingu	Doming	k1gInSc2	Doming
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvytíženějším	vytížený	k2eAgFnPc3d3	nejvytíženější
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
městskou	městský	k2eAgFnSc4d1	městská
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
síť	síť	k1gFnSc4	síť
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
a	a	k8xC	a
metro	metro	k1gNnSc1	metro
Bogotá	Bogotá	k1gFnSc2	Bogotá
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
Caracas	Caracas	k1gInSc1	Caracas
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
Catbalogan	Catbalogan	k1gInSc1	Catbalogan
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
Curitiba	Curitiba	k1gMnSc1	Curitiba
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
Guadalajara	Guadalajara	k1gFnSc1	Guadalajara
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
Haifa	Haifa	k1gFnSc1	Haifa
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
Havana	Havana	k1gFnSc1	Havana
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
Haifa	Haifa	k1gFnSc1	Haifa
<g/>
,	,	kIx,	,
Israel	Israel	k1gMnSc1	Israel
La	la	k1gNnSc2	la
Muela	Muelo	k1gNnSc2	Muelo
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Manaus	Manaus	k1gInSc1	Manaus
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Pontevedra	Pontevedra	k1gFnSc1	Pontevedra
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Rosario	Rosario	k1gNnSc1	Rosario
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
Quito	Quito	k1gNnSc1	Quito
<g/>
,	,	kIx,	,
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
de	de	k?	de
Tenerife	Tenerif	k1gMnSc5	Tenerif
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc4	Španělsko
Santiago	Santiago	k1gNnSc1	Santiago
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
Taipei	Taipei	k1gNnPc2	Taipei
<g/>
,	,	kIx,	,
Taiwan	Taiwana	k1gFnPc2	Taiwana
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k6eAd1	Domingo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc2	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k6eAd1	Domingo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
