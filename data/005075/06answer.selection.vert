<s>
Historie	historie	k1gFnSc1	historie
počítačového	počítačový	k2eAgInSc2d1	počítačový
softwaru	software	k1gInSc2	software
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
psát	psát	k5eAaImF	psát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
první	první	k4xOgFnSc1	první
programová	programový	k2eAgFnSc1d1	programová
chyba	chyba	k1gFnSc1	chyba
softwaru	software	k1gInSc2	software
<g/>
.	.	kIx.	.
</s>
