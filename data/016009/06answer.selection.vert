<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
vesmírná	vesmírný	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
International	International	k1gMnSc1
Space	Space	k1gFnSc2
Station	station	k1gInSc1
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
М	М	k?
<g/>
́	́	k?
<g/>
д	д	k?
к	к	k?
<g/>
́	́	k?
<g/>
ч	ч	k?
с	с	k?
<g/>
́	́	k?
<g/>
н	н	k?
<g/>
,	,	kIx,
М	М	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známější	známý	k2eAgFnSc1d2
pod	pod	k7c7
zkratkou	zkratka	k1gFnSc7
ISS	ISS	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jediná	jediný	k2eAgFnSc1d1
trvale	trvale	k6eAd1
obydlená	obydlený	k2eAgFnSc1d1
vesmírná	vesmírný	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
<g/>
.	.	kIx.
</s>