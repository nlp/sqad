<s>
Mafie	mafie	k1gFnSc1	mafie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
označovaná	označovaný	k2eAgFnSc1d1	označovaná
"	"	kIx"	"
<g/>
skutečnými	skutečný	k2eAgMnPc7d1	skutečný
pány	pan	k1gMnPc7	pan
<g/>
"	"	kIx"	"
Itálie	Itálie	k1gFnSc1	Itálie
jako	jako	k8xC	jako
Cosa	Cos	k2eAgFnSc1d1	Cosa
Nostra	Nostra	k1gFnSc1	Nostra
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
naše	náš	k3xOp1gFnSc1	náš
věc	věc	k1gFnSc1	věc
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tajná	tajný	k2eAgFnSc1d1	tajná
společnost	společnost	k1gFnSc1	společnost
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
odnož	odnož	k1gFnSc1	odnož
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
společně	společně	k6eAd1	společně
s	s	k7c7	s
vlnou	vlna	k1gFnSc7	vlna
italských	italský	k2eAgMnPc2d1	italský
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
mafie	mafie	k1gFnSc2	mafie
byli	být	k5eAaImAgMnP	být
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
mafie	mafie	k1gFnSc1	mafie
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
vrcholila	vrcholit	k5eAaImAgFnS	vrcholit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
FBI	FBI	kA	FBI
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
její	její	k3xOp3gInSc4	její
vliv	vliv	k1gInSc4	vliv
nesnížilo	snížit	k5eNaPmAgNnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
její	její	k3xOp3gNnSc4	její
oslabení	oslabení	k1gNnSc4	oslabení
se	se	k3xPyFc4	se
také	také	k9	také
stará	starat	k5eAaImIp3nS	starat
Interpol	interpol	k1gInSc1	interpol
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
úpadku	úpadek	k1gInSc3	úpadek
se	se	k3xPyFc4	se
mafie	mafie	k1gFnSc1	mafie
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
reputace	reputace	k1gFnSc1	reputace
zakořenila	zakořenit	k5eAaPmAgFnS	zakořenit
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
seriálech	seriál	k1gInPc6	seriál
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
reklamě	reklama	k1gFnSc3	reklama
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
úpadku	úpadek	k1gInSc6	úpadek
zůstala	zůstat	k5eAaPmAgFnS	zůstat
mafie	mafie	k1gFnSc1	mafie
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejmocnějších	mocný	k2eAgFnPc2d3	nejmocnější
kriminálních	kriminální	k2eAgFnPc2d1	kriminální
organizací	organizace	k1gFnPc2	organizace
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ovládá	ovládat	k5eAaImIp3nS	ovládat
kriminální	kriminální	k2eAgNnSc1d1	kriminální
podsvětí	podsvětí	k1gNnSc1	podsvětí
především	především	k6eAd1	především
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnSc1	mafie
také	také	k9	také
nadále	nadále	k6eAd1	nadále
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
"	"	kIx"	"
<g/>
domovské	domovský	k2eAgFnSc6d1	domovská
<g/>
"	"	kIx"	"
vlasti	vlast	k1gFnSc6	vlast
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
forem	forma	k1gFnPc2	forma
zločinu	zločin	k1gInSc2	zločin
se	se	k3xPyFc4	se
mafie	mafie	k1gFnSc1	mafie
liší	lišit	k5eAaImIp3nS	lišit
nejen	nejen	k6eAd1	nejen
svou	svůj	k3xOyFgFnSc7	svůj
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
infiltrací	infiltrace	k1gFnSc7	infiltrace
do	do	k7c2	do
státu	stát	k1gInSc2	stát
–	–	k?	–
mafie	mafie	k1gFnSc1	mafie
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgMnPc4	svůj
členy	člen	k1gMnPc4	člen
nebo	nebo	k8xC	nebo
lidi	člověk	k1gMnPc4	člověk
jí	jíst	k5eAaImIp3nS	jíst
nakloněné	nakloněný	k2eAgNnSc1d1	nakloněné
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
státních	státní	k2eAgMnPc2d1	státní
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
soudců	soudce	k1gMnPc2	soudce
a	a	k8xC	a
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
daří	dařit	k5eAaImIp3nS	dařit
nejen	nejen	k6eAd1	nejen
prosazovat	prosazovat	k5eAaImF	prosazovat
své	svůj	k3xOyFgInPc4	svůj
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
krýt	krýt	k5eAaImF	krýt
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
aktivity	aktivita	k1gFnPc4	aktivita
mafie	mafie	k1gFnSc2	mafie
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
drogami	droga	k1gFnPc7	droga
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
za	za	k7c2	za
prohibice	prohibice	k1gFnSc2	prohibice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řízená	řízený	k2eAgFnSc1d1	řízená
prostituce	prostituce	k1gFnSc1	prostituce
a	a	k8xC	a
kontrola	kontrola	k1gFnSc1	kontrola
pornoprůmyslu	pornoprůmysl	k1gInSc2	pornoprůmysl
<g/>
,	,	kIx,	,
hazardní	hazardní	k2eAgFnSc2d1	hazardní
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
zejm.	zejm.	k?	zejm.
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
)	)	kIx)	)
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
mafie	mafie	k1gFnSc2	mafie
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
pramenů	pramen	k1gInPc2	pramen
ze	z	k7c2	z
starého	starý	k2eAgNnSc2d1	staré
sicilského	sicilský	k2eAgNnSc2d1	sicilské
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
mafiusu	mafius	k1gInSc2	mafius
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
má	mít	k5eAaImIp3nS	mít
kořeny	kořen	k1gInPc1	kořen
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
mahjas	mahjas	k1gInSc1	mahjas
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgInS	být
zde	zde	k6eAd1	zde
dříve	dříve	k6eAd2	dříve
Sicilský	sicilský	k2eAgInSc1d1	sicilský
emirát	emirát	k1gInSc1	emirát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
agresivní	agresivní	k2eAgMnSc1d1	agresivní
<g/>
,	,	kIx,	,
chvástavý	chvástavý	k2eAgInSc1d1	chvástavý
<g/>
,	,	kIx,	,
vychloubavý	vychloubavý	k2eAgInSc1d1	vychloubavý
<g/>
.	.	kIx.	.
</s>
<s>
Doslovně	doslovně	k6eAd1	doslovně
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
naparovat	naparovat	k5eAaImF	naparovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
také	také	k9	také
znamenat	znamenat	k5eAaImF	znamenat
opovážlivost	opovážlivost	k1gFnSc4	opovážlivost
nebo	nebo	k8xC	nebo
odvaha	odvaha	k1gFnSc1	odvaha
<g/>
.	.	kIx.	.
</s>
<s>
Mohlo	moct	k5eAaImAgNnS	moct
se	se	k3xPyFc4	se
také	také	k9	také
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
původní	původní	k2eAgNnSc4d1	původní
slovo	slovo	k1gNnSc4	slovo
maffia	maffium	k1gNnSc2	maffium
nebo	nebo	k8xC	nebo
mafia	mafium	k1gNnSc2	mafium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
dialektu	dialekt	k1gInSc6	dialekt
znamenalo	znamenat	k5eAaImAgNnS	znamenat
chudoba	chudoba	k1gFnSc1	chudoba
<g/>
,	,	kIx,	,
bída	bída	k1gFnSc1	bída
<g/>
,	,	kIx,	,
nouze	nouze	k1gFnSc1	nouze
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
toskánštině	toskánština	k1gFnSc6	toskánština
existovalo	existovat	k5eAaImAgNnS	existovat
slovo	slovo	k1gNnSc1	slovo
malfusso	malfussa	k1gFnSc5	malfussa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
nevěřící	věřící	k2eNgNnSc1d1	nevěřící
nebo	nebo	k8xC	nebo
zločinec	zločinec	k1gMnSc1	zločinec
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
zdroje	zdroj	k1gInPc1	zdroj
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
ma	ma	k?	ma
fia	fia	k?	fia
znamenajících	znamenající	k2eAgInPc2d1	znamenající
moje	můj	k3xOp1gFnSc1	můj
dcera	dcera	k1gFnSc1	dcera
jež	jenž	k3xRgNnSc4	jenž
údajně	údajně	k6eAd1	údajně
křičela	křičet	k5eAaImAgFnS	křičet
italská	italský	k2eAgFnSc1d1	italská
žena	žena	k1gFnSc1	žena
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
jí	on	k3xPp3gFnSc3	on
znásilnili	znásilnit	k5eAaPmAgMnP	znásilnit
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
populární	populární	k2eAgInSc1d1	populární
výklad	výklad	k1gInSc1	výklad
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovo	slovo	k1gNnSc1	slovo
MAFIA	MAFIA	kA	MAFIA
je	být	k5eAaImIp3nS	být
složenina	složenina	k1gFnSc1	složenina
prvních	první	k4xOgFnPc2	první
písmen	písmeno	k1gNnPc2	písmeno
z	z	k7c2	z
věty	věta	k1gFnSc2	věta
Morte	Mort	k1gInSc5	Mort
alla	alla	k6eAd1	alla
Francia	francium	k1gNnSc2	francium
<g/>
,	,	kIx,	,
Italia	Italium	k1gNnSc2	Italium
avanti	avanti	k0	avanti
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
volně	volně	k6eAd1	volně
přeloženo	přeložen	k2eAgNnSc4d1	přeloženo
–	–	k?	–
"	"	kIx"	"
<g/>
Zabijme	zabít	k5eAaPmRp1nP	zabít
všechny	všechen	k3xTgMnPc4	všechen
Francouze	Francouz	k1gMnPc4	Francouz
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
vpřed	vpřed	k6eAd1	vpřed
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
Itálie	Itálie	k1gFnSc2	Itálie
pod	pod	k7c7	pod
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Kořeny	kořen	k1gInPc1	kořen
mafie	mafie	k1gFnSc2	mafie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
bezohlednost	bezohlednost	k1gFnSc1	bezohlednost
<g/>
,	,	kIx,	,
krvelačnost	krvelačnost	k1gFnSc1	krvelačnost
a	a	k8xC	a
ctižádostivost	ctižádostivost	k1gFnSc1	ctižádostivost
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
tajných	tajný	k2eAgNnPc6d1	tajné
bratrstvech	bratrstvo	k1gNnPc6	bratrstvo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
odpradávna	odpradávna	k6eAd1	odpradávna
působí	působit	k5eAaImIp3nP	působit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Neapole	Neapol	k1gFnSc2	Neapol
a	a	k8xC	a
Palerma	Palermo	k1gNnSc2	Palermo
<g/>
,	,	kIx,	,
starobylých	starobylý	k2eAgNnPc2d1	starobylé
měst	město	k1gNnPc2	město
u	u	k7c2	u
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
la	la	k1gNnSc7	la
Camórra	Camórr	k1gInSc2	Camórr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
pak	pak	k6eAd1	pak
ctihodná	ctihodný	k2eAgFnSc1d1	ctihodná
společnost	společnost	k1gFnSc1	společnost
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
la	la	k1gNnPc2	la
Mafia	Mafium	k1gNnSc2	Mafium
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
organizace	organizace	k1gFnPc1	organizace
našly	najít	k5eAaPmAgFnP	najít
vynikající	vynikající	k2eAgFnPc4d1	vynikající
podmínky	podmínka	k1gFnPc4	podmínka
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
právě	právě	k9	právě
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Staletí	staletí	k1gNnSc1	staletí
společenského	společenský	k2eAgInSc2d1	společenský
a	a	k8xC	a
ekonomického	ekonomický	k2eAgInSc2d1	ekonomický
vývoje	vývoj	k1gInSc2	vývoj
pod	pod	k7c7	pod
cizí	cizí	k2eAgFnSc7d1	cizí
nadvládou	nadvláda	k1gFnSc7	nadvláda
vypěstovala	vypěstovat	k5eAaPmAgFnS	vypěstovat
v	v	k7c6	v
lidech	lid	k1gInPc6	lid
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
oficiálním	oficiální	k2eAgFnPc3d1	oficiální
institucím	instituce	k1gFnPc3	instituce
-	-	kIx~	-
úřadům	úřad	k1gInPc3	úřad
<g/>
,	,	kIx,	,
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
bankám	banka	k1gFnPc3	banka
nebo	nebo	k8xC	nebo
sousedům	soused	k1gMnPc3	soused
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
státní	státní	k2eAgFnSc4d1	státní
autoritu	autorita	k1gFnSc4	autorita
pohlížel	pohlížet	k5eAaImAgMnS	pohlížet
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
člověk	člověk	k1gMnSc1	člověk
jako	jako	k8xC	jako
na	na	k7c4	na
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
největší	veliký	k2eAgFnPc4d3	veliký
a	a	k8xC	a
nejužitečnější	užitečný	k2eAgNnSc1d3	nejužitečnější
umění	umění	k1gNnSc1	umění
pokládalo	pokládat	k5eAaImAgNnS	pokládat
tohoto	tento	k3xDgMnSc4	tento
nepřítele	nepřítel	k1gMnSc4	nepřítel
přechytračit	přechytračit	k5eAaPmF	přechytračit
a	a	k8xC	a
přelstít	přelstít	k5eAaPmF	přelstít
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
dovednost	dovednost	k1gFnSc4	dovednost
<g/>
,	,	kIx,	,
zakořeněnou	zakořeněný	k2eAgFnSc4d1	zakořeněná
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
mentalitě	mentalita	k1gFnSc6	mentalita
si	se	k3xPyFc3	se
sebou	se	k3xPyFc7	se
italští	italský	k2eAgMnPc1d1	italský
vystěhovalci	vystěhovalec	k1gMnPc1	vystěhovalec
později	pozdě	k6eAd2	pozdě
přinesli	přinést	k5eAaPmAgMnP	přinést
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
la	la	k1gNnSc4	la
Camórra	Camórr	k1gMnSc2	Camórr
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
tajná	tajný	k2eAgFnSc1d1	tajná
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
jako	jako	k9	jako
název	název	k1gInSc4	název
zvolila	zvolit	k5eAaPmAgFnS	zvolit
španělské	španělský	k2eAgNnSc4d1	španělské
slovo	slovo	k1gNnSc4	slovo
camorra	camorr	k1gInSc2	camorr
<g/>
,	,	kIx,	,
značící	značící	k2eAgInSc4d1	značící
zápas	zápas	k1gInSc4	zápas
či	či	k8xC	či
půtku	půtka	k1gFnSc4	půtka
<g/>
.	.	kIx.	.
</s>
<s>
Působila	působit	k5eAaImAgFnS	působit
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
a	a	k8xC	a
jejím	její	k3xOp3gNnSc6	její
okolí	okolí	k1gNnSc6	okolí
a	a	k8xC	a
soustředila	soustředit	k5eAaPmAgFnS	soustředit
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
státní	státní	k2eAgFnSc3d1	státní
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
hrála	hrát	k5eAaImAgFnS	hrát
již	již	k9	již
camorra	camorra	k6eAd1	camorra
úlohu	úloha	k1gFnSc4	úloha
jakési	jakýsi	k3yIgFnSc2	jakýsi
stínové	stínový	k2eAgFnSc2d1	stínová
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
reagovala	reagovat	k5eAaBmAgFnS	reagovat
na	na	k7c4	na
potřeby	potřeba	k1gFnPc4	potřeba
obyvatel	obyvatel	k1gMnPc2	obyvatel
mnohem	mnohem	k6eAd1	mnohem
aktivněji	aktivně	k6eAd2	aktivně
než	než	k8xS	než
církev	církev	k1gFnSc1	církev
nebo	nebo	k8xC	nebo
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
výskyt	výskyt	k1gInSc4	výskyt
camorry	camorra	k1gFnSc2	camorra
datujeme	datovat	k5eAaImIp1nP	datovat
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1820	[number]	k4	1820
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Camorra	Camorr	k1gMnSc4	Camorr
k	k	k7c3	k
"	"	kIx"	"
<g/>
ochraně	ochrana	k1gFnSc3	ochrana
<g/>
"	"	kIx"	"
vězňů	vězeň	k1gMnPc2	vězeň
ve	v	k7c6	v
věznicích	věznice	k1gFnPc6	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgMnSc3	každý
novému	nový	k2eAgMnSc3d1	nový
vězni	vězeň	k1gMnSc3	vězeň
bylo	být	k5eAaImAgNnS	být
nabídnuto	nabídnut	k2eAgNnSc1d1	nabídnuto
členství	členství	k1gNnSc4	členství
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
<g/>
-li	i	k?	-li
přijat	přijat	k2eAgInSc1d1	přijat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pod	pod	k7c4	pod
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
spolku	spolek	k1gInSc2	spolek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zakládali	zakládat	k5eAaImAgMnP	zakládat
propuštění	propuštěný	k2eAgMnPc1d1	propuštěný
vězni	vězeň	k1gMnPc1	vězeň
"	"	kIx"	"
<g/>
pouliční	pouliční	k2eAgFnSc4d1	pouliční
Camorru	Camorra	k1gFnSc4	Camorra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
noví	nový	k2eAgMnPc1d1	nový
uchazeči	uchazeč	k1gMnPc1	uchazeč
byli	být	k5eAaImAgMnP	být
přijati	přijmout	k5eAaPmNgMnP	přijmout
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
zkoušce	zkouška	k1gFnSc6	zkouška
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
úkladná	úkladný	k2eAgFnSc1d1	úkladná
vražda	vražda	k1gFnSc1	vražda
<g/>
,	,	kIx,	,
či	či	k8xC	či
jiný	jiný	k2eAgInSc1d1	jiný
těžký	těžký	k2eAgInSc1d1	těžký
zločin	zločin	k1gInSc1	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Přijímání	přijímání	k1gNnSc1	přijímání
se	se	k3xPyFc4	se
dělo	dít	k5eAaBmAgNnS	dít
rituálním	rituální	k2eAgInSc7d1	rituální
aktem	akt	k1gInSc7	akt
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
spolku	spolek	k1gInSc2	spolek
byla	být	k5eAaImAgNnP	být
tradována	tradovat	k5eAaImNgNnP	tradovat
ústně	ústně	k6eAd1	ústně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
člena	člen	k1gMnSc4	člen
nesměl	smět	k5eNaImAgMnS	smět
být	být	k5eAaImF	být
přijat	přijmout	k5eAaPmNgMnS	přijmout
policista	policista	k1gMnSc1	policista
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
však	však	k9	však
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
špionem	špion	k1gMnSc7	špion
u	u	k7c2	u
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
organizaci	organizace	k1gFnSc4	organizace
informovat	informovat	k5eAaBmF	informovat
o	o	k7c6	o
jejích	její	k3xOp3gInPc6	její
plánech	plán	k1gInPc6	plán
<g/>
.	.	kIx.	.
</s>
<s>
Zrada	zrada	k1gFnSc1	zrada
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
trestána	trestat	k5eAaImNgFnS	trestat
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Strukturálně	strukturálně	k6eAd1	strukturálně
se	se	k3xPyFc4	se
spolek	spolek	k1gInSc1	spolek
členil	členit	k5eAaImAgInS	členit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
divize	divize	k1gFnPc4	divize
(	(	kIx(	(
<g/>
reparto	reparta	k1gFnSc5	reparta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
vicario	vicario	k6eAd1	vicario
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc7	jeho
poradními	poradní	k2eAgMnPc7d1	poradní
pracovníky	pracovník	k1gMnPc7	pracovník
byli	být	k5eAaImAgMnP	být
"	"	kIx"	"
<g/>
mistři	mistr	k1gMnPc1	mistr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
divize	divize	k1gFnSc1	divize
mělo	mít	k5eAaImAgNnS	mít
svého	svůj	k3xOyFgMnSc4	svůj
účetního	účetní	k1gMnSc4	účetní
<g/>
,	,	kIx,	,
ochránce	ochránce	k1gMnSc4	ochránce
pokladu	poklad	k1gInSc2	poklad
<g/>
,	,	kIx,	,
proviantního	proviantní	k2eAgMnSc2d1	proviantní
<g/>
,	,	kIx,	,
písaře	písař	k1gMnSc2	písař
a	a	k8xC	a
toho	ten	k3xDgMnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
oznamoval	oznamovat	k5eAaImAgMnS	oznamovat
členům	člen	k1gMnPc3	člen
místo	místo	k7c2	místo
schůzek	schůzka	k1gFnPc2	schůzka
<g/>
.	.	kIx.	.
</s>
<s>
Camorristé	Camorrista	k1gMnPc1	Camorrista
byli	být	k5eAaImAgMnP	být
vyškolení	vyškolení	k1gNnSc4	vyškolení
v	v	k7c6	v
krádežích	krádež	k1gFnPc6	krádež
<g/>
,	,	kIx,	,
loupežích	loupež	k1gFnPc6	loupež
<g/>
,	,	kIx,	,
drancování	drancování	k1gNnSc6	drancování
<g/>
,	,	kIx,	,
falešných	falešný	k2eAgFnPc6d1	falešná
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
vydírání	vydírání	k1gNnSc4	vydírání
<g/>
,	,	kIx,	,
např.	např.	kA	např.
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
postavou	postava	k1gFnSc7	postava
hierarchie	hierarchie	k1gFnSc2	hierarchie
byl	být	k5eAaImAgInS	být
Capo	capa	k1gFnSc5	capa
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
byl	být	k5eAaImAgMnS	být
jakýsi	jakýsi	k3yIgMnSc1	jakýsi
místní	místní	k2eAgMnSc1d1	místní
zástupce	zástupce	k1gMnSc1	zástupce
camorry	camorra	k1gFnSc2	camorra
<g/>
,	,	kIx,	,
vykonavatel	vykonavatel	k1gMnSc1	vykonavatel
její	její	k3xOp3gFnSc2	její
vůle	vůle	k1gFnSc2	vůle
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
zajišťoval	zajišťovat	k5eAaImAgInS	zajišťovat
obchodníkům	obchodník	k1gMnPc3	obchodník
a	a	k8xC	a
rolníkům	rolník	k1gMnPc3	rolník
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
staral	starat	k5eAaImAgMnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
splnění	splnění	k1gNnSc4	splnění
<g/>
,	,	kIx,	,
půjčoval	půjčovat	k5eAaImAgMnS	půjčovat
peníze	peníz	k1gInPc4	peníz
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odmítaly	odmítat	k5eAaImAgFnP	odmítat
banky	banka	k1gFnPc1	banka
<g/>
,	,	kIx,	,
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ochránce	ochránce	k1gMnSc1	ochránce
<g/>
"	"	kIx"	"
obchůdků	obchůdek	k1gInPc2	obchůdek
a	a	k8xC	a
hospůdek	hospůdka	k1gFnPc2	hospůdka
<g/>
,	,	kIx,	,
vybíral	vybírat	k5eAaImAgMnS	vybírat
daně	daň	k1gFnPc4	daň
z	z	k7c2	z
loterií	loterie	k1gFnPc2	loterie
a	a	k8xC	a
poplatky	poplatek	k1gInPc4	poplatek
od	od	k7c2	od
trhovců	trhovec	k1gMnPc2	trhovec
<g/>
,	,	kIx,	,
dohazoval	dohazovat	k5eAaImAgMnS	dohazovat
ženichy	ženich	k1gMnPc4	ženich
svobodným	svobodný	k2eAgFnPc3d1	svobodná
matkám	matka	k1gFnPc3	matka
<g/>
,	,	kIx,	,
trestal	trestat	k5eAaImAgMnS	trestat
znásilnění	znásilnění	k1gNnSc4	znásilnění
a	a	k8xC	a
nevěrné	věrný	k2eNgMnPc4d1	nevěrný
záletníky	záletník	k1gMnPc4	záletník
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
Camorra	Camorra	k1gFnSc1	Camorra
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
silné	silný	k2eAgFnPc1d1	silná
základny	základna	k1gFnPc1	základna
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
městech	město	k1gNnPc6	město
jako	jako	k8xC	jako
Salerno	Salerna	k1gFnSc5	Salerna
<g/>
,	,	kIx,	,
Avellino	Avellina	k1gFnSc5	Avellina
<g/>
,	,	kIx,	,
Caserta	Caserta	k1gFnSc1	Caserta
a	a	k8xC	a
Benevento	Benevento	k1gNnSc1	Benevento
<g/>
.	.	kIx.	.
</s>
<s>
Camorra	Camorra	k1gFnSc1	Camorra
je	být	k5eAaImIp3nS	být
organizovaná	organizovaný	k2eAgFnSc1d1	organizovaná
v	v	k7c6	v
klanech	klan	k1gInPc6	klan
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
menších	malý	k2eAgFnPc2d2	menší
skupin	skupina	k1gFnPc2	skupina
nebo	nebo	k8xC	nebo
frakcí	frakce	k1gFnPc2	frakce
<g/>
.	.	kIx.	.
</s>
<s>
Italské	italský	k2eAgFnPc1d1	italská
policejní	policejní	k2eAgFnPc1d1	policejní
agentury	agentura	k1gFnPc1	agentura
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Campagnia	Campagnium	k1gNnSc2	Campagnium
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
operuje	operovat	k5eAaImIp3nS	operovat
téměř	téměř	k6eAd1	téměř
6000	[number]	k4	6000
členů	člen	k1gMnPc2	člen
Camorry	Camorra	k1gFnSc2	Camorra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
byla	být	k5eAaImAgFnS	být
zjištěna	zjištěn	k2eAgFnSc1d1	zjištěna
příslušnost	příslušnost	k1gFnSc1	příslušnost
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
osob	osoba	k1gFnPc2	osoba
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
organizaci	organizace	k1gFnSc3	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnSc1	mafie
je	být	k5eAaImIp3nS	být
mohutná	mohutný	k2eAgFnSc1d1	mohutná
zločinecká	zločinecký	k2eAgFnSc1d1	zločinecká
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgFnSc1d1	podobná
neapolské	neapolský	k2eAgFnPc4d1	neapolská
camoře	camoř	k1gFnPc4	camoř
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
zájmy	zájem	k1gInPc1	zájem
se	se	k3xPyFc4	se
ale	ale	k9	ale
většinou	většinou	k6eAd1	většinou
nekřížily	křížit	k5eNaImAgFnP	křížit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mafie	mafie	k1gFnSc2	mafie
si	se	k3xPyFc3	se
za	za	k7c4	za
sféru	sféra	k1gFnSc4	sféra
vlivu	vliv	k1gInSc2	vliv
vyvolila	vyvolit	k5eAaPmAgFnS	vyvolit
Palermo	Palermo	k1gNnSc4	Palermo
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
camorry	camorra	k1gFnSc2	camorra
se	se	k3xPyFc4	se
mafie	mafie	k1gFnSc1	mafie
odlišovala	odlišovat	k5eAaImAgFnS	odlišovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakou	jaký	k3yRgFnSc7	jaký
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
v	v	k7c6	v
jakém	jaký	k3yRgInSc6	jaký
rozsahu	rozsah	k1gInSc6	rozsah
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
do	do	k7c2	do
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
camorra	camorra	k6eAd1	camorra
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
části	část	k1gFnSc6	část
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
působnost	působnost	k1gFnSc1	působnost
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
víceméně	víceméně	k9	víceméně
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
,	,	kIx,	,
mafie	mafie	k1gFnSc1	mafie
využila	využít	k5eAaPmAgFnS	využít
politického	politický	k2eAgNnSc2d1	politické
vakua	vakuum	k1gNnSc2	vakuum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
panovalo	panovat	k5eAaImAgNnS	panovat
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jí	jíst	k5eAaImIp3nS	jíst
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
osvobodil	osvobodit	k5eAaPmAgInS	osvobodit
Giuseppe	Giusepp	k1gInSc5	Giusepp
Garibaldi	Garibald	k1gMnPc1	Garibald
z	z	k7c2	z
bourbonského	bourbonský	k2eAgNnSc2d1	Bourbonské
područí	područí	k1gNnSc2	područí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
palermském	palermský	k2eAgInSc6d1	palermský
dialektu	dialekt	k1gInSc6	dialekt
označuje	označovat	k5eAaImIp3nS	označovat
Mafia	Mafia	k1gFnSc1	Mafia
hrdost	hrdost	k1gFnSc1	hrdost
<g/>
,	,	kIx,	,
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
jistotu	jistota	k1gFnSc4	jistota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dostalo	dostat	k5eAaPmAgNnS	dostat
označení	označení	k1gNnSc1	označení
Mafia	Mafia	k1gFnSc1	Mafia
příchuť	příchuť	k1gFnSc1	příchuť
něčeho	něco	k3yInSc2	něco
kriminálního	kriminální	k2eAgNnSc2d1	kriminální
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc4	slovo
úřady	úřad	k1gInPc1	úřad
používaly	používat	k5eAaImAgInP	používat
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
typ	typ	k1gInSc4	typ
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oficiální	oficiální	k2eAgFnSc6d1	oficiální
úřední	úřední	k2eAgFnSc6d1	úřední
řeči	řeč	k1gFnSc6	řeč
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
slovo	slovo	k1gNnSc1	slovo
Mafia	Mafium	k1gNnSc2	Mafium
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Přijímání	přijímání	k1gNnSc1	přijímání
do	do	k7c2	do
Mafie	mafie	k1gFnSc2	mafie
se	se	k3xPyFc4	se
dělo	dělo	k1gNnSc1	dělo
rituálním	rituální	k2eAgInSc7d1	rituální
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
bylo	být	k5eAaImAgNnS	být
prokázat	prokázat	k5eAaPmF	prokázat
nebojácnost	nebojácnost	k1gFnSc4	nebojácnost
v	v	k7c6	v
boji	boj	k1gInSc6	boj
dvou	dva	k4xCgFnPc2	dva
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Odvaha	odvaha	k1gFnSc1	odvaha
a	a	k8xC	a
odhodlanost	odhodlanost	k1gFnSc1	odhodlanost
byly	být	k5eAaImAgFnP	být
ceněny	cenit	k5eAaImNgFnP	cenit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zručnost	zručnost	k1gFnSc1	zručnost
a	a	k8xC	a
rafinovanost	rafinovanost	k1gFnSc1	rafinovanost
<g/>
.	.	kIx.	.
</s>
<s>
Chybějící	chybějící	k2eAgNnSc1d1	chybějící
vědomí	vědomí	k1gNnSc1	vědomí
státnosti	státnost	k1gFnSc2	státnost
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
vědomím	vědomí	k1gNnSc7	vědomí
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
rodině	rodina	k1gFnSc3	rodina
<g/>
,	,	kIx,	,
ke	k	k7c3	k
klanu	klan	k1gInSc3	klan
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
Mafie	mafie	k1gFnSc2	mafie
od	od	k7c2	od
poslední	poslední	k2eAgFnSc2d1	poslední
třetiny	třetina	k1gFnSc2	třetina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
loupež	loupež	k1gFnSc4	loupež
a	a	k8xC	a
krádež	krádež	k1gFnSc4	krádež
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
absolutní	absolutní	k2eAgFnSc4d1	absolutní
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
kontrolovaných	kontrolovaný	k2eAgInPc6d1	kontrolovaný
regionech	region	k1gInPc6	region
<g/>
,	,	kIx,	,
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
moci	moc	k1gFnSc6	moc
a	a	k8xC	a
jejích	její	k3xOp3gInPc6	její
zákonech	zákon	k1gInPc6	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Mlčenlivost	mlčenlivost	k1gFnSc1	mlčenlivost
(	(	kIx(	(
<g/>
omerta	omerta	k1gFnSc1	omerta
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nejen	nejen	k6eAd1	nejen
povinností	povinnost	k1gFnSc7	povinnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ctí	čest	k1gFnSc7	čest
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
vrahům	vrah	k1gMnPc3	vrah
a	a	k8xC	a
jiným	jiný	k2eAgMnPc3d1	jiný
zločincům	zločinec	k1gMnPc3	zločinec
nesmí	smět	k5eNaImIp3nS	smět
vypovídat	vypovídat	k5eAaPmF	vypovídat
ani	ani	k8xC	ani
poškození	poškození	k1gNnSc1	poškození
či	či	k8xC	či
jiní	jiný	k2eAgMnPc1d1	jiný
očití	očitý	k2eAgMnPc1d1	očitý
svědkové	svědek	k1gMnPc1	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ovládala	ovládat	k5eAaImAgFnS	ovládat
mafie	mafie	k1gFnPc4	mafie
celý	celý	k2eAgInSc1d1	celý
západ	západ	k1gInSc1	západ
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Podléhaly	podléhat	k5eAaImAgFnP	podléhat
jí	on	k3xPp3gFnSc2	on
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
dosazovala	dosazovat	k5eAaImAgFnS	dosazovat
vedení	vedení	k1gNnSc4	vedení
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
rozhodovala	rozhodovat	k5eAaImAgFnS	rozhodovat
o	o	k7c6	o
hospodaření	hospodaření	k1gNnSc6	hospodaření
statků	statek	k1gInPc2	statek
<g/>
.	.	kIx.	.
</s>
<s>
Příkazy	příkaz	k1gInPc1	příkaz
mafie	mafie	k1gFnSc2	mafie
se	se	k3xPyFc4	se
rovnaly	rovnat	k5eAaImAgInP	rovnat
zákonu	zákon	k1gInSc3	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnPc1d1	sociální
podmínky	podmínka	k1gFnPc1	podmínka
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Sicílie	Sicílie	k1gFnSc2	Sicílie
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
takový	takový	k3xDgInSc4	takový
vývoj	vývoj	k1gInSc4	vývoj
velmi	velmi	k6eAd1	velmi
příznivé	příznivý	k2eAgFnPc1d1	příznivá
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnSc1	mafie
splňovala	splňovat	k5eAaImAgFnS	splňovat
představu	představa	k1gFnSc4	představa
lidí	člověk	k1gMnPc2	člověk
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
má	mít	k5eAaImIp3nS	mít
fungovat	fungovat	k5eAaImF	fungovat
státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
<g/>
.	.	kIx.	.
</s>
<s>
Zabezpečovala	zabezpečovat	k5eAaImAgFnS	zabezpečovat
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vláda	vláda	k1gFnSc1	vláda
nemohla	moct	k5eNaImAgFnS	moct
nebo	nebo	k8xC	nebo
nechtěla	chtít	k5eNaImAgFnS	chtít
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
<g/>
.	.	kIx.	.
</s>
<s>
Mentalita	mentalita	k1gFnSc1	mentalita
typického	typický	k2eAgMnSc2d1	typický
Sicilana	Sicilan	k1gMnSc2	Sicilan
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
takovému	takový	k3xDgInSc3	takový
typu	typ	k1gInSc3	typ
zřízení	zřízení	k1gNnSc2	zřízení
historicky	historicky	k6eAd1	historicky
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnSc1	mafie
nabízela	nabízet	k5eAaImAgFnS	nabízet
přesně	přesně	k6eAd1	přesně
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
prostý	prostý	k2eAgMnSc1d1	prostý
člověk	člověk	k1gMnSc1	člověk
hledal	hledat	k5eAaImAgMnS	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
sicilská	sicilský	k2eAgFnSc1d1	sicilská
mafie	mafie	k1gFnSc1	mafie
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Palermu	Palermo	k1gNnSc6	Palermo
a	a	k8xC	a
její	její	k3xOp3gNnPc1	její
další	další	k2eAgNnPc1d1	další
silná	silný	k2eAgNnPc1d1	silné
místa	místo	k1gNnPc1	místo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
sicilských	sicilský	k2eAgFnPc6d1	sicilská
provinciích	provincie	k1gFnPc6	provincie
Trapani	Trapan	k1gMnPc1	Trapan
a	a	k8xC	a
Agrigento	Agrigento	k1gNnSc1	Agrigento
<g/>
.	.	kIx.	.
</s>
<s>
Sicilské	sicilský	k2eAgFnPc1d1	sicilská
mafiánské	mafiánský	k2eAgFnPc1d1	mafiánská
rodiny	rodina	k1gFnPc1	rodina
jsou	být	k5eAaImIp3nP	být
seskupeny	seskupit	k5eAaPmNgFnP	seskupit
v	v	k7c6	v
okresech	okres	k1gInPc6	okres
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
kontrolovány	kontrolovat	k5eAaImNgInP	kontrolovat
provinciálními	provinciální	k2eAgFnPc7d1	provinciální
komisemi	komise	k1gFnPc7	komise
<g/>
.	.	kIx.	.
</s>
<s>
Meziprovinciální	Meziprovinciální	k2eAgFnSc1d1	Meziprovinciální
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
představitelů	představitel	k1gMnPc2	představitel
každé	každý	k3xTgFnSc2	každý
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
Palermu	Palermo	k1gNnSc6	Palermo
a	a	k8xC	a
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
jak	jak	k6eAd1	jak
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
domácí	domácí	k2eAgFnSc3d1	domácí
činnosti	činnost	k1gFnSc3	činnost
sicilské	sicilský	k2eAgFnSc2d1	sicilská
mafie	mafie	k1gFnSc2	mafie
<g/>
.	.	kIx.	.
</s>
<s>
Italské	italský	k2eAgInPc1d1	italský
úřady	úřad	k1gInPc1	úřad
odhadují	odhadovat	k5eAaImIp3nP	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
asi	asi	k9	asi
5000	[number]	k4	5000
členů	člen	k1gMnPc2	člen
sicilské	sicilský	k2eAgFnSc2d1	sicilská
mafie	mafie	k1gFnSc2	mafie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
až	až	k9	až
na	na	k7c4	na
3000	[number]	k4	3000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
sicilskou	sicilský	k2eAgFnSc4d1	sicilská
mafii	mafie	k1gFnSc4	mafie
napojeni	napojen	k2eAgMnPc1d1	napojen
<g/>
.	.	kIx.	.
</s>
<s>
Italsko-americká	italskomerický	k2eAgFnSc1d1	italsko-americká
mafie	mafie	k1gFnSc1	mafie
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
jako	jako	k8xC	jako
Cosa	Cos	k2eAgFnSc1d1	Cosa
Nostra	Nostra	k1gFnSc1	Nostra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejmocnější	mocný	k2eAgFnSc1d3	nejmocnější
zločinecká	zločinecký	k2eAgFnSc1d1	zločinecká
organizace	organizace	k1gFnSc1	organizace
na	na	k7c6	na
území	území	k1gNnSc6	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
základ	základ	k1gInSc4	základ
položili	položit	k5eAaPmAgMnP	položit
italští	italský	k2eAgMnPc1d1	italský
imigranti	imigrant	k1gMnPc1	imigrant
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
velké	velký	k2eAgFnPc1d1	velká
imigrační	imigrační	k2eAgFnPc1d1	imigrační
vlny	vlna	k1gFnPc1	vlna
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
změnily	změnit	k5eAaPmAgFnP	změnit
demografické	demografický	k2eAgNnSc4d1	demografické
složení	složení	k1gNnSc4	složení
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Italové	Ital	k1gMnPc1	Ital
se	se	k3xPyFc4	se
usazovali	usazovat	k5eAaImAgMnP	usazovat
zejména	zejména	k9	zejména
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
,	,	kIx,	,
Detroit	Detroit	k1gInSc1	Detroit
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dalších	další	k1gNnPc6	další
<g/>
.	.	kIx.	.
</s>
<s>
Chudinské	chudinský	k2eAgNnSc1d1	chudinské
podhoubí	podhoubí	k1gNnSc1	podhoubí
těchto	tento	k3xDgNnPc2	tento
velkoměst	velkoměsto	k1gNnPc2	velkoměsto
utvořilo	utvořit	k5eAaPmAgNnS	utvořit
ideální	ideální	k2eAgNnSc1d1	ideální
prostředí	prostředí	k1gNnSc1	prostředí
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
napomohl	napomoct	k5eAaPmAgMnS	napomoct
i	i	k8xC	i
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
imigranty	imigrant	k1gMnPc7	imigrant
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k6eAd1	mnoho
původních	původní	k2eAgNnPc2d1	původní
sicilských	sicilský	k2eAgNnPc2d1	sicilské
<g/>
,	,	kIx,	,
či	či	k8xC	či
italských	italský	k2eAgMnPc2d1	italský
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnPc4	svůj
kriminální	kriminální	k2eAgFnPc4d1	kriminální
zvyklosti	zvyklost	k1gFnPc4	zvyklost
přivezli	přivézt	k5eAaPmAgMnP	přivézt
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
několika	několik	k4yIc2	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
,	,	kIx,	,
italsko-americká	italskomerický	k2eAgFnSc1d1	italsko-americká
mafie	mafie	k1gFnSc1	mafie
absolutně	absolutně	k6eAd1	absolutně
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
organizovaný	organizovaný	k2eAgInSc4d1	organizovaný
zločin	zločin	k1gInSc4	zločin
na	na	k7c6	na
území	území	k1gNnSc6	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
hazard	hazard	k1gInSc4	hazard
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
,	,	kIx,	,
prostituci	prostituce	k1gFnSc4	prostituce
<g/>
,	,	kIx,	,
loterie	loterie	k1gFnPc4	loterie
<g/>
,	,	kIx,	,
pronikla	proniknout	k5eAaPmAgFnS	proniknout
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
odborů	odbor	k1gInPc2	odbor
<g/>
,	,	kIx,	,
obchodů	obchod	k1gInPc2	obchod
s	s	k7c7	s
narkotiky	narkotikon	k1gNnPc7	narkotikon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
moci	moc	k1gFnSc2	moc
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
byla	být	k5eAaImAgFnS	být
Sicílie	Sicílie	k1gFnSc1	Sicílie
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocený	sjednocený	k2eAgInSc1d1	sjednocený
právní	právní	k2eAgInSc1d1	právní
řád	řád	k1gInSc1	řád
již	již	k9	již
neumožňoval	umožňovat	k5eNaImAgInS	umožňovat
tradiční	tradiční	k2eAgNnSc4d1	tradiční
aristokratické	aristokratický	k2eAgNnSc4d1	aristokratické
vynucování	vynucování	k1gNnSc4	vynucování
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zhoršující	zhoršující	k2eAgFnSc3d1	zhoršující
se	se	k3xPyFc4	se
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
situaci	situace	k1gFnSc3	situace
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
Sicilanů	Sicilan	k1gMnPc2	Sicilan
odstěhovalo	odstěhovat	k5eAaPmAgNnS	odstěhovat
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
zločinecké	zločinecký	k2eAgFnSc6d1	zločinecká
organizaci	organizace	k1gFnSc6	organizace
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
protokolu	protokol	k1gInSc6	protokol
soudní	soudní	k2eAgFnSc2d1	soudní
poroty	porota	k1gFnSc2	porota
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleans	Orleans	k1gInSc4	Orleans
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
projednávání	projednávání	k1gNnSc6	projednávání
případu	případ	k1gInSc2	případ
jistého	jistý	k2eAgMnSc2d1	jistý
Vincenza	Vincenz	k1gMnSc2	Vincenz
Ottunwa	Ottunwa	k1gMnSc1	Ottunwa
zapisovatel	zapisovatel	k1gMnSc1	zapisovatel
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
tuto	tento	k3xDgFnSc4	tento
větu	věta	k1gFnSc4	věta
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mafie	mafie	k1gFnSc1	mafie
je	být	k5eAaImIp3nS	být
tajná	tajný	k2eAgFnSc1d1	tajná
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
poskvrnila	poskvrnit	k5eAaPmAgFnS	poskvrnit
nejhanebnějšími	hanebný	k2eAgInPc7d3	nejhanebnější
zločiny	zločin	k1gInPc7	zločin
a	a	k8xC	a
vždycky	vždycky	k6eAd1	vždycky
dokázala	dokázat	k5eAaPmAgFnS	dokázat
umlčet	umlčet	k5eAaPmF	umlčet
svědky	svědek	k1gMnPc4	svědek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
přicházelo	přicházet	k5eAaImAgNnS	přicházet
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
zejm.	zejm.	k?	zejm.
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
italských	italský	k2eAgMnPc2d1	italský
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
výrazněji	výrazně	k6eAd2	výrazně
zasaženo	zasáhnout	k5eAaPmNgNnS	zasáhnout
italsko-americkým	italskomerický	k2eAgInSc7d1	italsko-americký
organizovaným	organizovaný	k2eAgInSc7d1	organizovaný
zločinem	zločin	k1gInSc7	zločin
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
zmiňovaný	zmiňovaný	k2eAgMnSc1d1	zmiňovaný
New	New	k1gMnSc1	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnSc1	mafie
se	se	k3xPyFc4	se
v	v	k7c4	v
New	New	k1gFnSc4	New
Orleans	Orleans	k1gInSc4	Orleans
zpočátku	zpočátku	k6eAd1	zpočátku
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Mano	mana	k1gFnSc5	mana
nera	r	k2eNgFnSc1d1	r
-	-	kIx~	-
Černá	černý	k2eAgFnSc1d1	černá
ruka	ruka	k1gFnSc1	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
stáli	stát	k5eAaImAgMnP	stát
bratři	bratr	k1gMnPc1	bratr
Antonio	Antonio	k1gMnSc1	Antonio
a	a	k8xC	a
Carlo	Carlo	k1gNnSc1	Carlo
Mantrangové	Mantrangový	k2eAgNnSc1d1	Mantrangový
ze	z	k7c2	z
sicilského	sicilský	k2eAgNnSc2d1	sicilské
Palerma	Palermo	k1gNnSc2	Palermo
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
byli	být	k5eAaImAgMnP	být
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
začali	začít	k5eAaPmAgMnP	začít
obchodovat	obchodovat	k5eAaImF	obchodovat
s	s	k7c7	s
ovocem	ovoce	k1gNnSc7	ovoce
a	a	k8xC	a
zeleninou	zelenina	k1gFnSc7	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
místní	místní	k2eAgInSc4d1	místní
trh	trh	k1gInSc4	trh
a	a	k8xC	a
v	v	k7c6	v
rušném	rušný	k2eAgInSc6d1	rušný
neworleanském	worleanský	k2eNgInSc6d1	neworleanský
přístavu	přístav	k1gInSc6	přístav
nevyložili	vyložit	k5eNaPmAgMnP	vyložit
dělníci	dělník	k1gMnPc1	dělník
jedinou	jediný	k2eAgFnSc4d1	jediná
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
majitel	majitel	k1gMnSc1	majitel
či	či	k8xC	či
kapitán	kapitán	k1gMnSc1	kapitán
nezaplatili	zaplatit	k5eNaPmAgMnP	zaplatit
"	"	kIx"	"
<g/>
poplatek	poplatek	k1gInSc1	poplatek
za	za	k7c4	za
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Policisté	policista	k1gMnPc1	policista
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
také	také	k9	také
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc4	tento
aktivity	aktivita	k1gFnPc4	aktivita
za	za	k7c4	za
úplatu	úplata	k1gFnSc4	úplata
tolerovali	tolerovat	k5eAaImAgMnP	tolerovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
více	hodně	k6eAd2	hodně
tajných	tajný	k2eAgNnPc2d1	tajné
společenství	společenství	k1gNnPc2	společenství
"	"	kIx"	"
<g/>
Černé	Černá	k1gFnSc2	Černá
ruky	ruka	k1gFnSc2	ruka
<g/>
"	"	kIx"	"
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
"	"	kIx"	"
<g/>
Unione	union	k1gInSc5	union
siciliano	siciliana	k1gFnSc5	siciliana
<g/>
"	"	kIx"	"
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
měst	město	k1gNnPc2	město
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
již	již	k9	již
40	[number]	k4	40
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
bankéřů	bankéř	k1gMnPc2	bankéř
<g/>
,	,	kIx,	,
státních	státní	k2eAgMnPc2d1	státní
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
kuplířů	kuplíř	k1gMnPc2	kuplíř
a	a	k8xC	a
vrahů	vrah	k1gMnPc2	vrah
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Chicagskému	chicagský	k2eAgNnSc3d1	Chicagské
podsvětí	podsvětí	k1gNnSc3	podsvětí
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgInS	vládnout
Giacomo	Giacoma	k1gFnSc5	Giacoma
Colosimo	Colosima	k1gFnSc5	Colosima
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
(	(	kIx(	(
<g/>
Big	Big	k1gFnSc1	Big
<g/>
)	)	kIx)	)
Jim	on	k3xPp3gMnPc3	on
Colosimo	Colosima	k1gFnSc5	Colosima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
se	se	k3xPyFc4	se
Jim	on	k3xPp3gMnPc3	on
Colosimo	Colosima	k1gFnSc5	Colosima
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
Vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
Victorii	Victorie	k1gFnSc4	Victorie
Moresco	Moresco	k1gNnSc4	Moresco
<g/>
,	,	kIx,	,
majitelku	majitelka	k1gFnSc4	majitelka
dvou	dva	k4xCgInPc2	dva
nevěstinců	nevěstinec	k1gInPc2	nevěstinec
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
si	se	k3xPyFc3	se
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
další	další	k2eAgInPc4d1	další
nevěstince	nevěstinec	k1gInPc4	nevěstinec
a	a	k8xC	a
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
už	už	k6eAd1	už
jich	on	k3xPp3gMnPc2	on
měli	mít	k5eAaImAgMnP	mít
desítky	desítka	k1gFnPc4	desítka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
Jim	on	k3xPp3gMnPc3	on
Colosimo	Colosima	k1gFnSc5	Colosima
oblast	oblast	k1gFnSc4	oblast
hazardních	hazardní	k2eAgFnPc2d1	hazardní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Colosimo	Colosimo	k6eAd1	Colosimo
byl	být	k5eAaImAgMnS	být
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
aktivitám	aktivita	k1gFnPc3	aktivita
velmi	velmi	k6eAd1	velmi
bohatý	bohatý	k2eAgInSc4d1	bohatý
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
řízené	řízený	k2eAgFnSc2d1	řízená
prostituce	prostituce	k1gFnSc2	prostituce
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
ženami	žena	k1gFnPc7	žena
neměl	mít	k5eNaImAgMnS	mít
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
konkurenci	konkurence	k1gFnSc4	konkurence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
ho	on	k3xPp3gMnSc4	on
začala	začít	k5eAaPmAgFnS	začít
vydírat	vydírat	k5eAaImF	vydírat
skupina	skupina	k1gFnSc1	skupina
Mano	mana	k1gFnSc5	mana
Nera	Nero	k1gMnSc2	Nero
(	(	kIx(	(
<g/>
Černá	černý	k2eAgFnSc1d1	černá
ruka	ruka	k1gFnSc1	ruka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Moresco	Moresco	k6eAd1	Moresco
proto	proto	k8xC	proto
pozvala	pozvat	k5eAaPmAgFnS	pozvat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
svého	svůj	k3xOyFgMnSc2	svůj
synovce	synovec	k1gMnSc2	synovec
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
byl	být	k5eAaImAgMnS	být
Johnny	Johnna	k1gFnSc2	Johnna
Torrio	Torrio	k6eAd1	Torrio
<g/>
.	.	kIx.	.
</s>
<s>
Torrio	Torrio	k1gMnSc1	Torrio
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
předání	předání	k1gNnSc4	předání
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
si	se	k3xPyFc3	se
tři	tři	k4xCgMnPc1	tři
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
pro	pro	k7c4	pro
peníze	peníz	k1gInPc4	peníz
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
Colosimo	Colosima	k1gFnSc5	Colosima
vzal	vzít	k5eAaPmAgMnS	vzít
poté	poté	k6eAd1	poté
Torria	Torrium	k1gNnPc1	Torrium
k	k	k7c3	k
sobě	se	k3xPyFc3	se
a	a	k8xC	a
svěřil	svěřit	k5eAaPmAgMnS	svěřit
mu	on	k3xPp3gMnSc3	on
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
svými	svůj	k3xOyFgFnPc7	svůj
záležitostmi	záležitost	k1gFnPc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Torrio	Torrio	k1gMnSc1	Torrio
postupně	postupně	k6eAd1	postupně
získal	získat	k5eAaPmAgMnS	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
kriminální	kriminální	k2eAgFnSc7d1	kriminální
říší	říš	k1gFnSc7	říš
Colosima	Colosim	k1gMnSc2	Colosim
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
nakonec	nakonec	k6eAd1	nakonec
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
začínající	začínající	k2eAgFnSc2d1	začínající
prohibice	prohibice	k1gFnSc2	prohibice
<g/>
,	,	kIx,	,
zastřelit	zastřelit	k5eAaPmF	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
Colosimovo	Colosimův	k2eAgNnSc1d1	Colosimův
negativní	negativní	k2eAgNnSc1d1	negativní
stanovisko	stanovisko	k1gNnSc1	stanovisko
na	na	k7c4	na
Torriův	Torriův	k2eAgInSc4d1	Torriův
zájem	zájem	k1gInSc4	zájem
zapojit	zapojit	k5eAaPmF	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
alkoholových	alkoholový	k2eAgFnPc2d1	alkoholová
kšeftů	kšeftů	k?	kšeftů
<g/>
.	.	kIx.	.
</s>
<s>
Torrio	Torrio	k1gMnSc1	Torrio
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
neomezeným	omezený	k2eNgMnSc7d1	neomezený
vládcem	vládce	k1gMnSc7	vládce
chicagského	chicagský	k2eAgNnSc2d1	Chicagské
podsvětí	podsvětí	k1gNnSc2	podsvětí
a	a	k8xC	a
jako	jako	k9	jako
pravou	pravý	k2eAgFnSc4d1	pravá
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
vykonavatele	vykonavatel	k1gMnPc4	vykonavatel
špinavé	špinavý	k2eAgFnSc2d1	špinavá
práce	práce	k1gFnSc2	práce
povolal	povolat	k5eAaPmAgMnS	povolat
z	z	k7c2	z
New	New	k1gMnSc2	New
Yorku	York	k1gInSc2	York
mladého	mladý	k1gMnSc2	mladý
Al	ala	k1gFnPc2	ala
Capona	Capon	k1gMnSc2	Capon
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
rostoucím	rostoucí	k2eAgMnSc6d1	rostoucí
New	New	k1gMnSc6	New
Yorku	York	k1gInSc2	York
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
kriminální	kriminální	k2eAgInSc1d1	kriminální
gang	gang	k1gInSc1	gang
"	"	kIx"	"
<g/>
Whyos	Whyos	k1gInSc1	Whyos
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
gangu	gang	k1gInSc2	gang
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
členové	člen	k1gMnPc1	člen
oddělili	oddělit	k5eAaPmAgMnP	oddělit
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
"	"	kIx"	"
<g/>
Five-Points-Gang	Five-Points-Gang	k1gInSc4	Five-Points-Gang
<g/>
"	"	kIx"	"
ke	k	k7c3	k
kterému	který	k3yRgMnSc3	který
patřil	patřit	k5eAaImAgInS	patřit
např.	např.	kA	např.
Johnny	Johnna	k1gFnSc2	Johnna
Torrio	Torrio	k6eAd1	Torrio
<g/>
,	,	kIx,	,
<g/>
Frankie	Frankie	k1gFnSc1	Frankie
Yale	Yale	k1gFnPc2	Yale
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
Capone	Capon	k1gMnSc5	Capon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Lucky	lucky	k6eAd1	lucky
Luciano	Luciana	k1gFnSc5	Luciana
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
jeho	jeho	k3xOp3gFnSc7	jeho
vážnou	vážný	k2eAgFnSc7d1	vážná
konkurencí	konkurence	k1gFnSc7	konkurence
byl	být	k5eAaImAgMnS	být
Monk	Monk	k1gMnSc1	Monk
Eastman	Eastman	k1gMnSc1	Eastman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Gang	Ganga	k1gFnPc2	Ganga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
New	New	k1gFnPc2	New
Yorku	York	k1gInSc2	York
velká	velký	k2eAgFnSc1d1	velká
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
získá	získat	k5eAaPmIp3nS	získat
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
byl	být	k5eAaImAgInS	být
nerozhodný	rozhodný	k2eNgInSc1d1	nerozhodný
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
však	však	k9	však
byla	být	k5eAaImAgFnS	být
moc	moc	k6eAd1	moc
gangu	gang	k1gInSc2	gang
Monk	Monk	k1gMnSc1	Monk
Eastman	Eastman	k1gMnSc1	Eastman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
oslabena	oslabit	k5eAaPmNgFnS	oslabit
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
gangu	gang	k1gInSc2	gang
Five	Five	k1gInSc4	Five
Points	Points	k1gInSc1	Points
Gang	gang	k1gInSc1	gang
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zařadili	zařadit	k5eAaPmAgMnP	zařadit
do	do	k7c2	do
struktur	struktura	k1gFnPc2	struktura
vznikající	vznikající	k2eAgFnSc2d1	vznikající
mafie	mafie	k1gFnSc2	mafie
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
oběťmi	oběť	k1gFnPc7	oběť
mafie	mafie	k1gFnPc1	mafie
byli	být	k5eAaImAgMnP	být
vlastní	vlastní	k2eAgMnPc1d1	vlastní
krajané	krajan	k1gMnPc1	krajan
<g/>
.	.	kIx.	.
</s>
<s>
Nečinili	činit	k5eNaImAgMnP	činit
totiž	totiž	k9	totiž
žádné	žádný	k3yNgFnPc4	žádný
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Chtěla	chtít	k5eAaImAgFnS	chtít
poplatek	poplatek	k1gInSc4	poplatek
za	za	k7c4	za
opatření	opatření	k1gNnSc4	opatření
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
za	za	k7c4	za
její	její	k3xOp3gNnSc4	její
udržení	udržení	k1gNnSc4	udržení
<g/>
,	,	kIx,	,
za	za	k7c4	za
volnou	volný	k2eAgFnSc4d1	volná
postel	postel	k1gFnSc4	postel
<g/>
,	,	kIx,	,
za	za	k7c4	za
obchodní	obchodní	k2eAgInSc4d1	obchodní
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
za	za	k7c4	za
provozování	provozování	k1gNnSc4	provozování
téměř	téměř	k6eAd1	téměř
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnSc1	mafie
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
začala	začít	k5eAaPmAgFnS	začít
vydírat	vydírat	k5eAaImF	vydírat
i	i	k9	i
starousedlíky	starousedlík	k1gMnPc4	starousedlík
a	a	k8xC	a
obchodníky	obchodník	k1gMnPc4	obchodník
kteří	který	k3yIgMnPc1	který
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
"	"	kIx"	"
<g/>
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
"	"	kIx"	"
mafie	mafie	k1gFnSc1	mafie
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
,	,	kIx,	,
našli	najít	k5eAaPmAgMnP	najít
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
prádelnu	prádelna	k1gFnSc4	prádelna
<g/>
,	,	kIx,	,
řeznictví	řeznictví	k1gNnSc4	řeznictví
či	či	k8xC	či
pekárnu	pekárna	k1gFnSc4	pekárna
bez	bez	k7c2	bez
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
příště	příště	k6eAd1	příště
jejich	jejich	k3xOp3gInPc1	jejich
obchody	obchod	k1gInPc1	obchod
lehly	lehnout	k5eAaPmAgInP	lehnout
popelem	popel	k1gInSc7	popel
<g/>
.	.	kIx.	.
</s>
<s>
Italští	italský	k2eAgMnPc1d1	italský
imigranti	imigrant	k1gMnPc1	imigrant
se	se	k3xPyFc4	se
stávali	stávat	k5eAaImAgMnP	stávat
kořistí	kořist	k1gFnSc7	kořist
mafie	mafie	k1gFnSc1	mafie
už	už	k6eAd1	už
v	v	k7c6	v
přístavech	přístav	k1gInPc6	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Podepisovali	podepisovat	k5eAaImAgMnP	podepisovat
smlouvy	smlouva	k1gFnPc4	smlouva
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
nerozuměli	rozumět	k5eNaImAgMnP	rozumět
a	a	k8xC	a
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleansu	Orleans	k1gInSc2	Orleans
nebo	nebo	k8xC	nebo
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
museli	muset	k5eAaImAgMnP	muset
pracovat	pracovat	k5eAaImF	pracovat
mnoho	mnoho	k4c4	mnoho
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
samozřejmě	samozřejmě	k6eAd1	samozřejmě
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
běžných	běžný	k2eAgFnPc2d1	běžná
mezd	mzda	k1gFnPc2	mzda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
imigranti	imigrant	k1gMnPc1	imigrant
přepluli	přeplout	k5eAaPmAgMnP	přeplout
moře	moře	k1gNnSc4	moře
v	v	k7c6	v
tmavých	tmavý	k2eAgNnPc6d1	tmavé
podpalubích	podpalubí	k1gNnPc6	podpalubí
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
častěji	často	k6eAd2	často
na	na	k7c4	na
dluh	dluh	k1gInSc4	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnPc1	mafie
tyto	tento	k3xDgFnPc4	tento
nebožáky	nebožák	k1gMnPc7	nebožák
přivítala	přivítat	k5eAaPmAgFnS	přivítat
nabízenou	nabízený	k2eAgFnSc7d1	nabízená
půjčkou	půjčka	k1gFnSc7	půjčka
a	a	k8xC	a
kdekdo	kdekdo	k3yInSc1	kdekdo
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
pochopitelných	pochopitelný	k2eAgInPc2d1	pochopitelný
důvodů	důvod	k1gInPc2	důvod
rád	rád	k6eAd1	rád
přijal	přijmout	k5eAaPmAgInS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
zpoždění	zpoždění	k1gNnSc6	zpoždění
splátky	splátka	k1gFnSc2	splátka
mafiáni	mafián	k1gMnPc1	mafián
dlužníkovi	dlužníkův	k2eAgMnPc1d1	dlužníkův
zlomili	zlomit	k5eAaPmAgMnP	zlomit
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
našel	najít	k5eAaPmAgMnS	najít
odvahu	odvaha	k1gFnSc4	odvaha
a	a	k8xC	a
udal	udat	k5eAaPmAgMnS	udat
tyto	tento	k3xDgMnPc4	tento
vyděrače	vyděrač	k1gMnPc4	vyděrač
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
většinou	většinou	k6eAd1	většinou
bez	bez	k7c2	bez
milosti	milost	k1gFnSc2	milost
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Policisté	policista	k1gMnPc1	policista
navíc	navíc	k6eAd1	navíc
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
uplacení	uplacení	k1gNnSc2	uplacení
a	a	k8xC	a
podobné	podobný	k2eAgFnPc1d1	podobná
stížnosti	stížnost	k1gFnPc1	stížnost
většnou	většný	k2eAgFnSc4d1	většný
smetli	smetnout	k5eAaPmAgMnP	smetnout
ze	z	k7c2	z
stolu	stol	k1gInSc2	stol
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnSc1	mafie
postupně	postupně	k6eAd1	postupně
sílila	sílit	k5eAaImAgFnS	sílit
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnSc1	mafie
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
jak	jak	k6eAd1	jak
získávala	získávat	k5eAaImAgFnS	získávat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInPc4d2	veliký
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
přecházela	přecházet	k5eAaImAgFnS	přecházet
k	k	k7c3	k
sofistikovanějším	sofistikovaný	k2eAgFnPc3d2	sofistikovanější
zločineckým	zločinecký	k2eAgFnPc3d1	zločinecká
metodám	metoda	k1gFnPc3	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
organizovala	organizovat	k5eAaBmAgFnS	organizovat
politické	politický	k2eAgInPc4d1	politický
kluby	klub	k1gInPc4	klub
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
sjednotili	sjednotit	k5eAaPmAgMnP	sjednotit
nevědomé	vědomý	k2eNgMnPc4d1	nevědomý
čističe	čistič	k1gMnPc4	čistič
bot	bota	k1gFnPc2	bota
<g/>
,	,	kIx,	,
dělníky	dělník	k1gMnPc4	dělník
<g/>
,	,	kIx,	,
kuplíře	kuplíř	k1gMnPc4	kuplíř
<g/>
,	,	kIx,	,
taxikáře	taxikář	k1gMnPc4	taxikář
a	a	k8xC	a
vystrašené	vystrašený	k2eAgMnPc4d1	vystrašený
imigranty	imigrant	k1gMnPc4	imigrant
ze	z	k7c2	z
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
hlasy	hlas	k1gInPc1	hlas
pak	pak	k6eAd1	pak
při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
rozdávali	rozdávat	k5eAaImAgMnP	rozdávat
předem	předem	k6eAd1	předem
určeným	určený	k2eAgMnPc3d1	určený
kandidátům	kandidát	k1gMnPc3	kandidát
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
k	k	k7c3	k
mafii	mafie	k1gFnSc3	mafie
i	i	k9	i
díky	díky	k7c3	díky
takto	takto	k6eAd1	takto
získaným	získaný	k2eAgInPc3d1	získaný
hlasům	hlas	k1gInPc3	hlas
bezmezně	bezmezně	k6eAd1	bezmezně
loajální	loajální	k2eAgMnPc4d1	loajální
<g/>
.	.	kIx.	.
</s>
<s>
Zvolená	zvolený	k2eAgFnSc1d1	zvolená
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
musela	muset	k5eAaImAgFnS	muset
tolerovat	tolerovat	k5eAaImF	tolerovat
nevěstince	nevěstinka	k1gFnSc3	nevěstinka
<g/>
,	,	kIx,	,
pouliční	pouliční	k2eAgFnSc3d1	pouliční
prostituci	prostituce	k1gFnSc3	prostituce
<g/>
,	,	kIx,	,
ilegální	ilegální	k2eAgMnPc4d1	ilegální
nálevny	nálevna	k1gFnSc2	nálevna
a	a	k8xC	a
hazard	hazard	k1gInSc1	hazard
<g/>
.	.	kIx.	.
</s>
<s>
Amerika	Amerika	k1gFnSc1	Amerika
měla	mít	k5eAaImAgFnS	mít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
fanatických	fanatický	k2eAgMnPc2d1	fanatický
nepřátel	nepřítel	k1gMnPc2	nepřítel
alkoholu	alkohol	k1gInSc2	alkohol
už	už	k6eAd1	už
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
kampaň	kampaň	k1gFnSc4	kampaň
za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
zákaz	zákaz	k1gInSc4	zákaz
postupně	postupně	k6eAd1	postupně
sílila	sílit	k5eAaImAgFnS	sílit
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
alkoholu	alkohol	k1gInSc2	alkohol
se	se	k3xPyFc4	se
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
až	až	k9	až
když	když	k8xS	když
se	se	k3xPyFc4	se
do	do	k7c2	do
díla	dílo	k1gNnSc2	dílo
abstinentů	abstinent	k1gMnPc2	abstinent
vložily	vložit	k5eAaPmAgFnP	vložit
americké	americký	k2eAgFnPc1d1	americká
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
typickou	typický	k2eAgFnSc7d1	typická
sveřepostí	sveřepost	k1gFnSc7	sveřepost
přinášely	přinášet	k5eAaImAgFnP	přinášet
petice	petice	k1gFnPc4	petice
a	a	k8xC	a
požadavky	požadavek	k1gInPc1	požadavek
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
jednáních	jednání	k1gNnPc6	jednání
a	a	k8xC	a
hlasováních	hlasování	k1gNnPc6	hlasování
vyšli	vyjít	k5eAaPmAgMnP	vyjít
se	s	k7c7	s
slávou	sláva	k1gFnSc7	sláva
odpůrci	odpůrce	k1gMnPc7	odpůrce
alkoholu	alkohol	k1gInSc2	alkohol
a	a	k8xC	a
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1920	[number]	k4	1920
zavedla	zavést	k5eAaPmAgFnS	zavést
vláda	vláda	k1gFnSc1	vláda
prohibici	prohibice	k1gFnSc4	prohibice
–	–	k?	–
nařízení	nařízení	k1gNnSc2	nařízení
zakazující	zakazující	k2eAgFnSc1d1	zakazující
vyrábět	vyrábět	k5eAaImF	vyrábět
<g/>
,	,	kIx,	,
dovážet	dovážet	k5eAaImF	dovážet
a	a	k8xC	a
prodávat	prodávat	k5eAaImF	prodávat
na	na	k7c6	na
území	území	k1gNnSc6	území
USA	USA	kA	USA
alkoholické	alkoholický	k2eAgInPc1d1	alkoholický
nápoje	nápoj	k1gInPc4	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zákon	zákon	k1gInSc1	zákon
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
mafii	mafie	k1gFnSc4	mafie
doslova	doslova	k6eAd1	doslova
darem	dar	k1gInSc7	dar
z	z	k7c2	z
nebes	nebesa	k1gNnPc2	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
platnosti	platnost	k1gFnSc2	platnost
zákonů	zákon	k1gInPc2	zákon
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
spekulanti	spekulant	k1gMnPc1	spekulant
ilegální	ilegální	k2eAgFnSc2d1	ilegální
sítě	síť	k1gFnSc2	síť
pašeráků	pašerák	k1gMnPc2	pašerák
s	s	k7c7	s
lihovinami	lihovina	k1gFnPc7	lihovina
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnSc1	mafie
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
sjednotila	sjednotit	k5eAaPmAgFnS	sjednotit
anebo	anebo	k8xC	anebo
násilím	násilí	k1gNnSc7	násilí
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Obrovské	obrovský	k2eAgInPc1d1	obrovský
zisky	zisk	k1gInPc1	zisk
z	z	k7c2	z
černého	černý	k2eAgInSc2d1	černý
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
jim	on	k3xPp3gMnPc3	on
umožnily	umožnit	k5eAaPmAgInP	umožnit
investovat	investovat	k5eAaBmF	investovat
peníze	peníz	k1gInPc1	peníz
do	do	k7c2	do
dalšího	další	k2eAgNnSc2d1	další
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
alkoholové	alkoholový	k2eAgFnSc2d1	alkoholová
prohibice	prohibice	k1gFnSc2	prohibice
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
-	-	kIx~	-
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
spotřeby	spotřeba	k1gFnSc2	spotřeba
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
k	k	k7c3	k
přemístění	přemístění	k1gNnSc3	přemístění
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
postavou	postava	k1gFnSc7	postava
newyorského	newyorský	k2eAgInSc2d1	newyorský
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
i	i	k9	i
legálně	legálně	k6eAd1	legálně
vyráběným	vyráběný	k2eAgInSc7d1	vyráběný
alkoholem	alkohol	k1gInSc7	alkohol
Boss	boss	k1gMnSc1	boss
Joe	Joe	k1gMnSc2	Joe
Masseria	Masserium	k1gNnSc2	Masserium
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
domohl	domoct	k5eAaPmAgMnS	domoct
značného	značný	k2eAgInSc2d1	značný
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mladým	mladý	k2eAgMnSc7d1	mladý
Carlem	Carl	k1gMnSc7	Carl
Gambinem	Gambin	k1gMnSc7	Gambin
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc2	on
pracovalo	pracovat	k5eAaImAgNnS	pracovat
několik	několik	k4yIc1	několik
perspektivních	perspektivní	k2eAgMnPc2d1	perspektivní
a	a	k8xC	a
ambiciózních	ambiciózní	k2eAgMnPc2d1	ambiciózní
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
dva	dva	k4xCgInPc1	dva
<g/>
,	,	kIx,	,
Joe	Joe	k1gFnSc1	Joe
Adonis	Adonis	k1gFnSc1	Adonis
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
Anastasia	Anastasium	k1gNnSc2	Anastasium
<g/>
,	,	kIx,	,
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
newyorském	newyorský	k2eAgNnSc6d1	newyorské
podsvětí	podsvětí	k1gNnSc6	podsvětí
významné	významný	k2eAgFnSc2d1	významná
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
objevil	objevit	k5eAaPmAgMnS	objevit
další	další	k2eAgMnSc1d1	další
silný	silný	k2eAgMnSc1d1	silný
soupeř	soupeř	k1gMnSc1	soupeř
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Masseriovo	Masseriův	k2eAgNnSc4d1	Masseriův
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
ohrozil	ohrozit	k5eAaPmAgInS	ohrozit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jím	on	k3xPp3gInSc7	on
Salvatore	Salvator	k1gMnSc5	Salvator
Maranzano	Maranzana	k1gFnSc5	Maranzana
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
přímořského	přímořský	k2eAgNnSc2d1	přímořské
městečka	městečko	k1gNnSc2	městečko
Castellammare	Castellammar	k1gMnSc5	Castellammar
del	del	k?	del
Golfo	Golfa	k1gMnSc5	Golfa
<g/>
,	,	kIx,	,
ležícího	ležící	k2eAgMnSc2d1	ležící
západně	západně	k6eAd1	západně
od	od	k7c2	od
Palerma	Palermo	k1gNnSc2	Palermo
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
všechny	všechen	k3xTgFnPc4	všechen
věrné	věrný	k2eAgFnPc4d1	věrná
z	z	k7c2	z
Castellammare	Castellammar	k1gMnSc5	Castellammar
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
velkého	velký	k2eAgNnSc2d1	velké
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
nebylo	být	k5eNaImAgNnS	být
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
úplné	úplný	k2eAgNnSc1d1	úplné
ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
italsko	italsko	k6eAd1	italsko
americké	americký	k2eAgFnSc2d1	americká
mafie	mafie	k1gFnSc2	mafie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
v	v	k7c6	v
Masseriově	Masseriově	k1gFnSc6	Masseriově
gangu	gang	k1gInSc2	gang
byli	být	k5eAaImAgMnP	být
Lucky	lucky	k6eAd1	lucky
Luciano	Luciana	k1gFnSc5	Luciana
<g/>
,	,	kIx,	,
Meyer	Meyer	k1gMnSc1	Meyer
Lansky	Lanska	k1gFnSc2	Lanska
<g/>
,	,	kIx,	,
Bugsy	Bugsa	k1gFnPc1	Bugsa
Siegel	Siegel	k1gMnSc1	Siegel
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Costello	Costello	k1gNnSc4	Costello
a	a	k8xC	a
Vito	vit	k2eAgNnSc4d1	Vito
Genovese	Genovese	k1gFnPc4	Genovese
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
teď	teď	k6eAd1	teď
bojovali	bojovat	k5eAaImAgMnP	bojovat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Alberta	Alberta	k1gFnSc1	Alberta
Anastasii	Anastasie	k1gFnSc3	Anastasie
a	a	k8xC	a
Carla	Carla	k1gFnSc1	Carla
Gambina	Gambina	k1gFnSc1	Gambina
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemné	vzájemný	k2eAgFnPc1d1	vzájemná
potyčky	potyčka	k1gFnPc1	potyčka
obou	dva	k4xCgInPc2	dva
gangů	gang	k1gInPc2	gang
nakonec	nakonec	k6eAd1	nakonec
vyustily	vyustit	k5eAaPmAgFnP	vyustit
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Castellammarskou	Castellammarský	k2eAgFnSc4d1	Castellammarský
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
a	a	k8xC	a
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
popudu	popud	k1gInSc2	popud
Lucky	Lucka	k1gFnSc2	Lucka
Luciana	Lucian	k1gMnSc2	Lucian
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
Masseria	Masserium	k1gNnPc5	Masserium
<g/>
.	.	kIx.	.
</s>
<s>
Maranzano	Maranzana	k1gFnSc5	Maranzana
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgInS	stát
nejmocnějším	mocný	k2eAgMnSc7d3	nejmocnější
vládcem	vládce	k1gMnSc7	vládce
zločineckého	zločinecký	k2eAgInSc2d1	zločinecký
světa	svět	k1gInSc2	svět
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
oficiálně	oficiálně	k6eAd1	oficiálně
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nový	nový	k2eAgInSc4d1	nový
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
od	od	k7c2	od
nynějška	nynějšek	k1gInSc2	nynějšek
ctihodná	ctihodný	k2eAgFnSc1d1	ctihodná
společnost	společnost	k1gFnSc1	společnost
řídit	řídit	k5eAaImF	řídit
<g/>
,	,	kIx,	,
svolal	svolat	k5eAaPmAgMnS	svolat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1931	[number]	k4	1931
do	do	k7c2	do
hotelu	hotel	k1gInSc2	hotel
Grand	grand	k1gMnSc1	grand
Concourse	Concourse	k1gFnSc2	Concourse
v	v	k7c6	v
Bronxu	Bronx	k1gInSc6	Bronx
schůzku	schůzka	k1gFnSc4	schůzka
všech	všecek	k3xTgFnPc2	všecek
mafiánských	mafiánský	k2eAgFnPc2d1	mafiánská
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
Maranzano	Maranzana	k1gFnSc5	Maranzana
newyorskou	newyorský	k2eAgFnSc4d1	newyorská
mafii	mafie	k1gFnSc4	mafie
na	na	k7c4	na
pět	pět	k4xCc4	pět
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
rodin	rodina	k1gFnPc2	rodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ustálily	ustálit	k5eAaPmAgFnP	ustálit
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
Gambino	Gambin	k2eAgNnSc1d1	Gambino
<g/>
,	,	kIx,	,
Genovese	Genovese	k1gFnSc5	Genovese
<g/>
,	,	kIx,	,
Colombo	Colomba	k1gFnSc5	Colomba
<g/>
,	,	kIx,	,
Lucchese	Lucchesa	k1gFnSc6	Lucchesa
a	a	k8xC	a
Bonanno	Bonanno	k6eAd1	Bonanno
<g/>
.	.	kIx.	.
</s>
<s>
Chicagskému	chicagský	k2eAgNnSc3d1	Chicagské
podsvětí	podsvětí	k1gNnSc3	podsvětí
vládl	vládnout	k5eAaImAgInS	vládnout
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
a	a	k8xC	a
nemilosrdnou	milosrdný	k2eNgFnSc7d1	nemilosrdná
rukou	ruka	k1gFnSc7	ruka
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
gangsterů	gangster	k1gMnPc2	gangster
Al	ala	k1gFnPc2	ala
Capone	Capon	k1gInSc5	Capon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
nahradil	nahradit	k5eAaPmAgMnS	nahradit
stárnoucího	stárnoucí	k2eAgMnSc4d1	stárnoucí
bosse	boss	k1gMnSc4	boss
ze	z	k7c2	z
"	"	kIx"	"
<g/>
staré	starý	k2eAgFnSc2d1	stará
školy	škola	k1gFnSc2	škola
<g/>
"	"	kIx"	"
Johnyho	Johny	k1gMnSc2	Johny
Torria	Torrium	k1gNnSc2	Torrium
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Maranzano	Maranzana	k1gFnSc5	Maranzana
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
"	"	kIx"	"
<g/>
capa	capa	k1gFnSc1	capa
di	di	k?	di
tutti	tutti	k2eAgMnPc1d1	tutti
capi	cap	k1gMnPc1	cap
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
za	za	k7c4	za
"	"	kIx"	"
<g/>
bosse	boss	k1gMnSc4	boss
všech	všecek	k3xTgMnPc2	všecek
bossů	boss	k1gMnPc2	boss
<g/>
"	"	kIx"	"
a	a	k8xC	a
seznámil	seznámit	k5eAaPmAgMnS	seznámit
shromáždění	shromáždění	k1gNnSc4	shromáždění
se	s	k7c7	s
zákoníkem	zákoník	k1gInSc7	zákoník
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgNnPc3	jenž
se	se	k3xPyFc4	se
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
bude	být	k5eAaImBp3nS	být
řídit	řídit	k5eAaImF	řídit
podsvětí	podsvětí	k1gNnSc1	podsvětí
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
řeč	řeč	k1gFnSc4	řeč
pronášel	pronášet	k5eAaImAgMnS	pronášet
v	v	k7c6	v
italštině	italština	k1gFnSc6	italština
se	se	k3xPyFc4	se
sicilském	sicilský	k2eAgNnSc6d1	sicilské
nářečí	nářečí	k1gNnSc6	nářečí
a	a	k8xC	a
proložil	proložit	k5eAaPmAgMnS	proložit
ji	on	k3xPp3gFnSc4	on
několika	několik	k4yIc7	několik
latinskými	latinský	k2eAgFnPc7d1	Latinská
větičkami	větička	k1gFnPc7	větička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgInS	použít
termín	termín	k1gInSc1	termín
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
označoval	označovat	k5eAaImAgInS	označovat
zločinecký	zločinecký	k2eAgInSc1d1	zločinecký
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
řád	řád	k1gInSc1	řád
určen	určit	k5eAaPmNgInS	určit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
termín	termín	k1gInSc1	termín
Cosa	Cos	k2eAgFnSc1d1	Cosa
nostra	nostra	k1gFnSc1	nostra
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
největší	veliký	k2eAgFnSc7d3	veliký
mocí	moc	k1gFnSc7	moc
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
mafie	mafie	k1gFnSc2	mafie
<g/>
,	,	kIx,	,
Lucky	lucky	k6eAd1	lucky
Luciano	Luciana	k1gFnSc5	Luciana
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
září	září	k1gNnSc6	září
1931	[number]	k4	1931
odstranit	odstranit	k5eAaPmF	odstranit
samotného	samotný	k2eAgMnSc4d1	samotný
Maranzana	Maranzan	k1gMnSc4	Maranzan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vybral	vybrat	k5eAaPmAgMnS	vybrat
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
zástupce	zástupce	k1gMnSc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Maranzano	Maranzana	k1gFnSc5	Maranzana
připravoval	připravovat	k5eAaImAgInS	připravovat
vraždu	vražda	k1gFnSc4	vražda
Luciana	Lucian	k1gMnSc2	Lucian
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
přebrala	přebrat	k5eAaPmAgFnS	přebrat
moc	moc	k6eAd1	moc
mafie	mafie	k1gFnSc1	mafie
mladší	mladý	k2eAgFnSc2d2	mladší
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
stanovil	stanovit	k5eAaPmAgInS	stanovit
tzv.	tzv.	kA	tzv.
Komisi	komise	k1gFnSc4	komise
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sestávala	sestávat	k5eAaImAgFnS	sestávat
z	z	k7c2	z
čelních	čelní	k2eAgMnPc2d1	čelní
představitelů	představitel	k1gMnPc2	představitel
všech	všecek	k3xTgFnPc2	všecek
pěti	pět	k4xCc3	pět
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
stanoveny	stanoven	k2eAgFnPc1d1	stanovena
hranice	hranice	k1gFnPc1	hranice
teritorií	teritorium	k1gNnPc2	teritorium
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
rodina	rodina	k1gFnSc1	rodina
uznávala	uznávat	k5eAaImAgFnS	uznávat
autonomii	autonomie	k1gFnSc4	autonomie
ostatních	ostatní	k2eAgMnPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
důležité	důležitý	k2eAgFnPc1d1	důležitá
věci	věc	k1gFnPc1	věc
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
řešeny	řešit	k5eAaImNgFnP	řešit
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
rodina	rodina	k1gFnSc1	rodina
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
při	při	k7c6	při
rozhodování	rozhodování	k1gNnSc6	rozhodování
stejný	stejný	k2eAgInSc4d1	stejný
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byla	být	k5eAaImAgFnS	být
napadena	napadnout	k5eAaPmNgFnS	napadnout
jedna	jeden	k4xCgFnSc1	jeden
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
očekávat	očekávat	k5eAaImF	očekávat
podporu	podpora	k1gFnSc4	podpora
ostatních	ostatní	k2eAgFnPc2d1	ostatní
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
některá	některý	k3yIgFnSc1	některý
rodina	rodina	k1gFnSc1	rodina
pravidla	pravidlo	k1gNnSc2	pravidlo
porušila	porušit	k5eAaPmAgFnS	porušit
<g/>
,	,	kIx,	,
stály	stát	k5eAaImAgFnP	stát
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
rodiny	rodina	k1gFnPc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
tak	tak	k6eAd1	tak
šel	jít	k5eAaImAgInS	jít
od	od	k7c2	od
italských	italský	k2eAgInPc2d1	italský
kriminálních	kriminální	k2eAgInPc2d1	kriminální
pouličních	pouliční	k2eAgInPc2d1	pouliční
gangů	gang	k1gInPc2	gang
<g/>
,	,	kIx,	,
až	až	k9	až
k	k	k7c3	k
"	"	kIx"	"
<g/>
národnímu	národní	k2eAgInSc3d1	národní
zločineckému	zločinecký	k2eAgInSc3d1	zločinecký
syndikátu	syndikát	k1gInSc3	syndikát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
nejmocnějšího	mocný	k2eAgInSc2d3	nejmocnější
může	moct	k5eAaImIp3nS	moct
Cosa	Cos	k2eAgFnSc1d1	Cosa
Nostry	Nostr	k1gInPc4	Nostr
<g/>
,	,	kIx,	,
Lucky	lucky	k6eAd1	lucky
Luciana	Luciana	k1gFnSc1	Luciana
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1933	[number]	k4	1933
skončilo	skončit	k5eAaPmAgNnS	skončit
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
období	období	k1gNnSc1	období
mafiánských	mafiánský	k2eAgFnPc2d1	mafiánská
spekulací	spekulace	k1gFnPc2	spekulace
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Ramsey	Ramsea	k1gFnSc2	Ramsea
Clark	Clark	k1gInSc1	Clark
vystihl	vystihnout	k5eAaPmAgInS	vystihnout
situaci	situace	k1gFnSc4	situace
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Těžko	těžko	k6eAd1	těžko
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
škody	škoda	k1gFnPc4	škoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
z	z	k7c2	z
prohibičního	prohibiční	k2eAgInSc2d1	prohibiční
sebeklamu	sebeklam	k1gInSc2	sebeklam
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejzávažnější	závažný	k2eAgInPc4d3	nejzávažnější
důsledky	důsledek	k1gInPc4	důsledek
patří	patřit	k5eAaImIp3nS	patřit
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
sicilská	sicilský	k2eAgFnSc1d1	sicilská
mafie	mafie	k1gFnSc1	mafie
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
gangsterské	gangsterský	k2eAgNnSc4d1	gangsterské
impérium	impérium	k1gNnSc4	impérium
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
ilegální	ilegální	k2eAgFnSc1d1	ilegální
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
pašování	pašování	k1gNnSc1	pašování
alkoholu	alkohol	k1gInSc2	alkohol
vynesly	vynést	k5eAaPmAgInP	vynést
obrovské	obrovský	k2eAgInPc1d1	obrovský
zisky	zisk	k1gInPc1	zisk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mafie	mafie	k1gFnSc1	mafie
s	s	k7c7	s
nebývalou	bývalý	k2eNgFnSc7d1	bývalý
vervou	verva	k1gFnSc7	verva
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
na	na	k7c4	na
odborové	odborový	k2eAgInPc4d1	odborový
svazy	svaz	k1gInPc4	svaz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
zcela	zcela	k6eAd1	zcela
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
získala	získat	k5eAaPmAgFnS	získat
vlivný	vlivný	k2eAgInSc4d1	vlivný
politický	politický	k2eAgInSc4d1	politický
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
komise	komise	k1gFnSc1	komise
schválila	schválit	k5eAaPmAgFnS	schválit
oficiální	oficiální	k2eAgFnSc4d1	oficiální
popravčí	popravčí	k2eAgFnSc4d1	popravčí
firmu	firma	k1gFnSc4	firma
Murder	Murder	k1gMnSc1	Murder
Incorporated	Incorporated	k1gMnSc1	Incorporated
<g/>
.	.	kIx.	.
</s>
<s>
Murder	Murder	k1gMnSc1	Murder
Incorporated	Incorporated	k1gMnSc1	Incorporated
se	se	k3xPyFc4	se
týkala	týkat	k5eAaImAgFnS	týkat
hlavně	hlavně	k9	hlavně
osoby	osoba	k1gFnPc1	osoba
Alberta	Albert	k1gMnSc4	Albert
Anastasii	Anastasie	k1gFnSc6	Anastasie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gInSc7	její
šefem	šef	k1gInSc7	šef
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
speciální	speciální	k2eAgFnSc4d1	speciální
odnož	odnož	k1gFnSc4	odnož
Cosa	Cos	k1gInSc2	Cos
nostry	nostr	k1gInPc4	nostr
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	s	k7c7	s
vraždami	vražda	k1gFnPc7	vražda
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
přímo	přímo	k6eAd1	přímo
napojena	napojit	k5eAaPmNgFnS	napojit
na	na	k7c4	na
žádnou	žádný	k3yNgFnSc4	žádný
mafiánskou	mafiánský	k2eAgFnSc4d1	mafiánská
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gMnPc1	jejich
bossové	boss	k1gMnPc1	boss
chtěli	chtít	k5eAaImAgMnP	chtít
zůstat	zůstat	k5eAaPmF	zůstat
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
oklností	oklnost	k1gFnPc2	oklnost
čistí	čistý	k2eAgMnPc1d1	čistý
a	a	k8xC	a
nepřáli	přát	k5eNaImAgMnP	přát
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jejich	jejich	k3xOp3gNnPc1	jejich
jména	jméno	k1gNnPc1	jméno
figurovala	figurovat	k5eAaImAgNnP	figurovat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nejtěžšími	těžký	k2eAgInPc7d3	nejtěžší
zločiny	zločin	k1gInPc7	zločin
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Murder	Murder	k1gMnSc1	Murder
Incorporated	Incorporated	k1gMnSc1	Incorporated
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
na	na	k7c6	na
rodinách	rodina	k1gFnPc6	rodina
jak	jak	k8xC	jak
organizačně	organizačně	k6eAd1	organizačně
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
personálně	personálně	k6eAd1	personálně
zcela	zcela	k6eAd1	zcela
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
mafii	mafie	k1gFnSc4	mafie
naskytl	naskytnout	k5eAaPmAgMnS	naskytnout
další	další	k2eAgInSc4d1	další
bohatý	bohatý	k2eAgInSc4d1	bohatý
zdroj	zdroj	k1gInSc4	zdroj
příjmů	příjem	k1gInPc2	příjem
<g/>
:	:	kIx,	:
benzín	benzín	k1gInSc1	benzín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
velmi	velmi	k6eAd1	velmi
nedostatkovou	dostatkový	k2eNgFnSc7d1	nedostatková
surovinou	surovina	k1gFnSc7	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
slibů	slib	k1gInPc2	slib
a	a	k8xC	a
zbraní	zbraň	k1gFnPc2	zbraň
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
od	od	k7c2	od
benzínových	benzínový	k2eAgNnPc2d1	benzínové
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
dřívější	dřívější	k2eAgFnPc1d1	dřívější
prodavače	prodavač	k1gMnSc2	prodavač
a	a	k8xC	a
do	do	k7c2	do
benzinového	benzinový	k2eAgInSc2d1	benzinový
monopolu	monopol	k1gInSc2	monopol
dosadili	dosadit	k5eAaPmAgMnP	dosadit
vlastní	vlastní	k2eAgMnPc4d1	vlastní
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
disponovali	disponovat	k5eAaBmAgMnP	disponovat
falešnými	falešný	k2eAgInPc7d1	falešný
benzinovými	benzinový	k2eAgInPc7d1	benzinový
kupóny	kupón	k1gInPc7	kupón
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
cenový	cenový	k2eAgInSc1d1	cenový
úřad	úřad	k1gInSc1	úřad
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
mafie	mafie	k1gFnSc1	mafie
dennodenně	dennodenně	k6eAd1	dennodenně
připravila	připravit	k5eAaPmAgFnS	připravit
válečné	válečný	k2eAgNnSc4d1	válečné
hospodářství	hospodářství	k1gNnSc4	hospodářství
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
milióny	milión	k4xCgInPc4	milión
litrů	litr	k1gInPc2	litr
benzínu	benzín	k1gInSc2	benzín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
využila	využít	k5eAaPmAgFnS	využít
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
Lucianových	Lucianův	k2eAgInPc2d1	Lucianův
kontaktů	kontakt	k1gInPc2	kontakt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
a	a	k8xC	a
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
mu	on	k3xPp3gMnSc3	on
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
Franka	Frank	k1gMnSc2	Frank
Costella	Costell	k1gMnSc2	Costell
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
doky	dok	k1gInPc7	dok
v	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
odbory	odbor	k1gInPc1	odbor
přístavních	přístavní	k2eAgMnPc2d1	přístavní
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
si	se	k3xPyFc3	se
byla	být	k5eAaImAgFnS	být
vědoma	vědom	k2eAgFnSc1d1	vědoma
sabotáží	sabotáž	k1gFnSc7	sabotáž
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
německými	německý	k2eAgMnPc7d1	německý
špiony	špion	k1gMnPc7	špion
nebo	nebo	k8xC	nebo
samotnou	samotný	k2eAgFnSc7d1	samotná
mafií	mafie	k1gFnSc7	mafie
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
Lucianem	Lucian	k1gMnSc7	Lucian
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ničení	ničení	k1gNnSc1	ničení
lodí	loď	k1gFnPc2	loď
přestane	přestat	k5eAaPmIp3nS	přestat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
Lucianovým	Lucianův	k2eAgInSc7d1	Lucianův
úkolem	úkol	k1gInSc7	úkol
spojit	spojit	k5eAaPmF	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
v	v	k7c6	v
Palermu	Palermo	k1gNnSc6	Palermo
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
a	a	k8xC	a
ulehčit	ulehčit	k5eAaPmF	ulehčit
tak	tak	k9	tak
vylodění	vylodění	k1gNnSc4	vylodění
amerických	americký	k2eAgNnPc2d1	americké
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Invaze	invaze	k1gFnSc1	invaze
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
mafie	mafie	k1gFnSc1	mafie
opouštěla	opouštět	k5eAaImAgFnS	opouštět
dřívější	dřívější	k2eAgFnSc4d1	dřívější
parazitní	parazitní	k2eAgFnSc4d1	parazitní
úlohu	úloha	k1gFnSc4	úloha
a	a	k8xC	a
dávala	dávat	k5eAaImAgFnS	dávat
dolarům	dolar	k1gInPc3	dolar
nahromaděným	nahromaděný	k2eAgInPc3d1	nahromaděný
za	za	k7c4	za
prohibice	prohibice	k1gFnPc4	prohibice
<g/>
,	,	kIx,	,
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
drog	droga	k1gFnPc2	droga
<g/>
,	,	kIx,	,
z	z	k7c2	z
heren	herna	k1gFnPc2	herna
<g/>
,	,	kIx,	,
prostituce	prostituce	k1gFnSc2	prostituce
a	a	k8xC	a
nakradených	nakradený	k2eAgInPc2d1	nakradený
peněz	peníze	k1gInPc2	peníze
z	z	k7c2	z
válečného	válečný	k2eAgNnSc2d1	válečné
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
zákonnou	zákonný	k2eAgFnSc4d1	zákonná
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
postavou	postava	k1gFnSc7	postava
Cosa	Cosa	k1gMnSc1	Cosa
Nostry	Nostr	k1gInPc4	Nostr
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Frank	Frank	k1gMnSc1	Frank
Costelo	Costela	k1gFnSc5	Costela
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
uvězněného	uvězněný	k2eAgMnSc4d1	uvězněný
a	a	k8xC	a
následně	následně	k6eAd1	následně
vyhoštěného	vyhoštěný	k2eAgMnSc2d1	vyhoštěný
Luciana	Lucian	k1gMnSc2	Lucian
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnSc1	mafie
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
investovala	investovat	k5eAaBmAgFnS	investovat
do	do	k7c2	do
nemovitostí	nemovitost	k1gFnPc2	nemovitost
<g/>
,	,	kIx,	,
skupovala	skupovat	k5eAaImAgFnS	skupovat
firmy	firma	k1gFnPc1	firma
<g/>
,	,	kIx,	,
stavěla	stavět	k5eAaImAgNnP	stavět
kasina	kasino	k1gNnPc1	kasino
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
těchto	tento	k3xDgFnPc2	tento
aktivit	aktivita	k1gFnPc2	aktivita
řídili	řídit	k5eAaImAgMnP	řídit
Meyer	Meyer	k1gInSc4	Meyer
Lansky	Lanska	k1gFnSc2	Lanska
a	a	k8xC	a
Bugsy	Bugsa	k1gFnSc2	Bugsa
Siegel	Siegela	k1gFnPc2	Siegela
<g/>
.	.	kIx.	.
</s>
<s>
Siegel	Siegel	k1gMnSc1	Siegel
nakonec	nakonec	k6eAd1	nakonec
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
aktivity	aktivita	k1gFnPc4	aktivita
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
za	za	k7c4	za
souvislosti	souvislost	k1gFnPc4	souvislost
kolem	kolem	k7c2	kolem
výstavby	výstavba	k1gFnSc2	výstavba
kasína	kasín	k1gMnSc4	kasín
Flamingo	Flamingo	k1gMnSc1	Flamingo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
předběžných	předběžný	k2eAgInPc2d1	předběžný
odhadů	odhad	k1gInPc2	odhad
mělo	mít	k5eAaImAgNnS	mít
Flamingo	Flamingo	k6eAd1	Flamingo
stát	stát	k5eAaImF	stát
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
Flamingo	Flamingo	k1gNnSc4	Flamingo
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
vyšplhaly	vyšplhat	k5eAaPmAgInP	vyšplhat
až	až	k9	až
k	k	k7c3	k
šesti	šest	k4xCc3	šest
milionům	milion	k4xCgInPc3	milion
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
současných	současný	k2eAgFnPc6d1	současná
cenách	cena	k1gFnPc6	cena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Mafie	mafie	k1gFnSc1	mafie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
do	do	k7c2	do
Flaminga	Flaminga	k1gFnSc1	Flaminga
vložila	vložit	k5eAaPmAgFnS	vložit
své	svůj	k3xOyFgInPc4	svůj
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
nehodlala	hodlat	k5eNaImAgFnS	hodlat
takovou	takový	k3xDgFnSc4	takový
věc	věc	k1gFnSc4	věc
Sieglovi	Siegl	k1gMnSc3	Siegl
odpustit	odpustit	k5eAaPmF	odpustit
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
sílily	sílit	k5eAaImAgFnP	sílit
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bugsy	Bugsa	k1gFnPc4	Bugsa
posílá	posílat	k5eAaImIp3nS	posílat
peníze	peníz	k1gInPc4	peníz
přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
milenku	milenka	k1gFnSc4	milenka
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
havanská	havanský	k2eAgFnSc1d1	Havanská
konference	konference	k1gFnSc1	konference
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
nebyl	být	k5eNaImAgInS	být
pro	pro	k7c4	pro
Siegla	Siegl	k1gMnSc4	Siegl
příznivý	příznivý	k2eAgInSc1d1	příznivý
<g/>
.	.	kIx.	.
</s>
<s>
Bossové	boss	k1gMnPc1	boss
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaPmAgMnP	shodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Siegel	Siegel	k1gInSc1	Siegel
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
Flaminga	Flaminga	k1gFnSc1	Flaminga
hrozbou	hrozba	k1gFnSc7	hrozba
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1947	[number]	k4	1947
byl	být	k5eAaImAgInS	být
zastřelen	zastřelit	k5eAaPmNgInS	zastřelit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
Beverly	Beverla	k1gFnSc2	Beverla
Hills	Hillsa	k1gFnPc2	Hillsa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
nesmírně	smírně	k6eNd1	smírně
výnosnou	výnosný	k2eAgFnSc7d1	výnosná
aktivitou	aktivita	k1gFnSc7	aktivita
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zřízení	zřízení	k1gNnSc1	zřízení
tzv.	tzv.	kA	tzv.
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
spojky	spojka	k1gFnSc2	spojka
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
obrovského	obrovský	k2eAgInSc2d1	obrovský
heroinového	heroinový	k2eAgInSc2d1	heroinový
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Joseph	Joseph	k1gInSc1	Joseph
Profaci	Proface	k1gFnSc4	Proface
a	a	k8xC	a
Lucky	lucky	k6eAd1	lucky
Luciano	Luciana	k1gFnSc5	Luciana
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
žije	žít	k5eAaImIp3nS	žít
po	po	k7c6	po
nucené	nucený	k2eAgFnSc6d1	nucená
deportaci	deportace	k1gFnSc6	deportace
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
vymyslí	vymyslet	k5eAaPmIp3nS	vymyslet
způsob	způsob	k1gInSc4	způsob
jak	jak	k8xC	jak
dostat	dostat	k5eAaPmF	dostat
heroin	heroin	k1gInSc4	heroin
bezpečně	bezpečně	k6eAd1	bezpečně
ze	z	k7c2	z
Sicílie	Sicílie	k1gFnSc2	Sicílie
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Profaciho	Profacize	k6eAd1	Profacize
importní	importní	k2eAgFnSc1d1	importní
firma	firma	k1gFnSc1	firma
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
ideální	ideální	k2eAgNnSc4d1	ideální
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Profaci	Proface	k1gFnSc4	Proface
začne	začít	k5eAaPmIp3nS	začít
připravovat	připravovat	k5eAaImF	připravovat
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
lukrativní	lukrativní	k2eAgInSc4d1	lukrativní
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
rok	rok	k1gInSc1	rok
buduje	budovat	k5eAaImIp3nS	budovat
odběratelské	odběratelský	k2eAgInPc4d1	odběratelský
vztahy	vztah	k1gInPc4	vztah
na	na	k7c4	na
sicilské	sicilský	k2eAgInPc4d1	sicilský
pomeranče	pomeranč	k1gInPc4	pomeranč
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
dumpingových	dumpingový	k2eAgFnPc2d1	dumpingová
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
objednávek	objednávka	k1gFnPc2	objednávka
a	a	k8xC	a
pomeranče	pomeranč	k1gInPc4	pomeranč
začnou	začít	k5eAaPmIp3nP	začít
proudit	proudit	k5eAaPmF	proudit
do	do	k7c2	do
USA	USA	kA	USA
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
<g/>
,	,	kIx,	,
rozjedou	rozjet	k5eAaPmIp3nP	rozjet
s	s	k7c7	s
Lucianem	Lucian	k1gInSc7	Lucian
pravou	pravý	k2eAgFnSc4d1	pravá
podstatu	podstata	k1gFnSc4	podstata
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bednách	bedna	k1gFnPc6	bedna
s	s	k7c7	s
pomeranči	pomeranč	k1gInPc7	pomeranč
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
pašují	pašovat	k5eAaImIp3nP	pašovat
pomeranče	pomeranč	k1gInPc1	pomeranč
falešné	falešný	k2eAgInPc1d1	falešný
<g/>
,	,	kIx,	,
z	z	k7c2	z
vosku	vosk	k1gInSc2	vosk
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yIgNnPc6	který
je	být	k5eAaImIp3nS	být
obsažen	obsažen	k2eAgInSc1d1	obsažen
heroin	heroin	k1gInSc1	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
bednička	bednička	k1gFnSc1	bednička
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
50	[number]	k4	50
kg	kg	kA	kg
heroinu	heroin	k1gInSc2	heroin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
prodával	prodávat	k5eAaImAgInS	prodávat
přibližně	přibližně	k6eAd1	přibližně
za	za	k7c4	za
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
bosem	bos	k1gMnSc7	bos
mafie	mafie	k1gFnSc2	mafie
stal	stát	k5eAaPmAgMnS	stát
Vito	vít	k5eAaImNgNnS	vít
Genovese	Genovesa	k1gFnSc3	Genovesa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
násilně	násilně	k6eAd1	násilně
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
Franka	Frank	k1gMnSc4	Frank
Costella	Costell	k1gMnSc4	Costell
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterého	který	k3yRgMnSc4	který
nechal	nechat	k5eAaPmAgMnS	nechat
spáchat	spáchat	k5eAaPmF	spáchat
atentát	atentát	k1gInSc4	atentát
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Costello	Costello	k1gNnSc4	Costello
jen	jen	k6eAd1	jen
čirou	čirý	k2eAgFnSc7d1	čirá
náhodou	náhoda	k1gFnSc7	náhoda
přežil	přežít	k5eAaPmAgInS	přežít
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
se	se	k3xPyFc4	se
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Genovese	Genovese	k1gFnSc2	Genovese
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
Apalachinu	Apalachin	k1gInSc6	Apalachin
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domě	dům	k1gInSc6	dům
Josepha	Joseph	k1gMnSc2	Joseph
Barbary	Barbara	k1gFnSc2	Barbara
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
bossové	boss	k1gMnPc1	boss
největších	veliký	k2eAgFnPc2d3	veliký
rodin	rodina	k1gFnPc2	rodina
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Genovese	Genovese	k1gFnSc1	Genovese
si	se	k3xPyFc3	se
tam	tam	k6eAd1	tam
chtěl	chtít	k5eAaImAgMnS	chtít
utvrdit	utvrdit	k5eAaPmF	utvrdit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
domu	dům	k1gInSc2	dům
Josepha	Joseph	k1gMnSc2	Joseph
Barbary	Barbara	k1gFnSc2	Barbara
jeli	jet	k5eAaImAgMnP	jet
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
dva	dva	k4xCgMnPc1	dva
policisté	policista	k1gMnPc1	policista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
všimli	všimnout	k5eAaPmAgMnP	všimnout
množství	množství	k1gNnSc4	množství
luxusních	luxusní	k2eAgNnPc2d1	luxusní
aut	auto	k1gNnPc2	auto
<g/>
.	.	kIx.	.
</s>
<s>
Začali	začít	k5eAaPmAgMnP	začít
si	se	k3xPyFc3	se
poznamenávat	poznamenávat	k5eAaImF	poznamenávat
jejich	jejich	k3xOp3gFnPc4	jejich
značky	značka	k1gFnPc4	značka
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
přítomní	přítomný	k1gMnPc1	přítomný
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
snažili	snažit	k5eAaImAgMnP	snažit
rychle	rychle	k6eAd1	rychle
odjet	odjet	k5eAaPmF	odjet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ulice	ulice	k1gFnSc1	ulice
byla	být	k5eAaImAgFnS	být
zablokovaná	zablokovaný	k2eAgFnSc1d1	zablokovaná
<g/>
.	.	kIx.	.
</s>
<s>
Někdo	někdo	k3yInSc1	někdo
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
utéci	utéct	k5eAaPmF	utéct
pěšky	pěšky	k6eAd1	pěšky
<g/>
,	,	kIx,	,
policisté	policista	k1gMnPc1	policista
později	pozdě	k6eAd2	pozdě
našli	najít	k5eAaPmAgMnP	najít
v	v	k7c6	v
blízkém	blízký	k2eAgInSc6d1	blízký
lese	les	k1gInSc6	les
několik	několik	k4yIc4	několik
odhozených	odhozený	k2eAgFnPc2d1	odhozená
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
také	také	k9	také
značnou	značný	k2eAgFnSc4d1	značná
peněžní	peněžní	k2eAgFnSc4d1	peněžní
hotovost	hotovost	k1gFnSc4	hotovost
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
skončili	skončit	k5eAaPmAgMnP	skončit
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
vyslýcháni	vyslýchat	k5eAaImNgMnP	vyslýchat
a	a	k8xC	a
poté	poté	k6eAd1	poté
propuštěni	propustit	k5eAaPmNgMnP	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Vina	vina	k1gFnSc1	vina
za	za	k7c4	za
nezdar	nezdar	k1gInSc4	nezdar
této	tento	k3xDgFnSc2	tento
konference	konference	k1gFnSc2	konference
byla	být	k5eAaImAgFnS	být
přisuzována	přisuzován	k2eAgFnSc1d1	přisuzována
Vitu	vit	k2eAgFnSc4d1	Vita
Genovesovi	Genoves	k1gMnSc3	Genoves
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
připravili	připravit	k5eAaPmAgMnP	připravit
Luciano	Luciana	k1gFnSc5	Luciana
<g/>
,	,	kIx,	,
Lansky	Lansek	k1gInPc4	Lansek
a	a	k8xC	a
Costello	Costello	k1gNnSc4	Costello
na	na	k7c4	na
Genoveseho	Genovese	k1gMnSc4	Genovese
past	past	k1gFnSc1	past
<g/>
.	.	kIx.	.
</s>
<s>
Nalákali	nalákat	k5eAaPmAgMnP	nalákat
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
fingovaného	fingovaný	k2eAgInSc2d1	fingovaný
drogového	drogový	k2eAgInSc2d1	drogový
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
Genoveseho	Genovese	k1gMnSc2	Genovese
zatčení	zatčení	k1gNnSc1	zatčení
a	a	k8xC	a
odsouzení	odsouzení	k1gNnSc1	odsouzení
na	na	k7c4	na
15	[number]	k4	15
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
odplatu	odplata	k1gFnSc4	odplata
za	za	k7c4	za
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
Franka	Frank	k1gMnSc4	Frank
Costella	Costell	k1gMnSc4	Costell
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
Genoveseho	Genoveseha	k1gFnSc5	Genoveseha
prohřešky	prohřešek	k1gInPc4	prohřešek
<g/>
.	.	kIx.	.
</s>
<s>
Genovese	Genovese	k1gFnSc1	Genovese
umírá	umírat	k5eAaImIp3nS	umírat
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
nejmocnějšího	mocný	k2eAgMnSc4d3	nejmocnější
bosse	boss	k1gMnSc4	boss
ho	on	k3xPp3gInSc2	on
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
Carlo	Carlo	k1gNnSc1	Carlo
Gambino	Gambin	k2eAgNnSc1d1	Gambino
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
Gambino	Gambin	k2eAgNnSc4d1	Gambino
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
stává	stávat	k5eAaImIp3nS	stávat
nejmocnější	mocný	k2eAgNnSc1d3	nejmocnější
<g/>
,	,	kIx,	,
nejrespektovanější	respektovaný	k2eAgFnSc7d3	nejrespektovanější
a	a	k8xC	a
nejobávanější	obávaný	k2eAgFnSc7d3	nejobávanější
rodinou	rodina	k1gFnSc7	rodina
Cosa	Cos	k1gInSc2	Cos
Nostry	Nostr	k1gMnPc7	Nostr
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
rodiny	rodina	k1gFnPc1	rodina
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podřízeném	podřízený	k2eAgNnSc6d1	podřízené
postavení	postavení	k1gNnSc6	postavení
a	a	k8xC	a
Carlo	Carlo	k1gNnSc4	Carlo
Gambino	Gambin	k2eAgNnSc4d1	Gambino
dokonce	dokonce	k9	dokonce
přímo	přímo	k6eAd1	přímo
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
rodinách	rodina	k1gFnPc6	rodina
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Bonanno	Bonanno	k6eAd1	Bonanno
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
úzce	úzko	k6eAd1	úzko
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
Meyer	Meyer	k1gInSc1	Meyer
Lansky	Lanska	k1gFnSc2	Lanska
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
offshore	offshor	k1gInSc5	offshor
aktivity	aktivita	k1gFnPc4	aktivita
rodiny	rodina	k1gFnSc2	rodina
Gambino	Gambin	k2eAgNnSc4d1	Gambino
na	na	k7c6	na
Bahamách	Bahamy	k1gFnPc6	Bahamy
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
postupně	postupně	k6eAd1	postupně
získává	získávat	k5eAaImIp3nS	získávat
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
postupně	postupně	k6eAd1	postupně
zcela	zcela	k6eAd1	zcela
ovládne	ovládnout	k5eAaPmIp3nS	ovládnout
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
tak	tak	k9	tak
téměř	téměř	k6eAd1	téměř
vešerou	vešerý	k2eAgFnSc4d1	vešerý
výstavbu	výstavba	k1gFnSc4	výstavba
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ujímá	ujímat	k5eAaImIp3nS	ujímat
vedení	vedení	k1gNnSc1	vedení
rodiny	rodina	k1gFnSc2	rodina
Gambino	Gambin	k2eAgNnSc1d1	Gambino
Paul	Paul	k1gMnSc1	Paul
Castellano	Castellana	k1gFnSc5	Castellana
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
si	se	k3xPyFc3	se
stářím	stáří	k1gNnSc7	stáří
umírající	umírající	k1gFnSc2	umírající
Carlo	Carlo	k1gNnSc1	Carlo
Gambino	Gambin	k2eAgNnSc1d1	Gambino
vybere	vybrat	k5eAaPmIp3nS	vybrat
jako	jako	k9	jako
svého	svůj	k3xOyFgMnSc4	svůj
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
Gambino	Gambin	k2eAgNnSc1d1	Gambino
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celá	celý	k2eAgFnSc1d1	celá
mafie	mafie	k1gFnSc1	mafie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
mocenském	mocenský	k2eAgInSc6d1	mocenský
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
bezstarostností	bezstarostnost	k1gFnSc7	bezstarostnost
mafiánských	mafiánský	k2eAgMnPc2d1	mafiánský
šéfů	šéf	k1gMnPc2	šéf
se	se	k3xPyFc4	se
ale	ale	k9	ale
pomalu	pomalu	k6eAd1	pomalu
začínají	začínat	k5eAaImIp3nP	začínat
stahovat	stahovat	k5eAaImF	stahovat
mračna	mračno	k1gNnPc4	mračno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
je	být	k5eAaImIp3nS	být
rodina	rodina	k1gFnSc1	rodina
Bonanno	Bonanno	k6eAd1	Bonanno
infiltrována	infiltrovat	k5eAaBmNgFnS	infiltrovat
špionem	špion	k1gMnSc7	špion
FBI	FBI	kA	FBI
Josephem	Joseph	k1gInSc7	Joseph
D.	D.	kA	D.
Pistonem	piston	k1gInSc7	piston
<g/>
,	,	kIx,	,
známým	známý	k2eAgMnSc7d1	známý
jako	jako	k8xS	jako
Donnie	Donnie	k1gFnPc4	Donnie
Brasco	Brasco	k6eAd1	Brasco
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
6	[number]	k4	6
let	léto	k1gNnPc2	léto
sbírat	sbírat	k5eAaImF	sbírat
důležité	důležitý	k2eAgFnPc4d1	důležitá
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
fungování	fungování	k1gNnSc4	fungování
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
mapovat	mapovat	k5eAaImF	mapovat
zločiny	zločin	k1gInPc4	zločin
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
vejde	vejít	k5eAaPmIp3nS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
zločinných	zločinný	k2eAgFnPc6d1	zločinná
organizacích	organizace	k1gFnPc6	organizace
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
zákon	zákon	k1gInSc1	zákon
proti	proti	k7c3	proti
zločinnému	zločinný	k2eAgNnSc3d1	zločinné
spolčování	spolčování	k1gNnSc3	spolčování
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
RICO	RICO	kA	RICO
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
velmi	velmi	k6eAd1	velmi
účinným	účinný	k2eAgInSc7d1	účinný
nástrojem	nástroj	k1gInSc7	nástroj
v	v	k7c6	v
potírání	potírání	k1gNnSc6	potírání
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zločinné	zločinný	k2eAgNnSc4d1	zločinné
spolčení	spolčení	k1gNnSc4	spolčení
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hrozí	hrozit	k5eAaImIp3nS	hrozit
20	[number]	k4	20
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
přímo	přímo	k6eAd1	přímo
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
každého	každý	k3xTgMnSc4	každý
zainteresovaného	zainteresovaný	k2eAgMnSc4d1	zainteresovaný
spolupracovníka	spolupracovník	k1gMnSc4	spolupracovník
mafie	mafie	k1gFnSc2	mafie
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
mafiány	mafián	k1gMnPc4	mafián
samotné	samotný	k2eAgNnSc1d1	samotné
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
narušení	narušení	k1gNnSc3	narušení
struktury	struktura	k1gFnSc2	struktura
mafie	mafie	k1gFnSc2	mafie
<g/>
.	.	kIx.	.
</s>
<s>
FBI	FBI	kA	FBI
podniká	podnikat	k5eAaImIp3nS	podnikat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
razie	razie	k1gFnPc4	razie
<g/>
,	,	kIx,	,
odposlechy	odposlech	k1gInPc4	odposlech
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
práce	práce	k1gFnSc1	práce
informátorů	informátor	k1gMnPc2	informátor
nasazených	nasazený	k2eAgMnPc2d1	nasazený
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
center	centrum	k1gNnPc2	centrum
rodin	rodina	k1gFnPc2	rodina
(	(	kIx(	(
<g/>
Donnie	Donnie	k1gFnSc1	Donnie
Brasco	Brasco	k1gMnSc1	Brasco
<g/>
)	)	kIx)	)
a	a	k8xC	a
získává	získávat	k5eAaImIp3nS	získávat
tak	tak	k9	tak
důležité	důležitý	k2eAgMnPc4d1	důležitý
svědky	svědek	k1gMnPc4	svědek
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
mafie	mafie	k1gFnSc2	mafie
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nechtějí	chtít	k5eNaImIp3nP	chtít
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
se	se	k3xPyFc4	se
radši	rád	k6eAd2	rád
vypovídat	vypovídat	k5eAaPmF	vypovídat
<g/>
.	.	kIx.	.
</s>
<s>
Zavádí	zavádět	k5eAaImIp3nS	zavádět
se	se	k3xPyFc4	se
důležitý	důležitý	k2eAgInSc1d1	důležitý
institut	institut	k1gInSc1	institut
ochrany	ochrana	k1gFnSc2	ochrana
svědků	svědek	k1gMnPc2	svědek
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
dostává	dostávat	k5eAaImIp3nS	dostávat
několik	několik	k4yIc1	několik
vysoce	vysoce	k6eAd1	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
mafiánů	mafián	k1gMnPc2	mafián
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
kdo	kdo	k3yRnSc1	kdo
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
a	a	k8xC	a
rozkrývá	rozkrývat	k5eAaImIp3nS	rozkrývat
strukturu	struktura	k1gFnSc4	struktura
mafie	mafie	k1gFnSc2	mafie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
Joe	Joe	k1gFnSc1	Joe
Valachi	Valach	k1gFnSc2	Valach
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
mafiánem	mafián	k1gMnSc7	mafián
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
informátorem	informátor	k1gMnSc7	informátor
FBI	FBI	kA	FBI
je	on	k3xPp3gInPc4	on
Sammy	Samm	k1gInPc4	Samm
Gravano	Gravana	k1gFnSc5	Gravana
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgInSc1d1	samotný
underboss	underboss	k1gInSc1	underboss
mocné	mocný	k2eAgFnSc2d1	mocná
rodiny	rodina	k1gFnSc2	rodina
Gambino	Gambin	k2eAgNnSc1d1	Gambino
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
mafii	mafie	k1gFnSc4	mafie
těžká	těžký	k2eAgFnSc1d1	těžká
rána	rána	k1gFnSc1	rána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	on	k3xPp3gNnSc2	on
svědectví	svědectví	k1gNnSc2	svědectví
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
usvědčen	usvědčit	k5eAaPmNgMnS	usvědčit
samotný	samotný	k2eAgMnSc1d1	samotný
velký	velký	k2eAgMnSc1d1	velký
boss	boss	k1gMnSc1	boss
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
rodiny	rodina	k1gFnSc2	rodina
Gambino	Gambin	k2eAgNnSc1d1	Gambino
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Gotti	Gotť	k1gFnSc2	Gotť
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
zavraždit	zavraždit	k5eAaPmF	zavraždit
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
Paula	Paul	k1gMnSc4	Paul
Castellana	Castellan	k1gMnSc4	Castellan
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
je	být	k5eAaImIp3nS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
a	a	k8xC	a
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
23	[number]	k4	23
šéfů	šéf	k1gMnPc2	šéf
<g/>
,	,	kIx,	,
13	[number]	k4	13
podšéfů	podšéf	k1gMnPc2	podšéf
<g/>
,	,	kIx,	,
43	[number]	k4	43
posádkových	posádkový	k2eAgMnPc2d1	posádkový
kapitánů	kapitán	k1gMnPc2	kapitán
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
"	"	kIx"	"
<g/>
vojáků	voják	k1gMnPc2	voják
<g/>
"	"	kIx"	"
a	a	k8xC	a
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
mafie	mafie	k1gFnSc2	mafie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
značný	značný	k2eAgInSc4d1	značný
pokles	pokles	k1gInSc4	pokles
moci	moc	k1gFnSc2	moc
Cosa	Cos	k1gInSc2	Cos
Nostry	Nostr	k1gInPc4	Nostr
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
Cosa	Cos	k2eAgFnSc1d1	Cosa
Nostra	Nostra	k1gFnSc1	Nostra
jednoznačně	jednoznačně	k6eAd1	jednoznačně
nejmocnější	mocný	k2eAgFnSc7d3	nejmocnější
zločineckou	zločinecký	k2eAgFnSc7d1	zločinecká
organizací	organizace	k1gFnSc7	organizace
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úderu	úder	k1gInSc6	úder
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc4d1	nová
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
lépe	dobře	k6eAd2	dobře
utajené	utajený	k2eAgFnPc1d1	utajená
<g/>
.	.	kIx.	.
</s>
<s>
Cosa	Cos	k2eAgFnSc1d1	Cosa
Nostra	Nostra	k1gFnSc1	Nostra
nashromáždila	nashromáždit	k5eAaPmAgFnS	nashromáždit
obrovský	obrovský	k2eAgInSc4d1	obrovský
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
finance	finance	k1gFnPc4	finance
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
silou	síla	k1gFnSc7	síla
a	a	k8xC	a
proto	proto	k8xC	proto
nebude	být	k5eNaImBp3nS	být
nikdy	nikdy	k6eAd1	nikdy
možné	možný	k2eAgNnSc1d1	možné
její	její	k3xOp3gFnPc4	její
aktivity	aktivita	k1gFnPc4	aktivita
úplně	úplně	k6eAd1	úplně
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
organizací	organizace	k1gFnSc7	organizace
je	být	k5eAaImIp3nS	být
nekonečný	konečný	k2eNgInSc1d1	nekonečný
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
zločinci	zločinec	k1gMnPc1	zločinec
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
krok	krok	k1gInSc4	krok
napřed	napřed	k6eAd1	napřed
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc7d1	hlavní
prioritou	priorita	k1gFnSc7	priorita
FBI	FBI	kA	FBI
tedy	tedy	k8xC	tedy
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nedovolit	dovolit	k5eNaPmF	dovolit
opětovný	opětovný	k2eAgInSc4d1	opětovný
rozmach	rozmach	k1gInSc4	rozmach
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc4	jaký
mafie	mafie	k1gFnSc1	mafie
zažila	zažít	k5eAaPmAgFnS	zažít
v	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
činnosti	činnost	k1gFnPc4	činnost
současné	současný	k2eAgFnSc2d1	současná
mafie	mafie	k1gFnSc2	mafie
patří	patřit	k5eAaImIp3nS	patřit
nájemné	nájemné	k1gNnSc1	nájemné
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
vydírání	vydírání	k1gNnSc2	vydírání
<g/>
,	,	kIx,	,
korupce	korupce	k1gFnSc1	korupce
veřejných	veřejný	k2eAgMnPc2d1	veřejný
činitelů	činitel	k1gMnPc2	činitel
<g/>
,	,	kIx,	,
pronikání	pronikání	k1gNnSc4	pronikání
do	do	k7c2	do
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
hazardní	hazardní	k2eAgFnSc2d1	hazardní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
infiltrace	infiltrace	k1gFnSc2	infiltrace
legitimního	legitimní	k2eAgNnSc2d1	legitimní
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
,	,	kIx,	,
nelegální	legální	k2eNgNnPc4d1	nelegální
zaměstnávání	zaměstnávání	k1gNnPc4	zaměstnávání
<g/>
,	,	kIx,	,
úvěrové	úvěrový	k2eAgInPc4d1	úvěrový
a	a	k8xC	a
daňové	daňový	k2eAgInPc4d1	daňový
podvody	podvod	k1gInPc4	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Zločinecká	zločinecký	k2eAgFnSc1d1	zločinecká
rodina	rodina	k1gFnSc1	rodina
Magaddino	Magaddin	k2eAgNnSc1d1	Magaddin
(	(	kIx(	(
<g/>
Buffalo	Buffalo	k1gFnSc1	Buffalo
<g/>
)	)	kIx)	)
-	-	kIx~	-
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
20	[number]	k4	20
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
+	+	kIx~	+
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
boss	boss	k1gMnSc1	boss
-	-	kIx~	-
Stefano	Stefana	k1gFnSc5	Stefana
Magaddino	Magaddin	k2eAgNnSc1d1	Magaddin
Chicago	Chicago	k1gNnSc4	Chicago
Outfit	Outfita	k1gFnPc2	Outfita
-	-	kIx~	-
250	[number]	k4	250
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významní	významný	k2eAgMnPc1d1	významný
bossové	boss	k1gMnPc1	boss
-	-	kIx~	-
Giacomo	Giacoma	k1gFnSc5	Giacoma
"	"	kIx"	"
<g/>
Big	Big	k1gFnSc2	Big
Jim	on	k3xPp3gMnPc3	on
<g/>
"	"	kIx"	"
Colosimo	Colosima	k1gFnSc5	Colosima
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
"	"	kIx"	"
<g/>
Papa	papa	k1gMnSc1	papa
Johnny	Johnna	k1gMnSc2	Johnna
<g/>
"	"	kIx"	"
Torrio	Torrio	k1gMnSc1	Torrio
<g/>
,	,	kIx,	,
Alphonse	Alphonse	k1gFnSc1	Alphonse
"	"	kIx"	"
<g/>
Scarface	Scarface	k1gFnSc1	Scarface
Al	ala	k1gFnPc2	ala
<g/>
"	"	kIx"	"
Capone	Capon	k1gMnSc5	Capon
<g/>
,	,	kIx,	,
Detroit	Detroit	k1gInSc1	Detroit
Partnership	Partnership	k1gInSc1	Partnership
-	-	kIx~	-
50	[number]	k4	50
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
200	[number]	k4	200
<g/>
+	+	kIx~	+
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
boss	boss	k1gMnSc1	boss
-	-	kIx~	-
Joseph	Joseph	k1gMnSc1	Joseph
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Old	Olda	k1gFnPc2	Olda
Man	Man	k1gMnSc1	Man
<g/>
"	"	kIx"	"
Zerilli	Zerille	k1gFnSc3	Zerille
Zločinecká	zločinecký	k2eAgFnSc1d1	zločinecká
rodina	rodina	k1gFnSc1	rodina
Kansas	Kansas	k1gInSc1	Kansas
City	City	k1gFnSc1	City
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
20	[number]	k4	20
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
+	+	kIx~	+
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
boss	boss	k1gMnSc1	boss
-	-	kIx~	-
Nicholas	Nicholas	k1gMnSc1	Nicholas
Civella	Civell	k1gMnSc2	Civell
Zločinecká	zločinecký	k2eAgFnSc1d1	zločinecká
rodina	rodina	k1gFnSc1	rodina
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
-	-	kIx~	-
10-20	[number]	k4	10-20
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
boss	boss	k1gMnSc1	boss
-	-	kIx~	-
Frank	Frank	k1gMnSc1	Frank
DeSimone	DeSimon	k1gInSc5	DeSimon
Zločinecká	zločinecký	k2eAgFnSc1d1	zločinecká
rodina	rodina	k1gFnSc1	rodina
Providence	providence	k1gFnSc2	providence
(	(	kIx(	(
<g/>
Boston	Boston	k1gInSc1	Boston
<g/>
)	)	kIx)	)
-	-	kIx~	-
40	[number]	k4	40
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
100	[number]	k4	100
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
boss	boss	k1gMnSc1	boss
-	-	kIx~	-
Raymond	Raymond	k1gMnSc1	Raymond
<g />
.	.	kIx.	.
</s>
<s>
Patriarca	Patriarc	k2eAgFnSc1d1	Patriarc
Zločinecká	zločinecký	k2eAgFnSc1d1	zločinecká
rodina	rodina	k1gFnSc1	rodina
DeCavalcante	DeCavalcant	k1gMnSc5	DeCavalcant
(	(	kIx(	(
<g/>
Atlantic	Atlantice	k1gFnPc2	Atlantice
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
)	)	kIx)	)
-	-	kIx~	-
40	[number]	k4	40
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
200	[number]	k4	200
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
boss	boss	k1gMnSc1	boss
-	-	kIx~	-
Simone	Simon	k1gMnSc5	Simon
"	"	kIx"	"
<g/>
Sam	Sam	k1gMnSc1	Sam
the	the	k?	the
Plumber	Plumber	k1gMnSc1	Plumber
<g/>
"	"	kIx"	"
DeCavalcante	DeCavalcant	k1gMnSc5	DeCavalcant
Pět	pět	k4xCc1	pět
rodin	rodina	k1gFnPc2	rodina
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
-	-	kIx~	-
celkem	celkem	k6eAd1	celkem
825	[number]	k4	825
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
7900	[number]	k4	7900
<g />
.	.	kIx.	.
</s>
<s>
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
důležití	důležitý	k2eAgMnPc1d1	důležitý
členové	člen	k1gMnPc1	člen
-	-	kIx~	-
Meyer	Meyer	k1gMnSc1	Meyer
Lansky	Lanska	k1gFnSc2	Lanska
<g/>
,	,	kIx,	,
Bugsy	Bugsa	k1gFnSc2	Bugsa
Siegel	Siegela	k1gFnPc2	Siegela
<g/>
,	,	kIx,	,
Joe	Joe	k1gFnSc1	Joe
Adonis	Adonis	k1gFnSc1	Adonis
<g/>
,	,	kIx,	,
Frankie	Frankie	k1gFnSc1	Frankie
Yale	Yale	k1gFnSc1	Yale
<g/>
,	,	kIx,	,
Dutch	Dutch	k1gInSc1	Dutch
Schultz	Schultza	k1gFnPc2	Schultza
Bonanno	Bonanno	k6eAd1	Bonanno
-	-	kIx~	-
110	[number]	k4	110
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
500	[number]	k4	500
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významní	významný	k2eAgMnPc1d1	významný
bossové	boss	k1gMnPc1	boss
-	-	kIx~	-
Salvatore	Salvator	k1gMnSc5	Salvator
Maranzano	Maranzana	k1gFnSc5	Maranzana
<g/>
,	,	kIx,	,
Giuseppe	Giusepp	k1gMnSc5	Giusepp
"	"	kIx"	"
<g/>
Joseph	Joseph	k1gInSc4	Joseph
<g/>
"	"	kIx"	"
Bonanno	Bonanno	k6eAd1	Bonanno
Colombo	Colomba	k1gFnSc5	Colomba
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
115	[number]	k4	115
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
400	[number]	k4	400
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významní	významný	k2eAgMnPc1d1	významný
bossové	boss	k1gMnPc1	boss
-	-	kIx~	-
Joseph	Joseph	k1gMnSc1	Joseph
Profaci	Proface	k1gFnSc4	Proface
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc4	Joseph
Colombo	Colomba	k1gFnSc5	Colomba
<g/>
,	,	kIx,	,
Gambino	Gambina	k1gFnSc5	Gambina
-	-	kIx~	-
200	[number]	k4	200
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
3000	[number]	k4	3000
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významní	významný	k2eAgMnPc1d1	významný
bossové	boss	k1gMnPc1	boss
-	-	kIx~	-
Albert	Albert	k1gMnSc1	Albert
"	"	kIx"	"
<g/>
Mad	Mad	k1gMnSc1	Mad
Hatter	Hatter	k1gMnSc1	Hatter
<g/>
"	"	kIx"	"
Anastasia	Anastasia	k1gFnSc1	Anastasia
<g/>
,	,	kIx,	,
Carlo	Carlo	k1gNnSc1	Carlo
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Godfather	Godfathra	k1gFnPc2	Godfathra
<g/>
"	"	kIx"	"
Gambino	Gambin	k2eAgNnSc1d1	Gambino
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
"	"	kIx"	"
<g/>
Big	Big	k1gMnSc1	Big
Paul	Paul	k1gMnSc1	Paul
<g/>
"	"	kIx"	"
Castellano	Castellana	k1gFnSc5	Castellana
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Teflon	teflon	k1gInSc1	teflon
Don	Don	k1gMnSc1	Don
<g/>
"	"	kIx"	"
Gotti	Gotti	k1gNnSc1	Gotti
Genovese	Genovese	k1gFnSc2	Genovese
-	-	kIx~	-
300	[number]	k4	300
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
3000	[number]	k4	3000
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významní	významný	k2eAgMnPc1d1	významný
bossové	boss	k1gMnPc1	boss
-	-	kIx~	-
Giuseppe	Giusepp	k1gInSc5	Giusepp
"	"	kIx"	"
<g/>
Joe	Joe	k1gMnSc1	Joe
the	the	k?	the
Boss	boss	k1gMnSc1	boss
<g/>
"	"	kIx"	"
Masseria	Masserium	k1gNnSc2	Masserium
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
"	"	kIx"	"
<g/>
Lucky	lucky	k6eAd1	lucky
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
Luciano	Luciana	k1gFnSc5	Luciana
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
"	"	kIx"	"
<g/>
the	the	k?	the
Prime	prim	k1gInSc5	prim
Minister	Minister	k1gInSc4	Minister
<g/>
"	"	kIx"	"
Costello	Costello	k1gNnSc4	Costello
<g/>
,	,	kIx,	,
Vito	vit	k2eAgNnSc4d1	Vito
"	"	kIx"	"
<g/>
Don	Don	k1gInSc4	Don
Vito	vit	k2eAgNnSc1d1	Vito
<g/>
"	"	kIx"	"
Genovese	Genovese	k1gFnSc1	Genovese
Lucchese	Lucchese	k1gFnSc1	Lucchese
-	-	kIx~	-
100	[number]	k4	100
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
1000	[number]	k4	1000
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
boss	boss	k1gMnSc1	boss
-	-	kIx~	-
Gaetano	Gaetana	k1gFnSc5	Gaetana
"	"	kIx"	"
<g/>
Tommy	Tomm	k1gInPc4	Tomm
Brown	Brown	k1gInSc1	Brown
<g/>
"	"	kIx"	"
Lucchese	Lucchese	k1gFnSc1	Lucchese
Zločinecká	zločinecký	k2eAgFnSc1d1	zločinecká
rodina	rodina	k1gFnSc1	rodina
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
-	-	kIx~	-
50	[number]	k4	50
členů	člen	k1gMnPc2	člen
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
100	[number]	k4	100
společníků	společník	k1gMnPc2	společník
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
boss	boss	k1gMnSc1	boss
-	-	kIx~	-
Angelo	Angela	k1gFnSc5	Angela
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Gentle	Gentle	k1gFnSc2	Gentle
Don	Don	k1gMnSc1	Don
<g/>
"	"	kIx"	"
Bruno	Bruno	k1gMnSc1	Bruno
Clevelandská	Clevelandský	k2eAgFnSc1d1	Clevelandská
kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
malá	malý	k2eAgFnSc1d1	malá
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
10	[number]	k4	10
členů	člen	k1gInPc2	člen
Dallaská	Dallaský	k2eAgFnSc1d1	Dallaská
kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
Denverská	denverský	k2eAgFnSc1d1	Denverská
kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
Milwaukee	Milwaukee	k1gFnSc1	Milwaukee
-	-	kIx~	-
malá	malý	k2eAgFnSc1d1	malá
skupina	skupina	k1gFnSc1	skupina
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
5	[number]	k4	5
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
15-20	[number]	k4	15-20
společníků	společník	k1gMnPc2	společník
Kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
New	New	k1gMnSc1	New
Orleans	Orleans	k1gInSc1	Orleans
-	-	kIx~	-
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zaniklou	zaniklý	k2eAgFnSc4d1	zaniklá
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
její	její	k3xOp3gFnSc1	její
skrytá	skrytý	k2eAgFnSc1d1	skrytá
aktivita	aktivita	k1gFnSc1	aktivita
Kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
Bufalino	Bufalin	k2eAgNnSc1d1	Bufalino
-	-	kIx~	-
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
Kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
-	-	kIx~	-
malá	malý	k2eAgFnSc1d1	malá
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
než	než	k8xS	než
5	[number]	k4	5
členů	člen	k1gInPc2	člen
Kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
Rochester	Rochestra	k1gFnPc2	Rochestra
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
Kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
San	San	k1gFnSc2	San
Francisko	Francisko	k1gNnSc4	Francisko
-	-	kIx~	-
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
Kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
San	San	k1gFnSc1	San
Jose	Jose	k1gFnSc1	Jose
-	-	kIx~	-
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
St.	st.	kA	st.
<g/>
Louis	louis	k1gInSc2	louis
-	-	kIx~	-
malá	malý	k2eAgFnSc1d1	malá
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
10	[number]	k4	10
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
35	[number]	k4	35
společníků	společník	k1gMnPc2	společník
Kriminální	kriminální	k2eAgFnSc1d1	kriminální
rodina	rodina	k1gFnSc1	rodina
Trafficante	Trafficant	k1gMnSc5	Trafficant
(	(	kIx(	(
<g/>
Florida	Florida	k1gFnSc1	Florida
<g/>
)	)	kIx)	)
-	-	kIx~	-
malá	malý	k2eAgFnSc1d1	malá
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
10	[number]	k4	10
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
rodinou	rodina	k1gFnSc7	rodina
Gambino	Gambina	k1gFnSc5	Gambina
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
mafie	mafie	k1gFnSc2	mafie
vydělává	vydělávat	k5eAaImIp3nS	vydělávat
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
EUR	euro	k1gNnPc2	euro
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Peníze	peníz	k1gInPc4	peníz
pak	pak	k6eAd1	pak
mafie	mafie	k1gFnSc2	mafie
dává	dávat	k5eAaImIp3nS	dávat
do	do	k7c2	do
normálního	normální	k2eAgInSc2d1	normální
oběhu	oběh	k1gInSc2	oběh
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	on	k3xPp3gMnPc4	on
legalizuje	legalizovat	k5eAaBmIp3nS	legalizovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jádru	jádro	k1gNnSc3	jádro
mafie	mafie	k1gFnSc2	mafie
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
patřit	patřit	k5eAaImF	patřit
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
000	[number]	k4	000
sympatizantů	sympatizant	k1gMnPc2	sympatizant
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mafie	mafie	k1gFnSc2	mafie
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
(	(	kIx(	(
<g/>
Cosa	Cos	k2eAgFnSc1d1	Cosa
Nostra	Nostra	k1gFnSc1	Nostra
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
mafiánských	mafiánský	k2eAgFnPc2d1	mafiánská
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Ruská	ruský	k2eAgFnSc1d1	ruská
mafie	mafie	k1gFnSc1	mafie
Triády	triáda	k1gFnSc2	triáda
(	(	kIx(	(
<g/>
Čína	Čína	k1gFnSc1	Čína
<g/>
)	)	kIx)	)
Turecká	turecký	k2eAgFnSc1d1	turecká
mafie	mafie	k1gFnSc1	mafie
Jakuza	Jakuza	k1gFnSc1	Jakuza
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
činnost	činnost	k1gFnSc4	činnost
kromě	kromě	k7c2	kromě
Cosa	Cosum	k1gNnSc2	Cosum
Nostry	Nostrum	k1gNnPc7	Nostrum
také	také	k9	také
Camorra	Camorra	k1gMnSc1	Camorra
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Ndrangheta	Ndranghet	k1gMnSc2	Ndranghet
<g/>
,	,	kIx,	,
Sacra	Sacr	k1gMnSc2	Sacr
Corona	Coron	k1gMnSc2	Coron
Unita	unita	k1gMnSc1	unita
a	a	k8xC	a
Stidda	Stidda	k1gMnSc1	Stidda
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
mafie	mafie	k1gFnSc1	mafie
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
pyramidové	pyramidový	k2eAgFnSc3d1	pyramidová
struktuře	struktura	k1gFnSc3	struktura
vedení	vedení	k1gNnSc2	vedení
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Capo	capa	k1gFnSc5	capa
di	di	k?	di
Capu	capat	k5eAaImIp1nS	capat
Re	re	k9	re
(	(	kIx(	(
<g/>
Král	Král	k1gMnSc1	Král
šéfa	šéf	k1gMnSc2	šéf
šéfů	šéf	k1gMnPc2	šéf
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
respektovaného	respektovaný	k2eAgNnSc2d1	respektované
starého	starý	k2eAgNnSc2d1	staré
nebo	nebo	k8xC	nebo
už	už	k6eAd1	už
vysloužilého	vysloužilý	k2eAgMnSc2d1	vysloužilý
člena	člen	k1gMnSc2	člen
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Capo	capa	k1gFnSc5	capa
di	di	k?	di
Tutti	tutti	k2eAgMnPc1d1	tutti
Capi	cap	k1gMnPc1	cap
(	(	kIx(	(
<g/>
Šéf	šéf	k1gMnSc1	šéf
šéfů	šéf	k1gMnPc2	šéf
<g/>
)	)	kIx)	)
Capo	capa	k1gFnSc5	capa
Crimini	Crimin	k2eAgMnPc1d1	Crimin
(	(	kIx(	(
<g/>
Kriminální	kriminální	k2eAgMnPc1d1	kriminální
šéf	šéf	k1gMnSc1	šéf
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
–	–	k?	–
hlava	hlava	k1gFnSc1	hlava
kriminální	kriminální	k2eAgFnSc2d1	kriminální
rodiny	rodina	k1gFnSc2	rodina
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Capo	capa	k1gFnSc5	capa
Bastone	Baston	k1gInSc5	Baston
(	(	kIx(	(
<g/>
Hlava	hlava	k1gFnSc1	hlava
bouchačů	bouchač	k1gMnPc2	bouchač
<g/>
,	,	kIx,	,
podšéf	podšéf	k1gMnSc1	podšéf
<g/>
,	,	kIx,	,
podřízený	podřízený	k2eAgMnSc1d1	podřízený
Capo	capa	k1gFnSc5	capa
Crimini	Crimin	k2eAgMnPc1d1	Crimin
<g/>
)	)	kIx)	)
Consigliere	Consiglier	k1gInSc5	Consiglier
(	(	kIx(	(
<g/>
Poradce	poradce	k1gMnSc1	poradce
<g/>
)	)	kIx)	)
Caporegime	Caporegim	k1gInSc5	Caporegim
(	(	kIx(	(
<g/>
Hlava	hlava	k1gFnSc1	hlava
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
velí	velet	k5eAaImIp3nS	velet
"	"	kIx"	"
<g/>
posádce	posádka	k1gFnSc3	posádka
<g/>
"	"	kIx"	"
asi	asi	k9	asi
deseti	deset	k4xCc7	deset
Sgarristi	Sgarrist	k1gMnPc1	Sgarrist
<g/>
)	)	kIx)	)
Sgarrista	Sgarrista	k1gMnSc1	Sgarrista
nebo	nebo	k8xC	nebo
Soldato	Soldat	k2eAgNnSc1d1	Soldato
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
mafii	mafie	k1gFnSc4	mafie
jako	jako	k8xC	jako
pěšáci	pěšák	k1gMnPc1	pěšák
<g/>
)	)	kIx)	)
Picciotto	Picciotto	k1gNnSc1	Picciotto
(	(	kIx(	(
<g/>
Malý	Malý	k1gMnSc1	Malý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
nízká	nízký	k2eAgFnSc1d1	nízká
hodnost	hodnost	k1gFnSc1	hodnost
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k6eAd1	především
jako	jako	k9	jako
"	"	kIx"	"
<g/>
vymahač	vymahač	k1gMnSc1	vymahač
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Giovane	Giovan	k1gMnSc5	Giovan
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Onore	Onor	k1gInSc5	Onor
(	(	kIx(	(
<g/>
Spřízněný	spřízněný	k2eAgMnSc1d1	spřízněný
člen	člen	k1gMnSc1	člen
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
bez	bez	k7c2	bez
sicilských	sicilský	k2eAgInPc2d1	sicilský
nebo	nebo	k8xC	nebo
italských	italský	k2eAgInPc2d1	italský
kořenů	kořen	k1gInPc2	kořen
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Capo	capa	k1gFnSc5	capa
di	di	k?	di
Tuti	tuti	k1gNnSc1	tuti
capi	cap	k1gMnPc1	cap
–	–	k?	–
(	(	kIx(	(
<g/>
Don	Don	k1gMnSc1	Don
<g/>
)	)	kIx)	)
Consigliere	Consiglier	k1gInSc5	Consiglier
–	–	k?	–
(	(	kIx(	(
<g/>
Kancléř	kancléř	k1gMnSc1	kancléř
nebo	nebo	k8xC	nebo
poradce	poradce	k1gMnSc1	poradce
<g/>
)	)	kIx)	)
Sotto	Sotto	k1gNnSc1	Sotto
Capo	capa	k1gFnSc5	capa
–	–	k?	–
(	(	kIx(	(
<g/>
Podšéf	podšéf	k1gMnSc1	podšéf
<g/>
)	)	kIx)	)
Caporegime	Caporegim	k1gInSc5	Caporegim
–	–	k?	–
(	(	kIx(	(
<g/>
Šéf	šéf	k1gMnSc1	šéf
skupin	skupina	k1gFnPc2	skupina
nebo	nebo	k8xC	nebo
kápo	kápo	k1gNnSc1	kápo
<g/>
)	)	kIx)	)
Uomino	Uomin	k2eAgNnSc1d1	Uomin
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
onore	onor	k1gInSc5	onor
–	–	k?	–
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Čestný	čestný	k2eAgMnSc1d1	čestný
muž	muž	k1gMnSc1	muž
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Boss	boss	k1gMnSc1	boss
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
–	–	k?	–
(	(	kIx(	(
<g/>
šéf	šéf	k1gMnSc1	šéf
<g/>
)	)	kIx)	)
Underboss	Underboss	k1gInSc1	Underboss
–	–	k?	–
(	(	kIx(	(
<g/>
Podšéf	podšéf	k1gMnSc1	podšéf
<g/>
)	)	kIx)	)
Consigliere	Consiglier	k1gInSc5	Consiglier
–	–	k?	–
(	(	kIx(	(
<g/>
Poradce	poradce	k1gMnSc1	poradce
<g/>
)	)	kIx)	)
Capo	capa	k1gFnSc5	capa
<g/>
,	,	kIx,	,
Captain	Captain	k1gMnSc1	Captain
–	–	k?	–
(	(	kIx(	(
<g/>
Kápo	kápo	k1gMnSc1	kápo
<g/>
,	,	kIx,	,
Kapitán	kapitán	k1gMnSc1	kapitán
<g/>
)	)	kIx)	)
Soldier	Soldier	k1gMnSc1	Soldier
–	–	k?	–
(	(	kIx(	(
<g/>
Klasický	klasický	k2eAgMnSc1d1	klasický
mafián	mafián	k1gMnSc1	mafián
<g/>
)	)	kIx)	)
Associate	Associat	k1gInSc5	Associat
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
(	(	kIx(	(
<g/>
Společník	společník	k1gMnSc1	společník
<g/>
,	,	kIx,	,
společníků	společník	k1gMnPc2	společník
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
nejvíce	hodně	k6eAd3	hodně
<g/>
)	)	kIx)	)
Outsider	outsider	k1gMnSc1	outsider
–	–	k?	–
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
<g/>
)	)	kIx)	)
Mario	Mario	k1gMnSc1	Mario
Puzo	Puzo	k1gMnSc1	Puzo
–	–	k?	–
Kmotr	kmotr	k1gMnSc1	kmotr
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
o	o	k7c6	o
mafiánském	mafiánský	k2eAgInSc6d1	mafiánský
klanu	klan	k1gInSc6	klan
rodiny	rodina	k1gFnSc2	rodina
Corleone	Corleon	k1gInSc5	Corleon
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
natočil	natočit	k5eAaBmAgInS	natočit
Francis	Francis	k1gInSc1	Francis
Ford	ford	k1gInSc1	ford
Coppola	Coppola	k1gFnSc1	Coppola
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Marlon	Marlon	k1gInSc1	Marlon
Brando	Brando	k6eAd1	Brando
v	v	k7c6	v
roli	role	k1gFnSc6	role
kmotra	kmotr	k1gMnSc2	kmotr
a	a	k8xC	a
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc1	Pacino
jako	jako	k8xS	jako
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
i	i	k9	i
zpracování	zpracování	k1gNnSc4	zpracování
do	do	k7c2	do
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Godfather	Godfathra	k1gFnPc2	Godfathra
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Game	game	k1gInSc1	game
<g/>
)	)	kIx)	)
Počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
Mafia	Mafia	k1gFnSc1	Mafia
od	od	k7c2	od
Illusion	Illusion	k1gInSc4	Illusion
Softworks	Softworksa	k1gFnPc2	Softworksa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
a	a	k8xC	a
Mafia	Mafium	k1gNnSc2	Mafium
II	II	kA	II
od	od	k7c2	od
2K	[number]	k4	2K
games	gamesa	k1gFnPc2	gamesa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Filmy	film	k1gInPc1	film
Příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
Bronxu	Bronx	k1gInSc2	Bronx
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mafiáni	mafián	k1gMnPc1	mafián
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Neúplatní	úplatný	k2eNgMnPc1d1	neúplatný
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Casino	Casino	k1gNnSc1	Casino
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tenkrát	tenkrát	k6eAd1	tenkrát
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Krycí	krycí	k2eAgNnSc4d1	krycí
jméno	jméno	k1gNnSc4	jméno
Donnie	Donnie	k1gFnSc2	Donnie
Brasco	Brasco	k1gMnSc1	Brasco
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
zatracení	zatracení	k1gNnSc2	zatracení
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
