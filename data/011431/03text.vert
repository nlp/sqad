<p>
<s>
Chang-čou	Chang-čou	k1gNnSc1	Chang-čou
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
杭	杭	k?	杭
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gMnSc7	pchin-jin
Hángzhō	Hángzhō	k1gMnSc2	Hángzhō
Wā	Wā	k1gMnSc2	Wā
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zátoka	zátoka	k1gFnSc1	zátoka
Východočínského	východočínský	k2eAgNnSc2d1	Východočínské
moře	moře	k1gNnSc2	moře
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
Šanghaje	Šanghaj	k1gFnSc2	Šanghaj
a	a	k8xC	a
provincie	provincie	k1gFnSc2	provincie
Če-ťiang	Če-ťianga	k1gFnPc2	Če-ťianga
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
řeka	řeka	k1gFnSc1	řeka
Čchien-tchang	Čchienchang	k1gInSc4	Čchien-tchang
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
rekordně	rekordně	k6eAd1	rekordně
veliké	veliký	k2eAgFnPc1d1	veliká
přílivové	přílivový	k2eAgFnPc1d1	přílivová
vlny	vlna	k1gFnPc1	vlna
–	–	k?	–
vysoké	vysoká	k1gFnSc2	vysoká
až	až	k6eAd1	až
devět	devět	k4xCc1	devět
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
rychlé	rychlý	k2eAgInPc1d1	rychlý
až	až	k9	až
40	[number]	k4	40
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
přes	přes	k7c4	přes
Chang-čou	Chang-čou	k1gNnSc4	Chang-čou
otevřen	otevřen	k2eAgInSc1d1	otevřen
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdelších	dlouhý	k2eAgInPc2d3	nejdelší
mostů	most	k1gInPc2	most
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
cestu	cesta	k1gFnSc4	cesta
mezi	mezi	k7c7	mezi
východním	východní	k2eAgInSc7d1	východní
Če-ťiangem	Če-ťiang	k1gInSc7	Če-ťiang
a	a	k8xC	a
Šanghají	Šanghaj	k1gFnSc7	Šanghaj
ze	z	k7c2	z
zhruba	zhruba	k6eAd1	zhruba
400	[number]	k4	400
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hangzhou	Hangzha	k1gMnSc7	Hangzha
Bay	Bay	k1gMnSc7	Bay
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chang-čou	Chang-čou	k1gNnSc2	Chang-čou
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
