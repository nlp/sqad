<s>
Josef	Josef	k1gMnSc1	Josef
Daněk	Daněk	k1gMnSc1	Daněk
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1920	[number]	k4	1920
Kotvrdovice	Kotvrdovice	k1gFnSc2	Kotvrdovice
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
Třemošnice	Třemošnice	k1gFnSc2	Třemošnice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
technik	technik	k1gMnSc1	technik
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
československou	československý	k2eAgFnSc4d1	Československá
vlakovou	vlakový	k2eAgFnSc4d1	vlaková
pneumatickou	pneumatický	k2eAgFnSc4d1	pneumatická
brzdu	brzda	k1gFnSc4	brzda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	on	k3xPp3gInSc2	on
úspěchu	úspěch	k1gInSc2	úspěch
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
firma	firma	k1gFnSc1	firma
DAKO	DAKO	kA	DAKO
v	v	k7c6	v
Třemošnici	Třemošnice	k1gFnSc6	Třemošnice
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Daněk	Daněk	k1gMnSc1	Daněk
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1920	[number]	k4	1920
v	v	k7c6	v
Kotvrdovicích	Kotvrdovice	k1gFnPc6	Kotvrdovice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
Vyučil	vyučit	k5eAaPmAgMnS	vyučit
se	s	k7c7	s
soustružníkem	soustružník	k1gMnSc7	soustružník
a	a	k8xC	a
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
Vyšší	vysoký	k2eAgFnSc4d2	vyšší
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
školu	škola	k1gFnSc4	škola
strojnickou	strojnický	k2eAgFnSc4d1	strojnická
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
technik	technik	k1gMnSc1	technik
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
vlakových	vlakový	k2eAgFnPc2d1	vlaková
brzd	brzda	k1gFnPc2	brzda
typu	typ	k1gInSc2	typ
Božič	Božič	k1gInSc4	Božič
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
Škoda	škoda	k1gFnSc1	škoda
-	-	kIx~	-
Adamov	Adamov	k1gInSc1	Adamov
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
tato	tento	k3xDgFnSc1	tento
výroba	výroba	k1gFnSc1	výroba
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
do	do	k7c2	do
Třemošnice	Třemošnice	k1gFnSc2	Třemošnice
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
také	také	k9	také
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
29	[number]	k4	29
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
socialistickém	socialistický	k2eAgInSc6d1	socialistický
závazku	závazek	k1gInSc6	závazek
podniku	podnik	k1gInSc6	podnik
Kovolis	Kovolis	k1gFnSc7	Kovolis
Třemošnice	Třemošnice	k1gFnSc2	Třemošnice
vyvinout	vyvinout	k5eAaPmF	vyvinout
vlastní	vlastní	k2eAgFnSc4d1	vlastní
československou	československý	k2eAgFnSc4d1	Československá
brzdu	brzda	k1gFnSc4	brzda
pro	pro	k7c4	pro
trhy	trh	k1gInPc4	trh
RVHP	RVHP	kA	RVHP
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgInS	pověřit
vedením	vedení	k1gNnSc7	vedení
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
tento	tento	k3xDgInSc4	tento
závazek	závazek	k1gInSc4	závazek
splnit	splnit	k5eAaPmF	splnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
první	první	k4xOgFnSc1	první
série	série	k1gFnSc1	série
rozváděčů	rozváděč	k1gMnPc2	rozváděč
DAKO	DAKO	kA	DAKO
C	C	kA	C
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
předvedeny	předveden	k2eAgInPc4d1	předveden
brzdové	brzdový	k2eAgInPc4d1	brzdový
subkomisi	subkomise	k1gFnSc6	subkomise
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
železniční	železniční	k2eAgFnSc2d1	železniční
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
UIC	UIC	kA	UIC
<g/>
)	)	kIx)	)
a	a	k8xC	a
brzdová	brzdový	k2eAgFnSc1d1	brzdová
subkomise	subkomise	k1gFnSc1	subkomise
připustila	připustit	k5eAaPmAgFnS	připustit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
rozváděč	rozváděč	k1gInSc1	rozváděč
DAKO	DAKO	kA	DAKO
C	C	kA	C
k	k	k7c3	k
užití	užití	k1gNnSc3	užití
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc2	průběh
zkoušek	zkouška	k1gFnPc2	zkouška
však	však	k9	však
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
rozváděči	rozváděč	k1gInSc6	rozváděč
DAKO	DAKO	kA	DAKO
C	C	kA	C
provedeny	proveden	k2eAgFnPc4d1	provedena
úpravy	úprava	k1gFnPc4	úprava
vedoucí	vedoucí	k2eAgFnPc1d1	vedoucí
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
jeho	jeho	k3xOp3gFnSc2	jeho
provozní	provozní	k2eAgFnSc2d1	provozní
spolehlivosti	spolehlivost	k1gFnSc2	spolehlivost
a	a	k8xC	a
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
předvedeny	předveden	k2eAgInPc1d1	předveden
rozváděče	rozváděč	k1gInPc1	rozváděč
DAKO	DAKO	kA	DAKO
CV	CV	kA	CV
a	a	k8xC	a
DAKO	DAKO	kA	DAKO
CV-	CV-	k1gFnSc1	CV-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
brzdovou	brzdový	k2eAgFnSc7d1	brzdová
subkomisí	subkomise	k1gFnSc7	subkomise
schváleny	schválen	k2eAgFnPc1d1	schválena
jako	jako	k8xS	jako
odvozeniny	odvozenina	k1gFnPc1	odvozenina
již	již	k6eAd1	již
schválené	schválený	k2eAgFnPc1d1	schválená
brzdy	brzda	k1gFnPc1	brzda
DAKO	DAKO	kA	DAKO
C.	C.	kA	C.
Mimochodem	mimochodem	k9	mimochodem
<g/>
:	:	kIx,	:
zkoušky	zkouška	k1gFnPc4	zkouška
rozváděčů	rozváděč	k1gInPc2	rozváděč
DAKO	DAKO	kA	DAKO
C	C	kA	C
<g/>
,	,	kIx,	,
DAKO	DAKO	kA	DAKO
CV	CV	kA	CV
a	a	k8xC	a
DAKO	DAKO	kA	DAKO
CV-1	CV-1	k1gFnPc4	CV-1
se	se	k3xPyFc4	se
prováděly	provádět	k5eAaImAgFnP	provádět
i	i	k9	i
na	na	k7c6	na
vlacích	vlak	k1gInPc6	vlak
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
vozech	vůz	k1gInPc6	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábět	vyrábět	k5eAaImF	vyrábět
se	se	k3xPyFc4	se
však	však	k9	však
začal	začít	k5eAaPmAgMnS	začít
pouze	pouze	k6eAd1	pouze
rozváděč	rozváděč	k1gMnSc1	rozváděč
DAKO	DAKO	kA	DAKO
CV-	CV-	k1gFnSc1	CV-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
aktivitu	aktivita	k1gFnSc4	aktivita
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
Daněk	Daněk	k1gMnSc1	Daněk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
odměněn	odměnit	k5eAaPmNgInS	odměnit
Státní	státní	k2eAgFnSc7d1	státní
cenou	cena	k1gFnSc7	cena
Klementa	Klement	k1gMnSc2	Klement
Gottwalda	Gottwald	k1gMnSc2	Gottwald
<g/>
.	.	kIx.	.
</s>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
vývojových	vývojový	k2eAgMnPc2d1	vývojový
pracovníků	pracovník	k1gMnPc2	pracovník
Ing.	ing.	kA	ing.
Miloš	Miloš	k1gMnSc1	Miloš
Rais	Rais	k1gMnSc1	Rais
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Špatenka	špatenka	k1gMnSc1	špatenka
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Holub	Holub	k1gMnSc1	Holub
<g/>
,	,	kIx,	,
lng	lng	k?	lng
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Kříž	Kříž	k1gMnSc1	Kříž
a	a	k8xC	a
Ing.	ing.	kA	ing.
Bohumil	Bohumil	k1gMnSc1	Bohumil
Fořt	Fořt	k1gMnSc1	Fořt
byli	být	k5eAaImAgMnP	být
oceněni	ocenit	k5eAaPmNgMnP	ocenit
Řádem	řád	k1gInSc7	řád
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
tento	tento	k3xDgInSc1	tento
kolektiv	kolektiv	k1gInSc1	kolektiv
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
rychlíkovou	rychlíkový	k2eAgFnSc7d1	rychlíková
brzdou	brzda	k1gFnSc7	brzda
DAKO	DAKO	kA	DAKO
R.	R.	kA	R.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
již	již	k9	již
byla	být	k5eAaImAgFnS	být
železniční	železniční	k2eAgFnSc1d1	železniční
brzda	brzda	k1gFnSc1	brzda
DAKO	DAKO	kA	DAKO
vyráběna	vyráběn	k2eAgFnSc1d1	vyráběna
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
závodě	závod	k1gInSc6	závod
v	v	k7c6	v
Třemošnici	Třemošnice	k1gFnSc6	Třemošnice
u	u	k7c2	u
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
pojmenován	pojmenován	k2eAgInSc4d1	pojmenován
DAKO	DAKO	kA	DAKO
(	(	kIx(	(
<g/>
pojmenování	pojmenování	k1gNnSc1	pojmenování
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k8xC	jako
zkratka	zkratka	k1gFnSc1	zkratka
počátečních	počáteční	k2eAgNnPc2d1	počáteční
písmen	písmeno	k1gNnPc2	písmeno
tvůrce	tvůrce	k1gMnSc2	tvůrce
brzdy	brzda	k1gFnSc2	brzda
Daňka	Daněk	k1gMnSc2	Daněk
a	a	k8xC	a
závodu	závod	k1gInSc2	závod
KOVOLIS	KOVOLIS	kA	KOVOLIS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzda	brzda	k1gFnSc1	brzda
má	mít	k5eAaImIp3nS	mít
unikátní	unikátní	k2eAgFnPc4d1	unikátní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
využil	využít	k5eAaPmAgInS	využít
tým	tým	k1gInSc4	tým
vývojářů	vývojář	k1gMnPc2	vývojář
DAKO-CZ	DAKO-CZ	k1gMnSc1	DAKO-CZ
odkazu	odkaz	k1gInSc2	odkaz
(	(	kIx(	(
<g/>
a	a	k8xC	a
ještě	ještě	k9	ještě
i	i	k9	i
konzultací	konzultace	k1gFnPc2	konzultace
<g/>
)	)	kIx)	)
p.	p.	k?	p.
Daňka	Daněk	k1gMnSc2	Daněk
v	v	k7c6	v
modifikovaném	modifikovaný	k2eAgInSc6d1	modifikovaný
rozvaděči	rozvaděč	k1gInSc6	rozvaděč
CV-	CV-	k1gFnSc1	CV-
<g/>
1	[number]	k4	1
<g/>
nP	nP	k?	nP
<g/>
.	.	kIx.	.
</s>
<s>
Opětovná	opětovný	k2eAgFnSc1d1	opětovná
homologace	homologace	k1gFnSc1	homologace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
před	před	k7c7	před
komisí	komise	k1gFnSc7	komise
UIC	UIC	kA	UIC
<g/>
,	,	kIx,	,
složené	složený	k2eAgFnSc6d1	složená
z	z	k7c2	z
cca	cca	kA	cca
20	[number]	k4	20
reprezentantů	reprezentant	k1gMnPc2	reprezentant
národních	národní	k2eAgFnPc2d1	národní
železnic	železnice	k1gFnPc2	železnice
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
tenkráte	tenkráte	k?	tenkráte
sešla	sejít	k5eAaPmAgFnS	sejít
a	a	k8xC	a
zkoušela	zkoušet	k5eAaImAgFnS	zkoušet
charakteristiky	charakteristika	k1gFnPc4	charakteristika
rozváděče	rozváděč	k1gInSc2	rozváděč
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Třemošnici	Třemošnice	k1gFnSc6	Třemošnice
<g/>
.	.	kIx.	.
</s>
<s>
Rozváděč	rozváděč	k1gMnSc1	rozváděč
DAKO	DAKO	kA	DAKO
CV	CV	kA	CV
<g/>
1	[number]	k4	1
<g/>
nP	nP	k?	nP
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
součásti	součást	k1gFnPc1	součást
brzdové	brzdový	k2eAgFnSc2d1	brzdová
výstroje	výstroj	k1gFnSc2	výstroj
DAKO	DAKO	kA	DAKO
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
samočinné	samočinný	k2eAgNnSc4d1	samočinné
brzdění	brzdění	k1gNnSc4	brzdění
nákladu	náklad	k1gInSc2	náklad
<g/>
,	,	kIx,	,
protismykové	protismykový	k2eAgNnSc1d1	protismykový
zařízení	zařízení	k1gNnSc1	zařízení
či	či	k8xC	či
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
kolejnicová	kolejnicový	k2eAgFnSc1d1	kolejnicová
brzda	brzda	k1gFnSc1	brzda
<g/>
)	)	kIx)	)
splňují	splňovat	k5eAaImIp3nP	splňovat
i	i	k9	i
požadavky	požadavek	k1gInPc1	požadavek
Technických	technický	k2eAgFnPc2d1	technická
specifikací	specifikace	k1gFnPc2	specifikace
pro	pro	k7c4	pro
interoperabilitu	interoperabilita	k1gFnSc4	interoperabilita
(	(	kIx(	(
<g/>
TSI	TSI	kA	TSI
<g/>
)	)	kIx)	)
a	a	k8xC	a
příslušných	příslušný	k2eAgFnPc2d1	příslušná
evropských	evropský	k2eAgFnPc2d1	Evropská
norem	norma	k1gFnPc2	norma
resp.	resp.	kA	resp.
vyhlášek	vyhláška	k1gFnPc2	vyhláška
UIC	UIC	kA	UIC
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Daněk	Daněk	k1gMnSc1	Daněk
dožil	dožít	k5eAaPmAgMnS	dožít
svá	svůj	k3xOyFgNnPc4	svůj
poslední	poslední	k2eAgNnPc4d1	poslední
léta	léto	k1gNnPc4	léto
v	v	k7c6	v
Třemošnici	Třemošnice	k1gFnSc6	Třemošnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemřel	zemřít	k5eAaPmAgMnS	zemřít
náhle	náhle	k6eAd1	náhle
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
81	[number]	k4	81
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
