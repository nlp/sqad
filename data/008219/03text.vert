<p>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
hlavou	hlava	k1gFnSc7	hlava
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Kramářově	kramářův	k2eAgFnSc6d1	Kramářova
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
Andrej	Andrej	k1gMnSc1	Andrej
Babiš	Babiš	k1gMnSc1	Babiš
z	z	k7c2	z
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
dvakrát	dvakrát	k6eAd1	dvakrát
neuspěje	uspět	k5eNaPmIp3nS	uspět
při	při	k7c6	při
sestavení	sestavení	k1gNnSc6	sestavení
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
vláda	vláda	k1gFnSc1	vláda
nezíská	získat	k5eNaPmIp3nS	získat
důvěru	důvěra	k1gFnSc4	důvěra
parlamentu	parlament	k1gInSc2	parlament
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
třetí	třetí	k4xOgInSc4	třetí
pokus	pokus	k1gInSc4	pokus
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
předsedu	předseda	k1gMnSc4	předseda
na	na	k7c6	na
základě	základ	k1gInSc6	základ
návrhu	návrh	k1gInSc2	návrh
předsedy	předseda	k1gMnSc2	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
podává	podávat	k5eAaImIp3nS	podávat
demisi	demise	k1gFnSc4	demise
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
demise	demise	k1gFnSc1	demise
znamená	znamenat	k5eAaImIp3nS	znamenat
zároveň	zároveň	k6eAd1	zároveň
demisi	demise	k1gFnSc4	demise
celé	celý	k2eAgFnSc2d1	celá
vlády	vláda	k1gFnSc2	vláda
jen	jen	k9	jen
podle	podle	k7c2	podle
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
ústavní	ústavní	k2eAgFnSc2d1	ústavní
zvyklosti	zvyklost	k1gFnSc2	zvyklost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
klíčů	klíč	k1gInPc2	klíč
od	od	k7c2	od
dveří	dveře	k1gFnPc2	dveře
do	do	k7c2	do
Korunní	korunní	k2eAgFnSc2d1	korunní
komory	komora	k1gFnSc2	komora
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
uloženy	uložen	k2eAgInPc4d1	uložen
české	český	k2eAgInPc4d1	český
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kompetence	kompetence	k1gFnSc2	kompetence
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
organizuje	organizovat	k5eAaBmIp3nS	organizovat
činnost	činnost	k1gFnSc4	činnost
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
její	její	k3xOp3gFnSc1	její
schůze	schůze	k1gFnSc1	schůze
a	a	k8xC	a
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jejím	její	k3xOp3gNnSc7	její
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Vykonává	vykonávat	k5eAaImIp3nS	vykonávat
rovněž	rovněž	k9	rovněž
další	další	k2eAgFnPc4d1	další
činnosti	činnost	k1gFnPc4	činnost
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
svěří	svěřit	k5eAaPmIp3nS	svěřit
ústava	ústava	k1gFnSc1	ústava
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgInPc1d1	jiný
zákony	zákon	k1gInPc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Spolupodepisuje	spolupodepisovat	k5eAaImIp3nS	spolupodepisovat
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
zákonná	zákonný	k2eAgNnPc4d1	zákonné
opatření	opatření	k1gNnPc4	opatření
Senátu	senát	k1gInSc2	senát
a	a	k8xC	a
nařízení	nařízení	k1gNnSc2	nařízení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
návrh	návrh	k1gInSc4	návrh
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
další	další	k2eAgMnPc4d1	další
členy	člen	k1gMnPc4	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
také	také	k9	také
podávají	podávat	k5eAaImIp3nP	podávat
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
vlády	vláda	k1gFnSc2	vláda
demisi	demise	k1gFnSc4	demise
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
kontrasignuje	kontrasignovat	k5eAaBmIp3nS	kontrasignovat
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
63	[number]	k4	63
a	a	k8xC	a
nese	nést	k5eAaImIp3nS	nést
za	za	k7c4	za
ně	on	k3xPp3gInPc4	on
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
vládou	vláda	k1gFnSc7	vláda
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Kontrasignací	kontrasignace	k1gFnSc7	kontrasignace
může	moct	k5eAaImIp3nS	moct
pověřit	pověřit	k5eAaPmF	pověřit
jiného	jiný	k2eAgMnSc4d1	jiný
člena	člen	k1gMnSc4	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
uvolnění	uvolnění	k1gNnSc4	uvolnění
úřadu	úřad	k1gInSc2	úřad
prezidenta	prezident	k1gMnSc2	prezident
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
nemůže	moct	k5eNaImIp3nS	moct
prezident	prezident	k1gMnSc1	prezident
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
ze	z	k7c2	z
závažných	závažný	k2eAgInPc2d1	závažný
důvodů	důvod	k1gInPc2	důvod
vykonávat	vykonávat	k5eAaImF	vykonávat
<g/>
,	,	kIx,	,
přísluší	příslušet	k5eAaImIp3nS	příslušet
mu	on	k3xPp3gMnSc3	on
výkon	výkon	k1gInSc4	výkon
některých	některý	k3yIgFnPc2	některý
funkcí	funkce	k1gFnPc2	funkce
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
63	[number]	k4	63
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zastupování	zastupování	k1gNnSc1	zastupování
státu	stát	k1gInSc2	stát
navenek	navenek	k6eAd1	navenek
</s>
</p>
<p>
<s>
sjednávání	sjednávání	k1gNnSc1	sjednávání
a	a	k8xC	a
ratifikace	ratifikace	k1gFnSc1	ratifikace
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
</s>
</p>
<p>
<s>
přijímání	přijímání	k1gNnSc1	přijímání
vedoucích	vedoucí	k2eAgFnPc2d1	vedoucí
zastupitelských	zastupitelský	k2eAgFnPc2d1	zastupitelská
misí	mise	k1gFnPc2	mise
</s>
</p>
<p>
<s>
pověřování	pověřování	k1gNnSc1	pověřování
a	a	k8xC	a
odvolávání	odvolávání	k1gNnSc1	odvolávání
vedoucích	vedoucí	k2eAgFnPc2d1	vedoucí
zastupitelských	zastupitelský	k2eAgFnPc2d1	zastupitelská
misí	mise	k1gFnPc2	mise
</s>
</p>
<p>
<s>
propůjčování	propůjčování	k1gNnSc4	propůjčování
a	a	k8xC	a
udělování	udělování	k1gNnSc4	udělování
státních	státní	k2eAgNnPc2d1	státní
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
</s>
</p>
<p>
<s>
jmenování	jmenování	k1gNnSc1	jmenování
soudců	soudce	k1gMnPc2	soudce
</s>
</p>
<p>
<s>
nařízení	nařízení	k1gNnSc1	nařízení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
trestní	trestní	k2eAgNnSc1d1	trestní
řízení	řízení	k1gNnSc1	řízení
nezahajovalo	zahajovat	k5eNaImAgNnS	zahajovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
<g/>
-li	i	k?	-li
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nepokračovalo	pokračovat	k5eNaImAgNnS	pokračovat
</s>
</p>
<p>
<s>
udělování	udělování	k1gNnSc4	udělování
amnestie	amnestie	k1gFnSc2	amnestie
</s>
</p>
<p>
<s>
výkon	výkon	k1gInSc1	výkon
pravomocí	pravomoc	k1gFnPc2	pravomoc
neuvedených	uvedený	k2eNgFnPc2d1	neuvedená
v	v	k7c6	v
ústavních	ústavní	k2eAgInPc6d1	ústavní
zákonech	zákon	k1gInPc6	zákon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
zákonech	zákon	k1gInPc6	zákon
</s>
</p>
<p>
<s>
==	==	k?	==
Postavení	postavení	k1gNnSc1	postavení
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
silná	silný	k2eAgFnSc1d1	silná
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
vláda	vláda	k1gFnSc1	vláda
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
ve	v	k7c6	v
sboru	sbor	k1gInSc6	sbor
<g/>
,	,	kIx,	,
směřování	směřování	k1gNnSc1	směřování
politiky	politika	k1gFnSc2	politika
určuje	určovat	k5eAaImIp3nS	určovat
její	její	k3xOp3gMnSc1	její
předseda	předseda	k1gMnSc1	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nesmí	smět	k5eNaImIp3nS	smět
přímo	přímo	k6eAd1	přímo
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
činnost	činnost	k1gFnSc4	činnost
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
dokládá	dokládat	k5eAaImIp3nS	dokládat
zejména	zejména	k9	zejména
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
návrh	návrh	k1gInSc4	návrh
jsou	být	k5eAaImIp3nP	být
jmenováni	jmenovat	k5eAaImNgMnP	jmenovat
a	a	k8xC	a
odvoláváni	odvoláván	k2eAgMnPc1d1	odvoláván
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
fakticky	fakticky	k6eAd1	fakticky
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
obvykle	obvykle	k6eAd1	obvykle
omezen	omezit	k5eAaPmNgInS	omezit
koaliční	koaliční	k2eAgFnSc7d1	koaliční
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
<g/>
Přestože	přestože	k8xS	přestože
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
není	být	k5eNaImIp3nS	být
opora	opora	k1gFnSc1	opora
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
demise	demise	k1gFnSc1	demise
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
zavedené	zavedený	k2eAgFnSc2d1	zavedená
praxe	praxe	k1gFnSc2	praxe
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
demisi	demise	k1gFnSc4	demise
celé	celý	k2eAgFnSc2d1	celá
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
názor	názor	k1gInSc1	názor
je	být	k5eAaImIp3nS	být
také	také	k9	také
obecně	obecně	k6eAd1	obecně
přijímán	přijímat	k5eAaImNgInS	přijímat
v	v	k7c6	v
právní	právní	k2eAgFnSc6d1	právní
teorii	teorie	k1gFnSc6	teorie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
někteří	některý	k3yIgMnPc1	některý
právníci	právník	k1gMnPc1	právník
jej	on	k3xPp3gInSc4	on
nesdílejí	sdílet	k5eNaImIp3nP	sdílet
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
podle	podle	k7c2	podle
ústavního	ústavní	k2eAgMnSc2d1	ústavní
právníka	právník	k1gMnSc2	právník
Jana	Jan	k1gMnSc2	Jan
Filipa	Filip	k1gMnSc2	Filip
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
demise	demise	k1gFnSc1	demise
vlády	vláda	k1gFnSc2	vláda
zásadním	zásadní	k2eAgNnSc7d1	zásadní
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
celé	celý	k2eAgFnSc2d1	celá
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
nový	nový	k2eAgMnSc1d1	nový
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
by	by	kYmCp3nS	by
navíc	navíc	k6eAd1	navíc
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
dostatek	dostatek	k1gInSc1	dostatek
možností	možnost	k1gFnPc2	možnost
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
potřebných	potřebný	k2eAgFnPc2d1	potřebná
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
podle	podle	k7c2	podle
výslovného	výslovný	k2eAgNnSc2d1	výslovné
znění	znění	k1gNnSc2	znění
ústavy	ústava	k1gFnSc2	ústava
podat	podat	k5eAaPmF	podat
demisi	demise	k1gFnSc4	demise
a	a	k8xC	a
současně	současně	k6eAd1	současně
dát	dát	k5eAaPmF	dát
prezidentovi	prezident	k1gMnSc3	prezident
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
odvolání	odvolání	k1gNnSc4	odvolání
ostatních	ostatní	k2eAgInPc2d1	ostatní
členů	člen	k1gInPc2	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
při	při	k7c6	při
vládní	vládní	k2eAgFnSc6d1	vládní
krizi	krize	k1gFnSc6	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
podal	podat	k5eAaPmAgMnS	podat
premiér	premiér	k1gMnSc1	premiér
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
demisi	demise	k1gFnSc4	demise
s	s	k7c7	s
dovětkem	dovětek	k1gInSc7	dovětek
o	o	k7c4	o
demisi	demise	k1gFnSc4	demise
celé	celý	k2eAgFnSc2d1	celá
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
s	s	k7c7	s
demisemi	demise	k1gFnPc7	demise
některých	některý	k3yIgMnPc2	některý
dalších	další	k2eAgMnPc2d1	další
premiérů	premiér	k1gMnPc2	premiér
nakládáno	nakládat	k5eAaImNgNnS	nakládat
jako	jako	k8xC	jako
s	s	k7c7	s
demisemi	demise	k1gFnPc7	demise
celých	celý	k2eAgFnPc2d1	celá
vlád	vláda	k1gFnPc2	vláda
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
ústavní	ústavní	k2eAgFnSc1d1	ústavní
zvyklost	zvyklost	k1gFnSc1	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
Výslovně	výslovně	k6eAd1	výslovně
ji	on	k3xPp3gFnSc4	on
zakotvit	zakotvit	k5eAaPmF	zakotvit
do	do	k7c2	do
českého	český	k2eAgInSc2d1	český
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozsáhlejší	rozsáhlý	k2eAgFnSc7d2	rozsáhlejší
novelizací	novelizace	k1gFnSc7	novelizace
Ústavy	ústava	k1gFnSc2	ústava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
návrh	návrh	k1gInSc4	návrh
ale	ale	k8xC	ale
tehdy	tehdy	k6eAd1	tehdy
neprošel	projít	k5eNaPmAgInS	projít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ústava	ústava	k1gFnSc1	ústava
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
jmenován	jmenován	k2eAgMnSc1d1	jmenován
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	můj	k3xOp1gFnSc1	můj
země	země	k1gFnSc1	země
dva	dva	k4xCgInPc4	dva
úřadující	úřadující	k2eAgMnPc4d1	úřadující
předsedy	předseda	k1gMnPc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
svou	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
hledí	hledět	k5eAaImIp3nS	hledět
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
na	na	k7c4	na
designovaného	designovaný	k2eAgMnSc4d1	designovaný
<g/>
.	.	kIx.	.
</s>
<s>
Pravomoci	pravomoc	k1gFnPc1	pravomoc
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
tudíž	tudíž	k8xC	tudíž
plní	plnit	k5eAaImIp3nS	plnit
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pověřením	pověření	k1gNnSc7	pověření
od	od	k7c2	od
prezidenta	prezident	k1gMnSc2	prezident
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
funkce	funkce	k1gFnSc2	funkce
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
předseda	předseda	k1gMnSc1	předseda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
premiérů	premiér	k1gMnPc2	premiér
Česka	Česko	k1gNnSc2	Česko
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
manželek	manželka	k1gFnPc2	manželka
premiérů	premiér	k1gMnPc2	premiér
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
