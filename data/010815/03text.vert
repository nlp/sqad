<p>
<s>
Har	Har	k?	Har
Achina	Achina	k1gFnSc1	Achina
<g/>
'	'	kIx"	'
<g/>
am	am	k?	am
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ה	ה	k?	ה
א	א	k?	א
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
o	o	k7c6	o
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
451	[number]	k4	451
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Gilboa	Gilbo	k1gInSc2	Gilbo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
pohoří	pohořet	k5eAaPmIp3nS	pohořet
Gilboa	Gilboa	k1gFnSc1	Gilboa
<g/>
,	,	kIx,	,
cca	cca	kA	cca
8	[number]	k4	8
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
Bejt	Bejt	k?	Bejt
Še	Še	k1gFnSc1	Še
<g/>
'	'	kIx"	'
<g/>
an	an	k?	an
a	a	k8xC	a
3	[number]	k4	3
kilometry	kilometr	k1gInPc7	kilometr
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
vesnice	vesnice	k1gFnSc2	vesnice
Ma	Ma	k1gFnSc2	Ma
<g/>
'	'	kIx"	'
<g/>
ale	ale	k8xC	ale
Gilboa	Gilboa	k1gFnSc1	Gilboa
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
výrazného	výrazný	k2eAgNnSc2d1	výrazné
návrší	návrší	k1gNnSc2	návrší
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
zalesněnou	zalesněný	k2eAgFnSc7d1	zalesněná
vrcholovou	vrcholový	k2eAgFnSc7d1	vrcholová
partií	partie	k1gFnSc7	partie
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
vrcholku	vrcholek	k1gInSc2	vrcholek
vede	vést	k5eAaImIp3nS	vést
lokální	lokální	k2eAgFnSc1d1	lokální
silnice	silnice	k1gFnSc1	silnice
667	[number]	k4	667
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
terén	terén	k1gInSc1	terén
prudce	prudko	k6eAd1	prudko
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
po	po	k7c6	po
odlesněných	odlesněný	k2eAgInPc6d1	odlesněný
svazích	svah	k1gInPc6	svah
<g/>
,	,	kIx,	,
do	do	k7c2	do
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívaného	využívaný	k2eAgMnSc2d1	využívaný
Bejtše	Bejtš	k1gMnSc2	Bejtš
<g/>
'	'	kIx"	'
<g/>
anského	anský	k2eAgMnSc2d1	anský
a	a	k8xC	a
Charodského	Charodský	k2eAgNnSc2d1	Charodský
údolí	údolí	k1gNnSc2	údolí
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
odtud	odtud	k6eAd1	odtud
klesá	klesat	k5eAaImIp3nS	klesat
také	také	k9	také
vádí	vádí	k1gNnSc1	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Gefet	Gefet	k1gMnSc1	Gefet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
odtud	odtud	k6eAd1	odtud
stojí	stát	k5eAaImIp3nS	stát
sousední	sousední	k2eAgInSc4d1	sousední
vrchol	vrchol	k1gInSc4	vrchol
Har	Har	k1gMnSc1	Har
Jicpor	Jicpor	k1gMnSc1	Jicpor
<g/>
,	,	kIx,	,
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vrch	vrch	k1gInSc1	vrch
Har	Har	k1gFnSc2	Har
Barkan	Barkany	k1gInPc2	Barkany
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Har	Har	k1gMnSc1	Har
Gefet	Gefet	k1gMnSc1	Gefet
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jihozápadních	jihozápadní	k2eAgInPc6d1	jihozápadní
svazích	svah	k1gInPc6	svah
hory	hora	k1gFnSc2	hora
vede	vést	k5eAaImIp3nS	vést
izraelská	izraelský	k2eAgFnSc1d1	izraelská
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
bariéra	bariéra	k1gFnSc1	bariéra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
přilehlý	přilehlý	k2eAgInSc1d1	přilehlý
Západní	západní	k2eAgInSc1d1	západní
břeh	břeh	k1gInSc1	břeh
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
leží	ležet	k5eAaImIp3nS	ležet
palestinská	palestinský	k2eAgFnSc1d1	palestinská
vesnice	vesnice	k1gFnSc1	vesnice
Faqqua	Faqqua	k1gFnSc1	Faqqua
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Bejtše	Bejtše	k1gFnSc1	Bejtše
<g/>
'	'	kIx"	'
<g/>
anské	anský	k2eAgNnSc1d1	anské
údolí	údolí	k1gNnSc1	údolí
</s>
</p>
