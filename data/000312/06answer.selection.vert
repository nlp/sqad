<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Republik	republika	k1gFnPc2	republika
Österreich	Österreich	k1gInSc1	Österreich
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemská	vnitrozemský	k2eAgFnSc1d1	vnitrozemská
spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
ležící	ležící	k2eAgFnSc1d1	ležící
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
