<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Republik	republika	k1gFnPc2	republika
Österreich	Österreich	k1gInSc1	Österreich
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemská	vnitrozemský	k2eAgFnSc1d1	vnitrozemská
spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
ležící	ležící	k2eAgFnSc1d1	ležící
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
devíti	devět	k4xCc2	devět
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Lichtenštejnskem	Lichtenštejnsko	k1gNnSc7	Lichtenštejnsko
a	a	k8xC	a
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
a	a	k8xC	a
Slovenskem	Slovensko	k1gNnSc7	Slovensko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
Eurozóny	Eurozóna	k1gFnSc2	Eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnPc4d3	veliký
města	město	k1gNnPc4	město
jsou	být	k5eAaImIp3nP	být
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
Linec	Linec	k1gInSc1	Linec
<g/>
,	,	kIx,	,
Salzburg	Salzburg	k1gInSc1	Salzburg
a	a	k8xC	a
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
turistickou	turistický	k2eAgFnSc7d1	turistická
destinací	destinace	k1gFnSc7	destinace
<g/>
,	,	kIx,	,
lákající	lákající	k2eAgFnSc4d1	lákající
na	na	k7c4	na
malebné	malebný	k2eAgFnPc4d1	malebná
krajiny	krajina	k1gFnPc4	krajina
<g/>
,	,	kIx,	,
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
vysokohorské	vysokohorský	k2eAgInPc4d1	vysokohorský
terény	terén	k1gInPc4	terén
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejoblíbenější	oblíbený	k2eAgInPc4d3	nejoblíbenější
cíle	cíl	k1gInPc4	cíl
patří	patřit	k5eAaImIp3nS	patřit
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
mj.	mj.	kA	mj.
významným	významný	k2eAgNnSc7d1	významné
kulturním	kulturní	k2eAgNnSc7d1	kulturní
střediskem	středisko	k1gNnSc7	středisko
<g/>
,	,	kIx,	,
a	a	k8xC	a
města	město	k1gNnSc2	město
Salzburg	Salzburg	k1gInSc1	Salzburg
a	a	k8xC	a
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Lákavé	lákavý	k2eAgFnPc1d1	lákavá
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
početné	početný	k2eAgInPc1d1	početný
zámky	zámek	k1gInPc1	zámek
<g/>
,	,	kIx,	,
hrady	hrad	k1gInPc1	hrad
<g/>
,	,	kIx,	,
kláštery	klášter	k1gInPc1	klášter
a	a	k8xC	a
kostely	kostel	k1gInPc1	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
nabízí	nabízet	k5eAaImIp3nS	nabízet
Rakousko	Rakousko	k1gNnSc4	Rakousko
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
světově	světově	k6eAd1	světově
známá	známý	k2eAgNnPc4d1	známé
alpská	alpský	k2eAgNnPc4d1	alpské
horská	horský	k2eAgNnPc4d1	horské
střediska	středisko	k1gNnPc4	středisko
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
Innsbruck	Innsbruck	k1gMnSc1	Innsbruck
<g/>
,	,	kIx,	,
Kitzbühel	Kitzbühel	k1gMnSc1	Kitzbühel
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
Solné	solný	k2eAgFnSc2d1	solná
komory	komora	k1gFnSc2	komora
nebo	nebo	k8xC	nebo
Zell	Zell	k1gMnSc1	Zell
am	am	k?	am
See	See	k1gMnSc1	See
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Německý	německý	k2eAgInSc1d1	německý
název	název	k1gInSc1	název
Österreich	Österreicha	k1gFnPc2	Österreicha
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
staroněmeckého	staroněmecký	k2eAgNnSc2d1	staroněmecké
slova	slovo	k1gNnSc2	slovo
Ostarrichi	Ostarrichi	k1gNnSc2	Ostarrichi
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
v	v	k7c6	v
listině	listina	k1gFnSc6	listina
císaře	císař	k1gMnSc2	císař
Oty	Ota	k1gMnSc2	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
996	[number]	k4	996
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
"	"	kIx"	"
<g/>
Východní	východní	k2eAgFnSc4d1	východní
marku	marka	k1gFnSc4	marka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Marchia	Marchia	k1gFnSc1	Marchia
orientalis	orientalis	k1gFnSc2	orientalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
nejvýchodnějším	východní	k2eAgNnSc7d3	nejvýchodnější
územím	území	k1gNnSc7	území
s	s	k7c7	s
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
mluvícím	mluvící	k2eAgNnSc7d1	mluvící
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
početných	početný	k2eAgInPc2d1	početný
dialektů	dialekt	k1gInPc2	dialekt
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
latině	latina	k1gFnSc6	latina
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
zkomolen	zkomolit	k5eAaPmNgMnS	zkomolit
na	na	k7c4	na
Austria	Austrium	k1gNnPc4	Austrium
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
převzaly	převzít	k5eAaPmAgInP	převzít
další	další	k2eAgInPc4d1	další
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
negermánské	germánský	k2eNgNnSc1d1	germánský
<g/>
.	.	kIx.	.
</s>
<s>
Zavádějící	zavádějící	k2eAgFnSc1d1	zavádějící
podoba	podoba	k1gFnSc1	podoba
s	s	k7c7	s
australis	australis	k1gFnSc7	australis
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
jižní	jižní	k2eAgNnSc1d1	jižní
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
náhodná	náhodný	k2eAgFnSc1d1	náhodná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Originální	originální	k2eAgNnSc1d1	originální
české	český	k2eAgNnSc1d1	české
pojmenování	pojmenování	k1gNnSc1	pojmenování
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
historicky	historicky	k6eAd1	historicky
Rakúsy	Rakús	k1gInPc4	Rakús
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Rakousy	Rakousy	k1gInPc1	Rakousy
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
pohraničního	pohraniční	k2eAgInSc2d1	pohraniční
hradu	hrad	k1gInSc2	hrad
Ratgoz	Ratgoza	k1gFnPc2	Ratgoza
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Raabs	Raabs	k1gInSc1	Raabs
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
moravské	moravský	k2eAgFnSc2d1	Moravská
a	a	k8xC	a
rakouské	rakouský	k2eAgFnSc2d1	rakouská
řeky	řeka	k1gFnSc2	řeka
Dyje	Dyje	k1gFnSc2	Dyje
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
název	název	k1gInSc4	název
čeští	český	k2eAgMnPc1d1	český
kupci	kupec	k1gMnPc1	kupec
zkomolili	zkomolit	k5eAaPmAgMnP	zkomolit
na	na	k7c4	na
Rakús	Rakús	k1gInSc4	Rakús
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Rakuš	Rakuš	k1gMnSc1	Rakuš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
pak	pak	k6eAd1	pak
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
okolní	okolní	k2eAgNnSc4d1	okolní
území	území	k1gNnSc4	území
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc4	název
převzala	převzít	k5eAaPmAgFnS	převzít
slovenština	slovenština	k1gFnSc1	slovenština
jako	jako	k8xS	jako
Rakúsko	Rakúsko	k1gNnSc1	Rakúsko
a	a	k8xC	a
dříve	dříve	k6eAd2	dříve
též	též	k9	též
polština	polština	k1gFnSc1	polština
jako	jako	k8xS	jako
Rakusy	Rakus	k1gInPc1	Rakus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Pravěké	pravěký	k2eAgNnSc1d1	pravěké
osídlení	osídlení	k1gNnSc1	osídlení
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
lidské	lidský	k2eAgNnSc1d1	lidské
osídlení	osídlení	k1gNnSc1	osídlení
na	na	k7c6	na
území	území	k1gNnSc6	území
Rakouska	Rakousko	k1gNnSc2	Rakousko
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
paleolitu	paleolit	k1gInSc6	paleolit
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
neandrtálců	neandrtálec	k1gMnPc2	neandrtálec
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
archeologických	archeologický	k2eAgNnPc2d1	Archeologické
nalezišť	naleziště	k1gNnPc2	naleziště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Dolních	dolní	k2eAgMnPc6d1	dolní
Rakousech	Rakous	k1gMnPc6	Rakous
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgNnPc1d3	nejznámější
ve	v	k7c6	v
Wachau	Wachaus	k1gInSc6	Wachaus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
nalezena	naleznout	k5eAaPmNgFnS	naleznout
obě	dva	k4xCgFnPc1	dva
dvě	dva	k4xCgNnPc4	dva
nejstarší	starý	k2eAgNnPc4d3	nejstarší
rakouská	rakouský	k2eAgNnPc4d1	rakouské
umělecká	umělecký	k2eAgNnPc4d1	umělecké
díla	dílo	k1gNnPc4	dílo
–	–	k?	–
Venuše	Venuše	k1gFnSc2	Venuše
z	z	k7c2	z
Galgenbergu	Galgenberg	k1gInSc2	Galgenberg
(	(	kIx(	(
<g/>
též	též	k9	též
"	"	kIx"	"
<g/>
Tancující	tancující	k2eAgFnPc4d1	tancující
Fanny	Fanna	k1gFnPc4	Fanna
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nalezená	nalezený	k2eAgFnSc1d1	nalezená
u	u	k7c2	u
Stratzingu	Stratzing	k1gInSc2	Stratzing
a	a	k8xC	a
Willendorfská	Willendorfský	k2eAgFnSc1d1	Willendorfská
venuše	venuše	k1gFnSc1	venuše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
mladší	mladý	k2eAgFnSc2d2	mladší
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
pocházejí	pocházet	k5eAaImIp3nP	pocházet
první	první	k4xOgNnPc1	první
naleziště	naleziště	k1gNnPc1	naleziště
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
řazena	řadit	k5eAaImNgFnS	řadit
také	také	k9	také
známá	známý	k2eAgFnSc1d1	známá
zmrzlá	zmrzlý	k2eAgFnSc1d1	zmrzlá
mumie	mumie	k1gFnSc1	mumie
Ötziho	Ötzi	k1gMnSc2	Ötzi
<g/>
,	,	kIx,	,
nalezená	nalezený	k2eAgFnSc1d1	nalezená
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgInPc4d1	bronzový
mezi	mezi	k7c7	mezi
3.	[number]	k4	3.
a	a	k8xC	a
1.	[number]	k4	1.
stoletím	století	k1gNnSc7	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
vznikala	vznikat	k5eAaImAgFnS	vznikat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgNnPc4d2	veliký
obchodní	obchodní	k2eAgNnPc4d1	obchodní
centra	centrum	k1gNnPc4	centrum
a	a	k8xC	a
opevnění	opevnění	k1gNnPc4	opevnění
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
těžby	těžba	k1gFnSc2	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Hallstattu	Hallstatt	k1gInSc2	Hallstatt
začalo	začít	k5eAaPmAgNnS	začít
systematické	systematický	k2eAgNnSc1d1	systematické
dobývání	dobývání	k1gNnSc1	dobývání
soli	sůl	k1gFnSc2	sůl
<g/>
;	;	kIx,	;
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
také	také	k9	také
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
starší	starý	k2eAgFnSc1d2	starší
doba	doba	k1gFnSc1	doba
železná	železný	k2eAgFnSc1d1	železná
–	–	k?	–
doba	doba	k1gFnSc1	doba
halštatská	halštatský	k2eAgFnSc1d1	halštatská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mladší	mladý	k2eAgFnSc6d2	mladší
době	doba	k1gFnSc6	doba
železné	železný	k2eAgFnPc1d1	železná
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgFnPc1d1	patrná
známky	známka	k1gFnPc1	známka
keltského	keltský	k2eAgNnSc2d1	keltské
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
státních	státní	k2eAgInPc2d1	státní
útvarů	útvar	k1gInPc2	útvar
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
západ	západ	k1gInSc1	západ
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
osídlen	osídlen	k2eAgInSc4d1	osídlen
Réty	rét	k1gInPc4	rét
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgInSc4	první
státní	státní	k2eAgInSc4d1	státní
útvary	útvar	k1gInPc4	útvar
na	na	k7c4	na
území	území	k1gNnSc4	území
Rakouska	Rakousko	k1gNnSc2	Rakousko
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
dnešního	dnešní	k2eAgInSc2d1	dnešní
rakouského	rakouský	k2eAgInSc2d1	rakouský
státu	stát	k1gInSc2	stát
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
15	[number]	k4	15
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
obsazena	obsadit	k5eAaPmNgFnS	obsadit
Říší	říš	k1gFnSc7	říš
římskou	římský	k2eAgFnSc7d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rakouska	Rakousko	k1gNnSc2	Rakousko
zřídili	zřídit	k5eAaPmAgMnP	zřídit
tři	tři	k4xCgFnPc4	tři
římské	římský	k2eAgFnPc4d1	římská
provincie	provincie	k1gFnPc4	provincie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejstarší	starý	k2eAgMnSc1d3	nejstarší
a	a	k8xC	a
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
bylo	být	k5eAaImAgNnS	být
Noricum	Noricum	k1gNnSc1	Noricum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
ustavil	ustavit	k5eAaPmAgMnS	ustavit
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Claudius	Claudius	k1gMnSc1	Claudius
(	(	kIx(	(
<g/>
vládl	vládnout	k5eAaImAgMnS	vládnout
41	[number]	k4	41
<g/>
–	–	k?	–
<g/>
54	[number]	k4	54
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
dvěma	dva	k4xCgFnPc7	dva
provinciemi	provincie	k1gFnPc7	provincie
byla	být	k5eAaImAgFnS	být
Panonie	Panonie	k1gFnSc1	Panonie
a	a	k8xC	a
Raetie	Raetie	k1gFnSc1	Raetie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Vídně	Vídeň	k1gFnSc2	Vídeň
leželo	ležet	k5eAaImAgNnS	ležet
největší	veliký	k2eAgNnSc1d3	veliký
římské	římský	k2eAgNnSc1d1	římské
město	město	k1gNnSc1	město
Carnuntum	Carnuntum	k1gNnSc1	Carnuntum
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
další	další	k2eAgNnPc4d1	další
důležitá	důležitý	k2eAgNnPc4d1	důležité
místa	místo	k1gNnPc4	místo
byla	být	k5eAaImAgFnS	být
například	například	k6eAd1	například
Virunum	Virunum	k1gInSc4	Virunum
(	(	kIx(	(
<g/>
severně	severně	k6eAd1	severně
od	od	k7c2	od
Klagenfurtu	Klagenfurt	k1gInSc2	Klagenfurt
<g/>
)	)	kIx)	)
a	a	k8xC	a
Teurnia	Teurnium	k1gNnSc2	Teurnium
(	(	kIx(	(
<g/>
poblíž	poblíž	k6eAd1	poblíž
Spittal	Spittal	k1gMnSc1	Spittal
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Drau	Draus	k1gInSc6	Draus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
4.	[number]	k4	4.
století	století	k1gNnSc2	století
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
přicházeli	přicházet	k5eAaImAgMnP	přicházet
Germáni	Germán	k1gMnPc1	Germán
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
přispívali	přispívat	k5eAaImAgMnP	přispívat
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
moci	moc	k1gFnSc2	moc
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
se	se	k3xPyFc4	se
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
roku	rok	k1gInSc2	rok
476	[number]	k4	476
a	a	k8xC	a
namísto	namísto	k7c2	namísto
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
začali	začít	k5eAaPmAgMnP	začít
objevovat	objevovat	k5eAaImF	objevovat
Bavoři	Bavor	k1gMnPc1	Bavor
a	a	k8xC	a
Slované	Slovan	k1gMnPc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Bavoři	Bavor	k1gMnPc1	Bavor
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
dostali	dostat	k5eAaPmAgMnP	dostat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
však	však	k9	však
zabránit	zabránit	k5eAaPmF	zabránit
Slovanům	Slovan	k1gMnPc3	Slovan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
zde	zde	k6eAd1	zde
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
své	svůj	k3xOyFgNnSc4	svůj
karantánské	karantánský	k2eAgNnSc4d1	karantánský
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Bavoři	Bavor	k1gMnPc1	Bavor
získali	získat	k5eAaPmAgMnP	získat
převahu	převaha	k1gFnSc4	převaha
až	až	k6eAd1	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
8.	[number]	k4	8.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Karantánii	Karantánie	k1gFnSc4	Karantánie
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
<g/>
,	,	kIx,	,
sami	sám	k3xTgMnPc1	sám
se	se	k3xPyFc4	se
však	však	k9	však
roku	rok	k1gInSc2	rok
757	[number]	k4	757
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
područí	područí	k1gNnSc2	područí
franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
<g/>
Frankové	Franková	k1gFnSc2	Franková
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
ustavili	ustavit	k5eAaPmAgMnP	ustavit
Východní	východní	k2eAgFnSc4d1	východní
marku	marka	k1gFnSc4	marka
(	(	kIx(	(
<g/>
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
východofranské	východofranský	k2eAgFnSc2d1	Východofranská
říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
však	však	k9	však
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
1.	[number]	k4	1.
polovinu	polovina	k1gFnSc4	polovina
10.	[number]	k4	10.
století	století	k1gNnPc2	století
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c4	v
plen	plen	k1gInSc4	plen
Maďarům	Maďar	k1gMnPc3	Maďar
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
dokázal	dokázat	k5eAaPmAgInS	dokázat
porazit	porazit	k5eAaPmF	porazit
až	až	k9	až
král	král	k1gMnSc1	král
Ota	Ota	k1gMnSc1	Ota
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
955	[number]	k4	955
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Ota	Ota	k1gMnSc1	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
pak	pak	k6eAd1	pak
Východní	východní	k2eAgFnSc4d1	východní
marku	marka	k1gFnSc4	marka
roku	rok	k1gInSc2	rok
976	[number]	k4	976
reorganizoval	reorganizovat	k5eAaBmAgInS	reorganizovat
a	a	k8xC	a
svěřil	svěřit	k5eAaPmAgInS	svěřit
ji	on	k3xPp3gFnSc4	on
Leopoldovi	Leopold	k1gMnSc3	Leopold
I.	I.	kA	I.
Babenberskému	babenberský	k2eAgMnSc3d1	babenberský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vláda	vláda	k1gFnSc1	vláda
Babenberků	Babenberk	k1gInPc2	Babenberk
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Babenberkové	Babenberek	k1gMnPc1	Babenberek
se	se	k3xPyFc4	se
zprvu	zprvu	k6eAd1	zprvu
museli	muset	k5eAaImAgMnP	muset
ještě	ještě	k6eAd1	ještě
vypořádávat	vypořádávat	k5eAaImF	vypořádávat
s	s	k7c7	s
Maďary	Maďar	k1gMnPc7	Maďar
<g/>
,	,	kIx,	,
po	po	k7c6	po
uklidnění	uklidnění	k1gNnSc6	uklidnění
východní	východní	k2eAgFnSc2d1	východní
hranice	hranice	k1gFnSc2	hranice
se	se	k3xPyFc4	se
však	však	k9	však
mohli	moct	k5eAaImAgMnP	moct
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
11.	[number]	k4	11.
a	a	k8xC	a
12.	[number]	k4	12.
století	století	k1gNnPc2	století
zapojit	zapojit	k5eAaPmF	zapojit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
o	o	k7c4	o
investituru	investitura	k1gFnSc4	investitura
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c6	na
papežově	papežův	k2eAgFnSc6d1	papežova
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
mocenského	mocenský	k2eAgInSc2d1	mocenský
zápasu	zápas	k1gInSc2	zápas
mezi	mezi	k7c7	mezi
Štaufy	Štauf	k1gMnPc7	Štauf
a	a	k8xC	a
Welfy	Welf	k1gMnPc7	Welf
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1156	[number]	k4	1156
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Jindřich	Jindřich	k1gMnSc1	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jasomirgott	Jasomirgott	k1gInSc1	Jasomirgott
privilegium	privilegium	k1gNnSc1	privilegium
minus	minus	k1gInSc1	minus
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
císař	císař	k1gMnSc1	císař
povýšil	povýšit	k5eAaPmAgMnS	povýšit
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
marku	marka	k1gFnSc4	marka
na	na	k7c4	na
rakouské	rakouský	k2eAgNnSc4d1	rakouské
vévodství	vévodství	k1gNnSc4	vévodství
a	a	k8xC	a
dědičnou	dědičný	k2eAgFnSc4d1	dědičná
držbu	držba	k1gFnSc4	držba
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
i	i	k8xC	i
ženské	ženský	k2eAgFnSc6d1	ženská
linii	linie	k1gFnSc6	linie
udělil	udělit	k5eAaPmAgMnS	udělit
právě	právě	k9	právě
Babenberkům	Babenberka	k1gMnPc3	Babenberka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1192	[number]	k4	1192
pak	pak	k6eAd1	pak
Babenberkové	Babenberek	k1gMnPc1	Babenberek
získali	získat	k5eAaPmAgMnP	získat
vládu	vláda	k1gFnSc4	vláda
také	také	k9	také
nad	nad	k7c7	nad
štýrským	štýrský	k2eAgNnSc7d1	štýrské
vévodstvím	vévodství	k1gNnSc7	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
Babenberkové	Babenberek	k1gMnPc1	Babenberek
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zapojovali	zapojovat	k5eAaImAgMnP	zapojovat
do	do	k7c2	do
říšské	říšský	k2eAgFnSc2d1	říšská
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
účastnili	účastnit	k5eAaImAgMnP	účastnit
se	se	k3xPyFc4	se
například	například	k6eAd1	například
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Agresivnější	agresivní	k2eAgFnSc7d2	agresivnější
politikou	politika	k1gFnSc7	politika
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
13.	[number]	k4	13.
století	století	k1gNnSc1	století
si	se	k3xPyFc3	se
však	však	k9	však
Babenberkové	Babenberek	k1gMnPc1	Babenberek
znepřátelili	znepřátelit	k5eAaPmAgMnP	znepřátelit
své	svůj	k3xOyFgMnPc4	svůj
bavorské	bavorský	k2eAgMnPc4d1	bavorský
<g/>
,	,	kIx,	,
české	český	k2eAgMnPc4d1	český
i	i	k8xC	i
uherské	uherský	k2eAgMnPc4d1	uherský
sousedy	soused	k1gMnPc4	soused
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1246	[number]	k4	1246
rod	rod	k1gInSc4	rod
Babenberků	Babenberka	k1gMnPc2	Babenberka
vymřel	vymřít	k5eAaPmAgMnS	vymřít
<g/>
,	,	kIx,	,
nastal	nastat	k5eAaPmAgInS	nastat
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
dědictví	dědictví	k1gNnSc4	dědictví
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
Dočasně	dočasně	k6eAd1	dočasně
získal	získat	k5eAaPmAgMnS	získat
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
rakouském	rakouský	k2eAgNnSc6d1	rakouské
<g/>
,	,	kIx,	,
štýrském	štýrský	k2eAgNnSc6d1	štýrské
i	i	k8xC	i
korutanském	korutanský	k2eAgNnSc6d1	korutanské
vévodství	vévodství	k1gNnSc6	vévodství
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1273	[number]	k4	1273
se	se	k3xPyFc4	se
však	však	k9	však
císaři	císař	k1gMnSc3	císař
Rudolfovi	Rudolf	k1gMnSc3	Rudolf
I.	I.	kA	I.
Habsburskému	habsburský	k2eAgMnSc3d1	habsburský
dařilo	dařit	k5eAaImAgNnS	dařit
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
z	z	k7c2	z
rakouských	rakouský	k2eAgFnPc2d1	rakouská
zemí	zem	k1gFnPc2	zem
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
jej	on	k3xPp3gMnSc4	on
nakonec	nakonec	k6eAd1	nakonec
zcela	zcela	k6eAd1	zcela
porazil	porazit	k5eAaPmAgMnS	porazit
roku	rok	k1gInSc2	rok
1278	[number]	k4	1278
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
babenberské	babenberský	k2eAgNnSc1d1	babenberské
dědictví	dědictví	k1gNnSc1	dědictví
definitivně	definitivně	k6eAd1	definitivně
získali	získat	k5eAaPmAgMnP	získat
Habsburkové	Habsburk	k1gMnPc1	Habsburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Habsburské	habsburský	k2eAgNnSc1d1	habsburské
soustátí	soustátí	k1gNnSc1	soustátí
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Moc	moc	k1gFnSc1	moc
Habsburků	Habsburk	k1gMnPc2	Habsburk
se	se	k3xPyFc4	se
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
držav	država	k1gFnPc2	država
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
ovšem	ovšem	k9	ovšem
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
rakouské	rakouský	k2eAgNnSc4d1	rakouské
a	a	k8xC	a
štýrské	štýrský	k2eAgNnSc4d1	štýrské
vévodství	vévodství	k1gNnSc4	vévodství
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
záhy	záhy	k6eAd1	záhy
také	také	k9	také
získali	získat	k5eAaPmAgMnP	získat
Korutany	Korutany	k1gInPc4	Korutany
a	a	k8xC	a
Tyroly	Tyroly	k1gInPc4	Tyroly
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Rudolfa	Rudolf	k1gMnSc2	Rudolf
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Habsburského	habsburský	k2eAgInSc2d1	habsburský
roku	rok	k1gInSc2	rok
1365	[number]	k4	1365
však	však	k8xC	však
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
syny	syn	k1gMnPc7	syn
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
roku	rok	k1gInSc2	rok
1379	[number]	k4	1379
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
větve	větev	k1gFnPc1	větev
rodu	rod	k1gInSc2	rod
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
následující	následující	k2eAgInPc4d1	následující
století	století	k1gNnSc3	století
soupeřily	soupeřit	k5eAaImAgFnP	soupeřit
a	a	k8xC	a
ke	k	k7c3	k
sjednocení	sjednocení	k1gNnSc3	sjednocení
habsburských	habsburský	k2eAgFnPc2d1	habsburská
držav	država	k1gFnPc2	država
došlo	dojít	k5eAaPmAgNnS	dojít
znovu	znovu	k6eAd1	znovu
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1491	[number]	k4	1491
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
I.	I.	kA	I.
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
rakouské	rakouský	k2eAgFnSc2d1	rakouská
země	zem	k1gFnSc2	zem
obdržel	obdržet	k5eAaPmAgMnS	obdržet
jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
roku	rok	k1gInSc2	rok
1526	[number]	k4	1526
získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
uherskou	uherský	k2eAgFnSc4d1	uherská
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
a	a	k8xC	a
položil	položit	k5eAaPmAgMnS	položit
tak	tak	k6eAd1	tak
základy	základ	k1gInPc1	základ
habsburského	habsburský	k2eAgNnSc2d1	habsburské
soustátí	soustátí	k1gNnSc2	soustátí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
se	se	k3xPyFc4	se
však	však	k9	však
moc	moc	k6eAd1	moc
v	v	k7c6	v
habsburském	habsburský	k2eAgNnSc6d1	habsburské
soustátí	soustátí	k1gNnSc6	soustátí
opět	opět	k6eAd1	opět
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
mezi	mezi	k7c4	mezi
jeho	on	k3xPp3gMnSc4	on
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
země	země	k1gFnSc1	země
musela	muset	k5eAaImAgFnS	muset
čelit	čelit	k5eAaImF	čelit
náboženskému	náboženský	k2eAgInSc3d1	náboženský
neklidu	neklid	k1gInSc3	neklid
i	i	k8xC	i
třicetileté	třicetiletý	k2eAgFnSc3d1	třicetiletá
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
k	k	k7c3	k
jejímu	její	k3xOp3gMnSc3	její
sjednocení	sjednocení	k1gNnSc1	sjednocení
došlo	dojít	k5eAaPmAgNnS	dojít
opět	opět	k6eAd1	opět
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1665	[number]	k4	1665
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
musel	muset	k5eAaImAgInS	muset
vzápětí	vzápětí	k6eAd1	vzápětí
čelit	čelit	k5eAaImF	čelit
masivnímu	masivní	k2eAgNnSc3d1	masivní
osmanskému	osmanský	k2eAgNnSc3d1	osmanské
tažení	tažení	k1gNnSc3	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Protiútok	protiútok	k1gInSc1	protiútok
rakouských	rakouský	k2eAgNnPc2d1	rakouské
a	a	k8xC	a
spřátelených	spřátelený	k2eAgNnPc2d1	spřátelené
vojsk	vojsko	k1gNnPc2	vojsko
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
Osmany	Osman	k1gMnPc4	Osman
z	z	k7c2	z
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
území	území	k1gNnSc3	území
habsburské	habsburský	k2eAgFnSc2d1	habsburská
monarchie	monarchie	k1gFnSc2	monarchie
podstatně	podstatně	k6eAd1	podstatně
zvětšilo	zvětšit	k5eAaPmAgNnS	zvětšit
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
územních	územní	k2eAgInPc2d1	územní
zisků	zisk	k1gInPc2	zisk
pak	pak	k6eAd1	pak
habsburská	habsburský	k2eAgFnSc1d1	habsburská
monarchie	monarchie	k1gFnSc1	monarchie
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
o	o	k7c4	o
dědictví	dědictví	k1gNnSc4	dědictví
španělské	španělský	k2eAgNnSc4d1	španělské
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18.	[number]	k4	18.
století	století	k1gNnPc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jí	on	k3xPp3gFnSc3	on
připadla	připadnout	k5eAaPmAgFnS	připadnout
některá	některý	k3yIgNnPc4	některý
území	území	k1gNnPc4	území
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
španělskými	španělský	k2eAgMnPc7d1	španělský
Habsburky	Habsburk	k1gMnPc7	Habsburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
Karel	Karel	k1gMnSc1	Karel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
ovšem	ovšem	k9	ovšem
roku	rok	k1gInSc2	rok
1740	[number]	k4	1740
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bez	bez	k7c2	bez
mužského	mužský	k2eAgMnSc2d1	mužský
potomka	potomek	k1gMnSc2	potomek
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
proto	proto	k8xC	proto
musela	muset	k5eAaImAgFnS	muset
čelit	čelit	k5eAaImF	čelit
několika	několik	k4yIc2	několik
armádám	armáda	k1gFnPc3	armáda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
si	se	k3xPyFc3	se
chtěly	chtít	k5eAaImAgFnP	chtít
habsburské	habsburský	k2eAgNnSc4d1	habsburské
dědictví	dědictví	k1gNnSc4	dědictví
rozdělit	rozdělit	k5eAaPmF	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nastalé	nastalý	k2eAgFnSc6d1	nastalá
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
jen	jen	k9	jen
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
získalo	získat	k5eAaPmAgNnS	získat
většinu	většina	k1gFnSc4	většina
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
menší	malý	k2eAgNnPc1d2	menší
území	území	k1gNnPc1	území
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
války	válka	k1gFnPc1	válka
zemi	zem	k1gFnSc4	zem
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
vyčerpaly	vyčerpat	k5eAaPmAgFnP	vyčerpat
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
postavilo	postavit	k5eAaPmAgNnS	postavit
Marii	Maria	k1gFnSc3	Maria
Terezii	Terezie	k1gFnSc3	Terezie
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
syna	syn	k1gMnSc2	syn
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
před	před	k7c4	před
nutnost	nutnost	k1gFnSc4	nutnost
provést	provést	k5eAaPmF	provést
osvícenské	osvícenský	k2eAgFnPc4d1	osvícenská
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
začaly	začít	k5eAaPmAgFnP	začít
feudální	feudální	k2eAgFnSc4d1	feudální
správu	správa	k1gFnSc4	správa
nahrazovat	nahrazovat	k5eAaImF	nahrazovat
moderní	moderní	k2eAgFnSc7d1	moderní
státní	státní	k2eAgFnSc7d1	státní
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
monarchie	monarchie	k1gFnSc2	monarchie
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18.	[number]	k4	18.
století	století	k1gNnSc4	století
rozšířeno	rozšířen	k2eAgNnSc4d1	rozšířeno
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
válek	válka	k1gFnPc2	válka
s	s	k7c7	s
revoluční	revoluční	k2eAgFnSc7d1	revoluční
Francií	Francie	k1gFnSc7	Francie
monarchie	monarchie	k1gFnSc2	monarchie
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
Nizozemí	Nizozemí	k1gNnSc4	Nizozemí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19.	[number]	k4	19.
století	století	k1gNnSc1	století
postihlo	postihnout	k5eAaPmAgNnS	postihnout
habsburskou	habsburský	k2eAgFnSc4d1	habsburská
monarchii	monarchie	k1gFnSc4	monarchie
Napoleonovo	Napoleonův	k2eAgNnSc1d1	Napoleonovo
tažení	tažení	k1gNnSc1	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
habsburský	habsburský	k2eAgMnSc1d1	habsburský
panovník	panovník	k1gMnSc1	panovník
zůstal	zůstat	k5eAaPmAgMnS	zůstat
pouze	pouze	k6eAd1	pouze
rakouským	rakouský	k2eAgMnSc7d1	rakouský
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Snahou	snaha	k1gFnSc7	snaha
státu	stát	k1gInSc2	stát
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
přestávkou	přestávka	k1gFnSc7	přestávka
při	při	k7c6	při
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
potlačovat	potlačovat	k5eAaImF	potlačovat
liberální	liberální	k2eAgFnPc4d1	liberální
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěchy	neúspěch	k1gInPc1	neúspěch
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
císaře	císař	k1gMnSc4	císař
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc4	Josef
I.	I.	kA	I.
donutily	donutit	k5eAaPmAgFnP	donutit
změnit	změnit	k5eAaPmF	změnit
kurs	kurs	k1gInSc4	kurs
a	a	k8xC	a
přetvořit	přetvořit	k5eAaPmF	přetvořit
absolutistický	absolutistický	k2eAgInSc4d1	absolutistický
systém	systém	k1gInSc4	systém
na	na	k7c4	na
konstituční	konstituční	k2eAgFnSc4d1	konstituční
monarchii	monarchie	k1gFnSc4	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Rakouska	Rakousko	k1gNnSc2	Rakousko
v	v	k7c6	v
prusko-rakouské	pruskoakouský	k2eAgFnSc6d1	prusko-rakouská
válce	válka	k1gFnSc6	válka
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
Německý	německý	k2eAgInSc1d1	německý
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
Rakousko	Rakousko	k1gNnSc1	Rakousko
dominovalo	dominovat	k5eAaImAgNnS	dominovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
70.	[number]	k4	70.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
monarchie	monarchie	k1gFnSc1	monarchie
začala	začít	k5eAaPmAgFnS	začít
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c4	na
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgInPc1d1	rostoucí
nacionální	nacionální	k2eAgInPc1d1	nacionální
problémy	problém	k1gInPc1	problém
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
vyřešit	vyřešit	k5eAaPmF	vyřešit
přeměnou	přeměna	k1gFnSc7	přeměna
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
císařství	císařství	k1gNnSc2	císařství
na	na	k7c4	na
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
však	však	k9	však
uspokojil	uspokojit	k5eAaPmAgMnS	uspokojit
jen	jen	k9	jen
Maďary	Maďar	k1gMnPc4	Maďar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Katastrofu	katastrofa	k1gFnSc4	katastrofa
pro	pro	k7c4	pro
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
znamenala	znamenat	k5eAaImAgFnS	znamenat
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Rakouští	rakouský	k2eAgMnPc1d1	rakouský
Němci	Němec	k1gMnPc1	Němec
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
prosadit	prosadit	k5eAaPmF	prosadit
koncepci	koncepce	k1gFnSc4	koncepce
Německého	německý	k2eAgNnSc2d1	německé
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
přijmout	přijmout	k5eAaPmF	přijmout
svůj	svůj	k3xOyFgInSc4	svůj
stát	stát	k1gInSc4	stát
v	v	k7c6	v
hranicích	hranice	k1gFnPc6	hranice
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jej	on	k3xPp3gInSc4	on
vymezila	vymezit	k5eAaPmAgFnS	vymezit
Saintgermainská	Saintgermainský	k2eAgFnSc1d1	Saintgermainský
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
potýkala	potýkat	k5eAaImAgFnS	potýkat
s	s	k7c7	s
obrovskými	obrovský	k2eAgInPc7d1	obrovský
hospodářskými	hospodářský	k2eAgInPc7d1	hospodářský
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
nestabilní	stabilní	k2eNgFnPc1d1	nestabilní
vlády	vláda	k1gFnPc1	vláda
dvou	dva	k4xCgFnPc2	dva
znepřátelených	znepřátelený	k2eAgFnPc2d1	znepřátelená
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
uspokojivě	uspokojivě	k6eAd1	uspokojivě
řešit	řešit	k5eAaImF	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
konečně	konečně	k9	konečně
Engelbert	Engelbert	k1gInSc1	Engelbert
Dollfuss	Dollfussa	k1gFnPc2	Dollfussa
nastolil	nastolit	k5eAaPmAgInS	nastolit
autoritativní	autoritativní	k2eAgInSc1d1	autoritativní
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
on	on	k3xPp3gMnSc1	on
musel	muset	k5eAaImAgMnS	muset
čelit	čelit	k5eAaImF	čelit
vážným	vážný	k2eAgInPc3d1	vážný
problémům	problém	k1gInPc3	problém
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vzestupu	vzestup	k1gInSc2	vzestup
rakouského	rakouský	k2eAgInSc2d1	rakouský
nacismu	nacismus	k1gInSc2	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
rakouských	rakouský	k2eAgMnPc2d1	rakouský
nacistů	nacista	k1gMnPc2	nacista
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
sice	sice	k8xC	sice
nakonec	nakonec	k6eAd1	nakonec
skončil	skončit	k5eAaPmAgInS	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc4	Rakousko
však	však	k9	však
muselo	muset	k5eAaImAgNnS	muset
čelit	čelit	k5eAaImF	čelit
stále	stále	k6eAd1	stále
většímu	veliký	k2eAgInSc3d2	veliký
tlaku	tlak	k1gInSc3	tlak
Hitlerova	Hitlerův	k2eAgNnSc2d1	Hitlerovo
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
si	se	k3xPyFc3	se
nakonec	nakonec	k6eAd1	nakonec
18.	[number]	k4	18.
března	březen	k1gInSc2	březen
1938	[number]	k4	1938
vynutilo	vynutit	k5eAaPmAgNnS	vynutit
"	"	kIx"	"
<g/>
pozvání	pozvání	k1gNnSc1	pozvání
<g/>
"	"	kIx"	"
k	k	k7c3	k
záboru	zábor	k1gInSc3	zábor
(	(	kIx(	(
<g/>
anšlusu	anšlus	k1gInSc2	anšlus
<g/>
)	)	kIx)	)
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Anšlus	anšlus	k1gInSc4	anšlus
podpořila	podpořit	k5eAaPmAgFnS	podpořit
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
většina	většina	k1gFnSc1	většina
Rakušanů	Rakušan	k1gMnPc2	Rakušan
<g/>
.	.	kIx.	.
<g/>
Jakožto	jakožto	k8xS	jakožto
součást	součást	k1gFnSc1	součást
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
Rakousko	Rakousko	k1gNnSc1	Rakousko
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
druhé	druhý	k4xOgFnPc4	druhý
světové	světový	k2eAgFnPc4d1	světová
války	válka	k1gFnPc4	válka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
rakouských	rakouský	k2eAgMnPc2d1	rakouský
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
prominentní	prominentní	k2eAgMnPc1d1	prominentní
nacisté	nacista	k1gMnPc1	nacista
byli	být	k5eAaImAgMnP	být
původem	původ	k1gInSc7	původ
Rakušané	Rakušan	k1gMnPc1	Rakušan
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
Kaltenbrunner	Kaltenbrunner	k1gMnSc1	Kaltenbrunner
<g/>
,	,	kIx,	,
Seyss-Inquart	Seyss-Inquart	k1gInSc1	Seyss-Inquart
<g/>
,	,	kIx,	,
Stangl	Stangl	k1gInSc1	Stangl
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Globocnik	Globocnik	k1gInSc1	Globocnik
<g/>
,	,	kIx,	,
a	a	k8xC	a
Rakušané	Rakušan	k1gMnPc1	Rakušan
tvořili	tvořit	k5eAaImAgMnP	tvořit
přes	přes	k7c4	přes
13	[number]	k4	13
%	%	kIx~	%
příslušníků	příslušník	k1gMnPc2	příslušník
SS	SS	kA	SS
a	a	k8xC	a
okolo	okolo	k7c2	okolo
40	[number]	k4	40
%	%	kIx~	%
dozorců	dozorce	k1gMnPc2	dozorce
v	v	k7c6	v
německých	německý	k2eAgInPc6d1	německý
vyhlazovacích	vyhlazovací	k2eAgInPc6d1	vyhlazovací
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
nacistické	nacistický	k2eAgFnSc2d1	nacistická
moci	moc	k1gFnSc2	moc
bylo	být	k5eAaImAgNnS	být
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
Německo	Německo	k1gNnSc1	Německo
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
Spojenci	spojenec	k1gMnPc1	spojenec
a	a	k8xC	a
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
do	do	k7c2	do
okupačních	okupační	k2eAgFnPc2d1	okupační
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Plnou	plný	k2eAgFnSc4d1	plná
suverenitu	suverenita	k1gFnSc4	suverenita
Rakousko	Rakousko	k1gNnSc1	Rakousko
získalo	získat	k5eAaPmAgNnS	získat
zpět	zpět	k6eAd1	zpět
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
mezi	mezi	k7c7	mezi
zástupci	zástupce	k1gMnPc7	zástupce
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Spojenců	spojenec	k1gMnPc2	spojenec
státní	státní	k2eAgFnSc1d1	státní
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
stažení	stažení	k1gNnSc3	stažení
všech	všecek	k3xTgFnPc2	všecek
cizích	cizí	k2eAgFnPc2d1	cizí
armád	armáda	k1gFnPc2	armáda
z	z	k7c2	z
okupační	okupační	k2eAgFnSc2d1	okupační
zóny	zóna	k1gFnSc2	zóna
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
na	na	k7c4	na
osvobození	osvobození	k1gNnSc4	osvobození
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Vídně	Vídeň	k1gFnSc2	Vídeň
podílela	podílet	k5eAaImAgFnS	podílet
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
dokázali	dokázat	k5eAaPmAgMnP	dokázat
rakouští	rakouský	k2eAgMnPc1d1	rakouský
politikové	politik	k1gMnPc1	politik
při	při	k7c6	při
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
využít	využít	k5eAaPmF	využít
příznivé	příznivý	k2eAgFnPc4d1	příznivá
situace	situace	k1gFnPc4	situace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
a	a	k8xC	a
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
trvalé	trvalý	k2eAgFnSc2d1	trvalá
neutrality	neutralita	k1gFnSc2	neutralita
zajistit	zajistit	k5eAaPmF	zajistit
své	svůj	k3xOyFgFnSc3	svůj
zemi	zem	k1gFnSc3	zem
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
a	a	k8xC	a
1953	[number]	k4	1953
získali	získat	k5eAaPmAgMnP	získat
rakouští	rakouský	k2eAgMnPc1d1	rakouský
komunisté	komunista	k1gMnPc1	komunista
pouze	pouze	k6eAd1	pouze
kolem	kolem	k7c2	kolem
5	[number]	k4	5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
se	se	k3xPyFc4	se
nezařadilo	zařadit	k5eNaPmAgNnS	zařadit
do	do	k7c2	do
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
jeho	on	k3xPp3gInSc4	on
pozoruhodný	pozoruhodný	k2eAgInSc4d1	pozoruhodný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
vzestup	vzestup	k1gInSc4	vzestup
a	a	k8xC	a
prosperitu	prosperita	k1gFnSc4	prosperita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
státní	státní	k2eAgFnSc2d1	státní
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
je	být	k5eAaImIp3nS	být
Rakousko	Rakousko	k1gNnSc4	Rakousko
od	od	k7c2	od
zpětného	zpětný	k2eAgNnSc2d1	zpětné
získání	získání	k1gNnSc2	získání
suverenity	suverenita	k1gFnSc2	suverenita
neutrální	neutrální	k2eAgFnSc1d1	neutrální
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
členem	člen	k1gMnSc7	člen
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
Rakousko	Rakousko	k1gNnSc1	Rakousko
začlenilo	začlenit	k5eAaPmAgNnS	začlenit
do	do	k7c2	do
západních	západní	k2eAgFnPc2d1	západní
nevojenských	vojenský	k2eNgFnPc2d1	nevojenská
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
do	do	k7c2	do
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
Rakousko	Rakousko	k1gNnSc1	Rakousko
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
izolace	izolace	k1gFnSc2	izolace
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
Rakouska	Rakousko	k1gNnSc2	Rakousko
zvolen	zvolit	k5eAaPmNgMnS	zvolit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
OSN	OSN	kA	OSN
Kurt	Kurt	k1gMnSc1	Kurt
Waldheim	Waldheim	k1gMnSc1	Waldheim
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgMnSc6	jenž
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
nacistickou	nacistický	k2eAgFnSc4d1	nacistická
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
575	[number]	k4	575
km	km	kA	km
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
západ-východ	západýchod	k1gInSc1	západ-východ
a	a	k8xC	a
294	[number]	k4	294
km	km	kA	km
sever-jih	severiha	k1gFnPc2	sever-jiha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
tvar	tvar	k1gInSc4	tvar
obrácené	obrácený	k2eAgFnSc2d1	obrácená
křivule	křivule	k1gFnSc2	křivule
<g/>
,	,	kIx,	,
s	s	k7c7	s
protaženým	protažený	k2eAgNnSc7d1	protažené
hrdlem	hrdlo	k1gNnSc7	hrdlo
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Povrch	povrch	k1gInSc1	povrch
===	===	k?	===
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
%	%	kIx~	%
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
hornaté	hornatý	k2eAgFnPc4d1	hornatá
povahy	povaha	k1gFnPc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
většinu	většina	k1gFnSc4	většina
Východních	východní	k2eAgFnPc2d1	východní
Alp	Alpy	k1gFnPc2	Alpy
(	(	kIx(	(
<g/>
jmenovitě	jmenovitě	k6eAd1	jmenovitě
Tyrolské	tyrolský	k2eAgFnPc1d1	tyrolská
Střední	střední	k2eAgFnPc1d1	střední
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Vysoké	vysoký	k2eAgFnPc1d1	vysoká
Taury	Taura	k1gFnPc1	Taura
a	a	k8xC	a
Nízké	nízký	k2eAgFnPc1d1	nízká
Taury	Taura	k1gFnPc1	Taura
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnPc1d1	severní
vápencové	vápencový	k2eAgFnPc1d1	vápencová
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnPc1d1	jižní
vápencové	vápencový	k2eAgFnPc1d1	vápencová
Alpy	Alpy	k1gFnPc1	Alpy
a	a	k8xC	a
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
les	les	k1gInSc1	les
<g/>
,	,	kIx,	,
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Karnské	Karnský	k2eAgFnSc2d1	Karnský
Alpy	alpa	k1gFnSc2	alpa
a	a	k8xC	a
Karavanky	karavanka	k1gFnSc2	karavanka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Horních	horní	k2eAgInPc6d1	horní
a	a	k8xC	a
Dolních	dolní	k2eAgInPc6d1	dolní
Rakousech	Rakous	k1gInPc6	Rakous
<g/>
,	,	kIx,	,
severně	severně	k6eAd1	severně
od	od	k7c2	od
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
jižní	jižní	k2eAgInSc1d1	jižní
okraj	okraj	k1gInSc1	okraj
starého	starý	k2eAgNnSc2d1	staré
pohoří	pohoří	k1gNnSc2	pohoří
Českého	český	k2eAgInSc2d1	český
masivu	masiv	k1gInSc2	masiv
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
severovýchod	severovýchod	k1gInSc4	severovýchod
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
okrajem	okraj	k1gInSc7	okraj
Západní	západní	k2eAgInPc4d1	západní
Karpaty	Karpaty	k1gInPc4	Karpaty
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nížiny	nížina	k1gFnPc1	nížina
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
podél	podél	k7c2	podél
Dunaje	Dunaj	k1gInSc2	Dunaj
(	(	kIx(	(
<g/>
především	především	k9	především
Alpské	alpský	k2eAgNnSc1d1	alpské
předpolí	předpolí	k1gNnSc1	předpolí
a	a	k8xC	a
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
pánev	pánev	k1gFnSc1	pánev
s	s	k7c7	s
Moravským	moravský	k2eAgNnSc7d1	Moravské
polem	pole	k1gNnSc7	pole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
ve	v	k7c6	v
Štýrsku	Štýrsko	k1gNnSc6	Štýrsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
podobnost	podobnost	k1gFnSc4	podobnost
své	svůj	k3xOyFgFnSc2	svůj
krajiny	krajina	k1gFnSc2	krajina
s	s	k7c7	s
italským	italský	k2eAgNnSc7d1	italské
Toskánskem	Toskánsko	k1gNnSc7	Toskánsko
nazýváno	nazývat	k5eAaImNgNnS	nazývat
také	také	k9	také
Štýrská	štýrský	k2eAgFnSc1d1	štýrská
Toskána	Toskána	k1gFnSc1	Toskána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
plochy	plocha	k1gFnSc2	plocha
Rakouska	Rakousko	k1gNnSc2	Rakousko
(	(	kIx(	(
<g/>
83	[number]	k4	83
871,1	[number]	k4	871,1
km2	km2	k4	km2
<g/>
)	)	kIx)	)
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
pahorkatiny	pahorkatina	k1gFnPc4	pahorkatina
a	a	k8xC	a
nížiny	nížina	k1gFnPc4	nížina
přibližně	přibližně	k6eAd1	přibližně
jedna	jeden	k4xCgFnSc1	jeden
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
32	[number]	k4	32
%	%	kIx~	%
leží	ležet	k5eAaImIp3nS	ležet
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
500	[number]	k4	500
m	m	kA	m
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
a	a	k8xC	a
43	[number]	k4	43
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
je	být	k5eAaImIp3nS	být
zalesněno	zalesněn	k2eAgNnSc1d1	zalesněno
<g/>
.	.	kIx.	.
</s>
<s>
Nejníže	nízce	k6eAd3	nízce
položené	položený	k2eAgNnSc1d1	položené
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
poblíž	poblíž	k6eAd1	poblíž
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
u	u	k7c2	u
Apetlonu	Apetlon	k1gInSc2	Apetlon
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Neusiedl	Neusiedl	k1gFnSc2	Neusiedl
am	am	k?	am
See	See	k1gFnSc2	See
–	–	k?	–
Burgenland	Burgenland	k1gInSc1	Burgenland
<g/>
)	)	kIx)	)
114	[number]	k4	114
m	m	kA	m
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
nejvýše	nejvýše	k6eAd1	nejvýše
položeným	položený	k2eAgNnSc7d1	položené
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
vrch	vrch	k1gInSc1	vrch
Grossglockner	Grossglocknra	k1gFnPc2	Grossglocknra
(	(	kIx(	(
<g/>
3798	[number]	k4	3798
m	m	kA	m
n	n	k0	n
<g/>
.	.	kIx.	.
m	m	kA	m
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pět	pět	k4xCc1	pět
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
geografických	geografický	k2eAgInPc2d1	geografický
útvarů	útvar	k1gInPc2	útvar
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
Východních	východní	k2eAgFnPc2d1	východní
Alp	Alpy	k1gFnPc2	Alpy
(	(	kIx(	(
<g/>
52	[number]	k4	52
600	[number]	k4	600
km2	km2	k4	km2
<g/>
,	,	kIx,	,
62,8	[number]	k4	62,8
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
Alpského	alpský	k2eAgNnSc2d1	alpské
a	a	k8xC	a
Karpatského	karpatský	k2eAgNnSc2d1	Karpatské
předpolí	předpolí	k1gNnSc2	předpolí
(	(	kIx(	(
<g/>
9500	[number]	k4	9500
km2	km2	k4	km2
<g/>
,	,	kIx,	,
11,3	[number]	k4	11,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Předpolí	předpolí	k1gNnSc1	předpolí
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
okrajové	okrajový	k2eAgFnSc2d1	okrajová
oblasti	oblast	k1gFnSc2	oblast
Panonské	panonský	k2eAgFnSc2d1	Panonská
pánve	pánev	k1gFnSc2	pánev
(	(	kIx(	(
<g/>
9500	[number]	k4	9500
km2	km2	k4	km2
<g/>
,	,	kIx,	,
11,3	[number]	k4	11,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Granitové	granitový	k2eAgFnPc1d1	granitová
a	a	k8xC	a
rulové	rulový	k2eAgFnPc1d1	rulová
roviny	rovina	k1gFnPc1	rovina
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
pohoří	pohoří	k1gNnSc2	pohoří
Českého	český	k2eAgInSc2d1	český
masivu	masiv	k1gInSc2	masiv
(	(	kIx(	(
<g/>
8500	[number]	k4	8500
km2	km2	k4	km2
<g/>
,	,	kIx,	,
10,2	[number]	k4	10,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
pánve	pánev	k1gFnSc2	pánev
(	(	kIx(	(
<g/>
3700	[number]	k4	3700
km2	km2	k4	km2
<g/>
,	,	kIx,	,
4,4	[number]	k4	4,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hranice	hranice	k1gFnSc1	hranice
===	===	k?	===
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
<g/>
:	:	kIx,	:
2832	[number]	k4	2832
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
784	[number]	k4	784
km	km	kA	km
<g/>
,	,	kIx,	,
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
362	[number]	k4	362
km	km	kA	km
<g/>
,	,	kIx,	,
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
430	[number]	k4	430
km	km	kA	km
<g/>
,	,	kIx,	,
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
366	[number]	k4	366
km	km	kA	km
<g/>
,	,	kIx,	,
Slovinskem	Slovinsko	k1gNnSc7	Slovinsko
330	[number]	k4	330
km	km	kA	km
<g/>
,	,	kIx,	,
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
164	[number]	k4	164
km	km	kA	km
<g/>
,	,	kIx,	,
Slovenskem	Slovensko	k1gNnSc7	Slovensko
91	[number]	k4	91
km	km	kA	km
a	a	k8xC	a
s	s	k7c7	s
Lichtenštejnskem	Lichtenštejnsko	k1gNnSc7	Lichtenštejnsko
35	[number]	k4	35
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Údolí	údolí	k1gNnSc1	údolí
Kleinwalsertal	Kleinwalsertal	k1gFnPc2	Kleinwalsertal
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
spolkové	spolkový	k2eAgFnSc3d1	spolková
zemi	zem	k1gFnSc3	zem
Vorarlbersko	Vorarlbersko	k1gNnSc1	Vorarlbersko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
polohy	poloha	k1gFnSc2	poloha
dostupné	dostupný	k2eAgFnSc2d1	dostupná
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
funkční	funkční	k2eAgFnSc7d1	funkční
enklávou	enkláva	k1gFnSc7	enkláva
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
obec	obec	k1gFnSc1	obec
Jungholz	Jungholza	k1gFnPc2	Jungholza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
funkční	funkční	k2eAgFnSc1d1	funkční
enkláva	enkláva	k1gFnSc1	enkláva
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
Švýcarsku	Švýcarsko	k1gNnSc3	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Samnaun	Samnauna	k1gFnPc2	Samnauna
nebyla	být	k5eNaImAgFnS	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
spojená	spojený	k2eAgFnSc1d1	spojená
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
jakoukoli	jakýkoli	k3yIgFnSc7	jakýkoli
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
přístupná	přístupný	k2eAgFnSc1d1	přístupná
pouze	pouze	k6eAd1	pouze
přes	přes	k7c4	přes
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odtud	odtud	k6eAd1	odtud
vymizela	vymizet	k5eAaPmAgFnS	vymizet
rétorománština	rétorománština	k1gFnSc1	rétorománština
a	a	k8xC	a
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
přijali	přijmout	k5eAaPmAgMnP	přijmout
tamní	tamní	k2eAgMnPc1d1	tamní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
dialekt	dialekt	k1gInSc1	dialekt
podobný	podobný	k2eAgInSc1d1	podobný
tyrolskému	tyrolský	k2eAgInSc3d1	tyrolský
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
silnice	silnice	k1gFnSc1	silnice
vedoucí	vedoucí	k1gFnSc1	vedoucí
do	do	k7c2	do
Samnaun	Samnauna	k1gFnPc2	Samnauna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
ubírá	ubírat	k5eAaImIp3nS	ubírat
výhradně	výhradně	k6eAd1	výhradně
po	po	k7c6	po
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
už	už	k6eAd1	už
zde	zde	k6eAd1	zde
kdysi	kdysi	k6eAd1	kdysi
byla	být	k5eAaImAgNnP	být
zavedena	zavést	k5eAaPmNgNnP	zavést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobném	podobný	k2eAgInSc6d1	podobný
stavu	stav	k1gInSc6	stav
jako	jako	k8xC	jako
Samnaun	Samnaun	k1gNnSc1	Samnaun
byla	být	k5eAaImAgFnS	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
obec	obec	k1gFnSc1	obec
Spiss	Spissa	k1gFnPc2	Spissa
na	na	k7c6	na
rakousko-švýcarské	rakousko-švýcarský	k2eAgFnSc6d1	rakousko-švýcarský
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
dosažitelná	dosažitelný	k2eAgFnSc1d1	dosažitelná
pouze	pouze	k6eAd1	pouze
přes	přes	k7c4	přes
Samnaun	Samnaun	k1gInSc4	Samnaun
a	a	k8xC	a
bojovala	bojovat	k5eAaImAgFnS	bojovat
se	se	k3xPyFc4	se
silným	silný	k2eAgInSc7d1	silný
odchodem	odchod	k1gInSc7	odchod
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgFnPc3d1	ostatní
enklávám	enkláva	k1gFnPc3	enkláva
měla	mít	k5eAaImAgFnS	mít
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc4d1	malá
možnosti	možnost	k1gFnPc4	možnost
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Hory	hora	k1gFnSc2	hora
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Deset	deset	k4xCc1	deset
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
Rakouska	Rakousko	k1gNnSc2	Rakousko
(	(	kIx(	(
<g/>
Rakouské	rakouský	k2eAgFnPc1d1	rakouská
Alpy	Alpy	k1gFnPc1	Alpy
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jezera	jezero	k1gNnSc2	jezero
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
Rakouska	Rakousko	k1gNnSc2	Rakousko
je	být	k5eAaImIp3nS	být
mělké	mělký	k2eAgNnSc1d1	mělké
stepní	stepní	k2eAgNnSc1d1	stepní
Neziderské	neziderský	k2eAgNnSc1d1	Neziderské
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
Burgenlandu	Burgenlando	k1gNnSc6	Burgenlando
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgMnSc3	který
náleží	náležet	k5eAaImIp3nS	náležet
77	[number]	k4	77
%	%	kIx~	%
svojí	svůj	k3xOyFgFnSc2	svůj
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
315	[number]	k4	315
km2	km2	k4	km2
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
jezera	jezero	k1gNnSc2	jezero
patří	patřit	k5eAaImIp3nP	patřit
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
větší	veliký	k2eAgNnPc1d2	veliký
jezera	jezero	k1gNnPc1	jezero
jsou	být	k5eAaImIp3nP	být
horská	horský	k2eAgNnPc1d1	horské
nebo	nebo	k8xC	nebo
podhorská	podhorský	k2eAgFnSc1d1	podhorská
Attersee	Attersee	k1gFnSc1	Attersee
s	s	k7c7	s
46	[number]	k4	46
km2	km2	k4	km2
a	a	k8xC	a
Traunsee	Traunsee	k1gInSc1	Traunsee
s	s	k7c7	s
24	[number]	k4	24
km2	km2	k4	km2
v	v	k7c6	v
Horních	horní	k2eAgInPc6d1	horní
Rakousech	Rakous	k1gInPc6	Rakous
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
je	být	k5eAaImIp3nS	být
také	také	k9	také
Bodamské	bodamský	k2eAgNnSc1d1	Bodamské
jezero	jezero	k1gNnSc1	jezero
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
536	[number]	k4	536
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Rakousku	Rakousko	k1gNnSc3	Rakousko
však	však	k9	však
náleží	náležet	k5eAaImIp3nS	náležet
pouze	pouze	k6eAd1	pouze
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
jezero	jezero	k1gNnSc1	jezero
totiž	totiž	k9	totiž
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
německými	německý	k2eAgFnPc7d1	německá
spolkovými	spolkový	k2eAgFnPc7d1	spolková
zeměmi	zem	k1gFnPc7	zem
Bavorskem	Bavorsko	k1gNnSc7	Bavorsko
a	a	k8xC	a
Bádensko-Württemberskem	Bádensko-Württembersek	k1gInSc7	Bádensko-Württembersek
a	a	k8xC	a
se	s	k7c7	s
Švýcarskem	Švýcarsko	k1gNnSc7	Švýcarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jezera	jezero	k1gNnPc1	jezero
mají	mít	k5eAaImIp3nP	mít
vedle	vedle	k7c2	vedle
hor	hora	k1gFnPc2	hora
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
také	také	k9	také
v	v	k7c6	v
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Korutanská	korutanský	k2eAgNnPc4d1	korutanské
jezera	jezero	k1gNnPc4	jezero
a	a	k8xC	a
oblast	oblast	k1gFnSc4	oblast
Salzkammergut	Salzkammergut	k2eAgMnSc1d1	Salzkammergut
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Wörthersee	Wörtherse	k1gFnPc1	Wörtherse
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
Korutan	Korutany	k1gInPc2	Korutany
<g/>
,	,	kIx,	,
Millstätter	Millstättra	k1gFnPc2	Millstättra
See	See	k1gFnPc2	See
<g/>
,	,	kIx,	,
Ossiacher	Ossiachra	k1gFnPc2	Ossiachra
See	See	k1gFnSc2	See
a	a	k8xC	a
Weißensee	Weißense	k1gFnSc2	Weißense
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
jezera	jezero	k1gNnSc2	jezero
Mondsee	Mondse	k1gFnSc2	Mondse
a	a	k8xC	a
Wolfgangsee	Wolfgangse	k1gFnSc2	Wolfgangse
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Salcburska	Salcbursk	k1gInSc2	Salcbursk
a	a	k8xC	a
Horních	horní	k2eAgInPc2d1	horní
Rakous	Rakousy	k1gInPc2	Rakousy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Řeky	Řek	k1gMnPc4	Řek
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
Rakouska	Rakousko	k1gNnSc2	Rakousko
(	(	kIx(	(
<g/>
80	[number]	k4	80
566	[number]	k4	566
km2	km2	k4	km2
<g/>
,	,	kIx,	,
96	[number]	k4	96
%	%	kIx~	%
území	území	k1gNnSc6	území
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odvodňována	odvodňovat	k5eAaImNgFnS	odvodňovat
Dunajem	Dunaj	k1gInSc7	Dunaj
do	do	k7c2	do
Černého	černé	k1gNnSc2	černé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
Vorarlbersko	Vorarlbersko	k1gNnSc1	Vorarlbersko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
(	(	kIx(	(
<g/>
2366	[number]	k4	2366
km2	km2	k4	km2
<g/>
)	)	kIx)	)
odvodněno	odvodnit	k5eAaPmNgNnS	odvodnit
Rýnem	Rýn	k1gInSc7	Rýn
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velké	velký	k2eAgInPc1d1	velký
přítoky	přítok	k1gInPc1	přítok
Dunaje	Dunaj	k1gInSc2	Dunaj
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
(	(	kIx(	(
<g/>
od	od	k7c2	od
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Inn	Inn	k?	Inn
(	(	kIx(	(
<g/>
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
většinu	většina	k1gFnSc4	většina
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
)	)	kIx)	)
s	s	k7c7	s
přítokem	přítok	k1gInSc7	přítok
Salzach	Salzach	k1gInSc1	Salzach
(	(	kIx(	(
<g/>
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
většinu	většina	k1gFnSc4	většina
Salcburska	Salcbursk	k1gInSc2	Salcbursk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
Malý	malý	k2eAgInSc1d1	malý
Mühl	Mühl	k1gInSc1	Mühl
<g/>
,	,	kIx,	,
Rodl	Rodl	k1gInSc1	Rodl
<g/>
,	,	kIx,	,
Aist	Aist	k1gInSc1	Aist
<g/>
,	,	kIx,	,
Kamp	kamp	k1gInSc1	kamp
<g/>
,	,	kIx,	,
Schmida	Schmida	k1gFnSc1	Schmida
a	a	k8xC	a
Rußbach	Rußbach	k1gInSc1	Rußbach
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Dyje	Dyje	k1gFnSc1	Dyje
(	(	kIx(	(
<g/>
Thaya	Thaya	k1gFnSc1	Thaya
<g/>
)	)	kIx)	)
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
(	(	kIx(	(
<g/>
March	March	k1gInSc1	March
<g/>
)	)	kIx)	)
na	na	k7c6	na
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
hranicích	hranice	k1gFnPc6	hranice
<g/>
,	,	kIx,	,
odvodňují	odvodňovat	k5eAaImIp3nP	odvodňovat
oblasti	oblast	k1gFnPc4	oblast
Horních	horní	k2eAgInPc2d1	horní
a	a	k8xC	a
Dolních	dolní	k2eAgInPc2d1	dolní
Rakous	Rakousy	k1gInPc2	Rakousy
položené	položená	k1gFnSc2	položená
severně	severně	k6eAd1	severně
od	od	k7c2	od
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aschach	Aschach	k1gMnSc1	Aschach
<g/>
,	,	kIx,	,
Traun	Traun	k1gMnSc1	Traun
<g/>
,	,	kIx,	,
Enže	Enže	k1gFnSc1	Enže
(	(	kIx(	(
<g/>
Enns	Enns	k1gInSc1	Enns
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ybbs	Ybbs	k1gInSc1	Ybbs
<g/>
,	,	kIx,	,
Erlauf	Erlauf	k1gInSc1	Erlauf
<g/>
,	,	kIx,	,
Traisen	Traisen	k1gInSc1	Traisen
<g/>
,	,	kIx,	,
Vídeňka	Vídeňka	k1gFnSc1	Vídeňka
(	(	kIx(	(
<g/>
Wien	Wien	k1gInSc1	Wien
<g/>
)	)	kIx)	)
a	a	k8xC	a
Fischa	Fischa	k1gFnSc1	Fischa
odvodňují	odvodňovat	k5eAaImIp3nP	odvodňovat
severní	severní	k2eAgNnSc1d1	severní
Štýrsko	Štýrsko	k1gNnSc1	Štýrsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
části	část	k1gFnPc1	část
Horních	horní	k2eAgInPc2d1	horní
a	a	k8xC	a
Dolních	dolní	k2eAgInPc2d1	dolní
Rakous	Rakousy	k1gInPc2	Rakousy
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
<g/>
Přítoky	přítok	k1gInPc1	přítok
Dunaje	Dunaj	k1gInSc2	Dunaj
ústící	ústící	k2eAgInPc1d1	ústící
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Lech	Lech	k1gMnSc1	Lech
pramení	pramenit	k5eAaImIp3nS	pramenit
ve	v	k7c6	v
Vorarlbersku	Vorarlbersko	k1gNnSc6	Vorarlbersko
a	a	k8xC	a
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Litava	Litava	k1gFnSc1	Litava
(	(	kIx(	(
<g/>
Leitha	Leitha	k1gFnSc1	Leitha
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rába	Rába	k1gMnSc1	Rába
(	(	kIx(	(
<g/>
Raab	Raab	k1gMnSc1	Raab
<g/>
)	)	kIx)	)
s	s	k7c7	s
přítokem	přítok	k1gInSc7	přítok
Pinka	pinka	k1gFnSc1	pinka
odvodňují	odvodňovat	k5eAaImIp3nP	odvodňovat
Burgenland	Burgenland	k1gInSc4	Burgenland
a	a	k8xC	a
přilehlé	přilehlý	k2eAgFnSc6d1	přilehlá
části	část	k1gFnSc6	část
Dolních	dolní	k2eAgInPc2d1	dolní
Rakous	Rakousy	k1gInPc2	Rakousy
a	a	k8xC	a
Štýrska	Štýrsko	k1gNnSc2	Štýrsko
a	a	k8xC	a
vlévají	vlévat	k5eAaImIp3nP	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
Dunaje	Dunaj	k1gInSc2	Dunaj
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mura	mura	k1gFnSc1	mura
(	(	kIx(	(
<g/>
Mur	mur	k1gInSc1	mur
<g/>
)	)	kIx)	)
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
salcburské	salcburský	k2eAgNnSc1d1	salcburské
údolí	údolí	k1gNnSc1	údolí
Lungau	Lungaus	k1gInSc2	Lungaus
a	a	k8xC	a
většinu	většina	k1gFnSc4	většina
Štýrska	Štýrsko	k1gNnSc2	Štýrsko
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
vody	voda	k1gFnPc1	voda
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
přijímá	přijímat	k5eAaImIp3nS	přijímat
</s>
</p>
<p>
<s>
Dráva	Dráva	k1gFnSc1	Dráva
(	(	kIx(	(
<g/>
Drau	Draus	k1gInSc2	Draus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
Korutany	Korutany	k1gInPc4	Korutany
a	a	k8xC	a
Východní	východní	k2eAgNnSc4d1	východní
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
a	a	k8xC	a
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
se	s	k7c7	s
Srbskem	Srbsko	k1gNnSc7	Srbsko
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
<g/>
Úmoří	úmoří	k1gNnSc1	úmoří
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Rýn	Rýn	k1gInSc1	Rýn
(	(	kIx(	(
<g/>
Rhein	Rhein	k1gInSc1	Rhein
<g/>
)	)	kIx)	)
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
většinu	většina	k1gFnSc4	většina
Vorarlberska	Vorarlbersko	k1gNnSc2	Vorarlbersko
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
Bodamským	bodamský	k2eAgNnSc7d1	Bodamské
jezerem	jezero	k1gNnSc7	jezero
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lužnice	Lužnice	k1gFnSc1	Lužnice
(	(	kIx(	(
<g/>
Lainsitz	Lainsitz	k1gInSc1	Lainsitz
<g/>
)	)	kIx)	)
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
nejhořejším	horní	k2eAgInSc6d3	nejhořejší
úseku	úsek	k1gInSc6	úsek
odvodňuje	odvodňovat	k5eAaImIp3nS	odvodňovat
Vitorazsko	Vitorazsko	k1gNnSc1	Vitorazsko
a	a	k8xC	a
teče	téct	k5eAaImIp3nS	téct
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
pak	pak	k9	pak
do	do	k7c2	do
Labe	Labe	k1gNnSc2	Labe
<g/>
.	.	kIx.	.
<g/>
Žádná	žádný	k3yNgFnSc1	žádný
část	část	k1gFnSc1	část
Rakouska	Rakousko	k1gNnSc2	Rakousko
není	být	k5eNaImIp3nS	být
odvodňována	odvodňovat	k5eAaImNgFnS	odvodňovat
do	do	k7c2	do
jemu	on	k3xPp3gMnSc3	on
nejbližšího	blízký	k2eAgNnSc2d3	nejbližší
moře	moře	k1gNnSc2	moře
Jaderského	jaderský	k2eAgNnSc2d1	Jaderské
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
vede	vést	k5eAaImIp3nS	vést
právě	právě	k9	právě
po	po	k7c6	po
rozvodí	rozvodí	k1gNnSc6	rozvodí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Rakouské	rakouský	k2eAgNnSc1d1	rakouské
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
smíšení	smíšení	k1gNnSc4	smíšení
oceánského	oceánský	k2eAgNnSc2d1	oceánské
a	a	k8xC	a
kontinentálního	kontinentální	k2eAgNnSc2d1	kontinentální
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
panonské	panonský	k2eAgNnSc1d1	panonské
klima	klima	k1gNnSc1	klima
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
zvláštnostem	zvláštnost	k1gFnPc3	zvláštnost
těchto	tento	k3xDgFnPc6	tento
podnebí	podnebí	k1gNnSc6	podnebí
je	být	k5eAaImIp3nS	být
východní	východní	k2eAgNnSc4d1	východní
Rakousko	Rakousko	k1gNnSc4	Rakousko
známé	známý	k2eAgInPc1d1	známý
mrazivými	mrazivý	k2eAgFnPc7d1	mrazivá
zimami	zima	k1gFnPc7	zima
a	a	k8xC	a
horkými	horký	k2eAgNnPc7d1	horké
léty	léto	k1gNnPc7	léto
s	s	k7c7	s
celoročně	celoročně	k6eAd1	celoročně
nízkými	nízký	k2eAgFnPc7d1	nízká
srážkami	srážka	k1gFnPc7	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Západ	západ	k1gInSc1	západ
země	zem	k1gFnSc2	zem
podléhá	podléhat	k5eAaImIp3nS	podléhat
zpravidla	zpravidla	k6eAd1	zpravidla
méně	málo	k6eAd2	málo
silným	silný	k2eAgFnPc3d1	silná
podnebním	podnební	k2eAgFnPc3d1	podnební
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
zimy	zima	k1gFnPc1	zima
většinou	většinou	k6eAd1	většinou
mírnější	mírný	k2eAgFnPc1d2	mírnější
a	a	k8xC	a
léta	léto	k1gNnPc4	léto
spíše	spíše	k9	spíše
teplá	teplat	k5eAaImIp3nS	teplat
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
zde	zde	k6eAd1	zde
také	také	k9	také
oblasti	oblast	k1gFnPc1	oblast
bohaté	bohatý	k2eAgFnPc1d1	bohatá
na	na	k7c4	na
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geografických	geografický	k2eAgFnPc2d1	geografická
podmínek	podmínka	k1gFnPc2	podmínka
vychází	vycházet	k5eAaImIp3nS	vycházet
další	další	k2eAgFnSc1d1	další
klimatická	klimatický	k2eAgFnSc1d1	klimatická
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
alpínské	alpínský	k2eAgNnSc1d1	alpínské
klima	klima	k1gNnSc1	klima
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
silnější	silný	k2eAgFnSc4d2	silnější
zimu	zima	k1gFnSc4	zima
než	než	k8xS	než
na	na	k7c6	na
hlouběji	hluboko	k6eAd2	hluboko
položeném	položený	k2eAgInSc6d1	položený
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
zajímavostí	zajímavost	k1gFnSc7	zajímavost
jsou	být	k5eAaImIp3nP	být
občasné	občasný	k2eAgInPc1d1	občasný
severní	severní	k2eAgInPc1d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgInPc1d1	jižní
větry	vítr	k1gInPc1	vítr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
polárně	polárně	k6eAd1	polárně
ledové	ledový	k2eAgFnPc1d1	ledová
a	a	k8xC	a
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
s	s	k7c7	s
sebou	se	k3xPyFc7	se
někdy	někdy	k6eAd1	někdy
přinášejí	přinášet	k5eAaImIp3nP	přinášet
saharský	saharský	k2eAgInSc4d1	saharský
písek	písek	k1gInSc4	písek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
Rakousko	Rakousko	k1gNnSc1	Rakousko
zcela	zcela	k6eAd1	zcela
oprávněně	oprávněně	k6eAd1	oprávněně
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
středoevropskému	středoevropský	k2eAgNnSc3d1	středoevropské
přechodovému	přechodový	k2eAgNnSc3d1	přechodové
klimatu	klima	k1gNnSc3	klima
<g/>
,	,	kIx,	,
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
k	k	k7c3	k
Alpám	Alpy	k1gFnPc3	Alpy
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
k	k	k7c3	k
Panonské	panonský	k2eAgFnSc3d1	Panonská
nížině	nížina	k1gFnSc3	nížina
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Teploty	teplota	k1gFnPc1	teplota
====	====	k?	====
</s>
</p>
<p>
<s>
Když	když	k8xS	když
působí	působit	k5eAaImIp3nS	působit
stabilní	stabilní	k2eAgFnSc1d1	stabilní
tlaková	tlakový	k2eAgFnSc1d1	tlaková
výše	výše	k1gFnSc1	výše
z	z	k7c2	z
východu	východ	k1gInSc2	východ
(	(	kIx(	(
<g/>
ideálně	ideálně	k6eAd1	ideálně
"	"	kIx"	"
<g/>
Omega-Hoch	Omega-Hoch	k1gInSc1	Omega-Hoch
<g/>
"	"	kIx"	"
či	či	k8xC	či
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
posledních	poslední	k2eAgNnPc2d1	poslední
písmen	písmeno	k1gNnPc2	písmeno
řecké	řecký	k2eAgFnSc2d1	řecká
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
člověk	člověk	k1gMnSc1	člověk
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
dlouhým	dlouhý	k2eAgMnSc7d1	dlouhý
a	a	k8xC	a
nerušeným	rušený	k2eNgNnSc7d1	nerušené
sluníčkem	sluníčko	k1gNnSc7	sluníčko
a	a	k8xC	a
až	až	k6eAd1	až
dvěma	dva	k4xCgInPc7	dva
týdny	týden	k1gInPc7	týden
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
tlaková	tlakový	k2eAgFnSc1d1	tlaková
výše	výše	k1gFnSc1	výše
svit	svit	k1gInSc4	svit
slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
ostré	ostrý	k2eAgInPc1d1	ostrý
mrazy	mráz	k1gInPc1	mráz
(	(	kIx(	(
<g/>
do	do	k7c2	do
−	−	k?	−
<g/>
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
svitu	svit	k1gInSc2	svit
slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
kupříkladu	kupříkladu	k6eAd1	kupříkladu
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Topná	topný	k2eAgFnSc1d1	topná
sezóna	sezóna	k1gFnSc1	sezóna
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
vysokou	vysoký	k2eAgFnSc4d1	vysoká
spotřebu	spotřeba	k1gFnSc4	spotřeba
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
teplota	teplota	k1gFnSc1	teplota
naměřená	naměřený	k2eAgFnSc1d1	naměřená
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
:	:	kIx,	:
Sonnblick-Gipfel	Sonnblick-Gipfel	k1gMnSc1	Sonnblick-Gipfel
(	(	kIx(	(
<g/>
SBG	SBG	kA	SBG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
−	−	k?	−
<g/>
37	[number]	k4	37
<g/>
,	,	kIx,	,
<g/>
2	[number]	k4	2
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejnižší	nízký	k2eAgFnSc1d3	nejnižší
teplota	teplota	k1gFnSc1	teplota
naměřená	naměřený	k2eAgFnSc1d1	naměřená
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
:	:	kIx,	:
Zwettl	Zwettl	k1gMnSc1	Zwettl
(	(	kIx(	(
<g/>
NÖ	NÖ	kA	NÖ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
−	−	k?	−
<g/>
36	[number]	k4	36
<g/>
,	,	kIx,	,
<g/>
6	[number]	k4	6
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
naměřená	naměřený	k2eAgFnSc1d1	naměřená
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
:	:	kIx,	:
Bad	Bad	k1gMnSc1	Bad
Deutsch-Altenburg	Deutsch-Altenburg	k1gMnSc1	Deutsch-Altenburg
(	(	kIx(	(
<g/>
K	k	k7c3	k
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
+	+	kIx~	+
40,5	[number]	k4	40,5
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Srážky	srážka	k1gFnPc1	srážka
====	====	k?	====
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
směrem	směr	k1gInSc7	směr
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
slábne	slábnout	k5eAaImIp3nS	slábnout
oceánské	oceánský	k2eAgNnSc1d1	oceánské
klima	klima	k1gNnSc1	klima
<g/>
,	,	kIx,	,
klesá	klesat	k5eAaImIp3nS	klesat
i	i	k9	i
průměrné	průměrný	k2eAgNnSc1d1	průměrné
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
např.	např.	kA	např.
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
poloviční	poloviční	k2eAgFnPc4d1	poloviční
srážky	srážka	k1gFnPc4	srážka
oproti	oproti	k7c3	oproti
Salzburgu	Salzburg	k1gInSc3	Salzburg
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
a	a	k8xC	a
jihovýchodních	jihovýchodní	k2eAgFnPc6d1	jihovýchodní
částech	část	k1gFnPc6	část
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
již	již	k6eAd1	již
projevuje	projevovat	k5eAaImIp3nS	projevovat
panonské	panonský	k2eAgNnSc1d1	panonské
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
klima	klima	k1gNnSc1	klima
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
jezerních	jezerní	k2eAgFnPc6d1	jezerní
oblastech	oblast	k1gFnPc6	oblast
částečně	částečně	k6eAd1	částečně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
středomořským	středomořský	k2eAgNnSc7d1	středomořské
klimatem	klima	k1gNnSc7	klima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celoročně	celoročně	k6eAd1	celoročně
se	se	k3xPyFc4	se
také	také	k9	také
oblasti	oblast	k1gFnPc1	oblast
podél	podél	k7c2	podél
alpských	alpský	k2eAgInPc2d1	alpský
hřebenů	hřeben	k1gInPc2	hřeben
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
vysokými	vysoký	k2eAgFnPc7d1	vysoká
srážkami	srážka	k1gFnPc7	srážka
<g/>
,	,	kIx,	,
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
poloh	poloha	k1gFnPc2	poloha
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
tlakem	tlak	k1gInSc7	tlak
vanou	vanout	k5eAaImIp3nP	vanout
vlhké	vlhký	k2eAgInPc1d1	vlhký
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
proudy	proud	k1gInPc1	proud
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
či	či	k8xC	či
jihu	jih	k1gInSc2	jih
a	a	k8xC	a
zmizí	zmizet	k5eAaPmIp3nS	zmizet
zase	zase	k9	zase
jako	jako	k9	jako
srážka	srážka	k1gFnSc1	srážka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
vysoké	vysoký	k2eAgNnSc4d1	vysoké
množství	množství	k1gNnSc4	množství
sněhu	sníh	k1gInSc2	sníh
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
i	i	k8xC	i
v	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
kalamitní	kalamitní	k2eAgFnSc6d1	kalamitní
oblasti	oblast	k1gFnSc6	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
i	i	k8xC	i
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
jsou	být	k5eAaImIp3nP	být
následkem	následkem	k7c2	následkem
vysokých	vysoký	k2eAgFnPc2d1	vysoká
srážek	srážka	k1gFnPc2	srážka
silné	silný	k2eAgInPc1d1	silný
deště	dešť	k1gInPc1	dešť
<g/>
,	,	kIx,	,
eroze	eroze	k1gFnSc1	eroze
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
záplavy	záplava	k1gFnSc2	záplava
<g/>
.	.	kIx.	.
</s>
<s>
Kraje	kraj	k1gInPc1	kraj
s	s	k7c7	s
nejsilnějšími	silný	k2eAgInPc7d3	nejsilnější
dešti	dešť	k1gInPc7	dešť
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
celé	celý	k2eAgNnSc1d1	celé
alpské	alpský	k2eAgNnSc1d1	alpské
předhůří	předhůří	k1gNnSc1	předhůří
<g/>
,	,	kIx,	,
tyrolské	tyrolský	k2eAgFnPc1d1	tyrolská
nížiny	nížina	k1gFnPc1	nížina
<g/>
,	,	kIx,	,
tyrolský	tyrolský	k2eAgInSc1d1	tyrolský
okres	okres	k1gInSc1	okres
Reutte	Reutt	k1gInSc5	Reutt
a	a	k8xC	a
Bregenzský	Bregenzský	k2eAgInSc4d1	Bregenzský
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
spadne	spadnout	k5eAaPmIp3nS	spadnout
až	až	k9	až
3000	[number]	k4	3000
mm	mm	kA	mm
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
nebo	nebo	k8xC	nebo
sněhu	sníh	k1gInSc2	sníh
za	za	k7c4	za
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
900	[number]	k4	900
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
všechno	všechen	k3xTgNnSc1	všechen
–	–	k?	–
od	od	k7c2	od
sněžení	sněžení	k1gNnSc2	sněžení
po	po	k7c4	po
horka	horko	k1gNnPc4	horko
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgFnPc1d1	denní
teploty	teplota	k1gFnPc1	teplota
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
nezřídka	nezřídka	k6eAd1	nezřídka
vystoupit	vystoupit	k5eAaPmF	vystoupit
i	i	k9	i
přes	přes	k7c4	přes
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
vlhkost	vlhkost	k1gFnSc1	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
dešťových	dešťový	k2eAgInPc2d1	dešťový
mraků	mrak	k1gInPc2	mrak
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
k	k	k7c3	k
náhlým	náhlý	k2eAgFnPc3d1	náhlá
bouřkám	bouřka	k1gFnPc3	bouřka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
sousední	sousední	k2eAgFnSc2d1	sousední
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
častou	častý	k2eAgFnSc7d1	častá
obětí	oběť	k1gFnSc7	oběť
poruch	porucha	k1gFnPc2	porucha
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
mnoha	mnoho	k4c3	mnoho
experty	expert	k1gMnPc4	expert
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
důsledky	důsledek	k1gInPc1	důsledek
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
(	(	kIx(	(
<g/>
průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
trvale	trvale	k6eAd1	trvale
zvedají	zvedat	k5eAaImIp3nP	zvedat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
silným	silný	k2eAgInPc3d1	silný
dešťům	dešť	k1gInPc3	dešť
byly	být	k5eAaImAgInP	být
již	již	k9	již
několikrát	několikrát	k6eAd1	několikrát
ničivé	ničivý	k2eAgFnPc1d1	ničivá
záplavy	záplava	k1gFnPc1	záplava
a	a	k8xC	a
sesuvy	sesuv	k1gInPc1	sesuv
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
i	i	k8xC	i
oběti	oběť	k1gFnPc1	oběť
na	na	k7c6	na
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Náhodně	náhodně	k6eAd1	náhodně
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
bouřím	bouř	k1gFnPc3	bouř
síly	síla	k1gFnSc2	síla
orkánu	orkán	k1gInSc2	orkán
a	a	k8xC	a
ohromným	ohromný	k2eAgFnPc3d1	ohromná
vánicím	vánice	k1gFnPc3	vánice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
obcí	obec	k1gFnPc2	obec
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
odříznuto	odříznut	k2eAgNnSc1d1	odříznuto
od	od	k7c2	od
světa	svět	k1gInSc2	svět
a	a	k8xC	a
množí	množit	k5eAaImIp3nP	množit
se	se	k3xPyFc4	se
také	také	k9	také
pády	pád	k1gInPc1	pád
lavin	lavina	k1gFnPc2	lavina
<g/>
.	.	kIx.	.
</s>
<s>
Zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
sužovala	sužovat	k5eAaImAgFnS	sužovat
ale	ale	k9	ale
i	i	k9	i
častá	častý	k2eAgNnPc4d1	časté
sucha	sucho	k1gNnPc4	sucho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Státní	státní	k2eAgNnSc1d1	státní
zřízení	zřízení	k1gNnSc1	zřízení
a	a	k8xC	a
vrcholné	vrcholný	k2eAgInPc1d1	vrcholný
orgány	orgán	k1gInPc1	orgán
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
spolkovou	spolkový	k2eAgFnSc7d1	spolková
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
spolkový	spolkový	k2eAgMnSc1d1	spolkový
prezident	prezident	k1gMnSc1	prezident
volený	volený	k2eAgInSc4d1	volený
na	na	k7c4	na
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
v	v	k7c6	v
přímých	přímý	k2eAgFnPc6d1	přímá
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Spolkový	spolkový	k2eAgMnSc1d1	spolkový
prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
členy	člen	k1gMnPc4	člen
spolkové	spolkový	k2eAgFnSc2d1	spolková
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Rakouský	rakouský	k2eAgInSc1d1	rakouský
parlament	parlament	k1gInSc1	parlament
je	být	k5eAaImIp3nS	být
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
Spolkové	spolkový	k2eAgFnSc2d1	spolková
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
členy	člen	k1gInPc7	člen
Spolkové	spolkový	k2eAgFnSc2d1	spolková
rady	rada	k1gFnSc2	rada
volí	volit	k5eAaImIp3nP	volit
zemské	zemský	k2eAgInPc1d1	zemský
sněmy	sněm	k1gInPc1	sněm
rakouských	rakouský	k2eAgFnPc2d1	rakouská
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
spolková	spolkový	k2eAgFnSc1d1	spolková
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
Národní	národní	k2eAgFnSc3d1	národní
radě	rada	k1gFnSc3	rada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
spolkový	spolkový	k2eAgMnSc1d1	spolkový
kancléř	kancléř	k1gMnSc1	kancléř
<g/>
,	,	kIx,	,
členy	člen	k1gMnPc7	člen
vlády	vláda	k1gFnSc2	vláda
jsou	být	k5eAaImIp3nP	být
vicekancléř	vicekancléř	k1gMnSc1	vicekancléř
a	a	k8xC	a
spolkoví	spolkový	k2eAgMnPc1d1	spolkový
ministři	ministr	k1gMnPc1	ministr
(	(	kIx(	(
<g/>
vicekancléř	vicekancléř	k1gMnSc1	vicekancléř
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
ministrem	ministr	k1gMnSc7	ministr
jednoho	jeden	k4xCgInSc2	jeden
rezortu	rezort	k1gInSc2	rezort
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
spolkovým	spolkový	k2eAgMnSc7d1	spolkový
prezidentem	prezident	k1gMnSc7	prezident
Alexander	Alexandra	k1gFnPc2	Alexandra
Van	van	k1gInSc1	van
der	drát	k5eAaImRp2nS	drát
Bellen	Bellen	k1gInSc4	Bellen
<g/>
.	.	kIx.	.
</s>
<s>
Spolkovým	spolkový	k2eAgMnSc7d1	spolkový
kancléřem	kancléř	k1gMnSc7	kancléř
se	se	k3xPyFc4	se
dne	den	k1gInSc2	den
18.	[number]	k4	18.
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
stal	stát	k5eAaPmAgMnS	stát
31letý	31letý	k2eAgMnSc1d1	31letý
předseda	předseda	k1gMnSc1	předseda
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
lidové	lidový	k2eAgFnSc2d1	lidová
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
ÖVP	ÖVP	kA	ÖVP
<g/>
)	)	kIx)	)
Sebastian	Sebastian	k1gMnSc1	Sebastian
Kurz	Kurz	k1gMnSc1	Kurz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
května	květen	k1gInSc2	květen
2019	[number]	k4	2019
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
vlády	vláda	k1gFnSc2	vláda
Sebastiana	Sebastian	k1gMnSc2	Sebastian
Kurze	kurz	k1gInSc6	kurz
oznámil	oznámit	k5eAaPmAgMnS	oznámit
prezident	prezident	k1gMnSc1	prezident
Van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Bellen	Bellen	k2eAgMnSc1d1	Bellen
<g/>
,	,	kIx,	,
že	že	k8xS	že
pověří	pověřit	k5eAaPmIp3nS	pověřit
Brigitte	Brigitte	k1gFnSc4	Brigitte
Bierleinovou	Bierleinový	k2eAgFnSc4d1	Bierleinový
sestavením	sestavení	k1gNnSc7	sestavení
úřednické	úřednický	k2eAgFnSc2d1	úřednická
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zemi	zem	k1gFnSc4	zem
povede	vést	k5eAaImIp3nS	vést
až	až	k9	až
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2019.	[number]	k4	2019.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Politické	politický	k2eAgNnSc1d1	politické
a	a	k8xC	a
správní	správní	k2eAgNnSc1d1	správní
členění	členění	k1gNnSc1	členění
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
devíti	devět	k4xCc2	devět
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zase	zase	k9	zase
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
80	[number]	k4	80
okresů	okres	k1gInPc2	okres
a	a	k8xC	a
15	[number]	k4	15
statutárních	statutární	k2eAgNnPc2d1	statutární
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Okresy	okres	k1gInPc1	okres
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
obce	obec	k1gFnPc4	obec
<g/>
,	,	kIx,	,
města	město	k1gNnPc4	město
či	či	k8xC	či
městysy	městys	k1gInPc4	městys
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vídeň	Vídeň	k1gFnSc1	Vídeň
je	být	k5eAaImIp3nS	být
územně	územně	k6eAd1	územně
zcela	zcela	k6eAd1	zcela
obklopena	obklopit	k5eAaPmNgFnS	obklopit
Dolními	dolní	k2eAgInPc7d1	dolní
Rakousy	Rakousy	k1gInPc7	Rakousy
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgNnSc1d1	východní
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
sice	sice	k8xC	sice
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
spolkové	spolkový	k2eAgFnSc3d1	spolková
zemi	zem	k1gFnSc3	zem
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
fyzicky	fyzicky	k6eAd1	fyzicky
spojeno	spojit	k5eAaPmNgNnS	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kuriozita	kuriozita	k1gFnSc1	kuriozita
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Jižní	jižní	k2eAgNnSc4d1	jižní
Tyrolsko	Tyrolsko	k1gNnSc4	Tyrolsko
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Východní	východní	k2eAgNnSc1d1	východní
Tyrolsko	Tyrolsko	k1gNnSc1	Tyrolsko
exklávou	exkláva	k1gFnSc7	exkláva
Tyrolska	Tyrolsko	k1gNnSc2	Tyrolsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Rovnost	rovnost	k1gFnSc1	rovnost
pohlaví	pohlaví	k1gNnSc2	pohlaví
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
je	být	k5eAaImIp3nS	být
rovnost	rovnost	k1gFnSc1	rovnost
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
pevně	pevně	k6eAd1	pevně
zakotvena	zakotvit	k5eAaPmNgFnS	zakotvit
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
vyplývající	vyplývající	k2eAgFnPc4d1	vyplývající
výjimky	výjimka	k1gFnPc4	výjimka
jsou	být	k5eAaImIp3nP	být
regulace	regulace	k1gFnSc1	regulace
důchodu	důchod	k1gInSc2	důchod
a	a	k8xC	a
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
smějí	smát	k5eAaImIp3nP	smát
jít	jít	k5eAaImF	jít
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
o	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
odchod	odchod	k1gInSc1	odchod
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
státních	státní	k2eAgMnPc2d1	státní
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
tzv.	tzv.	kA	tzv.
definitivu	definitiva	k1gFnSc4	definitiva
<g/>
.	.	kIx.	.
</s>
<s>
Právně	právně	k6eAd1	právně
je	být	k5eAaImIp3nS	být
však	však	k9	však
podloženo	podložen	k2eAgNnSc1d1	podloženo
postupné	postupný	k2eAgNnSc1d1	postupné
zvyšování	zvyšování	k1gNnSc1	zvyšování
nástupního	nástupní	k2eAgInSc2d1	nástupní
věku	věk	k1gInSc2	věk
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2027.	[number]	k4	2027.
</s>
</p>
<p>
<s>
Skoro	skoro	k6eAd1	skoro
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
Rakouska	Rakousko	k1gNnSc2	Rakousko
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
mzda	mzda	k1gFnSc1	mzda
žen	žena	k1gFnPc2	žena
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
mzda	mzda	k1gFnSc1	mzda
muže	muž	k1gMnSc2	muž
na	na	k7c6	na
odpovídajícím	odpovídající	k2eAgNnSc6d1	odpovídající
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
výjimku	výjimek	k1gInSc2	výjimek
tvoří	tvořit	k5eAaImIp3nP	tvořit
úřady	úřad	k1gInPc1	úřad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
neúplného	úplný	k2eNgNnSc2d1	neúplné
prosazení	prosazení	k1gNnSc2	prosazení
rovnosti	rovnost	k1gFnSc2	rovnost
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
žen	žena	k1gFnPc2	žena
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c4	na
částečný	částečný	k2eAgInSc4d1	částečný
pracovní	pracovní	k2eAgInSc4d1	pracovní
úvazek	úvazek	k1gInSc4	úvazek
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
mají	mít	k5eAaImIp3nP	mít
nižší	nízký	k2eAgFnPc4d2	nižší
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
pracovní	pracovní	k2eAgInSc4d1	pracovní
postup	postup	k1gInSc4	postup
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k2eAgFnSc1d1	vedoucí
pozice	pozice	k1gFnSc1	pozice
tak	tak	k9	tak
obsazují	obsazovat	k5eAaImIp3nP	obsazovat
většinou	většinou	k6eAd1	většinou
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Tarifní	tarifní	k2eAgFnPc1d1	tarifní
mzdy	mzda	k1gFnPc1	mzda
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
využívají	využívat	k5eAaImIp3nP	využívat
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
ze	z	k7c2	z
sta	sto	k4xCgNnPc4	sto
možnost	možnost	k1gFnSc4	možnost
vzít	vzít	k5eAaPmF	vzít
si	se	k3xPyFc3	se
tzv.	tzv.	kA	tzv.
otcovskou	otcovský	k2eAgFnSc4d1	otcovská
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
jsou	být	k5eAaImIp3nP	být
připravována	připravován	k2eAgNnPc1d1	připravováno
různá	různý	k2eAgNnPc1d1	různé
podpůrná	podpůrný	k2eAgNnPc1d1	podpůrné
opatření	opatření	k1gNnPc1	opatření
(	(	kIx(	(
<g/>
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
diskriminace	diskriminace	k1gFnSc1	diskriminace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
při	při	k7c6	při
obsazování	obsazování	k1gNnSc6	obsazování
veřejných	veřejný	k2eAgNnPc2d1	veřejné
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
by	by	kYmCp3nS	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
ženy	žena	k1gFnPc4	žena
stejné	stejný	k2eAgFnSc2d1	stejná
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
upřednostňovány	upřednostňován	k2eAgInPc4d1	upřednostňován
před	před	k7c7	před
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
mužů	muž	k1gMnPc2	muž
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
podpora	podpora	k1gFnSc1	podpora
nevykazuje	vykazovat	k5eNaImIp3nS	vykazovat
významné	významný	k2eAgInPc4d1	významný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
nahlášení	nahlášený	k2eAgMnPc1d1	nahlášený
lidé	člověk	k1gMnPc1	člověk
bez	bez	k7c2	bez
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
skládali	skládat	k5eAaImAgMnP	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
z	z	k7c2	z
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
z	z	k7c2	z
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
systém	systém	k1gInSc1	systém
===	===	k?	===
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
přešlo	přejít	k5eAaPmAgNnS	přejít
z	z	k7c2	z
tradičního	tradiční	k2eAgNnSc2d1	tradiční
rozložení	rozložení	k1gNnSc2	rozložení
pravomocí	pravomoc	k1gFnPc2	pravomoc
mezi	mezi	k7c7	mezi
policií	policie	k1gFnSc7	policie
a	a	k8xC	a
četnictvem	četnictvo	k1gNnSc7	četnictvo
na	na	k7c6	na
moderní	moderní	k2eAgFnSc6d1	moderní
federální	federální	k2eAgFnSc6d1	federální
policii	policie	k1gFnSc6	policie
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
disponuje	disponovat	k5eAaBmIp3nS	disponovat
protiteroristickou	protiteroristický	k2eAgFnSc7d1	protiteroristická
jednotkou	jednotka	k1gFnSc7	jednotka
EKO	EKO	kA	EKO
Kobra	kobra	k1gFnSc1	kobra
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
mimo	mimo	k7c4	mimo
stát	stát	k1gInSc4	stát
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starost	starost	k1gFnSc4	starost
armáda	armáda	k1gFnSc1	armáda
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
protiteroristickou	protiteroristický	k2eAgFnSc7d1	protiteroristická
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
Jagdkommando	Jagdkommanda	k1gFnSc5	Jagdkommanda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
EU	EU	kA	EU
===	===	k?	===
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
EU	EU	kA	EU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
ČR	ČR	kA	ČR
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
má	mít	k5eAaImIp3nS	mít
Rakousko	Rakousko	k1gNnSc1	Rakousko
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
konzuláty	konzulát	k1gInPc1	konzulát
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
Českých	český	k2eAgInPc6d1	český
Budějovicích	Budějovice	k1gInPc6	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
oficiální	oficiální	k2eAgFnSc6d1	oficiální
úrovni	úroveň	k1gFnSc6	úroveň
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
doposud	doposud	k6eAd1	doposud
nijak	nijak	k6eAd1	nijak
úzké	úzký	k2eAgFnPc1d1	úzká
<g/>
.	.	kIx.	.
</s>
<s>
Tématy	téma	k1gNnPc7	téma
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
vzbuzují	vzbuzovat	k5eAaImIp3nP	vzbuzovat
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
otázky	otázka	k1gFnPc4	otázka
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
odsunem	odsun	k1gInSc7	odsun
Sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
téma	téma	k1gNnSc4	téma
české	český	k2eAgFnSc2d1	Česká
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energetiky	energetika	k1gFnSc2	energetika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
méně	málo	k6eAd2	málo
u	u	k7c2	u
hlavních	hlavní	k2eAgMnPc2d1	hlavní
představitelů	představitel	k1gMnPc2	představitel
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
spíše	spíše	k9	spíše
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c4	v
hodnocení	hodnocení	k1gNnSc4	hodnocení
podle	podle	k7c2	podle
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
10.	[number]	k4	10.
nejbohatším	bohatý	k2eAgInSc7d3	nejbohatší
státem	stát	k1gInSc7	stát
světa	svět	k1gInSc2	svět
a	a	k8xC	a
3.	[number]	k4	3.
nejbohatším	bohatý	k2eAgInSc7d3	nejbohatší
státem	stát	k1gInSc7	stát
v	v	k7c6	v
EU	EU	kA	EU
–	–	k?	–
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ukazateli	ukazatel	k1gInSc6	ukazatel
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnoty	hodnota	k1gFnSc2	hodnota
39 500	[number]	k4	39 500
USD	USD	kA	USD
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
MMF	MMF	kA	MMF
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
3 420 788	[number]	k4	3 420 788
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
396 288	[number]	k4	396 288
pracovištích	pracoviště	k1gNnPc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
burza	burza	k1gFnSc1	burza
Rakouska	Rakousko	k1gNnSc2	Rakousko
je	být	k5eAaImIp3nS	být
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
burza	burza	k1gFnSc1	burza
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
největší	veliký	k2eAgInSc4d3	veliký
index	index	k1gInSc4	index
je	být	k5eAaImIp3nS	být
ATX	ATX	kA	ATX
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Energetika	energetika	k1gFnSc1	energetika
===	===	k?	===
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
60	[number]	k4	60
%	%	kIx~	%
výroby	výroba	k1gFnSc2	výroba
energie	energie	k1gFnSc2	energie
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
vodní	vodní	k2eAgFnPc4d1	vodní
elektrárny	elektrárna	k1gFnPc4	elektrárna
<g/>
,	,	kIx,	,
32	[number]	k4	32
%	%	kIx~	%
tepelné	tepelný	k2eAgFnSc2d1	tepelná
elektrárny	elektrárna	k1gFnSc2	elektrárna
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
jiné	jiný	k2eAgInPc4d1	jiný
obnovitelné	obnovitelný	k2eAgInPc4d1	obnovitelný
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
elektřiny	elektřina	k1gFnSc2	elektřina
musí	muset	k5eAaImIp3nS	muset
Rakousko	Rakousko	k1gNnSc1	Rakousko
dovážet	dovážet	k5eAaImF	dovážet
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Automobilová	automobilový	k2eAgFnSc1d1	automobilová
síť	síť	k1gFnSc1	síť
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
východo-západních	východoápadní	k2eAgFnPc6d1	východo-západní
liniích	linie	k1gFnPc6	linie
podél	podél	k7c2	podél
Dunaje	Dunaj	k1gInSc2	Dunaj
a	a	k8xC	a
v	v	k7c6	v
severojižní	severojižní	k2eAgFnSc6d1	severojižní
linii	linie	k1gFnSc6	linie
podél	podél	k7c2	podél
pohoří	pohoří	k1gNnSc2	pohoří
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
dálnic	dálnice	k1gFnPc2	dálnice
a	a	k8xC	a
silnic	silnice	k1gFnPc2	silnice
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
povolenou	povolený	k2eAgFnSc7d1	povolená
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
provozují	provozovat	k5eAaImIp3nP	provozovat
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
státní	státní	k2eAgFnSc2d1	státní
železnice	železnice	k1gFnSc2	železnice
(	(	kIx(	(
<g/>
Österreichische	Österreichische	k1gInSc1	Österreichische
Bundesbahnen	Bundesbahnen	k1gInSc1	Bundesbahnen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
částí	část	k1gFnPc2	část
disponují	disponovat	k5eAaBmIp3nP	disponovat
soukromé	soukromý	k2eAgFnPc1d1	soukromá
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejfrekventovanější	frekventovaný	k2eAgFnSc1d3	nejfrekventovanější
trať	trať	k1gFnSc1	trať
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
Linec	Linec	k1gInSc4	Linec
<g/>
,	,	kIx,	,
Salcburk	Salcburk	k1gInSc1	Salcburk
a	a	k8xC	a
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
/	/	kIx~	/
<g/>
Innsbruck	Innsbruck	k1gInSc1	Innsbruck
a	a	k8xC	a
od	od	k7c2	od
konce	konec	k1gInSc2	konec
20.	[number]	k4	20.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
upravována	upravovat	k5eAaImNgFnS	upravovat
pro	pro	k7c4	pro
vysokorychlostní	vysokorychlostní	k2eAgInSc4d1	vysokorychlostní
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
síť	síť	k1gFnSc1	síť
S-Bahn	S-Bahna	k1gFnPc2	S-Bahna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
<g/>
:	:	kIx,	:
Letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
Austrian	Austriana	k1gFnPc2	Austriana
Airlines	Airlinesa	k1gFnPc2	Airlinesa
<g/>
,	,	kIx,	,
Österreichischer	Österreichischra	k1gFnPc2	Österreichischra
Luftverkehr	Luftverkehra	k1gFnPc2	Luftverkehra
létají	létat	k5eAaImIp3nP	létat
vzdušnými	vzdušný	k2eAgFnPc7d1	vzdušná
cestami	cesta	k1gFnPc7	cesta
do	do	k7c2	do
56	[number]	k4	56
států	stát	k1gInPc2	stát
a	a	k8xC	a
36	[number]	k4	36
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
,	,	kIx,	,
části	část	k1gFnSc6	část
jeho	jeho	k3xOp3gInPc2	jeho
vedlejších	vedlejší	k2eAgInPc2d1	vedlejší
přítoků	přítok	k1gInPc2	přítok
a	a	k8xC	a
jezerech	jezero	k1gNnPc6	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vývoj	vývoj	k1gInSc1	vývoj
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
dnešním	dnešní	k2eAgNnPc3d1	dnešní
kritériím	kritérion	k1gNnPc3	kritérion
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc6	Rakousku-Uhersek
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1869	[number]	k4	1869
a	a	k8xC	a
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
sčítání	sčítání	k1gNnSc2	sčítání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rakouska	Rakousko	k1gNnSc2	Rakousko
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
každoročně	každoročně	k6eAd1	každoročně
stoupal	stoupat	k5eAaImAgInS	stoupat
až	až	k9	až
do	do	k7c2	do
posledního	poslední	k2eAgNnSc2d1	poslední
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
1.	[number]	k4	1.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
rozpadu	rozpad	k1gInSc2	rozpad
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2	Rakouska-Uhersko
byl	být	k5eAaImAgInS	být
silný	silný	k2eAgInSc1d1	silný
nárůst	nárůst	k1gInSc1	nárůst
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rakouska	Rakousko	k1gNnSc2	Rakousko
způsoben	způsobit	k5eAaPmNgInS	způsobit
významnou	významný	k2eAgFnSc7d1	významná
měrou	míra	k1gFnSc7wR	míra
přísunem	přísun	k1gInSc7	přísun
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
korunních	korunní	k2eAgFnPc2d1	korunní
zemí	zem	k1gFnPc2	zem
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
přesouvali	přesouvat	k5eAaImAgMnP	přesouvat
do	do	k7c2	do
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
především	především	k9	především
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
prvním	první	k4xOgNnSc6	první
sčítání	sčítání	k1gNnSc6	sčítání
po	po	k7c6	po
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
snížil	snížit	k5eAaPmAgInS	snížit
o	o	k7c4	o
347 000	[number]	k4	347 000
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
35 000	[number]	k4	35 000
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
zvedal	zvedat	k5eAaImAgMnS	zvedat
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
kontinuálně	kontinuálně	k6eAd1	kontinuálně
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
přírůstku	přírůstek	k1gInSc2	přírůstek
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1934	[number]	k4	1934
a	a	k8xC	a
1935	[number]	k4	1935
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
1000	[number]	k4	1000
osob	osoba	k1gFnPc2	osoba
následoval	následovat	k5eAaImAgInS	následovat
opětovný	opětovný	k2eAgInSc1d1	opětovný
pokles	pokles	k1gInSc1	pokles
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
1.	[number]	k4	1.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
3000	[number]	k4	3000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
dále	daleko	k6eAd2	daleko
klesal	klesat	k5eAaImAgInS	klesat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
poslední	poslední	k2eAgNnSc1d1	poslední
sčítání	sčítání	k1gNnSc1	sčítání
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
2.	[number]	k4	2.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
pokles	pokles	k1gInSc1	pokles
o	o	k7c4	o
108 000	[number]	k4	108 000
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
k	k	k7c3	k
dosavadnímu	dosavadní	k2eAgInSc3d1	dosavadní
maximu	maxim	k1gInSc3	maxim
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
zjišťován	zjišťovat	k5eAaImNgInS	zjišťovat
počet	počet	k1gInSc1	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
potravinových	potravinový	k2eAgInPc2d1	potravinový
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
spočteno	spočíst	k5eAaPmNgNnS	spočíst
cca	cca	kA	cca
7	[number]	k4	7
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
nové	nový	k2eAgNnSc1d1	nové
maximum	maximum	k1gNnSc1	maximum
–	–	k?	–
i	i	k8xC	i
přes	přes	k7c4	přes
velké	velký	k2eAgFnPc4d1	velká
válečné	válečný	k2eAgFnPc4d1	válečná
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
opět	opět	k6eAd1	opět
spadly	spadnout	k5eAaPmAgInP	spadnout
počty	počet	k1gInPc1	počet
osob	osoba	k1gFnPc2	osoba
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
672 000	[number]	k4	672 000
na	na	k7c4	na
6 928 000	[number]	k4	6 928 000
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
pak	pak	k6eAd1	pak
stoupaly	stoupat	k5eAaImAgFnP	stoupat
díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
porodnosti	porodnost	k1gFnSc3	porodnost
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
maximum	maximum	k1gNnSc4	maximum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
7 599 000	[number]	k4	7 599 000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgNnPc2d1	následující
6	[number]	k4	6
let	léto	k1gNnPc2	léto
pak	pak	k6eAd1	pak
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
nepatrně	nepatrně	k6eAd1	nepatrně
oslabovalo	oslabovat	k5eAaImAgNnS	oslabovat
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
na	na	k7c4	na
7 549 000	[number]	k4	7 549 000
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
střídaly	střídat	k5eAaImAgInP	střídat
drobné	drobný	k2eAgInPc1d1	drobný
přírůstky	přírůstek	k1gInPc1	přírůstek
a	a	k8xC	a
úbytky	úbytek	k1gInPc1	úbytek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
začal	začít	k5eAaPmAgInS	začít
počet	počet	k1gInSc1	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
opět	opět	k6eAd1	opět
znatelně	znatelně	k6eAd1	znatelně
sílit	sílit	k5eAaImF	sílit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90.	[number]	k4	90.
let	léto	k1gNnPc2	léto
má	mít	k5eAaImIp3nS	mít
znatelný	znatelný	k2eAgInSc1d1	znatelný
vliv	vliv	k1gInSc1	vliv
imigrace	imigrace	k1gFnSc2	imigrace
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
8 175 000	[number]	k4	8 175 000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1754	[number]	k4	1754
a	a	k8xC	a
1857	[number]	k4	1857
bylo	být	k5eAaImAgNnS	být
počítáno	počítat	k5eAaImNgNnS	počítat
přítomné	přítomný	k2eAgNnSc1d1	přítomné
civilní	civilní	k2eAgNnSc1d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
<g/>
,	,	kIx,	,
od	od	k7c2	od
1869	[number]	k4	1869
do	do	k7c2	do
1981	[number]	k4	1981
se	se	k3xPyFc4	se
zakládaly	zakládat	k5eAaImAgInP	zakládat
počty	počet	k1gInPc1	počet
na	na	k7c6	na
výsledcích	výsledek	k1gInPc6	výsledek
počtů	počet	k1gInPc2	počet
s	s	k7c7	s
desetiletými	desetiletý	k2eAgFnPc7d1	desetiletá
mezerami	mezera	k1gFnPc7	mezera
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byly	být	k5eAaImAgFnP	být
mezi	mezi	k7c7	mezi
počítáními	počítání	k1gNnPc7	počítání
zjišťovány	zjišťován	k2eAgInPc1d1	zjišťován
změny	změna	k1gFnPc4	změna
a	a	k8xC	a
od	od	k7c2	od
1869	[number]	k4	1869
do	do	k7c2	do
1923	[number]	k4	1923
počítáno	počítat	k5eAaImNgNnS	počítat
přítomné	přítomný	k2eAgNnSc1d1	přítomné
civilní	civilní	k2eAgNnSc1d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
a	a	k8xC	a
od	od	k7c2	od
1934	[number]	k4	1934
do	do	k7c2	do
1981	[number]	k4	1981
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
s	s	k7c7	s
trvalým	trvalý	k2eAgInSc7d1	trvalý
pobytem	pobyt	k1gInSc7	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1982	[number]	k4	1982
a	a	k8xC	a
2001	[number]	k4	2001
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
konalo	konat	k5eAaImAgNnS	konat
sčítání	sčítání	k1gNnSc4	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
zpětně	zpětně	k6eAd1	zpětně
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
z	z	k7c2	z
průměrných	průměrný	k2eAgFnPc2d1	průměrná
změn	změna	k1gFnPc2	změna
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
spočívá	spočívat	k5eAaImIp3nS	spočívat
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
na	na	k7c6	na
centrálním	centrální	k2eAgInSc6d1	centrální
rejstříku	rejstřík	k1gInSc6	rejstřík
přihlašovaných	přihlašovaný	k2eAgFnPc2d1	přihlašovaná
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
Zentrales	Zentrales	k1gMnSc1	Zentrales
Melderegister	Melderegister	k1gMnSc1	Melderegister
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
počítání	počítání	k1gNnSc1	počítání
lidu	lid	k1gInSc2	lid
čistě	čistě	k6eAd1	čistě
k	k	k7c3	k
účelu	účel	k1gInSc3	účel
zaznamenání	zaznamenání	k1gNnSc2	zaznamenání
změny	změna	k1gFnSc2	změna
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tak	tak	k9	tak
už	už	k6eAd1	už
vlastně	vlastně	k9	vlastně
nebude	být	k5eNaImBp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
===	===	k?	===
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
Rakušanů	Rakušan	k1gMnPc2	Rakušan
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
82,1	[number]	k4	82,1
let	léto	k1gNnPc2	léto
a	a	k8xC	a
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
76,4	[number]	k4	76,4
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
1971	[number]	k4	1971
<g/>
:	:	kIx,	:
75,7	[number]	k4	75,7
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
73,3	[number]	k4	73,3
muži	muž	k1gMnPc7	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
dětská	dětský	k2eAgFnSc1d1	dětská
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
0,45	[number]	k4	0,45
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
<s>
Němčina	němčina	k1gFnSc1	němčina
je	být	k5eAaImIp3nS	být
úřední	úřední	k2eAgInSc4d1	úřední
a	a	k8xC	a
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
přibližně	přibližně	k6eAd1	přibližně
98	[number]	k4	98
%	%	kIx~	%
rakouských	rakouský	k2eAgMnPc2d1	rakouský
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
varianta	varianta	k1gFnSc1	varianta
standardní	standardní	k2eAgFnSc2d1	standardní
němčiny	němčina	k1gFnSc2	němčina
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
určité	určitý	k2eAgFnPc4d1	určitá
odchylky	odchylka	k1gFnPc4	odchylka
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
německy	německy	k6eAd1	německy
hovořících	hovořící	k2eAgFnPc2d1	hovořící
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
zachyceny	zachycen	k2eAgInPc1d1	zachycen
v	v	k7c6	v
Rakouském	rakouský	k2eAgInSc6d1	rakouský
slovníku	slovník	k1gInSc6	slovník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
brán	brát	k5eAaImNgInS	brát
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
pravidel	pravidlo	k1gNnPc2	pravidlo
nad	nad	k7c7	nad
Dudenovým	Dudenův	k2eAgInSc7d1	Dudenův
slovníkem	slovník	k1gInSc7	slovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
dialekty	dialekt	k1gInPc4	dialekt
hornoněmeckých	hornoněmecký	k2eAgInPc2d1	hornoněmecký
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Alemanština	Alemanština	k1gFnSc1	Alemanština
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Alamannisch	Alamannisch	k1gInSc1	Alamannisch
<g/>
)	)	kIx)	)
–	–	k?	–
především	především	k9	především
Vorarlbersko	Vorarlbersko	k1gNnSc1	Vorarlbersko
a	a	k8xC	a
Tyrolský	tyrolský	k2eAgInSc1d1	tyrolský
okres	okres	k1gInSc1	okres
Reutte	Reutt	k1gInSc5	Reutt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bavorština	bavorština	k1gFnSc1	bavorština
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Bairisch	Bairisch	k1gInSc1	Bairisch
<g/>
)	)	kIx)	)
–	–	k?	–
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
ostatních	ostatní	k2eAgFnPc6d1	ostatní
spolkových	spolkový	k2eAgFnPc6d1	spolková
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
sedm	sedm	k4xCc1	sedm
milionů	milion	k4xCgInPc2	milion
Rakušanů	Rakušan	k1gMnPc2	Rakušan
mluví	mluvit	k5eAaImIp3nP	mluvit
středním	střední	k2eAgInSc7d1	střední
nebo	nebo	k8xC	nebo
jižním	jižní	k2eAgInSc7d1	jižní
bairišským	bairišský	k2eAgInSc7d1	bairišský
dialektem	dialekt	k1gInSc7	dialekt
<g/>
.	.	kIx.	.
<g/>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
mluví	mluvit	k5eAaImIp3nS	mluvit
také	také	k9	také
některé	některý	k3yIgFnPc4	některý
původní	původní	k2eAgFnPc4d1	původní
menšiny	menšina	k1gFnPc4	menšina
slovanskými	slovanský	k2eAgInPc7d1	slovanský
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
burgenlandská	burgenlandský	k2eAgFnSc1d1	burgenlandská
chorvatština	chorvatština	k1gFnSc1	chorvatština
<g/>
,	,	kIx,	,
slovinština	slovinština	k1gFnSc1	slovinština
a	a	k8xC	a
maďarština	maďarština	k1gFnSc1	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hovořící	hovořící	k2eAgFnSc2d1	hovořící
těmito	tento	k3xDgInPc7	tento
jazyky	jazyk	k1gInPc7	jazyk
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
a	a	k8xC	a
úřední	úřední	k2eAgInSc4d1	úřední
styk	styk	k1gInSc4	styk
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
mateřském	mateřský	k2eAgInSc6d1	mateřský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Burgenlandská	Burgenlandský	k2eAgFnSc1d1	Burgenlandský
chorvatština	chorvatština	k1gFnSc1	chorvatština
a	a	k8xC	a
slovinština	slovinština	k1gFnSc1	slovinština
jsou	být	k5eAaImIp3nP	být
dodatečné	dodatečný	k2eAgInPc1d1	dodatečný
úřední	úřední	k2eAgInPc1d1	úřední
jazyky	jazyk	k1gInPc1	jazyk
v	v	k7c6	v
těch	ten	k3xDgInPc6	ten
správních	správní	k2eAgInPc6d1	správní
a	a	k8xC	a
soudních	soudní	k2eAgInPc6d1	soudní
okresech	okres	k1gInPc6	okres
Štýrska	Štýrsko	k1gNnSc2	Štýrsko
<g/>
,	,	kIx,	,
Burgenlandska	Burgenlandsko	k1gNnSc2	Burgenlandsko
a	a	k8xC	a
Korutan	Korutany	k1gInPc2	Korutany
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
chorvatské	chorvatský	k2eAgFnPc4d1	chorvatská
nebo	nebo	k8xC	nebo
slovinské	slovinský	k2eAgFnPc4d1	slovinská
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
smíšené	smíšený	k2eAgNnSc4d1	smíšené
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obcích	obec	k1gFnPc6	obec
Oberpullendorf	Oberpullendorf	k1gInSc1	Oberpullendorf
<g/>
,	,	kIx,	,
Oberwart	Oberwart	k1gInSc1	Oberwart
<g/>
,	,	kIx,	,
Rotenturm	Rotenturm	k1gInSc1	Rotenturm
a	a	k8xC	a
Unterwart	Unterwart	k1gInSc1	Unterwart
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
němčiny	němčina	k1gFnSc2	němčina
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
také	také	k6eAd1	také
maďarština	maďarština	k1gFnSc1	maďarština
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
přibližně	přibližně	k6eAd1	přibližně
35 000	[number]	k4	35 000
Jenišů	Jeniš	k1gMnPc2	Jeniš
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yQgFnPc2	který
přibližně	přibližně	k6eAd1	přibližně
3500	[number]	k4	3500
vede	vést	k5eAaImIp3nS	vést
stále	stále	k6eAd1	stále
kočovný	kočovný	k2eAgInSc4d1	kočovný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60.	[number]	k4	60.
letech	let	k1gInPc6	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
v	v	k7c6	v
Burgenlandsku	Burgenlandsko	k1gNnSc6	Burgenlandsko
<g/>
,	,	kIx,	,
Mühlviertel	Mühlviertel	k1gMnSc1	Mühlviertel
<g/>
,	,	kIx,	,
Waldviertel	Waldviertel	k1gMnSc1	Waldviertel
<g/>
,	,	kIx,	,
na	na	k7c4	na
Hausrucku	Hausrucka	k1gFnSc4	Hausrucka
<g/>
,	,	kIx,	,
v	v	k7c4	v
Totes	Totes	k1gInSc4	Totes
Gebirge	Gebirg	k1gFnSc2	Gebirg
<g/>
,	,	kIx,	,
v	v	k7c6	v
Seetálských	Seetálský	k2eAgFnPc6d1	Seetálský
a	a	k8xC	a
Fischbachských	Fischbachský	k2eAgFnPc6d1	Fischbachský
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Imigrace	imigrace	k1gFnSc2	imigrace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
Rakousko	Rakousko	k1gNnSc1	Rakousko
vydalo	vydat	k5eAaPmAgNnS	vydat
novou	nový	k2eAgFnSc7d1	nová
cestou	cesta	k1gFnSc7	cesta
v	v	k7c6	v
imigračním	imigrační	k2eAgNnSc6d1	imigrační
zákonodárství	zákonodárství	k1gNnSc6	zákonodárství
vydáním	vydání	k1gNnSc7	vydání
pobytového	pobytový	k2eAgInSc2d1	pobytový
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
např.	např.	kA	např.
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Německa	Německo	k1gNnSc2	Německo
neřídí	řídit	k5eNaImIp3nS	řídit
etnickými	etnický	k2eAgNnPc7d1	etnické
a	a	k8xC	a
nacionálními	nacionální	k2eAgNnPc7d1	nacionální
hledisky	hledisko	k1gNnPc7	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
určuje	určovat	k5eAaImIp3nS	určovat
horní	horní	k2eAgFnSc4d1	horní
hranici	hranice	k1gFnSc4	hranice
celkové	celkový	k2eAgFnSc2d1	celková
hrubé	hrubý	k2eAgFnSc2d1	hrubá
migrace	migrace	k1gFnSc2	migrace
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
stanovení	stanovení	k1gNnSc6	stanovení
bere	brát	k5eAaImIp3nS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
demografický	demografický	k2eAgInSc4d1	demografický
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
vývoj	vývoj	k1gInSc4	vývoj
přijímající	přijímající	k2eAgFnSc2d1	přijímající
země	zem	k1gFnSc2	zem
i	i	k8xC	i
počet	počet	k1gInSc1	počet
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
uznaným	uznaný	k2eAgInSc7d1	uznaný
statusem	status	k1gInSc7	status
uprchlíka	uprchlík	k1gMnSc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
pětiletém	pětiletý	k2eAgInSc6d1	pětiletý
pobytu	pobyt	k1gInSc6	pobyt
se	se	k3xPyFc4	se
přistěhovalcům	přistěhovalec	k1gMnPc3	přistěhovalec
otevírá	otevírat	k5eAaImIp3nS	otevírat
perspektiva	perspektiva	k1gFnSc1	perspektiva
trvalého	trvalý	k2eAgInSc2d1	trvalý
pobytu	pobyt	k1gInSc2	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
1 579 000	[number]	k4	1 579 000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Rakouska	Rakousko	k1gNnSc2	Rakousko
(	(	kIx(	(
<g/>
19	[number]	k4	19
%	%	kIx~	%
<g/>
)	)	kIx)	)
přistěhovaleckého	přistěhovalecký	k2eAgInSc2d1	přistěhovalecký
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
64,1	[number]	k4	64,1
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
Římsko-katolické	římskoatolický	k2eAgFnSc3d1	římsko-katolická
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
3,8	[number]	k4	3,8
%	%	kIx~	%
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
evangelických	evangelický	k2eAgFnPc2d1	evangelická
církví	církev	k1gFnPc2	církev
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
k	k	k7c3	k
augsburského	augsburský	k2eAgNnSc2d1	Augsburské
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
helvétského	helvétský	k2eAgNnSc2d1	helvétský
vyznání	vyznání	k1gNnSc2	vyznání
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
180 000	[number]	k4	180 000
křesťanů	křesťan	k1gMnPc2	křesťan
je	být	k5eAaImIp3nS	být
členy	člen	k1gInPc4	člen
ortodoxních	ortodoxní	k2eAgFnPc2d1	ortodoxní
církví	církev	k1gFnPc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
židovství	židovství	k1gNnSc3	židovství
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
8140	[number]	k4	8140
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
stav	stav	k1gInSc4	stav
k	k	k7c3	k
sčítání	sčítání	k1gNnSc3	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
15 000	[number]	k4	15 000
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
700 000	[number]	k4	700 000
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
)	)	kIx)	)
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nP	tvořit
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
zdvojnásobil	zdvojnásobit	k5eAaPmAgMnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
buddhismu	buddhismus	k1gInSc3	buddhismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
uznán	uznán	k2eAgMnSc1d1	uznán
jako	jako	k8xS	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
"	"	kIx"	"
<g/>
církev	církev	k1gFnSc1	církev
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nP	hlásit
něco	něco	k3yInSc4	něco
přes	přes	k7c4	přes
10 000	[number]	k4	10 000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
20 000	[number]	k4	20 000
lidí	člověk	k1gMnPc2	člověk
jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgMnPc1d1	aktivní
členové	člen	k1gMnPc1	člen
Svědků	svědek	k1gMnPc2	svědek
Jehovových	Jehovův	k2eAgMnPc2d1	Jehovův
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
plnohodnotného	plnohodnotný	k2eAgNnSc2d1	plnohodnotné
právního	právní	k2eAgNnSc2d1	právní
uznání	uznání	k1gNnSc2	uznání
jako	jako	k8xC	jako
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
12	[number]	k4	12
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
nehlásí	hlásit	k5eNaImIp3nS	hlásit
k	k	k7c3	k
žádnému	žádný	k3yNgNnSc3	žádný
vyznání	vyznání	k1gNnSc3	vyznání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
obydlená	obydlený	k2eAgFnSc1d1	obydlená
oblast	oblast	k1gFnSc1	oblast
Rakouska	Rakousko	k1gNnSc2	Rakousko
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
metropole	metropol	k1gFnSc2	metropol
Vídeň	Vídeň	k1gFnSc1	Vídeň
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
2 067 652	[number]	k4	2 067 652
(	(	kIx(	(
<g/>
stav	stav	k1gInSc4	stav
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
v	v	k7c6	v
sobě	se	k3xPyFc3	se
celou	celý	k2eAgFnSc4d1	celá
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
200	[number]	k4	200
rakouských	rakouský	k2eAgFnPc2d1	rakouská
obcí	obec	k1gFnPc2	obec
jsou	být	k5eAaImIp3nP	být
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
problém	problém	k1gInSc1	problém
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
hospodářsky	hospodářsky	k6eAd1	hospodářsky
slabších	slabý	k2eAgFnPc6d2	slabší
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
odliv	odliv	k1gInSc1	odliv
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
do	do	k7c2	do
městských	městský	k2eAgFnPc2d1	městská
aglomerací	aglomerace	k1gFnPc2	aglomerace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Svátky	svátek	k1gInPc4	svátek
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
svátky	svátek	k1gInPc1	svátek
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
upravuje	upravovat	k5eAaImIp3nS	upravovat
§	§	k?	§
7	[number]	k4	7
Spolkového	spolkový	k2eAgInSc2d1	spolkový
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
dnech	den	k1gInPc6	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
vydán	vydán	k2eAgInSc1d1	vydán
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
13	[number]	k4	13
státních	státní	k2eAgInPc2d1	státní
svátků	svátek	k1gInPc2	svátek
(	(	kIx(	(
<g/>
dnů	den	k1gInPc2	den
pracovního	pracovní	k2eAgNnSc2d1	pracovní
volna	volno	k1gNnSc2	volno
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kuchyně	kuchyně	k1gFnSc2	kuchyně
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
kuchyně	kuchyně	k1gFnSc1	kuchyně
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
redukována	redukovat	k5eAaBmNgFnS	redukovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
vídeňskou	vídeňský	k2eAgFnSc4d1	Vídeňská
kuchyni	kuchyně	k1gFnSc4	kuchyně
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
tradiční	tradiční	k2eAgNnSc1d1	tradiční
kuchařské	kuchařský	k2eAgNnSc1d1	kuchařské
umění	umění	k1gNnSc1	umění
bývalého	bývalý	k2eAgInSc2d1	bývalý
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
samotných	samotný	k2eAgFnPc2d1	samotná
krajových	krajový	k2eAgFnPc2d1	krajová
specialit	specialita	k1gFnPc2	specialita
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
kuchyně	kuchyně	k1gFnSc1	kuchyně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
především	především	k9	především
svými	svůj	k3xOyFgMnPc7	svůj
sousedy	soused	k1gMnPc7	soused
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
Maďary	Maďar	k1gMnPc4	Maďar
<g/>
.	.	kIx.	.
</s>
<s>
Pokrmy	pokrm	k1gInPc1	pokrm
a	a	k8xC	a
způsoby	způsob	k1gInPc1	způsob
jejich	jejich	k3xOp3gFnSc2	jejich
přípravy	příprava	k1gFnSc2	příprava
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
často	často	k6eAd1	často
přebírány	přebírán	k2eAgFnPc1d1	přebírána
a	a	k8xC	a
začleňovány	začleňován	k2eAgFnPc1d1	začleňována
do	do	k7c2	do
vlastní	vlastní	k2eAgFnSc2d1	vlastní
rakouské	rakouský	k2eAgFnSc2d1	rakouská
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příklad	příklad	k1gInSc1	příklad
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
třeba	třeba	k6eAd1	třeba
guláš	guláš	k1gInSc4	guláš
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
architektonickém	architektonický	k2eAgInSc6d1	architektonický
slohu	sloh	k1gInSc6	sloh
vznikala	vznikat	k5eAaImAgNnP	vznikat
významná	významný	k2eAgNnPc1d1	významné
stavební	stavební	k2eAgNnPc1d1	stavební
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
na	na	k7c6	na
dnešním	dnešní	k2eAgInSc6d1	dnešní
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	Unesco	k1gNnSc1	Unesco
(	(	kIx(	(
<g/>
historická	historický	k2eAgNnPc1d1	historické
centra	centrum	k1gNnPc1	centrum
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
Salcburku	Salcburk	k1gInSc2	Salcburk
a	a	k8xC	a
Štýrského	štýrský	k2eAgInSc2d1	štýrský
Hradce	Hradec	k1gInSc2	Hradec
<g/>
,	,	kIx,	,
palác	palác	k1gInSc1	palác
Schönbrunn	Schönbrunn	k1gInSc1	Schönbrunn
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Eggenberg	Eggenberg	k1gInSc1	Eggenberg
<g/>
,	,	kIx,	,
horská	horský	k2eAgFnSc1d1	horská
dráha	dráha	k1gFnSc1	dráha
Semmering	Semmering	k1gInSc1	Semmering
<g/>
,	,	kIx,	,
prehistorická	prehistorický	k2eAgNnPc1d1	prehistorické
kůlová	kůlový	k2eAgNnPc1d1	kůlový
obydlí	obydlí	k1gNnPc1	obydlí
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18.	[number]	k4	18.
a	a	k8xC	a
19.	[number]	k4	19.
století	století	k1gNnPc2	století
byla	být	k5eAaImAgFnS	být
Vídeň	Vídeň	k1gFnSc1	Vídeň
směr	směr	k1gInSc4	směr
udávající	udávající	k2eAgNnPc4d1	udávající
centrum	centrum	k1gNnSc4	centrum
evropského	evropský	k2eAgInSc2d1	evropský
kulturního	kulturní	k2eAgInSc2d1	kulturní
a	a	k8xC	a
především	především	k9	především
hudebního	hudební	k2eAgInSc2d1	hudební
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nebylo	být	k5eNaImAgNnS	být
pouze	pouze	k6eAd1	pouze
díky	díky	k7c3	díky
mnoha	mnoho	k4c3	mnoho
významným	významný	k2eAgMnPc3d1	významný
hudebníkům	hudebník	k1gMnPc3	hudebník
a	a	k8xC	a
hudebním	hudební	k2eAgMnPc3d1	hudební
skladatelům	skladatel	k1gMnPc3	skladatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
zemí	zem	k1gFnSc7	zem
spjati	spjat	k2eAgMnPc1d1	spjat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
díky	díky	k7c3	díky
mnoha	mnoho	k4c3	mnoho
operním	operní	k2eAgNnPc3d1	operní
i	i	k8xC	i
běžným	běžný	k2eAgNnPc3d1	běžné
divadlům	divadlo	k1gNnPc3	divadlo
a	a	k8xC	a
orchestrům	orchestr	k1gInPc3	orchestr
<g/>
,	,	kIx,	,
mnohostranně	mnohostranně	k6eAd1	mnohostranně
zaměřeným	zaměřený	k2eAgFnPc3d1	zaměřená
hudebním	hudební	k2eAgFnPc3d1	hudební
tradicím	tradice	k1gFnPc3	tradice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Novoroční	novoroční	k2eAgInSc1d1	novoroční
koncert	koncert	k1gInSc1	koncert
Vídeňských	vídeňský	k2eAgMnPc2d1	vídeňský
filharmoniků	filharmonik	k1gMnPc2	filharmonik
a	a	k8xC	a
mnoha	mnoho	k4c3	mnoho
festivalům	festival	k1gInPc3	festival
(	(	kIx(	(
<g/>
Salcburský	salcburský	k2eAgInSc1d1	salcburský
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
Bregenzské	Bregenzský	k2eAgFnPc1d1	Bregenzský
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
,	,	kIx,	,
Slavnosti	slavnost	k1gFnPc1	slavnost
v	v	k7c6	v
Reichenau	Reichenaus	k1gInSc6	Reichenaus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
má	mít	k5eAaImIp3nS	mít
Rakousko	Rakousko	k1gNnSc4	Rakousko
vybudovánu	vybudován	k2eAgFnSc4d1	vybudována
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
divadelní	divadelní	k2eAgFnSc4d1	divadelní
tradici	tradice	k1gFnSc4	tradice
a	a	k8xC	a
živou	živý	k2eAgFnSc4d1	živá
kabaretní	kabaretní	k2eAgFnSc4d1	kabaretní
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kulinářského	kulinářský	k2eAgNnSc2d1	kulinářské
umění	umění	k1gNnSc2	umění
jsou	být	k5eAaImIp3nP	být
Rakušané	Rakušan	k1gMnPc1	Rakušan
známí	známit	k5eAaImIp3nP	známit
svou	svůj	k3xOyFgFnSc7	svůj
kavárenskou	kavárenský	k2eAgFnSc7d1	kavárenská
kulturou	kultura	k1gFnSc7	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Vinárny	vinárna	k1gFnPc1	vinárna
a	a	k8xC	a
rozličné	rozličný	k2eAgInPc1d1	rozličný
krajové	krajový	k2eAgInPc1d1	krajový
pokrmy	pokrm	k1gInPc1	pokrm
nezapřou	zapřít	k5eNaPmIp3nP	zapřít
svou	svůj	k3xOyFgFnSc4	svůj
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
Štýrský	štýrský	k2eAgInSc1d1	štýrský
Hradec	Hradec	k1gInSc1	Hradec
evropským	evropský	k2eAgNnSc7d1	Evropské
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
rakouské	rakouský	k2eAgFnSc2d1	rakouská
kultury	kultura	k1gFnSc2	kultura
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
slouží	sloužit	k5eAaImIp3nS	sloužit
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
kulturní	kulturní	k2eAgNnSc4d1	kulturní
fórum	fórum	k1gNnSc4	fórum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Až	až	k9	až
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
si	se	k3xPyFc3	se
v	v	k7c6	v
kulturním	kulturní	k2eAgNnSc6d1	kulturní
dění	dění	k1gNnSc6	dění
Rakouska	Rakousko	k1gNnSc2	Rakousko
udržela	udržet	k5eAaPmAgFnS	udržet
výsadní	výsadní	k2eAgNnSc4d1	výsadní
místo	místo	k1gNnSc4	místo
klasická	klasický	k2eAgFnSc1d1	klasická
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
ohlížet	ohlížet	k5eAaImF	ohlížet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
na	na	k7c4	na
hudební	hudební	k2eAgMnPc4d1	hudební
velikány	velikán	k1gMnPc4	velikán
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
skladatelům	skladatel	k1gMnPc3	skladatel
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
řadí	řadit	k5eAaImIp3nS	řadit
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Haydn	Haydn	k1gMnSc1	Haydn
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Schubert	Schubert	k1gMnSc1	Schubert
<g/>
,	,	kIx,	,
Anton	Anton	k1gMnSc1	Anton
Bruckner	Bruckner	k1gMnSc1	Bruckner
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Haydn	Haydn	k1gMnSc1	Haydn
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Ditters	Dittersa	k1gFnPc2	Dittersa
von	von	k1gInSc4	von
Dittersdorf	Dittersdorf	k1gMnSc1	Dittersdorf
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Steiner	Steiner	k1gMnSc1	Steiner
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
von	von	k1gInSc4	von
Zemlinsky	Zemlinsky	k1gFnSc4	Zemlinsky
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Xaver	Xaver	k1gMnSc1	Xaver
Süssmayr	Süssmayr	k1gMnSc1	Süssmayr
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Joseph	Joseph	k1gMnSc1	Joseph
Fux	Fux	k1gMnSc1	Fux
či	či	k8xC	či
Johann	Johann	k1gMnSc1	Johann
Georg	Georg	k1gMnSc1	Georg
Albrechtsberger	Albrechtsberger	k1gMnSc1	Albrechtsberger
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
Xaver	Xaver	k1gMnSc1	Xaver
Gruber	Gruber	k1gMnSc1	Gruber
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
písní	píseň	k1gFnSc7	píseň
Tichá	Tichá	k1gFnSc1	Tichá
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
svatá	svatý	k2eAgFnSc1d1	svatá
noc	noc	k1gFnSc1	noc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zlidověla	zlidovět	k5eAaPmAgFnS	zlidovět
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
vánoční	vánoční	k2eAgFnSc7d1	vánoční
koledou	koleda	k1gFnSc7	koleda
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Strauss	Straussa	k1gFnPc2	Straussa
starší	starší	k1gMnSc1	starší
zpopularizoval	zpopularizovat	k5eAaPmAgMnS	zpopularizovat
valčík	valčík	k1gInSc4	valčík
<g/>
,	,	kIx,	,
Johann	Johann	k1gInSc1	Johann
Strauss	Straussa	k1gFnPc2	Straussa
mladší	mladý	k2eAgMnSc1d2	mladší
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
"	"	kIx"	"
<g/>
králem	král	k1gMnSc7	král
valčíku	valčík	k1gInSc2	valčík
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
klasické	klasický	k2eAgFnSc2d1	klasická
a	a	k8xC	a
modernistické	modernistický	k2eAgFnSc2d1	modernistická
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
stál	stát	k5eAaImAgMnS	stát
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc4d1	čistý
modernismus	modernismus	k1gInSc4	modernismus
pak	pak	k6eAd1	pak
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
Arnold	Arnold	k1gMnSc1	Arnold
Schönberg	Schönberg	k1gMnSc1	Schönberg
<g/>
,	,	kIx,	,
Alban	Alban	k1gMnSc1	Alban
Berg	Berg	k1gMnSc1	Berg
a	a	k8xC	a
Anton	Anton	k1gMnSc1	Anton
von	von	k1gInSc4	von
Webern	Weberna	k1gFnPc2	Weberna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tuto	tento	k3xDgFnSc4	tento
tradici	tradice	k1gFnSc4	tradice
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
následovalo	následovat	k5eAaImAgNnS	následovat
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
známých	známý	k2eAgMnPc2d1	známý
dirigentů	dirigent	k1gMnPc2	dirigent
jako	jako	k9	jako
Erich	Erich	k1gMnSc1	Erich
Kleiber	Kleiber	k1gMnSc1	Kleiber
<g/>
,	,	kIx,	,
Herbert	Herbert	k1gInSc1	Herbert
von	von	k1gInSc1	von
Karajan	Karajan	k1gInSc1	Karajan
<g/>
,	,	kIx,	,
Karl	Karl	k1gMnSc1	Karl
Böhm	Böhm	k1gMnSc1	Böhm
<g/>
,	,	kIx,	,
Nikolaus	Nikolaus	k1gMnSc1	Nikolaus
Harnoncourt	Harnoncourt	k1gInSc1	Harnoncourt
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
Weingartner	Weingartner	k1gMnSc1	Weingartner
<g/>
,	,	kIx,	,
Clemens	Clemens	k1gInSc1	Clemens
Krauss	Krauss	k1gInSc1	Krauss
nebo	nebo	k8xC	nebo
Franz	Franz	k1gMnSc1	Franz
Welser-Möst	Welser-Möst	k1gMnSc1	Welser-Möst
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
interpretů	interpret	k1gMnPc2	interpret
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
klavíristé	klavírista	k1gMnPc1	klavírista
Carl	Carla	k1gFnPc2	Carla
Czerny	Czerna	k1gFnSc2	Czerna
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Nepomuk	Nepomuk	k1gMnSc1	Nepomuk
Hummel	Hummel	k1gMnSc1	Hummel
<g/>
,	,	kIx,	,
Sigismund	Sigismund	k1gMnSc1	Sigismund
Thalberg	Thalberg	k1gMnSc1	Thalberg
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
a	a	k8xC	a
Artur	Artur	k1gMnSc1	Artur
Schnabel	Schnabel	k1gMnSc1	Schnabel
či	či	k8xC	či
houslista	houslista	k1gMnSc1	houslista
Fritz	Fritz	k1gMnSc1	Fritz
Kreisler	Kreisler	k1gMnSc1	Kreisler
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
představitelkou	představitelka	k1gFnSc7	představitelka
romantického	romantický	k2eAgInSc2d1	romantický
baletu	balet	k1gInSc2	balet
19.	[number]	k4	19.
století	století	k1gNnSc2	století
byla	být	k5eAaImAgFnS	být
Fanny	Fann	k1gMnPc4	Fann
Elsslerová	Elsslerový	k2eAgFnSc1d1	Elsslerový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novoroční	novoroční	k2eAgInSc1d1	novoroční
koncert	koncert	k1gInSc1	koncert
Vídeňských	vídeňský	k2eAgMnPc2d1	vídeňský
filharmoniků	filharmonik	k1gMnPc2	filharmonik
je	být	k5eAaImIp3nS	být
světově	světově	k6eAd1	světově
známý	známý	k2eAgMnSc1d1	známý
a	a	k8xC	a
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
1.	[number]	k4	1.
lednové	lednový	k2eAgInPc1d1	lednový
dopoledne	dopoledne	k1gNnSc4	dopoledne
ve	v	k7c6	v
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
koncertním	koncertní	k2eAgInSc6d1	koncertní
domě	dům	k1gInSc6	dům
Wiener	Wiener	k1gMnSc1	Wiener
Musikverein	Musikverein	k1gMnSc1	Musikverein
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
je	být	k5eAaImIp3nS	být
přenášen	přenášet	k5eAaImNgInS	přenášet
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
skoro	skoro	k6eAd1	skoro
miliardu	miliarda	k4xCgFnSc4	miliarda
lidí	člověk	k1gMnPc2	člověk
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hrané	hraný	k2eAgInPc1d1	hraný
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
známé	známý	k2eAgInPc1d1	známý
valčíky	valčík	k1gInPc1	valčík
<g/>
,	,	kIx,	,
polky	polka	k1gFnPc1	polka
a	a	k8xC	a
pochody	pochod	k1gInPc1	pochod
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
silně	silně	k6eAd1	silně
zastoupeno	zastoupen	k2eAgNnSc1d1	zastoupeno
dílo	dílo	k1gNnSc1	dílo
rodu	rod	k1gInSc2	rod
Straußů	Strauß	k1gMnPc2	Strauß
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
operetě	opereta	k1gFnSc6	opereta
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
známými	známý	k1gMnPc7	známý
jmény	jméno	k1gNnPc7	jméno
Franz	Franz	k1gMnSc1	Franz
von	von	k1gInSc4	von
Suppé	Suppý	k2eAgFnPc1d1	Suppý
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Stolz	Stolz	k1gMnSc1	Stolz
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Zeller	Zeller	k1gMnSc1	Zeller
nebo	nebo	k8xC	nebo
Oscar	Oscar	k1gMnSc1	Oscar
Straus	Straus	k1gMnSc1	Straus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazzu	jazz	k1gInSc6	jazz
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Ernst	Ernst	k1gMnSc1	Ernst
Křenek	Křenek	k1gMnSc1	Křenek
(	(	kIx(	(
<g/>
jazzová	jazzový	k2eAgFnSc1d1	jazzová
opera	opera	k1gFnSc1	opera
Jonny	Jonna	k1gFnSc2	Jonna
spielt	spielt	k1gInSc1	spielt
auf	auf	k?	auf
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Joe	Joe	k1gMnSc1	Joe
Zawinul	Zawinul	k1gMnSc1	Zawinul
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
syntetizátoru	syntetizátor	k1gInSc2	syntetizátor
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
druhu	druh	k1gInSc6	druh
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
zejména	zejména	k9	zejména
Udo	Udo	k1gMnPc1	Udo
Jürgens	Jürgens	k1gInSc4	Jürgens
<g/>
,	,	kIx,	,
Falco	Falco	k6eAd1	Falco
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Hans	Hans	k1gMnSc1	Hans
Hölzl	Hölzl	k1gMnSc1	Hölzl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
skupina	skupina	k1gFnSc1	skupina
Opus	opus	k1gInSc1	opus
<g/>
,	,	kIx,	,
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
hitem	hit	k1gInSc7	hit
Life	Life	k1gNnSc4	Life
is	is	k?	is
life	life	k1gInSc1	life
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rocku	rock	k1gInSc6	rock
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
blackmetalová	blackmetalový	k2eAgFnSc1d1	blackmetalová
skupina	skupina	k1gFnSc1	skupina
Summoning	Summoning	k1gInSc1	Summoning
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
muzikálová	muzikálový	k2eAgFnSc1d1	muzikálová
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
na	na	k7c4	na
Broadwayi	Broadwaye	k1gFnSc4	Broadwaye
Lotte	Lott	k1gInSc5	Lott
Lenya	Leny	k2eAgFnSc1d1	Leny
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
autor	autor	k1gMnSc1	autor
filmové	filmový	k2eAgFnSc2d1	filmová
hudby	hudba	k1gFnSc2	hudba
Erich	Erich	k1gMnSc1	Erich
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Korngold	Korngold	k1gMnSc1	Korngold
<g/>
.	.	kIx.	.
</s>
<s>
Známou	známý	k2eAgFnSc7d1	známá
melodií	melodie	k1gFnSc7	melodie
je	být	k5eAaImIp3nS	být
i	i	k9	i
na	na	k7c4	na
citeru	citera	k1gFnSc4	citera
hraná	hraný	k2eAgFnSc1d1	hraná
titulní	titulní	k2eAgFnSc1d1	titulní
píseň	píseň	k1gFnSc1	píseň
k	k	k7c3	k
britskému	britský	k2eAgInSc3d1	britský
filmu	film	k1gInSc3	film
Třetí	třetí	k4xOgMnSc1	třetí
muž	muž	k1gMnSc1	muž
od	od	k7c2	od
Antona	Anton	k1gMnSc2	Anton
Karase	Karas	k1gMnSc2	Karas
<g/>
.	.	kIx.	.
</s>
<s>
Představiteli	představitel	k1gMnSc3	představitel
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
jsou	být	k5eAaImIp3nP	být
Parov	Parov	k1gInSc4	Parov
Stelar	Stelar	k1gInSc1	Stelar
nebo	nebo	k8xC	nebo
DJ	DJ	kA	DJ
Ötzi	Ötz	k1gFnPc4	Ötz
<g/>
.	.	kIx.	.
</s>
<s>
Nezanedbatelný	zanedbatelný	k2eNgInSc1d1	nezanedbatelný
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
i	i	k9	i
lidová	lidový	k2eAgFnSc1d1	lidová
hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
kombinace	kombinace	k1gFnSc1	kombinace
s	s	k7c7	s
populární	populární	k2eAgFnSc7d1	populární
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Populárním	populární	k2eAgMnSc7d1	populární
zpěvákem	zpěvák	k1gMnSc7	zpěvák
těchto	tento	k3xDgInPc6	tento
tzv.	tzv.	kA	tzv.
šlágrů	šlágr	k1gInPc2	šlágr
byl	být	k5eAaImAgInS	být
Freddy	Fredda	k1gFnSc2	Fredda
Quinn	Quinn	k1gInSc1	Quinn
<g/>
,	,	kIx,	,
lyžař	lyžař	k1gMnSc1	lyžař
Hansi	Hans	k1gMnSc3	Hans
Hinterseer	Hintersera	k1gFnPc2	Hintersera
nebo	nebo	k8xC	nebo
Hubert	Hubert	k1gMnSc1	Hubert
von	von	k1gInSc4	von
Goisern	Goiserna	k1gFnPc2	Goiserna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejpopulárnějším	populární	k2eAgMnPc3d3	nejpopulárnější
zpěvákům	zpěvák	k1gMnPc3	zpěvák
pop-music	popusic	k1gFnSc2	pop-music
současnosti	současnost	k1gFnSc2	současnost
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
patří	patřit	k5eAaImIp3nS	patřit
Nadine	Nadin	k1gInSc5	Nadin
Beiler	Beiler	k1gInSc1	Beiler
nebo	nebo	k8xC	nebo
Christina	Christina	k1gFnSc1	Christina
Stürmer	Stürmra	k1gFnPc2	Stürmra
<g/>
.	.	kIx.	.
</s>
<s>
Vítězstvím	vítězství	k1gNnSc7	vítězství
na	na	k7c4	na
Eurovision	Eurovision	k1gInSc4	Eurovision
Song	song	k1gInSc1	song
Contest	Contest	k1gFnSc1	Contest
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
Conchita	Conchit	k2eAgFnSc1d1	Conchita
Wurst	Wurst	k1gFnSc1	Wurst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
filmovém	filmový	k2eAgNnSc6d1	filmové
plátně	plátno	k1gNnSc6	plátno
se	se	k3xPyFc4	se
proslavili	proslavit	k5eAaPmAgMnP	proslavit
herci	herec	k1gMnPc1	herec
Curd	Curdo	k1gNnPc2	Curdo
Jürgens	Jürgensa	k1gFnPc2	Jürgensa
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Oscara	Oscar	k1gMnSc2	Oscar
Maximilian	Maximilian	k1gMnSc1	Maximilian
Schell	Schell	k1gMnSc1	Schell
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
Norimberský	norimberský	k2eAgInSc1d1	norimberský
proces	proces	k1gInSc1	proces
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
dvou	dva	k4xCgMnPc2	dva
Oscarů	Oscar	k1gMnPc2	Oscar
Christoph	Christoph	k1gInSc4	Christoph
Waltz	waltz	k1gInSc4	waltz
(	(	kIx(	(
<g/>
za	za	k7c4	za
výkony	výkon	k1gInPc4	výkon
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Hanebný	hanebný	k2eAgMnSc1d1	hanebný
pancharti	panchart	k1gMnPc1	panchart
a	a	k8xC	a
Nespoutaný	spoutaný	k2eNgMnSc1d1	nespoutaný
Django	Django	k1gMnSc1	Django
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Zlatégo	Zlatégo	k1gMnSc1	Zlatégo
globu	globus	k1gInSc2	globus
Klaus	Klaus	k1gMnSc1	Klaus
Maria	Maria	k1gFnSc1	Maria
Brandauer	Brandauer	k1gMnSc1	Brandauer
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Romy	Rom	k1gMnPc4	Rom
Schneiderová	Schneiderová	k1gFnSc1	Schneiderová
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
v	v	k7c6	v
trilogii	trilogie	k1gFnSc6	trilogie
Sissi	Sisse	k1gFnSc4	Sisse
<g/>
,	,	kIx,	,
Senta	Senta	k1gFnSc1	Senta
Bergerová	Bergerová	k1gFnSc1	Bergerová
<g/>
,	,	kIx,	,
Oskar	Oskar	k1gMnSc1	Oskar
Werner	Werner	k1gMnSc1	Werner
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
Schellová	Schellová	k1gFnSc1	Schellová
<g/>
,	,	kIx,	,
O.	O.	kA	O.
W.	W.	kA	W.
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Martin	Martin	k1gMnSc1	Martin
Weinek	Weinek	k1gMnSc1	Weinek
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
z	z	k7c2	z
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
Komisař	komisař	k1gMnSc1	komisař
Rex	Rex	k1gMnSc1	Rex
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
mezinárodně	mezinárodně	k6eAd1	mezinárodně
nejúspěšnějších	úspěšný	k2eAgInPc2d3	nejúspěšnější
rakouských	rakouský	k2eAgInPc2d1	rakouský
televizních	televizní	k2eAgInPc2d1	televizní
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
němé	němý	k2eAgFnSc6d1	němá
éře	éra	k1gFnSc6	éra
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
Erich	Erich	k1gMnSc1	Erich
von	von	k1gInSc4	von
Stroheim	Stroheima	k1gFnPc2	Stroheima
<g/>
.	.	kIx.	.
</s>
<s>
Rakouského	rakouský	k2eAgInSc2d1	rakouský
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
také	také	k9	také
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gMnSc1	Schwarzenegger
<g/>
,	,	kIx,	,
hvězda	hvězda	k1gFnSc1	hvězda
hollywoodských	hollywoodský	k2eAgInPc2d1	hollywoodský
akčních	akční	k2eAgInPc2d1	akční
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
Hedy	Heda	k1gFnSc2	Heda
Lamarrová	Lamarrový	k2eAgFnSc1d1	Lamarrový
byla	být	k5eAaImAgFnS	být
hollywoodskou	hollywoodský	k2eAgFnSc7d1	hollywoodská
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
rakouští	rakouský	k2eAgMnPc1d1	rakouský
předváleční	předválečný	k2eAgMnPc1d1	předválečný
režiséři	režisér	k1gMnPc1	režisér
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
pilíři	pilíř	k1gInSc6	pilíř
filmového	filmový	k2eAgInSc2d1	filmový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
:	:	kIx,	:
Billy	Bill	k1gMnPc4	Bill
Wilder	Wilder	k1gInSc1	Wilder
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Lang	Lang	k1gMnSc1	Lang
<g/>
,	,	kIx,	,
Fred	Fred	k1gMnSc1	Fred
Zinnemann	Zinnemann	k1gMnSc1	Zinnemann
či	či	k8xC	či
Otto	Otto	k1gMnSc1	Otto
Preminger	Preminger	k1gMnSc1	Preminger
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
prožili	prožít	k5eAaPmAgMnP	prožít
svůj	svůj	k3xOyFgInSc4	svůj
tvůrčí	tvůrčí	k2eAgInSc4d1	tvůrčí
vrchol	vrchol	k1gInSc4	vrchol
jejich	jejich	k3xOp3gMnPc1	jejich
kolegové	kolega	k1gMnPc1	kolega
Josef	Josefa	k1gFnPc2	Josefa
von	von	k1gInSc1	von
Sternberg	Sternberg	k1gMnSc1	Sternberg
nebo	nebo	k8xC	nebo
Georg	Georg	k1gMnSc1	Georg
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Pabst	Pabst	k1gMnSc1	Pabst
<g/>
.	.	kIx.	.
</s>
<s>
Režisérskými	režisérský	k2eAgFnPc7d1	režisérská
hvězdami	hvězda	k1gFnPc7	hvězda
současné	současný	k2eAgFnSc2d1	současná
rakouské	rakouský	k2eAgFnSc2d1	rakouská
kinematografie	kinematografie	k1gFnSc2	kinematografie
jsou	být	k5eAaImIp3nP	být
Michael	Michael	k1gMnSc1	Michael
Haneke	Hanek	k1gFnSc2	Hanek
<g/>
,	,	kIx,	,
Ulrich	Ulrich	k1gMnSc1	Ulrich
Seidl	Seidl	k1gMnSc1	Seidl
nebo	nebo	k8xC	nebo
Stefan	Stefan	k1gMnSc1	Stefan
Ruzowitzky	Ruzowitzka	k1gFnSc2	Ruzowitzka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
získal	získat	k5eAaPmAgInS	získat
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
Oscara	Oscara	k1gFnSc1	Oscara
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
dílna	dílna	k1gFnSc1	dílna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
první	první	k4xOgMnSc1	první
Rakušan	Rakušan	k1gMnSc1	Rakušan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slavným	slavný	k2eAgMnSc7d1	slavný
divadelním	divadelní	k2eAgMnSc7d1	divadelní
režisérem	režisér	k1gMnSc7	režisér
byl	být	k5eAaImAgMnS	být
Max	Max	k1gMnSc1	Max
Reinhardt	Reinhardt	k1gMnSc1	Reinhardt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
rakouským	rakouský	k2eAgMnPc3d1	rakouský
prozaikům	prozaik	k1gMnPc3	prozaik
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Stefan	Stefan	k1gMnSc1	Stefan
Zweig	Zweig	k1gMnSc1	Zweig
(	(	kIx(	(
<g/>
Šachová	šachový	k2eAgFnSc1d1	šachová
novela	novela	k1gFnSc1	novela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Musil	Musil	k1gMnSc1	Musil
(	(	kIx(	(
<g/>
Muž	muž	k1gMnSc1	muž
bez	bez	k7c2	bez
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
Roth	Roth	k1gMnSc1	Roth
(	(	kIx(	(
<g/>
Pochod	pochod	k1gInSc1	pochod
Radeckého	Radeckého	k2eAgFnSc1d1	Radeckého
<g/>
,	,	kIx,	,
Kapucínská	kapucínský	k2eAgFnSc1d1	Kapucínská
krypta	krypta	k1gFnSc1	krypta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
Meyrink	Meyrink	k1gInSc1	Meyrink
(	(	kIx(	(
<g/>
Golem	Golem	k1gMnSc1	Golem
<g/>
,	,	kIx,	,
Zelená	zelený	k2eAgFnSc1d1	zelená
tvář	tvář	k1gFnSc1	tvář
<g/>
)	)	kIx)	)
či	či	k8xC	či
Hermann	Hermann	k1gMnSc1	Hermann
Broch	Broch	k1gMnSc1	Broch
(	(	kIx(	(
<g/>
Náměsíčníci	náměsíčník	k1gMnPc1	náměsíčník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Adalbert	Adalbert	k1gMnSc1	Adalbert
Stifter	Stifter	k1gMnSc1	Stifter
věnoval	věnovat	k5eAaPmAgMnS	věnovat
mnoho	mnoho	k4c4	mnoho
zájmu	zájem	k1gInSc2	zájem
šumavské	šumavský	k2eAgFnSc3d1	Šumavská
přírodě	příroda	k1gFnSc3	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Altenberg	Altenberg	k1gMnSc1	Altenberg
mapoval	mapovat	k5eAaImAgMnS	mapovat
životní	životní	k2eAgInSc4d1	životní
pocit	pocit	k1gInSc4	pocit
Vídeňáků	Vídeňák	k1gMnPc2	Vídeňák
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
von	von	k1gInSc1	von
Ebner-Eschenbachová	Ebner-Eschenbachová	k1gFnSc1	Ebner-Eschenbachová
byla	být	k5eAaImAgFnS	být
představitelkou	představitelka	k1gFnSc7	představitelka
realismu	realismus	k1gInSc2	realismus
19.	[number]	k4	19.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Králi	Král	k1gMnSc3	Král
rakouské	rakouský	k2eAgFnSc2d1	rakouská
poezie	poezie	k1gFnSc2	poezie
jsou	být	k5eAaImIp3nP	být
romantik	romantik	k1gMnSc1	romantik
Nikolaus	Nikolaus	k1gMnSc1	Nikolaus
Lenau	Lenaus	k1gInSc2	Lenaus
a	a	k8xC	a
Georg	Georg	k1gMnSc1	Georg
Trakl	Trakl	k1gInSc1	Trakl
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
expresionismu	expresionismus	k1gInSc2	expresionismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejslavnějšími	slavný	k2eAgMnPc7d3	nejslavnější
rakouskými	rakouský	k2eAgMnPc7d1	rakouský
dramatiky	dramatik	k1gMnPc7	dramatik
jsou	být	k5eAaImIp3nP	být
Arthur	Arthur	k1gMnSc1	Arthur
Schnitzler	Schnitzler	k1gMnSc1	Schnitzler
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
von	von	k1gInSc4	von
Hofmannsthal	Hofmannsthal	k1gMnSc1	Hofmannsthal
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Nestroy	Nestroa	k1gFnSc2	Nestroa
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Grillparzer	Grillparzer	k1gMnSc1	Grillparzer
a	a	k8xC	a
Ödön	Ödön	k1gInSc1	Ödön
von	von	k1gInSc1	von
Horváth	Horváth	k1gInSc1	Horváth
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
rakouské	rakouský	k2eAgMnPc4d1	rakouský
spisovatele	spisovatel	k1gMnPc4	spisovatel
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
i	i	k9	i
pražské	pražský	k2eAgMnPc4d1	pražský
německy	německy	k6eAd1	německy
píšící	píšící	k2eAgMnPc4d1	píšící
autory	autor	k1gMnPc4	autor
jako	jako	k9	jako
byli	být	k5eAaImAgMnP	být
Franz	Franz	k1gMnSc1	Franz
Kafka	Kafka	k1gMnSc1	Kafka
<g/>
,	,	kIx,	,
Rainer	Rainer	k1gMnSc1	Rainer
Maria	Mario	k1gMnSc2	Mario
Rilke	Rilke	k1gNnSc2	Rilke
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Werfel	Werfel	k1gMnSc1	Werfel
či	či	k8xC	či
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
Elfriede	Elfried	k1gMnSc5	Elfried
Jelineková	Jelinekový	k2eAgFnSc1d1	Jelineková
<g/>
,	,	kIx,	,
oceněná	oceněný	k2eAgFnSc1d1	oceněná
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
Bernhard	Bernhard	k1gMnSc1	Bernhard
<g/>
,	,	kIx,	,
Ernst	Ernst	k1gMnSc1	Ernst
Jandl	Jandl	k1gMnSc1	Jandl
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
Fried	Fried	k1gMnSc1	Fried
<g/>
,	,	kIx,	,
Ilse	Ilse	k1gFnSc1	Ilse
Aichingerová	Aichingerová	k1gFnSc1	Aichingerová
či	či	k8xC	či
Ingeborg	Ingeborg	k1gMnSc1	Ingeborg
Bachmannová	Bachmannová	k1gFnSc1	Bachmannová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
oceňováni	oceňován	k2eAgMnPc1d1	oceňován
Peter	Peter	k1gMnSc1	Peter
Handke	Handk	k1gFnSc2	Handk
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
Ebner	Ebner	k1gMnSc1	Ebner
<g/>
,	,	kIx,	,
Friederike	Friederike	k1gFnSc1	Friederike
Mayröcker	Mayröcker	k1gMnSc1	Mayröcker
či	či	k8xC	či
Daniel	Daniel	k1gMnSc1	Daniel
Kehlmann	Kehlmann	k1gMnSc1	Kehlmann
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dětské	dětský	k2eAgFnSc2d1	dětská
literatury	literatura	k1gFnSc2	literatura
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
Felix	Felix	k1gMnSc1	Felix
Salten	Saltno	k1gNnPc2	Saltno
a	a	k8xC	a
Christine	Christin	k1gInSc5	Christin
Nöstlingerová	Nöstlingerový	k2eAgFnSc1d1	Nöstlingerový
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
humorista	humorista	k1gMnSc1	humorista
a	a	k8xC	a
satirik	satirik	k1gMnSc1	satirik
proslul	proslout	k5eAaPmAgMnS	proslout
Karl	Karl	k1gMnSc1	Karl
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
zejm	zejm	k?	zejm
<g/>
.	.	kIx.	.
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Die	Die	k1gMnSc1	Die
Fackel	Fackel	k1gMnSc1	Fackel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sektoru	sektor	k1gInSc6	sektor
erotické	erotický	k2eAgFnSc2d1	erotická
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
nepřehlédnutelným	přehlédnutelný	k2eNgInSc7d1	nepřehlédnutelný
Leopold	Leopolda	k1gFnPc2	Leopolda
von	von	k1gInSc1	von
Sacher-Masoch	Sacher-Masoch	k1gInSc4	Sacher-Masoch
<g/>
,	,	kIx,	,
za	za	k7c4	za
populární	populární	k2eAgFnSc4d1	populární
literaturu	literatura	k1gFnSc4	literatura
bývá	bývat	k5eAaImIp3nS	bývat
označováno	označovat	k5eAaImNgNnS	označovat
dílo	dílo	k1gNnSc1	dílo
Johannese	Johannese	k1gFnSc2	Johannese
Maria	Mario	k1gMnSc2	Mario
Simmela	Simmel	k1gMnSc2	Simmel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Vrcholným	vrcholný	k2eAgMnSc7d1	vrcholný
představitelem	představitel	k1gMnSc7	představitel
gotického	gotický	k2eAgNnSc2d1	gotické
malířství	malířství	k1gNnSc2	malířství
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
byl	být	k5eAaImAgMnS	být
Michael	Michael	k1gMnSc1	Michael
Pacher	Pachra	k1gFnPc2	Pachra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19.	[number]	k4	19.
století	století	k1gNnSc2	století
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
kulturu	kultura	k1gFnSc4	kultura
významně	významně	k6eAd1	významně
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
směr	směr	k1gInSc4	směr
zvaný	zvaný	k2eAgInSc1d1	zvaný
biedermeier	biedermeier	k1gInSc1	biedermeier
<g/>
,	,	kIx,	,
v	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
ho	on	k3xPp3gMnSc4	on
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
zvláště	zvláště	k6eAd1	zvláště
Moritz	moritz	k1gInSc4	moritz
von	von	k1gInSc4	von
Schwind	Schwinda	k1gFnPc2	Schwinda
a	a	k8xC	a
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Georg	Georg	k1gMnSc1	Georg
Waldmüller	Waldmüller	k1gMnSc1	Waldmüller
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
fázích	fáze	k1gFnPc6	fáze
19.	[number]	k4	19.
století	století	k1gNnSc2	století
převládl	převládnout	k5eAaPmAgMnS	převládnout
akademismus	akademismus	k1gInSc4	akademismus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
došel	dojít	k5eAaPmAgInS	dojít
naplnění	naplnění	k1gNnSc4	naplnění
zejména	zejména	k6eAd1	zejména
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Hanse	Hans	k1gMnSc2	Hans
Makarta	Makart	k1gMnSc2	Makart
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejslavnější	slavný	k2eAgNnSc4d3	nejslavnější
období	období	k1gNnSc4	období
rakouského	rakouský	k2eAgNnSc2d1	rakouské
malířství	malířství	k1gNnSc2	malířství
zažila	zažít	k5eAaPmAgFnS	zažít
Vídeň	Vídeň	k1gFnSc1	Vídeň
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
centrem	centrum	k1gNnSc7	centrum
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
zde	zde	k6eAd1	zde
působili	působit	k5eAaImAgMnP	působit
malíři	malíř	k1gMnPc1	malíř
Gustav	Gustav	k1gMnSc1	Gustav
Klimt	Klimt	k1gMnSc1	Klimt
<g/>
,	,	kIx,	,
Oskar	Oskar	k1gMnSc1	Oskar
Kokoschka	Kokoschka	k1gMnSc1	Kokoschka
a	a	k8xC	a
Egon	Egon	k1gMnSc1	Egon
Schiele	Schiel	k1gInSc2	Schiel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
designu	design	k1gInSc6	design
secesi	secese	k1gFnSc4	secese
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
Koloman	Koloman	k1gMnSc1	Koloman
Moser	Moser	k1gMnSc1	Moser
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20.	[number]	k4	20.
století	století	k1gNnPc2	století
přišla	přijít	k5eAaPmAgFnS	přijít
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
i	i	k9	i
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
vlna	vlna	k1gFnSc1	vlna
modernismu	modernismus	k1gInSc2	modernismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rakouském	rakouský	k2eAgNnSc6d1	rakouské
malířství	malířství	k1gNnSc6	malířství
ho	on	k3xPp3gMnSc4	on
představují	představovat	k5eAaImIp3nP	představovat
například	například	k6eAd1	například
Alfred	Alfred	k1gMnSc1	Alfred
Kubin	Kubin	k1gMnSc1	Kubin
(	(	kIx(	(
<g/>
expresionismus	expresionismus	k1gInSc1	expresionismus
<g/>
)	)	kIx)	)
či	či	k8xC	či
Raoul	Raoul	k1gInSc1	Raoul
Hausmann	Hausmanna	k1gFnPc2	Hausmanna
(	(	kIx(	(
<g/>
expresionismus	expresionismus	k1gInSc1	expresionismus
a	a	k8xC	a
dadaismus	dadaismus	k1gInSc1	dadaismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
výrazná	výrazný	k2eAgFnSc1d1	výrazná
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
škola	škola	k1gFnSc1	škola
fantastického	fantastický	k2eAgInSc2d1	fantastický
realismu	realismus	k1gInSc2	realismus
<g/>
,	,	kIx,	,
označovaná	označovaný	k2eAgFnSc1d1	označovaná
za	za	k7c4	za
pozdně	pozdně	k6eAd1	pozdně
surrealistické	surrealistický	k2eAgNnSc4d1	surrealistické
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentoval	reprezentovat	k5eAaImAgMnS	reprezentovat
ji	on	k3xPp3gFnSc4	on
např.	např.	kA	např.
Ernst	Ernst	k1gMnSc1	Ernst
Fuchs	Fuchs	k1gMnSc1	Fuchs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
okruhu	okruh	k1gInSc6	okruh
tvořil	tvořit	k5eAaImAgMnS	tvořit
také	také	k9	také
Friedensreich	Friedensreich	k1gMnSc1	Friedensreich
Hundertwasser	Hundertwasser	k1gMnSc1	Hundertwasser
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
výrazným	výrazný	k2eAgInSc7d1	výrazný
fenoménem	fenomén	k1gInSc7	fenomén
je	být	k5eAaImIp3nS	být
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
akcionismus	akcionismus	k1gInSc1	akcionismus
(	(	kIx(	(
<g/>
Wiener	Wiener	k1gInSc1	Wiener
Aktionismus	Aktionismus	k1gInSc1	Aktionismus
<g/>
)	)	kIx)	)
60.	[number]	k4	60.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
Günter	Günter	k1gInSc1	Günter
Brus	brus	k1gInSc1	brus
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Muehl	Muehl	k1gMnSc1	Muehl
<g/>
,	,	kIx,	,
Hermann	Hermann	k1gMnSc1	Hermann
Nitsch	Nitsch	k1gMnSc1	Nitsch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
malířství	malířství	k1gNnSc2	malířství
a	a	k8xC	a
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
vizuální	vizuální	k2eAgMnSc1d1	vizuální
umělec	umělec	k1gMnSc1	umělec
Gottfried	Gottfried	k1gMnSc1	Gottfried
Helnwein	Helnwein	k1gMnSc1	Helnwein
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
umělecký	umělecký	k2eAgMnSc1d1	umělecký
fotograf	fotograf	k1gMnSc1	fotograf
Willy	Willa	k1gFnSc2	Willa
Puchner	Puchner	k1gMnSc1	Puchner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgMnPc4d1	významný
rakouské	rakouský	k2eAgMnPc4d1	rakouský
sochaře	sochař	k1gMnPc4	sochař
patří	patřit	k5eAaImIp3nS	patřit
mj.	mj.	kA	mj.
Niclas	Niclas	k1gInSc1	Niclas
Gerhaert	Gerhaert	k1gInSc1	Gerhaert
van	vana	k1gFnPc2	vana
Leyden	Leydna	k1gFnPc2	Leydna
<g/>
,	,	kIx,	,
Franz	Franz	k1gMnSc1	Franz
Xaver	Xaver	k1gMnSc1	Xaver
Messerschmidt	Messerschmidt	k1gMnSc1	Messerschmidt
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gMnSc1	Fritz
Wotruba	Wotruba	k1gMnSc1	Wotruba
nebo	nebo	k8xC	nebo
Alfred	Alfred	k1gMnSc1	Alfred
Hrdlicka	Hrdlicka	k1gFnSc1	Hrdlicka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
architekty	architekt	k1gMnPc7	architekt
barokní	barokní	k2eAgFnSc2d1	barokní
éry	éra	k1gFnSc2	éra
byli	být	k5eAaImAgMnP	být
Johann	Johann	k1gMnSc1	Johann
Bernhard	Bernhard	k1gMnSc1	Bernhard
Fischer	Fischer	k1gMnSc1	Fischer
a	a	k8xC	a
Johann	Johann	k1gMnSc1	Johann
Lucas	Lucas	k1gMnSc1	Lucas
von	von	k1gInSc4	von
Hildebrandt	Hildebrandt	k2eAgInSc4d1	Hildebrandt
<g/>
.	.	kIx.	.
</s>
<s>
Vídeňskou	vídeňský	k2eAgFnSc4d1	Vídeňská
secesi	secese	k1gFnSc4	secese
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
Otto	Otto	k1gMnSc1	Otto
Wagner	Wagner	k1gMnSc1	Wagner
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Hoffmann	Hoffmann	k1gMnSc1	Hoffmann
<g/>
.	.	kIx.	.
</s>
<s>
Modernismu	modernismus	k1gInSc2	modernismus
byl	být	k5eAaImAgMnS	být
věrný	věrný	k2eAgMnSc1d1	věrný
Adolf	Adolf	k1gMnSc1	Adolf
Loos	Loos	k1gInSc1	Loos
či	či	k8xC	či
Richard	Richard	k1gMnSc1	Richard
Neutra	neutrum	k1gNnSc2	neutrum
<g/>
.	.	kIx.	.
</s>
<s>
Představitelem	představitel	k1gMnSc7	představitel
postmoderní	postmoderní	k2eAgFnSc2d1	postmoderní
architektury	architektura	k1gFnSc2	architektura
je	být	k5eAaImIp3nS	být
Hans	Hans	k1gMnSc1	Hans
Hollein	Hollein	k1gMnSc1	Hollein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Věda	věda	k1gFnSc1	věda
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
vedoucích	vedoucí	k2eAgNnPc2d1	vedoucí
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
center	centrum	k1gNnPc2	centrum
světa	svět	k1gInSc2	svět
a	a	k8xC	a
přineslo	přinést	k5eAaPmAgNnS	přinést
mu	on	k3xPp3gMnSc3	on
geniální	geniální	k2eAgMnPc4d1	geniální
myslitele	myslitel	k1gMnPc4	myslitel
a	a	k8xC	a
výzkumníky	výzkumník	k1gMnPc4	výzkumník
jako	jako	k8xC	jako
zakladatele	zakladatel	k1gMnPc4	zakladatel
kvantové	kvantový	k2eAgFnSc2d1	kvantová
fyziky	fyzika	k1gFnSc2	fyzika
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
Pauliho	Pauli	k1gMnSc2	Pauli
a	a	k8xC	a
Erwina	Erwin	k1gMnSc2	Erwin
Schrödingera	Schrödinger	k1gMnSc2	Schrödinger
<g/>
,	,	kIx,	,
otce	otec	k1gMnSc2	otec
zoopsychologie	zoopsychologie	k1gFnSc2	zoopsychologie
Konrada	Konrada	k1gFnSc1	Konrada
Lorenze	Lorenz	k1gMnSc2	Lorenz
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc2	vynálezce
Viktora	Viktor	k1gMnSc2	Viktor
Kaplana	Kaplan	k1gMnSc2	Kaplan
<g/>
,	,	kIx,	,
průkopníka	průkopník	k1gMnSc2	průkopník
termodynamiky	termodynamika	k1gFnSc2	termodynamika
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Boltzmanna	Boltzmann	k1gMnSc2	Boltzmann
<g/>
,	,	kIx,	,
objevitele	objevitel	k1gMnSc2	objevitel
struktury	struktura	k1gFnSc2	struktura
benzolu	benzol	k1gInSc2	benzol
Johanna	Johann	k1gMnSc2	Johann
Josefa	Josef	k1gMnSc2	Josef
Loschmidta	Loschmidt	k1gMnSc2	Loschmidt
<g/>
,	,	kIx,	,
objevitele	objevitel	k1gMnSc2	objevitel
krevních	krevní	k2eAgFnPc2d1	krevní
skupin	skupina	k1gFnPc2	skupina
Karla	Karel	k1gMnSc2	Karel
Landsteinera	Landsteiner	k1gMnSc2	Landsteiner
<g/>
,	,	kIx,	,
zachránce	zachránce	k1gMnSc2	zachránce
matek	matka	k1gFnPc2	matka
doktora	doktor	k1gMnSc2	doktor
Ignaze	Ignaha	k1gFnSc6	Ignaha
Semmelweise	Semmelweis	k1gInSc6	Semmelweis
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
vědcům	vědec	k1gMnPc3	vědec
patřil	patřit	k5eAaImAgMnS	patřit
též	též	k9	též
objevitel	objevitel	k1gMnSc1	objevitel
Dopplerova	Dopplerův	k2eAgInSc2d1	Dopplerův
jevu	jev	k1gInSc2	jev
Christian	Christian	k1gMnSc1	Christian
Doppler	Doppler	k1gMnSc1	Doppler
<g/>
,	,	kIx,	,
badatelka	badatelka	k1gFnSc1	badatelka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
Lise	Lisa	k1gFnSc3	Lisa
Meitnerová	Meitnerová	k1gFnSc1	Meitnerová
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
synovec	synovec	k1gMnSc1	synovec
Otto	Otto	k1gMnSc1	Otto
Frisch	Frisch	k1gMnSc1	Frisch
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
etologie	etologie	k1gFnSc2	etologie
Karl	Karl	k1gInSc1	Karl
von	von	k1gInSc1	von
Frisch	Frisch	k1gInSc4	Frisch
<g/>
,	,	kIx,	,
molekulární	molekulární	k2eAgMnSc1d1	molekulární
biolog	biolog	k1gMnSc1	biolog
Max	Max	k1gMnSc1	Max
Perutz	Perutz	k1gMnSc1	Perutz
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Martin	Martin	k1gMnSc1	Martin
Karplus	Karplus	k1gMnSc1	Karplus
<g/>
,	,	kIx,	,
neuropsychiatr	neuropsychiatr	k1gMnSc1	neuropsychiatr
Eric	Eric	k1gFnSc4	Eric
Kandel	kandela	k1gFnPc2	kandela
<g/>
,	,	kIx,	,
teoretický	teoretický	k2eAgInSc1d1	teoretický
<g />
.	.	kIx.	.
</s>
<s>
fyzik	fyzik	k1gMnSc1	fyzik
Walter	Walter	k1gMnSc1	Walter
Kohn	Kohn	k1gMnSc1	Kohn
<g/>
,	,	kIx,	,
biochemik	biochemik	k1gMnSc1	biochemik
Richard	Richard	k1gMnSc1	Richard
Kuhn	Kuhn	k1gMnSc1	Kuhn
<g/>
,	,	kIx,	,
farmakolog	farmakolog	k1gMnSc1	farmakolog
Otto	Otto	k1gMnSc1	Otto
Loewi	Loewe	k1gFnSc4	Loewe
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
Victor	Victor	k1gMnSc1	Victor
Franz	Franz	k1gMnSc1	Franz
Hess	Hess	k1gInSc1	Hess
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
Julius	Julius	k1gMnSc1	Julius
Wagner-Jauregg	Wagner-Jauregg	k1gMnSc1	Wagner-Jauregg
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
stupnice	stupnice	k1gFnSc2	stupnice
tvrdosti	tvrdost	k1gFnSc2	tvrdost
nerostů	nerost	k1gInPc2	nerost
Friedrich	Friedrich	k1gMnSc1	Friedrich
Mohs	Mohs	k1gInSc1	Mohs
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
teze	teze	k1gFnSc2	teze
o	o	k7c6	o
prakontinentu	prakontinent	k1gInSc6	prakontinent
Gondwana	Gondwan	k1gMnSc2	Gondwan
geolog	geolog	k1gMnSc1	geolog
Eduard	Eduard	k1gMnSc1	Eduard
Suess	Suess	k1gInSc1	Suess
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
paleobiologie	paleobiologie	k1gFnSc2	paleobiologie
Othenio	Othenio	k1gMnSc1	Othenio
Abel	Abel	k1gMnSc1	Abel
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
teorie	teorie	k1gFnSc2	teorie
fázového	fázový	k2eAgInSc2d1	fázový
přechodu	přechod	k1gInSc2	přechod
fyzik	fyzik	k1gMnSc1	fyzik
Paul	Paul	k1gMnSc1	Paul
Ehrenfest	Ehrenfest	k1gMnSc1	Ehrenfest
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
sexuologie	sexuologie	k1gFnSc2	sexuologie
Richard	Richard	k1gMnSc1	Richard
von	von	k1gInSc1	von
Krafft-Ebing	Krafft-Ebing	k1gInSc1	Krafft-Ebing
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
Emil	Emil	k1gMnSc1	Emil
Artin	Artin	k1gMnSc1	Artin
<g/>
,	,	kIx,	,
biolog	biolog	k1gMnSc1	biolog
Giovanni	Giovann	k1gMnPc1	Giovann
Antonio	Antonio	k1gMnSc1	Antonio
Scopoli	Scopole	k1gFnSc4	Scopole
<g/>
,	,	kIx,	,
významný	významný	k2eAgMnSc1d1	významný
lékař	lékař	k1gMnSc1	lékař
Theodor	Theodor	k1gMnSc1	Theodor
Billroth	Billroth	k1gMnSc1	Billroth
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
Richard	Richard	k1gMnSc1	Richard
von	von	k1gInSc4	von
Mises	Mises	k1gMnSc1	Mises
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
Georg	Georg	k1gMnSc1	Georg
Joachim	Joachim	k1gMnSc1	Joachim
Rhaeticus	Rhaeticus	k1gMnSc1	Rhaeticus
<g/>
,	,	kIx,	,
středověký	středověký	k2eAgMnSc1d1	středověký
astronom	astronom	k1gMnSc1	astronom
Georg	Georg	k1gMnSc1	Georg
von	von	k1gInSc4	von
Peuerbach	Peuerbach	k1gMnSc1	Peuerbach
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
výzkumu	výzkum	k1gInSc2	výzkum
stresu	stres	k1gInSc2	stres
Hans	hansa	k1gFnPc2	hansa
Selye	Selye	k1gInSc1	Selye
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
Nikolaus	Nikolaus	k1gMnSc1	Nikolaus
Joseph	Joseph	k1gMnSc1	Joseph
von	von	k1gInSc4	von
Jacquin	Jacquin	k1gMnSc1	Jacquin
<g/>
,	,	kIx,	,
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
Victor	Victor	k1gMnSc1	Victor
Frederick	Frederick	k1gMnSc1	Frederick
Weisskopf	Weisskopf	k1gMnSc1	Weisskopf
nebo	nebo	k8xC	nebo
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
firmy	firma	k1gFnSc2	firma
Osram	Osram	k1gInSc1	Osram
Carl	Carl	k1gMnSc1	Carl
Auer	Auer	k1gMnSc1	Auer
von	von	k1gInSc4	von
Welsbach	Welsbacha	k1gFnPc2	Welsbacha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Také	také	k9	také
v	v	k7c6	v
humanitních	humanitní	k2eAgFnPc6d1	humanitní
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc6d1	sociální
vědách	věda	k1gFnPc6	věda
se	se	k3xPyFc4	se
Rakousko	Rakousko	k1gNnSc1	Rakousko
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
hodině	hodina	k1gFnSc6	hodina
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19.	[number]	k4	19.
a	a	k8xC	a
20.	[number]	k4	20.
století	století	k1gNnPc2	století
stalo	stát	k5eAaPmAgNnS	stát
epicentrem	epicentrum	k1gNnSc7	epicentrum
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Lingvista	lingvista	k1gMnSc1	lingvista
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
Ludwig	Ludwig	k1gMnSc1	Ludwig
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
způsobil	způsobit	k5eAaPmAgMnS	způsobit
myšlenkovou	myšlenkový	k2eAgFnSc4d1	myšlenková
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
k	k	k7c3	k
jazyku	jazyk	k1gInSc3	jazyk
a	a	k8xC	a
ke	k	k7c3	k
světu	svět	k1gInSc3	svět
skrze	skrze	k?	skrze
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgFnSc4	první
psychoterapii	psychoterapie	k1gFnSc4	psychoterapie
-	-	kIx~	-
psychoanalýzu	psychoanalýza	k1gFnSc4	psychoanalýza
-	-	kIx~	-
a	a	k8xC	a
změnil	změnit	k5eAaPmAgInS	změnit
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
moderní	moderní	k2eAgMnPc1d1	moderní
člověk	člověk	k1gMnSc1	člověk
nazírá	nazírat	k5eAaImIp3nS	nazírat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
učebnic	učebnice	k1gFnPc2	učebnice
psychologie	psychologie	k1gFnSc2	psychologie
vzápětí	vzápětí	k6eAd1	vzápětí
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
i	i	k9	i
Alfred	Alfred	k1gMnSc1	Alfred
Adler	Adler	k1gMnSc1	Adler
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Reich	Reich	k?	Reich
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
Freudová	Freudová	k1gFnSc1	Freudová
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
Viktor	Viktor	k1gMnSc1	Viktor
Frankl	Frankl	k1gMnSc1	Frankl
<g/>
.	.	kIx.	.
</s>
<s>
Oborem	obor	k1gInSc7	obor
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
obzvláště	obzvláště	k6eAd1	obzvláště
dařilo	dařit	k5eAaImAgNnS	dařit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
ekonomie	ekonomie	k1gFnSc1	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
jedna	jeden	k4xCgFnSc1	jeden
škola	škola	k1gFnSc1	škola
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
ekonomii	ekonomie	k1gFnSc6	ekonomie
nazývána	nazýván	k2eAgFnSc1d1	nazývána
"	"	kIx"	"
<g/>
rakouská	rakouský	k2eAgFnSc1d1	rakouská
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Carl	Carl	k1gMnSc1	Carl
Menger	Menger	k1gMnSc1	Menger
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
August	August	k1gMnSc1	August
von	von	k1gInSc4	von
Hayek	Hayek	k1gInSc1	Hayek
<g/>
,	,	kIx,	,
Ludwig	Ludwig	k1gInSc1	Ludwig
von	von	k1gInSc1	von
Mises	Mises	k1gInSc1	Mises
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc1	von
Wieser	Wieser	k1gInSc1	Wieser
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
proslulost	proslulost	k1gFnSc4	proslulost
získali	získat	k5eAaPmAgMnP	získat
i	i	k9	i
autoři	autor	k1gMnPc1	autor
mimo	mimo	k7c4	mimo
tuto	tento	k3xDgFnSc4	tento
tradici	tradice	k1gFnSc4	tradice
<g/>
:	:	kIx,	:
Ekonom	ekonom	k1gMnSc1	ekonom
Karl	Karl	k1gMnSc1	Karl
Polanyi	Polanye	k1gFnSc4	Polanye
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
slavného	slavný	k2eAgNnSc2d1	slavné
díla	dílo	k1gNnSc2	dílo
Velká	velký	k2eAgFnSc1d1	velká
transformace	transformace	k1gFnSc1	transformace
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
antropologické	antropologický	k2eAgFnSc2d1	antropologická
ekonomie	ekonomie	k1gFnSc2	ekonomie
či	či	k8xC	či
zakladatel	zakladatel	k1gMnSc1	zakladatel
moderního	moderní	k2eAgInSc2d1	moderní
managementu	management	k1gInSc2	management
Peter	Peter	k1gMnSc1	Peter
Drucker	Drucker	k1gMnSc1	Drucker
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
rakouským	rakouský	k2eAgMnSc7d1	rakouský
sociologem	sociolog	k1gMnSc7	sociolog
byl	být	k5eAaImAgMnS	být
Alfred	Alfred	k1gMnSc1	Alfred
Schütz	Schütz	k1gMnSc1	Schütz
<g/>
,	,	kIx,	,
jazykovědcem	jazykovědec	k1gMnSc7	jazykovědec
Karl	Karla	k1gFnPc2	Karla
Bühler	Bühler	k1gInSc1	Bühler
<g/>
.	.	kIx.	.
</s>
<s>
Paul	Paul	k1gMnSc1	Paul
Felix	Felix	k1gMnSc1	Felix
Lazarsfeld	Lazarsfeld	k1gMnSc1	Lazarsfeld
založil	založit	k5eAaPmAgMnS	založit
kvantitativní	kvantitativní	k2eAgInSc4d1	kvantitativní
výzkum	výzkum	k1gInSc4	výzkum
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Prosadili	prosadit	k5eAaPmAgMnP	prosadit
se	se	k3xPyFc4	se
i	i	k9	i
muzikolog	muzikolog	k1gMnSc1	muzikolog
Ludwig	Ludwiga	k1gFnPc2	Ludwiga
von	von	k1gInSc1	von
Köchel	Köchel	k1gMnSc1	Köchel
či	či	k8xC	či
religionista	religionista	k1gMnSc1	religionista
Volker	Volker	k1gMnSc1	Volker
Zotz	Zotz	k1gMnSc1	Zotz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filozofii	filozofie	k1gFnSc6	filozofie
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kroužku	kroužek	k1gInSc6	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Přináležel	přináležet	k5eAaImAgInS	přináležet
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
i	i	k8xC	i
Karl	Karl	k1gMnSc1	Karl
Popper	Popper	k1gMnSc1	Popper
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
filozofů	filozof	k1gMnPc2	filozof
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Otto	Otto	k1gMnSc1	Otto
Neurath	Neurath	k1gMnSc1	Neurath
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
existencialismu	existencialismus	k1gInSc3	existencialismus
je	být	k5eAaImIp3nS	být
řazeno	řazen	k2eAgNnSc1d1	řazeno
dílo	dílo	k1gNnSc1	dílo
Martina	Martin	k1gMnSc2	Martin
Bubera	Buber	k1gMnSc2	Buber
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
představitelem	představitel	k1gMnSc7	představitel
postmoderní	postmoderní	k2eAgFnSc2d1	postmoderní
filozofie	filozofie	k1gFnSc2	filozofie
byl	být	k5eAaImAgMnS	být
Paul	Paul	k1gMnSc1	Paul
Karl	Karl	k1gMnSc1	Karl
Feyerabend	Feyerabend	k1gMnSc1	Feyerabend
<g/>
.	.	kIx.	.
</s>
<s>
Marxismus	marxismus	k1gInSc4	marxismus
rozvíjeli	rozvíjet	k5eAaImAgMnP	rozvíjet
Otto	Otto	k1gMnSc1	Otto
Bauer	Bauer	k1gMnSc1	Bauer
či	či	k8xC	či
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hilferding	Hilferding	k1gInSc1	Hilferding
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
středoevropské	středoevropský	k2eAgFnSc2d1	středoevropská
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
,	,	kIx,	,
především	především	k9	především
jako	jako	k9	jako
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
sehrál	sehrát	k5eAaPmAgMnS	sehrát
Franz	Franz	k1gMnSc1	Franz
Brentano	Brentana	k1gFnSc5	Brentana
<g/>
.	.	kIx.	.
</s>
<s>
Kritikou	kritika	k1gFnSc7	kritika
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
civilizace	civilizace	k1gFnSc2	civilizace
proslul	proslout	k5eAaPmAgMnS	proslout
Ivan	Ivan	k1gMnSc1	Ivan
Illich	Illich	k1gMnSc1	Illich
<g/>
.	.	kIx.	.
</s>
<s>
Představitelem	představitel	k1gMnSc7	představitel
hnutí	hnutí	k1gNnSc2	hnutí
new	new	k?	new
age	age	k?	age
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Fritjof	Fritjof	k1gMnSc1	Fritjof
Capra	Capra	k1gMnSc1	Capra
<g/>
.	.	kIx.	.
</s>
<s>
Filozofem	filozof	k1gMnSc7	filozof
Nové	Nové	k2eAgFnSc2d1	Nové
levice	levice	k1gFnSc2	levice
byl	být	k5eAaImAgMnS	být
André	André	k1gMnSc1	André
Gorz	Gorz	k1gMnSc1	Gorz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Média	médium	k1gNnPc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
novinového	novinový	k2eAgInSc2d1	novinový
trhu	trh	k1gInSc2	trh
ovládají	ovládat	k5eAaImIp3nP	ovládat
bulvární	bulvární	k2eAgInPc1d1	bulvární
tituly	titul	k1gInPc1	titul
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Kronen	Kronna	k1gFnPc2	Kronna
Zeitung	Zeitunga	k1gFnPc2	Zeitunga
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
serióznější	seriózní	k2eAgInPc4d2	serióznější
deníky	deník	k1gInPc4	deník
patří	patřit	k5eAaImIp3nS	patřit
Der	drát	k5eAaImRp2nS	drát
Standard	standard	k1gInSc1	standard
<g/>
,	,	kIx,	,
Die	Die	k1gFnSc1	Die
Presse	Presse	k1gFnSc1	Presse
a	a	k8xC	a
Kurier	Kurier	k1gInSc1	Kurier
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
týdeníkům	týdeník	k1gInPc3	týdeník
patří	patřit	k5eAaImIp3nS	patřit
News	News	k1gInSc1	News
<g/>
,	,	kIx,	,
Profil	profil	k1gInSc1	profil
<g/>
,	,	kIx,	,
Format	Format	k1gInSc1	Format
a	a	k8xC	a
Falter	Falter	k1gInSc1	Falter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
působí	působit	k5eAaImIp3nS	působit
veřejnoprávní	veřejnoprávní	k2eAgInSc1d1	veřejnoprávní
rozhlas	rozhlas	k1gInSc1	rozhlas
a	a	k8xC	a
televize	televize	k1gFnSc1	televize
Österreichischer	Österreichischra	k1gFnPc2	Österreichischra
Rundfunk	Rundfunk	k1gMnSc1	Rundfunk
(	(	kIx(	(
<g/>
ORF	ORF	kA	ORF
<g/>
)	)	kIx)	)
s	s	k7c7	s
několika	několik	k4yIc7	několik
stanicemi	stanice	k1gFnPc7	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
tiskovou	tiskový	k2eAgFnSc7d1	tisková
agenturou	agentura	k1gFnSc7	agentura
je	být	k5eAaImIp3nS	být
APA	APA	kA	APA
(	(	kIx(	(
<g/>
Austria	Austrium	k1gNnSc2	Austrium
Presse	Presse	k1gFnSc2	Presse
Agentur	agentura	k1gFnPc2	agentura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
rakouským	rakouský	k2eAgMnPc3d1	rakouský
sportovcům	sportovec	k1gMnPc3	sportovec
patří	patřit	k5eAaImIp3nP	patřit
pilot	pilot	k1gMnSc1	pilot
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
a	a	k8xC	a
trojnásobný	trojnásobný	k2eAgMnSc1d1	trojnásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
Niki	Niki	k1gNnSc2	Niki
Lauda	Laudo	k1gNnSc2	Laudo
<g/>
.	.	kIx.	.
</s>
<s>
Mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
byl	být	k5eAaImAgMnS	být
i	i	k8xC	i
Jochen	Jochen	k2eAgMnSc1d1	Jochen
Rindt	Rindt	k1gMnSc1	Rindt
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
stáj	stáj	k1gFnSc1	stáj
Red	Red	k1gFnSc2	Red
Bull	bulla	k1gFnPc2	bulla
Racing	Racing	k1gInSc4	Racing
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
seriál	seriál	k1gInSc4	seriál
F1	F1	k1gFnSc2	F1
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nS	jezdit
závod	závod	k1gInSc1	závod
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
Red	Red	k1gFnSc2	Red
Bull	bulla	k1gFnPc2	bulla
Ring	ring	k1gInSc1	ring
poblíž	poblíž	k6eAd1	poblíž
Spielbergu	Spielberg	k1gMnSc3	Spielberg
ve	v	k7c6	v
Štýrsku	Štýrsko	k1gNnSc6	Štýrsko
<g/>
.	.	kIx.	.
</s>
<s>
Mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
závodech	závod	k1gInPc6	závod
silničních	silniční	k2eAgInPc2d1	silniční
motocyklů	motocykl	k1gInPc2	motocykl
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Rupert	Rupert	k1gMnSc1	Rupert
Hollaus	Hollaus	k1gMnSc1	Hollaus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
Rakušané	Rakušan	k1gMnPc1	Rakušan
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
ve	v	k7c6	v
sjezdovém	sjezdový	k2eAgNnSc6d1	sjezdové
lyžování	lyžování	k1gNnSc6	lyžování
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
kralují	kralovat	k5eAaImIp3nP	kralovat
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
tabulce	tabulka	k1gFnSc6	tabulka
olympijských	olympijský	k2eAgMnPc2d1	olympijský
medailistů	medailista	k1gMnPc2	medailista
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc4	tři
zlaté	zlatý	k2eAgFnPc4d1	zlatá
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
má	mít	k5eAaImIp3nS	mít
sjezdař	sjezdař	k1gMnSc1	sjezdař
Toni	Toni	k1gMnSc1	Toni
Sailer	Sailer	k1gMnSc1	Sailer
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
Hermann	Hermann	k1gMnSc1	Hermann
Maier	Maier	k1gMnSc1	Maier
<g/>
,	,	kIx,	,
Petra	Petra	k1gFnSc1	Petra
Kronbergerová	Kronbergerová	k1gFnSc1	Kronbergerová
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Raich	Raich	k1gMnSc1	Raich
<g/>
,	,	kIx,	,
Michaela	Michaela	k1gFnSc1	Michaela
Dorfmeisterová	Dorfmeisterová	k1gFnSc1	Dorfmeisterová
a	a	k8xC	a
Marcel	Marcel	k1gMnSc1	Marcel
Hirscher	Hirschra	k1gFnPc2	Hirschra
<g/>
.	.	kIx.	.
</s>
<s>
Annemarie	Annemarie	k1gFnSc1	Annemarie
Moserová-Pröllová	Moserová-Pröllová	k1gFnSc1	Moserová-Pröllová
šestkrát	šestkrát	k6eAd1	šestkrát
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
titulů	titul	k1gInPc2	titul
mistryně	mistryně	k1gFnSc2	mistryně
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severské	severský	k2eAgFnSc2d1	severská
kombinace	kombinace	k1gFnSc2	kombinace
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
olympijské	olympijský	k2eAgFnPc4d1	olympijská
zlaté	zlatá	k1gFnPc4	zlatá
Felix	Felix	k1gMnSc1	Felix
Gottwald	Gottwald	k1gMnSc1	Gottwald
<g/>
,	,	kIx,	,
ze	z	k7c2	z
skokanského	skokanský	k2eAgInSc2d1	skokanský
můstku	můstek	k1gInSc2	můstek
Thomas	Thomas	k1gMnSc1	Thomas
Morgenstern	Morgenstern	k1gMnSc1	Morgenstern
<g/>
.	.	kIx.	.
</s>
<s>
Olympijské	olympijský	k2eAgNnSc1d1	Olympijské
zlato	zlato	k1gNnSc1	zlato
na	na	k7c6	na
můstcích	můstek	k1gInPc6	můstek
vybojovali	vybojovat	k5eAaPmAgMnP	vybojovat
i	i	k9	i
Ernst	Ernst	k1gMnSc1	Ernst
Vettori	Vettor	k1gFnSc2	Vettor
<g/>
,	,	kIx,	,
Andreas	Andreas	k1gMnSc1	Andreas
Widhölzl	Widhölzl	k1gMnSc1	Widhölzl
či	či	k8xC	či
Gregor	Gregor	k1gMnSc1	Gregor
Schlierenzauer	Schlierenzauer	k1gMnSc1	Schlierenzauer
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
pořádalo	pořádat	k5eAaImAgNnS	pořádat
dvoje	dvoje	k4xRgFnPc4	dvoje
zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
a	a	k8xC	a
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
v	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
hrách	hra	k1gFnPc6	hra
se	se	k3xPyFc4	se
Rakušanům	Rakušan	k1gMnPc3	Rakušan
nejvíce	nejvíce	k6eAd1	nejvíce
daří	dařit	k5eAaImIp3nS	dařit
v	v	k7c6	v
kanoistice	kanoistika	k1gFnSc6	kanoistika
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc4	dva
zlaté	zlatá	k1gFnPc4	zlatá
má	mít	k5eAaImIp3nS	mít
judista	judista	k1gMnSc1	judista
Peter	Peter	k1gMnSc1	Peter
Seisenbacher	Seisenbachra	k1gFnPc2	Seisenbachra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rakouský	rakouský	k2eAgInSc1d1	rakouský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
reprezentační	reprezentační	k2eAgInSc1d1	reprezentační
tým	tým	k1gInSc1	tým
zažil	zažít	k5eAaPmAgInS	zažít
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
éru	éra	k1gFnSc4	éra
v	v	k7c6	v
50.	[number]	k4	50.
letech	léto	k1gNnPc6	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Rakušané	Rakušan	k1gMnPc1	Rakušan
získali	získat	k5eAaPmAgMnP	získat
bronz	bronz	k1gInSc4	bronz
na	na	k7c4	na
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Oporou	opora	k1gFnSc7	opora
tohoto	tento	k3xDgInSc2	tento
týmu	tým	k1gInSc2	tým
byl	být	k5eAaImAgMnS	být
Gerhard	Gerhard	k1gMnSc1	Gerhard
Hanappi	Hanapp	k1gFnSc2	Hanapp
<g/>
.	.	kIx.	.
</s>
<s>
Rapid	rapid	k1gInSc1	rapid
Vídeň	Vídeň	k1gFnSc1	Vídeň
hrál	hrát	k5eAaImAgInS	hrát
dvakrát	dvakrát	k6eAd1	dvakrát
finále	finále	k1gNnSc7	finále
Poháru	pohár	k1gInSc2	pohár
vítězů	vítěz	k1gMnPc2	vítěz
pohárů	pohár	k1gInPc2	pohár
<g/>
,	,	kIx,	,
Austria	Austrium	k1gNnPc1	Austrium
Vídeň	Vídeň	k1gFnSc1	Vídeň
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
FC	FC	kA	FC
Red	Red	k1gFnSc4	Red
Bull	bulla	k1gFnPc2	bulla
Salzburg	Salzburg	k1gInSc1	Salzburg
jednou	jednou	k6eAd1	jednou
postoupil	postoupit	k5eAaPmAgInS	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
Evropské	evropský	k2eAgFnSc2d1	Evropská
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejslavnějším	slavný	k2eAgMnPc3d3	nejslavnější
rakouským	rakouský	k2eAgMnPc3d1	rakouský
fotbalistům	fotbalista	k1gMnPc3	fotbalista
patří	patřit	k5eAaImIp3nP	patřit
Hans	Hans	k1gMnSc1	Hans
Krankl	Krankl	k1gMnSc1	Krankl
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
Poháru	pohár	k1gInSc2	pohár
vítězů	vítěz	k1gMnPc2	vítěz
s	s	k7c7	s
Barcelonou	Barcelona	k1gFnSc7	Barcelona
<g/>
,	,	kIx,	,
či	či	k8xC	či
Andreas	Andreas	k1gMnSc1	Andreas
Herzog	Herzog	k1gMnSc1	Herzog
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Evropskou	evropský	k2eAgFnSc4d1	Evropská
ligu	liga	k1gFnSc4	liga
s	s	k7c7	s
Bayernem	Bayern	k1gInSc7	Bayern
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
trenérem	trenér	k1gMnSc7	trenér
byl	být	k5eAaImAgMnS	být
Ernst	Ernst	k1gMnSc1	Ernst
Happel	Happel	k1gMnSc1	Happel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přivedl	přivést	k5eAaPmAgMnS	přivést
Nizozemce	Nizozemec	k1gMnPc4	Nizozemec
ke	k	k7c3	k
stříbru	stříbro	k1gNnSc3	stříbro
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
a	a	k8xC	a
Feyenoord	Feyenoord	k1gMnSc1	Feyenoord
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
a	a	k8xC	a
Hamburger	hamburger	k1gInSc1	hamburger
SV	sv	kA	sv
k	k	k7c3	k
triumfu	triumf	k1gInSc3	triumf
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
rakouským	rakouský	k2eAgMnSc7d1	rakouský
tenistou	tenista	k1gMnSc7	tenista
je	být	k5eAaImIp3nS	být
Thomas	Thomas	k1gMnSc1	Thomas
Muster	Muster	k1gMnSc1	Muster
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Roland	Roland	k1gInSc1	Roland
Garros	Garrosa	k1gFnPc2	Garrosa
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
rakouským	rakouský	k2eAgMnSc7d1	rakouský
ledním	lední	k2eAgMnSc7d1	lední
hokejistou	hokejista	k1gMnSc7	hokejista
byl	být	k5eAaImAgMnS	být
Thomas	Thomas	k1gMnSc1	Thomas
Vanek	Vanek	k1gMnSc1	Vanek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
severoamerické	severoamerický	k2eAgFnSc6d1	severoamerická
NHL	NHL	kA	NHL
a	a	k8xC	a
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
rakouský	rakouský	k2eAgMnSc1d1	rakouský
hokejista	hokejista	k1gMnSc1	hokejista
historie	historie	k1gFnSc2	historie
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
sportovcem	sportovec	k1gMnSc7	sportovec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jasna	jasno	k1gNnPc1	jasno
Merdan-Kolarová	Merdan-Kolarový	k2eAgNnPc1d1	Merdan-Kolarový
a	a	k8xC	a
Ausra	Ausra	k1gMnSc1	Ausra
Fridrikasová	Fridrikasová	k1gFnSc1	Fridrikasová
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
a	a	k8xC	a
1999	[number]	k4	1999
vyhlášeny	vyhlásit	k5eAaPmNgInP	vyhlásit
nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
házenkářkami	házenkářka	k1gFnPc7	házenkářka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
extrémními	extrémní	k2eAgInPc7d1	extrémní
kousky	kousek	k1gInPc7	kousek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
skokem	skokem	k6eAd1	skokem
ze	z	k7c2	z
stratosféry	stratosféra	k1gFnSc2	stratosféra
<g/>
,	,	kIx,	,
proslul	proslout	k5eAaPmAgMnS	proslout
Felix	Felix	k1gMnSc1	Felix
Baumgartner	Baumgartner	k1gMnSc1	Baumgartner
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martina	Martina	k1gFnSc1	Martina
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7106-239-4	[number]	k4	978-80-7106-239-4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BISCHOF	BISCHOF	kA	BISCHOF
<g/>
,	,	kIx,	,
Günter	Günter	k1gInSc1	Günter
<g/>
.	.	kIx.	.
</s>
<s>
Rakousko	Rakousko	k1gNnSc4	Rakousko
v	v	k7c6	v
první	první	k4xOgFnSc6	první
studené	studený	k2eAgFnSc6d1	studená
válce	válka	k1gFnSc6	válka
1945-1955	[number]	k4	1945-1955
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-200-2706-1	[number]	k4	978-80-200-2706-1
</s>
</p>
<p>
<s>
Die	Die	k?	Die
wirtschaftliche	wirtschaftlichat	k5eAaPmIp3nS	wirtschaftlichat
Lage	Lage	k1gInSc1	Lage
Österreichs	Österreichs	k1gInSc1	Österreichs
am	am	k?	am
Ende	End	k1gFnSc2	End
des	des	k1gNnSc2	des
ersten	ersten	k2eAgMnSc1d1	ersten
Nachkriegsjahres	Nachkriegsjahres	k1gMnSc1	Nachkriegsjahres
/	/	kIx~	/
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
Rakouska	Rakousko	k1gNnSc2	Rakousko
na	na	k7c6	na
konci	konec	k1gInSc6	konec
prvního	první	k4xOgInSc2	první
poválečného	poválečný	k2eAgInSc2d1	poválečný
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
výroční	výroční	k2eAgFnSc1d1	výroční
zpráva	zpráva	k1gFnSc1	zpráva
31.	[number]	k4	31.
července	červenec	k1gInSc2	červenec
1946	[number]	k4	1946
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Předlitavsko	Předlitavsko	k1gNnSc1	Předlitavsko
</s>
</p>
<p>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rakousko	Rakousko	k1gNnSc4	Rakousko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Rakousko	Rakousko	k1gNnSc4	Rakousko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
Rakousku	Rakousko	k1gNnSc6	Rakousko
</s>
</p>
<p>
<s>
Rakouský	rakouský	k2eAgInSc1d1	rakouský
turistický	turistický	k2eAgInSc1d1	turistický
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
Rakouský	rakouský	k2eAgInSc1d1	rakouský
turismus	turismus	k1gInSc1	turismus
</s>
</p>
<p>
<s>
Rakouský	rakouský	k2eAgInSc1d1	rakouský
lexikon	lexikon	k1gInSc1	lexikon
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc3	jeho
vládě	vláda	k1gFnSc3	vláda
a	a	k8xC	a
parlamentu	parlament	k1gInSc2	parlament
na	na	k7c6	na
úřadu	úřad	k1gInSc6	úřad
spolkového	spolkový	k2eAgMnSc2d1	spolkový
kancléře	kancléř	k1gMnSc2	kancléř
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
služba	služba	k1gFnSc1	služba
ÖAMTC	ÖAMTC	kA	ÖAMTC
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Webové	webový	k2eAgFnSc2d1	webová
stránky	stránka	k1gFnSc2	stránka
Statistik	statistika	k1gFnPc2	statistika
Austria	Austrium	k1gNnSc2	Austrium
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rakousko-německý	rakouskoěmecký	k2eAgInSc1d1	rakousko-německý
slovníček	slovníček	k1gInSc1	slovníček
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
spolkových	spolkový	k2eAgInPc2d1	spolkový
úřadů	úřad	k1gInPc2	úřad
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dunajská	dunajský	k2eAgFnSc1d1	Dunajská
monarchie	monarchie	k1gFnSc1	monarchie
<g/>
,	,	kIx,	,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
<g/>
,	,	kIx,	,
vč	vč	k?	vč
<g/>
.	.	kIx.	.
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
historických	historický	k2eAgFnPc2d1	historická
map	mapa	k1gFnPc2	mapa
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
Rakouské	rakouský	k2eAgFnPc1d1	rakouská
cyklostezky	cyklostezka	k1gFnPc1	cyklostezka
(	(	kIx(	(
<g/>
info	info	k1gNnSc1	info
<g/>
,	,	kIx,	,
foto	foto	k1gNnSc1	foto
<g/>
,	,	kIx,	,
výškový	výškový	k2eAgInSc1d1	výškový
profil	profil	k1gInSc1	profil
<g/>
,	,	kIx,	,
mapy	mapa	k1gFnPc1	mapa
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Austria	Austrium	k1gNnPc1	Austrium
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-21	[number]	k4	2011-08-21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Austria	Austrium	k1gNnPc1	Austrium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-21	[number]	k4	2011-08-21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Austria	Austrium	k1gNnSc2	Austrium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.S.	U.S.	k?	U.S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-05-26	[number]	k4	2011-05-26
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-21	[number]	k4	2011-08-21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Austria	Austrium	k1gNnSc2	Austrium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-21	[number]	k4	2011-08-21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Rakousko	Rakousko	k1gNnSc1	Rakousko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo.cz	Businessinfo.cz	k1gMnSc1	Businessinfo.cz
<g/>
,	,	kIx,	,
2011-05-01	[number]	k4	2011-05-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-21	[number]	k4	2011-08-21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2013-05-12	[number]	k4	2013-05-12
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FELLNER	FELLNER	kA	FELLNER
<g/>
,	,	kIx,	,
Fritz	Fritz	k1gInSc1	Fritz
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Austria	Austrium	k1gNnPc1	Austrium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-21	[number]	k4	2011-08-21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
