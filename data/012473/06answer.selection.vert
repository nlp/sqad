<s>
Hollywoodský	hollywoodský	k2eAgInSc1d1	hollywoodský
chodník	chodník	k1gInSc1	chodník
slávy	sláva	k1gFnSc2	sláva
je	být	k5eAaImIp3nS	být
chodník	chodník	k1gInSc1	chodník
na	na	k7c4	na
Hollywood	Hollywood	k1gInSc4	Hollywood
Boulevard	Boulevarda	k1gFnPc2	Boulevarda
a	a	k8xC	a
Vine	vinout	k5eAaImIp3nS	vinout
Street	Street	k1gInSc4	Street
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zdobí	zdobit	k5eAaImIp3nP	zdobit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2000	[number]	k4	2000
pěticípých	pěticípý	k2eAgFnPc2d1	pěticípá
hvězd	hvězda	k1gFnPc2	hvězda
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
a	a	k8xC	a
příjmeními	příjmení	k1gNnPc7	příjmení
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
