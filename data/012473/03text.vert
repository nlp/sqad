<p>
<s>
Hollywoodský	hollywoodský	k2eAgInSc1d1	hollywoodský
chodník	chodník	k1gInSc1	chodník
slávy	sláva	k1gFnSc2	sláva
je	být	k5eAaImIp3nS	být
chodník	chodník	k1gInSc1	chodník
na	na	k7c4	na
Hollywood	Hollywood	k1gInSc4	Hollywood
Boulevard	Boulevarda	k1gFnPc2	Boulevarda
a	a	k8xC	a
Vine	vinout	k5eAaImIp3nS	vinout
Street	Street	k1gInSc4	Street
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zdobí	zdobit	k5eAaImIp3nP	zdobit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2000	[number]	k4	2000
pěticípých	pěticípý	k2eAgFnPc2d1	pěticípá
hvězd	hvězda	k1gFnPc2	hvězda
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
a	a	k8xC	a
příjmeními	příjmení	k1gNnPc7	příjmení
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdu	hvězda	k1gFnSc4	hvězda
zde	zde	k6eAd1	zde
nemusí	muset	k5eNaImIp3nP	muset
mít	mít	k5eAaImF	mít
pouze	pouze	k6eAd1	pouze
žijící	žijící	k2eAgFnPc4d1	žijící
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
fiktivní	fiktivní	k2eAgFnPc4d1	fiktivní
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
hvězdu	hvězda	k1gFnSc4	hvězda
zde	zde	k6eAd1	zde
položili	položit	k5eAaPmAgMnP	položit
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1960	[number]	k4	1960
pro	pro	k7c4	pro
herečku	herečka	k1gFnSc4	herečka
Joanne	Joann	k1gInSc5	Joann
Woodwardovou	Woodwardův	k2eAgFnSc7d1	Woodwardova
<g/>
.	.	kIx.	.
<g/>
Chodník	chodník	k1gInSc1	chodník
spravuje	spravovat	k5eAaImIp3nS	spravovat
společnost	společnost	k1gFnSc4	společnost
Hollywood	Hollywood	k1gInSc1	Hollywood
Historic	Historic	k1gMnSc1	Historic
Trust	trust	k1gInSc1	trust
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
potřeba	potřeba	k6eAd1	potřeba
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
uspořádání	uspořádání	k1gNnSc3	uspořádání
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
a	a	k8xC	a
zaplacení	zaplacení	k1gNnSc4	zaplacení
25	[number]	k4	25
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
ceremoniálu	ceremoniál	k1gInSc3	ceremoniál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Chodník	chodník	k1gInSc1	chodník
slávy	sláva	k1gFnSc2	sláva
je	být	k5eAaImIp3nS	být
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
5,6	[number]	k4	5,6
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Umístění	umístění	k1gNnSc1	umístění
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
trvalé	trvalý	k2eAgNnSc1d1	trvalé
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
oprav	oprava	k1gFnPc2	oprava
a	a	k8xC	a
technických	technický	k2eAgFnPc2d1	technická
přestaveb	přestavba	k1gFnPc2	přestavba
chodníku	chodník	k1gInSc2	chodník
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
růžového	růžový	k2eAgInSc2d1	růžový
broušeného	broušený	k2eAgInSc2d1	broušený
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
orámovaná	orámovaný	k2eAgFnSc1d1	orámovaná
bronzem	bronz	k1gInSc7	bronz
a	a	k8xC	a
vložená	vložený	k2eAgFnSc1d1	vložená
do	do	k7c2	do
černého	černý	k2eAgInSc2d1	černý
čtverce	čtverec	k1gInSc2	čtverec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hvězdě	hvězda	k1gFnSc6	hvězda
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
bronzu	bronz	k1gInSc2	bronz
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
nápis	nápis	k1gInSc1	nápis
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
honorované	honorovaný	k2eAgFnSc2d1	honorovaná
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
je	být	k5eAaImIp3nS	být
kruhový	kruhový	k2eAgInSc1d1	kruhový
bronzový	bronzový	k2eAgInSc4d1	bronzový
emblém	emblém	k1gInSc4	emblém
<g/>
,	,	kIx,	,
znázorňující	znázorňující	k2eAgFnSc4d1	znázorňující
kategorii	kategorie	k1gFnSc4	kategorie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
osobnost	osobnost	k1gFnSc1	osobnost
svoji	svůj	k3xOyFgFnSc4	svůj
hvězdu	hvězda	k1gFnSc4	hvězda
získala	získat	k5eAaPmAgFnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Chodník	chodník	k1gInSc1	chodník
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
známý	známý	k2eAgInSc1d1	známý
nápis	nápis	k1gInSc1	nápis
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
amerických	americký	k2eAgInPc6d1	americký
filmech	film	k1gInPc6	film
odehrávajících	odehrávající	k2eAgInPc6d1	odehrávající
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
patří	patřit	k5eAaImIp3nS	patřit
romantický	romantický	k2eAgInSc1d1	romantický
film	film	k1gInSc1	film
Pretty	Pretta	k1gFnSc2	Pretta
Woman	Womana	k1gFnPc2	Womana
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ZUNA	zuna	k1gFnSc1	zuna
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
hollywoodský	hollywoodský	k2eAgInSc1d1	hollywoodský
chodník	chodník	k1gInSc1	chodník
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
únor	únor	k1gInSc4	únor
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
film	film	k1gInSc1	film
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Střih	střih	k1gInSc1	střih
<g/>
:	:	kIx,	:
Lucie	Lucie	k1gFnSc1	Lucie
Douchová	Douchová	k1gFnSc1	Douchová
<g/>
.	.	kIx.	.
</s>
<s>
Dramaturg	dramaturg	k1gMnSc1	dramaturg
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Zuna	zuna	k1gFnSc1	zuna
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
<g/>
:	:	kIx,	:
Lucie	Lucie	k1gFnSc1	Lucie
Dřízhalová	Dřízhalová	k1gFnSc1	Dřízhalová
<g/>
.	.	kIx.	.
</s>
<s>
Šéfproducent	šéfproducent	k1gMnSc1	šéfproducent
<g/>
:	:	kIx,	:
Lukáš	Lukáš	k1gMnSc1	Lukáš
Záhoř	Záhoř	k1gMnSc1	Záhoř
<g/>
.	.	kIx.	.
</s>
<s>
Stream	Stream	k1gInSc1	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
chodníků	chodník	k1gInPc2	chodník
slávy	sláva	k1gFnSc2	sláva
</s>
</p>
<p>
<s>
Kanadský	kanadský	k2eAgInSc1d1	kanadský
chodník	chodník	k1gInSc1	chodník
slávy	sláva	k1gFnSc2	sláva
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hollywoodský	hollywoodský	k2eAgInSc4d1	hollywoodský
chodník	chodník	k1gInSc4	chodník
slávy	sláva	k1gFnSc2	sláva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
osobností	osobnost	k1gFnPc2	osobnost
s	s	k7c7	s
umístěními	umístění	k1gNnPc7	umístění
hvězd	hvězda	k1gFnPc2	hvězda
</s>
</p>
<p>
<s>
http://www.answers.com/topic/hollywood-walk-of-fame?cat=entertainment	[url]	k1gMnSc1	http://www.answers.com/topic/hollywood-walk-of-fame?cat=entertainment
</s>
</p>
<p>
<s>
Hollywood	Hollywood	k1gInSc1	Hollywood
Walk	Walk	k1gInSc1	Walk
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
interactive	interactiv	k1gInSc5	interactiv
tour	tour	k1gInSc4	tour
guide	guide	k6eAd1	guide
for	forum	k1gNnPc2	forum
mobile	mobile	k1gNnSc4	mobile
phones	phones	k1gInSc1	phones
-	-	kIx~	-
hwof	hwof	k1gInSc1	hwof
<g/>
.	.	kIx.	.
<g/>
mobi	mobi	k6eAd1	mobi
</s>
</p>
<p>
<s>
Hollywood	Hollywood	k1gInSc1	Hollywood
Walk	Walk	k1gInSc1	Walk
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
interactive	interactiv	k1gInSc5	interactiv
tour	tour	k1gInSc4	tour
guide	guide	k6eAd1	guide
for	forum	k1gNnPc2	forum
PC	PC	kA	PC
-	-	kIx~	-
hwof	hwof	k1gMnSc1	hwof
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Hollywood	Hollywood	k1gInSc1	Hollywood
Walk	Walk	k1gInSc1	Walk
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
at	at	k?	at
Seeing-Stars	Seeing-Stars	k1gInSc1	Seeing-Stars
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
