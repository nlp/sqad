<p>
<s>
Dayton	Dayton	k1gInSc1	Dayton
Bombers	Bombersa	k1gFnPc2	Bombersa
byl	být	k5eAaImAgInS	být
profesionální	profesionální	k2eAgInSc1d1	profesionální
americký	americký	k2eAgInSc1d1	americký
klub	klub	k1gInSc1	klub
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
Daytonu	Dayton	k1gInSc6	Dayton
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
profesionální	profesionální	k2eAgFnSc6d1	profesionální
soutěži	soutěž	k1gFnSc6	soutěž
East	East	k2eAgInSc1d1	East
Coast	Coast	k1gInSc1	Coast
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
Bombers	Bombers	k6eAd1	Bombers
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
poslední	poslední	k2eAgFnSc6d1	poslední
sezóně	sezóna	k1gFnSc6	sezóna
v	v	k7c6	v
ECHL	ECHL	kA	ECHL
skončily	skončit	k5eAaPmAgFnP	skončit
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
odehrával	odehrávat	k5eAaImAgInS	odehrávat
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
Nutter	Nuttra	k1gFnPc2	Nuttra
Center	centrum	k1gNnPc2	centrum
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
9	[number]	k4	9
919	[number]	k4	919
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Klubové	klubový	k2eAgFnPc4d1	klubová
barvy	barva	k1gFnPc4	barva
byly	být	k5eAaImAgInP	být
námořnická	námořnický	k2eAgFnSc1d1	námořnická
modř	modř	k1gFnSc1	modř
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
ligové	ligový	k2eAgFnSc2d1	ligová
účasti	účast	k1gFnSc2	účast
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
:	:	kIx,	:
East	East	k2eAgInSc1d1	East
Coast	Coast	k1gInSc1	Coast
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
(	(	kIx(	(
<g/>
Západní	západní	k2eAgFnSc2d1	západní
divize	divize	k1gFnSc2	divize
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
East	East	k2eAgInSc1d1	East
Coast	Coast	k1gInSc1	Coast
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc2d1	severní
divize	divize	k1gFnSc2	divize
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
East	East	k2eAgInSc1d1	East
Coast	Coast	k1gInSc1	Coast
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
(	(	kIx(	(
<g/>
Severozápadní	severozápadní	k2eAgFnSc2d1	severozápadní
divize	divize	k1gFnSc2	divize
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
East	East	k2eAgInSc1d1	East
Coast	Coast	k1gInSc1	Coast
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc2d1	severní
divize	divize	k1gFnSc2	divize
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
klubu	klub	k1gInSc2	klub
na	na	k7c4	na
hockeydb	hockeydb	k1gInSc4	hockeydb
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
