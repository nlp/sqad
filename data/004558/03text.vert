<s>
De	De	k?	De
facto	facto	k1gNnSc1	facto
je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc4d1	latinský
výraz	výraz	k1gInSc4	výraz
vyjadřující	vyjadřující	k2eAgNnSc1d1	vyjadřující
"	"	kIx"	"
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
protiklad	protiklad	k1gInSc1	protiklad
k	k	k7c3	k
latinskému	latinský	k2eAgNnSc3d1	latinské
de	de	k?	de
iure	iure	k1gNnPc4	iure
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
podle	podle	k7c2	podle
práva	právo	k1gNnSc2	právo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
skutečnost	skutečnost	k1gFnSc1	skutečnost
nebo	nebo	k8xC	nebo
praxe	praxe	k1gFnSc1	praxe
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
regulací	regulace	k1gFnPc2	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Určitá	určitý	k2eAgFnSc1d1	určitá
věc	věc	k1gFnSc1	věc
nebo	nebo	k8xC	nebo
uspořádání	uspořádání	k1gNnSc1	uspořádání
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
může	moct	k5eAaImIp3nS	moct
prosadit	prosadit	k5eAaPmF	prosadit
i	i	k9	i
jako	jako	k9	jako
de	de	k?	de
facto	facto	k1gNnSc1	facto
standard	standard	k1gInSc4	standard
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
praktická	praktický	k2eAgFnSc1d1	praktická
norma	norma	k1gFnSc1	norma
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oficiální	oficiální	k2eAgFnSc1d1	oficiální
norma	norma	k1gFnSc1	norma
(	(	kIx(	(
<g/>
de	de	k?	de
iure	iure	k1gInSc1	iure
<g/>
)	)	kIx)	)
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
užívaná	užívaný	k2eAgFnSc1d1	užívaná
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
jinak	jinak	k6eAd1	jinak
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
normální	normální	k2eAgNnSc1d1	normální
či	či	k8xC	či
očekávané	očekávaný	k2eAgNnSc1d1	očekávané
řídit	řídit	k5eAaImF	řídit
se	se	k3xPyFc4	se
de	de	k?	de
facto	facto	k1gNnSc4	facto
standardem	standard	k1gInSc7	standard
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
de	de	k?	de
facto	facto	k1gNnSc1	facto
standardu	standard	k1gInSc2	standard
je	být	k5eAaImIp3nS	být
série	série	k1gFnSc1	série
dokumentů	dokument	k1gInPc2	dokument
Request	Request	k1gFnSc1	Request
for	forum	k1gNnPc2	forum
Comments	Commentsa	k1gFnPc2	Commentsa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
specifikují	specifikovat	k5eAaBmIp3nP	specifikovat
mnoho	mnoho	k4c4	mnoho
protokolů	protokol	k1gInPc2	protokol
používaných	používaný	k2eAgInPc2d1	používaný
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
–	–	k?	–
např.	např.	kA	např.
široce	široko	k6eAd1	široko
používaný	používaný	k2eAgInSc4d1	používaný
HTTP	HTTP	kA	HTTP
protokol	protokol	k1gInSc4	protokol
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
webových	webový	k2eAgFnPc2d1	webová
stránek	stránka	k1gFnPc2	stránka
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
de	de	k?	de
facto	facto	k1gNnSc1	facto
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
