<s>
Poběda	poběda	k1gFnSc1
(	(	kIx(
<g/>
hodinky	hodinka	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Ukázka	ukázka	k1gFnSc1
modelů	model	k1gInPc2
Poběda	poběda	k1gFnSc1
vyráběných	vyráběný	k2eAgNnPc2d1
před	před	k7c7
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
</s>
<s>
Hodinky	hodinka	k1gFnPc1
Poběda	poběda	k1gFnSc1
1	#num#	k4
MČ3	MČ3	k1gFnPc2
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
poštovní	poštovní	k2eAgFnSc6d1
známce	známka	k1gFnSc6
</s>
<s>
Poběda	poběda	k1gFnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
П	П	k?
–	–	k?
vítězství	vítězství	k1gNnSc4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ruská	ruský	k2eAgFnSc1d1
značka	značka	k1gFnSc1
vyrábějící	vyrábějící	k2eAgFnSc1d1
převážně	převážně	k6eAd1
mechanické	mechanický	k2eAgFnPc4d1
náramkové	náramkový	k2eAgFnPc4d1
hodinky	hodinka	k1gFnPc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
vlastní	vlastnit	k5eAaImIp3nS
hodinářská	hodinářský	k2eAgFnSc1d1
továrna	továrna	k1gFnSc1
Petrodvorec	Petrodvorec	k1gInSc1
–	–	k?
Raketa	raketa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
Poběda	poběda	k1gFnSc1
si	se	k3xPyFc3
osobně	osobně	k6eAd1
zvolil	zvolit	k5eAaPmAgMnS
Josef	Josef	k1gMnSc1
Stalin	Stalin	k1gMnSc1
v	v	k7c6
dubnu	duben	k1gInSc6
1945	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
dal	dát	k5eAaPmAgInS
také	také	k9
příkaz	příkaz	k1gInSc1
<g/>
,	,	kIx,
aby	aby	k9
první	první	k4xOgFnPc1
hodinky	hodinka	k1gFnPc1
byly	být	k5eAaImAgFnP
připraveny	připravit	k5eAaPmNgFnP
k	k	k7c3
masové	masový	k2eAgFnSc3d1
výrobě	výroba	k1gFnSc3
k	k	k7c3
prvnímu	první	k4xOgNnSc3
výročí	výročí	k1gNnSc3
vítězství	vítězství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
hodinky	hodinka	k1gFnPc4
vzešly	vzejít	k5eAaPmAgFnP
z	z	k7c2
továrny	továrna	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
Penza	penzum	k1gNnSc2
ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
1945	#num#	k4
a	a	k8xC
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
vyšly	vyjít	k5eAaPmAgInP
první	první	k4xOgInPc1
modely	model	k1gInPc1
z	z	k7c2
moskevské	moskevský	k2eAgFnSc2d1
První	první	k4xOgFnSc2
hodinářské	hodinářský	k2eAgFnSc2d1
továrny	továrna	k1gFnSc2
A.	A.	kA
S.	S.	kA
Kirova	Kirov	k1gInSc2
v	v	k7c6
březnu	březen	k1gInSc6
roku	rok	k1gInSc2
1946	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Hodinky	hodinka	k1gFnPc1
Poběda	poběda	k1gFnSc1
jsou	být	k5eAaImIp3nP
založeny	založit	k5eAaPmNgInP
na	na	k7c6
jednoduchém	jednoduchý	k2eAgInSc6d1
francouzském	francouzský	k2eAgInSc6d1
návrhu	návrh	k1gInSc6
(	(	kIx(
<g/>
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
francouzskou	francouzský	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
LIP	lípa	k1gFnPc2
Besancon	Besancona	k1gFnPc2
<g/>
)	)	kIx)
strojku	strojek	k1gInSc2
LIP	lípa	k1gFnPc2
K-26	K-26	k1gFnSc2
o	o	k7c6
průměru	průměr	k1gInSc6
25	#num#	k4
mm	mm	kA
s	s	k7c7
15	#num#	k4
až	až	k8xS
17	#num#	k4
kameny	kámen	k1gInPc7
z	z	k7c2
roku	rok	k1gInSc2
1937	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byly	být	k5eAaImAgFnP
standardní	standardní	k2eAgFnPc4d1
hodinky	hodinka	k1gFnPc4
francouzské	francouzský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
přesné	přesný	k2eAgInPc1d1
<g/>
,	,	kIx,
spolehlivé	spolehlivý	k2eAgInPc1d1
<g/>
,	,	kIx,
snadno	snadno	k6eAd1
vyrobitelné	vyrobitelný	k2eAgFnPc1d1
a	a	k8xC
opravitelné	opravitelný	k2eAgFnPc1d1
a	a	k8xC
mají	mít	k5eAaImIp3nP
dlouhou	dlouhý	k2eAgFnSc4d1
životnost	životnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
modely	model	k1gInPc1
s	s	k7c7
vteřinovou	vteřinový	k2eAgFnSc7d1
ručičkou	ručička	k1gFnSc7
uprostřed	uprostřed	k7c2
(	(	kIx(
<g/>
"	"	kIx"
<g/>
centrální	centrální	k2eAgFnSc1d1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dole	dole	k6eAd1
na	na	k7c6
ciferníku	ciferník	k1gInSc6
nebo	nebo	k8xC
bez	bez	k7c2
ní	on	k3xPp3gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
byly	být	k5eAaImAgFnP
hodinky	hodinka	k1gFnPc1
Poběda	poběda	k1gFnSc1
vyráběny	vyráběn	k2eAgFnPc1d1
na	na	k7c6
šesti	šest	k4xCc6
místech	místo	k1gNnPc6
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
/	/	kIx~
<g/>
Ruska	Rusko	k1gNnSc2
v	v	k7c6
milionových	milionový	k2eAgFnPc6d1
sériích	série	k1gFnPc6
a	a	k8xC
téměř	téměř	k6eAd1
100	#num#	k4
různých	různý	k2eAgFnPc6d1
modifikacích	modifikace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
se	se	k3xPyFc4
vyrábějí	vyrábět	k5eAaImIp3nP
pouze	pouze	k6eAd1
v	v	k7c6
Petrohradu	Petrohrad	k1gInSc3
v	v	k7c6
továrně	továrna	k1gFnSc6
Petrodvorec	Petrodvorec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://ussrlife.blogspot.ru/2012/11/blog-post_21.html	http://ussrlife.blogspot.ru/2012/11/blog-post_21.html	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Raketa	raketa	k1gFnSc1
(	(	kIx(
<g/>
hodinky	hodinka	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
https://web.archive.org/web/20150710052421/http://pobeda1945.com/	https://web.archive.org/web/20150710052421/http://pobeda1945.com/	k4
</s>
