<s>
V	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
je	být	k5eAaImIp3nS	být
heliocentrismus	heliocentrismus	k1gInSc4	heliocentrismus
modelem	model	k1gInSc7	model
<g/>
,	,	kIx,	,
stavícím	stavící	k2eAgInSc7d1	stavící
Slunce	slunce	k1gNnSc4	slunce
do	do	k7c2	do
středu	střed	k1gInSc2	střed
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
celého	celý	k2eAgInSc2d1	celý
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
(	(	kIx(	(
<g/>
ἥ	ἥ	k?	ἥ
hélios	hélios	k1gInSc1	hélios
=	=	kIx~	=
"	"	kIx"	"
<g/>
Slunce	slunce	k1gNnSc1	slunce
<g/>
"	"	kIx"	"
a	a	k8xC	a
κ	κ	k?	κ
kentron	kentron	k1gInSc1	kentron
=	=	kIx~	=
"	"	kIx"	"
<g/>
střed	střed	k1gInSc4	střed
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
heliocentrismus	heliocentrismus	k1gInSc1	heliocentrismus
protikladem	protiklad	k1gInSc7	protiklad
geocentrismu	geocentrismus	k1gInSc2	geocentrismus
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
modernímu	moderní	k2eAgInSc3d1	moderní
geocentrismu	geocentrismus	k1gInSc3	geocentrismus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
kladou	klást	k5eAaImIp3nP	klást
jako	jako	k9	jako
centrum	centrum	k1gNnSc4	centrum
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
Slunce	slunce	k1gNnSc2	slunce
jako	jako	k8xC	jako
středu	střed	k1gInSc2	střed
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
již	již	k6eAd1	již
v	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
myšlenku	myšlenka	k1gFnSc4	myšlenka
Slunce	slunce	k1gNnSc2	slunce
jako	jako	k8xS	jako
středu	středa	k1gFnSc4	středa
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
znovu	znovu	k6eAd1	znovu
zavádí	zavádět	k5eAaImIp3nS	zavádět
polský	polský	k2eAgMnSc1d1	polský
astronom	astronom	k1gMnSc1	astronom
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Koperník	Koperník	k1gMnSc1	Koperník
(	(	kIx(	(
<g/>
1473	[number]	k4	1473
<g/>
-	-	kIx~	-
<g/>
1543	[number]	k4	1543
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazují	navazovat	k5eAaImIp3nP	navazovat
další	další	k2eAgMnPc1d1	další
jako	jako	k8xC	jako
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnSc7	Galilei
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
století	století	k1gNnSc2	století
musel	muset	k5eAaImAgInS	muset
heliocentrismus	heliocentrismus	k1gInSc1	heliocentrismus
čelit	čelit	k5eAaImF	čelit
zdánlivě	zdánlivě	k6eAd1	zdánlivě
samozřejmému	samozřejmý	k2eAgInSc3d1	samozřejmý
obecnému	obecný	k2eAgInSc3d1	obecný
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
otáčela	otáčet	k5eAaImAgFnS	otáčet
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
předměty	předmět	k1gInPc1	předmět
by	by	kYmCp3nP	by
musely	muset	k5eAaImAgInP	muset
spadnout	spadnout	k5eAaPmF	spadnout
<g/>
,	,	kIx,	,
dalším	další	k2eAgInPc3d1	další
argumentům	argument	k1gInPc3	argument
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
meteorologie	meteorologie	k1gFnSc2	meteorologie
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k6eAd1	také
nesouladu	nesoulad	k1gInSc2	nesoulad
se	s	k7c7	s
soudobými	soudobý	k2eAgFnPc7d1	soudobá
filosofickými	filosofický	k2eAgFnPc7d1	filosofická
či	či	k8xC	či
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
představami	představa	k1gFnPc7	představa
<g/>
.	.	kIx.	.
</s>
<s>
Komukoliv	kdokoliv	k3yInSc3	kdokoliv
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
a	a	k8xC	a
pohlédne	pohlédnout	k5eAaPmIp3nS	pohlédnout
na	na	k7c4	na
oblohu	obloha	k1gFnSc4	obloha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
země	země	k1gFnSc1	země
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
všechno	všechen	k3xTgNnSc1	všechen
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
ubíhá	ubíhat	k5eAaImIp3nS	ubíhat
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
delším	dlouhý	k2eAgNnSc6d2	delší
pozorování	pozorování	k1gNnSc6	pozorování
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
mnohem	mnohem	k6eAd1	mnohem
komplikovanější	komplikovaný	k2eAgInPc4d2	komplikovanější
pohyby	pohyb	k1gInPc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
dělá	dělat	k5eAaImIp3nS	dělat
menší	malý	k2eAgInSc4d2	menší
či	či	k8xC	či
větší	veliký	k2eAgInSc4d2	veliký
okruh	okruh	k1gInSc4	okruh
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
roční	roční	k2eAgFnSc6d1	roční
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
planety	planeta	k1gFnPc1	planeta
mají	mít	k5eAaImIp3nP	mít
podobné	podobný	k2eAgInPc4d1	podobný
pohyby	pohyb	k1gInPc4	pohyb
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
mezi	mezi	k7c7	mezi
hvězdami	hvězda	k1gFnPc7	hvězda
obrátí	obrátit	k5eAaPmIp3nP	obrátit
a	a	k8xC	a
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
(	(	kIx(	(
<g/>
retrográdní	retrográdní	k2eAgInSc1d1	retrográdní
pohyb	pohyb	k1gInSc1	pohyb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
pochopení	pochopení	k1gNnSc4	pochopení
těchto	tento	k3xDgInPc2	tento
pohybů	pohyb	k1gInPc2	pohyb
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
zavádět	zavádět	k5eAaImF	zavádět
komplikované	komplikovaný	k2eAgInPc4d1	komplikovaný
popisy	popis	k1gInPc4	popis
<g/>
,	,	kIx,	,
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
Ptolemaiovský	Ptolemaiovský	k2eAgInSc1d1	Ptolemaiovský
systém	systém	k1gInSc1	systém
formulovaný	formulovaný	k2eAgInSc1d1	formulovaný
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Neintuitivní	intuitivní	k2eNgFnSc1d1	neintuitivní
myšlenka	myšlenka	k1gFnSc1	myšlenka
heliocentrismu	heliocentrismus	k1gInSc2	heliocentrismus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
nebesa	nebesa	k1gNnPc1	nebesa
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
dokonce	dokonce	k9	dokonce
již	již	k6eAd1	již
v	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
13	[number]	k4	13
druhé	druhý	k4xOgFnSc2	druhý
knihy	kniha	k1gFnSc2	kniha
svého	svůj	k3xOyFgInSc2	svůj
spisu	spis	k1gInSc2	spis
O	o	k7c6	o
nebesích	nebesa	k1gNnPc6	nebesa
napsal	napsat	k5eAaBmAgInS	napsat
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
"	"	kIx"	"
<g/>
Jak	jak	k8xC	jak
tvrdí	tvrdý	k2eAgMnPc1d1	tvrdý
pythagorejci	pythagorejec	k1gMnPc1	pythagorejec
<g/>
,	,	kIx,	,
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
je	být	k5eAaImIp3nS	být
oheň	oheň	k1gInSc1	oheň
a	a	k8xC	a
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgFnSc4d1	vytvářející
noc	noc	k1gFnSc4	noc
a	a	k8xC	a
den	den	k1gInSc4	den
krouživým	krouživý	k2eAgInSc7d1	krouživý
pohybem	pohyb	k1gInSc7	pohyb
kolem	kolem	k7c2	kolem
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Důvody	důvod	k1gInPc1	důvod
tohoto	tento	k3xDgNnSc2	tento
umístění	umístění	k1gNnSc2	umístění
byly	být	k5eAaImAgFnP	být
spíše	spíše	k9	spíše
filosofické	filosofický	k2eAgFnPc1d1	filosofická
než	než	k8xS	než
vědecké	vědecký	k2eAgInPc1d1	vědecký
–	–	k?	–
pro	pro	k7c4	pro
pythagorejskou	pythagorejský	k2eAgFnSc4d1	pythagorejská
filosofii	filosofie	k1gFnSc4	filosofie
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
základních	základní	k2eAgInPc6d1	základní
živlech	živel	k1gInPc6	živel
byl	být	k5eAaImAgInS	být
oheň	oheň	k1gInSc1	oheň
živlem	živel	k1gInSc7	živel
ušlechtilejším	ušlechtilý	k2eAgInSc7d2	ušlechtilejší
než	než	k8xS	než
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
Slunce	slunce	k1gNnSc1	slunce
(	(	kIx(	(
<g/>
reprezentující	reprezentující	k2eAgInSc1d1	reprezentující
oheň	oheň	k1gInSc1	oheň
<g/>
)	)	kIx)	)
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
tento	tento	k3xDgInSc4	tento
argument	argument	k1gInSc4	argument
a	a	k8xC	a
hlásal	hlásat	k5eAaImAgInS	hlásat
geocentrismus	geocentrismus	k1gInSc1	geocentrismus
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
heliocentrismus	heliocentrismus	k1gInSc1	heliocentrismus
opět	opět	k6eAd1	opět
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Aristarchos	Aristarchos	k1gInSc1	Aristarchos
ze	z	k7c2	z
Samu	Samos	k1gInSc2	Samos
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
270	[number]	k4	270
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
psal	psát	k5eAaImAgInS	psát
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
velikost	velikost	k1gFnSc1	velikost
Země	zem	k1gFnSc2	zem
poměrně	poměrně	k6eAd1	poměrně
přesně	přesně	k6eAd1	přesně
spočítána	spočítán	k2eAgFnSc1d1	spočítána
<g/>
,	,	kIx,	,
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
změřil	změřit	k5eAaPmAgInS	změřit
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
Slunce	slunce	k1gNnSc2	slunce
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gInPc1	jeho
odhady	odhad	k1gInPc1	odhad
nebyly	být	k5eNaImAgInP	být
podle	podle	k7c2	podle
současných	současný	k2eAgInPc2d1	současný
standardů	standard	k1gInPc2	standard
moc	moc	k6eAd1	moc
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
seriózní	seriózní	k2eAgInSc4d1	seriózní
začátek	začátek	k1gInSc4	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
mnozí	mnohý	k2eAgMnPc1d1	mnohý
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
usoudil	usoudit	k5eAaPmAgMnS	usoudit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
měl	mít	k5eAaImAgInS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohyb	pohyb	k1gInSc1	pohyb
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
obrovského	obrovský	k2eAgNnSc2d1	obrovské
Slunce	slunce	k1gNnSc2	slunce
dává	dávat	k5eAaImIp3nS	dávat
větší	veliký	k2eAgInSc4d2	veliký
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
obráceně	obráceně	k6eAd1	obráceně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
vlastní	vlastní	k2eAgFnSc6d1	vlastní
době	doba	k1gFnSc6	doba
však	však	k9	však
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
soudili	soudit	k5eAaImAgMnP	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
je	být	k5eAaImIp3nS	být
protináboženská	protináboženský	k2eAgFnSc1d1	protináboženská
<g/>
.	.	kIx.	.
</s>
<s>
Aristarchově	aristarchův	k2eAgFnSc3d1	aristarchův
heliocentrickému	heliocentrický	k2eAgInSc3d1	heliocentrický
modelu	model	k1gInSc2	model
oponoval	oponovat	k5eAaImAgMnS	oponovat
Archimédés	Archimédés	k1gInSc4	Archimédés
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
O	o	k7c6	o
počtu	počet	k1gInSc6	počet
písečných	písečný	k2eAgNnPc2d1	písečné
zrn	zrno	k1gNnPc2	zrno
prohlašoval	prohlašovat	k5eAaImAgInS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc1	svět
(	(	kIx(	(
<g/>
vesmír	vesmír	k1gInSc1	vesmír
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
konečný	konečný	k2eAgInSc1d1	konečný
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
množiny	množina	k1gFnSc2	množina
prvotních	prvotní	k2eAgInPc2d1	prvotní
předpokladů	předpoklad	k1gInPc2	předpoklad
spočítal	spočítat	k5eAaPmAgMnS	spočítat
horní	horní	k2eAgFnSc4d1	horní
mez	mez	k1gFnSc4	mez
poloměru	poloměr	k1gInSc2	poloměr
vesmíru	vesmír	k1gInSc2	vesmír
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
000	[number]	k4	000
000	[number]	k4	000
stadií	stadion	k1gNnPc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tohoto	tento	k3xDgNnSc2	tento
čísla	číslo	k1gNnSc2	číslo
pro	pro	k7c4	pro
velikost	velikost	k1gFnSc4	velikost
vesmíru	vesmír	k1gInSc2	vesmír
vyvozoval	vyvozovat	k5eAaImAgInS	vyvozovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
maximální	maximální	k2eAgFnSc4d1	maximální
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yIgFnSc6	jaký
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
hvězdy	hvězda	k1gFnPc4	hvězda
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
heliocentrickém	heliocentrický	k2eAgInSc6d1	heliocentrický
systému	systém	k1gInSc6	systém
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
ocitá	ocitat	k5eAaImIp3nS	ocitat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
vzdálených	vzdálený	k2eAgNnPc6d1	vzdálené
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
dvojnásobek	dvojnásobek	k1gInSc1	dvojnásobek
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
Země-Slunce	Země-Slunka	k1gFnSc3	Země-Slunka
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
hvězd	hvězda	k1gFnPc2	hvězda
by	by	kYmCp3nP	by
zde	zde	k6eAd1	zde
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
viditelná	viditelný	k2eAgFnSc1d1	viditelná
paralaxa	paralaxa	k1gFnSc1	paralaxa
umožňující	umožňující	k2eAgNnSc1d1	umožňující
rozlišit	rozlišit	k5eAaPmF	rozlišit
bližší	blízký	k2eAgFnPc4d2	bližší
a	a	k8xC	a
vzdálenější	vzdálený	k2eAgFnPc4d2	vzdálenější
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádná	žádný	k3yNgFnSc1	žádný
paralaxa	paralaxa	k1gFnSc1	paralaxa
není	být	k5eNaImIp3nS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
<g/>
,	,	kIx,	,
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Archimédes	Archimédes	k1gMnSc1	Archimédes
heliocentrismus	heliocentrismus	k1gInSc4	heliocentrismus
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
odsunout	odsunout	k5eAaPmF	odsunout
hvězdy	hvězda	k1gFnPc4	hvězda
do	do	k7c2	do
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Archiméda	Archimédes	k1gMnSc4	Archimédes
<g/>
)	)	kIx)	)
nepřijatelné	přijatelný	k2eNgFnSc2d1	nepřijatelná
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
indický	indický	k2eAgMnSc1d1	indický
astronom	astronom	k1gMnSc1	astronom
Arjabhata	Arjabhat	k1gMnSc2	Arjabhat
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
Aristarchovi	aristarch	k1gMnSc6	aristarch
<g/>
)	)	kIx)	)
také	také	k9	také
navrhoval	navrhovat	k5eAaImAgInS	navrhovat
heliocentrický	heliocentrický	k2eAgInSc1d1	heliocentrický
vesmír	vesmír	k1gInSc1	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
nebyla	být	k5eNaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
latiny	latina	k1gFnSc2	latina
až	až	k9	až
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Koperník	Koperník	k1gMnSc1	Koperník
napsal	napsat	k5eAaBmAgMnS	napsat
De	De	k?	De
revolutionibus	revolutionibus	k1gMnSc1	revolutionibus
orbium	orbium	k1gNnSc1	orbium
coelestium	coelestium	k1gNnSc1	coelestium
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
zůstala	zůstat	k5eAaPmAgFnS	zůstat
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
prakticky	prakticky	k6eAd1	prakticky
neznámá	známý	k2eNgFnSc1d1	neznámá
až	až	k9	až
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
byly	být	k5eAaImAgInP	být
přeloženy	přeložit	k5eAaPmNgInP	přeložit
do	do	k7c2	do
latiny	latina	k1gFnSc2	latina
některé	některý	k3yIgFnPc1	některý
staré	starý	k2eAgFnPc1d1	stará
arabské	arabský	k2eAgFnPc1d1	arabská
astrologické	astrologický	k2eAgFnPc1d1	astrologická
a	a	k8xC	a
astronomické	astronomický	k2eAgFnPc1d1	astronomická
spisy	spis	k1gInPc4	spis
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jeho	jeho	k3xOp3gFnPc1	jeho
teorie	teorie	k1gFnPc1	teorie
komentovaly	komentovat	k5eAaBmAgFnP	komentovat
a	a	k8xC	a
rozpracovávaly	rozpracovávat	k5eAaImAgFnP	rozpracovávat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Aliboron	Aliboron	k1gInSc1	Aliboron
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Indické	indický	k2eAgFnSc2d1	indická
kroniky	kronika	k1gFnSc2	kronika
<g/>
"	"	kIx"	"
Ta	ten	k3xDgFnSc1	ten
<g/>
'	'	kIx"	'
<g/>
rikh	rikh	k1gMnSc1	rikh
al-Hind	al-Hind	k1gMnSc1	al-Hind
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Egyptský	egyptský	k2eAgMnSc1d1	egyptský
(	(	kIx(	(
<g/>
možná	možná	k9	možná
napůl	napůl	k6eAd1	napůl
mýtický	mýtický	k2eAgMnSc1d1	mýtický
<g/>
)	)	kIx)	)
filosof	filosof	k1gMnSc1	filosof
Hermés	Hermésa	k1gFnPc2	Hermésa
Trismegistos	Trismegistos	k1gMnSc1	Trismegistos
také	také	k9	také
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
heliocentrický	heliocentrický	k2eAgInSc4d1	heliocentrický
náhled	náhled	k1gInSc4	náhled
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
povšimnutí	povšimnutí	k1gNnSc4	povšimnutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
Koperník	Koperník	k1gMnSc1	Koperník
dokonce	dokonce	k9	dokonce
ve	v	k7c6	v
svém	své	k1gNnSc6	své
De	De	k?	De
revolutionibus	revolutionibus	k1gInSc1	revolutionibus
orbium	orbium	k1gNnSc1	orbium
coelestium	coelestium	k1gNnSc4	coelestium
poděkoval	poděkovat	k5eAaPmAgMnS	poděkovat
za	za	k7c4	za
inspiraci	inspirace	k1gFnSc4	inspirace
Trismegistovými	Trismegistův	k2eAgFnPc7d1	Trismegistův
myšlenkami	myšlenka	k1gFnPc7	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
datace	datace	k1gFnSc1	datace
není	být	k5eNaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Trismegistos	Trismegistos	k1gMnSc1	Trismegistos
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
žil	žít	k5eAaImAgMnS	žít
ve	v	k7c6	v
faraónském	faraónský	k2eAgInSc6d1	faraónský
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
skutečné	skutečný	k2eAgInPc1d1	skutečný
texty	text	k1gInPc1	text
dokládající	dokládající	k2eAgFnSc4d1	dokládající
hermetickou	hermetický	k2eAgFnSc4d1	hermetická
tradici	tradice	k1gFnSc4	tradice
lze	lze	k6eAd1	lze
datovat	datovat	k5eAaImF	datovat
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
časů	čas	k1gInPc2	čas
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
arabsky	arabsky	k6eAd1	arabsky
psaných	psaný	k2eAgInPc6d1	psaný
spisech	spis	k1gInPc6	spis
významný	významný	k2eAgMnSc1d1	významný
perský	perský	k2eAgMnSc1d1	perský
astrolog	astrolog	k1gMnSc1	astrolog
Muhammad	Muhammad	k1gInSc4	Muhammad
Abú	abú	k1gMnSc3	abú
ar	ar	k1gInSc4	ar
Rajhán	Rajhán	k2eAgMnSc1d1	Rajhán
al-Birúní	al-Birúní	k1gMnSc1	al-Birúní
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
latinizovaným	latinizovaný	k2eAgNnSc7d1	latinizované
jménem	jméno	k1gNnSc7	jméno
Aliboron	Aliboron	k1gInSc1	Aliboron
<g/>
,	,	kIx,	,
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
rotuje	rotovat	k5eAaImIp3nS	rotovat
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
přitom	přitom	k6eAd1	přitom
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
důkladné	důkladný	k2eAgFnSc2d1	důkladná
znalosti	znalost	k1gFnSc2	znalost
Aristotelova	Aristotelův	k2eAgNnSc2d1	Aristotelovo
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
překladů	překlad	k1gInPc2	překlad
klasických	klasický	k2eAgInPc2d1	klasický
astrologických	astrologický	k2eAgInPc2d1	astrologický
a	a	k8xC	a
hermetických	hermetický	k2eAgInPc2d1	hermetický
spisů	spis	k1gInPc2	spis
do	do	k7c2	do
arabštiny	arabština	k1gFnSc2	arabština
i	i	k8xC	i
důkladné	důkladný	k2eAgFnSc2d1	důkladná
znalosti	znalost	k1gFnSc2	znalost
tradic	tradice	k1gFnPc2	tradice
indických	indický	k2eAgMnPc2d1	indický
astrologů	astrolog	k1gMnPc2	astrolog
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
spisy	spis	k1gInPc1	spis
<g/>
,	,	kIx,	,
a	a	k8xC	a
díla	dílo	k1gNnSc2	dílo
jeho	jeho	k3xOp3gMnPc2	jeho
arabských	arabský	k2eAgMnPc2d1	arabský
následovníků	následovník	k1gMnPc2	následovník
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
Francii	Francie	k1gFnSc4	Francie
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
pozdně	pozdně	k6eAd1	pozdně
středověký	středověký	k2eAgMnSc1d1	středověký
scholastický	scholastický	k2eAgMnSc1d1	scholastický
teolog	teolog	k1gMnSc1	teolog
a	a	k8xC	a
myslitel	myslitel	k1gMnSc1	myslitel
biskup	biskup	k1gMnSc1	biskup
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Oresme	Oresme	k1gMnSc1	Oresme
zastával	zastávat	k5eAaImAgMnS	zastávat
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
heliocentrický	heliocentrický	k2eAgInSc1d1	heliocentrický
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
hypotéza	hypotéza	k1gFnSc1	hypotéza
jako	jako	k8xC	jako
geocentrický	geocentrický	k2eAgInSc1d1	geocentrický
<g/>
.	.	kIx.	.
</s>
<s>
Odmítl	odmítnout	k5eAaPmAgInS	odmítnout
argument	argument	k1gInSc1	argument
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
rotující	rotující	k2eAgFnSc1d1	rotující
Země	země	k1gFnSc1	země
způsobovala	způsobovat	k5eAaImAgFnS	způsobovat
nutně	nutně	k6eAd1	nutně
katastrofální	katastrofální	k2eAgFnSc1d1	katastrofální
větry	vítr	k1gInPc7	vítr
a	a	k8xC	a
také	také	k9	také
připravil	připravit	k5eAaPmAgMnS	připravit
myšlenku	myšlenka	k1gFnSc4	myšlenka
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
hybnosti	hybnost	k1gFnSc2	hybnost
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
spisy	spis	k1gInPc1	spis
vycházely	vycházet	k5eAaImAgInP	vycházet
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
tiskem	tisk	k1gInSc7	tisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
teorii	teorie	k1gFnSc3	teorie
oživil	oživit	k5eAaPmAgMnS	oživit
polský	polský	k2eAgMnSc1d1	polský
katolický	katolický	k2eAgMnSc1d1	katolický
duchovní	duchovní	k1gMnSc1	duchovní
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Koperník	Koperník	k1gMnSc1	Koperník
<g/>
,	,	kIx,	,
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
dílem	dílem	k6eAd1	dílem
Šest	šest	k4xCc1	šest
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
obězích	oběh	k1gInPc6	oběh
nebeských	nebeský	k2eAgFnPc2d1	nebeská
sfér	sféra	k1gFnPc2	sféra
(	(	kIx(	(
<g/>
De	De	k?	De
revolutionibu	revolutionibat	k5eAaPmIp1nS	revolutionibat
orbium	orbium	k1gNnSc4	orbium
coelestium	coelestium	k1gNnSc1	coelestium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
publikoval	publikovat	k5eAaBmAgMnS	publikovat
roku	rok	k1gInSc2	rok
1543	[number]	k4	1543
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
svých	svůj	k3xOyFgFnPc2	svůj
teorií	teorie	k1gFnPc2	teorie
planetárního	planetární	k2eAgInSc2d1	planetární
pohybu	pohyb	k1gInSc2	pohyb
Koperníka	Koperník	k1gMnSc2	Koperník
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
dřívější	dřívější	k2eAgFnPc1d1	dřívější
práce	práce	k1gFnPc1	práce
arabského	arabský	k2eAgMnSc2d1	arabský
astronoma	astronom	k1gMnSc2	astronom
Ibn	Ibn	k1gMnSc2	Ibn
al-Šatira	al-Šatir	k1gMnSc2	al-Šatir
a	a	k8xC	a
perského	perský	k2eAgMnSc2d1	perský
astronoma	astronom	k1gMnSc2	astronom
Nasir	Nasir	k1gMnSc1	Nasir
al-Din	al-Din	k1gMnSc1	al-Din
al-Tusiho	al-Tusize	k6eAd1	al-Tusize
<g/>
.	.	kIx.	.
</s>
<s>
Koperníkovu	Koperníkův	k2eAgFnSc4d1	Koperníkova
knihu	kniha	k1gFnSc4	kniha
doplnil	doplnit	k5eAaPmAgMnS	doplnit
jeho	jeho	k3xOp3gFnSc4	jeho
vydavatel	vydavatel	k1gMnSc1	vydavatel
předmluvou	předmluva	k1gFnSc7	předmluva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
heliocentrismus	heliocentrismus	k1gInSc4	heliocentrismus
popsala	popsat	k5eAaPmAgFnS	popsat
jako	jako	k8xC	jako
pouhou	pouhý	k2eAgFnSc4d1	pouhá
matematickou	matematický	k2eAgFnSc4d1	matematická
teorii	teorie	k1gFnSc4	teorie
a	a	k8xC	a
tak	tak	k6eAd1	tak
chránil	chránit	k5eAaImAgMnS	chránit
autora	autor	k1gMnSc4	autor
před	před	k7c7	před
případným	případný	k2eAgNnSc7d1	případné
obviněním	obvinění	k1gNnSc7	obvinění
z	z	k7c2	z
rouhání	rouhání	k1gNnSc2	rouhání
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
stejně	stejně	k6eAd1	stejně
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c4	na
katolický	katolický	k2eAgInSc4d1	katolický
index	index	k1gInSc4	index
zakázané	zakázaný	k2eAgFnSc2d1	zakázaná
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
figurovala	figurovat	k5eAaImAgFnS	figurovat
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1616	[number]	k4	1616
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
pokračovatelem	pokračovatel	k1gMnSc7	pokračovatel
a	a	k8xC	a
učencem	učenec	k1gMnSc7	učenec
prohlubující	prohlubující	k2eAgFnSc4d1	prohlubující
myšlenku	myšlenka	k1gFnSc4	myšlenka
heliocentrismu	heliocentrismus	k1gInSc2	heliocentrismus
byl	být	k5eAaImAgInS	být
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnSc6	Galile
(	(	kIx(	(
<g/>
1564	[number]	k4	1564
<g/>
-	-	kIx~	-
<g/>
1642	[number]	k4	1642
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc4	jeho
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
objevy	objev	k1gInPc4	objev
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
jezuitští	jezuitský	k2eAgMnPc1d1	jezuitský
astronomové	astronom	k1gMnPc1	astronom
jako	jako	k8xC	jako
páter	páter	k1gMnSc1	páter
Grienberger	Grienberger	k1gMnSc1	Grienberger
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
osobně	osobně	k6eAd1	osobně
ověřil	ověřit	k5eAaPmAgMnS	ověřit
Galileiho	Galilei	k1gMnSc2	Galilei
objev	objev	k1gInSc1	objev
měsíců	měsíc	k1gInPc2	měsíc
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgMnPc1d1	církevní
učenci	učenec	k1gMnPc1	učenec
tuto	tento	k3xDgFnSc4	tento
myšlenku	myšlenka	k1gFnSc4	myšlenka
dále	daleko	k6eAd2	daleko
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
ji	on	k3xPp3gFnSc4	on
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
spolehlivější	spolehlivý	k2eAgFnSc4d2	spolehlivější
než	než	k8xS	než
kterýkoliv	kterýkoliv	k3yIgMnSc1	kterýkoliv
jiný	jiný	k1gMnSc1	jiný
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
známý	známý	k2eAgInSc4d1	známý
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
pouhou	pouhý	k2eAgFnSc4d1	pouhá
matematickou	matematický	k2eAgFnSc4d1	matematická
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
filosofickou	filosofický	k2eAgFnSc4d1	filosofická
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
Země	země	k1gFnSc1	země
reálně	reálně	k6eAd1	reálně
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1616	[number]	k4	1616
až	až	k9	až
1633	[number]	k4	1633
byly	být	k5eAaImAgFnP	být
církví	církev	k1gFnPc2	církev
ustanoveny	ustanoven	k2eAgInPc1d1	ustanoven
dekrety	dekret	k1gInPc1	dekret
(	(	kIx(	(
<g/>
1616	[number]	k4	1616
dekret	dekret	k1gInSc4	dekret
Svatého	svatý	k2eAgNnSc2d1	svaté
oficia	oficium	k1gNnSc2	oficium
<g/>
)	)	kIx)	)
a	a	k8xC	a
Galileo	Galilea	k1gFnSc5	Galilea
byl	být	k5eAaImAgInS	být
upozorněn	upozornit	k5eAaPmNgMnS	upozornit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svou	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
neprosazoval	prosazovat	k5eNaImAgMnS	prosazovat
jako	jako	k9	jako
absolutní	absolutní	k2eAgFnSc4d1	absolutní
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
potřeba	potřeba	k6eAd1	potřeba
zkoumat	zkoumat	k5eAaImF	zkoumat
a	a	k8xC	a
prohlubovat	prohlubovat	k5eAaImF	prohlubovat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
všechna	všechen	k3xTgNnPc4	všechen
varování	varování	k1gNnPc4	varování
Galileo	Galilea	k1gFnSc5	Galilea
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
publikacích	publikace	k1gFnPc6	publikace
toto	tento	k3xDgNnSc4	tento
napomenutí	napomenutí	k1gNnSc4	napomenutí
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jeho	jeho	k3xOp3gNnSc6	jeho
učení	učení	k1gNnSc6	učení
církev	církev	k1gFnSc1	církev
oficiálně	oficiálně	k6eAd1	oficiálně
zakázala	zakázat	k5eAaPmAgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
dala	dát	k5eAaPmAgFnS	dát
církev	církev	k1gFnSc1	církev
na	na	k7c4	na
index	index	k1gInSc4	index
zakázané	zakázaný	k2eAgFnSc2d1	zakázaná
literatury	literatura	k1gFnSc2	literatura
(	(	kIx(	(
<g/>
Index	index	k1gInSc1	index
Librorum	Librorum	k1gNnSc1	Librorum
Prohibitorum	Prohibitorum	k1gInSc1	Prohibitorum
<g/>
)	)	kIx)	)
nejen	nejen	k6eAd1	nejen
Galileovy	Galileův	k2eAgFnSc2d1	Galileova
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
starší	starý	k2eAgInPc1d2	starší
Koperníkovy	Koperníkův	k2eAgInPc1d1	Koperníkův
Oběhy	oběh	k1gInPc1	oběh
nebeských	nebeský	k2eAgFnPc2d1	nebeská
sfér	sféra	k1gFnPc2	sféra
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
jedu	jet	k5eAaImIp1nS	jet
knihu	kniha	k1gFnSc4	kniha
Keplerovu	Keplerův	k2eAgFnSc4d1	Keplerova
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prosazování	prosazování	k1gNnSc4	prosazování
heliocentrické	heliocentrický	k2eAgFnSc2d1	heliocentrická
teorie	teorie	k1gFnSc2	teorie
byl	být	k5eAaImAgInS	být
Galileo	Galilea	k1gFnSc5	Galilea
posledních	poslední	k2eAgNnPc2d1	poslední
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
držen	držen	k2eAgInSc1d1	držen
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Edikt	edikt	k1gInSc4	edikt
proti	proti	k7c3	proti
Galileovi	Galileus	k1gMnSc3	Galileus
zrušil	zrušit	k5eAaPmAgMnS	zrušit
papež	papež	k1gMnSc1	papež
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
ve	v	k7c6	v
sporech	spor	k1gInPc6	spor
kolem	kolem	k7c2	kolem
postavení	postavení	k1gNnSc2	postavení
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Země	zem	k1gFnSc2	zem
hráli	hrát	k5eAaImAgMnP	hrát
i	i	k9	i
dva	dva	k4xCgMnPc1	dva
astronomové	astronom	k1gMnPc1	astronom
<g/>
,	,	kIx,	,
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
:	:	kIx,	:
Tycho	Tyc	k1gMnSc4	Tyc
Brahe	Brah	k1gMnSc4	Brah
a	a	k8xC	a
Johanes	Johanes	k1gMnSc1	Johanes
Kepler	Kepler	k1gMnSc1	Kepler
<g/>
.	.	kIx.	.
</s>
<s>
Brahe	Brahe	k6eAd1	Brahe
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
hybridní	hybridní	k2eAgInSc1d1	hybridní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
přímo	přímo	k6eAd1	přímo
kolem	kolem	k6eAd1	kolem
kolem	kolem	k7c2	kolem
nehybné	hybný	k2eNgFnSc2d1	nehybná
Země	zem	k1gFnSc2	zem
obíhaly	obíhat	k5eAaImAgInP	obíhat
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
Slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
planety	planeta	k1gFnPc1	planeta
obíhaly	obíhat	k5eAaImAgFnP	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Kepler	Kepler	k1gMnSc1	Kepler
pak	pak	k6eAd1	pak
objevením	objevení	k1gNnSc7	objevení
zákonů	zákon	k1gInPc2	zákon
pohybu	pohyb	k1gInSc3	pohyb
planet	planeta	k1gFnPc2	planeta
zakotvil	zakotvit	k5eAaPmAgInS	zakotvit
heliocentrický	heliocentrický	k2eAgInSc1d1	heliocentrický
systém	systém	k1gInSc1	systém
i	i	k9	i
prakticky	prakticky	k6eAd1	prakticky
<g/>
:	:	kIx,	:
pohyb	pohyb	k1gInSc1	pohyb
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
třetím	třetí	k4xOgInSc7	třetí
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pohyb	pohyb	k1gInSc1	pohyb
Slunce	slunce	k1gNnSc2	slunce
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Objevem	objev	k1gInSc7	objev
gravitačního	gravitační	k2eAgInSc2d1	gravitační
zákona	zákon	k1gInSc2	zákon
pak	pak	k6eAd1	pak
definitivně	definitivně	k6eAd1	definitivně
tuto	tento	k3xDgFnSc4	tento
otázku	otázka	k1gFnSc4	otázka
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
Isaac	Isaac	k1gFnSc4	Isaac
Newton	Newton	k1gMnSc1	Newton
<g/>
.	.	kIx.	.
</s>
<s>
Archimédova	Archimédův	k2eAgFnSc1d1	Archimédova
námitka	námitka	k1gFnSc1	námitka
proti	proti	k7c3	proti
heliocentrismu	heliocentrismus	k1gInSc3	heliocentrismus
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jeho	jeho	k3xOp3gFnSc2	jeho
pravdivosti	pravdivost	k1gFnSc2	pravdivost
bychom	by	kYmCp1nP	by
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
museli	muset	k5eAaImAgMnP	muset
pozorovat	pozorovat	k5eAaImF	pozorovat
paralaxu	paralaxa	k1gFnSc4	paralaxa
zdánlivě	zdánlivě	k6eAd1	zdánlivě
stálých	stálý	k2eAgFnPc2d1	stálá
pozic	pozice	k1gFnPc2	pozice
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
vyřešena	vyřešit	k5eAaPmNgFnS	vyřešit
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
paralaxa	paralaxa	k1gFnSc1	paralaxa
skutečně	skutečně	k6eAd1	skutečně
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
pomocí	pomocí	k7c2	pomocí
vylepšených	vylepšený	k2eAgInPc2d1	vylepšený
přístrojů	přístroj	k1gInPc2	přístroj
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
Friedrich	Friedrich	k1gMnSc1	Friedrich
Bessel	Bessel	k1gMnSc1	Bessel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
model	model	k1gInSc1	model
heliocentrické	heliocentrický	k2eAgFnSc2d1	heliocentrická
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
větším	veliký	k2eAgInSc6d2	veliký
vesmíru	vesmír	k1gInSc6	vesmír
přijat	přijmout	k5eAaPmNgInS	přijmout
téměř	téměř	k6eAd1	téměř
všemi	všecek	k3xTgFnPc7	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Poznání	poznání	k1gNnSc1	poznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
heliocentrický	heliocentrický	k2eAgInSc1d1	heliocentrický
pohled	pohled	k1gInSc1	pohled
také	také	k9	také
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
doslovném	doslovný	k2eAgNnSc6d1	doslovné
znění	znění	k1gNnSc6	znění
zcela	zcela	k6eAd1	zcela
pravdivý	pravdivý	k2eAgMnSc1d1	pravdivý
<g/>
,	,	kIx,	,
přicházelo	přicházet	k5eAaImAgNnS	přicházet
po	po	k7c6	po
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
krocích	krok	k1gInPc6	krok
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
vznesl	vznést	k5eAaPmAgMnS	vznést
silné	silný	k2eAgInPc4d1	silný
argumenty	argument	k1gInPc4	argument
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slunce	slunce	k1gNnSc1	slunce
není	být	k5eNaImIp3nS	být
středem	střed	k1gInSc7	střed
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nespočetných	spočetný	k2eNgFnPc2d1	nespočetná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
;	;	kIx,	;
Galileo	Galilea	k1gFnSc5	Galilea
měl	mít	k5eAaImAgInS	mít
tentýž	týž	k3xTgInSc4	týž
názor	názor	k1gInSc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
pozice	pozice	k1gFnSc1	pozice
Slunce	slunce	k1gNnSc2	slunce
jako	jako	k8xC	jako
pouhé	pouhý	k2eAgFnSc2d1	pouhá
jedné	jeden	k4xCgFnSc2	jeden
hvězdy	hvězda	k1gFnSc2	hvězda
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
stala	stát	k5eAaPmAgFnS	stát
stále	stále	k6eAd1	stále
samozřejmější	samozřejmý	k2eAgFnSc1d2	samozřejmější
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
ještě	ještě	k9	ještě
před	před	k7c7	před
objevem	objev	k1gInSc7	objev
existence	existence	k1gFnSc2	existence
mnoha	mnoho	k4c2	mnoho
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k8xS	jako
sporná	sporný	k2eAgFnSc1d1	sporná
věc	věc	k1gFnSc1	věc
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
když	když	k8xS	když
omezíme	omezit	k5eAaPmIp1nP	omezit
diskuse	diskuse	k1gFnPc1	diskuse
na	na	k7c4	na
sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc1	slunce
není	být	k5eNaImIp3nS	být
geometrickým	geometrický	k2eAgInSc7d1	geometrický
centrem	centr	k1gInSc7	centr
žádné	žádný	k3yNgFnSc2	žádný
z	z	k7c2	z
planetárních	planetární	k2eAgFnPc2d1	planetární
drah	draha	k1gFnPc2	draha
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
ohniskem	ohnisko	k1gNnSc7	ohnisko
jejich	jejich	k3xOp3gFnPc2	jejich
eliptických	eliptický	k2eAgFnPc2d1	eliptická
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hmotnost	hmotnost	k1gFnSc1	hmotnost
planet	planeta	k1gFnPc2	planeta
nelze	lze	k6eNd1	lze
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
hmotnosti	hmotnost	k1gFnSc3	hmotnost
Slunce	slunce	k1gNnSc2	slunce
zanedbat	zanedbat	k5eAaPmF	zanedbat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
centrum	centrum	k1gNnSc1	centrum
gravitace	gravitace	k1gFnSc2	gravitace
slunečního	sluneční	k2eAgInSc2d1	sluneční
systému	systém	k1gInSc2	systém
umístěno	umístěn	k2eAgNnSc1d1	umístěno
mimo	mimo	k7c4	mimo
střed	střed	k1gInSc4	střed
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
roky	rok	k1gInPc4	rok
i	i	k9	i
mimo	mimo	k7c4	mimo
objem	objem	k1gInSc4	objem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
především	především	k9	především
Jupiteru	Jupiter	k1gInSc2	Jupiter
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
0,14	[number]	k4	0,14
<g/>
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc3	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Celý	celý	k2eAgInSc1d1	celý
koncept	koncept	k1gInSc1	koncept
centra	centrum	k1gNnSc2	centrum
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
termínu	termín	k1gInSc2	termín
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
popřen	popřít	k5eAaPmNgInS	popřít
principem	princip	k1gInSc7	princip
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
neexistuje	existovat	k5eNaImIp3nS	existovat
žádné	žádný	k3yNgNnSc4	žádný
privilegované	privilegovaný	k2eAgNnSc4d1	privilegované
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
formulování	formulování	k1gNnSc2	formulování
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
se	se	k3xPyFc4	se
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
existence	existence	k1gFnSc1	existence
privilegované	privilegovaný	k2eAgFnSc2d1	privilegovaná
třídy	třída	k1gFnSc2	třída
inerciálních	inerciální	k2eAgInPc2d1	inerciální
systémů	systém	k1gInPc2	systém
absolutně	absolutně	k6eAd1	absolutně
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
hypotézy	hypotéza	k1gFnSc2	hypotéza
světlonosného	světlonosný	k2eAgInSc2d1	světlonosný
éteru	éter	k1gInSc2	éter
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc4	některý
formulace	formulace	k1gFnPc4	formulace
Machova	Machův	k2eAgInSc2d1	Machův
principu	princip	k1gInSc2	princip
předpokládaly	předpokládat	k5eAaImAgInP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vztažná	vztažný	k2eAgFnSc1d1	vztažná
soustava	soustava	k1gFnSc1	soustava
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
hmotě	hmota	k1gFnSc3	hmota
vesmíru	vesmír	k1gInSc2	vesmír
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praktických	praktický	k2eAgInPc6d1	praktický
výpočtech	výpočet	k1gInPc6	výpočet
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
často	často	k6eAd1	často
určen	určit	k5eAaPmNgInS	určit
počátek	počátek	k1gInSc1	počátek
a	a	k8xC	a
orientace	orientace	k1gFnSc1	orientace
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
systémy	systém	k1gInPc1	systém
s	s	k7c7	s
počátkem	počátek	k1gInSc7	počátek
ve	v	k7c6	v
hmotném	hmotný	k2eAgInSc6d1	hmotný
středu	střed	k1gInSc6	střed
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Slunce	slunce	k1gNnSc2	slunce
nebo	nebo	k8xC	nebo
v	v	k7c6	v
hmotném	hmotný	k2eAgInSc6d1	hmotný
středu	střed	k1gInSc6	střed
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Přídavná	přídavný	k2eAgNnPc4d1	přídavné
jména	jméno	k1gNnPc4	jméno
geocentrický	geocentrický	k2eAgInSc1d1	geocentrický
a	a	k8xC	a
heliocentrický	heliocentrický	k2eAgMnSc1d1	heliocentrický
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
používána	používat	k5eAaImNgFnS	používat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
výběr	výběr	k1gInSc1	výběr
vztažné	vztažný	k2eAgFnSc2d1	vztažná
soustavy	soustava	k1gFnSc2	soustava
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgInPc4	žádný
filosofické	filosofický	k2eAgInPc4d1	filosofický
ani	ani	k8xC	ani
fyzikální	fyzikální	k2eAgInPc4d1	fyzikální
dopady	dopad	k1gInPc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Sir	sir	k1gMnSc1	sir
Fred	Fred	k1gMnSc1	Fred
Hoyle	Hoyle	k1gFnSc1	Hoyle
<g/>
,	,	kIx,	,
Nicolaus	Nicolaus	k1gMnSc1	Nicolaus
Copernicus	Copernicus	k1gMnSc1	Copernicus
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
