<s>
ArmA	ArmA	k?
3	#num#	k4
</s>
<s>
ArmA	ArmA	k?
3	#num#	k4
<g/>
Logo	logo	k1gNnSc4
hryZákladní	hryZákladný	k2eAgMnPc1d1
informaceVývojářBohemia	informaceVývojářBohemium	k1gNnPc1
Interactive	Interactiv	k1gInSc5
StudioDesignérMartin	StudioDesignérMartin	k1gInSc1
Vaňo	Vaňo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
Herní	herní	k2eAgInSc1d1
sérieArmAEngineReal	sérieArmAEngineReal	k1gInSc1
Virtuality	virtualita	k1gFnSc2
4	#num#	k4
<g/>
PlatformyMicrosoft	PlatformyMicrosoft	k1gInSc1
Windows	Windows	kA
<g/>
,	,	kIx,
Linux	Linux	kA
<g/>
,	,	kIx,
OS	OS	kA
X	X	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
Datum	datum	k1gInSc1
vydání	vydání	k1gNnSc1
<g/>
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
ŽánryFirst-person	ŽánryFirst-person	k1gMnSc1
shooter	shooter	k1gMnSc1
<g/>
,	,	kIx,
SimulátorHerní	SimulátorHerní	k2eAgMnSc1d1
módySingleplayer	módySingleplayer	k1gMnSc1
<g/>
,	,	kIx,
MultiplayerKlasifikacePEGI	MultiplayerKlasifikacePEGI	k1gMnSc1
Oficiální	oficiální	k2eAgFnSc2d1
webové	webový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
ArmA	ArmA	k?
3	#num#	k4
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
sandbox	sandbox	k1gInSc1
od	od	k7c2
Bohemia	bohemia	k1gFnSc2
Interactive	Interactive	k1gFnSc2
Studio	studio	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
pokračováním	pokračování	k1gNnSc7
série	série	k1gFnSc2
ArmA	ArmA	k1gFnSc2
(	(	kIx(
<g/>
ArmA	ArmA	k1gFnSc1
<g/>
,	,	kIx,
ArmA	ArmA	k1gFnSc1
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
2012	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
herním	herní	k2eAgInSc6d1
festivalu	festival	k1gInSc6
E3	E3	k1gFnPc2
předvedena	předveden	k2eAgFnSc1d1
alfa	alfa	k1gFnSc1
verze	verze	k1gFnSc2
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hra	hra	k1gFnSc1
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Děj	děj	k1gInSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
okolo	okolo	k7c2
roku	rok	k1gInSc2
2035	#num#	k4
během	během	k7c2
fiktivní	fiktivní	k2eAgFnSc2d1
operace	operace	k1gFnSc2
Magnitude	Magnitud	k1gMnSc5
<g/>
,	,	kIx,
kde	kde	k6eAd1
proti	proti	k7c3
sobě	se	k3xPyFc3
bojují	bojovat	k5eAaImIp3nP
síly	síla	k1gFnPc4
NATO	NATO	kA
a	a	k8xC
Íránu	Írán	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
soutěži	soutěž	k1gFnSc6
Booom	Booom	k1gInSc4
2013	#num#	k4
zvolena	zvolit	k5eAaPmNgFnS
Nejlepší	dobrý	k2eAgFnSc7d3
českou	český	k2eAgFnSc7d1
hrou	hra	k1gFnSc7
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
byla	být	k5eAaImAgNnP
v	v	k7c6
soutěži	soutěž	k1gFnSc6
Česká	český	k2eAgFnSc1d1
hra	hra	k1gFnSc1
roku	rok	k1gInSc2
oceněna	ocenit	k5eAaPmNgFnS
za	za	k7c4
technický	technický	k2eAgInSc4d1
přínos	přínos	k1gInSc4
české	český	k2eAgFnSc3d1
herní	herní	k2eAgFnSc3d1
tvorbě	tvorba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
</s>
<s>
Příběhová	příběhový	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
Východní	východní	k2eAgInSc1d1
vítr	vítr	k1gInSc1
a	a	k8xC
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
tří	tři	k4xCgInPc2
stáhnutelných	stáhnutelný	k2eAgFnPc2d1
DLC	DLC	kA
–	–	k?
Survive	Surviev	k1gFnSc2
<g/>
,	,	kIx,
Adapt	Adapta	k1gFnPc2
a	a	k8xC
Win	Win	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Survive	Survivat	k5eAaPmIp3nS
</s>
<s>
Síly	síla	k1gFnPc1
NATO	NATO	kA
začnou	začít	k5eAaPmIp3nP
opouštět	opouštět	k5eAaImF
republiku	republika	k1gFnSc4
Altis	Altis	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
posledních	poslední	k2eAgInPc2d1
pět	pět	k4xCc4
let	léto	k1gNnPc2
od	od	k7c2
konce	konec	k1gInSc2
místní	místní	k2eAgFnSc2d1
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
udržují	udržovat	k5eAaImIp3nP
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napětí	napětí	k1gNnSc1
se	se	k3xPyFc4
stupňuje	stupňovat	k5eAaImIp3nS
a	a	k8xC
AAF	AAF	kA
zahajují	zahajovat	k5eAaImIp3nP
blokádu	blokáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vakuum	vakuum	k1gNnSc1
vytvořené	vytvořený	k2eAgFnSc2d1
odchodem	odchod	k1gInSc7
sil	síla	k1gFnPc2
NATO	nato	k6eAd1
vytvoří	vytvořit	k5eAaPmIp3nS
vhodné	vhodný	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
vpád	vpád	k1gInSc4
CSAT	CSAT	kA
<g/>
.	.	kIx.
</s>
<s>
Demontáž	demontáž	k1gFnSc1
vybavení	vybavení	k1gNnSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Stratis	Stratis	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
plném	plný	k2eAgInSc6d1
proudu	proud	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
hlavní	hlavní	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
<g/>
,	,	kIx,
desátník	desátník	k1gMnSc1
Ben	Ben	k1gInSc4
Kerry	Kerra	k1gFnSc2
–	–	k?
člen	člen	k1gMnSc1
formace	formace	k1gFnSc2
Task	Task	k1gMnSc1
Force	force	k1gFnPc2
Aegis	Aegis	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nenadání	nenadání	k1gNnSc2
ocitá	ocitat	k5eAaImIp3nS
pod	pod	k7c7
útokem	útok	k1gInSc7
AAF	AAF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechna	všechen	k3xTgNnPc1
místní	místní	k2eAgNnPc1d1
velení	velení	k1gNnPc1
NATO	NATO	kA
padla	padnout	k5eAaPmAgFnS,k5eAaImAgFnS
a	a	k8xC
komunikace	komunikace	k1gFnSc1
s	s	k7c7
vnějším	vnější	k2eAgInSc7d1
světem	svět	k1gInSc7
byla	být	k5eAaImAgFnS
přerušena	přerušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Zbylým	zbylý	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
přeskupit	přeskupit	k5eAaPmF
se	se	k3xPyFc4
v	v	k7c6
táboře	tábor	k1gInSc6
Maxwell	maxwell	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Velení	velení	k1gNnSc1
se	se	k3xPyFc4
ujímá	ujímat	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgMnSc1d3
přeživší	přeživší	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
kapitán	kapitán	k1gMnSc1
Scott	Scott	k1gMnSc1
Miller	Miller	k1gMnSc1
–	–	k?
britský	britský	k2eAgMnSc1d1
voják	voják	k1gMnSc1
<g/>
,	,	kIx,
příslušník	příslušník	k1gMnSc1
Combat	Combat	k1gMnSc3
Technology	technolog	k1gMnPc4
Research	Research	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
účastnil	účastnit	k5eAaImAgMnS
bojů	boj	k1gInPc2
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
<g/>
,	,	kIx,
Pacifiku	Pacifik	k1gInSc6
a	a	k8xC
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
tábora	tábor	k1gInSc2
Maxwell	maxwell	k1gInSc1
podnikají	podnikat	k5eAaImIp3nP
výpady	výpad	k1gInPc7
proti	proti	k7c3
nepříteli	nepřítel	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
má	mít	k5eAaImIp3nS
nad	nad	k7c7
nimi	on	k3xPp3gMnPc7
početní	početní	k2eAgMnSc1d1
i	i	k9
technickou	technický	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotky	jednotka	k1gFnSc2
AAF	AAF	kA
však	však	k9
jejich	jejich	k3xOp3gNnPc4
postavení	postavení	k1gNnPc4
lokalizovali	lokalizovat	k5eAaBmAgMnP
a	a	k8xC
uprostřed	uprostřed	k7c2
noci	noc	k1gFnSc2
zahájili	zahájit	k5eAaPmAgMnP
útok	útok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
byl	být	k5eAaImAgInS
s	s	k7c7
těžkými	těžký	k2eAgFnPc7d1
ztrátami	ztráta	k1gFnPc7
odražen	odrazit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitánu	kapitán	k1gMnSc3
Millerovi	Miller	k1gMnSc3
se	se	k3xPyFc4
podaří	podařit	k5eAaPmIp3nS
navázat	navázat	k5eAaPmF
kontakt	kontakt	k1gInSc4
s	s	k7c7
vedením	vedení	k1gNnSc7
zdravotnické	zdravotnický	k2eAgFnSc2d1
sekce	sekce	k1gFnSc2
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgInSc6d1
zoufalém	zoufalý	k2eAgInSc6d1
pokusu	pokus	k1gInSc6
útočí	útočit	k5eAaImIp3nS
na	na	k7c4
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
s	s	k7c7
cílem	cíl	k1gInSc7
uvolnit	uvolnit	k5eAaPmF
cestu	cesta	k1gFnSc4
posilám	posila	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
ostrov	ostrov	k1gInSc4
však	však	k9
nečekaně	nečekaně	k6eAd1
doráží	dorážet	k5eAaImIp3nP
síly	síla	k1gFnPc1
CSAT	CSAT	kA
a	a	k8xC
operace	operace	k1gFnSc1
je	být	k5eAaImIp3nS
přerušena	přerušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeživší	přeživší	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
ve	v	k7c6
dvou	dva	k4xCgInPc6
člunech	člun	k1gInPc6
ustupují	ustupovat	k5eAaImIp3nP
na	na	k7c6
Altis	Altis	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
moři	moře	k1gNnSc6
jsou	být	k5eAaImIp3nP
však	však	k9
zpozorováni	zpozorován	k2eAgMnPc1d1
nepřátelskými	přátelský	k2eNgFnPc7d1
helikoptérami	helikoptéra	k1gFnPc7
a	a	k8xC
potopeni	potopen	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Adapt	Adapt	k1gMnSc1
</s>
<s>
Desátník	desátník	k1gMnSc1
Ben	Ben	k1gInSc4
Kerry	Kerra	k1gFnSc2
nabývá	nabývat	k5eAaImIp3nS
vědomí	vědomí	k1gNnSc1
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
Altisu	Altis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naváže	navázat	k5eAaPmIp3nS
kontakt	kontakt	k1gInSc4
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
přeživšími	přeživší	k2eAgFnPc7d1
a	a	k8xC
společně	společně	k6eAd1
se	se	k3xPyFc4
přidávají	přidávat	k5eAaImIp3nP
k	k	k7c3
partyzánské	partyzánský	k2eAgFnSc3d1
buňce	buňka	k1gFnSc3
FIA	FIA	kA
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
vede	vést	k5eAaImIp3nS
Kostas	Kostas	k1gMnSc1
Stavrou	Stavra	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
nepřítelem	nepřítel	k1gMnSc7
bojují	bojovat	k5eAaImIp3nP
guerillovým	guerillový	k2eAgInSc7d1
stylem	styl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
FIA	FIA	kA
se	se	k3xPyFc4
dozví	dozvědět	k5eAaPmIp3nS
o	o	k7c6
chystané	chystaný	k2eAgFnSc6d1
invazi	invaze	k1gFnSc6
NATO	NATO	kA
a	a	k8xC
rozhodne	rozhodnout	k5eAaPmIp3nS
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
podpořit	podpořit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	nato	k6eAd1
ovšem	ovšem	k9
nemá	mít	k5eNaImIp3nS
žádné	žádný	k3yNgFnPc4
zprávy	zpráva	k1gFnPc4
o	o	k7c4
působení	působení	k1gNnSc4
partyzánů	partyzán	k1gMnPc2
na	na	k7c6
Altisu	Altis	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
omylem	omylem	k6eAd1
napadá	napadat	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
jejich	jejich	k3xOp3gFnPc4
jednotky	jednotka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavrou	Stavra	k1gFnSc7
byl	být	k5eAaImAgMnS
pravděpodobně	pravděpodobně	k6eAd1
zabit	zabít	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miller	Miller	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
tým	tým	k1gInSc4
jsou	být	k5eAaImIp3nP
nezvěstní	zvěstný	k2eNgMnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
Kerry	Kerra	k1gFnPc1
se	se	k3xPyFc4
přidává	přidávat	k5eAaImIp3nS
k	k	k7c3
silám	síla	k1gFnPc3
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
rozhovoru	rozhovor	k1gInSc6
s	s	k7c7
důstojníkem	důstojník	k1gMnSc7
se	se	k3xPyFc4
dozvídá	dozvídat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
NATO	NATO	kA
nemá	mít	k5eNaImIp3nS
žádné	žádný	k3yNgFnPc4
zprávy	zpráva	k1gFnPc4
o	o	k7c4
působení	působení	k1gNnSc4
britských	britský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Postavy	postava	k1gFnPc1
</s>
<s>
Ben	Ben	k1gInSc1
Kerry	Kerra	k1gFnSc2
–	–	k?
Desátník	desátník	k1gMnSc1
Kerry	Kerra	k1gFnSc2
je	být	k5eAaImIp3nS
voják	voják	k1gMnSc1
NATO	nato	k6eAd1
účastnící	účastnící	k2eAgMnSc1d1
se	se	k3xPyFc4
mise	mise	k1gFnSc2
na	na	k7c6
Stratisu	Stratis	k1gInSc6
a	a	k8xC
hlavní	hlavní	k2eAgFnSc1d1
postava	postava	k1gFnSc1
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Portlandu	Portland	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
působí	působit	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2034	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nucen	nutit	k5eAaImNgMnS
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
jednotkám	jednotka	k1gFnPc3
AAF	AAF	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
zaútočily	zaútočit	k5eAaPmAgInP
na	na	k7c4
zbývající	zbývající	k2eAgFnPc4d1
síly	síla	k1gFnPc4
NATO	NATO	kA
v	v	k7c4
egejské	egejský	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
do	do	k7c2
boje	boj	k1gInSc2
zapojuje	zapojovat	k5eAaImIp3nS
i	i	k9
CSAT	CSAT	kA
a	a	k8xC
Kerry	Kerra	k1gFnPc1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
stahuje	stahovat	k5eAaImIp3nS
na	na	k7c4
Altis	Altis	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nS
bojů	boj	k1gInPc2
po	po	k7c4
boku	boka	k1gFnSc4
partyzánů	partyzán	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Scott	Scott	k1gMnSc1
Miller	Miller	k1gMnSc1
–	–	k?
Kapitán	kapitán	k1gMnSc1
Miller	Miller	k1gMnSc1
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
důstojník	důstojník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
po	po	k7c6
smrti	smrt	k1gFnSc6
MacKinnona	MacKinnon	k1gMnSc2
převzal	převzít	k5eAaPmAgMnS
velení	velení	k1gNnSc4
nad	nad	k7c7
silami	síla	k1gFnPc7
NATO	NATO	kA
na	na	k7c6
Stratisu	Stratis	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
bojují	bojovat	k5eAaImIp3nP
proti	proti	k7c3
AAF	AAF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
Kerrym	Kerrym	k1gInSc1
<g/>
,	,	kIx,
připojuje	připojovat	k5eAaImIp3nS
k	k	k7c3
FIA	FIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
však	však	k9
vychází	vycházet	k5eAaImIp3nS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
NATO	NATO	kA
i	i	k8xC
FIA	FIA	kA
pouze	pouze	k6eAd1
využíval	využívat	k5eAaImAgInS,k5eAaPmAgInS
k	k	k7c3
získání	získání	k1gNnSc3
tektonické	tektonický	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
Altisu	Altis	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
hlavní	hlavní	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
on	on	k3xPp3gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kostas	Kostas	k1gMnSc1
Stavrou	Stavra	k1gMnSc7
–	–	k?
Velitel	velitel	k1gMnSc1
FIA	FIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
jednotky	jednotka	k1gFnPc1
spolupracují	spolupracovat	k5eAaImIp3nP
s	s	k7c7
Millerem	Miller	k1gMnSc7
a	a	k8xC
Kerrym	Kerrym	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Nikos	Nikos	k1gMnSc1
Panagopoulos	Panagopoulos	k1gMnSc1
–	–	k?
Významný	významný	k2eAgMnSc1d1
představitel	představitel	k1gMnSc1
FIA	FIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
na	na	k7c4
starosti	starost	k1gFnPc4
především	především	k9
zásobování	zásobování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Seržant	seržant	k1gMnSc1
Adams	Adamsa	k1gFnPc2
–	–	k?
Kerryho	Kerry	k1gMnSc2
nadřízený	nadřízený	k1gMnSc1
a	a	k8xC
přítel	přítel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojují	bojovat	k5eAaImIp3nP
spolu	spolu	k6eAd1
proti	proti	k7c3
vojákům	voják	k1gMnPc3
AAF	AAF	kA
<g/>
,	,	kIx,
ale	ale	k8xC
Adams	Adams	k1gInSc1
je	být	k5eAaImIp3nS
zabit	zabit	k2eAgInSc4d1
minou	mina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Andrew	Andrew	k?
MacKinnon	MacKinnon	k1gMnSc1
–	–	k?
Plukovník	plukovník	k1gMnSc1
MacKinnon	MacKinnon	k1gMnSc1
je	být	k5eAaImIp3nS
velitel	velitel	k1gMnSc1
sil	síla	k1gFnPc2
NATO	NATO	kA
v	v	k7c6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zabit	zabít	k5eAaPmNgMnS
na	na	k7c6
začátku	začátek	k1gInSc6
bojů	boj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vahid	Vahid	k1gInSc1
Namdar	Namdar	k1gMnSc1
–	–	k?
Plukovník	plukovník	k1gMnSc1
Namdar	Namdar	k1gMnSc1
velí	velet	k5eAaImIp3nS
invazi	invaze	k1gFnSc3
jednotek	jednotka	k1gFnPc2
CSAT	CSAT	kA
na	na	k7c6
Altis	Altis	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Georgios	Georgios	k1gMnSc1
Akhanteros	Akhanterosa	k1gFnPc2
–	–	k?
Velitel	velitel	k1gMnSc1
jednotek	jednotka	k1gFnPc2
AAF	AAF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
spojencem	spojenec	k1gMnSc7
CSAT	CSAT	kA
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Frakce	frakce	k1gFnSc1
</s>
<s>
NATO	NATO	kA
−	−	k?
vojenské	vojenský	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
západu	západ	k1gInSc2
<g/>
,	,	kIx,
bojující	bojující	k2eAgMnSc1d1
proti	proti	k7c3
CSAT	CSAT	kA
a	a	k8xC
AAF	AAF	kA
<g/>
.	.	kIx.
</s>
<s>
CSAT	CSAT	kA
–	–	k?
vojska	vojsko	k1gNnSc2
východu	východ	k1gInSc3
vedená	vedený	k2eAgFnSc1d1
Íránem	Írán	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
AAF	AAF	kA
(	(	kIx(
<g/>
zelená	zelený	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
altiské	altiský	k2eAgFnPc4d1
ozbrojené	ozbrojený	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
FIA	FIA	kA
(	(	kIx(
<g/>
zelení	zelený	k2eAgMnPc1d1
partyzáni	partyzán	k1gMnPc1
<g/>
)	)	kIx)
–	–	k?
obyvatelé	obyvatel	k1gMnPc1
Altisu	Altis	k1gInSc2
bojující	bojující	k2eAgFnSc1d1
proti	proti	k7c3
okupantům	okupant	k1gMnPc3
<g/>
,	,	kIx,
spojenci	spojenec	k1gMnPc1
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s>
Herní	herní	k2eAgInSc1d1
svět	svět	k1gInSc1
</s>
<s>
Ostrov	ostrov	k1gInSc1
Lémnos	Lémnos	k1gInSc1
(	(	kIx(
<g/>
ve	v	k7c6
hře	hra	k1gFnSc6
jako	jako	k8xC,k8xS
Altis	Altis	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
hře	hra	k1gFnSc6
jsou	být	k5eAaImIp3nP
zpracovány	zpracovat	k5eAaPmNgInP
dva	dva	k4xCgInPc1
řecké	řecký	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
rozloze	rozloha	k1gFnSc6
290	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
severu	sever	k1gInSc6
egejského	egejský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Altis	Altis	k1gFnSc1
–	–	k?
je	být	k5eAaImIp3nS
vytvořen	vytvořit	k5eAaPmNgInS
podle	podle	k7c2
skutečného	skutečný	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
Lémnos	Lémnos	k1gMnSc1
(	(	kIx(
<g/>
Limnos	Limnos	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
rozloze	rozloha	k1gFnSc6
270	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvorba	tvorba	k1gFnSc1
mapy	mapa	k1gFnSc2
probíhala	probíhat	k5eAaImAgFnS
na	na	k7c6
základě	základ	k1gInSc6
satelitních	satelitní	k2eAgInPc2d1
snímků	snímek	k1gInPc2
a	a	k8xC
fotografií	fotografia	k1gFnPc2
pořízených	pořízený	k2eAgFnPc2d1
na	na	k7c6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Oproti	oproti	k7c3
skutečnosti	skutečnost	k1gFnSc3
je	být	k5eAaImIp3nS
o	o	k7c4
něco	něco	k3yInSc4
menší	malý	k2eAgFnSc1d2
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
více	hodně	k6eAd2
vegetace	vegetace	k1gFnSc2
a	a	k8xC
vojenských	vojenský	k2eAgInPc2d1
objektů	objekt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ostrově	ostrov	k1gInSc6
se	se	k3xPyFc4
díky	díky	k7c3
podnebí	podnebí	k1gNnSc3
nachází	nacházet	k5eAaImIp3nS
řada	řada	k1gFnSc1
slunečných	slunečný	k2eAgFnPc2d1
a	a	k8xC
větrných	větrný	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Stratis	Stratis	k1gFnSc1
−	−	k?
je	být	k5eAaImIp3nS
ostrov	ostrov	k1gInSc1
nacházející	nacházející	k2eAgInSc1d1
se	s	k7c7
zhruba	zhruba	k6eAd1
30	#num#	k4
km	km	kA
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Altisu	Altis	k1gInSc2
s	s	k7c7
rozlohou	rozloha	k1gFnSc7
přes	přes	k7c4
19	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vytvořený	vytvořený	k2eAgInSc1d1
podle	podle	k7c2
ostrova	ostrov	k1gInSc2
Agios	Agios	k1gMnSc1
Efstratios	Efstratios	k1gMnSc1
(	(	kIx(
<g/>
Ai	Ai	k1gFnSc1
Stratis	Stratis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Stratis	Stratis	k1gFnSc1
je	být	k5eAaImIp3nS
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
husté	hustý	k2eAgFnSc3d1
vegetaci	vegetace	k1gFnSc3
odlišný	odlišný	k2eAgInSc1d1
od	od	k7c2
Altisu	Altis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západní	západní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
letiště	letiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozovky	vozovka	k1gFnPc1
tvoří	tvořit	k5eAaImIp3nP
zejména	zejména	k9
nezpevněný	zpevněný	k2eNgInSc4d1
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
Stratis	Stratis	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ostrova	ostrov	k1gInSc2
Altis	Altis	k1gFnSc2
<g/>
,	,	kIx,
obsahovala	obsahovat	k5eAaImAgFnS
již	již	k6eAd1
alfa	alfa	k1gFnSc1
verze	verze	k1gFnSc2
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
obou	dva	k4xCgInPc6
ostrovech	ostrov	k1gInPc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
vojenských	vojenský	k2eAgFnPc2d1
základen	základna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Novinkou	novinka	k1gFnSc7
v	v	k7c6
sérii	série	k1gFnSc6
ArmA	ArmA	k1gFnSc2
je	být	k5eAaImIp3nS
zpracovaný	zpracovaný	k2eAgInSc1d1
podvodní	podvodní	k2eAgInSc1d1
svět	svět	k1gInSc1
a	a	k8xC
možnost	možnost	k1gFnSc1
se	se	k3xPyFc4
potápět	potápět	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
přidat	přidat	k5eAaPmF
další	další	k2eAgFnPc4d1
mapy	mapa	k1gFnPc4
vytvořené	vytvořený	k2eAgFnPc4d1
komunitou	komunita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Lémnos	Lémnos	k1gMnSc1
byl	být	k5eAaImAgMnS
vybrán	vybrat	k5eAaPmNgMnS
jako	jako	k8xC,k8xS
inspirace	inspirace	k1gFnSc1
pro	pro	k7c4
hru	hra	k1gFnSc4
ARMA	ARMA	kA
3	#num#	k4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
generální	generální	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
společnosti	společnost	k1gFnSc2
Bohemia	bohemia	k1gFnSc1
Interactive	Interactiv	k1gInSc5
Studio	studio	k1gNnSc1
Marek	Marek	k1gMnSc1
Španěl	Španěl	k1gMnSc1
navštívil	navštívit	k5eAaPmAgMnS
ostrov	ostrov	k1gInSc4
na	na	k7c6
dovolené	dovolená	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
kolem	kolem	k7c2
vydání	vydání	k1gNnSc2
alfa	alfa	k1gNnSc1
verze	verze	k1gFnSc2
na	na	k7c6
hře	hra	k1gFnSc6
pracovalo	pracovat	k5eAaImAgNnS
zhruba	zhruba	k6eAd1
70	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
vyvíjena	vyvíjet	k5eAaImNgFnS
brněnskou	brněnský	k2eAgFnSc7d1
pobočkou	pobočka	k1gFnSc7
Bohemia	bohemia	k1gFnSc1
Interactive	Interactiv	k1gInSc5
Studio	studio	k1gNnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
tvoří	tvořit	k5eAaImIp3nP
bývalé	bývalý	k2eAgNnSc1d1
studio	studio	k1gNnSc1
ALTAR	ALTAR	kA
Games	Games	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
dříve	dříve	k6eAd2
podílelo	podílet	k5eAaImAgNnS
na	na	k7c6
ArmA	ArmA	k1gFnSc6
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kontroverze	kontroverze	k1gFnSc1
</s>
<s>
Zadržení	zadržený	k2eAgMnPc1d1
vývojáři	vývojář	k1gMnPc1
</s>
<s>
Dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2012	#num#	k4
byli	být	k5eAaImAgMnP
na	na	k7c6
Lémnu	Lémno	k1gNnSc6
zadrženi	zadržet	k5eAaPmNgMnP
dva	dva	k4xCgMnPc1
čeští	český	k2eAgMnPc1d1
vývojáři	vývojář	k1gMnPc1
a	a	k8xC
obviněni	obvinit	k5eAaPmNgMnP
ze	z	k7c2
špionáže	špionáž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
policejního	policejní	k2eAgNnSc2d1
vyšetřování	vyšetřování	k1gNnSc2
se	se	k3xPyFc4
provinili	provinit	k5eAaPmAgMnP
dopoledne	dopoledne	k6eAd1
v	v	k7c6
neděli	neděle	k1gFnSc6
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
těsné	těsný	k2eAgFnSc6d1
blízkosti	blízkost	k1gFnSc6
a	a	k8xC
okolí	okolí	k1gNnSc6
vojenské	vojenský	k2eAgFnSc2d1
základny	základna	k1gFnSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Lémnos	Lémnos	k1gInSc1
pořizovali	pořizovat	k5eAaImAgMnP
různé	různý	k2eAgInPc4d1
audiovizuální	audiovizuální	k2eAgInPc4d1
materiály	materiál	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
tento	tento	k3xDgInSc4
přečin	přečin	k1gInSc4
jim	on	k3xPp3gMnPc3
teoreticky	teoreticky	k6eAd1
hrozilo	hrozit	k5eAaImAgNnS
až	až	k9
20	#num#	k4
let	léto	k1gNnPc2
vězení	vězení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
zadržení	zadržení	k1gNnSc6
měly	mít	k5eAaImAgFnP
zásadní	zásadní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
vyhrocené	vyhrocený	k2eAgInPc4d1
řecko-turecké	řecko-turecký	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Propuštěni	propustit	k5eAaPmNgMnP
byli	být	k5eAaImAgMnP
až	až	k9
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2013	#num#	k4
na	na	k7c4
kauci	kauce	k1gFnSc4
5000	#num#	k4
eur	euro	k1gNnPc2
za	za	k7c4
každého	každý	k3xTgMnSc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
,	,	kIx,
po	po	k7c6
celkem	celkem	k6eAd1
128	#num#	k4
dnech	den	k1gInPc6
ve	v	k7c6
vazbě	vazba	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
čeká	čekat	k5eAaImIp3nS
na	na	k7c4
rozhodnutí	rozhodnutí	k1gNnSc4
řecké	řecký	k2eAgFnSc2d1
justice	justice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
průtahy	průtah	k1gInPc4
ve	v	k7c6
vyšetřování	vyšetřování	k1gNnSc6
má	mít	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
vliv	vliv	k1gInSc4
probíhající	probíhající	k2eAgFnSc2d1
krize	krize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zákaz	zákaz	k1gInSc1
v	v	k7c6
Íránu	Írán	k1gInSc6
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2012	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
ArmA	ArmA	k1gFnSc1
3	#num#	k4
nedostala	dostat	k5eNaPmAgFnS
v	v	k7c6
Íránu	Írán	k1gInSc6
licenci	licence	k1gFnSc4
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
se	se	k3xPyFc4
tam	tam	k6eAd1
nesmí	smět	k5eNaImIp3nS
prodávat	prodávat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizacím	organizace	k1gFnPc3
se	se	k3xPyFc4
nelíbí	líbit	k5eNaImIp3nS
zobrazování	zobrazování	k1gNnSc2
íránských	íránský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
jako	jako	k8xC,k8xS
nepřátel	nepřítel	k1gMnPc2
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Změna	změna	k1gFnSc1
názvu	název	k1gInSc2
ostrova	ostrov	k1gInSc2
</s>
<s>
V	v	k7c6
důsledku	důsledek	k1gInSc6
incidentu	incident	k1gInSc2
zatčení	zatčení	k1gNnSc2
dvou	dva	k4xCgMnPc2
vývojářů	vývojář	k1gMnPc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2013	#num#	k4
Bohemia	bohemia	k1gFnSc1
Interactive	Interactiv	k1gInSc5
oznámila	oznámit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jméno	jméno	k1gNnSc1
hlavního	hlavní	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
Limnos	Limnos	k1gInSc1
změní	změnit	k5eAaPmIp3nS
na	na	k7c4
Altis	Altis	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
podle	podle	k7c2
tiskové	tiskový	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
stále	stále	k6eAd1
odráží	odrážet	k5eAaImIp3nS
středomořskou	středomořský	k2eAgFnSc4d1
identitu	identita	k1gFnSc4
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
odlišuje	odlišovat	k5eAaImIp3nS
jej	on	k3xPp3gNnSc4
od	od	k7c2
skutečného	skutečný	k2eAgInSc2d1
řeckého	řecký	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
sloužil	sloužit	k5eAaImAgInS
jako	jako	k9
zdroj	zdroj	k1gInSc1
inspirace	inspirace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Menšímu	malý	k2eAgInSc3d2
ostrovu	ostrov	k1gInSc3
s	s	k7c7
názvem	název	k1gInSc7
Stratis	Stratis	k1gFnPc2
jméno	jméno	k1gNnSc1
zůstalo	zůstat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2011	#num#	k4
−	−	k?
Bohemia	bohemia	k1gFnSc1
Interactive	Interactiv	k1gInSc5
oficiálně	oficiálně	k6eAd1
oznamuje	oznamovat	k5eAaImIp3nS
vývoj	vývoj	k1gInSc1
Army	Arma	k1gFnSc2
3	#num#	k4
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
červen	červen	k1gInSc1
2012	#num#	k4
−	−	k?
na	na	k7c6
herním	herní	k2eAgInSc6d1
festivalu	festival	k1gInSc6
E3	E3	k1gFnPc2
předvedena	předveden	k2eAgFnSc1d1
alfa	alfa	k1gFnSc1
verze	verze	k1gFnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
−	−	k?
vydána	vydán	k2eAgFnSc1d1
Arma	Arma	k1gFnSc1
3	#num#	k4
Alpha	Alpha	k1gFnSc1
<g/>
,	,	kIx,
okamžitě	okamžitě	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
Steamu	Steam	k1gInSc6
dostala	dostat	k5eAaPmAgFnS
na	na	k7c4
první	první	k4xOgFnSc4
příčku	příčka	k1gFnSc4
prodejnosti	prodejnost	k1gFnSc2
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
nejprodávanějším	prodávaný	k2eAgInSc7d3
titulem	titul	k1gInSc7
týdne	týden	k1gInSc2
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
−	−	k?
Arma	Arma	k1gMnSc1
3	#num#	k4
Beta	beta	k1gNnSc1
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
–	–	k?
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
kampaň	kampaň	k1gFnSc1
vyjde	vyjít	k5eAaPmIp3nS
ve	v	k7c6
formě	forma	k1gFnSc6
3	#num#	k4
bezplatných	bezplatný	k2eAgInPc2d1
přídavků	přídavek	k1gInPc2
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
po	po	k7c6
vydání	vydání	k1gNnSc6
plné	plný	k2eAgFnSc2d1
verze	verze	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
−	−	k?
vydána	vydán	k2eAgFnSc1d1
plná	plný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
hry	hra	k1gFnSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
–	–	k?
vydána	vydán	k2eAgFnSc1d1
první	první	k4xOgFnSc1
část	část	k1gFnSc1
kampaně	kampaň	k1gFnSc2
Survive	Surviev	k1gFnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
–	–	k?
vydána	vydán	k2eAgFnSc1d1
druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
kampaně	kampaň	k1gFnSc2
Adapt	Adapta	k1gFnPc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
–	–	k?
vydána	vydán	k2eAgFnSc1d1
třetí	třetí	k4xOgFnSc1
část	část	k1gFnSc1
kampaně	kampaň	k1gFnSc2
Win	Win	k1gFnSc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2014	#num#	k4
–	–	k?
vydán	vydán	k2eAgInSc1d1
Bootcamp	Bootcamp	k1gInSc1
Update	update	k1gInSc4
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgFnSc4d1
výukovou	výukový	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
vydání	vydání	k1gNnSc2
hry	hra	k1gFnSc2
recenzenti	recenzent	k1gMnPc1
vyčítali	vyčítat	k5eAaImAgMnP
převážně	převážně	k6eAd1
chybějící	chybějící	k2eAgFnSc4d1
kampaň	kampaň	k1gFnSc4
<g/>
,	,	kIx,
problémy	problém	k1gInPc4
s	s	k7c7
umělou	umělý	k2eAgFnSc7d1
inteligencí	inteligence	k1gFnSc7
a	a	k8xC
technické	technický	k2eAgFnPc4d1
chyby	chyba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Chválena	chválen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
zejména	zejména	k9
volnost	volnost	k1gFnSc1
a	a	k8xC
rozlehlý	rozlehlý	k2eAgInSc1d1
herní	herní	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
,	,	kIx,
povedená	povedený	k2eAgFnSc1d1
optimalizace	optimalizace	k1gFnSc1
<g/>
,	,	kIx,
propracovaný	propracovaný	k2eAgInSc1d1
editor	editor	k1gInSc1
a	a	k8xC
multiplayer	multiplayer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
RecenzentHodnoceníVerdikt	RecenzentHodnoceníVerdikt	k1gInSc1
</s>
<s>
Zing	Zing	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
Otevřený	otevřený	k2eAgInSc1d1
<g/>
,	,	kIx,
propracovaný	propracovaný	k2eAgInSc1d1
a	a	k8xC
vyšperkovaný	vyšperkovaný	k2eAgInSc1d1
simulátor	simulátor	k1gInSc1
a	a	k8xC
víc	hodně	k6eAd2
než	než	k8xS
důstojný	důstojný	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
série	série	k1gFnSc2
Arma	Arma	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
preferujete	preferovat	k5eAaImIp2nP
realističtější	realistický	k2eAgFnSc4d2
hratelnost	hratelnost	k1gFnSc4
a	a	k8xC
fakt	fakt	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
vám	vy	k3xPp2nPc3
hra	hra	k1gFnSc1
nenaservíruje	naservírovat	k5eNaPmIp3nS
všechno	všechen	k3xTgNnSc1
přímo	přímo	k6eAd1
pod	pod	k7c4
čumák	čumák	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
nic	nic	k3yNnSc4
nečekejte	čekat	k5eNaImRp2nP
a	a	k8xC
vydejte	vydat	k5eAaPmRp2nP
se	se	k3xPyFc4
do	do	k7c2
boje	boj	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
si	se	k3xPyFc3
vás	vy	k3xPp2nPc4
dokáže	dokázat	k5eAaPmIp3nS
získat	získat	k5eAaPmF
na	na	k7c4
zatraceně	zatraceně	k6eAd1
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Games	Games	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
Arma	Arm	k1gInSc2
3	#num#	k4
je	být	k5eAaImIp3nS
výborná	výborný	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
simulace	simulace	k1gFnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
konečně	konečně	k6eAd1
i	i	k9
hra	hra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgFnPc4
chyby	chyba	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
při	při	k7c6
obrovském	obrovský	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
se	se	k3xPyFc4
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
dá	dát	k5eAaPmIp3nS
tolerovat	tolerovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lepší	dobrý	k2eAgInSc1d2
„	„	k?
<g/>
simulátor	simulátor	k1gInSc1
pěšáka	pěšák	k1gMnSc2
<g/>
“	“	k?
nenajdete	najít	k5eNaPmIp2nP
a	a	k8xC
větší	veliký	k2eAgNnSc4d2
sandboxové	sandboxový	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
také	také	k9
ne	ne	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bonusweb	Bonuswba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
Arma	Arm	k1gInSc2
3	#num#	k4
je	být	k5eAaImIp3nS
asi	asi	k9
přesně	přesně	k6eAd1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
všichni	všechen	k3xTgMnPc1
fanoušcí	fanoušcí	k1gMnPc1
série	série	k1gFnSc2
čekali	čekat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Určitě	určitě	k6eAd1
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
vyladěnější	vyladěný	k2eAgFnSc1d2
<g/>
,	,	kIx,
ale	ale	k8xC
stále	stále	k6eAd1
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nic	nic	k3yNnSc1
ani	ani	k8xC
vzdáleně	vzdáleně	k6eAd1
podobného	podobný	k2eAgInSc2d1
na	na	k7c6
trhu	trh	k1gInSc6
nenajdete	najít	k5eNaPmIp2nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hrej	hrát	k5eAaImRp2nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
Vrchol	vrchol	k1gInSc1
série	série	k1gFnSc2
ve	v	k7c6
všech	všecek	k3xTgInPc6
ohledech	ohled	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nádherná	nádherný	k2eAgFnSc1d1
a	a	k8xC
realistická	realistický	k2eAgFnSc1d1
hra	hra	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
přitom	přitom	k6eAd1
naštěstí	naštěstí	k6eAd1
nezapomíná	zapomínat	k5eNaImIp3nS
být	být	k5eAaImF
zábavná	zábavný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čekáme	čekat	k5eAaImIp1nP
ale	ale	k9
ještě	ještě	k9
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
dopadne	dopadnout	k5eAaPmIp3nS
kampaň	kampaň	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Czechgamer	Czechgamer	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
7,5	7,5	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
ArmA	ArmA	k1gFnSc2
III	III	kA
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jeho	jeho	k3xOp3gMnPc1
předchůdci	předchůdce	k1gMnPc1
kvalitní	kvalitní	k2eAgFnSc2d1
simulace	simulace	k1gFnSc2
<g/>
,	,	kIx,
plná	plný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
však	však	k9
přišla	přijít	k5eAaPmAgFnS
příliš	příliš	k6eAd1
brzo	brzo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
spíše	spíše	k9
„	„	k?
<g/>
pískoviště	pískoviště	k1gNnSc1
<g/>
“	“	k?
pro	pro	k7c4
tvorbou	tvorba	k1gFnSc7
posedlé	posedlý	k2eAgMnPc4d1
hráče	hráč	k1gMnPc4
než	než	k8xS
plnohodnotnou	plnohodnotný	k2eAgFnSc4d1
hru	hra	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Doupě	doupě	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
…	…	k?
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
totiž	totiž	k9
o	o	k7c4
neúplný	úplný	k2eNgInSc4d1
produkt	produkt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastně	vlastně	k9
to	ten	k3xDgNnSc1
vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
kdyby	kdyby	kYmCp3nS
někdo	někdo	k3yInSc1
vzal	vzít	k5eAaPmAgMnS
solidní	solidní	k2eAgInSc4d1
základ	základ	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
konečně	konečně	k6eAd1
vypadá	vypadat	k5eAaImIp3nS,k5eAaPmIp3nS
jako	jako	k9
hra	hra	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
jejíž	jejíž	k3xOyRp3gNnSc4
technické	technický	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
se	se	k3xPyFc4
nemusíme	muset	k5eNaImIp1nP
stydět	stydět	k5eAaImF
a	a	k8xC
vyřízl	vyříznout	k5eAaPmAgMnS
z	z	k7c2
něj	on	k3xPp3gNnSc2
téměř	téměř	k6eAd1
všechen	všechen	k3xTgInSc4
obsah	obsah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
pokukujete	pokukovat	k5eAaImIp2nP
hlavně	hlavně	k9
po	po	k7c6
multiplayeru	multiplayer	k1gInSc6
a	a	k8xC
jste	být	k5eAaImIp2nP
se	s	k7c7
sérií	série	k1gFnSc7
a	a	k8xC
jejími	její	k3xOp3gNnPc7
pravidly	pravidlo	k1gNnPc7
obeznámeni	obeznámit	k5eAaPmNgMnP
<g/>
,	,	kIx,
pak	pak	k8xC
není	být	k5eNaImIp3nS
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
řešit	řešit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Eurogamer	Eurogamer	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
Je	být	k5eAaImIp3nS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
jednu	jeden	k4xCgFnSc4
stranu	strana	k1gFnSc4
Arma	Arm	k1gInSc2
3	#num#	k4
ukazuje	ukazovat	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
obří	obří	k2eAgInSc4d1
rozsah	rozsah	k1gInSc4
a	a	k8xC
hlavně	hlavně	k9
díky	díky	k7c3
komunitě	komunita	k1gFnSc3
i	i	k9
vcelku	vcelku	k6eAd1
bohatý	bohatý	k2eAgInSc4d1
obsah	obsah	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
asi	asi	k9
už	už	k6eAd1
definitivně	definitivně	k6eAd1
se	se	k3xPyFc4
nedokáže	dokázat	k5eNaPmIp3nS
zbavit	zbavit	k5eAaPmF
toho	ten	k3xDgNnSc2
břímě	břímě	k1gNnSc4
na	na	k7c4
dnešní	dnešní	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
chybové	chybový	k2eAgFnPc1d1
a	a	k8xC
hodně	hodně	k6eAd1
nedokonalé	dokonalý	k2eNgFnSc2d1
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
plná	plný	k2eAgFnSc1d1
velkých	velký	k2eAgNnPc2d1
i	i	k8xC
drobných	drobný	k2eAgNnPc2d1
zaškobrtnutí	zaškobrtnutí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
nakonec	nakonec	k6eAd1
vidíte	vidět	k5eAaImIp2nP
stejnou	stejný	k2eAgFnSc4d1
známku	známka	k1gFnSc4
<g/>
,	,	kIx,
jakou	jaký	k3yRgFnSc7,k3yIgFnSc7,k3yQgFnSc7
od	od	k7c2
nás	my	k3xPp1nPc2
dostal	dostat	k5eAaPmAgMnS
její	její	k3xOp3gMnSc1
předchůdce	předchůdce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
i	i	k9
do	do	k7c2
třetice	třetice	k1gFnSc2
Arma	Arm	k1gInSc2
rozdělí	rozdělit	k5eAaPmIp3nS
hráčstvo	hráčstvo	k1gNnSc4
na	na	k7c4
dva	dva	k4xCgInPc4
tábory	tábor	k1gInPc4
<g/>
:	:	kIx,
Jedněm	jeden	k4xCgFnPc3
staré	starý	k2eAgFnSc2d1
známé	známý	k2eAgFnPc4d1
nemoci	nemoc	k1gFnSc2
už	už	k9
po	po	k7c6
těch	ten	k3xDgNnPc6
letech	léto	k1gNnPc6
nevadí	vadit	k5eNaImIp3nP
a	a	k8xC
při	při	k7c6
hraní	hraní	k1gNnSc6
tak	tak	k6eAd1
zvedají	zvedat	k5eAaImIp3nP
nohy	noha	k1gFnPc4
aby	aby	kYmCp3nP
nezakopli	zakopnout	k5eNaPmAgMnP
<g/>
,	,	kIx,
ovšem	ovšem	k9
ten	ten	k3xDgInSc4
druhý	druhý	k4xOgInSc4
tábor	tábor	k1gInSc4
je	být	k5eAaImIp3nS
zvedat	zvedat	k5eAaImF
stále	stále	k6eAd1
a	a	k8xC
znovu	znovu	k6eAd1
nechce	chtít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takže	takže	k8xS
na	na	k7c4
jakou	jaký	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
stranu	strana	k1gFnSc4
se	se	k3xPyFc4
přidáte	přidat	k5eAaPmIp2nP
vy	vy	k3xPp2nPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Arma	Arma	k1gMnSc1
3	#num#	k4
rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Martinem	Martin	k1gMnSc7
Vaněm	Vaněma	k1gFnPc2
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
část	část	k1gFnSc1
<g/>
.	.	kIx.
games	games	k1gInSc1
<g/>
.	.	kIx.
<g/>
tiscali	tiscat	k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-22	2011-08-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Experimental	Experimental	k1gMnSc1
client	client	k1gMnSc1
ports	ports	k6eAd1
to	ten	k3xDgNnSc1
Mac	Mac	kA
and	and	k?
Linux	linux	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
LOUKOTA	Loukota	k1gMnSc1
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arma	Arm	k1gInSc2
3	#num#	k4
bez	bez	k7c2
kampaně	kampaň	k1gFnSc2
dorazí	dorazit	k5eAaPmIp3nS
na	na	k7c4
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
games	games	k1gInSc1
<g/>
.	.	kIx.
<g/>
tiscali	tiscat	k5eAaPmAgMnP,k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2013-08-08	2013-08-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Arma	Arma	k1gFnSc1
3	#num#	k4
–	–	k?
E3	E3	k1gMnSc1
2012	#num#	k4
rozhovor	rozhovor	k1gInSc1
<g/>
.	.	kIx.
games	games	k1gInSc1
<g/>
.	.	kIx.
<g/>
tiscali	tiscat	k5eAaPmAgMnP,k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-06-29	2012-06-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Cpt	Cpt	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scott	Scott	k1gMnSc1
Miller	Miller	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
armedassault	armedassault	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikia	wikia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2012-06-19	2012-06-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ArmA	ArmA	k1gFnSc7
3	#num#	k4
Personnel	Personnela	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohemia	bohemia	k1gFnSc1
Interactive	Interactiv	k1gInSc5
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
POLÁČEK	Poláček	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohemia	bohemia	k1gFnSc1
Interactive	Interactiv	k1gInSc5
oznamuje	oznamovat	k5eAaImIp3nS
Arma	Arm	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
tiscali	tiscat	k5eAaPmAgMnP,k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-05-19	2011-05-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HAVRYLUK	HAVRYLUK	kA
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reportáž	reportáž	k1gFnSc1
<g/>
:	:	kIx,
Příběh	příběh	k1gInSc1
mapování	mapování	k1gNnSc2
řeckého	řecký	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
pro	pro	k7c4
ArmA	ArmA	k1gFnSc4
3	#num#	k4
<g/>
.	.	kIx.
eurogamer	eurogamra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-01-22	2013-01-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FIALA	Fiala	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývojáři	vývojář	k1gMnPc1
z	z	k7c2
Bohemia	bohemia	k1gFnSc1
Interactive	Interactiv	k1gInSc5
zatčeni	zatknout	k5eAaPmNgMnP
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
,	,	kIx,
fotili	fotit	k5eAaImAgMnP
vojenské	vojenský	k2eAgFnPc4d1
základny	základna	k1gFnPc4
<g/>
.	.	kIx.
cnews	cnews	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-09-11	2012-09-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
KALIŠ	kališ	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arma	Arm	k1gInSc2
3	#num#	k4
–	–	k?
zaměřeno	zaměřen	k2eAgNnSc4d1
na	na	k7c6
Stratis	Stratis	k1gFnSc6
(	(	kIx(
<g/>
komentované	komentovaný	k2eAgNnSc1d1
video	video	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doupě	doupě	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-06-06	2012-06-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JUNG	Jung	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovor	rozhovor	k1gInSc1
o	o	k7c4
Arma	Armum	k1gNnPc4
3	#num#	k4
prozrazuje	prozrazovat	k5eAaImIp3nS
velké	velký	k2eAgFnPc4d1
ambice	ambice	k1gFnPc4
tvůrců	tvůrce	k1gMnPc2
z	z	k7c2
Bohemky	Bohemka	k1gFnSc2
<g/>
.	.	kIx.
games	games	k1gInSc1
<g/>
.	.	kIx.
<g/>
tiscali	tiscat	k5eAaPmAgMnP,k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-03-06	2013-03-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CVRČEK	Cvrček	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohemia	bohemia	k1gFnSc1
Interactive	Interactiv	k1gInSc5
<g/>
:	:	kIx,
Studio	studio	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
zlepšilo	zlepšit	k5eAaPmAgNnS
herní	herní	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
–	–	k?
Kapitola	kapitola	k1gFnSc1
šestá	šestý	k4xOgFnSc1
–	–	k?
Arma	Arma	k1gFnSc1
2	#num#	k4
se	se	k3xPyFc4
rozšiřuje	rozšiřovat	k5eAaImIp3nS
<g/>
.	.	kIx.
zing	zing	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-05-19	2013-05-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Řecká	řecký	k2eAgFnSc1d1
policie	policie	k1gFnSc1
o	o	k7c6
okolnostech	okolnost	k1gFnPc6
zadržení	zadržení	k1gNnSc2
tvůrců	tvůrce	k1gMnPc2
ArmA	ArmA	k1gMnSc2
3	#num#	k4
<g/>
.	.	kIx.
denik	denika	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-09-13	2012-09-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VÁVRA	Vávra	k1gMnSc1
<g/>
,	,	kIx,
Dan	Dan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Případ	případ	k1gInSc1
Reflex	reflex	k1gInSc4
aneb	aneb	k?
lži	lež	k1gFnSc2
o	o	k7c6
zadržení	zadržení	k1gNnSc6
vývojářů	vývojář	k1gMnPc2
Arma	Arm	k1gInSc2
3	#num#	k4
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
games	games	k1gMnSc1
<g/>
.	.	kIx.
<g/>
tiscali	tiscat	k5eAaPmAgMnP,k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-09-27	2010-09-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DOSKOCIL	DOSKOCIL	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
vývojáři	vývojář	k1gMnPc1
budou	být	k5eAaImBp3nP
z	z	k7c2
Řecka	Řecko	k1gNnSc2
propuštěni	propustit	k5eAaPmNgMnP
na	na	k7c4
kauci	kauce	k1gFnSc4
<g/>
.	.	kIx.
eurogamer	eurogamer	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-01-15	2013-01-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KALIŠ	kališ	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arma	Arma	k1gFnSc1
III	III	kA
nedostala	dostat	k5eNaPmAgFnS
v	v	k7c6
Íránu	Írán	k1gInSc6
licenci	licence	k1gFnSc4
<g/>
,	,	kIx,
tudíž	tudíž	k8xC
se	se	k3xPyFc4
tam	tam	k6eAd1
nesmí	smět	k5eNaImIp3nS
prodávat	prodávat	k5eAaImF
<g/>
.	.	kIx.
doupě	doupě	k1gNnSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-09-20	2012-09-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SAVAGE	SAVAGE	kA
<g/>
,	,	kIx,
Phil	Phil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arma	Arm	k1gInSc2
3	#num#	k4
renames	renames	k1gMnSc1
main	main	k1gMnSc1
island	island	k1gInSc4
to	ten	k3xDgNnSc1
avoid	avoid	k1gInSc1
“	“	k?
<g/>
undesired	undesired	k1gInSc1
real-life	real-lif	k1gInSc5
connotations	connotationsit	k5eAaPmRp2nS
<g/>
”	”	k?
<g/>
.	.	kIx.
pcgamer	pcgamer	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-02-01	2013-02-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KALIŠ	kališ	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Limnos	Limnos	k1gMnSc1
se	se	k3xPyFc4
v	v	k7c6
Armě	Arm	k1gInSc6
3	#num#	k4
změní	změnit	k5eAaPmIp3nS
na	na	k7c4
Altis	Altis	k1gInSc4
kvůli	kvůli	k7c3
incidentu	incident	k1gInSc3
v	v	k7c6
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
doupě	doupě	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-02-01	2013-02-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POLÁČEK	Poláček	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohemia	bohemia	k1gFnSc1
Interactive	Interactiv	k1gInSc5
oznamuje	oznamovat	k5eAaImIp3nS
Arma	Arm	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
games	games	k1gInSc1
<g/>
.	.	kIx.
<g/>
tiscali	tiscat	k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-05-19	2011-05-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CVRČEK	Cvrček	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arma	Arm	k1gInSc2
3	#num#	k4
Alpha	Alph	k1gMnSc4
nejprodávanější	prodávaný	k2eAgFnSc7d3
hrou	hra	k1gFnSc7
na	na	k7c6
Steamu	Steam	k1gInSc6
za	za	k7c4
poslední	poslední	k2eAgInSc4d1
týden	týden	k1gInSc4
<g/>
.	.	kIx.
zing	zing	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-03-20	2013-03-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
CVRČEK	Cvrček	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arma	Arm	k1gInSc2
3	#num#	k4
Beta	beta	k1gNnSc2
vyjde	vyjít	k5eAaPmIp3nS
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
<g/>
.	.	kIx.
zing	zing	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-06-13	2013-06-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
http://games.tiscali.cz/oznameni/velky-update-pridava-do-arma-3-zdarma-tutorialove-mise-a-virtualni-trenink-240610	http://games.tiscali.cz/oznameni/velky-update-pridava-do-arma-3-zdarma-tutorialove-mise-a-virtualni-trenink-240610	k4
<g/>
↑	↑	k?
CVRČEK	Cvrček	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arma	Arm	k1gInSc2
3	#num#	k4
–	–	k?
pravá	pravý	k2eAgFnSc1d1
válečná	válečný	k2eAgFnSc1d1
svoboda	svoboda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zing	Zing	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-12	2013-09-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOVÁŘ	Kovář	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recenze	recenze	k1gFnSc1
–	–	k?
games	games	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
games	games	k1gMnSc1
<g/>
.	.	kIx.
<g/>
tiscali	tiscat	k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-12	2013-09-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SILLMEN	SILLMEN	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mistři	mistr	k1gMnPc1
válečné	válečný	k2eAgFnSc2d1
simulace	simulace	k1gFnSc2
bodují	bodovat	k5eAaImIp3nP
i	i	k9
s	s	k7c7
nekompletní	kompletní	k2eNgFnSc7d1
hrou	hra	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recenze	recenze	k1gFnSc1
Arma	Arm	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bonusweb	Bonuswba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-10-03	2013-10-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
NOVOTNÝ	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Recenze	recenze	k1gFnSc1
–	–	k?
hrej	hrát	k5eAaImRp2nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrej	hrát	k5eAaImRp2nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-16	2013-09-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SZABÓ	SZABÓ	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ArmA	ArmA	k1gFnSc1
III	III	kA
–	–	k?
recenze	recenze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Czechgamer	Czechgamer	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KREMSER	KREMSER	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arma	Arm	k1gInSc2
3	#num#	k4
–	–	k?
napůl	napůl	k6eAd1
vykastrovaný	vykastrovaný	k2eAgInSc4d1
pokrok	pokrok	k1gInSc4
(	(	kIx(
<g/>
recenze	recenze	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doupě	doupě	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-21	2013-09-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JONÁŠ	Jonáš	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
RECENZE	recenze	k1gFnSc1
Arma	Arm	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eurogamer	Eurogamer	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-12	2013-09-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
www.arma3.com	www.arma3.com	k1gInSc1
oficiální	oficiální	k2eAgFnSc2d1
webové	webový	k2eAgFnSc2d1
stránky	stránka	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
www.armaholic.com/forums	www.armaholic.com/forumsit	k5eAaPmRp2nS
<g/>
…	…	k?
<g/>
14450	#num#	k4
obsáhlý	obsáhlý	k2eAgInSc4d1
rozcestník	rozcestník	k1gInSc4
</s>
<s>
www.games.tiscali.cz/arma-3	www.games.tiscali.cz/arma-3	k4
ArmA	ArmA	k1gFnSc1
3	#num#	k4
na	na	k7c4
Games	Games	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
www.steamcommunity.com/app/107410	www.steamcommunity.com/app/107410	k4
ArmA	ArmA	k1gFnSc1
3	#num#	k4
na	na	k7c6
službě	služba	k1gFnSc6
Steam	Steam	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Herní	herní	k2eAgFnSc1d1
série	série	k1gFnSc1
ArmA	ArmA	k1gFnSc2
Hlavní	hlavní	k2eAgFnSc1d1
série	série	k1gFnSc1
</s>
<s>
ArmA	ArmA	k?
(	(	kIx(
<g/>
Queen	Queen	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Gambit	gambit	k1gInSc1
<g/>
)	)	kIx)
<g/>
Arma	Arm	k2eAgFnSc1d1
II	II	kA
(	(	kIx(
<g/>
Operation	Operation	k1gInSc1
Arrowhead	Arrowhead	k1gInSc1
•	•	k?
British	British	k1gInSc1
Armed	Armed	k1gMnSc1
Forces	Forces	k1gMnSc1
•	•	k?
Private	Privat	k1gInSc5
Military	Militara	k1gFnSc2
Company	Compan	k1gInPc4
•	•	k?
Army	Arma	k1gFnSc2
of	of	k?
the	the	k?
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
ArmA	ArmA	k1gFnSc1
III	III	kA
(	(	kIx(
<g/>
Zeus	Zeus	k1gInSc1
•	•	k?
Karts	Karts	k1gInSc1
•	•	k?
Helicopters	Helicopters	k1gInSc1
•	•	k?
Marksmen	Marksmen	k1gInSc1
•	•	k?
Apex	apex	k1gInSc1
•	•	k?
Jets	Jets	k1gInSc1
•	•	k?
Malden	Maldna	k1gFnPc2
2035	#num#	k4
•	•	k?
Laws	Laws	k1gInSc1
of	of	k?
War	War	k1gFnSc2
•	•	k?
Tac-Ops	Tac-Ops	k1gInSc1
•	•	k?
Tanks	Tanks	k1gInSc1
•	•	k?
Global	globat	k5eAaImAgInS
Mobilization	Mobilization	k1gInSc1
•	•	k?
Contact	Contact	k1gInSc1
<g/>
)	)	kIx)
Vedlejší	vedlejší	k2eAgFnSc1d1
série	série	k1gFnSc1
</s>
<s>
ArmA	ArmA	k?
Tactics	Tactics	k1gInSc1
•	•	k?
ArmaA	ArmaA	k1gFnSc2
2	#num#	k4
<g/>
:	:	kIx,
Firing	Firing	k1gInSc1
Range	Range	k1gInSc1
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Bohemia	bohemia	k1gFnSc1
Interactive	Interactiv	k1gInSc5
Studio	studio	k1gNnSc1
•	•	k?
Operace	operace	k1gFnSc1
Flashpoint	Flashpoint	k1gMnSc1
•	•	k?
DayZ	DayZ	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Počítačové	počítačový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
