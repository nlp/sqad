<p>
<s>
Slanina	slanina	k1gFnSc1	slanina
nebo	nebo	k8xC	nebo
také	také	k9	také
špek	špek	k1gInSc4	špek
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
solené	solený	k2eAgNnSc4d1	solené
či	či	k8xC	či
uzené	uzený	k2eAgNnSc4d1	uzené
vepřové	vepřový	k2eAgNnSc4d1	vepřové
sádlo	sádlo	k1gNnSc4	sádlo
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
vepřového	vepřové	k1gNnSc2	vepřové
bůčku	bůček	k1gInSc2	bůček
<g/>
,	,	kIx,	,
kýty	kýta	k1gFnSc2	kýta
nebo	nebo	k8xC	nebo
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
slanina	slanina	k1gFnSc1	slanina
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
naložením	naložení	k1gNnSc7	naložení
do	do	k7c2	do
soli	sůl	k1gFnSc2	sůl
na	na	k7c4	na
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
případně	případně	k6eAd1	případně
pozdějším	pozdní	k2eAgNnSc7d2	pozdější
vyuzením	vyuzení	k1gNnSc7	vyuzení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výraz	výraz	k1gInSc1	výraz
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
zkrácený	zkrácený	k2eAgInSc1d1	zkrácený
název	název	k1gInSc1	název
pro	pro	k7c4	pro
anglickou	anglický	k2eAgFnSc4d1	anglická
slaninu	slanina	k1gFnSc4	slanina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kromě	kromě	k7c2	kromě
sádla	sádlo	k1gNnSc2	sádlo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
maso	maso	k1gNnSc1	maso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Slanina	slanina	k1gFnSc1	slanina
se	se	k3xPyFc4	se
</s>
</p>
<p>
<s>
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
syrovém	syrový	k2eAgInSc6d1	syrový
stavu	stav	k1gInSc6	stav
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
na	na	k7c4	na
oblohu	obloha	k1gFnSc4	obloha
chlebíčků	chlebíček	k1gInPc2	chlebíček
nebo	nebo	k8xC	nebo
do	do	k7c2	do
housky	houska	k1gFnSc2	houska
(	(	kIx(	(
<g/>
bagety	bageta	k1gFnSc2	bageta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
dále	daleko	k6eAd2	daleko
tepelně	tepelně	k6eAd1	tepelně
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
jako	jako	k9	jako
</s>
</p>
<p>
<s>
součást	součást	k1gFnSc1	součást
hlavního	hlavní	k2eAgNnSc2d1	hlavní
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c6	na
špikování	špikování	k?	špikování
masa	maso	k1gNnPc4	maso
či	či	k8xC	či
obal	obal	k1gInSc4	obal
masné	masný	k2eAgFnSc2d1	Masná
rolády	roláda	k1gFnSc2	roláda
</s>
</p>
<p>
<s>
samostatný	samostatný	k2eAgInSc1d1	samostatný
doplněk	doplněk	k1gInSc1	doplněk
pokrmu	pokrm	k1gInSc2	pokrm
<g/>
.	.	kIx.	.
<g/>
Opečená	opečený	k2eAgFnSc1d1	opečená
slanina	slanina	k1gFnSc1	slanina
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
typických	typický	k2eAgFnPc2d1	typická
součástí	součást	k1gFnPc2	součást
anglické	anglický	k2eAgFnSc2d1	anglická
snídaně	snídaně	k1gFnSc2	snídaně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
použití	použití	k1gNnSc1	použití
==	==	k?	==
</s>
</p>
<p>
<s>
vajíčka	vajíčko	k1gNnPc1	vajíčko
se	s	k7c7	s
slaninou	slanina	k1gFnSc7	slanina
</s>
</p>
<p>
<s>
špikování	špikování	k?	špikování
masa	masa	k1gFnSc1	masa
<g/>
,	,	kIx,	,
např.	např.	kA	např.
králík	králík	k1gMnSc1	králík
špikovaný	špikovaný	k2eAgMnSc1d1	špikovaný
slaninou	slanina	k1gFnSc7	slanina
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Pancetta	Pancetta	k1gFnSc1	Pancetta
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
slanina	slanina	k1gFnSc1	slanina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
slanina	slanina	k1gFnSc1	slanina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
