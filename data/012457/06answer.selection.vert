<s>
Mariánské	mariánský	k2eAgNnSc1d1	Mariánské
údolí	údolí	k1gNnSc1	údolí
porostlé	porostlý	k2eAgNnSc1d1	porostlé
převážně	převážně	k6eAd1	převážně
bukovými	bukový	k2eAgInPc7d1	bukový
lesy	les	k1gInPc7	les
je	být	k5eAaImIp3nS	být
sevřené	sevřený	k2eAgNnSc1d1	sevřené
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
a	a	k8xC	a
západu	západ	k1gInSc2	západ
kopcem	kopec	k1gInSc7	kopec
Kapucín	kapucín	k1gMnSc1	kapucín
(	(	kIx(	(
<g/>
743	[number]	k4	743
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
a	a	k8xC	a
východu	východ	k1gInSc2	východ
kopcem	kopec	k1gInSc7	kopec
Kopřivník	Kopřivník	k1gMnSc1	Kopřivník
(	(	kIx(	(
<g/>
699	[number]	k4	699
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
