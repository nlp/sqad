<p>
<s>
Mariánské	mariánský	k2eAgNnSc1d1	Mariánské
Údolí	údolí	k1gNnSc1	údolí
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Marienthal	Marienthal	k1gMnSc1	Marienthal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
místní	místní	k2eAgFnSc4d1	místní
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
Horní	horní	k2eAgInSc1d1	horní
Jiřetín	Jiřetín	k1gInSc1	Jiřetín
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Most	most	k1gInSc1	most
v	v	k7c6	v
Ústeckém	ústecký	k2eAgInSc6d1	ústecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
údolí	údolí	k1gNnSc2	údolí
v	v	k7c6	v
Krušných	krušný	k2eAgFnPc6d1	krušná
horách	hora	k1gFnPc6	hora
podél	podél	k7c2	podél
Jiřetínského	Jiřetínský	k2eAgInSc2d1	Jiřetínský
potoka	potok	k1gInSc2	potok
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
347	[number]	k4	347
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
stavebně	stavebně	k6eAd1	stavebně
plynule	plynule	k6eAd1	plynule
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Horní	horní	k2eAgInSc4d1	horní
Jiřetín	Jiřetín	k1gInSc4	Jiřetín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mariánské	mariánský	k2eAgNnSc1d1	Mariánské
údolí	údolí	k1gNnSc1	údolí
porostlé	porostlý	k2eAgNnSc1d1	porostlé
převážně	převážně	k6eAd1	převážně
bukovými	bukový	k2eAgInPc7d1	bukový
lesy	les	k1gInPc7	les
je	být	k5eAaImIp3nS	být
sevřené	sevřený	k2eAgNnSc1d1	sevřené
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
a	a	k8xC	a
západu	západ	k1gInSc2	západ
kopcem	kopec	k1gInSc7	kopec
Kapucín	kapucín	k1gMnSc1	kapucín
(	(	kIx(	(
<g/>
743	[number]	k4	743
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
a	a	k8xC	a
východu	východ	k1gInSc2	východ
kopcem	kopec	k1gInSc7	kopec
Kopřivník	Kopřivník	k1gMnSc1	Kopřivník
(	(	kIx(	(
<g/>
699	[number]	k4	699
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šestikilometrové	šestikilometrový	k2eAgNnSc1d1	šestikilometrové
údolí	údolí	k1gNnSc1	údolí
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Mariánském	mariánský	k2eAgNnSc6d1	Mariánské
Údolí	údolí	k1gNnSc6	údolí
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
osadě	osada	k1gFnSc6	osada
Mikulovice	Mikulovice	k1gFnSc2	Mikulovice
<g/>
.	.	kIx.	.
</s>
<s>
Údolím	údolí	k1gNnSc7	údolí
teče	téct	k5eAaImIp3nS	téct
Jiřetínský	Jiřetínský	k2eAgInSc1d1	Jiřetínský
potok	potok	k1gInSc1	potok
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnSc1	silnice
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
2541	[number]	k4	2541
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Údolím	údolí	k1gNnSc7	údolí
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
vedla	vést	k5eAaImAgFnS	vést
zemská	zemský	k2eAgFnSc1d1	zemská
obchodní	obchodní	k2eAgFnSc1d1	obchodní
stezka	stezka	k1gFnSc1	stezka
do	do	k7c2	do
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
názvu	název	k1gInSc6	název
osady	osada	k1gFnSc2	osada
Mariánské	mariánský	k2eAgNnSc1d1	Mariánské
Údolí	údolí	k1gNnSc1	údolí
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k9	až
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1845	[number]	k4	1845
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
budoucím	budoucí	k2eAgNnSc6d1	budoucí
Mariánském	mariánský	k2eAgNnSc6d1	Mariánské
Údolí	údolí	k1gNnSc6	údolí
postavena	postaven	k2eAgFnSc1d1	postavena
přádelna	přádelna	k1gFnSc1	přádelna
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
továrny	továrna	k1gFnSc2	továrna
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
domů	dům	k1gInPc2	dům
pro	pro	k7c4	pro
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
a	a	k8xC	a
nově	nově	k6eAd1	nově
založená	založený	k2eAgFnSc1d1	založená
osada	osada	k1gFnSc1	osada
získala	získat	k5eAaPmAgFnS	získat
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
kněžny	kněžna	k1gFnSc2	kněžna
Marie	Maria	k1gFnSc2	Maria
Lobkovicové	Lobkovicový	k2eAgFnSc2d1	Lobkovicová
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Valdštejnové	Valdštejnová	k1gFnSc2	Valdštejnová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
přádelna	přádelna	k1gFnSc1	přádelna
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
ní	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
vybudována	vybudován	k2eAgFnSc1d1	vybudována
tkalcovna	tkalcovna	k1gFnSc1	tkalcovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
má	mít	k5eAaImIp3nS	mít
dodnes	dodnes	k6eAd1	dodnes
výrobní	výrobní	k2eAgInSc1d1	výrobní
závod	závod	k1gInSc1	závod
textilní	textilní	k2eAgInSc1d1	textilní
podnik	podnik	k1gInSc1	podnik
Triola	triola	k1gFnSc1	triola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Osada	osada	k1gFnSc1	osada
původně	původně	k6eAd1	původně
administrativně	administrativně	k6eAd1	administrativně
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
Nové	Nové	k2eAgFnSc3d1	Nové
Vsi	ves	k1gFnSc3	ves
v	v	k7c6	v
Horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
je	být	k5eAaImIp3nS	být
místní	místní	k2eAgFnSc7d1	místní
částí	část	k1gFnSc7	část
Horního	horní	k2eAgInSc2d1	horní
Jiřetína	Jiřetín	k1gInSc2	Jiřetín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
57	[number]	k4	57
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
33	[number]	k4	33
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
kromě	kromě	k7c2	kromě
tří	tři	k4xCgMnPc2	tři
evangelíků	evangelík	k1gMnPc2	evangelík
členy	člen	k1gMnPc7	člen
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
měla	mít	k5eAaImAgFnS	mít
vesnice	vesnice	k1gFnSc1	vesnice
také	také	k9	také
57	[number]	k4	57
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
pět	pět	k4xCc4	pět
Čechoslováků	Čechoslovák	k1gMnPc2	Čechoslovák
<g/>
,	,	kIx,	,
49	[number]	k4	49
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
tři	tři	k4xCgMnPc4	tři
cizince	cizinec	k1gMnPc4	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
na	na	k7c4	na
tři	tři	k4xCgMnPc4	tři
lidi	člověk	k1gMnPc4	člověk
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
se	se	k3xPyFc4	se
hlásili	hlásit	k5eAaImAgMnP	hlásit
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Philipp	Philipp	k1gMnSc1	Philipp
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
<g/>
,	,	kIx,	,
Erinnerungsbuch	Erinnerungsbuch	k1gMnSc1	Erinnerungsbuch
der	drát	k5eAaImRp2nS	drát
Gemeinden	Gemeindna	k1gFnPc2	Gemeindna
Gebirgsneudorf	Gebirgsneudorf	k1gMnSc1	Gebirgsneudorf
<g/>
,	,	kIx,	,
Katharinaberg	Katharinaberg	k1gMnSc1	Katharinaberg
<g/>
,	,	kIx,	,
Brandau	Brandaus	k1gInSc2	Brandaus
<g/>
,	,	kIx,	,
Einsiedl	Einsiedl	k1gMnSc1	Einsiedl
<g/>
,	,	kIx,	,
Kleinhan	Kleinhan	k1gMnSc1	Kleinhan
<g/>
,	,	kIx,	,
Rudelsdorf	Rudelsdorf	k1gMnSc1	Rudelsdorf
<g/>
,	,	kIx,	,
Deutschneudorf	Deutschneudorf	k1gMnSc1	Deutschneudorf
<g/>
,	,	kIx,	,
Deutscheinsiedel	Deutscheinsiedlo	k1gNnPc2	Deutscheinsiedlo
<g/>
,	,	kIx,	,
Deisenhofen	Deisenhofen	k1gInSc1	Deisenhofen
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
s.	s.	k?	s.
39	[number]	k4	39
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mariánské	mariánský	k2eAgFnSc2d1	Mariánská
Údolí	údolí	k1gNnSc4	údolí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
Mariánského	mariánský	k2eAgNnSc2d1	Mariánské
údolí	údolí	k1gNnSc2	údolí
i	i	k8xC	i
Mariánského	mariánský	k2eAgNnSc2d1	Mariánské
Údolí	údolí	k1gNnSc2	údolí
</s>
</p>
