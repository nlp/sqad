<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
George	George	k1gFnSc4	George
Eastman	Eastman	k1gMnSc1	Eastman
první	první	k4xOgInSc4	první
fotografický	fotografický	k2eAgInSc4d1	fotografický
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zbavil	zbavit	k5eAaPmAgMnS	zbavit
fotografy	fotograf	k1gMnPc4	fotograf
nutnosti	nutnost	k1gFnSc2	nutnost
nosit	nosit	k5eAaImF	nosit
s	s	k7c7	s
sebou	se	k3xPyFc7	se
těžké	těžký	k2eAgNnSc4d1	těžké
skleněné	skleněný	k2eAgFnPc4d1	skleněná
fotografické	fotografický	k2eAgFnPc4d1	fotografická
desky	deska	k1gFnPc4	deska
a	a	k8xC	a
jedovaté	jedovatý	k2eAgFnPc4d1	jedovatá
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
.	.	kIx.	.
</s>
