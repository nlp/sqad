<s>
Fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
umění	umění	k1gNnSc4	umění
i	i	k8xC	i
věda	věda	k1gFnSc1	věda
využívající	využívající	k2eAgFnSc2d1	využívající
světla	světlo	k1gNnSc2	světlo
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
trvalého	trvalý	k2eAgInSc2d1	trvalý
obrazového	obrazový	k2eAgInSc2d1	obrazový
záznamu	záznam	k1gInSc2	záznam
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
digitálně	digitálně	k6eAd1	digitálně
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
čipu	čip	k1gInSc2	čip
nebo	nebo	k8xC	nebo
chemickým	chemický	k2eAgInSc7d1	chemický
procesem	proces	k1gInSc7	proces
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
světlocitlivého	světlocitlivý	k2eAgInSc2d1	světlocitlivý
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
vyzařováno	vyzařován	k2eAgNnSc1d1	vyzařováno
nebo	nebo	k8xC	nebo
odráženo	odrážen	k2eAgNnSc1d1	odráženo
objekty	objekt	k1gInPc7	objekt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
zaostřeno	zaostřen	k2eAgNnSc1d1	Zaostřeno
pomocí	pomocí	k7c2	pomocí
objektivu	objektiv	k1gInSc2	objektiv
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
během	běh	k1gInSc7	běh
expozice	expozice	k1gFnSc1	expozice
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
světlocitlivý	světlocitlivý	k2eAgInSc4d1	světlocitlivý
povrch	povrch	k1gInSc4	povrch
uvnitř	uvnitř	k7c2	uvnitř
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nP	tvořit
skutečný	skutečný	k2eAgInSc4d1	skutečný
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
digitálních	digitální	k2eAgInPc2d1	digitální
čipů	čip	k1gInPc2	čip
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
pixelech	pixel	k1gInPc6	pixel
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Získaný	získaný	k2eAgInSc1d1	získaný
signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
konvertován	konvertován	k2eAgMnSc1d1	konvertován
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
digitálního	digitální	k2eAgInSc2d1	digitální
údaje	údaj	k1gInSc2	údaj
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgInSc1d1	uložen
jako	jako	k9	jako
digitálního	digitální	k2eAgInSc2d1	digitální
soubor	soubor	k1gInSc4	soubor
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgNnSc4d2	pozdější
zobrazení	zobrazení	k1gNnSc4	zobrazení
nebo	nebo	k8xC	nebo
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
postupu	postup	k1gInSc2	postup
využívajícího	využívající	k2eAgInSc2d1	využívající
fotografickou	fotografický	k2eAgFnSc4d1	fotografická
emulzi	emulze	k1gFnSc4	emulze
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
výsledkem	výsledek	k1gInSc7	výsledek
neviditelný	viditelný	k2eNgInSc4d1	neviditelný
latentní	latentní	k2eAgInSc4d1	latentní
obraz	obraz	k1gInSc4	obraz
a	a	k8xC	a
až	až	k9	až
jeho	jeho	k3xOp3gNnSc7	jeho
chemickým	chemický	k2eAgNnSc7d1	chemické
vyvoláním	vyvolání	k1gNnSc7	vyvolání
vzniká	vznikat	k5eAaImIp3nS	vznikat
obraz	obraz	k1gInSc1	obraz
viditelný	viditelný	k2eAgInSc1d1	viditelný
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
negativ	negativ	k1gInSc4	negativ
nebo	nebo	k8xC	nebo
pozitiv	pozitiv	k1gInSc4	pozitiv
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
účelu	účel	k1gInSc6	účel
fotografického	fotografický	k2eAgInSc2d1	fotografický
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
metodě	metoda	k1gFnSc6	metoda
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc1d1	filmový
negativ	negativ	k1gInSc1	negativ
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
využíván	využívat	k5eAaPmNgInS	využívat
k	k	k7c3	k
tisku	tisk	k1gInSc3	tisk
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
pozitivního	pozitivní	k2eAgInSc2d1	pozitivní
obrazu	obraz	k1gInSc2	obraz
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
zvětšovacího	zvětšovací	k2eAgInSc2d1	zvětšovací
přístroje	přístroj	k1gInSc2	přístroj
nebo	nebo	k8xC	nebo
kontaktní	kontaktní	k2eAgFnSc2d1	kontaktní
kopie	kopie	k1gFnSc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
využívaná	využívaný	k2eAgFnSc1d1	využívaná
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
výroby	výroba	k1gFnSc2	výroba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
fotolitografie	fotolitografie	k1gFnSc2	fotolitografie
<g/>
)	)	kIx)	)
i	i	k8xC	i
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Přímější	přímý	k2eAgNnSc1d2	přímější
využití	využití	k1gNnSc1	využití
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c4	v
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
filmové	filmový	k2eAgNnSc4d1	filmové
a	a	k8xC	a
video	video	k1gNnSc4	video
produkci	produkce	k1gFnSc3	produkce
i	i	k8xC	i
oblastech	oblast	k1gFnPc6	oblast
rekreace	rekreace	k1gFnSc2	rekreace
a	a	k8xC	a
masové	masový	k2eAgFnSc2d1	masová
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
řeckých	řecký	k2eAgNnPc2d1	řecké
slov	slovo	k1gNnPc2	slovo
<g/>
:	:	kIx,	:
φ	φ	k?	φ
(	(	kIx(	(
<g/>
fō	fō	k?	fō
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
světlo	světlo	k1gNnSc4	světlo
<g/>
"	"	kIx"	"
a	a	k8xC	a
γ	γ	k?	γ
(	(	kIx(	(
<g/>
grafé	grafá	k1gFnSc2	grafá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zobrazení	zobrazení	k1gNnSc1	zobrazení
pomocí	pomoc	k1gFnPc2	pomoc
čar	čára	k1gFnPc2	čára
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
kreslení	kreslení	k1gNnSc1	kreslení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
tedy	tedy	k9	tedy
tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
znamenají	znamenat	k5eAaImIp3nP	znamenat
"	"	kIx"	"
<g/>
kreslení	kreslení	k1gNnSc4	kreslení
světlem	světlo	k1gNnSc7	světlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgInSc4d1	nový
termín	termín	k1gInSc4	termín
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
dvou	dva	k4xCgNnPc2	dva
slov	slovo	k1gNnPc2	slovo
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
lidí	člověk	k1gMnPc2	člověk
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Hercules	Hercules	k1gInSc1	Hercules
Florence	Florenc	k1gFnSc2	Florenc
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
brazilském	brazilský	k2eAgNnSc6d1	brazilské
městě	město	k1gNnSc6	město
Campinas	Campinas	k1gMnSc1	Campinas
<g/>
,	,	kIx,	,
použil	použít	k5eAaPmAgMnS	použít
podle	podle	k7c2	podle
historika	historik	k1gMnSc2	historik
Borise	Boris	k1gMnSc2	Boris
Kossoje	Kossoj	k1gInSc2	Kossoj
francouzský	francouzský	k2eAgInSc1d1	francouzský
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
photographie	photographie	k1gFnSc1	photographie
<g/>
"	"	kIx"	"
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
osobních	osobní	k2eAgInPc6d1	osobní
zápiscích	zápisek	k1gInPc6	zápisek
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
kniha	kniha	k1gFnSc1	kniha
Geschichte	Geschicht	k1gInSc5	Geschicht
der	drát	k5eAaImRp2nS	drát
Photographie	Photographie	k1gFnSc2	Photographie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
pak	pak	k6eAd1	pak
připisuje	připisovat	k5eAaImIp3nS	připisovat
použití	použití	k1gNnSc4	použití
tohoto	tento	k3xDgInSc2	tento
výrazu	výraz	k1gInSc2	výraz
berlínskému	berlínský	k2eAgMnSc3d1	berlínský
astronomu	astronom	k1gMnSc3	astronom
Johannu	Johann	k1gMnSc3	Johann
von	von	k1gInSc4	von
Mädlerovi	Mädler	k1gMnSc6	Mädler
v	v	k7c6	v
článku	článek	k1gInSc6	článek
publikovaném	publikovaný	k2eAgInSc6d1	publikovaný
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1839	[number]	k4	1839
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
novinách	novina	k1gFnPc6	novina
Vossische	Vossisch	k1gFnSc2	Vossisch
Zeitung	Zeitung	k1gInSc1	Zeitung
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
o	o	k7c6	o
obou	dva	k4xCgInPc6	dva
těchto	tento	k3xDgNnPc6	tento
tvrzeních	tvrzení	k1gNnPc6	tvrzení
hojně	hojně	k6eAd1	hojně
informuje	informovat	k5eAaBmIp3nS	informovat
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
strana	strana	k1gFnSc1	strana
zatím	zatím	k6eAd1	zatím
nepotvrdila	potvrdit	k5eNaPmAgFnS	potvrdit
jejich	jejich	k3xOp3gFnSc4	jejich
pravdivost	pravdivost	k1gFnSc4	pravdivost
<g/>
.	.	kIx.	.
</s>
<s>
Zásluha	zásluha	k1gFnSc1	zásluha
za	za	k7c4	za
vytvoření	vytvoření	k1gNnSc4	vytvoření
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
jeho	jeho	k3xOp3gFnPc4	jeho
anglické	anglický	k2eAgFnPc4d1	anglická
formy	forma	k1gFnPc4	forma
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
photography	photographa	k1gFnPc4	photographa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
i	i	k9	i
seznámení	seznámení	k1gNnSc1	seznámení
veřejnosti	veřejnost	k1gFnSc2	veřejnost
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
slovem	slovo	k1gNnSc7	slovo
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
připisovaná	připisovaný	k2eAgFnSc1d1	připisovaná
Johnu	John	k1gMnSc3	John
Herschelovi	Herschel	k1gMnSc3	Herschel
<g/>
.	.	kIx.	.
</s>
<s>
Herschel	Herschel	k1gInSc1	Herschel
novotvar	novotvar	k1gInSc1	novotvar
použil	použít	k5eAaPmAgInS	použít
v	v	k7c6	v
korespondenci	korespondence	k1gFnSc6	korespondence
již	již	k6eAd1	již
před	před	k7c7	před
25	[number]	k4	25
<g/>
.	.	kIx.	.
únorem	únor	k1gInSc7	únor
1839	[number]	k4	1839
a	a	k8xC	a
veřejně	veřejně	k6eAd1	veřejně
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1839	[number]	k4	1839
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
londýnské	londýnský	k2eAgFnSc6d1	londýnská
přednášce	přednáška	k1gFnSc6	přednáška
pro	pro	k7c4	pro
Královskou	královský	k2eAgFnSc4d1	královská
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
uvedených	uvedený	k2eAgInPc6d1	uvedený
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
Herschlovo	Herschlův	k2eAgNnSc1d1	Herschlův
používání	používání	k1gNnSc1	používání
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
"	"	kIx"	"
natolik	natolik	k6eAd1	natolik
dobře	dobře	k6eAd1	dobře
doložené	doložený	k2eAgNnSc1d1	doložené
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
přijímáno	přijímat	k5eAaImNgNnS	přijímat
bez	bez	k7c2	bez
další	další	k2eAgFnSc2d1	další
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
zpochybňování	zpochybňování	k1gNnSc2	zpochybňování
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historie	historie	k1gFnSc1	historie
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
díky	díky	k7c3	díky
spojení	spojení	k1gNnSc4	spojení
několika	několik	k4yIc2	několik
technických	technický	k2eAgInPc2d1	technický
objevů	objev	k1gInPc2	objev
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgInPc4	svůj
počátky	počátek	k1gInPc4	počátek
dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
prvních	první	k4xOgFnPc2	první
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Číně	Čína	k1gFnSc6	Čína
Mocius	Mocius	k1gMnSc1	Mocius
<g/>
,	,	kIx,	,
příslušník	příslušník	k1gMnSc1	příslušník
monistické	monistický	k2eAgFnSc2d1	monistická
filozofické	filozofický	k2eAgFnSc2d1	filozofická
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgInS	objevit
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
vědecké	vědecký	k2eAgInPc4d1	vědecký
principy	princip	k1gInPc4	princip
optiky	optika	k1gFnSc2	optika
a	a	k8xC	a
camery	camera	k1gFnSc2	camera
obscury	obscura	k1gFnSc2	obscura
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Camera	Camera	k1gFnSc1	Camera
obscura	obscura	k1gFnSc1	obscura
<g/>
"	"	kIx"	"
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
temná	temný	k2eAgFnSc1d1	temná
komora	komora	k1gFnSc1	komora
<g/>
"	"	kIx"	"
–	–	k?	–
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
objekt	objekt	k1gInSc1	objekt
s	s	k7c7	s
otvorem	otvor	k1gInSc7	otvor
umožňujícím	umožňující	k2eAgInSc7d1	umožňující
průchod	průchod	k1gInSc1	průchod
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
ležící	ležící	k2eAgFnSc6d1	ležící
proti	proti	k7c3	proti
otvoru	otvor	k1gInSc3	otvor
vzniká	vznikat	k5eAaImIp3nS	vznikat
převrácený	převrácený	k2eAgInSc4d1	převrácený
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
její	její	k3xOp3gInSc1	její
princip	princip	k1gInSc1	princip
popsán	popsat	k5eAaPmNgInS	popsat
i	i	k9	i
Aristotelem	Aristoteles	k1gMnSc7	Aristoteles
a	a	k8xC	a
Euklidem	Euklides	k1gMnSc7	Euklides
a	a	k8xC	a
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
byzantským	byzantský	k2eAgMnSc7d1	byzantský
matematikem	matematik	k1gMnSc7	matematik
Anthémiem	Anthémius	k1gMnSc7	Anthémius
z	z	k7c2	z
Trallu	Trall	k1gInSc2	Trall
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
vlastním	vlastní	k2eAgInPc3d1	vlastní
experimentům	experiment	k1gInPc3	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Camera	Camera	k1gFnSc1	Camera
obscura	obscura	k1gFnSc1	obscura
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
také	také	k9	také
čínským	čínský	k2eAgMnSc7d1	čínský
učencem	učenec	k1gMnSc7	učenec
Šen	Šen	k1gMnSc7	Šen
Kua	Kua	k1gMnSc7	Kua
(	(	kIx(	(
<g/>
1031	[number]	k4	1031
<g/>
-	-	kIx~	-
<g/>
1095	[number]	k4	1095
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Mengxi	Mengxe	k1gFnSc4	Mengxe
bitan	bitan	k1gMnSc1	bitan
věnuje	věnovat	k5eAaImIp3nS	věnovat
jak	jak	k6eAd1	jak
jejímu	její	k3xOp3gInSc3	její
popisu	popis	k1gInSc3	popis
<g/>
,	,	kIx,	,
tak	tak	k9	tak
optické	optický	k2eAgFnSc6d1	optická
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
a	a	k8xC	a
arabským	arabský	k2eAgMnSc7d1	arabský
fyzikem	fyzik	k1gMnSc7	fyzik
Alhazenem	Alhazen	k1gMnSc7	Alhazen
(	(	kIx(	(
<g/>
965	[number]	k4	965
<g/>
-	-	kIx~	-
<g/>
1040	[number]	k4	1040
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
metody	metoda	k1gFnPc4	metoda
popsané	popsaný	k2eAgFnPc4d1	popsaná
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Velká	velký	k2eAgFnSc1d1	velká
optika	optika	k1gFnSc1	optika
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
<g/>
,	,	kIx,	,
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
středověkých	středověký	k2eAgInPc2d1	středověký
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
primitivních	primitivní	k2eAgFnPc2d1	primitivní
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
fotografii	fotografia	k1gFnSc4	fotografia
byl	být	k5eAaImAgInS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
i	i	k8xC	i
objev	objev	k1gInSc1	objev
dusičnanu	dusičnan	k1gInSc2	dusičnan
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
Albertem	Albert	k1gMnSc7	Albert
Velikým	veliký	k2eAgInSc7d1	veliký
(	(	kIx(	(
<g/>
1193	[number]	k4	1193
<g/>
-	-	kIx~	-
<g/>
1280	[number]	k4	1280
<g/>
)	)	kIx)	)
a	a	k8xC	a
chloridu	chlorid	k1gInSc2	chlorid
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
Georgem	Georg	k1gInSc7	Georg
Fabriciem	Fabricium	k1gNnSc7	Fabricium
(	(	kIx(	(
<g/>
1516	[number]	k4	1516
<g/>
-	-	kIx~	-
<g/>
1571	[number]	k4	1571
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
jako	jako	k8xS	jako
Fabricius	Fabricius	k1gInSc4	Fabricius
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1566	[number]	k4	1566
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
Daniele	Daniela	k1gFnSc3	Daniela
Barbaro	Barbara	k1gFnSc5	Barbara
clonu	clona	k1gFnSc4	clona
a	a	k8xC	a
o	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1694	[number]	k4	1694
<g/>
,	,	kIx,	,
popsal	popsat	k5eAaPmAgMnS	popsat
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Homberg	Homberg	k1gMnSc1	Homberg
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
světlo	světlo	k1gNnSc1	světlo
ztmavuje	ztmavovat	k5eAaImIp3nS	ztmavovat
některé	některý	k3yIgFnPc4	některý
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
fotografii	fotografia	k1gFnSc6	fotografia
se	se	k3xPyFc4	se
však	však	k9	však
neomezovaly	omezovat	k5eNaImAgInP	omezovat
jenom	jenom	k9	jenom
na	na	k7c4	na
vědecké	vědecký	k2eAgNnSc4d1	vědecké
bádání	bádání	k1gNnSc4	bádání
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
fotografie	fotografia	k1gFnSc2	fotografia
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
popsán	popsat	k5eAaPmNgInS	popsat
i	i	k9	i
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
románu	román	k1gInSc6	román
Tiphaigne	Tiphaign	k1gInSc5	Tiphaign
de	de	k?	de
la	la	k1gNnSc3	la
Roche	Roch	k1gFnSc2	Roch
Giphanie	Giphanie	k1gFnSc2	Giphanie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1760	[number]	k4	1760
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
objev	objev	k1gInSc1	objev
camery	camera	k1gFnSc2	camera
obscury	obscura	k1gFnSc2	obscura
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
až	až	k9	až
do	do	k7c2	do
starověké	starověký	k2eAgFnSc2d1	starověká
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
,	,	kIx,	,
že	že	k8xS	že
našla	najít	k5eAaPmAgFnS	najít
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
i	i	k9	i
o	o	k7c4	o
staletí	staletí	k1gNnPc4	staletí
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
renesanční	renesanční	k2eAgFnSc6d1	renesanční
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Camera	Camera	k1gFnSc1	Camera
obscura	obscura	k1gFnSc1	obscura
byla	být	k5eAaImAgFnS	být
využívaná	využívaný	k2eAgFnSc1d1	využívaná
renesančními	renesanční	k2eAgMnPc7d1	renesanční
malíři	malíř	k1gMnPc7	malíř
a	a	k8xC	a
barevné	barevný	k2eAgNnSc4d1	barevné
podání	podání	k1gNnSc4	podání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgFnSc1d1	dominantní
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
vychází	vycházet	k5eAaImIp3nS	vycházet
právě	právě	k9	právě
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
Leonardo	Leonardo	k1gMnSc1	Leonardo
da	da	k?	da
Vinci	Vinca	k1gMnSc2	Vinca
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
cameře	camera	k1gFnSc6	camera
obscuře	obscura	k1gFnSc6	obscura
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
její	její	k3xOp3gFnSc3	její
přírodní	přírodní	k2eAgFnSc3d1	přírodní
formě	forma	k1gFnSc3	forma
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
temných	temný	k2eAgFnPc2d1	temná
jeskyní	jeskyně	k1gFnPc2	jeskyně
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
slunečného	slunečný	k2eAgNnSc2d1	slunečné
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Otvor	otvor	k1gInSc1	otvor
ve	v	k7c6	v
stěně	stěna	k1gFnSc6	stěna
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jeskyni	jeskyně	k1gFnSc4	jeskyně
fungovat	fungovat	k5eAaImF	fungovat
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
na	na	k7c6	na
jakém	jaký	k3yQgNnSc6	jaký
funguje	fungovat	k5eAaImIp3nS	fungovat
právě	právě	k9	právě
camera	camera	k1gFnSc1	camera
obscura	obscura	k1gFnSc1	obscura
<g/>
,	,	kIx,	,
a	a	k8xC	a
procházející	procházející	k2eAgNnSc1d1	procházející
světlo	světlo	k1gNnSc1	světlo
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
protilehlé	protilehlý	k2eAgFnPc4d1	protilehlá
otvoru	otvor	k1gInSc6	otvor
převrácený	převrácený	k2eAgInSc4d1	převrácený
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
fotografie	fotografia	k1gFnSc2	fotografia
byly	být	k5eAaImAgInP	být
věnované	věnovaný	k2eAgNnSc1d1	věnované
hlavně	hlavně	k9	hlavně
vymýšlení	vymýšlení	k1gNnSc1	vymýšlení
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tento	tento	k3xDgInSc4	tento
obraz	obraz	k1gInSc4	obraz
zachytit	zachytit	k5eAaPmF	zachytit
a	a	k8xC	a
uchovat	uchovat	k5eAaPmF	uchovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
známý	známý	k2eAgInSc1d1	známý
pokus	pokus	k1gInSc1	pokus
o	o	k7c6	o
zachycení	zachycení	k1gNnSc6	zachycení
obrazu	obraz	k1gInSc2	obraz
v	v	k7c6	v
cameře	camera	k1gFnSc6	camera
obscuře	obscura	k1gFnSc6	obscura
pomocí	pomocí	k7c2	pomocí
světlocitlivého	světlocitlivý	k2eAgInSc2d1	světlocitlivý
materiálu	materiál	k1gInSc2	materiál
provedl	provést	k5eAaPmAgMnS	provést
britský	britský	k2eAgMnSc1d1	britský
vynálezce	vynálezce	k1gMnSc1	vynálezce
Thomas	Thomas	k1gMnSc1	Thomas
Wedgwood	Wedgwood	k1gInSc4	Wedgwood
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
papíru	papír	k1gInSc2	papír
či	či	k8xC	či
kůže	kůže	k1gFnSc2	kůže
naimpregnovaných	naimpregnovaný	k2eAgFnPc2d1	naimpregnovaná
dusičnanem	dusičnan	k1gInSc7	dusičnan
stříbrným	stříbrný	k2eAgInSc7d1	stříbrný
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
povedlo	povést	k5eAaPmAgNnS	povést
zachytit	zachytit	k5eAaPmF	zachytit
siluety	silueta	k1gFnPc4	silueta
objektů	objekt	k1gInPc2	objekt
umístěných	umístěný	k2eAgInPc2d1	umístěný
na	na	k7c6	na
přímém	přímý	k2eAgNnSc6d1	přímé
slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
zvládl	zvládnout	k5eAaPmAgInS	zvládnout
zachytit	zachytit	k5eAaPmF	zachytit
i	i	k9	i
obrysy	obrys	k1gInPc4	obrys
maleb	malba	k1gFnPc2	malba
na	na	k7c6	na
skle	sklo	k1gNnSc6	sklo
nasvíceném	nasvícený	k2eAgNnSc6d1	nasvícené
sluncem	slunce	k1gNnSc7	slunce
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
ohlášeno	ohlášet	k5eAaImNgNnS	ohlášet
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
obrazy	obraz	k1gInPc1	obraz
vytvořené	vytvořený	k2eAgInPc1d1	vytvořený
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
camery	camera	k1gFnSc2	camera
obscury	obscura	k1gFnSc2	obscura
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
slabé	slabý	k2eAgFnPc1d1	slabá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
přiměřenou	přiměřený	k2eAgFnSc4d1	přiměřená
dobu	doba	k1gFnSc4	doba
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
dusičnany	dusičnan	k1gInPc4	dusičnan
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
siluet	silueta	k1gFnPc2	silueta
časem	čas	k1gInSc7	čas
zcela	zcela	k6eAd1	zcela
ztmavly	ztmavnout	k5eAaPmAgFnP	ztmavnout
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
známá	známý	k2eAgFnSc1d1	známá
fotografie	fotografie	k1gFnSc1	fotografie
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
francouzským	francouzský	k2eAgMnSc7d1	francouzský
vynálezcem	vynálezce	k1gMnSc7	vynálezce
Nicéphorem	Nicéphor	k1gMnSc7	Nicéphor
Niépcem	Niépec	k1gMnSc7	Niépec
<g/>
,	,	kIx,	,
při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
o	o	k7c4	o
vyhotovení	vyhotovení	k1gNnSc4	vyhotovení
tisku	tisk	k1gInSc2	tisk
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Niépce	Niépce	k1gFnPc4	Niépce
byl	být	k5eAaImAgInS	být
znovu	znovu	k6eAd1	znovu
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1825	[number]	k4	1825
<g/>
,	,	kIx,	,
když	když	k8xS	když
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
heliografickou	heliografický	k2eAgFnSc4d1	heliografická
kopii	kopie	k1gFnSc4	kopie
rytiny	rytina	k1gFnSc2	rytina
chlapce	chlapec	k1gMnSc4	chlapec
vedoucího	vedoucí	k1gMnSc4	vedoucí
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
považována	považován	k2eAgMnSc4d1	považován
za	za	k7c4	za
nejstarší	starý	k2eAgNnSc4d3	nejstarší
dochovaný	dochovaný	k2eAgInSc4d1	dochovaný
snímek	snímek	k1gInSc4	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
však	však	k9	však
známější	známý	k2eAgFnSc1d2	známější
jeho	jeho	k3xOp3gFnSc1	jeho
fotografie	fotografie	k1gFnSc1	fotografie
Pohled	pohled	k1gInSc4	pohled
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
v	v	k7c6	v
Le	Le	k1gFnSc6	Le
Gras	Grasa	k1gFnPc2	Grasa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejstarší	starý	k2eAgInSc4d3	nejstarší
dochovaný	dochovaný	k2eAgInSc4d1	dochovaný
snímek	snímek	k1gInSc4	snímek
venkovní	venkovní	k2eAgFnSc2d1	venkovní
krajiny	krajina	k1gFnSc2	krajina
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
a	a	k8xC	a
Niépce	Niépec	k1gInPc1	Niépec
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vytvoření	vytvoření	k1gNnSc3	vytvoření
použil	použít	k5eAaPmAgInS	použít
cínovou	cínový	k2eAgFnSc4d1	cínová
desku	deska	k1gFnSc4	deska
pokrytou	pokrytý	k2eAgFnSc4d1	pokrytá
petrolejovým	petrolejový	k2eAgInSc7d1	petrolejový
roztokem	roztok	k1gInSc7	roztok
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
čas	čas	k1gInSc1	čas
expozice	expozice	k1gFnSc2	expozice
byl	být	k5eAaImAgInS	být
celých	celý	k2eAgFnPc2d1	celá
osm	osm	k4xCc1	osm
hodin	hodina	k1gFnPc2	hodina
za	za	k7c2	za
slunného	slunný	k2eAgInSc2d1	slunný
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zdlouhavý	zdlouhavý	k2eAgInSc1d1	zdlouhavý
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
být	být	k5eAaImF	být
slepou	slepý	k2eAgFnSc7d1	slepá
uličkou	ulička	k1gFnSc7	ulička
a	a	k8xC	a
Niépce	Niépec	k1gMnSc2	Niépec
začal	začít	k5eAaPmAgMnS	začít
experimentovat	experimentovat	k5eAaImF	experimentovat
se	s	k7c7	s
sloučeninami	sloučenina	k1gFnPc7	sloučenina
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
poznatků	poznatek	k1gInPc2	poznatek
Johanna	Johann	k1gMnSc2	Johann
Heinricha	Heinrich	k1gMnSc2	Heinrich
Schultze	Schultze	k1gFnSc2	Schultze
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
směs	směs	k1gFnSc1	směs
křídy	křída	k1gFnSc2	křída
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
tmavne	tmavnout	k5eAaImIp3nS	tmavnout
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
osvětlena	osvětlit	k5eAaPmNgFnS	osvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Niépce	Niépce	k1gFnSc1	Niépce
a	a	k8xC	a
umělec	umělec	k1gMnSc1	umělec
Louis	Louis	k1gMnSc1	Louis
Daguerre	Daguerr	k1gInSc5	Daguerr
společně	společně	k6eAd1	společně
zdokonalili	zdokonalit	k5eAaPmAgMnP	zdokonalit
existující	existující	k2eAgInSc4d1	existující
proces	proces	k1gInSc4	proces
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1833	[number]	k4	1833
Niépce	Niépce	k1gFnSc2	Niépce
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
své	svůj	k3xOyFgFnPc4	svůj
poznámky	poznámka	k1gFnPc4	poznámka
Daguerrovi	Daguerrův	k2eAgMnPc1d1	Daguerrův
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
neměl	mít	k5eNaImAgMnS	mít
příliš	příliš	k6eAd1	příliš
zkušeností	zkušenost	k1gFnPc2	zkušenost
s	s	k7c7	s
vědou	věda	k1gFnSc7	věda
<g/>
,	,	kIx,	,
učinil	učinit	k5eAaPmAgMnS	učinit
dva	dva	k4xCgInPc4	dva
klíčové	klíčový	k2eAgInPc4d1	klíčový
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
stříbro	stříbro	k1gNnSc1	stříbro
nejprve	nejprve	k6eAd1	nejprve
vystaví	vystavit	k5eAaPmIp3nS	vystavit
jódovým	jódový	k2eAgFnPc3d1	jódová
parám	para	k1gFnPc3	para
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
snímek	snímek	k1gInSc1	snímek
exponuje	exponovat	k5eAaImIp3nS	exponovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
nechá	nechat	k5eAaPmIp3nS	nechat
působit	působit	k5eAaImF	působit
rtuťové	rtuťový	k2eAgInPc4d1	rtuťový
výpary	výpar	k1gInPc4	výpar
<g/>
,	,	kIx,	,
získá	získat	k5eAaPmIp3nS	získat
viditelný	viditelný	k2eAgInSc1d1	viditelný
a	a	k8xC	a
nestálý	stálý	k2eNgInSc1d1	nestálý
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
ustálit	ustálit	k5eAaPmF	ustálit
ponořením	ponoření	k1gNnSc7	ponoření
desky	deska	k1gFnSc2	deska
do	do	k7c2	do
solné	solný	k2eAgFnSc2d1	solná
lázně	lázeň	k1gFnSc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnohahodinových	mnohahodinový	k2eAgFnPc2d1	mnohahodinová
expozic	expozice	k1gFnPc2	expozice
stačily	stačit	k5eAaBmAgFnP	stačit
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
novém	nový	k2eAgInSc6d1	nový
postupu	postup	k1gInSc6	postup
minuty	minuta	k1gFnSc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
Daguerre	Daguerr	k1gInSc5	Daguerr
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
fotografem	fotograf	k1gMnSc7	fotograf
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgMnSc2	jenž
lze	lze	k6eAd1	lze
doložit	doložit	k5eAaPmF	doložit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pořídil	pořídit	k5eAaPmAgMnS	pořídit
snímek	snímek	k1gInSc4	snímek
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
<g/>
,	,	kIx,	,
když	když	k8xS	když
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
ulice	ulice	k1gFnPc4	ulice
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rušném	rušný	k2eAgInSc6d1	rušný
bulváru	bulvár	k1gInSc6	bulvár
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
pěšími	pěší	k1gMnPc7	pěší
a	a	k8xC	a
povozy	povoz	k1gInPc1	povoz
taženými	tažený	k2eAgMnPc7d1	tažený
koňmi	kůň	k1gMnPc7	kůň
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
chodec	chodec	k1gMnSc1	chodec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
nechává	nechávat	k5eAaImIp3nS	nechávat
naleštit	naleštit	k5eAaPmF	naleštit
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgMnPc1d1	ostatní
lidé	člověk	k1gMnPc1	člověk
nejsou	být	k5eNaImIp3nP	být
při	při	k7c6	při
tak	tak	k6eAd1	tak
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
době	doba	k1gFnSc6	doba
expozice	expozice	k1gFnSc2	expozice
vůbec	vůbec	k9	vůbec
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc4	tento
člověk	člověk	k1gMnSc1	člověk
stál	stát	k5eAaImAgMnS	stát
dostatečně	dostatečně	k6eAd1	dostatečně
nehybně	hybně	k6eNd1	hybně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
jediný	jediný	k2eAgInSc1d1	jediný
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
zdánlivě	zdánlivě	k6eAd1	zdánlivě
opuštěné	opuštěný	k2eAgFnSc2d1	opuštěná
ulice	ulice	k1gFnSc2	ulice
zachycen	zachytit	k5eAaPmNgMnS	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
Daguerre	Daguerr	k1gInSc5	Daguerr
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgInS	objevit
proces	proces	k1gInSc1	proces
využívající	využívající	k2eAgFnSc4d1	využívající
postříbřenou	postříbřený	k2eAgFnSc4d1	postříbřená
měděnou	měděný	k2eAgFnSc4d1	měděná
desku	deska	k1gFnSc4	deska
<g/>
,	,	kIx,	,
a	a	k8xC	a
nazval	nazvat	k5eAaPmAgMnS	nazvat
jej	on	k3xPp3gMnSc4	on
daguerrotypie	daguerrotypie	k1gFnSc1	daguerrotypie
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
proces	proces	k1gInSc4	proces
dodnes	dodnes	k6eAd1	dodnes
využívají	využívat	k5eAaPmIp3nP	využívat
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
Polaroid	polaroid	k1gInSc1	polaroid
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
patent	patent	k1gInSc4	patent
koupila	koupit	k5eAaPmAgFnS	koupit
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
jej	on	k3xPp3gMnSc4	on
dala	dát	k5eAaPmAgFnS	dát
k	k	k7c3	k
volnému	volný	k2eAgNnSc3d1	volné
užití	užití	k1gNnSc3	užití
(	(	kIx(	(
<g/>
public	publicum	k1gNnPc2	publicum
domain	domaina	k1gFnPc2	domaina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
obor	obor	k1gInSc4	obor
měla	mít	k5eAaImAgFnS	mít
značný	značný	k2eAgInSc4d1	značný
přínos	přínos	k1gInSc4	přínos
i	i	k9	i
práce	práce	k1gFnSc1	práce
britského	britský	k2eAgMnSc2d1	britský
chemika	chemik	k1gMnSc2	chemik
Johna	John	k1gMnSc2	John
Herschela	Herschel	k1gMnSc2	Herschel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
použil	použít	k5eAaPmAgMnS	použít
výrazy	výraz	k1gInPc4	výraz
"	"	kIx"	"
<g/>
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
negativ	negativ	k1gInSc1	negativ
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
pozitiv	pozitiv	k1gInSc1	pozitiv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Herschel	Herschel	k1gMnSc1	Herschel
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
kyanotypii	kyanotypie	k1gFnSc4	kyanotypie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známou	známý	k2eAgFnSc4d1	známá
též	též	k6eAd1	též
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
modrotisk	modrotisk	k1gInSc1	modrotisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1819	[number]	k4	1819
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
thiosíran	thiosíran	k1gInSc1	thiosíran
sodný	sodný	k2eAgInSc1d1	sodný
je	být	k5eAaImIp3nS	být
rozpouštědlem	rozpouštědlo	k1gNnSc7	rozpouštědlo
halogenidů	halogenid	k1gMnPc2	halogenid
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Williamu	William	k1gInSc3	William
Foxu	fox	k1gInSc2	fox
Talbotovi	Talbotův	k2eAgMnPc1d1	Talbotův
(	(	kIx(	(
<g/>
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
i	i	k9	i
Daguerrovi	Daguerrův	k2eAgMnPc1d1	Daguerrův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
tohoto	tento	k3xDgInSc2	tento
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
využito	využit	k2eAgNnSc1d1	využito
k	k	k7c3	k
ustálení	ustálení	k1gNnSc3	ustálení
fotografií	fotografia	k1gFnPc2	fotografia
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
halogenidech	halogenid	k1gInPc6	halogenid
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
učinit	učinit	k5eAaImF	učinit
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
světluodolné	světluodolný	k2eAgNnSc1d1	světluodolný
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
také	také	k9	také
udělal	udělat	k5eAaPmAgInS	udělat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
negativ	negativ	k1gInSc4	negativ
na	na	k7c4	na
skleněnou	skleněný	k2eAgFnSc4d1	skleněná
fotografickou	fotografický	k2eAgFnSc4d1	fotografická
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Fox	fox	k1gInSc1	fox
Talbot	Talbota	k1gFnPc2	Talbota
objevil	objevit	k5eAaPmAgInS	objevit
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ustálit	ustálit	k5eAaPmF	ustálit
obraz	obraz	k1gInSc4	obraz
získaný	získaný	k2eAgInSc4d1	získaný
pomocí	pomocí	k7c2	pomocí
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
expozice	expozice	k1gFnSc2	expozice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
udržoval	udržovat	k5eAaImAgMnS	udržovat
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
četl	číst	k5eAaImAgMnS	číst
o	o	k7c6	o
Daguerrově	Daguerrův	k2eAgInSc6d1	Daguerrův
vynálezu	vynález	k1gInSc6	vynález
<g/>
,	,	kIx,	,
zdokonalil	zdokonalit	k5eAaPmAgInS	zdokonalit
Talbot	Talbot	k1gInSc1	Talbot
svůj	svůj	k3xOyFgInSc4	svůj
proces	proces	k1gInSc4	proces
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
dostatečně	dostatečně	k6eAd1	dostatečně
rychlý	rychlý	k2eAgInSc1d1	rychlý
a	a	k8xC	a
citlivý	citlivý	k2eAgInSc1d1	citlivý
pro	pro	k7c4	pro
snímání	snímání	k1gNnSc4	snímání
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
oznámil	oznámit	k5eAaPmAgMnS	oznámit
vynález	vynález	k1gInSc4	vynález
kalotypie	kalotypie	k1gFnSc2	kalotypie
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc4	lista
papíru	papír	k1gInSc2	papír
potáhl	potáhnout	k5eAaPmAgMnS	potáhnout
vrstvou	vrstva	k1gFnSc7	vrstva
chloridu	chlorid	k1gInSc2	chlorid
stříbrného	stříbrný	k1gInSc2	stříbrný
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
okamžitého	okamžitý	k2eAgInSc2d1	okamžitý
negativního	negativní	k2eAgInSc2d1	negativní
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
libovolného	libovolný	k2eAgNnSc2d1	libovolné
množství	množství	k1gNnSc2	množství
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
i	i	k8xC	i
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
běžnému	běžný	k2eAgInSc3d1	běžný
negativnímu	negativní	k2eAgInSc3d1	negativní
procesu	proces	k1gInSc3	proces
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
Talbotův	Talbotův	k2eAgInSc1d1	Talbotův
malý	malý	k2eAgInSc1d1	malý
papírový	papírový	k2eAgInSc1d1	papírový
negativ	negativ	k1gInSc1	negativ
arkýřového	arkýřový	k2eAgNnSc2d1	arkýřové
okna	okno	k1gNnSc2	okno
v	v	k7c4	v
Lacock	Lacock	k1gInSc4	Lacock
Abbey	Abbea	k1gMnSc2	Abbea
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
figuruje	figurovat	k5eAaImIp3nS	figurovat
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
fotografií	fotografia	k1gFnPc2	fotografia
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
Talbotem	Talbot	k1gInSc7	Talbot
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
úplně	úplně	k6eAd1	úplně
nejstarší	starý	k2eAgInSc4d3	nejstarší
existující	existující	k2eAgInSc4d1	existující
fotografický	fotografický	k2eAgInSc4d1	fotografický
negativ	negativ	k1gInSc4	negativ
<g/>
.	.	kIx.	.
</s>
<s>
Talbot	Talbot	k1gInSc4	Talbot
si	se	k3xPyFc3	se
proces	proces	k1gInSc4	proces
patentoval	patentovat	k5eAaBmAgInS	patentovat
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
značně	značně	k6eAd1	značně
omezil	omezit	k5eAaPmAgInS	omezit
jeho	jeho	k3xOp3gFnSc4	jeho
používanost	používanost	k1gFnSc4	používanost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
pak	pak	k6eAd1	pak
soudní	soudní	k2eAgFnSc7d1	soudní
cestou	cesta	k1gFnSc7	cesta
svůj	svůj	k3xOyFgInSc4	svůj
patent	patent	k1gInSc4	patent
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
fotografie	fotografia	k1gFnSc2	fotografia
zanechal	zanechat	k5eAaPmAgMnS	zanechat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
však	však	k9	však
Talbotův	Talbotův	k2eAgInSc1d1	Talbotův
proces	proces	k1gInSc1	proces
zdokonalen	zdokonalen	k2eAgInSc1d1	zdokonalen
Georgem	Georg	k1gMnSc7	Georg
Eastmanem	Eastman	k1gMnSc7	Eastman
a	a	k8xC	a
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zdokonalené	zdokonalený	k2eAgFnSc6d1	zdokonalená
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Hippolyte	Hippolyt	k1gInSc5	Hippolyt
Bayard	Bayarda	k1gFnPc2	Bayarda
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
fotografovat	fotografovat	k5eAaImF	fotografovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
počítán	počítán	k2eAgMnSc1d1	počítán
mezi	mezi	k7c4	mezi
objevitele	objevitel	k1gMnSc4	objevitel
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
s	s	k7c7	s
oznámením	oznámení	k1gNnSc7	oznámení
vynálezu	vynález	k1gInSc2	vynález
příliš	příliš	k6eAd1	příliš
zpozdil	zpozdit	k5eAaPmAgMnS	zpozdit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1851	[number]	k4	1851
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Frederick	Frederick	k1gMnSc1	Frederick
Scott	Scott	k1gMnSc1	Scott
Archer	Archra	k1gFnPc2	Archra
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
The	The	k1gFnSc1	The
Chemist	Chemist	k1gInSc1	Chemist
článek	článek	k1gInSc1	článek
o	o	k7c6	o
mokrém	mokrý	k2eAgInSc6d1	mokrý
kolodiovém	kolodiový	k2eAgInSc6d1	kolodiový
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgFnPc1	tři
podskupiny	podskupina	k1gFnPc1	podskupina
kolodiového	kolodiový	k2eAgInSc2d1	kolodiový
procesu	proces	k1gInSc2	proces
<g/>
:	:	kIx,	:
ambrotypie	ambrotypie	k1gFnSc1	ambrotypie
(	(	kIx(	(
<g/>
pozitiv	pozitiv	k1gInSc1	pozitiv
na	na	k7c6	na
skle	sklo	k1gNnSc6	sklo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ferrotypie	ferrotypie	k1gFnSc1	ferrotypie
(	(	kIx(	(
<g/>
pozitiv	pozitiv	k1gMnSc1	pozitiv
na	na	k7c6	na
plechové	plechový	k2eAgFnSc6d1	plechová
desce	deska	k1gFnSc6	deska
<g/>
)	)	kIx)	)
a	a	k8xC	a
pannotypie	pannotypie	k1gFnSc1	pannotypie
(	(	kIx(	(
<g/>
negativ	negativ	k1gInSc1	negativ
na	na	k7c6	na
skle	sklo	k1gNnSc6	sklo
<g/>
)	)	kIx)	)
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
suchého	suchý	k2eAgInSc2d1	suchý
želatinového	želatinový	k2eAgInSc2d1	želatinový
filmu	film	k1gInSc2	film
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Archerova	Archerův	k2eAgFnSc1d1	Archerova
deska	deska	k1gFnSc1	deska
nejpoužívanějším	používaný	k2eAgNnSc7d3	nejpoužívanější
fotografickým	fotografický	k2eAgNnSc7d1	fotografické
médiem	médium	k1gNnSc7	médium
<g/>
.	.	kIx.	.
</s>
<s>
Využíval	využívat	k5eAaPmAgMnS	využívat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
i	i	k8xC	i
spisovatel	spisovatel	k1gMnSc1	spisovatel
Lewis	Lewis	k1gFnPc2	Lewis
Carroll	Carroll	k1gMnSc1	Carroll
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
věnoval	věnovat	k5eAaImAgInS	věnovat
fotografii	fotografia	k1gFnSc4	fotografia
poměrně	poměrně	k6eAd1	poměrně
intenzivně	intenzivně	k6eAd1	intenzivně
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
činěna	činěn	k2eAgFnSc1d1	činěna
řada	řada	k1gFnSc1	řada
pokroků	pokrok	k1gInPc2	pokrok
ve	v	k7c6	v
fotografických	fotografický	k2eAgFnPc6d1	fotografická
skleněných	skleněný	k2eAgFnPc6d1	skleněná
deskách	deska	k1gFnPc6	deska
a	a	k8xC	a
tisku	tisk	k1gInSc6	tisk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
představil	představit	k5eAaPmAgMnS	představit
Gabriel	Gabriel	k1gMnSc1	Gabriel
Lippmann	Lippmann	k1gMnSc1	Lippmann
proces	proces	k1gInSc4	proces
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
fotografií	fotografia	k1gFnPc2	fotografia
v	v	k7c6	v
přirozených	přirozený	k2eAgFnPc6d1	přirozená
barvách	barva	k1gFnPc6	barva
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
optickém	optický	k2eAgNnSc6d1	optické
fenoménu	fenomén	k1gNnSc6	fenomén
interference	interference	k1gFnSc1	interference
světelných	světelný	k2eAgFnPc2d1	světelná
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vynález	vynález	k1gInSc1	vynález
sice	sice	k8xC	sice
nebyl	být	k5eNaImAgInS	být
praktický	praktický	k2eAgInSc1d1	praktický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
vědeckého	vědecký	k2eAgNnSc2d1	vědecké
hlediska	hledisko	k1gNnSc2	hledisko
byl	být	k5eAaImAgInS	být
natolik	natolik	k6eAd1	natolik
elegantní	elegantní	k2eAgInSc1d1	elegantní
a	a	k8xC	a
důležitý	důležitý	k2eAgInSc1d1	důležitý
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
Lippman	Lippman	k1gMnSc1	Lippman
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
skleněné	skleněný	k2eAgFnPc1d1	skleněná
fotografické	fotografický	k2eAgFnPc1d1	fotografická
desky	deska	k1gFnPc1	deska
nejpoužívanějším	používaný	k2eAgNnSc7d3	nejpoužívanější
médiem	médium	k1gNnSc7	médium
ve	v	k7c6	v
fotografii	fotografia	k1gFnSc6	fotografia
a	a	k8xC	a
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
jím	on	k3xPp3gNnSc7	on
až	až	k9	až
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
používán	používat	k5eAaImNgInS	používat
ohebný	ohebný	k2eAgInSc1d1	ohebný
plastický	plastický	k2eAgInSc1d1	plastický
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
řadu	řada	k1gFnSc4	řada
předností	přednost	k1gFnPc2	přednost
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
pomohl	pomoct	k5eAaPmAgMnS	pomoct
popularizaci	popularizace	k1gFnSc4	popularizace
amatérské	amatérský	k2eAgFnSc2d1	amatérská
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
filmy	film	k1gInPc1	film
však	však	k9	však
byly	být	k5eAaImAgInP	být
poměrně	poměrně	k6eAd1	poměrně
drahé	drahý	k2eAgInPc1d1	drahý
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jejich	jejich	k3xOp3gFnSc1	jejich
optická	optický	k2eAgFnSc1d1	optická
kvalita	kvalita	k1gFnSc1	kvalita
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
skleněnými	skleněný	k2eAgFnPc7d1	skleněná
deskami	deska	k1gFnPc7	deska
znatelně	znatelně	k6eAd1	znatelně
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
ani	ani	k9	ani
nebyly	být	k5eNaImAgInP	být
plastické	plastický	k2eAgInPc1d1	plastický
filmy	film	k1gInPc1	film
dostupné	dostupný	k2eAgInPc1d1	dostupný
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
formátech	formát	k1gInPc6	formát
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
preferovala	preferovat	k5eAaImAgFnS	preferovat
většina	většina	k1gFnSc1	většina
profesionálů	profesionál	k1gMnPc2	profesionál
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
tedy	tedy	k9	tedy
nenahradil	nahradit	k5eNaPmAgInS	nahradit
skleněné	skleněný	k2eAgFnPc4d1	skleněná
desky	deska	k1gFnPc4	deska
ze	z	k7c2	z
dne	den	k1gInSc2	den
na	na	k7c4	na
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
lepší	dobrý	k2eAgFnSc3d2	lepší
permanenci	permanence	k1gFnSc3	permanence
(	(	kIx(	(
<g/>
schopnosti	schopnost	k1gFnSc3	schopnost
zachování	zachování	k1gNnSc2	zachování
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
)	)	kIx)	)
skla	sklo	k1gNnSc2	sklo
se	se	k3xPyFc4	se
skleněné	skleněný	k2eAgFnPc1d1	skleněná
desky	deska	k1gFnPc1	deska
používaly	používat	k5eAaImAgFnP	používat
k	k	k7c3	k
vědeckým	vědecký	k2eAgInPc3d1	vědecký
účelů	účel	k1gInPc2	účel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
astrofotografii	astrofotografie	k1gFnSc6	astrofotografie
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
v	v	k7c6	v
úzce	úzko	k6eAd1	úzko
specializovaném	specializovaný	k2eAgInSc6d1	specializovaný
oboru	obor	k1gInSc6	obor
laserové	laserový	k2eAgFnPc1d1	laserová
holografie	holografie	k1gFnPc1	holografie
si	se	k3xPyFc3	se
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
ještě	ještě	k6eAd1	ještě
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
započali	započnout	k5eAaPmAgMnP	započnout
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Hurter	Hurter	k1gMnSc1	Hurter
a	a	k8xC	a
Vero	Vero	k1gMnSc1	Vero
Charles	Charles	k1gMnSc1	Charles
Driffield	Driffield	k1gMnSc1	Driffield
průkopnickou	průkopnický	k2eAgFnSc4d1	průkopnická
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fotografických	fotografický	k2eAgFnPc2d1	fotografická
emulzí	emulze	k1gFnPc2	emulze
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
citlivosti	citlivost	k1gFnSc2	citlivost
vůči	vůči	k7c3	vůči
světlu	světlo	k1gNnSc3	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
Hurtera	Hurter	k1gMnSc2	Hurter
a	a	k8xC	a
Driffielda	Driffield	k1gMnSc2	Driffield
umožnila	umožnit	k5eAaPmAgFnS	umožnit
navržení	navržení	k1gNnSc1	navržení
prvního	první	k4xOgNnSc2	první
kvantitativního	kvantitativní	k2eAgNnSc2d1	kvantitativní
měření	měření	k1gNnSc2	měření
citlivosti	citlivost	k1gFnSc2	citlivost
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
George	George	k1gFnSc4	George
Eastman	Eastman	k1gMnSc1	Eastman
první	první	k4xOgInSc4	první
fotografický	fotografický	k2eAgInSc4d1	fotografický
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zbavil	zbavit	k5eAaPmAgMnS	zbavit
fotografy	fotograf	k1gMnPc4	fotograf
nutnosti	nutnost	k1gFnSc2	nutnost
nosit	nosit	k5eAaImF	nosit
s	s	k7c7	s
sebou	se	k3xPyFc7	se
těžké	těžký	k2eAgNnSc4d1	těžké
skleněné	skleněný	k2eAgFnPc4d1	skleněná
fotografické	fotografický	k2eAgFnPc4d1	fotografická
desky	deska	k1gFnPc4	deska
a	a	k8xC	a
jedovaté	jedovatý	k2eAgFnPc4d1	jedovatá
chemikálie	chemikálie	k1gFnPc4	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
první	první	k4xOgInSc1	první
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
papírový	papírový	k2eAgInSc1d1	papírový
a	a	k8xC	a
potažený	potažený	k2eAgInSc1d1	potažený
citlivou	citlivý	k2eAgFnSc7d1	citlivá
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
snímku	snímek	k1gInSc2	snímek
oddělila	oddělit	k5eAaPmAgFnS	oddělit
od	od	k7c2	od
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
přenesla	přenést	k5eAaPmAgFnS	přenést
na	na	k7c4	na
suché	suchý	k2eAgFnPc4d1	suchá
želatinové	želatinový	k2eAgFnPc4d1	želatinová
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
uvedla	uvést	k5eAaPmAgFnS	uvést
Eastmanova	Eastmanův	k2eAgFnSc1d1	Eastmanův
firma	firma	k1gFnSc1	firma
Kodak	Kodak	kA	Kodak
první	první	k4xOgInSc4	první
filmový	filmový	k2eAgInSc4d1	filmový
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
první	první	k4xOgInSc1	první
plastový	plastový	k2eAgInSc1d1	plastový
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
z	z	k7c2	z
vysoce	vysoce	k6eAd1	vysoce
hořlavého	hořlavý	k2eAgInSc2d1	hořlavý
nitrátu	nitrát	k1gInSc2	nitrát
celulózy	celulóza	k1gFnSc2	celulóza
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k6eAd1	právě
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
hořlavosti	hořlavost	k1gFnSc2	hořlavost
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
firmou	firma	k1gFnSc7	firma
Kodak	Kodak	kA	Kodak
představen	představit	k5eAaPmNgInS	představit
acetátcelulózový	acetátcelulózový	k2eAgInSc1d1	acetátcelulózový
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
našel	najít	k5eAaPmAgInS	najít
jenom	jenom	k9	jenom
omezené	omezený	k2eAgNnSc1d1	omezené
využití	využití	k1gNnSc1	využití
jako	jako	k8xC	jako
náhrada	náhrada	k1gFnSc1	náhrada
nebezpečného	bezpečný	k2eNgInSc2d1	nebezpečný
nitrátového	nitrátový	k2eAgInSc2d1	nitrátový
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
oproti	oproti	k7c3	oproti
němu	on	k3xPp3gMnSc3	on
odolnější	odolný	k2eAgNnSc1d2	odolnější
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
průsvitnější	průsvitný	k2eAgFnPc1d2	průsvitnější
a	a	k8xC	a
levnější	levný	k2eAgFnPc1d2	levnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
rentgenových	rentgenový	k2eAgInPc2d1	rentgenový
filmů	film	k1gInPc2	film
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úplnému	úplný	k2eAgInSc3d1	úplný
přechodu	přechod	k1gInSc3	přechod
na	na	k7c4	na
acetátocelulózový	acetátocelulózový	k2eAgInSc4d1	acetátocelulózový
film	film	k1gInSc4	film
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
acetátcelulózový	acetátcelulózový	k2eAgInSc1d1	acetátcelulózový
film	film	k1gInSc1	film
vždy	vždy	k6eAd1	vždy
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
16	[number]	k4	16
<g/>
mm	mm	kA	mm
a	a	k8xC	a
8	[number]	k4	8
<g/>
mm	mm	kA	mm
amatérské	amatérský	k2eAgInPc4d1	amatérský
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
nitrátový	nitrátový	k2eAgInSc1d1	nitrátový
film	film	k1gInSc1	film
zůstal	zůstat	k5eAaPmAgInS	zůstat
standardem	standard	k1gInSc7	standard
pro	pro	k7c4	pro
35	[number]	k4	35
<g/>
mm	mm	kA	mm
film	film	k1gInSc1	film
až	až	k9	až
do	do	k7c2	do
ukončení	ukončení	k1gNnSc2	ukončení
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejznámějším	známý	k2eAgMnSc7d3	nejznámější
zástupcem	zástupce	k1gMnSc7	zástupce
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
používajících	používající	k2eAgInPc2d1	používající
právě	právě	k6eAd1	právě
35	[number]	k4	35
<g/>
mm	mm	kA	mm
film	film	k1gInSc4	film
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Leica	Leica	k1gFnSc1	Leica
<g/>
,	,	kIx,	,
na	na	k7c4	na
trh	trh	k1gInSc4	trh
poprvé	poprvé	k6eAd1	poprvé
uvedená	uvedený	k2eAgFnSc1d1	uvedená
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc4	film
si	se	k3xPyFc3	se
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
fotografie	fotografia	k1gFnSc2	fotografia
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
dominantní	dominantní	k2eAgNnSc4d1	dominantní
postavení	postavení	k1gNnSc4	postavení
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vedly	vést	k5eAaImAgInP	vést
pokroky	pokrok	k1gInPc1	pokrok
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
fotografii	fotografia	k1gFnSc6	fotografia
konzumenty	konzument	k1gMnPc4	konzument
k	k	k7c3	k
odklonu	odklon	k1gInSc3	odklon
od	od	k7c2	od
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
fotografii	fotografia	k1gFnSc6	fotografia
tvoří	tvořit	k5eAaImIp3nP	tvořit
většinu	většina	k1gFnSc4	většina
zastánci	zastánce	k1gMnPc1	zastánce
digitálních	digitální	k2eAgFnPc2d1	digitální
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
používán	používat	k5eAaImNgInS	používat
jak	jak	k6eAd1	jak
nadšenci	nadšenec	k1gMnSc3	nadšenec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
profesionálními	profesionální	k2eAgMnPc7d1	profesionální
fotografy	fotograf	k1gMnPc7	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Jedinečný	jedinečný	k2eAgInSc4d1	jedinečný
vzhled	vzhled	k1gInSc4	vzhled
filmových	filmový	k2eAgFnPc2d1	filmová
fotografií	fotografia	k1gFnPc2	fotografia
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
fotografiemi	fotografia	k1gFnPc7	fotografia
digitálními	digitální	k2eAgFnPc7d1	digitální
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dán	dát	k5eAaPmNgInS	dát
kombinací	kombinace	k1gFnSc7	kombinace
různých	různý	k2eAgInPc2d1	různý
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
rozdílu	rozdíl	k1gInSc2	rozdíl
ve	v	k7c6	v
spektrální	spektrální	k2eAgFnSc6d1	spektrální
a	a	k8xC	a
tonální	tonální	k2eAgFnSc6d1	tonální
citlivosti	citlivost	k1gFnSc6	citlivost
<g/>
,	,	kIx,	,
rozlišení	rozlišení	k1gNnSc6	rozlišení
a	a	k8xC	a
barevném	barevný	k2eAgInSc6d1	barevný
přechodu	přechod	k1gInSc6	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Černobílá	černobílý	k2eAgFnSc1d1	černobílá
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
fotografie	fotografia	k1gFnPc1	fotografia
byly	být	k5eAaImAgFnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
monochromatické	monochromatický	k2eAgFnPc1d1	monochromatická
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
monochromatický	monochromatický	k2eAgInSc1d1	monochromatický
efekt	efekt	k1gInSc1	efekt
není	být	k5eNaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
jenom	jenom	k9	jenom
na	na	k7c4	na
bílou	bílý	k2eAgFnSc4d1	bílá
<g/>
,	,	kIx,	,
černou	černý	k2eAgFnSc4d1	černá
a	a	k8xC	a
přechodné	přechodný	k2eAgFnSc2d1	přechodná
šedé	šedý	k2eAgFnSc2d1	šedá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
procesu	proces	k1gInSc6	proces
může	moct	k5eAaImIp3nS	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
také	také	k6eAd1	také
odstíny	odstín	k1gInPc4	odstín
jedné	jeden	k4xCgFnSc2	jeden
určité	určitý	k2eAgFnSc2d1	určitá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
výsledkem	výsledek	k1gInSc7	výsledek
kyanotypie	kyanotypie	k1gFnSc2	kyanotypie
je	být	k5eAaImIp3nS	být
snímek	snímek	k1gInSc1	snímek
vykreslený	vykreslený	k2eAgInSc1d1	vykreslený
v	v	k7c6	v
modrých	modrý	k2eAgInPc6d1	modrý
odstínech	odstín	k1gInPc6	odstín
a	a	k8xC	a
pro	pro	k7c4	pro
albuminový	albuminový	k2eAgInSc4d1	albuminový
tisk	tisk	k1gInSc4	tisk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
před	před	k7c7	před
160	[number]	k4	160
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
odstíny	odstín	k1gInPc1	odstín
hnědé	hnědý	k2eAgInPc1d1	hnědý
<g/>
.	.	kIx.	.
</s>
<s>
Černobílá	černobílý	k2eAgFnSc1d1	černobílá
fotografie	fotografie	k1gFnSc1	fotografie
zůstala	zůstat	k5eAaPmAgFnS	zůstat
dominantní	dominantní	k2eAgFnSc1d1	dominantní
i	i	k9	i
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
byl	být	k5eAaImAgInS	být
barevný	barevný	k2eAgInSc1d1	barevný
film	film	k1gInSc1	film
již	již	k6eAd1	již
desetiletí	desetiletí	k1gNnSc4	desetiletí
snadno	snadno	k6eAd1	snadno
dostupný	dostupný	k2eAgInSc1d1	dostupný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
levnější	levný	k2eAgFnSc1d2	levnější
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
fotografii	fotografia	k1gFnSc4	fotografia
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
barevná	barevný	k2eAgFnSc1d1	barevná
fotografie	fotografie	k1gFnSc1	fotografie
převládá	převládat	k5eAaImIp3nS	převládat
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
fotografů	fotograf	k1gMnPc2	fotograf
stále	stále	k6eAd1	stále
monochromatické	monochromatický	k2eAgInPc4d1	monochromatický
snímky	snímek	k1gInPc4	snímek
aspoň	aspoň	k9	aspoň
někdy	někdy	k6eAd1	někdy
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prokázaná	prokázaný	k2eAgFnSc1d1	prokázaná
stálost	stálost	k1gFnSc1	stálost
v	v	k7c6	v
případě	případ	k1gInSc6	případ
archivace	archivace	k1gFnSc2	archivace
správně	správně	k6eAd1	správně
zpracovaných	zpracovaný	k2eAgInPc2d1	zpracovaný
materiálů	materiál	k1gInPc2	materiál
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
halogenidech	halogenid	k1gInPc6	halogenid
stříbra	stříbro	k1gNnSc2	stříbro
či	či	k8xC	či
umělecký	umělecký	k2eAgInSc4d1	umělecký
záměr	záměr	k1gInSc4	záměr
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
barevné	barevný	k2eAgInPc1d1	barevný
digitální	digitální	k2eAgInPc1d1	digitální
snímky	snímek	k1gInPc1	snímek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
různých	různý	k2eAgFnPc2d1	různá
technik	technika	k1gFnPc2	technika
zpracovány	zpracovat	k5eAaPmNgFnP	zpracovat
do	do	k7c2	do
černobílé	černobílý	k2eAgFnSc2d1	černobílá
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
monochromatický	monochromatický	k2eAgInSc4d1	monochromatický
tisk	tisk	k1gInSc4	tisk
nebo	nebo	k8xC	nebo
elektronické	elektronický	k2eAgNnSc4d1	elektronické
zobrazení	zobrazení	k1gNnSc4	zobrazení
v	v	k7c6	v
monochromatické	monochromatický	k2eAgFnSc6d1	monochromatická
podobě	podoba	k1gFnSc6	podoba
k	k	k7c3	k
záchraně	záchrana	k1gFnSc3	záchrana
barevných	barevný	k2eAgFnPc2d1	barevná
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
v	v	k7c6	v
originální	originální	k2eAgFnSc6d1	originální
podobě	podoba	k1gFnSc6	podoba
nepřijatelné	přijatelný	k2eNgFnSc3d1	nepřijatelná
–	–	k?	–
po	po	k7c6	po
převodu	převod	k1gInSc6	převod
na	na	k7c4	na
monochromatický	monochromatický	k2eAgInSc4d1	monochromatický
snímek	snímek	k1gInSc4	snímek
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
někdy	někdy	k6eAd1	někdy
považované	považovaný	k2eAgNnSc1d1	považované
za	za	k7c4	za
lepší	dobrý	k2eAgNnSc4d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Skoro	skoro	k6eAd1	skoro
všechny	všechen	k3xTgInPc1	všechen
digitální	digitální	k2eAgInPc1d1	digitální
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
nabízejí	nabízet	k5eAaImIp3nP	nabízet
možnost	možnost	k1gFnSc4	možnost
monochromatického	monochromatický	k2eAgNnSc2d1	monochromatické
focení	focení	k1gNnSc2	focení
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
všechen	všechen	k3xTgInSc1	všechen
software	software	k1gInSc1	software
pro	pro	k7c4	pro
editaci	editace	k1gFnSc4	editace
fotografií	fotografia	k1gFnPc2	fotografia
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kombinovat	kombinovat	k5eAaImF	kombinovat
nebo	nebo	k8xC	nebo
selektivně	selektivně	k6eAd1	selektivně
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
barevný	barevný	k2eAgInSc4d1	barevný
model	model	k1gInSc4	model
RGB	RGB	kA	RGB
a	a	k8xC	a
tvořit	tvořit	k5eAaImF	tvořit
monochromatické	monochromatický	k2eAgFnPc4d1	monochromatická
snímky	snímka	k1gFnPc4	snímka
z	z	k7c2	z
původně	původně	k6eAd1	původně
barevných	barevný	k2eAgFnPc2d1	barevná
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
výrobci	výrobce	k1gMnPc1	výrobce
dokonce	dokonce	k9	dokonce
prodávají	prodávat	k5eAaImIp3nP	prodávat
digitální	digitální	k2eAgInPc1d1	digitální
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
fotí	fotit	k5eAaImIp3nP	fotit
pouze	pouze	k6eAd1	pouze
monochromaticky	monochromaticky	k6eAd1	monochromaticky
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Barevná	barevný	k2eAgFnSc1d1	barevná
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
barevné	barevný	k2eAgFnSc2d1	barevná
fotografie	fotografia	k1gFnSc2	fotografia
bylo	být	k5eAaImAgNnS	být
brzděno	brzdit	k5eAaImNgNnS	brzdit
omezenou	omezený	k2eAgFnSc7d1	omezená
citlivostí	citlivost	k1gFnSc7	citlivost
raných	raný	k2eAgInPc2d1	raný
fotografických	fotografický	k2eAgInPc2d1	fotografický
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
citlivé	citlivý	k2eAgInPc1d1	citlivý
zejména	zejména	k9	zejména
na	na	k7c4	na
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
málo	málo	k6eAd1	málo
na	na	k7c4	na
zelenou	zelený	k2eAgFnSc4d1	zelená
a	a	k8xC	a
prakticky	prakticky	k6eAd1	prakticky
zcela	zcela	k6eAd1	zcela
necitlivé	citlivý	k2eNgNnSc1d1	necitlivé
na	na	k7c4	na
červenou	červená	k1gFnSc4	červená
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
princip	princip	k1gInSc1	princip
barevné	barevný	k2eAgFnSc2d1	barevná
fotografie	fotografia	k1gFnSc2	fotografia
poprvé	poprvé	k6eAd1	poprvé
předvedl	předvést	k5eAaPmAgInS	předvést
James	James	k1gInSc1	James
Clerk	Clerk	k1gInSc1	Clerk
Maxwell	maxwell	k1gInSc1	maxwell
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1861	[number]	k4	1861
<g/>
.	.	kIx.	.
</s>
<s>
Promítl	promítnout	k5eAaPmAgMnS	promítnout
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
současně	současně	k6eAd1	současně
tři	tři	k4xCgInPc4	tři
černobílé	černobílý	k2eAgInPc4d1	černobílý
snímky	snímek	k1gInPc4	snímek
barevné	barevný	k2eAgFnSc2d1	barevná
řádové	řádový	k2eAgFnSc2d1	řádová
stuhy	stuha	k1gFnSc2	stuha
přes	přes	k7c4	přes
červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
zelený	zelený	k2eAgInSc4d1	zelený
a	a	k8xC	a
modrý	modrý	k2eAgInSc4d1	modrý
filtr	filtr	k1gInSc4	filtr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
předtím	předtím	k6eAd1	předtím
exponovány	exponován	k2eAgInPc4d1	exponován
přes	přes	k7c4	přes
filtry	filtr	k1gInPc4	filtr
stejných	stejný	k2eAgFnPc2d1	stejná
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Prokázal	prokázat	k5eAaPmAgInS	prokázat
tak	tak	k9	tak
princip	princip	k1gInSc1	princip
aditivního	aditivní	k2eAgNnSc2d1	aditivní
míchání	míchání	k1gNnSc2	míchání
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
použitá	použitý	k2eAgFnSc1d1	použitá
exponovaná	exponovaný	k2eAgFnSc1d1	exponovaná
fotocitlivá	fotocitlivý	k2eAgFnSc1d1	fotocitlivá
emulze	emulze	k1gFnSc1	emulze
necitlivá	citlivý	k2eNgFnSc1d1	necitlivá
na	na	k7c4	na
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
červené	červená	k1gFnSc2	červená
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
přes	přes	k7c4	přes
červený	červený	k2eAgInSc4d1	červený
filtr	filtr	k1gInSc4	filtr
exponována	exponován	k2eAgFnSc1d1	exponována
okem	oke	k1gNnSc7	oke
neviditelná	viditelný	k2eNgFnSc1d1	neviditelná
ultrafialová	ultrafialový	k2eAgFnSc1d1	ultrafialová
část	část	k1gFnSc1	část
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
však	však	k9	však
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
komplikovanosti	komplikovanost	k1gFnSc3	komplikovanost
nepoužitelná	použitelný	k2eNgFnSc1d1	nepoužitelná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výzkumy	výzkum	k1gInPc4	výzkum
Jamese	Jamese	k1gFnSc2	Jamese
Maxwella	Maxwell	k1gMnSc2	Maxwell
navázal	navázat	k5eAaPmAgMnS	navázat
Louis	Louis	k1gMnSc1	Louis
Ducos	Ducos	k1gMnSc1	Ducos
du	du	k?	du
Hauron	Hauron	k1gMnSc1	Hauron
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
pracoval	pracovat	k5eAaImAgMnS	pracovat
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
na	na	k7c6	na
praktickém	praktický	k2eAgInSc6d1	praktický
způsob	způsob	k1gInSc1	způsob
záznamu	záznam	k1gInSc2	záznam
barevných	barevný	k2eAgFnPc2d1	barevná
fotografií	fotografia	k1gFnPc2	fotografia
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgInPc2	dva
barevných	barevný	k2eAgInPc2d1	barevný
systémů	systém	k1gInPc2	systém
<g/>
:	:	kIx,	:
subtraktivního	subtraktivní	k2eAgMnSc2d1	subtraktivní
(	(	kIx(	(
<g/>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
azurová	azurový	k2eAgFnSc1d1	azurová
<g/>
,	,	kIx,	,
purpurová	purpurový	k2eAgFnSc1d1	purpurová
<g/>
)	)	kIx)	)
a	a	k8xC	a
aditivního	aditivní	k2eAgMnSc2d1	aditivní
(	(	kIx(	(
<g/>
červená	červená	k1gFnSc1	červená
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
tyto	tento	k3xDgFnPc4	tento
metody	metoda	k1gFnPc4	metoda
patentoval	patentovat	k5eAaBmAgMnS	patentovat
<g/>
.	.	kIx.	.
</s>
<s>
Osvítil	osvítit	k5eAaPmAgInS	osvítit
bromostříbrnou	bromostříbrný	k2eAgFnSc4d1	bromostříbrný
kolodiovou	kolodiový	k2eAgFnSc4d1	kolodiová
desku	deska	k1gFnSc4	deska
výtažkovými	výtažkový	k2eAgInPc7d1	výtažkový
filtry	filtr	k1gInPc7	filtr
a	a	k8xC	a
zhotovil	zhotovit	k5eAaPmAgInS	zhotovit
tak	tak	k9	tak
diapozitivy	diapozitiv	k1gInPc4	diapozitiv
zabarvené	zabarvený	k2eAgInPc4d1	zabarvený
do	do	k7c2	do
červena	červeno	k1gNnSc2	červeno
<g/>
,	,	kIx,	,
modra	modro	k1gNnSc2	modro
a	a	k8xC	a
žluta	žluto	k1gNnSc2	žluto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
tři	tři	k4xCgFnPc1	tři
části	část	k1gFnPc1	část
pak	pak	k6eAd1	pak
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
položeny	položit	k5eAaPmNgFnP	položit
zcela	zcela	k6eAd1	zcela
přesně	přesně	k6eAd1	přesně
přes	přes	k7c4	přes
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
konečné	konečný	k2eAgFnSc2d1	konečná
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vysokým	vysoký	k2eAgInPc3d1	vysoký
nákladům	náklad	k1gInPc3	náklad
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
mnoho	mnoho	k6eAd1	mnoho
nepoužívala	používat	k5eNaImAgFnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
barevných	barevný	k2eAgFnPc2d1	barevná
fotografií	fotografia	k1gFnPc2	fotografia
je	být	k5eAaImIp3nS	být
Landscape	Landscap	k1gInSc5	Landscap
of	of	k?	of
Southern	Southern	k1gInSc4	Southern
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pořízena	pořídit	k5eAaPmNgFnS	pořídit
subtraktivní	subtraktivní	k2eAgFnSc7d1	subtraktivní
metodou	metoda	k1gFnSc7	metoda
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
Hauronem	Hauron	k1gInSc7	Hauron
objevil	objevit	k5eAaPmAgInS	objevit
podobný	podobný	k2eAgInSc1d1	podobný
systém	systém	k1gInSc1	systém
Charles	Charles	k1gMnSc1	Charles
Cros	Cros	k1gInSc1	Cros
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
byl	být	k5eAaImAgInS	být
i	i	k9	i
objev	objev	k1gInSc1	objev
fotografa	fotograf	k1gMnSc2	fotograf
a	a	k8xC	a
chemika	chemik	k1gMnSc2	chemik
Hermanna	Hermann	k1gMnSc2	Hermann
Vogela	Vogel	k1gMnSc2	Vogel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
zvýšení	zvýšení	k1gNnSc2	zvýšení
citlivosti	citlivost	k1gFnSc2	citlivost
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
červené	červený	k2eAgFnSc3d1	červená
pomocí	pomocí	k7c2	pomocí
přidání	přidání	k1gNnSc2	přidání
barviva	barvivo	k1gNnSc2	barvivo
<g/>
.	.	kIx.	.
</s>
<s>
Pokroky	pokrok	k1gInPc1	pokrok
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
látek	látka	k1gFnPc2	látka
zvyšujících	zvyšující	k2eAgFnPc2d1	zvyšující
citlivost	citlivost	k1gFnSc4	citlivost
i	i	k9	i
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
citlivosti	citlivost	k1gFnSc6	citlivost
roztoků	roztok	k1gInPc2	roztok
postupně	postupně	k6eAd1	postupně
snižovaly	snižovat	k5eAaImAgFnP	snižovat
expoziční	expoziční	k2eAgInPc4d1	expoziční
časy	čas	k1gInPc4	čas
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
bývaly	bývat	k5eAaImAgInP	bývat
pro	pro	k7c4	pro
barevnou	barevný	k2eAgFnSc4d1	barevná
fotografii	fotografia	k1gFnSc4	fotografia
příliš	příliš	k6eAd1	příliš
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
,	,	kIx,	,
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
ji	on	k3xPp3gFnSc4	on
činily	činit	k5eAaImAgInP	činit
životaschopnou	životaschopný	k2eAgFnSc4d1	životaschopná
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
vyvolal	vyvolat	k5eAaPmAgMnS	vyvolat
F.	F.	kA	F.
E.	E.	kA	E.
Ives	Ivesa	k1gFnPc2	Ivesa
tříbarevnou	tříbarevný	k2eAgFnSc4d1	tříbarevná
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
pak	pak	k6eAd1	pak
navázal	navázat	k5eAaPmAgMnS	navázat
Němec	Němec	k1gMnSc1	Němec
Adolf	Adolf	k1gMnSc1	Adolf
Miethe	Mieth	k1gMnSc2	Mieth
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
panchromatické	panchromatický	k2eAgNnSc4d1	panchromatický
zcitlivění	zcitlivění	k1gNnSc4	zcitlivění
pro	pro	k7c4	pro
reprodukci	reprodukce	k1gFnSc4	reprodukce
barevných	barevný	k2eAgInPc2d1	barevný
tónů	tón	k1gInPc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
komerčně	komerčně	k6eAd1	komerčně
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
barevným	barevný	k2eAgInSc7d1	barevný
postupem	postup	k1gInSc7	postup
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
autochrom	autochrom	k1gInSc1	autochrom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
představili	představit	k5eAaPmAgMnP	představit
bratři	bratr	k1gMnPc1	bratr
Lumiérové	Lumiérový	k2eAgFnSc2d1	Lumiérový
<g/>
.	.	kIx.	.
</s>
<s>
Autochromové	autochromový	k2eAgFnPc1d1	autochromový
desky	deska	k1gFnPc1	deska
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
filtr	filtr	k1gInSc4	filtr
tvořený	tvořený	k2eAgInSc4d1	tvořený
barevnou	barevný	k2eAgFnSc7d1	barevná
mozaikou	mozaika	k1gFnSc7	mozaika
ze	z	k7c2	z
suchých	suchý	k2eAgNnPc2d1	suché
zrnek	zrnko	k1gNnPc2	zrnko
bramborového	bramborový	k2eAgInSc2d1	bramborový
škrobu	škrob	k1gInSc2	škrob
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
barevné	barevný	k2eAgFnPc1d1	barevná
složky	složka	k1gFnPc1	složka
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgFnP	být
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
světla	světlo	k1gNnSc2	světlo
zachyceny	zachycen	k2eAgFnPc1d1	zachycena
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
mikroskopických	mikroskopický	k2eAgFnPc2d1	mikroskopická
částeček	částečka	k1gFnPc2	částečka
snímku	snímek	k1gInSc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zpracování	zpracování	k1gNnSc6	zpracování
autochromové	autochromový	k2eAgFnSc2d1	autochromový
desky	deska	k1gFnSc2	deska
na	na	k7c4	na
pozitiv	pozitiv	k1gInSc4	pozitiv
dodala	dodat	k5eAaPmAgFnS	dodat
zrnka	zrnko	k1gNnPc1	zrnko
škrobu	škrob	k1gInSc6	škrob
každé	každý	k3xTgFnSc3	každý
částečce	částečka	k1gFnSc3	částečka
snímku	snímek	k1gInSc2	snímek
správnou	správný	k2eAgFnSc4d1	správná
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
moderní	moderní	k2eAgInSc1d1	moderní
inverzní	inverzní	k2eAgInSc1d1	inverzní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Kodachrome	Kodachrom	k1gInSc5	Kodachrom
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
představen	představen	k2eAgInSc1d1	představen
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
mnohovrstvé	mnohovrstvý	k2eAgFnSc2d1	mnohovrstvý
emulze	emulze	k1gFnSc2	emulze
zachycoval	zachycovat	k5eAaImAgMnS	zachycovat
Kodachrome	Kodachrom	k1gInSc5	Kodachrom
všechny	všechen	k3xTgFnPc4	všechen
tři	tři	k4xCgFnPc4	tři
barevné	barevný	k2eAgFnPc4d1	barevná
složky	složka	k1gFnPc4	složka
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
vrstva	vrstva	k1gFnSc1	vrstva
byla	být	k5eAaImAgFnS	být
citlivá	citlivý	k2eAgFnSc1d1	citlivá
na	na	k7c4	na
záznam	záznam	k1gInSc4	záznam
červeného	červený	k2eAgNnSc2d1	červené
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
na	na	k7c4	na
záznam	záznam	k1gInSc4	záznam
spektra	spektrum	k1gNnSc2	spektrum
zeleného	zelený	k2eAgNnSc2d1	zelené
a	a	k8xC	a
poslední	poslední	k2eAgFnPc1d1	poslední
na	na	k7c4	na
záznam	záznam	k1gInSc4	záznam
spektra	spektrum	k1gNnSc2	spektrum
modrého	modré	k1gNnSc2	modré
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
zpracování	zpracování	k1gNnSc2	zpracování
by	by	kYmCp3nS	by
však	však	k9	však
výsledkem	výsledek	k1gInSc7	výsledek
byly	být	k5eAaImAgFnP	být
tři	tři	k4xCgFnPc4	tři
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
naskládané	naskládaný	k2eAgFnPc1d1	naskládaná
černobílé	černobílý	k2eAgFnPc1d1	černobílá
snímky	snímka	k1gFnPc1	snímka
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
komplexním	komplexní	k2eAgInSc7d1	komplexní
postupem	postup	k1gInSc7	postup
ve	v	k7c6	v
specializované	specializovaný	k2eAgFnSc6d1	specializovaná
laboratoři	laboratoř	k1gFnSc6	laboratoř
vzniká	vznikat	k5eAaImIp3nS	vznikat
snímek	snímek	k1gInSc1	snímek
barevný	barevný	k2eAgInSc1d1	barevný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
představila	představit	k5eAaPmAgFnS	představit
firma	firma	k1gFnSc1	firma
Agfa	Agfa	k1gFnSc1	Agfa
film	film	k1gInSc4	film
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
podobný	podobný	k2eAgInSc4d1	podobný
–	–	k?	–
Agfacolor	Agfacolor	k1gInSc4	Agfacolor
Neu	Neu	k1gFnSc2	Neu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Kodachromu	Kodachrom	k1gInSc2	Kodachrom
byly	být	k5eAaImAgFnP	být
v	v	k7c4	v
Agfacolor	Agfacolor	k1gInSc4	Agfacolor
Neu	Neu	k1gFnSc2	Neu
barvotvorné	barvotvorný	k2eAgFnSc2d1	barvotvorná
složky	složka	k1gFnSc2	složka
vpraveny	vpravit	k5eAaPmNgInP	vpravit
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vrstev	vrstva	k1gFnPc2	vrstva
již	již	k6eAd1	již
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
značně	značně	k6eAd1	značně
usnadnilo	usnadnit	k5eAaPmAgNnS	usnadnit
zpracování	zpracování	k1gNnSc4	zpracování
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgInPc1d1	barevný
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dostupné	dostupný	k2eAgFnSc6d1	dostupná
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
využívají	využívat	k5eAaPmIp3nP	využívat
mnohavrstvou	mnohavrstvý	k2eAgFnSc4d1	mnohavrstvý
emulzi	emulze	k1gFnSc4	emulze
a	a	k8xC	a
principy	princip	k1gInPc4	princip
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
podobají	podobat	k5eAaImIp3nP	podobat
právě	právě	k9	právě
výrobkům	výrobek	k1gInPc3	výrobek
od	od	k7c2	od
Agfy	Agfa	k1gFnSc2	Agfa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
představila	představit	k5eAaPmAgFnS	představit
firma	firma	k1gFnSc1	firma
Polaroid	polaroid	k1gInSc4	polaroid
instantní	instantní	k2eAgInSc1d1	instantní
barevný	barevný	k2eAgInSc1d1	barevný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
ve	v	k7c6	v
zvláštním	zvláštní	k2eAgInSc6d1	zvláštní
fotoaparátu	fotoaparát	k1gInSc6	fotoaparát
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
jedinečné	jedinečný	k2eAgNnSc4d1	jedinečné
tištěné	tištěný	k2eAgFnPc4d1	tištěná
barevné	barevný	k2eAgFnPc4d1	barevná
snímky	snímka	k1gFnPc4	snímka
jenom	jenom	k8xS	jenom
minutu	minuta	k1gFnSc4	minuta
či	či	k8xC	či
dvě	dva	k4xCgFnPc4	dva
po	po	k7c6	po
expozici	expozice	k1gFnSc6	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Barevná	barevný	k2eAgFnSc1d1	barevná
fotografie	fotografie	k1gFnSc1	fotografie
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
diapozitivy	diapozitiv	k1gInPc4	diapozitiv
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
lze	lze	k6eAd1	lze
promítat	promítat	k5eAaImF	promítat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
diaprojektoru	diaprojektor	k1gInSc2	diaprojektor
nebo	nebo	k8xC	nebo
barevné	barevný	k2eAgInPc4d1	barevný
negativy	negativ	k1gInPc4	negativ
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
barevné	barevný	k2eAgFnPc4d1	barevná
zvětšeniny	zvětšenina	k1gFnPc4	zvětšenina
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
automatizovanému	automatizovaný	k2eAgNnSc3d1	automatizované
vybavení	vybavení	k1gNnSc3	vybavení
na	na	k7c4	na
tisk	tisk	k1gInSc4	tisk
fotek	fotka	k1gFnPc2	fotka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
právě	právě	k9	právě
tisk	tisk	k1gInSc4	tisk
na	na	k7c4	na
papír	papír	k1gInSc4	papír
tou	ten	k3xDgFnSc7	ten
nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
formou	forma	k1gFnSc7	forma
barevné	barevný	k2eAgFnSc2d1	barevná
fotografie	fotografia	k1gFnSc2	fotografia
na	na	k7c4	na
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přechodném	přechodný	k2eAgNnSc6d1	přechodné
období	období	k1gNnSc6	období
datovaném	datovaný	k2eAgNnSc6d1	datované
zhruba	zhruba	k6eAd1	zhruba
1995-2005	[number]	k4	1995-2005
byl	být	k5eAaImAgInS	být
barevný	barevný	k2eAgInSc1d1	barevný
film	film	k1gInSc1	film
odsunut	odsunut	k2eAgInSc1d1	odsunut
levnými	levný	k2eAgInPc7d1	levný
mnohapixelovými	mnohapixelův	k2eAgInPc7d1	mnohapixelův
digitálními	digitální	k2eAgInPc7d1	digitální
fotoaparáty	fotoaparát	k1gInPc7	fotoaparát
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
někteří	některý	k3yIgMnPc1	některý
fotografové	fotograf	k1gMnPc1	fotograf
i	i	k9	i
nadále	nadále	k6eAd1	nadále
preferují	preferovat	k5eAaImIp3nP	preferovat
film	film	k1gInSc4	film
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
vynalezli	vynaleznout	k5eAaPmAgMnP	vynaleznout
George	George	k1gNnSc4	George
Elwood	Elwooda	k1gFnPc2	Elwooda
Smith	Smith	k1gMnSc1	Smith
a	a	k8xC	a
Willard	Willard	k1gMnSc1	Willard
Boyle	Boyl	k1gMnSc2	Boyl
snímače	snímač	k1gInSc2	snímač
typu	typ	k1gInSc2	typ
CCD	CCD	kA	CCD
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
zabudovali	zabudovat	k5eAaPmAgMnP	zabudovat
CCD	CCD	kA	CCD
do	do	k7c2	do
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
společnost	společnost	k1gFnSc1	společnost
Sony	Sony	kA	Sony
první	první	k4xOgInSc4	první
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
<g/>
,	,	kIx,	,
Sony	Sony	kA	Sony
Mavica	Mavic	k1gInSc2	Mavic
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
místo	místo	k7c2	místo
filmu	film	k1gInSc2	film
na	na	k7c6	na
chemickém	chemický	k2eAgInSc6d1	chemický
principu	princip	k1gInSc6	princip
zaznamenával	zaznamenávat	k5eAaImAgInS	zaznamenávat
obraz	obraz	k1gInSc1	obraz
na	na	k7c4	na
elektronické	elektronický	k2eAgInPc4d1	elektronický
prvky	prvek	k1gInPc4	prvek
CCD	CCD	kA	CCD
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
analogové	analogový	k2eAgInPc1d1	analogový
výstupy	výstup	k1gInPc1	výstup
se	se	k3xPyFc4	se
zapisovaly	zapisovat	k5eAaImAgInP	zapisovat
na	na	k7c4	na
disketu	disketa	k1gFnSc4	disketa
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
tahounem	tahoun	k1gInSc7	tahoun
vývoje	vývoj	k1gInSc2	vývoj
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
létech	léto	k1gNnPc6	léto
firma	firma	k1gFnSc1	firma
Kodak	Kodak	kA	Kodak
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
přinesla	přinést	k5eAaPmAgFnS	přinést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
model	model	k1gInSc1	model
DCS	DCS	kA	DCS
100	[number]	k4	100
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
komerčně	komerčně	k6eAd1	komerčně
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
zrcadlovku	zrcadlovka	k1gFnSc4	zrcadlovka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
komerční	komerční	k2eAgFnSc2d1	komerční
digitální	digitální	k2eAgFnSc2d1	digitální
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
vysoká	vysoký	k2eAgFnSc1d1	vysoká
cena	cena	k1gFnSc1	cena
omezovala	omezovat	k5eAaImAgFnS	omezovat
její	její	k3xOp3gInSc4	její
prodej	prodej	k1gInSc4	prodej
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
fotožurnalistů	fotožurnalista	k1gMnPc2	fotožurnalista
a	a	k8xC	a
profesionálů	profesionál	k1gMnPc2	profesionál
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
digitální	digitální	k2eAgInSc1d1	digitální
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
výrazný	výrazný	k2eAgInSc4d1	výrazný
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
Apple	Apple	kA	Apple
QuickTake	QuickTak	k1gInSc2	QuickTak
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
prodeji	prodej	k1gInSc6	prodej
byly	být	k5eAaImAgInP	být
digitální	digitální	k2eAgInPc1d1	digitální
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
i	i	k8xC	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
začaly	začít	k5eAaPmAgInP	začít
aparáty	aparát	k1gInPc1	aparát
používající	používající	k2eAgInSc1d1	používající
digitální	digitální	k2eAgInSc4d1	digitální
záznam	záznam	k1gInSc4	záznam
vytlačovat	vytlačovat	k5eAaImF	vytlačovat
běžné	běžný	k2eAgFnPc4d1	běžná
kinofilmové	kinofilmový	k2eAgFnPc4d1	kinofilmová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
více	hodně	k6eAd2	hodně
než	než	k8xS	než
99	[number]	k4	99
%	%	kIx~	%
fotografií	fotografia	k1gFnPc2	fotografia
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
pomocí	pomocí	k7c2	pomocí
digitálních	digitální	k2eAgInPc2d1	digitální
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
narůstá	narůstat	k5eAaImIp3nS	narůstat
podíl	podíl	k1gInSc1	podíl
digitálních	digitální	k2eAgInPc2d1	digitální
snímků	snímek	k1gInPc2	snímek
ze	z	k7c2	z
smartphonů	smartphon	k1gInPc2	smartphon
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgInPc1d1	digitální
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
využívají	využívat	k5eAaImIp3nP	využívat
k	k	k7c3	k
záznamu	záznam	k1gInSc3	záznam
obrazu	obraz	k1gInSc2	obraz
elektronický	elektronický	k2eAgInSc4d1	elektronický
čip	čip	k1gInSc4	čip
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
jako	jako	k9	jako
soubor	soubor	k1gInSc1	soubor
elektronických	elektronický	k2eAgNnPc2d1	elektronické
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
ne	ne	k9	ne
jako	jako	k9	jako
soubor	soubor	k1gInSc4	soubor
chemických	chemický	k2eAgFnPc2d1	chemická
změn	změna	k1gFnPc2	změna
na	na	k7c6	na
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgInSc7d1	významný
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
digitální	digitální	k2eAgFnSc7d1	digitální
a	a	k8xC	a
chemickou	chemický	k2eAgFnSc7d1	chemická
fotografií	fotografia	k1gFnSc7	fotografia
tvoří	tvořit	k5eAaImIp3nS	tvořit
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
chemická	chemický	k2eAgFnSc1d1	chemická
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgFnSc1d1	pracující
s	s	k7c7	s
filmem	film	k1gInSc7	film
a	a	k8xC	a
fotografickým	fotografický	k2eAgInSc7d1	fotografický
papírem	papír	k1gInSc7	papír
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
těžko	těžko	k6eAd1	těžko
manipulovatelná	manipulovatelný	k2eAgFnSc1d1	manipulovatelná
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
digitální	digitální	k2eAgInPc1d1	digitální
snímky	snímek	k1gInPc1	snímek
se	se	k3xPyFc4	se
manipulují	manipulovat	k5eAaImIp3nP	manipulovat
běžně	běžně	k6eAd1	běžně
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
manipulovat	manipulovat	k5eAaImF	manipulovat
se	se	k3xPyFc4	se
snímkem	snímek	k1gInSc7	snímek
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
s	s	k7c7	s
fotografií	fotografia	k1gFnSc7	fotografia
na	na	k7c4	na
film	film	k1gInSc4	film
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
stěží	stěží	k6eAd1	stěží
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
otevírá	otevírat	k5eAaImIp3nS	otevírat
nový	nový	k2eAgInSc1d1	nový
komunikativní	komunikativní	k2eAgInSc1d1	komunikativní
potenciál	potenciál	k1gInSc1	potenciál
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
Československu	Československo	k1gNnSc6	Československo
bylo	být	k5eAaImAgNnS	být
fotografování	fotografování	k1gNnSc1	fotografování
rozšířeným	rozšířený	k2eAgInSc7d1	rozšířený
domácím	domácí	k2eAgInSc7d1	domácí
koníčkem	koníček	k1gInSc7	koníček
běžných	běžný	k2eAgMnPc2d1	běžný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Fotografické	fotografický	k2eAgNnSc4d1	fotografické
zboží	zboží	k1gNnSc4	zboží
se	se	k3xPyFc4	se
prodávalo	prodávat	k5eAaImAgNnS	prodávat
v	v	k7c6	v
prodejnách	prodejna	k1gFnPc6	prodejna
Fotokino	Fotokin	k2eAgNnSc1d1	Fotokin
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
drogerií	drogerie	k1gFnPc2	drogerie
<g/>
;	;	kIx,	;
na	na	k7c4	na
žádané	žádaný	k2eAgNnSc4d1	žádané
zboží	zboží	k1gNnSc4	zboží
se	se	k3xPyFc4	se
stávaly	stávat	k5eAaImAgFnP	stávat
i	i	k9	i
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
metrové	metrový	k2eAgFnSc2d1	metrová
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnSc1d1	západní
zboží	zboží	k1gNnSc1	zboží
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
velmi	velmi	k6eAd1	velmi
drahé	drahý	k2eAgNnSc1d1	drahé
(	(	kIx(	(
<g/>
nebylo	být	k5eNaImAgNnS	být
výjimečné	výjimečný	k2eAgNnSc1d1	výjimečné
<g/>
,	,	kIx,	,
že	že	k8xS	že
použitý	použitý	k2eAgInSc1d1	použitý
starý	starý	k2eAgInSc1d1	starý
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
stál	stát	k5eAaImAgInS	stát
několik	několik	k4yIc4	několik
měsíčních	měsíční	k2eAgInPc2d1	měsíční
platů	plat	k1gInPc2	plat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Praze	Praha	k1gFnSc6	Praha
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc4d1	jediný
bazar	bazar	k1gInSc4	bazar
s	s	k7c7	s
fotozbožím	fotozbožit	k5eAaPmIp1nS	fotozbožit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
kvalitní	kvalitní	k2eAgInPc1d1	kvalitní
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
tuzemské	tuzemský	k2eAgFnSc2d1	tuzemská
výroby	výroba	k1gFnSc2	výroba
značky	značka	k1gFnSc2	značka
FLEXARETA	FLEXARETA	kA	FLEXARETA
a	a	k8xC	a
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
PENTAKON	PENTAKON	kA	PENTAKON
a	a	k8xC	a
PRAKTICA	PRAKTICA	kA	PRAKTICA
z	z	k7c2	z
NDR	NDR	kA	NDR
s	s	k7c7	s
vynikajícími	vynikající	k2eAgFnPc7d1	vynikající
optickými	optický	k2eAgFnPc7d1	optická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
s	s	k7c7	s
uvolněním	uvolnění	k1gNnSc7	uvolnění
trhu	trh	k1gInSc2	trh
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
zařízením	zařízení	k1gNnSc7	zařízení
pro	pro	k7c4	pro
pořízení	pořízení	k1gNnSc4	pořízení
fotografie	fotografia	k1gFnSc2	fotografia
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
<g/>
,	,	kIx,	,
médiem	médium	k1gNnSc7	médium
pro	pro	k7c4	pro
zachycení	zachycení	k1gNnSc4	zachycení
obrazu	obraz	k1gInSc2	obraz
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
fotografická	fotografický	k2eAgFnSc1d1	fotografická
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
fotografický	fotografický	k2eAgInSc1d1	fotografický
film	film	k1gInSc1	film
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
digitálního	digitální	k2eAgInSc2d1	digitální
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
,	,	kIx,	,
elektronický	elektronický	k2eAgInSc4d1	elektronický
čip	čip	k1gInSc4	čip
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
a	a	k8xC	a
film	film	k1gInSc1	film
mohou	moct	k5eAaImIp3nP	moct
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
k	k	k7c3	k
uchování	uchování	k1gNnSc3	uchování
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zachycení	zachycení	k1gNnSc2	zachycení
na	na	k7c4	na
čip	čip	k1gInSc4	čip
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
uchování	uchování	k1gNnSc3	uchování
slouží	sloužit	k5eAaImIp3nP	sloužit
digitální	digitální	k2eAgFnSc1d1	digitální
magnetická	magnetický	k2eAgFnSc1d1	magnetická
či	či	k8xC	či
elektronická	elektronický	k2eAgFnSc1d1	elektronická
paměť	paměť	k1gFnSc1	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
další	další	k2eAgFnPc4d1	další
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
fotokopírky	fotokopírka	k1gFnSc2	fotokopírka
nebo	nebo	k8xC	nebo
xeroxy	xerox	k1gInPc4	xerox
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
fungují	fungovat	k5eAaImIp3nP	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Fotografové	fotograf	k1gMnPc1	fotograf
ovládají	ovládat	k5eAaImIp3nP	ovládat
tělo	tělo	k1gNnSc4	tělo
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
a	a	k8xC	a
objektiv	objektiv	k1gInSc4	objektiv
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vystavili	vystavit	k5eAaPmAgMnP	vystavit
světlocitlivý	světlocitlivý	k2eAgInSc4d1	světlocitlivý
materiál	materiál	k1gInSc4	materiál
požadovanému	požadovaný	k2eAgNnSc3d1	požadované
množství	množství	k1gNnSc3	množství
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
latentní	latentní	k2eAgInSc1d1	latentní
obraz	obraz	k1gInSc1	obraz
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
desky	deska	k1gFnSc2	deska
nebo	nebo	k8xC	nebo
filmu	film	k1gInSc2	film
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
soubor	soubor	k1gInSc1	soubor
RAW	RAW	kA	RAW
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
digitálních	digitální	k2eAgInPc2d1	digitální
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zpracován	zpracovat	k5eAaPmNgInS	zpracovat
a	a	k8xC	a
následně	následně	k6eAd1	následně
zkonvertován	zkonvertovat	k5eAaPmNgInS	zkonvertovat
do	do	k7c2	do
vhodného	vhodný	k2eAgInSc2d1	vhodný
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgInPc1d1	digitální
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
elektronický	elektronický	k2eAgInSc4d1	elektronický
čip	čip	k1gInSc4	čip
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
světlocitlivé	světlocitlivý	k2eAgFnSc6d1	světlocitlivá
elektronice	elektronika	k1gFnSc6	elektronika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
CCD	CCD	kA	CCD
čip	čip	k1gInSc4	čip
(	(	kIx(	(
<g/>
využívající	využívající	k2eAgInPc1d1	využívající
vázané	vázaný	k2eAgInPc1d1	vázaný
náboje	náboj	k1gInPc1	náboj
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
CMOS	CMOS	kA	CMOS
čip	čip	k1gInSc1	čip
(	(	kIx(	(
<g/>
doplňkový	doplňkový	k2eAgInSc1d1	doplňkový
polovodič	polovodič	k1gInSc1	polovodič
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
kovu	kov	k1gInSc2	kov
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
digitální	digitální	k2eAgInSc1d1	digitální
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
přechováván	přechovávat	k5eAaImNgInS	přechovávat
v	v	k7c6	v
elektronické	elektronický	k2eAgFnSc6d1	elektronická
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
reprodukován	reprodukovat	k5eAaBmNgInS	reprodukovat
i	i	k9	i
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Camera	Camera	k1gFnSc1	Camera
obscura	obscura	k1gFnSc1	obscura
je	být	k5eAaImIp3nS	být
temnou	temný	k2eAgFnSc7d1	temná
místností	místnost	k1gFnSc7	místnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
do	do	k7c2	do
maximální	maximální	k2eAgFnSc2d1	maximální
možné	možný	k2eAgFnSc2d1	možná
míry	míra	k1gFnSc2	míra
brání	bránit	k5eAaImIp3nP	bránit
průchodu	průchod	k1gInSc3	průchod
okolního	okolní	k2eAgNnSc2d1	okolní
světla	světlo	k1gNnSc2	světlo
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
světla	světlo	k1gNnSc2	světlo
formujícího	formující	k2eAgNnSc2d1	formující
obraz	obraz	k1gInSc4	obraz
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
zpravidla	zpravidla	k6eAd1	zpravidla
prochází	procházet	k5eAaImIp3nS	procházet
malým	malý	k2eAgInSc7d1	malý
otvorem	otvor	k1gInSc7	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
fotografovaný	fotografovaný	k2eAgInSc1d1	fotografovaný
subjekt	subjekt	k1gInSc1	subjekt
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
snímku	snímek	k1gInSc2	snímek
osvětlen	osvětlit	k5eAaPmNgMnS	osvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Camery	Camera	k1gFnPc1	Camera
obscury	obscura	k1gFnSc2	obscura
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
svými	svůj	k3xOyFgInPc7	svůj
rozměry	rozměr	k1gInPc7	rozměr
velmi	velmi	k6eAd1	velmi
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
velké	velká	k1gFnPc1	velká
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
celou	celý	k2eAgFnSc4d1	celá
temnou	temný	k2eAgFnSc4d1	temná
místnost	místnost	k1gFnSc4	místnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
fotografovaný	fotografovaný	k2eAgInSc1d1	fotografovaný
subjekt	subjekt	k1gInSc1	subjekt
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
jiné	jiný	k2eAgFnSc6d1	jiná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
nasvícen	nasvícen	k2eAgInSc1d1	nasvícen
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
fotografické	fotografický	k2eAgInPc1d1	fotografický
materiály	materiál	k1gInPc1	materiál
staly	stát	k5eAaPmAgInP	stát
dostatečně	dostatečně	k6eAd1	dostatečně
"	"	kIx"	"
<g/>
rychlé	rychlý	k2eAgFnSc3d1	rychlá
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
citlivé	citlivý	k2eAgInPc1d1	citlivý
<g/>
)	)	kIx)	)
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
umožnily	umožnit	k5eAaPmAgInP	umožnit
vytváření	vytváření	k1gNnSc4	vytváření
momentek	momentka	k1gFnPc2	momentka
a	a	k8xC	a
snímků	snímek	k1gInPc2	snímek
bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
fotografovaného	fotografovaný	k2eAgInSc2d1	fotografovaný
subjektu	subjekt	k1gInSc2	subjekt
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
vyrábět	vyrábět	k5eAaImF	vyrábět
malé	malý	k2eAgInPc4d1	malý
"	"	kIx"	"
<g/>
detektivní	detektivní	k2eAgInPc4d1	detektivní
<g/>
"	"	kIx"	"
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
dokonce	dokonce	k9	dokonce
maskované	maskovaný	k2eAgFnPc1d1	maskovaná
jako	jako	k9	jako
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
kabelka	kabelka	k1gFnSc1	kabelka
nebo	nebo	k8xC	nebo
kapesní	kapesní	k2eAgFnPc1d1	kapesní
hodinky	hodinka	k1gFnPc1	hodinka
(	(	kIx(	(
<g/>
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
Ticka	Ticek	k1gInSc2	Ticek
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
skrývaly	skrývat	k5eAaImAgInP	skrývat
za	za	k7c7	za
širokou	široký	k2eAgFnSc7d1	široká
kravatou	kravata	k1gFnSc7	kravata
či	či	k8xC	či
šálou	šála	k1gFnSc7	šála
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
spona	spona	k1gFnSc1	spona
do	do	k7c2	do
kravaty	kravata	k1gFnSc2	kravata
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
objektivem	objektiv	k1gInSc7	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
vychází	vycházet	k5eAaImIp3nS	vycházet
videokamera	videokamera	k1gFnSc1	videokamera
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgNnPc4d1	charakteristické
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
sekvence	sekvence	k1gFnSc1	sekvence
fotografií	fotografia	k1gFnPc2	fotografia
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
co	co	k9	co
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
obraz	obraz	k1gInSc1	obraz
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
snímku	snímek	k1gInSc6	snímek
<g/>
,	,	kIx,	,
kamera	kamera	k1gFnSc1	kamera
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
celé	celý	k2eAgFnPc4d1	celá
série	série	k1gFnPc4	série
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
snímky	snímek	k1gInPc1	snímek
jsou	být	k5eAaImIp3nP	být
později	pozdě	k6eAd2	pozdě
přehrány	přehrát	k5eAaPmNgFnP	přehrát
určitou	určitý	k2eAgFnSc7d1	určitá
rychlostí	rychlost	k1gFnSc7	rychlost
počtu	počet	k1gInSc2	počet
snímků	snímek	k1gInPc2	snímek
za	za	k7c4	za
vteřinu	vteřina	k1gFnSc4	vteřina
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
přebírán	přebírán	k2eAgInSc1d1	přebírán
anglický	anglický	k2eAgInSc1d1	anglický
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
frame	framat	k5eAaPmIp3nS	framat
rate	rate	k1gFnSc1	rate
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
fps	fps	k?	fps
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sledování	sledování	k1gNnSc6	sledování
lidské	lidský	k2eAgFnSc2d1	lidská
oči	oko	k1gNnPc1	oko
a	a	k8xC	a
mozek	mozek	k1gInSc1	mozek
spojí	spojit	k5eAaPmIp3nS	spojit
tyto	tento	k3xDgInPc4	tento
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
snímky	snímek	k1gInPc4	snímek
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
iluze	iluze	k1gFnSc1	iluze
<g/>
,	,	kIx,	,
že	že	k8xS	že
sledujeme	sledovat	k5eAaImIp1nP	sledovat
skutečný	skutečný	k2eAgInSc4d1	skutečný
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
vhodné	vhodný	k2eAgFnSc2d1	vhodná
expozice	expozice	k1gFnSc2	expozice
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
provést	provést	k5eAaPmF	provést
určitá	určitý	k2eAgNnPc4d1	určité
nastavení	nastavení	k1gNnPc4	nastavení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
automaticky	automaticky	k6eAd1	automaticky
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
manuálně	manuálně	k6eAd1	manuálně
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
čistého	čistý	k2eAgNnSc2d1	čisté
<g/>
,	,	kIx,	,
ostrého	ostrý	k2eAgNnSc2d1	ostré
a	a	k8xC	a
vhodně	vhodně	k6eAd1	vhodně
osvětleného	osvětlený	k2eAgInSc2d1	osvětlený
snímku	snímek	k1gInSc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Ovládání	ovládání	k1gNnSc1	ovládání
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
zpravidla	zpravidla	k6eAd1	zpravidla
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
Výrazný	výrazný	k2eAgInSc1d1	výrazný
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
nebo	nebo	k8xC	nebo
estetiku	estetika	k1gFnSc4	estetika
fotografie	fotografia	k1gFnSc2	fotografia
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
částí	část	k1gFnPc2	část
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
<g/>
:	:	kIx,	:
Ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
a	a	k8xC	a
druh	druh	k1gInSc1	druh
objektivu	objektiv	k1gInSc2	objektiv
(	(	kIx(	(
<g/>
normální	normální	k2eAgInPc1d1	normální
<g/>
,	,	kIx,	,
teleobjektiv	teleobjektiv	k1gInSc1	teleobjektiv
<g/>
,	,	kIx,	,
širokoúhlý	širokoúhlý	k2eAgInSc1d1	širokoúhlý
<g/>
,	,	kIx,	,
makro	makro	k1gNnSc1	makro
<g/>
,	,	kIx,	,
rybí	rybí	k2eAgNnSc1d1	rybí
oko	oko	k1gNnSc1	oko
<g />
.	.	kIx.	.
</s>
<s>
nebo	nebo	k8xC	nebo
zoom	zoom	k6eAd1	zoom
<g/>
)	)	kIx)	)
Použité	použitý	k2eAgInPc1d1	použitý
fotografické	fotografický	k2eAgInPc1d1	fotografický
filtry	filtr	k1gInPc1	filtr
<g/>
,	,	kIx,	,
nástavce	nástavec	k1gInPc1	nástavec
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
doplňky	doplněk	k1gInPc1	doplněk
Citlivost	citlivost	k1gFnSc1	citlivost
média	médium	k1gNnSc2	médium
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
a	a	k8xC	a
barvu	barva	k1gFnSc4	barva
Parametry	parametr	k1gInPc1	parametr
materiálu	materiál	k1gInSc2	materiál
zaznamenávajícího	zaznamenávající	k2eAgInSc2d1	zaznamenávající
světlo	světlo	k1gNnSc1	světlo
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
případě	případ	k1gInSc6	případ
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
na	na	k7c4	na
film	film	k1gInSc4	film
hraje	hrát	k5eAaImIp3nS	hrát
roli	role	k1gFnSc4	role
velikost	velikost	k1gFnSc1	velikost
světlocitlivých	světlocitlivý	k2eAgInPc2d1	světlocitlivý
krystalků	krystalek	k1gInPc2	krystalek
halogenidů	halogenid	k1gMnPc2	halogenid
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
u	u	k7c2	u
digitálních	digitální	k2eAgInPc2d1	digitální
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
rozlišení	rozlišení	k1gNnSc4	rozlišení
dané	daný	k2eAgFnSc2d1	daná
počtem	počet	k1gInSc7	počet
pixelů	pixel	k1gInPc2	pixel
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
uvedená	uvedený	k2eAgNnPc1d1	uvedené
hlediska	hledisko	k1gNnPc1	hledisko
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
clona	clona	k1gFnSc1	clona
<g/>
,	,	kIx,	,
čas	čas	k1gInSc1	čas
expozice	expozice	k1gFnSc1	expozice
a	a	k8xC	a
citlivost	citlivost	k1gFnSc1	citlivost
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
nerozlučně	rozlučně	k6eNd1	rozlučně
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mění	měnit	k5eAaImIp3nS	měnit
celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
světla	světlo	k1gNnSc2	světlo
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
na	na	k7c6	na
médium	médium	k1gNnSc4	médium
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
kteréhokoli	kterýkoli	k3yIgInSc2	kterýkoli
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
změní	změnit	k5eAaPmIp3nS	změnit
i	i	k9	i
výslednou	výsledný	k2eAgFnSc4d1	výsledná
expozici	expozice	k1gFnSc4	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
dnešních	dnešní	k2eAgInPc2d1	dnešní
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
je	být	k5eAaImIp3nS	být
schopných	schopný	k2eAgMnPc2d1	schopný
provádět	provádět	k5eAaImF	provádět
nastavení	nastavení	k1gNnSc1	nastavení
těchto	tento	k3xDgMnPc2	tento
parametrů	parametr	k1gInPc2	parametr
automaticky	automaticky	k6eAd1	automaticky
<g/>
,	,	kIx,	,
např.	např.	kA	např.
přizpůsobí	přizpůsobit	k5eAaPmIp3nS	přizpůsobit
clonu	clona	k1gFnSc4	clona
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nastavenému	nastavený	k2eAgInSc3d1	nastavený
času	čas	k1gInSc3	čas
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
expozice	expozice	k1gFnSc2	expozice
(	(	kIx(	(
<g/>
také	také	k9	také
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
"	"	kIx"	"
<g/>
rychlost	rychlost	k1gFnSc1	rychlost
závěrky	závěrka	k1gFnSc2	závěrka
<g/>
"	"	kIx"	"
–	–	k?	–
i	i	k9	i
u	u	k7c2	u
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
závěrku	závěrka	k1gFnSc4	závěrka
nemají	mít	k5eNaImIp3nP	mít
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
udávána	udáván	k2eAgFnSc1d1	udávána
ve	v	k7c6	v
zlomcích	zlomek	k1gInPc6	zlomek
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
i	i	k9	i
expoziční	expoziční	k2eAgInPc4d1	expoziční
časy	čas	k1gInPc4	čas
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
více	hodně	k6eAd2	hodně
vteřin	vteřina	k1gFnPc2	vteřina
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
pro	pro	k7c4	pro
fotografie	fotografia	k1gFnPc4	fotografia
neživých	živý	k2eNgInPc2d1	neživý
subjektů	subjekt	k1gInPc2	subjekt
či	či	k8xC	či
noční	noční	k2eAgFnSc4d1	noční
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
expoziční	expoziční	k2eAgInPc1d1	expoziční
časy	čas	k1gInPc1	čas
i	i	k8xC	i
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
pohyblivých	pohyblivý	k2eAgInPc2d1	pohyblivý
subjektů	subjekt	k1gInPc2	subjekt
se	se	k3xPyFc4	se
však	však	k9	však
častěji	často	k6eAd2	často
používá	používat	k5eAaImIp3nS	používat
krátká	krátký	k2eAgFnSc1d1	krátká
doba	doba	k1gFnSc1	doba
expozice	expozice	k1gFnSc2	expozice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
rozmazání	rozmazání	k1gNnSc1	rozmazání
obsahu	obsah	k1gInSc2	obsah
fotografie	fotografia	k1gFnSc2	fotografia
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
i	i	k9	i
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
někdy	někdy	k6eAd1	někdy
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
zdůraznění	zdůraznění	k1gNnSc4	zdůraznění
pohybu	pohyb	k1gInSc2	pohyb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Efektivní	efektivní	k2eAgFnSc1d1	efektivní
clona	clona	k1gFnSc1	clona
je	být	k5eAaImIp3nS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
clonovým	clonový	k2eAgNnSc7d1	clonové
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
dané	daný	k2eAgFnPc4d1	daná
poměrem	poměr	k1gInSc7	poměr
ohniskové	ohniskový	k2eAgFnSc2d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
a	a	k8xC	a
průměrem	průměr	k1gInSc7	průměr
otvoru	otvor	k1gInSc2	otvor
clony	clona	k1gFnSc2	clona
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stejném	stejný	k2eAgInSc6d1	stejný
průměru	průměr	k1gInSc6	průměr
clony	clona	k1gFnSc2	clona
projde	projít	k5eAaPmIp3nS	projít
objektivem	objektiv	k1gInSc7	objektiv
s	s	k7c7	s
delší	dlouhý	k2eAgFnSc7d2	delší
ohniskovou	ohniskový	k2eAgFnSc7d1	ohnisková
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
méně	málo	k6eAd2	málo
světla	světlo	k1gNnSc2	světlo
než	než	k8xS	než
u	u	k7c2	u
objektivů	objektiv	k1gInPc2	objektiv
s	s	k7c7	s
kratší	krátký	k2eAgFnSc7d2	kratší
ohniskovou	ohniskový	k2eAgFnSc7d1	ohnisková
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
propustností	propustnost	k1gFnSc7	propustnost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
je	být	k5eAaImIp3nS	být
clonové	clonový	k2eAgNnSc1d1	clonové
číslo	číslo	k1gNnSc1	číslo
menší	malý	k2eAgNnSc1d2	menší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
je	být	k5eAaImIp3nS	být
efektivní	efektivní	k2eAgFnSc1d1	efektivní
clona	clona	k1gFnSc1	clona
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
systém	systém	k1gInSc1	systém
clonových	clonový	k2eAgNnPc2d1	clonové
čísel	číslo	k1gNnPc2	číslo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Britský	britský	k2eAgInSc1d1	britský
standard	standard	k1gInSc1	standard
(	(	kIx(	(
<g/>
BS-	BS-	k1gFnSc1	BS-
<g/>
1013	[number]	k4	1013
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
standardizován	standardizovat	k5eAaBmNgMnS	standardizovat
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
shromáždění	shromáždění	k1gNnSc6	shromáždění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
byly	být	k5eAaImAgFnP	být
používané	používaný	k2eAgFnPc1d1	používaná
i	i	k8xC	i
jiné	jiný	k2eAgFnPc1d1	jiná
stupnice	stupnice	k1gFnPc1	stupnice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Evropská	evropský	k2eAgFnSc1d1	Evropská
stupnice	stupnice	k1gFnSc1	stupnice
<g/>
,	,	kIx,	,
Střední	střední	k2eAgNnSc1d1	střední
nastavení	nastavení	k1gNnSc1	nastavení
nebo	nebo	k8xC	nebo
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
stupnice	stupnice	k1gFnSc1	stupnice
navržená	navržený	k2eAgFnSc1d1	navržená
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
Královskou	královský	k2eAgFnSc7d1	královská
fotografickou	fotografický	k2eAgFnSc7d1	fotografická
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
stupnice	stupnice	k1gFnPc1	stupnice
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zastaralé	zastaralý	k2eAgNnSc4d1	zastaralé
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
udávána	udáván	k2eAgNnPc4d1	udáváno
i	i	k8xC	i
T-čísla	T-čísla	k1gFnSc2	T-čísla
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
berou	brát	k5eAaImIp3nP	brát
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
propustnost	propustnost	k1gFnSc1	propustnost
světla	světlo	k1gNnSc2	světlo
daného	daný	k2eAgInSc2d1	daný
objektivu	objektiv	k1gInSc2	objektiv
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
vypočítat	vypočítat	k5eAaPmF	vypočítat
vynásobením	vynásobení	k1gNnSc7	vynásobení
clonového	clonový	k2eAgNnSc2d1	clonové
čísla	číslo	k1gNnSc2	číslo
a	a	k8xC	a
odmocniny	odmocnina	k1gFnSc2	odmocnina
propustnosti	propustnost	k1gFnSc2	propustnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
clonové	clonový	k2eAgNnSc4d1	clonové
číslo	číslo	k1gNnSc4	číslo
zmenšeno	zmenšen	k2eAgNnSc4d1	zmenšeno
o	o	k7c4	o
násobek	násobek	k1gInSc4	násobek
√	√	k?	√
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
clony	clona	k1gFnSc2	clona
se	se	k3xPyFc4	se
o	o	k7c4	o
stejný	stejný	k2eAgInSc4d1	stejný
násobek	násobek	k1gInSc4	násobek
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
plocha	plocha	k1gFnSc1	plocha
clony	clona	k1gFnSc2	clona
se	se	k3xPyFc4	se
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
o	o	k7c4	o
násobek	násobek	k1gInSc4	násobek
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
clonová	clonový	k2eAgNnPc4d1	clonové
čísla	číslo	k1gNnPc4	číslo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
běžném	běžný	k2eAgInSc6d1	běžný
objektivu	objektiv	k1gInSc6	objektiv
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
2.8	[number]	k4	2.8
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
5.6	[number]	k4	5.6
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
,	,	kIx,	,
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
clonové	clonový	k2eAgNnSc4d1	clonové
číslo	číslo	k1gNnSc4	číslo
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
hodnotu	hodnota	k1gFnSc4	hodnota
snížíme	snížit	k5eAaPmIp1nP	snížit
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc2	množství
světla	světlo	k1gNnSc2	světlo
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
na	na	k7c4	na
film	film	k1gInSc4	film
nebo	nebo	k8xC	nebo
čip	čip	k1gInSc4	čip
se	se	k3xPyFc4	se
zdvojnásobí	zdvojnásobit	k5eAaPmIp3nS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
naopak	naopak	k6eAd1	naopak
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
číslo	číslo	k1gNnSc4	číslo
přicloníme	přiclonit	k5eAaPmIp1nP	přiclonit
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dopadat	dopadat	k5eAaImF	dopadat
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
původní	původní	k2eAgFnSc3d1	původní
hodnotě	hodnota	k1gFnSc3	hodnota
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
polovina	polovina	k1gFnSc1	polovina
množství	množství	k1gNnSc2	množství
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc4	snímek
můžeme	moct	k5eAaImIp1nP	moct
zachytit	zachytit	k5eAaPmF	zachytit
pomocí	pomocí	k7c2	pomocí
různých	různý	k2eAgFnPc2d1	různá
kombinací	kombinace	k1gFnPc2	kombinace
závěrky	závěrka	k1gFnSc2	závěrka
<g/>
,	,	kIx,	,
clony	clona	k1gFnSc2	clona
a	a	k8xC	a
citlivosti	citlivost	k1gFnSc2	citlivost
<g/>
.	.	kIx.	.
</s>
<s>
Různá	různý	k2eAgNnPc1d1	různé
nastavení	nastavení	k1gNnPc1	nastavení
clony	clona	k1gFnSc2	clona
a	a	k8xC	a
doby	doba	k1gFnSc2	doba
expozice	expozice	k1gFnSc2	expozice
mohou	moct	k5eAaImIp3nP	moct
umožnit	umožnit	k5eAaPmF	umožnit
pořizování	pořizování	k1gNnSc4	pořizování
snímků	snímek	k1gInPc2	snímek
na	na	k7c6	na
určitých	určitý	k2eAgFnPc6d1	určitá
hodnotách	hodnota	k1gFnPc6	hodnota
citlivosti	citlivost	k1gFnSc2	citlivost
<g/>
,	,	kIx,	,
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
světelných	světelný	k2eAgFnPc6d1	světelná
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
subjektu	subjekt	k1gInSc2	subjekt
či	či	k8xC	či
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
hloubky	hloubka	k1gFnSc2	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
menší	malý	k2eAgFnSc6d2	menší
citlivosti	citlivost	k1gFnSc6	citlivost
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
v	v	k7c6	v
případě	případ	k1gInSc6	případ
filmu	film	k1gInSc2	film
menší	malý	k2eAgFnSc4d2	menší
zrnitost	zrnitost	k1gFnSc4	zrnitost
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
digitálních	digitální	k2eAgInPc2d1	digitální
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
pak	pak	k6eAd1	pak
menší	malý	k2eAgInSc4d2	menší
šum	šum	k1gInSc4	šum
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
citlivost	citlivost	k1gFnSc1	citlivost
však	však	k9	však
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kratší	krátký	k2eAgInSc4d2	kratší
čas	čas	k1gInSc4	čas
expozice	expozice	k1gFnSc2	expozice
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
např.	např.	kA	např.
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
pohybové	pohybový	k2eAgFnPc4d1	pohybová
neostrosti	neostrost	k1gFnPc4	neostrost
<g/>
,	,	kIx,	,
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
vyššího	vysoký	k2eAgNnSc2d2	vyšší
clonového	clonový	k2eAgNnSc2d1	clonové
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
hloubky	hloubka	k1gFnSc2	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
clona	clona	k1gFnSc1	clona
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
využívaná	využívaný	k2eAgFnSc1d1	využívaná
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
větší	veliký	k2eAgFnSc1d2	veliký
clona	clona	k1gFnSc1	clona
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
světla	světlo	k1gNnPc1	světlo
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
subjekt	subjekt	k1gInSc1	subjekt
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
a	a	k8xC	a
chceme	chtít	k5eAaImIp1nP	chtít
zabránit	zabránit	k5eAaPmF	zabránit
pohybové	pohybový	k2eAgFnPc4d1	pohybová
neostrosti	neostrost	k1gFnPc4	neostrost
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
ho	on	k3xPp3gNnSc4	on
"	"	kIx"	"
<g/>
zmrazit	zmrazit	k5eAaPmF	zmrazit
<g/>
"	"	kIx"	"
díky	díky	k7c3	díky
kratší	krátký	k2eAgFnSc3d2	kratší
době	doba	k1gFnSc3	doba
expozice	expozice	k1gFnSc2	expozice
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zabránění	zabránění	k1gNnSc3	zabránění
pohybové	pohybový	k2eAgFnSc2d1	pohybová
neostrosti	neostrost	k1gFnSc2	neostrost
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
i	i	k9	i
stativ	stativ	k1gInSc1	stativ
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
není	být	k5eNaImIp3nS	být
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
fotografa	fotograf	k1gMnSc2	fotograf
a	a	k8xC	a
expozice	expozice	k1gFnSc2	expozice
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
delší	dlouhý	k2eAgMnSc1d2	delší
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozmazání	rozmazání	k1gNnSc3	rozmazání
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
clona	clona	k1gFnSc1	clona
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
(	(	kIx(	(
<g/>
clonové	clonový	k2eAgNnSc1d1	clonové
číslo	číslo	k1gNnSc1	číslo
8	[number]	k4	8
<g/>
)	)	kIx)	)
a	a	k8xC	a
čas	čas	k1gInSc1	čas
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
125	[number]	k4	125
(	(	kIx(	(
<g/>
doba	doba	k1gFnSc1	doba
expozice	expozice	k1gFnSc1	expozice
8	[number]	k4	8
<g/>
ms	ms	k?	ms
<g/>
)	)	kIx)	)
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
a	a	k8xC	a
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
250	[number]	k4	250
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
stejného	stejný	k2eAgInSc2d1	stejný
celkového	celkový	k2eAgInSc2d1	celkový
objemu	objem	k1gInSc2	objem
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
tato	tento	k3xDgNnPc1	tento
dvě	dva	k4xCgNnPc1	dva
nastavení	nastavení	k1gNnPc1	nastavení
neposkytnou	poskytnout	k5eNaPmIp3nP	poskytnout
stejný	stejný	k2eAgInSc4d1	stejný
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Clona	clona	k1gFnSc1	clona
a	a	k8xC	a
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
objektivu	objektiv	k1gInSc2	objektiv
společně	společně	k6eAd1	společně
utváří	utvářet	k5eAaImIp3nS	utvářet
hloubku	hloubka	k1gFnSc4	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
rozmezí	rozmezí	k1gNnSc4	rozmezí
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ostré	ostrý	k2eAgNnSc4d1	ostré
<g/>
.	.	kIx.	.
</s>
<s>
Objektiv	objektiv	k1gInSc1	objektiv
s	s	k7c7	s
delší	dlouhý	k2eAgFnSc7d2	delší
ohniskovou	ohniskový	k2eAgFnSc7d1	ohnisková
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
a	a	k8xC	a
otevřenější	otevřený	k2eAgFnSc7d2	otevřenější
clonou	clona	k1gFnSc7	clona
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
snímek	snímek	k1gInSc1	snímek
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
hloubkou	hloubka	k1gFnSc7	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
(	(	kIx(	(
<g/>
jenom	jenom	k9	jenom
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
bude	být	k5eAaImBp3nS	být
zaostřena	zaostřit	k5eAaPmNgFnS	zaostřit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
využíváno	využívat	k5eAaPmNgNnS	využívat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
oddělit	oddělit	k5eAaPmF	oddělit
subjekt	subjekt	k1gInSc4	subjekt
od	od	k7c2	od
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
portrétní	portrétní	k2eAgFnSc6d1	portrétní
fotografii	fotografia	k1gFnSc6	fotografia
nebo	nebo	k8xC	nebo
makrofotografii	makrofotografie	k1gFnSc6	makrofotografie
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
použití	použití	k1gNnSc1	použití
širokoúhlejšího	širokoúhlý	k2eAgInSc2d2	širokoúhlejší
objektivu	objektiv	k1gInSc2	objektiv
nebo	nebo	k8xC	nebo
vyššího	vysoký	k2eAgNnSc2d2	vyšší
clonového	clonový	k2eAgNnSc2d1	clonové
čísla	číslo	k1gNnSc2	číslo
povede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
hloubce	hloubka	k1gFnSc3	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vhodné	vhodný	k2eAgNnSc1d1	vhodné
například	například	k6eAd1	například
při	při	k7c6	při
fotografování	fotografování	k1gNnSc6	fotografování
krajiny	krajina	k1gFnSc2	krajina
nebo	nebo	k8xC	nebo
skupin	skupina	k1gFnPc2	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
více	hodně	k6eAd2	hodně
objektů	objekt	k1gInPc2	objekt
(	(	kIx(	(
<g/>
blízkých	blízký	k2eAgFnPc2d1	blízká
i	i	k8xC	i
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
<g/>
)	)	kIx)	)
bude	být	k5eAaImBp3nS	být
na	na	k7c6	na
výsledném	výsledný	k2eAgInSc6d1	výsledný
snímku	snímek	k1gInSc6	snímek
zaostřených	zaostřený	k2eAgFnPc2d1	zaostřená
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
opravdu	opravdu	k6eAd1	opravdu
velkými	velký	k2eAgFnPc7d1	velká
clonami	clona	k1gFnPc7	clona
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
dírkových	dírkový	k2eAgFnPc2d1	dírková
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
spadá	spadat	k5eAaPmIp3nS	spadat
značná	značný	k2eAgFnSc1d1	značná
škála	škála	k1gFnSc1	škála
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
,	,	kIx,	,
vinou	vinout	k5eAaImIp3nP	vinout
difrakce	difrakce	k1gFnPc4	difrakce
způsobené	způsobený	k2eAgFnPc4d1	způsobená
velmi	velmi	k6eAd1	velmi
malým	malý	k2eAgInSc7d1	malý
otvorem	otvor	k1gInSc7	otvor
pro	pro	k7c4	pro
průchod	průchod	k1gInSc4	průchod
světla	světlo	k1gNnSc2	světlo
je	být	k5eAaImIp3nS	být
však	však	k9	však
celková	celkový	k2eAgFnSc1d1	celková
ostrost	ostrost	k1gFnSc1	ostrost
snímku	snímek	k1gInSc2	snímek
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc2d3	veliký
ostrosti	ostrost	k1gFnSc2	ostrost
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
okolo	okolo	k7c2	okolo
střední	střední	k2eAgFnSc2d1	střední
hodnoty	hodnota	k1gFnSc2	hodnota
rozsahu	rozsah	k1gInSc2	rozsah
clony	clona	k1gFnSc2	clona
objektivu	objektiv	k1gInSc2	objektiv
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
objektiv	objektiv	k1gInSc4	objektiv
s	s	k7c7	s
rozsahem	rozsah	k1gInSc7	rozsah
f	f	k?	f
<g/>
2,8	[number]	k4	2,8
<g/>
-f	-f	k?	-f
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
technologickému	technologický	k2eAgInSc3d1	technologický
pokroku	pokrok	k1gInSc3	pokrok
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
objektivy	objektiv	k1gInPc1	objektiv
čím	čí	k3xOyRgNnSc7	čí
dál	daleko	k6eAd2	daleko
lepší	dobrý	k2eAgFnSc1d2	lepší
schopnost	schopnost	k1gFnSc1	schopnost
dosahovat	dosahovat	k5eAaImF	dosahovat
ostrosti	ostrost	k1gFnSc2	ostrost
i	i	k9	i
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
široce	široko	k6eAd1	široko
otevřené	otevřený	k2eAgFnSc2d1	otevřená
clony	clona	k1gFnSc2	clona
<g/>
.	.	kIx.	.
</s>
<s>
Zachycení	zachycení	k1gNnSc1	zachycení
obrazu	obraz	k1gInSc2	obraz
je	být	k5eAaImIp3nS	být
jenom	jenom	k9	jenom
částí	část	k1gFnSc7	část
tvorby	tvorba	k1gFnSc2	tvorba
snímku	snímek	k1gInSc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
materiál	materiál	k1gInSc4	materiál
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
nutné	nutný	k2eAgNnSc1d1	nutné
podniknout	podniknout	k5eAaPmF	podniknout
jisté	jistý	k2eAgInPc4d1	jistý
kroky	krok	k1gInPc4	krok
k	k	k7c3	k
převodu	převod	k1gInSc3	převod
latentního	latentní	k2eAgInSc2d1	latentní
obrazu	obraz	k1gInSc2	obraz
zachyceného	zachycený	k2eAgInSc2d1	zachycený
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
na	na	k7c4	na
viditelný	viditelný	k2eAgInSc4d1	viditelný
snímek	snímek	k1gInSc4	snímek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
diapozitivů	diapozitiv	k1gInPc2	diapozitiv
se	se	k3xPyFc4	se
vyvolávaný	vyvolávaný	k2eAgInSc1d1	vyvolávaný
film	film	k1gInSc1	film
vloží	vložit	k5eAaPmIp3nS	vložit
do	do	k7c2	do
diaprojektoru	diaprojektor	k1gInSc2	diaprojektor
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
promítán	promítán	k2eAgMnSc1d1	promítán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vytištění	vytištění	k1gNnSc3	vytištění
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vytisknout	vytisknout	k5eAaPmF	vytisknout
již	již	k6eAd1	již
vyvolaný	vyvolaný	k2eAgInSc4d1	vyvolaný
filmový	filmový	k2eAgInSc4d1	filmový
negativ	negativ	k1gInSc4	negativ
na	na	k7c4	na
fotografický	fotografický	k2eAgInSc4d1	fotografický
papír	papír	k1gInSc4	papír
nebo	nebo	k8xC	nebo
diapozitiv	diapozitiv	k1gInSc4	diapozitiv
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
laserových	laserový	k2eAgInPc2d1	laserový
a	a	k8xC	a
inkoustových	inkoustový	k2eAgFnPc2d1	inkoustová
tiskáren	tiskárna	k1gFnPc2	tiskárna
se	se	k3xPyFc4	se
celuloidové	celuloidový	k2eAgInPc1d1	celuloidový
negativy	negativ	k1gInPc1	negativ
dávaly	dávat	k5eAaImAgInP	dávat
do	do	k7c2	do
zvětšovacího	zvětšovací	k2eAgInSc2d1	zvětšovací
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
měřenou	měřený	k2eAgFnSc4d1	měřená
na	na	k7c4	na
vteřiny	vteřina	k1gFnPc4	vteřina
nebo	nebo	k8xC	nebo
zlomky	zlomek	k1gInPc4	zlomek
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
)	)	kIx)	)
promítl	promítnout	k5eAaPmAgInS	promítnout
obraz	obraz	k1gInSc1	obraz
na	na	k7c4	na
světlocitlivý	světlocitlivý	k2eAgInSc4d1	světlocitlivý
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
papír	papír	k1gInSc1	papír
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
ponořen	ponořit	k5eAaPmNgInS	ponořit
do	do	k7c2	do
vývojky	vývojka	k1gFnSc2	vývojka
(	(	kIx(	(
<g/>
chemického	chemický	k2eAgInSc2d1	chemický
roztoku	roztok	k1gInSc2	roztok
sloužícího	sloužící	k2eAgInSc2d1	sloužící
ke	k	k7c3	k
zviditelnění	zviditelnění	k1gNnSc3	zviditelnění
latentního	latentní	k2eAgInSc2d1	latentní
obrazu	obraz	k1gInSc2	obraz
<g/>
)	)	kIx)	)
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
poté	poté	k6eAd1	poté
do	do	k7c2	do
přerušovací	přerušovací	k2eAgFnSc2d1	přerušovací
lázně	lázeň	k1gFnSc2	lázeň
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
zastaví	zastavit	k5eAaPmIp3nS	zastavit
chemické	chemický	k2eAgInPc4d1	chemický
procesy	proces	k1gInPc4	proces
vývojky	vývojka	k1gFnSc2	vývojka
a	a	k8xC	a
zajistí	zajistit	k5eAaPmIp3nS	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
na	na	k7c6	na
normálním	normální	k2eAgNnSc6d1	normální
světle	světlo	k1gNnSc6	světlo
dále	daleko	k6eAd2	daleko
neměnil	měnit	k5eNaImAgMnS	měnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
obraz	obraz	k1gInSc1	obraz
pověšen	pověsit	k5eAaPmNgInS	pověsit
a	a	k8xC	a
nechal	nechat	k5eAaPmAgInS	nechat
se	se	k3xPyFc4	se
usušit	usušit	k5eAaPmF	usušit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
uschnutí	uschnutí	k1gNnSc6	uschnutí
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
manipulovat	manipulovat	k5eAaImF	manipulovat
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
hrozilo	hrozit	k5eAaImAgNnS	hrozit
jeho	jeho	k3xOp3gNnSc1	jeho
snadné	snadný	k2eAgNnSc1d1	snadné
poškození	poškození	k1gNnSc1	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
zpracování	zpracování	k1gNnSc2	zpracování
umožnil	umožnit	k5eAaPmAgInS	umožnit
fotografovi	fotograf	k1gMnSc3	fotograf
manipulovat	manipulovat	k5eAaImF	manipulovat
s	s	k7c7	s
výsledným	výsledný	k2eAgInSc7d1	výsledný
vzhledem	vzhled	k1gInSc7	vzhled
fotografie	fotografia	k1gFnSc2	fotografia
–	–	k?	–
nebyl	být	k5eNaImAgInS	být
zcela	zcela	k6eAd1	zcela
svázán	svázat	k5eAaPmNgInS	svázat
podobou	podoba	k1gFnSc7	podoba
negativu	negativ	k1gInSc2	negativ
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
upravovat	upravovat	k5eAaImF	upravovat
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
po	po	k7c4	po
kterou	který	k3yRgFnSc4	který
byla	být	k5eAaImAgFnS	být
fotografie	fotografie	k1gFnSc1	fotografie
ve	v	k7c6	v
zvětšovači	zvětšovač	k1gInSc6	zvětšovač
i	i	k8xC	i
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
chemických	chemický	k2eAgInPc6d1	chemický
roztocích	roztok	k1gInPc6	roztok
a	a	k8xC	a
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
<g/>
,	,	kIx,	,
světlost	světlost	k1gFnSc4	světlost
a	a	k8xC	a
ostrost	ostrost	k1gFnSc4	ostrost
snímku	snímek	k1gInSc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Zpracování	zpracování	k1gNnSc1	zpracování
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
fotografií	fotografia	k1gFnPc2	fotografia
je	být	k5eAaImIp3nS	být
odvětví	odvětví	k1gNnSc1	odvětví
spíše	spíše	k9	spíše
profesionálního	profesionální	k2eAgInSc2d1	profesionální
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
výrobní	výrobní	k2eAgInPc1d1	výrobní
prostředky	prostředek	k1gInPc1	prostředek
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
vysoké	vysoký	k2eAgInPc1d1	vysoký
finanční	finanční	k2eAgInPc1d1	finanční
náklady	náklad	k1gInPc1	náklad
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
správné	správný	k2eAgFnSc3d1	správná
obsluze	obsluha	k1gFnSc3	obsluha
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
mít	mít	k5eAaImF	mít
určité	určitý	k2eAgNnSc4d1	určité
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
častou	častý	k2eAgFnSc7d1	častá
praxí	praxe	k1gFnSc7	praxe
objednávat	objednávat	k5eAaImF	objednávat
si	se	k3xPyFc3	se
zpracování	zpracování	k1gNnSc4	zpracování
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
fotografií	fotografia	k1gFnPc2	fotografia
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Poskytovatelé	poskytovatel	k1gMnPc1	poskytovatel
těchto	tento	k3xDgFnPc2	tento
služeb	služba	k1gFnPc2	služba
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
negativy	negativ	k1gInPc4	negativ
a	a	k8xC	a
diapozitivy	diapozitiv	k1gInPc4	diapozitiv
<g/>
,	,	kIx,	,
skenují	skenovat	k5eAaImIp3nP	skenovat
a	a	k8xC	a
upravují	upravovat	k5eAaImIp3nP	upravovat
skeny	skeen	k2eAgInPc1d1	skeen
i	i	k8xC	i
jiné	jiný	k2eAgInPc1d1	jiný
digitální	digitální	k2eAgInPc1d1	digitální
vstupy	vstup	k1gInPc1	vstup
<g/>
,	,	kIx,	,
provádějí	provádět	k5eAaImIp3nP	provádět
úpravu	úprava	k1gFnSc4	úprava
samotných	samotný	k2eAgFnPc2d1	samotná
fotografií	fotografia	k1gFnPc2	fotografia
jako	jako	k8xC	jako
i	i	k9	i
archivaci	archivace	k1gFnSc4	archivace
nebo	nebo	k8xC	nebo
digitalizaci	digitalizace	k1gFnSc4	digitalizace
archivů	archiv	k1gInPc2	archiv
(	(	kIx(	(
<g/>
k	k	k7c3	k
digitalizaci	digitalizace	k1gFnSc3	digitalizace
historických	historický	k2eAgInPc2d1	historický
fotografických	fotografický	k2eAgInPc2d1	fotografický
archivů	archiv	k1gInPc2	archiv
se	se	k3xPyFc4	se
v	v	k7c6	v
profesionální	profesionální	k2eAgFnSc6d1	profesionální
oblasti	oblast	k1gFnSc6	oblast
využívají	využívat	k5eAaPmIp3nP	využívat
např.	např.	kA	např.
bubnové	bubnový	k2eAgInPc4d1	bubnový
skenery	skener	k1gInPc4	skener
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
profesionální	profesionální	k2eAgMnPc1d1	profesionální
i	i	k8xC	i
amatérští	amatérský	k2eAgMnPc1d1	amatérský
fotografové	fotograf	k1gMnPc1	fotograf
si	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
a	a	k8xC	a
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
film	film	k1gInSc4	film
sami	sám	k3xTgMnPc1	sám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nástup	nástup	k1gInSc1	nástup
digitální	digitální	k2eAgFnSc2d1	digitální
fotografie	fotografia	k1gFnSc2	fotografia
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
fotografií	fotografia	k1gFnPc2	fotografia
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
pořízena	pořídit	k5eAaPmNgFnS	pořídit
digitálně	digitálně	k6eAd1	digitálně
a	a	k8xC	a
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
pomocí	pomocí	k7c2	pomocí
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tisk	tisk	k1gInSc4	tisk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
chemických	chemický	k2eAgFnPc6d1	chemická
reakcích	reakce	k1gFnPc6	reakce
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgFnPc1d1	digitální
fotografie	fotografia	k1gFnPc1	fotografia
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nahrány	nahrát	k5eAaPmNgInP	nahrát
na	na	k7c4	na
internetové	internetový	k2eAgFnPc4d1	internetová
fotogalerie	fotogalerie	k1gFnPc4	fotogalerie
<g/>
,	,	kIx,	,
prohlíženy	prohlížen	k2eAgMnPc4d1	prohlížen
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
nebo	nebo	k8xC	nebo
v	v	k7c6	v
digitálním	digitální	k2eAgInSc6d1	digitální
fotorámečku	fotorámeček	k1gInSc6	fotorámeček
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
zobrazení	zobrazení	k1gNnSc2	zobrazení
nemá	mít	k5eNaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
tisku	tisk	k1gInSc2	tisk
fotografie	fotografia	k1gFnSc2	fotografia
na	na	k7c4	na
papír	papír	k1gInSc4	papír
či	či	k8xC	či
speciální	speciální	k2eAgInSc4d1	speciální
fotopapír	fotopapír	k1gInSc4	fotopapír
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tiskárny	tiskárna	k1gFnSc2	tiskárna
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
lze	lze	k6eAd1	lze
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
finální	finální	k2eAgFnSc7d1	finální
verzí	verze	k1gFnSc7	verze
různě	různě	k6eAd1	různě
modifikovat	modifikovat	k5eAaBmF	modifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
možnosti	možnost	k1gFnPc1	možnost
manipulace	manipulace	k1gFnSc2	manipulace
se	s	k7c7	s
snímkem	snímek	k1gInSc7	snímek
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgFnPc4d1	podobná
možnostem	možnost	k1gFnPc3	možnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
při	při	k7c6	při
zachycování	zachycování	k1gNnSc6	zachycování
snímku	snímek	k1gInSc2	snímek
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgInPc1d1	jiný
jsou	být	k5eAaImIp3nP	být
jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
pro	pro	k7c4	pro
proces	proces	k1gInSc4	proces
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
možností	možnost	k1gFnPc2	možnost
manipulace	manipulace	k1gFnSc2	manipulace
s	s	k7c7	s
filmem	film	k1gInSc7	film
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
v	v	k7c6	v
digitálním	digitální	k2eAgInSc6d1	digitální
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
odlišným	odlišný	k2eAgInPc3d1	odlišný
efektům	efekt	k1gInPc3	efekt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dodging	dodging	k1gInSc1	dodging
(	(	kIx(	(
<g/>
zesvětlování	zesvětlování	k1gNnSc1	zesvětlování
<g/>
)	)	kIx)	)
a	a	k8xC	a
burning	burning	k1gInSc1	burning
(	(	kIx(	(
<g/>
ztmavování	ztmavování	k1gNnSc1	ztmavování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
výslednou	výsledný	k2eAgFnSc4d1	výsledná
fotografii	fotografia	k1gFnSc4	fotografia
mají	mít	k5eAaImIp3nP	mít
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
:	:	kIx,	:
Použité	použitý	k2eAgFnPc1d1	použitá
chemikálie	chemikálie	k1gFnPc1	chemikálie
a	a	k8xC	a
postupy	postup	k1gInPc1	postup
během	během	k7c2	během
vyvolávání	vyvolávání	k1gNnSc2	vyvolávání
filmu	film	k1gInSc2	film
Délka	délka	k1gFnSc1	délka
expozice	expozice	k1gFnSc2	expozice
tisku	tisk	k1gInSc2	tisk
Kontrast	kontrast	k1gInSc1	kontrast
Dodging	Dodging	k1gInSc1	Dodging
–	–	k?	–
zesvětlování	zesvětlování	k1gNnSc2	zesvětlování
určitých	určitý	k2eAgFnPc2d1	určitá
částí	část	k1gFnPc2	část
fotografie	fotografia	k1gFnSc2	fotografia
Burning	Burning	k1gInSc1	Burning
–	–	k?	–
ztmavování	ztmavování	k1gNnSc2	ztmavování
určitých	určitý	k2eAgFnPc2d1	určitá
částí	část	k1gFnPc2	část
fotografie	fotografie	k1gFnSc1	fotografie
Struktura	struktura	k1gFnSc1	struktura
papíru	papír	k1gInSc2	papír
–	–	k?	–
např.	např.	kA	např.
lesklý	lesklý	k2eAgMnSc1d1	lesklý
<g/>
,	,	kIx,	,
matný	matný	k2eAgInSc1d1	matný
Druh	druh	k1gInSc1	druh
papíru	papír	k1gInSc2	papír
–	–	k?	–
např.	např.	kA	např.
oboustranně	oboustranně	k6eAd1	oboustranně
laminovaný	laminovaný	k2eAgInSc4d1	laminovaný
papír	papír	k1gInSc4	papír
(	(	kIx(	(
<g/>
RC	RC	kA	RC
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
barytonový	barytonový	k2eAgMnSc1d1	barytonový
(	(	kIx(	(
<g/>
FB	FB	kA	FB
<g/>
)	)	kIx)	)
Velikost	velikost	k1gFnSc4	velikost
papíru	papír	k1gInSc2	papír
Tvar	tvar	k1gInSc1	tvar
snímku	snímek	k1gInSc2	snímek
–	–	k?	–
tisk	tisk	k1gInSc1	tisk
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
tvarech	tvar	k1gInPc6	tvar
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kolečko	kolečko	k1gNnSc4	kolečko
nebo	nebo	k8xC	nebo
ovál	ovál	k1gInSc4	ovál
Tónování	tónování	k1gNnSc2	tónování
–	–	k?	–
změna	změna	k1gFnSc1	změna
barvy	barva	k1gFnSc2	barva
černobílé	černobílý	k2eAgFnSc2d1	černobílá
fotografie	fotografia	k1gFnSc2	fotografia
(	(	kIx(	(
<g/>
např.	např.	kA	např.
přidáním	přidání	k1gNnSc7	přidání
teplejších	teplý	k2eAgFnPc2d2	teplejší
sépiových	sépiový	k2eAgFnPc2d1	sépiová
barev	barva	k1gFnPc2	barva
<g/>
)	)	kIx)	)
Technická	technický	k2eAgFnSc1d1	technická
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
odvětví	odvětví	k1gNnSc4	odvětví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
technickými	technický	k2eAgInPc7d1	technický
aspekty	aspekt	k1gInPc7	aspekt
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
:	:	kIx,	:
fotografická	fotografický	k2eAgFnSc1d1	fotografická
optika	optika	k1gFnSc1	optika
fotografická	fotografický	k2eAgFnSc1d1	fotografická
mechanika	mechanika	k1gFnSc1	mechanika
fotografická	fotografický	k2eAgFnSc1d1	fotografická
elektronika	elektronika	k1gFnSc1	elektronika
fotografická	fotografický	k2eAgFnSc1d1	fotografická
chemie	chemie	k1gFnSc1	chemie
digitální	digitální	k2eAgFnSc2d1	digitální
fotografie	fotografia	k1gFnSc2	fotografia
fotografický	fotografický	k2eAgInSc4d1	fotografický
software	software	k1gInSc4	software
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
amatérského	amatérský	k2eAgMnSc4d1	amatérský
fotografa	fotograf	k1gMnSc4	fotograf
je	být	k5eAaImIp3nS	být
fotografování	fotografování	k1gNnSc1	fotografování
koníčkem	koníček	k1gInSc7	koníček
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
výdělečnou	výdělečný	k2eAgFnSc7d1	výdělečná
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
některých	některý	k3yIgFnPc2	některý
amatérských	amatérský	k2eAgFnPc2d1	amatérská
prací	práce	k1gFnPc2	práce
je	být	k5eAaImIp3nS	být
však	však	k9	však
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
kvalitou	kvalita	k1gFnSc7	kvalita
mnohých	mnohý	k2eAgMnPc2d1	mnohý
profesionálů	profesionál	k1gMnPc2	profesionál
a	a	k8xC	a
amatérská	amatérský	k2eAgFnSc1d1	amatérská
fotografie	fotografie	k1gFnSc1	fotografie
často	často	k6eAd1	často
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jenom	jenom	k9	jenom
malá	malý	k2eAgFnSc1d1	malá
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
komerční	komerční	k2eAgNnPc4d1	komerční
využití	využití	k1gNnPc4	využití
či	či	k8xC	či
zisk	zisk	k1gInSc4	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
subjektů	subjekt	k1gInPc2	subjekt
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jak	jak	k8xS	jak
úzce	úzko	k6eAd1	úzko
specializovaná	specializovaný	k2eAgNnPc1d1	specializované
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
velmi	velmi	k6eAd1	velmi
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
.	.	kIx.	.
</s>
<s>
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
fotografie	fotografie	k1gFnSc1	fotografie
zažila	zažít	k5eAaPmAgFnS	zažít
rozmach	rozmach	k1gInSc4	rozmach
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
díky	díky	k7c3	díky
popularizaci	popularizace	k1gFnSc3	popularizace
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
držet	držet	k5eAaImF	držet
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
fotografování	fotografování	k1gNnSc3	fotografování
a	a	k8xC	a
šíření	šíření	k1gNnSc3	šíření
fotografií	fotografia	k1gFnPc2	fotografia
slouží	sloužit	k5eAaImIp3nS	sloužit
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
technologie	technologie	k1gFnPc4	technologie
a	a	k8xC	a
platformy	platforma	k1gFnPc4	platforma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jsou	být	k5eAaImIp3nP	být
vlivné	vlivný	k2eAgInPc4d1	vlivný
zejména	zejména	k9	zejména
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
v	v	k7c6	v
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
zpřístupnily	zpřístupnit	k5eAaPmAgInP	zpřístupnit
fotografování	fotografování	k1gNnSc4	fotografování
většímu	veliký	k2eAgNnSc3d2	veliký
množství	množství	k1gNnSc3	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
sociální	sociální	k2eAgNnPc1d1	sociální
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
která	který	k3yIgNnPc4	který
se	se	k3xPyFc4	se
fotografie	fotografia	k1gFnSc2	fotografia
šíří	šíř	k1gFnPc2	šíř
<g/>
.	.	kIx.	.
</s>
<s>
Komerční	komerční	k2eAgFnSc1d1	komerční
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
druh	druh	k1gInSc4	druh
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yRgInSc4	který
je	být	k5eAaImIp3nS	být
fotograf	fotograf	k1gMnSc1	fotograf
placen	platit	k5eAaImNgMnS	platit
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejsou	být	k5eNaImIp3nP	být
jeho	jeho	k3xOp3gFnPc4	jeho
fotografie	fotografia	k1gFnPc4	fotografia
viděny	viděn	k2eAgFnPc4d1	viděna
jako	jako	k8xS	jako
umělecké	umělecký	k2eAgNnSc1d1	umělecké
dílo	dílo	k1gNnSc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
kategorie	kategorie	k1gFnPc1	kategorie
jsou	být	k5eAaImIp3nP	být
příklady	příklad	k1gInPc4	příklad
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
komerční	komerční	k2eAgFnSc2d1	komerční
fotografie	fotografia	k1gFnSc2	fotografia
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
<g/>
:	:	kIx,	:
Reklamní	reklamní	k2eAgFnSc1d1	reklamní
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
dělaná	dělaný	k2eAgFnSc1d1	dělaná
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vyobrazit	vyobrazit	k5eAaPmF	vyobrazit
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
i	i	k9	i
prodat	prodat	k5eAaPmF	prodat
službu	služba	k1gFnSc4	služba
nebo	nebo	k8xC	nebo
produkt	produkt	k1gInSc4	produkt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
fotografie	fotografie	k1gFnSc1	fotografie
výrobku	výrobek	k1gInSc2	výrobek
snažící	snažící	k2eAgInSc1d1	snažící
se	se	k3xPyFc4	se
zobrazit	zobrazit	k5eAaPmF	zobrazit
ho	on	k3xPp3gNnSc4	on
v	v	k7c6	v
co	co	k9	co
nejlepším	dobrý	k2eAgMnSc6d3	nejlepší
světle	světle	k6eAd1	světle
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
snímky	snímek	k1gInPc1	snímek
zpravidla	zpravidla	k6eAd1	zpravidla
dělají	dělat	k5eAaImIp3nP	dělat
reklamní	reklamní	k2eAgFnPc1d1	reklamní
agentury	agentura	k1gFnPc1	agentura
<g/>
,	,	kIx,	,
designové	designový	k2eAgFnPc1d1	designová
firmy	firma	k1gFnPc1	firma
nebo	nebo	k8xC	nebo
oddělení	oddělení	k1gNnSc1	oddělení
pro	pro	k7c4	pro
design	design	k1gInSc4	design
působící	působící	k2eAgMnPc4d1	působící
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
firmách	firma	k1gFnPc6	firma
<g/>
.	.	kIx.	.
</s>
<s>
Módní	módní	k2eAgFnPc1d1	módní
fotografie	fotografia	k1gFnPc1	fotografia
a	a	k8xC	a
glamour	glamour	k1gMnSc1	glamour
jsou	být	k5eAaImIp3nP	být
druhem	druh	k1gMnSc7	druh
reklamní	reklamní	k2eAgFnSc2d1	reklamní
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zpravidla	zpravidla	k6eAd1	zpravidla
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
modela	model	k1gMnSc4	model
nebo	nebo	k8xC	nebo
modelku	modelka	k1gFnSc4	modelka
<g/>
.	.	kIx.	.
</s>
<s>
Módní	módní	k2eAgFnPc1d1	módní
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
například	například	k6eAd1	například
americký	americký	k2eAgInSc1d1	americký
časopis	časopis	k1gInSc1	časopis
Harper	Harpra	k1gFnPc2	Harpra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bazaar	Bazaar	k1gInSc1	Bazaar
<g/>
,	,	kIx,	,
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
oblečení	oblečení	k1gNnSc4	oblečení
a	a	k8xC	a
doplňky	doplněk	k1gInPc4	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
glamour	glamour	k1gMnSc1	glamour
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
modela	model	k1gMnSc4	model
nebo	nebo	k8xC	nebo
modelku	modelka	k1gFnSc4	modelka
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
někdy	někdy	k6eAd1	někdy
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
akty	akt	k1gInPc4	akt
<g/>
.	.	kIx.	.
</s>
<s>
Módní	módní	k2eAgFnPc1d1	módní
fotografie	fotografia	k1gFnPc1	fotografia
i	i	k8xC	i
glamour	glamour	k1gMnSc1	glamour
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgMnPc1d1	populární
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Koncertní	koncertní	k2eAgFnSc1d1	koncertní
fotografie	fotografie	k1gFnSc1	fotografie
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
zachycování	zachycování	k1gNnSc4	zachycování
nearanžovaných	aranžovaný	k2eNgInPc2d1	aranžovaný
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
předmětem	předmět	k1gInSc7	předmět
je	být	k5eAaImIp3nS	být
jak	jak	k6eAd1	jak
umělec	umělec	k1gMnSc1	umělec
nebo	nebo	k8xC	nebo
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
diváci	divák	k1gMnPc1	divák
a	a	k8xC	a
atmosféra	atmosféra	k1gFnSc1	atmosféra
koncertu	koncert	k1gInSc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
koncertních	koncertní	k2eAgMnPc2d1	koncertní
fotografů	fotograf	k1gMnPc2	fotograf
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
a	a	k8xC	a
získávají	získávat	k5eAaImIp3nP	získávat
zakázky	zakázka	k1gFnPc4	zakázka
přímo	přímo	k6eAd1	přímo
od	od	k7c2	od
umělce	umělec	k1gMnSc2	umělec
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gMnSc2	jejich
manažera	manažer	k1gMnSc2	manažer
k	k	k7c3	k
fotografování	fotografování	k1gNnSc3	fotografování
určité	určitý	k2eAgFnSc2d1	určitá
akce	akce	k1gFnSc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
propagace	propagace	k1gFnSc2	propagace
dějiště	dějiště	k1gNnSc1	dějiště
koncertu	koncert	k1gInSc2	koncert
slouží	sloužit	k5eAaImIp3nS	sloužit
koncertní	koncertní	k2eAgFnSc1d1	koncertní
fotografie	fotografie	k1gFnSc1	fotografie
i	i	k9	i
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
umělce	umělec	k1gMnSc2	umělec
nebo	nebo	k8xC	nebo
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
zátiší	zátiší	k1gNnSc2	zátiší
zpravidla	zpravidla	k6eAd1	zpravidla
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
neživé	živý	k2eNgInPc1d1	neživý
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jak	jak	k6eAd1	jak
přírodní	přírodní	k2eAgFnPc4d1	přírodní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
dílem	dílo	k1gNnSc7	dílo
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
jídla	jídlo	k1gNnSc2	jídlo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využita	využít	k5eAaPmNgFnS	využít
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
a	a	k8xC	a
časopisech	časopis	k1gInPc6	časopis
<g/>
,	,	kIx,	,
na	na	k7c6	na
obalech	obal	k1gInPc6	obal
nebo	nebo	k8xC	nebo
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
fotografii	fotografia	k1gFnSc4	fotografia
zátiší	zátiší	k1gNnSc2	zátiší
<g/>
.	.	kIx.	.
</s>
<s>
Fotožurnalismus	Fotožurnalismus	k1gInSc1	Fotožurnalismus
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
doplnění	doplnění	k1gNnSc3	doplnění
zpráv	zpráva	k1gFnPc2	zpráva
nebo	nebo	k8xC	nebo
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
podkategorií	podkategorie	k1gFnPc2	podkategorie
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
paparazzi	paparazh	k1gMnPc1	paparazh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
fotografy	fotograf	k1gMnPc4	fotograf
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
nearanžované	aranžovaný	k2eNgFnPc4d1	aranžovaný
fotografie	fotografia	k1gFnPc4	fotografia
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
celebrit	celebrita	k1gFnPc2	celebrita
<g/>
,	,	kIx,	,
politiků	politik	k1gMnPc2	politik
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
významných	významný	k2eAgMnPc2d1	významný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
bez	bez	k7c2	bez
jejich	jejich	k3xOp3gNnSc2	jejich
svolení	svolení	k1gNnSc2	svolení
<g/>
.	.	kIx.	.
</s>
<s>
Konceptuální	konceptuální	k2eAgFnSc1d1	konceptuální
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
myšlence	myšlenka	k1gFnSc6	myšlenka
nebo	nebo	k8xC	nebo
představě	představa	k1gFnSc6	představa
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
skutečné	skutečný	k2eAgInPc4d1	skutečný
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
abstraktní	abstraktní	k2eAgMnSc1d1	abstraktní
<g/>
.	.	kIx.	.
</s>
<s>
Makrofotografie	makrofotografie	k1gFnSc1	makrofotografie
je	být	k5eAaImIp3nS	být
fotografování	fotografování	k1gNnSc4	fotografování
detailů	detail	k1gInPc2	detail
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
daný	daný	k2eAgInSc1d1	daný
objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
na	na	k7c6	na
filmu	film	k1gInSc6	film
či	či	k8xC	či
snímači	snímač	k1gInSc6	snímač
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
makrofotografie	makrofotografie	k1gFnSc1	makrofotografie
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
snímky	snímek	k1gInPc4	snímek
drobných	drobný	k2eAgMnPc2d1	drobný
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
motýli	motýl	k1gMnPc1	motýl
nebo	nebo	k8xC	nebo
mouchy	moucha	k1gFnPc1	moucha
<g/>
,	,	kIx,	,
či	či	k8xC	či
fotografii	fotografia	k1gFnSc4	fotografia
malých	malý	k2eAgFnPc2d1	malá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Portrétní	portrétní	k2eAgFnSc1d1	portrétní
a	a	k8xC	a
svatební	svatební	k2eAgFnSc1d1	svatební
fotografie	fotografie	k1gFnSc1	fotografie
Krajinářská	krajinářský	k2eAgFnSc1d1	krajinářská
fotografie	fotografie	k1gFnSc1	fotografie
Fotografie	fotografia	k1gFnSc2	fotografia
divoké	divoký	k2eAgFnSc2d1	divoká
přírody	příroda	k1gFnSc2	příroda
Trh	trh	k1gInSc1	trh
pro	pro	k7c4	pro
fotografické	fotografický	k2eAgFnPc4d1	fotografická
služby	služba	k1gFnPc4	služba
ztělesňuje	ztělesňovat	k5eAaImIp3nS	ztělesňovat
aforismus	aforismus	k1gInSc1	aforismus
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
obraz	obraz	k1gInSc1	obraz
často	často	k6eAd1	často
řekne	říct	k5eAaPmIp3nS	říct
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíc	tisíc	k4xCgInSc4	tisíc
slov	slovo	k1gNnPc2	slovo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
fotí	fotit	k5eAaImIp3nS	fotit
pro	pro	k7c4	pro
komerční	komerční	k2eAgInPc4d1	komerční
účely	účel	k1gInPc4	účel
a	a	k8xC	a
za	za	k7c4	za
jejich	jejich	k3xOp3gInPc4	jejich
snímky	snímek	k1gInPc4	snímek
platí	platit	k5eAaImIp3nS	platit
řada	řada	k1gFnSc1	řada
časopisů	časopis	k1gInPc2	časopis
<g/>
,	,	kIx,	,
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
reklamních	reklamní	k2eAgFnPc2d1	reklamní
agentur	agentura	k1gFnPc2	agentura
nebo	nebo	k8xC	nebo
firem	firma	k1gFnPc2	firma
provozujících	provozující	k2eAgFnPc2d1	provozující
internetové	internetový	k2eAgFnPc4d1	internetová
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
fotografie	fotografia	k1gFnPc4	fotografia
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
rozpočet	rozpočet	k1gInSc4	rozpočet
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
z	z	k7c2	z
několika	několik	k4yIc2	několik
možností	možnost	k1gFnPc2	možnost
<g/>
:	:	kIx,	:
mohou	moct	k5eAaImIp3nP	moct
fotografa	fotograf	k1gMnSc2	fotograf
přímo	přímo	k6eAd1	přímo
zaměstnat	zaměstnat	k5eAaPmF	zaměstnat
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
soutěž	soutěž	k1gFnSc4	soutěž
nebo	nebo	k8xC	nebo
k	k	k7c3	k
fotografiím	fotografia	k1gFnPc3	fotografia
získat	získat	k5eAaPmF	získat
práva	právo	k1gNnPc4	právo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
fotobank	fotobanka	k1gFnPc2	fotobanka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tradičním	tradiční	k2eAgFnPc3d1	tradiční
velkým	velký	k2eAgFnPc3d1	velká
fotobankám	fotobanka	k1gFnPc3	fotobanka
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
například	například	k6eAd1	například
Getty	Getta	k1gFnPc4	Getta
Images	Imagesa	k1gFnPc2	Imagesa
a	a	k8xC	a
Corbys	Corbysa	k1gFnPc2	Corbysa
<g/>
,	,	kIx,	,
k	k	k7c3	k
menším	malý	k2eAgFnPc3d2	menší
mikrofotobankám	mikrofotobanka	k1gFnPc3	mikrofotobanka
například	například	k6eAd1	například
Fotolia	Fotolium	k1gNnSc2	Fotolium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
internetová	internetový	k2eAgNnPc1d1	internetové
tržiště	tržiště	k1gNnPc1	tržiště
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
Cutcaster	Cutcastra	k1gFnPc2	Cutcastra
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
umělecká	umělecký	k2eAgFnSc1d1	umělecká
i	i	k8xC	i
dokumentární	dokumentární	k2eAgFnSc1d1	dokumentární
fotografie	fotografie	k1gFnSc1	fotografie
přijata	přijat	k2eAgFnSc1d1	přijata
anglicky	anglicky	k6eAd1	anglicky
hovořícím	hovořící	k2eAgInSc7d1	hovořící
uměleckým	umělecký	k2eAgInSc7d1	umělecký
světem	svět	k1gInSc7	svět
a	a	k8xC	a
galeriemi	galerie	k1gFnPc7	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
několik	několik	k4yIc1	několik
fotografů	fotograf	k1gMnPc2	fotograf
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Alfred	Alfred	k1gMnSc1	Alfred
Stiglitz	Stiglitz	k1gMnSc1	Stiglitz
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
Steichen	Steichen	k1gInSc1	Steichen
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Szarkowski	Szarkowsk	k1gFnSc2	Szarkowsk
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Holland	Holland	k1gInSc1	Holland
Day	Day	k1gFnSc1	Day
nebo	nebo	k8xC	nebo
Edward	Edward	k1gMnSc1	Edward
Weston	Weston	k1gInSc4	Weston
<g/>
,	,	kIx,	,
zasvětilo	zasvětit	k5eAaPmAgNnS	zasvětit
své	svůj	k3xOyFgInPc4	svůj
životy	život	k1gInPc4	život
boji	boj	k1gInSc3	boj
za	za	k7c4	za
přijetí	přijetí	k1gNnSc4	přijetí
fotografie	fotografia	k1gFnSc2	fotografia
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Umělecká	umělecký	k2eAgFnSc1d1	umělecká
fotografie	fotografie	k1gFnSc1	fotografie
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
počátcích	počátek	k1gInPc6	počátek
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
piktorialismem	piktorialismus	k1gInSc7	piktorialismus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
styl	styl	k1gInSc1	styl
snažící	snažící	k2eAgInSc1d1	snažící
se	se	k3xPyFc4	se
napodobit	napodobit	k5eAaPmF	napodobit
malbu	malba	k1gFnSc4	malba
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
malé	malý	k2eAgFnSc2d1	malá
hloubky	hloubka	k1gFnSc2	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
,	,	kIx,	,
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
snového	snový	k2eAgInSc2d1	snový
<g/>
,	,	kIx,	,
romantického	romantický	k2eAgInSc2d1	romantický
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
piktorialismus	piktorialismus	k1gInSc4	piktorialismus
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Skupina	skupina	k1gFnSc1	skupina
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
64	[number]	k4	64
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
založení	založení	k1gNnSc6	založení
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
například	například	k6eAd1	například
Edward	Edward	k1gMnSc1	Edward
Weston	Weston	k1gInSc1	Weston
a	a	k8xC	a
Ansel	Ansel	k1gInSc1	Ansel
Adams	Adamsa	k1gFnPc2	Adamsa
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
64	[number]	k4	64
propagovala	propagovat	k5eAaImAgFnS	propagovat
"	"	kIx"	"
<g/>
přímou	přímý	k2eAgFnSc4d1	přímá
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
ostrost	ostrost	k1gFnSc1	ostrost
a	a	k8xC	a
pojetí	pojetí	k1gNnSc1	pojetí
fotografie	fotografia	k1gFnSc2	fotografia
jako	jako	k8xS	jako
jedinečného	jedinečný	k2eAgNnSc2d1	jedinečné
média	médium	k1gNnSc2	médium
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
snahy	snaha	k1gFnSc2	snaha
napodobovat	napodobovat	k5eAaImF	napodobovat
jiné	jiný	k2eAgInPc4d1	jiný
umělecké	umělecký	k2eAgInPc4d1	umělecký
směry	směr	k1gInPc4	směr
a	a	k8xC	a
řemesla	řemeslo	k1gNnPc4	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Estetika	estetika	k1gFnSc1	estetika
fotografie	fotografia	k1gFnSc2	fotografia
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
uměleckých	umělecký	k2eAgInPc6d1	umělecký
kruzích	kruh	k1gInPc6	kruh
<g/>
,	,	kIx,	,
pravidelně	pravidelně	k6eAd1	pravidelně
probíraným	probíraný	k2eAgNnSc7d1	probírané
tématem	téma	k1gNnSc7	téma
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
umělců	umělec	k1gMnPc2	umělec
dříve	dříve	k6eAd2	dříve
zastávalo	zastávat	k5eAaImAgNnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
reprodukcí	reprodukce	k1gFnSc7	reprodukce
obrazu	obraz	k1gInSc2	obraz
a	a	k8xC	a
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
skutečným	skutečný	k2eAgNnSc7d1	skutečné
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
umění	umění	k1gNnSc2	umění
předefinována	předefinován	k2eAgMnSc4d1	předefinován
–	–	k?	–
například	například	k6eAd1	například
stanovením	stanovení	k1gNnSc7	stanovení
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dělají	dělat	k5eAaImIp3nP	dělat
pro	pro	k7c4	pro
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
fotografii	fotografia	k1gFnSc4	fotografia
krásnou	krásný	k2eAgFnSc4d1	krásná
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
polemika	polemika	k1gFnSc1	polemika
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
úplně	úplně	k6eAd1	úplně
prvních	první	k4xOgFnPc6	první
fotografiích	fotografia	k1gFnPc6	fotografia
"	"	kIx"	"
<g/>
psaných	psaný	k2eAgFnPc2d1	psaná
světlem	světlo	k1gNnSc7	světlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nicéphore	Nicéphor	k1gMnSc5	Nicéphor
Niécpe	Niécp	k1gMnSc5	Niécp
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
Daguerre	Daguerr	k1gInSc5	Daguerr
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
fotografů	fotograf	k1gMnPc2	fotograf
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
setkávali	setkávat	k5eAaImAgMnP	setkávat
s	s	k7c7	s
uznáním	uznání	k1gNnSc7	uznání
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
někteří	některý	k3yIgMnPc1	některý
zpochybňovali	zpochybňovat	k5eAaImAgMnP	zpochybňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
práce	práce	k1gFnSc1	práce
naplňuje	naplňovat	k5eAaImIp3nS	naplňovat
definice	definice	k1gFnPc4	definice
a	a	k8xC	a
cíle	cíl	k1gInPc4	cíl
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Clive	Clivat	k5eAaPmIp3nS	Clivat
Bell	bell	k1gInSc1	bell
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
eseji	esej	k1gFnSc6	esej
Art	Art	k1gMnSc3	Art
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
významová	významový	k2eAgFnSc1d1	významová
forma	forma	k1gFnSc1	forma
<g/>
"	"	kIx"	"
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
umění	umění	k1gNnSc1	umění
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
uměním	umění	k1gNnSc7	umění
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nP	muset
existovat	existovat	k5eAaImF	existovat
jeden	jeden	k4xCgInSc4	jeden
určitý	určitý	k2eAgInSc4d1	určitý
rys	rys	k1gInSc4	rys
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
umělecké	umělecký	k2eAgNnSc1d1	umělecké
dílo	dílo	k1gNnSc1	dílo
neobejde	obejde	k6eNd1	obejde
–	–	k?	–
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
dílo	dílo	k1gNnSc1	dílo
tento	tento	k3xDgInSc4	tento
rys	rys	k1gInSc1	rys
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
bezcenné	bezcenný	k2eAgNnSc1d1	bezcenné
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
rysem	rys	k1gInSc7	rys
<g/>
?	?	kIx.	?
</s>
<s>
Které	který	k3yRgInPc1	který
rysy	rys	k1gInPc1	rys
jsou	být	k5eAaImIp3nP	být
sdílené	sdílený	k2eAgInPc1d1	sdílený
všemi	všecek	k3xTgInPc7	všecek
předměty	předmět	k1gInPc7	předmět
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
v	v	k7c4	v
nás	my	k3xPp1nPc4	my
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
estetické	estetický	k2eAgFnPc1d1	estetická
emoce	emoce	k1gFnPc1	emoce
<g/>
?	?	kIx.	?
</s>
<s>
Které	který	k3yRgInPc4	který
rysy	rys	k1gInPc4	rys
sdílí	sdílet	k5eAaImIp3nS	sdílet
Hagia	Hagia	k1gFnSc1	Hagia
Sofia	Sofia	k1gFnSc1	Sofia
s	s	k7c7	s
okny	okno	k1gNnPc7	okno
katedrály	katedrála	k1gFnSc2	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
<g/>
,	,	kIx,	,
mexickou	mexický	k2eAgFnSc7d1	mexická
sochou	socha	k1gFnSc7	socha
<g/>
,	,	kIx,	,
perskou	perský	k2eAgFnSc7d1	perská
mísou	mísa	k1gFnSc7	mísa
<g/>
,	,	kIx,	,
čínskými	čínský	k2eAgInPc7d1	čínský
koberci	koberec	k1gInPc7	koberec
<g/>
,	,	kIx,	,
Giottovými	Giottův	k2eAgFnPc7d1	Giottova
freskami	freska	k1gFnPc7	freska
v	v	k7c6	v
Padově	Padova	k1gFnSc6	Padova
a	a	k8xC	a
mistrovskými	mistrovský	k2eAgNnPc7d1	mistrovské
díly	dílo	k1gNnPc7	dílo
Poussina	Poussin	k2eAgFnSc1d1	Poussina
<g/>
,	,	kIx,	,
Piero	Piero	k1gNnSc1	Piero
della	dell	k1gMnSc2	dell
Francesca	Francescus	k1gMnSc2	Francescus
a	a	k8xC	a
Cézanna	Cézann	k1gMnSc2	Cézann
<g/>
?	?	kIx.	?
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
připadá	připadat	k5eAaPmIp3nS	připadat
jenom	jenom	k9	jenom
jedna	jeden	k4xCgFnSc1	jeden
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
:	:	kIx,	:
významová	významový	k2eAgFnSc1d1	významová
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
linie	linie	k1gFnPc1	linie
a	a	k8xC	a
barvy	barva	k1gFnPc1	barva
zkombinovaly	zkombinovat	k5eAaPmAgInP	zkombinovat
specifickým	specifický	k2eAgInSc7d1	specifický
způsobem	způsob	k1gInSc7	způsob
a	a	k8xC	a
určité	určitý	k2eAgFnPc4d1	určitá
formy	forma	k1gFnPc4	forma
a	a	k8xC	a
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
probouzí	probouzet	k5eAaImIp3nP	probouzet
naše	náš	k3xOp1gFnPc1	náš
estetické	estetický	k2eAgFnPc1d1	estetická
emoce	emoce	k1gFnPc1	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
má	mít	k5eAaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
a	a	k8xC	a
významnou	významný	k2eAgFnSc4d1	významná
historii	historie	k1gFnSc4	historie
zaznamenávání	zaznamenávání	k1gNnSc2	zaznamenávání
vědeckých	vědecký	k2eAgInPc2d1	vědecký
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
kořeny	kořen	k1gInPc1	kořen
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
Daguerrovi	Daguerrův	k2eAgMnPc1d1	Daguerrův
a	a	k8xC	a
Fox-Talbotovy	Fox-Talbotův	k2eAgFnPc1d1	Fox-Talbotův
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
záznamům	záznam	k1gInPc3	záznam
astronomických	astronomický	k2eAgFnPc2d1	astronomická
událostí	událost	k1gFnPc2	událost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zatmění	zatmění	k1gNnSc4	zatmění
<g/>
)	)	kIx)	)
i	i	k9	i
makrosnímkům	makrosnímek	k1gMnPc3	makrosnímek
malých	malý	k2eAgFnPc2d1	malá
tvorů	tvor	k1gMnPc2	tvor
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
skutečně	skutečně	k6eAd1	skutečně
malých	malý	k2eAgInPc2d1	malý
subjektů	subjekt	k1gInPc2	subjekt
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
mikrofotografie	mikrofotografie	k1gFnSc1	mikrofotografie
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
připevnění	připevnění	k1gNnSc1	připevnění
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
na	na	k7c4	na
mikroskop	mikroskop	k1gInSc4	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
se	se	k3xPyFc4	se
také	také	k9	také
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
užitečnou	užitečný	k2eAgFnSc4d1	užitečná
při	pře	k1gFnSc4	pře
zaznamenávání	zaznamenávání	k1gNnSc2	zaznamenávání
místa	místo	k1gNnSc2	místo
zločinu	zločin	k1gInSc2	zločin
nebo	nebo	k8xC	nebo
nehody	nehoda	k1gFnSc2	nehoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
Wootonského	Wootonský	k2eAgInSc2d1	Wootonský
mostu	most	k1gInSc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Analýzou	analýza	k1gFnSc7	analýza
fotografií	fotografia	k1gFnPc2	fotografia
pro	pro	k7c4	pro
právní	právní	k2eAgInPc4d1	právní
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
zabývá	zabývat	k5eAaImIp3nS	zabývat
forenzní	forenzní	k2eAgFnPc4d1	forenzní
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
fotografie	fotografia	k1gFnPc1	fotografia
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
činu	čin	k1gInSc2	čin
jsou	být	k5eAaImIp3nP	být
pořizované	pořizovaný	k2eAgFnPc1d1	pořizovaná
alespoň	alespoň	k9	alespoň
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
jak	jak	k6eAd1	jak
celkový	celkový	k2eAgInSc4d1	celkový
pohled	pohled	k1gInSc4	pohled
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
užší	úzký	k2eAgInSc1d2	užší
pohled	pohled	k1gInSc1	pohled
včetně	včetně	k7c2	včetně
detailu	detail	k1gInSc2	detail
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zachycení	zachycení	k1gNnSc3	zachycení
specifických	specifický	k2eAgInPc2d1	specifický
detailů	detail	k1gInPc2	detail
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
černobílá	černobílý	k2eAgFnSc1d1	černobílá
nebo	nebo	k8xC	nebo
infračervená	infračervený	k2eAgFnSc1d1	infračervená
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
Francis	Francis	k1gFnSc4	Francis
Ronalds	Ronaldsa	k1gFnPc2	Ronaldsa
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
ředitel	ředitel	k1gMnSc1	ředitel
londýnské	londýnský	k2eAgFnSc2d1	londýnská
hvězdárny	hvězdárna	k1gFnSc2	hvězdárna
Kew	Kew	k1gFnSc7	Kew
Observatory	Observator	k1gInPc4	Observator
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
schopný	schopný	k2eAgInSc1d1	schopný
bez	bez	k1gInSc1	bez
přestání	přestání	k1gNnSc4	přestání
zaznamenávat	zaznamenávat	k5eAaImF	zaznamenávat
meteorologické	meteorologický	k2eAgInPc4d1	meteorologický
a	a	k8xC	a
geomagnetické	geomagnetický	k2eAgInPc4d1	geomagnetický
parametry	parametr	k1gInPc4	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
stroje	stroj	k1gInPc1	stroj
tvořily	tvořit	k5eAaImAgInP	tvořit
12-	[number]	k4	12-
nebo	nebo	k8xC	nebo
24	[number]	k4	24
<g/>
hodinové	hodinový	k2eAgInPc1d1	hodinový
fotografické	fotografický	k2eAgInPc1d1	fotografický
záznamy	záznam	k1gInPc1	záznam
minutu	minuta	k1gFnSc4	minuta
po	po	k7c6	po
minutě	minuta	k1gFnSc6	minuta
se	se	k3xPyFc4	se
měnících	měnící	k2eAgFnPc2d1	měnící
hodnot	hodnota	k1gFnPc2	hodnota
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
,	,	kIx,	,
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
tří	tři	k4xCgFnPc2	tři
složek	složka	k1gFnPc2	složka
magnetických	magnetický	k2eAgFnPc2d1	magnetická
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
byly	být	k5eAaImAgInP	být
dodány	dodat	k5eAaPmNgInP	dodat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
hvězdáren	hvězdárna	k1gFnPc2	hvězdárna
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
někde	někde	k6eAd1	někde
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
využívané	využívaný	k2eAgFnPc1d1	využívaná
i	i	k9	i
po	po	k7c4	po
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
Ronaldsovi	Ronalds	k1gMnSc6	Ronalds
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
podobné	podobný	k2eAgInPc4d1	podobný
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
Královskou	královský	k2eAgFnSc4d1	královská
greenwichskou	greenwichský	k2eAgFnSc4d1	greenwichská
observatoř	observatoř	k1gFnSc4	observatoř
Charles	Charles	k1gMnSc1	Charles
Brooke	Brook	k1gInSc2	Brook
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
využívá	využívat	k5eAaPmIp3nS	využívat
obrazové	obrazový	k2eAgFnPc4d1	obrazová
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
konstrukce	konstrukce	k1gFnSc2	konstrukce
dírkové	dírkový	k2eAgFnSc2d1	dírková
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
všudypřítomnou	všudypřítomný	k2eAgFnSc4d1	všudypřítomná
při	pře	k1gFnSc4	pře
záznamu	záznam	k1gInSc2	záznam
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
dat	datum	k1gNnPc2	datum
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
a	a	k8xC	a
technických	technický	k2eAgInPc6d1	technický
oborech	obor	k1gInPc6	obor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
i	i	k9	i
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
zločinu	zločin	k1gInSc2	zločin
a	a	k8xC	a
nehod	nehoda	k1gFnPc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
využití	využití	k1gNnSc2	využití
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
díky	díky	k7c3	díky
využití	využití	k1gNnSc3	využití
jiných	jiný	k2eAgFnPc2d1	jiná
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
infračervené	infračervený	k2eAgFnSc2d1	infračervená
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
ultrafialové	ultrafialový	k2eAgFnSc2d1	ultrafialová
fotografie	fotografia	k1gFnSc2	fotografia
nebo	nebo	k8xC	nebo
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
metody	metoda	k1gFnPc1	metoda
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
použity	použít	k5eAaPmNgFnP	použít
ve	v	k7c6	v
viktoriánském	viktoriánský	k2eAgNnSc6d1	viktoriánské
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
významně	významně	k6eAd1	významně
vylepšeny	vylepšen	k2eAgFnPc1d1	vylepšena
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
zobrazovací	zobrazovací	k2eAgFnPc4d1	zobrazovací
technologie	technologie	k1gFnPc4	technologie
a	a	k8xC	a
už	už	k6eAd1	už
od	od	k7c2	od
svých	svůj	k3xOyFgInPc2	svůj
počátků	počátek	k1gInPc2	počátek
se	se	k3xPyFc4	se
těší	těšit	k5eAaImIp3nS	těšit
velkému	velký	k2eAgInSc3d1	velký
zájmu	zájem	k1gInSc3	zájem
vědců	vědec	k1gMnPc2	vědec
i	i	k8xC	i
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
používají	používat	k5eAaImIp3nP	používat
fotografii	fotografia	k1gFnSc4	fotografia
pro	pro	k7c4	pro
přesný	přesný	k2eAgInSc4d1	přesný
a	a	k8xC	a
věrný	věrný	k2eAgInSc4d1	věrný
záznam	záznam	k1gInSc4	záznam
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Eadweard	Eadweard	k1gInSc1	Eadweard
Muybridge	Muybridge	k1gInSc1	Muybridge
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
o	o	k7c6	o
lidských	lidský	k2eAgInPc6d1	lidský
a	a	k8xC	a
zvířecích	zvířecí	k2eAgInPc6d1	zvířecí
pohybech	pohyb	k1gInPc6	pohyb
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Umělci	umělec	k1gMnPc1	umělec
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
zajímali	zajímat	k5eAaImAgMnP	zajímat
o	o	k7c4	o
věrnost	věrnost	k1gFnSc4	věrnost
zobrazení	zobrazení	k1gNnSc2	zobrazení
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
také	také	k9	také
další	další	k2eAgFnPc4d1	další
možnosti	možnost	k1gFnPc4	možnost
než	než	k8xS	než
jen	jen	k9	jen
pouhé	pouhý	k2eAgNnSc1d1	pouhé
zachycování	zachycování	k1gNnSc1	zachycování
obrazu	obraz	k1gInSc2	obraz
reálného	reálný	k2eAgInSc2d1	reálný
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Fotografii	fotografia	k1gFnSc4	fotografia
začala	začít	k5eAaPmAgFnS	začít
brzy	brzy	k6eAd1	brzy
využívat	využívat	k5eAaPmF	využívat
také	také	k9	také
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnPc1d1	bezpečnostní
složky	složka	k1gFnPc1	složka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
fotografie	fotografie	k1gFnSc1	fotografie
nepřehlédnutelně	přehlédnutelně	k6eNd1	přehlédnutelně
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
médiích	médium	k1gNnPc6	médium
(	(	kIx(	(
<g/>
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
internet	internet	k1gInSc1	internet
<g/>
,	,	kIx,	,
TV	TV	kA	TV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
v	v	k7c6	v
marketingu	marketing	k1gInSc6	marketing
a	a	k8xC	a
reklamě	reklama	k1gFnSc3	reklama
a	a	k8xC	a
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
v	v	k7c6	v
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
možná	možná	k9	možná
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
její	její	k3xOp3gInSc4	její
další	další	k2eAgInSc4d1	další
rozmach	rozmach	k1gInSc4	rozmach
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
přetrvávajících	přetrvávající	k2eAgFnPc2d1	přetrvávající
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
různých	různý	k2eAgInPc2d1	různý
aspektů	aspekt	k1gInPc2	aspekt
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Susan	Susan	k1gInSc1	Susan
Sontagová	Sontagový	k2eAgFnSc1d1	Sontagová
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
O	o	k7c6	o
fotografii	fotografia	k1gFnSc6	fotografia
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
zabývá	zabývat	k5eAaImIp3nS	zabývat
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
hojně	hojně	k6eAd1	hojně
diskutovaných	diskutovaný	k2eAgInPc2d1	diskutovaný
problémů	problém	k1gInPc2	problém
ve	v	k7c6	v
fotografické	fotografický	k2eAgFnSc6d1	fotografická
komunitě	komunita	k1gFnSc6	komunita
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
znepokojením	znepokojení	k1gNnSc7	znepokojení
ohledně	ohledně	k7c2	ohledně
objektivity	objektivita	k1gFnSc2	objektivita
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Sontagová	Sontagový	k2eAgFnSc1d1	Sontagová
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Fotografovat	fotografovat	k5eAaImF	fotografovat
znamená	znamenat	k5eAaImIp3nS	znamenat
přivlastnit	přivlastnit	k5eAaPmF	přivlastnit
si	se	k3xPyFc3	se
fotografovaný	fotografovaný	k2eAgInSc4d1	fotografovaný
objekt	objekt	k1gInSc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
do	do	k7c2	do
určitého	určitý	k2eAgInSc2d1	určitý
vztahu	vztah	k1gInSc2	vztah
se	s	k7c7	s
světem	svět	k1gInSc7	svět
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
vztah	vztah	k1gInSc1	vztah
mu	on	k3xPp3gMnSc3	on
dává	dávat	k5eAaImIp3nS	dávat
pocit	pocit	k1gInSc4	pocit
vědění	vědění	k1gNnSc2	vědění
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
moci	moct	k5eAaImF	moct
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Fotograf	fotograf	k1gMnSc1	fotograf
činí	činit	k5eAaImIp3nS	činit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
ohledně	ohledně	k7c2	ohledně
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
fotografovat	fotografovat	k5eAaImF	fotografovat
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
prvky	prvek	k1gInPc1	prvek
ze	z	k7c2	z
snímku	snímek	k1gInSc2	snímek
vyloučit	vyloučit	k5eAaPmF	vyloučit
nebo	nebo	k8xC	nebo
z	z	k7c2	z
jakého	jaký	k3yQgInSc2	jaký
úhlu	úhel	k1gInSc2	úhel
objekty	objekt	k1gInPc4	objekt
zachytit	zachytit	k5eAaPmF	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
mohou	moct	k5eAaImIp3nP	moct
odrážet	odrážet	k5eAaImF	odrážet
určitý	určitý	k2eAgInSc4d1	určitý
socio-historický	socioistorický	k2eAgInSc4d1	socio-historický
kontext	kontext	k1gInSc4	kontext
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
subjektivní	subjektivní	k2eAgFnSc7d1	subjektivní
metodou	metoda	k1gFnSc7	metoda
znázornění	znázornění	k1gNnSc2	znázornění
<g/>
.	.	kIx.	.
</s>
<s>
Sontagová	Sontagový	k2eAgFnSc1d1	Sontagová
se	se	k3xPyFc4	se
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
O	o	k7c6	o
fotografii	fotografia	k1gFnSc6	fotografia
věnuje	věnovat	k5eAaPmIp3nS	věnovat
i	i	k9	i
spojení	spojení	k1gNnSc1	spojení
voyerství	voyerství	k1gNnSc1	voyerství
a	a	k8xC	a
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
filmu	film	k1gInSc2	film
Alfreda	Alfred	k1gMnSc2	Alfred
Hitchcocka	Hitchcocko	k1gNnSc2	Hitchcocko
Okno	okno	k1gNnSc1	okno
do	do	k7c2	do
dvora	dvůr	k1gInSc2	dvůr
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
zobrazován	zobrazován	k2eAgInSc4d1	zobrazován
jako	jako	k8xC	jako
předmět	předmět	k1gInSc4	předmět
podporující	podporující	k2eAgNnSc4d1	podporující
voyeurství	voyeurství	k1gNnSc4	voyeurství
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
místem	místem	k6eAd1	místem
<g />
.	.	kIx.	.
</s>
<s>
sloužícím	sloužící	k1gMnPc3	sloužící
k	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgInSc1d1	samotný
proces	proces	k1gInSc1	proces
fotografování	fotografování	k1gNnSc2	fotografování
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k6eAd1	jen
pasivním	pasivní	k2eAgNnSc7d1	pasivní
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Fotoaparát	fotoaparát	k1gInSc1	fotoaparát
neznásilňuje	znásilňovat	k5eNaImIp3nS	znásilňovat
ani	ani	k8xC	ani
nezískává	získávat	k5eNaImIp3nS	získávat
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vměšovat	vměšovat	k5eAaImF	vměšovat
<g/>
,	,	kIx,	,
vnikat	vnikat	k5eAaImF	vnikat
bez	bez	k7c2	bez
povolení	povolení	k1gNnSc2	povolení
<g/>
,	,	kIx,	,
zkreslovat	zkreslovat	k5eAaImF	zkreslovat
<g/>
,	,	kIx,	,
zneužívat	zneužívat	k5eAaImF	zneužívat
a	a	k8xC	a
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
dotáhneme	dotáhnout	k5eAaPmIp1nP	dotáhnout
metaforu	metafora	k1gFnSc4	metafora
do	do	k7c2	do
extrému	extrém	k1gInSc2	extrém
<g/>
,	,	kIx,	,
zabíjet	zabíjet	k5eAaImF	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
pronikání	pronikání	k1gNnSc2	pronikání
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
všechny	všechen	k3xTgFnPc4	všechen
tyto	tento	k3xDgFnPc4	tento
aktivity	aktivita	k1gFnPc4	aktivita
prováděny	prováděn	k2eAgFnPc4d1	prováděna
zpovzdálí	zpovzdálí	k6eAd1	zpovzdálí
a	a	k8xC	a
s	s	k7c7	s
určitým	určitý	k2eAgInSc7d1	určitý
odstupem	odstup	k1gInSc7	odstup
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Digitální	digitální	k2eAgFnSc1d1	digitální
technologie	technologie	k1gFnSc1	technologie
s	s	k7c7	s
sebou	se	k3xPyFc7	se
také	také	k6eAd1	také
přinesla	přinést	k5eAaPmAgFnS	přinést
manipulaci	manipulace	k1gFnSc4	manipulace
fotografiemi	fotografia	k1gFnPc7	fotografia
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
snadná	snadný	k2eAgFnSc1d1	snadná
i	i	k9	i
pro	pro	k7c4	pro
začínající	začínající	k2eAgMnPc4d1	začínající
fotografy	fotograf	k1gMnPc4	fotograf
a	a	k8xC	a
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
otázky	otázka	k1gFnPc4	otázka
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
etičnosti	etičnost	k1gFnSc2	etičnost
zpracování	zpracování	k1gNnSc2	zpracování
digitálních	digitální	k2eAgFnPc2d1	digitální
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
fotožurnalistů	fotožurnalista	k1gMnPc2	fotožurnalista
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
žánru	žánr	k1gInSc6	žánr
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
co	co	k3yQnSc4	co
nejobjektivnější	objektivní	k2eAgNnSc4d3	nejobjektivnější
zobrazení	zobrazení	k1gNnSc4	zobrazení
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nesmí	smět	k5eNaImIp3nS	smět
své	svůj	k3xOyFgFnPc4	svůj
fotografie	fotografia	k1gFnPc4	fotografia
ořezávat	ořezávat	k5eAaImF	ořezávat
ani	ani	k8xC	ani
kombinovat	kombinovat	k5eAaImF	kombinovat
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
několika	několik	k4yIc2	několik
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
výslednou	výsledný	k2eAgFnSc4d1	výsledná
fotomontáž	fotomontáž	k1gFnSc4	fotomontáž
prezentovat	prezentovat	k5eAaBmF	prezentovat
jako	jako	k9	jako
jeden	jeden	k4xCgInSc4	jeden
autentický	autentický	k2eAgInSc4d1	autentický
snímek	snímek	k1gInSc4	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Technologie	technologie	k1gFnSc1	technologie
s	s	k7c7	s
sebou	se	k3xPyFc7	se
však	však	k9	však
přinesla	přinést	k5eAaPmAgFnS	přinést
i	i	k9	i
změny	změna	k1gFnPc4	změna
ve	v	k7c4	v
zpracování	zpracování	k1gNnSc4	zpracování
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
dochází	docházet	k5eAaImIp3nS	docházet
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
fotoaparátu	fotoaparát	k1gInSc6	fotoaparát
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc3	on
možné	možný	k2eAgNnSc1d1	možné
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
manipulaci	manipulace	k1gFnSc3	manipulace
obrazu	obraz	k1gInSc2	obraz
<g/>
;	;	kIx,	;
toho	ten	k3xDgNnSc2	ten
využívá	využívat	k5eAaImIp3nS	využívat
zejména	zejména	k9	zejména
forenzní	forenzní	k2eAgFnPc4d1	forenzní
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgFnSc1d1	digitální
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nových	nový	k2eAgNnPc2d1	nové
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mění	měnit	k5eAaImIp3nP	měnit
jak	jak	k6eAd1	jak
naše	náš	k3xOp1gNnPc4	náš
vnímání	vnímání	k1gNnPc4	vnímání
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
strukturu	struktura	k1gFnSc4	struktura
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
však	však	k9	však
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
obavami	obava	k1gFnPc7	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
současnosti	současnost	k1gFnPc4	současnost
využívaná	využívaný	k2eAgFnSc1d1	využívaná
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
desenzibilizaci	desenzibilizace	k1gFnSc3	desenzibilizace
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
se	s	k7c7	s
znepokojujícím	znepokojující	k2eAgInSc7d1	znepokojující
nebo	nebo	k8xC	nebo
explicitním	explicitní	k2eAgInSc7d1	explicitní
obsahem	obsah	k1gInSc7	obsah
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
válečných	válečný	k2eAgFnPc2d1	válečná
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
pornografie	pornografie	k1gFnSc2	pornografie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
celé	celý	k2eAgFnPc1d1	celá
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Diskuze	diskuze	k1gFnSc1	diskuze
o	o	k7c4	o
desenzibilizaci	desenzibilizace	k1gFnSc4	desenzibilizace
způsobené	způsobený	k2eAgFnSc2d1	způsobená
fotografiemi	fotografia	k1gFnPc7	fotografia
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
debatou	debata	k1gFnSc7	debata
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
cenzuře	cenzura	k1gFnSc6	cenzura
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
Sontagová	Sontagový	k2eAgFnSc1d1	Sontagová
zapojila	zapojit	k5eAaPmAgFnS	zapojit
s	s	k7c7	s
varováním	varování	k1gNnSc7	varování
<g/>
,	,	kIx,	,
že	že	k8xS	že
možnost	možnost	k1gFnSc1	možnost
cenzurovat	cenzurovat	k5eAaImF	cenzurovat
fotografie	fotografia	k1gFnSc2	fotografia
dává	dávat	k5eAaImIp3nS	dávat
fotografovi	fotograf	k1gMnSc3	fotograf
moc	moc	k6eAd1	moc
konstruovat	konstruovat	k5eAaImF	konstruovat
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
fotografických	fotografický	k2eAgFnPc2d1	fotografická
praktik	praktika	k1gFnPc2	praktika
utvářejících	utvářející	k2eAgFnPc2d1	utvářející
společnost	společnost	k1gFnSc4	společnost
je	být	k5eAaImIp3nS	být
turismus	turismus	k1gInSc1	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Turismus	turismus	k1gInSc1	turismus
a	a	k8xC	a
fotografie	fotografie	k1gFnSc1	fotografie
společně	společně	k6eAd1	společně
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
jakýsi	jakýsi	k3yIgInSc4	jakýsi
"	"	kIx"	"
<g/>
neuhýbající	uhýbající	k2eNgInSc4d1	uhýbající
turistický	turistický	k2eAgInSc4d1	turistický
pohled	pohled	k1gInSc4	pohled
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
jsou	být	k5eAaImIp3nP	být
místní	místní	k2eAgMnPc1d1	místní
lidé	člověk	k1gMnPc1	člověk
charakterizovaní	charakterizovaný	k2eAgMnPc1d1	charakterizovaný
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
objektivu	objektiv	k1gInSc2	objektiv
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
však	však	k9	však
zastávají	zastávat	k5eAaImIp3nP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
obrácený	obrácený	k2eAgInSc1d1	obrácený
pohled	pohled	k1gInSc1	pohled
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
mohou	moct	k5eAaImIp3nP	moct
místní	místní	k2eAgMnPc1d1	místní
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
subjekty	subjekt	k1gInPc7	subjekt
fotografování	fotografování	k1gNnSc2	fotografování
<g/>
,	,	kIx,	,
sami	sám	k3xTgMnPc1	sám
situovat	situovat	k5eAaBmF	situovat
fotografující	fotografující	k2eAgMnPc4d1	fotografující
turisty	turist	k1gMnPc4	turist
do	do	k7c2	do
role	role	k1gFnSc2	role
povrchních	povrchní	k2eAgMnPc2d1	povrchní
konzumentů	konzument	k1gMnPc2	konzument
fotek	fotka	k1gFnPc2	fotka
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
se	se	k3xPyFc4	se
také	také	k6eAd1	také
stala	stát	k5eAaPmAgFnS	stát
tématem	téma	k1gNnSc7	téma
mnoha	mnoho	k4c2	mnoho
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Právní	právní	k2eAgFnSc2d1	právní
regulace	regulace	k1gFnSc2	regulace
fotografování	fotografování	k1gNnSc2	fotografování
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
fotografování	fotografování	k1gNnSc4	fotografování
(	(	kIx(	(
<g/>
i	i	k9	i
filmování	filmování	k1gNnSc2	filmování
<g/>
)	)	kIx)	)
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
oproti	oproti	k7c3	oproti
dřívějším	dřívější	k2eAgFnPc3d1	dřívější
technologiím	technologie	k1gFnPc3	technologie
mnohem	mnohem	k6eAd1	mnohem
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
a	a	k8xC	a
přesnější	přesný	k2eAgNnSc4d2	přesnější
zachycení	zachycení	k1gNnSc4	zachycení
zobrazované	zobrazovaný	k2eAgFnSc2d1	zobrazovaná
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
do	do	k7c2	do
střetu	střet	k1gInSc2	střet
s	s	k7c7	s
tradičně	tradičně	k6eAd1	tradičně
či	či	k8xC	či
nově	nově	k6eAd1	nově
uznávanými	uznávaný	k2eAgNnPc7d1	uznávané
právy	právo	k1gNnPc7	právo
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
,	,	kIx,	,
osobního	osobní	k2eAgNnSc2d1	osobní
soukromí	soukromí	k1gNnSc2	soukromí
nebo	nebo	k8xC	nebo
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
podléhá	podléhat	k5eAaImIp3nS	podléhat
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
právní	právní	k2eAgFnSc4d1	právní
regulaci	regulace	k1gFnSc4	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
regulace	regulace	k1gFnSc1	regulace
je	být	k5eAaImIp3nS	být
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
a	a	k8xC	a
územích	území	k1gNnPc6	území
s	s	k7c7	s
nestabilními	stabilní	k2eNgInPc7d1	nestabilní
nebo	nebo	k8xC	nebo
totalitními	totalitní	k2eAgInPc7d1	totalitní
politickými	politický	k2eAgInPc7d1	politický
či	či	k8xC	či
bezpečnostními	bezpečnostní	k2eAgInPc7d1	bezpečnostní
poměry	poměr	k1gInPc7	poměr
nebo	nebo	k8xC	nebo
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
osobního	osobní	k2eAgNnSc2d1	osobní
soukromí	soukromí	k1gNnSc2	soukromí
či	či	k8xC	či
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
