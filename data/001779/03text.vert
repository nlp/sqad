<s>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
Nobelových	Nobelových	k2eAgFnPc2d1	Nobelových
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
udělovanou	udělovaný	k2eAgFnSc4d1	udělovaná
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
závěti	závěť	k1gFnSc2	závěť
"	"	kIx"	"
<g/>
za	za	k7c4	za
nejvýznačnější	význačný	k2eAgNnSc4d3	nejvýznačnější
literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
směru	směr	k1gInSc6	směr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
Čechem	Čech	k1gMnSc7	Čech
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
byla	být	k5eAaImAgFnS	být
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
udělena	udělen	k2eAgFnSc1d1	udělena
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Dílo	dílo	k1gNnSc1	dílo
Wikizdroje	Wikizdroj	k1gInSc2	Wikizdroj
<g/>
:	:	kIx,	:
<g/>
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
nobelprize	nobelprize	k1gFnSc2	nobelprize
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
