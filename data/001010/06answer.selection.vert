<s>
Křenovice	Křenovice	k1gFnSc1	Křenovice
(	(	kIx(	(
Křenovice	Křenovice	k1gFnSc1	Křenovice
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Krenowitz	Krenowitz	k1gInSc1	Krenowitz
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
také	také	k9	také
Křenovice	Křenovice	k1gFnSc1	Křenovice
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
<g/>
,	,	kIx,	,
s	s	k7c7	s
předložkou	předložka	k1gFnSc7	předložka
2	[number]	k4	2
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
do	do	k7c2	do
Křenovic	Křenovice	k1gFnPc2	Křenovice
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
v	v	k7c6	v
Křenovicích	Křenovice	k1gFnPc6	Křenovice
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc4	obec
nacházející	nacházející	k2eAgFnSc4d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
okresu	okres	k1gInSc3	okres
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
