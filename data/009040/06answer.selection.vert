<s>
Organizace	organizace	k1gFnSc2	organizace
zemí	zem	k1gFnPc2	zem
vyvážejících	vyvážející	k2eAgMnPc2d1	vyvážející
ropu	ropa	k1gFnSc4	ropa
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
OPEC	opéct	k5eAaPmRp2nSwK	opéct
z	z	k7c2	z
angl.	angl.	k?	angl.
Organization	Organization	k1gInSc1	Organization
of	of	k?	of
the	the	k?	the
Petroleum	Petroleum	k1gInSc1	Petroleum
Exporting	Exporting	k1gInSc1	Exporting
Countries	Countries	k1gInSc1	Countries
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezivládní	mezivládní	k2eAgFnSc1d1	mezivládní
organizace	organizace	k1gFnSc1	organizace
sdružující	sdružující	k2eAgFnSc1d1	sdružující
14	[number]	k4	14
zemí	zem	k1gFnPc2	zem
exportujících	exportující	k2eAgFnPc2d1	exportující
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
