<s>
Josef	Josef	k1gMnSc1	Josef
Masopust	Masopust	k1gMnSc1	Masopust
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1931	[number]	k4	1931
Střimice	Střimika	k1gFnSc6	Střimika
u	u	k7c2	u
Mostu	most	k1gInSc2	most
-	-	kIx~	-
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
hráč	hráč	k1gMnSc1	hráč
a	a	k8xC	a
trenér	trenér	k1gMnSc1	trenér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
obdržel	obdržet	k5eAaPmAgMnS	obdržet
cenu	cena	k1gFnSc4	cena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
evropského	evropský	k2eAgMnSc4d1	evropský
fotbalistu	fotbalista	k1gMnSc4	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Českomoravský	českomoravský	k2eAgInSc1d1	českomoravský
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
svaz	svaz	k1gInSc1	svaz
ho	on	k3xPp3gMnSc4	on
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
českým	český	k2eAgMnSc7d1	český
fotbalistou	fotbalista	k1gMnSc7	fotbalista
posledních	poslední	k2eAgNnPc2d1	poslední
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
anketu	anketa	k1gFnSc4	anketa
Česká	český	k2eAgFnSc1d1	Česká
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
osobnost	osobnost	k1gFnSc1	osobnost
století	století	k1gNnSc2	století
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
jej	on	k3xPp3gMnSc4	on
odborníci	odborník	k1gMnPc1	odborník
zvolili	zvolit	k5eAaPmAgMnP	zvolit
za	za	k7c4	za
českého	český	k2eAgMnSc4d1	český
Fotbalistu	fotbalista	k1gMnSc4	fotbalista
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Pelé	Pelé	k1gNnSc1	Pelé
ho	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
125	[number]	k4	125
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
žijících	žijící	k2eAgMnPc2d1	žijící
fotbalistů	fotbalista	k1gMnPc2	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Figuroval	figurovat	k5eAaImAgMnS	figurovat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
UEFA	UEFA	kA	UEFA
Jubilee	Jubilee	k1gFnSc1	Jubilee
52	[number]	k4	52
Golden	Goldno	k1gNnPc2	Goldno
Players	Playersa	k1gFnPc2	Playersa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
mostecký	mostecký	k2eAgInSc1d1	mostecký
stadion	stadion	k1gInSc1	stadion
(	(	kIx(	(
<g/>
Fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
stadion	stadion	k1gInSc1	stadion
Josefa	Josef	k1gMnSc2	Josef
Masopusta	Masopust	k1gMnSc2	Masopust
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
odhalena	odhalit	k5eAaPmNgFnS	odhalit
u	u	k7c2	u
stadionu	stadion	k1gInSc2	stadion
Dukly	Dukla	k1gFnSc2	Dukla
Praha	Praha	k1gFnSc1	Praha
na	na	k7c6	na
Julisce	Juliska	k1gFnSc6	Juliska
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
jeho	jeho	k3xOp3gFnSc4	jeho
socha	socha	k1gFnSc1	socha
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Střimice	Střimika	k1gFnSc6	Střimika
u	u	k7c2	u
Mostu	most	k1gInSc2	most
jako	jako	k9	jako
nejstarší	starý	k2eAgFnSc1d3	nejstarší
z	z	k7c2	z
šesti	šest	k4xCc2	šest
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
horníka	horník	k1gMnSc2	horník
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
těžbě	těžba	k1gFnSc3	těžba
hnědého	hnědý	k2eAgNnSc2d1	hnědé
uhlí	uhlí	k1gNnSc2	uhlí
přestala	přestat	k5eAaPmAgFnS	přestat
obec	obec	k1gFnSc1	obec
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
ligu	liga	k1gFnSc4	liga
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c6	na
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
stadiónu	stadión	k1gInSc6	stadión
Na	na	k7c6	na
drožďárně	drožďárna	k1gFnSc6	drožďárna
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yQgInSc4	který
hrál	hrát	k5eAaImAgMnS	hrát
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
dresu	dres	k1gInSc6	dres
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
reprezentačního	reprezentační	k2eAgNnSc2d1	reprezentační
<g/>
,	,	kIx,	,
také	také	k9	také
získával	získávat	k5eAaImAgMnS	získávat
fotbalovou	fotbalový	k2eAgFnSc4d1	fotbalová
slávu	sláva	k1gFnSc4	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2015	[number]	k4	2015
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
nemoci	nemoc	k1gFnSc6	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hráčem	hráč	k1gMnSc7	hráč
dorosteneckého	dorostenecký	k2eAgNnSc2d1	dorostenecké
mužstva	mužstvo	k1gNnSc2	mužstvo
Mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
už	už	k9	už
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
A-mužstvo	Aužstvo	k1gNnSc4	A-mužstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
v	v	k7c6	v
osmnácti	osmnáct	k4xCc6	osmnáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
podepsal	podepsat	k5eAaPmAgMnS	podepsat
přestup	přestup	k1gInSc4	přestup
do	do	k7c2	do
ligových	ligový	k2eAgFnPc2d1	ligová
Teplic	Teplice	k1gFnPc2	Teplice
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yRgInPc4	který
hrál	hrát	k5eAaImAgInS	hrát
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
sezóny	sezóna	k1gFnSc2	sezóna
1950	[number]	k4	1950
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
základní	základní	k2eAgFnSc4d1	základní
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgMnS	přejít
do	do	k7c2	do
nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
tvořícího	tvořící	k2eAgInSc2d1	tvořící
klubu	klub	k1gInSc2	klub
ATK	ATK	kA	ATK
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
později	pozdě	k6eAd2	pozdě
působil	působit	k5eAaImAgMnS	působit
pod	pod	k7c7	pod
názvy	název	k1gInPc7	název
ÚDA	úd	k1gMnSc2	úd
a	a	k8xC	a
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
armádním	armádní	k2eAgNnSc6d1	armádní
mužstvu	mužstvo	k1gNnSc6	mužstvo
zůstal	zůstat	k5eAaPmAgMnS	zůstat
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
levého	levý	k2eAgMnSc2d1	levý
záložníka	záložník	k1gMnSc2	záložník
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
tvůrcem	tvůrce	k1gMnSc7	tvůrce
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
získal	získat	k5eAaPmAgMnS	získat
sedm	sedm	k4xCc4	sedm
ligových	ligový	k2eAgInPc2d1	ligový
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Reprezentační	reprezentační	k2eAgInSc4d1	reprezentační
dres	dres	k1gInSc4	dres
Československa	Československo	k1gNnSc2	Československo
oblékl	obléct	k5eAaPmAgMnS	obléct
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
a	a	k8xC	a
za	za	k7c4	za
národní	národní	k2eAgNnSc4d1	národní
mužstvo	mužstvo	k1gNnSc4	mužstvo
odehrál	odehrát	k5eAaPmAgInS	odehrát
celkem	celkem	k6eAd1	celkem
63	[number]	k4	63
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc2	který
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
10	[number]	k4	10
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
našich	náš	k3xOp1gMnPc2	náš
fotbalistů	fotbalista	k1gMnPc2	fotbalista
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
1960	[number]	k4	1960
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
především	především	k6eAd1	především
na	na	k7c6	na
stříbrném	stříbrný	k2eAgNnSc6d1	stříbrné
umístění	umístění	k1gNnSc6	umístění
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
1962	[number]	k4	1962
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgMnS	získat
Zlatý	zlatý	k2eAgInSc4d1	zlatý
míč	míč	k1gInSc4	míč
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
uděluje	udělovat	k5eAaImIp3nS	udělovat
nejlepšímu	dobrý	k2eAgMnSc3d3	nejlepší
fotbalistovi	fotbalista	k1gMnSc3	fotbalista
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1963	[number]	k4	1963
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
hrál	hrát	k5eAaImAgMnS	hrát
za	za	k7c4	za
vybraná	vybraný	k2eAgNnPc4d1	vybrané
mužstva	mužstvo	k1gNnPc4	mužstvo
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Evropy	Evropa	k1gFnSc2	Evropa
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
svěřena	svěřen	k2eAgFnSc1d1	svěřena
funkce	funkce	k1gFnSc1	funkce
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Závěr	závěr	k1gInSc1	závěr
hráčské	hráčský	k2eAgFnSc2d1	hráčská
kariéry	kariéra	k1gFnSc2	kariéra
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1968	[number]	k4	1968
<g/>
-	-	kIx~	-
<g/>
1970	[number]	k4	1970
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholné	vrcholný	k2eAgFnSc6d1	vrcholná
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgMnS	pohybovat
do	do	k7c2	do
třiceti	třicet	k4xCc2	třicet
devíti	devět	k4xCc2	devět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
set	sto	k4xCgNnPc2	sto
utkání	utkání	k1gNnPc2	utkání
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
sehrál	sehrát	k5eAaPmAgMnS	sehrát
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
nejvíce	nejvíce	k6eAd1	nejvíce
utkvěl	utkvět	k5eAaPmAgMnS	utkvět
zápasy	zápas	k1gInPc4	zápas
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
FC	FC	kA	FC
Santos	Santos	k1gInSc1	Santos
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
v	v	k7c4	v
Mexiko	Mexiko	k1gNnSc4	Mexiko
City	city	k1gNnSc1	city
a	a	k8xC	a
Československo	Československo	k1gNnSc1	Československo
-	-	kIx~	-
Brazílie	Brazílie	k1gFnSc1	Brazílie
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
1962	[number]	k4	1962
v	v	k7c4	v
Santiago	Santiago	k1gNnSc4	Santiago
de	de	k?	de
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
utkání	utkání	k1gNnSc6	utkání
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
nad	nad	k7c7	nad
tehdy	tehdy	k6eAd1	tehdy
patrně	patrně	k6eAd1	patrně
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
světovým	světový	k2eAgInSc7d1	světový
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
klubem	klub	k1gInSc7	klub
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
dva	dva	k4xCgInPc4	dva
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
hráčem	hráč	k1gMnSc7	hráč
zápasu	zápas	k1gInSc2	zápas
a	a	k8xC	a
podal	podat	k5eAaPmAgMnS	podat
lepší	dobrý	k2eAgInSc4d2	lepší
výkon	výkon	k1gInSc4	výkon
než	než	k8xS	než
největší	veliký	k2eAgFnSc1d3	veliký
hvězda	hvězda	k1gFnSc1	hvězda
Santosu	Santosa	k1gFnSc4	Santosa
Pelé	Pelá	k1gFnSc2	Pelá
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
vrcholem	vrchol	k1gInSc7	vrchol
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
finále	finále	k1gNnSc1	finále
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
Brazilcům	Brazilec	k1gMnPc3	Brazilec
jediný	jediný	k2eAgMnSc1d1	jediný
a	a	k8xC	a
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
chvíli	chvíle	k1gFnSc6	chvíle
vedoucí	vedoucí	k2eAgInSc1d1	vedoucí
gól	gól	k1gInSc1	gól
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
63	[number]	k4	63
reprezentačních	reprezentační	k2eAgInPc6d1	reprezentační
zápasech	zápas	k1gInPc6	zápas
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
-	-	kIx~	-
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
dal	dát	k5eAaPmAgMnS	dát
Masopust	Masopust	k1gMnSc1	Masopust
10	[number]	k4	10
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
hráčskou	hráčský	k2eAgFnSc4d1	hráčská
kariéru	kariéra	k1gFnSc4	kariéra
prožil	prožít	k5eAaPmAgMnS	prožít
v	v	k7c6	v
týmech	tým	k1gInPc6	tým
Teplic	Teplice	k1gFnPc2	Teplice
(	(	kIx(	(
<g/>
54	[number]	k4	54
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
ATK	ATK	kA	ATK
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
přejmenovaný	přejmenovaný	k2eAgMnSc1d1	přejmenovaný
na	na	k7c4	na
Dukla	Dukla	k1gFnSc1	Dukla
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
(	(	kIx(	(
<g/>
350	[number]	k4	350
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
období	období	k1gNnSc6	období
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
386	[number]	k4	386
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
79	[number]	k4	79
gólů	gól	k1gInPc2	gól
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2004	[number]	k4	2004
ho	on	k3xPp3gMnSc4	on
Pelé	Pelá	k1gFnSc6	Pelá
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
125	[number]	k4	125
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
žijících	žijící	k2eAgMnPc2d1	žijící
fotbalistů	fotbalista	k1gMnPc2	fotbalista
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hrajícím	hrající	k2eAgMnSc7d1	hrající
trenérem	trenér	k1gMnSc7	trenér
belgického	belgický	k2eAgInSc2d1	belgický
týmu	tým	k1gInSc2	tým
Crossing	Crossing	k1gInSc1	Crossing
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1977	[number]	k4	1977
<g/>
/	/	kIx~	/
<g/>
78	[number]	k4	78
přivedl	přivést	k5eAaPmAgMnS	přivést
k	k	k7c3	k
titulu	titul	k1gInSc3	titul
ligového	ligový	k2eAgMnSc2d1	ligový
mistra	mistr	k1gMnSc2	mistr
Zbrojovku	zbrojovka	k1gFnSc4	zbrojovka
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
byl	být	k5eAaImAgMnS	být
trenérem	trenér	k1gMnSc7	trenér
reprezentace	reprezentace	k1gFnSc2	reprezentace
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
až	až	k9	až
1991	[number]	k4	1991
pak	pak	k6eAd1	pak
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
jako	jako	k8xS	jako
trenér	trenér	k1gMnSc1	trenér
tamějšího	tamější	k2eAgNnSc2d1	tamější
reprezentačního	reprezentační	k2eAgNnSc2d1	reprezentační
mužstva	mužstvo	k1gNnSc2	mužstvo
juniorů	junior	k1gMnPc2	junior
(	(	kIx(	(
<g/>
hráčů	hráč	k1gMnPc2	hráč
do	do	k7c2	do
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řadu	řada	k1gFnSc4	řada
let	let	k1gInSc1	let
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
Nadace	nadace	k1gFnSc2	nadace
fotbalových	fotbalový	k2eAgMnPc2d1	fotbalový
internacionálů	internacionál	k1gMnPc2	internacionál
<g/>
.	.	kIx.	.
</s>
