<s>
Fauna	fauna	k1gFnSc1	fauna
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
atypická	atypický	k2eAgFnSc1d1	atypická
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
výskytu	výskyt	k1gInSc3	výskyt
jak	jak	k6eAd1	jak
mořských	mořský	k2eAgInPc2d1	mořský
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
sladkovodních	sladkovodní	k2eAgInPc2d1	sladkovodní
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
