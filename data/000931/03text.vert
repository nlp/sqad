<p>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1368	[number]	k4	1368
Norimberk	Norimberk	k1gInSc1	Norimberk
–	–	k?	–
9.	[number]	k4	9.
prosince	prosinec	k1gInSc2	prosinec
1437	[number]	k4	1437
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
čtvrté	čtvrtý	k4xOgFnPc1	čtvrtý
manželky	manželka	k1gFnPc1	manželka
Alžběty	Alžběta	k1gFnSc2	Alžběta
Pomořanské	pomořanský	k2eAgFnPc1d1	Pomořanská
<g/>
,	,	kIx,	,
braniborský	braniborský	k2eAgMnSc1d1	braniborský
markrabě	markrabě	k1gMnSc1	markrabě
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1373	[number]	k4	1373
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
od	od	k7c2	od
31.	[number]	k4	31.
března	březen	k1gInSc2	březen
1387	[number]	k4	1387
<g/>
.	.	kIx.	.
)	)	kIx)	)
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
zvolen	zvolen	k2eAgInSc1d1	zvolen
roku	rok	k1gInSc2	rok
1410	[number]	k4	1410
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
21.	[number]	k4	21.
července	červenec	k1gInSc2	červenec
1411	[number]	k4	1411
<g/>
,	,	kIx,	,
korunován	korunován	k2eAgMnSc1d1	korunován
8.	[number]	k4	8.
listopadu	listopad	k1gInSc2	listopad
1414	[number]	k4	1414
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slezský	slezský	k2eAgMnSc1d1	slezský
vévoda	vévoda	k1gMnSc1	vévoda
a	a	k8xC	a
lužický	lužický	k2eAgMnSc1d1	lužický
markrabě	markrabě	k1gMnSc1	markrabě
(	(	kIx(	(
<g/>
od	od	k7c2	od
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
(	(	kIx(	(
<g/>
1419	[number]	k4	1419
<g/>
–	–	k?	–
<g/>
1423	[number]	k4	1423
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
korunován	korunován	k2eAgMnSc1d1	korunován
28.	[number]	k4	28.
července	červenec	k1gInSc2	červenec
1420	[number]	k4	1420
<g/>
,	,	kIx,	,
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1436	[number]	k4	1436
<g/>
–	–	k?	–
<g/>
1437	[number]	k4	1437
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lombardský	lombardský	k2eAgMnSc1d1	lombardský
král	král	k1gMnSc1	král
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1431	[number]	k4	1431
<g/>
)	)	kIx)	)
a	a	k8xC	a
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1433	[number]	k4	1433
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
vzdělaný	vzdělaný	k2eAgMnSc1d1	vzdělaný
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
sedmi	sedm	k4xCc7	sedm
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
společenský	společenský	k2eAgInSc1d1	společenský
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
rytířských	rytířský	k2eAgInPc6d1	rytířský
turnajích	turnaj	k1gInPc6	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
vedoucích	vedoucí	k2eAgFnPc2d1	vedoucí
postav	postava	k1gFnPc2	postava
kostnického	kostnický	k2eAgInSc2d1	kostnický
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sice	sice	k8xC	sice
ukončil	ukončit	k5eAaPmAgInS	ukončit
papežské	papežský	k2eAgNnSc4d1	papežské
schizma	schizma	k1gNnSc4	schizma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
husitským	husitský	k2eAgFnPc3d1	husitská
válkám	válka	k1gFnPc3	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
jako	jako	k8xC	jako
středoevropský	středoevropský	k2eAgMnSc1d1	středoevropský
politik	politik	k1gMnSc1	politik
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
svatém	svatý	k2eAgMnSc6d1	svatý
Zikmundovi	Zikmund	k1gMnSc6	Zikmund
(	(	kIx(	(
<g/>
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
za	za	k7c4	za
přispění	přispění	k1gNnSc4	přispění
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
stal	stát	k5eAaPmAgInS	stát
novým	nový	k2eAgInSc7d1	nový
českým	český	k2eAgInSc7d1	český
patronem	patron	k1gInSc7	patron
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
evropský	evropský	k2eAgMnSc1d1	evropský
politik	politik	k1gMnSc1	politik
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
však	však	k9	však
neoblíbený	oblíbený	k2eNgInSc1d1	neoblíbený
kvůli	kvůli	k7c3	kvůli
Janu	Jan	k1gMnSc3	Jan
Husovi	Hus	k1gMnSc3	Hus
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
smrti	smrt	k1gFnSc6	smrt
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
připisován	připisovat	k5eAaImNgInS	připisovat
podíl	podíl	k1gInSc1	podíl
<g/>
.	.	kIx.	.
</s>
<s>
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
liška	liška	k1gFnSc1	liška
ryšavá	ryšavý	k2eAgFnSc1d1	ryšavá
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jenom	jenom	k9	jenom
kvůli	kvůli	k7c3	kvůli
zrzavým	zrzavý	k2eAgInPc3d1	zrzavý
vlasům	vlas	k1gInPc3	vlas
<g/>
,	,	kIx,	,
ne	ne	k9	ne
prohnanosti	prohnanost	k1gFnSc2	prohnanost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
jako	jako	k9	jako
o	o	k7c6	o
"	"	kIx"	"
<g/>
posledním	poslední	k2eAgMnSc6d1	poslední
císaři	císař	k1gMnSc6	císař
středověku	středověk	k1gInSc6	středověk
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Drška	drška	k1gFnSc1	drška
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
hlavním	hlavní	k2eAgInSc7d1	hlavní
zájmem	zájem	k1gInSc7	zájem
byla	být	k5eAaImAgFnS	být
reforma	reforma	k1gFnSc1	reforma
církve	církev	k1gFnSc2	církev
(	(	kIx(	(
<g/>
koncily	koncil	k1gInPc1	koncil
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
a	a	k8xC	a
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
)	)	kIx)	)
a	a	k8xC	a
reforma	reforma	k1gFnSc1	reforma
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zasahoval	zasahovat	k5eAaImAgInS	zasahovat
i	i	k9	i
do	do	k7c2	do
sporů	spor	k1gInPc2	spor
mezi	mezi	k7c7	mezi
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
(	(	kIx(	(
<g/>
Zikmund	Zikmund	k1gMnSc1	Zikmund
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
císař	císař	k1gMnSc1	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stanul	stanout	k5eAaPmAgMnS	stanout
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
sporů	spor	k1gInPc2	spor
mezi	mezi	k7c7	mezi
Polským	polský	k2eAgNnSc7d1	polské
královstvím	království	k1gNnSc7	království
a	a	k8xC	a
Řádem	řád	k1gInSc7	řád
německých	německý	k2eAgMnPc2d1	německý
rytířů	rytíř	k1gMnPc2	rytíř
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
pomohla	pomoct	k5eAaPmAgFnS	pomoct
Byzantské	byzantský	k2eAgInPc4d1	byzantský
říši	říš	k1gFnSc3	říš
proti	proti	k7c3	proti
Turkům	turek	k1gInPc3	turek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
snahách	snaha	k1gFnPc6	snaha
často	často	k6eAd1	často
býval	bývat	k5eAaImAgInS	bývat
osamocen	osamocen	k2eAgInSc1d1	osamocen
<g/>
.	.	kIx.	.
</s>
<s>
Zastával	zastávat	k5eAaImAgMnS	zastávat
podřízenost	podřízenost	k1gFnSc4	podřízenost
církve	církev	k1gFnSc2	církev
světské	světský	k2eAgFnSc2d1	světská
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1403	[number]	k4	1403
<g/>
–	–	k?	–
<g/>
1404	[number]	k4	1404
zakázal	zakázat	k5eAaPmAgInS	zakázat
odvádět	odvádět	k5eAaImF	odvádět
peněžní	peněžní	k2eAgFnPc4d1	peněžní
dávky	dávka	k1gFnPc4	dávka
papežské	papežský	k2eAgFnPc4d1	Papežská
kurii	kurie	k1gFnSc4	kurie
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
obsazovat	obsazovat	k5eAaImF	obsazovat
některé	některý	k3yIgInPc4	některý
církevní	církevní	k2eAgInPc4d1	církevní
úřady	úřad	k1gInPc4	úřad
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
biskupských	biskupský	k2eAgInPc2d1	biskupský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
podunajské	podunajský	k2eAgNnSc4d1	podunajské
soustátí	soustátí	k1gNnSc4	soustátí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
sestávalo	sestávat	k5eAaImAgNnS	sestávat
z	z	k7c2	z
uherského	uherský	k2eAgMnSc2d1	uherský
a	a	k8xC	a
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
a	a	k8xC	a
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
hrází	hráz	k1gFnSc7	hráz
proti	proti	k7c3	proti
dravé	dravý	k2eAgFnSc3d1	dravá
osmanské	osmanský	k2eAgFnSc3d1	Osmanská
expanzi	expanze	k1gFnSc3	expanze
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
dalšímu	další	k2eAgNnSc3d1	další
rozšíření	rozšíření	k1gNnSc3	rozšíření
směřoval	směřovat	k5eAaImAgInS	směřovat
také	také	k9	také
sňatek	sňatek	k1gInSc1	sňatek
Zikmundovy	Zikmundův	k2eAgFnSc2d1	Zikmundova
dcery	dcera	k1gFnSc2	dcera
Alžběty	Alžběta	k1gFnSc2	Alžběta
s	s	k7c7	s
rakouským	rakouský	k2eAgMnSc7d1	rakouský
arcivévodou	arcivévoda	k1gMnSc7	arcivévoda
Albrechtem	Albrecht	k1gMnSc7	Albrecht
V.	V.	kA	V.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
císaři	císař	k1gMnPc1	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
Karlu	Karel	k1gMnSc3	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
čtvrté	čtvrtá	k1gFnSc3	čtvrtá
manželce	manželka	k1gFnSc3	manželka
Alžbětě	Alžběta	k1gFnSc3	Alžběta
Pomořanské	pomořanský	k2eAgFnSc2d1	Pomořanská
<g/>
,	,	kIx,	,
vnučce	vnučka	k1gFnSc6	vnučka
polského	polský	k2eAgMnSc2d1	polský
krále	král	k1gMnSc2	král
Kazimíra	Kazimír	k1gMnSc2	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Velikého	veliký	k2eAgInSc2d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
sv.	sv.	kA	sv.
Zikmunda	Zikmund	k1gMnSc4	Zikmund
<g/>
,	,	kIx,	,
oblíbeného	oblíbený	k2eAgMnSc4d1	oblíbený
světce	světec	k1gMnSc4	světec
a	a	k8xC	a
patrona	patron	k1gMnSc4	patron
jeho	on	k3xPp3gMnSc4	on
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
přezdívku	přezdívka	k1gFnSc4	přezdívka
<g/>
,	,	kIx,	,
liška	liška	k1gFnSc1	liška
ryšavá	ryšavý	k2eAgFnSc1d1	ryšavá
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
již	již	k6eAd1	již
za	za	k7c2	za
svého	svůj	k3xOyFgNnSc2	svůj
dětství	dětství	k1gNnSc2	dětství
kvůli	kvůli	k7c3	kvůli
barvě	barva	k1gFnSc3	barva
svých	svůj	k3xOyFgInPc2	svůj
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
===	===	k?	===
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1374	[number]	k4	1374
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
syna	syn	k1gMnSc4	syn
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc7d3	nejstarší
přeživší	přeživší	k2eAgFnSc7d1	přeživší
dcerou	dcera	k1gFnSc7	dcera
polského	polský	k2eAgMnSc2d1	polský
a	a	k8xC	a
uherského	uherský	k2eAgMnSc2d1	uherský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zamýšlel	zamýšlet	k5eAaImAgInS	zamýšlet
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marie	Marie	k1gFnSc1	Marie
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
ho	on	k3xPp3gMnSc4	on
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
nahradí	nahradit	k5eAaPmIp3nS	nahradit
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1378	[number]	k4	1378
se	se	k3xPyFc4	se
Zikmund	Zikmund	k1gMnSc1	Zikmund
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
stal	stát	k5eAaPmAgMnS	stát
braniborským	braniborský	k2eAgMnSc7d1	braniborský
markrabětem	markrabě	k1gMnSc7	markrabě
<g/>
.	.	kIx.	.
</s>
<s>
Desetiletý	desetiletý	k2eAgMnSc1d1	desetiletý
Zikmund	Zikmund	k1gMnSc1	Zikmund
byl	být	k5eAaImAgMnS	být
ovšem	ovšem	k9	ovšem
na	na	k7c4	na
několik	několik	k4yIc4	několik
příštích	příští	k2eAgNnPc2d1	příští
let	léto	k1gNnPc2	léto
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
uherský	uherský	k2eAgInSc4d1	uherský
dvůr	dvůr	k1gInSc4	dvůr
a	a	k8xC	a
s	s	k7c7	s
Uherskem	Uhersko	k1gNnSc7	Uhersko
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
identifikoval	identifikovat	k5eAaBmAgMnS	identifikovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1381	[number]	k4	1381
poslal	poslat	k5eAaPmAgMnS	poslat
třináctiletého	třináctiletý	k2eAgMnSc4d1	třináctiletý
Zikmunda	Zikmund	k1gMnSc4	Zikmund
jeho	jeho	k3xOp3gNnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
do	do	k7c2	do
Krakova	Krakov	k1gInSc2	Krakov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
polsky	polsky	k6eAd1	polsky
<g/>
,	,	kIx,	,
zvykl	zvyknout	k5eAaPmAgMnS	zvyknout
si	se	k3xPyFc3	se
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
místní	místní	k2eAgInSc4d1	místní
lid	lid	k1gInSc4	lid
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Ludvík	Ludvík	k1gMnSc1	Ludvík
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1382	[number]	k4	1382
a	a	k8xC	a
Poláci	Polák	k1gMnPc1	Polák
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
další	další	k2eAgNnSc4d1	další
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
Uhrami	Uhry	k1gFnPc7	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
polskou	polský	k2eAgFnSc4d1	polská
královnu	královna	k1gFnSc4	královna
zvolili	zvolit	k5eAaPmAgMnP	zvolit
Mariinu	Mariin	k2eAgFnSc4d1	Mariina
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
Hedviku	Hedvika	k1gFnSc4	Hedvika
<g/>
,	,	kIx,	,
provdanou	provdaný	k2eAgFnSc4d1	provdaná
za	za	k7c2	za
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagella	Jagella	k6eAd1	Jagella
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
nechala	nechat	k5eAaPmAgFnS	nechat
královna	královna	k1gFnSc1	královna
vdova	vdova	k1gFnSc1	vdova
Alžběta	Alžběta	k1gFnSc1	Alžběta
Bosenská	bosenský	k2eAgFnSc1d1	bosenská
korunovat	korunovat	k5eAaBmF	korunovat
královnou	královna	k1gFnSc7	královna
Marii	Maria	k1gFnSc3	Maria
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zasnoubila	zasnoubit	k5eAaPmAgFnS	zasnoubit
s	s	k7c7	s
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
Orleánským	orleánský	k2eAgInSc7d1	orleánský
<g/>
.	.	kIx.	.
15.	[number]	k4	15.
listopadu	listopad	k1gInSc2	listopad
1385	[number]	k4	1385
si	se	k3xPyFc3	se
Zikmund	Zikmund	k1gMnSc1	Zikmund
vynutil	vynutit	k5eAaPmAgMnS	vynutit
sňatek	sňatek	k1gInSc4	sňatek
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
(	(	kIx(	(
<g/>
svatba	svatba	k1gFnSc1	svatba
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
ve	v	k7c6	v
slovenském	slovenský	k2eAgInSc6d1	slovenský
Zvolenu	Zvolen	k1gInSc6	Zvolen
<g/>
)	)	kIx)	)
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
tak	tak	k6eAd1	tak
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zajištění	zajištění	k1gNnSc6	zajištění
podpory	podpora	k1gFnSc2	podpora
šlechty	šlechta	k1gFnSc2	šlechta
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
korunovat	korunovat	k5eAaBmF	korunovat
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
31.	[number]	k4	31.
března	březen	k1gInSc2	březen
1387	[number]	k4	1387
v	v	k7c6	v
Székesfehérváru	Székesfehérvár	k1gInSc6	Székesfehérvár
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Stoličný	stoličný	k1gMnSc1	stoličný
Bělehrad	Bělehrad	k1gInSc1	Bělehrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1387	[number]	k4	1387
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Marie	Maria	k1gFnSc2	Maria
i	i	k8xC	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
Alžbětou	Alžběta	k1gFnSc7	Alžběta
Bosenskou	bosenský	k2eAgFnSc7d1	bosenská
<g/>
,	,	kIx,	,
zajata	zajat	k2eAgMnSc4d1	zajat
během	během	k7c2	během
rebelie	rebelie	k1gFnSc2	rebelie
záhřebským	záhřebský	k2eAgMnSc7d1	záhřebský
biskupem	biskup	k1gMnSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
byla	být	k5eAaImAgFnS	být
brzy	brzy	k6eAd1	brzy
osvobozena	osvobozen	k2eAgFnSc1d1	osvobozena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
i	i	k9	i
nadále	nadále	k6eAd1	nadále
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
jediným	jediný	k2eAgMnSc7d1	jediný
Lucemburkem	Lucemburk	k1gMnSc7	Lucemburk
na	na	k7c6	na
uherském	uherský	k2eAgInSc6d1	uherský
trůně	trůn	k1gInSc6	trůn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
udržel	udržet	k5eAaPmAgMnS	udržet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
celých	celý	k2eAgNnPc2d1	celé
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1396	[number]	k4	1396
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Joštem	Jošto	k1gNnSc7	Jošto
Moravským	moravský	k2eAgNnSc7d1	Moravské
postavil	postavit	k5eAaPmAgMnS	postavit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
Václavovi	Václav	k1gMnSc3	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
také	také	k9	také
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
obnovil	obnovit	k5eAaPmAgMnS	obnovit
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
nástupnictví	nástupnictví	k1gNnSc6	nástupnictví
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jím	jíst	k5eAaImIp1nS	jíst
jmenován	jmenován	k2eAgMnSc1d1	jmenován
vikářem	vikář	k1gMnSc7	vikář
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
organizoval	organizovat	k5eAaBmAgInS	organizovat
proti	proti	k7c3	proti
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
září	září	k1gNnSc6	září
1396	[number]	k4	1396
rozbita	rozbít	k5eAaPmNgFnS	rozbít
Turky	Turek	k1gMnPc7	Turek
u	u	k7c2	u
Nikopole	Nikopole	k1gFnSc2	Nikopole
<g/>
,	,	kIx,	,
sultán	sultán	k1gMnSc1	sultán
ovšem	ovšem	k9	ovšem
toto	tento	k3xDgNnSc4	tento
vítězství	vítězství	k1gNnSc4	vítězství
nevyužil	využít	k5eNaPmAgMnS	využít
k	k	k7c3	k
expanzi	expanze	k1gFnSc3	expanze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1401	[number]	k4	1401
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
vzpoura	vzpoura	k1gFnSc1	vzpoura
a	a	k8xC	a
král	král	k1gMnSc1	král
byl	být	k5eAaImAgMnS	být
dočasně	dočasně	k6eAd1	dočasně
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
sesazen	sesadit	k5eAaPmNgMnS	sesadit
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obrátil	obrátit	k5eAaPmAgMnS	obrátit
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
ohledně	ohledně	k7c2	ohledně
císařské	císařský	k2eAgFnSc2d1	císařská
korunovace	korunovace	k1gFnSc2	korunovace
(	(	kIx(	(
<g/>
stále	stále	k6eAd1	stále
ještě	ještě	k9	ještě
možné	možný	k2eAgMnPc4d1	možný
<g/>
)	)	kIx)	)
k	k	k7c3	k
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
mu	on	k3xPp3gMnSc3	on
správu	správa	k1gFnSc4	správa
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
začal	začít	k5eAaPmAgMnS	začít
zabírat	zabírat	k5eAaImF	zabírat
královské	královský	k2eAgInPc4d1	královský
hrady	hrad	k1gInPc4	hrad
<g/>
,	,	kIx,	,
Václava	Václava	k1gFnSc1	Václava
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
odporu	odpor	k1gInSc3	odpor
nechal	nechat	k5eAaPmAgInS	nechat
zajmout	zajmout	k5eAaPmF	zajmout
a	a	k8xC	a
odvést	odvést	k5eAaPmF	odvést
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
rychle	rychle	k6eAd1	rychle
vzrůstal	vzrůstat	k5eAaImAgInS	vzrůstat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zlomit	zlomit	k5eAaPmF	zlomit
vojensky	vojensky	k6eAd1	vojensky
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
však	však	k9	však
musel	muset	k5eAaImAgInS	muset
znovu	znovu	k6eAd1	znovu
zasahovat	zasahovat	k5eAaImF	zasahovat
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
proti	proti	k7c3	proti
novému	nový	k2eAgMnSc3d1	nový
uchazeči	uchazeč	k1gMnSc3	uchazeč
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Václav	Václav	k1gMnSc1	Václav
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
uprchl	uprchnout	k5eAaPmAgInS	uprchnout
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1404	[number]	k4	1404
se	se	k3xPyFc4	se
bratři	bratr	k1gMnPc1	bratr
smířili	smířit	k5eAaPmAgMnP	smířit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Václavova	Václavův	k2eAgMnSc2d1	Václavův
římského	římský	k2eAgMnSc2d1	římský
protikrále	protikrál	k1gMnSc2	protikrál
Ruprechta	Ruprechto	k1gNnSc2	Ruprechto
III	III	kA	III
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
Zikmund	Zikmund	k1gMnSc1	Zikmund
21.	[number]	k4	21.
července	červenec	k1gInSc2	červenec
1411	[number]	k4	1411
s	s	k7c7	s
Václavovým	Václavův	k2eAgInSc7d1	Václavův
souhlasem	souhlas	k1gInSc7	souhlas
zvolen	zvolit	k5eAaPmNgInS	zvolit
hlasy	hlas	k1gInPc7	hlas
pěti	pět	k4xCc3	pět
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g/>
korunován	korunován	k2eAgInSc1d1	korunován
8.	[number]	k4	8.
listopadu	listopad	k1gInSc2	listopad
1414	[number]	k4	1414
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
podruhé	podruhé	k6eAd1	podruhé
zastavil	zastavit	k5eAaPmAgInS	zastavit
Braniborsko	Braniborsko	k1gNnSc4	Braniborsko
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
svému	svůj	k3xOyFgMnSc3	svůj
přívrženci	přívrženec	k1gMnSc3	přívrženec
<g/>
,	,	kIx,	,
norimberskému	norimberský	k2eAgMnSc3d1	norimberský
purkrabímu	purkrabí	k1gMnSc3	purkrabí
Fridrichu	Fridrich	k1gMnSc3	Fridrich
I.	I.	kA	I.
Hohenzollernskému	hohenzollernský	k2eAgMnSc3d1	hohenzollernský
<g/>
,	,	kIx,	,
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
odtrhl	odtrhnout	k5eAaPmAgInS	odtrhnout
od	od	k7c2	od
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Papežské	papežský	k2eAgNnSc1d1	papežské
schizma	schizma	k1gNnSc1	schizma
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Jako	jako	k8xC	jako
německý	německý	k2eAgMnSc1d1	německý
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
Zikmund	Zikmund	k1gMnSc1	Zikmund
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
odstranění	odstranění	k1gNnSc4	odstranění
papežského	papežský	k2eAgNnSc2d1	papežské
schizmatu	schizma	k1gNnSc2	schizma
(	(	kIx(	(
<g/>
trojpapežství	trojpapežství	k1gNnSc2	trojpapežství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1410	[number]	k4	1410
zemřel	zemřít	k5eAaPmAgMnS	zemřít
papež	papež	k1gMnSc1	papež
Alexandr	Alexandr	k1gMnSc1	Alexandr
V.	V.	kA	V.
Situace	situace	k1gFnSc1	situace
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
se	se	k3xPyFc4	se
nelíbila	líbit	k5eNaImAgFnS	líbit
římskému	římský	k2eAgMnSc3d1	římský
králi	král	k1gMnSc3	král
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přiměl	přimět	k5eAaPmAgMnS	přimět
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
papežů	papež	k1gMnPc2	papež
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svolal	svolat	k5eAaPmAgInS	svolat
nový	nový	k2eAgInSc1d1	nový
koncil	koncil	k1gInSc1	koncil
do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
<g/>
.	.	kIx.	.
</s>
<s>
Koncil	koncil	k1gInSc1	koncil
roku	rok	k1gInSc2	rok
1414	[number]	k4	1414
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c4	o
abdikaci	abdikace	k1gFnSc4	abdikace
všech	všecek	k3xTgMnPc2	všecek
tří	tři	k4xCgMnPc2	tři
papežů	papež	k1gMnPc2	papež
a	a	k8xC	a
jako	jako	k9	jako
nového	nový	k2eAgMnSc4d1	nový
papeže	papež	k1gMnSc4	papež
11.	[number]	k4	11.
listopadu	listopad	k1gInSc6	listopad
1417	[number]	k4	1417
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Ottu	Otto	k1gMnSc3	Otto
Colonnu	Colonn	k1gMnSc3	Colonn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přijal	přijmout	k5eAaPmAgMnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Martin	Martina	k1gFnPc2	Martina
V.	V.	kA	V.
Na	na	k7c6	na
jednání	jednání	k1gNnSc6	jednání
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
měl	mít	k5eAaImAgMnS	mít
obhájit	obhájit	k5eAaPmF	obhájit
své	svůj	k3xOyFgNnSc4	svůj
učení	učení	k1gNnSc4	učení
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
vydal	vydat	k5eAaPmAgMnS	vydat
Zikmund	Zikmund	k1gMnSc1	Zikmund
ochranný	ochranný	k2eAgInSc4d1	ochranný
glejt	glejt	k1gInSc4	glejt
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ale	ale	k9	ale
nařčen	nařčen	k2eAgMnSc1d1	nařčen
z	z	k7c2	z
kacířství	kacířství	k1gNnSc2	kacířství
a	a	k8xC	a
6.	[number]	k4	6.
července	červenec	k1gInSc2	červenec
1415	[number]	k4	1415
upálen	upálit	k5eAaPmNgMnS	upálit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
koncilu	koncil	k1gInSc6	koncil
mělo	mít	k5eAaImAgNnS	mít
křesťanstvo	křesťanstvo	k1gNnSc1	křesťanstvo
opět	opět	k6eAd1	opět
jediného	jediný	k2eAgMnSc4d1	jediný
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Císař	Císař	k1gMnSc1	Císař
a	a	k8xC	a
husité	husita	k1gMnPc1	husita
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Václavově	Václavův	k2eAgFnSc6d1	Václavova
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Zikmund	Zikmund	k1gMnSc1	Zikmund
jediným	jediný	k2eAgMnSc7d1	jediný
dědicem	dědic	k1gMnSc7	dědic
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
šlechta	šlechta	k1gFnSc1	šlechta
ho	on	k3xPp3gInSc4	on
očekávala	očekávat	k5eAaImAgFnS	očekávat
se	s	k7c7	s
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
<g/>
,	,	kIx,	,
husitská	husitský	k2eAgFnSc1d1	husitská
šlechta	šlechta	k1gFnSc1	šlechta
pak	pak	k6eAd1	pak
s	s	k7c7	s
určitými	určitý	k2eAgFnPc7d1	určitá
obavami	obava	k1gFnPc7	obava
<g/>
.	.	kIx.	.
</s>
<s>
Připravovaly	připravovat	k5eAaImAgInP	připravovat
se	se	k3xPyFc4	se
volební	volební	k2eAgInPc1d1	volební
požadavky	požadavek	k1gInPc1	požadavek
<g/>
:	:	kIx,	:
přijímání	přijímání	k1gNnSc1	přijímání
z	z	k7c2	z
kalicha	kalich	k1gInSc2	kalich
a	a	k8xC	a
sekularizace	sekularizace	k1gFnSc2	sekularizace
(	(	kIx(	(
<g/>
zabavení	zabavení	k1gNnSc2	zabavení
<g/>
)	)	kIx)	)
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Zikmunda	Zikmund	k1gMnSc4	Zikmund
podporovala	podporovat	k5eAaImAgFnS	podporovat
jako	jako	k8xC	jako
příštího	příští	k2eAgMnSc2d1	příští
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
vdova	vdova	k1gFnSc1	vdova
po	po	k7c6	po
Václavovi	Václavův	k2eAgMnPc1d1	Václavův
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Žofie	Žofie	k1gFnSc1	Žofie
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nakonec	nakonec	k9	nakonec
sama	sám	k3xTgFnSc1	sám
uchýlila	uchýlit	k5eAaPmAgFnS	uchýlit
pod	pod	k7c4	pod
Zikmundovu	Zikmundův	k2eAgFnSc4d1	Zikmundova
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1420	[number]	k4	1420
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
přítomnosti	přítomnost	k1gFnSc2	přítomnost
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
křížová	křížový	k2eAgFnSc1d1	křížová
výprava	výprava	k1gFnSc1	výprava
proti	proti	k7c3	proti
kacířským	kacířský	k2eAgFnPc3d1	kacířská
Čechám	Čechy	k1gFnPc3	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
května	květen	k1gInSc2	květen
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
Zikmund	Zikmund	k1gMnSc1	Zikmund
s	s	k7c7	s
křižáckým	křižácký	k2eAgNnSc7d1	křižácké
vojskem	vojsko	k1gNnSc7	vojsko
Prahu	práh	k1gInSc2	práh
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
poražen	porazit	k5eAaPmNgMnS	porazit
na	na	k7c6	na
Vítkově	Vítkov	k1gInSc6	Vítkov
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
a	a	k8xC	a
pod	pod	k7c7	pod
Vyšehradem	Vyšehrad	k1gInSc7	Vyšehrad
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
korunovat	korunovat	k5eAaBmF	korunovat
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g/>
korunoval	korunovat	k5eAaBmAgInS	korunovat
jej	on	k3xPp3gMnSc4	on
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Konrád	Konrád	k1gMnSc1	Konrád
z	z	k7c2	z
Vechty	Vechta	k1gFnSc2	Vechta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
moci	moc	k1gFnSc6	moc
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1421	[number]	k4	1421
se	se	k3xPyFc4	se
v	v	k7c6	v
Čáslavi	Čáslav	k1gFnSc6	Čáslav
sešel	sejít	k5eAaPmAgInS	sejít
sněm	sněm	k1gInSc1	sněm
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ho	on	k3xPp3gMnSc4	on
jako	jako	k8xC	jako
krále	král	k1gMnSc4	král
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
;	;	kIx,	;
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
a	a	k8xC	a
Lužici	Lužice	k1gFnSc6	Lužice
ho	on	k3xPp3gMnSc4	on
šlechta	šlechta	k1gFnSc1	šlechta
uznala	uznat	k5eAaPmAgFnS	uznat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
prohrané	prohraný	k2eAgFnSc6d1	prohraná
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
8.	[number]	k4	8.
ledna	leden	k1gInSc2	leden
1422	[number]	k4	1422
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
odtáhl	odtáhnout	k5eAaPmAgMnS	odtáhnout
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
příští	příští	k2eAgInSc1d1	příští
léta	léto	k1gNnPc4	léto
získat	získat	k5eAaPmF	získat
kombinací	kombinace	k1gFnSc7	kombinace
diplomatického	diplomatický	k2eAgInSc2d1	diplomatický
a	a	k8xC	a
vojenského	vojenský	k2eAgInSc2d1	vojenský
nátlaku	nátlak	k1gInSc2	nátlak
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1429	[number]	k4	1429
navázali	navázat	k5eAaPmAgMnP	navázat
husité	husita	k1gMnPc1	husita
kontakty	kontakt	k1gInPc4	kontakt
se	s	k7c7	s
Zikmundem	Zikmund	k1gMnSc7	Zikmund
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
hodlali	hodlat	k5eAaImAgMnP	hodlat
přijmout	přijmout	k5eAaPmF	přijmout
za	za	k7c4	za
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
uznal	uznat	k5eAaPmAgMnS	uznat
jejich	jejich	k3xOp3gInSc4	jejich
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ovšem	ovšem	k9	ovšem
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
světskou	světský	k2eAgFnSc4d1	světská
hlavu	hlava	k1gFnSc4	hlava
křesťanstva	křesťanstvo	k1gNnSc2	křesťanstvo
nepřijatelná	přijatelný	k2eNgFnSc1d1	nepřijatelná
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
tedy	tedy	k9	tedy
musel	muset	k5eAaImAgMnS	muset
čekat	čekat	k5eAaImF	čekat
celých	celý	k2eAgInPc2d1	celý
sedmnáct	sedmnáct	k4xCc4	sedmnáct
let	léto	k1gNnPc2	léto
po	po	k7c6	po
Václavově	Václavův	k2eAgFnSc6d1	Václavova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
všeobecně	všeobecně	k6eAd1	všeobecně
přijat	přijat	k2eAgMnSc1d1	přijat
jako	jako	k8xC	jako
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Legitimní	legitimní	k2eAgMnSc1d1	legitimní
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
31.	[number]	k4	31.
května	květen	k1gInSc2	květen
1433	[number]	k4	1433
byl	být	k5eAaImAgMnS	být
Zikmund	Zikmund	k1gMnSc1	Zikmund
papežem	papež	k1gMnSc7	papež
Evženem	Evžen	k1gMnSc7	Evžen
IV	IV	kA	IV
<g/>
.	.	kIx.	.
korunován	korunován	k2eAgMnSc1d1	korunován
na	na	k7c4	na
římského	římský	k2eAgMnSc4d1	římský
císaře	císař	k1gMnSc4	císař
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lipan	lipan	k1gMnSc1	lipan
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cesta	cesta	k1gFnSc1	cesta
k	k	k7c3	k
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6.	[number]	k4	6.
srpna	srpen	k1gInSc2	srpen
1436	[number]	k4	1436
na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
byla	být	k5eAaImAgNnP	být
vyhlášena	vyhlášen	k2eAgNnPc1d1	vyhlášeno
kompaktáta	kompaktáta	k1gNnPc1	kompaktáta
(	(	kIx(	(
<g/>
kompromis	kompromis	k1gInSc1	kompromis
mezi	mezi	k7c7	mezi
stanovisky	stanovisko	k1gNnPc7	stanovisko
basilejského	basilejský	k2eAgInSc2d1	basilejský
koncilu	koncil	k1gInSc2	koncil
a	a	k8xC	a
husitů	husita	k1gMnPc2	husita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
podmínkách	podmínka	k1gFnPc6	podmínka
přijetí	přijetí	k1gNnSc2	přijetí
císaře	císař	k1gMnSc2	císař
Zikmunda	Zikmund	k1gMnSc2	Zikmund
za	za	k7c4	za
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
přijetí	přijetí	k1gNnSc4	přijetí
v	v	k7c6	v
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
na	na	k7c6	na
jihlavském	jihlavský	k2eAgNnSc6d1	jihlavské
náměstí	náměstí	k1gNnSc6	náměstí
14.	[number]	k4	14.
srpna	srpen	k1gInSc2	srpen
1436	[number]	k4	1436
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sněmu	sněm	k1gInSc6	sněm
30.	[number]	k4	30.
září	září	k1gNnSc2	září
1437	[number]	k4	1437
byla	být	k5eAaImAgFnS	být
proti	proti	k7c3	proti
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
vznesena	vznést	k5eAaPmNgFnS	vznést
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
stížností	stížnost	k1gFnPc2	stížnost
–	–	k?	–
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
že	že	k8xS	že
dal	dát	k5eAaPmAgMnS	dát
popravit	popravit	k5eAaPmF	popravit
hejtmana	hejtman	k1gMnSc2	hejtman
Jana	Jan	k1gMnSc2	Jan
Roháče	roháč	k1gMnSc2	roháč
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
se	se	k3xPyFc4	se
chystal	chystat	k5eAaImAgMnS	chystat
zajistit	zajistit	k5eAaPmF	zajistit
dědictví	dědictví	k1gNnSc4	dědictví
manželovi	manžel	k1gMnSc3	manžel
své	svůj	k3xOyFgFnSc2	svůj
jediné	jediný	k2eAgFnSc2d1	jediná
dcery	dcera	k1gFnSc2	dcera
Alžběty	Alžběta	k1gFnSc2	Alžběta
Albrechtovi	Albrecht	k1gMnSc3	Albrecht
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
vyhnout	vyhnout	k5eAaPmF	vyhnout
politickým	politický	k2eAgFnPc3d1	politická
ambicím	ambice	k1gFnPc3	ambice
své	svůj	k3xOyFgFnSc2	svůj
druhé	druhý	k4xOgFnSc2	druhý
ženy	žena	k1gFnSc2	žena
Barbory	Barbora	k1gFnSc2	Barbora
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
synovce	synovec	k1gMnSc2	synovec
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
slavný	slavný	k2eAgMnSc1d1	slavný
Lucemburk	Lucemburk	k1gMnSc1	Lucemburk
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
9.	[number]	k4	9.
prosince	prosinec	k1gInSc2	prosinec
1437	[number]	k4	1437
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
ve	v	k7c6	v
Velkém	velký	k2eAgInSc6d1	velký
Varadíně	Varadín	k1gInSc6	Varadín
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
hrobky	hrobka	k1gFnSc2	hrobka
sv.	sv.	kA	sv.
Ladislava	Ladislav	k1gMnSc2	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
nezanechal	zanechat	k5eNaPmAgMnS	zanechat
mužského	mužský	k2eAgMnSc4d1	mužský
potomka	potomek	k1gMnSc4	potomek
<g/>
,	,	kIx,	,
jím	on	k3xPp3gInSc7	on
vymřela	vymřít	k5eAaPmAgFnS	vymřít
linie	linie	k1gFnSc2	linie
Lucemburků	Lucemburk	k1gMnPc2	Lucemburk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
==	==	k?	==
</s>
</p>
<p>
<s>
1368	[number]	k4	1368
narození	narození	k1gNnSc1	narození
</s>
</p>
<p>
<s>
1387	[number]	k4	1387
korunovace	korunovace	k1gFnSc1	korunovace
uherským	uherský	k2eAgMnSc7d1	uherský
králem	král	k1gMnSc7	král
31.	[number]	k4	31.
března	březen	k1gInSc2	březen
</s>
</p>
<p>
<s>
1392	[number]	k4	1392
první	první	k4xOgFnSc7	první
tažení	tažení	k1gNnPc2	tažení
proti	proti	k7c3	proti
Turkům	turek	k1gInPc3	turek
</s>
</p>
<p>
<s>
1394	[number]	k4	1394
dobytí	dobytí	k1gNnSc4	dobytí
Dalmácie	Dalmácie	k1gFnSc2	Dalmácie
a	a	k8xC	a
tažení	tažení	k1gNnSc2	tažení
proti	proti	k7c3	proti
Moldavsku	Moldavsko	k1gNnSc3	Moldavsko
</s>
</p>
<p>
<s>
1395	[number]	k4	1395
tažení	tažení	k1gNnPc1	tažení
proti	proti	k7c3	proti
Turkům	Turek	k1gMnPc3	Turek
a	a	k8xC	a
do	do	k7c2	do
Valašska	Valašsko	k1gNnSc2	Valašsko
</s>
</p>
<p>
<s>
1396	[number]	k4	1396
útěk	útěk	k1gInSc4	útěk
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Nikopole	Nikopole	k1gFnSc2	Nikopole
</s>
</p>
<p>
<s>
1397	[number]	k4	1397
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
v	v	k7c6	v
Temešváru	Temešvár	k1gInSc6	Temešvár
</s>
</p>
<p>
<s>
1401	[number]	k4	1401
držen	držet	k5eAaImNgMnS	držet
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Siklós	Siklósa	k1gFnPc2	Siklósa
</s>
</p>
<p>
<s>
1401	[number]	k4	1401
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
v	v	k7c6	v
Pápě	Pápa	k1gFnSc6	Pápa
</s>
</p>
<p>
<s>
1402	[number]	k4	1402
zajetí	zajetí	k1gNnSc1	zajetí
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
věznění	věznění	k1gNnSc2	věznění
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
</s>
</p>
<p>
<s>
1404	[number]	k4	1404
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
v	v	k7c6	v
Budíně	Budín	k1gInSc6	Budín
</s>
</p>
<p>
<s>
1408	[number]	k4	1408
tažení	tažení	k1gNnPc1	tažení
do	do	k7c2	do
Bosny	Bosna	k1gFnSc2	Bosna
</s>
</p>
<p>
<s>
1408	[number]	k4	1408
založení	založení	k1gNnSc3	založení
dračího	dračí	k2eAgInSc2d1	dračí
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
<s>
1411	[number]	k4	1411
smíření	smíření	k1gNnSc1	smíření
s	s	k7c7	s
Václavem	Václav	k1gMnSc7	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1414	[number]	k4	1414
až	až	k6eAd1	až
1418	[number]	k4	1418
sněm	sněm	k1gInSc4	sněm
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
</s>
</p>
<p>
<s>
1419	[number]	k4	1419
až	až	k6eAd1	až
1436	[number]	k4	1436
husitské	husitský	k2eAgFnSc2d1	husitská
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
1424	[number]	k4	1424
jednání	jednání	k1gNnSc1	jednání
s	s	k7c7	s
byzantským	byzantský	k2eAgMnSc7d1	byzantský
císařem	císař	k1gMnSc7	císař
Janem	Jan	k1gMnSc7	Jan
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
</s>
</p>
<p>
<s>
1429	[number]	k4	1429
uzavření	uzavření	k1gNnSc1	uzavření
tříletého	tříletý	k2eAgNnSc2d1	tříleté
příměří	příměří	k1gNnSc2	příměří
s	s	k7c7	s
Turky	Turek	k1gMnPc7	Turek
</s>
</p>
<p>
<s>
1430	[number]	k4	1430
říšský	říšský	k2eAgInSc1d1	říšský
sněm	sněm	k1gInSc1	sněm
ve	v	k7c6	v
Straubingu	Straubing	k1gInSc6	Straubing
</s>
</p>
<p>
<s>
1433	[number]	k4	1433
až	až	k6eAd1	až
1437	[number]	k4	1437
sněm	sněm	k1gInSc4	sněm
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
odešel	odejít	k5eAaPmAgMnS	odejít
nespokojen	spokojen	k2eNgMnSc1d1	nespokojen
</s>
</p>
<p>
<s>
1434	[number]	k4	1434
projekt	projekt	k1gInSc1	projekt
reformy	reforma	k1gFnSc2	reforma
říše	říš	k1gFnSc2	říš
</s>
</p>
<p>
<s>
1436	[number]	k4	1436
příjezd	příjezd	k1gInSc4	příjezd
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
</s>
</p>
<p>
<s>
1437	[number]	k4	1437
říšský	říšský	k2eAgInSc1d1	říšský
sněm	sněm	k1gInSc1	sněm
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
<g/>
,	,	kIx,	,
diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
reformě	reforma	k1gFnSc6	reforma
říše	říš	k1gFnSc2	říš
</s>
</p>
<p>
<s>
1437	[number]	k4	1437
úmrtí	úmrtí	k1gNnPc1	úmrtí
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
obraz	obraz	k1gInSc1	obraz
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
ve	v	k7c6	v
filmu	film	k1gInSc6	film
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Genealogie	genealogie	k1gFnSc2	genealogie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zikmund	Zikmund	k1gMnSc1	Zikmund
dnes	dnes	k6eAd1	dnes
==	==	k?	==
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
komiksu	komiks	k1gInSc2	komiks
Opráski	Oprásk	k1gFnSc2	Oprásk
sčeskí	sčeskí	k2eAgFnSc2d1	sčeskí
historje	historj	k1gFnSc2	historj
je	být	k5eAaImIp3nS	být
Zmikund	Zmikund	k1gInSc1	Zmikund
<g/>
.	.	kIx.	.
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1	vydavatel
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
zdánlivě	zdánlivě	k6eAd1	zdánlivě
hloupých	hloupý	k2eAgInPc6d1	hloupý
obrázcích	obrázek	k1gInPc6	obrázek
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dějiny	dějiny	k1gFnPc1	dějiny
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vykládány	vykládat	k5eAaImNgInP	vykládat
i	i	k9	i
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
léta	léto	k1gNnSc2	léto
tradovalo	tradovat	k5eAaImAgNnS	tradovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAUM	BAUM	kA	BAUM
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Zikmund	Zikmund	k1gMnSc1	Zikmund
:	:	kIx,	:
Kostnice	Kostnice	k1gFnSc1	Kostnice
<g/>
,	,	kIx,	,
Hus	Hus	k1gMnSc1	Hus
a	a	k8xC	a
války	válka	k1gFnPc1	válka
proti	proti	k7c3	proti
Turkům	Turek	k1gMnPc3	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1996.	[number]	k4	1996.
405	[number]	k4	405
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-204-0543-7	[number]	k4	80-204-0543-7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BARTL	Bartl	k1gMnSc1	Bartl
<g/>
,	,	kIx,	,
Július	Július	k1gMnSc1	Július
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Prvý	prvý	k4xOgInSc1	prvý
cisár	cisár	k1gInSc1	cisár
na	na	k7c4	na
uhorskom	uhorskom	k1gInSc4	uhorskom
tróne	trón	k1gInSc5	trón
:	:	kIx,	:
Slovensko	Slovensko	k1gNnSc1	Slovensko
v	v	k7c6	v
čase	čas	k1gInSc6	čas
polstoročnej	polstoročnat	k5eAaPmRp2nS	polstoročnat
vlády	vláda	k1gFnSc2	vláda
uhorského	uhorský	k2eAgInSc2d1	uhorský
<g/>
,	,	kIx,	,
českého	český	k2eAgInSc2d1	český
<g/>
,	,	kIx,	,
lombardského	lombardský	k2eAgMnSc2d1	lombardský
a	a	k8xC	a
nemeckého	mecký	k2eNgMnSc2d1	nemecký
kráľa	kráľus	k1gMnSc2	kráľus
a	a	k8xC	a
rímského	rímský	k1gMnSc2	rímský
císara	císar	k1gMnSc2	císar
Žigmunda	Žigmund	k1gMnSc2	Žigmund
Luxemburského	Luxemburský	k2eAgMnSc2d1	Luxemburský
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Karola	Karola	k1gFnSc1	Karola
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Literárne	Literárn	k1gInSc5	Literárn
informačné	informačný	k2eAgNnSc1d1	informačné
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
2001.	[number]	k4	2001.
375	[number]	k4	375
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-88878-57-8	[number]	k4	80-88878-57-8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BARTL	Bartl	k1gMnSc1	Bartl
<g/>
,	,	kIx,	,
Július	Július	k1gMnSc1	Július
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Žigmund	Žigmund	k1gInSc1	Žigmund
Luxemburský	Luxemburský	k2eAgInSc1d1	Luxemburský
<g/>
.	.	kIx.	.
</s>
<s>
Budmerice	Budmerika	k1gFnSc3	Budmerika
<g/>
:	:	kIx,	:
Rak	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
1996.	[number]	k4	1996.
102	[number]	k4	102
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-85501-11-2	[number]	k4	80-85501-11-2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BOBKOVÁ	Bobková	k1gFnSc1	Bobková
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
<g/>
;	;	kIx,	;
ŠMAHEL	ŠMAHEL	kA	ŠMAHEL
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
:	:	kIx,	:
česká	český	k2eAgFnSc1d1	Česká
koruna	koruna	k1gFnSc1	koruna
uprostřed	uprostřed	k7c2	uprostřed
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2012.	[number]	k4	2012.
886	[number]	k4	886
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7422-093-7	[number]	k4	978-80-7422-093-7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
v	v	k7c6	v
letech	let	k1gInPc6	let
1378	[number]	k4	1378
<g/>
–	–	k?	–
<g/>
1437	[number]	k4	1437
:	:	kIx,	:
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2000.	[number]	k4	2000.
438	[number]	k4	438
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-85983-98-2	[number]	k4	80-85983-98-2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
ŽŮREK	ŽŮREK	kA	ŽŮREK
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
:	:	kIx,	:
životopisná	životopisný	k2eAgFnSc1d1	životopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2012.	[number]	k4	2012.
260	[number]	k4	260
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-86829-69-2	[number]	k4	978-80-86829-69-2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Světla	světlo	k1gNnPc1	světlo
a	a	k8xC	a
stíny	stín	k1gInPc1	stín
husitství	husitství	k1gNnSc2	husitství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2011.	[number]	k4	2011.
482	[number]	k4	482
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7422-084-5	[number]	k4	978-80-7422-084-5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
V.	V.	kA	V.
1402	[number]	k4	1402
<g/>
–	–	k?	–
<g/>
1437	[number]	k4	1437
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2000.	[number]	k4	2000.
790	[number]	k4	790
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7185-296-1	[number]	k4	80-7185-296-1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7185-940-6	[number]	k4	978-80-7185-940-6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
223-236	[number]	k4	223-236
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DRŠKA	drška	k1gFnSc1	drška
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
:	:	kIx,	:
liška	liška	k1gFnSc1	liška
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Epocha	epocha	k1gFnSc1	epocha
<g/>
,	,	kIx,	,
1996.	[number]	k4	1996.
87	[number]	k4	87
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-902129-0-5	[number]	k4	80-902129-0-5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ELBEL	ELBEL	kA	ELBEL
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
draka	drak	k1gMnSc4	drak
a	a	k8xC	a
kříže	kříž	k1gInPc4	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
(	(	kIx(	(
<g/>
1419-1423	[number]	k4	1419-1423
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
MITÁČEK	MITÁČEK	kA	MITÁČEK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Vládcové	vládce	k1gMnPc1	vládce
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
statí	stať	k1gFnPc2	stať
ze	z	k7c2	z
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
cyklu	cyklus	k1gInSc2	cyklus
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7028-304-2	[number]	k4	978-80-7028-304-2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
53-82	[number]	k4	53-82
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GADE	GADE	kA	GADE
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Allyne	Allyn	k1gInSc5	Allyn
<g/>
.	.	kIx.	.
</s>
<s>
Luxembourg	Luxembourg	k1gInSc1	Luxembourg
in	in	k?	in
the	the	k?	the
Middle	Middle	k1gFnSc2	Middle
Ages	Agesa	k1gFnPc2	Agesa
<g/>
.	.	kIx.	.
</s>
<s>
Luxembourg	Luxembourg	k1gMnSc1	Luxembourg
<g/>
:	:	kIx,	:
E.	E.	kA	E.
J.	J.	kA	J.
Brill	Brill	k1gMnSc1	Brill
<g/>
,	,	kIx,	,
1951.	[number]	k4	1951.
251	[number]	k4	251
s	s	k7c7	s
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HOENSCH	HOENSCH	kA	HOENSCH
<g/>
,	,	kIx,	,
Jörg	Jörg	k1gInSc4	Jörg
Konrad	Konrada	k1gFnPc2	Konrada
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
:	:	kIx,	:
pozdně	pozdně	k6eAd1	pozdně
středověká	středověký	k2eAgFnSc1d1	středověká
dynastie	dynastie	k1gFnSc1	dynastie
celoevropského	celoevropský	k2eAgInSc2d1	celoevropský
významu	význam	k1gInSc2	význam
1308	[number]	k4	1308
<g/>
–	–	k?	–
<g/>
1437	[number]	k4	1437
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2003.	[number]	k4	2003.
304	[number]	k4	304
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-7203-518-5	[number]	k4	80-7203-518-5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOENSCH	HOENSCH	kA	HOENSCH
<g/>
,	,	kIx,	,
Jörg	Jörg	k1gInSc4	Jörg
Konrad	Konrada	k1gFnPc2	Konrada
<g/>
.	.	kIx.	.
</s>
<s>
Kaiser	Kaiser	k1gMnSc1	Kaiser
Sigismund	Sigismund	k1gMnSc1	Sigismund
<g/>
.	.	kIx.	.
</s>
<s>
Herrscher	Herrschra	k1gFnPc2	Herrschra
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Schwelle	Schwelle	k1gInSc1	Schwelle
zur	zur	k?	zur
Neuzeit	Neuzeit	k1gInSc1	Neuzeit
(	(	kIx(	(
<g/>
1368	[number]	k4	1368
<g/>
–	–	k?	–
<g/>
1437	[number]	k4	1437
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
München	München	k1gInSc1	München
<g/>
:	:	kIx,	:
C.	C.	kA	C.
H.	H.	kA	H.
Beck	Beck	k1gMnSc1	Beck
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3-406-41119-3	[number]	k4	3-406-41119-3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAVKA	Kavka	k1gMnSc1	Kavka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
Lucemburk	Lucemburk	k1gInSc1	Lucemburk
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Králem	Král	k1gMnSc7	Král
uprostřed	uprostřed	k7c2	uprostřed
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1998.	[number]	k4	1998.
290	[number]	k4	290
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
80-204-0680-8	[number]	k4	80-204-0680-8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RAPP	Rapp	k1gInSc1	Rapp
<g/>
,	,	kIx,	,
Francis	Francis	k1gInSc1	Francis
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
národa	národ	k1gInSc2	národ
německého	německý	k2eAgInSc2d1	německý
:	:	kIx,	:
od	od	k7c2	od
Oty	Ota	k1gMnSc2	Ota
Velikého	veliký	k2eAgMnSc2d1	veliký
po	po	k7c4	po
Karla	Karel	k1gMnSc4	Karel
V.	V.	kA	V.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2007.	[number]	k4	2007.
316	[number]	k4	316
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
978-80-7185-726-6	[number]	k4	978-80-7185-726-6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SCHNEIDMÜLLER	SCHNEIDMÜLLER	kA	SCHNEIDMÜLLER
<g/>
,	,	kIx,	,
Bernd	Bernd	k1gMnSc1	Bernd
<g/>
;	;	kIx,	;
WEINFURTER	WEINFURTER	kA	WEINFURTER
<g/>
,	,	kIx,	,
Stefan	Stefan	k1gMnSc1	Stefan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
deutschen	deutschen	k1gInSc1	deutschen
Herrscher	Herrschra	k1gFnPc2	Herrschra
des	des	k1gNnSc2	des
Mittelalters	Mittelaltersa	k1gFnPc2	Mittelaltersa
:	:	kIx,	:
Historische	Historische	k1gInSc1	Historische
Porträts	Porträtsa	k1gFnPc2	Porträtsa
von	von	k1gInSc1	von
Heinrich	Heinrich	k1gMnSc1	Heinrich
I.	I.	kA	I.
bis	bis	k?	bis
Maximilian	Maximilian	k1gInSc1	Maximilian
I.	I.	kA	I.
München	München	k2eAgInSc1d1	München
<g/>
:	:	kIx,	:
Beck	Beck	k1gInSc1	Beck
<g/>
,	,	kIx,	,
2003.	[number]	k4	2003.
624	[number]	k4	624
s	s	k7c7	s
<g/>
.	.	kIx.	.
ISBN	ISBN	kA	ISBN
3-406-50958-4	[number]	k4	3-406-50958-4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
Lucemburků	Lucemburk	k1gMnPc2	Lucemburk
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
panovníků	panovník	k1gMnPc2	panovník
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Klípa	Klíp	k1gMnSc2	Klíp
<g/>
:	:	kIx,	:
Suverén	suverén	k1gMnSc1	suverén
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
zbroji	zbroj	k1gFnSc6	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Portréty	portrét	k1gInPc1	portrét
císaře	císař	k1gMnSc2	císař
Zikmunda	Zikmund	k1gMnSc2	Zikmund
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
</s>
</p>
<p>
<s>
Majestátní	majestátní	k2eAgFnSc4d1	majestátní
pečeť	pečeť	k1gFnSc4	pečeť
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
,	,	kIx,	,
římského	římský	k2eAgMnSc2d1	římský
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
uherského	uherský	k2eAgMnSc2d1	uherský
a	a	k8xC	a
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
1437	[number]	k4	1437
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
psaní	psaní	k1gNnSc1	psaní
císaře	císař	k1gMnSc2	císař
Sigmunda	Sigmund	k1gMnSc2	Sigmund
ke	k	k7c3	k
stawům	staw	k1gMnPc3	staw
Českým	český	k2eAgFnPc3d1	Česká
</s>
</p>
<p>
<s>
Zikmundův	Zikmundův	k2eAgInSc4d1	Zikmundův
list	list	k1gInSc4	list
potvrzující	potvrzující	k2eAgNnSc1d1	potvrzující
přijímání	přijímání	k1gNnSc1	přijímání
podobojí	podobojí	k2eAgNnSc1d1	podobojí
městu	město	k1gNnSc3	město
Přerovu	Přerov	k1gInSc3	Přerov
(	(	kIx(	(
<g/>
1436	[number]	k4	1436
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
<p>
<s>
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
6/6	[number]	k4	6/6
–	–	k?	–
Zikmund	Zikmund	k1gMnSc1	Zikmund
–	–	k?	–
dokument	dokument	k1gInSc1	dokument
ČT	ČT	kA	ČT
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
on-line	onin	k1gInSc5	on-lin
přehrání	přehrání	k1gNnSc3	přehrání
</s>
</p>
