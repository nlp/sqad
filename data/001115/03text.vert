<s>
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Josef	Josef	k1gMnSc1	Josef
Jakub	Jakub	k1gMnSc1	Jakub
Jungmann	Jungmann	k1gMnSc1	Jungmann
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1773	[number]	k4	1773
Hudlice	Hudlice	k1gFnPc1	Hudlice
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1847	[number]	k4	1847
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
filolog	filolog	k1gMnSc1	filolog
<g/>
,	,	kIx,	,
lexikograf	lexikograf	k1gMnSc1	lexikograf
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
šesté	šestý	k4xOgNnSc4	šestý
dítě	dítě	k1gNnSc4	dítě
z	z	k7c2	z
deseti	deset	k4xCc2	deset
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
ševce	švec	k1gMnSc2	švec
v	v	k7c6	v
Hudlicích	Hudlice	k1gFnPc6	Hudlice
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
určen	určit	k5eAaPmNgInS	určit
k	k	k7c3	k
církevní	církevní	k2eAgFnSc3d1	církevní
kariéře	kariéra	k1gFnSc3	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Antonín	Antonín	k1gMnSc1	Antonín
Jan	Jan	k1gMnSc1	Jan
Jungmann	Jungmann	k1gMnSc1	Jungmann
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
lékařem	lékař	k1gMnSc7	lékař
a	a	k8xC	a
univerzitním	univerzitní	k2eAgMnSc7d1	univerzitní
profesorem	profesor	k1gMnSc7	profesor
gynekologie	gynekologie	k1gFnSc2	gynekologie
<g/>
,	,	kIx,	,
další	další	k2eAgMnSc1d1	další
bratr	bratr	k1gMnSc1	bratr
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Řádu	řád	k1gInSc2	řád
křižovníků	křižovník	k1gMnPc2	křižovník
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1784	[number]	k4	1784
až	až	k9	až
1788	[number]	k4	1788
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
piaristické	piaristický	k2eAgNnSc4d1	Piaristické
gymnasium	gymnasium	k1gNnSc4	gymnasium
v	v	k7c6	v
Berouně	Beroun	k1gInSc6	Beroun
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1792	[number]	k4	1792
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
piaristické	piaristický	k2eAgNnSc4d1	Piaristické
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Panské	panský	k2eAgFnSc6d1	Panská
ulici	ulice	k1gFnSc6	ulice
na	na	k7c6	na
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
-	-	kIx~	-
<g/>
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
<g/>
)	)	kIx)	)
studoval	studovat	k5eAaImAgMnS	studovat
také	také	k9	také
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
studium	studium	k1gNnSc4	studium
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
působil	působit	k5eAaImAgMnS	působit
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k8xS	jako
soukromý	soukromý	k2eAgMnSc1d1	soukromý
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
1799	[number]	k4	1799
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
na	na	k7c6	na
tamním	tamní	k2eAgNnSc6d1	tamní
gymnáziu	gymnázium	k1gNnSc6	gymnázium
vyučoval	vyučovat	k5eAaImAgInS	vyučovat
češtinu	čeština	k1gFnSc4	čeština
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
nepobíral	pobírat	k5eNaImAgInS	pobírat
plat	plat	k1gInSc1	plat
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
prvním	první	k4xOgMnSc7	první
učitelem	učitel	k1gMnSc7	učitel
češtiny	čeština	k1gFnSc2	čeština
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
tam	tam	k6eAd1	tam
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Jodl	Jodl	k1gMnSc1	Jodl
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
litoměřické	litoměřický	k2eAgNnSc1d1	Litoměřické
gymnázium	gymnázium	k1gNnSc1	gymnázium
nese	nést	k5eAaImIp3nS	nést
Jungmannovo	Jungmannův	k2eAgNnSc4d1	Jungmannovo
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1815	[number]	k4	1815
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
(	(	kIx(	(
<g/>
prefektem	prefekt	k1gMnSc7	prefekt
<g/>
)	)	kIx)	)
Akademického	akademický	k2eAgNnSc2d1	akademické
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
jazyk	jazyk	k1gMnSc1	jazyk
Jungmann	Jungmann	k1gMnSc1	Jungmann
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
také	také	k9	také
na	na	k7c6	na
filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
stal	stát	k5eAaPmAgMnS	stát
rektorem	rektor	k1gMnSc7	rektor
<g/>
.	.	kIx.	.
</s>
<s>
Jungmann	Jungmann	k1gMnSc1	Jungmann
položil	položit	k5eAaPmAgMnS	položit
teoretické	teoretický	k2eAgInPc4d1	teoretický
základy	základ	k1gInPc4	základ
vývoji	vývoj	k1gInSc3	vývoj
novodobé	novodobý	k2eAgFnSc2d1	novodobá
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
tzv.	tzv.	kA	tzv.
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
preromantické	preromantický	k2eAgFnPc1d1	preromantický
<g/>
)	)	kIx)	)
generace	generace	k1gFnPc1	generace
obrozenců	obrozenec	k1gMnPc2	obrozenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Johannou	Johanný	k2eAgFnSc7d1	Johanný
Světeckou	světecký	k2eAgFnSc7d1	světecká
z	z	k7c2	z
Černčic	Černčice	k1gFnPc2	Černčice
(	(	kIx(	(
<g/>
1780	[number]	k4	1780
<g/>
-	-	kIx~	-
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1800	[number]	k4	1800
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
<g/>
.	.	kIx.	.
</s>
<s>
Ženichovi	ženichův	k2eAgMnPc1d1	ženichův
bylo	být	k5eAaImAgNnS	být
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nevěstě	nevěsta	k1gFnSc3	nevěsta
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Manželka	manželka	k1gFnSc1	manželka
pocházela	pocházet	k5eAaImAgFnS	pocházet
ze	z	k7c2	z
staročeského	staročeský	k2eAgInSc2d1	staročeský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc4d1	známý
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
rodu	rod	k1gInSc2	rod
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
třeboňský	třeboňský	k2eAgMnSc1d1	třeboňský
archivář	archivář	k1gMnSc1	archivář
Petr	Petr	k1gMnSc1	Petr
Kašpar	Kašpar	k1gMnSc1	Kašpar
Světecký	světecký	k2eAgMnSc1d1	světecký
<g/>
.	.	kIx.	.
</s>
<s>
Mateřštinou	mateřština	k1gFnSc7	mateřština
Anny	Anna	k1gFnSc2	Anna
Světecké	světecký	k2eAgFnSc2d1	světecká
byla	být	k5eAaImAgFnS	být
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
se	se	k3xPyFc4	se
však	však	k9	však
naučila	naučit	k5eAaPmAgFnS	naučit
a	a	k8xC	a
manželé	manžel	k1gMnPc1	manžel
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnPc1	jejich
děti	dítě	k1gFnPc1	dítě
budou	být	k5eAaImBp3nP	být
vychovávány	vychovávat	k5eAaImNgFnP	vychovávat
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
bydlel	bydlet	k5eAaImAgMnS	bydlet
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
v	v	k7c6	v
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
749	[number]	k4	749
<g/>
/	/	kIx~	/
<g/>
II	II	kA	II
v	v	k7c6	v
Široké	Široké	k2eAgFnSc6d1	Široké
ulici	ulice	k1gFnSc6	ulice
32	[number]	k4	32
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
na	na	k7c4	na
Jungmannovu	Jungmannův	k2eAgFnSc4d1	Jungmannova
ulici	ulice	k1gFnSc4	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
měli	mít	k5eAaImAgMnP	mít
spolu	spolu	k6eAd1	spolu
šest	šest	k4xCc4	šest
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
dospělosti	dospělost	k1gFnPc1	dospělost
dožily	dožít	k5eAaPmAgFnP	dožít
čtyři	čtyři	k4xCgNnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
na	na	k7c6	na
Olšanských	olšanský	k2eAgInPc6d1	olšanský
hřbitovech	hřbitov	k1gInPc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
prvního	první	k4xOgInSc2	první
českého	český	k2eAgInSc2d1	český
vědeckého	vědecký	k2eAgInSc2d1	vědecký
časopisu	časopis	k1gInSc2	časopis
Krok	krok	k1gInSc1	krok
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
obrozenců	obrozenec	k1gMnPc2	obrozenec
okolo	okolo	k7c2	okolo
něho	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
vytvořivší	vytvořivší	k2eAgMnPc1d1	vytvořivší
nazýváme	nazývat	k5eAaImIp1nP	nazývat
dnes	dnes	k6eAd1	dnes
tzv.	tzv.	kA	tzv.
Jungmannovou	Jungmannový	k2eAgFnSc7d1	Jungmannová
básnickou	básnický	k2eAgFnSc7d1	básnická
a	a	k8xC	a
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
školou	škola	k1gFnSc7	škola
(	(	kIx(	(
<g/>
patřily	patřit	k5eAaImAgInP	patřit
sem	sem	k6eAd1	sem
všechny	všechen	k3xTgFnPc1	všechen
významné	významný	k2eAgFnPc1d1	významná
osobnosti	osobnost	k1gFnPc1	osobnost
české	český	k2eAgFnSc2d1	Česká
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Ladislav	Ladislav	k1gMnSc1	Ladislav
Čelakovský	Čelakovský	k2eAgMnSc1d1	Čelakovský
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
této	tento	k3xDgFnSc2	tento
(	(	kIx(	(
<g/>
vědecké	vědecký	k2eAgFnSc2d1	vědecká
<g/>
)	)	kIx)	)
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vydat	vydat	k5eAaPmF	vydat
své	svůj	k3xOyFgNnSc4	svůj
životní	životní	k2eAgNnSc4d1	životní
dílo	dílo	k1gNnSc4	dílo
<g/>
:	:	kIx,	:
pětidílný	pětidílný	k2eAgInSc1d1	pětidílný
Slovník	slovník	k1gInSc1	slovník
česko-německý	českoěmecký	k2eAgInSc1d1	česko-německý
(	(	kIx(	(
<g/>
120	[number]	k4	120
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
(	(	kIx(	(
<g/>
po	po	k7c6	po
Dobrovského	Dobrovského	k2eAgFnSc6d1	Dobrovského
Zevrubné	zevrubný	k2eAgFnSc6d1	zevrubná
mluvnici	mluvnice	k1gFnSc6	mluvnice
české	český	k2eAgFnSc6d1	Česká
<g/>
)	)	kIx)	)
druhým	druhý	k4xOgInSc7	druhý
základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
pro	pro	k7c4	pro
pozvolné	pozvolný	k2eAgNnSc4d1	pozvolné
ustanovení	ustanovení	k1gNnSc4	ustanovení
normy	norma	k1gFnSc2	norma
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Dobrotivý	dobrotivý	k2eAgInSc1d1	dobrotivý
mu	on	k3xPp3gNnSc3	on
za	za	k7c4	za
vydání	vydání	k1gNnSc4	vydání
Slovníku	slovník	k1gInSc2	slovník
udělil	udělit	k5eAaPmAgMnS	udělit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1839	[number]	k4	1839
Leopoldův	Leopoldův	k2eAgInSc4d1	Leopoldův
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
sestavit	sestavit	k5eAaPmF	sestavit
první	první	k4xOgFnSc4	první
českou	český	k2eAgFnSc4d1	Česká
"	"	kIx"	"
<g/>
malou	malý	k2eAgFnSc4d1	malá
encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
obsáhnout	obsáhnout	k5eAaPmF	obsáhnout
hlavní	hlavní	k2eAgFnPc4d1	hlavní
vědní	vědní	k2eAgFnPc4d1	vědní
obory	obora	k1gFnPc4	obora
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
záměr	záměr	k1gInSc1	záměr
se	se	k3xPyFc4	se
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
rukopisů	rukopis	k1gInPc2	rukopis
královédvorského	královédvorský	k2eAgNnSc2d1	královédvorské
a	a	k8xC	a
zelenohorského	zelenohorský	k2eAgNnSc2d1	zelenohorský
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
jejich	jejich	k3xOp3gFnSc4	jejich
pravost	pravost	k1gFnSc4	pravost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
překladech	překlad	k1gInPc6	překlad
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
netvořil	tvořit	k5eNaImAgInS	tvořit
však	však	k9	však
slova	slovo	k1gNnPc1	slovo
bez	bez	k7c2	bez
pravidel	pravidlo	k1gNnPc2	pravidlo
(	(	kIx(	(
<g/>
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
dodržovat	dodržovat	k5eAaImF	dodržovat
zásady	zásada	k1gFnPc1	zásada
J.	J.	kA	J.
Dobrovského	Dobrovského	k2eAgInPc1d1	Dobrovského
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyhledával	vyhledávat	k5eAaImAgMnS	vyhledávat
tato	tento	k3xDgNnPc4	tento
slova	slovo	k1gNnPc4	slovo
z	z	k7c2	z
příbuzných	příbuzný	k1gMnPc2	příbuzný
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
ta	ten	k3xDgNnPc1	ten
pak	pak	k6eAd1	pak
počešťoval	počešťovat	k5eAaImAgMnS	počešťovat
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
maximální	maximální	k2eAgFnSc4d1	maximální
přesnost	přesnost	k1gFnSc4	přesnost
překladů	překlad	k1gInPc2	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
k	k	k7c3	k
překladům	překlad	k1gInPc3	překlad
připojoval	připojovat	k5eAaImAgMnS	připojovat
hesláře	heslář	k1gInPc4	heslář
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zjednodušil	zjednodušit	k5eAaPmAgMnS	zjednodušit
četbu	četba	k1gFnSc4	četba
a	a	k8xC	a
napomohl	napomoct	k5eAaPmAgMnS	napomoct
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
přijetí	přijetí	k1gNnSc3	přijetí
novotvarů	novotvar	k1gInPc2	novotvar
<g/>
.	.	kIx.	.
</s>
<s>
Atala	Atala	k1gMnSc1	Atala
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
dwau	dwaus	k1gInSc2	dwaus
diwochů	diwoch	k1gMnPc2	diwoch
na	na	k7c6	na
paussti	pausst	k1gFnSc6	pausst
(	(	kIx(	(
<g/>
od	od	k7c2	od
François-René	François-Rená	k1gFnSc2	François-Rená
de	de	k?	de
Chateaubriand	Chateaubriand	k1gInSc1	Chateaubriand
<g/>
,	,	kIx,	,
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Frantissek	Frantissek	k1gMnSc1	Frantissek
Geřábek	Geřábek	k1gMnSc1	Geřábek
<g/>
,	,	kIx,	,
1805	[number]	k4	1805
<g/>
)	)	kIx)	)
Jana	Jan	k1gMnSc4	Jan
Miltona	Milton	k1gMnSc4	Milton
Ztracený	ztracený	k2eAgInSc1d1	ztracený
rág	rág	k?	rág
(	(	kIx(	(
<g/>
autor	autor	k1gMnSc1	autor
John	John	k1gMnSc1	John
Milton	Milton	k1gInSc4	Milton
<g/>
,	,	kIx,	,
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Háse	Hás	k1gFnSc2	Hás
<g/>
,	,	kIx,	,
1811	[number]	k4	1811
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Únos	únos	k1gInSc1	únos
ze	z	k7c2	z
serailu	serail	k1gInSc2	serail
(	(	kIx(	(
<g/>
zpěwohra	zpěwohra	k1gFnSc1	zpěwohra
we	we	k?	we
třech	tři	k4xCgInPc2	tři
děgstwich	děgstwicha	k1gFnPc2	děgstwicha
do	do	k7c2	do
hudby	hudba	k1gFnSc2	hudba
uwedená	uwedenat	k5eAaPmIp3nS	uwedenat
od	od	k7c2	od
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
;	;	kIx,	;
Christoph	Christoph	k1gMnSc1	Christoph
Friedrich	Friedrich	k1gMnSc1	Friedrich
Bretzner	Bretzner	k1gMnSc1	Bretzner
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Gottlieb	Gottliba	k1gFnPc2	Gottliba
<g/>
,	,	kIx,	,
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
Fetterlová	Fetterlová	k1gFnSc1	Fetterlová
<g/>
,	,	kIx,	,
1830	[number]	k4	1830
<g/>
)	)	kIx)	)
Herman	Herman	k1gMnSc1	Herman
a	a	k8xC	a
Dorota	Dorota	k1gFnSc1	Dorota
(	(	kIx(	(
<g/>
epos	epos	k1gInSc1	epos
idylické	idylický	k2eAgFnSc2d1	idylická
(	(	kIx(	(
<g/>
sic	sic	k8xC	sic
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Volfgang	Volfgang	k1gMnSc1	Volfgang
Goethe	Goethe	k1gFnSc1	Goethe
<g/>
,	,	kIx,	,
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
I.L.	I.L.	k1gFnSc3	I.L.
Kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
1841	[number]	k4	1841
<g/>
-	-	kIx~	-
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
zvonu	zvon	k1gInSc6	zvon
(	(	kIx(	(
<g/>
báseň	báseň	k1gFnSc1	báseň
od	od	k7c2	od
Friedricha	Friedrich	k1gMnSc2	Friedrich
ze	z	k7c2	z
Schillerů	Schiller	k1gMnPc2	Schiller
;	;	kIx,	;
dle	dle	k7c2	dle
překladu	překlad	k1gInSc2	překlad
J.	J.	kA	J.
Jungmannova	Jungmannov	k1gInSc2	Jungmannov
a	a	k8xC	a
J.	J.	kA	J.
Purkyňova	Purkyňův	k2eAgFnSc1d1	Purkyňova
hudbě	hudba	k1gFnSc3	hudba
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Romberga	Romberg	k1gMnSc2	Romberg
přispůsobil	přispůsobit	k5eAaImAgMnS	přispůsobit
(	(	kIx(	(
<g/>
sic	sic	k8xC	sic
<g/>
)	)	kIx)	)
František	František	k1gMnSc1	František
Xav	Xav	k1gMnSc1	Xav
<g/>
.	.	kIx.	.
</s>
<s>
Částka	částka	k1gFnSc1	částka
<g/>
,	,	kIx,	,
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Tiskem	tisk	k1gInSc7	tisk
a	a	k8xC	a
nákladem	náklad	k1gInSc7	náklad
Antonín	Antonín	k1gMnSc1	Antonín
Renna	Renn	k1gMnSc2	Renn
<g/>
,	,	kIx,	,
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
Slowesnost	Slowesnost	k1gFnSc1	Slowesnost
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
<g/>
,	,	kIx,	,
Zbjrka	Zbjrka	k1gMnSc1	Zbjrka
přjkladů	přjklad	k1gInPc2	přjklad
s	s	k7c7	s
krátkým	krátký	k2eAgNnSc7d1	krátké
pogednánjm	pogednánj	k1gNnSc7	pogednánj
o	o	k7c6	o
slohu	sloh	k1gInSc6	sloh
k	k	k7c3	k
prospěchu	prospěch	k1gInSc3	prospěch
wlastenecké	wlastenecký	k2eAgFnSc2d1	wlastenecký
mládeže	mládež	k1gFnSc2	mládež
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Jungmanna	Jungmann	k1gMnSc2	Jungmann
(	(	kIx(	(
<g/>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Josefa	Josefa	k1gFnSc1	Josefa
Fetteerlowá	Fetteerlowá	k1gFnSc1	Fetteerlowá
z	z	k7c2	z
Wildenbrunu	Wildenbrun	k1gInSc2	Wildenbrun
<g/>
,	,	kIx,	,
1820	[number]	k4	1820
<g/>
)	)	kIx)	)
-	-	kIx~	-
teorie	teorie	k1gFnSc1	teorie
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
líčí	líčit	k5eAaImIp3nS	líčit
zde	zde	k6eAd1	zde
bohatství	bohatství	k1gNnSc4	bohatství
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
od	od	k7c2	od
Husa	Hus	k1gMnSc2	Hus
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
slohovou	slohový	k2eAgFnSc4d1	slohová
čítanku	čítanka	k1gFnSc4	čítanka
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
literatury	literatura	k1gFnSc2	literatura
české	český	k2eAgFnSc2d1	Česká
aneb	aneb	k?	aneb
saustawný	saustawný	k2eAgInSc4d1	saustawný
přehled	přehled	k1gInSc4	přehled
spisů	spis	k1gInPc2	spis
českých	český	k2eAgInPc2d1	český
<g/>
,	,	kIx,	,
s	s	k7c7	s
krátkau	krátkau	k6eAd1	krátkau
historij	historít	k5eAaPmRp2nS	historít
národu	národ	k1gInSc3	národ
<g/>
,	,	kIx,	,
oswiceni	oswicen	k2eAgMnPc1d1	oswicen
a	a	k8xC	a
gazyka	gazyka	k6eAd1	gazyka
pracj	pracj	k1gFnSc1	pracj
Josefa	Josef	k1gMnSc2	Josef
Jungmanna	Jungmann	k1gMnSc2	Jungmann
(	(	kIx(	(
<g/>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Pjsmem	Pjsm	k1gInSc7	Pjsm
Antonina	Antonin	k2eAgFnSc1d1	Antonina
Straširypky	Straširypek	k1gInPc4	Straširypek
<g/>
,	,	kIx,	,
1825	[number]	k4	1825
<g/>
)	)	kIx)	)
-	-	kIx~	-
bývá	bývat	k5eAaImIp3nS	bývat
používán	používat	k5eAaImNgInS	používat
zkrácený	zkrácený	k2eAgInSc1d1	zkrácený
název	název	k1gInSc1	název
Historie	historie	k1gFnSc2	historie
literatury	literatura	k1gFnSc2	literatura
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
soupis	soupis	k1gInSc4	soupis
všech	všecek	k3xTgFnPc2	všecek
tehdy	tehdy	k6eAd1	tehdy
známých	známý	k2eAgFnPc2d1	známá
českých	český	k2eAgFnPc2d1	Česká
literárních	literární	k2eAgFnPc2d1	literární
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
šest	šest	k4xCc4	šest
dílů	díl	k1gInPc2	díl
<g/>
:	:	kIx,	:
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
až	až	k9	až
po	po	k7c4	po
vymření	vymření	k1gNnSc4	vymření
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
(	(	kIx(	(
<g/>
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
1306	[number]	k4	1306
-	-	kIx~	-
až	až	k9	až
do	do	k7c2	do
Husa	Hus	k1gMnSc2	Hus
Od	od	k7c2	od
Husa	Hus	k1gMnSc2	Hus
po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
knihtisku	knihtisk	k1gInSc2	knihtisk
Od	od	k7c2	od
rozšíření	rozšíření	k1gNnSc2	rozšíření
knihtisku	knihtisk	k1gInSc2	knihtisk
do	do	k7c2	do
přemožení	přemožení	k1gNnSc2	přemožení
nekatolické	katolický	k2eNgFnSc2d1	nekatolická
strany	strana	k1gFnSc2	strana
Od	od	k7c2	od
přemožení	přemožení	k1gNnSc2	přemožení
nekatolické	katolický	k2eNgFnSc2d1	nekatolická
strany	strana	k1gFnSc2	strana
do	do	k7c2	do
zavedení	zavedení	k1gNnSc2	zavedení
němčiny	němčina	k1gFnSc2	němčina
do	do	k7c2	do
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
úřadů	úřad	k1gInPc2	úřad
Od	od	k7c2	od
zavedení	zavedení	k1gNnSc2	zavedení
němčiny	němčina	k1gFnSc2	němčina
do	do	k7c2	do
<g />
.	.	kIx.	.
</s>
<s>
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
úřadů	úřad	k1gInPc2	úřad
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
O	o	k7c6	o
počátku	počátek	k1gInSc6	počátek
a	a	k8xC	a
proměnách	proměna	k1gFnPc6	proměna
prawopisu	prawopis	k1gInSc2	prawopis
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
w	w	k?	w
čem	co	k3yQnSc6	co
tak	tak	k9	tak
nazwaná	nazwaná	k1gFnSc1	nazwaná
analogická	analogický	k2eAgFnSc1d1	analogická
od	od	k7c2	od
bratrské	bratrský	k2eAgFnSc2d1	bratrská
posawád	posawáda	k1gFnPc2	posawáda
užjwané	užjwaný	k2eAgFnSc2d1	užjwaný
orthografie	orthografie	k1gFnSc2	orthografie
se	se	k3xPyFc4	se
rozděluge	rozdělugat	k5eAaPmIp3nS	rozdělugat
<g/>
,	,	kIx,	,
k	k	k7c3	k
ljbeznému	ljbezný	k2eAgMnSc3d1	ljbezný
a	a	k8xC	a
nestrannému	nestranný	k2eAgInSc3d1	nestranný
uwáženj	uwáženj	k1gInSc1	uwáženj
wšem	wšem	k1gInSc4	wšem
P.	P.	kA	P.
<g/>
P.	P.	kA	P.
wlastencům	wlastenec	k1gMnPc3	wlastenec
w	w	k?	w
krátkosti	krátkost	k1gFnSc2	krátkost
podáno	podán	k2eAgNnSc1d1	podáno
(	(	kIx(	(
<g/>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Tiskem	tisk	k1gInSc7	tisk
Knjžecj	Knjžecj	k1gFnSc1	Knjžecj
arcibiskupské	arcibiskupský	k2eAgFnSc2d1	arcibiskupská
knihtiskárny	knihtiskárna	k1gFnSc2	knihtiskárna
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
Slownjk	Slownjk	k1gInSc4	Slownjk
česko-německý	českoěmecký	k2eAgInSc4d1	česko-německý
Josefa	Josef	k1gMnSc4	Josef
Jungmanna	Jungmanna	k1gFnSc1	Jungmanna
(	(	kIx(	(
<g/>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Pomocj	Pomocj	k1gMnSc1	Pomocj
Českého	český	k2eAgInSc2d1	český
Museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
1835	[number]	k4	1835
<g/>
-	-	kIx~	-
<g/>
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
-	-	kIx~	-
vydáván	vydáván	k2eAgInSc1d1	vydáván
1834	[number]	k4	1834
<g/>
-	-	kIx~	-
<g/>
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
000	[number]	k4	000
hesel	heslo	k1gNnPc2	heslo
Josefa	Josef	k1gMnSc2	Josef
Jungmanna	Jungmann	k1gMnSc2	Jungmann
Sebrané	sebraný	k2eAgInPc1d1	sebraný
spisy	spis	k1gInPc1	spis
<g />
.	.	kIx.	.
</s>
<s>
weršem	werš	k1gInSc7	werš
i	i	k9	i
prosau	prosau	k6eAd1	prosau
(	(	kIx(	(
<g/>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Nákladem	náklad	k1gInSc7	náklad
Českého	český	k2eAgInSc2d1	český
museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
W	W	kA	W
kommissj	kommissj	k1gInSc1	kommissj
u	u	k7c2	u
Kronbergra	Kronbergr	k1gInSc2	Kronbergr
i	i	k8xC	i
Řiwnáče	Řiwnáč	k1gInSc2	Řiwnáč
<g/>
,	,	kIx,	,
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
Josefa	Josef	k1gMnSc2	Josef
Jungmanna	Jungmanna	k1gFnSc1	Jungmanna
Slowesnost	Slowesnost	k1gFnSc1	Slowesnost
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
<g/>
,	,	kIx,	,
Náuka	Náuka	k1gFnSc1	Náuka
o	o	k7c4	o
wýmluwnosti	wýmluwnost	k1gFnPc4	wýmluwnost
prosaické	prosaický	k2eAgFnPc4d1	prosaická
<g/>
,	,	kIx,	,
básnické	básnický	k2eAgFnPc4d1	básnická
i	i	k8xC	i
řečnické	řečnický	k2eAgFnPc4d1	řečnická
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sbírkau	sbírkau	k6eAd1	sbírkau
příkladů	příklad	k1gInPc2	příklad
newázané	wázaný	k2eNgFnSc2d1	wázaný
i	i	k8xC	i
wázané	wázaný	k2eAgFnSc2d1	wázaný
<g />
.	.	kIx.	.
</s>
<s>
řeči	řeč	k1gFnPc1	řeč
<g/>
,	,	kIx,	,
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
České	český	k2eAgNnSc1d1	české
museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
W	W	kA	W
kommissí	kommissí	k1gNnSc1	kommissí
u	u	k7c2	u
Kronbergra	Kronbergr	k1gInSc2	Kronbergr
a	a	k8xC	a
Řiwnáče	Řiwnáč	k1gInSc2	Řiwnáč
<g/>
,	,	kIx,	,
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
Josefa	Josef	k1gMnSc2	Josef
Jungmanna	Jungmanen	k2eAgFnSc1d1	Jungmanen
Historie	historie	k1gFnSc1	historie
literatury	literatura	k1gFnSc2	literatura
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
<g/>
,	,	kIx,	,
Saustawný	Saustawný	k2eAgInSc1d1	Saustawný
přehled	přehled	k1gInSc1	přehled
spisů	spis	k1gInPc2	spis
českých	český	k2eAgInPc2d1	český
s	s	k7c7	s
krátkau	krátkau	k6eAd1	krátkau
historií	historie	k1gFnSc7	historie
národu	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
oswícení	oswícení	k1gNnSc2	oswícení
a	a	k8xC	a
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Nákladem	náklad	k1gInSc7	náklad
Českého	český	k2eAgInSc2d1	český
museum	museum	k1gNnSc1	museum
<g/>
,	,	kIx,	,
W	W	kA	W
komissí	komissit	k5eAaPmIp3nS	komissit
kněhkupectví	kněhkupectví	k1gNnSc4	kněhkupectví
F.	F.	kA	F.
Řivnáče	řivnáč	k1gMnSc2	řivnáč
<g/>
,	,	kIx,	,
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
Neues	Neues	k1gMnSc1	Neues
Taschenwörterbuch	Taschenwörterbuch	k1gMnSc1	Taschenwörterbuch
der	drát	k5eAaImRp2nS	drát
böhmischen	böhmischen	k1gInSc1	böhmischen
und	und	k?	und
deutschen	deutschen	k2eAgInSc1d1	deutschen
Sprache	Sprache	k1gInSc1	Sprache
<g/>
.	.	kIx.	.
</s>
<s>
Böhmischdeutscher	Böhmischdeutschra	k1gFnPc2	Böhmischdeutschra
Theil	Theila	k1gFnPc2	Theila
(	(	kIx(	(
<g/>
nach	nach	k1gInSc1	nach
Jungmann	Jungmann	k1gInSc1	Jungmann
<g/>
,	,	kIx,	,
Šumavský	šumavský	k2eAgInSc1d1	šumavský
und	und	k?	und
Anderen	Anderna	k1gFnPc2	Anderna
von	von	k1gInSc1	von
Josef	Josef	k1gMnSc1	Josef
Rank	rank	k1gInSc1	rank
<g/>
;	;	kIx,	;
Nový	nový	k2eAgInSc1d1	nový
slovník	slovník	k1gInSc1	slovník
kapesní	kapesní	k2eAgInSc1d1	kapesní
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
i	i	k8xC	i
německého	německý	k2eAgInSc2d1	německý
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
českoněmecký	českoněmecký	k2eAgInSc1d1	českoněmecký
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
Jungmanna	Jungmann	k1gInSc2	Jungmann
<g/>
,	,	kIx,	,
<g/>
Šumavského	šumavský	k2eAgInSc2d1	šumavský
a	a	k8xC	a
mn	mn	k?	mn
<g/>
.	.	kIx.	.
jiných	jiný	k1gMnPc2	jiný
sestavil	sestavit	k5eAaPmAgMnS	sestavit
Josef	Josef	k1gMnSc1	Josef
Rank	rank	k1gInSc4	rank
<g/>
,	,	kIx,	,
Prag	Prag	k1gInSc1	Prag
<g/>
,	,	kIx,	,
Druck	Druck	k1gInSc1	Druck
und	und	k?	und
Papier	Papier	k1gInSc1	Papier
von	von	k1gInSc1	von
Gottlieb	Gottliba	k1gFnPc2	Gottliba
Haase	Haas	k1gMnSc5	Haas
Söhne	Söhn	k1gMnSc5	Söhn
<g/>
,	,	kIx,	,
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
Nový	nový	k2eAgInSc1d1	nový
slovník	slovník	k1gInSc1	slovník
kapesní	kapesní	k2eAgInSc1d1	kapesní
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
i	i	k8xC	i
německého	německý	k2eAgInSc2d1	německý
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
německočeský	německočeský	k2eAgInSc1d1	německočeský
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
Šumavského	šumavský	k2eAgInSc2d1	šumavský
<g/>
,	,	kIx,	,
Jungmanna	Jungmann	k1gInSc2	Jungmann
<g/>
,	,	kIx,	,
Weniga	Weniga	k1gFnSc1	Weniga
a	a	k8xC	a
mn	mn	k?	mn
<g/>
.	.	kIx.	.
jiných	jiný	k1gMnPc2	jiný
sestavil	sestavit	k5eAaPmAgMnS	sestavit
Josef	Josef	k1gMnSc1	Josef
Rank	rank	k1gInSc1	rank
<g/>
;	;	kIx,	;
Neues	Neues	k1gMnSc1	Neues
Taschenwörterbuch	Taschenwörterbuch	k1gMnSc1	Taschenwörterbuch
der	drát	k5eAaImRp2nS	drát
böhmischen	böhmischen	k1gInSc1	böhmischen
und	und	k?	und
deutschen	deutschen	k2eAgInSc1d1	deutschen
Sprache	Sprache	k1gInSc1	Sprache
<g/>
.	.	kIx.	.
</s>
<s>
Deutschböhmischer	Deutschböhmischra	k1gFnPc2	Deutschböhmischra
Theil	Theila	k1gFnPc2	Theila
<g/>
,	,	kIx,	,
nach	nach	k1gInSc1	nach
Šumavský	šumavský	k2eAgInSc1d1	šumavský
<g/>
,	,	kIx,	,
Jungmann	Jungmann	k1gInSc1	Jungmann
<g/>
,	,	kIx,	,
Wenig	Wenig	k1gInSc1	Wenig
und	und	k?	und
Anderen	Anderna	k1gFnPc2	Anderna
von	von	k1gInSc1	von
Josef	Josef	k1gMnSc1	Josef
Rank	rank	k1gInSc1	rank
<g/>
,	,	kIx,	,
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
C.k.	C.k.	k1gFnSc1	C.k.
dvorní	dvorní	k2eAgFnSc1d1	dvorní
knihtiskárna	knihtiskárna	k1gFnSc1	knihtiskárna
synův	synův	k2eAgMnSc1d1	synův
Bohumila	Bohumil	k1gMnSc4	Bohumil
Haase	Haase	k1gFnSc2	Haase
<g/>
,	,	kIx,	,
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
Dvojí	dvojit	k5eAaImIp3nP	dvojit
rozmlouvání	rozmlouvání	k1gNnSc4	rozmlouvání
o	o	k7c6	o
jazyce	jazyk	k1gInSc6	jazyk
českém	český	k2eAgInSc6d1	český
(	(	kIx(	(
<g/>
1806	[number]	k4	1806
<g/>
)	)	kIx)	)
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
statě	stať	k1gFnPc4	stať
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Hlasatel	hlasatel	k1gMnSc1	hlasatel
český	český	k2eAgMnSc1d1	český
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
jakýsi	jakýsi	k3yIgInSc1	jakýsi
kulturní	kulturní	k2eAgInSc1d1	kulturní
program	program	k1gInSc1	program
své	svůj	k3xOyFgFnSc2	svůj
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
spolu	spolu	k6eAd1	spolu
rozmlouvá	rozmlouvat	k5eAaImIp3nS	rozmlouvat
v	v	k7c6	v
podsvětí	podsvětí	k1gNnSc6	podsvětí
současný	současný	k2eAgMnSc1d1	současný
Čech	Čech	k1gMnSc1	Čech
a	a	k8xC	a
Němec	Němec	k1gMnSc1	Němec
s	s	k7c7	s
Čechem	Čech	k1gMnSc7	Čech
veleslavínské	veleslavínský	k2eAgFnSc2d1	Veleslavínská
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jungmann	Jungmann	k1gMnSc1	Jungmann
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
kvalitu	kvalita	k1gFnSc4	kvalita
veleslavínské	veleslavínský	k2eAgFnSc2d1	Veleslavínská
češtiny	čeština	k1gFnSc2	čeština
a	a	k8xC	a
ostře	ostro	k6eAd1	ostro
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
češtinu	čeština	k1gFnSc4	čeština
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Čech	Čechy	k1gFnPc2	Čechy
mluví	mluvit	k5eAaImIp3nS	mluvit
špatně	špatně	k6eAd1	špatně
a	a	k8xC	a
nesrozumitelně	srozumitelně	k6eNd1	srozumitelně
<g/>
,	,	kIx,	,
poloněmecky	poloněmecky	k6eAd1	poloněmecky
<g/>
.	.	kIx.	.
</s>
<s>
Němec	Němec	k1gMnSc1	Němec
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
Čechy	Čechy	k1gFnPc4	Čechy
za	za	k7c4	za
napodobování	napodobování	k1gNnSc4	napodobování
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
dialog	dialog	k1gInSc4	dialog
stoupenců	stoupenec	k1gMnPc2	stoupenec
a	a	k8xC	a
odpůrců	odpůrce	k1gMnPc2	odpůrce
národních	národní	k2eAgFnPc2d1	národní
snah	snaha	k1gFnPc2	snaha
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Čech	Čech	k1gMnSc1	Čech
není	být	k5eNaImIp3nS	být
každý	každý	k3xTgMnSc1	každý
obyvatel	obyvatel	k1gMnSc1	obyvatel
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
mluví	mluvit	k5eAaImIp3nS	mluvit
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
a	a	k8xC	a
Božena	Božena	k1gFnSc1	Božena
-	-	kIx~	-
historická	historický	k2eAgFnSc1d1	historická
romance	romance	k1gFnSc1	romance
Zápisky	zápiska	k1gFnSc2	zápiska
(	(	kIx(	(
<g/>
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
připravil	připravit	k5eAaPmAgMnS	připravit
<g/>
,	,	kIx,	,
doslov	doslov	k1gInSc4	doslov
<g/>
,	,	kIx,	,
ediční	ediční	k2eAgFnSc4d1	ediční
poznámku	poznámka	k1gFnSc4	poznámka
a	a	k8xC	a
vysvětlivky	vysvětlivka	k1gFnPc4	vysvětlivka
napsal	napsat	k5eAaBmAgMnS	napsat
Rudolf	Rudolf	k1gMnSc1	Rudolf
Havel	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Slovník	slovník	k1gInSc1	slovník
česko-německý	českoěmecký	k2eAgInSc1d1	česko-německý
<g/>
,	,	kIx,	,
Díly	díl	k1gInPc1	díl
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Zápisky	zápiska	k1gFnSc2	zápiska
(	(	kIx(	(
<g/>
edičně	edičně	k6eAd1	edičně
připravil	připravit	k5eAaPmAgMnS	připravit
<g/>
,	,	kIx,	,
vydavatelskou	vydavatelský	k2eAgFnSc4d1	vydavatelská
poznámku	poznámka	k1gFnSc4	poznámka
napsal	napsat	k5eAaBmAgMnS	napsat
a	a	k8xC	a
vysvětlivky	vysvětlivka	k1gFnPc4	vysvětlivka
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
Radek	Radek	k1gMnSc1	Radek
Lunga	Lung	k1gMnSc2	Lung
<g/>
;	;	kIx,	;
předmluva	předmluva	k1gFnSc1	předmluva
Eva	Eva	k1gFnSc1	Eva
Ryšavá	Ryšavá	k1gFnSc1	Ryšavá
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Budka	budka	k1gFnSc1	budka
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Pomník	pomník	k1gInSc1	pomník
sedícího	sedící	k2eAgMnSc2d1	sedící
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
bronz	bronz	k1gInSc1	bronz
<g/>
,	,	kIx,	,
podstavec	podstavec	k1gInSc1	podstavec
Antonín	Antonín	k1gMnSc1	Antonín
Barvitius	Barvitius	k1gMnSc1	Barvitius
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1873	[number]	k4	1873
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
pomník	pomník	k1gInSc4	pomník
sedícího	sedící	k2eAgMnSc2d1	sedící
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
zlacená	zlacený	k2eAgFnSc1d1	zlacená
sádra	sádra	k1gFnSc1	sádra
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
kolem	kolem	k7c2	kolem
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
pomník	pomník	k1gInSc4	pomník
sedícího	sedící	k2eAgMnSc2d1	sedící
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
Šimek	Šimek	k1gMnSc1	Šimek
(	(	kIx(	(
<g/>
varianta	varianta	k1gFnSc1	varianta
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patinovaná	patinovaný	k2eAgFnSc1d1	patinovaná
sádra	sádra	k1gFnSc1	sádra
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
kolem	kolem	k7c2	kolem
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
pomník	pomník	k1gInSc4	pomník
stojícího	stojící	k2eAgMnSc2d1	stojící
spisovatele	spisovatel	k1gMnSc2	spisovatel
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Schnirch	Schnirch	k1gMnSc1	Schnirch
<g/>
,	,	kIx,	,
miniatura	miniatura	k1gFnSc1	miniatura
<g/>
,	,	kIx,	,
patinovaná	patinovaný	k2eAgFnSc1d1	patinovaná
sádra	sádra	k1gFnSc1	sádra
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
;	;	kIx,	;
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
Poprsí	poprsí	k1gNnSc1	poprsí
-	-	kIx~	-
portrét	portrét	k1gInSc1	portrét
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
pro	pro	k7c4	pro
Panteon	panteon	k1gInSc4	panteon
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g />
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
gotické	gotický	k2eAgFnSc2d1	gotická
věže	věž	k1gFnSc2	věž
s	s	k7c7	s
poprsím	poprsí	k1gNnSc7	poprsí
J.J.	J.J.	k1gFnSc2	J.J.
v	v	k7c6	v
životní	životní	k2eAgFnSc6d1	životní
velikosti	velikost	k1gFnSc6	velikost
před	před	k7c7	před
rodným	rodný	k2eAgInSc7d1	rodný
domem	dům	k1gInSc7	dům
v	v	k7c6	v
Hudlicích	Hudlice	k1gFnPc6	Hudlice
<g/>
,	,	kIx,	,
železná	železný	k2eAgFnSc1d1	železná
litina	litina	k1gFnSc1	litina
Pomník	pomník	k1gInSc4	pomník
v	v	k7c6	v
Olovnici	olovnice	k1gFnSc6	olovnice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
místní	místní	k2eAgMnSc1d1	místní
kameník	kameník	k1gMnSc1	kameník
František	František	k1gMnSc1	František
Kdýr	Kdýr	k1gMnSc1	Kdýr
Pomník	pomník	k1gInSc4	pomník
v	v	k7c6	v
Jungmannových	Jungmannův	k2eAgInPc6d1	Jungmannův
sadech	sad	k1gInPc6	sad
ve	v	k7c6	v
Vysokém	vysoký	k2eAgNnSc6d1	vysoké
Mýtě	mýto	k1gNnSc6	mýto
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
Antonín	Antonín	k1gMnSc1	Antonín
Machek	Machek	k1gMnSc1	Machek
<g/>
:	:	kIx,	:
Polopostava	Polopostava	k1gFnSc1	Polopostava
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
olejomalba	olejomalba	k1gFnSc1	olejomalba
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
;	;	kIx,	;
Národní	národní	k2eAgFnSc1d1	národní
muzeum	muzeum	k1gNnSc4	muzeum
Anonym	anonym	k1gInSc1	anonym
podle	podle	k7c2	podle
Antonína	Antonín	k1gMnSc2	Antonín
Machka	Machek	k1gMnSc2	Machek
<g/>
:	:	kIx,	:
Polopostava	Polopostava	k1gFnSc1	Polopostava
Skica	skica	k1gFnSc1	skica
k	k	k7c3	k
portrétu	portrét	k1gInSc3	portrét
<g/>
,	,	kIx,	,
kresba	kresba	k1gFnSc1	kresba
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
Jubilejní	jubilejní	k2eAgInSc1d1	jubilejní
portrét	portrét	k1gInSc1	portrét
<g/>
,	,	kIx,	,
litografie	litografie	k1gFnSc1	litografie
Medaile	medaile	k1gFnSc1	medaile
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
pomníku	pomník	k1gInSc2	pomník
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Medaile	medaile	k1gFnSc2	medaile
ke	k	k7c3	k
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Kříž	Kříž	k1gMnSc1	Kříž
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1873	[number]	k4	1873
<g />
.	.	kIx.	.
</s>
<s>
Medaile	medaile	k1gFnSc1	medaile
ke	k	k7c3	k
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Kvasnička	kvasnička	k1gFnSc1	kvasnička
Medaile	medaile	k1gFnSc1	medaile
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
pomníku	pomník	k1gInSc2	pomník
ve	v	k7c6	v
Volovicích	volovice	k1gFnPc6	volovice
<g/>
,	,	kIx,	,
1873	[number]	k4	1873
Medailon	medailon	k1gInSc1	medailon
pamětní	pamětní	k2eAgFnSc2d1	pamětní
Národopisné	národopisný	k2eAgFnSc2d1	národopisná
výstavy	výstava	k1gFnSc2	výstava
českoslovanské	českoslovanský	k2eAgFnSc2d1	českoslovanská
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Hořovice	Hořovice	k1gFnPc1	Hořovice
1895	[number]	k4	1895
Medaile	medaile	k1gFnSc1	medaile
ke	k	k7c3	k
125	[number]	k4	125
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
narození	narození	k1gNnSc2	narození
v	v	k7c6	v
Hudlicích	Hudlice	k1gFnPc6	Hudlice
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Šmakal	šmakat	k5eAaImAgInS	šmakat
1899	[number]	k4	1899
Medaile	medaile	k1gFnSc1	medaile
ke	k	k7c3	k
sjezdu	sjezd	k1gInSc3	sjezd
Spolku	spolek	k1gInSc2	spolek
českých	český	k2eAgFnPc2d1	Česká
<g />
.	.	kIx.	.
</s>
<s>
vysloužilců	vysloužilec	k1gMnPc2	vysloužilec
a	a	k8xC	a
k	k	k7c3	k
oslavám	oslava	k1gFnPc3	oslava
125	[number]	k4	125
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
narození	narození	k1gNnSc2	narození
v	v	k7c6	v
Hudlicích	Hudlice	k1gFnPc6	Hudlice
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Šmakal	šmakat	k5eAaImAgInS	šmakat
1899	[number]	k4	1899
Medaile	medaile	k1gFnSc1	medaile
ke	k	k7c3	k
125	[number]	k4	125
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
narození	narození	k1gNnSc2	narození
v	v	k7c6	v
Hudlicích	Hudlice	k1gFnPc6	Hudlice
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Šmakal	šmakat	k5eAaImAgInS	šmakat
1899	[number]	k4	1899
Medaile	medaile	k1gFnSc1	medaile
ke	k	k7c3	k
200	[number]	k4	200
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
narození	narození	k1gNnSc2	narození
Plaketa	plaketa	k1gFnSc1	plaketa
pamětní	pamětní	k2eAgFnSc1d1	pamětní
jednostranná	jednostranný	k2eAgFnSc1d1	jednostranná
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1910	[number]	k4	1910
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
padesátikorunová	padesátikorunový	k2eAgFnSc1d1	padesátikorunová
mince	mince	k1gFnSc1	mince
k	k	k7c3	k
200	[number]	k4	200
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Jméno	jméno	k1gNnSc4	jméno
Josefa	Josef	k1gMnSc2	Josef
Jungmanna	Jungmann	k1gMnSc2	Jungmann
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
pod	pod	k7c7	pod
okny	okno	k1gNnPc7	okno
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
dalšími	další	k1gNnPc7	další
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Dvaasedmdesát	dvaasedmdesát	k4xCc4	dvaasedmdesát
jmen	jméno	k1gNnPc2	jméno
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalých	bývalý	k2eAgFnPc6d1	bývalá
lázních	lázeň	k1gFnPc6	lázeň
Šternberk	Šternberk	k1gInSc4	Šternberk
na	na	k7c4	na
Kladensku	Kladensko	k1gNnSc6	Kladensko
Jungmann	Jungmann	k1gMnSc1	Jungmann
napsal	napsat	k5eAaPmAgMnS	napsat
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
posledních	poslední	k2eAgNnPc2d1	poslední
děl	dělo	k1gNnPc2	dělo
Zápisky	zápiska	k1gFnSc2	zápiska
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
mimo	mimo	k6eAd1	mimo
jiné	jiná	k1gFnSc2	jiná
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
německou	německý	k2eAgFnSc4d1	německá
mluvu	mluva	k1gFnSc4	mluva
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
