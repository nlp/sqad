<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
[	[	kIx(	[
<g/>
fɛ	fɛ	k?	fɛ
<g/>
̃	̃	k?	̃
də	də	k?	də
sosyʁ	sosyʁ	k?	sosyʁ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
Ženeva	Ženeva	k1gFnSc1	Ženeva
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Vufflens-le-Château	Vufflense-Châteaa	k1gFnSc4	Vufflens-le-Châteaa
u	u	k7c2	u
Morges	Morgesa	k1gFnPc2	Morgesa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
strukturalistické	strukturalistický	k2eAgFnSc2d1	strukturalistická
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
vývoj	vývoj	k1gInSc1	vývoj
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
sémiotiky	sémiotika	k1gFnSc2	sémiotika
<g/>
,	,	kIx,	,
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc2d1	literární
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
estetiky	estetika	k1gFnSc2	estetika
<g/>
.	.	kIx.	.
</s>
<s>
Navázali	navázat	k5eAaPmAgMnP	navázat
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
autoři	autor	k1gMnPc1	autor
jako	jako	k8xC	jako
například	například	k6eAd1	například
Claude	Claud	k1gInSc5	Claud
Lévi-Strauss	Lévi-Strauss	k1gInSc1	Lévi-Strauss
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
Sapir	Sapir	k1gMnSc1	Sapir
nebo	nebo	k8xC	nebo
Noam	Noam	k1gMnSc1	Noam
Chomsky	Chomsky	k1gMnSc1	Chomsky
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Mongin	Mongin	k1gMnSc1	Mongin
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
již	již	k6eAd1	již
od	od	k7c2	od
malička	maličko	k1gNnSc2	maličko
vykazoval	vykazovat	k5eAaImAgMnS	vykazovat
známky	známka	k1gFnPc4	známka
nesmírného	smírný	k2eNgInSc2d1	nesmírný
talentu	talent	k1gInSc2	talent
a	a	k8xC	a
intelektuálních	intelektuální	k2eAgFnPc2d1	intelektuální
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
studia	studio	k1gNnSc2	studio
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
,	,	kIx,	,
sanskrtu	sanskrt	k1gInSc2	sanskrt
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgInPc2d1	další
kurzů	kurz	k1gInPc2	kurz
na	na	k7c6	na
Ženevské	ženevský	k2eAgFnSc6d1	Ženevská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
Lipské	lipský	k2eAgFnSc6d1	Lipská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
studoval	studovat	k5eAaImAgMnS	studovat
Saussure	Saussur	k1gMnSc5	Saussur
rok	rok	k1gInSc4	rok
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napsal	napsat	k5eAaPmAgMnS	napsat
svou	svůj	k3xOyFgFnSc4	svůj
jedinou	jediný	k2eAgFnSc4d1	jediná
plnohodnotnou	plnohodnotný	k2eAgFnSc4d1	plnohodnotná
práci	práce	k1gFnSc4	práce
Mémoire	Mémoir	k1gInSc5	Mémoir
sur	sur	k?	sur
le	le	k?	le
systè	systè	k?	systè
primitif	primitif	k1gInSc1	primitif
des	des	k1gNnSc1	des
voyelles	voyellesa	k1gFnPc2	voyellesa
dans	dansa	k1gFnPc2	dansa
les	les	k1gInSc1	les
langues	langues	k1gMnSc1	langues
indo-européenes	indouropéenes	k1gInSc1	indo-européenes
(	(	kIx(	(
<g/>
Pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c6	o
primitivním	primitivní	k2eAgInSc6d1	primitivní
vokalickém	vokalický	k2eAgInSc6d1	vokalický
systému	systém	k1gInSc6	systém
v	v	k7c6	v
indoevropských	indoevropský	k2eAgInPc6d1	indoevropský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Saussure	Saussur	k1gMnSc5	Saussur
přemístil	přemístit	k5eAaPmAgInS	přemístit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přednášel	přednášet	k5eAaImAgInS	přednášet
starobylé	starobylý	k2eAgFnPc4d1	starobylá
a	a	k8xC	a
moderní	moderní	k2eAgInPc1d1	moderní
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
působil	působit	k5eAaImAgInS	působit
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Ženevy	Ženeva	k1gFnSc2	Ženeva
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
přednášel	přednášet	k5eAaImAgMnS	přednášet
sanskrt	sanskrt	k1gInSc4	sanskrt
a	a	k8xC	a
indoevropštinu	indoevropština	k1gFnSc4	indoevropština
na	na	k7c6	na
Ženevské	ženevský	k2eAgFnSc6d1	Ženevská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
začal	začít	k5eAaPmAgInS	začít
Saussure	Saussur	k1gMnSc5	Saussur
vyučovat	vyučovat	k5eAaImF	vyučovat
Kurz	kurz	k1gInSc4	kurz
obecné	obecný	k2eAgFnSc2d1	obecná
lingvistiky	lingvistika	k1gFnSc2	lingvistika
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kurs	kurs	k1gInSc1	kurs
obecné	obecná	k1gFnSc2	obecná
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Saussurovo	Saussurův	k2eAgNnSc1d1	Saussurovo
nejvlivnější	vlivný	k2eAgNnSc1d3	nejvlivnější
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
Kurs	kurs	k1gInSc1	kurs
obecné	obecný	k2eAgFnSc2d1	obecná
lingvistiky	lingvistika	k1gFnSc2	lingvistika
(	(	kIx(	(
<g/>
Course	Course	k1gFnSc1	Course
de	de	k?	de
linguistique	linguistiquat	k5eAaPmIp3nS	linguistiquat
générale	générale	k6eAd1	générale
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
1989	[number]	k4	1989
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Františka	František	k1gMnSc2	František
Čermáka	Čermák	k1gMnSc2	Čermák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
Učinili	učinit	k5eAaPmAgMnP	učinit
tak	tak	k6eAd1	tak
jeho	jeho	k3xOp3gMnPc1	jeho
bývalí	bývalý	k2eAgMnPc1d1	bývalý
žáci	žák	k1gMnPc1	žák
Charles	Charles	k1gMnSc1	Charles
Bally	Balla	k1gFnSc2	Balla
a	a	k8xC	a
Albert	Albert	k1gMnSc1	Albert
Sechehaye	Sechehay	k1gFnSc2	Sechehay
na	na	k7c6	na
základě	základ	k1gInSc6	základ
studentských	studentský	k2eAgFnPc2d1	studentská
poznámek	poznámka	k1gFnPc2	poznámka
ze	z	k7c2	z
Saussurových	Saussurův	k2eAgFnPc2d1	Saussurova
přednášek	přednáška	k1gFnPc2	přednáška
na	na	k7c6	na
Ženevské	ženevský	k2eAgFnSc6d1	Ženevská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Kurs	kurs	k1gInSc1	kurs
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
kanonických	kanonický	k2eAgNnPc2d1	kanonické
děl	dělo	k1gNnPc2	dělo
lingvistiky	lingvistika	k1gFnSc2	lingvistika
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nestalo	stát	k5eNaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
primárně	primárně	k6eAd1	primárně
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
obsahu	obsah	k1gInSc3	obsah
(	(	kIx(	(
<g/>
mnoho	mnoho	k4c1	mnoho
myšlenek	myšlenka	k1gFnPc2	myšlenka
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
předvídáno	předvídat	k5eAaImNgNnS	předvídat
jinými	jiný	k2eAgInPc7d1	jiný
jazykovědci	jazykovědec	k1gMnSc3	jazykovědec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
inovativní	inovativní	k2eAgInSc4d1	inovativní
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Saussure	Saussur	k1gMnSc5	Saussur
při	při	k7c6	při
pojednání	pojednání	k1gNnSc6	pojednání
lingvistických	lingvistický	k2eAgInPc2d1	lingvistický
jevů	jev	k1gInPc2	jev
použil	použít	k5eAaPmAgInS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc7d1	ústřední
myšlenkou	myšlenka	k1gFnSc7	myšlenka
Kursu	kurs	k1gInSc2	kurs
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
analyzovat	analyzovat	k5eAaImF	analyzovat
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xC	jako
formální	formální	k2eAgInSc4d1	formální
systém	systém	k1gInSc4	systém
různých	různý	k2eAgInPc2d1	různý
prvků	prvek	k1gInPc2	prvek
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
chaotickou	chaotický	k2eAgFnSc4d1	chaotická
dialektiku	dialektika	k1gFnSc4	dialektika
okamžité	okamžitý	k2eAgFnSc2d1	okamžitá
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
porozumění	porozumění	k1gNnSc2	porozumění
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
příklady	příklad	k1gInPc7	příklad
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
patří	patřit	k5eAaImIp3nS	patřit
jazykový	jazykový	k2eAgInSc1d1	jazykový
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
označující	označující	k2eAgFnPc1d1	označující
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
<g/>
,	,	kIx,	,
referent	referent	k1gMnSc1	referent
<g/>
.	.	kIx.	.
</s>
<s>
Saussure	Saussur	k1gMnSc5	Saussur
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
jako	jako	k9	jako
student	student	k1gMnSc1	student
<g/>
,	,	kIx,	,
publikoval	publikovat	k5eAaBmAgMnS	publikovat
důležitou	důležitý	k2eAgFnSc4d1	důležitá
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
indoevropské	indoevropský	k2eAgFnSc6d1	indoevropská
filologii	filologie	k1gFnSc6	filologie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
předložil	předložit	k5eAaPmAgMnS	předložit
teorii	teorie	k1gFnSc4	teorie
sonantních	sonantní	k2eAgInPc2d1	sonantní
koeficientů	koeficient	k1gInPc2	koeficient
v	v	k7c6	v
praindoevropštině	praindoevropština	k1gFnSc6	praindoevropština
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
blíže	blízce	k6eAd2	blízce
definoval	definovat	k5eAaBmAgInS	definovat
jejich	jejich	k3xOp3gFnSc4	jejich
fonetickou	fonetický	k2eAgFnSc4d1	fonetická
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Skandinávský	skandinávský	k2eAgMnSc1d1	skandinávský
vědec	vědec	k1gMnSc1	vědec
Hermann	Hermann	k1gMnSc1	Hermann
Möller	Möller	k1gMnSc1	Möller
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
laryngální	laryngální	k2eAgInPc4d1	laryngální
konsonanty	konsonant	k1gInPc4	konsonant
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
laryngální	laryngální	k2eAgFnSc2d1	laryngální
teorie	teorie	k1gFnSc2	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
zato	zato	k6eAd1	zato
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
ty	ten	k3xDgInPc4	ten
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
Saussure	Saussur	k1gMnSc5	Saussur
čelil	čelit	k5eAaImAgInS	čelit
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dokázal	dokázat	k5eAaPmAgInS	dokázat
formulovat	formulovat	k5eAaImF	formulovat
systematické	systematický	k2eAgFnPc4d1	systematická
a	a	k8xC	a
prediktivní	prediktivní	k2eAgFnPc4d1	prediktivní
hypotézy	hypotéza	k1gFnPc4	hypotéza
o	o	k7c6	o
neznámých	známý	k2eNgNnPc6d1	neznámé
lingvistických	lingvistický	k2eAgNnPc6d1	lingvistické
datech	datum	k1gNnPc6	datum
na	na	k7c6	na
základě	základ	k1gInSc6	základ
těch	ten	k3xDgFnPc2	ten
známých	známá	k1gFnPc2	známá
<g/>
,	,	kIx,	,
podnítily	podnítit	k5eAaPmAgFnP	podnítit
rozvoj	rozvoj	k1gInSc4	rozvoj
strukturalismu	strukturalismus	k1gInSc2	strukturalismus
<g/>
.	.	kIx.	.
</s>
<s>
Saussurovy	Saussurův	k2eAgInPc1d1	Saussurův
předpoklady	předpoklad	k1gInPc1	předpoklad
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
prapůvodních	prapůvodní	k2eAgInPc2d1	prapůvodní
koeficientů	koeficient	k1gInPc2	koeficient
<g/>
/	/	kIx~	/
<g/>
laryngál	laryngála	k1gFnPc2	laryngála
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
vývoji	vývoj	k1gInSc6	vývoj
se	se	k3xPyFc4	se
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
jako	jako	k9	jako
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
o	o	k7c4	o
padesát	padesát	k4xCc4	padesát
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
nálezu	nález	k1gInSc6	nález
a	a	k8xC	a
rozluštění	rozluštění	k1gNnSc6	rozluštění
chetitštiny	chetitština	k1gFnSc2	chetitština
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
</s>
