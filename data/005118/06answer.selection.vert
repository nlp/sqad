<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
de	de	k?	de
Saussure	Saussur	k1gMnSc5	Saussur
[	[	kIx(	[
<g/>
fɛ	fɛ	k?	fɛ
<g/>
̃	̃	k?	̃
də	də	k?	də
sosyʁ	sosyʁ	k?	sosyʁ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1857	[number]	k4	1857
<g/>
,	,	kIx,	,
Ženeva	Ženeva	k1gFnSc1	Ženeva
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Vufflens-le-Château	Vufflense-Châteaa	k1gFnSc4	Vufflens-le-Châteaa
u	u	k7c2	u
Morges	Morgesa	k1gFnPc2	Morgesa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
strukturalistické	strukturalistický	k2eAgFnSc2d1	strukturalistická
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
.	.	kIx.	.
</s>
