<s>
Albert	Albert	k1gMnSc1	Albert
Camus	Camus	k1gMnSc1	Camus
[	[	kIx(	[
<g/>
alˈ	alˈ	k?	alˈ
kaˈ	kaˈ	k?	kaˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
[	[	kIx(	[
<g/>
albér	albér	k1gMnSc1	albér
kami	kam	k1gFnSc2	kam
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1913	[number]	k4	1913
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
čelných	čelný	k2eAgMnPc2d1	čelný
představitelů	představitel	k1gMnPc2	představitel
existencialismu	existencialismus	k1gInSc2	existencialismus
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
taková	takový	k3xDgNnPc1	takový
označení	označení	k1gNnPc4	označení
odmítal	odmítat	k5eAaImAgInS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
nepřál	přát	k5eNaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
filosofa	filosof	k1gMnSc4	filosof
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
pozdější	pozdní	k2eAgNnSc4d2	pozdější
filosofické	filosofický	k2eAgNnSc4d1	filosofické
myšlení	myšlení	k1gNnSc4	myšlení
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
francouzsko-španělského	francouzsko-španělský	k2eAgNnSc2d1	francouzsko-španělské
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Mondovi	Mond	k1gMnSc6	Mond
(	(	kIx(	(
<g/>
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
tehdy	tehdy	k6eAd1	tehdy
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
na	na	k7c4	na
Marně	marně	k6eAd1	marně
vyrůstal	vyrůstat	k5eAaImAgInS	vyrůstat
v	v	k7c6	v
chudinské	chudinský	k2eAgFnSc6d1	chudinská
čtvrti	čtvrt	k1gFnSc6	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
Camus	Camus	k1gMnSc1	Camus
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
filosofii	filosofie	k1gFnSc4	filosofie
a	a	k8xC	a
klasické	klasický	k2eAgFnSc2d1	klasická
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
pro	pro	k7c4	pro
názorové	názorový	k2eAgFnPc4d1	názorová
neshody	neshoda	k1gFnPc4	neshoda
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
až	až	k8xS	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
několik	několik	k4yIc1	několik
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaPmF	věnovat
literatuře	literatura	k1gFnSc6	literatura
-	-	kIx~	-
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
novinářem	novinář	k1gMnSc7	novinář
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
novinářská	novinářský	k2eAgFnSc1d1	novinářská
činnost	činnost	k1gFnSc1	činnost
vedla	vést	k5eAaImAgFnS	vést
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vyhoštění	vyhoštění	k1gNnSc3	vyhoštění
z	z	k7c2	z
Alžíru	Alžír	k1gInSc2	Alžír
<g/>
,	,	kIx,	,
Camus	Camus	k1gMnSc1	Camus
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
protifašistického	protifašistický	k2eAgNnSc2d1	protifašistické
hnutí	hnutí	k1gNnSc2	hnutí
(	(	kIx(	(
<g/>
hnutí	hnutí	k1gNnSc2	hnutí
odporu	odpor	k1gInSc2	odpor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
především	především	k9	především
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
ilegálním	ilegální	k2eAgInSc6d1	ilegální
časopise	časopis	k1gInSc6	časopis
Combat	Combat	k1gFnSc2	Combat
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgFnSc1d1	aktivní
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
francouzské	francouzský	k2eAgFnSc2d1	francouzská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
politiky	politika	k1gFnSc2	politika
v	v	k7c6	v
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jejím	její	k3xOp3gMnSc7	její
nesmiřitelným	smiřitelný	k2eNgMnSc7d1	nesmiřitelný
kritikem	kritik	k1gMnSc7	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
