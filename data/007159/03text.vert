<s>
List	list	k1gInSc1	list
(	(	kIx(	(
<g/>
folium	folium	k1gNnSc1	folium
<g/>
,	,	kIx,	,
fylom	fylom	k1gInSc1	fylom
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
postranní	postranní	k2eAgInSc1d1	postranní
vegetativní	vegetativní	k2eAgInSc1d1	vegetativní
zelený	zelený	k2eAgInSc1d1	zelený
orgán	orgán	k1gInSc1	orgán
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
omezeného	omezený	k2eAgInSc2d1	omezený
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
velikosti	velikost	k1gFnPc4	velikost
od	od	k7c2	od
několika	několik	k4yIc2	několik
milimetrů	milimetr	k1gInPc2	milimetr
až	až	k6eAd1	až
po	po	k7c4	po
2	[number]	k4	2
metry	metr	k1gInPc4	metr
čtvereční	čtvereční	k2eAgMnSc1d1	čtvereční
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
do	do	k7c2	do
plochy	plocha	k1gFnSc2	plocha
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
<g/>
.	.	kIx.	.
</s>
<s>
Plošné	plošný	k2eAgNnSc1d1	plošné
rozšíření	rozšíření	k1gNnSc1	rozšíření
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
evolučně	evolučně	k6eAd1	evolučně
výhodná	výhodný	k2eAgFnSc1d1	výhodná
adaptace	adaptace	k1gFnSc1	adaptace
rostlinného	rostlinný	k2eAgNnSc2d1	rostlinné
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rostlině	rostlina	k1gFnSc3	rostlina
efektivnější	efektivní	k2eAgNnSc1d2	efektivnější
zachytávání	zachytávání	k1gNnSc1	zachytávání
světla	světlo	k1gNnSc2	světlo
listy	lista	k1gFnSc2	lista
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
efektivita	efektivita	k1gFnSc1	efektivita
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ještě	ještě	k6eAd1	ještě
umocněna	umocnit	k5eAaPmNgFnS	umocnit
aktivním	aktivní	k2eAgNnSc7d1	aktivní
nastavením	nastavení	k1gNnSc7	nastavení
listu	list	k1gInSc2	list
vůči	vůči	k7c3	vůči
světelným	světelný	k2eAgInPc3d1	světelný
paprskům	paprsek	k1gInPc3	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc7	list
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
asimilační	asimilační	k2eAgInPc4d1	asimilační
listy	list	k1gInPc4	list
(	(	kIx(	(
<g/>
základní	základní	k2eAgInSc4d1	základní
typ	typ	k1gInSc4	typ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
děložní	děložní	k2eAgInPc4d1	děložní
lístky	lístek	k1gInPc4	lístek
a	a	k8xC	a
listeny	listen	k1gInPc4	listen
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
základní	základní	k2eAgFnPc4d1	základní
funkce	funkce	k1gFnPc4	funkce
–	–	k?	–
výměna	výměna	k1gFnSc1	výměna
plynů	plyn	k1gInPc2	plyn
s	s	k7c7	s
okolním	okolní	k2eAgNnSc7d1	okolní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
a	a	k8xC	a
odpařování	odpařování	k1gNnSc1	odpařování
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
znečištění	znečištění	k1gNnSc4	znečištění
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c4	na
vyšší	vysoký	k2eAgFnPc4d2	vyšší
koncentrace	koncentrace	k1gFnPc4	koncentrace
sloučenin	sloučenina	k1gFnPc2	sloučenina
fluoru	fluor	k1gInSc2	fluor
nebo	nebo	k8xC	nebo
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Morfologie	morfologie	k1gFnSc2	morfologie
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Listová	listový	k2eAgFnSc1d1	listová
čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
nebo	nebo	k8xC	nebo
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
celistvá	celistvý	k2eAgFnSc1d1	celistvá
nebo	nebo	k8xC	nebo
nejrůznějším	různý	k2eAgInSc7d3	nejrůznější
způsobem	způsob	k1gInSc7	způsob
členěná	členěný	k2eAgFnSc1d1	členěná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
členitosti	členitost	k1gFnSc2	členitost
čepele	čepel	k1gInSc2	čepel
je	být	k5eAaImIp3nS	být
celistvý	celistvý	k2eAgInSc4d1	celistvý
list	list	k1gInSc4	list
eliptičný	eliptičný	k2eAgInSc4d1	eliptičný
<g/>
,	,	kIx,	,
kopinatý	kopinatý	k2eAgInSc4d1	kopinatý
<g/>
,	,	kIx,	,
vejčitý	vejčitý	k2eAgInSc4d1	vejčitý
<g/>
,	,	kIx,	,
obvejčitý	obvejčitý	k2eAgInSc4d1	obvejčitý
<g/>
,	,	kIx,	,
okrouhlý	okrouhlý	k2eAgInSc4d1	okrouhlý
<g/>
,	,	kIx,	,
čárkovitý	čárkovitý	k2eAgInSc4d1	čárkovitý
<g/>
,	,	kIx,	,
jehlicovitý	jehlicovitý	k2eAgInSc4d1	jehlicovitý
<g/>
,	,	kIx,	,
kosníkovitý	kosníkovitý	k2eAgInSc4d1	kosníkovitý
<g/>
,	,	kIx,	,
trojúhlý	trojúhlý	k2eAgInSc4d1	trojúhlý
<g/>
,	,	kIx,	,
srdčitý	srdčitý	k2eAgInSc4d1	srdčitý
<g/>
,	,	kIx,	,
ledvinovitý	ledvinovitý	k2eAgInSc4d1	ledvinovitý
<g/>
,	,	kIx,	,
střelovitý	střelovitý	k2eAgInSc4d1	střelovitý
nebo	nebo	k8xC	nebo
hrálovitý	hrálovitý	k2eAgInSc4d1	hrálovitý
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
okraje	okraj	k1gInSc2	okraj
čepele	čepel	k1gInSc2	čepel
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
celistvý	celistvý	k2eAgInSc4d1	celistvý
list	list	k1gInSc4	list
celokrajný	celokrajný	k2eAgInSc4d1	celokrajný
<g/>
,	,	kIx,	,
pilovitý	pilovitý	k2eAgInSc4d1	pilovitý
<g/>
,	,	kIx,	,
vroubkovaný	vroubkovaný	k2eAgInSc4d1	vroubkovaný
<g/>
,	,	kIx,	,
kracovitý	kracovitý	k2eAgInSc4d1	kracovitý
<g/>
,	,	kIx,	,
zubatý	zubatý	k2eAgInSc4d1	zubatý
a	a	k8xC	a
vykrajovaný	vykrajovaný	k2eAgInSc4d1	vykrajovaný
<g/>
.	.	kIx.	.
</s>
<s>
Listová	listový	k2eAgFnSc1d1	listová
čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
členěná	členěný	k2eAgFnSc1d1	členěná
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
a	a	k8xC	a
hloubky	hloubka	k1gFnSc2	hloubka
zářezu	zářez	k1gInSc2	zářez
buď	buď	k8xC	buď
dlanitě	dlanitě	k6eAd1	dlanitě
nebo	nebo	k8xC	nebo
zpeřeně	zpeřeně	k6eAd1	zpeřeně
<g/>
.	.	kIx.	.
</s>
<s>
Členěné	členěný	k2eAgInPc1d1	členěný
listy	list	k1gInPc1	list
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
podle	podle	k7c2	podle
hloubky	hloubka	k1gFnSc2	hloubka
zářezů	zářez	k1gInPc2	zářez
na	na	k7c4	na
laločnaté	laločnatý	k2eAgNnSc4d1	laločnaté
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
zářezy	zářez	k1gInPc4	zářez
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
čepele	čepel	k1gFnSc2	čepel
<g/>
;	;	kIx,	;
pak	pak	k6eAd1	pak
list	list	k1gInSc4	list
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
peřenolaločnatý	peřenolaločnatý	k2eAgMnSc1d1	peřenolaločnatý
a	a	k8xC	a
dlanitolaločný	dlanitolaločný	k2eAgMnSc1d1	dlanitolaločný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zářezy	zářez	k1gInPc1	zářez
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
jedné	jeden	k4xCgFnSc2	jeden
poloviny	polovina	k1gFnSc2	polovina
čepele	čepel	k1gFnSc2	čepel
<g/>
,	,	kIx,	,
označujeme	označovat	k5eAaImIp1nP	označovat
listy	list	k1gInPc4	list
jako	jako	k8xC	jako
peřenoklané	peřenoklaný	k2eAgFnPc4d1	peřenoklaný
a	a	k8xC	a
dlanitoklané	dlanitoklaný	k2eAgFnPc4d1	dlanitoklaný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zářez	zářez	k1gInSc1	zářez
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
čepele	čepel	k1gInSc2	čepel
<g/>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
listech	list	k1gInPc6	list
peřenodílných	peřenodílný	k2eAgInPc6d1	peřenodílný
a	a	k8xC	a
dlanitodílných	dlanitodílný	k2eAgInPc6d1	dlanitodílný
<g/>
,	,	kIx,	,
když	když	k8xS	když
zářez	zářez	k1gInSc1	zářez
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
k	k	k7c3	k
hlavní	hlavní	k2eAgFnSc3d1	hlavní
žilce	žilka	k1gFnSc3	žilka
<g/>
,	,	kIx,	,
hodnotíme	hodnotit	k5eAaImIp1nP	hodnotit
listy	list	k1gInPc1	list
jako	jako	k8xC	jako
peřenosečné	peřenosečný	k2eAgFnPc1d1	peřenosečná
a	a	k8xC	a
dlanitosečné	dlanitosečný	k2eAgFnPc1d1	dlanitosečný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
listům	list	k1gInPc3	list
s	s	k7c7	s
členěnou	členěný	k2eAgFnSc7d1	členěná
čepelí	čepel	k1gFnSc7	čepel
patří	patřit	k5eAaImIp3nP	patřit
listy	list	k1gInPc4	list
znožené	znožený	k2eAgInPc4d1	znožený
se	se	k3xPyFc4	se
znoženou	znožený	k2eAgFnSc7d1	znožený
žilnatinou	žilnatina	k1gFnSc7	žilnatina
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
složené	složený	k2eAgInPc1d1	složený
mají	mít	k5eAaImIp3nP	mít
čepel	čepel	k1gFnSc4	čepel
rozdělenou	rozdělená	k1gFnSc4	rozdělená
na	na	k7c4	na
samostatné	samostatný	k2eAgInPc4d1	samostatný
lístky	lístek	k1gInPc4	lístek
a	a	k8xC	a
podle	podle	k7c2	podle
uspořádání	uspořádání	k1gNnSc2	uspořádání
na	na	k7c6	na
vřetenu	vřeteno	k1gNnSc6	vřeteno
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
zpeřené	zpeřený	k2eAgFnPc1d1	zpeřená
nebo	nebo	k8xC	nebo
dlanitě	dlanitě	k6eAd1	dlanitě
zpeřené	zpeřený	k2eAgNnSc1d1	zpeřené
<g/>
.	.	kIx.	.
</s>
<s>
Zpeřené	zpeřený	k2eAgInPc1d1	zpeřený
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
sudozpeřené	sudozpeřený	k2eAgFnPc1d1	sudozpeřený
nebo	nebo	k8xC	nebo
lichozpeřené	lichozpeřený	k2eAgFnPc1d1	lichozpeřený
<g/>
.	.	kIx.	.
</s>
<s>
Střídají	střídat	k5eAaImIp3nP	střídat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
jařma	jařmo	k1gNnSc2	jařmo
velkých	velký	k2eAgInPc2d1	velký
a	a	k8xC	a
malých	malý	k2eAgInPc2d1	malý
lístků	lístek	k1gInPc2	lístek
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
list	list	k1gInSc4	list
přetrhovaně	přetrhovaně	k6eAd1	přetrhovaně
zpeřený	zpeřený	k2eAgMnSc1d1	zpeřený
<g/>
,	,	kIx,	,
např.	např.	kA	např.
brambor	brambor	k1gInSc1	brambor
(	(	kIx(	(
<g/>
Solanum	Solanum	k1gInSc1	Solanum
tuberosum	tuberosum	k1gInSc1	tuberosum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zástupci	zástupce	k1gMnPc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
miříkovitých	miříkovitý	k2eAgNnPc2d1	miříkovité
(	(	kIx(	(
<g/>
Apiaceae	Apiaceae	k1gNnPc2	Apiaceae
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
kapradiny	kapradina	k1gFnPc1	kapradina
mají	mít	k5eAaImIp3nP	mít
třikrát	třikrát	k6eAd1	třikrát
i	i	k9	i
vícekrát	vícekrát	k6eAd1	vícekrát
zpeřené	zpeřený	k2eAgInPc4d1	zpeřený
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Dlanitě	dlanitě	k6eAd1	dlanitě
složené	složený	k2eAgInPc1d1	složený
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
tvořené	tvořený	k2eAgInPc1d1	tvořený
z	z	k7c2	z
paprsčitě	paprsčitě	k6eAd1	paprsčitě
uspořádaných	uspořádaný	k2eAgInPc2d1	uspořádaný
lístků	lístek	k1gInPc2	lístek
na	na	k7c6	na
konci	konec	k1gInSc6	konec
řapíku	řapík	k1gInSc2	řapík
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
lístků	lístek	k1gInPc2	lístek
jsou	být	k5eAaImIp3nP	být
trojčetné	trojčetný	k2eAgFnSc3d1	trojčetná
–	–	k?	–
např.	např.	kA	např.
jetel	jetel	k1gInSc1	jetel
(	(	kIx(	(
<g/>
Trifolium	Trifolium	k1gNnSc1	Trifolium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čtyřčetné	čtyřčetný	k2eAgFnSc2d1	čtyřčetná
–	–	k?	–
např.	např.	kA	např.
šťavel	šťavel	k1gInSc1	šťavel
(	(	kIx(	(
<g/>
Oxalis	Oxalis	k1gInSc1	Oxalis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
petičetné	petičetný	k2eAgFnSc2d1	petičetný
<g/>
,	,	kIx,	,
sedmičetné	sedmičetný	k2eAgFnSc2d1	sedmičetný
a	a	k8xC	a
mnohočetné	mnohočetný	k2eAgFnSc2d1	mnohočetná
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vlčí	vlčí	k2eAgInSc1d1	vlčí
bob	bob	k1gInSc1	bob
mnoholistý	mnoholistý	k2eAgInSc1d1	mnoholistý
(	(	kIx(	(
<g/>
Lupinus	Lupinus	k1gInSc1	Lupinus
polyphyllus	polyphyllus	k1gInSc1	polyphyllus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Listy	lista	k1gFnPc4	lista
také	také	k9	také
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
řapíkaté	řapíkatý	k2eAgNnSc4d1	řapíkaté
(	(	kIx(	(
<g/>
javor	javor	k1gInSc4	javor
<g/>
,	,	kIx,	,
lípa	lípa	k1gFnSc1	lípa
<g/>
)	)	kIx)	)
a	a	k8xC	a
bez	bez	k7c2	bez
řapíku	řapík	k1gInSc2	řapík
(	(	kIx(	(
<g/>
dub	dub	k1gInSc4	dub
letní	letní	k2eAgInSc4d1	letní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
bez	bez	k7c2	bez
řapíku	řapík	k1gInSc2	řapík
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
přisedlé	přisedlý	k2eAgFnPc1d1	přisedlá
nebo	nebo	k8xC	nebo
objímavé	objímavý	k2eAgFnPc1d1	objímavá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
pokožka	pokožka	k1gFnSc1	pokožka
tvořená	tvořený	k2eAgFnSc1d1	tvořená
plochými	plochý	k2eAgFnPc7d1	plochá
buňkami	buňka	k1gFnPc7	buňka
–	–	k?	–
epidermis	epidermis	k1gFnPc6	epidermis
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
krytá	krytý	k2eAgFnSc1d1	krytá
kutikulou	kutikula	k1gFnSc7	kutikula
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
epidermis	epidermis	k1gFnPc2	epidermis
jsou	být	k5eAaImIp3nP	být
průduchy	průduch	k1gInPc1	průduch
<g/>
,	,	kIx,	,
trichomy	trichom	k1gInPc1	trichom
a	a	k8xC	a
papily	papila	k1gFnPc1	papila
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dřevin	dřevina	k1gFnPc2	dřevina
jsou	být	k5eAaImIp3nP	být
průduchy	průduch	k1gInPc1	průduch
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
listů	list	k1gInPc2	list
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
u	u	k7c2	u
bylin	bylina	k1gFnPc2	bylina
jsou	být	k5eAaImIp3nP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hustota	hustota	k1gFnSc1	hustota
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
jich	on	k3xPp3gInPc2	on
být	být	k5eAaImF	být
i	i	k9	i
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
na	na	k7c4	na
1	[number]	k4	1
mm2	mm2	k4	mm2
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
průduchu	průduch	k1gInSc2	průduch
a	a	k8xC	a
struktura	struktura	k1gFnSc1	struktura
sousedních	sousední	k2eAgFnPc2d1	sousední
buněk	buňka	k1gFnPc2	buňka
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
diagnostickým	diagnostický	k2eAgInSc7d1	diagnostický
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Přebytečná	přebytečný	k2eAgFnSc1d1	přebytečná
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
listů	list	k1gInPc2	list
vylučována	vylučovat	k5eAaImNgFnS	vylučovat
vodními	vodní	k2eAgFnPc7d1	vodní
skulinami	skulina	k1gFnPc7	skulina
–	–	k?	–
hydatodami	hydatoda	k1gFnPc7	hydatoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
na	na	k7c6	na
špičce	špička	k1gFnSc6	špička
listů	list	k1gInPc2	list
nebo	nebo	k8xC	nebo
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
zoubků	zoubek	k1gInPc2	zoubek
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
pletivo	pletivo	k1gNnSc1	pletivo
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
mezofyl	mezofyl	k1gInSc1	mezofyl
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
bifaciálního	bifaciální	k2eAgInSc2d1	bifaciální
listu	list	k1gInSc2	list
rozčleněn	rozčlenit	k5eAaPmNgInS	rozčlenit
na	na	k7c4	na
svrchní	svrchní	k2eAgFnSc4d1	svrchní
–	–	k?	–
<g/>
palisádový	palisádový	k2eAgInSc1d1	palisádový
parenchym	parenchym	k1gInSc1	parenchym
a	a	k8xC	a
dolní	dolní	k2eAgInSc1d1	dolní
houbový	houbový	k2eAgInSc1d1	houbový
parenchym	parenchym	k1gInSc1	parenchym
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
listů	list	k1gInPc2	list
monofaciálních	monofaciální	k2eAgNnPc2d1	monofaciální
je	být	k5eAaImIp3nS	být
palisádový	palisádový	k2eAgInSc1d1	palisádový
parenchym	parenchym	k1gInSc1	parenchym
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
a	a	k8xC	a
uprostřed	uprostřed	k6eAd1	uprostřed
je	být	k5eAaImIp3nS	být
houbový	houbový	k2eAgInSc4d1	houbový
parenchym	parenchym	k1gInSc4	parenchym
<g/>
.	.	kIx.	.
</s>
<s>
Palisádové	palisádový	k2eAgFnPc1d1	palisádová
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
protáhlé	protáhlý	k2eAgFnPc1d1	protáhlá
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
chloroplastů	chloroplast	k1gInPc2	chloroplast
<g/>
.	.	kIx.	.
</s>
<s>
Houbový	houbový	k2eAgInSc1d1	houbový
parenchym	parenchym	k1gInSc1	parenchym
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
méně	málo	k6eAd2	málo
chloroplastů	chloroplast	k1gInPc2	chloroplast
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
laločnatými	laločnatý	k2eAgFnPc7d1	laločnatá
buňkami	buňka	k1gFnPc7	buňka
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
mezibuněčné	mezibuněčný	k2eAgInPc4d1	mezibuněčný
prostory	prostor	k1gInPc4	prostor
pro	pro	k7c4	pro
výměnu	výměna	k1gFnSc4	výměna
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Žilnatina	žilnatina	k1gFnSc1	žilnatina
(	(	kIx(	(
<g/>
botanika	botanika	k1gFnSc1	botanika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
svazků	svazek	k1gInPc2	svazek
cévních	cévní	k2eAgInPc2d1	cévní
v	v	k7c6	v
listech	list	k1gInPc6	list
tvoří	tvořit	k5eAaImIp3nP	tvořit
žilnatinu	žilnatina	k1gFnSc4	žilnatina
neboli	neboli	k8xC	neboli
nervaturu	nervatura	k1gFnSc4	nervatura
<g/>
.	.	kIx.	.
</s>
<s>
Nejpůvodnějším	původní	k2eAgInSc7d3	nejpůvodnější
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
žilnatina	žilnatina	k1gFnSc1	žilnatina
vidličnatá	vidličnatý	k2eAgFnSc1d1	vidličnatá
<g/>
,	,	kIx,	,
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
kapradin	kapradina	k1gFnPc2	kapradina
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgFnPc1d1	stejná
žilky	žilka	k1gFnPc1	žilka
<g/>
,	,	kIx,	,
vidličnatě	vidličnatě	k6eAd1	vidličnatě
větvené	větvený	k2eAgInPc1d1	větvený
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
spojení	spojení	k1gNnSc1	spojení
příčnými	příčný	k2eAgFnPc7d1	příčná
spojkami	spojka	k1gFnPc7	spojka
–	–	k?	–
anastomózami	anastomóza	k1gFnPc7	anastomóza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
dvouděložných	dvouděložná	k1gFnPc2	dvouděložná
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nejčastěji	často	k6eAd3	často
žilnatina	žilnatina	k1gFnSc1	žilnatina
zpeřená	zpeřený	k2eAgFnSc1d1	zpeřená
a	a	k8xC	a
dlanitá	dlanitý	k2eAgFnSc1d1	dlanitá
a	a	k8xC	a
vzácněji	vzácně	k6eAd2	vzácně
žilnatina	žilnatina	k1gFnSc1	žilnatina
znožená	znožený	k2eAgFnSc1d1	znožený
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zpeřenou	zpeřený	k2eAgFnSc4d1	zpeřená
žilnatinu	žilnatina	k1gFnSc4	žilnatina
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
jedna	jeden	k4xCgFnSc1	jeden
hlavní	hlavní	k2eAgFnSc1d1	hlavní
žilka	žilka	k1gFnSc1	žilka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
středem	střed	k1gInSc7	střed
listu	list	k1gInSc2	list
a	a	k8xC	a
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
odbočují	odbočovat	k5eAaImIp3nP	odbočovat
žilky	žilka	k1gFnPc1	žilka
postranní	postranní	k2eAgFnPc1d1	postranní
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
větví	větvit	k5eAaImIp3nP	větvit
<g/>
,	,	kIx,	,
např.	např.	kA	např.
list	list	k1gInSc1	list
topolu	topol	k1gInSc2	topol
(	(	kIx(	(
<g/>
Populus	Populus	k1gInSc1	Populus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jednoděložních	jednoděložní	k2eAgFnPc2d1	jednoděložní
rostlin	rostlina	k1gFnPc2	rostlina
s	s	k7c7	s
listy	list	k1gInPc7	list
typu	typ	k1gInSc2	typ
konvalinky	konvalinka	k1gFnSc2	konvalinka
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
žilnatina	žilnatina	k1gFnSc1	žilnatina
souběžná	souběžný	k2eAgFnSc1d1	souběžná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dlanité	dlanitý	k2eAgFnSc2d1	dlanitá
žilnatiny	žilnatina	k1gFnSc2	žilnatina
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
listu	list	k1gInSc2	list
paprsčitě	paprsčitě	k6eAd1	paprsčitě
několik	několik	k4yIc1	několik
žilek	žilka	k1gFnPc2	žilka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
větví	větvit	k5eAaImIp3nS	větvit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
znožené	znožený	k2eAgFnSc6d1	znožený
žilnatině	žilnatina	k1gFnSc6	žilnatina
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
z	z	k7c2	z
báze	báze	k1gFnSc2	báze
čepele	čepel	k1gFnSc2	čepel
jediná	jediný	k2eAgFnSc1d1	jediná
hlavní	hlavní	k2eAgFnSc1d1	hlavní
žilka	žilka	k1gFnSc1	žilka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
větví	větvit	k5eAaImIp3nS	větvit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgFnPc1d1	další
žilky	žilka	k1gFnPc1	žilka
vznikají	vznikat	k5eAaImIp3nP	vznikat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
zevní	zevní	k2eAgFnSc6d1	zevní
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jednoděložných	jednoděložný	k2eAgFnPc2d1	jednoděložná
rostlin	rostlina	k1gFnPc2	rostlina
převládá	převládat	k5eAaImIp3nS	převládat
žilnatina	žilnatina	k1gFnSc1	žilnatina
rovnoběžná	rovnoběžný	k2eAgFnSc1d1	rovnoběžná
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
žilkami	žilka	k1gFnPc7	žilka
probíhajícími	probíhající	k2eAgMnPc7d1	probíhající
rovnoběžně	rovnoběžně	k6eAd1	rovnoběžně
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
listu	list	k1gInSc2	list
nebo	nebo	k8xC	nebo
souběžná	souběžný	k2eAgFnSc1d1	souběžná
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
žilkami	žilka	k1gFnPc7	žilka
vybíhajícími	vybíhající	k2eAgFnPc7d1	vybíhající
od	od	k7c2	od
báze	báze	k1gFnSc2	báze
listu	list	k1gInSc2	list
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Rovnoběžnou	rovnoběžný	k2eAgFnSc4d1	rovnoběžná
žilnatinu	žilnatina	k1gFnSc4	žilnatina
mají	mít	k5eAaImIp3nP	mít
listy	list	k1gInPc1	list
lipnicovitých	lipnicovitý	k2eAgInPc2d1	lipnicovitý
(	(	kIx(	(
<g/>
Poaceae	Poaceae	k1gNnPc2	Poaceae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stonku	stonek	k1gInSc2	stonek
do	do	k7c2	do
listu	list	k1gInSc2	list
přecházejí	přecházet	k5eAaImIp3nP	přecházet
svazky	svazek	k1gInPc1	svazek
bez	bez	k7c2	bez
překrucování	překrucování	k1gNnSc2	překrucování
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
kolaterálního	kolaterální	k2eAgInSc2d1	kolaterální
svazku	svazek	k1gInSc2	svazek
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
na	na	k7c4	na
vrchní	vrchní	k1gFnSc4	vrchní
a	a	k8xC	a
lýko	lýko	k1gNnSc4	lýko
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
straně	strana	k1gFnSc6	strana
čepele	čepel	k1gInSc2	čepel
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
žilek	žilka	k1gFnPc2	žilka
jsou	být	k5eAaImIp3nP	být
parenchymatické	parenchymatický	k2eAgFnPc1d1	parenchymatická
nebo	nebo	k8xC	nebo
sklerenchymatické	sklerenchymatický	k2eAgFnPc1d1	sklerenchymatická
pochvy	pochva	k1gFnPc1	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
se	se	k3xPyFc4	se
zakládají	zakládat	k5eAaImIp3nP	zakládat
na	na	k7c6	na
vzrostném	vzrostný	k2eAgInSc6d1	vzrostný
vrcholu	vrchol	k1gInSc6	vrchol
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
sledu	sled	k1gInSc6	sled
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgNnSc1d3	nejrozšířenější
postavení	postavení	k1gNnSc1	postavení
listů	list	k1gInPc2	list
je	být	k5eAaImIp3nS	být
střídavé	střídavý	k2eAgNnSc1d1	střídavé
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
základy	základ	k1gInPc1	základ
listů	list	k1gInPc2	list
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
druhů	druh	k1gInPc2	druh
se	s	k7c7	s
vstřícnými	vstřícný	k2eAgInPc7d1	vstřícný
listy	list	k1gInPc7	list
vznikají	vznikat	k5eAaImIp3nP	vznikat
dva	dva	k4xCgInPc4	dva
listové	listový	k2eAgInPc4d1	listový
základy	základ	k1gInPc4	základ
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Přeslenité	přeslenitý	k2eAgInPc1d1	přeslenitý
listy	list	k1gInPc1	list
se	se	k3xPyFc4	se
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
z	z	k7c2	z
několika	několik	k4yIc2	několik
hrbolků	hrbolek	k1gInPc2	hrbolek
současně	současně	k6eAd1	současně
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
potřebný	potřebný	k2eAgInSc1d1	potřebný
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
dvou	dva	k4xCgMnPc2	dva
po	po	k7c6	po
sobě	se	k3xPyFc3	se
následujících	následující	k2eAgInPc2d1	následující
listových	listový	k2eAgInPc2d1	listový
základů	základ	k1gInPc2	základ
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
plastochron	plastochron	k1gInSc1	plastochron
<g/>
.	.	kIx.	.
</s>
<s>
Sledujeme	sledovat	k5eAaImIp1nP	sledovat
<g/>
-li	i	k?	-li
postavení	postavení	k1gNnSc4	postavení
střídavě	střídavě	k6eAd1	střídavě
uspořádaných	uspořádaný	k2eAgInPc2d1	uspořádaný
listů	list	k1gInPc2	list
na	na	k7c4	na
stonku	stonka	k1gFnSc4	stonka
<g/>
,	,	kIx,	,
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
listy	list	k1gInPc1	list
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
pomyslné	pomyslný	k2eAgFnSc6d1	pomyslná
genetické	genetický	k2eAgFnSc6d1	genetická
spirále	spirála	k1gFnSc6	spirála
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
svislé	svislý	k2eAgFnPc1d1	svislá
řady	řada	k1gFnPc1	řada
stojící	stojící	k2eAgFnPc1d1	stojící
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
ortostichy	ortostich	k1gInPc4	ortostich
<g/>
.	.	kIx.	.
</s>
<s>
Fylotaxie	fylotaxie	k1gFnSc1	fylotaxie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vystižena	vystižen	k2eAgFnSc1d1	vystižena
počtem	počet	k1gInSc7	počet
otáček	otáčka	k1gFnPc2	otáčka
šroubovice	šroubovice	k1gFnSc2	šroubovice
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
listů	list	k1gInPc2	list
na	na	k7c6	na
spirále	spirála	k1gFnSc6	spirála
od	od	k7c2	od
nulového	nulový	k2eAgNnSc2d1	nulové
až	až	k9	až
k	k	k7c3	k
následujícímu	následující	k2eAgInSc3d1	následující
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stojí	stát	k5eAaImIp3nS	stát
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
zlomkem	zlomek	k1gInSc7	zlomek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zlomek	zlomek	k1gInSc1	zlomek
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
poměry	poměr	k1gInPc4	poměr
u	u	k7c2	u
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
listy	list	k1gInPc1	list
stojí	stát	k5eAaImIp3nP	stát
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
pěti	pět	k4xCc6	pět
řadách	řada	k1gFnPc6	řada
a	a	k8xC	a
šroubovice	šroubovice	k1gFnSc2	šroubovice
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
dvakrát	dvakrát	k6eAd1	dvakrát
kolem	kolem	k7c2	kolem
stonku	stonek	k1gInSc2	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
složení	složení	k1gNnSc2	složení
čepelí	čepel	k1gFnPc2	čepel
a	a	k8xC	a
mladých	mladý	k2eAgInPc2d1	mladý
listů	list	k1gInPc2	list
v	v	k7c6	v
pupenech	pupen	k1gInPc6	pupen
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
vernace	vernace	k1gFnSc1	vernace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
poloviny	polovina	k1gFnPc1	polovina
čepele	čepel	k1gFnSc2	čepel
přikládají	přikládat	k5eAaImIp3nP	přikládat
k	k	k7c3	k
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
vernace	vernace	k1gFnSc1	vernace
složená	složený	k2eAgFnSc1d1	složená
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vernaci	vernace	k1gFnSc6	vernace
nadvinuté	nadvinutý	k2eAgFnSc6d1	nadvinutý
<g/>
,	,	kIx,	,
podvinuté	podvinutý	k2eAgFnSc6d1	podvinutá
<g/>
,	,	kIx,	,
řasnaté	řasnatý	k2eAgFnSc6d1	řasnatá
<g/>
,	,	kIx,	,
svinuté	svinutý	k2eAgFnSc6d1	svinutá
nebo	nebo	k8xC	nebo
zmuchlané	zmuchlaný	k2eAgNnSc1d1	zmuchlané
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
charakteristické	charakteristický	k2eAgNnSc4d1	charakteristické
svinutí	svinutí	k1gNnSc4	svinutí
mladých	mladý	k2eAgFnPc2d1	mladá
čepelí	čepel	k1gFnPc2	čepel
v	v	k7c6	v
pupenech	pupen	k1gInPc6	pupen
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
poloha	poloha	k1gFnSc1	poloha
listových	listový	k2eAgInPc2d1	listový
základů	základ	k1gInPc2	základ
v	v	k7c6	v
pupenu	pupen	k1gInSc6	pupen
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
listová	listový	k2eAgFnSc1d1	listová
estivace	estivace	k1gFnSc1	estivace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
mladé	mladý	k2eAgInPc4d1	mladý
lístky	lístek	k1gInPc1	lístek
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
spirálovitě	spirálovitě	k6eAd1	spirálovitě
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
estivace	estivace	k1gFnSc1	estivace
svinutá	svinutý	k2eAgFnSc1d1	svinutá
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
se	se	k3xPyFc4	se
překrývají	překrývat	k5eAaImIp3nP	překrývat
<g/>
,	,	kIx,	,
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
estivaci	estivace	k1gFnSc6	estivace
střechovité	střechovitý	k2eAgFnSc6d1	střechovitá
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ontogeneze	ontogeneze	k1gFnSc2	ontogeneze
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
stonku	stonek	k1gInSc6	stonek
několik	několik	k4yIc1	několik
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
listových	listový	k2eAgInPc2d1	listový
typů	typ	k1gInPc2	typ
(	(	kIx(	(
<g/>
modifikací	modifikace	k1gFnPc2	modifikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
klíční	klíční	k2eAgFnSc6d1	klíční
rostlině	rostlina	k1gFnSc6	rostlina
bývají	bývat	k5eAaImIp3nP	bývat
zřetelné	zřetelný	k2eAgFnPc1d1	zřetelná
dělohy	děloha	k1gFnPc1	děloha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
především	především	k6eAd1	především
zásobní	zásobní	k2eAgFnSc4d1	zásobní
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Šupiny	šupina	k1gFnPc1	šupina
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
stonku	stonek	k1gInSc2	stonek
nebo	nebo	k8xC	nebo
chrání	chránit	k5eAaImIp3nS	chránit
základy	základ	k1gInPc4	základ
listů	list	k1gInPc2	list
v	v	k7c6	v
pupenech	pupen	k1gInPc6	pupen
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
na	na	k7c6	na
stonku	stonek	k1gInSc6	stonek
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
lupenité	lupenitý	k2eAgInPc4d1	lupenitý
<g/>
,	,	kIx,	,
asimilační	asimilační	k2eAgInPc4d1	asimilační
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
spodnější	spodní	k2eAgInPc1d2	spodnější
listy	list	k1gInPc1	list
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
a	a	k8xC	a
vyšší	vysoký	k2eAgInPc1d2	vyšší
mívají	mívat	k5eAaImIp3nP	mívat
čepel	čepel	k1gFnSc4	čepel
členitější	členitý	k2eAgFnSc4d2	členitější
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
rostliny	rostlina	k1gFnPc4	rostlina
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
heterofylie	heterofylie	k1gFnSc1	heterofylie
(	(	kIx(	(
<g/>
různolistost	různolistost	k1gFnSc1	různolistost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výskyt	výskyt	k1gInSc4	výskyt
dvou	dva	k4xCgMnPc2	dva
výrazně	výrazně	k6eAd1	výrazně
odlišných	odlišný	k2eAgMnPc2d1	odlišný
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Listeny	listen	k1gInPc1	listen
a	a	k8xC	a
listence	listenec	k1gInPc1	listenec
jsou	být	k5eAaImIp3nP	být
redukované	redukovaný	k2eAgInPc1d1	redukovaný
listové	listový	k2eAgInPc1d1	listový
úkazy	úkaz	k1gInPc1	úkaz
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
květů	květ	k1gInPc2	květ
nebo	nebo	k8xC	nebo
květenství	květenství	k1gNnSc2	květenství
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
rostlin	rostlina	k1gFnPc2	rostlina
tvoří	tvořit	k5eAaImIp3nS	tvořit
listeny	listen	k1gInPc4	listen
tzv.	tzv.	kA	tzv.
zákrov	zákrov	k1gInSc4	zákrov
<g/>
.	.	kIx.	.
</s>
<s>
Listeny	listen	k1gInPc1	listen
u	u	k7c2	u
lipnicovitých	lipnicovitý	k2eAgFnPc2d1	lipnicovitý
(	(	kIx(	(
<g/>
Poaceae	Poaceae	k1gFnPc2	Poaceae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
plevy	pleva	k1gFnPc1	pleva
a	a	k8xC	a
pluchy	plucha	k1gFnPc1	plucha
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
přeměnám	přeměna	k1gFnPc3	přeměna
listu	list	k1gInSc2	list
dochází	docházet	k5eAaImIp3nP	docházet
při	při	k7c6	při
metamorfóze	metamorfóza	k1gFnSc6	metamorfóza
v	v	k7c4	v
zásobní	zásobní	k2eAgInPc4d1	zásobní
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
nezelené	zelený	k2eNgFnPc1d1	nezelená
<g/>
,	,	kIx,	,
masité	masitý	k2eAgFnPc1d1	masitá
šupiny	šupina	k1gFnPc1	šupina
cibule	cibule	k1gFnSc2	cibule
nebo	nebo	k8xC	nebo
zelené	zelený	k2eAgInPc1d1	zelený
listy	list	k1gInPc1	list
rostlin	rostlina	k1gFnPc2	rostlina
tlusticovitých	tlusticovitý	k2eAgFnPc2d1	tlusticovitý
<g/>
.	.	kIx.	.
</s>
<s>
Listové	listový	k2eAgInPc1d1	listový
trny	trn	k1gInPc1	trn
vznikají	vznikat	k5eAaImIp3nP	vznikat
přeměnou	přeměna	k1gFnSc7	přeměna
listů	list	k1gInPc2	list
s	s	k7c7	s
palisty	palist	k1gInPc7	palist
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zesílením	zesílení	k1gNnSc7	zesílení
listových	listový	k2eAgInPc2d1	listový
svazků	svazek	k1gInPc2	svazek
vznikají	vznikat	k5eAaImIp3nP	vznikat
trojdílné	trojdílný	k2eAgInPc1d1	trojdílný
trny	trn	k1gInPc1	trn
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
trnovníku	trnovník	k1gInSc2	trnovník
akátu	akát	k1gInSc2	akát
(	(	kIx(	(
<g/>
Robinia	Robinium	k1gNnSc2	Robinium
pseudoacacia	pseudoacacium	k1gNnSc2	pseudoacacium
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
trny	trn	k1gInPc4	trn
z	z	k7c2	z
palistů	palist	k1gInPc2	palist
<g/>
.	.	kIx.	.
</s>
<s>
Listové	listový	k2eAgInPc1d1	listový
úponky	úponek	k1gInPc1	úponek
hrachu	hrách	k1gInSc2	hrách
(	(	kIx(	(
<g/>
Pisum	Pisum	k1gInSc1	Pisum
sativum	sativum	k1gInSc1	sativum
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
přeměněná	přeměněný	k2eAgNnPc4d1	přeměněné
vřetena	vřeteno	k1gNnPc4	vřeteno
zpeřených	zpeřený	k2eAgInPc2d1	zpeřený
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Paždí	paždí	k1gNnSc1	paždí
(	(	kIx(	(
<g/>
úžlabí	úžlabí	k1gNnSc1	úžlabí
<g/>
)	)	kIx)	)
je	on	k3xPp3gNnSc4	on
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
úhlu	úhel	k1gInSc6	úhel
mezi	mezi	k7c7	mezi
stonkem	stonek	k1gInSc7	stonek
a	a	k8xC	a
listem	list	k1gInSc7	list
<g/>
.	.	kIx.	.
potrava	potrava	k1gFnSc1	potrava
–	–	k?	–
zelí	zelí	k1gNnSc1	zelí
<g/>
,	,	kIx,	,
kapusta	kapusta	k1gFnSc1	kapusta
<g/>
,	,	kIx,	,
cibule	cibule	k1gFnSc1	cibule
<g/>
,	,	kIx,	,
špenát	špenát	k1gInSc1	špenát
<g/>
,	,	kIx,	,
salát	salát	k1gInSc1	salát
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
koření	kořenit	k5eAaImIp3nS	kořenit
–	–	k?	–
vavřín	vavřín	k1gInSc1	vavřín
(	(	kIx(	(
<g/>
bobkový	bobkový	k2eAgInSc1d1	bobkový
list	list	k1gInSc1	list
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
majoránka	majoránka	k1gFnSc1	majoránka
<g/>
,	,	kIx,	,
léčiva	léčivo	k1gNnSc2	léčivo
–	–	k?	–
máta	máta	k1gFnSc1	máta
<g/>
,	,	kIx,	,
durman	durman	k1gInSc1	durman
<g/>
,	,	kIx,	,
náprstník	náprstník	k1gInSc1	náprstník
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
rostliny	rostlina	k1gFnPc1	rostlina
–	–	k?	–
sisal	sisal	k1gInSc1	sisal
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
konopí	konopí	k1gNnSc1	konopí
<g/>
,	,	kIx,	,
pícniny	pícnina	k1gFnPc1	pícnina
–	–	k?	–
jetel	jetel	k1gInSc1	jetel
<g/>
,	,	kIx,	,
traviny	travina	k1gFnPc1	travina
<g/>
.	.	kIx.	.
</s>
<s>
REISENAUER	REISENAUER	kA	REISENAUER
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
aj.	aj.	kA	aj.
Co	co	k9	co
je	být	k5eAaImIp3nS	být
co	co	k3yInSc1	co
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
:	:	kIx,	:
příručka	příručka	k1gFnSc1	příručka
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přeprac	přeprac	k1gFnSc1	přeprac
<g/>
.	.	kIx.	.
a	a	k8xC	a
dopl	dopl	k1gInSc1	dopl
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Pressfoto	Pressfota	k1gFnSc5	Pressfota
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
3	[number]	k4	3
sv.	sv.	kA	sv.
Slovník	slovník	k1gInSc1	slovník
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
pro	pro	k7c4	pro
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
:	:	kIx,	:
s	s	k7c7	s
Dodatkem	dodatek	k1gInSc7	dodatek
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vyd	Vyd	k?	Vyd
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
opr	opr	k?	opr
<g/>
.	.	kIx.	.
a	a	k8xC	a
dopl	dopl	k1gInSc4	dopl
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
647	[number]	k4	647
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
493	[number]	k4	493
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
PROCHÁZKA	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Botanika	botanika	k1gFnSc1	botanika
<g/>
:	:	kIx,	:
morfologie	morfologie	k1gFnSc1	morfologie
a	a	k8xC	a
fyziologie	fyziologie	k1gFnSc1	fyziologie
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
:	:	kIx,	:
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
lesnická	lesnický	k2eAgFnSc1d1	lesnická
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
242	[number]	k4	242
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7157	[number]	k4	7157
<g/>
-	-	kIx~	-
<g/>
313	[number]	k4	313
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
list	lista	k1gFnPc2	lista
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
list	list	k1gInSc1	list
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
