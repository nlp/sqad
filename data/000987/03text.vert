<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
(	(	kIx(	(
<g/>
zastarale	zastarale	k6eAd1	zastarale
Norvežsko	Norvežsko	k1gNnSc1	Norvežsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
jazykové	jazykový	k2eAgFnSc6d1	jazyková
verzi	verze	k1gFnSc6	verze
bokmå	bokmå	k?	bokmå
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
Kongeriket	Kongeriketa	k1gFnPc2	Kongeriketa
Norge	Norge	k1gFnPc2	Norge
(	(	kIx(	(
<g/>
Norské	norský	k2eAgNnSc1d1	norské
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
nynorsk	nynorsk	k1gInSc1	nynorsk
Kongeriket	Kongeriketa	k1gFnPc2	Kongeriketa
Noreg	Norega	k1gFnPc2	Norega
<g/>
,	,	kIx,	,
v	v	k7c6	v
řeči	řeč	k1gFnSc6	řeč
národnostní	národnostní	k2eAgFnSc2d1	národnostní
menšiny	menšina	k1gFnSc2	menšina
Sámů	Sámo	k1gMnPc2	Sámo
Norka	Norka	k1gFnSc1	Norka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
323 758	[number]	k4	323 758
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
činí	činit	k5eAaImIp3nS	činit
8.	[number]	k4	8.
největší	veliký	k2eAgInSc1d3	veliký
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Oslo	Oslo	k1gNnSc1	Oslo
se	s	k7c7	s
zhruba	zhruba	k6eAd1	zhruba
911 000	[number]	k4	911 000
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Velké	velký	k2eAgNnSc1d1	velké
Oslo	Oslo	k1gNnSc1	Oslo
–	–	k?	–
Stor	Stor	k1gMnSc1	Stor
Oslo	Oslo	k1gNnSc2	Oslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
a	a	k8xC	a
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
se	se	k3xPyFc4	se
Švédskem	Švédsko	k1gNnSc7	Švédsko
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
hranice	hranice	k1gFnSc2	hranice
<g/>
,	,	kIx,	,
s	s	k7c7	s
Finskem	Finsko	k1gNnSc7	Finsko
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
vede	vést	k5eAaImIp3nS	vést
spor	spor	k1gInSc1	spor
o	o	k7c4	o
dělící	dělící	k2eAgFnSc4d1	dělící
linii	linie	k1gFnSc4	linie
teritoriálních	teritoriální	k2eAgFnPc2d1	teritoriální
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
Barentsově	barentsově	k6eAd1	barentsově
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
norskou	norský	k2eAgFnSc4d1	norská
suverenitu	suverenita	k1gFnSc4	suverenita
patří	patřit	k5eAaImIp3nS	patřit
rovněž	rovněž	k9	rovněž
arktické	arktický	k2eAgNnSc1d1	arktické
souostroví	souostroví	k1gNnSc1	souostroví
Svalbard	Svalbard	k1gInSc1	Svalbard
(	(	kIx(	(
<g/>
Špicberky	Špicberky	k1gFnPc1	Špicberky
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostrov	ostrov	k1gInSc1	ostrov
Jan	Jan	k1gMnSc1	Jan
Mayen	Mayna	k1gFnPc2	Mayna
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
leží	ležet	k5eAaImIp3nP	ležet
nedaleko	nedaleko	k7c2	nedaleko
Islandu	Island	k1gInSc2	Island
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
dosud	dosud	k6eAd1	dosud
patří	patřit	k5eAaImIp3nS	patřit
Norsku	Norsko	k1gNnSc6	Norsko
také	také	k9	také
ostrov	ostrov	k1gInSc4	ostrov
Bouvetø	Bouvetø	k1gFnSc2	Bouvetø
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
si	se	k3xPyFc3	se
činí	činit	k5eAaImIp3nS	činit
nárok	nárok	k1gInSc4	nárok
i	i	k9	i
na	na	k7c4	na
Ostrov	ostrov	k1gInSc4	ostrov
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Pacifiku	Pacifik	k1gInSc6	Pacifik
a	a	k8xC	a
teritorium	teritorium	k1gNnSc1	teritorium
Země	zem	k1gFnSc2	zem
královny	královna	k1gFnSc2	královna
Maud	Maud	k1gInSc1	Maud
(	(	kIx(	(
<g/>
Dronning	Dronning	k1gInSc1	Dronning
Maud	Maud	k1gMnSc1	Maud
Land	Land	k1gMnSc1	Land
<g/>
)	)	kIx)	)
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
</p>
<s>
Prvními	první	k4xOgFnPc7	první
obyvateli	obyvatel	k1gMnPc7	obyvatel
Norska	Norsko	k1gNnSc2	Norsko
byli	být	k5eAaImAgMnP	být
Vikingové	Viking	k1gMnPc1	Viking
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zde	zde	k6eAd1	zde
žili	žít	k5eAaImAgMnP	žít
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnSc2d1	železná
<g/>
.	.	kIx.	.
</s>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
hornatá	hornatý	k2eAgFnSc1d1	hornatá
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Galdhø	Galdhø	k1gFnSc2	Galdhø
měří	měřit	k5eAaImIp3nS	měřit
2469	[number]	k4	2469
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
její	její	k3xOp3gFnSc6	její
blízkosti	blízkost	k1gFnSc6	blízkost
také	také	k9	také
leží	ležet	k5eAaImIp3nP	ležet
dva	dva	k4xCgInPc1	dva
norské	norský	k2eAgInPc1d1	norský
ledovce	ledovec	k1gInPc1	ledovec
Jostedalsbreen	Jostedalsbrena	k1gFnPc2	Jostedalsbrena
a	a	k8xC	a
Jotunheimen	Jotunheimen	k1gInSc1	Jotunheimen
<g/>
,	,	kIx,	,
lákající	lákající	k2eAgFnPc1d1	lákající
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
tisíce	tisíc	k4xCgInSc2	tisíc
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
silnici	silnice	k1gFnSc6	silnice
E16	E16	k1gFnSc2	E16
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgNnSc1d1	spojující
Oslo	Oslo	k1gNnSc1	Oslo
s	s	k7c7	s
Bergenem	Bergen	k1gInSc7	Bergen
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
silniční	silniční	k2eAgInSc1d1	silniční
tunel	tunel	k1gInSc1	tunel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
Læ	Læ	k1gFnSc1	Læ
tunel	tunel	k1gInSc1	tunel
<g/>
,	,	kIx,	,
24,5	[number]	k4	24,5
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
severský	severský	k2eAgInSc4d1	severský
model	model	k1gInSc4	model
sociálního	sociální	k2eAgInSc2d1	sociální
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Norský	norský	k2eAgInSc1d1	norský
stát	stát	k1gInSc1	stát
rovněž	rovněž	k9	rovněž
udržuje	udržovat	k5eAaImIp3nS	udržovat
své	svůj	k3xOyFgNnSc4	svůj
vlastnictví	vlastnictví	k1gNnSc4	vlastnictví
v	v	k7c6	v
klíčových	klíčový	k2eAgNnPc6d1	klíčové
průmyslových	průmyslový	k2eAgNnPc6d1	průmyslové
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ropný	ropný	k2eAgInSc1d1	ropný
průmysl	průmysl	k1gInSc1	průmysl
představuje	představovat	k5eAaImIp3nS	představovat
zhruba	zhruba	k6eAd1	zhruba
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
patnáctým	patnáctý	k4xOgMnSc7	patnáctý
největším	veliký	k2eAgMnSc7d3	veliký
světovým	světový	k2eAgMnSc7d1	světový
producentem	producent	k1gMnSc7	producent
ropy	ropa	k1gFnSc2	ropa
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
těžařem	těžař	k1gMnSc7	těžař
ropy	ropa	k1gFnSc2	ropa
(	(	kIx(	(
<g/>
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
<g/>
)	)	kIx)	)
mimo	mimo	k7c4	mimo
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
má	mít	k5eAaImIp3nS	mít
4.	[number]	k4	4.
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
Norsko	Norsko	k1gNnSc1	Norsko
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Indexu	index	k1gInSc2	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
i	i	k8xC	i
ve	v	k7c6	v
Zprávě	zpráva	k1gFnSc6	zpráva
o	o	k7c6	o
světovém	světový	k2eAgNnSc6d1	světové
štěstí	štěstí	k1gNnSc6	štěstí
OSN	OSN	kA	OSN
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
indexu	index	k1gInSc2	index
demokracie	demokracie	k1gFnSc2	demokracie
vytvářeného	vytvářený	k2eAgInSc2d1	vytvářený
časopisem	časopis	k1gInSc7	časopis
The	The	k1gMnSc1	The
Economist	Economist	k1gMnSc1	Economist
má	mít	k5eAaImIp3nS	mít
Norsko	Norsko	k1gNnSc4	Norsko
nejkvalitnější	kvalitní	k2eAgFnSc4d3	nejkvalitnější
demokracii	demokracie	k1gFnSc4	demokracie
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
úrovní	úroveň	k1gFnPc2	úroveň
kriminality	kriminalita	k1gFnSc2	kriminalita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
dali	dát	k5eAaPmAgMnP	dát
Norsku	Norsko	k1gNnSc6	Norsko
zřejmě	zřejmě	k6eAd1	zřejmě
staří	starý	k2eAgMnPc1d1	starý
Gótové	Gót	k1gMnPc1	Gót
nebo	nebo	k8xC	nebo
Dáni	Dán	k1gMnPc1	Dán
a	a	k8xC	a
znamenalo	znamenat	k5eAaImAgNnS	znamenat
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
jazyce	jazyk	k1gInSc6	jazyk
"	"	kIx"	"
<g/>
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historickém	historický	k2eAgInSc6d1	historický
záznamu	záznam	k1gInSc6	záznam
řeči	řeč	k1gFnSc2	řeč
vikingského	vikingský	k2eAgMnSc4d1	vikingský
náčelníka	náčelník	k1gMnSc4	náčelník
Ottara	Ottar	k1gMnSc4	Ottar
k	k	k7c3	k
anglickému	anglický	k2eAgMnSc3d1	anglický
králi	král	k1gMnSc3	král
Alfrédovi	Alfréd	k1gMnSc3	Alfréd
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
880	[number]	k4	880
je	být	k5eAaImIp3nS	být
dnešní	dnešní	k2eAgNnSc4d1	dnešní
území	území	k1gNnSc4	území
Norska	Norsko	k1gNnSc2	Norsko
nazýváno	nazýván	k2eAgNnSc4d1	nazýváno
Norð	Norð	k1gFnSc7	Norð
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
14.	[number]	k4	14.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
formy	forma	k1gFnPc4	forma
jména	jméno	k1gNnSc2	jméno
země	zem	k1gFnSc2	zem
Noreg	Noreg	k1gMnSc1	Noreg
<g/>
(	(	kIx(	(
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
e	e	k0	e
<g/>
,	,	kIx,	,
Norig	Norig	k1gMnSc1	Norig
<g/>
(	(	kIx(	(
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
e	e	k0	e
<g/>
,	,	kIx,	,
Norg	Norg	k1gMnSc1	Norg
<g/>
(	(	kIx(	(
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
e	e	k0	e
nebo	nebo	k8xC	nebo
Norie	Norie	k1gFnSc2	Norie
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Noreg	Norega	k1gFnPc2	Norega
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
nynorsk	nynorsk	k1gInSc1	nynorsk
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
jazykové	jazykový	k2eAgFnSc6d1	jazyková
reformě	reforma	k1gFnSc6	reforma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
název	název	k1gInSc1	název
Norge	Norg	k1gFnSc2	Norg
v	v	k7c6	v
jazykové	jazykový	k2eAgFnSc6d1	jazyková
verzi	verze	k1gFnSc6	verze
bokmå	bokmå	k?	bokmå
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
zhruba	zhruba	k6eAd1	zhruba
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1450.	[number]	k4	1450.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
Norska	Norsko	k1gNnSc2	Norsko
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Stopy	stopa	k1gFnPc1	stopa
po	po	k7c6	po
prvním	první	k4xOgNnSc6	první
lidském	lidský	k2eAgNnSc6d1	lidské
osídlení	osídlení	k1gNnSc6	osídlení
archeologové	archeolog	k1gMnPc1	archeolog
dávají	dávat	k5eAaImIp3nP	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
kulturou	kultura	k1gFnSc7	kultura
zvanou	zvaný	k2eAgFnSc7d1	zvaná
Ahrensburgien	Ahrensburgien	k1gInSc4	Ahrensburgien
(	(	kIx(	(
<g/>
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
po	po	k7c6	po
vesnici	vesnice	k1gFnSc6	vesnice
Ahrensburg	Ahrensburg	k1gInSc4	Ahrensburg
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
25	[number]	k4	25
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Hamburku	Hamburk	k1gInSc2	Hamburk
<g/>
,	,	kIx,	,
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
státě	stát	k1gInSc6	stát
Šlesvicko-Holštýnsko	Šlesvicko-Holštýnsko	k1gNnSc1	Šlesvicko-Holštýnsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kultura	kultura	k1gFnSc1	kultura
je	být	k5eAaImIp3nS	být
lokalizována	lokalizovat	k5eAaBmNgFnS	lokalizovat
do	do	k7c2	do
severoevropských	severoevropský	k2eAgFnPc2d1	severoevropská
nížin	nížina	k1gFnPc2	nížina
v	v	k7c6	v
období	období	k1gNnSc6	období
9.	[number]	k4	9.
-	-	kIx~	-
7.	[number]	k4	7.
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
Příslušníci	příslušník	k1gMnPc1	příslušník
této	tento	k3xDgFnSc2	tento
kultury	kultura	k1gFnSc2	kultura
byly	být	k5eAaImAgFnP	být
především	především	k9	především
lovci	lovec	k1gMnPc1	lovec
sobů	sob	k1gMnPc2	sob
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
ve	v	k7c6	v
stanech	stan	k1gInPc6	stan
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mírně	mírně	k6eAd1	mírně
zahloubených	zahloubený	k2eAgFnPc6d1	zahloubená
chatách	chata	k1gFnPc6	chata
<g/>
.	.	kIx.	.
</s>
<s>
Užívali	užívat	k5eAaImAgMnP	užívat
štípané	štípaný	k2eAgInPc4d1	štípaný
hroty	hrot	k1gInPc4	hrot
s	s	k7c7	s
řapíkem	řapík	k1gInSc7	řapík
<g/>
,	,	kIx,	,
oštěpy	oštěp	k1gInPc7	oštěp
a	a	k8xC	a
luky	luk	k1gInPc7	luk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
nejstarší	starý	k2eAgFnPc1d3	nejstarší
stopy	stopa	k1gFnPc1	stopa
lidského	lidský	k2eAgNnSc2d1	lidské
osídlení	osídlení	k1gNnSc2	osídlení
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
po	po	k7c6	po
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgNnSc1d1	ledové
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
11 000	[number]	k4	11 000
a	a	k8xC	a
8 000	[number]	k4	8 000
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
ledovec	ledovec	k1gInSc1	ledovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejcennějším	cenný	k2eAgInPc3d3	nejcennější
nálezům	nález	k1gInPc3	nález
patří	patřit	k5eAaImIp3nS	patřit
ty	ten	k3xDgInPc4	ten
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
okolo	okolo	k7c2	okolo
5000	[number]	k4	5000
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
z	z	k7c2	z
Finnmarku	Finnmark	k1gInSc2	Finnmark
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Alta	Alt	k1gInSc2	Alt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
pozoruhodné	pozoruhodný	k2eAgFnPc1d1	pozoruhodná
skalní	skalní	k2eAgFnPc1d1	skalní
rytiny	rytina	k1gFnPc1	rytina
<g/>
,	,	kIx,	,
znázorňující	znázorňující	k2eAgFnSc1d1	znázorňující
především	především	k9	především
lov	lov	k1gInSc4	lov
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
součást	součást	k1gFnSc1	součást
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
nálezy	nález	k1gInPc7	nález
někdy	někdy	k6eAd1	někdy
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
kultuře	kultura	k1gFnSc6	kultura
Komsa	Koms	k1gMnSc2	Koms
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
centrum	centrum	k1gNnSc1	centrum
nálezů	nález	k1gInPc2	nález
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rogalandu	Rogaland	k1gInSc6	Rogaland
(	(	kIx(	(
<g/>
kultura	kultura	k1gFnSc1	kultura
Fosna	Fosna	k1gFnSc1	Fosna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
těchto	tento	k3xDgInPc2	tento
nálezů	nález	k1gInPc2	nález
vedla	vést	k5eAaImAgFnS	vést
dlouho	dlouho	k6eAd1	dlouho
k	k	k7c3	k
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
různé	různý	k2eAgFnPc4d1	různá
kultury	kultura	k1gFnPc4	kultura
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
od	od	k7c2	od
70.	[number]	k4	70.
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
zpochybňováno	zpochybňován	k2eAgNnSc1d1	zpochybňováno
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
většina	většina	k1gFnSc1	většina
historiků	historik	k1gMnPc2	historik
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jižní	jižní	k2eAgNnSc1d1	jižní
centrum	centrum	k1gNnSc1	centrum
bylo	být	k5eAaImAgNnS	být
starší	starý	k2eAgMnSc1d2	starší
a	a	k8xC	a
sever	sever	k1gInSc1	sever
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
centra	centrum	k1gNnSc2	centrum
osídlen	osídlit	k5eAaPmNgMnS	osídlit
<g/>
,	,	kIx,	,
pochodem	pochod	k1gInSc7	pochod
po	po	k7c6	po
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
Alty	alt	k1gInPc1	alt
jsou	být	k5eAaImIp3nP	být
nejrozsáhlejšími	rozsáhlý	k2eAgFnPc7d3	nejrozsáhlejší
skalními	skalní	k2eAgFnPc7d1	skalní
kresbami	kresba	k1gFnPc7	kresba
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Prozrazují	prozrazovat	k5eAaImIp3nP	prozrazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgNnSc4d1	kamenné
byli	být	k5eAaImAgMnP	být
především	především	k9	především
lovci	lovec	k1gMnPc1	lovec
a	a	k8xC	a
rybáři	rybář	k1gMnPc1	rybář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
3000	[number]	k4	3000
až	až	k9	až
2500	[number]	k4	2500
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
východního	východní	k2eAgNnSc2d1	východní
Norska	Norsko	k1gNnSc2	Norsko
noví	nový	k2eAgMnPc1d1	nový
osadníci	osadník	k1gMnPc1	osadník
<g/>
,	,	kIx,	,
řazení	řazený	k2eAgMnPc1d1	řazený
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
kultuře	kultura	k1gFnSc3	kultura
se	s	k7c7	s
šňůrovou	šňůrový	k2eAgFnSc7d1	šňůrová
keramikou	keramika	k1gFnSc7	keramika
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
indoevropští	indoevropský	k2eAgMnPc1d1	indoevropský
zemědělci	zemědělec	k1gMnPc1	zemědělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
již	již	k6eAd1	již
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
obilí	obilí	k1gNnSc4	obilí
a	a	k8xC	a
chovali	chovat	k5eAaImAgMnP	chovat
krávy	kráva	k1gFnPc4	kráva
a	a	k8xC	a
ovce	ovce	k1gFnPc4	ovce
<g/>
.	.	kIx.	.
</s>
<s>
Lov	lov	k1gInSc1	lov
a	a	k8xC	a
rybolov	rybolov	k1gInSc1	rybolov
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
až	až	k6eAd1	až
sekundárním	sekundární	k2eAgInSc7d1	sekundární
zdrojem	zdroj	k1gInSc7	zdroj
obživy	obživa	k1gFnSc2	obživa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenán	k2eAgFnPc4d1	zaznamenána
stopy	stopa	k1gFnPc4	stopa
po	po	k7c4	po
užití	užití	k1gNnSc4	užití
bronzu	bronz	k1gInSc2	bronz
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bronzové	bronzový	k2eAgInPc4d1	bronzový
nástroje	nástroj	k1gInPc4	nástroj
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
omezovaly	omezovat	k5eAaImAgFnP	omezovat
jen	jen	k9	jen
na	na	k7c4	na
náčelníky	náčelník	k1gMnPc4	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
skalních	skalní	k2eAgFnPc6d1	skalní
rytinách	rytina	k1gFnPc6	rytina
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
nové	nový	k2eAgInPc1d1	nový
motivy	motiv	k1gInPc1	motiv
<g/>
,	,	kIx,	,
především	především	k9	především
sluneční	sluneční	k2eAgInSc1d1	sluneční
kotouč	kotouč	k1gInSc1	kotouč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
patrně	patrně	k6eAd1	patrně
uctíván	uctívat	k5eAaImNgMnS	uctívat
<g/>
.	.	kIx.	.
</s>
<s>
Výrazným	výrazný	k2eAgInSc7d1	výrazný
motivem	motiv	k1gInSc7	motiv
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
typu	typ	k1gInSc2	typ
hjortspring	hjortspring	k1gInSc1	hjortspring
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vztyčené	vztyčený	k2eAgInPc1d1	vztyčený
kamenné	kamenný	k2eAgInPc1d1	kamenný
monumenty	monument	k1gInPc1	monument
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnSc2d1	železná
(	(	kIx(	(
<g/>
posledních	poslední	k2eAgNnPc2d1	poslední
500	[number]	k4	500
let	léto	k1gNnPc2	léto
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
zřejmě	zřejmě	k6eAd1	zřejmě
k	k	k7c3	k
populační	populační	k2eAgFnSc3d1	populační
krizi	krize	k1gFnSc3	krize
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
archeologové	archeolog	k1gMnPc1	archeolog
nacházejí	nacházet	k5eAaImIp3nP	nacházet
jen	jen	k9	jen
málo	málo	k4c4	málo
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
<g/>
.	.	kIx.	.
</s>
<s>
Oživení	oživení	k1gNnSc1	oživení
nastalo	nastat	k5eAaPmAgNnS	nastat
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
začátku	začátek	k1gInSc6	začátek
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Norska	Norsko	k1gNnSc2	Norsko
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
římskou	římský	k2eAgFnSc7d1	římská
Galií	Galie	k1gFnSc7	Galie
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
asi	asi	k9	asi
70	[number]	k4	70
římských	římský	k2eAgInPc2d1	římský
bronzových	bronzový	k2eAgInPc2d1	bronzový
kotlů	kotel	k1gInPc2	kotel
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
používaných	používaný	k2eAgNnPc2d1	používané
jako	jako	k8xC	jako
pohřební	pohřební	k2eAgFnPc1d1	pohřební
urny	urna	k1gFnPc1	urna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
kontinentu	kontinent	k1gInSc2	kontinent
se	se	k3xPyFc4	se
také	také	k9	také
do	do	k7c2	do
Norska	Norsko	k1gNnSc2	Norsko
dostalo	dostat	k5eAaPmAgNnS	dostat
patrně	patrně	k6eAd1	patrně
runové	runový	k2eAgNnSc4d1	runové
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
norský	norský	k2eAgInSc1d1	norský
runový	runový	k2eAgInSc1d1	runový
nápis	nápis	k1gInSc1	nápis
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
3.	[number]	k4	3.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejstarším	starý	k2eAgNnPc3d3	nejstarší
užívaným	užívaný	k2eAgNnPc3d1	užívané
slovům	slovo	k1gNnPc3	slovo
patří	patřit	k5eAaImIp3nS	patřit
"	"	kIx"	"
<g/>
vik	vika	k1gFnPc2	vika
<g/>
"	"	kIx"	"
označující	označující	k2eAgInSc1d1	označující
fjord	fjord	k1gInSc1	fjord
či	či	k8xC	či
záliv	záliv	k1gInSc1	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
také	také	k9	také
kořenem	kořen	k1gInSc7	kořen
pojmu	pojem	k1gInSc2	pojem
Viking	Viking	k1gMnSc1	Viking
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Vikinzích	Viking	k1gMnPc6	Viking
historici	historik	k1gMnPc1	historik
hovoří	hovořit	k5eAaImIp3nP	hovořit
od	od	k7c2	od
8.	[number]	k4	8.
století	století	k1gNnPc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začaly	začít	k5eAaPmAgInP	začít
jejich	jejich	k3xOp3gInPc1	jejich
výboje	výboj	k1gInPc1	výboj
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
mezník	mezník	k1gInSc4	mezník
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
pokládáno	pokládán	k2eAgNnSc4d1	pokládáno
vikingské	vikingský	k2eAgNnSc4d1	vikingské
přepadení	přepadení	k1gNnSc4	přepadení
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Lindisfarne	Lindisfarn	k1gInSc5	Lindisfarn
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Anglie	Anglie	k1gFnSc2	Anglie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
793	[number]	k4	793
<g/>
.	.	kIx.	.
</s>
<s>
Vikingové	Viking	k1gMnPc1	Viking
pak	pak	k6eAd1	pak
pronikli	proniknout	k5eAaPmAgMnP	proniknout
takřka	takřka	k6eAd1	takřka
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
částí	část	k1gFnPc2	část
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Vikingové	Viking	k1gMnPc1	Viking
z	z	k7c2	z
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Norska	Norsko	k1gNnSc2	Norsko
byli	být	k5eAaImAgMnP	být
nejvíce	hodně	k6eAd3	hodně
aktivní	aktivní	k2eAgMnPc1d1	aktivní
na	na	k7c6	na
britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
východních	východní	k2eAgInPc6d1	východní
ostrovech	ostrov	k1gInPc6	ostrov
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnPc1d1	dnešní
irská	irský	k2eAgNnPc1d1	irské
města	město	k1gNnPc1	město
Dublin	Dublin	k1gInSc1	Dublin
<g/>
,	,	kIx,	,
Limerick	Limerick	k1gMnSc1	Limerick
a	a	k8xC	a
Waterford	Waterford	k1gMnSc1	Waterford
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
právě	právě	k9	právě
osadníky	osadník	k1gMnPc4	osadník
z	z	k7c2	z
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Norští	norský	k2eAgMnPc1d1	norský
Vikingové	Viking	k1gMnPc1	Viking
v	v	k7c6	v
9.	[number]	k4	9.
století	století	k1gNnSc2	století
osídlili	osídlit	k5eAaPmAgMnP	osídlit
také	také	k9	také
Faerské	Faerský	k2eAgInPc4d1	Faerský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Orkneje	Orkneje	k1gFnPc4	Orkneje
a	a	k8xC	a
Island	Island	k1gInSc4	Island
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
985	[number]	k4	985
založili	založit	k5eAaPmAgMnP	založit
první	první	k4xOgFnPc4	první
osady	osada	k1gFnPc4	osada
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
norském	norský	k2eAgNnSc6d1	norské
národním	národní	k2eAgNnSc6d1	národní
povědomí	povědomí	k1gNnSc6	povědomí
je	být	k5eAaImIp3nS	být
klíčovým	klíčový	k2eAgInSc7d1	klíčový
rok	rok	k1gInSc1	rok
872	[number]	k4	872
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Harald	Harald	k1gInSc4	Harald
I.	I.	kA	I.
Krásnovlasý	krásnovlasý	k2eAgMnSc1d1	krásnovlasý
<g/>
,	,	kIx,	,
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hafrsfjordu	Hafrsfjord	k1gInSc2	Hafrsfjord
(	(	kIx(	(
<g/>
nedaleko	nedaleko	k7c2	nedaleko
Stavangeru	Stavanger	k1gInSc2	Stavanger
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
sjednotil	sjednotit	k5eAaPmAgMnS	sjednotit
většinu	většina	k1gFnSc4	většina
norského	norský	k2eAgNnSc2d1	norské
území	území	k1gNnSc2	území
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgMnSc7	první
norským	norský	k2eAgMnSc7d1	norský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
Haraldovy	Haraldův	k2eAgFnSc2d1	Haraldova
říše	říš	k1gFnSc2	říš
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Haraldovi	Harald	k1gMnSc6	Harald
se	se	k3xPyFc4	se
moci	moc	k1gFnSc2	moc
chopil	chopit	k5eAaPmAgMnS	chopit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Erik	Erika	k1gFnPc2	Erika
Krvavá	krvavý	k2eAgFnSc1d1	krvavá
sekyra	sekyra	k1gFnSc1	sekyra
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
sesazení	sesazení	k1gNnSc6	sesazení
pak	pak	k6eAd1	pak
další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
Haraldových	Haraldův	k2eAgMnPc2d1	Haraldův
synů	syn	k1gMnPc2	syn
Haakon	Haakon	k1gMnSc1	Haakon
I.	I.	kA	I.
Norský	norský	k2eAgMnSc1d1	norský
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
již	již	k6eAd1	již
začal	začít	k5eAaPmAgMnS	začít
koketovat	koketovat	k5eAaImF	koketovat
s	s	k7c7	s
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Christianizaci	christianizace	k1gFnSc4	christianizace
však	však	k9	však
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
moci	moct	k5eAaImF	moct
po	po	k7c6	po
Haakonově	Haakonův	k2eAgFnSc6d1	Haakonův
smrti	smrt	k1gFnSc6	smrt
ujali	ujmout	k5eAaPmAgMnP	ujmout
velcí	velký	k2eAgMnPc1d1	velký
ctitelé	ctitel	k1gMnPc1	ctitel
severských	severský	k2eAgFnPc2d1	severská
tradic	tradice	k1gFnPc2	tradice
Harald	Haralda	k1gFnPc2	Haralda
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Norský	norský	k2eAgMnSc1d1	norský
a	a	k8xC	a
Haakon	Haakon	k1gMnSc1	Haakon
Sigurdsson	Sigurdsson	k1gMnSc1	Sigurdsson
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
Olaf	Olaf	k1gMnSc1	Olaf
I.	I.	kA	I.
Tryggvason	Tryggvason	k1gMnSc1	Tryggvason
znovu	znovu	k6eAd1	znovu
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
prosazovat	prosazovat	k5eAaImF	prosazovat
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
násilně	násilně	k6eAd1	násilně
<g/>
.	.	kIx.	.
</s>
<s>
Postavil	postavit	k5eAaPmAgMnS	postavit
také	také	k9	také
první	první	k4xOgInSc4	první
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
995	[number]	k4	995
v	v	k7c6	v
Mosteru	Moster	k1gInSc6	Moster
<g/>
.	.	kIx.	.
</s>
<s>
Olaf	Olaf	k1gMnSc1	Olaf
I.	I.	kA	I.
Tryggvason	Tryggvason	k1gMnSc1	Tryggvason
proslul	proslout	k5eAaPmAgMnS	proslout
též	též	k9	též
jako	jako	k9	jako
vikingský	vikingský	k2eAgMnSc1d1	vikingský
dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
<g/>
,	,	kIx,	,
když	když	k8xS	když
s	s	k7c7	s
390	[number]	k4	390
loděmi	loď	k1gFnPc7	loď
napadl	napadnout	k5eAaPmAgMnS	napadnout
Anglii	Anglie	k1gFnSc4	Anglie
a	a	k8xC	a
oblehl	oblehnout	k5eAaPmAgInS	oblehnout
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
christianizačním	christianizační	k2eAgNnSc6d1	christianizační
díle	dílo	k1gNnSc6	dílo
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
jinak	jinak	k6eAd1	jinak
poměrně	poměrně	k6eAd1	poměrně
neúspěšný	úspěšný	k2eNgMnSc1d1	neúspěšný
panovník	panovník	k1gMnSc1	panovník
Olaf	Olaf	k1gMnSc1	Olaf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Norský	norský	k2eAgMnSc1d1	norský
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
prohlášený	prohlášený	k2eAgMnSc1d1	prohlášený
za	za	k7c4	za
svatého	svatý	k2eAgMnSc4d1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Knut	knuta	k1gFnPc2	knuta
Veliký	veliký	k2eAgInSc1d1	veliký
pak	pak	k6eAd1	pak
Norsko	Norsko	k1gNnSc4	Norsko
prvně	prvně	k?	prvně
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
historii	historie	k1gFnSc6	historie
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
vícenárodní	vícenárodní	k2eAgFnSc2d1	vícenárodní
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Severské	severský	k2eAgFnSc2d1	severská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jen	jen	k9	jen
nakrátko	nakrátko	k6eAd1	nakrátko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
prosadili	prosadit	k5eAaPmAgMnP	prosadit
lokální	lokální	k2eAgMnPc1d1	lokální
vůdci	vůdce	k1gMnPc1	vůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
norského	norský	k2eAgInSc2d1	norský
státu	stát	k1gInSc2	stát
bylo	být	k5eAaImAgNnS	být
nástupnictví	nástupnictví	k1gNnSc1	nástupnictví
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
dávala	dávat	k5eAaImAgFnS	dávat
právo	právo	k1gNnSc4	právo
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
všem	všecek	k3xTgMnPc3	všecek
královým	králův	k2eAgMnPc3d1	králův
synům	syn	k1gMnPc3	syn
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
bojům	boj	k1gInPc3	boj
a	a	k8xC	a
také	také	k9	také
posilovalo	posilovat	k5eAaImAgNnS	posilovat
církev	církev	k1gFnSc4	církev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
činila	činit	k5eAaImAgFnS	činit
nárok	nárok	k1gInSc4	nárok
do	do	k7c2	do
otázky	otázka	k1gFnSc2	otázka
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Sérii	série	k1gFnSc4	série
norských	norský	k2eAgFnPc2d1	norská
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
ukončil	ukončit	k5eAaPmAgMnS	ukončit
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1217	[number]	k4	1217
Haakon	Haakona	k1gFnPc2	Haakona
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Norský	norský	k2eAgMnSc1d1	norský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nastolil	nastolit	k5eAaPmAgMnS	nastolit
jasný	jasný	k2eAgInSc4d1	jasný
nástupnický	nástupnický	k2eAgInSc4d1	nástupnický
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
konsolidace	konsolidace	k1gFnSc1	konsolidace
monarchie	monarchie	k1gFnSc2	monarchie
a	a	k8xC	a
definitivního	definitivní	k2eAgNnSc2d1	definitivní
sjednocení	sjednocení	k1gNnSc2	sjednocení
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
jeho	jeho	k3xOp3gNnSc2	jeho
panování	panování	k1gNnSc2	panování
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
období	období	k1gNnSc4	období
středověkého	středověký	k2eAgNnSc2d1	středověké
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
i	i	k9	i
ve	v	k7c6	v
14.	[number]	k4	14.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
třeba	třeba	k6eAd1	třeba
epidemie	epidemie	k1gFnSc1	epidemie
moru	mor	k1gInSc2	mor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1349	[number]	k4	1349
vyhubila	vyhubit	k5eAaPmAgFnS	vyhubit
třetinu	třetina	k1gFnSc4	třetina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Společenská	společenský	k2eAgFnSc1d1	společenská
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
středověku	středověk	k1gInSc2	středověk
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
typicky	typicky	k6eAd1	typicky
feudální	feudální	k2eAgFnSc1d1	feudální
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
skandinávských	skandinávský	k2eAgFnPc6d1	skandinávská
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
šlechta	šlechta	k1gFnSc1	šlechta
sice	sice	k8xC	sice
získali	získat	k5eAaPmAgMnP	získat
mnoho	mnoho	k4c4	mnoho
pozemků	pozemek	k1gInPc2	pozemek
a	a	k8xC	a
původně	původně	k6eAd1	původně
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
zemědělci	zemědělec	k1gMnPc1	zemědělec
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
jejich	jejich	k3xOp3gMnPc1	jejich
nájemci	nájemce	k1gMnPc1	nájemce
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
svobodnějším	svobodný	k2eAgNnSc6d2	svobodnější
postavení	postavení	k1gNnSc6	postavení
než	než	k8xS	než
poddaní	poddaný	k1gMnPc1	poddaný
na	na	k7c6	na
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nízké	nízký	k2eAgFnSc3d1	nízká
hustotě	hustota	k1gFnSc3	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
(	(	kIx(	(
<g/>
posilované	posilovaný	k2eAgNnSc1d1	posilované
i	i	k8xC	i
decimací	decimace	k1gFnSc7	decimace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
epidemiemi	epidemie	k1gFnPc7	epidemie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
půdy	půda	k1gFnSc2	půda
stále	stále	k6eAd1	stále
dostatek	dostatek	k1gInSc4	dostatek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
činilo	činit	k5eAaImAgNnS	činit
vyjednávací	vyjednávací	k2eAgFnSc4d1	vyjednávací
pozici	pozice	k1gFnSc4	pozice
nájemců	nájemce	k1gMnPc2	nájemce
mnohem	mnohem	k6eAd1	mnohem
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rys	rys	k1gInSc1	rys
zformoval	zformovat	k5eAaPmAgInS	zformovat
rovnostářskou	rovnostářský	k2eAgFnSc4d1	rovnostářská
tradici	tradice	k1gFnSc4	tradice
Norska	Norsko	k1gNnSc2	Norsko
(	(	kIx(	(
<g/>
i	i	k9	i
celé	celý	k2eAgFnSc2d1	celá
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
jisté	jistý	k2eAgFnSc6d1	jistá
transformaci	transformace	k1gFnSc6	transformace
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
začal	začít	k5eAaPmAgInS	začít
ve	v	k7c4	v
14.	[number]	k4	14.
století	století	k1gNnPc2	století
sehrávat	sehrávat	k5eAaImF	sehrávat
hanzovní	hanzovní	k2eAgInSc4d1	hanzovní
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
centrem	centr	k1gInSc7	centr
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Bergen	Bergen	k1gInSc1	Bergen
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
byl	být	k5eAaImAgInS	být
Bergen	Bergen	k1gInSc1	Bergen
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
tak	tak	k6eAd1	tak
trochu	trochu	k6eAd1	trochu
"	"	kIx"	"
<g/>
státem	stát	k1gInSc7	stát
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1380	[number]	k4	1380
Olaf	Olaf	k1gInSc1	Olaf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Dánský	dánský	k2eAgMnSc1d1	dánský
spojil	spojit	k5eAaPmAgMnS	spojit
Norsko	Norsko	k1gNnSc4	Norsko
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc4	Dánsko
do	do	k7c2	do
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1397	[number]	k4	1397
královna	královna	k1gFnSc1	královna
Markéta	Markéta	k1gFnSc1	Markéta
I.	I.	kA	I.
připojila	připojit	k5eAaPmAgFnS	připojit
i	i	k9	i
Švédsko	Švédsko	k1gNnSc4	Švédsko
(	(	kIx(	(
<g/>
sňatkovou	sňatkový	k2eAgFnSc7d1	sňatková
politikou	politika	k1gFnSc7	politika
<g/>
)	)	kIx)	)
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Kalmarská	Kalmarský	k2eAgFnSc1d1	Kalmarská
unie	unie	k1gFnSc1	unie
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
této	tento	k3xDgFnSc2	tento
říše	říš	k1gFnSc2	říš
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
a	a	k8xC	a
Norsko	Norsko	k1gNnSc4	Norsko
tak	tak	k9	tak
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
poněkud	poněkud	k6eAd1	poněkud
periferizováno	periferizován	k2eAgNnSc1d1	periferizován
<g/>
.	.	kIx.	.
</s>
<s>
Nadvláda	nadvláda	k1gFnSc1	nadvláda
Dánů	Dán	k1gMnPc2	Dán
trvala	trvat	k5eAaImAgFnS	trvat
dalších	další	k2eAgNnPc2d1	další
434	[number]	k4	434
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
norští	norský	k2eAgMnPc1d1	norský
nacionalisté	nacionalista	k1gMnPc1	nacionalista
v	v	k7c4	v
19.	[number]	k4	19.
století	století	k1gNnPc2	století
hovořili	hovořit	k5eAaImAgMnP	hovořit
o	o	k7c4	o
"	"	kIx"	"
<g/>
čtyřsetleté	čtyřsetletý	k2eAgFnPc4d1	čtyřsetletá
noci	noc	k1gFnPc4	noc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
nárůstu	nárůst	k1gInSc6	nárůst
daňového	daňový	k2eAgNnSc2d1	daňové
zatížení	zatížení	k1gNnSc2	zatížení
Norů	Nor	k1gMnPc2	Nor
také	také	k9	také
již	již	k6eAd1	již
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
unie	unie	k1gFnSc2	unie
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
proti	proti	k7c3	proti
dánské	dánský	k2eAgFnSc3d1	dánská
nadvládě	nadvláda	k1gFnSc3	nadvláda
první	první	k4xOgInSc1	první
povstání	povstání	k1gNnPc2	povstání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspělo	uspět	k5eNaPmAgNnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomicky	ekonomicky	k6eAd1	ekonomicky
Norsko	Norsko	k1gNnSc1	Norsko
pod	pod	k7c7	pod
Dány	Dán	k1gMnPc7	Dán
skutečně	skutečně	k6eAd1	skutečně
chřadlo	chřadnout	k5eAaImAgNnS	chřadnout
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
ještě	ještě	k6eAd1	ještě
zhoršily	zhoršit	k5eAaPmAgFnP	zhoršit
silné	silný	k2eAgFnPc4d1	silná
pirátské	pirátský	k2eAgFnPc4d1	pirátská
ataky	ataka	k1gFnPc4	ataka
<g/>
.	.	kIx.	.
</s>
<s>
Periferizace	Periferizace	k1gFnSc1	Periferizace
ještě	ještě	k6eAd1	ještě
zesílila	zesílit	k5eAaPmAgFnS	zesílit
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
oldenburské	oldenburský	k2eAgFnSc2d1	oldenburský
dynastie	dynastie	k1gFnSc2	dynastie
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Norové	Norové	k2eAgMnSc2d1	Norové
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1502	[number]	k4	1502
pokusili	pokusit	k5eAaPmAgMnP	pokusit
o	o	k7c4	o
vzpouru	vzpoura	k1gFnSc4	vzpoura
vedenou	vedený	k2eAgFnSc4d1	vedená
šlechticem	šlechtic	k1gMnSc7	šlechtic
Knutem	Knut	k1gMnSc7	Knut
Alvssonem	Alvsson	k1gMnSc7	Alvsson
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
rozdrcena	rozdrcen	k2eAgFnSc1d1	rozdrcena
<g/>
.	.	kIx.	.
</s>
<s>
Norové	Nor	k1gMnPc1	Nor
si	se	k3xPyFc3	se
z	z	k7c2	z
Oldeburků	Oldeburk	k1gInPc2	Oldeburk
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
jen	jen	k6eAd1	jen
krále	král	k1gMnSc2	král
Kristiána	Kristián	k1gMnSc2	Kristián
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Dánského	dánský	k2eAgMnSc4d1	dánský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
našel	najít	k5eAaPmAgInS	najít
milenku	milenka	k1gFnSc4	milenka
a	a	k8xC	a
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
tam	tam	k6eAd1	tam
žil	žít	k5eAaImAgMnS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Kristián	Kristián	k1gMnSc1	Kristián
však	však	k9	však
začal	začít	k5eAaPmAgMnS	začít
čelit	čelit	k5eAaImF	čelit
švédské	švédský	k2eAgFnSc3d1	švédská
vzpouře	vzpoura	k1gFnSc3	vzpoura
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc4	Švédsko
také	také	k9	také
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Norové	Norové	k2eAgInSc4d1	Norové
se	se	k3xPyFc4	se
švédsko-dánského	švédskoánský	k2eAgInSc2d1	švédsko-dánský
sporu	spor	k1gInSc2	spor
neúčastnili	účastnit	k5eNaImAgMnP	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
však	však	k9	však
Švédové	Švéd	k1gMnPc1	Švéd
získali	získat	k5eAaPmAgMnP	získat
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
pokusili	pokusit	k5eAaPmAgMnP	pokusit
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
o	o	k7c4	o
vzpouru	vzpoura	k1gFnSc4	vzpoura
i	i	k9	i
oni	onen	k3xDgMnPc1	onen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znovu	znovu	k6eAd1	znovu
byli	být	k5eAaImAgMnP	být
neúspěšní	úspěšný	k2eNgMnPc1d1	neúspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
stát	stát	k1gInSc1	stát
bez	bez	k7c2	bez
Švédů	Švéd	k1gMnPc2	Švéd
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
Dánsko-Norsko	Dánsko-Norsko	k1gNnSc4	Dánsko-Norsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
dánský	dánský	k2eAgMnSc1d1	dánský
panovník	panovník	k1gMnSc1	panovník
přijal	přijmout	k5eAaPmAgMnS	přijmout
roku	rok	k1gInSc2	rok
1536	[number]	k4	1536
luterství	luterství	k1gNnSc1	luterství
jako	jako	k8xS	jako
státní	státní	k2eAgNnPc1d1	státní
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
arcibiskupství	arcibiskupství	k1gNnSc1	arcibiskupství
v	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
Trondheimu	Trondheim	k1gInSc6	Trondheim
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ještě	ještě	k6eAd1	ještě
snížilo	snížit	k5eAaPmAgNnS	snížit
míru	míra	k1gFnSc4	míra
norské	norský	k2eAgFnSc2d1	norská
samostatnosti	samostatnost	k1gFnSc2	samostatnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
církevní	církevní	k2eAgInPc4d1	církevní
příjmy	příjem	k1gInPc4	příjem
z	z	k7c2	z
pozemků	pozemek	k1gInPc2	pozemek
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
mířily	mířit	k5eAaImAgFnP	mířit
do	do	k7c2	do
Kodaně	Kodaň	k1gFnSc2	Kodaň
<g/>
.	.	kIx.	.
</s>
<s>
Pád	Pád	k1gInSc1	Pád
katolicismu	katolicismus	k1gInSc2	katolicismus
také	také	k9	také
znamenal	znamenat	k5eAaImAgInS	znamenat
konec	konec	k1gInSc1	konec
poutí	poutí	k1gNnSc2	poutí
ke	k	k7c3	k
svatyni	svatyně	k1gFnSc3	svatyně
sv.	sv.	kA	sv.
Olafa	Olaf	k1gMnSc2	Olaf
v	v	k7c6	v
Nidaru	Nidar	k1gMnSc6	Nidar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
snížilo	snížit	k5eAaPmAgNnS	snížit
kulturní	kulturní	k2eAgFnSc4d1	kulturní
výměnu	výměna	k1gFnSc4	výměna
Norska	Norsko	k1gNnSc2	Norsko
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ústupkem	ústupek	k1gInSc7	ústupek
Dánů	Dán	k1gMnPc2	Dán
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
roku	rok	k1gInSc2	rok
1661	[number]	k4	1661
formálně	formálně	k6eAd1	formálně
obnovili	obnovit	k5eAaPmAgMnP	obnovit
norské	norský	k2eAgNnSc4d1	norské
království	království	k1gNnSc4	království
(	(	kIx(	(
<g/>
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Norové	Nor	k1gMnPc1	Nor
neustále	neustále	k6eAd1	neustále
platili	platit	k5eAaImAgMnP	platit
za	za	k7c2	za
dánské	dánský	k2eAgFnSc2d1	dánská
katastrofální	katastrofální	k2eAgFnSc2d1	katastrofální
porážky	porážka	k1gFnSc2	porážka
od	od	k7c2	od
Švédů	Švéd	k1gMnPc2	Švéd
ztrátou	ztráta	k1gFnSc7	ztráta
svých	svůj	k3xOyFgNnPc2	svůj
tradičních	tradiční	k2eAgNnPc2d1	tradiční
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
Bå	Bå	k1gFnSc1	Bå
<g/>
,	,	kIx,	,
Jemtland	Jemtland	k1gInSc1	Jemtland
<g/>
,	,	kIx,	,
Herjedalen	Herjedalen	k2eAgInSc1d1	Herjedalen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
nedobré	dobrý	k2eNgFnSc6d1	nedobrá
kondici	kondice	k1gFnSc6	kondice
Norska	Norsko	k1gNnSc2	Norsko
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
hladomory	hladomor	k1gMnPc4	hladomor
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1695	[number]	k4	1695
<g/>
–	–	k?	–
<g/>
1696	[number]	k4	1696
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zabily	zabít	k5eAaPmAgFnP	zabít
asi	asi	k9	asi
10	[number]	k4	10
procent	procento	k1gNnPc2	procento
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkým	velký	k2eAgInSc7d1	velký
přelomem	přelom	k1gInSc7	přelom
v	v	k7c6	v
norských	norský	k2eAgFnPc6d1	norská
dějinách	dějiny	k1gFnPc6	dějiny
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
napoleonské	napoleonský	k2eAgFnPc1d1	napoleonská
války	válka	k1gFnPc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
dánský	dánský	k2eAgMnSc1d1	dánský
král	král	k1gMnSc1	král
nešťastně	šťastně	k6eNd1	šťastně
zapletl	zaplést	k5eAaPmAgMnS	zaplést
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc1	Dánsko
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
pokořeno	pokořit	k5eAaPmNgNnS	pokořit
Brity	Brit	k1gMnPc7	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Norové	Nor	k1gMnPc1	Nor
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
využili	využít	k5eAaPmAgMnP	využít
a	a	k8xC	a
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
17.	[number]	k4	17.
května	květen	k1gInSc2	květen
1814	[number]	k4	1814
přijali	přijmout	k5eAaPmAgMnP	přijmout
ústavu	ústava	k1gFnSc4	ústava
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
a	a	k8xC	a
francouzském	francouzský	k2eAgInSc6d1	francouzský
vzoru	vzor	k1gInSc6	vzor
<g/>
.	.	kIx.	.
17.	[number]	k4	17.
květen	květen	k1gInSc4	květen
(	(	kIx(	(
<g/>
Syttende	Syttend	k1gMnSc5	Syttend
Mai	Mai	k1gMnSc5	Mai
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
pro	pro	k7c4	pro
Nory	Nor	k1gMnPc4	Nor
velký	velký	k2eAgInSc4d1	velký
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
<g/>
,	,	kIx,	,
Den	den	k1gInSc4	den
norské	norský	k2eAgFnSc2d1	norská
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Jenomže	jenomže	k8xC	jenomže
události	událost	k1gFnPc1	událost
nezávislosti	nezávislost	k1gFnSc2	nezávislost
nepřály	přát	k5eNaImAgFnP	přát
<g/>
.	.	kIx.	.
</s>
<s>
Švédové	Švéd	k1gMnPc1	Švéd
<g/>
,	,	kIx,	,
spojenci	spojenec	k1gMnPc1	spojenec
Britů	Brit	k1gMnPc2	Brit
v	v	k7c6	v
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
získali	získat	k5eAaPmAgMnP	získat
podle	podle	k7c2	podle
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
Kielu	Kiel	k1gInSc2	Kiel
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
smlouvy	smlouva	k1gFnSc2	smlouva
Norsko	Norsko	k1gNnSc4	Norsko
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Dánska	Dánsko	k1gNnSc2	Dánsko
ztrácelo	ztrácet	k5eAaImAgNnS	ztrácet
své	své	k1gNnSc4	své
tradiční	tradiční	k2eAgFnSc2d1	tradiční
zámořské	zámořský	k2eAgFnSc2d1	zámořská
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Grónsko	Grónsko	k1gNnSc1	Grónsko
a	a	k8xC	a
Faerské	Faerský	k2eAgInPc1d1	Faerský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
Norům	Nor	k1gMnPc3	Nor
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vyčerpaní	vyčerpaný	k2eAgMnPc1d1	vyčerpaný
britskou	britský	k2eAgFnSc7d1	britská
námořní	námořní	k2eAgFnSc7d1	námořní
blokádou	blokáda	k1gFnSc7	blokáda
se	se	k3xPyFc4	se
do	do	k7c2	do
vojenského	vojenský	k2eAgInSc2d1	vojenský
konfliktu	konflikt	k1gInSc2	konflikt
se	se	k3xPyFc4	se
Švédy	švéda	k1gFnPc4	švéda
pouštět	pouštět	k5eAaImF	pouštět
nechtěli	chtít	k5eNaImAgMnP	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
složitých	složitý	k2eAgNnPc6d1	složité
politických	politický	k2eAgNnPc6d1	politické
jednáních	jednání	k1gNnPc6	jednání
4.	[number]	k4	4.
listopadu	listopad	k1gInSc6	listopad
1814	[number]	k4	1814
norský	norský	k2eAgInSc1d1	norský
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
Storting	Storting	k1gInSc1	Storting
<g/>
)	)	kIx)	)
zvolil	zvolit	k5eAaPmAgMnS	zvolit
švédského	švédský	k2eAgMnSc4d1	švédský
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc2	Karel
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
i	i	k8xC	i
králem	král	k1gMnSc7	král
norským	norský	k2eAgMnSc7d1	norský
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
posvětil	posvětit	k5eAaPmAgMnS	posvětit
novou	nový	k2eAgFnSc4d1	nová
personální	personální	k2eAgFnSc4d1	personální
unii	unie	k1gFnSc4	unie
<g/>
.	.	kIx.	.
</s>
<s>
Panovník	panovník	k1gMnSc1	panovník
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Norům	Nor	k1gMnPc3	Nor
ponechal	ponechat	k5eAaPmAgInS	ponechat
jejich	jejich	k3xOp3gFnSc4	jejich
liberální	liberální	k2eAgFnSc4d1	liberální
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
i	i	k8xC	i
řadu	řada	k1gFnSc4	řada
správních	správní	k2eAgFnPc2d1	správní
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
politického	politický	k2eAgInSc2d1	politický
kompromisu	kompromis	k1gInSc2	kompromis
<g/>
,	,	kIx,	,
neunáhlenost	neunáhlenost	k1gFnSc1	neunáhlenost
a	a	k8xC	a
mírnost	mírnost	k1gFnSc1	mírnost
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
staly	stát	k5eAaPmAgInP	stát
trvalejším	trvalý	k2eAgInSc7d2	trvalejší
rysem	rys	k1gInSc7	rys
norské	norský	k2eAgFnSc2d1	norská
politické	politický	k2eAgFnSc2d1	politická
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
30.	[number]	k4	30.
let	léto	k1gNnPc2	léto
19.	[number]	k4	19.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
moderní	moderní	k2eAgInSc1d1	moderní
norský	norský	k2eAgInSc1d1	norský
nacionalismus	nacionalismus	k1gInSc1	nacionalismus
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
Norové	Nor	k1gMnPc1	Nor
vymezovali	vymezovat	k5eAaImAgMnP	vymezovat
svou	svůj	k3xOyFgFnSc4	svůj
identitu	identita	k1gFnSc4	identita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
říše	říš	k1gFnSc2	říš
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc4	důraz
kladli	klást	k5eAaImAgMnP	klást
na	na	k7c4	na
jazyk	jazyk	k1gInSc4	jazyk
(	(	kIx(	(
<g/>
výsledkem	výsledek	k1gInSc7	výsledek
jazykových	jazykový	k2eAgFnPc2d1	jazyková
třenic	třenice	k1gFnPc2	třenice
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
dvě	dva	k4xCgFnPc1	dva
spisovné	spisovný	k2eAgFnPc1d1	spisovná
norštiny	norština	k1gFnPc1	norština
-	-	kIx~	-
bokmå	bokmå	k?	bokmå
a	a	k8xC	a
nynorsk	nynorsk	k1gInSc1	nynorsk
<g/>
)	)	kIx)	)
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
tendence	tendence	k1gFnPc1	tendence
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
nelíbily	líbit	k5eNaImAgFnP	líbit
novému	nový	k2eAgMnSc3d1	nový
králi	král	k1gMnSc3	král
Karlovi	Karel	k1gMnSc3	Karel
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vůči	vůči	k7c3	vůči
národnímu	národní	k2eAgNnSc3d1	národní
hnutí	hnutí	k1gNnSc3	hnutí
začal	začít	k5eAaPmAgInS	začít
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
represe	represe	k1gFnPc4	represe
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc1	který
ovšem	ovšem	k9	ovšem
nešlo	jít	k5eNaImAgNnS	jít
srovnat	srovnat	k5eAaPmF	srovnat
například	například	k6eAd1	například
s	s	k7c7	s
Metternichovými	Metternichův	k2eAgInPc7d1	Metternichův
postupy	postup	k1gInPc7	postup
v	v	k7c6	v
Rakouské	rakouský	k2eAgFnSc6d1	rakouská
říši	říš	k1gFnSc6	říš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Karlově	Karlův	k2eAgFnSc6d1	Karlova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
však	však	k9	však
tlak	tlak	k1gInSc1	tlak
povolil	povolit	k5eAaPmAgInS	povolit
a	a	k8xC	a
národně-liberální	národněiberální	k2eAgNnSc1d1	národně-liberální
hnutí	hnutí	k1gNnSc1	hnutí
protlačilo	protlačit	k5eAaPmAgNnS	protlačit
řadu	řada	k1gFnSc4	řada
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
žen	žena	k1gFnPc2	žena
<g/>
:	:	kIx,	:
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
získaly	získat	k5eAaPmAgFnP	získat
ženy	žena	k1gFnPc1	žena
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
dědictví	dědictví	k1gNnSc4	dědictví
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
byly	být	k5eAaImAgFnP	být
neprovdané	provdaný	k2eNgFnPc1d1	neprovdaná
ženy	žena	k1gFnPc1	žena
osvobozeny	osvobodit	k5eAaPmNgFnP	osvobodit
z	z	k7c2	z
područí	područí	k1gNnSc2	područí
otců	otec	k1gMnPc2	otec
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
také	také	k9	také
středostavovské	středostavovský	k2eAgFnPc1d1	středostavovská
ženy	žena	k1gFnPc1	žena
začaly	začít	k5eAaPmAgFnP	začít
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
jako	jako	k9	jako
učitelky	učitelka	k1gFnSc2	učitelka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
na	na	k7c4	na
evropské	evropský	k2eAgInPc4d1	evropský
poměry	poměr	k1gInPc4	poměr
časné	časný	k2eAgNnSc4d1	časné
osvobození	osvobození	k1gNnSc4	osvobození
žen	žena	k1gFnPc2	žena
dalo	dát	k5eAaPmAgNnS	dát
norské	norský	k2eAgFnPc4d1	norská
společnosti	společnost	k1gFnPc4	společnost
feministický	feministický	k2eAgInSc1d1	feministický
rys	rys	k1gInSc1	rys
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
znatelný	znatelný	k2eAgInSc1d1	znatelný
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
postupné	postupný	k2eAgFnPc1d1	postupná
reformy	reforma	k1gFnPc1	reforma
způsobily	způsobit	k5eAaPmAgFnP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Norsko	Norsko	k1gNnSc1	Norsko
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
minula	minout	k5eAaImAgFnS	minout
revoluční	revoluční	k2eAgFnSc1d1	revoluční
vlna	vlna	k1gFnSc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
příčinou	příčina	k1gFnSc7	příčina
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
byla	být	k5eAaImAgFnS	být
slabá	slabý	k2eAgFnSc1d1	slabá
měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
politiku	politika	k1gFnSc4	politika
i	i	k8xC	i
ekonomiku	ekonomika	k1gFnSc4	ekonomika
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
stále	stále	k6eAd1	stále
kontrolovaly	kontrolovat	k5eAaImAgFnP	kontrolovat
šlechtici	šlechtic	k1gMnSc3	šlechtic
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
liberálně	liberálně	k6eAd1	liberálně
naladění	naladění	k1gNnSc4	naladění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
volební	volební	k2eAgNnSc1d1	volební
právo	právo	k1gNnSc1	právo
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
na	na	k7c4	na
ženy	žena	k1gFnPc4	žena
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
až	až	k9	až
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vyvrcholil	vyvrcholit	k5eAaPmAgMnS	vyvrcholit
nenásilný	násilný	k2eNgMnSc1d1	nenásilný
a	a	k8xC	a
postupný	postupný	k2eAgInSc1d1	postupný
proces	proces	k1gInSc1	proces
demokratizace	demokratizace	k1gFnSc2	demokratizace
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
politickou	politický	k2eAgFnSc7d1	politická
postavou	postava	k1gFnSc7	postava
počátku	počátek	k1gInSc2	počátek
20.	[number]	k4	20.
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Christian	Christian	k1gMnSc1	Christian
Michelsen	Michelsna	k1gFnPc2	Michelsna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
založil	založit	k5eAaPmAgMnS	založit
stranu	strana	k1gFnSc4	strana
Samlingspartiet	Samlingspartiet	k1gMnSc1	Samlingspartiet
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
odchod	odchod	k1gInSc4	odchod
Norska	Norsko	k1gNnSc2	Norsko
ze	z	k7c2	z
švédsko-norské	švédskoorský	k2eAgFnSc2d1	švédsko-norská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
takticky	takticky	k6eAd1	takticky
na	na	k7c4	na
separaci	separace	k1gFnSc4	separace
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
král	král	k1gMnSc1	král
Oskar	Oskar	k1gMnSc1	Oskar
II	II	kA	II
<g/>
.	.	kIx.	.
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
norské	norský	k2eAgFnSc3d1	norská
části	část	k1gFnSc3	část
federace	federace	k1gFnSc2	federace
zakládat	zakládat	k5eAaImF	zakládat
vlastní	vlastní	k2eAgInPc4d1	vlastní
konzuláty	konzulát	k1gInPc4	konzulát
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
Michelsenova	Michelsenův	k2eAgFnSc1d1	Michelsenův
vláda	vláda	k1gFnSc1	vláda
na	na	k7c4	na
protest	protest	k1gInSc4	protest
podala	podat	k5eAaPmAgFnS	podat
demisi	demise	k1gFnSc3	demise
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
nového	nový	k2eAgMnSc2d1	nový
premiéra	premiér	k1gMnSc2	premiér
nejmenoval	jmenovat	k5eNaImAgInS	jmenovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
demise	demise	k1gFnSc1	demise
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
prázdným	prázdný	k2eAgNnSc7d1	prázdné
politickým	politický	k2eAgNnSc7d1	politické
gestem	gesto	k1gNnSc7	gesto
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
však	však	k9	však
Michelsen	Michelsen	k1gInSc4	Michelsen
využil	využít	k5eAaPmAgMnS	využít
a	a	k8xC	a
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
parlamentu	parlament	k1gInSc6	parlament
usnesení	usnesení	k1gNnSc4	usnesení
konstatující	konstatující	k2eAgFnSc2d1	konstatující
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
nejmenoval	jmenovat	k5eNaImAgMnS	jmenovat
novou	nový	k2eAgFnSc4d1	nová
norskou	norský	k2eAgFnSc4d1	norská
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
zpronevěřil	zpronevěřit	k5eAaPmAgMnS	zpronevěřit
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgInSc3	svůj
základnímu	základní	k2eAgInSc3d1	základní
úkolu	úkol	k1gInSc3	úkol
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
již	již	k6eAd1	již
právo	právo	k1gNnSc1	právo
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
vládnout	vládnout	k5eAaImF	vládnout
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
Norsko	Norsko	k1gNnSc1	Norsko
z	z	k7c2	z
personální	personální	k2eAgFnSc2d1	personální
unie	unie	k1gFnSc2	unie
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc4	závěr
si	se	k3xPyFc3	se
Michelsen	Michelsen	k1gInSc4	Michelsen
nechal	nechat	k5eAaPmAgInS	nechat
ještě	ještě	k9	ještě
potvrdit	potvrdit	k5eAaPmF	potvrdit
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
referendum	referendum	k1gNnSc1	referendum
v	v	k7c6	v
samostatném	samostatný	k2eAgInSc6d1	samostatný
státě	stát	k1gInSc6	stát
pak	pak	k6eAd1	pak
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Norové	Norové	k2eAgMnPc1d1	Norové
ponechají	ponechat	k5eAaPmIp3nP	ponechat
monarchistické	monarchistický	k2eAgNnSc4d1	monarchistické
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
Michelsena	Michelsen	k2eAgMnSc4d1	Michelsen
<g/>
,	,	kIx,	,
věřícího	věřící	k2eAgMnSc4d1	věřící
v	v	k7c4	v
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
post	post	k1gInSc4	post
<g/>
,	,	kIx,	,
zklamalo	zklamat	k5eAaPmAgNnS	zklamat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
to	ten	k3xDgNnSc1	ten
respektoval	respektovat	k5eAaImAgMnS	respektovat
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
vzniku	vznik	k1gInSc2	vznik
moderního	moderní	k2eAgInSc2d1	moderní
norského	norský	k2eAgInSc2d1	norský
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
7.	[number]	k4	7.
červen	červen	k1gInSc1	červen
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
formálně	formálně	k6eAd1	formálně
byla	být	k5eAaImAgFnS	být
jen	jen	k9	jen
vyměněna	vyměnit	k5eAaPmNgFnS	vyměnit
dynastie	dynastie	k1gFnSc1	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Nenásilné	násilný	k2eNgNnSc1d1	nenásilné
rozbití	rozbití	k1gNnSc1	rozbití
švédsko-norské	švédskoorský	k2eAgFnSc2d1	švédsko-norská
federace	federace	k1gFnSc2	federace
bývá	bývat	k5eAaImIp3nS	bývat
uváděno	uvádět	k5eAaImNgNnS	uvádět
jako	jako	k8xC	jako
ukázkový	ukázkový	k2eAgInSc1d1	ukázkový
příklad	příklad	k1gInSc1	příklad
takové	takový	k3xDgFnSc2	takový
možnosti	možnost	k1gFnSc2	možnost
(	(	kIx(	(
<g/>
další	další	k2eAgInSc1d1	další
byl	být	k5eAaImAgInS	být
až	až	k9	až
rozpad	rozpad	k1gInSc1	rozpad
Československa	Československo	k1gNnSc2	Československo
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
Norsko	Norsko	k1gNnSc1	Norsko
neutrální	neutrální	k2eAgNnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
bylo	být	k5eAaImAgNnS	být
přepadeno	přepaden	k2eAgNnSc1d1	přepadeno
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
(	(	kIx(	(
<g/>
o	o	k7c4	o
významný	významný	k2eAgInSc4d1	významný
přístav	přístav	k1gInSc4	přístav
Narvik	Narvika	k1gFnPc2	Narvika
na	na	k7c6	na
severu	sever	k1gInSc6	sever
bojovali	bojovat	k5eAaImAgMnP	bojovat
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Norů	Nor	k1gMnPc2	Nor
i	i	k9	i
Britové	Brit	k1gMnPc1	Brit
svou	svůj	k3xOyFgFnSc7	svůj
plnou	plný	k2eAgFnSc7d1	plná
válečnou	válečný	k2eAgFnSc7d1	válečná
silou	síla	k1gFnSc7	síla
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
vlády	vláda	k1gFnSc2	vláda
kolaborant	kolaborant	k1gMnSc1	kolaborant
Vidkun	Vidkun	k1gMnSc1	Vidkun
Quisling	quisling	k1gMnSc1	quisling
<g/>
.	.	kIx.	.
</s>
<s>
Norský	norský	k2eAgInSc1d1	norský
odboj	odboj	k1gInSc1	odboj
předvedl	předvést	k5eAaPmAgInS	předvést
několik	několik	k4yIc4	několik
výjimečných	výjimečný	k2eAgFnPc2d1	výjimečná
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bylo	být	k5eAaImAgNnS	být
zničení	zničení	k1gNnSc4	zničení
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
Norsk	Norsk	k1gInSc1	Norsk
Hydro	hydra	k1gFnSc5	hydra
a	a	k8xC	a
zásob	zásobit	k5eAaPmRp2nS	zásobit
tzv.	tzv.	kA	tzv.
těžké	těžký	k2eAgFnSc2d1	těžká
vody	voda	k1gFnSc2	voda
u	u	k7c2	u
Vemorku	Vemorek	k1gInSc2	Vemorek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgMnS	být
paralyzován	paralyzován	k2eAgInSc4d1	paralyzován
německý	německý	k2eAgInSc4d1	německý
jaderný	jaderný	k2eAgInSc4d1	jaderný
program	program	k1gInSc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
rovněž	rovněž	k9	rovněž
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Spojenců	spojenec	k1gMnPc2	spojenec
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
takřka	takřka	k6eAd1	takřka
celé	celý	k2eAgNnSc1d1	celé
norské	norský	k2eAgNnSc1d1	norské
obchodní	obchodní	k2eAgNnSc1d1	obchodní
loďstvo	loďstvo	k1gNnSc1	loďstvo
<g/>
,	,	kIx,	,
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
největší	veliký	k2eAgFnSc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
evakuace	evakuace	k1gFnSc2	evakuace
v	v	k7c6	v
Dunquerke	Dunquerke	k1gNnSc6	Dunquerke
a	a	k8xC	a
vylodění	vylodění	k1gNnSc6	vylodění
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
okupována	okupovat	k5eAaBmNgFnS	okupovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945.	[number]	k4	1945.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
začali	začít	k5eAaPmAgMnP	začít
norské	norský	k2eAgFnSc3d1	norská
politice	politika	k1gFnSc3	politika
dominovat	dominovat	k5eAaImF	dominovat
sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
Norská	norský	k2eAgFnSc1d1	norská
strana	strana	k1gFnSc1	strana
práce	práce	k1gFnSc2	práce
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
většinu	většina	k1gFnSc4	většina
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
osobností	osobnost	k1gFnSc7	osobnost
byl	být	k5eAaImAgInS	být
Einar	Einar	k1gInSc1	Einar
Gerhardsen	Gerhardsen	k2eAgInSc1d1	Gerhardsen
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
socialistické	socialistický	k2eAgFnSc3d1	socialistická
politice	politika	k1gFnSc3	politika
<g/>
,	,	kIx,	,
norští	norský	k2eAgMnPc1d1	norský
labouristé	labourista	k1gMnPc1	labourista
se	se	k3xPyFc4	se
jasně	jasně	k6eAd1	jasně
distancovali	distancovat	k5eAaBmAgMnP	distancovat
od	od	k7c2	od
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
zakladateli	zakladatel	k1gMnPc7	zakladatel
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Položili	položit	k5eAaPmAgMnP	položit
také	také	k6eAd1	také
základy	základ	k1gInPc4	základ
norského	norský	k2eAgInSc2d1	norský
sociálního	sociální	k2eAgInSc2d1	sociální
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
rolí	role	k1gFnSc7	role
státní	státní	k2eAgFnSc2d1	státní
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
ekonomikou	ekonomika	k1gFnSc7	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
cenová	cenový	k2eAgFnSc1d1	cenová
kontrola	kontrola	k1gFnSc1	kontrola
a	a	k8xC	a
státní	státní	k2eAgInSc1d1	státní
příděl	příděl	k1gInSc1	příděl
bydlení	bydlení	k1gNnSc2	bydlení
a	a	k8xC	a
automobilů	automobil	k1gInPc2	automobil
trvaly	trvat	k5eAaImAgFnP	trvat
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
však	však	k9	však
prudce	prudko	k6eAd1	prudko
rostla	růst	k5eAaImAgFnS	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70.	[number]	k4	70.
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgInS	začít
stát	stát	k1gInSc1	stát
těžit	těžit	k5eAaImF	těžit
ropu	ropa	k1gFnSc4	ropa
(	(	kIx(	(
<g/>
její	její	k3xOp3gInSc4	její
nález	nález	k1gInSc4	nález
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgMnS	datovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
a	a	k8xC	a
norské	norský	k2eAgNnSc1d1	norské
bohatství	bohatství	k1gNnSc1	bohatství
se	se	k3xPyFc4	se
tak	tak	k9	tak
ještě	ještě	k6eAd1	ještě
znásobilo	znásobit	k5eAaPmAgNnS	znásobit
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
Norsko	Norsko	k1gNnSc1	Norsko
splatilo	splatit	k5eAaPmAgNnS	splatit
státní	státní	k2eAgInSc4d1	státní
dluh	dluh	k1gInSc4	dluh
a	a	k8xC	a
přebytky	přebytek	k1gInPc4	přebytek
rozpočtů	rozpočet	k1gInPc2	rozpočet
začalo	začít	k5eAaPmAgNnS	začít
hromadit	hromadit	k5eAaImF	hromadit
ve	v	k7c6	v
státním	státní	k2eAgInSc6d1	státní
investičním	investiční	k2eAgInSc6d1	investiční
fondu	fond	k1gInSc6	fond
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
největším	veliký	k2eAgInSc7d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
bylo	být	k5eAaImAgNnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
vyzváno	vyzván	k2eAgNnSc1d1	vyzváno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
připojilo	připojit	k5eAaPmAgNnS	připojit
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
občané	občan	k1gMnPc1	občan
vstup	vstup	k1gInSc4	vstup
vždy	vždy	k6eAd1	vždy
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
referendech	referendum	k1gNnPc6	referendum
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1972	[number]	k4	1972
a	a	k8xC	a
1994.	[number]	k4	1994.
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgMnPc1d1	sociální
demokraté	demokrat	k1gMnPc1	demokrat
již	již	k6eAd1	již
několikráte	několikráte	k6eAd1	několikráte
přepustili	přepustit	k5eAaPmAgMnP	přepustit
vládu	vláda	k1gFnSc4	vláda
konzervativně-liberálním	konzervativněiberální	k2eAgFnPc3d1	konzervativně-liberální
stranám	strana	k1gFnPc3	strana
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
norský	norský	k2eAgInSc1d1	norský
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
poměrně	poměrně	k6eAd1	poměrně
konsensualistický	konsensualistický	k2eAgMnSc1d1	konsensualistický
a	a	k8xC	a
zaměření	zaměření	k1gNnSc1	zaměření
na	na	k7c4	na
sociální	sociální	k2eAgInSc4d1	sociální
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgInPc4d1	nízký
mzdové	mzdový	k2eAgInPc4d1	mzdový
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
,	,	kIx,	,
toleranci	tolerance	k1gFnSc4	tolerance
vůči	vůči	k7c3	vůči
všem	všecek	k3xTgFnPc3	všecek
menšinám	menšina	k1gFnPc3	menšina
a	a	k8xC	a
multikulturalismus	multikulturalismus	k1gInSc1	multikulturalismus
prostupuje	prostupovat	k5eAaImIp3nS	prostupovat
politikou	politika	k1gFnSc7	politika
většiny	většina	k1gFnSc2	většina
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Konsensus	konsensus	k1gInSc1	konsensus
je	být	k5eAaImIp3nS	být
atakován	atakovat	k5eAaBmNgInS	atakovat
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pravicový	pravicový	k2eAgMnSc1d1	pravicový
extremista	extremista	k1gMnSc1	extremista
Anders	Andersa	k1gFnPc2	Andersa
Breivik	Breivik	k1gMnSc1	Breivik
napadl	napadnout	k5eAaPmAgMnS	napadnout
automatickou	automatický	k2eAgFnSc7d1	automatická
puškou	puška	k1gFnSc7	puška
letní	letní	k2eAgInSc1d1	letní
tábor	tábor	k1gInSc1	tábor
sociálnědemokratické	sociálnědemokratický	k2eAgFnSc2d1	sociálnědemokratická
mládeže	mládež	k1gFnSc2	mládež
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Utø	Utø	k1gFnSc2	Utø
a	a	k8xC	a
umístil	umístit	k5eAaPmAgMnS	umístit
bombu	bomba	k1gFnSc4	bomba
před	před	k7c4	před
vládní	vládní	k2eAgFnSc4d1	vládní
budovu	budova	k1gFnSc4	budova
v	v	k7c6	v
Oslu	Oslo	k1gNnSc6	Oslo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
77	[number]	k4	77
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
319	[number]	k4	319
zraněných	zraněný	k1gMnPc2	zraněný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
Skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
ostrůvcích	ostrůvek	k1gInPc6	ostrůvek
v	v	k7c6	v
Severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
Norském	norský	k2eAgNnSc6d1	norské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
Barentsově	barentsově	k6eAd1	barentsově
moři	moře	k1gNnSc3	moře
a	a	k8xC	a
Severním	severní	k2eAgInSc6d1	severní
ledovém	ledový	k2eAgInSc6d1	ledový
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Pevninskými	pevninský	k2eAgInPc7d1	pevninský
ostrovy	ostrov	k1gInPc7	ostrov
jsou	být	k5eAaImIp3nP	být
Lofoty	Lofota	k1gFnSc2	Lofota
a	a	k8xC	a
severněji	severně	k6eAd2	severně
ležící	ležící	k2eAgFnSc2d1	ležící
Vesterály	Vesterála	k1gFnSc2	Vesterála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
Norsku	Norsko	k1gNnSc3	Norsko
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
si	se	k3xPyFc3	se
Norsko	Norsko	k1gNnSc1	Norsko
nárokuje	nárokovat	k5eAaImIp3nS	nárokovat
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
Antarktidy	Antarktida	k1gFnSc2	Antarktida
–	–	k?	–
Zemi	zem	k1gFnSc4	zem
královny	královna	k1gFnSc2	královna
Maud	Maud	k1gInSc1	Maud
a	a	k8xC	a
ostrov	ostrov	k1gInSc1	ostrov
Petra	Petr	k1gMnSc2	Petr
I.	I.	kA	I.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Pevninská	pevninský	k2eAgFnSc1d1	pevninská
část	část	k1gFnSc1	část
===	===	k?	===
</s>
</p>
<p>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
část	část	k1gFnSc1	část
Norska	Norsko	k1gNnSc2	Norsko
leží	ležet	k5eAaImIp3nS	ležet
mezi	mezi	k7c4	mezi
58	[number]	k4	58
<g/>
°	°	k?	°
s	s	k7c7	s
<g/>
.	.	kIx.	.
š	š	k?	š
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Lindesneský	Lindesneský	k2eAgInSc1d1	Lindesneský
maják	maják	k1gInSc1	maják
)	)	kIx)	)
a	a	k8xC	a
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
k	k	k7c3	k
nejsevernějšímu	severní	k2eAgInSc3d3	nejsevernější
výběžku	výběžek	k1gInSc3	výběžek
pevninské	pevninský	k2eAgFnSc2d1	pevninská
Evropy	Evropa	k1gFnSc2	Evropa
na	na	k7c4	na
Nordkinnu	Nordkinna	k1gFnSc4	Nordkinna
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
místa	místo	k1gNnPc1	místo
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
1700	[number]	k4	1700
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
Norska	Norsko	k1gNnSc2	Norsko
byl	být	k5eAaImAgInS	být
modelován	modelovat	k5eAaImNgInS	modelovat
mnoha	mnoho	k4c7	mnoho
ledovci	ledovec	k1gInPc7	ledovec
<g/>
,	,	kIx,	,
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
jsou	být	k5eAaImIp3nP	být
četné	četný	k2eAgInPc1d1	četný
fjordy	fjord	k1gInPc1	fjord
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
zatopením	zatopení	k1gNnSc7	zatopení
ledovcových	ledovcový	k2eAgNnPc2d1	ledovcové
údolí	údolí	k1gNnPc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
členitá	členitý	k2eAgFnSc1d1	členitá
<g/>
,	,	kIx,	,
příkrá	příkrý	k2eAgFnSc1d1	příkrá
a	a	k8xC	a
fjordová	fjordový	k2eAgFnSc1d1	fjordový
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
drobnými	drobný	k2eAgInPc7d1	drobný
ostrovy	ostrov	k1gInPc7	ostrov
a	a	k8xC	a
zálivy	záliv	k1gInPc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
je	být	k5eAaImIp3nS	být
pobřeží	pobřeží	k1gNnSc4	pobřeží
méně	málo	k6eAd2	málo
členité	členitý	k2eAgNnSc4d1	členité
a	a	k8xC	a
pozvolné	pozvolný	k2eAgNnSc4d1	pozvolné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přemodelování	přemodelování	k1gNnSc2	přemodelování
ledovci	ledovec	k1gInSc6	ledovec
je	být	k5eAaImIp3nS	být
také	také	k9	také
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
.	.	kIx.	.
</s>
<s>
Celým	celý	k2eAgNnSc7d1	celé
Norskem	Norsko	k1gNnSc7	Norsko
se	se	k3xPyFc4	se
od	od	k7c2	od
jihu	jih	k1gInSc2	jih
táhne	táhnout	k5eAaImIp3nS	táhnout
Skandinávské	skandinávský	k2eAgNnSc4d1	skandinávské
pohoří	pohoří	k1gNnSc4	pohoří
s	s	k7c7	s
recentním	recentní	k2eAgNnSc7d1	recentní
zaledněním	zalednění	k1gNnSc7	zalednění
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Galdhø	Galdhø	k1gFnSc1	Galdhø
s	s	k7c7	s
2469	[number]	k4	2469
metry	metr	k1gInPc4	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Skandinávském	skandinávský	k2eAgNnSc6d1	skandinávské
pohoří	pohoří	k1gNnSc6	pohoří
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
největší	veliký	k2eAgInSc1d3	veliký
ledovec	ledovec	k1gInSc1	ledovec
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
Evropy	Evropa	k1gFnSc2	Evropa
Jostedalsbreen	Jostedalsbrena	k1gFnPc2	Jostedalsbrena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
Norska	Norsko	k1gNnSc2	Norsko
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
systémem	systém	k1gInSc7	systém
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýrazněji	výrazně	k6eAd3	výrazně
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
větev	větev	k1gFnSc1	větev
Norského	norský	k2eAgInSc2d1	norský
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc3d1	jižní
oblasti	oblast	k1gFnSc3	oblast
Norska	Norsko	k1gNnSc2	Norsko
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgFnP	řadit
k	k	k7c3	k
mírně	mírně	k6eAd1	mírně
teplému	teplý	k2eAgNnSc3d1	teplé
<g/>
,	,	kIx,	,
vlhkému	vlhký	k2eAgNnSc3d1	vlhké
podnebí	podnebí	k1gNnSc3	podnebí
západních	západní	k2eAgNnPc2d1	západní
pobřeží	pobřeží	k1gNnPc2	pobřeží
<g/>
,	,	kIx,	,
v	v	k7c6	v
Köppenově	Köppenův	k2eAgFnSc6d1	Köppenova
klasifikaci	klasifikace	k1gFnSc6	klasifikace
podnebí	podnebí	k1gNnSc4	podnebí
označovaného	označovaný	k2eAgMnSc2d1	označovaný
Cfb	Cfb	k1gMnSc2	Cfb
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
srážek	srážka	k1gFnPc2	srážka
spadne	spadnout	k5eAaPmIp3nS	spadnout
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Å	Å	k?	Å
až	až	k9	až
k	k	k7c3	k
70	[number]	k4	70
<g/>
°	°	k?	°
s	s	k7c7	s
<g/>
.	.	kIx.	.
š	š	k?	š
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
podnebný	podnebný	k2eAgInSc1d1	podnebný
pás	pás	k1gInSc1	pás
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
mírně	mírně	k6eAd1	mírně
teplému	teplý	k2eAgNnSc3d1	teplé
podnebí	podnebí	k1gNnSc3	podnebí
západních	západní	k2eAgNnPc2d1	západní
podnebí	podnebí	k1gNnPc2	podnebí
s	s	k7c7	s
chladným	chladný	k2eAgNnSc7d1	chladné
létem	léto	k1gNnSc7	léto
(	(	kIx(	(
<g/>
Cfc	Cfc	k1gMnSc1	Cfc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Skandinávského	skandinávský	k2eAgNnSc2d1	skandinávské
pohoří	pohoří	k1gNnSc2	pohoří
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
pás	pás	k1gInSc1	pás
mírně	mírně	k6eAd1	mírně
studeného	studený	k2eAgNnSc2d1	studené
kontinentálního	kontinentální	k2eAgNnSc2d1	kontinentální
podnebí	podnebí	k1gNnSc2	podnebí
s	s	k7c7	s
chladným	chladný	k2eAgNnSc7d1	chladné
létem	léto	k1gNnSc7	léto
(	(	kIx(	(
<g/>
Dfc	Dfc	k1gMnSc1	Dfc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
jižního	jižní	k2eAgNnSc2d1	jižní
Skandinávského	skandinávský	k2eAgNnSc2d1	skandinávské
pohoří	pohoří	k1gNnSc2	pohoří
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
horského	horský	k2eAgNnSc2d1	horské
podnebí	podnebí	k1gNnSc2	podnebí
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
polárním	polární	k2eAgInSc7d1	polární
kruhem	kruh	k1gInSc7	kruh
panuje	panovat	k5eAaImIp3nS	panovat
podnebí	podnebí	k1gNnSc1	podnebí
tundry	tundra	k1gFnSc2	tundra
(	(	kIx(	(
<g/>
ET	ET	kA	ET
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
tabulky	tabulka	k1gFnSc2	tabulka
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
srážkově	srážkově	k6eAd1	srážkově
bohaté	bohatý	k2eAgNnSc1d1	bohaté
<g/>
.	.	kIx.	.
</s>
<s>
Četné	četný	k2eAgFnPc1d1	četná
a	a	k8xC	a
vodné	vodný	k2eAgFnPc1d1	vodná
řeky	řeka	k1gFnPc1	řeka
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
spádem	spád	k1gInSc7	spád
jsou	být	k5eAaImIp3nP	být
velkým	velký	k2eAgNnPc3d1	velké
norským	norský	k2eAgNnPc3d1	norské
bohatstvím	bohatství	k1gNnPc3	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
utvářela	utvářet	k5eAaImAgFnS	utvářet
jen	jen	k9	jen
velice	velice	k6eAd1	velice
krátce	krátce	k6eAd1	krátce
<g/>
.	.	kIx.	.
</s>
<s>
Spádové	spádový	k2eAgFnPc1d1	spádová
křivky	křivka	k1gFnPc1	křivka
řek	řeka	k1gFnPc2	řeka
jsou	být	k5eAaImIp3nP	být
charakteristicky	charakteristicky	k6eAd1	charakteristicky
nevyrovnané	vyrovnaný	k2eNgInPc1d1	nevyrovnaný
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Glomma	Glomma	k1gFnSc1	Glomma
a	a	k8xC	a
Gudbrandsdalslå	Gudbrandsdalslå	k1gFnSc1	Gudbrandsdalslå
(	(	kIx(	(
<g/>
Lå	Lå	k1gFnSc1	Lå
<g/>
,	,	kIx,	,
Vorma	Vorma	k1gFnSc1	Vorma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dramselv	Dramselv	k1gMnSc1	Dramselv
<g/>
,	,	kIx,	,
Numedalslå	Numedalslå	k1gMnSc1	Numedalslå
(	(	kIx(	(
<g/>
Lå	Lå	k1gMnSc1	Lå
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Otra	Otrus	k1gMnSc4	Otrus
<g/>
,	,	kIx,	,
Sira	sir	k1gMnSc4	sir
<g/>
,	,	kIx,	,
Namsen	Namsen	k1gInSc1	Namsen
<g/>
,	,	kIx,	,
Vefsna	Vefsna	k1gFnSc1	Vefsna
<g/>
,	,	kIx,	,
Altelv	Altelv	k1gMnSc1	Altelv
<g/>
,	,	kIx,	,
Tana	tanout	k5eAaImSgMnS	tanout
<g/>
,	,	kIx,	,
Lundeelv	Lundeelv	k1gInSc1	Lundeelv
<g/>
,	,	kIx,	,
Songa	Songa	k1gFnSc1	Songa
<g/>
,	,	kIx,	,
Kvenna	Kvenna	k1gFnSc1	Kvenna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
jsou	být	k5eAaImIp3nP	být
četná	četný	k2eAgNnPc4d1	četné
jezera	jezero	k1gNnPc4	jezero
a	a	k8xC	a
silně	silně	k6eAd1	silně
podmáčené	podmáčený	k2eAgFnSc6d1	podmáčená
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Finsko	Finsko	k1gNnSc4	Finsko
<g/>
,	,	kIx,	,
nazýváno	nazýván	k2eAgNnSc4d1	nazýváno
"	"	kIx"	"
<g/>
Zemí	zem	k1gFnSc7	zem
tisíců	tisíc	k4xCgInPc2	tisíc
jezer	jezero	k1gNnPc2	jezero
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
jezerům	jezero	k1gNnPc3	jezero
patří	patřit	k5eAaImIp3nS	patřit
jezero	jezero	k1gNnSc1	jezero
Mjø	Mjø	k1gFnPc2	Mjø
<g/>
,	,	kIx,	,
Rø	Rø	k1gFnPc2	Rø
<g/>
,	,	kIx,	,
Femunden	Femundna	k1gFnPc2	Femundna
<g/>
,	,	kIx,	,
Randsfjorden	Randsfjordna	k1gFnPc2	Randsfjordna
<g/>
,	,	kIx,	,
Tyrifjorden	Tyrifjordna	k1gFnPc2	Tyrifjordna
<g/>
,	,	kIx,	,
Snå	Snå	k1gFnSc1	Snå
<g/>
,	,	kIx,	,
Tunnsjø	Tunnsjø	k1gFnSc1	Tunnsjø
<g/>
,	,	kIx,	,
Limingen	Limingen	k1gInSc1	Limingen
<g/>
,	,	kIx,	,
Ø	Ø	k?	Ø
a	a	k8xC	a
Blå	Blå	k1gFnSc7	Blå
(	(	kIx(	(
<g/>
všechna	všechen	k3xTgNnPc1	všechen
jezera	jezero	k1gNnPc4	jezero
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
80	[number]	k4	80
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hornindalsvatnet	Hornindalsvatnet	k1gInSc4	Hornindalsvatnet
je	být	k5eAaImIp3nS	být
nejhlubší	hluboký	k2eAgNnSc1d3	nejhlubší
norské	norský	k2eAgNnSc1d1	norské
a	a	k8xC	a
evropské	evropský	k2eAgNnSc1d1	Evropské
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
hloubka	hloubka	k1gFnSc1	hloubka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
514	[number]	k4	514
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Půdy	půda	k1gFnPc1	půda
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
podzolované	podzolovaný	k2eAgFnPc1d1	podzolovaná
<g/>
,	,	kIx,	,
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
Skand	Skanda	k1gFnPc2	Skanda
jsou	být	k5eAaImIp3nP	být
půdy	půda	k1gFnPc4	půda
slabě	slabě	k6eAd1	slabě
vyvinuté	vyvinutý	k2eAgFnPc4d1	vyvinutá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
částech	část	k1gFnPc6	část
Norska	Norsko	k1gNnSc2	Norsko
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
luvisoly	luvisola	k1gFnPc1	luvisola
<g/>
.	.	kIx.	.
</s>
<s>
Četné	četný	k2eAgNnSc1d1	četné
je	být	k5eAaImIp3nS	být
i	i	k9	i
zastoupení	zastoupení	k1gNnSc1	zastoupení
histosolů	histosol	k1gInPc2	histosol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
oficiální	oficiální	k2eAgFnSc7d1	oficiální
hlavou	hlava	k1gFnSc7	hlava
je	být	k5eAaImIp3nS	být
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
panovníkem	panovník	k1gMnSc7	panovník
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
král	král	k1gMnSc1	král
Harald	Harald	k1gMnSc1	Harald
V.	V.	kA	V.
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgInSc1d1	narozen
21.	[number]	k4	21.
února	únor	k1gInSc2	únor
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nastoupí	nastoupit	k5eAaPmIp3nS	nastoupit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Haakon	Haakon	k1gMnSc1	Haakon
Magnus	Magnus	k1gMnSc1	Magnus
(	(	kIx(	(
<g/>
nar.	nar.	kA	nar.
20.	[number]	k4	20.
července	červenec	k1gInSc2	červenec
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
parlamentu	parlament	k1gInSc2	parlament
–	–	k?	–
Stortingu	Storting	k1gInSc2	Storting
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
výkonná	výkonný	k2eAgFnSc1d1	výkonná
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
norské	norský	k2eAgFnSc2d1	norská
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
hlavou	hlava	k1gFnSc7	hlava
Norského	norský	k2eAgNnSc2d1	norské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Norské	norský	k2eAgFnSc2d1	norská
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
Norských	norský	k2eAgFnPc2d1	norská
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
plní	plnit	k5eAaImIp3nS	plnit
reprezentativní	reprezentativní	k2eAgFnPc4d1	reprezentativní
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Vládnoucí	vládnoucí	k2eAgMnSc1d1	vládnoucí
panovník	panovník	k1gMnSc1	panovník
je	být	k5eAaImIp3nS	být
také	také	k9	také
Velmistrem	velmistr	k1gMnSc7	velmistr
Norského	norský	k2eAgInSc2d1	norský
královského	královský	k2eAgInSc2d1	královský
řádu	řád	k1gInSc2	řád
svatého	svatý	k2eAgMnSc2d1	svatý
Olafa	Olaf	k1gMnSc2	Olaf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norský	norský	k2eAgInSc1d1	norský
parlament	parlament	k1gInSc1	parlament
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Oslu	osel	k1gMnSc6	osel
<g/>
.	.	kIx.	.
</s>
<s>
Storting	Storting	k1gInSc1	Storting
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
Lagting	Lagting	k1gInSc4	Lagting
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
komora	komora	k1gFnSc1	komora
<g/>
)	)	kIx)	)
a	a	k8xC	a
Odelsting	Odelsting	k1gInSc1	Odelsting
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgFnSc1d1	dolní
komora	komora	k1gFnSc1	komora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
křesel	křeslo	k1gNnPc2	křeslo
ve	v	k7c6	v
Stortingu	Storting	k1gInSc6	Storting
se	se	k3xPyFc4	se
měnil	měnit	k5eAaImAgInS	měnit
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
měl	mít	k5eAaImAgInS	mít
114	[number]	k4	114
křesel	křeslo	k1gNnPc2	křeslo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903 117	[number]	k4	1903 117
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906 123	[number]	k4	1906 123
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918 126	[number]	k4	1918 126
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921 150	[number]	k4	1921 150
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973 155	[number]	k4	1973 155
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985 157	[number]	k4	1985 157
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989 165	[number]	k4	1989 165
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
má	mít	k5eAaImIp3nS	mít
169	[number]	k4	169
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
===	===	k?	===
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
strana	strana	k1gFnSc1	strana
práce	práce	k1gFnSc2	práce
–	–	k?	–
Det	Det	k1gFnPc2	Det
norske	norskat	k5eAaPmIp3nS	norskat
Arbeiderparti	Arbeiderpart	k1gMnPc1	Arbeiderpart
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
též	též	k9	též
Arbeiderpartiet	Arbeiderpartiet	k1gMnSc1	Arbeiderpartiet
(	(	kIx(	(
<g/>
AP	ap	kA	ap
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pravice	pravice	k1gFnSc1	pravice
<g/>
,	,	kIx,	,
Norská	norský	k2eAgFnSc1d1	norská
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
strana	strana	k1gFnSc1	strana
–	–	k?	–
Hø	Hø	k1gFnSc1	Hø
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
rozvoje	rozvoj	k1gInSc2	rozvoj
–	–	k?	–
Fremskrittspartiet	Fremskrittspartiet	k1gMnSc1	Fremskrittspartiet
(	(	kIx(	(
<g/>
FrP	FrP	k1gMnSc1	FrP
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
lidová	lidový	k2eAgFnSc1d1	lidová
strana	strana	k1gFnSc1	strana
–	–	k?	–
Kristelig	Kristeliga	k1gFnPc2	Kristeliga
Folkeparti	Folkepart	k1gMnPc1	Folkepart
(	(	kIx(	(
<g/>
KrF	KrF	k1gMnPc1	KrF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Středová	středový	k2eAgFnSc1d1	středová
strana	strana	k1gFnSc1	strana
–	–	k?	–
Senterpartiet	Senterpartiet	k1gInSc1	Senterpartiet
(	(	kIx(	(
<g/>
Sp	Sp	k1gFnSc1	Sp
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Socialistická	socialistický	k2eAgFnSc1d1	socialistická
levicová	levicový	k2eAgFnSc1d1	levicová
strana	strana	k1gFnSc1	strana
–	–	k?	–
Sosialistisk	Sosialistisk	k1gInSc4	Sosialistisk
Venstreparti	Venstrepart	k1gMnPc1	Venstrepart
(	(	kIx(	(
<g/>
SV	sv	kA	sv
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Levice	levice	k1gFnSc1	levice
<g/>
,	,	kIx,	,
Norská	norský	k2eAgFnSc1d1	norská
liberální	liberální	k2eAgFnSc1d1	liberální
strana	strana	k1gFnSc1	strana
–	–	k?	–
Venstre	Venstr	k1gInSc5	Venstr
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ekologická	ekologický	k2eAgFnSc1d1	ekologická
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
má	mít	k5eAaImIp3nS	mít
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
další	další	k2eAgFnPc1d1	další
skandinávské	skandinávský	k2eAgFnPc1d1	skandinávská
země	zem	k1gFnPc1	zem
aktivní	aktivní	k2eAgFnSc4d1	aktivní
politiku	politika	k1gFnSc4	politika
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
zavedlo	zavést	k5eAaPmAgNnS	zavést
např.	např.	kA	např.
zákonné	zákonný	k2eAgFnPc4d1	zákonná
restrikce	restrikce	k1gFnPc4	restrikce
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
rtuti	rtuť	k1gFnSc2	rtuť
nebo	nebo	k8xC	nebo
bromovaných	bromovaný	k2eAgInPc2d1	bromovaný
zpomalovačů	zpomalovač	k1gInPc2	zpomalovač
hoření	hoření	k1gNnSc2	hoření
<g/>
,	,	kIx,	,
opatření	opatření	k1gNnPc1	opatření
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
emisí	emise	k1gFnPc2	emise
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
nebo	nebo	k8xC	nebo
projekt	projekt	k1gInSc4	projekt
zachycování	zachycování	k1gNnSc2	zachycování
a	a	k8xC	a
skladování	skladování	k1gNnSc2	skladování
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
(	(	kIx(	(
<g/>
CCS	CCS	kA	CCS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
také	také	k9	také
navrhlo	navrhnout	k5eAaPmAgNnS	navrhnout
zařazení	zařazení	k1gNnSc1	zařazení
hexabromcyklododekanu	hexabromcyklododekan	k1gInSc2	hexabromcyklododekan
(	(	kIx(	(
<g/>
HBCD	HBCD	kA	HBCD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
bromovaný	bromovaný	k2eAgInSc1d1	bromovaný
zpomalovač	zpomalovač	k1gInSc1	zpomalovač
hoření	hoření	k2eAgInSc1d1	hoření
<g/>
,	,	kIx,	,
na	na	k7c4	na
černou	černý	k2eAgFnSc4d1	černá
listinu	listina	k1gFnSc4	listina
perzistentních	perzistentní	k2eAgFnPc2d1	perzistentní
organických	organický	k2eAgFnPc2d1	organická
látek	látka	k1gFnPc2	látka
regulovaných	regulovaný	k2eAgFnPc2d1	regulovaná
Stockholmskou	stockholmský	k2eAgFnSc7d1	Stockholmská
úmluvou	úmluva	k1gFnSc7	úmluva
<g/>
.	.	kIx.	.
<g/>
Spory	spora	k1gFnSc2	spora
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
norské	norský	k2eAgNnSc1d1	norské
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
ignorovat	ignorovat	k5eAaImF	ignorovat
zákaz	zákaz	k1gInSc4	zákaz
komerčního	komerční	k2eAgInSc2d1	komerční
lovu	lov	k1gInSc2	lov
velryb	velryba	k1gFnPc2	velryba
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
velrybářská	velrybářský	k2eAgFnSc1d1	velrybářská
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
IWC	IWC	kA	IWC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
pravidelně	pravidelně	k6eAd1	pravidelně
velryby	velryba	k1gFnPc4	velryba
loví	lovit	k5eAaImIp3nP	lovit
a	a	k8xC	a
například	například	k6eAd1	například
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
úřady	úřad	k1gInPc1	úřad
vydaly	vydat	k5eAaPmAgInP	vydat
povolení	povolení	k1gNnSc4	povolení
ulovit	ulovit	k5eAaPmF	ulovit
pro	pro	k7c4	pro
obchodní	obchodní	k2eAgInPc4d1	obchodní
účely	účel	k1gInPc4	účel
885	[number]	k4	885
plejtváků	plejtvák	k1gMnPc2	plejtvák
malých	malý	k2eAgMnPc2d1	malý
(	(	kIx(	(
<g/>
Balaenoptera	Balaenopter	k1gMnSc2	Balaenopter
acutorostrata	acutorostrat	k1gMnSc2	acutorostrat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Problematika	problematika	k1gFnSc1	problematika
ochrany	ochrana	k1gFnSc2	ochrana
dětí	dítě	k1gFnPc2	dítě
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
velkých	velký	k2eAgFnPc2d1	velká
kontroverzí	kontroverze	k1gFnPc2	kontroverze
–	–	k?	–
někteří	některý	k3yIgMnPc1	některý
ji	on	k3xPp3gFnSc4	on
chválí	chválit	k5eAaImIp3nP	chválit
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
ji	on	k3xPp3gFnSc4	on
naopak	naopak	k6eAd1	naopak
odsuzují	odsuzovat	k5eAaImIp3nP	odsuzovat
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c6	o
hrubém	hrubý	k2eAgNnSc6d1	hrubé
porušování	porušování	k1gNnSc6	porušování
práv	právo	k1gNnPc2	právo
dětí	dítě	k1gFnPc2	dítě
i	i	k8xC	i
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
mezinárodně	mezinárodně	k6eAd1	mezinárodně
známým	známý	k2eAgInPc3d1	známý
sporným	sporný	k2eAgInPc3d1	sporný
případům	případ	k1gInPc3	případ
je	být	k5eAaImIp3nS	být
země	zem	k1gFnPc1	zem
obviňována	obviňován	k2eAgFnSc1d1	obviňována
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
plánovitému	plánovitý	k2eAgNnSc3d1	plánovité
odebírání	odebírání	k1gNnSc3	odebírání
dětí	dítě	k1gFnPc2	dítě
kvůli	kvůli	k7c3	kvůli
finančnímu	finanční	k2eAgNnSc3d1	finanční
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
osvojitelů	osvojitel	k1gMnPc2	osvojitel
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
činí	činit	k5eAaImIp3nS	činit
až	až	k9	až
30 000	[number]	k4	30 000
eur	euro	k1gNnPc2	euro
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Evropský	evropský	k2eAgInSc1d1	evropský
soud	soud	k1gInSc1	soud
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
konstatoval	konstatovat	k5eAaBmAgMnS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Norsko	Norsko	k1gNnSc1	Norsko
v	v	k7c6	v
případu	případ	k1gInSc6	případ
pana	pan	k1gMnSc2	pan
Sancheze	Sancheze	k1gFnSc2	Sancheze
Cardenase	Cardenas	k1gInSc5	Cardenas
porušilo	porušit	k5eAaPmAgNnS	porušit
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
základě	základ	k1gInSc6	základ
neprokázaných	prokázaný	k2eNgNnPc2d1	neprokázané
obvinění	obvinění	k1gNnPc2	obvinění
zakázalo	zakázat	k5eAaPmAgNnS	zakázat
vídat	vídat	k5eAaImF	vídat
jeho	jeho	k3xOp3gFnPc4	jeho
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Kritizovanou	kritizovaný	k2eAgFnSc4d1	kritizovaná
praxi	praxe	k1gFnSc4	praxe
však	však	k9	však
norský	norský	k2eAgInSc1d1	norský
úřad	úřad	k1gInSc1	úřad
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
Barnevernet	Barnevernet	k1gInSc1	Barnevernet
<g/>
)	)	kIx)	)
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
soudní	soudní	k2eAgNnSc4d1	soudní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
výrazněji	výrazně	k6eAd2	výrazně
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Rozličné	rozličný	k2eAgInPc1d1	rozličný
sporné	sporný	k2eAgInPc1d1	sporný
případy	případ	k1gInPc1	případ
odebrání	odebrání	k1gNnSc2	odebrání
dětí	dítě	k1gFnPc2	dítě
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
občanům	občan	k1gMnPc3	občan
vzbudily	vzbudit	k5eAaPmAgInP	vzbudit
pozornost	pozornost	k1gFnSc4	pozornost
v	v	k7c6	v
zahraničních	zahraniční	k2eAgInPc6d1	zahraniční
médiích	médium	k1gNnPc6	médium
či	či	k8xC	či
vyústily	vyústit	k5eAaPmAgFnP	vyústit
v	v	k7c4	v
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
a	a	k8xC	a
soudní	soudní	k2eAgInPc4d1	soudní
spory	spor	k1gInPc4	spor
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
úřadů	úřad	k1gInPc2	úřad
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
<g/>
.	.	kIx.	.
<g/>
Případ	případ	k1gInSc1	případ
konfiskovaných	konfiskovaný	k2eAgFnPc2d1	konfiskovaná
dětí	dítě	k1gFnPc2	dítě
české	český	k2eAgFnSc2d1	Česká
matky	matka	k1gFnSc2	matka
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
pozornost	pozornost	k1gFnSc4	pozornost
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Norsko	Norsko	k1gNnSc1	Norsko
bylo	být	k5eAaImAgNnS	být
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
i	i	k9	i
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
občanské	občanský	k2eAgFnPc4d1	občanská
svobody	svoboda	k1gFnPc4	svoboda
<g/>
,	,	kIx,	,
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
věci	věc	k1gFnPc4	věc
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
18	[number]	k4	18
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
norsky	norsky	k6eAd1	norsky
<g/>
:	:	kIx,	:
fylke	fylke	k1gFnSc1	fylke
<g/>
,	,	kIx,	,
pl	pl	k?	pl
<g/>
.	.	kIx.	.
fylker	fylker	k1gMnSc1	fylker
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
známých	známý	k2eAgNnPc2d1	známé
jako	jako	k8xC	jako
amt	amt	k?	amt
<g/>
,	,	kIx,	,
pl	pl	k?	pl
<g/>
.	.	kIx.	.
amter	amter	k1gMnSc1	amter
<g/>
,	,	kIx,	,
a	a	k8xC	a
422	[number]	k4	422
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
kommune	kommun	k1gMnSc5	kommun
<g/>
,	,	kIx,	,
pl	pl	k?	pl
<g/>
.	.	kIx.	.
kommuner	kommuner	k1gMnSc1	kommuner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Oslo	Oslo	k1gNnSc2	Oslo
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
kraj	kraj	k1gInSc4	kraj
a	a	k8xC	a
samosprávné	samosprávný	k2eAgNnSc4d1	samosprávné
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
kraji	kraj	k1gInSc6	kraj
je	být	k5eAaImIp3nS	být
král	král	k1gMnSc1	král
reprezentován	reprezentovat	k5eAaImNgMnS	reprezentovat
guvernérem	guvernér	k1gMnSc7	guvernér
(	(	kIx(	(
<g/>
fylkesmann	fylkesmann	k1gMnSc1	fylkesmann
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norskými	norský	k2eAgInPc7d1	norský
kraji	kraj	k1gInPc7	kraj
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
existuje	existovat	k5eAaImIp3nS	existovat
celkem	celkem	k6eAd1	celkem
96	[number]	k4	96
sídel	sídlo	k1gNnPc2	sídlo
se	s	k7c7	s
statusem	status	k1gInSc7	status
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
devět	devět	k4xCc4	devět
největších	veliký	k2eAgFnPc2d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
1.	[number]	k4	1.
červenci	červenec	k1gInSc3	červenec
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
dvě	dva	k4xCgFnPc4	dva
integrální	integrální	k2eAgFnPc4d1	integrální
zámořská	zámořský	k2eAgNnPc4d1	zámořské
teritoria	teritorium	k1gNnPc4	teritorium
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ostrovy	ostrov	k1gInPc7	ostrov
Jan	Jan	k1gMnSc1	Jan
Mayen	Mayen	k1gInSc1	Mayen
a	a	k8xC	a
Špicberky	Špicberky	k1gFnPc1	Špicberky
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
antarktická	antarktický	k2eAgNnPc4d1	antarktické
a	a	k8xC	a
subarktická	subarktický	k2eAgNnPc4d1	subarktické
závislá	závislý	k2eAgNnPc4d1	závislé
území	území	k1gNnPc4	území
<g/>
:	:	kIx,	:
Bouvetův	Bouvetův	k2eAgInSc1d1	Bouvetův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
Petra	Petra	k1gFnSc1	Petra
I.	I.	kA	I.
a	a	k8xC	a
Zemi	zem	k1gFnSc4	zem
královny	královna	k1gFnSc2	královna
Maud	Mauda	k1gFnPc2	Mauda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
stát	stát	k1gInSc1	stát
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
odvětvovou	odvětvový	k2eAgFnSc7d1	odvětvová
specializací	specializace	k1gFnSc7	specializace
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
rybolov	rybolov	k1gInSc1	rybolov
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
kvůli	kvůli	k7c3	kvůli
případnému	případný	k2eAgNnSc3d1	případné
omezení	omezení	k1gNnSc3	omezení
těžby	těžba	k1gFnSc2	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
rybolovu	rybolov	k1gInSc3	rybolov
Norsko	Norsko	k1gNnSc4	Norsko
opakovaně	opakovaně	k6eAd1	opakovaně
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
výrobcem	výrobce	k1gMnSc7	výrobce
poměrně	poměrně	k6eAd1	poměrně
čisté	čistý	k2eAgFnSc2d1	čistá
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
99	[number]	k4	99
%	%	kIx~	%
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
hydroelektrárnách	hydroelektrárna	k1gFnPc6	hydroelektrárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Před	před	k7c7	před
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
revolucí	revoluce	k1gFnSc7	revoluce
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Před	před	k7c7	před
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
revolucí	revoluce	k1gFnSc7	revoluce
bylo	být	k5eAaImAgNnS	být
norské	norský	k2eAgNnSc1d1	norské
hospodářství	hospodářství	k1gNnSc1	hospodářství
založeno	založit	k5eAaPmNgNnS	založit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
a	a	k8xC	a
rybolovu	rybolov	k1gInSc6	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Norové	Nor	k1gMnPc1	Nor
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
nedostatku	nedostatek	k1gInSc6	nedostatek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hladomory	hladomor	k1gInPc1	hladomor
byly	být	k5eAaImAgInP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
několika	několik	k4yIc2	několik
úrodných	úrodný	k2eAgFnPc2d1	úrodná
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c4	v
Hedemarken	Hedemarken	k1gInSc4	Hedemarken
a	a	k8xC	a
Ø	Ø	k?	Ø
se	se	k3xPyFc4	se
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
výroba	výroba	k1gFnSc1	výroba
omezovala	omezovat	k5eAaImAgFnS	omezovat
na	na	k7c4	na
obiloviny	obilovina	k1gFnPc4	obilovina
–	–	k?	–
oves	oves	k1gInSc1	oves
<g/>
,	,	kIx,	,
žito	žito	k1gNnSc1	žito
a	a	k8xC	a
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
a	a	k8xC	a
hospodářská	hospodářský	k2eAgNnPc1d1	hospodářské
zvířata	zvíře	k1gNnPc1	zvíře
–	–	k?	–
ovce	ovce	k1gFnPc4	ovce
<g/>
,	,	kIx,	,
kozy	koza	k1gFnPc4	koza
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
a	a	k8xC	a
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
bylo	být	k5eAaImAgNnS	být
zemědělství	zemědělství	k1gNnSc1	zemědělství
doplněno	doplnit	k5eAaPmNgNnS	doplnit
lovem	lov	k1gInSc7	lov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Norsku	Norsko	k1gNnSc6	Norsko
(	(	kIx(	(
<g/>
Finnmarka	Finnmarka	k1gFnSc1	Finnmarka
<g/>
)	)	kIx)	)
žije	žít	k5eAaImIp3nS	žít
národnostní	národnostní	k2eAgFnSc1d1	národnostní
menšina	menšina	k1gFnSc1	menšina
Sámové	Sámo	k1gMnPc1	Sámo
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
příslušníci	příslušník	k1gMnPc1	příslušník
kočovali	kočovat	k5eAaImAgMnP	kočovat
a	a	k8xC	a
kočují	kočovat	k5eAaImIp3nP	kočovat
se	s	k7c7	s
stády	stádo	k1gNnPc7	stádo
sobů	sob	k1gMnPc2	sob
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
Sámové	Sámo	k1gMnPc1	Sámo
soby	soba	k1gFnSc2	soba
chovají	chovat	k5eAaImIp3nP	chovat
a	a	k8xC	a
věnují	věnovat	k5eAaImIp3nP	věnovat
se	se	k3xPyFc4	se
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Sámové	Sámo	k1gMnPc1	Sámo
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
používat	používat	k5eAaImF	používat
vlastní	vlastní	k2eAgInSc4d1	vlastní
jazyk	jazyk	k1gInSc4	jazyk
sámegiella	sámegiella	k6eAd1	sámegiella
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
finštině	finština	k1gFnSc3	finština
a	a	k8xC	a
postavený	postavený	k2eAgMnSc1d1	postavený
na	na	k7c4	na
roveň	roveň	k1gFnSc4	roveň
norštině	norština	k1gFnSc3	norština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rybolov	rybolov	k1gInSc1	rybolov
byl	být	k5eAaImAgInS	být
podél	podél	k7c2	podél
celého	celý	k2eAgNnSc2d1	celé
pobřeží	pobřeží	k1gNnSc2	pobřeží
drsnou	drsný	k2eAgFnSc7d1	drsná
a	a	k8xC	a
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Rybí	rybí	k2eAgFnPc1d1	rybí
populace	populace	k1gFnPc1	populace
byly	být	k5eAaImAgFnP	být
velice	velice	k6eAd1	velice
hojné	hojný	k2eAgFnPc1d1	hojná
<g/>
,	,	kIx,	,
lovili	lovit	k5eAaImAgMnP	lovit
se	se	k3xPyFc4	se
sledi	sleď	k1gMnPc1	sleď
<g/>
,	,	kIx,	,
tresky	treska	k1gFnPc1	treska
<g/>
,	,	kIx,	,
platýsi	platýs	k1gMnPc1	platýs
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
spíše	spíše	k9	spíše
chladnomilnější	chladnomilný	k2eAgInPc1d2	chladnomilný
druhy	druh	k1gInPc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
brambor	brambora	k1gFnPc2	brambora
se	se	k3xPyFc4	se
Norům	Nor	k1gMnPc3	Nor
značně	značně	k6eAd1	značně
ulevilo	ulevit	k5eAaPmAgNnS	ulevit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
nikdy	nikdy	k6eAd1	nikdy
plně	plně	k6eAd1	plně
neumožnila	umožnit	k5eNaPmAgFnS	umožnit
rozvoj	rozvoj	k1gInSc4	rozvoj
feudálního	feudální	k2eAgNnSc2d1	feudální
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
někteří	některý	k3yIgMnPc1	některý
králové	král	k1gMnPc1	král
odměňovali	odměňovat	k5eAaImAgMnP	odměňovat
půdou	půda	k1gFnSc7	půda
své	svůj	k3xOyFgInPc4	svůj
oddané	oddaný	k2eAgInPc4d1	oddaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samostatně	samostatně	k6eAd1	samostatně
hospodařící	hospodařící	k2eAgMnPc1d1	hospodařící
zemědělci	zemědělec	k1gMnPc1	zemědělec
měli	mít	k5eAaImAgMnP	mít
a	a	k8xC	a
stále	stále	k6eAd1	stále
mají	mít	k5eAaImIp3nP	mít
hlavní	hlavní	k2eAgInSc4d1	hlavní
podíl	podíl	k1gInSc4	podíl
při	při	k7c6	při
obhospodařování	obhospodařování	k1gNnSc6	obhospodařování
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19.	[number]	k4	19.
století	století	k1gNnPc2	století
byla	být	k5eAaImAgFnS	být
všechna	všechen	k3xTgFnSc1	všechen
dostupná	dostupný	k2eAgFnSc1d1	dostupná
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
zcela	zcela	k6eAd1	zcela
zabrána	zabrán	k2eAgFnSc1d1	zabrána
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
rodin	rodina	k1gFnPc2	rodina
pracovalo	pracovat	k5eAaImAgNnS	pracovat
v	v	k7c6	v
nájmu	nájem	k1gInSc6	nájem
pachtýřů	pachtýř	k1gMnPc2	pachtýř
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hybnou	hybný	k2eAgFnSc7d1	hybná
silou	síla	k1gFnSc7	síla
norské	norský	k2eAgFnSc2d1	norská
emigrace	emigrace	k1gFnSc2	emigrace
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třída	třída	k1gFnSc1	třída
obchodníků	obchodník	k1gMnPc2	obchodník
a	a	k8xC	a
úředníků	úředník	k1gMnPc2	úředník
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
jen	jen	k9	jen
málo	málo	k6eAd1	málo
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Bergenu	Bergen	k1gInSc6	Bergen
<g/>
,	,	kIx,	,
vzrůstal	vzrůstat	k5eAaImAgInS	vzrůstat
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
různým	různý	k2eAgNnSc7d1	různé
zbožím	zboží	k1gNnSc7	zboží
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
skutečně	skutečně	k6eAd1	skutečně
zámožní	zámožný	k2eAgMnPc1d1	zámožný
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
mezi	mezi	k7c7	mezi
lodními	lodní	k2eAgMnPc7d1	lodní
přepravci	přepravce	k1gMnPc7	přepravce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
revoluce	revoluce	k1gFnSc1	revoluce
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
dolů	dol	k1gInPc2	dol
v	v	k7c6	v
Kongsbergu	Kongsberg	k1gInSc6	Kongsberg
a	a	k8xC	a
Rø	Rø	k1gFnSc3	Rø
přicházela	přicházet	k5eAaImAgFnS	přicházet
industrializace	industrializace	k1gFnSc1	industrializace
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
prvních	první	k4xOgFnPc2	první
textilních	textilní	k2eAgFnPc2d1	textilní
továren	továrna	k1gFnPc2	továrna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
budovány	budovat	k5eAaImNgFnP	budovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19.	[number]	k4	19.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
velké	velký	k2eAgInPc1d1	velký
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
podniky	podnik	k1gInPc1	podnik
byly	být	k5eAaImAgInP	být
budovány	budovat	k5eAaImNgInP	budovat
kolem	kolem	k7c2	kolem
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Samem	Sam	k1gMnSc7	Sam
Eydem	Eyd	k1gMnSc7	Eyd
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
založeno	založit	k5eAaPmNgNnS	založit
Norsk	Norsk	k1gInSc1	Norsk
Hydro	hydra	k1gFnSc5	hydra
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc3d1	průmyslová
společnosti	společnost	k1gFnSc3	společnost
začaly	začít	k5eAaPmAgFnP	začít
růst	růst	k5eAaImF	růst
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Rjukan	Rjukan	k1gMnSc1	Rjukan
<g/>
,	,	kIx,	,
Odda	Odda	k1gMnSc1	Odda
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
norská	norský	k2eAgFnSc1d1	norská
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
přesáhla	přesáhnout	k5eAaPmAgFnS	přesáhnout
výrobu	výroba	k1gFnSc4	výroba
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
<g/>
.	.	kIx.	.
</s>
<s>
Přínos	přínos	k1gInSc1	přínos
průmyslu	průmysl	k1gInSc2	průmysl
byl	být	k5eAaImAgInS	být
zjevný	zjevný	k2eAgInSc1d1	zjevný
<g/>
,	,	kIx,	,
kapitál	kapitál	k1gInSc1	kapitál
byl	být	k5eAaImAgInS	být
více	hodně	k6eAd2	hodně
a	a	k8xC	a
více	hodně	k6eAd2	hodně
dosažitelný	dosažitelný	k2eAgMnSc1d1	dosažitelný
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
otvírány	otvírán	k2eAgFnPc1d1	otvírána
manufaktury	manufaktura	k1gFnPc1	manufaktura
–	–	k?	–
mlékárny	mlékárna	k1gFnSc2	mlékárna
<g/>
,	,	kIx,	,
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
papírny	papírna	k1gFnPc1	papírna
<g/>
,	,	kIx,	,
hutě	huť	k1gFnPc1	huť
<g/>
,	,	kIx,	,
továrny	továrna	k1gFnPc1	továrna
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
dřeva	dřevo	k1gNnSc2	dřevo
atd.	atd.	kA	atd.
Rozvoj	rozvoj	k1gInSc1	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
měnil	měnit	k5eAaImAgInS	měnit
i	i	k9	i
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
zakládání	zakládání	k1gNnSc3	zakládání
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
dalším	další	k2eAgFnPc3d1	další
potřebám	potřeba	k1gFnPc3	potřeba
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
průmyslu	průmysl	k1gInSc2	průmysl
odcházel	odcházet	k5eAaImAgInS	odcházet
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
již	již	k6eAd1	již
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
pojmout	pojmout	k5eAaPmF	pojmout
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
sektor	sektor	k1gInSc1	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Mzdy	mzda	k1gFnPc1	mzda
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
začaly	začít	k5eAaPmAgInP	začít
přesahovat	přesahovat	k5eAaImF	přesahovat
výdělky	výdělek	k1gInPc1	výdělek
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
začal	začít	k5eAaPmAgInS	začít
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
trend	trend	k1gInSc1	trend
snižování	snižování	k1gNnSc2	snižování
podílu	podíl	k1gInSc2	podíl
obdělávané	obdělávaný	k2eAgFnSc2d1	obdělávaná
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
trend	trend	k1gInSc4	trend
snižování	snižování	k1gNnSc4	snižování
počtu	počet	k1gInSc2	počet
venkovského	venkovský	k2eAgNnSc2d1	venkovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pracující	pracující	k2eAgFnSc1d1	pracující
třída	třída	k1gFnSc1	třída
se	se	k3xPyFc4	se
stávala	stávat	k5eAaImAgFnS	stávat
výrazným	výrazný	k2eAgInSc7d1	výrazný
fenoménem	fenomén	k1gInSc7	fenomén
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
vlastní	vlastní	k2eAgFnPc4d1	vlastní
čtvrtě	čtvrt	k1gFnPc4	čtvrt
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Etnicky	etnicky	k6eAd1	etnicky
jsou	být	k5eAaImIp3nP	být
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Norska	Norsko	k1gNnSc2	Norsko
hlavně	hlavně	k9	hlavně
Norové	Norové	k2eAgNnSc1d1	Norové
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
původní	původní	k2eAgFnSc1d1	původní
komunita	komunita	k1gFnSc1	komunita
Sámů	Sámo	k1gMnPc2	Sámo
(	(	kIx(	(
<g/>
Laponců	Laponec	k1gMnPc2	Laponec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
překročil	překročit	k5eAaPmAgInS	překročit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
16	[number]	k4	16
%	%	kIx~	%
(	(	kIx(	(
<g/>
805	[number]	k4	805
000	[number]	k4	000
<g/>
)	)	kIx)	)
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
původ	původ	k1gInSc1	původ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
92 000	[number]	k4	92 000
Poláků	Polák	k1gMnPc2	Polák
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tak	tak	k6eAd1	tak
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
nejsilnější	silný	k2eAgFnSc4d3	nejsilnější
přistěhovaleckou	přistěhovalecký	k2eAgFnSc4d1	přistěhovalecká
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
450 000	[number]	k4	450 000
lidí	člověk	k1gMnPc2	člověk
neevropského	evropský	k2eNgInSc2d1	neevropský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
36 000	[number]	k4	36 000
Pákistánců	Pákistánec	k1gMnPc2	Pákistánec
<g/>
,	,	kIx,	,
38 000	[number]	k4	38 000
Somálců	Somálec	k1gMnPc2	Somálec
a	a	k8xC	a
30 000	[number]	k4	30 000
Iráčanů	Iráčan	k1gMnPc2	Iráčan
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
velkou	velký	k2eAgFnSc4d1	velká
skupinu	skupina	k1gFnSc4	skupina
Afričanů	Afričan	k1gMnPc2	Afričan
a	a	k8xC	a
Asiatů	Asiat	k1gMnPc2	Asiat
žije	žít	k5eAaImIp3nS	žít
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
ještě	ještě	k6eAd1	ještě
40 000	[number]	k4	40 000
Litevců	Litevec	k1gMnPc2	Litevec
a	a	k8xC	a
39 000	[number]	k4	39 000
Švédů	Švéd	k1gMnPc2	Švéd
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
oficiální	oficiální	k2eAgFnPc1d1	oficiální
spisovné	spisovný	k2eAgFnPc1d1	spisovná
varianty	varianta	k1gFnPc1	varianta
norštiny	norština	k1gFnSc2	norština
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
bokmå	bokmå	k?	bokmå
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
dánštinou	dánština	k1gFnSc7	dánština
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
let	léto	k1gNnPc2	léto
úředním	úřední	k2eAgMnSc7d1	úřední
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
,	,	kIx,	,
nynorsk	nynorsk	k1gInSc1	nynorsk
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
venkovských	venkovský	k2eAgNnPc2d1	venkovské
nářečí	nářečí	k1gNnPc2	nářečí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
uchovala	uchovat	k5eAaPmAgFnS	uchovat
staronorské	staronorský	k2eAgInPc4d1	staronorský
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
vláda	vláda	k1gFnSc1	vláda
podporuje	podporovat	k5eAaImIp3nS	podporovat
politiku	politika	k1gFnSc4	politika
multikulturalismu	multikulturalismus	k1gInSc2	multikulturalismus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
prospěšnost	prospěšnost	k1gFnSc4	prospěšnost
kulturní	kulturní	k2eAgFnSc2d1	kulturní
<g/>
,	,	kIx,	,
etnické	etnický	k2eAgFnSc2d1	etnická
a	a	k8xC	a
náboženské	náboženský	k2eAgFnSc2d1	náboženská
rozmanitosti	rozmanitost	k1gFnSc2	rozmanitost
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1	vládní
koalice	koalice	k1gFnSc1	koalice
premiéra	premiér	k1gMnSc2	premiér
Jense	Jens	k1gMnSc2	Jens
Stoltenberga	Stoltenberg	k1gMnSc2	Stoltenberg
zahrnula	zahrnout	k5eAaPmAgFnS	zahrnout
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
vládního	vládní	k2eAgInSc2d1	vládní
programu	program	k1gInSc2	program
prosazování	prosazování	k1gNnSc2	prosazování
multikulturní	multikulturní	k2eAgFnSc2d1	multikulturní
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
diverzita	diverzita	k1gFnSc1	diverzita
obohacuje	obohacovat	k5eAaImIp3nS	obohacovat
norskou	norský	k2eAgFnSc4d1	norská
společnost	společnost	k1gFnSc4	společnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Norská	norský	k2eAgFnSc1d1	norská
literární	literární	k2eAgFnSc1d1	literární
věda	věda	k1gFnSc1	věda
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
"	"	kIx"	"
<g/>
Velké	velký	k2eAgFnSc6d1	velká
čtyřce	čtyřka	k1gFnSc6	čtyřka
<g/>
"	"	kIx"	"
klasických	klasický	k2eAgMnPc2d1	klasický
norských	norský	k2eAgMnPc2d1	norský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
Henrik	Henrik	k1gMnSc1	Henrik
Ibsen	Ibsen	k1gMnSc1	Ibsen
<g/>
,	,	kIx,	,
Bjø	Bjø	k1gMnSc1	Bjø
Bjø	Bjø	k1gMnSc1	Bjø
<g/>
,	,	kIx,	,
Jonas	Jonas	k1gMnSc1	Jonas
Lie	Lie	k1gFnSc2	Lie
a	a	k8xC	a
Alexander	Alexandra	k1gFnPc2	Alexandra
Kielland	Kiellanda	k1gFnPc2	Kiellanda
<g/>
.	.	kIx.	.
</s>
<s>
Bjø	Bjø	k?	Bjø
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Sigrid	Sigrid	k1gInSc4	Sigrid
Undsetová	Undsetový	k2eAgFnSc1d1	Undsetová
a	a	k8xC	a
Knut	knuta	k1gFnPc2	knuta
Hamsun	Hamsuna	k1gFnPc2	Hamsuna
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
norské	norský	k2eAgFnSc2d1	norská
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
Ludvig	Ludvig	k1gMnSc1	Ludvig
Holberg	Holberg	k1gMnSc1	Holberg
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
průkopníkům	průkopník	k1gMnPc3	průkopník
moderní	moderní	k2eAgFnSc2d1	moderní
literatury	literatura	k1gFnSc2	literatura
patřil	patřit	k5eAaImAgMnS	patřit
též	též	k9	též
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
Henrik	Henrik	k1gMnSc1	Henrik
Wergeland	Wergelando	k1gNnPc2	Wergelando
či	či	k8xC	či
Arne	Arne	k1gMnSc1	Arne
Garborg	Garborg	k1gMnSc1	Garborg
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
norštiny	norština	k1gFnSc2	norština
Odysseu	Odyssea	k1gFnSc4	Odyssea
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
Tarjei	Tarje	k1gFnSc2	Tarje
Vesaas	Vesaas	k1gInSc1	Vesaas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
představitelem	představitel	k1gMnSc7	představitel
rozmachu	rozmach	k1gInSc2	rozmach
skandinávské	skandinávský	k2eAgFnSc2d1	skandinávská
detektivky	detektivka	k1gFnSc2	detektivka
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
21.	[number]	k4	21.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
Jo	jo	k9	jo
Nesbø	Nesbø	k1gFnSc1	Nesbø
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
oceňovaným	oceňovaný	k2eAgMnSc7d1	oceňovaný
současným	současný	k2eAgMnSc7d1	současný
autorem	autor	k1gMnSc7	autor
dětské	dětský	k2eAgFnSc2d1	dětská
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
Jostein	Jostein	k2eAgInSc1d1	Jostein
Gaarder	Gaarder	k1gInSc1	Gaarder
<g/>
,	,	kIx,	,
v	v	k7c6	v
humoristické	humoristický	k2eAgFnSc6d1	humoristická
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Erlend	Erlend	k1gMnSc1	Erlend
Loe	Loe	k1gMnSc1	Loe
<g/>
,	,	kIx,	,
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
beletrii	beletrie	k1gFnSc6	beletrie
Lars	Larsa	k1gFnPc2	Larsa
Saabye	Saabye	k1gFnSc1	Saabye
Christensen	Christensen	k1gInSc1	Christensen
<g/>
.	.	kIx.	.
</s>
<s>
Nejuznávanějším	uznávaný	k2eAgMnSc7d3	nejuznávanější
současným	současný	k2eAgMnSc7d1	současný
dramatikem	dramatik	k1gMnSc7	dramatik
je	být	k5eAaImIp3nS	být
Jon	Jon	k1gMnSc1	Jon
Fosse	Fosse	k1gFnSc2	Fosse
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
norským	norský	k2eAgMnSc7d1	norský
malířem	malíř	k1gMnSc7	malíř
je	být	k5eAaImIp3nS	být
bezpochyby	bezpochyby	k6eAd1	bezpochyby
Edvard	Edvard	k1gMnSc1	Edvard
Munch	Munch	k1gMnSc1	Munch
<g/>
.	.	kIx.	.
</s>
<s>
Theodor	Theodor	k1gMnSc1	Theodor
Kittelsen	Kittelsen	k2eAgMnSc1d1	Kittelsen
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
jako	jako	k9	jako
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
starých	starý	k2eAgFnPc2d1	stará
bájí	báj	k1gFnPc2	báj
a	a	k8xC	a
lidových	lidový	k2eAgFnPc2d1	lidová
pověstí	pověst	k1gFnPc2	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
sochařem	sochař	k1gMnSc7	sochař
je	být	k5eAaImIp3nS	být
Gustav	Gustav	k1gMnSc1	Gustav
Vigeland	Vigelanda	k1gFnPc2	Vigelanda
<g/>
,	,	kIx,	,
architektem	architekt	k1gMnSc7	architekt
Sverre	Sverr	k1gInSc5	Sverr
Fehn	Fehn	k1gMnSc1	Fehn
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
prestižní	prestižní	k2eAgFnSc2d1	prestižní
Pritzkerovy	Pritzkerův	k2eAgFnSc2d1	Pritzkerova
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
norská	norský	k2eAgFnSc1d1	norská
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
datována	datovat	k5eAaImNgFnS	datovat
rokem	rok	k1gInSc7	rok
1839	[number]	k4	1839
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Hans	Hans	k1gMnSc1	Hans
Thø	Thø	k1gMnSc1	Thø
Winther	Winthra	k1gFnPc2	Winthra
přivezl	přivézt	k5eAaPmAgMnS	přivézt
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
fotografie	fotografie	k1gFnSc1	fotografie
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
prvních	první	k4xOgInPc2	první
snímků	snímek	k1gInPc2	snímek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
<g/>
.	.	kIx.	.
</s>
<s>
Daguerrotypie	daguerrotypie	k1gFnSc1	daguerrotypie
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
mnoho	mnoho	k6eAd1	mnoho
pionýrských	pionýrský	k2eAgMnPc2d1	pionýrský
fotografů	fotograf	k1gMnPc2	fotograf
otevíralo	otevírat	k5eAaImAgNnS	otevírat
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
ateliéry	ateliér	k1gInPc1	ateliér
zaměřené	zaměřený	k2eAgInPc1d1	zaměřený
především	především	k6eAd1	především
na	na	k7c6	na
portrétní	portrétní	k2eAgFnSc6d1	portrétní
fotografii	fotografia	k1gFnSc6	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
menších	malý	k2eAgFnPc2d2	menší
obcí	obec	k1gFnPc2	obec
fotografové	fotograf	k1gMnPc1	fotograf
přicházeli	přicházet	k5eAaImAgMnP	přicházet
s	s	k7c7	s
mobilním	mobilní	k2eAgNnSc7d1	mobilní
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
fotografy	fotograf	k1gMnPc7	fotograf
byl	být	k5eAaImAgInS	být
významný	významný	k2eAgInSc1d1	významný
podíl	podíl	k1gInSc1	podíl
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Thø	Thø	k?	Thø
Winther	Winthra	k1gFnPc2	Winthra
publikoval	publikovat	k5eAaBmAgMnS	publikovat
množství	množství	k1gNnSc3	množství
novinových	novinový	k2eAgMnPc2d1	novinový
článků	článek	k1gInPc2	článek
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
fotografie	fotografia	k1gFnSc2	fotografia
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
i	i	k8xC	i
knihu	kniha	k1gFnSc4	kniha
fotografických	fotografický	k2eAgInPc2d1	fotografický
návodů	návod	k1gInPc2	návod
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
pozici	pozice	k1gFnSc4	pozice
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
v	v	k7c6	v
norské	norský	k2eAgFnSc6d1	norská
fotografii	fotografia	k1gFnSc6	fotografia
také	také	k9	také
krajinářská	krajinářský	k2eAgFnSc1d1	krajinářská
fotografie	fotografie	k1gFnSc1	fotografie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zde	zde	k6eAd1	zde
zapustila	zapustit	k5eAaPmAgFnS	zapustit
kořeny	kořen	k1gInPc4	kořen
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
.	.	kIx.	.
</s>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Selmer	Selmer	k1gMnSc1	Selmer
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
krajinářů	krajinář	k1gMnPc2	krajinář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsystematičtější	systematický	k2eAgNnSc1d3	nejsystematičtější
úsilí	úsilí	k1gNnSc1	úsilí
cestování	cestování	k1gNnSc2	cestování
a	a	k8xC	a
fotografování	fotografování	k1gNnSc2	fotografování
krajiny	krajina	k1gFnSc2	krajina
věnovali	věnovat	k5eAaImAgMnP	věnovat
Knud	Knud	k1gInSc4	Knud
Knudsen	Knudsna	k1gFnPc2	Knudsna
a	a	k8xC	a
Švéd	Švéd	k1gMnSc1	Švéd
Axel	Axel	k1gMnSc1	Axel
Lindahl	Lindahl	k1gMnSc1	Lindahl
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
vývoj	vývoj	k1gInSc1	vývoj
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
řemesla	řemeslo	k1gNnSc2	řemeslo
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
fotografie	fotografia	k1gFnSc2	fotografia
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
důsledkem	důsledek	k1gInSc7	důsledek
změn	změna	k1gFnPc2	změna
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
zlepšování	zlepšování	k1gNnSc1	zlepšování
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
míry	míra	k1gFnSc2	míra
uznání	uznání	k1gNnSc2	uznání
fotografie	fotografia	k1gFnSc2	fotografia
jako	jako	k8xS	jako
svéprávné	svéprávný	k2eAgFnSc2d1	svéprávná
formy	forma	k1gFnSc2	forma
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Světově	světově	k6eAd1	světově
proslulým	proslulý	k2eAgMnSc7d1	proslulý
hudebním	hudební	k2eAgMnSc7d1	hudební
skladatelem	skladatel	k1gMnSc7	skladatel
je	být	k5eAaImIp3nS	být
Edvard	Edvard	k1gMnSc1	Edvard
Grieg	Grieg	k1gMnSc1	Grieg
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známým	známý	k2eAgMnPc3d1	známý
skladatelům	skladatel	k1gMnPc3	skladatel
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Johan	Johan	k1gMnSc1	Johan
Svendsen	Svendsen	k2eAgMnSc1d1	Svendsen
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
interpretů	interpret	k1gMnPc2	interpret
lze	lze	k6eAd1	lze
vzpomenout	vzpomenout	k5eAaPmF	vzpomenout
operní	operní	k2eAgFnSc3d1	operní
pěvkyni	pěvkyně	k1gFnSc3	pěvkyně
Sissel	Sissel	k1gMnSc1	Sissel
Kyrkjebø	Kyrkjebø	k1gMnSc1	Kyrkjebø
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
v	v	k7c6	v
80.	[number]	k4	80.
letech	let	k1gInPc6	let
synthpopová	synthpopový	k2eAgFnSc1d1	synthpopová
skupina	skupina	k1gFnSc1	skupina
A-ha	Aum	k1gNnSc2	A-hum
s	s	k7c7	s
frontmanem	frontman	k1gMnSc7	frontman
Mortenem	Morten	k1gMnSc7	Morten
Harketem	Harket	k1gMnSc7	Harket
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
verzi	verze	k1gFnSc4	verze
soutěže	soutěž	k1gFnSc2	soutěž
Pop	pop	k1gInSc1	pop
Idol	idol	k1gInSc1	idol
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Kurt	kurt	k1gInSc4	kurt
Nilsen	Nilsen	k2eAgInSc4d1	Nilsen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
hudbě	hudba	k1gFnSc6	hudba
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
například	například	k6eAd1	například
Alexander	Alexandra	k1gFnPc2	Alexandra
Rybak	Rybak	k1gMnSc1	Rybak
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
je	být	k5eAaImIp3nS	být
také	také	k9	také
dívčí	dívčí	k2eAgFnSc1d1	dívčí
skupina	skupina	k1gFnSc1	skupina
Katzenjammer	Katzenjammra	k1gFnPc2	Katzenjammra
a	a	k8xC	a
z	z	k7c2	z
mladší	mladý	k2eAgFnSc2d2	mladší
generace	generace	k1gFnSc2	generace
duo	duo	k1gNnSc1	duo
Marcus	Marcus	k1gInSc1	Marcus
&	&	k?	&
Martinus	Martinus	k1gInSc1	Martinus
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
DJ	DJ	kA	DJ
Alan	Alan	k1gMnSc1	Alan
Walker	Walker	k1gMnSc1	Walker
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
svým	svůj	k3xOyFgInSc7	svůj
hitem	hit	k1gInSc7	hit
"	"	kIx"	"
<g/>
Faded	Faded	k1gInSc1	Faded
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015.	[number]	k4	2015.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgMnSc1d1	populární
také	také	k9	také
hard	hard	k1gMnSc1	hard
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xS	jako
například	například	k6eAd1	například
Gluecifer	Gluecifra	k1gFnPc2	Gluecifra
nebo	nebo	k8xC	nebo
The	The	k1gFnPc2	The
Carburetors	Carburetorsa	k1gFnPc2	Carburetorsa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
skupina	skupina	k1gFnSc1	skupina
Turbonegro	Turbonegro	k1gNnSc1	Turbonegro
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svůj	svůj	k3xOyFgInSc4	svůj
žánr	žánr	k1gInSc1	žánr
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
death-punk	deathunk	k1gInSc4	death-punk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
má	mít	k5eAaImIp3nS	mít
tisíce	tisíc	k4xCgInPc1	tisíc
fanoušků	fanoušek	k1gMnPc2	fanoušek
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
Světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
Turbojugend	Turbojugend	k1gInSc4	Turbojugend
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
začal	začít	k5eAaPmAgInS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
hudební	hudební	k2eAgInSc1d1	hudební
subžánr	subžánr	k1gInSc1	subžánr
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
zvaný	zvaný	k2eAgInSc1d1	zvaný
black	black	k1gInSc1	black
metal	metat	k5eAaImAgInS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgFnPc4	první
kapely	kapela	k1gFnPc4	kapela
patřily	patřit	k5eAaImAgInP	patřit
Mayhem	Mayh	k1gInSc7	Mayh
<g/>
,	,	kIx,	,
Burzum	Burzum	k1gInSc1	Burzum
<g/>
,	,	kIx,	,
Darkthrone	Darkthron	k1gInSc5	Darkthron
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgFnPc2d3	nejvýraznější
osobností	osobnost	k1gFnPc2	osobnost
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
black	black	k1gMnSc1	black
metalu	metal	k1gInSc2	metal
byl	být	k5eAaImAgMnS	být
Ø	Ø	k?	Ø
Euronymous	Euronymous	k1gMnSc1	Euronymous
Aarseth	Aarseth	k1gMnSc1	Aarseth
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
Vargem	Vargem	k?	Vargem
Vikernesem	Vikernes	k1gInSc7	Vikernes
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zároveň	zároveň	k6eAd1	zároveň
podpálil	podpálit	k5eAaPmAgMnS	podpálit
několik	několik	k4yIc4	několik
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
kostelů	kostel	k1gInPc2	kostel
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
pomstít	pomstít	k5eAaPmF	pomstít
vymýcení	vymýcení	k1gNnSc4	vymýcení
původního	původní	k2eAgNnSc2d1	původní
náboženství	náboženství	k1gNnSc2	náboženství
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
témata	téma	k1gNnPc4	téma
black	black	k1gInSc1	black
metalu	metal	k1gInSc2	metal
patří	patřit	k5eAaImIp3nS	patřit
temnota	temnota	k1gFnSc1	temnota
a	a	k8xC	a
misantropie	misantropie	k1gFnSc1	misantropie
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
stinné	stinný	k2eAgFnPc4d1	stinná
stránky	stránka	k1gFnPc4	stránka
lidské	lidský	k2eAgFnSc2d1	lidská
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k8xC	i
když	když	k8xS	když
mnoho	mnoho	k4c1	mnoho
kapel	kapela	k1gFnPc2	kapela
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
textech	text	k1gInPc6	text
satanismus	satanismus	k1gInSc4	satanismus
jako	jako	k8xS	jako
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
spíše	spíše	k9	spíše
o	o	k7c4	o
metaforické	metaforický	k2eAgNnSc4d1	metaforické
vyjádření	vyjádření	k1gNnSc4	vyjádření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
nenávisti	nenávist	k1gFnSc2	nenávist
vůči	vůči	k7c3	vůči
náboženstvím	náboženství	k1gNnPc3	náboženství
<g/>
,	,	kIx,	,
církvím	církev	k1gFnPc3	církev
a	a	k8xC	a
jiným	jiný	k1gMnPc3	jiný
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejich	jejich	k3xOp3gInSc2	jejich
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
pochybným	pochybný	k2eAgFnPc3d1	pochybná
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Black	Black	k6eAd1	Black
metal	metal	k1gInSc1	metal
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
dokument	dokument	k1gInSc4	dokument
Until	Until	k1gMnSc1	Until
the	the	k?	the
Light	Light	k1gMnSc1	Light
Takes	Takes	k1gMnSc1	Takes
Us	Us	k1gMnSc1	Us
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008.	[number]	k4	2008.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
byl	být	k5eAaImAgMnS	být
nominován	nominován	k2eAgMnSc1d1	nominován
režisér	režisér	k1gMnSc1	režisér
Morten	Morten	k2eAgInSc4d1	Morten
Tyldum	Tyldum	k1gInSc4	Tyldum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
švédského	švédský	k2eAgMnSc2d1	švédský
režiséra	režisér	k1gMnSc2	režisér
Ingmara	Ingmar	k1gMnSc2	Ingmar
Bergmana	Bergman	k1gMnSc2	Bergman
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
norská	norský	k2eAgFnSc1d1	norská
herečka	herečka	k1gFnSc1	herečka
Liv	Liv	k1gFnSc1	Liv
Ullmannová	Ullmannová	k1gFnSc1	Ullmannová
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
úspěchu	úspěch	k1gInSc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
norský	norský	k2eAgInSc1d1	norský
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
Okupace	okupace	k1gFnSc1	okupace
(	(	kIx(	(
<g/>
Okkupert	Okkupert	k1gInSc1	Okkupert
<g/>
)	)	kIx)	)
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2015	[number]	k4	2015
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
vznikají	vznikat	k5eAaImIp3nP	vznikat
také	také	k9	také
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
velkofilmy	velkofilm	k1gInPc4	velkofilm
jako	jako	k8xS	jako
například	například	k6eAd1	například
Max	Max	k1gMnSc1	Max
Manus	Manus	k1gMnSc1	Manus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
o	o	k7c6	o
norském	norský	k2eAgInSc6d1	norský
odbojáři	odbojář	k1gMnPc1	odbojář
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
také	také	k9	také
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
románu	román	k1gInSc2	román
Joa	Joa	k1gFnSc1	Joa
Nesbø	Nesbø	k1gFnSc2	Nesbø
Lovci	lovec	k1gMnPc1	lovec
hlav	hlava	k1gFnPc2	hlava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
filmech	film	k1gInPc6	film
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
populární	populární	k2eAgMnSc1d1	populární
norský	norský	k2eAgMnSc1d1	norský
herec	herec	k1gMnSc1	herec
Aksel	Aksel	k1gMnSc1	Aksel
Hennie	Hennie	k1gFnSc1	Hennie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Gastronomie	gastronomie	k1gFnSc2	gastronomie
===	===	k?	===
</s>
</p>
<p>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
skandinávská	skandinávský	k2eAgFnSc1d1	skandinávská
kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
"	"	kIx"	"
<g/>
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
<g/>
"	"	kIx"	"
–	–	k?	–
skromná	skromný	k2eAgFnSc1d1	skromná
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
nápaditá	nápaditý	k2eAgFnSc1d1	nápaditá
<g/>
.	.	kIx.	.
</s>
<s>
Pravdou	pravda	k1gFnSc7	pravda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ta	ten	k3xDgFnSc1	ten
norská	norský	k2eAgFnSc1d1	norská
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
myslet	myslet	k5eAaImF	myslet
velmi	velmi	k6eAd1	velmi
tradičně	tradičně	k6eAd1	tradičně
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
postupy	postup	k1gInPc1	postup
a	a	k8xC	a
suroviny	surovina	k1gFnPc1	surovina
známé	známý	k2eAgFnPc1d1	známá
už	už	k9	už
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Vikingů	Viking	k1gMnPc2	Viking
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc7d1	hlavní
surovinou	surovina	k1gFnSc7	surovina
jsou	být	k5eAaImIp3nP	být
různá	různý	k2eAgNnPc1d1	různé
masa	maso	k1gNnPc1	maso
–	–	k?	–
klasické	klasický	k2eAgNnSc4d1	klasické
je	být	k5eAaImIp3nS	být
maso	maso	k1gNnSc1	maso
jehněčí	jehněčí	k2eAgNnSc1d1	jehněčí
a	a	k8xC	a
zvěřina	zvěřina	k1gFnSc1	zvěřina
jako	jako	k8xC	jako
sob	sob	k1gMnSc1	sob
a	a	k8xC	a
los	los	k1gInSc1	los
<g/>
.	.	kIx.	.
</s>
<s>
Masa	maso	k1gNnPc1	maso
se	se	k3xPyFc4	se
upravují	upravovat	k5eAaImIp3nP	upravovat
hlavně	hlavně	k9	hlavně
vařením	vaření	k1gNnSc7	vaření
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
pečením	pečeně	k1gFnPc3	pečeně
<g/>
.	.	kIx.	.
</s>
<s>
Tradičními	tradiční	k2eAgInPc7d1	tradiční
recepty	recept	k1gInPc7	recept
jsou	být	k5eAaImIp3nP	být
få	få	k?	få
–	–	k?	–
pečené	pečený	k2eAgNnSc4d1	pečené
jehněčí	jehněčí	k1gNnSc4	jehněčí
<g/>
,	,	kIx,	,
finnbiff	finnbiff	k1gMnSc1	finnbiff
–	–	k?	–
vařené	vařený	k2eAgNnSc1d1	vařené
sobí	sobí	k2eAgNnSc1d1	sobí
maso	maso	k1gNnSc1	maso
s	s	k7c7	s
houbami	houba	k1gFnPc7	houba
a	a	k8xC	a
krémovou	krémový	k2eAgFnSc7d1	krémová
omáčkou	omáčka	k1gFnSc7	omáčka
<g/>
,	,	kIx,	,
elggryte	elggryt	k1gInSc5	elggryt
–	–	k?	–
vařené	vařený	k2eAgNnSc1d1	vařené
losí	losí	k2eAgNnSc1d1	losí
maso	maso	k1gNnSc1	maso
s	s	k7c7	s
cibulí	cibule	k1gFnSc7	cibule
a	a	k8xC	a
jalovcem	jalovec	k1gInSc7	jalovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
jídelníčku	jídelníček	k1gInSc6	jídelníček
mají	mít	k5eAaImIp3nP	mít
velké	velký	k2eAgNnSc4d1	velké
zastoupení	zastoupení	k1gNnSc4	zastoupení
ryby	ryba	k1gFnSc2	ryba
a	a	k8xC	a
"	"	kIx"	"
<g/>
dary	dar	k1gInPc1	dar
moře	moře	k1gNnSc2	moře
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětově	celosvětově	k6eAd1	celosvětově
známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
norský	norský	k2eAgMnSc1d1	norský
losos	losos	k1gMnSc1	losos
a	a	k8xC	a
lososí	lososí	k2eAgMnSc1d1	lososí
tatarák	tatarák	k1gMnSc1	tatarák
(	(	kIx(	(
<g/>
laksetartar	laksetartar	k1gMnSc1	laksetartar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
treska	treska	k1gFnSc1	treska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ryby	ryba	k1gFnPc4	ryba
sladkovodní	sladkovodní	k2eAgFnPc4d1	sladkovodní
z	z	k7c2	z
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
řek	řeka	k1gFnPc2	řeka
-	-	kIx~	-
specialitou	specialita	k1gFnSc7	specialita
je	být	k5eAaImIp3nS	být
pstruh	pstruh	k1gMnSc1	pstruh
(	(	kIx(	(
<g/>
ø	ø	k?	ø
<g/>
)	)	kIx)	)
z	z	k7c2	z
Hardangerviddy	Hardangervidda	k1gFnSc2	Hardangervidda
<g/>
.	.	kIx.	.
</s>
<s>
Unikátní	unikátní	k2eAgFnSc4d1	unikátní
chuť	chuť	k1gFnSc4	chuť
má	mít	k5eAaImIp3nS	mít
velrybí	velrybí	k2eAgNnSc1d1	velrybí
maso	maso	k1gNnSc1	maso
-	-	kIx~	-
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
jsou	být	k5eAaImIp3nP	být
připravované	připravovaný	k2eAgInPc1d1	připravovaný
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
steaků	steak	k1gInPc2	steak
(	(	kIx(	(
<g/>
hvalbiff	hvalbiff	k1gInSc1	hvalbiff
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rybáři	rybář	k1gMnPc1	rybář
ze	z	k7c2	z
severských	severský	k2eAgInPc2d1	severský
krajů	kraj	k1gInPc2	kraj
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
i	i	k8xC	i
maso	maso	k1gNnSc4	maso
tuleňů	tuleň	k1gMnPc2	tuleň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přílohou	příloha	k1gFnSc7	příloha
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
jsou	být	k5eAaImIp3nP	být
brambory	brambora	k1gFnPc1	brambora
a	a	k8xC	a
kořenová	kořenový	k2eAgFnSc1d1	kořenová
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Norové	Nor	k1gMnPc1	Nor
také	také	k9	také
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
sýry	sýr	k1gInPc1	sýr
a	a	k8xC	a
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
karamelovou	karamelový	k2eAgFnSc4d1	karamelová
příchuť	příchuť	k1gFnSc4	příchuť
má	mít	k5eAaImIp3nS	mít
tradiční	tradiční	k2eAgInSc1d1	tradiční
sýr	sýr	k1gInSc1	sýr
Brunost	Brunost	k1gFnSc1	Brunost
<g/>
,	,	kIx,	,
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
jak	jak	k8xC	jak
z	z	k7c2	z
kravského	kravský	k2eAgNnSc2d1	kravské
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
kozího	kozí	k2eAgNnSc2d1	kozí
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
jídelníčku	jídelníček	k1gInSc6	jídelníček
nechybí	chybit	k5eNaPmIp3nP	chybit
také	také	k9	také
dezerty	dezert	k1gInPc1	dezert
<g/>
:	:	kIx,	:
vafle	vafle	k1gFnSc1	vafle
se	s	k7c7	s
zakysanou	zakysaný	k2eAgFnSc7d1	zakysaná
smetanou	smetana	k1gFnSc7	smetana
a	a	k8xC	a
různými	různý	k2eAgInPc7d1	různý
druhy	druh	k1gInPc7	druh
marmelád	marmeláda	k1gFnPc2	marmeláda
a	a	k8xC	a
džemů	džem	k1gInPc2	džem
z	z	k7c2	z
lesního	lesní	k2eAgNnSc2d1	lesní
ovoce	ovoce	k1gNnSc2	ovoce
nebo	nebo	k8xC	nebo
Lofotských	Lofotský	k2eAgFnPc2d1	Lofotský
jahod	jahoda	k1gFnPc2	jahoda
<g/>
.	.	kIx.	.
<g/>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Norsko	Norsko	k1gNnSc1	Norsko
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
spotřebitelů	spotřebitel	k1gMnPc2	spotřebitel
mražené	mražený	k2eAgFnSc2d1	mražená
pizzy	pizza	k1gFnSc2	pizza
Grandiosa	Grandiosa	k1gFnSc1	Grandiosa
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
mexické	mexický	k2eAgInPc4d1	mexický
tacos	tacos	k1gInSc4	tacos
(	(	kIx(	(
<g/>
Tex-Mex	Tex-Mex	k1gInSc4	Tex-Mex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Proslavená	proslavený	k2eAgFnSc1d1	proslavená
je	být	k5eAaImIp3nS	být
norská	norský	k2eAgFnSc1d1	norská
matematická	matematický	k2eAgFnSc1d1	matematická
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
světových	světový	k2eAgMnPc2d1	světový
matematiků	matematik	k1gMnPc2	matematik
byl	být	k5eAaImAgInS	být
Niels	Niels	k1gInSc1	Niels
Henrik	Henrik	k1gMnSc1	Henrik
Abel	Abel	k1gMnSc1	Abel
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
funkcionální	funkcionální	k2eAgFnSc3d1	funkcionální
analýze	analýza	k1gFnSc3	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Sophus	Sophus	k1gMnSc1	Sophus
Lie	Lie	k1gMnSc1	Lie
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
teorii	teorie	k1gFnSc4	teorie
spojité	spojitý	k2eAgFnSc2d1	spojitá
symetrie	symetrie	k1gFnSc2	symetrie
<g/>
,	,	kIx,	,
Atle	Atle	k1gNnSc1	Atle
Selberg	Selberg	k1gMnSc1	Selberg
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
teorie	teorie	k1gFnSc2	teorie
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
Caspar	Caspar	k1gMnSc1	Caspar
Wessel	Wessel	k1gMnSc1	Wessel
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
geometrickou	geometrický	k2eAgFnSc4d1	geometrická
interpretaci	interpretace	k1gFnSc4	interpretace
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Ludwig	Ludwig	k1gMnSc1	Ludwig
Mejdell	Mejdell	k1gMnSc1	Mejdell
Sylow	Sylow	k1gMnSc1	Sylow
rozvinul	rozvinout	k5eAaPmAgMnS	rozvinout
teorii	teorie	k1gFnSc4	teorie
grup	grupa	k1gFnPc2	grupa
<g/>
,	,	kIx,	,
Axel	Axela	k1gFnPc2	Axela
Thue	Thu	k1gInSc2	Thu
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
zakladatele	zakladatel	k1gMnPc4	zakladatel
formálních	formální	k2eAgInPc2d1	formální
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
skandinávských	skandinávský	k2eAgFnPc6d1	skandinávská
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
také	také	k9	také
informatika	informatika	k1gFnSc1	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
programátoři	programátor	k1gMnPc1	programátor
Ole-Johan	Ole-Johana	k1gFnPc2	Ole-Johana
Dahl	dahl	k1gInSc4	dahl
a	a	k8xC	a
Kristen	Kristen	k2eAgInSc4d1	Kristen
Nygaard	Nygaard	k1gInSc4	Nygaard
byli	být	k5eAaImAgMnP	být
tvůrci	tvůrce	k1gMnPc1	tvůrce
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
Simula	Simula	k1gFnSc1	Simula
67	[number]	k4	67
<g/>
.	.	kIx.	.
</s>
<s>
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
získal	získat	k5eAaPmAgMnS	získat
Ivar	Ivar	k1gMnSc1	Ivar
Giaever	Giaever	k1gMnSc1	Giaever
<g/>
,	,	kIx,	,
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Lars	Larsa	k1gFnPc2	Larsa
Onsager	Onsagra	k1gFnPc2	Onsagra
<g/>
,	,	kIx,	,
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
May-Britt	May-Britta	k1gFnPc2	May-Britta
Moserová	Moserová	k1gFnSc1	Moserová
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Edvard	Edvard	k1gMnSc1	Edvard
Moser	Moser	k1gMnSc1	Moser
<g/>
.	.	kIx.	.
</s>
<s>
Vilhelm	Vilhelm	k1gMnSc1	Vilhelm
Bjerknes	Bjerknes	k1gMnSc1	Bjerknes
je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
meteorologie	meteorologie	k1gFnSc2	meteorologie
<g/>
.	.	kIx.	.
</s>
<s>
Gerhard	Gerhard	k1gInSc1	Gerhard
Armauer	Armauer	k1gInSc1	Armauer
Hansen	Hansen	k1gInSc1	Hansen
objevil	objevit	k5eAaPmAgInS	objevit
bakteriálního	bakteriální	k2eAgMnSc4d1	bakteriální
původce	původce	k1gMnSc4	původce
lepry	lepra	k1gFnSc2	lepra
<g/>
.	.	kIx.	.
</s>
<s>
Linného	Linný	k1gMnSc4	Linný
žákem	žák	k1gMnSc7	žák
byl	být	k5eAaImAgMnS	být
botanik	botanik	k1gMnSc1	botanik
Martin	Martin	k1gMnSc1	Martin
Vahl	Vahl	k1gMnSc1	Vahl
<g/>
.	.	kIx.	.
</s>
<s>
Mineralog	mineralog	k1gMnSc1	mineralog
Victor	Victor	k1gMnSc1	Victor
Goldschmidt	Goldschmidt	k1gMnSc1	Goldschmidt
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
zakladatelům	zakladatel	k1gMnPc3	zakladatel
geochemie	geochemie	k1gFnSc2	geochemie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ragnar	Ragnar	k1gMnSc1	Ragnar
Frisch	Frisch	k1gMnSc1	Frisch
<g/>
,	,	kIx,	,
Finn	Finn	k1gMnSc1	Finn
E.	E.	kA	E.
Kydland	Kydland	k1gInSc1	Kydland
a	a	k8xC	a
Trygve	Trygev	k1gFnPc1	Trygev
Haavelmo	Haavelma	k1gFnSc5	Haavelma
získali	získat	k5eAaPmAgMnP	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
humanitních	humanitní	k2eAgFnPc6d1	humanitní
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc6d1	sociální
vědách	věda	k1gFnPc6	věda
získali	získat	k5eAaPmAgMnP	získat
věhlas	věhlas	k1gInSc4	věhlas
"	"	kIx"	"
<g/>
zelený	zelený	k2eAgMnSc1d1	zelený
filozof	filozof	k1gMnSc1	filozof
<g/>
"	"	kIx"	"
Arne	Arne	k1gMnSc1	Arne
Næ	Næ	k1gMnSc1	Næ
či	či	k8xC	či
sociolog	sociolog	k1gMnSc1	sociolog
Stein	Stein	k1gMnSc1	Stein
Rokkan	Rokkan	k1gMnSc1	Rokkan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
politologii	politologie	k1gFnSc6	politologie
je	být	k5eAaImIp3nS	být
průkopníkem	průkopník	k1gMnSc7	průkopník
výzkumu	výzkum	k1gInSc2	výzkum
míru	mír	k1gInSc2	mír
a	a	k8xC	a
konfliktů	konflikt	k1gInPc2	konflikt
Johan	Johan	k1gMnSc1	Johan
Galtung	Galtung	k1gMnSc1	Galtung
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
známých	známý	k2eAgInPc2d1	známý
pojmů	pojem	k1gInPc2	pojem
"	"	kIx"	"
<g/>
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
mír	mír	k1gInSc1	mír
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
negativní	negativní	k2eAgInSc1d1	negativní
mír	mír	k1gInSc1	mír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kodifikátorem	kodifikátor	k1gMnSc7	kodifikátor
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
verzí	verze	k1gFnPc2	verze
spisovné	spisovný	k2eAgFnSc2d1	spisovná
norštiny	norština	k1gFnSc2	norština
<g/>
,	,	kIx,	,
nynorsku	nynorsek	k1gInSc2	nynorsek
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
lingvista	lingvista	k1gMnSc1	lingvista
Ivar	Ivar	k1gMnSc1	Ivar
Aasen	Aasno	k1gNnPc2	Aasno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
vede	vést	k5eAaImIp3nS	vést
historickou	historický	k2eAgFnSc4d1	historická
medailovou	medailový	k2eAgFnSc4d1	medailová
tabulku	tabulka	k1gFnSc4	tabulka
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
má	mít	k5eAaImIp3nS	mít
biatlonista	biatlonista	k1gMnSc1	biatlonista
Ole	Ola	k1gFnSc6	Ola
Einar	Einar	k1gMnSc1	Einar
Bjø	Bjø	k1gMnSc1	Bjø
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
běžec	běžec	k1gMnSc1	běžec
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Bjø	Bjø	k1gFnSc2	Bjø
Dæ	Dæ	k1gFnSc2	Dæ
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
lyžařka	lyžařka	k1gFnSc1	lyžařka
Marit	Marit	k1gMnSc1	Marit
Bjø	Bjø	k1gMnSc1	Bjø
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
biatlonista	biatlonista	k1gMnSc1	biatlonista
Emil	Emil	k1gMnSc1	Emil
Hegle	Hegle	k1gFnSc2	Hegle
Svendsen	Svendsen	k1gInSc1	Svendsen
<g/>
,	,	kIx,	,
sjezdař	sjezdař	k1gMnSc1	sjezdař
Kjetil	Kjetil	k1gFnSc2	Kjetil
André	André	k1gMnSc1	André
Aamodt	Aamodt	k1gMnSc1	Aamodt
a	a	k8xC	a
rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
Johann	Johann	k1gMnSc1	Johann
Olav	Olav	k1gMnSc1	Olav
Koss	Koss	k1gInSc4	Koss
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
legendárním	legendární	k2eAgMnPc3d1	legendární
sportovcům	sportovec	k1gMnPc3	sportovec
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
krasobruslaři	krasobruslař	k1gMnPc1	krasobruslař
Axel	Axel	k1gMnSc1	Axel
Paulsen	Paulsen	k2eAgMnSc1d1	Paulsen
a	a	k8xC	a
Sonja	Sonj	k2eAgFnSc1d1	Sonja
Henie	Henie	k1gFnSc1	Henie
<g/>
,	,	kIx,	,
rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
Hjalmar	Hjalmar	k1gMnSc1	Hjalmar
Andersen	Andersen	k1gMnSc1	Andersen
<g/>
,	,	kIx,	,
sjezdař	sjezdař	k1gMnSc1	sjezdař
Aksel	Aksel	k1gMnSc1	Aksel
Lund	Lund	k1gMnSc1	Lund
Svindal	Svindal	k1gMnSc1	Svindal
<g/>
,	,	kIx,	,
maratonská	maratonský	k2eAgFnSc1d1	maratonská
běžkyně	běžkyně	k1gFnSc1	běžkyně
Grete	Gret	k1gInSc5	Gret
Waitzová	Waitzová	k1gFnSc1	Waitzová
nebo	nebo	k8xC	nebo
oštěpař	oštěpař	k1gMnSc1	oštěpař
Andreas	Andreas	k1gMnSc1	Andreas
Thorkildsen	Thorkildsno	k1gNnPc2	Thorkildsno
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
šachistům	šachista	k1gMnPc3	šachista
současnosti	současnost	k1gFnSc2	současnost
patří	patřit	k5eAaImIp3nS	patřit
velmistr	velmistr	k1gMnSc1	velmistr
Magnus	Magnus	k1gMnSc1	Magnus
Carlsen	Carlsno	k1gNnPc2	Carlsno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
jsou	být	k5eAaImIp3nP	být
Norové	Nor	k1gMnPc1	Nor
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
<g/>
,	,	kIx,	,
norská	norský	k2eAgFnSc1d1	norská
házenkářská	házenkářský	k2eAgFnSc1d1	házenkářská
reprezentace	reprezentace	k1gFnSc1	reprezentace
žen	žena	k1gFnPc2	žena
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
dvakrát	dvakrát	k6eAd1	dvakrát
zlato	zlato	k1gNnSc4	zlato
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
tituly	titul	k1gInPc1	titul
mistryň	mistryně	k1gFnPc2	mistryně
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
a	a	k8xC	a
sedmkrát	sedmkrát	k6eAd1	sedmkrát
triumfovala	triumfovat	k5eAaBmAgFnS	triumfovat
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Trine	Trinout	k5eAaImIp3nS	Trinout
Haltviková	Haltvikový	k2eAgFnSc1d1	Haltvikový
<g/>
,	,	kIx,	,
Cecilie	Cecilie	k1gFnSc1	Cecilie
Legangerová	Legangerová	k1gFnSc1	Legangerová
<g/>
,	,	kIx,	,
Linn	Linn	k1gNnSc1	Linn
Kristin	Kristina	k1gFnPc2	Kristina
Riegelhuthová	Riegelhuthová	k1gFnSc1	Riegelhuthová
<g/>
,	,	kIx,	,
Gro	Gro	k1gFnSc1	Gro
Hammersengová	Hammersengový	k2eAgFnSc1d1	Hammersengová
a	a	k8xC	a
Heidi	Heid	k1gMnPc1	Heid
Lø	Lø	k1gFnSc2	Lø
byly	být	k5eAaImAgInP	být
vyhlášeny	vyhlásit	k5eAaPmNgInP	vyhlásit
nejlepšími	dobrý	k2eAgFnPc7d3	nejlepší
házenkářkami	házenkářka	k1gFnPc7	házenkářka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Norské	norský	k2eAgFnPc1d1	norská
ženy	žena	k1gFnPc1	žena
jsou	být	k5eAaImIp3nP	být
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
i	i	k9	i
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
<g/>
,	,	kIx,	,
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgInSc1d1	olympijský
turnaj	turnaj	k1gInSc1	turnaj
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
i	i	k8xC	i
evropský	evropský	k2eAgInSc1d1	evropský
šampionát	šampionát	k1gInSc1	šampionát
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejúspěšnějším	úspěšný	k2eAgMnPc3d3	nejúspěšnější
norským	norský	k2eAgMnPc3d1	norský
mužským	mužský	k2eAgMnPc3d1	mužský
fotbalistům	fotbalista	k1gMnPc3	fotbalista
patří	patřit	k5eAaImIp3nP	patřit
John	John	k1gMnSc1	John
Arne	Arne	k1gMnSc1	Arne
Riise	Riise	k1gFnSc1	Riise
<g/>
,	,	kIx,	,
Tore	Tor	k1gMnSc5	Tor
André	André	k1gMnSc5	André
Flo	Flo	k1gMnSc5	Flo
<g/>
,	,	kIx,	,
Ronny	Ronno	k1gNnPc7	Ronno
Johnsen	Johnsen	k1gInSc1	Johnsen
či	či	k8xC	či
Ole	Ola	k1gFnSc3	Ola
Gunnar	Gunnara	k1gFnPc2	Gunnara
Solskjæ	Solskjæ	k1gFnSc2	Solskjæ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
byly	být	k5eAaImAgFnP	být
dvakrát	dvakrát	k6eAd1	dvakrát
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
zimní	zimní	k2eAgFnPc1d1	zimní
olympijské	olympijský	k2eAgFnPc1d1	olympijská
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
v	v	k7c6	v
Oslu	osel	k1gMnSc6	osel
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1994	[number]	k4	1994
v	v	k7c6	v
Lillehammeru	Lillehammer	k1gInSc6	Lillehammer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BAKKE	BAKKE	kA	BAKKE
<g/>
,	,	kIx,	,
Elisabeth	Elisabeth	k1gMnSc1	Elisabeth
<g/>
;	;	kIx,	;
KADEČKOVÁ	KADEČKOVÁ	kA	KADEČKOVÁ
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
;	;	kIx,	;
HROCH	Hroch	k1gMnSc1	Hroch
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7106-407-6	[number]	k4	80-7106-407-6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
norských	norský	k2eAgMnPc2d1	norský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
pohřebišť	pohřebiště	k1gNnPc2	pohřebiště
norských	norský	k2eAgMnPc2d1	norský
panovníků	panovník	k1gMnPc2	panovník
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
norských	norský	k2eAgFnPc2d1	norská
obcí	obec	k1gFnPc2	obec
</s>
</p>
<p>
<s>
Horská	horský	k2eAgFnSc1d1	horská
turistika	turistika	k1gFnSc1	turistika
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Norsko	Norsko	k1gNnSc4	Norsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Norsko	Norsko	k1gNnSc1	Norsko
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Norsko	Norsko	k1gNnSc4	Norsko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Průvodce	průvodka	k1gFnSc3	průvodka
Norsko	Norsko	k1gNnSc1	Norsko
ve	v	k7c6	v
Wikicestách	Wikicesta	k1gFnPc6	Wikicesta
</s>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Norsko	Norsko	k1gNnSc1	Norsko
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Norway	Norwaa	k1gFnPc1	Norwaa
<g/>
.	.	kIx.	.
<g/>
no	no	k9	no
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
<p>
<s>
Norge.cz	Norge.cz	k1gInSc4	Norge.cz
–	–	k?	–
české	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
Norsku	Norsko	k1gNnSc6	Norsko
a	a	k8xC	a
norštině	norština	k1gFnSc6	norština
</s>
</p>
<p>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
–	–	k?	–
další	další	k2eAgFnPc4d1	další
zajímavé	zajímavý	k2eAgFnPc4d1	zajímavá
české	český	k2eAgFnPc4d1	Česká
stránky	stránka	k1gFnPc4	stránka
o	o	k7c6	o
Norsku	Norsko	k1gNnSc6	Norsko
</s>
</p>
<p>
<s>
Norway	Norway	k1gInPc1	Norway
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-09	[number]	k4	2011-08-09
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gFnSc2	Not
<g/>
:	:	kIx,	:
Norway	Norwaa	k1gFnSc2	Norwaa
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.S.	U.S.	k?	U.S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-07-18	[number]	k4	2011-07-18
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-09	[number]	k4	2011-08-09
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Norway	Norwaa	k1gFnPc1	Norwaa
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-12	[number]	k4	2011-07-12
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-09	[number]	k4	2011-08-09
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Oslo	Oslo	k1gNnSc6	Oslo
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Norsko	Norsko	k1gNnSc1	Norsko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo.cz	Businessinfo.cz	k1gMnSc1	Businessinfo.cz
<g/>
,	,	kIx,	,
2010-10-01	[number]	k4	2010-10-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-09	[number]	k4	2011-08-09
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012-01-11	[number]	k4	2012-01-11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CHRISTENSEN	CHRISTENSEN	kA	CHRISTENSEN
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Norway	Norwaa	k1gFnPc1	Norwaa
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-09	[number]	k4	2011-08-09
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
