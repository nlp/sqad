<s>
Elamština	Elamština	k1gFnSc1	Elamština
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
mrtvých	mrtvý	k2eAgInPc2d1	mrtvý
jazyků	jazyk	k1gInPc2	jazyk
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
Elamské	elamský	k2eAgFnSc2d1	elamský
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
i	i	k8xC	i
Achaimenovské	Achaimenovský	k2eAgFnSc2d1	Achaimenovský
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
starou	starý	k2eAgFnSc7d1	stará
perštinou	perština	k1gFnSc7	perština
a	a	k8xC	a
babylonštinou	babylonština	k1gFnSc7	babylonština
ji	on	k3xPp3gFnSc4	on
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
na	na	k7c6	na
achaimenovských	achaimenovský	k2eAgInPc6d1	achaimenovský
kamenných	kamenný	k2eAgInPc6d1	kamenný
reliéfech	reliéf	k1gInPc6	reliéf
v	v	k7c6	v
Bísotúnu	Bísotúno	k1gNnSc6	Bísotúno
<g/>
,	,	kIx,	,
Gandž	Gandž	k1gFnSc1	Gandž
Náme	Náme	k1gFnSc1	Náme
a	a	k8xC	a
Persepoli	Persepolis	k1gFnSc3	Persepolis
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
památky	památka	k1gFnPc1	památka
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
elamštiny	elamština	k1gFnSc2	elamština
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
perské	perský	k2eAgFnSc2d1	perská
říše	říš	k1gFnSc2	říš
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Velikým	veliký	k2eAgMnSc7d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
elamské	elamský	k2eAgFnPc1d1	elamský
epigrafické	epigrafický	k2eAgFnPc1d1	epigrafická
památky	památka	k1gFnPc1	památka
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
Súsách	Súsy	k1gFnPc6	Súsy
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
jazyk	jazyk	k1gInSc4	jazyk
nazýváme	nazývat	k5eAaImIp1nP	nazývat
proto-elamštinou	protolamština	k1gFnSc7	proto-elamština
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
znaky	znak	k1gInPc7	znak
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
jsou	být	k5eAaImIp3nP	být
ideogramy	ideogram	k1gInPc4	ideogram
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
Akkadských	Akkadský	k2eAgMnPc2d1	Akkadský
vládců	vládce	k1gMnPc2	vládce
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
písaři	písař	k1gMnPc1	písař
používat	používat	k5eAaImF	používat
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
písma	písmo	k1gNnSc2	písmo
odvozený	odvozený	k2eAgInSc1d1	odvozený
od	od	k7c2	od
sumerského	sumerský	k2eAgNnSc2d1	sumerské
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
písma	písmo	k1gNnSc2	písmo
se	se	k3xPyFc4	se
však	však	k9	však
používal	používat	k5eAaImAgInS	používat
pouze	pouze	k6eAd1	pouze
250	[number]	k4	250
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
2500	[number]	k4	2500
<g/>
–	–	k?	–
<g/>
331	[number]	k4	331
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
elamštiny	elamština	k1gFnSc2	elamština
akkadského	akkadský	k2eAgNnSc2d1	akkadské
klínového	klínový	k2eAgNnSc2d1	klínové
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgMnS	skládat
ze	z	k7c2	z
130	[number]	k4	130
znaků	znak	k1gInPc2	znak
<g/>
.	.	kIx.	.
</s>
