<s>
Advanced	Advanced	k1gInSc1	Advanced
Encryption	Encryption	k1gInSc1	Encryption
Standard	standard	k1gInSc1	standard
(	(	kIx(	(
<g/>
AES	AES	kA	AES
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
standard	standard	k1gInSc1	standard
pokročilého	pokročilý	k2eAgNnSc2d1	pokročilé
šifrování	šifrování	k1gNnSc2	šifrování
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
standardizovaný	standardizovaný	k2eAgInSc1d1	standardizovaný
algoritmus	algoritmus	k1gInSc1	algoritmus
používaný	používaný	k2eAgInSc1d1	používaný
k	k	k7c3	k
šifrování	šifrování	k1gNnSc3	šifrování
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
symetrickou	symetrický	k2eAgFnSc4d1	symetrická
blokovou	blokový	k2eAgFnSc4d1	bloková
šifru	šifra	k1gFnSc4	šifra
šifrující	šifrující	k2eAgFnSc4d1	šifrující
i	i	k8xC	i
dešifrující	dešifrující	k2eAgFnSc4d1	dešifrující
stejným	stejný	k2eAgInSc7d1	stejný
klíčem	klíč	k1gInSc7	klíč
data	datum	k1gNnSc2	datum
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
do	do	k7c2	do
bloků	blok	k1gInPc2	blok
pevně	pevně	k6eAd1	pevně
dané	daný	k2eAgFnSc2d1	daná
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Norma	Norma	k1gFnSc1	Norma
nahradila	nahradit	k5eAaPmAgFnS	nahradit
dříve	dříve	k6eAd2	dříve
užívanou	užívaný	k2eAgFnSc4d1	užívaná
šifru	šifra	k1gFnSc4	šifra
DES	DES	k1gFnPc2	DES
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
například	například	k6eAd1	například
pro	pro	k7c4	pro
bezdrátové	bezdrátový	k2eAgNnSc4d1	bezdrátové
Wi-Fi	Wi-Fi	k1gNnSc4	Wi-Fi
sítě	síť	k1gFnSc2	síť
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
WPA2	WPA2	k1gFnSc2	WPA2
dle	dle	k7c2	dle
standardu	standard	k1gInSc2	standard
IEEE	IEEE	kA	IEEE
802.11	[number]	k4	802.11
<g/>
i.	i.	k?	i.
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
šifry	šifra	k1gFnSc2	šifra
AES	AES	kA	AES
je	být	k5eAaImIp3nS	být
Rijndael	Rijndael	k1gInSc4	Rijndael
(	(	kIx(	(
<g/>
vyslov	vyslovit	k5eAaPmRp2nS	vyslovit
[	[	kIx(	[
<g/>
rejndál	rejndál	k1gMnSc1	rejndál
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
název	název	k1gInSc1	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
přesmyčkou	přesmyčka	k1gFnSc7	přesmyčka
jmen	jméno	k1gNnPc2	jméno
jejích	její	k3xOp3gMnPc2	její
dvou	dva	k4xCgMnPc2	dva
autorů	autor	k1gMnPc2	autor
Joana	Joana	k1gFnSc1	Joana
Daemena	Daemen	k2eAgFnSc1d1	Daemen
a	a	k8xC	a
Vincenta	Vincent	k1gMnSc2	Vincent
Rijmena	Rijmen	k2eAgFnSc1d1	Rijmen
z	z	k7c2	z
belgické	belgický	k2eAgFnSc2d1	belgická
Lovaně	Lovaň	k1gFnSc2	Lovaň
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tuto	tento	k3xDgFnSc4	tento
šifru	šifra	k1gFnSc4	šifra
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
do	do	k7c2	do
veřejné	veřejný	k2eAgFnSc2d1	veřejná
soutěže	soutěž	k1gFnSc2	soutěž
NIST	NIST	kA	NIST
o	o	k7c4	o
federální	federální	k2eAgInSc4d1	federální
šifrovací	šifrovací	k2eAgInSc4d1	šifrovací
algoritmus	algoritmus	k1gInSc4	algoritmus
AES	AES	kA	AES
vyhlášené	vyhlášený	k2eAgFnSc2d1	vyhlášená
2	[number]	k4	2
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
standardizaci	standardizace	k1gFnSc4	standardizace
(	(	kIx(	(
<g/>
NIST	NIST	kA	NIST
<g/>
)	)	kIx)	)
schválil	schválit	k5eAaPmAgInS	schválit
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
2001	[number]	k4	2001
šifru	šifra	k1gFnSc4	šifra
AES	AES	kA	AES
jako	jako	k8xS	jako
nejvhodnější	vhodný	k2eAgInSc4d3	nejvhodnější
návrh	návrh	k1gInSc4	návrh
z	z	k7c2	z
patnácti	patnáct	k4xCc2	patnáct
předložených	předložený	k2eAgInPc2d1	předložený
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
schvalovací	schvalovací	k2eAgFnSc2d1	schvalovací
procedury	procedura	k1gFnSc2	procedura
<g/>
.	.	kIx.	.
</s>
<s>
AES	AES	kA	AES
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
šifra	šifra	k1gFnSc1	šifra
dostupná	dostupný	k2eAgFnSc1d1	dostupná
široké	široký	k2eAgFnSc3d1	široká
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
uznaná	uznaný	k2eAgFnSc1d1	uznaná
Národní	národní	k2eAgFnSc7d1	národní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
agenturou	agentura	k1gFnSc7	agentura
NSA	NSA	kA	NSA
ke	k	k7c3	k
šifrovaní	šifrovaný	k2eAgMnPc1d1	šifrovaný
nejtajnějších	tajný	k2eAgMnPc2d3	nejtajnější
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2002	[number]	k4	2002
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
používána	používán	k2eAgFnSc1d1	používána
jako	jako	k9	jako
federální	federální	k2eAgInSc4d1	federální
standard	standard	k1gInSc4	standard
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
AES	AES	kA	AES
šifra	šifra	k1gFnSc1	šifra
je	být	k5eAaImIp3nS	být
rychlá	rychlý	k2eAgFnSc1d1	rychlá
v	v	k7c6	v
softwaru	software	k1gInSc6	software
i	i	k8xC	i
hardwaru	hardware	k1gInSc6	hardware
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
DES	des	k1gNnSc2	des
nepoužívá	používat	k5eNaImIp3nS	používat
Feistelovu	Feistelův	k2eAgFnSc4d1	Feistelův
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
AES	AES	kA	AES
má	mít	k5eAaImIp3nS	mít
pevně	pevně	k6eAd1	pevně
danou	daný	k2eAgFnSc4d1	daná
velikost	velikost	k1gFnSc4	velikost
bloku	blok	k1gInSc2	blok
na	na	k7c4	na
128	[number]	k4	128
bitů	bit	k1gInPc2	bit
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
klíče	klíč	k1gInSc2	klíč
na	na	k7c4	na
128	[number]	k4	128
<g/>
,	,	kIx,	,
192	[number]	k4	192
nebo	nebo	k8xC	nebo
256	[number]	k4	256
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
u	u	k7c2	u
původní	původní	k2eAgFnSc2d1	původní
Rijndael	Rijndael	k1gInSc1	Rijndael
šifry	šifra	k1gFnPc4	šifra
je	být	k5eAaImIp3nS	být
velikost	velikost	k1gFnSc1	velikost
bloku	blok	k1gInSc2	blok
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
klíče	klíč	k1gInSc2	klíč
určena	určit	k5eAaPmNgFnS	určit
jakýmkoliv	jakýkoliv	k3yIgInSc7	jakýkoliv
násobkem	násobek	k1gInSc7	násobek
32	[number]	k4	32
bitů	bit	k1gInPc2	bit
s	s	k7c7	s
minimální	minimální	k2eAgFnSc7d1	minimální
velikostí	velikost	k1gFnSc7	velikost
128	[number]	k4	128
bitů	bit	k1gInPc2	bit
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
bloku	blok	k1gInSc2	blok
je	být	k5eAaImIp3nS	být
maximálně	maximálně	k6eAd1	maximálně
256	[number]	k4	256
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
velikost	velikost	k1gFnSc1	velikost
klíče	klíč	k1gInSc2	klíč
žádné	žádný	k3yNgNnSc4	žádný
teoretické	teoretický	k2eAgNnSc4d1	teoretické
maximum	maximum	k1gNnSc4	maximum
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
AES	AES	kA	AES
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
maticí	matice	k1gFnSc7	matice
bytů	byt	k1gInPc2	byt
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xS	jako
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Expanze	expanze	k1gFnSc1	expanze
klíče	klíč	k1gInPc1	klíč
−	−	k?	−
podklíče	podklíč	k1gInSc2	podklíč
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
z	z	k7c2	z
klíče	klíč	k1gInSc2	klíč
šifry	šifra	k1gFnSc2	šifra
užitím	užití	k1gNnSc7	užití
Rijndael	Rijndaela	k1gFnPc2	Rijndaela
programu	program	k1gInSc2	program
Inicializační	inicializační	k2eAgFnSc1d1	inicializační
část	část	k1gFnSc1	část
Přidání	přidání	k1gNnSc2	přidání
podklíče	podklíč	k1gInSc2	podklíč
−	−	k?	−
každý	každý	k3xTgInSc4	každý
byte	byte	k1gInSc4	byte
stavu	stav	k1gInSc2	stav
je	být	k5eAaImIp3nS	být
zkombinován	zkombinovat	k5eAaPmNgInS	zkombinovat
s	s	k7c7	s
podklíčem	podklíč	k1gInSc7	podklíč
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
operace	operace	k1gFnSc2	operace
xor	xor	k?	xor
nad	nad	k7c7	nad
všemi	všecek	k3xTgInPc7	všecek
bity	bit	k1gInPc4	bit
Iterace	iterace	k1gFnSc1	iterace
Záměna	záměna	k1gFnSc1	záměna
bytů	byt	k1gInPc2	byt
−	−	k?	−
nelineární	lineární	k2eNgInSc1d1	nelineární
nahrazovací	nahrazovací	k2eAgInSc1d1	nahrazovací
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
byte	byte	k1gInSc1	byte
nahrazen	nahradit	k5eAaPmNgInS	nahradit
jiným	jiný	k2eAgInSc7d1	jiný
podle	podle	k7c2	podle
vyhledávací	vyhledávací	k2eAgFnSc2d1	vyhledávací
tabulky	tabulka	k1gFnSc2	tabulka
Prohození	prohození	k1gNnSc2	prohození
<g />
.	.	kIx.	.
</s>
<s>
řádků	řádek	k1gInPc2	řádek
−	−	k?	−
provedení	provedení	k1gNnSc4	provedení
kroku	krok	k1gInSc2	krok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
řádek	řádek	k1gInSc1	řádek
stavu	stav	k1gInSc2	stav
postupně	postupně	k6eAd1	postupně
posunut	posunout	k5eAaPmNgInS	posunout
o	o	k7c4	o
určitý	určitý	k2eAgInSc4d1	určitý
počet	počet	k1gInSc4	počet
kroků	krok	k1gInPc2	krok
Kombinování	kombinování	k1gNnSc2	kombinování
sloupců	sloupec	k1gInPc2	sloupec
−	−	k?	−
zkombinuje	zkombinovat	k5eAaPmIp3nS	zkombinovat
čtyři	čtyři	k4xCgInPc4	čtyři
byty	byt	k1gInPc4	byt
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
sloupci	sloupec	k1gInSc6	sloupec
Přidání	přidání	k1gNnSc2	přidání
podklíče	podklíč	k1gFnSc2	podklíč
Závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
nekombinují	kombinovat	k5eNaImIp3nP	kombinovat
se	se	k3xPyFc4	se
sloupce	sloupec	k1gInPc1	sloupec
<g/>
)	)	kIx)	)
Záměna	záměna	k1gFnSc1	záměna
bytů	byt	k1gInPc2	byt
Prohození	prohození	k1gNnSc2	prohození
řádků	řádek	k1gInPc2	řádek
Přidání	přidání	k1gNnSc4	přidání
podklíče	podklíč	k1gFnSc2	podklíč
Při	při	k7c6	při
záměně	záměna	k1gFnSc6	záměna
bytů	byt	k1gInPc2	byt
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
byte	byte	k1gInSc1	byte
v	v	k7c4	v
matici	matice	k1gFnSc4	matice
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
pomocí	pomocí	k7c2	pomocí
8	[number]	k4	8
<g/>
bitového	bitový	k2eAgInSc2d1	bitový
zaměňovacího	zaměňovací	k2eAgInSc2d1	zaměňovací
boxu	box	k1gInSc2	box
<g/>
,	,	kIx,	,
Rijndeal	Rijndeal	k1gInSc1	Rijndeal
S-boxu	Sox	k1gInSc2	S-box
(	(	kIx(	(
<g/>
nelineární	lineární	k2eNgFnPc1d1	nelineární
substituční	substituční	k2eAgFnPc1d1	substituční
funkce	funkce	k1gFnPc1	funkce
měnící	měnící	k2eAgInPc4d1	měnící
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
bity	bit	k1gInPc4	bit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
operace	operace	k1gFnSc1	operace
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
nelinearitu	nelinearita	k1gFnSc4	nelinearita
v	v	k7c6	v
šifře	šifra	k1gFnSc6	šifra
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
S-box	Sox	k1gInSc1	S-box
minimalizoval	minimalizovat	k5eAaBmAgInS	minimalizovat
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
možných	možný	k2eAgInPc2d1	možný
útoků	útok	k1gInPc2	útok
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
jednoduchých	jednoduchý	k2eAgFnPc6d1	jednoduchá
algebraických	algebraický	k2eAgFnPc6d1	algebraická
vlastnostech	vlastnost	k1gFnPc6	vlastnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
konstruován	konstruován	k2eAgInSc1d1	konstruován
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
nevznikaly	vznikat	k5eNaImAgFnP	vznikat
pevné	pevný	k2eAgInPc4d1	pevný
body	bod	k1gInPc4	bod
a	a	k8xC	a
žádné	žádný	k3yNgInPc4	žádný
jejich	jejich	k3xOp3gInPc4	jejich
protějšky	protějšek	k1gInPc4	protějšek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
operace	operace	k1gFnSc1	operace
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
úrovních	úroveň	k1gFnPc6	úroveň
řádků	řádek	k1gInPc2	řádek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jednotlivě	jednotlivě	k6eAd1	jednotlivě
prohazuje	prohazovat	k5eAaImIp3nS	prohazovat
byty	byt	k1gInPc4	byt
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
řádku	řádek	k1gInSc6	řádek
o	o	k7c4	o
určitý	určitý	k2eAgInSc4d1	určitý
přesah	přesah	k1gInSc4	přesah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
AES	AES	kA	AES
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
řádek	řádek	k1gInSc1	řádek
ponechán	ponechat	k5eAaPmNgInS	ponechat
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
byte	byte	k1gInSc1	byte
druhého	druhý	k4xOgInSc2	druhý
řádku	řádek	k1gInSc2	řádek
je	být	k5eAaImIp3nS	být
posunut	posunout	k5eAaPmNgInS	posunout
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgNnSc1	třetí
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
řádek	řádek	k1gInSc1	řádek
posunut	posunout	k5eAaPmNgInS	posunout
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
tři	tři	k4xCgFnPc4	tři
pozice	pozice	k1gFnPc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bloky	blok	k1gInPc4	blok
o	o	k7c6	o
velikostech	velikost	k1gFnPc6	velikost
128	[number]	k4	128
bitů	bit	k1gInPc2	bit
a	a	k8xC	a
192	[number]	k4	192
bitů	bit	k1gInPc2	bit
je	být	k5eAaImIp3nS	být
postup	postup	k1gInSc1	postup
prohození	prohození	k1gNnSc2	prohození
řádků	řádek	k1gInPc2	řádek
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
sloupec	sloupec	k1gInSc1	sloupec
výstupu	výstup	k1gInSc2	výstup
složen	složit	k5eAaPmNgInS	složit
z	z	k7c2	z
bytů	byt	k1gInPc2	byt
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
sloupce	sloupec	k1gInSc2	sloupec
na	na	k7c6	na
vstupu	vstup	k1gInSc6	vstup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
šifry	šifra	k1gFnPc4	šifra
Rijndael	Rijndaela	k1gFnPc2	Rijndaela
mají	mít	k5eAaImIp3nP	mít
varianty	varianta	k1gFnPc4	varianta
s	s	k7c7	s
většími	veliký	k2eAgFnPc7d2	veliký
velikostmi	velikost	k1gFnPc7	velikost
bloků	blok	k1gInPc2	blok
lehce	lehko	k6eAd1	lehko
odlišné	odlišný	k2eAgInPc1d1	odlišný
přesahy	přesah	k1gInPc1	přesah
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
256	[number]	k4	256
<g/>
bitového	bitový	k2eAgInSc2d1	bitový
bloku	blok	k1gInSc2	blok
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
první	první	k4xOgInSc4	první
řádek	řádek	k1gInSc4	řádek
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
řádek	řádek	k1gInSc4	řádek
je	být	k5eAaImIp3nS	být
posunut	posunout	k5eAaPmNgInS	posunout
o	o	k7c4	o
1	[number]	k4	1
<g/>
,	,	kIx,	,
3	[number]	k4	3
a	a	k8xC	a
4	[number]	k4	4
byty	byt	k1gInPc7	byt
−	−	k?	−
tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
platí	platit	k5eAaImIp3nS	platit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
šifru	šifra	k1gFnSc4	šifra
Rijndael	Rijndaela	k1gFnPc2	Rijndaela
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
AES	AES	kA	AES
nepoužívá	používat	k5eNaImIp3nS	používat
256	[number]	k4	256
<g/>
bitové	bitový	k2eAgInPc4d1	bitový
bloky	blok	k1gInPc4	blok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kroku	krok	k1gInSc6	krok
kombinujeme	kombinovat	k5eAaImIp1nP	kombinovat
4	[number]	k4	4
byty	byt	k1gInPc4	byt
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
sloupci	sloupec	k1gInSc6	sloupec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
vezme	vzít	k5eAaPmIp3nS	vzít
čtyři	čtyři	k4xCgInPc4	čtyři
byty	byt	k1gInPc4	byt
jako	jako	k8xC	jako
vstup	vstup	k1gInSc4	vstup
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nP	vrátit
čtyřbytový	čtyřbytový	k2eAgInSc4d1	čtyřbytový
výstup	výstup	k1gInSc4	výstup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každý	každý	k3xTgInSc1	každý
vstupní	vstupní	k2eAgInSc1d1	vstupní
byte	byte	k1gInSc1	byte
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
všechny	všechen	k3xTgInPc4	všechen
výstupní	výstupní	k2eAgInPc4d1	výstupní
byty	byt	k1gInPc4	byt
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
předchozími	předchozí	k2eAgInPc7d1	předchozí
kroky	krok	k1gInPc7	krok
zajistí	zajistit	k5eAaPmIp3nP	zajistit
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
náhodnost	náhodnost	k1gFnSc4	náhodnost
v	v	k7c6	v
šifře	šifra	k1gFnSc6	šifra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přidání	přidání	k1gNnSc6	přidání
podklíče	podklíč	k1gFnSc2	podklíč
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
byte	byte	k1gInSc1	byte
zkombinován	zkombinován	k2eAgInSc1d1	zkombinován
s	s	k7c7	s
podklíčem	podklíč	k1gInSc7	podklíč
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
iteraci	iterace	k1gFnSc4	iterace
je	být	k5eAaImIp3nS	být
podklíč	podklíč	k6eAd1	podklíč
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
hlavního	hlavní	k2eAgInSc2d1	hlavní
klíče	klíč	k1gInSc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
podklíč	podklíč	k1gInSc1	podklíč
má	mít	k5eAaImIp3nS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přidán	přidat	k5eAaPmNgMnS	přidat
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
kombinace	kombinace	k1gFnSc2	kombinace
každého	každý	k3xTgInSc2	každý
bytu	byt	k1gInSc2	byt
stavu	stav	k1gInSc2	stav
s	s	k7c7	s
odpovídajícím	odpovídající	k2eAgInSc7d1	odpovídající
bytem	byt	k1gInSc7	byt
podklíče	podklíč	k1gInSc2	podklíč
užitím	užití	k1gNnSc7	užití
XORu	XORus	k1gInSc2	XORus
nad	nad	k7c7	nad
všemi	všecek	k3xTgInPc7	všecek
bity	bit	k1gInPc7	bit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
systémů	systém	k1gInPc2	systém
s	s	k7c7	s
32	[number]	k4	32
<g/>
bitovými	bitový	k2eAgFnPc7d1	bitová
a	a	k8xC	a
většími	veliký	k2eAgFnPc7d2	veliký
délkami	délka	k1gFnPc7	délka
slov	slovo	k1gNnPc2	slovo
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
urychlit	urychlit	k5eAaPmF	urychlit
provádění	provádění	k1gNnSc2	provádění
této	tento	k3xDgFnSc2	tento
šifry	šifra	k1gFnSc2	šifra
pomocí	pomocí	k7c2	pomocí
zkombinování	zkombinování	k1gNnPc2	zkombinování
operací	operace	k1gFnPc2	operace
záměna	záměna	k1gFnSc1	záměna
bytů	byt	k1gInPc2	byt
a	a	k8xC	a
prohození	prohození	k1gNnSc4	prohození
řádků	řádek	k1gInPc2	řádek
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
kombinování	kombinování	k1gNnSc2	kombinování
sloupců	sloupec	k1gInPc2	sloupec
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
transformují	transformovat	k5eAaBmIp3nP	transformovat
do	do	k7c2	do
sekvencí	sekvence	k1gFnPc2	sekvence
vyhledávacích	vyhledávací	k2eAgFnPc2d1	vyhledávací
tabulek	tabulka	k1gFnPc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
provedení	provedení	k1gNnSc4	provedení
této	tento	k3xDgFnSc2	tento
operace	operace	k1gFnSc2	operace
jsou	být	k5eAaImIp3nP	být
nezbytné	zbytný	k2eNgInPc1d1	zbytný
čtyři	čtyři	k4xCgInPc1	čtyři
32	[number]	k4	32
<g/>
bitové	bitový	k2eAgFnSc2d1	bitová
tabulky	tabulka	k1gFnSc2	tabulka
o	o	k7c6	o
256	[number]	k4	256
vstupech	vstup	k1gInPc6	vstup
<g/>
.	.	kIx.	.
</s>
<s>
Iterace	iterace	k1gFnSc1	iterace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nyní	nyní	k6eAd1	nyní
provedena	provést	k5eAaPmNgFnS	provést
s	s	k7c7	s
šestnácti	šestnáct	k4xCc7	šestnáct
vyhledávacími	vyhledávací	k2eAgFnPc7d1	vyhledávací
tabulkami	tabulka	k1gFnPc7	tabulka
a	a	k8xC	a
dvanácti	dvanáct	k4xCc2	dvanáct
32	[number]	k4	32
<g/>
bitovými	bitový	k2eAgInPc7d1	bitový
XOR	XOR	kA	XOR
operacemi	operace	k1gFnPc7	operace
<g/>
,	,	kIx,	,
následovanými	následovaný	k2eAgFnPc7d1	následovaná
čtyřmi	čtyři	k4xCgInPc7	čtyři
32	[number]	k4	32
<g/>
bitovými	bitový	k2eAgInPc7d1	bitový
XOR	XOR	kA	XOR
operacemi	operace	k1gFnPc7	operace
v	v	k7c6	v
kroku	krok	k1gInSc6	krok
přidání	přidání	k1gNnSc2	přidání
podklíče	podklíč	k1gFnSc2	podklíč
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
jediný	jediný	k2eAgInSc1d1	jediný
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
publikovaný	publikovaný	k2eAgInSc1d1	publikovaný
útok	útok	k1gInSc1	útok
na	na	k7c4	na
plnou	plný	k2eAgFnSc4d1	plná
AES	AES	kA	AES
pomocí	pomocí	k7c2	pomocí
postranních	postranní	k2eAgInPc2d1	postranní
kanálů	kanál	k1gInPc2	kanál
dané	daný	k2eAgFnSc2d1	daná
implementace	implementace	k1gFnSc2	implementace
<g/>
.	.	kIx.	.
</s>
<s>
NSA	NSA	kA	NSA
prozkoumala	prozkoumat	k5eAaPmAgFnS	prozkoumat
všechny	všechen	k3xTgInPc4	všechen
AES	AES	kA	AES
finalisty	finalista	k1gMnPc4	finalista
včetně	včetně	k7c2	včetně
Rijndael	Rijndaela	k1gFnPc2	Rijndaela
a	a	k8xC	a
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc4	všechen
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
pro	pro	k7c4	pro
neutajovaná	utajovaný	k2eNgNnPc4d1	utajovaný
data	datum	k1gNnPc4	datum
vlády	vláda	k1gFnSc2	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2003	[number]	k4	2003
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
AES	AES	kA	AES
Rijndael	Rijndael	k1gInSc1	Rijndael
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použita	použít	k5eAaPmNgFnS	použít
i	i	k9	i
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
utajovaných	utajovaný	k2eAgFnPc2d1	utajovaná
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
AES	AES	kA	AES
provádí	provádět	k5eAaImIp3nS	provádět
10	[number]	k4	10
iterací	iterace	k1gFnPc2	iterace
s	s	k7c7	s
128	[number]	k4	128
<g/>
bitovými	bitový	k2eAgInPc7d1	bitový
klíči	klíč	k1gInPc7	klíč
<g/>
,	,	kIx,	,
12	[number]	k4	12
iterací	iterace	k1gFnPc2	iterace
s	s	k7c7	s
192	[number]	k4	192
<g/>
bitovými	bitový	k2eAgInPc7d1	bitový
klíči	klíč	k1gInPc7	klíč
a	a	k8xC	a
14	[number]	k4	14
iterací	iterace	k1gFnPc2	iterace
s	s	k7c7	s
256	[number]	k4	256
<g/>
bitovými	bitový	k2eAgInPc7d1	bitový
klíči	klíč	k1gInPc7	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
tyto	tento	k3xDgInPc1	tento
nejznámější	známý	k2eAgInPc1d3	nejznámější
útoky	útok	k1gInPc1	útok
<g/>
:	:	kIx,	:
na	na	k7c4	na
7	[number]	k4	7
iterací	iterace	k1gFnPc2	iterace
s	s	k7c7	s
128	[number]	k4	128
<g/>
bitovými	bitový	k2eAgInPc7d1	bitový
klíči	klíč	k1gInPc7	klíč
<g/>
,	,	kIx,	,
8	[number]	k4	8
iterací	iterace	k1gFnPc2	iterace
s	s	k7c7	s
192	[number]	k4	192
<g/>
bitovými	bitový	k2eAgInPc7d1	bitový
klíči	klíč	k1gInPc7	klíč
a	a	k8xC	a
9	[number]	k4	9
iterací	iterace	k1gFnPc2	iterace
s	s	k7c7	s
256	[number]	k4	256
<g/>
bitovými	bitový	k2eAgInPc7d1	bitový
klíči	klíč	k1gInPc7	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Kryptologové	Kryptolog	k1gMnPc1	Kryptolog
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
částečné	částečný	k2eAgNnSc4d1	částečné
prolomení	prolomení	k1gNnSc4	prolomení
šifry	šifra	k1gFnSc2	šifra
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
rozluštění	rozluštění	k1gNnSc3	rozluštění
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
vyzkoušení	vyzkoušení	k1gNnSc4	vyzkoušení
všech	všecek	k3xTgInPc2	všecek
možných	možný	k2eAgInPc2d1	možný
klíčů	klíč	k1gInPc2	klíč
(	(	kIx(	(
<g/>
metoda	metoda	k1gFnSc1	metoda
řešení	řešení	k1gNnSc2	řešení
hrubou	hrubý	k2eAgFnSc7d1	hrubá
silou	síla	k1gFnSc7	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
hrubou	hrubý	k2eAgFnSc4d1	hrubá
silou	síla	k1gFnSc7	síla
proti	proti	k7c3	proti
AES	AES	kA	AES
s	s	k7c7	s
256	[number]	k4	256
<g/>
bitovým	bitový	k2eAgInSc7d1	bitový
klíčem	klíč	k1gInSc7	klíč
by	by	kYmCp3nP	by
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
2256	[number]	k4	2256
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
celkový	celkový	k2eAgInSc1d1	celkový
výpočet	výpočet	k1gInSc1	výpočet
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
trval	trvat	k5eAaImAgInS	trvat
mnohem	mnohem	k6eAd1	mnohem
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
celkové	celkový	k2eAgNnSc4d1	celkové
stáří	stáří	k1gNnSc4	stáří
celého	celý	k2eAgInSc2d1	celý
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
AES	AES	kA	AES
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
algebraický	algebraický	k2eAgInSc4d1	algebraický
popis	popis	k1gInSc4	popis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
provedli	provést	k5eAaPmAgMnP	provést
Nicolas	Nicolas	k1gInSc4	Nicolas
Courtois	Courtois	k1gFnSc2	Courtois
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Pieprzyk	Pieprzyk	k1gMnSc1	Pieprzyk
teoretický	teoretický	k2eAgInSc1d1	teoretický
útok	útok	k1gInSc1	útok
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xC	jako
XSL	XSL	kA	XSL
útok	útok	k1gInSc1	útok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
chtěli	chtít	k5eAaImAgMnP	chtít
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
šifra	šifra	k1gFnSc1	šifra
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
jednoduchosti	jednoduchost	k1gFnSc3	jednoduchost
prolomitelná	prolomitelný	k2eAgFnSc1d1	prolomitelná
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
publikace	publikace	k1gFnPc1	publikace
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
útok	útok	k1gInSc1	útok
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
byl	být	k5eAaImAgInS	být
originálně	originálně	k6eAd1	originálně
prezentován	prezentovat	k5eAaBmNgInS	prezentovat
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
nefungoval	fungovat	k5eNaImAgMnS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
schvalovací	schvalovací	k2eAgFnSc2d1	schvalovací
procedury	procedura	k1gFnSc2	procedura
AES	AES	kA	AES
Bruce	Bruce	k1gMnSc1	Bruce
Schneier	Schneier	k1gMnSc1	Schneier
<g/>
,	,	kIx,	,
vývojář	vývojář	k1gMnSc1	vývojář
zúčastněného	zúčastněný	k2eAgInSc2d1	zúčastněný
algoritmu	algoritmus	k1gInSc2	algoritmus
Twofish	Twofisha	k1gFnPc2	Twofisha
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nevěřím	věřit	k5eNaImIp1nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednou	jednou	k6eAd1	jednou
někdo	někdo	k3yInSc1	někdo
objeví	objevit	k5eAaPmIp3nS	objevit
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
dovolí	dovolit	k5eAaPmIp3nS	dovolit
číst	číst	k5eAaImF	číst
komunikaci	komunikace	k1gFnSc4	komunikace
kódovanou	kódovaný	k2eAgFnSc4d1	kódovaná
Rijndael	Rijndaela	k1gFnPc2	Rijndaela
šifrou	šifra	k1gFnSc7	šifra
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Bruce	Bruce	k1gMnSc1	Bruce
Schneier	Schneier	k1gMnSc1	Schneier
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
na	na	k7c6	na
svém	svůj	k3xOyFgMnSc6	svůj
blogu	blog	k1gMnSc6	blog
napsal	napsat	k5eAaBmAgMnS	napsat
o	o	k7c6	o
útoku	útok	k1gInSc6	útok
založeném	založený	k2eAgInSc6d1	založený
na	na	k7c6	na
podobnosti	podobnost	k1gFnSc6	podobnost
klíčů	klíč	k1gInPc2	klíč
provedeném	provedený	k2eAgInSc6d1	provedený
Alexem	Alex	k1gMnSc7	Alex
Biryukovem	Biryukov	k1gInSc7	Biryukov
a	a	k8xC	a
Dmitry	Dmitr	k1gInPc7	Dmitr
Khorvratovichem	Khorvratovich	k1gMnSc7	Khorvratovich
se	s	k7c7	s
složitostí	složitost	k1gFnSc7	složitost
2119	[number]	k4	2119
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
složitost	složitost	k1gFnSc1	složitost
ještě	ještě	k6eAd1	ještě
vylepšena	vylepšit	k5eAaPmNgFnS	vylepšit
na	na	k7c4	na
299.5	[number]	k4	299.5
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
oznámen	oznámit	k5eAaPmNgInS	oznámit
Brucem	Bruce	k1gMnSc7	Bruce
Schneierem	Schneier	k1gMnSc7	Schneier
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
od	od	k7c2	od
autorů	autor	k1gMnPc2	autor
Alexe	Alex	k1gMnSc5	Alex
Biryukova	Biryukův	k2eAgMnSc2d1	Biryukův
<g/>
,	,	kIx,	,
Orr	Orr	k1gMnSc2	Orr
Dunkelmana	Dunkelman	k1gMnSc2	Dunkelman
<g/>
,	,	kIx,	,
Nathana	Nathan	k1gMnSc2	Nathan
Kelleryho	Kellery	k1gMnSc2	Kellery
<g/>
,	,	kIx,	,
Dmitry	Dmitr	k1gMnPc7	Dmitr
Khovratoviche	Khovratoviche	k1gNnSc2	Khovratoviche
a	a	k8xC	a
Adi	Adi	k1gFnSc2	Adi
Shamira	Shamir	k1gInSc2	Shamir
proti	proti	k7c3	proti
AES-	AES-	k1gFnSc3	AES-
<g/>
256	[number]	k4	256
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
užívá	užívat	k5eAaImIp3nS	užívat
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc1	dva
související	související	k2eAgInPc1d1	související
klíče	klíč	k1gInPc1	klíč
a	a	k8xC	a
čas	čas	k1gInSc1	čas
239	[number]	k4	239
ke	k	k7c3	k
kompletní	kompletní	k2eAgFnSc3d1	kompletní
obnově	obnova	k1gFnSc3	obnova
256	[number]	k4	256
<g/>
bitového	bitový	k2eAgInSc2d1	bitový
klíče	klíč	k1gInSc2	klíč
o	o	k7c6	o
9	[number]	k4	9
iteracích	iterace	k1gFnPc6	iterace
nebo	nebo	k8xC	nebo
245	[number]	k4	245
o	o	k7c4	o
10	[number]	k4	10
iteracích	iterace	k1gFnPc6	iterace
se	s	k7c7	s
silnějším	silný	k2eAgInSc7d2	silnější
typem	typ	k1gInSc7	typ
souvisejícího	související	k2eAgInSc2d1	související
podklíčového	podklíčový	k2eAgInSc2d1	podklíčový
útoku	útok	k1gInSc2	útok
a	a	k8xC	a
nebo	nebo	k8xC	nebo
270	[number]	k4	270
o	o	k7c4	o
11	[number]	k4	11
iteracích	iterace	k1gFnPc6	iterace
<g/>
.	.	kIx.	.
256-AES	[number]	k4	256-AES
používá	používat	k5eAaImIp3nS	používat
14	[number]	k4	14
iterací	iterace	k1gFnPc2	iterace
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
tyto	tento	k3xDgInPc4	tento
útoky	útok	k1gInPc4	útok
nejsou	být	k5eNaImIp3nP	být
efektivní	efektivní	k2eAgMnSc1d1	efektivní
na	na	k7c4	na
plnou	plný	k2eAgFnSc4d1	plná
AES	AES	kA	AES
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
plnou	plný	k2eAgFnSc4d1	plná
AES	AES	kA	AES
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
obnovy	obnova	k1gFnSc2	obnova
klíče	klíč	k1gInSc2	klíč
byly	být	k5eAaImAgInP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
těmito	tento	k3xDgMnPc7	tento
autory	autor	k1gMnPc7	autor
<g/>
:	:	kIx,	:
Andrey	Andrea	k1gFnSc2	Andrea
Bogdanov	Bogdanov	k1gInSc1	Bogdanov
<g/>
,	,	kIx,	,
Dmitry	Dmitr	k1gInPc1	Dmitr
Khovratovich	Khovratovich	k1gInSc1	Khovratovich
a	a	k8xC	a
Christian	Christian	k1gMnSc1	Christian
Rechberger	Rechberger	k1gMnSc1	Rechberger
<g/>
.	.	kIx.	.
</s>
<s>
Klíč	klíč	k1gInSc1	klíč
je	být	k5eAaImIp3nS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
z	z	k7c2	z
AES-128	AES-128	k1gFnSc2	AES-128
za	za	k7c4	za
2126.1	[number]	k4	2126.1
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
AES-192	AES-192	k1gFnSc4	AES-192
a	a	k8xC	a
AES-256	AES-256	k1gFnSc4	AES-256
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
2189.7	[number]	k4	2189.7
a	a	k8xC	a
2254.4	[number]	k4	2254.4
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
útoku	útok	k1gInSc2	útok
neútočí	útočit	k5eNaImIp3nS	útočit
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
základní	základní	k2eAgFnSc4d1	základní
šifru	šifra	k1gFnSc4	šifra
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nemá	mít	k5eNaImIp3nS	mít
co	co	k3yInSc4	co
do	do	k7c2	do
činění	činění	k1gNnSc2	činění
s	s	k7c7	s
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
šifry	šifra	k1gFnSc2	šifra
jako	jako	k8xS	jako
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
implementaci	implementace	k1gFnSc4	implementace
na	na	k7c6	na
daném	daný	k2eAgInSc6d1	daný
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
někdy	někdy	k6eAd1	někdy
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
únikům	únik	k1gInPc3	únik
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
známých	známý	k2eAgInPc2d1	známý
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
určité	určitý	k2eAgFnPc4d1	určitá
implementace	implementace	k1gFnPc4	implementace
AES	AES	kA	AES
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
oznámil	oznámit	k5eAaPmAgMnS	oznámit
D.	D.	kA	D.
J.	J.	kA	J.
Bernstein	Bernstein	k1gInSc4	Bernstein
prolomení	prolomení	k1gNnSc2	prolomení
uživatelského	uživatelský	k2eAgInSc2d1	uživatelský
serveru	server	k1gInSc2	server
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
cache-timing	cacheiming	k1gInSc1	cache-timing
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc4	server
používal	používat	k5eAaImAgMnS	používat
OpenSSL	OpenSSL	k1gMnSc1	OpenSSL
AES	AES	kA	AES
šifrování	šifrování	k1gNnSc2	šifrování
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nastaven	nastavit	k5eAaPmNgInS	nastavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
možných	možný	k2eAgFnPc2d1	možná
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
útok	útok	k1gInSc1	útok
se	s	k7c7	s
složitostí	složitost	k1gFnSc7	složitost
232	[number]	k4	232
na	na	k7c6	na
implementaci	implementace	k1gFnSc6	implementace
hardwaru	hardware	k1gInSc2	hardware
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
použil	použít	k5eAaPmAgInS	použít
diferenciální	diferenciální	k2eAgFnSc4d1	diferenciální
analýzu	analýza	k1gFnSc4	analýza
chyby	chyba	k1gFnSc2	chyba
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
získání	získání	k1gNnSc4	získání
klíče	klíč	k1gInSc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
Endre	Endr	k1gInSc5	Endr
Bangerter	Bangerter	k1gMnSc1	Bangerter
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Gullasch	Gullasch	k1gMnSc1	Gullasch
a	a	k8xC	a
Stephan	Stephan	k1gMnSc1	Stephan
Krenn	Krenn	k1gMnSc1	Krenn
publikovali	publikovat	k5eAaBmAgMnP	publikovat
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
popisoval	popisovat	k5eAaImAgInS	popisovat
praktický	praktický	k2eAgInSc4d1	praktický
postup	postup	k1gInSc4	postup
jak	jak	k8xC	jak
v	v	k7c4	v
"	"	kIx"	"
<g/>
téměř	téměř	k6eAd1	téměř
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
<g/>
"	"	kIx"	"
obnovit	obnovit	k5eAaPmF	obnovit
tajné	tajný	k2eAgInPc4d1	tajný
klíče	klíč	k1gInPc4	klíč
z	z	k7c2	z
AES-128	AES-128	k1gFnSc2	AES-128
bez	bez	k7c2	bez
potřeby	potřeba	k1gFnSc2	potřeba
šifrovaného	šifrovaný	k2eAgInSc2d1	šifrovaný
nebo	nebo	k8xC	nebo
otevřeného	otevřený	k2eAgInSc2d1	otevřený
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
také	také	k9	také
fungoval	fungovat	k5eAaImAgInS	fungovat
vůči	vůči	k7c3	vůči
AES-128	AES-128	k1gFnSc3	AES-128
implementacím	implementace	k1gFnPc3	implementace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
používaly	používat	k5eAaImAgFnP	používat
zkomprimované	zkomprimovaný	k2eAgFnPc4d1	zkomprimovaná
tabulky	tabulka	k1gFnPc4	tabulka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
OpenSSL	OpenSSL	k1gFnSc1	OpenSSL
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
některé	některý	k3yIgInPc1	některý
dřívější	dřívější	k2eAgInPc1d1	dřívější
útoky	útok	k1gInPc1	útok
i	i	k8xC	i
tento	tento	k3xDgInSc1	tento
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
možnost	možnost	k1gFnSc4	možnost
spuštění	spuštění	k1gNnSc2	spuštění
libovolného	libovolný	k2eAgInSc2d1	libovolný
kódu	kód	k1gInSc2	kód
na	na	k7c6	na
systému	systém	k1gInSc6	systém
provádějící	provádějící	k2eAgFnSc1d1	provádějící
AES	AES	kA	AES
šifrování	šifrování	k1gNnSc2	šifrování
<g/>
.	.	kIx.	.
</s>
<s>
Validační	validační	k2eAgInSc1d1	validační
program	program	k1gInSc1	program
kryptografických	kryptografický	k2eAgInPc2d1	kryptografický
modulů	modul	k1gInPc2	modul
(	(	kIx(	(
<g/>
CMVP	CMVP	kA	CMVP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
provozován	provozovat	k5eAaImNgInS	provozovat
americkým	americký	k2eAgInSc7d1	americký
úřadem	úřad	k1gInSc7	úřad
NIST	NIST	kA	NIST
a	a	k8xC	a
kanadským	kanadský	k2eAgInSc7d1	kanadský
Institutem	institut	k1gInSc7	institut
komunikační	komunikační	k2eAgFnSc2d1	komunikační
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
CSE	CSE	kA	CSE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
schválených	schválený	k2eAgInPc2d1	schválený
kryptografických	kryptografický	k2eAgInPc2d1	kryptografický
modulů	modul	k1gInPc2	modul
není	být	k5eNaImIp3nS	být
vyžadováno	vyžadovat	k5eAaImNgNnS	vyžadovat
vládou	vláda	k1gFnSc7	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
přesně	přesně	k6eAd1	přesně
specifikované	specifikovaný	k2eAgNnSc4d1	specifikované
použití	použití	k1gNnSc4	použití
kryptografie	kryptografie	k1gFnSc2	kryptografie
<g/>
.	.	kIx.	.
</s>
<s>
Kanadská	kanadský	k2eAgFnSc1d1	kanadská
vláda	vláda	k1gFnSc1	vláda
pro	pro	k7c4	pro
takové	takový	k3xDgMnPc4	takový
aplikace	aplikace	k1gFnSc1	aplikace
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
použití	použití	k1gNnSc4	použití
FIPS	FIPS	kA	FIPS
140	[number]	k4	140
ověřených	ověřený	k2eAgInPc2d1	ověřený
kryptografických	kryptografický	k2eAgInPc2d1	kryptografický
modulů	modul	k1gInPc2	modul
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
publikace	publikace	k1gFnPc1	publikace
FIPS	FIPS	kA	FIPS
197	[number]	k4	197
vydaná	vydaný	k2eAgFnSc1d1	vydaná
organizací	organizace	k1gFnSc7	organizace
NIST	NIST	kA	NIST
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgInSc4d1	unikátní
dokument	dokument	k1gInSc4	dokument
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
AES	AES	kA	AES
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
CMVP	CMVP	kA	CMVP
pod	pod	k7c4	pod
FIPS	FIPS	kA	FIPS
140	[number]	k4	140
a	a	k8xC	a
ptají	ptat	k5eAaImIp3nP	ptat
se	se	k3xPyFc4	se
po	po	k7c6	po
dalších	další	k2eAgInPc6d1	další
starších	starý	k2eAgInPc6d2	starší
algoritmech	algoritmus	k1gInPc6	algoritmus
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Triple	tripl	k1gInSc5	tripl
DES	des	k1gNnPc7	des
nebo	nebo	k8xC	nebo
SHA	SHA	kA	SHA
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
jen	jen	k9	jen
ojediněle	ojediněle	k6eAd1	ojediněle
najít	najít	k5eAaPmF	najít
kryptografické	kryptografický	k2eAgInPc4d1	kryptografický
moduly	modul	k1gInPc4	modul
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
ověřeny	ověřit	k5eAaPmNgInP	ověřit
pouze	pouze	k6eAd1	pouze
FIPS	FIPS	kA	FIPS
197	[number]	k4	197
<g/>
,	,	kIx,	,
a	a	k8xC	a
samotný	samotný	k2eAgInSc1d1	samotný
NIST	NIST	kA	NIST
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
moc	moc	k6eAd1	moc
nezabývá	zabývat	k5eNaImIp3nS	zabývat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umístil	umístit	k5eAaPmAgMnS	umístit
schválené	schválený	k2eAgNnSc4d1	schválené
FIPS	FIPS	kA	FIPS
197	[number]	k4	197
moduly	modul	k1gInPc7	modul
odděleně	odděleně	k6eAd1	odděleně
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
webové	webový	k2eAgFnPc4d1	webová
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
místo	místo	k1gNnSc4	místo
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
FIPS	FIPS	kA	FIPS
197	[number]	k4	197
ověření	ověření	k1gNnSc1	ověření
běžně	běžně	k6eAd1	běžně
uvedeno	uvést	k5eAaPmNgNnS	uvést
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
"	"	kIx"	"
<g/>
FIPS	FIPS	kA	FIPS
schváleno	schválit	k5eAaPmNgNnS	schválit
<g/>
:	:	kIx,	:
AES	AES	kA	AES
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
uvedením	uvedení	k1gNnSc7	uvedení
FIPS	FIPS	kA	FIPS
197	[number]	k4	197
certifikačního	certifikační	k2eAgNnSc2d1	certifikační
čísla	číslo	k1gNnSc2	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
