<p>
<s>
Brasília	Brasília	k1gFnSc1	Brasília
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
hluboko	hluboko	k6eAd1	hluboko
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
,	,	kIx,	,
asi	asi	k9	asi
1000	[number]	k4	1000
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Ria	Ria	k1gFnSc2	Ria
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
okolní	okolní	k2eAgFnSc4d1	okolní
aglomeraci	aglomerace	k1gFnSc4	aglomerace
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
ve	v	k7c6	v
Federálním	federální	k2eAgInSc6d1	federální
distriktu	distrikt	k1gInSc6	distrikt
<g/>
)	)	kIx)	)
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
Brasílie	Brasílie	k1gFnSc2	Brasílie
dělá	dělat	k5eAaImIp3nS	dělat
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
největší	veliký	k2eAgNnSc4d3	veliký
brazilské	brazilský	k2eAgNnSc4d1	brazilské
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Brasília	Brasília	k1gFnSc1	Brasília
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
v	v	k7c6	v
Centrální	centrální	k2eAgFnSc6d1	centrální
brazilské	brazilský	k2eAgFnSc6d1	brazilská
vysočině	vysočina	k1gFnSc6	vysočina
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pramení	pramenit	k5eAaImIp3nS	pramenit
řeka	řeka	k1gFnSc1	řeka
Tocantins	Tocantinsa	k1gFnPc2	Tocantinsa
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
průměrné	průměrný	k2eAgFnSc6d1	průměrná
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
1172	[number]	k4	1172
m	m	kA	m
<g/>
,	,	kIx,	,
nejnižší	nízký	k2eAgInSc1d3	nejnižší
bod	bod	k1gInSc1	bod
leží	ležet	k5eAaImIp3nS	ležet
1067	[number]	k4	1067
m	m	kA	m
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
1230	[number]	k4	1230
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
velmi	velmi	k6eAd1	velmi
vyrovnaná	vyrovnaný	k2eAgFnSc1d1	vyrovnaná
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
23	[number]	k4	23
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
22	[number]	k4	22
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
spadne	spadnout	k5eAaPmIp3nS	spadnout
průměrně	průměrně	k6eAd1	průměrně
310	[number]	k4	310
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
nespadnou	spadnout	k5eNaPmIp3nP	spadnout
žádné	žádný	k3yNgInPc4	žádný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Brasília	Brasília	k1gFnSc1	Brasília
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
mladé	mladý	k2eAgNnSc4d1	mladé
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k8xS	jako
nová	nový	k2eAgFnSc1d1	nová
brazilská	brazilský	k2eAgFnSc1d1	brazilská
metropole	metropole	k1gFnSc1	metropole
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
vybudování	vybudování	k1gNnSc2	vybudování
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Brazílie	Brazílie	k1gFnSc2	Brazílie
Rio	Rio	k1gFnPc2	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc4	Janeiro
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
země	země	k1gFnSc1	země
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
zrodila	zrodit	k5eAaPmAgFnS	zrodit
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1822	[number]	k4	1822
<g/>
)	)	kIx)	)
myšlenka	myšlenka	k1gFnSc1	myšlenka
na	na	k7c6	na
vybudování	vybudování	k1gNnSc6	vybudování
nového	nový	k2eAgNnSc2d1	nové
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
návrhem	návrh	k1gInSc7	návrh
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
jméno	jméno	k1gNnSc1	jméno
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
město	město	k1gNnSc4	město
–	–	k?	–
mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
jmenovat	jmenovat	k5eAaBmF	jmenovat
právě	právě	k9	právě
Brasília	Brasília	k1gFnSc1	Brasília
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
idea	idea	k1gFnSc1	idea
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
projektu	projekt	k1gInSc2	projekt
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
když	když	k8xS	když
začaly	začít	k5eAaPmAgFnP	začít
přípravné	přípravný	k2eAgFnPc4d1	přípravná
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
a	a	k8xC	a
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1956	[number]	k4	1956
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
brazilským	brazilský	k2eAgMnSc7d1	brazilský
prezidentem	prezident	k1gMnSc7	prezident
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Juscelinem	Juscelin	k1gInSc7	Juscelin
Kubitschekem	Kubitschek	k1gInSc7	Kubitschek
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
nová	nový	k2eAgFnSc1d1	nová
metropole	metropole	k1gFnSc1	metropole
nebyla	být	k5eNaImAgFnS	být
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
též	též	k9	též
vystavět	vystavět	k5eAaPmF	vystavět
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
silniční	silniční	k2eAgFnSc4d1	silniční
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
nové	nový	k2eAgNnSc1d1	nové
město	město	k1gNnSc1	město
spojily	spojit	k5eAaPmAgFnP	spojit
se	s	k7c7	s
Sã	Sã	k1gFnSc7	Sã
Paulem	Paul	k1gMnSc7	Paul
<g/>
,	,	kIx,	,
Rio	Rio	k1gFnSc1	Rio
de	de	k?	de
Janeirem	Janeirma	k1gFnPc2	Janeirma
a	a	k8xC	a
dalšími	další	k2eAgNnPc7d1	další
důležitými	důležitý	k2eAgNnPc7d1	důležité
sídly	sídlo	k1gNnPc7	sídlo
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rozložení	rozložení	k1gNnSc1	rozložení
města	město	k1gNnSc2	město
odráží	odrážet	k5eAaImIp3nS	odrážet
jeho	jeho	k3xOp3gInSc1	jeho
moderní	moderní	k2eAgInSc1d1	moderní
charakter	charakter	k1gInSc1	charakter
<g/>
;	;	kIx,	;
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
čtvrti	čtvrt	k1gFnPc1	čtvrt
jsou	být	k5eAaImIp3nP	být
oddělené	oddělený	k2eAgFnPc1d1	oddělená
<g/>
,	,	kIx,	,
silnice	silnice	k1gFnPc1	silnice
byly	být	k5eAaImAgFnP	být
vyprojektovány	vyprojektovat	k5eAaPmNgFnP	vyprojektovat
jako	jako	k9	jako
široké	široký	k2eAgFnPc1d1	široká
a	a	k8xC	a
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
mnohaproudé	mnohaproudý	k2eAgFnPc1d1	mnohaproudý
komunikace	komunikace	k1gFnPc1	komunikace
s	s	k7c7	s
mimoúrovňovými	mimoúrovňový	k2eAgFnPc7d1	mimoúrovňová
křižovatkami	křižovatka	k1gFnPc7	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Architektura	architektura	k1gFnSc1	architektura
významných	významný	k2eAgFnPc2d1	významná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
státních	státní	k2eAgInPc2d1	státní
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejtypičtějším	typický	k2eAgFnPc3d3	nejtypičtější
ukázkám	ukázka	k1gFnPc3	ukázka
architektury	architektura	k1gFnSc2	architektura
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
slavnostnímu	slavnostní	k2eAgNnSc3d1	slavnostní
otevření	otevření	k1gNnSc3	otevření
nového	nový	k2eAgNnSc2d1	nové
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
slavnostního	slavnostní	k2eAgInSc2d1	slavnostní
obřadu	obřad	k1gInSc2	obřad
byl	být	k5eAaImAgInS	být
i	i	k9	i
náboženský	náboženský	k2eAgInSc1d1	náboženský
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
a	a	k8xC	a
na	na	k7c6	na
Centrálním	centrální	k2eAgNnSc6d1	centrální
náměstí	náměstí	k1gNnSc6	náměstí
byl	být	k5eAaImAgInS	být
vztyčen	vztyčen	k2eAgInSc1d1	vztyčen
železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
z	z	k7c2	z
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
je	být	k5eAaImIp3nS	být
Brasília	Brasília	k1gFnSc1	Brasília
zařazená	zařazený	k2eAgFnSc1d1	zařazená
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
má	mít	k5eAaImIp3nS	mít
Brasília	Brasília	k1gFnSc1	Brasília
také	také	k9	také
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
systém	systém	k1gInSc4	systém
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
ekonomickým	ekonomický	k2eAgNnSc7d1	ekonomické
odvětvím	odvětví	k1gNnSc7	odvětví
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
stavebnictví	stavebnictví	k1gNnSc4	stavebnictví
<g/>
,	,	kIx,	,
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
komunikace	komunikace	k1gFnPc4	komunikace
<g/>
,	,	kIx,	,
finančnictví	finančnictví	k1gNnSc4	finančnictví
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
,	,	kIx,	,
dřevařský	dřevařský	k2eAgInSc4d1	dřevařský
a	a	k8xC	a
zábavní	zábavní	k2eAgInSc4d1	zábavní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
spíše	spíše	k9	spíše
vládní	vládní	k2eAgInSc4d1	vládní
než	než	k8xS	než
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
několika	několik	k4yIc2	několik
národních	národní	k2eAgFnPc2d1	národní
společností	společnost	k1gFnPc2	společnost
jako	jako	k8xS	jako
Brasil	Brasil	k1gFnSc1	Brasil
Telecom	Telecom	k1gInSc1	Telecom
<g/>
,	,	kIx,	,
Correios	Correios	k1gInSc1	Correios
<g/>
,	,	kIx,	,
banky	banka	k1gFnPc1	banka
Caixa	Caixa	k1gMnSc1	Caixa
Econômica	Econômica	k1gMnSc1	Econômica
Federal	Federal	k1gMnSc1	Federal
a	a	k8xC	a
největší	veliký	k2eAgMnSc1d3	veliký
Banco	Banco	k1gMnSc1	Banco
do	do	k7c2	do
Brasil	Brasil	k1gFnSc2	Brasil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Brasília	Brasílium	k1gNnSc2	Brasílium
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
polohu	poloha	k1gFnSc4	poloha
hlavním	hlavní	k2eAgInSc7d1	hlavní
leteckým	letecký	k2eAgInSc7d1	letecký
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
přepravených	přepravený	k2eAgMnPc2d1	přepravený
pasažérů	pasažér	k1gMnPc2	pasažér
a	a	k8xC	a
pohybu	pohyb	k1gInSc6	pohyb
letadel	letadlo	k1gNnPc2	letadlo
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
nejrušnější	rušný	k2eAgFnSc1d3	nejrušnější
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
spuštěno	spuštěn	k2eAgNnSc1d1	spuštěno
metro	metro	k1gNnSc1	metro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Patronem	patron	k1gMnSc7	patron
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Bosco	Bosco	k1gMnSc1	Bosco
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
získalo	získat	k5eAaPmAgNnS	získat
město	město	k1gNnSc1	město
titul	titul	k1gInSc4	titul
americké	americký	k2eAgNnSc4d1	americké
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavá	zajímavý	k2eAgNnPc1d1	zajímavé
místa	místo	k1gNnPc1	místo
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Brasília	Brasília	k1gFnSc1	Brasília
není	být	k5eNaImIp3nS	být
nikterak	nikterak	k6eAd1	nikterak
staré	starý	k2eAgNnSc4d1	staré
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
zajímavých	zajímavý	k2eAgNnPc2d1	zajímavé
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Tří	tři	k4xCgFnPc2	tři
mocností	mocnost	k1gFnPc2	mocnost
stojí	stát	k5eAaImIp3nS	stát
budova	budova	k1gFnSc1	budova
národního	národní	k2eAgInSc2d1	národní
kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
památník	památník	k1gInSc4	památník
připomínající	připomínající	k2eAgNnSc4d1	připomínající
založení	založení	k1gNnSc4	založení
města	město	k1gNnSc2	město
a	a	k8xC	a
palác	palác	k1gInSc1	palác
Planalto	Planalto	k1gNnSc4	Planalto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
kanceláře	kancelář	k1gFnPc4	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
budovou	budova	k1gFnSc7	budova
je	být	k5eAaImIp3nS	být
také	také	k9	také
moderní	moderní	k2eAgFnSc1d1	moderní
Katedrála	katedrála	k1gFnSc1	katedrála
Zjevení	zjevení	k1gNnSc1	zjevení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
svojí	svůj	k3xOyFgFnSc7	svůj
konstrukcí	konstrukce	k1gFnSc7	konstrukce
připomíná	připomínat	k5eAaImIp3nS	připomínat
kostru	kostra	k1gFnSc4	kostra
obrovského	obrovský	k2eAgMnSc2d1	obrovský
obratlovce	obratlovec	k1gMnSc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zajímavé	zajímavý	k2eAgInPc4d1	zajímavý
mosty	most	k1gInPc4	most
patří	patřit	k5eAaImIp3nS	patřit
obloukový	obloukový	k2eAgInSc1d1	obloukový
most	most	k1gInSc1	most
Ponte	Pont	k1gInSc5	Pont
Juscelino	Juscelin	k2eAgNnSc1d1	Juscelino
Kubitschek	Kubitschek	k6eAd1	Kubitschek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Oscar	Oscar	k1gMnSc1	Oscar
Niemeyer	Niemeyer	k1gMnSc1	Niemeyer
</s>
</p>
<p>
<s>
Juscelino	Juscelin	k2eAgNnSc1d1	Juscelino
Kubitschek	Kubitschek	k6eAd1	Kubitschek
de	de	k?	de
Oliveira	Oliveira	k1gMnSc1	Oliveira
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brasília	Brasílium	k1gNnSc2	Brasílium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Brasilia	Brasilia	k1gFnSc1	Brasilia
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
dál	daleko	k6eAd2	daleko
<g/>
...	...	k?	...
Brasilia	Brasilium	k1gNnPc4	Brasilium
–	–	k?	–
architektonická	architektonický	k2eAgFnSc5d1	architektonická
přednáška	přednáška	k1gFnSc1	přednáška
Davida	David	k1gMnSc2	David
Vávry	Vávra	k1gMnSc2	Vávra
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Brasília	Brasílium	k1gNnSc2	Brasílium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
