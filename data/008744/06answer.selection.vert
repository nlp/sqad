<s>
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Székesfehérvár	Székesfehérvár	k1gInSc1	Székesfehérvár
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
maďarský	maďarský	k2eAgMnSc1d1	maďarský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
strany	strana	k1gFnSc2	strana
Fidesz-MPS	Fidesz-MPS	k1gFnSc2	Fidesz-MPS
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
premiér	premiéra	k1gFnPc2	premiéra
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
byl	být	k5eAaImAgInS	být
také	také	k9	také
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
