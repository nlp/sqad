<p>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
trvající	trvající	k2eAgInSc1d1	trvající
válečný	válečný	k2eAgInSc1d1	válečný
konflikt	konflikt	k1gInSc1	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
započaly	započnout	k5eAaPmAgInP	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
když	když	k8xS	když
ozbrojená	ozbrojený	k2eAgFnSc1d1	ozbrojená
klanová	klanový	k2eAgFnSc1d1	klanová
opozice	opozice	k1gFnSc1	opozice
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
režim	režim	k1gInSc4	režim
diktátora	diktátor	k1gMnSc2	diktátor
Muhammada	Muhammada	k1gFnSc1	Muhammada
Siada	Siad	k1gMnSc4	Siad
Barreho	Barre	k1gMnSc4	Barre
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
nastalém	nastalý	k2eAgNnSc6d1	nastalé
mocenském	mocenský	k2eAgNnSc6d1	mocenské
vakuu	vakuum	k1gNnSc6	vakuum
začaly	začít	k5eAaPmAgFnP	začít
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
moc	moc	k6eAd1	moc
různé	různý	k2eAgFnPc4d1	různá
frakce	frakce	k1gFnPc4	frakce
<g/>
.	.	kIx.	.
</s>
<s>
Selhala	selhat	k5eAaPmAgFnS	selhat
i	i	k9	i
intervence	intervence	k1gFnSc1	intervence
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
mírové	mírový	k2eAgFnPc1d1	mírová
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
stáhly	stáhnout	k5eAaPmAgFnP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
období	období	k1gNnSc1	období
decentralizace	decentralizace	k1gFnSc2	decentralizace
charakterizované	charakterizovaný	k2eAgFnSc2d1	charakterizovaná
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
oblastí	oblast	k1gFnPc2	oblast
návratem	návrat	k1gInSc7	návrat
ke	k	k7c3	k
kmenovým	kmenový	k2eAgInPc3d1	kmenový
a	a	k8xC	a
náboženským	náboženský	k2eAgInPc3d1	náboženský
zákonům	zákon	k1gInPc3	zákon
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
vznikem	vznik	k1gInSc7	vznik
řady	řada	k1gFnSc2	řada
autonomních	autonomní	k2eAgFnPc2d1	autonomní
oblastních	oblastní	k2eAgFnPc2d1	oblastní
vlád	vláda	k1gFnPc2	vláda
především	především	k9	především
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgInPc1	první
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
nové	nový	k2eAgFnSc2d1	nová
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
vytvořením	vytvoření	k1gNnSc7	vytvoření
Somálské	somálský	k2eAgFnSc2d1	Somálská
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
Transitional	Transitional	k1gMnSc1	Transitional
Federal	Federal	k1gMnSc1	Federal
Government	Government	k1gMnSc1	Government
<g/>
,	,	kIx,	,
TSG	TSG	kA	TSG
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zahájila	zahájit	k5eAaPmAgFnS	zahájit
TFG	TFG	kA	TFG
<g/>
,	,	kIx,	,
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
etiopské	etiopský	k2eAgFnSc2d1	etiopská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
proti	proti	k7c3	proti
Svazu	svaz	k1gInSc3	svaz
islámských	islámský	k2eAgInPc2d1	islámský
soudů	soud	k1gInPc2	soud
(	(	kIx(	(
<g/>
Islamic	Islamice	k1gFnPc2	Islamice
Court	Courta	k1gFnPc2	Courta
Union	union	k1gInSc1	union
<g/>
,	,	kIx,	,
ICU	ICU	kA	ICU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kontroloval	kontrolovat	k5eAaImAgInS	kontrolovat
velká	velký	k2eAgNnPc4d1	velké
území	území	k1gNnSc4	území
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
ICU	ICU	kA	ICU
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
a	a	k8xC	a
zanechal	zanechat	k5eAaPmAgInS	zanechat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
několik	několik	k4yIc4	několik
menších	malý	k2eAgMnPc2d2	menší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
radikálnějších	radikální	k2eAgNnPc2d2	radikálnější
uskupení	uskupení	k1gNnPc2	uskupení
<g/>
,	,	kIx,	,
především	především	k9	především
Aš-Šabáb	Aš-Šabáb	k1gMnSc1	Aš-Šabáb
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgNnPc7	tento
uskupeními	uskupení	k1gNnPc7	uskupení
a	a	k8xC	a
somálskou	somálský	k2eAgFnSc7d1	Somálská
vládou	vláda	k1gFnSc7	vláda
podporovanou	podporovaný	k2eAgFnSc7d1	podporovaná
spojenci	spojenec	k1gMnPc7	spojenec
z	z	k7c2	z
mise	mise	k1gFnSc2	mise
Africké	africký	k2eAgFnSc2d1	africká
unie	unie	k1gFnSc2	unie
AMISOM	AMISOM	kA	AMISOM
o	o	k7c4	o
kontrolu	kontrola	k1gFnSc4	kontrola
jižních	jižní	k2eAgNnPc2d1	jižní
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Spojená	spojený	k2eAgFnSc1d1	spojená
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
vládních	vládní	k2eAgFnPc2d1	vládní
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
afrických	africký	k2eAgMnPc2d1	africký
spojenců	spojenec	k1gMnPc2	spojenec
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
islámské	islámský	k2eAgFnPc4d1	islámská
milice	milice	k1gFnPc4	milice
z	z	k7c2	z
klíčových	klíčový	k2eAgFnPc2d1	klíčová
bašt	bašta	k1gFnPc2	bašta
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
milníkem	milník	k1gInSc7	milník
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
radikálním	radikální	k2eAgMnPc3d1	radikální
islamistům	islamista	k1gMnPc3	islamista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pád	Pád	k1gInSc1	Pád
Barrova	Barrův	k2eAgInSc2d1	Barrův
režimu	režim	k1gInSc2	režim
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Etiopií	Etiopie	k1gFnSc7	Etiopie
o	o	k7c4	o
Ogaden	Ogadna	k1gFnPc2	Ogadna
v	v	k7c6	v
letech	let	k1gInPc6	let
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
socialistická	socialistický	k2eAgFnSc1d1	socialistická
vláda	vláda	k1gFnSc1	vláda
Somálské	somálský	k2eAgFnSc2d1	Somálská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generálmajora	generálmajor	k1gMnSc2	generálmajor
Muhammada	Muhammada	k1gFnSc1	Muhammada
Siada	Siada	k1gFnSc1	Siada
Barreho	Barre	k1gMnSc2	Barre
začala	začít	k5eAaPmAgFnS	začít
zatýkat	zatýkat	k5eAaImF	zatýkat
vládní	vládní	k2eAgMnPc4d1	vládní
a	a	k8xC	a
armádní	armádní	k2eAgMnPc4d1	armádní
úředníky	úředník	k1gMnPc4	úředník
kvůli	kvůli	k7c3	kvůli
podezření	podezření	k1gNnSc3	podezření
z	z	k7c2	z
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
nezdařeném	zdařený	k2eNgInSc6d1	nezdařený
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgFnPc2	tento
osob	osoba	k1gFnPc2	osoba
byla	být	k5eAaImAgFnS	být
popravena	popravit	k5eAaPmNgFnS	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
před	před	k7c7	před
zatčením	zatčení	k1gNnSc7	zatčení
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
formovat	formovat	k5eAaImF	formovat
první	první	k4xOgFnPc1	první
disidentské	disidentský	k2eAgFnPc1d1	disidentská
skupiny	skupina	k1gFnPc1	skupina
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
svrhnout	svrhnout	k5eAaPmF	svrhnout
Barreho	Barre	k1gMnSc4	Barre
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1986	[number]	k4	1986
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
Barre	Barr	k1gInSc5	Barr
vážná	vážný	k2eAgNnPc4d1	vážné
zranění	zranění	k1gNnPc4	zranění
při	při	k7c6	při
automobilové	automobilový	k2eAgFnSc6d1	automobilová
nehodě	nehoda	k1gFnSc6	nehoda
nedaleko	nedaleko	k7c2	nedaleko
Mogadiša	Mogadišo	k1gNnSc2	Mogadišo
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
hustém	hustý	k2eAgInSc6d1	hustý
dešti	dešť	k1gInSc6	dešť
automobil	automobil	k1gInSc4	automobil
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gMnSc4	on
převážel	převážet	k5eAaImAgMnS	převážet
<g/>
,	,	kIx,	,
narazil	narazit	k5eAaPmAgMnS	narazit
zezadu	zezadu	k6eAd1	zezadu
do	do	k7c2	do
autobusu	autobus	k1gInSc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zraněním	zranění	k1gNnSc7	zranění
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
zlomenými	zlomený	k2eAgInPc7d1	zlomený
žebry	žebr	k1gInPc7	žebr
a	a	k8xC	a
šokem	šok	k1gInSc7	šok
strávil	strávit	k5eAaPmAgMnS	strávit
Barre	Barr	k1gInSc5	Barr
více	hodně	k6eAd2	hodně
než	než	k8xS	než
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Generálporučík	generálporučík	k1gMnSc1	generálporučík
Muhammad	Muhammad	k1gInSc4	Muhammad
Ali	Ali	k1gMnSc1	Ali
Samatar	Samatar	k1gMnSc1	Samatar
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
stal	stát	k5eAaPmAgInS	stát
de	de	k?	de
facto	facto	k1gNnSc4	facto
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Barre	Barr	k1gMnSc5	Barr
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
zotavil	zotavit	k5eAaPmAgMnS	zotavit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1986	[number]	k4	1986
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
kandidát	kandidát	k1gMnSc1	kandidát
nechal	nechat	k5eAaPmAgMnS	nechat
zvolit	zvolit	k5eAaPmF	zvolit
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
sedmileté	sedmiletý	k2eAgNnSc4d1	sedmileté
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
chatrné	chatrný	k2eAgNnSc4d1	chatrné
zdraví	zdraví	k1gNnSc4	zdraví
však	však	k9	však
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
spekulacím	spekulace	k1gFnPc3	spekulace
nad	nad	k7c7	nad
případným	případný	k2eAgNnSc7d1	případné
následnictvím	následnictví	k1gNnSc7	následnictví
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
možnými	možný	k2eAgMnPc7d1	možný
nástupci	nástupce	k1gMnPc7	nástupce
byli	být	k5eAaImAgMnP	být
zmiňováni	zmiňovat	k5eAaImNgMnP	zmiňovat
Barreho	Barre	k1gMnSc2	Barre
tchán	tchán	k1gMnSc1	tchán
generál	generál	k1gMnSc1	generál
Ahmed	Ahmed	k1gMnSc1	Ahmed
Suleiman	Suleiman	k1gMnSc1	Suleiman
Abdille	Abdill	k1gMnSc2	Abdill
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zastával	zastávat	k5eAaImAgMnS	zastávat
úřad	úřad	k1gInSc4	úřad
ministra	ministr	k1gMnSc2	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
viceprezident	viceprezident	k1gMnSc1	viceprezident
generálporučík	generálporučík	k1gMnSc1	generálporučík
Samatar	Samatar	k1gMnSc1	Samatar
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
morální	morální	k2eAgFnSc1d1	morální
autorita	autorita	k1gFnSc1	autorita
Barreho	Barre	k1gMnSc2	Barre
režimu	režim	k1gInSc2	režim
silně	silně	k6eAd1	silně
upadla	upadnout	k5eAaPmAgFnS	upadnout
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Somálců	Somálec	k1gMnPc2	Somálec
bylo	být	k5eAaImAgNnS	být
zklamáno	zklamat	k5eAaPmNgNnS	zklamat
životem	život	k1gInSc7	život
pod	pod	k7c7	pod
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
diktaturou	diktatura	k1gFnSc7	diktatura
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
navíc	navíc	k6eAd1	navíc
skončila	skončit	k5eAaPmAgFnS	skončit
studená	studený	k2eAgFnSc1d1	studená
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
Somálsko	Somálsko	k1gNnSc1	Somálsko
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
strategický	strategický	k2eAgInSc4d1	strategický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Protivládní	protivládní	k2eAgNnPc1d1	protivládní
hnutí	hnutí	k1gNnPc1	hnutí
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
etiopskou	etiopský	k2eAgFnSc7d1	etiopská
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
vládou	vláda	k1gFnSc7	vláda
(	(	kIx(	(
<g/>
Derg	Derg	k1gInSc1	Derg
<g/>
)	)	kIx)	)
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
pádu	pád	k1gInSc3	pád
Barreho	Barre	k1gMnSc2	Barre
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
Somálské	somálský	k2eAgFnSc2d1	Somálská
národní	národní	k2eAgFnSc2d1	národní
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
SNA	sen	k1gInSc2	sen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
skupinami	skupina	k1gFnPc7	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgFnP	podílet
na	na	k7c6	na
pádu	pád	k1gInSc6	pád
Barreho	Barre	k1gMnSc2	Barre
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
fronta	fronta	k1gFnSc1	fronta
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
Somálska	Somálsko	k1gNnSc2	Somálsko
(	(	kIx(	(
<g/>
SSDF	SSDF	kA	SSDF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Somálské	somálský	k2eAgNnSc4d1	somálské
národní	národní	k2eAgNnSc4d1	národní
hnutí	hnutí	k1gNnSc4	hnutí
(	(	kIx(	(
<g/>
SNM	SNM	kA	SNM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Somálské	somálský	k2eAgNnSc4d1	somálské
vlastenecké	vlastenecký	k2eAgNnSc4d1	vlastenecké
hnutí	hnutí	k1gNnSc4	hnutí
(	(	kIx(	(
<g/>
SPM	SPM	kA	SPM
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jednotný	jednotný	k2eAgInSc1d1	jednotný
somálský	somálský	k2eAgInSc1d1	somálský
kongres	kongres	k1gInSc1	kongres
(	(	kIx(	(
<g/>
USC	USC	kA	USC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
opoziční	opoziční	k2eAgFnPc1d1	opoziční
skupiny	skupina	k1gFnPc1	skupina
však	však	k9	však
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
režimu	režim	k1gInSc2	režim
brzy	brzy	k6eAd1	brzy
začaly	začít	k5eAaPmAgInP	začít
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
USC	USC	kA	USC
se	se	k3xPyFc4	se
rozštěpil	rozštěpit	k5eAaPmAgInS	rozštěpit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Mohameda	Mohamed	k1gMnSc2	Mohamed
Farraha	Farrah	k1gMnSc2	Farrah
Aidida	Aidid	k1gMnSc2	Aidid
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Aliho	Ali	k1gMnSc2	Ali
Mahdiho	Mahdi	k1gMnSc2	Mahdi
Muhammada	Muhammada	k1gFnSc1	Muhammada
<g/>
.	.	kIx.	.
</s>
<s>
Muhammad	Muhammad	k6eAd1	Muhammad
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
dočasným	dočasný	k2eAgMnSc7d1	dočasný
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
což	což	k9	což
ovšem	ovšem	k9	ovšem
Aidid	Aidid	k1gInSc1	Aidid
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
uznat	uznat	k5eAaPmF	uznat
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
frakce	frakce	k1gFnPc1	frakce
začaly	začít	k5eAaPmAgFnP	začít
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Mogadišem	Mogadišo	k1gNnSc7	Mogadišo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Intervence	intervence	k1gFnPc4	intervence
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Rezoluce	rezoluce	k1gFnSc1	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
733	[number]	k4	733
a	a	k8xC	a
Rezoluce	rezoluce	k1gFnSc1	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
746	[number]	k4	746
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
mise	mise	k1gFnSc2	mise
UNOSOM	UNOSOM	kA	UNOSOM
I	i	k9	i
(	(	kIx(	(
<g/>
United	United	k1gInSc1	United
Nations	Nations	k1gInSc1	Nations
Operation	Operation	k1gInSc1	Operation
in	in	k?	in
Somalia	Somalia	k1gFnSc1	Somalia
I	I	kA	I
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
byla	být	k5eAaImAgFnS	být
humanitární	humanitární	k2eAgFnSc4d1	humanitární
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
nastolení	nastolení	k1gNnSc4	nastolení
pořádku	pořádek	k1gInSc2	pořádek
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rezoluce	rezoluce	k1gFnSc1	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
794	[number]	k4	794
<g/>
,	,	kIx,	,
jednomyslně	jednomyslně	k6eAd1	jednomyslně
přijatá	přijatý	k2eAgFnSc1d1	přijatá
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
schválila	schválit	k5eAaPmAgFnS	schválit
vytvoření	vytvoření	k1gNnSc4	vytvoření
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
mírových	mírový	k2eAgFnPc2d1	mírová
sil	síla	k1gFnPc2	síla
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
UNITAF	UNITAF	kA	UNITAF
(	(	kIx(	(
<g/>
Unified	Unified	k1gMnSc1	Unified
Task	Task	k1gMnSc1	Task
Force	force	k1gFnSc1	force
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgInP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zajistit	zajistit	k5eAaPmF	zajistit
v	v	k7c6	v
regionu	region	k1gInSc6	region
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zajistit	zajistit	k5eAaPmF	zajistit
dodávky	dodávka	k1gFnPc4	dodávka
humanitární	humanitární	k2eAgFnSc2d1	humanitární
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Příchod	příchod	k1gInSc1	příchod
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
misi	mise	k1gFnSc4	mise
UNOSOM	UNOSOM	kA	UNOSOM
II	II	kA	II
(	(	kIx(	(
<g/>
United	United	k1gInSc1	United
Nations	Nations	k1gInSc1	Nations
Operation	Operation	k1gInSc1	Operation
in	in	k?	in
Somalia	Somalia	k1gFnSc1	Somalia
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
operovaly	operovat	k5eAaImAgFnP	operovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgInSc7d1	původní
cílem	cíl	k1gInSc7	cíl
UNITAF	UNITAF	kA	UNITAF
bylo	být	k5eAaImAgNnS	být
zajistit	zajistit	k5eAaPmF	zajistit
doručení	doručení	k1gNnSc4	doručení
humanitární	humanitární	k2eAgFnSc2d1	humanitární
pomoci	pomoc	k1gFnSc2	pomoc
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
(	(	kIx(	(
<g/>
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
kapitolou	kapitola	k1gFnSc7	kapitola
VII	VII	kA	VII
Charty	charta	k1gFnSc2	charta
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
"	"	kIx"	"
<g/>
všech	všecek	k3xTgInPc2	všecek
nezbytných	nezbytný	k2eAgInPc2d1	nezbytný
prostředků	prostředek	k1gInPc2	prostředek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
je	být	k5eAaImIp3nS	být
mise	mise	k1gFnSc1	mise
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
frakcí	frakce	k1gFnPc2	frakce
bojujících	bojující	k2eAgFnPc2d1	bojující
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
však	však	k9	však
přítomnost	přítomnost	k1gFnSc4	přítomnost
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
jednotek	jednotka	k1gFnPc2	jednotka
považovaly	považovat	k5eAaImAgFnP	považovat
za	za	k7c4	za
ohrožení	ohrožení	k1gNnSc4	ohrožení
jejich	jejich	k3xOp3gFnSc2	jejich
hegemonie	hegemonie	k1gFnSc2	hegemonie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
místními	místní	k2eAgFnPc7d1	místní
milicemi	milice	k1gFnPc7	milice
a	a	k8xC	a
mírovými	mírový	k2eAgFnPc7d1	mírová
jednotkami	jednotka	k1gFnPc7	jednotka
tak	tak	k6eAd1	tak
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
k	k	k7c3	k
přestřelkám	přestřelka	k1gFnPc3	přestřelka
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
bojů	boj	k1gInPc2	boj
byla	být	k5eAaImAgFnS	být
i	i	k9	i
známá	známý	k2eAgFnSc1d1	známá
bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vykreslená	vykreslený	k2eAgFnSc1d1	vykreslená
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Černý	černý	k2eAgMnSc1d1	černý
jestřáb	jestřáb	k1gMnSc1	jestřáb
sestřelen	sestřelen	k2eAgMnSc1d1	sestřelen
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
operaci	operace	k1gFnSc6	operace
Gothic	Gothice	k1gFnPc2	Gothice
Serpent	Serpent	k1gInSc1	Serpent
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusily	pokusit	k5eAaPmAgFnP	pokusit
zadržet	zadržet	k5eAaPmF	zadržet
Mohameda	Mohamed	k1gMnSc4	Mohamed
Farraha	Farrah	k1gMnSc4	Farrah
Aidida	Aidid	k1gMnSc4	Aidid
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc4	vůdce
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
mocenských	mocenský	k2eAgFnPc2d1	mocenská
frakcí	frakce	k1gFnPc2	frakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
narůstajících	narůstající	k2eAgFnPc2d1	narůstající
ztrát	ztráta	k1gFnPc2	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
ze	z	k7c2	z
Somálska	Somálsko	k1gNnSc2	Somálsko
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Odchod	odchod	k1gInSc1	odchod
mírových	mírový	k2eAgFnPc2d1	mírová
sil	síla	k1gFnPc2	síla
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
k	k	k7c3	k
3	[number]	k4	3
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc3	březen
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Decentralizace	decentralizace	k1gFnSc2	decentralizace
==	==	k?	==
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
neexistenci	neexistence	k1gFnSc3	neexistence
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
uplatňovala	uplatňovat	k5eAaImAgFnS	uplatňovat
svou	svůj	k3xOyFgFnSc4	svůj
pravomoc	pravomoc	k1gFnSc4	pravomoc
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
některé	některý	k3yIgFnSc2	některý
oblasti	oblast	k1gFnSc2	oblast
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
samosprávu	samospráva	k1gFnSc4	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Barreho	Barre	k1gMnSc2	Barre
režimu	režim	k1gInSc2	režim
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
Somaliland	Somalilanda	k1gFnPc2	Somalilanda
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
bývalého	bývalý	k2eAgNnSc2d1	bývalé
Britského	britský	k2eAgNnSc2d1	Britské
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
velká	velký	k2eAgFnSc1d1	velká
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
,	,	kIx,	,
Puntland	Puntland	k1gInSc4	Puntland
<g/>
,	,	kIx,	,
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
nebyl	být	k5eNaImAgInS	být
dodnes	dodnes	k6eAd1	dodnes
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznán	uznat	k5eAaPmNgInS	uznat
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Somaliland	Somaliland	k1gInSc1	Somaliland
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
úplné	úplný	k2eAgNnSc4d1	úplné
odtržení	odtržení	k1gNnSc4	odtržení
<g/>
,	,	kIx,	,
Puntland	Puntland	k1gInSc1	Puntland
je	být	k5eAaImIp3nS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
podřídit	podřídit	k5eAaPmF	podřídit
se	se	k3xPyFc4	se
autoritě	autorita	k1gFnSc3	autorita
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
státy	stát	k1gInPc1	stát
vedou	vést	k5eAaImIp3nP	vést
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
pohraniční	pohraniční	k2eAgInSc4d1	pohraniční
spory	spor	k1gInPc4	spor
o	o	k7c4	o
regiony	region	k1gInPc4	region
Sanaag	Sanaaga	k1gFnPc2	Sanaaga
a	a	k8xC	a
Sool	Soola	k1gFnPc2	Soola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesla	přinést	k5eAaPmAgFnS	přinést
i	i	k9	i
rozpad	rozpad	k1gInSc4	rozpad
soudního	soudní	k2eAgInSc2d1	soudní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Somálcům	Somálec	k1gMnPc3	Somálec
tedy	tedy	k9	tedy
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
oblastí	oblast	k1gFnPc2	oblast
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
k	k	k7c3	k
místním	místní	k2eAgFnPc3d1	místní
formám	forma	k1gFnPc3	forma
řešení	řešení	k1gNnSc2	řešení
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
:	:	kIx,	:
různým	různý	k2eAgFnPc3d1	různá
formám	forma	k1gFnPc3	forma
světského	světský	k2eAgNnSc2d1	světské
<g/>
,	,	kIx,	,
zvykového	zvykový	k2eAgNnSc2d1	zvykové
nebo	nebo	k8xC	nebo
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Puntlandu	Puntlando	k1gNnSc6	Puntlando
a	a	k8xC	a
Somalilandu	Somalilando	k1gNnSc6	Somalilando
byly	být	k5eAaImAgInP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
nové	nový	k2eAgInPc1d1	nový
soudní	soudní	k2eAgInPc1d1	soudní
systémy	systém	k1gInPc1	systém
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
místních	místní	k2eAgFnPc2d1	místní
vlád	vláda	k1gFnPc2	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Somálské	somálský	k2eAgFnSc2d1	Somálská
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
TSG	TSG	kA	TSG
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
základy	základ	k1gInPc1	základ
nového	nový	k2eAgInSc2d1	nový
soudního	soudní	k2eAgInSc2d1	soudní
systému	systém	k1gInSc2	systém
dojednávány	dojednávat	k5eAaImNgInP	dojednávat
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
konferencích	konference	k1gFnPc6	konference
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
různé	různý	k2eAgInPc4d1	různý
politické	politický	k2eAgInPc4d1	politický
rozdíly	rozdíl	k1gInPc4	rozdíl
mají	mít	k5eAaImIp3nP	mít
zmíněné	zmíněný	k2eAgInPc4d1	zmíněný
soudní	soudní	k2eAgInPc4d1	soudní
systémy	systém	k1gInPc4	systém
některé	některý	k3yIgInPc4	některý
základní	základní	k2eAgInPc4d1	základní
prvky	prvek	k1gInPc4	prvek
společné	společný	k2eAgInPc4d1	společný
<g/>
:	:	kIx,	:
a	a	k8xC	a
<g/>
)	)	kIx)	)
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
precedenci	precedence	k1gFnSc4	precedence
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
šaríi	šaríi	k1gNnSc2	šaríi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
šaría	šaría	k6eAd1	šaría
uplatňována	uplatňovat	k5eAaImNgNnP	uplatňovat
především	především	k6eAd1	především
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
osobního	osobní	k2eAgInSc2d1	osobní
statusu	status	k1gInSc2	status
jako	jako	k8xS	jako
manželství	manželství	k1gNnSc2	manželství
<g/>
,	,	kIx,	,
rozvod	rozvod	k1gInSc1	rozvod
<g/>
,	,	kIx,	,
dědictví	dědictví	k1gNnSc1	dědictví
atd.	atd.	kA	atd.
<g/>
,	,	kIx,	,
b	b	k?	b
<g/>
)	)	kIx)	)
třístupňový	třístupňový	k2eAgInSc1d1	třístupňový
soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
zahrnující	zahrnující	k2eAgInSc1d1	zahrnující
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
odvolací	odvolací	k2eAgInPc4d1	odvolací
soudy	soud	k1gInPc4	soud
a	a	k8xC	a
soudy	soud	k1gInPc4	soud
první	první	k4xOgFnSc2	první
instance	instance	k1gFnSc2	instance
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
)	)	kIx)	)
civilní	civilní	k2eAgInPc4d1	civilní
zákony	zákon	k1gInPc4	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
před	před	k7c7	před
pučem	puč	k1gInSc7	puč
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
Barre	Barr	k1gMnSc5	Barr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzestup	vzestup	k1gInSc1	vzestup
a	a	k8xC	a
pád	pád	k1gInSc1	pád
ICU	ICU	kA	ICU
<g/>
,	,	kIx,	,
etiopská	etiopský	k2eAgFnSc1d1	etiopská
intervence	intervence	k1gFnSc1	intervence
a	a	k8xC	a
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
nové	nový	k2eAgInPc1d1	nový
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
Somálská	somálský	k2eAgFnSc1d1	Somálská
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
národní	národní	k2eAgFnSc1d1	národní
vláda	vláda	k1gFnSc1	vláda
(	(	kIx(	(
<g/>
Transitional	Transitional	k1gMnSc1	Transitional
National	National	k1gMnSc1	National
Government	Government	k1gMnSc1	Government
<g/>
,	,	kIx,	,
TNG	TNG	kA	TNG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jejím	její	k3xOp3gMnSc7	její
následovníkem	následovník	k1gMnSc7	následovník
stala	stát	k5eAaPmAgFnS	stát
Somálská	somálský	k2eAgFnSc1d1	Somálská
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
(	(	kIx(	(
<g/>
Transitional	Transitional	k1gMnSc1	Transitional
Federal	Federal	k1gMnSc1	Federal
Government	Government	k1gMnSc1	Government
<g/>
,	,	kIx,	,
TFG	TFG	kA	TFG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
opozice	opozice	k1gFnSc2	opozice
se	se	k3xPyFc4	se
zformoval	zformovat	k5eAaPmAgInS	zformovat
Svaz	svaz	k1gInSc1	svaz
islámských	islámský	k2eAgInPc2d1	islámský
soudů	soud	k1gInPc2	soud
(	(	kIx(	(
<g/>
Islamic	Islamice	k1gFnPc2	Islamice
Court	Courta	k1gFnPc2	Courta
Union	union	k1gInSc1	union
<g/>
,	,	kIx,	,
ICU	ICU	kA	ICU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
organizace	organizace	k1gFnSc1	organizace
položila	položit	k5eAaPmAgFnS	položit
své	svůj	k3xOyFgInPc4	svůj
základy	základ	k1gInPc4	základ
již	již	k6eAd1	již
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
spojilo	spojit	k5eAaPmAgNnS	spojit
několik	několik	k4yIc1	několik
místních	místní	k2eAgInPc2d1	místní
soudů	soud	k1gInPc2	soud
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
uplatňování	uplatňování	k1gNnSc2	uplatňování
společné	společný	k2eAgFnSc2d1	společná
autority	autorita	k1gFnSc2	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
Svazu	svaz	k1gInSc2	svaz
postupně	postupně	k6eAd1	postupně
rostl	růst	k5eAaImAgInS	růst
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
opozicí	opozice	k1gFnSc7	opozice
vůči	vůči	k7c3	vůči
místním	místní	k2eAgInPc3d1	místní
warlordům	warlord	k1gInPc3	warlord
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
spojili	spojit	k5eAaPmAgMnP	spojit
do	do	k7c2	do
Spojenectví	spojenectví	k1gNnSc2	spojenectví
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
(	(	kIx(	(
<g/>
Alliance	Alliance	k1gFnSc1	Alliance
for	forum	k1gNnPc2	forum
the	the	k?	the
Restoration	Restoration	k1gInSc1	Restoration
of	of	k?	of
Peace	Peace	k1gFnSc2	Peace
and	and	k?	and
Counter-Terrorism	Counter-Terrorism	k1gMnSc1	Counter-Terrorism
<g/>
,	,	kIx,	,
ARPCT	ARPCT	kA	ARPCT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
oběma	dva	k4xCgNnPc7	dva
uskupeními	uskupení	k1gNnPc7	uskupení
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Svaz	svaz	k1gInSc1	svaz
islámských	islámský	k2eAgInPc2d1	islámský
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
těžící	těžící	k2eAgInSc4d1	těžící
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
podpory	podpora	k1gFnSc2	podpora
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
i	i	k8xC	i
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
islamistů	islamista	k1gMnPc2	islamista
brzy	brzy	k6eAd1	brzy
získal	získat	k5eAaPmAgMnS	získat
převahu	převaha	k1gFnSc4	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Ovládl	ovládnout	k5eAaPmAgMnS	ovládnout
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
a	a	k8xC	a
postupoval	postupovat	k5eAaImAgMnS	postupovat
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
venkovních	venkovní	k2eAgFnPc2d1	venkovní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
většinu	většina	k1gFnSc4	většina
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dobytých	dobytý	k2eAgNnPc6d1	dobyté
územích	území	k1gNnPc6	území
Svaz	svaz	k1gInSc4	svaz
záváděl	závádět	k5eAaImAgMnS	závádět
důsledné	důsledný	k2eAgNnSc1d1	důsledné
uplatňování	uplatňování	k1gNnSc1	uplatňování
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
šaría	šarí	k1gInSc2	šarí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
radikálně	radikálně	k6eAd1	radikálně
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
TFG	TFG	kA	TFG
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
intervenci	intervence	k1gFnSc3	intervence
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
bojů	boj	k1gInPc2	boj
se	se	k3xPyFc4	se
zapojila	zapojit	k5eAaPmAgFnS	zapojit
etiopská	etiopský	k2eAgFnSc1d1	etiopská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
mírové	mírový	k2eAgFnPc1d1	mírová
jednotky	jednotka	k1gFnPc1	jednotka
Africké	africký	k2eAgFnSc2d1	africká
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
podporu	podpora	k1gFnSc4	podpora
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Svaz	svaz	k1gInSc1	svaz
islámských	islámský	k2eAgInPc2d1	islámský
soudů	soud	k1gInPc2	soud
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
v	v	k7c6	v
defenzívě	defenzíva	k1gFnSc6	defenzíva
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c6	o
Ras	ras	k1gMnSc1	ras
Kamboni	Kambon	k1gMnPc1	Kambon
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
zakladatel	zakladatel	k1gMnSc1	zakladatel
TFG	TFG	kA	TFG
Abdullahi	Abdullah	k1gFnPc1	Abdullah
Yusuf	Yusuf	k1gMnSc1	Yusuf
Ahmed	Ahmed	k1gMnSc1	Ahmed
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
plukovník	plukovník	k1gMnSc1	plukovník
somálské	somálský	k2eAgFnSc2d1	Somálská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
vyznamenaný	vyznamenaný	k2eAgMnSc1d1	vyznamenaný
válečný	válečný	k2eAgMnSc1d1	válečný
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
zvolení	zvolení	k1gNnSc2	zvolení
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Mogadiša	Mogadišo	k1gNnSc2	Mogadišo
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
tak	tak	k9	tak
ze	z	k7c2	z
svého	svůj	k3xOyFgNnSc2	svůj
přechodného	přechodný	k2eAgNnSc2d1	přechodné
sídla	sídlo	k1gNnSc2	sídlo
v	v	k7c4	v
Baidoa	Baidous	k1gMnSc4	Baidous
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
Villa	Vill	k1gMnSc2	Vill
Somalia	Somalius	k1gMnSc2	Somalius
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
byla	být	k5eAaImAgFnS	být
významným	významný	k2eAgInSc7d1	významný
mezníkem	mezník	k1gInSc7	mezník
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
měla	mít	k5eAaImAgFnS	mít
centrální	centrální	k2eAgFnSc1d1	centrální
vláda	vláda	k1gFnSc1	vláda
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
většinu	většina	k1gFnSc4	většina
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
<g/>
ICU	ICU	kA	ICU
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
rozpadl	rozpadnout	k5eAaPmAgInS	rozpadnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
několik	několik	k4yIc4	několik
menších	malý	k2eAgNnPc2d2	menší
militantních	militantní	k2eAgNnPc2d1	militantní
uskupení	uskupení	k1gNnPc2	uskupení
<g/>
,	,	kIx,	,
především	především	k9	především
Aš-Šabáb	Aš-Šabáb	k1gMnSc1	Aš-Šabáb
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
uskupení	uskupení	k1gNnPc1	uskupení
pokračovala	pokračovat	k5eAaImAgNnP	pokračovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
TFG	TFG	kA	TFG
a	a	k8xC	a
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
Aš-Šabáb	Aš-Šabáb	k1gMnSc1	Aš-Šabáb
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
několik	několik	k4yIc4	několik
významných	významný	k2eAgNnPc2d1	významné
vítězství	vítězství	k1gNnPc2	vítězství
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
některými	některý	k3yIgNnPc7	některý
městy	město	k1gNnPc7	město
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
a	a	k8xC	a
centrálním	centrální	k2eAgNnSc6d1	centrální
Somálsku	Somálsko	k1gNnSc6	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
dobyl	dobýt	k5eAaPmAgInS	dobýt
Aš-Šabáb	Aš-Šabáb	k1gInSc1	Aš-Šabáb
dřívější	dřívější	k2eAgFnSc4d1	dřívější
provizorní	provizorní	k2eAgNnSc1d1	provizorní
sídlo	sídlo	k1gNnSc1	sídlo
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
Baidou	Baida	k1gMnSc7	Baida
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
se	se	k3xPyFc4	se
etiopská	etiopský	k2eAgFnSc1d1	etiopská
armáda	armáda	k1gFnSc1	armáda
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
Aš-Šabábu	Aš-Šabáb	k1gInSc2	Aš-Šabáb
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
milic	milice	k1gFnPc2	milice
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
a	a	k8xC	a
na	na	k7c6	na
podporu	podpor	k1gInSc6	podpor
TSG	TSG	kA	TSG
zůstaly	zůstat	k5eAaPmAgInP	zůstat
pouze	pouze	k6eAd1	pouze
nedostatečně	dostatečně	k6eNd1	dostatečně
vyzbrojené	vyzbrojený	k2eAgFnSc2d1	vyzbrojená
mírové	mírový	k2eAgFnSc2d1	mírová
jednotky	jednotka	k1gFnSc2	jednotka
Africké	africký	k2eAgFnSc2d1	africká
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
<g/>
Kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
finančních	finanční	k2eAgInPc2d1	finanční
a	a	k8xC	a
lidských	lidský	k2eAgInPc2d1	lidský
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
embargu	embargo	k1gNnSc6	embargo
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
netečnosti	netečnost	k1gFnSc2	netečnost
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
komunity	komunita	k1gFnSc2	komunita
byl	být	k5eAaImAgMnS	být
prezident	prezident	k1gMnSc1	prezident
Yusuf	Yusuf	k1gMnSc1	Yusuf
Ahmed	Ahmed	k1gMnSc1	Ahmed
nucen	nucen	k2eAgMnSc1d1	nucen
požádat	požádat	k5eAaPmF	požádat
Puntland	Puntland	k1gInSc4	Puntland
o	o	k7c6	o
vyslání	vyslání	k1gNnSc6	vyslání
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInSc4	tisíc
vojáků	voják	k1gMnPc2	voják
do	do	k7c2	do
Mogadišu	Mogadišo	k1gNnSc3	Mogadišo
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
autonomního	autonomní	k2eAgInSc2d1	autonomní
regionu	region	k1gInSc2	region
prezidentovi	prezident	k1gMnSc3	prezident
vyhověla	vyhovět	k5eAaPmAgFnS	vyhovět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
úbytek	úbytek	k1gInSc1	úbytek
takového	takový	k3xDgNnSc2	takový
množství	množství	k1gNnSc2	množství
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
sil	síla	k1gFnPc2	síla
zanechal	zanechat	k5eAaPmAgMnS	zanechat
oblast	oblast	k1gFnSc4	oblast
zranitelnou	zranitelný	k2eAgFnSc4d1	zranitelná
vůči	vůči	k7c3	vůči
pirátství	pirátství	k1gNnSc3	pirátství
a	a	k8xC	a
teroristickým	teroristický	k2eAgInPc3d1	teroristický
útokům	útok	k1gInPc3	útok
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
prezident	prezident	k1gMnSc1	prezident
Abdullahi	Abdullahi	k1gNnSc2	Abdullahi
Yusuf	Yusuf	k1gMnSc1	Yusuf
Ahmed	Ahmed	k1gMnSc1	Ahmed
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
řeči	řeč	k1gFnSc6	řeč
<g/>
,	,	kIx,	,
přenášené	přenášený	k2eAgNnSc1d1	přenášené
státním	státní	k2eAgInSc7d1	státní
rozhlasem	rozhlas	k1gInSc7	rozhlas
<g/>
,	,	kIx,	,
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
lítost	lítost	k1gFnSc4	lítost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
ukončit	ukončit	k5eAaPmF	ukončit
sedmnáctiletý	sedmnáctiletý	k2eAgInSc4d1	sedmnáctiletý
válečný	válečný	k2eAgInSc4d1	válečný
konflikt	konflikt	k1gInSc4	konflikt
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
zavázala	zavázat	k5eAaPmAgFnS	zavázat
<g/>
.	.	kIx.	.
</s>
<s>
Obvinil	obvinit	k5eAaPmAgMnS	obvinit
také	také	k9	také
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
společenství	společenství	k1gNnSc4	společenství
z	z	k7c2	z
nezájmu	nezájem	k1gInSc2	nezájem
a	a	k8xC	a
nedostatečné	dostatečný	k2eNgFnSc2d1	nedostatečná
podpory	podpora	k1gFnSc2	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
se	se	k3xPyFc4	se
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
prozatímní	prozatímní	k2eAgFnSc7d1	prozatímní
ústavou	ústava	k1gFnSc7	ústava
stane	stanout	k5eAaPmIp3nS	stanout
mluvčí	mluvčí	k1gMnSc1	mluvčí
parlamentu	parlament	k1gInSc2	parlament
Jama	Jama	k1gMnSc1	Jama
Ali	Ali	k1gMnSc1	Ali
Jama	Jama	k1gMnSc1	Jama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Koaliční	koaliční	k2eAgFnSc1d1	koaliční
vláda	vláda	k1gFnSc1	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
31	[number]	k4	31
<g/>
.	.	kIx.	.
květnem	květen	k1gInSc7	květen
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
červnem	červen	k1gInSc7	červen
2008	[number]	k4	2008
probíhala	probíhat	k5eAaImAgFnS	probíhat
na	na	k7c6	na
neutrální	neutrální	k2eAgFnSc6d1	neutrální
půdě	půda	k1gFnSc6	půda
v	v	k7c6	v
Džíbútí	Džíbútí	k1gNnSc6	Džíbútí
jednání	jednání	k1gNnSc2	jednání
mezi	mezi	k7c7	mezi
zástupci	zástupce	k1gMnPc7	zástupce
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
umírněnými	umírněný	k2eAgMnPc7d1	umírněný
islamisty	islamista	k1gMnPc7	islamista
z	z	k7c2	z
Aliance	aliance	k1gFnSc2	aliance
pro	pro	k7c4	pro
znovu-osvobození	znovusvobození	k1gNnSc4	znovu-osvobození
Somálska	Somálsko	k1gNnSc2	Somálsko
(	(	kIx(	(
<g/>
Alliance	Alliance	k1gFnSc1	Alliance
for	forum	k1gNnPc2	forum
the	the	k?	the
Re-liberation	Reiberation	k1gInSc1	Re-liberation
of	of	k?	of
Somalia	Somalius	k1gMnSc2	Somalius
<g/>
,	,	kIx,	,
ARS	ARS	kA	ARS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zprostředkovatelem	zprostředkovatel	k1gMnSc7	zprostředkovatel
byl	být	k5eAaImAgMnS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
vyslanec	vyslanec	k1gMnSc1	vyslanec
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
Somálsko	Somálsko	k1gNnSc4	Somálsko
Ahmedou	Ahmeda	k1gMnSc7	Ahmeda
Ould-Abdallah	Ould-Abdallaha	k1gFnPc2	Ould-Abdallaha
<g/>
.	.	kIx.	.
</s>
<s>
Konference	konference	k1gFnSc1	konference
skončila	skončit	k5eAaPmAgFnS	skončit
dohodou	dohoda	k1gFnSc7	dohoda
o	o	k7c4	o
stažení	stažení	k1gNnSc4	stažení
etiopských	etiopský	k2eAgFnPc2d1	etiopská
sil	síla	k1gFnPc2	síla
ze	z	k7c2	z
Somálska	Somálsko	k1gNnSc2	Somálsko
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
ukončení	ukončení	k1gNnSc4	ukončení
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
mezi	mezi	k7c7	mezi
ARS	ARS	kA	ARS
a	a	k8xC	a
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
následně	následně	k6eAd1	následně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
počet	počet	k1gInSc1	počet
míst	místo	k1gNnPc2	místo
na	na	k7c4	na
550	[number]	k4	550
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
přijal	přijmout	k5eAaPmAgMnS	přijmout
zástupce	zástupce	k1gMnSc1	zástupce
ARS	ARS	kA	ARS
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
bývalý	bývalý	k2eAgMnSc1d1	bývalý
vůdce	vůdce	k1gMnSc1	vůdce
ARS	ARS	kA	ARS
<g/>
,	,	kIx,	,
Sheikh	Sheikh	k1gMnSc1	Sheikh
Sharif	Sharif	k1gMnSc1	Sharif
Sheikh	Sheikh	k1gMnSc1	Sheikh
Ahmed	Ahmed	k1gMnSc1	Ahmed
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
následně	následně	k6eAd1	následně
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
premiérem	premiér	k1gMnSc7	premiér
Omara	Omar	k1gInSc2	Omar
Abdirashida	Abdirashid	k1gMnSc2	Abdirashid
Aliho	Ali	k1gMnSc2	Ali
Sharmarkeho	Sharmarke	k1gMnSc2	Sharmarke
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
bývalého	bývalý	k2eAgMnSc2d1	bývalý
prezidenta	prezident	k1gMnSc2	prezident
Abdirashida	Abdirashid	k1gMnSc2	Abdirashid
Aliho	Ali	k1gMnSc2	Ali
Sharmarkeho	Sharmarke	k1gMnSc2	Sharmarke
<g/>
,	,	kIx,	,
zavražděného	zavražděný	k2eAgMnSc2d1	zavražděný
při	při	k7c6	při
Barreho	Barre	k1gMnSc2	Barre
puči	puč	k1gInSc6	puč
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
jednotek	jednotka	k1gFnPc2	jednotka
Africké	africký	k2eAgFnSc2d1	africká
unie	unie	k1gFnSc2	unie
zahájila	zahájit	k5eAaPmAgFnS	zahájit
koaliční	koaliční	k2eAgFnSc1d1	koaliční
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
protiofenzívu	protiofenzíva	k1gFnSc4	protiofenzíva
vůči	vůči	k7c3	vůči
islamistickým	islamistický	k2eAgMnPc3d1	islamistický
rebelům	rebel	k1gMnPc3	rebel
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
upevnila	upevnit	k5eAaPmAgFnS	upevnit
svojí	svojit	k5eAaImIp3nP	svojit
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
alianci	aliance	k1gFnSc4	aliance
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
bývalého	bývalý	k2eAgInSc2d1	bývalý
ICU	ICU	kA	ICU
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
členy	člen	k1gInPc1	člen
ARS	ARS	kA	ARS
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ahlu	Ahla	k1gFnSc4	Ahla
Sunna	sunna	k1gFnSc1	sunna
Waljama	Waljama	k1gFnSc1	Waljama
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
umírněnými	umírněný	k2eAgFnPc7d1	umírněná
súfijskými	súfijský	k2eAgFnPc7d1	súfijská
milicemi	milice	k1gFnPc7	milice
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
příměří	příměří	k1gNnSc2	příměří
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
koaliční	koaliční	k2eAgFnSc1d1	koaliční
vláda	vláda	k1gFnSc1	vláda
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ochotná	ochotný	k2eAgFnSc1d1	ochotná
ustoupit	ustoupit	k5eAaPmF	ustoupit
rebelům	rebel	k1gMnPc3	rebel
a	a	k8xC	a
implementovat	implementovat	k5eAaImF	implementovat
islámské	islámský	k2eAgNnSc4d1	islámské
právo	právo	k1gNnSc4	právo
šaría	šarí	k1gInSc2	šarí
do	do	k7c2	do
státního	státní	k2eAgInSc2d1	státní
soudního	soudní	k2eAgInSc2d1	soudní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
však	však	k9	však
nadále	nadále	k6eAd1	nadále
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
postupně	postupně	k6eAd1	postupně
ztrácela	ztrácet	k5eAaImAgFnS	ztrácet
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
situací	situace	k1gFnSc7	situace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
abdikace	abdikace	k1gFnSc2	abdikace
prezidenta	prezident	k1gMnSc2	prezident
Yusufa	Yusuf	k1gMnSc2	Yusuf
Ahmada	Ahmada	k1gFnSc1	Ahmada
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2008	[number]	k4	2008
centrální	centrální	k2eAgFnSc1d1	centrální
vláda	vláda	k1gFnSc1	vláda
kontrolovala	kontrolovat	k5eAaImAgFnS	kontrolovat
až	až	k9	až
70	[number]	k4	70
%	%	kIx~	%
území	území	k1gNnSc6	území
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
však	však	k9	však
tento	tento	k3xDgInSc1	tento
podíl	podíl	k1gInSc1	podíl
klesl	klesnout	k5eAaPmAgInS	klesnout
až	až	k9	až
ke	k	k7c3	k
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nová	nový	k2eAgFnSc1d1	nová
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
krachu	krach	k1gInSc6	krach
příměří	příměří	k1gNnSc2	příměří
z	z	k7c2	z
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
islámské	islámský	k2eAgFnPc1d1	islámská
milice	milice	k1gFnPc1	milice
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
proti	proti	k7c3	proti
vládním	vládní	k2eAgFnPc3d1	vládní
jednotkám	jednotka	k1gFnPc3	jednotka
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Aš-Šabáb	Aš-Šabáb	k1gMnSc1	Aš-Šabáb
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
islamističtí	islamistický	k2eAgMnPc1d1	islamistický
spojenci	spojenec	k1gMnPc1	spojenec
spojenci	spojenec	k1gMnSc3	spojenec
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
pozicím	pozice	k1gFnPc3	pozice
vládních	vládní	k2eAgFnPc2d1	vládní
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hlavní	hlavní	k2eAgInPc4d1	hlavní
boje	boj	k1gInPc4	boj
utichly	utichnout	k5eAaPmAgFnP	utichnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rebelům	rebel	k1gMnPc3	rebel
podařilo	podařit	k5eAaPmAgNnS	podařit
ovládnout	ovládnout	k5eAaPmF	ovládnout
většinu	většina	k1gFnSc4	většina
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
svrhnout	svrhnout	k5eAaPmF	svrhnout
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
menšími	malý	k2eAgFnPc7d2	menší
přestávkami	přestávka	k1gFnPc7	přestávka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
přestřelkám	přestřelka	k1gFnPc3	přestřelka
až	až	k8xS	až
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Aš-Šabáb	Aš-Šabáb	k1gInSc1	Aš-Šabáb
dobyl	dobýt	k5eAaPmAgInS	dobýt
strategické	strategický	k2eAgNnSc4d1	strategické
město	město	k1gNnSc4	město
Jowhar	Jowhara	k1gFnPc2	Jowhara
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
přerušil	přerušit	k5eAaPmAgMnS	přerušit
koridor	koridor	k1gInSc4	koridor
spojující	spojující	k2eAgMnSc1d1	spojující
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
centrálního	centrální	k2eAgNnSc2d1	centrální
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
</s>
</p>
<p>
<s>
dobyl	dobýt	k5eAaPmAgInS	dobýt
Aš-Šabáb	Aš-Šabáb	k1gInSc1	Aš-Šabáb
po	po	k7c6	po
krvavé	krvavý	k2eAgFnSc6d1	krvavá
bitvě	bitva	k1gFnSc6	bitva
Wabho	Wab	k1gMnSc4	Wab
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
mluvčí	mluvčí	k1gMnSc1	mluvčí
prozatímního	prozatímní	k2eAgInSc2d1	prozatímní
parlamentu	parlament	k1gInSc2	parlament
Adan	Adan	k1gMnSc1	Adan
Mohamed	Mohamed	k1gMnSc1	Mohamed
Nuur	Nuur	k1gMnSc1	Nuur
Madobe	Madob	k1gInSc5	Madob
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vládní	vládní	k2eAgNnPc1d1	vládní
vojska	vojsko	k1gNnPc1	vojsko
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
porážky	porážka	k1gFnSc2	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
stav	stav	k1gInSc4	stav
a	a	k8xC	a
požádala	požádat	k5eAaPmAgFnS	požádat
okolní	okolní	k2eAgInPc4d1	okolní
státy	stát	k1gInPc4	stát
(	(	kIx(	(
<g/>
Keňu	keňu	k1gNnSc2	keňu
<g/>
,	,	kIx,	,
Etiopii	Etiopie	k1gFnSc4	Etiopie
<g/>
,	,	kIx,	,
Džíbútí	Džíbútí	k1gNnSc4	Džíbútí
a	a	k8xC	a
Jemen	Jemen	k1gInSc4	Jemen
<g/>
)	)	kIx)	)
o	o	k7c4	o
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Aš-Šabáb	Aš-Šabáb	k1gMnSc1	Aš-Šabáb
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
jakýmkoli	jakýkoli	k3yIgFnPc3	jakýkoli
zahraničním	zahraniční	k2eAgFnPc3d1	zahraniční
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc4	útok
vůči	vůči	k7c3	vůči
keňským	keňský	k2eAgInPc3d1	keňský
cílům	cíl	k1gInPc3	cíl
pohrozila	pohrozit	k5eAaPmAgFnS	pohrozit
i	i	k9	i
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
června	červen	k1gInSc2	červen
navíc	navíc	k6eAd1	navíc
dezertovalo	dezertovat	k5eAaBmAgNnS	dezertovat
několik	několik	k4yIc1	několik
bývalých	bývalý	k2eAgMnPc2d1	bývalý
vojenských	vojenský	k2eAgMnPc2d1	vojenský
velitelů	velitel	k1gMnPc2	velitel
ICU	ICU	kA	ICU
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
z	z	k7c2	z
Džíbútí	Džíbútí	k1gNnSc2	Džíbútí
bojovali	bojovat	k5eAaImAgMnP	bojovat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
<g/>
.6	.6	k4	.6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
rebelové	rebel	k1gMnPc1	rebel
dali	dát	k5eAaPmAgMnP	dát
vládním	vládní	k2eAgMnPc3d1	vládní
jednotkám	jednotka	k1gFnPc3	jednotka
pětidenní	pětidenní	k2eAgFnSc2d1	pětidenní
ultimátum	ultimátum	k1gNnSc4	ultimátum
na	na	k7c4	na
složení	složení	k1gNnSc4	složení
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Ultimátum	ultimátum	k1gNnSc1	ultimátum
bylo	být	k5eAaImAgNnS	být
odmítnuto	odmítnout	k5eAaPmNgNnS	odmítnout
<g/>
.17	.17	k4	.17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
únosu	únos	k1gInSc3	únos
dvou	dva	k4xCgMnPc2	dva
francouzských	francouzský	k2eAgMnPc2d1	francouzský
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dělali	dělat	k5eAaImAgMnP	dělat
poradce	poradce	k1gMnSc1	poradce
centrální	centrální	k2eAgFnSc3d1	centrální
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
v	v	k7c4	v
odpověď	odpověď	k1gFnSc4	odpověď
vyslala	vyslat	k5eAaPmAgFnS	vyslat
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
Somálska	Somálsko	k1gNnSc2	Somálsko
svoje	svůj	k3xOyFgFnPc4	svůj
armádní	armádní	k2eAgFnPc4d1	armádní
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
rukojmích	rukojmí	k1gMnPc2	rukojmí
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
<g/>
,	,	kIx,	,
osud	osud	k1gInSc1	osud
druhého	druhý	k4xOgMnSc4	druhý
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
významnému	významný	k2eAgInSc3d1	významný
zvratu	zvrat	k1gInSc3	zvrat
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
hlavními	hlavní	k2eAgFnPc7d1	hlavní
rebelskými	rebelský	k2eAgFnPc7d1	rebelská
frakcemi	frakce	k1gFnPc7	frakce
Aš-Šabábem	Aš-Šabáb	k1gInSc7	Aš-Šabáb
a	a	k8xC	a
Hizbul	Hizbul	k1gInSc1	Hizbul
Islam	Islam	k1gInSc1	Islam
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
byly	být	k5eAaImAgFnP	být
spory	spor	k1gInPc4	spor
o	o	k7c4	o
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
strategickým	strategický	k2eAgInSc7d1	strategický
přístavem	přístav	k1gInSc7	přístav
Kismaayo	Kismaayo	k6eAd1	Kismaayo
<g/>
.	.	kIx.	.
</s>
<s>
Těžké	těžký	k2eAgInPc1d1	těžký
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
frakcemi	frakce	k1gFnPc7	frakce
trvaly	trvat	k5eAaImAgFnP	trvat
celý	celý	k2eAgInSc4d1	celý
říjen	říjen	k1gInSc4	říjen
a	a	k8xC	a
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
frakce	frakce	k1gFnPc1	frakce
byly	být	k5eAaImAgFnP	být
nuceny	nucen	k2eAgFnPc1d1	nucena
stáhnout	stáhnout	k5eAaPmF	stáhnout
část	část	k1gFnSc4	část
svých	svůj	k3xOyFgFnPc2	svůj
sil	síla	k1gFnPc2	síla
z	z	k7c2	z
Mogadiša	Mogadišo	k1gNnSc2	Mogadišo
a	a	k8xC	a
situace	situace	k1gFnSc2	situace
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
zklidnila	zklidnit	k5eAaPmAgFnS	zklidnit
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
intenzitou	intenzita	k1gFnSc7	intenzita
i	i	k8xC	i
celý	celý	k2eAgInSc1d1	celý
následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
<s>
Aš-Šabáb	Aš-Šabáb	k1gMnSc1	Aš-Šabáb
postupně	postupně	k6eAd1	postupně
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
protivníka	protivník	k1gMnSc2	protivník
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
bašt	bašta	k1gFnPc2	bašta
a	a	k8xC	a
zbylé	zbylý	k2eAgMnPc4d1	zbylý
bojovníky	bojovník	k1gMnPc4	bojovník
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
posílili	posílit	k5eAaPmAgMnP	posílit
jeho	jeho	k3xOp3gFnPc4	jeho
řady	řada	k1gFnPc4	řada
<g/>
.	.	kIx.	.
</s>
<s>
Splynutí	splynutí	k1gNnSc1	splynutí
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
poslední	poslední	k2eAgMnPc1d1	poslední
vůdci	vůdce	k1gMnPc1	vůdce
Hizbul	Hizbula	k1gFnPc2	Hizbula
Islam	Islam	k1gInSc1	Islam
vzdali	vzdát	k5eAaPmAgMnP	vzdát
jednotkám	jednotka	k1gFnPc3	jednotka
Aš-Šabábu	Aš-Šabáb	k1gInSc2	Aš-Šabáb
a	a	k8xC	a
oznámili	oznámit	k5eAaPmAgMnP	oznámit
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
<g/>
Centrální	centrální	k2eAgFnSc1d1	centrální
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
spojenci	spojenec	k1gMnPc1	spojenec
využili	využít	k5eAaPmAgMnP	využít
mezidobí	mezidobí	k1gNnSc4	mezidobí
k	k	k7c3	k
nabírání	nabírání	k1gNnSc3	nabírání
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
nově	nově	k6eAd1	nově
zvolená	zvolený	k2eAgFnSc1d1	zvolená
technokratická	technokratický	k2eAgFnSc1d1	technokratická
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ihned	ihned	k6eAd1	ihned
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
reformami	reforma	k1gFnPc7	reforma
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejdůležitějším	důležitý	k2eAgInPc3d3	nejdůležitější
patřilo	patřit	k5eAaImAgNnS	patřit
zavedení	zavedení	k1gNnSc4	zavedení
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
měsíčních	měsíční	k2eAgFnPc2d1	měsíční
výplat	výplata	k1gFnPc2	výplata
služného	služné	k1gNnSc2	služné
vládním	vládní	k2eAgMnPc3d1	vládní
vojákům	voják	k1gMnPc3	voják
a	a	k8xC	a
zavedení	zavedení	k1gNnSc2	zavedení
biometrického	biometrický	k2eAgInSc2d1	biometrický
registru	registr	k1gInSc2	registr
pro	pro	k7c4	pro
bezpečnostní	bezpečnostní	k2eAgFnPc4d1	bezpečnostní
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
převzaly	převzít	k5eAaPmAgFnP	převzít
vládní	vládní	k2eAgFnPc1d1	vládní
jednotky	jednotka	k1gFnPc1	jednotka
iniciativu	iniciativa	k1gFnSc4	iniciativa
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
afrických	africký	k2eAgFnPc2d1	africká
mírových	mírový	k2eAgFnPc2d1	mírová
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mise	mise	k1gFnSc2	mise
AMISOM	AMISOM	kA	AMISOM
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
9000	[number]	k4	9000
<g/>
.	.	kIx.	.
</s>
<s>
Islamistické	islamistický	k2eAgFnPc1d1	islamistická
milice	milice	k1gFnPc1	milice
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
vytlačovány	vytlačovat	k5eAaImNgFnP	vytlačovat
z	z	k7c2	z
Mogadišu	Mogadišo	k1gNnSc3	Mogadišo
<g/>
,	,	kIx,	,
vládní	vládní	k2eAgFnPc1d1	vládní
jednotky	jednotka	k1gFnPc1	jednotka
postupovaly	postupovat	k5eAaImAgFnP	postupovat
i	i	k9	i
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
bylo	být	k5eAaImAgNnS	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
osvobozování	osvobozování	k1gNnSc1	osvobozování
Mogadiša	Mogadišo	k1gNnSc2	Mogadišo
<g/>
,	,	kIx,	,
svědci	svědek	k1gMnPc1	svědek
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
stahování	stahování	k1gNnSc4	stahování
rebelských	rebelský	k2eAgFnPc2d1	rebelská
jednotek	jednotka	k1gFnPc2	jednotka
z	z	k7c2	z
města	město	k1gNnSc2	město
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Baidoe	Baidoe	k1gNnSc3	Baidoe
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
keňské	keňský	k2eAgNnSc1d1	keňské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
odřízlo	odříznout	k5eAaPmAgNnS	odříznout
Kismaayo	Kismaayo	k6eAd1	Kismaayo
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
zahájily	zahájit	k5eAaPmAgFnP	zahájit
vládní	vládní	k2eAgFnPc1d1	vládní
jednotky	jednotka	k1gFnPc1	jednotka
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
keňské	keňský	k2eAgFnSc2d1	keňská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
jednotek	jednotka	k1gFnPc2	jednotka
AMISOM	AMISOM	kA	AMISOM
pozemní	pozemní	k2eAgInSc4d1	pozemní
útok	útok	k1gInSc4	útok
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několikadenních	několikadenní	k2eAgInPc6d1	několikadenní
bojích	boj	k1gInPc6	boj
bylo	být	k5eAaImAgNnS	být
Kismaayo	Kismaayo	k6eAd1	Kismaayo
dobyto	dobýt	k5eAaPmNgNnS	dobýt
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
Aš-Šabáb	Aš-Šabáb	k1gInSc1	Aš-Šabáb
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
poslední	poslední	k2eAgNnSc4d1	poslední
větší	veliký	k2eAgNnSc4d2	veliký
město	město	k1gNnSc4	město
pod	pod	k7c7	pod
svou	svůj	k3xOyFgFnSc7	svůj
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2011	[number]	k4	2011
zahájily	zahájit	k5eAaPmAgInP	zahájit
somálská	somálský	k2eAgFnSc1d1	Somálská
a	a	k8xC	a
keňská	keňský	k2eAgFnSc1d1	keňská
armáda	armáda	k1gFnSc1	armáda
společnou	společný	k2eAgFnSc4d1	společná
operaci	operace	k1gFnSc4	operace
proti	proti	k7c3	proti
islamistickým	islamistický	k2eAgFnPc3d1	islamistická
milicím	milice	k1gFnPc3	milice
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
oblastech	oblast	k1gFnPc6	oblast
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Keňské	keňský	k2eAgFnPc1d1	keňská
jednotky	jednotka	k1gFnPc1	jednotka
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2012	[number]	k4	2012
formálně	formálně	k6eAd1	formálně
začleněny	začlenit	k5eAaPmNgFnP	začlenit
do	do	k7c2	do
AMISOM	AMISOM	kA	AMISOM
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
vládními	vládní	k2eAgFnPc7d1	vládní
jednotkami	jednotka	k1gFnPc7	jednotka
a	a	k8xC	a
islamistickými	islamistický	k2eAgMnPc7d1	islamistický
rebely	rebel	k1gMnPc7	rebel
stále	stále	k6eAd1	stále
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Islamisté	islamista	k1gMnPc1	islamista
stále	stále	k6eAd1	stále
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
venkovní	venkovní	k2eAgFnPc4d1	venkovní
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Aktuální	aktuální	k2eAgFnSc1d1	aktuální
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
na	na	k7c4	na
přiložené	přiložený	k2eAgNnSc4d1	přiložené
<g/>
,	,	kIx,	,
průběžně	průběžně	k6eAd1	průběžně
aktualizované	aktualizovaný	k2eAgFnSc6d1	aktualizovaná
mapce	mapka	k1gFnSc6	mapka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Somali	Somali	k1gFnSc2	Somali
Civil	civil	k1gMnSc1	civil
War	War	k1gMnSc1	War
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
War	War	k1gFnSc6	War
in	in	k?	in
Somalia	Somalia	k1gFnSc1	Somalia
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
present	present	k1gMnSc1	present
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Somálsko	Somálsko	k1gNnSc1	Somálsko
-	-	kIx~	-
začarovaný	začarovaný	k2eAgInSc1d1	začarovaný
kruh	kruh	k1gInSc1	kruh
hladomoru	hladomor	k1gInSc2	hladomor
a	a	k8xC	a
nekonečných	konečný	k2eNgInPc2d1	nekonečný
bojů	boj	k1gInPc2	boj
–	–	k?	–
reportáž	reportáž	k1gFnSc4	reportáž
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Šimon	Šimon	k1gMnSc1	Šimon
<g/>
:	:	kIx,	:
Vývoj	vývoj	k1gInSc1	vývoj
Somálska	Somálsko	k1gNnSc2	Somálsko
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Politický	politický	k2eAgInSc1d1	politický
vývoj	vývoj	k1gInSc1	vývoj
předcházející	předcházející	k2eAgInSc1d1	předcházející
rozpadu	rozpad	k1gInSc3	rozpad
Somálské	somálský	k2eAgFnSc2d1	Somálská
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Šimon	Šimon	k1gMnSc1	Šimon
<g/>
:	:	kIx,	:
Vývoj	vývoj	k1gInSc1	vývoj
Somálska	Somálsko	k1gNnSc2	Somálsko
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Post-intervenční	Postntervenční	k2eAgInSc1d1	Post-intervenční
vývoj	vývoj	k1gInSc1	vývoj
Somálska	Somálsko	k1gNnSc2	Somálsko
a	a	k8xC	a
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
státu	stát	k1gInSc2	stát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Somalia	Somalia	k1gFnSc1	Somalia
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Struggle	Struggle	k1gNnSc7	Struggle
for	forum	k1gNnPc2	forum
Stability	stabilita	k1gFnSc2	stabilita
(	(	kIx(	(
<g/>
Somálský	somálský	k2eAgInSc1d1	somálský
boj	boj	k1gInSc1	boj
za	za	k7c4	za
stabilitu	stabilita	k1gFnSc4	stabilita
<g/>
)	)	kIx)	)
z	z	k7c2	z
pořadu	pořad	k1gInSc2	pořad
The	The	k1gMnSc1	The
NewsHour	NewsHour	k1gMnSc1	NewsHour
with	with	k1gMnSc1	with
Jim	on	k3xPp3gMnPc3	on
Lehrer	Lehrer	k1gMnSc1	Lehrer
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Somalia	Somalia	k1gFnSc1	Somalia
–	–	k?	–
War	War	k1gFnSc1	War
situation	situation	k1gInSc1	situation
since	since	k1gFnSc1	since
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
Somálsko	Somálsko	k1gNnSc1	Somálsko
-	-	kIx~	-
válečná	válečný	k2eAgFnSc1d1	válečná
situace	situace	k1gFnSc1	situace
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
reportáž	reportáž	k1gFnSc1	reportáž
na	na	k7c6	na
zpravodajském	zpravodajský	k2eAgInSc6d1	zpravodajský
kanálu	kanál	k1gInSc6	kanál
France	Franc	k1gMnSc2	Franc
24	[number]	k4	24
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Somali	Somali	k1gFnSc1	Somali
–	–	k?	–
U.	U.	kA	U.
S.	S.	kA	S.
Relations	Relationsa	k1gFnPc2	Relationsa
(	(	kIx(	(
<g/>
Somálsko-americké	somálskomerický	k2eAgInPc1d1	somálsko-americký
vztahy	vztah	k1gInPc1	vztah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dean	Dean	k1gMnSc1	Dean
Peter	Peter	k1gMnSc1	Peter
Krogh	Krogh	k1gMnSc1	Krogh
Foreign	Foreign	k1gMnSc1	Foreign
Affairs	Affairsa	k1gFnPc2	Affairsa
Digital	Digital	kA	Digital
Archives	Archives	k1gMnSc1	Archives
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Somalia	Somalia	k1gFnSc1	Somalia
Operations	Operations	k1gInSc1	Operations
<g/>
:	:	kIx,	:
Lessons	Lessons	k1gInSc1	Lessons
Learned	Learned	k1gInSc1	Learned
(	(	kIx(	(
<g/>
Operace	operace	k1gFnSc1	operace
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
<g/>
:	:	kIx,	:
Co	co	k9	co
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
naučili	naučit	k5eAaPmAgMnP	naučit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kenneth	Kenneth	k1gMnSc1	Kenneth
Allard	Allard	k1gMnSc1	Allard
(	(	kIx(	(
<g/>
CCRP	CCRP	kA	CCRP
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Changed	Changed	k1gMnSc1	Changed
Arab	Arab	k1gMnSc1	Arab
attitudes	attitudes	k1gMnSc1	attitudes
to	ten	k3xDgNnSc1	ten
Somalia	Somalia	k1gFnSc1	Somalia
Conflict	Conflicta	k1gFnPc2	Conflicta
(	(	kIx(	(
<g/>
Změna	změna	k1gFnSc1	změna
arabských	arabský	k2eAgInPc2d1	arabský
postojů	postoj	k1gInPc2	postoj
ke	k	k7c3	k
konfliktu	konflikt	k1gInSc3	konflikt
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Leaked	Leaked	k1gInSc1	Leaked
cables	cables	k1gInSc1	cables
confirm	confirm	k1gInSc4	confirm
U.	U.	kA	U.
S.	S.	kA	S.
role	role	k1gFnSc1	role
in	in	k?	in
Somalia	Somalia	k1gFnSc1	Somalia
war	war	k?	war
(	(	kIx(	(
<g/>
Uniklé	uniklý	k2eAgFnPc1d1	uniklá
depeše	depeše	k1gFnPc1	depeše
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
roli	role	k1gFnSc4	role
USA	USA	kA	USA
v	v	k7c6	v
Somálské	somálský	k2eAgFnSc6d1	Somálská
válce	válka	k1gFnSc6	válka
<g/>
)	)	kIx)	)
</s>
</p>
