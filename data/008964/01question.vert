<s>
Proč	proč	k6eAd1	proč
po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Etiopií	Etiopie	k1gFnSc7	Etiopie
o	o	k7c4	o
Ogaden	Ogadna	k1gFnPc2	Ogadna
v	v	k7c6	v
letech	let	k1gInPc6	let
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
začala	začít	k5eAaPmAgFnS	začít
socialistická	socialistický	k2eAgFnSc1d1	socialistická
vláda	vláda	k1gFnSc1	vláda
Somálské	somálský	k2eAgFnSc2d1	Somálská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generálmajora	generálmajor	k1gMnSc2	generálmajor
Muhammada	Muhammada	k1gFnSc1	Muhammada
Siada	Siad	k1gMnSc2	Siad
Barreho	Barre	k1gMnSc2	Barre
zatýkat	zatýkat	k5eAaImF	zatýkat
vládní	vládní	k2eAgInPc4d1	vládní
a	a	k8xC	a
armádní	armádní	k2eAgMnPc4d1	armádní
úředníky	úředník	k1gMnPc4	úředník
<g/>
?	?	kIx.	?
</s>
