<s>
Po	po	k7c6	po
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Etiopií	Etiopie	k1gFnSc7	Etiopie
o	o	k7c4	o
Ogaden	Ogadna	k1gFnPc2	Ogadna
v	v	k7c6	v
letech	let	k1gInPc6	let
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
socialistická	socialistický	k2eAgFnSc1d1	socialistická
vláda	vláda	k1gFnSc1	vláda
Somálské	somálský	k2eAgFnSc2d1	Somálská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generálmajora	generálmajor	k1gMnSc2	generálmajor
Muhammada	Muhammada	k1gFnSc1	Muhammada
Siada	Siada	k1gFnSc1	Siada
Barreho	Barre	k1gMnSc2	Barre
začala	začít	k5eAaPmAgFnS	začít
zatýkat	zatýkat	k5eAaImF	zatýkat
vládní	vládní	k2eAgMnPc4d1	vládní
a	a	k8xC	a
armádní	armádní	k2eAgMnPc4d1	armádní
úředníky	úředník	k1gMnPc4	úředník
kvůli	kvůli	k7c3	kvůli
podezření	podezření	k1gNnSc3	podezření
z	z	k7c2	z
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
nezdařeném	zdařený	k2eNgInSc6d1	nezdařený
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
