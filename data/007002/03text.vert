<s>
Svědomí	svědomí	k1gNnSc1	svědomí
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Jiřího	Jiří	k1gMnSc2	Jiří
Krejčíka	Krejčík	k1gMnSc2	Krejčík
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
skutečně	skutečně	k6eAd1	skutečně
psychologických	psychologický	k2eAgInPc2d1	psychologický
filmů	film	k1gInPc2	film
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
kin	kino	k1gNnPc2	kino
přišel	přijít	k5eAaPmAgMnS	přijít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
únorového	únorový	k2eAgInSc2d1	únorový
puče	puč	k1gInSc2	puč
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
kritikou	kritika	k1gFnSc7	kritika
přehlížen	přehlížen	k2eAgMnSc1d1	přehlížen
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
napadán	napadán	k2eAgMnSc1d1	napadán
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zajímá	zajímat	k5eAaImIp3nS	zajímat
o	o	k7c4	o
"	"	kIx"	"
<g/>
citečky	citeček	k1gInPc4	citeček
<g/>
"	"	kIx"	"
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
blaho	blaho	k1gNnSc4	blaho
a	a	k8xC	a
životy	život	k1gInPc1	život
všech	všecek	k3xTgFnPc2	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
Miloš	Miloš	k1gMnSc1	Miloš
Nedbal	Nedbal	k1gMnSc1	Nedbal
<g/>
)	)	kIx)	)
usmrtí	usmrtit	k5eAaPmIp3nP	usmrtit
dítě	dítě	k1gNnSc4	dítě
při	při	k7c6	při
dopravní	dopravní	k2eAgFnSc6d1	dopravní
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
činu	čin	k1gInSc2	čin
ujede	ujet	k5eAaPmIp3nS	ujet
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zrovna	zrovna	k6eAd1	zrovna
veze	vézt	k5eAaImIp3nS	vézt
svou	svůj	k3xOyFgFnSc4	svůj
milenku	milenka	k1gFnSc4	milenka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
potom	potom	k6eAd1	potom
vydírá	vydírat	k5eAaImIp3nS	vydírat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
popisu	popis	k1gInSc2	popis
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
pozná	poznat	k5eAaPmIp3nS	poznat
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
že	že	k8xS	že
pachatelem	pachatel	k1gMnSc7	pachatel
je	být	k5eAaImIp3nS	být
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
společně	společně	k6eAd1	společně
s	s	k7c7	s
výčitkami	výčitka	k1gFnPc7	výčitka
svědomí	svědomí	k1gNnSc2	svědomí
přiměje	přimět	k5eAaPmIp3nS	přimět
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
.	.	kIx.	.
</s>
<s>
Námět	námět	k1gInSc1	námět
<g/>
:	:	kIx,	:
Vladimír	Vladimír	k1gMnSc1	Vladimír
Valenta	Valenta	k1gMnSc1	Valenta
Scénář	scénář	k1gInSc1	scénář
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Fried	Fried	k1gMnSc1	Fried
<g/>
,	,	kIx,	,
J.	J.	kA	J.
A.	A.	kA	A.
Novotný	Novotný	k1gMnSc1	Novotný
Hudba	hudba	k1gFnSc1	hudba
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Šust	šust	k1gInSc4	šust
Kamera	kamera	k1gFnSc1	kamera
<g/>
:	:	kIx,	:
Rudolf	Rudolf	k1gMnSc1	Rudolf
Stahl	Stahl	k1gMnSc1	Stahl
Režie	režie	k1gFnSc1	režie
<g/>
:	:	kIx,	:
Jiří	Jiří	k1gMnSc1	Jiří
Krejčík	Krejčík	k1gMnSc1	Krejčík
Hrají	hrát	k5eAaImIp3nP	hrát
<g/>
:	:	kIx,	:
Miloš	Miloš	k1gMnSc1	Miloš
Nedbal	Nedbal	k1gMnSc1	Nedbal
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Vášová	Vášová	k1gFnSc1	Vášová
<g/>
,	,	kIx,	,
Irena	Irena	k1gFnSc1	Irena
Kačírková	Kačírková	k1gFnSc1	Kačírková
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Prokeš	Prokeš	k1gMnSc1	Prokeš
<g/>
,	,	kIx,	,
Bohuš	Bohuš	k1gMnSc1	Bohuš
Záhorský	záhorský	k2eAgMnSc1d1	záhorský
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Dubský	Dubský	k1gMnSc1	Dubský
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kovářík	kovářík	k1gMnSc1	kovářík
Další	další	k2eAgMnSc1d1	další
údaje	údaj	k1gInSc2	údaj
<g/>
:	:	kIx,	:
černobílý	černobílý	k2eAgMnSc1d1	černobílý
<g/>
,	,	kIx,	,
101	[number]	k4	101
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
Výroba	výroba	k1gFnSc1	výroba
<g/>
:	:	kIx,	:
Československá	československý	k2eAgFnSc1d1	Československá
filmová	filmový	k2eAgFnSc1d1	filmová
společnost	společnost	k1gFnSc1	společnost
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
Svědomí	svědomí	k1gNnSc2	svědomí
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
