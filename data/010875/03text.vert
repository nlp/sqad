<p>
<s>
Smolikas	Smolikas	k1gInSc1	Smolikas
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Σ	Σ	k?	Σ
<g/>
,	,	kIx,	,
arumunsky	arumunsky	k6eAd1	arumunsky
Smolcu	Smolcus	k1gInSc3	Smolcus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výškou	výška	k1gFnSc7	výška
2637	[number]	k4	2637
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
pohoří	pohoří	k1gNnSc4	pohoří
Pindos	Pindosa	k1gFnPc2	Pindosa
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Konitsa	Konits	k1gMnSc2	Konits
v	v	k7c6	v
regionu	region	k1gInSc6	region
Epirus	Epirus	k1gInSc1	Epirus
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
stezkou	stezka	k1gFnSc7	stezka
vedoucí	vedoucí	k2eAgFnSc7d1	vedoucí
ze	z	k7c2	z
Samariny	Samarin	k1gInPc4	Samarin
<g/>
,	,	kIx,	,
nejvýše	nejvýše	k6eAd1	nejvýše
položené	položený	k2eAgFnSc2d1	položená
vesnice	vesnice	k1gFnSc2	vesnice
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Smolikas	Smolikas	k1gInSc1	Smolikas
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ultraprominentní	ultraprominentní	k2eAgInPc4d1	ultraprominentní
vrcholy	vrchol	k1gInPc4	vrchol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
ofiolitovým	ofiolitový	k2eAgInSc7d1	ofiolitový
komplexem	komplex	k1gInSc7	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
jehličnatým	jehličnatý	k2eAgInSc7d1	jehličnatý
lesem	les	k1gInSc7	les
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
končí	končit	k5eAaImIp3nS	končit
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
okolo	okolo	k7c2	okolo
dvou	dva	k4xCgInPc2	dva
tisíc	tisíc	k4xCgInPc2	tisíc
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
sněží	sněžit	k5eAaImIp3nS	sněžit
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
zde	zde	k6eAd1	zde
řeka	řeka	k1gFnSc1	řeka
Sarantaporos	Sarantaporosa	k1gFnPc2	Sarantaporosa
<g/>
,	,	kIx,	,
přítok	přítok	k1gInSc4	přítok
Vjosy	Vjosa	k1gFnSc2	Vjosa
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
vrcholu	vrchol	k1gInSc2	vrchol
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
horské	horský	k2eAgNnSc4d1	horské
jezírko	jezírko	k1gNnSc4	jezírko
Drakolimni	Drakolimni	k1gFnSc2	Drakolimni
(	(	kIx(	(
<g/>
Dračí	dračí	k2eAgNnSc1d1	dračí
jezero	jezero	k1gNnSc1	jezero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
o	o	k7c6	o
dracích	drak	k1gMnPc6	drak
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
žili	žít	k5eAaImAgMnP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc4	jezero
obývá	obývat	k5eAaImIp3nS	obývat
čolek	čolek	k1gMnSc1	čolek
horský	horský	k2eAgMnSc1d1	horský
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
dotčená	dotčený	k2eAgFnSc1d1	dotčená
příroda	příroda	k1gFnSc1	příroda
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hory	hora	k1gFnSc2	hora
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
život	život	k1gInSc4	život
vzácných	vzácný	k2eAgInPc2d1	vzácný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
orlosup	orlosup	k1gMnSc1	orlosup
bradatý	bradatý	k2eAgMnSc1d1	bradatý
<g/>
,	,	kIx,	,
medvěd	medvěd	k1gMnSc1	medvěd
hnědý	hnědý	k2eAgMnSc1d1	hnědý
nebo	nebo	k8xC	nebo
vlk	vlk	k1gMnSc1	vlk
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
würmského	würmský	k2eAgNnSc2d1	würmský
zalednění	zalednění	k1gNnSc2	zalednění
byly	být	k5eAaImAgFnP	být
vrcholové	vrcholový	k2eAgFnPc1d1	vrcholová
partie	partie	k1gFnPc1	partie
hory	hora	k1gFnSc2	hora
pokryty	pokrýt	k5eAaPmNgFnP	pokrýt
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Smolikas	Smolikasa	k1gFnPc2	Smolikasa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Mountains	Mountains	k1gInSc1	Mountains
Greece	Greece	k1gFnSc2	Greece
</s>
</p>
<p>
<s>
Summit	summit	k1gInSc1	summit
Post	posta	k1gFnPc2	posta
</s>
</p>
