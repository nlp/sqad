<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
י	י	k?	י
<g/>
ְ	ְ	k?	ְ
<g/>
ר	ר	k?	ר
<g/>
ּ	ּ	k?	ּ
<g/>
ש	ש	k?	ש
<g/>
ָ	ָ	k?	ָ
<g/>
ׁ	ׁ	k?	ׁ
<g/>
ל	ל	k?	ל
<g/>
ַ	ַ	k?	ַ
<g/>
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ם	ם	k?	ם
<g/>
,	,	kIx,	,
Jerušalajim	Jerušalajim	k1gInSc1	Jerušalajim
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
<g/>
ُ	ُ	k?	ُ
<g/>
د	د	k?	د
<g/>
,	,	kIx,	,
al-Kuds	al-Kuds	k1gInSc1	al-Kuds
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
do	do	k7c2	do
rozlohy	rozloha	k1gFnSc2	rozloha
a	a	k8xC	a
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInPc1d3	veliký
město	město	k1gNnSc4	město
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
území	území	k1gNnSc6	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
125,1	[number]	k4	125,1
km2	km2	k4	km2
žije	žít	k5eAaImIp3nS	žít
celkem	celkem	k6eAd1	celkem
865	[number]	k4	865
700	[number]	k4	700
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
