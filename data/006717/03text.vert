<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
י	י	k?	י
<g/>
ְ	ְ	k?	ְ
<g/>
ר	ר	k?	ר
<g/>
ּ	ּ	k?	ּ
<g/>
ש	ש	k?	ש
<g/>
ָ	ָ	k?	ָ
<g/>
ׁ	ׁ	k?	ׁ
<g/>
ל	ל	k?	ל
<g/>
ַ	ַ	k?	ַ
<g/>
י	י	k?	י
<g/>
ִ	ִ	k?	ִ
<g/>
ם	ם	k?	ם
<g/>
,	,	kIx,	,
Jerušalajim	Jerušalajim	k1gInSc1	Jerušalajim
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
<g/>
ُ	ُ	k?	ُ
<g/>
د	د	k?	د
<g/>
,	,	kIx,	,
al-Kuds	al-Kuds	k1gInSc1	al-Kuds
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
do	do	k7c2	do
rozlohy	rozloha	k1gFnSc2	rozloha
a	a	k8xC	a
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInPc1d3	veliký
město	město	k1gNnSc4	město
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
území	území	k1gNnSc6	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
125,1	[number]	k4	125,1
km2	km2	k4	km2
žije	žít	k5eAaImIp3nS	žít
celkem	celkem	k6eAd1	celkem
865	[number]	k4	865
700	[number]	k4	700
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
z	z	k7c2	z
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
Palestina	Palestina	k1gFnSc1	Palestina
považují	považovat	k5eAaImIp3nP	považovat
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
však	však	k9	však
jako	jako	k9	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Izraele	Izrael	k1gInSc2	Izrael
uznává	uznávat	k5eAaImIp3nS	uznávat
Tel	tel	kA	tel
Aviv-Jaffa	Aviv-Jaff	k1gMnSc2	Aviv-Jaff
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Judských	judský	k2eAgFnPc6d1	Judská
horách	hora	k1gFnPc6	hora
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
úmoří	úmoří	k1gNnSc2	úmoří
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
a	a	k8xC	a
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Judské	judský	k2eAgFnSc2d1	Judská
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
se	se	k3xPyFc4	se
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k6eAd1	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
činí	činit	k5eAaImIp3nS	činit
tak	tak	k6eAd1	tak
z	z	k7c2	z
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgNnPc2d3	nejstarší
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
je	být	k5eAaImIp3nS	být
nejsvětějším	nejsvětější	k2eAgNnSc7d1	nejsvětější
místem	místo	k1gNnSc7	místo
judaismu	judaismus	k1gInSc2	judaismus
a	a	k8xC	a
duchovním	duchovní	k2eAgNnSc7d1	duchovní
centrem	centrum	k1gNnSc7	centrum
židovského	židovský	k2eAgInSc2d1	židovský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
však	však	k9	však
také	také	k9	také
množství	množství	k1gNnSc4	množství
významných	významný	k2eAgNnPc2d1	významné
starověkých	starověký	k2eAgNnPc2d1	starověké
křesťanských	křesťanský	k2eAgNnPc2d1	křesťanské
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
třetí	třetí	k4xOgNnSc4	třetí
nejsvětější	nejsvětější	k2eAgNnSc4d1	nejsvětější
místo	místo	k1gNnSc4	místo
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1	nejsvětější
místa	místo	k1gNnSc2	místo
tří	tři	k4xCgNnPc2	tři
monoteistických	monoteistický	k2eAgNnPc2d1	monoteistické
náboženství	náboženství	k1gNnPc2	náboženství
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
necelého	celý	k2eNgInSc2d1	necelý
čtverečního	čtvereční	k2eAgInSc2d1	čtvereční
kilometru	kilometr	k1gInSc2	kilometr
a	a	k8xC	a
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
Chrámovou	chrámový	k2eAgFnSc4d1	chrámová
horu	hora	k1gFnSc4	hora
<g/>
,	,	kIx,	,
Západní	západní	k2eAgFnSc4d1	západní
zeď	zeď	k1gFnSc4	zeď
<g/>
,	,	kIx,	,
baziliku	bazilika	k1gFnSc4	bazilika
Svatého	svatý	k2eAgInSc2d1	svatý
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
Skalní	skalní	k2eAgInSc4d1	skalní
dóm	dóm	k1gInSc4	dóm
a	a	k8xC	a
mešitu	mešita	k1gFnSc4	mešita
al-Aksá	al-Aksat	k5eAaPmIp3nS	al-Aksat
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
jeruzalémskými	jeruzalémský	k2eAgFnPc7d1	Jeruzalémská
hradbami	hradba	k1gFnPc7	hradba
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
arménské	arménský	k2eAgFnSc2d1	arménská
<g/>
,	,	kIx,	,
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
<g/>
,	,	kIx,	,
židovské	židovský	k2eAgFnSc2d1	židovská
a	a	k8xC	a
muslimské	muslimský	k2eAgFnSc2d1	muslimská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
názvy	název	k1gInPc1	název
však	však	k9	však
byly	být	k5eAaImAgInP	být
zavedeny	zavést	k5eAaPmNgInP	zavést
až	až	k6eAd1	až
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
byl	být	k5eAaImAgInS	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
dvakrát	dvakrát	k6eAd1	dvakrát
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
krát	krát	k6eAd1	krát
obléhán	obléhat	k5eAaImNgInS	obléhat
<g/>
,	,	kIx,	,
52	[number]	k4	52
<g/>
krát	krát	k6eAd1	krát
napaden	napadnout	k5eAaPmNgMnS	napadnout
a	a	k8xC	a
44	[number]	k4	44
<g/>
krát	krát	k6eAd1	krát
dobyt	dobýt	k5eAaPmNgInS	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
status	status	k1gInSc4	status
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgInPc2d1	klíčový
problémů	problém	k1gInPc2	problém
izraelsko-palestinského	izraelskoalestinský	k2eAgInSc2d1	izraelsko-palestinský
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
anexe	anexe	k1gFnSc1	anexe
východního	východní	k2eAgInSc2d1	východní
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
byla	být	k5eAaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
za	za	k7c4	za
okupaci	okupace	k1gFnSc4	okupace
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
Organizací	organizace	k1gFnSc7	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
a	a	k8xC	a
právě	právě	k9	právě
východní	východní	k2eAgInSc1d1	východní
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Palestinskou	palestinský	k2eAgFnSc7d1	palestinská
autonomií	autonomie	k1gFnSc7	autonomie
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
jejich	jejich	k3xOp3gInSc2	jejich
budoucího	budoucí	k2eAgInSc2d1	budoucí
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rezoluce	rezoluce	k1gFnSc2	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
číslo	číslo	k1gNnSc1	číslo
478	[number]	k4	478
(	(	kIx(	(
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
přesunula	přesunout	k5eAaPmAgFnS	přesunout
většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
svá	svůj	k3xOyFgNnPc4	svůj
velvyslanectví	velvyslanectví	k1gNnPc4	velvyslanectví
mimo	mimo	k7c4	mimo
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Nejstaršími	starý	k2eAgInPc7d3	nejstarší
dokumenty	dokument	k1gInPc7	dokument
<g/>
,	,	kIx,	,
zmiňujícími	zmiňující	k2eAgInPc7d1	zmiňující
jméno	jméno	k1gNnSc4	jméno
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
egyptské	egyptský	k2eAgInPc1d1	egyptský
klatebné	klatebný	k2eAgInPc1d1	klatebný
nápisy	nápis	k1gInPc1	nápis
(	(	kIx(	(
<g/>
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
byli	být	k5eAaImAgMnP	být
proklínáni	proklínán	k2eAgMnPc1d1	proklínán
potenciální	potenciální	k2eAgMnPc1d1	potenciální
nepřátelé	nepřítel	k1gMnPc1	nepřítel
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jej	on	k3xPp3gMnSc4	on
napadali	napadat	k5eAaPmAgMnP	napadat
často	často	k6eAd1	často
právě	právě	k6eAd1	právě
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Syropalestiny	Syropalestina	k1gFnSc2	Syropalestina
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
nápisech	nápis	k1gInPc6	nápis
objevuje	objevovat	k5eAaImIp3nS	objevovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Ašamam	Ašamam	k1gInSc1	Ašamam
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Rašlamam	Rašlamam	k1gInSc1	Rašlamam
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Souhlásky	souhláska	k1gFnPc1	souhláska
š	š	k?	š
a	a	k8xC	a
m	m	kA	m
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
v	v	k7c6	v
podobném	podobný	k2eAgNnSc6d1	podobné
pojmenování	pojmenování	k1gNnSc6	pojmenování
–	–	k?	–
knize	kniha	k1gFnSc3	kniha
Genesis	Genesis	k1gFnSc4	Genesis
zmiňován	zmiňován	k2eAgMnSc1d1	zmiňován
"	"	kIx"	"
<g/>
Melchisedech	Melchised	k1gMnPc6	Melchised
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Šálemu	Šálem	k1gInSc2	Šálem
<g/>
"	"	kIx"	"
–	–	k?	–
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
ztotožňováno	ztotožňovat	k5eAaImNgNnS	ztotožňovat
s	s	k7c7	s
Jeruzalémem	Jeruzalém	k1gInSc7	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Jebús	Jebús	k1gInSc1	Jebús
<g/>
"	"	kIx"	"
pak	pak	k6eAd1	pak
vychází	vycházet	k5eAaImIp3nS	vycházet
od	od	k7c2	od
Jebusejců	Jebusejec	k1gMnPc2	Jebusejec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
město	město	k1gNnSc4	město
původně	původně	k6eAd1	původně
obývali	obývat	k5eAaImAgMnP	obývat
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
možná	možná	k9	možná
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
výrazů	výraz	k1gInPc2	výraz
do	do	k7c2	do
"	"	kIx"	"
<g/>
Jebús-Šálem	Jebús-Šál	k1gMnSc7	Jebús-Šál
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
pak	pak	k6eAd1	pak
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
"	"	kIx"	"
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
město	město	k1gNnSc1	město
pokoje	pokoj	k1gInSc2	pokoj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
́	́	k?	́
<g/>
ír	ír	k?	ír
šalom	šalom	k0	šalom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
pojmenování	pojmenování	k1gNnSc2	pojmenování
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
midraše	midraš	k1gInSc2	midraš
má	mít	k5eAaImIp3nS	mít
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
celkem	celkem	k6eAd1	celkem
70	[number]	k4	70
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
mnohá	mnohý	k2eAgFnSc1d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
jsou	být	k5eAaImIp3nP	být
básnická	básnický	k2eAgNnPc1d1	básnické
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
tyto	tento	k3xDgInPc1	tento
výrazy	výraz	k1gInPc1	výraz
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
užívány	užívat	k5eAaImNgFnP	užívat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
"	"	kIx"	"
<g/>
Město	město	k1gNnSc1	město
Davidovo	Davidův	k2eAgNnSc1d1	Davidovo
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Sijón	Sijón	k1gInSc1	Sijón
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
pojem	pojem	k1gInSc1	pojem
Sijón	Sijón	k1gInSc4	Sijón
však	však	k9	však
označuje	označovat	k5eAaImIp3nS	označovat
spíše	spíše	k9	spíše
Chrámovou	chrámový	k2eAgFnSc4d1	chrámová
horu	hora	k1gFnSc4	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
131	[number]	k4	131
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
císař	císař	k1gMnSc1	císař
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
na	na	k7c6	na
troskách	troska	k1gFnPc6	troska
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
římské	římský	k2eAgNnSc4d1	římské
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
"	"	kIx"	"
<g/>
Colonia	Colonium	k1gNnSc2	Colonium
Aelia	Aelium	k1gNnSc2	Aelium
Capitolina	Capitolina	k1gFnSc1	Capitolina
<g/>
"	"	kIx"	"
–	–	k?	–
Aelius	Aelius	k1gInSc1	Aelius
bylo	být	k5eAaImAgNnS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
jmen	jméno	k1gNnPc2	jméno
císaře	císař	k1gMnSc2	císař
Hadriana	Hadrian	k1gMnSc2	Hadrian
a	a	k8xC	a
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
Capitolina	Capitolina	k1gFnSc1	Capitolina
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
římského	římský	k2eAgMnSc2d1	římský
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
boha	bůh	k1gMnSc2	bůh
Jupitera	Jupiter	k1gMnSc2	Jupiter
Kapitolského	kapitolský	k2eAgMnSc2d1	kapitolský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
je	být	k5eAaImIp3nS	být
rozšířeným	rozšířený	k2eAgNnSc7d1	rozšířené
jménem	jméno	k1gNnSc7	jméno
pro	pro	k7c4	pro
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
"	"	kIx"	"
<g/>
Al-Kuds	Al-Kuds	k1gInSc4	Al-Kuds
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ا	ا	k?	ا
<g/>
ُ	ُ	k?	ُ
<g/>
د	د	k?	د
<g/>
ْ	ْ	k?	ْ
<g/>
س	س	k?	س
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
doslova	doslova	k6eAd1	doslova
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
svaté	svatý	k2eAgNnSc1d1	svaté
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
pro	pro	k7c4	pro
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
v	v	k7c6	v
arabštině	arabština	k1gFnSc6	arabština
pochází	pocházet	k5eAaImIp3nS	pocházet
právě	právě	k9	právě
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
"	"	kIx"	"
<g/>
Aelia	Aelia	k1gFnSc1	Aelia
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Ilija	Ilija	k1gMnSc1	Ilija
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
إ	إ	k?	إ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
eneolitu	eneolit	k1gInSc6	eneolit
na	na	k7c6	na
vyvýšenině	vyvýšenina	k1gFnSc6	vyvýšenina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nazývána	nazývat	k5eAaImNgFnS	nazývat
jako	jako	k8xC	jako
Město	město	k1gNnSc1	město
Davidovo	Davidův	k2eAgNnSc1d1	Davidovo
<g/>
.	.	kIx.	.
</s>
<s>
Proměna	proměna	k1gFnSc1	proměna
z	z	k7c2	z
přechodného	přechodný	k2eAgNnSc2d1	přechodné
sídliště	sídliště	k1gNnSc2	sídliště
v	v	k7c4	v
regulérní	regulérní	k2eAgNnSc4d1	regulérní
město	město	k1gNnSc4	město
se	se	k3xPyFc4	se
udála	udát	k5eAaPmAgFnS	udát
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
již	již	k9	již
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
opevněným	opevněný	k2eAgInSc7d1	opevněný
a	a	k8xC	a
důležitým	důležitý	k2eAgNnSc7d1	důležité
kenaánským	kenaánský	k2eAgNnSc7d1	kenaánský
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
biblických	biblický	k2eAgFnPc2d1	biblická
zpráv	zpráva	k1gFnPc2	zpráva
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc4	město
přinejmenším	přinejmenším	k6eAd1	přinejmenším
až	až	k6eAd1	až
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
obýváno	obýván	k2eAgNnSc1d1	obýváno
Jebusity	Jebusit	k1gInPc1	Jebusit
a	a	k8xC	a
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
padl	padnout	k5eAaImAgInS	padnout
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Izraelitů	izraelita	k1gMnPc2	izraelita
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
krále	král	k1gMnSc2	král
Davida	David	k1gMnSc2	David
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
ustanoven	ustanoven	k2eAgInSc1d1	ustanoven
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
izraelského	izraelský	k2eAgNnSc2d1	izraelské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
byla	být	k5eAaImAgFnS	být
údajně	údajně	k6eAd1	údajně
přenesena	přenést	k5eAaPmNgFnS	přenést
Archa	archa	k1gFnSc1	archa
úmluvy	úmluva	k1gFnSc2	úmluva
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
stal	stát	k5eAaPmAgInS	stát
nejen	nejen	k6eAd1	nejen
správním	správní	k2eAgMnPc3d1	správní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
náboženským	náboženský	k2eAgNnSc7d1	náboženské
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Davidův	Davidův	k2eAgMnSc1d1	Davidův
syn	syn	k1gMnSc1	syn
Šalomoun	Šalomoun	k1gMnSc1	Šalomoun
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
navíc	navíc	k6eAd1	navíc
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zbudoval	zbudovat	k5eAaPmAgInS	zbudovat
Chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
pozici	pozice	k1gFnSc4	pozice
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
významně	významně	k6eAd1	významně
posílilo	posílit	k5eAaPmAgNnS	posílit
<g/>
.	.	kIx.	.
</s>
<s>
Archeologický	archeologický	k2eAgInSc1d1	archeologický
průzkum	průzkum	k1gInSc1	průzkum
však	však	k9	však
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př.n.l.	př.n.l.	k?	př.n.l.
byl	být	k5eAaImAgInS	být
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
pouhou	pouhý	k2eAgFnSc7d1	pouhá
horskou	horský	k2eAgFnSc7d1	horská
vesnicí	vesnice	k1gFnSc7	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
staletí	staletí	k1gNnPc2	staletí
se	se	k3xPyFc4	se
však	však	k9	však
tato	tento	k3xDgFnSc1	tento
vesnice	vesnice	k1gFnSc1	vesnice
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
ve	v	k7c4	v
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Chizkijáše	Chizkijáš	k1gMnSc2	Chizkijáš
byl	být	k5eAaImAgInS	být
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
značně	značně	k6eAd1	značně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
701	[number]	k4	701
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
obležen	obležen	k2eAgInSc1d1	obležen
asyrským	asyrský	k2eAgNnSc7d1	asyrské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
však	však	k9	však
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
přestál	přestát	k5eAaPmAgInS	přestát
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
severoizraelského	severoizraelský	k2eAgNnSc2d1	severoizraelské
Samaří	Samaří	k1gNnSc2	Samaří
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
722	[number]	k4	722
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyto	dobyt	k2eAgNnSc1d1	dobyto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
díky	díky	k7c3	díky
Šiloašskému	Šiloašský	k2eAgInSc3d1	Šiloašský
tunelu	tunel	k1gInSc3	tunel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přiváděl	přivádět	k5eAaImAgInS	přivádět
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
pramene	pramen	k1gInSc2	pramen
Gichon	Gichon	k1gInSc1	Gichon
nacházejícího	nacházející	k2eAgMnSc2d1	nacházející
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
do	do	k7c2	do
nádrže	nádrž	k1gFnSc2	nádrž
Siloam	Siloam	k1gInSc1	Siloam
uvnitř	uvnitř	k7c2	uvnitř
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
598	[number]	k4	598
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
však	však	k9	však
byl	být	k5eAaImAgInS	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
dobyt	dobyt	k2eAgInSc1d1	dobyt
babylonským	babylonský	k2eAgNnSc7d1	babylonské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
vzpouru	vzpoura	k1gFnSc4	vzpoura
proti	proti	k7c3	proti
babylonské	babylonský	k2eAgFnSc3d1	Babylonská
nadvládě	nadvláda	k1gFnSc3	nadvláda
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
roku	rok	k1gInSc2	rok
586	[number]	k4	586
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyto	dobýt	k5eAaPmNgNnS	dobýt
znovu	znovu	k6eAd1	znovu
–	–	k?	–
babylonský	babylonský	k2eAgMnSc1d1	babylonský
král	král	k1gMnSc1	král
Nebukadnesar	Nebukadnesar	k1gMnSc1	Nebukadnesar
II	II	kA	II
<g/>
.	.	kIx.	.
zničil	zničit	k5eAaPmAgInS	zničit
jeruzalémský	jeruzalémský	k2eAgInSc4d1	jeruzalémský
Chrám	chrám	k1gInSc4	chrám
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
odvedl	odvést	k5eAaPmAgMnS	odvést
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
Babylonii	Babylonie	k1gFnSc4	Babylonie
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
Peršané	Peršan	k1gMnPc1	Peršan
a	a	k8xC	a
Kýros	Kýros	k1gInSc1	Kýros
II	II	kA	II
<g/>
.	.	kIx.	.
povolil	povolit	k5eAaPmAgInS	povolit
návrat	návrat	k1gInSc4	návrat
zajatců	zajatec	k1gMnPc2	zajatec
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
svobodu	svoboda	k1gFnSc4	svoboda
(	(	kIx(	(
<g/>
539	[number]	k4	539
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
znovu	znovu	k6eAd1	znovu
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
judské	judský	k2eAgFnSc2d1	Judská
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
i	i	k9	i
Chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
tentokrát	tentokrát	k6eAd1	tentokrát
poměrně	poměrně	k6eAd1	poměrně
skromný	skromný	k2eAgMnSc1d1	skromný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
332	[number]	k4	332
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zemi	zem	k1gFnSc6	zem
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
dotčen	dotknout	k5eAaPmNgInS	dotknout
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
období	období	k1gNnSc1	období
helenizace	helenizace	k1gFnSc1	helenizace
<g/>
,	,	kIx,	,
izraelská	izraelský	k2eAgFnSc1d1	izraelská
země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
zprvu	zprvu	k6eAd1	zprvu
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Ptolemaiovců	Ptolemaiovec	k1gMnPc2	Ptolemaiovec
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
198	[number]	k4	198
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pak	pak	k6eAd1	pak
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Seleukovců	Seleukovec	k1gInPc2	Seleukovec
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
židovské	židovský	k2eAgFnPc1d1	židovská
vrstvy	vrstva	k1gFnPc1	vrstva
ochotně	ochotně	k6eAd1	ochotně
přijímaly	přijímat	k5eAaImAgFnP	přijímat
řecké	řecký	k2eAgFnPc4d1	řecká
zvyklosti	zvyklost	k1gFnPc4	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
proti	proti	k7c3	proti
židům	žid	k1gMnPc3	žid
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
<g/>
,	,	kIx,	,
když	když	k8xS	když
Antiochos	Antiochos	k1gInSc1	Antiochos
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Epifanés	Epifanés	k1gInSc1	Epifanés
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
říši	říš	k1gFnSc6	říš
jediný	jediný	k2eAgInSc1d1	jediný
kult	kult	k1gInSc1	kult
a	a	k8xC	a
v	v	k7c6	v
jeruzalémském	jeruzalémský	k2eAgInSc6d1	jeruzalémský
Chrámu	chrám	k1gInSc6	chrám
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
oltáře	oltář	k1gInSc2	oltář
postavena	postaven	k2eAgFnSc1d1	postavena
socha	socha	k1gFnSc1	socha
Dia	Dia	k1gMnSc2	Dia
Olympského	olympský	k2eAgMnSc2d1	olympský
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
roku	rok	k1gInSc2	rok
167	[number]	k4	167
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
Makabejské	makabejský	k2eAgNnSc1d1	Makabejské
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
Judeu	Judea	k1gFnSc4	Judea
zbavilo	zbavit	k5eAaPmAgNnS	zbavit
cizí	cizí	k2eAgFnPc4d1	cizí
nadvlády	nadvláda	k1gFnPc4	nadvláda
–	–	k?	–
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
Hasmonejci	Hasmonejec	k1gMnPc1	Hasmonejec
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Hasmonejců	Hasmonejec	k1gMnPc2	Hasmonejec
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
63	[number]	k4	63
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
37	[number]	k4	37
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
Římem	Řím	k1gInSc7	Řím
jmenován	jmenovat	k5eAaImNgInS	jmenovat
judským	judský	k2eAgMnSc7d1	judský
tetrarchou	tetrarcha	k1gMnSc7	tetrarcha
Herodes	Herodes	k1gMnSc1	Herodes
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
město	město	k1gNnSc4	město
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
zkrášlil	zkrášlit	k5eAaPmAgMnS	zkrášlit
–	–	k?	–
nechal	nechat	k5eAaPmAgInS	nechat
přebudovat	přebudovat	k5eAaPmF	přebudovat
Chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
ulice	ulice	k1gFnPc1	ulice
nechal	nechat	k5eAaPmAgInS	nechat
vydláždit	vydláždit	k5eAaPmF	vydláždit
mramorem	mramor	k1gInSc7	mramor
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
pevnost	pevnost	k1gFnSc4	pevnost
Antonia	Antonio	k1gMnSc2	Antonio
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
4	[number]	k4	4
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Judeu	Judea	k1gFnSc4	Judea
spravovali	spravovat	k5eAaImAgMnP	spravovat
římští	římský	k2eAgMnPc1d1	římský
prefekti	prefekt	k1gMnPc1	prefekt
a	a	k8xC	a
prokurátoři	prokurátor	k1gMnPc1	prokurátor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
s	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stále	stále	k6eAd1	stále
necitlivěji	citlivě	k6eNd2	citlivě
zasahovali	zasahovat	k5eAaImAgMnP	zasahovat
do	do	k7c2	do
záležitostí	záležitost	k1gFnPc2	záležitost
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
uvrhovali	uvrhovat	k5eAaImAgMnP	uvrhovat
zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
chaosu	chaos	k1gInSc2	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
66	[number]	k4	66
nakonec	nakonec	k6eAd1	nakonec
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
protiřímské	protiřímský	k2eAgNnSc1d1	protiřímské
povstání	povstání	k1gNnSc1	povstání
–	–	k?	–
začala	začít	k5eAaPmAgFnS	začít
První	první	k4xOgFnSc1	první
židovská	židovský	k2eAgFnSc1d1	židovská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
69	[number]	k4	69
Římané	Říman	k1gMnPc1	Říman
oblehli	oblehnout	k5eAaPmAgMnP	oblehnout
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
70	[number]	k4	70
téměř	téměř	k6eAd1	téměř
vyhladověn	vyhladověn	k2eAgMnSc1d1	vyhladověn
<g/>
,	,	kIx,	,
dobyli	dobýt	k5eAaPmAgMnP	dobýt
jej	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc4	chrám
vypálili	vypálit	k5eAaPmAgMnP	vypálit
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
zcela	zcela	k6eAd1	zcela
srovnali	srovnat	k5eAaPmAgMnP	srovnat
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Chrámu	chrám	k1gInSc2	chrám
zbyla	zbýt	k5eAaPmAgFnS	zbýt
jen	jen	k9	jen
západní	západní	k2eAgFnSc1d1	západní
zeď	zeď	k1gFnSc1	zeď
–	–	k?	–
Zeď	zeď	k1gFnSc4	zeď
nářků	nářek	k1gInPc2	nářek
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
až	až	k6eAd1	až
do	do	k7c2	do
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Hadriána	Hadrián	k1gMnSc2	Hadrián
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gNnSc4	on
znovu	znovu	k6eAd1	znovu
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
jako	jako	k9	jako
římské	římský	k2eAgNnSc4d1	římské
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgFnPc4d1	pojmenovaná
Colonia	Colonium	k1gNnPc4	Colonium
Aelia	Aelius	k1gMnSc2	Aelius
Capitolina	Capitolin	k2eAgMnSc2d1	Capitolin
(	(	kIx(	(
<g/>
135	[number]	k4	135
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
zakázal	zakázat	k5eAaPmAgMnS	zakázat
židům	žid	k1gMnPc3	žid
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
vstupovat	vstupovat	k5eAaImF	vstupovat
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
striktně	striktně	k6eAd1	striktně
dodržován	dodržován	k2eAgInSc1d1	dodržován
–	–	k?	–
ještě	ještě	k9	ještě
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
samém	samý	k3xTgNnSc6	samý
století	století	k1gNnSc6	století
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
židovská	židovský	k2eAgFnSc1d1	židovská
osada	osada	k1gFnSc1	osada
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Constantinus	Constantinus	k1gMnSc1	Constantinus
I.	I.	kA	I.
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
přeměnit	přeměnit	k5eAaPmF	přeměnit
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
v	v	k7c4	v
křesťanské	křesťanský	k2eAgNnSc4d1	křesťanské
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
obnovil	obnovit	k5eAaPmAgInS	obnovit
tedy	tedy	k9	tedy
Hadriánův	Hadriánův	k2eAgInSc1d1	Hadriánův
zákaz	zákaz	k1gInSc1	zákaz
<g/>
.	.	kIx.	.
</s>
<s>
Mírně	mírně	k6eAd1	mírně
však	však	k9	však
slevil	slevit	k5eAaPmAgMnS	slevit
<g/>
,	,	kIx,	,
když	když	k8xS	když
dovolil	dovolit	k5eAaPmAgMnS	dovolit
židům	žid	k1gMnPc3	žid
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
města	město	k1gNnSc2	město
na	na	k7c4	na
jediný	jediný	k2eAgInSc4d1	jediný
den	den	k1gInSc4	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
–	–	k?	–
ve	v	k7c4	v
výroční	výroční	k2eAgInSc4d1	výroční
den	den	k1gInSc4	den
zboření	zboření	k1gNnSc2	zboření
Chrámu	chrám	k1gInSc2	chrám
(	(	kIx(	(
<g/>
Tiš	tiš	k1gFnSc1	tiš
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
be-av	bev	k1gInSc1	be-av
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Konstantina	Konstantin	k1gMnSc2	Konstantin
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nalezla	nalézt	k5eAaBmAgFnS	nalézt
zbytky	zbytek	k1gInPc4	zbytek
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
byl	být	k5eAaImAgMnS	být
ukřižován	ukřižován	k2eAgMnSc1d1	ukřižován
Ježíš	Ježíš	k1gMnSc1	Ježíš
<g/>
.	.	kIx.	.
</s>
<s>
Určila	určit	k5eAaPmAgFnS	určit
místo	místo	k1gNnSc4	místo
ukřižování	ukřižování	k1gNnPc2	ukřižování
a	a	k8xC	a
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
bazilika	bazilika	k1gFnSc1	bazilika
Svatého	svatý	k2eAgInSc2d1	svatý
hrobu	hrob	k1gInSc2	hrob
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Boží	boží	k2eAgInSc1d1	boží
hrob	hrob	k1gInSc1	hrob
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konstantinův	Konstantinův	k2eAgMnSc1d1	Konstantinův
následník	následník	k1gMnSc1	následník
<g/>
,	,	kIx,	,
Iulianus	Iulianus	k1gMnSc1	Iulianus
Apostata	apostata	k1gMnSc1	apostata
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
,	,	kIx,	,
zákaz	zákaz	k1gInSc4	zákaz
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
zrušil	zrušit	k5eAaPmAgInS	zrušit
a	a	k8xC	a
také	také	k9	také
zahájil	zahájit	k5eAaPmAgInS	zahájit
stavbu	stavba	k1gFnSc4	stavba
nového	nový	k2eAgInSc2d1	nový
židovského	židovský	k2eAgInSc2d1	židovský
Chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
363	[number]	k4	363
<g/>
)	)	kIx)	)
zemřel	zemřít	k5eAaPmAgMnS	zemřít
při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
proti	proti	k7c3	proti
Persii	Persie	k1gFnSc3	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
následníci	následník	k1gMnPc1	následník
pak	pak	k6eAd1	pak
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
znovu	znovu	k6eAd1	znovu
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
395	[number]	k4	395
se	se	k3xPyFc4	se
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
Palestiny	Palestina	k1gFnSc2	Palestina
připadla	připadnout	k5eAaPmAgFnS	připadnout
Východořímské	východořímský	k2eAgFnSc3d1	Východořímská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Východořímská	východořímský	k2eAgFnSc1d1	Východořímská
říše	říše	k1gFnSc1	říše
plně	plně	k6eAd1	plně
podporovala	podporovat	k5eAaImAgFnS	podporovat
šíření	šíření	k1gNnSc4	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
a	a	k8xC	a
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
prokřesťanské	prokřesťanský	k2eAgFnSc2d1	prokřesťanská
politiky	politika	k1gFnSc2	politika
přijímala	přijímat	k5eAaImAgNnP	přijímat
i	i	k8xC	i
protižidovská	protižidovský	k2eAgNnPc1d1	protižidovské
opatření	opatření	k1gNnPc1	opatření
(	(	kIx(	(
<g/>
Theodosius	Theodosius	k1gInSc1	Theodosius
I.	I.	kA	I.
a	a	k8xC	a
Theodosius	Theodosius	k1gMnSc1	Theodosius
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
především	především	k9	především
trvání	trvání	k1gNnSc4	trvání
zákazu	zákaz	k1gInSc2	zákaz
pobytu	pobyt	k1gInSc2	pobyt
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
se	se	k3xPyFc4	se
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
církevních	církevní	k2eAgFnPc2d1	církevní
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
klášterů	klášter	k1gInPc2	klášter
(	(	kIx(	(
<g/>
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Theodosia	Theodosius	k1gMnSc2	Theodosius
II	II	kA	II
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
již	již	k6eAd1	již
200	[number]	k4	200
klášterů	klášter	k1gInPc2	klášter
a	a	k8xC	a
hospiců	hospic	k1gInPc2	hospic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
byl	být	k5eAaImAgInS	být
sídlem	sídlo	k1gNnSc7	sídlo
křesťanského	křesťanský	k2eAgMnSc2d1	křesťanský
patriarchy	patriarcha	k1gMnSc2	patriarcha
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
hlavou	hlava	k1gFnSc7	hlava
církve	církev	k1gFnSc2	církev
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
rovněž	rovněž	k9	rovněž
cílem	cíl	k1gInSc7	cíl
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
poutníků	poutník	k1gMnPc2	poutník
všech	všecek	k3xTgNnPc2	všecek
vyznání	vyznání	k1gNnPc2	vyznání
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
latinských	latinský	k2eAgMnPc2d1	latinský
poutníků	poutník	k1gMnPc2	poutník
ze	z	k7c2	z
Západu	západ	k1gInSc2	západ
a	a	k8xC	a
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
přicházeli	přicházet	k5eAaImAgMnP	přicházet
i	i	k9	i
afričtí	africký	k2eAgMnPc1d1	africký
poutníci	poutník	k1gMnPc1	poutník
z	z	k7c2	z
Núbie	Núbie	k1gFnSc2	Núbie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
také	také	k9	také
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
Židy	Žid	k1gMnPc4	Žid
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
638	[number]	k4	638
byl	být	k5eAaImAgInS	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
ovládaný	ovládaný	k2eAgInSc1d1	ovládaný
Byzantinci	Byzantinec	k1gMnPc7	Byzantinec
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
dobyt	dobýt	k5eAaPmNgInS	dobýt
muslimskými	muslimský	k2eAgInPc7d1	muslimský
vojsky	vojsko	k1gNnPc7	vojsko
chalífy	chalífa	k1gMnSc2	chalífa
Umara	Umar	k1gMnSc2	Umar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
rychle	rychle	k6eAd1	rychle
povolili	povolit	k5eAaPmAgMnP	povolit
židům	žid	k1gMnPc3	žid
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Chrámové	chrámový	k2eAgFnSc6d1	chrámová
hoře	hora	k1gFnSc6	hora
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
Skalní	skalní	k2eAgInSc1d1	skalní
dóm	dóm	k1gInSc1	dóm
a	a	k8xC	a
mešita	mešita	k1gFnSc1	mešita
al-Aksá	al-Aksat	k5eAaImIp3nS	al-Aksat
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
poté	poté	k6eAd1	poté
na	na	k7c4	na
dalších	další	k2eAgNnPc2d1	další
400	[number]	k4	400
let	léto	k1gNnPc2	léto
náležel	náležet	k5eAaImAgMnS	náležet
muslimským	muslimský	k2eAgMnSc7d1	muslimský
Umajjovcům	Umajjovec	k1gMnPc3	Umajjovec
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
Fátimovcům	Fátimovec	k1gMnPc3	Fátimovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
roku	rok	k1gInSc3	rok
1071	[number]	k4	1071
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
roku	rok	k1gInSc2	rok
1097	[number]	k4	1097
jej	on	k3xPp3gInSc4	on
Fátimovci	Fátimovec	k1gInPc7	Fátimovec
od	od	k7c2	od
Turků	turek	k1gInPc2	turek
<g/>
,	,	kIx,	,
oslabených	oslabený	k2eAgInPc2d1	oslabený
postupem	postupem	k7c2	postupem
první	první	k4xOgFnSc2	první
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
dobyli	dobýt	k5eAaPmAgMnP	dobýt
nazpět	nazpět	k6eAd1	nazpět
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
obležen	obležen	k2eAgInSc1d1	obležen
a	a	k8xC	a
dobyt	dobyt	k2eAgInSc1d1	dobyt
evropskými	evropský	k2eAgInPc7d1	evropský
křižáky	křižák	k1gInPc7	křižák
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
většinu	většina	k1gFnSc4	většina
jeho	on	k3xPp3gNnSc2	on
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
povraždili	povraždit	k5eAaPmAgMnP	povraždit
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
centrem	centr	k1gInSc7	centr
křižáckého	křižácký	k2eAgNnSc2d1	křižácké
Jeruzalémského	jeruzalémský	k2eAgNnSc2d1	Jeruzalémské
království	království	k1gNnSc2	království
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
88	[number]	k4	88
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
křižáků	křižák	k1gInPc2	křižák
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hattínu	Hattín	k1gInSc2	Hattín
roku	rok	k1gInSc2	rok
1187	[number]	k4	1187
se	se	k3xPyFc4	se
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
Jeruzalému	Jeruzalém	k1gInSc3	Jeruzalém
dostal	dostat	k5eAaPmAgInS	dostat
Saladin	Saladin	k2eAgInSc1d1	Saladin
<g/>
,	,	kIx,	,
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
obléhání	obléhání	k1gNnSc6	obléhání
si	se	k3xPyFc3	se
město	město	k1gNnSc4	město
podmanil	podmanit	k5eAaPmAgMnS	podmanit
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
pod	pod	k7c4	pod
muslimskou	muslimský	k2eAgFnSc4d1	muslimská
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1192	[number]	k4	1192
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
Saladin	Saladin	k2eAgInSc1d1	Saladin
s	s	k7c7	s
křižáky	křižák	k1gInPc7	křižák
dohodu	dohoda	k1gFnSc4	dohoda
z	z	k7c2	z
Ramly	Ramla	k1gFnSc2	Ramla
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
volný	volný	k2eAgInSc4d1	volný
přístup	přístup	k1gInSc4	přístup
křesťanským	křesťanský	k2eAgFnPc3d1	křesťanská
poutníkům	poutník	k1gMnPc3	poutník
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
návratem	návrat	k1gInSc7	návrat
muslimské	muslimský	k2eAgFnSc2d1	muslimská
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
vrátili	vrátit	k5eAaPmAgMnP	vrátit
i	i	k9	i
židé	žid	k1gMnPc1	žid
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nadvláda	nadvláda	k1gFnSc1	nadvláda
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
střídala	střídat	k5eAaImAgFnS	střídat
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
křižáci	křižák	k1gMnPc1	křižák
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Akkonu	Akkon	k1gInSc2	Akkon
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc2d1	poslední
křižácké	křižácký	k2eAgFnSc2d1	křižácká
výspy	výspa	k1gFnSc2	výspa
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1291	[number]	k4	1291
definitivně	definitivně	k6eAd1	definitivně
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1517	[number]	k4	1517
padl	padnout	k5eAaImAgInS	padnout
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
město	město	k1gNnSc1	město
vkročilo	vkročit	k5eAaPmAgNnS	vkročit
do	do	k7c2	do
období	období	k1gNnSc2	období
rozkvětu	rozkvět	k1gInSc2	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
Sulejman	Sulejman	k1gMnSc1	Sulejman
I.	I.	kA	I.
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
dodnes	dodnes	k6eAd1	dodnes
dochované	dochovaný	k2eAgFnPc4d1	dochovaná
jeruzalémské	jeruzalémský	k2eAgFnPc4d1	Jeruzalémská
hradby	hradba	k1gFnPc4	hradba
<g/>
,	,	kIx,	,
obnovil	obnovit	k5eAaPmAgMnS	obnovit
tvrz	tvrz	k1gFnSc4	tvrz
a	a	k8xC	a
Davidovu	Davidův	k2eAgFnSc4d1	Davidova
věž	věž	k1gFnSc4	věž
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
zeslábla	zeslábnout	k5eAaPmAgFnS	zeslábnout
moc	moc	k1gFnSc1	moc
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
moc	moc	k1gFnSc1	moc
lokálních	lokální	k2eAgMnPc2d1	lokální
vládců	vládce	k1gMnPc2	vládce
a	a	k8xC	a
náčelníků	náčelník	k1gMnPc2	náčelník
usazených	usazený	k2eAgInPc2d1	usazený
i	i	k8xC	i
nomádských	nomádský	k2eAgInPc2d1	nomádský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
skupiny	skupina	k1gFnPc1	skupina
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
často	často	k6eAd1	často
bojovaly	bojovat	k5eAaImAgFnP	bojovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
pustnutí	pustnutí	k1gNnSc3	pustnutí
celé	celý	k2eAgFnSc2d1	celá
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
přirozeně	přirozeně	k6eAd1	přirozeně
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
i	i	k9	i
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
velké	velký	k2eAgFnPc1d1	velká
části	část	k1gFnPc1	část
uprostřed	uprostřed	k7c2	uprostřed
města	město	k1gNnSc2	město
vylidnily	vylidnit	k5eAaPmAgFnP	vylidnit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
čítalo	čítat	k5eAaImAgNnS	čítat
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
asi	asi	k9	asi
9	[number]	k4	9
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
4	[number]	k4	4
tisíce	tisíc	k4xCgInSc2	tisíc
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
3	[number]	k4	3
tisíce	tisíc	k4xCgInPc1	tisíc
křesťanů	křesťan	k1gMnPc2	křesťan
a	a	k8xC	a
2	[number]	k4	2
tisíce	tisíc	k4xCgInSc2	tisíc
židů	žid	k1gMnPc2	žid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgFnSc1	veškerý
populace	populace	k1gFnSc1	populace
žila	žít	k5eAaImAgFnS	žít
doposud	doposud	k6eAd1	doposud
uvnitř	uvnitř	k7c2	uvnitř
hradeb	hradba	k1gFnPc2	hradba
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
tvořily	tvořit	k5eAaImAgFnP	tvořit
čtyři	čtyři	k4xCgFnPc1	čtyři
čtvrti	čtvrt	k1gFnPc1	čtvrt
<g/>
:	:	kIx,	:
muslimská	muslimský	k2eAgFnSc1d1	muslimská
<g/>
,	,	kIx,	,
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
<g/>
,	,	kIx,	,
židovská	židovský	k2eAgFnSc1d1	židovská
a	a	k8xC	a
arménská	arménský	k2eAgFnSc1d1	arménská
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
mírnou	mírný	k2eAgFnSc4d1	mírná
imigraci	imigrace	k1gFnSc4	imigrace
židů	žid	k1gMnPc2	žid
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
však	však	k9	však
město	město	k1gNnSc4	město
stále	stále	k6eAd1	stále
stagnovalo	stagnovat	k5eAaImAgNnS	stagnovat
<g/>
.	.	kIx.	.
</s>
<s>
Změnu	změna	k1gFnSc4	změna
přinesl	přinést	k5eAaPmAgInS	přinést
až	až	k6eAd1	až
nový	nový	k2eAgInSc1d1	nový
správní	správní	k2eAgInSc1d1	správní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zavedl	zavést	k5eAaPmAgInS	zavést
Ibráhím	Ibráhí	k1gNnSc7	Ibráhí
Álí	Álí	k1gFnPc2	Álí
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
s	s	k7c7	s
egyptským	egyptský	k2eAgNnSc7d1	egyptské
vojskem	vojsko	k1gNnSc7	vojsko
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Palestinu	Palestina	k1gFnSc4	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Egypťané	Egypťan	k1gMnPc1	Egypťan
dokázali	dokázat	k5eAaPmAgMnP	dokázat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zajistit	zajistit	k5eAaPmF	zajistit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
pacifikovat	pacifikovat	k5eAaBmF	pacifikovat
odbojné	odbojný	k2eAgInPc4d1	odbojný
kmenové	kmenový	k2eAgInPc4d1	kmenový
klany	klan	k1gInPc4	klan
a	a	k8xC	a
podpořit	podpořit	k5eAaPmF	podpořit
rozvoj	rozvoj	k1gInSc4	rozvoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
událostí	událost	k1gFnPc2	událost
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
během	během	k7c2	během
egyptské	egyptský	k2eAgFnSc2d1	egyptská
vlády	vláda	k1gFnSc2	vláda
stala	stát	k5eAaPmAgFnS	stát
imigrace	imigrace	k1gFnSc1	imigrace
asi	asi	k9	asi
1	[number]	k4	1
500	[number]	k4	500
židů	žid	k1gMnPc2	žid
<g/>
,	,	kIx,	,
již	jenž	k3xRgMnPc1	jenž
přišli	přijít	k5eAaPmAgMnP	přijít
ze	z	k7c2	z
zemětřesením	zemětřesení	k1gNnPc3	zemětřesení
poničeného	poničený	k2eAgMnSc4d1	poničený
Safedu	Safeda	k1gMnSc4	Safeda
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
židé	žid	k1gMnPc1	žid
stali	stát	k5eAaPmAgMnP	stát
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
skupinou	skupina	k1gFnSc7	skupina
(	(	kIx(	(
<g/>
5000	[number]	k4	5000
židů	žid	k1gMnPc2	žid
<g/>
,	,	kIx,	,
4500	[number]	k4	4500
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
3500	[number]	k4	3500
křesťanů	křesťan	k1gMnPc2	křesťan
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
přispěchala	přispěchat	k5eAaPmAgFnS	přispěchat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
dalšími	další	k2eAgFnPc7d1	další
evropskými	evropský	k2eAgFnPc7d1	Evropská
mocnostmi	mocnost	k1gFnPc7	mocnost
a	a	k8xC	a
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
Egypťany	Egypťan	k1gMnPc4	Egypťan
z	z	k7c2	z
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Osmané	Osman	k1gMnPc1	Osman
sice	sice	k8xC	sice
od	od	k7c2	od
Egypťanů	Egypťan	k1gMnPc2	Egypťan
převzali	převzít	k5eAaPmAgMnP	převzít
moderní	moderní	k2eAgInSc4d1	moderní
správní	správní	k2eAgInSc4d1	správní
systém	systém	k1gInSc4	systém
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
,	,	kIx,	,
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
již	již	k6eAd1	již
však	však	k9	však
zajistit	zajistit	k5eAaPmF	zajistit
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
boje	boj	k1gInPc1	boj
kmenových	kmenový	k2eAgMnPc2d1	kmenový
náčelníků	náčelník	k1gMnPc2	náčelník
se	se	k3xPyFc4	se
obnovily	obnovit	k5eAaPmAgInP	obnovit
a	a	k8xC	a
země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
uvržena	uvrhnout	k5eAaPmNgFnS	uvrhnout
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
stagnace	stagnace	k1gFnSc2	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1	Osmanská
říše	říše	k1gFnSc1	říše
si	se	k3xPyFc3	se
byla	být	k5eAaImAgFnS	být
vědoma	vědom	k2eAgFnSc1d1	vědoma
<g/>
,	,	kIx,	,
že	že	k8xS	že
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Palestinou	Palestina	k1gFnSc7	Palestina
jí	on	k3xPp3gFnSc2	on
získaly	získat	k5eAaPmAgFnP	získat
zpět	zpět	k6eAd1	zpět
evropské	evropský	k2eAgFnPc1d1	Evropská
mocnosti	mocnost	k1gFnPc1	mocnost
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
proto	proto	k8xC	proto
zavádět	zavádět	k5eAaImF	zavádět
reformy	reforma	k1gFnPc4	reforma
a	a	k8xC	a
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
také	také	k9	také
liberálnější	liberální	k2eAgInSc4d2	liberálnější
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
menšinám	menšina	k1gFnPc3	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Evropské	evropský	k2eAgInPc1d1	evropský
státy	stát	k1gInPc1	stát
si	se	k3xPyFc3	se
navíc	navíc	k6eAd1	navíc
mohly	moct	k5eAaImAgFnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zasahovat	zasahovat	k5eAaImF	zasahovat
do	do	k7c2	do
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
záležitostí	záležitost	k1gFnPc2	záležitost
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
obnově	obnova	k1gFnSc3	obnova
kapitulací	kapitulace	k1gFnPc2	kapitulace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
projevilo	projevit	k5eAaPmAgNnS	projevit
příchodem	příchod	k1gInSc7	příchod
evropských	evropský	k2eAgMnPc2d1	evropský
konzulů	konzul	k1gMnPc2	konzul
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
ochranou	ochrana	k1gFnSc7	ochrana
si	se	k3xPyFc3	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
městě	město	k1gNnSc6	město
zřídil	zřídit	k5eAaPmAgInS	zřídit
své	svůj	k3xOyFgMnPc4	svůj
sídlo	sídlo	k1gNnSc1	sídlo
také	také	k6eAd1	také
anglikánský	anglikánský	k2eAgMnSc1d1	anglikánský
biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
řecký	řecký	k2eAgMnSc1d1	řecký
i	i	k8xC	i
latinský	latinský	k2eAgMnSc1d1	latinský
patriarcha	patriarcha	k1gMnSc1	patriarcha
a	a	k8xC	a
misijní	misijní	k2eAgFnPc1d1	misijní
organizace	organizace	k1gFnPc1	organizace
vybudovaly	vybudovat	k5eAaPmAgFnP	vybudovat
nemocnice	nemocnice	k1gFnPc4	nemocnice
a	a	k8xC	a
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zlepšení	zlepšení	k1gNnSc2	zlepšení
dopravního	dopravní	k2eAgNnSc2d1	dopravní
spojení	spojení	k1gNnSc2	spojení
Palestiny	Palestina	k1gFnSc2	Palestina
s	s	k7c7	s
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
represí	represe	k1gFnPc2	represe
v	v	k7c6	v
Ruském	ruský	k2eAgNnSc6d1	ruské
impériu	impérium	k1gNnSc6	impérium
vůči	vůči	k7c3	vůči
židům	žid	k1gMnPc3	žid
<g/>
,	,	kIx,	,
přicházelo	přicházet	k5eAaImAgNnS	přicházet
do	do	k7c2	do
Palestiny	Palestina	k1gFnSc2	Palestina
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
aškenázských	aškenázský	k2eAgMnPc2d1	aškenázský
židů	žid	k1gMnPc2	žid
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
nakonec	nakonec	k6eAd1	nakonec
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
právní	právní	k2eAgFnSc4d1	právní
ochranu	ochrana	k1gFnSc4	ochrana
zejména	zejména	k9	zejména
britský	britský	k2eAgInSc4d1	britský
konzulát	konzulát	k1gInSc4	konzulát
<g/>
.	.	kIx.	.
</s>
<s>
Židovské	židovský	k2eAgNnSc1d1	Židovské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
začalo	začít	k5eAaPmAgNnS	začít
diferencovat	diferencovat	k5eAaImF	diferencovat
<g/>
;	;	kIx,	;
sefardští	sefardský	k2eAgMnPc1d1	sefardský
židé	žid	k1gMnPc1	žid
totiž	totiž	k9	totiž
byli	být	k5eAaImAgMnP	být
zpravidla	zpravidla	k6eAd1	zpravidla
osmanskými	osmanský	k2eAgMnPc7d1	osmanský
občany	občan	k1gMnPc7	občan
<g/>
,	,	kIx,	,
a	a	k8xC	a
disponovali	disponovat	k5eAaBmAgMnP	disponovat
tudíž	tudíž	k8xC	tudíž
značně	značně	k6eAd1	značně
většími	veliký	k2eAgFnPc7d2	veliký
právy	práv	k2eAgMnPc4d1	práv
než	než	k8xS	než
Aškenázové	Aškenázový	k2eAgMnPc4d1	Aškenázový
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
například	například	k6eAd1	například
jakožto	jakožto	k8xS	jakožto
cizinci	cizinec	k1gMnPc1	cizinec
nemohli	moct	k5eNaImAgMnP	moct
nakupovat	nakupovat	k5eAaBmF	nakupovat
nemovitosti	nemovitost	k1gFnPc4	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
Aškenázské	Aškenázský	k2eAgMnPc4d1	Aškenázský
židy	žid	k1gMnPc4	žid
proto	proto	k8xC	proto
podporovali	podporovat	k5eAaImAgMnP	podporovat
židé	žid	k1gMnPc1	žid
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
domovských	domovský	k2eAgFnPc2d1	domovská
zemí	zem	k1gFnPc2	zem
skrze	skrze	k?	skrze
fondy	fond	k1gInPc1	fond
<g/>
,	,	kIx,	,
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
chaluka	chaluk	k1gMnSc4	chaluk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
připadl	připadnout	k5eAaPmAgInS	připadnout
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1917	[number]	k4	1917
Britům	Brit	k1gMnPc3	Brit
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
do	do	k7c2	do
města	město	k1gNnSc2	město
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
vojenský	vojenský	k2eAgMnSc1d1	vojenský
velitel	velitel	k1gMnSc1	velitel
Edmund	Edmund	k1gMnSc1	Edmund
Allenby	Allenba	k1gFnSc2	Allenba
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
města	město	k1gNnSc2	město
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
Britů	Brit	k1gMnPc2	Brit
stouplo	stoupnout	k5eAaPmAgNnS	stoupnout
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
významným	významný	k2eAgNnSc7d1	významné
centrem	centrum	k1gNnSc7	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
založením	založení	k1gNnSc7	založení
Britského	britský	k2eAgInSc2d1	britský
mandátu	mandát	k1gInSc2	mandát
Palestina	Palestina	k1gFnSc1	Palestina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
místem	místo	k1gNnSc7	místo
sídla	sídlo	k1gNnSc2	sídlo
nejvyššího	vysoký	k2eAgMnSc2d3	nejvyšší
komisaře	komisař	k1gMnSc2	komisař
<g/>
.	.	kIx.	.
</s>
<s>
Sionisté	sionista	k1gMnPc1	sionista
navíc	navíc	k6eAd1	navíc
ve	v	k7c6	v
městě	město	k1gNnSc6	město
postavili	postavit	k5eAaPmAgMnP	postavit
sídlo	sídlo	k1gNnSc4	sídlo
Národního	národní	k2eAgInSc2d1	národní
institutu	institut	k1gInSc2	institut
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgMnPc4	který
sídlila	sídlit	k5eAaImAgFnS	sídlit
Židovská	židovský	k2eAgFnSc1d1	židovská
agentura	agentura	k1gFnSc1	agentura
a	a	k8xC	a
Židovská	židovský	k2eAgFnSc1d1	židovská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
Vaad	Vaad	k1gInSc1	Vaad
Leumi	Leu	k1gFnPc7	Leu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Skopus	Skopus	k1gInSc4	Skopus
otevřena	otevřen	k2eAgFnSc1d1	otevřena
Hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
také	také	k9	také
založeny	založit	k5eAaPmNgFnP	založit
nové	nový	k2eAgFnPc1d1	nová
čtvrti	čtvrt	k1gFnPc1	čtvrt
mimo	mimo	k7c4	mimo
hradby	hradba	k1gFnPc4	hradba
města	město	k1gNnSc2	město
–	–	k?	–
jak	jak	k8xS	jak
židovské	židovský	k2eAgFnPc1d1	židovská
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
arabské	arabský	k2eAgInPc1d1	arabský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
zostřoval	zostřovat	k5eAaImAgInS	zostřovat
etnický	etnický	k2eAgInSc1d1	etnický
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
Araby	Arab	k1gMnPc7	Arab
a	a	k8xC	a
Židy	Žid	k1gMnPc7	Žid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
v	v	k7c6	v
Palestině	Palestina	k1gFnSc6	Palestina
násilné	násilný	k2eAgInPc1d1	násilný
nepokoje	nepokoj	k1gInPc1	nepokoj
s	s	k7c7	s
náboženským	náboženský	k2eAgInSc7d1	náboženský
podtextem	podtext	k1gInSc7	podtext
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
silně	silně	k6eAd1	silně
projevovaly	projevovat	k5eAaImAgFnP	projevovat
i	i	k9	i
v	v	k7c6	v
Jeruzalému	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
konfrontaci	konfrontace	k1gFnSc4	konfrontace
přineslo	přinést	k5eAaPmAgNnS	přinést
Arabské	arabský	k2eAgNnSc1d1	arabské
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
letech	let	k1gInPc6	let
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
plánu	plán	k1gInSc2	plán
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
Palestiny	Palestina	k1gFnSc2	Palestina
bylo	být	k5eAaImAgNnS	být
předpokládáno	předpokládán	k2eAgNnSc1d1	předpokládáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Betlémem	Betlém	k1gInSc7	Betlém
bude	být	k5eAaImBp3nS	být
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
územím	území	k1gNnSc7	území
(	(	kIx(	(
<g/>
corpus	corpus	k1gInSc1	corpus
separatum	separatum	k1gNnSc1	separatum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
když	když	k8xS	když
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
První	první	k4xOgFnSc1	první
arabsko-izraelská	arabskozraelský	k2eAgFnSc1d1	arabsko-izraelská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
Válka	válka	k1gFnSc1	válka
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ignorovaly	ignorovat	k5eAaImAgFnP	ignorovat
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
tento	tento	k3xDgInSc4	tento
status	status	k1gInSc4	status
a	a	k8xC	a
snažily	snažit	k5eAaImAgFnP	snažit
se	se	k3xPyFc4	se
získat	získat	k5eAaPmF	získat
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Jeruzalémem	Jeruzalém	k1gInSc7	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Arabské	arabský	k2eAgFnPc1d1	arabská
jednotky	jednotka	k1gFnPc1	jednotka
zablokovaly	zablokovat	k5eAaPmAgFnP	zablokovat
židům	žid	k1gMnPc3	žid
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
a	a	k8xC	a
zastavily	zastavit	k5eAaPmAgFnP	zastavit
přívod	přívod	k1gInSc4	přívod
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
ozbrojené	ozbrojený	k2eAgInPc1d1	ozbrojený
konvoje	konvoj	k1gInPc1	konvoj
dokázaly	dokázat	k5eAaPmAgInP	dokázat
do	do	k7c2	do
města	město	k1gNnSc2	město
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
těžkých	těžký	k2eAgFnPc2d1	těžká
ztrát	ztráta	k1gFnPc2	ztráta
proniknout	proniknout	k5eAaPmF	proniknout
a	a	k8xC	a
přivézt	přivézt	k5eAaPmF	přivézt
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Britové	Brit	k1gMnPc1	Brit
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
odešli	odejít	k5eAaPmAgMnP	odejít
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
a	a	k8xC	a
Izrael	Izrael	k1gInSc1	Izrael
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
země	zem	k1gFnSc2	zem
Arabská	arabský	k2eAgFnSc1d1	arabská
legie	legie	k1gFnSc1	legie
a	a	k8xC	a
boje	boj	k1gInPc1	boj
se	se	k3xPyFc4	se
rozpoutaly	rozpoutat	k5eAaPmAgInP	rozpoutat
naplno	naplno	k6eAd1	naplno
<g/>
.	.	kIx.	.
</s>
<s>
Arabské	arabský	k2eAgFnPc1d1	arabská
legie	legie	k1gFnPc1	legie
již	již	k6eAd1	již
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
do	do	k7c2	do
města	město	k1gNnSc2	město
a	a	k8xC	a
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
až	až	k9	až
do	do	k7c2	do
židovské	židovský	k2eAgFnSc2d1	židovská
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
izraelské	izraelský	k2eAgFnSc2d1	izraelská
jednotky	jednotka	k1gFnSc2	jednotka
získaly	získat	k5eAaPmAgFnP	získat
území	území	k1gNnPc1	území
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
však	však	k9	však
nadlouho	nadlouho	k6eAd1	nadlouho
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
padla	padnout	k5eAaImAgFnS	padnout
židovská	židovský	k2eAgFnSc1d1	židovská
čtvrť	čtvrť	k1gFnSc1	čtvrť
opět	opět	k6eAd1	opět
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Arabů	Arab	k1gMnPc2	Arab
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
začínali	začínat	k5eAaImAgMnP	začínat
Izraelci	Izraelec	k1gMnPc1	Izraelec
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
prohrávat	prohrávat	k5eAaImF	prohrávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
učiněn	učinit	k5eAaPmNgInS	učinit
pokus	pokus	k1gInSc1	pokus
dobýt	dobýt	k5eAaPmF	dobýt
zpět	zpět	k6eAd1	zpět
Staré	Staré	k2eAgNnSc4d1	Staré
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konečném	konečný	k2eAgNnSc6d1	konečné
uzavření	uzavření	k1gNnSc6	uzavření
příměří	příměří	k1gNnSc2	příměří
tak	tak	k9	tak
Izraelcům	Izraelec	k1gMnPc3	Izraelec
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jen	jen	k6eAd1	jen
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1949	[number]	k4	1949
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
přesídlil	přesídlit	k5eAaPmAgInS	přesídlit
Kneset	Kneset	k1gInSc1	Kneset
<g/>
.	.	kIx.	.
</s>
<s>
Přesunout	přesunout	k5eAaPmF	přesunout
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
také	také	k9	také
Hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1949	[number]	k4	1949
pak	pak	k6eAd1	pak
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
David	David	k1gMnSc1	David
Ben	Ben	k1gInSc4	Ben
Gurion	Gurion	k1gInSc1	Gurion
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Státu	stát	k1gInSc2	stát
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1967	[number]	k4	1967
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
Šestidenní	šestidenní	k2eAgFnSc1d1	šestidenní
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jordánské	jordánský	k2eAgNnSc1d1	Jordánské
vojsko	vojsko	k1gNnSc1	vojsko
ostřelovalo	ostřelovat	k5eAaImAgNnS	ostřelovat
židovské	židovský	k2eAgNnSc1d1	Židovské
město	město	k1gNnSc1	město
a	a	k8xC	a
dobylo	dobýt	k5eAaPmAgNnS	dobýt
budovu	budova	k1gFnSc4	budova
velitelství	velitelství	k1gNnSc2	velitelství
sil	síla	k1gFnPc2	síla
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
Cahal	Cahal	k1gInSc1	Cahal
<g/>
)	)	kIx)	)
však	však	k9	však
odrazila	odrazit	k5eAaPmAgFnS	odrazit
jordánský	jordánský	k2eAgInSc4d1	jordánský
útok	útok	k1gInSc4	útok
a	a	k8xC	a
zahájila	zahájit	k5eAaPmAgFnS	zahájit
protiútok	protiútok	k1gInSc4	protiútok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
odřízla	odříznout	k5eAaPmAgFnS	odříznout
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
od	od	k7c2	od
Ramalláhu	Ramalláh	k1gInSc2	Ramalláh
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
Izraelci	Izraelec	k1gMnPc1	Izraelec
skrz	skrz	k7c4	skrz
Lví	lví	k2eAgFnSc4d1	lví
bránu	brána	k1gFnSc4	brána
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
Starého	Starého	k2eAgNnSc2d1	Starého
města	město	k1gNnSc2	město
a	a	k8xC	a
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
celý	celý	k2eAgInSc4d1	celý
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Palestinští	palestinský	k2eAgMnPc1d1	palestinský
obyvatelé	obyvatel	k1gMnPc1	obyvatel
získali	získat	k5eAaPmAgMnP	získat
trvalý	trvalý	k2eAgInSc4d1	trvalý
pobyt	pobyt	k1gInSc4	pobyt
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
občanství	občanství	k1gNnSc4	občanství
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnohá	mnohý	k2eAgNnPc4d1	mnohé
muzea	muzeum	k1gNnPc4	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
je	být	k5eAaImIp3nS	být
Izraelské	izraelský	k2eAgNnSc1d1	izraelské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velkou	velký	k2eAgFnSc4d1	velká
sbírku	sbírka	k1gFnSc4	sbírka
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
včetně	včetně	k7c2	včetně
svitků	svitek	k1gInPc2	svitek
od	od	k7c2	od
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Muzeem	muzeum	k1gNnSc7	muzeum
zaměřeným	zaměřený	k2eAgInPc3d1	zaměřený
speciálně	speciálně	k6eAd1	speciálně
na	na	k7c6	na
archeologii	archeologie	k1gFnSc6	archeologie
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
Rockefellerovo	Rockefellerův	k2eAgNnSc1d1	Rockefellerovo
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
je	být	k5eAaImIp3nS	být
zase	zase	k9	zase
vystaveno	vystaven	k2eAgNnSc1d1	vystaveno
v	v	k7c6	v
Tichově	Tichův	k2eAgInSc6d1	Tichův
domě	dům	k1gInSc6	dům
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
samotným	samotný	k2eAgMnPc3d1	samotný
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Davidově	Davidův	k2eAgFnSc6d1	Davidova
věži	věž	k1gFnSc6	věž
<g/>
.	.	kIx.	.
</s>
<s>
Jad	Jad	k?	Jad
vašem	váš	k3xOp2gInSc6	váš
je	být	k5eAaImIp3nS	být
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
památník	památník	k1gInSc1	památník
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
ještě	ještě	k9	ještě
Islámské	islámský	k2eAgNnSc1d1	islámské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
profesionální	profesionální	k2eAgInPc4d1	profesionální
orchestry	orchestr	k1gInPc4	orchestr
–	–	k?	–
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
filharmonie	filharmonie	k1gFnSc1	filharmonie
a	a	k8xC	a
Izraelská	izraelský	k2eAgFnSc1d1	izraelská
Camerata	Camerata	k1gFnSc1	Camerata
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
taky	taky	k6eAd1	taky
množství	množství	k1gNnSc4	množství
divadel	divadlo	k1gNnPc2	divadlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jerar	Jerar	k1gMnSc1	Jerar
Bachar	Bachar	k1gMnSc1	Bachar
<g/>
,	,	kIx,	,
Bejt	Bejt	k?	Bejt
Šmuel	Šmuel	k1gInSc1	Šmuel
či	či	k8xC	či
Divadlo	divadlo	k1gNnSc1	divadlo
Chan	Chana	k1gFnPc2	Chana
<g/>
)	)	kIx)	)
a	a	k8xC	a
kinosálů	kinosál	k1gInPc2	kinosál
–	–	k?	–
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
pořádány	pořádán	k2eAgInPc4d1	pořádán
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
filmové	filmový	k2eAgInPc4d1	filmový
festivaly	festival	k1gInPc4	festival
(	(	kIx(	(
<g/>
Jeruzalémský	jeruzalémský	k2eAgInSc1d1	jeruzalémský
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
událostí	událost	k1gFnSc7	událost
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
Izraelský	izraelský	k2eAgInSc1d1	izraelský
festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c6	v
kulturním	kulturní	k2eAgInSc6d1	kulturní
komplexu	komplex	k1gInSc6	komplex
nazvaném	nazvaný	k2eAgInSc6d1	nazvaný
Jeruzalémské	jeruzalémský	k2eAgNnSc4d1	Jeruzalémské
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
sálů	sál	k1gInPc2	sál
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
celoročně	celoročně	k6eAd1	celoročně
konají	konat	k5eAaImIp3nP	konat
kulturní	kulturní	k2eAgFnPc4d1	kulturní
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
vzdělávacím	vzdělávací	k2eAgNnSc7d1	vzdělávací
střediskem	středisko	k1gNnSc7	středisko
je	být	k5eAaImIp3nS	být
Hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
kampusu	kampus	k1gInSc6	kampus
na	na	k7c6	na
Giv	Giv	k1gFnSc6	Giv
<g/>
'	'	kIx"	'
<g/>
at	at	k?	at
Ram	Ram	k1gFnSc1	Ram
stojí	stát	k5eAaImIp3nS	stát
Židovská	židovský	k2eAgFnSc1d1	židovská
národní	národní	k2eAgFnSc1d1	národní
a	a	k8xC	a
univerzitní	univerzitní	k2eAgFnSc1d1	univerzitní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
Judských	judský	k2eAgFnPc2d1	Judská
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
Olivový	olivový	k2eAgInSc4d1	olivový
vrch	vrch	k1gInSc4	vrch
(	(	kIx(	(
<g/>
východ	východ	k1gInSc4	východ
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrch	vrch	k1gInSc1	vrch
Skopus	Skopus	k1gInSc1	Skopus
(	(	kIx(	(
<g/>
severovýchod	severovýchod	k1gInSc1	severovýchod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
760	[number]	k4	760
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Celý	celý	k2eAgInSc4d1	celý
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgMnS	obklopit
údolími	údolí	k1gNnPc7	údolí
a	a	k8xC	a
suchými	suchý	k2eAgNnPc7d1	suché
říčními	říční	k2eAgNnPc7d1	říční
koryty	koryto	k1gNnPc7	koryto
(	(	kIx(	(
<g/>
vádí	vádí	k1gNnSc1	vádí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
protínají	protínat	k5eAaImIp3nP	protínat
tři	tři	k4xCgNnPc1	tři
údolí	údolí	k1gNnPc1	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Kidronské	Kidronský	k2eAgNnSc1d1	Kidronský
údolí	údolí	k1gNnSc1	údolí
probíhá	probíhat	k5eAaImIp3nS	probíhat
východně	východně	k6eAd1	východně
od	od	k7c2	od
Starého	Starého	k2eAgNnSc2d1	Starého
města	město	k1gNnSc2	město
a	a	k8xC	a
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Olivovou	olivový	k2eAgFnSc4d1	olivová
horu	hora	k1gFnSc4	hora
od	od	k7c2	od
vlastního	vlastní	k2eAgNnSc2d1	vlastní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
starého	starý	k2eAgInSc2d1	starý
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
nachází	nacházet	k5eAaImIp3nS	nacházet
Hinonské	Hinonský	k2eAgNnSc4d1	Hinonský
údolí	údolí	k1gNnSc4	údolí
se	s	k7c7	s
strmými	strmý	k2eAgFnPc7d1	strmá
roklemi	rokle	k1gFnPc7	rokle
spojovanými	spojovaný	k2eAgFnPc7d1	spojovaná
s	s	k7c7	s
biblickou	biblický	k2eAgFnSc7d1	biblická
eschatologií	eschatologie	k1gFnSc7	eschatologie
konceptů	koncept	k1gInPc2	koncept
geheny	gehena	k1gFnSc2	gehena
nebo	nebo	k8xC	nebo
pekla	peklo	k1gNnSc2	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Tyropoeonské	Tyropoeonský	k2eAgNnSc1d1	Tyropoeonský
údolí	údolí	k1gNnSc1	údolí
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
poblíž	poblíž	k6eAd1	poblíž
Damašské	damašský	k2eAgFnSc2d1	damašská
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
směrem	směr	k1gInSc7	směr
jihojihovýchod	jihojihovýchod	k1gInSc1	jihojihovýchod
centrem	centrum	k1gNnSc7	centrum
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
k	k	k7c3	k
Siloamské	Siloamský	k2eAgFnSc3d1	Siloamský
nádrži	nádrž	k1gFnSc3	nádrž
a	a	k8xC	a
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
spodní	spodní	k2eAgFnSc4d1	spodní
část	část	k1gFnSc4	část
dvou	dva	k4xCgInPc2	dva
vrchů	vrch	k1gInPc2	vrch
<g/>
,	,	kIx,	,
Chrámové	chrámový	k2eAgFnPc1d1	chrámová
hory	hora	k1gFnPc1	hora
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
zbytku	zbytek	k1gInSc6	zbytek
města	město	k1gNnSc2	město
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
údolí	údolí	k1gNnSc4	údolí
skryto	skrýt	k5eAaPmNgNnS	skrýt
sutí	suť	k1gFnSc7	suť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
během	během	k7c2	během
století	století	k1gNnSc2	století
nahromadila	nahromadit	k5eAaPmAgFnS	nahromadit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc4	seznam
čtvrtí	čtvrtit	k5eAaImIp3nP	čtvrtit
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
celky	celek	k1gInPc4	celek
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
s	s	k7c7	s
tradici	tradice	k1gFnSc4	tradice
kontinuálního	kontinuální	k2eAgNnSc2d1	kontinuální
osídlení	osídlení	k1gNnSc2	osídlení
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
a	a	k8xC	a
nová	nový	k2eAgNnPc1d1	nové
předměstí	předměstí	k1gNnPc1	předměstí
vznikající	vznikající	k2eAgNnPc1d1	vznikající
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
další	další	k2eAgFnPc4d1	další
podčásti	podčást	k1gFnPc4	podčást
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Západní	západní	k2eAgInSc1d1	západní
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
a	a	k8xC	a
Východní	východní	k2eAgInSc1d1	východní
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInSc1d1	západní
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Izraele	Izrael	k1gInSc2	Izrael
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
městské	městský	k2eAgFnSc2d1	městská
čtvrtě	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
Západního	západní	k2eAgInSc2d1	západní
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
leží	ležet	k5eAaImIp3nS	ležet
při	při	k7c6	při
ulici	ulice	k1gFnSc6	ulice
Derech	Derech	k1gInSc1	Derech
Jafo	Jafo	k6eAd1	Jafo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Střed	střed	k1gInSc1	střed
Města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Lev	levit	k5eAaImRp2nS	levit
ha-Ir	ha-Ir	k1gInSc1	ha-Ir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
čtvrtě	čtvrt	k1gFnPc4	čtvrt
Západního	západní	k2eAgInSc2d1	západní
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
patří	patřit	k5eAaImIp3nS	patřit
ultraortodoxní	ultraortodoxní	k2eAgFnSc1d1	ultraortodoxní
Me	Me	k1gFnSc1	Me
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
Še	Še	k1gMnSc1	Še
<g/>
'	'	kIx"	'
<g/>
arim	arim	k1gMnSc1	arim
<g/>
,	,	kIx,	,
rušné	rušný	k2eAgFnPc4d1	rušná
a	a	k8xC	a
historické	historický	k2eAgFnPc4d1	historická
čtvrtě	čtvrt	k1gFnPc4	čtvrt
jako	jako	k8xS	jako
Machane	Machan	k1gMnSc5	Machan
Jehuda	Jehuda	k1gMnSc1	Jehuda
nebo	nebo	k8xC	nebo
Nachla	Nachla	k1gMnSc1	Nachla
<g/>
'	'	kIx"	'
<g/>
ot	ot	k1gMnSc1	ot
<g/>
,	,	kIx,	,
obytné	obytný	k2eAgInPc1d1	obytný
distrikty	distrikt	k1gInPc1	distrikt
Katamon	Katamona	k1gFnPc2	Katamona
<g/>
,	,	kIx,	,
Talpijot	Talpijota	k1gFnPc2	Talpijota
<g/>
,	,	kIx,	,
Rechavja	Rechavja	k1gFnSc1	Rechavja
nebo	nebo	k8xC	nebo
Romema	Romema	k1gFnSc1	Romema
<g/>
,	,	kIx,	,
vládní	vládní	k2eAgFnSc1d1	vládní
čtvrť	čtvrť	k1gFnSc1	čtvrť
Kirjat	Kirjat	k1gInSc1	Kirjat
<g />
.	.	kIx.	.
</s>
<s>
ha-Memšala	ha-Memšala	k1gFnSc1	ha-Memšala
a	a	k8xC	a
univerzitní	univerzitní	k2eAgFnSc1d1	univerzitní
Giv	Giv	k1gFnSc1	Giv
<g/>
'	'	kIx"	'
<g/>
at	at	k?	at
Ram	Ram	k1gFnPc4	Ram
<g/>
,	,	kIx,	,
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
jinonárodních	jinonárodní	k2eAgFnPc2d1	jinonárodní
osadnických	osadnický	k2eAgFnPc2d1	osadnická
aktivit	aktivita	k1gFnPc2	aktivita
jako	jako	k8xC	jako
Německá	německý	k2eAgFnSc1d1	německá
Kolonie	kolonie	k1gFnSc1	kolonie
<g/>
,	,	kIx,	,
Řecká	řecký	k2eAgFnSc1d1	řecká
Kolonie	kolonie	k1gFnSc1	kolonie
nebo	nebo	k8xC	nebo
Ruský	ruský	k2eAgInSc1d1	ruský
Dvůr	Dvůr	k1gInSc1	Dvůr
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
sídlištní	sídlištní	k2eAgNnSc4d1	sídlištní
předměstí	předměstí	k1gNnSc4	předměstí
Kirjat	Kirjat	k2eAgMnSc1d1	Kirjat
Menachem	Menach	k1gMnSc7	Menach
či	či	k8xC	či
Kirjat	Kirjat	k2eAgInSc1d1	Kirjat
ha-Jovel	ha-Jovel	k1gInSc1	ha-Jovel
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
zóny	zóna	k1gFnSc2	zóna
jako	jako	k8xS	jako
Har	Har	k1gFnSc2	Har
Chocvim	Chocvima	k1gFnPc2	Chocvima
nebo	nebo	k8xC	nebo
historické	historický	k2eAgFnSc2d1	historická
vesnice	vesnice	k1gFnSc2	vesnice
dodatečně	dodatečně	k6eAd1	dodatečně
integrované	integrovaný	k2eAgNnSc1d1	integrované
do	do	k7c2	do
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Ejn	Ejn	k1gFnSc1	Ejn
Kerem	Kerem	k1gInSc1	Kerem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc1d1	východní
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
severních	severní	k2eAgInPc6d1	severní
a	a	k8xC	a
jižních	jižní	k2eAgInPc6d1	jižní
okrajích	okraj	k1gInPc6	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
okupována	okupovat	k5eAaBmNgFnS	okupovat
Izraelem	Izrael	k1gInSc7	Izrael
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
a	a	k8xC	a
přičleněna	přičleněn	k2eAgFnSc1d1	přičleněna
do	do	k7c2	do
správních	správní	k2eAgFnPc2d1	správní
hranic	hranice	k1gFnPc2	hranice
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
jednak	jednak	k8xC	jednak
historická	historický	k2eAgNnPc1d1	historické
arabská	arabský	k2eAgNnPc1d1	arabské
předměstí	předměstí	k1gNnPc1	předměstí
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
jako	jako	k8xS	jako
Bab	baba	k1gFnPc2	baba
az-Zahra	az-Zahra	k1gFnSc1	az-Zahra
nebo	nebo	k8xC	nebo
Šejch	šejch	k1gMnSc1	šejch
Džarach	Džarach	k1gInSc4	Džarach
vyrůstající	vyrůstající	k2eAgInSc4d1	vyrůstající
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
srůstající	srůstající	k2eAgInSc1d1	srůstající
s	s	k7c7	s
Jeruzalémem	Jeruzalém	k1gInSc7	Jeruzalém
jako	jako	k8xC	jako
Ras	ras	k1gMnSc1	ras
al-Amud	al-Amud	k1gMnSc1	al-Amud
nebo	nebo	k8xC	nebo
Bejt	Bejt	k?	Bejt
Chanina	Chanina	k1gFnSc1	Chanina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Východního	východní	k2eAgInSc2d1	východní
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
leží	ležet	k5eAaImIp3nP	ležet
i	i	k9	i
četné	četný	k2eAgFnPc1d1	četná
židovské	židovský	k2eAgFnPc1d1	židovská
čtvrti	čtvrt	k1gFnPc1	čtvrt
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
pojaté	pojatý	k2eAgInPc1d1	pojatý
jako	jako	k8xS	jako
samostatné	samostatný	k2eAgInPc1d1	samostatný
sídlištní	sídlištní	k2eAgInPc1d1	sídlištní
celky	celek	k1gInPc1	celek
jako	jako	k8xS	jako
Har	Har	k1gFnSc1	Har
Choma	Choma	k1gFnSc1	Choma
<g/>
,	,	kIx,	,
Pisgat	Pisgat	k1gInSc1	Pisgat
Ze	z	k7c2	z
<g/>
'	'	kIx"	'
<g/>
ev	ev	k?	ev
nebo	nebo	k8xC	nebo
Ramot	Ramot	k1gInSc1	Ramot
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
přímo	přímo	k6eAd1	přímo
navazují	navazovat	k5eAaImIp3nP	navazovat
na	na	k7c4	na
židovskou	židovský	k2eAgFnSc4d1	židovská
zástavbu	zástavba	k1gFnSc4	zástavba
Západního	západní	k2eAgInSc2d1	západní
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
jako	jako	k8xS	jako
Ramat	Ramat	k2eAgInSc1d1	Ramat
Eškol	Eškol	k1gInSc1	Eškol
nebo	nebo	k8xC	nebo
Ma	Ma	k1gFnSc1	Ma
<g/>
'	'	kIx"	'
<g/>
alot	alot	k1gInSc1	alot
Dafna	Dafn	k1gInSc2	Dafn
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
izolované	izolovaný	k2eAgFnSc2d1	izolovaná
židovské	židovský	k2eAgFnSc2d1	židovská
enklávy	enkláva	k1gFnSc2	enkláva
umístěné	umístěný	k2eAgFnSc2d1	umístěná
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
většinově	většinově	k6eAd1	většinově
arabských	arabský	k2eAgFnPc6d1	arabská
čtvrtích	čtvrt	k1gFnPc6	čtvrt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Ma	Ma	k1gMnSc1	Ma
<g/>
'	'	kIx"	'
<g/>
ale	ale	k8xC	ale
ha-Zejtim	ha-Zejtim	k6eAd1	ha-Zejtim
v	v	k7c6	v
Ras	rasa	k1gFnPc2	rasa
al-Amud	al-Amudo	k1gNnPc2	al-Amudo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
geograficko-politického	geografickoolitický	k2eAgNnSc2d1	geograficko-politický
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
unikátem	unikát	k1gInSc7	unikát
čtvrť	čtvrť	k1gFnSc1	čtvrť
Talpijot	Talpijot	k1gMnSc1	Talpijot
Mizrach	Mizrach	k1gMnSc1	Mizrach
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neleží	ležet	k5eNaImIp3nS	ležet
ani	ani	k8xC	ani
v	v	k7c6	v
Západním	západní	k2eAgInSc6d1	západní
ani	ani	k8xC	ani
ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Jeruzalému	Jeruzalém	k1gInSc6	Jeruzalém
ale	ale	k8xC	ale
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1967	[number]	k4	1967
nárazníkovým	nárazníkový	k2eAgNnSc7d1	nárazníkové
pásmem	pásmo	k1gNnSc7	pásmo
a	a	k8xC	a
tedy	tedy	k9	tedy
zónou	zóna	k1gFnSc7	zóna
nikoho	nikdo	k3yNnSc2	nikdo
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
specifická	specifický	k2eAgFnSc1d1	specifická
je	být	k5eAaImIp3nS	být
lokalita	lokalita	k1gFnSc1	lokalita
hory	hora	k1gFnSc2	hora
Skopus	Skopus	k1gInSc1	Skopus
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sice	sice	k8xC	sice
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Jeruzalému	Jeruzalém	k1gInSc6	Jeruzalém
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1967	[number]	k4	1967
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
enklávou	enkláva	k1gFnSc7	enkláva
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
patřila	patřit	k5eAaImAgFnS	patřit
i	i	k9	i
sousední	sousední	k2eAgFnSc1d1	sousední
arabská	arabský	k2eAgFnSc1d1	arabská
vesnice	vesnice	k1gFnSc1	vesnice
Isavija	Isavija	k1gFnSc1	Isavija
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
biblických	biblický	k2eAgFnPc6d1	biblická
dobách	doba	k1gFnPc6	doba
byl	být	k5eAaImAgInS	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
obklopen	obklopen	k2eAgInSc1d1	obklopen
lesy	les	k1gInPc4	les
mandloní	mandloň	k1gFnPc2	mandloň
<g/>
,	,	kIx,	,
olivovníků	olivovník	k1gInPc2	olivovník
a	a	k8xC	a
borovic	borovice	k1gFnPc2	borovice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
století	století	k1gNnSc2	století
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
zanedbávání	zanedbávání	k1gNnPc2	zanedbávání
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
zničení	zničení	k1gNnSc3	zničení
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělci	zemědělec	k1gMnPc1	zemědělec
ze	z	k7c2	z
zdejší	zdejší	k2eAgFnSc2d1	zdejší
oblasti	oblast	k1gFnSc2	oblast
proto	proto	k8xC	proto
při	při	k7c6	při
svazích	svah	k1gInPc6	svah
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
kamenné	kamenný	k2eAgFnPc4d1	kamenná
terasy	terasa	k1gFnPc4	terasa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zadržují	zadržovat	k5eAaImIp3nP	zadržovat
půdu	půda	k1gFnSc4	půda
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
odnosu	odnos	k1gInSc3	odnos
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
zdejších	zdejší	k2eAgInPc2d1	zdejší
hlavních	hlavní	k2eAgInPc2d1	hlavní
problémů	problém	k1gInPc2	problém
též	též	k9	též
vždy	vždy	k6eAd1	vždy
bylo	být	k5eAaImAgNnS	být
zásobování	zásobování	k1gNnSc1	zásobování
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
složitá	složitý	k2eAgFnSc1d1	složitá
síť	síť	k1gFnSc1	síť
starověkých	starověký	k2eAgInPc2d1	starověký
akvaduktů	akvadukt	k1gInPc2	akvadukt
<g/>
,	,	kIx,	,
tunelů	tunel	k1gInPc2	tunel
<g/>
,	,	kIx,	,
nádrží	nádrž	k1gFnPc2	nádrž
a	a	k8xC	a
cisteren	cisterna	k1gFnPc2	cisterna
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
zalesňování	zalesňování	k1gNnSc4	zalesňování
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
Jeruzalémský	jeruzalémský	k2eAgInSc4d1	jeruzalémský
koridor	koridor	k1gInSc4	koridor
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
zalesněn	zalesněn	k2eAgInSc1d1	zalesněn
<g/>
.	.	kIx.	.
</s>
<s>
Procházejí	procházet	k5eAaImIp3nP	procházet
tudy	tudy	k6eAd1	tudy
významné	významný	k2eAgInPc1d1	významný
vodní	vodní	k2eAgInPc1d1	vodní
toky	tok	k1gInPc1	tok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
původ	původ	k1gInSc4	původ
na	na	k7c6	na
rozvodí	rozvodí	k1gNnSc6	rozvodí
ležícím	ležící	k2eAgNnSc6d1	ležící
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
jako	jako	k8xS	jako
potok	potok	k1gInSc4	potok
Sorek	Sorky	k1gFnPc2	Sorky
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sezónní	sezónní	k2eAgInPc4d1	sezónní
toky	tok	k1gInPc4	tok
(	(	kIx(	(
<g/>
vádí	vádí	k1gNnSc2	vádí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
60	[number]	k4	60
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc4	Aviv
a	a	k8xC	a
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
36	[number]	k4	36
kilometrů	kilometr	k1gInPc2	kilometr
východně	východně	k6eAd1	východně
Mrtvé	mrtvý	k2eAgNnSc4d1	mrtvé
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
nejníže	nízce	k6eAd3	nízce
položenou	položený	k2eAgFnSc7d1	položená
vodní	vodní	k2eAgFnSc7d1	vodní
plochou	plocha	k1gFnSc7	plocha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
sídelní	sídelní	k2eAgFnSc1d1	sídelní
aglomerace	aglomerace	k1gFnSc1	aglomerace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
politické	politický	k2eAgFnPc4d1	politická
hranice	hranice	k1gFnPc4	hranice
i	i	k9	i
na	na	k7c4	na
Západní	západní	k2eAgInSc4d1	západní
břeh	břeh	k1gInSc4	břeh
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
její	její	k3xOp3gFnPc4	její
součásti	součást	k1gFnPc4	součást
patří	patřit	k5eAaImIp3nS	patřit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Betlém	Betlém	k1gInSc1	Betlém
a	a	k8xC	a
Bejt	Bejt	k?	Bejt
Džala	Džala	k1gFnSc1	Džala
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Abu	Abu	k1gFnSc2	Abu
Dis	dis	k1gNnSc2	dis
a	a	k8xC	a
Ma	Ma	k1gFnSc2	Ma
<g/>
'	'	kIx"	'
<g/>
ale	ale	k8xC	ale
Adumim	Adumim	k1gInSc4	Adumim
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Mevaseret	Mevaseret	k1gMnSc1	Mevaseret
Cijon	Cijon	k1gMnSc1	Cijon
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Ramalláh	Ramalláha	k1gFnPc2	Ramalláha
nebo	nebo	k8xC	nebo
Giv	Giv	k1gFnPc2	Giv
<g/>
'	'	kIx"	'
<g/>
at	at	k?	at
Ze	z	k7c2	z
<g/>
'	'	kIx"	'
<g/>
ev.	ev.	k?	ev.
Pro	pro	k7c4	pro
město	město	k1gNnSc4	město
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
středozemní	středozemní	k2eAgNnSc1d1	středozemní
klima	klima	k1gNnSc1	klima
s	s	k7c7	s
teplými	teplé	k1gNnPc7	teplé
suchými	suchý	k2eAgNnPc7d1	suché
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
studenými	studený	k2eAgFnPc7d1	studená
deštivými	deštivý	k2eAgFnPc7d1	deštivá
zimami	zima	k1gFnPc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Sněhové	sněhový	k2eAgFnPc1d1	sněhová
srážky	srážka	k1gFnPc1	srážka
se	se	k3xPyFc4	se
během	během	k7c2	během
zimy	zima	k1gFnSc2	zima
obvykle	obvykle	k6eAd1	obvykle
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jednou	jednou	k6eAd1	jednou
či	či	k8xC	či
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
každé	každý	k3xTgInPc4	každý
tři	tři	k4xCgInPc4	tři
až	až	k9	až
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
zažívá	zažívat	k5eAaImIp3nS	zažívat
vydatné	vydatný	k2eAgInPc4d1	vydatný
návaly	nával	k1gInPc4	nával
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Nejchladnějším	chladný	k2eAgInSc7d3	nejchladnější
měsícem	měsíc	k1gInSc7	měsíc
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
teplotou	teplota	k1gFnSc7	teplota
8	[number]	k4	8
°	°	k?	°
<g/>
C	C	kA	C
leden	leden	k1gInSc4	leden
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nejteplejšími	teplý	k2eAgInPc7d3	nejteplejší
měsíci	měsíc	k1gInPc7	měsíc
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
teplotou	teplota	k1gFnSc7	teplota
23	[number]	k4	23
°	°	k?	°
<g/>
C	C	kA	C
červenec	červenec	k1gInSc4	červenec
a	a	k8xC	a
srpen	srpen	k1gInSc4	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
denními	denní	k2eAgFnPc7d1	denní
a	a	k8xC	a
nočními	noční	k2eAgFnPc7d1	noční
teplotami	teplota	k1gFnPc7	teplota
panují	panovat	k5eAaImIp3nP	panovat
díky	díky	k7c3	díky
nadmořské	nadmořský	k2eAgFnSc3d1	nadmořská
výšce	výška	k1gFnSc3	výška
významně	významně	k6eAd1	významně
rozdíly	rozdíl	k1gInPc1	rozdíl
a	a	k8xC	a
chladné	chladný	k2eAgFnPc1d1	chladná
noci	noc	k1gFnPc1	noc
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
590	[number]	k4	590
milimetrů	milimetr	k1gInPc2	milimetr
a	a	k8xC	a
nejčastější	častý	k2eAgInSc1d3	nejčastější
výskyt	výskyt	k1gInSc1	výskyt
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
říjnem	říjen	k1gInSc7	říjen
a	a	k8xC	a
květnem	květen	k1gInSc7	květen
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
znečištění	znečištění	k1gNnSc2	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
automobilovou	automobilový	k2eAgFnSc7d1	automobilová
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
zejména	zejména	k9	zejména
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
hlavních	hlavní	k2eAgFnPc2d1	hlavní
silnic	silnice	k1gFnPc2	silnice
nebylo	být	k5eNaImAgNnS	být
kapacitně	kapacitně	k6eAd1	kapacitně
stavěno	stavit	k5eAaImNgNnS	stavit
pro	pro	k7c4	pro
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc4d1	velký
objem	objem	k1gInSc4	objem
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
dopravním	dopravní	k2eAgFnPc3d1	dopravní
zácpám	zácpa	k1gFnPc3	zácpa
a	a	k8xC	a
většímu	veliký	k2eAgNnSc3d2	veliký
uvolňování	uvolňování	k1gNnSc3	uvolňování
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgNnSc2d1	uhelnatý
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
znečištění	znečištění	k1gNnSc1	znečištění
uvnitř	uvnitř	k7c2	uvnitř
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
řídké	řídký	k2eAgNnSc1d1	řídké
<g/>
,	,	kIx,	,
větrem	vítr	k1gInSc7	vítr
sem	sem	k6eAd1	sem
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
přeneseny	přenesen	k2eAgFnPc4d1	přenesena
emise	emise	k1gFnPc4	emise
z	z	k7c2	z
továren	továrna	k1gFnPc2	továrna
na	na	k7c6	na
izraelském	izraelský	k2eAgNnSc6d1	izraelské
středomořském	středomořský	k2eAgNnSc6d1	středomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2015	[number]	k4	2015
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
865	[number]	k4	865
700	[number]	k4	700
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
bylo	být	k5eAaImAgNnS	být
528	[number]	k4	528
700	[number]	k4	700
Židů	Žid	k1gMnPc2	Žid
(	(	kIx(	(
<g/>
cca	cca	kA	cca
61,1	[number]	k4	61,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Databáze	databáze	k1gFnPc1	databáze
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
Izraele	Izrael	k1gInSc2	Izrael
(	(	kIx(	(
<g/>
s	s	k7c7	s
poněkud	poněkud	k6eAd1	poněkud
jinou	jiný	k2eAgFnSc7d1	jiná
metodikou	metodika	k1gFnSc7	metodika
<g/>
)	)	kIx)	)
ovšem	ovšem	k9	ovšem
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnSc4d2	vyšší
populaci	populace	k1gFnSc4	populace
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
měl	mít	k5eAaImAgInS	mít
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
933	[number]	k4	933
113	[number]	k4	113
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
dochází	docházet	k5eAaImIp3nS	docházet
nicméně	nicméně	k8xC	nicméně
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
podílu	podíl	k1gInSc2	podíl
židovského	židovský	k2eAgNnSc2d1	Židovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
připisováno	připisovat	k5eAaImNgNnS	připisovat
jak	jak	k8xS	jak
vyšší	vysoký	k2eAgFnPc1d2	vyšší
porodnosti	porodnost	k1gFnPc1	porodnost
muslimského	muslimský	k2eAgNnSc2d1	muslimské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
odchodu	odchod	k1gInSc3	odchod
židovských	židovská	k1gFnPc2	židovská
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
rovněž	rovněž	k9	rovněž
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
32	[number]	k4	32
488	[number]	k4	488
obyvatel	obyvatel	k1gMnPc2	obyvatel
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
pouhých	pouhý	k2eAgNnPc2d1	pouhé
9	[number]	k4	9
%	%	kIx~	%
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
usadilo	usadit	k5eAaPmAgNnS	usadit
2850	[number]	k4	2850
nových	nový	k2eAgMnPc2d1	nový
imigrantů	imigrant	k1gMnPc2	imigrant
<g/>
,	,	kIx,	,
především	především	k9	především
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
zemí	zem	k1gFnPc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Migrační	migrační	k2eAgNnSc1d1	migrační
saldo	saldo	k1gNnSc1	saldo
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
záporné	záporný	k2eAgInPc4d1	záporný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odcházejí	odcházet	k5eAaImIp3nP	odcházet
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
počet	počet	k1gInSc4	počet
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přicházejí	přicházet	k5eAaImIp3nP	přicházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
například	například	k6eAd1	například
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
přistěhovalo	přistěhovat	k5eAaPmAgNnS	přistěhovat
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
o	o	k7c4	o
6	[number]	k4	6
tisíc	tisíc	k4xCgInPc2	tisíc
víc	hodně	k6eAd2	hodně
bylo	být	k5eAaImAgNnS	být
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
vystěhovali	vystěhovat	k5eAaPmAgMnP	vystěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
jeruzalémského	jeruzalémský	k2eAgNnSc2d1	Jeruzalémské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
kontinuálně	kontinuálně	k6eAd1	kontinuálně
rozrůstá	rozrůstat	k5eAaImIp3nS	rozrůstat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	díky	k7c3	díky
vysoké	vysoký	k2eAgFnSc3d1	vysoká
porodnosti	porodnost	k1gFnSc3	porodnost
v	v	k7c6	v
arabské	arabský	k2eAgFnSc6d1	arabská
a	a	k8xC	a
charedi	chared	k1gMnPc1	chared
komunitě	komunita	k1gFnSc3	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
hrubá	hrubý	k2eAgFnSc1d1	hrubá
míra	míra	k1gFnSc1	míra
porodnosti	porodnost	k1gFnSc2	porodnost
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
(	(	kIx(	(
<g/>
4,02	[number]	k4	4,02
‰	‰	k?	‰
<g/>
)	)	kIx)	)
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc6	Aviv
(	(	kIx(	(
<g/>
1,98	[number]	k4	1,98
‰	‰	k?	‰
<g/>
)	)	kIx)	)
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
celostátním	celostátní	k2eAgInSc7d1	celostátní
průměrem	průměr	k1gInSc7	průměr
2,9	[number]	k4	2,9
‰	‰	k?	‰
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikost	velikost	k1gFnSc1	velikost
jeruzalémské	jeruzalémský	k2eAgFnSc2d1	Jeruzalémská
domácnosti	domácnost	k1gFnSc2	domácnost
je	být	k5eAaImIp3nS	být
3,8	[number]	k4	3,8
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
relativních	relativní	k2eAgNnPc6d1	relativní
číslech	číslo	k1gNnPc6	číslo
k	k	k7c3	k
1,8	[number]	k4	1,8
<g/>
%	%	kIx~	%
populačnímu	populační	k2eAgInSc3d1	populační
růstu	růst	k1gInSc3	růst
(	(	kIx(	(
<g/>
13	[number]	k4	13
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
blízko	blízko	k6eAd1	blízko
celostátnímu	celostátní	k2eAgInSc3d1	celostátní
průměru	průměr	k1gInSc3	průměr
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
posunu	posun	k1gInSc3	posun
náboženské	náboženský	k2eAgFnSc2d1	náboženská
a	a	k8xC	a
etnické	etnický	k2eAgFnSc2d1	etnická
skladby	skladba	k1gFnSc2	skladba
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
31	[number]	k4	31
%	%	kIx~	%
židovské	židovský	k2eAgFnSc2d1	židovská
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
dětmi	dítě	k1gFnPc7	dítě
do	do	k7c2	do
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
arabské	arabský	k2eAgFnSc2d1	arabská
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
42	[number]	k4	42
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
existující	existující	k2eAgInSc1d1	existující
trend	trend	k1gInSc1	trend
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgInSc2	který
v	v	k7c6	v
uplynulých	uplynulý	k2eAgNnPc6d1	uplynulé
čtyřech	čtyři	k4xCgNnPc6	čtyři
desetiletích	desetiletí	k1gNnPc6	desetiletí
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
podílu	podíl	k1gInSc2	podíl
židovského	židovský	k2eAgNnSc2d1	Židovské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
měli	mít	k5eAaImAgMnP	mít
Židé	Žid	k1gMnPc1	Žid
74	[number]	k4	74
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
9	[number]	k4	9
%	%	kIx~	%
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
možné	možný	k2eAgInPc4d1	možný
faktory	faktor	k1gInPc4	faktor
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
tento	tento	k3xDgInSc4	tento
trend	trend	k1gInSc4	trend
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
vysoké	vysoký	k2eAgInPc4d1	vysoký
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
pracovních	pracovní	k2eAgFnPc2d1	pracovní
příležitostí	příležitost	k1gFnPc2	příležitost
a	a	k8xC	a
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc4d2	veliký
náboženský	náboženský	k2eAgInSc4d1	náboženský
charakter	charakter	k1gInSc4	charakter
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hledání	hledání	k1gNnSc2	hledání
levnějšího	levný	k2eAgNnSc2d2	levnější
a	a	k8xC	a
sekulárnějšího	sekulárný	k2eAgNnSc2d2	sekulárný
bydlení	bydlení	k1gNnSc2	bydlení
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
a	a	k8xC	a
měst	město	k1gNnPc2	město
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
charedim	charedim	k1gInSc1	charedim
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
strmě	strmě	k6eAd1	strmě
rostl	růst	k5eAaImAgInS	růst
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
navštěvovalo	navštěvovat	k5eAaImAgNnS	navštěvovat
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
150	[number]	k4	150
100	[number]	k4	100
školáků	školák	k1gMnPc2	školák
pouhých	pouhý	k2eAgInPc2d1	pouhý
40	[number]	k4	40
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
státem	stát	k1gInSc7	stát
provozované	provozovaný	k2eAgFnSc2d1	provozovaná
sekulární	sekulární	k2eAgFnSc2d1	sekulární
a	a	k8xC	a
národní	národní	k2eAgFnSc2d1	národní
náboženské	náboženský	k2eAgFnSc2d1	náboženská
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
60	[number]	k4	60
%	%	kIx~	%
navštěvovalo	navštěvovat	k5eAaImAgNnS	navštěvovat
charedi	chared	k1gMnPc1	chared
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
koreluje	korelovat	k5eAaImIp3nS	korelovat
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
počtem	počet	k1gInSc7	počet
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
charedi	chared	k1gMnPc1	chared
rodinách	rodina	k1gFnPc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
mnoho	mnoho	k4c1	mnoho
Izraelců	Izraelec	k1gMnPc2	Izraelec
vnímá	vnímat	k5eAaImIp3nS	vnímat
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
jako	jako	k8xC	jako
chudé	chudý	k2eAgNnSc1d1	chudé
a	a	k8xC	a
zanedbané	zanedbaný	k2eAgNnSc1d1	zanedbané
město	město	k1gNnSc1	město
zatažené	zatažený	k2eAgNnSc1d1	zatažené
do	do	k7c2	do
náboženských	náboženský	k2eAgInPc2d1	náboženský
a	a	k8xC	a
politických	politický	k2eAgInPc2d1	politický
tlaků	tlak	k1gInPc2	tlak
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
palestinské	palestinský	k2eAgMnPc4d1	palestinský
Araby	Arab	k1gMnPc4	Arab
je	být	k5eAaImIp3nS	být
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
magnetem	magnet	k1gInSc7	magnet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
pracovních	pracovní	k2eAgFnPc2d1	pracovní
příležitostí	příležitost	k1gFnPc2	příležitost
než	než	k8xS	než
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
město	město	k1gNnSc1	město
na	na	k7c6	na
Západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Pásmu	pásmo	k1gNnSc6	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
.	.	kIx.	.
</s>
<s>
Palestinští	palestinský	k2eAgMnPc1d1	palestinský
představitelé	představitel	k1gMnPc1	představitel
tak	tak	k9	tak
Araby	Arab	k1gMnPc7	Arab
povzbuzují	povzbuzovat	k5eAaImIp3nP	povzbuzovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zůstali	zůstat	k5eAaPmAgMnP	zůstat
a	a	k8xC	a
udrželi	udržet	k5eAaPmAgMnP	udržet
tak	tak	k6eAd1	tak
arabský	arabský	k2eAgInSc4d1	arabský
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Palestinští	palestinský	k2eAgMnPc1d1	palestinský
Arabové	Arab	k1gMnPc1	Arab
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
přitahováni	přitahovat	k5eAaImNgMnP	přitahovat
především	především	k6eAd1	především
pracovními	pracovní	k2eAgFnPc7d1	pracovní
příležitostmi	příležitost	k1gFnPc7	příležitost
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
péčí	péče	k1gFnSc7	péče
<g/>
,	,	kIx,	,
sociálním	sociální	k2eAgNnSc6d1	sociální
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
<g/>
,	,	kIx,	,
dalšími	další	k2eAgInPc7d1	další
benefity	benefit	k1gInPc7	benefit
a	a	k8xC	a
kvalitou	kvalita	k1gFnSc7	kvalita
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
Izrael	Izrael	k1gInSc1	Izrael
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
jeruzalémským	jeruzalémský	k2eAgMnPc3d1	jeruzalémský
občanům	občan	k1gMnPc3	občan
<g/>
.	.	kIx.	.
</s>
<s>
Arabským	arabský	k2eAgMnPc3d1	arabský
obyvatelům	obyvatel	k1gMnPc3	obyvatel
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
nemít	mít	k5eNaImF	mít
izraelské	izraelský	k2eAgNnSc4d1	izraelské
občanství	občanství	k1gNnSc4	občanství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
udělena	udělit	k5eAaPmNgFnS	udělit
izraelská	izraelský	k2eAgFnSc1d1	izraelská
identifikační	identifikační	k2eAgFnSc1d1	identifikační
karta	karta	k1gFnSc1	karta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	on	k3xPp3gInPc4	on
opravňuje	opravňovat	k5eAaImIp3nS	opravňovat
k	k	k7c3	k
relativně	relativně	k6eAd1	relativně
snadným	snadný	k2eAgInPc3d1	snadný
průchodům	průchod	k1gInPc3	průchod
kontrolními	kontrolní	k2eAgNnPc7d1	kontrolní
stanovišti	stanoviště	k1gNnPc7	stanoviště
a	a	k8xC	a
k	k	k7c3	k
cestování	cestování	k1gNnSc3	cestování
po	po	k7c6	po
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
značně	značně	k6eAd1	značně
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
hledání	hledání	k1gNnSc4	hledání
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
rovněž	rovněž	k9	rovněž
mají	mít	k5eAaImIp3nP	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
dotované	dotovaný	k2eAgNnSc4d1	dotované
zdravotnictví	zdravotnictví	k1gNnSc4	zdravotnictví
a	a	k8xC	a
sociální	sociální	k2eAgNnSc4d1	sociální
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Izrael	Izrael	k1gInSc1	Izrael
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
svým	svůj	k3xOyFgMnPc3	svůj
občanům	občan	k1gMnPc3	občan
<g/>
.	.	kIx.	.
</s>
<s>
Arabové	Arab	k1gMnPc1	Arab
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
mohou	moct	k5eAaImIp3nP	moct
též	též	k9	též
posílat	posílat	k5eAaImF	posílat
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
do	do	k7c2	do
izraelských	izraelský	k2eAgFnPc2d1	izraelská
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Demografie	demografie	k1gFnSc1	demografie
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
při	při	k7c6	při
sporu	spor	k1gInSc6	spor
o	o	k7c4	o
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
bylo	být	k5eAaImAgNnS	být
jeruzalémskou	jeruzalémský	k2eAgFnSc7d1	Jeruzalémská
správou	správa	k1gFnSc7	správa
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
navrženo	navržen	k2eAgNnSc1d1	navrženo
rozšířit	rozšířit	k5eAaPmF	rozšířit
městské	městský	k2eAgFnSc2d1	městská
hranice	hranice	k1gFnSc2	hranice
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
a	a	k8xC	a
inkorporovat	inkorporovat	k5eAaBmF	inkorporovat
tak	tak	k6eAd1	tak
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
židovské	židovský	k2eAgFnSc2d1	židovská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1949	[number]	k4	1949
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
první	první	k4xOgMnSc1	první
izraelský	izraelský	k2eAgMnSc1d1	izraelský
premiér	premiér	k1gMnSc1	premiér
David	David	k1gMnSc1	David
Ben	Ben	k1gInSc4	Ben
Gurion	Gurion	k1gInSc1	Gurion
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
všechny	všechen	k3xTgFnPc4	všechen
složky	složka	k1gFnPc4	složka
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
(	(	kIx(	(
<g/>
moc	moc	k6eAd1	moc
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
<g/>
,	,	kIx,	,
výkonná	výkonný	k2eAgFnSc1d1	výkonná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc1d1	soudní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
sídlí	sídlet	k5eAaImIp3nP	sídlet
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc6	Aviv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
tohoto	tento	k3xDgNnSc2	tento
prohlášení	prohlášení	k1gNnSc2	prohlášení
byl	být	k5eAaImAgInS	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
mezi	mezi	k7c4	mezi
Izrael	Izrael	k1gInSc4	Izrael
a	a	k8xC	a
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
izraelské	izraelský	k2eAgNnSc4d1	izraelské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
považován	považován	k2eAgMnSc1d1	považován
pouze	pouze	k6eAd1	pouze
západní	západní	k2eAgInSc4d1	západní
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
šestidenní	šestidenní	k2eAgFnSc6d1	šestidenní
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
však	však	k9	však
Izrael	Izrael	k1gInSc1	Izrael
anektoval	anektovat	k5eAaBmAgInS	anektovat
východní	východní	k2eAgInSc4d1	východní
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
a	a	k8xC	a
učinil	učinit	k5eAaImAgMnS	učinit
tak	tak	k6eAd1	tak
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
de	de	k?	de
facto	facto	k1gNnSc1	facto
součást	součást	k1gFnSc1	součást
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc1	status
"	"	kIx"	"
<g/>
celého	celý	k2eAgInSc2d1	celý
a	a	k8xC	a
sjednoceného	sjednocený	k2eAgInSc2d1	sjednocený
<g/>
"	"	kIx"	"
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
coby	coby	k?	coby
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
zakotvil	zakotvit	k5eAaPmAgInS	zakotvit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
zákonu	zákon	k1gInSc6	zákon
Základní	základní	k2eAgInSc1d1	základní
zákon	zákon	k1gInSc1	zákon
<g/>
:	:	kIx,	:
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Jeruzalémský	jeruzalémský	k2eAgInSc1d1	jeruzalémský
zákon	zákon	k1gInSc1	zákon
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc1	status
"	"	kIx"	"
<g/>
sjednoceného	sjednocený	k2eAgInSc2d1	sjednocený
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
izraelského	izraelský	k2eAgMnSc2d1	izraelský
"	"	kIx"	"
<g/>
věčného	věčné	k1gNnSc2	věčné
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
společenství	společenství	k1gNnSc2	společenství
kontroverzní	kontroverzní	k2eAgFnSc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
udržují	udržovat	k5eAaImIp3nP	udržovat
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
své	svůj	k3xOyFgInPc4	svůj
konzuláty	konzulát	k1gInPc4	konzulát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
ambasády	ambasáda	k1gFnPc1	ambasáda
umístěny	umístit	k5eAaPmNgFnP	umístit
mimo	mimo	k7c4	mimo
vlastní	vlastní	k2eAgNnSc4d1	vlastní
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc6	Aviv
<g/>
.	.	kIx.	.
</s>
<s>
Nezávazná	závazný	k2eNgFnSc1d1	nezávazná
rezoluce	rezoluce	k1gFnSc1	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
478	[number]	k4	478
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc4	měsíc
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
Jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
tento	tento	k3xDgInSc4	tento
základní	základní	k2eAgInSc4d1	základní
zákon	zákon	k1gInSc4	zákon
"	"	kIx"	"
<g/>
neplatným	platný	k2eNgInSc7d1	neplatný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
navíc	navíc	k6eAd1	navíc
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
okamžitě	okamžitě	k6eAd1	okamžitě
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
dále	daleko	k6eAd2	daleko
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
své	svůj	k3xOyFgInPc4	svůj
členské	členský	k2eAgInPc4d1	členský
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přesunuly	přesunout	k5eAaPmAgInP	přesunout
své	svůj	k3xOyFgFnPc4	svůj
ambasády	ambasáda	k1gFnPc4	ambasáda
z	z	k7c2	z
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
forma	forma	k1gFnSc1	forma
trestného	trestný	k2eAgNnSc2d1	trestné
opatření	opatření	k1gNnSc2	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
s	s	k7c7	s
ambasádami	ambasáda	k1gFnPc7	ambasáda
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
této	tento	k3xDgFnSc3	tento
rezoluci	rezoluce	k1gFnSc3	rezoluce
vyhověla	vyhovět	k5eAaPmAgFnS	vyhovět
a	a	k8xC	a
přesunula	přesunout	k5eAaPmAgFnS	přesunout
svá	svůj	k3xOyFgNnPc4	svůj
zastoupení	zastoupení	k1gNnPc4	zastoupení
do	do	k7c2	do
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
existovalo	existovat	k5eAaImAgNnS	existovat
mnoho	mnoho	k4c1	mnoho
ambasád	ambasáda	k1gFnPc2	ambasáda
již	již	k6eAd1	již
před	před	k7c7	před
přijetím	přijetí	k1gNnSc7	přijetí
rezoluce	rezoluce	k1gFnSc2	rezoluce
č.	č.	k?	č.
478	[number]	k4	478
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nenachází	nacházet	k5eNaImIp3nS	nacházet
žádná	žádný	k3yNgFnSc1	žádný
ambasáda	ambasáda	k1gFnSc1	ambasáda
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
existuje	existovat	k5eAaImIp3nS	existovat
ambasáda	ambasáda	k1gFnSc1	ambasáda
v	v	k7c4	v
Mevaseret	Mevaseret	k1gInSc4	Mevaseret
Cijon	Cijona	k1gFnPc2	Cijona
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc4	čtyři
konzuláty	konzulát	k1gInPc4	konzulát
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
americký	americký	k2eAgInSc1d1	americký
Kongres	kongres	k1gInSc1	kongres
schválil	schválit	k5eAaPmAgInS	schválit
přesun	přesun	k1gInSc4	přesun
americké	americký	k2eAgFnSc2d1	americká
ambasády	ambasáda	k1gFnSc2	ambasáda
z	z	k7c2	z
Tel	tel	kA	tel
Avivu	Aviva	k1gFnSc4	Aviva
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
přijetím	přijetí	k1gNnSc7	přijetí
takzvaného	takzvaný	k2eAgNnSc2d1	takzvané
Jerusalem	Jerusalem	k1gInSc4	Jerusalem
Embassy	Embassa	k1gFnPc1	Embassa
Act	Act	k1gFnSc2	Act
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
zákon	zákon	k1gInSc1	zákon
schválen	schválit	k5eAaPmNgInS	schválit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
platnost	platnost	k1gFnSc4	platnost
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
odsouvá	odsouvat	k5eAaImIp3nS	odsouvat
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
nevešel	vejít	k5eNaPmAgInS	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
a	a	k8xC	a
americká	americký	k2eAgFnSc1d1	americká
ambasáda	ambasáda	k1gFnSc1	ambasáda
je	být	k5eAaImIp3nS	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc6	Aviv
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
varoval	varovat	k5eAaImAgMnS	varovat
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
OSN	OSN	kA	OSN
Pan	Pan	k1gMnSc1	Pan
Ki-mun	Kiun	k1gMnSc1	Ki-mun
<g/>
,	,	kIx,	,
že	že	k8xS	že
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
míru	mír	k1gInSc6	mír
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
být	být	k5eAaImF	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Izraele	Izrael	k1gInSc2	Izrael
i	i	k8xC	i
uvažovaného	uvažovaný	k2eAgInSc2d1	uvažovaný
budoucího	budoucí	k2eAgInSc2d1	budoucí
palestinského	palestinský	k2eAgInSc2d1	palestinský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
státní	státní	k2eAgFnPc4d1	státní
instituce	instituce	k1gFnPc4	instituce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Knesetu	Kneset	k1gInSc2	Kneset
<g/>
,	,	kIx,	,
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
či	či	k8xC	či
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
rezidencí	rezidence	k1gFnPc2	rezidence
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
premiéra	premiér	k1gMnSc2	premiér
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
založením	založení	k1gNnSc7	založení
Izraele	Izrael	k1gInSc2	Izrael
sloužil	sloužit	k5eAaImAgMnS	sloužit
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
jako	jako	k8xC	jako
administrativní	administrativní	k2eAgNnSc4d1	administrativní
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
britského	britský	k2eAgInSc2d1	britský
mandátu	mandát	k1gInSc2	mandát
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
dnešní	dnešní	k2eAgInSc1d1	dnešní
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
Jordánsko	Jordánsko	k1gNnSc1	Jordánsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
až	až	k9	až
1967	[number]	k4	1967
byl	být	k5eAaImAgInS	být
západní	západní	k2eAgInSc1d1	západní
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
tak	tak	k6eAd1	tak
nebyl	být	k5eNaImAgInS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznáván	uznáván	k2eAgInSc1d1	uznáván
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
rezoluci	rezoluce	k1gFnSc3	rezoluce
valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
OSN	OSN	kA	OSN
č.	č.	k?	č.
149	[number]	k4	149
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
počítala	počítat	k5eAaImAgFnS	počítat
s	s	k7c7	s
Jeruzalémem	Jeruzalém	k1gInSc7	Jeruzalém
jako	jako	k8xC	jako
s	s	k7c7	s
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
městem	město	k1gNnSc7	město
(	(	kIx(	(
<g/>
corpus	corpus	k1gInSc1	corpus
separatum	separatum	k1gNnSc1	separatum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
celý	celý	k2eAgInSc1d1	celý
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
pod	pod	k7c4	pod
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1967	[number]	k4	1967
vláda	vláda	k1gFnSc1	vláda
Leviho	Levi	k1gMnSc2	Levi
Eškola	Eškola	k1gFnSc1	Eškola
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
izraelské	izraelský	k2eAgNnSc4d1	izraelské
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
jurisdikci	jurisdikce	k1gFnSc4	jurisdikce
na	na	k7c4	na
východní	východní	k2eAgInSc4d1	východní
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
správa	správa	k1gFnSc1	správa
Chrámové	chrámový	k2eAgFnSc2d1	chrámová
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
přenechána	přenechat	k5eAaPmNgFnS	přenechat
jordánskému	jordánský	k2eAgInSc3d1	jordánský
waqfu	waqf	k1gInSc3	waqf
pod	pod	k7c7	pod
jordánským	jordánský	k2eAgNnSc7d1	Jordánské
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
pro	pro	k7c4	pro
náboženské	náboženský	k2eAgFnPc4d1	náboženská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
Izrael	Izrael	k1gInSc1	Izrael
nařídil	nařídit	k5eAaPmAgInS	nařídit
z	z	k7c2	z
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
důvodů	důvod	k1gInPc2	důvod
uzavřít	uzavřít	k5eAaPmF	uzavřít
Orient	Orient	k1gInSc4	Orient
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
domovem	domov	k1gInSc7	domov
arabské	arabský	k2eAgFnPc1d1	arabská
studentské	studentský	k2eAgFnPc1d1	studentská
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
též	též	k9	též
velitelství	velitelství	k1gNnSc2	velitelství
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Palestiny	Palestina	k1gFnSc2	Palestina
(	(	kIx(	(
<g/>
OOP	OOP	kA	OOP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
jako	jako	k8xS	jako
palestinský	palestinský	k2eAgInSc4d1	palestinský
penzion	penzion	k1gInSc4	penzion
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Mírových	Mírův	k2eAgFnPc2d1	Mírova
dohod	dohoda	k1gFnPc2	dohoda
z	z	k7c2	z
Osla	Oslo	k1gNnSc2	Oslo
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
konečný	konečný	k2eAgInSc1d1	konečný
status	status	k1gInSc1	status
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
určen	určit	k5eAaPmNgInS	určit
jednáním	jednání	k1gNnSc7	jednání
s	s	k7c7	s
Palestinskou	palestinský	k2eAgFnSc7d1	palestinská
samosprávou	samospráva	k1gFnSc7	samospráva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
požaduje	požadovat	k5eAaImIp3nS	požadovat
východní	východní	k2eAgInSc4d1	východní
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
svého	svůj	k3xOyFgInSc2	svůj
budoucího	budoucí	k2eAgInSc2d1	budoucí
palestinského	palestinský	k2eAgInSc2d1	palestinský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
za	za	k7c4	za
budoucí	budoucí	k2eAgNnSc4d1	budoucí
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Izraele	Izrael	k1gInSc2	Izrael
i	i	k8xC	i
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ale	ale	k8xC	ale
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
rezoluci	rezoluce	k1gFnSc4	rezoluce
UNESCO	UNESCO	kA	UNESCO
o	o	k7c6	o
památkách	památka	k1gFnPc6	památka
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
označila	označit	k5eAaPmAgFnS	označit
město	město	k1gNnSc4	město
již	již	k6eAd1	již
za	za	k7c4	za
stávající	stávající	k2eAgNnSc4d1	stávající
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zasazovala	zasazovat	k5eAaImAgFnS	zasazovat
o	o	k7c4	o
respektování	respektování	k1gNnSc4	respektování
tohoto	tento	k3xDgInSc2	tento
statusu	status	k1gInSc2	status
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
je	být	k5eAaImIp3nS	být
posvátným	posvátný	k2eAgNnSc7d1	posvátné
místem	místo	k1gNnSc7	místo
tří	tři	k4xCgFnPc2	tři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
světových	světový	k2eAgFnPc2d1	světová
monoteistických	monoteistický	k2eAgFnPc2d1	monoteistická
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
:	:	kIx,	:
judaismu	judaismus	k1gInSc2	judaismus
<g/>
,	,	kIx,	,
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
statistické	statistický	k2eAgFnSc2d1	statistická
ročenky	ročenka	k1gFnSc2	ročenka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nachází	nacházet	k5eAaImIp3nS	nacházet
1204	[number]	k4	1204
synagog	synagoga	k1gFnPc2	synagoga
<g/>
,	,	kIx,	,
158	[number]	k4	158
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
73	[number]	k4	73
mešit	mešita	k1gFnPc2	mešita
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
snahám	snaha	k1gFnPc3	snaha
o	o	k7c4	o
pokojné	pokojný	k2eAgNnSc4d1	pokojné
mírové	mírový	k2eAgNnSc4d1	Mírové
náboženské	náboženský	k2eAgNnSc4d1	náboženské
soužití	soužití	k1gNnSc4	soužití
jsou	být	k5eAaImIp3nP	být
některá	některý	k3yIgNnPc4	některý
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Chrámová	chrámový	k2eAgFnSc1d1	chrámová
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
místem	místem	k6eAd1	místem
neustálého	neustálý	k2eAgNnSc2d1	neustálé
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
židy	žid	k1gMnPc4	žid
je	být	k5eAaImIp3nS	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
nejposvátnějším	posvátný	k2eAgNnSc7d3	nejposvátnější
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Hospodin	Hospodin	k1gMnSc1	Hospodin
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
vyvolil	vyvolit	k5eAaPmAgMnS	vyvolit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
přebýval	přebývat	k5eAaImAgMnS	přebývat
<g/>
.	.	kIx.	.
</s>
<s>
Zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
byl	být	k5eAaImAgInS	být
Židům	Žid	k1gMnPc3	Žid
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
již	již	k6eAd1	již
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
biblického	biblický	k2eAgInSc2d1	biblický
Izraele	Izrael	k1gInSc2	Izrael
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
král	král	k1gMnSc1	král
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýsadnější	výsadní	k2eAgNnSc1d3	výsadní
postavení	postavení	k1gNnSc1	postavení
má	mít	k5eAaImIp3nS	mít
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
rovněž	rovněž	k9	rovněž
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
stály	stát	k5eAaImAgInP	stát
dva	dva	k4xCgInPc1	dva
jeruzalémské	jeruzalémský	k2eAgInPc1d1	jeruzalémský
Chrámy	chrám	k1gInPc1	chrám
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Šalomounův	Šalomounův	k2eAgInSc1d1	Šalomounův
chrám	chrám	k1gInSc1	chrám
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
druhý	druhý	k4xOgInSc4	druhý
Chrám	chrám	k1gInSc4	chrám
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
významnosti	významnost	k1gFnSc6	významnost
města	město	k1gNnSc2	město
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
zmínka	zmínka	k1gFnSc1	zmínka
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jmenováno	jmenovat	k5eAaImNgNnS	jmenovat
celkem	celkem	k6eAd1	celkem
632	[number]	k4	632
<g/>
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
Nejsvětějším	nejsvětější	k2eAgNnSc7d1	nejsvětější
židovským	židovský	k2eAgNnSc7d1	Židovské
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
Západní	západní	k2eAgFnSc1d1	západní
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xC	jako
Zeď	zeď	k1gFnSc1	zeď
nářků	nářek	k1gInPc2	nářek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pozůstatkem	pozůstatek	k1gInSc7	pozůstatek
vnější	vnější	k2eAgFnSc2d1	vnější
hradby	hradba	k1gFnSc2	hradba
druhého	druhý	k4xOgInSc2	druhý
Chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
i	i	k9	i
stavbu	stavba	k1gFnSc4	stavba
synagog	synagoga	k1gFnPc2	synagoga
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
stavěny	stavit	k5eAaImNgInP	stavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
aron	aron	k1gInSc4	aron
ha-kodeš	haodat	k5eAaPmIp2nS	ha-kodat
(	(	kIx(	(
<g/>
schrána	schrána	k1gFnSc1	schrána
na	na	k7c4	na
Tóru	tóra	k1gFnSc4	tóra
<g/>
)	)	kIx)	)
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
městu	město	k1gNnSc3	město
(	(	kIx(	(
<g/>
u	u	k7c2	u
jeruzalémských	jeruzalémský	k2eAgFnPc2d1	Jeruzalémská
synagog	synagoga	k1gFnPc2	synagoga
pak	pak	k6eAd1	pak
má	mít	k5eAaImIp3nS	mít
směřovat	směřovat	k5eAaImF	směřovat
k	k	k7c3	k
Chrámové	chrámový	k2eAgFnSc3d1	chrámová
hoře	hora	k1gFnSc3	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Jeruzalému	Jeruzalém	k1gInSc3	Jeruzalém
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
recitují	recitovat	k5eAaImIp3nP	recitovat
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
předpisu	předpis	k1gInSc2	předpis
v	v	k7c6	v
Mišně	Mišn	k1gInSc6	Mišn
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
kodifikován	kodifikovat	k5eAaBmNgInS	kodifikovat
v	v	k7c6	v
kodexu	kodex	k1gInSc6	kodex
Šulchan	Šulchan	k1gMnSc1	Šulchan
aruch	aruch	k1gMnSc1	aruch
<g/>
,	,	kIx,	,
denní	denní	k2eAgFnPc4d1	denní
modlitby	modlitba	k1gFnPc4	modlitba
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
uctívá	uctívat	k5eAaImIp3nS	uctívat
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
nejenom	nejenom	k6eAd1	nejenom
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
starozákonní	starozákonní	k2eAgFnSc4d1	starozákonní
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
významnost	významnost	k1gFnSc4	významnost
v	v	k7c6	v
životě	život	k1gInSc6	život
Ježíše	Ježíš	k1gMnSc2	Ježíš
Nazaretského	nazaretský	k2eAgMnSc2d1	nazaretský
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
byl	být	k5eAaImAgMnS	být
Ježíš	Ježíš	k1gMnSc1	Ježíš
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
přinesen	přinesen	k2eAgInSc1d1	přinesen
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
narození	narození	k1gNnSc6	narození
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
jako	jako	k9	jako
dospělý	dospělý	k2eAgMnSc1d1	dospělý
<g/>
,	,	kIx,	,
vyčistil	vyčistit	k5eAaPmAgInS	vyčistit
druhý	druhý	k4xOgInSc1	druhý
Chrám	chrám	k1gInSc1	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
rovněž	rovněž	k9	rovněž
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
Ježíš	Ježíš	k1gMnSc1	Ježíš
podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
apoštoly	apoštol	k1gMnPc7	apoštol
poslední	poslední	k2eAgFnSc6d1	poslední
večeři	večeře	k1gFnSc6	večeře
(	(	kIx(	(
<g/>
též	též	k9	též
večeři	večeře	k1gFnSc4	večeře
Páně	páně	k2eAgFnSc4d1	páně
<g/>
)	)	kIx)	)
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Sijón	Sijón	k1gInSc4	Sijón
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
křesťanskými	křesťanský	k2eAgMnPc7d1	křesťanský
místy	místo	k1gNnPc7	místo
jsou	být	k5eAaImIp3nP	být
Via	via	k7c4	via
Dolorosa	Dolorosa	k1gFnSc1	Dolorosa
(	(	kIx(	(
<g/>
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
<g/>
)	)	kIx)	)
na	na	k7c4	na
Golgotu	Golgota	k1gFnSc4	Golgota
a	a	k8xC	a
Golgota	Golgota	k1gFnSc1	Golgota
samotná	samotný	k2eAgFnSc1d1	samotná
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
místem	místo	k1gNnSc7	místo
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
ukřižování	ukřižování	k1gNnSc2	ukřižování
<g/>
.	.	kIx.	.
</s>
<s>
Evangelium	evangelium	k1gNnSc1	evangelium
podle	podle	k7c2	podle
Jana	Jan	k1gMnSc2	Jan
popisuje	popisovat	k5eAaImIp3nS	popisovat
Golgotu	Golgota	k1gFnSc4	Golgota
jako	jako	k8xC	jako
místo	místo	k1gNnSc4	místo
"	"	kIx"	"
<g/>
blízko	blízko	k7c2	blízko
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
zatímco	zatímco	k8xS	zatímco
archeologická	archeologický	k2eAgFnSc1d1	archeologická
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Golgota	Golgota	k1gFnSc1	Golgota
mohla	moct	k5eAaImAgFnS	moct
nacházet	nacházet	k5eAaImF	nacházet
poblíž	poblíž	k7c2	poblíž
hradeb	hradba	k1gFnPc2	hradba
uvnitř	uvnitř	k7c2	uvnitř
dnešní	dnešní	k2eAgFnSc2d1	dnešní
hranice	hranice	k1gFnSc2	hranice
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
biblickou	biblický	k2eAgFnSc4d1	biblická
Golgotu	Golgota	k1gFnSc4	Golgota
je	být	k5eAaImIp3nS	být
však	však	k9	však
většinou	většina	k1gFnSc7	většina
křesťanů	křesťan	k1gMnPc2	křesťan
považováno	považován	k2eAgNnSc1d1	považováno
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
stojí	stát	k5eAaImIp3nS	stát
bazilika	bazilika	k1gFnSc1	bazilika
Svatého	svatý	k2eAgInSc2d1	svatý
hrobu	hrob	k1gInSc2	hrob
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
let	léto	k1gNnPc2	léto
křesťanským	křesťanský	k2eAgNnSc7d1	křesťanské
poutním	poutní	k2eAgNnSc7d1	poutní
místem	místo	k1gNnSc7	místo
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
třetí	třetí	k4xOgNnSc4	třetí
nejsvětější	nejsvětější	k2eAgNnSc4d1	nejsvětější
místo	místo	k1gNnSc4	místo
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
významnosti	významnost	k1gFnSc6	významnost
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeruzalémská	jeruzalémský	k2eAgFnSc1d1	Jeruzalémská
Chrámová	chrámový	k2eAgFnSc1d1	chrámová
hora	hora	k1gFnSc1	hora
byla	být	k5eAaImAgFnS	být
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgMnSc7	první
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
směrem	směr	k1gInSc7	směr
(	(	kIx(	(
<g/>
kibla	kibla	k6eAd1	kibla
<g/>
)	)	kIx)	)
směřovaly	směřovat	k5eAaImAgFnP	směřovat
muslimské	muslimský	k2eAgFnPc1d1	muslimská
modlitby	modlitba	k1gFnPc1	modlitba
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ص	ص	k?	ص
<g/>
,	,	kIx,	,
salát	salát	k1gInSc1	salát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mohamed	Mohamed	k1gMnSc1	Mohamed
však	však	k9	však
po	po	k7c6	po
roztržce	roztržka	k1gFnSc6	roztržka
s	s	k7c7	s
židy	žid	k1gMnPc7	žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
proroka	prorok	k1gMnSc4	prorok
<g/>
,	,	kIx,	,
změnil	změnit	k5eAaPmAgMnS	změnit
kiblu	kiblat	k5eAaPmIp1nS	kiblat
ke	k	k7c3	k
svatyni	svatyně	k1gFnSc3	svatyně
Ka	Ka	k1gFnSc2	Ka
<g/>
'	'	kIx"	'
<g/>
ba	ba	k9	ba
v	v	k7c6	v
Mekce	Mekka	k1gFnSc6	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
islám	islám	k1gInSc4	islám
má	mít	k5eAaImIp3nS	mít
nicméně	nicméně	k8xC	nicméně
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
význam	význam	k1gInSc4	význam
především	především	k9	především
díky	díky	k7c3	díky
Mohamedově	Mohamedův	k2eAgFnSc3d1	Mohamedova
noční	noční	k2eAgFnSc3d1	noční
cestě	cesta	k1gFnSc3	cesta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
620	[number]	k4	620
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Mohamed	Mohamed	k1gMnSc1	Mohamed
zázračně	zázračně	k6eAd1	zázračně
přenesl	přenést	k5eAaPmAgMnS	přenést
z	z	k7c2	z
Mekky	Mekka	k1gFnSc2	Mekka
na	na	k7c4	na
Chrámovou	chrámový	k2eAgFnSc4d1	chrámová
horu	hora	k1gFnSc4	hora
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
nebesa	nebesa	k1gNnPc4	nebesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
proroky	prorok	k1gMnPc7	prorok
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
v	v	k7c6	v
Koránu	korán	k1gInSc6	korán
samotném	samotný	k2eAgInSc6d1	samotný
není	být	k5eNaImIp3nS	být
o	o	k7c6	o
Jeruzalému	Jeruzalém	k1gInSc6	Jeruzalém
žádná	žádný	k3yNgFnSc1	žádný
zmínka	zmínka	k1gFnSc1	zmínka
<g/>
,	,	kIx,	,
přinejmenším	přinejmenším	k6eAd1	přinejmenším
ne	ne	k9	ne
výslovná	výslovný	k2eAgFnSc1d1	výslovná
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
muslimské	muslimský	k2eAgInPc1d1	muslimský
komentáře	komentář	k1gInPc1	komentář
však	však	k9	však
ztotožňují	ztotožňovat	k5eAaImIp3nP	ztotožňovat
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
měl	mít	k5eAaImAgMnS	mít
Mohamed	Mohamed	k1gMnSc1	Mohamed
vystoupit	vystoupit	k5eAaPmF	vystoupit
do	do	k7c2	do
sedmého	sedmý	k4xOgNnSc2	sedmý
nebe	nebe	k1gNnSc2	nebe
<g/>
,	,	kIx,	,
navštívit	navštívit	k5eAaPmF	navštívit
ráj	ráj	k1gInSc4	ráj
a	a	k8xC	a
peklo	peklo	k1gNnSc4	peklo
a	a	k8xC	a
hovořit	hovořit	k5eAaImF	hovořit
s	s	k7c7	s
Alláhem	Alláh	k1gMnSc7	Alláh
a	a	k8xC	a
proroky	prorok	k1gMnPc7	prorok
<g/>
,	,	kIx,	,
s	s	k7c7	s
Chrámovou	chrámový	k2eAgFnSc7d1	chrámová
horou	hora	k1gFnSc7	hora
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
první	první	k4xOgInSc1	první
verš	verš	k1gInSc1	verš
súry	súra	k1gFnSc2	súra
Noční	noční	k2eAgFnSc1d1	noční
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
Al-Isrá	Al-Isrý	k2eAgFnSc1d1	Al-Isrá
<g/>
́	́	k?	́
<g/>
)	)	kIx)	)
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
jako	jako	k9	jako
cíl	cíl	k1gInSc1	cíl
Mohamedovy	Mohamedův	k2eAgFnSc2d1	Mohamedova
cesty	cesta	k1gFnSc2	cesta
nejvzdálenější	vzdálený	k2eAgFnSc4d3	nejvzdálenější
mešitu	mešita	k1gFnSc4	mešita
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
mešitu	mešita	k1gFnSc4	mešita
al-Aksá	al-Aksat	k5eAaPmIp3nS	al-Aksat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
na	na	k7c4	na
Chrámové	chrámový	k2eAgNnSc4d1	chrámové
hoře	hoře	k1gNnSc4	hoře
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgFnPc1	dva
muslimské	muslimský	k2eAgFnPc1d1	muslimská
památky	památka	k1gFnPc1	památka
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
připomínají	připomínat	k5eAaImIp3nP	připomínat
<g/>
:	:	kIx,	:
mešita	mešita	k1gFnSc1	mešita
al-Aksá	al-Aksý	k2eAgFnSc1d1	al-Aksá
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
jméno	jméno	k1gNnSc4	jméno
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
muslimů	muslim	k1gMnPc2	muslim
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
v	v	k7c6	v
Koránu	korán	k1gInSc6	korán
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
ještě	ještě	k9	ještě
tato	tento	k3xDgFnSc1	tento
mešita	mešita	k1gFnSc1	mešita
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
Skalní	skalní	k2eAgInSc1d1	skalní
dóm	dóm	k1gInSc1	dóm
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stojí	stát	k5eAaImIp3nS	stát
nad	nad	k7c7	nad
základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
muslimové	muslim	k1gMnPc1	muslim
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
Mohamed	Mohamed	k1gMnSc1	Mohamed
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
nebesa	nebesa	k1gNnPc4	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
byla	být	k5eAaImAgFnS	být
jeruzalémská	jeruzalémský	k2eAgFnSc1d1	Jeruzalémská
ekonomika	ekonomika	k1gFnSc1	ekonomika
postavena	postaven	k2eAgFnSc1d1	postavena
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
věřících	věřící	k2eAgMnPc6d1	věřící
poutnících	poutník	k1gMnPc6	poutník
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
od	od	k7c2	od
hlavních	hlavní	k2eAgInPc2d1	hlavní
přístavů	přístav	k1gInPc2	přístav
v	v	k7c6	v
Jaffě	Jaffa	k1gFnSc6	Jaffa
a	a	k8xC	a
Gaze	Gaze	k1gFnSc6	Gaze
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalémské	jeruzalémský	k2eAgFnPc1d1	Jeruzalémská
religiózní	religiózní	k2eAgFnPc1d1	religiózní
památky	památka	k1gFnPc1	památka
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
i	i	k9	i
dnes	dnes	k6eAd1	dnes
nejnavštěvovanější	navštěvovaný	k2eAgFnSc1d3	nejnavštěvovanější
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
návštěvníky	návštěvník	k1gMnPc7	návštěvník
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
turistů	turist	k1gMnPc2	turist
navštíví	navštívit	k5eAaPmIp3nS	navštívit
Západní	západní	k2eAgFnSc1d1	západní
zeď	zeď	k1gFnSc1	zeď
a	a	k8xC	a
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
uplynulých	uplynulý	k2eAgNnPc6d1	uplynulé
zhruba	zhruba	k6eAd1	zhruba
padesáti	padesát	k4xCc6	padesát
letech	léto	k1gNnPc6	léto
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
stále	stále	k6eAd1	stále
jasnější	jasný	k2eAgFnSc1d2	jasnější
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
nemůže	moct	k5eNaImIp3nS	moct
stavět	stavět	k5eAaImF	stavět
svou	svůj	k3xOyFgFnSc4	svůj
ekonomiku	ekonomika	k1gFnSc4	ekonomika
jen	jen	k9	jen
na	na	k7c6	na
náboženském	náboženský	k2eAgInSc6d1	náboženský
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
náboženské	náboženský	k2eAgFnSc2d1	náboženská
významnosti	významnost	k1gFnSc2	významnost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
mnohé	mnohý	k2eAgFnPc1d1	mnohá
statistiky	statistika	k1gFnPc1	statistika
indikují	indikovat	k5eAaBmIp3nP	indikovat
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
růst	růst	k1gInSc4	růst
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
východní	východní	k2eAgInSc1d1	východní
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
zaostával	zaostávat	k5eAaImAgMnS	zaostávat
za	za	k7c7	za
západním	západní	k2eAgInSc7d1	západní
Jeruzalémem	Jeruzalém	k1gInSc7	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
domácností	domácnost	k1gFnPc2	domácnost
se	s	k7c7	s
zaměstnanými	zaměstnaný	k2eAgFnPc7d1	zaměstnaná
osobami	osoba	k1gFnPc7	osoba
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
vyšší	vysoký	k2eAgFnSc1d2	vyšší
u	u	k7c2	u
arabských	arabský	k2eAgFnPc2d1	arabská
domácností	domácnost	k1gFnPc2	domácnost
(	(	kIx(	(
<g/>
76,1	[number]	k4	76,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
než	než	k8xS	než
u	u	k7c2	u
židovských	židovský	k2eAgFnPc2d1	židovská
domácností	domácnost	k1gFnPc2	domácnost
(	(	kIx(	(
<g/>
66,8	[number]	k4	66,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
8,3	[number]	k4	8,3
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
celostátní	celostátní	k2eAgInSc1d1	celostátní
průměr	průměr	k1gInSc1	průměr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
činí	činit	k5eAaImIp3nS	činit
9	[number]	k4	9
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
uplynulých	uplynulý	k2eAgInPc6d1	uplynulý
letech	let	k1gInPc6	let
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
k	k	k7c3	k
dramatickému	dramatický	k2eAgInSc3d1	dramatický
nárůstu	nárůst	k1gInSc3	nárůst
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2001	[number]	k4	2001
a	a	k8xC	a
2007	[number]	k4	2007
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
žijící	žijící	k2eAgMnSc1d1	žijící
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
činila	činit	k5eAaImAgFnS	činit
průměrná	průměrný	k2eAgFnSc1d1	průměrná
měsíční	měsíční	k2eAgFnSc1d1	měsíční
mzda	mzda	k1gFnSc1	mzda
5940	[number]	k4	5940
šekelů	šekel	k1gInPc2	šekel
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
1410	[number]	k4	1410
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
1350	[number]	k4	1350
šekelů	šekel	k1gInPc2	šekel
méně	málo	k6eAd2	málo
než	než	k8xS	než
průměrná	průměrný	k2eAgFnSc1d1	průměrná
měsíční	měsíční	k2eAgFnSc1d1	měsíční
mzda	mzda	k1gFnSc1	mzda
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc6	Aviv
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
britského	britský	k2eAgInSc2d1	britský
Mandátu	mandát	k1gInSc2	mandát
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
všechny	všechen	k3xTgFnPc4	všechen
budovy	budova	k1gFnPc4	budova
postaveny	postaven	k2eAgFnPc4d1	postavena
z	z	k7c2	z
jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
kamene	kámen	k1gInSc2	kámen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
historický	historický	k2eAgInSc1d1	historický
a	a	k8xC	a
estetický	estetický	k2eAgInSc1d1	estetický
ráz	ráz	k1gInSc1	ráz
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dodržování	dodržování	k1gNnSc1	dodržování
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stále	stále	k6eAd1	stále
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
odrazuje	odrazovat	k5eAaImIp3nS	odrazovat
těžký	těžký	k2eAgInSc1d1	těžký
průmysl	průmysl	k1gInSc1	průmysl
od	od	k7c2	od
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
;	;	kIx,	;
pouze	pouze	k6eAd1	pouze
2,2	[number]	k4	2,2
%	%	kIx~	%
území	území	k1gNnSc6	území
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
má	mít	k5eAaImIp3nS	mít
funkční	funkční	k2eAgNnSc4d1	funkční
využití	využití	k1gNnSc4	využití
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
podíl	podíl	k1gInSc4	podíl
pozemků	pozemek	k1gInPc2	pozemek
s	s	k7c7	s
funkčním	funkční	k2eAgNnSc7d1	funkční
využitím	využití	k1gNnSc7	využití
pro	pro	k7c4	pro
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gMnSc6	Aviv
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
velký	velký	k2eAgMnSc1d1	velký
a	a	k8xC	a
v	v	k7c6	v
Haifě	Haifa	k1gFnSc6	Haifa
dokonce	dokonce	k9	dokonce
sedminásobně	sedminásobně	k6eAd1	sedminásobně
velký	velký	k2eAgInSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
8,5	[number]	k4	8,5
%	%	kIx~	%
pracovně	pracovně	k6eAd1	pracovně
činných	činný	k2eAgMnPc2d1	činný
obyvatel	obyvatel	k1gMnPc2	obyvatel
Jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
distriktu	distrikt	k1gInSc2	distrikt
je	být	k5eAaImIp3nS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
ve	v	k7c6	v
zpracovatelském	zpracovatelský	k2eAgInSc6d1	zpracovatelský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
celostátního	celostátní	k2eAgInSc2d1	celostátní
průměru	průměr	k1gInSc2	průměr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
činí	činit	k5eAaImIp3nS	činit
15,8	[number]	k4	15,8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
celostátní	celostátní	k2eAgInSc1d1	celostátní
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
překračován	překračovat	k5eAaImNgInS	překračovat
v	v	k7c6	v
zaměstnanosti	zaměstnanost	k1gFnSc6	zaměstnanost
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
(	(	kIx(	(
<g/>
17,9	[number]	k4	17,9
%	%	kIx~	%
oproti	oproti	k7c3	oproti
12,7	[number]	k4	12,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc4	zdravotnictví
a	a	k8xC	a
sociální	sociální	k2eAgFnSc4d1	sociální
péči	péče	k1gFnSc4	péče
(	(	kIx(	(
<g/>
12,6	[number]	k4	12,6
%	%	kIx~	%
oproti	oproti	k7c3	oproti
10,7	[number]	k4	10,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
(	(	kIx(	(
<g/>
6,4	[number]	k4	6,4
%	%	kIx~	%
oproti	oproti	k7c3	oproti
4,7	[number]	k4	4,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
veřejné	veřejný	k2eAgFnSc6d1	veřejná
správě	správa	k1gFnSc6	správa
(	(	kIx(	(
<g/>
8,2	[number]	k4	8,2
%	%	kIx~	%
oproti	oproti	k7c3	oproti
4,7	[number]	k4	4,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc1	Aviv
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
izraelským	izraelský	k2eAgInSc7d1	izraelský
finančním	finanční	k2eAgInSc7d1	finanční
centrem	centr	k1gInSc7	centr
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
hi-tech	hi	k1gInPc6	hi-t
společností	společnost	k1gFnPc2	společnost
se	se	k3xPyFc4	se
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
tvořily	tvořit	k5eAaImAgInP	tvořit
12	[number]	k4	12
tisíc	tisíc	k4xCgInSc1	tisíc
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
park	park	k1gInSc1	park
Har	Har	k1gFnSc2	Har
Chocvim	Chocvima	k1gFnPc2	Chocvima
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
některých	některý	k3yIgFnPc2	některý
hlavních	hlavní	k2eAgFnPc2d1	hlavní
izraelských	izraelský	k2eAgFnPc2d1	izraelská
korporací	korporace	k1gFnPc2	korporace
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
Intel	Intel	kA	Intel
<g/>
,	,	kIx,	,
Teva	Teva	k1gMnSc1	Teva
Pharmaceutical	Pharmaceutical	k1gMnSc1	Pharmaceutical
Industries	Industries	k1gMnSc1	Industries
<g/>
,	,	kIx,	,
Ophir	Ophir	k1gMnSc1	Ophir
Optronics	Optronics	k1gInSc1	Optronics
a	a	k8xC	a
ECI	ECI	kA	ECI
Telecom	Telecom	k1gInSc1	Telecom
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
expanze	expanze	k1gFnSc2	expanze
parku	park	k1gInSc2	park
představují	představovat	k5eAaImIp3nP	představovat
kapacity	kapacita	k1gFnPc4	kapacita
pro	pro	k7c4	pro
dalších	další	k2eAgNnPc2d1	další
sto	sto	k4xCgNnSc1	sto
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
požární	požární	k2eAgFnSc4d1	požární
stanici	stanice	k1gFnSc4	stanice
a	a	k8xC	a
školu	škola	k1gFnSc4	škola
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
530	[number]	k4	530
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Izraele	Izrael	k1gInSc2	Izrael
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
národní	národní	k2eAgFnSc1d1	národní
vláda	vláda	k1gFnSc1	vláda
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hráčem	hráč	k1gMnSc7	hráč
v	v	k7c6	v
jeruzalémské	jeruzalémský	k2eAgFnSc6d1	Jeruzalémská
ekonomice	ekonomika	k1gFnSc6	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
generuje	generovat	k5eAaImIp3nS	generovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
dotace	dotace	k1gFnPc4	dotace
a	a	k8xC	a
pobídky	pobídka	k1gFnPc4	pobídka
pro	pro	k7c4	pro
nové	nový	k2eAgFnPc4d1	nová
obchodní	obchodní	k2eAgFnPc4d1	obchodní
iniciativy	iniciativa	k1gFnPc4	iniciativa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
stojí	stát	k5eAaImIp3nP	stát
velké	velký	k2eAgInPc1d1	velký
nemocniční	nemocniční	k2eAgInPc1d1	nemocniční
komplexy	komplex	k1gInPc1	komplex
jako	jako	k8xS	jako
nemocnice	nemocnice	k1gFnPc1	nemocnice
Hadasa	Hadas	k1gMnSc2	Hadas
nebo	nebo	k8xC	nebo
nemocnice	nemocnice	k1gFnSc2	nemocnice
Ša	Ša	k1gMnPc2	Ša
<g/>
'	'	kIx"	'
<g/>
arej	arej	k1gMnSc1	arej
Cedek	Cedek	k1gMnSc1	Cedek
<g/>
.	.	kIx.	.
</s>
<s>
Převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
ultraortodoxní	ultraortodoxní	k2eAgFnSc4d1	ultraortodoxní
židovskou	židovský	k2eAgFnSc4d1	židovská
populaci	populace	k1gFnSc4	populace
slouží	sloužit	k5eAaImIp3nS	sloužit
nemocnice	nemocnice	k1gFnSc1	nemocnice
Bikur	Bikur	k1gMnSc1	Bikur
cholim	cholim	k1gMnSc1	cholim
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
napojen	napojen	k2eAgInSc1d1	napojen
na	na	k7c4	na
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc1	Aviv
-	-	kIx~	-
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
výchozí	výchozí	k2eAgFnSc7d1	výchozí
stanicí	stanice	k1gFnSc7	stanice
Jaffa	Jaffa	k1gFnSc1	Jaffa
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc1	Aviv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
tu	tu	k6eAd1	tu
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Gan	Gan	k1gFnSc2	Gan
ha-chajot	hahajota	k1gFnPc2	ha-chajota
ha-tanachi	haanach	k1gFnSc2	ha-tanach
a	a	k8xC	a
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
Jerušalajim	Jerušalajim	k1gMnSc1	Jerušalajim
Malcha	Malcha	k1gMnSc1	Malcha
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
koncová	koncový	k2eAgFnSc1d1	koncová
historická	historický	k2eAgFnSc1d1	historická
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Jerušalajim	Jerušalajima	k1gFnPc2	Jerušalajima
fungovala	fungovat	k5eAaImAgFnS	fungovat
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
pak	pak	k6eAd1	pak
po	po	k7c6	po
velké	velký	k2eAgFnSc6d1	velká
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
tratě	trať	k1gFnSc2	trať
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
obnovena	obnoven	k2eAgFnSc1d1	obnovena
a	a	k8xC	a
neslouží	sloužit	k5eNaImIp3nS	sloužit
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
výstavby	výstavba	k1gFnSc2	výstavba
je	být	k5eAaImIp3nS	být
i	i	k9	i
druhé	druhý	k4xOgNnSc4	druhý
vlakové	vlakový	k2eAgNnSc4d1	vlakové
spojení	spojení	k1gNnSc4	spojení
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
blíží	blížit	k5eAaImIp3nS	blížit
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc1	Aviv
-	-	kIx~	-
Modi	Modi	k1gNnSc1	Modi
<g/>
'	'	kIx"	'
<g/>
in	in	k?	in
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
tratě	trať	k1gFnSc2	trať
měla	mít	k5eAaImAgFnS	mít
zkrátit	zkrátit	k5eAaPmF	zkrátit
na	na	k7c4	na
28	[number]	k4	28
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
dokončení	dokončení	k1gNnSc1	dokončení
úseku	úsek	k1gInSc2	úsek
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
nebo	nebo	k8xC	nebo
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Zbývalo	zbývat	k5eAaImAgNnS	zbývat
dokončit	dokončit	k5eAaPmF	dokončit
úsek	úsek	k1gInSc4	úsek
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
22	[number]	k4	22
kilometrů	kilometr	k1gInPc2	kilometr
mezi	mezi	k7c7	mezi
soutěskou	soutěska	k1gFnSc7	soutěska
Ša	Ša	k1gFnSc1	Ša
<g/>
'	'	kIx"	'
<g/>
ar	ar	k1gInSc1	ar
ha-Gaj	ha-Gaj	k1gFnSc1	ha-Gaj
<g/>
,	,	kIx,	,
městem	město	k1gNnSc7	město
Mevaseret	Mevasereta	k1gFnPc2	Mevasereta
Cijon	Cijon	k1gInSc4	Cijon
a	a	k8xC	a
centrem	centrum	k1gNnSc7	centrum
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
(	(	kIx(	(
<g/>
kongresové	kongresový	k2eAgNnSc1d1	Kongresové
centrum	centrum	k1gNnSc1	centrum
Binjanej	Binjanej	k1gMnSc2	Binjanej
ha-Uma	ha-Um	k1gMnSc2	ha-Um
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Kirjat	Kirjat	k1gInSc4	Kirjat
ha-Memšala	ha-Memšal	k1gMnSc2	ha-Memšal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
podzemní	podzemní	k2eAgFnSc1d1	podzemní
konečná	konečný	k2eAgFnSc1d1	konečná
stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
80	[number]	k4	80
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
vysokorychlostní	vysokorychlostní	k2eAgFnSc1d1	vysokorychlostní
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
je	být	k5eAaImIp3nS	být
výškově	výškově	k6eAd1	výškově
a	a	k8xC	a
technicky	technicky	k6eAd1	technicky
náročná	náročný	k2eAgFnSc1d1	náročná
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
z	z	k7c2	z
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
planiny	planina	k1gFnSc2	planina
vystoupat	vystoupat	k5eAaPmF	vystoupat
do	do	k7c2	do
Judských	judský	k2eAgInPc2d1	judský
hor.	hor.	k?	hor.
Spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vedly	vést	k5eAaImAgFnP	vést
ohledně	ohledně	k7c2	ohledně
ekologických	ekologický	k2eAgInPc2d1	ekologický
dopadů	dopad	k1gInPc2	dopad
a	a	k8xC	a
řešení	řešení	k1gNnSc4	řešení
některých	některý	k3yIgFnPc2	některý
přírodních	přírodní	k2eAgFnPc2d1	přírodní
překážek	překážka	k1gFnPc2	překážka
jako	jako	k8xS	jako
například	například	k6eAd1	například
překonání	překonání	k1gNnSc1	překonání
údolí	údolí	k1gNnSc2	údolí
vádí	vádí	k1gNnSc2	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Jitla	Jitla	k1gMnSc1	Jitla
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
čelí	čelit	k5eAaImIp3nS	čelit
i	i	k9	i
politickým	politický	k2eAgFnPc3d1	politická
námitkám	námitka	k1gFnPc3	námitka
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
totiž	totiž	k9	totiž
krátkým	krátký	k2eAgInSc7d1	krátký
úsekem	úsek	k1gInSc7	úsek
přes	přes	k7c4	přes
Latrunský	Latrunský	k2eAgInSc4d1	Latrunský
výběžek	výběžek	k1gInSc4	výběžek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
Jordánu	Jordán	k1gInSc2	Jordán
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
většinou	většina	k1gFnSc7	většina
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
společenství	společenství	k1gNnSc2	společenství
za	za	k7c4	za
okupované	okupovaný	k2eAgNnSc4d1	okupované
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
tohoto	tento	k3xDgInSc2	tento
úseku	úsek	k1gInSc2	úsek
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2011	[number]	k4	2011
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
německá	německý	k2eAgFnSc1d1	německá
státní	státní	k2eAgFnSc1d1	státní
firma	firma	k1gFnSc1	firma
Deutsche	Deutsch	k1gFnSc2	Deutsch
Bahn	Bahna	k1gFnPc2	Bahna
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
většinou	většinou	k6eAd1	většinou
probíhá	probíhat	k5eAaImIp3nS	probíhat
po	po	k7c6	po
komunikacích	komunikace	k1gFnPc6	komunikace
dálničního	dálniční	k2eAgInSc2d1	dálniční
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
východozápadní	východozápadní	k2eAgFnSc7d1	východozápadní
osou	osa	k1gFnSc7	osa
a	a	k8xC	a
spojnicí	spojnice	k1gFnSc7	spojnice
s	s	k7c7	s
Tel	tel	kA	tel
Avivem	Avivo	k1gNnSc7	Avivo
je	být	k5eAaImIp3nS	být
dálnice	dálnice	k1gFnSc1	dálnice
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
až	až	k9	až
k	k	k7c3	k
Mrtvému	mrtvý	k2eAgNnSc3d1	mrtvé
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
páteřní	páteřní	k2eAgFnSc4d1	páteřní
roli	role	k1gFnSc4	role
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
severojižním	severojižní	k2eAgInSc6d1	severojižní
směru	směr	k1gInSc6	směr
dálnice	dálnice	k1gFnSc2	dálnice
číslo	číslo	k1gNnSc1	číslo
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ale	ale	k9	ale
prochází	procházet	k5eAaImIp3nS	procházet
severně	severně	k6eAd1	severně
i	i	k9	i
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
územím	území	k1gNnSc7	území
Západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
Jordánu	Jordán	k1gInSc2	Jordán
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
masově	masově	k6eAd1	masově
využívaná	využívaný	k2eAgFnSc1d1	využívaná
<g/>
.	.	kIx.	.
</s>
<s>
Roli	role	k1gFnSc4	role
severojižního	severojižní	k2eAgInSc2d1	severojižní
průtahu	průtah	k1gInSc2	průtah
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vlastního	vlastní	k2eAgInSc2d1	vlastní
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
hraje	hrát	k5eAaImIp3nS	hrát
nová	nový	k2eAgFnSc1d1	nová
dálniční	dálniční	k2eAgFnSc1d1	dálniční
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
třídou	třída	k1gFnSc7	třída
Sderot	Sderot	k1gInSc4	Sderot
Menachem	Menach	k1gInSc7	Menach
Begin	Begin	k2eAgMnSc1d1	Begin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
hromadné	hromadný	k2eAgFnSc6d1	hromadná
i	i	k8xC	i
veřejné	veřejný	k2eAgFnSc6d1	veřejná
dálkové	dálkový	k2eAgFnSc6d1	dálková
dopravě	doprava	k1gFnSc6	doprava
dominují	dominovat	k5eAaImIp3nP	dominovat
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
soustředěné	soustředěný	k2eAgNnSc1d1	soustředěné
na	na	k7c4	na
Jeruzalémské	jeruzalémský	k2eAgNnSc4d1	Jeruzalémské
centrální	centrální	k2eAgNnSc4d1	centrální
autobusové	autobusový	k2eAgNnSc4d1	autobusové
nádraží	nádraží	k1gNnSc4	nádraží
(	(	kIx(	(
<g/>
autobusový	autobusový	k2eAgInSc4d1	autobusový
terminál	terminál	k1gInSc4	terminál
převážně	převážně	k6eAd1	převážně
pro	pro	k7c4	pro
arabskou	arabský	k2eAgFnSc4d1	arabská
populaci	populace	k1gFnSc4	populace
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
ve	v	k7c6	v
Východním	východní	k2eAgInSc6d1	východní
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
i	i	k9	i
Jeruzalémská	jeruzalémský	k2eAgFnSc1d1	Jeruzalémská
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
lince	linka	k1gFnSc6	linka
<g/>
.	.	kIx.	.
</s>
<s>
Letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
okrajová	okrajový	k2eAgFnSc1d1	okrajová
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
masové	masový	k2eAgFnSc6d1	masová
podobě	podoba	k1gFnSc6	podoba
odkázaná	odkázaný	k2eAgFnSc1d1	odkázaná
na	na	k7c4	na
Ben	Ben	k1gInSc4	Ben
Gurionovo	Gurionův	k2eAgNnSc4d1	Gurionovo
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
u	u	k7c2	u
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
zóny	zóna	k1gFnSc2	zóna
Atarot	Atarota	k1gFnPc2	Atarota
v	v	k7c6	v
nejsevernějším	severní	k2eAgInSc6d3	nejsevernější
výběžku	výběžek	k1gInSc6	výběžek
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
nachází	nacházet	k5eAaImIp3nS	nacházet
letiště	letiště	k1gNnSc1	letiště
Kalandia	Kalandium	k1gNnSc2	Kalandium
(	(	kIx(	(
<g/>
též	též	k6eAd1	též
zváno	zvát	k5eAaImNgNnS	zvát
letiště	letiště	k1gNnSc4	letiště
Atarot	Atarota	k1gFnPc2	Atarota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
zhoršení	zhoršení	k1gNnSc3	zhoršení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
druhé	druhý	k4xOgFnSc2	druhý
intifády	intifáda	k1gFnSc2	intifáda
zavřeno	zavřen	k2eAgNnSc1d1	zavřeno
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dva	dva	k4xCgInPc4	dva
nejpopulárnější	populární	k2eAgInPc4d3	nejpopulárnější
sporty	sport	k1gInPc4	sport
patří	patřit	k5eAaImIp3nP	patřit
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
fotbal	fotbal	k1gInSc4	fotbal
a	a	k8xC	a
basketbal	basketbal	k1gInSc4	basketbal
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
Bejtar	Bejtara	k1gFnPc2	Bejtara
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
fanoušky	fanoušek	k1gMnPc7	fanoušek
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
političtí	politický	k2eAgMnPc1d1	politický
představitelé	představitel	k1gMnPc1	představitel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zápasy	zápas	k1gInPc4	zápas
často	často	k6eAd1	často
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
velkým	velký	k2eAgInSc7d1	velký
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
klubem	klub	k1gInSc7	klub
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
rivaly	rival	k1gMnPc7	rival
Bejtaru	Bejtar	k1gInSc2	Bejtar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Hapoel	Hapoel	k1gMnSc1	Hapoel
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Bejtar	Bejtar	k1gMnSc1	Bejtar
byl	být	k5eAaImAgMnS	být
šampionem	šampion	k1gMnSc7	šampion
Izraelského	izraelský	k2eAgInSc2d1	izraelský
státního	státní	k2eAgInSc2d1	státní
poháru	pohár	k1gInSc2	pohár
(	(	kIx(	(
<g/>
Israel	Israel	k1gInSc1	Israel
State	status	k1gInSc5	status
Cup	cup	k1gInSc4	cup
<g/>
)	)	kIx)	)
sedmkrát	sedmkrát	k6eAd1	sedmkrát
<g/>
,	,	kIx,	,
Hapoel	Hapoel	k1gMnSc1	Hapoel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
šampionem	šampion	k1gMnSc7	šampion
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
lize	liga	k1gFnSc6	liga
pak	pak	k6eAd1	pak
Bejtar	Bejtar	k1gMnSc1	Bejtar
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Hapoel	Hapoel	k1gInSc1	Hapoel
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
</s>
<s>
Bejtar	Bejtar	k1gInSc1	Bejtar
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
nejprestižnější	prestižní	k2eAgFnSc6d3	nejprestižnější
izraelské	izraelský	k2eAgFnSc2d1	izraelská
Ligat	Ligat	k2eAgInSc4d1	Ligat
HaAl	HaAl	k1gInSc4	HaAl
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Hapoel	Hapoel	k1gInSc1	Hapoel
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
divizi	divize	k1gFnSc3	divize
Ligy	liga	k1gFnSc2	liga
Leumit	Leumita	k1gFnPc2	Leumita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
basketbalu	basketbal	k1gInSc6	basketbal
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
top	topit	k5eAaImRp2nS	topit
divizi	divize	k1gFnSc3	divize
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
Hapoel	Hapoel	k1gMnSc1	Hapoel
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
třikrát	třikrát	k6eAd1	třikrát
Státní	státní	k2eAgInSc1d1	státní
pohár	pohár	k1gInSc1	pohár
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
ULEB	ULEB	kA	ULEB
Cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
jeruzalémským	jeruzalémský	k2eAgInSc7d1	jeruzalémský
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
stadionem	stadion	k1gInSc7	stadion
je	být	k5eAaImIp3nS	být
již	již	k9	již
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
otevření	otevření	k1gNnSc2	otevření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
Stadion	stadion	k1gInSc4	stadion
Teddyho	Teddy	k1gMnSc2	Teddy
Kolleka	Kolleek	k1gMnSc2	Kolleek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
21	[number]	k4	21
600	[number]	k4	600
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Každoroční	každoroční	k2eAgFnSc7d1	každoroční
událostí	událost	k1gFnSc7	událost
je	být	k5eAaImIp3nS	být
Jeruzalémský	jeruzalémský	k2eAgInSc4d1	jeruzalémský
půlmaraton	půlmaraton	k1gInSc4	půlmaraton
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
běžci	běžec	k1gMnPc1	běžec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
závodí	závodit	k5eAaImIp3nS	závodit
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
kolem	kolem	k7c2	kolem
některých	některý	k3yIgInPc2	některý
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
jeruzalémských	jeruzalémský	k2eAgFnPc2d1	Jeruzalémská
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
21,1	[number]	k4	21,1
<g/>
kilometrového	kilometrový	k2eAgInSc2d1	kilometrový
půlmaratonu	půlmaraton	k1gInSc2	půlmaraton
mohou	moct	k5eAaImIp3nP	moct
běžci	běžec	k1gMnPc1	běžec
zvolit	zvolit	k5eAaPmF	zvolit
kratší	krátký	k2eAgInSc4d2	kratší
10	[number]	k4	10
<g/>
kilometrovou	kilometrový	k2eAgFnSc4d1	kilometrová
trasu	trasa	k1gFnSc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
trasy	trasa	k1gFnPc1	trasa
začínají	začínat	k5eAaImIp3nP	začínat
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Giv	Giv	k1gFnSc6	Giv
<g/>
'	'	kIx"	'
<g/>
at	at	k?	at
Ram	Ram	k1gFnSc1	Ram
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnPc1d1	americká
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Česko	Česko	k1gNnSc1	Česko
</s>
