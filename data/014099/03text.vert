<s>
Markka	Markka	k6eAd1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
Finská	finský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
norském	norský	k2eAgNnSc6d1
území	území	k1gNnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Finnmark	Finnmark	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Finská	finský	k2eAgFnSc1d1
markka	markka	k1gFnSc1
byla	být	k5eAaImAgFnS
měna	měna	k1gFnSc1
Finska	Finsko	k1gNnSc2
od	od	k7c2
roku	rok	k1gInSc2
1860	#num#	k4
až	až	k9
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1999	#num#	k4
<g/>
,	,	kIx,
resp.	resp.	kA
v	v	k7c6
praxi	praxe	k1gFnSc6
až	až	k9
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2002	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ji	on	k3xPp3gFnSc4
nahradilo	nahradit	k5eAaPmAgNnS
euro	euro	k1gNnSc1
(	(	kIx(
<g/>
€	€	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
měnový	měnový	k2eAgInSc1d1
kód	kód	k1gInSc1
byl	být	k5eAaImAgInS
FIM	FIM	kA
a	a	k8xC
obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
pro	pro	k7c4
vyjádření	vyjádření	k1gNnSc4
částek	částka	k1gFnPc2
v	v	k7c6
ní	on	k3xPp3gFnSc6
používala	používat	k5eAaImAgFnS
značka	značka	k1gFnSc1
mk	mk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělila	dělit	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c6
100	#num#	k4
penni	peneň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směnný	směnný	k2eAgInSc1d1
kurz	kurz	k1gInSc1
s	s	k7c7
eurem	euro	k1gNnSc7
činil	činit	k5eAaImAgInS
5,945	5,945	k4
<g/>
73	#num#	k4
marek	marka	k1gFnPc2
za	za	k7c4
1	#num#	k4
euro	euro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Jednomarkková	Jednomarkkový	k2eAgFnSc1d1
bankovka	bankovka	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1860	#num#	k4
</s>
<s>
Markka	Markka	k1gFnSc1
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1860	#num#	k4
jako	jako	k8xC,k8xS
čtvrtina	čtvrtina	k1gFnSc1
ruského	ruský	k2eAgInSc2d1
rublu	rubl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1865	#num#	k4
se	se	k3xPyFc4
markka	markka	k6eAd1
od	od	k7c2
rublu	rubl	k1gInSc2
oddělila	oddělit	k5eAaPmAgFnS
a	a	k8xC
svázala	svázat	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
hodnotu	hodnota	k1gFnSc4
se	s	k7c7
stříbrem	stříbro	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
Finsko	Finsko	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
roku	rok	k1gInSc2
1917	#num#	k4
nezávislost	nezávislost	k1gFnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
Finská	finský	k2eAgFnSc1d1
banka	banka	k1gFnSc1
a	a	k8xC
markka	markka	k1gFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
používat	používat	k5eAaImF
jako	jako	k9
nezávislá	závislý	k2eNgFnSc1d1
měna	měna	k1gFnSc1
krytá	krytý	k2eAgFnSc1d1
zlatem	zlato	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlatý	zlatý	k2eAgInSc1d1
standard	standard	k1gInSc1
byl	být	k5eAaImAgInS
zrušen	zrušen	k2eAgInSc1d1
roku	rok	k1gInSc2
1940	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
markka	markka	k6eAd1
během	během	k7c2
Zimní	zimní	k2eAgFnSc2d1
války	válka	k1gFnSc2
trpěla	trpět	k5eAaImAgFnS
vysokou	vysoký	k2eAgFnSc7d1
inflací	inflace	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
byla	být	k5eAaImAgFnS
původní	původní	k2eAgFnSc1d1
markka	markka	k1gFnSc1
nahrazena	nahradit	k5eAaPmNgFnS
novou	nový	k2eAgFnSc7d1
markkou	markka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
odpovídala	odpovídat	k5eAaImAgFnS
stu	sto	k4xCgNnSc3
dosavadních	dosavadní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
markka	markka	k6eAd1
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
na	na	k7c6
středověké	středověký	k2eAgFnSc6d1
jednotce	jednotka	k1gFnSc6
váhy	váha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
názvy	název	k1gInPc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
markka	markka	k6eAd1
<g/>
,	,	kIx,
tak	tak	k6eAd1
penni	penen	k2eAgMnPc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
vypůjčená	vypůjčený	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
z	z	k7c2
němčiny	němčina	k1gFnSc2
(	(	kIx(
<g/>
srovnej	srovnat	k5eAaPmRp2nS
s	s	k7c7
německou	německý	k2eAgFnSc7d1
markou	marka	k1gFnSc7
a	a	k8xC
pfennigem	pfennigo	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ačkoli	ačkoli	k8xS
slovo	slovo	k1gNnSc1
markka	markka	k6eAd1
je	být	k5eAaImIp3nS
o	o	k7c4
několik	několik	k4yIc4
století	století	k1gNnPc2
starší	starý	k2eAgFnSc1d2
než	než	k8xS
sama	sám	k3xTgFnSc1
měna	měna	k1gFnSc1
<g/>
,	,	kIx,
tato	tento	k3xDgFnSc1
měna	měna	k1gFnSc1
byla	být	k5eAaImAgFnS
ustanovena	ustanovit	k5eAaPmNgFnS
ještě	ještě	k9
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
byla	být	k5eAaImAgFnS
nazvána	nazván	k2eAgFnSc1d1
markka	markka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
název	název	k1gInSc4
měny	měna	k1gFnSc2
byla	být	k5eAaImAgFnS
vypsána	vypsán	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc4
další	další	k2eAgInPc1d1
navrhované	navrhovaný	k2eAgInPc1d1
názvy	název	k1gInPc1
byly	být	k5eAaImAgInP
sataikko	sataikko	k6eAd1
(	(	kIx(
<g/>
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
mající	mající	k2eAgMnPc4d1
sto	sto	k4xCgNnSc4
částí	část	k1gFnPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
omena	omena	k1gFnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
jablko	jablko	k1gNnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
nebo	nebo	k8xC
suomo	suomo	k6eAd1
(	(	kIx(
<g/>
od	od	k7c2
Suomi	Suo	k1gFnPc7
–	–	k?
„	„	k?
<g/>
Finsko	Finsko	k1gNnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
více	hodně	k6eAd2
než	než	k8xS
140	#num#	k4
<g/>
leté	letý	k2eAgFnSc2d1
historie	historie	k1gFnSc2
finské	finský	k2eAgFnSc2d1
markky	markka	k1gFnSc2
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
28	#num#	k4
různých	různý	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyobrazení	vyobrazení	k1gNnSc1
na	na	k7c6
nich	on	k3xPp3gFnPc6
se	se	k3xPyFc4
během	běh	k1gInSc7
let	léto	k1gNnPc2
měnila	měnit	k5eAaImAgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
zůstávala	zůstávat	k5eAaImAgFnS
zřetelně	zřetelně	k6eAd1
finská	finský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finská	finský	k2eAgFnSc1d1
markka	markka	k1gFnSc1
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
již	již	k9
historií	historie	k1gFnSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
Finsko	Finsko	k1gNnSc1
ji	on	k3xPp3gFnSc4
roku	rok	k1gInSc3
1999	#num#	k4
vyměnilo	vyměnit	k5eAaPmAgNnS
za	za	k7c4
společnou	společný	k2eAgFnSc4d1
evropskou	evropský	k2eAgFnSc4d1
měnu	měna	k1gFnSc4
Euro	euro	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markkové	Markkový	k2eAgFnPc4d1
mince	mince	k1gFnPc4
a	a	k8xC
bankovky	bankovka	k1gFnPc4
byly	být	k5eAaImAgFnP
v	v	k7c6
oběhu	oběh	k1gInSc6
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Portréty	portrét	k1gInPc1
na	na	k7c6
bankovkách	bankovka	k1gFnPc6
</s>
<s>
Tata	tata	k1gMnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
popisuje	popisovat	k5eAaImIp3nS
poslední	poslední	k2eAgFnSc4d1
sérii	série	k1gFnSc4
finské	finský	k2eAgFnSc2d1
markky	markka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
navržena	navržen	k2eAgFnSc1d1
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
finským	finský	k2eAgMnSc7d1
výtvarníkem	výtvarník	k1gMnSc7
Erikem	Erik	k1gMnSc7
Bruunem	Bruun	k1gMnSc7
a	a	k8xC
vydána	vydán	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
mk	mk	k?
(	(	kIx(
<g/>
modrá	modrý	k2eAgFnSc1d1
<g/>
)	)	kIx)
–	–	k?
Paavo	Paavo	k1gNnSc1
Nurmi	Nur	k1gFnPc7
(	(	kIx(
<g/>
1897	#num#	k4
<g/>
–	–	k?
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
atlet	atlet	k1gMnSc1
a	a	k8xC
olympijský	olympijský	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
(	(	kIx(
<g/>
bankovka	bankovka	k1gFnSc1
byla	být	k5eAaImAgFnS
stažena	stáhnout	k5eAaPmNgFnS
kvůli	kvůli	k7c3
zavedení	zavedení	k1gNnSc3
dvacetimarkkové	dvacetimarkkový	k2eAgFnSc2d1
bankovky	bankovka	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
20	#num#	k4
mk	mk	k?
(	(	kIx(
<g/>
modrozelená	modrozelený	k2eAgFnSc1d1
<g/>
)	)	kIx)
–	–	k?
Väinö	Väinö	k1gFnSc1
Linna	Linna	k1gFnSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
</s>
<s>
50	#num#	k4
mk	mk	k?
(	(	kIx(
<g/>
hnědá	hnědý	k2eAgFnSc1d1
<g/>
)	)	kIx)
–	–	k?
Alvar	Alvar	k1gInSc1
Aalto	Aalto	k1gNnSc1
(	(	kIx(
<g/>
1898	#num#	k4
<g/>
–	–	k?
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
architekt	architekt	k1gMnSc1
</s>
<s>
100	#num#	k4
mk	mk	k?
(	(	kIx(
<g/>
zelená	zelený	k2eAgFnSc1d1
<g/>
)	)	kIx)
–	–	k?
Jean	Jean	k1gMnSc1
Sibelius	Sibelius	k1gMnSc1
(	(	kIx(
<g/>
1865	#num#	k4
<g/>
-	-	kIx~
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
skladatel	skladatel	k1gMnSc1
</s>
<s>
500	#num#	k4
mk	mk	k?
(	(	kIx(
<g/>
červená	červený	k2eAgFnSc1d1
<g/>
)	)	kIx)
–	–	k?
Elias	Elias	k1gMnSc1
Lönnrot	Lönnrot	k1gMnSc1
(	(	kIx(
<g/>
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
1884	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
historik	historik	k1gMnSc1
</s>
<s>
1000	#num#	k4
mk	mk	k?
(	(	kIx(
<g/>
modropurpurová	modropurpurový	k2eAgFnSc1d1
<g/>
)	)	kIx)
–	–	k?
Anders	Anders	k1gInSc1
Chydenius	Chydenius	k1gInSc1
(	(	kIx(
<g/>
1729	#num#	k4
<g/>
-	-	kIx~
<g/>
1803	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
a	a	k8xC
státník	státník	k1gMnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
série	série	k1gFnSc1
měla	mít	k5eAaImAgFnS
také	také	k9
zahrnovat	zahrnovat	k5eAaImF
červenopurpurovou	červenopurpurový	k2eAgFnSc4d1
bankovku	bankovka	k1gFnSc4
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
5000	#num#	k4
mk	mk	k?
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
farář	farář	k1gMnSc1
a	a	k8xC
lingvista	lingvista	k1gMnSc1
Mikael	Mikael	k1gMnSc1
Agricola	Agricola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
ale	ale	k9
nebyla	být	k5eNaImAgFnS
nikdy	nikdy	k6eAd1
oficiálně	oficiálně	k6eAd1
dána	dát	k5eAaPmNgFnS
do	do	k7c2
oběhu	oběh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Starší	starý	k2eAgFnPc1d2
bankovky	bankovka	k1gFnPc1
</s>
<s>
Předposlední	předposlední	k2eAgFnSc1d1
série	série	k1gFnSc1
bankovek	bankovka	k1gFnPc2
byla	být	k5eAaImAgFnS
navržena	navržen	k2eAgFnSc1d1
Tapiem	Tapium	k1gNnSc7
Wirkkalou	Wirkkalý	k2eAgFnSc7d1
<g/>
,	,	kIx,
zavedena	zavést	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1955	#num#	k4
a	a	k8xC
přepracována	přepracován	k2eAgFnSc1d1
při	při	k7c6
reformě	reforma	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
první	první	k4xOgFnSc1
série	série	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
byly	být	k5eAaImAgFnP
vyobrazeny	vyobrazen	k2eAgFnPc1d1
konkrétní	konkrétní	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
byli	být	k5eAaImAgMnP
Juho	Juho	k1gMnSc1
Kusti	Kusť	k1gFnSc2
Paasikivi	Paasikiev	k1gFnSc6
na	na	k7c4
desetimarkkovce	desetimarkkovec	k1gMnSc4
<g/>
,	,	kIx,
J.	J.	kA
V.	V.	kA
Snellman	Snellman	k1gMnSc1
na	na	k7c6
stomarkkovce	stomarkkovka	k1gFnSc6
a	a	k8xC
Urho	Urho	k6eAd1
Kekkonen	Kekkonen	k1gInSc4
na	na	k7c6
pětisetmarkkovce	pětisetmarkkovka	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
do	do	k7c2
oběhu	oběh	k1gInSc2
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
pozdější	pozdní	k2eAgFnSc2d2
série	série	k1gFnSc2
Erika	Erika	k1gFnSc1
Bruuna	Bruuna	k1gFnSc1
na	na	k7c6
této	tento	k3xDgFnSc6
sérii	série	k1gFnSc6
nebyly	být	k5eNaImAgInP
zobrazeny	zobrazit	k5eAaPmNgInP
žádné	žádný	k3yNgInPc1
další	další	k2eAgInPc1d1
skutečné	skutečný	k2eAgInPc1d1
předměty	předmět	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
osobnostmi	osobnost	k1gFnPc7
zde	zde	k6eAd1
byly	být	k5eAaImAgInP
jen	jen	k9
abstraktní	abstraktní	k2eAgInPc1d1
ornamenty	ornament	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejším	tehdejší	k2eAgInSc7d1
oblíbeným	oblíbený	k2eAgInSc7d1
vtipem	vtip	k1gInSc7
bylo	být	k5eAaImAgNnS
přeložení	přeložení	k1gNnSc1
desetimarkkové	desetimarkkový	k2eAgFnSc2d1
bankovky	bankovka	k1gFnSc2
v	v	k7c6
místě	místo	k1gNnSc6
Paasikiviho	Paasikivi	k1gMnSc2
obličeje	obličej	k1gInSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
něco	něco	k3yInSc1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
připomínalo	připomínat	k5eAaImAgNnS
myš	myš	k1gFnSc4
a	a	k8xC
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
vydáváno	vydávat	k5eAaPmNgNnS,k5eAaImNgNnS
za	za	k7c4
jediné	jediný	k2eAgNnSc4d1
vyobrazení	vyobrazení	k1gNnSc4
zvířete	zvíře	k1gNnSc2
v	v	k7c6
celé	celý	k2eAgFnSc6d1
sérii	série	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Ještě	ještě	k6eAd1
starší	starý	k2eAgFnPc1d2
bankovky	bankovka	k1gFnPc1
<g/>
,	,	kIx,
navržené	navržený	k2eAgNnSc1d1
Elielem	Eliel	k1gMnSc7
Saarinenem	Saarinen	k1gMnSc7
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
zavedeny	zavést	k5eAaPmNgFnP
roku	rok	k1gInSc3
1922	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
na	na	k7c6
nich	on	k3xPp3gFnPc6
také	také	k9
vyobrazeni	vyobrazen	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
ti	ten	k3xDgMnPc1
ale	ale	k8xC
nepředstavovali	představovat	k5eNaImAgMnP
žádné	žádný	k3yNgFnPc4
konkrétní	konkrétní	k2eAgFnPc4d1
osobnosti	osobnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
to	ten	k3xDgNnSc1
byly	být	k5eAaImAgFnP
trochu	trochu	k6eAd1
kontroverzní	kontroverzní	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
<g/>
,	,	kIx,
protože	protože	k8xS
muži	muž	k1gMnPc1
a	a	k8xC
ženy	žena	k1gFnPc1
na	na	k7c6
nich	on	k3xPp3gMnPc6
byli	být	k5eAaImAgMnP
vyobrazeni	vyobrazit	k5eAaPmNgMnP
nazí	nahý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnSc1
</s>
<s>
Sada	sada	k1gFnSc1
mincí	mince	k1gFnPc2
finské	finský	k2eAgFnSc2d1
markky	markka	k1gFnSc2
</s>
<s>
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
sérii	série	k1gFnSc6
mincí	mince	k1gFnPc2
finské	finský	k2eAgFnSc2d1
markky	markka	k1gFnSc2
bylo	být	k5eAaImAgNnS
pět	pět	k4xCc1
mincí	mince	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
penni	peneň	k1gFnSc6
(	(	kIx(
<g/>
stříbrná	stříbrný	k2eAgFnSc1d1
barva	barva	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
lilie	lilie	k1gFnSc2
na	na	k7c4
lícu	líc	k2eAgFnSc4d1
a	a	k8xC
včelí	včelí	k2eAgFnSc4d1
plástev	plástev	k1gFnSc4
na	na	k7c6
rubu	rub	k1gInSc6
</s>
<s>
50	#num#	k4
penni	peneň	k1gFnSc6
(	(	kIx(
<g/>
stříbrná	stříbrný	k2eAgFnSc1d1
barva	barva	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
medvěd	medvěd	k1gMnSc1
na	na	k7c4
lícu	líca	k1gFnSc4
a	a	k8xC
ploník	ploník	k1gInSc4
obecný	obecný	k2eAgInSc4d1
(	(	kIx(
<g/>
druh	druh	k1gInSc4
mechu	mech	k1gInSc2
<g/>
)	)	kIx)
na	na	k7c6
rubu	rub	k1gInSc6
</s>
<s>
1	#num#	k4
markka	markka	k1gFnSc1
(	(	kIx(
<g/>
měděná	měděný	k2eAgFnSc1d1
barva	barva	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
finský	finský	k2eAgInSc4d1
státní	státní	k2eAgInSc4d1
znak	znak	k1gInSc4
na	na	k7c4
lícu	líca	k1gFnSc4
</s>
<s>
5	#num#	k4
marek	marka	k1gFnPc2
(	(	kIx(
<g/>
měděná	měděný	k2eAgFnSc1d1
barva	barva	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
tuleň	tuleň	k1gMnSc1
kroužkovaný	kroužkovaný	k2eAgMnSc1d1
saimaaský	saimaaský	k1gMnSc1
na	na	k7c4
lícu	líca	k1gFnSc4
a	a	k8xC
list	list	k1gInSc4
leknínu	leknín	k1gInSc2
s	s	k7c7
vážkou	vážka	k1gFnSc7
na	na	k7c6
rubu	rub	k1gInSc6
</s>
<s>
10	#num#	k4
marek	marka	k1gFnPc2
(	(	kIx(
<g/>
dvoukovová	dvoukovový	k2eAgFnSc1d1
mince	mince	k1gFnSc1
<g/>
,	,	kIx,
střed	střed	k1gInSc1
měděné	měděný	k2eAgFnSc2d1
a	a	k8xC
okraj	okraj	k1gInSc1
stříbrné	stříbrný	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
–	–	k?
tetřev	tetřev	k1gMnSc1
na	na	k7c6
lícu	lícus	k1gInSc6
a	a	k8xC
větve	větev	k1gFnPc1
jeřábu	jeřáb	k1gInSc2
a	a	k8xC
bobule	bobule	k1gFnSc2
na	na	k7c6
rubu	rub	k1gInSc6
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Finské	finský	k2eAgInPc1d1
euromince	eurominec	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Markka	Markek	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Finská	finský	k2eAgFnSc1d1
markka	markka	k1gFnSc1
–	–	k?
přehled	přehled	k1gInSc1
BBC	BBC	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Finská	finský	k2eAgFnSc1d1
markka	markka	k1gFnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Finské	finský	k2eAgFnSc2d1
banky	banka	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Obrázky	obrázek	k1gInPc1
finských	finský	k2eAgFnPc2d1
bankovek	bankovka	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
History	Histor	k1gInPc1
of	of	k?
the	the	k?
Finnish	Finnish	k1gMnSc1
Markka	Markka	k1gMnSc1
(	(	kIx(
<g/>
1860	#num#	k4
<g/>
-	-	kIx~
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
Historie	historie	k1gFnSc1
finské	finský	k2eAgFnSc2d1
markky	markka	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnPc1
nahrazené	nahrazený	k2eAgFnPc1d1
eurem	euro	k1gNnSc7
</s>
<s>
Belgický	belgický	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Estonská	estonský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Finská	finský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Francouzský	francouzský	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Irská	irský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Italská	italský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Kyperská	kyperský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Litevský	litevský	k2eAgInSc1d1
litas	litas	k1gInSc1
•	•	k?
Lotyšský	lotyšský	k2eAgInSc1d1
lat	lat	k1gInSc1
•	•	k?
Lucemburský	lucemburský	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Maltská	maltský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Monacký	monacký	k2eAgInSc1d1
frank	frank	k1gInSc1
•	•	k?
Německá	německý	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Nizozemský	nizozemský	k2eAgInSc1d1
gulden	gulden	k1gInSc1
•	•	k?
Portugalské	portugalský	k2eAgNnSc4d1
escudo	escudo	k1gNnSc4
•	•	k?
Rakouský	rakouský	k2eAgInSc1d1
šilink	šilink	k1gInSc1
•	•	k?
Řecká	řecký	k2eAgFnSc1d1
drachma	drachma	k1gFnSc1
•	•	k?
Sanmarinská	Sanmarinský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
•	•	k?
Slovenská	slovenský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Slovinský	slovinský	k2eAgInSc1d1
tolar	tolar	k1gInSc1
•	•	k?
Španělská	španělský	k2eAgFnSc1d1
peseta	peseta	k1gFnSc1
•	•	k?
Vatikánská	vatikánský	k2eAgFnSc1d1
lira	lira	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Finsko	Finsko	k1gNnSc1
</s>
