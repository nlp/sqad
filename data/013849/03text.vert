<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tarutina	Tarutin	k2eAgInSc2d1
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tarutina	Tarutin	k2eAgInSc2d1
</s>
<s>
konflikt	konflikt	k1gInSc1
<g/>
:	:	kIx,
Napoleonské	napoleonský	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tarutina	Tarutin	k1gMnSc2
<g/>
,	,	kIx,
od	od	k7c2
Pitera	Pitero	k1gNnSc2
von	von	k1gInSc4
Hesse	Hesse	k1gFnSc2
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1812	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Tarutino	Tarutin	k2eAgNnSc4d1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc4
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
vítězství	vítězství	k1gNnSc1
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Neapolské	neapolský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Generál	generál	k1gMnSc1
Benningsen	Benningsna	k1gFnPc2
</s>
<s>
Joachim	Joachim	k6eAd1
Murat	Murat	k2eAgInSc1d1
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
36	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
<g/>
;	;	kIx,
13	#num#	k4
000	#num#	k4
se	se	k3xPyFc4
účastnilo	účastnit	k5eAaImAgNnS
bitvy	bitva	k1gFnSc2
</s>
<s>
25	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
1200	#num#	k4
mužů	muž	k1gMnPc2
</s>
<s>
2500	#num#	k4
mrtvých	mrtvý	k2eAgMnPc2d1
a	a	k8xC
2000	#num#	k4
zajatých	zajatá	k1gFnPc2
<g/>
,	,	kIx,
38	#num#	k4
zajatých	zajatý	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tarutina	Tarutin	k1gMnSc2
byla	být	k5eAaImAgFnS
bitvou	bitva	k1gFnSc7
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
vítězstvím	vítězství	k1gNnSc7
ruské	ruský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
nad	nad	k7c7
francouzskými	francouzský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Nástin	nástin	k1gInSc1
událostí	událost	k1gFnPc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1812	#num#	k4
vpadla	vpadnout	k5eAaPmAgFnS
Napoleonova	Napoleonův	k2eAgFnSc1d1
půlmilionová	půlmilionový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
do	do	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
nemohla	moct	k5eNaImAgFnS
francouzský	francouzský	k2eAgInSc4d1
postup	postup	k1gInSc4
zastavit	zastavit	k5eAaPmF
a	a	k8xC
musela	muset	k5eAaImAgFnS
ustupovat	ustupovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nerozhodné	rozhodný	k2eNgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Borodina	Borodin	k2eAgInSc2d1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
<g/>
)	)	kIx)
Francouzi	Francouz	k1gMnPc1
dokonce	dokonce	k9
obsadili	obsadit	k5eAaPmAgMnP
Moskvu	Moskva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
stáhla	stáhnout	k5eAaPmAgFnS
do	do	k7c2
Tarutina	Tarutin	k1gMnSc2
a	a	k8xC
začala	začít	k5eAaPmAgFnS
zde	zde	k6eAd1
sbírat	sbírat	k5eAaImF
síly	síla	k1gFnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Napoleon	Napoleon	k1gMnSc1
zůstal	zůstat	k5eAaPmAgMnS
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
a	a	k8xC
snažil	snažit	k5eAaImAgMnS
se	se	k3xPyFc4
o	o	k7c4
uzavření	uzavření	k1gNnSc4
míru	mír	k1gInSc2
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
však	však	k9
Napoleon	Napoleon	k1gMnSc1
pochopil	pochopit	k5eAaPmAgMnS
nutnost	nutnost	k1gFnSc4
ústupu	ústup	k1gInSc2
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
trasu	trasa	k1gFnSc4
ústupu	ústup	k1gInSc2
si	se	k3xPyFc3
zvolil	zvolit	k5eAaPmAgMnS
jít	jít	k5eAaImF
na	na	k7c4
Kalugu	Kaluga	k1gFnSc4
a	a	k8xC
pak	pak	k6eAd1
na	na	k7c4
Smolensk	Smolensk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Směrem	směr	k1gInSc7
k	k	k7c3
Rusům	Rus	k1gMnPc3
vyslal	vyslat	k5eAaPmAgMnS
předvoj	předvoj	k1gInSc4
neapolského	neapolský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Joachima	Joachim	k1gMnSc2
Murata	Murat	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Murat	Murat	k1gInSc1
rozmístil	rozmístit	k5eAaPmAgInS
svých	svůj	k3xOyFgInPc4
25	#num#	k4
000	#num#	k4
mužů	muž	k1gMnPc2
v	v	k7c6
prostoru	prostor	k1gInSc6
mezi	mezi	k7c7
vesnicí	vesnice	k1gFnSc7
Tětěrinka	Tětěrinka	k1gFnSc1
a	a	k8xC
říčkou	říčka	k1gFnSc7
Černyšná	Černyšná	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Murat	Murat	k1gInSc1
se	se	k3xPyFc4
však	však	k9
dopustil	dopustit	k5eAaPmAgMnS
těžké	těžký	k2eAgFnPc4d1
chyby	chyba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nedalekém	daleký	k2eNgInSc6d1
lese	les	k1gInSc6
zapomněl	zapomenout	k5eAaPmAgMnS,k5eAaImAgMnS
rozestavět	rozestavět	k5eAaPmF
hlídky	hlídka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
neuniklo	uniknout	k5eNaPmAgNnS
ruskému	ruský	k2eAgInSc3d1
průzkumu	průzkum	k1gInSc3
a	a	k8xC
vrchní	vrchní	k2eAgMnSc1d1
ubytovatel	ubytovatel	k1gMnSc1
ruské	ruský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
Karel	Karel	k1gMnSc1
Toll	Toll	k1gMnSc1
vypracoval	vypracovat	k5eAaPmAgMnS
plán	plán	k1gInSc4
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
tím	ten	k3xDgNnSc7
však	však	k9
nesouhlasil	souhlasit	k5eNaImAgMnS
ruský	ruský	k2eAgMnSc1d1
vrchní	vrchní	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
<g/>
,	,	kIx,
maršál	maršál	k1gMnSc1
Kutuzov	Kutuzov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgMnSc7
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
střetu	střet	k1gInSc2
s	s	k7c7
jinými	jiný	k2eAgMnPc7d1
veliteli	velitel	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
s	s	k7c7
plánem	plán	k1gInSc7
souhlasili	souhlasit	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavu	hlava	k1gFnSc4
této	tento	k3xDgFnSc2
opozice	opozice	k1gFnSc2
tvořil	tvořit	k5eAaImAgMnS
generál	generál	k1gMnSc1
Benningsen	Benningsna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kutuzov	Kutuzov	k1gInSc1
nakonec	nakonec	k6eAd1
plán	plán	k1gInSc1
přijal	přijmout	k5eAaPmAgInS
<g/>
,	,	kIx,
ale	ale	k8xC
jeho	jeho	k3xOp3gNnSc4
provedení	provedení	k1gNnSc4
nechal	nechat	k5eAaPmAgMnS
na	na	k7c6
Benningsenovi	Benningsen	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
akce	akce	k1gFnSc1
nezačala	začít	k5eNaPmAgFnS
dobře	dobře	k6eAd1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
někteří	některý	k3yIgMnPc1
velitelé	velitel	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
měli	mít	k5eAaImAgMnP
účastnit	účastnit	k5eAaImF
<g/>
,	,	kIx,
dostali	dostat	k5eAaPmAgMnP
pokyny	pokyn	k1gInPc4
pozdě	pozdě	k6eAd1
a	a	k8xC
jiní	jiný	k1gMnPc1
je	on	k3xPp3gMnPc4
nechápali	chápat	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
postup	postup	k1gInSc1
nezačal	začít	k5eNaPmAgInS
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
večer	večer	k6eAd1
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochod	pochod	k1gInSc1
k	k	k7c3
Muratovi	Murat	k1gMnSc3
byl	být	k5eAaImAgInS
zmatený	zmatený	k2eAgInSc1d1
a	a	k8xC
pomalý	pomalý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
určených	určený	k2eAgInPc2d1
pět	pět	k4xCc4
hodin	hodina	k1gFnPc2
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
stála	stát	k5eAaImAgFnS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
místě	místo	k1gNnSc6
jen	jen	k9
kolona	kolon	k1gNnSc2
generálporučíka	generálporučík	k1gMnSc2
Baggovouta	Baggovout	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
v	v	k7c4
sedm	sedm	k4xCc4
hodin	hodina	k1gFnPc2
byli	být	k5eAaImAgMnP
Rusové	Rus	k1gMnPc1
připraveni	připravit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
Ruský	ruský	k2eAgInSc1d1
útok	útok	k1gInSc1
začal	začít	k5eAaPmAgInS
v	v	k7c4
sedm	sedm	k4xCc4
hodin	hodina	k1gFnPc2
a	a	k8xC
zastihl	zastihnout	k5eAaPmAgInS
Francouze	Francouz	k1gMnSc4
nepřipravené	připravený	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Murat	Murat	k1gInSc4
skočil	skočit	k5eAaPmAgMnS
do	do	k7c2
sedla	sedlo	k1gNnSc2
svého	svůj	k3xOyFgMnSc4
koně	kůň	k1gMnSc4
polooblečený	polooblečený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzi	Francouz	k1gMnPc7
nakonec	nakonec	k6eAd1
ustoupili	ustoupit	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
morálně	morálně	k6eAd1
posílila	posílit	k5eAaPmAgFnS
ruskou	ruský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
a	a	k8xC
urychlila	urychlit	k5eAaPmAgFnS
Napoleonův	Napoleonův	k2eAgInSc4d1
ústup	ústup	k1gInSc4
z	z	k7c2
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kovařík	Kovařík	k1gMnSc1
<g/>
:	:	kIx,
Napoleonova	Napoleonův	k2eAgNnSc2d1
tažení	tažení	k1gNnSc2
III	III	kA
<g/>
.	.	kIx.
–	–	k?
Proti	proti	k7c3
všem	všecek	k3xTgMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třebíč	Třebíč	k1gFnSc1
<g/>
:	:	kIx,
Akcent	akcent	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
593	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7268	#num#	k4
<g/>
-	-	kIx~
<g/>
296	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Bitvy	bitva	k1gFnPc1
napoleonských	napoleonský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Wertingen	Wertingen	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Elchingenu	Elchingen	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ulmu	Ulmus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Trafalgaru	Trafalgar	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Caldiera	Caldiero	k1gNnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Amstettenu	Amstetten	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dürnsteinu	Dürnstein	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Schöngrabernu	Schöngraberna	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Štoků	štok	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Maidy	Maida	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Saalfeldu	Saalfeld	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Jeny	jen	k1gInPc7
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Auerstedtu	Auerstedt	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Prenzlau	Prenzlaus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Pultuska	Pultuska	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Jílového	jílový	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Heilsbergu	Heilsberg	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Friedlandu	Friedland	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Medina	Medina	k1gFnSc1
del	del	k?
Rio	Rio	k1gMnSc1
Seco	Seco	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Bailénu	Bailén	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Somosierry	Somosierra	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
La	la	k1gNnSc2
Coruñ	Coruñ	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tarragony	Tarragona	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
u	u	k7c2
Eggmühlu	Eggmühl	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Aspern	Asperna	k1gFnPc2
a	a	k8xC
Esslingu	Essling	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Wagramu	Wagram	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Znojma	Znojmo	k1gNnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ciudad	Ciudad	k1gInSc4
Rodrigo	Rodrigo	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Salamancy	Salamanca	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ostrovna	Ostrovna	k1gFnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Smolensk	Smolensk	k1gInSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Borodina	Borodin	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tarutina	Tarutin	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Malojaroslavce	Malojaroslavec	k1gMnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Krasného	Krasný	k2eAgInSc2d1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Druhá	druhý	k4xOgFnSc1
bitva	bitva	k1gFnSc1
u	u	k7c2
Polocku	Polock	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
na	na	k7c6
Berezině	Berezina	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Möckern	Möckern	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Lützenu	Lützen	k2eAgFnSc4d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Budyšína	Budyšín	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Vitorie	Vitorie	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Grossbeeren	Grossbeerna	k1gFnPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Drážďan	Drážďany	k1gInPc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Chlumce	Chlumec	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
na	na	k7c6
Kačavě	Kačava	k1gFnSc6
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Dennewitz	Dennewitz	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Wartenburgu	Wartenburg	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
u	u	k7c2
Lipska	Lipsko	k1gNnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hanau	Hanaus	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Brienne	Brienn	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
La	la	k1gNnSc2
Rothiè	Rothiè	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Champaubertu	Champaubert	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Montmirailu	Montmirail	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Chateau-Thierry	Chateau-Thierra	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Montereau	Montereaus	k1gInSc2
•	•	k?
Obležení	obležení	k1gNnPc2
Soissons	Soissons	k1gInSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Craonne	Craonn	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Laonu	Laon	k1gInSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Remeše	Remeš	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Arcis-sur-Aube	Arcis-sur-Aub	k1gInSc5
•	•	k?
Bitva	bitva	k1gFnSc1
o	o	k7c4
Paříž	Paříž	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Tolentina	Tolentin	k2eAgInSc2d1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
San	San	k1gFnSc2
Germana	German	k1gMnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Ligny	Ligna	k1gFnSc2
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Quatre-Bras	Quatre-Bras	k1gMnSc1
•	•	k?
Bitva	bitva	k1gFnSc1
u	u	k7c2
Waterloo	Waterloo	k1gNnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
