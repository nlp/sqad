<s>
Dělník	dělník	k1gMnSc1	dělník
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
námezdně	námezdně	k6eAd1	námezdně
pracujícího	pracující	k1gMnSc4	pracující
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
vykonávajícího	vykonávající	k2eAgNnSc2d1	vykonávající
převážně	převážně	k6eAd1	převážně
fyzicky	fyzicky	k6eAd1	fyzicky
namáhavou	namáhavý	k2eAgFnSc4d1	namáhavá
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
živnostníka	živnostník	k1gMnSc2	živnostník
jej	on	k3xPp3gMnSc4	on
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
především	především	k9	především
závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
zaměstnavateli	zaměstnavatel	k1gMnSc6	zaměstnavatel
a	a	k8xC	a
mentální	mentální	k2eAgInSc4d1	mentální
pocit	pocit	k1gInSc4	pocit
nesamostatnosti	nesamostatnost	k1gFnSc2	nesamostatnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
specifikováno	specifikovat	k5eAaBmNgNnS	specifikovat
přívlastkem	přívlastek	k1gInSc7	přívlastek
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
činnosti	činnost	k1gFnSc2	činnost
<g/>
:	:	kIx,	:
např.	např.	kA	např.
textilní	textilní	k2eAgInSc1d1	textilní
(	(	kIx(	(
<g/>
textilák	textilák	k1gMnSc1	textilák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přadelní	přadelní	k2eAgMnSc1d1	přadelní
(	(	kIx(	(
<g/>
přadlák	přadlák	k1gMnSc1	přadlák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgFnSc1d1	lesní
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgFnSc1d1	stavební
či	či	k8xC	či
hutní	hutní	k2eAgMnSc1d1	hutní
dělník	dělník	k1gMnSc1	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
<g/>
vyučení	vyučení	k1gNnSc1	vyučení
lze	lze	k6eAd1	lze
dělníky	dělník	k1gMnPc4	dělník
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
kvalifikované	kvalifikovaný	k2eAgNnSc4d1	kvalifikované
a	a	k8xC	a
nekvalifikované	kvalifikovaný	k2eNgNnSc4d1	nekvalifikované
(	(	kIx(	(
<g/>
pomocné	pomocný	k2eAgNnSc4d1	pomocné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společenská	společenský	k2eAgFnSc1d1	společenská
skupina	skupina	k1gFnSc1	skupina
dělníků	dělník	k1gMnPc2	dělník
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
dělnictvo	dělnictvo	k1gNnSc1	dělnictvo
nebo	nebo	k8xC	nebo
dělnická	dělnický	k2eAgFnSc1d1	Dělnická
třída	třída	k1gFnSc1	třída
(	(	kIx(	(
<g/>
ať	ať	k8xS	ať
už	už	k6eAd1	už
v	v	k7c6	v
Marxově	Marxův	k2eAgFnSc6d1	Marxova
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Weberově	Weberův	k2eAgNnSc6d1	Weberovo
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
vrstva	vrstva	k1gFnSc1	vrstva
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xS	jako
dělnictvo	dělnictvo	k1gNnSc4	dělnictvo
se	se	k3xPyFc4	se
rodila	rodit	k5eAaImAgFnS	rodit
v	v	k7c6	v
postupném	postupný	k2eAgInSc6d1	postupný
procesu	proces	k1gInSc6	proces
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
utvářelo	utvářet	k5eAaImAgNnS	utvářet
vědomí	vědomí	k1gNnSc1	vědomí
společných	společný	k2eAgInPc2d1	společný
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
identity	identita	k1gFnSc2	identita
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Homogenizace	homogenizace	k1gFnSc1	homogenizace
dělnictva	dělnictvo	k1gNnSc2	dělnictvo
spočívala	spočívat	k5eAaImAgFnS	spočívat
především	především	k6eAd1	především
v	v	k7c6	v
proměně	proměna	k1gFnSc6	proměna
tovaryše	tovaryš	k1gMnSc2	tovaryš
v	v	k7c6	v
dělníka	dělník	k1gMnSc4	dělník
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přechodu	přechod	k1gInSc2	přechod
od	od	k7c2	od
cechovní	cechovní	k2eAgFnSc2d1	cechovní
k	k	k7c3	k
tovární	tovární	k2eAgFnSc3d1	tovární
výrobě	výroba	k1gFnSc3	výroba
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
musely	muset	k5eAaImAgFnP	muset
překonat	překonat	k5eAaPmF	překonat
partikulární	partikulární	k2eAgFnSc2d1	partikulární
identity	identita	k1gFnSc2	identita
dělníků	dělník	k1gMnPc2	dělník
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
výrobních	výrobní	k2eAgNnPc6d1	výrobní
odvětvích	odvětví	k1gNnPc6	odvětví
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
míru	mír	k1gInSc2	mír
stmelení	stmelení	k1gNnSc2	stmelení
dělnictva	dělnictvo	k1gNnSc2	dělnictvo
určovaly	určovat	k5eAaImAgFnP	určovat
také	také	k9	také
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
dělníky	dělník	k1gMnPc7	dělník
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgNnSc2	jeden
odvětví	odvětví	k1gNnSc2	odvětví
(	(	kIx(	(
<g/>
kvalifikovaní	kvalifikovaný	k2eAgMnPc1d1	kvalifikovaný
vs	vs	k?	vs
<g/>
.	.	kIx.	.
nekvalifikovaní	kvalifikovaný	k2eNgMnPc1d1	nekvalifikovaný
dělníci	dělník	k1gMnPc1	dělník
<g/>
,	,	kIx,	,
mistři	mistr	k1gMnPc1	mistr
vs	vs	k?	vs
<g/>
.	.	kIx.	.
nádeníci	nádeník	k1gMnPc1	nádeník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
integračně	integračně	k6eAd1	integračně
působily	působit	k5eAaImAgFnP	působit
zejména	zejména	k9	zejména
společné	společný	k2eAgInPc4d1	společný
prožitky	prožitek	k1gInPc4	prožitek
z	z	k7c2	z
práce	práce	k1gFnSc2	práce
jako	jako	k8xS	jako
sdílení	sdílení	k1gNnSc2	sdílení
těžké	těžký	k2eAgFnSc2d1	těžká
fyzické	fyzický	k2eAgFnSc2d1	fyzická
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
šikany	šikana	k1gFnSc2	šikana
<g/>
,	,	kIx,	,
zážitek	zážitek	k1gInSc1	zážitek
těžkého	těžký	k2eAgInSc2d1	těžký
úrazu	úraz	k1gInSc2	úraz
či	či	k8xC	či
smrti	smrt	k1gFnSc2	smrt
spolupracovníka	spolupracovník	k1gMnSc2	spolupracovník
nebo	nebo	k8xC	nebo
třeba	třeba	k9	třeba
stávky	stávka	k1gFnPc1	stávka
<g/>
.	.	kIx.	.
</s>
