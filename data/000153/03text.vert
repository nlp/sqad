<s>
Jarda	Jarda	k1gMnSc1	Jarda
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1966	[number]	k4	1966
Kolín	Kolín	k1gInSc1	Kolín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
výtvarník	výtvarník	k1gMnSc1	výtvarník
známý	známý	k1gMnSc1	známý
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
frontman	frontman	k1gMnSc1	frontman
kapel	kapela	k1gFnPc2	kapela
Otcovy	otcův	k2eAgInPc1d1	otcův
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
Traband	Traband	k1gInSc4	Traband
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Zásmuk	Zásmuky	k1gInPc2	Zásmuky
u	u	k7c2	u
Kolína	Kolín	k1gInSc2	Kolín
a	a	k8xC	a
ačkoli	ačkoli	k8xS	ačkoli
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
několik	několik	k4yIc4	několik
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
samouk	samouk	k1gMnSc1	samouk
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
výtvarnou	výtvarný	k2eAgFnSc4d1	výtvarná
výchovu	výchova	k1gFnSc4	výchova
a	a	k8xC	a
český	český	k2eAgInSc4d1	český
jazyk	jazyk	k1gInSc4	jazyk
na	na	k7c6	na
Pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
učitel	učitel	k1gMnSc1	učitel
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
však	však	k9	však
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgNnPc2d1	jiné
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
,	,	kIx,	,
např.	např.	kA	např.
topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
barman	barman	k1gMnSc1	barman
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
hudebního	hudební	k2eAgInSc2d1	hudební
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
pečovatel	pečovatel	k1gMnSc1	pečovatel
<g/>
,	,	kIx,	,
editor	editor	k1gMnSc1	editor
teletextu	teletext	k1gInSc2	teletext
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgMnSc1d1	sociální
pracovník	pracovník	k1gMnSc1	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
až	až	k9	až
1993	[number]	k4	1993
vedl	vést	k5eAaImAgInS	vést
křesťansky	křesťansky	k6eAd1	křesťansky
orientovanou	orientovaný	k2eAgFnSc4d1	orientovaná
kapelu	kapela	k1gFnSc4	kapela
Otcovy	otcův	k2eAgFnPc1d1	otcova
děti	dítě	k1gFnPc1	dítě
<g/>
;	;	kIx,	;
sám	sám	k3xTgMnSc1	sám
přitom	přitom	k6eAd1	přitom
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
žádné	žádný	k3yNgFnSc2	žádný
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
k	k	k7c3	k
zájmu	zájem	k1gInSc3	zájem
o	o	k7c4	o
křesťanství	křesťanství	k1gNnSc4	křesťanství
a	a	k8xC	a
židovství	židovství	k1gNnSc4	židovství
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
přes	přes	k7c4	přes
faráře	farář	k1gMnSc4	farář
a	a	k8xC	a
hudebníka	hudebník	k1gMnSc4	hudebník
Svatopluka	Svatopluk	k1gMnSc4	Svatopluk
Karáska	Karásek	k1gMnSc4	Karásek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
založil	založit	k5eAaPmAgInS	založit
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vede	vést	k5eAaImIp3nS	vést
kapelu	kapela	k1gFnSc4	kapela
Traband	Trabanda	k1gFnPc2	Trabanda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
či	či	k8xC	či
spoluautorem	spoluautor	k1gMnSc7	spoluautor
většiny	většina	k1gFnSc2	většina
jejích	její	k3xOp3gFnPc2	její
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
společně	společně	k6eAd1	společně
s	s	k7c7	s
Michalem	Michal	k1gMnSc7	Michal
Horáčkem	Horáček	k1gMnSc7	Horáček
realizoval	realizovat	k5eAaBmAgInS	realizovat
společný	společný	k2eAgInSc1d1	společný
projekt	projekt	k1gInSc1	projekt
a	a	k8xC	a
CD	CD	kA	CD
Tak	tak	k6eAd1	tak
to	ten	k3xDgNnSc4	ten
chodí	chodit	k5eAaImIp3nP	chodit
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
navázal	navázat	k5eAaPmAgInS	navázat
i	i	k9	i
spoluprací	spolupráce	k1gFnSc7	spolupráce
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
písni	píseň	k1gFnSc6	píseň
na	na	k7c6	na
novějším	nový	k2eAgNnSc6d2	novější
Horáčkově	Horáčkův	k2eAgNnSc6d1	Horáčkovo
albu	album	k1gNnSc6	album
Ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
druh	druh	k1gInSc1	druh
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
desky	deska	k1gFnPc1	deska
Trabandu	Traband	k1gInSc2	Traband
Hyjé	Hyjá	k1gFnPc1	Hyjá
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Přítel	přítel	k1gMnSc1	přítel
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
Domasa	Domas	k1gMnSc2	Domas
získaly	získat	k5eAaPmAgFnP	získat
výroční	výroční	k2eAgFnSc4d1	výroční
cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
worldmusic	worldmusice	k1gFnPc2	worldmusice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
folk	folk	k1gInSc1	folk
a	a	k8xC	a
country	country	k2eAgFnSc1d1	country
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
hrál	hrát	k5eAaImAgInS	hrát
také	také	k9	také
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Zuby	zub	k1gInPc4	zub
nehty	nehet	k1gInPc7	nehet
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
Michal	Michal	k1gMnSc1	Michal
Hrubý	Hrubý	k1gMnSc1	Hrubý
(	(	kIx(	(
<g/>
klarinet	klarinet	k1gInSc1	klarinet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Hanzlová	Hanzlová	k1gFnSc1	Hanzlová
(	(	kIx(	(
<g/>
Neočekávaný	očekávaný	k2eNgInSc1d1	neočekávaný
dýchánek	dýchánek	k1gInSc1	dýchánek
<g/>
,	,	kIx,	,
flétna	flétna	k1gFnSc1	flétna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Tichý	Tichý	k1gMnSc1	Tichý
(	(	kIx(	(
<g/>
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
)	)	kIx)	)
a	a	k8xC	a
samotný	samotný	k2eAgMnSc1d1	samotný
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
harmonium	harmonium	k1gNnSc1	harmonium
<g/>
)	)	kIx)	)
zaaranžovali	zaaranžovat	k5eAaImAgMnP	zaaranžovat
v	v	k7c6	v
90	[number]	k4	90
<g />
.	.	kIx.	.
</s>
<s>
<g/>
letech	let	k1gInPc6	let
rekonstruované	rekonstruovaný	k2eAgInPc1d1	rekonstruovaný
nápěvy	nápěv	k1gInPc1	nápěv
Karla	Karel	k1gMnSc2	Karel
Reinera	Reiner	k1gMnSc2	Reiner
k	k	k7c3	k
staročeské	staročeský	k2eAgFnSc3d1	staročeská
hře	hra	k1gFnSc3	hra
Ester	Ester	k1gFnSc2	Ester
(	(	kIx(	(
<g/>
původní	původní	k2eAgFnSc1d1	původní
partitura	partitura	k1gFnSc1	partitura
se	se	k3xPyFc4	se
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
realizované	realizovaný	k2eAgFnPc1d1	realizovaná
původně	původně	k6eAd1	původně
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
terezínském	terezínský	k2eAgNnSc6d1	Terezínské
ghettu	ghetto	k1gNnSc6	ghetto
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
původně	původně	k6eAd1	původně
vznikalo	vznikat	k5eAaImAgNnS	vznikat
jako	jako	k9	jako
představení	představení	k1gNnSc4	představení
pro	pro	k7c4	pro
Déčko	déčko	k1gNnSc4	déčko
E.	E.	kA	E.
F.	F.	kA	F.
Buriana	Burian	k1gMnSc2	Burian
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Živě	živě	k6eAd1	živě
uvedené	uvedený	k2eAgNnSc1d1	uvedené
kvarteto	kvarteto	k1gNnSc1	kvarteto
své	svůj	k3xOyFgFnSc2	svůj
variace	variace	k1gFnSc2	variace
na	na	k7c4	na
Reinerovy	Reinerův	k2eAgInPc4d1	Reinerův
motivy	motiv	k1gInPc4	motiv
předvedlo	předvést	k5eAaPmAgNnS	předvést
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
v	v	k7c6	v
lounské	lounský	k2eAgFnSc6d1	Lounská
židovská	židovský	k2eAgFnSc1d1	židovská
obřadní	obřadní	k2eAgFnSc3d1	obřadní
síni	síň	k1gFnSc3	síň
při	pře	k1gFnSc3	pře
vernisáž	vernisáž	k1gFnSc4	vernisáž
výstavy	výstava	k1gFnSc2	výstava
malíře	malíř	k1gMnSc2	malíř
a	a	k8xC	a
sochaře	sochař	k1gMnSc2	sochař
Tomáše	Tomáš	k1gMnSc2	Tomáš
Polcara	Polcar	k1gMnSc2	Polcar
ze	z	k7c2	z
Slavětína	Slavětín	k1gInSc2	Slavětín
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
Arché	arché	k1gFnPc2	arché
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
v	v	k7c6	v
libeňské	libeňský	k2eAgFnSc6d1	Libeňská
synagoze	synagoga	k1gFnSc6	synagoga
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
matematicko-fyzikální	matematickoyzikální	k2eAgFnSc3d1	matematicko-fyzikální
fakultě	fakulta	k1gFnSc3	fakulta
UK	UK	kA	UK
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
klubu	klub	k1gInSc6	klub
Rybanaruby	Rybanaruba	k1gFnSc2	Rybanaruba
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
v	v	k7c6	v
lounské	lounský	k2eAgFnSc6d1	Lounská
židovské	židovský	k2eAgFnSc6d1	židovská
obřadní	obřadní	k2eAgFnSc6d1	obřadní
síni	síň	k1gFnSc6	síň
při	při	k7c6	při
dernisáži	dernisáž	k1gFnSc6	dernisáž
výstavy	výstava	k1gFnSc2	výstava
Miroslava	Miroslav	k1gMnSc2	Miroslav
Veselého	Veselý	k1gMnSc2	Veselý
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
ghetta	ghetto	k1gNnSc2	ghetto
Památníku	památník	k1gInSc2	památník
Terezín	Terezín	k1gInSc1	Terezín
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
i	i	k9	i
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
klubu	klub	k1gInSc6	klub
Kaštan	kaštan	k1gInSc1	kaštan
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
jako	jako	k9	jako
kytarista	kytarista	k1gMnSc1	kytarista
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
improvizace	improvizace	k1gFnSc2	improvizace
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
freejazzu	freejazz	k1gInSc2	freejazz
a	a	k8xC	a
noisu	nois	k1gInSc2	nois
<g/>
,	,	kIx,	,
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
klasický	klasický	k2eAgInSc1d1	klasický
němý	němý	k2eAgInSc1d1	němý
snímek	snímek	k1gInSc1	snímek
Jeana	Jean	k1gMnSc2	Jean
Epsteina	Epsteina	k1gFnSc1	Epsteina
Pád	Pád	k1gInSc1	Pád
domu	dům	k1gInSc2	dům
Usherů	Usher	k1gMnPc2	Usher
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Mikolášem	Mikoláš	k1gMnSc7	Mikoláš
Chadimou	Chadima	k1gMnSc7	Chadima
(	(	kIx(	(
<g/>
altsaxofon	altsaxofon	k1gInSc4	altsaxofon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Janem	Jan	k1gMnSc7	Jan
Gruntem	Grunt	k1gMnSc7	Grunt
(	(	kIx(	(
<g/>
tenorsaxofon	tenorsaxofon	k1gInSc4	tenorsaxofon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michalem	Michal	k1gMnSc7	Michal
Hrubým	Hrubý	k1gMnSc7	Hrubý
(	(	kIx(	(
<g/>
basklarinet	basklarinet	k1gInSc4	basklarinet
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vojtěchem	Vojtěch	k1gMnSc7	Vojtěch
Petržilkou	petržilka	k1gFnSc7	petržilka
(	(	kIx(	(
<g/>
theremin	theremin	k2eAgMnSc1d1	theremin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
Ryba	ryba	k1gFnSc1	ryba
na	na	k7c4	na
ruby	rub	k1gInPc4	rub
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
hostoval	hostovat	k5eAaImAgInS	hostovat
s	s	k7c7	s
harmoniem	harmonium	k1gNnSc7	harmonium
na	na	k7c4	na
albu	alba	k1gFnSc4	alba
Konec	konec	k1gInSc4	konec
cesty	cesta	k1gFnSc2	cesta
skupiny	skupina	k1gFnSc2	skupina
Holy	hola	k1gFnSc2	hola
Fanda	Fanda	k1gMnSc1	Fanda
and	and	k?	and
The	The	k1gMnSc4	The
Reverends	Reverends	k1gInSc1	Reverends
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
díla	dílo	k1gNnSc2	dílo
dvojího	dvojí	k4xRgMnSc2	dvojí
druhu	druh	k1gInSc2	druh
<g/>
:	:	kIx,	:
koláže	koláž	k1gFnPc1	koláž
z	z	k7c2	z
nalezených	nalezený	k2eAgFnPc2d1	nalezená
rezavých	rezavý	k2eAgFnPc2d1	rezavá
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
malby	malba	k1gFnPc1	malba
na	na	k7c4	na
listí	listí	k1gNnSc4	listí
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
synové	syn	k1gMnPc1	syn
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
,	,	kIx,	,
Jakub	Jakub	k1gMnSc1	Jakub
Svoboda	Svoboda	k1gMnSc1	Svoboda
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Néro	Néro	k1gMnSc1	Néro
Scartch	Scartch	k1gMnSc1	Scartch
<g/>
.	.	kIx.	.
</s>
