<s>
Jarda	Jarda	k1gMnSc1	Jarda
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1966	[number]	k4	1966
Kolín	Kolín	k1gInSc1	Kolín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
výtvarník	výtvarník	k1gMnSc1	výtvarník
známý	známý	k1gMnSc1	známý
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
frontman	frontman	k1gMnSc1	frontman
kapel	kapela	k1gFnPc2	kapela
Otcovy	otcův	k2eAgInPc1d1	otcův
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
Traband	Traband	k1gInSc4	Traband
<g/>
.	.	kIx.	.
</s>
