<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
DPMB	DPMB	kA	DPMB
<g/>
)	)	kIx)	)
provozuje	provozovat	k5eAaImIp3nS	provozovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc1	loď
jezdí	jezdit	k5eAaImIp3nP	jezdit
z	z	k7c2	z
přístaviště	přístaviště	k1gNnSc2	přístaviště
v	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
do	do	k7c2	do
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
<g/>
,	,	kIx,	,
obce	obec	k1gFnSc2	obec
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
již	již	k6eAd1	již
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
(	(	kIx(	(
<g/>
od	od	k7c2	od
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
rekreační	rekreační	k2eAgFnSc6d1	rekreační
lodní	lodní	k2eAgFnSc6d1	lodní
dopravě	doprava	k1gFnSc6	doprava
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
výstavby	výstavba	k1gFnSc2	výstavba
přehrady	přehrada	k1gFnSc2	přehrada
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
koncesi	koncese	k1gFnSc6	koncese
zažádalo	zažádat	k5eAaPmAgNnS	zažádat
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc4	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
výstavba	výstavba	k1gFnSc1	výstavba
přehrady	přehrada	k1gFnSc2	přehrada
teprve	teprve	k6eAd1	teprve
začínala	začínat	k5eAaImAgFnS	začínat
<g/>
.	.	kIx.	.
</s>
<s>
Koncese	koncese	k1gFnSc1	koncese
byla	být	k5eAaImAgFnS	být
vydána	vydat	k5eAaPmNgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
a	a	k8xC	a
město	město	k1gNnSc1	město
ji	on	k3xPp3gFnSc4	on
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
Společnosti	společnost	k1gFnSc3	společnost
brněnských	brněnský	k2eAgNnPc2d1	brněnské
pouličních	pouliční	k2eAgNnPc2d1	pouliční
drah	draha	k1gNnPc2	draha
jakožto	jakožto	k8xS	jakožto
provozovateli	provozovatel	k1gMnPc7	provozovatel
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
však	však	k9	však
veškeré	veškerý	k3xTgFnPc4	veškerý
aktivity	aktivita	k1gFnPc4	aktivita
ohledně	ohledně	k7c2	ohledně
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c4	na
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
<g/>
)	)	kIx)	)
Kníničské	kníničský	k2eAgFnSc6d1	Kníničská
přehradě	přehrada	k1gFnSc6	přehrada
ustaly	ustat	k5eAaPmAgFnP	ustat
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
přehrada	přehrada	k1gFnSc1	přehrada
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
používat	používat	k5eAaImF	používat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
ale	ale	k9	ale
k	k	k7c3	k
objednání	objednání	k1gNnSc6	objednání
dvou	dva	k4xCgFnPc2	dva
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
i	i	k9	i
přes	přes	k7c4	přes
obtíže	obtíž	k1gFnPc4	obtíž
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
válečnými	válečný	k2eAgFnPc7d1	válečná
událostmi	událost	k1gFnPc7	událost
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
vyrobeny	vyroben	k2eAgInPc1d1	vyroben
a	a	k8xC	a
převezeny	převezen	k2eAgInPc1d1	převezen
z	z	k7c2	z
Königswinteru	Königswinter	k1gInSc2	Königswinter
na	na	k7c6	na
Rýně	Rýn	k1gInSc6	Rýn
(	(	kIx(	(
<g/>
poblíž	poblíž	k7c2	poblíž
Bonnu	Bonn	k1gInSc2	Bonn
<g/>
)	)	kIx)	)
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
provizorně	provizorně	k6eAd1	provizorně
uskladněny	uskladnit	k5eAaPmNgInP	uskladnit
v	v	k7c6	v
tramvajové	tramvajový	k2eAgFnSc6d1	tramvajová
vozovně	vozovna	k1gFnSc6	vozovna
Pisárky	Pisárka	k1gFnSc2	Pisárka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
neznámí	známý	k2eNgMnPc1d1	neznámý
žháři	žhář	k1gMnPc1	žhář
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
ustupující	ustupující	k2eAgMnPc1d1	ustupující
němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
<g/>
)	)	kIx)	)
zapálili	zapálit	k5eAaPmAgMnP	zapálit
celou	celý	k2eAgFnSc4d1	celá
pisáreckou	pisárecký	k2eAgFnSc4d1	Pisárecká
vozovnu	vozovna	k1gFnSc4	vozovna
<g/>
.	.	kIx.	.
</s>
<s>
Shořelo	shořet	k5eAaPmAgNnS	shořet
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
mnoho	mnoho	k4c1	mnoho
tramvajových	tramvajový	k2eAgInPc2d1	tramvajový
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc4	dva
lodě	loď	k1gFnPc4	loď
ale	ale	k8xC	ale
požár	požár	k1gInSc4	požár
přežily	přežít	k5eAaPmAgFnP	přežít
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
dost	dost	k6eAd1	dost
poškozena	poškodit	k5eAaPmNgFnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
byla	být	k5eAaImAgFnS	být
Brnu	Brno	k1gNnSc6	Brno
znovu	znovu	k6eAd1	znovu
udělena	udělen	k2eAgFnSc1d1	udělena
koncese	koncese	k1gFnSc1	koncese
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
lodě	loď	k1gFnPc1	loď
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
důkladně	důkladně	k6eAd1	důkladně
opraveny	opravit	k5eAaPmNgFnP	opravit
a	a	k8xC	a
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
bystrckého	bystrcký	k2eAgNnSc2d1	bystrcké
přístaviště	přístaviště	k1gNnSc2	přístaviště
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
zahájení	zahájení	k1gNnSc1	zahájení
provozu	provoz	k1gInSc2	provoz
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
poháněné	poháněný	k2eAgFnPc1d1	poháněná
elektricky	elektricky	k6eAd1	elektricky
pomocí	pomocí	k7c2	pomocí
akumulátorů	akumulátor	k1gInPc2	akumulátor
<g/>
,	,	kIx,	,
obdržely	obdržet	k5eAaPmAgFnP	obdržet
jména	jméno	k1gNnPc1	jméno
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
elektrického	elektrický	k2eAgInSc2d1	elektrický
pohonu	pohon	k1gInSc2	pohon
byl	být	k5eAaImAgInS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
kapalných	kapalný	k2eAgFnPc2d1	kapalná
pohonných	pohonný	k2eAgFnPc2d1	pohonná
hmot	hmota	k1gFnPc2	hmota
v	v	k7c6	v
poválečných	poválečný	k2eAgNnPc6d1	poválečné
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
byl	být	k5eAaImAgInS	být
také	také	k9	také
malý	malý	k2eAgInSc1d1	malý
člun	člun	k1gInSc1	člun
se	s	k7c7	s
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
dostal	dostat	k5eAaPmAgInS	dostat
jméno	jméno	k1gNnSc4	jméno
Svratka	Svratka	k1gFnSc1	Svratka
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
velké	velký	k2eAgFnPc1d1	velká
lodě	loď	k1gFnPc1	loď
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
plavily	plavit	k5eAaImAgFnP	plavit
mezi	mezi	k7c7	mezi
přístavištěm	přístaviště	k1gNnSc7	přístaviště
v	v	k7c6	v
Bystrci	Bystrc	k1gFnSc6	Bystrc
a	a	k8xC	a
hradem	hrad	k1gInSc7	hrad
Veveří	veveří	k2eAgFnSc1d1	veveří
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
konečnými	konečná	k1gFnPc7	konečná
bylo	být	k5eAaImAgNnS	být
šest	šest	k4xCc1	šest
nácestných	nácestný	k2eAgFnPc2d1	nácestná
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
,	,	kIx,	,
trasa	trasa	k1gFnSc1	trasa
měřila	měřit	k5eAaImAgFnS	měřit
6,3	[number]	k4	6,3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Člun	člun	k1gInSc4	člun
Svratka	Svratka	k1gFnSc1	Svratka
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaPmNgInS	využívat
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
malých	malý	k2eAgFnPc2d1	malá
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
poruchovost	poruchovost	k1gFnSc4	poruchovost
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
často	často	k6eAd1	často
mimo	mimo	k7c4	mimo
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
přepravu	přeprava	k1gFnSc4	přeprava
loděmi	loď	k1gFnPc7	loď
byl	být	k5eAaImAgInS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
<g/>
,	,	kIx,	,
do	do	k7c2	do
konce	konec	k1gInSc2	konec
sezóny	sezóna	k1gFnSc2	sezóna
bylo	být	k5eAaImAgNnS	být
převezeno	převézt	k5eAaPmNgNnS	převézt
128	[number]	k4	128
351	[number]	k4	351
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
bystrckém	bystrcký	k2eAgNnSc6d1	bystrcké
přístavišti	přístaviště	k1gNnSc6	přístaviště
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
hangáru	hangár	k1gInSc2	hangár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
původně	původně	k6eAd1	původně
sloužil	sloužit	k5eAaImAgInS	sloužit
v	v	k7c6	v
Líšni	Líšeň	k1gFnSc6	Líšeň
vojenským	vojenský	k2eAgInPc3d1	vojenský
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
dostaly	dostat	k5eAaPmAgInP	dostat
lodě	loď	k1gFnPc4	loď
základní	základní	k2eAgNnSc4d1	základní
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
přezimování	přezimování	k1gNnSc4	přezimování
mezi	mezi	k7c7	mezi
plavebními	plavební	k2eAgFnPc7d1	plavební
sezónami	sezóna	k1gFnPc7	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
byla	být	k5eAaImAgFnS	být
poblíž	poblíž	k6eAd1	poblíž
postavena	postaven	k2eAgFnSc1d1	postavena
restaurace	restaurace	k1gFnSc1	restaurace
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
sloužící	sloužící	k1gFnSc2	sloužící
pro	pro	k7c4	pro
odbavení	odbavení	k1gNnSc4	odbavení
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
provizorium	provizorium	k1gNnSc1	provizorium
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
ale	ale	k9	ale
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
pokřtěna	pokřtít	k5eAaPmNgFnS	pokřtít
a	a	k8xC	a
spuštěna	spustit	k5eAaPmNgFnS	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
již	již	k9	již
třetí	třetí	k4xOgFnSc4	třetí
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Veveří	veveří	k2eAgNnSc4d1	veveří
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bylo	být	k5eAaImAgNnS	být
plavidlo	plavidlo	k1gNnSc1	plavidlo
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
téměř	téměř	k6eAd1	téměř
shodné	shodný	k2eAgFnSc2d1	shodná
konstrukce	konstrukce	k1gFnSc2	konstrukce
jako	jako	k9	jako
obě	dva	k4xCgNnPc4	dva
"	"	kIx"	"
<g/>
válečné	válečný	k2eAgFnPc1d1	válečná
<g/>
"	"	kIx"	"
lodě	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Trup	trup	k1gInSc1	trup
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
firmou	firma	k1gFnSc7	firma
Adolf	Adolf	k1gMnSc1	Adolf
Špirk	Špirk	k1gInSc1	Špirk
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
nástavbu	nástavba	k1gFnSc4	nástavba
provedli	provést	k5eAaPmAgMnP	provést
pracovníci	pracovník	k1gMnPc1	pracovník
dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
v	v	k7c6	v
nově	nově	k6eAd1	nově
postaveném	postavený	k2eAgInSc6d1	postavený
dřevěném	dřevěný	k2eAgInSc6d1	dřevěný
hangáru	hangár	k1gInSc6	hangár
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
zájmu	zájem	k1gInSc2	zájem
návštěvníků	návštěvník	k1gMnPc2	návštěvník
přehrady	přehrada	k1gFnSc2	přehrada
o	o	k7c4	o
svezení	svezení	k1gNnSc4	svezení
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
Komína	Komín	k1gInSc2	Komín
do	do	k7c2	do
Bystrce	Bystrc	k1gFnSc2	Bystrc
(	(	kIx(	(
<g/>
konečná	konečný	k2eAgFnSc1d1	konečná
zastávka	zastávka	k1gFnSc1	zastávka
ležela	ležet	k5eAaImAgFnS	ležet
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Svratky	Svratka	k1gFnSc2	Svratka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
objednání	objednání	k1gNnSc3	objednání
další	další	k2eAgFnSc2d1	další
lodě	loď	k1gFnSc2	loď
u	u	k7c2	u
pražské	pražský	k2eAgFnSc2d1	Pražská
firmy	firma	k1gFnSc2	firma
Špirk	Špirk	k1gInSc1	Špirk
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
obě	dva	k4xCgFnPc1	dva
předchozí	předchozí	k2eAgFnPc1d1	předchozí
lodě	loď	k1gFnPc1	loď
měly	mít	k5eAaImAgFnP	mít
stanoviště	stanoviště	k1gNnSc4	stanoviště
kapitána	kapitán	k1gMnSc2	kapitán
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rovině	rovina	k1gFnSc6	rovina
s	s	k7c7	s
cestujícími	cestující	k1gMnPc7	cestující
(	(	kIx(	(
<g/>
kapacitně	kapacitně	k6eAd1	kapacitně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
malé	malý	k2eAgFnPc4d1	malá
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
plavidlo	plavidlo	k1gNnSc1	plavidlo
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
přepravit	přepravit	k5eAaPmF	přepravit
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
osob	osoba	k1gFnPc2	osoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
loď	loď	k1gFnSc1	loď
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
kormidelnu	kormidelna	k1gFnSc4	kormidelna
vyvýšenou	vyvýšený	k2eAgFnSc4d1	vyvýšená
nad	nad	k7c4	nad
střechu	střecha	k1gFnSc4	střecha
paluby	paluba	k1gFnSc2	paluba
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
rozměry	rozměr	k1gInPc1	rozměr
byly	být	k5eAaImAgInP	být
zvětšeny	zvětšen	k2eAgInPc4d1	zvětšen
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
kapacita	kapacita	k1gFnSc1	kapacita
se	se	k3xPyFc4	se
zvedla	zvednout	k5eAaPmAgFnS	zvednout
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
<g/>
.	.	kIx.	.
</s>
<s>
Trup	trup	k1gMnSc1	trup
lodi	loď	k1gFnSc2	loď
Úderník	úderník	k1gInSc1	úderník
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
nástavbu	nástavba	k1gFnSc4	nástavba
opět	opět	k6eAd1	opět
provedli	provést	k5eAaPmAgMnP	provést
pracovníci	pracovník	k1gMnPc1	pracovník
v	v	k7c6	v
hangáru	hangár	k1gInSc6	hangár
v	v	k7c6	v
bystrckém	bystrcký	k2eAgNnSc6d1	bystrcké
přístavišti	přístaviště	k1gNnSc6	přístaviště
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
plavidlo	plavidlo	k1gNnSc1	plavidlo
bylo	být	k5eAaImAgNnS	být
spuštěno	spustit	k5eAaPmNgNnS	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
došlo	dojít	k5eAaPmAgNnS	dojít
ještě	ještě	k6eAd1	ještě
k	k	k7c3	k
prodloužení	prodloužení	k1gNnSc3	prodloužení
plavební	plavební	k2eAgFnSc2d1	plavební
dráhy	dráha	k1gFnSc2	dráha
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
zastávky	zastávka	k1gFnSc2	zastávka
Skály	skála	k1gFnSc2	skála
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Mečkov	Mečkov	k1gInSc1	Mečkov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
dráha	dráha	k1gFnSc1	dráha
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
ještě	ještě	k6eAd1	ještě
do	do	k7c2	do
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
k	k	k7c3	k
mostu	most	k1gInSc3	most
(	(	kIx(	(
<g/>
na	na	k7c4	na
konec	konec	k1gInSc4	konec
vzdutí	vzdutí	k1gNnSc2	vzdutí
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
tak	tak	k6eAd1	tak
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
9,3	[number]	k4	9,3
km	km	kA	km
<g/>
,	,	kIx,	,
plavba	plavba	k1gFnSc1	plavba
jedním	jeden	k4xCgInSc7	jeden
směrem	směr	k1gInSc7	směr
trvala	trvat	k5eAaImAgFnS	trvat
67	[number]	k4	67
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1950	[number]	k4	1950
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
první	první	k4xOgFnSc1	první
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
kompletně	kompletně	k6eAd1	kompletně
v	v	k7c6	v
bystrcké	bystrcký	k2eAgFnSc6d1	bystrcká
loděnici	loděnice	k1gFnSc6	loděnice
(	(	kIx(	(
<g/>
dřevěném	dřevěný	k2eAgInSc6d1	dřevěný
hangáru	hangár	k1gInSc6	hangár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Úderníku	úderník	k1gInSc2	úderník
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
loď	loď	k1gFnSc4	loď
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Plavidlo	plavidlo	k1gNnSc1	plavidlo
bylo	být	k5eAaImAgNnS	být
pokřtěno	pokřtít	k5eAaPmNgNnS	pokřtít
jménem	jméno	k1gNnSc7	jméno
Pionýr	pionýr	k1gInSc1	pionýr
<g/>
.	.	kIx.	.
</s>
<s>
Obrovský	obrovský	k2eAgInSc1d1	obrovský
zájem	zájem	k1gInSc1	zájem
Brňanů	Brňan	k1gMnPc2	Brňan
o	o	k7c4	o
přehradu	přehrada	k1gFnSc4	přehrada
byl	být	k5eAaImAgMnS	být
příčinou	příčina	k1gFnSc7	příčina
přeplňování	přeplňování	k1gNnSc2	přeplňování
lodí	loď	k1gFnSc7	loď
zejména	zejména	k9	zejména
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
plavební	plavební	k2eAgFnSc6d1	plavební
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ani	ani	k8xC	ani
stav	stav	k1gInSc4	stav
pěti	pět	k4xCc2	pět
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
malý	malý	k2eAgInSc4d1	malý
a	a	k8xC	a
nespolehlivý	spolehlivý	k2eNgInSc4d1	nespolehlivý
člun	člun	k1gInSc4	člun
Svratka	Svratka	k1gFnSc1	Svratka
neměl	mít	k5eNaImAgInS	mít
prakticky	prakticky	k6eAd1	prakticky
žádnou	žádný	k3yNgFnSc4	žádný
přepravní	přepravní	k2eAgFnSc4d1	přepravní
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
)	)	kIx)	)
nepostačoval	postačovat	k5eNaImAgInS	postačovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
postavit	postavit	k5eAaPmF	postavit
další	další	k2eAgNnSc4d1	další
plavidlo	plavidlo	k1gNnSc4	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
Pionýra	pionýr	k1gMnSc2	pionýr
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
shodná	shodný	k2eAgFnSc1d1	shodná
loď	loď	k1gFnSc1	loď
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
Pionýra	pionýr	k1gMnSc2	pionýr
lišila	lišit	k5eAaImAgFnS	lišit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
detailech	detail	k1gInPc6	detail
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1952	[number]	k4	1952
a	a	k8xC	a
obdržela	obdržet	k5eAaPmAgFnS	obdržet
jméno	jméno	k1gNnSc4	jméno
Mír	Míra	k1gFnPc2	Míra
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvládnutí	zvládnutí	k1gNnSc4	zvládnutí
náporu	nápor	k1gInSc2	nápor
cestujících	cestující	k1gMnPc2	cestující
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
další	další	k2eAgInPc4d1	další
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
dvoupalubové	dvoupalubový	k2eAgFnPc4d1	dvoupalubová
lodi	loď	k1gFnPc4	loď
(	(	kIx(	(
<g/>
další	další	k2eAgFnPc4d1	další
lodě	loď	k1gFnPc4	loď
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
v	v	k7c6	v
bystrcké	bystrcký	k2eAgFnSc6d1	bystrcká
loděnici	loděnice	k1gFnSc6	loděnice
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
palubami	paluba	k1gFnPc7	paluba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
spuštěná	spuštěný	k2eAgFnSc1d1	spuštěná
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
Mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pyšnila	pyšnit	k5eAaImAgFnS	pyšnit
jménem	jméno	k1gNnSc7	jméno
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Trup	trup	k1gInSc1	trup
širší	široký	k2eAgInSc1d2	širší
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
než	než	k8xS	než
předchozí	předchozí	k2eAgFnPc4d1	předchozí
lodě	loď	k1gFnPc4	loď
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
horní	horní	k2eAgFnSc1d1	horní
<g/>
,	,	kIx,	,
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
sluneční	sluneční	k2eAgFnSc1d1	sluneční
paluba	paluba	k1gFnSc1	paluba
navýšily	navýšit	k5eAaPmAgFnP	navýšit
kapacitu	kapacita	k1gFnSc4	kapacita
plavidla	plavidlo	k1gNnSc2	plavidlo
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
touto	tento	k3xDgFnSc7	tento
lodí	loď	k1gFnSc7	loď
mohlo	moct	k5eAaImAgNnS	moct
přepravit	přepravit	k5eAaPmF	přepravit
350	[number]	k4	350
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
vlastních	vlastní	k2eAgFnPc2d1	vlastní
dvoupalubových	dvoupalubový	k2eAgFnPc2d1	dvoupalubová
lodí	loď	k1gFnPc2	loď
v	v	k7c6	v
loděnici	loděnice	k1gFnSc6	loděnice
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1955	[number]	k4	1955
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
loď	loď	k1gFnSc1	loď
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1956	[number]	k4	1956
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
loď	loď	k1gFnSc1	loď
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
lodí	loď	k1gFnPc2	loď
Pionýr	pionýr	k1gInSc4	pionýr
a	a	k8xC	a
Mír	mír	k1gInSc4	mír
<g/>
)	)	kIx)	)
téměř	téměř	k6eAd1	téměř
identickým	identický	k2eAgNnSc7d1	identické
dvojčetem	dvojče	k1gNnSc7	dvojče
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
dvoupalubová	dvoupalubový	k2eAgFnSc1d1	dvoupalubová
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
pro	pro	k7c4	pro
Brněnskou	brněnský	k2eAgFnSc4d1	brněnská
přehradu	přehrada	k1gFnSc4	přehrada
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
spuštěna	spustit	k5eAaPmNgFnS	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
září	září	k1gNnSc6	září
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Plavidlo	plavidlo	k1gNnSc1	plavidlo
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
jméno	jméno	k1gNnSc4	jméno
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyřazení	vyřazení	k1gNnSc3	vyřazení
nejstarších	starý	k2eAgFnPc2d3	nejstarší
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
byla	být	k5eAaImAgFnS	být
malá	malý	k2eAgFnSc1d1	malá
loď	loď	k1gFnSc1	loď
Veveří	veveří	k2eAgFnSc1d1	veveří
odprodána	odprodán	k2eAgFnSc1d1	odprodána
národnímu	národní	k2eAgInSc3d1	národní
podniku	podnik	k1gInSc2	podnik
Pramen	pramen	k1gInSc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
ještě	ještě	k6eAd1	ještě
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
loděnici	loděnice	k1gFnSc6	loděnice
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
na	na	k7c4	na
plovoucí	plovoucí	k2eAgFnSc4d1	plovoucí
prodejnu	prodejna	k1gFnSc4	prodejna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
bezejmenná	bezejmenný	k2eAgFnSc1d1	bezejmenná
loď	loď	k1gFnSc1	loď
sloužila	sloužit	k5eAaImAgFnS	sloužit
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
zřejmě	zřejmě	k6eAd1	zřejmě
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgNnP	být
odprodána	odprodat	k5eAaPmNgNnP	odprodat
na	na	k7c4	na
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
Oravskou	oravský	k2eAgFnSc4d1	Oravská
přehradu	přehrada	k1gFnSc4	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
byl	být	k5eAaImAgInS	být
také	také	k9	také
vyřazen	vyřazen	k2eAgInSc1d1	vyřazen
člun	člun	k1gInSc1	člun
Svratka	Svratka	k1gFnSc1	Svratka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
používal	používat	k5eAaImAgMnS	používat
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
ke	k	k7c3	k
služebním	služební	k2eAgInPc3d1	služební
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyřazení	vyřazení	k1gNnSc3	vyřazení
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
původních	původní	k2eAgFnPc2d1	původní
malých	malý	k2eAgFnPc2d1	malá
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
odprodána	odprodat	k5eAaPmNgFnS	odprodat
na	na	k7c4	na
Vranovskou	vranovský	k2eAgFnSc4d1	Vranovská
přehradu	přehrada	k1gFnSc4	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
17	[number]	k4	17
<g/>
.	.	kIx.	.
plavební	plavební	k2eAgFnSc2d1	plavební
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
8	[number]	k4	8
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
velké	velká	k1gFnPc1	velká
dvoupalubové	dvoupalubový	k2eAgFnPc1d1	dvoupalubová
<g/>
,	,	kIx,	,
tři	tři	k4xCgMnPc1	tři
střední	střední	k2eAgMnPc1d1	střední
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
malou	malá	k1gFnSc4	malá
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
staří	starý	k1gMnPc1	starý
lodního	lodní	k2eAgInSc2d1	lodní
parku	park	k1gInSc2	park
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
9,5	[number]	k4	9,5
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
Rakovci	Rakovec	k1gInSc6	Rakovec
poblíž	poblíž	k7c2	poblíž
přehrady	přehrada	k1gFnSc2	přehrada
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
měnírny	měnírna	k1gFnSc2	měnírna
pro	pro	k7c4	pro
tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
trať	trať	k1gFnSc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
Nabíjecí	nabíjecí	k2eAgNnPc1d1	nabíjecí
zařízení	zařízení	k1gNnPc1	zařízení
pro	pro	k7c4	pro
lodní	lodní	k2eAgInPc4d1	lodní
akumulátory	akumulátor	k1gInPc4	akumulátor
tak	tak	k9	tak
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
přemístěno	přemístit	k5eAaPmNgNnS	přemístit
z	z	k7c2	z
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
hangárů	hangár	k1gInPc2	hangár
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
zděné	zděný	k2eAgFnSc2d1	zděná
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
s	s	k7c7	s
areálem	areál	k1gInSc7	areál
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
kabelem	kabel	k1gInSc7	kabel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
postupně	postupně	k6eAd1	postupně
ubývali	ubývat	k5eAaImAgMnP	ubývat
brněnské	brněnský	k2eAgFnSc3d1	brněnská
lodní	lodní	k2eAgFnSc3d1	lodní
dopravě	doprava	k1gFnSc3	doprava
cestující	cestující	k1gMnPc1	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
přepravených	přepravený	k2eAgFnPc2d1	přepravená
osob	osoba	k1gFnPc2	osoba
za	za	k7c4	za
rok	rok	k1gInSc4	rok
drží	držet	k5eAaImIp3nS	držet
rok	rok	k1gInSc4	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lodě	loď	k1gFnPc1	loď
převezly	převézt	k5eAaPmAgFnP	převézt
848	[number]	k4	848
192	[number]	k4	192
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
813	[number]	k4	813
042	[number]	k4	042
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
764	[number]	k4	764
924	[number]	k4	924
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
o	o	k7c4	o
10	[number]	k4	10
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
dokonce	dokonce	k9	dokonce
pouze	pouze	k6eAd1	pouze
350	[number]	k4	350
618	[number]	k4	618
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
zřejmě	zřejmě	k6eAd1	zřejmě
byla	být	k5eAaImAgFnS	být
možnost	možnost	k1gFnSc1	možnost
pořízení	pořízení	k1gNnSc2	pořízení
si	se	k3xPyFc3	se
chaty	chata	k1gFnSc2	chata
<g/>
,	,	kIx,	,
automobilu	automobil	k1gInSc2	automobil
<g/>
,	,	kIx,	,
či	či	k8xC	či
dovolené	dovolená	k1gFnSc2	dovolená
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
přikročeno	přikročen	k2eAgNnSc1d1	přikročeno
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
počtu	počet	k1gInSc2	počet
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Vranovskou	vranovský	k2eAgFnSc4d1	Vranovská
přehradu	přehrada	k1gFnSc4	přehrada
odprodána	odprodat	k5eAaPmNgFnS	odprodat
druhá	druhý	k4xOgFnSc1	druhý
původní	původní	k2eAgFnSc1d1	původní
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
odstavena	odstaven	k2eAgFnSc1d1	odstavena
loď	loď	k1gFnSc1	loď
Úderník	úderník	k1gInSc1	úderník
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
následujících	následující	k2eAgNnPc2d1	následující
téměř	téměř	k6eAd1	téměř
20	[number]	k4	20
let	léto	k1gNnPc2	léto
stála	stát	k5eAaImAgFnS	stát
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
bez	bez	k7c2	bez
využití	využití	k1gNnSc2	využití
a	a	k8xC	a
chátrala	chátrat	k5eAaImAgFnS	chátrat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
výrazné	výrazný	k2eAgFnSc3d1	výrazná
změně	změna	k1gFnSc3	změna
v	v	k7c6	v
lodní	lodní	k2eAgFnSc6d1	lodní
dopravě	doprava	k1gFnSc6	doprava
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
malou	malý	k2eAgFnSc7d1	malá
změnou	změna	k1gFnSc7	změna
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
uskutečněné	uskutečněný	k2eAgNnSc4d1	uskutečněné
přejmenování	přejmenování	k1gNnSc4	přejmenování
dvou	dva	k4xCgFnPc2	dva
zastávek	zastávka	k1gFnPc2	zastávka
(	(	kIx(	(
<g/>
Krnovec	Krnovec	k1gInSc1	Krnovec
→	→	k?	→
Mečkov	Mečkov	k1gInSc1	Mečkov
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
Mečkov	Mečkov	k1gInSc1	Mečkov
→	→	k?	→
Skály	skála	k1gFnSc2	skála
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
přestavba	přestavba	k1gFnSc1	přestavba
areálu	areál	k1gInSc2	areál
celého	celý	k2eAgNnSc2d1	celé
bystrckého	bystrcký	k2eAgNnSc2d1	bystrcké
přístaviště	přístaviště	k1gNnSc2	přístaviště
<g/>
.	.	kIx.	.
</s>
<s>
Dřevěný	dřevěný	k2eAgMnSc1d1	dřevěný
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
30	[number]	k4	30
let	léto	k1gNnPc2	léto
starý	starý	k2eAgInSc1d1	starý
hangár	hangár	k1gInSc1	hangár
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
v	v	k7c6	v
letech	let	k1gInPc6	let
1978	[number]	k4	1978
–	–	k?	–
1980	[number]	k4	1980
zděným	zděný	k2eAgInSc7d1	zděný
hangárem	hangár	k1gInSc7	hangár
s	s	k7c7	s
dílnami	dílna	k1gFnPc7	dílna
<g/>
,	,	kIx,	,
kancelářemi	kancelář	k1gFnPc7	kancelář
a	a	k8xC	a
sociálním	sociální	k2eAgNnSc7d1	sociální
zařízením	zařízení	k1gNnSc7	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1982	[number]	k4	1982
–	–	k?	–
1984	[number]	k4	1984
pak	pak	k8xC	pak
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
novostavbě	novostavba	k1gFnSc3	novostavba
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
další	další	k2eAgFnSc1d1	další
budova	budova	k1gFnSc1	budova
pro	pro	k7c4	pro
přezimování	přezimování	k1gNnSc4	přezimování
dalších	další	k2eAgFnPc2d1	další
dvou	dva	k4xCgFnPc2	dva
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velkého	velký	k2eAgInSc2d1	velký
plánu	plán	k1gInSc2	plán
tak	tak	k9	tak
byly	být	k5eAaImAgInP	být
realizovány	realizován	k2eAgInPc1d1	realizován
dva	dva	k4xCgInPc1	dva
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
provozně	provozně	k6eAd1	provozně
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
běžného	běžný	k2eAgMnSc4d1	běžný
cestujícího	cestující	k1gMnSc4	cestující
bezvýznamné	bezvýznamný	k2eAgNnSc1d1	bezvýznamné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
většina	většina	k1gFnSc1	většina
velkých	velký	k2eAgFnPc2d1	velká
<g/>
,	,	kIx,	,
dvoupalubových	dvoupalubový	k2eAgFnPc2d1	dvoupalubová
lodí	loď	k1gFnPc2	loď
stáří	stáří	k1gNnSc1	stáří
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
koupi	koupě	k1gFnSc6	koupě
nových	nový	k2eAgNnPc2d1	nové
plavidel	plavidlo	k1gNnPc2	plavidlo
z	z	k7c2	z
NDR	NDR	kA	NDR
se	s	k7c7	s
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
zachování	zachování	k1gNnSc4	zachování
elektrického	elektrický	k2eAgInSc2d1	elektrický
pohonu	pohon	k1gInSc2	pohon
a	a	k8xC	a
postupné	postupný	k2eAgFnSc3d1	postupná
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
všech	všecek	k3xTgFnPc2	všecek
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
modernizovanou	modernizovaný	k2eAgFnSc7d1	modernizovaná
lodí	loď	k1gFnSc7	loď
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
loď	loď	k1gFnSc1	loď
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1980	[number]	k4	1980
a	a	k8xC	a
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
nástavba	nástavba	k1gFnSc1	nástavba
byla	být	k5eAaImAgFnS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
,	,	kIx,	,
vyměněna	vyměnit	k5eAaPmNgFnS	vyměnit
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
kormidelna	kormidelna	k1gFnSc1	kormidelna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1983	[number]	k4	1983
a	a	k8xC	a
1984	[number]	k4	1984
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
totální	totální	k2eAgFnSc3d1	totální
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
lodě	loď	k1gFnSc2	loď
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
původním	původní	k2eAgInSc6d1	původní
trupu	trup	k1gInSc6	trup
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
zcela	zcela	k6eAd1	zcela
nová	nový	k2eAgFnSc1d1	nová
nástavba	nástavba	k1gFnSc1	nástavba
včetně	včetně	k7c2	včetně
moderní	moderní	k2eAgFnSc2d1	moderní
kormidelny	kormidelna	k1gFnSc2	kormidelna
<g/>
.	.	kIx.	.
</s>
<s>
Rozsahem	rozsah	k1gInSc7	rozsah
stejná	stejný	k2eAgFnSc1d1	stejná
modernizace	modernizace	k1gFnSc1	modernizace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
u	u	k7c2	u
lodi	loď	k1gFnSc2	loď
Bratislava	Bratislava	k1gFnSc1	Bratislava
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1984	[number]	k4	1984
až	až	k9	až
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
a	a	k8xC	a
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
zrenovována	zrenovován	k2eAgFnSc1d1	zrenovována
loď	loď	k1gFnSc1	loď
Úderník	úderník	k1gInSc1	úderník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
nevyužívalo	využívat	k5eNaImAgNnS	využívat
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
po	po	k7c6	po
dvacetileté	dvacetiletý	k2eAgFnSc6d1	dvacetiletá
odstávce	odstávka	k1gFnSc6	odstávka
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
trup	trupa	k1gFnPc2	trupa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
proto	proto	k8xC	proto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
opravě	oprava	k1gFnSc3	oprava
trupu	trup	k1gInSc2	trup
a	a	k8xC	a
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnSc2d1	nová
nástavby	nástavba	k1gFnSc2	nástavba
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
již	již	k6eAd1	již
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
linkový	linkový	k2eAgInSc4d1	linkový
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
soukromé	soukromý	k2eAgFnPc4d1	soukromá
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
večírky	večírek	k1gInPc4	večírek
<g/>
,	,	kIx,	,
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k6eAd1	také
z	z	k7c2	z
přejmenování	přejmenování	k1gNnSc2	přejmenování
této	tento	k3xDgFnSc2	tento
lodě	loď	k1gFnSc2	loď
na	na	k7c4	na
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
již	již	k9	již
druhé	druhý	k4xOgNnSc1	druhý
obsazení	obsazení	k1gNnSc1	obsazení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
salónní	salónní	k2eAgFnSc1d1	salónní
loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
spuštěna	spustit	k5eAaPmNgFnS	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
v	v	k7c6	v
září	září	k1gNnSc6	září
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
nerekonstruovanou	rekonstruovaný	k2eNgFnSc7d1	nerekonstruovaná
dvoupalubovou	dvoupalubový	k2eAgFnSc7d1	dvoupalubová
lodí	loď	k1gFnSc7	loď
byl	být	k5eAaImAgInS	být
Kyjev	Kyjev	k1gInSc1	Kyjev
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
plavidlo	plavidlo	k1gNnSc1	plavidlo
bylo	být	k5eAaImAgNnS	být
modernizováno	modernizovat	k5eAaBmNgNnS	modernizovat
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
lodě	loď	k1gFnSc2	loď
zůstal	zůstat	k5eAaPmAgInS	zůstat
pouze	pouze	k6eAd1	pouze
trup	trup	k1gInSc1	trup
<g/>
,	,	kIx,	,
nástavba	nástavba	k1gFnSc1	nástavba
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
této	tento	k3xDgFnSc2	tento
lodě	loď	k1gFnSc2	loď
na	na	k7c4	na
Veveří	veveří	k2eAgNnSc4d1	veveří
(	(	kIx(	(
<g/>
druhé	druhý	k4xOgNnSc1	druhý
obsazení	obsazení	k1gNnSc1	obsazení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
a	a	k8xC	a
poslední	poslední	k2eAgNnSc4d1	poslední
přejmenování	přejmenování	k1gNnSc4	přejmenování
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
loď	loď	k1gFnSc1	loď
Moskva	Moskva	k1gFnSc1	Moskva
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
na	na	k7c4	na
Dallas	Dallas	k1gInSc4	Dallas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
jsou	být	k5eAaImIp3nP	být
zavedeny	zaveden	k2eAgInPc1d1	zaveden
dva	dva	k4xCgInPc1	dva
plavební	plavební	k2eAgInPc1d1	plavební
okruhy	okruh	k1gInPc1	okruh
<g/>
:	:	kIx,	:
velký	velký	k2eAgInSc1d1	velký
a	a	k8xC	a
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
okruh	okruh	k1gInSc1	okruh
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
přehradě	přehrada	k1gFnSc6	přehrada
z	z	k7c2	z
Bystrce	Bystrc	k1gFnSc2	Bystrc
do	do	k7c2	do
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
spoje	spoj	k1gInPc1	spoj
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
malém	malý	k2eAgInSc6d1	malý
okruhu	okruh	k1gInSc6	okruh
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Bystrc	Bystrc	k1gFnSc4	Bystrc
–	–	k?	–
Rokle	rokle	k1gFnSc1	rokle
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
plavební	plavební	k2eAgFnSc2d1	plavební
sezóny	sezóna	k1gFnSc2	sezóna
1992	[number]	k4	1992
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
neodborné	odborný	k2eNgFnSc6d1	neodborná
manipulaci	manipulace	k1gFnSc6	manipulace
poškozen	poškodit	k5eAaPmNgInS	poškodit
trup	trup	k1gInSc1	trup
lodě	loď	k1gFnSc2	loď
Mír	Míra	k1gFnPc2	Míra
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
opravě	oprava	k1gFnSc6	oprava
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
stavebním	stavební	k2eAgFnPc3d1	stavební
aktivitám	aktivita	k1gFnPc3	aktivita
v	v	k7c6	v
bystrckém	bystrcký	k2eAgNnSc6d1	bystrcké
přístavišti	přístaviště	k1gNnSc6	přístaviště
a	a	k8xC	a
napnutému	napnutý	k2eAgInSc3d1	napnutý
rozpočtu	rozpočet	k1gInSc3	rozpočet
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
byla	být	k5eAaImAgFnS	být
odstavena	odstavit	k5eAaPmNgFnS	odstavit
na	na	k7c6	na
venkovním	venkovní	k2eAgNnSc6d1	venkovní
prostranství	prostranství	k1gNnSc6	prostranství
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
13	[number]	k4	13
let	léto	k1gNnPc2	léto
chátrala	chátrat	k5eAaImAgFnS	chátrat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
převezena	převézt	k5eAaPmNgFnS	převézt
do	do	k7c2	do
hangáru	hangár	k1gInSc2	hangár
<g/>
,	,	kIx,	,
započata	započnout	k5eAaPmNgFnS	započnout
byla	být	k5eAaImAgFnS	být
také	také	k9	také
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
;	;	kIx,	;
práce	práce	k1gFnSc1	práce
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
brzy	brzy	k6eAd1	brzy
zastaveny	zastavit	k5eAaPmNgFnP	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
prodeji	prodej	k1gInSc6	prodej
salónní	salónní	k2eAgFnSc2d1	salónní
lodi	loď	k1gFnSc2	loď
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
utržené	utržený	k2eAgInPc1d1	utržený
peníze	peníz	k1gInPc1	peníz
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
pro	pro	k7c4	pro
opravu	oprava	k1gFnSc4	oprava
Míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
akci	akce	k1gFnSc3	akce
ale	ale	k8xC	ale
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
torzo	torzo	k1gNnSc1	torzo
lodi	loď	k1gFnSc2	loď
opět	opět	k6eAd1	opět
přepraveno	přepravit	k5eAaPmNgNnS	přepravit
do	do	k7c2	do
hangáru	hangár	k1gInSc2	hangár
<g/>
,	,	kIx,	,
Státní	státní	k2eAgFnSc1d1	státní
plavební	plavební	k2eAgFnSc1d1	plavební
správa	správa	k1gFnSc1	správa
ovšem	ovšem	k9	ovšem
shledala	shledat	k5eAaPmAgFnS	shledat
trup	trup	k1gInSc4	trup
nezpůsobilý	způsobilý	k2eNgInSc4d1	nezpůsobilý
k	k	k7c3	k
opravě	oprava	k1gFnSc3	oprava
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
veškeré	veškerý	k3xTgNnSc4	veškerý
naděje	naděje	k1gFnPc1	naděje
skončily	skončit	k5eAaPmAgFnP	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Nástavba	nástavba	k1gFnSc1	nástavba
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
rozebírána	rozebírán	k2eAgFnSc1d1	rozebírána
<g/>
,	,	kIx,	,
trup	trup	k1gInSc1	trup
byl	být	k5eAaImAgInS	být
sešrotován	sešrotovat	k5eAaPmNgInS	sešrotovat
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
stavební	stavební	k2eAgFnPc1d1	stavební
akce	akce	k1gFnPc1	akce
se	se	k3xPyFc4	se
bystrcké	bystrcký	k2eAgNnSc1d1	bystrcké
přístaviště	přístaviště	k1gNnSc1	přístaviště
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994	[number]	k4	1994
a	a	k8xC	a
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
ocelová	ocelový	k2eAgFnSc1d1	ocelová
provozně-správní	provozněprávní	k2eAgFnSc1d1	provozně-správní
budova	budova	k1gFnSc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
a	a	k8xC	a
barevným	barevný	k2eAgNnSc7d1	barevné
řešením	řešení	k1gNnSc7	řešení
připomíná	připomínat	k5eAaImIp3nS	připomínat
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
architekt	architekt	k1gMnSc1	architekt
Pavel	Pavel	k1gMnSc1	Pavel
Šrubař	Šrubař	k1gMnSc1	Šrubař
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
projekt	projekt	k1gInSc1	projekt
také	také	k9	také
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
objekty	objekt	k1gInPc7	objekt
(	(	kIx(	(
<g/>
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
terasa	terasa	k1gFnSc1	terasa
<g/>
,	,	kIx,	,
odbavovací	odbavovací	k2eAgInSc1d1	odbavovací
prostor	prostor	k1gInSc1	prostor
s	s	k7c7	s
pokladnami	pokladna	k1gFnPc7	pokladna
a	a	k8xC	a
třemi	tři	k4xCgNnPc7	tři
nástupišti	nástupiště	k1gNnPc7	nástupiště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
realizovány	realizovat	k5eAaBmNgFnP	realizovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
nádrží	nádrž	k1gFnPc2	nádrž
s	s	k7c7	s
lodní	lodní	k2eAgFnSc7d1	lodní
dopravou	doprava	k1gFnSc7	doprava
(	(	kIx(	(
<g/>
Slapy	slap	k1gInPc1	slap
<g/>
,	,	kIx,	,
Vranov	Vranov	k1gInSc1	Vranov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nepostihl	postihnout	k5eNaPmAgInS	postihnout
Brněnskou	brněnský	k2eAgFnSc4d1	brněnská
přehradu	přehrada	k1gFnSc4	přehrada
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
obrovský	obrovský	k2eAgInSc4d1	obrovský
úbytek	úbytek	k1gInSc4	úbytek
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
desetiletí	desetiletí	k1gNnSc2	desetiletí
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
začaly	začít	k5eAaPmAgFnP	začít
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
objevovat	objevovat	k5eAaImF	objevovat
sinice	sinice	k1gFnSc1	sinice
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
nemožnost	nemožnost	k1gFnSc4	nemožnost
koupání	koupání	k1gNnSc2	koupání
se	se	k3xPyFc4	se
ale	ale	k9	ale
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
nijak	nijak	k6eAd1	nijak
rapidně	rapidně	k6eAd1	rapidně
nezmenšil	zmenšit	k5eNaPmAgMnS	zmenšit
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
lodě	loď	k1gFnPc1	loď
přepravily	přepravit	k5eAaPmAgFnP	přepravit
sice	sice	k8xC	sice
méně	málo	k6eAd2	málo
osob	osoba	k1gFnPc2	osoba
(	(	kIx(	(
<g/>
minimum	minimum	k1gNnSc1	minimum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
147	[number]	k4	147
369	[number]	k4	369
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejhorší	zlý	k2eAgInSc1d3	Nejhorší
výsledek	výsledek	k1gInSc1	výsledek
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
vyjma	vyjma	k7c2	vyjma
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
přepravy	přeprava	k1gFnSc2	přeprava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
ale	ale	k9	ale
úbytek	úbytek	k1gInSc1	úbytek
cestujících	cestující	k1gFnPc2	cestující
zastavil	zastavit	k5eAaPmAgInS	zastavit
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
počet	počet	k1gInSc1	počet
zájemců	zájemce	k1gMnPc2	zájemce
o	o	k7c4	o
přepravu	přeprava	k1gFnSc4	přeprava
každoročně	každoročně	k6eAd1	každoročně
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
narůstal	narůstat	k5eAaImAgMnS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plavební	plavební	k2eAgFnSc6d1	plavební
sezóně	sezóna	k1gFnSc6	sezóna
2007	[number]	k4	2007
dokonce	dokonce	k9	dokonce
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
počet	počet	k1gInSc1	počet
přepravených	přepravený	k2eAgFnPc2d1	přepravená
osob	osoba	k1gFnPc2	osoba
téměř	téměř	k6eAd1	téměř
230	[number]	k4	230
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
přepraveno	přepravit	k5eAaPmNgNnS	přepravit
222	[number]	k4	222
tisíc	tisíc	k4xCgInPc2	tisíc
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
bylo	být	k5eAaImAgNnS	být
přikročeno	přikročen	k2eAgNnSc1d1	přikročeno
k	k	k7c3	k
první	první	k4xOgFnSc3	první
změně	změna	k1gFnSc3	změna
umístění	umístění	k1gNnSc2	umístění
zastávek	zastávka	k1gFnPc2	zastávka
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
mezeře	mezera	k1gFnSc6	mezera
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
přístavišti	přístaviště	k1gNnPc7	přístaviště
Kozí	kozí	k2eAgFnSc1d1	kozí
horka	horka	k1gFnSc1	horka
a	a	k8xC	a
Rokle	rokle	k1gFnSc1	rokle
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
zastávka	zastávka	k1gFnSc1	zastávka
U	u	k7c2	u
Kotvy	kotva	k1gFnSc2	kotva
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc1	loď
tak	tak	k9	tak
přibližně	přibližně	k6eAd1	přibližně
rok	rok	k1gInSc4	rok
pravidelně	pravidelně	k6eAd1	pravidelně
křižovaly	křižovat	k5eAaImAgFnP	křižovat
nádrž	nádrž	k1gFnSc4	nádrž
z	z	k7c2	z
levé	levý	k2eAgFnSc2d1	levá
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
až	až	k6eAd1	až
po	po	k7c4	po
zastávku	zastávka	k1gFnSc4	zastávka
Rokle	rokle	k1gFnSc2	rokle
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
přístaviště	přístaviště	k1gNnSc2	přístaviště
Zouvalka	Zouvalka	k1gFnSc1	Zouvalka
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
této	tento	k3xDgFnSc2	tento
zastávky	zastávka	k1gFnSc2	zastávka
bylo	být	k5eAaImAgNnS	být
zpřístupnění	zpřístupnění	k1gNnSc1	zpřístupnění
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
Veveří	veveří	k2eAgNnSc4d1	veveří
byl	být	k5eAaImAgMnS	být
zničen	zničen	k2eAgMnSc1d1	zničen
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
nová	nový	k2eAgFnSc1d1	nová
lávka	lávka	k1gFnSc1	lávka
pro	pro	k7c4	pro
pěší	pěší	k1gMnPc4	pěší
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
právě	právě	k6eAd1	právě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
málo	málo	k6eAd1	málo
využívané	využívaný	k2eAgNnSc1d1	využívané
přístaviště	přístaviště	k1gNnSc1	přístaviště
Obora	obora	k1gFnSc1	obora
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
téměř	téměř	k6eAd1	téměř
naproti	naproti	k6eAd1	naproti
<g/>
,	,	kIx,	,
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
zastávka	zastávka	k1gFnSc1	zastávka
Cyklistická	cyklistický	k2eAgFnSc1d1	cyklistická
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
plavební	plavební	k2eAgFnSc1d1	plavební
dráha	dráha	k1gFnSc1	dráha
již	již	k6eAd1	již
nezměnila	změnit	k5eNaPmAgFnS	změnit
<g/>
,	,	kIx,	,
lodě	loď	k1gFnPc1	loď
tak	tak	k9	tak
pravidelně	pravidelně	k6eAd1	pravidelně
křižují	křižovat	k5eAaImIp3nP	křižovat
nádrž	nádrž	k1gFnSc4	nádrž
až	až	k9	až
po	po	k7c4	po
zastávku	zastávka	k1gFnSc4	zastávka
Hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgMnSc1d1	veveří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
oslavila	oslavit	k5eAaPmAgFnS	oslavit
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
své	své	k1gNnSc1	své
60	[number]	k4	60
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
totální	totální	k2eAgFnSc1d1	totální
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
téměř	téměř	k6eAd1	téměř
nevyužívané	využívaný	k2eNgFnSc2d1	nevyužívaná
salónní	salónní	k2eAgFnSc2d1	salónní
lodě	loď	k1gFnSc2	loď
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
lodě	loď	k1gFnSc2	loď
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
podruhé	podruhé	k6eAd1	podruhé
<g/>
)	)	kIx)	)
zbyl	zbýt	k5eAaPmAgInS	zbýt
pouze	pouze	k6eAd1	pouze
trup	trup	k1gInSc1	trup
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
zcela	zcela	k6eAd1	zcela
nová	nový	k2eAgFnSc1d1	nová
nástavba	nástavba	k1gFnSc1	nástavba
<g/>
,	,	kIx,	,
obdobná	obdobný	k2eAgFnSc1d1	obdobná
jako	jako	k9	jako
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Pionýr	pionýr	k1gInSc1	pionýr
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
byly	být	k5eAaImAgInP	být
využity	využít	k5eAaPmNgInP	využít
některé	některý	k3yIgInPc1	některý
díly	díl	k1gInPc1	díl
z	z	k7c2	z
odstavené	odstavený	k2eAgFnSc2d1	odstavená
lodi	loď	k1gFnSc2	loď
Mír	Míra	k1gFnPc2	Míra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
sešrotována	sešrotovat	k5eAaPmNgFnS	sešrotovat
<g/>
.	.	kIx.	.
</s>
<s>
Křest	křest	k1gInSc1	křest
a	a	k8xC	a
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
spuštění	spuštění	k1gNnSc1	spuštění
modernizovaného	modernizovaný	k2eAgNnSc2d1	modernizované
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dne	den	k1gInSc2	den
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dveří	dveře	k1gFnPc2	dveře
v	v	k7c6	v
bystrckém	bystrcký	k2eAgInSc6d1	bystrcký
areálu	areál	k1gInSc6	areál
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
zcela	zcela	k6eAd1	zcela
vynechána	vynechat	k5eAaPmNgFnS	vynechat
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
snížení	snížení	k1gNnSc2	snížení
hladiny	hladina	k1gFnSc2	hladina
kvůli	kvůli	k7c3	kvůli
zavápňování	zavápňování	k1gNnSc3	zavápňování
břehů	břeh	k1gInPc2	břeh
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
akcí	akce	k1gFnSc7	akce
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
den	den	k1gInSc1	den
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dveří	dveře	k1gFnPc2	dveře
v	v	k7c6	v
bystrcké	bystrcký	k2eAgFnSc6d1	bystrcká
loděnici	loděnice	k1gFnSc6	loděnice
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
návštěvníci	návštěvník	k1gMnPc1	návštěvník
měli	mít	k5eAaImAgMnP	mít
možnost	možnost	k1gFnSc4	možnost
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
si	se	k3xPyFc3	se
všechny	všechen	k3xTgFnPc4	všechen
lodě	loď	k1gFnPc4	loď
vytažené	vytažený	k2eAgFnPc4d1	vytažená
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
či	či	k8xC	či
v	v	k7c6	v
hangáru	hangár	k1gInSc6	hangár
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
nové	nový	k2eAgFnSc2d1	nová
opěrné	opěrný	k2eAgFnSc2d1	opěrná
zdi	zeď	k1gFnSc2	zeď
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
nástupními	nástupní	k2eAgInPc7d1	nástupní
můstky	můstek	k1gInPc7	můstek
v	v	k7c6	v
bystrckém	bystrcký	k2eAgNnSc6d1	bystrcké
přístavišti	přístaviště	k1gNnSc6	přístaviště
<g/>
;	;	kIx,	;
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
stavbu	stavba	k1gFnSc4	stavba
navázalo	navázat	k5eAaPmAgNnS	navázat
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
vybudování	vybudování	k1gNnSc2	vybudování
technických	technický	k2eAgFnPc2d1	technická
sítí	síť	k1gFnPc2	síť
–	–	k?	–
rozvodů	rozvod	k1gInPc2	rozvod
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
elektroinstalace	elektroinstalace	k1gFnSc2	elektroinstalace
a	a	k8xC	a
kanalizace	kanalizace	k1gFnSc2	kanalizace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
příprava	příprava	k1gFnSc1	příprava
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
budovy	budova	k1gFnSc2	budova
nové	nový	k2eAgFnSc2d1	nová
výpravny	výpravna	k1gFnSc2	výpravna
lodí	loď	k1gFnPc2	loď
se	s	k7c7	s
zázemím	zázemí	k1gNnSc7	zázemí
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nová	nový	k2eAgFnSc1d1	nová
výpravna	výpravna	k1gFnSc1	výpravna
za	za	k7c4	za
pět	pět	k4xCc4	pět
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
otevření	otevření	k1gNnSc3	otevření
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
zbořena	zbořen	k2eAgFnSc1d1	zbořena
původní	původní	k2eAgFnSc1d1	původní
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
výpravní	výpravní	k2eAgFnSc1d1	výpravní
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
místě	místo	k1gNnSc6	místo
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
dětské	dětský	k2eAgNnSc1d1	dětské
hřiště	hřiště	k1gNnSc1	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
prvního	první	k4xOgNnSc2	první
a	a	k8xC	a
druhého	druhý	k4xOgNnSc2	druhý
desetiletí	desetiletí	k1gNnSc2	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
téměř	téměř	k6eAd1	téměř
kompletní	kompletní	k2eAgFnSc1d1	kompletní
obnova	obnova	k1gFnSc1	obnova
flotily	flotila	k1gFnSc2	flotila
Dopravního	dopravní	k2eAgInSc2d1	dopravní
podniku	podnik	k1gInSc2	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
za	za	k7c4	za
nové	nový	k2eAgFnPc4d1	nová
lodě	loď	k1gFnPc4	loď
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Jesko	Jesko	k1gNnSc1	Jesko
CZ	CZ	kA	CZ
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc7	první
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2010	[number]	k4	2010
odprodána	odprodán	k2eAgFnSc1d1	odprodána
vyřazená	vyřazený	k2eAgFnSc1d1	vyřazená
loď	loď	k1gFnSc1	loď
Pionýr	pionýr	k1gInSc1	pionýr
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc1	její
poslední	poslední	k2eAgFnSc1d1	poslední
plavba	plavba	k1gFnSc1	plavba
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zahájení	zahájení	k1gNnSc6	zahájení
nové	nový	k2eAgFnSc2d1	nová
plavební	plavební	k2eAgFnSc2d1	plavební
sezóny	sezóna	k1gFnSc2	sezóna
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
spuštěna	spuštěn	k2eAgMnSc4d1	spuštěn
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
loď	loď	k1gFnSc1	loď
Lipsko	Lipsko	k1gNnSc1	Lipsko
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
a	a	k8xC	a
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
dvoupalubové	dvoupalubový	k2eAgFnPc1d1	dvoupalubová
lodě	loď	k1gFnPc1	loď
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
Bratislava	Bratislava	k1gFnSc1	Bratislava
vypluly	vyplout	k5eAaPmAgFnP	vyplout
naposledy	naposledy	k6eAd1	naposledy
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2010	[number]	k4	2010
v	v	k7c4	v
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
den	den	k1gInSc4	den
plavební	plavební	k2eAgFnSc2d1	plavební
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
byly	být	k5eAaImAgInP	být
odstaveny	odstavit	k5eAaPmNgInP	odstavit
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
odprodány	odprodán	k2eAgInPc1d1	odprodán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
nahradily	nahradit	k5eAaPmAgFnP	nahradit
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
lodě	loď	k1gFnPc4	loď
Utrecht	Utrechta	k1gFnPc2	Utrechta
a	a	k8xC	a
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnSc2	loď
Dallas	Dallas	k1gInSc1	Dallas
a	a	k8xC	a
Veveří	veveří	k2eAgFnPc1d1	veveří
definitivně	definitivně	k6eAd1	definitivně
ukončily	ukončit	k5eAaPmAgFnP	ukončit
svůj	svůj	k3xOyFgInSc4	svůj
provoz	provoz	k1gInSc4	provoz
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
dodány	dodán	k2eAgFnPc4d1	dodána
poslední	poslední	k2eAgFnPc4d1	poslední
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
lodě	loď	k1gFnPc4	loď
Dallas	Dallas	k1gInSc1	Dallas
a	a	k8xC	a
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
kompletaci	kompletace	k1gFnSc6	kompletace
slavnostně	slavnostně	k6eAd1	slavnostně
spuštěny	spustit	k5eAaPmNgFnP	spustit
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
následující	následující	k2eAgFnSc2d1	následující
plavební	plavební	k2eAgFnSc2d1	plavební
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Veveří	veveří	k2eAgNnSc4d1	veveří
bylo	být	k5eAaImAgNnS	být
odprodáno	odprodán	k2eAgNnSc1d1	odprodán
soukromé	soukromý	k2eAgFnSc3d1	soukromá
osobě	osoba	k1gFnSc3	osoba
<g/>
,	,	kIx,	,
stará	starý	k2eAgFnSc1d1	stará
loď	loď	k1gFnSc1	loď
Dallas	Dallas	k1gInSc1	Dallas
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
odstavena	odstaven	k2eAgFnSc1d1	odstavena
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
stane	stanout	k5eAaPmIp3nS	stanout
historické	historický	k2eAgNnSc4d1	historické
plavidlo	plavidlo	k1gNnSc4	plavidlo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
provozu	provoz	k1gInSc6	provoz
pět	pět	k4xCc4	pět
dvoupalubových	dvoupalubový	k2eAgFnPc2d1	dvoupalubová
lodí	loď	k1gFnPc2	loď
vyrobených	vyrobený	k2eAgFnPc2d1	vyrobená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
a	a	k8xC	a
starší	starý	k2eAgNnSc4d2	starší
jednopalubové	jednopalubový	k2eAgNnSc4d1	jednopalubový
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc4d1	poslední
z	z	k7c2	z
tradičních	tradiční	k2eAgFnPc2d1	tradiční
brněnských	brněnský	k2eAgFnPc2d1	brněnská
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
upravena	upravit	k5eAaPmNgFnS	upravit
vyklizením	vyklizení	k1gNnSc7	vyklizení
zadní	zadní	k2eAgFnSc2d1	zadní
otevřené	otevřený	k2eAgFnSc2d1	otevřená
paluby	paluba	k1gFnSc2	paluba
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
pro	pro	k7c4	pro
vozíčkáře	vozíčkář	k1gMnPc4	vozíčkář
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyřazeném	vyřazený	k2eAgInSc6d1	vyřazený
Veveří	veveří	k2eAgNnSc4d1	veveří
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
umístěno	umístit	k5eAaPmNgNnS	umístit
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
přehrady	přehrada	k1gFnSc2	přehrada
u	u	k7c2	u
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
bystrckému	bystrcký	k2eAgNnSc3d1	bystrcké
přístavišti	přístaviště	k1gNnSc3	přístaviště
<g/>
,	,	kIx,	,
zřídil	zřídit	k5eAaPmAgMnS	zřídit
majitel	majitel	k1gMnSc1	majitel
kromě	kromě	k7c2	kromě
restaurace	restaurace	k1gFnSc2	restaurace
také	také	k9	také
muzeum	muzeum	k1gNnSc4	muzeum
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2013	[number]	k4	2013
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
stavby	stavba	k1gFnSc2	stavba
nového	nový	k2eAgNnSc2d1	nové
přístaviště	přístaviště	k1gNnSc2	přístaviště
pod	pod	k7c7	pod
hradem	hrad	k1gInSc7	hrad
Veveří	veveří	k2eAgMnPc1d1	veveří
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
to	ten	k3xDgNnSc1	ten
staré	staré	k1gNnSc1	staré
svým	svůj	k3xOyFgNnSc7	svůj
umístěním	umístění	k1gNnSc7	umístění
a	a	k8xC	a
přístupem	přístup	k1gInSc7	přístup
nevyhovuje	vyhovovat	k5eNaImIp3nS	vyhovovat
cyklistům	cyklista	k1gMnPc3	cyklista
<g/>
,	,	kIx,	,
vozíčkářům	vozíčkář	k1gMnPc3	vozíčkář
ani	ani	k8xC	ani
kočárkům	kočárek	k1gInPc3	kočárek
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
zastávka	zastávka	k1gFnSc1	zastávka
Hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgFnSc1d1	veveří
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
realizována	realizovat	k5eAaBmNgFnS	realizovat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
zastřešené	zastřešený	k2eAgFnSc2d1	zastřešená
lávky	lávka	k1gFnSc2	lávka
na	na	k7c6	na
betonových	betonový	k2eAgInPc6d1	betonový
pilířích	pilíř	k1gInPc6	pilíř
u	u	k7c2	u
ústí	ústí	k1gNnSc2	ústí
potoka	potok	k1gInSc2	potok
Veverky	veverka	k1gFnSc2	veverka
přesáhly	přesáhnout	k5eAaPmAgFnP	přesáhnout
částku	částka	k1gFnSc4	částka
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lodní	lodní	k2eAgFnSc2d1	lodní
prodejny	prodejna	k1gFnSc2	prodejna
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
po	po	k7c6	po
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
také	také	k9	také
plavily	plavit	k5eAaImAgFnP	plavit
lodě	loď	k1gFnPc1	loď
národního	národní	k2eAgInSc2d1	národní
podniku	podnik	k1gInSc2	podnik
Pramen	pramen	k1gInSc1	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
křižovaly	křižovat	k5eAaImAgFnP	křižovat
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
nádrž	nádrž	k1gFnSc1	nádrž
a	a	k8xC	a
při	při	k7c6	při
zastávkách	zastávka	k1gFnPc6	zastávka
u	u	k7c2	u
břehů	břeh	k1gInPc2	břeh
si	se	k3xPyFc3	se
rekreanti	rekreant	k1gMnPc1	rekreant
mohli	moct	k5eAaImAgMnP	moct
koupit	koupit	k5eAaPmF	koupit
občerstvení	občerstvení	k1gNnSc4	občerstvení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
Státní	státní	k2eAgFnSc1d1	státní
plánovací	plánovací	k2eAgFnSc1d1	plánovací
komise	komise	k1gFnSc1	komise
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
bystrcké	bystrcký	k2eAgFnSc6d1	bystrcká
loděnici	loděnice	k1gFnSc6	loděnice
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
v	v	k7c6	v
dřevěném	dřevěný	k2eAgInSc6d1	dřevěný
hangáru	hangár	k1gInSc6	hangár
<g/>
)	)	kIx)	)
bude	být	k5eAaImBp3nS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
12	[number]	k4	12
lodí	loď	k1gFnPc2	loď
pro	pro	k7c4	pro
provozovatele	provozovatel	k1gMnPc4	provozovatel
lodní	lodní	k2eAgFnSc2d1	lodní
dopravy	doprava	k1gFnSc2	doprava
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
v	v	k7c6	v
mohutném	mohutný	k2eAgNnSc6d1	mohutné
tempu	tempo	k1gNnSc6	tempo
stavěly	stavět	k5eAaImAgFnP	stavět
velké	velký	k2eAgFnPc1d1	velká
přehrady	přehrada	k1gFnPc1	přehrada
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
lodě	loď	k1gFnPc1	loď
nebyly	být	k5eNaImAgFnP	být
téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
velké	velký	k2eAgFnPc1d1	velká
české	český	k2eAgFnPc1d1	Česká
loděnice	loděnice	k1gFnPc1	loděnice
měly	mít	k5eAaImAgFnP	mít
pro	pro	k7c4	pro
státní	státní	k2eAgNnSc4d1	státní
hospodářství	hospodářství	k1gNnSc4	hospodářství
důležitější	důležitý	k2eAgFnSc2d2	důležitější
zakázky	zakázka	k1gFnSc2	zakázka
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
plavidla	plavidlo	k1gNnPc1	plavidlo
postaví	postavit	k5eAaPmIp3nP	postavit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
postaveno	postavit	k5eAaPmNgNnS	postavit
pro	pro	k7c4	pro
Brněnskou	brněnský	k2eAgFnSc4d1	brněnská
přehradu	přehrada	k1gFnSc4	přehrada
pět	pět	k4xCc4	pět
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
nástavby	nástavba	k1gFnPc1	nástavba
pro	pro	k7c4	pro
již	již	k6eAd1	již
dodaný	dodaný	k2eAgInSc4d1	dodaný
trup	trup	k1gInSc4	trup
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
lodě	loď	k1gFnPc4	loď
podobné	podobný	k2eAgFnSc2d1	podobná
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
brněnské	brněnský	k2eAgFnPc1d1	brněnská
dvoupalubové	dvoupalubový	k2eAgFnPc1d1	dvoupalubová
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgInP	mít
však	však	k9	však
být	být	k5eAaImF	být
o	o	k7c4	o
pět	pět	k4xCc4	pět
metrů	metr	k1gInPc2	metr
delší	dlouhý	k2eAgFnSc4d2	delší
a	a	k8xC	a
o	o	k7c4	o
1,2	[number]	k4	1,2
m	m	kA	m
užší	úzký	k2eAgNnSc1d2	užší
<g/>
.	.	kIx.	.
</s>
<s>
Poháněny	poháněn	k2eAgFnPc1d1	poháněna
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
dieselovými	dieselový	k2eAgInPc7d1	dieselový
motory	motor	k1gInPc7	motor
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
brněnských	brněnský	k2eAgInPc2d1	brněnský
elektromotorů	elektromotor	k1gInPc2	elektromotor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
první	první	k4xOgFnSc1	první
loď	loď	k1gFnSc1	loď
pro	pro	k7c4	pro
jiného	jiný	k2eAgMnSc4d1	jiný
provozovatele	provozovatel	k1gMnSc4	provozovatel
<g/>
.	.	kIx.	.
</s>
<s>
Plavidlo	plavidlo	k1gNnSc1	plavidlo
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgFnPc1d1	pojmenovaná
Družba	družba	k1gFnSc1	družba
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
předáno	předat	k5eAaPmNgNnS	předat
Československé	československý	k2eAgFnSc3d1	Československá
plavbě	plavba	k1gFnSc3	plavba
labsko-oderské	labskoderský	k2eAgFnSc2d1	labsko-oderská
(	(	kIx(	(
<g/>
ČSPLO	ČSPLO	kA	ČSPLO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stejného	stejný	k2eAgMnSc4d1	stejný
provozovatele	provozovatel	k1gMnSc4	provozovatel
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
další	další	k2eAgFnPc1d1	další
dvě	dva	k4xCgFnPc1	dva
lodě	loď	k1gFnPc1	loď
<g/>
:	:	kIx,	:
Mír	mír	k1gInSc1	mír
a	a	k8xC	a
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tři	tři	k4xCgNnPc1	tři
plavidla	plavidlo	k1gNnPc1	plavidlo
byla	být	k5eAaImAgNnP	být
nasazena	nasadit	k5eAaPmNgNnP	nasadit
na	na	k7c4	na
Slapskou	slapský	k2eAgFnSc4d1	Slapská
přehradu	přehrada	k1gFnSc4	přehrada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
už	už	k9	už
malé	malý	k2eAgFnSc2d1	malá
a	a	k8xC	a
staré	starý	k2eAgFnSc2d1	stará
lodě	loď	k1gFnSc2	loď
nestačily	stačit	k5eNaBmAgInP	stačit
poptávce	poptávka	k1gFnSc3	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
loď	loď	k1gFnSc1	loď
Žilina	Žilina	k1gFnSc1	Žilina
pro	pro	k7c4	pro
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
Oravskou	oravský	k2eAgFnSc4d1	Oravská
přehradu	přehrada	k1gFnSc4	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
pátá	pátý	k4xOgFnSc1	pátý
loď	loď	k1gFnSc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
obdržela	obdržet	k5eAaPmAgFnS	obdržet
jméno	jméno	k1gNnSc4	jméno
Solidarita	solidarita	k1gFnSc1	solidarita
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ČSPLO	ČSPLO	kA	ČSPLO
provozována	provozovat	k5eAaImNgNnP	provozovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lodní	lodní	k2eAgFnSc6d1	lodní
dopravě	doprava	k1gFnSc6	doprava
provozované	provozovaný	k2eAgFnSc2d1	provozovaná
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
platí	platit	k5eAaImIp3nS	platit
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
pásmový	pásmový	k2eAgInSc1d1	pásmový
tarif	tarif	k1gInSc1	tarif
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
plavební	plavební	k2eAgFnSc1d1	plavební
dráha	dráha	k1gFnSc1	dráha
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
čtyři	čtyři	k4xCgNnPc4	čtyři
pásma	pásmo	k1gNnPc4	pásmo
(	(	kIx(	(
<g/>
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
do	do	k7c2	do
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
na	na	k7c4	na
šest	šest	k4xCc4	šest
pásem	pásmo	k1gNnPc2	pásmo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
za	za	k7c4	za
jízdu	jízda	k1gFnSc4	jízda
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
pásmu	pásmo	k1gNnSc6	pásmo
platil	platit	k5eAaImAgMnS	platit
cestující	cestující	k1gMnSc1	cestující
2,50	[number]	k4	2,50
Kčs	Kčs	kA	Kčs
(	(	kIx(	(
<g/>
samozřejmě	samozřejmě	k6eAd1	samozřejmě
existovalo	existovat	k5eAaImAgNnS	existovat
i	i	k9	i
zlevněné	zlevněný	k2eAgNnSc1d1	zlevněné
jízdné	jízdné	k1gNnSc1	jízdné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
měnové	měnový	k2eAgFnSc6d1	měnová
reformě	reforma	k1gFnSc6	reforma
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
stála	stát	k5eAaImAgFnS	stát
přeprava	přeprava	k1gFnSc1	přeprava
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgNnSc2	jeden
pásma	pásmo	k1gNnSc2	pásmo
0,60	[number]	k4	0,60
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ale	ale	k8xC	ale
jízdné	jízdné	k1gNnSc1	jízdné
neodpovídalo	odpovídat	k5eNaImAgNnS	odpovídat
reálným	reálný	k2eAgMnSc7d1	reálný
nákladům	náklad	k1gInPc3	náklad
<g/>
,	,	kIx,	,
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
se	se	k3xPyFc4	se
cena	cena	k1gFnSc1	cena
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
pro	pro	k7c4	pro
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgNnSc2	jeden
pásma	pásmo	k1gNnSc2	pásmo
na	na	k7c4	na
1,20	[number]	k4	1,20
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tarif	tarif	k1gInSc1	tarif
platil	platit	k5eAaImAgInS	platit
na	na	k7c6	na
lodní	lodní	k2eAgFnSc6d1	lodní
dopravě	doprava	k1gFnSc6	doprava
až	až	k9	až
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
upravena	upravit	k5eAaPmNgFnS	upravit
nejen	nejen	k6eAd1	nejen
cena	cena	k1gFnSc1	cena
jízdného	jízdné	k1gNnSc2	jízdné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
počet	počet	k1gInSc1	počet
pásem	pásmo	k1gNnPc2	pásmo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zredukován	zredukovat	k5eAaPmNgInS	zredukovat
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
již	již	k6eAd1	již
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
tarif	tarif	k1gInSc1	tarif
zcela	zcela	k6eAd1	zcela
přepracován	přepracovat	k5eAaPmNgInS	přepracovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
pásmo	pásmo	k1gNnSc1	pásmo
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
plavební	plavební	k2eAgInSc1d1	plavební
úsek	úsek	k1gInSc1	úsek
mezi	mezi	k7c7	mezi
sousedními	sousední	k2eAgFnPc7d1	sousední
zastávkami	zastávka	k1gFnPc7	zastávka
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
celá	celý	k2eAgFnSc1d1	celá
nádrž	nádrž	k1gFnSc1	nádrž
je	být	k5eAaImIp3nS	být
rozdělena	rozdělen	k2eAgFnSc1d1	rozdělena
10	[number]	k4	10
pásem	pásmo	k1gNnPc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jízdu	jízda	k1gFnSc4	jízda
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
pásmu	pásmo	k1gNnSc6	pásmo
zaplatil	zaplatit	k5eAaPmAgMnS	zaplatit
cestující	cestující	k1gMnSc1	cestující
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
2	[number]	k4	2
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
bylo	být	k5eAaImAgNnS	být
jízdné	jízdné	k1gNnSc1	jízdné
zvyšováno	zvyšován	k2eAgNnSc1d1	zvyšováno
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevilo	objevit	k5eAaPmAgNnS	objevit
zvýhodněné	zvýhodněný	k2eAgNnSc1d1	zvýhodněné
jízdné	jízdné	k1gNnSc1	jízdné
pro	pro	k7c4	pro
okružní	okružní	k2eAgFnSc4d1	okružní
plavbu	plavba	k1gFnSc4	plavba
z	z	k7c2	z
Bystrce	Bystrc	k1gFnSc2	Bystrc
do	do	k7c2	do
Rokle	rokle	k1gFnSc2	rokle
či	či	k8xC	či
do	do	k7c2	do
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
tzv.	tzv.	kA	tzv.
rodinného	rodinný	k2eAgNnSc2d1	rodinné
jízdného	jízdné	k1gNnSc2	jízdné
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
dva	dva	k4xCgMnPc4	dva
dospělé	dospělí	k1gMnPc4	dospělí
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
jízdné	jízdné	k1gNnSc1	jízdné
postupně	postupně	k6eAd1	postupně
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
plavební	plavební	k2eAgFnSc6d1	plavební
sezóně	sezóna	k1gFnSc6	sezóna
2007	[number]	k4	2007
činila	činit	k5eAaImAgFnS	činit
cena	cena	k1gFnSc1	cena
jízdenky	jízdenka	k1gFnSc2	jízdenka
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
plavební	plavební	k2eAgInSc4d1	plavební
úsek	úsek	k1gInSc4	úsek
8	[number]	k4	8
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
15	[number]	k4	15
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k9	i
zvýhodněné	zvýhodněný	k2eAgNnSc4d1	zvýhodněné
okružní	okružní	k2eAgNnSc4d1	okružní
jízdné	jízdné	k1gNnSc4	jízdné
pro	pro	k7c4	pro
plavbu	plavba	k1gFnSc4	plavba
Bystrc	Bystrc	k1gFnSc1	Bystrc
–	–	k?	–
Hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgMnSc1d1	veveří
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
stejného	stejný	k2eAgInSc2d1	stejný
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
existence	existence	k1gFnSc1	existence
tzv.	tzv.	kA	tzv.
pojízdných	pojízdný	k2eAgFnPc2d1	pojízdná
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc4	ten
opravňují	opravňovat	k5eAaImIp3nP	opravňovat
jejich	jejich	k3xOp3gMnPc4	jejich
majitele	majitel	k1gMnPc4	majitel
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
okružní	okružní	k2eAgFnSc4d1	okružní
plavbu	plavba	k1gFnSc4	plavba
lodí	loď	k1gFnPc2	loď
z	z	k7c2	z
Bystrce	Bystrc	k1gFnSc2	Bystrc
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Veveří	veveří	k2eAgInSc4d1	veveří
(	(	kIx(	(
<g/>
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
až	až	k9	až
do	do	k7c2	do
Veverské	Veverský	k2eAgFnSc2d1	Veverská
Bítýšky	Bítýška	k1gFnSc2	Bítýška
(	(	kIx(	(
<g/>
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
brněnské	brněnský	k2eAgFnSc2d1	brněnská
zoologické	zoologický	k2eAgFnSc2d1	zoologická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
i	i	k9	i
druhý	druhý	k4xOgInSc1	druhý
druh	druh	k1gInSc1	druh
pojízdné	pojízdný	k2eAgFnSc2d1	pojízdná
vstupenky	vstupenka	k1gFnSc2	vstupenka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
jízdenka	jízdenka	k1gFnSc1	jízdenka
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
lodí	loď	k1gFnPc2	loď
z	z	k7c2	z
Bystrce	Bystrc	k1gFnSc2	Bystrc
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Veveří	veveří	k2eAgInSc4d1	veveří
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k9	jako
vstupenka	vstupenka	k1gFnSc1	vstupenka
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
Veveří	veveří	k2eAgInSc4d1	veveří
<g/>
.	.	kIx.	.
</s>
<s>
Lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
brněnského	brněnský	k2eAgInSc2d1	brněnský
systému	systém	k1gInSc2	systém
MHD	MHD	kA	MHD
ani	ani	k8xC	ani
IDS	IDS	kA	IDS
JMK	JMK	kA	JMK
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
tarif	tarif	k1gInSc4	tarif
<g/>
.	.	kIx.	.
</s>
<s>
Držitelé	držitel	k1gMnPc1	držitel
předplatních	předplatní	k2eAgFnPc2d1	předplatní
jízdenek	jízdenka	k1gFnPc2	jízdenka
IDS	IDS	kA	IDS
JMK	JMK	kA	JMK
pro	pro	k7c4	pro
brněnské	brněnský	k2eAgFnPc4d1	brněnská
zóny	zóna	k1gFnPc4	zóna
100	[number]	k4	100
+	+	kIx~	+
101	[number]	k4	101
však	však	k8xC	však
mají	mít	k5eAaImIp3nP	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
50	[number]	k4	50
<g/>
%	%	kIx~	%
slevu	sleva	k1gFnSc4	sleva
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
byli	být	k5eAaImAgMnP	být
cestující	cestující	k1gMnPc1	cestující
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
odbavováni	odbavovat	k5eAaImNgMnP	odbavovat
průvodčím	průvodčí	k1gMnSc7	průvodčí
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vydával	vydávat	k5eAaImAgMnS	vydávat
jízdenky	jízdenka	k1gFnSc2	jízdenka
a	a	k8xC	a
kleštičkami	kleštička	k1gFnPc7	kleštička
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
vyznačil	vyznačit	k5eAaPmAgInS	vyznačit
potřebné	potřebný	k2eAgInPc4d1	potřebný
údaje	údaj	k1gInPc4	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
se	se	k3xPyFc4	se
využívaly	využívat	k5eAaPmAgInP	využívat
mechanické	mechanický	k2eAgInPc1d1	mechanický
strojky	strojek	k1gInPc1	strojek
Setright	Setrighta	k1gFnPc2	Setrighta
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
především	především	k9	především
z	z	k7c2	z
linek	linka	k1gFnPc2	linka
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
ČSAD	ČSAD	kA	ČSAD
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
odbavování	odbavování	k1gNnSc2	odbavování
vydržel	vydržet	k5eAaPmAgInS	vydržet
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
již	již	k6eAd1	již
výhradně	výhradně	k6eAd1	výhradně
používá	používat	k5eAaImIp3nS	používat
elektronická	elektronický	k2eAgFnSc1d1	elektronická
pokladna	pokladna	k1gFnSc1	pokladna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
ovládá	ovládat	k5eAaImIp3nS	ovládat
průvodčí	průvodčí	k1gFnSc1	průvodčí
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
funguje	fungovat	k5eAaImIp3nS	fungovat
také	také	k9	také
klasická	klasický	k2eAgFnSc1d1	klasická
pokladna	pokladna	k1gFnSc1	pokladna
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
přístavišti	přístaviště	k1gNnSc6	přístaviště
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
tzv.	tzv.	kA	tzv.
pojízdných	pojízdný	k2eAgFnPc2d1	pojízdná
vstupenek	vstupenka	k1gFnPc2	vstupenka
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
vydávány	vydáván	k2eAgFnPc1d1	vydávána
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
předem	předem	k6eAd1	předem
vytištěné	vytištěný	k2eAgNnSc1d1	vytištěné
<g/>
,	,	kIx,	,
prodávající	prodávající	k2eAgMnSc1d1	prodávající
doplní	doplnit	k5eAaPmIp3nS	doplnit
razítkem	razítko	k1gNnSc7	razítko
pouze	pouze	k6eAd1	pouze
datum	datum	k1gInSc4	datum
<g/>
)	)	kIx)	)
na	na	k7c6	na
pokladnách	pokladna	k1gFnPc6	pokladna
v	v	k7c6	v
bystrckém	bystrcký	k2eAgNnSc6d1	bystrcké
přístavišti	přístaviště	k1gNnSc6	přístaviště
<g/>
,	,	kIx,	,
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Veveří	veveří	k2eAgMnSc1d1	veveří
a	a	k8xC	a
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Tučně	tučně	k6eAd1	tučně
jsou	být	k5eAaImIp3nP	být
vyznačeny	vyznačit	k5eAaPmNgFnP	vyznačit
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
