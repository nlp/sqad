<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
DPMB	DPMB	kA	DPMB
<g/>
)	)	kIx)	)
provozuje	provozovat	k5eAaImIp3nS	provozovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
