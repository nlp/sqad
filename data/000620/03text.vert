<s>
Antoni	Anton	k1gMnPc1	Anton
van	vana	k1gFnPc2	vana
Leeuwenhoek	Leeuwenhoek	k6eAd1	Leeuwenhoek
[	[	kIx(	[
<g/>
le	le	k?	le
<g/>
:	:	kIx,	:
<g/>
uvnhuk	uvnhuk	k1gMnSc1	uvnhuk
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1632	[number]	k4	1632
<g/>
,	,	kIx,	,
Delft	Delft	k1gInSc1	Delft
-	-	kIx~	-
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1723	[number]	k4	1723
<g/>
,	,	kIx,	,
Delft	Delft	k1gInSc1	Delft
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
mikroskopie	mikroskopie	k1gFnSc2	mikroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Občanským	občanský	k2eAgNnSc7d1	občanské
povoláním	povolání	k1gNnSc7	povolání
byl	být	k5eAaImAgMnS	být
obchodník	obchodník	k1gMnSc1	obchodník
s	s	k7c7	s
textilem	textil	k1gInSc7	textil
<g/>
,	,	kIx,	,
zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
a	a	k8xC	a
výrobce	výrobce	k1gMnSc1	výrobce
mikroskopů	mikroskop	k1gInPc2	mikroskop
<g/>
,	,	kIx,	,
vědeckému	vědecký	k2eAgInSc3d1	vědecký
výzkumu	výzkum	k1gInSc3	výzkum
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
amatér	amatér	k1gMnSc1	amatér
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
však	však	k9	však
výsledků	výsledek	k1gInPc2	výsledek
prvořadé	prvořadý	k2eAgFnSc2d1	prvořadá
důležitosti	důležitost	k1gFnSc2	důležitost
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
objevitelem	objevitel	k1gMnSc7	objevitel
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
krevních	krevní	k2eAgFnPc2d1	krevní
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
spermií	spermie	k1gFnPc2	spermie
<g/>
,	,	kIx,	,
svalových	svalový	k2eAgNnPc2d1	svalové
vláken	vlákno	k1gNnPc2	vlákno
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
mikroskopických	mikroskopický	k2eAgInPc2d1	mikroskopický
útvarů	útvar	k1gInPc2	útvar
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
otcem	otec	k1gMnSc7	otec
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
byl	být	k5eAaImAgInS	být
zaměstnán	zaměstnat	k5eAaPmNgInS	zaměstnat
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
s	s	k7c7	s
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
22	[number]	k4	22
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
nazpět	nazpět	k6eAd1	nazpět
do	do	k7c2	do
rodného	rodný	k2eAgInSc2d1	rodný
Delftu	Delft	k1gInSc2	Delft
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
založil	založit	k5eAaPmAgMnS	založit
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
zde	zde	k6eAd1	zde
celých	celý	k2eAgNnPc2d1	celé
70	[number]	k4	70
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
mikroskopy	mikroskop	k1gInPc4	mikroskop
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
vyráběl	vyrábět	k5eAaImAgMnS	vyrábět
<g/>
,	,	kIx,	,
dochované	dochovaný	k2eAgInPc1d1	dochovaný
kusy	kus	k1gInPc1	kus
mají	mít	k5eAaImIp3nP	mít
zvětšení	zvětšení	k1gNnSc4	zvětšení
až	až	k6eAd1	až
275	[number]	k4	275
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
svými	svůj	k3xOyFgInPc7	svůj
nejlepšími	dobrý	k2eAgInPc7d3	nejlepší
výrobky	výrobek	k1gInPc7	výrobek
mohl	moct	k5eAaImAgInS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k6eAd1	až
asi	asi	k9	asi
pětisetnásobného	pětisetnásobný	k2eAgNnSc2d1	pětisetnásobný
zvětšení	zvětšení	k1gNnSc2	zvětšení
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vyrábět	vyrábět	k5eAaImF	vyrábět
přesné	přesný	k2eAgFnPc4d1	přesná
skleněné	skleněný	k2eAgFnPc4d1	skleněná
kuličky	kulička	k1gFnPc4	kulička
nepatrných	nepatrný	k2eAgInPc2d1	nepatrný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
používal	používat	k5eAaImAgMnS	používat
jako	jako	k9	jako
čočky	čočka	k1gFnPc4	čočka
svých	svůj	k3xOyFgInPc2	svůj
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
překonal	překonat	k5eAaPmAgMnS	překonat
úroveň	úroveň	k1gFnSc4	úroveň
tehdy	tehdy	k6eAd1	tehdy
dostupné	dostupný	k2eAgFnPc1d1	dostupná
mikroskopické	mikroskopický	k2eAgFnPc1d1	mikroskopická
techniky	technika	k1gFnPc1	technika
<g/>
.	.	kIx.	.
</s>
<s>
Tajemství	tajemství	k1gNnSc4	tajemství
výroby	výroba	k1gFnSc2	výroba
si	se	k3xPyFc3	se
ovšem	ovšem	k9	ovšem
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
držel	držet	k5eAaImAgMnS	držet
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
zajistil	zajistit	k5eAaPmAgMnS	zajistit
vědecké	vědecký	k2eAgNnSc4d1	vědecké
prvenství	prvenství	k1gNnSc4	prvenství
a	a	k8xC	a
prestiž	prestiž	k1gFnSc4	prestiž
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
pod	pod	k7c7	pod
mikroskopem	mikroskop	k1gInSc7	mikroskop
listy	list	k1gInPc4	list
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
drobný	drobný	k2eAgInSc4d1	drobný
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
vlasy	vlas	k1gInPc1	vlas
<g/>
,	,	kIx,	,
lidskou	lidský	k2eAgFnSc7d1	lidská
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
sliny	slina	k1gFnPc4	slina
<g/>
,	,	kIx,	,
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
pepřový	pepřový	k2eAgInSc1d1	pepřový
nálev	nálev	k1gInSc1	nálev
<g/>
,	,	kIx,	,
sperma	sperma	k1gNnSc1	sperma
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
prvnímu	první	k4xOgMnSc3	první
člověku	člověk	k1gMnSc6	člověk
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
objevit	objevit	k5eAaPmF	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidská	lidský	k2eAgFnSc1d1	lidská
krev	krev	k1gFnSc1	krev
protéká	protékat	k5eAaImIp3nS	protékat
tenkými	tenký	k2eAgFnPc7d1	tenká
cévkami	cévka	k1gFnPc7	cévka
(	(	kIx(	(
<g/>
kapilárami	kapilára	k1gFnPc7	kapilára
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
popsat	popsat	k5eAaPmF	popsat
krevní	krevní	k2eAgFnPc4d1	krevní
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
jeho	jeho	k3xOp3gInSc1	jeho
velký	velký	k2eAgInSc1d1	velký
přínos	přínos	k1gInSc1	přínos
mikrobiologii	mikrobiologie	k1gFnSc3	mikrobiologie
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
roku	rok	k1gInSc2	rok
1676	[number]	k4	1676
poprvé	poprvé	k6eAd1	poprvé
uviděl	uvidět	k5eAaPmAgInS	uvidět
pod	pod	k7c7	pod
mikroskopem	mikroskop	k1gInSc7	mikroskop
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
označil	označit	k5eAaPmAgInS	označit
jako	jako	k9	jako
animalcules	animalcules	k1gInSc1	animalcules
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zvířátka	zvířátko	k1gNnPc4	zvířátko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1703	[number]	k4	1703
objevil	objevit	k5eAaPmAgInS	objevit
sladkovodní	sladkovodní	k2eAgInPc4d1	sladkovodní
polypy	polyp	k1gInPc4	polyp
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
výsledky	výsledek	k1gInPc4	výsledek
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
kvalitními	kvalitní	k2eAgFnPc7d1	kvalitní
kresbami	kresba	k1gFnPc7	kresba
mikroskopických	mikroskopický	k2eAgInPc2d1	mikroskopický
preparátů	preparát	k1gInPc2	preparát
<g/>
,	,	kIx,	,
publikoval	publikovat	k5eAaBmAgMnS	publikovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Philosophical	Philosophical	k1gFnSc2	Philosophical
Transactions	Transactionsa	k1gFnPc2	Transactionsa
britské	britský	k2eAgFnSc2d1	britská
učené	učený	k2eAgFnSc2d1	učená
společnosti	společnost	k1gFnSc2	společnost
The	The	k1gMnSc1	The
Royal	Royal	k1gMnSc1	Royal
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
Leeuwenhoek	Leeuwenhoek	k6eAd1	Leeuwenhoek
neměl	mít	k5eNaImAgInS	mít
příslušné	příslušný	k2eAgNnSc4d1	příslušné
vědecké	vědecký	k2eAgNnSc4d1	vědecké
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
jeho	jeho	k3xOp3gInPc1	jeho
popisy	popis	k1gInPc1	popis
mikroskopického	mikroskopický	k2eAgInSc2d1	mikroskopický
světa	svět	k1gInSc2	svět
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
neobratné	obratný	k2eNgFnPc1d1	neobratná
a	a	k8xC	a
naivní	naivní	k2eAgFnPc1d1	naivní
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
planetka	planetka	k1gFnSc1	planetka
(	(	kIx(	(
<g/>
2766	[number]	k4	2766
<g/>
)	)	kIx)	)
Leeuwenhoek	Leeuwenhoky	k1gFnPc2	Leeuwenhoky
<g/>
.	.	kIx.	.
</s>
