<s>
Antoni	Anton	k1gMnPc1	Anton
van	vana	k1gFnPc2	vana
Leeuwenhoek	Leeuwenhoek	k6eAd1	Leeuwenhoek
[	[	kIx(	[
<g/>
le	le	k?	le
<g/>
:	:	kIx,	:
<g/>
uvnhuk	uvnhuk	k1gMnSc1	uvnhuk
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1632	[number]	k4	1632
<g/>
,	,	kIx,	,
Delft	Delft	k1gInSc1	Delft
-	-	kIx~	-
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1723	[number]	k4	1723
<g/>
,	,	kIx,	,
Delft	Delft	k1gInSc1	Delft
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
mikroskopie	mikroskopie	k1gFnSc2	mikroskopie
<g/>
.	.	kIx.	.
</s>
