<s>
Skotská	skotský	k2eAgFnSc1d1
sypavka	sypavka	k1gFnSc1
douglasky	douglaska	k1gFnSc2
</s>
<s>
Skotská	skotský	k2eAgFnSc1d1
sypavka	sypavka	k1gFnSc1
douglasky	douglaska	k1gFnSc2
je	být	k5eAaImIp3nS
houbová	houbový	k2eAgFnSc1d1
choroba	choroba	k1gFnSc1
rostlin	rostlina	k1gFnPc2
způsobená	způsobený	k2eAgFnSc1d1
houbou	houba	k1gFnSc7
Rhabdocline	Rhabdoclin	k1gInSc5
pseudotsugae	pseudotsugae	k1gNnPc7
z	z	k7c2
čeledě	čeleď	k1gFnSc2
Hemiphacidiaceae	Hemiphacidiaceae	k1gNnSc1
řádu	řád	k1gInSc2
voskovičkotvaré	voskovičkotvarý	k2eAgNnSc1d1
(	(	kIx(
<g/>
Helotiales	Helotiales	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patogen	patogen	k1gInSc1
Rhabdocline	Rhabdoclin	k1gInSc5
pseudotsugae	pseudotsugae	k1gNnPc2
působí	působit	k5eAaImIp3nP
vážné	vážný	k2eAgNnSc4d1
poškození	poškození	k1gNnSc4
dřevin	dřevina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochází	docházet	k5eAaImIp3nS
<g/>
-li	-li	k?
k	k	k7c3
nákazám	nákaza	k1gFnPc3
jehlic	jehlice	k1gFnPc2
opakovaně	opakovaně	k6eAd1
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
až	až	k9
k	k	k7c3
odumření	odumření	k1gNnSc3
mladých	mladý	k2eAgInPc2d1
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickým	typický	k2eAgInSc7d1
projevem	projev	k1gInSc7
je	být	k5eAaImIp3nS
opad	opad	k1gInSc1
celých	celý	k2eAgInPc2d1
ročníků	ročník	k1gInPc2
jehlic	jehlice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Míra	Míra	k1gFnSc1
defoliace	defoliace	k1gFnSc1
závisí	záviset	k5eAaImIp3nS
na	na	k7c4
překrývání	překrývání	k1gNnSc4
fenologických	fenologický	k2eAgFnPc2d1
fází	fáze	k1gFnPc2
sypavky	sypavka	k1gFnSc2
a	a	k8xC
stupně	stupeň	k1gInSc2
počátku	počátek	k1gInSc2
rašení	rašení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
tyto	tento	k3xDgFnPc1
dvě	dva	k4xCgFnPc1
fáze	fáze	k1gFnPc1
nesetkají	setkat	k5eNaPmIp3nP
<g/>
,	,	kIx,
k	k	k7c3
vzniku	vznik	k1gInSc3
infekce	infekce	k1gFnSc2
nedochází	docházet	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Skotská	skotský	k2eAgFnSc1d1
sypavka	sypavka	k1gFnSc1
douglasky	douglaska	k1gFnSc2
a	a	k8xC
švýcarská	švýcarský	k2eAgFnSc1d1
sypavka	sypavka	k1gFnSc1
douglasky	douglaska	k1gFnSc2
jsou	být	k5eAaImIp3nP
choroby	choroba	k1gFnPc4
způsobované	způsobovaný	k2eAgFnPc4d1
houbovými	houbový	k2eAgInPc7d1
patogeny	patogen	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
infikují	infikovat	k5eAaBmIp3nP
pouze	pouze	k6eAd1
douglasku	douglaska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Infikované	infikovaný	k2eAgFnPc4d1
jehlice	jehlice	k1gFnPc4
žloutnou	žloutnout	k5eAaImIp3nP
<g/>
,	,	kIx,
hynou	hynout	k5eAaImIp3nP
a	a	k8xC
předčasně	předčasně	k6eAd1
opadávají	opadávat	k5eAaImIp3nP
ze	z	k7c2
stromu	strom	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
školkách	školka	k1gFnPc6
a	a	k8xC
u	u	k7c2
výsadeb	výsadba	k1gFnPc2
vánočních	vánoční	k2eAgInPc2d1
stromků	stromek	k1gInPc2
<g/>
,	,	kIx,
tyto	tento	k3xDgFnPc1
infekce	infekce	k1gFnPc1
mohou	moct	k5eAaImIp3nP
zničit	zničit	k5eAaPmF
estetickou	estetický	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
stromy	strom	k1gInPc1
staly	stát	k5eAaPmAgInP
neprodejnými	prodejný	k2eNgFnPc7d1
pro	pro	k7c4
použití	použití	k1gNnSc4
jako	jako	k8xS,k8xC
vánoční	vánoční	k2eAgInPc4d1
stromy	strom	k1gInPc4
nebo	nebo	k8xC
výsadby	výsadba	k1gFnPc4
v	v	k7c6
krajině	krajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
EPPO	EPPO	kA
kód	kód	k1gInSc1
</s>
<s>
RBDCPS	RBDCPS	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Synonyma	synonymum	k1gNnPc1
patogena	patogen	k2eAgNnPc1d1
</s>
<s>
Vědecké	vědecký	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Podle	podle	k7c2
biolib	bioliba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
je	být	k5eAaImIp3nS
pro	pro	k7c4
patogena	patogen	k1gMnSc4
s	s	k7c7
označením	označení	k1gNnSc7
Rhabdocline	Rhabdoclin	k1gInSc5
pseudotsugae	pseudotsugae	k1gFnSc4
používáno	používat	k5eAaImNgNnS
více	hodně	k6eAd2
rozdílných	rozdílný	k2eAgInPc2d1
názvů	název	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
Rhabdocline	Rhabdoclin	k1gInSc5
pseudotsugae	pseudotsugae	k1gFnSc1
subsp	subsp	k1gMnSc1
<g/>
.	.	kIx.
epiphylla	epiphylla	k1gMnSc1
nebo	nebo	k8xC
Rhabdocline	Rhabdoclin	k1gInSc5
pseudotsugae	pseudotsuga	k1gInPc4
subsp	subsp	k1gInSc1
<g/>
.	.	kIx.
pseudotsugae	pseudotsugae	k1gInSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
EPPO	EPPO	kA
uvádí	uvádět	k5eAaImIp3nS
i	i	k9
Dothistroma	Dothistroma	k1gFnSc1
rhabdoclinis	rhabdoclinis	k1gFnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zeměpisné	zeměpisný	k2eAgNnSc1d1
rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Choroba	choroba	k1gFnSc1
byla	být	k5eAaImAgFnS
prvně	prvně	k?
zjištěna	zjištěn	k2eAgFnSc1d1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
v	v	k7c6
r.	r.	kA
1911	#num#	k4
a	a	k8xC
již	již	k6eAd1
v	v	k7c6
r.	r.	kA
1914	#num#	k4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
doklady	doklad	k1gInPc1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
r.	r.	kA
1930	#num#	k4
z	z	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
Dánska	Dánsko	k1gNnSc2
a	a	k8xC
Holandska	Holandsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
v	v	k7c6
ČR	ČR	kA
</s>
<s>
Před	před	k7c7
r.	r.	kA
1938	#num#	k4
byla	být	k5eAaImAgFnS
choroba	choroba	k1gFnSc1
zjištěna	zjistit	k5eAaPmNgFnS
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
je	být	k5eAaImIp3nS
výskyt	výskyt	k1gInSc1
houby	houba	k1gFnSc2
Rhabdocline	Rhabdoclin	k1gInSc5
pseudotsugae	pseudotsugae	k1gFnPc3
v	v	k7c6
Česku	Česko	k1gNnSc6
opakovaně	opakovaně	k6eAd1
prokazován	prokazovat	k5eAaImNgInS
v	v	k7c6
přehoustlých	přehoustlý	k2eAgInPc6d1
mladých	mladý	k2eAgInPc6d1
porostech	porost	k1gInPc6
(	(	kIx(
<g/>
Dobříšsko	Dobříšsko	k1gNnSc4
<g/>
,	,	kIx,
Rožmitálsko	Rožmitálsko	k1gNnSc1
<g/>
,	,	kIx,
Jindřichohradecko	Jindřichohradecko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
novým	nový	k2eAgInPc3d1
nálezům	nález	k1gInPc3
obou	dva	k4xCgFnPc2
sypavek	sypavka	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
ČR	ČR	kA
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
věnovat	věnovat	k5eAaPmF,k5eAaImF
porostům	porost	k1gInPc3
douglasky	douglaska	k1gFnPc1
zvýšenou	zvýšený	k2eAgFnSc4d1
pozornost	pozornost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelze	lze	k6eNd1
ani	ani	k8xC
vyloučit	vyloučit	k5eAaPmF
<g/>
,	,	kIx,
chorobba	chorobba	k1gFnSc1
v	v	k7c6
ČR	ČR	kA
dosud	dosud	k6eAd1
unikala	unikat	k5eAaImAgFnS
pozornosti	pozornost	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
její	její	k3xOp3gInSc1
výskyt	výskyt	k1gInSc1
se	se	k3xPyFc4
zprvu	zprvu	k6eAd1
neprojevuje	projevovat	k5eNaImIp3nS
opadem	opad	k1gInSc7
infikovaného	infikovaný	k2eAgNnSc2d1
jehličí	jehličí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hostitel	hostitel	k1gMnSc1
</s>
<s>
douglaska	douglaska	k1gFnSc1
tisolistá	tisolistý	k2eAgFnSc1d1
Pseudotsuga	Pseudotsuga	k1gFnSc1
menziensii	menziensie	k1gFnSc4
<g/>
,	,	kIx,
citlivé	citlivý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
modrozelené	modrozelený	k2eAgInPc1d1
kultivary	kultivar	k1gInPc1
'	'	kIx"
<g/>
Glauca	Glauca	k1gFnSc1
<g/>
'	'	kIx"
a	a	k8xC
'	'	kIx"
<g/>
Glauca	Glauc	k2eAgFnSc1d1
Pendula	Pendula	k1gFnSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příznaky	příznak	k1gInPc1
</s>
<s>
opad	opad	k1gInSc1
jehlic	jehlice	k1gFnPc2
</s>
<s>
chlorotické	chlorotický	k2eAgFnPc1d1
skvrny	skvrna	k1gFnPc1
na	na	k7c6
jehlicích	jehlice	k1gFnPc6
</s>
<s>
fialovošedé	fialovošedý	k2eAgNnSc1d1
zbarvení	zbarvení	k1gNnSc1
jehlic	jehlice	k1gFnPc2
</s>
<s>
na	na	k7c4
spodní	spodní	k2eAgFnPc4d1
strany	strana	k1gFnPc4
jehlice	jehlice	k1gFnSc2
podél	podél	k7c2
hlavního	hlavní	k2eAgInSc2d1
nervu	nerv	k1gInSc2
se	se	k3xPyFc4
tvoří	tvořit	k5eAaImIp3nS
plodnice	plodnice	k1gFnSc1
</s>
<s>
drobné	drobný	k2eAgFnPc1d1
černé	černý	k2eAgFnPc1d1
pyknidy	pyknida	k1gFnPc1
v	v	k7c6
řadách	řada	k1gFnPc6
podél	podél	k7c2
středního	střední	k2eAgInSc2d1
nervu	nerv	k1gInSc2
jehlice	jehlice	k1gFnSc2
</s>
<s>
na	na	k7c6
stromě	strom	k1gInSc6
zůstávají	zůstávat	k5eAaImIp3nP
jen	jen	k9
jehlice	jehlice	k1gFnPc4
posledního	poslední	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
</s>
<s>
První	první	k4xOgInPc1
symptomy	symptom	k1gInPc1
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
v	v	k7c6
srpnu	srpen	k1gInSc6
až	až	k8xS
listopadu	listopad	k1gInSc6
v	v	k7c6
podobě	podoba	k1gFnSc6
drobných	drobný	k2eAgFnPc2d1
chlorotických	chlorotický	k2eAgFnPc2d1
skvrn	skvrna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
přechází	přecházet	k5eAaImIp3nP
do	do	k7c2
fialovošedého	fialovošedý	k2eAgNnSc2d1
mramorování	mramorování	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
jaře	jaro	k1gNnSc6
ještě	ještě	k6eAd1
výraznější	výrazný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbarvení	zbarvení	k1gNnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
šíří	šířit	k5eAaImIp3nS
<g/>
,	,	kIx,
až	až	k8xS
splývá	splývat	k5eAaImIp3nS
a	a	k8xC
jehlice	jehlice	k1gFnPc4
zhnědnou	zhnědnout	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
symptom	symptom	k1gInSc1
napadení	napadení	k1gNnSc2
je	být	k5eAaImIp3nS
popsány	popsat	k5eAaPmNgFnP
jeden	jeden	k4xCgInSc4
nebo	nebo	k8xC
více	hodně	k6eAd2
purpurově	purpurově	k6eAd1
hnědých	hnědý	k2eAgInPc2d1
pruhů	pruh	k1gInPc2
nebo	nebo	k8xC
skvrn	skvrna	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
zcela	zcela	k6eAd1
zřetelné	zřetelný	k2eAgFnPc1d1
a	a	k8xC
nachází	nacházet	k5eAaImIp3nP
se	se	k3xPyFc4
jak	jak	k6eAd1
na	na	k7c4
horní	horní	k2eAgFnSc4d1
<g/>
,	,	kIx,
tak	tak	k9
na	na	k7c6
spodní	spodní	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
jehlice	jehlice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
před	před	k7c7
rašením	rašení	k1gNnSc7
pupenů	pupen	k1gInPc2
oblasti	oblast	k1gFnSc2
s	s	k7c7
pruhy	pruh	k1gInPc7
začnou	začít	k5eAaPmIp3nP
bobtnat	bobtnat	k5eAaImF
a	a	k8xC
rozevře	rozevřít	k5eAaPmIp3nS
se	se	k3xPyFc4
podélně	podélně	k6eAd1
spodní	spodní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
infikovaných	infikovaný	k2eAgFnPc2d1
jehlic	jehlice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
se	se	k3xPyFc4
uvolní	uvolnit	k5eAaPmIp3nP
infekční	infekční	k2eAgFnPc1d1
spóry	spóra	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černé	Černé	k2eAgFnSc1d1
plodnice	plodnice	k1gFnSc1
(	(	kIx(
<g/>
pseudothecia	pseudothecia	k1gFnSc1
<g/>
)	)	kIx)
lze	lze	k6eAd1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
na	na	k7c6
spodní	spodní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
infikované	infikovaný	k2eAgFnPc4d1
jehlice	jehlice	k1gFnPc4
na	na	k7c6
jaře	jaro	k1gNnSc6
po	po	k7c6
infekci	infekce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
jediný	jediný	k2eAgInSc1d1
definitivní	definitivní	k2eAgInSc1d1
příznak	příznak	k1gInSc1
infekce	infekce	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
najít	najít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepší	dobrý	k2eAgInSc4d3
místo	místo	k1gNnSc1
pro	pro	k7c4
hledání	hledání	k1gNnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
části	část	k1gFnSc6
koruny	koruna	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
jehlicích	jehlice	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
již	již	k6eAd1
začaly	začít	k5eAaPmAgFnP
žloutnout	žloutnout	k5eAaImF
nebo	nebo	k8xC
hnědnout	hnědnout	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
zdát	zdát	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
malé	malý	k2eAgFnPc1d1
řady	řada	k1gFnPc1
černých	černý	k2eAgFnPc2d1
teček	tečka	k1gFnPc2
na	na	k7c4
spodní	spodní	k2eAgNnSc4d1
každé	každý	k3xTgFnPc4
infikované	infikovaný	k2eAgFnPc4d1
jehlice	jehlice	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k9
když	když	k8xS
houba	houba	k1gFnSc1
infikuje	infikovat	k5eAaBmIp3nS
nově	nově	k6eAd1
rašící	rašící	k2eAgFnPc4d1
jehlice	jehlice	k1gFnPc4
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
nezpůsobí	způsobit	k5eNaPmIp3nS
hned	hned	k6eAd1
opad	opad	k1gInSc1
jehlic	jehlice	k1gFnPc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
jsou	být	k5eAaImIp3nP
2	#num#	k4
až	až	k9
3	#num#	k4
roky	rok	k1gInPc1
staré	starý	k2eAgInPc1d1
<g/>
,	,	kIx,
takže	takže	k8xS
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
</s>
<s>
třeba	třeba	k6eAd1
zkoumat	zkoumat	k5eAaImF
dva	dva	k4xCgInPc4
až	až	k9
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
staré	starý	k2eAgFnSc2d1
jehlice	jehlice	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
nalézt	nalézt	k5eAaBmF,k5eAaPmF
důkazy	důkaz	k1gInPc4
o	o	k7c4
infekci	infekce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
až	až	k8xS
květnu	květen	k1gInSc6
(	(	kIx(
<g/>
červnu	červen	k1gInSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
podél	podél	k7c2
hlavního	hlavní	k2eAgInSc2d1
nervu	nerv	k1gInSc2
ze	z	k7c2
spodní	spodní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
jehlice	jehlice	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nP
žlutooranžové	žlutooranžový	k2eAgFnPc1d1
až	až	k8xS
hnědé	hnědý	k2eAgFnPc1d1
polštářkovité	polštářkovitý	k2eAgFnPc1d1
plodnice	plodnice	k1gFnPc1
(	(	kIx(
<g/>
apotecia	apotecia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
plodnic	plodnice	k1gFnPc2
se	se	k3xPyFc4
uvolňují	uvolňovat	k5eAaImIp3nP
se	se	k3xPyFc4
z	z	k7c2
nich	on	k3xPp3gFnPc2
oválné	oválný	k2eAgInPc4d1
<g/>
,	,	kIx,
vřetenovité	vřetenovitý	k2eAgInPc4d1
askospory	askospor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konidiové	konidiový	k2eAgFnPc4d1
stádium	stádium	k1gNnSc1
Rhabdogloeum	Rhabdogloeum	k1gInSc1
hypophyllum	hypophyllum	k1gNnSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
drobné	drobný	k2eAgFnPc4d1
černé	černý	k2eAgFnPc4d1
pyknidy	pyknida	k1gFnPc4
v	v	k7c6
řadách	řada	k1gFnPc6
podél	podél	k7c2
středního	střední	k2eAgInSc2d1
nervu	nerv	k1gInSc2
jehlice	jehlice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skotská	skotský	k2eAgFnSc1d1
sypavka	sypavka	k1gFnSc1
douglasky	douglaska	k1gFnSc2
postihuje	postihovat	k5eAaImIp3nS
vždy	vždy	k6eAd1
jeden	jeden	k4xCgInSc1
ročník	ročník	k1gInSc1
jehličí	jehličí	k1gNnSc2
a	a	k8xC
opakováním	opakování	k1gNnSc7
infekce	infekce	k1gFnSc2
se	se	k3xPyFc4
zkracují	zkracovat	k5eAaImIp3nP
přirůstající	přirůstající	k2eAgInPc1d1
výhony	výhon	k1gInPc1
a	a	k8xC
zůstávají	zůstávat	k5eAaImIp3nP
jehlice	jehlice	k1gFnPc4
pouze	pouze	k6eAd1
posledního	poslední	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Největší	veliký	k2eAgFnPc4d3
škody	škoda	k1gFnPc4
působí	působit	k5eAaImIp3nS
skotská	skotský	k2eAgFnSc1d1
sypavka	sypavka	k1gFnSc1
douglasky	douglaska	k1gFnSc2
ve	v	k7c6
školkách	školka	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
mladých	mladý	k2eAgInPc6d1
porostech	porost	k1gInPc6
ve	v	k7c6
věku	věk	k1gInSc6
5	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
především	především	k6eAd1
na	na	k7c6
plantážích	plantáž	k1gFnPc6
vánočních	vánoční	k2eAgInPc2d1
stromků	stromek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
napadených	napadený	k2eAgInPc6d1
porostech	porost	k1gInPc6
vznikají	vznikat	k5eAaImIp3nP
hospodářské	hospodářský	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
nejen	nejen	k6eAd1
snížením	snížení	k1gNnSc7
přírůstu	přírůst	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
následným	následný	k2eAgNnSc7d1
poškozením	poškození	k1gNnSc7
oslabených	oslabený	k2eAgInPc2d1
stromů	strom	k1gInPc2
a	a	k8xC
napadení	napadení	k1gNnSc2
chřadnoucích	chřadnoucí	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
dalšími	další	k2eAgInPc7d1
patogeny	patogen	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biologie	biologie	k1gFnSc1
</s>
<s>
Během	během	k7c2
jara	jaro	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
dubnu	duben	k1gInSc6
až	až	k8xS
červnu	červen	k1gInSc6
se	se	k3xPyFc4
podél	podél	k7c2
hlavního	hlavní	k2eAgInSc2d1
nervu	nerv	k1gInSc2
ze	z	k7c2
spodní	spodní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
jehlice	jehlice	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nP
a	a	k8xC
dozrávají	dozrávat	k5eAaImIp3nP
žlutooranžové	žlutooranžový	k2eAgFnPc1d1
až	až	k8xS
hnědé	hnědý	k2eAgFnPc1d1
polštářkovité	polštářkovitý	k2eAgFnPc1d1
plodničky	plodnička	k1gFnPc1
2	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
mm	mm	kA
velké	velká	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
nadzvedávají	nadzvedávat	k5eAaImIp3nP
pokožku	pokožka	k1gFnSc4
jehlic	jehlice	k1gFnPc2
a	a	k8xC
v	v	k7c6
době	doba	k1gFnSc6
zralosti	zralost	k1gFnSc2
se	se	k3xPyFc4
za	za	k7c2
vlhkého	vlhký	k2eAgNnSc2d1
počasí	počasí	k1gNnSc2
otevírají	otevírat	k5eAaImIp3nP
úzkou	úzký	k2eAgFnSc7d1
štěrbinou	štěrbina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
apotecií	apotecie	k1gFnPc2
se	se	k3xPyFc4
uvolňují	uvolňovat	k5eAaImIp3nP
askospory	askospora	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
infikují	infikovat	k5eAaBmIp3nP
rašící	rašící	k2eAgFnPc4d1
jehlice	jehlice	k1gFnPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výtrusy	výtrus	k1gInPc1
jsou	být	k5eAaImIp3nP
18	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
x	x	k?
6,5	6,5	k4
<g/>
–	–	k?
<g/>
7,5	7,5	k4
um	uma	k1gFnPc2
velké	velký	k2eAgFnPc4d1
<g/>
,	,	kIx,
oválné	oválný	k2eAgFnPc4d1
nebo	nebo	k8xC
vřetenovité	vřetenovitý	k2eAgFnPc4d1
<g/>
,	,	kIx,
až	až	k6eAd1
při	při	k7c6
úplné	úplný	k2eAgFnSc3d1
zralosti	zralost	k1gFnSc3
rozdělené	rozdělená	k1gFnSc2
příčnou	příčný	k2eAgFnSc7d1
přehrádkou	přehrádka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
napadeném	napadený	k2eAgNnSc6d1
jehličí	jehličí	k1gNnSc6
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
i	i	k9
konidiové	konidiový	k2eAgNnSc1d1
stadium	stadium	k1gNnSc1
Rhabdogloeum	Rhabdogloeum	k1gInSc1
hypophyllum	hypophyllum	k1gNnSc1
Ellis	Ellis	k1gInSc1
et	et	k?
Gil	Gil	k1gFnSc2
<g/>
.	.	kIx.
s	s	k7c7
pyknidami	pyknida	k1gFnPc7
uspořádanými	uspořádaný	k2eAgFnPc7d1
v	v	k7c6
řadách	řada	k1gFnPc6
podél	podél	k7c2
středního	střední	k2eAgInSc2d1
nervu	nerv	k1gInSc2
jehlice	jehlice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jehličí	jehličí	k1gNnSc1
napadené	napadený	k2eAgNnSc1d1
skotskou	skotský	k2eAgFnSc7d1
sypavkou	sypavka	k1gFnSc7
v	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
odumírá	odumírat	k5eAaImIp3nS
a	a	k8xC
na	na	k7c4
podzim	podzim	k1gInSc4
postupně	postupně	k6eAd1
opadává	opadávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
opakované	opakovaný	k2eAgFnSc6d1
infekci	infekce	k1gFnSc6
po	po	k7c4
několik	několik	k4yIc4
let	léto	k1gNnPc2
zůstávají	zůstávat	k5eAaImIp3nP
na	na	k7c6
větvích	větev	k1gFnPc6
douglasek	douglaska	k1gFnPc2
jen	jen	k9
jehlice	jehlice	k1gFnSc1
posledního	poslední	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
<g/>
,	,	kIx,
výhony	výhon	k1gInPc1
jsou	být	k5eAaImIp3nP
většinou	většina	k1gFnSc7
velmi	velmi	k6eAd1
krátké	krátký	k2eAgNnSc1d1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Šíření	šíření	k1gNnSc1
</s>
<s>
Větrem	vítr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Ochrana	ochrana	k1gFnSc1
rostlin	rostlina	k1gFnPc2
</s>
<s>
Chemické	chemický	k2eAgInPc1d1
postřiky	postřik	k1gInPc1
se	se	k3xPyFc4
provádějí	provádět	k5eAaImIp3nP
pouze	pouze	k6eAd1
u	u	k7c2
citlivých	citlivý	k2eAgInPc2d1
kultivarů	kultivar	k1gInPc2
při	při	k7c6
aktuálním	aktuální	k2eAgInSc6d1
výskytu	výskyt	k1gInSc6
a	a	k8xC
to	ten	k3xDgNnSc4
v	v	k7c6
malých	malý	k2eAgFnPc6d1
okrasných	okrasný	k2eAgFnPc6d1
výsadbách	výsadba	k1gFnPc6
<g/>
,	,	kIx,
popř.	popř.	kA
ve	v	k7c6
školkách	školka	k1gFnPc6
<g/>
,	,	kIx,
spektrum	spektrum	k1gNnSc4
používaných	používaný	k2eAgInPc2d1
fungicidů	fungicid	k1gInPc2
je	být	k5eAaImIp3nS
stejné	stejný	k2eAgNnSc1d1
jako	jako	k9
u	u	k7c2
sypavek	sypavka	k1gFnPc2
borovice	borovice	k1gFnSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
EPPO	EPPO	kA
<g/>
,	,	kIx,
RBDCPS	RBDCPS	kA
<g/>
↑	↑	k?
biolib	biolib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
PEŠKOVÁ	Pešková	k1gFnSc1
<g/>
,	,	kIx,
Vítězslava	Vítězslava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebezpečné	bezpečný	k2eNgFnPc4d1
sypavky	sypavka	k1gFnPc4
na	na	k7c6
douglasce	douglaska	k1gFnSc6
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
ČERMÁK	Čermák	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
poškození	poškození	k1gNnSc2
dřevin	dřevina	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mendelu	mendela	k1gFnSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
plantclinic	plantclinice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cornell	cornell	k1gInSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
popis	popis	k1gInSc1
<g/>
,	,	kIx,
obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
plantclinic	plantclinice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cornell	cornell	k1gInSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
popis	popis	k1gInSc1
<g/>
,	,	kIx,
forestry-dev	forestry-dev	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
