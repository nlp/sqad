<s>
Skotská	skotský	k2eAgFnSc1d1
sypavka	sypavka	k1gFnSc1
douglasky	douglaska	k1gFnSc2
je	být	k5eAaImIp3nS
houbová	houbový	k2eAgFnSc1d1
choroba	choroba	k1gFnSc1
rostlin	rostlina	k1gFnPc2
způsobená	způsobený	k2eAgFnSc1d1
houbou	houba	k1gFnSc7
Rhabdocline	Rhabdoclin	k1gInSc5
pseudotsugae	pseudotsugae	k1gNnPc7
z	z	k7c2
čeledě	čeleď	k1gFnSc2
Hemiphacidiaceae	Hemiphacidiaceae	k1gNnSc1
řádu	řád	k1gInSc2
voskovičkotvaré	voskovičkotvarý	k2eAgNnSc1d1
(	(	kIx(
<g/>
Helotiales	Helotiales	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>