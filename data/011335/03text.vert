<p>
<s>
Inner	Inner	k1gMnSc1	Inner
Space	Space	k1gFnSc2	Space
Explorers	Explorers	k1gInSc1	Explorers
(	(	kIx(	(
<g/>
ISE	ISE	kA	ISE
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
potápěčská	potápěčský	k2eAgFnSc1d1	potápěčská
organizace	organizace	k1gFnSc1	organizace
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
filosofie	filosofie	k1gFnSc2	filosofie
Doing	Doing	k1gMnSc1	Doing
It	It	k1gMnSc1	It
Right	Right	k1gMnSc1	Right
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
bývalým	bývalý	k2eAgMnSc7d1	bývalý
instruktorem	instruktor	k1gMnSc7	instruktor
GUE	GUE	kA	GUE
Achimem	Achim	k1gMnSc7	Achim
R.	R.	kA	R.
Schlöffelem	Schlöffel	k1gMnSc7	Schlöffel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kurzy	kurz	k1gInPc1	kurz
ISE	ISE	kA	ISE
==	==	k?	==
</s>
</p>
<p>
<s>
Úvodní	úvodní	k2eAgInSc1d1	úvodní
</s>
</p>
<p>
<s>
Requalification	Requalification	k1gInSc1	Requalification
</s>
</p>
<p>
<s>
Basics	Basics	k1gInSc1	Basics
of	of	k?	of
Exploration	Exploration	k1gInSc1	Exploration
(	(	kIx(	(
<g/>
BOE	BOE	kA	BOE
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sidemount	Sidemount	k1gMnSc1	Sidemount
</s>
</p>
<p>
<s>
Basics	Basics	k1gInSc1	Basics
of	of	k?	of
Exploration	Exploration	k1gInSc1	Exploration
Extended	Extended	k1gMnSc1	Extended
(	(	kIx(	(
<g/>
BoE	BoE	k1gMnSc1	BoE
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
<g/>
Technické	technický	k2eAgNnSc1d1	technické
</s>
</p>
<p>
<s>
Exploration	Exploration	k1gInSc1	Exploration
Diver	Diver	k1gInSc1	Diver
Level	level	k1gInSc4	level
I	i	k9	i
</s>
</p>
<p>
<s>
Exploration	Exploration	k1gInSc1	Exploration
Diver	Diver	k1gInSc1	Diver
Level	level	k1gInSc4	level
II	II	kA	II
</s>
</p>
<p>
<s>
Exploration	Exploration	k1gInSc1	Exploration
Diver	Diver	k1gInSc1	Diver
Level	level	k1gInSc4	level
III	III	kA	III
</s>
</p>
<p>
<s>
Wreck	Wreck	k1gInSc1	Wreck
Explorer	Explorer	k1gInSc1	Explorer
Level	level	k1gInSc4	level
I	i	k9	i
</s>
</p>
<p>
<s>
Wreck	Wreck	k1gInSc1	Wreck
Explorer	Explorer	k1gInSc1	Explorer
Level	level	k1gInSc4	level
II	II	kA	II
</s>
</p>
<p>
<s>
Wreck	Wreck	k1gInSc1	Wreck
Explorer	Explorer	k1gInSc1	Explorer
Level	level	k1gInSc4	level
III	III	kA	III
</s>
</p>
<p>
<s>
Cave	Cave	k1gInSc1	Cave
Explorer	Explorer	k1gInSc1	Explorer
Level	level	k1gInSc4	level
I	i	k9	i
</s>
</p>
<p>
<s>
Cave	Cave	k1gInSc1	Cave
Explorer	Explorer	k1gInSc1	Explorer
Level	level	k1gInSc4	level
II	II	kA	II
</s>
</p>
<p>
<s>
Cave	Cave	k6eAd1	Cave
Explorer	Explorer	k1gInSc1	Explorer
Level	level	k1gInSc1	level
IIIRebreather	IIIRebreathra	k1gFnPc2	IIIRebreathra
</s>
</p>
<p>
<s>
Rebreather	Rebreathra	k1gFnPc2	Rebreathra
Exploration	Exploration	k1gInSc1	Exploration
Diver	Diver	k1gMnSc1	Diver
</s>
</p>
<p>
<s>
Oxygen	oxygen	k1gInSc1	oxygen
Rebreather	Rebreathra	k1gFnPc2	Rebreathra
Diver	Divra	k1gFnPc2	Divra
</s>
</p>
<p>
<s>
MCCR	MCCR	kA	MCCR
Diver	Diver	k1gMnSc1	Diver
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
Inner	Innra	k1gFnPc2	Innra
Space	Spaec	k1gInSc2	Spaec
Explorers	Explorersa	k1gFnPc2	Explorersa
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
American	American	k1gInSc1	American
Nitrox	Nitrox	k1gInSc1	Nitrox
Divers	Divers	k1gInSc4	Divers
International	International	k1gFnSc2	International
</s>
</p>
<p>
<s>
Global	globat	k5eAaImAgInS	globat
Underwater	Underwater	k1gMnSc1	Underwater
Explorers	Explorersa	k1gFnPc2	Explorersa
</s>
</p>
<p>
<s>
Technical	Technicat	k5eAaPmAgInS	Technicat
Diving	Diving	k1gInSc1	Diving
International	International	k1gFnSc2	International
</s>
</p>
<p>
<s>
Unified	Unified	k1gInSc1	Unified
Team	team	k1gInSc1	team
Diving	Diving	k1gInSc4	Diving
</s>
</p>
