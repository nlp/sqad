<p>
<s>
Dětský	dětský	k2eAgInSc1d1	dětský
fond	fond	k1gInSc1	fond
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
United	United	k1gInSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Children	Childrna	k1gFnPc2	Childrna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Fund	fund	k1gInSc1	fund
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgFnSc1d3	veliký
světová	světový	k2eAgFnSc1d1	světová
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
celosvětově	celosvětově	k6eAd1	celosvětově
zabývá	zabývat	k5eAaImIp3nS	zabývat
ochranou	ochrana	k1gFnSc7	ochrana
a	a	k8xC	a
zlepšováním	zlepšování	k1gNnSc7	zlepšování
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
podporou	podpora	k1gFnSc7	podpora
jejich	jejich	k3xOp3gInSc2	jejich
všestranného	všestranný	k2eAgInSc2d1	všestranný
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgInS	nazývat
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
dětský	dětský	k2eAgInSc1d1	dětský
fond	fond	k1gInSc1	fond
neodkladné	odkladný	k2eNgFnSc2d1	neodkladná
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
originálu	originál	k1gInSc2	originál
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
United	United	k1gInSc1	United
Nations	Nations	k1gInSc1	Nations
International	International	k1gFnPc2	International
Children	Childrna	k1gFnPc2	Childrna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Emergency	Emergenc	k1gMnPc7	Emergenc
Fund	fund	k1gInSc4	fund
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zkratka	zkratka	k1gFnSc1	zkratka
UNICEF	UNICEF	kA	UNICEF
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
158	[number]	k4	158
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Organizace	organizace	k1gFnSc1	organizace
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
jako	jako	k8xC	jako
podpora	podpora	k1gFnSc1	podpora
proti	proti	k7c3	proti
utrpení	utrpení	k1gNnSc3	utrpení
dětí	dítě	k1gFnPc2	dítě
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
fond	fond	k1gInSc1	fond
stal	stát	k5eAaPmAgInS	stát
trvalou	trvalý	k2eAgFnSc7d1	trvalá
součástí	součást	k1gFnSc7	součást
OSN	OSN	kA	OSN
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
přejmenování	přejmenování	k1gNnSc3	přejmenování
na	na	k7c4	na
United	United	k1gInSc4	United
Nations	Nationsa	k1gFnPc2	Nationsa
Children	Childrna	k1gFnPc2	Childrna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Fund	fund	k1gInSc1	fund
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
zkratka	zkratka	k1gFnSc1	zkratka
UNICEF	UNICEF	kA	UNICEF
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
míru	mír	k1gInSc2	mír
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sídlo	sídlo	k1gNnSc4	sídlo
a	a	k8xC	a
vedení	vedení	k1gNnSc4	vedení
==	==	k?	==
</s>
</p>
<p>
<s>
UNICEF	UNICEF	kA	UNICEF
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
sedm	sedm	k4xCc4	sedm
regionálních	regionální	k2eAgFnPc2d1	regionální
kanceláří	kancelář	k1gFnPc2	kancelář
a	a	k8xC	a
zastoupení	zastoupení	k1gNnSc2	zastoupení
ve	v	k7c6	v
158	[number]	k4	158
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
37	[number]	k4	37
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
tzv.	tzv.	kA	tzv.
národní	národní	k2eAgInPc1d1	národní
výbory	výbor	k1gInPc1	výbor
pro	pro	k7c4	pro
UNICEF	UNICEF	kA	UNICEF
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
popularizaci	popularizace	k1gFnSc3	popularizace
práv	právo	k1gNnPc2	právo
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
získávání	získávání	k1gNnSc2	získávání
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
pro	pro	k7c4	pro
činnosti	činnost	k1gFnPc4	činnost
UNICEF	UNICEF	kA	UNICEF
v	v	k7c6	v
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
UNICEF	UNICEF	kA	UNICEF
řídí	řídit	k5eAaImIp3nS	řídit
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
36	[number]	k4	36
členů	člen	k1gMnPc2	člen
a	a	k8xC	a
která	který	k3yQgFnSc1	který
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
konsensem	konsens	k1gInSc7	konsens
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
vede	vést	k5eAaImIp3nS	vést
UNICEF	UNICEF	kA	UNICEF
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc1	čtyři
viceprezidenti	viceprezident	k1gMnPc1	viceprezident
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gMnPc7	člen
Výkonné	výkonný	k2eAgFnSc2d1	výkonná
rady	rada	k1gFnSc2	rada
nominují	nominovat	k5eAaBmIp3nP	nominovat
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vlády	vláda	k1gFnPc1	vláda
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každá	každý	k3xTgFnSc1	každý
část	část	k1gFnSc1	část
světa	svět	k1gInSc2	svět
má	mít	k5eAaImIp3nS	mít
stanoven	stanoven	k2eAgInSc1d1	stanoven
počet	počet	k1gInSc1	počet
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
má	mít	k5eAaImIp3nS	mít
rezervováno	rezervovat	k5eAaBmNgNnS	rezervovat
8	[number]	k4	8
křesel	křeslo	k1gNnPc2	křeslo
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc1	Asie
7	[number]	k4	7
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
Evropa	Evropa	k1gFnSc1	Evropa
4	[number]	k4	4
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
,	,	kIx,	,
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
a	a	k8xC	a
Karibik	Karibik	k1gInSc1	Karibik
5	[number]	k4	5
křesel	křeslo	k1gNnPc2	křeslo
<g/>
,	,	kIx,	,
západní	západní	k2eAgFnSc1d1	západní
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
státy	stát	k1gInPc4	stát
12	[number]	k4	12
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
založení	založení	k1gNnSc2	založení
UNICEF	UNICEF	kA	UNICEF
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
se	se	k3xPyFc4	se
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
organizace	organizace	k1gFnSc2	organizace
vystřídaly	vystřídat	k5eAaPmAgFnP	vystřídat
tyto	tento	k3xDgFnPc1	tento
osobnosti	osobnost	k1gFnPc1	osobnost
</s>
</p>
<p>
<s>
Maurice	Maurika	k1gFnSc3	Maurika
Pate	pat	k1gInSc5	pat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
</s>
</p>
<p>
<s>
Henry	Henry	k1gMnSc1	Henry
R.	R.	kA	R.
Labouisse	Labouisse	k1gFnPc1	Labouisse
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
P.	P.	kA	P.
Grant	grant	k1gInSc1	grant
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
Carol	Carol	k1gInSc1	Carol
Bellamy	Bellam	k1gInPc1	Bellam
–	–	k?	–
současná	současný	k2eAgFnSc1d1	současná
prezidentka	prezidentka	k1gFnSc1	prezidentka
</s>
</p>
<p>
<s>
==	==	k?	==
Český	český	k2eAgInSc1d1	český
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
UNICEF	UNICEF	kA	UNICEF
==	==	k?	==
</s>
</p>
<p>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
jako	jako	k8xS	jako
nevládní	vládní	k2eNgFnSc1d1	nevládní
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Dětského	dětský	k2eAgInSc2d1	dětský
fondu	fond	k1gInSc2	fond
OSN	OSN	kA	OSN
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
si	se	k3xPyFc3	se
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
cíle	cíl	k1gInPc4	cíl
vytkl	vytknout	k5eAaPmAgMnS	vytknout
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
informovat	informovat	k5eAaBmF	informovat
veřejnost	veřejnost	k1gFnSc4	veřejnost
o	o	k7c4	o
poslání	poslání	k1gNnSc4	poslání
a	a	k8xC	a
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
činnosti	činnost	k1gFnPc4	činnost
UNICEF	UNICEF	kA	UNICEF
</s>
</p>
<p>
<s>
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
dětem	dítě	k1gFnPc3	dítě
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
upozorňovat	upozorňovat	k5eAaImF	upozorňovat
veřejnost	veřejnost	k1gFnSc4	veřejnost
na	na	k7c4	na
situaci	situace	k1gFnSc4	situace
dětí	dítě	k1gFnPc2	dítě
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
i	i	k8xC	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
podporovat	podporovat	k5eAaImF	podporovat
naplňování	naplňování	k1gNnSc4	naplňování
dětských	dětský	k2eAgNnPc2d1	dětské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dětský	dětský	k2eAgInSc4d1	dětský
fond	fond	k1gInSc4	fond
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
UNICEF	UNICEF	kA	UNICEF
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc4	stránka
Českého	český	k2eAgInSc2d1	český
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
UNICEF	UNICEF	kA	UNICEF
</s>
</p>
