<s>
Dornier	Dornier	k1gInSc1
Do	do	kA
215	#num#	k4
byl	být	k5eAaImAgInS
německý	německý	k2eAgInSc1d1
lehký	lehký	k2eAgInSc1d1
bombardér	bombardér	k1gInSc1
<g/>
,	,	kIx,
průzkumný	průzkumný	k2eAgInSc1d1
letoun	letoun	k1gInSc1
a	a	k8xC
později	pozdě	k6eAd2
noční	noční	k2eAgMnSc1d1
stíhač	stíhač	k1gMnSc1
původně	původně	k6eAd1
vyráběný	vyráběný	k2eAgMnSc1d1
pro	pro	k7c4
export	export	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
však	však	k9
téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc1
letouny	letoun	k1gInPc1
sloužily	sloužit	k5eAaImAgInP
v	v	k7c6
Luftwaffe	Luftwaff	k1gMnSc6
<g/>
.	.	kIx.
</s>