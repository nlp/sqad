<s>
Dornier	Dornier	k1gInSc1
Do	do	k7c2
215	#num#	k4
</s>
<s>
Do	do	k7c2
215	#num#	k4
Určení	určení	k1gNnPc2
</s>
<s>
Bombardér	bombardér	k1gInSc1
<g/>
/	/	kIx~
<g/>
noční	noční	k2eAgMnSc1d1
stíhač	stíhač	k1gMnSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Dornier	Dornier	k1gMnSc1
První	první	k4xOgMnSc1
let	léto	k1gNnPc2
</s>
<s>
1938	#num#	k4
Zařazeno	zařazen	k2eAgNnSc4d1
</s>
<s>
1939	#num#	k4
Vyřazeno	vyřadit	k5eAaPmNgNnS
</s>
<s>
1944	#num#	k4
Uživatel	uživatel	k1gMnSc1
</s>
<s>
Luftwaffe	Luftwaff	k1gMnSc5
Výroba	výroba	k1gFnSc1
</s>
<s>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
105	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vyvinuto	vyvinout	k5eAaPmNgNnS
z	z	k7c2
typu	typ	k1gInSc2
</s>
<s>
Dornier	Dornier	k1gInSc1
Do	do	k7c2
17	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dornier	Dornier	k1gInSc1
Do	do	kA
215	#num#	k4
byl	být	k5eAaImAgInS
německý	německý	k2eAgInSc1d1
lehký	lehký	k2eAgInSc1d1
bombardér	bombardér	k1gInSc1
<g/>
,	,	kIx,
průzkumný	průzkumný	k2eAgInSc1d1
letoun	letoun	k1gInSc1
a	a	k8xC
později	pozdě	k6eAd2
noční	noční	k2eAgMnSc1d1
stíhač	stíhač	k1gMnSc1
původně	původně	k6eAd1
vyráběný	vyráběný	k2eAgMnSc1d1
pro	pro	k7c4
export	export	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nakonec	nakonec	k6eAd1
však	však	k9
téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc1
letouny	letoun	k1gInPc1
sloužily	sloužit	k5eAaImAgInP
v	v	k7c6
Luftwaffe	Luftwaff	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následníkem	následník	k1gMnSc7
strojů	stroj	k1gInPc2
Do	do	k7c2
215	#num#	k4
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
Do	do	k7c2
217	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Do	do	k7c2
215	#num#	k4
za	za	k7c2
letu	let	k1gInSc2
<g/>
,	,	kIx,
cca	cca	kA
1941	#num#	k4
</s>
<s>
Po	po	k7c6
úspěšném	úspěšný	k2eAgNnSc6d1
vystoupení	vystoupení	k1gNnSc6
letounu	letoun	k1gInSc2
Dornier	Dornira	k1gFnPc2
Do	do	k7c2
17	#num#	k4
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
v	v	k7c6
Curychu	Curych	k1gInSc6
se	s	k7c7
RLM	RLM	kA
rozhodlo	rozhodnout	k5eAaPmAgNnS
přidělit	přidělit	k5eAaPmF
jednomu	jeden	k4xCgNnSc3
Do	do	k7c2
17Z-0	17Z-0	k4
z	z	k7c2
ověřovací	ověřovací	k2eAgFnSc2d1
série	série	k1gFnSc2
nové	nový	k2eAgNnSc4d1
označení	označení	k1gNnSc4
Do	do	k7c2
215	#num#	k4
V1	V1	k1gFnPc2
(	(	kIx(
<g/>
D-AIIB	D-AIIB	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
měl	mít	k5eAaImAgInS
sloužit	sloužit	k5eAaImF
k	k	k7c3
propagaci	propagace	k1gFnSc4
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
export	export	k1gInSc4
pro	pro	k7c4
Jugoslávii	Jugoslávie	k1gFnSc4
byl	být	k5eAaImAgInS
určen	určit	k5eAaPmNgInS
další	další	k2eAgInSc1d1
Do	do	k7c2
17	#num#	k4
<g/>
Z-	Z-	k1gFnPc2
<g/>
0	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
obdržel	obdržet	k5eAaPmAgInS
dvouhvězdicové	dvouhvězdicový	k2eAgFnPc4d1
čtrnáctiválcové	čtrnáctiválcový	k2eAgFnPc4d1
vzduchem	vzduch	k1gInSc7
chlazené	chlazený	k2eAgFnSc2d1
pohonné	pohonný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
Gnome-Rhône	Gnome-Rhôn	k1gInSc5
14N	14N	k4
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stroj	stroj	k1gInSc1
pak	pak	k6eAd1
dostal	dostat	k5eAaPmAgInS
označení	označení	k1gNnSc4
Do	do	k7c2
215	#num#	k4
V2	V2	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgInS
předveden	předvést	k5eAaPmNgInS
jugoslávským	jugoslávský	k2eAgMnPc3d1
představitelům	představitel	k1gMnPc3
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Druhý	druhý	k4xOgInSc1
prototyp	prototyp	k1gInSc1
však	však	k9
nevykazoval	vykazovat	k5eNaImAgInS
lepší	dobrý	k2eAgInPc4d2
výkony	výkon	k1gInPc4
než	než	k8xS
jugoslávské	jugoslávský	k2eAgFnPc4d1
licenční	licenční	k2eAgFnSc1d1
Do	do	k7c2
17	#num#	k4
K.	K.	kA
Říšské	říšský	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
letectví	letectví	k1gNnSc2
proto	proto	k8xC
rozhodlo	rozhodnout	k5eAaPmAgNnS
o	o	k7c6
stavbě	stavba	k1gFnSc6
třetího	třetí	k4xOgInSc2
prototypu	prototyp	k1gInSc2
Do	do	k7c2
215	#num#	k4
V3	V3	k1gFnPc2
s	s	k7c7
řadovými	řadový	k2eAgInPc7d1
dvanáctiválcovými	dvanáctiválcový	k2eAgInPc7d1
kapalinou	kapalina	k1gFnSc7
chlazenými	chlazený	k2eAgInPc7d1
motory	motor	k1gInPc7
Daimler-Benz	Daimler-Benza	k1gFnPc2
DB	db	kA
601	#num#	k4
A	a	k8xC
o	o	k7c6
vzletovém	vzletový	k2eAgInSc6d1
výkonu	výkon	k1gInSc6
809	#num#	k4
kW	kW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
1939	#num#	k4
stroj	stroj	k1gInSc1
s	s	k7c7
lepšími	dobrý	k2eAgInPc7d2
výkony	výkon	k1gInPc7
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
Do	do	k7c2
17	#num#	k4
Z	z	k7c2
oceňovaly	oceňovat	k5eAaImAgFnP
četné	četný	k2eAgFnPc1d1
zahraniční	zahraniční	k2eAgFnPc1d1
komise	komise	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
1939	#num#	k4
proto	proto	k8xC
přišla	přijít	k5eAaPmAgFnS
zahraniční	zahraniční	k2eAgFnSc1d1
objednávka	objednávka	k1gFnSc1
na	na	k7c4
stavbu	stavba	k1gFnSc4
18	#num#	k4
kusů	kus	k1gInPc2
Do	do	k7c2
215	#num#	k4
A-1	A-1	k1gFnPc2
pro	pro	k7c4
Švédsko	Švédsko	k1gNnSc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
výroba	výroba	k1gFnSc1
se	se	k3xPyFc4
rozběhla	rozběhnout	k5eAaPmAgFnS
koncem	koncem	k7c2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flygvapnet	Flygvapnet	k1gInSc4
však	však	k9
po	po	k7c6
uvalení	uvalení	k1gNnSc6
německého	německý	k2eAgNnSc2d1
embarga	embargo	k1gNnSc2
na	na	k7c4
vývoz	vývoz	k1gInSc4
bombardérů	bombardér	k1gInPc2
tyto	tento	k3xDgInPc1
stroje	stroj	k1gInPc1
nikdy	nikdy	k6eAd1
nedostalo	dostat	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luftwaffe	Luftwaff	k1gInSc5
výrobu	výroba	k1gFnSc4
převzala	převzít	k5eAaPmAgFnS
pod	pod	k7c7
novým	nový	k2eAgNnSc7d1
označením	označení	k1gNnSc7
Dornier	Dornira	k1gFnPc2
Do	do	k7c2
215	#num#	k4
B-0	B-0	k1gFnPc2
a	a	k8xC
Do	do	k7c2
215	#num#	k4
B-	B-	k1gMnPc2
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohotovené	dohotovený	k2eAgInPc4d1
dálkové	dálkový	k2eAgInPc4d1
fotoprůzkumné	fotoprůzkumný	k2eAgInPc4d1
letouny	letoun	k1gInPc4
byly	být	k5eAaImAgFnP
zařazeny	zařadit	k5eAaPmNgFnP
k	k	k7c3
3	#num#	k4
<g/>
.	.	kIx.
průzkumné	průzkumný	k2eAgFnSc6d1
letce	letka	k1gFnSc6
vrchního	vrchní	k2eAgNnSc2d1
velení	velení	k1gNnSc2
vojenského	vojenský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Aufkl	Aufkl	k1gInSc1
<g/>
.	.	kIx.
<g/>
St.	st.	kA
<g/>
/	/	kIx~
<g/>
Ob.	Ob.	k1gMnSc1
<g/>
d.L.	d.L.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
obsazení	obsazení	k1gNnSc6
Norska	Norsko	k1gNnSc2
německou	německý	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
začala	začít	k5eAaPmAgFnS
jednotka	jednotka	k1gFnSc1
operovat	operovat	k5eAaImF
od	od	k7c2
léta	léto	k1gNnSc2
1940	#num#	k4
z	z	k7c2
letiště	letiště	k1gNnSc2
Stavanger	Stavangra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
výzbroj	výzbroj	k1gFnSc4
tvořilo	tvořit	k5eAaImAgNnS
13	#num#	k4
Do	do	k7c2
215	#num#	k4
B-0	B-0	k1gMnPc2
a	a	k8xC
B-1	B-1	k1gMnPc2
společně	společně	k6eAd1
s	s	k7c7
trojicí	trojice	k1gFnSc7
strojů	stroj	k1gInPc2
Heinkel	Heinkel	k1gInSc4
He	he	k0
111	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jediné	jediný	k2eAgInPc4d1
dva	dva	k4xCgInPc4
stroje	stroj	k1gInPc4
Do	do	k7c2
215	#num#	k4
B-3	B-3	k1gFnPc2
byly	být	k5eAaImAgFnP
společně	společně	k6eAd1
s	s	k7c7
jinými	jiný	k2eAgInPc7d1
typy	typ	k1gInPc7
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
dodány	dodat	k5eAaPmNgFnP
do	do	k7c2
SSSR	SSSR	kA
J.	J.	kA
V.	V.	kA
Stalinovi	Stalin	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
března	březen	k1gInSc2
roku	rok	k1gInSc2
1940	#num#	k4
začaly	začít	k5eAaPmAgInP
přicházet	přicházet	k5eAaImF
Do	do	k7c2
215	#num#	k4
B-4	B-4	k1gFnPc2
se	s	k7c7
zdokonalenou	zdokonalený	k2eAgFnSc7d1
fotografickou	fotografický	k2eAgFnSc7d1
výstrojí	výstroj	k1gFnSc7
přístroji	přístroj	k1gInSc6
Rb	Rb	k1gFnSc4
50	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
a	a	k8xC
Rb	Rb	k1gMnSc3
20	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrannou	obranný	k2eAgFnSc4d1
výzbroj	výzbroj	k1gFnSc4
tvořilo	tvořit	k5eAaImAgNnS
šest	šest	k4xCc1
pohyblivých	pohyblivý	k2eAgInPc2d1
kulometů	kulomet	k1gInPc2
MG	mg	kA
15	#num#	k4
ráže	ráže	k1gFnSc2
7,9	7,9	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
plnění	plnění	k1gNnSc6
kombinovaných	kombinovaný	k2eAgInPc2d1
bombardovacích	bombardovací	k2eAgInPc2d1
a	a	k8xC
průzkumných	průzkumný	k2eAgInPc2d1
úkolů	úkol	k1gInPc2
nesly	nést	k5eAaImAgInP
stroje	stroj	k1gInPc1
navíc	navíc	k6eAd1
pět	pět	k4xCc4
50	#num#	k4
kg	kg	kA
pum	puma	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
letech	let	k1gInPc6
na	na	k7c4
kratší	krátký	k2eAgFnSc4d2
vzdálenost	vzdálenost	k1gFnSc4
až	až	k9
10	#num#	k4
kusů	kus	k1gInPc2
těchto	tento	k3xDgFnPc2
pum	puma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
1940	#num#	k4
tvořily	tvořit	k5eAaImAgInP
tři	tři	k4xCgMnPc1
Do	do	k7c2
215	#num#	k4
B	B	kA
součást	součást	k1gFnSc1
výzbroje	výzbroj	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staffel	Staffel	k1gInSc1
der	drát	k5eAaImRp2nS
Aufkl	Aufkl	k1gInSc1
<g/>
.	.	kIx.
<g/>
Gr	Gr	k1gFnSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
Ob.	Ob.	k1gFnSc1
<g/>
d.L.	d.L.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staffel	Staffela	k1gFnPc2
měla	mít	k5eAaImAgFnS
10	#num#	k4
kusů	kus	k1gInPc2
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staffel	Staffel	k1gInSc1
11	#num#	k4
letounů	letoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zanedlouho	zanedlouho	k6eAd1
byly	být	k5eAaImAgInP
zařazeny	zařadit	k5eAaPmNgInP
také	také	k9
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
124	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
F	F	kA
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
100	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachtaufklärungsstaffel	Nachtaufklärungsstaffela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dornier	Dornier	k1gInSc1
Do-	Do-	k1gFnSc2
<g/>
215	#num#	k4
</s>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1
přestavba	přestavba	k1gFnSc1
letounu	letoun	k1gInSc2
Do	do	k7c2
17	#num#	k4
Z-3	Z-3	k1gFnPc2
na	na	k7c4
noční	noční	k2eAgInSc4d1
stíhací	stíhací	k2eAgInSc4d1
Do	do	k7c2
17	#num#	k4
Z-10	Z-10	k1gFnPc2
Kauz	kauza	k1gFnPc2
II	II	kA
s	s	k7c7
detektorem	detektor	k1gInSc7
infračerveného	infračervený	k2eAgNnSc2d1
záření	záření	k1gNnSc2
Spanner-Anlage	Spanner-Anlag	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
ukazatelem	ukazatel	k1gInSc7
Q-Rohr	Q-Rohra	k1gFnPc2
na	na	k7c6
palubní	palubní	k2eAgFnSc6d1
desce	deska	k1gFnSc6
<g/>
,	,	kIx,
uskutečněná	uskutečněný	k2eAgFnSc1d1
koncem	koncem	k7c2
podzimu	podzim	k1gInSc2
1940	#num#	k4
<g/>
,	,	kIx,
vedla	vést	k5eAaImAgFnS
velení	velení	k1gNnSc4
německého	německý	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
k	k	k7c3
podobné	podobný	k2eAgFnSc3d1
úpravě	úprava	k1gFnSc3
letounů	letoun	k1gInPc2
Do	do	k7c2
215	#num#	k4
B-	B-	k1gMnPc2
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
tak	tak	k6eAd1
k	k	k7c3
rychlé	rychlý	k2eAgFnSc3d1
přestavbě	přestavba	k1gFnSc3
několika	několik	k4yIc2
kusů	kus	k1gInPc2
na	na	k7c6
Do	do	k7c2
215	#num#	k4
B-5	B-5	k1gFnPc2
se	s	k7c7
zakrytou	zakrytý	k2eAgFnSc7d1
zahrocenou	zahrocený	k2eAgFnSc7d1
přídí	příď	k1gFnSc7
před	před	k7c7
kabinou	kabina	k1gFnSc7
s	s	k7c7
čtyřmi	čtyři	k4xCgInPc7
kulomety	kulomet	k1gInPc7
MG	mg	kA
17	#num#	k4
ráže	ráže	k1gFnSc2
7,9	7,9	k4
mm	mm	kA
a	a	k8xC
dvojicí	dvojice	k1gFnSc7
kanónů	kanón	k1gInPc2
MG	mg	kA
FF	ff	kA
ráže	ráže	k1gFnSc1
20	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
1940	#num#	k4
byly	být	k5eAaImAgFnP
zahájeny	zahájit	k5eAaPmNgFnP
jejich	jejich	k3xOp3gFnPc1
dodávky	dodávka	k1gFnPc1
ke	k	k7c3
4	#num#	k4
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
NJG	NJG	kA
1	#num#	k4
se	s	k7c7
základnou	základna	k1gFnSc7
v	v	k7c6
holandském	holandský	k2eAgInSc6d1
Leeuwardenu	Leeuwarden	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účinnost	účinnost	k1gFnSc1
nočních	noční	k2eAgInPc2d1
útoků	útok	k1gInPc2
na	na	k7c4
nepřátelské	přátelský	k2eNgInPc4d1
letouny	letoun	k1gInPc4
byla	být	k5eAaImAgFnS
vylepšena	vylepšit	k5eAaPmNgFnS
instalací	instalace	k1gFnSc7
prvního	první	k4xOgInSc2
předsériového	předsériový	k2eAgInSc2d1
radaru	radar	k1gInSc2
Telefunken	Telefunkna	k1gFnPc2
FuG	fuga	k1gFnPc2
202	#num#	k4
Lichtenstein-Gerät	Lichtenstein-Geräta	k1gFnPc2
do	do	k7c2
jednoho	jeden	k4xCgInSc2
Do	do	k7c2
215	#num#	k4
B-5	B-5	k1gMnPc2
ze	z	k7c2
stavu	stav	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
NJG	NJG	kA
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radar	radar	k1gInSc1
měl	mít	k5eAaImAgInS
dosah	dosah	k1gInSc4
od	od	k7c2
183	#num#	k4
do	do	k7c2
4000	#num#	k4
m	m	kA
a	a	k8xC
pracoval	pracovat	k5eAaImAgInS
na	na	k7c4
frekvenci	frekvence	k1gFnSc4
490	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyznačoval	vyznačovat	k5eAaImAgMnS
se	se	k3xPyFc4
velkým	velký	k2eAgInSc7d1
počtem	počet	k1gInSc7
malých	malý	k2eAgFnPc2d1
dipólových	dipólův	k2eAgFnPc2d1
antének	anténka	k1gFnPc2
nesených	nesený	k2eAgFnPc2d1
na	na	k7c6
ramenech	rameno	k1gNnPc6
před	před	k7c7
přídí	příď	k1gFnSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
tato	tento	k3xDgFnSc1
instalace	instalace	k1gFnSc1
snižovala	snižovat	k5eAaImAgFnS
rychlost	rychlost	k1gFnSc4
stroje	stroj	k1gInSc2
až	až	k9
o	o	k7c4
25	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	kA
První	první	k4xOgInSc4
sestřel	sestřel	k1gInSc4
bombardéru	bombardér	k1gInSc2
Royal	Royal	k2eAgNnSc1d1
Air	Air	k2eAgNnSc1d1
Force	force	k1gNnSc1
zachyceného	zachycený	k2eAgInSc2d1
tímto	tento	k3xDgNnSc7
zařízením	zařízení	k1gNnSc7
dosáhl	dosáhnout	k5eAaPmAgMnS
Oberleutnant	Oberleutnant	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Becker	Becker	k1gMnSc1
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalších	další	k2eAgMnPc2d1
sestřelů	sestřel	k1gInPc2
na	na	k7c6
radarovém	radarový	k2eAgNnSc6d1
Do	do	k7c2
215	#num#	k4
B-5	B-5	k1gFnSc2
dosáhl	dosáhnout	k5eAaPmAgMnS
Becker	Becker	k1gMnSc1
15	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
a	a	k8xC
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
poslední	poslední	k2eAgNnPc1d1
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
2790	#num#	k4
m	m	kA
znamenal	znamenat	k5eAaImAgInS
konečné	konečný	k2eAgNnSc4d1
uznání	uznání	k1gNnSc4
FuG	fuga	k1gFnPc2
202	#num#	k4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc1
masové	masový	k2eAgNnSc1d1
zavedení	zavedení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Posledních	poslední	k2eAgNnPc2d1
20	#num#	k4
letounů	letoun	k1gInPc2
varianty	varianta	k1gFnSc2
B-4	B-4	k1gFnPc2
bylo	být	k5eAaImAgNnS
dokončeno	dokončit	k5eAaPmNgNnS
jako	jako	k9
B-	B-	k1gFnSc1
<g/>
5	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
pak	pak	k6eAd1
sloužily	sloužit	k5eAaImAgInP
u	u	k7c2
NJG	NJG	kA
1	#num#	k4
a	a	k8xC
2	#num#	k4
až	až	k9
do	do	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
1944	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
1942	#num#	k4
předala	předat	k5eAaPmAgFnS
Luftwaffe	Luftwaff	k1gInSc5
10	#num#	k4
Do	do	k7c2
215	#num#	k4
B-4	B-4	k1gFnSc1
maďarskému	maďarský	k2eAgNnSc3d1
letectvu	letectvo	k1gNnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
doplnily	doplnit	k5eAaPmAgInP
šest	šest	k4xCc4
He	he	k0
111	#num#	k4
P-2	P-2	k1gMnPc2
a	a	k8xC
P-4	P-4	k1gMnPc2
u	u	k7c2
1	#num#	k4
<g/>
.	.	kIx.
dálkové	dálkový	k2eAgFnSc2d1
průzkumné	průzkumný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
na	na	k7c6
východní	východní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
stíhacích	stíhací	k2eAgInPc2d1
Do	do	k7c2
215	#num#	k4
B-5	B-5	k1gFnPc2
byly	být	k5eAaImAgInP
zbývající	zbývající	k2eAgInPc1d1
„	„	k?
<g/>
dvěstěpatnáctky	dvěstěpatnáctek	k1gInPc1
<g/>
“	“	k?
staženy	stáhnout	k5eAaPmNgInP
z	z	k7c2
bojových	bojový	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
Do	do	k7c2
215	#num#	k4
B	B	kA
byla	být	k5eAaImAgFnS
ukončena	ukončit	k5eAaPmNgFnS
po	po	k7c6
dohotovení	dohotovení	k1gNnSc6
101	#num#	k4
exempláře	exemplář	k1gInSc2
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
1941	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1
</s>
<s>
Dornier	Dornier	k1gInSc1
Do	do	k7c2
215	#num#	k4
</s>
<s>
Německo	Německo	k1gNnSc1
</s>
<s>
Luftwaffe	Luftwaff	k1gMnSc5
</s>
<s>
Maďarsko	Maďarsko	k1gNnSc1
</s>
<s>
Maďarské	maďarský	k2eAgNnSc1d1
královské	královský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
používalo	používat	k5eAaImAgNnS
nejméně	málo	k6eAd3
11	#num#	k4
letounů	letoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Sovětské	sovětský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
od	od	k7c2
Německa	Německo	k1gNnSc2
zakoupilo	zakoupit	k5eAaPmAgNnS
2	#num#	k4
letouny	letoun	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Specifikace	specifikace	k1gFnSc1
(	(	kIx(
<g/>
Do	do	k7c2
215	#num#	k4
B-	B-	k1gFnPc2
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
4	#num#	k4
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1
<g/>
:	:	kIx,
18	#num#	k4
m	m	kA
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
15,79	15,79	k4
m	m	kA
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
4,56	4,56	k4
m	m	kA
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
:	:	kIx,
55	#num#	k4
m²	m²	k?
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
prázdného	prázdný	k2eAgInSc2d1
stroje	stroj	k1gInSc2
<g/>
:	:	kIx,
5780	#num#	k4
kg	kg	kA
</s>
<s>
Vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
8810	#num#	k4
kg	kg	kA
</s>
<s>
Stoupavost	stoupavost	k1gFnSc1
u	u	k7c2
země	zem	k1gFnSc2
<g/>
:	:	kIx,
6,1	6,1	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
</s>
<s>
Pohonné	pohonný	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
×	×	k?
řadové	řadový	k2eAgNnSc1d1
dvanáctiválcové	dvanáctiválcový	k2eAgNnSc1d1
kapalinou	kapalina	k1gFnSc7
chlazené	chlazený	k2eAgInPc1d1
motory	motor	k1gInPc1
Daimler-Benz	Daimler-Benza	k1gFnPc2
DB	db	kA
601	#num#	k4
Aa	Aa	k1gFnSc2
</s>
<s>
Vzletový	vzletový	k2eAgInSc1d1
výkon	výkon	k1gInSc1
<g/>
:	:	kIx,
809	#num#	k4
kW	kW	kA
</s>
<s>
Výkony	výkon	k1gInPc1
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
u	u	k7c2
hladiny	hladina	k1gFnSc2
moře	moře	k1gNnSc2
<g/>
:	:	kIx,
384	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
ve	v	k7c6
4000	#num#	k4
m	m	kA
<g/>
:	:	kIx,
465	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
v	v	k7c6
5000	#num#	k4
m	m	kA
:	:	kIx,
470	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
Dostup	dostup	k1gInSc1
<g/>
:	:	kIx,
9000	#num#	k4
m	m	kA
</s>
<s>
Dolet	dolet	k1gInSc1
s	s	k7c7
přídavnou	přídavný	k2eAgFnSc7d1
nádrží	nádrž	k1gFnSc7
<g/>
:	:	kIx,
2445	#num#	k4
km	km	kA
</s>
<s>
Operační	operační	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
s	s	k7c7
pumovým	pumový	k2eAgInSc7d1
nákladem	náklad	k1gInSc7
do	do	k7c2
1000	#num#	k4
kg	kg	kA
<g/>
:	:	kIx,
380	#num#	k4
km	km	kA
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
šest	šest	k4xCc1
kulometů	kulomet	k1gInPc2
MG	mg	kA
15	#num#	k4
ráže	ráže	k1gFnSc2
7,92	7,92	k4
mm	mm	kA
</s>
<s>
do	do	k7c2
1000	#num#	k4
kg	kg	kA
pum	puma	k1gFnPc2
SD	SD	kA
50	#num#	k4
nebo	nebo	k8xC
SD	SD	kA
250	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Griehl	Griehl	k1gFnSc1
2005	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
10	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Green	Green	k1gInSc1
1967	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
<g/>
10	#num#	k4
<g/>
↑	↑	k?
Nowarra	Nowarr	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MURAWSKI	MURAWSKI	kA
<g/>
,	,	kIx,
Marek	Marek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letadla	letadlo	k1gNnPc1
Luftwaffe	Luftwaff	k1gInSc5
Část	část	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hostomice	Hostomika	k1gFnSc6
<g/>
:	:	kIx,
Intermodel	Intermodlo	k1gNnPc2
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
240	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901976	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
NĚMEČEK	Němeček	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dornier	Dornier	k1gInSc1
Do	do	k7c2
215	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
LXXIV	LXXIV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
20	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Dornier	Dornira	k1gFnPc2
Do	do	k7c2
215	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dornier	Dornier	k1gInSc1
215	#num#	k4
a	a	k8xC
217	#num#	k4
</s>
<s>
Maďarské	maďarský	k2eAgInPc1d1
stroje	stroj	k1gInPc1
Dornier	Dorniero	k1gNnPc2
Do	do	k7c2
215	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Německé	německý	k2eAgInPc4d1
vojenské	vojenský	k2eAgInPc4d1
letouny	letoun	k1gInPc4
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
Stíhací	stíhací	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
Ar	ar	k1gInSc1
65	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
68	#num#	k4
•	•	k?
Do	do	k7c2
335	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
190	#num#	k4
•	•	k?
Ta	ten	k3xDgFnSc1
152	#num#	k4
•	•	k?
Ta	ten	k3xDgFnSc1
154	#num#	k4
•	•	k?
He	he	k0
51	#num#	k4
•	•	k?
He	he	k0
112	#num#	k4
•	•	k?
He	he	k0
162	#num#	k4
•	•	k?
He	he	k0
219	#num#	k4
•	•	k?
Bf	Bf	k1gFnPc2
109	#num#	k4
•	•	k?
Bf	Bf	k1gFnPc2
110	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
163	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
210	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
262	#num#	k4
•	•	k?
Me	Me	k1gFnSc1
410	#num#	k4
Bombardovací	bombardovací	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
Ar	ar	k1gInSc1
234	#num#	k4
•	•	k?
Do	do	k7c2
17	#num#	k4
•	•	k?
Do	do	k7c2
23	#num#	k4
•	•	k?
Do	do	k7c2
217	#num#	k4
•	•	k?
Do	do	k7c2
335	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
200	#num#	k4
•	•	k?
He	he	k0
111	#num#	k4
•	•	k?
He	he	k0
115	#num#	k4
•	•	k?
He	he	k0
177	#num#	k4
•	•	k?
Ju	ju	k0
86	#num#	k4
•	•	k?
Ju	ju	k0
88	#num#	k4
•	•	k?
Ju	ju	k0
188	#num#	k4
•	•	k?
Ju	ju	k0
388	#num#	k4
Plovákové	plovákový	k2eAgInPc1d1
letouny	letoun	k1gInPc1
a	a	k8xC
létající	létající	k2eAgInPc1d1
čluny	člun	k1gInPc1
</s>
<s>
Ar	ar	k1gInSc1
95	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
196	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
231	#num#	k4
•	•	k?
BV	BV	kA
138	#num#	k4
•	•	k?
Ha	ha	kA
139	#num#	k4
•	•	k?
BV	BV	kA
222	#num#	k4
•	•	k?
Do	do	k7c2
16	#num#	k4
•	•	k?
Do	do	k7c2
18	#num#	k4
•	•	k?
Do	do	k7c2
24	#num#	k4
•	•	k?
Do	do	k7c2
26	#num#	k4
•	•	k?
He	he	k0
59	#num#	k4
•	•	k?
He	he	k0
60	#num#	k4
•	•	k?
He	he	k0
114	#num#	k4
•	•	k?
He	he	k0
115	#num#	k4
•	•	k?
We	We	k1gFnSc1
271	#num#	k4
Průzkumné	průzkumný	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
Ar	ar	k1gInSc1
95	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
196	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
234	#num#	k4
•	•	k?
Do	do	k7c2
17	#num#	k4
•	•	k?
Do	do	k7c2
215	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
189	#num#	k4
•	•	k?
He	he	k0
45	#num#	k4
•	•	k?
He	he	k0
46	#num#	k4
•	•	k?
He	he	k0
70	#num#	k4
•	•	k?
He	he	k0
170	#num#	k4
•	•	k?
Hs	Hs	k1gFnSc1
126	#num#	k4
Transportní	transportní	k2eAgInPc1d1
letouny	letoun	k1gInPc1
a	a	k8xC
kluzáky	kluzák	k1gInPc1
</s>
<s>
Ar	ar	k1gInSc1
232	#num#	k4
•	•	k?
DFS	DFS	kA
230	#num#	k4
•	•	k?
Go	Go	k1gFnPc2
242	#num#	k4
•	•	k?
Go	Go	k1gFnPc2
244	#num#	k4
•	•	k?
Ju	ju	k0
52	#num#	k4
•	•	k?
Ju	ju	k0
90	#num#	k4
•	•	k?
Ju	ju	k0
252	#num#	k4
•	•	k?
Ju	ju	k0
352	#num#	k4
•	•	k?
Ju	ju	k0
290	#num#	k4
•	•	k?
Ju	ju	k0
390	#num#	k4
•	•	k?
Ka	Ka	k1gFnPc2
430	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
321	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
323	#num#	k4
•	•	k?
Si	se	k3xPyFc3
204	#num#	k4
Cvičné	cvičný	k2eAgInPc1d1
a	a	k8xC
spojovací	spojovací	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
Ar	ar	k1gInSc1
66	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
69	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
76	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
79	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
96	#num#	k4
•	•	k?
Bü	Bü	k1gFnPc2
131	#num#	k4
•	•	k?
Bü	Bü	k1gFnPc2
133	#num#	k4
•	•	k?
Bü	Bü	k1gFnPc2
180	#num#	k4
•	•	k?
Bü	Bü	k1gFnPc2
181	#num#	k4
•	•	k?
Bü	Bü	k1gFnPc2
182	#num#	k4
•	•	k?
Fi	fi	k0
156	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
44	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
56	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
58	#num#	k4
•	•	k?
F	F	kA
13	#num#	k4
•	•	k?
W	W	kA
34	#num#	k4
•	•	k?
He	he	k0
72	#num#	k4
•	•	k?
Kl	kl	kA
31	#num#	k4
•	•	k?
Kl	kl	kA
32	#num#	k4
•	•	k?
Kl	kl	kA
35	#num#	k4
•	•	k?
Bf	Bf	k1gFnPc2
108	#num#	k4
•	•	k?
Fh	Fh	k1gFnPc2
104	#num#	k4
•	•	k?
Si	se	k3xPyFc3
202	#num#	k4
•	•	k?
Sk	Sk	kA
257	#num#	k4
Bitevní	bitevní	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
Ar	ar	k1gInSc1
66	#num#	k4
•	•	k?
Go	Go	k1gFnPc2
145	#num#	k4
•	•	k?
He	he	k0
45	#num#	k4
•	•	k?
He	he	k0
50	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
123	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
129	#num#	k4
•	•	k?
Ju	ju	k0
87	#num#	k4
Vrtulníky	vrtulník	k1gInPc1
a	a	k8xC
vírníky	vírník	k1gInPc1
</s>
<s>
Fa	fa	k1gNnSc1
223	#num#	k4
•	•	k?
Fa	fa	kA
330	#num#	k4
•	•	k?
Fl	Fl	k1gFnPc2
265	#num#	k4
•	•	k?
Fl	Fl	k1gFnSc1
282	#num#	k4
Experimentální	experimentální	k2eAgInPc1d1
a	a	k8xC
specializované	specializovaný	k2eAgInPc1d1
letouny	letoun	k1gInPc1
</s>
<s>
DFS	DFS	kA
39	#num#	k4
•	•	k?
DFS	DFS	kA
193	#num#	k4
•	•	k?
DFS	DFS	kA
194	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
47	#num#	k4
•	•	k?
Fi	fi	k0
158	#num#	k4
•	•	k?
Ju	ju	k0
49	#num#	k4
•	•	k?
He	he	k0
116	#num#	k4
•	•	k?
He	he	k0
119	#num#	k4
•	•	k?
He	he	k0
176	#num#	k4
•	•	k?
He	he	k0
178	#num#	k4
•	•	k?
LF	LF	kA
1	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
261	#num#	k4
•	•	k?
Mistel	Mistel	k1gMnSc1
Malosériové	malosériový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
Ar	ar	k1gInSc1
240	#num#	k4
•	•	k?
BV	BV	kA
141	#num#	k4
•	•	k?
Fi	fi	k0
167	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
187	#num#	k4
•	•	k?
He	he	k0
100	#num#	k4
(	(	kIx(
<g/>
He	he	k0
113	#num#	k4
<g/>
)	)	kIx)
Prototypy	prototyp	k1gInPc1
a	a	k8xC
projekty	projekt	k1gInPc1
</s>
<s>
Ao	Ao	k?
225	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
77	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
80	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
81	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
195	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
197	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
198	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
199	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
340	#num#	k4
•	•	k?
Ar	ar	k1gInSc1
396	#num#	k4
•	•	k?
E	E	kA
<g/>
.555	.555	k4
•	•	k?
AS	as	k9
6	#num#	k4
•	•	k?
Ba	ba	k9
349	#num#	k4
•	•	k?
BV	BV	kA
40	#num#	k4
•	•	k?
Ha	ha	kA
137	#num#	k4
•	•	k?
Ha	ha	kA
140	#num#	k4
•	•	k?
BV	BV	kA
142	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
BV	BV	kA
144	#num#	k4
•	•	k?
BV	BV	kA
155	#num#	k4
•	•	k?
BV	BV	kA
238	#num#	k4
•	•	k?
P.	P.	kA
<g/>
175	#num#	k4
•	•	k?
Do	do	k7c2
19	#num#	k4
•	•	k?
Do	do	k7c2
317	#num#	k4
•	•	k?
DFS	DFS	kA
228	#num#	k4
•	•	k?
DFS	DFS	kA
346	#num#	k4
•	•	k?
Fi	fi	k0
98	#num#	k4
•	•	k?
Fi	fi	k0
99	#num#	k4
•	•	k?
Fi	fi	k0
256	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
57	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
62	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
159	#num#	k4
•	•	k?
Ta	ten	k3xDgFnSc1
183	#num#	k4
•	•	k?
Fw	Fw	k1gFnPc2
191	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
He	he	k0
118	#num#	k4
•	•	k?
He	he	k0
274	#num#	k4
•	•	k?
He	he	k0
280	#num#	k4
•	•	k?
He	he	k0
343	#num#	k4
•	•	k?
He	he	k0
519	#num#	k4
•	•	k?
Ho	on	k3xPp3gInSc4
229	#num#	k4
•	•	k?
Ju	ju	k0
85	#num#	k4
•	•	k?
Ju	ju	k0
89	#num#	k4
•	•	k?
Ju	ju	k0
187	#num#	k4
•	•	k?
Ju	ju	k0
288	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
122	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
124	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
125	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
127	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
128	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
130	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
132	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Ju	ju	k0
322	#num#	k4
•	•	k?
Bf	Bf	k1gFnPc2
161	#num#	k4
•	•	k?
Bf	Bf	k1gFnPc2
162	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
209-II	209-II	k4
•	•	k?
Me	Me	k1gFnPc2
263	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
264	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
309	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
328	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
509	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
609	#num#	k4
•	•	k?
Me	Me	k1gFnPc2
P.	P.	kA
1101	#num#	k4
•	•	k?
Li	li	k8xS
P.	P.	kA
<g/>
01	#num#	k4
•	•	k?
Li	li	k8xS
P.	P.	kA
<g/>
04	#num#	k4
•	•	k?
Li	li	k8xS
P.	P.	kA
<g/>
10	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Li	li	k8xS
P.	P.	kA
<g/>
11	#num#	k4
•	•	k?
Li	li	k8xS
DM-1	DM-1	k1gFnSc1
•	•	k?
Li	li	k8xS
P.	P.	kA
<g/>
13	#num#	k4
•	•	k?
Li	li	k8xS
P.	P.	kA
<g/>
13	#num#	k4
<g/>
a	a	k8xC
•	•	k?
Li	li	k8xS
P.	P.	kA
<g/>
13	#num#	k4
<g/>
b	b	k?
•	•	k?
Li	li	k8xS
P.	P.	kA
<g/>
15	#num#	k4
•	•	k?
Li	li	k8xS
P.	P.	kA
<g/>
20	#num#	k4
•	•	k?
Sk	Sk	kA
P.	P.	kA
<g/>
14	#num#	k4
•	•	k?
Sk	Sk	kA
SL-6	SL-6	k1gMnSc3
•	•	k?
So	So	kA
334	#num#	k4
Bezpilotní	bezpilotní	k2eAgInPc1d1
letouny	letoun	k1gInPc1
a	a	k8xC
řízené	řízený	k2eAgInPc1d1
střely	střel	k1gInPc1
</s>
<s>
As	as	k9
292	#num#	k4
•	•	k?
BV	BV	kA
143	#num#	k4
•	•	k?
BV	BV	kA
246	#num#	k4
•	•	k?
Fi	fi	k0
103	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
117	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
293	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
295	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
296	#num#	k4
•	•	k?
Hs	Hs	k1gFnPc2
297	#num#	k4
•	•	k?
Hs	Hs	k1gMnSc1
298	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7531651-1	7531651-1	k4
</s>
