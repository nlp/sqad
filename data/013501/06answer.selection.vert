<s desamb="1">
Vyznačoval	vyznačovat	k5eAaImAgMnS
se	se	k3xPyFc4
velkým	velký	k2eAgInSc7d1
počtem	počet	k1gInSc7
malých	malý	k2eAgFnPc2d1
dipólových	dipólův	k2eAgFnPc2d1
antének	anténka	k1gFnPc2
nesených	nesený	k2eAgFnPc2d1
na	na	k7c6
ramenech	rameno	k1gNnPc6
před	před	k7c7
přídí	příď	k1gFnSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
tato	tento	k3xDgFnSc1
instalace	instalace	k1gFnSc1
snižovala	snižovat	k5eAaImAgFnS
rychlost	rychlost	k1gFnSc4
stroje	stroj	k1gInSc2
až	až	k9
o	o	k7c4
25	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	kA
První	první	k4xOgInSc4
sestřel	sestřel	k1gInSc4
bombardéru	bombardér	k1gInSc2
Royal	Royal	k2eAgNnSc1d1
Air	Air	k2eAgNnSc1d1
Force	force	k1gNnSc1
zachyceného	zachycený	k2eAgInSc2d1
tímto	tento	k3xDgNnSc7
zařízením	zařízení	k1gNnSc7
dosáhl	dosáhnout	k5eAaPmAgMnS
Oberleutnant	Oberleutnant	k1gMnSc1
Ludwig	Ludwig	k1gMnSc1
Becker	Becker	k1gMnSc1
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1941	#num#	k4
<g/>
.	.	kIx.
</s>