<p>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
Pavarotti	Pavarotť	k1gFnSc5	Pavarotť
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
Modena	Modena	k1gFnSc1	Modena
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2007	[number]	k4	2007
Modena	Modena	k1gFnSc1	Modena
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
světoznámý	světoznámý	k2eAgMnSc1d1	světoznámý
operní	operní	k2eAgMnSc1d1	operní
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
"	"	kIx"	"
<g/>
král	král	k1gMnSc1	král
vysokého	vysoký	k2eAgInSc2d1	vysoký
C	C	kA	C
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
tenorů	tenor	k1gMnPc2	tenor
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
italské	italský	k2eAgFnSc6d1	italská
Modeně	Modena	k1gFnSc6	Modena
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
amatérského	amatérský	k2eAgMnSc2d1	amatérský
zpěváka	zpěvák	k1gMnSc2	zpěvák
a	a	k8xC	a
pekaře	pekař	k1gMnSc2	pekař
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
tenorů	tenor	k1gMnPc2	tenor
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
koncerty	koncert	k1gInPc1	koncert
vysílaly	vysílat	k5eAaImAgInP	vysílat
televize	televize	k1gFnSc2	televize
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
charitě	charita	k1gFnSc3	charita
<g/>
,	,	kIx,	,
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
uprchlíkům	uprchlík	k1gMnPc3	uprchlík
<g/>
,	,	kIx,	,
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
Červený	červený	k2eAgInSc4d1	červený
kříž	kříž	k1gInSc4	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Talent	talent	k1gInSc1	talent
Luciano	Luciana	k1gFnSc5	Luciana
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ač	ač	k8xS	ač
hlasově	hlasově	k6eAd1	hlasově
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
,	,	kIx,	,
nedal	dát	k5eNaPmAgMnS	dát
se	se	k3xPyFc4	se
na	na	k7c4	na
profesionální	profesionální	k2eAgFnSc4d1	profesionální
kariéru	kariéra	k1gFnSc4	kariéra
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
přílišné	přílišný	k2eAgFnSc2d1	přílišná
nervozity	nervozita	k1gFnSc2	nervozita
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
ho	on	k3xPp3gMnSc4	on
ke	k	k7c3	k
zpěvu	zpěv	k1gInSc3	zpěv
vedl	vést	k5eAaImAgMnS	vést
od	od	k7c2	od
malička	maličko	k1gNnSc2	maličko
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
ho	on	k3xPp3gMnSc2	on
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
do	do	k7c2	do
místního	místní	k2eAgInSc2d1	místní
farního	farní	k2eAgInSc2d1	farní
sboru	sbor	k1gInSc2	sbor
"	"	kIx"	"
<g/>
Gioacchino	Gioacchino	k1gNnSc1	Gioacchino
Rossini	Rossin	k2eAgMnPc1d1	Rossin
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgNnSc7	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
zpěv	zpěv	k1gInSc1	zpěv
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
zajímal	zajímat	k5eAaImAgInS	zajímat
sport	sport	k1gInSc1	sport
-	-	kIx~	-
především	především	k9	především
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
Pavarotti	Pavarotť	k1gFnPc1	Pavarotť
po	po	k7c6	po
škole	škola	k1gFnSc6	škola
řešil	řešit	k5eAaImAgInS	řešit
dilema	dilema	k1gNnSc4	dilema
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
stát	stát	k1gInSc1	stát
profesionálním	profesionální	k2eAgMnSc7d1	profesionální
fotbalistou	fotbalista	k1gMnSc7	fotbalista
nebo	nebo	k8xC	nebo
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
navštívil	navštívit	k5eAaPmAgMnS	navštívit
koncert	koncert	k1gInSc4	koncert
Benjamina	Benjamin	k1gMnSc2	Benjamin
Gigliho	Gigli	k1gMnSc2	Gigli
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
nakonec	nakonec	k6eAd1	nakonec
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
a	a	k8xC	a
Lucianno	Lucianno	k6eAd1	Lucianno
v	v	k7c6	v
19	[number]	k4	19
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
zpěv	zpěv	k1gInSc1	zpěv
u	u	k7c2	u
respektovaného	respektovaný	k2eAgMnSc2d1	respektovaný
profesora	profesor	k1gMnSc2	profesor
Arrigo	Arrigo	k1gMnSc1	Arrigo
Pola	pola	k1gFnSc1	pola
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studií	studio	k1gNnPc2	studio
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
ve	v	k7c6	v
zpěvu	zpěv	k1gInSc6	zpěv
pokračovat	pokračovat	k5eAaImF	pokračovat
a	a	k8xC	a
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
si	se	k3xPyFc3	se
jako	jako	k8xS	jako
učitel	učitel	k1gMnSc1	učitel
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
pojišťovák	pojišťovák	k1gInSc1	pojišťovák
<g/>
.	.	kIx.	.
<g/>
Zlom	zlom	k1gInSc1	zlom
nastal	nastat	k5eAaPmAgInS	nastat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
roli	role	k1gFnSc4	role
Rodolfa	Rodolf	k1gMnSc2	Rodolf
v	v	k7c6	v
Pucciniho	Puccini	k1gMnSc2	Puccini
opeře	opera	k1gFnSc6	opera
La	la	k1gNnPc2	la
bohè	bohè	k?	bohè
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
následovala	následovat	k5eAaImAgFnS	následovat
angažmá	angažmá	k1gNnSc4	angažmá
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
<g/>
,	,	kIx,	,
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
Curychu	Curych	k1gInSc6	Curych
<g/>
,	,	kIx,	,
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
zažil	zažít	k5eAaPmAgInS	zažít
fenomenální	fenomenální	k2eAgInSc1d1	fenomenální
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
opeře	opera	k1gFnSc6	opera
v	v	k7c6	v
New	New	k1gMnSc6	New
Yorku	York	k1gInSc2	York
zazpíval	zazpívat	k5eAaPmAgInS	zazpívat
v	v	k7c6	v
představení	představení	k1gNnSc6	představení
La	la	k1gNnSc2	la
Fille	Fill	k1gMnSc5	Fill
du	du	k?	du
Regiment	regiment	k1gInSc1	regiment
bez	bez	k7c2	bez
námahy	námaha	k1gFnSc2	námaha
devětkrát	devětkrát	k6eAd1	devětkrát
tříčárkované	tříčárkovaný	k2eAgInPc1d1	tříčárkovaný
C	C	kA	C
-	-	kIx~	-
publikum	publikum	k1gNnSc4	publikum
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
17	[number]	k4	17
<g/>
x	x	k?	x
vytleskalo	vytleskat	k5eAaPmAgNnS	vytleskat
zpět	zpět	k6eAd1	zpět
před	před	k7c4	před
oponu	opona	k1gFnSc4	opona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gFnSc1	jeho
sláva	sláva	k1gFnSc1	sláva
a	a	k8xC	a
popularita	popularita	k1gFnSc1	popularita
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
vešel	vejít	k5eAaPmAgInS	vejít
svými	svůj	k3xOyFgInPc7	svůj
televizními	televizní	k2eAgInPc7d1	televizní
operními	operní	k2eAgInPc7d1	operní
přenosy	přenos	k1gInPc7	přenos
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
popularitu	popularita	k1gFnSc4	popularita
mu	on	k3xPp3gMnSc3	on
přineslo	přinést	k5eAaPmAgNnS	přinést
jeho	jeho	k3xOp3gFnSc4	jeho
"	"	kIx"	"
<g/>
členství	členství	k1gNnSc2	členství
<g/>
"	"	kIx"	"
ve	v	k7c6	v
hvězdném	hvězdný	k2eAgInSc6d1	hvězdný
týmu	tým	k1gInSc6	tým
Tří	tři	k4xCgInPc2	tři
tenorů	tenor	k1gInPc2	tenor
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
José	José	k1gNnSc7	José
Carrerasem	Carrerasma	k1gFnPc2	Carrerasma
a	a	k8xC	a
Plácidem	Plácid	k1gInSc7	Plácid
Domingem	Doming	k1gInSc7	Doming
zazpíval	zazpívat	k5eAaPmAgInS	zazpívat
na	na	k7c6	na
koncertu	koncert	k1gInSc6	koncert
při	při	k7c6	při
fotbalovém	fotbalový	k2eAgNnSc6d1	fotbalové
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
přenášel	přenášet	k5eAaImAgInS	přenášet
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pavarotti	Pavarotti	k1gNnSc1	Pavarotti
ale	ale	k8xC	ale
miloval	milovat	k5eAaImAgMnS	milovat
i	i	k9	i
moderní	moderní	k2eAgFnSc4d1	moderní
rockovou	rockový	k2eAgFnSc4d1	rocková
a	a	k8xC	a
popovou	popový	k2eAgFnSc4d1	popová
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokládá	dokládat	k5eAaImIp3nS	dokládat
několik	několik	k4yIc4	několik
jeho	jeho	k3xOp3gInPc2	jeho
koncertů	koncert	k1gInPc2	koncert
se	s	k7c7	s
Stingem	Sting	k1gInSc7	Sting
<g/>
,	,	kIx,	,
irskými	irský	k2eAgFnPc7d1	irská
U2	U2	k1gFnPc7	U2
nebo	nebo	k8xC	nebo
Bryanem	Bryan	k1gInSc7	Bryan
Adamsem	Adams	k1gInSc7	Adams
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
popularizaci	popularizace	k1gFnSc4	popularizace
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
Pavarotti	Pavarotť	k1gFnSc5	Pavarotť
se	se	k3xPyFc4	se
intenzivně	intenzivně	k6eAd1	intenzivně
věnoval	věnovat	k5eAaPmAgMnS	věnovat
také	také	k9	také
charitativním	charitativní	k2eAgInPc3d1	charitativní
projektům	projekt	k1gInPc3	projekt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pomáhaly	pomáhat	k5eAaImAgInP	pomáhat
získat	získat	k5eAaPmF	získat
peníze	peníz	k1gInPc1	peníz
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
a	a	k8xC	a
Červený	červený	k2eAgInSc1d1	červený
kříž	kříž	k1gInSc1	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
zdarma	zdarma	k6eAd1	zdarma
věnoval	věnovat	k5eAaPmAgMnS	věnovat
i	i	k8xC	i
výuce	výuka	k1gFnSc3	výuka
zpěvu	zpěv	k1gInSc2	zpěv
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
rodném	rodný	k2eAgInSc6d1	rodný
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2006	[number]	k4	2006
u	u	k7c2	u
něj	on	k3xPp3gMnSc4	on
lékaři	lékař	k1gMnPc1	lékař
objevili	objevit	k5eAaPmAgMnP	objevit
rakovinový	rakovinový	k2eAgInSc4d1	rakovinový
nádor	nádor	k1gInSc4	nádor
slinivky	slinivka	k1gFnSc2	slinivka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyžadoval	vyžadovat	k5eAaImAgInS	vyžadovat
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
operaci	operace	k1gFnSc4	operace
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
lékaři	lékař	k1gMnPc1	lékař
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
uzdravuje	uzdravovat	k5eAaImIp3nS	uzdravovat
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
stav	stav	k1gInSc1	stav
nebyl	být	k5eNaImAgInS	být
uspokojivý	uspokojivý	k2eAgMnSc1d1	uspokojivý
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
opět	opět	k6eAd1	opět
hospitalizován	hospitalizován	k2eAgMnSc1d1	hospitalizován
kvůli	kvůli	k7c3	kvůli
komplikacím	komplikace	k1gFnPc3	komplikace
a	a	k8xC	a
ráno	ráno	k6eAd1	ráno
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
–	–	k?	–
Yes	Yes	k1gMnSc1	Yes
<g/>
,	,	kIx,	,
Giorgio	Giorgio	k1gMnSc1	Giorgio
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
–	–	k?	–
3	[number]	k4	3
Tenors	Tenors	k1gInSc1	Tenors
in	in	k?	in
Concert	Concert	k1gInSc1	Concert
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
–	–	k?	–
Pavarotti	Pavarotť	k1gFnSc2	Pavarotť
&	&	k?	&
Friends	Friends	k1gInSc1	Friends
Together	Togethra	k1gFnPc2	Togethra
for	forum	k1gNnPc2	forum
the	the	k?	the
Children	Childrno	k1gNnPc2	Childrno
of	of	k?	of
Bosnia	Bosnium	k1gNnPc1	Bosnium
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
Pavarotti	Pavarotť	k1gFnSc6	Pavarotť
&	&	k?	&
Friends	Friendsa	k1gFnPc2	Friendsa
for	forum	k1gNnPc2	forum
War	War	k1gMnSc1	War
Child	Child	k1gMnSc1	Child
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Pavarotti	Pavarotť	k1gFnSc6	Pavarotť
&	&	k?	&
Friends	Friendsa	k1gFnPc2	Friendsa
for	forum	k1gNnPc2	forum
the	the	k?	the
Children	Childrno	k1gNnPc2	Childrno
of	of	k?	of
Liberia	Liberium	k1gNnPc1	Liberium
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
Pavarotti	Pavarotť	k1gFnSc2	Pavarotť
&	&	k?	&
Friends	Friends	k1gInSc1	Friends
for	forum	k1gNnPc2	forum
Cambodia	Cambodium	k1gNnSc2	Cambodium
and	and	k?	and
Tibet	Tibet	k1gInSc1	Tibet
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
–	–	k?	–
Pavarotti	Pavarotť	k1gFnSc2	Pavarotť
&	&	k?	&
Friends	Friends	k1gInSc1	Friends
for	forum	k1gNnPc2	forum
Afghanistan	Afghanistan	k1gInSc4	Afghanistan
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
Pavarotti	Pavarotť	k1gFnSc2	Pavarotť
canta	canto	k1gNnSc2	canto
Verdi	Verd	k1gMnPc1	Verd
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
–	–	k?	–
Pavarotti	Pavarotť	k1gFnSc2	Pavarotť
&	&	k?	&
Friends	Friends	k1gInSc1	Friends
2002	[number]	k4	2002
for	forum	k1gNnPc2	forum
Angola	Angola	k1gFnSc1	Angola
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
Royal	Royal	k1gMnSc1	Royal
Variety	varieta	k1gFnSc2	varieta
Performance	performance	k1gFnSc2	performance
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
diskografie	diskografie	k1gFnSc1	diskografie
není	být	k5eNaImIp3nS	být
úplná	úplný	k2eAgFnSc1d1	úplná
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Mamma	Mamma	k1gNnSc1	Mamma
-	-	kIx~	-
Decca	Decca	k1gFnSc1	Decca
EAN	EAN	kA	EAN
028941195920	[number]	k4	028941195920
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Amore	Amor	k1gMnSc5	Amor
-	-	kIx~	-
Decca	Decc	k1gInSc2	Decc
EAN	EAN	kA	EAN
028943671927	[number]	k4	028943671927
</s>
</p>
<p>
<s>
===	===	k?	===
The	The	k1gMnPc7	The
Three	Thre	k1gFnSc2	Thre
Tenors	Tenors	k1gInSc1	Tenors
===	===	k?	===
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Three	Thre	k1gInSc2	Thre
Tenors	Tenors	k1gInSc1	Tenors
in	in	k?	in
Concert	Concert	k1gInSc1	Concert
1994	[number]	k4	1994
-	-	kIx~	-
Atlantic	Atlantice	k1gFnPc2	Atlantice
EAN	EAN	kA	EAN
0	[number]	k4	0
<g/>
75678261428	[number]	k4	75678261428
<g/>
,	,	kIx,	,
CD	CD	kA	CD
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
The	The	k1gFnSc1	The
Three	Thre	k1gFnSc2	Thre
Tenors	Tenorsa	k1gFnPc2	Tenorsa
Christmas	Christmas	k1gMnSc1	Christmas
-	-	kIx~	-
Sony	Sony	kA	Sony
EAN	EAN	kA	EAN
696998913127	[number]	k4	696998913127
<g/>
,	,	kIx,	,
CD	CD	kA	CD
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
the	the	k?	the
Three	Three	k1gInSc1	Three
Tenors	Tenors	k1gInSc1	Tenors
-	-	kIx~	-
Greatest	Greatest	k1gFnSc1	Greatest
Trios	triosa	k1gFnPc2	triosa
-	-	kIx~	-
Decca	Decca	k1gFnSc1	Decca
EAN	EAN	kA	EAN
0	[number]	k4	0
<g/>
28946699928	[number]	k4	28946699928
<g/>
,	,	kIx,	,
CD	CD	kA	CD
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Original	Original	k1gFnSc1	Original
Three	Thre	k1gFnSc2	Thre
Tenors	Tenorsa	k1gFnPc2	Tenorsa
Concert	Concert	k1gMnSc1	Concert
-	-	kIx~	-
Decca	Decca	k1gMnSc1	Decca
<g/>
,	,	kIx,	,
EAN	EAN	kA	EAN
00044007431894	[number]	k4	00044007431894
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
The	The	k1gFnSc1	The
Three	Thre	k1gFnSc2	Thre
Tenors	Tenorsa	k1gFnPc2	Tenorsa
At	At	k1gMnSc1	At
Christmas	Christmas	k1gMnSc1	Christmas
-	-	kIx~	-
Decca	Decca	k1gMnSc1	Decca
EAN	EAN	kA	EAN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
28947803362	[number]	k4	28947803362
<g/>
,	,	kIx,	,
CD	CD	kA	CD
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LEWISOVÁ	LEWISOVÁ	kA	LEWISOVÁ
<g/>
,	,	kIx,	,
Marcia	Marcium	k1gNnSc2	Marcium
<g/>
.	.	kIx.	.
</s>
<s>
Soukromý	soukromý	k2eAgInSc1d1	soukromý
život	život	k1gInSc1	život
božských	božský	k2eAgMnPc2d1	božský
pěvců	pěvec	k1gMnPc2	pěvec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rybka	rybka	k1gFnSc1	rybka
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86182	[number]	k4	86182
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
text	text	k1gInSc1	text
uvolněný	uvolněný	k2eAgInSc1d1	uvolněný
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
GFDL	GFDL	kA	GFDL
ze	z	k7c2	z
stránky	stránka	k1gFnSc2	stránka
na	na	k7c6	na
webu	web	k1gInSc6	web
Kulturák	kulturák	k1gInSc1	kulturák
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
José	José	k6eAd1	José
Carreras	Carreras	k1gMnSc1	Carreras
</s>
</p>
<p>
<s>
Plácido	Plácida	k1gFnSc5	Plácida
Domingo	Dominga	k1gFnSc5	Dominga
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Luciano	Luciana	k1gFnSc5	Luciana
Pavarotti	Pavarott	k2eAgMnPc1d1	Pavarott
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Luciano	Luciana	k1gFnSc5	Luciana
Pavarotti	Pavarotti	k1gNnPc3	Pavarotti
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
operní	operní	k2eAgFnSc1d1	operní
legenda	legenda	k1gFnSc1	legenda
Luciano	Luciana	k1gFnSc5	Luciana
Pavarotti	Pavarotti	k1gNnSc7	Pavarotti
</s>
</p>
<p>
<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
<g/>
:	:	kIx,	:
Luciano	Luciana	k1gFnSc5	Luciana
Pavarotti	Pavarott	k1gMnPc5	Pavarott
-	-	kIx~	-
La	la	k1gNnSc4	la
Donna	donna	k1gFnSc1	donna
È	È	k?	È
Mobile	mobile	k1gNnSc1	mobile
(	(	kIx(	(
<g/>
Rigoletto	Rigoletto	k1gNnSc1	Rigoletto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
<g/>
:	:	kIx,	:
Luciano	Luciana	k1gFnSc5	Luciana
Pavarotti	Pavarotti	k1gNnPc7	Pavarotti
-	-	kIx~	-
'	'	kIx"	'
<g/>
O	o	k7c6	o
sole	sol	k1gInSc5	sol
mio	mio	k?	mio
</s>
</p>
<p>
<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
<g/>
:	:	kIx,	:
Pavarotti	Pavarotti	k1gNnSc1	Pavarotti
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Legendary	Legendar	k1gMnPc7	Legendar
High	Higha	k1gFnPc2	Higha
C	C	kA	C
'	'	kIx"	'
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
<g/>
:	:	kIx,	:
James	James	k1gMnSc1	James
Brown	Brown	k1gMnSc1	Brown
&	&	k?	&
Luciano	Luciana	k1gFnSc5	Luciana
Pavarotti	Pavarott	k1gMnPc5	Pavarott
-	-	kIx~	-
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
Man	mana	k1gFnPc2	mana
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
World	World	k1gInSc1	World
</s>
</p>
<p>
<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
<g/>
:	:	kIx,	:
Pavarotti	Pavarotti	k1gNnSc1	Pavarotti
Last	Lasta	k1gFnPc2	Lasta
Performance	performance	k1gFnSc2	performance
"	"	kIx"	"
<g/>
Nessun	Nessun	k1gInSc1	Nessun
Dorma	Dormum	k1gNnSc2	Dormum
<g/>
"	"	kIx"	"
@	@	kIx~	@
Torino	Torino	k1gNnSc1	Torino
2006	[number]	k4	2006
</s>
</p>
