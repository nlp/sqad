<p>
<s>
Předkřesťanští	předkřesťanský	k2eAgMnPc1d1	předkřesťanský
Slované	Slovan	k1gMnPc1	Slovan
věřili	věřit	k5eAaImAgMnP	věřit
v	v	k7c6	v
existenci	existence	k1gFnSc6	existence
duchů	duch	k1gMnPc2	duch
kteří	který	k3yQgMnPc1	který
bdí	bdít	k5eAaImIp3nS	bdít
nad	nad	k7c7	nad
lidským	lidský	k2eAgInSc7d1	lidský
osudem	osud	k1gInSc7	osud
a	a	k8xC	a
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
že	že	k8xS	že
můžou	můžou	k?	můžou
jej	on	k3xPp3gMnSc4	on
můžou	můžou	k?	můžou
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
takové	takový	k3xDgFnPc4	takový
bytosti	bytost	k1gFnPc4	bytost
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
především	především	k9	především
Roda	Roda	k1gMnSc1	Roda
<g/>
,	,	kIx,	,
zmiňovaného	zmiňovaný	k2eAgInSc2d1	zmiňovaný
již	již	k6eAd1	již
v	v	k7c6	v
raných	raný	k2eAgFnPc2d1	raná
v	v	k7c6	v
ruských	ruský	k2eAgInPc6d1	ruský
pramenech	pramen	k1gInPc6	pramen
<g/>
,	,	kIx,	,
chápaného	chápaný	k2eAgInSc2d1	chápaný
některými	některý	k3yIgMnPc7	některý
badateli	badatel	k1gMnPc7	badatel
jako	jako	k8xS	jako
démon	démon	k1gMnSc1	démon
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ním	on	k3xPp3gInSc7	on
jsou	být	k5eAaImIp3nP	být
zmiňovány	zmiňován	k2eAgFnPc1d1	zmiňována
i	i	k9	i
Rožanice	Rožanice	k1gFnPc1	Rožanice
<g/>
,	,	kIx,	,
ženské	ženská	k1gFnPc1	ženská
bytosti	bytost	k1gFnSc3	bytost
<g/>
,	,	kIx,	,
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
pozdějších	pozdní	k2eAgInPc6d2	pozdější
pramenech	pramen	k1gInPc6	pramen
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
Rod	rod	k1gInSc1	rod
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
<g/>
,	,	kIx,	,
chápány	chápán	k2eAgInPc1d1	chápán
jako	jako	k8xC	jako
dárkyně	dárkyně	k1gFnSc1	dárkyně
osudu	osud	k1gInSc2	osud
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
mimo	mimo	k7c4	mimo
Rusko	Rusko	k1gNnSc4	Rusko
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
podobné	podobný	k2eAgFnPc1d1	podobná
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
české	český	k2eAgFnPc1d1	Česká
sudičky	sudička	k1gFnPc1	sudička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
obdobou	obdoba	k1gFnSc7	obdoba
řeckých	řecký	k2eAgFnPc2d1	řecká
Moir	Moira	k1gFnPc2	Moira
a	a	k8xC	a
severských	severský	k2eAgFnPc2d1	severská
Norn	Norna	k1gFnPc2	Norna
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
sporadicky	sporadicky	k6eAd1	sporadicky
i	i	k9	i
v	v	k7c6	v
raných	raný	k2eAgInPc6d1	raný
písemných	písemný	k2eAgInPc6d1	písemný
pramenech	pramen	k1gInPc6	pramen
<g/>
,	,	kIx,	,
objevovali	objevovat	k5eAaImAgMnP	objevovat
personifikace	personifikace	k1gFnPc4	personifikace
šťastného	šťastný	k2eAgMnSc2d1	šťastný
či	či	k8xC	či
nešťastného	šťastný	k2eNgInSc2d1	nešťastný
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
personifikovaný	personifikovaný	k2eAgInSc1d1	personifikovaný
Osud	osud	k1gInSc1	osud
<g/>
,	,	kIx,	,
nemoci	nemoc	k1gFnPc1	nemoc
i	i	k8xC	i
Smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
lidové	lidový	k2eAgFnSc6d1	lidová
kultuře	kultura	k1gFnSc6	kultura
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
Dolja	Dolja	k1gFnSc1	Dolja
<g/>
,	,	kIx,	,
personifikovaný	personifikovaný	k2eAgInSc1d1	personifikovaný
osud	osud	k1gInSc1	osud
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgNnSc1d1	zajišťující
štěstí	štěstí	k1gNnSc1	štěstí
v	v	k7c6	v
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
dárci	dárce	k1gMnPc7	dárce
je	být	k5eAaImIp3nS	být
především	především	k9	především
rožanice	rožanice	k1gFnSc1	rožanice
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
rod	rod	k1gInSc1	rod
tedy	tedy	k8xC	tedy
předci	předek	k1gMnPc1	předek
<g/>
,	,	kIx,	,
v	v	k7c6	v
žalozpěvech	žalozpěv	k1gInPc6	žalozpěv
děti	dítě	k1gFnPc1	dítě
často	často	k6eAd1	často
naříkají	naříkat	k5eAaBmIp3nP	naříkat
že	že	k8xS	že
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
Dolju	Dolju	k1gMnPc1	Dolju
nedostali	dostat	k5eNaPmAgMnP	dostat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
popisována	popisován	k2eAgFnSc1d1	popisována
jako	jako	k8xS	jako
bílá	bílý	k2eAgFnSc1d1	bílá
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
chatrném	chatrný	k2eAgInSc6d1	chatrný
oděvu	oděv	k1gInSc6	oděv
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
se	se	k3xPyFc4	se
proměnit	proměnit	k5eAaPmF	proměnit
v	v	k7c4	v
různé	různý	k2eAgFnPc4d1	různá
podoby	podoba	k1gFnPc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Bydlí	bydlet	k5eAaImIp3nS	bydlet
za	za	k7c7	za
pecí	pec	k1gFnSc7	pec
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
košem	koš	k1gInSc7	koš
ve	v	k7c6	v
světnici	světnice	k1gFnSc6	světnice
a	a	k8xC	a
stará	starat	k5eAaImIp3nS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
svého	svůj	k3xOyFgMnSc4	svůj
člověka	člověk	k1gMnSc4	člověk
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
dívky	dívka	k1gFnSc2	dívka
ji	on	k3xPp3gFnSc4	on
prosí	prosit	k5eAaImIp3nS	prosit
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gNnSc4	on
doprovodila	doprovodit	k5eAaPmAgFnS	doprovodit
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
příbytku	příbytek	k1gInSc2	příbytek
<g/>
.	.	kIx.	.
</s>
<s>
Dolja	Dolja	k6eAd1	Dolja
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
hospodářstvím	hospodářství	k1gNnSc7	hospodářství
i	i	k8xC	i
zdravím	zdraví	k1gNnSc7	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Dolje	Dolje	k1gFnSc1	Dolje
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
srbská	srbský	k2eAgFnSc1d1	Srbská
Sreća	Sreća	k1gFnSc1	Sreća
<g/>
,	,	kIx,	,
zmiňovaná	zmiňovaný	k2eAgFnSc1d1	zmiňovaná
již	již	k6eAd1	již
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,,	,,	k?	,,
zobrazována	zobrazován	k2eAgFnSc1d1	zobrazována
jako	jako	k8xS	jako
krásná	krásný	k2eAgFnSc1d1	krásná
dívka	dívka	k1gFnSc1	dívka
předoucí	předoucí	k2eAgFnSc4d1	předoucí
zlatou	zlatý	k2eAgFnSc4d1	zlatá
nit	nit	k1gFnSc4	nit
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
také	také	k9	také
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
šťastný	šťastný	k2eAgInSc1d1	šťastný
osud	osud	k1gInSc1	osud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Rusi	Rus	k1gFnSc6	Rus
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
i	i	k8xC	i
Dolja	Dolj	k2eAgFnSc1d1	Dolj
negativní	negativní	k2eAgFnSc1d1	negativní
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Nedolja	Nedolja	k1gFnSc1	Nedolja
či	či	k8xC	či
Licho	licho	k6eAd1	licho
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
naopak	naopak	k6eAd1	naopak
nic	nic	k3yNnSc1	nic
nedělá	dělat	k5eNaImIp3nS	dělat
<g/>
,	,	kIx,	,
přínáší	přínášit	k5eAaPmIp3nS	přínášit
smůlu	smůla	k1gFnSc4	smůla
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
pokusy	pokus	k1gInPc1	pokus
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
jsou	být	k5eAaImIp3nP	být
marné	marný	k2eAgInPc1d1	marný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Rusi	Rus	k1gFnSc6	Rus
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
i	i	k8xC	i
Dolja	Dolj	k2eAgFnSc1d1	Dolj
negativní	negativní	k2eAgFnSc1d1	negativní
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Nedolja	Nedolja	k1gFnSc1	Nedolja
či	či	k8xC	či
Licho	licho	k6eAd1	licho
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
naopak	naopak	k6eAd1	naopak
nic	nic	k3yNnSc1	nic
nedělá	dělat	k5eNaImIp3nS	dělat
<g/>
,	,	kIx,	,
přínáší	přínášit	k5eAaPmIp3nS	přínášit
smůlu	smůla	k1gFnSc4	smůla
a	a	k8xC	a
všechny	všechen	k3xTgInPc1	všechen
pokusy	pokus	k1gInPc1	pokus
zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
jsou	být	k5eAaImIp3nP	být
marné	marný	k2eAgInPc1d1	marný
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
licho	licho	k6eAd1	licho
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
ruským	ruský	k2eAgMnSc7d1	ruský
л	л	k?	л
lišnij	lišnít	k5eAaPmRp2nS	lišnít
"	"	kIx"	"
<g/>
zbytečný	zbytečný	k2eAgInSc1d1	zbytečný
<g/>
,	,	kIx,	,
přebytečný	přebytečný	k2eAgInSc1d1	přebytečný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
polským	polský	k2eAgMnSc7d1	polský
lichy	lichy	k?	lichy
"	"	kIx"	"
<g/>
nekvalitní	kvalitní	k2eNgInPc4d1	nekvalitní
<g/>
"	"	kIx"	"
a	a	k8xC	a
českým	český	k2eAgMnPc3d1	český
lichý	lichý	k2eAgInSc4d1	lichý
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
příslovích	přísloví	k1gNnPc6	přísloví
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
ruské	ruský	k2eAgNnSc1d1	ruské
"	"	kIx"	"
<g/>
Nebuďte	budit	k5eNaImRp2nP	budit
licho	licho	k6eAd1	licho
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
ticho	ticho	k1gNnSc1	ticho
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
polské	polský	k2eAgNnSc1d1	polské
"	"	kIx"	"
<g/>
Ticho	ticho	k1gNnSc1	ticho
<g/>
!	!	kIx.	!
</s>
<s>
Licho	licho	k6eAd1	licho
nespí	spát	k5eNaImIp3nS	spát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hrdina	hrdina	k1gMnSc1	hrdina
snaží	snažit	k5eAaImIp3nS	snažit
svého	svůj	k3xOyFgInSc2	svůj
špatného	špatný	k2eAgInSc2d1	špatný
osudu	osud	k1gInSc2	osud
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
licho	licho	k6eAd1	licho
přelstí	přelstít	k5eAaPmIp3nS	přelstít
<g/>
.	.	kIx.	.
</s>
<s>
Podobají	podobat	k5eAaImIp3nP	podobat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
další	další	k2eAgInPc1d1	další
pozdní	pozdní	k2eAgInPc1d1	pozdní
zosobnění	zosobnění	k1gNnPc4	zosobnění
špatného	špatný	k2eAgInSc2d1	špatný
osudu	osud	k1gInSc2	osud
jako	jako	k8xS	jako
Hoře	hora	k1gFnSc6	hora
<g/>
,	,	kIx,	,
Bída	bída	k1gFnSc1	bída
nebo	nebo	k8xC	nebo
Nouze	nouze	k1gFnSc1	nouze
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
bytosti	bytost	k1gFnPc1	bytost
se	se	k3xPyFc4	se
často	často	k6eAd1	často
objevují	objevovat	k5eAaImIp3nP	objevovat
jako	jako	k8xC	jako
hudená	hudený	k2eAgFnSc1d1	hudený
dívka	dívka	k1gFnSc1	dívka
či	či	k8xC	či
jednooká	jednooký	k2eAgFnSc1d1	jednooká
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
šedé	šedý	k2eAgFnSc2d1	šedá
stařeny	stařena	k1gFnSc2	stařena
s	s	k7c7	s
krhavýma	krhavý	k2eAgNnPc7d1	krhavé
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnPc4d1	zvaná
Nesreća	Nesreć	k2eAgNnPc4d1	Nesreć
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
znají	znát	k5eAaImIp3nP	znát
bytost	bytost	k1gFnSc4	bytost
zvanou	zvaný	k2eAgFnSc4d1	zvaná
zlydni	zlydnout	k5eAaPmRp2nS	zlydnout
"	"	kIx"	"
<g/>
zlé	zlý	k2eAgInPc1d1	zlý
dny	den	k1gInPc1	den
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
neváže	vázat	k5eNaImIp3nS	vázat
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
<g/>
.	.	kIx.	.
</s>
<s>
Věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c4	v
personifikovaný	personifikovaný	k2eAgInSc4d1	personifikovaný
osud	osud	k1gInSc4	osud
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
stařeny	stařena	k1gFnSc2	stařena
zvané	zvaný	k2eAgFnSc2d1	zvaná
sudba	sudba	k1gFnSc1	sudba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
zda	zda	k9	zda
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
bohatství	bohatství	k1gNnSc6	bohatství
či	či	k8xC	či
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc7	její
obdobou	obdoba	k1gFnSc7	obdoba
stařec	stařec	k1gMnSc1	stařec
zvaný	zvaný	k2eAgInSc1d1	zvaný
sud	sud	k1gInSc1	sud
či	či	k8xC	či
usud	usud	k1gInSc1	usud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
paláci	palác	k1gInSc6	palác
rozsypává	rozsypávat	k5eAaImIp3nS	rozsypávat
zlato	zlato	k1gNnSc4	zlato
nebo	nebo	k8xC	nebo
střepy	střep	k1gInPc4	střep
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
boháči	boháč	k1gMnPc1	boháč
či	či	k8xC	či
chudáci	chudák	k1gMnPc1	chudák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
démony	démon	k1gMnPc7	démon
osudu	osud	k1gInSc2	osud
lze	lze	k6eAd1	lze
spojit	spojit	k5eAaPmF	spojit
i	i	k9	i
personifikace	personifikace	k1gFnSc1	personifikace
nemocí	nemoc	k1gFnPc2	nemoc
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
zosobnění	zosobnění	k1gNnSc6	zosobnění
moru	mor	k1gInSc2	mor
<g/>
:	:	kIx,	:
ruskou	ruský	k2eAgFnSc4d1	ruská
čumu	čuma	k1gFnSc4	čuma
<g/>
,	,	kIx,	,
polskou	polský	k2eAgFnSc4d1	polská
dżumu	dżuma	k1gFnSc4	dżuma
a	a	k8xC	a
jihoslovanskou	jihoslovanský	k2eAgFnSc4d1	Jihoslovanská
kugu	kuga	k1gFnSc4	kuga
nebo	nebo	k8xC	nebo
také	také	k9	také
horečky	horečka	k1gFnPc1	horečka
pristrit	pristrit	k1gInSc1	pristrit
<g/>
,	,	kIx,	,
zimnice	zimnice	k1gFnSc1	zimnice
lichoradka	lichoradka	k1gFnSc1	lichoradka
a	a	k8xC	a
neštovic	neštovice	k1gFnPc2	neštovice
ospa	ospa	k1gFnSc1	ospa
matuška	matuška	k1gFnSc1	matuška
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
měla	mít	k5eAaImAgFnS	mít
podobu	podoba	k1gFnSc4	podoba
bíle	bíle	k6eAd1	bíle
oděné	oděný	k2eAgFnPc1d1	oděná
dívky	dívka	k1gFnPc1	dívka
či	či	k8xC	či
ženy	žena	k1gFnPc1	žena
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
podzemním	podzemní	k2eAgNnSc6d1	podzemní
sídle	sídlo	k1gNnSc6	sídlo
hořely	hořet	k5eAaImAgFnP	hořet
svíčky	svíčka	k1gFnPc1	svíčka
vyměřujícím	vyměřující	k2eAgFnPc3d1	vyměřující
lidské	lidský	k2eAgFnPc1d1	lidská
životy	život	k1gInPc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Zjevovala	zjevovat	k5eAaImAgFnS	zjevovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c6	na
křižovatkách	křižovatka	k1gFnPc6	křižovatka
a	a	k8xC	a
u	u	k7c2	u
postelí	postel	k1gFnPc2	postel
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
stála	stát	k5eAaImAgFnS	stát
u	u	k7c2	u
hlavy	hlava	k1gFnSc2	hlava
zvěstovalo	zvěstovat	k5eAaImAgNnS	zvěstovat
to	ten	k3xDgNnSc1	ten
úmrtí	úmrtí	k1gNnSc1	úmrtí
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
u	u	k7c2	u
nohou	noha	k1gFnPc2	noha
tak	tak	k8xS	tak
uzdravení	uzdravení	k1gNnSc2	uzdravení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
démonů	démon	k1gMnPc2	démon
špatného	špatný	k2eAgInSc2d1	špatný
osudu	osud	k1gInSc2	osud
a	a	k8xC	a
nemocí	nemoc	k1gFnPc2	nemoc
šla	jít	k5eAaImAgFnS	jít
oklamat	oklamat	k5eAaPmF	oklamat
jen	jen	k9	jen
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
