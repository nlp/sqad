<s>
Hynek	Hynek	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Heger	Heger	k1gMnSc1
</s>
<s>
Hynek	Hynek	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Heger	Heger	k1gMnSc1
Hynek	Hynek	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Heger	Heger	k1gMnSc1
(	(	kIx(
<g/>
kreslil	kreslit	k5eAaImAgMnS
Jan	Jan	k1gMnSc1
Vilímek	Vilímek	k1gMnSc1
1884	#num#	k4
<g/>
)	)	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1808	#num#	k4
<g/>
PoličkaRakouské	PoličkaRakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1854	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
45	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Vídeň	Vídeň	k1gFnSc1
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Povolání	povolání	k1gNnSc2
</s>
<s>
pedagog	pedagog	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hynek	Hynek	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Heger	Heger	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1808	#num#	k4
Polička	Polička	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1854	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
německé	německý	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
uváděný	uváděný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
Ignaz	Ignaz	k1gMnSc1
Jacob	Jacob	k1gMnSc1
Heger	Heger	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
česko-rakouský	česko-rakouský	k2eAgMnSc1d1
těsnopisec	těsnopisec	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
první	první	k4xOgFnSc2
české	český	k2eAgFnSc2d1
těsnopisné	těsnopisný	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Studoval	studovat	k5eAaImAgMnS
piaristické	piaristický	k2eAgNnSc4d1
gymnázium	gymnázium	k1gNnSc4
v	v	k7c6
Litomyšli	Litomyšl	k1gFnSc6
<g/>
,	,	kIx,
filozofii	filozofie	k1gFnSc4
a	a	k8xC
práva	právo	k1gNnPc4
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
a	a	k8xC
právnická	právnický	k2eAgNnPc4d1
studia	studio	k1gNnPc4
dokončil	dokončit	k5eAaPmAgMnS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Potom	potom	k6eAd1
nastoupil	nastoupit	k5eAaPmAgMnS
do	do	k7c2
zaměstnání	zaměstnání	k1gNnSc2
jako	jako	k8xS,k8xC
praktikant	praktikant	k1gMnSc1
u	u	k7c2
vídeňského	vídeňský	k2eAgInSc2d1
soudu	soud	k1gInSc2
a	a	k8xC
pracoval	pracovat	k5eAaImAgMnS
i	i	k9
v	v	k7c6
právních	právní	k2eAgFnPc6d1
kancelářích	kancelář	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Tam	tam	k6eAd1
si	se	k3xPyFc3
uvědomil	uvědomit	k5eAaPmAgMnS
nedokonalost	nedokonalost	k1gFnSc4
běžného	běžný	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
neumožňovalo	umožňovat	k5eNaImAgNnS
psát	psát	k5eAaImF
dostatečně	dostatečně	k6eAd1
rychle	rychle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
proto	proto	k8xC
zajímat	zajímat	k5eAaImF
o	o	k7c4
těsnopis	těsnopis	k1gInSc4
a	a	k8xC
jako	jako	k9
nejvhodnější	vhodný	k2eAgInSc4d3
z	z	k7c2
několika	několik	k4yIc2
možností	možnost	k1gFnPc2
vybral	vybrat	k5eAaPmAgInS
roku	rok	k1gInSc2
1839	#num#	k4
soustavu	soustava	k1gFnSc4
německého	německý	k2eAgMnSc2d1
vynálezce	vynálezce	k1gMnSc2
F.	F.	kA
X.	X.	kA
Gabelsbergera	Gabelsbergera	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tu	tu	k6eAd1
potom	potom	k6eAd1
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
propagoval	propagovat	k5eAaImAgMnS
do	do	k7c2
té	ten	k3xDgFnSc2
míry	míra	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
sám	sám	k3xTgMnSc1
Gabelsberger	Gabelsberger	k1gMnSc1
jej	on	k3xPp3gMnSc4
označil	označit	k5eAaPmAgMnS
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
apoštola	apoštol	k1gMnSc4
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Nejprve	nejprve	k6eAd1
nové	nový	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
s	s	k7c7
úspěchem	úspěch	k1gInSc7
vyzkoušel	vyzkoušet	k5eAaPmAgMnS
při	při	k7c6
zápisu	zápis	k1gInSc6
schůze	schůze	k1gFnSc2
lesníků	lesník	k1gMnPc2
v	v	k7c6
Brně	Brno	k1gNnSc6
(	(	kIx(
<g/>
1841	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1842	#num#	k4
založil	založit	k5eAaPmAgMnS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
těsnopisný	těsnopisný	k2eAgInSc4d1
ústav	ústav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
sice	sice	k8xC
neměl	mít	k5eNaImAgInS
kvůli	kvůli	k7c3
malému	malý	k2eAgInSc3d1
zájmu	zájem	k1gInSc3
veřejnosti	veřejnost	k1gFnSc2
dlouhého	dlouhý	k2eAgNnSc2d1
trvání	trvání	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Hegerových	Hegerových	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
si	se	k3xPyFc3
všimla	všimnout	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
a	a	k8xC
už	už	k6eAd1
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
mu	on	k3xPp3gMnSc3
nabídla	nabídnout	k5eAaPmAgFnS
místo	místo	k7c2
mimořádného	mimořádný	k2eAgMnSc2d1
profesora	profesor	k1gMnSc2
těsnopisu	těsnopis	k1gInSc2
na	na	k7c6
vídeňské	vídeňský	k2eAgFnSc6d1
polytechnice	polytechnika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1844	#num#	k4
zavítal	zavítat	k5eAaPmAgMnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
propagaci	propagace	k1gFnSc4
mu	on	k3xPp3gMnSc3
dělal	dělat	k5eAaImAgMnS
časopis	časopis	k1gInSc4
Česká	český	k2eAgFnSc1d1
včela	včela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gFnSc4
přednášku	přednáška	k1gFnSc4
přišla	přijít	k5eAaPmAgFnS
řada	řada	k1gFnSc1
významných	významný	k2eAgMnPc2d1
obrozenců	obrozenec	k1gMnPc2
–	–	k?
Josef	Josef	k1gMnSc1
Jungmann	Jungmann	k1gMnSc1
<g/>
,	,	kIx,
Šebestián	Šebestián	k1gMnSc1
Hněvkovský	Hněvkovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Ladislav	Ladislav	k1gMnSc1
Rieger	Rieger	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Bělský	Bělský	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Franta	Franta	k1gMnSc1
Šumavský	šumavský	k2eAgMnSc1d1
a	a	k8xC
Jiří	Jiří	k1gMnSc1
Krouský	Krouský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInPc1d1
dva	dva	k4xCgInPc1
se	s	k7c7
stali	stát	k5eAaPmAgMnP
jeho	jeho	k3xOp3gMnPc7
nadšenými	nadšený	k2eAgMnPc7d1
spolupracovníky	spolupracovník	k1gMnPc7
a	a	k8xC
následovníky	následovník	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1845	#num#	k4
přijel	přijet	k5eAaPmAgMnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
na	na	k7c4
další	další	k2eAgFnSc4d1
prezentaci	prezentace	k1gFnSc4
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
Frantou	Franta	k1gMnSc7
Šumavským	šumavský	k2eAgMnSc7d1
pak	pak	k6eAd1
zpracoval	zpracovat	k5eAaPmAgInS
Soustavu	soustava	k1gFnSc4
českoslovanského	českoslovanský	k2eAgInSc2d1
rychlotěsnopisu	rychlotěsnopis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebyla	být	k5eNaImAgFnS
ale	ale	k9
vydána	vydán	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
je	být	k5eAaImIp3nS
považován	považován	k2eAgMnSc1d1
za	za	k7c2
jednoho	jeden	k4xCgInSc2
z	z	k7c2
prvních	první	k4xOgInPc2
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
převedl	převést	k5eAaPmAgMnS
Gabelsbergerovo	Gabelsbergerův	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
do	do	k7c2
cizího	cizí	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
<g/>
,	,	kIx,
se	s	k7c7
svoláním	svolání	k1gNnSc7
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
nastala	nastat	k5eAaPmAgFnS
velká	velký	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
po	po	k7c6
zapisovatelích	zapisovatel	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heger	Heger	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
ředitelem	ředitel	k1gMnSc7
stenografické	stenografický	k2eAgFnSc2d1
kanceláře	kancelář	k1gFnSc2
na	na	k7c6
zasedání	zasedání	k1gNnSc6
říšského	říšský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
a	a	k8xC
Kroměříži	Kroměříž	k1gFnSc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
finanční	finanční	k2eAgFnSc1d1
situace	situace	k1gFnSc1
se	se	k3xPyFc4
konečně	konečně	k6eAd1
zlepšila	zlepšit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1849	#num#	k4
vydal	vydat	k5eAaPmAgInS
spis	spis	k1gInSc1
Kurze	kurz	k1gInSc6
Anleitung	Anleitung	k1gInSc4
zur	zur	k?
Stenographie	Stenographie	k1gFnSc2
für	für	k?
die	die	k?
vier	vier	k1gInSc1
slaw	slaw	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hauptsprachen	Hauptsprachen	k1gInSc1
(	(	kIx(
<g/>
Krátký	krátký	k2eAgInSc1d1
úvod	úvod	k1gInSc1
do	do	k7c2
těsnopisu	těsnopis	k1gInSc2
pro	pro	k7c4
čtyři	čtyři	k4xCgInPc4
hlavní	hlavní	k2eAgInPc4d1
slovanské	slovanský	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
–	–	k?
myšleno	myslet	k5eAaImNgNnS
český	český	k2eAgMnSc1d1
<g/>
,	,	kIx,
polský	polský	k2eAgMnSc1d1
<g/>
,	,	kIx,
ilyrský	ilyrský	k2eAgMnSc1d1
a	a	k8xC
ruský	ruský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
založil	založit	k5eAaPmAgInS
Ústřední	ústřední	k2eAgInSc1d1
svaz	svaz	k1gInSc1
stenografů	stenograf	k1gMnPc2
rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Věnoval	věnovat	k5eAaImAgInS,k5eAaPmAgInS
se	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
i	i	k8xC
literární	literární	k2eAgFnSc2d1
a	a	k8xC
kaligrafické	kaligrafický	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
vídeňské	vídeňský	k2eAgFnSc6d1
státní	státní	k2eAgFnSc6d1
tiskárně	tiskárna	k1gFnSc6
pomohl	pomoct	k5eAaPmAgInS
zhotovit	zhotovit	k5eAaPmF
těsnopisné	těsnopisný	k2eAgFnPc4d1
tiskové	tiskový	k2eAgFnPc4d1
formy	forma	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
profesorem	profesor	k1gMnSc7
vídeňské	vídeňský	k2eAgFnSc2d1
Tereziánské	tereziánský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
a	a	k8xC
Josefínského	josefínský	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1852	#num#	k4
se	se	k3xPyFc4
účastnil	účastnit	k5eAaImAgMnS
sjezdu	sjezd	k1gInSc3
stenografů	stenograf	k1gMnPc2
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
zásluhy	zásluha	k1gFnSc2
ocenil	ocenit	k5eAaPmAgMnS
jak	jak	k6eAd1
rakouský	rakouský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
Dobrotivý	dobrotivý	k2eAgMnSc1d1
(	(	kIx(
<g/>
pořídil	pořídit	k5eAaPmAgMnS
si	se	k3xPyFc3
jeho	jeho	k3xOp3gFnSc4
knihu	kniha	k1gFnSc4
Stručná	stručný	k2eAgFnSc1d1
mluvnice	mluvnice	k1gFnSc1
stenotachygrafie	stenotachygrafie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
pruský	pruský	k2eAgMnSc1d1
král	král	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
udělil	udělit	k5eAaPmAgMnS
mu	on	k3xPp3gInSc3
medaili	medaile	k1gFnSc4
Za	za	k7c4
umění	umění	k1gNnSc4
a	a	k8xC
vědu	věda	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Heger	Heger	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
na	na	k7c4
revmatismus	revmatismus	k1gInSc4
a	a	k8xC
plicní	plicní	k2eAgFnSc4d1
vodnatelnost	vodnatelnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pohřben	pohřbít	k5eAaPmNgMnS
v	v	k7c6
Matzleinsdorfu	Matzleinsdorf	k1gInSc6
<g/>
,	,	kIx,
pomník	pomník	k1gInSc4
mu	on	k3xPp3gMnSc3
vybudoval	vybudovat	k5eAaPmAgInS
spolek	spolek	k1gInSc1
rakouských	rakouský	k2eAgMnPc2d1
těsnopisců	těsnopisec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1876	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Poličce	Polička	k1gFnSc6
konala	konat	k5eAaImAgFnS
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
počest	počest	k1gFnSc4
slavnost	slavnost	k1gFnSc4
<g/>
,	,	kIx,
během	během	k7c2
níž	jenž	k3xRgFnSc2
po	po	k7c6
něm	on	k3xPp3gMnSc6
pojmenovali	pojmenovat	k5eAaPmAgMnP
ulici	ulice	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
rodný	rodný	k2eAgInSc4d1
dům	dům	k1gInSc4
umístili	umístit	k5eAaPmAgMnP
pamětní	pamětní	k2eAgFnSc4d1
desku	deska	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
zrušení	zrušení	k1gNnSc6
hřbitova	hřbitov	k1gInSc2
byly	být	k5eAaImAgInP
ostatky	ostatek	k1gInPc1
přemístěny	přemístěn	k2eAgInPc1d1
na	na	k7c4
Vídeňský	vídeňský	k2eAgInSc4d1
ústřední	ústřední	k2eAgInSc4d1
hřbitov	hřbitov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
osudy	osud	k1gInPc1
Hegerovy	Hegerův	k2eAgFnSc2d1
těsnopisné	těsnopisný	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
</s>
<s>
Jakkoli	jakkoli	k8xS
měl	mít	k5eAaImAgMnS
Heger	Heger	k1gMnSc1
velký	velký	k2eAgMnSc1d1
podíl	podíl	k1gInSc4
na	na	k7c4
rozšíření	rozšíření	k1gNnSc4
těsnopisných	těsnopisný	k2eAgFnPc2d1
znalostí	znalost	k1gFnPc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
jím	on	k3xPp3gInSc7
navržená	navržený	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
neměla	mít	k5eNaImAgFnS
dlouhého	dlouhý	k2eAgNnSc2d1
trvání	trvání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
výběr	výběr	k1gInSc1
některých	některý	k3yIgInPc2
znaků	znak	k1gInPc2
byl	být	k5eAaImAgInS
nevhodný	vhodný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heger	Heger	k1gMnSc1
také	také	k9
z	z	k7c2
úcty	úcta	k1gFnSc2
před	před	k7c7
velkým	velký	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
(	(	kIx(
<g/>
Gabelsbergerem	Gabelsberger	k1gInSc7
<g/>
)	)	kIx)
nechtěl	chtít	k5eNaImAgMnS
do	do	k7c2
jeho	jeho	k3xOp3gFnSc2
soustavy	soustava	k1gFnSc2
vnášet	vnášet	k5eAaImF
bez	bez	k7c2
souhlasu	souhlas	k1gInSc2
mnoho	mnoho	k4c1
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
jedinou	jediný	k2eAgFnSc7d1
větší	veliký	k2eAgFnSc7d2
novinkou	novinka	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
Heger	Heger	k1gMnSc1
po	po	k7c6
dohodě	dohoda	k1gFnSc6
zavedl	zavést	k5eAaPmAgInS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
vytvoření	vytvoření	k1gNnSc1
znaku	znak	k1gInSc2
pro	pro	k7c4
č	č	k0
<g/>
,	,	kIx,
resp.	resp.	kA
tsch	tsch	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
dostal	dostat	k5eAaPmAgMnS
zpětně	zpětně	k6eAd1
i	i	k9
do	do	k7c2
soustavy	soustava	k1gFnSc2
německé	německý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hegerovi	Hegerův	k2eAgMnPc1d1
následovníci	následovník	k1gMnPc1
se	se	k3xPyFc4
více	hodně	k6eAd2
řídili	řídit	k5eAaImAgMnP
praktickými	praktický	k2eAgFnPc7d1
potřebami	potřeba	k1gFnPc7
a	a	k8xC
moderní	moderní	k2eAgInSc1d1
český	český	k2eAgInSc1d1
těsnopis	těsnopis	k1gInSc1
se	se	k3xPyFc4
od	od	k7c2
německého	německý	k2eAgInSc2d1
vzoru	vzor	k1gInSc2
vzdaloval	vzdalovat	k5eAaImAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1859	#num#	k4
byl	být	k5eAaImAgInS
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Jindřicha	Jindřich	k1gMnSc2
Fügnera	Fügner	k1gMnSc2
založen	založen	k2eAgInSc1d1
Pražský	pražský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
stenografů	stenograf	k1gMnPc2
gabelsbergerských	gabelsbergerský	k2eAgMnPc2d1
<g/>
,	,	kIx,
s	s	k7c7
cílem	cíl	k1gInSc7
šířit	šířit	k5eAaImF
znalosti	znalost	k1gFnSc3
tohoto	tento	k3xDgNnSc2
umění	umění	k1gNnSc2
v	v	k7c6
české	český	k2eAgFnSc6d1
inteligenci	inteligence	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1861	#num#	k4
vypsal	vypsat	k5eAaPmAgMnS
soutěž	soutěž	k1gFnSc4
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
převod	převod	k1gInSc4
Gabelsbergovy	Gabelsbergův	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
do	do	k7c2
češtiny	čeština	k1gFnSc2
<g/>
,	,	kIx,
dotovaný	dotovaný	k2eAgInSc4d1
cenou	cena	k1gFnSc7
25	#num#	k4
dukátů	dukát	k1gInPc2
ve	v	k7c6
zlatě	zlato	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sešlo	sejít	k5eAaPmAgNnS
se	se	k3xPyFc4
pět	pět	k4xCc1
návrhů	návrh	k1gInPc2
<g/>
,	,	kIx,
porota	porota	k1gFnSc1
(	(	kIx(
<g/>
Jiří	Jiří	k1gMnSc1
Krouský	Krouský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Eduard	Eduard	k1gMnSc1
Novotný	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
Dominik	Dominik	k1gMnSc1
Haasz	Haasz	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Prill	Prill	k1gMnSc1
<g/>
)	)	kIx)
ale	ale	k8xC
žádný	žádný	k3yNgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
nedoporučila	doporučit	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práce	práce	k1gFnSc1
ale	ale	k9
pokračovala	pokračovat	k5eAaImAgFnS
<g/>
;	;	kIx,
roku	rok	k1gInSc2
1863	#num#	k4
vyšel	vyjít	k5eAaPmAgInS
Těsnopis	těsnopis	k1gInSc1
český	český	k2eAgInSc1d1
jako	jako	k8xC,k8xS
výsledek	výsledek	k1gInSc1
činnosti	činnost	k1gFnSc2
sedmičlenného	sedmičlenný	k2eAgInSc2d1
týmu	tým	k1gInSc2
(	(	kIx(
<g/>
Eduard	Eduard	k1gMnSc1
Novotný	Novotný	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Brzobohatý	Brzobohatý	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Faigl	Faigl	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
Konrad	Konrad	k1gInSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Novotný	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
Stáně	Stáňa	k1gFnSc6
a	a	k8xC
František	František	k1gMnSc1
Tauer	Tauer	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
20	#num#	k4
letech	léto	k1gNnPc6
se	se	k3xPyFc4
dočkal	dočkat	k5eAaPmAgMnS
ještě	ještě	k9
dalších	další	k2eAgNnPc2d1
čtyř	čtyři	k4xCgNnPc2
vydání	vydání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komise	komise	k1gFnSc1
zároveň	zároveň	k6eAd1
spolupracovala	spolupracovat	k5eAaImAgFnS
s	s	k7c7
odbornými	odborný	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
v	v	k7c6
dalších	další	k2eAgFnPc6d1
slovanských	slovanský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
společně	společně	k6eAd1
s	s	k7c7
českou	český	k2eAgFnSc7d1
soustavou	soustava	k1gFnSc7
vznikla	vzniknout	k5eAaPmAgFnS
i	i	k9
polská	polský	k2eAgFnSc1d1
<g/>
,	,	kIx,
chorvatská	chorvatský	k2eAgFnSc1d1
<g/>
,	,	kIx,
ruská	ruský	k2eAgFnSc1d1
<g/>
,	,	kIx,
slovinská	slovinský	k2eAgFnSc1d1
<g/>
,	,	kIx,
srbská	srbský	k2eAgFnSc1d1
a	a	k8xC
bulharská	bulharský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
Hegerova	Hegerův	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
už	už	k6eAd1
prakticky	prakticky	k6eAd1
neznámá	známý	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komise	komise	k1gFnSc1
navíc	navíc	k6eAd1
prosadila	prosadit	k5eAaPmAgFnS
řadu	řada	k1gFnSc4
užitečných	užitečný	k2eAgFnPc2d1
radikálních	radikální	k2eAgFnPc2d1
změn	změna	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
záměnu	záměna	k1gFnSc4
p	p	k?
a	a	k8xC
g	g	kA
oproti	oproti	k7c3
německému	německý	k2eAgInSc3d1
vzoru	vzor	k1gInSc3
<g/>
,	,	kIx,
z	z	k7c2
důvodu	důvod	k1gInSc2
rozdílné	rozdílný	k2eAgFnSc2d1
četnosti	četnost	k1gFnSc2
těchto	tento	k3xDgNnPc2
písmen	písmeno	k1gNnPc2
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
českém	český	k2eAgInSc6d1
a	a	k8xC
německém	německý	k2eAgInSc6d1
textu	text	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
by	by	kYmCp3nS
si	se	k3xPyFc3
Heger	Heger	k1gMnSc1
asi	asi	k9
nedovolil	dovolit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc4d1
těsnopis	těsnopis	k1gInSc4
<g/>
,	,	kIx,
používaný	používaný	k2eAgInSc4d1
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
na	na	k7c6
původní	původní	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
nenavazuje	navazovat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
si	se	k3xPyFc3
i	i	k9
tehdejší	tehdejší	k2eAgFnPc1d1
generace	generace	k1gFnPc1
Čechů	Čech	k1gMnPc2
i	i	k8xC
německy	německy	k6eAd1
mluvících	mluvící	k2eAgMnPc2d1
Rakušanů	Rakušan	k1gMnPc2
Hegera	Heger	k1gMnSc2
vážila	vážit	k5eAaImAgFnS
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
průkopnickou	průkopnický	k2eAgFnSc4d1
práci	práce	k1gFnSc4
na	na	k7c4
rozšíření	rozšíření	k1gNnSc4
těsnopisu	těsnopis	k1gInSc2
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
jeho	jeho	k3xOp3gNnSc1
zavedení	zavedení	k1gNnSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
a	a	k8xC
snahu	snaha	k1gFnSc4
o	o	k7c4
co	co	k9
největší	veliký	k2eAgFnSc4d3
jednotu	jednota	k1gFnSc4
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
slovanských	slovanský	k2eAgFnPc2d1
těsnopisných	těsnopisný	k2eAgFnPc2d1
soustav	soustava	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SOA	SOA	kA
Zámrsk	Zámrsk	k1gInSc1
<g/>
,	,	kIx,
Matrika	matrika	k1gFnSc1
narozených	narozený	k2eAgFnPc2d1
1792-1817	1792-1817	k4
v	v	k7c6
Poličce	Polička	k1gFnSc6
<g/>
,	,	kIx,
sign	signum	k1gNnPc2
<g/>
.1655	.1655	k4
<g/>
,	,	kIx,
ukn	ukn	k?
<g/>
.7568	.7568	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.272	.272	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
online	onlinout	k5eAaPmIp3nS
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
SCHWARZ	Schwarz	k1gMnSc1
<g/>
,	,	kIx,
Anny	Anna	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heger	Heger	k1gMnSc1
<g/>
,	,	kIx,
Ignaz	Ignaz	k1gInSc1
Jacob	Jacoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Neue	Neue	k1gFnSc1
Deutsche	Deutsche	k1gFnSc1
Biographie	Biographie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Berlin	berlina	k1gFnPc2
<g/>
:	:	kIx,
Duncker	Duncker	k1gMnSc1
&	&	k?
Humblot	Humblot	k1gMnSc1
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
229	#num#	k4
-	-	kIx~
230	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
2	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hynek	Hynek	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Heger	Heger	k1gMnSc1
(	(	kIx(
<g/>
1808	#num#	k4
–	–	k?
1854	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jitřenka	Jitřenka	k1gFnSc1
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
PRAŽÁK	Pražák	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Otakar	Otakar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hynek	Hynek	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Heger	Heger	k1gMnSc1
a	a	k8xC
těsnopis	těsnopis	k1gInSc1
český	český	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlatá	zlatý	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1884	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
49	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
589	#num#	k4
-	-	kIx~
591	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
BAUER	Bauer	k1gMnSc1
<g/>
,	,	kIx,
E.	E.	kA
Heger	Heger	k1gMnSc1
<g/>
,	,	kIx,
Ignaz	Ignaz	k1gInSc1
Jacob	Jacoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Allgemeine	Allgemein	k1gMnSc5
Deutsche	Deutschus	k1gMnSc5
Biographie	Biographius	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leipzig	Leipzig	k1gMnSc1
<g/>
:	:	kIx,
Duncker	Duncker	k1gMnSc1
&	&	k?
Humblot	Humblot	k1gMnSc1
<g/>
,	,	kIx,
1880	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
275	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hynek	Hynek	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Heger	Heger	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pohřbení	pohřbení	k1gNnSc1
</s>
<s>
Článek	článek	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
s	s	k7c7
využitím	využití	k1gNnSc7
materiálů	materiál	k1gInPc2
z	z	k7c2
Digitálního	digitální	k2eAgInSc2d1
archivu	archiv	k1gInSc2
časopisů	časopis	k1gInPc2
ÚČL	ÚČL	kA
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
v.	v.	k?
v.	v.	k?
i.	i.	k?
(	(	kIx(
<g/>
http://archiv.ucl.cas.cz/	http://archiv.ucl.cas.cz/	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1040479	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
102573263	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1177	#num#	k4
7497	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
83726283	#num#	k4
</s>
