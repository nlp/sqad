<s>
Hynek	Hynek	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Heger	Heger	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1808	#num#	k4
Polička	Polička	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1854	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
německé	německý	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
uváděný	uváděný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
Ignaz	Ignaz	k1gMnSc1
Jacob	Jacob	k1gMnSc1
Heger	Heger	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
česko-rakouský	česko-rakouský	k2eAgMnSc1d1
těsnopisec	těsnopisec	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
první	první	k4xOgFnSc2
české	český	k2eAgFnSc2d1
těsnopisné	těsnopisný	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
.	.	kIx.
</s>