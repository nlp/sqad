<s>
V	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
probíhá	probíhat	k5eAaImIp3nS	probíhat
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
buněčného	buněčný	k2eAgNnSc2d1	buněčné
dýchání	dýchání	k1gNnSc2	dýchání
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
zejména	zejména	k9	zejména
glykolýzy	glykolýza	k1gFnSc2	glykolýza
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
cytosolu	cytosol	k1gInSc6	cytosol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
