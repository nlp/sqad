<p>
<s>
BMW	BMW	kA	BMW
řady	řada	k1gFnSc2	řada
5	[number]	k4	5
(	(	kIx(	(
<g/>
BMW	BMW	kA	BMW
5	[number]	k4	5
nebo	nebo	k8xC	nebo
anglicky	anglicky	k6eAd1	anglicky
BMW	BMW	kA	BMW
5	[number]	k4	5
Series	Series	k1gInSc4	Series
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
automobil	automobil	k1gInSc4	automobil
vyšší	vysoký	k2eAgFnSc2d2	vyšší
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
německé	německý	k2eAgFnSc2d1	německá
automobilky	automobilka	k1gFnSc2	automobilka
BMW	BMW	kA	BMW
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
generaci	generace	k1gFnSc3	generace
<g/>
.	.	kIx.	.
</s>
<s>
Legendárním	legendární	k2eAgInSc7d1	legendární
vozem	vůz	k1gInSc7	vůz
je	být	k5eAaImIp3nS	být
sportovní	sportovní	k2eAgInSc1d1	sportovní
model	model	k1gInSc1	model
M	M	kA	M
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
čtyřdveřový	čtyřdveřový	k2eAgInSc4d1	čtyřdveřový
vůz	vůz	k1gInSc4	vůz
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
s	s	k7c7	s
karosérií	karosérie	k1gFnSc7	karosérie
kombi	kombi	k1gNnSc2	kombi
označuje	označovat	k5eAaImIp3nS	označovat
BMW	BMW	kA	BMW
jako	jako	k8xS	jako
Touring	Touring	k1gInSc1	Touring
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
druhý	druhý	k4xOgInSc1	druhý
nejprodávanější	prodávaný	k2eAgInSc1d3	nejprodávanější
model	model	k1gInSc1	model
BMW	BMW	kA	BMW
po	po	k7c6	po
vozech	vůz	k1gInPc6	vůz
3	[number]	k4	3
<g/>
.	.	kIx.	.
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
z	z	k7c2	z
50	[number]	k4	50
<g/>
%	%	kIx~	%
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
zisku	zisk	k1gInSc6	zisk
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
pěti	pět	k4xCc7	pět
miliontý	miliontý	k4xOgInSc4	miliontý
vůz	vůz	k1gInSc4	vůz
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
limuzínu	limuzína	k1gFnSc4	limuzína
verze	verze	k1gFnSc1	verze
530	[number]	k4	530
<g/>
d	d	k?	d
v	v	k7c6	v
černé	černý	k2eAgFnSc6d1	černá
metalíze	metalíza	k1gFnSc6	metalíza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgFnSc1	první
generace	generace	k1gFnSc1	generace
-	-	kIx~	-
E12	E12	k1gFnSc1	E12
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
debutovala	debutovat	k5eAaBmAgFnS	debutovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
na	na	k7c6	na
frankfurtském	frankfurtský	k2eAgInSc6d1	frankfurtský
autosalónu	autosalón	k1gInSc6	autosalón
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
představeny	představen	k2eAgInPc1d1	představen
modely	model	k1gInPc1	model
BMW	BMW	kA	BMW
520	[number]	k4	520
a	a	k8xC	a
BMW	BMW	kA	BMW
520	[number]	k4	520
<g/>
i	i	k8xC	i
<g/>
,	,	kIx,	,
osazené	osazený	k2eAgFnSc6d1	osazená
4	[number]	k4	4
<g/>
-válcovým	álcův	k2eAgInSc7d1	-válcův
motorem	motor	k1gInSc7	motor
<g/>
,	,	kIx,	,
poskytujícím	poskytující	k2eAgInSc7d1	poskytující
115	[number]	k4	115
resp.	resp.	kA	resp.
130	[number]	k4	130
koňských	koňský	k2eAgFnPc2d1	koňská
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byl	být	k5eAaImAgMnS	být
tímto	tento	k3xDgInSc7	tento
představen	představit	k5eAaPmNgInS	představit
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
značení	značení	k1gNnSc2	značení
vozů	vůz	k1gInPc2	vůz
BMW	BMW	kA	BMW
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
první	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
určuje	určovat	k5eAaImIp3nS	určovat
modelovou	modelový	k2eAgFnSc4d1	modelová
řadu	řada	k1gFnSc4	řada
a	a	k8xC	a
následující	následující	k2eAgInPc4d1	následující
dvě	dva	k4xCgNnPc1	dva
informují	informovat	k5eAaBmIp3nP	informovat
o	o	k7c6	o
objemu	objem	k1gInSc6	objem
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neopakovatelný	opakovatelný	k2eNgInSc1d1	neopakovatelný
design	design	k1gInSc1	design
všech	všecek	k3xTgInPc2	všecek
automobilů	automobil	k1gInPc2	automobil
BMW	BMW	kA	BMW
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
stanovil	stanovit	k5eAaPmAgInS	stanovit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
designer	designer	k1gMnSc1	designer
Paul	Paul	k1gMnSc1	Paul
Bracq	Bracq	k1gMnSc1	Bracq
<g/>
.	.	kIx.	.
</s>
<s>
Protáhlé	protáhlý	k2eAgFnPc1d1	protáhlá
a	a	k8xC	a
hladké	hladký	k2eAgFnPc1d1	hladká
linie	linie	k1gFnPc1	linie
<g/>
,	,	kIx,	,
velká	velký	k2eAgNnPc1d1	velké
okna	okno	k1gNnPc1	okno
a	a	k8xC	a
nízká	nízký	k2eAgFnSc1d1	nízká
karoserie	karoserie	k1gFnSc1	karoserie
<g/>
,	,	kIx,	,
originální	originální	k2eAgInPc4d1	originální
prvky	prvek	k1gInPc4	prvek
-	-	kIx~	-
například	například	k6eAd1	například
zdvojené	zdvojený	k2eAgInPc4d1	zdvojený
přední	přední	k2eAgInPc4d1	přední
světlomety	světlomet	k1gInPc4	světlomet
či	či	k8xC	či
prohnutí	prohnutí	k1gNnSc4	prohnutí
zadních	zadní	k2eAgInPc2d1	zadní
sloupků	sloupek	k1gInPc2	sloupek
karoserie	karoserie	k1gFnSc2	karoserie
-	-	kIx~	-
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
stylu	styl	k1gInSc6	styl
a	a	k8xC	a
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
nových	nový	k2eAgFnPc2d1	nová
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
roce	rok	k1gInSc6	rok
výroby	výroba	k1gFnSc2	výroba
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
trh	trh	k1gInSc4	trh
uveden	uvést	k5eAaPmNgInS	uvést
první	první	k4xOgInSc1	první
model	model	k1gInSc1	model
s	s	k7c7	s
šestiválcovým	šestiválcový	k2eAgInSc7d1	šestiválcový
motorem	motor	k1gInSc7	motor
-	-	kIx~	-
BMW	BMW	kA	BMW
525	[number]	k4	525
<g/>
,	,	kIx,	,
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
145	[number]	k4	145
koňských	koňský	k2eAgFnPc2d1	koňská
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Úsilí	úsilí	k1gNnSc1	úsilí
o	o	k7c4	o
postupné	postupný	k2eAgNnSc4d1	postupné
zvyšování	zvyšování	k1gNnSc4	zvyšování
výkonů	výkon	k1gInPc2	výkon
motorů	motor	k1gInPc2	motor
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
důvodů	důvod	k1gInPc2	důvod
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
modelové	modelový	k2eAgFnSc2d1	modelová
řady	řada	k1gFnSc2	řada
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
řady	řada	k1gFnSc2	řada
byly	být	k5eAaImAgInP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
následující	následující	k2eAgInPc1d1	následující
modely	model	k1gInPc1	model
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
*	*	kIx~	*
Vozy	vůz	k1gInPc1	vůz
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
BMW	BMW	kA	BMW
Motorsport	Motorsport	k1gInSc1	Motorsport
GmbH	GmbH	k1gFnSc1	GmbH
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
BMW	BMW	kA	BMW
M	M	kA	M
GmbH	GmbH	k1gMnSc1	GmbH
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
Republice	republika	k1gFnSc6	republika
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
závodě	závod	k1gInSc6	závod
BMW	BMW	kA	BMW
v	v	k7c6	v
Pretorii	Pretorie	k1gFnSc6	Pretorie
montovány	montován	k2eAgFnPc1d1	montována
verze	verze	k1gFnPc1	verze
520	[number]	k4	520
<g/>
i	i	k9	i
<g/>
,	,	kIx,	,
528	[number]	k4	528
<g/>
i	i	k8xC	i
and	and	k?	and
530	[number]	k4	530
<g/>
i.	i.	k?	i.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
-	-	kIx~	-
E28	E28	k1gFnSc1	E28
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
páté	pátý	k4xOgFnSc2	pátý
řady	řada	k1gFnSc2	řada
přímo	přímo	k6eAd1	přímo
navazovala	navazovat	k5eAaImAgFnS	navazovat
na	na	k7c4	na
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
sérii	série	k1gFnSc4	série
modelů	model	k1gInPc2	model
E12	E12	k1gFnSc2	E12
a	a	k8xC	a
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
-	-	kIx~	-
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
dostaly	dostat	k5eAaPmAgInP	dostat
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
modely	model	k1gInPc4	model
s	s	k7c7	s
dieselovým	dieselový	k2eAgInSc7d1	dieselový
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
E28	E28	k4	E28
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
první	první	k4xOgFnSc7	první
sérií	série	k1gFnSc7	série
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
navržený	navržený	k2eAgInSc4d1	navržený
středový	středový	k2eAgInSc4d1	středový
tunel	tunel	k1gInSc4	tunel
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
řidiče	řidič	k1gMnPc4	řidič
a	a	k8xC	a
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
systému	systém	k1gInSc2	systém
ABS	ABS	kA	ABS
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
E28	E28	k1gFnPc2	E28
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
sérií	série	k1gFnPc2	série
E	E	kA	E
<g/>
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
produkce	produkce	k1gFnSc2	produkce
série	série	k1gFnSc2	série
E28	E28	k1gFnSc2	E28
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
v	v	k7c6	v
k	k	k7c3	k
prvnímu	první	k4xOgNnSc3	první
představení	představení	k1gNnPc2	představení
BMW	BMW	kA	BMW
M	M	kA	M
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Třetí	třetí	k4xOgFnSc1	třetí
generace	generace	k1gFnSc1	generace
-	-	kIx~	-
E34	E34	k1gFnSc1	E34
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Sériová	sériový	k2eAgFnSc1d1	sériová
výroba	výroba	k1gFnSc1	výroba
modelu	model	k1gInSc2	model
E34	E34	k1gFnSc1	E34
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1988	[number]	k4	1988
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
až	až	k6eAd1	až
do	do	k7c2	do
června	červen	k1gInSc2	červen
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgInS	nahradit
model	model	k1gInSc1	model
E	E	kA	E
<g/>
39	[number]	k4	39
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
generace	generace	k1gFnSc1	generace
-	-	kIx~	-
E39	E39	k1gFnSc1	E39
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Prodej	prodej	k1gInSc1	prodej
modelu	model	k1gInSc2	model
E39	E39	k1gFnSc2	E39
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
na	na	k7c6	na
trzích	trh	k1gInPc6	trh
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ostatní	ostatní	k2eAgInPc4d1	ostatní
trhy	trh	k1gInPc4	trh
dorazil	dorazit	k5eAaPmAgMnS	dorazit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
série	série	k1gFnSc1	série
E39	E39	k1gFnSc2	E39
vizuálně	vizuálně	k6eAd1	vizuálně
a	a	k8xC	a
technicky	technicky	k6eAd1	technicky
revidována	revidovat	k5eAaImNgFnS	revidovat
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vůz	vůz	k1gInSc1	vůz
dostal	dostat	k5eAaPmAgInS	dostat
populární	populární	k2eAgNnPc4d1	populární
přední	přední	k2eAgNnPc4d1	přední
světla	světlo	k1gNnPc4	světlo
"	"	kIx"	"
<g/>
Angel	Angela	k1gFnPc2	Angela
eyes	eyes	k1gInSc1	eyes
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pozměněná	pozměněný	k2eAgNnPc4d1	pozměněné
zadní	zadní	k2eAgNnPc4d1	zadní
světla	světlo	k1gNnPc4	světlo
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc4d1	nový
nárazníky	nárazník	k1gInPc4	nárazník
a	a	k8xC	a
mlhovky	mlhovka	k1gFnPc4	mlhovka
<g/>
.	.	kIx.	.
</s>
<s>
Modernizována	modernizován	k2eAgFnSc1d1	modernizována
byla	být	k5eAaImAgFnS	být
i	i	k9	i
paleta	paleta	k1gFnSc1	paleta
motorů	motor	k1gInPc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
vůz	vůz	k1gInSc1	vůz
např.	např.	kA	např.
dostal	dostat	k5eAaPmAgInS	dostat
nový	nový	k2eAgInSc1d1	nový
navigační	navigační	k2eAgInSc1d1	navigační
systém	systém	k1gInSc1	systém
s	s	k7c7	s
moderním	moderní	k2eAgInSc7d1	moderní
displejem	displej	k1gInSc7	displej
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Motory	motor	k1gInPc1	motor
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
520	[number]	k4	520
<g/>
i	i	k8xC	i
-	-	kIx~	-
110	[number]	k4	110
<g/>
/	/	kIx~	/
<g/>
125	[number]	k4	125
kW	kW	kA	kW
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
523	[number]	k4	523
<g/>
i	i	k8xC	i
-	-	kIx~	-
125	[number]	k4	125
kW	kW	kA	kW
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
525	[number]	k4	525
<g/>
i	i	k8xC	i
-	-	kIx~	-
141	[number]	k4	141
kW	kW	kA	kW
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
528	[number]	k4	528
<g/>
i	i	k8xC	i
-	-	kIx~	-
142	[number]	k4	142
kW	kW	kA	kW
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
530	[number]	k4	530
<g/>
i	i	k8xC	i
-	-	kIx~	-
170	[number]	k4	170
kW	kW	kA	kW
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
535	[number]	k4	535
<g/>
i	i	k8xC	i
-	-	kIx~	-
180	[number]	k4	180
kW	kW	kA	kW
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
540	[number]	k4	540
<g/>
i	i	k8xC	i
-	-	kIx~	-
210	[number]	k4	210
kW	kW	kA	kW
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
M5	M5	k4	M5
-	-	kIx~	-
294	[number]	k4	294
kW	kW	kA	kW
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
525	[number]	k4	525
<g/>
td	td	k?	td
-	-	kIx~	-
85	[number]	k4	85
kW	kW	kA	kW
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
E34	E34	k1gFnSc2	E34
</s>
</p>
<p>
<s>
525	[number]	k4	525
<g/>
tds	tds	k?	tds
-	-	kIx~	-
105	[number]	k4	105
kW	kW	kA	kW
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
525	[number]	k4	525
<g/>
td	td	k?	td
s	s	k7c7	s
mezichladičem	mezichladič	k1gInSc7	mezichladič
plnícího	plnící	k2eAgInSc2d1	plnící
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
úpravami	úprava	k1gFnPc7	úprava
na	na	k7c4	na
turbu	turba	k1gFnSc4	turba
<g/>
;	;	kIx,	;
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
E34	E34	k1gFnSc2	E34
</s>
</p>
<p>
<s>
520	[number]	k4	520
<g/>
d	d	k?	d
-	-	kIx~	-
105	[number]	k4	105
kW	kW	kA	kW
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
525	[number]	k4	525
<g/>
d	d	k?	d
-	-	kIx~	-
120	[number]	k4	120
kW	kW	kA	kW
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
hliníkový	hliníkový	k2eAgInSc1d1	hliníkový
motor	motor	k1gInSc1	motor
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
Common-rail	Commonaila	k1gFnPc2	Common-raila
</s>
</p>
<p>
<s>
530	[number]	k4	530
<g/>
d	d	k?	d
-	-	kIx~	-
135	[number]	k4	135
<g/>
/	/	kIx~	/
<g/>
142	[number]	k4	142
kW	kW	kA	kW
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
hliníkový	hliníkový	k2eAgInSc1d1	hliníkový
motor	motor	k1gInSc1	motor
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
Common-rail	Commonaila	k1gFnPc2	Common-raila
</s>
</p>
<p>
<s>
====	====	k?	====
Přehled	přehled	k1gInSc4	přehled
motorů	motor	k1gInPc2	motor
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
====	====	k?	====
Přehled	přehled	k1gInSc4	přehled
motorů	motor	k1gInPc2	motor
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
===	===	k?	===
Produkce	produkce	k1gFnSc1	produkce
===	===	k?	===
</s>
</p>
<p>
<s>
Limousine	Limousinout	k5eAaPmIp3nS	Limousinout
<g/>
:	:	kIx,	:
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Touring	Touring	k1gInSc1	Touring
<g/>
:	:	kIx,	:
1997	[number]	k4	1997
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
==	==	k?	==
Pátá	pátý	k4xOgFnSc1	pátý
generace	generace	k1gFnSc1	generace
-	-	kIx~	-
E	E	kA	E
<g/>
60	[number]	k4	60
<g/>
/	/	kIx~	/
<g/>
E	E	kA	E
<g/>
61	[number]	k4	61
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
BMW	BMW	kA	BMW
E60	E60	k1gMnPc2	E60
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kombi	kombi	k1gNnSc1	kombi
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Model	model	k1gInSc1	model
E60	E60	k1gFnSc2	E60
dostal	dostat	k5eAaPmAgInS	dostat
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
sedmičkovou	sedmičkův	k2eAgFnSc7d1	sedmičkův
řadou	řada	k1gFnSc7	řada
<g/>
.	.	kIx.	.
</s>
<s>
Sklidil	sklidit	k5eAaPmAgMnS	sklidit
ovšem	ovšem	k9	ovšem
vlnu	vlna	k1gFnSc4	vlna
kritiky	kritika	k1gFnSc2	kritika
za	za	k7c4	za
nevyváženost	nevyváženost	k1gFnSc4	nevyváženost
přední	přední	k2eAgFnSc2d1	přední
a	a	k8xC	a
zadní	zadní	k2eAgFnSc2d1	zadní
partie	partie	k1gFnSc2	partie
(	(	kIx(	(
<g/>
u	u	k7c2	u
sedanu	sedan	k1gInSc2	sedan
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoha	mnoho	k4c3	mnoho
motoristům	motorista	k1gMnPc3	motorista
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
líbí	líbit	k5eAaImIp3nP	líbit
předchozí	předchozí	k2eAgFnPc4d1	předchozí
generace	generace	k1gFnPc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
automobil	automobil	k1gInSc1	automobil
byl	být	k5eAaImAgInS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
i	i	k8xC	i
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
prošla	projít	k5eAaPmAgFnS	projít
řada	řada	k1gFnSc1	řada
E60	E60	k1gFnSc2	E60
decentním	decentní	k2eAgInSc7d1	decentní
faceliftem	facelift	k1gInSc7	facelift
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šestá	šestý	k4xOgFnSc1	šestý
generace	generace	k1gFnSc1	generace
-	-	kIx~	-
F	F	kA	F
<g/>
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
F	F	kA	F
<g/>
11	[number]	k4	11
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c7	před
představením	představení	k1gNnSc7	představení
nové	nový	k2eAgFnSc2d1	nová
řady	řada	k1gFnSc2	řada
5	[number]	k4	5
<g/>
,	,	kIx,	,
BMW	BMW	kA	BMW
ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
uvedlo	uvést	k5eAaPmAgNnS	uvést
verzi	verze	k1gFnSc6	verze
GranTurismo	GranTurisma	k1gFnSc5	GranTurisma
<g/>
,	,	kIx,	,
označovanou	označovaný	k2eAgFnSc4d1	označovaná
jako	jako	k8xC	jako
BMW	BMW	kA	BMW
řady	řada	k1gFnSc2	řada
5	[number]	k4	5
GT	GT	kA	GT
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sedmá	sedmý	k4xOgFnSc1	sedmý
generace	generace	k1gFnSc1	generace
-	-	kIx~	-
G	G	kA	G
<g/>
30	[number]	k4	30
<g/>
/	/	kIx~	/
<g/>
G	G	kA	G
<g/>
31	[number]	k4	31
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
BMW	BMW	kA	BMW
</s>
</p>
<p>
<s>
BMW	BMW	kA	BMW
řady	řada	k1gFnSc2	řada
3	[number]	k4	3
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
BMW	BMW	kA	BMW
řady	řada	k1gFnSc2	řada
5	[number]	k4	5
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInPc1d1	oficiální
kompletní	kompletní	k2eAgInPc1d1	kompletní
katalogy	katalog	k1gInPc1	katalog
dílů	díl	k1gInPc2	díl
ke	k	k7c3	k
všem	všecek	k3xTgMnPc3	všecek
vyráběným	vyráběný	k2eAgInSc7d1	vyráběný
BMW	BMW	kA	BMW
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
rozkreslených	rozkreslený	k2eAgFnPc2d1	rozkreslená
sestav	sestava	k1gFnPc2	sestava
<g/>
,	,	kIx,	,
part	parta	k1gFnPc2	parta
numberů	number	k1gInPc2	number
<g/>
,	,	kIx,	,
orientační	orientační	k2eAgFnPc1d1	orientační
ceny	cena	k1gFnPc1	cena
<g/>
,	,	kIx,	,
hmotnosti	hmotnost	k1gFnPc1	hmotnost
dílů	díl	k1gInPc2	díl
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
