<s>
William	William	k6eAd1	William
Butler	Butler	k1gInSc1	Butler
Yeats	Yeatsa	k1gFnPc2	Yeatsa
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
Sandymount	Sandymount	k1gInSc1	Sandymount
u	u	k7c2	u
Dublinu	Dublin	k1gInSc2	Dublin
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Roquebrune-Cap-Martin	Roquebrune-Cap-Martin	k1gInSc1	Roquebrune-Cap-Martin
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglicky	anglicky	k6eAd1	anglicky
píšící	píšící	k2eAgMnSc1d1	píšící
irský	irský	k2eAgMnSc1d1	irský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
esejista	esejista	k1gMnSc1	esejista
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Yeats	Yeats	k6eAd1	Yeats
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
právníka	právník	k1gMnSc2	právník
a	a	k8xC	a
významného	významný	k2eAgMnSc2d1	významný
malíře	malíř	k1gMnSc2	malíř
portrétů	portrét	k1gInPc2	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
žila	žít	k5eAaImAgFnS	žít
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Yeats	Yeats	k1gInSc1	Yeats
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
rodnou	rodný	k2eAgFnSc7d1	rodná
zemí	zem	k1gFnSc7	zem
neztratil	ztratit	k5eNaPmAgMnS	ztratit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
trávil	trávit	k5eAaImAgMnS	trávit
některé	některý	k3yIgFnPc4	některý
prázdniny	prázdniny	k1gFnPc4	prázdniny
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
prarodičů	prarodič	k1gMnPc2	prarodič
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Sligo	Sligo	k6eAd1	Sligo
v	v	k7c6	v
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
rodiny	rodina	k1gFnSc2	rodina
do	do	k7c2	do
Irska	Irsko	k1gNnSc2	Irsko
studoval	studovat	k5eAaImAgMnS	studovat
Yeats	Yeats	k1gInSc4	Yeats
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1883	[number]	k4	1883
až	až	k9	až
1886	[number]	k4	1886
výtvarnou	výtvarný	k2eAgFnSc4d1	výtvarná
akademii	akademie	k1gFnSc4	akademie
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
četnými	četný	k2eAgMnPc7d1	četný
umělci	umělec	k1gMnPc7	umělec
a	a	k8xC	a
literáty	literát	k1gMnPc7	literát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
pak	pak	k6eAd1	pak
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
uveřejňovat	uveřejňovat	k5eAaImF	uveřejňovat
své	svůj	k3xOyFgFnPc1	svůj
první	první	k4xOgFnPc1	první
básně	báseň	k1gFnPc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
natrvalo	natrvalo	k6eAd1	natrvalo
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Yeats	Yeats	k6eAd1	Yeats
prožil	prožít	k5eAaPmAgMnS	prožít
mimo	mimo	k7c4	mimo
Irsko	Irsko	k1gNnSc4	Irsko
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc1	třetina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
zapůsobila	zapůsobit	k5eAaPmAgFnS	zapůsobit
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
památkami	památka	k1gFnPc7	památka
renesanční	renesanční	k2eAgFnSc2d1	renesanční
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
projevoval	projevovat	k5eAaImAgInS	projevovat
značný	značný	k2eAgInSc1d1	značný
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
esoterická	esoterický	k2eAgNnPc4d1	esoterické
učení	učení	k1gNnPc4	učení
<g/>
,	,	kIx,	,
astrologii	astrologie	k1gFnSc4	astrologie
<g/>
,	,	kIx,	,
alchymii	alchymie	k1gFnSc4	alchymie
<g/>
,	,	kIx,	,
okultismus	okultismus	k1gInSc4	okultismus
a	a	k8xC	a
spiritismus	spiritismus	k1gInSc4	spiritismus
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
několika	několik	k4yIc2	několik
hermetických	hermetický	k2eAgFnPc2d1	hermetická
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
romantické	romantický	k2eAgFnSc2d1	romantická
imaginace	imaginace	k1gFnSc2	imaginace
a	a	k8xC	a
vizionářské	vizionářský	k2eAgFnSc2d1	vizionářská
poezie	poezie	k1gFnSc2	poezie
Willama	Willamum	k1gNnSc2	Willamum
Blakea	Blakeum	k1gNnSc2	Blakeum
a	a	k8xC	a
horlivým	horlivý	k2eAgMnSc7d1	horlivý
čtenářem	čtenář	k1gMnSc7	čtenář
soudobých	soudobý	k2eAgInPc2d1	soudobý
anglických	anglický	k2eAgInPc2d1	anglický
i	i	k8xC	i
francouzských	francouzský	k2eAgMnPc2d1	francouzský
symbolistických	symbolistický	k2eAgMnPc2d1	symbolistický
a	a	k8xC	a
dekadentních	dekadentní	k2eAgMnPc2d1	dekadentní
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
mnohé	mnohé	k1gNnSc4	mnohé
znal	znát	k5eAaImAgInS	znát
osobně	osobně	k6eAd1	osobně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Paula	Paul	k1gMnSc2	Paul
Verlaina	Verlain	k1gMnSc2	Verlain
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
také	také	k9	také
zapůsobila	zapůsobit	k5eAaPmAgFnS	zapůsobit
filozofie	filozofie	k1gFnSc1	filozofie
Friedricha	Friedrich	k1gMnSc2	Friedrich
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
a	a	k8xC	a
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
básníkem	básník	k1gMnSc7	básník
Ezrou	Ezra	k1gMnSc7	Ezra
Poundem	pound	k1gInSc7	pound
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
japonským	japonský	k2eAgNnSc7d1	Japonské
divadlem	divadlo	k1gNnSc7	divadlo
nó	nó	k0	nó
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1890	[number]	k4	1890
-	-	kIx~	-
1920	[number]	k4	1920
byl	být	k5eAaImAgInS	být
Yeats	Yeats	k1gInSc1	Yeats
vůdcem	vůdce	k1gMnSc7	vůdce
irského	irský	k2eAgNnSc2d1	irské
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
spoluzakladatelem	spoluzakladatel	k1gMnSc7	spoluzakladatel
Irské	irský	k2eAgFnSc2d1	irská
literární	literární	k2eAgFnSc2d1	literární
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
Irish	Irish	k1gInSc1	Irish
Literary	Literara	k1gFnSc2	Literara
Society	societa	k1gFnSc2	societa
<g/>
)	)	kIx)	)
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
Národní	národní	k2eAgFnSc3d1	národní
literární	literární	k2eAgFnSc3d1	literární
společnosti	společnost	k1gFnSc3	společnost
(	(	kIx(	(
<g/>
National	National	k1gFnSc1	National
Literary	Literara	k1gFnSc2	Literara
Soicety	Soiceta	k1gFnSc2	Soiceta
<g/>
)	)	kIx)	)
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
na	na	k7c4	na
obrození	obrození	k1gNnSc4	obrození
irského	irský	k2eAgNnSc2d1	irské
divadelnictví	divadelnictví	k1gNnSc2	divadelnictví
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
snahy	snaha	k1gFnPc1	snaha
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
roku	rok	k1gInSc3	rok
1904	[number]	k4	1904
založením	založení	k1gNnSc7	založení
irského	irský	k2eAgNnSc2d1	irské
národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Abbey	Abbea	k1gFnSc2	Abbea
Theatre	Theatr	k1gInSc5	Theatr
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
byl	být	k5eAaImAgInS	být
spoluředitelem	spoluředitel	k1gMnSc7	spoluředitel
a	a	k8xC	a
které	který	k3yQgNnSc1	který
uvádělo	uvádět	k5eAaImAgNnS	uvádět
jeho	jeho	k3xOp3gFnPc4	jeho
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Irský	irský	k2eAgInSc1d1	irský
boj	boj	k1gInSc1	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
prožil	prožít	k5eAaPmAgMnS	prožít
Yeats	Yeats	k1gInSc4	Yeats
mimo	mimo	k7c4	mimo
svou	svůj	k3xOyFgFnSc4	svůj
vlast	vlast	k1gFnSc4	vlast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1923	[number]	k4	1923
až	až	k9	až
1928	[number]	k4	1928
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
samostatném	samostatný	k2eAgInSc6d1	samostatný
irském	irský	k2eAgInSc6d1	irský
státě	stát	k1gInSc6	stát
jako	jako	k9	jako
senátor	senátor	k1gMnSc1	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Yeatsovy	Yeatsův	k2eAgInPc1d1	Yeatsův
verše	verš	k1gInPc1	verš
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc1d1	mnohá
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
na	na	k7c4	na
tradiční	tradiční	k2eAgFnSc4d1	tradiční
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mystické	mystický	k2eAgFnSc3d1	mystická
symbolice	symbolika	k1gFnSc3	symbolika
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
irském	irský	k2eAgInSc6d1	irský
folklóru	folklór	k1gInSc6	folklór
a	a	k8xC	a
ústní	ústní	k2eAgFnSc4d1	ústní
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
jeho	jeho	k3xOp3gNnPc1	jeho
dramata	drama	k1gNnPc1	drama
jsou	být	k5eAaImIp3nP	být
ovlivněna	ovlivněn	k2eAgNnPc1d1	ovlivněno
tradicí	tradice	k1gFnSc7	tradice
antického	antický	k2eAgMnSc2d1	antický
a	a	k8xC	a
středověkého	středověký	k2eAgNnSc2d1	středověké
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
také	také	k9	také
japonského	japonský	k2eAgNnSc2d1	Japonské
divadla	divadlo	k1gNnSc2	divadlo
Nó	nó	k0	nó
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
uvádění	uvádění	k1gNnPc1	uvádění
na	na	k7c6	na
scéně	scéna	k1gFnSc6	scéna
dublinského	dublinský	k2eAgNnSc2d1	Dublinské
národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
mělo	mít	k5eAaImAgNnS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
irského	irský	k2eAgNnSc2d1	irské
národního	národní	k2eAgNnSc2d1	národní
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
,	,	kIx,	,
Yeats	Yeats	k1gInSc1	Yeats
experimentoval	experimentovat	k5eAaImAgInS	experimentovat
s	s	k7c7	s
maskami	maska	k1gFnPc7	maska
<g/>
,	,	kIx,	,
tancem	tanec	k1gInSc7	tanec
a	a	k8xC	a
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
dílem	dílo	k1gNnSc7	dílo
se	se	k3xPyFc4	se
Yeats	Yeats	k1gInSc1	Yeats
stal	stát	k5eAaPmAgInS	stát
předním	přední	k2eAgMnSc7d1	přední
reprezentantem	reprezentant	k1gMnSc7	reprezentant
širokého	široký	k2eAgNnSc2d1	široké
<g/>
,	,	kIx,	,
národně	národně	k6eAd1	národně
zaměřeného	zaměřený	k2eAgNnSc2d1	zaměřené
hnutí	hnutí	k1gNnSc2	hnutí
tzv.	tzv.	kA	tzv.
keltské	keltský	k2eAgFnSc2d1	keltská
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
zařadil	zařadit	k5eAaPmAgMnS	zařadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
mistry	mistr	k1gMnPc7	mistr
anglické	anglický	k2eAgFnSc2d1	anglická
poezie	poezie	k1gFnSc2	poezie
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
T.	T.	kA	T.
S.	S.	kA	S.
Eliotem	Eliot	k1gInSc7	Eliot
nejvíce	nejvíce	k6eAd1	nejvíce
zapůsobil	zapůsobit	k5eAaPmAgInS	zapůsobit
na	na	k7c4	na
následující	následující	k2eAgFnSc4d1	následující
básnickou	básnický	k2eAgFnSc4d1	básnická
generaci	generace	k1gFnSc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
za	za	k7c4	za
jeho	jeho	k3xOp3gNnSc4	jeho
trvale	trvale	k6eAd1	trvale
inspirované	inspirovaný	k2eAgNnSc4d1	inspirované
básnické	básnický	k2eAgNnSc4d1	básnické
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
nejpřísnější	přísný	k2eAgFnSc7d3	nejpřísnější
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
podobou	podoba	k1gFnSc7	podoba
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
ducha	duch	k1gMnSc4	duch
celého	celý	k2eAgInSc2d1	celý
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Yeats	Yeats	k1gInSc1	Yeats
však	však	k9	však
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
menšímu	malý	k2eAgInSc3d2	menší
počtu	počet	k1gInSc3	počet
laureátů	laureát	k1gMnPc2	laureát
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
ještě	ještě	k9	ještě
významně	významně	k6eAd1	významně
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
a	a	k8xC	a
obohatili	obohatit	k5eAaPmAgMnP	obohatit
svou	svůj	k3xOyFgFnSc4	svůj
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
jeho	jeho	k3xOp3gNnSc7	jeho
závěrečné	závěrečný	k2eAgInPc1d1	závěrečný
a	a	k8xC	a
také	také	k9	také
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
tvůrčí	tvůrčí	k2eAgNnSc4d1	tvůrčí
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
Yeats	Yeats	k1gInSc1	Yeats
snažil	snažit	k5eAaImAgInS	snažit
v	v	k7c6	v
systému	systém	k1gInSc6	systém
symbolů	symbol	k1gInPc2	symbol
zachytit	zachytit	k5eAaPmF	zachytit
tajnou	tajný	k2eAgFnSc4d1	tajná
podstatu	podstata	k1gFnSc4	podstata
chodu	chod	k1gInSc2	chod
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
posmrtného	posmrtný	k2eAgInSc2d1	posmrtný
života	život	k1gInSc2	život
a	a	k8xC	a
organicky	organicky	k6eAd1	organicky
spojil	spojit	k5eAaPmAgInS	spojit
symbolismus	symbolismus	k1gInSc1	symbolismus
<g/>
,	,	kIx,	,
tradice	tradice	k1gFnSc1	tradice
vizionářské	vizionářský	k2eAgFnSc2d1	vizionářská
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
starobylou	starobylý	k2eAgFnSc7d1	starobylá
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
keltskou	keltský	k2eAgFnSc4d1	keltská
mytologii	mytologie	k1gFnSc4	mytologie
a	a	k8xC	a
dobový	dobový	k2eAgInSc1d1	dobový
mysticismus	mysticismus	k1gInSc1	mysticismus
se	se	k3xPyFc4	se
svěžím	svěžit	k5eAaImIp1nS	svěžit
vnímáním	vnímání	k1gNnSc7	vnímání
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
přesvědčivého	přesvědčivý	k2eAgInSc2d1	přesvědčivý
projasněného	projasněný	k2eAgInSc2d1	projasněný
tvaru	tvar	k1gInSc2	tvar
bez	bez	k7c2	bez
zbytečných	zbytečný	k2eAgFnPc2d1	zbytečná
ozdob	ozdoba	k1gFnPc2	ozdoba
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
si	se	k3xPyFc3	se
podržel	podržet	k5eAaPmAgInS	podržet
svou	svůj	k3xOyFgFnSc4	svůj
sugestivnost	sugestivnost	k1gFnSc4	sugestivnost
a	a	k8xC	a
hloubku	hloubka	k1gFnSc4	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Butler	Butler	k1gInSc1	Butler
Yeats	Yeats	k1gInSc1	Yeats
zemřel	zemřít	k5eAaPmAgInS	zemřít
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Wanderings	Wanderings	k1gInSc1	Wanderings
of	of	k?	of
Oisin	Oisin	k1gInSc1	Oisin
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
Oisinovo	Oisinův	k2eAgNnSc1d1	Oisinův
bloudění	bloudění	k1gNnSc1	bloudění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Básně	báseň	k1gFnPc1	báseň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Wind	Wind	k1gMnSc1	Wind
Among	Among	k1gMnSc1	Among
The	The	k1gMnSc1	The
Reeds	Reedsa	k1gFnPc2	Reedsa
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
Vítr	vítr	k1gInSc1	vítr
v	v	k7c6	v
rákosí	rákosí	k1gNnSc6	rákosí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
In	In	k1gMnSc1	In
the	the	k?	the
Seven	Seven	k1gInSc1	Seven
Woods	Woods	k1gInSc1	Woods
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
V	V	kA	V
Sedmilesí	Sedmilese	k1gFnSc7	Sedmilese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Green	Grena	k1gFnPc2	Grena
Helmet	Helmet	k1gMnSc1	Helmet
and	and	k?	and
Other	Other	k1gInSc1	Other
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Zelená	zelený	k2eAgFnSc1d1	zelená
přilbice	přilbice	k1gFnSc1	přilbice
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
básně	báseň	k1gFnPc1	báseň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Responsibilites	Responsibilites	k1gMnSc1	Responsibilites
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
Zodpovědnost	zodpovědnost	k1gFnSc1	zodpovědnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Wild	Wild	k1gMnSc1	Wild
Swans	Swans	k1gInSc1	Swans
at	at	k?	at
Coole	Coole	k1gInSc1	Coole
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Divoké	divoký	k2eAgFnSc6d1	divoká
<g />
.	.	kIx.	.
</s>
<s>
labutě	labuť	k1gFnPc1	labuť
v	v	k7c6	v
Coole	Cool	k1gInSc6	Cool
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Robartes	Robartes	k1gMnSc1	Robartes
and	and	k?	and
the	the	k?	the
Dancer	Dancer	k1gInSc1	Dancer
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
Robartes	Robartes	k1gMnSc1	Robartes
a	a	k8xC	a
tanečnice	tanečnice	k1gFnPc1	tanečnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Tower	Tower	k1gMnSc1	Tower
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Věž	věž	k1gFnSc1	věž
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gFnSc1	The
Winding	Winding	k1gInSc1	Winding
Stair	Stair	k1gInSc1	Stair
and	and	k?	and
Other	Other	k1gInSc1	Other
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Točité	točitý	k2eAgNnSc1d1	točité
schodiště	schodiště	k1gNnSc1	schodiště
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
básně	báseň	k1gFnPc1	báseň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
autorovy	autorův	k2eAgFnSc2d1	autorova
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgNnSc1d1	postavené
na	na	k7c4	na
tradiční	tradiční	k2eAgFnSc4d1	tradiční
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mystické	mystický	k2eAgFnSc3d1	mystická
symbolice	symbolika	k1gFnSc3	symbolika
(	(	kIx(	(
<g/>
labuť	labuť	k1gFnSc1	labuť
<g/>
,	,	kIx,	,
jednorožec	jednorožec	k1gMnSc1	jednorožec
<g/>
,	,	kIx,	,
jezdec	jezdec	k1gMnSc1	jezdec
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
napětí	napětí	k1gNnSc6	napětí
mezi	mezi	k7c7	mezi
okultismem	okultismus	k1gInSc7	okultismus
a	a	k8xC	a
vlastenectvím	vlastenectví	k1gNnSc7	vlastenectví
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k6eAd1	William
Butler	Butler	k1gInSc1	Butler
Yeats	Yeatsa	k1gFnPc2	Yeatsa
napsal	napsat	k5eAaBmAgInS	napsat
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
třicet	třicet	k4xCc1	třicet
dva	dva	k4xCgInPc4	dva
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejznámější	známý	k2eAgMnPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Mosada	Mosada	k1gFnSc1	Mosada
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vešované	vešovaný	k2eAgNnSc1d1	vešovaný
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
Yeatsova	Yeatsův	k2eAgFnSc1d1	Yeatsova
první	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Countess	Countessa	k1gFnPc2	Countessa
Kathleen	Kathlena	k1gFnPc2	Kathlena
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Hraběnka	hraběnka	k1gFnSc1	hraběnka
Kathleen	Kathlena	k1gFnPc2	Kathlena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veršované	veršovaný	k2eAgNnSc4d1	veršované
drama	drama	k1gNnSc4	drama
založené	založený	k2eAgNnSc4d1	založené
na	na	k7c4	na
staré	starý	k2eAgFnPc4d1	stará
keltské	keltský	k2eAgFnPc4d1	keltská
pověsti	pověst	k1gFnPc4	pověst
o	o	k7c6	o
vládkyni	vládkyně	k1gFnSc6	vládkyně
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zachránila	zachránit	k5eAaPmAgFnS	zachránit
svůj	svůj	k3xOyFgInSc4	svůj
lid	lid	k1gInSc4	lid
před	před	k7c7	před
hladomorem	hladomor	k1gInSc7	hladomor
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
upsala	upsat	k5eAaPmAgFnS	upsat
svou	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc4	duše
ďáblu	ďábel	k1gMnSc3	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Land	Land	k1gInSc1	Land
of	of	k?	of
Heart	Heart	k1gInSc1	Heart
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Desire	Desir	k1gInSc5	Desir
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
touhy	touha	k1gFnSc2	touha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veršované	veršovaný	k2eAgNnSc1d1	veršované
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Shady	Shada	k1gFnSc2	Shada
Waters	Watersa	k1gFnPc2	Watersa
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Temné	temný	k2eAgFnSc2d1	temná
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cathleen	Cathleen	k1gInSc1	Cathleen
ni	on	k3xPp3gFnSc4	on
Houlihan	Houlihan	k1gMnSc1	Houlihan
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slavná	slavný	k2eAgFnSc1d1	slavná
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
legendy	legenda	k1gFnSc2	legenda
o	o	k7c6	o
stařeně	stařena	k1gFnSc6	stařena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
symbol	symbol	k1gInSc1	symbol
národa	národ	k1gInSc2	národ
vede	vést	k5eAaImIp3nS	vést
lid	lid	k1gInSc1	lid
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
cizím	cizí	k2eAgMnPc3d1	cizí
vetřelcům	vetřelec	k1gMnPc3	vetřelec
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Hour-Glass	Hour-Glass	k1gInSc1	Hour-Glass
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Přesýpací	přesýpací	k2eAgFnPc4d1	přesýpací
hodiny	hodina	k1gFnPc4	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
On	on	k3xPp3gMnSc1	on
Baile	Baile	k1gInSc4	Baile
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Strand	strand	k1gInSc1	strand
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Bailově	Bailův	k2eAgInSc6d1	Bailův
břehu	břeh	k1gInSc6	břeh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veršované	veršovaný	k2eAgNnSc1d1	veršované
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
Deirdre	Deirdr	k1gInSc5	Deirdr
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
veršované	veršovaný	k2eAgNnSc1d1	veršované
drama	drama	k1gNnSc1	drama
založené	založený	k2eAgFnSc2d1	založená
opět	opět	k6eAd1	opět
na	na	k7c6	na
staré	stará	k1gFnSc6	stará
keltské	keltský	k2eAgFnSc2d1	keltská
pověsti	pověst	k1gFnSc2	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Four	Four	k1gInSc1	Four
Plays	Playsa	k1gFnPc2	Playsa
for	forum	k1gNnPc2	forum
Dances	Dances	k1gInSc1	Dances
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Čtyři	čtyři	k4xCgFnPc1	čtyři
hry	hra	k1gFnPc1	hra
pro	pro	k7c4	pro
tanečníky	tanečník	k1gMnPc4	tanečník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
japonským	japonský	k2eAgMnPc3d1	japonský
divadlem	divadlo	k1gNnSc7	divadlo
nó	nó	k0	nó
<g/>
.	.	kIx.	.
</s>
<s>
Fairy	Fair	k1gInPc1	Fair
and	and	k?	and
Folk	folk	k1gInSc1	folk
Tales	Tales	k1gInSc1	Tales
of	of	k?	of
the	the	k?	the
Irish	Irish	k1gInSc1	Irish
Peasantry	Peasantr	k1gInPc1	Peasantr
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Pohádky	pohádka	k1gFnPc1	pohádka
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
irských	irský	k2eAgMnPc2d1	irský
rolníků	rolník	k1gMnPc2	rolník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Celtic	Celtice	k1gFnPc2	Celtice
Twilight	Twilight	k1gMnSc1	Twilight
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Keltské	keltský	k2eAgNnSc1d1	keltské
přítmí	přítmí	k1gNnSc1	přítmí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
próz	próza	k1gFnPc2	próza
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
Yeats	Yeats	k1gInSc1	Yeats
podal	podat	k5eAaPmAgMnS	podat
jemným	jemný	k2eAgMnSc7d1	jemný
<g/>
,	,	kIx,	,
bohatým	bohatý	k2eAgMnSc7d1	bohatý
a	a	k8xC	a
sugestivním	sugestivní	k2eAgInSc7d1	sugestivní
jazykem	jazyk	k1gInSc7	jazyk
obraz	obraz	k1gInSc1	obraz
bájné	bájný	k2eAgFnSc2d1	bájná
říše	říš	k1gFnSc2	říš
s	s	k7c7	s
kouzly	kouzlo	k1gNnPc7	kouzlo
víl	víla	k1gFnPc2	víla
a	a	k8xC	a
skřítků	skřítek	k1gMnPc2	skřítek
<g/>
.	.	kIx.	.
</s>
<s>
Irish	Irish	k1gMnSc1	Irish
Fairy	Faira	k1gFnSc2	Faira
and	and	k?	and
Folk	folk	k1gInSc1	folk
Tales	Tales	k1gInSc1	Tales
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
Irské	irský	k2eAgFnSc2d1	irská
pohádky	pohádka	k1gFnSc2	pohádka
a	a	k8xC	a
pověsti	pověst	k1gFnSc2	pověst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnSc2	sbírka
povídek	povídka	k1gFnPc2	povídka
The	The	k1gMnSc1	The
Secret	Secret	k1gMnSc1	Secret
Rose	Rose	k1gMnSc1	Rose
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
růže	růže	k1gFnSc1	růže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rosa	Rosa	k1gMnSc1	Rosa
Alchemica	Alchemica	k1gMnSc1	Alchemica
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stories	Stories	k1gMnSc1	Stories
of	of	k?	of
Red	Red	k1gMnSc1	Red
Hanrahan	Hanrahan	k1gMnSc1	Hanrahan
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Příběhy	příběh	k1gInPc1	příběh
ryšavého	ryšavý	k2eAgMnSc2d1	ryšavý
Hanrahana	Hanrahan	k1gMnSc2	Hanrahan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Discoveries	Discoveries	k1gMnSc1	Discoveries
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Objevy	objev	k1gInPc1	objev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInSc2	esej
<g/>
,	,	kIx,	,
Per	pero	k1gNnPc2	pero
amica	amica	k6eAd1	amica
silentia	silentium	k1gNnSc2	silentium
lunae	lunae	k1gNnSc1	lunae
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
A	a	k9	a
Vison	Vison	k1gInSc1	Vison
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Vidění	vidění	k1gNnSc1	vidění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
symbolických	symbolický	k2eAgFnPc2d1	symbolická
próz	próza	k1gFnPc2	próza
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
jednotu	jednota	k1gFnSc4	jednota
historických	historický	k2eAgFnPc2d1	historická
epoch	epocha	k1gFnPc2	epocha
<g/>
,	,	kIx,	,
hlavních	hlavní	k2eAgNnPc2d1	hlavní
období	období	k1gNnPc2	období
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
a	a	k8xC	a
charakterových	charakterový	k2eAgInPc2d1	charakterový
typů	typ	k1gInPc2	typ
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
měsíčních	měsíční	k2eAgFnPc2d1	měsíční
fází	fáze	k1gFnPc2	fáze
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
sumou	suma	k1gFnSc7	suma
autorova	autorův	k2eAgNnSc2d1	autorovo
poznání	poznání	k1gNnSc2	poznání
života	život	k1gInSc2	život
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
Yeats	Yeatsa	k1gFnPc2	Yeatsa
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
pracoval	pracovat	k5eAaImAgMnS	pracovat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Autobiographies	Autobiographies	k1gInSc1	Autobiographies
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Autobiografie	autobiografie	k1gFnSc1	autobiografie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
růže	růže	k1gFnSc1	růže
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Říše	říše	k1gFnSc1	říše
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Antonín	Antonín	k1gMnSc1	Antonín
Ludvík	Ludvík	k1gMnSc1	Ludvík
Stříž	stříž	k1gFnSc1	stříž
<g/>
,	,	kIx,	,
Objevy	objev	k1gInPc1	objev
(	(	kIx(	(
<g/>
essaye	essaye	k1gFnSc1	essaye
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stanislava	Stanislava	k1gFnSc1	Stanislava
Jílovská	jílovský	k2eAgFnSc1d1	Jílovská
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skalický	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Jan	Jan	k1gMnSc1	Jan
V.	V.	kA	V.
Pojer	Pojer	k1gMnSc1	Pojer
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1947	[number]	k4	1947
a	a	k8xC	a
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Cathleen	Cathleen	k1gInSc1	Cathleen
ni	on	k3xPp3gFnSc4	on
houlihan	houlihan	k1gInSc1	houlihan
<g/>
,	,	kIx,	,
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
přilba	přilba	k1gFnSc1	přilba
<g/>
,	,	kIx,	,
Přesýpací	přesýpací	k2eAgFnPc1d1	přesýpací
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Říše	říše	k1gFnSc1	říše
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skalický	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
Rosa	Rosa	k1gMnSc1	Rosa
Alchemica	Alchemica	k1gMnSc1	Alchemica
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Říše	říše	k1gFnSc1	říše
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skalický	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
Ryšavého	Ryšavý	k1gMnSc2	Ryšavý
Hanrahana	Hanrahan	k1gMnSc2	Hanrahan
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Štorch-Marien	Štorch-Marien	k1gInSc1	Štorch-Marien
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skalický	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Dauphin	dauphin	k1gMnSc1	dauphin
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Přesýpací	přesýpací	k2eAgFnPc4d1	přesýpací
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgInPc4d1	lidový
závody	závod	k1gInPc4	závod
tiskařské	tiskařský	k2eAgInPc4d1	tiskařský
a	a	k8xC	a
nakladatelské	nakladatelský	k2eAgInPc4d1	nakladatelský
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Otto	Otto	k1gMnSc1	Otto
F.	F.	kA	F.
Babler	Babler	k1gMnSc1	Babler
<g/>
,	,	kIx,	,
Tři	tři	k4xCgFnPc1	tři
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skalický	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Temné	temný	k2eAgFnPc4d1	temná
vody	voda	k1gFnPc4	voda
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Bailově	Bailův	k2eAgInSc6d1	Bailův
břehu	břeh	k1gInSc6	břeh
a	a	k8xC	a
Deirdre	Deirdr	k1gInSc5	Deirdr
<g/>
.	.	kIx.	.
</s>
<s>
Per	pero	k1gNnPc2	pero
amica	amica	k6eAd1	amica
silentia	silentium	k1gNnSc2	silentium
lunae	lunae	k1gNnSc1	lunae
(	(	kIx(	(
<g/>
essaye	essaye	k1gNnSc1	essaye
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
V.	V.	kA	V.
Pojer	Pojer	k1gMnSc1	Pojer
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
bibliofilie	bibliofilie	k1gFnSc1	bibliofilie
<g/>
,	,	kIx,	,
Hraběnka	hraběnka	k1gFnSc1	hraběnka
Cathleenová	Cathleenová	k1gFnSc1	Cathleenová
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Josef	Josef	k1gMnSc1	Josef
J.	J.	kA	J.
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
Země	země	k1gFnSc1	země
touhy	touha	k1gFnSc2	touha
<g/>
,	,	kIx,	,
Philobiblon	Philobiblon	k1gInSc1	Philobiblon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g />
.	.	kIx.	.
</s>
<s>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skalický	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
Růže	růže	k1gFnSc1	růže
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
básně	báseň	k1gFnPc1	báseň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Bohdan	Bohdan	k1gMnSc1	Bohdan
Chudoba	Chudoba	k1gMnSc1	Chudoba
<g/>
,	,	kIx,	,
Básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
V.	V.	kA	V.
Pojer	Pojer	k1gMnSc1	Pojer
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skalický	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
Essaye	Essaye	k1gFnSc1	Essaye
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
V.	V.	kA	V.
Pojer	Pojer	k1gMnSc1	Pojer
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skalický	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
1995	[number]	k4	1995
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Eseje	esej	k1gInSc2	esej
<g/>
.	.	kIx.	.
</s>
<s>
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
růže	růže	k1gFnSc1	růže
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
V.	V.	kA	V.
Pojer	Pojer	k1gMnSc1	Pojer
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skalický	Skalický	k1gMnSc1	Skalický
<g/>
,	,	kIx,	,
Slova	slovo	k1gNnPc4	slovo
snad	snad	k9	snad
pro	pro	k7c4	pro
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jiří	Jiří	k1gMnSc1	Jiří
Valja	Valja	k1gMnSc1	Valja
<g/>
,	,	kIx,	,
výbor	výbor	k1gInSc1	výbor
z	z	k7c2	z
poezie	poezie	k1gFnSc2	poezie
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
Jiří	Jiří	k1gMnSc1	Jiří
Levý	levý	k2eAgMnSc1d1	levý
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
William	William	k1gInSc4	William
Butler	Butler	k1gInSc4	Butler
Yeats	Yeatsa	k1gFnPc2	Yeatsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
William	William	k1gInSc1	William
Butler	Butler	k1gInSc1	Butler
Yeats	Yeatsa	k1gFnPc2	Yeatsa
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
William	William	k1gInSc1	William
Butler	Butler	k1gInSc1	Butler
Yeats	Yeats	k1gInSc1	Yeats
Plné	plný	k2eAgInPc1d1	plný
texty	text	k1gInPc1	text
děl	dělo	k1gNnPc2	dělo
autora	autor	k1gMnSc2	autor
William	William	k1gInSc4	William
Butler	Butler	k1gInSc1	Butler
Yeats	Yeats	k1gInSc1	Yeats
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
<g />
.	.	kIx.	.
</s>
<s>
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
Autor	autor	k1gMnSc1	autor
William	William	k1gInSc4	William
Butler	Butler	k1gInSc1	Butler
Yeats	Yeatsa	k1gFnPc2	Yeatsa
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
bio	bio	k?	bio
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Major	major	k1gMnSc1	major
exhibition	exhibition	k1gInSc1	exhibition
at	at	k?	at
the	the	k?	the
National	National	k1gMnSc1	National
Library	Librara	k1gFnSc2	Librara
of	of	k?	of
Ireland	Ireland	k1gInSc1	Ireland
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Životopis	životopis	k1gInSc1	životopis
<g/>
,	,	kIx,	,
bibliografie	bibliografie	k1gFnSc1	bibliografie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Yeats	Yeatsa	k1gFnPc2	Yeatsa
Summer	Summra	k1gFnPc2	Summra
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
Sligo	Sligo	k1gNnSc1	Sligo
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Gallery	Galler	k1gInPc1	Galler
of	of	k?	of
Locations	Locations	k1gInSc1	Locations
of	of	k?	of
Yeats	Yeats	k1gInSc1	Yeats
<g/>
'	'	kIx"	'
Poems	Poems	k1gInSc1	Poems
</s>
