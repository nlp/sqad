<s>
Manětín	Manětín	k1gInSc1	Manětín
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Mantiná	Mantiná	k1gFnSc1	Mantiná
<g/>
,	,	kIx,	,
Manetín	Manetín	k1gInSc1	Manetín
<g/>
,	,	kIx,	,
v	v	k7c6	v
lat.	lat.	k?	lat.
a	a	k8xC	a
něm.	něm.	k?	něm.
textech	text	k1gInPc6	text
Manetin	Manetin	k2eAgMnSc1d1	Manetin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
okresu	okres	k1gInSc2	okres
Plzeň-sever	Plzeňevra	k1gFnPc2	Plzeň-sevra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
dalších	další	k2eAgFnPc2d1	další
14	[number]	k4	14
vsí	ves	k1gFnPc2	ves
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
přes	přes	k7c4	přes
jeden	jeden	k4xCgInSc4	jeden
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
8	[number]	k4	8
465,20	[number]	k4	465,20
ha	ha	kA	ha
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
katastrem	katastr	k1gInSc7	katastr
obce	obec	k1gFnSc2	obec
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
Plzeňsku	Plzeňsko	k1gNnSc6	Plzeňsko
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
samotného	samotný	k2eAgInSc2d1	samotný
Manětína	Manětín	k1gInSc2	Manětín
má	mít	k5eAaImIp3nS	mít
výměru	výměra	k1gFnSc4	výměra
876,3	[number]	k4	876,3
ha	ha	kA	ha
a	a	k8xC	a
PSČ	PSČ	kA	PSČ
adres	adresa	k1gFnPc2	adresa
je	být	k5eAaImIp3nS	být
331	[number]	k4	331
01	[number]	k4	01
a	a	k8xC	a
331	[number]	k4	331
62	[number]	k4	62
<g/>
.	.	kIx.	.
</s>
<s>
Manětín	Manětín	k1gInSc1	Manětín
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Manětínského	manětínský	k2eAgInSc2d1	manětínský
potoka	potok	k1gInSc2	potok
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
a	a	k8xC	a
svahu	svah	k1gInSc6	svah
Chlumské	chlumský	k2eAgFnSc2d1	Chlumská
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
29	[number]	k4	29
km	km	kA	km
ssz	ssz	k?	ssz
<g/>
.	.	kIx.	.
od	od	k7c2	od
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nP	křížit
silnice	silnice	k1gFnPc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
201	[number]	k4	201
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
205	[number]	k4	205
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
Manětín	Manětín	k1gInSc1	Manětín
sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c4	na
ssv	ssv	k?	ssv
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
Stvolny	Stvolna	k1gFnPc1	Stvolna
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Hrádkem	Hrádok	k1gInSc7	Hrádok
a	a	k8xC	a
Brdem	brdo	k1gNnSc7	brdo
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Vladměřicemi	Vladměřice	k1gFnPc7	Vladměřice
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Lipí	Lip	k1gFnSc7	Lip
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Lešovicemi	Lešovice	k1gFnPc7	Lešovice
a	a	k8xC	a
Újezdem	Újezd	k1gInSc7	Újezd
<g/>
.	.	kIx.	.
</s>
<s>
Katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
sv.	sv.	kA	sv.
s	s	k7c7	s
obcí	obec	k1gFnSc7	obec
Žihle	žihle	k1gNnSc1	žihle
<g/>
,	,	kIx,	,
na	na	k7c6	na
v.	v.	k?	v.
s	s	k7c7	s
obcemi	obec	k1gFnPc7	obec
Mladotice	Mladotice	k1gFnSc2	Mladotice
a	a	k8xC	a
Štichovice	Štichovice	k1gFnSc2	Štichovice
<g/>
,	,	kIx,	,
na	na	k7c6	na
jv	jv	k?	jv
<g/>
.	.	kIx.	.
s	s	k7c7	s
obcí	obec	k1gFnSc7	obec
Hvozd	hvozd	k1gInSc1	hvozd
<g/>
,	,	kIx,	,
na	na	k7c6	na
j.	j.	k?	j.
pak	pak	k6eAd1	pak
Líté	lítý	k2eAgFnPc1d1	lítá
a	a	k8xC	a
Horní	horní	k2eAgFnSc1d1	horní
Bělá	bělat	k5eAaImIp3nS	bělat
a	a	k8xC	a
na	na	k7c4	na
z.	z.	k?	z.
Nečtiny	Nečtina	k1gFnPc4	Nečtina
a	a	k8xC	a
Bezvěrov	Bezvěrov	k1gInSc4	Bezvěrov
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
hranice	hranice	k1gFnSc1	hranice
katastru	katastr	k1gInSc2	katastr
tvoří	tvořit	k5eAaImIp3nS	tvořit
také	také	k9	také
hranici	hranice	k1gFnSc4	hranice
okresů	okres	k1gInPc2	okres
Plzeň-sever	Plzeňever	k1gInSc1	Plzeň-sever
a	a	k8xC	a
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Sousedními	sousední	k2eAgFnPc7d1	sousední
obcemi	obec	k1gFnPc7	obec
z	z	k7c2	z
okresu	okres	k1gInSc2	okres
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Štědrá	štědrý	k2eAgFnSc1d1	štědrá
<g/>
,	,	kIx,	,
Pšov	Pšov	k1gInSc1	Pšov
a	a	k8xC	a
Chyše	Chyše	k?	Chyše
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ač	ač	k8xS	ač
zabírá	zabírat	k5eAaImIp3nS	zabírat
katastr	katastr	k1gInSc4	katastr
města	město	k1gNnSc2	město
téměř	téměř	k6eAd1	téměř
85	[number]	k4	85
km2	km2	k4	km2
<g/>
,	,	kIx,	,
severní	severní	k2eAgInSc4d1	severní
okraj	okraj	k1gInSc4	okraj
zástavby	zástavba	k1gFnSc2	zástavba
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
jen	jen	k9	jen
cca	cca	kA	cca
300	[number]	k4	300
m	m	kA	m
od	od	k7c2	od
obecní	obecní	k2eAgFnSc2d1	obecní
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgFnSc2d1	okresní
i	i	k8xC	i
krajské	krajský	k2eAgFnSc2d1	krajská
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Manětín	Manětín	k1gInSc1	Manětín
<g/>
,	,	kIx,	,
městečko	městečko	k1gNnSc1	městečko
na	na	k7c6	na
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
zemské	zemský	k2eAgFnSc6d1	zemská
stezce	stezka	k1gFnSc6	stezka
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Chebu	Cheb	k1gInSc2	Cheb
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
připomínán	připomínat	k5eAaImNgInS	připomínat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1169	[number]	k4	1169
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zdejší	zdejší	k2eAgInSc4d1	zdejší
újezd	újezd	k1gInSc4	újezd
daroval	darovat	k5eAaPmAgMnS	darovat
král	král	k1gMnSc1	král
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
řádu	řád	k1gInSc2	řád
pražských	pražský	k2eAgMnPc2d1	pražský
johanitů	johanita	k1gMnPc2	johanita
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1235	[number]	k4	1235
na	na	k7c6	na
králi	král	k1gMnSc6	král
vymohli	vymoct	k5eAaPmAgMnP	vymoct
udělení	udělení	k1gNnSc3	udělení
práva	právo	k1gNnSc2	právo
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
dnešního	dnešní	k2eAgInSc2d1	dnešní
zámku	zámek	k1gInSc2	zámek
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
tvrz	tvrz	k1gFnSc1	tvrz
a	a	k8xC	a
klášter	klášter	k1gInSc1	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
dal	dát	k5eAaPmAgMnS	dát
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
manětínské	manětínský	k2eAgNnSc4d1	manětínský
panství	panství	k1gNnSc4	panství
do	do	k7c2	do
zástavy	zástava	k1gFnSc2	zástava
Bohuslavu	Bohuslava	k1gFnSc4	Bohuslava
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
stavěl	stavět	k5eAaImAgMnS	stavět
proti	proti	k7c3	proti
husitům	husita	k1gMnPc3	husita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1422	[number]	k4	1422
zajali	zajmout	k5eAaPmAgMnP	zajmout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc4	panství
později	pozdě	k6eAd2	pozdě
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Hynek	Hynek	k1gMnSc1	Hynek
Krušina	krušina	k1gFnSc1	krušina
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
manětínské	manětínský	k2eAgFnSc6d1	Manětínská
tvrzi	tvrz	k1gFnSc6	tvrz
sídlil	sídlit	k5eAaImAgMnS	sídlit
správce	správce	k1gMnSc1	správce
(	(	kIx(	(
<g/>
purkrabí	purkrabí	k1gMnSc1	purkrabí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Krušina	krušina	k1gFnSc1	krušina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1455	[number]	k4	1455
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
panství	panství	k1gNnSc1	panství
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
ostře	ostro	k6eAd1	ostro
stavěl	stavět	k5eAaImAgMnS	stavět
proti	proti	k7c3	proti
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
králi	král	k1gMnSc3	král
Jiřímu	Jiří	k1gMnSc3	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
českými	český	k2eAgMnPc7d1	český
pány	pan	k1gMnPc7	pan
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1465	[number]	k4	1465
tzv.	tzv.	kA	tzv.
jednotu	jednota	k1gFnSc4	jednota
zelenohorskou	zelenohorský	k2eAgFnSc7d1	Zelenohorská
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
křižáky	křižák	k1gMnPc7	křižák
a	a	k8xC	a
podnikal	podnikat	k5eAaImAgMnS	podnikat
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
ničivé	ničivý	k2eAgInPc1d1	ničivý
výpady	výpad	k1gInPc1	výpad
na	na	k7c6	na
území	území	k1gNnSc6	území
západních	západní	k2eAgFnPc2d1	západní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1483	[number]	k4	1483
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
od	od	k7c2	od
řádu	řád	k1gInSc2	řád
johanitů	johanita	k1gMnPc2	johanita
dědičně	dědičně	k6eAd1	dědičně
odkoupil	odkoupit	k5eAaPmAgMnS	odkoupit
celé	celý	k2eAgNnSc4d1	celé
panství	panství	k1gNnSc4	panství
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ho	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
zástavě	zástava	k1gFnSc6	zástava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Švamberků	Švamberka	k1gMnPc2	Švamberka
panství	panství	k1gNnSc2	panství
prodal	prodat	k5eAaPmAgInS	prodat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1544	[number]	k4	1544
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
majitelé	majitel	k1gMnPc1	majitel
střídali	střídat	k5eAaImAgMnP	střídat
častěji	často	k6eAd2	často
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
rod	rod	k1gInSc1	rod
Hrobčických	Hrobčická	k1gFnPc2	Hrobčická
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
panství	panství	k1gNnPc4	panství
vlastnili	vlastnit	k5eAaImAgMnP	vlastnit
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1560	[number]	k4	1560
<g/>
-	-	kIx~	-
<g/>
1617	[number]	k4	1617
a	a	k8xC	a
za	za	k7c4	za
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
vlády	vláda	k1gFnPc4	vláda
vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původní	původní	k2eAgFnSc2d1	původní
tvrze	tvrz	k1gFnSc2	tvrz
renesanční	renesanční	k2eAgInSc1d1	renesanční
zámek	zámek	k1gInSc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
získala	získat	k5eAaPmAgFnS	získat
panství	panství	k1gNnSc4	panství
Ester	Ester	k1gFnSc2	Ester
Lažanská	Lažanský	k2eAgFnSc1d1	Lažanská
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
Jiřím	Jiří	k1gMnSc7	Jiří
Mitrovským	Mitrovský	k2eAgMnSc7d1	Mitrovský
z	z	k7c2	z
Nemyšle	Nemyšle	k1gNnSc2	Nemyšle
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
<g/>
"	"	kIx"	"
o	o	k7c4	o
násilnou	násilný	k2eAgFnSc4d1	násilná
rekatolizaci	rekatolizace	k1gFnSc4	rekatolizace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
-	-	kIx~	-
odvolal	odvolat	k5eAaPmAgMnS	odvolat
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
faráře	farář	k1gMnSc4	farář
a	a	k8xC	a
dosadil	dosadit	k5eAaPmAgMnS	dosadit
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
jezuitu	jezuita	k1gMnSc4	jezuita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1625	[number]	k4	1625
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
vzbouřili	vzbouřit	k5eAaPmAgMnP	vzbouřit
poddaní	poddaný	k1gMnPc1	poddaný
a	a	k8xC	a
obléhali	obléhat	k5eAaImAgMnP	obléhat
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Mitrovského	Mitrovský	k2eAgInSc2d1	Mitrovský
osvobodilo	osvobodit	k5eAaPmAgNnS	osvobodit
až	až	k9	až
přivolané	přivolaný	k2eAgNnSc1d1	přivolané
vojsko	vojsko	k1gNnSc1	vojsko
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
odkázala	odkázat	k5eAaPmAgFnS	odkázat
majitelka	majitelka	k1gFnSc1	majitelka
Ester	Ester	k1gFnSc1	Ester
Lažanská	Lažanský	k2eAgFnSc1d1	Lažanská
panství	panství	k1gNnSc4	panství
svému	svůj	k1gMnSc3	svůj
bratrovi	bratr	k1gMnSc3	bratr
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Manětín	Manětín	k1gInSc4	Manětín
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
stavebního	stavební	k2eAgInSc2d1	stavební
rozkvětu	rozkvět	k1gInSc2	rozkvět
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
paradoxně	paradoxně	k6eAd1	paradoxně
zahájil	zahájit	k5eAaPmAgInS	zahájit
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
města	město	k1gNnSc2	město
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
lehl	lehnout	k5eAaPmAgMnS	lehnout
popelem	popel	k1gInSc7	popel
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
polovina	polovina	k1gFnSc1	polovina
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
nechal	nechat	k5eAaPmAgMnS	nechat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
majitel	majitel	k1gMnSc1	majitel
Václav	Václav	k1gMnSc1	Václav
Josef	Josef	k1gMnSc1	Josef
Lažanský	Lažanský	k2eAgInSc4d1	Lažanský
vyhořelý	vyhořelý	k2eAgInSc4d1	vyhořelý
zámek	zámek	k1gInSc4	zámek
barokně	barokně	k6eAd1	barokně
přestavět	přestavět	k5eAaPmF	přestavět
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
architekta	architekt	k1gMnSc2	architekt
Tomáše	Tomáš	k1gMnSc2	Tomáš
Haffeneckra	Haffeneckr	k1gMnSc2	Haffeneckr
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
barokní	barokní	k2eAgFnSc2d1	barokní
kultury	kultura	k1gFnSc2	kultura
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Václava	Václav	k1gMnSc2	Václav
Josefa	Josef	k1gMnSc2	Josef
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Manětíně	Manětín	k1gInSc6	Manětín
tehdy	tehdy	k6eAd1	tehdy
tvořili	tvořit	k5eAaImAgMnP	tvořit
malíři	malíř	k1gMnPc1	malíř
Petr	Petr	k1gMnSc1	Petr
Brandl	Brandl	k1gMnSc1	Brandl
a	a	k8xC	a
F.	F.	kA	F.
K.	K.	kA	K.
Bentum	Bentum	k1gNnSc4	Bentum
<g/>
,	,	kIx,	,
architekti	architekt	k1gMnPc1	architekt
J.	J.	kA	J.
B.	B.	kA	B.
Santini	Santin	k2eAgMnPc1d1	Santin
a	a	k8xC	a
J.	J.	kA	J.
J.	J.	kA	J.
Hess	Hessa	k1gFnPc2	Hessa
<g/>
,	,	kIx,	,
sochaři	sochař	k1gMnPc1	sochař
Š.	Š.	kA	Š.
Borovec	Borovec	k1gMnSc1	Borovec
a	a	k8xC	a
J.	J.	kA	J.
Herscher	Herschra	k1gFnPc2	Herschra
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Jan	Jan	k1gMnSc1	Jan
Josef	Josef	k1gMnSc1	Josef
Brixi	Brixe	k1gFnSc4	Brixe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
zámku	zámek	k1gInSc2	zámek
založen	založit	k5eAaPmNgInS	založit
anglický	anglický	k2eAgInSc1d1	anglický
park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
sílily	sílit	k5eAaImAgInP	sílit
protesty	protest	k1gInPc1	protest
proti	proti	k7c3	proti
robotě	robota	k1gFnSc3	robota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
zrušení	zrušení	k1gNnSc6	zrušení
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
nový	nový	k2eAgInSc1d1	nový
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Manětín	Manětín	k1gInSc1	Manětín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1857	[number]	k4	1857
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
silnice	silnice	k1gFnSc1	silnice
do	do	k7c2	do
Žlutic	Žlutice	k1gFnPc2	Žlutice
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
byla	být	k5eAaImAgFnS	být
projektována	projektovat	k5eAaBmNgFnS	projektovat
i	i	k9	i
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Plzeň	Plzeň	k1gFnSc1	Plzeň
-	-	kIx~	-
Žlutice	žlutice	k1gFnSc1	žlutice
-	-	kIx~	-
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
činnost	činnost	k1gFnSc1	činnost
SDH	SDH	kA	SDH
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
výstavba	výstavba	k1gFnSc1	výstavba
silnic	silnice	k1gFnPc2	silnice
-	-	kIx~	-
na	na	k7c4	na
Nečtiny	Nečtina	k1gFnPc4	Nečtina
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgFnPc1d1	dolní
Bělou	Běla	k1gFnSc7	Běla
a	a	k8xC	a
do	do	k7c2	do
Kaznějova	Kaznějův	k2eAgNnSc2d1	Kaznějův
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Mladotic	Mladotice	k1gFnPc2	Mladotice
a	a	k8xC	a
do	do	k7c2	do
Rabštejna	Rabštejn	k1gInSc2	Rabštejn
nad	nad	k7c7	nad
Střelou	střela	k1gFnSc7	střela
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatné	samostatný	k2eAgFnSc2d1	samostatná
ČSR	ČSR	kA	ČSR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
vyvěsila	vyvěsit	k5eAaPmAgFnS	vyvěsit
většina	většina	k1gFnSc1	většina
občanů	občan	k1gMnPc2	občan
města	město	k1gNnSc2	město
české	český	k2eAgInPc1d1	český
prapory	prapor	k1gInPc1	prapor
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
visel	viset	k5eAaImAgInS	viset
černo-žlutý	černo-žlutý	k2eAgInSc1d1	černo-žlutý
prapor	prapor	k1gInSc1	prapor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
následovně	následovně	k6eAd1	následovně
místními	místní	k2eAgMnPc7d1	místní
Čechy	Čech	k1gMnPc7	Čech
odstraněn	odstraněn	k2eAgInSc1d1	odstraněn
<g/>
.	.	kIx.	.
</s>
<s>
Manětín	Manětín	k1gInSc1	Manětín
byl	být	k5eAaImAgInS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
měst	město	k1gNnPc2	město
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
s	s	k7c7	s
převládajícím	převládající	k2eAgNnSc7d1	převládající
českým	český	k2eAgNnSc7d1	české
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
německými	německý	k2eAgFnPc7d1	německá
obcemi	obec	k1gFnPc7	obec
(	(	kIx(	(
<g/>
Rabštejn	Rabštejn	k1gInSc1	Rabštejn
nad	nad	k7c7	nad
Střelou	střela	k1gFnSc7	střela
<g/>
,	,	kIx,	,
Pšov	Pšov	k1gInSc1	Pšov
<g/>
,	,	kIx,	,
Bezvěrov	Bezvěrov	k1gInSc1	Bezvěrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
1921	[number]	k4	1921
bylo	být	k5eAaImAgNnS	být
napočítáno	napočítat	k5eAaPmNgNnS	napočítat
1	[number]	k4	1
017	[number]	k4	017
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
242	[number]	k4	242
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
podíl	podíl	k1gInSc1	podíl
Čechů	Čech	k1gMnPc2	Čech
80	[number]	k4	80
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
definitivně	definitivně	k6eAd1	definitivně
padly	padnout	k5eAaImAgInP	padnout
veškeré	veškerý	k3xTgInPc1	veškerý
plány	plán	k1gInPc1	plán
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
lokální	lokální	k2eAgFnSc2d1	lokální
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
z	z	k7c2	z
Plzně	Plzeň	k1gFnSc2	Plzeň
do	do	k7c2	do
Manětína	Manětín	k1gInSc2	Manětín
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Manětín	Manětín	k1gInSc1	Manětín
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Plzeň	Plzeň	k1gFnSc1	Plzeň
-	-	kIx~	-
Žatec	Žatec	k1gInSc1	Žatec
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
vzdálena	vzdálen	k2eAgFnSc1d1	vzdálena
10	[number]	k4	10
km	km	kA	km
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c6	na
"	"	kIx"	"
<g/>
Mladotice	Mladotika	k1gFnSc6	Mladotika
-	-	kIx~	-
zastávka	zastávka	k1gFnSc1	zastávka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1924	[number]	k4	1924
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
mezi	mezi	k7c7	mezi
Plzní	Plzeň	k1gFnSc7	Plzeň
a	a	k8xC	a
Manětínem	Manětín	k1gInSc7	Manětín
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
dohodě	dohoda	k1gFnSc6	dohoda
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Manětín	Manětín	k1gInSc1	Manětín
poslední	poslední	k2eAgInSc1d1	poslední
českou	český	k2eAgFnSc7d1	Česká
obcí	obec	k1gFnSc7	obec
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nebyla	být	k5eNaImAgFnS	být
přičleněna	přičlenit	k5eAaPmNgFnS	přičlenit
k	k	k7c3	k
Říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Osvobozen	osvobozen	k2eAgMnSc1d1	osvobozen
byl	být	k5eAaImAgMnS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
americkou	americký	k2eAgFnSc7d1	americká
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
byl	být	k5eAaImAgInS	být
zkonfiskován	zkonfiskován	k2eAgInSc1d1	zkonfiskován
rodu	rod	k1gInSc3	rod
Lažanských	Lažanský	k2eAgFnPc2d1	Lažanská
<g/>
,	,	kIx,	,
zestátněn	zestátněn	k2eAgInSc1d1	zestátněn
a	a	k8xC	a
využíván	využíván	k2eAgInSc1d1	využíván
jako	jako	k8xS	jako
kanceláře	kancelář	k1gFnPc1	kancelář
různých	různý	k2eAgFnPc2d1	různá
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Odsun	odsun	k1gInSc4	odsun
Němců	Němec	k1gMnPc2	Němec
Manětín	Manětín	k1gInSc1	Manětín
nezasáhl	zasáhnout	k5eNaPmAgInS	zasáhnout
tolik	tolik	k6eAd1	tolik
jako	jako	k9	jako
většinu	většina	k1gFnSc4	většina
okolních	okolní	k2eAgFnPc2d1	okolní
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Vysídleny	vysídlen	k2eAgFnPc1d1	vysídlena
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
dříve	dříve	k6eAd2	dříve
"	"	kIx"	"
<g/>
německé	německý	k2eAgFnPc4d1	německá
<g/>
"	"	kIx"	"
vesnice	vesnice	k1gFnPc4	vesnice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
pod	pod	k7c4	pod
Manětín	Manětín	k1gInSc4	Manětín
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nepatřily	patřit	k5eNaImAgInP	patřit
-	-	kIx~	-
např.	např.	kA	např.
Rabštejn	Rabštejn	k1gInSc1	Rabštejn
nad	nad	k7c7	nad
Střelou	střela	k1gFnSc7	střela
nebo	nebo	k8xC	nebo
Zhořec	Zhořec	k1gMnSc1	Zhořec
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
komunismu	komunismus	k1gInSc2	komunismus
přinesl	přinést	k5eAaPmAgInS	přinést
také	také	k9	také
správní	správní	k2eAgFnSc4d1	správní
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
stal	stát	k5eAaPmAgInS	stát
Manětín	Manětín	k1gInSc1	Manětín
součástí	součást	k1gFnPc2	součást
okresu	okres	k1gInSc2	okres
Plasy	plasa	k1gFnSc2	plasa
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
okres	okres	k1gInSc1	okres
Kralovice	Kralovice	k1gFnSc2	Kralovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1957	[number]	k4	1957
se	se	k3xPyFc4	se
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
podařilo	podařit	k5eAaPmAgNnS	podařit
ustanovit	ustanovit	k5eAaPmF	ustanovit
JZD	JZD	kA	JZD
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
Manětín	Manětín	k1gInSc1	Manětín
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
okresu	okres	k1gInSc2	okres
Plzeň-sever	Plzeňevra	k1gFnPc2	Plzeň-sevra
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1973	[number]	k4	1973
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
Chlumské	chlumský	k2eAgFnSc2d1	Chlumská
hory	hora	k1gFnSc2	hora
vystavěny	vystavěn	k2eAgFnPc4d1	vystavěna
nové	nový	k2eAgFnPc4d1	nová
bytovky	bytovka	k1gFnPc4	bytovka
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
ke	k	k7c3	k
městu	město	k1gNnSc3	město
integrovaly	integrovat	k5eAaBmAgFnP	integrovat
další	další	k2eAgFnPc1d1	další
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
vesnice	vesnice	k1gFnPc1	vesnice
<g/>
,	,	kIx,	,
maximálního	maximální	k2eAgInSc2d1	maximální
stavu	stav	k1gInSc2	stav
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mělo	mít	k5eAaImAgNnS	mít
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
obce	obec	k1gFnSc2	obec
rozlohu	rozloh	k1gInSc2	rozloh
96	[number]	k4	96
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
převzalo	převzít	k5eAaPmAgNnS	převzít
zámek	zámek	k1gInSc4	zámek
od	od	k7c2	od
Státních	státní	k2eAgInPc2d1	státní
lesů	les	k1gInPc2	les
Muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
severního	severní	k2eAgNnSc2d1	severní
Plzeňska	Plzeňsko	k1gNnSc2	Plzeňsko
a	a	k8xC	a
provedlo	provést	k5eAaPmAgNnS	provést
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
45	[number]	k4	45
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Manětín	Manětín	k1gInSc1	Manětín
potýká	potýkat	k5eAaImIp3nS	potýkat
s	s	k7c7	s
úbytkem	úbytek	k1gInSc7	úbytek
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
značná	značný	k2eAgFnSc1d1	značná
odlehlost	odlehlost	k1gFnSc1	odlehlost
města	město	k1gNnSc2	město
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
dvou	dva	k4xCgInPc2	dva
krajů	kraj	k1gInPc2	kraj
-	-	kIx~	-
Plzeňského	plzeňský	k2eAgMnSc2d1	plzeňský
a	a	k8xC	a
Karlovarského	karlovarský	k2eAgMnSc2d1	karlovarský
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
spojený	spojený	k2eAgInSc4d1	spojený
nedostatek	nedostatek	k1gInSc4	nedostatek
dostupných	dostupný	k2eAgFnPc2d1	dostupná
pracovních	pracovní	k2eAgFnPc2d1	pracovní
příležitostí	příležitost	k1gFnPc2	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Autobus	autobus	k1gInSc4	autobus
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
jede	jet	k5eAaImIp3nS	jet
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgNnPc1d3	nejbližší
sousední	sousední	k2eAgNnPc1d1	sousední
města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
vzdálena	vzdálit	k5eAaPmNgNnP	vzdálit
kolem	kolem	k7c2	kolem
15	[number]	k4	15
km	km	kA	km
(	(	kIx(	(
<g/>
Žlutice	žlutice	k1gFnSc2	žlutice
a	a	k8xC	a
Plasy	plasa	k1gFnSc2	plasa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
do	do	k7c2	do
Žlutic	Žlutice	k1gFnPc2	Žlutice
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
kvůli	kvůli	k7c3	kvůli
okresní	okresní	k2eAgFnSc3d1	okresní
a	a	k8xC	a
krajské	krajský	k2eAgFnSc3d1	krajská
hranici	hranice	k1gFnSc3	hranice
špatná	špatný	k2eAgFnSc1d1	špatná
dopravní	dopravní	k2eAgFnSc1d1	dopravní
dostupnost	dostupnost	k1gFnSc1	dostupnost
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
snad	snad	k9	snad
ještě	ještě	k9	ještě
poměrně	poměrně	k6eAd1	poměrně
méně	málo	k6eAd2	málo
pracovních	pracovní	k2eAgFnPc2d1	pracovní
příležitostí	příležitost	k1gFnPc2	příležitost
<g/>
,	,	kIx,	,
než	než	k8xS	než
v	v	k7c6	v
Manětíně	Manětín	k1gInSc6	Manětín
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
Manětín	Manětín	k1gInSc1	Manětín
velmi	velmi	k6eAd1	velmi
pěkným	pěkný	k2eAgNnSc7d1	pěkné
a	a	k8xC	a
příjemným	příjemný	k2eAgNnSc7d1	příjemné
městem	město	k1gNnSc7	město
v	v	k7c6	v
krásné	krásný	k2eAgFnSc6d1	krásná
a	a	k8xC	a
čisté	čistý	k2eAgFnSc6d1	čistá
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
barokní	barokní	k2eAgFnSc7d1	barokní
architekturou	architektura	k1gFnSc7	architektura
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
okolními	okolní	k2eAgNnPc7d1	okolní
městy	město	k1gNnPc7	město
naprosto	naprosto	k6eAd1	naprosto
unikátní	unikátní	k2eAgFnSc1d1	unikátní
<g/>
.	.	kIx.	.
</s>
<s>
Přilehlé	přilehlý	k2eAgInPc1d1	přilehlý
husté	hustý	k2eAgInPc1d1	hustý
lesy	les	k1gInPc1	les
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
katastru	katastr	k1gInSc2	katastr
tvoří	tvořit	k5eAaImIp3nS	tvořit
souvislá	souvislý	k2eAgFnSc1d1	souvislá
lesní	lesní	k2eAgFnSc1d1	lesní
plocha	plocha	k1gFnSc1	plocha
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
skoro	skoro	k6eAd1	skoro
95	[number]	k4	95
km2	km2	k4	km2
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgNnPc1d1	dosahující
až	až	k9	až
k	k	k7c3	k
Úněšovu	Úněšův	k2eAgMnSc3d1	Úněšův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnSc1	údolí
Manětínského	manětínský	k2eAgInSc2d1	manětínský
potoka	potok	k1gInSc2	potok
a	a	k8xC	a
Střely	střela	k1gFnSc2	střela
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
zajímavá	zajímavý	k2eAgNnPc4d1	zajímavé
sídla	sídlo	k1gNnPc4	sídlo
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
(	(	kIx(	(
<g/>
vesnice	vesnice	k1gFnSc1	vesnice
Nečtiny	Nečtina	k1gFnSc2	Nečtina
se	s	k7c7	s
zámkem	zámek	k1gInSc7	zámek
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgNnSc4d3	nejmenší
"	"	kIx"	"
<g/>
město	město	k1gNnSc4	město
<g/>
"	"	kIx"	"
Evropy	Evropa	k1gFnSc2	Evropa
Rabštejn	Rabštejn	k1gMnSc1	Rabštejn
nad	nad	k7c7	nad
Střelou	střela	k1gFnSc7	střela
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
lákadlem	lákadlo	k1gNnSc7	lákadlo
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
patří	patřit	k5eAaImIp3nS	patřit
Manětín	Manětín	k1gInSc1	Manětín
mezi	mezi	k7c4	mezi
oblíbená	oblíbený	k2eAgNnPc4d1	oblíbené
rekreační	rekreační	k2eAgNnPc4d1	rekreační
zázemí	zázemí	k1gNnPc4	zázemí
Plzeňanů	Plzeňan	k1gMnPc2	Plzeňan
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgNnSc1d1	zdravotní
středisko	středisko	k1gNnSc1	středisko
<g/>
,	,	kIx,	,
obchody	obchod	k1gInPc1	obchod
<g/>
,	,	kIx,	,
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
služebna	služebna	k1gFnSc1	služebna
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
a	a	k8xC	a
několik	několik	k4yIc4	několik
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
penzion	penzion	k1gInSc1	penzion
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
1	[number]	k4	1
245	[number]	k4	245
Národnost	národnost	k1gFnSc1	národnost
<g/>
:	:	kIx,	:
česká	český	k2eAgFnSc1d1	Česká
:	:	kIx,	:
96,5	[number]	k4	96,5
%	%	kIx~	%
slovenská	slovenský	k2eAgFnSc1d1	slovenská
:	:	kIx,	:
1,7	[number]	k4	1,7
%	%	kIx~	%
německá	německý	k2eAgFnSc1d1	německá
:	:	kIx,	:
0,2	[number]	k4	0,2
%	%	kIx~	%
Náboženské	náboženský	k2eAgNnSc1d1	náboženské
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
:	:	kIx,	:
věřící	věřící	k1gFnSc1	věřící
:	:	kIx,	:
32,3	[number]	k4	32,3
%	%	kIx~	%
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
:	:	kIx,	:
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
:	:	kIx,	:
89,3	[number]	k4	89,3
%	%	kIx~	%
českobratrská	českobratrský	k2eAgFnSc1d1	Českobratrská
církev	církev	k1gFnSc1	církev
evangelická	evangelický	k2eAgFnSc1d1	evangelická
:	:	kIx,	:
2,2	[number]	k4	2,2
%	%	kIx~	%
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
:	:	kIx,	:
ekonomicky	ekonomicky	k6eAd1	ekonomicky
<g />
.	.	kIx.	.
</s>
<s>
aktivní	aktivní	k2eAgMnSc1d1	aktivní
:	:	kIx,	:
675	[number]	k4	675
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
:	:	kIx,	:
nezaměstnaní	nezaměstnaný	k1gMnPc1	nezaměstnaný
<g/>
:	:	kIx,	:
5,63	[number]	k4	5,63
%	%	kIx~	%
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
zaměstnaní	zaměstnaný	k1gMnPc1	zaměstnaný
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
:	:	kIx,	:
24,6	[number]	k4	24,6
%	%	kIx~	%
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
:	:	kIx,	:
23,0	[number]	k4	23,0
%	%	kIx~	%
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
správě	správa	k1gFnSc6	správa
<g/>
:	:	kIx,	:
9,2	[number]	k4	9,2
%	%	kIx~	%
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
a	a	k8xC	a
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
<g/>
:	:	kIx,	:
8,1	[number]	k4	8,1
%	%	kIx~	%
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
8,0	[number]	k4	8,0
%	%	kIx~	%
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
:	:	kIx,	:
1	[number]	k4	1
210	[number]	k4	210
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
:	:	kIx,	:
podíl	podíl	k1gInSc1	podíl
mužů	muž	k1gMnPc2	muž
<g/>
:	:	kIx,	:
51,1	[number]	k4	51,1
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
13,3	[number]	k4	13,3
%	%	kIx~	%
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
<g/>
:	:	kIx,	:
40,9	[number]	k4	40,9
let	léto	k1gNnPc2	léto
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Manětína	Manětín	k1gInSc2	Manětín
také	také	k9	také
obec	obec	k1gFnSc1	obec
Štichovice	Štichovice	k1gFnSc1	Štichovice
<g/>
.	.	kIx.	.
</s>
<s>
Manětín	Manětín	k1gInSc1	Manětín
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
městečko	městečko	k1gNnSc1	městečko
poprvé	poprvé	k6eAd1	poprvé
připomínán	připomínán	k2eAgMnSc1d1	připomínán
ve	v	k7c4	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesná	přesný	k2eAgFnSc1d1	přesná
doba	doba	k1gFnSc1	doba
přidělení	přidělení	k1gNnSc4	přidělení
znaku	znak	k1gInSc2	znak
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
doložen	doložit	k5eAaPmNgMnS	doložit
znakovou	znakový	k2eAgFnSc7d1	znaková
pečetí	pečeť	k1gFnSc7	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
polovina	polovina	k1gFnSc1	polovina
štítu	štít	k1gInSc2	štít
připomíná	připomínat	k5eAaImIp3nS	připomínat
stříbrným	stříbrný	k2eAgInSc7d1	stříbrný
maltézským	maltézský	k2eAgInSc7d1	maltézský
křížem	kříž	k1gInSc7	kříž
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
pozadí	pozadí	k1gNnSc6	pozadí
působení	působení	k1gNnSc1	působení
johanitů	johanita	k1gMnPc2	johanita
ve	v	k7c6	v
městě	město	k1gNnSc6	město
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1169	[number]	k4	1169
<g/>
-	-	kIx~	-
<g/>
1420	[number]	k4	1420
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
Čarek	Čarek	k6eAd1	Čarek
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
znak	znak	k1gInSc1	znak
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
nejdříve	dříve	k6eAd3	dříve
erbem	erb	k1gInSc7	erb
vrchnosti	vrchnost	k1gFnSc2	vrchnost
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
johanitského	johanitský	k2eAgMnSc4d1	johanitský
velkopřevora	velkopřevor	k1gMnSc4	velkopřevor
Semovita	Semovita	k1gFnSc1	Semovita
<g/>
,	,	kIx,	,
knížete	kníže	k1gMnSc2	kníže
těšínského	těšínské	k1gNnSc2	těšínské
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
vrchnosti	vrchnost	k1gFnSc2	vrchnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1420	[number]	k4	1420
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
erb	erb	k1gInSc1	erb
změnit	změnit	k5eAaPmF	změnit
na	na	k7c4	na
znak	znak	k1gInSc4	znak
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
levá	levý	k2eAgFnSc1d1	levá
polovina	polovina	k1gFnSc1	polovina
má	mít	k5eAaImIp3nS	mít
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
pozadí	pozadí	k1gNnSc4	pozadí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nese	nést	k5eAaImIp3nS	nést
polovinu	polovina	k1gFnSc4	polovina
černé	černý	k2eAgFnSc2d1	černá
orlice	orlice	k1gFnSc2	orlice
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
zbrojí	zbroj	k1gFnSc7	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
pohled	pohled	k1gInSc1	pohled
orlice	orlice	k1gFnSc2	orlice
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nemíří	mířit	k5eNaImIp3nS	mířit
vlevo	vlevo	k6eAd1	vlevo
<g/>
,	,	kIx,	,
vně	vně	k7c2	vně
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
doprava	doprava	k1gFnSc1	doprava
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
středu	střed	k1gInSc3	střed
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nP	křížit
silnice	silnice	k1gFnPc1	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
201	[number]	k4	201
(	(	kIx(	(
<g/>
Jeneč	Jeneč	k1gInSc1	Jeneč
-	-	kIx~	-
Křivoklát	Křivoklát	k1gInSc1	Křivoklát
-	-	kIx~	-
Kralovice	Kralovice	k1gFnPc1	Kralovice
-	-	kIx~	-
Konstantinovy	Konstantinův	k2eAgFnPc1d1	Konstantinova
Lázně	lázeň	k1gFnPc1	lázeň
-	-	kIx~	-
Planá	Planá	k1gFnSc1	Planá
-	-	kIx~	-
hraniční	hraniční	k2eAgInSc1d1	hraniční
přechod	přechod	k1gInSc1	přechod
Broumov	Broumov	k1gInSc1	Broumov
<g/>
/	/	kIx~	/
<g/>
Mähring	Mähring	k1gInSc1	Mähring
<g/>
)	)	kIx)	)
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
205	[number]	k4	205
(	(	kIx(	(
<g/>
Nevřeň	Nevřeň	k1gFnSc1	Nevřeň
-	-	kIx~	-
Nekmíř	Nekmíř	k1gFnSc1	Nekmíř
-	-	kIx~	-
Žlutice	žlutice	k1gFnSc1	žlutice
-	-	kIx~	-
Hvězda	Hvězda	k1gMnSc1	Hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
sem	sem	k6eAd1	sem
přichází	přicházet	k5eAaImIp3nS	přicházet
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
silnic	silnice	k1gFnPc2	silnice
nižších	nízký	k2eAgFnPc2d2	nižší
tříd	třída	k1gFnPc2	třída
spojujících	spojující	k2eAgFnPc2d1	spojující
zejména	zejména	k9	zejména
vesnice	vesnice	k1gFnPc1	vesnice
s	s	k7c7	s
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
440070	[number]	k4	440070
Plzeň	Plzeň	k1gFnSc1	Plzeň
-	-	kIx~	-
Dolní	dolní	k2eAgNnSc1d1	dolní
Bělá	bělat	k5eAaImIp3nS	bělat
-	-	kIx~	-
Manětín	Manětín	k1gInSc1	Manětín
-	-	kIx~	-
Žlutice	žlutice	k1gFnSc1	žlutice
460030	[number]	k4	460030
Plzeň	Plzeň	k1gFnSc1	Plzeň
-	-	kIx~	-
Dolní	dolní	k2eAgNnSc1d1	dolní
Bělá	bělat	k5eAaImIp3nS	bělat
-	-	kIx~	-
Manětín	Manětín	k1gInSc1	Manětín
-	-	kIx~	-
Nečtiny	Nečtina	k1gFnSc2	Nečtina
460127	[number]	k4	460127
Manětín	Manětín	k1gInSc1	Manětín
-	-	kIx~	-
Bezvěrov	Bezvěrov	k1gInSc1	Bezvěrov
-	-	kIx~	-
Úterý	úterý	k1gNnSc1	úterý
-	-	kIx~	-
Bezdružice	Bezdružice	k1gFnPc1	Bezdružice
460181	[number]	k4	460181
Kralovice	Kralovice	k1gFnPc1	Kralovice
-	-	kIx~	-
Žihle	žihle	k1gNnSc1	žihle
-	-	kIx~	-
Rabštejn	Rabštejn	k1gNnSc1	Rabštejn
nad	nad	k7c7	nad
Střelou	střela	k1gFnSc7	střela
-	-	kIx~	-
Manětín	Manětín	k1gInSc1	Manětín
460710	[number]	k4	460710
Plasy	plasa	k1gFnSc2	plasa
-	-	kIx~	-
Manětín	Manětín	k1gInSc1	Manětín
-	-	kIx~	-
Nečtiny	Nečtina	k1gFnSc2	Nečtina
460780	[number]	k4	460780
Plasy	plasa	k1gFnSc2	plasa
-	-	kIx~	-
Dolní	dolní	k2eAgNnSc1d1	dolní
Bělá	bělat	k5eAaImIp3nS	bělat
-	-	kIx~	-
Úněšov	Úněšov	k1gInSc1	Úněšov
-	-	kIx~	-
Nečtiny	Nečtina	k1gFnPc1	Nečtina
-	-	kIx~	-
Manětín	Manětín	k1gInSc1	Manětín
460790	[number]	k4	460790
Kralovice	Kralovice	k1gFnSc2	Kralovice
-	-	kIx~	-
Mladotice	Mladotice	k1gFnSc1	Mladotice
-	-	kIx~	-
Manětín	Manětín	k1gInSc1	Manětín
-	-	kIx~	-
Rabštejn	Rabštejn	k1gInSc1	Rabštejn
nad	nad	k7c7	nad
Střelou	střela	k1gFnSc7	střela
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Manětíně	Manětín	k1gInSc6	Manětín
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
množství	množství	k1gNnSc4	množství
barokních	barokní	k2eAgFnPc2d1	barokní
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
soch	socha	k1gFnPc2	socha
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
barokní	barokní	k2eAgFnSc1d1	barokní
perla	perla	k1gFnSc1	perla
západních	západní	k2eAgFnPc2d1	západní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
zámek	zámek	k1gInSc1	zámek
Manětín	Manětín	k1gInSc1	Manětín
barokní	barokní	k2eAgFnSc2d1	barokní
sochy	socha	k1gFnSc2	socha
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
poutní	poutní	k2eAgInSc1d1	poutní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Barbory	Barbora	k1gFnSc2	Barbora
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
barokní	barokní	k2eAgInPc1d1	barokní
domy	dům	k1gInPc1	dům
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Jeroným	Jeroným	k1gMnSc1	Jeroným
Brixi	Brixe	k1gFnSc4	Brixe
-	-	kIx~	-
plasský	plasský	k1gMnSc1	plasský
cisterciák	cisterciák	k1gMnSc1	cisterciák
<g/>
,	,	kIx,	,
varhaník	varhaník	k1gMnSc1	varhaník
František	František	k1gMnSc1	František
Wonka	Wonka	k1gMnSc1	Wonka
-	-	kIx~	-
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
manětínský	manětínský	k2eAgMnSc1d1	manětínský
děkan	děkan	k1gMnSc1	děkan
</s>
