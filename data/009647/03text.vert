<p>
<s>
BNP	BNP	kA	BNP
Paribas	Paribas	k1gInSc1	Paribas
Open	Open	k1gNnSc1	Open
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc4	Wells
Masters	Masters	k1gInSc1	Masters
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
společný	společný	k2eAgInSc4d1	společný
tenisový	tenisový	k2eAgInSc4d1	tenisový
turnaj	turnaj	k1gInSc4	turnaj
mužského	mužský	k2eAgInSc2d1	mužský
okruhu	okruh	k1gInSc2	okruh
ATP	atp	kA	atp
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
a	a	k8xC	a
ženského	ženský	k2eAgInSc2d1	ženský
okruhu	okruh	k1gInSc2	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
<g/>
,	,	kIx,	,
hraný	hraný	k2eAgInSc1d1	hraný
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
Tennis	Tennis	k1gFnSc2	Tennis
Garden	Gardna	k1gFnPc2	Gardna
na	na	k7c6	na
otevřených	otevřený	k2eAgInPc6d1	otevřený
dvorcích	dvorec	k1gInPc6	dvorec
s	s	k7c7	s
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
povrchem	povrch	k1gInSc7	povrch
Plexipave	Plexipav	k1gInSc5	Plexipav
<g/>
.	.	kIx.	.
</s>
<s>
Konal	konat	k5eAaImAgInS	konat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
18	[number]	k4	18
<g/>
.	.	kIx.	.
březnem	březen	k1gInSc7	březen
2018	[number]	k4	2018
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
jako	jako	k8xC	jako
43	[number]	k4	43
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
mužského	mužský	k2eAgNnSc2d1	mužské
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
ženského	ženský	k2eAgInSc2d1	ženský
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mužská	mužský	k2eAgFnSc1d1	mužská
polovina	polovina	k1gFnSc1	polovina
se	se	k3xPyFc4	se
po	po	k7c6	po
grandslamu	grandslam	k1gInSc6	grandslam
řadila	řadit	k5eAaImAgFnS	řadit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
kategorie	kategorie	k1gFnSc2	kategorie
okruhu	okruh	k1gInSc2	okruh
ATP	atp	kA	atp
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
Masters	Masters	k1gInSc4	Masters
1000	[number]	k4	1000
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
dotace	dotace	k1gFnSc1	dotace
činila	činit	k5eAaImAgFnS	činit
8	[number]	k4	8
909	[number]	k4	909
960	[number]	k4	960
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Ženská	ženský	k2eAgFnSc1d1	ženská
část	část	k1gFnSc1	část
disponovala	disponovat	k5eAaBmAgFnS	disponovat
rozpočtem	rozpočet	k1gInSc7	rozpočet
8	[number]	k4	8
648	[number]	k4	648
508	[number]	k4	508
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
součástí	součást	k1gFnPc2	součást
druhé	druhý	k4xOgFnSc2	druhý
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
úrovně	úroveň	k1gFnSc2	úroveň
okruhu	okruh	k1gInSc2	okruh
WTA	WTA	kA	WTA
Premier	Premier	k1gInSc1	Premier
Mandatory	Mandator	k1gInPc1	Mandator
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornská	kalifornský	k2eAgFnSc1d1	kalifornská
událost	událost	k1gFnSc1	událost
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
kategoriích	kategorie	k1gFnPc6	kategorie
tradičně	tradičně	k6eAd1	tradičně
představovala	představovat	k5eAaImAgFnS	představovat
úvodní	úvodní	k2eAgInSc4d1	úvodní
turnaj	turnaj	k1gInSc4	turnaj
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýše	vysoce	k6eAd3	vysoce
nasazenými	nasazený	k2eAgFnPc7d1	nasazená
v	v	k7c6	v
singlových	singlový	k2eAgFnPc6d1	singlová
soutěžích	soutěž	k1gFnPc6	soutěž
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
světové	světový	k2eAgFnPc4d1	světová
jedničky	jednička	k1gFnPc4	jednička
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
a	a	k8xC	a
Simona	Simona	k1gFnSc1	Simona
Halepová	Halepová	k1gFnSc1	Halepová
z	z	k7c2	z
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
poslední	poslední	k2eAgMnSc1d1	poslední
přímí	přímit	k5eAaImIp3nS	přímit
účastnici	účastnice	k1gFnSc4	účastnice
do	do	k7c2	do
dvouhry	dvouhra	k1gFnSc2	dvouhra
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
německý	německý	k2eAgMnSc1d1	německý
94	[number]	k4	94
<g/>
.	.	kIx.	.
hráč	hráč	k1gMnSc1	hráč
žebříčku	žebříček	k1gInSc2	žebříček
Maximilian	Maximilian	k1gMnSc1	Maximilian
Marterer	Marterer	k1gMnSc1	Marterer
a	a	k8xC	a
mezi	mezi	k7c7	mezi
ženami	žena	k1gFnPc7	žena
chorvatská	chorvatský	k2eAgFnSc1d1	chorvatská
81	[number]	k4	81
<g/>
.	.	kIx.	.
žena	žena	k1gFnSc1	žena
klasifikace	klasifikace	k1gFnSc1	klasifikace
Petra	Petra	k1gFnSc1	Petra
Martićová	Martićová	k1gFnSc1	Martićová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
výpadku	výpadek	k1gInSc6	výpadek
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
do	do	k7c2	do
turnaje	turnaj	k1gInSc2	turnaj
zasáhnou	zasáhnout	k5eAaPmIp3nP	zasáhnout
bývalé	bývalý	k2eAgFnPc4d1	bývalá
světové	světový	k2eAgFnPc4d1	světová
jedničky	jednička	k1gFnPc4	jednička
Serena	Serena	k1gFnSc1	Serena
Williamsová	Williamsová	k1gFnSc1	Williamsová
a	a	k8xC	a
Viktoria	Viktoria	k1gFnSc1	Viktoria
Azarenková	Azarenkový	k2eAgFnSc1d1	Azarenková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
turnaje	turnaj	k1gInSc2	turnaj
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
organizátoři	organizátor	k1gMnPc1	organizátor
bonus	bonus	k1gInSc4	bonus
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
dolarů	dolar	k1gInPc2	dolar
tenistovi	tenista	k1gMnSc6	tenista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgInSc2	jeden
ročníku	ročník	k1gInSc2	ročník
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
dvouhru	dvouhra	k1gFnSc4	dvouhra
i	i	k8xC	i
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnSc4	první
trofej	trofej	k1gFnSc4	trofej
ze	z	k7c2	z
série	série	k1gFnSc2	série
Masters	Masters	k1gInSc1	Masters
získal	získat	k5eAaPmAgInS	získat
29	[number]	k4	29
<g/>
letý	letý	k2eAgMnSc1d1	letý
Juan	Juan	k1gMnSc1	Juan
Martín	Martína	k1gFnPc2	Martína
del	del	k?	del
Potro	Potro	k1gNnSc4	Potro
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něhož	jenž	k3xRgMnSc4	jenž
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
dvacátá	dvacátý	k4xOgFnSc1	dvacátý
druhá	druhý	k4xOgFnSc1	druhý
singlová	singlový	k2eAgFnSc1d1	singlová
trofej	trofej	k1gFnSc1	trofej
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
Tour	Tour	k1gInSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
přitom	přitom	k6eAd1	přitom
odvrátil	odvrátit	k5eAaPmAgMnS	odvrátit
tři	tři	k4xCgInPc4	tři
mečboly	mečbol	k1gInPc4	mečbol
Rogeru	Roger	k1gInSc2	Roger
Federovi	Feder	k1gMnSc3	Feder
<g/>
.	.	kIx.	.
</s>
<s>
Premiérový	premiérový	k2eAgInSc1d1	premiérový
titul	titul	k1gInSc1	titul
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
WTA	WTA	kA	WTA
Tour	Toura	k1gFnPc2	Toura
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
Japonka	Japonka	k1gFnSc1	Japonka
Naomi	Nao	k1gFnPc7	Nao
Ósakaová	Ósakaová	k1gFnSc1	Ósakaová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
20	[number]	k4	20
letech	léto	k1gNnPc6	léto
stala	stát	k5eAaPmAgFnS	stát
nejmladší	mladý	k2eAgFnSc7d3	nejmladší
šampionkou	šampionka	k1gFnSc7	šampionka
turnaje	turnaj	k1gInSc2	turnaj
Premier	Premira	k1gFnPc2	Premira
Mandatory	Mandator	k1gMnPc4	Mandator
od	od	k7c2	od
výhry	výhra	k1gFnSc2	výhra
Caroline	Carolin	k1gInSc5	Carolin
Wozniacké	Wozniacké	k2eAgFnSc4d1	Wozniacké
na	na	k7c6	na
China	China	k1gFnSc1	China
Open	Open	k1gNnSc1	Open
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Mužského	mužský	k2eAgNnSc2d1	mužské
debla	deblo	k1gNnSc2	deblo
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
americký	americký	k2eAgInSc1d1	americký
pár	pár	k1gInSc1	pár
John	John	k1gMnSc1	John
Isner	Isner	k1gMnSc1	Isner
a	a	k8xC	a
Jack	Jack	k1gMnSc1	Jack
Sock	Sock	k1gMnSc1	Sock
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
členové	člen	k1gMnPc1	člen
si	se	k3xPyFc3	se
připsali	připsat	k5eAaPmAgMnP	připsat
druhou	druhý	k4xOgFnSc4	druhý
společnou	společný	k2eAgFnSc4d1	společná
trofej	trofej	k1gFnSc4	trofej
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
třetí	třetí	k4xOgMnSc1	třetí
ze	z	k7c2	z
série	série	k1gFnSc2	série
Masters	Mastersa	k1gFnPc2	Mastersa
<g/>
.	.	kIx.	.
</s>
<s>
Vítězem	vítěz	k1gMnSc7	vítěz
ženské	ženský	k2eAgFnSc2d1	ženská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
tchajwansko-český	tchajwansko-český	k2eAgInSc1d1	tchajwansko-český
pár	pár	k1gInSc1	pár
Sie	Sie	k1gFnPc2	Sie
Su-wej	Suej	k1gInSc1	Su-wej
a	a	k8xC	a
Barbora	Barbora	k1gFnSc1	Barbora
Strýcová	Strýcová	k1gFnSc1	Strýcová
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
členky	členka	k1gFnPc1	členka
odehrály	odehrát	k5eAaPmAgFnP	odehrát
první	první	k4xOgInSc4	první
společný	společný	k2eAgInSc4d1	společný
turnaj	turnaj	k1gInSc4	turnaj
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
finančních	finanční	k2eAgFnPc2d1	finanční
odměn	odměna	k1gFnPc2	odměna
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
bodů	bod	k1gInPc2	bod
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Finanční	finanční	k2eAgFnPc1d1	finanční
odměny	odměna	k1gFnPc1	odměna
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Dvouhra	dvouhra	k1gFnSc1	dvouhra
mužů	muž	k1gMnPc2	muž
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nasazení	nasazení	k1gNnSc1	nasazení
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgFnPc1d1	jiná
formy	forma	k1gFnPc1	forma
účasti	účast	k1gFnSc2	účast
===	===	k?	===
</s>
</p>
<p>
<s>
Následující	následující	k2eAgMnPc1d1	následující
hráči	hráč	k1gMnPc1	hráč
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Alex	Alex	k1gMnSc1	Alex
de	de	k?	de
Minaur	Minaur	k1gMnSc1	Minaur
</s>
</p>
<p>
<s>
Ernesto	Ernesto	k6eAd1	Ernesto
Escobedo	Escobedo	k1gNnSc1	Escobedo
</s>
</p>
<p>
<s>
Bradley	Bradlea	k1gFnPc1	Bradlea
Klahn	Klahna	k1gFnPc2	Klahna
</s>
</p>
<p>
<s>
Reilly	Reilla	k1gFnPc1	Reilla
Opelka	Opelko	k1gNnSc2	Opelko
</s>
</p>
<p>
<s>
Tennys	Tennys	k6eAd1	Tennys
SandgrenNásledující	SandgrenNásledující	k2eAgMnSc1d1	SandgrenNásledující
hráč	hráč	k1gMnSc1	hráč
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
pod	pod	k7c7	pod
žebříčkovou	žebříčkový	k2eAgFnSc7d1	žebříčková
ochranou	ochrana	k1gFnSc7	ochrana
</s>
</p>
<p>
<s>
Jošihito	Jošihit	k2eAgNnSc1d1	Jošihit
NišiokaNásledující	NišiokaNásledující	k2eAgNnSc1d1	NišiokaNásledující
hráči	hráč	k1gMnPc7	hráč
postoupili	postoupit	k5eAaPmAgMnP	postoupit
z	z	k7c2	z
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Félix	Félix	k1gInSc1	Félix
Auger-Aliassime	Auger-Aliassim	k1gInSc5	Auger-Aliassim
</s>
</p>
<p>
<s>
Marcos	Marcos	k1gInSc1	Marcos
Baghdatis	Baghdatis	k1gFnSc2	Baghdatis
</s>
</p>
<p>
<s>
Ričardas	Ričardas	k1gInSc1	Ričardas
Berankis	Berankis	k1gFnSc2	Berankis
</s>
</p>
<p>
<s>
Juki	Juki	k1gNnSc1	Juki
Bhambri	Bhambr	k1gFnSc2	Bhambr
</s>
</p>
<p>
<s>
Taró	Taró	k?	Taró
Daniel	Daniel	k1gMnSc1	Daniel
</s>
</p>
<p>
<s>
Evan	Evan	k1gMnSc1	Evan
King	King	k1gMnSc1	King
</s>
</p>
<p>
<s>
Mitchell	Mitchell	k1gMnSc1	Mitchell
Krueger	Krueger	k1gMnSc1	Krueger
</s>
</p>
<p>
<s>
Nicolas	Nicolas	k1gMnSc1	Nicolas
Mahut	Mahut	k1gMnSc1	Mahut
</s>
</p>
<p>
<s>
Cameron	Cameron	k1gInSc1	Cameron
Norrie	Norrie	k1gFnSc2	Norrie
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Polansky	Polansky	k1gMnSc1	Polansky
</s>
</p>
<p>
<s>
Vasek	Vasek	k1gMnSc1	Vasek
Pospisil	Pospisil	k1gMnSc1	Pospisil
</s>
</p>
<p>
<s>
Tim	Tim	k?	Tim
SmyczekNásledující	SmyczekNásledující	k2eAgMnPc1d1	SmyczekNásledující
hráči	hráč	k1gMnPc1	hráč
postoupili	postoupit	k5eAaPmAgMnP	postoupit
z	z	k7c2	z
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
šťastní	šťastný	k2eAgMnPc1d1	šťastný
poražení	poražený	k1gMnPc1	poražený
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ruben	ruben	k2eAgInSc1d1	ruben
Bemelmans	Bemelmans	k1gInSc1	Bemelmans
</s>
</p>
<p>
<s>
Matteo	Matteo	k6eAd1	Matteo
Berrettini	Berrettin	k2eAgMnPc1d1	Berrettin
</s>
</p>
<p>
<s>
Dudi	Dudi	k6eAd1	Dudi
Sela	sít	k5eAaImAgFnS	sít
</s>
</p>
<p>
<s>
===	===	k?	===
Odhlášení	odhlášení	k1gNnSc2	odhlášení
===	===	k?	===
</s>
</p>
<p>
<s>
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
turnaje	turnaj	k1gInSc2	turnaj
Aljaž	Aljaž	k1gFnSc1	Aljaž
Bedene	Beden	k1gInSc5	Beden
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Michail	Michail	k1gMnSc1	Michail
Južnyj	Južnyj	k1gMnSc1	Južnyj
</s>
</p>
<p>
<s>
Alexandr	Alexandr	k1gMnSc1	Alexandr
Dolgopolov	Dolgopolovo	k1gNnPc2	Dolgopolovo
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Ivo	Ivo	k1gMnSc1	Ivo
Karlović	Karlović	k1gMnSc1	Karlović
</s>
</p>
<p>
<s>
Guillermo	Guillermo	k6eAd1	Guillermo
García-López	García-López	k1gMnSc1	García-López
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Maximilian	Maximilian	k1gInSc4	Maximilian
Marterer	Marterra	k1gFnPc2	Marterra
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Gasquet	Gasquet	k1gMnSc1	Gasquet
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Lukáš	Lukáš	k1gMnSc1	Lukáš
Lacko	Lacka	k1gFnSc5	Lacka
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Goffin	Goffin	k1gMnSc1	Goffin
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Nicolás	Nicolás	k1gInSc4	Nicolás
Kicker	Kickra	k1gFnPc2	Kickra
</s>
</p>
<p>
<s>
Robin	Robina	k1gFnPc2	Robina
Haase	Haase	k1gFnSc2	Haase
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Radu	Rada	k1gMnSc4	Rada
Albot	Albot	k1gInSc4	Albot
</s>
</p>
<p>
<s>
Denis	Denisa	k1gFnPc2	Denisa
Istomin	Istomin	k1gInSc1	Istomin
→	→	k?	→
nahradil	nahradit	k5eAaPmAgInS	nahradit
jej	on	k3xPp3gNnSc4	on
Dudi	Dudi	k1gNnSc4	Dudi
Sela	sít	k5eAaImAgFnS	sít
</s>
</p>
<p>
<s>
Nick	Nick	k1gMnSc1	Nick
Kyrgios	Kyrgios	k1gMnSc1	Kyrgios
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Matteo	Matteo	k6eAd1	Matteo
Berrettini	Berrettin	k2eAgMnPc1d1	Berrettin
</s>
</p>
<p>
<s>
Paolo	Paolo	k1gNnSc1	Paolo
Lorenzi	Lorenz	k1gMnSc3	Lorenz
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Stefanos	Stefanos	k1gInSc4	Stefanos
Cicipas	Cicipasa	k1gFnPc2	Cicipasa
</s>
</p>
<p>
<s>
Lu	Lu	k?	Lu
Jan-sun	Janun	k1gMnSc1	Jan-sun
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gMnSc4	on
Taylor	Taylor	k1gMnSc1	Taylor
Fritz	Fritz	k1gMnSc1	Fritz
</s>
</p>
<p>
<s>
Florian	Florian	k1gMnSc1	Florian
Mayer	Mayer	k1gMnSc1	Mayer
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Frances	Frances	k1gInSc4	Frances
Tiafoe	Tiafo	k1gFnSc2	Tiafo
</s>
</p>
<p>
<s>
Andy	Anda	k1gFnPc1	Anda
Murray	Murraa	k1gFnSc2	Murraa
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Víctor	Víctor	k1gInSc4	Víctor
Estrella	Estrella	k1gMnSc1	Estrella
Burgos	Burgos	k1gMnSc1	Burgos
</s>
</p>
<p>
<s>
Rafael	Rafael	k1gMnSc1	Rafael
Nadal	nadat	k5eAaPmAgMnS	nadat
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Marius	Marius	k1gInSc4	Marius
Copil	Copila	k1gFnPc2	Copila
</s>
</p>
<p>
<s>
Kei	Kei	k?	Kei
Nišikori	Nišikore	k1gFnSc4	Nišikore
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Ruben	ruben	k2eAgInSc4d1	ruben
Bemelmans	Bemelmans	k1gInSc4	Bemelmans
</s>
</p>
<p>
<s>
Andreas	Andreas	k1gMnSc1	Andreas
Seppi	Sepp	k1gFnSc2	Sepp
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Márton	Márton	k1gInSc4	Márton
Fucsovics	Fucsovicsa	k1gFnPc2	Fucsovicsa
</s>
</p>
<p>
<s>
Jo-Wilfried	Jo-Wilfried	k1gInSc1	Jo-Wilfried
Tsonga	Tsong	k1gMnSc2	Tsong
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gNnSc4	on
Laslo	Laslo	k1gNnSc4	Laslo
Djere	Djer	k1gInSc5	Djer
</s>
</p>
<p>
<s>
Stan	stan	k1gInSc1	stan
Wawrinka	Wawrinka	k1gFnSc1	Wawrinka
→	→	k?	→
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
Jérémy	Jérém	k1gInPc4	Jérém
Chardy	Chard	k1gInPc5	Chard
</s>
</p>
<p>
<s>
===	===	k?	===
Skrečování	skrečování	k1gNnSc2	skrečování
===	===	k?	===
</s>
</p>
<p>
<s>
Nikoloz	Nikoloz	k1gMnSc1	Nikoloz
Basilašvili	Basilašvili	k1gMnSc1	Basilašvili
</s>
</p>
<p>
<s>
Gaël	Gaël	k1gInSc1	Gaël
Monfils	Monfilsa	k1gFnPc2	Monfilsa
</s>
</p>
<p>
<s>
Dominic	Dominice	k1gFnPc2	Dominice
Thiem	Thiem	k1gInSc1	Thiem
</s>
</p>
<p>
<s>
==	==	k?	==
Mužská	mužský	k2eAgFnSc1d1	mužská
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nasazení	nasazení	k1gNnSc1	nasazení
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgFnPc1d1	jiná
formy	forma	k1gFnPc1	forma
účasti	účast	k1gFnSc2	účast
===	===	k?	===
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInPc1d1	následující
páry	pár	k1gInPc1	pár
obdržely	obdržet	k5eAaPmAgInP	obdržet
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInPc1d1	následující
páry	pár	k1gInPc1	pár
nastoupily	nastoupit	k5eAaPmAgInP	nastoupit
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
náhradníka	náhradník	k1gMnSc2	náhradník
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Steve	Steve	k1gMnSc1	Steve
Johnson	Johnson	k1gMnSc1	Johnson
/	/	kIx~	/
Daniel	Daniel	k1gMnSc1	Daniel
Nestor	Nestor	k1gMnSc1	Nestor
</s>
</p>
<p>
<s>
Philipp	Philipp	k1gMnSc1	Philipp
Petzschner	Petzschner	k1gMnSc1	Petzschner
/	/	kIx~	/
Dominic	Dominice	k1gFnPc2	Dominice
Thiem	Thiem	k1gInSc1	Thiem
</s>
</p>
<p>
<s>
==	==	k?	==
Ženská	ženský	k2eAgFnSc1d1	ženská
dvouhra	dvouhra	k1gFnSc1	dvouhra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nasazení	nasazení	k1gNnSc1	nasazení
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgFnPc1d1	jiná
formy	forma	k1gFnPc1	forma
účasti	účast	k1gFnSc2	účast
===	===	k?	===
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnPc1d1	následující
hráčky	hráčka	k1gFnPc1	hráčka
obdržely	obdržet	k5eAaPmAgFnP	obdržet
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Amanda	Amanda	k1gFnSc1	Amanda
Anisimovová	Anisimovová	k1gFnSc1	Anisimovová
</s>
</p>
<p>
<s>
Viktoria	Viktoria	k1gFnSc1	Viktoria
Azarenková	Azarenkový	k2eAgFnSc1d1	Azarenková
</s>
</p>
<p>
<s>
Eugenie	Eugenie	k1gFnSc1	Eugenie
Bouchardová	Bouchardová	k1gFnSc1	Bouchardová
</s>
</p>
<p>
<s>
Danielle	Danielle	k6eAd1	Danielle
Collinsová	Collinsový	k2eAgFnSc1d1	Collinsová
</s>
</p>
<p>
<s>
Kayla	Kayla	k6eAd1	Kayla
Dayová	Dayový	k2eAgFnSc1d1	Dayový
</s>
</p>
<p>
<s>
Caroline	Carolin	k1gInSc5	Carolin
Dolehideová	Dolehideový	k2eAgNnPc4d1	Dolehideový
</s>
</p>
<p>
<s>
Claire	Clair	k1gMnSc5	Clair
Liuová	Liuový	k2eAgFnSc5d1	Liuový
</s>
</p>
<p>
<s>
Sofja	Sofja	k6eAd1	Sofja
ŽukováNásledující	ŽukováNásledující	k2eAgFnPc1d1	ŽukováNásledující
hráčky	hráčka	k1gFnPc1	hráčka
postoupily	postoupit	k5eAaPmAgFnP	postoupit
z	z	k7c2	z
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lara	Lar	k2eAgFnSc1d1	Lara
Arruabarrenová	Arruabarrenová	k1gFnSc1	Arruabarrenová
</s>
</p>
<p>
<s>
Madison	Madison	k1gInSc1	Madison
Brengleová	Brengleová	k1gFnSc1	Brengleová
</s>
</p>
<p>
<s>
Tuan	tuan	k1gMnSc1	tuan
Jing-jing	Jinging	k1gInSc4	Jing-jing
</s>
</p>
<p>
<s>
Sie	Sie	k?	Sie
Su-wej	Suej	k1gInSc1	Su-wej
</s>
</p>
<p>
<s>
Sofia	Sofia	k1gFnSc1	Sofia
Keninová	Keninová	k1gFnSc1	Keninová
</s>
</p>
<p>
<s>
Kurumi	Kuru	k1gFnPc7	Kuru
Narová	Narová	k1gFnSc5	Narová
</s>
</p>
<p>
<s>
Monica	Monic	k2eAgFnSc1d1	Monica
Niculescuová	Niculescuová	k1gFnSc1	Niculescuová
</s>
</p>
<p>
<s>
Sara	Sar	k2eAgFnSc1d1	Sara
Sorribesová	Sorribesový	k2eAgFnSc1d1	Sorribesový
Tormová	Tormová	k1gFnSc1	Tormová
</s>
</p>
<p>
<s>
Taylor	Taylor	k1gInSc1	Taylor
Townsendová	Townsendový	k2eAgFnSc1d1	Townsendová
</s>
</p>
<p>
<s>
Sachia	Sachia	k1gFnSc1	Sachia
Vickeryová	Vickeryová	k1gFnSc1	Vickeryová
</s>
</p>
<p>
<s>
Yanina	Yanin	k2eAgFnSc1d1	Yanina
Wickmayerová	Wickmayerová	k1gFnSc1	Wickmayerová
</s>
</p>
<p>
<s>
Věra	Věra	k1gFnSc1	Věra
Zvonarevová	Zvonarevová	k1gFnSc1	Zvonarevová
</s>
</p>
<p>
<s>
====	====	k?	====
Odhlášení	odhlášení	k1gNnSc2	odhlášení
====	====	k?	====
</s>
</p>
<p>
<s>
před	před	k7c7	před
zahájením	zahájení	k1gNnSc7	zahájení
turnaje	turnaj	k1gInSc2	turnaj
Margarita	Margarita	k1gFnSc1	Margarita
Gasparjanová	Gasparjanová	k1gFnSc1	Gasparjanová
→	→	k?	→
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
Belinda	Belinda	k1gFnSc1	Belinda
Bencicová	Bencicový	k2eAgFnSc1d1	Bencicový
</s>
</p>
<p>
<s>
Camila	Camila	k1gFnSc1	Camila
Giorgiová	Giorgiový	k2eAgFnSc1d1	Giorgiový
→	→	k?	→
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
Pauline	Paulin	k1gInSc5	Paulin
Parmentierová	Parmentierový	k2eAgNnPc1d1	Parmentierový
</s>
</p>
<p>
<s>
Ana	Ana	k?	Ana
Konjuhová	Konjuhová	k1gFnSc1	Konjuhová
→	→	k?	→
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
Kaia	Kai	k2eAgFnSc1d1	Kai
Kanepiová	Kanepiový	k2eAgFnSc1d1	Kanepiová
</s>
</p>
<p>
<s>
Mirjana	Mirjana	k1gFnSc1	Mirjana
Lučićová	Lučićová	k1gFnSc1	Lučićová
Baroniová	Baroniový	k2eAgFnSc1d1	Baroniový
→	→	k?	→
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
Alison	Alison	k1gInSc4	Alison
Van	van	k1gInSc1	van
Uytvancková	Uytvancková	k1gFnSc1	Uytvancková
</s>
</p>
<p>
<s>
Pcheng	Pcheng	k1gMnSc1	Pcheng
Šuaj	Šuaj	k1gFnSc1	Šuaj
→	→	k?	→
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
Verónica	Verónic	k2eAgFnSc1d1	Verónica
Cepedeová	Cepedeová	k1gFnSc1	Cepedeová
Roygová	Roygový	k2eAgFnSc1d1	Roygový
</s>
</p>
<p>
<s>
Lucie	Lucie	k1gFnSc1	Lucie
Šafářová	Šafářová	k1gFnSc1	Šafářová
→	→	k?	→
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
Petra	Petra	k1gFnSc1	Petra
Martićová	Martićový	k2eAgFnSc1d1	Martićový
</s>
</p>
<p>
<s>
Laura	Laura	k1gFnSc1	Laura
Siegemundová	Siegemundový	k2eAgFnSc1d1	Siegemundový
→	→	k?	→
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ji	on	k3xPp3gFnSc4	on
Lauren	Laurno	k1gNnPc2	Laurno
Davisová	Davisový	k2eAgNnPc4d1	Davisové
</s>
</p>
<p>
<s>
===	===	k?	===
Skrečování	skrečování	k1gNnSc2	skrečování
===	===	k?	===
</s>
</p>
<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Siniaková	Siniakový	k2eAgFnSc1d1	Siniaková
</s>
</p>
<p>
<s>
Carina	Carin	k2eAgFnSc1d1	Carina
Witthöftová	Witthöftová	k1gFnSc1	Witthöftová
</s>
</p>
<p>
<s>
==	==	k?	==
Ženská	ženský	k2eAgFnSc1d1	ženská
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nasazení	nasazení	k1gNnSc1	nasazení
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Jiné	jiný	k2eAgFnPc1d1	jiná
formy	forma	k1gFnPc1	forma
účasti	účast	k1gFnSc2	účast
===	===	k?	===
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInPc1d1	následující
páry	pár	k1gInPc1	pár
obdržely	obdržet	k5eAaPmAgInP	obdržet
divokou	divoký	k2eAgFnSc4d1	divoká
kartu	karta	k1gFnSc4	karta
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Viktoria	Viktoria	k1gFnSc1	Viktoria
Azarenková	Azarenkový	k2eAgFnSc1d1	Azarenková
/	/	kIx~	/
Aryna	Aryen	k2eAgFnSc1d1	Aryen
Sabalenková	Sabalenkový	k2eAgFnSc1d1	Sabalenkový
</s>
</p>
<p>
<s>
Eugenie	Eugenie	k1gFnSc1	Eugenie
Bouchardová	Bouchardová	k1gFnSc1	Bouchardová
/	/	kIx~	/
Sloane	Sloan	k1gInSc5	Sloan
Stephensová	Stephensová	k1gFnSc5	Stephensová
</s>
</p>
<p>
<s>
Karolína	Karolína	k1gFnSc1	Karolína
Plíšková	plíškový	k2eAgFnSc1d1	Plíšková
/	/	kIx~	/
Kristýna	Kristýna	k1gFnSc1	Kristýna
Plíšková	plíškový	k2eAgFnSc1d1	Plíšková
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
finále	finále	k1gNnSc2	finále
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mužská	mužský	k2eAgFnSc1d1	mužská
dvouhra	dvouhra	k1gFnSc1	dvouhra
===	===	k?	===
</s>
</p>
<p>
<s>
Juan	Juan	k1gMnSc1	Juan
Martín	Martín	k1gMnSc1	Martín
del	del	k?	del
Potro	Potro	k1gNnSc4	Potro
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gMnSc1	Roger
Federer	Federer	k1gMnSc1	Federer
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Ženská	ženský	k2eAgFnSc1d1	ženská
dvouhra	dvouhra	k1gFnSc1	dvouhra
===	===	k?	===
</s>
</p>
<p>
<s>
Naomi	Nao	k1gFnPc7	Nao
Ósakaová	Ósakaový	k2eAgNnPc4d1	Ósakaový
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Darja	Darja	k1gFnSc1	Darja
Kasatkinová	Kasatkinová	k1gFnSc1	Kasatkinová
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
===	===	k?	===
Mužská	mužský	k2eAgFnSc1d1	mužská
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
===	===	k?	===
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Isner	Isner	k1gMnSc1	Isner
/	/	kIx~	/
Jack	Jack	k1gMnSc1	Jack
Sock	Sock	k1gMnSc1	Sock
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Bob	Bob	k1gMnSc1	Bob
Bryan	Bryan	k1gMnSc1	Bryan
/	/	kIx~	/
Mike	Mike	k1gFnSc1	Mike
Bryan	Bryany	k1gInPc2	Bryany
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Ženská	ženský	k2eAgFnSc1d1	ženská
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
===	===	k?	===
</s>
</p>
<p>
<s>
Sie	Sie	k?	Sie
Su-wej	Suej	k1gInSc1	Su-wej
/	/	kIx~	/
Barbora	Barbora	k1gFnSc1	Barbora
Strýcová	Strýcová	k1gFnSc1	Strýcová
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
Jekatěrina	Jekatěrin	k2eAgFnSc1d1	Jekatěrina
Makarovová	Makarovová	k1gFnSc1	Makarovová
/	/	kIx~	/
Jelena	Jelena	k1gFnSc1	Jelena
Vesninová	Vesninový	k2eAgFnSc1d1	Vesninová
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
2018	[number]	k4	2018
BNP	BNP	kA	BNP
Paribas	Paribas	k1gMnSc1	Paribas
Open	Open	k1gMnSc1	Open
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
