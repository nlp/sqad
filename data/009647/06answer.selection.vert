<s>
BNP	BNP	kA	BNP
Paribas	Paribas	k1gInSc1	Paribas
Open	Open	k1gNnSc1	Open
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wells	k1gInSc4	Wells
Masters	Masters	k1gInSc1	Masters
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
společný	společný	k2eAgInSc4d1	společný
tenisový	tenisový	k2eAgInSc4d1	tenisový
turnaj	turnaj	k1gInSc4	turnaj
mužského	mužský	k2eAgInSc2d1	mužský
okruhu	okruh	k1gInSc2	okruh
ATP	atp	kA	atp
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
a	a	k8xC	a
ženského	ženský	k2eAgInSc2d1	ženský
okruhu	okruh	k1gInSc2	okruh
WTA	WTA	kA	WTA
Tour	Tour	k1gInSc1	Tour
<g/>
,	,	kIx,	,
hraný	hraný	k2eAgInSc1d1	hraný
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Indian	Indiana	k1gFnPc2	Indiana
Wells	Wellsa	k1gFnPc2	Wellsa
Tennis	Tennis	k1gFnSc2	Tennis
Garden	Gardna	k1gFnPc2	Gardna
na	na	k7c6	na
otevřených	otevřený	k2eAgInPc6d1	otevřený
dvorcích	dvorec	k1gInPc6	dvorec
s	s	k7c7	s
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
povrchem	povrch	k1gInSc7	povrch
Plexipave	Plexipav	k1gInSc5	Plexipav
<g/>
.	.	kIx.	.
</s>
