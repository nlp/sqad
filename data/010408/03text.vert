<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1083	[number]	k4	1083
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1150	[number]	k4	1150
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Henricus	Henricus	k1gMnSc1	Henricus
Sdiko	Sdiko	k1gNnSc1	Sdiko
<g/>
,	,	kIx,	,
Zdico	Zdico	k1gNnSc1	Zdico
<g/>
,	,	kIx,	,
či	či	k8xC	či
Sdico	Sdico	k6eAd1	Sdico
<g/>
,	,	kIx,	,
původním	původní	k2eAgInSc7d1	původní
"	"	kIx"	"
<g/>
barbarským	barbarský	k2eAgMnSc7d1	barbarský
<g/>
"	"	kIx"	"
jménem	jméno	k1gNnSc7	jméno
Zdík	Zdík	k1gMnSc1	Zdík
a	a	k8xC	a
od	od	k7c2	od
svěcení	svěcení	k1gNnSc2	svěcení
za	za	k7c4	za
biskupa	biskup	k1gMnSc4	biskup
Henricus	Henricus	k1gMnSc1	Henricus
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
strýc	strýc	k1gMnSc1	strýc
pražského	pražský	k2eAgMnSc2d1	pražský
biskupa	biskup	k1gMnSc2	biskup
Daniela	Daniel	k1gMnSc2	Daniel
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
biskupa	biskup	k1gMnSc2	biskup
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
původu	původ	k1gInSc6	původ
dosud	dosud	k6eAd1	dosud
panují	panovat	k5eAaImIp3nP	panovat
mezi	mezi	k7c4	mezi
odborníky	odborník	k1gMnPc4	odborník
dohady	dohad	k1gInPc7	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
syna	syn	k1gMnSc4	syn
některého	některý	k3yIgMnSc4	některý
člena	člen	k1gMnSc4	člen
pražské	pražský	k2eAgFnSc2d1	Pražská
dómské	dómský	k2eAgFnSc2d1	Dómská
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejspíše	nejspíše	k9	nejspíše
kronikáře	kronikář	k1gMnSc2	kronikář
Kosmy	Kosma	k1gMnSc2	Kosma
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Božetěchy	Božetěch	k1gMnPc4	Božetěch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
málo	málo	k1gNnSc4	málo
pravděpodobné	pravděpodobný	k2eAgNnSc4d1	pravděpodobné
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
jeví	jevit	k5eAaImIp3nS	jevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pocházel	pocházet	k5eAaImAgMnS	pocházet
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možnou	možný	k2eAgFnSc7d1	možná
verzí	verze	k1gFnSc7	verze
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
byl	být	k5eAaImAgMnS	být
Magnus	Magnus	k1gMnSc1	Magnus
<g/>
,	,	kIx,	,
kronikář	kronikář	k1gMnSc1	kronikář
a	a	k8xC	a
kanovník	kanovník	k1gMnSc1	kanovník
pražské	pražský	k2eAgFnSc2d1	Pražská
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
,	,	kIx,	,
Kosmův	Kosmův	k2eAgInSc4d1	Kosmův
druh	druh	k1gInSc4	druh
v	v	k7c6	v
kapitule	kapitula	k1gFnSc6	kapitula
a	a	k8xC	a
následovník	následovník	k1gMnSc1	následovník
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
možná	možná	k9	možná
syn	syn	k1gMnSc1	syn
<g/>
)	)	kIx)	)
probošta	probošt	k1gMnSc4	probošt
a	a	k8xC	a
kancléře	kancléř	k1gMnSc4	kancléř
Juraty	Jurata	k1gFnSc2	Jurata
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
vzdělání	vzdělání	k1gNnSc4	vzdělání
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1123	[number]	k4	1123
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1126	[number]	k4	1126
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
biskupem	biskup	k1gMnSc7	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Vysvěcen	vysvěcen	k2eAgMnSc1d1	vysvěcen
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1126	[number]	k4	1126
ve	v	k7c6	v
Wormsu	Worms	k1gInSc6	Worms
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1130	[number]	k4	1130
mu	on	k3xPp3gMnSc3	on
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
údělný	údělný	k2eAgMnSc1d1	údělný
kníže	kníže	k1gMnSc1	kníže
Václav	Václav	k1gMnSc1	Václav
Jindřich	Jindřich	k1gMnSc1	Jindřich
svěřil	svěřit	k5eAaPmAgMnS	svěřit
dostavbu	dostavba	k1gFnSc4	dostavba
chrámu	chrám	k1gInSc2	chrám
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xS	jako
nedostavěnou	dostavěný	k2eNgFnSc4d1	nedostavěná
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1131	[number]	k4	1131
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1140	[number]	k4	1140
kostel	kostel	k1gInSc1	kostel
dokončil	dokončit	k5eAaPmAgInS	dokončit
a	a	k8xC	a
založil	založit	k5eAaPmAgInS	založit
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
novou	nový	k2eAgFnSc4d1	nová
dvanáctičlennou	dvanáctičlenný	k2eAgFnSc4d1	dvanáctičlenná
kapitulu	kapitula	k1gFnSc4	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1141	[number]	k4	1141
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
udělal	udělat	k5eAaPmAgMnS	udělat
katedrálu	katedrál	k1gMnSc3	katedrál
<g/>
,	,	kIx,	,
když	když	k8xS	když
přenesl	přenést	k5eAaPmAgInS	přenést
sídlo	sídlo	k1gNnSc4	sídlo
biskupství	biskupství	k1gNnSc2	biskupství
od	od	k7c2	od
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
postavit	postavit	k5eAaPmF	postavit
románský	románský	k2eAgInSc1d1	románský
biskupský	biskupský	k2eAgInSc1d1	biskupský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Zdíkův	Zdíkův	k2eAgInSc1d1	Zdíkův
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
a	a	k8xC	a
dům	dům	k1gInSc1	dům
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
také	také	k9	také
vysvětil	vysvětit	k5eAaPmAgMnS	vysvětit
kostel	kostel	k1gInSc4	kostel
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Říp	Říp	k1gInSc4	Říp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
reformu	reforma	k1gFnSc4	reforma
majetku	majetek	k1gInSc2	majetek
a	a	k8xC	a
organizace	organizace	k1gFnSc2	organizace
církve	církev	k1gFnSc2	církev
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vytvoření	vytvoření	k1gNnSc3	vytvoření
kapituly	kapitula	k1gFnSc2	kapitula
(	(	kIx(	(
<g/>
skládala	skládat	k5eAaImAgFnS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
správců	správce	k1gMnPc2	správce
bývalých	bývalý	k2eAgInPc2d1	bývalý
hradských	hradský	k2eAgInPc2d1	hradský
kostelů	kostel	k1gInPc2	kostel
<g/>
)	)	kIx)	)
rozmnožil	rozmnožit	k5eAaPmAgInS	rozmnožit
majetky	majetek	k1gInPc4	majetek
olomouckého	olomoucký	k2eAgNnSc2d1	olomoucké
biskupství	biskupství	k1gNnSc2	biskupství
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
některé	některý	k3yIgFnPc1	některý
enklávy	enkláva	k1gFnPc1	enkláva
pražského	pražský	k2eAgNnSc2d1	Pražské
biskupství	biskupství	k1gNnSc2	biskupství
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovitě	jmenovitě	k6eAd1	jmenovitě
Podivín	podivín	k1gMnSc1	podivín
i	i	k9	i
s	s	k7c7	s
mincovním	mincovní	k2eAgNnSc7d1	mincovní
právem	právo	k1gNnSc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
církve	církev	k1gFnSc2	církev
na	na	k7c4	na
světské	světský	k2eAgFnPc4d1	světská
moci	moc	k1gFnPc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
dosažení	dosažení	k1gNnSc4	dosažení
viděl	vidět	k5eAaImAgMnS	vidět
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
knížetem	kníže	k1gMnSc7	kníže
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
podporoval	podporovat	k5eAaImAgMnS	podporovat
českého	český	k2eAgMnSc4d1	český
knížete	kníže	k1gMnSc4	kníže
Soběslava	Soběslav	k1gMnSc2	Soběslav
I.	I.	kA	I.
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jeho	jeho	k3xOp3gNnSc4	jeho
nástupce	nástupce	k1gMnSc1	nástupce
knížete	kníže	k1gMnSc2	kníže
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Prosazoval	prosazovat	k5eAaImAgInS	prosazovat
také	také	k9	také
aktivně	aktivně	k6eAd1	aktivně
celibát	celibát	k1gInSc4	celibát
pro	pro	k7c4	pro
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozpory	rozpor	k1gInPc1	rozpor
s	s	k7c7	s
moravskými	moravský	k2eAgMnPc7d1	moravský
údělnými	údělný	k2eAgMnPc7d1	údělný
knížaty	kníže	k1gMnPc7wR	kníže
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1136	[number]	k4	1136
se	se	k3xPyFc4	se
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
sporu	spor	k1gInSc2	spor
s	s	k7c7	s
brněnským	brněnský	k2eAgMnSc7d1	brněnský
údělným	údělný	k2eAgMnSc7d1	údělný
knížetem	kníže	k1gMnSc7	kníže
Vratislavem	Vratislav	k1gMnSc7	Vratislav
o	o	k7c4	o
statky	statek	k1gInPc4	statek
v	v	k7c6	v
Blansku	Blansko	k1gNnSc6	Blansko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1137	[number]	k4	1137
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
vykonal	vykonat	k5eAaPmAgInS	vykonat
druhou	druhý	k4xOgFnSc4	druhý
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
si	se	k3xPyFc3	se
dovezl	dovézt	k5eAaPmAgInS	dovézt
relikvii	relikvie	k1gFnSc4	relikvie
sv.	sv.	kA	sv.
Kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1139	[number]	k4	1139
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Druhého	druhý	k4xOgInSc2	druhý
lateránského	lateránský	k2eAgInSc2d1	lateránský
koncilu	koncil	k1gInSc2	koncil
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
českého	český	k2eAgMnSc2d1	český
knížete	kníže	k1gMnSc2	kníže
Soběslava	Soběslav	k1gMnSc2	Soběslav
I.	I.	kA	I.
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
za	za	k7c2	za
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
podporoval	podporovat	k5eAaImAgMnS	podporovat
ho	on	k3xPp3gNnSc4	on
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
boji	boj	k1gInSc6	boj
o	o	k7c4	o
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
s	s	k7c7	s
moravskými	moravský	k2eAgMnPc7d1	moravský
údělnými	údělný	k2eAgMnPc7d1	údělný
knížaty	kníže	k1gMnPc7wR	kníže
(	(	kIx(	(
<g/>
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
opírali	opírat	k5eAaImAgMnP	opírat
o	o	k7c4	o
seniorátní	seniorátní	k2eAgNnSc4d1	seniorátní
právo	právo	k1gNnSc4	právo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1142	[number]	k4	1142
dal	dát	k5eAaPmAgMnS	dát
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
údělníky	údělník	k1gMnPc4	údělník
do	do	k7c2	do
klatby	klatba	k1gFnSc2	klatba
a	a	k8xC	a
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
nad	nad	k7c7	nad
diecézí	diecéze	k1gFnSc7	diecéze
interdikt	interdikt	k1gInSc1	interdikt
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
však	však	k9	však
pokusil	pokusit	k5eAaPmAgMnS	pokusit
údělníky	údělník	k1gMnPc4	údělník
odradit	odradit	k5eAaPmF	odradit
od	od	k7c2	od
vystoupení	vystoupení	k1gNnPc2	vystoupení
proti	proti	k7c3	proti
Vladislavovi	Vladislav	k1gMnSc3	Vladislav
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Vladislava	Vladislav	k1gMnSc2	Vladislav
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Vysoké	vysoká	k1gFnSc2	vysoká
ho	on	k3xPp3gMnSc4	on
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
následoval	následovat	k5eAaImAgMnS	následovat
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
úspěšně	úspěšně	k6eAd1	úspěšně
žádali	žádat	k5eAaImAgMnP	žádat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Lothara	Lothar	k1gMnSc2	Lothar
III	III	kA	III
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1144	[number]	k4	1144
se	se	k3xPyFc4	se
navrátil	navrátit	k5eAaPmAgMnS	navrátit
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Konráda	Konrád	k1gMnSc2	Konrád
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
od	od	k7c2	od
knížete	kníže	k1gMnSc2	kníže
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
vymohl	vymoct	k5eAaPmAgMnS	vymoct
tzv.	tzv.	kA	tzv.
imunitu	imunita	k1gFnSc4	imunita
(	(	kIx(	(
<g/>
poddaní	poddaný	k1gMnPc1	poddaný
biskupa	biskup	k1gInSc2	biskup
byli	být	k5eAaImAgMnP	být
vyjmuti	vyjmout	k5eAaPmNgMnP	vyjmout
z	z	k7c2	z
pravomocí	pravomoc	k1gFnPc2	pravomoc
údělníků	údělník	k1gMnPc2	údělník
a	a	k8xC	a
zbaveni	zbavit	k5eAaPmNgMnP	zbavit
zemských	zemský	k2eAgFnPc2d1	zemská
povinností	povinnost	k1gFnSc7	povinnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
ražby	ražba	k1gFnSc2	ražba
mincí	mince	k1gFnPc2	mince
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
imunitě	imunita	k1gFnSc3	imunita
se	se	k3xPyFc4	se
Olomoucké	olomoucký	k2eAgNnSc1d1	olomoucké
biskupství	biskupství	k1gNnSc1	biskupství
stalo	stát	k5eAaPmAgNnS	stát
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
samostatným	samostatný	k2eAgInSc7d1	samostatný
církevně-politickým	církevněolitický	k2eAgInSc7d1	církevně-politický
útvarem	útvar	k1gInSc7	útvar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napříště	napříště	k6eAd1	napříště
nespadal	spadat	k5eNaImAgInS	spadat
pod	pod	k7c4	pod
moc	moc	k1gFnSc4	moc
moravských	moravský	k2eAgNnPc2d1	Moravské
údělných	údělný	k2eAgNnPc2d1	údělné
knížat	kníže	k1gNnPc2	kníže
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1145	[number]	k4	1145
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
u	u	k7c2	u
Úsobrna	Úsobrno	k1gNnSc2	Úsobrno
přepaden	přepadnout	k5eAaPmNgMnS	přepadnout
brněnským	brněnský	k2eAgMnSc7d1	brněnský
údělníkem	údělník	k1gMnSc7	údělník
Vratislavem	Vratislav	k1gMnSc7	Vratislav
<g/>
,	,	kIx,	,
znojemským	znojemský	k2eAgMnSc7d1	znojemský
Konrádem	Konrád	k1gMnSc7	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
přemyslovcem	přemyslovec	k1gMnSc7	přemyslovec
Děpoltem	Děpolt	k1gInSc7	Děpolt
(	(	kIx(	(
<g/>
bratr	bratr	k1gMnSc1	bratr
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
Zdíkovi	Zdík	k1gMnSc3	Zdík
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
prchnout	prchnout	k5eAaPmF	prchnout
a	a	k8xC	a
ukrýt	ukrýt	k5eAaPmF	ukrýt
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1146	[number]	k4	1146
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
byl	být	k5eAaImAgMnS	být
zprostředkovatelem	zprostředkovatel	k1gMnSc7	zprostředkovatel
mezi	mezi	k7c7	mezi
znepřátelenými	znepřátelený	k2eAgMnPc7d1	znepřátelený
polskými	polský	k2eAgMnPc7d1	polský
knížaty	kníže	k1gMnPc7wR	kníže
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
papeže	papež	k1gMnSc2	papež
a	a	k8xC	a
krále	král	k1gMnSc2	král
Konráda	Konrád	k1gMnSc2	Konrád
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1147	[number]	k4	1147
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
účastníkem	účastník	k1gMnSc7	účastník
neúspěšné	úspěšný	k2eNgFnSc2d1	neúspěšná
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
proti	proti	k7c3	proti
polabským	polabský	k2eAgInPc3d1	polabský
Slovanům	Slovan	k1gInPc3	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
Vladislava	Vladislav	k1gMnSc4	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
křížové	křížový	k2eAgFnSc6d1	křížová
výpravě	výprava	k1gFnSc6	výprava
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zakládání	zakládání	k1gNnPc1	zakládání
klášterů	klášter	k1gInPc2	klášter
a	a	k8xC	a
osobnost	osobnost	k1gFnSc1	osobnost
biskupa	biskup	k1gMnSc2	biskup
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
významným	významný	k2eAgInPc3d1	významný
počinům	počin	k1gInPc3	počin
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přivedl	přivést	k5eAaPmAgMnS	přivést
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
nový	nový	k2eAgInSc1d1	nový
řeholní	řeholní	k2eAgInSc1d1	řeholní
řád	řád	k1gInSc1	řád
premonstrátů	premonstrát	k1gMnPc2	premonstrát
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vždy	vždy	k6eAd1	vždy
upřednostňoval	upřednostňovat	k5eAaImAgMnS	upřednostňovat
před	před	k7c7	před
benediktiny	benediktin	k1gMnPc7	benediktin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1142	[number]	k4	1142
<g/>
–	–	k?	–
<g/>
1143	[number]	k4	1143
založil	založit	k5eAaPmAgInS	založit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Vladislavem	Vladislav	k1gMnSc7	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
nejstarší	starý	k2eAgNnSc4d3	nejstarší
sídlo	sídlo	k1gNnSc4	sídlo
premonstrátů	premonstrát	k1gMnPc2	premonstrát
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
–	–	k?	–
Strahovský	strahovský	k2eAgInSc4d1	strahovský
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
premonstrátský	premonstrátský	k2eAgInSc1d1	premonstrátský
klášter	klášter	k1gInSc1	klášter
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
podporoval	podporovat	k5eAaImAgInS	podporovat
premonstráty	premonstrát	k1gMnPc4	premonstrát
v	v	k7c6	v
Želivě	Želiv	k1gInSc6	Želiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
kapitulní	kapitulní	k2eAgFnSc4d1	kapitulní
knihovnu	knihovna	k1gFnSc4	knihovna
a	a	k8xC	a
písárnu	písárna	k1gFnSc4	písárna
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
skriptorium	skriptorium	k1gNnSc1	skriptorium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
dílně	dílna	k1gFnSc6	dílna
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kodex	kodex	k1gInSc1	kodex
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zvané	zvaný	k2eAgNnSc1d1	zvané
Olomoucké	olomoucký	k2eAgNnSc1d1	olomoucké
horologium	horologium	k1gNnSc1	horologium
nebo	nebo	k8xC	nebo
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kolektář	kolektář	k1gInSc1	kolektář
<g/>
,	,	kIx,	,
a	a	k8xC	a
biskupovy	biskupův	k2eAgFnSc2d1	biskupova
listiny	listina	k1gFnSc2	listina
<g/>
.	.	kIx.	.
<g/>
Přátelil	přátelit	k5eAaImAgMnS	přátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
významnými	významný	k2eAgFnPc7d1	významná
muži	muž	k1gMnPc1	muž
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
Bernardem	Bernard	k1gMnSc7	Bernard
z	z	k7c2	z
Clairvaux	Clairvaux	k1gInSc4	Clairvaux
<g/>
.	.	kIx.	.
</s>
<s>
Dopisoval	dopisovat	k5eAaImAgMnS	dopisovat
si	se	k3xPyFc3	se
i	i	k9	i
s	s	k7c7	s
několika	několik	k4yIc7	několik
papeži	papež	k1gMnPc7	papež
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
mužem	muž	k1gMnSc7	muž
velmi	velmi	k6eAd1	velmi
vzdělaným	vzdělaný	k2eAgMnPc3d1	vzdělaný
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
asketickým	asketický	k2eAgNnSc7d1	asketické
<g/>
.	.	kIx.	.
</s>
<s>
Zavázal	Zavázal	k1gMnSc1	Zavázal
se	se	k3xPyFc4	se
řeholi	řehole	k1gFnSc4	řehole
sv.	sv.	kA	sv.
Augustina	Augustin	k1gMnSc2	Augustin
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
nejedl	jíst	k5eNaImAgInS	jíst
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
dodržoval	dodržovat	k5eAaImAgMnS	dodržovat
mlčenlivost	mlčenlivost	k1gFnSc4	mlčenlivost
<g/>
.	.	kIx.	.
</s>
<s>
Biskup	biskup	k1gMnSc1	biskup
Zdík	Zdík	k1gMnSc1	Zdík
zemřel	zemřít	k5eAaPmAgMnS	zemřít
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1150	[number]	k4	1150
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
výslovné	výslovný	k2eAgNnSc4d1	výslovné
přání	přání	k1gNnSc4	přání
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
ve	v	k7c6	v
Strahovském	strahovský	k2eAgInSc6d1	strahovský
klášteře	klášter	k1gInSc6	klášter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
nástupcem	nástupce	k1gMnSc7	nástupce
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
biskupa	biskup	k1gInSc2	biskup
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Založení	založení	k1gNnSc1	založení
Strahova	Strahov	k1gInSc2	Strahov
dodnes	dodnes	k6eAd1	dodnes
připomíná	připomínat	k5eAaImIp3nS	připomínat
barokní	barokní	k2eAgInSc1d1	barokní
epitaf	epitaf	k1gInSc1	epitaf
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
uvedeni	uveden	k2eAgMnPc1d1	uveden
zakladatelé	zakladatel	k1gMnPc1	zakladatel
kláštera	klášter	k1gInSc2	klášter
–	–	k?	–
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
biskup	biskup	k1gMnSc1	biskup
Jan	Jan	k1gMnSc1	Jan
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Zdíkem	Zdík	k1gMnSc7	Zdík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BISTŘICKÝ	BISTŘICKÝ	kA	BISTŘICKÝ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
reformy	reforma	k1gFnSc2	reforma
na	na	k7c6	na
olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
stolci	stolec	k1gInSc6	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Libor	Libor	k1gMnSc1	Libor
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Drahoš	Drahoš	k1gMnSc1	Drahoš
<g/>
.	.	kIx.	.
</s>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
moravských	moravský	k2eAgFnPc2d1	Moravská
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86488	[number]	k4	86488
<g/>
-	-	kIx~	-
<g/>
38	[number]	k4	38
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
27	[number]	k4	27
<g/>
-	-	kIx~	-
<g/>
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BISTŘICKÝ	BISTŘICKÝ	kA	BISTŘICKÝ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Papežské	papežský	k2eAgFnSc2d1	Papežská
listy	lista	k1gFnSc2	lista
Jindřichu	Jindřich	k1gMnSc3	Jindřich
Zdíkovi	Zdík	k1gMnSc3	Zdík
<g/>
.	.	kIx.	.
</s>
<s>
Vlastivědný	vlastivědný	k2eAgMnSc1d1	vlastivědný
věstník	věstník	k1gMnSc1	věstník
moravský	moravský	k2eAgMnSc1d1	moravský
<g/>
.	.	kIx.	.
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
42	[number]	k4	42
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
225	[number]	k4	225
<g/>
-	-	kIx~	-
<g/>
226	[number]	k4	226
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
323	[number]	k4	323
<g/>
-	-	kIx~	-
<g/>
2581	[number]	k4	2581
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BISTŘICKÝ	BISTŘICKÝ	kA	BISTŘICKÝ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Písemnosti	písemnost	k1gFnPc1	písemnost
olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
biskupa	biskup	k1gInSc2	biskup
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Zdíka	Zdík	k1gMnSc4	Zdík
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
archivních	archivní	k2eAgFnPc2d1	archivní
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
33	[number]	k4	33
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
32	[number]	k4	32
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g/>
5246	[number]	k4	5246
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BISTŘICKÝ	BISTŘICKÝ	kA	BISTŘICKÝ
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Zdíkovy	Zdíkův	k2eAgFnPc4d1	Zdíkova
listiny	listina	k1gFnPc4	listina
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
96	[number]	k4	96
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
292	[number]	k4	292
<g/>
-	-	kIx~	-
<g/>
306	[number]	k4	306
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
6111	[number]	k4	6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BOLINA	BOLINA	kA	BOLINA
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
přepaden	přepaden	k2eAgMnSc1d1	přepaden
biskup	biskup	k1gMnSc1	biskup
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
roku	rok	k1gInSc2	rok
1145	[number]	k4	1145
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Příspěvek	příspěvek	k1gInSc1	příspěvek
k	k	k7c3	k
historii	historie	k1gFnSc3	historie
moravsko-českého	moravsko-český	k2eAgNnSc2d1	moravsko-český
pomezí	pomezí	k1gNnSc2	pomezí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Matice	matice	k1gFnSc2	matice
moravské	moravský	k2eAgFnSc2d1	Moravská
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
122	[number]	k4	122
<g/>
,	,	kIx,	,
s.	s.	k?	s.
343	[number]	k4	343
<g/>
-	-	kIx~	-
<g/>
373	[number]	k4	373
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
323	[number]	k4	323
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
52	[number]	k4	52
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
BOLINA	BOLINA	kA	BOLINA
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
interpretaci	interpretace	k1gFnSc3	interpretace
a	a	k8xC	a
datování	datování	k1gNnSc3	datování
Zdíkových	Zdíkův	k2eAgFnPc2d1	Zdíkova
listin	listina	k1gFnPc2	listina
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
časopis	časopis	k1gInSc1	časopis
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
97	[number]	k4	97
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
273	[number]	k4	273
<g/>
-	-	kIx~	-
<g/>
292	[number]	k4	292
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
6111	[number]	k4	6111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČAPKA	Čapka	k1gMnSc1	Čapka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Moravy	Morava	k1gFnSc2	Morava
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Cerm	Cerm	k1gMnSc1	Cerm
–	–	k?	–
Akademické	akademický	k2eAgNnSc1d1	akademické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
216	[number]	k4	216
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7204	[number]	k4	7204
<g/>
-	-	kIx~	-
<g/>
219	[number]	k4	219
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
DOHNAL	Dohnal	k1gMnSc1	Dohnal
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
;	;	kIx,	;
POJSL	POJSL	kA	POJSL
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
v	v	k7c6	v
době	doba	k1gFnSc6	doba
biskupa	biskup	k1gMnSc2	biskup
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Zdíka	Zdík	k1gMnSc2	Zdík
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
cyrilometodějská	cyrilometodějský	k2eAgFnSc1d1	Cyrilometodějská
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
80	[number]	k4	80
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
238	[number]	k4	238
<g/>
-	-	kIx~	-
<g/>
1832	[number]	k4	1832
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HRBÁČOVÁ	HRBÁČOVÁ	kA	HRBÁČOVÁ
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
(	(	kIx(	(
<g/>
1126	[number]	k4	1126
<g/>
-	-	kIx~	-
<g/>
1150	[number]	k4	1150
<g/>
)	)	kIx)	)
:	:	kIx,	:
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
uprostřed	uprostřed	k7c2	uprostřed
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Muzeum	muzeum	k1gNnSc1	muzeum
umění	umění	k1gNnSc2	umění
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
247	[number]	k4	247
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87149	[number]	k4	87149
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
do	do	k7c2	do
Přemysla	Přemysl	k1gMnSc2	Přemysl
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
1214	[number]	k4	1214
s.	s.	k?	s.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
906	[number]	k4	906
<g/>
–	–	k?	–
<g/>
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
464	[number]	k4	464
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
563	[number]	k4	563
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Čechy	Čechy	k1gFnPc1	Čechy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
1034	[number]	k4	1034
<g/>
–	–	k?	–
<g/>
1198	[number]	k4	1198
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
712	[number]	k4	712
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
905	[number]	k4	905
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jindřich	Jindřich	k1gMnSc1	Jindřich
Zdík	Zdík	k1gMnSc1	Zdík
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jindřich	Jindřich	k1gMnSc1	Jindřich
ZdíkVýstavě	ZdíkVýstava	k1gFnSc3	ZdíkVýstava
kraluje	kralovat	k5eAaImIp3nS	kralovat
800	[number]	k4	800
let	léto	k1gNnPc2	léto
starý	starý	k2eAgInSc1d1	starý
spis	spis	k1gInSc1	spis
–	–	k?	–
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
25	[number]	k4	25
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Zdíkova	Zdíkův	k2eAgFnSc1d1	Zdíkova
pečeť	pečeť	k1gFnSc1	pečeť
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Zdíka	Zdík	k1gMnSc2	Zdík
</s>
</p>
