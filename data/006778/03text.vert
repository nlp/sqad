<s>
Miley	Mile	k2eAgFnPc1d1	Mile
Ray	Ray	k1gFnPc1	Ray
Cyrusová	Cyrusový	k2eAgFnSc1d1	Cyrusová
<g/>
,	,	kIx,	,
nepřechýleně	přechýleně	k6eNd1	přechýleně
Miley	Mile	k1gMnPc4	Mile
Ray	Ray	k1gMnSc1	Ray
Cyrus	Cyrus	k1gMnSc1	Cyrus
<g/>
,	,	kIx,	,
pravým	pravý	k2eAgNnSc7d1	pravé
jménem	jméno	k1gNnSc7	jméno
Destiny	Destin	k2eAgFnSc2d1	Destin
Hope	Hop	k1gFnSc2	Hop
Cyrus	Cyrus	k1gMnSc1	Cyrus
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
Nashville	Nashville	k1gFnSc1	Nashville
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gFnSc1	Tennessee
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
textařka	textařka	k1gFnSc1	textařka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
účinkování	účinkování	k1gNnSc3	účinkování
v	v	k7c4	v
Disney	Disnea	k1gFnPc4	Disnea
Channel	Channela	k1gFnPc2	Channela
sériích	série	k1gFnPc6	série
Hannah	Hannah	k1gMnSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
seriálu	seriál	k1gInSc2	seriál
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
vydán	vydat	k5eAaPmNgInS	vydat
soundtrack	soundtrack	k1gInSc1	soundtrack
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
8	[number]	k4	8
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
sólová	sólový	k2eAgFnSc1d1	sólová
hudební	hudební	k2eAgFnSc1d1	hudební
kariéra	kariéra	k1gFnSc1	kariéra
započala	započnout	k5eAaPmAgFnS	započnout
vydáním	vydání	k1gNnSc7	vydání
jejího	její	k3xOp3gNnSc2	její
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
Meet	Meeta	k1gFnPc2	Meeta
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gInSc1	Cyrus
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
její	její	k3xOp3gInSc4	její
první	první	k4xOgInSc4	první
top	topit	k5eAaImRp2nS	topit
ten	ten	k3xDgInSc4	ten
singl	singl	k1gInSc4	singl
See	See	k1gMnSc1	See
You	You	k1gMnSc1	You
Again	Again	k1gMnSc1	Again
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
Breakout	Breakout	k1gMnSc1	Breakout
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Breakout	Breakout	k1gMnSc1	Breakout
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gNnPc3	její
prvním	první	k4xOgNnSc7	první
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
Hannah	Hannah	k1gInSc1	Hannah
Montanou	Montana	k1gFnSc7	Montana
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
alba	album	k1gNnPc1	album
debutovala	debutovat	k5eAaBmAgNnP	debutovat
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
na	na	k7c4	na
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Hannah	Hannaha	k1gFnPc2	Hannaha
Montana	Montana	k1gFnSc1	Montana
&	&	k?	&
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gMnSc1	Cyrus
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Both	Both	k1gInSc1	Both
Worlds	Worlds	k1gInSc1	Worlds
Concert	Concert	k1gInSc1	Concert
<g/>
.	.	kIx.	.
</s>
<s>
Dabovala	dabovat	k5eAaBmAgFnS	dabovat
také	také	k9	také
postavu	postava	k1gFnSc4	postava
Penny	penny	k1gFnSc2	penny
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Bolt	Bolta	k1gFnPc2	Bolta
a	a	k8xC	a
nahrála	nahrát	k5eAaBmAgFnS	nahrát
píseň	píseň	k1gFnSc4	píseň
I	i	k9	i
Thought	Thought	k2eAgMnSc1d1	Thought
I	i	k8xC	i
Lost	Lost	k2eAgMnSc1d1	Lost
You	You	k1gMnSc1	You
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
získala	získat	k5eAaPmAgFnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
účinkovala	účinkovat	k5eAaImAgFnS	účinkovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
inspirovaného	inspirovaný	k2eAgInSc2d1	inspirovaný
TV	TV	kA	TV
sériemi	série	k1gFnPc7	série
Hannah	Hannah	k1gInSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc2	Movie
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
premiéru	premiér	k1gMnSc3	premiér
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Time	Tim	k1gFnSc2	Tim
zařazena	zařadit	k5eAaPmNgFnS	zařadit
mezi	mezi	k7c4	mezi
100	[number]	k4	100
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Forbes	forbes	k1gInSc1	forbes
jí	on	k3xPp3gFnSc3	on
udělil	udělit	k5eAaPmAgMnS	udělit
35	[number]	k4	35
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
"	"	kIx"	"
<g/>
Celebrity	celebrita	k1gFnSc2	celebrita
100	[number]	k4	100
<g/>
"	"	kIx"	"
s	s	k7c7	s
výdělkem	výdělek	k1gInSc7	výdělek
25	[number]	k4	25
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
si	se	k3xPyFc3	se
polepšila	polepšit	k5eAaPmAgFnS	polepšit
a	a	k8xC	a
postoupila	postoupit	k5eAaPmAgFnS	postoupit
na	na	k7c4	na
29	[number]	k4	29
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Nashvillu	Nashvill	k1gInSc6	Nashvill
v	v	k7c6	v
Tennessee	Tennessee	k1gFnSc6	Tennessee
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
1992	[number]	k4	1992
Leticii	Leticie	k1gFnSc4	Leticie
"	"	kIx"	"
<g/>
Tish	Tish	k1gInSc4	Tish
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
rozené	rozený	k2eAgFnSc2d1	rozená
Finleyové	Finleyová	k1gFnSc2	Finleyová
<g/>
)	)	kIx)	)
a	a	k8xC	a
country	country	k2eAgMnSc3d1	country
zpěvákovi	zpěvák	k1gMnSc3	zpěvák
Billovi	Bill	k1gMnSc3	Bill
Rayovi	Raya	k1gMnSc3	Raya
Cyrusovým	Cyrusový	k2eAgNnSc7d1	Cyrusový
jako	jako	k8xS	jako
Destiny	Destin	k2eAgFnPc4d1	Destin
Hope	Hop	k1gFnPc4	Hop
Cyrus	Cyrus	k1gMnSc1	Cyrus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
její	její	k3xOp3gFnSc2	její
původní	původní	k2eAgFnSc2d1	původní
přezdívky	přezdívka	k1gFnSc2	přezdívka
,,	,,	k?	,,
<g/>
Smiley	Smilea	k1gFnSc2	Smilea
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
později	pozdě	k6eAd2	pozdě
Miley	Milea	k1gFnSc2	Milea
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
starší	starý	k2eAgMnPc1d2	starší
sourozenci	sourozenec	k1gMnPc1	sourozenec
Trace	Trace	k1gMnSc1	Trace
a	a	k8xC	a
Brandi	Brand	k1gMnPc1	Brand
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc4	dítě
Tish	Tisha	k1gFnPc2	Tisha
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
předchozího	předchozí	k2eAgInSc2d1	předchozí
vztahu	vztah	k1gInSc2	vztah
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgFnP	být
adoptovány	adoptován	k2eAgMnPc4d1	adoptován
Billy	Bill	k1gMnPc4	Bill
Rayem	Rayem	k1gInSc4	Rayem
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
mladé	mladý	k2eAgFnPc1d1	mladá
<g/>
.	.	kIx.	.
</s>
<s>
Trace	Trako	k6eAd1	Trako
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
elektro-rockové	elektroockový	k2eAgFnSc2d1	elektro-rockový
skupiny	skupina	k1gFnSc2	skupina
Metro	metro	k1gNnSc1	metro
Station	station	k1gInSc4	station
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Brandi	Brand	k1gMnPc1	Brand
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
Miley	Milea	k1gFnSc2	Milea
a	a	k8xC	a
založila	založit	k5eAaPmAgFnS	založit
kapelu	kapela	k1gFnSc4	kapela
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Megan	Megana	k1gFnPc2	Megana
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
hvězdou	hvězda	k1gFnSc7	hvězda
z	z	k7c2	z
The	The	k1gFnSc2	The
Secret	Secreta	k1gFnPc2	Secreta
Life	Lif	k1gFnSc2	Lif
of	of	k?	of
American	American	k1gMnSc1	American
Teenager	teenager	k1gMnSc1	teenager
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
staršího	starý	k2eAgMnSc4d2	starší
nevlastního	vlastní	k2eNgMnSc4d1	nevlastní
bratra	bratr	k1gMnSc4	bratr
<g/>
,	,	kIx,	,
Christophera	Christopher	k1gMnSc4	Christopher
Codyho	Cody	k1gMnSc4	Cody
<g/>
,	,	kIx,	,
z	z	k7c2	z
otcova	otcův	k2eAgInSc2d1	otcův
předchozího	předchozí	k2eAgInSc2d1	předchozí
vztahu	vztah	k1gInSc2	vztah
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
Braisona	Braison	k1gMnSc4	Braison
a	a	k8xC	a
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
Noah	Noaha	k1gFnPc2	Noaha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
herečkou	herečka	k1gFnSc7	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vnučku	vnučka	k1gFnSc4	vnučka
politika	politik	k1gMnSc2	politik
Rona	Ron	k1gMnSc2	Ron
Cyruse	Cyruse	k1gFnSc2	Cyruse
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
jí	on	k3xPp3gFnSc3	on
dali	dát	k5eAaPmAgMnP	dát
jméno	jméno	k1gNnSc4	jméno
Destiny	Destin	k2eAgFnSc2d1	Destin
Hope	Hop	k1gFnSc2	Hop
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
dokázat	dokázat	k5eAaPmF	dokázat
velké	velký	k2eAgFnPc4d1	velká
věci	věc	k1gFnPc4	věc
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
dostala	dostat	k5eAaPmAgFnS	dostat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
Smiley	Smilea	k1gFnSc2	Smilea
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jako	jako	k9	jako
malá	malý	k2eAgFnSc1d1	malá
stále	stále	k6eAd1	stále
smála	smát	k5eAaImAgFnS	smát
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
Heritage	Heritage	k1gFnSc1	Heritage
Middle	Middle	k1gFnSc2	Middle
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
roztleskávačkou	roztleskávačka	k1gFnSc7	roztleskávačka
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
školu	škola	k1gFnSc4	škola
Options	Optionsa	k1gFnPc2	Optionsa
For	forum	k1gNnPc2	forum
Youth	Youtha	k1gFnPc2	Youtha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
učila	učít	k5eAaPmAgFnS	učít
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
soukromým	soukromý	k2eAgMnSc7d1	soukromý
učitelem	učitel	k1gMnSc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
mimo	mimo	k7c4	mimo
Nashville	Nashville	k1gInSc4	Nashville
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
kostel	kostel	k1gInSc4	kostel
The	The	k1gFnSc2	The
People	People	k1gFnSc2	People
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Church	Churcha	k1gFnPc2	Churcha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
začala	začít	k5eAaPmAgFnS	začít
Cyrusová	Cyrusový	k2eAgFnSc1d1	Cyrusová
chodit	chodit	k5eAaImF	chodit
s	s	k7c7	s
Nickem	Nicek	k1gMnSc7	Nicek
Jonasem	Jonas	k1gMnSc7	Jonas
z	z	k7c2	z
Jonas	Jonasa	k1gFnPc2	Jonasa
Brothers	Brothersa	k1gFnPc2	Brothersa
<g/>
.	.	kIx.	.
,,	,,	k?	,,
<g/>
Byla	být	k5eAaImAgFnS	být
jsem	být	k5eAaImIp1nS	být
najednou	najednou	k6eAd1	najednou
beznadějně	beznadějně	k6eAd1	beznadějně
zamilovaná	zamilovaný	k2eAgFnSc1d1	zamilovaná
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgInS	zastavit
celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ničem	nic	k3yNnSc6	nic
jiném	jiný	k2eAgNnSc6d1	jiné
nezáleželo	záležet	k5eNaImAgNnS	záležet
<g/>
"	"	kIx"	"
uvedla	uvést	k5eAaPmAgFnS	uvést
Cyrusová	Cyrusový	k2eAgFnSc1d1	Cyrusová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
také	také	k9	také
řekla	říct	k5eAaPmAgFnS	říct
,,	,,	k?	,,
<g/>
Je	být	k5eAaImIp3nS	být
zábava	zábava	k1gFnSc1	zábava
sledovat	sledovat	k5eAaImF	sledovat
jak	jak	k6eAd1	jak
se	s	k7c7	s
průběhem	průběh	k1gInSc7	průběh
času	čas	k1gInSc2	čas
náš	náš	k3xOp1gInSc4	náš
vztah	vztah	k1gInSc4	vztah
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mi	já	k3xPp1nSc3	já
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
do	do	k7c2	do
něho	on	k3xPp3gNnSc2	on
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Teď	teď	k6eAd1	teď
je	být	k5eAaImIp3nS	být
mi	já	k3xPp1nSc3	já
15	[number]	k4	15
a	a	k8xC	a
pořád	pořád	k6eAd1	pořád
se	se	k3xPyFc4	se
cítím	cítit	k5eAaImIp1nS	cítit
stejně	stejně	k6eAd1	stejně
zamilovaně	zamilovaně	k6eAd1	zamilovaně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Napsala	napsat	k5eAaPmAgFnS	napsat
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
všechny	všechen	k3xTgFnPc4	všechen
písně	píseň	k1gFnPc4	píseň
na	na	k7c6	na
albu	album	k1gNnSc6	album
Meet	Meeta	k1gFnPc2	Meeta
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gMnSc1	Cyrus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
se	se	k3xPyFc4	se
pár	pár	k4xCyI	pár
rozešel	rozejít	k5eAaPmAgMnS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
album	album	k1gNnSc4	album
Breakout	Breakout	k1gMnSc1	Breakout
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
napsala	napsat	k5eAaBmAgFnS	napsat
píseň	píseň	k1gFnSc1	píseň
7	[number]	k4	7
Thing	Thinga	k1gFnPc2	Thinga
(	(	kIx(	(
<g/>
I	i	k9	i
Hate	Hate	k1gFnPc2	Hate
about	about	k1gMnSc1	about
You	You	k1gMnSc1	You
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
chvíli	chvíle	k1gFnSc6	chvíle
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
Carterem	Carter	k1gMnSc7	Carter
Jenkinsem	Jenkins	k1gMnSc7	Jenkins
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008-2009	[number]	k4	2008-2009
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
modelem	model	k1gInSc7	model
Justinem	Justin	k1gMnSc7	Justin
Gastonem	Gaston	k1gInSc7	Gaston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
Nickovi	Nicek	k1gMnSc3	Nicek
Jonasovi	Jonas	k1gMnSc3	Jonas
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
na	na	k7c6	na
natáčení	natáčení	k1gNnSc6	natáčení
filmu	film	k1gInSc2	film
Poslední	poslední	k2eAgFnSc1d1	poslední
píseň	píseň	k1gFnSc1	píseň
dala	dát	k5eAaPmAgFnS	dát
dohromady	dohromady	k6eAd1	dohromady
s	s	k7c7	s
australským	australský	k2eAgMnSc7d1	australský
hercem	herec	k1gMnSc7	herec
Liamem	Liam	k1gMnSc7	Liam
Hemsworthem	Hemsworth	k1gInSc7	Hemsworth
<g/>
.	.	kIx.	.
</s>
<s>
Liam	Liam	k6eAd1	Liam
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
zahrál	zahrát	k5eAaPmAgMnS	zahrát
i	i	k9	i
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
videoklipu	videoklip	k1gInSc6	videoklip
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
písni	píseň	k1gFnSc3	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
ale	ale	k9	ale
rozešli	rozejít	k5eAaPmAgMnP	rozejít
a	a	k8xC	a
již	již	k6eAd1	již
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
pár	pár	k4xCyI	pár
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bez	bez	k1gInSc1	bez
sebe	sebe	k3xPyFc4	sebe
nemůžou	nemůžou	k?	nemůžou
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
dali	dát	k5eAaPmAgMnP	dát
opět	opět	k6eAd1	opět
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
opravdu	opravdu	k6eAd1	opravdu
zamilovaná	zamilovaný	k2eAgFnSc1d1	zamilovaná
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
nic	nic	k6eAd1	nic
k	k	k7c3	k
smíchu	smích	k1gInSc3	smích
<g/>
.	.	kIx.	.
</s>
<s>
Opravdu	opravdu	k6eAd1	opravdu
ho	on	k3xPp3gMnSc4	on
miluju	milovat	k5eAaImIp1nS	milovat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvedla	uvést	k5eAaPmAgFnS	uvést
Miley	Milea	k1gFnPc4	Milea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
zasnoubili	zasnoubit	k5eAaPmAgMnP	zasnoubit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
zasnoubení	zasnoubení	k1gNnSc1	zasnoubení
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
začala	začít	k5eAaPmAgFnS	začít
Cyrusová	Cyrusový	k2eAgFnSc1d1	Cyrusová
znovu	znovu	k6eAd1	znovu
nosit	nosit	k5eAaImF	nosit
snubní	snubní	k2eAgInSc4d1	snubní
prsten	prsten	k1gInSc4	prsten
a	a	k8xC	a
po	po	k7c6	po
spekulacích	spekulace	k1gFnPc6	spekulace
médií	médium	k1gNnPc2	médium
skutečně	skutečně	k6eAd1	skutečně
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Hemsworthem	Hemsworth	k1gInSc7	Hemsworth
znovu	znovu	k6eAd1	znovu
chodí	chodit	k5eAaImIp3nP	chodit
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
počáteční	počáteční	k2eAgFnSc1d1	počáteční
kariéra	kariéra	k1gFnSc1	kariéra
probíhala	probíhat	k5eAaImAgFnS	probíhat
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
malých	malý	k2eAgFnPc2d1	malá
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
herectví	herectví	k1gNnSc6	herectví
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
čtyřletého	čtyřletý	k2eAgInSc2d1	čtyřletý
života	život	k1gInSc2	život
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
brala	brát	k5eAaImAgFnS	brát
lekce	lekce	k1gFnSc1	lekce
herectví	herectví	k1gNnSc2	herectví
v	v	k7c6	v
Armstrong	Armstrong	k1gMnSc1	Armstrong
Acting	Acting	k1gInSc4	Acting
Studiu	studio	k1gNnSc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
první	první	k4xOgFnSc1	první
herecká	herecký	k2eAgFnSc1d1	herecká
role	role	k1gFnSc1	role
byla	být	k5eAaImAgFnS	být
malá	malý	k2eAgFnSc1d1	malá
hostující	hostující	k2eAgFnSc1d1	hostující
hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
TV	TV	kA	TV
sériích	série	k1gFnPc6	série
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Doc	doc	kA	doc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
dívku	dívka	k1gFnSc4	dívka
jménem	jméno	k1gNnSc7	jméno
Kylie	Kylie	k1gFnSc1	Kylie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
hrála	hrát	k5eAaImAgFnS	hrát
"	"	kIx"	"
<g/>
malou	malá	k1gFnSc4	malá
Ruthie	Ruthie	k1gFnSc2	Ruthie
<g/>
"	"	kIx"	"
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Tima	Timus	k1gMnSc4	Timus
Burtona	Burton	k1gMnSc4	Burton
<g/>
,	,	kIx,	,
Big	Big	k1gMnSc1	Big
Fish	Fish	k1gMnSc1	Fish
a	a	k8xC	a
v	v	k7c6	v
titulcích	titulek	k1gInPc6	titulek
byla	být	k5eAaImAgNnP	být
zapsána	zapsán	k2eAgNnPc1d1	zapsáno
svým	svůj	k3xOyFgNnSc7	svůj
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
konkurzem	konkurz	k1gInSc7	konkurz
na	na	k7c4	na
roli	role	k1gFnSc4	role
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
kamarádky	kamarádka	k1gFnPc4	kamarádka
<g/>
"	"	kIx"	"
v	v	k7c4	v
Disney	Disney	k1gInPc4	Disney
seriálu	seriál	k1gInSc2	seriál
o	o	k7c6	o
"	"	kIx"	"
<g/>
tajné	tajný	k2eAgFnSc3d1	tajná
popové	popový	k2eAgFnSc3d1	popová
hvězdě	hvězda	k1gFnSc3	hvězda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
bylo	být	k5eAaImAgNnS	být
dvanáct	dvanáct	k4xCc1	dvanáct
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
vedení	vedení	k1gNnSc2	vedení
Disney	Disnea	k1gFnSc2	Disnea
Channel	Channel	k1gMnSc1	Channel
ji	on	k3xPp3gFnSc4	on
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c7	za
příliš	příliš	k6eAd1	příliš
mladou	mladý	k2eAgFnSc7d1	mladá
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
Miley	Mile	k1gMnPc4	Mile
byla	být	k5eAaImAgFnS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
stát	stát	k5eAaImF	stát
se	s	k7c7	s
součástí	součást	k1gFnSc7	součást
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
ji	on	k3xPp3gFnSc4	on
Disney	Disnea	k1gMnSc2	Disnea
opět	opět	k6eAd1	opět
pozval	pozvat	k5eAaPmAgMnS	pozvat
ke	k	k7c3	k
konkurzu	konkurz	k1gInSc3	konkurz
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
ucházela	ucházet	k5eAaImAgFnS	ucházet
o	o	k7c4	o
protagonistku	protagonistka	k1gFnSc4	protagonistka
Chloe	Chloe	k1gFnSc1	Chloe
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
změněná	změněný	k2eAgFnSc1d1	změněná
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
prezidenta	prezident	k1gMnSc2	prezident
Disney	Disnea	k1gMnSc2	Disnea
Channel	Channel	k1gMnSc1	Channel
Garyho	Gary	k1gMnSc2	Gary
Marshe	Marsh	k1gMnSc2	Marsh
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
energický	energický	k2eAgInSc4d1	energický
a	a	k8xC	a
živý	živý	k2eAgInSc4d1	živý
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k9	jako
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
"	"	kIx"	"
<g/>
miluje	milovat	k5eAaImIp3nS	milovat
každou	každý	k3xTgFnSc4	každý
minutu	minuta	k1gFnSc4	minuta
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
s	s	k7c7	s
každodenní	každodenní	k2eAgFnSc7d1	každodenní
podobností	podobnost	k1gFnSc7	podobnost
s	s	k7c7	s
Hilary	Hilara	k1gFnSc2	Hilara
Duffovou	Duffův	k2eAgFnSc7d1	Duffův
a	a	k8xC	a
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
působící	působící	k2eAgFnSc2d1	působící
jako	jako	k8xC	jako
Shania	Shanium	k1gNnSc2	Shanium
Twain	Twaina	k1gFnPc2	Twaina
<g/>
.	.	kIx.	.
</s>
<s>
Strávila	strávit	k5eAaPmAgFnS	strávit
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
zbavit	zbavit	k5eAaPmF	zbavit
svého	svůj	k3xOyFgInSc2	svůj
jižanského	jižanský	k2eAgInSc2d1	jižanský
přízvuku	přízvuk	k1gInSc2	přízvuk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
to	ten	k3xDgNnSc1	ten
Disney	Disney	k1gInPc1	Disney
začlenil	začlenit	k5eAaPmAgMnS	začlenit
do	do	k7c2	do
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Přivedla	přivést	k5eAaPmAgFnS	přivést
na	na	k7c4	na
konkurz	konkurz	k1gInSc4	konkurz
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
Billyho	Billy	k1gMnSc2	Billy
Raye	Ray	k1gMnSc2	Ray
Cyruse	Cyruse	k1gFnSc2	Cyruse
<g/>
,	,	kIx,	,
na	na	k7c4	na
roli	role	k1gFnSc4	role
otce	otec	k1gMnSc2	otec
hlavní	hlavní	k2eAgFnSc2d1	hlavní
hrdinky	hrdinka	k1gFnSc2	hrdinka
<g/>
.	.	kIx.	.
</s>
<s>
Show	show	k1gFnSc1	show
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
dospívání	dospívání	k1gNnSc4	dospívání
a	a	k8xC	a
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
také	také	k9	také
věnuje	věnovat	k5eAaImIp3nS	věnovat
i	i	k9	i
tajné	tajný	k2eAgFnSc3d1	tajná
identitě	identita	k1gFnSc3	identita
popové	popový	k2eAgFnSc2d1	popová
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hannah	Hannah	k1gMnSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vysílaná	vysílaný	k2eAgFnSc1d1	vysílaná
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vysoce	vysoce	k6eAd1	vysoce
hodnoceným	hodnocený	k2eAgInSc7d1	hodnocený
hitem	hit	k1gInSc7	hit
<g/>
,	,	kIx,	,
se	s	k7c7	s
sledovaností	sledovanost	k1gFnSc7	sledovanost
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
4	[number]	k4	4
miliónů	milión	k4xCgInPc2	milión
diváků	divák	k1gMnPc2	divák
na	na	k7c4	na
epizodu	epizoda	k1gFnSc4	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
Účast	účast	k1gFnSc1	účast
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
jí	jíst	k5eAaImIp3nS	jíst
umožnila	umožnit	k5eAaPmAgFnS	umožnit
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
mezi	mezi	k7c7	mezi
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
dospívajícím	dospívající	k2eAgNnSc7d1	dospívající
publikem	publikum	k1gNnSc7	publikum
a	a	k8xC	a
započít	započít	k5eAaPmF	započít
svou	svůj	k3xOyFgFnSc4	svůj
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
debutovala	debutovat	k5eAaBmAgFnS	debutovat
jako	jako	k9	jako
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
edice	edice	k1gFnSc1	edice
DisneyMania	DisneyManium	k1gNnSc2	DisneyManium
<g/>
.	.	kIx.	.
</s>
<s>
Zpívala	zpívat	k5eAaImAgFnS	zpívat
předělávku	předělávka	k1gFnSc4	předělávka
"	"	kIx"	"
<g/>
Zip-a-Dee-Doo-Dah	Zip-Dee-Doo-Dah	k1gInSc4	Zip-a-Dee-Doo-Dah
<g/>
"	"	kIx"	"
od	od	k7c2	od
Jamese	Jamese	k1gFnSc2	Jamese
Basketta	Baskett	k1gInSc2	Baskett
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
animovaném	animovaný	k2eAgInSc6d1	animovaný
filmu	film	k1gInSc6	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
Song	song	k1gInSc1	song
of	of	k?	of
the	the	k?	the
South	South	k1gInSc1	South
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
vydal	vydat	k5eAaPmAgInS	vydat
první	první	k4xOgInSc4	první
Hannah	Hannah	k1gInSc4	Hannah
Montana	Montana	k1gFnSc1	Montana
soundtrack	soundtrack	k1gInSc1	soundtrack
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Soundtrack	soundtrack	k1gInSc1	soundtrack
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
osm	osm	k4xCc4	osm
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
jako	jako	k8xS	jako
Hannah	Hannah	k1gMnSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
jako	jako	k8xS	jako
Miley	Milea	k1gFnPc1	Milea
Cyrusová	Cyrusový	k2eAgFnSc1d1	Cyrusová
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
Billy	Bill	k1gMnPc7	Bill
Rayem	Ray	k1gMnSc7	Ray
Cyrusem	Cyrus	k1gMnSc7	Cyrus
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
na	na	k7c4	na
US	US	kA	US
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
prodejem	prodej	k1gInSc7	prodej
281	[number]	k4	281
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
předčilo	předčit	k5eAaBmAgNnS	předčit
například	například	k6eAd1	například
takové	takový	k3xDgMnPc4	takový
umělce	umělec	k1gMnPc4	umělec
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
My	my	k3xPp1nPc1	my
Chemical	Chemical	k1gFnSc6	Chemical
Romance	romance	k1gFnPc4	romance
<g/>
,	,	kIx,	,
a	a	k8xC	a
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
tam	tam	k6eAd1	tam
po	po	k7c4	po
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
osmé	osmý	k4xOgNnSc1	osmý
nejprodávanější	prodávaný	k2eAgNnSc1d3	nejprodávanější
album	album	k1gNnSc1	album
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
prodaly	prodat	k5eAaPmAgFnP	prodat
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
milióny	milión	k4xCgInPc4	milión
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
následně	následně	k6eAd1	následně
opětovně	opětovně	k6eAd1	opětovně
vydáno	vydat	k5eAaPmNgNnS	vydat
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
"	"	kIx"	"
<g/>
speciálních	speciální	k2eAgFnPc6d1	speciální
<g/>
"	"	kIx"	"
edicích	edice	k1gFnPc6	edice
-	-	kIx~	-
Holiday	Holidaa	k1gMnSc2	Holidaa
Edition	Edition	k1gInSc1	Edition
obsahující	obsahující	k2eAgInSc1d1	obsahující
předělávku	předělávka	k1gFnSc4	předělávka
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Rockin	Rockin	k2eAgInSc1d1	Rockin
Around	Around	k1gInSc1	Around
Christmas	Christmasa	k1gFnPc2	Christmasa
Tree	Tre	k1gFnSc2	Tre
<g/>
"	"	kIx"	"
a	a	k8xC	a
Special	Special	k1gInSc1	Special
Edition	Edition	k1gInSc1	Edition
obsahující	obsahující	k2eAgFnSc4d1	obsahující
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Nobody	Nobod	k1gInPc4	Nobod
Perfect	Perfect	k1gInSc1	Perfect
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
albu	album	k1gNnSc6	album
Disney	Disnea	k1gFnSc2	Disnea
Channel	Channel	k1gInSc4	Channel
Holiday	Holidaa	k1gFnSc2	Holidaa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Mile	k1gMnPc4	Mile
také	také	k6eAd1	také
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
jako	jako	k8xS	jako
předskokan	předskokan	k1gMnSc1	předskokan
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
The	The	k1gFnSc2	The
Cheetah	Cheetaha	k1gFnPc2	Cheetaha
Girls	girl	k1gFnPc2	girl
<g/>
,	,	kIx,	,
vystupujících	vystupující	k2eAgFnPc2d1	vystupující
v	v	k7c6	v
39	[number]	k4	39
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
remake	remake	k6eAd1	remake
klasické	klasický	k2eAgFnPc4d1	klasická
Disney	Disne	k2eAgFnPc4d1	Disne
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
Part	part	k1gInSc1	part
of	of	k?	of
Your	Your	k1gInSc1	Your
World	World	k1gInSc1	World
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
pátou	pátý	k4xOgFnSc4	pátý
edici	edice	k1gFnSc4	edice
DisneyMania	DisneyManium	k1gNnSc2	DisneyManium
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
vydala	vydat	k5eAaPmAgFnS	vydat
dvojalbum	dvojalbum	k1gNnSc4	dvojalbum
Hannah	Hannah	k1gInSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
<g/>
:	:	kIx,	:
Meet	Meet	k1gMnSc1	Meet
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gMnSc1	Cyrus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
disk	disk	k1gInSc1	disk
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
druhý	druhý	k4xOgInSc1	druhý
soundtrack	soundtrack	k1gInSc1	soundtrack
Hannah	Hannah	k1gInSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhý	druhý	k4xOgInSc4	druhý
disk	disk	k1gInSc4	disk
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc4	její
první	první	k4xOgNnSc4	první
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgInPc4d1	vydaný
pod	pod	k7c7	pod
jejím	její	k3xOp3gNnSc7	její
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
a	a	k8xC	a
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
326	[number]	k4	326
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
se	se	k3xPyFc4	se
prodávalo	prodávat	k5eAaImAgNnS	prodávat
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
předchozí	předchozí	k2eAgInSc4d1	předchozí
soundtrack	soundtrack	k1gInSc4	soundtrack
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
prodejní	prodejní	k2eAgFnSc3d1	prodejní
síle	síla	k1gFnSc3	síla
svátků	svátek	k1gInPc2	svátek
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
a	a	k8xC	a
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
v	v	k7c4	v
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
700	[number]	k4	700
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3	[number]	k4	3
milióny	milión	k4xCgInPc1	milión
kopií	kopie	k1gFnPc2	kopie
a	a	k8xC	a
získalo	získat	k5eAaPmAgNnS	získat
tak	tak	k6eAd1	tak
tři	tři	k4xCgInPc1	tři
platinové	platinový	k2eAgInPc1d1	platinový
certifikáty	certifikát	k1gInPc1	certifikát
RIAA	RIAA	kA	RIAA
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c4	na
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
See	See	k1gMnSc1	See
You	You	k1gMnSc1	You
Again	Again	k1gMnSc1	Again
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
umístěná	umístěný	k2eAgFnSc1d1	umístěná
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
její	její	k3xOp3gFnSc7	její
první	první	k4xOgFnSc7	první
písní	píseň	k1gFnSc7	píseň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
tohoto	tento	k3xDgInSc2	tento
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Milea	k1gFnPc4	Milea
měla	mít	k5eAaImAgFnS	mít
také	také	k9	také
krátký	krátký	k2eAgInSc4d1	krátký
výstup	výstup	k1gInSc4	výstup
v	v	k7c4	v
High	High	k1gInSc4	High
School	School	k1gInSc1	School
Musical	musical	k1gInSc1	musical
2	[number]	k4	2
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
"	"	kIx"	"
<g/>
dívku	dívka	k1gFnSc4	dívka
u	u	k7c2	u
bazénu	bazén	k1gInSc2	bazén
<g/>
"	"	kIx"	"
a	a	k8xC	a
hostovala	hostovat	k5eAaImAgFnS	hostovat
jako	jako	k9	jako
dabérka	dabérka	k1gFnSc1	dabérka
postavy	postava	k1gFnSc2	postava
Yatta	Yatto	k1gNnSc2	Yatto
v	v	k7c4	v
Disney	Disnea	k1gFnPc4	Disnea
Channel	Channela	k1gFnPc2	Channela
produkovaném	produkovaný	k2eAgInSc6d1	produkovaný
The	The	k1gFnPc3	The
Emperor	Emperor	k1gInSc4	Emperor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
New	New	k1gFnSc7	New
School	Schoola	k1gFnPc2	Schoola
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
představovala	představovat	k5eAaImAgFnS	představovat
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
i	i	k8xC	i
seriálový	seriálový	k2eAgInSc1d1	seriálový
charakter	charakter	k1gInSc1	charakter
Hannu	Hanen	k2eAgFnSc4d1	Hanna
Montanu	Montana	k1gFnSc4	Montana
v	v	k7c6	v
severoamerické	severoamerický	k2eAgFnSc6d1	severoamerická
Best	Best	k2eAgMnSc1d1	Best
of	of	k?	of
Both	Both	k1gInSc1	Both
Worlds	Worldsa	k1gFnPc2	Worldsa
Tour	Tour	k1gInSc4	Tour
s	s	k7c7	s
celkem	celkem	k6eAd1	celkem
69	[number]	k4	69
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
14	[number]	k4	14
nebylo	být	k5eNaImAgNnS	být
původně	původně	k6eAd1	původně
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
<g/>
.	.	kIx.	.
</s>
<s>
Jonas	Jonas	k1gInSc1	Jonas
Brothers	Brothersa	k1gFnPc2	Brothersa
dělali	dělat	k5eAaImAgMnP	dělat
předskokany	předskokan	k1gMnPc4	předskokan
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
turné	turné	k1gNnSc2	turné
<g/>
.	.	kIx.	.
</s>
<s>
Vstupenky	vstupenka	k1gFnPc4	vstupenka
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
byly	být	k5eAaImAgInP	být
vyprodané	vyprodaný	k2eAgInPc1d1	vyprodaný
v	v	k7c6	v
rekordně	rekordně	k6eAd1	rekordně
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
zklamání	zklamání	k1gNnSc6	zklamání
mnoha	mnoho	k4c2	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
společností	společnost	k1gFnSc7	společnost
Disney	Disnea	k1gFnSc2	Disnea
a	a	k8xC	a
uvedeno	uveden	k2eAgNnSc1d1	uvedeno
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
v	v	k7c6	v
3-D	[number]	k4	3-D
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vynesl	vynést	k5eAaPmAgInS	vynést
8	[number]	k4	8
651	[number]	k4	651
758	[number]	k4	758
dolarů	dolar	k1gInPc2	dolar
v	v	k7c4	v
den	den	k1gInSc4	den
premiéry	premiéra	k1gFnSc2	premiéra
a	a	k8xC	a
skvělé	skvělý	k2eAgInPc1d1	skvělý
výsledky	výsledek	k1gInPc1	výsledek
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
následujícím	následující	k2eAgInSc7d1	následující
víkendem	víkend	k1gInSc7	víkend
kdy	kdy	k6eAd1	kdy
vydělal	vydělat	k5eAaPmAgInS	vydělat
31	[number]	k4	31
117	[number]	k4	117
834	[number]	k4	834
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
částka	částka	k1gFnSc1	částka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
kdy	kdy	k6eAd1	kdy
vydělal	vydělat	k5eAaPmAgInS	vydělat
3D	[number]	k4	3D
film	film	k1gInSc1	film
za	za	k7c4	za
premiérový	premiérový	k2eAgInSc4d1	premiérový
víkend	víkend	k1gInSc4	víkend
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c4	na
Disney	Disnea	k1gFnPc4	Disnea
Channel	Channela	k1gFnPc2	Channela
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Records	Records	k1gInSc1	Records
<g/>
/	/	kIx~	/
<g/>
Hollywood	Hollywood	k1gInSc1	Hollywood
Records	Records	k1gInSc1	Records
vydal	vydat	k5eAaPmAgInS	vydat
live	live	k6eAd1	live
album	album	k1gNnSc4	album
z	z	k7c2	z
nahrávek	nahrávka	k1gFnPc2	nahrávka
pořízených	pořízený	k2eAgFnPc2d1	pořízená
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sedm	sedm	k4xCc1	sedm
písní	píseň	k1gFnPc2	píseň
které	který	k3yQgFnSc2	který
Miley	Milea	k1gFnSc2	Milea
zpívá	zpívat	k5eAaImIp3nS	zpívat
jako	jako	k9	jako
Hanna	Hanen	k2eAgFnSc1d1	Hanna
Montana	Montana	k1gFnSc1	Montana
a	a	k8xC	a
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
písní	píseň	k1gFnPc2	píseň
vlastních	vlastní	k2eAgFnPc2d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
US	US	kA	US
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
Miley	Milea	k1gFnPc4	Milea
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
na	na	k7c6	na
CMT	CMT	kA	CMT
Music	Musice	k1gFnPc2	Musice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
též	též	k9	též
na	na	k7c4	na
Teen	Teen	k1gNnSc4	Teen
Choice	Choice	k1gFnSc2	Choice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
vydala	vydat	k5eAaPmAgFnS	vydat
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Breakout	Breakout	k1gMnSc1	Breakout
<g/>
.	.	kIx.	.
</s>
<s>
Řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Breakout	Breakout	k1gMnSc1	Breakout
bylo	být	k5eAaImAgNnS	být
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
životě	život	k1gInSc6	život
událo	udát	k5eAaPmAgNnS	udát
za	za	k7c4	za
poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
písních	píseň	k1gFnPc6	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dvou	dva	k4xCgFnPc2	dva
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Skládání	skládání	k1gNnSc1	skládání
písní	píseň	k1gFnPc2	píseň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
chci	chtít	k5eAaImIp1nS	chtít
dělat	dělat	k5eAaImF	dělat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
Jen	jen	k9	jen
doufám	doufat	k5eAaImIp1nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
cokoliv	cokoliv	k3yInSc4	cokoliv
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
,	,	kIx,	,
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
skladatelka	skladatelka	k1gFnSc1	skladatelka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
US	US	kA	US
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
chart	charta	k1gFnPc2	charta
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
když	když	k8xS	když
za	za	k7c4	za
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
371	[number]	k4	371
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
7	[number]	k4	7
Things	Things	k1gInSc1	Things
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
z	z	k7c2	z
Breakout	Breakout	k1gMnSc1	Breakout
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
samostatně	samostatně	k6eAd1	samostatně
vydán	vydat	k5eAaPmNgInS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Debutoval	debutovat	k5eAaBmAgMnS	debutovat
na	na	k7c4	na
84	[number]	k4	84
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
Billboard	billboard	k1gInSc1	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
se	se	k3xPyFc4	se
vyšplhal	vyšplhat	k5eAaPmAgInS	vyšplhat
ze	z	k7c2	z
70	[number]	k4	70
<g/>
.	.	kIx.	.
místa	místo	k1gNnPc4	místo
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Milea	k1gMnSc2	Milea
Cyrus	Cyrus	k1gMnSc1	Cyrus
propůjčila	propůjčit	k5eAaPmAgFnS	propůjčit
svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
Penny	penny	k1gFnSc2	penny
<g/>
,	,	kIx,	,
postavě	postava	k1gFnSc6	postava
z	z	k7c2	z
animovaného	animovaný	k2eAgInSc2d1	animovaný
celovečerního	celovečerní	k2eAgInSc2d1	celovečerní
filmu	film	k1gInSc2	film
Bolt	Bolt	k1gInSc1	Bolt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
cestě	cesta	k1gFnSc6	cesta
psa	pes	k1gMnSc2	pes
zpět	zpět	k6eAd1	zpět
za	za	k7c7	za
svou	svůj	k3xOyFgFnSc7	svůj
majitelkou	majitelka	k1gFnSc7	majitelka
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tržbami	tržba	k1gFnPc7	tržba
26	[number]	k4	26
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
víkendu	víkend	k1gInSc2	víkend
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
za	za	k7c4	za
filmy	film	k1gInPc4	film
Twilight	Twilighta	k1gFnPc2	Twilighta
sága	sága	k1gFnSc1	sága
<g/>
:	:	kIx,	:
Stmívání	stmívání	k1gNnSc1	stmívání
a	a	k8xC	a
Quantum	Quantum	k1gNnSc1	Quantum
of	of	k?	of
Solace	Solace	k1gFnSc1	Solace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhého	druhý	k4xOgInSc2	druhý
víkendu	víkend	k1gInSc2	víkend
postoupil	postoupit	k5eAaPmAgInS	postoupit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
místo	místo	k1gNnSc4	místo
za	za	k7c7	za
filmem	film	k1gInSc7	film
Čtvery	čtvero	k4xRgFnPc4	čtvero
Vánoce	Vánoce	k1gFnPc1	Vánoce
s	s	k7c7	s
1,4	[number]	k4	1,4
<g/>
%	%	kIx~	%
nárůstem	nárůst	k1gInSc7	nárůst
tržeb	tržba	k1gFnPc2	tržba
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vydělal	vydělat	k5eAaPmAgInS	vydělat
113	[number]	k4	113
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
USA	USA	kA	USA
<g/>
,	,	kIx,	,
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
přes	přes	k7c4	přes
300	[number]	k4	300
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
a	a	k8xC	a
nahrávce	nahrávka	k1gFnSc6	nahrávka
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
originálních	originální	k2eAgFnPc2d1	originální
skladeb	skladba	k1gFnPc2	skladba
z	z	k7c2	z
filmového	filmový	k2eAgInSc2d1	filmový
soundtracku	soundtrack	k1gInSc2	soundtrack
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
Johnem	John	k1gMnSc7	John
Travoltou	Travolta	k1gMnSc7	Travolta
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
I	i	k9	i
Thought	Thought	k1gMnSc1	Thought
I	i	k8xC	i
Lost	Lost	k1gMnSc1	Lost
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
originální	originální	k2eAgFnSc4d1	originální
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Milea	k1gFnPc1	Milea
si	se	k3xPyFc3	se
poté	poté	k6eAd1	poté
zahrála	zahrát	k5eAaPmAgFnS	zahrát
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Hannah	Hannah	k1gInSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc1	Movie
<g/>
,	,	kIx,	,
inspirovaném	inspirovaný	k2eAgInSc6d1	inspirovaný
seriálem	seriál	k1gInSc7	seriál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
hlavní	hlavní	k2eAgFnSc4d1	hlavní
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
Miley	Milea	k1gFnPc1	Milea
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
najít	najít	k5eAaPmF	najít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
světy	svět	k1gInPc7	svět
<g/>
,	,	kIx,	,
svým	své	k1gNnSc7	své
a	a	k8xC	a
Hannou	Hanný	k2eAgFnSc7d1	Hanný
Montanou	Montana	k1gFnSc7	Montana
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
kritiky	kritika	k1gFnSc2	kritika
posouzen	posoudit	k5eAaPmNgInS	posoudit
výborně	výborně	k6eAd1	výborně
a	a	k8xC	a
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
vydělal	vydělat	k5eAaPmAgInS	vydělat
skoro	skoro	k6eAd1	skoro
153	[number]	k4	153
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Climb	Climba	k1gFnPc2	Climba
<g/>
"	"	kIx"	"
z	z	k7c2	z
filmového	filmový	k2eAgInSc2d1	filmový
soundtracku	soundtrack	k1gInSc2	soundtrack
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
číslem	číslo	k1gNnSc7	číslo
4	[number]	k4	4
na	na	k7c4	na
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
umístěním	umístění	k1gNnSc7	umístění
Miley	Milea	k1gFnSc2	Milea
Cyrusové	Cyrusový	k2eAgInPc4d1	Cyrusový
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
,	,	kIx,	,
předstihl	předstihnout	k5eAaPmAgInS	předstihnout
i	i	k9	i
"	"	kIx"	"
<g/>
See	See	k1gMnSc1	See
You	You	k1gMnSc1	You
Again	Again	k1gMnSc1	Again
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
7	[number]	k4	7
Things	Thingsa	k1gFnPc2	Thingsa
<g/>
"	"	kIx"	"
s	s	k7c7	s
umístěním	umístění	k1gNnSc7	umístění
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Filmový	filmový	k2eAgInSc4d1	filmový
soundtrack	soundtrack	k1gInSc4	soundtrack
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
příčce	příčka	k1gFnSc6	příčka
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
jeden	jeden	k4xCgInSc4	jeden
platinový	platinový	k2eAgInSc4d1	platinový
certifikát	certifikát	k1gInSc4	certifikát
RIAA	RIAA	kA	RIAA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
vyměnila	vyměnit	k5eAaPmAgFnS	vyměnit
agenturu	agentura	k1gFnSc4	agentura
United	United	k1gInSc1	United
Talent	talent	k1gInSc4	talent
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ji	on	k3xPp3gFnSc4	on
představovala	představovat	k5eAaImAgFnS	představovat
jako	jako	k9	jako
televizní	televizní	k2eAgFnSc4d1	televizní
a	a	k8xC	a
filmovou	filmový	k2eAgFnSc4d1	filmová
herečku	herečka	k1gFnSc4	herečka
<g/>
,	,	kIx,	,
za	za	k7c4	za
Creative	Creativ	k1gInSc5	Creativ
Artists	Artistsa	k1gFnPc2	Artistsa
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ji	on	k3xPp3gFnSc4	on
již	již	k6eAd1	již
představovala	představovat	k5eAaImAgFnS	představovat
jako	jako	k8xS	jako
hudebnici	hudebnice	k1gFnSc4	hudebnice
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
začala	začít	k5eAaPmAgFnS	začít
natáčet	natáčet	k5eAaImF	natáčet
film	film	k1gInSc4	film
The	The	k1gFnSc2	The
Last	Last	k2eAgInSc4d1	Last
Song	song	k1gInSc4	song
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
románu	román	k1gInSc6	román
Nicholase	Nicholas	k1gInSc6	Nicholas
Sparkse	Sparkse	k1gFnSc2	Sparkse
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Mile	k1gMnPc4	Mile
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
vzpurnou	vzpurný	k2eAgFnSc4d1	vzpurná
dospívající	dospívající	k2eAgFnSc4d1	dospívající
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tráví	trávit	k5eAaImIp3nS	trávit
léto	léto	k1gNnSc4	léto
s	s	k7c7	s
odcizeným	odcizený	k2eAgMnSc7d1	odcizený
otcem	otec	k1gMnSc7	otec
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
domě	dům	k1gInSc6	dům
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
poslání	poslání	k1gNnSc2	poslání
filmu	film	k1gInSc6	film
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
představit	představit	k5eAaPmF	představit
Miley	Milea	k1gFnPc4	Milea
starším	starý	k2eAgMnPc3d2	starší
divákům	divák	k1gMnPc3	divák
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
nahrála	nahrát	k5eAaBmAgFnS	nahrát
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
Jonas	Jonas	k1gInSc4	Jonas
Brothers	Brothers	k1gInSc4	Brothers
"	"	kIx"	"
<g/>
Before	Befor	k1gMnSc5	Befor
The	The	k1gMnSc5	The
Storm	Storm	k1gInSc4	Storm
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Lines	Lines	k1gMnSc1	Lines
<g/>
,	,	kIx,	,
Vines	Vines	k1gMnSc1	Vines
and	and	k?	and
Trying	Trying	k1gInSc1	Trying
Times	Times	k1gInSc1	Times
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vydala	vydat	k5eAaPmAgFnS	vydat
další	další	k2eAgInSc4d1	další
soundtrack	soundtrack	k1gInSc4	soundtrack
Hannah	Hannah	k1gInSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
třetí	třetí	k4xOgFnSc4	třetí
sezónu	sezóna	k1gFnSc4	sezóna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
názvem	název	k1gInSc7	název
Hannah	Hannaha	k1gFnPc2	Hannaha
Montana	Montana	k1gFnSc1	Montana
Season	Season	k1gInSc1	Season
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Vyšel	vyjít	k5eAaPmAgMnS	vyjít
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
jí	jíst	k5eAaImIp3nS	jíst
první	první	k4xOgMnSc1	první
Top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Hannah	Hannah	k1gInSc1	Hannah
<g/>
"	"	kIx"	"
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
He	he	k0	he
Could	Could	k1gMnSc1	Could
Be	Be	k1gMnPc2	Be
The	The	k1gMnSc1	The
One	One	k1gMnSc1	One
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Party	party	k1gFnSc1	party
in	in	k?	in
the	the	k?	the
USA	USA	kA	USA
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
v	v	k7c6	v
rádiích	rádio	k1gNnPc6	rádio
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
exklusivního	exklusivní	k2eAgMnSc2d1	exklusivní
Wal-Mart	Wal-Mart	k1gInSc4	Wal-Mart
EP	EP	kA	EP
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gMnSc1	The
Times	Times	k1gMnSc1	Times
of	of	k?	of
Our	Our	k1gMnSc1	Our
Lives	Lives	k1gMnSc1	Lives
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
kolekci	kolekce	k1gFnSc4	kolekce
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Miley	Miley	k1gInPc4	Miley
společně	společně	k6eAd1	společně
s	s	k7c7	s
Maxem	Max	k1gMnSc7	Max
Azriou	Azria	k1gMnSc7	Azria
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
Hot	hot	k0	hot
Digital	Digital	kA	Digital
Songs	Songs	k1gInSc1	Songs
s	s	k7c7	s
226	[number]	k4	226
tisíci	tisíc	k4xCgInPc7	tisíc
placenými	placený	k2eAgNnPc7d1	placené
staženími	stažení	k1gNnPc7	stažení
(	(	kIx(	(
<g/>
Miley	Miley	k1gInPc7	Miley
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
umělcem	umělec	k1gMnSc7	umělec
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
této	tento	k3xDgFnSc2	tento
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c4	na
Billboard	billboard	k1gInSc4	billboard
100	[number]	k4	100
2	[number]	k4	2
<g/>
.	.	kIx.	.
místem	místo	k1gNnSc7	místo
a	a	k8xC	a
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
tak	tak	k6eAd1	tak
i	i	k8xC	i
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Climb	Climb	k1gMnSc1	Climb
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
nejlépe	dobře	k6eAd3	dobře
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Podělila	podělit	k5eAaPmAgFnS	podělit
se	se	k3xPyFc4	se
tak	tak	k9	tak
s	s	k7c7	s
Black	Black	k1gMnSc1	Black
Eyed	Eyed	k1gMnSc1	Eyed
Peas	Peasa	k1gFnPc2	Peasa
o	o	k7c4	o
nejlépe	dobře	k6eAd3	dobře
umístěný	umístěný	k2eAgInSc4d1	umístěný
debut	debut	k1gInSc4	debut
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
nejlépe	dobře	k6eAd3	dobře
umístěný	umístěný	k2eAgInSc1d1	umístěný
debut	debut	k1gInSc1	debut
sólové	sólový	k2eAgFnSc2d1	sólová
umělkyně	umělkyně	k1gFnSc2	umělkyně
hned	hned	k6eAd1	hned
za	za	k7c7	za
debutem	debut	k1gInSc7	debut
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Inside	Insid	k1gInSc5	Insid
Your	Youra	k1gFnPc2	Youra
Heaven	Heavna	k1gFnPc2	Heavna
<g/>
"	"	kIx"	"
od	od	k7c2	od
Carrie	Carrie	k1gFnSc2	Carrie
Underwoodové	Underwoodový	k2eAgFnSc2d1	Underwoodová
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
oznámila	oznámit	k5eAaPmAgFnS	oznámit
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
názvem	název	k1gInSc7	název
2009	[number]	k4	2009
North	Northa	k1gFnPc2	Northa
American	American	k1gMnSc1	American
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
začalo	začít	k5eAaPmAgNnS	začít
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2009	[number]	k4	2009
v	v	k7c6	v
Portlandu	Portland	k1gInSc6	Portland
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Oregon	Oregona	k1gFnPc2	Oregona
s	s	k7c7	s
plánovaným	plánovaný	k2eAgNnSc7d1	plánované
časovým	časový	k2eAgNnSc7d1	časové
rozpětím	rozpětí	k1gNnSc7	rozpětí
45	[number]	k4	45
dnů	den	k1gInPc2	den
a	a	k8xC	a
koncerty	koncert	k1gInPc1	koncert
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgInP	být
oznámeny	oznámen	k2eAgInPc1d1	oznámen
další	další	k2eAgInPc1d1	další
termíny	termín	k1gInPc1	termín
pro	pro	k7c4	pro
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
a	a	k8xC	a
Irsko	Irsko	k1gNnSc4	Irsko
a	a	k8xC	a
turné	turné	k1gNnSc4	turné
se	se	k3xPyFc4	se
přejmenovalo	přejmenovat	k5eAaPmAgNnS	přejmenovat
na	na	k7c4	na
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
World	Worldo	k1gNnPc2	Worldo
Tour	Toura	k1gFnPc2	Toura
a	a	k8xC	a
následně	následně	k6eAd1	následně
na	na	k7c4	na
Wonder	Wonder	k1gInSc4	Wonder
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
kapelu	kapela	k1gFnSc4	kapela
Metro	metro	k1gNnSc4	metro
Station	station	k1gInSc1	station
jako	jako	k9	jako
předskokany	předskokan	k1gMnPc7	předskokan
<g/>
.	.	kIx.	.
</s>
<s>
Vstupenky	vstupenka	k1gFnSc2	vstupenka
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zakoupit	zakoupit	k5eAaPmF	zakoupit
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
pro	pro	k7c4	pro
USA	USA	kA	USA
a	a	k8xC	a
od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
pro	pro	k7c4	pro
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
a	a	k8xC	a
Irsko	Irsko	k1gNnSc4	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
Miley	Mile	k1gMnPc7	Mile
zazpívala	zazpívat	k5eAaPmAgFnS	zazpívat
své	svůj	k3xOyFgInPc4	svůj
hity	hit	k1gInPc4	hit
"	"	kIx"	"
<g/>
See	See	k1gMnSc1	See
You	You	k1gMnSc1	You
Again	Again	k1gMnSc1	Again
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Climb	Climb	k1gMnSc1	Climb
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Party	party	k1gFnSc1	party
in	in	k?	in
the	the	k?	the
USA	USA	kA	USA
<g/>
"	"	kIx"	"
na	na	k7c4	na
95.8	[number]	k4	95.8
Capital	Capital	k1gMnSc1	Capital
FM	FM	kA	FM
Jingle	Jingle	k1gNnSc1	Jingle
Bell	bell	k1gInSc1	bell
Ball	Ball	k1gMnSc1	Ball
v	v	k7c6	v
londýnské	londýnský	k2eAgFnSc6d1	londýnská
O2	O2	k1gFnSc6	O2
aréně	aréna	k1gFnSc6	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
Miley	Milea	k1gFnSc2	Milea
zpívala	zpívat	k5eAaImAgFnS	zpívat
pro	pro	k7c4	pro
anglickou	anglický	k2eAgFnSc4d1	anglická
královnu	královna	k1gFnSc4	královna
Alžbětu	Alžběta	k1gFnSc4	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
členů	člen	k1gMnPc2	člen
britské	britský	k2eAgFnSc2d1	britská
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
v	v	k7c4	v
Royal	Royal	k1gInSc4	Royal
Variety	varieta	k1gFnPc1	varieta
Performance	performance	k1gFnSc2	performance
v	v	k7c6	v
Blackpoolu	Blackpool	k1gInSc6	Blackpool
<g/>
,	,	kIx,	,
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
americkou	americký	k2eAgFnSc7d1	americká
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Lady	Lada	k1gFnSc2	Lada
GaGa	GaG	k1gInSc2	GaG
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
Timbaland	Timbaland	k1gInSc1	Timbaland
vydal	vydat	k5eAaPmAgInS	vydat
své	svůj	k3xOyFgNnSc4	svůj
album	album	k1gNnSc4	album
Shock	Shock	k1gMnSc1	Shock
Value	Valu	k1gInSc2	Valu
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Miley	Mile	k1gMnPc7	Mile
nahrál	nahrát	k5eAaPmAgMnS	nahrát
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
We	We	k1gFnSc1	We
Belong	Belonga	k1gFnPc2	Belonga
To	to	k9	to
The	The	k1gMnSc1	The
Music	Music	k1gMnSc1	Music
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skladatelé	skladatel	k1gMnPc1	skladatel
písně	píseň	k1gFnSc2	píseň
The	The	k1gMnSc1	The
Climb	Climb	k1gMnSc1	Climb
<g/>
,	,	kIx,	,
Jessi	Jesse	k1gFnSc4	Jesse
Alexander	Alexandra	k1gFnPc2	Alexandra
a	a	k8xC	a
Jon	Jon	k1gFnPc2	Jon
Mabe	Mabe	k1gFnPc2	Mabe
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
nominováni	nominovat	k5eAaBmNgMnP	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
píseň	píseň	k1gFnSc4	píseň
napsanou	napsaný	k2eAgFnSc4d1	napsaná
pro	pro	k7c4	pro
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
televizi	televize	k1gFnSc4	televize
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgNnPc4d1	jiné
vizuální	vizuální	k2eAgNnPc4d1	vizuální
média	médium	k1gNnPc4	médium
na	na	k7c4	na
52	[number]	k4	52
<g/>
.	.	kIx.	.
udílení	udílení	k1gNnSc1	udílení
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
píseň	píseň	k1gFnSc1	píseň
The	The	k1gMnPc2	The
Climb	Climba	k1gFnPc2	Climba
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
vyškrtnuta	vyškrtnout	k5eAaPmNgFnS	vyškrtnout
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Milea	k1gFnSc2	Milea
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
na	na	k7c4	na
Billboard	billboard	k1gInSc4	billboard
Top	topit	k5eAaImRp2nS	topit
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
"	"	kIx"	"
<g/>
Top	topit	k5eAaImRp2nS	topit
umělkyně	umělkyně	k1gFnSc2	umělkyně
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
"	"	kIx"	"
nad	nad	k7c4	nad
Britney	Britney	k1gInPc4	Britney
Spears	Spearsa	k1gFnPc2	Spearsa
a	a	k8xC	a
Kelly	Kella	k1gFnSc2	Kella
Clarkson	Clarksona	k1gFnPc2	Clarksona
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
filmu	film	k1gInSc3	film
The	The	k1gMnPc2	The
Last	Last	k2eAgInSc4d1	Last
Song	song	k1gInSc4	song
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrála	hrát	k5eAaImAgFnS	hrát
<g/>
,	,	kIx,	,
nazpívala	nazpívat	k5eAaBmAgFnS	nazpívat
i	i	k9	i
dvě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
-	-	kIx~	-
"	"	kIx"	"
<g/>
When	When	k1gMnSc1	When
I	i	k8xC	i
Look	Look	k1gMnSc1	Look
At	At	k1gMnSc1	At
You	You	k1gMnSc1	You
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
I	i	k9	i
Hope	Hope	k1gInSc1	Hope
You	You	k1gMnSc2	You
Find	Find	k?	Find
It	It	k1gMnSc2	It
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
nechybí	chybit	k5eNaPmIp3nP	chybit
v	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
soundtracku	soundtrack	k1gInSc6	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
When	Whena	k1gFnPc2	Whena
I	i	k8xC	i
Look	Looka	k1gFnPc2	Looka
At	At	k1gFnSc1	At
You	You	k1gFnSc1	You
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
velkým	velký	k2eAgInSc7d1	velký
hitem	hit	k1gInSc7	hit
i	i	k9	i
na	na	k7c6	na
albu	album	k1gNnSc6	album
The	The	k1gMnSc2	The
Time	Tim	k1gMnSc2	Tim
Of	Of	k1gMnSc1	Of
Our	Our	k1gMnSc1	Our
Lives	Lives	k1gMnSc1	Lives
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
poslední	poslední	k2eAgFnSc2d1	poslední
sezóny	sezóna	k1gFnSc2	sezóna
Hannah	Hannaha	k1gFnPc2	Hannaha
Montany	Montana	k1gFnSc2	Montana
začala	začít	k5eAaPmAgFnS	začít
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pomoci	pomoc	k1gFnSc2	pomoc
po	po	k7c4	po
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
na	na	k7c4	na
Haiti	Haiti	k1gNnSc4	Haiti
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
se	se	k3xPyFc4	se
Miley	Milea	k1gFnSc2	Milea
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
nahrávání	nahrávání	k1gNnSc4	nahrávání
remaků	remak	k1gInPc2	remak
songů	song	k1gInPc2	song
"	"	kIx"	"
<g/>
We	We	k1gMnSc1	We
Are	ar	k1gInSc5	ar
The	The	k1gMnSc7	The
World	World	k1gMnSc1	World
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
For	forum	k1gNnPc2	forum
Haiti	Haiti	k1gNnSc2	Haiti
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Everybody	Everybod	k1gInPc1	Everybod
Hurts	Hurtsa	k1gFnPc2	Hurtsa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
třetí	třetí	k4xOgNnSc1	třetí
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
Can	Can	k1gFnSc2	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Be	Be	k1gFnSc4	Be
Tamed	Tamed	k1gInSc4	Tamed
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
skladba	skladba	k1gFnSc1	skladba
nesla	nést	k5eAaImAgFnS	nést
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Can	Can	k1gFnSc4	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Be	Be	k1gMnSc1	Be
Tamed	Tamed	k1gMnSc1	Tamed
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
a	a	k8xC	a
debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c4	v
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kostýmy	kostým	k1gInPc1	kostým
a	a	k8xC	a
pohyby	pohyb	k1gInPc1	pohyb
jsou	být	k5eAaImIp3nP	být
podstatně	podstatně	k6eAd1	podstatně
více	hodně	k6eAd2	hodně
provokativní	provokativní	k2eAgMnSc1d1	provokativní
než	než	k8xS	než
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
terčem	terč	k1gInSc7	terč
mediální	mediální	k2eAgFnSc2d1	mediální
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
si	se	k3xPyFc3	se
Miley	Miley	k1gInPc7	Miley
dává	dávat	k5eAaImIp3nS	dávat
pauzu	pauza	k1gFnSc4	pauza
od	od	k7c2	od
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
filmovou	filmový	k2eAgFnSc4d1	filmová
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Miley	k1gInPc1	Miley
se	se	k3xPyFc4	se
také	také	k9	také
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
odložit	odložit	k5eAaPmF	odložit
přihlášku	přihláška	k1gFnSc4	přihláška
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
-	-	kIx~	-
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pevně	pevně	k6eAd1	pevně
věřím	věřit	k5eAaImIp1nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
se	se	k3xPyFc4	se
můžete	moct	k5eAaImIp2nP	moct
vrátit	vrátit	k5eAaPmF	vrátit
kdykoliv	kdykoliv	k6eAd1	kdykoliv
chcete	chtít	k5eAaImIp2nP	chtít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
moje	můj	k3xOp1gFnSc1	můj
babička	babička	k1gFnSc1	babička
šla	jít	k5eAaImAgFnS	jít
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
až	až	k9	až
v	v	k7c6	v
62	[number]	k4	62
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pracovala	pracovat	k5eAaImAgFnS	pracovat
jsem	být	k5eAaImIp1nS	být
tvrdě	tvrdě	k6eAd1	tvrdě
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
teď	teď	k6eAd1	teď
jsem	být	k5eAaImIp1nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Nechci	chtít	k5eNaImIp1nS	chtít
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
vzdát	vzdát	k5eAaPmF	vzdát
<g/>
,	,	kIx,	,
chci	chtít	k5eAaImIp1nS	chtít
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
užívat	užívat	k5eAaImF	užívat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Miley	Milea	k1gFnSc2	Milea
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
již	již	k6eAd1	již
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
filmu	film	k1gInSc6	film
The	The	k1gFnSc2	The
Last	Last	k2eAgInSc4d1	Last
Song	song	k1gInSc4	song
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c6	na
plátnech	plátno	k1gNnPc6	plátno
kin	kino	k1gNnPc2	kino
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
a	a	k8xC	a
dostal	dostat	k5eAaPmAgInS	dostat
obecně	obecně	k6eAd1	obecně
chabé	chabý	k2eAgFnPc4d1	chabá
recenze	recenze	k1gFnPc4	recenze
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
výkon	výkon	k1gInSc4	výkon
Miley	Milea	k1gFnSc2	Milea
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
komerčně	komerčně	k6eAd1	komerčně
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
vydělal	vydělat	k5eAaPmAgInS	vydělat
celosvětově	celosvětově	k6eAd1	celosvětově
zhruba	zhruba	k6eAd1	zhruba
88	[number]	k4	88
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
z	z	k7c2	z
Exhibitor	Exhibitor	k1gInSc4	Exhibitor
Realtions	Realtions	k1gInSc1	Realtions
film	film	k1gInSc1	film
označili	označit	k5eAaPmAgMnP	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
přechod	přechod	k1gInSc4	přechod
Miley	Milea	k1gFnSc2	Milea
Cyrusové	Cyrusový	k2eAgFnSc2d1	Cyrusová
k	k	k7c3	k
dospělým	dospělý	k2eAgFnPc3d1	dospělá
rolím	role	k1gFnPc3	role
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
poslední	poslední	k2eAgFnSc1d1	poslední
sezóna	sezóna	k1gFnSc1	sezóna
Hannah	Hannaha	k1gFnPc2	Hannaha
Montany	Montana	k1gFnSc2	Montana
<g/>
,	,	kIx,	,
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Hannah	Hannah	k1gInSc4	Hannah
Montana	Montana	k1gFnSc1	Montana
Forever	Forevero	k1gNnPc2	Forevero
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2010	[number]	k4	2010
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
je	být	k5eAaImIp3nS	být
naplánovaný	naplánovaný	k2eAgInSc1d1	naplánovaný
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Disney	Disnea	k1gFnSc2	Disnea
ale	ale	k8xC	ale
pojede	pojet	k5eAaPmIp3nS	pojet
rekapitulace	rekapitulace	k1gFnSc1	rekapitulace
všech	všecek	k3xTgFnPc2	všecek
epizod	epizoda	k1gFnPc2	epizoda
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gInSc1	Cyrus
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
také	také	k6eAd1	také
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Rock	rock	k1gInSc1	rock
in	in	k?	in
Rio	Rio	k1gFnSc2	Rio
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
a	a	k8xC	a
Rock	rock	k1gInSc1	rock
in	in	k?	in
Rio	Rio	k1gFnSc2	Rio
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
přijala	přijmout	k5eAaPmAgFnS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
na	na	k7c4	na
několik	několik	k4yIc4	několik
vlastních	vlastní	k2eAgFnPc2d1	vlastní
koncertních	koncertní	k2eAgFnPc2d1	koncertní
show	show	k1gFnPc2	show
od	od	k7c2	od
producenta	producent	k1gMnSc2	producent
Mika	Mik	k1gMnSc2	Mik
Carpy	Carpa	k1gFnSc2	Carpa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
proběhnou	proběhnout	k5eAaPmIp3nP	proběhnout
po	po	k7c6	po
odvysílání	odvysílání	k1gNnSc6	odvysílání
uvedené	uvedený	k2eAgFnSc2d1	uvedená
rekapitulace	rekapitulace	k1gFnSc2	rekapitulace
všech	všecek	k3xTgFnPc2	všecek
epizod	epizoda	k1gFnPc2	epizoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Milea	k1gFnPc1	Milea
tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
natáčela	natáčet	k5eAaImAgFnS	natáčet
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
LOL	LOL	kA	LOL
a	a	k8xC	a
So	So	kA	So
Undercover	Undercover	k1gInSc1	Undercover
<g/>
.	.	kIx.	.
</s>
<s>
Premiéry	premiéra	k1gFnPc1	premiéra
se	se	k3xPyFc4	se
však	však	k9	však
dočkaly	dočkat	k5eAaPmAgInP	dočkat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
Miley	Milea	k1gFnSc2	Milea
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
udělá	udělat	k5eAaPmIp3nS	udělat
prozatím	prozatím	k6eAd1	prozatím
konec	konec	k1gInSc1	konec
s	s	k7c7	s
hraním	hraní	k1gNnSc7	hraní
<g/>
,	,	kIx,	,
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
turné	turné	k1gNnSc7	turné
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Miley	k1gInPc4	Miley
řekla	říct	k5eAaPmAgFnS	říct
E	E	kA	E
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
Online	Onlin	k1gMnSc5	Onlin
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Teď	teď	k6eAd1	teď
žádné	žádný	k3yNgInPc4	žádný
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
jedu	jet	k5eAaImIp1nS	jet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Miley	Milea	k1gMnSc2	Milea
také	také	k9	také
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
fanoušek	fanoušek	k1gMnSc1	fanoušek
Nirvany	Nirvan	k1gMnPc4	Nirvan
a	a	k8xC	a
Radiohead	Radiohead	k1gInSc4	Radiohead
a	a	k8xC	a
tak	tak	k6eAd1	tak
bude	být	k5eAaImBp3nS	být
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Smells	Smells	k1gInSc1	Smells
Like	Lik	k1gFnSc2	Lik
Teen	Teena	k1gFnPc2	Teena
Spirit	Spirit	k1gInSc1	Spirit
<g/>
"	"	kIx"	"
zpívat	zpívat	k5eAaImF	zpívat
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
turné	turné	k1gNnSc2	turné
Gypsy	gyps	k1gInPc1	gyps
Heart	Hearta	k1gFnPc2	Hearta
Tour	Toura	k1gFnPc2	Toura
<g/>
,	,	kIx,	,
začínajícího	začínající	k2eAgInSc2d1	začínající
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Miley	Milea	k1gFnSc2	Milea
Cyrusová	Cyrusový	k2eAgFnSc1d1	Cyrusová
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
53	[number]	k4	53
<g/>
.	.	kIx.	.
udílení	udílení	k1gNnSc1	udílení
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
interview	interview	k1gNnSc6	interview
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
absolvuje	absolvovat	k5eAaPmIp3nS	absolvovat
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
Jižní	jižní	k2eAgFnSc4d1	jižní
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
turné	turné	k1gNnSc1	turné
mělo	mít	k5eAaImAgNnS	mít
skládat	skládat	k5eAaImF	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
by	by	kYmCp3nS	by
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
Americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
Evropu	Evropa	k1gFnSc4	Evropa
včetně	včetně	k7c2	včetně
dlouho	dlouho	k6eAd1	dlouho
uvažované	uvažovaný	k2eAgFnSc2d1	uvažovaná
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
liché	lichý	k2eAgFnPc1d1	lichá
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Milea	k1gFnPc1	Milea
Cyrusová	Cyrusový	k2eAgFnSc1d1	Cyrusová
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c4	v
Saturday	Saturda	k2eAgInPc4d1	Saturda
Night	Night	k2eAgInSc4d1	Night
Live	Liv	k1gInPc4	Liv
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nejen	nejen	k6eAd1	nejen
zparodovala	zparodovat	k5eAaPmAgFnS	zparodovat
mnoho	mnoho	k4c4	mnoho
slavných	slavný	k2eAgMnPc2d1	slavný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
se	s	k7c7	s
songem	song	k1gInSc7	song
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Sorry	sorry	k9	sorry
That	That	k1gInSc4	That
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Not	nota	k1gFnPc2	nota
Perfect	Perfecta	k1gFnPc2	Perfecta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zpívá	zpívat	k5eAaImIp3nS	zpívat
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
za	za	k7c4	za
co	co	k3yInSc4	co
ji	on	k3xPp3gFnSc4	on
lidé	člověk	k1gMnPc1	člověk
poslední	poslední	k2eAgMnPc1d1	poslední
dobou	doba	k1gFnSc7	doba
odsuzovali	odsuzovat	k5eAaImAgMnP	odsuzovat
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
kouření	kouření	k1gNnSc1	kouření
bongu	bongo	k1gNnSc3	bongo
<g/>
,	,	kIx,	,
tančení	tančení	k1gNnSc3	tančení
na	na	k7c6	na
tyči	tyč	k1gFnSc6	tyč
během	během	k7c2	během
vystoupení	vystoupení	k1gNnSc2	vystoupení
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
písní	píseň	k1gFnSc7	píseň
Party	party	k1gFnPc2	party
in	in	k?	in
the	the	k?	the
USA	USA	kA	USA
na	na	k7c4	na
Teen	Teen	k1gInSc4	Teen
Choice	Choic	k1gMnSc2	Choic
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
Twitter	Twitter	k1gInSc4	Twitter
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
účet	účet	k1gInSc4	účet
smazala	smazat	k5eAaPmAgFnS	smazat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
soukromí	soukromí	k1gNnSc2	soukromí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
otec	otec	k1gMnSc1	otec
Billy	Bill	k1gMnPc4	Bill
Ray	Ray	k1gMnSc1	Ray
Cyrus	Cyrus	k1gMnSc1	Cyrus
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
The	The	k1gFnSc7	The
View	View	k1gFnSc2	View
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Miley	Milea	k1gMnSc2	Milea
jednala	jednat	k5eAaImAgFnS	jednat
s	s	k7c7	s
producentem	producent	k1gMnSc7	producent
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Luke	Luk	k1gMnSc2	Luk
ohledně	ohledně	k7c2	ohledně
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
se	se	k3xPyFc4	se
Miley	Miley	k1gInPc1	Miley
Cyrusová	Cyrusový	k2eAgFnSc1d1	Cyrusová
vydává	vydávat	k5eAaPmIp3nS	vydávat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
nové	nový	k2eAgNnSc4d1	nové
turné	turné	k1gNnSc4	turné
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Gypsy	gyps	k1gInPc1	gyps
Heart	Hearta	k1gFnPc2	Hearta
Tour	Toura	k1gFnPc2	Toura
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
Jižní	jižní	k2eAgFnSc4d1	jižní
a	a	k8xC	a
Střední	střední	k2eAgFnSc4d1	střední
Ameriku	Amerika	k1gFnSc4	Amerika
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc4	Filipíny
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc4	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Milea	k1gFnSc2	Milea
vysvětlila	vysvětlit	k5eAaPmAgFnS	vysvětlit
název	název	k1gInSc4	název
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dříve	dříve	k6eAd2	dříve
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
hodně	hodně	k6eAd1	hodně
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
cikáni	cikán	k1gMnPc1	cikán
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
milovala	milovat	k5eAaImAgFnS	milovat
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Gypsy	gyps	k1gInPc4	gyps
Heart	Hearta	k1gFnPc2	Hearta
Tour	Toura	k1gFnPc2	Toura
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Mile	k1gMnPc4	Mile
dále	daleko	k6eAd2	daleko
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgNnSc1d1	nové
turné	turné	k1gNnSc1	turné
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
šíření	šíření	k1gNnSc4	šíření
lásky	láska	k1gFnSc2	láska
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
skončilo	skončit	k5eAaPmAgNnS	skončit
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
M	M	kA	M
<g/>
(	(	kIx(	(
<g/>
Magazine	Magazin	k1gMnSc5	Magazin
<g/>
)	)	kIx)	)
nejbohatší	bohatý	k2eAgFnSc7d3	nejbohatší
teenagerskou	teenagerský	k2eAgFnSc7d1	teenagerská
hvězdou	hvězda	k1gFnSc7	hvězda
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
ročním	roční	k2eAgInSc7d1	roční
odhadem	odhad	k1gInSc7	odhad
příjmu	příjem	k1gInSc2	příjem
okolo	okolo	k7c2	okolo
120	[number]	k4	120
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Zanechala	zanechat	k5eAaPmAgFnS	zanechat
tak	tak	k6eAd1	tak
za	za	k7c7	za
sebou	se	k3xPyFc7	se
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Justin	Justin	k1gMnSc1	Justin
Bieber	Bieber	k1gMnSc1	Bieber
<g/>
,	,	kIx,	,
Nick	Nick	k1gMnSc1	Nick
Jonas	Jonas	k1gMnSc1	Jonas
nebo	nebo	k8xC	nebo
Selena	selen	k2eAgFnSc1d1	Selena
Gomezová	Gomezová	k1gFnSc1	Gomezová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
Can	Can	k1gMnSc2	Can
<g/>
́	́	k?	́
<g/>
t	t	k?	t
Be	Be	k1gFnSc1	Be
Tamed	Tamed	k1gInSc1	Tamed
oznámila	oznámit	k5eAaPmAgFnS	oznámit
Miley	Milea	k1gFnSc2	Milea
pauzu	pauza	k1gFnSc4	pauza
od	od	k7c2	od
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
zaměření	zaměření	k1gNnSc2	zaměření
na	na	k7c4	na
hereckou	herecký	k2eAgFnSc4d1	herecká
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
neplánuje	plánovat	k5eNaImIp3nS	plánovat
jít	jít	k5eAaImF	jít
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
době	doba	k1gFnSc6	doba
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
radši	rád	k6eAd2	rád
se	se	k3xPyFc4	se
zaměří	zaměřit	k5eAaPmIp3nS	zaměřit
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
také	také	k6eAd1	také
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
hudební	hudební	k2eAgFnSc7d1	hudební
stanicí	stanice	k1gFnSc7	stanice
MTV	MTV	kA	MTV
na	na	k7c6	na
pár	pár	k4xCyI	pár
dílech	dílo	k1gNnPc6	dílo
série	série	k1gFnSc2	série
Punk	punk	k1gInSc1	punk
<g/>
́	́	k?	́
<g/>
d	d	k?	d
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Kelly	Kella	k1gFnSc2	Kella
Osbourne	Osbourn	k1gInSc5	Osbourn
<g/>
,	,	kIx,	,
Justina	Justin	k1gMnSc4	Justin
Biebera	Bieber	k1gMnSc4	Bieber
<g/>
,	,	kIx,	,
Khloé	Khloé	k1gNnPc7	Khloé
Kardashian	Kardashian	k1gInSc1	Kardashian
nebo	nebo	k8xC	nebo
Liama	Liama	k1gFnSc1	Liama
Hemswortha	Hemswortha	k1gFnSc1	Hemswortha
<g/>
.	.	kIx.	.
</s>
<s>
Adele	Adele	k1gNnSc1	Adele
<g/>
,	,	kIx,	,
Miley	Mileum	k1gNnPc7	Mileum
Cyrus	Cyrus	k1gInSc1	Cyrus
<g/>
,	,	kIx,	,
Darren	Darrna	k1gFnPc2	Darrna
Criss	Crissa	k1gFnPc2	Crissa
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Matthews	Matthewsa	k1gFnPc2	Matthewsa
Band	band	k1gInSc1	band
<g/>
,	,	kIx,	,
Maroon	Maroon	k1gInSc1	Maroon
5	[number]	k4	5
<g/>
,	,	kIx,	,
Elvis	Elvis	k1gMnSc1	Elvis
Costello	Costello	k1gNnSc1	Costello
<g/>
,	,	kIx,	,
Pete	Pete	k1gFnSc1	Pete
Seeger	Seeger	k1gMnSc1	Seeger
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
Morello	Morello	k1gNnSc1	Morello
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ke	k	k7c3	k
<g/>
$	$	kIx~	$
<g/>
ha	ha	kA	ha
přispěli	přispět	k5eAaPmAgMnP	přispět
do	do	k7c2	do
alba	album	k1gNnSc2	album
Chimnes	Chimnes	k1gMnSc1	Chimnes
of	of	k?	of
Freedom	Freedom	k1gInSc1	Freedom
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
čest	čest	k1gFnSc4	čest
Bobu	Bob	k1gMnSc3	Bob
Dylanovi	Dylan	k1gMnSc3	Dylan
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nové	nový	k2eAgFnPc4d1	nová
nahrávky	nahrávka	k1gFnPc4	nahrávka
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
)	)	kIx)	)
a	a	k8xC	a
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnPc2	International
(	(	kIx(	(
<g/>
veškeré	veškerý	k3xTgInPc1	veškerý
zisky	zisk	k1gInPc1	zisk
jdou	jít	k5eAaImIp3nP	jít
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
účet	účet	k1gInSc4	účet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4-CD	[number]	k4	4-CD
set	set	k1gInSc1	set
vyšel	vyjít	k5eAaPmAgInS	vyjít
24.1	[number]	k4	24.1
<g/>
.2012	.2012	k4	.2012
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Miley	k1gInPc4	Miley
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
písničku	písnička	k1gFnSc4	písnička
'	'	kIx"	'
<g/>
You	You	k1gFnSc4	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Gonna	Gonen	k2eAgFnSc1d1	Gonna
Make	Make	k1gFnSc1	Make
Me	Me	k1gFnSc2	Me
Lonesome	Lonesom	k1gInSc5	Lonesom
When	When	k1gNnSc1	When
You	You	k1gMnPc1	You
Go	Go	k1gFnSc2	Go
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kinech	kino	k1gNnPc6	kino
mají	mít	k5eAaImIp3nP	mít
premiéru	premiéra	k1gFnSc4	premiéra
filmy	film	k1gInPc1	film
LOL	LOL	kA	LOL
a	a	k8xC	a
So	So	kA	So
Undercover	Undercover	k1gInSc1	Undercover
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
s	s	k7c7	s
Miley	Mile	k1gMnPc7	Mile
Cyrus	Cyrus	k1gInSc4	Cyrus
v	v	k7c4	v
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
remake	remake	k1gFnSc4	remake
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
francouzského	francouzský	k2eAgInSc2d1	francouzský
filmu	film	k1gInSc2	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Mile	k1gMnPc4	Mile
zde	zde	k6eAd1	zde
hraje	hrát	k5eAaImIp3nS	hrát
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
přátelí	přátelit	k5eAaImIp3nS	přátelit
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
špatnými	špatný	k2eAgMnPc7d1	špatný
teenagery	teenager	k1gMnPc7	teenager
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
kouří	kouřit	k5eAaImIp3nS	kouřit
<g/>
,	,	kIx,	,
pije	pít	k5eAaImIp3nS	pít
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c6	o
sexu	sex	k1gInSc6	sex
<g/>
,	,	kIx,	,
užívá	užívat	k5eAaImIp3nS	užívat
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
chodí	chodit	k5eAaImIp3nS	chodit
za	za	k7c4	za
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
přese	přese	k7c4	přese
všechno	všechen	k3xTgNnSc4	všechen
snaží	snažit	k5eAaImIp3nP	snažit
omlouvat	omlouvat	k5eAaImF	omlouvat
a	a	k8xC	a
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
americkou	americký	k2eAgFnSc4d1	americká
premiéru	premiéra	k1gFnSc4	premiéra
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
So	So	kA	So
Undercover	Undercover	k1gInSc1	Undercover
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
Miley	Mile	k2eAgFnPc4d1	Mile
soukromou	soukromý	k2eAgFnSc4d1	soukromá
detektivku	detektivka	k1gFnSc4	detektivka
<g/>
.	.	kIx.	.
</s>
<s>
FBI	FBI	kA	FBI
ji	on	k3xPp3gFnSc4	on
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
urovnání	urovnání	k1gNnSc4	urovnání
otcových	otcův	k2eAgInPc2d1	otcův
dluhů	dluh	k1gInPc2	dluh
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
přestrojení	přestrojení	k1gNnSc6	přestrojení
za	za	k7c4	za
studentku	studentka	k1gFnSc4	studentka
prestižní	prestižní	k2eAgFnSc2d1	prestižní
univerzity	univerzita	k1gFnSc2	univerzita
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
obstará	obstarat	k5eAaPmIp3nS	obstarat
důležité	důležitý	k2eAgFnPc4d1	důležitá
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
premiéra	premiéra	k1gFnSc1	premiéra
je	být	k5eAaImIp3nS	být
naplánována	naplánovat	k5eAaBmNgFnS	naplánovat
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
Miley	Mile	k1gMnPc7	Mile
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
hudbě	hudba	k1gFnSc3	hudba
když	když	k8xS	když
nahrála	nahrát	k5eAaBmAgFnS	nahrát
akustické	akustický	k2eAgFnPc4d1	akustická
covery	covera	k1gFnPc4	covera
svých	svůj	k3xOyFgFnPc2	svůj
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
písniček	písnička	k1gFnPc2	písnička
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
později	pozdě	k6eAd2	pozdě
průběžně	průběžně	k6eAd1	průběžně
nahrávala	nahrávat	k5eAaImAgFnS	nahrávat
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
také	také	k9	také
objevila	objevit	k5eAaPmAgFnS	objevit
jako	jako	k9	jako
hostující	hostující	k2eAgFnSc1d1	hostující
postava	postava	k1gFnSc1	postava
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Dva	dva	k4xCgInPc1	dva
a	a	k8xC	a
Půl	půl	k1xP	půl
Chlapa	chlap	k1gMnSc2	chlap
jako	jako	k8xC	jako
dívka	dívka	k1gFnSc1	dívka
Missi	Misse	k1gFnSc4	Misse
<g/>
,	,	kIx,	,
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
postavy	postava	k1gFnSc2	postava
Jakea	Jakea	k1gFnSc1	Jakea
Harpera	Harpera	k1gFnSc1	Harpera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
velké	velký	k2eAgNnSc1d1	velké
rozrušení	rozrušení	k1gNnSc1	rozrušení
zasnoubení	zasnoubení	k1gNnSc2	zasnoubení
Miley	Milea	k1gMnSc2	Milea
a	a	k8xC	a
jejího	její	k3xOp3gMnSc2	její
přítele	přítel	k1gMnSc2	přítel
Liama	Liam	k1gMnSc2	Liam
Hemswortha	Hemsworth	k1gMnSc2	Hemsworth
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
zasnoubení	zasnoubení	k1gNnPc1	zasnoubení
ale	ale	k9	ale
dlouho	dlouho	k6eAd1	dlouho
nevydrželo	vydržet	k5eNaPmAgNnS	vydržet
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
září	září	k1gNnSc2	září
2013	[number]	k4	2013
jejich	jejich	k3xOp3gNnSc2	jejich
management	management	k1gInSc1	management
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
jejich	jejich	k3xOp3gNnSc4	jejich
definitivní	definitivní	k2eAgNnSc4d1	definitivní
zrušení	zrušení	k1gNnSc4	zrušení
zásnub	zásnuba	k1gFnPc2	zásnuba
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
rozchod	rozchod	k1gInSc4	rozchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
Miley	Mile	k1gMnPc7	Mile
šokovala	šokovat	k5eAaBmAgFnS	šokovat
svým	svůj	k3xOyFgInSc7	svůj
novým	nový	k2eAgInSc7d1	nový
vzhledem	vzhled	k1gInSc7	vzhled
-	-	kIx~	-
ostříhala	ostříhat	k5eAaPmAgFnS	ostříhat
se	se	k3xPyFc4	se
nakrátko	nakrátko	k6eAd1	nakrátko
a	a	k8xC	a
barvy	barva	k1gFnPc4	barva
obarvila	obarvit	k5eAaPmAgFnS	obarvit
na	na	k7c4	na
blond	blond	k1gInSc4	blond
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
ostříhané	ostříhaný	k2eAgInPc4d1	ostříhaný
vlasy	vlas	k1gInPc4	vlas
později	pozdě	k6eAd2	pozdě
darovala	darovat	k5eAaPmAgFnS	darovat
charitě	charita	k1gFnSc6	charita
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
to	ten	k3xDgNnSc4	ten
okomentovala	okomentovat	k5eAaPmAgFnS	okomentovat
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
cítí	cítit	k5eAaImIp3nS	cítit
svá	svůj	k3xOyFgNnPc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
si	se	k3xPyFc3	se
Miley	Mile	k1gMnPc7	Mile
najala	najmout	k5eAaPmAgFnS	najmout
nového	nový	k2eAgMnSc4d1	nový
manažera	manažer	k1gMnSc4	manažer
a	a	k8xC	a
opustila	opustit	k5eAaPmAgFnS	opustit
Hollywood	Hollywood	k1gInSc4	Hollywood
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
RCA	RCA	kA	RCA
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
kterou	který	k3yIgFnSc4	který
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
novou	nový	k2eAgFnSc4d1	nová
desku	deska	k1gFnSc4	deska
-	-	kIx~	-
Bangerz	Bangerz	k1gInSc1	Bangerz
<g/>
.	.	kIx.	.
</s>
<s>
Pilotním	pilotní	k2eAgInSc7d1	pilotní
singlem	singl	k1gInSc7	singl
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
letní	letní	k2eAgFnSc1d1	letní
párty	párty	k1gFnSc1	párty
hymna	hymna	k1gFnSc1	hymna
We	We	k1gFnSc1	We
Can	Can	k1gFnSc1	Can
<g/>
́	́	k?	́
<g/>
t	t	k?	t
Stop	stop	k1gInSc1	stop
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
v	v	k7c4	v
Billboard	billboard	k1gInSc4	billboard
Hot	hot	k0	hot
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Umístil	umístit	k5eAaPmAgInS	umístit
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
UK	UK	kA	UK
nebo	nebo	k8xC	nebo
Novém	nový	k2eAgInSc6d1	nový
Zélandě	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1	hudební
video	video	k1gNnSc1	video
později	pozdě	k6eAd2	pozdě
zlomilo	zlomit	k5eAaPmAgNnS	zlomit
VEVO	VEVO	kA	VEVO
rekordy	rekord	k1gInPc1	rekord
pro	pro	k7c4	pro
nejvíce	hodně	k6eAd3	hodně
zhlédnutí	zhlédnutí	k1gNnSc4	zhlédnutí
během	během	k7c2	během
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
nejrychlejšího	rychlý	k2eAgNnSc2d3	nejrychlejší
dosažení	dosažení	k1gNnSc2	dosažení
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
zhlédnutí	zhlédnutí	k1gNnPc2	zhlédnutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
později	pozdě	k6eAd2	pozdě
sama	sám	k3xTgFnSc1	sám
utvrdila	utvrdit	k5eAaPmAgFnS	utvrdit
svým	svůj	k3xOyFgNnSc7	svůj
hudebním	hudební	k2eAgNnSc7d1	hudební
videem	video	k1gNnSc7	video
k	k	k7c3	k
Wrecking	Wrecking	k1gInSc4	Wrecking
Ball	Balla	k1gFnPc2	Balla
<g/>
.	.	kIx.	.
</s>
<s>
Zahájení	zahájení	k1gNnSc1	zahájení
Bangerz	Bangerza	k1gFnPc2	Bangerza
Tour	Toura	k1gFnPc2	Toura
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Tour	Tour	k1gInSc4	Tour
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
celkem	celek	k1gInSc7	celek
80	[number]	k4	80
koncertů	koncert	k1gInPc2	koncert
na	na	k7c6	na
3	[number]	k4	3
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
i	i	k8xC	i
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
koncert	koncert	k1gInSc1	koncert
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Australském	australský	k2eAgInSc6d1	australský
Perthu	Perth	k1gInSc6	Perth
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
minulém	minulý	k2eAgNnSc6d1	Minulé
Gypsy	gyps	k1gInPc4	gyps
Heart	Hearta	k1gFnPc2	Hearta
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
turné	turné	k1gNnSc1	turné
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
tour	toura	k1gFnPc2	toura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
měla	mít	k5eAaImAgFnS	mít
Miley	Milea	k1gMnSc2	Milea
Cyrus	Cyrus	k1gInSc4	Cyrus
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
mnoho	mnoho	k6eAd1	mnoho
rekvizit	rekvizit	k1gInSc4	rekvizit
<g/>
,	,	kIx,	,
např.	např.	kA	např.
létající	létající	k2eAgInSc1d1	létající
Hotdog	Hotdog	k1gInSc1	Hotdog
<g/>
,	,	kIx,	,
obřího	obří	k2eAgMnSc4d1	obří
psa	pes	k1gMnSc4	pes
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgNnSc1d1	speciální
podium	podium	k1gNnSc1	podium
přímo	přímo	k6eAd1	přímo
mezi	mezi	k7c7	mezi
diváky	divák	k1gMnPc7	divák
na	na	k7c4	na
akustickou	akustický	k2eAgFnSc4d1	akustická
část	část	k1gFnSc4	část
koncertu	koncert	k1gInSc2	koncert
<g/>
,	,	kIx,	,
klouzačku	klouzačka	k1gFnSc4	klouzačka
představující	představující	k2eAgFnSc4d1	představující
její	její	k3xOp3gFnSc4	její
vypláznutý	vypláznutý	k2eAgInSc1d1	vypláznutý
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
koncertu	koncert	k1gInSc2	koncert
sklouzla	sklouznout	k5eAaPmAgFnS	sklouznout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
10	[number]	k4	10
<g/>
.	.	kIx.	.
série	série	k1gFnSc1	série
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
The	The	k1gMnSc2	The
Voice	Voice	k1gMnSc2	Voice
a	a	k8xC	a
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
sérii	série	k1gFnSc3	série
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
přímo	přímo	k6eAd1	přímo
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
porotců	porotce	k1gMnPc2	porotce
<g/>
,	,	kIx,	,
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Gwen	Gwen	k1gNnSc4	Gwen
Stefani	Stefaň	k1gFnSc3	Stefaň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2016	[number]	k4	2016
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
obsazení	obsazení	k1gNnSc3	obsazení
seriálu	seriál	k1gInSc2	seriál
Woodyho	Woody	k1gMnSc2	Woody
Allena	Allen	k1gMnSc2	Allen
Crisis	Crisis	k1gFnSc2	Crisis
in	in	k?	in
Six	Six	k1gMnSc1	Six
Scenes	Scenes	k1gMnSc1	Scenes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
bisexuální	bisexuální	k2eAgNnSc1d1	bisexuální
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
posledním	poslední	k2eAgInSc7d1	poslední
počinem	počin	k1gInSc7	počin
v	v	k7c6	v
diskografii	diskografie	k1gFnSc6	diskografie
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
její	její	k3xOp3gNnSc1	její
experimentální	experimentální	k2eAgNnSc1d1	experimentální
album	album	k1gNnSc1	album
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gInSc1	Cyrus
&	&	k?	&
Her	hra	k1gFnPc2	hra
Dead	Dead	k1gMnSc1	Dead
Petz	Petz	k1gMnSc1	Petz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
recenzi	recenze	k1gFnSc6	recenze
iReportu	iReport	k1gInSc2	iReport
získala	získat	k5eAaPmAgFnS	získat
deska	deska	k1gFnSc1	deska
pět	pět	k4xCc4	pět
bodů	bod	k1gInPc2	bod
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
<g/>
;	;	kIx,	;
autor	autor	k1gMnSc1	autor
článku	článek	k1gInSc2	článek
však	však	k9	však
podotkl	podotknout	k5eAaPmAgMnS	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
neškodilo	škodit	k5eNaImAgNnS	škodit
zkrátit	zkrátit	k5eAaPmF	zkrátit
stopáž	stopáž	k1gFnSc4	stopáž
nahrávky	nahrávka	k1gFnSc2	nahrávka
a	a	k8xC	a
dotáhnout	dotáhnout	k5eAaPmF	dotáhnout
ty	ten	k3xDgInPc4	ten
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
tracky	track	k1gInPc4	track
až	až	k9	až
do	do	k7c2	do
úplného	úplný	k2eAgInSc2d1	úplný
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
také	také	k9	také
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k6eAd1	právě
kvůli	kvůli	k7c3	kvůli
těmto	tento	k3xDgInPc3	tento
faktorům	faktor	k1gInPc3	faktor
bude	být	k5eAaImBp3nS	být
na	na	k7c4	na
dílo	dílo	k1gNnSc4	dílo
nejspíše	nejspíše	k9	nejspíše
brzy	brzy	k6eAd1	brzy
zapomenuto	zapomenout	k5eAaPmNgNnS	zapomenout
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
-	-	kIx~	-
Miley	Milea	k1gFnSc2	Milea
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Movement	Movement	k1gMnSc1	Movement
-	-	kIx~	-
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
EP	EP	kA	EP
The	The	k1gFnSc7	The
Time	Time	k1gNnSc2	Time
of	of	k?	of
Our	Our	k1gMnSc1	Our
Lives	Lives	k1gMnSc1	Lives
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Studiová	studiový	k2eAgFnSc1d1	studiová
alba	alba	k1gFnSc1	alba
Meet	Meeta	k1gFnPc2	Meeta
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gMnSc1	Cyrus
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Breakout	Breakout	k1gMnSc1	Breakout
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Time	Tim	k1gFnSc2	Tim
of	of	k?	of
Our	Our	k1gMnSc1	Our
Lives	Lives	k1gMnSc1	Lives
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
-	-	kIx~	-
EP	EP	kA	EP
Can	Can	k1gFnSc6	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Be	Be	k1gMnSc1	Be
Tamed	Tamed	k1gMnSc1	Tamed
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Bangerz	Bangerza	k1gFnPc2	Bangerza
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gInSc1	Cyrus
&	&	k?	&
Her	hra	k1gFnPc2	hra
Dead	Dead	k1gMnSc1	Dead
Petz	Petz	k1gMnSc1	Petz
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Younger	Younger	k1gMnSc1	Younger
Now	Now	k1gMnSc1	Now
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
Živá	živý	k2eAgNnPc4d1	živé
alba	album	k1gNnPc4	album
Hannah	Hannaha	k1gFnPc2	Hannaha
Montana	Montana	k1gFnSc1	Montana
&	&	k?	&
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gMnSc1	Cyrus
<g/>
:	:	kIx,	:
Best	Best	k1gMnSc1	Best
of	of	k?	of
Both	Both	k1gInSc1	Both
Worlds	Worlds	k1gInSc1	Worlds
Concert	Concert	k1gInSc1	Concert
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
iTunes	iTunes	k1gMnSc1	iTunes
Live	Liv	k1gFnSc2	Liv
from	from	k1gMnSc1	from
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Jako	jako	k8xS	jako
Hannah	Hannah	k1gInSc4	Hannah
Montana	Montana	k1gFnSc1	Montana
Hannah	Hannah	k1gInSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Hannah	Hannah	k1gInSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
2	[number]	k4	2
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Hannah	Hannah	k1gInSc1	Hannah
<g />
.	.	kIx.	.
</s>
<s>
Montana	Montana	k1gFnSc1	Montana
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc1	Movie
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Hannah	Hannah	k1gInSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
3	[number]	k4	3
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Hannah	Hannah	k1gInSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
Forever	Forever	k1gMnSc1	Forever
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Debutové	debutový	k2eAgNnSc1d1	debutové
koncertní	koncertní	k2eAgNnSc1d1	koncertní
turné	turné	k1gNnSc1	turné
Miley	Milea	k1gFnSc2	Milea
Cyrusové	Cyrusový	k2eAgFnSc2d1	Cyrusová
<g/>
,	,	kIx,	,
Best	Best	k1gMnSc1	Best
of	of	k?	of
Both	Both	k1gInSc1	Both
Worlds	Worldsa	k1gFnPc2	Worldsa
Tour	Toura	k1gFnPc2	Toura
<g/>
,	,	kIx,	,
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Miley	k1gInPc1	Miley
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
jako	jako	k9	jako
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
a	a	k8xC	a
jako	jako	k9	jako
své	svůj	k3xOyFgNnSc4	svůj
fiktivní	fiktivní	k2eAgInSc1d1	fiktivní
alter	alter	k1gInSc1	alter
ego	ego	k1gNnSc1	ego
Hannah	Hannah	k1gInSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
cenu	cena	k1gFnSc4	cena
2008	[number]	k4	2008
Billboard	billboard	k1gInSc4	billboard
Touring	Touring	k1gInSc4	Touring
Award	Awardo	k1gNnPc2	Awardo
za	za	k7c4	za
průlomové	průlomový	k2eAgNnSc4d1	průlomové
představení	představení	k1gNnSc4	představení
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
zpracováno	zpracovat	k5eAaPmNgNnS	zpracovat
v	v	k7c4	v
koncertní	koncertní	k2eAgInSc4d1	koncertní
film	film	k1gInSc4	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
Hannah	Hannaha	k1gFnPc2	Hannaha
Montana	Montana	k1gFnSc1	Montana
&	&	k?	&
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gMnSc1	Cyrus
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Both	Both	k1gInSc1	Both
Worlds	Worlds	k1gInSc1	Worlds
Concert	Concert	k1gInSc1	Concert
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
uvedení	uvedení	k1gNnSc6	uvedení
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
prodejnosti	prodejnost	k1gFnSc6	prodejnost
lístků	lístek	k1gInPc2	lístek
číslem	číslo	k1gNnSc7	číslo
jedna	jeden	k4xCgFnSc1	jeden
a	a	k8xC	a
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
výdělkem	výdělek	k1gInSc7	výdělek
kolem	kolem	k7c2	kolem
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
byl	být	k5eAaImAgInS	být
alespoň	alespoň	k9	alespoň
dočasně	dočasně	k6eAd1	dočasně
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
koncertním	koncertní	k2eAgInSc7d1	koncertní
filmem	film	k1gInSc7	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
(	(	kIx(	(
<g/>
než	než	k8xS	než
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
předstihl	předstihnout	k5eAaPmAgMnS	předstihnout
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
This	This	k1gInSc1	This
Is	Is	k1gMnSc1	Is
It	It	k1gMnSc1	It
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
druhé	druhý	k4xOgNnSc1	druhý
koncertní	koncertní	k2eAgNnSc1d1	koncertní
turné	turné	k1gNnSc1	turné
<g/>
,	,	kIx,	,
Wonder	Wonder	k1gInSc1	Wonder
World	World	k1gInSc1	World
Tour	Tour	k1gInSc4	Tour
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
<g/>
které	který	k3yIgNnSc1	který
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
alter	alter	k1gInSc1	alter
egem	ego	k1gNnSc7	ego
<g/>
,	,	kIx,	,
Hannah	Hannah	k1gInSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgFnSc7	první
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
i	i	k9	i
koncerty	koncert	k1gInPc1	koncert
mimo	mimo	k7c4	mimo
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
Miley	Mile	k2eAgInPc4d1	Mile
Cyrus	Cyrus	k1gInSc4	Cyrus
svoje	svůj	k3xOyFgNnSc4	svůj
třetí	třetí	k4xOgNnSc4	třetí
turné	turné	k1gNnSc4	turné
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Gypsy	gyps	k1gInPc4	gyps
Heart	Hearta	k1gFnPc2	Hearta
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
obsahu	obsah	k1gInSc6	obsah
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
známé	známý	k2eAgFnPc1d1	známá
písně	píseň	k1gFnPc1	píseň
Miley	Milea	k1gFnSc2	Milea
Cyrusové	Cyrusový	k2eAgFnPc1d1	Cyrusová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
cover-verze	covererze	k1gFnSc1	cover-verze
některých	některý	k3yIgFnPc2	některý
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
především	především	k9	především
od	od	k7c2	od
Joan	Joana	k1gFnPc2	Joana
Jett	Jetta	k1gFnPc2	Jetta
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
prvním	první	k4xOgInSc7	první
koncertem	koncert	k1gInSc7	koncert
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
koncerty	koncert	k1gInPc1	koncert
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
na	na	k7c6	na
Filipínách	Filipíny	k1gFnPc6	Filipíny
a	a	k8xC	a
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
koncert	koncert	k1gInSc1	koncert
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2011	[number]	k4	2011
v	v	k7c6	v
Perthu	Perth	k1gInSc6	Perth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
probíhalo	probíhat	k5eAaImAgNnS	probíhat
turné	turné	k1gNnSc1	turné
Milky	Milka	k1gFnSc2	Milka
Milky	Milka	k1gFnSc2	Milka
Milk	Milk	k1gMnSc1	Milk
Tour	Tour	k1gMnSc1	Tour
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
podporovala	podporovat	k5eAaImAgFnS	podporovat
vydané	vydaný	k2eAgFnPc4d1	vydaná
páté	pátá	k1gFnPc4	pátá
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
Miley	Milea	k1gFnSc2	Milea
Cyrus	Cyrus	k1gInSc1	Cyrus
&	&	k?	&
Her	hra	k1gFnPc2	hra
Dead	Dead	k1gMnSc1	Dead
Petz	Petz	k1gMnSc1	Petz
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Miley	k1gInPc4	Miley
navštívila	navštívit	k5eAaPmAgFnS	navštívit
osm	osm	k4xCc4	osm
měst	město	k1gNnPc2	město
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
začalo	začít	k5eAaPmAgNnS	začít
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
a	a	k8xC	a
poslední	poslední	k2eAgNnSc1d1	poslední
vystoupení	vystoupení	k1gNnSc1	vystoupení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
turné	turné	k1gNnSc4	turné
ji	on	k3xPp3gFnSc4	on
podporovali	podporovat	k5eAaImAgMnP	podporovat
The	The	k1gMnPc2	The
Flaming	Flaming	k1gInSc4	Flaming
Lips	Lipsa	k1gFnPc2	Lipsa
a	a	k8xC	a
Dan	Dan	k1gMnSc1	Dan
Deacon	Deacon	k1gMnSc1	Deacon
<g/>
.	.	kIx.	.
</s>
