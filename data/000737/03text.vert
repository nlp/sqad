<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1824	[number]	k4	1824
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1884	[number]	k4	1884
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
období	období	k1gNnSc2	období
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
poměrně	poměrně	k6eAd1	poměrně
zámožné	zámožný	k2eAgFnSc2d1	zámožná
sládkovské	sládkovský	k2eAgFnSc2d1	sládkovská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
raného	raný	k2eAgNnSc2d1	rané
mládí	mládí	k1gNnSc2	mládí
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
hudbě	hudba	k1gFnSc3	hudba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
hře	hra	k1gFnSc3	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
kompozici	kompozice	k1gFnSc4	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiu	studio	k1gNnSc6	studio
u	u	k7c2	u
Josefa	Josef	k1gMnSc2	Josef
Proksche	Proksch	k1gMnSc2	Proksch
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k8xS	jako
koncertní	koncertní	k2eAgMnSc1d1	koncertní
klavírista	klavírista	k1gMnSc1	klavírista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
1861	[number]	k4	1861
pobýval	pobývat	k5eAaImAgMnS	pobývat
jako	jako	k9	jako
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
hudby	hudba	k1gFnSc2	hudba
ve	v	k7c6	v
švédském	švédský	k2eAgInSc6d1	švédský
Göteborgu	Göteborg	k1gInSc6	Göteborg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
mnohostranně	mnohostranně	k6eAd1	mnohostranně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
českého	český	k2eAgInSc2d1	český
společenského	společenský	k2eAgInSc2d1	společenský
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
jako	jako	k9	jako
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
kapelník	kapelník	k1gMnSc1	kapelník
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1866	[number]	k4	1866
<g/>
-	-	kIx~	-
<g/>
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zejména	zejména	k9	zejména
jako	jako	k9	jako
operní	operní	k2eAgMnSc1d1	operní
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesáti	padesát	k4xCc6	padesát
letech	let	k1gInPc6	let
věku	věk	k1gInSc2	věk
zcela	zcela	k6eAd1	zcela
ohluchl	ohluchnout	k5eAaPmAgMnS	ohluchnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
sluchu	sluch	k1gInSc2	sluch
dokázal	dokázat	k5eAaPmAgInS	dokázat
složit	složit	k5eAaPmF	složit
řadu	řada	k1gFnSc4	řada
vrcholných	vrcholný	k2eAgNnPc2d1	vrcholné
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
choroba	choroba	k1gFnSc1	choroba
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
<g/>
,	,	kIx,	,
až	až	k9	až
přešla	přejít	k5eAaPmAgFnS	přejít
v	v	k7c6	v
šílenství	šílenství	k1gNnSc6	šílenství
<g/>
;	;	kIx,	;
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
ústavu	ústav	k1gInSc6	ústav
pro	pro	k7c4	pro
choromyslné	choromyslný	k1gMnPc4	choromyslný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duchu	duch	k1gMnSc6	duch
pozdního	pozdní	k2eAgNnSc2d1	pozdní
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
celým	celý	k2eAgInSc7d1	celý
svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
svébytného	svébytný	k2eAgInSc2d1	svébytný
českého	český	k2eAgInSc2d1	český
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
na	na	k7c6	na
základě	základ	k1gInSc6	základ
domácích	domácí	k2eAgInPc2d1	domácí
i	i	k8xC	i
soudobých	soudobý	k2eAgInPc2d1	soudobý
světových	světový	k2eAgInPc2d1	světový
podnětů	podnět	k1gInPc2	podnět
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
hudebním	hudební	k2eAgInSc7d1	hudební
vzorem	vzor	k1gInSc7	vzor
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
klavírista	klavírista	k1gMnSc1	klavírista
Franz	Franz	k1gMnSc1	Franz
Liszt	Liszt	k1gMnSc1	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgInPc7	svůj
osmi	osm	k4xCc2	osm
dokončenými	dokončený	k2eAgFnPc7d1	dokončená
operami	opera	k1gFnPc7	opera
různých	různý	k2eAgInPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
položil	položit	k5eAaPmAgInS	položit
základ	základ	k1gInSc1	základ
českého	český	k2eAgInSc2d1	český
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
;	;	kIx,	;
největší	veliký	k2eAgFnSc2d3	veliký
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
komická	komický	k2eAgFnSc1d1	komická
opera	opera	k1gFnSc1	opera
z	z	k7c2	z
venkovského	venkovský	k2eAgNnSc2d1	venkovské
prostředí	prostředí	k1gNnSc2	prostředí
Prodaná	Prodaná	k1gFnSc1	Prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
prototyp	prototyp	k1gInSc4	prototyp
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
skladeb	skladba	k1gFnPc2	skladba
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
cyklus	cyklus	k1gInSc1	cyklus
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc4	vlast
a	a	k8xC	a
smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
kvartet	kvartet	k1gInSc4	kvartet
č.	č.	k?	č.
1	[number]	k4	1
e	e	k0	e
moll	moll	k1gNnSc6	moll
"	"	kIx"	"
<g/>
Z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
zásadními	zásadní	k2eAgInPc7d1	zásadní
příspěvky	příspěvek	k1gInPc7	příspěvek
do	do	k7c2	do
vývoje	vývoj	k1gInSc2	vývoj
těchto	tento	k3xDgInPc2	tento
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgInSc2	ten
napsal	napsat	k5eAaBmAgMnS	napsat
také	také	k9	také
četné	četný	k2eAgFnPc4d1	četná
klavírní	klavírní	k2eAgFnPc4d1	klavírní
skladby	skladba	k1gFnPc4	skladba
a	a	k8xC	a
vokální	vokální	k2eAgNnPc4d1	vokální
díla	dílo	k1gNnPc4	dílo
pro	pro	k7c4	pro
sbory	sbor	k1gInPc4	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jeho	jeho	k3xOp3gNnSc1	jeho
působení	působení	k1gNnSc1	působení
i	i	k8xC	i
tvorba	tvorba	k1gFnSc1	tvorba
vyvolávaly	vyvolávat	k5eAaImAgFnP	vyvolávat
bouřlivé	bouřlivý	k2eAgFnPc4d1	bouřlivá
polemiky	polemika	k1gFnPc4	polemika
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
ještě	ještě	k9	ještě
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
předním	přední	k2eAgMnSc7d1	přední
reprezentantem	reprezentant	k1gMnSc7	reprezentant
české	český	k2eAgFnSc2d1	Česká
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
národní	národní	k2eAgFnSc2d1	národní
kultury	kultura	k1gFnSc2	kultura
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
následující	následující	k2eAgFnPc4d1	následující
generace	generace	k1gFnPc4	generace
českých	český	k2eAgMnPc2d1	český
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
i	i	k8xC	i
osobnost	osobnost	k1gFnSc1	osobnost
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
předmětem	předmět	k1gInSc7	předmět
soustavné	soustavný	k2eAgFnSc3d1	soustavná
pozornosti	pozornost	k1gFnSc3	pozornost
odborníků	odborník	k1gMnPc2	odborník
i	i	k8xC	i
interpretů	interpret	k1gMnPc2	interpret
<g/>
,	,	kIx,	,
těší	těšit	k5eAaImIp3nS	těšit
se	se	k3xPyFc4	se
značné	značný	k2eAgFnSc3d1	značná
oblibě	obliba	k1gFnSc3	obliba
u	u	k7c2	u
posluchačů	posluchač	k1gMnPc2	posluchač
a	a	k8xC	a
pronikly	proniknout	k5eAaPmAgFnP	proniknout
i	i	k9	i
do	do	k7c2	do
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
povědomí	povědomí	k1gNnSc2	povědomí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
se	se	k3xPyFc4	se
však	však	k9	však
zájem	zájem	k1gInSc1	zájem
o	o	k7c6	o
Smetanu	smetan	k1gInSc6	smetan
nerovná	rovnat	k5eNaImIp3nS	rovnat
proslulosti	proslulost	k1gFnPc4	proslulost
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
nebo	nebo	k8xC	nebo
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
kráter	kráter	k1gInSc1	kráter
Smetana	Smetana	k1gMnSc1	Smetana
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
planetka	planetka	k1gFnSc1	planetka
2047	[number]	k4	2047
Smetana	Smetana	k1gMnSc1	Smetana
a	a	k8xC	a
také	také	k6eAd1	také
(	(	kIx(	(
<g/>
k	k	k7c3	k
r.	r.	kA	r.
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
vlak	vlak	k1gInSc1	vlak
ČD	ČD	kA	ČD
a	a	k8xC	a
ÖBB	ÖBB	kA	ÖBB
railjet	railjet	k1gInSc1	railjet
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
přes	přes	k7c4	přes
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
Wien	Wien	k1gNnSc4	Wien
do	do	k7c2	do
Grazu	Graz	k1gInSc2	Graz
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
(	(	kIx(	(
<g/>
křtěn	křtěn	k2eAgMnSc1d1	křtěn
jako	jako	k8xC	jako
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
v	v	k7c6	v
česky	česky	k6eAd1	česky
vedené	vedený	k2eAgFnSc6d1	vedená
matrice	matrika	k1gFnSc6	matrika
psáno	psát	k5eAaImNgNnS	psát
jako	jako	k8xS	jako
Frydrych	Frydrych	k1gMnSc1	Frydrych
<g/>
)	)	kIx)	)
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1824	[number]	k4	1824
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
pivovaru	pivovar	k1gInSc2	pivovar
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
do	do	k7c2	do
poměrně	poměrně	k6eAd1	poměrně
zámožné	zámožný	k2eAgFnSc2d1	zámožná
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
František	František	k1gMnSc1	František
(	(	kIx(	(
<g/>
Franz	Franz	k1gMnSc1	Franz
<g/>
)	)	kIx)	)
Smetana	Smetana	k1gMnSc1	Smetana
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1777	[number]	k4	1777
Sadová	sadový	k2eAgFnSc1d1	Sadová
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1857	[number]	k4	1857
Nové	Nová	k1gFnSc2	Nová
Město	město	k1gNnSc4	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
sládkem	sládek	k1gMnSc7	sládek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
praxi	praxe	k1gFnSc6	praxe
si	se	k3xPyFc3	se
pronajal	pronajmout	k5eAaPmAgMnS	pronajmout
postupně	postupně	k6eAd1	postupně
pivovary	pivovar	k1gInPc4	pivovar
ve	v	k7c6	v
slezské	slezský	k2eAgFnSc6d1	Slezská
Nise	Nisa	k1gFnSc6	Nisa
<g/>
,	,	kIx,	,
Chvalkovicích	Chvalkovice	k1gFnPc6	Chvalkovice
<g/>
,	,	kIx,	,
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
a	a	k8xC	a
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1823	[number]	k4	1823
pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
nájmu	nájem	k1gInSc6	nájem
panský	panský	k2eAgInSc1d1	panský
(	(	kIx(	(
<g/>
valdštejnský	valdštejnský	k2eAgInSc1d1	valdštejnský
<g/>
)	)	kIx)	)
pivovar	pivovar	k1gInSc1	pivovar
ve	v	k7c6	v
východočeské	východočeský	k2eAgFnSc6d1	Východočeská
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
třikrát	třikrát	k6eAd1	třikrát
ženatý	ženatý	k2eAgMnSc1d1	ženatý
(	(	kIx(	(
<g/>
s	s	k7c7	s
Annou	Anna	k1gFnSc7	Anna
Bartoníčkovou	Bartoníčková	k1gFnSc7	Bartoníčková
<g/>
,	,	kIx,	,
Ludmilou	Ludmila	k1gFnSc7	Ludmila
Exnerovou	Exnerová	k1gFnSc7	Exnerová
a	a	k8xC	a
Barborou	Barbora	k1gFnSc7	Barbora
Lynkovou	Lynková	k1gFnSc7	Lynková
<g/>
)	)	kIx)	)
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
celkem	celkem	k6eAd1	celkem
18	[number]	k4	18
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
však	však	k9	však
sedm	sedm	k4xCc1	sedm
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
dětském	dětský	k2eAgInSc6d1	dětský
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
byl	být	k5eAaImAgMnS	být
třetí	třetí	k4xOgNnSc4	třetí
dítě	dítě	k1gNnSc4	dítě
z	z	k7c2	z
třetího	třetí	k4xOgNnSc2	třetí
manželství	manželství	k1gNnSc2	manželství
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
jedenácté	jedenáctý	k4xOgFnPc4	jedenáctý
<g/>
)	)	kIx)	)
a	a	k8xC	a
první	první	k4xOgMnSc1	první
přeživší	přeživší	k2eAgMnSc1d1	přeživší
syn	syn	k1gMnSc1	syn
<g/>
;	;	kIx,	;
vedle	vedle	k7c2	vedle
pěti	pět	k4xCc2	pět
nevlastních	vlastní	k2eNgFnPc2d1	nevlastní
sester	sestra	k1gFnPc2	sestra
z	z	k7c2	z
otcova	otcův	k2eAgInSc2d1	otcův
druhého	druhý	k4xOgNnSc2	druhý
manželství	manželství	k1gNnSc2	manželství
(	(	kIx(	(
<g/>
Anna	Anna	k1gFnSc1	Anna
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
,	,	kIx,	,
Žofie	Žofie	k1gFnSc1	Žofie
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
a	a	k8xC	a
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
sestry	sestra	k1gFnPc4	sestra
Albínu	albín	k1gInSc2	albín
<g/>
,	,	kIx,	,
Barboru	Barbora	k1gFnSc4	Barbora
(	(	kIx(	(
<g/>
Betty	Betty	k1gFnSc7	Betty
<g/>
)	)	kIx)	)
a	a	k8xC	a
Františku	Františka	k1gFnSc4	Františka
a	a	k8xC	a
mladší	mladý	k2eAgMnPc4d2	mladší
bratry	bratr	k1gMnPc4	bratr
Antonína	Antonín	k1gMnSc4	Antonín
a	a	k8xC	a
Karla	Karel	k1gMnSc4	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
Smetanovi	Smetanův	k2eAgMnPc1d1	Smetanův
rodiče	rodič	k1gMnPc1	rodič
měli	mít	k5eAaImAgMnP	mít
jisté	jistý	k2eAgNnSc4d1	jisté
hudební	hudební	k2eAgNnSc4d1	hudební
nadání	nadání	k1gNnSc4	nadání
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Barbora	Barbora	k1gFnSc1	Barbora
Lynková	Lynková	k1gFnSc1	Lynková
(	(	kIx(	(
<g/>
1791	[number]	k4	1791
Miletín	Miletína	k1gFnPc2	Miletína
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1864	[number]	k4	1864
Mladá	mladá	k1gFnSc1	mladá
Boleslav	Boleslav	k1gMnSc1	Boleslav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocházela	pocházet	k5eAaImAgFnS	pocházet
ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
rodu	rod	k1gInSc2	rod
jako	jako	k8xS	jako
skladatel	skladatel	k1gMnSc1	skladatel
Jiří	Jiří	k1gMnSc1	Jiří
Ignác	Ignác	k1gMnSc1	Ignác
Linek	linka	k1gFnPc2	linka
<g/>
;	;	kIx,	;
otec	otec	k1gMnSc1	otec
nebyl	být	k5eNaImAgMnS	být
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
muzikantské	muzikantský	k2eAgFnSc2d1	muzikantská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nadšeně	nadšeně	k6eAd1	nadšeně
amatérsky	amatérsky	k6eAd1	amatérsky
pěstoval	pěstovat	k5eAaImAgMnS	pěstovat
komorní	komorní	k2eAgFnSc4d1	komorní
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
od	od	k7c2	od
útlého	útlý	k2eAgInSc2d1	útlý
věku	věk	k1gInSc2	věk
zasvěcoval	zasvěcovat	k5eAaImAgMnS	zasvěcovat
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
v	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
nechal	nechat	k5eAaPmAgMnS	nechat
Bedřichovi	Bedřichův	k2eAgMnPc1d1	Bedřichův
dávat	dávat	k5eAaImF	dávat
hodiny	hodina	k1gFnPc4	hodina
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
od	od	k7c2	od
místního	místní	k2eAgMnSc2d1	místní
učitele	učitel	k1gMnSc2	učitel
a	a	k8xC	a
hudebního	hudební	k2eAgMnSc2d1	hudební
organizátora	organizátor	k1gMnSc2	organizátor
Jana	Jan	k1gMnSc2	Jan
Chmelíka	Chmelík	k1gMnSc2	Chmelík
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
hrál	hrát	k5eAaImAgMnS	hrát
chlapec	chlapec	k1gMnSc1	chlapec
při	při	k7c6	při
domácím	domácí	k2eAgNnSc6d1	domácí
provedení	provedení	k1gNnSc6	provedení
Haydnova	Haydnův	k2eAgInSc2d1	Haydnův
smyčcového	smyčcový	k2eAgInSc2d1	smyčcový
kvartetu	kvartet	k1gInSc2	kvartet
part	parta	k1gFnPc2	parta
prvních	první	k4xOgFnPc2	první
houslí	housle	k1gFnPc2	housle
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1830	[number]	k4	1830
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
poprvé	poprvé	k6eAd1	poprvé
veřejně	veřejně	k6eAd1	veřejně
na	na	k7c6	na
hudební	hudební	k2eAgFnSc6d1	hudební
akademii	akademie	k1gFnSc6	akademie
litomyšlských	litomyšlský	k2eAgMnPc2d1	litomyšlský
filosofů	filosof	k1gMnPc2	filosof
s	s	k7c7	s
předehrou	předehra	k1gFnSc7	předehra
k	k	k7c3	k
Auberově	Auberův	k2eAgFnSc3d1	Auberův
opeře	opera	k1gFnSc3	opera
Němá	němý	k2eAgFnSc1d1	němá
z	z	k7c2	z
Portici	Portice	k1gFnSc3	Portice
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
začal	začít	k5eAaPmAgMnS	začít
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
komponovat	komponovat	k5eAaImF	komponovat
i	i	k9	i
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
kousky	kousek	k1gInPc4	kousek
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
skladba	skladba	k1gFnSc1	skladba
-	-	kIx~	-
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
kvapík	kvapík	k1gInSc1	kvapík
D	D	kA	D
dur	dur	k1gNnSc4	dur
-	-	kIx~	-
však	však	k9	však
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k9	až
v	v	k7c6	v
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
roku	rok	k1gInSc2	rok
1831	[number]	k4	1831
<g/>
,	,	kIx,	,
když	když	k8xS	když
otec	otec	k1gMnSc1	otec
získal	získat	k5eAaPmAgMnS	získat
výhodnější	výhodný	k2eAgInSc4d2	výhodnější
čtyřletý	čtyřletý	k2eAgInSc4d1	čtyřletý
nájem	nájem	k1gInSc4	nájem
pivovaru	pivovar	k1gInSc2	pivovar
hraběte	hrabě	k1gMnSc2	hrabě
Černína	Černín	k1gMnSc2	Černín
<g/>
.	.	kIx.	.
</s>
<s>
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
neměl	mít	k5eNaImAgInS	mít
tak	tak	k6eAd1	tak
bohatý	bohatý	k2eAgInSc1d1	bohatý
kulturní	kulturní	k2eAgInSc1d1	kulturní
život	život	k1gInSc1	život
jako	jako	k8xC	jako
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
,	,	kIx,	,
Smetana	Smetana	k1gMnSc1	Smetana
však	však	k9	však
zde	zde	k6eAd1	zde
vedle	vedle	k7c2	vedle
pravidelné	pravidelný	k2eAgFnSc2d1	pravidelná
školní	školní	k2eAgFnSc2d1	školní
docházky	docházka	k1gFnSc2	docházka
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgFnSc2d1	hlavní
školy	škola	k1gFnSc2	škola
a	a	k8xC	a
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
)	)	kIx)	)
získával	získávat	k5eAaImAgMnS	získávat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
vyučování	vyučování	k1gNnSc4	vyučování
hře	hra	k1gFnSc3	hra
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
violu	viola	k1gFnSc4	viola
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
od	od	k7c2	od
místního	místní	k2eAgMnSc2d1	místní
regenschoriho	regenschori	k1gMnSc2	regenschori
Františka	František	k1gMnSc2	František
Ikavce	ikavec	k1gMnSc2	ikavec
(	(	kIx(	(
<g/>
1800	[number]	k4	1800
<g/>
-	-	kIx~	-
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
jej	on	k3xPp3gMnSc4	on
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
i	i	k8xC	i
zpěvu	zpěv	k1gInSc2	zpěv
a	a	k8xC	a
hoch	hoch	k1gMnSc1	hoch
zpíval	zpívat	k5eAaImAgMnS	zpívat
sopránová	sopránový	k2eAgNnPc4d1	sopránové
sóla	sólo	k1gNnPc4	sólo
v	v	k7c6	v
proboštském	proboštský	k2eAgInSc6d1	proboštský
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
se	se	k3xPyFc4	se
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
rodina	rodina	k1gFnSc1	rodina
úzce	úzko	k6eAd1	úzko
stýkala	stýkat	k5eAaImAgFnS	stýkat
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
Kolářovou	Kolářová	k1gFnSc7	Kolářová
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
pocházela	pocházet	k5eAaImAgFnS	pocházet
jeho	jeho	k3xOp3gFnSc1	jeho
pozdější	pozdní	k2eAgFnSc1d2	pozdější
první	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
Bedřich	Bedřich	k1gMnSc1	Bedřich
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
zranění	zranění	k1gNnSc4	zranění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zanechalo	zanechat	k5eAaPmAgNnS	zanechat
trvalé	trvalý	k2eAgInPc4d1	trvalý
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
klukovské	klukovský	k2eAgFnSc6d1	klukovská
hře	hra	k1gFnSc6	hra
s	s	k7c7	s
vrstevníky	vrstevník	k1gMnPc7	vrstevník
jim	on	k3xPp3gMnPc3	on
explodovala	explodovat	k5eAaBmAgFnS	explodovat
láhev	láhev	k1gFnSc1	láhev
se	s	k7c7	s
střelným	střelný	k2eAgInSc7d1	střelný
prachem	prach	k1gInSc7	prach
a	a	k8xC	a
Bedřicha	Bedřich	k1gMnSc2	Bedřich
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
střepiny	střepina	k1gFnPc1	střepina
do	do	k7c2	do
pravé	pravý	k2eAgFnSc2d1	pravá
tváře	tvář	k1gFnSc2	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Malý	Malý	k1gMnSc1	Malý
Bedřich	Bedřich	k1gMnSc1	Bedřich
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
si	se	k3xPyFc3	se
zranění	zranění	k1gNnSc4	zranění
ošetřit	ošetřit	k5eAaPmF	ošetřit
omytím	omytí	k1gNnSc7	omytí
v	v	k7c6	v
rybníce	rybník	k1gInSc6	rybník
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
potrestání	potrestání	k1gNnSc4	potrestání
za	za	k7c4	za
klukovinu	klukovina	k1gFnSc4	klukovina
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
však	však	k9	však
bolestivý	bolestivý	k2eAgInSc1d1	bolestivý
zánět	zánět	k1gInSc1	zánět
postihující	postihující	k2eAgFnSc4d1	postihující
horní	horní	k2eAgFnSc4d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgFnSc4d1	dolní
čelist	čelist	k1gFnSc4	čelist
a	a	k8xC	a
spánkovou	spánkový	k2eAgFnSc4d1	spánková
kost	kost	k1gFnSc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
byla	být	k5eAaImAgFnS	být
deformita	deformita	k1gFnSc1	deformita
tváře	tvář	k1gFnSc2	tvář
a	a	k8xC	a
přetrvávající	přetrvávající	k2eAgFnSc4d1	přetrvávající
bolestivost	bolestivost	k1gFnSc4	bolestivost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyšším	vysoký	k2eAgInSc6d2	vyšší
věku	věk	k1gInSc6	věk
maskoval	maskovat	k5eAaBmAgMnS	maskovat
Smetana	Smetana	k1gMnSc1	Smetana
důsledky	důsledek	k1gInPc4	důsledek
zranění	zranění	k1gNnPc4	zranění
plnovousem	plnovous	k1gInSc7	plnovous
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nosil	nosit	k5eAaImAgInS	nosit
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
poněkud	poněkud	k6eAd1	poněkud
nesymetrický	symetrický	k2eNgInSc1d1	nesymetrický
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
některým	některý	k3yIgFnPc3	některý
spekulacím	spekulace	k1gFnPc3	spekulace
se	se	k3xPyFc4	se
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
zranění	zranění	k1gNnSc1	zranění
na	na	k7c6	na
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
postižení	postižení	k1gNnSc6	postižení
hluchotou	hluchota	k1gFnSc7	hluchota
nepodílelo	podílet	k5eNaImAgNnS	podílet
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
skončil	skončit	k5eAaPmAgMnS	skončit
Františku	František	k1gMnSc3	František
Smetanovi	Smetana	k1gMnSc3	Smetana
nájem	nájem	k1gInSc4	nájem
jindřichohradeckého	jindřichohradecký	k2eAgInSc2d1	jindřichohradecký
pivovaru	pivovar	k1gInSc2	pivovar
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
dražbě	dražba	k1gFnSc6	dražba
výhodně	výhodně	k6eAd1	výhodně
koupit	koupit	k5eAaPmF	koupit
svobodnický	svobodnický	k2eAgInSc4d1	svobodnický
statek	statek	k1gInSc4	statek
Růžkovy	Růžkův	k2eAgFnSc2d1	Růžkova
Lhotice	Lhotice	k1gFnSc2	Lhotice
poblíž	poblíž	k7c2	poblíž
Čechtic	Čechtice	k1gFnPc2	Čechtice
se	s	k7c7	s
zámečkem	zámeček	k1gInSc7	zámeček
<g/>
,	,	kIx,	,
pivovarem	pivovar	k1gInSc7	pivovar
a	a	k8xC	a
hostincem	hostinec	k1gInSc7	hostinec
<g/>
.	.	kIx.	.
</s>
<s>
Přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
i	i	k9	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
zde	zde	k6eAd1	zde
však	však	k9	však
trávil	trávit	k5eAaImAgMnS	trávit
jen	jen	k9	jen
školní	školní	k2eAgFnPc4d1	školní
prázdniny	prázdniny	k1gFnPc4	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
gymnaziální	gymnaziální	k2eAgNnSc1d1	gymnaziální
studium	studium	k1gNnSc1	studium
bylo	být	k5eAaImAgNnS	být
často	často	k6eAd1	často
přerušované	přerušovaný	k2eAgNnSc1d1	přerušované
a	a	k8xC	a
málo	málo	k6eAd1	málo
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
<g/>
;	;	kIx,	;
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
druhého	druhý	k4xOgInSc2	druhý
ročníku	ročník	k1gInSc2	ročník
měl	mít	k5eAaImAgInS	mít
-	-	kIx~	-
možná	možná	k9	možná
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nemoci	nemoc	k1gFnSc2	nemoc
-	-	kIx~	-
samé	samý	k3xTgFnPc4	samý
nedostatečné	nedostatečná	k1gFnPc4	nedostatečná
a	a	k8xC	a
školu	škola	k1gFnSc4	škola
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
<g/>
/	/	kIx~	/
<g/>
36	[number]	k4	36
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nedařilo	dařit	k5eNaImAgNnS	dařit
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
propadal	propadat	k5eAaImAgMnS	propadat
depresím	deprese	k1gFnPc3	deprese
<g/>
;	;	kIx,	;
nakonec	nakonec	k6eAd1	nakonec
uprosil	uprosit	k5eAaPmAgInS	uprosit
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
odtud	odtud	k6eAd1	odtud
ještě	ještě	k9	ještě
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
odvedl	odvést	k5eAaPmAgMnS	odvést
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
doporučení	doporučení	k1gNnSc4	doporučení
svého	svůj	k3xOyFgMnSc2	svůj
bratrance	bratranec	k1gMnSc2	bratranec
<g/>
,	,	kIx,	,
premonstráta	premonstrát	k1gMnSc2	premonstrát
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Josefa	Josef	k1gMnSc2	Josef
Františka	František	k1gMnSc2	František
Smetany	Smetana	k1gMnSc2	Smetana
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
-	-	kIx~	-
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
řádové	řádový	k2eAgNnSc4d1	řádové
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Německém	německý	k2eAgInSc6d1	německý
Brodě	Brod	k1gInSc6	Brod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
líbilo	líbit	k5eAaImAgNnS	líbit
více	hodně	k6eAd2	hodně
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgInS	seznámit
mj.	mj.	kA	mj.
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Havlíčkem	Havlíček	k1gMnSc7	Havlíček
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgNnSc4d1	hudební
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
omezoval	omezovat	k5eAaImAgMnS	omezovat
na	na	k7c4	na
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
P.	P.	kA	P.
Šindeláře	Šindelář	k1gMnSc2	Šindelář
a	a	k8xC	a
prázdninové	prázdninový	k2eAgFnSc2d1	prázdninová
lekce	lekce	k1gFnSc2	lekce
u	u	k7c2	u
čechtického	čechtický	k2eAgMnSc2d1	čechtický
učitele	učitel	k1gMnSc2	učitel
Jana	Jan	k1gMnSc2	Jan
Syrového	syrový	k2eAgMnSc2d1	syrový
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimního	podzimní	k2eAgInSc2d1	podzimní
semestru	semestr	k1gInSc2	semestr
1839	[number]	k4	1839
přecházela	přecházet	k5eAaImAgFnS	přecházet
řada	řada	k1gFnSc1	řada
Smetanových	Smetanových	k2eAgMnPc2d1	Smetanových
přátel	přítel	k1gMnPc2	přítel
z	z	k7c2	z
Německého	německý	k2eAgInSc2d1	německý
Brodu	Brod	k1gInSc2	Brod
na	na	k7c4	na
akademické	akademický	k2eAgNnSc4d1	akademické
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
hoch	hoch	k1gMnSc1	hoch
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
rovněž	rovněž	k9	rovněž
zařídil	zařídit	k5eAaPmAgInS	zařídit
přestup	přestup	k1gInSc1	přestup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
plné	plný	k2eAgFnSc6d1	plná
kulturních	kulturní	k2eAgInPc2d1	kulturní
podnětů	podnět	k1gInPc2	podnět
-	-	kIx~	-
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
měla	mít	k5eAaImAgFnS	mít
pro	pro	k7c4	pro
Smetanu	smetana	k1gFnSc4	smetana
zejména	zejména	k9	zejména
série	série	k1gFnSc2	série
koncertů	koncert	k1gInPc2	koncert
Franze	Franze	k1gFnSc2	Franze
Liszta	Liszto	k1gNnSc2	Liszto
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1840	[number]	k4	1840
-	-	kIx~	-
se	se	k3xPyFc4	se
však	však	k9	však
začal	začít	k5eAaPmAgInS	začít
brzy	brzy	k6eAd1	brzy
zabývat	zabývat	k5eAaImF	zabývat
více	hodně	k6eAd2	hodně
hudbou	hudba	k1gFnSc7	hudba
než	než	k8xS	než
studiem	studio	k1gNnSc7	studio
<g/>
.	.	kIx.	.
</s>
<s>
Oznámil	oznámit	k5eAaPmAgMnS	oznámit
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
řediteli	ředitel	k1gMnSc3	ředitel
Josefu	Josef	k1gMnSc3	Josef
Jungmannovi	Jungmann	k1gMnSc3	Jungmann
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
gymnázia	gymnázium	k1gNnSc2	gymnázium
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
a	a	k8xC	a
hodlá	hodlat	k5eAaImIp3nS	hodlat
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Smetanův	Smetanův	k2eAgMnSc1d1	Smetanův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
přirozeně	přirozeně	k6eAd1	přirozeně
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
odvezl	odvézt	k5eAaPmAgInS	odvézt
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
od	od	k7c2	od
následujícího	následující	k2eAgInSc2d1	následující
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
ho	on	k3xPp3gMnSc4	on
zapsal	zapsat	k5eAaPmAgInS	zapsat
na	na	k7c4	na
premonstrátské	premonstrátský	k2eAgNnSc4d1	premonstrátské
gymnázium	gymnázium	k1gNnSc4	gymnázium
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
profesor	profesor	k1gMnSc1	profesor
Josef	Josef	k1gMnSc1	Josef
František	František	k1gMnSc1	František
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
a	a	k8xC	a
dohledem	dohled	k1gInSc7	dohled
svého	své	k1gNnSc2	své
o	o	k7c4	o
23	[number]	k4	23
let	léto	k1gNnPc2	léto
staršího	starý	k2eAgMnSc2d2	starší
bratrance	bratranec	k1gMnSc2	bratranec
tam	tam	k6eAd1	tam
Bedřich	Bedřich	k1gMnSc1	Bedřich
studia	studio	k1gNnSc2	studio
přece	přece	k9	přece
jen	jen	k6eAd1	jen
dokončil	dokončit	k5eAaPmAgMnS	dokončit
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
nevalným	valný	k2eNgInSc7d1	nevalný
prospěchem	prospěch	k1gInSc7	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
se	se	k3xPyFc4	se
dospívající	dospívající	k2eAgFnSc1d1	dospívající
Smetana	smetana	k1gFnSc1	smetana
pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
svého	svůj	k3xOyFgMnSc2	svůj
profesora	profesor	k1gMnSc2	profesor
humanitních	humanitní	k2eAgInPc2d1	humanitní
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
hudbymilovného	hudbymilovný	k2eAgMnSc2d1	hudbymilovný
Gustava	Gustav	k1gMnSc2	Gustav
Beera	Beer	k1gMnSc2	Beer
<g/>
,	,	kIx,	,
živě	živě	k6eAd1	živě
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c6	na
návštěvách	návštěva	k1gFnPc6	návštěva
<g/>
,	,	kIx,	,
v	v	k7c6	v
salonech	salon	k1gInPc6	salon
a	a	k8xC	a
k	k	k7c3	k
tanci	tanec	k1gInSc3	tanec
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
sám	sám	k3xTgMnSc1	sám
rád	rád	k6eAd1	rád
tancoval	tancovat	k5eAaImAgMnS	tancovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
deníku	deník	k1gInSc2	deník
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
prožil	prožít	k5eAaPmAgMnS	prožít
také	také	k9	také
několik	několik	k4yIc4	několik
citových	citový	k2eAgNnPc2d1	citové
vzplanutí	vzplanutí	k1gNnPc2	vzplanutí
<g/>
,	,	kIx,	,
první	první	k4xOgMnPc1	první
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
sestřenici	sestřenice	k1gFnSc3	sestřenice
Aloisii	Aloisie	k1gFnSc4	Aloisie
Smetanové	Smetanové	k2eAgFnSc4d1	Smetanové
ještě	ještě	k9	ještě
o	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
před	před	k7c7	před
plzeňským	plzeňský	k2eAgInSc7d1	plzeňský
pobytem	pobyt	k1gInSc7	pobyt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
trávil	trávit	k5eAaImAgInS	trávit
u	u	k7c2	u
strýce	strýc	k1gMnSc2	strýc
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
.	.	kIx.	.
</s>
<s>
Aloisii	Aloisie	k1gFnSc4	Aloisie
(	(	kIx(	(
<g/>
Louise	Louis	k1gMnPc4	Louis
<g/>
)	)	kIx)	)
také	také	k9	také
věnoval	věnovat	k5eAaImAgInS	věnovat
Louisinu	Louisin	k2eAgFnSc4d1	Louisina
polku	polka	k1gFnSc4	polka
(	(	kIx(	(
<g/>
Louisen-Polka	Louisen-Polka	k1gFnSc1	Louisen-Polka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgFnSc1	první
v	v	k7c6	v
úplnosti	úplnost	k1gFnSc6	úplnost
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
skladbou	skladba	k1gFnSc7	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
1842	[number]	k4	1842
se	se	k3xPyFc4	se
však	však	k9	však
datuje	datovat	k5eAaImIp3nS	datovat
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
Kateřině	Kateřina	k1gFnSc3	Kateřina
Kolářové	Kolářová	k1gFnSc3	Kolářová
<g/>
,	,	kIx,	,
dceři	dcera	k1gFnSc3	dcera
otcova	otcův	k2eAgMnSc2d1	otcův
přítele	přítel	k1gMnSc2	přítel
a	a	k8xC	a
talentované	talentovaný	k2eAgFnSc3d1	talentovaná
pianistce	pianistka	k1gFnSc3	pianistka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
Smetana	Smetana	k1gMnSc1	Smetana
složil	složit	k5eAaPmAgMnS	složit
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
drobných	drobné	k1gInPc2	drobné
klavírních	klavírní	k2eAgFnPc2d1	klavírní
skladeb	skladba	k1gFnPc2	skladba
většinou	většinou	k6eAd1	většinou
tanečních	taneční	k2eAgInPc2d1	taneční
(	(	kIx(	(
<g/>
polky	polka	k1gFnPc1	polka
<g/>
,	,	kIx,	,
kvapíky	kvapík	k1gInPc1	kvapík
<g/>
,	,	kIx,	,
čtverylky	čtverylka	k1gFnPc1	čtverylka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
zejména	zejména	k9	zejména
Louisina	Louisin	k2eAgFnSc1d1	Louisina
polka	polka	k1gFnSc1	polka
<g/>
,	,	kIx,	,
Jiřinková	jiřinkový	k2eAgFnSc1d1	Jiřinková
polka	polka	k1gFnSc1	polka
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiřinkových	jiřinkový	k2eAgFnPc2d1	Jiřinková
slavností	slavnost	k1gFnPc2	slavnost
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
Skalici	Skalice	k1gFnSc6	Skalice
<g/>
)	)	kIx)	)
a	a	k8xC	a
polka	polka	k1gFnSc1	polka
Ze	z	k7c2	z
studentského	studentský	k2eAgInSc2d1	studentský
života	život	k1gInSc2	život
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
již	již	k6eAd1	již
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
značné	značný	k2eAgFnSc2d1	značná
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgInS	pokoušet
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
o	o	k7c4	o
první	první	k4xOgFnPc4	první
orchestrální	orchestrální	k2eAgFnPc4d1	orchestrální
skladby	skladba	k1gFnPc4	skladba
(	(	kIx(	(
<g/>
Menuet	menuet	k1gInSc4	menuet
B	B	kA	B
dur	dur	k1gNnSc1	dur
a	a	k8xC	a
Galop	galop	k1gInSc1	galop
bajadérek	bajadérka	k1gFnPc2	bajadérka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
náročnější	náročný	k2eAgInPc4d2	náročnější
klavírní	klavírní	k2eAgInPc4d1	klavírní
kusy	kus	k1gInPc4	kus
(	(	kIx(	(
<g/>
tři	tři	k4xCgFnPc4	tři
impromptus	impromptus	k1gInSc1	impromptus
1841	[number]	k4	1841
<g/>
-	-	kIx~	-
<g/>
42	[number]	k4	42
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
studií	studie	k1gFnPc2	studie
se	se	k3xPyFc4	se
vzepřel	vzepřít	k5eAaPmAgMnS	vzepřít
přání	přání	k1gNnSc4	přání
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
úředníkem	úředník	k1gMnSc7	úředník
či	či	k8xC	či
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
studiích	studio	k1gNnPc6	studio
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
věnovat	věnovat	k5eAaPmF	věnovat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
deníku	deník	k1gInSc2	deník
si	se	k3xPyFc3	se
tehdy	tehdy	k6eAd1	tehdy
zapsal	zapsat	k5eAaPmAgMnS	zapsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Boží	boží	k2eAgFnSc2d1	boží
budu	být	k5eAaImBp1nS	být
jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
technice	technika	k1gFnSc6	technika
Lisztem	Liszto	k1gNnSc7	Liszto
<g/>
,	,	kIx,	,
v	v	k7c6	v
komponování	komponování	k1gNnSc6	komponování
Mozartem	Mozart	k1gMnSc7	Mozart
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
bydlel	bydlet	k5eAaImAgMnS	bydlet
Smetana	Smetana	k1gMnSc1	Smetana
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
příbuzné	příbuzná	k1gFnSc2	příbuzná
a	a	k8xC	a
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
nuzoval	nuzovat	k5eAaImAgMnS	nuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
jej	on	k3xPp3gMnSc4	on
peněžně	peněžně	k6eAd1	peněžně
nepodporoval	podporovat	k5eNaImAgMnS	podporovat
<g/>
,	,	kIx,	,
nacházel	nacházet	k5eAaImAgMnS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ve	v	k7c6	v
vážných	vážný	k2eAgFnPc6d1	vážná
finančních	finanční	k2eAgFnPc6d1	finanční
potížích	potíž	k1gFnPc6	potíž
a	a	k8xC	a
roku	rok	k1gInSc6	rok
1844	[number]	k4	1844
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
prodat	prodat	k5eAaPmF	prodat
statek	statek	k1gInSc1	statek
v	v	k7c6	v
Růžkových	Růžkových	k2eAgFnPc6d1	Růžkových
Lhoticích	Lhotice	k1gFnPc6	Lhotice
a	a	k8xC	a
přijmout	přijmout	k5eAaPmF	přijmout
opět	opět	k6eAd1	opět
nájem	nájem	k1gInSc4	nájem
pivovaru	pivovar	k1gInSc2	pivovar
v	v	k7c6	v
Obříství	Obříství	k1gNnSc6	Obříství
<g/>
.	.	kIx.	.
</s>
<s>
Mladík	mladík	k1gMnSc1	mladík
se	se	k3xPyFc4	se
však	však	k9	však
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
umělecké	umělecký	k2eAgFnSc2d1	umělecká
společnosti	společnost	k1gFnSc2	společnost
Concordia	Concordium	k1gNnSc2	Concordium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
s	s	k7c7	s
ředitelem	ředitel	k1gMnSc7	ředitel
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
Janem	Jan	k1gMnSc7	Jan
Bedřichem	Bedřich	k1gMnSc7	Bedřich
Kittlem	Kittl	k1gMnSc7	Kittl
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jehož	k3xOyRp3gFnSc4	jehož
přímluvu	přímluva	k1gFnSc4	přímluva
se	se	k3xPyFc4	se
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1844	[number]	k4	1844
stal	stát	k5eAaPmAgInS	stát
domácím	domácí	k2eAgMnSc7d1	domácí
učitelem	učitel	k1gMnSc7	učitel
pěti	pět	k4xCc3	pět
dětí	dítě	k1gFnPc2	dítě
hraběte	hrabě	k1gMnSc2	hrabě
Leopolda	Leopold	k1gMnSc2	Leopold
Thuna	Thun	k1gMnSc2	Thun
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
-	-	kIx~	-
<g/>
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebně	hudebně	k6eAd1	hudebně
vesměs	vesměs	k6eAd1	vesměs
málo	málo	k4c1	málo
nadaných	nadaný	k2eAgInPc2d1	nadaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
navázal	navázat	k5eAaPmAgMnS	navázat
mnoho	mnoho	k4c4	mnoho
užitečných	užitečný	k2eAgInPc2d1	užitečný
kontaktů	kontakt	k1gInPc2	kontakt
mezi	mezi	k7c7	mezi
aristokracií	aristokracie	k1gFnSc7	aristokracie
i	i	k8xC	i
hudebníky	hudebník	k1gMnPc7	hudebník
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
Thunových	Thunův	k2eAgMnPc2d1	Thunův
zůstala	zůstat	k5eAaPmAgFnS	zůstat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
jeho	jeho	k3xOp3gFnSc7	jeho
podporovatelkou	podporovatelka	k1gFnSc7	podporovatelka
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hraběcí	hraběcí	k2eAgFnSc7d1	hraběcí
rodinou	rodina	k1gFnSc7	rodina
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
pražské	pražský	k2eAgFnSc6d1	Pražská
rezidenci	rezidence	k1gFnSc6	rezidence
a	a	k8xC	a
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
je	být	k5eAaImIp3nS	být
i	i	k9	i
na	na	k7c4	na
venkovská	venkovský	k2eAgNnPc4d1	venkovské
sídla	sídlo	k1gNnPc4	sídlo
(	(	kIx(	(
<g/>
zámek	zámek	k1gInSc1	zámek
Bon	bona	k1gFnPc2	bona
Repos	Repos	k1gInSc4	Repos
u	u	k7c2	u
Nových	Nových	k2eAgFnPc2d1	Nových
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
,	,	kIx,	,
Ronšperk	Ronšperk	k1gInSc1	Ronšperk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
Anny	Anna	k1gFnSc2	Anna
Kolářové	Kolářové	k2eAgFnSc2d1	Kolářové
<g/>
,	,	kIx,	,
Kateřininy	Kateřinin	k2eAgFnSc2d1	Kateřinina
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
soustavné	soustavný	k2eAgNnSc4d1	soustavné
studium	studium	k1gNnSc4	studium
kompozice	kompozice	k1gFnSc2	kompozice
u	u	k7c2	u
Josefa	Josef	k1gMnSc2	Josef
Proksche	Proksch	k1gMnSc2	Proksch
<g/>
.	.	kIx.	.
</s>
<s>
Proksch	Proksch	k1gMnSc1	Proksch
se	se	k3xPyFc4	se
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
tehdy	tehdy	k6eAd1	tehdy
nejmodernější	moderní	k2eAgFnSc4d3	nejmodernější
učebnici	učebnice	k1gFnSc4	učebnice
Adolfa	Adolf	k1gMnSc2	Adolf
Bernarda	Bernard	k1gMnSc2	Bernard
Marxe	Marx	k1gMnSc2	Marx
a	a	k8xC	a
o	o	k7c4	o
díla	dílo	k1gNnPc4	dílo
Beethovena	Beethoven	k1gMnSc2	Beethoven
<g/>
,	,	kIx,	,
Berlioze	Berlioz	k1gMnSc2	Berlioz
<g/>
,	,	kIx,	,
Chopina	Chopin	k1gMnSc2	Chopin
a	a	k8xC	a
lipského	lipský	k2eAgInSc2d1	lipský
okruhu	okruh	k1gInSc2	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vyprodukoval	vyprodukovat	k5eAaPmAgMnS	vyprodukovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
cvičných	cvičný	k2eAgFnPc2d1	cvičná
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
své	svůj	k3xOyFgFnSc2	svůj
jediné	jediný	k2eAgFnSc2d1	jediná
klavírní	klavírní	k2eAgFnSc2d1	klavírní
sonáty	sonáta	k1gFnSc2	sonáta
g	g	kA	g
moll	moll	k1gNnSc7	moll
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1847	[number]	k4	1847
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
Robert	Robert	k1gMnSc1	Robert
a	a	k8xC	a
Clara	Clara	k1gFnSc1	Clara
Schumannovi	Schumannův	k2eAgMnPc1d1	Schumannův
<g/>
,	,	kIx,	,
ukázal	ukázat	k5eAaPmAgMnS	ukázat
jim	on	k3xPp3gMnPc3	on
Smetana	Smetana	k1gMnSc1	Smetana
sonátu	sonáta	k1gFnSc4	sonáta
k	k	k7c3	k
posouzení	posouzení	k1gNnSc3	posouzení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
však	však	k9	však
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
nepříznivě	příznivě	k6eNd1	příznivě
<g/>
:	:	kIx,	:
shledávali	shledávat	k5eAaImAgMnP	shledávat
ji	on	k3xPp3gFnSc4	on
příliš	příliš	k6eAd1	příliš
berliozovskou	berliozovský	k2eAgFnSc4d1	berliozovský
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Proksche	Proksch	k1gInSc2	Proksch
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
důkladnou	důkladný	k2eAgFnSc4d1	důkladná
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
průpravu	průprava	k1gFnSc4	průprava
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
později	pozdě	k6eAd2	pozdě
postrádal	postrádat	k5eAaImAgMnS	postrádat
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
svých	svůj	k3xOyFgMnPc2	svůj
současníků	současník	k1gMnPc2	současník
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
studijních	studijní	k2eAgFnPc2d1	studijní
prací	práce	k1gFnPc2	práce
psal	psát	k5eAaImAgInS	psát
drobné	drobný	k2eAgNnSc4d1	drobné
příležitostné	příležitostný	k2eAgFnPc4d1	příležitostná
klavírní	klavírní	k2eAgFnPc4d1	klavírní
skladby	skladba	k1gFnPc4	skladba
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Chopina	Chopin	k1gMnSc2	Chopin
<g/>
,	,	kIx,	,
Schumanna	Schumann	k1gMnSc2	Schumann
nebo	nebo	k8xC	nebo
Adolfa	Adolf	k1gMnSc2	Adolf
Henselta	Henselt	k1gMnSc2	Henselt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
cyklus	cyklus	k1gInSc1	cyklus
Bagately	bagatela	k1gFnSc2	bagatela
a	a	k8xC	a
impromptus	impromptus	k1gMnSc1	impromptus
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
)	)	kIx)	)
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
vznikající	vznikající	k2eAgInPc4d1	vznikající
Lístky	lístek	k1gInPc4	lístek
do	do	k7c2	do
památníku	památník	k1gInSc2	památník
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
studia	studio	k1gNnSc2	studio
kompozice	kompozice	k1gFnSc2	kompozice
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
Smetana	Smetana	k1gMnSc1	Smetana
ve	v	k7c6	v
cvičení	cvičení	k1gNnSc6	cvičení
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pokusit	pokusit	k5eAaPmF	pokusit
se	se	k3xPyFc4	se
o	o	k7c4	o
kariéru	kariéra	k1gFnSc4	kariéra
koncertního	koncertní	k2eAgMnSc2d1	koncertní
pianisty	pianista	k1gMnSc2	pianista
<g/>
;	;	kIx,	;
ta	ten	k3xDgFnSc1	ten
mu	on	k3xPp3gMnSc3	on
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
především	především	k9	především
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
proslulosti	proslulost	k1gFnSc2	proslulost
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
stát	stát	k5eAaImF	stát
kapelníkem	kapelník	k1gMnSc7	kapelník
<g/>
,	,	kIx,	,
dirigentem	dirigent	k1gMnSc7	dirigent
nebo	nebo	k8xC	nebo
učitelem	učitel	k1gMnSc7	učitel
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
sestavit	sestavit	k5eAaPmF	sestavit
vlastní	vlastní	k2eAgInSc4d1	vlastní
orchestr	orchestr	k1gInSc4	orchestr
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1847	[number]	k4	1847
ukončil	ukončit	k5eAaPmAgInS	ukončit
vyučování	vyučování	k1gNnSc4	vyučování
u	u	k7c2	u
Thunů	Thun	k1gInPc2	Thun
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
převzala	převzít	k5eAaPmAgFnS	převzít
Kateřina	Kateřina	k1gFnSc1	Kateřina
Kolářová	Kolářová	k1gFnSc1	Kolářová
<g/>
,	,	kIx,	,
i	i	k8xC	i
studium	studium	k1gNnSc1	studium
u	u	k7c2	u
Proksche	Proksch	k1gFnSc2	Proksch
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
koncertní	koncertní	k2eAgFnSc1d1	koncertní
cesta	cesta	k1gFnSc1	cesta
po	po	k7c6	po
západočeských	západočeský	k2eAgFnPc6d1	Západočeská
lázních	lázeň	k1gFnPc6	lázeň
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
fiaskem	fiasko	k1gNnSc7	fiasko
<g/>
:	:	kIx,	:
plzeňský	plzeňský	k2eAgInSc4d1	plzeňský
koncert	koncert	k1gInSc4	koncert
byl	být	k5eAaImAgMnS	být
slušně	slušně	k6eAd1	slušně
navštíven	navštíven	k2eAgMnSc1d1	navštíven
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
jen	jen	k9	jen
čtyři	čtyři	k4xCgMnPc1	čtyři
diváci	divák	k1gMnPc1	divák
a	a	k8xC	a
vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6	v
Mariánských	mariánský	k2eAgFnPc6d1	Mariánská
Lázních	lázeň	k1gFnPc6	lázeň
<g/>
,	,	kIx,	,
Františkových	Františkův	k2eAgFnPc6d1	Františkova
Lázních	lázeň	k1gFnPc6	lázeň
a	a	k8xC	a
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
musel	muset	k5eAaImAgInS	muset
Smetana	Smetana	k1gMnSc1	Smetana
odvolat	odvolat	k5eAaPmF	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
založit	založit	k5eAaPmF	založit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
hudební	hudební	k2eAgInSc4d1	hudební
ústav	ústav	k1gInSc4	ústav
(	(	kIx(	(
<g/>
klavírní	klavírní	k2eAgFnSc4d1	klavírní
školu	škola	k1gFnSc4	škola
<g/>
)	)	kIx)	)
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1848	[number]	k4	1848
podal	podat	k5eAaPmAgMnS	podat
zemskému	zemský	k2eAgInSc3d1	zemský
guberniu	gubernium	k1gNnSc6	gubernium
žádost	žádost	k1gFnSc4	žádost
o	o	k7c4	o
její	její	k3xOp3gNnPc4	její
povolení	povolení	k1gNnPc4	povolení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pražských	pražský	k2eAgInPc6d1	pražský
nepokojích	nepokoj	k1gInPc6	nepokoj
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1848	[number]	k4	1848
se	se	k3xPyFc4	se
Smetana	Smetana	k1gMnSc1	Smetana
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
udržoval	udržovat	k5eAaImAgMnS	udržovat
styky	styk	k1gInPc4	styk
s	s	k7c7	s
radikálními	radikální	k2eAgMnPc7d1	radikální
demokraty	demokrat	k1gMnPc7	demokrat
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgInS	nechat
strhnout	strhnout	k5eAaPmF	strhnout
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
na	na	k7c6	na
barikádovém	barikádový	k2eAgInSc6d1	barikádový
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
také	také	k9	také
dva	dva	k4xCgInPc4	dva
příležitostné	příležitostný	k2eAgInPc4d1	příležitostný
pochody	pochod	k1gInPc4	pochod
<g/>
,	,	kIx,	,
Pochod	pochod	k1gInSc1	pochod
Národní	národní	k2eAgFnSc2d1	národní
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
Pochod	pochod	k1gInSc1	pochod
studentské	studentský	k2eAgFnSc2d1	studentská
legie	legie	k1gFnSc2	legie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
jeho	jeho	k3xOp3gFnPc1	jeho
prvními	první	k4xOgMnPc7	první
skladbami	skladba	k1gFnPc7	skladba
vydanými	vydaný	k2eAgFnPc7d1	vydaná
tiskem	tisk	k1gInSc7	tisk
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
revoluční	revoluční	k2eAgFnSc1d1	revoluční
Píseň	píseň	k1gFnSc1	píseň
svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
<g/>
!	!	kIx.	!
</s>
<s>
Prapor	prapor	k1gInSc1	prapor
vlaje	vlát	k5eAaImIp3nS	vlát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
J.	J.	kA	J.
J.	J.	kA	J.
Kolára	Kolár	k1gMnSc2	Kolár
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
povstání	povstání	k1gNnSc2	povstání
nakrátko	nakrátko	k6eAd1	nakrátko
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
k	k	k7c3	k
rodičům	rodič	k1gMnPc3	rodič
do	do	k7c2	do
Obříství	Obříství	k1gNnPc2	Obříství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jej	on	k3xPp3gMnSc4	on
zastihlo	zastihnout	k5eAaPmAgNnS	zastihnout
příznivé	příznivý	k2eAgNnSc1d1	příznivé
vyřízení	vyřízení	k1gNnSc1	vyřízení
žádosti	žádost	k1gFnSc2	žádost
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
finanční	finanční	k2eAgFnSc7d1	finanční
podporou	podpora	k1gFnSc7	podpora
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
známých	známý	k1gMnPc2	známý
mohl	moct	k5eAaImAgMnS	moct
od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1848	[number]	k4	1848
otevřít	otevřít	k5eAaPmF	otevřít
hudební	hudební	k2eAgInSc4d1	hudební
ústav	ústav	k1gInSc4	ústav
(	(	kIx(	(
<g/>
klavírní	klavírní	k2eAgFnSc4d1	klavírní
školu	škola	k1gFnSc4	škola
<g/>
)	)	kIx)	)
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
příprav	příprava	k1gFnPc2	příprava
na	na	k7c6	na
otevření	otevření	k1gNnSc6	otevření
Smetana	smetana	k1gFnSc1	smetana
poprvé	poprvé	k6eAd1	poprvé
navázal	navázat	k5eAaPmAgInS	navázat
kontakt	kontakt	k1gInSc4	kontakt
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
vzorem	vzor	k1gInSc7	vzor
Franzem	Franz	k1gMnSc7	Franz
Lisztem	Liszt	k1gMnSc7	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Zaslal	zaslat	k5eAaPmAgMnS	zaslat
mu	on	k3xPp3gMnSc3	on
s	s	k7c7	s
věnováním	věnování	k1gNnSc7	věnování
své	svůj	k3xOyFgFnSc2	svůj
Six	Six	k1gFnSc2	Six
morceaux	morceaux	k1gInSc1	morceaux
caractéristiques	caractéristiques	k1gInSc1	caractéristiques
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
a	a	k8xC	a
zařízení	zařízení	k1gNnSc1	zařízení
jejich	jejich	k3xOp3gFnSc2	jejich
publikace	publikace	k1gFnSc2	publikace
a	a	k8xC	a
o	o	k7c4	o
půjčku	půjčka	k1gFnSc4	půjčka
400	[number]	k4	400
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Liszt	Liszt	k1gMnSc1	Liszt
věnování	věnování	k1gNnSc2	věnování
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
,	,	kIx,	,
půjčku	půjčka	k1gFnSc4	půjčka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
po	po	k7c6	po
osobním	osobní	k2eAgNnSc6d1	osobní
seznámení	seznámení	k1gNnSc6	seznámení
se	s	k7c7	s
Smetanou	Smetana	k1gMnSc7	Smetana
zařídil	zařídit	k5eAaPmAgMnS	zařídit
i	i	k9	i
vydání	vydání	k1gNnSc4	vydání
skladeb	skladba	k1gFnPc2	skladba
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
u	u	k7c2	u
Friedricha	Friedrich	k1gMnSc2	Friedrich
Kistnera	Kistner	k1gMnSc2	Kistner
jako	jako	k8xC	jako
Smetanův	Smetanův	k2eAgInSc1d1	Smetanův
Opus	opus	k1gInSc1	opus
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
zpočátku	zpočátku	k6eAd1	zpočátku
značně	značně	k6eAd1	značně
prosperovala	prosperovat	k5eAaImAgFnS	prosperovat
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
si	se	k3xPyFc3	se
dobré	dobrý	k2eAgFnPc4d1	dobrá
jméno	jméno	k1gNnSc1	jméno
žákovskými	žákovský	k2eAgInPc7d1	žákovský
koncerty	koncert	k1gInPc7	koncert
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
získal	získat	k5eAaPmAgMnS	získat
Smetana	Smetana	k1gMnSc1	Smetana
řadu	řada	k1gFnSc4	řada
soukromých	soukromý	k2eAgFnPc2d1	soukromá
žaček	žačka	k1gFnPc2	žačka
v	v	k7c6	v
aristokratických	aristokratický	k2eAgInPc6d1	aristokratický
kruzích	kruh	k1gInPc6	kruh
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
byl	být	k5eAaImAgInS	být
zaměstnán	zaměstnat	k5eAaPmNgInS	zaměstnat
pro	pro	k7c4	pro
klavírní	klavírní	k2eAgFnPc4d1	klavírní
předehrávky	předehrávka	k1gFnPc4	předehrávka
u	u	k7c2	u
císaře	císař	k1gMnSc2	císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Dobré	dobrý	k2eAgFnPc1d1	dobrá
majetkové	majetkový	k2eAgFnPc1d1	majetková
situace	situace	k1gFnPc1	situace
mohl	moct	k5eAaImAgInS	moct
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1849	[number]	k4	1849
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
dávnou	dávný	k2eAgFnSc7d1	dávná
láskou	láska	k1gFnSc7	láska
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Otilií	Otilie	k1gFnSc7	Otilie
Kolářovou	Kolářová	k1gFnSc7	Kolářová
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1827	[number]	k4	1827
Klatovy	Klatovy	k1gInPc4	Klatovy
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1859	[number]	k4	1859
Drážďany	Drážďany	k1gInPc4	Drážďany
<g/>
)	)	kIx)	)
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
brzy	brzy	k6eAd1	brzy
čtyři	čtyři	k4xCgFnPc4	čtyři
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
dosti	dosti	k6eAd1	dosti
úspěšně	úspěšně	k6eAd1	úspěšně
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
pianista	pianista	k1gMnSc1	pianista
a	a	k8xC	a
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
komorní	komorní	k2eAgFnSc2d1	komorní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Vyznamenal	vyznamenat	k5eAaPmAgInS	vyznamenat
se	se	k3xPyFc4	se
účinkováním	účinkování	k1gNnSc7	účinkování
při	při	k7c6	při
Beethovenových	Beethovenová	k1gFnPc6	Beethovenová
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mozartových	Mozartových	k2eAgFnSc1d1	Mozartových
(	(	kIx(	(
<g/>
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
slavnostech	slavnost	k1gFnPc6	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Škroup	Škroup	k1gMnSc1	Škroup
poprvé	poprvé	k6eAd1	poprvé
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
jeho	jeho	k3xOp3gFnSc4	jeho
Slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
předehru	předehra	k1gFnSc4	předehra
D	D	kA	D
dur	dur	k1gNnSc1	dur
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
komponování	komponování	k1gNnSc6	komponování
virtuózních	virtuózní	k2eAgInPc2d1	virtuózní
klavírních	klavírní	k2eAgInPc2d1	klavírní
kusů	kus	k1gInPc2	kus
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
i	i	k8xC	i
řazených	řazený	k2eAgMnPc2d1	řazený
do	do	k7c2	do
volných	volný	k2eAgInPc2d1	volný
cyklů	cyklus	k1gInPc2	cyklus
(	(	kIx(	(
<g/>
Svatební	svatební	k2eAgFnPc4d1	svatební
scény	scéna	k1gFnPc4	scéna
1849	[number]	k4	1849
<g/>
,	,	kIx,	,
Lístky	lístek	k1gInPc4	lístek
do	do	k7c2	do
památníku	památník	k1gInSc2	památník
<g/>
,	,	kIx,	,
Črty	črta	k1gFnSc2	črta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
posouzení	posouzení	k1gNnSc4	posouzení
Lisztovi	Liszt	k1gMnSc3	Liszt
a	a	k8xC	a
Kláře	Klára	k1gFnSc3	Klára
Schumannové	Schumannová	k1gFnSc3	Schumannová
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
specialitou	specialita	k1gFnSc7	specialita
se	se	k3xPyFc4	se
stávaly	stávat	k5eAaImAgFnP	stávat
polky	polka	k1gFnPc1	polka
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
těm	ten	k3xDgInPc3	ten
z	z	k7c2	z
plzeňského	plzeňský	k2eAgNnSc2d1	plzeňské
období	období	k1gNnSc2	období
umělecky	umělecky	k6eAd1	umělecky
stylizované	stylizovaný	k2eAgFnPc1d1	stylizovaná
do	do	k7c2	do
koncertní	koncertní	k2eAgFnSc2d1	koncertní
podoby	podoba	k1gFnSc2	podoba
(	(	kIx(	(
<g/>
Tři	tři	k4xCgFnPc1	tři
salonní	salonní	k2eAgFnPc1d1	salonní
polky	polka	k1gFnPc1	polka
<g/>
,	,	kIx,	,
Tři	tři	k4xCgFnPc1	tři
poetické	poetický	k2eAgFnPc1d1	poetická
polky	polka	k1gFnPc1	polka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
pozvednutí	pozvednutí	k1gNnSc4	pozvednutí
polky	polka	k1gFnSc2	polka
na	na	k7c4	na
umělecký	umělecký	k2eAgInSc4d1	umělecký
žánr	žánr	k1gInSc4	žánr
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
učinil	učinit	k5eAaPmAgMnS	učinit
Chopin	Chopin	k1gMnSc1	Chopin
s	s	k7c7	s
mazurkou	mazurka	k1gFnSc7	mazurka
a	a	k8xC	a
polonézou	polonéza	k1gFnSc7	polonéza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1853	[number]	k4	1853
<g/>
-	-	kIx~	-
<g/>
54	[number]	k4	54
složil	složit	k5eAaPmAgMnS	složit
také	také	k9	také
svou	svůj	k3xOyFgFnSc4	svůj
jedinou	jediný	k2eAgFnSc4d1	jediná
symfonii	symfonie	k1gFnSc4	symfonie
<g/>
,	,	kIx,	,
Triumfální	triumfální	k2eAgFnSc4d1	triumfální
symfonii	symfonie	k1gFnSc4	symfonie
(	(	kIx(	(
<g/>
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
ji	on	k3xPp3gFnSc4	on
revidoval	revidovat	k5eAaImAgMnS	revidovat
a	a	k8xC	a
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
Slavnostní	slavnostní	k2eAgInSc4d1	slavnostní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
určenou	určený	k2eAgFnSc4d1	určená
ke	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
Dedikace	dedikace	k1gFnSc1	dedikace
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
a	a	k8xC	a
symfonie	symfonie	k1gFnSc1	symfonie
se	se	k3xPyFc4	se
také	také	k9	také
vcelku	vcelku	k6eAd1	vcelku
nikdy	nikdy	k6eAd1	nikdy
neprosadila	prosadit	k5eNaPmAgFnS	prosadit
<g/>
;	;	kIx,	;
nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
samostatně	samostatně	k6eAd1	samostatně
3	[number]	k4	3
<g/>
.	.	kIx.	.
věta	věta	k1gFnSc1	věta
Scherzo	scherzo	k1gNnSc1	scherzo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
citaci	citace	k1gFnSc4	citace
rakouské	rakouský	k2eAgFnSc2d1	rakouská
císařské	císařský	k2eAgFnSc2d1	císařská
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
prvním	první	k4xOgNnSc6	první
veřejném	veřejný	k2eAgNnSc6d1	veřejné
provedení	provedení	k1gNnSc6	provedení
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1855	[number]	k4	1855
se	se	k3xPyFc4	se
Smetana	Smetana	k1gMnSc1	Smetana
poprvé	poprvé	k6eAd1	poprvé
představil	představit	k5eAaPmAgMnS	představit
jako	jako	k9	jako
dirigent	dirigent	k1gMnSc1	dirigent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
Smetanův	Smetanův	k2eAgInSc4d1	Smetanův
rodinný	rodinný	k2eAgInSc4d1	rodinný
život	život	k1gInSc4	život
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
vážné	vážný	k2eAgFnPc1d1	vážná
rány	rána	k1gFnPc1	rána
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
čtyř	čtyři	k4xCgFnPc2	čtyři
dcer	dcera	k1gFnPc2	dcera
-	-	kIx~	-
Gabriela	Gabriela	k1gFnSc1	Gabriela
<g/>
,	,	kIx,	,
Bedřiška	Bedřiška	k1gFnSc1	Bedřiška
a	a	k8xC	a
Kateřina	Kateřina	k1gFnSc1	Kateřina
-	-	kIx~	-
zemřely	zemřít	k5eAaPmAgFnP	zemřít
a	a	k8xC	a
naživu	naživu	k6eAd1	naživu
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pouze	pouze	k6eAd1	pouze
Žofie	Žofie	k1gFnSc1	Žofie
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
hudebně	hudebně	k6eAd1	hudebně
nejtalentovanější	talentovaný	k2eAgFnSc2d3	nejtalentovanější
dcery	dcera	k1gFnSc2	dcera
Bedřišky	Bedřiška	k1gFnSc2	Bedřiška
otce	otec	k1gMnSc2	otec
velmi	velmi	k6eAd1	velmi
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
a	a	k8xC	a
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
památku	památka	k1gFnSc4	památka
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
svou	svůj	k3xOyFgFnSc4	svůj
dosud	dosud	k6eAd1	dosud
nejmodernější	moderní	k2eAgFnSc4d3	nejmodernější
a	a	k8xC	a
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
skladbu	skladba	k1gFnSc4	skladba
<g/>
,	,	kIx,	,
Klavírní	klavírní	k2eAgNnSc4d1	klavírní
trio	trio	k1gNnSc4	trio
g	g	kA	g
moll	moll	k1gNnSc4	moll
(	(	kIx(	(
<g/>
1855	[number]	k4	1855
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
jeho	jeho	k3xOp3gFnSc4	jeho
manželku	manželka	k1gFnSc4	manželka
tyto	tento	k3xDgFnPc1	tento
tragédie	tragédie	k1gFnPc1	tragédie
zdrtily	zdrtit	k5eAaPmAgFnP	zdrtit
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
začala	začít	k5eAaPmAgFnS	začít
projevovat	projevovat	k5eAaImF	projevovat
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
a	a	k8xC	a
změna	změna	k1gFnSc1	změna
povahy	povaha	k1gFnSc2	povaha
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
manželským	manželský	k2eAgFnPc3d1	manželská
neshodám	neshoda	k1gFnPc3	neshoda
<g/>
.	.	kIx.	.
</s>
<s>
Rodinné	rodinný	k2eAgFnPc4d1	rodinná
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
problémy	problém	k1gInPc4	problém
i	i	k8xC	i
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
pouhým	pouhý	k2eAgNnSc7d1	pouhé
vyučováním	vyučování	k1gNnSc7	vyučování
<g/>
,	,	kIx,	,
nemožností	nemožnost	k1gFnSc7	nemožnost
získat	získat	k5eAaPmF	získat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
význačnější	význačný	k2eAgNnSc1d2	význačnější
hudební	hudební	k2eAgNnSc1d1	hudební
postavení	postavení	k1gNnSc1	postavení
<g/>
,	,	kIx,	,
snižující	snižující	k2eAgInPc1d1	snižující
se	se	k3xPyFc4	se
příjmy	příjem	k1gInPc7	příjem
z	z	k7c2	z
hudební	hudební	k2eAgFnSc2d1	hudební
školy	škola	k1gFnSc2	škola
i	i	k9	i
politicky	politicky	k6eAd1	politicky
tíživá	tíživý	k2eAgFnSc1d1	tíživá
situace	situace	k1gFnSc1	situace
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Smetana	Smetana	k1gMnSc1	Smetana
využil	využít	k5eAaPmAgMnS	využít
příležitosti	příležitost	k1gFnSc3	příležitost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mu	on	k3xPp3gMnSc3	on
zprostředkoval	zprostředkovat	k5eAaPmAgMnS	zprostředkovat
pražský	pražský	k2eAgMnSc1d1	pražský
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuos	virtuos	k1gMnSc1	virtuos
Alexander	Alexandra	k1gFnPc2	Alexandra
Dreyschock	Dreyschock	k1gMnSc1	Dreyschock
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
v	v	k7c6	v
severských	severský	k2eAgFnPc6d1	severská
zemích	zem	k1gFnPc6	zem
nabídnuto	nabídnut	k2eAgNnSc1d1	nabídnuto
ředitelství	ředitelství	k1gNnSc1	ředitelství
Filharmonické	filharmonický	k2eAgFnSc2d1	filharmonická
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
Harmoniska	Harmonisko	k1gNnPc1	Harmonisko
sällskapet	sällskapeta	k1gFnPc2	sällskapeta
<g/>
)	)	kIx)	)
v	v	k7c6	v
Göteborgu	Göteborg	k1gInSc6	Göteborg
<g/>
.	.	kIx.	.
</s>
<s>
Dreyschock	Dreyschock	k1gMnSc1	Dreyschock
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
doporučil	doporučit	k5eAaPmAgMnS	doporučit
však	však	k9	však
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
Smetanu	smetan	k1gInSc2	smetan
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1856	[number]	k4	1856
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
na	na	k7c4	na
zkoušku	zkouška	k1gFnSc4	zkouška
dva	dva	k4xCgInPc4	dva
klavírní	klavírní	k2eAgInPc4d1	klavírní
koncerty	koncert	k1gInPc4	koncert
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
za	za	k7c2	za
dirigenta	dirigent	k1gMnSc2	dirigent
Filharmonické	filharmonický	k2eAgFnSc2d1	filharmonická
společnosti	společnost	k1gFnSc2	společnost
<g/>
;	;	kIx,	;
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vyhledávaným	vyhledávaný	k2eAgMnSc7d1	vyhledávaný
učitelem	učitel	k1gMnSc7	učitel
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
pro	pro	k7c4	pro
místní	místní	k2eAgFnPc4d1	místní
dívky	dívka	k1gFnPc4	dívka
a	a	k8xC	a
dámy	dáma	k1gFnPc4	dáma
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
významnou	významný	k2eAgFnSc7d1	významná
osobností	osobnost	k1gFnSc7	osobnost
göteborské	göteborský	k2eAgFnSc2d1	göteborský
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Göteborg	Göteborg	k1gInSc4	Göteborg
bylo	být	k5eAaImAgNnS	být
otevřené	otevřený	k2eAgNnSc1d1	otevřené
a	a	k8xC	a
prosperující	prosperující	k2eAgNnSc1d1	prosperující
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
kulturního	kulturní	k2eAgNnSc2d1	kulturní
hlediska	hledisko	k1gNnSc2	hledisko
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Prahou	Praha	k1gFnSc7	Praha
provincionální	provincionální	k2eAgNnSc4d1	provincionální
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgMnS	mít
Smetana	Smetana	k1gMnSc1	Smetana
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
jen	jen	k9	jen
amatérské	amatérský	k2eAgFnPc1d1	amatérská
a	a	k8xC	a
i	i	k9	i
místní	místní	k2eAgInSc1d1	místní
hudební	hudební	k2eAgInSc1d1	hudební
vkus	vkus	k1gInSc1	vkus
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Smetanu	smetana	k1gFnSc4	smetana
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zdejší	zdejší	k2eAgNnSc1d1	zdejší
působení	působení	k1gNnSc1	působení
velmi	velmi	k6eAd1	velmi
přínosné	přínosný	k2eAgNnSc4d1	přínosné
<g/>
:	:	kIx,	:
získával	získávat	k5eAaImAgMnS	získávat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
jako	jako	k9	jako
kapelník	kapelník	k1gMnSc1	kapelník
a	a	k8xC	a
sbormistr	sbormistr	k1gMnSc1	sbormistr
po	po	k7c6	po
umělecké	umělecký	k2eAgFnSc6d1	umělecká
i	i	k8xC	i
praktické	praktický	k2eAgFnSc3d1	praktická
stránce	stránka	k1gFnSc3	stránka
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
-	-	kIx~	-
s	s	k7c7	s
nutnými	nutný	k2eAgInPc7d1	nutný
ohledy	ohled	k1gInPc7	ohled
na	na	k7c4	na
záliby	zálib	k1gInPc4	zálib
obecenstva	obecenstvo	k1gNnSc2	obecenstvo
-	-	kIx~	-
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
dramaturgii	dramaturgie	k1gFnSc6	dramaturgie
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
pobíral	pobírat	k5eAaImAgInS	pobírat
poměrně	poměrně	k6eAd1	poměrně
vysoký	vysoký	k2eAgInSc1d1	vysoký
plat	plat	k1gInSc1	plat
ve	v	k7c6	v
Filharmonické	filharmonický	k2eAgFnSc6d1	filharmonická
společnosti	společnost	k1gFnSc6	společnost
i	i	k9	i
honoráře	honorář	k1gInPc4	honorář
za	za	k7c4	za
klavírní	klavírní	k2eAgFnPc4d1	klavírní
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Uváděl	uvádět	k5eAaImAgMnS	uvádět
zde	zde	k6eAd1	zde
jak	jak	k8xS	jak
klasiky	klasika	k1gFnSc2	klasika
(	(	kIx(	(
<g/>
Händla	Händla	k1gFnSc1	Händla
<g/>
,	,	kIx,	,
Haydna	Haydna	k1gFnSc1	Haydna
<g/>
,	,	kIx,	,
Mozarta	Mozart	k1gMnSc2	Mozart
<g/>
,	,	kIx,	,
Beethovena	Beethoven	k1gMnSc2	Beethoven
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
zejména	zejména	k9	zejména
soudobé	soudobý	k2eAgMnPc4d1	soudobý
skladatele	skladatel	k1gMnPc4	skladatel
<g/>
:	:	kIx,	:
Mendelssohna	Mendelssohn	k1gMnSc4	Mendelssohn
<g/>
,	,	kIx,	,
Schumanna	Schumann	k1gMnSc4	Schumann
<g/>
,	,	kIx,	,
Liszta	Liszt	k1gMnSc4	Liszt
<g/>
,	,	kIx,	,
Chopina	Chopin	k1gMnSc4	Chopin
<g/>
,	,	kIx,	,
Berlioze	Berlioz	k1gMnSc4	Berlioz
<g/>
,	,	kIx,	,
Wagnera	Wagner	k1gMnSc4	Wagner
<g/>
,	,	kIx,	,
Gadeho	Gade	k1gMnSc4	Gade
<g/>
,	,	kIx,	,
Verdiho	Verdi	k1gMnSc4	Verdi
<g/>
,	,	kIx,	,
Rubinsteina	Rubinsteina	k1gFnSc1	Rubinsteina
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
vlastní	vlastní	k2eAgFnPc4d1	vlastní
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
moderním	moderní	k2eAgInSc7d1	moderní
hudebním	hudební	k2eAgInSc7d1	hudební
vývojem	vývoj	k1gInSc7	vývoj
udržoval	udržovat	k5eAaImAgMnS	udržovat
Smetana	Smetana	k1gMnSc1	Smetana
styk	styk	k1gInSc4	styk
především	především	k9	především
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Liszta	Liszt	k1gInSc2	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
v	v	k7c6	v
září	září	k1gNnSc6	září
1857	[number]	k4	1857
slyšel	slyšet	k5eAaImAgInS	slyšet
poprvé	poprvé	k6eAd1	poprvé
Lisztovu	Lisztův	k2eAgFnSc4d1	Lisztova
Faustovskou	faustovský	k2eAgFnSc4d1	faustovská
symfonii	symfonie	k1gFnSc4	symfonie
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgInSc7d1	nový
žánrem	žánr	k1gInSc7	žánr
programní	programní	k2eAgFnSc2d1	programní
symfonie	symfonie	k1gFnSc2	symfonie
neboli	neboli	k8xC	neboli
symfonické	symfonický	k2eAgFnSc2d1	symfonická
básně	báseň	k1gFnSc2	báseň
fascinován	fascinovat	k5eAaBmNgMnS	fascinovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
složil	složit	k5eAaPmAgInS	složit
postupně	postupně	k6eAd1	postupně
tři	tři	k4xCgFnPc1	tři
symfonické	symfonický	k2eAgFnPc1d1	symfonická
básně	báseň	k1gFnPc1	báseň
<g/>
:	:	kIx,	:
Richard	Richard	k1gMnSc1	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
na	na	k7c4	na
námět	námět	k1gInSc4	námět
tragédie	tragédie	k1gFnSc2	tragédie
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
Valdštýnův	Valdštýnův	k2eAgInSc1d1	Valdštýnův
tábor	tábor	k1gInSc1	tábor
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
prvním	první	k4xOgInSc7	první
dílem	díl	k1gInSc7	díl
Schillerovy	Schillerův	k2eAgFnSc2d1	Schillerova
valdštejnské	valdštejnský	k2eAgFnSc2d1	Valdštejnská
trilogie	trilogie	k1gFnSc2	trilogie
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
Hakon	Hakon	k1gMnSc1	Hakon
Jarl	jarl	k1gMnSc1	jarl
na	na	k7c6	na
základě	základ	k1gInSc6	základ
eposu	epos	k1gInSc2	epos
dánského	dánský	k2eAgMnSc4d1	dánský
básníka	básník	k1gMnSc4	básník
Adama	Adam	k1gMnSc4	Adam
Oehlenschlägera	Oehlenschläger	k1gMnSc4	Oehlenschläger
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
byla	být	k5eAaImAgFnS	být
možná	možná	k9	možná
původně	původně	k6eAd1	původně
zamýšlena	zamýšlen	k2eAgFnSc1d1	zamýšlena
i	i	k8xC	i
shakespearovská	shakespearovský	k2eAgFnSc1d1	shakespearovská
klavírní	klavírní	k2eAgFnSc1d1	klavírní
skizza	skizza	k1gFnSc1	skizza
Macbeth	Macbetha	k1gFnPc2	Macbetha
a	a	k8xC	a
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1859	[number]	k4	1859
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
účastnil	účastnit	k5eAaImAgMnS	účastnit
významného	významný	k2eAgInSc2d1	významný
sjezdu	sjezd	k1gInSc2	sjezd
umělců	umělec	k1gMnPc2	umělec
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
Allgemeines	Allgemeines	k1gInSc1	Allgemeines
Deutsches	Deutschesa	k1gFnPc2	Deutschesa
Musikverein	Musikverein	k1gInSc1	Musikverein
a	a	k8xC	a
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
tzv.	tzv.	kA	tzv.
novoněmecká	novoněmecký	k2eAgFnSc1d1	novoněmecký
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
neudeutsche	neudeutsche	k1gFnSc1	neudeutsche
Schule	Schule	k1gFnSc2	Schule
<g/>
)	)	kIx)	)
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
opět	opět	k6eAd1	opět
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Liszta	Liszta	k1gMnSc1	Liszta
a	a	k8xC	a
slyšel	slyšet	k5eAaImAgMnS	slyšet
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
poprvé	poprvé	k6eAd1	poprvé
preludium	preludium	k1gNnSc4	preludium
k	k	k7c3	k
Wagnerovu	Wagnerův	k2eAgMnSc3d1	Wagnerův
Tristanovi	Tristan	k1gMnSc3	Tristan
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
strávil	strávit	k5eAaPmAgMnS	strávit
Smetana	Smetana	k1gMnSc1	Smetana
bez	bez	k7c2	bez
rodiny	rodina	k1gFnSc2	rodina
<g/>
;	;	kIx,	;
tehdy	tehdy	k6eAd1	tehdy
navázal	navázat	k5eAaPmAgMnS	navázat
dlouholeté	dlouholetý	k2eAgNnSc4d1	dlouholeté
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
Fröjdou	Fröjda	k1gFnSc7	Fröjda
Beneckovou	Benecková	k1gFnSc7	Benecková
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Gumpertová	Gumpertová	k1gFnSc1	Gumpertová
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Rubensová	Rubensový	k2eAgFnSc1d1	Rubensový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
obvykle	obvykle	k6eAd1	obvykle
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Čechy	Čech	k1gMnPc4	Čech
a	a	k8xC	a
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
sezónu	sezóna	k1gFnSc4	sezóna
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
sever	sever	k1gInSc4	sever
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Smetanová	Smetanová	k1gFnSc1	Smetanová
však	však	k9	však
trpěla	trpět	k5eAaImAgFnS	trpět
depresemi	deprese	k1gFnPc7	deprese
i	i	k8xC	i
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
při	při	k7c6	při
předčasném	předčasný	k2eAgInSc6d1	předčasný
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
zemřela	zemřít	k5eAaPmAgFnS	zemřít
cestou	cesta	k1gFnSc7	cesta
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
trávil	trávit	k5eAaImAgMnS	trávit
Smetana	Smetana	k1gMnSc1	Smetana
v	v	k7c6	v
Chloumku	chloumek	k1gInSc6	chloumek
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Karla	Karel	k1gMnSc2	Karel
a	a	k8xC	a
blíže	blízce	k6eAd2	blízce
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
švagrovou	švagrův	k2eAgFnSc7d1	švagrova
Barborou	Barbora	k1gFnSc7	Barbora
(	(	kIx(	(
<g/>
Betty	Betty	k1gFnSc7	Betty
<g/>
)	)	kIx)	)
Ferdinandiovou	Ferdinandiový	k2eAgFnSc7d1	Ferdinandiový
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1840	[number]	k4	1840
Černé	Černé	k2eAgFnSc2d1	Černé
Budy	Buda	k1gFnSc2	Buda
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1908	[number]	k4	1908
Luhačovice	Luhačovice	k1gFnPc1	Luhačovice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Požádal	požádat	k5eAaPmAgMnS	požádat
ji	on	k3xPp3gFnSc4	on
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mezidobí	mezidobí	k1gNnSc6	mezidobí
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
dárek	dárek	k1gInSc4	dárek
pro	pro	k7c4	pro
snoubenku	snoubenka	k1gFnSc4	snoubenka
Bettina	Bettin	k2eAgFnSc1d1	Bettina
polka	polka	k1gFnSc1	polka
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
omylem	omylem	k6eAd1	omylem
<g/>
:	:	kIx,	:
Betty	Betty	k1gFnSc1	Betty
necítila	cítit	k5eNaImAgFnS	cítit
k	k	k7c3	k
více	hodně	k6eAd2	hodně
než	než	k8xS	než
o	o	k7c4	o
šestnáct	šestnáct	k4xCc4	šestnáct
let	let	k1gInSc4	let
staršímu	starý	k2eAgMnSc3d2	starší
skladateli	skladatel	k1gMnSc3	skladatel
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
nepřitahoval	přitahovat	k5eNaImAgMnS	přitahovat
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
neměla	mít	k5eNaImAgFnS	mít
porozumění	porozumění	k1gNnSc4	porozumění
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
talent	talent	k1gInSc4	talent
ani	ani	k8xC	ani
temperament	temperament	k1gInSc4	temperament
<g/>
;	;	kIx,	;
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
žili	žít	k5eAaImAgMnP	žít
již	již	k6eAd1	již
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nemoci	nemoc	k1gFnSc6	nemoc
Smetana	smetana	k1gFnSc1	smetana
odcizením	odcizení	k1gNnSc7	odcizení
a	a	k8xC	a
nezájmem	nezájem	k1gInSc7	nezájem
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
velmi	velmi	k6eAd1	velmi
trpěl	trpět	k5eAaImAgMnS	trpět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
dvě	dva	k4xCgFnPc1	dva
dcery	dcera	k1gFnPc1	dcera
<g/>
,	,	kIx,	,
Zdeňka	Zdeňka	k1gFnSc1	Zdeňka
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
a	a	k8xC	a
Božena	Božena	k1gFnSc1	Božena
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
si	se	k3xPyFc3	se
podržel	podržet	k5eAaPmAgMnS	podržet
nejužší	úzký	k2eAgInSc4d3	nejužší
vztah	vztah	k1gInSc4	vztah
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
Žofii	Žofie	k1gFnSc4	Žofie
<g/>
,	,	kIx,	,
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1874	[number]	k4	1874
provdané	provdaný	k2eAgNnSc4d1	provdané
za	za	k7c4	za
lesníka	lesník	k1gMnSc4	lesník
Josefa	Josef	k1gMnSc4	Josef
Schwarze	Schwarz	k1gMnSc4	Schwarz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
začínal	začínat	k5eAaImAgMnS	začínat
být	být	k5eAaImF	být
nespokojen	spokojen	k2eNgMnSc1d1	nespokojen
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
švédským	švédský	k2eAgNnSc7d1	švédské
působištěm	působiště	k1gNnSc7	působiště
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
tu	ten	k3xDgFnSc4	ten
jednak	jednak	k8xC	jednak
stesk	stesk	k1gInSc4	stesk
po	po	k7c6	po
domově	domov	k1gInSc6	domov
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
nejlépe	dobře	k6eAd3	dobře
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
čtyř	čtyři	k4xCgFnPc2	čtyři
polek	polka	k1gFnPc2	polka
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Souvenirs	Souvenirsa	k1gFnPc2	Souvenirsa
de	de	k?	de
Bohê	Bohê	k1gMnSc1	Bohê
en	en	k?	en
forme	format	k5eAaPmIp3nS	format
de	de	k?	de
polkas	polkas	k1gInSc1	polkas
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
/	/	kIx~	/
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
deníkové	deníkový	k2eAgInPc1d1	deníkový
zápisy	zápis	k1gInPc1	zápis
i	i	k8xC	i
korespondence	korespondence	k1gFnSc1	korespondence
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c4	o
zintenzivnění	zintenzivnění	k1gNnSc4	zintenzivnění
vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
;	;	kIx,	;
z	z	k7c2	z
popudu	popud	k1gInSc2	popud
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jana	Jan	k1gMnSc2	Jan
Ludevíta	Ludevít	k1gMnSc2	Ludevít
Procházky	Procházka	k1gMnSc2	Procházka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
složil	složit	k5eAaPmAgMnS	složit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
sbor	sbor	k1gInSc4	sbor
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
text	text	k1gInSc4	text
Píseň	píseň	k1gFnSc1	píseň
česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
však	však	k9	však
toužil	toužit	k5eAaImAgInS	toužit
naplnit	naplnit	k5eAaPmF	naplnit
své	svůj	k3xOyFgFnPc4	svůj
širší	široký	k2eAgFnPc4d2	širší
umělecké	umělecký	k2eAgFnPc4d1	umělecká
ambice	ambice	k1gFnPc4	ambice
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nemohu	moct	k5eNaImIp1nS	moct
se	se	k3xPyFc4	se
zahrabat	zahrabat	k5eAaPmF	zahrabat
v	v	k7c6	v
Göteborgu	Göteborg	k1gInSc6	Göteborg
<g/>
;	;	kIx,	;
musím	muset	k5eAaImIp1nS	muset
se	se	k3xPyFc4	se
vynasnažit	vynasnažit	k5eAaPmF	vynasnažit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mé	můj	k3xOp1gFnSc2	můj
skladby	skladba	k1gFnSc2	skladba
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mně	já	k3xPp1nSc3	já
byla	být	k5eAaImAgFnS	být
dána	dán	k2eAgFnSc1d1	dána
příležitost	příležitost	k1gFnSc1	příležitost
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
činnosti	činnost	k1gFnSc3	činnost
a	a	k8xC	a
popud	popud	k1gInSc4	popud
k	k	k7c3	k
rozsáhlejší	rozsáhlý	k2eAgFnSc3d2	rozsáhlejší
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jen	jen	k9	jen
ven	ven	k6eAd1	ven
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
co	co	k3yQnSc4	co
nejdříve	dříve	k6eAd3	dříve
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zápis	zápis	k1gInSc1	zápis
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
se	se	k3xPyFc4	se
rozloučil	rozloučit	k5eAaPmAgMnS	rozloučit
sérií	série	k1gFnSc7	série
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
až	až	k8xS	až
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
(	(	kIx(	(
<g/>
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
,	,	kIx,	,
Norrköping	Norrköping	k1gInSc1	Norrköping
<g/>
,	,	kIx,	,
Göteborg	Göteborg	k1gInSc1	Göteborg
<g/>
)	)	kIx)	)
a	a	k8xC	a
hudebně	hudebně	k6eAd1	hudebně
též	též	k9	též
kompozicí	kompozice	k1gFnSc7	kompozice
koncertní	koncertní	k2eAgFnSc2d1	koncertní
etudy	etuda	k1gFnSc2	etuda
Vid	vid	k1gInSc1	vid
strandem	strand	k1gInSc7	strand
(	(	kIx(	(
<g/>
Am	Am	k1gFnSc1	Am
Seegestrande	Seegestrand	k1gInSc5	Seegestrand
<g/>
,	,	kIx,	,
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
mořském	mořský	k2eAgInSc6d1	mořský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
nebyl	být	k5eNaImAgInS	být
zprvu	zprvu	k6eAd1	zprvu
definitivní	definitivní	k2eAgNnSc4d1	definitivní
<g/>
,	,	kIx,	,
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
pokusil	pokusit	k5eAaPmAgMnS	pokusit
prosadit	prosadit	k5eAaPmF	prosadit
jako	jako	k9	jako
klavírní	klavírní	k2eAgMnSc1d1	klavírní
virtuos	virtuos	k1gMnSc1	virtuos
vystoupeními	vystoupení	k1gNnPc7	vystoupení
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
finančním	finanční	k2eAgInSc7d1	finanční
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
musel	muset	k5eAaImAgInS	muset
zhojit	zhojit	k5eAaPmF	zhojit
novým	nový	k2eAgInSc7d1	nový
kratším	krátký	k2eAgInSc7d2	kratší
pobytem	pobyt	k1gInSc7	pobyt
v	v	k7c6	v
Göteborgu	Göteborg	k1gInSc6	Göteborg
(	(	kIx(	(
<g/>
březen	březen	k1gInSc4	březen
až	až	k9	až
květen	květen	k1gInSc4	květen
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Veškerou	veškerý	k3xTgFnSc4	veškerý
svou	svůj	k3xOyFgFnSc4	svůj
další	další	k2eAgFnSc4d1	další
činnost	činnost	k1gFnSc4	činnost
již	již	k6eAd1	již
soustředil	soustředit	k5eAaPmAgMnS	soustředit
na	na	k7c4	na
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Smetana	Smetana	k1gMnSc1	Smetana
opustil	opustit	k5eAaPmAgMnS	opustit
zajištěné	zajištěný	k2eAgNnSc4d1	zajištěné
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
znatelné	znatelný	k2eAgNnSc1d1	znatelné
politické	politický	k2eAgNnSc1d1	politické
a	a	k8xC	a
společenské	společenský	k2eAgNnSc1d1	společenské
uvolnění	uvolnění	k1gNnSc1	uvolnění
po	po	k7c6	po
publikaci	publikace	k1gFnSc6	publikace
Říjnového	říjnový	k2eAgInSc2d1	říjnový
diplomu	diplom	k1gInSc2	diplom
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
činně	činně	k6eAd1	činně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
společenského	společenský	k2eAgInSc2d1	společenský
i	i	k8xC	i
uměleckého	umělecký	k2eAgInSc2d1	umělecký
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgMnSc7	svůj
žákem	žák	k1gMnSc7	žák
a	a	k8xC	a
přítelem	přítel	k1gMnSc7	přítel
J.	J.	kA	J.
L.	L.	kA	L.
Procházkou	Procházka	k1gMnSc7	Procházka
byl	být	k5eAaImAgMnS	být
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
Měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
besedy	beseda	k1gFnSc2	beseda
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
také	také	k9	také
významné	významný	k2eAgFnPc4d1	významná
úterní	úterní	k2eAgFnPc4d1	úterní
schůzky	schůzka	k1gFnPc4	schůzka
české	český	k2eAgFnSc2d1	Česká
inteligence	inteligence	k1gFnSc2	inteligence
u	u	k7c2	u
knížete	kníže	k1gMnSc2	kníže
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Thurn-Taxise	Thurn-Taxise	k1gFnSc2	Thurn-Taxise
<g/>
.	.	kIx.	.
</s>
<s>
Spoluzakládal	spoluzakládat	k5eAaImAgInS	spoluzakládat
Uměleckou	umělecký	k2eAgFnSc4d1	umělecká
besedu	beseda	k1gFnSc4	beseda
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1863	[number]	k4	1863
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
prvním	první	k4xOgInSc7	první
předsedou	předseda	k1gMnSc7	předseda
jejího	její	k3xOp3gInSc2	její
hudebního	hudební	k2eAgInSc2d1	hudební
odboru	odbor	k1gInSc2	odbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
usilovně	usilovně	k6eAd1	usilovně
zdokonaloval	zdokonalovat	k5eAaImAgInS	zdokonalovat
svou	svůj	k3xOyFgFnSc4	svůj
psanou	psaný	k2eAgFnSc4d1	psaná
i	i	k8xC	i
mluvenou	mluvený	k2eAgFnSc4d1	mluvená
češtinu	čeština	k1gFnSc4	čeština
(	(	kIx(	(
<g/>
pocházel	pocházet	k5eAaImAgInS	pocházet
sice	sice	k8xC	sice
z	z	k7c2	z
česky	česky	k6eAd1	česky
mluvící	mluvící	k2eAgFnSc2d1	mluvící
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosud	dosud	k6eAd1	dosud
veškeré	veškerý	k3xTgNnSc4	veškerý
jeho	jeho	k3xOp3gNnSc4	jeho
vzdělání	vzdělání	k1gNnSc4	vzdělání
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
veškerý	veškerý	k3xTgInSc1	veškerý
společenský	společenský	k2eAgInSc1d1	společenský
kontakt	kontakt	k1gInSc1	kontakt
probíhaly	probíhat	k5eAaImAgInP	probíhat
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Politicky	politicky	k6eAd1	politicky
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
přiklonil	přiklonit	k5eAaPmAgInS	přiklonit
k	k	k7c3	k
mladočeské	mladočeský	k2eAgFnSc3d1	mladočeská
frakci	frakce	k1gFnSc3	frakce
Národní	národní	k2eAgFnSc2d1	národní
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
soupeření	soupeření	k1gNnSc1	soupeření
mladočechů	mladočech	k1gMnPc2	mladočech
se	s	k7c7	s
staročechy	staročech	k1gMnPc7	staročech
značně	značně	k6eAd1	značně
ovlivňovalo	ovlivňovat	k5eAaImAgNnS	ovlivňovat
celou	celý	k2eAgFnSc4d1	celá
jeho	jeho	k3xOp3gFnSc4	jeho
další	další	k2eAgFnSc4d1	další
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
zájmem	zájem	k1gInSc7	zájem
sledoval	sledovat	k5eAaImAgInS	sledovat
přípravu	příprava	k1gFnSc4	příprava
otevření	otevření	k1gNnSc1	otevření
prvního	první	k4xOgNnSc2	první
českého	český	k2eAgNnSc2d1	české
stálého	stálý	k2eAgNnSc2d1	stálé
profesionálního	profesionální	k2eAgNnSc2d1	profesionální
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
hodlal	hodlat	k5eAaImAgMnS	hodlat
ucházet	ucházet	k5eAaImF	ucházet
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
prvního	první	k4xOgMnSc2	první
kapelníka	kapelník	k1gMnSc2	kapelník
a	a	k8xC	a
připravoval	připravovat	k5eAaImAgMnS	připravovat
si	se	k3xPyFc3	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k9	jako
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
neznámý	známý	k2eNgMnSc1d1	neznámý
<g/>
,	,	kIx,	,
představil	představit	k5eAaPmAgInS	představit
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
koncerty	koncert	k1gInPc7	koncert
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
režii	režie	k1gFnSc6	režie
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
navštíveným	navštívený	k2eAgMnPc3d1	navštívený
a	a	k8xC	a
kriticky	kriticky	k6eAd1	kriticky
nejednoznačně	jednoznačně	k6eNd1	jednoznačně
přijatým	přijatý	k2eAgInSc7d1	přijatý
orchestrálním	orchestrální	k2eAgInSc7d1	orchestrální
koncertem	koncert	k1gInSc7	koncert
na	na	k7c6	na
Žofíně	Žofín	k1gInSc6	Žofín
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
premiéroval	premiérovat	k5eAaBmAgMnS	premiérovat
Richarda	Richard	k1gMnSc4	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Valdštejnův	Valdštejnův	k2eAgInSc1d1	Valdštejnův
tábor	tábor	k1gInSc1	tábor
<g/>
,	,	kIx,	,
a	a	k8xC	a
úspěšnějším	úspěšný	k2eAgInSc7d2	úspěšnější
klavírním	klavírní	k2eAgInSc7d1	klavírní
recitálem	recitál	k1gInSc7	recitál
v	v	k7c6	v
Konviktu	konvikt	k1gInSc6	konvikt
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obsazování	obsazování	k1gNnSc6	obsazování
kapelnického	kapelnický	k2eAgNnSc2d1	kapelnické
místa	místo	k1gNnSc2	místo
nového	nový	k2eAgNnSc2d1	nové
divadla	divadlo	k1gNnSc2	divadlo
však	však	k9	však
byla	být	k5eAaImAgFnS	být
dána	dán	k2eAgFnSc1d1	dána
přednost	přednost	k1gFnSc1	přednost
osvědčenému	osvědčený	k2eAgMnSc3d1	osvědčený
a	a	k8xC	a
zkušenému	zkušený	k2eAgMnSc3d1	zkušený
profesionálovi	profesionál	k1gMnSc3	profesionál
<g/>
,	,	kIx,	,
dosavadnímu	dosavadní	k2eAgMnSc3d1	dosavadní
druhému	druhý	k4xOgInSc3	druhý
kapelníkovi	kapelník	k1gMnSc3	kapelník
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
Janu	Jan	k1gMnSc3	Jan
Nepomukovi	Nepomuk	k1gMnSc3	Nepomuk
Maýrovi	Maýr	k1gMnSc3	Maýr
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
jeho	jeho	k3xOp3gNnSc4	jeho
úsilí	úsilí	k1gNnSc4	úsilí
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
místa	místo	k1gNnSc2	místo
ředitele	ředitel	k1gMnSc2	ředitel
pražské	pražský	k2eAgFnSc2d1	Pražská
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
po	po	k7c6	po
J.	J.	kA	J.
B.	B.	kA	B.
Kittlovi	Kittl	k1gMnSc6	Kittl
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
alkoholismus	alkoholismus	k1gInSc4	alkoholismus
nucen	nucen	k2eAgInSc4d1	nucen
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1865	[number]	k4	1865
své	svůj	k3xOyFgNnSc1	svůj
místo	místo	k1gNnSc1	místo
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
nepřineslo	přinést	k5eNaPmAgNnS	přinést
výsledky	výsledek	k1gInPc1	výsledek
(	(	kIx(	(
<g/>
jmenován	jmenován	k2eAgMnSc1d1	jmenován
byl	být	k5eAaImAgMnS	být
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
ředitel	ředitel	k1gMnSc1	ředitel
varhanické	varhanický	k2eAgFnSc2d1	varhanická
školy	škola	k1gFnSc2	škola
Josef	Josef	k1gMnSc1	Josef
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
neuspěl	uspět	k5eNaPmAgInS	uspět
ani	ani	k8xC	ani
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
rakouské	rakouský	k2eAgNnSc4d1	rakouské
umělecké	umělecký	k2eAgNnSc4d1	umělecké
stipendium	stipendium	k1gNnSc4	stipendium
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obživu	obživa	k1gFnSc4	obživa
tedy	tedy	k9	tedy
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1863	[number]	k4	1863
založil	založit	k5eAaPmAgInS	založit
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
sbormistrem	sbormistr	k1gMnSc7	sbormistr
a	a	k8xC	a
skladatelem	skladatel	k1gMnSc7	skladatel
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
Hellerem	Heller	k1gMnSc7	Heller
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgFnSc4d1	hudební
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Lažanských	Lažanský	k2eAgFnPc2d1	Lažanská
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Smetana	Smetana	k1gMnSc1	Smetana
soustředil	soustředit	k5eAaPmAgMnS	soustředit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
sbormistra	sbormistr	k1gMnSc2	sbormistr
Hlaholu	hlahol	k1gInSc2	hlahol
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1863	[number]	k4	1863
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
Umělecké	umělecký	k2eAgFnSc6d1	umělecká
besedě	beseda	k1gFnSc6	beseda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sestavoval	sestavovat	k5eAaImAgMnS	sestavovat
a	a	k8xC	a
řídil	řídit	k5eAaImAgMnS	řídit
její	její	k3xOp3gInPc4	její
hudební	hudební	k2eAgInPc4d1	hudební
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
velkou	velký	k2eAgFnSc4d1	velká
shakespearovskou	shakespearovský	k2eAgFnSc4d1	shakespearovská
slavnost	slavnost	k1gFnSc4	slavnost
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1864	[number]	k4	1864
a	a	k8xC	a
sérii	série	k1gFnSc4	série
abonentních	abonentní	k2eAgInPc2d1	abonentní
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1864	[number]	k4	1864
<g/>
/	/	kIx~	/
<g/>
65	[number]	k4	65
s	s	k7c7	s
ambiciózním	ambiciózní	k2eAgInSc7d1	ambiciózní
programem	program	k1gInSc7	program
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
končící	končící	k2eAgInSc1d1	končící
značným	značný	k2eAgInSc7d1	značný
schodkem	schodek	k1gInSc7	schodek
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
organizační	organizační	k2eAgFnSc2d1	organizační
a	a	k8xC	a
dirigentské	dirigentský	k2eAgFnSc2d1	dirigentská
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
byla	být	k5eAaImAgFnS	být
pražská	pražský	k2eAgFnSc1d1	Pražská
premiéra	premiéra	k1gFnSc1	premiéra
Lisztova	Lisztův	k2eAgNnSc2d1	Lisztovo
oratoria	oratorium	k1gNnSc2	oratorium
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
svaté	svatý	k2eAgFnSc6d1	svatá
Alžbětě	Alžběta	k1gFnSc6	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgInSc2	ten
psal	psát	k5eAaImAgMnS	psát
především	především	k9	především
do	do	k7c2	do
Národních	národní	k2eAgInPc2d1	národní
listů	list	k1gInPc2	list
kritiky	kritika	k1gFnSc2	kritika
hudebního	hudební	k2eAgInSc2d1	hudební
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
opery	opera	k1gFnSc2	opera
v	v	k7c6	v
Prozatímním	prozatímní	k2eAgNnSc6d1	prozatímní
divadle	divadlo	k1gNnSc6	divadlo
i	i	k9	i
koncertů	koncert	k1gInPc2	koncert
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
všeobecné	všeobecný	k2eAgFnPc4d1	všeobecná
programové	programový	k2eAgFnPc4d1	programová
úvahy	úvaha	k1gFnPc4	úvaha
jak	jak	k8xS	jak
o	o	k7c6	o
koncertní	koncertní	k2eAgFnSc6d1	koncertní
činnosti	činnost	k1gFnSc6	činnost
(	(	kIx(	(
<g/>
O	o	k7c6	o
našich	náš	k3xOp1gInPc6	náš
koncertech	koncert	k1gInPc6	koncert
<g/>
,	,	kIx,	,
Slavoj	Slavoj	k1gInSc1	Slavoj
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1862	[number]	k4	1862
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
o	o	k7c6	o
opeře	opera	k1gFnSc6	opera
(	(	kIx(	(
<g/>
Národní	národní	k2eAgFnSc2d1	národní
listy	lista	k1gFnSc2	lista
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opeře	opera	k1gFnSc6	opera
v	v	k7c6	v
Prozatímním	prozatímní	k2eAgNnSc6d1	prozatímní
divadle	divadlo	k1gNnSc6	divadlo
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
zejména	zejména	k9	zejména
repertoár	repertoár	k1gInSc4	repertoár
neodpovídající	odpovídající	k2eNgFnSc2d1	neodpovídající
společenské	společenský	k2eAgFnSc2d1	společenská
a	a	k8xC	a
výchovné	výchovný	k2eAgFnSc2d1	výchovná
funkci	funkce	k1gFnSc4	funkce
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
různé	různý	k2eAgInPc1d1	různý
<g/>
,	,	kIx,	,
z	z	k7c2	z
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
hlediska	hledisko	k1gNnSc2	hledisko
škodlivé	škodlivý	k2eAgFnSc2d1	škodlivá
provozní	provozní	k2eAgFnSc2d1	provozní
zvyklosti	zvyklost	k1gFnSc2	zvyklost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
častá	častý	k2eAgNnPc4d1	časté
benefiční	benefiční	k2eAgNnPc4d1	benefiční
představení	představení	k1gNnPc4	představení
a	a	k8xC	a
hostování	hostování	k1gNnPc4	hostování
<g/>
,	,	kIx,	,
málo	málo	k1gNnSc4	málo
zkoušek	zkouška	k1gFnPc2	zkouška
<g/>
,	,	kIx,	,
neopodstatněné	opodstatněný	k2eNgInPc4d1	neopodstatněný
škrty	škrt	k1gInPc7	škrt
v	v	k7c6	v
operách	opera	k1gFnPc6	opera
<g/>
,	,	kIx,	,
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
kombinování	kombinování	k1gNnSc1	kombinování
děl	dělo	k1gNnPc2	dělo
nebo	nebo	k8xC	nebo
zábavná	zábavný	k2eAgNnPc4d1	zábavné
meziaktní	meziaktní	k2eAgNnPc4d1	meziaktní
intermezza	intermezzo	k1gNnPc4	intermezzo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kompozic	kompozice	k1gFnPc2	kompozice
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
některé	některý	k3yIgInPc1	některý
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
činností	činnost	k1gFnSc7	činnost
v	v	k7c6	v
Hlaholu	hlahol	k1gInSc6	hlahol
(	(	kIx(	(
<g/>
sbory	sbor	k1gInPc1	sbor
Tři	tři	k4xCgMnPc4	tři
jezdci	jezdec	k1gMnPc1	jezdec
1862	[number]	k4	1862
a	a	k8xC	a
Odrodilec	odrodilec	k1gMnSc1	odrodilec
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
a	a	k8xC	a
Umělecké	umělecký	k2eAgFnSc3d1	umělecká
besedě	beseda	k1gFnSc3	beseda
(	(	kIx(	(
<g/>
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
pochod	pochod	k1gInSc1	pochod
k	k	k7c3	k
shakespearovské	shakespearovský	k2eAgFnSc3d1	shakespearovská
slavnosti	slavnost	k1gFnSc3	slavnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
úkol	úkol	k1gInSc1	úkol
však	však	k9	však
od	od	k7c2	od
návratu	návrat	k1gInSc2	návrat
ze	z	k7c2	z
Švédska	Švédsko	k1gNnSc2	Švédsko
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
opeře	opera	k1gFnSc6	opera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1862	[number]	k4	1862
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
J.	J.	kA	J.
L.	L.	kA	L.
Procházky	Procházka	k1gMnSc2	Procházka
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Sabiny	Sabina	k1gMnSc2	Sabina
libreto	libreto	k1gNnSc1	libreto
k	k	k7c3	k
historické	historický	k2eAgFnSc3d1	historická
opeře	opera	k1gFnSc3	opera
Braniboři	Branibor	k1gMnPc1	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Práci	práce	k1gFnSc4	práce
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
dokončil	dokončit	k5eAaPmAgInS	dokončit
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1863	[number]	k4	1863
a	a	k8xC	a
stihl	stihnout	k5eAaPmAgInS	stihnout
tedy	tedy	k9	tedy
(	(	kIx(	(
<g/>
odloženou	odložený	k2eAgFnSc4d1	odložená
<g/>
)	)	kIx)	)
uzávěrku	uzávěrka	k1gFnSc4	uzávěrka
operní	operní	k2eAgFnSc2d1	operní
soutěže	soutěž	k1gFnSc2	soutěž
vyhlášené	vyhlášený	k2eAgFnSc2d1	vyhlášená
hrabětem	hrabě	k1gMnSc7	hrabě
Janem	Jan	k1gMnSc7	Jan
Harrachem	Harrach	k1gMnSc7	Harrach
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
ji	on	k3xPp3gFnSc4	on
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Prozatímnímu	prozatímní	k2eAgNnSc3d1	prozatímní
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
kapelník	kapelník	k1gMnSc1	kapelník
Maýr	Maýr	k1gMnSc1	Maýr
(	(	kIx(	(
<g/>
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
Harrachovy	Harrachov	k1gInPc4	Harrachov
soutěže	soutěž	k1gFnSc2	soutěž
účastnil	účastnit	k5eAaImAgInS	účastnit
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
operou	opera	k1gFnSc7	opera
<g/>
)	)	kIx)	)
však	však	k9	však
Branibory	Branibor	k1gMnPc4	Branibor
dlouho	dlouho	k6eAd1	dlouho
odmítal	odmítat	k5eAaImAgMnS	odmítat
uvést	uvést	k5eAaPmF	uvést
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byli	být	k5eAaImAgMnP	být
přijati	přijmout	k5eAaPmNgMnP	přijmout
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
vůli	vůle	k1gFnSc3	vůle
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1865	[number]	k4	1865
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
ředitele	ředitel	k1gMnSc2	ředitel
Franze	Franze	k1gFnSc2	Franze
Thomého	Thomý	k2eAgInSc2d1	Thomý
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
nastudoval	nastudovat	k5eAaBmAgMnS	nastudovat
a	a	k8xC	a
řídil	řídit	k5eAaImAgMnS	řídit
sám	sám	k3xTgMnSc1	sám
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgNnSc4d1	úspěšné
přijetí	přijetí	k1gNnSc4	přijetí
u	u	k7c2	u
obecenstva	obecenstvo	k1gNnSc2	obecenstvo
i	i	k9	i
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
upevnilo	upevnit	k5eAaPmAgNnS	upevnit
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
postavení	postavení	k1gNnSc1	postavení
jako	jako	k8xC	jako
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
českých	český	k2eAgMnPc2d1	český
skladatelů	skladatel	k1gMnPc2	skladatel
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Harrachova	Harrachov	k1gInSc2	Harrachov
komise	komise	k1gFnSc2	komise
s	s	k7c7	s
výhradami	výhrada	k1gFnPc7	výhrada
přiznala	přiznat	k5eAaPmAgFnS	přiznat
Braniborům	Branibor	k1gMnPc3	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1866	[number]	k4	1866
první	první	k4xOgFnSc4	první
cenu	cena	k1gFnSc4	cena
600	[number]	k4	600
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c4	po
dokončení	dokončení	k1gNnSc4	dokončení
Braniborů	Branibor	k1gMnPc2	Branibor
požádal	požádat	k5eAaPmAgMnS	požádat
Smetana	Smetana	k1gMnSc1	Smetana
Sabinu	Sabina	k1gFnSc4	Sabina
o	o	k7c4	o
libreto	libreto	k1gNnSc4	libreto
ke	k	k7c3	k
komické	komický	k2eAgFnSc3d1	komická
opeře	opera	k1gFnSc3	opera
<g/>
.	.	kIx.	.
</s>
<s>
Sabina	Sabina	k1gFnSc1	Sabina
první	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
libreta	libreto	k1gNnSc2	libreto
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
dodal	dodat	k5eAaPmAgInS	dodat
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1863	[number]	k4	1863
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
předehra	předehra	k1gFnSc1	předehra
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
kompozice	kompozice	k1gFnSc1	kompozice
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1866	[number]	k4	1866
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
Braniborů	Branibor	k1gMnPc2	Branibor
byla	být	k5eAaImAgFnS	být
nová	nový	k2eAgFnSc1d1	nová
opera	opera	k1gFnSc1	opera
-	-	kIx~	-
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
jednáních	jednání	k1gNnPc6	jednání
a	a	k8xC	a
s	s	k7c7	s
mluvenými	mluvený	k2eAgInPc7d1	mluvený
dialogy	dialog	k1gInPc7	dialog
-	-	kIx~	-
zařazena	zařadit	k5eAaPmNgFnS	zařadit
na	na	k7c4	na
program	program	k1gInSc4	program
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
bez	bez	k7c2	bez
odkladu	odklad	k1gInSc2	odklad
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1866	[number]	k4	1866
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
příliš	příliš	k6eAd1	příliš
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
bezprostředně	bezprostředně	k6eAd1	bezprostředně
hrozící	hrozící	k2eAgFnSc2d1	hrozící
prusko-rakouské	pruskoakouský	k2eAgFnSc2d1	prusko-rakouská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Pražanů	Pražan	k1gMnPc2	Pražan
se	se	k3xPyFc4	se
uchylovalo	uchylovat	k5eAaImAgNnS	uchylovat
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
(	(	kIx(	(
<g/>
následoval	následovat	k5eAaImAgInS	následovat
je	být	k5eAaImIp3nS	být
i	i	k9	i
Smetana	Smetana	k1gMnSc1	Smetana
obávající	obávající	k2eAgMnSc1d1	obávající
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
Prusové	Prus	k1gMnPc1	Prus
jakožto	jakožto	k8xS	jakožto
autora	autor	k1gMnSc2	autor
Braniborů	Branibor	k1gMnPc2	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
zastřelí	zastřelit	k5eAaPmIp3nP	zastřelit
<g/>
)	)	kIx)	)
a	a	k8xC	a
nedostatek	nedostatek	k1gInSc1	nedostatek
diváků	divák	k1gMnPc2	divák
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
finančnímu	finanční	k2eAgInSc3d1	finanční
kolapsu	kolaps	k1gInSc3	kolaps
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
však	však	k9	však
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
ve	v	k7c4	v
Smetanův	Smetanův	k2eAgInSc4d1	Smetanův
prospěch	prospěch	k1gInSc4	prospěch
<g/>
:	:	kIx,	:
vedení	vedení	k1gNnSc1	vedení
divadla	divadlo	k1gNnSc2	divadlo
převzalo	převzít	k5eAaPmAgNnS	převzít
nově	nově	k6eAd1	nově
utvořené	utvořený	k2eAgNnSc1d1	utvořené
družstvo	družstvo	k1gNnSc1	družstvo
s	s	k7c7	s
převládajícím	převládající	k2eAgInSc7d1	převládající
mladočeským	mladočeský	k2eAgInSc7d1	mladočeský
vlivem	vliv	k1gInSc7	vliv
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1866	[number]	k4	1866
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
staročeského	staročeský	k2eAgInSc2d1	staročeský
prvního	první	k4xOgMnSc2	první
kapelníka	kapelník	k1gMnSc2	kapelník
Maýra	Maýro	k1gNnSc2	Maýro
mladočechem	mladočech	k1gMnSc7	mladočech
Smetanou	Smetana	k1gMnSc7	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
reprízy	repríza	k1gFnPc1	repríza
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
již	již	k6eAd1	již
byly	být	k5eAaImAgInP	být
přijímány	přijímat	k5eAaImNgInP	přijímat
stále	stále	k6eAd1	stále
nadšeněji	nadšeně	k6eAd2	nadšeně
<g/>
;	;	kIx,	;
v	v	k7c6	v
partituře	partitura	k1gFnSc6	partitura
však	však	k9	však
probíhaly	probíhat	k5eAaImAgFnP	probíhat
nadále	nadále	k6eAd1	nadále
podstatné	podstatný	k2eAgFnPc1d1	podstatná
změny	změna	k1gFnPc1	změna
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
konečná	konečný	k2eAgFnSc1d1	konečná
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
verze	verze	k1gFnSc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
přebíral	přebírat	k5eAaImAgMnS	přebírat
kapelnickou	kapelnický	k2eAgFnSc4d1	kapelnická
funkci	funkce	k1gFnSc4	funkce
s	s	k7c7	s
programem	program	k1gInSc7	program
učinit	učinit	k5eAaImF	učinit
Prozatímní	prozatímní	k2eAgNnSc4d1	prozatímní
divadlo	divadlo	k1gNnSc4	divadlo
"	"	kIx"	"
<g/>
centrálním	centrální	k2eAgNnSc7d1	centrální
střediskem	středisko	k1gNnSc7	středisko
veškerého	veškerý	k3xTgInSc2	veškerý
domácího	domácí	k2eAgInSc2d1	domácí
světa	svět	k1gInSc2	svět
uměleckého	umělecký	k2eAgInSc2d1	umělecký
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duchu	duch	k1gMnSc6	duch
Lisztova	Lisztův	k2eAgNnSc2d1	Lisztovo
působení	působení	k1gNnSc3	působení
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
zde	zde	k6eAd1	zde
chtěl	chtít	k5eAaImAgMnS	chtít
postupně	postupně	k6eAd1	postupně
uvést	uvést	k5eAaPmF	uvést
"	"	kIx"	"
<g/>
všechna	všechen	k3xTgNnPc4	všechen
mistrovská	mistrovský	k2eAgNnPc4d1	mistrovské
díla	dílo	k1gNnPc4	dílo
světová	světový	k2eAgNnPc4d1	světové
v	v	k7c6	v
nejpřesnější	přesný	k2eAgFnSc6d3	nejpřesnější
reprodukci	reprodukce	k1gFnSc6	reprodukce
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
však	však	k9	však
pěstovat	pěstovat	k5eAaImF	pěstovat
zárodky	zárodek	k1gInPc4	zárodek
svébytné	svébytný	k2eAgFnSc2d1	svébytná
národní	národní	k2eAgFnSc2d1	národní
hudební	hudební	k2eAgFnSc2d1	hudební
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Operu	opera	k1gFnSc4	opera
přebíral	přebírat	k5eAaImAgMnS	přebírat
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
konsolidovaném	konsolidovaný	k2eAgInSc6d1	konsolidovaný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
působení	působení	k1gNnSc1	působení
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
omezováno	omezován	k2eAgNnSc1d1	omezováno
týmiž	týž	k3xTgInPc7	týž
faktory	faktor	k1gInPc7	faktor
jako	jako	k8xS	jako
za	za	k7c4	za
Maýra	Maýr	k1gMnSc4	Maýr
a	a	k8xC	a
Smetana	Smetana	k1gMnSc1	Smetana
musel	muset	k5eAaImAgMnS	muset
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
svých	svůj	k3xOyFgFnPc2	svůj
zásad	zásada	k1gFnPc2	zásada
slevovat	slevovat	k5eAaImF	slevovat
<g/>
.	.	kIx.	.
</s>
<s>
Stísněné	stísněný	k2eAgFnPc1d1	stísněná
prostory	prostora	k1gFnPc1	prostora
divadla	divadlo	k1gNnSc2	divadlo
značně	značně	k6eAd1	značně
omezovaly	omezovat	k5eAaImAgFnP	omezovat
inscenační	inscenační	k2eAgFnPc1d1	inscenační
možnosti	možnost	k1gFnPc1	možnost
<g/>
,	,	kIx,	,
orchestr	orchestr	k1gInSc1	orchestr
zůstával	zůstávat	k5eAaImAgInS	zůstávat
již	již	k6eAd1	již
velikostí	velikost	k1gFnSc7	velikost
orchestřiště	orchestřiště	k1gNnSc2	orchestřiště
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
35	[number]	k4	35
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
i	i	k9	i
sbor	sbor	k1gInSc1	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
soubor	soubor	k1gInSc1	soubor
byl	být	k5eAaImAgInS	být
nevyrovnaný	vyrovnaný	k2eNgMnSc1d1	nevyrovnaný
a	a	k8xC	a
podléhal	podléhat	k5eAaImAgInS	podléhat
četným	četný	k2eAgFnPc3d1	četná
fluktuacím	fluktuace	k1gFnPc3	fluktuace
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejkontroverznější	kontroverzní	k2eAgInPc1d3	nejkontroverznější
bývaly	bývat	k5eAaImAgInP	bývat
odchody	odchod	k1gInPc1	odchod
do	do	k7c2	do
pražského	pražský	k2eAgNnSc2d1	Pražské
německého	německý	k2eAgNnSc2d1	německé
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
Dobš	Dobš	k1gMnSc1	Dobš
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šebesta	Šebesta	k1gMnSc1	Šebesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cenné	cenný	k2eAgFnPc1d1	cenná
síly	síla	k1gFnPc1	síla
však	však	k9	však
odešly	odejít	k5eAaPmAgFnP	odejít
také	také	k9	také
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Antonín	Antonín	k1gMnSc1	Antonín
Barcal	Barcal	k1gMnSc1	Barcal
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Paleček	Paleček	k1gMnSc1	Paleček
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zejména	zejména	k9	zejména
obory	obora	k1gFnPc4	obora
dramatického	dramatický	k2eAgInSc2d1	dramatický
tenoru	tenor	k1gInSc2	tenor
a	a	k8xC	a
sopránu	soprán	k1gInSc2	soprán
byly	být	k5eAaImAgInP	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
neobsazeny	obsazen	k2eNgInPc1d1	neobsazen
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
měl	mít	k5eAaImAgMnS	mít
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
konsolidaci	konsolidace	k1gFnSc6	konsolidace
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc6	rozvoj
operního	operní	k2eAgInSc2d1	operní
baletu	balet	k1gInSc2	balet
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1868	[number]	k4	1868
byla	být	k5eAaImAgFnS	být
při	při	k7c6	při
divadle	divadlo	k1gNnSc6	divadlo
zřízena	zřízen	k2eAgFnSc1d1	zřízena
baletní	baletní	k2eAgFnSc1d1	baletní
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vedla	vést	k5eAaImAgFnS	vést
nově	nově	k6eAd1	nově
jmenovaná	jmenovaný	k2eAgFnSc1d1	jmenovaná
baletní	baletní	k2eAgFnSc1d1	baletní
mistryně	mistryně	k1gFnSc1	mistryně
Marie	Marie	k1gFnSc1	Marie
Hentzová	Hentzová	k1gFnSc1	Hentzová
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
divadelní	divadelní	k2eAgFnSc2d1	divadelní
režie	režie	k1gFnSc2	režie
a	a	k8xC	a
zasadil	zasadit	k5eAaPmAgMnS	zasadit
se	se	k3xPyFc4	se
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
prvního	první	k4xOgMnSc2	první
operního	operní	k2eAgMnSc2d1	operní
režiséra	režisér	k1gMnSc2	režisér
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
významného	významný	k2eAgMnSc2d1	významný
zastánce	zastánce	k1gMnSc2	zastánce
jevištního	jevištní	k2eAgInSc2d1	jevištní
realismu	realismus	k1gInSc2	realismus
Edmunda	Edmund	k1gMnSc2	Edmund
Chvalovského	Chvalovský	k2eAgMnSc2d1	Chvalovský
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
byl	být	k5eAaImAgMnS	být
Smetanovi	Smetana	k1gMnSc3	Smetana
po	po	k7c6	po
ruce	ruka	k1gFnSc6	ruka
spolehlivý	spolehlivý	k2eAgMnSc1d1	spolehlivý
druhý	druhý	k4xOgMnSc1	druhý
kapelník	kapelník	k1gMnSc1	kapelník
Adolf	Adolf	k1gMnSc1	Adolf
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
další	další	k2eAgMnPc1d1	další
kapelníci	kapelník	k1gMnPc1	kapelník
<g/>
,	,	kIx,	,
kterými	který	k3yQgMnPc7	který
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
významní	významný	k2eAgMnPc1d1	významný
hudebníci	hudebník	k1gMnPc1	hudebník
a	a	k8xC	a
skladatelé	skladatel	k1gMnPc1	skladatel
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Šebor	Šebor	k1gMnSc1	Šebor
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hřímalý	Hřímalý	k2eAgMnSc1d1	Hřímalý
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Bendl	Bendl	k1gMnSc1	Bendl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odcházeli	odcházet	k5eAaImAgMnP	odcházet
mnohdy	mnohdy	k6eAd1	mnohdy
záhy	záhy	k6eAd1	záhy
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
vedle	vedle	k7c2	vedle
Smetany	smetana	k1gFnSc2	smetana
(	(	kIx(	(
<g/>
a	a	k8xC	a
Čecha	Čech	k1gMnSc4	Čech
<g/>
)	)	kIx)	)
uplatnit	uplatnit	k5eAaPmF	uplatnit
jako	jako	k9	jako
samostatné	samostatný	k2eAgFnPc4d1	samostatná
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
J.	J.	kA	J.
N.	N.	kA	N.
Maýr	Maýr	k1gMnSc1	Maýr
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
operní	operní	k2eAgFnSc2d1	operní
pěvecké	pěvecký	k2eAgFnSc2d1	pěvecká
školy	škola	k1gFnSc2	škola
při	při	k7c6	při
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobný	podrobný	k2eAgInSc1d1	podrobný
návrh	návrh	k1gInSc1	návrh
podal	podat	k5eAaPmAgInS	podat
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
pěvecký	pěvecký	k2eAgMnSc1d1	pěvecký
pedagog	pedagog	k1gMnSc1	pedagog
František	František	k1gMnSc1	František
Pivoda	Pivoda	k1gMnSc1	Pivoda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
však	však	k9	však
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
<g/>
,	,	kIx,	,
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
i	i	k9	i
pro	pro	k7c4	pro
rozdílnost	rozdílnost	k1gFnSc4	rozdílnost
uměleckých	umělecký	k2eAgInPc2d1	umělecký
záměrů	záměr	k1gInPc2	záměr
Smetany	smetana	k1gFnSc2	smetana
a	a	k8xC	a
Pivody	Pivoda	k1gFnSc2	Pivoda
<g/>
;	;	kIx,	;
ten	ten	k3xDgMnSc1	ten
tedy	tedy	k8xC	tedy
založil	založit	k5eAaPmAgMnS	založit
soukromou	soukromý	k2eAgFnSc4d1	soukromá
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
nakonec	nakonec	k6eAd1	nakonec
prosadil	prosadit	k5eAaPmAgMnS	prosadit
otevření	otevření	k1gNnSc4	otevření
operní	operní	k2eAgFnSc2d1	operní
školy	škola	k1gFnSc2	škola
při	při	k7c6	při
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1873	[number]	k4	1873
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
provoz	provoz	k1gInSc1	provoz
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1874	[number]	k4	1874
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
první	první	k4xOgInSc1	první
žákovský	žákovský	k2eAgInSc1d1	žákovský
koncert	koncert	k1gInSc1	koncert
<g/>
,	,	kIx,	,
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
však	však	k9	však
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Smetany	smetana	k1gFnSc2	smetana
škola	škola	k1gFnSc1	škola
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
soukromá	soukromý	k2eAgFnSc1d1	soukromá
škola	škola	k1gFnSc1	škola
J.	J.	kA	J.
L.	L.	kA	L.
Lukese	Lukese	k1gFnSc1	Lukese
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
dramaturgický	dramaturgický	k2eAgInSc4d1	dramaturgický
program	program	k1gInSc4	program
mohl	moct	k5eAaImAgMnS	moct
Smetana	Smetana	k1gMnSc1	Smetana
realizovat	realizovat	k5eAaBmF	realizovat
jen	jen	k9	jen
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
<s>
Limitovaly	limitovat	k5eAaBmAgFnP	limitovat
ho	on	k3xPp3gMnSc4	on
přitom	přitom	k6eAd1	přitom
nejen	nejen	k6eAd1	nejen
provozní	provozní	k2eAgFnPc1d1	provozní
a	a	k8xC	a
finanční	finanční	k2eAgFnPc1d1	finanční
otázky	otázka	k1gFnPc1	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Víra	víra	k1gFnSc1	víra
v	v	k7c4	v
obecenstvo	obecenstvo	k1gNnSc4	obecenstvo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
spontánně	spontánně	k6eAd1	spontánně
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
a	a	k8xC	a
přijímá	přijímat	k5eAaImIp3nS	přijímat
předkládané	předkládaný	k2eAgFnPc4d1	Předkládaná
umělecké	umělecký	k2eAgFnPc4d1	umělecká
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
ohledům	ohled	k1gInPc3	ohled
na	na	k7c4	na
konzervativní	konzervativní	k2eAgInSc4d1	konzervativní
vkus	vkus	k1gInSc4	vkus
pražského	pražský	k2eAgNnSc2d1	Pražské
publika	publikum	k1gNnSc2	publikum
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
dávalo	dávat	k5eAaImAgNnS	dávat
přednost	přednost	k1gFnSc4	přednost
výpravným	výpravný	k2eAgMnPc3d1	výpravný
<g/>
,	,	kIx,	,
zábavným	zábavný	k2eAgMnPc3d1	zábavný
nebo	nebo	k8xC	nebo
pěvecky	pěvecky	k6eAd1	pěvecky
efektním	efektní	k2eAgNnSc7d1	efektní
představením	představení	k1gNnSc7	představení
<g/>
.	.	kIx.	.
</s>
<s>
Velkému	velký	k2eAgInSc3d1	velký
zájmu	zájem	k1gInSc3	zájem
se	se	k3xPyFc4	se
těšila	těšit	k5eAaImAgFnS	těšit
opereta	opereta	k1gFnSc1	opereta
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díla	dílo	k1gNnPc1	dílo
Offenbachova	Offenbachův	k2eAgNnPc1d1	Offenbachův
<g/>
,	,	kIx,	,
Suppého	Suppý	k2eAgInSc2d1	Suppý
a	a	k8xC	a
Zajcova	Zajcův	k2eAgInSc2d1	Zajcův
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
tuto	tento	k3xDgFnSc4	tento
vlnu	vlna	k1gFnSc4	vlna
korigovat	korigovat	k5eAaBmF	korigovat
a	a	k8xC	a
vzdělávat	vzdělávat	k5eAaImF	vzdělávat
vkus	vkus	k1gInSc4	vkus
lidového	lidový	k2eAgMnSc2d1	lidový
diváka	divák	k1gMnSc2	divák
uváděním	uvádění	k1gNnPc3	uvádění
děl	dělo	k1gNnPc2	dělo
"	"	kIx"	"
<g/>
malých	malý	k2eAgMnPc2d1	malý
mistrů	mistr	k1gMnPc2	mistr
<g/>
"	"	kIx"	"
francouzské	francouzský	k2eAgFnSc2d1	francouzská
komické	komický	k2eAgFnSc2d1	komická
opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
Auber	Auber	k1gMnSc1	Auber
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
<g/>
,	,	kIx,	,
Hérold	Hérold	k1gMnSc1	Hérold
<g/>
,	,	kIx,	,
Boieldieu	Boieldieus	k1gInSc2	Boieldieus
<g/>
,	,	kIx,	,
Halévy	Haléva	k1gFnSc2	Haléva
<g/>
,	,	kIx,	,
Grisar	Grisar	k1gMnSc1	Grisar
<g/>
,	,	kIx,	,
Gevaert	Gevaert	k1gMnSc1	Gevaert
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
velká	velký	k2eAgFnSc1d1	velká
opera	opera	k1gFnSc1	opera
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
významným	významný	k2eAgInSc7d1	významný
a	a	k8xC	a
nevyhnutelným	vyhnutelný	k2eNgInSc7d1	nevyhnutelný
žánrem	žánr	k1gInSc7	žánr
<g/>
,	,	kIx,	,
Smetana	Smetana	k1gMnSc1	Smetana
však	však	k9	však
do	do	k7c2	do
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
zavedl	zavést	k5eAaPmAgInS	zavést
lyričtější	lyrický	k2eAgInSc1d2	lyričtější
a	a	k8xC	a
neokázalejší	okázalý	k2eNgNnPc4d2	okázalý
díla	dílo	k1gNnPc4	dílo
Charlese	Charles	k1gMnSc2	Charles
Gounoda	Gounod	k1gMnSc2	Gounod
<g/>
,	,	kIx,	,
jemu	on	k3xPp3gMnSc3	on
zvláště	zvláště	k6eAd1	zvláště
blízkého	blízký	k2eAgNnSc2d1	blízké
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stylově	stylově	k6eAd1	stylově
zastarávající	zastarávající	k2eAgInSc1d1	zastarávající
belcantový	belcantový	k2eAgInSc1d1	belcantový
repertoár	repertoár	k1gInSc1	repertoár
vážné	vážný	k2eAgFnSc2d1	vážná
opery	opera	k1gFnSc2	opera
(	(	kIx(	(
<g/>
Donizetti	Donizett	k1gMnPc1	Donizett
<g/>
,	,	kIx,	,
Bellini	Bellin	k2eAgMnPc1d1	Bellin
<g/>
)	)	kIx)	)
nerušil	rušit	k5eNaImAgMnS	rušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
příliš	příliš	k6eAd1	příliš
nerozšiřoval	rozšiřovat	k5eNaImAgMnS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Raději	rád	k6eAd2	rád
uváděl	uvádět	k5eAaImAgInS	uvádět
pozdní	pozdní	k2eAgInSc1d1	pozdní
Verdiho	Verdi	k1gMnSc2	Verdi
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
si	se	k3xPyFc3	se
cenil	cenit	k5eAaImAgMnS	cenit
Rossiniho	Rossini	k1gMnSc2	Rossini
a	a	k8xC	a
dbal	dbát	k5eAaImAgMnS	dbát
o	o	k7c4	o
větší	veliký	k2eAgNnSc4d2	veliký
zastoupení	zastoupení	k1gNnSc4	zastoupení
italské	italský	k2eAgFnSc2d1	italská
komické	komický	k2eAgFnSc2d1	komická
opery	opera	k1gFnSc2	opera
od	od	k7c2	od
Cimarosova	Cimarosův	k2eAgNnSc2d1	Cimarosův
Tajného	tajný	k2eAgNnSc2d1	tajné
manželství	manželství	k1gNnSc2	manželství
přes	přes	k7c4	přes
Donizettiho	Donizetti	k1gMnSc4	Donizetti
Dona	Don	k1gMnSc4	Don
Pasquala	Pasqual	k1gMnSc4	Pasqual
a	a	k8xC	a
Rossiniho	Rossini	k1gMnSc4	Rossini
Lazebníka	lazebník	k1gMnSc4	lazebník
sevillského	sevillský	k2eAgMnSc4d1	sevillský
a	a	k8xC	a
Popelku	Popelka	k1gMnSc4	Popelka
po	po	k7c4	po
Kryšpína	Kryšpín	k1gMnSc4	Kryšpín
a	a	k8xC	a
kmotru	kmotra	k1gFnSc4	kmotra
bratří	bratr	k1gMnPc2	bratr
Ricciů	Ricci	k1gMnPc2	Ricci
<g/>
,	,	kIx,	,
uvedenou	uvedený	k2eAgFnSc7d1	uvedená
na	na	k7c4	na
české	český	k2eAgNnSc4d1	české
jeviště	jeviště	k1gNnSc4	jeviště
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
hostující	hostující	k2eAgFnSc7d1	hostující
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Giuseppinou	Giuseppina	k1gFnSc7	Giuseppina
Vitelliovou	Vitelliový	k2eAgFnSc7d1	Vitelliový
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnSc3	svůj
představě	představa	k1gFnSc3	představa
o	o	k7c6	o
výchovném	výchovný	k2eAgNnSc6d1	výchovné
poslání	poslání	k1gNnSc6	poslání
národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Smetana	Smetana	k1gMnSc1	Smetana
dostál	dostát	k5eAaPmAgMnS	dostát
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
klasickém	klasický	k2eAgInSc6d1	klasický
německém	německý	k2eAgInSc6d1	německý
repertoáru	repertoár	k1gInSc6	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
kulturní	kulturní	k2eAgFnSc4d1	kulturní
tradici	tradice	k1gFnSc4	tradice
odkazovala	odkazovat	k5eAaImAgFnS	odkazovat
nejen	nejen	k6eAd1	nejen
hudebně	hudebně	k6eAd1	hudebně
co	co	k9	co
nejpečlivější	pečlivý	k2eAgNnSc4d3	nejpečlivější
nastudování	nastudování	k1gNnSc4	nastudování
oper	opera	k1gFnPc2	opera
Gluckových	Gluckových	k2eAgFnSc2d1	Gluckových
a	a	k8xC	a
Mozartových	Mozartových	k2eAgFnSc2d1	Mozartových
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
nově	nově	k6eAd1	nově
uvedený	uvedený	k2eAgMnSc1d1	uvedený
Beethovenův	Beethovenův	k2eAgMnSc1d1	Beethovenův
Fidelio	Fidelio	k1gMnSc1	Fidelio
<g/>
.	.	kIx.	.
</s>
<s>
Málo	málo	k6eAd1	málo
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
byl	být	k5eAaImAgInS	být
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
slovanského	slovanský	k2eAgInSc2d1	slovanský
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1867	[number]	k4	1867
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Prahu	Praha	k1gFnSc4	Praha
Milij	Milij	k1gMnSc1	Milij
Alexejevič	Alexejevič	k1gMnSc1	Alexejevič
Balakirev	Balakirev	k1gFnSc1	Balakirev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zde	zde	k6eAd1	zde
řídil	řídit	k5eAaImAgInS	řídit
Glinkův	Glinkův	k2eAgInSc1d1	Glinkův
Život	život	k1gInSc1	život
za	za	k7c4	za
cara	car	k1gMnSc4	car
a	a	k8xC	a
nově	nově	k6eAd1	nově
nastudoval	nastudovat	k5eAaBmAgMnS	nastudovat
Ruslana	Ruslan	k1gMnSc4	Ruslan
a	a	k8xC	a
Ludmilu	Ludmila	k1gFnSc4	Ludmila
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
však	však	k9	však
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
kvůli	kvůli	k7c3	kvůli
změně	změna	k1gFnSc3	změna
praktikovaných	praktikovaný	k2eAgInPc2d1	praktikovaný
škrtů	škrt	k1gInPc2	škrt
v	v	k7c6	v
partituře	partitura	k1gFnSc6	partitura
<g/>
,	,	kIx,	,
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
konflikty	konflikt	k1gInPc4	konflikt
s	s	k7c7	s
uměleckým	umělecký	k2eAgInSc7d1	umělecký
souborem	soubor	k1gInSc7	soubor
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
se	s	k7c7	s
Smetanou	Smetana	k1gMnSc7	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Lépe	dobře	k6eAd2	dobře
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
návštěva	návštěva	k1gFnSc1	návštěva
Stanisława	Stanisława	k1gFnSc1	Stanisława
Moniuszka	Moniuszka	k1gFnSc1	Moniuszka
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgInSc2	jenž
Halku	Halk	k1gInSc2	Halk
Smetana	Smetana	k1gMnSc1	Smetana
nastudoval	nastudovat	k5eAaBmAgMnS	nastudovat
<g/>
,	,	kIx,	,
inscenace	inscenace	k1gFnSc1	inscenace
však	však	k9	však
publikum	publikum	k1gNnSc4	publikum
nezaujala	zaujmout	k5eNaPmAgFnS	zaujmout
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
i	i	k9	i
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
tisku	tisk	k1gInSc6	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
slovanských	slovanský	k2eAgFnPc2d1	Slovanská
oper	opera	k1gFnPc2	opera
Smetana	Smetana	k1gMnSc1	Smetana
neuvedl	uvést	k5eNaPmAgMnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
novinek	novinka	k1gFnPc2	novinka
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
tvořila	tvořit	k5eAaImAgFnS	tvořit
díla	dílo	k1gNnPc4	dílo
českých	český	k2eAgMnPc2d1	český
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
uváděl	uvádět	k5eAaImAgMnS	uvádět
starší	starší	k1gMnSc1	starší
díla	dílo	k1gNnSc2	dílo
žijících	žijící	k2eAgMnPc2d1	žijící
skladatelů	skladatel	k1gMnPc2	skladatel
(	(	kIx(	(
<g/>
J.	J.	kA	J.
N.	N.	kA	N.
Škroup	Škroup	k1gMnSc1	Škroup
<g/>
,	,	kIx,	,
Skuherský	Skuherský	k2eAgMnSc1d1	Skuherský
<g/>
,	,	kIx,	,
Měchura	Měchura	k?	Měchura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
mladších	mladý	k2eAgMnPc2d2	mladší
skladatelů	skladatel	k1gMnPc2	skladatel
(	(	kIx(	(
<g/>
Šebor	Šebor	k1gMnSc1	Šebor
<g/>
,	,	kIx,	,
Bendl	Bendl	k1gMnSc1	Bendl
<g/>
,	,	kIx,	,
Blodek	Blodek	k1gMnSc1	Blodek
<g/>
,	,	kIx,	,
Rozkošný	rozkošný	k2eAgMnSc1d1	rozkošný
<g/>
,	,	kIx,	,
Fibich	Fibich	k1gMnSc1	Fibich
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dvořákova	Dvořákův	k2eAgFnSc1d1	Dvořákova
první	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
opera	opera	k1gFnSc1	opera
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
uhlíř	uhlíř	k1gMnSc1	uhlíř
se	se	k3xPyFc4	se
však	však	k9	však
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
verzi	verze	k1gFnSc6	verze
za	za	k7c2	za
Smetany	smetana	k1gFnSc2	smetana
na	na	k7c4	na
jeviště	jeviště	k1gNnSc4	jeviště
nedostala	dostat	k5eNaPmAgFnS	dostat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
zkoušena	zkoušen	k2eAgFnSc1d1	zkoušena
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
inscenační	inscenační	k2eAgFnSc6d1	inscenační
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
Smetanovi	Smetana	k1gMnSc3	Smetana
nepodařilo	podařit	k5eNaPmAgNnS	podařit
odstranit	odstranit	k5eAaPmF	odstranit
všechny	všechen	k3xTgInPc4	všechen
nešvary	nešvar	k1gInPc4	nešvar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dříve	dříve	k6eAd2	dříve
divadlu	divadlo	k1gNnSc3	divadlo
vytýkal	vytýkat	k5eAaImAgInS	vytýkat
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbená	oblíbený	k2eAgNnPc4d1	oblíbené
hostování	hostování	k1gNnPc4	hostování
cizích	cizí	k2eAgMnPc2d1	cizí
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
volili	volit	k5eAaImAgMnP	volit
vesměs	vesměs	k6eAd1	vesměs
efektní	efektní	k2eAgFnPc4d1	efektní
úlohy	úloha	k1gFnPc4	úloha
ze	z	k7c2	z
známých	známý	k2eAgNnPc2d1	známé
<g/>
,	,	kIx,	,
až	až	k9	až
ohraných	ohraný	k2eAgFnPc2d1	ohraná
oper	opera	k1gFnPc2	opera
<g/>
,	,	kIx,	,
přispívala	přispívat	k5eAaImAgFnS	přispívat
k	k	k7c3	k
repertoárové	repertoárový	k2eAgFnSc3d1	repertoárová
stagnaci	stagnace	k1gFnSc3	stagnace
<g/>
;	;	kIx,	;
navíc	navíc	k6eAd1	navíc
zpívali	zpívat	k5eAaImAgMnP	zpívat
i	i	k9	i
v	v	k7c6	v
českých	český	k2eAgNnPc6d1	české
představeních	představení	k1gNnPc6	představení
v	v	k7c6	v
cizích	cizí	k2eAgInPc6d1	cizí
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
dovolovali	dovolovat	k5eAaImAgMnP	dovolovat
si	se	k3xPyFc3	se
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
manýry	manýra	k1gFnPc4	manýra
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
systém	systém	k1gInSc1	systém
beneficí	benefic	k1gFnPc2	benefic
(	(	kIx(	(
<g/>
představení	představení	k1gNnSc1	představení
podle	podle	k7c2	podle
volby	volba	k1gFnSc2	volba
účinkujících	účinkující	k1gMnPc2	účinkující
se	s	k7c7	s
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
výdělkem	výdělek	k1gInSc7	výdělek
v	v	k7c4	v
jejich	jejich	k3xOp3gInSc4	jejich
prospěch	prospěch	k1gInSc4	prospěch
<g/>
)	)	kIx)	)
narušoval	narušovat	k5eAaImAgMnS	narušovat
soustavné	soustavný	k2eAgFnPc4d1	soustavná
dramaturgické	dramaturgický	k2eAgFnPc4d1	dramaturgická
snahy	snaha	k1gFnPc4	snaha
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
inscenačních	inscenační	k2eAgInPc2d1	inscenační
škrtů	škrt	k1gInPc2	škrt
v	v	k7c6	v
operách	opera	k1gFnPc6	opera
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Smetana	Smetana	k1gMnSc1	Smetana
u	u	k7c2	u
Maýra	Maýr	k1gInSc2	Maýr
často	často	k6eAd1	často
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
<g/>
,	,	kIx,	,
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
zachované	zachovaný	k2eAgInPc1d1	zachovaný
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
oba	dva	k4xCgMnPc1	dva
kapelníci	kapelník	k1gMnPc1	kapelník
nijak	nijak	k6eAd1	nijak
významně	významně	k6eAd1	významně
nelišili	lišit	k5eNaImAgMnP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
dirigent	dirigent	k1gMnSc1	dirigent
se	se	k3xPyFc4	se
Smetana	Smetana	k1gMnSc1	Smetana
uvedl	uvést	k5eAaPmAgMnS	uvést
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1866	[number]	k4	1866
Weberovým	Weberův	k2eAgMnSc7d1	Weberův
Čarostřelcem	čarostřelec	k1gMnSc7	čarostřelec
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
vedle	vedle	k7c2	vedle
svých	svůj	k3xOyFgFnPc2	svůj
oper	opera	k1gFnPc2	opera
většinou	většinou	k6eAd1	většinou
vážné	vážný	k2eAgFnSc2d1	vážná
opery	opera	k1gFnSc2	opera
všech	všecek	k3xTgFnPc2	všecek
národních	národní	k2eAgFnPc2d1	národní
škol	škola	k1gFnPc2	škola
včetně	včetně	k7c2	včetně
českých	český	k2eAgFnPc2d1	Česká
novinek	novinka	k1gFnPc2	novinka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
většinu	většina	k1gFnSc4	většina
italského	italský	k2eAgInSc2d1	italský
repertoáru	repertoár	k1gInSc2	repertoár
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
komické	komický	k2eAgFnPc1d1	komická
opery	opera	k1gFnPc1	opera
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Mozarta	Mozart	k1gMnSc2	Mozart
<g/>
,	,	kIx,	,
Rossiniho	Rossini	k1gMnSc2	Rossini
a	a	k8xC	a
Ricciů	Ricci	k1gMnPc2	Ricci
<g/>
)	)	kIx)	)
a	a	k8xC	a
operety	opereta	k1gFnPc4	opereta
přenechával	přenechávat	k5eAaImAgMnS	přenechávat
zejména	zejména	k9	zejména
svému	svůj	k3xOyFgMnSc3	svůj
druhému	druhý	k4xOgMnSc3	druhý
kapelníkovi	kapelník	k1gMnSc3	kapelník
Adolfu	Adolf	k1gMnSc3	Adolf
Čechovi	Čech	k1gMnSc3	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
dirigování	dirigování	k1gNnSc2	dirigování
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
využil	využít	k5eAaPmAgMnS	využít
dostupnosti	dostupnost	k1gFnPc4	dostupnost
orchestru	orchestr	k1gInSc2	orchestr
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1869	[number]	k4	1869
<g/>
/	/	kIx~	/
<g/>
70	[number]	k4	70
k	k	k7c3	k
pořádání	pořádání	k1gNnSc3	pořádání
symfonických	symfonický	k2eAgInPc2d1	symfonický
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
prosazoval	prosazovat	k5eAaImAgInS	prosazovat
náročnější	náročný	k2eAgInSc1d2	náročnější
nebo	nebo	k8xC	nebo
modernější	moderní	k2eAgInSc1d2	modernější
repertoár	repertoár	k1gInSc1	repertoár
včetně	včetně	k7c2	včetně
děl	dělo	k1gNnPc2	dělo
domácích	domácí	k2eAgMnPc2d1	domácí
umělců	umělec	k1gMnPc2	umělec
<g/>
:	:	kIx,	:
uvedl	uvést	k5eAaPmAgMnS	uvést
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
např.	např.	kA	např.
Dvořákovu	Dvořákův	k2eAgFnSc4d1	Dvořákova
předehru	předehra	k1gFnSc4	předehra
k	k	k7c3	k
opeře	opera	k1gFnSc3	opera
Král	Král	k1gMnSc1	Král
a	a	k8xC	a
uhlíř	uhlíř	k1gMnSc1	uhlíř
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
symfonii	symfonie	k1gFnSc4	symfonie
č.	č.	k?	č.
3	[number]	k4	3
Es	es	k1gNnSc2	es
dur	dur	k1gNnSc2	dur
nebo	nebo	k8xC	nebo
Fibichovu	Fibichův	k2eAgFnSc4d1	Fibichova
symfonickou	symfonický	k2eAgFnSc4d1	symfonická
báseň	báseň	k1gFnSc4	báseň
Othello	Othello	k1gMnSc1	Othello
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1870	[number]	k4	1870
<g/>
/	/	kIx~	/
<g/>
71	[number]	k4	71
se	se	k3xPyFc4	se
koncertů	koncert	k1gInPc2	koncert
účastnili	účastnit	k5eAaImAgMnP	účastnit
i	i	k9	i
členové	člen	k1gMnPc1	člen
orchestru	orchestr	k1gInSc2	orchestr
německého	německý	k2eAgNnSc2d1	německé
Stavovského	stavovský	k2eAgNnSc2d1	Stavovské
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
jejich	jejich	k3xOp3gNnSc1	jejich
pořádání	pořádání	k1gNnSc1	pořádání
zaštiťoval	zaštiťovat	k5eAaImAgInS	zaštiťovat
penzijní	penzijní	k2eAgInSc1d1	penzijní
spolek	spolek	k1gInSc1	spolek
orchestrů	orchestr	k1gInPc2	orchestr
obou	dva	k4xCgInPc2	dva
divadel	divadlo	k1gNnPc2	divadlo
"	"	kIx"	"
<g/>
Filharmonie	filharmonie	k1gFnSc1	filharmonie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
dirigování	dirigování	k1gNnSc6	dirigování
se	se	k3xPyFc4	se
Smetana	Smetana	k1gMnSc1	Smetana
střídal	střídat	k5eAaImAgMnS	střídat
s	s	k7c7	s
prvním	první	k4xOgMnSc7	první
kapelníkem	kapelník	k1gMnSc7	kapelník
německého	německý	k2eAgNnSc2d1	německé
divadla	divadlo	k1gNnSc2	divadlo
Ludwigem	Ludwig	k1gInSc7	Ludwig
Slanskym	Slanskym	k1gInSc1	Slanskym
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
kapelník	kapelník	k1gMnSc1	kapelník
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
zastával	zastávat	k5eAaImAgMnS	zastávat
Smetana	Smetana	k1gMnSc1	Smetana
významné	významný	k2eAgNnSc4d1	významné
společenské	společenský	k2eAgNnSc4d1	společenské
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
řady	řada	k1gFnSc2	řada
národně	národně	k6eAd1	národně
politických	politický	k2eAgFnPc2d1	politická
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1868	[number]	k4	1868
účastnil	účastnit	k5eAaImAgMnS	účastnit
národní	národní	k2eAgFnPc4d1	národní
pouti	pouť	k1gFnPc4	pouť
do	do	k7c2	do
Kostnice	Kostnice	k1gFnSc2	Kostnice
nebo	nebo	k8xC	nebo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
pouti	pouť	k1gFnSc2	pouť
do	do	k7c2	do
Borové	borový	k2eAgFnSc2d1	Borová
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
odhalení	odhalení	k1gNnSc2	odhalení
Havlíčkovy	Havlíčkův	k2eAgFnSc2d1	Havlíčkova
pamětní	pamětní	k2eAgFnSc2d1	pamětní
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
byla	být	k5eAaImAgFnS	být
slavnost	slavnost	k1gFnSc1	slavnost
položení	položení	k1gNnSc2	položení
základních	základní	k2eAgInPc2d1	základní
kamenů	kámen	k1gInPc2	kámen
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poklepu	poklep	k1gInSc6	poklep
na	na	k7c4	na
kámen	kámen	k1gInSc4	kámen
pronesl	pronést	k5eAaPmAgMnS	pronést
Smetana	Smetana	k1gMnSc1	Smetana
později	pozdě	k6eAd2	pozdě
proslavené	proslavený	k2eAgNnSc4d1	proslavené
heslo	heslo	k1gNnSc4	heslo
"	"	kIx"	"
<g/>
V	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
život	život	k1gInSc4	život
Čechů	Čech	k1gMnPc2	Čech
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dokládající	dokládající	k2eAgInSc1d1	dokládající
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
význam	význam	k1gInSc1	význam
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
emancipace	emancipace	k1gFnSc2	emancipace
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
skladatelská	skladatelský	k2eAgFnSc1d1	skladatelská
činnost	činnost	k1gFnSc1	činnost
ani	ani	k8xC	ani
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kapelnického	kapelnický	k2eAgNnSc2d1	kapelnické
vytížení	vytížení	k1gNnSc2	vytížení
neustoupila	ustoupit	k5eNaPmAgFnS	ustoupit
stranou	stranou	k6eAd1	stranou
<g/>
.	.	kIx.	.
</s>
<s>
Soustředila	soustředit	k5eAaPmAgFnS	soustředit
se	se	k3xPyFc4	se
pochopitelně	pochopitelně	k6eAd1	pochopitelně
na	na	k7c4	na
operu	opera	k1gFnSc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
Prodané	prodaný	k2eAgFnSc6d1	prodaná
nevěstě	nevěsta	k1gFnSc6	nevěsta
začal	začít	k5eAaPmAgMnS	začít
Smetana	Smetana	k1gMnSc1	Smetana
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
Daliborovi	Dalibor	k1gMnSc6	Dalibor
<g/>
,	,	kIx,	,
tragické	tragický	k2eAgFnSc3d1	tragická
opeře	opera	k1gFnSc3	opera
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
na	na	k7c4	na
původně	původně	k6eAd1	původně
německé	německý	k2eAgNnSc4d1	německé
libreto	libreto	k1gNnSc4	libreto
Josefa	Josef	k1gMnSc2	Josef
Wenziga	Wenzig	k1gMnSc2	Wenzig
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
opera	opera	k1gFnSc1	opera
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
partituře	partitura	k1gFnSc6	partitura
hotova	hotov	k2eAgFnSc1d1	hotova
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
položení	položení	k1gNnSc2	položení
základních	základní	k2eAgInPc2d1	základní
kamenů	kámen	k1gInPc2	kámen
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
;	;	kIx,	;
předcházela	předcházet	k5eAaImAgFnS	předcházet
jí	jíst	k5eAaImIp3nS	jíst
rovněž	rovněž	k9	rovněž
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
Slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
předehra	předehra	k1gFnSc1	předehra
C	C	kA	C
dur	dur	k1gNnSc1	dur
a	a	k8xC	a
proverb	proverb	k1gInSc1	proverb
J.	J.	kA	J.
J.	J.	kA	J.
Kolára	Kolár	k1gMnSc2	Kolár
Věštba	věštba	k1gFnSc1	věštba
Libušina	Libušin	k2eAgFnSc1d1	Libušina
<g/>
.	.	kIx.	.
</s>
<s>
Premiérové	premiérový	k2eAgNnSc1d1	premiérové
přijetí	přijetí	k1gNnSc1	přijetí
bylo	být	k5eAaImAgNnS	být
příznivé	příznivý	k2eAgNnSc1d1	příznivé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
při	při	k7c6	při
první	první	k4xOgFnSc6	první
repríze	repríza	k1gFnSc6	repríza
se	se	k3xPyFc4	se
nedostávalo	dostávat	k5eNaImAgNnS	dostávat
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
kritika	kritika	k1gFnSc1	kritika
byla	být	k5eAaImAgFnS	být
i	i	k9	i
od	od	k7c2	od
Smetanových	Smetanových	k2eAgMnPc2d1	Smetanových
přívrženců	přívrženec	k1gMnPc2	přívrženec
spíše	spíše	k9	spíše
zdrženlivá	zdrženlivý	k2eAgFnSc1d1	zdrženlivá
(	(	kIx(	(
<g/>
J.	J.	kA	J.
L.	L.	kA	L.
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
odpůrci	odpůrce	k1gMnPc1	odpůrce
ji	on	k3xPp3gFnSc4	on
ve	v	k7c6	v
staročeském	staročeský	k2eAgInSc6d1	staročeský
tisku	tisk	k1gInSc6	tisk
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
jako	jako	k9	jako
nenárodní	národní	k2eNgFnSc4d1	nenárodní
a	a	k8xC	a
wagnerovskou	wagnerovský	k2eAgFnSc4d1	wagnerovská
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
revize	revize	k1gFnSc1	revize
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kritických	kritický	k2eAgFnPc2d1	kritická
připomínek	připomínka	k1gFnPc2	připomínka
provedená	provedený	k2eAgFnSc1d1	provedená
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
nezvýšila	zvýšit	k5eNaPmAgFnS	zvýšit
oblibu	obliba	k1gFnSc4	obliba
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
pro	pro	k7c4	pro
Smetanu	smetana	k1gFnSc4	smetana
byl	být	k5eAaImAgMnS	být
neúspěch	neúspěch	k1gInSc4	neúspěch
této	tento	k3xDgFnSc2	tento
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
si	se	k3xPyFc3	se
velice	velice	k6eAd1	velice
zakládal	zakládat	k5eAaImAgMnS	zakládat
<g/>
,	,	kIx,	,
doživotním	doživotní	k2eAgNnSc7d1	doživotní
zklamáním	zklamání	k1gNnSc7	zklamání
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
revizí	revize	k1gFnPc2	revize
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
a	a	k8xC	a
Dalibora	Dalibor	k1gMnSc2	Dalibor
začal	začít	k5eAaPmAgInS	začít
zabývat	zabývat	k5eAaImF	zabývat
Libuší	Libuše	k1gFnSc7	Libuše
<g/>
,	,	kIx,	,
slavnostní	slavnostní	k2eAgFnSc7d1	slavnostní
operou	opera	k1gFnSc7	opera
opět	opět	k6eAd1	opět
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
Wenziga	Wenzig	k1gMnSc2	Wenzig
a	a	k8xC	a
Špindlera	Špindler	k1gMnSc2	Špindler
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc4	libreto
obdržel	obdržet	k5eAaPmAgMnS	obdržet
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
kompozici	kompozice	k1gFnSc4	kompozice
začal	začít	k5eAaPmAgMnS	začít
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgMnS	dokončit
ji	on	k3xPp3gFnSc4	on
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
tableau	tableau	k6eAd1	tableau
neboli	neboli	k8xC	neboli
Smetanovými	Smetanův	k2eAgNnPc7d1	Smetanovo
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
hudebně	hudebně	k6eAd1	hudebně
dramatické	dramatický	k2eAgNnSc4d1	dramatické
uživotnění	uživotnění	k1gNnSc4	uživotnění
<g/>
"	"	kIx"	"
než	než	k8xS	než
standardní	standardní	k2eAgFnSc1d1	standardní
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
určena	určit	k5eAaPmNgFnS	určit
k	k	k7c3	k
zamýšlené	zamýšlený	k2eAgFnSc3d1	zamýšlená
korunovaci	korunovace	k1gFnSc3	korunovace
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
však	však	k9	však
po	po	k7c6	po
ztroskotání	ztroskotání	k1gNnSc6	ztroskotání
fundamentálních	fundamentální	k2eAgInPc2d1	fundamentální
článků	článek	k1gInPc2	článek
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
definitivně	definitivně	k6eAd1	definitivně
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
,	,	kIx,	,
určil	určit	k5eAaPmAgMnS	určit
ji	on	k3xPp3gFnSc4	on
Smetana	Smetana	k1gMnSc1	Smetana
pro	pro	k7c4	pro
otevření	otevření	k1gNnSc4	otevření
"	"	kIx"	"
<g/>
velkého	velký	k2eAgNnSc2d1	velké
národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
"	"	kIx"	"
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
uvedení	uvedení	k1gNnSc4	uvedení
odložil	odložit	k5eAaPmAgMnS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Libuši	Libuše	k1gFnSc6	Libuše
neustoupil	ustoupit	k5eNaPmAgMnS	ustoupit
Smetana	Smetana	k1gMnSc1	Smetana
svým	svůj	k3xOyFgMnPc3	svůj
kritikům	kritik	k1gMnPc3	kritik
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
"	"	kIx"	"
<g/>
wagnerismu	wagnerismus	k1gInSc6	wagnerismus
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
tato	tento	k3xDgFnSc1	tento
programově	programově	k6eAd1	programově
nacionální	nacionální	k2eAgFnSc1d1	nacionální
opera	opera	k1gFnSc1	opera
je	být	k5eAaImIp3nS	být
nejbližší	blízký	k2eAgFnSc4d3	nejbližší
koncepci	koncepce	k1gFnSc4	koncepce
wagnerovského	wagnerovský	k2eAgNnSc2d1	wagnerovské
hudebního	hudební	k2eAgNnSc2d1	hudební
dramatu	drama	k1gNnSc2	drama
a	a	k8xC	a
právě	právě	k9	právě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
její	její	k3xOp3gFnSc2	její
kompozice	kompozice	k1gFnSc2	kompozice
Smetana	Smetana	k1gMnSc1	Smetana
jel	jet	k5eAaImAgMnS	jet
několikrát	několikrát	k6eAd1	několikrát
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tam	tam	k6eAd1	tam
viděl	vidět	k5eAaImAgMnS	vidět
Wagnerovy	Wagnerův	k2eAgFnPc4d1	Wagnerova
Tristana	Tristan	k1gMnSc4	Tristan
a	a	k8xC	a
Isoldu	Isolda	k1gFnSc4	Isolda
<g/>
,	,	kIx,	,
Zlato	zlato	k1gNnSc1	zlato
Rýna	Rýn	k1gInSc2	Rýn
a	a	k8xC	a
Valkýru	valkýra	k1gFnSc4	valkýra
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
vážně	vážně	k6eAd1	vážně
vzal	vzít	k5eAaPmAgMnS	vzít
připomínky	připomínka	k1gFnPc4	připomínka
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
k	k	k7c3	k
deklamaci	deklamace	k1gFnSc3	deklamace
češtiny	čeština	k1gFnSc2	čeština
mj.	mj.	kA	mj.
ve	v	k7c6	v
Smetanových	Smetanových	k2eAgFnPc6d1	Smetanových
operách	opera	k1gFnPc6	opera
formulovala	formulovat	k5eAaImAgFnS	formulovat
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
<g/>
.	.	kIx.	.
</s>
<s>
Navázal	navázat	k5eAaPmAgMnS	navázat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
mladou	mladý	k2eAgFnSc7d1	mladá
básnířkou	básnířka	k1gFnSc7	básnířka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
již	již	k6eAd1	již
znal	znát	k5eAaImAgInS	znát
ze	z	k7c2	z
ženské	ženský	k2eAgFnSc2d1	ženská
sekce	sekce	k1gFnSc2	sekce
Hlaholu	hlahol	k1gInSc2	hlahol
i	i	k9	i
jako	jako	k9	jako
sestru	sestra	k1gFnSc4	sestra
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Pecha	Pech	k1gMnSc2	Pech
<g/>
,	,	kIx,	,
učitele	učitel	k1gMnSc2	učitel
na	na	k7c6	na
Smetanově	Smetanův	k2eAgInSc6d1	Smetanův
a	a	k8xC	a
Hellerově	Hellerův	k2eAgInSc6d1	Hellerův
ústavu	ústav	k1gInSc6	ústav
<g/>
,	,	kIx,	,
a	a	k8xC	a
přihlížel	přihlížet	k5eAaImAgInS	přihlížet
k	k	k7c3	k
jejím	její	k3xOp3gInPc3	její
názorům	názor	k1gInPc3	názor
již	již	k6eAd1	již
při	při	k7c6	při
kompozici	kompozice	k1gFnSc6	kompozice
Libuše	Libuše	k1gFnSc2	Libuše
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
získal	získat	k5eAaPmAgMnS	získat
dvě	dva	k4xCgNnPc4	dva
nová	nový	k2eAgNnPc4d1	nové
libreta	libreto	k1gNnPc4	libreto
<g/>
:	:	kIx,	:
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
Lumíra	Lumír	k1gMnSc2	Lumír
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
si	se	k3xPyFc3	se
však	však	k9	však
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
sebekriticky	sebekriticky	k6eAd1	sebekriticky
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
a	a	k8xC	a
Violu	Viola	k1gFnSc4	Viola
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc4	jejíž
libreto	libreto	k1gNnSc4	libreto
obdržel	obdržet	k5eAaPmAgMnS	obdržet
roku	rok	k1gInSc2	rok
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1871	[number]	k4	1871
a	a	k8xC	a
prozatím	prozatím	k6eAd1	prozatím
ji	on	k3xPp3gFnSc4	on
odložil	odložit	k5eAaPmAgInS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
výčitkami	výčitka	k1gFnPc7	výčitka
skladatelské	skladatelský	k2eAgFnSc2d1	skladatelská
nečinnosti	nečinnost	k1gFnSc2	nečinnost
(	(	kIx(	(
<g/>
že	že	k8xS	že
totiž	totiž	k9	totiž
za	za	k7c2	za
svého	svůj	k3xOyFgNnSc2	svůj
kapelnictví	kapelnictví	k1gNnSc2	kapelnictví
složil	složit	k5eAaPmAgMnS	složit
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
neúspěšnou	úspěšný	k2eNgFnSc4d1	neúspěšná
operu	opera	k1gFnSc4	opera
<g/>
)	)	kIx)	)
Smetana	Smetana	k1gMnSc1	Smetana
jednak	jednak	k8xC	jednak
nechal	nechat	k5eAaPmAgMnS	nechat
veřejně	veřejně	k6eAd1	veřejně
provést	provést	k5eAaPmF	provést
některé	některý	k3yIgInPc4	některý
úryvky	úryvek	k1gInPc4	úryvek
z	z	k7c2	z
Libuše	Libuše	k1gFnSc2	Libuše
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
zejména	zejména	k9	zejména
předehra	předehra	k1gFnSc1	předehra
pronikla	proniknout	k5eAaPmAgFnS	proniknout
i	i	k9	i
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
se	se	k3xPyFc4	se
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
dokončení	dokončení	k1gNnSc6	dokončení
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
náročné	náročný	k2eAgFnSc2d1	náročná
komické	komický	k2eAgFnSc2d1	komická
opery	opera	k1gFnSc2	opera
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
jednáních	jednání	k1gNnPc6	jednání
s	s	k7c7	s
mluvenými	mluvený	k2eAgInPc7d1	mluvený
dialogy	dialog	k1gInPc7	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Libreto	libreto	k1gNnSc4	libreto
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
dílu	dílo	k1gNnSc3	dílo
nazvanému	nazvaný	k2eAgInSc3d1	nazvaný
Dvě	dva	k4xCgFnPc1	dva
vdovy	vdova	k1gFnSc2	vdova
mu	on	k3xPp3gMnSc3	on
napsal	napsat	k5eAaBmAgMnS	napsat
Emanuel	Emanuel	k1gMnSc1	Emanuel
František	František	k1gMnSc1	František
Züngel	Züngel	k1gMnSc1	Züngel
podle	podle	k7c2	podle
francouzské	francouzský	k2eAgFnSc2d1	francouzská
salónní	salónní	k2eAgFnSc2d1	salónní
komedie	komedie	k1gFnSc2	komedie
Jeana-Pierra	Jeana-Pierra	k1gFnSc1	Jeana-Pierra
Mallefilla	Mallefilla	k1gFnSc1	Mallefilla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
německy	německy	k6eAd1	německy
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
a	a	k8xC	a
v	v	k7c6	v
Züngelově	Züngelův	k2eAgInSc6d1	Züngelův
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
pracoval	pracovat	k5eAaImAgMnS	pracovat
se	s	k7c7	s
zaujetím	zaujetí	k1gNnSc7	zaujetí
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1873	[number]	k4	1873
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
premiéra	premiéra	k1gFnSc1	premiéra
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kritika	kritika	k1gFnSc1	kritika
opět	opět	k6eAd1	opět
diskutovala	diskutovat	k5eAaImAgFnS	diskutovat
o	o	k7c6	o
"	"	kIx"	"
<g/>
wagnerismu	wagnerismus	k1gInSc6	wagnerismus
<g/>
"	"	kIx"	"
této	tento	k3xDgFnSc2	tento
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
po	po	k7c6	po
slibném	slibný	k2eAgInSc6d1	slibný
rozběhu	rozběh	k1gInSc6	rozběh
reprízy	repríza	k1gFnSc2	repríza
vázly	váznout	k5eAaImAgFnP	váznout
<g/>
.	.	kIx.	.
</s>
<s>
Práci	práce	k1gFnSc4	práce
na	na	k7c6	na
operách	opera	k1gFnPc6	opera
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
téměř	téměř	k6eAd1	téměř
veškerá	veškerý	k3xTgFnSc1	veškerý
další	další	k2eAgFnSc1d1	další
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
skladatelská	skladatelský	k2eAgFnSc1d1	skladatelská
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
pár	pár	k4xCyI	pár
menších	malý	k2eAgFnPc2d2	menší
příležitostných	příležitostný	k2eAgFnPc2d1	příležitostná
skladeb	skladba	k1gFnPc2	skladba
jako	jako	k8xC	jako
právě	právě	k6eAd1	právě
Slavnostní	slavnostní	k2eAgFnSc1d1	slavnostní
předehra	předehra	k1gFnSc1	předehra
k	k	k7c3	k
položení	položení	k1gNnSc3	položení
základních	základní	k2eAgInPc2d1	základní
kamenů	kámen	k1gInPc2	kámen
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
živým	živý	k2eAgInPc3d1	živý
obrazům	obraz	k1gInPc3	obraz
pro	pro	k7c4	pro
dobročinné	dobročinný	k2eAgFnPc4d1	dobročinná
slavnosti	slavnost	k1gFnPc4	slavnost
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
Rybář	Rybář	k1gMnSc1	Rybář
a	a	k8xC	a
Libušin	Libušin	k2eAgInSc1d1	Libušin
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
a	a	k8xC	a
sbory	sbor	k1gInPc4	sbor
Rolnická	rolnický	k2eAgFnSc1d1	rolnická
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
píseň	píseň	k1gFnSc1	píseň
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
spolek	spolek	k1gInSc4	spolek
Hlahol	hlahol	k1gInSc1	hlahol
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
působení	působení	k1gNnSc1	působení
v	v	k7c6	v
kapelnické	kapelnický	k2eAgFnSc6d1	kapelnická
funkci	funkce	k1gFnSc6	funkce
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
i	i	k9	i
samotná	samotný	k2eAgFnSc1d1	samotná
osoba	osoba	k1gFnSc1	osoba
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
předmětem	předmět	k1gInSc7	předmět
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
a	a	k8xC	a
vzrušených	vzrušený	k2eAgFnPc2d1	vzrušená
polemik	polemika	k1gFnPc2	polemika
vedených	vedený	k2eAgFnPc2d1	vedená
v	v	k7c6	v
denících	deník	k1gInPc6	deník
i	i	k8xC	i
odborném	odborný	k2eAgInSc6d1	odborný
tisku	tisk	k1gInSc6	tisk
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
"	"	kIx"	"
<g/>
boje	boj	k1gInPc1	boj
o	o	k7c4	o
Smetanu	smetana	k1gFnSc4	smetana
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
střet	střet	k1gInSc1	střet
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
již	již	k6eAd1	již
Smetanových	Smetanových	k2eAgFnSc7d1	Smetanových
aspirací	aspirace	k1gFnSc7	aspirace
na	na	k7c4	na
kapelnické	kapelnický	k2eAgNnSc4d1	kapelnické
místo	místo	k1gNnSc4	místo
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
Braniborů	Branibor	k1gMnPc2	Branibor
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejintenzivnější	intenzivní	k2eAgFnSc1d3	nejintenzivnější
fáze	fáze	k1gFnSc1	fáze
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
spory	spora	k1gFnPc1	spora
měly	mít	k5eAaImAgFnP	mít
několik	několik	k4yIc4	několik
prolínajících	prolínající	k2eAgFnPc2d1	prolínající
se	se	k3xPyFc4	se
rovin	rovina	k1gFnPc2	rovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
plánu	plán	k1gInSc6	plán
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
osobní	osobní	k2eAgFnPc1d1	osobní
animozity	animozita	k1gFnPc1	animozita
<g/>
:	:	kIx,	:
proti	proti	k7c3	proti
Smetanovi	Smetana	k1gMnSc3	Smetana
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
střetli	střetnout	k5eAaPmAgMnP	střetnout
v	v	k7c6	v
profesionálním	profesionální	k2eAgInSc6d1	profesionální
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Patřil	patřit	k5eAaImAgMnS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
kapelník	kapelník	k1gMnSc1	kapelník
J.	J.	kA	J.
N.	N.	kA	N.
Maýr	Maýr	k1gMnSc1	Maýr
<g/>
,	,	kIx,	,
sesazený	sesazený	k2eAgMnSc1d1	sesazený
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
neúčastnil	účastnit	k5eNaImAgMnS	účastnit
sporů	spor	k1gInPc2	spor
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zůstával	zůstávat	k5eAaImAgInS	zůstávat
vážným	vážný	k2eAgMnSc7d1	vážný
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
Smetanovo	Smetanův	k2eAgNnSc4d1	Smetanovo
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
protivníci	protivník	k1gMnPc1	protivník
se	se	k3xPyFc4	se
rekrutovali	rekrutovat	k5eAaImAgMnP	rekrutovat
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Smetanových	Smetanových	k2eAgMnPc2d1	Smetanových
dřívějších	dřívější	k2eAgMnPc2d1	dřívější
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Pěvecký	pěvecký	k2eAgMnSc1d1	pěvecký
pedagog	pedagog	k1gMnSc1	pedagog
František	František	k1gMnSc1	František
Pivoda	Pivoda	k1gMnSc1	Pivoda
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
definitivně	definitivně	k6eAd1	definitivně
rozešel	rozejít	k5eAaPmAgMnS	rozejít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Smetana	Smetana	k1gMnSc1	Smetana
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
uměleckých	umělecký	k2eAgInPc2d1	umělecký
názorů	názor	k1gInPc2	názor
nepodpořil	podpořit	k5eNaPmAgInS	podpořit
Pivodův	Pivodův	k2eAgInSc1d1	Pivodův
plán	plán	k1gInSc1	plán
operní	operní	k2eAgFnSc2d1	operní
školy	škola	k1gFnSc2	škola
při	při	k7c6	při
Prozatímním	prozatímní	k2eAgNnSc6d1	prozatímní
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Nevraživost	nevraživost	k1gFnSc1	nevraživost
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
překladatele	překladatel	k1gMnSc2	překladatel
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Hanuše	Hanuš	k1gMnSc2	Hanuš
Böhma	Böhm	k1gMnSc2	Böhm
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
Smetanovým	Smetanův	k2eAgNnSc7d1	Smetanovo
odmítnutím	odmítnutí	k1gNnSc7	odmítnutí
komponovat	komponovat	k5eAaImF	komponovat
na	na	k7c4	na
Böhmovo	Böhmův	k2eAgNnSc4d1	Böhmovo
libreto	libreto	k1gNnSc4	libreto
Zakletý	zakletý	k2eAgMnSc1d1	zakletý
princ	princ	k1gMnSc1	princ
<g/>
;	;	kIx,	;
houslista	houslista	k1gMnSc1	houslista
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hřímalý	Hřímalý	k2eAgMnSc1d1	Hřímalý
nenacházel	nacházet	k5eNaImAgInS	nacházet
vedle	vedle	k7c2	vedle
Smetany	smetana	k1gFnSc2	smetana
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
jako	jako	k8xS	jako
kapelník	kapelník	k1gMnSc1	kapelník
i	i	k8xC	i
skladatel	skladatel	k1gMnSc1	skladatel
očekávané	očekávaný	k2eAgNnSc1d1	očekávané
uplatnění	uplatnění	k1gNnSc1	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
samotného	samotný	k2eAgMnSc2d1	samotný
Smetany	Smetana	k1gMnSc2	Smetana
-	-	kIx~	-
který	který	k3yIgMnSc1	který
veřejně	veřejně	k6eAd1	veřejně
reagoval	reagovat	k5eAaBmAgMnS	reagovat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
nediplomaticky	diplomaticky	k6eNd1	diplomaticky
-	-	kIx~	-
do	do	k7c2	do
polemik	polemika	k1gFnPc2	polemika
zapojoval	zapojovat	k5eAaImAgMnS	zapojovat
zejména	zejména	k9	zejména
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
Jan	Jan	k1gMnSc1	Jan
Ludevít	Ludevít	k1gMnSc1	Ludevít
Procházka	Procházka	k1gMnSc1	Procházka
a	a	k8xC	a
estetik	estetik	k1gMnSc1	estetik
Otakar	Otakar	k1gMnSc1	Otakar
Hostinský	hostinský	k1gMnSc1	hostinský
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
o	o	k7c4	o
Smetanovo	Smetanův	k2eAgNnSc4d1	Smetanovo
působení	působení	k1gNnSc4	působení
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
toho	ten	k3xDgMnSc2	ten
zařazovaly	zařazovat	k5eAaImAgInP	zařazovat
do	do	k7c2	do
prudkého	prudký	k2eAgInSc2d1	prudký
politického	politický	k2eAgInSc2d1	politický
boje	boj	k1gInSc2	boj
mezi	mezi	k7c7	mezi
frakcemi	frakce	k1gFnPc7	frakce
staročeskou	staročeský	k2eAgFnSc7d1	staročeská
a	a	k8xC	a
mladočeskou	mladočeský	k2eAgFnSc7d1	mladočeská
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
soupeřily	soupeřit	k5eAaImAgFnP	soupeřit
o	o	k7c4	o
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
Prozatímním	prozatímní	k2eAgNnSc6d1	prozatímní
divadle	divadlo	k1gNnSc6	divadlo
jakožto	jakožto	k8xS	jakožto
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
národních	národní	k2eAgFnPc2d1	národní
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
hlásil	hlásit	k5eAaImAgMnS	hlásit
k	k	k7c3	k
mladočeské	mladočeský	k2eAgFnSc3d1	mladočeská
skupině	skupina	k1gFnSc3	skupina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
přítelem	přítel	k1gMnSc7	přítel
byl	být	k5eAaImAgMnS	být
pozdější	pozdní	k2eAgMnSc1d2	pozdější
předseda	předseda	k1gMnSc1	předseda
mladočeské	mladočeský	k2eAgFnSc2d1	mladočeská
strany	strana	k1gFnSc2	strana
Karel	Karel	k1gMnSc1	Karel
Sladkovský	sladkovský	k2eAgMnSc1d1	sladkovský
a	a	k8xC	a
prosmetanovské	prosmetanovský	k2eAgInPc1d1	prosmetanovský
články	článek	k1gInPc1	článek
vycházely	vycházet	k5eAaImAgInP	vycházet
zejména	zejména	k9	zejména
v	v	k7c6	v
mladočeském	mladočeský	k2eAgInSc6d1	mladočeský
deníku	deník	k1gInSc6	deník
Národní	národní	k2eAgFnSc2d1	národní
listy	lista	k1gFnSc2	lista
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
odpůrci	odpůrce	k1gMnPc1	odpůrce
se	se	k3xPyFc4	se
soustřeďovali	soustřeďovat	k5eAaImAgMnP	soustřeďovat
ve	v	k7c6	v
staročeské	staročeský	k2eAgFnSc6d1	staročeská
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
vůdce	vůdce	k1gMnSc1	vůdce
František	František	k1gMnSc1	František
Ladislav	Ladislav	k1gMnSc1	Ladislav
Rieger	Rieger	k1gMnSc1	Rieger
byl	být	k5eAaImAgMnS	být
i	i	k9	i
v	v	k7c6	v
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
otázkách	otázka	k1gFnPc6	otázka
Smetanovým	Smetanův	k2eAgMnSc7d1	Smetanův
oponentem	oponent	k1gMnSc7	oponent
<g/>
;	;	kIx,	;
ze	z	k7c2	z
Smetanova	Smetanův	k2eAgMnSc2d1	Smetanův
příznivce	příznivec	k1gMnSc2	příznivec
v	v	k7c4	v
odpůrce	odpůrce	k1gMnPc4	odpůrce
se	se	k3xPyFc4	se
změnil	změnit	k5eAaPmAgMnS	změnit
i	i	k9	i
vydavatel	vydavatel	k1gMnSc1	vydavatel
Jan	Jan	k1gMnSc1	Jan
Stanislav	Stanislav	k1gMnSc1	Stanislav
Skrejšovský	Skrejšovský	k1gMnSc1	Skrejšovský
<g/>
.	.	kIx.	.
</s>
<s>
Protismetanovské	Protismetanovský	k2eAgInPc4d1	Protismetanovský
příspěvky	příspěvek	k1gInPc4	příspěvek
proto	proto	k8xC	proto
vycházely	vycházet	k5eAaImAgFnP	vycházet
ve	v	k7c6	v
staročeských	staročeský	k2eAgInPc6d1	staročeský
listech	list	k1gInPc6	list
Pokrok	pokrok	k1gInSc1	pokrok
<g/>
,	,	kIx,	,
Osvěta	osvěta	k1gFnSc1	osvěta
a	a	k8xC	a
německy	německy	k6eAd1	německy
psané	psaný	k2eAgFnSc2d1	psaná
Politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInPc1d1	hudební
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
<g/>
,	,	kIx,	,
patřily	patřit	k5eAaImAgFnP	patřit
původně	původně	k6eAd1	původně
mezi	mezi	k7c4	mezi
Smetanovy	Smetanův	k2eAgMnPc4d1	Smetanův
zastánce	zastánce	k1gMnPc4	zastánce
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
však	však	k9	však
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
převzal	převzít	k5eAaPmAgMnS	převzít
Skrejšovský	Skrejšovský	k1gMnSc1	Skrejšovský
<g/>
,	,	kIx,	,
šéfredaktor	šéfredaktor	k1gMnSc1	šéfredaktor
J.	J.	kA	J.
L.	L.	kA	L.
Procházka	Procházka	k1gMnSc1	Procházka
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgMnS	nahradit
jej	on	k3xPp3gInSc4	on
nejprve	nejprve	k6eAd1	nejprve
formálně	formálně	k6eAd1	formálně
skladatel	skladatel	k1gMnSc1	skladatel
Josef	Josef	k1gMnSc1	Josef
Richard	Richard	k1gMnSc1	Richard
Rozkošný	rozkošný	k2eAgMnSc1d1	rozkošný
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
přímo	přímo	k6eAd1	přímo
František	František	k1gMnSc1	František
Pivoda	Pivoda	k1gMnSc1	Pivoda
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
polemiky	polemika	k1gFnPc1	polemika
poté	poté	k6eAd1	poté
probíhaly	probíhat	k5eAaImAgFnP	probíhat
právě	právě	k9	právě
mezi	mezi	k7c7	mezi
Pivodovými	Pivodův	k2eAgInPc7d1	Pivodův
Hudebními	hudební	k2eAgInPc7d1	hudební
listy	list	k1gInPc7	list
a	a	k8xC	a
Procházkovým	Procházkův	k2eAgInSc7d1	Procházkův
nově	nově	k6eAd1	nově
založeným	založený	k2eAgInSc7d1	založený
časopisem	časopis	k1gInSc7	časopis
Dalibor	Dalibor	k1gMnSc1	Dalibor
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovi	Smetanův	k2eAgMnPc1d1	Smetanův
byly	být	k5eAaImAgFnP	být
činěny	činěn	k2eAgFnPc4d1	činěna
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
výčitky	výčitka	k1gFnPc4	výčitka
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
provozem	provoz	k1gInSc7	provoz
divadla	divadlo	k1gNnSc2	divadlo
(	(	kIx(	(
<g/>
prosazování	prosazování	k1gNnSc4	prosazování
vlastních	vlastní	k2eAgFnPc2d1	vlastní
oper	opera	k1gFnPc2	opera
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
jiných	jiný	k2eAgMnPc2d1	jiný
českých	český	k2eAgMnPc2d1	český
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
proněmecká	proněmecký	k2eAgFnSc1d1	proněmecká
orientace	orientace	k1gFnSc1	orientace
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
enormní	enormní	k2eAgFnSc1d1	enormní
<g/>
"	"	kIx"	"
gáže	gáže	k1gFnSc1	gáže
<g/>
,	,	kIx,	,
nedostatečně	dostatečně	k6eNd1	dostatečně
energické	energický	k2eAgNnSc1d1	energické
vedení	vedení	k1gNnSc1	vedení
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
favoritismus	favoritismus	k1gInSc1	favoritismus
mezi	mezi	k7c4	mezi
sólisty	sólista	k1gMnPc4	sólista
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1872	[number]	k4	1872
zveřejnilo	zveřejnit	k5eAaPmAgNnS	zveřejnit
68	[number]	k4	68
abonentů	abonent	k1gMnPc2	abonent
Politik	politik	k1gMnSc1	politik
otevřený	otevřený	k2eAgInSc1d1	otevřený
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
žádali	žádat	k5eAaImAgMnP	žádat
o	o	k7c4	o
nahrazení	nahrazení	k1gNnSc4	nahrazení
Smetany	smetana	k1gFnSc2	smetana
Maýrem	Maýrma	k1gFnPc2	Maýrma
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
našel	najít	k5eAaPmAgMnS	najít
podporu	podpora	k1gFnSc4	podpora
u	u	k7c2	u
divadelního	divadelní	k2eAgInSc2d1	divadelní
souboru	soubor	k1gInSc2	soubor
i	i	k8xC	i
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnPc1d1	kulturní
osobnosti	osobnost	k1gFnPc1	osobnost
sepsaly	sepsat	k5eAaPmAgFnP	sepsat
petici	petice	k1gFnSc4	petice
v	v	k7c4	v
jeho	on	k3xPp3gInSc4	on
prospěch	prospěch	k1gInSc4	prospěch
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
naopak	naopak	k6eAd1	naopak
zlepšení	zlepšení	k1gNnSc1	zlepšení
jeho	jeho	k3xOp3gFnSc2	jeho
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
jmenování	jmenování	k1gNnSc4	jmenování
uměleckým	umělecký	k2eAgMnSc7d1	umělecký
ředitelem	ředitel	k1gMnSc7	ředitel
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
od	od	k7c2	od
března	březen	k1gInSc2	březen
1874	[number]	k4	1874
v	v	k7c6	v
divadelním	divadelní	k2eAgNnSc6d1	divadelní
družstvu	družstvo	k1gNnSc6	družstvo
odehrával	odehrávat	k5eAaImAgInS	odehrávat
nový	nový	k2eAgInSc1d1	nový
usilovný	usilovný	k2eAgInSc1d1	usilovný
boj	boj	k1gInSc1	boj
o	o	k7c4	o
Smetanovo	Smetanův	k2eAgNnSc4d1	Smetanovo
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
má	mít	k5eAaImIp3nS	mít
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
estetický	estetický	k2eAgInSc1d1	estetický
rozměr	rozměr	k1gInSc1	rozměr
smetanovské	smetanovský	k2eAgFnSc2d1	smetanovská
debaty	debata	k1gFnSc2	debata
<g/>
.	.	kIx.	.
</s>
<s>
Otevření	otevření	k1gNnSc1	otevření
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
definování	definování	k1gNnSc4	definování
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
opery	opera	k1gFnSc2	opera
a	a	k8xC	a
české	český	k2eAgFnSc2d1	Česká
hudby	hudba	k1gFnSc2	hudba
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
obnovila	obnovit	k5eAaPmAgFnS	obnovit
debata	debata	k1gFnSc1	debata
o	o	k7c6	o
hudebních	hudební	k2eAgFnPc6d1	hudební
reformách	reforma	k1gFnPc6	reforma
Richarda	Richard	k1gMnSc2	Richard
Wagnera	Wagner	k1gMnSc2	Wagner
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
odmlčení	odmlčení	k1gNnSc6	odmlčení
právě	právě	k9	právě
uváděl	uvádět	k5eAaImAgMnS	uvádět
svá	svůj	k3xOyFgNnPc4	svůj
vrcholná	vrcholný	k2eAgNnPc4d1	vrcholné
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
pozadí	pozadí	k1gNnSc6	pozadí
se	se	k3xPyFc4	se
střetávala	střetávat	k5eAaImAgFnS	střetávat
koncepce	koncepce	k1gFnSc1	koncepce
reprezentovaná	reprezentovaný	k2eAgFnSc1d1	reprezentovaná
názory	názor	k1gInPc1	názor
F.	F.	kA	F.
L.	L.	kA	L.
Riegra	Riegra	k1gMnSc1	Riegra
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
stavět	stavět	k5eAaImF	stavět
národní	národní	k2eAgFnSc4d1	národní
hudbu	hudba	k1gFnSc4	hudba
na	na	k7c6	na
základech	základ	k1gInPc6	základ
národní	národní	k2eAgFnPc1d1	národní
písně	píseň	k1gFnPc1	píseň
a	a	k8xC	a
názory	názor	k1gInPc1	názor
F.	F.	kA	F.
Pivody	Pivoda	k1gFnSc2	Pivoda
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
kladl	klást	k5eAaImAgMnS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
primát	primát	k1gInSc4	primát
"	"	kIx"	"
<g/>
krásného	krásný	k2eAgInSc2d1	krásný
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
"	"	kIx"	"
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
<g/>
,	,	kIx,	,
s	s	k7c7	s
koncepcí	koncepce	k1gFnSc7	koncepce
formulovanou	formulovaný	k2eAgFnSc7d1	formulovaná
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
zejména	zejména	k9	zejména
O.	O.	kA	O.
Hostinským	hostinský	k1gMnSc7	hostinský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přijímal	přijímat	k5eAaImAgMnS	přijímat
Wagnerovu	Wagnerův	k2eAgFnSc4d1	Wagnerova
podobu	podoba	k1gFnSc4	podoba
hudebního	hudební	k2eAgNnSc2d1	hudební
dramatu	drama	k1gNnSc2	drama
jako	jako	k9	jako
nutné	nutný	k2eAgNnSc1d1	nutné
východisko	východisko	k1gNnSc1	východisko
a	a	k8xC	a
požadovaného	požadovaný	k2eAgInSc2d1	požadovaný
národního	národní	k2eAgInSc2d1	národní
charakteru	charakter	k1gInSc2	charakter
zpěvu	zpěv	k1gInSc2	zpěv
chtěl	chtít	k5eAaImAgMnS	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
především	především	k9	především
bezchybnou	bezchybný	k2eAgFnSc7d1	bezchybná
českou	český	k2eAgFnSc7d1	Česká
deklamací	deklamace	k1gFnSc7	deklamace
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovi	Smetana	k1gMnSc3	Smetana
byl	být	k5eAaImAgMnS	být
-	-	kIx~	-
pro	pro	k7c4	pro
jeho	on	k3xPp3gInSc4	on
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
stavbu	stavba	k1gFnSc4	stavba
opery	opera	k1gFnSc2	opera
i	i	k8xC	i
hojné	hojný	k2eAgNnSc1d1	hojné
využití	využití	k1gNnSc1	využití
orchestru	orchestr	k1gInSc2	orchestr
-	-	kIx~	-
vyčítán	vyčítán	k2eAgInSc1d1	vyčítán
"	"	kIx"	"
<g/>
wagnerismus	wagnerismus	k1gInSc1	wagnerismus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
poněmčování	poněmčování	k1gNnSc1	poněmčování
české	český	k2eAgFnSc2d1	Česká
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgFnPc7	tento
výčitkami	výčitka	k1gFnPc7	výčitka
byl	být	k5eAaImAgInS	být
stíhán	stíhat	k5eAaImNgMnS	stíhat
zejména	zejména	k9	zejména
Dalibor	Dalibor	k1gMnSc1	Dalibor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
byly	být	k5eAaImAgInP	být
základní	základní	k2eAgInPc1d1	základní
pojmy	pojem	k1gInPc1	pojem
diskuse	diskuse	k1gFnSc2	diskuse
vágní	vágní	k2eAgInPc1d1	vágní
a	a	k8xC	a
subjektivně	subjektivně	k6eAd1	subjektivně
cítěné	cítěný	k2eAgNnSc1d1	cítěné
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
národní	národní	k2eAgInSc1d1	národní
<g/>
"	"	kIx"	"
český	český	k2eAgInSc1d1	český
ráz	ráz	k1gInSc1	ráz
býval	bývat	k5eAaImAgInS	bývat
shledáván	shledávat	k5eAaImNgInS	shledávat
zejména	zejména	k9	zejména
u	u	k7c2	u
prostých	prostý	k2eAgFnPc2d1	prostá
<g/>
,	,	kIx,	,
zpěvných	zpěvný	k2eAgFnPc2d1	zpěvná
melodií	melodie	k1gFnPc2	melodie
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
hudby	hudba	k1gFnSc2	hudba
za	za	k7c4	za
"	"	kIx"	"
<g/>
cizí	cizí	k2eAgInSc4d1	cizí
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
wagnerovskou	wagnerovský	k2eAgFnSc4d1	wagnerovská
<g/>
"	"	kIx"	"
stačila	stačit	k5eAaBmAgFnS	stačit
hustější	hustý	k2eAgFnSc1d2	hustší
orchestrální	orchestrální	k2eAgFnSc1d1	orchestrální
sazba	sazba	k1gFnSc1	sazba
<g/>
,	,	kIx,	,
nezvyklé	zvyklý	k2eNgFnSc2d1	nezvyklá
harmonie	harmonie	k1gFnSc2	harmonie
a	a	k8xC	a
polyfonie	polyfonie	k1gFnSc2	polyfonie
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
sám	sám	k3xTgMnSc1	sám
své	svůj	k3xOyFgNnSc4	svůj
stanovisko	stanovisko	k1gNnSc4	stanovisko
formuloval	formulovat	k5eAaImAgMnS	formulovat
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
A.	A.	kA	A.
Čechovi	Čechův	k2eAgMnPc1d1	Čechův
z	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1882	[number]	k4	1882
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
nepadělám	padělat	k5eNaBmIp1nS	padělat
skladatele	skladatel	k1gMnSc4	skladatel
slovutného	slovutný	k2eAgMnSc2d1	slovutný
žádného	žádný	k1gMnSc2	žádný
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
jen	jen	k9	jen
se	se	k3xPyFc4	se
obdivuju	obdivovat	k5eAaImIp1nS	obdivovat
velikosti	velikost	k1gFnSc2	velikost
jejich	jejich	k3xOp3gMnPc2	jejich
<g/>
,	,	kIx,	,
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
přijímám	přijímat	k5eAaImIp1nS	přijímat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
uznám	uznat	k5eAaPmIp1nS	uznat
za	za	k7c4	za
dobré	dobrý	k2eAgFnPc4d1	dobrá
a	a	k8xC	a
krásné	krásný	k2eAgFnPc4d1	krásná
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k6eAd1	především
pravdivé	pravdivý	k2eAgFnPc1d1	pravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Vy	vy	k3xPp2nPc1	vy
to	ten	k3xDgNnSc4	ten
už	už	k6eAd1	už
dávno	dávno	k6eAd1	dávno
u	u	k7c2	u
mně	já	k3xPp1nSc6	já
znáte	znát	k5eAaImIp2nP	znát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiní	jiný	k2eAgMnPc1d1	jiný
to	ten	k3xDgNnSc4	ten
nevědí	vědět	k5eNaImIp3nP	vědět
<g/>
,	,	kIx,	,
a	a	k8xC	a
myslejí	myslet	k5eAaImIp3nP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
zavádím	zavádět	k5eAaImIp1nS	zavádět
Wagnerismus	wagnerismus	k1gInSc1	wagnerismus
<g/>
!!!	!!!	k?	!!!
Mám	mít	k5eAaImIp1nS	mít
dost	dost	k6eAd1	dost
co	co	k3yQnSc4	co
dělat	dělat	k5eAaImF	dělat
ze	z	k7c2	z
Smetanismem	Smetanismus	k1gInSc7	Smetanismus
<g/>
,	,	kIx,	,
jen	jen	k9	jen
když	když	k8xS	když
ten	ten	k3xDgInSc4	ten
sloh	sloh	k1gInSc4	sloh
je	být	k5eAaImIp3nS	být
poctivej	poctivej	k?	poctivej
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Přímá	přímý	k2eAgFnSc1d1	přímá
polemika	polemika	k1gFnSc1	polemika
utichla	utichnout	k5eAaPmAgFnS	utichnout
po	po	k7c6	po
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
ohluchnutí	ohluchnutí	k1gNnSc6	ohluchnutí
a	a	k8xC	a
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
<g/>
;	;	kIx,	;
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
zanikly	zaniknout	k5eAaPmAgInP	zaniknout
jak	jak	k6eAd1	jak
Hudební	hudební	k2eAgInPc1d1	hudební
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
tak	tak	k9	tak
Dalibor	Dalibor	k1gMnSc1	Dalibor
<g/>
.	.	kIx.	.
</s>
<s>
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
strana	strana	k1gFnSc1	strana
označovala	označovat	k5eAaImAgFnS	označovat
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
skladatelovo	skladatelův	k2eAgNnSc1d1	skladatelovo
onemocnění	onemocnění	k1gNnSc1	onemocnění
za	za	k7c4	za
následek	následek	k1gInSc4	následek
"	"	kIx"	"
<g/>
neustálého	neustálý	k2eAgNnSc2d1	neustálé
rozčilování	rozčilování	k1gNnSc2	rozčilování
<g/>
,	,	kIx,	,
jakých	jaký	k3yQgInPc2	jaký
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
způsobováno	způsobovat	k5eAaImNgNnS	způsobovat
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
jisté	jistý	k2eAgFnPc4d1	jistá
strany	strana	k1gFnSc2	strana
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Dalibor	Dalibor	k1gMnSc1	Dalibor
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
stylizovala	stylizovat	k5eAaImAgFnS	stylizovat
ho	on	k3xPp3gInSc4	on
tak	tak	k6eAd1	tak
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
mučedníka	mučedník	k1gMnSc2	mučedník
<g/>
.	.	kIx.	.
</s>
<s>
Hrubé	Hrubé	k2eAgInPc1d1	Hrubé
útoky	útok	k1gInPc1	útok
Smetanových	Smetanových	k2eAgMnPc2d1	Smetanových
oponentů	oponent	k1gMnPc2	oponent
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Hudebních	hudební	k2eAgInPc6d1	hudební
listech	list	k1gInPc6	list
vysmívali	vysmívat	k5eAaImAgMnP	vysmívat
jeho	jeho	k3xOp3gNnSc3	jeho
"	"	kIx"	"
<g/>
neslýchanému	slýchaný	k2eNgNnSc3d1	neslýchané
lazarství	lazarství	k1gNnSc3	lazarství
<g/>
"	"	kIx"	"
a	a	k8xC	a
penzi	penze	k1gFnSc4	penze
přiznanou	přiznaný	k2eAgFnSc4d1	přiznaná
mu	on	k3xPp3gInSc3	on
divadlem	divadlo	k1gNnSc7	divadlo
komentovali	komentovat	k5eAaBmAgMnP	komentovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Toť	Toť	k?	Toť
první	první	k4xOgFnSc1	první
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
české	český	k2eAgNnSc1d1	české
divadlo	divadlo	k1gNnSc1	divadlo
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
útulek	útulek	k1gInSc4	útulek
zaopatřovací	zaopatřovací	k2eAgInSc4d1	zaopatřovací
<g/>
,	,	kIx,	,
za	za	k7c4	za
invalidovnu	invalidovna	k1gFnSc4	invalidovna
<g/>
,	,	kIx,	,
za	za	k7c4	za
pathologický	pathologický	k2eAgInSc4d1	pathologický
ústav	ústav	k1gInSc4	ústav
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
veřejnost	veřejnost	k1gFnSc1	veřejnost
odsuzovala	odsuzovat	k5eAaImAgFnS	odsuzovat
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
osobnosti	osobnost	k1gFnPc1	osobnost
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
společensky	společensky	k6eAd1	společensky
izolovány	izolovat	k5eAaBmNgInP	izolovat
<g/>
.	.	kIx.	.
</s>
<s>
Estetická	estetický	k2eAgFnSc1d1	estetická
diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
"	"	kIx"	"
<g/>
wagnerismu	wagnerismus	k1gInSc6	wagnerismus
<g/>
"	"	kIx"	"
a	a	k8xC	a
české	český	k2eAgFnSc3d1	Česká
národní	národní	k2eAgFnSc3d1	národní
hudbě	hudba	k1gFnSc3	hudba
však	však	k9	však
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
Smetanův	Smetanův	k2eAgInSc1d1	Smetanův
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Náhle	náhle	k6eAd1	náhle
trpěl	trpět	k5eAaImAgMnS	trpět
úpornými	úporný	k2eAgFnPc7d1	úporná
kožními	kožní	k2eAgFnPc7d1	kožní
vyrážkami	vyrážka	k1gFnPc7	vyrážka
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgFnPc2d1	další
obtíží	obtíž	k1gFnPc2	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dobových	dobový	k2eAgInPc2d1	dobový
dokumentů	dokument	k1gInPc2	dokument
jej	on	k3xPp3gMnSc4	on
trápilo	trápit	k5eAaImAgNnS	trápit
"	"	kIx"	"
<g/>
pálení	pálení	k1gNnSc1	pálení
močového	močový	k2eAgInSc2d1	močový
měchýře	měchýř	k1gInSc2	měchýř
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
"	"	kIx"	"
<g/>
krčním	krční	k2eAgMnPc3d1	krční
katarům	katar	k1gMnPc3	katar
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
závratě	závrať	k1gFnPc4	závrať
a	a	k8xC	a
zvracel	zvracet	k5eAaImAgMnS	zvracet
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnPc4	nemoc
mu	on	k3xPp3gMnSc3	on
často	často	k6eAd1	často
bránily	bránit	k5eAaImAgInP	bránit
v	v	k7c6	v
plnění	plnění	k1gNnSc6	plnění
pracovních	pracovní	k2eAgFnPc2d1	pracovní
povinností	povinnost	k1gFnPc2	povinnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
vyčítala	vyčítat	k5eAaImAgFnS	vyčítat
kritika	kritika	k1gFnSc1	kritika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
za	za	k7c2	za
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Jabkenicích	Jabkenice	k1gFnPc6	Jabkenice
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
katarů	katar	k1gInPc2	katar
přešel	přejít	k5eAaPmAgInS	přejít
ve	v	k7c4	v
sluchové	sluchový	k2eAgFnPc4d1	sluchová
halucinace	halucinace	k1gFnPc4	halucinace
<g/>
,	,	kIx,	,
hučení	hučení	k1gNnSc1	hučení
a	a	k8xC	a
zaléhání	zaléhání	k1gNnSc1	zaléhání
zejména	zejména	k9	zejména
v	v	k7c6	v
pravém	pravý	k2eAgNnSc6d1	pravé
uchu	ucho	k1gNnSc6	ucho
a	a	k8xC	a
ztrátu	ztráta	k1gFnSc4	ztráta
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
léčbu	léčba	k1gFnSc4	léčba
u	u	k7c2	u
pražského	pražský	k2eAgMnSc2d1	pražský
profesora	profesor	k1gMnSc2	profesor
Emanuela	Emanuel	k1gMnSc2	Emanuel
Zaufala	Zaufala	k1gMnSc2	Zaufala
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
léčení	léčení	k1gNnSc4	léčení
postupně	postupně	k6eAd1	postupně
ztratil	ztratit	k5eAaPmAgMnS	ztratit
sluch	sluch	k1gInSc4	sluch
v	v	k7c6	v
pravém	pravý	k2eAgNnSc6d1	pravé
uchu	ucho	k1gNnSc6	ucho
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
levé	levý	k2eAgNnSc4d1	levé
ucho	ucho	k1gNnSc4	ucho
plně	plně	k6eAd1	plně
ohluchnul	ohluchnout	k5eAaPmAgInS	ohluchnout
náhle	náhle	k6eAd1	náhle
k	k	k7c3	k
ránu	ráno	k1gNnSc3	ráno
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
choroby	choroba	k1gFnSc2	choroba
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
předmětem	předmět	k1gInSc7	předmět
odborných	odborný	k2eAgFnPc2d1	odborná
diskusí	diskuse	k1gFnPc2	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hlava	Hlava	k1gMnSc1	Hlava
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prováděl	provádět	k5eAaImAgInS	provádět
pitvu	pitva	k1gFnSc4	pitva
Smetanova	Smetanův	k2eAgNnSc2d1	Smetanovo
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
diagnostikoval	diagnostikovat	k5eAaBmAgInS	diagnostikovat
progresivní	progresivní	k2eAgFnSc4d1	progresivní
paralýzu	paralýza	k1gFnSc4	paralýza
<g/>
,	,	kIx,	,
následek	následek	k1gInSc4	následek
příjice	příjice	k1gFnSc2	příjice
(	(	kIx(	(
<g/>
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
názor	názor	k1gInSc1	názor
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
nemedicínské	medicínský	k2eNgFnSc6d1	nemedicínská
literatuře	literatura	k1gFnSc6	literatura
převládá	převládat	k5eAaImIp3nS	převládat
a	a	k8xC	a
zastával	zastávat	k5eAaImAgMnS	zastávat
jej	on	k3xPp3gNnSc4	on
např.	např.	kA	např.
profesor	profesor	k1gMnSc1	profesor
Ladislav	Ladislav	k1gMnSc1	Ladislav
Haškovec	Haškovec	k1gMnSc1	Haškovec
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Harald	Harald	k1gMnSc1	Harald
Feldmann	Feldmann	k1gMnSc1	Feldmann
<g/>
,	,	kIx,	,
ke	k	k7c3	k
stejnému	stejný	k2eAgInSc3d1	stejný
závěru	závěr	k1gInSc3	závěr
vedl	vést	k5eAaImAgInS	vést
výzkum	výzkum	k1gInSc1	výzkum
Smetanových	Smetanových	k2eAgInPc2d1	Smetanových
ostatků	ostatek	k1gInPc2	ostatek
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
provedl	provést	k5eAaPmAgInS	provést
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
tým	tým	k1gInSc1	tým
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
profesora	profesor	k1gMnSc2	profesor
Emanuela	Emanuel	k1gMnSc2	Emanuel
Vlčka	Vlček	k1gMnSc2	Vlček
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
argumentem	argument	k1gInSc7	argument
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hladina	hladina	k1gFnSc1	hladina
protilátek	protilátka	k1gFnPc2	protilátka
na	na	k7c6	na
syfilis	syfilis	k1gFnSc1	syfilis
v	v	k7c6	v
ostatcích	ostatek	k1gInPc6	ostatek
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nP	by
bez	bez	k7c2	bez
onemocnění	onemocnění	k1gNnPc2	onemocnění
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
diagnózu	diagnóza	k1gFnSc4	diagnóza
hájí	hájit	k5eAaImIp3nS	hájit
nejnověji	nově	k6eAd3	nově
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vacek	Vacek	k1gMnSc1	Vacek
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
jako	jako	k8xC	jako
alternativa	alternativa	k1gFnSc1	alternativa
navrhováno	navrhován	k2eAgNnSc1d1	navrhováno
kornatění	kornatění	k1gNnSc4	kornatění
mozkových	mozkový	k2eAgFnPc2d1	mozková
tepen	tepna	k1gFnPc2	tepna
<g/>
:	:	kIx,	:
tento	tento	k3xDgInSc4	tento
názor	názor	k1gInSc4	názor
zastávali	zastávat	k5eAaImAgMnP	zastávat
například	například	k6eAd1	například
profesor	profesor	k1gMnSc1	profesor
Antonín	Antonín	k1gMnSc1	Antonín
Heveroch	Heveroch	k1gMnSc1	Heveroch
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vondráček	Vondráček	k1gMnSc1	Vondráček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
a	a	k8xC	a
Milada	Milada	k1gFnSc1	Milada
Semotánovi	Semotánův	k2eAgMnPc1d1	Semotánův
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
a	a	k8xC	a
Jana	Jana	k1gFnSc1	Jana
Boříkovi	Boříkův	k2eAgMnPc1d1	Boříkův
<g/>
;	;	kIx,	;
profesor	profesor	k1gMnSc1	profesor
Ivan	Ivan	k1gMnSc1	Ivan
Lesný	lesný	k2eAgMnSc1d1	lesný
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
Pickově	Pickův	k2eAgFnSc6d1	Pickova
nemoci	nemoc	k1gFnSc6	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Přednosta	přednosta	k1gMnSc1	přednosta
Psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
kliniky	klinika	k1gFnSc2	klinika
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
LF	LF	kA	LF
UK	UK	kA	UK
a	a	k8xC	a
VFN	VFN	kA	VFN
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
profesor	profesor	k1gMnSc1	profesor
Jiří	Jiří	k1gMnSc1	Jiří
Raboch	Raboch	k1gMnSc1	Raboch
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
současné	současný	k2eAgFnSc6d1	současná
nedostupnosti	nedostupnost	k1gFnSc6	nedostupnost
důkazů	důkaz	k1gInPc2	důkaz
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
diagnózu	diagnóza	k1gFnSc4	diagnóza
jednoznačně	jednoznačně	k6eAd1	jednoznačně
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
<s>
Nejnověji	nově	k6eAd3	nově
docent	docent	k1gMnSc1	docent
Jiří	Jiří	k1gMnSc1	Jiří
Ramba	Ramba	k1gMnSc1	Ramba
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyšetřoval	vyšetřovat	k5eAaImAgMnS	vyšetřovat
Smetanovu	Smetanův	k2eAgFnSc4d1	Smetanova
lebku	lebka	k1gFnSc4	lebka
<g/>
,	,	kIx,	,
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
skladatelovu	skladatelův	k2eAgFnSc4d1	skladatelova
nemoc	nemoc	k1gFnSc4	nemoc
od	od	k7c2	od
chronického	chronický	k2eAgInSc2d1	chronický
infekčního	infekční	k2eAgInSc2d1	infekční
zánětu	zánět	k1gInSc2	zánět
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
v	v	k7c6	v
obličejové	obličejový	k2eAgFnSc6d1	obličejová
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
trpěl	trpět	k5eAaImAgInS	trpět
od	od	k7c2	od
úrazu	úraz	k1gInSc2	úraz
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostatcích	ostatek	k1gInPc6	ostatek
byly	být	k5eAaImAgFnP	být
známky	známka	k1gFnPc1	známka
vývojové	vývojový	k2eAgFnSc2d1	vývojová
anomálie	anomálie	k1gFnSc2	anomálie
způsobené	způsobený	k2eAgFnSc2d1	způsobená
v	v	k7c6	v
dětském	dětský	k2eAgInSc6d1	dětský
věku	věk	k1gInSc6	věk
zpomalením	zpomalení	k1gNnSc7	zpomalení
růstu	růst	k1gInSc2	růst
pravé	pravý	k2eAgFnSc2d1	pravá
poloviny	polovina	k1gFnSc2	polovina
obličeje	obličej	k1gInSc2	obličej
následkem	následkem	k7c2	následkem
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Odborná	odborný	k2eAgFnSc1d1	odborná
asistentka	asistentka	k1gFnSc1	asistentka
Alena	Alena	k1gFnSc1	Alena
Němečková	Němečková	k1gFnSc1	Němečková
vyšetřila	vyšetřit	k5eAaPmAgFnS	vyšetřit
ušní	ušní	k2eAgFnPc4d1	ušní
kůstky	kůstka	k1gFnPc4	kůstka
skladatelova	skladatelův	k2eAgNnSc2d1	skladatelovo
pravého	pravý	k2eAgNnSc2d1	pravé
ucha	ucho	k1gNnSc2	ucho
a	a	k8xC	a
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
prohlodány	prohlodán	k2eAgFnPc1d1	prohlodán
chodbičkami	chodbička	k1gFnPc7	chodbička
(	(	kIx(	(
<g/>
lakunami	lakuna	k1gFnPc7	lakuna
<g/>
)	)	kIx)	)
vyplněnými	vyplněná	k1gFnPc7	vyplněná
granulační	granulační	k2eAgFnSc7d1	granulační
tkání	tkáň	k1gFnSc7	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
toho	ten	k3xDgInSc2	ten
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
těch	ten	k3xDgNnPc6	ten
místech	místo	k1gNnPc6	místo
musel	muset	k5eAaImAgMnS	muset
probíhat	probíhat	k5eAaImF	probíhat
zánět	zánět	k1gInSc4	zánět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
chronického	chronický	k2eAgNnSc2d1	chronické
stadia	stadion	k1gNnSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Profesor	profesor	k1gMnSc1	profesor
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vondráček	Vondráček	k1gMnSc1	Vondráček
hovoří	hovořit	k5eAaImIp3nS	hovořit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobová	dobový	k2eAgFnSc1d1	dobová
diagnóza	diagnóza	k1gFnSc1	diagnóza
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
nemoci	nemoc	k1gFnSc2	nemoc
zněla	znět	k5eAaImAgFnS	znět
anoia	anoia	k1gFnSc1	anoia
(	(	kIx(	(
<g/>
pošetilost	pošetilost	k1gFnSc1	pošetilost
<g/>
,	,	kIx,	,
pomatenost	pomatenost	k1gFnSc1	pomatenost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
zcela	zcela	k6eAd1	zcela
nepochybně	pochybně	k6eNd1	pochybně
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
pokročilé	pokročilý	k2eAgFnSc2d1	pokročilá
arteriosklerózy	arterioskleróza	k1gFnSc2	arterioskleróza
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
těžká	těžký	k2eAgFnSc1d1	těžká
sklerotická	sklerotický	k2eAgFnSc1d1	sklerotická
psychóza-činnost	psychóza-činnost	k1gFnSc1	psychóza-činnost
mozková	mozkový	k2eAgFnSc1d1	mozková
zachována-závěr	zachovánaávěr	k1gInSc4	zachována-závěr
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
vzestupný	vzestupný	k2eAgInSc1d1	vzestupný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Arteriosklerosu	arteriosklerosa	k1gFnSc4	arteriosklerosa
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
i	i	k9	i
samotný	samotný	k2eAgInSc1d1	samotný
pitevní	pitevní	k2eAgInSc1d1	pitevní
nález	nález	k1gInSc1	nález
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Cévy	céva	k1gFnPc1	céva
na	na	k7c6	na
spodině	spodina	k1gFnSc6	spodina
mozku	mozek	k1gInSc2	mozek
jsou	být	k5eAaImIp3nP	být
atheromatosní	atheromatosní	k2eAgFnPc1d1	atheromatosní
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Smetana	Smetana	k1gMnSc1	Smetana
zprvu	zprvu	k6eAd1	zprvu
neztrácel	ztrácet	k5eNaImAgMnS	ztrácet
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
konečné	konečný	k2eAgNnSc4d1	konečné
uzdravení	uzdravení	k1gNnSc4	uzdravení
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
sám	sám	k3xTgInSc1	sám
neměl	mít	k5eNaImAgInS	mít
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
u	u	k7c2	u
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
odborníků	odborník	k1gMnPc2	odborník
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgMnS	přijmout
peníze	peníz	k1gInPc4	peníz
ze	z	k7c2	z
sbírky	sbírka	k1gFnSc2	sbírka
uspořádané	uspořádaný	k2eAgFnSc2d1	uspořádaná
paní	paní	k1gFnSc2	paní
Fröjdou	Fröjda	k1gFnSc7	Fröjda
Beneckovou	Benecková	k1gFnSc7	Benecková
v	v	k7c6	v
Göteborgu	Göteborg	k1gInSc6	Göteborg
(	(	kIx(	(
<g/>
1244	[number]	k4	1244
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
výtěžku	výtěžek	k1gInSc2	výtěžek
soukromého	soukromý	k2eAgInSc2d1	soukromý
koncertu	koncert	k1gInSc2	koncert
uspořádaného	uspořádaný	k2eAgInSc2d1	uspořádaný
jeho	jeho	k3xOp3gFnPc7	jeho
aristokratickými	aristokratický	k2eAgFnPc7d1	aristokratická
žačkami	žačka	k1gFnPc7	žačka
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
hraběnkou	hraběnka	k1gFnSc7	hraběnka
Kaunicovou	Kaunicový	k2eAgFnSc7d1	Kaunicový
(	(	kIx(	(
<g/>
1800	[number]	k4	1800
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
)	)	kIx)	)
a	a	k8xC	a
veřejného	veřejný	k2eAgInSc2d1	veřejný
koncertu	koncert	k1gInSc2	koncert
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
poprvé	poprvé	k6eAd1	poprvé
zazněla	zaznít	k5eAaPmAgFnS	zaznít
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
odejel	odejet	k5eAaPmAgMnS	odejet
do	do	k7c2	do
Würzburgu	Würzburg	k1gInSc2	Würzburg
k	k	k7c3	k
přednímu	přední	k2eAgMnSc3d1	přední
ušnímu	ušní	k2eAgMnSc3d1	ušní
specialistovi	specialista	k1gMnSc3	specialista
profesorovi	profesor	k1gMnSc3	profesor
Antonu	Anton	k1gMnSc3	Anton
von	von	k1gInSc4	von
Tröltschovi	Tröltsch	k1gMnSc6	Tröltsch
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
-	-	kIx~	-
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
k	k	k7c3	k
profesoru	profesor	k1gMnSc3	profesor
Adamu	Adam	k1gMnSc3	Adam
Politzerovi	Politzer	k1gMnSc3	Politzer
(	(	kIx(	(
<g/>
1835	[number]	k4	1835
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
bezvýsledně	bezvýsledně	k6eAd1	bezvýsledně
<g/>
:	:	kIx,	:
lékaři	lékař	k1gMnPc1	lékař
konstatovali	konstatovat	k5eAaBmAgMnP	konstatovat
nezvratnou	zvratný	k2eNgFnSc4d1	nezvratná
paralýzu	paralýza	k1gFnSc4	paralýza
sluchových	sluchový	k2eAgInPc2d1	sluchový
nervů	nerv	k1gInPc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1874	[number]	k4	1874
požádal	požádat	k5eAaPmAgMnS	požádat
Smetana	Smetana	k1gMnSc1	Smetana
správu	správa	k1gFnSc4	správa
divadla	divadlo	k1gNnSc2	divadlo
o	o	k7c6	o
zproštění	zproštění	k1gNnSc6	zproštění
povinností	povinnost	k1gFnPc2	povinnost
na	na	k7c4	na
neurčitou	určitý	k2eNgFnSc4d1	neurčitá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
as	as	k9	as
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc6	říjen
pak	pak	k6eAd1	pak
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
veškeré	veškerý	k3xTgFnPc4	veškerý
své	svůj	k3xOyFgFnPc4	svůj
funkce	funkce	k1gFnPc4	funkce
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
divadelním	divadelní	k2eAgNnSc6d1	divadelní
družstvu	družstvo	k1gNnSc6	družstvo
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
staročeské	staročeský	k2eAgNnSc1d1	staročeské
vedení	vedení	k1gNnSc1	vedení
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byl	být	k5eAaImAgMnS	být
ředitelem	ředitel	k1gMnSc7	ředitel
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
jmenován	jmenovat	k5eAaBmNgInS	jmenovat
J.	J.	kA	J.
N.	N.	kA	N.
Maýr	Maýr	k1gMnSc1	Maýr
<g/>
,	,	kIx,	,
prvním	první	k4xOgMnSc7	první
kapelníkem	kapelník	k1gMnSc7	kapelník
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Adolf	Adolf	k1gMnSc1	Adolf
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
aktivní	aktivní	k2eAgFnSc1d1	aktivní
hudebnická	hudebnický	k2eAgFnSc1d1	hudebnická
i	i	k8xC	i
pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
činnost	činnost	k1gFnSc1	činnost
při	při	k7c6	při
Smetanově	Smetanův	k2eAgInSc6d1	Smetanův
stavu	stav	k1gInSc6	stav
nepadala	padat	k5eNaImAgFnS	padat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
a	a	k8xC	a
tantiémy	tantiéma	k1gFnPc4	tantiéma
z	z	k7c2	z
oper	opera	k1gFnPc2	opera
i	i	k8xC	i
autorské	autorský	k2eAgInPc4d1	autorský
příjmy	příjem	k1gInPc4	příjem
z	z	k7c2	z
vydaných	vydaný	k2eAgFnPc2d1	vydaná
skladeb	skladba	k1gFnPc2	skladba
byly	být	k5eAaImAgInP	být
nepravidelné	pravidelný	k2eNgInPc1d1	nepravidelný
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
,	,	kIx,	,
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
se	se	k3xPyFc4	se
skladatel	skladatel	k1gMnSc1	skladatel
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
finanční	finanční	k2eAgFnSc6d1	finanční
tísni	tíseň	k1gFnSc6	tíseň
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgNnSc1d1	divadelní
družstvo	družstvo	k1gNnSc1	družstvo
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
čestně	čestně	k6eAd1	čestně
<g/>
,	,	kIx,	,
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
jej	on	k3xPp3gMnSc4	on
v	v	k7c4	v
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
jako	jako	k8xS	jako
divadelního	divadelní	k2eAgMnSc4d1	divadelní
komponistu	komponista	k1gMnSc4	komponista
a	a	k8xC	a
vyměřilo	vyměřit	k5eAaPmAgNnS	vyměřit
mu	on	k3xPp3gMnSc3	on
čestnou	čestný	k2eAgFnSc4d1	čestná
penzi	penze	k1gFnSc4	penze
1200	[number]	k4	1200
zlatých	zlatá	k1gFnPc2	zlatá
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
však	však	k9	však
Smetana	Smetana	k1gMnSc1	Smetana
musel	muset	k5eAaImAgMnS	muset
zříci	zříct	k5eAaPmF	zříct
příjmů	příjem	k1gInPc2	příjem
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
dosavadní	dosavadní	k2eAgFnPc4d1	dosavadní
čtyři	čtyři	k4xCgFnPc4	čtyři
opery	opera	k1gFnPc4	opera
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pozdější	pozdní	k2eAgFnSc2d2	pozdější
opery	opera	k1gFnSc2	opera
opět	opět	k6eAd1	opět
pobíral	pobírat	k5eAaImAgInS	pobírat
tantiémy	tantiéma	k1gFnPc4	tantiéma
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
velký	velký	k2eAgInSc1d1	velký
příjem	příjem	k1gInSc1	příjem
a	a	k8xC	a
Smetana	Smetana	k1gMnSc1	Smetana
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
neustálých	neustálý	k2eAgFnPc6d1	neustálá
finančních	finanční	k2eAgFnPc6d1	finanční
starostech	starost	k1gFnPc6	starost
a	a	k8xC	a
tahanicích	tahanice	k1gFnPc6	tahanice
s	s	k7c7	s
měnícím	měnící	k2eAgMnSc7d1	měnící
se	se	k3xPyFc4	se
vedením	vedení	k1gNnSc7	vedení
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgInPc1d1	vysoký
životní	životní	k2eAgInPc1d1	životní
náklady	náklad	k1gInPc1	náklad
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jej	on	k3xPp3gInSc2	on
donutily	donutit	k5eAaPmAgInP	donutit
pobývat	pobývat	k5eAaImF	pobývat
i	i	k9	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
v	v	k7c6	v
Jabkenicích	Jabkenice	k1gFnPc6	Jabkenice
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
dcery	dcera	k1gFnSc2	dcera
Žofie	Žofie	k1gFnSc2	Žofie
Schwarzové	Schwarzová	k1gFnSc2	Schwarzová
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	s	k7c7	s
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1876	[number]	k4	1876
odstěhoval	odstěhovat	k5eAaPmAgInS	odstěhovat
definitivně	definitivně	k6eAd1	definitivně
<g/>
.	.	kIx.	.
</s>
<s>
Prozatímní	prozatímní	k2eAgNnSc1d1	prozatímní
divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
finančními	finanční	k2eAgInPc7d1	finanční
problémy	problém	k1gInPc7	problém
i	i	k9	i
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
krizí	krize	k1gFnSc7	krize
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byly	být	k5eAaImAgFnP	být
činěny	činěn	k2eAgFnPc1d1	činěna
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
snížení	snížení	k1gNnSc4	snížení
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
penze	penze	k1gFnSc2	penze
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vedení	vedení	k1gNnSc3	vedení
navíc	navíc	k6eAd1	navíc
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
zadržovalo	zadržovat	k5eAaImAgNnS	zadržovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
skladatele	skladatel	k1gMnSc4	skladatel
téměř	téměř	k6eAd1	téměř
zruinovalo	zruinovat	k5eAaPmAgNnS	zruinovat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc3	jeho
finanční	finanční	k2eAgFnSc1d1	finanční
situace	situace	k1gFnSc1	situace
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
díky	díky	k7c3	díky
vzrůstající	vzrůstající	k2eAgFnSc3d1	vzrůstající
popularitě	popularita	k1gFnSc3	popularita
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
<g/>
;	;	kIx,	;
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1883	[number]	k4	1883
mu	on	k3xPp3gMnSc3	on
ředitel	ředitel	k1gMnSc1	ředitel
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
František	František	k1gMnSc1	František
Adolf	Adolf	k1gMnSc1	Adolf
Šubert	Šubert	k1gMnSc1	Šubert
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
penzi	penze	k1gFnSc4	penze
na	na	k7c4	na
1500	[number]	k4	1500
zlatých	zlatá	k1gFnPc2	zlatá
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Jabkenické	Jabkenický	k2eAgNnSc1d1	Jabkenický
ústraní	ústraní	k1gNnSc1	ústraní
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
izolací	izolace	k1gFnSc7	izolace
<g/>
,	,	kIx,	,
Smetana	Smetana	k1gMnSc1	Smetana
zde	zde	k6eAd1	zde
přijímal	přijímat	k5eAaImAgMnS	přijímat
návštěvy	návštěva	k1gFnPc4	návštěva
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgInS	vést
čilou	čilý	k2eAgFnSc4d1	čilá
korespondenci	korespondence	k1gFnSc4	korespondence
a	a	k8xC	a
podle	podle	k7c2	podle
finančních	finanční	k2eAgFnPc2d1	finanční
možností	možnost	k1gFnPc2	možnost
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Prahu	Praha	k1gFnSc4	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rád	rád	k6eAd1	rád
chodil	chodit	k5eAaImAgMnS	chodit
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
sledoval	sledovat	k5eAaImAgMnS	sledovat
zkoušky	zkouška	k1gFnPc4	zkouška
i	i	k8xC	i
představení	představení	k1gNnPc4	představení
svých	svůj	k3xOyFgFnPc2	svůj
oper	opera	k1gFnPc2	opera
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
opravoval	opravovat	k5eAaImAgInS	opravovat
dirigentova	dirigentův	k2eAgNnSc2d1	dirigentovo
tempa	tempo	k1gNnSc2	tempo
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
partiturách	partitura	k1gFnPc6	partitura
sledoval	sledovat	k5eAaImAgMnS	sledovat
českou	český	k2eAgFnSc4d1	Česká
hudební	hudební	k2eAgFnSc4d1	hudební
produkci	produkce	k1gFnSc4	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
pražským	pražský	k2eAgNnSc7d1	Pražské
prostředím	prostředí	k1gNnSc7	prostředí
mu	on	k3xPp3gMnSc3	on
zajišťoval	zajišťovat	k5eAaImAgMnS	zajišťovat
především	především	k6eAd1	především
tajemník	tajemník	k1gMnSc1	tajemník
pražského	pražský	k2eAgInSc2d1	pražský
Hlaholu	hlahol	k1gInSc2	hlahol
Josef	Josef	k1gMnSc1	Josef
Srb-Debrnov	Srb-Debrnovo	k1gNnPc2	Srb-Debrnovo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
jeho	jeho	k3xOp3gMnSc7	jeho
sekretářem	sekretář	k1gMnSc7	sekretář
<g/>
,	,	kIx,	,
s	s	k7c7	s
divadlem	divadlo	k1gNnSc7	divadlo
jednal	jednat	k5eAaImAgInS	jednat
často	často	k6eAd1	často
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kapelníka	kapelník	k1gMnSc2	kapelník
Adolfa	Adolf	k1gMnSc2	Adolf
Čecha	Čech	k1gMnSc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Ohluchnutí	ohluchnutí	k1gNnSc1	ohluchnutí
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
schopnosti	schopnost	k1gFnSc2	schopnost
skládat	skládat	k5eAaImF	skládat
hudbu	hudba	k1gFnSc4	hudba
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
ostatní	ostatní	k2eAgFnSc2d1	ostatní
činnosti	činnost	k1gFnSc2	činnost
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
nyní	nyní	k6eAd1	nyní
znemožněna	znemožnit	k5eAaPmNgFnS	znemožnit
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
první	první	k4xOgNnPc4	první
léta	léto	k1gNnPc4	léto
nemoci	nemoc	k1gFnSc2	nemoc
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gFnSc7	jeho
nejplodnější	plodný	k2eAgMnSc1d3	nejplodnější
jak	jak	k6eAd1	jak
objemem	objem	k1gInSc7	objem
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
žánrovou	žánrový	k2eAgFnSc7d1	žánrová
různorodostí	různorodost	k1gFnSc7	různorodost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
září	září	k1gNnSc6	září
1874	[number]	k4	1874
začal	začít	k5eAaPmAgMnS	začít
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
cyklu	cyklus	k1gInSc6	cyklus
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
oslavujících	oslavující	k2eAgFnPc2d1	oslavující
krásu	krása	k1gFnSc4	krása
i	i	k8xC	i
dějiny	dějiny	k1gFnPc4	dějiny
české	český	k2eAgFnSc2d1	Česká
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
myslil	myslit	k5eAaImAgMnS	myslit
od	od	k7c2	od
dokončení	dokončení	k1gNnSc2	dokončení
Libuše	Libuše	k1gFnSc2	Libuše
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
dokončený	dokončený	k2eAgInSc1d1	dokončený
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
v	v	k7c6	v
rychlém	rychlý	k2eAgInSc6d1	rychlý
sledu	sled	k1gInSc6	sled
pak	pak	k6eAd1	pak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
Vltava	Vltava	k1gFnSc1	Vltava
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1874	[number]	k4	1874
<g/>
)	)	kIx)	)
a	a	k8xC	a
Šárka	Šárka	k1gFnSc1	Šárka
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
léčení	léčení	k1gNnSc2	léčení
pak	pak	k6eAd1	pak
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
Z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
luhů	luh	k1gInPc2	luh
a	a	k8xC	a
hájů	háj	k1gInPc2	háj
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
cyklus	cyklus	k1gInSc4	cyklus
prozatím	prozatím	k6eAd1	prozatím
ukončen	ukončen	k2eAgInSc4d1	ukončen
<g/>
;	;	kIx,	;
až	až	k9	až
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Tábor	Tábor	k1gInSc1	Tábor
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
následujícího	následující	k2eAgInSc2d1	následující
Blaník	Blaník	k1gInSc1	Blaník
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
cyklus	cyklus	k1gInSc1	cyklus
nazván	nazván	k2eAgInSc1d1	nazván
Vlast	vlast	k1gFnSc4	vlast
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc4	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
Smetana	Smetana	k1gMnSc1	Smetana
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ke	k	k7c3	k
klavírní	klavírní	k2eAgFnSc3d1	klavírní
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
šesti	šest	k4xCc2	šest
lisztovských	lisztovský	k2eAgFnPc2d1	lisztovský
skladeb	skladba	k1gFnPc2	skladba
Rê	Rê	k1gFnSc2	Rê
(	(	kIx(	(
<g/>
Sny	sen	k1gInPc4	sen
<g/>
)	)	kIx)	)
složil	složit	k5eAaPmAgMnS	složit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1875	[number]	k4	1875
a	a	k8xC	a
podělil	podělit	k5eAaPmAgMnS	podělit
jimi	on	k3xPp3gMnPc7	on
z	z	k7c2	z
vděčnosti	vděčnost	k1gFnSc2	vděčnost
své	svůj	k3xOyFgFnPc4	svůj
bývalé	bývalý	k2eAgFnPc4d1	bývalá
šlechtické	šlechtický	k2eAgFnPc4d1	šlechtická
žačky	žačka	k1gFnPc4	žačka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jej	on	k3xPp3gMnSc4	on
navštěvovaly	navštěvovat	k5eAaImAgInP	navštěvovat
a	a	k8xC	a
podporovaly	podporovat	k5eAaImAgInP	podporovat
v	v	k7c6	v
obtížné	obtížný	k2eAgFnSc6d1	obtížná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInPc1d2	pozdější
dva	dva	k4xCgInPc1	dva
cykly	cyklus	k1gInPc1	cyklus
Českých	český	k2eAgInPc2d1	český
tanců	tanec	k1gInPc2	tanec
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
vědomě	vědomě	k6eAd1	vědomě
jako	jako	k8xS	jako
pendant	pendant	k1gInSc4	pendant
a	a	k8xC	a
současně	současně	k6eAd1	současně
protipól	protipól	k1gInSc1	protipól
Dvořákových	Dvořáková	k1gFnPc2	Dvořáková
Slovanských	slovanský	k2eAgInPc2d1	slovanský
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
Smetanův	Smetanův	k2eAgMnSc1d1	Smetanův
mladší	mladý	k2eAgMnSc1d2	mladší
současník	současník	k1gMnSc1	současník
razantně	razantně	k6eAd1	razantně
pronikl	proniknout	k5eAaPmAgMnS	proniknout
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Komorní	komorní	k1gMnSc1	komorní
i	i	k9	i
sólové	sólový	k2eAgFnSc3d1	sólová
vokální	vokální	k2eAgFnSc3d1	vokální
hudbě	hudba	k1gFnSc3	hudba
se	se	k3xPyFc4	se
Smetana	Smetana	k1gMnSc1	Smetana
dosud	dosud	k6eAd1	dosud
věnoval	věnovat	k5eAaPmAgMnS	věnovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
okrajově	okrajově	k6eAd1	okrajově
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
však	však	k9	však
napsal	napsat	k5eAaBmAgInS	napsat
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
e	e	k0	e
moll	moll	k1gNnSc6	moll
"	"	kIx"	"
<g/>
Z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
programní	programní	k2eAgFnSc4d1	programní
komorní	komorní	k2eAgFnSc4d1	komorní
skladbu	skladba	k1gFnSc4	skladba
zachycující	zachycující	k2eAgInSc4d1	zachycující
běh	běh	k1gInSc4	běh
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
přes	přes	k7c4	přes
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
první	první	k4xOgFnSc4	první
ženu	žena	k1gFnSc4	žena
až	až	k9	až
po	po	k7c6	po
ohluchnutí	ohluchnutí	k1gNnSc6	ohluchnutí
<g/>
;	;	kIx,	;
přijetí	přijetí	k1gNnSc3	přijetí
této	tento	k3xDgFnSc2	tento
skladby	skladba	k1gFnSc2	skladba
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
prostředí	prostředí	k1gNnSc6	prostředí
bylo	být	k5eAaImAgNnS	být
pomalé	pomalý	k2eAgNnSc1d1	pomalé
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
jako	jako	k8xC	jako
první	první	k4xOgNnSc1	první
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
dílo	dílo	k1gNnSc1	dílo
vůbec	vůbec	k9	vůbec
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
prosadila	prosadit	k5eAaPmAgFnS	prosadit
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
dvě	dva	k4xCgNnPc1	dva
dua	duo	k1gNnPc1	duo
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
Z	z	k7c2	z
domoviny	domovina	k1gFnSc2	domovina
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
jsou	být	k5eAaImIp3nP	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
intimním	intimní	k2eAgInSc7d1	intimní
protějškem	protějšek	k1gInSc7	protějšek
Mé	můj	k3xOp1gFnSc2	můj
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
pak	pak	k6eAd1	pak
Smetana	Smetana	k1gMnSc1	Smetana
napsal	napsat	k5eAaPmAgMnS	napsat
svůj	svůj	k3xOyFgInSc4	svůj
jediný	jediný	k2eAgInSc4d1	jediný
písňový	písňový	k2eAgInSc4d1	písňový
cyklus	cyklus	k1gInSc4	cyklus
<g/>
,	,	kIx,	,
intimně	intimně	k6eAd1	intimně
a	a	k8xC	a
autobiograficky	autobiograficky	k6eAd1	autobiograficky
laděné	laděný	k2eAgFnPc4d1	laděná
Večerní	večerní	k2eAgFnPc4d1	večerní
písně	píseň	k1gFnPc4	píseň
na	na	k7c4	na
slova	slovo	k1gNnPc4	slovo
svého	svůj	k1gMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
<g/>
,	,	kIx,	,
básníka	básník	k1gMnSc2	básník
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Hálka	hálka	k1gFnSc1	hálka
<g/>
,	,	kIx,	,
zemřelého	zemřelý	k2eAgInSc2d1	zemřelý
roku	rok	k1gInSc2	rok
1874	[number]	k4	1874
<g/>
.	.	kIx.	.
</s>
<s>
Složil	složit	k5eAaPmAgMnS	složit
také	také	k9	také
několik	několik	k4yIc4	několik
mužských	mužský	k2eAgInPc2d1	mužský
sborů	sbor	k1gInPc2	sbor
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
Josefa	Josef	k1gMnSc2	Josef
Srba-Debrnova	Srba-Debrnov	k1gInSc2	Srba-Debrnov
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
nejrozsáhlejší	rozsáhlý	k2eAgInSc1d3	nejrozsáhlejší
Píseň	píseň	k1gFnSc4	píseň
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
-	-	kIx~	-
opět	opět	k6eAd1	opět
na	na	k7c4	na
Hálkova	Hálkův	k2eAgNnPc4d1	Hálkovo
slova	slovo	k1gNnPc4	slovo
-	-	kIx~	-
a	a	k8xC	a
nejznámější	známý	k2eAgNnSc1d3	nejznámější
Věno	věno	k1gNnSc1	věno
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
tři	tři	k4xCgInPc4	tři
ženské	ženský	k2eAgInPc4d1	ženský
sbory	sbor	k1gInPc4	sbor
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
dávného	dávný	k2eAgMnSc2d1	dávný
přítele	přítel	k1gMnSc2	přítel
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Hellera	Heller	k1gMnSc2	Heller
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vokální	vokální	k2eAgFnSc3d1	vokální
tvorbě	tvorba	k1gFnSc3	tvorba
se	se	k3xPyFc4	se
po	po	k7c6	po
Smetana	Smetana	k1gMnSc1	Smetana
po	po	k7c6	po
určitých	určitý	k2eAgFnPc6d1	určitá
obavách	obava	k1gFnPc6	obava
vrátil	vrátit	k5eAaPmAgInS	vrátit
až	až	k6eAd1	až
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
roce	rok	k1gInSc6	rok
od	od	k7c2	od
ohluchnutí	ohluchnutí	k1gNnSc2	ohluchnutí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
jedinou	jediný	k2eAgFnSc7d1	jediná
libretistkou	libretistka	k1gFnSc7	libretistka
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
životní	životní	k2eAgFnSc6d1	životní
etapě	etapa	k1gFnSc6	etapa
byla	být	k5eAaImAgFnS	být
Eliška	Eliška	k1gFnSc1	Eliška
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
dramaticky	dramaticky	k6eAd1	dramaticky
nenáročná	náročný	k2eNgNnPc1d1	nenáročné
libreta	libreto	k1gNnPc1	libreto
s	s	k7c7	s
prostými	prostý	k2eAgInPc7d1	prostý
příběhy	příběh	k1gInPc7	příběh
nevyvolávala	vyvolávat	k5eNaImAgFnS	vyvolávat
u	u	k7c2	u
skladatelových	skladatelův	k2eAgMnPc2d1	skladatelův
přátel	přítel	k1gMnPc2	přítel
nadšení	nadšení	k1gNnSc2	nadšení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
psána	psát	k5eAaImNgFnS	psát
vybraným	vybraný	k2eAgMnPc3d1	vybraný
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
rytmizovaným	rytmizovaný	k2eAgInSc7d1	rytmizovaný
básnickým	básnický	k2eAgInSc7d1	básnický
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
Smetana	Smetana	k1gMnSc1	Smetana
řekl	říct	k5eAaPmAgMnS	říct
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
A	a	k8xC	a
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
srostl	srůst	k5eAaPmAgInS	srůst
s	s	k7c7	s
vašimi	váš	k3xOp2gInPc7	váš
verši	verš	k1gInPc7	verš
<g/>
,	,	kIx,	,
s	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
nich	on	k3xPp3gFnPc2	on
vždycky	vždycky	k6eAd1	vždycky
vyciťují	vyciťovat	k5eAaImIp3nP	vyciťovat
a	a	k8xC	a
která	který	k3yRgFnSc1	který
v	v	k7c6	v
žádných	žádný	k3yNgInPc6	žádný
jiných	jiný	k2eAgInPc6d1	jiný
verších	verš	k1gInPc6	verš
již	již	k6eAd1	již
pro	pro	k7c4	pro
mne	já	k3xPp1nSc4	já
nezní	znět	k5eNaImIp3nS	znět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Její	její	k3xOp3gFnSc1	její
"	"	kIx"	"
<g/>
komická	komický	k2eAgFnSc1d1	komická
<g/>
"	"	kIx"	"
libreta	libreto	k1gNnPc1	libreto
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
lyrická	lyrický	k2eAgFnSc1d1	lyrická
s	s	k7c7	s
nádechem	nádech	k1gInSc7	nádech
humoru	humor	k1gInSc2	humor
a	a	k8xC	a
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
děje	děj	k1gInSc2	děj
stojí	stát	k5eAaImIp3nS	stát
vždy	vždy	k6eAd1	vždy
pár	pár	k4xCyI	pár
nacházející	nacházející	k2eAgFnSc1d1	nacházející
k	k	k7c3	k
sobě	se	k3xPyFc3	se
cestu	cesta	k1gFnSc4	cesta
ne	ne	k9	ne
přes	přes	k7c4	přes
vnější	vnější	k2eAgFnPc4d1	vnější
překážky	překážka	k1gFnPc4	překážka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vnitřním	vnitřní	k2eAgNnSc7d1	vnitřní
zráním	zrání	k1gNnSc7	zrání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
skladateli	skladatel	k1gMnSc3	skladatel
přesně	přesně	k6eAd1	přesně
vyhovovalo	vyhovovat	k5eAaImAgNnS	vyhovovat
<g/>
;	;	kIx,	;
navíc	navíc	k6eAd1	navíc
Krásnohorská	krásnohorský	k2eAgFnSc1d1	Krásnohorská
odmítala	odmítat	k5eAaImAgFnS	odmítat
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
odměnu	odměna	k1gFnSc4	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
ztrátou	ztráta	k1gFnSc7	ztráta
sluchu	sluch	k1gInSc2	sluch
skicoval	skicovat	k5eAaImAgMnS	skicovat
jednak	jednak	k8xC	jednak
její	její	k3xOp3gFnSc4	její
Violu	Viola	k1gFnSc4	Viola
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
Hubičku	hubička	k1gFnSc4	hubička
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1875	[number]	k4	1875
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgMnS	dokončit
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1876	[number]	k4	1876
měla	mít	k5eAaImAgFnS	mít
nová	nový	k2eAgFnSc1d1	nová
opera	opera	k1gFnSc1	opera
-	-	kIx~	-
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
konečné	konečný	k2eAgNnSc1d1	konečné
vážnější	vážní	k2eAgNnSc1d2	vážnější
vyznění	vyznění	k1gNnSc1	vyznění
přimělo	přimět	k5eAaPmAgNnS	přimět
Smetanu	smetana	k1gFnSc4	smetana
změnit	změnit	k5eAaPmF	změnit
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
komická	komický	k2eAgFnSc1d1	komická
<g/>
"	"	kIx"	"
na	na	k7c4	na
"	"	kIx"	"
<g/>
prostonárodní	prostonárodní	k2eAgFnSc4d1	prostonárodní
<g/>
"	"	kIx"	"
-	-	kIx~	-
nadšeně	nadšeně	k6eAd1	nadšeně
přijatou	přijatý	k2eAgFnSc4d1	přijatá
premiéru	premiéra	k1gFnSc4	premiéra
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
úspěch	úspěch	k1gInSc4	úspěch
vděčila	vděčit	k5eAaImAgFnS	vděčit
zvědavosti	zvědavost	k1gFnSc3	zvědavost
veřejnosti	veřejnost	k1gFnSc3	veřejnost
na	na	k7c4	na
dílo	dílo	k1gNnSc4	dílo
hluchého	hluchý	k2eAgMnSc2d1	hluchý
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Hubička	hubička	k1gFnSc1	hubička
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
stala	stát	k5eAaPmAgFnS	stát
nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
operou	opera	k1gFnSc7	opera
po	po	k7c6	po
Prodané	prodaný	k2eAgFnSc6d1	prodaná
nevěstě	nevěsta	k1gFnSc6	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
je	být	k5eAaImIp3nS	být
však	však	k9	však
Hubička	hubička	k1gFnSc1	hubička
plně	plně	k6eAd1	plně
prokomponovaná	prokomponovaný	k2eAgFnSc1d1	prokomponovaná
a	a	k8xC	a
hudebně	hudebně	k6eAd1	hudebně
jednolitá	jednolitý	k2eAgFnSc1d1	jednolitá
<g/>
,	,	kIx,	,
prostému	prostý	k2eAgInSc3d1	prostý
příběhu	příběh	k1gInSc3	příběh
dodává	dodávat	k5eAaImIp3nS	dodávat
váhu	váha	k1gFnSc4	váha
jemné	jemný	k2eAgNnSc4d1	jemné
psychologické	psychologický	k2eAgNnSc4d1	psychologické
vystižení	vystižení	k1gNnSc4	vystižení
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
Smetana	Smetana	k1gMnSc1	Smetana
pracoval	pracovat	k5eAaImAgMnS	pracovat
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
verzi	verze	k1gFnSc6	verze
Dvou	dva	k4xCgFnPc2	dva
vdov	vdova	k1gFnPc2	vdova
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
podstatně	podstatně	k6eAd1	podstatně
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
a	a	k8xC	a
mluvené	mluvený	k2eAgInPc4d1	mluvený
dialogy	dialog	k1gInPc4	dialog
převedl	převést	k5eAaPmAgMnS	převést
na	na	k7c4	na
recitativy	recitativ	k1gInPc4	recitativ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
května	květen	k1gInSc2	květen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
opeře	opera	k1gFnSc6	opera
Tajemství	tajemství	k1gNnSc1	tajemství
(	(	kIx(	(
<g/>
premiéra	premiéra	k1gFnSc1	premiéra
v	v	k7c6	v
září	září	k1gNnSc6	září
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
nejdokonalejší	dokonalý	k2eAgNnSc1d3	nejdokonalejší
Krásnohorské	krásnohorský	k2eAgNnSc1d1	Krásnohorské
libreto	libreto	k1gNnSc1	libreto
a	a	k8xC	a
současně	současně	k6eAd1	současně
stavebně	stavebně	k6eAd1	stavebně
nejdokonalejší	dokonalý	k2eAgFnSc1d3	nejdokonalejší
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
opera	opera	k1gFnSc1	opera
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
propracovaných	propracovaný	k2eAgInPc2d1	propracovaný
ansámblů	ansámbl	k1gInPc2	ansámbl
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c4	na
variační	variační	k2eAgFnSc4d1	variační
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
pouhými	pouhý	k2eAgInPc7d1	pouhý
dvěma	dva	k4xCgInPc7	dva
základními	základní	k2eAgInPc7d1	základní
motivy	motiv	k1gInPc7	motiv
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgFnSc1	tento
opera	opera	k1gFnSc1	opera
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
velmi	velmi	k6eAd1	velmi
příznivě	příznivě	k6eAd1	příznivě
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
méně	málo	k6eAd2	málo
vřele	vřele	k6eAd1	vřele
než	než	k8xS	než
Hubička	hubička	k1gFnSc1	hubička
<g/>
.	.	kIx.	.
</s>
<s>
Kompozice	kompozice	k1gFnSc1	kompozice
však	však	k9	však
byla	být	k5eAaImAgFnS	být
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
tvořil	tvořit	k5eAaImAgInS	tvořit
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
pomaleji	pomale	k6eAd2	pomale
a	a	k8xC	a
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
námahou	námaha	k1gFnSc7	námaha
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
vlastního	vlastní	k2eAgNnSc2d1	vlastní
vyjádření	vyjádření	k1gNnSc2	vyjádření
"	"	kIx"	"
<g/>
...	...	k?	...
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
bohužel	bohužel	k9	bohužel
<g/>
,	,	kIx,	,
nemohu	moct	k5eNaImIp1nS	moct
tak	tak	k6eAd1	tak
pracovati	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
zdravý	zdravý	k2eAgMnSc1d1	zdravý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
moje	můj	k3xOp1gFnSc1	můj
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
hluchota	hluchota	k1gFnSc1	hluchota
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojené	spojený	k2eAgNnSc1d1	spojené
ustavičné	ustavičný	k2eAgNnSc1d1	ustavičné
hučení	hučení	k1gNnSc1	hučení
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
hlavě	hlava	k1gFnSc6	hlava
mně	já	k3xPp1nSc3	já
nedovolí	dovolit	k5eNaPmIp3nP	dovolit
než	než	k8xS	než
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
časových	časový	k2eAgInPc6d1	časový
odstavcích	odstavec	k1gInPc6	odstavec
pracovati	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Smetana	Smetana	k1gMnSc1	Smetana
stával	stávat	k5eAaImAgInS	stávat
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
veřejnost	veřejnost	k1gFnSc4	veřejnost
reprezentantem	reprezentant	k1gMnSc7	reprezentant
národní	národní	k2eAgFnSc2d1	národní
hudby	hudba	k1gFnSc2	hudba
<g/>
;	;	kIx,	;
počátkem	počátkem	k7c2	počátkem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
toto	tento	k3xDgNnSc4	tento
jeho	jeho	k3xOp3gNnSc4	jeho
postavení	postavení	k1gNnSc4	postavení
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
série	série	k1gFnSc1	série
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1880	[number]	k4	1880
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
na	na	k7c6	na
Žofíně	Žofín	k1gInSc6	Žofín
konal	konat	k5eAaImAgInS	konat
velký	velký	k2eAgInSc1d1	velký
koncert	koncert	k1gInSc1	koncert
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
jeho	jeho	k3xOp3gFnSc2	jeho
prvního	první	k4xOgNnSc2	první
veřejného	veřejný	k2eAgNnSc2d1	veřejné
vystoupení	vystoupení	k1gNnSc2	vystoupení
<g/>
:	:	kIx,	:
skladatel	skladatel	k1gMnSc1	skladatel
zde	zde	k6eAd1	zde
sám	sám	k3xTgMnSc1	sám
zahrál	zahrát	k5eAaPmAgMnS	zahrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
byly	být	k5eAaImAgFnP	být
zpívány	zpíván	k2eAgFnPc1d1	zpívána
Večerní	večerní	k2eAgFnPc1d1	večerní
písně	píseň	k1gFnPc1	píseň
a	a	k8xC	a
hrány	hrát	k5eAaImNgFnP	hrát
Tábor	Tábor	k1gInSc1	Tábor
a	a	k8xC	a
Blaník	Blaník	k1gInSc1	Blaník
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1880	[number]	k4	1880
byl	být	k5eAaImAgMnS	být
Smetana	Smetana	k1gMnSc1	Smetana
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
přítomen	přítomno	k1gNnPc2	přítomno
národní	národní	k2eAgFnSc2d1	národní
slavnosti	slavnost	k1gFnSc2	slavnost
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
odhalena	odhalen	k2eAgFnSc1d1	odhalena
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1881	[number]	k4	1881
bylo	být	k5eAaImAgNnS	být
prozatímně	prozatímně	k6eAd1	prozatímně
otevřeno	otevřen	k2eAgNnSc1d1	otevřeno
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Smetanovou	Smetanová	k1gFnSc4	Smetanová
Libuší	Libuše	k1gFnPc2	Libuše
(	(	kIx(	(
<g/>
vedení	vedení	k1gNnSc1	vedení
divadla	divadlo	k1gNnSc2	divadlo
vypsalo	vypsat	k5eAaPmAgNnS	vypsat
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
soutěž	soutěž	k1gFnSc1	soutěž
o	o	k7c4	o
novou	nový	k2eAgFnSc4d1	nová
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Libuše	Libuše	k1gFnSc1	Libuše
podle	podle	k7c2	podle
očekávání	očekávání	k1gNnSc2	očekávání
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
definitivním	definitivní	k2eAgNnSc7d1	definitivní
otevřením	otevření	k1gNnSc7	otevření
Národní	národní	k2eAgFnSc2d1	národní
divadlo	divadlo	k1gNnSc4	divadlo
vyhořelo	vyhořet	k5eAaPmAgNnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
sbírek	sbírka	k1gFnPc2	sbírka
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
obnovení	obnovení	k1gNnSc6	obnovení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
výtěžek	výtěžek	k1gInSc1	výtěžek
byl	být	k5eAaImAgInS	být
určen	určit	k5eAaPmNgInS	určit
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
hluchotu	hluchota	k1gFnSc4	hluchota
dirigoval	dirigovat	k5eAaImAgMnS	dirigovat
předehru	předehra	k1gFnSc4	předehra
k	k	k7c3	k
Libuši	Libuše	k1gFnSc3	Libuše
a	a	k8xC	a
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
i	i	k9	i
jako	jako	k9	jako
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1881	[number]	k4	1881
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
stého	stý	k4xOgNnSc2	stý
představení	představení	k1gNnSc2	představení
-	-	kIx~	-
jako	jako	k8xS	jako
první	první	k4xOgFnSc4	první
česká	český	k2eAgFnSc1d1	Česká
opera	opera	k1gFnSc1	opera
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
odstupem	odstup	k1gInSc7	odstup
před	před	k7c7	před
ostatními	ostatní	k2eAgFnPc7d1	ostatní
českými	český	k2eAgFnPc7d1	Česká
operami	opera	k1gFnPc7	opera
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
představení	představení	k1gNnSc1	představení
bylo	být	k5eAaImAgNnS	být
holdem	hold	k1gInSc7	hold
skladateli	skladatel	k1gMnPc7	skladatel
a	a	k8xC	a
"	"	kIx"	"
<g/>
na	na	k7c4	na
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
žádost	žádost	k1gFnSc4	žádost
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
konat	konat	k5eAaImF	konat
druhé	druhý	k4xOgFnSc2	druhý
"	"	kIx"	"
<g/>
sté	stý	k4xOgFnSc2	stý
<g/>
"	"	kIx"	"
představení	představení	k1gNnPc4	představení
<g/>
.	.	kIx.	.
</s>
<s>
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
posledním	poslední	k2eAgNnSc7d1	poslední
představením	představení	k1gNnSc7	představení
hraným	hraný	k2eAgNnSc7d1	hrané
v	v	k7c6	v
Prozatímním	prozatímní	k2eAgNnSc6d1	prozatímní
divadle	divadlo	k1gNnSc6	divadlo
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nadšeného	nadšený	k2eAgNnSc2d1	nadšené
přijetí	přijetí	k1gNnSc2	přijetí
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
také	také	k9	také
první	první	k4xOgNnSc4	první
souborné	souborný	k2eAgNnSc4d1	souborné
uvedení	uvedení	k1gNnSc4	uvedení
Mé	můj	k3xOp1gFnSc2	můj
vlasti	vlast	k1gFnSc2	vlast
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
dříve	dříve	k6eAd2	dříve
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
poslední	poslední	k2eAgFnSc1d1	poslední
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
opera	opera	k1gFnSc1	opera
Čertova	čertův	k2eAgFnSc1d1	Čertova
stěna	stěna	k1gFnSc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
postupující	postupující	k2eAgFnSc4d1	postupující
chorobu	choroba	k1gFnSc4	choroba
stála	stát	k5eAaImAgFnS	stát
její	její	k3xOp3gFnSc1	její
kompozice	kompozice	k1gFnSc1	kompozice
skladatele	skladatel	k1gMnSc2	skladatel
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
i	i	k9	i
fyzického	fyzický	k2eAgNnSc2d1	fyzické
a	a	k8xC	a
duševního	duševní	k2eAgNnSc2d1	duševní
úsilí	úsilí	k1gNnSc2	úsilí
než	než	k8xS	než
předchozí	předchozí	k2eAgNnPc4d1	předchozí
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Zásahy	zásah	k1gInPc1	zásah
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
přitom	přitom	k6eAd1	přitom
učinil	učinit	k5eAaPmAgInS	učinit
do	do	k7c2	do
libreta	libreto	k1gNnSc2	libreto
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
i	i	k9	i
k	k	k7c3	k
ochlazení	ochlazení	k1gNnSc3	ochlazení
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
Krásnohorskou	krásnohorský	k2eAgFnSc7d1	Krásnohorská
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
spíše	spíše	k9	spíše
byl	být	k5eAaImAgMnS	být
hořce	hořko	k6eAd1	hořko
zklamán	zklamat	k5eAaPmNgMnS	zklamat
neúspěchem	neúspěch	k1gInSc7	neúspěch
jejího	její	k3xOp3gNnSc2	její
nastudování	nastudování	k1gNnSc2	nastudování
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
měly	mít	k5eAaImAgInP	mít
podíl	podíl	k1gInSc4	podíl
i	i	k8xC	i
zanedbaná	zanedbaný	k2eAgFnSc1d1	zanedbaná
výprava	výprava	k1gFnSc1	výprava
<g/>
,	,	kIx,	,
nepromyšlená	promyšlený	k2eNgFnSc1d1	nepromyšlená
režie	režie	k1gFnSc1	režie
i	i	k8xC	i
nešťastné	šťastný	k2eNgNnSc1d1	nešťastné
obsazení	obsazení	k1gNnSc1	obsazení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
představeních	představení	k1gNnPc6	představení
byla	být	k5eAaImAgFnS	být
Čertova	čertův	k2eAgFnSc1d1	Čertova
stěna	stěna	k1gFnSc1	stěna
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
se	se	k3xPyFc4	se
skladatelův	skladatelův	k2eAgInSc1d1	skladatelův
stav	stav	k1gInSc1	stav
vážně	vážně	k6eAd1	vážně
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
<g/>
.	.	kIx.	.
</s>
<s>
Obavy	obava	k1gFnPc1	obava
ze	z	k7c2	z
šílenství	šílenství	k1gNnSc2	šílenství
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
zmiňoval	zmiňovat	k5eAaImAgMnS	zmiňovat
již	již	k6eAd1	již
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Nerudovi	Neruda	k1gMnSc3	Neruda
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
<g/>
,	,	kIx,	,
nabývaly	nabývat	k5eAaImAgFnP	nabývat
reálné	reálný	k2eAgFnPc1d1	reálná
podoby	podoba	k1gFnPc1	podoba
<g/>
:	:	kIx,	:
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
nastal	nastat	k5eAaPmAgInS	nastat
první	první	k4xOgInSc1	první
záchvat	záchvat	k1gInSc1	záchvat
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
ztratil	ztratit	k5eAaPmAgInS	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
mluvit	mluvit	k5eAaImF	mluvit
i	i	k8xC	i
vybavovat	vybavovat	k5eAaImF	vybavovat
si	se	k3xPyFc3	se
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
stavy	stav	k1gInPc1	stav
se	se	k3xPyFc4	se
vracely	vracet	k5eAaImAgInP	vracet
<g/>
,	,	kIx,	,
množily	množit	k5eAaImAgInP	množit
se	se	k3xPyFc4	se
i	i	k9	i
halucinace	halucinace	k1gFnSc1	halucinace
<g/>
:	:	kIx,	:
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1883	[number]	k4	1883
popisoval	popisovat	k5eAaImAgMnS	popisovat
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
na	na	k7c6	na
večeru	večer	k1gInSc6	večer
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
Otakara	Otakar	k1gMnSc2	Otakar
Hostinského	hostinský	k1gMnSc2	hostinský
<g/>
.	.	kIx.	.
</s>
<s>
Skladatelská	skladatelský	k2eAgFnSc1d1	skladatelská
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
mohl	moct	k5eAaImAgInS	moct
soustředit	soustředit	k5eAaPmF	soustředit
jen	jen	k9	jen
po	po	k7c4	po
krátké	krátký	k2eAgInPc4d1	krátký
okamžiky	okamžik	k1gInPc4	okamžik
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnPc4	jeho
poslední	poslední	k2eAgFnPc4d1	poslední
skladby	skladba	k1gFnPc4	skladba
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
zkratkovitou	zkratkovitý	k2eAgFnSc7d1	zkratkovitá
<g/>
,	,	kIx,	,
stěsnanou	stěsnaný	k2eAgFnSc7d1	stěsnaná
hudební	hudební	k2eAgFnSc7d1	hudební
mluvou	mluva	k1gFnSc7	mluva
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
harmonickými	harmonický	k2eAgFnPc7d1	harmonická
nezvyklostmi	nezvyklost	k1gFnPc7	nezvyklost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
vykládány	vykládat	k5eAaImNgFnP	vykládat
jako	jako	k8xS	jako
projev	projev	k1gInSc1	projev
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
však	však	k9	však
předznamenávají	předznamenávat	k5eAaImIp3nP	předznamenávat
hudební	hudební	k2eAgInPc1d1	hudební
styly	styl	k1gInPc1	styl
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
1882	[number]	k4	1882
až	až	k9	až
březen	březen	k1gInSc1	březen
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jeho	jeho	k3xOp3gNnSc1	jeho
druhý	druhý	k4xOgInSc1	druhý
smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
d	d	k?	d
moll	moll	k1gNnSc1	moll
<g/>
,	,	kIx,	,
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
napsal	napsat	k5eAaBmAgMnS	napsat
svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
sbor	sbor	k1gInSc4	sbor
Naše	náš	k3xOp1gFnSc1	náš
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
nový	nový	k2eAgInSc4d1	nový
cyklus	cyklus	k1gInSc4	cyklus
symfonických	symfonický	k2eAgFnPc2d1	symfonická
skladeb	skladba	k1gFnPc2	skladba
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Pražský	pražský	k2eAgInSc4d1	pražský
karneval	karneval	k1gInSc4	karneval
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
dokončil	dokončit	k5eAaPmAgMnS	dokončit
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
jen	jen	k6eAd1	jen
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
<g/>
:	:	kIx,	:
Introdukci	introdukce	k1gFnSc4	introdukce
s	s	k7c7	s
rejem	rej	k1gInSc7	rej
masek	maska	k1gFnPc2	maska
a	a	k8xC	a
zahajovací	zahajovací	k2eAgFnSc4d1	zahajovací
polonézu	polonéza	k1gFnSc4	polonéza
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1883	[number]	k4	1883
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
odložené	odložený	k2eAgFnSc6d1	odložená
Viole	Viola	k1gFnSc6	Viola
<g/>
,	,	kIx,	,
stěží	stěží	k6eAd1	stěží
však	však	k9	však
zinstrumentoval	zinstrumentovat	k5eAaPmAgMnS	zinstrumentovat
starší	starý	k2eAgFnPc4d2	starší
skici	skica	k1gFnPc4	skica
a	a	k8xC	a
provedl	provést	k5eAaPmAgMnS	provést
některá	některý	k3yIgNnPc4	některý
doplnění	doplnění	k1gNnPc4	doplnění
<g/>
;	;	kIx,	;
Viola	Viola	k1gFnSc1	Viola
zůstala	zůstat	k5eAaPmAgFnS	zůstat
torzem	torzo	k1gNnSc7	torzo
a	a	k8xC	a
poslední	poslední	k2eAgInPc1d1	poslední
listy	list	k1gInPc1	list
partitury	partitura	k1gFnSc2	partitura
svědčí	svědčit	k5eAaImIp3nP	svědčit
o	o	k7c6	o
rychlé	rychlý	k2eAgFnSc6d1	rychlá
progresi	progrese	k1gFnSc6	progrese
skladatelovy	skladatelův	k2eAgFnSc2d1	skladatelova
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Naposledy	naposledy	k6eAd1	naposledy
komponoval	komponovat	k5eAaImAgInS	komponovat
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1883	[number]	k4	1883
účastnil	účastnit	k5eAaImAgMnS	účastnit
druhého	druhý	k4xOgNnSc2	druhý
otevření	otevření	k1gNnSc3	otevření
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
svou	svůj	k3xOyFgFnSc7	svůj
Libuší	Libuše	k1gFnSc7	Libuše
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
povaha	povaha	k1gFnSc1	povaha
již	již	k6eAd1	již
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zjevně	zjevně	k6eAd1	zjevně
narušena	narušit	k5eAaPmNgFnS	narušit
<g/>
,	,	kIx,	,
poznamenána	poznamenán	k2eAgFnSc1d1	poznamenána
halucinacemi	halucinace	k1gFnPc7	halucinace
<g/>
,	,	kIx,	,
megalomanií	megalomanie	k1gFnSc7	megalomanie
a	a	k8xC	a
paranoiou	paranoia	k1gFnSc7	paranoia
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
písemné	písemný	k2eAgNnSc1d1	písemné
i	i	k8xC	i
ústní	ústní	k2eAgNnSc1d1	ústní
vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
se	se	k3xPyFc4	se
stávalo	stávat	k5eAaImAgNnS	stávat
nesrozumitelným	srozumitelný	k2eNgMnSc7d1	nesrozumitelný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
světlých	světlý	k2eAgFnPc2d1	světlá
chvílí	chvíle	k1gFnSc7	chvíle
ubývalo	ubývat	k5eAaImAgNnS	ubývat
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
zmizely	zmizet	k5eAaPmAgInP	zmizet
docela	docela	k6eAd1	docela
<g/>
;	;	kIx,	;
celonárodních	celonárodní	k2eAgFnPc2d1	celonárodní
oslav	oslava	k1gFnPc2	oslava
svých	svůj	k3xOyFgNnPc2	svůj
60	[number]	k4	60
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
si	se	k3xPyFc3	se
již	již	k6eAd1	již
nebyl	být	k5eNaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
záchvatech	záchvat	k1gInPc6	záchvat
zuřivosti	zuřivost	k1gFnSc2	zuřivost
stával	stávat	k5eAaImAgMnS	stávat
nezvladatelným	zvladatelný	k2eNgMnSc7d1	nezvladatelný
a	a	k8xC	a
často	často	k6eAd1	často
nebezpečným	bezpečný	k2eNgNnSc7d1	nebezpečné
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1884	[number]	k4	1884
převezen	převézt	k5eAaPmNgInS	převézt
do	do	k7c2	do
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
choromyslné	choromyslný	k1gMnPc4	choromyslný
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
v	v	k7c6	v
Kateřinské	kateřinský	k2eAgFnSc6d1	Kateřinská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bez	bez	k7c2	bez
nabytí	nabytí	k1gNnSc2	nabytí
čistého	čistý	k2eAgNnSc2d1	čisté
vědomí	vědomí	k1gNnSc2	vědomí
zemřel	zemřít	k5eAaPmAgInS	zemřít
v	v	k7c4	v
půl	půl	k1xP	půl
páté	pátá	k1gFnSc2	pátá
odpoledne	odpoledne	k6eAd1	odpoledne
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Sládek	Sládek	k1gMnSc1	Sládek
psal	psát	k5eAaImAgMnS	psát
Juliu	Julius	k1gMnSc3	Julius
Zeyerovi	Zeyer	k1gMnSc3	Zeyer
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
ta	ten	k3xDgFnSc1	ten
duše	duše	k1gFnSc1	duše
těžce	těžce	k6eAd1	těžce
tělo	tělo	k1gNnSc4	tělo
opouštěla	opouštět	k5eAaImAgFnS	opouštět
a	a	k8xC	a
že	že	k8xS	že
trpěl	trpět	k5eAaImAgMnS	trpět
mnoho	mnoho	k6eAd1	mnoho
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
pitvě	pitva	k1gFnSc6	pitva
oděno	oděn	k2eAgNnSc4d1	oděno
do	do	k7c2	do
čamary	čamara	k1gFnSc2	čamara
a	a	k8xC	a
převezeno	převezen	k2eAgNnSc1d1	převezeno
do	do	k7c2	do
Týnského	týnský	k2eAgInSc2d1	týnský
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
pořádala	pořádat	k5eAaImAgFnS	pořádat
ho	on	k3xPp3gNnSc4	on
Umělecká	umělecký	k2eAgFnSc1d1	umělecká
beseda	beseda	k1gFnSc1	beseda
a	a	k8xC	a
účastnila	účastnit	k5eAaImAgFnS	účastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
řada	řada	k1gFnSc1	řada
zástupců	zástupce	k1gMnPc2	zástupce
politického	politický	k2eAgMnSc2d1	politický
i	i	k8xC	i
uměleckého	umělecký	k2eAgInSc2d1	umělecký
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
zástupci	zástupce	k1gMnPc1	zástupce
různých	různý	k2eAgInPc2d1	různý
spolků	spolek	k1gInPc2	spolek
(	(	kIx(	(
<g/>
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
Hlahol	hlahol	k1gInSc1	hlahol
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
personál	personál	k1gInSc1	personál
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Průvod	průvod	k1gInSc1	průvod
vedl	vést	k5eAaImAgInS	vést
od	od	k7c2	od
Týnského	týnský	k2eAgInSc2d1	týnský
chrámu	chrám	k1gInSc2	chrám
k	k	k7c3	k
Národnímu	národní	k2eAgNnSc3d1	národní
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
,	,	kIx,	,
k	k	k7c3	k
sídlu	sídlo	k1gNnSc3	sídlo
Umělecké	umělecký	k2eAgFnSc2d1	umělecká
besedy	beseda	k1gFnSc2	beseda
na	na	k7c6	na
Jungmannově	Jungmannův	k2eAgFnSc6d1	Jungmannova
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c4	na
Vyšehradský	vyšehradský	k2eAgInSc4d1	vyšehradský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
skladatel	skladatel	k1gMnSc1	skladatel
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
samostatném	samostatný	k2eAgInSc6d1	samostatný
hrobě	hrob	k1gInSc6	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
pohřbu	pohřeb	k1gInSc2	pohřeb
pak	pak	k6eAd1	pak
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
již	již	k6eAd1	již
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
;	;	kIx,	;
jeviště	jeviště	k1gNnSc1	jeviště
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
potaženo	potažen	k2eAgNnSc1d1	potaženo
černým	černý	k2eAgNnSc7d1	černé
suknem	sukno	k1gNnSc7	sukno
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
skladatelské	skladatelský	k2eAgNnSc1d1	skladatelské
dílo	dílo	k1gNnSc1	dílo
lze	lze	k6eAd1	lze
stylově	stylově	k6eAd1	stylově
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
novoromantismu	novoromantismus	k1gInSc2	novoromantismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
klasicistní	klasicistní	k2eAgFnSc7d1	klasicistní
i	i	k8xC	i
raně	raně	k6eAd1	raně
romantickou	romantický	k2eAgFnSc7d1	romantická
hudbou	hudba	k1gFnSc7	hudba
běžnou	běžný	k2eAgFnSc7d1	běžná
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
Gluck	Gluck	k1gMnSc1	Gluck
<g/>
,	,	kIx,	,
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
Beethoven	Beethoven	k1gMnSc1	Beethoven
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
například	například	k6eAd1	například
Schumann	Schumann	k1gMnSc1	Schumann
<g/>
,	,	kIx,	,
Chopin	Chopin	k1gMnSc1	Chopin
nebo	nebo	k8xC	nebo
Berlioz	Berlioz	k1gMnSc1	Berlioz
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
soudobých	soudobý	k2eAgInPc2d1	soudobý
hudebních	hudební	k2eAgInPc2d1	hudební
proudů	proud	k1gInPc2	proud
měl	mít	k5eAaImAgInS	mít
nepochybně	pochybně	k6eNd1	pochybně
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
novoněmecké	novoněmecký	k2eAgFnSc3d1	novoněmecký
škole	škola	k1gFnSc3	škola
kolem	kolem	k7c2	kolem
Liszta	Liszto	k1gNnSc2	Liszto
a	a	k8xC	a
Wagnera	Wagner	k1gMnSc4	Wagner
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednoznačné	jednoznačný	k2eAgNnSc1d1	jednoznačné
zařazení	zařazení	k1gNnSc1	zařazení
do	do	k7c2	do
"	"	kIx"	"
<g/>
německého	německý	k2eAgInSc2d1	německý
kulturního	kulturní	k2eAgInSc2d1	kulturní
okruhu	okruh	k1gInSc2	okruh
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
datující	datující	k2eAgMnSc1d1	datující
se	se	k3xPyFc4	se
již	již	k6eAd1	již
od	od	k7c2	od
výkladů	výklad	k1gInPc2	výklad
Otakara	Otakar	k1gMnSc2	Otakar
Hostinského	hostinský	k1gMnSc2	hostinský
<g/>
,	,	kIx,	,
zastírá	zastírat	k5eAaImIp3nS	zastírat
jak	jak	k6eAd1	jak
Smetanovu	Smetanův	k2eAgFnSc4d1	Smetanova
inspiraci	inspirace	k1gFnSc4	inspirace
soudobou	soudobý	k2eAgFnSc7d1	soudobá
italskou	italský	k2eAgFnSc7d1	italská
<g/>
,	,	kIx,	,
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
nebo	nebo	k8xC	nebo
slovanskou	slovanský	k2eAgFnSc7d1	Slovanská
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnSc4	jeho
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
osobitost	osobitost	k1gFnSc4	osobitost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
ni	on	k3xPp3gFnSc4	on
přitom	přitom	k6eAd1	přitom
Smetana	Smetana	k1gMnSc1	Smetana
usiloval	usilovat	k5eAaImAgMnS	usilovat
a	a	k8xC	a
zakládal	zakládat	k5eAaImAgMnS	zakládat
si	se	k3xPyFc3	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
neváhal	váhat	k5eNaImAgMnS	váhat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
dílem	dílo	k1gNnSc7	dílo
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
"	"	kIx"	"
<g/>
smetanismu	smetanismus	k1gInSc6	smetanismus
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
smetanovském	smetanovský	k2eAgInSc6d1	smetanovský
stylu	styl	k1gInSc6	styl
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
novoněmecké	novoněmecký	k2eAgFnSc3d1	novoněmecký
škole	škola	k1gFnSc3	škola
se	se	k3xPyFc4	se
-	-	kIx~	-
při	při	k7c6	při
všem	všecek	k3xTgInSc6	všecek
obdivu	obdiv	k1gInSc6	obdiv
k	k	k7c3	k
Lisztovi	Liszt	k1gMnSc3	Liszt
a	a	k8xC	a
Wagnerovi	Wagner	k1gMnSc3	Wagner
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
náleží	náležet	k5eAaImIp3nS	náležet
jen	jen	k9	jen
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
nakolik	nakolik	k6eAd1	nakolik
"	"	kIx"	"
<g/>
hlásá	hlásat	k5eAaImIp3nS	hlásat
pokrok	pokrok	k1gInSc4	pokrok
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
...	...	k?	...
<g/>
]	]	kIx)	]
v	v	k7c6	v
ostatním	ostatní	k1gNnSc6	ostatní
[	[	kIx(	[
<g/>
patřím	patřit	k5eAaImIp1nS	patřit
<g/>
]	]	kIx)	]
sám	sám	k3xTgMnSc1	sám
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Smetana	Smetana	k1gMnSc1	Smetana
sám	sám	k3xTgMnSc1	sám
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
těžiště	těžiště	k1gNnSc4	těžiště
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
operním	operní	k2eAgMnSc6d1	operní
a	a	k8xC	a
symfonickém	symfonický	k2eAgMnSc6d1	symfonický
<g/>
.	.	kIx.	.
</s>
<s>
Značný	značný	k2eAgInSc4d1	značný
objem	objem	k1gInSc4	objem
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
též	též	k9	též
klavírní	klavírní	k2eAgFnSc1d1	klavírní
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
soustředěná	soustředěný	k2eAgFnSc1d1	soustředěná
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
etapách	etapa	k1gFnPc6	etapa
jeho	jeho	k3xOp3gFnSc2	jeho
skladatelské	skladatelský	k2eAgFnSc2d1	skladatelská
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Komorní	komorní	k2eAgFnSc1d1	komorní
hudba	hudba	k1gFnSc1	hudba
je	být	k5eAaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
ojedinělými	ojedinělý	k2eAgInPc7d1	ojedinělý
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
zásadními	zásadní	k2eAgNnPc7d1	zásadní
díly	dílo	k1gNnPc7	dílo
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
okrajové	okrajový	k2eAgNnSc4d1	okrajové
postavení	postavení	k1gNnSc4	postavení
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
sbory	sbor	k1gInPc1	sbor
a	a	k8xC	a
písně	píseň	k1gFnPc1	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Sólové	sólový	k2eAgFnPc1d1	sólová
či	či	k8xC	či
koncertní	koncertní	k2eAgFnPc1d1	koncertní
skladby	skladba	k1gFnPc1	skladba
pro	pro	k7c4	pro
jiné	jiný	k2eAgInPc4d1	jiný
nástroje	nástroj	k1gInPc4	nástroj
než	než	k8xS	než
klavír	klavír	k1gInSc4	klavír
Smetana	Smetana	k1gMnSc1	Smetana
prakticky	prakticky	k6eAd1	prakticky
nepsal	psát	k5eNaImAgMnS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Prvořadý	prvořadý	k2eAgInSc1d1	prvořadý
význam	význam	k1gInSc1	význam
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Smetanova	Smetanův	k2eAgNnSc2d1	Smetanovo
díla	dílo	k1gNnSc2	dílo
má	mít	k5eAaImIp3nS	mít
jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
operní	operní	k2eAgFnSc1d1	operní
<g/>
.	.	kIx.	.
</s>
<s>
Souborem	soubor	k1gInSc7	soubor
svých	svůj	k3xOyFgInPc2	svůj
osmi	osm	k4xCc2	osm
dokončených	dokončený	k2eAgFnPc2d1	dokončená
oper	opera	k1gFnPc2	opera
(	(	kIx(	(
<g/>
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
zlomku	zlomek	k1gInSc2	zlomek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniklým	vzniklý	k2eAgMnSc7d1	vzniklý
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
,	,	kIx,	,
vědomě	vědomě	k6eAd1	vědomě
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c6	o
položení	položení	k1gNnSc6	položení
základu	základ	k1gInSc2	základ
českého	český	k2eAgInSc2d1	český
operního	operní	k2eAgInSc2d1	operní
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
nabídnout	nabídnout	k5eAaPmF	nabídnout
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
různých	různý	k2eAgFnPc2d1	různá
dobově	dobově	k6eAd1	dobově
aktuálních	aktuální	k2eAgFnPc2d1	aktuální
forem	forma	k1gFnPc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
svůj	svůj	k3xOyFgMnSc1	svůj
individuální	individuální	k2eAgMnSc1d1	individuální
<g/>
,	,	kIx,	,
národní	národní	k2eAgInSc1d1	národní
styl	styl	k1gInSc1	styl
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
osobitým	osobitý	k2eAgNnSc7d1	osobité
tvůrčím	tvůrčí	k2eAgNnSc7d1	tvůrčí
využitím	využití	k1gNnSc7	využití
různých	různý	k2eAgInPc2d1	různý
stylových	stylový	k2eAgInPc2d1	stylový
znaků	znak	k1gInPc2	znak
a	a	k8xC	a
technik	technika	k1gFnPc2	technika
soudobé	soudobý	k2eAgFnSc2d1	soudobá
evropské	evropský	k2eAgFnSc2d1	Evropská
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
si	se	k3xPyFc3	se
vážil	vážit	k5eAaImAgMnS	vážit
reforem	reforma	k1gFnPc2	reforma
Richarda	Richard	k1gMnSc4	Richard
Wagnera	Wagner	k1gMnSc4	Wagner
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnPc1	jeho
představy	představa	k1gFnPc1	představa
hudebního	hudební	k2eAgNnSc2d1	hudební
dramatu	drama	k1gNnSc2	drama
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
je	být	k5eAaImIp3nS	být
však	však	k9	však
nijak	nijak	k6eAd1	nijak
důsledně	důsledně	k6eAd1	důsledně
nesledoval	sledovat	k5eNaImAgMnS	sledovat
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
motivy	motiv	k1gInPc7	motiv
vychází	vycházet	k5eAaImIp3nS	vycházet
spíše	spíše	k9	spíše
ze	z	k7c2	z
staršího	starý	k2eAgInSc2d2	starší
německého	německý	k2eAgInSc2d1	německý
romantismu	romantismus	k1gInSc2	romantismus
a	a	k8xC	a
Liszta	Liszt	k1gInSc2	Liszt
než	než	k8xS	než
z	z	k7c2	z
Wagnera	Wagner	k1gMnSc2	Wagner
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
nevyhýbal	vyhýbat	k5eNaImAgMnS	vyhýbat
ansámblům	ansámbl	k1gInPc3	ansámbl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
ohledu	ohled	k1gInSc2	ohled
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
dramaturgickém	dramaturgický	k2eAgMnSc6d1	dramaturgický
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
často	často	k6eAd1	často
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
francouzské	francouzský	k2eAgInPc4d1	francouzský
vzory	vzor	k1gInPc4	vzor
a	a	k8xC	a
přes	přes	k7c4	přes
své	svůj	k3xOyFgFnPc4	svůj
výhrady	výhrada	k1gFnPc4	výhrada
k	k	k7c3	k
italskému	italský	k2eAgInSc3d1	italský
stylu	styl	k1gInSc3	styl
využíval	využívat	k5eAaImAgInS	využívat
nezřídka	nezřídka	k6eAd1	nezřídka
i	i	k9	i
belcantové	belcantový	k2eAgFnPc1d1	belcantová
pasáže	pasáž	k1gFnPc1	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
také	také	k9	také
nezavrhnul	zavrhnout	k5eNaPmAgInS	zavrhnout
koncept	koncept	k1gInSc4	koncept
uzavřených	uzavřený	k2eAgNnPc2d1	uzavřené
hudebních	hudební	k2eAgNnPc2d1	hudební
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
kladl	klást	k5eAaImAgInS	klást
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc1d2	veliký
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
jednotu	jednota	k1gFnSc4	jednota
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc4	sám
svůj	svůj	k3xOyFgInSc4	svůj
operní	operní	k2eAgInSc4d1	operní
styl	styl	k1gInSc4	styl
označil	označit	k5eAaPmAgMnS	označit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Smetanovský	smetanovský	k2eAgMnSc1d1	smetanovský
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
sloučení	sloučení	k1gNnSc4	sloučení
melodijích	melodij	k1gInPc6	melodij
<g/>
,	,	kIx,	,
i	i	k8xC	i
jednodušejších	jednodušý	k2eAgInPc2d2	jednodušý
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
svědomitě	svědomitě	k6eAd1	svědomitě
volenou	volený	k2eAgFnSc4d1	volená
harmonisaci	harmonisace	k1gFnSc4	harmonisace
a	a	k8xC	a
promyšleným	promyšlený	k2eAgInSc7d1	promyšlený
plánem	plán	k1gInSc7	plán
a	a	k8xC	a
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
<g/>
,	,	kIx,	,
souvyslosti	souvyslost	k1gFnSc6	souvyslost
a	a	k8xC	a
jednotě	jednota	k1gFnSc6	jednota
celé	celý	k2eAgFnSc2d1	celá
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jedná	jednat	k5eAaImIp3nS	jednat
velká	velký	k2eAgFnSc1d1	velká
synfonie	synfonie	k1gFnSc1	synfonie
<g/>
,	,	kIx,	,
arcit	arcit	k1gInSc1	arcit
zde	zde	k6eAd1	zde
-	-	kIx~	-
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
nejhlavnejší	hlavný	k2eAgNnSc1d3	hlavný
-	-	kIx~	-
s	s	k7c7	s
textem	text	k1gInSc7	text
spojená	spojený	k2eAgFnSc1d1	spojená
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
<s>
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
opery	opera	k1gFnSc2	opera
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
okruhů	okruh	k1gInPc2	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
vážné	vážný	k2eAgFnPc1d1	vážná
opery	opera	k1gFnPc1	opera
<g/>
:	:	kIx,	:
Braniboři	Branibor	k1gMnPc1	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kombinující	kombinující	k2eAgNnSc4d1	kombinující
základní	základní	k2eAgNnSc4d1	základní
schéma	schéma	k1gNnSc4	schéma
historické	historický	k2eAgFnSc2d1	historická
grand	grand	k1gMnSc1	grand
opera	opera	k1gFnSc1	opera
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
vážné	vážný	k2eAgFnSc2d1	vážná
opery	opera	k1gFnSc2	opera
italské	italský	k2eAgFnSc2d1	italská
a	a	k8xC	a
wagnerovského	wagnerovský	k2eAgNnSc2d1	wagnerovské
hudebního	hudební	k2eAgNnSc2d1	hudební
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Tvarově	tvarově	k6eAd1	tvarově
čistší	čistý	k2eAgMnSc1d2	čistší
je	být	k5eAaImIp3nS	být
Dalibor	Dalibor	k1gMnSc1	Dalibor
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tragická	tragický	k2eAgFnSc1d1	tragická
opera	opera	k1gFnSc1	opera
hlásící	hlásící	k2eAgFnSc1d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
"	"	kIx"	"
<g/>
novoněmecké	novoněmecký	k2eAgFnSc3d1	novoněmecký
<g/>
"	"	kIx"	"
škole	škola	k1gFnSc3	škola
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
motivickou	motivický	k2eAgFnSc4d1	motivická
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
posílením	posílení	k1gNnSc7	posílení
úlohy	úloha	k1gFnSc2	úloha
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Libuše	Libuše	k1gFnSc1	Libuše
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
-	-	kIx~	-
<g/>
72	[number]	k4	72
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
před	před	k7c7	před
Parsifalem	Parsifal	k1gInSc7	Parsifal
<g/>
)	)	kIx)	)
ojedinělým	ojedinělý	k2eAgInSc7d1	ojedinělý
mytologickým	mytologický	k2eAgInSc7d1	mytologický
obrazem	obraz	k1gInSc7	obraz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
nejvíce	nejvíce	k6eAd1	nejvíce
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
Wagnerovu	Wagnerův	k2eAgInSc3d1	Wagnerův
deklamačnímu	deklamační	k2eAgInSc3d1	deklamační
opernímu	operní	k2eAgInSc3d1	operní
jazyku	jazyk	k1gInSc3	jazyk
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
účinně	účinně	k6eAd1	účinně
využívá	využívat	k5eAaImIp3nS	využívat
velkooperní	velkooperní	k2eAgFnPc4d1	velkooperní
obřadnosti	obřadnost	k1gFnPc4	obřadnost
<g/>
.	.	kIx.	.
</s>
<s>
Komické	komický	k2eAgFnSc2d1	komická
opery	opera	k1gFnSc2	opera
Prodaná	Prodaná	k1gFnSc1	Prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
-	-	kIx~	-
<g/>
66	[number]	k4	66
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dvě	dva	k4xCgFnPc1	dva
vdovy	vdova	k1gFnPc1	vdova
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
)	)	kIx)	)
obě	dva	k4xCgNnPc4	dva
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
podob	podoba	k1gFnPc2	podoba
francouzské	francouzský	k2eAgFnSc2d1	francouzská
opéra	opéra	k6eAd1	opéra
comique	comique	k1gFnSc7	comique
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
i	i	k9	i
s	s	k7c7	s
mluvenými	mluvený	k2eAgInPc7d1	mluvený
dialogy	dialog	k1gInPc7	dialog
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
vesnického	vesnický	k2eAgInSc2d1	vesnický
žánrového	žánrový	k2eAgInSc2d1	žánrový
obrázku	obrázek	k1gInSc2	obrázek
a	a	k8xC	a
salónní	salónní	k2eAgFnSc2d1	salónní
konverzační	konverzační	k2eAgFnSc2d1	konverzační
komedie	komedie	k1gFnSc2	komedie
<g/>
,	,	kIx,	,
i	i	k8xC	i
zde	zde	k6eAd1	zde
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
podnětů	podnět	k1gInPc2	podnět
italské	italský	k2eAgFnPc1d1	italská
(	(	kIx(	(
<g/>
Rossini	Rossin	k2eAgMnPc1d1	Rossin
<g/>
)	)	kIx)	)
a	a	k8xC	a
německé	německý	k2eAgFnPc1d1	německá
(	(	kIx(	(
<g/>
Mozart	Mozart	k1gMnSc1	Mozart
<g/>
,	,	kIx,	,
Lortzing	Lortzing	k1gInSc1	Lortzing
<g/>
)	)	kIx)	)
komické	komický	k2eAgFnSc2d1	komická
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Trojice	trojice	k1gFnSc1	trojice
oper	opera	k1gFnPc2	opera
na	na	k7c4	na
libreta	libreto	k1gNnPc4	libreto
Elišky	Eliška	k1gFnSc2	Eliška
Krásnohorské	krásnohorský	k2eAgNnSc1d1	Krásnohorské
(	(	kIx(	(
<g/>
Hubička	hubička	k1gFnSc1	hubička
1875	[number]	k4	1875
<g/>
-	-	kIx~	-
<g/>
76	[number]	k4	76
<g/>
,	,	kIx,	,
Tajemství	tajemství	k1gNnSc1	tajemství
1877	[number]	k4	1877
<g/>
-	-	kIx~	-
<g/>
78	[number]	k4	78
<g/>
,	,	kIx,	,
Čertova	čertův	k2eAgFnSc1d1	Čertova
stěna	stěna	k1gFnSc1	stěna
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
méně	málo	k6eAd2	málo
sledují	sledovat	k5eAaImIp3nP	sledovat
tradiční	tradiční	k2eAgNnPc1d1	tradiční
operní	operní	k2eAgNnPc1d1	operní
schémata	schéma	k1gNnPc1	schéma
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
skladatelovou	skladatelův	k2eAgFnSc7d1	skladatelova
osobitou	osobitý	k2eAgFnSc7d1	osobitá
formou	forma	k1gFnSc7	forma
lyrické	lyrický	k2eAgFnSc2d1	lyrická
opery	opera	k1gFnSc2	opera
střídající	střídající	k2eAgNnPc4d1	střídající
komická	komický	k2eAgNnPc4d1	komické
a	a	k8xC	a
vážná	vážný	k2eAgNnPc4d1	vážné
místa	místo	k1gNnPc4	místo
<g/>
;	;	kIx,	;
uzavřená	uzavřený	k2eAgNnPc1d1	uzavřené
čísla	číslo	k1gNnPc1	číslo
jsou	být	k5eAaImIp3nP	být
podřízena	podřízen	k2eAgNnPc1d1	podřízeno
celkové	celkový	k2eAgFnSc6d1	celková
stavbě	stavba	k1gFnSc6	stavba
utvářené	utvářený	k2eAgFnSc6d1	utvářená
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
pomocí	pomocí	k7c2	pomocí
pamětných	pamětný	k2eAgInPc2d1	pamětný
motivů	motiv	k1gInPc2	motiv
a	a	k8xC	a
motivické	motivický	k2eAgFnSc2d1	motivická
transformace	transformace	k1gFnSc2	transformace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
vlasteneckým	vlastenecký	k2eAgInSc7d1	vlastenecký
programem	program	k1gInSc7	program
si	se	k3xPyFc3	se
Smetana	Smetana	k1gMnSc1	Smetana
volil	volit	k5eAaImAgMnS	volit
náměty	námět	k1gInPc4	námět
české	český	k2eAgInPc4d1	český
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
počeštěné	počeštěný	k2eAgNnSc4d1	počeštěné
<g/>
;	;	kIx,	;
jedinou	jediný	k2eAgFnSc4d1	jediná
operu	opera	k1gFnSc4	opera
na	na	k7c4	na
"	"	kIx"	"
<g/>
světový	světový	k2eAgInSc4d1	světový
námět	námět	k1gInSc4	námět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Violu	Viola	k1gFnSc4	Viola
<g/>
,	,	kIx,	,
charakteristicky	charakteristicky	k6eAd1	charakteristicky
několikrát	několikrát	k6eAd1	několikrát
odložil	odložit	k5eAaPmAgInS	odložit
<g/>
,	,	kIx,	,
až	až	k9	až
zůstala	zůstat	k5eAaPmAgFnS	zůstat
nedokončena	dokončen	k2eNgFnSc1d1	nedokončena
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
námětu	námět	k1gInSc2	námět
přitom	přitom	k6eAd1	přitom
zpravidla	zpravidla	k6eAd1	zpravidla
nevycházela	vycházet	k5eNaImAgFnS	vycházet
od	od	k7c2	od
něho	on	k3xPp3gMnSc2	on
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Dvou	dva	k4xCgFnPc2	dva
vdov	vdova	k1gFnPc2	vdova
a	a	k8xC	a
Violy	Viola	k1gFnSc2	Viola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
od	od	k7c2	od
libretistů	libretista	k1gMnPc2	libretista
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejichž	jejichž	k3xOyRp3gFnSc2	jejichž
práce	práce	k1gFnSc2	práce
ostatně	ostatně	k6eAd1	ostatně
Smetana	Smetana	k1gMnSc1	Smetana
zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
jen	jen	k9	jen
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
-	-	kIx~	-
nejvíce	hodně	k6eAd3	hodně
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Braniborů	Branibor	k1gMnPc2	Branibor
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
Čertovy	čertův	k2eAgFnPc4d1	Čertova
stěny	stěna	k1gFnPc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
řada	řada	k1gFnSc1	řada
rysů	rys	k1gInPc2	rys
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
jeho	jeho	k3xOp3gFnSc2	jeho
oper	opera	k1gFnPc2	opera
(	(	kIx(	(
<g/>
např.	např.	kA	např.
podíl	podíl	k1gInSc4	podíl
ansámblů	ansámbl	k1gInPc2	ansámbl
<g/>
,	,	kIx,	,
sborů	sbor	k1gInPc2	sbor
<g/>
,	,	kIx,	,
rozložení	rozložení	k1gNnSc1	rozložení
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
libretem	libreto	k1gNnSc7	libreto
spíše	spíše	k9	spíše
než	než	k8xS	než
skladatelovými	skladatelův	k2eAgInPc7d1	skladatelův
záměry	záměr	k1gInPc7	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
navíc	navíc	k6eAd1	navíc
psal	psát	k5eAaImAgMnS	psát
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
podmínky	podmínka	k1gFnPc4	podmínka
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gInSc2	on
orchestru	orchestr	k1gInSc2	orchestr
a	a	k8xC	a
sólistů	sólista	k1gMnPc2	sólista
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
psal	psát	k5eAaImAgMnS	psát
úlohy	úloha	k1gFnPc4	úloha
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
operách	opera	k1gFnPc6	opera
a	a	k8xC	a
pro	pro	k7c4	pro
něž	jenž	k3xRgMnPc4	jenž
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
neváhal	váhat	k5eNaImAgMnS	váhat
svou	svůj	k3xOyFgFnSc4	svůj
hudbu	hudba	k1gFnSc4	hudba
dodatečně	dodatečně	k6eAd1	dodatečně
doplnit	doplnit	k5eAaPmF	doplnit
nebo	nebo	k8xC	nebo
uzpůsobit	uzpůsobit	k5eAaPmF	uzpůsobit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
význam	význam	k1gInSc4	význam
oper	opera	k1gFnPc2	opera
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Smetanova	Smetanův	k2eAgNnSc2d1	Smetanovo
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc4	počet
a	a	k8xC	a
rozsah	rozsah	k1gInSc4	rozsah
dalších	další	k2eAgNnPc2d1	další
vokálních	vokální	k2eAgNnPc2d1	vokální
děl	dělo	k1gNnPc2	dělo
překvapivě	překvapivě	k6eAd1	překvapivě
malý	malý	k2eAgInSc1d1	malý
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
předchází	předcházet	k5eAaImIp3nS	předcházet
jeho	jeho	k3xOp3gFnSc1	jeho
prvním	první	k4xOgFnPc3	první
zpěvohrám	zpěvohra	k1gFnPc3	zpěvohra
<g/>
.	.	kIx.	.
</s>
<s>
Dominují	dominovat	k5eAaImIp3nP	dominovat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
sbory	sbor	k1gInPc7	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
psal	psát	k5eAaImAgMnS	psát
Smetana	Smetana	k1gMnSc1	Smetana
pro	pro	k7c4	pro
pražský	pražský	k2eAgInSc4d1	pražský
pěvecký	pěvecký	k2eAgInSc4d1	pěvecký
spolek	spolek	k1gInSc4	spolek
Hlahol	hlahol	k1gInSc4	hlahol
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
i	i	k9	i
krátce	krátce	k6eAd1	krátce
jeho	jeho	k3xOp3gMnSc7	jeho
sbormistrem	sbormistr	k1gMnSc7	sbormistr
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
v	v	k7c6	v
době	doba	k1gFnSc6	doba
hluchoty	hluchota	k1gFnSc2	hluchota
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
tajemník	tajemník	k1gMnSc1	tajemník
spolku	spolek	k1gInSc2	spolek
Josef	Josef	k1gMnSc1	Josef
Srb-Debrnov	Srb-Debrnovo	k1gNnPc2	Srb-Debrnovo
skladatelovým	skladatelův	k2eAgMnSc7d1	skladatelův
nejbližším	blízký	k2eAgMnSc7d3	nejbližší
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
pomocníkem	pomocník	k1gMnSc7	pomocník
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
většinou	většinou	k6eAd1	většinou
o	o	k7c4	o
sbory	sbor	k1gInPc4	sbor
mužské	mužský	k2eAgFnSc2d1	mužská
a	a	k8xC	a
bez	bez	k7c2	bez
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
postupně	postupně	k6eAd1	postupně
vznikaly	vznikat	k5eAaImAgFnP	vznikat
i	i	k9	i
různé	různý	k2eAgFnPc1d1	různá
jiné	jiný	k2eAgFnPc1d1	jiná
úpravy	úprava	k1gFnPc1	úprava
<g/>
;	;	kIx,	;
pouze	pouze	k6eAd1	pouze
konečná	konečný	k2eAgFnSc1d1	konečná
podoba	podoba	k1gFnSc1	podoba
České	český	k2eAgFnSc2d1	Česká
písně	píseň	k1gFnSc2	píseň
má	mít	k5eAaImIp3nS	mít
náročnější	náročný	k2eAgFnSc4d2	náročnější
formu	forma	k1gFnSc4	forma
kantáty	kantáta	k1gFnSc2	kantáta
pro	pro	k7c4	pro
smíšený	smíšený	k2eAgInSc4d1	smíšený
sbor	sbor	k1gInSc4	sbor
s	s	k7c7	s
průvodem	průvod	k1gInSc7	průvod
orchestru	orchestr	k1gInSc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
řadu	řada	k1gFnSc4	řada
otvírá	otvírat	k5eAaImIp3nS	otvírat
Píseň	píseň	k1gFnSc1	píseň
česká	český	k2eAgFnSc1d1	Česká
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
a	a	k8xC	a
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
Naše	náš	k3xOp1gFnSc1	náš
píseň	píseň	k1gFnSc1	píseň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
vůbec	vůbec	k9	vůbec
poslední	poslední	k2eAgFnSc1d1	poslední
dokončená	dokončený	k2eAgFnSc1d1	dokončená
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
skladba	skladba	k1gFnSc1	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
napsal	napsat	k5eAaPmAgMnS	napsat
Smetana	Smetana	k1gMnSc1	Smetana
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
také	také	k9	také
tři	tři	k4xCgInPc4	tři
trojhlasé	trojhlasý	k2eAgInPc4d1	trojhlasý
ženské	ženský	k2eAgInPc4d1	ženský
sbory	sbor	k1gInPc4	sbor
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
konkrétního	konkrétní	k2eAgMnSc2d1	konkrétní
adresáta	adresát	k1gMnSc2	adresát
<g/>
.	.	kIx.	.
</s>
<s>
Náměty	námět	k1gInPc1	námět
sborových	sborový	k2eAgFnPc2d1	sborová
skladeb	skladba	k1gFnPc2	skladba
jsou	být	k5eAaImIp3nP	být
vesměs	vesměs	k6eAd1	vesměs
vlastenecké	vlastenecký	k2eAgFnPc1d1	vlastenecká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tvarově	tvarově	k6eAd1	tvarově
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
pestré	pestrý	k2eAgInPc1d1	pestrý
od	od	k7c2	od
několikataktových	několikataktový	k2eAgNnPc2d1	několikataktový
Dvou	dva	k4xCgNnPc2	dva
hesel	heslo	k1gNnPc2	heslo
po	po	k7c4	po
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
scénu	scéna	k1gFnSc4	scéna
Píseň	píseň	k1gFnSc1	píseň
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
jejich	jejich	k3xOp3gInSc1	jejich
osud	osud	k1gInSc1	osud
byl	být	k5eAaImAgInS	být
různý	různý	k2eAgInSc1d1	různý
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
některé	některý	k3yIgInPc1	některý
sbory	sbor	k1gInPc1	sbor
zpopulárněly	zpopulárnět	k5eAaPmAgInP	zpopulárnět
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
Věno	věno	k1gNnSc1	věno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc4d1	jiný
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgInP	ukázat
pro	pro	k7c4	pro
amatérský	amatérský	k2eAgInSc4d1	amatérský
provoz	provoz	k1gInSc4	provoz
značně	značně	k6eAd1	značně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
(	(	kIx(	(
<g/>
Píseň	píseň	k1gFnSc1	píseň
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
Za	za	k7c2	za
hory	hora	k1gFnSc2	hora
slunce	slunce	k1gNnSc2	slunce
zapadá	zapadat	k5eAaPmIp3nS	zapadat
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
byly	být	k5eAaImAgFnP	být
objednateli	objednatel	k1gMnPc7	objednatel
odloženy	odložen	k2eAgFnPc4d1	odložena
a	a	k8xC	a
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
se	se	k3xPyFc4	se
prvního	první	k4xOgNnSc2	první
provedení	provedení	k1gNnSc2	provedení
až	až	k8xS	až
desítky	desítka	k1gFnSc2	desítka
let	léto	k1gNnPc2	léto
po	po	k7c6	po
skladatelově	skladatelův	k2eAgFnSc6d1	skladatelova
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
Modlitba	modlitba	k1gFnSc1	modlitba
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gFnSc1	náš
píseň	píseň	k1gFnSc1	píseň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sólovou	sólový	k2eAgFnSc4d1	sólová
vokální	vokální	k2eAgFnSc4d1	vokální
tvorbu	tvorba	k1gFnSc4	tvorba
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgNnSc6d1	Smetanovo
díle	dílo	k1gNnSc6	dílo
-	-	kIx~	-
vedle	vedle	k7c2	vedle
hrstky	hrstka	k1gFnSc2	hrstka
příležitostných	příležitostný	k2eAgFnPc2d1	příležitostná
nebo	nebo	k8xC	nebo
cvičných	cvičný	k2eAgFnPc2d1	cvičná
písní	píseň	k1gFnPc2	píseň
-	-	kIx~	-
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
pouze	pouze	k6eAd1	pouze
Večerní	večerní	k2eAgFnPc1d1	večerní
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
cyklus	cyklus	k1gInSc1	cyklus
pěti	pět	k4xCc2	pět
písní	píseň	k1gFnPc2	píseň
na	na	k7c6	na
slova	slovo	k1gNnSc2	slovo
Vítězslava	Vítězslava	k1gFnSc1	Vítězslava
Hálka	hálka	k1gFnSc1	hálka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
v	v	k7c6	v
schumannovském	schumannovský	k2eAgMnSc6d1	schumannovský
duchu	duch	k1gMnSc6	duch
<g/>
,	,	kIx,	,
vyjadřujících	vyjadřující	k2eAgMnPc6d1	vyjadřující
různé	různý	k2eAgFnPc4d1	různá
nálady	nálada	k1gFnPc4	nálada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spojených	spojený	k2eAgFnPc2d1	spojená
intenzitou	intenzita	k1gFnSc7	intenzita
výrazu	výraz	k1gInSc2	výraz
i	i	k8xC	i
autobiografickými	autobiografický	k2eAgInPc7d1	autobiografický
rysy	rys	k1gInPc7	rys
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Smetanovu	Smetanův	k2eAgFnSc4d1	Smetanova
orchestrální	orchestrální	k2eAgFnSc4d1	orchestrální
tvorbu	tvorba	k1gFnSc4	tvorba
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
odstup	odstup	k1gInSc1	odstup
od	od	k7c2	od
tradičních	tradiční	k2eAgFnPc2d1	tradiční
<g/>
,	,	kIx,	,
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
klasicistních	klasicistní	k2eAgFnPc2d1	klasicistní
forem	forma	k1gFnPc2	forma
"	"	kIx"	"
<g/>
absolutní	absolutní	k2eAgFnSc2d1	absolutní
hudby	hudba	k1gFnSc2	hudba
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
symfonie	symfonie	k1gFnSc1	symfonie
nebo	nebo	k8xC	nebo
koncert	koncert	k1gInSc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Tíhl	tíhnout	k5eAaImAgMnS	tíhnout
spíše	spíše	k9	spíše
k	k	k7c3	k
volnější	volný	k2eAgFnSc3d2	volnější
romantické	romantický	k2eAgFnSc3d1	romantická
formě	forma	k1gFnSc3	forma
osamostatněné	osamostatněný	k2eAgFnSc2d1	osamostatněná
programní	programní	k2eAgFnSc2d1	programní
předehry	předehra	k1gFnSc2	předehra
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
zpopularizovali	zpopularizovat	k5eAaPmAgMnP	zpopularizovat
Mendelssohn	Mendelssohn	k1gMnSc1	Mendelssohn
a	a	k8xC	a
Berlioz	Berlioz	k1gMnSc1	Berlioz
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
ve	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
Franze	Franze	k1gFnSc1	Franze
Liszta	Liszt	k1gInSc2	Liszt
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
první	první	k4xOgNnSc4	první
větší	veliký	k2eAgNnSc4d2	veliký
orchestrální	orchestrální	k2eAgNnSc4d1	orchestrální
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
předehra	předehra	k1gFnSc1	předehra
D	D	kA	D
dur	dur	k1gNnSc2	dur
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zejména	zejména	k9	zejména
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Berlioze	Berlioz	k1gMnSc2	Berlioz
a	a	k8xC	a
i	i	k9	i
jediná	jediný	k2eAgFnSc1d1	jediná
jeho	jeho	k3xOp3gFnSc1	jeho
symfonie	symfonie	k1gFnSc1	symfonie
<g/>
,	,	kIx,	,
Triumfální	triumfální	k2eAgFnSc1d1	triumfální
symfonie	symfonie	k1gFnSc1	symfonie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
strukturou	struktura	k1gFnSc7	struktura
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
způsobem	způsob	k1gInSc7	způsob
zpracování	zpracování	k1gNnSc2	zpracování
melodie	melodie	k1gFnSc2	melodie
rakouské	rakouský	k2eAgFnSc2d1	rakouská
hymny	hymna	k1gFnSc2	hymna
<g/>
,	,	kIx,	,
blízká	blízký	k2eAgFnSc1d1	blízká
žánru	žánr	k1gInSc3	žánr
předehry	předehra	k1gFnSc2	předehra
<g/>
.	.	kIx.	.
</s>
<s>
Bližší	blízký	k2eAgNnSc1d2	bližší
seznámení	seznámení	k1gNnSc1	seznámení
s	s	k7c7	s
Lisztovými	Lisztův	k2eAgFnPc7d1	Lisztova
orchestrálními	orchestrální	k2eAgFnPc7d1	orchestrální
skladbami	skladba	k1gFnPc7	skladba
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1856	[number]	k4	1856
<g/>
/	/	kIx~	/
<g/>
57	[number]	k4	57
podstatně	podstatně	k6eAd1	podstatně
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
směr	směr	k1gInSc4	směr
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
orchestrální	orchestrální	k2eAgFnSc2d1	orchestrální
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
strávených	strávený	k2eAgNnPc6d1	strávené
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
řadě	řada	k1gFnSc6	řada
námětů	námět	k1gInPc2	námět
na	na	k7c4	na
programní	programní	k2eAgNnPc4d1	programní
orchestrální	orchestrální	k2eAgNnPc4d1	orchestrální
díla	dílo	k1gNnPc4	dílo
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
symfonická	symfonický	k2eAgFnSc1d1	symfonická
báseň	báseň	k1gFnSc1	báseň
<g/>
"	"	kIx"	"
jim	on	k3xPp3gFnPc3	on
přiřknul	přiřknout	k5eAaPmAgMnS	přiřknout
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
konečné	konečná	k1gFnSc3	konečná
podoby	podoba	k1gFnSc2	podoba
nabyly	nabýt	k5eAaPmAgFnP	nabýt
Richard	Richard	k1gMnSc1	Richard
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Valdštýnův	Valdštýnův	k2eAgInSc1d1	Valdštýnův
tábor	tábor	k1gInSc1	tábor
a	a	k8xC	a
Hakon	Hakon	k1gMnSc1	Hakon
Jarl	jarl	k1gMnSc1	jarl
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mají	mít	k5eAaImIp3nP	mít
zřetelné	zřetelný	k2eAgInPc4d1	zřetelný
lisztovské	lisztovský	k2eAgInPc4d1	lisztovský
rysy	rys	k1gInPc4	rys
<g/>
:	:	kIx,	:
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
literární	literární	k2eAgInSc4d1	literární
námět	námět	k1gInSc4	námět
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
ústřední	ústřední	k2eAgFnSc4d1	ústřední
myšlenku	myšlenka	k1gFnSc4	myšlenka
má	mít	k5eAaImIp3nS	mít
hudba	hudba	k1gFnSc1	hudba
nikoli	nikoli	k9	nikoli
ilustrovat	ilustrovat	k5eAaBmF	ilustrovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlouběji	hluboko	k6eAd2	hluboko
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
literatuře	literatura	k1gFnSc3	literatura
či	či	k8xC	či
divadlu	divadlo	k1gNnSc3	divadlo
chybějí	chybět	k5eAaImIp3nP	chybět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
pražského	pražský	k2eAgNnSc2d1	Pražské
působení	působení	k1gNnSc2	působení
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Smetana	Smetana	k1gMnSc1	Smetana
omezil	omezit	k5eAaPmAgMnS	omezit
na	na	k7c4	na
drobnější	drobný	k2eAgFnPc4d2	drobnější
příležitostné	příležitostný	k2eAgFnPc4d1	příležitostná
orchestrální	orchestrální	k2eAgFnPc4d1	orchestrální
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
ohluchnutí	ohluchnutí	k1gNnSc6	ohluchnutí
<g/>
,	,	kIx,	,
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
odstupem	odstup	k1gInSc7	odstup
od	od	k7c2	od
"	"	kIx"	"
<g/>
švédského	švédský	k2eAgInSc2d1	švédský
cyklu	cyklus	k1gInSc2	cyklus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
symfonické	symfonický	k2eAgFnSc3d1	symfonická
básni	báseň	k1gFnSc3	báseň
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
čtyř	čtyři	k4xCgFnPc2	čtyři
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
šesti	šest	k4xCc2	šest
děl	dělo	k1gNnPc2	dělo
pod	pod	k7c7	pod
souborným	souborný	k2eAgInSc7d1	souborný
názvem	název	k1gInSc7	název
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc1	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Lisztův	Lisztův	k2eAgInSc1d1	Lisztův
příklad	příklad	k1gInSc1	příklad
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
sledován	sledovat	k5eAaImNgInS	sledovat
mnohem	mnohem	k6eAd1	mnohem
volněji	volně	k6eAd2	volně
již	již	k6eAd1	již
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
program	program	k1gInSc1	program
nemá	mít	k5eNaImIp3nS	mít
literární	literární	k2eAgInSc4d1	literární
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
definuje	definovat	k5eAaBmIp3nS	definovat
ho	on	k3xPp3gNnSc4	on
samotný	samotný	k2eAgMnSc1d1	samotný
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
náplň	náplň	k1gFnSc1	náplň
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
instrumentální	instrumentální	k2eAgFnSc4d1	instrumentální
hudbu	hudba	k1gFnSc4	hudba
nezvyklá	zvyklý	k2eNgFnSc1d1	nezvyklá
<g/>
.	.	kIx.	.
</s>
<s>
Má	můj	k3xOp1gFnSc1	můj
vlast	vlast	k1gFnSc1	vlast
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
slovy	slovo	k1gNnPc7	slovo
Marty	Marta	k1gFnSc2	Marta
Ottlové	Ottlová	k1gFnSc2	Ottlová
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
monumentální	monumentální	k2eAgInSc1d1	monumentální
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
jedinečnou	jedinečný	k2eAgFnSc7d1	jedinečná
apoteózou	apoteóza	k1gFnSc7	apoteóza
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
zakořeněna	zakořenit	k5eAaPmNgFnS	zakořenit
existence	existence	k1gFnSc1	existence
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
a	a	k8xC	a
oslavou	oslava	k1gFnSc7	oslava
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
vznikající	vznikající	k2eAgInSc4d1	vznikající
moderní	moderní	k2eAgInSc4d1	moderní
český	český	k2eAgInSc4d1	český
národ	národ	k1gInSc4	národ
naplněna	naplněn	k2eAgFnSc1d1	naplněna
mýtickými	mýtický	k2eAgFnPc7d1	mýtická
a	a	k8xC	a
historickými	historický	k2eAgFnPc7d1	historická
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
svázanými	svázaný	k2eAgFnPc7d1	svázaná
s	s	k7c7	s
vizí	vize	k1gFnSc7	vize
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Torzo	torzo	k1gNnSc1	torzo
dalšího	další	k2eAgInSc2d1	další
zamýšleného	zamýšlený	k2eAgInSc2d1	zamýšlený
cyklu	cyklus	k1gInSc2	cyklus
symfonických	symfonický	k2eAgFnPc2d1	symfonická
básní	báseň	k1gFnPc2	báseň
Pražský	pražský	k2eAgInSc1d1	pražský
karneval	karneval	k1gInSc1	karneval
opět	opět	k6eAd1	opět
dokládá	dokládat	k5eAaImIp3nS	dokládat
Smetanovo	Smetanův	k2eAgNnSc4d1	Smetanovo
nadšení	nadšení	k1gNnSc4	nadšení
pro	pro	k7c4	pro
společenský	společenský	k2eAgInSc4d1	společenský
ruch	ruch	k1gInSc4	ruch
a	a	k8xC	a
tanec	tanec	k1gInSc4	tanec
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
rysy	rys	k1gInPc4	rys
jeho	jeho	k3xOp3gNnSc2	jeho
pozdního	pozdní	k2eAgNnSc2d1	pozdní
období	období	k1gNnSc2	období
jako	jako	k8xC	jako
útržkovitost	útržkovitost	k1gFnSc4	útržkovitost
<g/>
,	,	kIx,	,
zhuštěnost	zhuštěnost	k1gFnSc4	zhuštěnost
a	a	k8xC	a
harmonickou	harmonický	k2eAgFnSc4d1	harmonická
příkrost	příkrost	k1gFnSc4	příkrost
<g/>
.	.	kIx.	.
</s>
<s>
Komorní	komorní	k2eAgFnSc1d1	komorní
tvorba	tvorba	k1gFnSc1	tvorba
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
je	být	k5eAaImIp3nS	být
nečetná	četný	k2eNgFnSc1d1	nečetná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zásadní	zásadní	k2eAgFnPc4d1	zásadní
díla	dílo	k1gNnPc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
své	svůj	k3xOyFgFnPc4	svůj
nejosobnější	osobní	k2eAgFnPc4d3	nejosobnější
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
autobiografické	autobiografický	k2eAgFnPc4d1	autobiografická
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohlédnutím	ohlédnutí	k1gNnSc7	ohlédnutí
od	od	k7c2	od
rané	raný	k2eAgFnSc2d1	raná
virtuózní	virtuózní	k2eAgFnSc2d1	virtuózní
a	a	k8xC	a
užitkové	užitkový	k2eAgFnSc2d1	užitková
Fantasie	fantasie	k1gFnSc2	fantasie
na	na	k7c4	na
českou	český	k2eAgFnSc4d1	Česká
píseň	píseň	k1gFnSc4	píseň
(	(	kIx(	(
<g/>
Sil	silo	k1gNnPc2	silo
jsem	být	k5eAaImIp1nS	být
proso	proso	k1gNnSc1	proso
na	na	k7c6	na
souvrati	souvrať	k1gFnSc6	souvrať
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nS	patřit
nejprve	nejprve	k6eAd1	nejprve
Trio	trio	k1gNnSc1	trio
g	g	kA	g
moll	moll	k1gNnSc2	moll
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
napsané	napsaný	k2eAgInPc1d1	napsaný
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
dcery	dcera	k1gFnSc2	dcera
Bedřišky	Bedřiška	k1gFnSc2	Bedřiška
(	(	kIx(	(
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rapsodické	rapsodický	k2eAgNnSc1d1	Rapsodické
dílo	dílo	k1gNnSc1	dílo
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
ploše	plocha	k1gFnSc6	plocha
měnící	měnící	k2eAgFnPc1d1	měnící
se	se	k3xPyFc4	se
nálady	nálada	k1gFnPc1	nálada
od	od	k7c2	od
bolu	bol	k1gInSc2	bol
a	a	k8xC	a
zoufalství	zoufalství	k1gNnSc2	zoufalství
přes	přes	k7c4	přes
rezignaci	rezignace	k1gFnSc4	rezignace
po	po	k7c4	po
smír	smír	k1gInSc4	smír
s	s	k7c7	s
životem	život	k1gInSc7	život
<g/>
;	;	kIx,	;
jednotu	jednota	k1gFnSc4	jednota
díla	dílo	k1gNnSc2	dílo
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
důmyslná	důmyslný	k2eAgFnSc1d1	důmyslná
tematická	tematický	k2eAgFnSc1d1	tematická
provázanost	provázanost	k1gFnSc1	provázanost
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
komorním	komorní	k2eAgInSc7d1	komorní
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
Smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
č.	č.	k?	č.
1	[number]	k4	1
e	e	k0	e
moll	moll	k1gNnSc6	moll
"	"	kIx"	"
<g/>
Z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vnější	vnější	k2eAgFnSc1d1	vnější
forma	forma	k1gFnSc1	forma
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
(	(	kIx(	(
<g/>
jen	jen	k9	jen
místo	místo	k7c2	místo
scherza	scherzo	k1gNnSc2	scherzo
je	být	k5eAaImIp3nS	být
polková	polkový	k2eAgFnSc1d1	Polková
věta	věta	k1gFnSc1	věta
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
již	již	k9	již
v	v	k7c6	v
dřívějších	dřívější	k2eAgNnPc6d1	dřívější
kvartetech	kvarteto	k1gNnPc6	kvarteto
Fibichových	Fibichových	k2eAgMnSc1d1	Fibichových
a	a	k8xC	a
Dvořákových	Dvořákových	k2eAgMnSc1d1	Dvořákových
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
je	být	k5eAaImIp3nS	být
autobiografický	autobiografický	k2eAgInSc1d1	autobiografický
charakter	charakter	k1gInSc1	charakter
(	(	kIx(	(
<g/>
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
pět	pět	k4xCc1	pět
programových	programový	k2eAgInPc2d1	programový
výkladů	výklad	k1gInPc2	výklad
od	od	k7c2	od
samotného	samotný	k2eAgMnSc2d1	samotný
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vůbec	vůbec	k9	vůbec
první	první	k4xOgNnSc4	první
významné	významný	k2eAgNnSc4d1	významné
programní	programní	k2eAgNnSc4d1	programní
dílo	dílo	k1gNnSc4	dílo
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
komorní	komorní	k2eAgFnSc2d1	komorní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
houslová	houslový	k2eAgNnPc4d1	houslové
dua	duo	k1gNnPc4	duo
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Z	z	k7c2	z
domoviny	domovina	k1gFnSc2	domovina
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jakási	jakýsi	k3yIgFnSc1	jakýsi
komorní	komorní	k2eAgFnSc1d1	komorní
<g/>
,	,	kIx,	,
virtuózní	virtuózní	k2eAgFnSc1d1	virtuózní
miniatura	miniatura	k1gFnSc1	miniatura
Mé	můj	k3xOp1gFnSc2	můj
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Soukromější	soukromý	k2eAgFnSc1d2	soukromější
a	a	k8xC	a
opět	opět	k6eAd1	opět
autobiografickou	autobiografický	k2eAgFnSc4d1	autobiografická
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
tak	tak	k9	tak
jasně	jasně	k6eAd1	jasně
programově	programově	k6eAd1	programově
vyložitelnou	vyložitelný	k2eAgFnSc4d1	vyložitelná
povahu	povaha	k1gFnSc4	povaha
má	mít	k5eAaImIp3nS	mít
poslední	poslední	k2eAgNnSc1d1	poslední
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
komorní	komorní	k2eAgNnSc1d1	komorní
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
Smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
č.	č.	k?	č.
2	[number]	k4	2
d	d	k?	d
moll	moll	k1gNnSc6	moll
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zkratkovitost	zkratkovitost	k1gFnSc1	zkratkovitost
<g/>
,	,	kIx,	,
útržkovitost	útržkovitost	k1gFnSc1	útržkovitost
a	a	k8xC	a
melodická	melodický	k2eAgFnSc1d1	melodická
zhuštěnost	zhuštěnost	k1gFnSc1	zhuštěnost
tohoto	tento	k3xDgInSc2	tento
kvartetu	kvartet	k1gInSc2	kvartet
je	být	k5eAaImIp3nS	být
dílem	dílo	k1gNnSc7	dílo
skladatelovy	skladatelův	k2eAgFnSc2d1	skladatelova
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
dovolovala	dovolovat	k5eAaImAgFnS	dovolovat
komponovat	komponovat	k5eAaImF	komponovat
jen	jen	k9	jen
v	v	k7c6	v
krátkých	krátký	k2eAgInPc6d1	krátký
časových	časový	k2eAgInPc6d1	časový
úsecích	úsek	k1gInPc6	úsek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současně	současně	k6eAd1	současně
předjímá	předjímat	k5eAaImIp3nS	předjímat
kompoziční	kompoziční	k2eAgInSc4d1	kompoziční
styl	styl	k1gInSc4	styl
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
tak	tak	k6eAd1	tak
nezvykle	zvykle	k6eNd1	zvykle
moderně	moderně	k6eAd1	moderně
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
klavír	klavír	k1gInSc4	klavír
tvoří	tvořit	k5eAaImIp3nP	tvořit
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
Smetanovy	Smetanův	k2eAgFnSc2d1	Smetanova
tvorby	tvorba	k1gFnSc2	tvorba
z	z	k7c2	z
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Počínají	počínat	k5eAaImIp3nP	počínat
prvními	první	k4xOgFnPc7	první
skladbami	skladba	k1gFnPc7	skladba
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
dětství	dětství	k1gNnSc2	dětství
i	i	k8xC	i
gymnaziálních	gymnaziální	k2eAgFnPc2d1	gymnaziální
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
převažují	převažovat	k5eAaImIp3nP	převažovat
i	i	k9	i
mezi	mezi	k7c7	mezi
kompozičními	kompoziční	k2eAgNnPc7d1	kompoziční
cvičeními	cvičení	k1gNnPc7	cvičení
i	i	k8xC	i
jinými	jiný	k2eAgNnPc7d1	jiné
díly	dílo	k1gNnPc7	dílo
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
studia	studio	k1gNnSc2	studio
u	u	k7c2	u
Proksche	Proksch	k1gFnSc2	Proksch
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
aktivitou	aktivita	k1gFnSc7	aktivita
jako	jako	k8xC	jako
učitele	učitel	k1gMnSc2	učitel
hry	hra	k1gFnSc2	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
didaktických	didaktický	k2eAgFnPc2d1	didaktická
skladeb	skladba	k1gFnPc2	skladba
a	a	k8xC	a
úprav	úprava	k1gFnPc2	úprava
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
typu	typ	k1gInSc2	typ
skladeb	skladba	k1gFnPc2	skladba
sledoval	sledovat	k5eAaImAgMnS	sledovat
Smetana	Smetana	k1gMnSc1	Smetana
několik	několik	k4yIc4	několik
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
byla	být	k5eAaImAgFnS	být
stylizace	stylizace	k1gFnSc1	stylizace
dobového	dobový	k2eAgInSc2d1	dobový
společenského	společenský	k2eAgInSc2d1	společenský
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
polky	polka	k1gFnSc2	polka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
užitkových	užitkový	k2eAgInPc2d1	užitkový
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
tanečních	taneční	k2eAgFnPc2d1	taneční
polek	polka	k1gFnPc2	polka
z	z	k7c2	z
plzeňských	plzeňský	k2eAgNnPc2d1	plzeňské
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
její	její	k3xOp3gNnSc1	její
zpracování	zpracování	k1gNnSc1	zpracování
zušlechťovalo	zušlechťovat	k5eAaImAgNnS	zušlechťovat
(	(	kIx(	(
<g/>
cykly	cyklus	k1gInPc1	cyklus
Tři	tři	k4xCgFnPc1	tři
salonní	salonní	k2eAgFnPc1d1	salonní
polky	polka	k1gFnPc1	polka
<g/>
,	,	kIx,	,
Tři	tři	k4xCgFnPc1	tři
poetické	poetický	k2eAgFnPc1d1	poetická
polky	polka	k1gFnPc1	polka
<g/>
)	)	kIx)	)
a	a	k8xC	a
umělecké	umělecký	k2eAgFnSc2d1	umělecká
svébytnosti	svébytnost	k1gFnSc2	svébytnost
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
na	na	k7c4	na
Čechy	Čech	k1gMnPc4	Čech
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
polek	polka	k1gFnPc2	polka
(	(	kIx(	(
<g/>
1859	[number]	k4	1859
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
linií	linie	k1gFnSc7	linie
byly	být	k5eAaImAgFnP	být
romantické	romantický	k2eAgFnPc1d1	romantická
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
salónní	salónní	k2eAgFnSc2d1	salónní
drobné	drobný	k2eAgFnSc2d1	drobná
skladby	skladba	k1gFnSc2	skladba
řazené	řazený	k2eAgFnSc2d1	řazená
do	do	k7c2	do
volných	volný	k2eAgInPc2d1	volný
cyklů	cyklus	k1gInPc2	cyklus
(	(	kIx(	(
<g/>
Bagately	bagatela	k1gFnPc1	bagatela
a	a	k8xC	a
impromptus	impromptus	k1gInSc1	impromptus
<g/>
,	,	kIx,	,
Šest	šest	k4xCc1	šest
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
Lístky	lístek	k1gInPc4	lístek
do	do	k7c2	do
památníku	památník	k1gInSc2	památník
<g/>
,	,	kIx,	,
Črty	črta	k1gFnSc2	črta
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
Smetanovým	Smetanův	k2eAgMnPc3d1	Smetanův
žákům	žák	k1gMnPc3	žák
<g/>
,	,	kIx,	,
známým	známá	k1gFnPc3	známá
nebo	nebo	k8xC	nebo
hudebním	hudební	k2eAgInPc3d1	hudební
idolům	idol	k1gInPc3	idol
<g/>
.	.	kIx.	.
</s>
<s>
Formou	forma	k1gFnSc7	forma
i	i	k8xC	i
zpracováním	zpracování	k1gNnSc7	zpracování
tyto	tento	k3xDgFnPc4	tento
skladby	skladba	k1gFnPc1	skladba
připomínají	připomínat	k5eAaImIp3nP	připomínat
skladatelovy	skladatelův	k2eAgInPc4d1	skladatelův
vzory	vzor	k1gInPc4	vzor
Mendelssohna	Mendelssohn	k1gMnSc2	Mendelssohn
<g/>
,	,	kIx,	,
Chopina	Chopin	k1gMnSc2	Chopin
<g/>
,	,	kIx,	,
Schumanna	Schumann	k1gMnSc2	Schumann
a	a	k8xC	a
Liszta	Liszt	k1gMnSc2	Liszt
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
skladby	skladba	k1gFnPc1	skladba
pak	pak	k6eAd1	pak
vznikaly	vznikat	k5eAaImAgFnP	vznikat
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
Smetanovou	Smetanův	k2eAgFnSc7d1	Smetanova
dráhou	dráha	k1gFnSc7	dráha
koncertního	koncertní	k2eAgMnSc2d1	koncertní
klavíristy	klavírista	k1gMnSc2	klavírista
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
vypracovával	vypracovávat	k5eAaImAgInS	vypracovávat
virtuózní	virtuózní	k2eAgFnPc4d1	virtuózní
klavírní	klavírní	k2eAgFnPc4d1	klavírní
transkripce	transkripce	k1gFnPc4	transkripce
<g/>
,	,	kIx,	,
kadence	kadence	k1gFnPc4	kadence
do	do	k7c2	do
klavírních	klavírní	k2eAgInPc2d1	klavírní
koncertů	koncert	k1gInPc2	koncert
i	i	k8xC	i
samostatné	samostatný	k2eAgFnPc4d1	samostatná
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
koncertní	koncertní	k2eAgFnSc1d1	koncertní
etuda	etuda	k1gFnSc1	etuda
Na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
mořském	mořský	k2eAgInSc6d1	mořský
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Fantasie	fantasie	k1gFnSc1	fantasie
na	na	k7c4	na
české	český	k2eAgFnPc4d1	Česká
národní	národní	k2eAgFnPc4d1	národní
písně	píseň	k1gFnPc4	píseň
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
klavírní	klavírní	k2eAgFnSc1d1	klavírní
tvorba	tvorba	k1gFnSc1	tvorba
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
<g/>
;	;	kIx,	;
skladatelova	skladatelův	k2eAgFnSc1d1	skladatelova
pozornost	pozornost	k1gFnSc1	pozornost
se	se	k3xPyFc4	se
obrátila	obrátit	k5eAaPmAgFnS	obrátit
k	k	k7c3	k
žánrům	žánr	k1gInPc3	žánr
považovaným	považovaný	k2eAgInPc3d1	považovaný
za	za	k7c4	za
významnější	významný	k2eAgInPc4d2	významnější
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
národního	národní	k2eAgInSc2d1	národní
hudebního	hudební	k2eAgInSc2d1	hudební
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
sbory	sbor	k1gInPc1	sbor
<g/>
,	,	kIx,	,
orchestrální	orchestrální	k2eAgFnSc1d1	orchestrální
hudba	hudba	k1gFnSc1	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
klavírní	klavírní	k2eAgFnSc3d1	klavírní
tvorbě	tvorba	k1gFnSc3	tvorba
se	se	k3xPyFc4	se
Smetana	Smetana	k1gMnSc1	Smetana
vrátil	vrátit	k5eAaPmAgMnS	vrátit
až	až	k6eAd1	až
po	po	k7c6	po
ohluchnutí	ohluchnutí	k1gNnSc6	ohluchnutí
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
zralých	zralý	k2eAgInPc6d1	zralý
cyklech	cyklus	k1gInPc6	cyklus
završujících	završující	k2eAgInPc6d1	završující
oba	dva	k4xCgMnPc1	dva
jeho	on	k3xPp3gNnSc2	on
dřívější	dřívější	k2eAgInPc1d1	dřívější
tematické	tematický	k2eAgInPc1d1	tematický
okruhy	okruh	k1gInPc1	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Sny	sen	k1gInPc1	sen
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
věnoval	věnovat	k5eAaPmAgMnS	věnovat
svým	svůj	k3xOyFgFnPc3	svůj
bývalým	bývalý	k2eAgFnPc3d1	bývalá
aristokratickým	aristokratický	k2eAgFnPc3d1	aristokratická
žačkám	žačka	k1gFnPc3	žačka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
přispěly	přispět	k5eAaPmAgFnP	přispět
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
léčení	léčení	k1gNnSc6	léčení
<g/>
.	.	kIx.	.
</s>
<s>
Nostalgicky	nostalgicky	k6eAd1	nostalgicky
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
intimní	intimní	k2eAgFnSc3d1	intimní
salonní	salonní	k2eAgFnSc3d1	salonní
formě	forma	k1gFnSc3	forma
Lístků	lístek	k1gInPc2	lístek
do	do	k7c2	do
památníku	památník	k1gInSc2	památník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
umělecké	umělecký	k2eAgNnSc4d1	umělecké
využití	využití	k1gNnSc4	využití
a	a	k8xC	a
zušlechtění	zušlechtění	k1gNnSc4	zušlechtění
českého	český	k2eAgNnSc2d1	české
(	(	kIx(	(
<g/>
polo	polo	k6eAd1	polo
<g/>
)	)	kIx)	)
<g/>
lidového	lidový	k2eAgInSc2d1	lidový
tance	tanec	k1gInSc2	tanec
skladatel	skladatel	k1gMnSc1	skladatel
navazuje	navazovat	k5eAaImIp3nS	navazovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
řadách	řada	k1gFnPc6	řada
Českých	český	k2eAgInPc2d1	český
tanců	tanec	k1gInPc2	tanec
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
ohlasem	ohlas	k1gInSc7	ohlas
soudobé	soudobý	k2eAgFnSc2d1	soudobá
popularity	popularita	k1gFnSc2	popularita
"	"	kIx"	"
<g/>
etnických	etnický	k2eAgFnPc2d1	etnická
<g/>
"	"	kIx"	"
skladeb	skladba	k1gFnPc2	skladba
Brahmsových	Brahmsová	k1gFnPc2	Brahmsová
a	a	k8xC	a
Dvořákových	Dvořáková	k1gFnPc2	Dvořáková
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
vlastenecký	vlastenecký	k2eAgInSc1d1	vlastenecký
podtext	podtext	k1gInSc1	podtext
mající	mající	k2eAgInSc1d1	mající
dokládat	dokládat	k5eAaImF	dokládat
původnost	původnost	k1gFnSc4	původnost
a	a	k8xC	a
hodnotu	hodnota	k1gFnSc4	hodnota
české	český	k2eAgFnSc2d1	Česká
kulturní	kulturní	k2eAgFnSc2d1	kulturní
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	Smetana	k1gMnSc1	Smetana
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	se	k3xPyFc3	se
ostatně	ostatně	k6eAd1	ostatně
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
života	život	k1gInSc2	život
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsemť	Jsemť	k1gFnSc1	Jsemť
dle	dle	k7c2	dle
záslůh	záslůha	k1gFnPc2	záslůha
mých	můj	k3xOp1gMnPc2	můj
a	a	k8xC	a
dle	dle	k7c2	dle
mého	můj	k3xOp1gNnSc2	můj
snažení	snažení	k1gNnSc2	snažení
skladatelem	skladatel	k1gMnSc7	skladatel
českým	český	k2eAgMnSc7d1	český
a	a	k8xC	a
stvořitelem	stvořitel	k1gMnSc7	stvořitel
českého	český	k2eAgInSc2d1	český
slohu	sloh	k1gInSc2	sloh
v	v	k7c6	v
dramatickém	dramatický	k2eAgNnSc6d1	dramatické
a	a	k8xC	a
ve	v	k7c6	v
symfonickém	symfonický	k2eAgInSc6d1	symfonický
oddílu	oddíl	k1gInSc6	oddíl
hudby	hudba	k1gFnSc2	hudba
-	-	kIx~	-
výhradně	výhradně	k6eAd1	výhradně
české	český	k2eAgFnPc1d1	Česká
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dopis	dopis	k1gInSc4	dopis
J.	J.	kA	J.
L.	L.	kA	L.
Procházkovi	Procházkův	k2eAgMnPc1d1	Procházkův
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hledání	hledání	k1gNnSc4	hledání
objektivních	objektivní	k2eAgNnPc2d1	objektivní
"	"	kIx"	"
<g/>
českých	český	k2eAgNnPc2d1	české
<g/>
"	"	kIx"	"
znaků	znak	k1gInPc2	znak
ve	v	k7c6	v
Smetanově	Smetanův	k2eAgFnSc6d1	Smetanova
hudbě	hudba	k1gFnSc6	hudba
(	(	kIx(	(
<g/>
ozvuky	ozvuk	k1gInPc1	ozvuk
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
české	český	k2eAgInPc1d1	český
taneční	taneční	k2eAgInPc1d1	taneční
rytmy	rytmus	k1gInPc1	rytmus
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc1	vliv
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
na	na	k7c4	na
formu	forma	k1gFnSc4	forma
deklamace	deklamace	k1gFnSc2	deklamace
<g/>
)	)	kIx)	)
však	však	k9	však
moderní	moderní	k2eAgMnPc1d1	moderní
hudební	hudební	k2eAgMnPc1d1	hudební
teoretici	teoretik	k1gMnPc1	teoretik
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
opačný	opačný	k2eAgInSc1d1	opačný
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
podle	podle	k7c2	podle
muzikologa	muzikolog	k1gMnSc2	muzikolog
Johna	John	k1gMnSc2	John
Tyrrella	Tyrrell	k1gMnSc2	Tyrrell
je	být	k5eAaImIp3nS	být
Smetana	Smetana	k1gMnSc1	Smetana
nepochybně	pochybně	k6eNd1	pochybně
vlastním	vlastní	k2eAgMnSc7d1	vlastní
zakladatelem	zakladatel	k1gMnSc7	zakladatel
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
českojazyčné	českojazyčný	k2eAgInPc1d1	českojazyčný
<g/>
)	)	kIx)	)
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
dal	dát	k5eAaPmAgInS	dát
základní	základní	k2eAgInSc1d1	základní
kánon	kánon	k1gInSc1	kánon
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
symfonickém	symfonický	k2eAgInSc6d1	symfonický
a	a	k8xC	a
komorním	komorní	k2eAgInSc6d1	komorní
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
originální	originální	k2eAgInSc4d1	originální
osobní	osobní	k2eAgInSc4d1	osobní
hudební	hudební	k2eAgInSc4d1	hudební
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
česká	český	k2eAgFnSc1d1	Česká
veřejnost	veřejnost	k1gFnSc1	veřejnost
brzy	brzy	k6eAd1	brzy
ztotožnila	ztotožnit	k5eAaPmAgFnS	ztotožnit
s	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
stylem	styl	k1gInSc7	styl
jako	jako	k8xS	jako
takovým	takový	k3xDgNnSc7	takový
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
Smetana	Smetana	k1gMnSc1	Smetana
stal	stát	k5eAaPmAgMnS	stát
<g/>
,	,	kIx,	,
slovy	slovo	k1gNnPc7	slovo
německého	německý	k2eAgMnSc2d1	německý
smetanovského	smetanovský	k2eAgMnSc2d1	smetanovský
badatele	badatel	k1gMnSc2	badatel
Kurta	Kurt	k1gMnSc2	Kurt
Honolky	Honolka	k1gFnSc2	Honolka
"	"	kIx"	"
<g/>
reprezentativní	reprezentativní	k2eAgNnSc1d1	reprezentativní
tvůrčí	tvůrčí	k2eAgFnSc7d1	tvůrčí
osobností	osobnost	k1gFnSc7	osobnost
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
vůbec	vůbec	k9	vůbec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Christophera	Christophero	k1gNnSc2	Christophero
Storcka	Storcko	k1gNnSc2	Storcko
je	být	k5eAaImIp3nS	být
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
dílo	dílo	k1gNnSc1	dílo
-	-	kIx~	-
zejména	zejména	k9	zejména
nejznámější	známý	k2eAgFnSc1d3	nejznámější
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
,	,	kIx,	,
Libuše	Libuše	k1gFnSc1	Libuše
a	a	k8xC	a
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc4	vlast
-	-	kIx~	-
dobově	dobově	k6eAd1	dobově
žádanou	žádaný	k2eAgFnSc7d1	žádaná
"	"	kIx"	"
<g/>
symbiózou	symbióza	k1gFnSc7	symbióza
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
národního	národní	k2eAgNnSc2d1	národní
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
spojuje	spojovat	k5eAaImIp3nS	spojovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
hodnotu	hodnota	k1gFnSc4	hodnota
s	s	k7c7	s
intenzivním	intenzivní	k2eAgNnSc7d1	intenzivní
vyjádřením	vyjádření	k1gNnSc7	vyjádření
národního	národní	k2eAgInSc2d1	národní
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
také	také	k9	také
právě	právě	k6eAd1	právě
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třeba	třeba	k6eAd1	třeba
Dvořákova	Dvořákův	k2eAgFnSc1d1	Dvořákova
nebo	nebo	k8xC	nebo
Janáčkova	Janáčkův	k2eAgFnSc1d1	Janáčkova
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
kulturním	kulturní	k2eAgInSc7d1	kulturní
a	a	k8xC	a
politickým	politický	k2eAgInSc7d1	politický
symbolem	symbol	k1gInSc7	symbol
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
účinek	účinek	k1gInSc1	účinek
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
za	za	k7c2	za
vypjatých	vypjatý	k2eAgFnPc2d1	vypjatá
dějinných	dějinný	k2eAgFnPc2d1	dějinná
okolností	okolnost	k1gFnPc2	okolnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
za	za	k7c2	za
první	první	k4xOgFnSc2	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
však	však	k9	však
také	také	k9	také
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
ideologicky	ideologicky	k6eAd1	ideologicky
a	a	k8xC	a
politicky	politicky	k6eAd1	politicky
instrumentalizována	instrumentalizován	k2eAgFnSc1d1	instrumentalizována
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
žurnalistických	žurnalistický	k2eAgInPc2d1	žurnalistický
"	"	kIx"	"
<g/>
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
Smetanu	smetana	k1gFnSc4	smetana
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
skupina	skupina	k1gFnSc1	skupina
skladatelových	skladatelův	k2eAgMnPc2d1	skladatelův
příznivců	příznivec	k1gMnPc2	příznivec
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Otakara	Otakar	k1gMnSc2	Otakar
Hostinského	hostinský	k1gMnSc2	hostinský
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
stereotypizovaný	stereotypizovaný	k2eAgInSc1d1	stereotypizovaný
obraz	obraz	k1gInSc1	obraz
Smetany	smetana	k1gFnSc2	smetana
i	i	k8xC	i
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
významného	významný	k2eAgMnSc2d1	významný
Smetanova	Smetanův	k2eAgMnSc2d1	Smetanův
propagátora	propagátor	k1gMnSc2	propagátor
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Nejedlého	Nejedlý	k1gMnSc2	Nejedlý
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
školy	škola	k1gFnSc2	škola
upevňoval	upevňovat	k5eAaImAgMnS	upevňovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c4	v
dogma	dogma	k1gNnSc4	dogma
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
za	za	k7c2	za
komunistické	komunistický	k2eAgFnSc2d1	komunistická
éry	éra	k1gFnSc2	éra
stalo	stát	k5eAaPmAgNnS	stát
ideologickým	ideologický	k2eAgNnSc7d1	ideologické
kultem	kult	k1gInSc7	kult
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
vykládáno	vykládat	k5eAaImNgNnS	vykládat
úzkým	úzký	k2eAgInSc7d1	úzký
pohledem	pohled	k1gInSc7	pohled
Hostinského	hostinský	k1gMnSc2	hostinský
teorie	teorie	k1gFnSc2	teorie
o	o	k7c6	o
pokroku	pokrok	k1gInSc6	pokrok
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
směřujícímu	směřující	k2eAgInSc3d1	směřující
k	k	k7c3	k
symfonické	symfonický	k2eAgFnSc3d1	symfonická
básni	báseň	k1gFnSc3	báseň
a	a	k8xC	a
wagnerovskému	wagnerovský	k2eAgNnSc3d1	wagnerovské
hudebnímu	hudební	k2eAgNnSc3d1	hudební
dramatu	drama	k1gNnSc3	drama
<g/>
;	;	kIx,	;
opačné	opačný	k2eAgInPc1d1	opačný
rysy	rys	k1gInPc1	rys
Smetanových	Smetanových	k2eAgNnPc2d1	Smetanových
děl	dělo	k1gNnPc2	dělo
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
vykládány	vykládat	k5eAaImNgInP	vykládat
jako	jako	k9	jako
kompromisy	kompromis	k1gInPc4	kompromis
nebo	nebo	k8xC	nebo
ústupky	ústupek	k1gInPc4	ústupek
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Podobně	podobně	k6eAd1	podobně
byly	být	k5eAaImAgInP	být
výběrově	výběrově	k6eAd1	výběrově
přiznávány	přiznáván	k2eAgInPc1d1	přiznáván
nebo	nebo	k8xC	nebo
odmítány	odmítán	k2eAgInPc1d1	odmítán
vnější	vnější	k2eAgInPc1d1	vnější
vlivy	vliv	k1gInPc1	vliv
na	na	k7c4	na
skladatelovo	skladatelův	k2eAgNnSc4d1	skladatelovo
dílo	dílo	k1gNnSc4	dílo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
zveličován	zveličován	k2eAgInSc4d1	zveličován
jeho	on	k3xPp3gInSc4	on
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
výběru	výběr	k1gInSc6	výběr
a	a	k8xC	a
konečné	konečný	k2eAgFnSc6d1	konečná
podobě	podoba	k1gFnSc6	podoba
libret	libreto	k1gNnPc2	libreto
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
osobnost	osobnost	k1gFnSc1	osobnost
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
idealizována	idealizovat	k5eAaBmNgFnS	idealizovat
a	a	k8xC	a
sentimentalizována	sentimentalizovat	k5eAaImNgFnS	sentimentalizovat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
osudem	osud	k1gInSc7	osud
i	i	k9	i
nepřáteli	nepřítel	k1gMnPc7	nepřítel
pronásledovaného	pronásledovaný	k2eAgMnSc2d1	pronásledovaný
českého	český	k2eAgMnSc2d1	český
mučedníka	mučedník	k1gMnSc2	mučedník
<g/>
,	,	kIx,	,
nepochopeného	pochopený	k2eNgMnSc2d1	nepochopený
génia	génius	k1gMnSc2	génius
(	(	kIx(	(
<g/>
z	z	k7c2	z
novější	nový	k2eAgFnSc2d2	novější
literatury	literatura	k1gFnSc2	literatura
teto	teta	k1gFnSc5	teta
přístup	přístup	k1gInSc4	přístup
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
například	například	k6eAd1	například
kniha	kniha	k1gFnSc1	kniha
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Mahlera	Mahler	k1gMnSc2	Mahler
Nekamenujte	kamenovat	k5eNaImRp2nP	kamenovat
proroky	prorok	k1gMnPc4	prorok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dogmaticky	dogmaticky	k6eAd1	dogmaticky
chápaný	chápaný	k2eAgInSc1d1	chápaný
Smetanův	Smetanův	k2eAgInSc1d1	Smetanův
odkaz	odkaz	k1gInSc1	odkaz
stal	stát	k5eAaPmAgInS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
zbraní	zbraň	k1gFnSc7	zbraň
Nejedlého	Nejedlého	k2eAgFnSc2d1	Nejedlého
hudebněkritické	hudebněkritický	k2eAgFnSc2d1	hudebněkritický
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
českým	český	k2eAgMnPc3d1	český
tvůrcům	tvůrce	k1gMnPc3	tvůrce
vnucovala	vnucovat	k5eAaImAgFnS	vnucovat
jedinou	jediný	k2eAgFnSc4d1	jediná
závaznou	závazný	k2eAgFnSc4d1	závazná
pokrokovou	pokrokový	k2eAgFnSc4d1	pokroková
linii	linie	k1gFnSc4	linie
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
zařazováni	zařazován	k2eAgMnPc1d1	zařazován
např.	např.	kA	např.
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fibich	Fibich	k1gMnSc1	Fibich
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Foerster	Foerster	k1gMnSc1	Foerster
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Zich	Zich	k1gMnSc1	Zich
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Ostrčil	Ostrčil	k1gMnSc1	Ostrčil
<g/>
,	,	kIx,	,
Otakar	Otakar	k1gMnSc1	Otakar
Jeremiáš	Jeremiáš	k1gMnSc1	Jeremiáš
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
osobnosti	osobnost	k1gFnPc1	osobnost
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
<g/>
,	,	kIx,	,
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Novák	Novák	k1gMnSc1	Novák
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
neuznávala	uznávat	k5eNaImAgFnS	uznávat
a	a	k8xC	a
haněla	hanět	k5eAaImAgFnS	hanět
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Smetana	Smetana	k1gMnSc1	Smetana
zažil	zažít	k5eAaPmAgMnS	zažít
některá	některý	k3yIgNnPc4	některý
zklamání	zklamání	k1gNnPc4	zklamání
(	(	kIx(	(
<g/>
Dalibor	Dalibor	k1gMnSc1	Dalibor
<g/>
,	,	kIx,	,
Čertova	čertův	k2eAgFnSc1d1	Čertova
stěna	stěna	k1gFnSc1	stěna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
jeho	jeho	k3xOp3gFnPc2	jeho
skladeb	skladba	k1gFnPc2	skladba
ze	z	k7c2	z
zralého	zralý	k2eAgNnSc2d1	zralé
období	období	k1gNnSc2	období
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
ohlasem	ohlas	k1gInSc7	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
sice	sice	k8xC	sice
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
bezprostřednější	bezprostřední	k2eAgFnPc1d2	bezprostřednější
obliby	obliba	k1gFnPc1	obliba
než	než	k8xS	než
jiné	jiný	k2eAgFnPc1d1	jiná
(	(	kIx(	(
<g/>
úspěch	úspěch	k1gInSc1	úspěch
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
skladatele	skladatel	k1gMnSc2	skladatel
až	až	k6eAd1	až
zarážel	zarážet	k5eAaImAgMnS	zarážet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vcelku	vcelku	k6eAd1	vcelku
však	však	k9	však
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
na	na	k7c4	na
repertoár	repertoár	k1gInSc4	repertoár
ještě	ještě	k9	ještě
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
nebo	nebo	k8xC	nebo
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
a	a	k8xC	a
udržela	udržet	k5eAaPmAgFnS	udržet
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
hojně	hojně	k6eAd1	hojně
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
Smetanovy	Smetanův	k2eAgFnPc4d1	Smetanova
skladby	skladba	k1gFnPc4	skladba
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
díky	díky	k7c3	díky
využití	využití	k1gNnSc3	využití
jako	jako	k9	jako
různé	různý	k2eAgFnPc1d1	různá
znělky	znělka	k1gFnPc1	znělka
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
vedle	vedle	k7c2	vedle
mnoha	mnoho	k4c2	mnoho
čísel	číslo	k1gNnPc2	číslo
z	z	k7c2	z
Prodané	prodaný	k2eAgFnSc2d1	prodaná
nevěsty	nevěsta	k1gFnSc2	nevěsta
také	také	k9	také
některé	některý	k3yIgFnPc4	některý
melodie	melodie	k1gFnPc4	melodie
z	z	k7c2	z
ostatních	ostatní	k2eAgFnPc2d1	ostatní
oper	opera	k1gFnPc2	opera
(	(	kIx(	(
<g/>
sbor	sbor	k1gInSc1	sbor
"	"	kIx"	"
<g/>
Udeřila	udeřit	k5eAaPmAgFnS	udeřit
naše	náš	k3xOp1gFnSc1	náš
hodina	hodina	k1gFnSc1	hodina
<g/>
"	"	kIx"	"
z	z	k7c2	z
Braniborů	Branibor	k1gMnPc2	Branibor
<g/>
,	,	kIx,	,
ukolébavka	ukolébavka	k1gFnSc1	ukolébavka
"	"	kIx"	"
<g/>
Letěla	letět	k5eAaImAgFnS	letět
bělounká	bělounký	k2eAgFnSc1d1	bělounká
holubička	holubička	k1gFnSc1	holubička
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
z	z	k7c2	z
Hubičky	hubička	k1gFnSc2	hubička
<g/>
,	,	kIx,	,
fanfáry	fanfára	k1gFnPc1	fanfára
z	z	k7c2	z
Libuše	Libuše	k1gFnSc2	Libuše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
"	"	kIx"	"
z	z	k7c2	z
Mé	můj	k3xOp1gFnSc2	můj
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
"	"	kIx"	"
<g/>
Věno	věno	k1gNnSc1	věno
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Zpěvu	zpěv	k1gInSc2	zpěv
dar	dar	k1gInSc1	dar
všech	všecek	k3xTgNnPc2	všecek
srdcí	srdce	k1gNnPc2	srdce
vroucích	vroucí	k2eAgMnPc2d1	vroucí
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Nekamenujte	kamenovat	k5eNaImRp2nP	kamenovat
proroky	prorok	k1gMnPc4	prorok
<g/>
"	"	kIx"	"
z	z	k7c2	z
Večerních	večerní	k2eAgFnPc2d1	večerní
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
oblíbenosti	oblíbenost	k1gFnSc6	oblíbenost
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c6	o
oficiální	oficiální	k2eAgFnSc6d1	oficiální
podpoře	podpora	k1gFnSc6	podpora
smetanovského	smetanovský	k2eAgInSc2d1	smetanovský
kultu	kult	k1gInSc2	kult
v	v	k7c6	v
době	doba	k1gFnSc6	doba
komunismu	komunismus	k1gInSc2	komunismus
<g/>
)	)	kIx)	)
svědčí	svědčit	k5eAaImIp3nS	svědčit
četné	četný	k2eAgFnSc2d1	četná
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
<g/>
,	,	kIx,	,
gramofonové	gramofonový	k2eAgFnSc2d1	gramofonová
a	a	k8xC	a
CD	CD	kA	CD
záznamy	záznam	k1gInPc1	záznam
i	i	k9	i
filmové	filmový	k2eAgFnSc2d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc2d1	televizní
inscenace	inscenace	k1gFnSc2	inscenace
oper	opera	k1gFnPc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
Smetana	Smetana	k1gMnSc1	Smetana
pronikal	pronikat	k5eAaImAgMnS	pronikat
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
pomaleji	pomale	k6eAd2	pomale
než	než	k8xS	než
jeho	jeho	k3xOp3gMnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
současník	současník	k1gMnSc1	současník
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Lisztovu	Lisztův	k2eAgFnSc4d1	Lisztova
pomoc	pomoc	k1gFnSc4	pomoc
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc2	jeho
klavírní	klavírní	k2eAgFnSc2d1	klavírní
a	a	k8xC	a
symfonické	symfonický	k2eAgFnSc2d1	symfonická
skladby	skladba	k1gFnSc2	skladba
z	z	k7c2	z
40	[number]	k4	40
<g/>
.	.	kIx.	.
a	a	k8xC	a
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
nerozšířily	rozšířit	k5eNaPmAgFnP	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oper	opera	k1gFnPc2	opera
byly	být	k5eAaImAgInP	být
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
provedeny	provést	k5eAaPmNgInP	provést
jen	jen	k6eAd1	jen
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
1873	[number]	k4	1873
Záhřeb	Záhřeb	k1gInSc1	Záhřeb
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dvě	dva	k4xCgFnPc1	dva
vdovy	vdova	k1gFnPc1	vdova
(	(	kIx(	(
<g/>
Hamburk	Hamburk	k1gInSc1	Hamburk
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
kontextu	kontext	k1gInSc6	kontext
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
jeho	jeho	k3xOp3gNnSc4	jeho
komorní	komorní	k2eAgNnSc4d1	komorní
dílo	dílo	k1gNnSc4	dílo
(	(	kIx(	(
<g/>
první	první	k4xOgInSc4	první
smyčcový	smyčcový	k2eAgInSc4d1	smyčcový
kvartet	kvartet	k1gInSc4	kvartet
a	a	k8xC	a
trio	trio	k1gNnSc4	trio
g	g	kA	g
moll	moll	k1gNnSc4	moll
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
kvartet	kvartet	k1gInSc1	kvartet
Z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
zpopularizovalo	zpopularizovat	k5eAaPmAgNnS	zpopularizovat
zejména	zejména	k9	zejména
České	český	k2eAgNnSc1d1	české
kvarteto	kvarteto	k1gNnSc1	kvarteto
<g/>
.	.	kIx.	.
</s>
<s>
Průlom	průlom	k1gInSc1	průlom
pro	pro	k7c4	pro
opery	opera	k1gFnPc4	opera
představoval	představovat	k5eAaImAgInS	představovat
triumf	triumf	k1gInSc1	triumf
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
divadelní	divadelní	k2eAgFnSc6d1	divadelní
výstavě	výstava	k1gFnSc6	výstava
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
docílila	docílit	k5eAaPmAgFnS	docílit
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
<g/>
;	;	kIx,	;
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
zejména	zejména	k9	zejména
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvícím	mluvící	k2eAgInSc6d1	mluvící
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
též	též	k9	též
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
prostředí	prostředí	k1gNnSc6	prostředí
prosadily	prosadit	k5eAaPmAgFnP	prosadit
i	i	k9	i
další	další	k2eAgFnPc1d1	další
opery	opera	k1gFnPc1	opera
(	(	kIx(	(
<g/>
Dalibor	Dalibor	k1gMnSc1	Dalibor
<g/>
,	,	kIx,	,
Dvě	dva	k4xCgFnPc1	dva
vdovy	vdova	k1gFnPc1	vdova
<g/>
,	,	kIx,	,
Hubička	hubička	k1gFnSc1	hubička
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
prvními	první	k4xOgMnPc7	první
velkými	velký	k2eAgMnPc7d1	velký
propagátory	propagátor	k1gMnPc7	propagátor
byli	být	k5eAaImAgMnP	být
pěvci	pěvec	k1gMnPc1	pěvec
Ema	Ema	k1gFnSc1	Ema
Destinnová	Destinnová	k1gFnSc1	Destinnová
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Burian	Burian	k1gMnSc1	Burian
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
dirigent	dirigent	k1gMnSc1	dirigent
Gustav	Gustav	k1gMnSc1	Gustav
Mahler	Mahler	k1gMnSc1	Mahler
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
působištích	působiště	k1gNnPc6	působiště
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
a	a	k8xC	a
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
nejhranější	hraný	k2eAgFnSc7d3	nejhranější
českou	český	k2eAgFnSc7d1	Česká
operou	opera	k1gFnSc7	opera
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
poslední	poslední	k2eAgFnSc2d1	poslední
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jí	on	k3xPp3gFnSc3	on
konkurují	konkurovat	k5eAaImIp3nP	konkurovat
opery	opera	k1gFnPc1	opera
Janáčkovy	Janáčkův	k2eAgFnSc2d1	Janáčkova
(	(	kIx(	(
<g/>
Její	její	k3xOp3gFnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Dvořákova	Dvořákův	k2eAgFnSc1d1	Dvořákova
Rusalka	rusalka	k1gFnSc1	rusalka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
statistiky	statistika	k1gFnSc2	statistika
Operabase	Operabasa	k1gFnSc6	Operabasa
za	za	k7c4	za
roky	rok	k1gInPc4	rok
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
nejhranější	hraný	k2eAgFnSc7d3	nejhranější
českou	český	k2eAgFnSc7d1	Česká
operou	opera	k1gFnSc7	opera
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Má	mít	k5eAaImIp3nS	mít
vlast	vlast	k1gFnSc4	vlast
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
<g/>
,	,	kIx,	,
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc4d1	jiný
díky	díky	k7c3	díky
přízni	přízeň	k1gFnSc3	přízeň
slavného	slavný	k2eAgMnSc2d1	slavný
dirigenta	dirigent	k1gMnSc2	dirigent
Artura	Artur	k1gMnSc2	Artur
Toscaniniho	Toscanini	k1gMnSc2	Toscanini
<g/>
,	,	kIx,	,
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
světově	světově	k6eAd1	světově
nejhranějším	hraný	k2eAgInSc7d3	nejhranější
a	a	k8xC	a
nejnahrávanějším	nahrávaný	k2eAgInSc7d3	nahrávaný
Smetanovým	Smetanův	k2eAgInSc7d1	Smetanův
dílem	díl	k1gInSc7	díl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světovém	světový	k2eAgInSc6d1	světový
kontextu	kontext	k1gInSc6	kontext
však	však	k9	však
Smetana	Smetana	k1gMnSc1	Smetana
nikdy	nikdy	k6eAd1	nikdy
nedosáhl	dosáhnout	k5eNaPmAgMnS	dosáhnout
postavení	postavení	k1gNnSc4	postavení
přiznávaného	přiznávaný	k2eAgMnSc2d1	přiznávaný
Antonínu	Antonín	k1gMnSc6	Antonín
Dvořákovi	Dvořák	k1gMnSc6	Dvořák
nebo	nebo	k8xC	nebo
Leoši	Leoš	k1gMnSc6	Leoš
Janáčkovi	Janáček	k1gMnSc6	Janáček
<g/>
.	.	kIx.	.
</s>
<s>
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
dominantní	dominantní	k2eAgFnSc1d1	dominantní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
osobnost	osobnost	k1gFnSc1	osobnost
silně	silně	k6eAd1	silně
zapůsobila	zapůsobit	k5eAaPmAgFnS	zapůsobit
na	na	k7c4	na
současníky	současník	k1gMnPc4	současník
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
Antonínem	Antonín	k1gMnSc7	Antonín
Dvořákem	Dvořák	k1gMnSc7	Dvořák
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zakladatelská	zakladatelský	k2eAgFnSc1d1	zakladatelská
pozice	pozice	k1gFnSc1	pozice
a	a	k8xC	a
stálá	stálý	k2eAgFnSc1d1	stálá
přítomnost	přítomnost	k1gFnSc1	přítomnost
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
hudební	hudební	k2eAgFnSc6d1	hudební
scéně	scéna	k1gFnSc6	scéna
navíc	navíc	k6eAd1	navíc
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
nutila	nutit	k5eAaImAgFnS	nutit
všechny	všechen	k3xTgMnPc4	všechen
české	český	k2eAgMnPc4d1	český
skladatele	skladatel	k1gMnPc4	skladatel
se	se	k3xPyFc4	se
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
osobností	osobnost	k1gFnSc7	osobnost
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
oddaně	oddaně	k6eAd1	oddaně
hlásili	hlásit	k5eAaImAgMnP	hlásit
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Josef	Josef	k1gMnSc1	Josef
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Foerster	Foerster	k1gMnSc1	Foerster
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gNnSc3	on
vymezovali	vymezovat	k5eAaImAgMnP	vymezovat
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Specificky	specificky	k6eAd1	specificky
Prodaná	prodaný	k2eAgFnSc1d1	prodaná
nevěsta	nevěsta	k1gFnSc1	nevěsta
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
stala	stát	k5eAaPmAgFnS	stát
přímým	přímý	k2eAgInSc7d1	přímý
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
díla	dílo	k1gNnPc4	dílo
řady	řada	k1gFnSc2	řada
skladatelů	skladatel	k1gMnPc2	skladatel
<g/>
,	,	kIx,	,
od	od	k7c2	od
Blodka	Blodek	k1gMnSc2	Blodek
<g/>
,	,	kIx,	,
Šebora	Šebor	k1gMnSc2	Šebor
<g/>
,	,	kIx,	,
Rozkošného	rozkošný	k2eAgMnSc4d1	rozkošný
a	a	k8xC	a
Bendla	Bendla	k1gMnSc4	Bendla
přes	přes	k7c4	přes
Dvořáka	Dvořák	k1gMnSc4	Dvořák
až	až	k9	až
po	po	k7c6	po
třeba	třeba	k6eAd1	třeba
Antonína	Antonín	k1gMnSc2	Antonín
Foerstera	Foerster	k1gMnSc2	Foerster
nebo	nebo	k8xC	nebo
Jana	Jan	k1gMnSc2	Jan
Maláta	Malát	k1gMnSc2	Malát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
později	pozdě	k6eAd2	pozdě
vznikaly	vznikat	k5eAaImAgFnP	vznikat
opery	opera	k1gFnPc1	opera
ve	v	k7c6	v
smetanovské	smetanovský	k2eAgFnSc6d1	smetanovská
tradici	tradice	k1gFnSc6	tradice
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Weinbergerův	Weinbergerův	k2eAgInSc4d1	Weinbergerův
meziválečný	meziválečný	k2eAgInSc4d1	meziválečný
hit	hit	k1gInSc4	hit
Švanda	Švanda	k1gMnSc1	Švanda
dudák	dudák	k1gMnSc1	dudák
nebo	nebo	k8xC	nebo
nejhranější	hraný	k2eAgFnSc1d3	nejhranější
poválečná	poválečný	k2eAgFnSc1d1	poválečná
česká	český	k2eAgFnSc1d1	Česká
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Vojířová	Vojířová	k1gFnSc1	Vojířová
od	od	k7c2	od
Jiřího	Jiří	k1gMnSc2	Jiří
Pauera	Pauer	k1gMnSc2	Pauer
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovu	Smetanův	k2eAgFnSc4d1	Smetanova
osobnost	osobnost	k1gFnSc4	osobnost
reflektovaly	reflektovat	k5eAaImAgInP	reflektovat
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
obory	obor	k1gInPc1	obor
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Sochařské	sochařský	k2eAgInPc1d1	sochařský
a	a	k8xC	a
malířské	malířský	k2eAgInPc1d1	malířský
portréty	portrét	k1gInPc1	portrét
Smetany	smetana	k1gFnSc2	smetana
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
například	například	k6eAd1	například
Josef	Josef	k1gMnSc1	Josef
Václav	Václav	k1gMnSc1	Václav
Myslbek	Myslbek	k1gMnSc1	Myslbek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Štursa	Štursa	k1gFnSc1	Štursa
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Švabinský	Švabinský	k2eAgMnSc1d1	Švabinský
<g/>
,	,	kIx,	,
ze	z	k7c2	z
spisovatelů	spisovatel	k1gMnPc2	spisovatel
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
například	například	k6eAd1	například
Jaroslava	Jaroslav	k1gMnSc4	Jaroslav
Vrchlického	Vrchlický	k1gMnSc4	Vrchlický
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Machara	Machar	k1gMnSc2	Machar
nebo	nebo	k8xC	nebo
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Nezvala	Nezval	k1gMnSc2	Nezval
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgInPc1d3	nejznámější
literární	literární	k2eAgFnSc7d1	literární
reflexí	reflexe	k1gFnSc7	reflexe
je	být	k5eAaImIp3nS	být
báseň	báseň	k1gFnSc1	báseň
Antonína	Antonín	k1gMnSc2	Antonín
Sovy	Sova	k1gMnSc2	Sova
<g/>
.	.	kIx.	.
</s>
<s>
Těžké	těžký	k2eAgFnPc1d1	těžká
poslední	poslední	k2eAgFnPc1d1	poslední
chvíle	chvíle	k1gFnPc1	chvíle
skladatelova	skladatelův	k2eAgInSc2d1	skladatelův
života	život	k1gInSc2	život
popsal	popsat	k5eAaPmAgMnS	popsat
Antonín	Antonín	k1gMnSc1	Antonín
Sova	Sova	k1gMnSc1	Sova
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Smetanovo	Smetanův	k2eAgNnSc4d1	Smetanovo
kvartetto	kvartetto	k1gNnSc4	kvartetto
"	"	kIx"	"
<g/>
Z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Smyčcový	smyčcový	k2eAgInSc1d1	smyčcový
kvartet	kvartet	k1gInSc1	kvartet
e	e	k0	e
moll	moll	k1gNnSc6	moll
"	"	kIx"	"
<g/>
Z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
"	"	kIx"	"
posloužil	posloužit	k5eAaPmAgInS	posloužit
jakožto	jakožto	k8xS	jakožto
název	název	k1gInSc1	název
pro	pro	k7c4	pro
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
životopisný	životopisný	k2eAgInSc4d1	životopisný
film	film	k1gInSc4	film
Z	z	k7c2	z
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
režiséra	režisér	k1gMnSc2	režisér
Václava	Václav	k1gMnSc2	Václav
Kršky	Krška	k1gMnSc2	Krška
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Högerem	Höger	k1gMnSc7	Höger
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgFnPc2d1	jiná
historických	historický	k2eAgFnPc2d1	historická
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
epizodně	epizodně	k6eAd1	epizodně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
další	další	k2eAgMnPc1d1	další
známí	známý	k2eAgMnPc1d1	známý
hudební	hudební	k2eAgMnPc1d1	hudební
skladatelé	skladatel	k1gMnPc1	skladatel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
a	a	k8xC	a
Antonín	Antonín	k1gMnSc1	Antonín
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
,	,	kIx,	,
ze	z	k7c2	z
spisovatelů	spisovatel	k1gMnPc2	spisovatel
například	například	k6eAd1	například
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
soustavné	soustavný	k2eAgFnSc3d1	soustavná
pozornosti	pozornost	k1gFnSc3	pozornost
hudební	hudební	k2eAgFnSc2d1	hudební
vědy	věda	k1gFnSc2	věda
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
dílo	dílo	k1gNnSc4	dílo
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	smetana	k1gFnSc2	smetana
-	-	kIx~	-
jako	jako	k8xC	jako
jediného	jediný	k2eAgMnSc2d1	jediný
českého	český	k2eAgMnSc2d1	český
skladatele	skladatel	k1gMnSc2	skladatel
-	-	kIx~	-
dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
kritické	kritický	k2eAgFnSc6d1	kritická
edici	edice	k1gFnSc6	edice
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
určitými	určitý	k2eAgInPc7d1	určitý
metodickými	metodický	k2eAgInPc7d1	metodický
nedostatky	nedostatek	k1gInPc7	nedostatek
<g/>
.	.	kIx.	.
</s>
<s>
Chybí	chybět	k5eAaImIp3nS	chybět
pouze	pouze	k6eAd1	pouze
zlomek	zlomek	k1gInSc4	zlomek
Violy	Viola	k1gFnSc2	Viola
<g/>
,	,	kIx,	,
klavírní	klavírní	k2eAgInSc1d1	klavírní
cyklus	cyklus	k1gInSc1	cyklus
Rê	Rê	k1gFnSc2	Rê
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
řady	řada	k1gFnPc1	řada
Českých	český	k2eAgInPc2d1	český
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
z	z	k7c2	z
kompozičních	kompoziční	k2eAgNnPc2d1	kompoziční
cvičení	cvičení	k1gNnPc2	cvičení
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1843	[number]	k4	1843
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
jen	jen	k9	jen
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Opery	opera	k1gFnPc1	opera
<g/>
,	,	kIx,	,
sbory	sbor	k1gInPc1	sbor
a	a	k8xC	a
orchestrální	orchestrální	k2eAgFnPc1d1	orchestrální
a	a	k8xC	a
komorní	komorní	k2eAgFnPc1d1	komorní
skladby	skladba	k1gFnPc1	skladba
vyšly	vyjít	k5eAaPmAgFnP	vyjít
v	v	k7c6	v
patnáctisvazkové	patnáctisvazkový	k2eAgFnSc6d1	patnáctisvazkový
edici	edice	k1gFnSc6	edice
Studijní	studijní	k2eAgNnSc4d1	studijní
vydání	vydání	k1gNnSc4	vydání
děl	dělo	k1gNnPc2	dělo
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
-	-	kIx~	-
<g/>
77	[number]	k4	77
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
skladby	skladba	k1gFnPc4	skladba
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
v	v	k7c6	v
pětisvazkovém	pětisvazkový	k2eAgNnSc6d1	pětisvazkové
Klavírním	klavírní	k2eAgNnSc6d1	klavírní
díle	dílo	k1gNnSc6	dílo
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
vyšly	vyjít	k5eAaPmAgFnP	vyjít
písně	píseň	k1gFnPc1	píseň
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
a	a	k8xC	a
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
mládí	mládí	k1gNnSc2	mládí
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1843	[number]	k4	1843
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Formou	forma	k1gFnSc7	forma
faksimile	faksimile	k1gNnSc2	faksimile
vyšel	vyjít	k5eAaPmAgInS	vyjít
Smetanův	Smetanův	k2eAgInSc1d1	Smetanův
zápisník	zápisník	k1gInSc1	zápisník
motivů	motiv	k1gInPc2	motiv
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydány	vydat	k5eAaPmNgFnP	vydat
byly	být	k5eAaImAgFnP	být
i	i	k8xC	i
Smetanovy	Smetanův	k2eAgFnPc1d1	Smetanova
kresby	kresba	k1gFnPc1	kresba
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
i	i	k8xC	i
jeho	jeho	k3xOp3gInPc4	jeho
novinové	novinový	k2eAgInPc4d1	novinový
články	článek	k1gInPc4	článek
a	a	k8xC	a
kritiky	kritika	k1gFnSc2	kritika
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovy	Smetanův	k2eAgFnPc1d1	Smetanova
kresby	kresba	k1gFnPc1	kresba
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kresbami	kresba	k1gFnPc7	kresba
dalších	další	k2eAgMnPc2d1	další
členů	člen	k1gMnPc2	člen
jeho	jeho	k3xOp3gFnSc2	jeho
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
manželky	manželka	k1gFnSc2	manželka
Betty	Betty	k1gFnSc2	Betty
Smetanové	Smetanová	k1gFnSc2	Smetanová
<g/>
,	,	kIx,	,
vnuka	vnuk	k1gMnSc2	vnuk
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Schwarze	Schwarz	k1gMnSc2	Schwarz
<g/>
)	)	kIx)	)
námětem	námět	k1gInSc7	námět
výstavy	výstava	k1gFnSc2	výstava
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
u	u	k7c2	u
Smetanů	Smetana	k1gMnPc2	Smetana
malovalo	malovat	k5eAaImAgNnS	malovat
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
nebyl	být	k5eNaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
tematický	tematický	k2eAgInSc1d1	tematický
katalog	katalog	k1gInSc1	katalog
Smetanových	Smetanových	k2eAgFnPc2d1	Smetanových
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
dokončený	dokončený	k2eAgInSc1d1	dokončený
v	v	k7c6	v
rukopise	rukopis	k1gInSc6	rukopis
Jiřím	Jiří	k1gMnSc7	Jiří
Berkovcem	Berkovec	k1gMnSc7	Berkovec
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
starší	starý	k2eAgFnSc2d2	starší
práce	práce	k1gFnSc2	práce
Františka	František	k1gMnSc2	František
Bartoše	Bartoš	k1gMnSc2	Bartoš
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
neúplně	úplně	k6eNd1	úplně
a	a	k8xC	a
roztroušeně	roztroušeně	k6eAd1	roztroušeně
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
korespondence	korespondence	k1gFnSc1	korespondence
a	a	k8xC	a
úryvky	úryvek	k1gInPc1	úryvek
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
deníku	deník	k1gInSc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovou	Smetanův	k2eAgFnSc7d1	Smetanova
osobností	osobnost	k1gFnSc7	osobnost
a	a	k8xC	a
hudbou	hudba	k1gFnSc7	hudba
se	se	k3xPyFc4	se
vědecky	vědecky	k6eAd1	vědecky
zabývali	zabývat	k5eAaImAgMnP	zabývat
například	například	k6eAd1	například
Otakar	Otakar	k1gMnSc1	Otakar
Hostinský	hostinský	k1gMnSc1	hostinský
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Helfert	Helfert	k1gMnSc1	Helfert
<g/>
,	,	kIx,	,
Julien	Julien	k2eAgMnSc1d1	Julien
Tiersot	Tiersot	k1gMnSc1	Tiersot
<g/>
,	,	kIx,	,
Mirko	Mirko	k1gMnSc1	Mirko
Očadlík	Očadlík	k1gMnSc1	Očadlík
<g/>
,	,	kIx,	,
Přemysl	Přemysl	k1gMnSc1	Přemysl
Pražák	Pražák	k1gMnSc1	Pražák
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
Large	Larg	k1gFnSc2	Larg
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Clapham	Clapham	k1gInSc1	Clapham
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Janeček	Janeček	k1gMnSc1	Janeček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Smolka	Smolka	k1gMnSc1	Smolka
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
Séquardtová	Séquardtová	k1gFnSc1	Séquardtová
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Holzknecht	Holzknecht	k1gMnSc1	Holzknecht
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
například	například	k6eAd1	například
John	John	k1gMnSc1	John
Tyrrell	Tyrrell	k1gMnSc1	Tyrrell
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Mojžíšová	Mojžíšová	k1gFnSc1	Mojžíšová
<g/>
,	,	kIx,	,
Marta	Marta	k1gFnSc1	Marta
Ottlová	Ottlová	k1gFnSc1	Ottlová
nebo	nebo	k8xC	nebo
Milan	Milan	k1gMnSc1	Milan
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
Václava	Václav	k1gMnSc4	Václav
Vladimíra	Vladimír	k1gMnSc4	Vladimír
Zeleného	Zelený	k1gMnSc4	Zelený
založeno	založen	k2eAgNnSc1d1	založeno
Družstvo	družstvo	k1gNnSc1	družstvo
ctitelů	ctitel	k1gMnPc2	ctitel
Smetanových	Smetanových	k2eAgMnPc2d1	Smetanových
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
prvním	první	k4xOgInSc7	první
činem	čin	k1gInSc7	čin
bylo	být	k5eAaImAgNnS	být
vydání	vydání	k1gNnSc1	vydání
klavírního	klavírní	k2eAgInSc2d1	klavírní
výtahu	výtah	k1gInSc2	výtah
Dalibora	Dalibor	k1gMnSc2	Dalibor
ke	k	k7c3	k
skladatelovým	skladatelův	k2eAgFnPc3d1	skladatelova
šedesátinám	šedesátina	k1gFnPc3	šedesátina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
pak	pak	k6eAd1	pak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Sbor	sbor	k1gInSc1	sbor
pro	pro	k7c4	pro
postavení	postavení	k1gNnSc4	postavení
Smetanova	Smetanův	k2eAgInSc2d1	Smetanův
pomníku	pomník	k1gInSc2	pomník
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
brzy	brzy	k6eAd1	brzy
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
aktivitu	aktivita	k1gFnSc4	aktivita
na	na	k7c4	na
výstavní	výstavní	k2eAgFnSc4d1	výstavní
<g/>
,	,	kIx,	,
sběratelskou	sběratelský	k2eAgFnSc4d1	sběratelská
a	a	k8xC	a
vydavatelskou	vydavatelský	k2eAgFnSc4d1	vydavatelská
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
se	se	k3xPyFc4	se
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
(	(	kIx(	(
<g/>
dosud	dosud	k6eAd1	dosud
existující	existující	k2eAgInSc1d1	existující
<g/>
)	)	kIx)	)
Společnost	společnost	k1gFnSc1	společnost
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jím	jíst	k5eAaImIp1nS	jíst
shromážděných	shromážděný	k2eAgFnPc2d1	shromážděná
sbírek	sbírka	k1gFnPc2	sbírka
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
Muzeum	muzeum	k1gNnSc1	muzeum
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Novotného	Novotného	k2eAgFnSc6d1	Novotného
lávce	lávka	k1gFnSc6	lávka
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
postátněné	postátněný	k2eAgInPc1d1	postátněný
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
spravované	spravovaný	k2eAgFnSc2d1	spravovaná
Národním	národní	k2eAgNnSc7d1	národní
muzeem	muzeum	k1gNnSc7	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
spravuje	spravovat	k5eAaImIp3nS	spravovat
též	též	k9	též
Památník	památník	k1gInSc1	památník
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
v	v	k7c6	v
Jabkenicích	Jabkenice	k1gFnPc6	Jabkenice
<g/>
,	,	kIx,	,
odkoupený	odkoupený	k2eAgInSc1d1	odkoupený
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
spolkem	spolek	k1gInSc7	spolek
Dědictví	dědictví	k1gNnSc2	dědictví
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
existuje	existovat	k5eAaImIp3nS	existovat
též	též	k9	též
muzeum	muzeum	k1gNnSc4	muzeum
Rodný	rodný	k2eAgInSc1d1	rodný
byt	byt	k1gInSc1	byt
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
expozice	expozice	k1gFnSc2	expozice
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
pak	pak	k9	pak
Památník	památník	k1gInSc1	památník
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
na	na	k7c4	na
Lamberku	Lamberka	k1gFnSc4	Lamberka
u	u	k7c2	u
Obříství	Obříství	k1gNnSc2	Obříství
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
smetanovská	smetanovský	k2eAgFnSc1d1	smetanovská
expozice	expozice	k1gFnSc1	expozice
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Růžkových	Růžkových	k2eAgFnPc6d1	Růžkových
Lhoticích	Lhotice	k1gFnPc6	Lhotice
<g/>
.	.	kIx.	.
</s>
<s>
Expozice	expozice	k1gFnSc1	expozice
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
po	po	k7c6	po
restituci	restituce	k1gFnSc6	restituce
zámku	zámek	k1gInSc2	zámek
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
existuje	existovat	k5eAaImIp3nS	existovat
též	též	k9	též
expozice	expozice	k1gFnSc1	expozice
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
burzy	burza	k1gFnSc2	burza
v	v	k7c6	v
Göteborgu	Göteborg	k1gInSc6	Göteborg
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
existuje	existovat	k5eAaImIp3nS	existovat
Muzeum	muzeum	k1gNnSc4	muzeum
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	smetana	k1gFnSc2	smetana
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Týn	Týn	k1gInSc4	Týn
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c2	mnoho
místech	místo	k1gNnPc6	místo
byly	být	k5eAaImAgFnP	být
Smetanovi	Smetana	k1gMnSc3	Smetana
postaveny	postaven	k2eAgMnPc4d1	postaven
pomníky	pomník	k1gInPc4	pomník
nebo	nebo	k8xC	nebo
zasazeny	zasazen	k2eAgFnPc4d1	zasazena
pamětní	pamětní	k2eAgFnPc4d1	pamětní
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
pamětní	pamětní	k2eAgFnPc1d1	pamětní
desky	deska	k1gFnPc1	deska
byly	být	k5eAaImAgFnP	být
zasazeny	zasadit	k5eAaPmNgFnP	zasadit
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jabkenicích	Jabkenice	k1gFnPc6	Jabkenice
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
pomník	pomník	k1gInSc1	pomník
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
Hořicích	Hořice	k1gFnPc6	Hořice
roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
českých	český	k2eAgNnPc6d1	české
městech	město	k1gNnPc6	město
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
některé	některý	k3yIgNnSc1	některý
veřejné	veřejný	k2eAgNnSc1d1	veřejné
prostranství	prostranství	k1gNnSc1	prostranství
<g/>
;	;	kIx,	;
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
Smetanovo	Smetanův	k2eAgNnSc4d1	Smetanovo
nábřeží	nábřeží	k1gNnSc4	nábřeží
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
od	od	k7c2	od
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
ke	k	k7c3	k
Smetanovu	Smetanův	k2eAgNnSc3d1	Smetanovo
muzeu	muzeum	k1gNnSc3	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
podobizna	podobizna	k1gFnSc1	podobizna
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
na	na	k7c6	na
československé	československý	k2eAgFnSc6d1	Československá
tisícikoruně	tisícikoruna	k1gFnSc6	tisícikoruna
(	(	kIx(	(
<g/>
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
93	[number]	k4	93
<g/>
)	)	kIx)	)
a	a	k8xC	a
pětitisícikoruně	pětitisícikoruna	k1gFnSc6	pětitisícikoruna
(	(	kIx(	(
<g/>
v	v	k7c6	v
oběhu	oběh	k1gInSc6	oběh
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
53	[number]	k4	53
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
nesla	nést	k5eAaImAgFnS	nést
Smetanovo	Smetanův	k2eAgNnSc4d1	Smetanovo
jméno	jméno	k1gNnSc4	jméno
budova	budova	k1gFnSc1	budova
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Státní	státní	k2eAgFnSc2d1	státní
opery	opera	k1gFnSc2	opera
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
koncertní	koncertní	k2eAgInSc4d1	koncertní
prostor	prostor	k1gInSc4	prostor
pražského	pražský	k2eAgInSc2d1	pražský
Obecního	obecní	k2eAgInSc2d1	obecní
domu	dům	k1gInSc2	dům
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc4	jméno
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
síň	síň	k1gFnSc1	síň
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
začíná	začínat	k5eAaImIp3nS	začínat
každoročně	každoročně	k6eAd1	každoročně
ve	v	k7c4	v
Smetanův	Smetanův	k2eAgInSc4d1	Smetanův
úmrtní	úmrtní	k2eAgInSc4d1	úmrtní
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
jeho	jeho	k3xOp3gFnSc1	jeho
Mou	můj	k3xOp1gFnSc7	můj
vlastí	vlast	k1gFnSc7	vlast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skladatelově	skladatelův	k2eAgNnSc6d1	skladatelovo
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
festival	festival	k1gInSc1	festival
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
,	,	kIx,	,
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
smetanovská	smetanovský	k2eAgFnSc1d1	smetanovská
klavírní	klavírní	k2eAgFnSc1d1	klavírní
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
řada	řada	k1gFnSc1	řada
pěveckých	pěvecký	k2eAgInPc2d1	pěvecký
sborů	sbor	k1gInPc2	sbor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Frýdku-Místku	Frýdku-Místek	k1gInSc6	Frýdku-Místek
<g/>
,	,	kIx,	,
Jindřichově	Jindřichův	k2eAgInSc6d1	Jindřichův
Hradci	Hradec	k1gInSc6	Hradec
<g/>
,	,	kIx,	,
Telči	Telč	k1gFnSc6	Telč
<g/>
,	,	kIx,	,
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
,	,	kIx,	,
Jičíně	Jičín	k1gInSc6	Jičín
nebo	nebo	k8xC	nebo
Hulíně	Hulín	k1gInSc6	Hulín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Smetanovo	Smetanův	k2eAgNnSc1d1	Smetanovo
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
též	též	k9	též
planetka	planetka	k1gFnSc1	planetka
č.	č.	k?	č.
2047	[number]	k4	2047
<g/>
.	.	kIx.	.
</s>
