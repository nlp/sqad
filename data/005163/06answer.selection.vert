<s>
Galileovy	Galileův	k2eAgInPc1d1	Galileův
měsíce	měsíc	k1gInPc1	měsíc
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Galileovské	Galileovský	k2eAgInPc4d1	Galileovský
měsíce	měsíc	k1gInPc4	měsíc
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
největší	veliký	k2eAgInSc4d3	veliký
a	a	k8xC	a
nejjasnější	jasný	k2eAgInSc4d3	nejjasnější
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
67	[number]	k4	67
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
