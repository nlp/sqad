<s>
Galileovy	Galileův	k2eAgInPc1d1	Galileův
měsíce	měsíc	k1gInPc1	měsíc
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Galileovské	Galileovský	k2eAgInPc4d1	Galileovský
měsíce	měsíc	k1gInPc4	měsíc
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
největší	veliký	k2eAgInSc4d3	veliký
a	a	k8xC	a
nejjasnější	jasný	k2eAgInSc4d3	nejjasnější
z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
67	[number]	k4	67
Jupiterových	Jupiterův	k2eAgInPc2d1	Jupiterův
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
jména	jméno	k1gNnPc1	jméno
inspirovaná	inspirovaný	k2eAgNnPc1d1	inspirované
postavami	postava	k1gFnPc7	postava
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
:	:	kIx,	:
Io	Io	k1gFnSc1	Io
<g/>
,	,	kIx,	,
Europa	Europa	k1gFnSc1	Europa
<g/>
,	,	kIx,	,
Ganymed	Ganymed	k1gMnSc1	Ganymed
a	a	k8xC	a
Callisto	Callista	k1gMnSc5	Callista
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
vůbec	vůbec	k9	vůbec
první	první	k4xOgNnPc1	první
nebeská	nebeský	k2eAgNnPc1d1	nebeské
tělesa	těleso	k1gNnPc1	těleso
objevená	objevený	k2eAgNnPc1d1	objevené
dalekohledem	dalekohled	k1gInSc7	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
pozorování	pozorování	k1gNnPc4	pozorování
možná	možná	k9	možná
provedl	provést	k5eAaPmAgInS	provést
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1609	[number]	k4	1609
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
však	však	k9	však
publikoval	publikovat	k5eAaBmAgMnS	publikovat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1614	[number]	k4	1614
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
Mundus	Mundus	k1gMnSc1	Mundus
Iovialis	Iovialis	k1gFnSc2	Iovialis
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Jupiterovský	Jupiterovský	k2eAgInSc1d1	Jupiterovský
svět	svět	k1gInSc1	svět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gFnSc5	Galile
je	on	k3xPp3gNnPc4	on
poprvé	poprvé	k6eAd1	poprvé
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1610	[number]	k4	1610
a	a	k8xC	a
považoval	považovat	k5eAaImAgMnS	považovat
je	být	k5eAaImIp3nS	být
zpočátku	zpočátku	k6eAd1	zpočátku
za	za	k7c4	za
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
ale	ale	k8xC	ale
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
tělesa	těleso	k1gNnPc4	těleso
obíhající	obíhající	k2eAgNnPc4d1	obíhající
kolem	kolem	k7c2	kolem
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
pojednání	pojednání	k1gNnSc6	pojednání
Sidereus	Sidereus	k1gMnSc1	Sidereus
Nuncius	nuncius	k1gMnSc1	nuncius
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Hvězdný	hvězdný	k2eAgMnSc1d1	hvězdný
posel	posel	k1gMnSc1	posel
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
měl	mít	k5eAaImAgInS	mít
pro	pro	k7c4	pro
astronomii	astronomie	k1gFnSc4	astronomie
veliký	veliký	k2eAgInSc4d1	veliký
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
nebeská	nebeský	k2eAgNnPc1d1	nebeské
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
neobíhají	obíhat	k5eNaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
znamenal	znamenat	k5eAaImAgInS	znamenat
velkou	velký	k2eAgFnSc4d1	velká
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
uznání	uznání	k1gNnSc4	uznání
heliocentrické	heliocentrický	k2eAgFnSc2d1	heliocentrická
nauky	nauka	k1gFnSc2	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Galilei	Galilei	k6eAd1	Galilei
původně	původně	k6eAd1	původně
nazval	nazvat	k5eAaBmAgMnS	nazvat
tyto	tento	k3xDgInPc4	tento
měsíce	měsíc	k1gInPc4	měsíc
Cosimovy	Cosimův	k2eAgFnSc2d1	Cosimův
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
Cosmica	Cosmic	k2eAgFnSc1d1	Cosmic
Sidera	Sidera	k1gFnSc1	Sidera
<g/>
)	)	kIx)	)
dle	dle	k7c2	dle
jména	jméno	k1gNnSc2	jméno
svého	svůj	k3xOyFgMnSc2	svůj
mecenáše	mecenáš	k1gMnSc2	mecenáš
a	a	k8xC	a
ochránce	ochránce	k1gMnSc2	ochránce
<g/>
,	,	kIx,	,
toskánského	toskánský	k2eAgMnSc2d1	toskánský
velkovévody	velkovévoda	k1gMnSc2	velkovévoda
Cosima	Cosimum	k1gNnSc2	Cosimum
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
–	–	k?	–
<g/>
1621	[number]	k4	1621
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
vévodovo	vévodův	k2eAgNnSc4d1	vévodovo
přání	přání	k1gNnSc4	přání
je	být	k5eAaImIp3nS	být
posléze	posléze	k6eAd1	posléze
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
obecněji	obecně	k6eAd2	obecně
Medicejské	Medicejský	k2eAgFnPc4d1	Medicejská
hvězdy	hvězda	k1gFnPc4	hvězda
(	(	kIx(	(
<g/>
Sidera	Sider	k1gMnSc2	Sider
Medicea	Mediceus	k1gMnSc2	Mediceus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
všech	všecek	k3xTgMnPc2	všecek
čtyř	čtyři	k4xCgMnPc2	čtyři
tehdy	tehdy	k6eAd1	tehdy
žijících	žijící	k2eAgMnPc2d1	žijící
bratrů	bratr	k1gMnPc2	bratr
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Medicejských	Medicejský	k2eAgMnPc2d1	Medicejský
<g/>
:	:	kIx,	:
Cosima	Cosim	k1gMnSc2	Cosim
<g/>
,	,	kIx,	,
Francesca	Francescus	k1gMnSc2	Francescus
<g/>
,	,	kIx,	,
Carla	Carl	k1gMnSc2	Carl
a	a	k8xC	a
Lorenza	Lorenza	k?	Lorenza
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
"	"	kIx"	"
<g/>
Medicejské	Medicejský	k2eAgFnPc4d1	Medicejská
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
"	"	kIx"	"
pak	pak	k6eAd1	pak
Galilei	Galilee	k1gFnSc4	Galilee
označoval	označovat	k5eAaImAgInS	označovat
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
I	i	k8xC	i
<g/>
–	–	k?	–
<g/>
IV	IV	kA	IV
(	(	kIx(	(
<g/>
Jupiter	Jupiter	k1gMnSc1	Jupiter
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
Io	Io	k1gMnSc1	Io
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
Europa	Europa	k1gFnSc1	Europa
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
Ganymed	Ganymed	k1gMnSc1	Ganymed
<g/>
,	,	kIx,	,
Jupiter	Jupiter	k1gMnSc1	Jupiter
IV	IV	kA	IV
<g/>
/	/	kIx~	/
<g/>
Callisto	Callista	k1gMnSc5	Callista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
se	se	k3xPyFc4	se
užívalo	užívat	k5eAaImAgNnS	užívat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začly	začly	k?	začly
být	být	k5eAaImF	být
objevovány	objevován	k2eAgInPc4d1	objevován
další	další	k2eAgInPc4d1	další
měsíce	měsíc	k1gInPc4	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
dosavadní	dosavadní	k2eAgNnSc1d1	dosavadní
číslování	číslování	k1gNnSc1	číslování
postupně	postupně	k6eAd1	postupně
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
zmatkům	zmatek	k1gInPc3	zmatek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
pojmenování	pojmenování	k1gNnSc1	pojmenování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
<g/>
.	.	kIx.	.
</s>
<s>
KLEZCEK	KLEZCEK	kA	KLEZCEK
<g/>
,	,	kIx,	,
Josip	Josip	k1gInSc1	Josip
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
906	[number]	k4	906
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
131	[number]	k4	131
<g/>
.	.	kIx.	.
</s>
<s>
Jupiterovy	Jupiterův	k2eAgInPc4d1	Jupiterův
měsíce	měsíc	k1gInPc4	měsíc
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gFnPc1	Galile
Seznam	seznam	k1gInSc4	seznam
astronomů	astronom	k1gMnPc2	astronom
</s>
