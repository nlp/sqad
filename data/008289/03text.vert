<p>
<s>
Kořist	kořist	k1gFnSc1	kořist
tygra	tygr	k1gMnSc2	tygr
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Tiger	Tiger	k1gMnSc1	Tiger
Kill	Kill	k1gMnSc1	Kill
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
televize	televize	k1gFnSc2	televize
BBC	BBC	kA	BBC
ze	z	k7c2	z
série	série	k1gFnSc2	série
Svět	svět	k1gInSc1	svět
přírody	příroda	k1gFnSc2	příroda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Simon	Simon	k1gMnSc1	Simon
King	King	k1gMnSc1	King
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
tygří	tygří	k2eAgFnSc2d1	tygří
rezervace	rezervace	k1gFnSc2	rezervace
Bandavgarh	Bandavgarha	k1gFnPc2	Bandavgarha
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
kameramanem	kameraman	k1gMnSc7	kameraman
Alphonsem	Alphons	k1gMnSc7	Alphons
Royem	Roy	k1gMnSc7	Roy
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
natáčel	natáčet	k5eAaImAgMnS	natáčet
divoké	divoký	k2eAgMnPc4d1	divoký
tygry	tygr	k1gMnPc4	tygr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
poprvé	poprvé	k6eAd1	poprvé
vysílán	vysílat	k5eAaImNgInS	vysílat
na	na	k7c6	na
Viasat	Viasat	k1gFnSc6	Viasat
Nature	Natur	k1gMnSc5	Natur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Kořist	kořist	k1gFnSc1	kořist
tygra	tygr	k1gMnSc2	tygr
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kořist	kořist	k1gFnSc1	kořist
tygra	tygr	k1gMnSc2	tygr
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Viasat	Viasat	k5eAaImF	Viasat
Nature	Natur	k1gInSc5	Natur
</s>
</p>
