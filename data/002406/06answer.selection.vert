<s>
Černý	černý	k2eAgInSc1d1	černý
kašel	kašel	k1gInSc1	kašel
nebo	nebo	k8xC	nebo
dávivý	dávivý	k2eAgInSc1d1	dávivý
kašel	kašel	k1gInSc1	kašel
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
pertussis	pertussis	k1gInSc1	pertussis
<g/>
,	,	kIx,	,
počeštěně	počeštěně	k6eAd1	počeštěně
někdy	někdy	k6eAd1	někdy
pertuse	pertuse	k1gFnSc1	pertuse
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
infekční	infekční	k2eAgNnSc1d1	infekční
bakteriální	bakteriální	k2eAgNnSc1d1	bakteriální
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
bakterie	bakterie	k1gFnSc1	bakterie
Bordetella	Bordetella	k1gFnSc1	Bordetella
pertussis	pertussis	k1gFnSc1	pertussis
<g/>
.	.	kIx.	.
</s>
