<s>
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
fakultaAkademie	fakultaAkademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Budova	budova	k1gFnSc1
DAMU	DAMU	kA
v	v	k7c6
Karlově	Karlův	k2eAgNnSc6d1
uliciVedení	uliciVedení	k1gNnSc6
fakulty	fakulta	k1gFnSc2
Děkanka	děkanka	k1gFnSc1
</s>
<s>
MgA.	MgA.	k?
et	et	k?
Mgr.	Mgr.	kA
Doubravka	Doubravka	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
Proděkan	proděkan	k1gMnSc1
</s>
<s>
doc.	doc.	kA
Mgr.	Mgr.	kA
Jaroslav	Jaroslav	k1gMnSc1
Provazník	Provazník	k1gMnSc1
Proděkan	proděkan	k1gMnSc1
</s>
<s>
doc.	doc.	kA
MgA.	MgA.	k1gMnSc1
Jakub	Jakub	k1gMnSc1
Korčák	Korčák	k1gMnSc1
Proděkan	proděkan	k1gMnSc1
</s>
<s>
doc.	doc.	kA
Mgr.	Mgr.	kA
Marek	Marek	k1gMnSc1
Bečka	Bečka	k1gMnSc1
Tajemnice	tajemnice	k1gFnSc1
</s>
<s>
Ing.	ing.	kA
Iva	Iva	k1gFnSc1
Štveráková	Štveráková	k1gFnSc1
Statistické	statistický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Katedry	katedra	k1gFnSc2
</s>
<s>
7	#num#	k4
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kontaktní	kontaktní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
5	#num#	k4
<g/>
′	′	k?
<g/>
9,6	9,6	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
2	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
www.damu.cz	www.damu.cz	k1gInSc1
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
(	(	kIx(
<g/>
DAMU	DAMU	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
tří	tři	k4xCgFnPc2
fakult	fakulta	k1gFnPc2
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
DAMU	DAMU	kA
<g/>
,	,	kIx,
FAMU	FAMU	kA
<g/>
,	,	kIx,
HAMU	HAMU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
fakultu	fakulta	k1gFnSc4
zaměřenou	zaměřený	k2eAgFnSc4d1
na	na	k7c4
výchovu	výchova	k1gFnSc4
umělců	umělec	k1gMnPc2
divadelních	divadelní	k2eAgInPc2d1
oborů	obor	k1gInPc2
(	(	kIx(
<g/>
herců	herec	k1gMnPc2
<g/>
,	,	kIx,
režisérů	režisér	k1gMnPc2
<g/>
,	,	kIx,
dramaturgů	dramaturg	k1gMnPc2
<g/>
,	,	kIx,
scénografů	scénograf	k1gMnPc2
<g/>
,	,	kIx,
manažerů	manažer	k1gMnPc2
<g/>
,	,	kIx,
producentů	producent	k1gMnPc2
ad	ad	k7c4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
fakulty	fakulta	k1gFnSc2
je	být	k5eAaImIp3nS
také	také	k9
studentské	studentský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
DISK	disk	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
studenti	student	k1gMnPc1
sbírají	sbírat	k5eAaImIp3nP
praktické	praktický	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
AMU	AMU	kA
vznikla	vzniknout	k5eAaPmAgFnS
bezprostředně	bezprostředně	k6eAd1
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
na	na	k7c6
základě	základ	k1gInSc6
potřeby	potřeba	k1gFnSc2
vychovávat	vychovávat	k5eAaImF
vzdělané	vzdělaný	k2eAgMnPc4d1
divadelní	divadelní	k2eAgMnPc4d1
umělce	umělec	k1gMnPc4
<g/>
,	,	kIx,
umělecké	umělecký	k2eAgFnPc1d1
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
schopny	schopen	k2eAgFnPc1d1
vnést	vnést	k5eAaPmF
do	do	k7c2
praxe	praxe	k1gFnSc2
vědomí	vědomí	k1gNnSc2
uměleckých	umělecký	k2eAgInPc2d1
výbojů	výboj	k1gInPc2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
století	století	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
české	český	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
zažívalo	zažívat	k5eAaImAgNnS
jeden	jeden	k4xCgInSc1
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
vrcholů	vrchol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakladatelé	zakladatel	k1gMnPc1
školy	škola	k1gFnSc2
–	–	k?
vedle	vedle	k7c2
Miroslava	Miroslav	k1gMnSc2
Hallera	Haller	k1gMnSc2
hlavně	hlavně	k9
pak	pak	k6eAd1
režisér	režisér	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Frejka	Frejka	k1gFnSc1
a	a	k8xC
scénograf	scénograf	k1gMnSc1
František	František	k1gMnSc1
Tröster	Tröster	k1gMnSc1
v	v	k7c6
sobě	se	k3xPyFc3
spojovali	spojovat	k5eAaImAgMnP
zkušenosti	zkušenost	k1gFnPc4
avantgardního	avantgardní	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
profesionalismem	profesionalismus	k1gInSc7
a	a	k8xC
pedagogickým	pedagogický	k2eAgInSc7d1
talentem	talent	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
se	se	k3xPyFc4
během	během	k7c2
60	#num#	k4
let	léto	k1gNnPc2
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
rozrostla	rozrůst	k5eAaPmAgFnS
a	a	k8xC
obohatila	obohatit	k5eAaPmAgFnS
o	o	k7c4
nové	nový	k2eAgInPc4d1
trendy	trend	k1gInPc4
ve	v	k7c6
vývoji	vývoj	k1gInSc6
divadla	divadlo	k1gNnSc2
a	a	k8xC
stala	stát	k5eAaPmAgFnS
se	s	k7c7
pluralitní	pluralitní	k2eAgFnSc7d1
školou	škola	k1gFnSc7
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
učí	učit	k5eAaImIp3nP
nejvýznamnější	významný	k2eAgFnPc1d3
osobnosti	osobnost	k1gFnPc1
českého	český	k2eAgInSc2d1
divadelního	divadelní	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
:	:	kIx,
oborů	obor	k1gInPc2
herectví	herectví	k1gNnSc2
<g/>
,	,	kIx,
režie	režie	k1gFnSc2
<g/>
,	,	kIx,
dramaturgie	dramaturgie	k1gFnSc2
a	a	k8xC
divadelní	divadelní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
,	,	kIx,
scénografie	scénografie	k1gFnSc2
a	a	k8xC
divadelní	divadelní	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
i	i	k9
osobnosti	osobnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
svým	svůj	k3xOyFgNnSc7
rozkročením	rozkročení	k1gNnSc7
divadlo	divadlo	k1gNnSc4
přesahují	přesahovat	k5eAaImIp3nP
do	do	k7c2
sféry	sféra	k1gFnSc2
autorské	autorský	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
<g/>
,	,	kIx,
filosofie	filosofie	k1gFnSc2
a	a	k8xC
pedagogiky	pedagogika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Katedry	katedra	k1gFnPc1
</s>
<s>
název	název	k1gInSc1
katedry	katedra	k1gFnSc2
</s>
<s>
vedoucí	vedoucí	k1gMnSc1
katedry	katedra	k1gFnSc2
</s>
<s>
obory	obor	k1gInPc1
studia	studio	k1gNnSc2
</s>
<s>
katedra	katedra	k1gFnSc1
činoherního	činoherní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
</s>
<s>
prof.	prof.	kA
MgA.	MgA.	k1gMnSc1
Jan	Jan	k1gMnSc1
Burian	Burian	k1gMnSc1
</s>
<s>
herectví	herectví	k1gNnSc1
</s>
<s>
režie	režie	k1gFnSc1
a	a	k8xC
dramaturgie	dramaturgie	k1gFnSc1
</s>
<s>
katedra	katedra	k1gFnSc1
alternativního	alternativní	k2eAgNnSc2d1
a	a	k8xC
loutkového	loutkový	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
</s>
<s>
Ing.	ing.	kA
MgA.	MgA.	k1gMnSc1
Branislav	Branislav	k1gMnSc1
Mazúch	Mazúch	k1gMnSc1
</s>
<s>
herectví	herectví	k1gNnSc1
</s>
<s>
režie	režie	k1gFnSc1
a	a	k8xC
dramaturgie	dramaturgie	k1gFnSc1
</s>
<s>
scénografie	scénografie	k1gFnSc1
</s>
<s>
katedra	katedra	k1gFnSc1
scénografie	scénografie	k1gFnSc2
</s>
<s>
prof.	prof.	kA
MgA.	MgA.	k1gMnSc1
Jan	Jan	k1gMnSc1
Dušek	Dušek	k1gMnSc1
</s>
<s>
scénografie	scénografie	k1gFnSc1
</s>
<s>
katedra	katedra	k1gFnSc1
produkce	produkce	k1gFnSc2
</s>
<s>
MgA.	MgA.	k?
Michal	Michal	k1gMnSc1
Lázňovský	Lázňovský	k1gMnSc1
</s>
<s>
produkce	produkce	k1gFnSc1
</s>
<s>
katedra	katedra	k1gFnSc1
výchovné	výchovný	k2eAgFnSc2d1
dramatiky	dramatika	k1gFnSc2
</s>
<s>
doc.	doc.	kA
Radek	Radek	k1gMnSc1
Marušák	Marušák	k1gMnSc1
</s>
<s>
dramatická	dramatický	k2eAgFnSc1d1
výchova	výchova	k1gFnSc1
</s>
<s>
katedra	katedra	k1gFnSc1
autorské	autorský	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
a	a	k8xC
pedagogiky	pedagogika	k1gFnSc2
</s>
<s>
doc.	doc.	kA
Mgr.	Mgr.	kA
MgA.	MgA.	k1gMnSc1
Michal	Michal	k1gMnSc1
Čunderle	Čunderle	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
</s>
<s>
autorské	autorský	k2eAgNnSc4d1
divadlo	divadlo	k1gNnSc4
</s>
<s>
katedra	katedra	k1gFnSc1
teorie	teorie	k1gFnSc2
a	a	k8xC
kritiky	kritika	k1gFnSc2
</s>
<s>
doc.	doc.	kA
<g/>
Mgr.	Mgr.	kA
Daniela	Daniela	k1gFnSc1
Jobertová	Jobertová	k1gFnSc1
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
</s>
<s>
teorie	teorie	k1gFnSc1
a	a	k8xC
kritika	kritika	k1gFnSc1
divadelní	divadelní	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
tvoří	tvořit	k5eAaImIp3nS
páteř	páteř	k1gFnSc1
školy	škola	k1gFnSc2
dvě	dva	k4xCgFnPc4
velké	velká	k1gFnPc4
katedry	katedra	k1gFnSc2
svým	svůj	k3xOyFgNnSc7
zaměřením	zaměření	k1gNnSc7
reflektující	reflektující	k2eAgFnSc4d1
různorodost	různorodost	k1gFnSc4
dnešní	dnešní	k2eAgFnSc2d1
divadelní	divadelní	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
–	–	k?
katedra	katedra	k1gFnSc1
činoherního	činoherní	k2eAgNnSc2d1
a	a	k8xC
katedra	katedra	k1gFnSc1
alternativního	alternativní	k2eAgNnSc2d1
a	a	k8xC
loutkového	loutkový	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedra	katedra	k1gFnSc1
činoherního	činoherní	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
a	a	k8xC
katedra	katedra	k1gFnSc1
činoherní	činoherní	k2eAgFnSc2d1
scénografie	scénografie	k1gFnSc2
se	se	k3xPyFc4
zaměřují	zaměřovat	k5eAaImIp3nP
zejména	zejména	k9
na	na	k7c4
výchovu	výchova	k1gFnSc4
k	k	k7c3
herectví	herectví	k1gNnSc3
<g/>
,	,	kIx,
režii	režie	k1gFnSc6
<g/>
,	,	kIx,
dramaturgii	dramaturgie	k1gFnSc6
a	a	k8xC
scénografii	scénografie	k1gFnSc6
v	v	k7c6
repertoárovém	repertoárový	k2eAgInSc6d1
divadelním	divadelní	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
podstatné	podstatný	k2eAgFnSc6d1
míře	míra	k1gFnSc6
založen	založit	k5eAaPmNgInS
na	na	k7c6
interpretaci	interpretace	k1gFnSc6
dramatických	dramatický	k2eAgInPc2d1
textů	text	k1gInPc2
jak	jak	k8xC,k8xS
klasických	klasický	k2eAgInPc2d1
tak	tak	k6eAd1
moderních	moderní	k2eAgInPc2d1
a	a	k8xC
současných	současný	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolventi	absolvent	k1gMnPc1
tvoří	tvořit	k5eAaImIp3nP
často	často	k6eAd1
jádra	jádro	k1gNnSc2
souborů	soubor	k1gInPc2
špičkových	špičkový	k2eAgNnPc2d1
českých	český	k2eAgNnPc2d1
divadel	divadlo	k1gNnPc2
(	(	kIx(
<g/>
např.	např.	kA
Národní	národní	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
stále	stále	k6eAd1
častěji	často	k6eAd2
nacházejí	nacházet	k5eAaImIp3nP
uplatnění	uplatnění	k1gNnSc4
i	i	k9
v	v	k7c6
nezávislých	závislý	k2eNgNnPc6d1
divadelních	divadelní	k2eAgNnPc6d1
uskupeních	uskupení	k1gNnPc6
překvapujících	překvapující	k2eAgNnPc6d1
svou	svůj	k3xOyFgFnSc7
vitalitou	vitalita	k1gFnSc7
a	a	k8xC
razancí	razance	k1gFnSc7
vlastní	vlastní	k2eAgFnSc2d1
divadelní	divadelní	k2eAgFnSc2d1
výpovědi	výpověď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedra	katedra	k1gFnSc1
scénografie	scénografie	k1gFnSc1
přitom	přitom	k6eAd1
vychovává	vychovávat	k5eAaImIp3nS
studenty	student	k1gMnPc4
k	k	k7c3
samostatnosti	samostatnost	k1gFnSc3
a	a	k8xC
profesionalitě	profesionalita	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
umožní	umožnit	k5eAaPmIp3nS
jejich	jejich	k3xOp3gNnSc4
uplatnění	uplatnění	k1gNnSc4
nejen	nejen	k6eAd1
v	v	k7c6
divadlech	divadlo	k1gNnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
v	v	k7c6
médiích	médium	k1gNnPc6
<g/>
,	,	kIx,
výstavnictví	výstavnictví	k1gNnSc1
apod.	apod.	kA
</s>
<s>
Katedra	katedra	k1gFnSc1
alternativního	alternativní	k2eAgNnSc2d1
a	a	k8xC
loutkového	loutkový	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
bohaté	bohatý	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
loutkářské	loutkářský	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
přesahuje	přesahovat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
v	v	k7c6
duchu	duch	k1gMnSc6
tradice	tradice	k1gFnSc2
mezinárodně	mezinárodně	k6eAd1
uznávaných	uznávaný	k2eAgNnPc2d1
českých	český	k2eAgNnPc2d1
divadel	divadlo	k1gNnPc2
studiového	studiový	k2eAgInSc2d1
typu	typ	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
DRAK	drak	k1gInSc1
<g/>
,	,	kIx,
Dejvické	dejvický	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
,	,	kIx,
Studio	studio	k1gNnSc1
Ypsilon	ypsilon	k1gNnPc2
ad	ad	k7c4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
směrem	směr	k1gInSc7
k	k	k7c3
výrazně	výrazně	k6eAd1
stylizovanému	stylizovaný	k2eAgNnSc3d1
divadlu	divadlo	k1gNnSc3
scénického	scénický	k2eAgInSc2d1
objektu	objekt	k1gInSc2
a	a	k8xC
k	k	k7c3
divadlu	divadlo	k1gNnSc3
performativity	performativita	k1gFnSc2
<g/>
,	,	kIx,
paradivadelním	paradivadelní	k2eAgFnPc3d1
aktivitám	aktivita	k1gFnPc3
<g/>
,	,	kIx,
apod.	apod.	kA
Charakteristická	charakteristický	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
úzká	úzký	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
herce	herec	k1gMnSc2
<g/>
,	,	kIx,
režiséra	režisér	k1gMnSc2
i	i	k8xC
výtvarníka	výtvarník	k1gMnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
katedry	katedra	k1gFnSc2
také	také	k9
kabinet	kabinet	k1gInSc1
alternativní	alternativní	k2eAgFnSc2d1
scénografie	scénografie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Absolventi	absolvent	k1gMnPc1
katedry	katedra	k1gFnPc4
alternativního	alternativní	k2eAgMnSc2d1
a	a	k8xC
loutkového	loutkový	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
často	často	k6eAd1
směřují	směřovat	k5eAaImIp3nP
k	k	k7c3
výrazné	výrazný	k2eAgFnSc3d1
osobností	osobnost	k1gFnPc2
i	i	k9
kolektivní	kolektivní	k2eAgFnSc6d1
poetice	poetika	k1gFnSc6
<g/>
,	,	kIx,
celé	celý	k2eAgInPc4d1
absolventské	absolventský	k2eAgInPc4d1
ročníky	ročník	k1gInPc4
zakládají	zakládat	k5eAaImIp3nP
nová	nový	k2eAgNnPc1d1
a	a	k8xC
úspěšná	úspěšný	k2eAgNnPc1d1
divadla	divadlo	k1gNnPc1
a	a	k8xC
jednotlivci	jednotlivec	k1gMnPc1
nacházejí	nacházet	k5eAaImIp3nP
uplatnění	uplatnění	k1gNnSc4
i	i	k9
v	v	k7c6
divadlech	divadlo	k1gNnPc6
určených	určený	k2eAgFnPc2d1
dětskému	dětský	k2eAgMnSc3d1
divákovi	divák	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
katedry	katedra	k1gFnPc1
přesahují	přesahovat	k5eAaImIp3nP
divadelní	divadelní	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedra	katedra	k1gFnSc1
autorské	autorský	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
a	a	k8xC
pedagogiky	pedagogika	k1gFnSc2
je	být	k5eAaImIp3nS
zaměřena	zaměřit	k5eAaPmNgFnS
na	na	k7c4
studium	studium	k1gNnSc4
herectví	herectví	k1gNnSc2
jako	jako	k8xC,k8xS
veřejného	veřejný	k2eAgNnSc2d1
vystupování	vystupování	k1gNnSc2
<g/>
,	,	kIx,
herectví	herectví	k1gNnSc1
osobnostního	osobnostní	k2eAgNnSc2d1
a	a	k8xC
autorského	autorský	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
je	být	k5eAaImIp3nS
improvizační	improvizační	k2eAgFnSc1d1
herecká	herecký	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
vyvinutá	vyvinutý	k2eAgFnSc1d1
prof.	prof.	kA
Ivanem	Ivan	k1gMnSc7
Vyskočilem	Vyskočil	k1gMnSc7
–	–	k?
dialogické	dialogický	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
s	s	k7c7
vnitřním	vnitřní	k2eAgMnSc7d1
partnerem	partner	k1gMnSc7
<g/>
,	,	kIx,
velká	velký	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
je	být	k5eAaImIp3nS
věnována	věnovat	k5eAaImNgFnS,k5eAaPmNgFnS
psychosomatickému	psychosomatický	k2eAgInSc3d1
výcviku	výcvik	k1gInSc3
<g/>
,	,	kIx,
zejména	zejména	k9
studiu	studio	k1gNnSc3
hlasu	hlas	k1gInSc2
a	a	k8xC
řeči	řeč	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katedra	katedra	k1gFnSc1
výchovné	výchovný	k2eAgFnSc2d1
dramatiky	dramatika	k1gFnSc2
je	být	k5eAaImIp3nS
zaměřena	zaměřit	k5eAaPmNgFnS
na	na	k7c4
divadlo	divadlo	k1gNnSc4
ve	v	k7c6
výchově	výchova	k1gFnSc6
<g/>
,	,	kIx,
katedra	katedra	k1gFnSc1
teorie	teorie	k1gFnSc2
a	a	k8xC
kritiky	kritika	k1gFnSc2
koncipuje	koncipovat	k5eAaBmIp3nS
teoretické	teoretický	k2eAgInPc4d1
předměty	předmět	k1gInPc4
studované	studovaný	k2eAgInPc4d1
na	na	k7c4
DAMU	DAMU	kA
<g/>
,	,	kIx,
realizuje	realizovat	k5eAaBmIp3nS
přednášky	přednáška	k1gFnPc4
a	a	k8xC
semináře	seminář	k1gInPc4
teoretických	teoretický	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
a	a	k8xC
vychovává	vychovávat	k5eAaImIp3nS
vlastní	vlastní	k2eAgMnPc4d1
studenty	student	k1gMnPc4
k	k	k7c3
poučené	poučený	k2eAgFnSc3d1
kritice	kritika	k1gFnSc3
<g/>
,	,	kIx,
katedra	katedra	k1gFnSc1
produkce	produkce	k1gFnSc1
vychová	vychovat	k5eAaPmIp3nS
divadelní	divadelní	k2eAgMnPc4d1
manažery	manažer	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
uplatňují	uplatňovat	k5eAaImIp3nP
ve	v	k7c6
všech	všecek	k3xTgInPc6
typech	typ	k1gInPc6
divadel	divadlo	k1gNnPc2
i	i	k9
mimo	mimo	k7c4
ně	on	k3xPp3gMnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
realizuje	realizovat	k5eAaBmIp3nS
také	také	k9
doktorské	doktorský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ve	v	k7c6
čtyřech	čtyři	k4xCgInPc6
oborech	obor	k1gInPc6
<g/>
:	:	kIx,
Scénická	scénický	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
a	a	k8xC
teorie	teorie	k1gFnSc1
scénické	scénický	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
<g/>
,	,	kIx,
Teorie	teorie	k1gFnSc2
a	a	k8xC
praxe	praxe	k1gFnSc2
divadelní	divadelní	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
<g/>
,	,	kIx,
Alternativní	alternativní	k2eAgFnSc1d1
a	a	k8xC
loutková	loutkový	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gFnSc1
teorie	teorie	k1gFnSc1
a	a	k8xC
Autorské	autorský	k2eAgNnSc1d1
herectví	herectví	k1gNnSc1
a	a	k8xC
teorie	teorie	k1gFnSc1
autorské	autorský	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
a	a	k8xC
pedagogiky	pedagogika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Děkani	děkan	k1gMnPc1
DAMU	DAMU	kA
</s>
<s>
1946	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
Jiří	Jiří	k1gMnSc2
Frejka	Frejka	k1gFnSc1
</s>
<s>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1949	#num#	k4
Klementina	Klementinum	k1gNnPc4
Rektorisová	Rektorisový	k2eAgNnPc4d1
</s>
<s>
1949	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
František	František	k1gMnSc1
Götz	Götz	k1gMnSc1
</s>
<s>
1950	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
Jan	Jan	k1gMnSc1
Kopecký	Kopecký	k1gMnSc1
</s>
<s>
1952	#num#	k4
<g/>
–	–	k?
<g/>
1953	#num#	k4
František	František	k1gMnSc1
Götz	Götz	k1gMnSc1
</s>
<s>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1954	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Adámek	Adámek	k1gMnSc1
</s>
<s>
1954	#num#	k4
<g/>
–	–	k?
<g/>
1955	#num#	k4
František	František	k1gMnSc1
Salzer	Salzer	k1gMnSc1
</s>
<s>
1955	#num#	k4
<g/>
–	–	k?
<g/>
1958	#num#	k4
Josef	Josef	k1gMnSc1
Bezdíček	Bezdíček	k1gMnSc1
</s>
<s>
1958	#num#	k4
<g/>
–	–	k?
<g/>
1961	#num#	k4
František	František	k1gMnSc1
Salzer	Salzer	k1gMnSc1
</s>
<s>
1961	#num#	k4
<g/>
–	–	k?
<g/>
1963	#num#	k4
Antonín	Antonín	k1gMnSc1
Dvořák	Dvořák	k1gMnSc1
</s>
<s>
1963	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
František	František	k1gMnSc1
Salzer	Salzer	k1gMnSc1
</s>
<s>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1972	#num#	k4
Jan	Jan	k1gMnSc1
Císař	Císař	k1gMnSc1
</s>
<s>
1972	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
Eva	Eva	k1gFnSc1
Šmeralová	šmeralový	k2eAgNnPc1d1
</s>
<s>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
Jana	Jana	k1gFnSc1
Makovská	Makovská	k1gFnSc1
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
Jan	Jan	k1gMnSc1
Dušek	Dušek	k1gMnSc1
</s>
<s>
1991	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
Miloslav	Miloslav	k1gMnSc1
Klíma	Klíma	k1gMnSc1
</s>
<s>
1994	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
Miloš	Miloš	k1gMnSc1
Horanský	Horanský	k2eAgMnSc5d1
</s>
<s>
1997	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
Vladimír	Vladimír	k1gMnSc1
Mikeš	Mikeš	k1gMnSc1
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
Markéta	Markéta	k1gFnSc1
Kočvarová-Schartová	Kočvarová-Schartová	k1gFnSc1
</s>
<s>
2006	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
Jan	Jan	k1gMnSc1
Hančil	Hančil	k1gMnSc1
</s>
<s>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
Doubravka	Doubravka	k1gFnSc1
Svobodová	Svobodová	k1gFnSc1
</s>
<s>
2020	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Karel	Karel	k1gMnSc1
František	František	k1gMnSc1
Tománek	Tománek	k1gMnSc1
</s>
<s>
Fotografie	fotografia	k1gFnPc4
ze	z	k7c2
studentských	studentský	k2eAgNnPc2d1
představení	představení	k1gNnPc2
</s>
<s>
Hra	hra	k1gFnSc1
</s>
<s>
Štěstí	štěstí	k1gNnSc1
dam	dáma	k1gFnPc2
</s>
<s>
Bratři	bratr	k1gMnPc1
</s>
<s>
Cardym	Cardym	k1gInSc1
</s>
<s>
John	John	k1gMnSc1
Sinclair	Sinclair	k1gMnSc1
</s>
<s>
Láska	láska	k1gFnSc1
<g/>
,	,	kIx,
vole	vole	k1gNnSc1
</s>
<s>
Ledový	ledový	k2eAgInSc1d1
hrot	hrot	k1gInSc1
</s>
<s>
Mužské	mužský	k2eAgFnPc1d1
záležitosti	záležitost	k1gFnPc1
</s>
<s>
Titus	Titus	k1gMnSc1
Andronicus	Andronicus	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Kolektiv	kolektiv	k1gInSc1
autorů	autor	k1gMnPc2
<g/>
:	:	kIx,
AMU	AMU	kA
60	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
Publikace	publikace	k1gFnSc1
k	k	k7c3
60	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
založení	založení	k1gNnSc2
AMU	AMU	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
–	–	k?
<g/>
29	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7331-065-1	80-7331-065-1	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Akademie	akademie	k1gFnSc2
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
AMU	AMU	kA
–	–	k?
oficiální	oficiální	k2eAgInSc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
The	The	k?
Prague	Prague	k1gInSc1
Summer	Summer	k1gInSc1
Theatre	Theatr	k1gInSc5
School	Schoola	k1gFnPc2
at	at	k?
the	the	k?
Theatre	Theatr	k1gInSc5
Faculty	Facult	k1gInPc7
of	of	k?
the	the	k?
Academy	Academa	k1gFnSc2
of	of	k?
Performing	Performing	k1gInSc1
Arts	Arts	k1gInSc1
(	(	kIx(
<g/>
DAMU	DAMU	kA
<g/>
)	)	kIx)
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
AMU	AMU	kA
–	–	k?
facebook	facebook	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Fakulty	fakulta	k1gFnSc2
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1
a	a	k8xC
televizní	televizní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Hudební	hudební	k2eAgFnSc1d1
a	a	k8xC
taneční	taneční	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
•	•	k?
Divadelní	divadelní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Pedagogická	pedagogický	k2eAgFnSc1d1
pracoviště	pracoviště	k1gNnSc4
</s>
<s>
Katedra	katedra	k1gFnSc1
cizích	cizí	k2eAgMnPc2d1
jazyků	jazyk	k1gMnPc2
AMU	AMU	kA
•	•	k?
Katedra	katedra	k1gFnSc1
tělesné	tělesný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
AMU	AMU	kA
Informační	informační	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
</s>
<s>
Knihovna	knihovna	k1gFnSc1
AMU	AMU	kA
•	•	k?
Počítačové	počítačový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
AMU	AMU	kA
•	•	k?
Nakladatelství	nakladatelství	k1gNnSc1
AMU	AMU	kA
Jednotky	jednotka	k1gFnSc2
fakult	fakulta	k1gFnPc2
</s>
<s>
Divadlo	divadlo	k1gNnSc1
DISK	disk	k1gInSc1
(	(	kIx(
<g/>
DAMU	DAMU	kA
<g/>
)	)	kIx)
•	•	k?
Studio	studio	k1gNnSc1
FAMU	FAMU	kA
(	(	kIx(
<g/>
FAMU	FAMU	kA
<g/>
)	)	kIx)
Účelová	účelový	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Kolej	kolej	k1gFnSc1
a	a	k8xC
učební	učební	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
•	•	k?
Ubytovací	ubytovací	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
•	•	k?
Učební	učební	k2eAgMnSc1d1
a	a	k8xC
výcvikové	výcvikový	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Hluboká	Hluboká	k1gFnSc1
nad	nad	k7c4
Vltavou-Poněšice	Vltavou-Poněšic	k1gMnSc4
•	•	k?
Učební	učební	k2eAgNnSc1d1
a	a	k8xC
výcvikové	výcvikový	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
Beroun	Beroun	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
|	|	kIx~
Umění	umění	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20010709001	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
125622952	#num#	k4
</s>
