<s>
Oběd	oběd	k1gInSc1	oběd
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
hlavních	hlavní	k2eAgNnPc2d1	hlavní
jídel	jídlo	k1gNnPc2	jídlo
(	(	kIx(	(
<g/>
zbývající	zbývající	k2eAgFnSc1d1	zbývající
snídaně	snídaně	k1gFnSc1	snídaně
<g/>
,	,	kIx,	,
večeře	večeře	k1gFnSc1	večeře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
jako	jako	k8xS	jako
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
v	v	k7c6	v
době	doba	k1gFnSc6	doba
poledne	poledne	k1gNnSc2	poledne
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
oběd	oběd	k1gInSc1	oběd
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
z	z	k7c2	z
denních	denní	k2eAgNnPc2d1	denní
jídel	jídlo	k1gNnPc2	jídlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
či	či	k8xC	či
tří	tři	k4xCgInPc2	tři
chodů	chod	k1gInPc2	chod
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tradici	tradice	k1gFnSc6	tradice
a	a	k8xC	a
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
chody	chod	k1gInPc1	chod
jsou	být	k5eAaImIp3nP	být
polévka	polévka	k1gFnSc1	polévka
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
jídlo	jídlo	k1gNnSc1	jídlo
a	a	k8xC	a
dezert	dezert	k1gInSc1	dezert
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
není	být	k5eNaImIp3nS	být
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
složkou	složka	k1gFnSc7	složka
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
obědu	oběd	k1gInSc3	oběd
je	být	k5eAaImIp3nS	být
různá	různý	k2eAgFnSc1d1	různá
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
denní	denní	k2eAgFnSc6d1	denní
aktivitě	aktivita	k1gFnSc6	aktivita
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
restauracích	restaurace	k1gFnPc6	restaurace
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
obědu	oběd	k1gInSc3	oběd
často	často	k6eAd1	často
prodávané	prodávaný	k2eAgFnSc3d1	prodávaná
hotové	hotová	k1gFnSc3	hotová
menu	menu	k1gNnSc2	menu
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
sníženou	snížený	k2eAgFnSc4d1	snížená
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
pokrmů	pokrm	k1gInPc2	pokrm
během	během	k7c2	během
oběda	oběd	k1gInSc2	oběd
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c6	na
regionálních	regionální	k2eAgInPc6d1	regionální
zvycích	zvyk	k1gInPc6	zvyk
a	a	k8xC	a
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
místo	místo	k1gNnSc1	místo
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
má	mít	k5eAaImIp3nS	mít
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
nedělní	nedělní	k2eAgInSc4d1	nedělní
oběd	oběd	k1gInSc4	oběd
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
schází	scházet	k5eAaImIp3nS	scházet
u	u	k7c2	u
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
příležitostí	příležitost	k1gFnSc7	příležitost
pro	pro	k7c4	pro
všeobecné	všeobecný	k2eAgNnSc4d1	všeobecné
setkání	setkání	k1gNnSc4	setkání
všech	všecek	k3xTgInPc2	všecek
jejich	jejich	k3xOp3gInPc2	jejich
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Nedělní	nedělní	k2eAgInSc1d1	nedělní
oběd	oběd	k1gInSc1	oběd
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
i	i	k9	i
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
postavení	postavení	k1gNnSc4	postavení
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
přípravu	příprava	k1gFnSc4	příprava
je	být	k5eAaImIp3nS	být
vynakládáno	vynakládán	k2eAgNnSc1d1	vynakládáno
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
a	a	k8xC	a
i	i	k9	i
sortiment	sortiment	k1gInSc1	sortiment
pokrmů	pokrm	k1gInPc2	pokrm
bývá	bývat	k5eAaImIp3nS	bývat
pestřejší	pestrý	k2eAgMnSc1d2	pestřejší
a	a	k8xC	a
složitější	složitý	k2eAgMnSc1d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
podávat	podávat	k5eAaImF	podávat
oběd	oběd	k1gInSc4	oběd
mezi	mezi	k7c4	mezi
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
daleko	daleko	k6eAd1	daleko
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Polsku	Polsko	k1gNnSc6	Polsko
se	se	k3xPyFc4	se
oběd	oběd	k1gInSc1	oběd
podává	podávat	k5eAaImIp3nS	podávat
mezi	mezi	k7c7	mezi
14	[number]	k4	14
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
hodinou	hodina	k1gFnSc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
situace	situace	k1gFnSc1	situace
panuje	panovat	k5eAaImIp3nS	panovat
i	i	k9	i
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oběd	oběd	k1gInSc1	oběd
tradičně	tradičně	k6eAd1	tradičně
podává	podávat	k5eAaImIp3nS	podávat
kolem	kolem	k7c2	kolem
15	[number]	k4	15
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ale	ale	k9	ale
řada	řada	k1gFnSc1	řada
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
determinovány	determinovat	k5eAaBmNgFnP	determinovat
zvláště	zvláště	k9	zvláště
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
lidem	lid	k1gInSc7	lid
dává	dávat	k5eAaImIp3nS	dávat
čas	čas	k1gInSc1	čas
na	na	k7c4	na
oběd	oběd	k1gInSc4	oběd
jejich	jejich	k3xOp3gMnSc1	jejich
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Oběd	oběd	k1gInSc4	oběd
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Kam-Dnes-Na-Obed	Kam-Dnes-Na-Obed	k1gMnSc1	Kam-Dnes-Na-Obed
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
