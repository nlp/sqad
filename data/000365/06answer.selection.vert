<s>
Nobelium	nobelium	k1gNnSc1	nobelium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
No	no	k9	no
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Nobelium	nobelium	k1gNnSc4	nobelium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
čtrnáctým	čtrnáctý	k4xOgInSc7	čtrnáctý
členem	člen	k1gInSc7	člen
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
aktinoidů	aktinoid	k1gInPc2	aktinoid
<g/>
,	,	kIx,	,
desátým	desátý	k4xOgInSc7	desátý
transuranem	transuran	k1gInSc7	transuran
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
uměle	uměle	k6eAd1	uměle
ozařováním	ozařování	k1gNnSc7	ozařování
jader	jádro	k1gNnPc2	jádro
curia	curium	k1gNnSc2	curium
<g/>
.	.	kIx.	.
</s>
