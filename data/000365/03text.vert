<s>
Nobelium	nobelium	k1gNnSc1	nobelium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
No	no	k9	no
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Nobelium	nobelium	k1gNnSc4	nobelium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
čtrnáctým	čtrnáctý	k4xOgInSc7	čtrnáctý
členem	člen	k1gInSc7	člen
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
aktinoidů	aktinoid	k1gInPc2	aktinoid
<g/>
,	,	kIx,	,
desátým	desátý	k4xOgInSc7	desátý
transuranem	transuran	k1gInSc7	transuran
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
uměle	uměle	k6eAd1	uměle
ozařováním	ozařování	k1gNnSc7	ozařování
jader	jádro	k1gNnPc2	jádro
curia	curium	k1gNnSc2	curium
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
<g/>
.	.	kIx.	.
</s>
<s>
Nobelium	nobelium	k1gNnSc1	nobelium
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
kovový	kovový	k2eAgInSc4d1	kovový
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
doposud	doposud	k6eAd1	doposud
nebyl	být	k5eNaImAgInS	být
izolován	izolovat	k5eAaBmNgInS	izolovat
v	v	k7c6	v
dostatečně	dostatečně	k6eAd1	dostatečně
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
určit	určit	k5eAaPmF	určit
všechny	všechen	k3xTgFnPc4	všechen
jeho	jeho	k3xOp3gFnPc4	jeho
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
konstanty	konstanta	k1gFnPc4	konstanta
<g/>
.	.	kIx.	.
</s>
<s>
Vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
α	α	k?	α
a	a	k8xC	a
γ	γ	k?	γ
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
silným	silný	k2eAgInSc7d1	silný
zdrojem	zdroj	k1gInSc7	zdroj
neutronů	neutron	k1gInPc2	neutron
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
manipulovat	manipulovat	k5eAaImF	manipulovat
za	za	k7c4	za
dodržování	dodržování	k1gNnSc4	dodržování
bezpečnostních	bezpečnostní	k2eAgNnPc2d1	bezpečnostní
opatření	opatření	k1gNnPc2	opatření
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
radioaktivními	radioaktivní	k2eAgInPc7d1	radioaktivní
materiály	materiál	k1gInPc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gFnPc6	jeho
sloučeninách	sloučenina	k1gFnPc6	sloučenina
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc6	jejich
chemickém	chemický	k2eAgNnSc6d1	chemické
chování	chování	k1gNnSc6	chování
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Nobelium	nobelium	k1gNnSc1	nobelium
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
uměle	uměle	k6eAd1	uměle
připravený	připravený	k2eAgInSc1d1	připravený
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
transuranů	transuran	k1gInPc2	transuran
<g/>
.	.	kIx.	.
</s>
<s>
Nobelium	nobelium	k1gNnSc1	nobelium
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
připraveno	připravit	k5eAaPmNgNnS	připravit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
university	universita	k1gFnSc2	universita
v	v	k7c4	v
Berkeley	Berkelea	k1gFnPc4	Berkelea
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
nového	nový	k2eAgInSc2d1	nový
lineárního	lineární	k2eAgInSc2d1	lineární
urychlovače	urychlovač	k1gInSc2	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
uvedeném	uvedený	k2eAgInSc6d1	uvedený
experimentu	experiment	k1gInSc6	experiment
byl	být	k5eAaImAgInS	být
bombardován	bombardován	k2eAgInSc1d1	bombardován
terč	terč	k1gInSc1	terč
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
izotopů	izotop	k1gInPc2	izotop
curia	curium	k1gNnSc2	curium
(	(	kIx(	(
<g/>
95	[number]	k4	95
%	%	kIx~	%
244	[number]	k4	244
<g/>
Cm	cm	kA	cm
+	+	kIx~	+
4,5	[number]	k4	4,5
%	%	kIx~	%
246	[number]	k4	246
<g/>
Cm	cm	kA	cm
<g/>
)	)	kIx)	)
jádry	jádro	k1gNnPc7	jádro
uhlíku	uhlík	k1gInSc2	uhlík
12C	[number]	k4	12C
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
získáno	získán	k2eAgNnSc4d1	získáno
nobelium	nobelium	k1gNnSc4	nobelium
252	[number]	k4	252
<g/>
No	no	k9	no
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
2,4	[number]	k4	2,4
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
244	[number]	k4	244
96	[number]	k4	96
Cm	cm	kA	cm
+	+	kIx~	+
12	[number]	k4	12
6	[number]	k4	6
C	C	kA	C
→	→	k?	→
256	[number]	k4	256
102	[number]	k4	102
No	no	k9	no
→	→	k?	→
252	[number]	k4	252
102	[number]	k4	102
No	no	k9	no
+	+	kIx~	+
4	[number]	k4	4
1	[number]	k4	1
0	[number]	k4	0
n	n	k0	n
Za	za	k7c2	za
jeho	jeho	k3xOp3gMnSc2	jeho
objevitele	objevitel	k1gMnSc2	objevitel
jsou	být	k5eAaImIp3nP	být
pokládáni	pokládat	k5eAaImNgMnP	pokládat
Albert	Alberta	k1gFnPc2	Alberta
Ghiorso	Ghiorsa	k1gFnSc5	Ghiorsa
<g/>
,	,	kIx,	,
Glenn	Glenn	k1gMnSc1	Glenn
T.	T.	kA	T.
Seaborg	Seaborg	k1gMnSc1	Seaborg
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
R.	R.	kA	R.
Walton	Walton	k1gInSc1	Walton
a	a	k8xC	a
Torbjø	Torbjø	k1gFnSc1	Torbjø
Sikkeland	Sikkelanda	k1gFnPc2	Sikkelanda
<g/>
.	.	kIx.	.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
švédského	švédský	k2eAgMnSc2d1	švédský
chemika	chemik	k1gMnSc2	chemik
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc2	vynálezce
dynamitu	dynamit	k1gInSc2	dynamit
Alfreda	Alfred	k1gMnSc2	Alfred
Nobela	Nobel	k1gMnSc2	Nobel
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
umělá	umělý	k2eAgFnSc1d1	umělá
příprava	příprava	k1gFnSc1	příprava
nobelia	nobelium	k1gNnSc2	nobelium
byla	být	k5eAaImAgFnS	být
oznámena	oznámen	k2eAgFnSc1d1	oznámena
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
bombardován	bombardován	k2eAgInSc1d1	bombardován
terčík	terčík	k1gInSc1	terčík
z	z	k7c2	z
uranu	uran	k1gInSc2	uran
238U	[number]	k4	238U
jádry	jádro	k1gNnPc7	jádro
neonu	neon	k1gInSc3	neon
22	[number]	k4	22
<g/>
Ne	ne	k9	ne
238	[number]	k4	238
92	[number]	k4	92
U	U	kA	U
+	+	kIx~	+
22	[number]	k4	22
10	[number]	k4	10
Ne	Ne	kA	Ne
→	→	k?	→
260	[number]	k4	260
102	[number]	k4	102
No	no	k9	no
→	→	k?	→
254	[number]	k4	254
102	[number]	k4	102
No	no	k9	no
+	+	kIx~	+
6	[number]	k4	6
1	[number]	k4	1
0	[number]	k4	0
n	n	k0	n
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
celkem	celkem	k6eAd1	celkem
17	[number]	k4	17
izotopů	izotop	k1gInPc2	izotop
nobelia	nobelium	k1gNnSc2	nobelium
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
nejstabilnější	stabilní	k2eAgFnSc4d3	nejstabilnější
jsou	být	k5eAaImIp3nP	být
259	[number]	k4	259
<g/>
No	no	k9	no
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
58	[number]	k4	58
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
255	[number]	k4	255
<g/>
No	no	k9	no
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
3,1	[number]	k4	3,1
minuty	minuta	k1gFnSc2	minuta
a	a	k8xC	a
253	[number]	k4	253
<g/>
No	no	k9	no
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
1,62	[number]	k4	1,62
minuty	minuta	k1gFnSc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgInPc1d1	zbývající
izotopy	izotop	k1gInPc1	izotop
mají	mít	k5eAaImIp3nP	mít
poločas	poločas	k1gInSc4	poločas
rozpadu	rozpad	k1gInSc2	rozpad
menší	malý	k2eAgFnSc3d2	menší
než	než	k8xS	než
jednu	jeden	k4xCgFnSc4	jeden
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
II	II	kA	II
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Aktinoidy	Aktinoid	k1gInPc4	Aktinoid
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
fyzika	fyzika	k1gFnSc1	fyzika
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
nobelium	nobelium	k1gNnSc4	nobelium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
nobelium	nobelium	k1gNnSc4	nobelium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
