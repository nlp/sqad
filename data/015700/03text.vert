<s>
Americká	americký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
vojenských	vojenský	k2eAgFnPc6d1
akcích	akce	k1gFnPc6
americké	americký	k2eAgFnSc2d1
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
politickém	politický	k2eAgInSc6d1
a	a	k8xC
společenském	společenský	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Americká	americký	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
Styl	styl	k1gInSc4
<g/>
,	,	kIx,
překlad	překlad	k1gInSc4
<g/>
,	,	kIx,
opravit	opravit	k5eAaPmF
některé	některý	k3yIgFnPc4
nesmyslné	smyslný	k2eNgFnPc4d1
věty	věta	k1gFnPc4
<g/>
,	,	kIx,
rozšířit	rozšířit	k5eAaPmF
kapitoly	kapitola	k1gFnPc4
o	o	k7c6
válce	válka	k1gFnSc6
jako	jako	k9
takové	takový	k3xDgNnSc1
<g/>
,	,	kIx,
neb	neb	k8xC
příliš	příliš	k6eAd1
zaměřené	zaměřený	k2eAgNnSc4d1
na	na	k7c4
politiku	politika	k1gFnSc4
-	-	kIx~
spíš	spíš	k9
vhodné	vhodný	k2eAgNnSc1d1
pro	pro	k7c4
článek	článek	k1gInSc4
Americká	americký	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
</s>
<s>
Koláž	koláž	k1gFnSc1
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1775	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1783	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
4	#num#	k4
měsíce	měsíc	k1gInSc2
a	a	k8xC
15	#num#	k4
dní	den	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
(	(	kIx(
<g/>
Třináct	třináct	k4xCc1
kolonií	kolonie	k1gFnPc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Karibské	karibský	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Indický	indický	k2eAgInSc1d1
subkontinent	subkontinent	k1gInSc1
<g/>
,	,	kIx,
Afrika	Afrika	k1gFnSc1
<g/>
,	,	kIx,
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
a	a	k8xC
Indický	indický	k2eAgInSc1d1
oceán	oceán	k1gInSc1
</s>
<s>
casus	casus	k1gInSc4
belli	bell	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Americká	americký	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
,	,	kIx,
vyhlášení	vyhlášení	k1gNnSc1
nezávislosti	nezávislost	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Pařížská	pařížský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
(	(	kIx(
<g/>
1783	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nezávislost	nezávislost	k1gFnSc4
USA	USA	kA
</s>
<s>
změny	změna	k1gFnPc1
území	území	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Británie	Británie	k1gFnSc1
uznala	uznat	k5eAaPmAgFnS
nezávislost	nezávislost	k1gFnSc4
USA	USA	kA
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Třináct	třináct	k4xCc1
kolonií	kolonie	k1gFnPc2
<g/>
(	(	kIx(
<g/>
před	před	k7c7
1776	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
(	(	kIx(
<g/>
po	po	k7c4
1776	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vermontská	Vermontský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1
koloniální	koloniální	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Španělské	španělský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
</s>
<s>
kooperace	kooperace	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Maisúrské	Maisúrský	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
domorodí	domorodý	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Britské	britský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
loyalisté	loyalista	k1gMnPc5
Hannoversko	Hannoversko	k1gNnSc1
</s>
<s>
němečtí	německý	k2eAgMnPc1d1
žoldnéři	žoldnér	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Hesensko-Kasselsko	Hesensko-Kasselsko	k6eAd1
</s>
<s>
Hesensko-Hanau	Hesensko-Hanau	k6eAd1
</s>
<s>
Waldeck-Pyrmont	Waldeck-Pyrmont	k1gMnSc1
</s>
<s>
Brunšvik	Brunšvik	k1gInSc1
</s>
<s>
Ansbach	Ansbach	k1gMnSc1
</s>
<s>
Anhalt-Zerbst	Anhalt-Zerbst	k1gFnSc1
</s>
<s>
domorodí	domorodý	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
George	George	k1gNnSc1
Washington	Washington	k1gInSc1
Thomas	Thomas	k1gMnSc1
Chittenden	Chittendna	k1gFnPc2
Ludvík	Ludvík	k1gMnSc1
XVI	XVI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gilbert	Gilbert	k1gMnSc1
de	de	k?
La	la	k1gNnPc2
Fayette	Fayett	k1gInSc5
Karel	Karel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tadeusz	Tadeusz	k1gInSc1
Kościuszko	Kościuszka	k1gFnSc5
Vilém	Vilém	k1gMnSc1
V.	V.	kA
Oranžský	oranžský	k2eAgInSc1d1
Hyder	hydra	k1gFnPc2
Ali	Ali	k1gMnSc7
†	†	k?
Fateh	Fateh	k1gMnSc1
Alí	Alí	k1gMnSc1
Tipú	Tipú	k1gMnSc1
<g/>
...	...	k?
</s>
<s>
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frederick	Frederick	k1gMnSc1
North	North	k1gMnSc1
Lord	lord	k1gMnSc1
George	Georg	k1gMnSc4
Sackville	Sackvill	k1gMnSc4
Charles	Charles	k1gMnSc1
Cornwallis	Cornwallis	k1gFnSc2
<g/>
...	...	k?
</s>
<s>
síla	síla	k1gFnSc1
</s>
<s>
Američanéarmáda	Američanéarmáda	k1gFnSc1
a	a	k8xC
milice	milice	k1gFnSc1
</s>
<s>
40	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
průměr	průměr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
200	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
námořnictvo	námořnictvo	k1gNnSc1
</s>
<s>
53	#num#	k4
fregat	fregata	k1gFnPc2
a	a	k8xC
dělovýchšalup	dělovýchšalup	k1gInSc1
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
5	#num#	k4
000	#num#	k4
námořníků	námořník	k1gMnPc2
(	(	kIx(
<g/>
1779	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
státní	státní	k2eAgMnSc1d1
námořnictva	námořnictvo	k1gNnSc2
</s>
<s>
106	#num#	k4
lodí	loď	k1gFnPc2
</s>
<s>
privatýři	privatýř	k1gMnPc1
</s>
<s>
1	#num#	k4
697	#num#	k4
lodí	loď	k1gFnPc2
</s>
<s>
55	#num#	k4
000	#num#	k4
námořníků	námořník	k1gMnPc2
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
spojenci	spojenec	k1gMnPc1
</s>
<s>
63	#num#	k4
000	#num#	k4
Francouzů	Francouz	k1gMnPc2
aŠpanělů	aŠpaněl	k1gInPc2
(	(	kIx(
<g/>
Gibraltar	Gibraltar	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
146	#num#	k4
řadových	řadový	k2eAgMnPc2d1
lodí	loď	k1gFnPc2
</s>
<s>
domorodí	domorodý	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
</s>
<s>
neznámý	známý	k2eNgInSc1d1
počet	počet	k1gInSc1
</s>
<s>
Britovéarmáda	Britovéarmáda	k1gFnSc1
</s>
<s>
48	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
121	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
globálně	globálně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
500	#num#	k4
(	(	kIx(
<g/>
Gibraltar	Gibraltar	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
námořnictvo	námořnictvo	k1gNnSc1
</s>
<s>
94	#num#	k4
řadových	řadový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
(	(	kIx(
<g/>
1782	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
104	#num#	k4
fregat	fregata	k1gFnPc2
(	(	kIx(
<g/>
1781	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
37	#num#	k4
dělových	dělový	k2eAgFnPc2d1
šalup	šalupa	k1gFnPc2
(	(	kIx(
<g/>
1781	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
171	#num#	k4
000	#num#	k4
námořníků	námořník	k1gMnPc2
</s>
<s>
loyalisté	loyalista	k1gMnPc1
</s>
<s>
25	#num#	k4
000	#num#	k4
</s>
<s>
hannoverští	hannoverský	k2eAgMnPc1d1
</s>
<s>
2	#num#	k4
365	#num#	k4
</s>
<s>
němečtí	německý	k2eAgMnPc1d1
žoldnéři	žoldnér	k1gMnPc1
</s>
<s>
29	#num#	k4
875	#num#	k4
</s>
<s>
domorodí	domorodý	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
</s>
<s>
13	#num#	k4
000	#num#	k4
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
Američané	Američan	k1gMnPc1
</s>
<s>
25	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
padlých	padlý	k1gMnPc2
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
6	#num#	k4
800	#num#	k4
padlýchv	padlýchva	k1gFnPc2
boji	boj	k1gInSc3
</s>
<s>
17	#num#	k4
000	#num#	k4
zemřelona	zemřelona	k1gFnSc1
zranění	zranění	k1gNnSc2
</s>
<s>
Francouzi	Francouz	k1gMnPc1
</s>
<s>
7	#num#	k4
000	#num#	k4
padlých	padlý	k1gMnPc2
</s>
<s>
Španělé	Španěl	k1gMnPc1
</s>
<s>
5	#num#	k4
000	#num#	k4
padlých	padlý	k1gMnPc2
</s>
<s>
Nizozemci	Nizozemec	k1gMnPc1
</s>
<s>
500	#num#	k4
padlých	padlý	k2eAgMnPc2d1
</s>
<s>
celkem	celkem	k6eAd1
<g/>
:	:	kIx,
37	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
82	#num#	k4
500	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Britovéarmáda	Britovéarmáda	k1gFnSc1
</s>
<s>
43	#num#	k4
633	#num#	k4
padlých	padlý	k1gMnPc2
</s>
<s>
~	~	kIx~
9	#num#	k4
372	#num#	k4
padlýchv	padlýchva	k1gFnPc2
boji	boj	k1gInSc3
</s>
<s>
27	#num#	k4
000	#num#	k4
zemřelona	zemřelona	k1gFnSc1
zranění	zranění	k1gNnSc2
</s>
<s>
námořnictvo	námořnictvo	k1gNnSc1
</s>
<s>
1	#num#	k4
243	#num#	k4
padlých	padlý	k1gMnPc2
</s>
<s>
18	#num#	k4
500	#num#	k4
zemřelona	zemřelona	k1gFnSc1
zranění	zranění	k1gNnSc2
</s>
<s>
42	#num#	k4
000	#num#	k4
dezertovalo	dezertovat	k5eAaBmAgNnS
</s>
<s>
20	#num#	k4
řadových	řadový	k2eAgMnPc2d1
lodí	loď	k1gFnPc2
</s>
<s>
70	#num#	k4
fregat	fregata	k1gFnPc2
</s>
<s>
2	#num#	k4
200	#num#	k4
obchodníchlodí	obchodníchlodí	k1gNnPc2
</s>
<s>
75	#num#	k4
lodí	loď	k1gFnPc2
privatýrů	privatýr	k1gInPc2
</s>
<s>
loyalisté	loyalista	k1gMnPc1
</s>
<s>
7	#num#	k4
000	#num#	k4
padlých	padlý	k1gMnPc2
</s>
<s>
1	#num#	k4
700	#num#	k4
padlýchv	padlýchva	k1gFnPc2
boji	boj	k1gInSc3
</s>
<s>
5	#num#	k4
300	#num#	k4
zemřelona	zemřelona	k1gFnSc1
zranění	zranění	k1gNnSc2
</s>
<s>
Němci	Němec	k1gMnPc1
</s>
<s>
7	#num#	k4
774	#num#	k4
padlých	padlý	k1gMnPc2
</s>
<s>
1	#num#	k4
800	#num#	k4
padlýchv	padlýchva	k1gFnPc2
boji	boj	k1gInSc3
</s>
<s>
4	#num#	k4
888	#num#	k4
dezertovalo	dezertovat	k5eAaBmAgNnS
</s>
<s>
celkem	celkem	k6eAd1
<g/>
:	:	kIx,
78,200	78,200	k4
<g/>
+	+	kIx~
padlých	padlý	k1gMnPc2
</s>
<s>
Americká	americký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
(	(	kIx(
<g/>
1775	#num#	k4
<g/>
–	–	k?
<g/>
1783	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
americká	americký	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
American	American	k1gMnSc1
Revolutionary	Revolutionara	k1gFnSc2
War	War	k1gMnSc1
<g/>
,	,	kIx,
American	American	k1gMnSc1
War	War	k1gFnSc2
of	of	k?
Independence	Independence	k1gFnSc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
válka	válka	k1gFnSc1
mezi	mezi	k7c7
Královstvím	království	k1gNnSc7
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
třinácti	třináct	k4xCc7
koloniemi	kolonie	k1gFnPc7
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střetnutí	střetnutí	k1gNnSc1
skončilo	skončit	k5eAaPmAgNnS
porážkou	porážka	k1gFnSc7
Británie	Británie	k1gFnSc2
a	a	k8xC
uznáním	uznání	k1gNnSc7
nezávislosti	nezávislost	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1765	#num#	k4
docházelo	docházet	k5eAaImAgNnS
ke	k	k7c3
stále	stále	k6eAd1
větším	veliký	k2eAgFnPc3d2
neshodám	neshoda	k1gFnPc3
mezi	mezi	k7c7
britskými	britský	k2eAgFnPc7d1
koloniemi	kolonie	k1gFnPc7
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
Velkou	velký	k2eAgFnSc7d1
Británií	Británie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
spory	spor	k1gInPc1
postupně	postupně	k6eAd1
přerostly	přerůst	k5eAaPmAgInP
v	v	k7c4
americkou	americký	k2eAgFnSc4d1
revoluci	revoluce	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
vyvrcholila	vyvrcholit	k5eAaPmAgFnS
válkou	válka	k1gFnSc7
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1773	#num#	k4
vypukly	vypuknout	k5eAaPmAgInP
otevřené	otevřený	k2eAgInPc1d1
nepokoje	nepokoj	k1gInPc1
v	v	k7c6
Bostonu	Boston	k1gInSc6
<g/>
,	,	kIx,
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
kolonie	kolonie	k1gFnSc2
Massachusetts	Massachusetts	k1gNnSc2
Bay	Bay	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
uzavřeli	uzavřít	k5eAaPmAgMnP
přístav	přístav	k1gInSc4
a	a	k8xC
začali	začít	k5eAaPmAgMnP
problém	problém	k1gInSc4
řešit	řešit	k5eAaImF
silou	síla	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američané	Američan	k1gMnPc1
reagovali	reagovat	k5eAaBmAgMnP
vytvořením	vytvoření	k1gNnSc7
Kontinentálního	kontinentální	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
koordinoval	koordinovat	k5eAaBmAgInS
postup	postup	k1gInSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
a	a	k8xC
postupně	postupně	k6eAd1
zformovali	zformovat	k5eAaPmAgMnP
kontinentální	kontinentální	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
1775	#num#	k4
se	se	k3xPyFc4
Britové	Brit	k1gMnPc1
pokusili	pokusit	k5eAaPmAgMnP
odzbrojit	odzbrojit	k5eAaPmF
massachusettskou	massachusettský	k2eAgFnSc4d1
domobranu	domobrana	k1gFnSc4
(	(	kIx(
<g/>
milici	milice	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
ve	v	k7c6
vojenském	vojenský	k2eAgInSc6d1
střetu	střet	k1gInSc6
u	u	k7c2
Lexingtonu	Lexington	k1gInSc2
a	a	k8xC
Concordu	Concorda	k1gFnSc4
neuspěli	uspět	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
bitvou	bitva	k1gFnSc7
začala	začít	k5eAaPmAgFnS
otevřená	otevřený	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američané	Američan	k1gMnPc1
oblehli	oblehnout	k5eAaPmAgMnP
britskou	britský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
kontrolovaný	kontrolovaný	k2eAgInSc1d1
Boston	Boston	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
obléhání	obléhání	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
bitvě	bitva	k1gFnSc3
u	u	k7c2
Bunker	Bunkra	k1gFnPc2
Hillu	Hill	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
britská	britský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
za	za	k7c4
cenu	cena	k1gFnSc4
těžkých	těžký	k2eAgFnPc2d1
ztrát	ztráta	k1gFnPc2
dosáhla	dosáhnout	k5eAaPmAgFnS
dílčího	dílčí	k2eAgInSc2d1
taktického	taktický	k2eAgInSc2d1
úspěchu	úspěch	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
však	však	k9
neměl	mít	k5eNaImAgInS
strategický	strategický	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
do	do	k7c2
čela	čelo	k1gNnSc2
americké	americký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
postavil	postavit	k5eAaPmAgInS
George	George	k1gInSc1
Washington	Washington	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
1776	#num#	k4
opustili	opustit	k5eAaPmAgMnP
Britové	Brit	k1gMnPc1
Boston	Boston	k1gInSc4
a	a	k8xC
začátkem	začátkem	k7c2
července	červenec	k1gInSc2
1776	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
zahájila	zahájit	k5eAaPmAgFnS
v	v	k7c6
létě	léto	k1gNnSc6
1776	#num#	k4
protiofenzivu	protiofenziva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shromáždila	shromáždit	k5eAaPmAgFnS
velkou	velký	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
velel	velet	k5eAaImAgInS
William	William	k1gInSc1
Howe	How	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
jež	jenž	k3xRgNnSc1
byla	být	k5eAaImAgFnS
posílená	posílená	k1gFnSc1
o	o	k7c4
německé	německý	k2eAgFnPc4d1
pomocné	pomocný	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
(	(	kIx(
<g/>
Hesenští	hesenský	k2eAgMnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
zaútočila	zaútočit	k5eAaPmAgFnS
na	na	k7c4
New	New	k1gFnSc4
York	York	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
po	po	k7c6
bitvě	bitva	k1gFnSc6
o	o	k7c4
Long	Long	k1gInSc4
Island	Island	k1gInSc4
obsadila	obsadit	k5eAaPmAgFnS
a	a	k8xC
držela	držet	k5eAaImAgFnS
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
zvítězili	zvítězit	k5eAaPmAgMnP
v	v	k7c6
několika	několik	k4yIc6
následných	následný	k2eAgFnPc6d1
bitvách	bitva	k1gFnPc6
<g/>
,	,	kIx,
ale	ale	k8xC
tyto	tento	k3xDgInPc1
úspěchy	úspěch	k1gInPc1
nakonec	nakonec	k6eAd1
vyzněly	vyznět	k5eAaImAgInP,k5eAaPmAgInP
do	do	k7c2
ztracena	ztraceno	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
díky	díky	k7c3
smělému	smělý	k2eAgInSc3d1
manévru	manévr	k1gInSc3
George	Georg	k1gMnSc2
Washingtona	Washington	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
v	v	k7c6
prosinci	prosinec	k1gInSc6
1776	#num#	k4
překročil	překročit	k5eAaPmAgMnS
řeku	řeka	k1gFnSc4
Delaware	Delawar	k1gMnSc5
a	a	k8xC
uštědřil	uštědřit	k5eAaPmAgInS
Hesenským	hesenský	k2eAgMnPc3d1
porážku	porážka	k1gFnSc4
u	u	k7c2
Trentonu	Trenton	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1777	#num#	k4
se	se	k3xPyFc4
britská	britský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
pokusila	pokusit	k5eAaPmAgFnS
rozdělit	rozdělit	k5eAaPmF
americké	americký	k2eAgInPc4d1
státy	stát	k1gInPc4
vedví	vedví	k6eAd1
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
že	že	k9
odřízne	odříznout	k5eAaPmIp3nS
Novou	nový	k2eAgFnSc4d1
Anglii	Anglie	k1gFnSc4
od	od	k7c2
ostatních	ostatní	k2eAgInPc2d1
rebelujících	rebelující	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
kampaň	kampaň	k1gFnSc1
však	však	k9
skončila	skončit	k5eAaPmAgFnS
britskou	britský	k2eAgFnSc7d1
porážkou	porážka	k1gFnSc7
u	u	k7c2
Saratogy	Saratoga	k1gFnSc2
v	v	k7c6
říjnu	říjen	k1gInSc6
1777	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1778	#num#	k4
se	se	k3xPyFc4
na	na	k7c4
stranu	strana	k1gFnSc4
USA	USA	kA
přidala	přidat	k5eAaPmAgFnS
Francie	Francie	k1gFnSc1
a	a	k8xC
v	v	k7c6
září	září	k1gNnSc6
1779	#num#	k4
i	i	k8xC
Španělsko	Španělsko	k1gNnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
výsledek	výsledek	k1gInSc4
války	válka	k1gFnSc2
výrazně	výrazně	k6eAd1
ovlivnilo	ovlivnit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
Francie	Francie	k1gFnSc1
sehrála	sehrát	k5eAaPmAgFnS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
,	,	kIx,
když	když	k8xS
zásobovala	zásobovat	k5eAaImAgFnS
americké	americký	k2eAgInPc4d1
patrioty	patriot	k1gMnPc4
financemi	finance	k1gFnPc7
a	a	k8xC
střelivem	střelivo	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
posléze	posléze	k6eAd1
vyslala	vyslat	k5eAaPmAgFnS
na	na	k7c4
pomoc	pomoc	k1gFnSc4
rebelům	rebel	k1gMnPc3
své	svůj	k3xOyFgNnSc4
námořnictvo	námořnictvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britskou	britský	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
zhoršila	zhoršit	k5eAaPmAgFnS
i	i	k9
válka	válka	k1gFnSc1
s	s	k7c7
Nizozemím	Nizozemí	k1gNnSc7
(	(	kIx(
<g/>
1780	#num#	k4
<g/>
–	–	k?
<g/>
1784	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Britové	Brit	k1gMnPc1
mezitím	mezitím	k6eAd1
přešli	přejít	k5eAaPmAgMnP
na	na	k7c6
tzv.	tzv.	kA
Jižní	jižní	k2eAgFnSc6d1
strategii	strategie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armáda	armáda	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
Charlesem	Charles	k1gMnSc7
Cornwallisem	Cornwallis	k1gInSc7
začala	začít	k5eAaPmAgFnS
operovat	operovat	k5eAaImF
v	v	k7c6
jižních	jižní	k2eAgInPc6d1
státech	stát	k1gInPc6
USA	USA	kA
(	(	kIx(
<g/>
Virginie	Virginie	k1gFnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
<g/>
,	,	kIx,
Georgie	Georgie	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
doufala	doufat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
podnítí	podnítit	k5eAaPmIp3nS
masivní	masivní	k2eAgNnSc1d1
probritské	probritský	k2eAgNnSc1d1
loyalistické	loyalistický	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
však	však	k9
nepovedlo	povést	k5eNaPmAgNnS
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
když	když	k8xS
zde	zde	k6eAd1
Britové	Brit	k1gMnPc1
zaznamenali	zaznamenat	k5eAaPmAgMnP
dílčí	dílčí	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
byli	být	k5eAaImAgMnP
zatlačováni	zatlačovat	k5eAaImNgMnP
do	do	k7c2
defenzívy	defenzíva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1781	#num#	k4
se	se	k3xPyFc4
Cornwallis	Cornwallis	k1gInSc1
stáhl	stáhnout	k5eAaPmAgInS
do	do	k7c2
přístavu	přístav	k1gInSc2
Yorktown	Yorktowna	k1gFnPc2
a	a	k8xC
vyčkával	vyčkávat	k5eAaImAgMnS
na	na	k7c4
pomoc	pomoc	k1gFnSc4
či	či	k8xC
evakuaci	evakuace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzsko-americká	francouzsko-americký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
však	však	k9
zabránila	zabránit	k5eAaPmAgFnS
oběma	dva	k4xCgFnPc3
alternativám	alternativa	k1gFnPc3
a	a	k8xC
Britové	Brit	k1gMnPc1
museli	muset	k5eAaImAgMnP
v	v	k7c6
říjnu	říjen	k1gInSc6
1781	#num#	k4
kapitulovat	kapitulovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
v	v	k7c6
zásadě	zásada	k1gFnSc6
skončily	skončit	k5eAaPmAgFnP
velké	velký	k2eAgFnPc1d1
vojenské	vojenský	k2eAgFnPc1d1
operace	operace	k1gFnPc1
a	a	k8xC
začalo	začít	k5eAaPmAgNnS
politické	politický	k2eAgNnSc1d1
vyjednávání	vyjednávání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Britský	britský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
prosadil	prosadit	k5eAaPmAgInS
v	v	k7c6
únoru	únor	k1gInSc6
1782	#num#	k4
zastavení	zastavení	k1gNnPc2
vojenských	vojenský	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
proti	proti	k7c3
Američanům	Američan	k1gMnPc3
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
válka	válka	k1gFnSc1
proti	proti	k7c3
Francii	Francie	k1gFnSc3
a	a	k8xC
Španělsku	Španělsko	k1gNnSc3
pokračovala	pokračovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
1783	#num#	k4
se	se	k3xPyFc4
USA	USA	kA
a	a	k8xC
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
dohodly	dohodnout	k5eAaPmAgInP
na	na	k7c6
mírové	mírový	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
uzavřená	uzavřený	k2eAgFnSc1d1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
oficiálně	oficiálně	k6eAd1
uznali	uznat	k5eAaPmAgMnP
nezávislost	nezávislost	k1gFnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Pozadí	pozadí	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Třináct	třináct	k4xCc4
kolonií	kolonie	k1gFnPc2
a	a	k8xC
Americká	americký	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
britských	britský	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
(	(	kIx(
<g/>
stav	stav	k1gInSc1
mezi	mezi	k7c7
lety	let	k1gInPc7
1763	#num#	k4
<g/>
-	-	kIx~
<g/>
76	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Období	období	k1gNnSc1
revoluce	revoluce	k1gFnSc2
začalo	začít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1763	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Francie	Francie	k1gFnSc1
přestala	přestat	k5eAaPmAgFnS
vojensky	vojensky	k6eAd1
ohrožovat	ohrožovat	k5eAaImF
britské	britský	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijetím	přijetí	k1gNnSc7
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
kolonie	kolonie	k1gFnPc1
měly	mít	k5eAaImAgFnP
platit	platit	k5eAaImF
značnou	značný	k2eAgFnSc4d1
část	část	k1gFnSc4
výdajů	výdaj	k1gInPc2
za	za	k7c4
jejich	jejich	k3xOp3gFnSc4
obranu	obrana	k1gFnSc4
<g/>
,	,	kIx,
zavedla	zavést	k5eAaPmAgFnS
Británie	Británie	k1gFnSc1
řadu	řad	k1gInSc2
daní	daň	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
velice	velice	k6eAd1
nepopulární	populární	k2eNgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koloniím	kolonie	k1gFnPc3
chyběli	chybět	k5eAaImAgMnP
volení	volený	k2eAgMnPc1d1
zástupci	zástupce	k1gMnPc1
v	v	k7c6
britském	britský	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k4c1
kolonistů	kolonista	k1gMnPc2
považovalo	považovat	k5eAaImAgNnS
zákony	zákon	k1gInPc4
za	za	k7c4
neoprávněné	oprávněný	k2eNgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
protestech	protest	k1gInPc6
v	v	k7c6
Bostonu	Boston	k1gInSc6
vyslala	vyslat	k5eAaPmAgFnS
Británie	Británie	k1gFnSc1
vojenské	vojenský	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
a	a	k8xC
Američané	Američan	k1gMnPc1
povolali	povolat	k5eAaPmAgMnP
do	do	k7c2
služby	služba	k1gFnSc2
milice	milice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boje	boj	k1gInSc2
vypukly	vypuknout	k5eAaPmAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1775	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
bylo	být	k5eAaImAgNnS
asi	asi	k9
15-20	15-20	k4
%	%	kIx~
obyvatelstva	obyvatelstvo	k1gNnSc2
loajální	loajální	k2eAgFnSc3d1
britské	britský	k2eAgFnSc3d1
monarchii	monarchie	k1gFnSc3
<g/>
,	,	kIx,
přes	přes	k7c4
válku	válka	k1gFnSc4
již	již	k6eAd1
vlastenci	vlastenec	k1gMnPc1
kontrolovali	kontrolovat	k5eAaImAgMnP
80-90	80-90	k4
%	%	kIx~
svého	svůj	k3xOyFgNnSc2
území	území	k1gNnSc2
a	a	k8xC
Británie	Británie	k1gFnSc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
moci	moc	k1gFnSc6
udržela	udržet	k5eAaPmAgFnS
jen	jen	k9
několik	několik	k4yIc4
pobřežních	pobřežní	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1776	#num#	k4
zástupci	zástupce	k1gMnPc1
třinácti	třináct	k4xCc2
kolonií	kolonie	k1gFnPc2
jednomyslně	jednomyslně	k6eAd1
odhlasovali	odhlasovat	k5eAaPmAgMnP
přijetí	přijetí	k1gNnSc4
Deklarace	deklarace	k1gFnSc2
nezávislosti	nezávislost	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc7,k3yIgFnSc7,k3yRgFnSc7
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
nezávislost	nezávislost	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
na	na	k7c6
britské	britský	k2eAgFnSc6d1
koruně	koruna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američané	Američan	k1gMnPc1
vytvořili	vytvořit	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1778	#num#	k4
koalici	koalice	k1gFnSc4
s	s	k7c7
Francií	Francie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
jim	on	k3xPp3gMnPc3
pomohla	pomoct	k5eAaPmAgFnS
vyrovnat	vyrovnat	k5eAaBmF,k5eAaPmF
vojenské	vojenský	k2eAgFnSc3d1
a	a	k8xC
námořní	námořní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc1
britské	britský	k2eAgFnPc1d1
armády	armáda	k1gFnPc1
byly	být	k5eAaImAgFnP
zajaty	zajmout	k5eAaPmNgFnP
u	u	k7c2
Saratogy	Saratoga	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1777	#num#	k4
a	a	k8xC
Yorktownu	Yorktown	k1gInSc6
v	v	k7c4
1781	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
podepsání	podepsání	k1gNnSc3
Pařížského	pařížský	k2eAgInSc2d1
míru	mír	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1783	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
v	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
ohraničeny	ohraničit	k5eAaPmNgInP
na	na	k7c6
severu	sever	k1gInSc6
britskou	britský	k2eAgFnSc4d1
Kanadou	Kanada	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc2
španělskou	španělský	k2eAgFnSc7d1
Floridou	Florida	k1gFnSc7
a	a	k8xC
na	na	k7c6
západě	západ	k1gInSc6
řekou	řeka	k1gFnSc7
Mississippi	Mississippi	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Žádné	žádný	k3yNgNnSc1
zdanění	zdanění	k1gNnSc1
bez	bez	k7c2
zastoupení	zastoupení	k1gNnSc2
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1763	#num#	k4
vlastnila	vlastnit	k5eAaImAgFnS
Velká	velká	k1gFnSc1
Británie	Británie	k1gFnSc2
velké	velký	k2eAgNnSc4d1
území	území	k1gNnSc4
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
třinácti	třináct	k4xCc2
kolonií	kolonie	k1gFnPc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
dalších	další	k2eAgNnPc2d1
22	#num#	k4
menších	malý	k2eAgNnPc2d2
spravováno	spravovat	k5eAaImNgNnS
přímo	přímo	k6eAd1
královskými	královský	k2eAgMnPc7d1
guvernéry	guvernér	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
v	v	k7c6
sedmileté	sedmiletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
přineslo	přinést	k5eAaPmAgNnS
Británii	Británie	k1gFnSc4
území	území	k1gNnSc2
Nové	Nové	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
(	(	kIx(
<g/>
Kanada	Kanada	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Španělsku	Španělsko	k1gNnSc6
Floridu	Florida	k1gFnSc4
a	a	k8xC
Indianu	Indiana	k1gFnSc4
<g/>
,	,	kIx,
východně	východně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Mississippi	Mississippi	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1765	#num#	k4
byli	být	k5eAaImAgMnP
kolonisté	kolonista	k1gMnPc1
stále	stále	k6eAd1
považováni	považován	k2eAgMnPc1d1
za	za	k7c4
subjekty	subjekt	k1gInPc4
věrné	věrný	k2eAgFnSc3d1
královské	královský	k2eAgFnSc3d1
koruně	koruna	k1gFnSc3
<g/>
,	,	kIx,
se	s	k7c7
stejnými	stejný	k2eAgNnPc7d1
právy	právo	k1gNnPc7
a	a	k8xC
povinnostmi	povinnost	k1gFnPc7
jako	jako	k8xS,k8xC
subjekty	subjekt	k1gInPc7
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Britská	britský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
své	svůj	k3xOyFgFnPc4
americké	americký	k2eAgFnPc4d1
kolonie	kolonie	k1gFnPc4
zatížit	zatížit	k5eAaPmF
daněmi	daň	k1gFnPc7
<g/>
,	,	kIx,
primárně	primárně	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
pomohly	pomoct	k5eAaPmAgInP
nést	nést	k5eAaImF
náklady	náklad	k1gInPc1
na	na	k7c4
jejich	jejich	k3xOp3gFnSc4
obranu	obrana	k1gFnSc4
před	před	k7c7
Francií	Francie	k1gFnSc7
během	během	k7c2
sedmileté	sedmiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problém	problém	k1gInSc4
kolonisté	kolonista	k1gMnPc1
neviděli	vidět	k5eNaImAgMnP
ani	ani	k8xC
tak	tak	k6eAd1
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
daní	daň	k1gFnPc2
(	(	kIx(
<g/>
daně	daň	k1gFnPc1
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
byly	být	k5eAaImAgFnP
nižší	nízký	k2eAgFnPc1d2
<g/>
,	,	kIx,
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
těmi	ten	k3xDgMnPc7
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
platil	platit	k5eAaImAgMnS
průměrný	průměrný	k2eAgMnSc1d1
občan	občan	k1gMnSc1
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
faktu	fakt	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
nové	nový	k2eAgFnPc1d1
daně	daň	k1gFnPc1
nebyly	být	k5eNaImAgFnP
s	s	k7c7
kolonisty	kolonista	k1gMnPc7
řádně	řádně	k6eAd1
projednány	projednat	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
neměli	mít	k5eNaImAgMnP
žádné	žádný	k3yNgNnSc4
zastoupení	zastoupení	k1gNnSc4
v	v	k7c6
britském	britský	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
<g/>
,	,	kIx,
fráze	fráze	k1gFnSc1
„	„	k?
<g/>
Žádné	žádný	k3yNgNnSc4
zdanění	zdanění	k1gNnSc4
bez	bez	k7c2
zastoupení	zastoupení	k1gNnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
No	no	k9
Taxation	Taxation	k1gInSc1
without	without	k1gMnSc1
Representation	Representation	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
populární	populární	k2eAgFnSc1d1
v	v	k7c6
mnoha	mnoho	k4c6
amerických	americký	k2eAgInPc6d1
kruzích	kruh	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Británie	Británie	k1gFnSc1
regulovala	regulovat	k5eAaImAgFnS
hospodářství	hospodářství	k1gNnSc4
kolonií	kolonie	k1gFnPc2
skrze	skrze	k?
námořní	námořní	k2eAgInPc4d1
zákony	zákon	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
odpovídaly	odpovídat	k5eAaImAgFnP
zásadám	zásada	k1gFnPc3
merkantilismu	merkantilismus	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
říkaly	říkat	k5eAaImAgInP
<g/>
,	,	kIx,
že	že	k8xS
všechno	všechen	k3xTgNnSc1
z	z	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
prospívá	prospívat	k5eAaImIp3nS
impérium	impérium	k1gNnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dobré	dobrý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozšířené	rozšířený	k2eAgNnSc1d1
vyhýbání	vyhýbání	k1gNnPc1
se	se	k3xPyFc4
tomuto	tento	k3xDgInSc3
zákonu	zákon	k1gInSc3
bylo	být	k5eAaImAgNnS
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
tolerováno	tolerován	k2eAgNnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
později	pozdě	k6eAd2
začal	začít	k5eAaPmAgInS
být	být	k5eAaImF
razantně	razantně	k6eAd1
vymáhán	vymáhán	k2eAgMnSc1d1
skrze	skrze	k?
výnosy	výnos	k1gInPc4
„	„	k?
<g/>
Writs	Writs	k1gInSc1
of	of	k?
Assistance	Assistance	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1761	#num#	k4
Massachusettský	massachusettský	k2eAgMnSc1d1
právník	právník	k1gMnSc1
James	James	k1gMnSc1
Otis	Otis	k1gInSc4
u	u	k7c2
soudu	soud	k1gInSc2
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgInPc1
výnosy	výnos	k1gInPc1
poškozují	poškozovat	k5eAaImIp3nP
ústavní	ústavní	k2eAgNnPc4d1
práva	právo	k1gNnPc4
kolonistů	kolonista	k1gMnPc2
a	a	k8xC
případ	případ	k1gInSc1
prohrál	prohrát	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
Adams	Adams	k1gInSc1
po	po	k7c6
výroku	výrok	k1gInSc6
soudu	soud	k1gInSc2
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Americká	americký	k2eAgFnSc1d1
nezávislost	nezávislost	k1gFnSc1
byla	být	k5eAaImAgFnS
zrozena	zrodit	k5eAaPmNgFnS
tehdy	tehdy	k6eAd1
a	a	k8xC
tam	tam	k6eAd1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Kolkový	kolkový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
stmeluje	stmelovat	k5eAaImIp3nS
kolonisty	kolonista	k1gMnPc4
</s>
<s>
Bostonský	bostonský	k2eAgInSc4d1
masakr	masakr	k1gInSc4
(	(	kIx(
<g/>
dobová	dobový	k2eAgFnSc1d1
ilustrace	ilustrace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1764	#num#	k4
uzákonil	uzákonit	k5eAaPmAgInS
britský	britský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
cukrový	cukrový	k2eAgInSc1d1
a	a	k8xC
měnový	měnový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
podporující	podporující	k2eAgInSc1d1
útlak	útlak	k1gInSc1
kolonistů	kolonista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protest	protest	k1gInSc1
vedl	vést	k5eAaImAgInS
k	k	k7c3
nové	nový	k2eAgFnSc3d1
silné	silný	k2eAgFnSc3d1
zbrani	zbraň	k1gFnSc3
<g/>
,	,	kIx,
systematickému	systematický	k2eAgInSc3d1
bojkotu	bojkot	k1gInSc3
britského	britský	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
poté	poté	k6eAd1
zatlačili	zatlačit	k5eAaPmAgMnP
kolonisty	kolonista	k1gMnPc7
ještě	ještě	k6eAd1
dále	daleko	k6eAd2
<g/>
,	,	kIx,
když	když	k8xS
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
uzákonili	uzákonit	k5eAaPmAgMnP
Quartering	Quartering	k1gInSc4
Act	Act	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
stanovil	stanovit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
určitých	určitý	k2eAgNnPc6d1
územích	území	k1gNnPc6
se	se	k3xPyFc4
mají	mít	k5eAaImIp3nP
občané	občan	k1gMnPc1
na	na	k7c4
své	svůj	k3xOyFgInPc4
náklady	náklad	k1gInPc4
postarat	postarat	k5eAaPmF
o	o	k7c4
ubytování	ubytování	k1gNnSc4
a	a	k8xC
stravu	strava	k1gFnSc4
pro	pro	k7c4
britské	britský	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1765	#num#	k4
byla	být	k5eAaImAgFnS
kolkovným	kolkovné	k1gNnPc3
zákonem	zákon	k1gInSc7
poprvé	poprvé	k6eAd1
zavedena	zaveden	k2eAgFnSc1d1
přímá	přímý	k2eAgFnSc1d1
daň	daň	k1gFnSc1
pro	pro	k7c4
kolonisty	kolonista	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
noviny	novina	k1gFnPc1
<g/>
,	,	kIx,
almanachy	almanach	k1gInPc1
<g/>
,	,	kIx,
pamflety	pamflet	k1gInPc1
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
stolní	stolní	k2eAgFnPc4d1
hry	hra	k1gFnPc4
a	a	k8xC
karty	karta	k1gFnPc4
musely	muset	k5eAaImAgFnP
být	být	k5eAaImF
opatřeny	opatřit	k5eAaPmNgInP
zvláštním	zvláštní	k2eAgInSc7d1
kolkem	kolek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Všech	všecek	k3xTgFnPc2
třináct	třináct	k4xCc1
kolonií	kolonie	k1gFnPc2
proti	proti	k7c3
výše	vysoce	k6eAd2
uvedenému	uvedený	k2eAgInSc3d1
vehementně	vehementně	k6eAd1
protestovalo	protestovat	k5eAaBmAgNnS
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
populární	populární	k2eAgMnPc1d1
vůdci	vůdce	k1gMnPc1
jako	jako	k9
například	například	k6eAd1
Patrik	Patrik	k1gMnSc1
Henry	Henry	k1gMnSc1
ve	v	k7c6
Virginii	Virginie	k1gFnSc6
a	a	k8xC
James	James	k1gInSc1
Otis	Otisa	k1gFnPc2
v	v	k7c6
Massachusetts	Massachusetts	k1gNnSc6
burcovali	burcovat	k5eAaImAgMnP
občany	občan	k1gMnPc4
k	k	k7c3
protestům	protest	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mnoha	mnoho	k4c6
městech	město	k1gNnPc6
vznikl	vzniknout	k5eAaPmAgInS
tajný	tajný	k2eAgInSc1d1
spolek	spolek	k1gInSc1
„	„	k?
<g/>
Sons	Sonsa	k1gFnPc2
of	of	k?
Liberty	Libert	k1gInPc1
<g/>
“	“	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
:	:	kIx,
Synové	syn	k1gMnPc1
svobody	svoboda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
vyhrožoval	vyhrožovat	k5eAaImAgInS
násilím	násilí	k1gNnSc7
pokud	pokud	k8xS
někdo	někdo	k3yInSc1
bude	být	k5eAaImBp3nS
prodávat	prodávat	k5eAaImF
kolky	kolek	k1gInPc4
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
z	z	k7c2
obav	obava	k1gFnPc2
raději	rád	k6eAd2
nikdo	nikdo	k3yNnSc1
neprodával	prodávat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Bostonu	Boston	k1gInSc6
spálili	spálit	k5eAaPmAgMnP
členové	člen	k1gMnPc1
Sons	Sonsa	k1gFnPc2
of	of	k?
Liberty	Libert	k1gInPc1
záznamy	záznam	k1gInPc1
soudu	soud	k1gInSc2
vice-admirality	vice-admiralita	k1gFnSc2
a	a	k8xC
zabrali	zabrat	k5eAaPmAgMnP
elegantní	elegantní	k2eAgInSc4d1
dům	dům	k1gInSc4
šéfa	šéf	k1gMnSc2
spravedlnosti	spravedlnost	k1gFnSc2
Thomase	Thomas	k1gMnSc2
Hutchinsona	Hutchinson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
zákonodárných	zákonodárný	k2eAgInPc2d1
sborů	sbor	k1gInPc2
požadovalo	požadovat	k5eAaImAgNnS
společnou	společný	k2eAgFnSc4d1
akci	akce	k1gFnSc4
<g/>
:	:	kIx,
9	#num#	k4
kolonií	kolonie	k1gFnPc2
nakonec	nakonec	k6eAd1
vyslalo	vyslat	k5eAaPmAgNnS
své	svůj	k3xOyFgMnPc4
delegáty	delegát	k1gMnPc4
na	na	k7c4
kongres	kongres	k1gInSc4
o	o	k7c6
kolkovném	kolkovné	k1gNnSc6
zákoně	zákon	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
v	v	k7c6
říjnu	říjen	k1gInSc6
1765	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
rozprav	rozprava	k1gFnPc2
vedených	vedený	k2eAgFnPc2d1
Johnem	John	k1gMnSc7
Dickinsonem	Dickinson	k1gInSc7
byla	být	k5eAaImAgFnS
vytvořena	vytvořen	k2eAgFnSc1d1
Deklarace	deklarace	k1gFnSc1
práv	právo	k1gNnPc2
a	a	k8xC
stížností	stížnost	k1gFnPc2
(	(	kIx(
<g/>
Declaration	Declaration	k1gInSc1
of	of	k?
Rights	Rights	k1gInSc1
and	and	k?
Grievances	Grievances	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
konsenzu	konsenz	k1gInSc3
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yRgMnSc2,k3yQgMnSc2,k3yIgMnSc2
jsou	být	k5eAaImIp3nP
daně	daň	k1gFnPc1
bezdůvodné	bezdůvodný	k2eAgFnPc1d1
a	a	k8xC
porušují	porušovat	k5eAaImIp3nP
přirozená	přirozený	k2eAgNnPc1d1
odvěká	odvěký	k2eAgNnPc1d1
práva	právo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejná	veřejný	k2eAgFnSc1d1
váha	váha	k1gFnSc1
argumentu	argument	k1gInSc2
vyprovokovala	vyprovokovat	k5eAaPmAgFnS
ekonomický	ekonomický	k2eAgInSc4d1
bojkot	bojkot	k1gInSc4
britského	britský	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
import	import	k1gInSc1
do	do	k7c2
kolonií	kolonie	k1gFnPc2
poklesl	poklesnout	k5eAaPmAgInS
z	z	k7c2
2	#num#	k4
250	#num#	k4
000	#num#	k4
liber	libra	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1764	#num#	k4
na	na	k7c4
1	#num#	k4
944	#num#	k4
000	#num#	k4
liber	libra	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1765	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Když	když	k8xS
se	se	k3xPyFc4
k	k	k7c3
moci	moc	k1gFnSc3
dostala	dostat	k5eAaPmAgFnS
Rockingamova	Rockingamův	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
,	,	kIx,
zahájil	zahájit	k5eAaPmAgInS
britský	britský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
debatu	debata	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
kolkový	kolkový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
zrušit	zrušit	k5eAaPmF
<g/>
,	,	kIx,
nebo	nebo	k8xC
vyslat	vyslat	k5eAaPmF
armádu	armáda	k1gFnSc4
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
prosazování	prosazování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Benjamin	Benjamin	k1gMnSc1
Franklin	Franklin	k1gInSc4
vysvětloval	vysvětlovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
kolonie	kolonie	k1gFnPc1
jsou	být	k5eAaImIp3nP
vyčerpány	vyčerpat	k5eAaPmNgFnP
po	po	k7c6
materiální	materiální	k2eAgFnSc6d1
i	i	k8xC
fyzické	fyzický	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
ze	z	k7c2
série	série	k1gFnSc2
válek	válka	k1gFnPc2
s	s	k7c7
Francouzi	Francouz	k1gMnPc7
a	a	k8xC
Indiány	Indián	k1gMnPc7
<g/>
,	,	kIx,
že	že	k8xS
krvácejí	krvácet	k5eAaImIp3nP
při	při	k7c6
obraně	obrana	k1gFnSc6
Britského	britský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
a	a	k8xC
že	že	k8xS
daně	daň	k1gFnPc1
za	za	k7c4
tyto	tento	k3xDgInPc4
války	válek	k1gInPc4
jsou	být	k5eAaImIp3nP
nespravedlivé	spravedlivý	k2eNgInPc1d1
a	a	k8xC
mohou	moct	k5eAaImIp3nP
přinést	přinést	k5eAaPmF
pouze	pouze	k6eAd1
rebelii	rebelie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlament	parlament	k1gInSc1
souhlasil	souhlasit	k5eAaImAgInS
a	a	k8xC
daň	daň	k1gFnSc4
tedy	tedy	k9
stáhl	stáhnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přehlašovacím	přehlašovací	k2eAgInSc6d1
aktu	akt	k1gInSc6
z	z	k7c2
března	březen	k1gInSc2
1766	#num#	k4
však	však	k9
požadoval	požadovat	k5eAaImAgMnS
ponechat	ponechat	k5eAaPmF
si	se	k3xPyFc3
moc	moc	k6eAd1
určovat	určovat	k5eAaImF
zákony	zákon	k1gInPc4
koloniím	kolonie	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Bostonu	Boston	k1gInSc6
se	s	k7c7
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1770	#num#	k4
kolem	kolem	k7c2
skupiny	skupina	k1gFnSc2
britských	britský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
shromáždil	shromáždit	k5eAaPmAgInS
dav	dav	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
chovat	chovat	k5eAaImF
agresivně	agresivně	k6eAd1
a	a	k8xC
házel	házet	k5eAaImAgMnS
sněhové	sněhový	k2eAgFnSc2d1
koule	koule	k1gFnSc2
a	a	k8xC
smetí	smetí	k1gNnSc2
na	na	k7c4
vojáky	voják	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
zmatku	zmatek	k1gInSc6
začali	začít	k5eAaPmAgMnP
vojáci	voják	k1gMnPc1
do	do	k7c2
davu	dav	k1gInSc2
střílet	střílet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zraněno	zranit	k5eAaPmNgNnS
jedenáct	jedenáct	k4xCc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
pět	pět	k4xCc1
lidí	člověk	k1gMnPc2
následkům	následek	k1gInPc3
vážných	vážná	k1gFnPc2
zranění	zranění	k1gNnSc1
podlehlo	podlehnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Událost	událost	k1gFnSc1
se	se	k3xPyFc4
rychle	rychle	k6eAd1
začala	začít	k5eAaPmAgFnS
označovat	označovat	k5eAaImF
jako	jako	k9
Bostonský	bostonský	k2eAgInSc4d1
masakr	masakr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehánění	přehánění	k1gNnSc1
a	a	k8xC
rozšiřování	rozšiřování	k1gNnSc1
informací	informace	k1gFnPc2
o	o	k7c6
masakru	masakr	k1gInSc6
změnilo	změnit	k5eAaPmAgNnS
postoj	postoj	k1gInSc4
mnoha	mnoho	k4c2
kolonistů	kolonista	k1gMnPc2
vůči	vůči	k7c3
Británii	Británie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Událost	událost	k1gFnSc1
také	také	k9
roztočila	roztočit	k5eAaPmAgFnS
spirálu	spirála	k1gFnSc4
zhoršování	zhoršování	k1gNnSc2
vztahů	vztah	k1gInPc2
mezi	mezi	k7c7
Brity	Brit	k1gMnPc7
a	a	k8xC
koloniemi	kolonie	k1gFnPc7
<g/>
,	,	kIx,
zvlášť	zvlášť	k6eAd1
v	v	k7c6
Massachusetts	Massachusetts	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Bostonské	bostonský	k2eAgNnSc1d1
pití	pití	k1gNnSc1
čaje	čaj	k1gInSc2
(	(	kIx(
<g/>
dobová	dobový	k2eAgFnSc1d1
kresba	kresba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1773	#num#	k4
schválila	schválit	k5eAaPmAgFnS
Britská	britský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
Townshend	Townshend	k1gMnSc1
Act	Act	k1gMnSc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
byl	být	k5eAaImAgMnS
ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
Charles	Charles	k1gMnSc1
Townshend	Townshend	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
zdanil	zdanit	k5eAaPmAgInS
několik	několik	k4yIc4
podstatných	podstatný	k2eAgFnPc2d1
komodit	komodita	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
papíru	papír	k1gInSc2
<g/>
,	,	kIx,
skla	sklo	k1gNnSc2
a	a	k8xC
čaje	čaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhořčení	rozhořčený	k2eAgMnPc1d1
kolonisté	kolonista	k1gMnPc1
zorganizovali	zorganizovat	k5eAaPmAgMnP
bojkot	bojkot	k1gInSc4
britského	britský	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
skupina	skupina	k1gFnSc1
mužů	muž	k1gMnPc2
vedených	vedený	k2eAgInPc2d1
Samuelem	Samuel	k1gMnSc7
Adamsem	Adamso	k1gNnSc7
a	a	k8xC
převlečených	převlečený	k2eAgMnPc2d1
za	za	k7c4
mohavské	mohavský	k2eAgMnPc4d1
indiány	indián	k1gMnPc4
vkradlo	vkradnout	k5eAaPmAgNnS
na	na	k7c4
britskou	britský	k2eAgFnSc4d1
obchodní	obchodní	k2eAgFnSc4d1
loď	loď	k1gFnSc4
s	s	k7c7
čajem	čaj	k1gInSc7
a	a	k8xC
vysypala	vysypat	k5eAaPmAgFnS
do	do	k7c2
moře	moře	k1gNnSc2
zboží	zboží	k1gNnSc2
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
zhruba	zhruba	k6eAd1
10	#num#	k4
000	#num#	k4
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
známou	známý	k2eAgFnSc7d1
jako	jako	k8xC,k8xS
bostonské	bostonský	k2eAgNnSc1d1
pití	pití	k1gNnSc1
čaje	čaj	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
„	„	k?
<g/>
Boston	Boston	k1gInSc1
Tea	Tea	k1gFnSc1
party	party	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Britská	britský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
odpověděla	odpovědět	k5eAaPmAgFnS
zavedením	zavedení	k1gNnSc7
několika	několik	k4yIc2
nařízení	nařízení	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
známá	známý	k2eAgFnSc1d1
mezi	mezi	k7c4
kolonisty	kolonista	k1gMnPc4
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Intolerable	Intolerable	k1gFnSc1
Acts	Acts	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
nepřijatelná	přijatelný	k2eNgNnPc1d1
nařízení	nařízení	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
spíše	spíše	k9
známa	znám	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Coercive	Coerciev	k1gFnSc2
Acts	Actsa	k1gFnPc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
česky	česky	k6eAd1
donucovací	donucovací	k2eAgInPc4d1
zákony	zákon	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
obsahovala	obsahovat	k5eAaImAgFnS
také	také	k9
uzavření	uzavření	k1gNnSc4
bostonského	bostonský	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
kolonie	kolonie	k1gFnSc1
neodškodní	odškodnit	k5eNaPmIp3nS
obchodníky	obchodník	k1gMnPc4
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yRgMnPc3,k3yIgMnPc3
bylo	být	k5eAaImAgNnS
zničeno	zničit	k5eAaPmNgNnS
zboží	zboží	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
ještě	ještě	k6eAd1
důležitějším	důležitý	k2eAgInSc7d2
faktem	fakt	k1gInSc7
bylo	být	k5eAaImAgNnS
sebrání	sebrání	k1gNnSc1
práva	právo	k1gNnSc2
na	na	k7c4
samosprávu	samospráva	k1gFnSc4
kolonie	kolonie	k1gFnSc2
Massachusetts	Massachusetts	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
zákony	zákon	k1gInPc4
dále	daleko	k6eAd2
vedly	vést	k5eAaImAgInP
k	k	k7c3
zhoršení	zhoršení	k1gNnSc3
mínění	mínění	k1gNnSc2
kolonistů	kolonista	k1gMnPc2
o	o	k7c6
Británii	Británie	k1gFnSc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
klíčových	klíčový	k2eAgInPc2d1
bodů	bod	k1gInPc2
na	na	k7c6
cestě	cesta	k1gFnSc6
k	k	k7c3
revoluci	revoluce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Liberalismus	liberalismus	k1gInSc1
a	a	k8xC
republikanismus	republikanismus	k1gInSc1
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1
Johna	John	k1gMnSc2
Lockeho	Locke	k1gMnSc2
o	o	k7c6
liberalismu	liberalismus	k1gInSc6
<g/>
,	,	kIx,
výrazně	výrazně	k6eAd1
ovlivnily	ovlivnit	k5eAaPmAgFnP
politické	politický	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
v	v	k7c6
pozadí	pozadí	k1gNnSc6
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
„	„	k?
<g/>
teorie	teorie	k1gFnSc2
společenské	společenský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
“	“	k?
zahrnovala	zahrnovat	k5eAaImAgFnS
přirozené	přirozený	k2eAgNnSc4d1
právo	právo	k1gNnSc4
lidu	lid	k1gInSc2
sesadit	sesadit	k5eAaPmF
svého	svůj	k3xOyFgMnSc4
vůdce	vůdce	k1gMnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
jim	on	k3xPp3gMnPc3
byla	být	k5eAaImAgNnP
upřena	upřen	k2eAgNnPc1d1
historická	historický	k2eAgNnPc1d1
práva	právo	k1gNnPc1
<g/>
,	,	kIx,
jaká	jaký	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
mají	mít	k5eAaImIp3nP
Angličané	Angličan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
sepisování	sepisování	k1gNnSc6
státní	státní	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
použili	použít	k5eAaPmAgMnP
Američané	Američan	k1gMnPc1
Montesquieuovu	Montesquieuův	k2eAgFnSc4d1
analýzu	analýza	k1gFnSc4
ideálně	ideálně	k6eAd1
vyvážené	vyvážený	k2eAgInPc4d1
britské	britský	k2eAgInPc4d1
ústavy	ústav	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Motivační	motivační	k2eAgFnSc7d1
silou	síla	k1gFnSc7
v	v	k7c6
pozadí	pozadí	k1gNnSc6
revoluce	revoluce	k1gFnSc2
byla	být	k5eAaImAgFnS
americká	americký	k2eAgFnSc1d1
„	„	k?
<g/>
nástavba	nástavba	k1gFnSc1
<g/>
“	“	k?
politické	politický	k2eAgFnSc2d1
ideologie	ideologie	k1gFnSc2
zvané	zvaný	k2eAgInPc1d1
republikanismus	republikanismus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
dominantní	dominantní	k2eAgMnSc1d1
v	v	k7c6
koloniích	kolonie	k1gFnPc6
do	do	k7c2
roku	rok	k1gInSc2
1775	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemská	zemský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
country	country	k2eAgFnSc1d1
party	party	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
kritizovala	kritizovat	k5eAaImAgFnS
britskou	britský	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
a	a	k8xC
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS
své	svůj	k3xOyFgFnPc4
obavy	obava	k1gFnPc4
z	z	k7c2
korupce	korupce	k1gFnSc2
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
silný	silný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
americké	americký	k2eAgMnPc4d1
politiky	politik	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolonisté	kolonista	k1gMnPc1
spojovali	spojovat	k5eAaImAgMnP
„	„	k?
<g/>
dvůr	dvůr	k1gInSc1
<g/>
“	“	k?
s	s	k7c7
luxusem	luxus	k1gInSc7
a	a	k8xC
dědičnou	dědičný	k2eAgFnSc7d1
aristokracií	aristokracie	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
Američané	Američan	k1gMnPc1
stále	stále	k6eAd1
více	hodně	k6eAd2
odsuzovali	odsuzovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korupce	korupce	k1gFnSc1
byla	být	k5eAaImAgFnS
největším	veliký	k2eAgNnSc7d3
potenciálním	potenciální	k2eAgNnSc7d1
zlem	zlo	k1gNnSc7
a	a	k8xC
občanská	občanský	k2eAgFnSc1d1
ctnost	ctnost	k1gFnSc1
vyžadovala	vyžadovat	k5eAaImAgFnS
postavit	postavit	k5eAaPmF
občanskou	občanský	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
proti	proti	k7c3
vlastním	vlastní	k2eAgInPc3d1
zájmům	zájem	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muži	muž	k1gMnPc1
měli	mít	k5eAaImAgMnP
občanskou	občanský	k2eAgFnSc4d1
povinnost	povinnost	k1gFnSc4
bojovat	bojovat	k5eAaImF
za	za	k7c4
vlast	vlast	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Pro	pro	k7c4
ženy	žena	k1gFnPc4
se	s	k7c7
„	„	k?
<g/>
republikové	republikový	k2eAgNnSc4d1
mateřství	mateřství	k1gNnSc4
<g/>
“	“	k?
stalo	stát	k5eAaPmAgNnS
ideálem	ideál	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Prvořadou	prvořadý	k2eAgFnSc7d1
povinností	povinnost	k1gFnSc7
žen	žena	k1gFnPc2
republikánek	republikánka	k1gFnPc2
bylo	být	k5eAaImAgNnS
vštípit	vštípit	k5eAaPmF
republikánské	republikánský	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
jejich	jejich	k3xOp3gFnPc3
dětem	dítě	k1gFnPc3
a	a	k8xC
vyvarovat	vyvarovat	k5eAaPmF
se	se	k3xPyFc4
luxusu	luxus	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
„	„	k?
<g/>
Zakládající	zakládající	k2eAgMnPc1d1
otcové	otec	k1gMnPc1
(	(	kIx(
<g/>
Founding	Founding	k1gInSc1
Fathers	Fathers	k1gInSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
byli	být	k5eAaImAgMnP
silnými	silný	k2eAgMnPc7d1
obhájci	obhájce	k1gMnPc7
republikanismu	republikanismus	k1gInSc2
<g/>
,	,	kIx,
zvlášť	zvlášť	k6eAd1
<g/>
:	:	kIx,
Samuel	Samuel	k1gMnSc1
Adams	Adamsa	k1gFnPc2
<g/>
,	,	kIx,
Patrick	Patrick	k1gMnSc1
Henry	Henry	k1gMnSc1
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
Paine	Pain	k1gInSc5
<g/>
,	,	kIx,
Benjamin	Benjamin	k1gMnSc1
Franklin	Franklin	k1gInSc1
<g/>
,	,	kIx,
George	George	k1gInSc1
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
Jefferson	Jefferson	k1gMnSc1
a	a	k8xC
John	John	k1gMnSc1
Adams	Adamsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Spor	spor	k1gInSc1
o	o	k7c4
západní	západní	k2eAgNnPc4d1
teritoria	teritorium	k1gNnPc4
</s>
<s>
Královský	královský	k2eAgInSc4d1
dekret	dekret	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1763	#num#	k4
omezil	omezit	k5eAaPmAgInS
pohyb	pohyb	k1gInSc1
Američanů	Američan	k1gMnPc2
přes	přes	k7c4
Appalačské	Appalačský	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
<g/>
,	,	kIx,
nedbaje	nedbat	k5eAaImSgMnS,k5eNaImSgMnS
na	na	k7c4
skupiny	skupina	k1gFnPc4
osadníků	osadník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
pokračovali	pokračovat	k5eAaImAgMnP
v	v	k7c6
pohybu	pohyb	k1gInSc6
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dekret	dekret	k1gInSc1
byl	být	k5eAaImAgInS
ve	v	k7c6
velmi	velmi	k6eAd1
krátké	krátký	k2eAgFnSc6d1
době	doba	k1gFnSc6
upraven	upraven	k2eAgInSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
již	již	k6eAd1
nebyl	být	k5eNaImAgInS
pro	pro	k7c4
osadníky	osadník	k1gMnPc4
překážkou	překážka	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
vznikl	vzniknout	k5eAaPmAgInS
bez	bez	k7c2
konzultace	konzultace	k1gFnSc2
s	s	k7c7
Američany	Američan	k1gMnPc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
pohoršilo	pohoršit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Québec	Québec	k1gMnSc1
Act	Act	k1gMnSc1
z	z	k7c2
roku	rok	k1gInSc2
1774	#num#	k4
posunul	posunout	k5eAaPmAgInS
hranice	hranice	k1gFnSc2
Québecu	Québec	k2eAgFnSc4d1
k	k	k7c3
řece	řeka	k1gFnSc3
Ohio	Ohio	k1gNnSc1
a	a	k8xC
nebral	brát	k5eNaImAgMnS
ohled	ohled	k1gInSc4
na	na	k7c4
stížnosti	stížnost	k1gFnPc4
13	#num#	k4
kolonií	kolonie	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
tohoto	tento	k3xDgInSc2
momentu	moment	k1gInSc2
začali	začít	k5eAaPmAgMnP
brát	brát	k5eAaImF
Američané	Američan	k1gMnPc1
jen	jen	k9
malý	malý	k2eAgInSc1d1
ohled	ohled	k1gInSc1
na	na	k7c4
zákony	zákon	k1gInPc4
z	z	k7c2
Londýna	Londýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začali	začít	k5eAaPmAgMnP
cvičit	cvičit	k5eAaImF
milice	milice	k1gFnPc4
a	a	k8xC
připravovat	připravovat	k5eAaImF
se	se	k3xPyFc4
na	na	k7c4
válku	válka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Krize	krize	k1gFnSc1
1772	#num#	k4
<g/>
–	–	k?
<g/>
1775	#num#	k4
</s>
<s>
I	i	k9
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
rámci	rámec	k1gInSc6
americké	americký	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
stala	stát	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
událostí	událost	k1gFnPc2
<g/>
,	,	kIx,
existoval	existovat	k5eAaImAgInS
specifický	specifický	k2eAgInSc1d1
sled	sled	k1gInSc1
událostí	událost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
vedly	vést	k5eAaImAgFnP
k	k	k7c3
vypuknutí	vypuknutí	k1gNnSc3
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
1772	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
Aféře	aféra	k1gFnSc3
Gaspée	Gaspée	k1gFnPc3
<g/>
:	:	kIx,
britská	britský	k2eAgFnSc1d1
válečná	válečný	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
ostře	ostro	k6eAd1
prosazovala	prosazovat	k5eAaImAgFnS
nepopulární	populární	k2eNgNnSc4d1
obchodní	obchodní	k2eAgNnSc4d1
omezení	omezení	k1gNnSc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
zapálena	zapálen	k2eAgFnSc1d1
americkými	americký	k2eAgMnPc7d1
vlastenci	vlastenec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
poté	poté	k6eAd1
guvernér	guvernér	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Hutchinson	Hutchinson	k1gMnSc1
z	z	k7c2
Massachusetts	Massachusetts	k1gNnSc2
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
on	on	k3xPp3gMnSc1
a	a	k8xC
královští	královský	k2eAgMnPc1d1
soudci	soudce	k1gMnPc1
budou	být	k5eAaImBp3nP
placeni	platit	k5eAaImNgMnP
přímo	přímo	k6eAd1
z	z	k7c2
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
obešel	obejít	k5eAaPmAgInS
koloniální	koloniální	k2eAgInSc1d1
zákonodárný	zákonodárný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
v	v	k7c6
roce	rok	k1gInSc6
1772	#num#	k4
vytvořil	vytvořit	k5eAaPmAgMnS
Samuel	Samuel	k1gMnSc1
Adams	Adamsa	k1gFnPc2
Výbor	výbor	k1gInSc1
korespondentů	korespondent	k1gMnPc2
(	(	kIx(
<g/>
Committees	Committees	k1gMnSc1
of	of	k?
Correspondencia	Correspondencia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
spojoval	spojovat	k5eAaImAgMnS
vlastence	vlastenec	k1gMnPc4
ve	v	k7c6
všech	všecek	k3xTgInPc6
13	#num#	k4
koloniích	kolonie	k1gFnPc6
a	a	k8xC
měl	mít	k5eAaImAgMnS
případně	případně	k6eAd1
poskytnout	poskytnout	k5eAaPmF
podporu	podpora	k1gFnSc4
pro	pro	k7c4
vládu	vláda	k1gFnSc4
rebelů	rebel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1773	#num#	k4
vytvořil	vytvořit	k5eAaPmAgInS
stát	stát	k1gInSc1
Virginie	Virginie	k1gFnSc2
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc2d3
kolonie	kolonie	k1gFnSc2
<g/>
,	,	kIx,
svůj	svůj	k3xOyFgInSc4
Výbor	výbor	k1gInSc4
korespondentů	korespondent	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgMnSc6
působili	působit	k5eAaImAgMnP
i	i	k9
Patrick	Patrick	k1gMnSc1
Henry	Henry	k1gMnSc1
a	a	k8xC
Thomas	Thomas	k1gMnSc1
Jefferson	Jefferson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Nepřijatelné	přijatelný	k2eNgNnSc1d1
nařízení	nařízení	k1gNnSc1
(	(	kIx(
<g/>
Intolerable	Intolerable	k1gFnSc1
Acts	Actsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jak	jak	k6eAd1
byly	být	k5eAaImAgFnP
pojmenovány	pojmenován	k2eAgMnPc4d1
kolonisty	kolonista	k1gMnPc4
<g/>
,	,	kIx,
obsahovaly	obsahovat	k5eAaImAgInP
čtyři	čtyři	k4xCgInPc1
zákony	zákon	k1gInPc1
schválené	schválený	k2eAgMnPc4d1
Britským	britský	k2eAgInSc7d1
parlamentem	parlament	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc7
byl	být	k5eAaImAgInS
„	„	k?
<g/>
Zákon	zákon	k1gInSc1
vlády	vláda	k1gFnSc2
v	v	k7c6
Massachusetts	Massachusetts	k1gNnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
změnil	změnit	k5eAaPmAgInS
massachusettskou	massachusettský	k2eAgFnSc4d1
chartu	charta	k1gFnSc4
a	a	k8xC
omezil	omezit	k5eAaPmAgInS
městská	městský	k2eAgNnPc4d1
zasedání	zasedání	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
nařízením	nařízení	k1gNnSc7
byl	být	k5eAaImAgInS
příkaz	příkaz	k1gInSc1
ministerstva	ministerstvo	k1gNnSc2
spravedlnosti	spravedlnost	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
nařídilo	nařídit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
všichni	všechen	k3xTgMnPc1
britští	britský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
verbováni	verbován	k2eAgMnPc1d1
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
v	v	k7c6
koloniích	kolonie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetím	třetí	k4xOgMnSc7
bylo	být	k5eAaImAgNnS
„	„	k?
<g/>
nařízení	nařízení	k1gNnSc1
pro	pro	k7c4
Bostonský	bostonský	k2eAgInSc4d1
přístav	přístav	k1gInSc4
(	(	kIx(
<g/>
Boston	Boston	k1gInSc1
Port	port	k1gInSc1
Act	Act	k1gFnSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
uzavřelo	uzavřít	k5eAaPmAgNnS
přístav	přístav	k1gInSc4
v	v	k7c6
Bostonu	Boston	k1gInSc6
<g/>
,	,	kIx,
dokud	dokud	k6eAd1
nebudou	být	k5eNaImBp3nP
Británii	Británie	k1gFnSc4
nahrazeny	nahradit	k5eAaPmNgFnP
všechny	všechen	k3xTgFnPc1
ztráty	ztráta	k1gFnPc1
způsobené	způsobený	k2eAgFnPc1d1
„	„	k?
<g/>
Bostonským	bostonský	k2eAgNnSc7d1
pitím	pití	k1gNnSc7
čaje	čaj	k1gInSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
Británii	Británie	k1gFnSc6
nikdy	nikdy	k6eAd1
nebyl	být	k5eNaImAgInS
vrácen	vrátit	k5eAaPmNgInS
ani	ani	k8xC
cent	cent	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrté	čtvrtý	k4xOgNnSc1
nařízení	nařízení	k1gNnSc1
byl	být	k5eAaImAgInS
Quartering	Quartering	k1gInSc4
Act	Act	k1gMnPc2
z	z	k7c2
roku	rok	k1gInSc2
1774	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
umožňoval	umožňovat	k5eAaImAgInS
umístit	umístit	k5eAaPmF
britské	britský	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
v	v	k7c6
neobydlených	obydlený	k2eNgFnPc6d1
budovách	budova	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
kontinentální	kontinentální	k2eAgInSc4d1
kongres	kongres	k1gInSc4
potvrdil	potvrdit	k5eAaPmAgMnS
Suffolské	Suffolský	k2eAgNnSc4d1
usnesení	usnesení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
deklarovalo	deklarovat	k5eAaBmAgNnS
„	„	k?
<g/>
nepřijatelné	přijatelný	k2eNgNnSc1d1
nařízení	nařízení	k1gNnSc1
<g/>
“	“	k?
jako	jako	k8xC,k8xS
protiústavní	protiústavní	k2eAgNnSc4d1
a	a	k8xC
vyzvalo	vyzvat	k5eAaPmAgNnS
lid	lid	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
začal	začít	k5eAaPmAgInS
formovat	formovat	k5eAaImF
milice	milice	k1gFnSc2
a	a	k8xC
Massachusetts	Massachusetts	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zformovalo	zformovat	k5eAaPmAgNnS
vládu	vláda	k1gFnSc4
patriotů	patriot	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
reakci	reakce	k1gFnSc6
zejména	zejména	k9
na	na	k7c4
„	„	k?
<g/>
Massachusetts	Massachusetts	k1gNnSc1
Government	Government	k1gMnSc1
Act	Act	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
vytvořil	vytvořit	k5eAaPmAgInS
lid	lid	k1gInSc1
ve	v	k7c6
Worchesteru	Worchester	k1gInSc6
linii	linie	k1gFnSc4
ozbrojenců	ozbrojenec	k1gMnPc2
před	před	k7c7
místním	místní	k2eAgInSc7d1
soudem	soud	k1gInSc7
a	a	k8xC
odmítl	odmítnout	k5eAaPmAgMnS
do	do	k7c2
budovy	budova	k1gFnSc2
vpustit	vpustit	k5eAaPmF
britské	britský	k2eAgMnPc4d1
soudce	soudce	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgFnPc4d1
události	událost	k1gFnPc4
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
dít	dít	k5eAaBmF,k5eAaImF
v	v	k7c6
celé	celý	k2eAgFnSc6d1
kolonii	kolonie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
potlačení	potlačení	k1gNnSc6
rebelie	rebelie	k1gFnSc2
bylo	být	k5eAaImAgNnS
z	z	k7c2
Anglie	Anglie	k1gFnSc2
vyslány	vyslat	k5eAaPmNgFnP
Britské	britský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
<g/>
;	;	kIx,
než	než	k8xS
však	však	k9
dorazilo	dorazit	k5eAaPmAgNnS
do	do	k7c2
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
Massachusetts	Massachusetts	k1gNnSc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
města	město	k1gNnSc2
Boston	Boston	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
velká	velký	k2eAgFnSc1d1
Britská	britský	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
rukou	ruka	k1gFnPc6
kolonistů	kolonista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Boje	boj	k1gInPc1
v	v	k7c6
Lexingtonu	Lexington	k1gInSc6
(	(	kIx(
<g/>
1775	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Britové	Brit	k1gMnPc1
ve	v	k7c6
městě	město	k1gNnSc6
Concorde	Concord	k1gInSc5
(	(	kIx(
<g/>
1775	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
Lexington	Lexington	k1gInSc4
a	a	k8xC
Concord	Concord	k1gInSc4
vypukla	vypuknout	k5eAaPmAgFnS
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1775	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Britové	Brit	k1gMnPc1
vyslali	vyslat	k5eAaPmAgMnP
vojenský	vojenský	k2eAgInSc4d1
oddíl	oddíl	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zabavil	zabavit	k5eAaPmAgMnS
zbraně	zbraň	k1gFnPc4
a	a	k8xC
zatkl	zatknout	k5eAaPmAgInS
revolucionáře	revolucionář	k1gMnSc4
ve	v	k7c6
městě	město	k1gNnSc6
Concorde	Concord	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
první	první	k4xOgFnSc1
bitva	bitva	k1gFnSc1
v	v	k7c6
americké	americký	k2eAgFnSc6d1
válce	válka	k1gFnSc6
o	o	k7c4
nezávislost	nezávislost	k1gFnSc4
a	a	k8xC
zpráva	zpráva	k1gFnSc1
o	o	k7c6
ní	on	k3xPp3gFnSc6
okamžitě	okamžitě	k6eAd1
vyburcovala	vyburcovat	k5eAaPmAgFnS
všech	všecek	k3xTgFnPc2
13	#num#	k4
kolonií	kolonie	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
povolaly	povolat	k5eAaPmAgFnP
milice	milice	k1gFnPc4
a	a	k8xC
vyslaly	vyslat	k5eAaPmAgFnP
jednotky	jednotka	k1gFnPc1
s	s	k7c7
cílem	cíl	k1gInSc7
obléhat	obléhat	k5eAaImF
Boston	Boston	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
o	o	k7c4
Bunker	Bunker	k1gInSc4
Hill	Hill	k1gInSc4
vypukla	vypuknout	k5eAaPmAgFnS
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1775	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
1776	#num#	k4
přinutili	přinutit	k5eAaPmAgMnP
kolonisté	kolonista	k1gMnPc1
vedení	vedení	k1gNnSc2
velitelem	velitel	k1gMnSc7
Georgem	Georg	k1gMnSc7
Washingtonem	Washington	k1gInSc7
Brity	Brit	k1gMnPc4
opustit	opustit	k5eAaPmF
Boston	Boston	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patrioti	patriot	k1gMnPc1
získali	získat	k5eAaPmAgMnP
kontrolu	kontrola	k1gFnSc4
ve	v	k7c6
všech	všecek	k3xTgInPc6
13	#num#	k4
koloniích	kolonie	k1gFnPc6
a	a	k8xC
byli	být	k5eAaImAgMnP
připraveni	připravit	k5eAaPmNgMnP
vyhlásit	vyhlásit	k5eAaPmF
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
bylo	být	k5eAaImAgNnS
v	v	k7c6
koloniích	kolonie	k1gFnPc6
mnoho	mnoho	k6eAd1
loyalistů	loyalista	k1gMnPc2
<g/>
,	,	kIx,
neměli	mít	k5eNaImAgMnP
od	od	k7c2
července	červenec	k1gInSc2
1776	#num#	k4
kontrolu	kontrola	k1gFnSc4
nikde	nikde	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
všichni	všechen	k3xTgMnPc1
britští	britský	k2eAgMnPc1d1
důstojníci	důstojník	k1gMnPc1
utekli	utéct	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Druhý	druhý	k4xOgInSc1
kontinentální	kontinentální	k2eAgInSc1d1
kongres	kongres	k1gInSc1
byl	být	k5eAaImAgInS
svolán	svolat	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1775	#num#	k4
po	po	k7c6
zahájení	zahájení	k1gNnSc6
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kongres	kongres	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
kontinentální	kontinentální	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
a	a	k8xC
poslal	poslat	k5eAaPmAgMnS
výzvu	výzva	k1gFnSc4
(	(	kIx(
<g/>
Olive	Oliev	k1gFnSc2
Branch	Branch	k1gInSc1
Petition	Petition	k1gInSc1
<g/>
)	)	kIx)
králi	král	k1gMnPc7
jako	jako	k8xS,k8xC
snahu	snaha	k1gFnSc4
o	o	k7c6
usmíření	usmíření	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
ji	on	k3xPp3gFnSc4
odmítl	odmítnout	k5eAaPmAgMnS
přijmout	přijmout	k5eAaPmF
a	a	k8xC
místo	místo	k7c2
toho	ten	k3xDgMnSc2
vydal	vydat	k5eAaPmAgInS
Prohlášení	prohlášení	k1gNnSc4
o	o	k7c6
rebelii	rebelie	k1gFnSc6
(	(	kIx(
<g/>
Proclamation	Proclamation	k1gInSc1
of	of	k?
Rebellion	Rebellion	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
akci	akce	k1gFnSc4
proti	proti	k7c3
zrádcům	zrádce	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1783	#num#	k4
s	s	k7c7
koloniemi	kolonie	k1gFnPc7
nevyjednává	vyjednávat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Názorové	názorový	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
mezi	mezi	k7c7
kolonisty	kolonista	k1gMnPc7
</s>
<s>
Patrioti	patriot	k1gMnPc1
–	–	k?
revolucionáři	revolucionář	k1gMnSc6
</s>
<s>
Revolucionáři	revolucionář	k1gMnPc1
byli	být	k5eAaImAgMnP
nazýváni	nazývat	k5eAaImNgMnP
také	také	k9
Patrioti	patriot	k1gMnPc1
(	(	kIx(
<g/>
Patriots	Patriots	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Liberálové	liberál	k1gMnPc1
(	(	kIx(
<g/>
Whigové	whig	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kongresmani	kongresman	k1gMnPc1
(	(	kIx(
<g/>
Congress-man	Congress-man	k1gMnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
také	také	k9
Američané	Američan	k1gMnPc1
(	(	kIx(
<g/>
Americans	Americans	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pocházeli	pocházet	k5eAaImAgMnP
ze	z	k7c2
všech	všecek	k3xTgFnPc2
ekonomických	ekonomický	k2eAgFnPc2d1
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
všichni	všechen	k3xTgMnPc1
jednomyslně	jednomyslně	k6eAd1
podpořili	podpořit	k5eAaPmAgMnP
potřebu	potřeba	k1gFnSc4
bránit	bránit	k5eAaImF
práva	právo	k1gNnPc4
Američanů	Američan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
patrioti	patriot	k1gMnPc1
jako	jako	k8xC,k8xS
George	Georg	k1gFnPc1
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
James	James	k1gMnSc1
Madison	Madison	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Adams	Adamsa	k1gFnPc2
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
Hamilton	Hamilton	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
John	John	k1gMnSc1
Jay	Jay	k1gMnSc1
byli	být	k5eAaImAgMnP
hluboce	hluboko	k6eAd1
oddaní	oddaný	k2eAgMnPc1d1
republikanizmu	republikanizmus	k1gInSc3
a	a	k8xC
také	také	k9
toužili	toužit	k5eAaImAgMnP
vybudovat	vybudovat	k5eAaPmF
bohatý	bohatý	k2eAgInSc4d1
a	a	k8xC
mocný	mocný	k2eAgInSc4d1
národ	národ	k1gInSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
vlastenci	vlastenec	k1gMnPc1
jako	jako	k9
Patrick	Patrick	k1gMnSc1
Henry	Henry	k1gMnSc1
<g/>
,	,	kIx,
Benjamin	Benjamin	k1gMnSc1
Franklin	Franklin	k2eAgMnSc1d1
a	a	k8xC
Thomas	Thomas	k1gMnSc1
Jefferson	Jefferson	k1gMnSc1
hájili	hájit	k5eAaImAgMnP
demokratické	demokratický	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
a	a	k8xC
zájmy	zájem	k1gInPc4
zemědělců	zemědělec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
chtěli	chtít	k5eAaImAgMnP
pro	pro	k7c4
společnost	společnost	k1gFnSc4
širší	široký	k2eAgFnSc4d2
politickou	politický	k2eAgFnSc4d1
rovnost	rovnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Loyalisté	Loyalista	k1gMnPc1
a	a	k8xC
neutrálové	neutrál	k1gMnPc1
</s>
<s>
Zakládající	zakládající	k2eAgMnSc1d1
člen	člen	k1gMnSc1
kvakerů	kvaker	k1gMnPc2
George	Georg	k1gFnSc2
Fox	fox	k1gInSc1
</s>
<s>
Minorita	minorita	k1gFnSc1
neurčité	určitý	k2eNgFnSc2d1
velikosti	velikost	k1gFnSc2
zůstala	zůstat	k5eAaPmAgFnS
během	během	k7c2
války	válka	k1gFnSc2
neutrální	neutrální	k2eAgFnSc2d1
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
z	z	k7c2
nižší	nízký	k2eAgFnSc2d2
třídy	třída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgFnSc7d3
z	z	k7c2
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
byli	být	k5eAaImAgMnP
kvakeři	kvaker	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existovali	existovat	k5eAaImAgMnP
také	také	k9
loyalisté	loyalista	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
nebyli	být	k5eNaImAgMnP
vysoce	vysoce	k6eAd1
postavení	postavení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
toho	ten	k3xDgNnSc2
byli	být	k5eAaImAgMnP
Indiáni	Indián	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
odmítli	odmítnout	k5eAaPmAgMnP
prosbu	prosba	k1gFnSc4
Američanů	Američan	k1gMnPc2
o	o	k7c4
neutralitu	neutralita	k1gFnSc4
a	a	k8xC
mnohé	mnohý	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
se	se	k3xPyFc4
přidaly	přidat	k5eAaPmAgInP
k	k	k7c3
loyalistům	loyalista	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
strany	strana	k1gFnSc2
vedla	vést	k5eAaImAgFnS
k	k	k7c3
loyalismu	loyalismus	k1gInSc3
pohnutka	pohnutka	k1gFnSc1
zajistit	zajistit	k5eAaPmF
si	se	k3xPyFc3
vliv	vliv	k1gInSc4
lokálních	lokální	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
závisely	záviset	k5eAaImAgInP
na	na	k7c6
koloniálním	koloniální	k2eAgInSc6d1
obchodu	obchod	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
odcizení	odcizení	k1gNnSc3
se	se	k3xPyFc4
od	od	k7c2
patriotů	patriot	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
když	když	k8xS
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
zjistit	zjistit	k5eAaPmF
přesné	přesný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
historici	historik	k1gMnPc1
odhadují	odhadovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
15-20	15-20	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
kolonií	kolonie	k1gFnPc2
zůstalo	zůstat	k5eAaPmAgNnS
věrných	věrný	k2eAgFnPc2d1
britské	britský	k2eAgFnSc3d1
koruně	koruna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tito	tento	k3xDgMnPc1
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
známí	známý	k1gMnPc1
jako	jako	k8xC,k8xS
loyalisté	loyalista	k1gMnPc1
(	(	kIx(
<g/>
Loyalist	Loyalist	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
konzervativci	konzervativec	k1gMnPc1
(	(	kIx(
<g/>
Tories	Tories	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Muži	muž	k1gMnPc1
krále	král	k1gMnSc2
(	(	kIx(
<g/>
Kings	Kings	k1gInSc1
men	men	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Loyalisté	Loyalista	k1gMnPc1
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
byli	být	k5eAaImAgMnP
často	často	k6eAd1
starší	starý	k2eAgFnSc4d2
<g/>
,	,	kIx,
si	se	k3xPyFc3
méně	málo	k6eAd2
přáli	přát	k5eAaImAgMnP
prolomit	prolomit	k5eAaPmF
starou	starý	k2eAgFnSc4d1
věrnost	věrnost	k1gFnSc4
koruně	koruna	k1gFnSc3
<g/>
,	,	kIx,
často	často	k6eAd1
byli	být	k5eAaImAgMnP
spojeni	spojit	k5eAaPmNgMnP
s	s	k7c7
anglikánskou	anglikánský	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
,	,	kIx,
a	a	k8xC
měli	mít	k5eAaImAgMnP
mnoho	mnoho	k4c4
zavedených	zavedený	k2eAgInPc2d1
obchodů	obchod	k1gInPc2
a	a	k8xC
obchodní	obchodní	k2eAgFnSc2d1
konexe	konexe	k1gFnSc2
v	v	k7c6
celém	celý	k2eAgNnSc6d1
Britském	britský	k2eAgNnSc6d1
impériu	impérium	k1gNnSc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
Thomas	Thomas	k1gMnSc1
Hutchinson	Hutchinson	k1gMnSc1
z	z	k7c2
Bostonu	Boston	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noví	nový	k2eAgMnPc1d1
imigranti	imigrant	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
nebyli	být	k5eNaImAgMnP
plně	plně	k6eAd1
amerikanizovaní	amerikanizovaný	k2eAgMnPc1d1
<g/>
,	,	kIx,
měli	mít	k5eAaImAgMnP
sklony	sklona	k1gFnSc2
podporovat	podporovat	k5eAaImF
krále	král	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
většina	většina	k1gFnSc1
loyalistů	loyalista	k1gMnPc2
v	v	k7c6
Americe	Amerika	k1gFnSc6
zůstala	zůstat	k5eAaPmAgFnS
a	a	k8xC
vedla	vést	k5eAaImAgFnS
normální	normální	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
Samuel	Samuel	k1gMnSc1
Seabury	Seabura	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
prominentními	prominentní	k2eAgMnPc7d1
americkými	americký	k2eAgMnPc7d1
vůdci	vůdce	k1gMnPc7
<g/>
.	.	kIx.
100	#num#	k4
000	#num#	k4
až	až	k9
200	#num#	k4
000	#num#	k4
loyalistů	loyalista	k1gMnPc2
se	se	k3xPyFc4
přestěhovalo	přestěhovat	k5eAaPmAgNnS
do	do	k7c2
Kanady	Kanada	k1gFnSc2
<g/>
,	,	kIx,
Britské	britský	k2eAgFnSc2d1
západní	západní	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
stáli	stát	k5eAaImAgMnP
za	za	k7c7
jednou	jeden	k4xCgFnSc7
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
migrací	migrace	k1gFnPc2
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	s	k7c7
přibližně	přibližně	k6eAd1
o	o	k7c4
5	#num#	k4
<g/>
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Britské	britský	k2eAgFnSc2d1
západní	západní	k2eAgFnSc2d1
Indie	Indie	k1gFnSc2
s	s	k7c7
sebou	se	k3xPyFc7
vzali	vzít	k5eAaPmAgMnP
75	#num#	k4
000	#num#	k4
otroků	otrok	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc1
potomci	potomek	k1gMnPc1
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
svobodnými	svobodný	k2eAgInPc7d1
o	o	k7c4
26	#num#	k4
let	léto	k1gNnPc2
dříve	dříve	k6eAd2
než	než	k8xS
by	by	kYmCp3nP
byli	být	k5eAaImAgMnP
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgMnSc1d1
velmi	velmi	k6eAd1
slabě	slabě	k6eAd1
zdokumentovanou	zdokumentovaný	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
připojila	připojit	k5eAaPmAgFnS
k	k	k7c3
loyalistům	loyalista	k1gMnPc3
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
afroameričtí	afroamerický	k2eAgMnPc1d1
otroci	otrok	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
aktivně	aktivně	k6eAd1
rekrutováni	rekrutovat	k5eAaImNgMnP
do	do	k7c2
britské	britský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
příslibem	příslib	k1gInSc7
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
možností	možnost	k1gFnPc2
chránit	chránit	k5eAaImF
své	svůj	k3xOyFgFnPc4
rodiny	rodina	k1gFnPc4
a	a	k8xC
často	často	k6eAd1
jim	on	k3xPp3gInPc3
byla	být	k5eAaImAgFnS
přislíbena	přislíben	k2eAgFnSc1d1
i	i	k8xC
půda	půda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Třídní	třídní	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
Patrioty	patriot	k1gMnPc7
</s>
<s>
Historikové	historik	k1gMnPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
J.	J.	kA
Franklin	Franklina	k1gFnPc2
Jameson	Jamesona	k1gFnPc2
ze	z	k7c2
začátku	začátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
zkoumali	zkoumat	k5eAaImAgMnP
třídní	třídní	k1gMnPc1
složení	složení	k1gNnSc4
patriotů	patriot	k1gMnPc2
<g/>
,	,	kIx,
hledajíce	hledat	k5eAaImSgFnP
důkaz	důkaz	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
během	během	k7c2
revoluce	revoluce	k1gFnSc2
probíhal	probíhat	k5eAaImAgInS
třídní	třídní	k2eAgInSc1d1
boj	boj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
posledních	poslední	k2eAgNnPc2d1
50	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
historici	historik	k1gMnPc1
od	od	k7c2
této	tento	k3xDgFnSc2
interpretace	interpretace	k1gFnSc2
upustili	upustit	k5eAaPmAgMnP
<g/>
,	,	kIx,
místo	místo	k7c2
toho	ten	k3xDgNnSc2
zdůraznili	zdůraznit	k5eAaPmAgMnP
vysokou	vysoký	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
ideologické	ideologický	k2eAgFnSc2d1
jednoty	jednota	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokud	dokud	k6eAd1
Loyalisti	Loyalist	k1gMnPc1
tvořili	tvořit	k5eAaImAgMnP
skupinu	skupina	k1gFnSc4
chudých	chudý	k2eAgMnPc2d1
a	a	k8xC
bohatých	bohatý	k2eAgMnPc2d1
<g/>
,	,	kIx,
vlastenci	vlastenec	k1gMnPc1
tvořili	tvořit	k5eAaImAgMnP
pestřejší	pestrý	k2eAgFnSc4d2
směs	směs	k1gFnSc4
<g/>
,	,	kIx,
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
bohatší	bohatý	k2eAgFnSc1d2
a	a	k8xC
lépe	dobře	k6eAd2
vzdělaný	vzdělaný	k2eAgMnSc1d1
měl	mít	k5eAaImAgMnS
větší	veliký	k2eAgFnSc4d2
šanci	šance	k1gFnSc4
stát	stát	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
důstojníkem	důstojník	k1gMnSc7
v	v	k7c6
armádě	armáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideologické	ideologický	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
byly	být	k5eAaImAgInP
na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
patrioti	patriot	k1gMnPc1
viděli	vidět	k5eAaImAgMnP
v	v	k7c6
nezávislosti	nezávislost	k1gFnSc6
způsob	způsob	k1gInSc4
osvobození	osvobození	k1gNnSc2
se	se	k3xPyFc4
zpod	zpod	k7c2
britského	britský	k2eAgInSc2d1
útlaku	útlak	k1gInSc2
a	a	k8xC
daní	daň	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
především	především	k6eAd1
se	se	k3xPyFc4
dožadovali	dožadovat	k5eAaImAgMnP
svých	svůj	k3xOyFgNnPc2
politických	politický	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
farmářů	farmář	k1gMnPc2
<g/>
,	,	kIx,
řemeslníků	řemeslník	k1gMnPc2
a	a	k8xC
drobných	drobný	k2eAgMnPc2d1
obchodníků	obchodník	k1gMnPc2
se	se	k3xPyFc4
přidalo	přidat	k5eAaPmAgNnS
k	k	k7c3
Patriotům	patriot	k1gMnPc3
právě	právě	k9
z	z	k7c2
těchto	tento	k3xDgInPc2
důvodů	důvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
nových	nový	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Mapa	mapa	k1gFnSc1
nových	nový	k2eAgInPc2d1
států	stát	k1gInPc2
s	s	k7c7
daty	datum	k1gNnPc7
přijetí	přijetí	k1gNnSc1
nové	nový	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
1776	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
patrioti	patriot	k1gMnPc1
kontrolovali	kontrolovat	k5eAaImAgMnP
všechna	všechen	k3xTgNnPc4
území	území	k1gNnPc4
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
bezmocní	bezmocný	k2eAgMnPc1d1
Loyalisti	Loyalist	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všech	všecek	k3xTgInPc2
13	#num#	k4
států	stát	k1gInPc2
svrhlo	svrhnout	k5eAaPmAgNnS
stávající	stávající	k2eAgFnPc4d1
vlády	vláda	k1gFnPc4
<g/>
,	,	kIx,
uzavřeli	uzavřít	k5eAaPmAgMnP
soudy	soud	k1gInPc4
a	a	k8xC
vyhnali	vyhnat	k5eAaPmAgMnP
britské	britský	k2eAgMnPc4d1
agenty	agent	k1gMnPc4
a	a	k8xC
guvernéry	guvernér	k1gMnPc4
z	z	k7c2
jejich	jejich	k3xOp3gInPc2
domů	dům	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvolili	zvolit	k5eAaPmAgMnP
zákonodárné	zákonodárný	k2eAgNnSc4d1
shromáždění	shromáždění	k1gNnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
však	však	k9
existovala	existovat	k5eAaImAgFnS
bez	bez	k7c2
zákonného	zákonný	k2eAgInSc2d1
rámce	rámec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
byla	být	k5eAaImAgFnS
nutná	nutný	k2eAgFnSc1d1
v	v	k7c6
každém	každý	k3xTgInSc6
státě	stát	k1gInSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
změnil	změnit	k5eAaPmAgInS
royalistický	royalistický	k2eAgInSc1d1
charakter	charakter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nyní	nyní	k6eAd1
tak	tak	k6eAd1
již	již	k6eAd1
byly	být	k5eAaImAgInP
státy	stát	k1gInPc1
<g/>
,	,	kIx,
nikoli	nikoli	k9
koloniemi	kolonie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1776	#num#	k4
New	New	k1gFnPc2
Hampshire	Hampshir	k1gInSc5
ratifikoval	ratifikovat	k5eAaBmAgInS
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
šest	šest	k4xCc4
měsíců	měsíc	k1gInPc2
před	před	k7c7
podpisem	podpis	k1gInSc7
Deklarace	deklarace	k1gFnSc2
nezávislosti	nezávislost	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1776	#num#	k4
kongres	kongres	k1gInSc1
schválil	schválit	k5eAaPmAgInS
potlačení	potlačení	k1gNnSc3
jakékoli	jakýkoli	k3yIgFnSc2
autority	autorita	k1gFnSc2
koruny	koruna	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gNnSc4
nahrazení	nahrazení	k1gNnSc4
lokální	lokální	k2eAgFnSc7d1
oprávněnou	oprávněný	k2eAgFnSc7d1
autoritou	autorita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Virginie	Virginie	k1gFnSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
a	a	k8xC
New	New	k1gFnPc1
Jersey	Jersea	k1gFnSc2
vytvořily	vytvořit	k5eAaPmAgFnP
ústavu	ústava	k1gFnSc4
před	před	k7c7
4	#num#	k4
<g/>
.	.	kIx.
červencem	červenec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rhode	Rhodos	k1gInSc5
Island	Island	k1gInSc1
a	a	k8xC
Connecticut	Connecticut	k1gInSc1
jednoduše	jednoduše	k6eAd1
upravily	upravit	k5eAaPmAgInP
jejich	jejich	k3xOp3gInPc1
stávající	stávající	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
a	a	k8xC
vymazaly	vymazat	k5eAaPmAgFnP
z	z	k7c2
nich	on	k3xPp3gMnPc2
veškeré	veškerý	k3xTgInPc4
vztahy	vztah	k1gInPc4
ke	k	k7c3
koruně	koruna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Nové	Nové	k2eAgInPc1d1
státy	stát	k1gInPc1
se	se	k3xPyFc4
musely	muset	k5eAaImAgInP
rozhodnout	rozhodnout	k5eAaPmF
<g/>
,	,	kIx,
jakou	jaký	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
formu	forma	k1gFnSc4
vlády	vláda	k1gFnSc2
si	se	k3xPyFc3
vytvoří	vytvořit	k5eAaPmIp3nS
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
však	však	k9
bylo	být	k5eAaImAgNnS
třeba	třeba	k6eAd1
vymyslet	vymyslet	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
vybrat	vybrat	k5eAaPmF
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
vytvoří	vytvořit	k5eAaPmIp3nP
ústavu	ústava	k1gFnSc4
a	a	k8xC
jak	jak	k8xS,k8xC
bude	být	k5eAaImBp3nS
následný	následný	k2eAgInSc1d1
dokument	dokument	k1gInSc1
ratifikován	ratifikován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
bohaté	bohatý	k2eAgFnPc1d1
firmy	firma	k1gFnPc1
dohlížely	dohlížet	k5eAaImAgFnP
na	na	k7c4
proces	proces	k1gInSc4
ratifikace	ratifikace	k1gFnSc1
(	(	kIx(
<g/>
Maryland	Maryland	k1gInSc1
<g/>
,	,	kIx,
Virginia	Virginium	k1gNnPc1
<g/>
,	,	kIx,
Delaware	Delawar	k1gMnSc5
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
a	a	k8xC
Massachusetts	Massachusetts	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
měly	mít	k5eAaImAgInP
ústavy	ústav	k1gInPc1
své	svůj	k3xOyFgFnPc4
charakteristické	charakteristický	k2eAgInPc1d1
rysy	rys	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
dvoukomorový	dvoukomorový	k2eAgInSc1d1
zákonodárný	zákonodárný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
</s>
<s>
majetkový	majetkový	k2eAgInSc1d1
cenzus	cenzus	k1gInSc1
pro	pro	k7c4
volitelná	volitelný	k2eAgNnPc4d1
místa	místo	k1gNnPc4
</s>
<s>
významní	významný	k2eAgMnPc1d1
jedinci	jedinec	k1gMnPc1
mohli	moct	k5eAaImAgMnP
mít	mít	k5eAaImF
několik	několik	k4yIc4
postů	post	k1gInPc2
ve	v	k7c6
vládě	vláda	k1gFnSc6
</s>
<s>
byla	být	k5eAaImAgFnS
v	v	k7c6
nich	on	k3xPp3gFnPc6
uvedená	uvedený	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
víra	víra	k1gFnSc1
</s>
<s>
silný	silný	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
s	s	k7c7
právem	právo	k1gNnSc7
veta	veto	k1gNnSc2
a	a	k8xC
velkou	velký	k2eAgFnSc7d1
autoritou	autorita	k1gFnSc7
</s>
<s>
V	v	k7c6
méně	málo	k6eAd2
majetných	majetný	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
,	,	kIx,
především	především	k6eAd1
Pensylvánie	Pensylvánie	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
Jersey	Jersea	k1gFnSc2
a	a	k8xC
New	New	k1gMnSc5
Hampshire	Hampshir	k1gMnSc5
<g/>
,	,	kIx,
byly	být	k5eAaImAgInP
schváleny	schválen	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
s	s	k7c7
těmito	tento	k3xDgInPc7
znaky	znak	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
silný	silný	k2eAgInSc1d1
jednokomorový	jednokomorový	k2eAgInSc1d1
zákonodárný	zákonodárný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
</s>
<s>
zákaz	zákaz	k1gInSc1
pro	pro	k7c4
jednotlivce	jednotlivec	k1gMnPc4
zastávat	zastávat	k5eAaImF
více	hodně	k6eAd2
postů	post	k1gInPc2
</s>
<s>
univerzální	univerzální	k2eAgNnSc1d1
hlasovací	hlasovací	k2eAgNnSc1d1
právo	právo	k1gNnSc1
podle	podle	k7c2
věku	věk	k1gInSc2
nebo	nebo	k8xC
nejméně	málo	k6eAd3
majetkové	majetkový	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
pro	pro	k7c4
možnost	možnost	k1gFnSc4
být	být	k5eAaImF
volen	volen	k2eAgMnSc1d1
nebo	nebo	k8xC
zastávat	zastávat	k5eAaImF
úřad	úřad	k1gInSc4
</s>
<s>
relativně	relativně	k6eAd1
slabý	slabý	k2eAgMnSc1d1
guvernér	guvernér	k1gMnSc1
<g/>
,	,	kIx,
bez	bez	k7c2
práva	právo	k1gNnSc2
veta	veto	k1gNnSc2
a	a	k8xC
bez	bez	k7c2
významné	významný	k2eAgFnSc2d1
autority	autorita	k1gFnSc2
</s>
<s>
Nezávislost	nezávislost	k1gFnSc1
–	–	k?
1776	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1776	#num#	k4
vydal	vydat	k5eAaPmAgMnS
Thomas	Thomas	k1gMnSc1
Paine	Pain	k1gInSc5
politický	politický	k2eAgInSc4d1
pamflet	pamflet	k1gInSc4
s	s	k7c7
názvem	název	k1gInSc7
Zdravý	zdravý	k2eAgInSc4d1
rozum	rozum	k1gInSc4
(	(	kIx(
<g/>
Common	Common	k1gInSc1
Sense	Sense	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
se	se	k3xPyFc4
psalo	psát	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
jediný	jediný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
vyřešit	vyřešit	k5eAaPmF
problémy	problém	k1gInPc4
s	s	k7c7
Británií	Británie	k1gFnSc7
je	být	k5eAaImIp3nS
republikanismus	republikanismus	k1gInSc4
a	a	k8xC
vyhlášení	vyhlášení	k1gNnSc4
nezávislosti	nezávislost	k1gFnSc2
na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1776	#num#	k4
byla	být	k5eAaImAgFnS
deklarace	deklarace	k1gFnSc1
nezávislosti	nezávislost	k1gFnSc2
ratifikována	ratifikován	k2eAgFnSc1d1
druhým	druhý	k4xOgInSc7
kontinentálním	kontinentální	k2eAgInSc7d1
kongresem	kongres	k1gInSc7
a	a	k8xC
podepsána	podepsat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
den	den	k1gInSc4
je	být	k5eAaImIp3nS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
státním	státní	k2eAgInSc7d1
svátkem	svátek	k1gInSc7
<g/>
,	,	kIx,
dnem	den	k1gInSc7
nezávislosti	nezávislost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
dubnu	duben	k1gInSc6
1775	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
deklarace	deklarace	k1gFnSc1
nezávislosti	nezávislost	k1gFnSc2
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
červenci	červenec	k1gInSc6
1776	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
tohoto	tento	k3xDgInSc2
momentu	moment	k1gInSc2
se	se	k3xPyFc4
kolonie	kolonie	k1gFnPc1
snažily	snažit	k5eAaImAgFnP
jen	jen	k9
o	o	k7c4
výhodné	výhodný	k2eAgNnSc4d1
příměří	příměří	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
od	od	k7c2
teď	teď	k6eAd1
všechny	všechen	k3xTgInPc1
důrazně	důrazně	k6eAd1
volaly	volat	k5eAaImAgInP
po	po	k7c6
nezávislosti	nezávislost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dokumenty	dokument	k1gInPc1
známé	známý	k2eAgFnSc2d1
jako	jako	k8xS,k8xC
The	The	k1gFnSc2
Articles	Articles	k1gMnSc1
of	of	k?
Confederation	Confederation	k1gInSc1
and	and	k?
Perpetual	Perpetual	k1gInSc1
Union	union	k1gInSc1
<g/>
,	,	kIx,
známější	známý	k2eAgFnSc1d2
též	též	k9
jako	jako	k8xC,k8xS
Articles	Articles	k1gMnSc1
of	of	k?
Confederation	Confederation	k1gInSc1
(	(	kIx(
<g/>
Články	článek	k1gInPc1
o	o	k7c4
konfederaci	konfederace	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vytvořily	vytvořit	k5eAaPmAgInP
první	první	k4xOgInPc1
ústavní	ústavní	k2eAgInPc1d1
dokument	dokument	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
a	a	k8xC
spojily	spojit	k5eAaPmAgFnP
kolonie	kolonie	k1gFnPc1
do	do	k7c2
volné	volný	k2eAgFnSc2d1
konfederace	konfederace	k1gFnSc2
svrchovaných	svrchovaný	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
kontinentální	kontinentální	k2eAgInSc4d1
kongres	kongres	k1gInSc4
přijal	přijmout	k5eAaPmAgMnS
články	článek	k1gInPc7
v	v	k7c6
listopadu	listopad	k1gInSc6
1777	#num#	k4
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
byly	být	k5eAaImAgFnP
oficiálně	oficiálně	k6eAd1
ratifikovány	ratifikovat	k5eAaBmNgFnP
do	do	k7c2
tří	tři	k4xCgNnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Válka	válka	k1gFnSc1
</s>
<s>
Britové	Brit	k1gMnPc1
se	se	k3xPyFc4
vracejí	vracet	k5eAaImIp3nP
(	(	kIx(
<g/>
1776	#num#	k4
<g/>
–	–	k?
<g/>
1777	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Washington	Washington	k1gInSc1
překračuje	překračovat	k5eAaImIp3nS
řeku	řeka	k1gFnSc4
Delaware	Delawar	k1gInSc5
</s>
<s>
Britové	Brit	k1gMnPc1
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
na	na	k7c4
bojovou	bojový	k2eAgFnSc4d1
scénu	scéna	k1gFnSc4
v	v	k7c6
srpnu	srpen	k1gInSc6
roku	rok	k1gInSc2
1776	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nasadili	nasadit	k5eAaPmAgMnP
kontinentální	kontinentální	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
k	k	k7c3
největší	veliký	k2eAgFnSc3d3
akci	akce	k1gFnSc3
během	během	k7c2
revoluce	revoluce	k1gFnSc2
<g/>
,	,	kIx,
Bitvě	bitva	k1gFnSc3
o	o	k7c4
Long	Long	k1gInSc4
Island	Island	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
zmocnili	zmocnit	k5eAaPmAgMnP
New	New	k1gFnPc7
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
dříve	dříve	k6eAd2
získal	získat	k5eAaPmAgMnS
americký	americký	k2eAgMnSc1d1
generál	generál	k1gMnSc1
George	Georg	k1gFnSc2
Washington	Washington	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
městě	město	k1gNnSc6
si	se	k3xPyFc3
zřídili	zřídit	k5eAaPmAgMnP
politickou	politický	k2eAgFnSc4d1
a	a	k8xC
vojenskou	vojenský	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
a	a	k8xC
drželi	držet	k5eAaImAgMnP
ji	on	k3xPp3gFnSc4
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1783	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
obsadili	obsadit	k5eAaPmAgMnP
New	New	k1gMnPc4
Jersey	Jersea	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Washington	Washington	k1gInSc1
nečekaně	nečekaně	k6eAd1
překročil	překročit	k5eAaPmAgInS
řeku	řek	k1gMnSc5
Delaware	Delawar	k1gMnSc5
<g/>
,	,	kIx,
porazil	porazit	k5eAaPmAgInS
britskou	britský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
v	v	k7c6
Trentonu	Trenton	k1gInSc6
a	a	k8xC
Pricetonu	Priceton	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
získal	získat	k5eAaPmAgMnS
tak	tak	k6eAd1
New	New	k1gFnSc3
Jersey	Jerse	k1gMnPc7
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1777	#num#	k4
Britové	Brit	k1gMnPc1
vyslali	vyslat	k5eAaPmAgMnP
dva	dva	k4xCgInPc4
nekoordinované	koordinovaný	k2eNgInPc4d1
útoky	útok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Posádka	posádka	k1gFnSc1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
porazila	porazit	k5eAaPmAgFnS
Washingtona	Washingtona	k1gFnSc1
a	a	k8xC
obsadila	obsadit	k5eAaPmAgFnS
Filadelfii	Filadelfie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simultánní	simultánní	k2eAgFnSc1d1
druhá	druhý	k4xOgFnSc1
armáda	armáda	k1gFnSc1
vtrhla	vtrhnout	k5eAaPmAgFnS
z	z	k7c2
Kanady	Kanada	k1gFnSc2
s	s	k7c7
cílem	cíl	k1gInSc7
odříznout	odříznout	k5eAaPmF
Novou	nový	k2eAgFnSc4d1
Anglii	Anglie	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
byla	být	k5eAaImAgNnP
chycena	chytit	k5eAaPmNgNnP
do	do	k7c2
pasti	past	k1gFnSc2
a	a	k8xC
v	v	k7c6
říjnu	říjen	k1gInSc6
1777	#num#	k4
uvězněna	uvěznit	k5eAaPmNgFnS
v	v	k7c4
Saratoze	Saratoz	k1gInSc5
<g/>
,	,	kIx,
ve	v	k7c6
státě	stát	k1gInSc6
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
vítězství	vítězství	k1gNnSc1
posílilo	posílit	k5eAaPmAgNnS
odhodlání	odhodlání	k1gNnSc4
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
oficiálně	oficiálně	k6eAd1
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
Benjamin	Benjamin	k1gMnSc1
Franklin	Franklin	k1gInSc4
zahájil	zahájit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1778	#num#	k4
jednání	jednání	k1gNnPc2
o	o	k7c6
vojenském	vojenský	k2eAgNnSc6d1
spojenectví	spojenectví	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
Španělsko	Španělsko	k1gNnSc1
a	a	k8xC
Nizozemí	Nizozemí	k1gNnSc1
spojilo	spojit	k5eAaPmAgNnS
s	s	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
nechali	nechat	k5eAaPmAgMnP
Británii	Británie	k1gFnSc3
bojovat	bojovat	k5eAaImF
válku	válka	k1gFnSc4
osamocenou	osamocený	k2eAgFnSc7d1
<g/>
,	,	kIx,
s	s	k7c7
blokádou	blokáda	k1gFnSc7
v	v	k7c6
Atlantiku	Atlantik	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
tak	tak	k6eAd1
Američané	Američan	k1gMnPc1
bojovali	bojovat	k5eAaImAgMnP
ve	v	k7c6
válce	válka	k1gFnSc6
s	s	k7c7
Británií	Británie	k1gFnSc7
pouze	pouze	k6eAd1
na	na	k7c6
jedné	jeden	k4xCgFnSc6
frontě	fronta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kvůli	kvůli	k7c3
tomuto	tento	k3xDgNnSc3
spojenectví	spojenectví	k1gNnSc3
a	a	k8xC
špatné	špatný	k2eAgFnSc3d1
vojenské	vojenský	k2eAgFnSc3d1
situaci	situace	k1gFnSc3
<g/>
,	,	kIx,
Sir	sir	k1gMnSc1
Henry	Henry	k1gMnSc1
Clinton	Clinton	k1gMnSc1
<g/>
,	,	kIx,
britský	britský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
<g/>
,	,	kIx,
opustil	opustit	k5eAaPmAgMnS
Filadelfii	Filadelfie	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
posílil	posílit	k5eAaPmAgInS
New	New	k1gFnSc4
York	York	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generál	generál	k1gMnSc1
Washington	Washington	k1gInSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgInS
zastavit	zastavit	k5eAaPmF
ústup	ústup	k1gInSc1
Britů	Brit	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
bitvě	bitva	k1gFnSc3
u	u	k7c2
Monmouth	Monmoutha	k1gFnPc2
Court	Courta	k1gFnPc2
House	house	k1gNnSc1
<g/>
,	,	kIx,
poslední	poslední	k2eAgFnSc3d1
velké	velký	k2eAgFnSc3d1
bitvě	bitva	k1gFnSc3
v	v	k7c6
severních	severní	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rozporuplných	rozporuplný	k2eAgNnPc6d1
jednáních	jednání	k1gNnPc6
Britové	Brit	k1gMnPc1
úspěšně	úspěšně	k6eAd1
ustoupili	ustoupit	k5eAaPmAgMnP
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
patové	patový	k2eAgFnSc2d1
situace	situace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Britové	Brit	k1gMnPc1
útočí	útočit	k5eAaImIp3nP
na	na	k7c6
jihu	jih	k1gInSc6
</s>
<s>
Generál	generál	k1gMnSc1
Cornwallis	Cornwallis	k1gFnSc2
se	se	k3xPyFc4
vzdává	vzdávat	k5eAaImIp3nS
u	u	k7c2
Yorktownu	Yorktown	k1gInSc2
(	(	kIx(
<g/>
dobová	dobový	k2eAgFnSc1d1
olejomalba	olejomalba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Dobývání	dobývání	k1gNnSc1
Yorktownu	Yorktown	k1gInSc2
skončilo	skončit	k5eAaPmAgNnS
kapitulací	kapitulace	k1gFnSc7
britské	britský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
otevřelo	otevřít	k5eAaPmAgNnS
cestu	cesta	k1gFnSc4
k	k	k7c3
ukončení	ukončení	k1gNnSc3
americké	americký	k2eAgFnSc2d1
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
prosince	prosinec	k1gInSc2
1778	#num#	k4
Britové	Brit	k1gMnPc1
obsadili	obsadit	k5eAaPmAgMnP
Savannah	Savannah	k1gInSc4
a	a	k8xC
začali	začít	k5eAaPmAgMnP
se	se	k3xPyFc4
přesouvat	přesouvat	k5eAaImF
na	na	k7c4
sever	sever	k1gInSc4
do	do	k7c2
Jižní	jižní	k2eAgFnSc2d1
Karolíny	Karolína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgFnSc1d1
Georgie	Georgie	k1gFnSc1
nebyla	být	k5eNaImAgFnS
obsazena	obsadit	k5eAaPmNgFnS
<g/>
;	;	kIx,
patrioti	patriot	k1gMnPc1
zvítězili	zvítězit	k5eAaPmAgMnP
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Kette	Kett	k1gInSc5
Creek	Creek	k6eAd1
v	v	k7c6
okrese	okres	k1gInSc6
Wilkes	Wilkesa	k1gFnPc2
v	v	k7c6
Georgii	Georgie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
se	se	k3xPyFc4
pohnuli	pohnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
obsadili	obsadit	k5eAaPmAgMnP
Charleston	charleston	k1gInSc4
a	a	k8xC
vybudovali	vybudovat	k5eAaPmAgMnP
síť	síť	k1gFnSc4
opevněných	opevněný	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
věříce	věřit	k5eAaImSgFnP
<g/>
,	,	kIx,
že	že	k8xS
loyalisti	loyalist	k1gMnPc1
se	se	k3xPyFc4
postaví	postavit	k5eAaPmIp3nP
na	na	k7c4
odpor	odpor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
loyalistů	loyalista	k1gMnPc2
nebylo	být	k5eNaImAgNnS
dost	dost	k6eAd1
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
Britové	Brit	k1gMnPc1
museli	muset	k5eAaImAgMnP
sami	sám	k3xTgMnPc1
probojovat	probojovat	k5eAaPmF
na	na	k7c4
sever	sever	k1gInSc4
do	do	k7c2
Severní	severní	k2eAgFnSc2d1
Karolíny	Karolína	k1gFnSc2
a	a	k8xC
Virginie	Virginie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
očekávali	očekávat	k5eAaImAgMnP
záchranu	záchrana	k1gFnSc4
od	od	k7c2
britské	britský	k2eAgFnSc2d1
flotily	flotila	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
poražena	porazit	k5eAaPmNgFnS
francouzským	francouzský	k2eAgNnSc7d1
loďstvem	loďstvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvězněná	uvězněný	k2eAgFnSc1d1
v	v	k7c4
Yorktownu	Yorktowna	k1gFnSc4
<g/>
,	,	kIx,
Virginii	Virginie	k1gFnSc3
a	a	k8xC
pod	pod	k7c7
kombinovanými	kombinovaný	k2eAgInPc7d1
útoky	útok	k1gInPc7
amerických	americký	k2eAgFnPc2d1
a	a	k8xC
francouzských	francouzský	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
se	se	k3xPyFc4
britská	britský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
pod	pod	k7c7
velením	velení	k1gNnSc7
generála	generál	k1gMnSc2
Corwallise	Corwallise	k1gFnSc2
Washingtonovi	Washingtonův	k2eAgMnPc1d1
v	v	k7c6
říjnu	říjen	k1gInSc6
1781	#num#	k4
vzdala	vzdát	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
chtěl	chtít	k5eAaImAgMnS
král	král	k1gMnSc1
Jiří	Jiří	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
bojovat	bojovat	k5eAaImF
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc4
podporovatelé	podporovatel	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
vliv	vliv	k1gInSc4
v	v	k7c6
parlamentu	parlament	k1gInSc6
a	a	k8xC
válka	válka	k1gFnSc1
skončila	skončit	k5eAaPmAgFnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konečná	konečný	k2eAgFnSc1d1
námořní	námořní	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
se	se	k3xPyFc4
udála	udát	k5eAaPmAgFnS
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1783	#num#	k4
u	u	k7c2
mysu	mys	k1gInSc2
Cape	capat	k5eAaImIp3nS
Canaveral	Canaveral	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Otázka	otázka	k1gFnSc1
zrádců	zrádce	k1gMnPc2
</s>
<s>
Američtí	americký	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
zahajují	zahajovat	k5eAaImIp3nP
útok	útok	k1gInSc4
na	na	k7c4
BrityV	BrityV	k1gFnSc1
srpnu	srpen	k1gInSc6
1775	#num#	k4
král	král	k1gMnSc1
prohlásil	prohlásit	k5eAaPmAgMnS
Američany	Američan	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
povstali	povstat	k5eAaPmAgMnP
<g/>
,	,	kIx,
za	za	k7c4
zrádce	zrádce	k1gMnPc4
koruny	koruna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britská	britský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
nejprve	nejprve	k6eAd1
začala	začít	k5eAaPmAgFnS
zacházet	zacházet	k5eAaImF
se	s	k7c7
zajatci	zajatec	k1gMnPc7
jako	jako	k8xC,k8xS
s	s	k7c7
kriminálníky	kriminálník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byli	být	k5eAaImAgMnP
uvrženi	uvržen	k2eAgMnPc1d1
do	do	k7c2
vězení	vězení	k1gNnPc2
a	a	k8xC
byly	být	k5eAaImAgInP
proti	proti	k7c3
nim	on	k3xPp3gInPc3
chystány	chystán	k2eAgFnPc1d1
soudy	soud	k1gInPc7
za	za	k7c4
velezradu	velezrada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lord	lord	k1gMnSc1
Sackville	Sackville	k1gInSc1
a	a	k8xC
Lord	lord	k1gMnSc1
Sandwich	Sandwich	k1gMnSc1
byli	být	k5eAaImAgMnP
dychtiví	dychtivý	k2eAgMnPc1d1
tyto	tento	k3xDgInPc1
procesy	proces	k1gInPc4
co	co	k9
nejdříve	dříve	k6eAd3
začít	začít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množství	množství	k1gNnSc1
zajatců	zajatec	k1gMnPc2
z	z	k7c2
Bunker	Bunkero	k1gNnPc2
Hill	Hillum	k1gNnPc2
očekávalo	očekávat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
oběšeni	oběsit	k5eAaPmNgMnP
<g/>
,	,	kIx,
ale	ale	k8xC
guvernér	guvernér	k1gMnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgMnS
soudy	soud	k1gInPc4
a	a	k8xC
popravy	poprava	k1gFnPc4
zastavit	zastavit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
územích	území	k1gNnPc6
kontrolovaných	kontrolovaný	k2eAgFnPc2d1
Američany	Američan	k1gMnPc4
se	se	k3xPyFc4
totiž	totiž	k9
nacházelo	nacházet	k5eAaImAgNnS
na	na	k7c4
10	#num#	k4
000	#num#	k4
loyalistů	loyalista	k1gMnPc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yRgMnPc3,k3yIgMnPc3
hrozila	hrozit	k5eAaImAgFnS
odvetná	odvetný	k2eAgNnPc1d1
opatření	opatření	k1gNnPc4
ze	z	k7c2
strany	strana	k1gFnSc2
Američanů	Američan	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
Britové	Brit	k1gMnPc1
stavěli	stavět	k5eAaImAgMnP
svou	svůj	k3xOyFgFnSc4
strategii	strategie	k1gFnSc4
na	na	k7c6
jejich	jejich	k3xOp3gFnSc6
podpoře	podpora	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
porážce	porážka	k1gFnSc6
u	u	k7c2
Saratogy	Saratoga	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1777	#num#	k4
byly	být	k5eAaImAgInP
tisíce	tisíc	k4xCgInPc1
britských	britský	k2eAgMnPc2d1
zajatců	zajatec	k1gMnPc2
v	v	k7c6
rukou	ruka	k1gFnPc6
Američanů	Američan	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
byly	být	k5eAaImAgFnP
účinnými	účinný	k2eAgMnPc7d1
rukojmími	rukojmí	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
ani	ani	k8xC
jeden	jeden	k4xCgMnSc1
zajatý	zajatý	k2eAgMnSc1d1
Američan	Američan	k1gMnSc1
nakonec	nakonec	k6eAd1
nebyl	být	k5eNaImAgMnS
souzen	soudit	k5eAaImNgMnS
za	za	k7c4
vlastizradu	vlastizrada	k1gFnSc4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
s	s	k7c7
nimi	on	k3xPp3gMnPc7
bylo	být	k5eAaImAgNnS
špatně	špatně	k6eAd1
zacházeno	zacházen	k2eAgNnSc4d1
a	a	k8xC
podléhali	podléhat	k5eAaImAgMnP
právu	práv	k2eAgFnSc4d1
válčící	válčící	k2eAgFnSc4d1
strany	strana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1782	#num#	k4
byl	být	k5eAaImAgInS
výnosem	výnos	k1gInSc7
parlamentu	parlament	k1gInSc2
změněn	změnit	k5eAaPmNgInS
jejich	jejich	k3xOp3gInSc4
status	status	k1gInSc4
ze	z	k7c2
zrádců	zrádce	k1gMnPc2
na	na	k7c4
válečné	válečný	k2eAgMnPc4d1
zajatce	zajatec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
války	válka	k1gFnSc2
obě	dva	k4xCgFnPc4
strany	strana	k1gFnPc4
své	svůj	k3xOyFgFnSc2
zajatce	zajatec	k1gMnPc4
propustily	propustit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s>
Mírová	mírový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Pařížská	pařížský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
(	(	kIx(
<g/>
1783	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mírová	mírový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
s	s	k7c7
Velkou	velký	k2eAgFnSc7d1
Británií	Británie	k1gFnSc7
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
také	také	k9
jako	jako	k8xC,k8xS
pařížská	pařížský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
dala	dát	k5eAaPmAgFnS
Spojeným	spojený	k2eAgInPc3d1
státům	stát	k1gInPc3
veškeré	veškerý	k3xTgFnPc4
území	území	k1gNnSc1
na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
řeky	řeka	k1gFnSc2
Mississippi	Mississippi	k1gFnSc2
a	a	k8xC
na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
Velkých	velký	k2eAgNnPc2d1
jezer	jezero	k1gNnPc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
nezahrnovala	zahrnovat	k5eNaImAgFnS
Floridu	Florida	k1gFnSc4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1783	#num#	k4
se	se	k3xPyFc4
Británie	Británie	k1gFnSc1
dohodla	dohodnout	k5eAaPmAgFnS
se	s	k7c7
Španělskem	Španělsko	k1gNnSc7
o	o	k7c6
vrácení	vrácení	k1gNnSc6
Floridy	Florida	k1gFnSc2
zpět	zpět	k6eAd1
Španělsku	Španělsko	k1gNnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národy	národ	k1gInPc4
amerických	americký	k2eAgMnPc2d1
indiánů	indián	k1gMnPc2
nebyly	být	k5eNaImAgFnP
součástí	součást	k1gFnSc7
žádné	žádný	k3yNgFnPc4
smlouvy	smlouva	k1gFnPc4
a	a	k8xC
nezávislost	nezávislost	k1gFnSc4
je	být	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
ani	ani	k8xC
nezajímala	zajímat	k5eNaImAgFnS
<g/>
,	,	kIx,
dokud	dokud	k8xS
nebyli	být	k5eNaImAgMnP
vojensky	vojensky	k6eAd1
poraženi	porazit	k5eAaPmNgMnP
armádou	armáda	k1gFnSc7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záležitosti	záležitost	k1gFnSc2
hranic	hranice	k1gFnPc2
a	a	k8xC
dluhů	dluh	k1gInPc2
nebyly	být	k5eNaImAgFnP
řešeny	řešit	k5eAaImNgFnP
až	až	k6eAd1
do	do	k7c2
Smlouvy	smlouva	k1gFnSc2
z	z	k7c2
Jay	Jay	k1gFnSc2
(	(	kIx(
<g/>
Jay	Jay	k1gMnSc1
Treaty	Treata	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1795	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Oneidové	Oneid	k1gMnPc1
<g/>
,	,	kIx,
Tuscarorové	Tuscaror	k1gMnPc1
<g/>
,	,	kIx,
Lenapové	Lenapová	k1gFnPc1
<g/>
,	,	kIx,
Mikmakové	Mikmakový	k2eAgFnPc1d1
<g/>
,	,	kIx,
Abenakové	Abenakový	k2eAgFnPc1d1
<g/>
,	,	kIx,
Seminolové	Seminolový	k2eAgFnPc1d1
...	...	k?
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
je	být	k5eAaImIp3nS
neúplný	úplný	k2eNgInSc1d1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Onondagové	Onondag	k1gMnPc1
<g/>
,	,	kIx,
Mohawkové	Mohawek	k1gMnPc1
<g/>
,	,	kIx,
Cayugové	Cayug	k1gMnPc1
<g/>
,	,	kIx,
Senekové	Seneka	k1gMnPc1
<g/>
,	,	kIx,
Mikmakové	Mikmakový	k2eAgFnPc1d1
<g/>
,	,	kIx,
Čerokíové	Čerokíová	k1gFnPc1
<g/>
,	,	kIx,
Kríkové	Kríková	k1gFnPc1
<g/>
,	,	kIx,
Šavané	Šavaná	k1gFnPc1
...	...	k?
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
je	být	k5eAaImIp3nS
neúplný	úplný	k2eNgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
American	American	k1gInSc4
Revolutionary	Revolutionara	k1gFnSc2
War	War	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Americká	americký	k2eAgFnSc1d1
vojna	vojna	k1gFnSc1
za	za	k7c4
nezávislosť	závislostit	k5eNaPmRp2nS
na	na	k7c6
slovenské	slovenský	k2eAgFnSc3d1
Wikipedii	Wikipedie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Black	Black	k1gInSc1
<g/>
,	,	kIx,
Jeremy	Jerem	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
War	War	k1gFnSc1
for	forum	k1gNnPc2
America	Americ	k1gInSc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Fight	Fight	k1gInSc1
for	forum	k1gNnPc2
Independence	Independence	k1gFnSc2
<g/>
,	,	kIx,
1775	#num#	k4
<g/>
–	–	k?
<g/>
1783	#num#	k4
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analysis	Analysis	k1gFnSc1
from	froma	k1gFnPc2
a	a	k8xC
noted	noted	k1gInSc1
British	Britisha	k1gFnPc2
military	militara	k1gFnSc2
historian	historiany	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Boatner	Boatner	k1gMnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Mayo	Mayo	k1gMnSc1
<g/>
,	,	kIx,
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnPc1
of	of	k?
the	the	k?
American	American	k1gInSc1
Revolution	Revolution	k1gInSc1
<g/>
.	.	kIx.
1966	#num#	k4
<g/>
;	;	kIx,
revised	revised	k1gInSc1
1974	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8117	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
578	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Military	Militara	k1gFnSc2
topics	topicsa	k1gFnPc2
<g/>
,	,	kIx,
references	references	k1gMnSc1
many	mana	k1gFnSc2
secondary	secondara	k1gFnSc2
sources	sourcesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Chambers	Chambers	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Whiteclay	Whiteclaa	k1gFnSc2
II	II	kA
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
in	in	k?
chief	chief	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Oxford	Oxford	k1gInSc4
Companion	Companion	k1gInSc1
to	ten	k3xDgNnSc4
American	American	k1gMnSc1
Military	Militara	k1gFnSc2
History	Histor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
507198	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Duffy	Duff	k1gInPc1
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Military	Militara	k1gFnSc2
Experience	Experience	k1gFnSc2
in	in	k?
the	the	k?
Age	Age	k1gMnSc1
of	of	k?
Reason	Reason	k1gMnSc1
<g/>
,	,	kIx,
1715	#num#	k4
<g/>
–	–	k?
<g/>
1789	#num#	k4
<g/>
.	.	kIx.
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
689	#num#	k4
<g/>
-	-	kIx~
<g/>
11993	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Edler	Edler	k1gMnSc1
<g/>
,	,	kIx,
Friedrich	Friedrich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Dutch	Dutch	k1gMnSc1
Republic	Republice	k1gFnPc2
and	and	k?
The	The	k1gMnSc1
American	American	k1gMnSc1
Revolution	Revolution	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
of	of	k?
the	the	k?
Pacific	Pacific	k1gMnSc1
<g/>
,	,	kIx,
1911	#num#	k4
<g/>
,	,	kIx,
reprinted	reprinted	k1gInSc1
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
89875	#num#	k4
<g/>
-	-	kIx~
<g/>
269	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ellis	Ellis	k1gFnSc1
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
J.	J.	kA
His	his	k1gNnSc4
Excellency	Excellenca	k1gFnSc2
<g/>
:	:	kIx,
George	George	k1gInSc1
Washington	Washington	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
4000	#num#	k4
<g/>
-	-	kIx~
<g/>
4031	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Fenn	Fenn	k1gNnSc1
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
Anne	Anne	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pox	Pox	k1gFnSc1
Americana	Americana	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gFnSc7
Great	Great	k2eAgInSc4d1
Smallpox	Smallpox	k1gInSc4
Epidemic	Epidemice	k1gInPc2
of	of	k?
1775	#num#	k4
<g/>
–	–	k?
<g/>
82	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Hill	Hill	k1gMnSc1
and	and	k?
Wang	Wang	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8090	#num#	k4
<g/>
-	-	kIx~
<g/>
7820	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Americká	americký	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
</s>
<s>
Deklarace	deklarace	k1gFnSc1
nezávislosti	nezávislost	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
americká	americký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
americká	americký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
247257	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4131739-7	4131739-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85140139	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85140139	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Novověk	novověk	k1gInSc4
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
