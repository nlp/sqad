<s>
Americká	americký	k2eAgFnSc1d1
válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
(	(	kIx(
<g/>
1775	#num#	k4
<g/>
–	–	k?
<g/>
1783	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
americká	americký	k2eAgFnSc1d1
revoluční	revoluční	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
American	American	k1gMnSc1
Revolutionary	Revolutionara	k1gFnSc2
War	War	k1gMnSc1
<g/>
,	,	kIx,
American	American	k1gMnSc1
War	War	k1gFnSc2
of	of	k?
Independence	Independence	k1gFnSc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
válka	válka	k1gFnSc1
mezi	mezi	k7c7
Královstvím	království	k1gNnSc7
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
třinácti	třináct	k4xCc7
koloniemi	kolonie	k1gFnPc7
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>