<s>
Antonín	Antonín	k1gMnSc1	Antonín
Leopold	Leopold	k1gMnSc1	Leopold
Dvořák	Dvořák	k1gMnSc1	Dvořák
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1841	[number]	k4	1841
Nelahozeves	Nelahozevesa	k1gFnPc2	Nelahozevesa
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1904	[number]	k4	1904
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
hudebních	hudební	k2eAgMnPc2d1	hudební
skladatelů	skladatel	k1gMnPc2	skladatel
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
světově	světově	k6eAd1	světově
nejproslulejším	proslulý	k2eAgMnSc7d3	nejproslulejší
a	a	k8xC	a
nejhranějším	hraný	k2eAgMnSc7d3	nejhranější
českým	český	k2eAgMnSc7d1	český
skladatelem	skladatel	k1gMnSc7	skladatel
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
