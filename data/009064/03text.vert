<p>
<s>
HipZip	HipZip	k1gInSc1	HipZip
je	být	k5eAaImIp3nS	být
kapesní	kapesní	k2eAgInSc1d1	kapesní
multimediální	multimediální	k2eAgInSc1d1	multimediální
přehrávač	přehrávač	k1gInSc1	přehrávač
společnosti	společnost	k1gFnSc2	společnost
Iomega	Iomeg	k1gMnSc2	Iomeg
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
média	médium	k1gNnPc4	médium
používá	používat	k5eAaImIp3nS	používat
disky	disk	k1gInPc1	disk
PocketZip	PocketZip	k1gInSc1	PocketZip
a	a	k8xC	a
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
USB	USB	kA	USB
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
podporován	podporovat	k5eAaImNgMnS	podporovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přehrává	přehrávat	k5eAaImIp3nS	přehrávat
stereofonní	stereofonní	k2eAgFnPc4d1	stereofonní
audiosoubory	audiosoubora	k1gFnPc4	audiosoubora
se	s	k7c7	s
ztrátovou	ztrátový	k2eAgFnSc7d1	ztrátová
kompresí	komprese	k1gFnSc7	komprese
(	(	kIx(	(
<g/>
formáty	formát	k1gInPc1	formát
MP3	MP3	k1gFnSc2	MP3
a	a	k8xC	a
WMA	WMA	kA	WMA
7	[number]	k4	7
<g/>
)	)	kIx)	)
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
správu	správa	k1gFnSc4	správa
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
DRM	DRM	kA	DRM
<g/>
.	.	kIx.	.
</s>
<s>
Potřebné	potřebný	k2eAgInPc4d1	potřebný
údaje	údaj	k1gInPc4	údaj
(	(	kIx(	(
<g/>
ID	Ida	k1gFnPc2	Ida
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
na	na	k7c6	na
podsvíceném	podsvícený	k2eAgInSc6d1	podsvícený
displeji	displej	k1gInSc6	displej
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vestavěnný	vestavěnný	k2eAgInSc4d1	vestavěnný
ekvalizér	ekvalizér	k1gInSc4	ekvalizér
s	s	k7c7	s
nastavitelnými	nastavitelný	k2eAgInPc7d1	nastavitelný
alty	alt	k1gInPc7	alt
a	a	k8xC	a
basy	bas	k1gInPc7	bas
i	i	k8xC	i
regulátor	regulátor	k1gInSc1	regulátor
hlasitosti	hlasitost	k1gFnSc2	hlasitost
<g/>
.	.	kIx.	.
</s>
<s>
Energii	energie	k1gFnSc3	energie
čerpá	čerpat	k5eAaImIp3nS	čerpat
ze	z	k7c2	z
zabudovaných	zabudovaný	k2eAgInPc2d1	zabudovaný
nabíjecích	nabíjecí	k2eAgInPc2d1	nabíjecí
Li-Ion	Li-Ion	k1gNnSc4	Li-Ion
baterií	baterie	k1gFnPc2	baterie
s	s	k7c7	s
uváděnou	uváděný	k2eAgFnSc7d1	uváděná
výdrží	výdrž	k1gFnSc7	výdrž
cca	cca	kA	cca
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
reálně	reálně	k6eAd1	reálně
cca	cca	kA	cca
11	[number]	k4	11
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
přenosnými	přenosný	k2eAgNnPc7d1	přenosné
disky	disco	k1gNnPc7	disco
PocketZip	PocketZip	k1gInSc1	PocketZip
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
může	moct	k5eAaImIp3nS	moct
ukládat	ukládat	k5eAaImF	ukládat
nejen	nejen	k6eAd1	nejen
hudební	hudební	k2eAgMnSc1d1	hudební
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
datové	datový	k2eAgInPc1d1	datový
soubory	soubor	k1gInPc1	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
přístroje	přístroj	k1gInSc2	přístroj
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
a	a	k8xC	a
existuje	existovat	k5eAaImIp3nS	existovat
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
barevných	barevný	k2eAgNnPc6d1	barevné
provedeních	provedení	k1gNnPc6	provedení
<g/>
:	:	kIx,	:
kovově	kovově	k6eAd1	kovově
šedé	šedý	k2eAgFnSc2d1	šedá
(	(	kIx(	(
<g/>
metallic	metallice	k1gFnPc2	metallice
gray	graa	k1gFnSc2	graa
[	[	kIx(	[
<g/>
miˈ	miˈ	k?	miˈ
grei	gre	k1gFnSc2	gre
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
a	a	k8xC	a
modré	modré	k1gNnSc1	modré
<g/>
.	.	kIx.	.
<g/>
Dodával	dodávat	k5eAaImAgInS	dodávat
se	se	k3xPyFc4	se
včetně	včetně	k7c2	včetně
koženkového	koženkový	k2eAgNnSc2d1	koženkové
pouzdra	pouzdro	k1gNnSc2	pouzdro
<g/>
,	,	kIx,	,
pecek	pecek	k1gInSc1	pecek
Koss	Kossa	k1gFnPc2	Kossa
<g/>
,	,	kIx,	,
datového	datový	k2eAgInSc2d1	datový
kabelu	kabel	k1gInSc2	kabel
<g/>
,	,	kIx,	,
nabíječky	nabíječka	k1gFnSc2	nabíječka
<g/>
,	,	kIx,	,
3	[number]	k4	3
PocketZipů	PocketZip	k1gInPc2	PocketZip
a	a	k8xC	a
dvou	dva	k4xCgFnPc6	dva
CD	CD	kA	CD
s	s	k7c7	s
ovladači	ovladač	k1gInPc7	ovladač
<g/>
,	,	kIx,	,
sadou	sada	k1gFnSc7	sada
nástrojů	nástroj	k1gInPc2	nástroj
iomega	iomega	k1gFnSc1	iomega
tools	tools	k1gInSc1	tools
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
formátování	formátování	k1gNnSc4	formátování
a	a	k8xC	a
kopírování	kopírování	k1gNnSc4	kopírování
<g/>
)	)	kIx)	)
a	a	k8xC	a
programy	program	k1gInPc1	program
Windows	Windows	kA	Windows
Media	medium	k1gNnSc2	medium
Player	Player	k1gInSc4	Player
7	[number]	k4	7
a	a	k8xC	a
MusicMatch	MusicMatch	k1gInSc1	MusicMatch
Jukebox	Jukebox	k1gInSc1	Jukebox
Plus	plus	k1gInSc1	plus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
PocketZip	PocketZip	k1gInSc4	PocketZip
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ocelovém	ocelový	k2eAgNnSc6d1	ocelové
pouzdru	pouzdro	k1gNnSc6	pouzdro
uložený	uložený	k2eAgInSc4d1	uložený
magnetický	magnetický	k2eAgInSc4d1	magnetický
disk	disk	k1gInSc4	disk
(	(	kIx(	(
<g/>
příbuzný	příbuzný	k2eAgInSc4d1	příbuzný
ZIP	zip	k1gInSc4	zip
disku	disk	k1gInSc2	disk
od	od	k7c2	od
téhož	týž	k3xTgMnSc4	týž
výrobce	výrobce	k1gMnSc4	výrobce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
kompaktnímu	kompaktní	k2eAgInSc3d1	kompaktní
disku	disk	k1gInSc3	disk
má	mít	k5eAaImIp3nS	mít
čtvrtinové	čtvrtinový	k2eAgFnSc2d1	čtvrtinová
rozměry	rozměra	k1gFnSc2	rozměra
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
udávaná	udávaný	k2eAgFnSc1d1	udávaná
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
40	[number]	k4	40
MiB	MiB	k1gFnPc2	MiB
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozměrově	rozměrově	k6eAd1	rozměrově
menší	malý	k2eAgMnSc1d2	menší
a	a	k8xC	a
odolnější	odolný	k2eAgMnSc1d2	odolnější
než	než	k8xS	než
ZIP	zip	k1gInSc1	zip
disk	disk	k1gInSc1	disk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přenos	přenos	k1gInSc1	přenos
dat	datum	k1gNnPc2	datum
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
připojeného	připojený	k2eAgInSc2d1	připojený
počítače	počítač	k1gInSc2	počítač
se	se	k3xPyFc4	se
HipZip	HipZip	k1gInSc1	HipZip
hlásí	hlásit	k5eAaImIp3nS	hlásit
jako	jako	k9	jako
výměnný	výměnný	k2eAgInSc4d1	výměnný
disk	disk	k1gInSc4	disk
(	(	kIx(	(
<g/>
USB	USB	kA	USB
Mass	Mass	k1gInSc1	Mass
Storage	Storage	k1gInSc1	Storage
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
přenosová	přenosový	k2eAgFnSc1d1	přenosová
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
461	[number]	k4	461
KiB	KiB	k1gFnPc2	KiB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
zápis	zápis	k1gInSc1	zápis
<g/>
)	)	kIx)	)
a	a	k8xC	a
476	[number]	k4	476
KiB	KiB	k1gFnPc2	KiB
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
čtení	čtení	k1gNnSc1	čtení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přístupová	přístupový	k2eAgFnSc1d1	přístupová
doba	doba	k1gFnSc1	doba
činí	činit	k5eAaImIp3nS	činit
37,1	[number]	k4	37,1
ms.	ms.	k?	ms.
Naplnění	naplnění	k1gNnSc1	naplnění
celého	celý	k2eAgInSc2d1	celý
disku	disk	k1gInSc2	disk
trvá	trvat	k5eAaImIp3nS	trvat
86	[number]	k4	86
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
