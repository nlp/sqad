<p>
<s>
Trojský	trojský	k2eAgInSc1d1	trojský
most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
silniční	silniční	k2eAgInSc1d1	silniční
a	a	k8xC	a
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
most	most	k1gInSc1	most
přes	přes	k7c4	přes
Vltavu	Vltava	k1gFnSc4	Vltava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnosti	veřejnost	k1gFnPc4	veřejnost
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vyjela	vyjet	k5eAaPmAgFnS	vyjet
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
ulice	ulice	k1gFnSc2	ulice
v	v	k7c6	v
Holešovicích	Holešovice	k1gFnPc6	Holešovice
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
ulic	ulice	k1gFnPc2	ulice
Nové	Nové	k2eAgFnSc2d1	Nové
Povltavské	povltavský	k2eAgFnSc2d1	Povltavská
a	a	k8xC	a
Trojské	trojský	k2eAgFnSc2d1	Trojská
v	v	k7c4	v
Troji	troje	k4xRgMnPc1	troje
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
místní	místní	k2eAgFnSc3d1	místní
dopravě	doprava	k1gFnSc3	doprava
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
původní	původní	k2eAgInSc1d1	původní
provizorní	provizorní	k2eAgInSc1d1	provizorní
trojský	trojský	k2eAgInSc1d1	trojský
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
Rámusák	Rámusák	k1gInSc1	Rámusák
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sloužil	sloužit	k5eAaImAgMnS	sloužit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
předmostí	předmostí	k1gNnSc6	předmostí
je	být	k5eAaImIp3nS	být
mimoúrovňovou	mimoúrovňový	k2eAgFnSc7d1	mimoúrovňová
křižovatkou	křižovatka	k1gFnSc7	křižovatka
Troja	Trojus	k1gMnSc2	Trojus
propojen	propojen	k2eAgInSc1d1	propojen
s	s	k7c7	s
Městským	městský	k2eAgInSc7d1	městský
okruhem	okruh	k1gInSc7	okruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
Trojského	trojský	k2eAgInSc2d1	trojský
mostu	most	k1gInSc2	most
stál	stát	k5eAaImAgInS	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
provizorní	provizorní	k2eAgInSc1d1	provizorní
Trojský	trojský	k2eAgInSc1d1	trojský
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
sloužil	sloužit	k5eAaImAgInS	sloužit
pro	pro	k7c4	pro
tramvaje	tramvaj	k1gFnPc4	tramvaj
a	a	k8xC	a
chodce	chodec	k1gMnPc4	chodec
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
mostu	most	k1gInSc2	most
Trojského	trojský	k2eAgInSc2d1	trojský
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Architektonická	architektonický	k2eAgFnSc1d1	architektonická
soutěž	soutěž	k1gFnSc1	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
Architektonicko-konstrukční	architektonickoonstrukční	k2eAgNnSc4d1	architektonicko-konstrukční
řešení	řešení	k1gNnSc4	řešení
vybral	vybrat	k5eAaPmAgInS	vybrat
Magistrát	magistrát	k1gInSc1	magistrát
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
z	z	k7c2	z
20	[number]	k4	20
návrhů	návrh	k1gInPc2	návrh
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
vypsané	vypsaný	k2eAgFnSc2d1	vypsaná
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
cena	cena	k1gFnSc1	cena
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
odměnou	odměna	k1gFnSc7	odměna
700	[number]	k4	700
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2006	[number]	k4	2006
udělena	udělit	k5eAaPmNgFnS	udělit
návrhu	návrh	k1gInSc2	návrh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
Ing.	ing.	kA	ing.
Jiří	Jiří	k1gMnSc1	Jiří
Petrák	Petrák	k1gMnSc1	Petrák
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Ladislav	Ladislav	k1gMnSc1	Ladislav
Šašek	Šašek	k1gMnSc1	Šašek
(	(	kIx(	(
<g/>
Mott	motto	k1gNnPc2	motto
MacDonald	Macdonald	k1gMnSc1	Macdonald
Praha	Praha	k1gFnSc1	Praha
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Doc.	doc.	kA	doc.
Ing.	ing.	kA	ing.
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
Koucký	Koucký	k1gMnSc1	Koucký
a	a	k8xC	a
Ing.	ing.	kA	ing.
akad	akad	k1gInSc1	akad
<g/>
.	.	kIx.	.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Libor	Libor	k1gMnSc1	Libor
Kábrt	Kábrt	k1gInSc1	Kábrt
(	(	kIx(	(
<g/>
Roman	Roman	k1gMnSc1	Roman
Koucký	Koucký	k1gMnSc1	Koucký
architektonická	architektonický	k2eAgFnSc1d1	architektonická
kancelář	kancelář	k1gFnSc1	kancelář
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
cena	cena	k1gFnSc1	cena
nebyla	být	k5eNaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc4	třetí
cenu	cena	k1gFnSc4	cena
(	(	kIx(	(
<g/>
450.000	[number]	k4	450.000
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
získaly	získat	k5eAaPmAgInP	získat
dva	dva	k4xCgInPc1	dva
návrhy	návrh	k1gInPc1	návrh
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
prvního	první	k4xOgNnSc2	první
je	být	k5eAaImIp3nS	být
Prof.	prof.	kA	prof.
Ing.	ing.	kA	ing.
Jiří	Jiří	k1gMnSc1	Jiří
Stráský	Stráský	k1gMnSc1	Stráský
(	(	kIx(	(
<g/>
Stráský	Stráský	k1gMnSc1	Stráský
<g/>
,	,	kIx,	,
Hustý	hustý	k2eAgMnSc1d1	hustý
a	a	k8xC	a
partneři	partner	k1gMnPc1	partner
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
druhého	druhý	k4xOgMnSc4	druhý
Ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Titz	Titz	k1gMnSc1	Titz
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Ladislav	Ladislav	k1gMnSc1	Ladislav
Huryta	Huryta	k1gMnSc1	Huryta
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Matůšů	Matůš	k1gMnPc2	Matůš
(	(	kIx(	(
<g/>
ARCHicon	ARCHicon	k1gNnSc1	ARCHicon
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
bylo	být	k5eAaImAgNnS	být
hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
celkem	celkem	k6eAd1	celkem
pět	pět	k4xCc1	pět
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
dalšího	další	k1gNnSc2	další
(	(	kIx(	(
<g/>
č.	č.	k?	č.
4	[number]	k4	4
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
Ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
Gale	Gale	k1gMnSc1	Gale
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Palaščák	Palaščák	k1gMnSc1	Palaščák
<g/>
,	,	kIx,	,
spoluautoři	spoluautor	k1gMnPc5	spoluautor
<g/>
:	:	kIx,	:
Bc.	Bc.	k1gFnSc1	Bc.
Barbora	Barbora	k1gFnSc1	Barbora
Šimonová	Šimonová	k1gFnSc1	Šimonová
<g/>
,	,	kIx,	,
Bc.	Bc.	k1gMnSc1	Bc.
Libor	Libor	k1gMnSc1	Libor
Dašek	Dašek	k1gMnSc1	Dašek
(	(	kIx(	(
<g/>
Atelier	atelier	k1gNnSc1	atelier
AGP	AGP	kA	AGP
<g/>
)	)	kIx)	)
a	a	k8xC	a
dalšího	další	k2eAgMnSc2d1	další
(	(	kIx(	(
<g/>
č.	č.	k?	č.
9	[number]	k4	9
<g/>
)	)	kIx)	)
Ing.	ing.	kA	ing.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Rösler	Rösler	k1gMnSc1	Rösler
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Vít	Vít	k1gMnSc1	Vít
Havlíček	Havlíček	k1gMnSc1	Havlíček
<g/>
,	,	kIx,	,
spolupráce	spolupráce	k1gFnSc1	spolupráce
<g/>
:	:	kIx,	:
Michaela	Michaela	k1gFnSc1	Michaela
Chvojková	Chvojková	k1gFnSc1	Chvojková
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
.	.	kIx.	.
</s>
<s>
Porota	porota	k1gFnSc1	porota
měla	mít	k5eAaImAgFnS	mít
11	[number]	k4	11
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
2	[number]	k4	2
zástupci	zástupce	k1gMnPc7	zástupce
magistrátu	magistrát	k1gInSc2	magistrát
<g/>
,	,	kIx,	,
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
zástupci	zástupce	k1gMnPc7	zástupce
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
Praha	Praha	k1gFnSc1	Praha
7	[number]	k4	7
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
8	[number]	k4	8
<g/>
,	,	kIx,	,
4	[number]	k4	4
zástupci	zástupce	k1gMnPc7	zástupce
České	český	k2eAgFnSc2d1	Česká
komory	komora	k1gFnSc2	komora
architektů	architekt	k1gMnPc2	architekt
<g/>
,	,	kIx,	,
2	[number]	k4	2
zástupci	zástupce	k1gMnPc7	zástupce
České	český	k2eAgFnSc2d1	Česká
komory	komora	k1gFnSc2	komora
autorizovaných	autorizovaný	k2eAgMnPc2d1	autorizovaný
inženýrů	inženýr	k1gMnPc2	inženýr
a	a	k8xC	a
techniků	technik	k1gMnPc2	technik
činných	činný	k2eAgMnPc2d1	činný
ve	v	k7c6	v
výstavbě	výstavba	k1gFnSc6	výstavba
a	a	k8xC	a
1	[number]	k4	1
zástupce	zástupce	k1gMnSc1	zástupce
České	český	k2eAgFnSc2d1	Česká
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
systémovou	systémový	k2eAgFnSc4d1	systémová
integraci	integrace	k1gFnSc4	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
odměnách	odměna	k1gFnPc6	odměna
bylo	být	k5eAaImAgNnS	být
autorům	autor	k1gMnPc3	autor
pěti	pět	k4xCc2	pět
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
postoupily	postoupit	k5eAaPmAgFnP	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
celkem	celek	k1gInSc7	celek
2,6	[number]	k4	2,6
milionu	milion	k4xCgInSc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nedodržené	dodržený	k2eNgNnSc1d1	nedodržené
zadání	zadání	k1gNnSc1	zadání
===	===	k?	===
</s>
</p>
<p>
<s>
Provedení	provedení	k1gNnSc1	provedení
mostu	most	k1gInSc2	most
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
tématem	téma	k1gNnSc7	téma
politických	politický	k2eAgFnPc2d1	politická
diskusí	diskuse	k1gFnPc2	diskuse
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
navýšení	navýšení	k1gNnSc3	navýšení
původně	původně	k6eAd1	původně
kalkulované	kalkulovaný	k2eAgFnPc1d1	kalkulovaná
ceny	cena	k1gFnPc1	cena
tunelu	tunel	k1gInSc2	tunel
Blanka	Blanka	k1gFnSc1	Blanka
a	a	k8xC	a
navazujících	navazující	k2eAgFnPc2d1	navazující
staveb	stavba	k1gFnPc2	stavba
Městského	městský	k2eAgInSc2d1	městský
okruhu	okruh	k1gInSc2	okruh
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
původní	původní	k2eAgFnSc2d1	původní
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
Metrostavu	metrostav	k1gInSc2	metrostav
uvedl	uvést	k5eAaPmAgMnS	uvést
jako	jako	k8xC	jako
příklad	příklad	k1gInSc4	příklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
zadávací	zadávací	k2eAgFnSc6d1	zadávací
dokumentaci	dokumentace	k1gFnSc6	dokumentace
byl	být	k5eAaImAgInS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
most	most	k1gInSc1	most
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
asi	asi	k9	asi
400	[number]	k4	400
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
kontraktu	kontrakt	k1gInSc2	kontrakt
s	s	k7c7	s
Metrostavem	metrostav	k1gInSc7	metrostav
však	však	k9	však
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
konstrukčně-architektonickou	konstrukčněrchitektonický	k2eAgFnSc4d1	konstrukčně-architektonický
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
most	most	k1gInSc1	most
jiný	jiný	k2eAgInSc1d1	jiný
–	–	k?	–
výrazně	výrazně	k6eAd1	výrazně
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
novými	nový	k2eAgInPc7d1	nový
požadavky	požadavek	k1gInPc7	požadavek
<g />
.	.	kIx.	.
</s>
<s>
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
navýšení	navýšení	k1gNnSc3	navýšení
jeho	jeho	k3xOp3gFnSc2	jeho
ceny	cena	k1gFnSc2	cena
o	o	k7c4	o
720	[number]	k4	720
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
téměř	téměř	k6eAd1	téměř
na	na	k7c4	na
trojnásobek	trojnásobek	k1gInSc4	trojnásobek
<g/>
.	.	kIx.	.
<g/>
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
udělil	udělit	k5eAaPmAgMnS	udělit
hlavnímu	hlavní	k2eAgNnSc3d1	hlavní
městu	město	k1gNnSc3	město
Praze	Praha	k1gFnSc3	Praha
pokutu	pokuta	k1gFnSc4	pokuta
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
Kč	Kč	kA	Kč
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
ve	v	k7c6	v
zcela	zcela	k6eAd1	zcela
jiné	jiný	k2eAgFnSc6d1	jiná
podobě	podoba	k1gFnSc6	podoba
a	a	k8xC	a
podle	podle	k7c2	podle
zcela	zcela	k6eAd1	zcela
jiného	jiný	k2eAgInSc2d1	jiný
architektonického	architektonický	k2eAgInSc2d1	architektonický
návrhu	návrh	k1gInSc2	návrh
<g/>
,	,	kIx,	,
než	než	k8xS	než
jaký	jaký	k3yQgInSc4	jaký
byl	být	k5eAaImAgMnS	být
obsahem	obsah	k1gInSc7	obsah
původní	původní	k2eAgFnSc2d1	původní
zadávací	zadávací	k2eAgFnSc2d1	zadávací
dokumentace	dokumentace	k1gFnSc2	dokumentace
na	na	k7c4	na
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
zakázku	zakázka	k1gFnSc4	zakázka
<g/>
,	,	kIx,	,
také	také	k9	také
vybraný	vybraný	k2eAgMnSc1d1	vybraný
uchazeč	uchazeč	k1gMnSc1	uchazeč
pro	pro	k7c4	pro
zadavatele	zadavatel	k1gMnPc4	zadavatel
provedl	provést	k5eAaPmAgMnS	provést
stavební	stavební	k2eAgFnPc4d1	stavební
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
zadány	zadat	k5eAaPmNgFnP	zadat
v	v	k7c6	v
řádném	řádný	k2eAgNnSc6d1	řádné
zadávacím	zadávací	k2eAgNnSc6d1	zadávací
řízení	řízení	k1gNnSc6	řízení
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
veřejných	veřejný	k2eAgFnPc6d1	veřejná
zakázkách	zakázka	k1gFnPc6	zakázka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
podala	podat	k5eAaPmAgFnS	podat
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
proti	proti	k7c3	proti
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
rozklad	rozklad	k1gInSc1	rozklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předpokládané	předpokládaný	k2eAgNnSc1d1	předpokládané
zahájení	zahájení	k1gNnSc1	zahájení
===	===	k?	===
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
uváděl	uvádět	k5eAaImAgInS	uvádět
jako	jako	k9	jako
zamýšlený	zamýšlený	k2eAgInSc1d1	zamýšlený
termín	termín	k1gInSc1	termín
zprovoznění	zprovoznění	k1gNnSc2	zprovoznění
mostu	most	k1gInSc2	most
rok	rok	k1gInSc1	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
zprovoznění	zprovoznění	k1gNnSc1	zprovoznění
tunelu	tunel	k1gInSc2	tunel
Blanka	Blanka	k1gFnSc1	Blanka
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
mezitím	mezitím	k6eAd1	mezitím
odloženo	odložit	k5eAaPmNgNnS	odložit
na	na	k7c4	na
konec	konec	k1gInSc4	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
magistrátního	magistrátní	k2eAgInSc2d1	magistrátní
odboru	odbor	k1gInSc2	odbor
městského	městský	k2eAgMnSc2d1	městský
investora	investor	k1gMnSc2	investor
Jiří	Jiří	k1gMnSc1	Jiří
Toman	Toman	k1gMnSc1	Toman
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
most	most	k1gInSc1	most
v	v	k7c4	v
Troji	troje	k4xRgFnSc4	troje
bude	být	k5eAaImBp3nS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
ke	k	k7c3	k
dni	den	k1gInSc3	den
otevření	otevření	k1gNnSc2	otevření
tunelu	tunel	k1gInSc2	tunel
Blanka	Blanka	k1gFnSc1	Blanka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výstavba	výstavba	k1gFnSc1	výstavba
===	===	k?	===
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
mostu	most	k1gInSc2	most
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
ředitel	ředitel	k1gMnSc1	ředitel
odboru	odbor	k1gInSc2	odbor
městského	městský	k2eAgMnSc4d1	městský
investora	investor	k1gMnSc4	investor
MHMP	MHMP	kA	MHMP
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
most	most	k1gInSc1	most
nebude	být	k5eNaImBp3nS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
plánovalo	plánovat	k5eAaImAgNnS	plánovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
s	s	k7c7	s
celým	celý	k2eAgInSc7d1	celý
tunelovým	tunelový	k2eAgInSc7d1	tunelový
komplexem	komplex	k1gInSc7	komplex
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nejdříve	dříve	k6eAd3	dříve
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pro	pro	k7c4	pro
tramvaje	tramvaj	k1gFnPc4	tramvaj
bude	být	k5eAaImBp3nS	být
možná	možná	k6eAd1	možná
zprovozněn	zprovoznit	k5eAaPmNgInS	zprovoznit
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
ze	z	k7c2	z
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
most	most	k1gInSc4	most
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
zprovozněn	zprovozněn	k2eAgInSc4d1	zprovozněn
pro	pro	k7c4	pro
tramvaje	tramvaj	k1gFnPc4	tramvaj
a	a	k8xC	a
nahradit	nahradit	k5eAaPmF	nahradit
tak	tak	k6eAd1	tak
provizorní	provizorní	k2eAgInSc1d1	provizorní
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
most	most	k1gInSc1	most
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
rozebrán	rozebrán	k2eAgInSc1d1	rozebrán
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zpráv	zpráva	k1gFnPc2	zpráva
z	z	k7c2	z
jara	jaro	k1gNnSc2	jaro
2013	[number]	k4	2013
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
zprovoznění	zprovoznění	k1gNnSc3	zprovoznění
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
až	až	k9	až
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
silniční	silniční	k2eAgNnPc4d1	silniční
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
,	,	kIx,	,
cyklisty	cyklista	k1gMnSc2	cyklista
a	a	k8xC	a
chodce	chodec	k1gMnSc2	chodec
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
až	až	k6eAd1	až
s	s	k7c7	s
otevřením	otevření	k1gNnSc7	otevření
tunelu	tunel	k1gInSc2	tunel
Blanka	Blanka	k1gFnSc1	Blanka
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Trojský	trojský	k2eAgInSc1d1	trojský
most	most	k1gInSc1	most
byl	být	k5eAaImAgInS	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
zprovozněn	zprovozněn	k2eAgInSc4d1	zprovozněn
pro	pro	k7c4	pro
tramvaje	tramvaj	k1gFnPc4	tramvaj
i	i	k9	i
pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Tunelový	tunelový	k2eAgInSc1d1	tunelový
komplex	komplex	k1gInSc1	komplex
Blanka	Blanka	k1gFnSc1	Blanka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
otevřený	otevřený	k2eAgInSc4d1	otevřený
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
termínu	termín	k1gInSc6	termín
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zprovozněn	zprovozněn	k2eAgMnSc1d1	zprovozněn
až	až	k9	až
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
soud	soud	k1gInSc4	soud
rozhodoval	rozhodovat	k5eAaImAgMnS	rozhodovat
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
Praha	Praha	k1gFnSc1	Praha
Metrostavu	metrostav	k1gInSc2	metrostav
za	za	k7c4	za
most	most	k1gInSc4	most
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2014	[number]	k4	2014
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
zatěžkávací	zatěžkávací	k2eAgFnPc1d1	zatěžkávací
zkoušky	zkouška	k1gFnPc1	zkouška
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
den	den	k1gInSc4	den
dynamické	dynamický	k2eAgFnPc1d1	dynamická
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
statické	statický	k2eAgFnSc6d1	statická
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
na	na	k7c4	na
most	most	k1gInSc4	most
najelo	najet	k5eAaPmAgNnS	najet
24	[number]	k4	24
plně	plně	k6eAd1	plně
naložených	naložený	k2eAgNnPc2d1	naložené
nákladních	nákladní	k2eAgNnPc2d1	nákladní
aut	auto	k1gNnPc2	auto
a	a	k8xC	a
4	[number]	k4	4
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
.	.	kIx.	.
<g/>
Výstavba	výstavba	k1gFnSc1	výstavba
mostu	most	k1gInSc2	most
vyšla	vyjít	k5eAaPmAgFnS	vyjít
celkem	celkem	k6eAd1	celkem
na	na	k7c4	na
1,3	[number]	k4	1,3
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
byl	být	k5eAaImAgInS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
návrh	návrh	k1gInSc1	návrh
síťového	síťový	k2eAgInSc2d1	síťový
obloukového	obloukový	k2eAgInSc2d1	obloukový
mostu	most	k1gInSc2	most
s	s	k7c7	s
dolní	dolní	k2eAgFnSc7d1	dolní
mostovkou	mostovka	k1gFnSc7	mostovka
<g/>
.	.	kIx.	.
</s>
<s>
Předpjatá	předpjatý	k2eAgFnSc1d1	předpjatá
deska	deska	k1gFnSc1	deska
mostovky	mostovka	k1gFnSc2	mostovka
je	být	k5eAaImIp3nS	být
uložena	uložen	k2eAgFnSc1d1	uložena
na	na	k7c6	na
prefabrikovaných	prefabrikovaný	k2eAgInPc6d1	prefabrikovaný
předpjatých	předpjatý	k2eAgInPc6d1	předpjatý
příčnících	příčník	k1gInPc6	příčník
zavěšených	zavěšený	k2eAgInPc6d1	zavěšený
přes	přes	k7c4	přes
síťový	síťový	k2eAgInSc4d1	síťový
systém	systém	k1gInSc4	systém
ocelových	ocelový	k2eAgNnPc2d1	ocelové
táhel	táhlo	k1gNnPc2	táhlo
do	do	k7c2	do
plochého	plochý	k2eAgInSc2d1	plochý
ocelového	ocelový	k2eAgInSc2d1	ocelový
obloukového	obloukový	k2eAgInSc2d1	obloukový
nosníku	nosník	k1gInSc2	nosník
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
mostu	most	k1gInSc2	most
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
262	[number]	k4	262
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc4	rozpětí
oblouku	oblouk	k1gInSc2	oblouk
hlavního	hlavní	k2eAgNnSc2d1	hlavní
pole	pole	k1gNnSc2	pole
200,4	[number]	k4	200,4
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
výška	výška	k1gFnSc1	výška
nosné	nosný	k2eAgFnSc2d1	nosná
konstrukce	konstrukce	k1gFnSc2	konstrukce
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
34	[number]	k4	34
metry	metr	k1gInPc4	metr
nad	nad	k7c7	nad
maximální	maximální	k2eAgFnSc7d1	maximální
plavební	plavební	k2eAgFnSc7d1	plavební
hladinou	hladina	k1gFnSc7	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Opěry	opěra	k1gFnPc1	opěra
a	a	k8xC	a
pilíře	pilíř	k1gInPc1	pilíř
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
vrtaných	vrtaný	k2eAgFnPc6d1	vrtaná
pilotách	pilota	k1gFnPc6	pilota
ve	v	k7c6	v
skalním	skalní	k2eAgNnSc6d1	skalní
podloží	podloží	k1gNnSc6	podloží
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
<g/>
,	,	kIx,	,
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
řeky	řeka	k1gFnSc2	řeka
žádný	žádný	k3yNgInSc4	žádný
pilíř	pilíř	k1gInSc4	pilíř
nezasahuje	zasahovat	k5eNaImIp3nS	zasahovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
jízdních	jízdní	k2eAgInPc6d1	jízdní
pruzích	pruh	k1gInPc6	pruh
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
střední	střední	k2eAgNnSc4d1	střední
tramvajové	tramvajový	k2eAgNnSc4d1	tramvajové
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
oboustranné	oboustranný	k2eAgInPc4d1	oboustranný
chodníky	chodník	k1gInPc4	chodník
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
a	a	k8xC	a
cyklisty	cyklista	k1gMnPc4	cyklista
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
křižovatky	křižovatka	k1gFnSc2	křižovatka
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
(	(	kIx(	(
<g/>
trojském	trojský	k2eAgNnSc6d1	Trojské
<g/>
)	)	kIx)	)
předmostí	předmostí	k1gNnSc6	předmostí
je	být	k5eAaImIp3nS	být
mimoúrovňová	mimoúrovňový	k2eAgFnSc1d1	mimoúrovňová
křižovatka	křižovatka	k1gFnSc1	křižovatka
Troja	Troja	k1gFnSc1	Troja
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
zaústění	zaústění	k1gNnSc1	zaústění
tunelu	tunel	k1gInSc2	tunel
Blanka	Blanka	k1gFnSc1	Blanka
na	na	k7c6	na
Městském	městský	k2eAgInSc6d1	městský
okruhu	okruh	k1gInSc6	okruh
<g/>
,	,	kIx,	,
s	s	k7c7	s
Městským	městský	k2eAgInSc7d1	městský
okruhem	okruh	k1gInSc7	okruh
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
frekvence	frekvence	k1gFnSc1	frekvence
z	z	k7c2	z
mostu	most	k1gInSc2	most
mimoúrovňově	mimoúrovňově	k6eAd1	mimoúrovňově
kříží	křížit	k5eAaImIp3nP	křížit
s	s	k7c7	s
propojením	propojení	k1gNnSc7	propojení
všech	všecek	k3xTgInPc2	všecek
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
na	na	k7c4	na
místní	místní	k2eAgFnSc4d1	místní
komunikaci	komunikace	k1gFnSc4	komunikace
v	v	k7c4	v
Troji	troje	k4xRgFnSc4	troje
(	(	kIx(	(
<g/>
Povltavská	povltavský	k2eAgFnSc1d1	Povltavská
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
navazuje	navazovat	k5eAaImIp3nS	navazovat
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
komunikace	komunikace	k1gFnSc1	komunikace
z	z	k7c2	z
mostu	most	k1gInSc2	most
úrovňovou	úrovňový	k2eAgFnSc7d1	úrovňová
křižovatkou	křižovatka	k1gFnSc7	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
na	na	k7c6	na
trojské	trojský	k2eAgFnSc6d1	Trojská
straně	strana	k1gFnSc6	strana
most	most	k1gInSc1	most
podchází	podcházet	k5eAaImIp3nP	podcházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
redaktora	redaktor	k1gMnSc2	redaktor
trj	trj	k?	trj
z	z	k7c2	z
Práva	právo	k1gNnSc2	právo
považují	považovat	k5eAaImIp3nP	považovat
kritici	kritik	k1gMnPc1	kritik
dva	dva	k4xCgInPc4	dva
pruhy	pruh	k1gInPc4	pruh
vozovky	vozovka	k1gFnSc2	vozovka
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
směru	směr	k1gInSc6	směr
za	za	k7c4	za
naddimenzování	naddimenzování	k1gNnSc4	naddimenzování
<g/>
,	,	kIx,	,
neúměrné	úměrný	k2eNgFnSc6d1	neúměrná
šíři	šíře	k1gFnSc4	šíře
železničního	železniční	k2eAgInSc2d1	železniční
podjezdu	podjezd	k1gInSc2	podjezd
navazujícího	navazující	k2eAgInSc2d1	navazující
na	na	k7c6	na
holešovické	holešovický	k2eAgFnSc6d1	Holešovická
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
úvahy	úvaha	k1gFnPc1	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
most	most	k1gInSc1	most
mohl	moct	k5eAaImAgInS	moct
jmenovat	jmenovat	k5eAaImF	jmenovat
po	po	k7c6	po
zesnulém	zesnulý	k1gMnSc6	zesnulý
prezidentovi	prezident	k1gMnSc6	prezident
Václavu	Václav	k1gMnSc6	Václav
Havlovi	Havel	k1gMnSc6	Havel
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
však	však	k9	však
rada	rada	k1gFnSc1	rada
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
oficiálně	oficiálně	k6eAd1	oficiálně
schválila	schválit	k5eAaPmAgFnS	schválit
vžitý	vžitý	k2eAgInSc4d1	vžitý
název	název	k1gInSc4	název
Trojský	trojský	k2eAgInSc4d1	trojský
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Trojský	trojský	k2eAgInSc1d1	trojský
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
most	most	k1gInSc1	most
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Trojský	trojský	k2eAgInSc4d1	trojský
most	most	k1gInSc4	most
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
Trójský	trójský	k2eAgInSc1d1	trójský
most	most	k1gInSc1	most
–	–	k?	–
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
projektu	projekt	k1gInSc6	projekt
<g/>
,	,	kIx,	,
web	web	k1gInSc1	web
Tunelový	tunelový	k2eAgInSc4d1	tunelový
komplex	komplex	k1gInSc4	komplex
Blanka	Blanka	k1gFnSc1	Blanka
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
sdružený	sdružený	k2eAgInSc1d1	sdružený
městský	městský	k2eAgInSc1d1	městský
most	most	k1gInSc1	most
přes	přes	k7c4	přes
Vltavu	Vltava	k1gFnSc4	Vltava
–	–	k?	–
návrhy	návrh	k1gInPc4	návrh
mostu	most	k1gInSc2	most
z	z	k7c2	z
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
architektonické	architektonický	k2eAgFnSc2d1	architektonická
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
komora	komora	k1gFnSc1	komora
architektů	architekt	k1gMnPc2	architekt
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
aktualizace	aktualizace	k1gFnSc1	aktualizace
<g/>
:	:	kIx,	:
12	[number]	k4	12
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Trojský	trojský	k2eAgInSc4d1	trojský
most	most	k1gInSc4	most
(	(	kIx(	(
<g/>
soutěžní	soutěžní	k2eAgFnSc1d1	soutěžní
varianta	varianta	k1gFnSc1	varianta
<g/>
)	)	kIx)	)
-	-	kIx~	-
Silniční	silniční	k2eAgInSc1d1	silniční
a	a	k8xC	a
tramvajový	tramvajový	k2eAgInSc1d1	tramvajový
most	most	k1gInSc1	most
-	-	kIx~	-
starší	starý	k2eAgInSc4d2	starší
návrh	návrh	k1gInSc4	návrh
mostu	most	k1gInSc2	most
použitý	použitý	k2eAgInSc4d1	použitý
v	v	k7c6	v
části	část	k1gFnSc6	část
stavební	stavební	k2eAgFnSc2d1	stavební
dokumentace	dokumentace	k1gFnSc2	dokumentace
<g/>
,	,	kIx,	,
web	web	k1gInSc4	web
Ing.	ing.	kA	ing.
Jaroslav	Jaroslava	k1gFnPc2	Jaroslava
Čambula	Čambula	k1gFnSc1	Čambula
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
stavební	stavební	k2eAgFnSc2d1	stavební
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Situační	situační	k2eAgInSc1d1	situační
výkres	výkres	k1gInSc1	výkres
–	–	k?	–
zákres	zákres	k1gInSc1	zákres
výše	výše	k1gFnSc2	výše
uvedeného	uvedený	k2eAgInSc2d1	uvedený
staršího	starý	k2eAgInSc2d2	starší
návrhu	návrh	k1gInSc2	návrh
<g/>
,	,	kIx,	,
prezentovaný	prezentovaný	k2eAgInSc1d1	prezentovaný
v	v	k7c6	v
informačním	informační	k2eAgNnSc6d1	informační
středisku	středisko	k1gNnSc6	středisko
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Vondrys	Vondrys	k1gInSc1	Vondrys
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Trojský	trojský	k2eAgInSc4d1	trojský
most	most	k1gInSc4	most
ze	z	k7c2	z
sedla	sedlo	k1gNnSc2	sedlo
kola	kolo	k1gNnSc2	kolo
-	-	kIx~	-
NaKole	NaKole	k1gFnSc1	NaKole
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
</s>
</p>
