<s>
Cetiosaurus	Cetiosaurus	k1gInSc1	Cetiosaurus
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
velrybí	velrybí	k2eAgMnSc1d1	velrybí
ještěr	ještěr	k1gMnSc1	ještěr
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
15	[number]	k4	15
až	až	k9	až
18	[number]	k4	18
metrů	metr	k1gInPc2	metr
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
býložravý	býložravý	k2eAgMnSc1d1	býložravý
sauropodní	sauropodní	k2eAgMnSc1d1	sauropodní
dinosaurus	dinosaurus	k1gMnSc1	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
hmotnosti	hmotnost	k1gFnPc4	hmotnost
až	až	k9	až
25	[number]	k4	25
tun	tuna	k1gFnPc2	tuna
a	a	k8xC	a
žil	žíla	k1gFnPc2	žíla
v	v	k7c6	v
období	období	k1gNnSc6	období
střední	střední	k2eAgFnSc2d1	střední
až	až	k8xS	až
svrchní	svrchní	k2eAgFnSc2d1	svrchní
jury	jura	k1gFnSc2	jura
<g/>
,	,	kIx,	,
asi	asi	k9	asi
před	před	k7c7	před
181	[number]	k4	181
až	až	k8xS	až
169	[number]	k4	169
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Fosílie	fosílie	k1gFnPc1	fosílie
tohoto	tento	k3xDgMnSc2	tento
dinosaura	dinosaurus	k1gMnSc2	dinosaurus
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Maroku	Maroko	k1gNnSc6	Maroko
<g/>
.	.	kIx.	.
</s>
<s>
Cetiosaurus	Cetiosaurus	k1gMnSc1	Cetiosaurus
byl	být	k5eAaImAgMnS	být
typický	typický	k2eAgMnSc1d1	typický
představitel	představitel	k1gMnSc1	představitel
vývojově	vývojově	k6eAd1	vývojově
primitivních	primitivní	k2eAgInPc2d1	primitivní
sauropodů	sauropod	k1gInPc2	sauropod
<g/>
.	.	kIx.	.
</s>
<s>
Krk	krk	k1gInSc1	krk
cetiosaura	cetiosaur	k1gMnSc2	cetiosaur
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
rodu	rod	k1gInSc2	rod
Diplodocus	Diplodocus	k1gMnSc1	Diplodocus
<g/>
,	,	kIx,	,
neschopný	schopný	k2eNgMnSc1d1	neschopný
vyzdvihnout	vyzdvihnout	k5eAaPmF	vyzdvihnout
hlavu	hlava	k1gFnSc4	hlava
nad	nad	k7c4	nad
úroveň	úroveň	k1gFnSc4	úroveň
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tento	tento	k3xDgMnSc1	tento
tvor	tvor	k1gMnSc1	tvor
spásal	spásat	k5eAaPmAgMnS	spásat
výlučně	výlučně	k6eAd1	výlučně
nízkou	nízký	k2eAgFnSc4d1	nízká
vegetaci	vegetace	k1gFnSc4	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vyspělejších	vyspělý	k2eAgMnPc2d2	vyspělejší
sauropodů	sauropod	k1gMnPc2	sauropod
ještě	ještě	k9	ještě
neměl	mít	k5eNaImAgMnS	mít
duté	dutý	k2eAgInPc4d1	dutý
obratle	obratel	k1gInPc4	obratel
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgFnPc2	který
zasahovaly	zasahovat	k5eAaImAgInP	zasahovat
vzdušné	vzdušný	k2eAgInPc1d1	vzdušný
vaky	vak	k1gInPc1	vak
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgFnPc1d1	podobná
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
mají	mít	k5eAaImIp3nP	mít
dnešní	dnešní	k2eAgMnPc1d1	dnešní
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
byly	být	k5eAaImAgInP	být
původně	původně	k6eAd1	původně
pokládané	pokládaný	k2eAgInPc4d1	pokládaný
za	za	k7c4	za
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
velryby	velryba	k1gFnSc2	velryba
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
rodové	rodový	k2eAgNnSc1d1	rodové
jméno	jméno	k1gNnSc1	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
historicky	historicky	k6eAd1	historicky
známého	známý	k2eAgMnSc4d1	známý
Sauropoda	Sauropod	k1gMnSc4	Sauropod
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnPc1	jeho
fosílie	fosílie	k1gFnPc1	fosílie
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
již	již	k6eAd1	již
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
a	a	k8xC	a
samotný	samotný	k2eAgInSc1d1	samotný
rod	rod	k1gInSc1	rod
byl	být	k5eAaImAgInS	být
popsán	popsán	k2eAgInSc1d1	popsán
roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prehistorický	prehistorický	k2eAgInSc1d1	prehistorický
plaz	plaz	k1gInSc1	plaz
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
cetiosaura	cetiosaura	k1gFnSc1	cetiosaura
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
stehenní	stehenní	k2eAgFnSc1d1	stehenní
kost	kost	k1gFnSc1	kost
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
jako	jako	k8xC	jako
dospělý	dospělý	k2eAgMnSc1d1	dospělý
muž	muž	k1gMnSc1	muž
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1,8	[number]	k4	1,8
metru	metr	k1gInSc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fastovsky	Fastovsky	k6eAd1	Fastovsky
DE	DE	k?	DE
<g/>
,	,	kIx,	,
Weishampel	Weishampel	k1gMnSc1	Weishampel
DB	db	kA	db
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Sauropodomorpha	Sauropodomorpha	k1gFnSc1	Sauropodomorpha
<g/>
:	:	kIx,	:
<g/>
The	The	k1gMnSc5	The
Big	Big	k1gMnSc5	Big
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Bizarre	Bizarr	k1gMnSc5	Bizarr
&	&	k?	&
The	The	k1gFnPc1	The
Majestic	Majestice	k1gFnPc2	Majestice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
in	in	k?	in
Fastovsky	Fastovsky	k1gFnSc1	Fastovsky
DE	DE	k?	DE
<g/>
,	,	kIx,	,
Weishampel	Weishampel	k1gMnSc1	Weishampel
DB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Evolution	Evolution	k1gInSc1	Evolution
and	and	k?	and
Extinction	Extinction	k1gInSc1	Extinction
of	of	k?	of
the	the	k?	the
Dinosaurs	Dinosaurs	k1gInSc1	Dinosaurs
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
nd	nd	k?	nd
Edition	Edition	k1gInSc1	Edition
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
pp	pp	k?	pp
<g/>
.	.	kIx.	.
229	[number]	k4	229
<g/>
-	-	kIx~	-
<g/>
264	[number]	k4	264
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
81172	[number]	k4	81172
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cetiosaurus	Cetiosaurus	k1gInSc1	Cetiosaurus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
