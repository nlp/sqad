<s>
Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	k1gMnSc1
</s>
<s>
Llanfairpwllgwyngyll	Llanfairpwllgwyngylnout	k5eAaPmAgInS
Štít	štít	k1gInSc1
železniční	železniční	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
s	s	k7c7
přibližným	přibližný	k2eAgInSc7d1
anglickým	anglický	k2eAgInSc7d1
přepisem	přepis	k1gInSc7
výslovnostiPoloha	výslovnostiPoloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
53	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
12	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
země	zem	k1gFnSc2
</s>
<s>
Wales	Wales	k1gInSc1
hrabství	hrabství	k1gNnSc2
</s>
<s>
Gwynedd	Gwynedd	k6eAd1
</s>
<s>
Llanfairpwllgwyngyll	Llanfairpwllgwyngyll	k1gMnSc1
</s>
<s>
Llanfairpwllgwyngyll	Llanfairpwllgwyngyll	k1gInSc1
<g/>
,	,	kIx,
Wales	Wales	k1gInSc1
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
3	#num#	k4
107	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	k1gInSc1
(	(	kIx(
<g/>
ɬ	ɬ	k?
<g/>
̞	̞	k?
<g/>
ɬ	ɬ	k?
poslech	poslech	k1gInSc4
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
Llanfairpwllgwyngyll	Llanfairpwllgwyngyll	k1gMnSc1
<g/>
,	,	kIx,
Llanfair	Llanfair	k1gMnSc1
Pwllgwyngyll	Pwllgwyngyll	k1gMnSc1
<g/>
,	,	kIx,
Llanfair	Llanfair	k1gMnSc1
PG	PG	kA
nebo	nebo	k8xC
Llanfairpwll	Llanfairpwll	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
obec	obec	k1gFnSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Anglesey	Anglesea	k1gFnSc2
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
v	v	k7c6
podstatě	podstata	k1gFnSc6
jen	jen	k9
kvůli	kvůli	k7c3
svému	svůj	k3xOyFgNnSc3
dlouhému	dlouhý	k2eAgNnSc3d1
jménu	jméno	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
tohoto	tento	k3xDgInSc2
velšského	velšský	k2eAgInSc2d1
názvu	název	k1gInSc2
zní	znět	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
„	„	k?
<g/>
Kostel	kostel	k1gInSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Llanfair	Llanfair	k1gMnSc1
<g/>
)	)	kIx)
v	v	k7c6
roklině	roklina	k1gFnSc6
(	(	kIx(
<g/>
pwll	pwlla	k1gFnPc2
<g/>
)	)	kIx)
bílých	bílý	k2eAgFnPc2d1
lísek	líska	k1gFnPc2
(	(	kIx(
<g/>
gwyn	gwyn	k1gMnSc1
gyll	gyll	k1gMnSc1
<g/>
)	)	kIx)
poblíž	poblíž	k6eAd1
(	(	kIx(
<g/>
ger	ger	k?
<g/>
)	)	kIx)
prudkého	prudký	k2eAgInSc2d1
víru	vír	k1gInSc2
(	(	kIx(
<g/>
chwyrn	chwyrn	k1gMnSc1
drobwll	drobwll	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Tysilia	Tysilius	k1gMnSc2
(	(	kIx(
<g/>
llantysilio	llantysilio	k6eAd1
<g/>
)	)	kIx)
u	u	k7c2
červené	červený	k2eAgFnSc2d1
jeskyně	jeskyně	k1gFnSc2
(	(	kIx(
<g/>
ogo	ogo	k?
goch	goch	k1gInSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
anglicky	anglicky	k6eAd1
St.	st.	kA
Mary	Mary	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Church	Church	k1gInSc1
in	in	k?
the	the	k?
hollow	hollow	k?
of	of	k?
the	the	k?
white	white	k5eAaPmIp2nP
hazel	hazel	k1gInSc4
near	near	k1gInSc1
the	the	k?
rapid	rapid	k1gInSc1
whirlpool	whirlpool	k1gInSc1
and	and	k?
the	the	k?
church	church	k1gInSc1
of	of	k?
St.	st.	kA
T.	T.	kA
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
listenlist	listenlist	k1gInSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
url	url	k?
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
//	//	k?
<g/>
upload	upload	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikimedia	wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
/	/	kIx~
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
commons	commons	k1gInSc1
<g/>
/	/	kIx~
<g/>
thumb	thumb	k1gInSc1
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
94	#num#	k4
<g/>
/	/	kIx~
<g/>
Gnome-speakernotes	Gnome-speakernotesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
px-Gnome-speakernotes	px-Gnome-speakernotesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
.	.	kIx.
<g/>
png	png	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
padding-left	padding-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
min-height	min-height	k1gInSc1
<g/>
:	:	kIx,
<g/>
50	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
margin	margin	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
background-position	background-position	k1gInSc4
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
left	left	k1gInSc1
<g/>
;	;	kIx,
<g/>
background-repeat	background-repeat	k1gInSc1
<g/>
:	:	kIx,
<g/>
no-repeat	no-repeat	k5eAaBmF,k5eAaImF,k5eAaPmF
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
list-style-type	list-style-typ	k1gInSc5
<g/>
:	:	kIx,
<g/>
none	none	k1gNnPc6
<g/>
;	;	kIx,
<g/>
list-style-image	list-style-image	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
margin	margin	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
li	li	k8xS
<g/>
{	{	kIx(
<g/>
padding-bottom	padding-bottom	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.5	0.5	k4
<g/>
em	em	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
li	li	k8xS
li	li	k8xS
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
91	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
padding-bottom	padding-bottom	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1
názvu	název	k1gInSc2
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1
názvu	název	k1gInSc2
obce	obec	k1gFnSc2
s	s	k7c7
přízvukem	přízvuk	k1gInSc7
z	z	k7c2
jižního	jižní	k2eAgInSc2d1
Walesu	Wales	k1gInSc2
</s>
<s>
Problémy	problém	k1gInPc1
s	s	k7c7
přehráváním	přehrávání	k1gNnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nápověda	nápověda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
název	název	k1gInSc4
velice	velice	k6eAd1
vyhledávanou	vyhledávaný	k2eAgFnSc7d1
turistickou	turistický	k2eAgFnSc7d1
atrakcí	atrakce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
především	především	k6eAd1
přijíždějí	přijíždět	k5eAaImIp3nP
fotografovat	fotografovat	k5eAaImF
pod	pod	k7c7
štítem	štít	k1gInSc7
železniční	železniční	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
(	(	kIx(
<g/>
na	na	k7c6
jízdenkách	jízdenka	k1gFnPc6
a	a	k8xC
v	v	k7c6
jízdních	jízdní	k2eAgInPc6d1
řádech	řád	k1gInPc6
zkracované	zkracovaný	k2eAgFnSc2d1
na	na	k7c4
Llanfair	Llanfair	k1gInSc4
PG	PG	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	k1gInSc1
bývá	bývat	k5eAaImIp3nS
uváděno	uváděn	k2eAgNnSc4d1
jako	jako	k8xC,k8xS
nejdelší	dlouhý	k2eAgNnSc4d3
jméno	jméno	k1gNnSc4
obce	obec	k1gFnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
potažmo	potažmo	k6eAd1
jako	jako	k8xS,k8xC
nejdelší	dlouhý	k2eAgNnSc4d3
jméno	jméno	k1gNnSc4
železniční	železniční	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
zapsáno	zapsat	k5eAaPmNgNnS
v	v	k7c6
Guinessově	Guinessův	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
rekordů	rekord	k1gInPc2
2002	#num#	k4
jako	jako	k8xC,k8xS
nejdelší	dlouhý	k2eAgNnSc4d3
platné	platný	k2eAgNnSc4d1
doménové	doménový	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
historie	historie	k1gFnSc2
snažilo	snažit	k5eAaImAgNnS
několik	několik	k4yIc1
dalších	další	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
toto	tento	k3xDgNnSc4
prvenství	prvenství	k1gNnSc4
převzít	převzít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
pokusy	pokus	k1gInPc4
neoficiální	neoficiální	k2eAgInPc4d1,k2eNgInPc4d1
<g/>
,	,	kIx,
spíše	spíše	k9
až	až	k9
recesistické	recesistický	k2eAgFnSc2d1
<g/>
,	,	kIx,
nebyly	být	k5eNaImAgInP
ani	ani	k8xC
uznány	uznán	k2eAgInPc1d1
oficiálními	oficiální	k2eAgFnPc7d1
autoritami	autorita	k1gFnPc7
a	a	k8xC
ani	ani	k8xC
nevešly	vejít	k5eNaPmAgFnP
v	v	k7c4
přílišnou	přílišný	k2eAgFnSc4d1
obecnou	obecný	k2eAgFnSc4d1
známost	známost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Osídlení	osídlení	k1gNnSc1
v	v	k7c6
místě	místo	k1gNnSc6
současné	současný	k2eAgFnSc2d1
obce	obec	k1gFnSc2
existovalo	existovat	k5eAaImAgNnS
už	už	k6eAd1
v	v	k7c6
neolitu	neolit	k1gInSc6
a	a	k8xC
souviselo	souviset	k5eAaImAgNnS
se	s	k7c7
zemědělstvím	zemědělství	k1gNnSc7
a	a	k8xC
rybařením	rybařením	k?
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
rané	raný	k2eAgFnSc2d1
existence	existence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
Anglesey	Anglesea	k1gFnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
dosažitelný	dosažitelný	k2eAgMnSc1d1
pouze	pouze	k6eAd1
loďmi	loď	k1gFnPc7
přes	přes	k7c4
úžinu	úžina	k1gFnSc4
Menai	Mena	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
byla	být	k5eAaImAgFnS
krátce	krátce	k6eAd1
obsazena	obsadit	k5eAaPmNgFnS
Římany	Říman	k1gMnPc7
pod	pod	k7c7
velením	velení	k1gNnSc7
Gaia	Gaia	k1gFnSc1
Suetonia	Suetonium	k1gNnSc2
Paulina	Paulin	k2eAgFnSc1d1
<g/>
,	,	kIx,
poté	poté	k6eAd1
jimi	on	k3xPp3gFnPc7
dočasně	dočasně	k6eAd1
opuštěna	opuštěn	k2eAgFnSc1d1
kvůli	kvůli	k7c3
konsolidaci	konsolidace	k1gFnSc3
římských	římský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
Boudice	Boudice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byla	být	k5eAaImAgNnP
držena	držet	k5eAaImNgNnP
až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
římského	římský	k2eAgNnSc2d1
působení	působení	k1gNnSc2
v	v	k7c6
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
stažení	stažení	k1gNnSc6
římských	římský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
spadla	spadnout	k5eAaPmAgFnS
oblast	oblast	k1gFnSc1
pod	pod	k7c4
kontrolu	kontrola	k1gFnSc4
raně	raně	k6eAd1
středověkého	středověký	k2eAgNnSc2d1
království	království	k1gNnSc2
Gwynedd	Gwynedda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
feudálním	feudální	k2eAgInSc6d1
systému	systém	k1gInSc6
obyvatelé	obyvatel	k1gMnPc1
pracovali	pracovat	k5eAaImAgMnP
na	na	k7c6
malých	malý	k2eAgInPc6d1
královských	královský	k2eAgInPc6d1
statcích	statek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Venkovský	venkovský	k2eAgInSc1d1
charakter	charakter	k1gInSc1
vesnice	vesnice	k1gFnSc2
byl	být	k5eAaImAgInS
důvodem	důvod	k1gInSc7
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
měla	mít	k5eAaImAgNnP
pouze	pouze	k6eAd1
kolem	kolem	k7c2
osmdesáti	osmdesát	k4xCc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Se	s	k7c7
zavedením	zavedení	k1gNnSc7
panství	panství	k1gNnSc2
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
oblasti	oblast	k1gFnSc2
absorbována	absorbovat	k5eAaBmNgFnS
do	do	k7c2
hrabství	hrabství	k1gNnSc2
Uxbridge	Uxbridg	k1gFnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
pod	pod	k7c4
Markýze	markýz	k1gMnSc4
z	z	k7c2
Anglesey	Anglesea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelstvo	obyvatelstvo	k1gNnSc1
nájemně	nájemně	k6eAd1
pracovalo	pracovat	k5eAaImAgNnS
na	na	k7c6
jeho	jeho	k3xOp3gInPc6
pozemcích	pozemek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populace	populace	k1gFnSc1
vesnice	vesnice	k1gFnSc2
rostla	růst	k5eAaImAgFnS
–	–	k?
při	při	k7c6
sčítání	sčítání	k1gNnSc6
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
1801	#num#	k4
jich	on	k3xPp3gMnPc2
bylo	být	k5eAaImAgNnS
385	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1826	#num#	k4
byl	být	k5eAaImAgInS
Anglesey	Anglesea	k1gFnSc2
připojen	připojen	k2eAgInSc1d1
ke	k	k7c3
zbytku	zbytek	k1gInSc3
Walesu	Wales	k1gInSc2
stavbou	stavba	k1gFnSc7
visutého	visutý	k2eAgInSc2d1
mostu	most	k1gInSc2
Menai	Mena	k1gFnSc2
Suspension	Suspension	k1gInSc1
Bridge	Bridg	k1gFnSc2
Thomasem	Thomas	k1gMnSc7
Telfordem	Telford	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1850	#num#	k4
byl	být	k5eAaImAgInS
spojen	spojit	k5eAaPmNgInS
s	s	k7c7
Londýnem	Londýn	k1gInSc7
stavbou	stavba	k1gFnSc7
mostu	most	k1gInSc2
Britannia	Britannium	k1gNnSc2
Bridge	Bridg	k1gFnSc2
a	a	k8xC
rušnou	rušný	k2eAgFnSc7d1
železniční	železniční	k2eAgFnSc7d1
pobřežní	pobřežní	k2eAgFnSc7d1
linkou	linka	k1gFnSc7
severního	severní	k2eAgInSc2d1
Walesu	Wales	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
spojovala	spojovat	k5eAaImAgFnS
Londýn	Londýn	k1gInSc4
s	s	k7c7
přístavem	přístav	k1gInSc7
trajektů	trajekt	k1gInPc2
v	v	k7c6
Holyheadu	Holyhead	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Decentralizovaná	decentralizovaný	k2eAgFnSc1d1
obec	obec	k1gFnSc1
byla	být	k5eAaImAgFnS
železnicí	železnice	k1gFnSc7
rozdělena	rozdělen	k2eAgFnSc1d1
na	na	k7c6
Horní	horní	k2eAgFnSc6d1
vesnici	vesnice	k1gFnSc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Upper	Upper	k1gInSc1
Village	Villag	k1gFnSc2
<g/>
,	,	kIx,
velšsky	velšsky	k6eAd1
Pentre	Pentr	k1gInSc5
Uchaf	Uchaf	k1gInSc4
<g/>
)	)	kIx)
složenou	složený	k2eAgFnSc4d1
především	především	k6eAd1
z	z	k7c2
původních	původní	k2eAgFnPc2d1
farem	farma	k1gFnPc2
a	a	k8xC
domů	dům	k1gInPc2
a	a	k8xC
novou	nový	k2eAgFnSc4d1
Dolní	dolní	k2eAgFnSc4d1
vesnici	vesnice	k1gFnSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Lower	Lower	k1gInSc1
Village	Villag	k1gFnSc2
<g/>
,	,	kIx,
velšsky	velšsky	k6eAd1
Pentre	Pentr	k1gInSc5
Isaf	Isaf	k1gInSc4
<g/>
)	)	kIx)
postavenou	postavený	k2eAgFnSc4d1
kolem	kolem	k7c2
železniční	železniční	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
a	a	k8xC
sestávající	sestávající	k2eAgMnSc1d1
především	především	k6eAd1
z	z	k7c2
obchodů	obchod	k1gInPc2
a	a	k8xC
dílen	dílna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obec	obec	k1gFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
centrem	centrum	k1gNnSc7
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
železnice	železnice	k1gFnSc1
přivážela	přivážet	k5eAaImAgFnS
obchodníky	obchodník	k1gMnPc7
a	a	k8xC
zákazníky	zákazník	k1gMnPc7
z	z	k7c2
celého	celý	k2eAgInSc2d1
severního	severní	k2eAgInSc2d1
Walesu	Wales	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vůbec	vůbec	k9
první	první	k4xOgNnSc4
britské	britský	k2eAgNnSc4d1
setkání	setkání	k1gNnSc4
Ženského	ženský	k2eAgInSc2d1
institutu	institut	k1gInSc2
se	se	k3xPyFc4
konalo	konat	k5eAaImAgNnS
v	v	k7c6
Llanfairpwllu	Llanfairpwll	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
a	a	k8xC
hnutí	hnutí	k1gNnSc4
(	(	kIx(
<g/>
původem	původ	k1gInSc7
z	z	k7c2
Kanady	Kanada	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
odtud	odtud	k6eAd1
rozšířilo	rozšířit	k5eAaPmAgNnS
na	na	k7c4
zbytek	zbytek	k1gInSc4
Britských	britský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Llanfairpwllgwyngyll	Llanfairpwllgwyngylla	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
KYNČL	Kynčl	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naučte	naučit	k5eAaPmRp2nP
se	se	k3xPyFc4
vyslovit	vyslovit	k5eAaPmF
nejkrkolomnější	krkolomný	k2eAgInSc4d3
název	název	k1gInSc4
vesnice	vesnice	k1gFnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-01-31	2013-01-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Gorsafawddacha	Gorsafawddacha	k1gFnSc1
<g/>
'	'	kIx"
<g/>
idraigodanheddogleddollônpenrhynareurdraethceredigion	idraigodanheddogleddollônpenrhynareurdraethceredigion	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch	k1gMnSc1
-	-	kIx~
anglicky	anglicky	k6eAd1
</s>
<s>
Článek	článek	k1gInSc1
na	na	k7c4
H2G2	H2G2	k1gMnSc4
(	(	kIx(
<g/>
průvodce	průvodce	k1gMnSc4
BBC	BBC	kA
<g/>
)	)	kIx)
</s>
<s>
Fotografie	fotografia	k1gFnPc1
vesnice	vesnice	k1gFnSc2
na	na	k7c6
Flickru	Flickr	k1gInSc6
</s>
<s>
Llanfairpwll	Llanfairpwll	k1gInSc1
<g/>
...	...	k?
v	v	k7c6
předpovědi	předpověď	k1gFnSc6
počasí	počasí	k1gNnSc2
na	na	k7c4
youtube	youtubat	k5eAaPmIp3nS
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2006051599	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
125856505	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2006051599	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
