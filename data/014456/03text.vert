<s>
Arnold	Arnold	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
</s>
<s>
Arnold	Arnold	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Arnold	Arnold	k1gMnSc1
Johannes	Johannes	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1868	#num#	k4
<g/>
Královec	Královec	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1951	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
82	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Mnichov	Mnichov	k1gInSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
silniční	silniční	k2eAgFnSc1d1
dopravní	dopravní	k2eAgFnSc1d1
nehoda	nehoda	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Nordfriedhof	Nordfriedhof	k1gInSc1
(	(	kIx(
<g/>
48	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
11	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
<g/>
11	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Královecká	Královecký	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
teoretický	teoretický	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
<g/>
,	,	kIx,
matematik	matematik	k1gMnSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Göttingenu	Göttingen	k1gInSc6
(	(	kIx(
<g/>
1895	#num#	k4
<g/>
–	–	k?
<g/>
1897	#num#	k4
<g/>
)	)	kIx)
<g/>
Clausthal	Clausthal	k1gMnSc1
University	universita	k1gFnSc2
of	of	k?
Technology	technolog	k1gMnPc7
(	(	kIx(
<g/>
1897	#num#	k4
<g/>
–	–	k?
<g/>
1900	#num#	k4
<g/>
)	)	kIx)
<g/>
RWTH	RWTH	kA
Aachen	Aachen	k1gInSc1
(	(	kIx(
<g/>
1900	#num#	k4
<g/>
–	–	k?
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
<g/>
Mnichovská	mnichovský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
–	–	k?
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Matteucciho	Matteuccize	k6eAd1
medaile	medaile	k1gFnSc1
(	(	kIx(
<g/>
1924	#num#	k4
<g/>
)	)	kIx)
<g/>
medaile	medaile	k1gFnSc2
Maxe	Max	k1gMnSc2
Plancka	Plancko	k1gNnSc2
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
Maxmiliánův	Maxmiliánův	k2eAgInSc1d1
řád	řád	k1gInSc1
pro	pro	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
umění	umění	k1gNnSc4
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
Lorentzova	Lorentzův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
<g/>
Oerstedova	Oerstedův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
(	(	kIx(
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gMnPc6
Funkce	funkce	k1gFnPc4
</s>
<s>
tajný	tajný	k2eAgMnSc1d1
rada	rada	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Arnold	Arnold	k1gMnSc1
Johannes	Johannes	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1868	#num#	k4
<g/>
,	,	kIx,
Královec	Královec	k1gInSc1
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1951	#num#	k4
<g/>
,	,	kIx,
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
teoretický	teoretický	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
<g/>
,	,	kIx,
průkopník	průkopník	k1gMnSc1
rozvoje	rozvoj	k1gInSc2
atomové	atomový	k2eAgFnSc2d1
a	a	k8xC
kvantové	kvantový	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
také	také	k9
vychoval	vychovat	k5eAaPmAgMnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
studentů	student	k1gMnPc2
pro	pro	k7c4
další	další	k2eAgFnSc4d1
éru	éra	k1gFnSc4
teoretické	teoretický	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kvantové	kvantový	k2eAgFnSc6d1
mechanice	mechanika	k1gFnSc6
je	být	k5eAaImIp3nS
důležitý	důležitý	k2eAgInSc4d1
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
objev	objev	k1gInSc4
konstanty	konstanta	k1gFnSc2
jemné	jemný	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
<g/>
.	.	kIx.
84	#num#	k4
<g/>
krát	krát	k6eAd1
byl	být	k5eAaImAgMnS
nominován	nominovat	k5eAaBmNgMnS
na	na	k7c4
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nikdy	nikdy	k6eAd1
ji	on	k3xPp3gFnSc4
nezískal	získat	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Arnold	Arnold	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
rodině	rodina	k1gFnSc6
lékaře	lékař	k1gMnSc2
v	v	k7c6
Královci	Královec	k1gInSc6
(	(	kIx(
<g/>
něm.	něm.	k?
Königsberg	Königsberg	k1gMnSc1
,	,	kIx,
rus	rus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kaliningrad	Kaliningrad	k1gInSc1
<g/>
)	)	kIx)
ve	v	k7c6
východním	východní	k2eAgNnSc6d1
Prusku	Prusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1886	#num#	k4
<g/>
–	–	k?
<g/>
1890	#num#	k4
studoval	studovat	k5eAaImAgMnS
matematiku	matematika	k1gFnSc4
na	na	k7c4
tamější	tamější	k2eAgInSc4d1
Albertus-Universität	Albertus-Universität	k1gInSc4
<g/>
,	,	kIx,
jednom	jeden	k4xCgNnSc6
z	z	k7c2
nejvýznamnějších	významný	k2eAgNnPc2d3
vědeckých	vědecký	k2eAgNnPc2d1
center	centrum	k1gNnPc2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
disertační	disertační	k2eAgFnSc6d1
práci	práce	k1gFnSc6
se	se	k3xPyFc4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Ferdinanda	Ferdinand	k1gMnSc2
von	von	k1gInSc1
Lindemanna	Lindemann	k1gMnSc4
věnoval	věnovat	k5eAaPmAgInS,k5eAaImAgInS
rovnicím	rovnice	k1gFnPc3
matematické	matematický	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
složil	složit	k5eAaPmAgMnS
zkoušky	zkouška	k1gFnSc2
k	k	k7c3
vyučování	vyučování	k1gNnSc3
matematiky	matematika	k1gFnSc2
na	na	k7c6
středních	střední	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
nastoupil	nastoupit	k5eAaPmAgMnS
roční	roční	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
vojenské	vojenský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
nastoupil	nastoupit	k5eAaPmAgMnS
na	na	k7c4
univerzitu	univerzita	k1gFnSc4
v	v	k7c6
Göttingenu	Göttingen	k1gInSc6
<g/>
,	,	kIx,
centrum	centrum	k1gNnSc1
matematické	matematický	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1894	#num#	k4
asistentem	asistent	k1gMnSc7
Felixe	Felix	k1gMnSc2
Kleina	Klein	k1gMnSc2
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
spolupracoval	spolupracovat	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1910	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
vedením	vedení	k1gNnSc7
tohoto	tento	k3xDgMnSc2
proslulého	proslulý	k2eAgMnSc2d1
matematika	matematik	k1gMnSc2
habilitoval	habilitovat	k5eAaBmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1895	#num#	k4
s	s	k7c7
prací	práce	k1gFnSc7
o	o	k7c6
matematické	matematický	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
difrakce	difrakce	k1gFnSc2
(	(	kIx(
<g/>
Mathematische	Mathematische	k1gInSc1
Theorie	theorie	k1gFnSc2
der	drát	k5eAaImRp2nS
Diffraction	Diffraction	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
napsali	napsat	k5eAaPmAgMnP,k5eAaBmAgMnP
čtyřdílnou	čtyřdílný	k2eAgFnSc4d1
publikaci	publikace	k1gFnSc4
o	o	k7c6
teorii	teorie	k1gFnSc6
setrvačníků	setrvačník	k1gInPc2
(	(	kIx(
<g/>
Theorie	theorie	k1gFnSc1
des	des	k1gNnSc2
Kreisels	Kreiselsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1897	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
profesorem	profesor	k1gMnSc7
matematiky	matematika	k1gFnSc2
na	na	k7c6
Báňské	báňský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Clausthalu	Clausthal	k1gInSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mu	on	k3xPp3gMnSc3
přineslo	přinést	k5eAaPmAgNnS
existenční	existenční	k2eAgNnSc4d1
zajištění	zajištění	k1gNnSc4
a	a	k8xC
umožnilo	umožnit	k5eAaPmAgNnS
sňatek	sňatek	k1gInSc4
s	s	k7c7
Johannou	Johanný	k2eAgFnSc7d1
Höpfnerovou	Höpfnerová	k1gFnSc7
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
kurátora	kurátor	k1gMnSc2
göttingenské	göttingenský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
manželství	manželství	k1gNnPc2
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
narodili	narodit	k5eAaPmAgMnP
tři	tři	k4xCgMnPc1
synové	syn	k1gMnPc1
a	a	k8xC
jedna	jeden	k4xCgFnSc1
dcera	dcera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1900	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
mimořádný	mimořádný	k2eAgMnSc1d1
profesor	profesor	k1gMnSc1
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
technické	technický	k2eAgFnSc2d1
v	v	k7c6
Cáchách	Cáchy	k1gFnPc6
(	(	kIx(
<g/>
později	pozdě	k6eAd2
RWTH	RWTH	kA
Aachen	Aachen	k1gInSc4
University	universita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1906	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
profesorem	profesor	k1gMnSc7
na	na	k7c4
Ludwig-Maximilians-Universität	Ludwig-Maximilians-Universität	k1gInSc4
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
života	život	k1gInSc2
přednášel	přednášet	k5eAaImAgMnS
teoretickou	teoretický	k2eAgFnSc4d1
fyziku	fyzika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedl	vést	k5eAaImAgMnS
desítky	desítka	k1gFnPc4
doktorských	doktorský	k2eAgFnPc2d1
disertací	disertace	k1gFnPc2
<g/>
,	,	kIx,
výsledky	výsledek	k1gInPc1
své	svůj	k3xOyFgFnSc2
vědecké	vědecký	k2eAgFnSc2d1
práce	práce	k1gFnSc2
obohatil	obohatit	k5eAaPmAgMnS
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
fyzikálních	fyzikální	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1942-51	1942-51	k4
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
kompletaci	kompletace	k1gFnSc6
svých	svůj	k3xOyFgFnPc2
přednášek	přednáška	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
zveřejněny	zveřejnit	k5eAaPmNgFnP
v	v	k7c6
šestisvazkovém	šestisvazkový	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
Přednášky	přednáška	k1gFnSc2
z	z	k7c2
teoretické	teoretický	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
(	(	kIx(
<g/>
Vorlesungen	Vorlesungen	k1gInSc1
über	übera	k1gFnPc2
theoretische	theoretisch	k1gInSc2
Physik	Physika	k1gFnPc2
<g/>
)	)	kIx)
Kromě	kromě	k7c2
přednáškového	přednáškový	k2eAgInSc2d1
pobytu	pobyt	k1gInSc2
v	v	k7c6
USA	USA	kA
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
cesty	cesta	k1gFnPc4
do	do	k7c2
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc2
a	a	k8xC
Japonska	Japonsko	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
žil	žít	k5eAaImAgMnS
Arnold	Arnold	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
stále	stále	k6eAd1
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1951	#num#	k4
na	na	k7c4
následky	následek	k1gInPc4
zranění	zranění	k1gNnPc2
při	při	k7c6
dopravní	dopravní	k2eAgFnSc6d1
nehodě	nehoda	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vědecká	vědecký	k2eAgFnSc1d1
práce	práce	k1gFnSc1
</s>
<s>
Sommerfeld	Sommerfeld	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
své	svůj	k3xOyFgFnSc2
akademické	akademický	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
zabýval	zabývat	k5eAaImAgInS
všemi	všecek	k3xTgInPc7
problémy	problém	k1gInPc7
teoretické	teoretický	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
od	od	k7c2
turbulence	turbulence	k1gFnSc2
kapalin	kapalina	k1gFnPc2
až	až	k9
po	po	k7c6
teorii	teorie	k1gFnSc6
relativity	relativita	k1gFnSc2
<g/>
,	,	kIx,
kvantovou	kvantový	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
a	a	k8xC
fyziku	fyzika	k1gFnSc4
tuhých	tuhý	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1899	#num#	k4
až	až	k8xS
1909	#num#	k4
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
především	především	k6eAd1
teorií	teorie	k1gFnSc7
rentgenového	rentgenový	k2eAgNnSc2d1
záření	záření	k1gNnSc2
a	a	k8xC
dynamikou	dynamika	k1gFnSc7
elektronů	elektron	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazně	výrazně	k6eAd1
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
na	na	k7c6
rozvoji	rozvoj	k1gInSc6
kvantové	kvantový	k2eAgFnSc2d1
mechaniky	mechanika	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
oblasti	oblast	k1gFnSc6
kvantové	kvantový	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
stavby	stavba	k1gFnSc2
atomu	atom	k1gInSc2
(	(	kIx(
<g/>
Bohrova-Sommerfeldova	Bohrova-Sommerfeldův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
práce	práce	k1gFnPc4
byla	být	k5eAaImAgFnS
završena	završit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Sommerfeld	Sommerfeld	k1gMnSc1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
kompendium	kompendium	k1gNnSc4
atomové	atomový	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
s	s	k7c7
názvem	název	k1gInSc7
Stavba	stavba	k1gFnSc1
atomů	atom	k1gInPc2
a	a	k8xC
spektrální	spektrální	k2eAgFnPc1d1
linie	linie	k1gFnPc1
(	(	kIx(
<g/>
Atombau	Atombaa	k1gFnSc4
und	und	k?
Spektrallinien	Spektrallinina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
"	"	kIx"
<g/>
biblí	bible	k1gFnPc2
<g/>
"	"	kIx"
pro	pro	k7c4
novou	nový	k2eAgFnSc4d1
generaci	generace	k1gFnSc4
atomových	atomový	k2eAgMnPc2d1
a	a	k8xC
kvantových	kvantový	k2eAgMnPc2d1
fyziků	fyzik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sommerfeld	Sommerfeld	k1gMnSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgMnS
i	i	k9
Zeemanovým	Zeemanův	k2eAgInSc7d1
jevem	jev	k1gInSc7
<g/>
,	,	kIx,
strukturou	struktura	k1gFnSc7
vodíkového	vodíkový	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
,	,	kIx,
zavedl	zavést	k5eAaPmAgInS
konstantu	konstanta	k1gFnSc4
jemné	jemný	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
a	a	k8xC
druhé	druhý	k4xOgNnSc1
kvantové	kvantový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Položil	položit	k5eAaPmAgMnS
základy	základ	k1gInPc4
kvantové	kvantový	k2eAgFnSc2d1
elektronové	elektronový	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
kovů	kov	k1gInPc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
vypracoval	vypracovat	k5eAaPmAgMnS
teorii	teorie	k1gFnSc4
brzdného	brzdný	k2eAgNnSc2d1
záření	záření	k1gNnSc2
elektronů	elektron	k1gInPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Arnold	Arnold	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
obdržel	obdržet	k5eAaPmAgMnS
mnoho	mnoho	k4c4
čestných	čestný	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
a	a	k8xC
vyznamenání	vyznamenání	k1gNnSc1
akademických	akademický	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
laureátem	laureát	k1gMnSc7
medaile	medaile	k1gFnSc2
Maxe	Max	k1gMnSc2
Plancka	Plancko	k1gNnSc2
<g/>
,	,	kIx,
Lorentzovy	Lorentzův	k2eAgFnPc1d1
medaile	medaile	k1gFnPc1
<g/>
,	,	kIx,
Oerstedovy	Oerstedův	k2eAgFnPc1d1
medaile	medaile	k1gFnPc1
<g/>
,	,	kIx,
členem	člen	k1gInSc7
evropských	evropský	k2eAgInPc2d1
i	i	k8xC
amerických	americký	k2eAgFnPc2d1
vědeckých	vědecký	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
čestným	čestný	k2eAgMnSc7d1
doktorem	doktor	k1gMnSc7
univerzit	univerzita	k1gFnPc2
v	v	k7c6
Rostocku	Rostocko	k1gNnSc6
<g/>
,	,	kIx,
Cáchách	Cáchy	k1gFnPc6
<g/>
,	,	kIx,
Kalkatě	Kalkata	k1gFnSc6
a	a	k8xC
Aténách	Atény	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
úspěchy	úspěch	k1gInPc4
<g/>
,	,	kIx,
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
dosáhl	dosáhnout	k5eAaPmAgInS
v	v	k7c6
kvantové	kvantový	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
atomu	atom	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1917	#num#	k4
až	až	k9
1951	#num#	k4
celkem	celkem	k6eAd1
84	#num#	k4
<g/>
krát	krát	k6eAd1
nominován	nominovat	k5eAaBmNgMnS
na	na	k7c4
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
fyziku	fyzika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nikdy	nikdy	k6eAd1
ji	on	k3xPp3gFnSc4
však	však	k9
nezískal	získat	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Arnold	Arnold	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Arnold	Arnold	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
↑	↑	k?
KRAUS	Kraus	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
;	;	kIx,
FIALA	Fiala	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elementární	elementární	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
pevných	pevný	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
technika-nakladatelství	technika-nakladatelství	k1gNnSc4
ČVUT	ČVUT	kA
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5942	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FOLTA	FOLTA	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
matematiky	matematika	k1gFnSc2
a	a	k8xC
fyziky	fyzika	k1gFnSc2
v	v	k7c6
obrazech	obraz	k1gInPc6
<g/>
,	,	kIx,
šestý	šestý	k4xOgInSc1
soubor	soubor	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Jednota	jednota	k1gFnSc1
československých	československý	k2eAgMnPc2d1
matematiků	matematik	k1gMnPc2
a	a	k8xC
fyziků	fyzik	k1gMnPc2
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Nositelé	nositel	k1gMnPc1
Lorentzovy	Lorentzův	k2eAgFnSc2d1
medaile	medaile	k1gFnSc2
</s>
<s>
Max	Max	k1gMnSc1
Planck	Planck	k1gMnSc1
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wolfgang	Wolfgang	k1gMnSc1
Pauli	Paule	k1gFnSc4
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Peter	Peter	k1gMnSc1
Debye	Deby	k1gMnSc2
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arnold	Arnold	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hendrik	Hendrik	k1gMnSc1
Kramers	Kramers	k1gInSc1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fritz	Fritz	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
London	London	k1gMnSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lars	Lars	k1gInSc1
Onsager	Onsager	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Peierls	Peierls	k1gInSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Freeman	Freeman	k1gMnSc1
Dyson	Dyson	k1gMnSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
•	•	k?
George	Georg	k1gMnSc4
Uhlenbeck	Uhlenbeck	k1gMnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Hasbrouck	Hasbrouck	k1gMnSc1
van	vana	k1gFnPc2
Vleck	Vleck	k1gMnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nicolaas	Nicolaas	k1gInSc1
Bloembergen	Bloembergen	k1gInSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Anatole	Anatola	k1gFnSc3
Abragam	Abragam	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gerardus	Gerardus	k1gInSc1
'	'	kIx"
<g/>
t	t	k?
Hooft	Hooft	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pierre-Gilles	Pierre-Gilles	k1gMnSc1
de	de	k?
Gennes	Gennes	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Poljakov	Poljakov	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Carl	Carl	k1gMnSc1
Wieman	Wieman	k1gMnSc1
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
Allin	Allin	k1gMnSc1
Cornell	Cornell	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Frank	frank	k1gInSc1
Wilczek	Wilczek	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leo	Leo	k1gMnSc1
Kadanoff	Kadanoff	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Edward	Edward	k1gMnSc1
Witten	Witten	k2eAgMnSc1d1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michael	Michael	k1gMnSc1
Berry	Berra	k1gMnSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Juan	Juan	k1gMnSc1
Maldacena	Maldacen	k2eAgFnSc1d1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jx	jx	k?
<g/>
20051221012	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
11861553X	11861553X	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2128	#num#	k4
1727	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
85801588	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
36967197	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
85801588	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fyzika	fyzika	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
