<p>
<s>
Potravinová	potravinový	k2eAgFnSc1d1	potravinová
pyramida	pyramida	k1gFnSc1	pyramida
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc4	způsob
grafického	grafický	k2eAgNnSc2d1	grafické
znázornění	znázornění	k1gNnSc2	znázornění
skupin	skupina	k1gFnPc2	skupina
potravin	potravina	k1gFnPc2	potravina
založený	založený	k2eAgMnSc1d1	založený
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
výživové	výživový	k2eAgFnSc6d1	výživová
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Pyramida	pyramida	k1gFnSc1	pyramida
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
typicky	typicky	k6eAd1	typicky
do	do	k7c2	do
čtyř	čtyři	k4xCgNnPc2	čtyři
pater	patro	k1gNnPc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Zařazení	zařazení	k1gNnSc1	zařazení
potraviny	potravina	k1gFnSc2	potravina
do	do	k7c2	do
patra	patro	k1gNnSc2	patro
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
často	často	k6eAd1	často
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
člověk	člověk	k1gMnSc1	člověk
danou	daný	k2eAgFnSc4d1	daná
potravinu	potravina	k1gFnSc4	potravina
konzumovat	konzumovat	k5eAaBmF	konzumovat
<g/>
:	:	kIx,	:
čím	co	k3yRnSc7	co
vyšší	vysoký	k2eAgNnSc1d2	vyšší
patro	patro	k1gNnSc1	patro
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklad	příklad	k1gInSc1	příklad
pyramidy	pyramida	k1gFnSc2	pyramida
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgNnSc1	první
patro	patro	k1gNnSc1	patro
===	===	k?	===
</s>
</p>
<p>
<s>
Nejníže	nízce	k6eAd3	nízce
položené	položený	k2eAgNnSc1d1	položené
patro	patro	k1gNnSc1	patro
tvoří	tvořit	k5eAaImIp3nS	tvořit
základní	základní	k2eAgFnPc4d1	základní
suroviny	surovina	k1gFnPc4	surovina
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
hlavně	hlavně	k9	hlavně
obiloviny	obilovina	k1gFnPc1	obilovina
<g/>
,	,	kIx,	,
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
těstoviny	těstovina	k1gFnPc1	těstovina
a	a	k8xC	a
rýže	rýže	k1gFnPc1	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
potraviny	potravina	k1gFnPc1	potravina
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
nejlépe	dobře	k6eAd3	dobře
celozrnné	celozrnný	k2eAgNnSc1d1	celozrnné
<g/>
.	.	kIx.	.
</s>
<s>
Obiloviny	obilovina	k1gFnPc1	obilovina
představují	představovat	k5eAaImIp3nP	představovat
základ	základ	k1gInSc4	základ
výživy	výživa	k1gFnSc2	výživa
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
porcí	porce	k1gFnPc2	porce
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
:	:	kIx,	:
3	[number]	k4	3
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhé	druhý	k4xOgNnSc1	druhý
patro	patro	k1gNnSc1	patro
===	===	k?	===
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
patro	patro	k1gNnSc1	patro
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
ovoce	ovoce	k1gNnSc2	ovoce
a	a	k8xC	a
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přednost	přednost	k1gFnSc1	přednost
má	mít	k5eAaImIp3nS	mít
hlavně	hlavně	k9	hlavně
syrová	syrový	k2eAgFnSc1d1	syrová
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
porcí	porce	k1gFnPc2	porce
ovoce	ovoce	k1gNnSc2	ovoce
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
:	:	kIx,	:
2-4	[number]	k4	2-4
porce	porce	k1gFnSc2	porce
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
porcí	porce	k1gFnPc2	porce
zeleniny	zelenina	k1gFnSc2	zelenina
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
:	:	kIx,	:
3-5	[number]	k4	3-5
porcí	porce	k1gFnPc2	porce
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
potraviny	potravina	k1gFnPc1	potravina
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
hlavně	hlavně	k9	hlavně
zdroje	zdroj	k1gInPc1	zdroj
ochranných	ochranný	k2eAgFnPc2d1	ochranná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
,	,	kIx,	,
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třetí	třetí	k4xOgNnSc1	třetí
patro	patro	k1gNnSc1	patro
===	===	k?	===
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
patro	patro	k1gNnSc1	patro
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
poloviny	polovina	k1gFnPc4	polovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mléčné	mléčný	k2eAgInPc1d1	mléčný
výrobky	výrobek	k1gInPc1	výrobek
(	(	kIx(	(
<g/>
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
jogurt	jogurt	k1gInSc1	jogurt
<g/>
,	,	kIx,	,
sýry	sýr	k1gInPc1	sýr
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc1	maso
a	a	k8xC	a
drůbež	drůbež	k1gFnSc1	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
potravinách	potravina	k1gFnPc6	potravina
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
podíl	podíl	k1gInSc1	podíl
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
bychom	by	kYmCp1nP	by
měli	mít	k5eAaImAgMnP	mít
nejvíce	hodně	k6eAd3	hodně
jíst	jíst	k5eAaImF	jíst
v	v	k7c6	v
době	doba	k1gFnSc6	doba
růstu	růst	k1gInSc2	růst
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
porcí	porce	k1gFnPc2	porce
mléčných	mléčný	k2eAgInPc2d1	mléčný
výrobků	výrobek	k1gInPc2	výrobek
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
porcí	porce	k1gFnPc2	porce
masových	masový	k2eAgInPc2d1	masový
výrobků	výrobek	k1gInPc2	výrobek
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
patro	patro	k1gNnSc1	patro
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgNnSc6	čtvrtý
patře	patro	k1gNnSc6	patro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejméně	málo	k6eAd3	málo
vhodné	vhodný	k2eAgFnPc4d1	vhodná
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
sůl	sůl	k1gFnSc4	sůl
<g/>
,	,	kIx,	,
cukr	cukr	k1gInSc4	cukr
a	a	k8xC	a
tuk	tuk	k1gInSc4	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
živočišné	živočišný	k2eAgInPc4d1	živočišný
tuky	tuk	k1gInPc4	tuk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
hladinu	hladina	k1gFnSc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
porcí	porce	k1gFnPc2	porce
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
:	:	kIx,	:
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Výživová	výživový	k2eAgNnPc4d1	výživové
doporučení	doporučení	k1gNnPc4	doporučení
</s>
</p>
<p>
<s>
Zdravá	zdravý	k2eAgFnSc1d1	zdravá
13	[number]	k4	13
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
některých	některý	k3yIgFnPc2	některý
potravinových	potravinový	k2eAgFnPc2d1	potravinová
pyramid	pyramida	k1gFnPc2	pyramida
na	na	k7c4	na
Jidelny	Jidelna	k1gFnPc4	Jidelna
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
potravinová	potravinový	k2eAgFnSc1d1	potravinová
pyramida	pyramida	k1gFnSc1	pyramida
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Fóra	fórum	k1gNnSc2	fórum
zdravé	zdravý	k2eAgFnSc2d1	zdravá
výživy	výživa	k1gFnSc2	výživa
</s>
</p>
