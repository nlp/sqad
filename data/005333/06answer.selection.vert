<s>
Korpusová	korpusový	k2eAgFnSc1d1	korpusová
lingvistika	lingvistika	k1gFnSc1	lingvistika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
američtí	americký	k2eAgMnPc1d1	americký
lingvisté	lingvista	k1gMnPc1	lingvista
(	(	kIx(	(
<g/>
Hill	Hill	k1gMnSc1	Hill
a	a	k8xC	a
Harris	Harris	k1gFnSc1	Harris
<g/>
)	)	kIx)	)
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
korpus	korpus	k1gInSc1	korpus
je	být	k5eAaImIp3nS	být
nutným	nutný	k2eAgInSc7d1	nutný
empirickým	empirický	k2eAgInSc7d1	empirický
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
popisu	popis	k1gInSc2	popis
gramatiky	gramatika	k1gFnSc2	gramatika
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
