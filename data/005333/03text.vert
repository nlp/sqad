<s>
Korpusová	korpusový	k2eAgFnSc1d1	korpusová
lingvistika	lingvistika	k1gFnSc1	lingvistika
je	být	k5eAaImIp3nS	být
odvětví	odvětví	k1gNnSc4	odvětví
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
rozvoj	rozvoj	k1gInSc4	rozvoj
je	být	k5eAaImIp3nS	být
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
počítačů	počítač	k1gMnPc2	počítač
a	a	k8xC	a
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
disciplína	disciplína	k1gFnSc1	disciplína
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jazyk	jazyk	k1gInSc4	jazyk
pomocí	pomocí	k7c2	pomocí
elektronických	elektronický	k2eAgInPc2d1	elektronický
jazykových	jazykový	k2eAgInPc2d1	jazykový
korpusů	korpus	k1gInPc2	korpus
a	a	k8xC	a
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
i	i	k9	i
výstavbou	výstavba	k1gFnSc7	výstavba
těchto	tento	k3xDgInPc2	tento
korpusů	korpus	k1gInPc2	korpus
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
zpracováním	zpracování	k1gNnSc7	zpracování
a	a	k8xC	a
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
metodologií	metodologie	k1gFnSc7	metodologie
<g/>
.	.	kIx.	.
</s>
<s>
Korpusová	korpusový	k2eAgFnSc1d1	korpusová
lingvistika	lingvistika	k1gFnSc1	lingvistika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
američtí	americký	k2eAgMnPc1d1	americký
lingvisté	lingvista	k1gMnPc1	lingvista
(	(	kIx(	(
<g/>
Hill	Hill	k1gMnSc1	Hill
a	a	k8xC	a
Harris	Harris	k1gFnSc1	Harris
<g/>
)	)	kIx)	)
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
korpus	korpus	k1gInSc1	korpus
je	být	k5eAaImIp3nS	být
nutným	nutný	k2eAgInSc7d1	nutný
empirickým	empirický	k2eAgInSc7d1	empirický
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
popisu	popis	k1gInSc2	popis
gramatiky	gramatika	k1gFnSc2	gramatika
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
korpusová	korpusový	k2eAgFnSc1d1	korpusová
lingvistika	lingvistika	k1gFnSc1	lingvistika
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
pojetí	pojetí	k1gNnSc6	pojetí
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vznikem	vznik	k1gInSc7	vznik
Survey	Survea	k1gMnSc2	Survea
of	of	k?	of
English	English	k1gInSc1	English
Usage	Usage	k1gFnSc1	Usage
(	(	kIx(	(
<g/>
SEU	SEU	kA	SEU
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
i	i	k9	i
se	s	k7c7	s
zpracováním	zpracování	k1gNnSc7	zpracování
mluveného	mluvený	k2eAgInSc2d1	mluvený
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
orientováno	orientovat	k5eAaBmNgNnS	orientovat
počítačově	počítačově	k6eAd1	počítačově
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
začali	začít	k5eAaPmAgMnP	začít
Čech	Čech	k1gMnSc1	Čech
Henry	Henry	k1gMnSc1	Henry
Kucera	Kucera	k1gFnSc1	Kucera
a	a	k8xC	a
Američan	Američan	k1gMnSc1	Američan
W.	W.	kA	W.
Nelson	Nelson	k1gMnSc1	Nelson
Francis	Francis	k1gFnSc2	Francis
na	na	k7c4	na
Brown	Brown	k1gNnSc4	Brown
University	universita	k1gFnSc2	universita
v	v	k7c6	v
USA	USA	kA	USA
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c4	na
Computation	Computation	k1gInSc4	Computation
Analysis	Analysis	k1gFnSc2	Analysis
of	of	k?	of
Present-Day	Present-Daa	k1gFnSc2	Present-Daa
American	American	k1gMnSc1	American
English	English	k1gMnSc1	English
–	–	k?	–
počítačovém	počítačový	k2eAgInSc6d1	počítačový
korpusu	korpus	k1gInSc6	korpus
současné	současný	k2eAgFnSc2d1	současná
americké	americký	k2eAgFnSc2d1	americká
angličtiny	angličtina	k1gFnSc2	angličtina
obsahující	obsahující	k2eAgMnSc1d1	obsahující
pouze	pouze	k6eAd1	pouze
psané	psaný	k2eAgInPc4d1	psaný
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Prudký	prudký	k2eAgInSc1d1	prudký
rozkvět	rozkvět	k1gInSc1	rozkvět
tohoto	tento	k3xDgNnSc2	tento
odvětví	odvětví	k1gNnSc2	odvětví
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
s	s	k7c7	s
rychlým	rychlý	k2eAgInSc7d1	rychlý
rozvojem	rozvoj	k1gInSc7	rozvoj
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
nejvýrazněji	výrazně	k6eAd3	výrazně
pak	pak	k6eAd1	pak
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvou	dva	k4xCgNnPc6	dva
desetiletích	desetiletí	k1gNnPc6	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
jazycích	jazyk	k1gInPc6	jazyk
korpusů	korpus	k1gInPc2	korpus
již	již	k6eAd1	již
mnoho	mnoho	k4c1	mnoho
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
British	British	k1gInSc1	British
National	National	k1gFnPc2	National
Corpus	corpus	k1gInSc2	corpus
obsahující	obsahující	k2eAgFnSc1d1	obsahující
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
také	také	k9	také
významnou	významný	k2eAgFnSc4d1	významná
složku	složka	k1gFnSc4	složka
mluvenou	mluvený	k2eAgFnSc7d1	mluvená
<g/>
.	.	kIx.	.
</s>
<s>
Korpusová	korpusový	k2eAgFnSc1d1	korpusová
lingvistika	lingvistika	k1gFnSc1	lingvistika
není	být	k5eNaImIp3nS	být
novou	nový	k2eAgFnSc7d1	nová
teorií	teorie	k1gFnSc7	teorie
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
důsledně	důsledně	k6eAd1	důsledně
jazykových	jazykový	k2eAgNnPc2d1	jazykové
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
chápou	chápat	k5eAaImIp3nP	chápat
jako	jako	k9	jako
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
produkty	produkt	k1gInPc4	produkt
jazykového	jazykový	k2eAgInSc2d1	jazykový
systému	systém	k1gInSc2	systém
a	a	k8xC	a
schopností	schopnost	k1gFnPc2	schopnost
jejich	jejich	k3xOp3gMnPc2	jejich
tvůrců	tvůrce	k1gMnPc2	tvůrce
a	a	k8xC	a
skrze	skrze	k?	skrze
ně	on	k3xPp3gNnSc4	on
dospívá	dospívat	k5eAaImIp3nS	dospívat
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
obecnějších	obecní	k2eAgFnPc2d2	obecní
jazykových	jazykový	k2eAgFnPc2d1	jazyková
zákonitostí	zákonitost	k1gFnPc2	zákonitost
a	a	k8xC	a
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
.	.	kIx.	.
</s>
