<s>
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
slavnost	slavnost	k1gFnSc1	slavnost
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
řazena	řadit	k5eAaImNgFnS	řadit
do	do	k7c2	do
žánru	žánr	k1gInSc2	žánr
absurdního	absurdní	k2eAgNnSc2d1	absurdní
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
mladý	mladý	k2eAgMnSc1d1	mladý
přičinlivý	přičinlivý	k2eAgMnSc1d1	přičinlivý
Hugo	Hugo	k1gMnSc1	Hugo
Pludek	Pludek	k1gMnSc1	Pludek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
společenského	společenský	k2eAgInSc2d1	společenský
vzestupu	vzestup	k1gInSc2	vzestup
pomocí	pomocí	k7c2	pomocí
dokonalého	dokonalý	k2eAgNnSc2d1	dokonalé
ovládnutí	ovládnutí	k1gNnSc2	ovládnutí
obsahově	obsahově	k6eAd1	obsahově
vyprázdněných	vyprázdněný	k2eAgFnPc2d1	vyprázdněná
frází	fráze	k1gFnPc2	fráze
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Pludek	Pludek	k1gMnSc1	Pludek
Oldřich	Oldřich	k1gMnSc1	Oldřich
Pludek	Pludek	k1gMnSc1	Pludek
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Huga	Hugo	k1gMnSc2	Hugo
Božena	Božena	k1gFnSc1	Božena
Pludková	Pludkový	k2eAgFnSc1d1	Pludková
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Huga	Hugo	k1gMnSc2	Hugo
Petr	Petr	k1gMnSc1	Petr
Pludek	Pludek	k1gMnSc1	Pludek
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Huga	Hugo	k1gMnSc2	Hugo
Amálka	Amálka	k1gFnSc1	Amálka
<g/>
,	,	kIx,	,
snoubenka	snoubenka	k1gFnSc1	snoubenka
Petra	Petra	k1gFnSc1	Petra
Ferda	Ferda	k1gMnSc1	Ferda
Plzák	Plzák	k1gMnSc1	Plzák
<g/>
,	,	kIx,	,
zahajovač	zahajovač	k1gMnSc1	zahajovač
Tajemnice	tajemnice	k1gFnSc2	tajemnice
Tajemník	tajemník	k1gMnSc1	tajemník
Ředitel	ředitel	k1gMnSc1	ředitel
Hugo	Hugo	k1gMnSc1	Hugo
Pludek	Pludek	k1gMnSc1	Pludek
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
ze	z	k7c2	z
středostavovské	středostavovský	k2eAgFnSc2d1	středostavovská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rodiči	rodič	k1gMnSc3	rodič
vyslán	vyslat	k5eAaPmNgInS	vyslat
na	na	k7c4	na
zahradní	zahradní	k2eAgFnSc4d1	zahradní
slavnost	slavnost	k1gFnSc4	slavnost
Likvidačního	likvidační	k2eAgInSc2d1	likvidační
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
pořádanou	pořádaný	k2eAgFnSc7d1	pořádaná
Zahajovačskou	Zahajovačský	k2eAgFnSc7d1	Zahajovačský
službou	služba	k1gFnSc7	služba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
vlivným	vlivný	k2eAgMnSc7d1	vlivný
Františkem	František	k1gMnSc7	František
Kalabisem	Kalabis	k1gInSc7	Kalabis
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
zde	zde	k6eAd1	zde
Kalabise	Kalabise	k1gFnSc2	Kalabise
nenajde	najít	k5eNaPmIp3nS	najít
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
rozvine	rozvinout	k5eAaPmIp3nS	rozvinout
sled	sled	k1gInSc1	sled
absurdních	absurdní	k2eAgFnPc2d1	absurdní
situací	situace	k1gFnPc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
funkcionáři	funkcionář	k1gMnPc1	funkcionář
Likvidačního	likvidační	k2eAgInSc2d1	likvidační
úřadu	úřad	k1gInSc2	úřad
i	i	k8xC	i
Zahajovačské	Zahajovačský	k2eAgFnSc2d1	Zahajovačský
služby	služba	k1gFnSc2	služba
hovoří	hovořit	k5eAaImIp3nS	hovořit
degenerovaným	degenerovaný	k2eAgMnSc7d1	degenerovaný
<g/>
,	,	kIx,	,
formálním	formální	k2eAgMnSc7d1	formální
a	a	k8xC	a
bezobsažným	bezobsažný	k2eAgInSc7d1	bezobsažný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
od	od	k7c2	od
jejich	jejich	k3xOp3gFnSc2	jejich
role	role	k1gFnSc2	role
v	v	k7c6	v
byrokratickém	byrokratický	k2eAgInSc6d1	byrokratický
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
si	se	k3xPyFc3	se
rychle	rychle	k6eAd1	rychle
osvojí	osvojit	k5eAaPmIp3nP	osvojit
frázi	fráze	k1gFnSc4	fráze
jako	jako	k8xC	jako
univerzální	univerzální	k2eAgFnSc4d1	univerzální
dorozumívací	dorozumívací	k2eAgFnSc4d1	dorozumívací
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
postupuje	postupovat	k5eAaImIp3nS	postupovat
na	na	k7c6	na
společenském	společenský	k2eAgInSc6d1	společenský
žebříčku	žebříček	k1gInSc6	žebříček
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
stane	stanout	k5eAaPmIp3nS	stanout
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nově	nově	k6eAd1	nově
ustavené	ustavený	k2eAgFnSc2d1	ustavená
Ústřední	ústřední	k2eAgFnSc2d1	ústřední
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
zahajování	zahajování	k1gNnSc4	zahajování
a	a	k8xC	a
likvidování	likvidování	k1gNnSc4	likvidování
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
vlastní	vlastní	k2eAgFnSc2d1	vlastní
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Hugo	Hugo	k1gMnSc1	Hugo
přijde	přijít	k5eAaPmIp3nS	přijít
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
změněn	změnit	k5eAaPmNgInS	změnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
vlastní	vlastní	k2eAgMnPc1d1	vlastní
rodiče	rodič	k1gMnPc1	rodič
nepoznávají	poznávat	k5eNaImIp3nP	poznávat
<g/>
.	.	kIx.	.
</s>
<s>
Odposlechnuté	odposlechnutý	k2eAgFnPc1d1	odposlechnutá
dobové	dobový	k2eAgFnPc1d1	dobová
fráze	fráze	k1gFnPc1	fráze
upomínají	upomínat	k5eAaImIp3nP	upomínat
na	na	k7c4	na
Československo	Československo	k1gNnSc4	Československo
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
věty	věta	k1gFnPc4	věta
a	a	k8xC	a
obraty	obrat	k1gInPc4	obrat
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dříve	dříve	k6eAd2	dříve
měly	mít	k5eAaImAgInP	mít
ideový	ideový	k2eAgInSc4d1	ideový
<g/>
,	,	kIx,	,
revoluční	revoluční	k2eAgInSc4d1	revoluční
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
už	už	k6eAd1	už
jen	jen	k9	jen
pro	pro	k7c4	pro
dokazování	dokazování	k1gNnSc4	dokazování
konformity	konformita	k1gFnSc2	konformita
s	s	k7c7	s
vládnoucím	vládnoucí	k2eAgInSc7d1	vládnoucí
režimem	režim	k1gInSc7	režim
(	(	kIx(	(
<g/>
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
to	ten	k3xDgNnSc4	ten
Havel	Havel	k1gMnSc1	Havel
později	pozdě	k6eAd2	pozdě
analyzoval	analyzovat	k5eAaImAgMnS	analyzovat
v	v	k7c6	v
eseji	esej	k1gFnSc6	esej
Moc	moc	k6eAd1	moc
bezmocných	bezmocný	k2eAgMnPc2d1	bezmocný
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyhnutí	vyhnutí	k1gNnSc1	vyhnutí
se	se	k3xPyFc4	se
skutečné	skutečný	k2eAgFnSc2d1	skutečná
zodpovědnosti	zodpovědnost	k1gFnSc2	zodpovědnost
<g/>
.	.	kIx.	.
</s>
<s>
Hru	hra	k1gFnSc4	hra
lze	lze	k6eAd1	lze
ale	ale	k9	ale
aplikovat	aplikovat	k5eAaBmF	aplikovat
i	i	k9	i
obecně	obecně	k6eAd1	obecně
na	na	k7c4	na
tendenci	tendence	k1gFnSc4	tendence
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
mocenské	mocenský	k2eAgFnSc2d1	mocenská
organizace	organizace	k1gFnSc2	organizace
vyvinout	vyvinout	k5eAaPmF	vyvinout
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgInSc4d1	vlastní
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
uzavřít	uzavřít	k5eAaPmF	uzavřít
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Fráze	fráze	k1gFnSc1	fráze
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
'	'	kIx"	'
<g/>
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
'	'	kIx"	'
hry	hra	k1gFnSc2	hra
<g/>
...	...	k?	...
Fráze	fráze	k1gFnSc1	fráze
osnuje	osnovat	k5eAaImIp3nS	osnovat
a	a	k8xC	a
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
zápletku	zápletka	k1gFnSc4	zápletka
<g/>
,	,	kIx,	,
posunuje	posunovat	k5eAaImIp3nS	posunovat
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
a	a	k8xC	a
odtržena	odtrhnout	k5eAaPmNgFnS	odtrhnout
od	od	k7c2	od
jedinečné	jedinečný	k2eAgFnSc2d1	jedinečná
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
a	a	k8xC	a
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
skutečnost	skutečnost	k1gFnSc4	skutečnost
novou	nový	k2eAgFnSc4d1	nová
a	a	k8xC	a
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
Jan	Jan	k1gMnSc1	Jan
Grossman	Grossman	k1gMnSc1	Grossman
<g/>
,	,	kIx,	,
doslov	doslov	k1gInSc1	doslov
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
hry	hra	k1gFnSc2	hra
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
slavnost	slavnost	k1gFnSc1	slavnost
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Orbis	orbis	k1gInSc1	orbis
1964	[number]	k4	1964
Hra	hra	k1gFnSc1	hra
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
slavnost	slavnost	k1gFnSc1	slavnost
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
uvedena	uvést	k5eAaPmNgFnS	uvést
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Pludka	Pludek	k1gMnSc4	Pludek
hrál	hrát	k5eAaImAgMnS	hrát
Václav	Václav	k1gMnSc1	Václav
Sloup	sloup	k1gInSc4	sloup
<g/>
.	.	kIx.	.
</s>
