<s>
Telč	Telč	k1gFnSc1	Telč
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Teltsch	Teltsch	k1gInSc1	Teltsch
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Telcz	Telcz	k1gInSc1	Telcz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jihlava	Jihlava	k1gFnSc1	Jihlava
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
25	[number]	k4	25
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
od	od	k7c2	od
varianty	varianta	k1gFnSc2	varianta
Telcz	Telcz	k1gInSc1	Telcz
(	(	kIx(	(
<g/>
1180	[number]	k4	1180
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Telci	Telce	k1gFnSc4	Telce
(	(	kIx(	(
<g/>
1207	[number]	k4	1207
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Telez	Telez	k1gMnSc1	Telez
(	(	kIx(	(
<g/>
1283	[number]	k4	1283
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Telcz	Telcz	k1gMnSc1	Telcz
(	(	kIx(	(
<g/>
1315	[number]	k4	1315
<g/>
,	,	kIx,	,
1331	[number]	k4	1331
<g/>
,	,	kIx,	,
1339	[number]	k4	1339
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Telsch	Telsch	k1gMnSc1	Telsch
(	(	kIx(	(
<g/>
1356	[number]	k4	1356
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Telcz	Telcz	k1gMnSc1	Telcz
(	(	kIx(	(
<g/>
1367	[number]	k4	1367
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Thelcz	Thelcz	k1gMnSc1	Thelcz
(	(	kIx(	(
<g/>
1392	[number]	k4	1392
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
Telče	Telč	k1gFnSc2	Telč
(	(	kIx(	(
<g/>
1406	[number]	k4	1406
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Telcz	Telcz	k1gMnSc1	Telcz
(	(	kIx(	(
<g/>
1447	[number]	k4	1447
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Telecz	Telecz	k1gMnSc1	Telecz
(	(	kIx(	(
<g/>
1480	[number]	k4	1480
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
Telče	Telč	k1gFnSc2	Telč
(	(	kIx(	(
<g/>
1481	[number]	k4	1481
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Telči	Telč	k1gFnSc6	Telč
(	(	kIx(	(
<g/>
1486	[number]	k4	1486
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nowa	Nowa	k1gMnSc1	Nowa
Telcz	Telcz	k1gMnSc1	Telcz
(	(	kIx(	(
<g/>
1490	[number]	k4	1490
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Telczie	Telczie	k1gFnSc1	Telczie
(	(	kIx(	(
<g/>
1580	[number]	k4	1580
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Teltsch	Teltsch	k1gMnSc1	Teltsch
(	(	kIx(	(
<g/>
1633	[number]	k4	1633
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Teltzsch	Teltzsch	k1gMnSc1	Teltzsch
(	(	kIx(	(
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Teltsch	Teltsch	k1gMnSc1	Teltsch
(	(	kIx(	(
<g/>
1678	[number]	k4	1678
<g/>
,	,	kIx,	,
1718	[number]	k4	1718
<g/>
,	,	kIx,	,
1720	[number]	k4	1720
<g/>
,	,	kIx,	,
1751	[number]	k4	1751
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Teltsch	Teltsch	k1gInSc1	Teltsch
a	a	k8xC	a
Telč	Telč	k1gFnSc1	Telč
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
až	až	k6eAd1	až
k	k	k7c3	k
podobě	podoba	k1gFnSc3	podoba
Telč	Telč	k1gFnSc1	Telč
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1881	[number]	k4	1881
a	a	k8xC	a
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgNnSc1d1	místní
jméno	jméno	k1gNnSc1	jméno
znělo	znět	k5eAaImAgNnS	znět
původně	původně	k6eAd1	původně
Teleč	Teleč	k1gInSc4	Teleč
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přidáním	přidání	k1gNnSc7	přidání
přivlastňovací	přivlastňovací	k2eAgFnSc2d1	přivlastňovací
přípony	přípona	k1gFnSc2	přípona
-jъ	ъ	k?	-jъ
k	k	k7c3	k
osobnímu	osobní	k2eAgNnSc3d1	osobní
jménu	jméno	k1gNnSc3	jméno
Telec	Telec	k1gMnSc1	Telec
(	(	kIx(	(
<g/>
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
mladý	mladý	k2eAgMnSc1d1	mladý
býček	býček	k1gMnSc1	býček
<g/>
)	)	kIx)	)
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
rodu	rod	k1gInSc3	rod
mužského	mužský	k2eAgInSc2d1	mužský
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
Telč	Telč	k1gFnSc1	Telč
je	být	k5eAaImIp3nS	být
rodu	rod	k1gInSc2	rod
ženského	ženský	k2eAgInSc2d1	ženský
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
místní	místní	k2eAgFnSc2d1	místní
pověsti	pověst	k1gFnSc2	pověst
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
založeno	založit	k5eAaPmNgNnS	založit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1099	[number]	k4	1099
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
však	však	k9	však
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k9	až
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1315	[number]	k4	1315
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgNnSc7d1	významné
datem	datum	k1gNnSc7	datum
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc4	rok
1339	[number]	k4	1339
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Telč	Telč	k1gFnSc4	Telč
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
Oldřich	Oldřich	k1gMnSc1	Oldřich
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Vítkovců	Vítkovec	k1gInPc2	Vítkovec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
rod	rod	k1gInSc1	rod
pak	pak	k6eAd1	pak
významně	významně	k6eAd1	významně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
Telče	Telč	k1gFnSc2	Telč
<g/>
.	.	kIx.	.
</s>
<s>
Vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
zdejší	zdejší	k2eAgInSc4d1	zdejší
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
kostel	kostel	k1gInSc4	kostel
<g/>
,	,	kIx,	,
opevnil	opevnit	k5eAaPmAgInS	opevnit
město	město	k1gNnSc4	město
a	a	k8xC	a
postavil	postavit	k5eAaPmAgInS	postavit
též	též	k9	též
gotické	gotický	k2eAgInPc4d1	gotický
domy	dům	k1gInPc4	dům
kolem	kolem	k7c2	kolem
tržiště	tržiště	k1gNnSc2	tržiště
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1386	[number]	k4	1386
zničil	zničit	k5eAaPmAgInS	zničit
požár	požár	k1gInSc1	požár
západní	západní	k2eAgFnSc4d1	západní
polovinu	polovina	k1gFnSc4	polovina
náměstí	náměstí	k1gNnSc2	náměstí
i	i	k9	i
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
a	a	k8xC	a
radnicí	radnice	k1gFnSc7	radnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1423	[number]	k4	1423
dobyli	dobýt	k5eAaPmAgMnP	dobýt
město	město	k1gNnSc4	město
husité	husita	k1gMnPc1	husita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
získával	získávat	k5eAaImAgMnS	získávat
zdejší	zdejší	k2eAgNnSc1d1	zdejší
panství	panství	k1gNnSc1	panství
Zachariáš	Zachariáš	k1gFnSc2	Zachariáš
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
a	a	k8xC	a
Telči	Telč	k1gFnSc6	Telč
nastává	nastávat	k5eAaImIp3nS	nastávat
opět	opět	k6eAd1	opět
doba	doba	k1gFnSc1	doba
rozkvětu	rozkvět	k1gInSc2	rozkvět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
trpělo	trpět	k5eAaImAgNnS	trpět
město	město	k1gNnSc1	město
i	i	k8xC	i
celý	celý	k2eAgInSc1d1	celý
kraj	kraj	k1gInSc1	kraj
zlovůlí	zlovůle	k1gFnPc2	zlovůle
švédských	švédský	k2eAgNnPc2d1	švédské
i	i	k8xC	i
císařských	císařský	k2eAgNnPc2d1	císařské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1712	[number]	k4	1712
zdědil	zdědit	k5eAaPmAgInS	zdědit
zdejší	zdejší	k2eAgNnSc4d1	zdejší
zboží	zboží	k1gNnSc4	zboží
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Lichtenštejn-Kastelkorn	Lichtenštejn-Kastelkorn	k1gMnSc1	Lichtenštejn-Kastelkorn
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
poslední	poslední	k2eAgMnSc1d1	poslední
člen	člen	k1gMnSc1	člen
této	tento	k3xDgFnSc2	tento
rodinné	rodinný	k2eAgFnSc2d1	rodinná
větve	větev	k1gFnSc2	větev
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1761	[number]	k4	1761
<g/>
.	.	kIx.	.
</s>
<s>
Panství	panství	k1gNnSc1	panství
Telč	Telč	k1gFnSc1	Telč
zdědil	zdědit	k5eAaPmAgMnS	zdědit
bratranec	bratranec	k1gMnSc1	bratranec
Alois	Alois	k1gMnSc1	Alois
Arnošt	Arnošt	k1gMnSc1	Arnošt
hrabě	hrabě	k1gMnSc1	hrabě
Podstatský	Podstatský	k2eAgMnSc1d1	Podstatský
z	z	k7c2	z
Prusinovic	Prusinovice	k1gFnPc2	Prusinovice
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
jméno	jméno	k1gNnSc1	jméno
a	a	k8xC	a
erb	erb	k1gInSc1	erb
Lichtenštejnů-Kastelkornů	Lichtenštejnů-Kastelkorn	k1gInPc2	Lichtenštejnů-Kastelkorn
a	a	k8xC	a
Podstatských	Podstatský	k2eAgInPc2d1	Podstatský
z	z	k7c2	z
Prusinovic	Prusinovice	k1gFnPc2	Prusinovice
sloučí	sloučit	k5eAaPmIp3nP	sloučit
jako	jako	k9	jako
Podstatský-Lichtenštejn	Podstatský-Lichtenštejn	k1gInSc4	Podstatský-Lichtenštejn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nastává	nastávat	k5eAaImIp3nS	nastávat
vzestup	vzestup	k1gInSc1	vzestup
měšťanského	měšťanský	k2eAgInSc2d1	měšťanský
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
industrializace	industrializace	k1gFnSc2	industrializace
<g/>
.	.	kIx.	.
</s>
<s>
Postavením	postavení	k1gNnSc7	postavení
železnice	železnice	k1gFnSc2	železnice
z	z	k7c2	z
Jihlavy	Jihlava	k1gFnSc2	Jihlava
do	do	k7c2	do
Telče	Telč	k1gFnSc2	Telč
1898	[number]	k4	1898
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
propojením	propojení	k1gNnSc7	propojení
přes	přes	k7c4	přes
Slavonice	Slavonice	k1gFnPc4	Slavonice
do	do	k7c2	do
rakouského	rakouský	k2eAgInSc2d1	rakouský
Schwarzenau	Schwarzenaus	k1gInSc2	Schwarzenaus
skončila	skončit	k5eAaPmAgFnS	skončit
i	i	k9	i
komunikační	komunikační	k2eAgFnSc1d1	komunikační
izolace	izolace	k1gFnSc1	izolace
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1980	[number]	k4	1980
byly	být	k5eAaImAgFnP	být
vesnice	vesnice	k1gFnPc1	vesnice
Borovná	Borovný	k2eAgFnSc1d1	Borovná
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgFnSc1d1	dolní
Dvorce	dvorec	k1gInPc4	dvorec
<g/>
,	,	kIx,	,
Doupě	doupě	k1gNnSc1	doupě
<g/>
,	,	kIx,	,
Dyjice	Dyjice	k1gFnSc1	Dyjice
<g/>
,	,	kIx,	,
Dyjička	Dyjička	k1gFnSc1	Dyjička
<g/>
,	,	kIx,	,
Stranná	stranný	k2eAgFnSc1d1	Stranná
<g/>
,	,	kIx,	,
Vanov	Vanov	k1gInSc1	Vanov
<g/>
,	,	kIx,	,
Vanůvek	Vanůvka	k1gFnPc2	Vanůvka
<g/>
,	,	kIx,	,
Volevčice	Volevčice	k1gFnPc4	Volevčice
a	a	k8xC	a
Zvolenovice	Zvolenovice	k1gFnPc4	Zvolenovice
připojeny	připojen	k2eAgFnPc4d1	připojena
jako	jako	k8xS	jako
místní	místní	k2eAgFnPc4d1	místní
části	část	k1gFnPc4	část
k	k	k7c3	k
Telči	Telč	k1gFnSc3	Telč
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
Horní	horní	k2eAgFnSc1d1	horní
Myslová	Myslová	k1gFnSc1	Myslová
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1986	[number]	k4	1986
pak	pak	k6eAd1	pak
Radkov	Radkov	k1gInSc1	Radkov
a	a	k8xC	a
Strachoňovice	Strachoňovice	k1gFnSc1	Strachoňovice
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
vlna	vlna	k1gFnSc1	vlna
slučování	slučování	k1gNnSc2	slučování
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
připojeny	připojit	k5eAaPmNgInP	připojit
Mysletice	Mysletika	k1gFnSc6	Mysletika
<g/>
,	,	kIx,	,
Olší	olší	k1gNnSc6	olší
a	a	k8xC	a
Zadní	zadní	k2eAgFnPc4d1	zadní
Vydří	vydří	k2eAgNnPc4d1	vydří
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
vesnice	vesnice	k1gFnPc1	vesnice
se	se	k3xPyFc4	se
ale	ale	k9	ale
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
783	[number]	k4	783
domech	dům	k1gInPc6	dům
4	[number]	k4	4
270	[number]	k4	270
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
4	[number]	k4	4
182	[number]	k4	182
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
29	[number]	k4	29
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
3	[number]	k4	3
924	[number]	k4	924
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
169	[number]	k4	169
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
32	[number]	k4	32
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
78	[number]	k4	78
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
pět	pět	k4xCc4	pět
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
–	–	k?	–
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
opevněné	opevněný	k2eAgNnSc1d1	opevněné
historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Podolí	Podolí	k1gNnSc1	Podolí
(	(	kIx(	(
<g/>
k	k	k7c3	k
centru	centrum	k1gNnSc3	centrum
přilehlé	přilehlý	k2eAgFnSc2d1	přilehlá
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Štěpnice	štěpnice	k1gFnSc1	štěpnice
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
a	a	k8xC	a
Studnice	studnice	k1gFnSc1	studnice
(	(	kIx(	(
<g/>
4	[number]	k4	4
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c4	na
dvou	dva	k4xCgNnPc2	dva
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
–	–	k?	–
Studnice	studnice	k1gFnSc2	studnice
u	u	k7c2	u
Telče	Telč	k1gFnSc2	Telč
a	a	k8xC	a
Telč	Telč	k1gFnSc1	Telč
<g/>
.	.	kIx.	.
</s>
<s>
Telč	Telč	k1gFnSc1	Telč
má	mít	k5eAaImIp3nS	mít
16	[number]	k4	16
základních	základní	k2eAgFnPc2d1	základní
sídelních	sídelní	k2eAgFnPc2d1	sídelní
jednotek	jednotka	k1gFnPc2	jednotka
–	–	k?	–
Studnice	studnice	k1gFnSc2	studnice
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Mokrovcích	Mokrovec	k1gMnPc6	Mokrovec
<g/>
,	,	kIx,	,
Na	na	k7c6	na
myslibořské	myslibořský	k2eAgFnSc6d1	myslibořský
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
Na	na	k7c6	na
myslovské	myslovský	k2eAgFnSc6d1	myslovský
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
Na	na	k7c6	na
novoříšské	novoříšský	k2eAgFnSc6d1	novoříšská
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
Na	na	k7c6	na
radkovské	radkovský	k2eAgFnSc6d1	radkovský
silnici	silnice	k1gFnSc6	silnice
<g/>
,	,	kIx,	,
Na	na	k7c6	na
statku	statek	k1gInSc6	statek
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Žabinci	žabinec	k1gInSc6	žabinec
<g/>
,	,	kIx,	,
Panské	panský	k2eAgFnPc1d1	Panská
nivy	niva	k1gFnPc1	niva
<g/>
,	,	kIx,	,
Podolí	Podolí	k1gNnSc1	Podolí
<g/>
,	,	kIx,	,
Rovné	rovný	k2eAgNnSc1d1	rovné
pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
Štěpnice	štěpnice	k1gFnSc1	štěpnice
<g/>
,	,	kIx,	,
Telč-historické	Telčistorický	k2eAgNnSc1d1	Telč-historický
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
U	u	k7c2	u
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
V	v	k7c6	v
Buzovech	Buzovo	k1gNnPc6	Buzovo
a	a	k8xC	a
Za	za	k7c7	za
Staroměstským	staroměstský	k2eAgInSc7d1	staroměstský
rybníkem	rybník	k1gInSc7	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Telč	Telč	k1gFnSc1	Telč
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
mikroregionů	mikroregion	k1gInPc2	mikroregion
Telčsko	Telčsko	k1gNnSc1	Telčsko
<g/>
,	,	kIx,	,
Dobrovolný	dobrovolný	k2eAgInSc1d1	dobrovolný
svazek	svazek	k1gInSc1	svazek
obcí	obec	k1gFnPc2	obec
Česká	český	k2eAgFnSc1d1	Česká
inspirace	inspirace	k1gFnSc1	inspirace
a	a	k8xC	a
České	český	k2eAgNnSc1d1	české
dědictví	dědictví	k1gNnSc1	dědictví
UNESCO	UNESCO	kA	UNESCO
a	a	k8xC	a
místní	místní	k2eAgFnSc2d1	místní
akční	akční	k2eAgFnSc2d1	akční
skupiny	skupina	k1gFnSc2	skupina
Mikroregionu	mikroregion	k1gInSc2	mikroregion
Telčsko	Telčsko	k1gNnSc1	Telčsko
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
21	[number]	k4	21
<g/>
členné	členný	k2eAgNnSc1d1	členné
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
7	[number]	k4	7
<g/>
členné	členný	k2eAgFnSc2d1	členná
rady	rada	k1gFnSc2	rada
stojí	stát	k5eAaImIp3nS	stát
starosta	starosta	k1gMnSc1	starosta
Roman	Roman	k1gMnSc1	Roman
Fabeš	Fabeš	k1gMnSc1	Fabeš
<g/>
.	.	kIx.	.
</s>
<s>
Právo	právo	k1gNnSc1	právo
užívat	užívat	k5eAaImF	užívat
vlajku	vlajka	k1gFnSc4	vlajka
bylo	být	k5eAaImAgNnS	být
městu	město	k1gNnSc3	město
uděleno	udělen	k2eAgNnSc1d1	uděleno
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
červeném	červený	k2eAgInSc6d1	červený
podkladu	podklad	k1gInSc6	podklad
bílé	bílý	k2eAgNnSc1d1	bílé
písmeno	písmeno	k1gNnSc1	písmeno
"	"	kIx"	"
<g/>
T	T	kA	T
<g/>
"	"	kIx"	"
sahajícím	sahající	k2eAgInSc7d1	sahající
na	na	k7c4	na
okraje	okraj	k1gInPc4	okraj
listu	list	k1gInSc2	list
s	s	k7c7	s
rameny	rameno	k1gNnPc7	rameno
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
šířky	šířka	k1gFnSc2	šířka
listu	list	k1gInSc2	list
praporu	prapor	k1gInSc2	prapor
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
položeno	položit	k5eAaPmNgNnS	položit
na	na	k7c4	na
list	list	k1gInSc4	list
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc4	jeho
kratší	krátký	k2eAgNnSc4d2	kratší
rameno	rameno	k1gNnSc4	rameno
je	být	k5eAaImIp3nS	být
rovnoběžné	rovnoběžný	k2eAgNnSc1d1	rovnoběžné
se	s	k7c7	s
žerdí	žerď	k1gFnSc7	žerď
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
žerďového	žerďový	k2eAgInSc2d1	žerďový
okraje	okraj	k1gInSc2	okraj
vzdáleno	vzdáleno	k1gNnSc1	vzdáleno
na	na	k7c4	na
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
šířky	šířka	k1gFnSc2	šířka
listu	list	k1gInSc2	list
praporu	prapor	k1gInSc2	prapor
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
šířky	šířka	k1gFnSc2	šířka
k	k	k7c3	k
délce	délka	k1gFnSc3	délka
listu	list	k1gInSc2	list
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Telčí	Telč	k1gFnPc2	Telč
sídlí	sídlet	k5eAaImIp3nP	sídlet
firmy	firma	k1gFnPc1	firma
AGROKOP	AGROKOP	kA	AGROKOP
HB	HB	kA	HB
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
EKOPLAST	EKOPLAST	kA	EKOPLAST
ŠTANCL	Štancl	k1gMnSc1	Štancl
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Opravny	opravna	k1gFnPc1	opravna
Telč	Telč	k1gFnSc1	Telč
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
Chesterton	Chesterton	k1gInSc1	Chesterton
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Trojstav	Trojstav	k1gFnSc1	Trojstav
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Terco	terca	k1gFnSc5	terca
CB	CB	kA	CB
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
družstvo	družstvo	k1gNnSc4	družstvo
Telč	Telč	k1gFnSc4	Telč
<g/>
,	,	kIx,	,
Café	café	k1gNnSc4	café
Telč	Telč	k1gFnSc1	Telč
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
LEPŠÍ	dobrý	k2eAgInSc1d2	lepší
ZVUK	zvuk	k1gInSc1	zvuk
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Obcí	obec	k1gFnPc2	obec
prochází	procházet	k5eAaImIp3nS	procházet
silnice	silnice	k1gFnSc1	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
23	[number]	k4	23
z	z	k7c2	z
Mrákotína	Mrákotín	k1gInSc2	Mrákotín
do	do	k7c2	do
Staré	Staré	k2eAgFnSc2d1	Staré
Říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
silnice	silnice	k1gFnPc1	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
112	[number]	k4	112
z	z	k7c2	z
Nové	Nové	k2eAgFnSc2d1	Nové
Říše	říš	k1gFnSc2	říš
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
Horní	horní	k2eAgFnSc2d1	horní
Cerekve	Cerekev	k1gFnSc2	Cerekev
<g/>
,	,	kIx,	,
č.	č.	k?	č.
403	[number]	k4	403
do	do	k7c2	do
Stonařova	Stonařův	k2eAgInSc2d1	Stonařův
a	a	k8xC	a
č.	č.	k?	č.
406	[number]	k4	406
z	z	k7c2	z
Dačic	Dačice	k1gFnPc2	Dačice
do	do	k7c2	do
Třeště	třeštit	k5eAaImSgInS	třeštit
a	a	k8xC	a
komunikace	komunikace	k1gFnPc4	komunikace
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
02321	[number]	k4	02321
do	do	k7c2	do
Mysliboře	Mysliboř	k1gFnSc2	Mysliboř
<g/>
,	,	kIx,	,
č.	č.	k?	č.
40618	[number]	k4	40618
do	do	k7c2	do
Radkova	Radkův	k2eAgInSc2d1	Radkův
<g/>
,	,	kIx,	,
č.	č.	k?	č.
40610	[number]	k4	40610
<g/>
,	,	kIx,	,
č.	č.	k?	č.
40611	[number]	k4	40611
do	do	k7c2	do
Horní	horní	k2eAgFnSc2d1	horní
Myslové	Myslová	k1gFnSc2	Myslová
<g/>
,	,	kIx,	,
č.	č.	k?	č.
40617	[number]	k4	40617
do	do	k7c2	do
Kostelní	kostelní	k2eAgFnSc2d1	kostelní
Myslové	Myslová	k1gFnSc2	Myslová
a	a	k8xC	a
č.	č.	k?	č.
11261	[number]	k4	11261
do	do	k7c2	do
Hostětic	Hostětice	k1gFnPc2	Hostětice
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nS	procházet
tudy	tudy	k6eAd1	tudy
též	též	k9	též
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
č.	č.	k?	č.
240	[number]	k4	240
z	z	k7c2	z
Kostelce	Kostelec	k1gInSc2	Kostelec
do	do	k7c2	do
Slavonic	Slavonice	k1gFnPc2	Slavonice
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgFnSc4d1	dopravní
obslužnost	obslužnost	k1gFnSc4	obslužnost
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
dopravci	dopravce	k1gMnPc1	dopravce
ICOM	ICOM	kA	ICOM
transport	transport	k1gInSc4	transport
<g/>
,	,	kIx,	,
AZ	AZ	kA	AZ
BUS	bus	k1gInSc1	bus
&	&	k?	&
TIR	TIR	kA	TIR
PRAHA	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ČSAD	ČSAD	kA	ČSAD
JIHOTRANS	JIHOTRANS	kA	JIHOTRANS
<g/>
,	,	kIx,	,
ČSAD	ČSAD	kA	ČSAD
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
Tourbus	Tourbus	k1gMnSc1	Tourbus
<g/>
,	,	kIx,	,
TRADO-BUS	TRADO-BUS	k1gMnSc1	TRADO-BUS
<g/>
,	,	kIx,	,
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
Čech	Čech	k1gMnSc1	Čech
-	-	kIx~	-
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
a	a	k8xC	a
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
jezdí	jezdit	k5eAaImIp3nP	jezdit
ve	v	k7c6	v
směrech	směr	k1gInPc6	směr
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
,	,	kIx,	,
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
,	,	kIx,	,
Slavonice	Slavonice	k1gFnSc1	Slavonice
<g/>
,	,	kIx,	,
Jemnice	Jemnice	k1gFnSc1	Jemnice
<g/>
,	,	kIx,	,
Moravské	moravský	k2eAgInPc1d1	moravský
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Znojmo	Znojmo	k1gNnSc1	Znojmo
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
,	,	kIx,	,
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Dačice	Dačice	k1gFnPc1	Dačice
<g/>
,	,	kIx,	,
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
<g/>
,	,	kIx,	,
Třeboň	Třeboň	k1gFnSc1	Třeboň
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
Říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
Budeč	Budeč	k1gFnSc1	Budeč
<g/>
,	,	kIx,	,
Řásná	řásný	k2eAgFnSc1d1	Řásná
<g/>
,	,	kIx,	,
Mrákotín	Mrákotín	k1gInSc1	Mrákotín
<g/>
,	,	kIx,	,
Mysletice	Mysletika	k1gFnSc6	Mysletika
<g/>
,	,	kIx,	,
Borovná	Borovný	k2eAgFnSc1d1	Borovná
<g/>
,	,	kIx,	,
Blažejov	Blažejov	k1gInSc1	Blažejov
<g/>
,	,	kIx,	,
Kunžak	Kunžak	k1gInSc1	Kunžak
<g/>
,	,	kIx,	,
Strmilov	Strmilov	k1gInSc1	Strmilov
<g/>
,	,	kIx,	,
Studená	studený	k2eAgFnSc1d1	studená
<g/>
,	,	kIx,	,
Třešť	třeštit	k5eAaImRp2nS	třeštit
<g/>
,	,	kIx,	,
Želetava	Želetava	k1gFnSc1	Želetava
<g/>
,	,	kIx,	,
Černíč	Černíč	k1gFnSc1	Černíč
<g/>
,	,	kIx,	,
Myslůvka	Myslůvka	k1gFnSc1	Myslůvka
a	a	k8xC	a
Zadní	zadní	k2eAgInPc1d1	zadní
Vydří	vydří	k2eAgInPc1d1	vydří
a	a	k8xC	a
vlaky	vlak	k1gInPc1	vlak
ve	v	k7c6	v
směrech	směr	k1gInPc6	směr
Kostelec	Kostelec	k1gInSc4	Kostelec
a	a	k8xC	a
Slavonice	Slavonice	k1gFnPc4	Slavonice
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
cyklistické	cyklistický	k2eAgFnPc4d1	cyklistická
trasy	trasa	k1gFnPc4	trasa
č.	č.	k?	č.
5091	[number]	k4	5091
do	do	k7c2	do
Mysliboře	Mysliboř	k1gFnSc2	Mysliboř
<g/>
,	,	kIx,	,
č.	č.	k?	č.
5125	[number]	k4	5125
do	do	k7c2	do
Dyjice	Dyjice	k1gFnSc2	Dyjice
<g/>
,	,	kIx,	,
č.	č.	k?	č.
16	[number]	k4	16
a	a	k8xC	a
Greenway	Greenwaa	k1gFnPc1	Greenwaa
ŘV	ŘV	kA	ŘV
z	z	k7c2	z
Třeště	třeštit	k5eAaImSgInS	třeštit
do	do	k7c2	do
Kostelní	kostelní	k2eAgFnSc2d1	kostelní
Myslové	Myslová	k1gFnSc2	Myslová
<g/>
,	,	kIx,	,
č.	č.	k?	č.
5261	[number]	k4	5261
do	do	k7c2	do
Horní	horní	k2eAgFnSc2d1	horní
Myslové	Myslová	k1gFnSc2	Myslová
<g/>
,	,	kIx,	,
1113	[number]	k4	1113
do	do	k7c2	do
Mrákotína	Mrákotín	k1gInSc2	Mrákotín
a	a	k8xC	a
č.	č.	k?	č.
5126	[number]	k4	5126
do	do	k7c2	do
Řásné	řásný	k2eAgFnSc2d1	Řásná
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
červeně	červeň	k1gFnSc2	červeň
a	a	k8xC	a
zeleně	zeleň	k1gFnSc2	zeleň
značené	značený	k2eAgFnSc2d1	značená
turistické	turistický	k2eAgFnSc2d1	turistická
trasy	trasa	k1gFnSc2	trasa
a	a	k8xC	a
naučné	naučný	k2eAgFnSc2d1	naučná
stezky	stezka	k1gFnSc2	stezka
Otokara	Otokar	k1gMnSc2	Otokar
Březiny	Březina	k1gFnSc2	Březina
a	a	k8xC	a
Lipky	lipka	k1gFnSc2	lipka
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
zde	zde	k6eAd1	zde
provozuje	provozovat	k5eAaImIp3nS	provozovat
své	svůj	k3xOyFgNnSc4	svůj
univerzitní	univerzitní	k2eAgNnSc4d1	univerzitní
centrum	centrum	k1gNnSc4	centrum
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
vysoké	vysoká	k1gFnSc2	vysoká
učení	učení	k1gNnSc2	učení
technické	technický	k2eAgNnSc4d1	technické
výukové	výukový	k2eAgNnSc4d1	výukové
středisko	středisko	k1gNnSc4	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Otokara	Otokar	k1gMnSc2	Otokar
Březiny	Březina	k1gFnSc2	Březina
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
Telč	Telč	k1gFnSc4	Telč
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
každoročně	každoročně	k6eAd1	každoročně
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Prázdniny	prázdniny	k1gFnPc1	prázdniny
v	v	k7c6	v
Telči	Telč	k1gFnSc6	Telč
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
hudebních	hudební	k2eAgInPc2d1	hudební
festivalů	festival	k1gInPc2	festival
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Vystupují	vystupovat	k5eAaImIp3nP	vystupovat
tu	tu	k6eAd1	tu
známí	známý	k2eAgMnPc1d1	známý
umělci	umělec	k1gMnPc1	umělec
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
na	na	k7c6	na
programu	program	k1gInSc6	program
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
divadla	divadlo	k1gNnPc4	divadlo
<g/>
,	,	kIx,	,
výtvarné	výtvarný	k2eAgFnPc4d1	výtvarná
dílny	dílna	k1gFnPc4	dílna
a	a	k8xC	a
výstavy	výstava	k1gFnPc4	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Památky	památka	k1gFnPc1	památka
UNESCO	UNESCO	kA	UNESCO
v	v	k7c6	v
Telči	Telč	k1gFnSc6	Telč
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
získaly	získat	k5eAaPmAgFnP	získat
grant	grant	k1gInSc4	grant
Kraje	kraj	k1gInSc2	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
vázaný	vázaný	k2eAgMnSc1d1	vázaný
na	na	k7c4	na
začlenění	začlenění	k1gNnSc4	začlenění
kamerových	kamerový	k2eAgInPc2d1	kamerový
záznamů	záznam	k1gInPc2	záznam
a	a	k8xC	a
přenosů	přenos	k1gInPc2	přenos
památek	památka	k1gFnPc2	památka
do	do	k7c2	do
pořadu	pořad	k1gInSc2	pořad
Panorama	panorama	k1gNnSc1	panorama
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
městem	město	k1gNnSc7	město
Telčí	Telč	k1gFnPc2	Telč
získaly	získat	k5eAaPmAgFnP	získat
tentýž	týž	k3xTgInSc4	týž
grant	grant	k1gInSc4	grant
i	i	k8xC	i
města	město	k1gNnSc2	město
Třebíč	Třebíč	k1gFnSc1	Třebíč
a	a	k8xC	a
Žďár	Žďár	k1gInSc1	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
.	.	kIx.	.
</s>
<s>
Vysílání	vysílání	k1gNnSc1	vysílání
mělo	mít	k5eAaImAgNnS	mít
probíhat	probíhat	k5eAaImF	probíhat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
do	do	k7c2	do
konce	konec	k1gInSc2	konec
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Záznamy	záznam	k1gInPc1	záznam
měly	mít	k5eAaImAgInP	mít
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
být	být	k5eAaImF	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
organizace	organizace	k1gFnPc1	organizace
UNESCO	UNESCO	kA	UNESCO
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
lokací	lokace	k1gFnSc7	lokace
českých	český	k2eAgMnPc2d1	český
i	i	k8xC	i
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
filmařů	filmař	k1gMnPc2	filmař
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
zde	zde	k6eAd1	zde
natáčeny	natáčet	k5eAaImNgFnP	natáčet
např.	např.	kA	např.
firmy	firma	k1gFnSc2	firma
Až	až	k6eAd1	až
přijde	přijít	k5eAaPmIp3nS	přijít
kocour	kocour	k1gInSc1	kocour
<g/>
,	,	kIx,	,
Jak	jak	k8xC	jak
se	se	k3xPyFc4	se
budí	budit	k5eAaImIp3nS	budit
princezny	princezna	k1gFnSc2	princezna
<g/>
,	,	kIx,	,
Z	z	k7c2	z
pekla	peklo	k1gNnSc2	peklo
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Telči	Telč	k1gFnSc6	Telč
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
Telče	Telč	k1gFnSc2	Telč
<g/>
,	,	kIx,	,
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejcennějším	cenný	k2eAgFnPc3d3	nejcennější
městským	městský	k2eAgFnPc3d1	městská
památkovým	památkový	k2eAgFnPc3d1	památková
rezervacím	rezervace	k1gFnPc3	rezervace
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
bylo	být	k5eAaImAgNnS	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
památkové	památkový	k2eAgFnSc6d1	památková
hodnotě	hodnota	k1gFnSc6	hodnota
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
i	i	k9	i
předměstí	předměstí	k1gNnSc4	předměstí
Staré	Stará	k1gFnSc2	Stará
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
historická	historický	k2eAgFnSc1d1	historická
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
okolí	okolí	k1gNnSc1	okolí
kostela	kostel	k1gInSc2	kostel
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgFnSc2d1	boží
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k9	i
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
výstavba	výstavba	k1gFnSc1	výstavba
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
města	město	k1gNnSc2	město
respektovala	respektovat	k5eAaImAgFnS	respektovat
hodnotné	hodnotný	k2eAgNnSc4d1	hodnotné
městské	městský	k2eAgNnSc4d1	Městské
panoráma	panoráma	k1gNnSc4	panoráma
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
není	být	k5eNaImIp3nS	být
narušeno	narušit	k5eAaPmNgNnS	narušit
převýšenou	převýšený	k2eAgFnSc7d1	převýšená
panelovou	panelový	k2eAgFnSc7d1	panelová
zástavbou	zástavba	k1gFnSc7	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
a	a	k8xC	a
také	také	k9	také
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
architektonickou	architektonický	k2eAgFnSc7d1	architektonická
památkou	památka	k1gFnSc7	památka
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
renesanční	renesanční	k2eAgInSc1d1	renesanční
telčský	telčský	k2eAgInSc1d1	telčský
zámek	zámek	k1gInSc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Telčský	telčský	k2eAgInSc1d1	telčský
zámek	zámek	k1gInSc1	zámek
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
klenoty	klenot	k1gInPc7	klenot
moravské	moravský	k2eAgFnSc2d1	Moravská
renesanční	renesanční	k2eAgFnSc2d1	renesanční
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
přitažlivost	přitažlivost	k1gFnSc1	přitažlivost
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
díky	díky	k7c3	díky
citlivému	citlivý	k2eAgInSc3d1	citlivý
přístupu	přístup	k1gInSc3	přístup
majitelů	majitel	k1gMnPc2	majitel
k	k	k7c3	k
dědictví	dědictví	k1gNnSc3	dědictví
minulosti	minulost	k1gFnSc2	minulost
zachovaly	zachovat	k5eAaPmAgInP	zachovat
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
původní	původní	k2eAgInPc4d1	původní
interiéry	interiér	k1gInPc4	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
významné	významný	k2eAgFnPc4d1	významná
památky	památka	k1gFnPc4	památka
Vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
Města	město	k1gNnSc2	město
patří	patřit	k5eAaImIp3nS	patřit
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
Staršího	starší	k1gMnSc2	starší
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgInSc1d1	barokní
jezuitský	jezuitský	k2eAgInSc1d1	jezuitský
kostel	kostel	k1gInSc1	kostel
Jména	jméno	k1gNnSc2	jméno
Ježíš	ježit	k5eAaImIp2nS	ježit
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Ducha	duch	k1gMnSc2	duch
s	s	k7c7	s
pozdně	pozdně	k6eAd1	pozdně
románskou	románský	k2eAgFnSc7d1	románská
věží	věž	k1gFnSc7	věž
nebo	nebo	k8xC	nebo
barokní	barokní	k2eAgInSc1d1	barokní
morový	morový	k2eAgInSc1d1	morový
sloup	sloup	k1gInSc1	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Památkově	památkově	k6eAd1	památkově
chráněny	chránit	k5eAaImNgFnP	chránit
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
jedinečně	jedinečně	k6eAd1	jedinečně
dochované	dochovaný	k2eAgInPc1d1	dochovaný
měšťanské	měšťanský	k2eAgInPc1d1	měšťanský
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
náměstí	náměstí	k1gNnSc4	náměstí
Zachariáše	Zachariáš	k1gMnSc2	Zachariáš
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Telčský	telčský	k2eAgInSc1d1	telčský
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
U	u	k7c2	u
Šeniglů	Šenigl	k1gInPc2	Šenigl
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
kostela	kostel	k1gInSc2	kostel
Matky	matka	k1gFnSc2	matka
Boží	božit	k5eAaImIp3nS	božit
památkově	památkově	k6eAd1	památkově
chráněn	chránit	k5eAaImNgInS	chránit
například	například	k6eAd1	například
i	i	k8xC	i
objekt	objekt	k1gInSc1	objekt
bývalé	bývalý	k2eAgFnSc2d1	bývalá
jezuitské	jezuitský	k2eAgFnSc2d1	jezuitská
koleje	kolej	k1gFnSc2	kolej
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc4d1	zvaný
dnes	dnes	k6eAd1	dnes
Lannerův	Lannerův	k2eAgInSc4d1	Lannerův
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Podolí	Podolí	k1gNnSc6	Podolí
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
památkou	památka	k1gFnSc7	památka
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kapli	kaple	k1gFnSc3	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Karla	Karel	k1gMnSc2	Karel
Boromejského	Boromejský	k2eAgMnSc4d1	Boromejský
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Telče	Telč	k1gFnSc2	Telč
Křížová	Křížová	k1gFnSc1	Křížová
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
památkám	památka	k1gFnPc3	památka
na	na	k7c4	na
zdejší	zdejší	k2eAgNnSc4d1	zdejší
židovské	židovský	k2eAgNnSc4d1	Židovské
osídlení	osídlení	k1gNnSc4	osídlení
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
zdejší	zdejší	k2eAgFnSc1d1	zdejší
synagoga	synagoga	k1gFnSc1	synagoga
a	a	k8xC	a
židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Míla	Míla	k1gFnSc1	Míla
Doleželová	Doleželová	k1gFnSc1	Doleželová
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malířka	malířka	k1gFnSc1	malířka
a	a	k8xC	a
ilustrátorka	ilustrátorka	k1gFnSc1	ilustrátorka
Jakub	Jakub	k1gMnSc1	Jakub
Dvořecký	Dvořecký	k2eAgMnSc1d1	Dvořecký
(	(	kIx(	(
<g/>
1750	[number]	k4	1750
<g/>
–	–	k?	–
<g/>
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Kypta	Kypta	k1gMnSc1	Kypta
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
–	–	k?	–
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kantor	kantor	k1gMnSc1	kantor
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Václav	Václav	k1gMnSc1	Václav
Beneš	Beneš	k1gMnSc1	Beneš
Optát	Optát	k1gMnSc1	Optát
<g/>
,	,	kIx,	,
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Palliardi	Palliard	k1gMnPc1	Palliard
(	(	kIx(	(
<g/>
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
Theodor	Theodor	k1gMnSc1	Theodor
Schaefer	Schaefer	k1gMnSc1	Schaefer
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
Josef	Josef	k1gMnSc1	Josef
Sobotka	Sobotka	k1gMnSc1	Sobotka
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1836	[number]	k4	1836
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Telče	Telč	k1gFnSc2	Telč
<g/>
,	,	kIx,	,
zemský	zemský	k2eAgMnSc1d1	zemský
poslanec	poslanec	k1gMnSc1	poslanec
Siegfried	Siegfried	k1gMnSc1	Siegfried
Taub	Taub	k1gMnSc1	Taub
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
,	,	kIx,	,
tajemník	tajemník	k1gMnSc1	tajemník
Eliška	Eliška	k1gFnSc1	Eliška
Urbancová	Urbancová	k1gFnSc1	Urbancová
<g/>
,	,	kIx,	,
modelka	modelka	k1gFnSc1	modelka
Jan	Jan	k1gMnSc1	Jan
Vlk	Vlk	k1gMnSc1	Vlk
(	(	kIx(	(
<g/>
1822	[number]	k4	1822
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
notář	notář	k1gMnSc1	notář
<g/>
,	,	kIx,	,
buditel	buditel	k1gMnSc1	buditel
Zachariáš	Zachariáš	k1gMnSc1	Zachariáš
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
<g/>
,	,	kIx,	,
humanista	humanista	k1gMnSc1	humanista
Belp	Belp	k1gMnSc1	Belp
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
Figeac	Figeac	k1gInSc1	Figeac
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Rothenburg	Rothenburg	k1gInSc4	Rothenburg
ob	ob	k7c4	ob
der	drát	k5eAaImRp2nS	drát
Tauber	Tauber	k1gInSc1	Tauber
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Šaľa	Šaľ	k1gInSc2	Šaľ
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
Waidhofen	Waidhofna	k1gFnPc2	Waidhofna
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Thaya	Thaya	k1gFnSc1	Thaya
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
Wilber	Wilbra	k1gFnPc2	Wilbra
<g/>
,	,	kIx,	,
Nebraska	Nebraska	k1gFnSc1	Nebraska
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
