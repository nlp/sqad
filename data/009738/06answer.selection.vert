<s>
Appeasement	appeasement	k1gInSc1	appeasement
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
apaisement	apaisement	k1gInSc1	apaisement
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
pacifistickou	pacifistický	k2eAgFnSc4d1	pacifistická
politiku	politika	k1gFnSc4	politika
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
symbolizovalo	symbolizovat	k5eAaImAgNnS	symbolizovat
hlavně	hlavně	k9	hlavně
ustupování	ustupování	k1gNnSc1	ustupování
agresivním	agresivní	k2eAgFnPc3d1	agresivní
stranám	strana	k1gFnPc3	strana
<g/>
.	.	kIx.	.
</s>
