<p>
<s>
Appeasement	appeasement	k1gInSc1	appeasement
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
apaisement	apaisement	k1gInSc1	apaisement
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
pacifistickou	pacifistický	k2eAgFnSc4d1	pacifistická
politiku	politika	k1gFnSc4	politika
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
symbolizovalo	symbolizovat	k5eAaImAgNnS	symbolizovat
hlavně	hlavně	k9	hlavně
ustupování	ustupování	k1gNnSc1	ustupování
agresivním	agresivní	k2eAgFnPc3d1	agresivní
stranám	strana	k1gFnPc3	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Politici	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
appeasement	appeasement	k1gInSc4	appeasement
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
<g/>
,	,	kIx,	,
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uspokojením	uspokojení	k1gNnSc7	uspokojení
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
požadavků	požadavek	k1gInPc2	požadavek
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
odvrátí	odvrátit	k5eAaPmIp3nP	odvrátit
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
zachovají	zachovat	k5eAaPmIp3nP	zachovat
mír	mír	k1gInSc4	mír
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
motivováno	motivován	k2eAgNnSc1d1	motivováno
snahou	snaha	k1gFnSc7	snaha
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
hrůzám	hrůza	k1gFnPc3	hrůza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgFnP	odehrát
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
s	s	k7c7	s
nadějí	naděje	k1gFnSc7	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
evropské	evropský	k2eAgInPc1d1	evropský
národy	národ	k1gInPc1	národ
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
uspořádání	uspořádání	k1gNnSc6	uspořádání
již	již	k6eAd1	již
další	další	k2eAgFnSc4d1	další
válku	válka	k1gFnSc4	válka
nedopustí	dopustit	k5eNaPmIp3nS	dopustit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
úsilí	úsilí	k1gNnSc1	úsilí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
trvalo	trvat	k5eAaImAgNnS	trvat
několik	několik	k4yIc1	několik
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
marné	marný	k2eAgNnSc1d1	marné
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
bylo	být	k5eAaImAgNnS	být
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
ještě	ještě	k6eAd1	ještě
posíleno	posílit	k5eAaPmNgNnS	posílit
a	a	k8xC	a
demokratické	demokratický	k2eAgFnSc2d1	demokratická
síly	síla	k1gFnSc2	síla
oslabeny	oslabit	k5eAaPmNgInP	oslabit
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
appeasement	appeasement	k1gInSc1	appeasement
vnímán	vnímat	k5eAaImNgInS	vnímat
velmi	velmi	k6eAd1	velmi
negativně	negativně	k6eAd1	negativně
a	a	k8xC	a
jako	jako	k9	jako
historický	historický	k2eAgInSc4d1	historický
omyl	omyl	k1gInSc4	omyl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uplatnění	uplatnění	k1gNnSc1	uplatnění
politiky	politika	k1gFnSc2	politika
appeasementu	appeasement	k1gInSc2	appeasement
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Janovská	janovský	k2eAgFnSc1d1	janovská
konference	konference	k1gFnSc1	konference
===	===	k?	===
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
probíhala	probíhat	k5eAaImAgFnS	probíhat
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
až	až	k9	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1922	[number]	k4	1922
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
29	[number]	k4	29
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
neúspěšnému	úspěšný	k2eNgNnSc3d1	neúspěšné
jednání	jednání	k1gNnSc3	jednání
o	o	k7c6	o
urovnání	urovnání	k1gNnSc6	urovnání
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
styků	styk	k1gInPc2	styk
s	s	k7c7	s
nově	nově	k6eAd1	nově
ustaveným	ustavený	k2eAgNnSc7d1	ustavené
RSFSR	RSFSR	kA	RSFSR
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Sověti	Sovět	k1gMnPc1	Sovět
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
uhradit	uhradit	k5eAaPmF	uhradit
dluhy	dluh	k1gInPc4	dluh
za	za	k7c4	za
carský	carský	k2eAgInSc4d1	carský
režim	režim	k1gInSc4	režim
a	a	k8xC	a
znárodněný	znárodněný	k2eAgInSc4d1	znárodněný
cizí	cizí	k2eAgInSc4d1	cizí
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Neúspěch	neúspěch	k1gInSc1	neúspěch
jednání	jednání	k1gNnSc2	jednání
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
konference	konference	k1gFnSc2	konference
vyústil	vyústit	k5eAaPmAgMnS	vyústit
ve	v	k7c4	v
dvoustranná	dvoustranný	k2eAgNnPc4d1	dvoustranné
jednání	jednání	k1gNnPc4	jednání
mezi	mezi	k7c7	mezi
německou	německý	k2eAgFnSc7d1	německá
a	a	k8xC	a
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
delegací	delegace	k1gFnSc7	delegace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
úspěšně	úspěšně	k6eAd1	úspěšně
zakončena	zakončit	k5eAaPmNgFnS	zakončit
podpisem	podpis	k1gInSc7	podpis
Rapallské	Rapallský	k2eAgFnSc2d1	Rapallská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tajná	tajný	k2eAgNnPc1d1	tajné
ustanovení	ustanovení	k1gNnPc1	ustanovení
dokumentu	dokument	k1gInSc2	dokument
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
nabídku	nabídka	k1gFnSc4	nabídka
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základě	základ	k1gInSc6	základ
získalo	získat	k5eAaPmAgNnS	získat
Německo	Německo	k1gNnSc1	Německo
možnost	možnost	k1gFnSc1	možnost
cvičit	cvičit	k5eAaImF	cvičit
své	svůj	k3xOyFgFnPc4	svůj
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
na	na	k7c6	na
sovětském	sovětský	k2eAgNnSc6d1	sovětské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ustanovení	ustanovení	k1gNnSc2	ustanovení
versailleské	versailleský	k2eAgFnSc2d1	Versailleská
mírové	mírový	k2eAgFnSc2d1	mírová
smlouvy	smlouva	k1gFnSc2	smlouva
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
možnosti	možnost	k1gFnPc4	možnost
zkoušet	zkoušet	k5eAaImF	zkoušet
nové	nový	k2eAgFnPc4d1	nová
vojenské	vojenský	k2eAgFnPc4d1	vojenská
teorie	teorie	k1gFnPc4	teorie
na	na	k7c6	na
vlastním	vlastní	k2eAgNnSc6d1	vlastní
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
prostě	prostě	k6eAd1	prostě
přesunula	přesunout	k5eAaPmAgFnS	přesunout
výcvik	výcvik	k1gInSc4	výcvik
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měla	mít	k5eAaImAgFnS	mít
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnPc4d1	dobrá
materiální	materiální	k2eAgFnPc4d1	materiální
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Masové	masový	k2eAgNnSc1d1	masové
nasazení	nasazení	k1gNnSc1	nasazení
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
podporovaných	podporovaný	k2eAgNnPc2d1	podporované
letectvem	letectvo	k1gNnSc7	letectvo
plnícím	plnící	k2eAgMnPc3d1	plnící
úlohu	úloha	k1gFnSc4	úloha
létajícího	létající	k2eAgNnSc2d1	létající
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
<g/>
,	,	kIx,	,
princip	princip	k1gInSc1	princip
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
založena	založit	k5eAaPmNgFnS	založit
strategie	strategie	k1gFnSc1	strategie
bleskové	bleskový	k2eAgFnSc2d1	blesková
války	válka	k1gFnSc2	válka
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
zformulován	zformulován	k2eAgInSc1d1	zformulován
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
vyzkoušena	vyzkoušet	k5eAaPmNgFnS	vyzkoušet
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Nekonečné	konečný	k2eNgFnPc4d1	nekonečná
ruské	ruský	k2eAgFnPc4d1	ruská
stepi	step	k1gFnPc4	step
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
nabízely	nabízet	k5eAaImAgInP	nabízet
teoretikům	teoretik	k1gMnPc3	teoretik
dostatek	dostatek	k1gInSc4	dostatek
příležitosti	příležitost	k1gFnSc2	příležitost
k	k	k7c3	k
ověření	ověření	k1gNnSc3	ověření
a	a	k8xC	a
případné	případný	k2eAgFnSc3d1	případná
korekci	korekce	k1gFnSc3	korekce
představ	představa	k1gFnPc2	představa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Kazaně	Kazaň	k1gFnSc2	Kazaň
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozeběhl	rozeběhnout	k5eAaPmAgInS	rozeběhnout
výcvik	výcvik	k1gInSc1	výcvik
budoucích	budoucí	k2eAgMnPc2d1	budoucí
tankistů	tankista	k1gMnPc2	tankista
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
dělo	dít	k5eAaBmAgNnS	dít
s	s	k7c7	s
leteckým	letecký	k2eAgInSc7d1	letecký
personálem	personál	k1gInSc7	personál
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Italsko-etiopská	italskotiopský	k2eAgFnSc1d1	italsko-etiopský
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Italská	italský	k2eAgFnSc1d1	italská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
expanze	expanze	k1gFnSc1	expanze
do	do	k7c2	do
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
italské	italský	k2eAgFnPc1d1	italská
jednotky	jednotka	k1gFnPc1	jednotka
bez	bez	k7c2	bez
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
války	válka	k1gFnSc2	válka
na	na	k7c6	na
území	území	k1gNnSc6	území
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
května	květen	k1gInSc2	květen
1936	[number]	k4	1936
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
ovládnuta	ovládnout	k5eAaPmNgFnS	ovládnout
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
</s>
</p>
<p>
<s>
Italská	italský	k2eAgFnSc1d1	italská
východní	východní	k2eAgFnSc1d1	východní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Španělská	španělský	k2eAgFnSc1d1	španělská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Berchtesgadenská	Berchtesgadenský	k2eAgFnSc1d1	Berchtesgadenský
schůzka	schůzka	k1gFnSc1	schůzka
===	===	k?	===
</s>
</p>
<p>
<s>
Berchtesgadenská	Berchtesgadenský	k2eAgFnSc1d1	Berchtesgadenský
schůzka	schůzka	k1gFnSc1	schůzka
byla	být	k5eAaImAgFnS	být
předehrou	předehra	k1gFnSc7	předehra
k	k	k7c3	k
Mnichovské	mnichovský	k2eAgFnSc3d1	Mnichovská
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Účastnili	účastnit	k5eAaImAgMnP	účastnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
a	a	k8xC	a
Neville	Neville	k1gFnSc1	Neville
Chamberlain	Chamberlaina	k1gFnPc2	Chamberlaina
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
zde	zde	k6eAd1	zde
formuloval	formulovat	k5eAaImAgMnS	formulovat
požadavky	požadavek	k1gInPc4	požadavek
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
vůči	vůči	k7c3	vůči
Československu	Československo	k1gNnSc3	Československo
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgMnPc4	který
hlavně	hlavně	k9	hlavně
patřilo	patřit	k5eAaImAgNnS	patřit
odstoupení	odstoupení	k1gNnSc1	odstoupení
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
československého	československý	k2eAgNnSc2d1	Československé
pohraničí	pohraničí	k1gNnSc2	pohraničí
(	(	kIx(	(
<g/>
okresy	okres	k1gInPc1	okres
s	s	k7c7	s
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
50	[number]	k4	50
%	%	kIx~	%
německých	německý	k2eAgMnPc2d1	německý
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
zrušení	zrušení	k1gNnSc1	zrušení
československo-sovětské	československoovětský	k2eAgFnSc2d1	československo-sovětská
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
s	s	k7c7	s
požadavky	požadavek	k1gInPc7	požadavek
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
===	===	k?	===
</s>
</p>
<p>
<s>
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
posledních	poslední	k2eAgInPc2d1	poslední
velkých	velký	k2eAgInPc2d1	velký
ústupků	ústupek	k1gInPc2	ústupek
politiky	politika	k1gFnSc2	politika
appeasmentu	appeasment	k1gInSc2	appeasment
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
okupace	okupace	k1gFnSc1	okupace
Československa	Československo	k1gNnSc2	Československo
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
přiměla	přimět	k5eAaPmAgFnS	přimět
britské	britský	k2eAgNnSc4d1	Britské
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
k	k	k7c3	k
odporu	odpor	k1gInSc3	odpor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
donutilo	donutit	k5eAaPmAgNnS	donutit
nerozhodnou	rozhodnout	k5eNaPmIp3nP	rozhodnout
a	a	k8xC	a
přímému	přímý	k2eAgInSc3d1	přímý
konfliktu	konflikt	k1gInSc3	konflikt
se	se	k3xPyFc4	se
vyhýbající	vyhýbající	k2eAgFnSc4d1	vyhýbající
vládu	vláda	k1gFnSc4	vláda
jednat	jednat	k5eAaImF	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
předpokládaje	předpokládat	k5eAaImSgInS	předpokládat
ústupnost	ústupnost	k1gFnSc4	ústupnost
<g/>
,	,	kIx,	,
napadl	napadnout	k5eAaPmAgMnS	napadnout
v	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
západní	západní	k2eAgInPc1d1	západní
státy	stát	k1gInPc1	stát
vyhlásily	vyhlásit	k5eAaPmAgInP	vyhlásit
Německu	Německo	k1gNnSc6	Německo
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
politickém	politický	k2eAgInSc6d1	politický
jazyce	jazyk	k1gInSc6	jazyk
pro	pro	k7c4	pro
celé	celý	k2eAgFnPc4d1	celá
generace	generace	k1gFnPc4	generace
stal	stát	k5eAaPmAgInS	stát
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
"	"	kIx"	"
synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
zbabělý	zbabělý	k2eAgInSc4d1	zbabělý
ústup	ústup	k1gInSc4	ústup
autoritářským	autoritářský	k2eAgFnPc3d1	autoritářská
vládám	vláda	k1gFnPc3	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
