<p>
<s>
Electronic	Electronice	k1gFnPc2	Electronice
Entertainment	Entertainment	k1gMnSc1	Entertainment
Expo	Expo	k1gNnSc4	Expo
neboli	neboli	k8xC	neboli
E3	E3	k1gFnSc4	E3
je	být	k5eAaImIp3nS	být
každoroční	každoroční	k2eAgInSc1d1	každoroční
veletrh	veletrh	k1gInSc1	veletrh
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
videoherní	videoherní	k2eAgInSc4d1	videoherní
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Výrobci	výrobce	k1gMnPc1	výrobce
často	často	k6eAd1	často
předvádějí	předvádět	k5eAaImIp3nP	předvádět
nové	nový	k2eAgFnPc4d1	nová
hry	hra	k1gFnPc4	hra
právě	právě	k9	právě
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
veletrhu	veletrh	k1gInSc6	veletrh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
hráčům	hráč	k1gMnPc3	hráč
předvést	předvést	k5eAaPmF	předvést
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
E3	E3	k4	E3
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
koná	konat	k5eAaImIp3nS	konat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
května	květen	k1gInSc2	květen
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
června	červen	k1gInSc2	červen
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
</s>
</p>
<p>
<s>
Veletrhu	veletrh	k1gInSc3	veletrh
E3	E3	k1gFnSc1	E3
2015	[number]	k4	2015
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
Convention	Convention	k1gInSc4	Convention
Center	centrum	k1gNnPc2	centrum
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
52200	[number]	k4	52200
návštěvníků	návštěvník	k1gMnPc2	návštěvník
a	a	k8xC	a
300	[number]	k4	300
vystavovatelů	vystavovatel	k1gMnPc2	vystavovatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
představili	představit	k5eAaPmAgMnP	představit
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
1600	[number]	k4	1600
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Electronic	Electronice	k1gFnPc2	Electronice
Entertainment	Entertainment	k1gMnSc1	Entertainment
Expo	Expo	k1gNnSc4	Expo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
E3	E3	k4	E3
2018	[number]	k4	2018
–	–	k?	–
přehled	přehled	k1gInSc1	přehled
</s>
</p>
