<s>
Optické	optický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Optické	optický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
charakterizují	charakterizovat	k5eAaBmIp3nP
odezvu	odezva	k1gFnSc4
materiálů	materiál	k1gInPc2
na	na	k7c4
dopadající	dopadající	k2eAgNnSc4d1
elektromagnetické	elektromagnetický	k2eAgNnSc4d1
záření	záření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
každý	každý	k3xTgInSc4
materiál	materiál	k1gInSc4
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
dopadající	dopadající	k2eAgNnSc1d1
záření	záření	k1gNnSc1
je	být	k5eAaImIp3nS
materiálem	materiál	k1gInSc7
částečně	částečně	k6eAd1
propuštěno	propuštěn	k2eAgNnSc1d1
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
odraženo	odražen	k2eAgNnSc1d1
a	a	k8xC
částečně	částečně	k6eAd1
pohlceno	pohlcen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
optické	optický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
emisivita	emisivita	k1gFnSc1
</s>
<s>
propustnost	propustnost	k1gFnSc1
</s>
<s>
odrazivost	odrazivost	k1gFnSc1
</s>
<s>
pohltivost	pohltivost	k1gFnSc1
(	(	kIx(
<g/>
absorptivita	absorptivita	k1gFnSc1
<g/>
,	,	kIx,
absorpce	absorpce	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Optické	optický	k2eAgFnPc1d1
konstanty	konstanta	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Optické	optický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
materiálů	materiál	k1gInPc2
a	a	k8xC
výrobků	výrobek	k1gInPc2
</s>
