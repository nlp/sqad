<s>
Gymnázium	gymnázium	k1gNnSc1
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
</s>
<s desamb="1">
První	první	k4xOgFnSc1
skupina	skupina	k1gFnSc1
francouzského	francouzský	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
vedená	vedený	k2eAgFnSc1d1
francouzskou	francouzský	k2eAgFnSc7d1
lektorkou	lektorka	k1gFnSc7
Brunhilde	Brunhilde	k1gFnSc1
Gendras	Gendras	k1gFnSc1
uvedla	uvést	k5eAaPmAgFnS
na	na	k7c6
Festivadle	Festivadlo	k1gNnSc6
Brno	Brno	k1gNnSc1
1999	#num#	k4
La	la	k1gNnPc2
Fontainovy	Fontainův	k2eAgFnPc1d1
bajky	bajka	k1gFnPc1
<g/>
,	,	kIx,
získala	získat	k5eAaPmAgFnS
první	první	k4xOgFnSc4
cenu	cena	k1gFnSc4
a	a	k8xC
reprezentovala	reprezentovat	k5eAaImAgFnS
ČR	ČR	kA
v	v	k7c4
La	la	k1gNnSc6
Roche-sur	Roche-sur	k1gNnSc6
Yon	Yon	k1gNnSc6
na	na	k7c6
Festivale	festival	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
škole	škola	k1gFnSc6
působí	působit	k5eAaImIp3nS
pěvecký	pěvecký	k2eAgInSc1d1
sbor	sbor	k2gInSc1
Gybon	Gybon	k1gFnSc1
a	a	k8xC
francouzské	francouzský	k2eAgNnSc4d1
divadlo	divadlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>