<s>
Gymnázium	gymnázium	k1gNnSc1
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
má	mít	k5eAaImIp3nS
znaky	znak	k1gInPc4
reklamy	reklama	k1gFnSc2
<g/>
,	,	kIx,
politické	politický	k2eAgFnSc2d1
agitace	agitace	k1gFnSc2
<g/>
,	,	kIx,
propagace	propagace	k1gFnSc2
<g/>
,	,	kIx,
sebepropagace	sebepropagace	k1gFnSc2
apod.	apod.	kA
</s>
<s>
Můžete	moct	k5eAaImIp2nP
pomoci	pomoct	k5eAaPmF
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
obsah	obsah	k1gInSc1
upravíte	upravit	k5eAaPmIp2nP
podle	podle	k7c2
nezaujatého	zaujatý	k2eNgInSc2d1
úhlu	úhel	k1gInSc2
pohledu	pohled	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
článku	článek	k1gInSc6
lze	lze	k6eAd1
diskutovat	diskutovat	k5eAaImF
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
diskusní	diskusní	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
vyřešení	vyřešení	k1gNnSc2
<g/>
,	,	kIx,
prosíme	prosit	k5eAaImIp1nP
<g/>
,	,	kIx,
tuto	tento	k3xDgFnSc4
zprávu	zpráva	k1gFnSc4
neodstraňujte	odstraňovat	k5eNaImRp2nP
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
Zkratka	zkratka	k1gFnSc1
</s>
<s>
GYBON	GYBON	kA
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
příspěvková	příspěvkový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Zřizovatel	zřizovatel	k1gMnSc1
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
Datum	datum	k1gNnSc4
založení	založení	k1gNnSc2
</s>
<s>
1978	#num#	k4
IČO	IČO	kA
</s>
<s>
62690043	#num#	k4
REDIZO	REDIZO	kA
</s>
<s>
600011666	#num#	k4
Ředitel	ředitel	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Štěrba	Štěrba	k1gMnSc1
Zástupce	zástupce	k1gMnSc1
</s>
<s>
Mgr.	Mgr.	kA
Iveta	Iveta	k1gFnSc1
Jalůvková	Jalůvková	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Mgr.	Mgr.	kA
Otto	Otto	k1gMnSc1
Urban	Urban	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Ing.	ing.	kA
Ondřej	Ondřej	k1gMnSc1
Rusek	Rusko	k1gNnPc2
Adresa	adresa	k1gFnSc1
</s>
<s>
Pospíšilova	Pospíšilův	k2eAgFnSc1d1
třída	třída	k1gFnSc1
324500	#num#	k4
03	#num#	k4
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
23	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Počet	počet	k1gInSc1
žáků	žák	k1gMnPc2
</s>
<s>
512	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
http://www.gybon.cz	http://www.gybon.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
je	být	k5eAaImIp3nS
královéhradecké	královéhradecký	k2eAgNnSc1d1
veřejné	veřejný	k2eAgNnSc1d1
gymnázium	gymnázium	k1gNnSc1
s	s	k7c7
jedním	jeden	k4xCgInSc7
typem	typ	k1gInSc7
studia	studio	k1gNnSc2
–	–	k?
šestiletým	šestiletý	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
výuku	výuka	k1gFnSc4
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
cizích	cizí	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
roku	rok	k1gInSc2
1978	#num#	k4
a	a	k8xC
prvním	první	k4xOgMnSc6
ředitelem	ředitel	k1gMnSc7
byl	být	k5eAaImAgMnS
Vladislav	Vladislav	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
sídlilo	sídlit	k5eAaImAgNnS
v	v	k7c6
Šimkově	Šimkův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
a	a	k8xC
bylo	být	k5eAaImAgNnS
relativně	relativně	k6eAd1
malé	malý	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
přesunu	přesun	k1gInSc6
části	část	k1gFnSc2
studentů	student	k1gMnPc2
z	z	k7c2
Gymnázia	gymnázium	k1gNnSc2
J.	J.	kA
K.	K.	kA
Tyla	Tyl	k1gMnSc2
se	se	k3xPyFc4
rozšířilo	rozšířit	k5eAaPmAgNnS
a	a	k8xC
přestěhovalo	přestěhovat	k5eAaPmAgNnS
do	do	k7c2
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
budovy	budova	k1gFnSc2
bývalé	bývalý	k2eAgFnSc2d1
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
Pospíšilově	Pospíšilův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
sídlí	sídlet	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Škola	škola	k1gFnSc1
měla	mít	k5eAaImAgFnS
k	k	k7c3
roku	rok	k1gInSc3
2017	#num#	k4
zhruba	zhruba	k6eAd1
540	#num#	k4
studentů	student	k1gMnPc2
v	v	k7c6
18	#num#	k4
třídách	třída	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
vyučovali	vyučovat	k5eAaImAgMnP
44	#num#	k4
interní	interní	k2eAgInSc4d1
a	a	k8xC
čtyři	čtyři	k4xCgMnPc1
externí	externí	k2eAgMnPc1d1
učitelé	učitel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
škole	škola	k1gFnSc6
působí	působit	k5eAaImIp3nS
pěvecký	pěvecký	k2eAgInSc1d1
sbor	sbor	k2gInSc1
Gybon	Gybon	k1gFnSc1
a	a	k8xC
francouzské	francouzský	k2eAgNnSc4d1
divadlo	divadlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gymnázium	gymnázium	k1gNnSc1
má	mít	k5eAaImIp3nS
dlouholeté	dlouholetý	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
se	s	k7c7
školami	škola	k1gFnPc7
ve	v	k7c6
Würzburgu	Würzburg	k1gInSc6
a	a	k8xC
v	v	k7c6
Lythamu	Lytham	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
škole	škola	k1gFnSc6
působí	působit	k5eAaImIp3nS
Klub	klub	k1gInSc1
rodičů	rodič	k1gMnPc2
a	a	k8xC
přátel	přítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
školy	škola	k1gFnSc2
</s>
<s>
Budova	budova	k1gFnSc1
škol	škola	k1gFnPc2
-	-	kIx~
Pospíšilova	Pospíšilův	k2eAgFnSc1d1
a	a	k8xC
Šimkova	Šimkův	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1
Gymnázium	gymnázium	k1gNnSc1
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
roku	rok	k1gInSc2
1978	#num#	k4
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
potřebu	potřeba	k1gFnSc4
vzniku	vznik	k1gInSc2
druhého	druhý	k4xOgNnSc2
gymnázia	gymnázium	k1gNnSc2
v	v	k7c6
rostoucím	rostoucí	k2eAgNnSc6d1
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
jediné	jediný	k2eAgNnSc1d1
hradecké	hradecký	k2eAgNnSc1d1
Gymnázium	gymnázium	k1gNnSc1
Josefa	Josef	k1gMnSc2
Kajetána	Kajetán	k1gMnSc2
Tyla	týt	k5eAaImAgFnS
už	už	k6eAd1
svou	svůj	k3xOyFgFnSc7
kapacitou	kapacita	k1gFnSc7
nestačilo	stačit	k5eNaBmAgNnS
pokrývat	pokrývat	k5eAaImF
poptávku	poptávka	k1gFnSc4
po	po	k7c6
studiu	studio	k1gNnSc6
gymnaziálního	gymnaziální	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c6
zřízení	zřízení	k1gNnSc6
nové	nový	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
školní	školní	k2eAgInSc4d1
rok	rok	k1gInSc4
studovalo	studovat	k5eAaImAgNnS
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
celkem	celkem	k6eAd1
osm	osm	k4xCc4
tříd	třída	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
převedeny	převést	k5eAaPmNgFnP
z	z	k7c2
Gymnázia	gymnázium	k1gNnSc2
Josefa	Josef	k1gMnSc2
Kajetána	Kajetán	k1gMnSc2
Tyla	Tyl	k1gMnSc2
a	a	k8xC
které	který	k3yIgFnSc3,k3yRgFnSc3,k3yQgFnSc3
vyučoval	vyučovat	k5eAaImAgInS
šestnáctičlenný	šestnáctičlenný	k2eAgInSc1d1
pedagogický	pedagogický	k2eAgInSc1d1
sbor	sbor	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
do	do	k7c2
nově	nově	k6eAd1
vzniklého	vzniklý	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
přešel	přejít	k5eAaPmAgInS
taktéž	taktéž	k?
z	z	k7c2
Gymnázia	gymnázium	k1gNnSc2
Josefa	Josef	k1gMnSc2
Kajetána	Kajetán	k1gMnSc2
Tyla	Tyl	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
gymnázium	gymnázium	k1gNnSc1
působilo	působit	k5eAaImAgNnS
v	v	k7c6
tzv.	tzv.	kA
pouchovské	pouchovský	k2eAgFnSc6d1
budově	budova	k1gFnSc6
<g/>
,	,	kIx,
podle	podle	k7c2
adresy	adresa	k1gFnSc2
budovy	budova	k1gFnSc2
bylo	být	k5eAaImAgNnS
gymnázium	gymnázium	k1gNnSc1
pojmenováno	pojmenovat	k5eAaPmNgNnS
–	–	k?
Gymnázium	gymnázium	k1gNnSc1
Velká	velká	k1gFnSc1
ulice	ulice	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ředitelem	ředitel	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Vladimír	Vladimír	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
společné	společný	k2eAgInPc4d1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
zaměstnanci	zaměstnanec	k1gMnPc7
školy	škola	k1gFnSc2
během	během	k7c2
letních	letní	k2eAgFnPc2d1
prázdnin	prázdniny	k1gFnPc2
roku	rok	k1gInSc2
1978	#num#	k4
intenzivně	intenzivně	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c4
vybavení	vybavení	k1gNnSc4
budovy	budova	k1gFnSc2
pro	pro	k7c4
účely	účel	k1gInPc4
výuky	výuka	k1gFnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yQnSc3,k3yRnSc3
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nP
začít	začít	k5eAaPmF
vyučovat	vyučovat	k5eAaImF
studenty	student	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podmínky	podmínka	k1gFnPc4
výuky	výuka	k1gFnSc2
v	v	k7c6
budově	budova	k1gFnSc6
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
byly	být	k5eAaImAgInP
velice	velice	k6eAd1
skromné	skromný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
školním	školní	k2eAgInSc6d1
roce	rok	k1gInSc6
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
byla	být	k5eAaImAgFnS
část	část	k1gFnSc1
gymnázia	gymnázium	k1gNnSc2
přesunuta	přesunout	k5eAaPmNgFnS
do	do	k7c2
části	část	k1gFnSc2
budovy	budova	k1gFnSc2
ZŠ	ZŠ	kA
v	v	k7c6
Šimkově	Šimkův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
gymnázia	gymnázium	k1gNnSc2
možné	možný	k2eAgNnSc1d1
uvolnit	uvolnit	k5eAaPmF
díky	díky	k7c3
vzniku	vznik	k1gInSc3
nových	nový	k2eAgFnPc2d1
školních	školní	k2eAgFnPc2d1
budov	budova	k1gFnPc2
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Společně	společně	k6eAd1
s	s	k7c7
přidělením	přidělení	k1gNnSc7
nových	nový	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
bylo	být	k5eAaImAgNnS
do	do	k7c2
školy	škola	k1gFnSc2
přesunuto	přesunout	k5eAaPmNgNnS
dalších	další	k2eAgFnPc2d1
devět	devět	k4xCc1
tříd	třída	k1gFnPc2
z	z	k7c2
GJKT	GJKT	kA
společně	společně	k6eAd1
s	s	k7c7
odpovídajícím	odpovídající	k2eAgInSc7d1
počtem	počet	k1gInSc7
pedagogů	pedagog	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výuka	výuka	k1gFnSc1
ve	v	k7c6
škole	škola	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
nově	nově	k6eAd1
jmenovala	jmenovat	k5eAaBmAgFnS,k5eAaImAgFnS
Gymnázium	gymnázium	k1gNnSc4
v	v	k7c6
Šimkově	Šimkův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
,	,	kIx,
probíhala	probíhat	k5eAaImAgFnS
současné	současný	k2eAgFnPc4d1
ve	v	k7c6
dvou	dva	k4xCgFnPc6
budovách	budova	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
od	od	k7c2
sebe	se	k3xPyFc2
byly	být	k5eAaImAgFnP
vzdáleny	vzdálit	k5eAaPmNgFnP
několik	několik	k4yIc1
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
organizačně	organizačně	k6eAd1
náročné	náročný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
se	se	k3xPyFc4
přestala	přestat	k5eAaPmAgFnS
používat	používat	k5eAaImF
pouchovská	pouchovský	k2eAgFnSc1d1
budova	budova	k1gFnSc1
a	a	k8xC
proběhlo	proběhnout	k5eAaPmAgNnS
poslední	poslední	k2eAgNnSc1d1
stěhování	stěhování	k1gNnSc1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
škola	škola	k1gFnSc1
získala	získat	k5eAaPmAgFnS
celou	celý	k2eAgFnSc4d1
budovu	budova	k1gFnSc4
zrušené	zrušený	k2eAgFnSc2d1
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
na	na	k7c6
Pospíšilově	Pospíšilův	k2eAgFnSc6d1
třidě	třida	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
toto	tento	k3xDgNnSc1
stěhování	stěhování	k1gNnSc1
bylo	být	k5eAaImAgNnS
spojeno	spojit	k5eAaPmNgNnS
se	s	k7c7
změnou	změna	k1gFnSc7
názvu	název	k1gInSc2
školy	škola	k1gFnSc2
na	na	k7c4
Gymnázium	gymnázium	k1gNnSc4
v	v	k7c6
Pospíšilově	Pospíšilův	k2eAgFnSc6d1
třidě	třida	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
bývalé	bývalý	k2eAgFnSc2d1
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
plně	plně	k6eAd1
nevyhovovala	vyhovovat	k5eNaImAgFnS
potřebám	potřeba	k1gFnPc3
gymnaziální	gymnaziální	k2eAgFnSc2d1
výuky	výuka	k1gFnSc2
<g/>
,	,	kIx,
chyběly	chybět	k5eAaImAgFnP
hlavně	hlavně	k9
odborné	odborný	k2eAgFnPc4d1
učebny	učebna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
přestavbám	přestavba	k1gFnPc3
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
vznikly	vzniknout	k5eAaPmAgInP
například	například	k6eAd1
posluchárny	posluchárna	k1gFnSc2
a	a	k8xC
laboratoře	laboratoř	k1gFnSc2
chemie	chemie	k1gFnSc2
<g/>
,	,	kIx,
fyziky	fyzika	k1gFnSc2
nebo	nebo	k8xC
biologie	biologie	k1gFnSc2
<g/>
,	,	kIx,
učebna	učebna	k1gFnSc1
jazyků	jazyk	k1gInPc2
nebo	nebo	k8xC
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Růst	růst	k1gInSc1
kvality	kvalita	k1gFnSc2
zázemí	zázemí	k1gNnSc2
pro	pro	k7c4
výuku	výuka	k1gFnSc4
pomáhal	pomáhat	k5eAaImAgMnS
v	v	k7c6
rozvoji	rozvoj	k1gInSc6
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
odrazem	odraz	k1gInSc7
je	být	k5eAaImIp3nS
školní	školní	k2eAgInSc4d1
rok	rok	k1gInSc4
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
studovalo	studovat	k5eAaImAgNnS
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
770	#num#	k4
studentů	student	k1gMnPc2
ve	v	k7c6
24	#num#	k4
třídách	třída	k1gFnPc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byly	být	k5eAaImAgFnP
nejvyšší	vysoký	k2eAgInPc4d3
počty	počet	k1gInPc4
v	v	k7c6
historii	historie	k1gFnSc6
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přejmenování	přejmenování	k1gNnSc3
školy	škola	k1gFnSc2
na	na	k7c4
Gymnázium	gymnázium	k1gNnSc4
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
debatě	debata	k1gFnSc6
zvítězil	zvítězit	k5eAaPmAgMnS
tento	tento	k3xDgInSc4
název	název	k1gInSc4
nad	nad	k7c7
druhou	druhý	k4xOgFnSc7
možností	možnost	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
název	název	k1gInSc4
školy	škola	k1gFnSc2
spojovala	spojovat	k5eAaImAgFnS
s	s	k7c7
osobností	osobnost	k1gFnSc7
prof.	prof.	kA
Františka	František	k1gMnSc2
Drtiny	drtina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výnos	výnos	k1gInSc1
Ministerstva	ministerstvo	k1gNnSc2
školství	školství	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1936	#num#	k4
sice	sice	k8xC
celý	celý	k2eAgInSc4d1
komplex	komplex	k1gInSc4
budov	budova	k1gFnPc2
pojmenoval	pojmenovat	k5eAaPmAgMnS
„	„	k?
<g/>
Školy	škola	k1gFnPc4
prof.	prof.	kA
Drtiny	drtina	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
nad	nad	k7c7
pokračováním	pokračování	k1gNnSc7
tradice	tradice	k1gFnSc2
pojmenování	pojmenování	k1gNnSc2
školy	škola	k1gFnSc2
po	po	k7c6
tomto	tento	k3xDgInSc6
filosofovi	filosofův	k2eAgMnPc1d1
a	a	k8xC
pedagogovi	pedagogův	k2eAgMnPc1d1
však	však	k9
zvítězila	zvítězit	k5eAaPmAgFnS
vazba	vazba	k1gFnSc1
na	na	k7c4
blízkou	blízký	k2eAgFnSc4d1
sochu	socha	k1gFnSc4
spisovatelky	spisovatelka	k1gFnSc2
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
už	už	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
nacházela	nacházet	k5eAaImAgFnS
před	před	k7c7
budovou	budova	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgInPc6d1
letech	let	k1gInPc6
proběhly	proběhnout	k5eAaPmAgFnP
další	další	k2eAgFnPc1d1
významné	významný	k2eAgFnPc1d1
stavební	stavební	k2eAgFnPc1d1
úpravy	úprava	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgFnP
vybudovány	vybudován	k2eAgFnPc1d1
podzemní	podzemní	k2eAgFnPc1d1
šatny	šatna	k1gFnPc1
a	a	k8xC
nový	nový	k2eAgInSc1d1
hlavní	hlavní	k2eAgInSc1d1
vstup	vstup	k1gInSc1
do	do	k7c2
školy	škola	k1gFnSc2
(	(	kIx(
<g/>
při	při	k7c6
stavbě	stavba	k1gFnSc6
byla	být	k5eAaImAgFnS
socha	socha	k1gFnSc1
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
dočasně	dočasně	k6eAd1
přemístěna	přemístěn	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
úpravě	úprava	k1gFnSc3
prostor	prostora	k1gFnPc2
před	před	k7c7
školou	škola	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
okolí	okolí	k1gNnSc6
sochy	socha	k1gFnSc2
proběhly	proběhnout	k5eAaPmAgFnP
sadové	sadový	k2eAgFnPc1d1
úpravy	úprava	k1gFnPc1
<g/>
,	,	kIx,
lavičky	lavička	k1gFnPc1
a	a	k8xC
chodníky	chodník	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
školního	školní	k2eAgInSc2d1
roku	rok	k1gInSc2
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zavedení	zavedení	k1gNnSc3
šestiletého	šestiletý	k2eAgNnSc2d1
studia	studio	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
nejprve	nejprve	k6eAd1
doplnilo	doplnit	k5eAaPmAgNnS
klasické	klasický	k2eAgNnSc4d1
studium	studium	k1gNnSc4
čtyřleté	čtyřletý	k2eAgNnSc4d1
a	a	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
jediným	jediný	k2eAgInSc7d1
typem	typ	k1gInSc7
studia	studio	k1gNnSc2
na	na	k7c6
Gymnáziu	gymnázium	k1gNnSc6
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgMnPc1d1
absolventi	absolvent	k1gMnPc1
čtyřletého	čtyřletý	k2eAgNnSc2d1
studia	studio	k1gNnSc2
maturovali	maturovat	k5eAaBmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
školního	školní	k2eAgInSc2d1
roku	rok	k1gInSc2
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
byla	být	k5eAaImAgFnS
rozšířena	rozšířit	k5eAaPmNgFnS
nabídka	nabídka	k1gFnSc1
volitelných	volitelný	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
,	,	kIx,
pro	pro	k7c4
předposlední	předposlední	k2eAgInSc4d1
ročník	ročník	k1gInSc4
studia	studio	k1gNnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
šest	šest	k4xCc4
hodin	hodina	k1gFnPc2
týdně	týdně	k6eAd1
a	a	k8xC
v	v	k7c6
maturitním	maturitní	k2eAgInSc6d1
ročníku	ročník	k1gInSc6
o	o	k7c6
hodin	hodina	k1gFnPc2
dvanáct	dvanáct	k4xCc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
učebnách	učebna	k1gFnPc6
jsou	být	k5eAaImIp3nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
počítače	počítač	k1gInSc2
<g/>
,	,	kIx,
dataprojektory	dataprojektor	k1gInPc4
nebo	nebo	k8xC
interaktivní	interaktivní	k2eAgFnPc4d1
tabule	tabule	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaveden	zaveden	k2eAgInSc1d1
byl	být	k5eAaImAgInS
také	také	k9
elektronický	elektronický	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nahradil	nahradit	k5eAaPmAgInS
papírovou	papírový	k2eAgFnSc4d1
třídní	třídní	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
rekonstrukce	rekonstrukce	k1gFnSc2
budovy	budova	k1gFnSc2
školy	škola	k1gFnSc2
přišla	přijít	k5eAaPmAgFnS
na	na	k7c4
radu	rada	k1gFnSc4
také	také	k9
výměna	výměna	k1gFnSc1
oken	okno	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
dokončena	dokončen	k2eAgFnSc1d1
roku	rok	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
školním	školní	k2eAgInSc6d1
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
gymnázium	gymnázium	k1gNnSc4
oslavilo	oslavit	k5eAaPmAgNnS
čtyřicáté	čtyřicátý	k4xOgNnSc1
výročí	výročí	k1gNnSc1
vzniku	vznik	k1gInSc2
<g/>
,	,	kIx,
studovalo	studovat	k5eAaImAgNnS
ve	v	k7c6
škole	škola	k1gFnSc6
v	v	k7c6
osmnácti	osmnáct	k4xCc6
třídách	třída	k1gFnPc6
celkem	celkem	k6eAd1
536	#num#	k4
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
vyučovalo	vyučovat	k5eAaImAgNnS
46	#num#	k4
interních	interní	k2eAgMnPc2d1
učitelů	učitel	k1gMnPc2
a	a	k8xC
4	#num#	k4
externí	externí	k2eAgMnPc1d1
učitelé	učitel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
Český	český	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
Za	za	k7c4
dobu	doba	k1gFnSc4
existence	existence	k1gFnSc2
školy	škola	k1gFnSc2
prošla	projít	k5eAaPmAgFnS
výuka	výuka	k1gFnSc1
i	i	k9
odborné	odborný	k2eAgFnPc4d1
učebny	učebna	k1gFnPc4
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
řadou	řada	k1gFnSc7
podstatných	podstatný	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
jaký	jaký	k9
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Na	na	k7c6
škole	škola	k1gFnSc6
jsou	být	k5eAaImIp3nP
3	#num#	k4
učebny	učebna	k1gFnPc4
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dějepis	dějepis	k1gInSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
se	se	k3xPyFc4
družstvo	družstvo	k1gNnSc1
gymnázia	gymnázium	k1gNnSc2
účastní	účastnit	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc2d1
dějepisné	dějepisný	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
gymnázií	gymnázium	k1gNnPc2
v	v	k7c6
Chebu	Cheb	k1gInSc6
<g/>
,	,	kIx,
zaměřené	zaměřený	k2eAgInPc4d1
na	na	k7c4
české	český	k2eAgFnPc4d1
a	a	k8xC
slovenské	slovenský	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepšího	dobrý	k2eAgInSc2d3
výsledku	výsledek	k1gInSc2
gymnázium	gymnázium	k1gNnSc1
dosáhlo	dosáhnout	k5eAaPmAgNnS
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
tým	tým	k1gInSc1
gymnázia	gymnázium	k1gNnSc2
obsadil	obsadit	k5eAaPmAgInS
3	#num#	k4
<g/>
.	.	kIx.
příčku	příčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Zeměpis	zeměpis	k1gInSc1
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
se	se	k3xPyFc4
zeměpis	zeměpis	k1gInSc1
vyučuje	vyučovat	k5eAaImIp3nS
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
primě	prima	k1gFnSc6
<g/>
,	,	kIx,
sekundě	sekunda	k1gFnSc6
<g/>
,	,	kIx,
tercii	tercie	k1gFnSc6
a	a	k8xC
kvartě	kvarta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zájemce	zájemce	k1gMnPc4
ve	v	k7c6
vyšších	vysoký	k2eAgInPc6d2
ročnících	ročník	k1gInPc6
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
otevřen	otevřen	k2eAgInSc1d1
seminář	seminář	k1gInSc1
ze	z	k7c2
zeměpisu	zeměpis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
škole	škola	k1gFnSc6
je	být	k5eAaImIp3nS
pro	pro	k7c4
výuku	výuka	k1gFnSc4
zeměpisu	zeměpis	k1gInSc2
vybavena	vybavit	k5eAaPmNgFnS
učebna	učebna	k1gFnSc1
(	(	kIx(
<g/>
dataprojektor	dataprojektor	k1gInSc1
<g/>
,	,	kIx,
interaktivní	interaktivní	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
a	a	k8xC
možnost	možnost	k1gFnSc1
výuky	výuka	k1gFnSc2
nabízí	nabízet	k5eAaImIp3nS
i	i	k9
multimediální	multimediální	k2eAgFnSc1d1
učebna	učebna	k1gFnSc1
–	–	k?
hlavně	hlavně	k9
pro	pro	k7c4
výuku	výuka	k1gFnSc4
GISu	GISus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenti	student	k1gMnPc1
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
zúčastňují	zúčastňovat	k5eAaImIp3nP
zeměpisné	zeměpisný	k2eAgFnPc1d1
olympiády	olympiáda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Matematika	matematika	k1gFnSc1
</s>
<s>
Téměř	téměř	k6eAd1
20	#num#	k4
let	léto	k1gNnPc2
škola	škola	k1gFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
organizaci	organizace	k1gFnSc4
krajských	krajský	k2eAgFnPc2d1
kol	kola	k1gFnPc2
Matematické	matematický	k2eAgFnSc2d1
olympiády	olympiáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Biologie	biologie	k1gFnSc1
</s>
<s>
Mezi	mezi	k7c4
soutěže	soutěž	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgFnPc6
je	být	k5eAaImIp3nS
úspěch	úspěch	k1gInSc1
tradičně	tradičně	k6eAd1
nejvíce	nejvíce	k6eAd1,k6eAd3
ceněn	cenit	k5eAaImNgMnS
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
biologická	biologický	k2eAgFnSc1d1
olympiáda	olympiáda	k1gFnSc1
kat	kat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	A	kA
<g/>
,	,	kIx,
B	B	kA
<g/>
,	,	kIx,
C.	C.	kA
V	v	k7c6
soutěži	soutěž	k1gFnSc6
Přírodovědný	přírodovědný	k2eAgMnSc1d1
klokan	klokan	k1gMnSc1
získali	získat	k5eAaPmAgMnP
studenti	student	k1gMnPc1
úspěchy	úspěch	k1gInPc7
na	na	k7c6
celostátní	celostátní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Na	na	k7c6
národní	národní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
studenti	student	k1gMnPc1
obsazovali	obsazovat	k5eAaImAgMnP
čelné	čelný	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
i	i	k9
v	v	k7c6
soutěži	soutěž	k1gFnSc6
KRNAPu	KRNAPa	k1gFnSc4
pro	pro	k7c4
mladé	mladý	k2eAgMnPc4d1
přírodovědce	přírodovědec	k1gMnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
opakovaně	opakovaně	k6eAd1
získali	získat	k5eAaPmAgMnP
umístění	umístění	k1gNnSc4
ve	v	k7c6
zlatém	zlatý	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opakovaně	opakovaně	k6eAd1
soutěžili	soutěžit	k5eAaImAgMnP
v	v	k7c6
ekologické	ekologický	k2eAgFnSc6d1
olympiádě	olympiáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
projektu	projekt	k1gInSc6
Naturnet	Naturnet	k1gInSc1
v	v	k7c6
rámci	rámec	k1gInSc6
programu	program	k1gInSc2
pro	pro	k7c4
vědu	věda	k1gFnSc4
a	a	k8xC
výzkum	výzkum	k1gInSc4
(	(	kIx(
<g/>
v	v	k7c6
letech	let	k1gInPc6
2002	#num#	k4
–	–	k?
2006	#num#	k4
<g/>
)	)	kIx)
studenti	student	k1gMnPc1
prokázali	prokázat	k5eAaPmAgMnP
schopnost	schopnost	k1gFnSc4
týmové	týmový	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
propojení	propojení	k1gNnSc1
biologických	biologický	k2eAgFnPc2d1
znalostí	znalost	k1gFnPc2
s	s	k7c7
jazykovými	jazykový	k2eAgInPc7d1
a	a	k8xC
IT	IT	kA
technologiemi	technologie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Fyzika	fyzika	k1gFnSc1
</s>
<s>
Studenti	student	k1gMnPc1
gymnázia	gymnázium	k1gNnSc2
se	se	k3xPyFc4
pravidelně	pravidelně	k6eAd1
účastní	účastnit	k5eAaImIp3nP
Fyzikální	fyzikální	k2eAgFnPc1d1
olympiády	olympiáda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
se	se	k3xPyFc4
ve	v	k7c6
výuce	výuka	k1gFnSc6
uplatňují	uplatňovat	k5eAaImIp3nP
divadelní	divadelní	k2eAgFnPc1d1
techniky	technika	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
skupina	skupina	k1gFnSc1
francouzského	francouzský	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
vedená	vedený	k2eAgFnSc1d1
francouzskou	francouzský	k2eAgFnSc7d1
lektorkou	lektorka	k1gFnSc7
Brunhilde	Brunhilde	k1gFnSc1
Gendras	Gendras	k1gFnSc1
uvedla	uvést	k5eAaPmAgFnS
na	na	k7c6
Festivadle	Festivadlo	k1gNnSc6
Brno	Brno	k1gNnSc1
1999	#num#	k4
La	la	k1gNnPc2
Fontainovy	Fontainův	k2eAgFnPc1d1
bajky	bajka	k1gFnPc1
<g/>
,	,	kIx,
získala	získat	k5eAaPmAgFnS
první	první	k4xOgFnSc4
cenu	cena	k1gFnSc4
a	a	k8xC
reprezentovala	reprezentovat	k5eAaImAgFnS
ČR	ČR	kA
v	v	k7c4
La	la	k1gNnSc6
Roche-sur	Roche-sur	k1gNnSc6
Yon	Yon	k1gNnSc6
na	na	k7c6
Festivale	festival	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
byla	být	k5eAaImAgFnS
skupině	skupina	k1gFnSc3
Atelier	atelier	k1gNnSc7
Théâtre	Théâtr	k1gInSc5
Gybon	Gybona	k1gFnPc2
udělena	udělit	k5eAaPmNgFnS
prestižní	prestižní	k2eAgFnSc1d1
evropská	evropský	k2eAgFnSc1d1
jazyková	jazykový	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Label	Labela	k1gFnPc2
za	za	k7c4
projekt	projekt	k1gInSc4
Hrát	hrát	k5eAaImF
francouzský	francouzský	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Dodnes	dodnes	k6eAd1
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Atelier	atelier	k1gNnSc2
Théâtre	Théâtr	k1gInSc5
Gybon	Gybon	k1gInSc4
uvedl	uvést	k5eAaPmAgMnS
na	na	k7c4
šedesát	šedesát	k4xCc4
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Tvůrčí	tvůrčí	k2eAgNnSc1d1
psaní	psaní	k1gNnSc1
</s>
<s>
Seminář	seminář	k1gInSc1
tvůrčího	tvůrčí	k2eAgNnSc2d1
psaní	psaní	k1gNnSc2
byl	být	k5eAaImAgInS
na	na	k7c6
Gymnáziu	gymnázium	k1gNnSc6
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
založen	založit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
a	a	k8xC
prošla	projít	k5eAaPmAgFnS
jím	jíst	k5eAaImIp1nS
řada	řada	k1gFnSc1
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimiž	jenž	k3xRgNnPc7
najdeme	najít	k5eAaPmIp1nP
i	i	k9
dnes	dnes	k6eAd1
již	již	k6eAd1
úspěšné	úspěšný	k2eAgMnPc4d1
autory	autor	k1gMnPc4
(	(	kIx(
<g/>
Ondrej	Ondrej	k1gMnSc1
Macl	Macl	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Kořínek	Kořínek	k1gMnSc1
<g/>
,	,	kIx,
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
semináře	seminář	k1gInSc2
vychází	vycházet	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
almanachy	almanach	k1gInPc1
studentských	studentský	k2eAgMnPc2d1
textů	text	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
na	na	k7c6
hodinách	hodina	k1gFnPc6
semináře	seminář	k1gInSc2
vznikaly	vznikat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2010	#num#	k4
a	a	k8xC
2011	#num#	k4
vyhrál	vyhrát	k5eAaPmAgInS
školní	školní	k2eAgInSc1d1
internetový	internetový	k2eAgInSc1d1
časopis	časopis	k1gInSc1
Punkl	Punkl	k1gInSc1
celostátní	celostátní	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
Časopis	časopis	k1gInSc4
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
pořádá	pořádat	k5eAaImIp3nS
škola	škola	k1gFnSc1
seminář	seminář	k1gInSc4
pro	pro	k7c4
studenty	student	k1gMnPc4
i	i	k9
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
Setkání	setkání	k1gNnSc2
s	s	k7c7
dobrým	dobrý	k2eAgInSc7d1
čajem	čaj	k1gInSc7
a	a	k8xC
s	s	k7c7
literaturou	literatura	k1gFnSc7
aneb	aneb	k?
Gybon	Gybona	k1gFnPc2
Tea	Tea	k1gFnSc1
Party	party	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
je	být	k5eAaImIp3nS
akce	akce	k1gFnSc1
zařazena	zařazen	k2eAgFnSc1d1
do	do	k7c2
programu	program	k1gInSc2
Východočeského	východočeský	k2eAgInSc2d1
uměleckého	umělecký	k2eAgInSc2d1
maratonu	maraton	k1gInSc2
Střediska	středisko	k1gNnSc2
východočeských	východočeský	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
spolupráci	spolupráce	k1gFnSc3
se	s	k7c7
Střediskem	středisko	k1gNnSc7
východočeských	východočeský	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
mají	mít	k5eAaImIp3nP
studenti	student	k1gMnPc1
možnost	možnost	k1gFnSc4
vystoupit	vystoupit	k5eAaPmF
také	také	k9
na	na	k7c6
dalších	další	k2eAgFnPc6d1
akcích	akce	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
kde	kde	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Hospodářské	hospodářský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
titul	titul	k1gInSc1
<g/>
=	=	kIx~
<g/>
Hledání	hledání	k1gNnSc1
účinných	účinný	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
vyučování	vyučování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nejlepší	dobrý	k2eAgInSc4d3
školní	školní	k2eAgInSc4d1
časopis	časopis	k1gInSc4
na	na	k7c6
webu	web	k1gInSc6
má	mít	k5eAaImIp3nS
královéhradecké	královéhradecký	k2eAgNnSc1d1
gymnázium	gymnázium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Středisko	středisko	k1gNnSc1
východočeských	východočeský	k2eAgMnPc2d1
spisovatelů	spisovatel	k1gMnPc2
<g/>
:	:	kIx,
22	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
od	od	k7c2
15.00	15.00	k4
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
studenti	student	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
kdokoliv	kdokoliv	k3yInSc1
z	z	k7c2
veřejnosti	veřejnost	k1gFnSc2
setkat	setkat	k5eAaPmF
na	na	k7c6
Gymnáziu	gymnázium	k1gNnSc6
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
<g/>
...	...	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gymnázium	gymnázium	k1gNnSc1
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
128316525	#num#	k4
</s>
