<p>
<s>
Soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
chemické	chemický	k2eAgFnPc1d1	chemická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
iontového	iontový	k2eAgInSc2d1	iontový
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kladnou	kladný	k2eAgFnSc4d1	kladná
(	(	kIx(	(
<g/>
kationty	kation	k1gInPc7	kation
<g/>
)	)	kIx)	)
i	i	k8xC	i
zápornou	záporný	k2eAgFnSc4d1	záporná
složku	složka	k1gFnSc4	složka
(	(	kIx(	(
<g/>
anionty	anion	k1gInPc4	anion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
celá	celý	k2eAgFnSc1d1	celá
sloučenina	sloučenina	k1gFnSc1	sloučenina
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgFnSc1d1	neutrální
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
složky	složka	k1gFnPc1	složka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
organické	organický	k2eAgInPc4d1	organický
i	i	k8xC	i
anorganické	anorganický	k2eAgNnSc4d1	anorganické
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc4	jeden
i	i	k8xC	i
víceatomové	víceatomový	k2eAgInPc4d1	víceatomový
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
pevné	pevný	k2eAgFnSc2d1	pevná
krystalické	krystalický	k2eAgFnSc2d1	krystalická
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
a	a	k8xC	a
tavenině	tavenina	k1gFnSc6	tavenina
vedou	vést	k5eAaImIp3nP	vést
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
Soli	sůl	k1gFnPc1	sůl
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
soli	sůl	k1gFnPc4	sůl
bezkyslíkatých	bezkyslíkatý	k2eAgFnPc2d1	bezkyslíkatá
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
soli	sůl	k1gFnSc2	sůl
kyslíkatých	kyslíkatý	k2eAgFnPc2d1	kyslíkatá
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
atomy	atom	k1gInPc1	atom
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgNnSc2	tento
dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
názvosloví	názvosloví	k1gNnSc1	názvosloví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Názvosloví	názvosloví	k1gNnSc2	názvosloví
===	===	k?	===
</s>
</p>
<p>
<s>
Soli	sůl	k1gFnPc1	sůl
bezkyslíkatých	bezkyslíkatý	k2eAgFnPc2d1	bezkyslíkatá
kyselin	kyselina	k1gFnPc2	kyselina
mají	mít	k5eAaImIp3nP	mít
koncovku	koncovka	k1gFnSc4	koncovka
"	"	kIx"	"
<g/>
-id	d	k?	-id
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kys	kys	k?	kys
<g/>
.	.	kIx.	.
jodovodíková	jodovodíkový	k2eAgFnSc1d1	jodovodíková
⟶	⟶	k?	⟶
jodid	jodid	k1gInSc1	jodid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soli	sůl	k1gFnPc1	sůl
kyslíkatých	kyslíkatý	k2eAgFnPc2d1	kyslíkatá
kyselin	kyselina	k1gFnPc2	kyselina
mají	mít	k5eAaImIp3nP	mít
koncovku	koncovka	k1gFnSc4	koncovka
podle	podle	k7c2	podle
kyseliny	kyselina	k1gFnSc2	kyselina
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
názvu	název	k1gInSc2	název
kyseliny	kyselina	k1gFnSc2	kyselina
odebereme	odebrat	k5eAaPmIp1nP	odebrat
koncovku	koncovka	k1gFnSc4	koncovka
přídavného	přídavný	k2eAgNnSc2d1	přídavné
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
připojíme	připojit	k5eAaPmIp1nP	připojit
příponu	přípona	k1gFnSc4	přípona
"	"	kIx"	"
<g/>
-an	n	k?	-an
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
soli	sůl	k1gFnPc1	sůl
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
atom	atom	k1gInSc4	atom
v	v	k7c6	v
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stupni	stupeň	k1gInSc6	stupeň
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
solí	sůl	k1gFnPc2	sůl
je	být	k5eAaImIp3nS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
celá	celý	k2eAgFnSc1d1	celá
přípona	přípona	k1gFnSc1	přípona
"	"	kIx"	"
<g/>
-ová	vá	k1gFnSc1	-ová
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
slovo	slovo	k1gNnSc1	slovo
názvu	název	k1gInSc2	název
soli	sůl	k1gFnSc2	sůl
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
kovovou	kovový	k2eAgFnSc4d1	kovová
část	část	k1gFnSc4	část
soli	sůl	k1gFnSc2	sůl
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
oxidačním	oxidační	k2eAgInSc7d1	oxidační
stupněm	stupeň	k1gInSc7	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PříkladyKyselina	PříkladyKyselina	k1gFnSc1	PříkladyKyselina
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
HCl	HCl	k?	HCl
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
HCl	HCl	k1gMnSc1	HCl
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
⟶	⟶	k?	⟶
chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
NaCl	NaCl	k1gMnSc1	NaCl
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
NaCl	NaCl	k1gMnSc1	NaCl
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
HNO	HNO	kA	HNO
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
HNO	HNO	kA	HNO
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
⟶	⟶	k?	⟶
dusičnan	dusičnan	k1gInSc1	dusičnan
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
NaNO	NaNO	k?	NaNO
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
NaNO	NaNO	k1gFnSc1	NaNO
<g/>
3	[number]	k4	3
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
SO	So	kA	So
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
⟶	⟶	k?	⟶
síran	síran	k1gInSc1	síran
sodný	sodný	k2eAgInSc1d1	sodný
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
SO	So	kA	So
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
}}}}	}}}}	k?	}}}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Soli	sůl	k1gFnPc1	sůl
vznikají	vznikat	k5eAaImIp3nP	vznikat
redoxními	redoxní	k2eAgFnPc7d1	redoxní
i	i	k8xC	i
jinými	jiný	k2eAgFnPc7d1	jiná
reakcemi	reakce	k1gFnPc7	reakce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
neutralizací	neutralizace	k1gFnPc2	neutralizace
(	(	kIx(	(
<g/>
reakcí	reakce	k1gFnPc2	reakce
kyseliny	kyselina	k1gFnSc2	kyselina
a	a	k8xC	a
zásady	zásada	k1gFnSc2	zásada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
KOH	KOH	kA	KOH
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
HCl	HCl	k?	HCl
</s>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
KCl	KCl	k?	KCl
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
KOH	KOH	kA	KOH
+	+	kIx~	+
HCl	HCl	k1gMnSc1	HCl
-	-	kIx~	-
<g/>
>	>	kIx)	>
H2O	H2O	k1gMnSc1	H2O
+	+	kIx~	+
KCl	KCl	k1gMnSc1	KCl
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
reakcí	reakce	k1gFnPc2	reakce
kovu	kov	k1gInSc2	kov
s	s	k7c7	s
nekovem	nekov	k1gInSc7	nekov
</s>
</p>
<p>
<s>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
Cl	Cl	k?	Cl
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
NaCl	NaCl	k1gMnSc1	NaCl
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
Na	na	k7c4	na
+	+	kIx~	+
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
-	-	kIx~	-
<g/>
>	>	kIx)	>
2	[number]	k4	2
<g/>
NaCl	NaClum	k1gNnPc2	NaClum
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
reakcí	reakce	k1gFnPc2	reakce
kovu	kov	k1gInSc2	kov
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
</s>
</p>
<p>
<s>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Zn	zn	kA	zn
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
SO	So	kA	So
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
</p>
<p>
<s>
ZnSO	ZnSO	k?	ZnSO
</s>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
Zn	zn	kA	zn
+	+	kIx~	+
H2SO4	H2SO4	k1gMnSc1	H2SO4
-	-	kIx~	-
<g/>
>	>	kIx)	>
ZnSO	ZnSO	k1gFnSc1	ZnSO
<g/>
4	[number]	k4	4
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
reakcí	reakce	k1gFnPc2	reakce
hydroxidu	hydroxid	k1gInSc2	hydroxid
s	s	k7c7	s
kyselinotvorným	kyselinotvorný	k2eAgInSc7d1	kyselinotvorný
oxidem	oxid	k1gInSc7	oxid
</s>
</p>
<p>
<s>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Ca	ca	kA	ca
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
OH	OH	kA	OH
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
CO	co	k3yInSc1	co
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
</p>
<p>
<s>
CaCO	CaCO	k?	CaCO
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
CO2	CO2	k1gFnSc1	CO2
-	-	kIx~	-
<g/>
>	>	kIx)	>
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
reakcí	reakce	k1gFnSc7	reakce
zásadotvorných	zásadotvorný	k2eAgInPc2d1	zásadotvorný
oxidů	oxid	k1gInPc2	oxid
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
</s>
</p>
<p>
<s>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
CaO	CaO	k?	CaO
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
HNO	HNO	kA	HNO
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
<s>
Ca	ca	kA	ca
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
NO	no	k9	no
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
CaO	CaO	k1gMnSc1	CaO
+	+	kIx~	+
2HNO3	[number]	k4	2HNO3
-	-	kIx~	-
<g/>
>	>	kIx)	>
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
reakcí	reakce	k1gFnSc7	reakce
zásadotvorného	zásadotvorný	k2eAgMnSc2d1	zásadotvorný
a	a	k8xC	a
kyselinotvorného	kyselinotvorný	k2eAgInSc2d1	kyselinotvorný
oxidu	oxid	k1gInSc2	oxid
</s>
</p>
<p>
<s>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
CaO	CaO	k?	CaO
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
CO	co	k3yQnSc1	co
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
</p>
<p>
<s>
CaCO	CaCO	k?	CaCO
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
CaO	CaO	k1gMnSc1	CaO
+	+	kIx~	+
CO2	CO2	k1gMnSc1	CO2
-	-	kIx~	-
<g/>
>	>	kIx)	>
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
srážením	srážení	k1gNnSc7	srážení
(	(	kIx(	(
<g/>
reakcí	reakce	k1gFnPc2	reakce
dvou	dva	k4xCgFnPc2	dva
solí	sůl	k1gFnPc2	sůl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Pb	Pb	k?	Pb
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
NO	no	k9	no
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
⟶	⟶	k?	⟶
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
NaNO	NaNO	k?	NaNO
</s>
</p>
<p>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
PbS	PbS	k?	PbS
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
ce	ce	k?	ce
{	{	kIx(	{
<g/>
Pb	Pb	k1gMnSc1	Pb
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
+	+	kIx~	+
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
-	-	kIx~	-
<g/>
>	>	kIx)	>
2	[number]	k4	2
<g/>
NaNO	NaNO	k1gFnSc1	NaNO
<g/>
3	[number]	k4	3
+	+	kIx~	+
PbS	PbS	k1gFnSc2	PbS
<g/>
}}}	}}}	k?	}}}
</s>
</p>
