<s>
Expedice	expedice	k1gFnSc1
12	#num#	k4
</s>
<s>
Expedice	expedice	k1gFnSc1
12	#num#	k4
</s>
<s>
Znak	znak	k1gInSc1
expedice	expedice	k1gFnSc2
</s>
<s>
Údaje	údaj	k1gInPc1
o	o	k7c6
expedici	expedice	k1gFnSc6
</s>
<s>
Na	na	k7c6
stanici	stanice	k1gFnSc6
</s>
<s>
ISS	ISS	kA
</s>
<s>
Loď	loď	k1gFnSc1
</s>
<s>
Sojuz	Sojuz	k1gInSc1
TMA-7	TMA-7	k1gFnSc2
</s>
<s>
Členů	člen	k1gInPc2
expedice	expedice	k1gFnSc2
</s>
<s>
2	#num#	k4
</s>
<s>
Přijaté	přijatý	k2eAgFnPc1d1
návštěvy	návštěva	k1gFnPc1
</s>
<s>
STS-114	STS-114	k4
</s>
<s>
Zásobovací	zásobovací	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
</s>
<s>
Progress	Progress	k1gInSc1
M-	M-	k1gFnSc1
<g/>
54	#num#	k4
<g/>
,	,	kIx,
Progress	Progress	k1gInSc1
M-55	M-55	k1gFnSc2
</s>
<s>
Délka	délka	k1gFnSc1
výstupů	výstup	k1gInPc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
</s>
<s>
5	#num#	k4
hodin	hodina	k1gFnPc2
22	#num#	k4
minuta	minut	k2eAgFnSc1d1
5	#num#	k4
hodin	hodina	k1gFnPc2
43	#num#	k4
minut	minuta	k1gFnPc2
</s>
<s>
Datum	datum	k1gNnSc1
startu	start	k1gInSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2005	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
55	#num#	k4
UTC	UTC	kA
</s>
<s>
Kosmodrom	kosmodrom	k1gInSc1
</s>
<s>
Bajkonur	Bajkonur	k1gMnSc1
</s>
<s>
Startovací	startovací	k2eAgFnSc1d1
loď	loď	k1gFnSc1
</s>
<s>
Sojuz	Sojuz	k1gInSc1
TMA-7	TMA-7	k1gFnSc2
</s>
<s>
Délka	délka	k1gFnSc1
letu	let	k1gInSc2
</s>
<s>
189	#num#	k4
dní	den	k1gInPc2
19	#num#	k4
hodin	hodina	k1gFnPc2
52	#num#	k4
minut	minuta	k1gFnPc2
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
na	na	k7c4
stanici	stanice	k1gFnSc4
</s>
<s>
187	#num#	k4
dní	den	k1gInPc2
14	#num#	k4
hodin	hodina	k1gFnPc2
19	#num#	k4
minut	minuta	k1gFnPc2
</s>
<s>
Datum	datum	k1gNnSc1
přistání	přistání	k1gNnSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
<g/>
:	:	kIx,
<g/>
48	#num#	k4
UTC	UTC	kA
</s>
<s>
Přistávací	přistávací	k2eAgFnSc1d1
loď	loď	k1gFnSc1
</s>
<s>
Sojuz	Sojuz	k1gInSc1
TMA-7	TMA-7	k1gFnSc2
</s>
<s>
Fotografie	fotografia	k1gFnPc1
posádky	posádka	k1gFnSc2
</s>
<s>
velitel	velitel	k1gMnSc1
expedice	expedice	k1gFnSc2
William	William	k1gInSc1
McArthur	McArthur	k1gMnSc1
a	a	k8xC
Valerij	Valerij	k1gMnSc1
Tokarev	Tokarev	k1gFnSc4
</s>
<s>
Navigace	navigace	k1gFnSc1
</s>
<s>
Předcházející	předcházející	k2eAgInPc1d1
</s>
<s>
Následující	následující	k2eAgInSc1d1
</s>
<s>
Expedice	expedice	k1gFnSc1
11	#num#	k4
</s>
<s>
Expedice	expedice	k1gFnSc1
13	#num#	k4
</s>
<s>
Expedice	expedice	k1gFnSc1
12	#num#	k4
byla	být	k5eAaImAgFnS
dvanáctá	dvanáctý	k4xOgFnSc1
posádka	posádka	k1gFnSc1
dlouhodobě	dlouhodobě	k6eAd1
obývající	obývající	k2eAgFnSc4d1
Mezinárodní	mezinárodní	k2eAgFnSc4d1
vesmírnou	vesmírný	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
(	(	kIx(
<g/>
ISS	ISS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velitel	velitel	k1gMnSc1
William	William	k1gInSc1
McArthur	McArthur	k1gMnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
a	a	k8xC
palubní	palubní	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
Valerij	Valerij	k1gMnSc1
Tokarev	Tokarev	k1gFnSc1
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
startovali	startovat	k5eAaBmAgMnP
z	z	k7c2
kosmodromu	kosmodrom	k1gInSc2
Bajkonur	Bajkonura	k1gFnPc2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2005	#num#	k4
na	na	k7c6
palubě	paluba	k1gFnSc6
Sojuzu	Sojuz	k1gInSc2
TMA-	TMA-	k1gFnSc2
<g/>
7	#num#	k4
<g/>
,	,	kIx,
se	s	k7c7
stanicí	stanice	k1gFnSc7
ISS	ISS	kA
se	se	k3xPyFc4
spojili	spojit	k5eAaPmAgMnP
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2005	#num#	k4
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stanici	stanice	k1gFnSc6
přijali	přijmout	k5eAaPmAgMnP
zásobovací	zásobovací	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Progress	Progressa	k1gFnPc2
a	a	k8xC
uskutečnili	uskutečnit	k5eAaPmAgMnP
dva	dva	k4xCgInPc4
výstupy	výstup	k1gInPc4
do	do	k7c2
kosmického	kosmický	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
půlročním	půlroční	k2eAgInSc6d1
pobytu	pobyt	k1gInSc6
předali	předat	k5eAaPmAgMnP
stanici	stanice	k1gFnSc4
Expedici	expedice	k1gFnSc4
13	#num#	k4
a	a	k8xC
přistáli	přistát	k5eAaPmAgMnP,k5eAaImAgMnP
na	na	k7c4
Zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Posádka	posádka	k1gFnSc1
</s>
<s>
William	William	k1gInSc1
McArthur	McArthura	k1gFnPc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
velitel	velitel	k1gMnSc1
–	–	k?
NASA	NASA	kA
</s>
<s>
Valerij	Valerít	k5eAaPmRp2nS
Tokarev	Tokarev	k1gFnSc1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
palubní	palubní	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
–	–	k?
Roskosmos	Roskosmos	k1gMnSc1
(	(	kIx(
<g/>
CPK	CPK	kA
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
závorkách	závorka	k1gFnPc6
je	být	k5eAaImIp3nS
uveden	uveden	k2eAgInSc1d1
dosavadní	dosavadní	k2eAgInSc1d1
počet	počet	k1gInSc1
letů	let	k1gInPc2
do	do	k7c2
vesmíru	vesmír	k1gInSc2
včetně	včetně	k7c2
této	tento	k3xDgFnSc2
mise	mise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Záložní	záložní	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
</s>
<s>
Jeffrey	Jeffrea	k1gFnPc1
Williams	Williamsa	k1gFnPc2
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
–	–	k?
NASA	NASA	kA
</s>
<s>
Michail	Michail	k1gMnSc1
Ťurin	Ťurin	k1gMnSc1
<g/>
,	,	kIx,
palubní	palubní	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
–	–	k?
Roskosmos	Roskosmos	k1gMnSc1
(	(	kIx(
<g/>
RKK	RKK	kA
Eněrgija	Eněrgija	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Průběh	průběh	k1gInSc1
mise	mise	k1gFnSc2
</s>
<s>
Expedice	expedice	k1gFnSc1
12	#num#	k4
startovala	startovat	k5eAaBmAgFnS
společně	společně	k6eAd1
s	s	k7c7
vesmírným	vesmírný	k2eAgMnSc7d1
turistou	turista	k1gMnSc7
Gregory	Gregor	k1gMnPc4
Olsenem	Olsen	k1gMnSc7
v	v	k7c6
Sojuzu	Sojuz	k1gInSc6
TMA-7	TMA-7	k1gFnSc2
z	z	k7c2
kosmodromu	kosmodrom	k1gInSc2
Bajkonur	Bajkonura	k1gFnPc2
v	v	k7c6
Kazachstánu	Kazachstán	k1gInSc6
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2005	#num#	k4
v	v	k7c4
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
55	#num#	k4
UTC	UTC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
vesmírné	vesmírný	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
přistáli	přistát	k5eAaPmAgMnP,k5eAaImAgMnP
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
v	v	k7c6
7	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
UTC	UTC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novou	nový	k2eAgFnSc4d1
posádku	posádka	k1gFnSc4
přivítali	přivítat	k5eAaPmAgMnP
členové	člen	k1gMnPc1
Expedice	expedice	k1gFnSc2
11	#num#	k4
<g/>
:	:	kIx,
ruský	ruský	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
Sergej	Sergej	k1gMnSc1
Krikaljov	Krikaljovo	k1gNnPc2
a	a	k8xC
americký	americký	k2eAgMnSc1d1
palubní	palubní	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
John	John	k1gMnSc1
Phillips	Phillips	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
týdne	týden	k1gInSc2
nová	nový	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
převzala	převzít	k5eAaPmAgFnS
stanici	stanice	k1gFnSc4
a	a	k8xC
11	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
se	se	k3xPyFc4
Krikaljov	Krikaljov	k1gInSc1
<g/>
,	,	kIx,
Phillips	Phillips	k1gInSc1
a	a	k8xC
Olsen	Olsen	k1gInSc1
v	v	k7c6
Sojuzu	Sojuz	k1gInSc6
TMA-6	TMA-6	k1gMnSc4
vrátili	vrátit	k5eAaPmAgMnP
na	na	k7c4
zem	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
ze	z	k7c2
dvou	dva	k4xCgInPc2
výstupů	výstup	k1gInPc2
do	do	k7c2
kosmického	kosmický	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
expedice	expedice	k1gFnSc2
proběhl	proběhnout	k5eAaPmAgInS
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
,	,	kIx,
kosmonauti	kosmonaut	k1gMnPc1
se	se	k3xPyFc4
věnovali	věnovat	k5eAaPmAgMnP,k5eAaImAgMnP
výměnám	výměna	k1gFnPc3
některých	některý	k3yIgInPc2
dílů	díl	k1gInPc2
na	na	k7c6
povrchu	povrch	k1gInSc6
stanice	stanice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vycházka	vycházka	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
5	#num#	k4
hodin	hodina	k1gFnPc2
22	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
kosmonauti	kosmonaut	k1gMnPc1
vykládali	vykládat	k5eAaImAgMnP
zásobovací	zásobovací	k2eAgFnSc4d1
loď	loď	k1gFnSc4
Progress	Progressa	k1gFnPc2
M-	M-	k1gMnPc2
<g/>
55	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zakotvila	zakotvit	k5eAaPmAgFnS
u	u	k7c2
stanice	stanice	k1gFnSc2
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
kosmická	kosmický	k2eAgFnSc1d1
vycházka	vycházka	k1gFnSc1
McArthura	McArthura	k1gFnSc1
a	a	k8xC
Tokareva	Tokareva	k1gFnSc1
začala	začít	k5eAaPmAgFnS
3	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2006	#num#	k4
v	v	k7c4
22	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
UTC	UTC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kosmonauti	kosmonaut	k1gMnPc1
během	během	k7c2
5	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
43	#num#	k4
minut	minuta	k1gFnPc2
vypustili	vypustit	k5eAaPmAgMnP
starý	starý	k2eAgInSc4d1
skafandr	skafandr	k1gInSc4
s	s	k7c7
vysílačkou	vysílačka	k1gFnSc7
<g/>
,	,	kIx,
obsloužili	obsloužit	k5eAaPmAgMnP
venkovní	venkovní	k2eAgInPc4d1
experimenty	experiment	k1gInPc4
a	a	k8xC
prohlédli	prohlédnout	k5eAaPmAgMnP
vybrané	vybraný	k2eAgFnPc4d1
části	část	k1gFnPc4
povrchu	povrch	k1gInSc2
stanice	stanice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2006	#num#	k4
přiletěli	přiletět	k5eAaPmAgMnP
v	v	k7c6
Sojuzu	Sojuz	k1gInSc6
TMA-8	TMA-8	k1gFnSc2
Pavel	Pavel	k1gMnSc1
Vinogradov	Vinogradov	k1gInSc1
a	a	k8xC
Jeffrey	Jeffrea	k1gFnPc1
Williams	Williams	k1gInSc1
(	(	kIx(
<g/>
Expedice	expedice	k1gFnSc1
13	#num#	k4
<g/>
)	)	kIx)
s	s	k7c7
brazilským	brazilský	k2eAgMnSc7d1
kosmonautem	kosmonaut	k1gMnSc7
Marcosem	Marcos	k1gMnSc7
Pontesem	Pontes	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojice	dvojice	k1gFnSc1
McArthur	McArthura	k1gFnPc2
<g/>
,	,	kIx,
Tokarev	Tokarev	k1gFnSc1
předala	předat	k5eAaPmAgFnS
stanici	stanice	k1gFnSc3
nováčkům	nováček	k1gMnPc3
a	a	k8xC
s	s	k7c7
Pontesem	Pontes	k1gInSc7
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
v	v	k7c6
Sojuzu	Sojuz	k1gInSc6
TMA-7	TMA-7	k1gFnSc2
na	na	k7c4
zem	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Expedice	expedice	k1gFnSc2
12	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
HOLUB	Holub	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MEK	mek	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
kosmonautiky	kosmonautika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2006-04-09	2006-04-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Expedice	expedice	k1gFnSc2
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Expedice	expedice	k1gFnSc1
na	na	k7c4
Mezinárodní	mezinárodní	k2eAgFnSc4d1
vesmírnou	vesmírný	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
Základní	základní	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
</s>
<s>
Proběhlé	proběhlý	k2eAgNnSc1d1
</s>
<s>
Expedice	expedice	k1gFnSc1
1	#num#	k4
•	•	k?
2	#num#	k4
•	•	k?
3	#num#	k4
•	•	k?
4	#num#	k4
•	•	k?
5	#num#	k4
•	•	k?
6	#num#	k4
•	•	k?
7	#num#	k4
•	•	k?
8	#num#	k4
•	•	k?
9	#num#	k4
•	•	k?
10	#num#	k4
•	•	k?
11	#num#	k4
•	•	k?
12	#num#	k4
•	•	k?
13	#num#	k4
•	•	k?
14	#num#	k4
•	•	k?
15	#num#	k4
•	•	k?
16	#num#	k4
•	•	k?
17	#num#	k4
•	•	k?
18	#num#	k4
•	•	k?
19	#num#	k4
•	•	k?
20	#num#	k4
•	•	k?
21	#num#	k4
•	•	k?
22	#num#	k4
•	•	k?
23	#num#	k4
•	•	k?
24	#num#	k4
•	•	k?
25	#num#	k4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
26	#num#	k4
•	•	k?
27	#num#	k4
•	•	k?
28	#num#	k4
•	•	k?
29	#num#	k4
•	•	k?
30	#num#	k4
•	•	k?
31	#num#	k4
•	•	k?
32	#num#	k4
•	•	k?
33	#num#	k4
•	•	k?
34	#num#	k4
•	•	k?
35	#num#	k4
•	•	k?
36	#num#	k4
•	•	k?
37	#num#	k4
•	•	k?
38	#num#	k4
•	•	k?
39	#num#	k4
•	•	k?
40	#num#	k4
•	•	k?
41	#num#	k4
•	•	k?
42	#num#	k4
•	•	k?
43	#num#	k4
•	•	k?
44	#num#	k4
•	•	k?
45	#num#	k4
•	•	k?
46	#num#	k4
•	•	k?
47	#num#	k4
•	•	k?
48	#num#	k4
•	•	k?
49	#num#	k4
•	•	k?
50	#num#	k4
•	•	k?
51	#num#	k4
•	•	k?
52	#num#	k4
•	•	k?
53	#num#	k4
•	•	k?
54	#num#	k4
•	•	k?
55	#num#	k4
•	•	k?
56	#num#	k4
•	•	k?
57	#num#	k4
•	•	k?
58	#num#	k4
•	•	k?
59	#num#	k4
•	•	k?
60	#num#	k4
•	•	k?
61	#num#	k4
•	•	k?
62	#num#	k4
•	•	k?
63	#num#	k4
<g/>
•	•	k?
64	#num#	k4
Současná	současný	k2eAgFnSc1d1
</s>
<s>
Expedice	expedice	k1gFnSc1
65	#num#	k4
Plánované	plánovaný	k2eAgInPc1d1
</s>
<s>
66	#num#	k4
•	•	k?
67	#num#	k4
</s>
<s>
Návštěvní	návštěvní	k2eAgFnSc1d1
expedice	expedice	k1gFnSc1
</s>
<s>
Proběhlé	proběhlý	k2eAgNnSc1d1
</s>
<s>
EP-1	EP-1	k4
•	•	k?
EP-2	EP-2	k1gMnSc1
•	•	k?
EP-3	EP-3	k1gMnSc1
•	•	k?
EP-4	EP-4	k1gMnSc1
•	•	k?
EP-5	EP-5	k1gMnSc1
•	•	k?
EP-6	EP-6	k1gMnSc1
•	•	k?
EP-7	EP-7	k1gMnSc1
•	•	k?
EP-8	EP-8	k1gMnSc1
•	•	k?
EP-9	EP-9	k1gMnSc1
•	•	k?
EP-10	EP-10	k1gMnSc1
•	•	k?
EP-11	EP-11	k1gMnSc1
•	•	k?
EP-12	EP-12	k1gMnSc1
•	•	k?
EP-13	EP-13	k1gMnSc1
•	•	k?
EP-14	EP-14	k1gMnSc1
•	•	k?
EP-15	EP-15	k1gMnSc1
•	•	k?
EP-16	EP-16	k1gMnSc1
•	•	k?
EP-17	EP-17	k1gMnSc1
•	•	k?
EP-18	EP-18	k1gMnSc1
•	•	k?
EP-19	EP-19	k1gFnSc2
Plánované	plánovaný	k2eAgNnSc1d1
</s>
<s>
Boe-CFT	Boe-CFT	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
</s>
