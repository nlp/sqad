<p>
<s>
Galenit	galenit	k1gInSc1	galenit
(	(	kIx(	(
<g/>
leštěnec	leštěnec	k1gInSc1	leštěnec
olověný	olověný	k2eAgInSc1d1	olověný
<g/>
,	,	kIx,	,
sulfid	sulfid	k1gInSc1	sulfid
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
-	-	kIx~	-
PbS	PbS	k1gFnSc1	PbS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hojný	hojný	k2eAgInSc4d1	hojný
minerál	minerál	k1gInSc4	minerál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
v	v	k7c6	v
krychlové	krychlový	k2eAgFnSc6d1	krychlová
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
blejno	blejno	k1gNnSc1	blejno
olověné	olověný	k2eAgNnSc1d1	olověné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krystaly	krystal	k1gInPc1	krystal
bývají	bývat	k5eAaImIp3nP	bývat
hojné	hojný	k2eAgInPc1d1	hojný
a	a	k8xC	a
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
nalezišť	naleziště	k1gNnPc2	naleziště
známé	známý	k2eAgFnSc2d1	známá
v	v	k7c6	v
pěkném	pěkný	k2eAgInSc6d1	pěkný
vývoji	vývoj	k1gInSc6	vývoj
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
narostlé	narostlý	k2eAgFnPc1d1	narostlá
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
značně	značně	k6eAd1	značně
velké	velký	k2eAgNnSc1d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInSc4d3	nejčastější
tvar	tvar	k1gInSc4	tvar
je	být	k5eAaImIp3nS	být
krychle	krychle	k1gFnSc1	krychle
{	{	kIx(	{
<g/>
100	[number]	k4	100
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
osmistěny	osmistěn	k1gInPc1	osmistěn
{	{	kIx(	{
<g/>
111	[number]	k4	111
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
spojky	spojka	k1gFnSc2	spojka
krychle	krychle	k1gFnSc2	krychle
a	a	k8xC	a
oktaedru	oktaedr	k1gInSc2	oktaedr
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
tvoří	tvořit	k5eAaImIp3nP	tvořit
tabulky	tabulka	k1gFnPc1	tabulka
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastější	častý	k2eAgFnSc1d3	nejčastější
je	být	k5eAaImIp3nS	být
samotná	samotný	k2eAgFnSc1d1	samotná
krychle	krychle	k1gFnSc1	krychle
<g/>
.	.	kIx.	.
</s>
<s>
Osmistěn	osmistěn	k1gInSc1	osmistěn
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
protáhlý	protáhlý	k2eAgInSc1d1	protáhlý
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
krystalografické	krystalografický	k2eAgFnSc2d1	krystalografická
osy	osa	k1gFnSc2	osa
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nabývá	nabývat	k5eAaImIp3nS	nabývat
vzhledu	vzhled	k1gInSc3	vzhled
krystalu	krystal	k1gInSc2	krystal
čtverečného	čtverečný	k2eAgInSc2d1	čtverečný
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jinak	jinak	k6eAd1	jinak
jsou	být	k5eAaImIp3nP	být
krystaly	krystal	k1gInPc1	krystal
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
nepravidelně	pravidelně	k6eNd1	pravidelně
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
monstrosity	monstrosita	k1gFnSc2	monstrosita
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
například	například	k6eAd1	například
podle	podle	k7c2	podle
ploch	plocha	k1gFnPc2	plocha
osmistěnu	osmistěn	k1gInSc2	osmistěn
zploštělé	zploštělý	k2eAgFnSc2d1	zploštělá
<g/>
,	,	kIx,	,
tabulkovité	tabulkovitý	k2eAgFnSc2d1	tabulkovitý
až	až	k8xS	až
lístkovité	lístkovitý	k2eAgFnSc2d1	lístkovitý
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
krystaly	krystal	k1gInPc1	krystal
klencové	klencový	k2eAgFnSc2d1	klencová
tabulkovité	tabulkovitý	k2eAgFnSc2d1	tabulkovitý
podle	podle	k7c2	podle
plochy	plocha	k1gFnSc2	plocha
spodové	spodový	k2eAgFnSc2d1	spodová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krystaly	krystal	k1gInPc1	krystal
mají	mít	k5eAaImIp3nP	mít
dosti	dosti	k6eAd1	dosti
často	často	k6eAd1	často
zaoblené	zaoblený	k2eAgFnPc4d1	zaoblená
hrany	hrana	k1gFnPc4	hrana
i	i	k8xC	i
rohy	roh	k1gInPc4	roh
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
nabývají	nabývat	k5eAaImIp3nP	nabývat
až	až	k6eAd1	až
tvarů	tvar	k1gInPc2	tvar
kulovitých	kulovitý	k2eAgInPc2d1	kulovitý
a	a	k8xC	a
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k9	jako
tavené	tavený	k2eAgFnPc1d1	tavená
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
rozleptané	rozleptaný	k2eAgFnPc1d1	rozleptaná
<g/>
,	,	kIx,	,
přpomínajcí	přpomínajcet	k5eAaImIp3nS	přpomínajcet
buňkovité	buňkovitý	k2eAgInPc4d1	buňkovitý
tvary	tvar	k1gInPc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Spojky	spojka	k1gFnPc1	spojka
tvaru	tvar	k1gInSc2	tvar
osmistěnu	osmistěn	k1gInSc2	osmistěn
a	a	k8xC	a
krychle	krychle	k1gFnSc2	krychle
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývaji	nazýva	k6eAd2	nazýva
steinmanit	steinmanit	k5eAaPmF	steinmanit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Galenit	galenit	k1gInSc1	galenit
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
na	na	k7c6	na
hydrotermálních	hydrotermální	k2eAgFnPc6d1	hydrotermální
žilách	žíla	k1gFnPc6	žíla
pří	přít	k5eAaImIp3nP	přít
výstupu	výstup	k1gInSc3	výstup
horkých	horký	k2eAgInPc2d1	horký
roztoků	roztok	k1gInPc2	roztok
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
jinými	jiný	k2eAgInPc7d1	jiný
minerály	minerál	k1gInPc7	minerál
včetně	včetně	k7c2	včetně
fluoritu	fluorit	k1gInSc2	fluorit
<g/>
,	,	kIx,	,
křemene	křemen	k1gInSc2	křemen
<g/>
,	,	kIx,	,
kalcitu	kalcit	k1gInSc2	kalcit
<g/>
,	,	kIx,	,
barytu	baryt	k1gInSc2	baryt
<g/>
,	,	kIx,	,
sfaleritu	sfalerit	k1gInSc2	sfalerit
a	a	k8xC	a
pyritu	pyrit	k1gInSc2	pyrit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
sulfidy	sulfid	k1gInPc7	sulfid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
===	===	k?	===
</s>
</p>
<p>
<s>
Barvu	barva	k1gFnSc4	barva
má	mít	k5eAaImIp3nS	mít
ocelově	ocelově	k6eAd1	ocelově
šedou	šedý	k2eAgFnSc4d1	šedá
<g/>
,	,	kIx,	,
tmavší	tmavý	k2eAgFnSc4d2	tmavší
než	než	k8xS	než
u	u	k7c2	u
ryzího	ryzí	k2eAgNnSc2d1	ryzí
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
t	t	k?	t
<g/>
=	=	kIx~	=
<g/>
2,5	[number]	k4	2,5
<g/>
-	-	kIx~	-
<g/>
2,75	[number]	k4	2,75
<g/>
,	,	kIx,	,
ρ	ρ	k?	ρ
<g/>
=	=	kIx~	=
<g/>
7,2	[number]	k4	7,2
<g/>
-	-	kIx~	-
<g/>
7,6	[number]	k4	7,6
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
intenzivní	intenzivní	k2eAgInSc4d1	intenzivní
kovový	kovový	k2eAgInSc4d1	kovový
lesk	lesk	k1gInSc4	lesk
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
na	na	k7c6	na
štěpných	štěpný	k2eAgFnPc6d1	štěpná
plochách	plocha	k1gFnPc6	plocha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krystalové	krystalový	k2eAgFnPc1d1	krystalová
plochy	plocha	k1gFnPc1	plocha
často	často	k6eAd1	často
matně	matně	k6eAd1	matně
naběhlé	naběhlý	k2eAgNnSc1d1	naběhlé
<g/>
.	.	kIx.	.
</s>
<s>
Vryp	vryp	k1gInSc1	vryp
má	mít	k5eAaImIp3nS	mít
šedočerný	šedočerný	k2eAgInSc1d1	šedočerný
až	až	k6eAd1	až
černý	černý	k2eAgInSc1d1	černý
a	a	k8xC	a
nelesklý	lesklý	k2eNgInSc1d1	nelesklý
<g/>
.	.	kIx.	.
</s>
<s>
Štípe	štípat	k5eAaImIp3nS	štípat
se	se	k3xPyFc4	se
dokonale	dokonale	k6eAd1	dokonale
podle	podle	k7c2	podle
krychle	krychle	k1gFnSc2	krychle
{	{	kIx(	{
<g/>
100	[number]	k4	100
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
znak	znak	k1gInSc1	znak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úderem	úder	k1gInSc7	úder
kladívka	kladívko	k1gNnSc2	kladívko
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
na	na	k7c4	na
drobné	drobný	k2eAgFnPc4d1	drobná
krychličky	krychlička	k1gFnPc4	krychlička
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
je	být	k5eAaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
štěpnost	štěpnost	k1gFnSc1	štěpnost
nebo	nebo	k8xC	nebo
dělitelnost	dělitelnost	k1gFnSc1	dělitelnost
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
111	[number]	k4	111
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
taví	tavit	k5eAaImIp3nS	tavit
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
stříbronosný	stříbronosný	k2eAgMnSc1d1	stříbronosný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
kusový	kusový	k2eAgInSc1d1	kusový
a	a	k8xC	a
zrnitý	zrnitý	k2eAgInSc1d1	zrnitý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stébelnatých	stébelnatý	k2eAgFnPc6d1	stébelnatá
(	(	kIx(	(
<g/>
stébla	stéblo	k1gNnSc2	stéblo
jsou	být	k5eAaImIp3nP	být
krystaloví	krystalový	k2eAgMnPc1d1	krystalový
jedinci	jedinec	k1gMnPc1	jedinec
prodloužení	prodloužený	k2eAgMnPc1d1	prodloužený
podle	podle	k7c2	podle
čtyřčetné	čtyřčetný	k2eAgFnSc2d1	čtyřčetná
osy	osa	k1gFnSc2	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celistvých	celistvý	k2eAgFnPc2d1	celistvá
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
i	i	k9	i
hroznovitých	hroznovitý	k2eAgFnPc2d1	hroznovitá
a	a	k8xC	a
krápníkovitých	krápníkovitý	k2eAgFnPc2d1	krápníkovitý
<g/>
,	,	kIx,	,
také	také	k6eAd1	také
stromečkovitých	stromečkovitý	k2eAgInPc6d1	stromečkovitý
agregátech	agregát	k1gInPc6	agregát
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
pseudomorfózy	pseudomorfóza	k1gFnPc1	pseudomorfóza
hlavně	hlavně	k9	hlavně
po	po	k7c6	po
pyromorfitu	pyromorfit	k1gInSc6	pyromorfit
a	a	k8xC	a
vzácně	vzácně	k6eAd1	vzácně
fosilizuje	fosilizovat	k5eAaImIp3nS	fosilizovat
některé	některý	k3yIgFnPc4	některý
zkameněliny	zkamenělina	k1gFnPc4	zkamenělina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hořících	hořící	k2eAgFnPc6d1	hořící
haldách	halda	k1gFnPc6	halda
tvoří	tvořit	k5eAaImIp3nP	tvořit
někdy	někdy	k6eAd1	někdy
i	i	k9	i
značně	značně	k6eAd1	značně
velké	velký	k2eAgInPc1d1	velký
kostrovitě	kostrovitě	k6eAd1	kostrovitě
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
krystaly	krystal	k1gInPc1	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Orientovaně	Orientovaně	k6eAd1	Orientovaně
srůstá	srůstat	k5eAaImIp3nS	srůstat
s	s	k7c7	s
cotunnitem	cotunnit	k1gInSc7	cotunnit
<g/>
,	,	kIx,	,
fosgenitem	fosgenit	k1gInSc7	fosgenit
a	a	k8xC	a
anglesitem	anglesit	k1gInSc7	anglesit
<g/>
,	,	kIx,	,
pyritem	pyrit	k1gInSc7	pyrit
<g/>
,	,	kIx,	,
arsenopyritem	arsenopyrit	k1gInSc7	arsenopyrit
<g/>
,	,	kIx,	,
pyromorfitem	pyromorfit	k1gInSc7	pyromorfit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
===	===	k?	===
</s>
</p>
<p>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
a	a	k8xC	a
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnPc1d1	sírová
za	za	k7c4	za
vylučování	vylučování	k1gNnSc4	vylučování
oxidů	oxid	k1gInPc2	oxid
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
PbSO	PbSO	k1gFnSc7	PbSO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
dmuchavkou	dmuchavka	k1gFnSc7	dmuchavka
se	se	k3xPyFc4	se
na	na	k7c6	na
uhlí	uhlí	k1gNnSc6	uhlí
rozstřikuje	rozstřikovat	k5eAaImIp3nS	rozstřikovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jemném	jemný	k2eAgInSc6d1	jemný
prášku	prášek	k1gInSc6	prášek
se	se	k3xPyFc4	se
pokojně	pokojně	k6eAd1	pokojně
taví	tavit	k5eAaImIp3nS	tavit
(	(	kIx(	(
<g/>
tavitelnost	tavitelnost	k1gFnSc1	tavitelnost
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Optické	optický	k2eAgFnPc4d1	optická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
===	===	k?	===
</s>
</p>
<p>
<s>
Dvojčatný	dvojčatný	k2eAgInSc1d1	dvojčatný
srůst	srůst	k1gInSc1	srůst
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
111	[number]	k4	111
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
častý	častý	k2eAgInSc1d1	častý
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejčastěji	často	k6eAd3	často
vznikají	vznikat	k5eAaImIp3nP	vznikat
penetrační	penetrační	k2eAgNnPc4d1	penetrační
dvojčata	dvojče	k1gNnPc4	dvojče
dvou	dva	k4xCgMnPc2	dva
nestejně	stejně	k6eNd1	stejně
velkých	velký	k2eAgMnPc2d1	velký
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
;	;	kIx,	;
dvojčata	dvojče	k1gNnPc1	dvojče
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
tabulkovitá	tabulkovitý	k2eAgNnPc1d1	tabulkovité
<g/>
.	.	kIx.	.
</s>
<s>
Kusovité	kusovitý	k2eAgInPc1d1	kusovitý
štěpné	štěpný	k2eAgInPc1d1	štěpný
tvary	tvar	k1gInPc1	tvar
jeví	jevit	k5eAaImIp3nP	jevit
často	často	k6eAd1	často
dvojčatné	dvojčatný	k2eAgNnSc4d1	dvojčatný
rýhování	rýhování	k1gNnSc4	rýhování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
lamelami	lamela	k1gFnPc7	lamela
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
441	[number]	k4	441
<g/>
}	}	kIx)	}
nebo	nebo	k8xC	nebo
{	{	kIx(	{
<g/>
331	[number]	k4	331
<g/>
}	}	kIx)	}
a	a	k8xC	a
{	{	kIx(	{
<g/>
311	[number]	k4	311
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
galenit	galenit	k1gInSc4	galenit
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgFnPc1d1	charakteristická
spojky	spojka	k1gFnPc1	spojka
{	{	kIx(	{
<g/>
100	[number]	k4	100
<g/>
}	}	kIx)	}
a	a	k8xC	a
{	{	kIx(	{
<g/>
111	[number]	k4	111
<g/>
}	}	kIx)	}
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
vývoji	vývoj	k1gInSc6	vývoj
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc1	dva
tvary	tvar	k1gInPc1	tvar
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
tzv.	tzv.	kA	tzv.
kubooktaedr	kubooktaedr	k1gInSc1	kubooktaedr
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
dvanáctistěn	dvanáctistěn	k2eAgMnSc1d1	dvanáctistěn
{	{	kIx(	{
<g/>
110	[number]	k4	110
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
triaksioktaedry	triaksioktaedr	k1gInPc1	triaksioktaedr
{	{	kIx(	{
<g/>
221	[number]	k4	221
<g/>
}	}	kIx)	}
a	a	k8xC	a
{	{	kIx(	{
<g/>
331	[number]	k4	331
<g/>
}	}	kIx)	}
a	a	k8xC	a
také	také	k9	také
často	často	k6eAd1	často
{	{	kIx(	{
<g/>
211	[number]	k4	211
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
asi	asi	k9	asi
33	[number]	k4	33
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Získávání	získávání	k1gNnSc2	získávání
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
rudy	ruda	k1gFnSc2	ruda
jejím	její	k3xOp3gNnSc7	její
pražením	pražení	k1gNnSc7	pražení
a	a	k8xC	a
poté	poté	k6eAd1	poté
žárovou	žárový	k2eAgFnSc7d1	Žárová
redukcí	redukce	k1gFnSc7	redukce
uhlíkem	uhlík	k1gInSc7	uhlík
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
a	a	k8xC	a
také	také	k9	také
nejhojnější	hojný	k2eAgFnSc7d3	nejhojnější
rudou	ruda	k1gFnSc7	ruda
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
také	také	k9	také
i	i	k9	i
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svrchních	svrchní	k2eAgFnPc6d1	svrchní
částech	část	k1gFnPc6	část
jeho	jeho	k3xOp3gNnPc2	jeho
ložisek	ložisko	k1gNnPc2	ložisko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
mnoho	mnoho	k4c1	mnoho
produktů	produkt	k1gInPc2	produkt
jeho	jeho	k3xOp3gFnSc2	jeho
oxidace	oxidace	k1gFnSc2	oxidace
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
pěkně	pěkně	k6eAd1	pěkně
vykrystalizovaných	vykrystalizovaný	k2eAgInPc2d1	vykrystalizovaný
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
cerusit	cerusit	k1gInSc1	cerusit
PbCO	PbCO	k1gFnSc2	PbCO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
pyromorfit	pyromorfit	k1gInSc1	pyromorfit
Pb	Pb	k1gFnSc1	Pb
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
PO	Po	kA	Po
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
Cl	Cl	k1gFnPc2	Cl
<g/>
,	,	kIx,	,
mimetesit	mimetesit	k1gInSc1	mimetesit
Pb	Pb	k1gFnSc1	Pb
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
AsO	AsO	k1gFnSc1	AsO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
Cl	Cl	k1gFnPc2	Cl
<g/>
,	,	kIx,	,
fosgenit	fosgenit	k5eAaPmF	fosgenit
Pb	Pb	k1gFnPc4	Pb
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
Cl	Cl	k1gFnPc2	Cl
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
hojněji	hojně	k6eAd2	hojně
i	i	k9	i
anglesit	anglesit	k1gInSc1	anglesit
PbSO	PbSO	k1gMnPc2	PbSO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
původní	původní	k2eAgInSc1d1	původní
obsah	obsah	k1gInSc1	obsah
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
vznikat	vznikat	k5eAaImF	vznikat
sekundární	sekundární	k2eAgNnSc4d1	sekundární
ryzí	ryzí	k2eAgNnSc4d1	ryzí
stříbro	stříbro	k1gNnSc4	stříbro
či	či	k8xC	či
akantit	akantit	k1gInSc4	akantit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
galenit	galenit	k1gInSc1	galenit
nahradil	nahradit	k5eAaPmAgInS	nahradit
malachitový	malachitový	k2eAgInSc1d1	malachitový
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
zeleného	zelený	k2eAgNnSc2d1	zelené
líčidla	líčidlo	k1gNnSc2	líčidlo
na	na	k7c4	na
očí	oko	k1gNnPc2	oko
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
součástí	součást	k1gFnSc7	součást
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
egyptských	egyptský	k2eAgFnPc2d1	egyptská
žen	žena	k1gFnPc2	žena
i	i	k9	i
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
galenitem	galenit	k1gInSc7	galenit
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
módní	módní	k2eAgNnSc1d1	módní
líčení	líčení	k1gNnSc1	líčení
černých	černý	k2eAgFnPc2d1	černá
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
krystaly	krystal	k1gInPc1	krystal
používaly	používat	k5eAaImAgInP	používat
v	v	k7c6	v
primitivních	primitivní	k2eAgInPc6d1	primitivní
rozhlasových	rozhlasový	k2eAgInPc6d1	rozhlasový
přijímačích	přijímač	k1gInPc6	přijímač
nazývaných	nazývaný	k2eAgInPc6d1	nazývaný
krystalka	krystalka	k1gFnSc1	krystalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
nacházen	nacházet	k5eAaImNgInS	nacházet
v	v	k7c6	v
pěkných	pěkný	k2eAgInPc6d1	pěkný
velkých	velký	k2eAgInPc6d1	velký
krystalech	krystal	k1gInPc6	krystal
ve	v	k7c6	v
Stříbře	stříbro	k1gNnSc6	stříbro
<g/>
,	,	kIx,	,
v	v	k7c6	v
oktaedrech	oktaedr	k1gInPc6	oktaedr
tzv.	tzv.	kA	tzv.
steinmanitech	steinmanit	k1gInPc6	steinmanit
v	v	k7c6	v
Příbrami	Příbram	k1gFnSc6	Příbram
<g/>
,	,	kIx,	,
těžen	těžen	k2eAgInSc1d1	těžen
do	do	k7c2	do
konce	konec	k1gInSc2	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
v	v	k7c6	v
Harrachově	Harrachov	k1gInSc6	Harrachov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
obsah	obsah	k1gInSc1	obsah
Pb	Pb	k1gFnSc4	Pb
na	na	k7c6	na
žilách	žíla	k1gFnPc6	žíla
až	až	k9	až
30	[number]	k4	30
%	%	kIx~	%
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgFnPc1d1	drobná
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgFnPc1d1	vyvinutá
krystalky	krystalka	k1gFnPc1	krystalka
nacházeny	nacházen	k2eAgFnPc1d1	nacházena
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
pelosideritů	pelosiderit	k1gInPc2	pelosiderit
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
<g/>
,	,	kIx,	,
v	v	k7c6	v
úhledných	úhledný	k2eAgFnPc6d1	úhledná
ukázkách	ukázka	k1gFnPc6	ukázka
ze	z	k7c2	z
Staré	Staré	k2eAgFnSc2d1	Staré
Vožice	Vožice	k1gFnSc2	Vožice
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
byly	být	k5eAaImAgFnP	být
kostrovitě	kostrovitě	k6eAd1	kostrovitě
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
krystaly	krystal	k1gInPc1	krystal
z	z	k7c2	z
hořících	hořící	k2eAgFnPc2d1	hořící
hald	halda	k1gFnPc2	halda
v	v	k7c6	v
Bečkově	Bečkův	k2eAgFnSc6d1	Bečkova
(	(	kIx(	(
<g/>
u	u	k7c2	u
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
)	)	kIx)	)
a	a	k8xC	a
nověji	nově	k6eAd2	nově
pak	pak	k6eAd1	pak
v	v	k7c6	v
Radvanicích	Radvanice	k1gFnPc6	Radvanice
(	(	kIx(	(
<g/>
u	u	k7c2	u
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
nacházeny	nacházen	k2eAgFnPc1d1	nacházena
až	až	k9	až
4	[number]	k4	4
cm	cm	kA	cm
velké	velký	k2eAgInPc4d1	velký
kostrovité	kostrovitý	k2eAgInPc4d1	kostrovitý
krystaly	krystal	k1gInPc4	krystal
<g/>
,	,	kIx,	,
modravě	modravě	k6eAd1	modravě
naběhlé	naběhlý	k2eAgFnPc1d1	naběhlá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
z	z	k7c2	z
Banské	banský	k2eAgFnSc2d1	Banská
Štiavnice	Štiavnica	k1gFnSc2	Štiavnica
drúzy	drúza	k1gFnSc2	drúza
galenitu	galenit	k1gInSc2	galenit
s	s	k7c7	s
krystaly	krystal	k1gInPc7	krystal
až	až	k9	až
5	[number]	k4	5
cm	cm	kA	cm
velkými	velký	k2eAgInPc7d1	velký
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
silně	silně	k6eAd1	silně
korodovanými	korodovaný	k2eAgFnPc7d1	korodovaná
<g/>
,	,	kIx,	,
běžný	běžný	k2eAgInSc4d1	běžný
ze	z	k7c2	z
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
Baně	baně	k1gFnSc2	baně
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledově	vzhledově	k6eAd1	vzhledově
hezké	hezký	k2eAgFnPc1d1	hezká
ukázky	ukázka	k1gFnPc1	ukázka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
např.	např.	kA	např.
z	z	k7c2	z
Joplinu	Joplin	k1gInSc2	Joplin
(	(	kIx(	(
<g/>
Missouri	Missouri	k1gFnSc1	Missouri
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
Picher	Pichra	k1gFnPc2	Pichra
(	(	kIx(	(
<g/>
Oklahoma	Oklahoma	k1gFnSc1	Oklahoma
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Galenit	galenit	k1gInSc1	galenit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galenit	galenit	k1gInSc1	galenit
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Galenit	galenit	k1gInSc1	galenit
na	na	k7c6	na
webu	web	k1gInSc6	web
Webmineral	Webmineral	k1gFnSc2	Webmineral
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Galenit	galenit	k1gInSc1	galenit
v	v	k7c6	v
atlasu	atlas	k1gInSc6	atlas
minerálů	minerál	k1gInPc2	minerál
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mineral	Minerat	k5eAaImAgMnS	Minerat
data	datum	k1gNnSc2	datum
publishing	publishing	k1gInSc1	publishing
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Galenit	galenit	k1gInSc1	galenit
v	v	k7c4	v
učebnice	učebnice	k1gFnPc4	učebnice
mineralogie	mineralogie	k1gFnSc2	mineralogie
PřF	PřF	k1gMnPc2	PřF
MU	MU	kA	MU
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
