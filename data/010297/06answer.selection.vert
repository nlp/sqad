<s>
Galenit	galenit	k1gInSc1	galenit
(	(	kIx(	(
<g/>
leštěnec	leštěnec	k1gInSc1	leštěnec
olověný	olověný	k2eAgInSc1d1	olověný
<g/>
,	,	kIx,	,
sulfid	sulfid	k1gInSc1	sulfid
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
-	-	kIx~	-
PbS	PbS	k1gFnSc1	PbS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hojný	hojný	k2eAgInSc4d1	hojný
minerál	minerál	k1gInSc4	minerál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
v	v	k7c6	v
krychlové	krychlový	k2eAgFnSc6d1	krychlová
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
