<s>
Société	Sociéta	k1gMnPc1
nationale	nationale	k6eAd1
des	des	k1gNnSc4
chemins	chemins	k6eAd1
de	de	k?
fer	fer	k?
français	français	k1gInSc1
</s>
<s>
Société	Sociéta	k1gMnPc1
nationale	nationale	k6eAd1
des	des	k1gNnSc4
chemins	chemins	k6eAd1
de	de	k?
fer	fer	k?
français	français	k1gInSc1
Logo	logo	k1gNnSc4
Základní	základní	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
société	sociéta	k1gMnPc1
anonyme	anonym	k1gInSc5
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1938	#num#	k4
Předchůdci	předchůdce	k1gMnPc1
</s>
<s>
Chemins	Chemins	k1gInSc1
de	de	k?
fer	fer	k?
du	du	k?
NordChemins	NordChemins	k1gInSc1
de	de	k?
fer	fer	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
Alsace	Alsace	k1gFnSc1
et	et	k?
de	de	k?
LorraineChemins	LorraineChemins	k1gInSc1
de	de	k?
fer	fer	k?
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
ÉtatChemins	ÉtatChemins	k1gInSc1
de	de	k?
fer	fer	k?
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
EstCompagnie	EstCompagnie	k1gFnSc1
des	des	k1gNnSc2
chemins	cheminsa	k1gFnPc2
de	de	k?
fer	fer	k?
de	de	k?
Paris	Paris	k1gMnSc1
à	à	k?
Lyon	Lyon	k1gInSc1
et	et	k?
à	à	k?
la	la	k1gNnSc7
MéditerranéeCompagnie	MéditerranéeCompagnie	k1gFnSc2
du	du	k?
chemin	chemin	k1gInSc1
de	de	k?
fer	fer	k?
de	de	k?
Paris	Paris	k1gMnSc1
à	à	k?
OrléansChemins	OrléansChemins	k1gInSc1
de	de	k?
fer	fer	k?
du	du	k?
Midi	Mid	k1gFnSc2
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Saint-Denis	Saint-Denis	k1gFnSc1
<g/>
,	,	kIx,
932	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
železnice	železnice	k1gFnSc1
Majitel	majitel	k1gMnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
100	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
Dceřiné	dceřiný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
</s>
<s>
Trains-Expo	Trains-Expa	k1gFnSc5
SNCFKeolisAlleoElipsosTGV	SNCFKeolisAlleoElipsosTGV	k1gFnSc5
LyriaEurostarThalysVoyages-sncf	LyriaEurostarThalysVoyages-sncf	k1gInSc4
<g/>
.	.	kIx.
<g/>
comTGV	comTGV	k?
inOuiFret	inOuiFret	k1gInSc1
SNCF	SNCF	kA
Identifikátory	identifikátor	k1gInPc1
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc4
</s>
<s>
www.sncf.com	www.sncf.com	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vlak	vlak	k1gInSc1
RER	RER	kA
</s>
<s>
Elektrický	elektrický	k2eAgInSc1d1
vlak	vlak	k1gInSc1
SNCF	SNCF	kA
</s>
<s>
Société	Sociéta	k1gMnPc1
nationale	nationale	k6eAd1
des	des	k1gNnSc4
chemins	chemins	k6eAd1
de	de	k?
fer	fer	k?
français	français	k1gInSc1
(	(	kIx(
<g/>
zkracováno	zkracovat	k5eAaImNgNnS
téměř	téměř	k6eAd1
vždy	vždy	k6eAd1
jako	jako	k8xS,k8xC
SNCF	SNCF	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
národní	národní	k2eAgMnSc1d1
železniční	železniční	k2eAgMnSc1d1
dopravce	dopravce	k1gMnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
je	být	k5eAaImIp3nS
vlastněná	vlastněný	k2eAgFnSc1d1
státem	stát	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozuje	provozovat	k5eAaImIp3nS
32	#num#	k4
000	#num#	k4
km	km	kA
železničních	železniční	k2eAgFnPc2d1
tratí	trať	k1gFnPc2
(	(	kIx(
<g/>
z	z	k7c2
nich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
1500	#num#	k4
vysokorychlostních	vysokorychlostní	k2eAgFnPc2d1
a	a	k8xC
14	#num#	k4
500	#num#	k4
elektrifikovaných	elektrifikovaný	k2eAgMnPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
denně	denně	k6eAd1
vypraví	vypravit	k5eAaPmIp3nS
14	#num#	k4
000	#num#	k4
vlaků	vlak	k1gInPc2
a	a	k8xC
zaměstnává	zaměstnávat	k5eAaImIp3nS
150	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídli	sídlet	k5eAaImRp2nS
v	v	k7c6
Paříži	Paříž	k1gFnSc6
a	a	k8xC
jejím	její	k3xOp3gMnSc7
současným	současný	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
je	být	k5eAaImIp3nS
Guillaume	Guillaum	k1gInSc5
Pepy	Pepa	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
SNCF	SNCF	kA
provozuje	provozovat	k5eAaImIp3nS
téměř	téměř	k6eAd1
všechny	všechen	k3xTgFnPc4
železniční	železniční	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
na	na	k7c6
území	území	k1gNnSc6
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
vysokorychlostních	vysokorychlostní	k2eAgInPc2d1
vlaků	vlak	k1gInPc2
TGV	TGV	kA
a	a	k8xC
pařížské	pařížský	k2eAgFnSc2d1
příměstské	příměstský	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
RER	RER	kA
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
RATP	RATP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
zajišťovala	zajišťovat	k5eAaImAgFnS
i	i	k9
provoz	provoz	k1gInSc4
traťové	traťový	k2eAgFnSc2d1
infrastruktury	infrastruktura	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
však	však	k9
toto	tento	k3xDgNnSc4
již	již	k6eAd1
neexistuje	existovat	k5eNaImIp3nS
z	z	k7c2
důvodu	důvod	k1gInSc2
nových	nový	k2eAgFnPc2d1
regulací	regulace	k1gFnPc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
tedy	tedy	k9
zřízena	zřídit	k5eAaPmNgFnS
společnost	společnost	k1gFnSc1
Réseau	Réseaus	k1gInSc2
Ferré	Ferrý	k2eAgFnSc2d1
de	de	k?
France	Franc	k1gMnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
řídí	řídit	k5eAaImIp3nS
přímo	přímo	k6eAd1
stát	stát	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takové	takový	k3xDgNnSc1
uspořádání	uspořádání	k1gNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
zavedení	zavedení	k1gNnSc4
podmínek	podmínka	k1gFnPc2
volného	volný	k2eAgInSc2d1
trhu	trh	k1gInSc2
do	do	k7c2
železniční	železniční	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
zatím	zatím	k6eAd1
to	ten	k3xDgNnSc1
však	však	k9
nebylo	být	k5eNaImAgNnS
realizováno	realizován	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
vývoj	vývoj	k1gInSc1
</s>
<s>
SNCF	SNCF	kA
vznikla	vzniknout	k5eAaPmAgFnS
sloučením	sloučení	k1gNnSc7
několika	několik	k4yIc2
menších	malý	k2eAgFnPc2d2
společností	společnost	k1gFnPc2
<g/>
,	,	kIx,
operujících	operující	k2eAgFnPc2d1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
částech	část	k1gFnPc6
země	zem	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novou	nový	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
vlastnil	vlastnit	k5eAaImAgInS
již	již	k6eAd1
stát	stát	k1gInSc1
a	a	k8xC
investoval	investovat	k5eAaBmAgInS
do	do	k7c2
ní	on	k3xPp3gFnSc2
nemalé	malý	k2eNgFnSc2d1
sumy	suma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
projekt	projekt	k1gInSc1
na	na	k7c4
výstavbu	výstavba	k1gFnSc4
vysokorychlostní	vysokorychlostní	k2eAgFnSc2d1
trati	trať	k1gFnSc2
(	(	kIx(
<g/>
LGV	LGV	kA
<g/>
)	)	kIx)
z	z	k7c2
metropole	metropol	k1gFnSc2
na	na	k7c4
jih	jih	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
Lyonu	Lyon	k1gInSc2
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
zrodila	zrodit	k5eAaPmAgFnS
první	první	k4xOgFnSc7
LGV	LGV	kA
roku	rok	k1gInSc2
1981	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
a	a	k8xC
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
přibyly	přibýt	k5eAaPmAgInP
další	další	k2eAgNnPc4d1
LGV	LGV	kA
<g/>
,	,	kIx,
technologie	technologie	k1gFnSc1
TGV	TGV	kA
se	se	k3xPyFc4
rozšířila	rozšířit	k5eAaPmAgFnS
i	i	k9
do	do	k7c2
dalších	další	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2006	#num#	k4
se	se	k3xPyFc4
vyjádřil	vyjádřit	k5eAaPmAgMnS
prezident	prezident	k1gMnSc1
republiky	republika	k1gFnSc2
Jacques	Jacques	k1gMnSc1
Chirac	Chirac	k1gMnSc1
že	že	k8xS
do	do	k7c2
dvaceti	dvacet	k4xCc2
let	léto	k1gNnPc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
železnice	železnice	k1gFnSc1
již	již	k6eAd1
nezávislá	závislý	k2eNgFnSc1d1
na	na	k7c6
fosilních	fosilní	k2eAgNnPc6d1
palivech	palivo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
tak	tak	k6eAd1
bude	být	k5eAaImBp3nS
pokračovat	pokračovat	k5eAaImF
elektrifikace	elektrifikace	k1gFnPc4
tratí	trať	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Société	Sociéta	k1gMnPc1
nationale	nationale	k6eAd1
des	des	k1gNnSc4
chemins	chemins	k6eAd1
de	de	k?
fer	fer	k?
français	français	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Mapa	mapa	k1gFnSc1
sítě	síť	k1gFnSc2
</s>
<s>
Stránky	stránka	k1gFnPc1
SNCF	SNCF	kA
v	v	k7c6
angličtině	angličtina	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Národní	národní	k2eAgFnSc2d1
železniční	železniční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
BDŽ	BDŽ	kA
•	•	k?
BR	br	k0
•	•	k?
BČ	BČ	kA
•	•	k?
CFL	CFL	kA
•	•	k?
CFM	CFM	kA
•	•	k?
CFR	CFR	kA
•	•	k?
CP	CP	kA
•	•	k?
ČD	ČD	kA
•	•	k?
DB	db	kA
•	•	k?
DSB	DSB	kA
•	•	k?
ELRON	ELRON	kA
•	•	k?
FS	FS	kA
•	•	k?
HŽ	HŽ	kA
•	•	k?
LDz	LDz	k1gFnSc2
•	•	k?
LG	LG	kA
•	•	k?
MÁV	MÁV	kA
•	•	k?
MŽ	MŽ	kA
•	•	k?
NS	NS	kA
•	•	k?
NSB	NSB	kA
•	•	k?
OSE	osa	k1gFnSc6
•	•	k?
ÖBB	ÖBB	kA
•	•	k?
PKP	PKP	kA
•	•	k?
RENFE	RENFE	kA
•	•	k?
RŽD	RŽD	kA
•	•	k?
SBB	SBB	kA
•	•	k?
SJ	SJ	kA
•	•	k?
SNCB	SNCB	kA
•	•	k?
SNCF	SNCF	kA
•	•	k?
SŽ	SŽ	kA
•	•	k?
UZ	UZ	kA
•	•	k?
VR	vr	k0
•	•	k?
ŽSR	ŽSR	kA
<g/>
,	,	kIx,
ZSSK	ZSSK	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Železnice	železnice	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2165	#num#	k4
4853	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
136754690	#num#	k4
</s>
