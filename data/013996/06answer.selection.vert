<s>
SNCF	SNCF	kA
provozuje	provozovat	k5eAaImIp3nS
téměř	téměř	k6eAd1
všechny	všechen	k3xTgFnPc4
železniční	železniční	k2eAgFnPc4d1
tratě	trať	k1gFnPc4
na	na	k7c6
území	území	k1gNnSc6
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
vysokorychlostních	vysokorychlostní	k2eAgInPc2d1
vlaků	vlak	k1gInPc2
TGV	TGV	kA
a	a	k8xC
pařížské	pařížský	k2eAgFnSc2d1
příměstské	příměstský	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
RER	RER	kA
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
RATP	RATP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>