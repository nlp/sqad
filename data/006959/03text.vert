<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
městě	město	k1gNnSc6	město
Chamonix	Chamonix	k1gNnSc2	Chamonix
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
patronací	patronace	k1gFnSc7	patronace
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
olympijského	olympijský	k2eAgInSc2d1	olympijský
komitétu	komitét	k1gInSc2	komitét
se	se	k3xPyFc4	se
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
hory	hora	k1gFnSc2	hora
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
jako	jako	k8xC	jako
předehra	předehra	k1gFnSc1	předehra
8	[number]	k4	8
<g/>
.	.	kIx.	.
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Týden	týden	k1gInSc1	týden
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
úspěch	úspěch	k1gInSc4	úspěch
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
olympijském	olympijský	k2eAgInSc6d1	olympijský
kongresu	kongres	k1gInSc6	kongres
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
zpětně	zpětně	k6eAd1	zpětně
uznán	uznat	k5eAaPmNgInS	uznat
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
zimní	zimní	k2eAgFnSc1d1	zimní
olympiáda	olympiáda	k1gFnSc1	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
psát	psát	k5eAaImF	psát
nová	nový	k2eAgFnSc1d1	nová
éra	éra	k1gFnSc1	éra
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prvém	prvý	k4xOgInSc6	prvý
zasedání	zasedání	k1gNnSc6	zasedání
MOV	MOV	kA	MOV
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
uspořádání	uspořádání	k1gNnSc4	uspořádání
novodobých	novodobý	k2eAgFnPc2d1	novodobá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgInS	mít
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Coubertin	Coubertin	k1gMnSc1	Coubertin
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
seznamu	seznam	k1gInSc6	seznam
vhodných	vhodný	k2eAgInPc2d1	vhodný
sportů	sport	k1gInPc2	sport
i	i	k8xC	i
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Atény	Atény	k1gFnPc1	Atény
neměly	mít	k5eNaImAgFnP	mít
ledovou	ledový	k2eAgFnSc4d1	ledová
plochu	plocha	k1gFnSc4	plocha
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
zařazení	zařazení	k1gNnSc1	zařazení
na	na	k7c4	na
program	program	k1gInSc4	program
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
oddálilo	oddálit	k5eAaPmAgNnS	oddálit
až	až	k9	až
na	na	k7c4	na
Letní	letní	k2eAgFnPc4d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
1908	[number]	k4	1908
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
však	však	k9	však
zimní	zimní	k2eAgInPc4d1	zimní
sporty	sport	k1gInPc4	sport
nestaly	stát	k5eNaPmAgFnP	stát
pevnou	pevný	k2eAgFnSc7d1	pevná
součástí	součást	k1gFnSc7	součást
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Krasobruslení	krasobruslení	k1gNnSc1	krasobruslení
a	a	k8xC	a
lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1920	[number]	k4	1920
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všem	všecek	k3xTgMnPc3	všecek
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
uspořádat	uspořádat	k5eAaPmF	uspořádat
samostatné	samostatný	k2eAgFnPc4d1	samostatná
zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
sportům	sport	k1gInPc3	sport
vyhovujícím	vyhovující	k2eAgInPc3d1	vyhovující
termínu	termín	k1gInSc2	termín
<g/>
.	.	kIx.	.
</s>
<s>
Nositeli	nositel	k1gMnSc3	nositel
této	tento	k3xDgFnSc3	tento
myšlenky	myšlenka	k1gFnSc2	myšlenka
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
především	především	k9	především
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
a	a	k8xC	a
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
byly	být	k5eAaImAgInP	být
především	především	k9	především
skandinávské	skandinávský	k2eAgFnPc1d1	skandinávská
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
totiž	totiž	k9	totiž
pořádaly	pořádat	k5eAaImAgFnP	pořádat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
Severské	severský	k2eAgFnSc2d1	severská
hry	hra	k1gFnSc2	hra
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
zániku	zánik	k1gInSc2	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodující	rozhodující	k2eAgNnSc1d1	rozhodující
střetnutí	střetnutí	k1gNnSc1	střetnutí
příznivců	příznivec	k1gMnPc2	příznivec
a	a	k8xC	a
odpůrců	odpůrce	k1gMnPc2	odpůrce
zimní	zimní	k2eAgFnSc2d1	zimní
olympiády	olympiáda	k1gFnSc2	olympiáda
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
2	[number]	k4	2
<g/>
.	.	kIx.	.
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1921	[number]	k4	1921
v	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
na	na	k7c6	na
přípravné	přípravný	k2eAgFnSc6d1	přípravná
konferenci	konference	k1gFnSc6	konference
konané	konaný	k2eAgNnSc1d1	konané
26	[number]	k4	26
<g/>
.	.	kIx.	.
-	-	kIx~	-
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
čistě	čistě	k6eAd1	čistě
o	o	k7c6	o
zimních	zimní	k2eAgInPc6d1	zimní
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lausanne	Lausanne	k1gNnSc6	Lausanne
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
po	po	k7c6	po
složitých	složitý	k2eAgNnPc6d1	složité
zákulisních	zákulisní	k2eAgNnPc6d1	zákulisní
jednáních	jednání	k1gNnPc6	jednání
se	se	k3xPyFc4	se
zástupci	zástupce	k1gMnPc1	zástupce
severských	severský	k2eAgFnPc6d1	severská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
Francie	Francie	k1gFnSc1	Francie
získá	získat	k5eAaPmIp3nS	získat
právo	právo	k1gNnSc4	právo
uspořádat	uspořádat	k5eAaPmF	uspořádat
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
připadne	připadnout	k5eAaPmIp3nS	připadnout
jí	on	k3xPp3gFnSc3	on
také	také	k9	také
právo	právo	k1gNnSc4	právo
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
MOV	MOV	kA	MOV
"	"	kIx"	"
<g/>
Týden	týden	k1gInSc1	týden
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
však	však	k9	však
nebude	být	k5eNaImBp3nS	být
součástí	součást	k1gFnSc7	součást
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
schválení	schválení	k1gNnSc6	schválení
Paříže	Paříž	k1gFnSc2	Paříž
za	za	k7c4	za
pořadatele	pořadatel	k1gMnPc4	pořadatel
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympiády	olympiáda	k1gFnSc2	olympiáda
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
pootevřely	pootevřít	k5eAaPmAgFnP	pootevřít
dveře	dveře	k1gFnPc4	dveře
i	i	k9	i
k	k	k7c3	k
pořádání	pořádání	k1gNnSc3	pořádání
zimních	zimní	k2eAgFnPc2d1	zimní
olympiád	olympiáda	k1gFnPc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
pořádání	pořádání	k1gNnSc4	pořádání
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
až	až	k9	až
na	na	k7c4	na
23	[number]	k4	23
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
MOV	MOV	kA	MOV
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obrovském	obrovský	k2eAgInSc6d1	obrovský
úspěchu	úspěch	k1gInSc6	úspěch
Týdne	týden	k1gInSc2	týden
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
rok	rok	k1gInSc1	rok
předtím	předtím	k6eAd1	předtím
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
završeno	završit	k5eAaPmNgNnS	završit
čtvrtstoletí	čtvrtstoletí	k1gNnSc1	čtvrtstoletí
úsilí	úsilí	k1gNnSc2	úsilí
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
pořádání	pořádání	k1gNnSc4	pořádání
<g/>
.	.	kIx.	.
</s>
<s>
Vždyť	vždyť	k9	vždyť
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
český	český	k2eAgMnSc1d1	český
průkopník	průkopník	k1gMnSc1	průkopník
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Rössler-Ořovský	Rössler-Ořovský	k1gMnSc1	Rössler-Ořovský
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
uspořádání	uspořádání	k1gNnSc4	uspořádání
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
také	také	k9	také
byl	být	k5eAaImAgInS	být
zpětně	zpětně	k6eAd1	zpětně
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
kongresu	kongres	k1gInSc6	kongres
uznán	uznán	k2eAgInSc4d1	uznán
Týden	týden	k1gInSc4	týden
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
za	za	k7c4	za
zimní	zimní	k2eAgFnSc4d1	zimní
olympiádu	olympiáda	k1gFnSc4	olympiáda
a	a	k8xC	a
přijatá	přijatý	k2eAgFnSc1d1	přijatá
Charta	charta	k1gFnSc1	charta
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
tak	tak	k6eAd1	tak
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
přidělila	přidělit	k5eAaPmAgFnS	přidělit
pořadové	pořadový	k2eAgNnSc4d1	pořadové
číslo	číslo	k1gNnSc4	číslo
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
se	se	k3xPyFc4	se
akt	akt	k1gInSc1	akt
uznání	uznání	k1gNnSc2	uznání
Týdne	týden	k1gInSc2	týden
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
za	za	k7c4	za
1	[number]	k4	1
<g/>
.	.	kIx.	.
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
završil	završit	k5eAaPmAgInS	završit
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc6	zasedání
MOV	MOV	kA	MOV
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Týden	týden	k1gInSc4	týden
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
plakát	plakát	k1gInSc1	plakát
legendárním	legendární	k2eAgMnSc7d1	legendární
francouzským	francouzský	k2eAgMnSc7d1	francouzský
plakátovým	plakátový	k2eAgMnSc7d1	plakátový
umělcem	umělec	k1gMnSc7	umělec
Augustem	August	k1gMnSc7	August
Matissem	Matiss	k1gMnSc7	Matiss
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
ho	on	k3xPp3gNnSc4	on
vytištěno	vytištěn	k2eAgNnSc1d1	vytištěno
zhruba	zhruba	k6eAd1	zhruba
5.000	[number]	k4	5.000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k9	ještě
nebylo	být	k5eNaImAgNnS	být
zpětně	zpětně	k6eAd1	zpětně
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
konaly	konat	k5eAaImAgFnP	konat
hry	hra	k1gFnPc1	hra
první	první	k4xOgFnSc2	první
zimní	zimní	k2eAgFnSc2d1	zimní
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
plakát	plakát	k1gInSc1	plakát
se	se	k3xPyFc4	se
o	o	k7c6	o
ničem	nic	k3yNnSc6	nic
takovém	takový	k3xDgNnSc6	takový
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
lze	lze	k6eAd1	lze
volně	volně	k6eAd1	volně
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
připojeno	připojen	k2eAgNnSc1d1	připojeno
k	k	k7c3	k
mistrovským	mistrovský	k2eAgFnPc3d1	mistrovská
soutěžím	soutěž	k1gFnPc3	soutěž
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
plakátu	plakát	k1gInSc6	plakát
je	být	k5eAaImIp3nS	být
zobrazen	zobrazen	k2eAgMnSc1d1	zobrazen
mírumilovný	mírumilovný	k2eAgMnSc1d1	mírumilovný
pták	pták	k1gMnSc1	pták
nesoucí	nesoucí	k2eAgFnSc2d1	nesoucí
vítězný	vítězný	k2eAgInSc4d1	vítězný
věnec	věnec	k1gInSc4	věnec
a	a	k8xC	a
letící	letící	k2eAgMnSc1d1	letící
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
vysokohorskou	vysokohorský	k2eAgFnSc7d1	vysokohorská
scenérií	scenérie	k1gFnSc7	scenérie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
jízdu	jízda	k1gFnSc4	jízda
bobu	bob	k1gInSc2	bob
dolů	dolů	k6eAd1	dolů
z	z	k7c2	z
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Olympijské	olympijský	k2eAgNnSc1d1	Olympijské
kluziště	kluziště	k1gNnSc1	kluziště
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
(	(	kIx(	(
<g/>
uprostřed	uprostřed	k7c2	uprostřed
oválu	ovál	k1gInSc2	ovál
hokejové	hokejový	k2eAgNnSc4d1	hokejové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
,	,	kIx,	,
krajní	krajní	k2eAgFnSc4d1	krajní
2	[number]	k4	2
části	část	k1gFnSc2	část
pro	pro	k7c4	pro
krasobruslení	krasobruslení	k1gNnSc4	krasobruslení
<g/>
,	,	kIx,	,
rychlobruslařská	rychlobruslařský	k2eAgFnSc1d1	rychlobruslařská
dráha	dráha	k1gFnSc1	dráha
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
<g/>
,	,	kIx,	,
vedle	vedle	k6eAd1	vedle
malé	malý	k2eAgNnSc4d1	malé
hřiště	hřiště	k1gNnSc4	hřiště
pro	pro	k7c4	pro
curling	curling	k1gInSc4	curling
<g/>
)	)	kIx)	)
Skokanský	skokanský	k2eAgInSc4d1	skokanský
můstek	můstek	k1gInSc4	můstek
poblíž	poblíž	k7c2	poblíž
ledovce	ledovec	k1gInSc2	ledovec
Les	les	k1gInSc4	les
Bossons	Bossons	k1gInSc1	Bossons
Bobová	bobový	k2eAgFnSc1d1	bobová
dráha	dráha	k1gFnSc1	dráha
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
1444	[number]	k4	1444
m	m	kA	m
Běžecké	běžecký	k2eAgFnSc2d1	běžecká
lyžařské	lyžařský	k2eAgFnSc2d1	lyžařská
tratě	trať	k1gFnSc2	trať
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
ještě	ještě	k9	ještě
neexistovala	existovat	k5eNaImAgFnS	existovat
tradice	tradice	k1gFnSc1	tradice
olympijské	olympijský	k2eAgFnSc2d1	olympijská
štafety	štafeta	k1gFnSc2	štafeta
s	s	k7c7	s
olympijským	olympijský	k2eAgInSc7d1	olympijský
ohněm	oheň	k1gInSc7	oheň
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
zapalování	zapalování	k1gNnSc1	zapalování
při	při	k7c6	při
zahajování	zahajování	k1gNnSc6	zahajování
olympiády	olympiáda	k1gFnSc2	olympiáda
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
plál	plát	k5eAaImAgInS	plát
olympijský	olympijský	k2eAgInSc1d1	olympijský
oheň	oheň	k1gInSc1	oheň
z	z	k7c2	z
Olympie	Olympia	k1gFnSc2	Olympia
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
se	se	k3xPyFc4	se
tradiční	tradiční	k2eAgFnSc1d1	tradiční
štafeta	štafeta	k1gFnSc1	štafeta
s	s	k7c7	s
ohněm	oheň	k1gInSc7	oheň
objevila	objevit	k5eAaPmAgFnS	objevit
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
St.	st.	kA	st.
Moritzi	moritz	k1gInSc6	moritz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
olympijský	olympijský	k2eAgInSc1d1	olympijský
oheň	oheň	k1gInSc1	oheň
zapalován	zapalován	k2eAgInSc1d1	zapalován
přímo	přímo	k6eAd1	přímo
při	při	k7c6	při
zahajovacím	zahajovací	k2eAgInSc6d1	zahajovací
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
a	a	k8xC	a
tak	tak	k9	tak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
<g/>
.	.	kIx.	.
</s>
<s>
Mapa	mapa	k1gFnSc1	mapa
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
zemí	zem	k1gFnPc2	zem
Akce	akce	k1gFnSc2	akce
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
disciplinách	disciplina	k1gFnPc6	disciplina
258	[number]	k4	258
sportovců	sportovec	k1gMnPc2	sportovec
(	(	kIx(	(
<g/>
245	[number]	k4	245
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
13	[number]	k4	13
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
ze	z	k7c2	z
šestnácti	šestnáct	k4xCc2	šestnáct
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tzv.	tzv.	kA	tzv.
ukázkových	ukázkový	k2eAgInPc6d1	ukázkový
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
označeny	označit	k5eAaPmNgInP	označit
dodatečně	dodatečně	k6eAd1	dodatečně
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
MOV	MOV	kA	MOV
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
bojovalo	bojovat	k5eAaImAgNnS	bojovat
dalších	další	k2eAgMnPc2d1	další
42	[number]	k4	42
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
zastupovalo	zastupovat	k5eAaImAgNnS	zastupovat
26	[number]	k4	26
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
soutěžních	soutěžní	k2eAgFnPc6d1	soutěžní
disciplínách	disciplína	k1gFnPc6	disciplína
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
3	[number]	k4	3
v	v	k7c6	v
ukázkových	ukázkový	k2eAgInPc6d1	ukázkový
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dějiště	dějiště	k1gNnSc2	dějiště
her	hra	k1gFnPc2	hra
dorazili	dorazit	k5eAaPmAgMnP	dorazit
i	i	k9	i
sportovci	sportovec	k1gMnPc1	sportovec
z	z	k7c2	z
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
,	,	kIx,	,
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
a	a	k8xC	a
Argentiny	Argentina	k1gFnSc2	Argentina
(	(	kIx(	(
<g/>
ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
nezúčastnili	zúčastnit	k5eNaPmAgMnP	zúčastnit
ani	ani	k9	ani
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
otevření	otevření	k1gNnSc2	otevření
her	hra	k1gFnPc2	hra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
olympijských	olympijský	k2eAgNnPc6d1	Olympijské
sportovištích	sportoviště	k1gNnPc6	sportoviště
nestartovali	startovat	k5eNaBmAgMnP	startovat
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
hokejového	hokejový	k2eAgInSc2d1	hokejový
turnaje	turnaj	k1gInSc2	turnaj
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
již	již	k6eAd1	již
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
výsledky	výsledek	k1gInPc4	výsledek
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Zápasy	zápas	k1gInPc1	zápas
jsou	být	k5eAaImIp3nP	být
řazeny	řadit	k5eAaImNgInP	řadit
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
dni	den	k1gInSc6	den
podle	podle	k7c2	podle
časové	časový	k2eAgFnSc2d1	časová
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgNnSc4	který
probíhaly	probíhat	k5eAaImAgFnP	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc4d3	nejdelší
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Chamonix	Chamonix	k1gNnSc2	Chamonix
měli	mít	k5eAaImAgMnP	mít
argentinští	argentinský	k2eAgMnPc1d1	argentinský
bobisté	bobista	k1gMnPc1	bobista
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zahájení	zahájení	k1gNnSc6	zahájení
se	se	k3xPyFc4	se
však	však	k9	však
neobjevili	objevit	k5eNaPmAgMnP	objevit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
tréninku	trénink	k1gInSc6	trénink
s	s	k7c7	s
bobem	bob	k1gInSc7	bob
těžce	těžce	k6eAd1	těžce
havarovali	havarovat	k5eAaPmAgMnP	havarovat
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
převezeni	převézt	k5eAaPmNgMnP	převézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
první	první	k4xOgNnSc1	první
neštěstí	neštěstí	k1gNnSc1	neštěstí
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Olympijský	olympijský	k2eAgInSc1d1	olympijský
slib	slib	k1gInSc1	slib
za	za	k7c4	za
všechny	všechen	k3xTgMnPc4	všechen
závodníky	závodník	k1gMnPc4	závodník
skládal	skládat	k5eAaImAgMnS	skládat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
lyžař	lyžař	k1gMnSc1	lyžař
Camille	Camille	k1gFnSc2	Camille
Mandrillon	Mandrillon	k1gInSc1	Mandrillon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
velitelem	velitel	k1gMnSc7	velitel
vojenské	vojenský	k2eAgFnSc2d1	vojenská
hlídky	hlídka	k1gFnSc2	hlídka
a	a	k8xC	a
soutěžil	soutěžit	k5eAaImAgInS	soutěžit
tedy	tedy	k9	tedy
v	v	k7c6	v
neolympijské	olympijský	k2eNgFnSc6d1	neolympijská
disciplíně	disciplína	k1gFnSc6	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězem	vítěz	k1gMnSc7	vítěz
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
zimních	zimní	k2eAgFnPc2d1	zimní
olympiád	olympiáda	k1gFnPc2	olympiáda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
Charles	Charles	k1gMnSc1	Charles
Jewtraw	Jewtraw	k1gMnSc1	Jewtraw
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
závodu	závod	k1gInSc2	závod
na	na	k7c4	na
500	[number]	k4	500
m.	m.	k?	m.
Hrdinou	Hrdina	k1gMnSc7	Hrdina
lyžařských	lyžařský	k2eAgFnPc2d1	lyžařská
disciplín	disciplína	k1gFnPc2	disciplína
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Nor	Nor	k1gMnSc1	Nor
Thorleif	Thorleif	k1gMnSc1	Thorleif
Haug	Haug	k1gMnSc1	Haug
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
tři	tři	k4xCgFnPc4	tři
zlaté	zlatý	k2eAgFnPc4d1	zlatá
medaile	medaile	k1gFnPc4	medaile
v	v	k7c6	v
běžeckých	běžecký	k2eAgFnPc6d1	běžecká
disciplinách	disciplina	k1gFnPc6	disciplina
a	a	k8xC	a
závodě	závod	k1gInSc6	závod
sdruženém	sdružený	k2eAgInSc6d1	sdružený
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
ještě	ještě	k9	ještě
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
za	za	k7c7	za
skoky	skok	k1gInPc7	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Běh	běh	k1gInSc1	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
na	na	k7c4	na
50	[number]	k4	50
km	km	kA	km
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
za	za	k7c2	za
nesmírně	smírně	k6eNd1	smírně
těžkých	těžký	k2eAgFnPc2d1	těžká
podmínek	podmínka	k1gFnPc2	podmínka
částečně	částečně	k6eAd1	částečně
ve	v	k7c6	v
sněhové	sněhový	k2eAgFnSc6d1	sněhová
bouřce	bouřka	k1gFnSc6	bouřka
a	a	k8xC	a
zledovatělé	zledovatělý	k2eAgFnSc6d1	zledovatělá
stopě	stopa	k1gFnSc6	stopa
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
vedoucí	vedoucí	k1gMnPc1	vedoucí
týmů	tým	k1gInPc2	tým
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
pustit	pustit	k5eAaPmF	pustit
své	svůj	k3xOyFgMnPc4	svůj
běžce	běžec	k1gMnPc4	běžec
na	na	k7c4	na
trať	trať	k1gFnSc4	trať
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
dvanáct	dvanáct	k4xCc1	dvanáct
běžců	běžec	k1gMnPc2	běžec
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
závodu	závod	k1gInSc2	závod
z	z	k7c2	z
tratě	trať	k1gFnSc2	trať
odstoupilo	odstoupit	k5eAaPmAgNnS	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
18	[number]	k4	18
km	km	kA	km
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
protest	protest	k1gInSc4	protest
finské	finský	k2eAgFnSc2d1	finská
výpravy	výprava	k1gFnSc2	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
původnímu	původní	k2eAgInSc3d1	původní
programu	program	k1gInSc3	program
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
i	i	k9	i
jako	jako	k9	jako
běh	běh	k1gInSc4	běh
pro	pro	k7c4	pro
závod	závod	k1gInSc4	závod
sdružený	sdružený	k2eAgInSc4d1	sdružený
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
původně	původně	k6eAd1	původně
konat	konat	k5eAaImF	konat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
dvě	dva	k4xCgNnPc4	dva
místa	místo	k1gNnPc4	místo
obsadili	obsadit	k5eAaPmAgMnP	obsadit
přitom	přitom	k6eAd1	přitom
Norové	Nor	k1gMnPc1	Nor
Haug	Hauga	k1gFnPc2	Hauga
a	a	k8xC	a
Grø	Grø	k1gMnPc2	Grø
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
přihlášeni	přihlásit	k5eAaPmNgMnP	přihlásit
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
závodu	závod	k1gInSc3	závod
sdruženému	sdružený	k2eAgInSc3d1	sdružený
<g/>
.	.	kIx.	.
</s>
<s>
Jury	jura	k1gFnPc1	jura
protest	protest	k1gInSc4	protest
Finů	Fin	k1gMnPc2	Fin
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
a	a	k8xC	a
tito	tento	k3xDgMnPc1	tento
závodníci	závodník	k1gMnPc1	závodník
tak	tak	k6eAd1	tak
získali	získat	k5eAaPmAgMnP	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
a	a	k8xC	a
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
18	[number]	k4	18
km	km	kA	km
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
původně	původně	k6eAd1	původně
nebyli	být	k5eNaImAgMnP	být
přihlášeni	přihlásit	k5eAaPmNgMnP	přihlásit
<g/>
.	.	kIx.	.
</s>
<s>
Přesně	přesně	k6eAd1	přesně
po	po	k7c6	po
padesáti	padesát	k4xCc6	padesát
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
olympiády	olympiáda	k1gFnSc2	olympiáda
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
zjistil	zjistit	k5eAaPmAgMnS	zjistit
Nor	Nor	k1gMnSc1	Nor
Jakob	Jakob	k1gMnSc1	Jakob
Vaage	Vaage	k1gFnSc1	Vaage
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
skocích	skok	k1gInPc6	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
se	se	k3xPyFc4	se
rozhodčí	rozhodčí	k1gMnPc1	rozhodčí
pomýlili	pomýlit	k5eAaPmAgMnP	pomýlit
ve	v	k7c6	v
výpočtu	výpočet	k1gInSc6	výpočet
a	a	k8xC	a
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
mylně	mylně	k6eAd1	mylně
zařadili	zařadit	k5eAaPmAgMnP	zařadit
Nora	Nor	k1gMnSc4	Nor
Hauga	Haug	k1gMnSc2	Haug
namísto	namísto	k7c2	namísto
Američana	Američan	k1gMnSc2	Američan
Haugena	Haugen	k1gMnSc2	Haugen
<g/>
.	.	kIx.	.
</s>
<s>
Haugova	Haugův	k2eAgFnSc1d1	Haugův
dcera	dcera	k1gFnSc1	dcera
pak	pak	k6eAd1	pak
dodatečně	dodatečně	k6eAd1	dodatečně
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
86	[number]	k4	86
let	léto	k1gNnPc2	léto
starému	starý	k2eAgMnSc3d1	starý
Haugenovi	Haugen	k1gMnSc3	Haugen
odevzdala	odevzdat	k5eAaPmAgFnS	odevzdat
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
příslušně	příslušně	k6eAd1	příslušně
opravena	opravit	k5eAaPmNgFnS	opravit
i	i	k9	i
výsledková	výsledkový	k2eAgFnSc1d1	výsledková
listina	listina	k1gFnSc1	listina
<g/>
.	.	kIx.	.
</s>
<s>
Hokejový	hokejový	k2eAgInSc1d1	hokejový
turnaj	turnaj	k1gInSc1	turnaj
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
tým	tým	k1gInSc4	tým
Kanady	Kanada	k1gFnSc2	Kanada
reprezentovaný	reprezentovaný	k2eAgInSc4d1	reprezentovaný
mužstvem	mužstvo	k1gNnSc7	mužstvo
Toronto	Toronto	k1gNnSc4	Toronto
Granites	Granites	k1gInSc1	Granites
s	s	k7c7	s
impozantním	impozantní	k2eAgInSc7d1	impozantní
skórem	skór	k1gInSc7	skór
110	[number]	k4	110
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
představila	představit	k5eAaPmAgFnS	představit
tehdy	tehdy	k6eAd1	tehdy
dvanáctiletá	dvanáctiletý	k2eAgFnSc1d1	dvanáctiletá
<g/>
,	,	kIx,	,
budoucí	budoucí	k2eAgFnSc1d1	budoucí
krasobruslařská	krasobruslařský	k2eAgFnSc1d1	krasobruslařská
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
Sonja	Sonj	k2eAgFnSc1d1	Sonja
Henie	Henie	k1gFnSc1	Henie
<g/>
.	.	kIx.	.
</s>
<s>
Vítězka	vítězka	k1gFnSc1	vítězka
soutěže	soutěž	k1gFnSc2	soutěž
krasobruslařek	krasobruslařka	k1gFnPc2	krasobruslařka
<g/>
,	,	kIx,	,
Herma	Herma	k1gFnSc1	Herma
Plancková-Szabóová	Plancková-Szabóová	k1gFnSc1	Plancková-Szabóová
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
účast	účast	k1gFnSc1	účast
okomentovala	okomentovat	k5eAaPmAgFnS	okomentovat
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
S	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
nezávodím	závodit	k5eNaImIp1nS	závodit
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
sportovcem	sportovec	k1gMnSc7	sportovec
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
finský	finský	k2eAgMnSc1d1	finský
rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
Clas	Clasa	k1gFnPc2	Clasa
Thunberg	Thunberg	k1gMnSc1	Thunberg
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
tři	tři	k4xCgFnPc4	tři
zlaté	zlatá	k1gFnPc4	zlatá
a	a	k8xC	a
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
stříbrné	stříbrná	k1gFnSc6	stříbrná
a	a	k8xC	a
bronzové	bronzový	k2eAgFnSc3d1	bronzová
medaili	medaile	k1gFnSc3	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
jediným	jediný	k2eAgMnSc7d1	jediný
vítězem	vítěz	k1gMnSc7	vítěz
čtyřboje	čtyřboj	k1gInSc2	čtyřboj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
od	od	k7c2	od
následujících	následující	k2eAgFnPc2d1	následující
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
už	už	k6eAd1	už
nebyl	být	k5eNaImAgMnS	být
do	do	k7c2	do
programu	program	k1gInSc2	program
zařazován	zařazovat	k5eAaImNgInS	zařazovat
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
a	a	k8xC	a
také	také	k9	také
zatím	zatím	k6eAd1	zatím
naposledy	naposledy	k6eAd1	naposledy
se	se	k3xPyFc4	se
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
udělovaly	udělovat	k5eAaImAgFnP	udělovat
medaile	medaile	k1gFnPc1	medaile
za	za	k7c4	za
rychlobruslařský	rychlobruslařský	k2eAgInSc4d1	rychlobruslařský
čtyřboj	čtyřboj	k1gInSc4	čtyřboj
<g/>
.	.	kIx.	.
</s>
<s>
Pravidla	pravidlo	k1gNnPc1	pravidlo
soutěže	soutěž	k1gFnSc2	soutěž
čtyřsedadlových	čtyřsedadlový	k2eAgInPc2d1	čtyřsedadlový
bobů	bob	k1gInPc2	bob
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
účast	účast	k1gFnSc1	účast
pětičlenných	pětičlenný	k2eAgFnPc2d1	pětičlenná
posádek	posádka	k1gFnPc2	posádka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
zemí	zem	k1gFnPc2	zem
využilo	využít	k5eAaPmAgNnS	využít
<g/>
.	.	kIx.	.
</s>
<s>
Těžká	těžký	k2eAgFnSc1d1	těžká
bobová	bobový	k2eAgFnSc1d1	bobová
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
1444	[number]	k4	1444
m	m	kA	m
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
s	s	k7c7	s
18	[number]	k4	18
zatáčkami	zatáčka	k1gFnPc7	zatáčka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
osádky	osádka	k1gFnPc4	osádka
osudnou	osudný	k2eAgFnSc4d1	osudná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
těžké	těžký	k2eAgInPc4d1	těžký
pády	pád	k1gInPc4	pád
při	při	k7c6	při
tréninku	trénink	k1gInSc6	trénink
nenastoupily	nastoupit	k5eNaPmAgInP	nastoupit
k	k	k7c3	k
závodům	závod	k1gInPc3	závod
osádky	osádka	k1gFnSc2	osádka
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
a	a	k8xC	a
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
před	před	k7c7	před
zimní	zimní	k2eAgFnSc7d1	zimní
olympiádou	olympiáda	k1gFnSc7	olympiáda
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
MOV	MOV	kA	MOV
<g/>
,	,	kIx,	,
že	že	k8xS	že
curling	curling	k1gInSc1	curling
na	na	k7c4	na
ZOH	ZOH	kA	ZOH
1924	[number]	k4	1924
nebyl	být	k5eNaImAgInS	být
jen	jen	k6eAd1	jen
ukázkovým	ukázkový	k2eAgInSc7d1	ukázkový
sportem	sport	k1gInSc7	sport
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sportem	sport	k1gInSc7	sport
oficiálním	oficiální	k2eAgInSc7d1	oficiální
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
soutěžilo	soutěžit	k5eAaImAgNnS	soutěžit
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
olympijských	olympijský	k2eAgFnPc6d1	olympijská
a	a	k8xC	a
dvou	dva	k4xCgInPc6	dva
exhibičních	exhibiční	k2eAgInPc6d1	exhibiční
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
sportů	sport	k1gInPc2	sport
do	do	k7c2	do
odvětví	odvětví	k1gNnSc2	odvětví
bylo	být	k5eAaImAgNnS	být
trochu	trochu	k6eAd1	trochu
odlišné	odlišný	k2eAgNnSc1d1	odlišné
od	od	k7c2	od
dělení	dělení	k1gNnSc2	dělení
nynějšího	nynější	k2eAgNnSc2d1	nynější
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
následujících	následující	k2eAgFnPc6d1	následující
stránkách	stránka	k1gFnPc6	stránka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
si	se	k3xPyFc3	se
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
výsledkové	výsledkový	k2eAgFnPc4d1	výsledková
listiny	listina	k1gFnPc4	listina
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sportovních	sportovní	k2eAgFnPc2d1	sportovní
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
:	:	kIx,	:
Boby	bob	k1gInPc1	bob
Bruslení	bruslení	k1gNnSc2	bruslení
Krasobruslení	krasobruslení	k1gNnSc2	krasobruslení
Rychlobruslení	rychlobruslení	k1gNnSc2	rychlobruslení
Lední	lední	k2eAgInSc1d1	lední
hokej	hokej	k1gInSc1	hokej
Lyžování	lyžování	k1gNnSc1	lyžování
Běh	běh	k1gInSc1	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Sdružený	sdružený	k2eAgInSc4d1	sdružený
závod	závod	k1gInSc4	závod
Skoky	skok	k1gInPc7	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Ukázkové	ukázkový	k2eAgInPc4d1	ukázkový
sporty	sport	k1gInPc4	sport
Curling	Curling	k1gInSc1	Curling
Závod	závod	k1gInSc1	závod
vojenských	vojenský	k2eAgFnPc2d1	vojenská
hlídek	hlídka	k1gFnPc2	hlídka
Na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
nezískali	získat	k5eNaPmAgMnP	získat
českoslovenští	československý	k2eAgMnPc1d1	československý
reprezentanti	reprezentant	k1gMnPc1	reprezentant
žádnou	žádný	k3yNgFnSc4	žádný
ze	z	k7c2	z
43	[number]	k4	43
rozdělených	rozdělený	k2eAgFnPc2d1	rozdělená
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
ale	ale	k9	ale
nevedli	vést	k5eNaImAgMnP	vést
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
medaili	medaile	k1gFnSc4	medaile
byl	být	k5eAaImAgMnS	být
krasobruslař	krasobruslař	k1gMnSc1	krasobruslař
Josef	Josef	k1gMnSc1	Josef
Slíva	Slíva	k1gMnSc1	Slíva
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
jen	jen	k9	jen
díky	díky	k7c3	díky
nepřízni	nepřízeň	k1gFnSc3	nepřízeň
rakouských	rakouský	k2eAgMnPc2d1	rakouský
rozhodčích	rozhodčí	k1gMnPc2	rozhodčí
nezískal	získat	k5eNaPmAgInS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
odsunut	odsunout	k5eAaPmNgInS	odsunout
na	na	k7c4	na
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ostatně	ostatně	k6eAd1	ostatně
i	i	k9	i
jury	jury	k1gFnSc1	jury
uznala	uznat	k5eAaPmAgFnS	uznat
protest	protest	k1gInSc4	protest
československé	československý	k2eAgFnSc2d1	Československá
výpravy	výprava	k1gFnSc2	výprava
proti	proti	k7c3	proti
rozhodování	rozhodování	k1gNnSc3	rozhodování
rozhodčích	rozhodčí	k2eAgFnPc2d1	rozhodčí
a	a	k8xC	a
nejkřiklavěji	křiklavě	k6eAd3	křiklavě
rozhodujícího	rozhodující	k2eAgMnSc2d1	rozhodující
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
výbornou	výborný	k2eAgFnSc4d1	výborná
volnou	volný	k2eAgFnSc4d1	volná
jízdu	jízda	k1gFnSc4	jízda
československého	československý	k2eAgMnSc4d1	československý
reprezentanta	reprezentant	k1gMnSc4	reprezentant
ohodnotil	ohodnotit	k5eAaPmAgMnS	ohodnotit
jako	jako	k9	jako
nejhorší	zlý	k2eAgMnSc1d3	nejhorší
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sboru	sbor	k1gInSc2	sbor
odvolala	odvolat	k5eAaPmAgFnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
neměnily	měnit	k5eNaImAgFnP	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Hokejisté	hokejista	k1gMnPc1	hokejista
obhajovali	obhajovat	k5eAaImAgMnP	obhajovat
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
z	z	k7c2	z
VII	VII	kA	VII
<g/>
.	.	kIx.	.
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Antverpách	Antverpy	k1gFnPc6	Antverpy
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dlouhotrvajícím	dlouhotrvající	k2eAgInPc3d1	dlouhotrvající
rozporům	rozpor	k1gInPc3	rozpor
v	v	k7c6	v
hokejovém	hokejový	k2eAgInSc6d1	hokejový
svazu	svaz	k1gInSc6	svaz
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
vyřešeny	vyřešit	k5eAaPmNgInP	vyřešit
až	až	k9	až
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
jejich	jejich	k3xOp3gInSc7	jejich
odjezdem	odjezd	k1gInSc7	odjezd
<g/>
,	,	kIx,	,
však	však	k9	však
neodjížděli	odjíždět	k5eNaImAgMnP	odjíždět
v	v	k7c6	v
optimálním	optimální	k2eAgNnSc6d1	optimální
složení	složení	k1gNnSc6	složení
a	a	k8xC	a
po	po	k7c6	po
nedostatečné	dostatečný	k2eNgFnSc6d1	nedostatečná
přípravě	příprava	k1gFnSc6	příprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
turnaje	turnaj	k1gInSc2	turnaj
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
největší	veliký	k2eAgFnSc4d3	veliký
prohru	prohra	k1gFnSc4	prohra
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
když	když	k8xS	když
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
Kanadě	Kanada	k1gFnSc3	Kanada
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
prohře	prohra	k1gFnSc6	prohra
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
ze	z	k7c2	z
základní	základní	k2eAgFnSc2d1	základní
skupiny	skupina	k1gFnSc2	skupina
nepostoupili	postoupit	k5eNaPmAgMnP	postoupit
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
slušných	slušný	k2eAgFnPc2d1	slušná
umístění	umístění	k1gNnSc4	umístění
naopak	naopak	k6eAd1	naopak
dosahovali	dosahovat	k5eAaImAgMnP	dosahovat
českoslovenští	československý	k2eAgMnPc1d1	československý
lyžaři	lyžař	k1gMnPc1	lyžař
a	a	k8xC	a
sdruženáři	sdruženář	k1gMnPc1	sdruženář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jako	jako	k8xS	jako
jedni	jeden	k4xCgMnPc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
dokázali	dokázat	k5eAaPmAgMnP	dokázat
aspoň	aspoň	k9	aspoň
částečně	částečně	k6eAd1	částečně
uspět	uspět	k5eAaPmF	uspět
mezi	mezi	k7c7	mezi
nedostižnými	nedostižnými	k?	nedostižnými
Skandinávci	Skandinávec	k1gMnPc1	Skandinávec
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgNnSc7d3	nejlepší
umístěním	umístění	k1gNnSc7	umístění
lyžařů	lyžař	k1gMnPc2	lyžař
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
bylo	být	k5eAaImAgNnS	být
šesté	šestý	k4xOgNnSc1	šestý
místo	místo	k1gNnSc1	místo
sdruženáře	sdruženář	k1gMnSc2	sdruženář
Josefa	Josef	k1gMnSc2	Josef
Adolfa	Adolf	k1gMnSc2	Adolf
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Československo	Československo	k1gNnSc4	Československo
na	na	k7c6	na
Zimních	zimní	k2eAgFnPc6d1	zimní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Procházka	Procházka	k1gMnSc1	Procházka
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zimné	zimný	k2eAgFnSc2d1	zimná
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
(	(	kIx(	(
<g/>
Od	od	k7c2	od
Chamonix	Chamonix	k1gNnSc2	Chamonix
1924	[number]	k4	1924
po	po	k7c4	po
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
–	–	k?	–
nakl	nakl	k1gInSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Šport	Šport	k1gInSc1	Šport
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
Pavol	Pavol	k1gInSc1	Pavol
Kršák	Kršák	k1gInSc1	Kršák
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Novoveké	Novoveký	k2eAgFnSc2d1	Novoveký
olympiády	olympiáda	k1gFnSc2	olympiáda
-	-	kIx~	-
nakl	nakl	k1gMnSc1	nakl
<g/>
.	.	kIx.	.
</s>
<s>
Šport	Šport	k1gInSc1	Šport
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
1924	[number]	k4	1924
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Stránka	stránka	k1gFnSc1	stránka
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Chamonix	Chamonix	k1gNnSc6	Chamonix
1924	[number]	k4	1924
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
zpráva	zpráva	k1gFnSc1	zpráva
z	z	k7c2	z
letních	letní	k2eAgFnPc2d1	letní
a	a	k8xC	a
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
1924	[number]	k4	1924
(	(	kIx(	(
<g/>
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
pdf	pdf	k?	pdf
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
hokejisté	hokejista	k1gMnPc1	hokejista
na	na	k7c6	na
první	první	k4xOgFnSc6	první
olympiádě	olympiáda	k1gFnSc6	olympiáda
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
debakl	debakl	k1gInSc4	debakl
U	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
zimních	zimní	k2eAgFnPc2d1	zimní
her	hra	k1gFnPc2	hra
byl	být	k5eAaImAgMnS	být
Čech	Čech	k1gMnSc1	Čech
<g/>
.	.	kIx.	.
</s>
