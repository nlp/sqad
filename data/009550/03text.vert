<p>
<s>
Ladožské	ladožský	k2eAgNnSc1d1	Ladožské
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Л	Л	k?	Л
о	о	k?	о
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
Laatokka	Laatokka	k1gFnSc1	Laatokka
<g/>
,	,	kIx,	,
karelsky	karelsky	k6eAd1	karelsky
Luadogu	Luadog	k1gInSc3	Luadog
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jezero	jezero	k1gNnSc1	jezero
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
v	v	k7c6	v
Karelské	karelský	k2eAgFnSc6d1	Karelská
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
v	v	k7c6	v
Leningradské	leningradský	k2eAgFnSc6d1	Leningradská
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
tvoří	tvořit	k5eAaImIp3nS	tvořit
severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
Karelské	karelský	k2eAgFnSc2d1	Karelská
šíje	šíj	k1gFnSc2	šíj
<g/>
,	,	kIx,	,
úzkého	úzký	k2eAgInSc2d1	úzký
pruhu	pruh	k1gInSc2	pruh
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
Finským	finský	k2eAgInSc7d1	finský
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Kotlina	kotlina	k1gFnSc1	kotlina
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
tektonického	tektonický	k2eAgInSc2d1	tektonický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dotvořena	dotvořit	k5eAaPmNgFnS	dotvořit
čtvrtohorními	čtvrtohorní	k2eAgInPc7d1	čtvrtohorní
pevninskými	pevninský	k2eAgInPc7d1	pevninský
ledovci	ledovec	k1gInPc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
17	[number]	k4	17
700	[number]	k4	700
km2	km2	k4	km2
(	(	kIx(	(
<g/>
s	s	k7c7	s
ostrovy	ostrov	k1gInPc7	ostrov
18	[number]	k4	18
135	[number]	k4	135
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
219	[number]	k4	219
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
průměrně	průměrně	k6eAd1	průměrně
83	[number]	k4	83
km	km	kA	km
široké	široký	k2eAgNnSc1d1	široké
(	(	kIx(	(
<g/>
maximální	maximální	k2eAgFnSc1d1	maximální
šířka	šířka	k1gFnSc1	šířka
je	být	k5eAaImIp3nS	být
138	[number]	k4	138
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrnou	průměrný	k2eAgFnSc4d1	průměrná
hloubku	hloubka	k1gFnSc4	hloubka
má	mít	k5eAaImIp3nS	mít
51	[number]	k4	51
m	m	kA	m
a	a	k8xC	a
maximální	maximální	k2eAgFnSc1d1	maximální
230	[number]	k4	230
m	m	kA	m
(	(	kIx(	(
<g/>
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
západně	západně	k6eAd1	západně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Valaam	Valaam	k1gInSc1	Valaam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
hloubka	hloubka	k1gFnSc1	hloubka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
70	[number]	k4	70
až	až	k9	až
200	[number]	k4	200
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
20	[number]	k4	20
až	až	k9	až
70	[number]	k4	70
m.	m.	k?	m.
Rozloha	rozloha	k1gFnSc1	rozloha
povodí	povodí	k1gNnSc2	povodí
je	být	k5eAaImIp3nS	být
276	[number]	k4	276
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
objem	objem	k1gInSc1	objem
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
908	[number]	k4	908
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
hladiny	hladina	k1gFnSc2	hladina
jezera	jezero	k1gNnSc2	jezero
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Pobřeží	pobřeží	k1gNnSc2	pobřeží
==	==	k?	==
</s>
</p>
<p>
<s>
Severní	severní	k2eAgInPc1d1	severní
břehy	břeh	k1gInPc1	břeh
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
vysoké	vysoký	k2eAgInPc1d1	vysoký
<g/>
,	,	kIx,	,
skalnaté	skalnatý	k2eAgInPc1d1	skalnatý
<g/>
,	,	kIx,	,
členité	členitý	k2eAgInPc1d1	členitý
s	s	k7c7	s
hluboko	hluboko	k6eAd1	hluboko
zařezanými	zařezaný	k2eAgInPc7d1	zařezaný
zálivy	záliv	k1gInPc7	záliv
podobnými	podobný	k2eAgInPc7d1	podobný
fjordům	fjord	k1gInPc3	fjord
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
pokryté	pokrytý	k2eAgInPc1d1	pokrytý
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
lesnatých	lesnatý	k2eAgInPc2d1	lesnatý
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
úzké	úzký	k2eAgInPc4d1	úzký
průlivy	průliv	k1gInPc4	průliv
(	(	kIx(	(
<g/>
ш	ш	k?	ш
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInPc1d1	jižní
břehy	břeh	k1gInPc1	břeh
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
mírně	mírně	k6eAd1	mírně
členité	členitý	k2eAgFnPc1d1	členitá
porostlé	porostlý	k2eAgFnPc1d1	porostlá
vrbou	vrba	k1gFnSc7	vrba
nebo	nebo	k8xC	nebo
olší	olše	k1gFnSc7	olše
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
písečných	písečný	k2eAgFnPc2d1	písečná
a	a	k8xC	a
valounových	valounový	k2eAgFnPc2d1	valounová
pláží	pláž	k1gFnPc2	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Místy	místy	k6eAd1	místy
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
staré	starý	k2eAgInPc1d1	starý
břehové	břehový	k2eAgInPc1d1	břehový
valy	val	k1gInPc1	val
porostlé	porostlý	k2eAgInPc1d1	porostlý
borovicemi	borovice	k1gFnPc7	borovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dno	dno	k1gNnSc1	dno
==	==	k?	==
</s>
</p>
<p>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
dna	dno	k1gNnSc2	dno
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
složitý	složitý	k2eAgInSc1d1	složitý
<g/>
.	.	kIx.	.
</s>
<s>
Střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
hluboké	hluboký	k2eAgFnPc1d1	hluboká
propadliny	propadlina	k1gFnPc1	propadlina
s	s	k7c7	s
mělčími	mělký	k2eAgInPc7d2	mělčí
úseky	úsek	k1gInPc7	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Převládající	převládající	k2eAgFnSc1d1	převládající
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
100	[number]	k4	100
m.	m.	k?	m.
Dno	dno	k1gNnSc4	dno
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
rovnější	rovný	k2eAgNnSc1d2	rovnější
<g/>
,	,	kIx,	,
hloubky	hloubka	k1gFnPc1	hloubka
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
zmenšují	zmenšovat	k5eAaImIp3nP	zmenšovat
ze	z	k7c2	z
100	[number]	k4	100
m	m	kA	m
na	na	k7c4	na
10	[number]	k4	10
m	m	kA	m
i	i	k8xC	i
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
písečných	písečný	k2eAgFnPc2d1	písečná
i	i	k8xC	i
kamenitých	kamenitý	k2eAgFnPc2d1	kamenitá
kos	kosa	k1gFnPc2	kosa
a	a	k8xC	a
mělčin	mělčina	k1gFnPc2	mělčina
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
dna	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
centrální	centrální	k2eAgFnSc6d1	centrální
hluboké	hluboký	k2eAgFnSc6d1	hluboká
části	část	k1gFnSc6	část
jezera	jezero	k1gNnSc2	jezero
jílový	jílový	k2eAgInSc1d1	jílový
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
písečný	písečný	k2eAgInSc1d1	písečný
s	s	k7c7	s
valouny	valoun	k1gInPc7	valoun
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
břehu	břeh	k1gInSc2	břeh
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
mnohých	mnohý	k2eAgNnPc6d1	mnohé
místech	místo	k1gNnPc6	místo
nahromaděny	nahromaděn	k2eAgFnPc1d1	nahromaděna
valouny	valoun	k1gInPc4	valoun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ostrovy	ostrov	k1gInPc1	ostrov
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
660	[number]	k4	660
ostrovů	ostrov	k1gInPc2	ostrov
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
435	[number]	k4	435
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
500	[number]	k4	500
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
severozápadního	severozápadní	k2eAgNnSc2d1	severozápadní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
65	[number]	k4	65
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
Valaamské	Valaamský	k2eAgInPc1d1	Valaamský
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgInSc1d3	nejznámější
–	–	k?	–
50	[number]	k4	50
ostrovů	ostrov	k1gInPc2	ostrov
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
36	[number]	k4	36
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Západní	západní	k2eAgNnSc1d1	západní
souostroví	souostroví	k1gNnSc1	souostroví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
ostrovy	ostrov	k1gInPc4	ostrov
jsou	být	k5eAaImIp3nP	být
Riskalansari	Riskalansar	k1gFnPc1	Riskalansar
<g/>
,	,	kIx,	,
Mantinsari	Mantinsari	k1gNnSc1	Mantinsari
<g/>
,	,	kIx,	,
Kilpola	Kilpola	k1gFnSc1	Kilpola
<g/>
,	,	kIx,	,
Tulolasari	Tulolasari	k1gNnSc1	Tulolasari
a	a	k8xC	a
Vallam	Vallam	k1gInSc1	Vallam
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
jezera	jezero	k1gNnSc2	jezero
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ostrov	ostrov	k1gInSc1	ostrov
Konevec	Konevec	k1gInSc1	Konevec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
lze	lze	k6eAd1	lze
zmínit	zmínit	k5eAaPmF	zmínit
dvě	dva	k4xCgNnPc1	dva
souostroví	souostroví	k1gNnPc1	souostroví
-	-	kIx~	-
Zelenecké	Zelenecký	k2eAgInPc1d1	Zelenecký
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
Karedžské	Karedžský	k2eAgInPc1d1	Karedžský
ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
Petrokreposť	Petrokreposť	k1gFnSc2	Petrokreposť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
chladné	chladný	k2eAgNnSc1d1	chladné
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
od	od	k7c2	od
-8	-8	k4	-8
do	do	k7c2	do
-10	-10	k4	-10
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
od	od	k7c2	od
16	[number]	k4	16
do	do	k7c2	do
17	[number]	k4	17
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrné	průměrný	k2eAgNnSc1d1	průměrné
roční	roční	k2eAgNnSc1d1	roční
množství	množství	k1gNnSc1	množství
atmosférických	atmosférický	k2eAgFnPc2d1	atmosférická
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
550	[number]	k4	550
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
povodí	povodí	k1gNnSc6	povodí
Ladožského	ladožský	k2eAgNnSc2d1	Ladožské
jezera	jezero	k1gNnSc2	jezero
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
okolo	okolo	k7c2	okolo
50	[number]	k4	50
000	[number]	k4	000
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
3	[number]	k4	3
500	[number]	k4	500
řek	řeka	k1gFnPc2	řeka
delších	dlouhý	k2eAgFnPc2d2	delší
než	než	k8xS	než
10	[number]	k4	10
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
přítoky	přítok	k1gInPc1	přítok
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Volchov	Volchov	k1gInSc1	Volchov
(	(	kIx(	(
<g/>
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
Ilmeň	Ilmeň	k1gFnSc1	Ilmeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
a	a	k8xC	a
východu	východ	k1gInSc2	východ
Svir	Svira	k1gFnPc2	Svira
(	(	kIx(	(
<g/>
z	z	k7c2	z
Oněžského	oněžský	k2eAgNnSc2d1	Oněžské
jezera	jezero	k1gNnSc2	jezero
<g/>
)	)	kIx)	)
a	a	k8xC	a
ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
Vuoksa	Vuoksa	k1gFnSc1	Vuoksa
(	(	kIx(	(
<g/>
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
Saimaa	Saima	k1gInSc2	Saima
<g/>
)	)	kIx)	)
a	a	k8xC	a
Taipale	Taipala	k1gFnSc3	Taipala
(	(	kIx(	(
<g/>
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
Suvanto	Suvanta	k1gFnSc5	Suvanta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
odvádí	odvádět	k5eAaImIp3nS	odvádět
do	do	k7c2	do
Finského	finský	k2eAgInSc2d1	finský
zálivu	záliv	k1gInSc2	záliv
řeka	řeka	k1gFnSc1	řeka
Něva	Něva	k1gFnSc1	Něva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
85	[number]	k4	85
<g/>
%	%	kIx~	%
přírůstku	přírůstek	k1gInSc2	přírůstek
vodní	vodní	k2eAgFnSc2d1	vodní
bilance	bilance	k1gFnSc2	bilance
jezera	jezero	k1gNnSc2	jezero
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
67,8	[number]	k4	67,8
km3	km3	k4	km3
vody	voda	k1gFnSc2	voda
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
přítok	přítok	k1gInSc1	přítok
řek	řeka	k1gFnPc2	řeka
(	(	kIx(	(
<g/>
především	především	k9	především
Volchov	Volchov	k1gInSc1	Volchov
<g/>
,	,	kIx,	,
Svir	Svir	k1gInSc1	Svir
a	a	k8xC	a
Vuoksa	Vuoksa	k1gFnSc1	Vuoksa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
atmosférické	atmosférický	k2eAgFnPc1d1	atmosférická
srážky	srážka	k1gFnPc1	srážka
a	a	k8xC	a
2	[number]	k4	2
<g/>
%	%	kIx~	%
přítok	přítok	k1gInSc1	přítok
podzemních	podzemní	k2eAgFnPc2d1	podzemní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
92	[number]	k4	92
<g/>
%	%	kIx~	%
úbytku	úbytek	k1gInSc2	úbytek
vodní	vodní	k2eAgFnSc2d1	vodní
bilance	bilance	k1gFnSc2	bilance
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
řeku	řeka	k1gFnSc4	řeka
Něvu	Něva	k1gFnSc4	Něva
(	(	kIx(	(
<g/>
průměrně	průměrně	k6eAd1	průměrně
78,1	[number]	k4	78,1
km3	km3	k4	km3
vody	voda	k1gFnSc2	voda
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
a	a	k8xC	a
8	[number]	k4	8
<g/>
%	%	kIx~	%
na	na	k7c4	na
vypařování	vypařování	k1gNnSc4	vypařování
z	z	k7c2	z
vodní	vodní	k2eAgFnSc2d1	vodní
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Kolísání	kolísání	k1gNnSc1	kolísání
úrovně	úroveň	k1gFnSc2	úroveň
hladiny	hladina	k1gFnSc2	hladina
má	mít	k5eAaImIp3nS	mít
plynulý	plynulý	k2eAgInSc4d1	plynulý
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
m.	m.	k?	m.
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
úrovně	úroveň	k1gFnSc2	úroveň
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
a	a	k8xC	a
lednu	leden	k1gInSc6	leden
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
rozsah	rozsah	k1gInSc1	rozsah
kolísání	kolísání	k1gNnSc2	kolísání
hladiny	hladina	k1gFnSc2	hladina
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
0,8	[number]	k4	0,8
m	m	kA	m
a	a	k8xC	a
maximální	maximální	k2eAgFnPc1d1	maximální
asi	asi	k9	asi
3	[number]	k4	3
m.	m.	k?	m.
Přílivové	přílivový	k2eAgNnSc4d1	přílivové
a	a	k8xC	a
odlivové	odlivový	k2eAgNnSc4d1	odlivový
kolísání	kolísání	k1gNnSc4	kolísání
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
okolo	okolo	k7c2	okolo
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
cm	cm	kA	cm
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
(	(	kIx(	(
<g/>
zřídka	zřídka	k6eAd1	zřídka
až	až	k9	až
90	[number]	k4	90
<g/>
)	)	kIx)	)
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
vlny	vlna	k1gFnPc1	vlna
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
periodou	perioda	k1gFnSc7	perioda
(	(	kIx(	(
<g/>
с	с	k?	с
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
výška	výška	k1gFnSc1	výška
vln	vlna	k1gFnPc2	vlna
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
jezera	jezero	k1gNnSc2	jezero
3	[number]	k4	3
až	až	k9	až
3,5	[number]	k4	3,5
m	m	kA	m
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
5	[number]	k4	5
až	až	k9	až
6	[number]	k4	6
m	m	kA	m
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
do	do	k7c2	do
2,5	[number]	k4	2,5
m.	m.	k?	m.
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgFnPc1d1	častá
bouře	bouř	k1gFnPc1	bouř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
vody	voda	k1gFnSc2	voda
==	==	k?	==
</s>
</p>
<p>
<s>
Teplotní	teplotní	k2eAgInSc1d1	teplotní
režim	režim	k1gInSc1	režim
je	být	k5eAaImIp3nS	být
různý	různý	k2eAgInSc1d1	různý
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
a	a	k8xC	a
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
u	u	k7c2	u
hladiny	hladina	k1gFnSc2	hladina
okolo	okolo	k7c2	okolo
16	[number]	k4	16
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgMnSc1d1	maximální
25	[number]	k4	25
°	°	k?	°
<g/>
C.	C.	kA	C.
Teplota	teplota	k1gFnSc1	teplota
vrstev	vrstva	k1gFnPc2	vrstva
vody	voda	k1gFnSc2	voda
u	u	k7c2	u
dna	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
až	až	k9	až
2,5	[number]	k4	2,5
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
oblasti	oblast	k1gFnPc1	oblast
a	a	k8xC	a
zálivy	záliv	k1gInPc1	záliv
zamrzají	zamrzat	k5eAaImIp3nP	zamrzat
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
centrální	centrální	k2eAgFnSc1d1	centrální
část	část	k1gFnSc1	část
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
až	až	k8xS	až
únoru	únor	k1gInSc6	únor
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
tloušťka	tloušťka	k1gFnSc1	tloušťka
ledu	led	k1gInSc2	led
je	být	k5eAaImIp3nS	být
50	[number]	k4	50
až	až	k9	až
60	[number]	k4	60
cm	cm	kA	cm
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc4d1	maximální
90	[number]	k4	90
až	až	k8xS	až
100	[number]	k4	100
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
část	část	k1gFnSc1	část
jezera	jezero	k1gNnSc2	jezero
rozmrzá	rozmrzat	k5eAaImIp3nS	rozmrzat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
nebo	nebo	k8xC	nebo
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
žlutohnědá	žlutohnědý	k2eAgFnSc1d1	žlutohnědá
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
průzračnost	průzračnost	k1gFnSc1	průzračnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
4,5	[number]	k4	4,5
m	m	kA	m
<g/>
,	,	kIx,	,
u	u	k7c2	u
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
2	[number]	k4	2
až	až	k9	až
2,5	[number]	k4	2,5
m	m	kA	m
<g/>
,	,	kIx,	,
u	u	k7c2	u
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
a	a	k8xC	a
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
ústí	ústit	k5eAaImIp3nS	ústit
řek	řeka	k1gFnPc2	řeka
0,3	[number]	k4	0,3
až	až	k9	až
0,9	[number]	k4	0,9
m.	m.	k?	m.
Největší	veliký	k2eAgFnSc1d3	veliký
průzračnost	průzračnost	k1gFnSc1	průzračnost
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
m	m	kA	m
je	být	k5eAaImIp3nS	být
západně	západně	k6eAd1	západně
od	od	k7c2	od
ostrova	ostrov	k1gInSc2	ostrov
Valaam	Valaam	k1gInSc1	Valaam
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
v	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
je	být	k5eAaImIp3nS	být
sladká	sladký	k2eAgFnSc1d1	sladká
(	(	kIx(	(
<g/>
п	п	k?	п
<g/>
)	)	kIx)	)
hydrouhličitanovo-vápenatá	hydrouhličitanovoápenatý	k2eAgFnSc1d1	hydrouhličitanovo-vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
mineralizace	mineralizace	k1gFnSc1	mineralizace
je	být	k5eAaImIp3nS	být
56	[number]	k4	56
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Obsah	obsah	k1gInSc4	obsah
rozpuštěného	rozpuštěný	k2eAgInSc2d1	rozpuštěný
kyslíku	kyslík	k1gInSc2	kyslík
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
14	[number]	k4	14
až	až	k9	až
15	[number]	k4	15
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
u	u	k7c2	u
hladiny	hladina	k1gFnSc2	hladina
10	[number]	k4	10
až	až	k9	až
11	[number]	k4	11
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
a	a	k8xC	a
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
12	[number]	k4	12
až	až	k8xS	až
13	[number]	k4	13
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
==	==	k?	==
</s>
</p>
<p>
<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
význam	význam	k1gInSc1	význam
mají	mít	k5eAaImIp3nP	mít
lososi	losos	k1gMnPc1	losos
<g/>
,	,	kIx,	,
pstruzi	pstruh	k1gMnPc1	pstruh
<g/>
,	,	kIx,	,
marény	maréna	k1gFnPc1	maréna
<g/>
,	,	kIx,	,
sízi	síz	k1gMnPc5	síz
<g/>
,	,	kIx,	,
candáti	candát	k1gMnPc5	candát
<g/>
,	,	kIx,	,
cejni	cejn	k1gMnPc5	cejn
<g/>
,	,	kIx,	,
okouni	okoun	k1gMnPc5	okoun
<g/>
,	,	kIx,	,
plotice	plotice	k1gFnPc4	plotice
<g/>
,	,	kIx,	,
štiky	štika	k1gFnPc4	štika
a	a	k8xC	a
korušky	koruška	k1gFnPc4	koruška
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
také	také	k9	také
jeseteři	jeseter	k1gMnPc1	jeseter
<g/>
,	,	kIx,	,
úhoři	úhoř	k1gMnPc1	úhoř
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
tuleni	tuleň	k1gMnPc1	tuleň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Jezero	jezero	k1gNnSc1	jezero
je	být	k5eAaImIp3nS	být
splavné	splavný	k2eAgNnSc1d1	splavné
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Bělomořsko-baltské	bělomořskoaltský	k2eAgFnSc2d1	bělomořsko-baltský
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
a	a	k8xC	a
Volžsko-baltské	volžskoaltský	k2eAgFnSc2d1	volžsko-baltský
vodní	vodní	k2eAgFnSc2d1	vodní
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Svir	Svira	k1gFnPc2	Svira
k	k	k7c3	k
Něvě	Něva	k1gFnSc3	Něva
podél	podél	k7c2	podél
jižního	jižní	k2eAgInSc2d1	jižní
břehu	břeh	k1gInSc2	břeh
prochází	procházet	k5eAaImIp3nS	procházet
Novoladožský	Novoladožský	k2eAgInSc4d1	Novoladožský
kanál	kanál	k1gInSc4	kanál
a	a	k8xC	a
také	také	k9	také
starší	starý	k2eAgInSc4d2	starší
Staroladožský	Staroladožský	k2eAgInSc4d1	Staroladožský
kanál	kanál	k1gInSc4	kanál
budovaný	budovaný	k2eAgInSc4d1	budovaný
již	již	k6eAd1	již
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
spojený	spojený	k2eAgMnSc1d1	spojený
z	z	k7c2	z
Ladožským	ladožský	k2eAgNnSc7d1	Ladožské
jezerem	jezero	k1gNnSc7	jezero
systémy	systém	k1gInPc1	systém
plavebních	plavební	k2eAgFnPc2d1	plavební
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hladina	hladina	k1gFnSc1	hladina
tohoto	tento	k3xDgInSc2	tento
kanálu	kanál	k1gInSc2	kanál
je	být	k5eAaImIp3nS	být
výše	výše	k1gFnSc1	výše
nežli	nežli	k8xS	nežli
stálá	stálý	k2eAgFnSc1d1	stálá
hladina	hladina	k1gFnSc1	hladina
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
oba	dva	k4xCgInPc1	dva
kanály	kanál	k1gInPc1	kanál
protínají	protínat	k5eAaImIp3nP	protínat
řeky	řeka	k1gFnPc1	řeka
Volchov	Volchovo	k1gNnPc2	Volchovo
a	a	k8xC	a
Sjas	Sjasa	k1gFnPc2	Sjasa
<g/>
́	́	k?	́
napříč	napříč	k7c7	napříč
jejich	jejich	k3xOp3gInPc2	jejich
toků	tok	k1gInPc2	tok
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
Novoladožského	Novoladožský	k2eAgInSc2d1	Novoladožský
kanálu	kanál	k1gInSc2	kanál
propojení	propojení	k1gNnSc2	propojení
otevřeným	otevřený	k2eAgInSc7d1	otevřený
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
Staroladožského	Staroladožský	k2eAgInSc2d1	Staroladožský
kanálu	kanál	k1gInSc2	kanál
jsou	být	k5eAaImIp3nP	být
ústí	ústit	k5eAaImIp3nP	ústit
výše	vysoce	k6eAd2	vysoce
proti	proti	k7c3	proti
proudům	proud	k1gInPc3	proud
těchto	tento	k3xDgFnPc2	tento
řek	řeka	k1gFnPc2	řeka
kvůli	kvůli	k7c3	kvůli
naplňování	naplňování	k1gNnSc3	naplňování
příslušných	příslušný	k2eAgInPc2d1	příslušný
úseků	úsek	k1gInPc2	úsek
kanálu	kanál	k1gInSc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Staroladožský	Staroladožský	k2eAgInSc1d1	Staroladožský
kanál	kanál	k1gInSc1	kanál
slouží	sloužit	k5eAaImIp3nS	sloužit
už	už	k6eAd1	už
jen	jen	k9	jen
lokálním	lokální	k2eAgFnPc3d1	lokální
potřebám	potřeba	k1gFnPc3	potřeba
rybářů	rybář	k1gMnPc2	rybář
a	a	k8xC	a
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
jezera	jezero	k1gNnSc2	jezero
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
baltofinského	baltofinský	k2eAgNnSc2d1	baltofinský
jména	jméno	k1gNnSc2	jméno
Alodejoki	Alodejoki	k1gNnSc2	Alodejoki
či	či	k8xC	či
Aladejoka	Aladejoko	k1gNnSc2	Aladejoko
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dole	dole	k6eAd1	dole
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
označovalo	označovat	k5eAaImAgNnS	označovat
staré	starý	k2eAgNnSc1d1	staré
sídliště	sídliště	k1gNnSc1	sídliště
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
Volchovu	Volchův	k2eAgFnSc4d1	Volchův
<g/>
;	;	kIx,	;
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
zkrátilo	zkrátit	k5eAaPmAgNnS	zkrátit
na	na	k7c6	na
Ladoga	Ladoga	k1gFnSc1	Ladoga
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Staraja	Staraj	k2eAgFnSc1d1	Staraja
Ladoga	Ladoga	k1gFnSc1	Ladoga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
bylo	být	k5eAaImAgNnS	být
jezero	jezero	k1gNnSc1	jezero
dříve	dříve	k6eAd2	dříve
nazýváno	nazývat	k5eAaImNgNnS	nazývat
Něvo	Něva	k1gFnSc5	Něva
(	(	kIx(	(
<g/>
Н	Н	k?	Н
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
řeky	řeka	k1gFnSc2	řeka
Něvy	Něva	k1gFnSc2	Něva
(	(	kIx(	(
<g/>
obdobně	obdobně	k6eAd1	obdobně
existuje	existovat	k5eAaImIp3nS	existovat
synonymní	synonymní	k2eAgNnSc1d1	synonymní
finské	finský	k2eAgNnSc1d1	finské
pojmenování	pojmenování	k1gNnSc1	pojmenování
jezera	jezero	k1gNnSc2	jezero
Nevajärvi	Nevajärev	k1gFnSc6	Nevajärev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
doloženo	doložen	k2eAgNnSc4d1	doloženo
používání	používání	k1gNnSc4	používání
dnešního	dnešní	k2eAgNnSc2d1	dnešní
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jezeře	jezero	k1gNnSc6	jezero
procházela	procházet	k5eAaImAgFnS	procházet
slavná	slavný	k2eAgFnSc1d1	slavná
vikinská	vikinský	k2eAgFnSc1d1	vikinská
Cesta	cesta	k1gFnSc1	cesta
od	od	k7c2	od
Varjagů	Varjag	k1gMnPc2	Varjag
k	k	k7c3	k
Řekům	Řek	k1gMnPc3	Řek
(	(	kIx(	(
<g/>
Puť	puťa	k1gFnPc2	puťa
iz	iz	k?	iz
Varjag	Varjag	k1gMnSc1	Varjag
v	v	k7c6	v
Greki	Grek	k1gInSc6	Grek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
březích	březí	k2eAgFnPc6d1	březí
byly	být	k5eAaImAgInP	být
mj.	mj.	kA	mj.
založeny	založit	k5eAaPmNgInP	založit
i	i	k9	i
důležité	důležitý	k2eAgFnPc1d1	důležitá
pevnosti	pevnost	k1gFnPc1	pevnost
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
města	město	k1gNnSc2	město
<g/>
:	:	kIx,	:
Korela	korela	k1gFnSc1	korela
(	(	kIx(	(
<g/>
Kexholm	Kexholm	k1gInSc1	Kexholm
<g/>
,	,	kIx,	,
Käkisalmi	Käkisal	k1gFnPc7	Käkisal
<g/>
)	)	kIx)	)
a	a	k8xC	a
Orešek	Orešek	k1gInSc1	Orešek
<g/>
/	/	kIx~	/
<g/>
Orechov	Orechov	k1gInSc1	Orechov
(	(	kIx(	(
<g/>
Nöteborg	Nöteborg	k1gInSc1	Nöteborg
<g/>
,	,	kIx,	,
Pähkinälinna	Pähkinälinna	k1gFnSc1	Pähkinälinna
<g/>
,	,	kIx,	,
Schlüsselburg	Schlüsselburg	k1gInSc1	Schlüsselburg
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Koněvic	Koněvice	k1gInPc2	Koněvice
a	a	k8xC	a
Valaam	Valaam	k1gInSc4	Valaam
významné	významný	k2eAgInPc4d1	významný
kláštery	klášter	k1gInPc4	klášter
Koněvský	Koněvský	k2eAgInSc4d1	Koněvský
a	a	k8xC	a
Valaamský	Valaamský	k2eAgInSc4d1	Valaamský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Livonské	Livonský	k2eAgFnSc2d1	Livonská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1558	[number]	k4	1558
<g/>
–	–	k?	–
<g/>
83	[number]	k4	83
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
boje	boj	k1gInPc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Švédové	Švéd	k1gMnPc1	Švéd
západní	západní	k2eAgFnSc2d1	západní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
Stolbovského	Stolbovský	k2eAgInSc2d1	Stolbovský
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
1617	[number]	k4	1617
<g/>
)	)	kIx)	)
přešel	přejít	k5eAaPmAgInS	přejít
severní	severní	k2eAgInSc4d1	severní
a	a	k8xC	a
západní	západní	k2eAgInSc4d1	západní
břeh	břeh	k1gInSc4	břeh
(	(	kIx(	(
<g/>
jakožto	jakožto	k8xS	jakožto
provincie	provincie	k1gFnSc1	provincie
Kexholm	Kexholma	k1gFnPc2	Kexholma
<g/>
)	)	kIx)	)
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
ke	k	k7c3	k
Švédsku	Švédsko	k1gNnSc3	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Severní	severní	k2eAgFnSc2d1	severní
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1700	[number]	k4	1700
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
rusko-švédské	rusko-švédský	k2eAgFnPc1d1	rusko-švédská
vojenské	vojenský	k2eAgFnPc1d1	vojenská
operace	operace	k1gFnPc1	operace
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
říše	říše	k1gFnSc1	říše
nakonec	nakonec	k6eAd1	nakonec
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
přilehlou	přilehlý	k2eAgFnSc7d1	přilehlá
Ingrií	Ingrie	k1gFnSc7	Ingrie
a	a	k8xC	a
částí	část	k1gFnSc7	část
Karélie	Karélie	k1gFnSc2	Karélie
<g/>
)	)	kIx)	)
dobyla	dobýt	k5eAaPmAgFnS	dobýt
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
Nystadskou	Nystadský	k2eAgFnSc7d1	Nystadský
mírovou	mírový	k2eAgFnSc7d1	mírová
smlouvou	smlouva	k1gFnSc7	smlouva
(	(	kIx(	(
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
pak	pak	k6eAd1	pak
kraj	kraj	k1gInSc1	kraj
okolo	okolo	k7c2	okolo
jezera	jezero	k1gNnSc2	jezero
drželo	držet	k5eAaImAgNnS	držet
následujících	následující	k2eAgNnPc2d1	následující
90	[number]	k4	90
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
Rusko	Rusko	k1gNnSc1	Rusko
dobylo	dobýt	k5eAaPmAgNnS	dobýt
na	na	k7c6	na
Švédsku	Švédsko	k1gNnSc6	Švédsko
celé	celý	k2eAgNnSc4d1	celé
Finsko	Finsko	k1gNnSc4	Finsko
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
car	car	k1gMnSc1	car
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
administrativně	administrativně	k6eAd1	administrativně
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
nové	nový	k2eAgFnSc3d1	nová
autonomní	autonomní	k2eAgFnSc3d1	autonomní
provincii	provincie	k1gFnSc3	provincie
i	i	k8xC	i
oblasti	oblast	k1gFnPc1	oblast
dobyté	dobytý	k2eAgFnPc1d1	dobytá
v	v	k7c6	v
letech	let	k1gInPc6	let
1721	[number]	k4	1721
<g/>
–	–	k?	–
<g/>
1743	[number]	k4	1743
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Staré	Staré	k2eAgNnSc1d1	Staré
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Karelské	karelský	k2eAgFnSc2d1	Karelská
šíje	šíj	k1gFnSc2	šíj
a	a	k8xC	a
severní	severní	k2eAgInPc1d1	severní
břehy	břeh	k1gInPc1	břeh
jezera	jezero	k1gNnSc2	jezero
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
opět	opět	k6eAd1	opět
dostaly	dostat	k5eAaPmAgInP	dostat
pod	pod	k7c4	pod
(	(	kIx(	(
<g/>
omezenou	omezený	k2eAgFnSc4d1	omezená
<g/>
)	)	kIx)	)
finskou	finský	k2eAgFnSc4d1	finská
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
<g/>
;	;	kIx,	;
provinciální	provinciální	k2eAgInSc1d1	provinciální
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
státní	státní	k2eAgFnPc4d1	státní
hranice	hranice	k1gFnPc4	hranice
mezi	mezi	k7c7	mezi
Finským	finský	k2eAgNnSc7d1	finské
velkoknížectvím	velkoknížectví	k1gNnSc7	velkoknížectví
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Finskou	finský	k2eAgFnSc7d1	finská
republikou	republika	k1gFnSc7	republika
<g/>
)	)	kIx)	)
a	a	k8xC	a
samotným	samotný	k2eAgNnSc7d1	samotné
Ruskem	Rusko	k1gNnSc7	Rusko
(	(	kIx(	(
<g/>
poději	podět	k5eAaPmIp1nS	podět
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
)	)	kIx)	)
vedla	vést	k5eAaImAgFnS	vést
šikmo	šikmo	k6eAd1	šikmo
napříč	napříč	k6eAd1	napříč
přes	přes	k7c4	přes
jezero	jezero	k1gNnSc4	jezero
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
polovině	polovina	k1gFnSc6	polovina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novověk	novověk	k1gInSc4	novověk
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
se	se	k3xPyFc4	se
nejbližší	blízký	k2eAgNnSc1d3	nejbližší
okolí	okolí	k1gNnSc1	okolí
jezera	jezero	k1gNnSc2	jezero
i	i	k9	i
jezero	jezero	k1gNnSc1	jezero
samé	samý	k3xTgFnSc2	samý
stalo	stát	k5eAaPmAgNnS	stát
významným	významný	k2eAgNnSc7d1	významné
bojištěm	bojiště	k1gNnSc7	bojiště
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
střetávaly	střetávat	k5eAaImAgFnP	střetávat
německé	německý	k2eAgFnPc1d1	německá
<g/>
,	,	kIx,	,
finské	finský	k2eAgFnPc1d1	finská
a	a	k8xC	a
ruské	ruský	k2eAgFnPc1d1	ruská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
zimní	zimní	k2eAgFnSc2d1	zimní
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
a	a	k8xC	a
Velké	velký	k2eAgFnSc2d1	velká
vlastenecké	vlastenecký	k2eAgFnSc2d1	vlastenecká
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
operovalo	operovat	k5eAaImAgNnS	operovat
finské	finský	k2eAgNnSc1d1	finské
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnPc1d1	německá
<g/>
,	,	kIx,	,
italské	italský	k2eAgFnPc1d1	italská
(	(	kIx(	(
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
sovětské	sovětský	k2eAgNnSc1d1	sovětské
loďstvo	loďstvo	k1gNnSc1	loďstvo
(	(	kIx(	(
<g/>
Ladožská	ladožský	k2eAgFnSc1d1	Ladožská
vojenská	vojenský	k2eAgFnSc1d1	vojenská
flotila	flotila	k1gFnSc1	flotila
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hrála	hrát	k5eAaImAgFnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
při	při	k7c6	při
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
úspěchu	úspěch	k1gInSc2	úspěch
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
na	na	k7c6	na
Leningradské	leningradský	k2eAgFnSc6d1	Leningradská
frontě	fronta	k1gFnSc6	fronta
a	a	k8xC	a
při	při	k7c6	při
podpoře	podpora	k1gFnSc6	podpora
Baltské	baltský	k2eAgFnSc2d1	Baltská
flotily	flotila	k1gFnSc2	flotila
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
blokády	blokáda	k1gFnSc2	blokáda
Leningradu	Leningrad	k1gInSc2	Leningrad
od	od	k7c2	od
září	září	k1gNnSc2	září
1941	[number]	k4	1941
do	do	k7c2	do
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
fungovala	fungovat	k5eAaImAgFnS	fungovat
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
jezera	jezero	k1gNnSc2	jezero
tzv.	tzv.	kA	tzv.
Cesta	cesta	k1gFnSc1	cesta
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
spojovala	spojovat	k5eAaImAgFnS	spojovat
Leningrad	Leningrad	k1gInSc4	Leningrad
<g/>
,	,	kIx,	,
obklíčený	obklíčený	k2eAgInSc4d1	obklíčený
německými	německý	k2eAgInPc7d1	německý
vojsky	vojsko	k1gNnPc7	vojsko
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zásoby	zásoba	k1gFnPc1	zásoba
byly	být	k5eAaImAgFnP	být
dopravovány	dopravovat	k5eAaImNgFnP	dopravovat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nákladními	nákladní	k2eAgInPc7d1	nákladní
auty	aut	k1gInPc7	aut
přes	přes	k7c4	přes
led	led	k1gInSc4	led
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
na	na	k7c6	na
člunech	člun	k1gInPc6	člun
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
zase	zase	k9	zase
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
blokády	blokáda	k1gFnSc2	blokáda
odváženi	odvážit	k5eAaPmNgMnP	odvážit
ranění	raněný	k1gMnPc1	raněný
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zimní	zimní	k2eAgFnSc6d1	zimní
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
jezero	jezero	k1gNnSc1	jezero
stalo	stát	k5eAaPmAgNnS	stát
plně	plně	k6eAd1	plně
vnitroruským	vnitroruský	k2eAgNnSc7d1	vnitroruský
<g/>
,	,	kIx,	,
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
Finskem	Finsko	k1gNnSc7	Finsko
byly	být	k5eAaImAgFnP	být
posunuty	posunut	k2eAgFnPc1d1	posunuta
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
35	[number]	k4	35
km	km	kA	km
od	od	k7c2	od
nejsevernějšího	severní	k2eAgInSc2d3	nejsevernější
bodu	bod	k1gInSc2	bod
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
sice	sice	k8xC	sice
v	v	k7c6	v
pokračovací	pokračovací	k2eAgFnSc6d1	pokračovací
válce	válka	k1gFnSc6	válka
Finové	Fin	k1gMnPc1	Fin
západní	západní	k2eAgFnSc2d1	západní
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
východní	východní	k2eAgInPc4d1	východní
břehy	břeh	k1gInPc4	břeh
jezera	jezero	k1gNnSc2	jezero
zpětně	zpětně	k6eAd1	zpětně
dobyli	dobýt	k5eAaPmAgMnP	dobýt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sovětská	sovětský	k2eAgFnSc1d1	sovětská
ofenziva	ofenziva	k1gFnSc1	ofenziva
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
vrátila	vrátit	k5eAaPmAgFnS	vrátit
územní	územní	k2eAgFnSc4d1	územní
situaci	situace	k1gFnSc4	situace
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
stvrdil	stvrdit	k5eAaPmAgMnS	stvrdit
Pařížský	pařížský	k2eAgInSc4d1	pařížský
mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jezero	jezero	k1gNnSc1	jezero
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc1	okolí
náleželo	náležet	k5eAaImAgNnS	náležet
Ruské	ruský	k2eAgFnPc4d1	ruská
SFSR	SFSR	kA	SFSR
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
rozpadu	rozpad	k1gInSc6	rozpad
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
Rusku	Ruska	k1gFnSc4	Ruska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osídlení	osídlení	k1gNnSc3	osídlení
pobřeží	pobřeží	k1gNnSc3	pobřeží
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Republika	republika	k1gFnSc1	republika
Karélie	Karélie	k1gFnSc1	Karélie
===	===	k?	===
</s>
</p>
<p>
<s>
Karelské	karelský	k2eAgFnSc3d1	Karelská
republice	republika	k1gFnSc3	republika
náleží	náležet	k5eAaImIp3nS	náležet
severozápadní	severozápadní	k2eAgInSc4d1	severozápadní
a	a	k8xC	a
severovýchodní	severovýchodní	k2eAgInSc4d1	severovýchodní
břeh	břeh	k1gInSc4	břeh
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
jezera	jezero	k1gNnSc2	jezero
zde	zde	k6eAd1	zde
leží	ležet	k5eAaImIp3nS	ležet
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
východu	východ	k1gInSc3	východ
města	město	k1gNnSc2	město
a	a	k8xC	a
vesnice	vesnice	k1gFnSc2	vesnice
Sortavala	Sortavala	k1gFnSc2	Sortavala
<g/>
,	,	kIx,	,
Pitkjaranta	Pitkjarant	k1gMnSc2	Pitkjarant
<g/>
,	,	kIx,	,
Salmi	salmi	k1gNnSc1	salmi
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
km	km	kA	km
od	od	k7c2	od
břehu	břeh	k1gInSc2	břeh
Olonec	Olonec	k1gInSc1	Olonec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Leningradská	leningradský	k2eAgFnSc1d1	Leningradská
oblast	oblast	k1gFnSc1	oblast
===	===	k?	===
</s>
</p>
<p>
<s>
Leningradské	leningradský	k2eAgFnPc1d1	Leningradská
oblasti	oblast	k1gFnPc1	oblast
náleží	náležet	k5eAaImIp3nP	náležet
západní	západní	k2eAgInSc4d1	západní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc4d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgInSc4d1	jihovýchodní
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
leží	ležet	k5eAaImIp3nP	ležet
město	město	k1gNnSc4	město
Priozersk	Priozersk	k1gInSc1	Priozersk
a	a	k8xC	a
vesnice	vesnice	k1gFnSc1	vesnice
More	mor	k1gInSc5	mor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
odtéká	odtékat	k5eAaImIp3nS	odtékat
řeka	řeka	k1gFnSc1	řeka
Něva	Něva	k1gFnSc1	Něva
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
město	město	k1gNnSc1	město
Šlisselburg	Šlisselburg	k1gInSc1	Šlisselburg
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1944-92	[number]	k4	1944-92
Petrokreposť	Petrokreposť	k1gFnSc4	Petrokreposť
<g/>
)	)	kIx)	)
s	s	k7c7	s
ostrovní	ostrovní	k2eAgFnSc7d1	ostrovní
pevností	pevnost	k1gFnSc7	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
a	a	k8xC	a
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
břehu	břeh	k1gInSc6	břeh
leží	ležet	k5eAaImIp3nS	ležet
od	od	k7c2	od
západu	západ	k1gInSc2	západ
Kobona	Kobona	k1gFnSc1	Kobona
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Ladoga	Ladoga	k1gFnSc1	Ladoga
<g/>
,	,	kIx,	,
Sjasstroj	Sjasstroj	k1gInSc1	Sjasstroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Hydrologický	hydrologický	k2eAgInSc1d1	hydrologický
režim	režim	k1gInSc1	režim
a	a	k8xC	a
vodní	vodní	k2eAgFnSc1d1	vodní
bilance	bilance	k1gFnSc1	bilance
Ladožského	ladožský	k2eAgNnSc2d1	Ladožské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1966	[number]	k4	1966
(	(	kIx(	(
<g/>
Publikace	publikace	k1gFnSc1	publikace
laboratoří	laboratoř	k1gFnPc2	laboratoř
jezerního	jezerní	k2eAgInSc2d1	jezerní
výzkumu	výzkum	k1gInSc2	výzkum
LGU	LGU	kA	LGU
<g/>
,	,	kIx,	,
č.	č.	k?	č.
20	[number]	k4	20
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Г	Г	k?	Г
р	р	k?	р
и	и	k?	и
в	в	k?	в
б	б	k?	б
Л	Л	k?	Л
о	о	k?	о
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
(	(	kIx(	(
<g/>
Т	Т	k?	Т
Л	Л	k?	Л
о	о	k?	о
Л	Л	k?	Л
<g/>
,	,	kIx,	,
т	т	k?	т
20	[number]	k4	20
<g/>
))	))	k?	))
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Semenovič	Semenovič	k1gInSc1	Semenovič
N.	N.	kA	N.
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Usazeniny	usazenina	k1gFnPc1	usazenina
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
Ladožského	ladožský	k2eAgNnSc2d1	Ladožské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1966	[number]	k4	1966
(	(	kIx(	(
<g/>
С	С	k?	С
Н	Н	k?	Н
И	И	k?	И
<g/>
,	,	kIx,	,
Д	Д	k?	Д
о	о	k?	о
Л	Л	k?	Л
о	о	k?	о
<g/>
,	,	kIx,	,
М	М	k?	М
<g/>
-	-	kIx~	-
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Biologické	biologický	k2eAgInPc1d1	biologický
zdroje	zdroj	k1gInPc1	zdroj
Ladožského	ladožský	k2eAgNnSc2d1	Ladožské
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
Б	Б	k?	Б
р	р	k?	р
Л	Л	k?	Л
о	о	k?	о
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Kalesnik	Kalesnik	k1gInSc1	Kalesnik
S.	S.	kA	S.
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Ladožské	ladožský	k2eAgNnSc1d1	Ladožské
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
К	К	k?	К
С	С	k?	С
В	В	k?	В
<g/>
,	,	kIx,	,
Л	Л	k?	Л
о	о	k?	о
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Domanickij	Domanickij	k1gMnSc1	Domanickij
A.	A.	kA	A.
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Dubrovina	Dubrovina	k1gFnSc1	Dubrovina
R.	R.	kA	R.
G.	G.	kA	G.
<g/>
,	,	kIx,	,
Isajeva	Isajevo	k1gNnSc2	Isajevo
A.	A.	kA	A.
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
jezera	jezero	k1gNnSc2	jezero
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Д	Д	k?	Д
А	А	k?	А
П	П	k?	П
<g/>
,	,	kIx,	,
Д	Д	k?	Д
Р	Р	k?	Р
Г	Г	k?	Г
<g/>
,	,	kIx,	,
И	И	k?	И
А	А	k?	А
И	И	k?	И
<g/>
,	,	kIx,	,
Р	Р	k?	Р
и	и	k?	и
о	о	k?	о
С	С	k?	С
С	С	k?	С
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Л	Л	k?	Л
о	о	k?	о
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ladožské	ladožský	k2eAgFnSc2d1	Ladožská
jezero	jezero	k1gNnSc4	jezero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Proceeding	Proceeding	k1gInSc1	Proceeding
of	of	k?	of
The	The	k1gFnSc1	The
First	First	k1gFnSc1	First
International	International	k1gFnSc1	International
Lake	Lake	k1gFnSc1	Lake
Ladoga	Ladoga	k1gFnSc1	Ladoga
Symposium	symposium	k1gNnSc1	symposium
<g/>
,	,	kIx,	,
Simola	Simola	k1gFnSc1	Simola
<g/>
,	,	kIx,	,
Heikki	Heikki	k1gNnSc1	Heikki
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
</s>
</p>
<p>
<s>
Fotky	fotka	k1gFnPc1	fotka
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
na	na	k7c6	na
Ladožském	ladožský	k2eAgNnSc6d1	Ladožské
jezeře	jezero	k1gNnSc6	jezero
1941-1944	[number]	k4	1941-1944
</s>
</p>
<p>
<s>
Mapy	mapa	k1gFnPc1	mapa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
</s>
</p>
