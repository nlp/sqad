<s>
Kat	kat	k1gMnSc1
a	a	k8xC
blázen	blázen	k1gMnSc1
</s>
<s>
Kat	kat	k1gMnSc1
a	a	k8xC
blázen	blázen	k1gMnSc1
Autor	autor	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Voskovec	Voskovec	k1gMnSc1
a	a	k8xC
Jan	Jan	k1gMnSc1
Werich	Werich	k1gMnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1934	#num#	k4
Režie	režie	k1gFnSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Honzl	Honzl	k1gFnSc2
Hudba	hudba	k1gFnSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
Výprava	výprava	k1gMnSc1
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Feuerstein	Feuerstein	k1gMnSc1
a	a	k8xC
Alois	Alois	k1gMnSc1
Wachsman	Wachsman	k1gMnSc1
Choreografie	choreografie	k1gFnSc1
</s>
<s>
Joe	Joe	k?
Jenčík	Jenčík	k1gInSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kat	kat	k1gMnSc1
a	a	k8xC
blázen	blázen	k1gMnSc1
<g/>
,	,	kIx,
satirická	satirický	k2eAgFnSc1d1
fantasie	fantasie	k1gFnSc1
o	o	k7c6
deseti	deset	k4xCc6
obrazech	obraz	k1gInPc6
je	být	k5eAaImIp3nS
19	#num#	k4
<g/>
.	.	kIx.
divadelní	divadelní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
autorů	autor	k1gMnPc2
Jana	Jan	k1gMnSc2
Wericha	Werich	k1gMnSc2
a	a	k8xC
Jiřího	Jiří	k1gMnSc2
Voskovce	Voskovec	k1gMnSc2
s	s	k7c7
hudbou	hudba	k1gFnSc7
Jaroslava	Jaroslav	k1gMnSc2
Ježka	Ježek	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
Osvobozeném	osvobozený	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Premiéra	premiéra	k1gFnSc1
byla	být	k5eAaImAgFnS
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1934	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režie	režie	k1gFnSc1
Jindřich	Jindřich	k1gMnSc1
Honzl	Honzl	k1gMnSc1
<g/>
,	,	kIx,
hudba	hudba	k1gFnSc1
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
,	,	kIx,
balet	balet	k1gInSc1
a	a	k8xC
choreografie	choreografie	k1gFnSc1
Joe	Joe	k1gMnSc1
Jenčík	Jenčík	k1gMnSc1
<g/>
,	,	kIx,
výprava	výprava	k1gFnSc1
a	a	k8xC
kostýmy	kostým	k1gInPc4
Bedřich	Bedřich	k1gMnSc1
Feuerstein	Feuerstein	k1gMnSc1
a	a	k8xC
Alois	Alois	k1gMnSc1
Wachsman	Wachsman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Hra	hra	k1gFnSc1
získala	získat	k5eAaPmAgFnS
podtitul	podtitul	k1gInSc4
satirická	satirický	k2eAgFnSc1d1
fantazie	fantazie	k1gFnSc1
o	o	k7c6
deseti	deset	k4xCc6
obrazech	obraz	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
předmluvě	předmluva	k1gFnSc6
k	k	k7c3
této	tento	k3xDgFnSc3
hře	hra	k1gFnSc3
napsali	napsat	k5eAaBmAgMnP,k5eAaPmAgMnP
Werich	Werich	k1gMnSc1
a	a	k8xC
Voskovec	Voskovec	k1gMnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
radši	rád	k6eAd2
psali	psát	k5eAaImAgMnP
pro	pro	k7c4
své	svůj	k3xOyFgNnSc4
divadlo	divadlo	k1gNnSc4
"	"	kIx"
<g/>
divoké	divoký	k2eAgFnSc2d1
fantazie	fantazie	k1gFnSc2
<g/>
,	,	kIx,
bláznivé	bláznivý	k2eAgFnSc2d1
frašky	fraška	k1gFnSc2
a	a	k8xC
absurdní	absurdní	k2eAgFnSc2d1
pohádky	pohádka	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proč	proč	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
správné	správný	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
má	mít	k5eAaImIp3nS
diváky	divák	k1gMnPc4
odvést	odvést	k5eAaPmF
na	na	k7c4
míle	míle	k1gFnPc4
daleko	daleko	k6eAd1
od	od	k7c2
šedé	šedý	k2eAgFnSc2d1
skutečnosti	skutečnost	k1gFnSc2
do	do	k7c2
časoprostoru	časoprostor	k1gInSc2
nereálného	reálný	k2eNgInSc2d1
<g/>
,	,	kIx,
absolutně	absolutně	k6eAd1
vymyšleného	vymyšlený	k2eAgNnSc2d1
a	a	k8xC
nesmyslného	smyslný	k2eNgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
jelikož	jelikož	k8xS
se	se	k3xPyFc4
psal	psát	k5eAaImAgInS
rok	rok	k1gInSc1
1934	#num#	k4
a	a	k8xC
skutečnost	skutečnost	k1gFnSc1
kolem	kolem	k7c2
nich	on	k3xPp3gMnPc2
nebyla	být	k5eNaImAgFnS
šedá	šedá	k1gFnSc1
a	a	k8xC
obyčejná	obyčejný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
stala	stát	k5eAaPmAgFnS
se	s	k7c7
"	"	kIx"
<g/>
strašlivou	strašlivý	k2eAgFnSc7d1
konkurentkou	konkurentka	k1gFnSc7
tohoto	tento	k3xDgNnSc2
ideálního	ideální	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
rozhodli	rozhodnout	k5eAaPmAgMnP
se	se	k3xPyFc4
nezůstat	zůstat	k5eNaPmF
k	k	k7c3
ní	on	k3xPp3gFnSc3
chladnými	chladný	k2eAgFnPc7d1
<g/>
,	,	kIx,
ale	ale	k8xC
naopak	naopak	k6eAd1
na	na	k7c4
ni	on	k3xPp3gFnSc4
reagovat	reagovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
poetismu	poetismus	k1gInSc2
přešla	přejít	k5eAaPmAgFnS
tvorba	tvorba	k1gFnSc1
Osvobozeného	osvobozený	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
k	k	k7c3
satirické	satirický	k2eAgFnSc3d1
<g/>
,	,	kIx,
ostře	ostro	k6eAd1
společensky	společensky	k6eAd1
zaměřené	zaměřený	k2eAgFnSc3d1
tvorbě	tvorba	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
reagovat	reagovat	k5eAaBmF
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
právě	právě	k9
děje	dít	k5eAaImIp3nS
<g/>
:	:	kIx,
Adolf	Adolf	k1gMnSc1
Hitler	Hitler	k1gMnSc1
v	v	k7c6
Německu	Německo	k1gNnSc6
uchvátil	uchvátit	k5eAaPmAgInS
moc	moc	k6eAd1
a	a	k8xC
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
zbavovat	zbavovat	k5eAaImF
svých	svůj	k3xOyFgMnPc2
odpůrců	odpůrce	k1gMnPc2
těmi	ten	k3xDgFnPc7
nejkrutějšími	krutý	k2eAgFnPc7d3
cestami	cesta	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezaměstnanost	nezaměstnanost	k1gFnSc1
rostla	růst	k5eAaImAgFnS
a	a	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
krize	krize	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
plném	plný	k2eAgInSc6d1
květu	květ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Proti	proti	k7c3
hře	hra	k1gFnSc3
protestovalo	protestovat	k5eAaBmAgNnS
mexické	mexický	k2eAgNnSc1d1
velvyslanectví	velvyslanectví	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Požadovalo	požadovat	k5eAaImAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
vynecháno	vynechán	k2eAgNnSc1d1
vše	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
nějak	nějak	k6eAd1
souviselo	souviset	k5eAaImAgNnS
s	s	k7c7
Mexikem	Mexiko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
se	se	k3xPyFc4
pozměnil	pozměnit	k5eAaPmAgMnS
i	i	k9
původní	původní	k2eAgInSc4d1
text	text	k1gInSc4
mexické	mexický	k2eAgFnSc2d1
hymny	hymna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Představení	představení	k1gNnSc4
byla	být	k5eAaImAgFnS
často	často	k6eAd1
narušována	narušován	k2eAgFnSc1d1
útoky	útok	k1gInPc4
fašistů	fašista	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
vytloukali	vytloukat	k5eAaImAgMnP
okna	okno	k1gNnSc2
a	a	k8xC
snažili	snažit	k5eAaImAgMnP
se	se	k3xPyFc4
všelijak	všelijak	k6eAd1
narušovat	narušovat	k5eAaImF
chod	chod	k1gInSc4
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yRnSc7,k3yQnSc7,k3yInSc7
dál	daleko	k6eAd2
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
přítomných	přítomný	k2eAgMnPc2d1
diváků	divák	k1gMnPc2
si	se	k3xPyFc3
s	s	k7c7
nimi	on	k3xPp3gNnPc7
ale	ale	k8xC
vědělo	vědět	k5eAaImAgNnS
čím	co	k3yRnSc7,k3yQnSc7,k3yInSc7
dál	daleko	k6eAd2
tím	ten	k3xDgNnSc7
lépe	dobře	k6eAd2
rady	rada	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osvobozené	osvobozený	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
fungovalo	fungovat	k5eAaImAgNnS
dál	daleko	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Gogolova	Gogolův	k2eAgNnPc1d1
slova	slovo	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yQgNnPc4,k3yRgNnPc4,k3yIgNnPc4
použili	použít	k5eAaPmAgMnP
i	i	k8xC
V	V	kA
<g/>
+	+	kIx~
<g/>
W	W	kA
v	v	k7c6
úvodu	úvod	k1gInSc6
k	k	k7c3
této	tento	k3xDgFnSc3
hře	hra	k1gFnSc3
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Zlobí	zlobit	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
zrcadlo	zrcadlo	k1gNnSc4
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
má	mít	k5eAaImIp3nS
křivou	křivý	k2eAgFnSc4d1
hubu	huba	k1gFnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
</s>
<s>
Osoby	osoba	k1gFnPc1
a	a	k8xC
premiérové	premiérový	k2eAgNnSc1d1
obsazení	obsazení	k1gNnSc1
</s>
<s>
Don	Don	k1gMnSc1
Blasco	Blasca	k1gMnSc5
Ibane	Iban	k1gMnSc5
<g/>
,	,	kIx,
radikální	radikální	k2eAgFnPc4d1
revolucionářF	revolucionářF	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černý	Černý	k1gMnSc1
</s>
<s>
Don	Don	k1gMnSc1
Vasco	Vasco	k1gMnSc1
Ibayo	Ibayo	k1gMnSc1
<g/>
,	,	kIx,
vlastenecký	vlastenecký	k2eAgInSc1d1
plantážníkV	plantážníkV	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trégl	Trégl	k1gInSc1
</s>
<s>
Rodrigo	Rodrigo	k1gMnSc1
Ibayo	Ibayo	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc1
synF	synF	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filipovský	Filipovský	k1gMnSc1
</s>
<s>
Doňa	doňa	k1gFnSc1
Cocepcion	Cocepcion	k1gInSc1
Ibane	Iban	k1gInSc5
<g/>
,	,	kIx,
MatkaNárodaJ	MatkaNárodaJ	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švabíková	Švabíková	k1gFnSc1
</s>
<s>
Dolores	Dolores	k1gInSc1
<g/>
,	,	kIx,
Dcera	dcera	k1gFnSc1
NárodaH	NárodaH	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítová	Vítová	k1gFnSc1
</s>
<s>
Don	Don	k1gMnSc1
Cristobal	Cristobal	k1gMnSc1
Almara	almara	k1gFnSc1
<g/>
,	,	kIx,
putykářJ	putykářJ	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plachta	plachta	k1gFnSc1
</s>
<s>
Juanilla	Juanilla	k1gFnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
neteřL	neteřL	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herrmannová	Herrmannová	k1gFnSc1
</s>
<s>
Don	Don	k1gMnSc1
Baltazar	Baltazar	k1gMnSc1
Carierra	Carierra	k1gMnSc1
<g/>
,	,	kIx,
oficiálB	oficiálB	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záhorský	záhorský	k2eAgInSc1d1
</s>
<s>
Gaspar	Gaspar	k1gMnSc1
Radůzo	Radůza	k1gFnSc5
<g/>
,	,	kIx,
katJ	katJ	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voskovec	Voskovec	k1gMnSc1
</s>
<s>
Melichar	Melichar	k1gMnSc1
Mahuleno	Mahulena	k1gFnSc5
<g/>
,	,	kIx,
vězeňJ	vězeňJ	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Werich	Werich	k1gMnSc1
</s>
<s>
zpěváci	zpěvák	k1gMnPc1
serenády	serenáda	k1gFnSc2
<g/>
,	,	kIx,
teroadoři	teroador	k1gMnPc1
<g/>
,	,	kIx,
tanečnice	tanečnice	k1gFnPc1
<g/>
,	,	kIx,
Bílé	bílý	k2eAgFnPc1d1
sestry	sestra	k1gFnPc1
<g/>
,	,	kIx,
Hodnostáři	hodnostář	k1gMnPc1
<g/>
,	,	kIx,
ceremoniáři	ceremoniář	k1gMnPc1
<g/>
,	,	kIx,
diktátorská	diktátorský	k2eAgFnSc1d1
garda	garda	k1gFnSc1
<g/>
,	,	kIx,
jeptišky	jeptiška	k1gFnPc1
a	a	k8xC
lid	lid	k1gInSc1
mexický	mexický	k2eAgInSc1d1
</s>
<s>
Děje	dít	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
imaginárním	imaginární	k2eAgNnSc6d1
Mexiku	Mexiko	k1gNnSc6
mimo	mimo	k7c4
čas	čas	k1gInSc4
a	a	k8xC
prostor	prostor	k1gInSc4
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	V	kA
<g/>
+	+	kIx~
<g/>
W	W	kA
zasadili	zasadit	k5eAaPmAgMnP
Kata	kat	k1gMnSc4
a	a	k8xC
blázna	blázen	k1gMnSc4
do	do	k7c2
imaginárního	imaginární	k2eAgNnSc2d1
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
kterému	který	k3yRgNnSc3,k3yQgNnSc3,k3yIgNnSc3
není	být	k5eNaImIp3nS
přiřazen	přiřazen	k2eAgInSc4d1
žádný	žádný	k3yNgInSc4
konkrétní	konkrétní	k2eAgInSc4d1
bod	bod	k1gInSc4
v	v	k7c6
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kam	kam	k6eAd1
doopravdy	doopravdy	k6eAd1
mířili	mířit	k5eAaImAgMnP
lze	lze	k6eAd1
odhalit	odhalit	k5eAaPmF
ze	z	k7c2
jmen	jméno	k1gNnPc2
dvou	dva	k4xCgFnPc2
postav	postava	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
si	se	k3xPyFc3
samozřejmě	samozřejmě	k6eAd1
zahráli	zahrát	k5eAaPmAgMnP
právě	právě	k6eAd1
V	V	kA
<g/>
+	+	kIx~
<g/>
W	W	kA
-	-	kIx~
kata	kat	k1gMnSc2
Gaspara	Gaspar	k1gMnSc2
Radůza	Radůz	k1gMnSc2
a	a	k8xC
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
vězně	vězeň	k1gMnSc2
Melichara	Melichar	k1gMnSc2
Mahulena	Mahulena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
scéna	scéna	k1gFnSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
dvacet	dvacet	k4xCc1
let	léto	k1gNnPc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
tito	tento	k3xDgMnPc1
dva	dva	k4xCgMnPc1
-	-	kIx~
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
národní	národní	k2eAgMnPc1d1
hrdinové	hrdina	k1gMnPc1
<g/>
,	,	kIx,
na	na	k7c4
jejichž	jejichž	k3xOyRp3gFnSc4
počest	počest	k1gFnSc4
je	být	k5eAaImIp3nS
zrovna	zrovna	k6eAd1
slaven	slaven	k2eAgInSc1d1
svátek	svátek	k1gInSc1
-	-	kIx~
skonali	skonat	k5eAaPmAgMnP
strašlivou	strašlivý	k2eAgFnSc7d1
smrtí	smrt	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
dvaceti	dvacet	k4xCc7
lety	let	k1gInPc7
zde	zde	k6eAd1
vládl	vládnout	k5eAaImAgInS
císař	císař	k1gMnSc1
<g/>
,	,	kIx,
o	o	k7c4
jehož	jenž	k3xRgNnSc4,k3xOyRp3gNnSc4
svržení	svržení	k1gNnSc4
se	se	k3xPyFc4
snažily	snažit	k5eAaImAgFnP
dvě	dva	k4xCgFnPc1
protichůdné	protichůdný	k2eAgFnPc1d1
strany	strana	k1gFnPc1
-	-	kIx~
Radikální	radikální	k2eAgMnPc1d1
Revolucionáři	revolucionář	k1gMnPc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
donem	don	k1gMnSc7
Blasco	Blasco	k6eAd1
Ibane	Iban	k1gInSc5
a	a	k8xC
Vlastenečtí	vlastenecký	k2eAgMnPc1d1
Plantážníci	plantážník	k1gMnPc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
donem	don	k1gMnSc7
Vasco	Vasco	k6eAd1
Ibayem	Ibayem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
nyní	nyní	k6eAd1
spojují	spojovat	k5eAaImIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vládli	vládnout	k5eAaImAgMnP
ve	v	k7c6
jménu	jméno	k1gNnSc6
národních	národní	k2eAgMnPc2d1
hrdinů	hrdina	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bohužel	bohužel	k6eAd1
je	on	k3xPp3gInPc4
čeká	čekat	k5eAaImIp3nS
strašlivé	strašlivý	k2eAgNnSc1d1
zjištění	zjištění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
vypraví	vypravit	k5eAaPmIp3nS
na	na	k7c4
ostrov	ostrov	k1gInSc4
Svatého	svatý	k2eAgMnSc2d1
Pankráce	Pankrác	k1gMnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
měli	mít	k5eAaImAgMnP
oba	dva	k4xCgMnPc1
muži	muž	k1gMnPc1
před	před	k7c7
dvaceti	dvacet	k4xCc7
lety	let	k1gInPc7
zemřít	zemřít	k5eAaPmF
<g/>
,	,	kIx,
najdou	najít	k5eAaPmIp3nP
je	on	k3xPp3gMnPc4
oba	dva	k4xCgMnPc4
živé	živý	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mahuleno	Mahulena	k1gFnSc5
byl	být	k5eAaImAgInS
nevinný	vinný	k2eNgMnSc1d1
a	a	k8xC
Radůzo	Radůza	k1gFnSc5
nebyl	být	k5eNaImAgInS
schopen	schopen	k2eAgMnSc1d1
jej	on	k3xPp3gMnSc4
zabít	zabít	k5eAaPmF
<g/>
,	,	kIx,
proto	proto	k8xC
zatloukl	zatlouct	k5eAaPmAgMnS
do	do	k7c2
gilotiny	gilotina	k1gFnSc2
hřebík	hřebík	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
nemohla	moct	k5eNaImAgFnS
vykonat	vykonat	k5eAaPmF
své	svůj	k3xOyFgNnSc4
dílo	dílo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mahuleno	Mahulena	k1gFnSc5
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
sťat	stnout	k5eAaPmNgMnS
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
křikl	křiknout	k5eAaPmAgMnS
na	na	k7c4
císaře	císař	k1gMnSc4
FUJ	fuj	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
ale	ale	k8xC
křikl	křiknout	k5eAaPmAgMnS
FUJ	fuj	k0
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
utrousil	utrousit	k5eAaPmAgMnS
jezevčík	jezevčík	k1gMnSc1
vedle	vedle	k7c2
něj	on	k3xPp3gNnSc2
<g/>
,	,	kIx,
nešťastnou	šťastný	k2eNgFnSc7d1
náhodou	náhoda	k1gFnSc7
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
zrovna	zrovna	k6eAd1
v	v	k7c4
okamžik	okamžik	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
kolem	kolem	k6eAd1
slavně	slavně	k6eAd1
projížděl	projíždět	k5eAaImAgMnS
císař	císař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomalu	pomalu	k6eAd1
se	se	k3xPyFc4
začínají	začínat	k5eAaImIp3nP
odhalovat	odhalovat	k5eAaImF
intriky	intrika	k1gFnPc4
Ibaya	Ibayum	k1gNnSc2
a	a	k8xC
Ibane	Iban	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ibane	Iban	k1gInSc5
zplodil	zplodit	k5eAaPmAgMnS
s	s	k7c7
donou	dona	k1gFnSc7
Concepcion	Concepcion	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
nazývána	nazývat	k5eAaImNgFnS
Matkou	matka	k1gFnSc7
vlasti	vlast	k1gFnSc2
<g/>
,	,	kIx,
dceru	dcera	k1gFnSc4
Dolores	Doloresa	k1gFnPc2
(	(	kIx(
<g/>
Dcera	dcera	k1gFnSc1
vlasti	vlast	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohodli	dohodnout	k5eAaPmAgMnP
se	se	k3xPyFc4
ale	ale	k9
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
budou	být	k5eAaImBp3nP
vydávat	vydávat	k5eAaImF,k5eAaPmF
za	za	k7c4
dceru	dcera	k1gFnSc4
Mahulena	Mahulena	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
Ibane	Iban	k1gInSc5
se	se	k3xPyFc4
stylizuje	stylizovat	k5eAaImIp3nS
do	do	k7c2
role	role	k1gFnSc2
hodného	hodný	k2eAgMnSc4d1
muže	muž	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
ujme	ujmout	k5eAaPmIp3nS
jako	jako	k9
vlastní	vlastní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
si	se	k3xPyFc3
má	mít	k5eAaImIp3nS
získat	získat	k5eAaPmF
srdce	srdce	k1gNnSc4
národa	národ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ibayo	Ibayo	k6eAd1
má	mít	k5eAaImIp3nS
pak	pak	k6eAd1
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
rodiny	rodina	k1gFnSc2
jako	jako	k9
o	o	k7c4
mnoho	mnoho	k6eAd1
starší	starý	k2eAgMnSc1d2
manžel	manžel	k1gMnSc1
mladé	mladý	k2eAgFnSc2d1
a	a	k8xC
odvážné	odvážný	k2eAgFnSc2d1
Dolores	Dolores	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
ale	ale	k8xC
o	o	k7c6
všem	všecek	k3xTgNnSc6
ví	vědět	k5eAaImIp3nS
a	a	k8xC
své	svůj	k3xOyFgMnPc4
rodiče	rodič	k1gMnSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
dva	dva	k4xCgMnPc4
chamtivce	chamtivec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dopisů	dopis	k1gInPc2
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
je	být	k5eAaImIp3nS
její	její	k3xOp3gFnSc4
pravý	pravý	k2eAgMnSc1d1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
nijak	nijak	k6eAd1
se	se	k3xPyFc4
s	s	k7c7
tím	ten	k3xDgNnSc7
před	před	k7c7
rodiči	rodič	k1gMnPc7
netají	tajit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
aby	aby	kYmCp3nS
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c2
Ibaya	Ibay	k1gInSc2
<g/>
,	,	kIx,
raději	rád	k6eAd2
jde	jít	k5eAaImIp3nS
do	do	k7c2
kláštera	klášter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
Mahuleno	Mahulena	k1gFnSc5
a	a	k8xC
Radůzo	Radůza	k1gFnSc5
dostanou	dostat	k5eAaPmIp3nP
z	z	k7c2
ostrovu	ostrov	k1gInSc3
dřív	dříve	k6eAd2
než	než	k8xS
jim	on	k3xPp3gMnPc3
v	v	k7c6
tom	ten	k3xDgNnSc6
mohou	moct	k5eAaImIp3nP
zabránit	zabránit	k5eAaPmF
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nS
najít	najít	k5eAaPmF
nové	nový	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
situace	situace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
provolají	provolat	k5eAaPmIp3nP
za	za	k7c7
diktátory	diktátor	k1gMnPc7
a	a	k8xC
dají	dát	k5eAaPmIp3nP
jim	on	k3xPp3gMnPc3
svou	svůj	k3xOyFgFnSc4
slávu	sláva	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ale	ale	k9
nemá	mít	k5eNaImIp3nS
trvat	trvat	k5eAaImF
dlouho	dlouho	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodlají	hodlat	k5eAaImIp3nP
totiž	totiž	k9
vládnout	vládnout	k5eAaImF
za	za	k7c4
ně	on	k3xPp3gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nosí	nosit	k5eAaImIp3nS
jim	on	k3xPp3gMnPc3
všelijaké	všelijaký	k3yIgInPc4
listiny	listina	k1gFnPc1
na	na	k7c4
podepsání	podepsání	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
většinou	většinou	k6eAd1
rozhodují	rozhodovat	k5eAaImIp3nP
o	o	k7c6
útlaku	útlak	k1gInSc6
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
štvou	štvát	k5eAaImIp3nP
tak	tak	k6eAd1
lid	lid	k1gInSc4
ne	ne	k9
proti	proti	k7c3
sobě	se	k3xPyFc3
<g/>
,	,	kIx,
ale	ale	k8xC
proti	proti	k7c3
Mahulenovi	Mahulen	k1gMnSc3
a	a	k8xC
Radůzovi	Radůz	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Myslí	myslet	k5eAaImIp3nS
si	se	k3xPyFc3
o	o	k7c6
nich	on	k3xPp3gFnPc6
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
Kat	kat	k1gMnSc1
a	a	k8xC
Blázen	blázen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
na	na	k7c4
ně	on	k3xPp3gInPc4
uspořádají	uspořádat	k5eAaPmIp3nP
atentát	atentát	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
vykonat	vykonat	k5eAaPmF
Carriera	Carrier	k1gMnSc4
-	-	kIx~
úředník	úředník	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
kdysi	kdysi	k6eAd1
křivě	křivě	k6eAd1
svědčil	svědčit	k5eAaImAgInS
o	o	k7c6
Mahulenově	Mahulenův	k2eAgNnSc6d1
provinění	provinění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celou	celý	k2eAgFnSc7d1
dobu	doba	k1gFnSc4
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
vyčítal	vyčítat	k5eAaImAgMnS
a	a	k8xC
teď	teď	k6eAd1
vraždit	vraždit	k5eAaImF
nechce	chtít	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
přece	přece	k9
jen	jen	k9
převleče	převléct	k5eAaPmIp3nS
za	za	k7c4
poustevníka	poustevník	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
k	k	k7c3
diktátorům	diktátor	k1gMnPc3
chodil	chodit	k5eAaImAgMnS
doučovat	doučovat	k5eAaImF
dějepis	dějepis	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
hodlá	hodlat	k5eAaImIp3nS
je	on	k3xPp3gInPc4
zabít	zabít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
je	být	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
za	za	k7c4
sebe	sebe	k3xPyFc4
tak	tak	k6eAd1
zoufalý	zoufalý	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
chce	chtít	k5eAaImIp3nS
spáchat	spáchat	k5eAaPmF
sebevraždu	sebevražda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
k	k	k7c3
tomu	ten	k3xDgNnSc3
nedokáže	dokázat	k5eNaPmIp3nS
najít	najít	k5eAaPmF
odvahu	odvaha	k1gFnSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
mu	on	k3xPp3gMnSc3
Mahuleno	Mahulena	k1gFnSc5
a	a	k8xC
Radůzo	Radůza	k1gFnSc5
slíbí	slíbit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc4
udělají	udělat	k5eAaPmIp3nP
obráceně	obráceně	k6eAd1
-	-	kIx~
on	on	k3xPp3gMnSc1
bude	být	k5eAaImBp3nS
diktátor	diktátor	k1gMnSc1
a	a	k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
tedy	tedy	k9
na	na	k7c4
něj	on	k3xPp3gMnSc4
budou	být	k5eAaImBp3nP
moci	moct	k5eAaImF
spáchat	spáchat	k5eAaPmF
atentát	atentát	k1gInSc4
a	a	k8xC
tak	tak	k6eAd1
mu	on	k3xPp3gMnSc3
pomohou	pomoct	k5eAaPmIp3nP
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
trápením	trápení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
Carriera	Carriera	k1gFnSc1
diktátorem	diktátor	k1gMnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
si	se	k3xPyFc3
stále	stále	k6eAd1
více	hodně	k6eAd2
vědom	vědom	k2eAgMnSc1d1
své	svůj	k3xOyFgInPc4
moci	moct	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gFnSc6
osobě	osoba	k1gFnSc6
ztvárnili	ztvárnit	k5eAaPmAgMnP
V	V	kA
<g/>
+	+	kIx~
<g/>
W	W	kA
postavu	postava	k1gFnSc4
typického	typický	k2eAgMnSc2d1
diktátora	diktátor	k1gMnSc2
-	-	kIx~
nenasytného	nasytný	k2eNgMnSc2d1
a	a	k8xC
bezohledně	bezohledně	k6eAd1
postupujícího	postupující	k2eAgNnSc2d1
<g/>
,	,	kIx,
bez	bez	k7c2
jakéhokoli	jakýkoli	k3yIgNnSc2
svědomí	svědomí	k1gNnSc2
<g/>
,	,	kIx,
schopného	schopný	k2eAgMnSc2d1
vraždit	vraždit	k5eAaImF
nevinné	vinný	k2eNgInPc4d1
jen	jen	k9
aby	aby	kYmCp3nS
získal	získat	k5eAaPmAgInS
další	další	k2eAgFnPc4d1
a	a	k8xC
další	další	k2eAgFnPc4d1
hodnosti	hodnost	k1gFnPc4
a	a	k8xC
slávu	sláva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
vyvraždí	vyvraždit	k5eAaPmIp3nS
Ibaya	Ibaya	k1gMnSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
syna	syn	k1gMnSc4
Rodríga	Rodríg	k1gMnSc4
<g/>
,	,	kIx,
Ibane	Iban	k1gInSc5
také	také	k9
není	být	k5eNaImIp3nS
ušetřen	ušetřen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
zastřelí	zastřelit	k5eAaPmIp3nP
sám	sám	k3xTgInSc4
jako	jako	k9
blázen	blázen	k1gMnSc1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
si	se	k3xPyFc3
to	ten	k3xDgNnSc4
nechá	nechat	k5eAaPmIp3nS
poradit	poradit	k5eAaPmF
od	od	k7c2
svého	svůj	k3xOyFgInSc2
vlastního	vlastní	k2eAgInSc2d1
stínu	stín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechce	chtít	k5eNaImIp3nS
poslouchat	poslouchat	k5eAaImF
jeho	jeho	k3xOp3gFnPc4
rady	rada	k1gFnPc4
a	a	k8xC
chce	chtít	k5eAaImIp3nS
zastřelit	zastřelit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
stín	stín	k1gInSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
umírá	umírat	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
zbudou	zbýt	k5eAaPmIp3nP
na	na	k7c6
scéně	scéna	k1gFnSc6
jen	jen	k9
Dolores	Dolores	k1gMnSc1
a	a	k8xC
Mahuleno	Mahulena	k1gFnSc5
<g/>
,	,	kIx,
Juanilla	Juanilla	k1gMnSc1
(	(	kIx(
<g/>
neteř	neteř	k1gFnSc1
podlého	podlý	k2eAgNnSc2d1
a	a	k8xC
bezohledného	bezohledný	k2eAgMnSc2d1
putykáře	putykář	k1gMnSc2
Almary	almara	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
také	také	k9
padl	padnout	k5eAaPmAgMnS,k5eAaImAgMnS
rukou	ruka	k1gFnSc7
Carriery	Carriera	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Radůzo	Radůza	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dívky	dívka	k1gFnPc1
se	se	k3xPyFc4
zdráhají	zdráhat	k5eAaImIp3nP
si	se	k3xPyFc3
je	on	k3xPp3gMnPc4
vzít	vzít	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
nejsou	být	k5eNaImIp3nP
obyčejní	obyčejný	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
národní	národní	k2eAgMnPc1d1
hrdinové	hrdina	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ale	ale	k8xC
oni	onen	k3xDgMnPc1,k3xPp3gMnPc1
jsou	být	k5eAaImIp3nP
naprosto	naprosto	k6eAd1
obyčejní	obyčejný	k2eAgMnPc1d1
a	a	k8xC
o	o	k7c4
to	ten	k3xDgNnSc4
ve	v	k7c6
světě	svět	k1gInSc6
jde	jít	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvědomit	uvědomit	k5eAaPmF
si	se	k3xPyFc3
<g/>
,	,	kIx,
že	že	k8xS
všichni	všechen	k3xTgMnPc1
obyčejní	obyčejný	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
měli	mít	k5eAaImAgMnP
spojit	spojit	k5eAaPmF
a	a	k8xC
přiložit	přiložit	k5eAaPmF
ruku	ruka	k1gFnSc4
k	k	k7c3
dílu	dílo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
závěr	závěr	k1gInSc4
všichni	všechen	k3xTgMnPc1
zpívají	zpívat	k5eAaImIp3nP
píseň	píseň	k1gFnSc4
HEJ	hej	k6eAd1
RUP	rupět	k5eAaImRp2nS
<g/>
!	!	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hudba	hudba	k1gFnSc1
ze	z	k7c2
hry	hra	k1gFnSc2
</s>
<s>
Ve	v	k7c6
hře	hra	k1gFnSc6
zazní	zaznít	k5eAaPmIp3nS,k5eAaImIp3nS
tango	tango	k1gNnSc1
Co	co	k9
na	na	k7c6
světě	svět	k1gInSc6
vadí	vadit	k5eAaImIp3nS
<g/>
,	,	kIx,
Svítá	svítat	k5eAaImIp3nS
<g/>
,	,	kIx,
Šaty	šata	k1gFnPc1
dělaj	dělaj	k?
<g/>
´	´	k?
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
Když	když	k8xS
jsem	být	k5eAaImIp1nS
kytici	kytice	k1gFnSc4
vázala	vázat	k5eAaImAgFnS
<g/>
,	,	kIx,
Kat	kat	k1gMnSc1
a	a	k8xC
blázen	blázen	k1gMnSc1
<g/>
,	,	kIx,
Hej	hej	k6eAd1
rup	rupět	k5eAaImRp2nS
<g/>
!	!	kIx.
</s>
<s>
Nahrávky	nahrávka	k1gFnPc1
</s>
<s>
Některé	některý	k3yIgFnPc1
písně	píseň	k1gFnPc1
a	a	k8xC
orchestrální	orchestrální	k2eAgFnPc1d1
skladby	skladba	k1gFnPc1
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
zaznamenány	zaznamenat	k5eAaPmNgInP
na	na	k7c4
gramofonové	gramofonový	k2eAgFnPc4d1
desky	deska	k1gFnPc4
firmy	firma	k1gFnSc2
Ultraphon	Ultraphona	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Full	Full	k1gMnSc1
Hand	Hand	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
,	,	kIx,
orchestrální	orchestrální	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
<g/>
,	,	kIx,
dirigent	dirigent	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
,	,	kIx,
nahráno	nahrát	k5eAaPmNgNnS,k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
istopadu	istopad	k1gInSc2
1934	#num#	k4
</s>
<s>
Svítá	svítat	k5eAaImIp3nS
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
/	/	kIx~
Voskovec	Voskovec	k1gMnSc1
a	a	k8xC
Werich	Werich	k1gMnSc1
<g/>
,	,	kIx,
dirigent	dirigent	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
,	,	kIx,
nahráno	nahrát	k5eAaPmNgNnS,k5eAaBmNgNnS
12	#num#	k4
<g/>
.	.	kIx.
istopadu	istopad	k1gInSc2
1937	#num#	k4
</s>
<s>
Carliosa	Carliosa	k1gFnSc1
sol	sol	k1gNnSc2
y	y	k?
umbra	umbra	k1gFnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslava	k1gFnPc2
Ježekorchestrální	Ježekorchestrální	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
<g/>
,	,	kIx,
dirigent	dirigent	k1gMnSc1
Robert	Robert	k1gMnSc1
Brock	Brock	k1gMnSc1
<g/>
,	,	kIx,
nahráno	nahrát	k5eAaBmNgNnS,k5eAaPmNgNnS
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1934	#num#	k4
</s>
<s>
Ředitel	ředitel	k1gMnSc1
<g/>
,	,	kIx,
učitel	učitel	k1gMnSc1
a	a	k8xC
Kukačka	kukačka	k1gFnSc1
<g/>
,	,	kIx,
Voskovec	Voskovec	k1gMnSc1
a	a	k8xC
Werich	Werich	k1gMnSc1
a	a	k8xC
Stanislav	Stanislav	k1gMnSc1
Kukačka	kukačka	k1gFnSc1
<g/>
,	,	kIx,
nahráno	nahrát	k5eAaBmNgNnS,k5eAaPmNgNnS
26	#num#	k4
<g/>
.	.	kIx.
istopadu	istopad	k1gInSc2
1934	#num#	k4
</s>
<s>
Pohádka	pohádka	k1gFnSc1
o	o	k7c6
katu	kat	k1gInSc6
a	a	k8xC
bláznu	blázen	k1gMnSc3
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
/	/	kIx~
Voskovec	Voskovec	k1gMnSc1
a	a	k8xC
Werich	Werich	k1gMnSc1
<g/>
,	,	kIx,
dirigent	dirigent	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
,	,	kIx,
nahráno	nahrát	k5eAaBmNgNnS,k5eAaPmNgNnS
26	#num#	k4
<g/>
.	.	kIx.
istopadu	istopad	k1gInSc2
1934	#num#	k4
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1
sestry	sestra	k1gFnPc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
,	,	kIx,
orchestrální	orchestrální	k2eAgFnSc1d1
skladba	skladba	k1gFnSc1
<g/>
,	,	kIx,
dirigent	dirigent	k1gMnSc1
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
<g/>
,	,	kIx,
nahráno	nahrát	k5eAaPmNgNnS,k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
istopadu	istopad	k1gInSc2
1934	#num#	k4
</s>
<s>
Peklo	peklo	k1gNnSc1
a	a	k8xC
ráj	ráj	k1gInSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Ježek	Ježek	k1gMnSc1
/	/	kIx~
Voskovec	Voskovec	k1gMnSc1
a	a	k8xC
Werich	Werich	k1gMnSc1
<g/>
,	,	kIx,
Melody	Melod	k1gMnPc4
Boys	boy	k1gMnPc2
<g/>
,	,	kIx,
dirigent	dirigent	k1gMnSc1
Robert	Robert	k1gMnSc1
Brock	Brock	k1gMnSc1
<g/>
,	,	kIx,
nahráno	nahrát	k5eAaPmNgNnS,k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
istopadu	istopad	k1gInSc2
1934	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
JUST	just	k6eAd1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osvobozené	osvobozený	k2eAgFnPc4d1
divadlo	divadlo	k1gNnSc4
1929	#num#	k4
<g/>
-	-	kIx~
<g/>
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Supraphon	supraphon	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
56	#num#	k4
s.	s.	k?
S.	S.	kA
18	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
VOSKOVEC	Voskovec	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
WERICH	Werich	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kat	Kata	k1gFnPc2
a	a	k8xC
blázen	blázen	k1gMnSc1
:	:	kIx,
satirická	satirický	k2eAgFnSc1d1
fantasie	fantasie	k1gFnSc1
o	o	k7c6
10	#num#	k4
obrazech	obraz	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Fr.	Fr.	k1gMnSc1
Borový	borový	k2eAgMnSc1d1
<g/>
,	,	kIx,
1934	#num#	k4
<g/>
.	.	kIx.
112	#num#	k4
s.	s.	k?
</s>
<s>
VOSKOVEC	Voskovec	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
WERICH	Werich	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Československý	československý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
.	.	kIx.
419	#num#	k4
s.	s.	k?
S.	S.	kA
291	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hry	hra	k1gFnPc1
Osvobozeného	osvobozený	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
</s>
<s>
Vest	vesta	k1gFnPc2
pocket	pocketa	k1gFnPc2
revue	revue	k1gFnSc2
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Smoking	smoking	k1gInSc1
revue	revue	k1gFnSc1
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gorila	gorila	k1gFnSc1
ex	ex	k6eAd1
machina	machin	k2eAgFnSc1d1
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kostky	kostka	k1gFnPc1
jsou	být	k5eAaImIp3nP
vrženy	vržen	k2eAgFnPc1d1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Premiéra	premiéra	k1gFnSc1
Skafandr	skafandr	k1gInSc1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fata	fatum	k1gNnSc2
Morgana	morgan	k1gMnSc2
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Ostrov	ostrov	k1gInSc1
Dynamit	dynamit	k1gInSc4
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sever	sever	k1gInSc4
proti	proti	k7c3
Jihu	jih	k1gInSc3
(	(	kIx(
<g/>
1930	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Don	Don	k1gMnSc1
Juan	Juan	k1gMnSc1
&	&	k?
comp	comp	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Golem	Golem	k1gMnSc1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Caesar	Caesar	k1gMnSc1
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Robin	robin	k2eAgMnSc1d1
zbojník	zbojník	k1gMnSc1
(	(	kIx(
<g/>
1932	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Svět	svět	k1gInSc1
za	za	k7c7
mřížemi	mříž	k1gFnPc7
(	(	kIx(
<g/>
1933	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Osel	osel	k1gMnSc1
a	a	k8xC
stín	stín	k1gInSc1
(	(	kIx(
<g/>
1933	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slaměný	slaměný	k2eAgInSc1d1
klobouk	klobouk	k1gInSc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kat	kat	k1gMnSc1
a	a	k8xC
blázen	blázen	k1gMnSc1
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vždy	vždy	k6eAd1
s	s	k7c7
úsměvem	úsměv	k1gInSc7
(	(	kIx(
<g/>
1934	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Panoptikum	panoptikum	k1gNnSc4
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Balada	balada	k1gFnSc1
z	z	k7c2
hadrů	hadr	k1gInPc2
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nebe	nebe	k1gNnSc1
na	na	k7c6
zemi	zem	k1gFnSc6
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rub	rub	k1gInSc1
a	a	k8xC
líc	líc	k1gInSc1
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Panorama	panorama	k1gNnSc1
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Těžká	těžký	k2eAgFnSc1d1
Barbora	Barbora	k1gFnSc1
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pěst	pěst	k1gFnSc4
na	na	k7c4
oko	oko	k1gNnSc4
(	(	kIx(
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
</s>
