<s>
Starobrněnský	Starobrněnský	k2eAgInSc4d1	Starobrněnský
klášter	klášter	k1gInSc4	klášter
založila	založit	k5eAaPmAgFnS	založit
jako	jako	k9	jako
klášter	klášter	k1gInSc4	klášter
cisterciaček	cisterciačka	k1gFnPc2	cisterciačka
roku	rok	k1gInSc2	rok
1323	[number]	k4	1323
česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
polská	polský	k2eAgFnSc1d1	polská
královna	královna	k1gFnSc1	královna
vdova	vdova	k1gFnSc1	vdova
Eliška	Eliška	k1gFnSc1	Eliška
Rejčka	Rejčka	k1gFnSc1	Rejčka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pochována	pochován	k2eAgFnSc1d1	pochována
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
moravským	moravský	k2eAgMnSc7d1	moravský
zemským	zemský	k2eAgMnSc7d1	zemský
hejtmanem	hejtman	k1gMnSc7	hejtman
Jindřichem	Jindřich	k1gMnSc7	Jindřich
z	z	k7c2	z
Lipé	Lipé	k1gNnSc2	Lipé
<g/>
.	.	kIx.	.
</s>
