<s>
Marie	Marie	k1gFnSc1	Marie
Curie-Skłodowská	Curie-Skłodowská	k1gFnSc1	Curie-Skłodowská
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Maria	Maria	k1gFnSc1	Maria
Salomea	Salomea	k1gFnSc1	Salomea
Skłodowska	Skłodowska	k1gFnSc1	Skłodowska
<g/>
,	,	kIx,	,
v	v	k7c6	v
polštině	polština	k1gFnSc6	polština
Maria	Maria	k1gFnSc1	Maria
Skłodowska-Curie	Skłodowska-Curie	k1gFnSc1	Skłodowska-Curie
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1867	[number]	k4	1867
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1934	[number]	k4	1934
Passy	Passa	k1gFnSc2	Passa
<g/>
,	,	kIx,	,
Haute-Savoie	Haute-Savoie	k1gFnSc1	Haute-Savoie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vědkyně	vědkyně	k1gFnSc1	vědkyně
polského	polský	k2eAgInSc2d1	polský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
