<s>
Makaróny	makarón	k1gInPc1	makarón
(	(	kIx(	(
<g/>
Maccheroni	Maccheron	k1gMnPc1	Maccheron
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
těstoviny	těstovina	k1gFnPc1	těstovina
podlouhlého	podlouhlý	k2eAgInSc2d1	podlouhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
makaróny	makarón	k1gInPc7	makarón
a	a	k8xC	a
špagetami	špagety	k1gFnPc7	špagety
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
makaróny	makarón	k1gInPc1	makarón
jsou	být	k5eAaImIp3nP	být
duté	dutý	k2eAgInPc1d1	dutý
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
i	i	k9	i
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
pojetí	pojetí	k1gNnSc6	pojetí
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
makaróny	makarón	k1gInPc4	makarón
nacházejí	nacházet	k5eAaImIp3nP	nacházet
krátké	krátká	k1gFnPc1	krátká
duté	dutý	k2eAgFnPc1d1	dutá
a	a	k8xC	a
zahnuté	zahnutý	k2eAgFnPc1d1	zahnutá
těstoviny	těstovina	k1gFnPc1	těstovina
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
obdoba	obdoba	k1gFnSc1	obdoba
českých	český	k2eAgNnPc2d1	české
kolínek	kolínko	k1gNnPc2	kolínko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnPc1	pojmenování
makaróny	makarón	k1gInPc1	makarón
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
italského	italský	k2eAgInSc2d1	italský
maccheroni	maccheroň	k1gFnSc6	maccheroň
(	(	kIx(	(
<g/>
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
od	od	k7c2	od
maccherone	maccheron	k1gMnSc5	maccheron
<g/>
,	,	kIx,	,
s	s	k7c7	s
významem	význam	k1gInSc7	význam
drcený	drcený	k2eAgInSc1d1	drcený
<g/>
,	,	kIx,	,
zmáčknutý	zmáčknutý	k2eAgInSc1d1	zmáčknutý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připravují	připravovat	k5eAaImIp3nP	připravovat
se	se	k3xPyFc4	se
vařením	vaření	k1gNnSc7	vaření
v	v	k7c6	v
osolené	osolený	k2eAgFnSc6d1	osolená
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
vaření	vaření	k1gNnSc2	vaření
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
při	při	k7c6	při
obědě	oběd	k1gInSc6	oběd
či	či	k8xC	či
večeři	večeře	k1gFnSc6	večeře
obvykle	obvykle	k6eAd1	obvykle
servírují	servírovat	k5eAaBmIp3nP	servírovat
se	s	k7c7	s
sýrovou	sýrový	k2eAgFnSc7d1	sýrová
<g/>
,	,	kIx,	,
masitou	masitý	k2eAgFnSc7d1	masitá
či	či	k8xC	či
česnekovou	česnekový	k2eAgFnSc7d1	česneková
omáčkou	omáčka	k1gFnSc7	omáčka
jako	jako	k9	jako
první	první	k4xOgInSc1	první
chod	chod	k1gInSc1	chod
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
primo	primo	k1gNnSc1	primo
piato	piato	k1gNnSc1	piato
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
místo	místo	k7c2	místo
polévky	polévka	k1gFnSc2	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
pojmenování	pojmenování	k1gNnSc1	pojmenování
Makarón	makarón	k1gInSc1	makarón
označuje	označovat	k5eAaImIp3nS	označovat
Itala	Ital	k1gMnSc4	Ital
(	(	kIx(	(
<g/>
mírně	mírně	k6eAd1	mírně
přezíravě	přezíravě	k6eAd1	přezíravě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Amarouny	Amaroun	k1gInPc1	Amaroun
Makaronismus	makaronismus	k1gInSc1	makaronismus
Špagety	špagety	k1gFnPc4	špagety
</s>
