<s>
Makaróny	makarón	k1gInPc1	makarón
(	(	kIx(	(
<g/>
Maccheroni	Maccheron	k1gMnPc1	Maccheron
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
těstoviny	těstovina	k1gFnPc1	těstovina
podlouhlého	podlouhlý	k2eAgInSc2d1	podlouhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
makaróny	makarón	k1gInPc7	makarón
a	a	k8xC	a
špagetami	špagety	k1gFnPc7	špagety
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
makaróny	makarón	k1gInPc1	makarón
jsou	být	k5eAaImIp3nP	být
duté	dutý	k2eAgInPc1d1	dutý
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
i	i	k9	i
kratší	krátký	k2eAgFnSc1d2	kratší
<g/>
.	.	kIx.	.
</s>
