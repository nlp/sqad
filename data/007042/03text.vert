<s>
Destilovaná	destilovaný	k2eAgFnSc1d1	destilovaná
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
vícekrát	vícekrát	k6eAd1	vícekrát
destilována	destilován	k2eAgFnSc1d1	destilována
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
byla	být	k5eAaImAgFnS	být
díky	díky	k7c3	díky
změně	změna	k1gFnSc3	změna
skupenství	skupenství	k1gNnSc2	skupenství
na	na	k7c4	na
vodní	vodní	k2eAgFnSc4d1	vodní
páru	pára	k1gFnSc4	pára
zbavena	zbavit	k5eAaPmNgFnS	zbavit
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
následně	následně	k6eAd1	následně
ochlazením	ochlazení	k1gNnSc7	ochlazení
znovu	znovu	k6eAd1	znovu
zkapalněna	zkapalněn	k2eAgFnSc1d1	zkapalněn
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
tak	tak	k6eAd1	tak
bývá	bývat	k5eAaImIp3nS	bývat
nepřesně	přesně	k6eNd1	přesně
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
výrobně	výrobně	k6eAd1	výrobně
méně	málo	k6eAd2	málo
nákladná	nákladný	k2eAgFnSc1d1	nákladná
demineralizovaná	demineralizovaný	k2eAgFnSc1d1	demineralizovaná
voda	voda	k1gFnSc1	voda
využívaná	využívaný	k2eAgFnSc1d1	využívaná
k	k	k7c3	k
podobným	podobný	k2eAgInPc3d1	podobný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
skupenství	skupenství	k1gNnSc2	skupenství
vody	voda	k1gFnSc2	voda
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
zvolna	zvolna	k6eAd1	zvolna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
slunci	slunce	k1gNnSc6	slunce
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zahříváním	zahřívání	k1gNnSc7	zahřívání
až	až	k9	až
k	k	k7c3	k
bodu	bod	k1gInSc3	bod
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
destilace	destilace	k1gFnSc2	destilace
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
separována	separován	k2eAgFnSc1d1	separována
prakticky	prakticky	k6eAd1	prakticky
od	od	k7c2	od
všeho	všecek	k3xTgNnSc2	všecek
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nachází	nacházet	k5eAaImIp3nS	nacházet
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
čistou	čistá	k1gFnSc4	čistá
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
čirá	čirý	k2eAgFnSc1d1	čirá
<g/>
,	,	kIx,	,
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
,	,	kIx,	,
v	v	k7c6	v
silné	silný	k2eAgFnSc6d1	silná
vrstvě	vrstva	k1gFnSc6	vrstva
namodralá	namodralý	k2eAgFnSc1d1	namodralá
kapalina	kapalina	k1gFnSc1	kapalina
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
nevodivá	vodivý	k2eNgFnSc1d1	nevodivá
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
tlaku	tlak	k1gInSc2	tlak
101	[number]	k4	101
kPa	kPa	k?	kPa
má	mít	k5eAaImIp3nS	mít
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnPc2	tání
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
teplotu	teplota	k1gFnSc4	teplota
varu	var	k1gInSc2	var
100	[number]	k4	100
°	°	k?	°
<g/>
C.	C.	kA	C.
Největší	veliký	k2eAgFnSc4d3	veliký
hustotu	hustota	k1gFnSc4	hustota
1,000	[number]	k4	1,000
g.	g.	k?	g.
<g/>
cm-	cm-	k?	cm-
<g/>
3	[number]	k4	3
má	mít	k5eAaImIp3nS	mít
voda	voda	k1gFnSc1	voda
při	při	k7c6	při
3,98	[number]	k4	3,98
°	°	k?	°
<g/>
C.	C.	kA	C.
pH	ph	kA	ph
destilované	destilovaný	k2eAgFnSc2d1	destilovaná
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgFnSc1d1	neutrální
(	(	kIx(	(
<g/>
pH	ph	kA	ph
<g/>
=	=	kIx~	=
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vodivost	vodivost	k1gFnSc1	vodivost
destilované	destilovaný	k2eAgFnSc2d1	destilovaná
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
11	[number]	k4	11
μ	μ	k?	μ
<g/>
.	.	kIx.	.
<g/>
cm-	cm-	k?	cm-
<g/>
1	[number]	k4	1
a	a	k8xC	a
celkové	celkový	k2eAgNnSc4d1	celkové
množství	množství	k1gNnSc4	množství
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pod	pod	k7c4	pod
10	[number]	k4	10
mg	mg	kA	mg
<g/>
.	.	kIx.	.
<g/>
dm-	dm-	k?	dm-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Destilovaná	destilovaný	k2eAgFnSc1d1	destilovaná
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
především	především	k9	především
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
<g/>
,	,	kIx,	,
při	při	k7c6	při
chemickém	chemický	k2eAgNnSc6d1	chemické
zpracování	zpracování	k1gNnSc6	zpracování
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
například	například	k6eAd1	například
do	do	k7c2	do
chladičů	chladič	k1gInPc2	chladič
automobilů	automobil	k1gInPc2	automobil
<g/>
,	,	kIx,	,
k	k	k7c3	k
doplňování	doplňování	k1gNnSc3	doplňování
elektrolytu	elektrolyt	k1gInSc2	elektrolyt
akumulátorů	akumulátor	k1gInPc2	akumulátor
nebo	nebo	k8xC	nebo
do	do	k7c2	do
napařovacích	napařovací	k2eAgFnPc2d1	napařovací
žehliček	žehlička	k1gFnPc2	žehlička
<g/>
.	.	kIx.	.
</s>
<s>
Všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
chemicky	chemicky	k6eAd1	chemicky
neutrální	neutrální	k2eAgFnPc1d1	neutrální
vody	voda	k1gFnPc1	voda
bez	bez	k7c2	bez
příměsí	příměs	k1gFnPc2	příměs
a	a	k8xC	a
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
destilovaná	destilovaný	k2eAgFnSc1d1	destilovaná
voda	voda	k1gFnSc1	voda
získaná	získaný	k2eAgFnSc1d1	získaná
přes	přes	k7c4	přes
reverzní	reverzní	k2eAgFnSc4d1	reverzní
osmózu	osmóza	k1gFnSc4	osmóza
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
v	v	k7c6	v
akváriích	akvárium	k1gNnPc6	akvárium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
minerální	minerální	k2eAgFnSc2d1	minerální
vody	voda	k1gFnSc2	voda
nezbude	zbýt	k5eNaPmIp3nS	zbýt
po	po	k7c6	po
odpaření	odpaření	k1gNnSc6	odpaření
destilované	destilovaný	k2eAgFnSc2d1	destilovaná
vody	voda	k1gFnSc2	voda
žádný	žádný	k3yNgInSc4	žádný
nerostný	nerostný	k2eAgInSc4d1	nerostný
materiál	materiál	k1gInSc4	materiál
(	(	kIx(	(
<g/>
vodní	vodní	k2eAgInSc4d1	vodní
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
kotelní	kotelní	k2eAgInSc4d1	kotelní
kámen	kámen	k1gInSc4	kámen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
destilovanou	destilovaný	k2eAgFnSc4d1	destilovaná
vodu	voda	k1gFnSc4	voda
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
vodu	voda	k1gFnSc4	voda
srážkovou	srážkový	k2eAgFnSc4d1	srážková
(	(	kIx(	(
<g/>
déšť	déšť	k1gInSc1	déšť
a	a	k8xC	a
sníh	sníh	k1gInSc1	sníh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
odpařováním	odpařování	k1gNnSc7	odpařování
z	z	k7c2	z
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
kondenzací	kondenzace	k1gFnSc7	kondenzace
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
déšť	déšť	k1gInSc1	déšť
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc1	chemikálie
a	a	k8xC	a
prachové	prachový	k2eAgFnPc1d1	prachová
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
k	k	k7c3	k
přímé	přímý	k2eAgFnSc3d1	přímá
konzumaci	konzumace	k1gFnSc3	konzumace
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
přípravě	příprava	k1gFnSc3	příprava
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
.	.	kIx.	.
</s>
