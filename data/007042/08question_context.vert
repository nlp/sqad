<s>
Destilovaná	destilovaný	k2eAgFnSc1d1
voda	voda	k1gFnSc1
je	být	k5eAaImIp3nS
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1
byla	být	k5eAaImAgFnS
jednou	jednou	k6eAd1
nebo	nebo	k8xC
vícekrát	vícekrát	k6eAd1
destilována	destilován	k2eAgFnSc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
byla	být	k5eAaImAgFnS
díky	díky	k7c3
změně	změna	k1gFnSc3
skupenství	skupenství	k1gNnSc2
na	na	k7c4
vodní	vodní	k2eAgFnSc4d1
páru	pára	k1gFnSc4
zbavena	zbavit	k5eAaPmNgFnS
rozpuštěných	rozpuštěný	k2eAgFnPc2d1
minerálních	minerální	k2eAgFnPc2d1
látek	látka	k1gFnPc2
a	a	k8xC
následně	následně	k6eAd1
ochlazením	ochlazení	k1gNnSc7
znovu	znovu	k6eAd1
zkapalněna	zkapalněn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>