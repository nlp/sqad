<s>
Isaac	Isaac	k1gInSc1	Isaac
Newton	newton	k1gInSc1	newton
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1643	[number]	k4	1643
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
tehdy	tehdy	k6eAd1	tehdy
užívaného	užívaný	k2eAgInSc2d1	užívaný
Juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1642	[number]	k4	1642
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Woolsthorpu	Woolsthorp	k1gInSc6	Woolsthorp
poblíž	poblíž	k7c2	poblíž
Granthamu	Grantham	k1gInSc2	Grantham
v	v	k7c6	v
Lincolnshire	Lincolnshir	k1gInSc5	Lincolnshir
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
