<s>
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
řeka	řeka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Morava	Morava	k1gFnSc1
Řeka	Řek	k1gMnSc2
u	u	k7c2
bzeneckého	bzenecký	k2eAgInSc2d1
přívozu	přívoz	k1gInSc2
<g/>
.	.	kIx.
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Délka	délka	k1gFnSc1
toku	tok	k1gInSc2
</s>
<s>
354	#num#	k4
km	km	kA
Plocha	plocha	k1gFnSc1
povodí	povodit	k5eAaPmIp3nS
</s>
<s>
26	#num#	k4
658	#num#	k4
km²	km²	k?
Průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
</s>
<s>
120,0	120,0	k4
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Hydrologické	hydrologický	k2eAgNnSc1d1
pořadí	pořadí	k1gNnSc4
</s>
<s>
4-10-01-001	4-10-01-001	k4
Pramen	pramen	k1gInSc1
</s>
<s>
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
Králického	králický	k2eAgInSc2d1
Sněžníku	Sněžník	k1gInSc2
50	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
18,35	18,35	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
57,69	57,69	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
1	#num#	k4
370,67	370,67	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Ústí	ústí	k1gNnSc1
</s>
<s>
do	do	k7c2
Dunaje	Dunaj	k1gInSc2
48	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
25,64	25,64	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
33,17	33,17	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Protéká	protékat	k5eAaImIp3nS
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
(	(	kIx(
<g/>
Pardubický	pardubický	k2eAgMnSc1d1
<g/>
,	,	kIx,
Olomoucký	olomoucký	k2eAgMnSc1d1
<g/>
,	,	kIx,
Zlínský	zlínský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jihomoravský	jihomoravský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
(	(	kIx(
<g/>
Trnavský	trnavský	k2eAgInSc1d1
<g/>
,	,	kIx,
Bratislavský	bratislavský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
<g/>
Rakousko	Rakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc1
(	(	kIx(
<g/>
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
<g/>
)	)	kIx)
Úmoří	úmoří	k1gNnPc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnPc1
</s>
<s>
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
,	,	kIx,
Černé	Černé	k2eAgNnSc1d1
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
Dunaj	Dunaj	k1gInSc1
</s>
<s>
Geodata	Geodata	k1gFnSc1
OpenStreetMap	OpenStreetMap	k1gInSc1
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
March	March	k1gInSc1
<g/>
,	,	kIx,
maďarsky	maďarsky	k6eAd1
Morva	Morva	k1gFnSc1
<g/>
,	,	kIx,
polsky	polsky	k6eAd1
Morawa	Moraw	k2eAgFnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
levý	levý	k2eAgInSc4d1
přítok	přítok	k1gInSc4
Dunaje	Dunaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protéká	protékat	k5eAaImIp3nS
stejnojmenným	stejnojmenný	k2eAgNnSc7d1
územím	území	k1gNnSc7
v	v	k7c6
Česku	Česko	k1gNnSc6
až	až	k9
po	po	k7c6
hranici	hranice	k1gFnSc6
mezi	mezi	k7c7
Rakouskem	Rakousko	k1gNnSc7
a	a	k8xC
Slovenskem	Slovensko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedaleko	nedaleko	k7c2
pramene	pramen	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
krátký	krátký	k2eAgInSc4d1
úsek	úsek	k1gInSc4
historické	historický	k2eAgFnSc2d1
zemské	zemský	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
Moravy	Morava	k1gFnSc2
a	a	k8xC
Čech	Čechy	k1gFnPc2
a	a	k8xC
na	na	k7c6
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
část	část	k1gFnSc1
hranice	hranice	k1gFnSc1
mezi	mezi	k7c7
Českem	Česko	k1gNnSc7
a	a	k8xC
Slovenskem	Slovensko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délkou	délka	k1gFnSc7
toku	tok	k1gInSc2
a	a	k8xC
plochou	plocha	k1gFnSc7
povodí	povodí	k1gNnSc2
je	být	k5eAaImIp3nS
po	po	k7c6
Vltavě	Vltava	k1gFnSc6
a	a	k8xC
Labi	Labe	k1gNnSc6
třetí	třetí	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
řekou	řeka	k1gFnSc7
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
délka	délka	k1gFnSc1
je	být	k5eAaImIp3nS
354	#num#	k4
km	km	kA
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
284	#num#	k4
km	km	kA
(	(	kIx(
<g/>
80	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
protéká	protékat	k5eAaImIp3nS
českým	český	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Povodí	povodí	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
26	#num#	k4
658	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
20	#num#	k4
692,4	692,4	k4
km²	km²	k?
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
tj.	tj.	kA
26	#num#	k4
%	%	kIx~
státního	státní	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdelším	dlouhý	k2eAgInSc7d3
tokem	tok	k1gInSc7
na	na	k7c6
moravském	moravský	k2eAgNnSc6d1
povodí	povodí	k1gNnSc6
není	být	k5eNaImIp3nS
ovšem	ovšem	k9
Morava	Morava	k1gFnSc1
jako	jako	k8xC,k8xS
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
systém	systém	k1gInSc1
Morava-Dyje-Rakouská	Morava-Dyje-Rakouský	k2eAgFnSc1d1
Dyje-Thauabach	Dyje-Thauabach	k1gInSc1
<g/>
/	/	kIx~
<g/>
Thauaský	Thauaský	k2eAgInSc1d1
potok	potok	k1gInSc1
(	(	kIx(
<g/>
pramen	pramen	k1gInSc1
<g/>
:	:	kIx,
Allentsteig-Edelbach	Allentsteig-Edelbach	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dosahující	dosahující	k2eAgFnSc2d1
délky	délka	k1gFnSc2
380	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Základ	základ	k1gInSc1
jména	jméno	k1gNnSc2
Morava	Morava	k1gFnSc1
patří	patřit	k5eAaImIp3nP
mezi	mezi	k7c4
nejstarší	starý	k2eAgInPc4d3
staroevropské	staroevropský	k2eAgInPc4d1
názvy	název	k1gInPc4
vodstev	vodstvo	k1gNnPc2
s	s	k7c7
původním	původní	k2eAgInSc7d1
významem	význam	k1gInSc7
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
močál	močál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řeku	řeka	k1gFnSc4
pojmenovalo	pojmenovat	k5eAaPmAgNnS
již	již	k6eAd1
předkeltské	předkeltský	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
jejím	její	k3xOp3gInSc6
dolním	dolní	k2eAgInSc6d1
toku	tok	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
názvu	název	k1gInSc2
řeky	řeka	k1gFnSc2
byla	být	k5eAaImAgFnS
pojmenována	pojmenován	k2eAgFnSc1d1
historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc4
hydrologickou	hydrologický	k2eAgFnSc4d1
osu	osa	k1gFnSc4
řeka	řek	k1gMnSc2
tvoří	tvořit	k5eAaImIp3nS
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
a	a	k8xC
rakouské	rakouský	k2eAgNnSc1d1
území	území	k1gNnSc1
Moravské	moravský	k2eAgFnSc2d1
pole	pole	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Průběh	průběh	k1gInSc1
toku	tok	k1gInSc2
</s>
<s>
Pramen	pramen	k1gInSc1
Moravy	Morava	k1gFnSc2
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
Králického	králický	k2eAgInSc2d1
Sněžníku	Sněžník	k1gInSc2
</s>
<s>
Horní	horní	k2eAgInSc1d1
tok	tok	k1gInSc1
</s>
<s>
Morava	Morava	k1gFnSc1
pramení	pramenit	k5eAaImIp3nS
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
Králického	králický	k2eAgInSc2d1
Sněžníku	Sněžník	k1gInSc2
na	na	k7c6
území	území	k1gNnSc6
obce	obec	k1gFnSc2
Dolní	dolní	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
1380	#num#	k4
m	m	kA
v	v	k7c6
upravené	upravený	k2eAgFnSc6d1
studánce	studánka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hluboké	Hluboká	k1gFnSc2
údolí	údolí	k1gNnSc2
odděluje	oddělovat	k5eAaImIp3nS
východní	východní	k2eAgInSc4d1
a	a	k8xC
západní	západní	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
pohoří	pohoří	k1gNnSc2
Králického	králický	k2eAgInSc2d1
Sněžníku	Sněžník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jižním	jižní	k2eAgNnSc6d1
úbočí	úbočí	k1gNnSc6
Králického	králický	k2eAgInSc2d1
Sněžníku	Sněžník	k1gInSc2
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
strmý	strmý	k2eAgInSc1d1
žleb	žleb	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
přijímá	přijímat	k5eAaImIp3nS
několik	několik	k4yIc4
potoků	potok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
zde	zde	k6eAd1
charakter	charakter	k1gInSc4
rychle	rychle	k6eAd1
mohutnícího	mohutnící	k2eAgInSc2d1
horského	horský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
níže	nízce	k6eAd2
pak	pak	k6eAd1
říčky	říčka	k1gFnSc2
s	s	k7c7
rychlou	rychlý	k2eAgFnSc7d1
čistou	čistý	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Hanušovické	hanušovický	k2eAgFnSc6d1
vrchovině	vrchovina	k1gFnSc6
přibírá	přibírat	k5eAaImIp3nS
tři	tři	k4xCgInPc4
větší	veliký	k2eAgInPc4d2
toky	tok	k1gInPc4
<g/>
:	:	kIx,
Krupá	Krupý	k2eAgFnSc1d1
<g/>
,	,	kIx,
Branná	branný	k2eAgFnSc1d1
a	a	k8xC
Desná	Desný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
protéká	protékat	k5eAaImIp3nS
Mohelnickou	mohelnický	k2eAgFnSc7d1
brázdou	brázda	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
u	u	k7c2
Zábřehu	Zábřeh	k1gInSc2
vlévá	vlévat	k5eAaImIp3nS
Moravská	moravský	k2eAgFnSc1d1
Sázava	Sázava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Hornomoravský	hornomoravský	k2eAgInSc1d1
úval	úval	k1gInSc1
</s>
<s>
Poté	poté	k6eAd1
Morava	Morava	k1gFnSc1
vstupuje	vstupovat	k5eAaImIp3nS
do	do	k7c2
širokého	široký	k2eAgInSc2d1
Hornomoravského	hornomoravský	k2eAgInSc2d1
úvalu	úval	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
přichází	přicházet	k5eAaImIp3nS
první	první	k4xOgInSc4
meandrující	meandrující	k2eAgInSc4d1
úsek	úsek	k1gInSc4
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
úseku	úsek	k1gInSc6
se	se	k3xPyFc4
do	do	k7c2
Moravy	Morava	k1gFnSc2
vlévá	vlévat	k5eAaImIp3nS
Třebůvka	Třebůvka	k1gFnSc1
<g/>
,	,	kIx,
Oskava	Oskava	k1gFnSc1
a	a	k8xC
Trusovický	Trusovický	k2eAgInSc1d1
potok	potok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
dále	daleko	k6eAd2
protéká	protékat	k5eAaImIp3nS
největším	veliký	k2eAgNnSc7d3
a	a	k8xC
metropolitním	metropolitní	k2eAgNnSc7d1
městem	město	k1gNnSc7
na	na	k7c6
Hané	Haná	k1gFnSc6
Olomoucí	Olomouc	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
od	od	k7c2
Tážal	Tážal	k1gMnSc1
po	po	k7c4
Tovačov	Tovačov	k1gInSc4
se	se	k3xPyFc4
po	po	k7c6
levé	levý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
hlavního	hlavní	k2eAgInSc2d1
toku	tok	k1gInSc2
odděluje	oddělovat	k5eAaImIp3nS
drobné	drobný	k2eAgNnSc4d1
odlehčovací	odlehčovací	k2eAgNnSc4d1
rameno	rameno	k1gNnSc4
<g/>
,	,	kIx,
zvané	zvaný	k2eAgNnSc4d1
Morávka	Morávek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
Troubkami	Troubka	k1gFnPc7
a	a	k8xC
Tovačovem	Tovačov	k1gInSc7
se	se	k3xPyFc4
do	do	k7c2
Moravy	Morava	k1gFnSc2
vlévá	vlévat	k5eAaImIp3nS
největší	veliký	k2eAgInSc4d3
levý	levý	k2eAgInSc4d1
přítok	přítok	k1gInSc4
Bečva	Bečva	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
odvádí	odvádět	k5eAaImIp3nS
vody	voda	k1gFnPc4
z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
části	část	k1gFnSc2
Moravskoslezských	moravskoslezský	k2eAgFnPc2d1
Beskyd	Beskydy	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
zprava	zprava	k6eAd1
Blata	Blata	k1gNnPc1
a	a	k8xC
u	u	k7c2
Kojetína	Kojetín	k1gInSc2
Valová	Valová	k1gFnSc1
<g/>
,	,	kIx,
níže	nízce	k6eAd2
Haná	Haná	k1gFnSc1
a	a	k8xC
zleva	zleva	k6eAd1
Moštěnka	Moštěnka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Dolnomoravský	dolnomoravský	k2eAgInSc1d1
úval	úval	k1gInSc1
</s>
<s>
Mezi	mezi	k7c7
Kroměříží	Kroměříž	k1gFnSc7
a	a	k8xC
Otrokovicemi	Otrokovice	k1gFnPc7
protéká	protékat	k5eAaImIp3nS
Morava	Morava	k1gFnSc1
průlomem	průlom	k1gInSc7
skrz	skrz	k6eAd1
Vnější	vnější	k2eAgInPc4d1
Západní	západní	k2eAgInPc4d1
Karpaty	Karpaty	k1gInPc4
(	(	kIx(
<g/>
mezi	mezi	k7c7
Chřiby	Chřiby	k1gInPc7
a	a	k8xC
Vizovickou	vizovický	k2eAgFnSc7d1
vrchovinou	vrchovina	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zleva	zleva	k6eAd1
přijímá	přijímat	k5eAaImIp3nS
Dřevnici	Dřevnice	k1gFnSc4
a	a	k8xC
vstupuje	vstupovat	k5eAaImIp3nS
do	do	k7c2
Dolnomoravského	dolnomoravský	k2eAgInSc2d1
úvalu	úval	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
osou	osa	k1gFnSc7
Moravského	moravský	k2eAgNnSc2d1
Slovácka	Slovácko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Otrokovic	Otrokovice	k1gFnPc2
po	po	k7c6
Rohatec	rohatec	k1gMnSc1
je	být	k5eAaImIp3nS
podél	podél	k7c2
Moravy	Morava	k1gFnSc2
vybudován	vybudován	k2eAgInSc4d1
Baťův	Baťův	k2eAgInSc4d1
kanál	kanál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
Bzencem	Bzenec	k1gInSc7
Přívozem	přívoz	k1gInSc7
a	a	k8xC
Rohatcem	rohatec	k1gMnSc7
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
další	další	k2eAgInSc4d1
meandrující	meandrující	k2eAgInSc4d1
úsek	úsek	k1gInSc4
Osypané	osypaný	k2eAgInPc4d1
břehy	břeh	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c6
území	území	k1gNnSc6
zvaném	zvaný	k2eAgNnSc6d1
Moravská	moravský	k2eAgFnSc1d1
Sahara	Sahara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
obce	obec	k1gFnSc2
Rohatec	rohatec	k1gMnSc1
začíná	začínat	k5eAaImIp3nS
tvořit	tvořit	k5eAaImF
společnou	společný	k2eAgFnSc4d1
česko-slovenskou	česko-slovenský	k2eAgFnSc4d1
státní	státní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
tvoří	tvořit	k5eAaImIp3nS
až	až	k9
k	k	k7c3
nejjižnějšímu	jižní	k2eAgInSc3d3
bodu	bod	k1gInSc3
katastrálního	katastrální	k2eAgNnSc2d1
území	území	k1gNnSc2
města	město	k1gNnSc2
Lanžhot	Lanžhota	k1gFnPc2
a	a	k8xC
celé	celý	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
země	zem	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
přijímá	přijímat	k5eAaImIp3nS
zprava	zprava	k6eAd1
svůj	svůj	k3xOyFgInSc4
vůbec	vůbec	k9
nejdelší	dlouhý	k2eAgInSc4d3
přítok	přítok	k1gInSc4
Dyji	Dyje	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současný	současný	k2eAgInSc1d1
průběh	průběh	k1gInSc1
tohoto	tento	k3xDgInSc2
hraničního	hraniční	k2eAgInSc2d1
úseku	úsek	k1gInSc2
má	mít	k5eAaImIp3nS
Morava	Morava	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1977	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
dokončeno	dokončit	k5eAaPmNgNnS
jeho	jeho	k3xOp3gNnSc1
narovnání	narovnání	k1gNnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
značné	značný	k2eAgNnSc1d1
prohloubení	prohloubení	k1gNnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
provedené	provedený	k2eAgInPc1d1
v	v	k7c6
letech	let	k1gInPc6
1969	#num#	k4
<g/>
–	–	k?
<g/>
1977	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotná	samotný	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
pak	pak	k6eAd1
ještě	ještě	k9
několik	několik	k4yIc4
desítek	desítka	k1gFnPc2
let	léto	k1gNnPc2
zachovávala	zachovávat	k5eAaImAgFnS
starou	starý	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
a	a	k8xC
přecházela	přecházet	k5eAaImAgFnS
z	z	k7c2
jednoho	jeden	k4xCgInSc2
břehu	břeh	k1gInSc2
Moravy	Morava	k1gFnSc2
na	na	k7c4
druhý	druhý	k4xOgInSc4
<g/>
,	,	kIx,
než	než	k8xS
byla	být	k5eAaImAgNnP
narovnána	narovnat	k5eAaPmNgNnP
podle	podle	k7c2
osy	osa	k1gFnSc2
toku	tok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1975	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ještě	ještě	k6eAd1
k	k	k7c3
úpravě	úprava	k1gFnSc3
soutoku	soutok	k1gInSc2
Moravy	Morava	k1gFnSc2
a	a	k8xC
Dyje	Dyje	k1gFnSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
pak	pak	k6eAd1
dále	daleko	k6eAd2
teče	téct	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
jižním	jižní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
až	až	k9
po	po	k7c4
soutok	soutok	k1gInSc4
s	s	k7c7
Dunajem	Dunaj	k1gInSc7
u	u	k7c2
Bratislavy-Devína	Bratislavy-Devín	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
rakousko-slovenskou	rakousko-slovenský	k2eAgFnSc4d1
státní	státní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
slovenského	slovenský	k2eAgInSc2d1
levého	levý	k2eAgInSc2d1
břehu	břeh	k1gInSc2
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
vlévají	vlévat	k5eAaImIp3nP
Myjava	Myjava	k1gFnSc1
<g/>
,	,	kIx,
Rudava	Rudava	k1gFnSc1
a	a	k8xC
Malina	Malina	k1gMnSc1
<g/>
,	,	kIx,
z	z	k7c2
rakouského	rakouský	k2eAgInSc2d1
pravého	pravý	k2eAgInSc2d1
břehu	břeh	k1gInSc2
Zaya	Zay	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sídla	sídlo	k1gNnPc1
</s>
<s>
Morava	Morava	k1gFnSc1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úsek	úsek	k1gInSc1
na	na	k7c4
fotografii	fotografia	k1gFnSc4
zobrazuje	zobrazovat	k5eAaImIp3nS
nejopevněnější	opevněný	k2eAgInSc1d3
a	a	k8xC
nejužší	úzký	k2eAgInSc1d3
profil	profil	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
Moravy	Morava	k1gFnSc2
protéká	protékat	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
řadou	řada	k1gFnSc7
měst	město	k1gNnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
na	na	k7c6
středním	střední	k2eAgInSc6d1
toku	tok	k1gInSc6
<g/>
:	:	kIx,
Litovel	Litovel	k1gFnSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Kojetín	Kojetín	k1gInSc1
<g/>
,	,	kIx,
Kroměříž	Kroměříž	k1gFnSc1
<g/>
,	,	kIx,
Otrokovice	Otrokovice	k1gFnPc1
<g/>
,	,	kIx,
Napajedla	Napajedla	k1gNnPc1
<g/>
,	,	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Uherský	uherský	k2eAgInSc1d1
Ostroh	Ostroh	k1gInSc1
<g/>
,	,	kIx,
Veselí	veselí	k1gNnSc1
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
<g/>
,	,	kIx,
Hodonín	Hodonín	k1gInSc1
a	a	k8xC
posledním	poslední	k2eAgNnSc7d1
významnějším	významný	k2eAgNnSc7d2
sídlem	sídlo	k1gNnSc7
na	na	k7c6
toku	tok	k1gInSc6
je	být	k5eAaImIp3nS
rakouský	rakouský	k2eAgInSc1d1
Marchegg	Marchegg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Větší	veliký	k2eAgInPc1d2
přítoky	přítok	k1gInPc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Krupá	Krupat	k5eAaPmIp3nS,k5eAaImIp3nS
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Branná	branný	k2eAgFnSc1d1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Desná	Desná	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1
Sázava	Sázava	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Mírovka	Mírovka	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Rohelnice	Rohelnice	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Třebůvka	Třebůvka	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Oskava	Oskava	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Trusovický	Trusovický	k2eAgInSc1d1
potok	potok	k1gInSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Bystřice	Bystřice	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Týnečka	Týnečka	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Bečva	Bečva	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Blata	Blata	k1gNnPc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Valová	Valová	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Haná	Haná	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Moštěnka	Moštěnka	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Kotojedka	Kotojedka	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Rusava	Rusava	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Dřevnice	Dřevnice	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Březnice	Březnice	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Salaška	salaška	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Olšava	Olšava	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Dlouhá	dlouhý	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Svodnice	svodnice	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Syrovinka	syrovinka	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Velička	Velička	k1gMnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Myjava	Myjava	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Dyje	Dyje	k1gFnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Zaya	Zaya	k1gMnSc1
(	(	kIx(
<g/>
P	P	kA
<g/>
)	)	kIx)
</s>
<s>
Rudava	Rudava	k1gFnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Malina	Malina	k1gMnSc1
(	(	kIx(
<g/>
L	L	kA
<g/>
)	)	kIx)
</s>
<s>
Směrem	směr	k1gInSc7
od	od	k7c2
Uherského	uherský	k2eAgInSc2d1
Ostrohu	Ostroh	k1gInSc2
do	do	k7c2
Vnorov	Vnorovo	k1gNnPc2
bylo	být	k5eAaImAgNnS
vybudováno	vybudovat	k5eAaPmNgNnS
odlehčovací	odlehčovací	k2eAgNnSc1d1
rameno	rameno	k1gNnSc1
Nová	Nová	k1gFnSc1
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Moravy	Morava	k1gFnSc2
se	se	k3xPyFc4
u	u	k7c2
Bzence-Přívoz	Bzence-Přívoza	k1gFnPc2
vlévá	vlévat	k5eAaImIp3nS
Syrovinka	syrovinka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Strážnice	Strážnice	k1gFnSc2
Velička	Velička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vodní	vodní	k2eAgInSc1d1
režim	režim	k1gInSc1
</s>
<s>
Jez	jez	k1gInSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
poblíž	poblíž	k6eAd1
Otrokovic	Otrokovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ústí	ústí	k1gNnSc1
Moravy	Morava	k1gFnSc2
do	do	k7c2
Dunaje	Dunaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
průtok	průtok	k1gInSc1
má	mít	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
na	na	k7c6
jaře	jaro	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
její	její	k3xOp3gFnSc2
hladina	hladina	k1gFnSc1
klesá	klesat	k5eAaImIp3nS
a	a	k8xC
opět	opět	k6eAd1
stoupá	stoupat	k5eAaImIp3nS
na	na	k7c4
podzim	podzim	k1gInSc4
díky	díky	k7c3
dešťovým	dešťový	k2eAgFnPc3d1
srážkám	srážka	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
Hlásné	hlásný	k2eAgInPc4d1
profily	profil	k1gInPc4
<g/>
:	:	kIx,
</s>
<s>
místo	místo	k7c2
</s>
<s>
říční	říční	k2eAgInSc1d1
km	km	kA
</s>
<s>
plocha	plocha	k1gFnSc1
povodí	povodí	k1gNnSc2
<g/>
[	[	kIx(
<g/>
km²	km²	k?
<g/>
]	]	kIx)
</s>
<s>
průměrný	průměrný	k2eAgInSc1d1
průtok	průtok	k1gInSc1
(	(	kIx(
<g/>
Qa	Qa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
]	]	kIx)
</s>
<s>
stoletá	stoletý	k2eAgFnSc1d1
voda	voda	k1gFnSc1
(	(	kIx(
<g/>
Q	Q	kA
<g/>
100	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
m³	m³	k?
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
]	]	kIx)
</s>
<s>
Vlaské	vlaský	k2eAgNnSc1d1
(	(	kIx(
<g/>
Malá	malý	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
331,20	331,20	k4
</s>
<s>
96,55	96,55	k4
</s>
<s>
1,88	1,88	k4
</s>
<s>
72,5	72,5	k4
</s>
<s>
Raškov	Raškov	k1gInSc1
(	(	kIx(
<g/>
Bohdíkov	Bohdíkov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
322,80	322,80	k4
</s>
<s>
349,79	349,79	k4
</s>
<s>
6,29	6,29	k4
</s>
<s>
189,0	189,0	k4
</s>
<s>
Moravičany	Moravičan	k1gMnPc4
</s>
<s>
272,80	272,80	k4
</s>
<s>
1561,19	1561,19	k4
</s>
<s>
17,10	17,10	k4
</s>
<s>
394,0	394,0	k4
</s>
<s>
Hynkov	Hynkov	k1gInSc1
(	(	kIx(
<g/>
Příkazy	příkaz	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
251,14	251,14	k4
</s>
<s>
2250,46	2250,46	k4
</s>
<s>
20,80	20,80	k4
</s>
<s>
483,0	483,0	k4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
-	-	kIx~
Nové	Nové	k2eAgInPc1d1
Sady	sad	k1gInPc1
</s>
<s>
232,30	232,30	k4
</s>
<s>
3323,59	3323,59	k4
</s>
<s>
26,40	26,40	k4
</s>
<s>
551,0	551,0	k4
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1
</s>
<s>
193,70	193,70	k4
</s>
<s>
7030,31	7030,31	k4
</s>
<s>
51,20	51,20	k4
</s>
<s>
860,0	860,0	k4
</s>
<s>
Spytihněv	Spytihnět	k5eAaPmDgInS
</s>
<s>
169,20	169,20	k4
</s>
<s>
7891,12	7891,12	k4
</s>
<s>
55,60	55,60	k4
</s>
<s>
817,0	817,0	k4
</s>
<s>
Strážnice	Strážnice	k1gFnSc1
</s>
<s>
133,50	133,50	k4
</s>
<s>
9145,84	9145,84	k4
</s>
<s>
59,30	59,30	k4
</s>
<s>
790,0	790,0	k4
</s>
<s>
Lanžhot	Lanžhot	k1gMnSc1
</s>
<s>
79,00	79,00	k4
</s>
<s>
9721,81	9721,81	k4
</s>
<s>
61,10	61,10	k4
</s>
<s>
791,0	791,0	k4
</s>
<s>
ústí	ústit	k5eAaImIp3nS
</s>
<s>
0,00	0,00	k4
</s>
<s>
26658,00	26658,00	k4
</s>
<s>
120,00	120,00	k4
</s>
<s>
Povodně	povodeň	k1gFnPc1
1997	#num#	k4
</s>
<s>
Povodně	povodeň	k1gFnPc1
v	v	k7c6
červenci	červenec	k1gInSc6
1997	#num#	k4
byly	být	k5eAaImAgFnP
do	do	k7c2
povodní	povodeň	k1gFnPc2
2002	#num#	k4
největšími	veliký	k2eAgFnPc7d3
povodněmi	povodeň	k1gFnPc7
v	v	k7c6
historii	historie	k1gFnSc6
samostatné	samostatný	k2eAgFnSc2d1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyžádaly	vyžádat	k5eAaPmAgInP
si	se	k3xPyFc3
49	#num#	k4
lidských	lidský	k2eAgMnPc2d1
životů	život	k1gInPc2
a	a	k8xC
škody	škoda	k1gFnSc2
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
63	#num#	k4
miliard	miliarda	k4xCgFnPc2
Kč	Kč	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povodeň	povodeň	k1gFnSc1
nastala	nastat	k5eAaPmAgFnS
vlivem	vlivem	k7c2
abnormálních	abnormální	k2eAgFnPc2d1
srážek	srážka	k1gFnPc2
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
a	a	k8xC
Beskydech	Beskyd	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
setkaly	setkat	k5eAaPmAgFnP
povodňové	povodňový	k2eAgFnPc1d1
vlny	vlna	k1gFnPc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
Bečvě	Bečva	k1gFnSc6
i	i	k8xC
dalších	další	k2eAgInPc6d1
přítocích	přítok	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
evakuováno	evakuovat	k5eAaBmNgNnS
přes	přes	k7c4
250	#num#	k4
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
zavlažování	zavlažování	k1gNnSc3
a	a	k8xC
výrobě	výroba	k1gFnSc3
vodní	vodní	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejvíce	nejvíce	k6eAd1,k6eAd3
regulovaných	regulovaný	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
,	,	kIx,
přestože	přestože	k8xS
na	na	k7c6
ní	on	k3xPp3gFnSc6
není	být	k5eNaImIp3nS
žádná	žádný	k3yNgFnSc1
přehrada	přehrada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
meandrující	meandrující	k2eAgInSc4d1
úseky	úsek	k1gInPc4
kromě	kromě	k7c2
dvou	dva	k4xCgInPc2
byly	být	k5eAaImAgInP
upraveny	upravit	k5eAaPmNgInP
a	a	k8xC
zkráceny	zkrátit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
prevence	prevence	k1gFnSc1
povodní	povodeň	k1gFnPc2
slouží	sloužit	k5eAaImIp3nS
četné	četný	k2eAgFnPc4d1
hráze	hráz	k1gFnPc4
a	a	k8xC
odlehčovací	odlehčovací	k2eAgNnPc4d1
ramena	rameno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Po	po	k7c6
řece	řeka	k1gFnSc6
Moravě	Morava	k1gFnSc6
bylo	být	k5eAaImAgNnS
taktéž	taktéž	k?
pojmenováno	pojmenován	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
na	na	k7c6
povrchu	povrch	k1gInSc6
Marsu	Mars	k1gInSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Morava	Morava	k1gFnSc1
Valles	Vallesa	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
kvadrantu	kvadrant	k1gInSc6
Margaritifer	Margaritifer	k1gMnSc1
Sinus	sinus	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Předpovědi	předpověď	k1gFnSc2
průtoků	průtok	k1gInPc2
pro	pro	k7c4
soutokovou	soutokový	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
Dyje	Dyje	k1gFnSc2
<g/>
.	.	kIx.
old	old	k?
<g/>
.	.	kIx.
<g/>
chmi	chm	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠRÁMEK	Šrámek	k1gMnSc1
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
,	,	kIx,
MAJTÁN	MAJTÁN	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
;	;	kIx,
Lutterer	Lutterer	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměpisná	zeměpisný	k2eAgFnSc1d1
jména	jméno	k1gNnPc4
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Str	str	kA
<g/>
.	.	kIx.
202	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
www.pmo.cz	www.pmo.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.lesycr.cz	www.lesycr.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
http://www.chmu.cz/meteo/CBKS/sbornik02/klimanek.pdf%5B%5D	http://www.chmu.cz/meteo/CBKS/sbornik02/klimanek.pdf%5B%5D	k4
<g/>
↑	↑	k?
Morava	Morava	k1gFnSc1
Valles	Valles	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Morava	Morava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
řeka	řeka	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
řeka	řeka	k1gFnSc1
<g/>
)	)	kIx)
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
ČHMÚ	ČHMÚ	kA
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Magistrát	magistrát	k1gInSc1
města	město	k1gNnSc2
Olomouce	Olomouc	k1gFnSc2
–	–	k?
Hlásné	hlásný	k2eAgInPc4d1
profily	profil	k1gInPc4
povodňové	povodňový	k2eAgFnSc2d1
služby	služba	k1gFnSc2
</s>
<s>
VÚV	VÚV	kA
T.G.	T.G.	k1gMnSc1
<g/>
Masaryka	Masaryk	k1gMnSc2
–	–	k?
Oddělení	oddělení	k1gNnSc1
GIS	gis	k1gNnSc2
–	–	k?
Charakteristiky	charakteristika	k1gFnSc2
toků	tok	k1gInPc2
a	a	k8xC
povodí	povodí	k1gNnSc2
ČR	ČR	kA
</s>
<s>
Vodácká	vodácký	k2eAgNnPc1d1
putování	putování	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Lukášem	Lukáš	k1gMnSc7
Pollertem	Pollert	k1gMnSc7
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
[	[	kIx(
<g/>
dokumentární	dokumentární	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režie	režie	k1gFnSc1
Ludvík	Ludvík	k1gMnSc1
Klega	Kleg	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
–	–	k?
video	video	k1gNnSc1
on-line	on-lin	k1gInSc5
v	v	k7c6
archivu	archiv	k1gInSc6
ČT	ČT	kA
</s>
<s>
Unie	unie	k1gFnSc1
pro	pro	k7c4
řeku	řeka	k1gFnSc4
Moravu	Morava	k1gFnSc4
</s>
<s>
Seznam	seznam	k1gInSc1
vodních	vodní	k2eAgFnPc2d1
elektráren	elektrárna	k1gFnPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
130078	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4037462-2	4037462-2	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
238346431	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
</s>
