<s desamb="1">
Její	její	k3xOp3gFnSc1
délka	délka	k1gFnSc1
je	být	k5eAaImIp3nS
354	#num#	k4
km	km	kA
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
284	#num#	k4
km	km	kA
(	(	kIx(
<g/>
80	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
protéká	protékat	k5eAaImIp3nS
českým	český	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Povodí	povodí	k1gNnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
26	#num#	k4
658	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
20	#num#	k4
692,4	692,4	k4
km²	km²	k?
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
tj.	tj.	kA
26	#num#	k4
%	%	kIx~
státního	státní	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>