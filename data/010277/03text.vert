<p>
<s>
Ekvatoriální	ekvatoriální	k2eAgNnSc1d1	ekvatoriální
podnebí	podnebí	k1gNnSc1	podnebí
(	(	kIx(	(
<g/>
rovníkové	rovníkový	k2eAgNnSc1d1	rovníkové
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejpříznivější	příznivý	k2eAgNnSc4d3	nejpříznivější
podnebí	podnebí	k1gNnSc4	podnebí
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
podnebí	podnebí	k1gNnSc1	podnebí
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
rovníku	rovník	k1gInSc2	rovník
a	a	k8xC	a
počasí	počasí	k1gNnSc1	počasí
je	být	k5eAaImIp3nS	být
celoročně	celoročně	k6eAd1	celoročně
prakticky	prakticky	k6eAd1	prakticky
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
také	také	k9	také
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
skoro	skoro	k6eAd1	skoro
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
10	[number]	k4	10
až	až	k9	až
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
má	mít	k5eAaImIp3nS	mít
nejkratší	krátký	k2eAgInSc1d3	nejkratší
den	den	k1gInSc1	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
a	a	k8xC	a
13	[number]	k4	13
až	až	k9	až
14	[number]	k4	14
ten	ten	k3xDgInSc1	ten
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Teploty	teplota	k1gFnPc1	teplota
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
teploty	teplota	k1gFnPc1	teplota
jsou	být	k5eAaImIp3nP	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nejchladnějších	chladný	k2eAgNnPc6d3	nejchladnější
ránech	ráno	k1gNnPc6	ráno
mohou	moct	k5eAaImIp3nP	moct
teploty	teplota	k1gFnPc1	teplota
klesat	klesat	k5eAaImF	klesat
i	i	k8xC	i
k	k	k7c3	k
15	[number]	k4	15
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
spíš	spíš	k9	spíš
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
den	den	k1gInSc4	den
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
teploty	teplota	k1gFnPc1	teplota
mezi	mezi	k7c7	mezi
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
-	-	kIx~	-
40	[number]	k4	40
°	°	k?	°
<g/>
C.	C.	kA	C.
Rozdíly	rozdíl	k1gInPc4	rozdíl
průměrné	průměrný	k2eAgFnSc2d1	průměrná
měsíční	měsíční	k2eAgFnSc2d1	měsíční
teploty	teplota	k1gFnSc2	teplota
mezi	mezi	k7c7	mezi
nejteplejším	teplý	k2eAgInSc7d3	nejteplejší
a	a	k8xC	a
nejchladnějším	chladný	k2eAgInSc7d3	nejchladnější
měsícem	měsíc	k1gInSc7	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
jsou	být	k5eAaImIp3nP	být
maximálně	maximálně	k6eAd1	maximálně
2	[number]	k4	2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tu	ten	k3xDgFnSc4	ten
neexistují	existovat	k5eNaImIp3nP	existovat
žádná	žádný	k3yNgNnPc1	žádný
roční	roční	k2eAgNnPc1d1	roční
období	období	k1gNnPc1	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Srážky	srážka	k1gFnPc1	srážka
==	==	k?	==
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
minimálně	minimálně	k6eAd1	minimálně
60	[number]	k4	60
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
(	(	kIx(	(
<g/>
720	[number]	k4	720
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
a	a	k8xC	a
množství	množství	k1gNnSc1	množství
napadaných	napadaný	k2eAgFnPc2d1	napadaná
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
roka	rok	k1gInSc2	rok
různit	různit	k5eAaImF	různit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
je	být	k5eAaImIp3nS	být
deště	dešť	k1gInSc2	dešť
dostatek	dostatek	k1gInSc4	dostatek
pro	pro	k7c4	pro
dobrý	dobrý	k2eAgInSc4d1	dobrý
růst	růst	k1gInSc4	růst
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
srážek	srážka	k1gFnPc2	srážka
spadne	spadnout	k5eAaPmIp3nS	spadnout
při	při	k7c6	při
bouřkách	bouřka	k1gFnPc6	bouřka
nebo	nebo	k8xC	nebo
delších	dlouhý	k2eAgInPc6d2	delší
prudkých	prudký	k2eAgInPc6d1	prudký
deštích	dešť	k1gInPc6	dešť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
ekvatoriálním	ekvatoriální	k2eAgNnSc7d1	ekvatoriální
podnebím	podnebí	k1gNnSc7	podnebí
za	za	k7c4	za
rok	rok	k1gInSc4	rok
většinou	většinou	k6eAd1	většinou
napadá	napadat	k5eAaBmIp3nS	napadat
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
i	i	k8xC	i
místa	místo	k1gNnPc4	místo
kde	kde	k6eAd1	kde
napadá	napadat	k5eAaPmIp3nS	napadat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
000	[number]	k4	000
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ekvatoriální	ekvatoriální	k2eAgFnSc2d1	ekvatoriální
podnebí	podnebí	k1gNnSc4	podnebí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Ekvatoriální	ekvatoriální	k2eAgNnSc1d1	ekvatoriální
podnebí	podnebí	k1gNnSc1	podnebí
(	(	kIx(	(
<g/>
Googlem	Googl	k1gInSc7	Googl
uložená	uložený	k2eAgFnSc1d1	uložená
kopie	kopie	k1gFnSc1	kopie
webové	webový	k2eAgFnSc2d1	webová
stránky	stránka	k1gFnSc2	stránka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
před	před	k7c7	před
počítačovými	počítačový	k2eAgInPc7d1	počítačový
viry	vir	k1gInPc7	vir
<g/>
)	)	kIx)	)
</s>
</p>
