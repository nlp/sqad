<p>
<s>
Panamericana	Panamericana	k1gFnSc1	Panamericana
je	být	k5eAaImIp3nS	být
síť	síť	k1gFnSc4	síť
silnic	silnice	k1gFnPc2	silnice
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
000	[number]	k4	000
kilometrů	kilometr	k1gInPc2	kilometr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
87	[number]	k4	87
kilometrů	kilometr	k1gInPc2	kilometr
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
přerušení	přerušení	k1gNnSc2	přerušení
v	v	k7c6	v
deštném	deštný	k2eAgInSc6d1	deštný
pralese	prales	k1gInSc6	prales
v	v	k7c6	v
Panamě	Panama	k1gFnSc6	Panama
spojuje	spojovat	k5eAaImIp3nS	spojovat
všechny	všechen	k3xTgInPc1	všechen
pevninské	pevninský	k2eAgInPc1d1	pevninský
národy	národ	k1gInPc1	národ
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
obou	dva	k4xCgInPc6	dva
Amerik	Amerika	k1gFnPc2	Amerika
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
silniční	silniční	k2eAgInPc1d1	silniční
systémy	systém	k1gInPc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
dálniční	dálniční	k2eAgFnSc1d1	dálniční
síť	síť	k1gFnSc1	síť
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panamerický	panamerický	k2eAgInSc1d1	panamerický
silniční	silniční	k2eAgInSc1d1	silniční
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
kompletní	kompletní	k2eAgFnSc1d1	kompletní
a	a	k8xC	a
táhne	táhnout	k5eAaImIp3nS	táhnout
se	se	k3xPyFc4	se
od	od	k7c2	od
Fairbanks	Fairbanksa	k1gFnPc2	Fairbanksa
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
až	až	k9	až
k	k	k7c3	k
nejjižnějšímu	jižní	k2eAgInSc3d3	nejjižnější
konci	konec	k1gInSc3	konec
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
končiny	končina	k1gFnPc1	končina
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
závislé	závislý	k2eAgNnSc1d1	závislé
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
Puerto	Puerta	k1gFnSc5	Puerta
Montt	Montt	k1gMnSc1	Montt
a	a	k8xC	a
Quellón	Quellón	k1gMnSc1	Quellón
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
nebo	nebo	k8xC	nebo
Ushuaia	Ushuaia	k1gFnSc1	Ushuaia
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Panamericana	Panamericana	k1gFnSc1	Panamericana
není	být	k5eNaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
ustanovená	ustanovený	k2eAgFnSc1d1	ustanovená
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Panamericana	Panamericana	k1gFnSc1	Panamericana
prochází	procházet	k5eAaImIp3nS	procházet
mnoha	mnoho	k4c2	mnoho
rozdílnými	rozdílný	k2eAgFnPc7d1	rozdílná
klimatickými	klimatický	k2eAgFnPc7d1	klimatická
oblastmi	oblast	k1gFnPc7	oblast
a	a	k8xC	a
ekosystémy	ekosystém	k1gInPc7	ekosystém
od	od	k7c2	od
hustých	hustý	k2eAgFnPc2d1	hustá
džunglí	džungle	k1gFnPc2	džungle
až	až	k9	až
po	po	k7c4	po
vysokohorské	vysokohorský	k2eAgInPc4d1	vysokohorský
průsmyky	průsmyk	k1gInPc4	průsmyk
<g/>
.	.	kIx.	.
</s>
<s>
Panamericana	Panamericana	k1gFnSc1	Panamericana
prochází	procházet	k5eAaImIp3nS	procházet
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
daleko	daleko	k6eAd1	daleko
do	do	k7c2	do
jednoty	jednota	k1gFnSc2	jednota
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
úseky	úsek	k1gInPc1	úsek
jsou	být	k5eAaImIp3nP	být
průjezdné	průjezdný	k2eAgMnPc4d1	průjezdný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
období	období	k1gNnSc6	období
sucha	sucho	k1gNnSc2	sucho
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
průjezd	průjezd	k1gInSc1	průjezd
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
části	část	k1gFnPc4	část
Panamericany	Panamericana	k1gFnSc2	Panamericana
patří	patřit	k5eAaImIp3nS	patřit
Aljašská	aljašský	k2eAgFnSc1d1	aljašská
dálnice	dálnice	k1gFnSc1	dálnice
a	a	k8xC	a
Interamericana	Interamericana	k1gFnSc1	Interamericana
<g/>
,	,	kIx,	,
novější	nový	k2eAgInSc4d2	novější
úsek	úsek	k1gInSc4	úsek
mezi	mezi	k7c7	mezi
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Panamským	panamský	k2eAgInSc7d1	panamský
průplavem	průplav	k1gInSc7	průplav
(	(	kIx(	(
<g/>
známá	známá	k1gFnSc1	známá
mezi	mezi	k7c7	mezi
americkými	americký	k2eAgMnPc7d1	americký
turisty	turist	k1gMnPc7	turist
jedoucími	jedoucí	k2eAgMnPc7d1	jedoucí
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
popsal	popsat	k5eAaPmAgMnS	popsat
Jake	Jake	k1gFnSc4	Jake
Silverstein	Silverstein	k1gMnSc1	Silverstein
Panamericanu	Panamerican	k1gInSc2	Panamerican
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
systém	systém	k1gInSc1	systém
tak	tak	k6eAd1	tak
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
<g/>
,	,	kIx,	,
nekompletní	kompletní	k2eNgInSc1d1	nekompletní
a	a	k8xC	a
nepochopitelný	pochopitelný	k2eNgInSc1d1	nepochopitelný
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
tolik	tolik	k6eAd1	tolik
jako	jako	k9	jako
silnice	silnice	k1gFnSc1	silnice
<g/>
,	,	kIx,	,
než	než	k8xS	než
</s>
</p>
<p>
<s>
myšlenka	myšlenka	k1gFnSc1	myšlenka
Panamerikanismu	panamerikanismus	k1gInSc2	panamerikanismus
samotného	samotný	k2eAgInSc2d1	samotný
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Jake	Jake	k1gFnSc1	Jake
Silverstein	Silverstein	k1gInSc1	Silverstein
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
71	[number]	k4	71
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Souhrn	souhrn	k1gInSc1	souhrn
Panamerického	panamerický	k2eAgInSc2d1	panamerický
systému	systém	k1gInSc2	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Panamericana	Panamericana	k1gFnSc1	Panamericana
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
následujících	následující	k2eAgInPc2d1	následující
14	[number]	k4	14
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kanada	Kanada	k1gFnSc1	Kanada
(	(	kIx(	(
<g/>
neoficiálně	oficiálně	k6eNd1	oficiálně
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
USA	USA	kA	USA
(	(	kIx(	(
<g/>
neoficiálně	oficiálně	k6eNd1	oficiálně
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
</s>
</p>
<p>
<s>
Guatemala	Guatemala	k1gFnSc1	Guatemala
</s>
</p>
<p>
<s>
Salvador	Salvador	k1gMnSc1	Salvador
</s>
</p>
<p>
<s>
Honduras	Honduras	k1gInSc1	Honduras
</s>
</p>
<p>
<s>
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
</s>
</p>
<p>
<s>
Kostarika	Kostarika	k1gFnSc1	Kostarika
</s>
</p>
<p>
<s>
Panama	Panama	k1gFnSc1	Panama
</s>
</p>
<p>
<s>
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
</s>
</p>
<p>
<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
</s>
</p>
<p>
<s>
Peru	Peru	k1gNnSc1	Peru
</s>
</p>
<p>
<s>
Chile	Chile	k1gNnSc1	Chile
</s>
</p>
<p>
<s>
ArgentinaDůležité	ArgentinaDůležitý	k2eAgFnPc1d1	ArgentinaDůležitý
odbočky	odbočka	k1gFnPc1	odbočka
také	také	k9	také
vedou	vést	k5eAaImIp3nP	vést
do	do	k7c2	do
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
Paraguaye	Paraguay	k1gFnSc2	Paraguay
<g/>
,	,	kIx,	,
Uruguaye	Uruguay	k1gFnSc2	Uruguay
a	a	k8xC	a
</s>
</p>
<p>
<s>
Venezuely	Venezuela	k1gFnPc1	Venezuela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
turistické	turistický	k2eAgInPc4d1	turistický
účely	účel	k1gInPc4	účel
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
Panamericany	Panamericana	k1gFnSc2	Panamericana
severně	severně	k6eAd1	severně
od	od	k7c2	od
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
považována	považován	k2eAgFnSc1d1	považována
Aljašská	aljašský	k2eAgFnSc1d1	aljašská
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
dálnice	dálnice	k1gFnSc1	dálnice
vedoucí	vedoucí	k1gFnSc2	vedoucí
po	po	k7c6	po
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
až	až	k9	až
východně	východně	k6eAd1	východně
od	od	k7c2	od
San	San	k1gMnSc2	San
Diega	Dieg	k1gMnSc2	Dieg
(	(	kIx(	(
<g/>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
k	k	k7c3	k
Nogales	Nogales	k1gInSc4	Nogales
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgInSc4	první
Čech	Čechy	k1gFnPc2	Čechy
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
Čechem	Čech	k1gMnSc7	Čech
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
projel	projet	k5eAaPmAgMnS	projet
celou	celý	k2eAgFnSc4d1	celá
Panamericanu	Panamericana	k1gFnSc4	Panamericana
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Dostál	Dostál	k1gMnSc1	Dostál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ale	ale	k9	ale
vynechal	vynechat	k5eAaPmAgMnS	vynechat
úsek	úsek	k1gInSc4	úsek
v	v	k7c6	v
Dariénském	Dariénský	k2eAgInSc6d1	Dariénský
pralese	prales	k1gInSc6	prales
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
29	[number]	k4	29
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
z	z	k7c2	z
Anchorage	Anchorag	k1gFnSc2	Anchorag
<g/>
,	,	kIx,	,
do	do	k7c2	do
Ushuaiy	Ushuaia	k1gFnSc2	Ushuaia
dorazil	dorazit	k5eAaPmAgInS	dorazit
8	[number]	k4	8
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Darién	Darién	k1gInSc4	Darién
==	==	k?	==
</s>
</p>
<p>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
úsek	úsek	k1gInSc1	úsek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
chybí	chybět	k5eAaImIp3nS	chybět
ke	k	k7c3	k
kompletnímu	kompletní	k2eAgNnSc3d1	kompletní
dokončení	dokončení	k1gNnSc3	dokončení
Panamericany	Panamericana	k1gFnSc2	Panamericana
mezi	mezi	k7c7	mezi
Panamou	Panama	k1gFnSc7	Panama
a	a	k8xC	a
Kolumbií	Kolumbie	k1gFnSc7	Kolumbie
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Darién	Darién	k1gInSc1	Darién
(	(	kIx(	(
<g/>
Darién	Darién	k1gInSc1	Darién
Gap	Gap	k1gFnSc2	Gap
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
87	[number]	k4	87
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
úsek	úsek	k1gInSc4	úsek
procházející	procházející	k2eAgNnSc1d1	procházející
hustým	hustý	k2eAgInSc7d1	hustý
tropickým	tropický	k2eAgInSc7d1	tropický
pralesem	prales	k1gInSc7	prales
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
prales	prales	k1gInSc1	prales
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
překročen	překročen	k2eAgInSc1d1	překročen
dobrodruhy	dobrodruh	k1gMnPc7	dobrodruh
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
přejezd	přejezd	k1gInSc1	přejezd
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
-	-	kIx~	-
Chris	Chris	k1gInSc1	Chris
Bechard	Bechard	k1gInSc1	Bechard
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
motorce	motorka	k1gFnSc3	motorka
<g/>
,	,	kIx,	,
terénními	terénní	k2eAgNnPc7d1	terénní
vozidly	vozidlo	k1gNnPc7	vozidlo
i	i	k8xC	i
pěšky	pěšky	k6eAd1	pěšky
<g/>
,	,	kIx,	,
cestovatelé	cestovatel	k1gMnPc1	cestovatel
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
potýkat	potýkat	k5eAaImF	potýkat
s	s	k7c7	s
džunglí	džungle	k1gFnSc7	džungle
<g/>
,	,	kIx,	,
pohyblivými	pohyblivý	k2eAgInPc7d1	pohyblivý
písky	písek	k1gInPc7	písek
<g/>
,	,	kIx,	,
rozlehlými	rozlehlý	k2eAgFnPc7d1	rozlehlá
bažinami	bažina	k1gFnPc7	bažina
a	a	k8xC	a
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
ohrožováni	ohrožovat	k5eAaImNgMnP	ohrožovat
pronikáním	pronikání	k1gNnSc7	pronikání
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
skupin	skupina	k1gFnPc2	skupina
z	z	k7c2	z
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
.	.	kIx.	.
<g/>
Mnoho	mnoho	k6eAd1	mnoho
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
domorodců	domorodec	k1gMnPc2	domorodec
a	a	k8xC	a
vlád	vláda	k1gFnPc2	vláda
protestuje	protestovat	k5eAaBmIp3nS	protestovat
proti	proti	k7c3	proti
dokončení	dokončení	k1gNnSc3	dokončení
Dariénského	Dariénský	k2eAgInSc2d1	Dariénský
úseku	úsek	k1gInSc2	úsek
Panamericany	Panamericana	k1gFnSc2	Panamericana
<g/>
.	.	kIx.	.
</s>
<s>
Oponují	oponovat	k5eAaImIp3nP	oponovat
důvody	důvod	k1gInPc4	důvod
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
snaha	snaha	k1gFnSc1	snaha
zachránit	zachránit	k5eAaPmF	zachránit
prales	prales	k1gInSc4	prales
<g/>
,	,	kIx,	,
ochránit	ochránit	k5eAaPmF	ochránit
živobytí	živobytí	k1gNnPc4	živobytí
domorodých	domorodý	k2eAgMnPc2d1	domorodý
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
zabránit	zabránit	k5eAaPmF	zabránit
rozšíření	rozšíření	k1gNnSc4	rozšíření
slintavky	slintavka	k1gFnSc2	slintavka
a	a	k8xC	a
kulhavky	kulhavka	k1gFnSc2	kulhavka
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
prodlužování	prodlužování	k1gNnSc2	prodlužování
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
až	až	k9	až
do	do	k7c2	do
Yavizy	Yaviza	k1gFnSc2	Yaviza
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
vážné	vážný	k2eAgNnSc4d1	vážné
odlesňování	odlesňování	k1gNnSc4	odlesňování
podél	podél	k7c2	podél
silnice	silnice	k1gFnSc2	silnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bio-Pacifico	Bio-Pacifico	k6eAd1	Bio-Pacifico
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
vytvořit	vytvořit	k5eAaPmF	vytvořit
novou	nový	k2eAgFnSc4d1	nová
trajektovou	trajektový	k2eAgFnSc4d1	trajektová
linku	linka	k1gFnSc4	linka
z	z	k7c2	z
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
do	do	k7c2	do
nově	nově	k6eAd1	nově
zbudovaného	zbudovaný	k2eAgInSc2d1	zbudovaný
přístavu	přístav	k1gInSc2	přístav
v	v	k7c6	v
Panamě	Panama	k1gFnSc6	Panama
a	a	k8xC	a
prodloužit	prodloužit	k5eAaPmF	prodloužit
existující	existující	k2eAgFnSc4d1	existující
silnici	silnice	k1gFnSc4	silnice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
dokončilo	dokončit	k5eAaPmAgNnS	dokončit
Panamericanu	Panamericana	k1gFnSc4	Panamericana
bez	bez	k7c2	bez
porušení	porušení	k1gNnSc2	porušení
těchto	tento	k3xDgInPc2	tento
ekologických	ekologický	k2eAgInPc2d1	ekologický
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trajekt	trajekt	k1gInSc1	trajekt
by	by	kYmCp3nS	by
překonáním	překonání	k1gNnPc3	překonání
zálivu	záliv	k1gInSc6	záliv
Urabá	Urabý	k2eAgFnSc1d1	Urabá
z	z	k7c2	z
kolumbijského	kolumbijský	k2eAgMnSc2d1	kolumbijský
Turba	turba	k1gFnSc1	turba
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
panamského	panamský	k2eAgInSc2d1	panamský
přístavu	přístav	k1gInSc2	přístav
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
Carreto	Carret	k2eAgNnSc4d1	Carret
<g/>
)	)	kIx)	)
spojil	spojit	k5eAaPmAgMnS	spojit
oba	dva	k4xCgInPc4	dva
konce	konec	k1gInPc4	konec
silnice	silnice	k1gFnSc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Účinná	účinný	k2eAgFnSc1d1	účinná
oklika	oklika	k1gFnSc1	oklika
by	by	kYmCp3nS	by
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
způsobila	způsobit	k5eAaPmAgFnS	způsobit
ponížení	ponížení	k1gNnSc4	ponížení
stávající	stávající	k2eAgFnSc2d1	stávající
silnice	silnice	k1gFnSc2	silnice
do	do	k7c2	do
Yavizy	Yaviza	k1gFnSc2	Yaviza
na	na	k7c4	na
status	status	k1gInSc4	status
druhé	druhý	k4xOgFnSc2	druhý
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstavba	výstavba	k1gFnSc1	výstavba
a	a	k8xC	a
dokončování	dokončování	k1gNnSc1	dokončování
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
návrh	návrh	k1gInSc1	návrh
trasy	trasa	k1gFnSc2	trasa
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
konce	konec	k1gInSc2	konec
Ameriky	Amerika	k1gFnSc2	Amerika
na	na	k7c4	na
druhý	druhý	k4xOgInSc1	druhý
byl	být	k5eAaImAgInS	být
předložen	předložit	k5eAaPmNgInS	předložit
na	na	k7c6	na
První	první	k4xOgFnSc6	první
Panamerické	panamerický	k2eAgFnSc6d1	Panamerická
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
jako	jako	k8xC	jako
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
uskutečněn	uskutečnit	k5eAaPmNgInS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
vybudovat	vybudovat	k5eAaPmF	vybudovat
Panamerickou	panamerický	k2eAgFnSc4d1	Panamerická
dálnici	dálnice	k1gFnSc4	dálnice
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgNnP	objevit
na	na	k7c4	na
Páté	pátá	k1gFnPc4	pátá
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
konferenci	konference	k1gFnSc4	konference
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
naplánována	naplánován	k2eAgFnSc1d1	naplánována
jako	jako	k8xC	jako
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
silnice	silnice	k1gFnSc1	silnice
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Konference	konference	k1gFnSc1	konference
o	o	k7c6	o
Panamerické	panamerický	k2eAgFnSc6d1	Panamerická
dálnici	dálnice	k1gFnSc6	dálnice
byla	být	k5eAaImAgFnS	být
svolána	svolán	k2eAgFnSc1d1	svolána
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1925	[number]	k4	1925
v	v	k7c4	v
Buenos	Buenos	k1gInSc4	Buenos
Aires	Airesa	k1gFnPc2	Airesa
<g/>
.	.	kIx.	.
</s>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
bylo	být	k5eAaImAgNnS	být
první	první	k4xOgFnSc7	první
latinskoamerickou	latinskoamerický	k2eAgFnSc7d1	latinskoamerická
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokončila	dokončit	k5eAaPmAgFnS	dokončit
svoji	svůj	k3xOyFgFnSc4	svůj
část	část	k1gFnSc4	část
silnice	silnice	k1gFnSc2	silnice
-	-	kIx~	-
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úsek	úsek	k1gInSc1	úsek
Prudhoe	Prudho	k1gMnSc2	Prudho
Bay	Bay	k1gMnSc2	Bay
-	-	kIx~	-
Yaviza	Yaviz	k1gMnSc2	Yaviz
==	==	k?	==
</s>
</p>
<p>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
trasa	trasa	k1gFnSc1	trasa
nebyla	být	k5eNaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
ustanovena	ustanovit	k5eAaPmNgFnS	ustanovit
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
Panamericana	Panamericana	k1gFnSc1	Panamericana
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c6	na
americkou-mexické	americkouexický	k2eAgFnSc6d1	americkou-mexický
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
města	město	k1gNnSc2	město
Nuevo	Nuevo	k1gNnSc1	Nuevo
Laredo	Laredo	k1gNnSc1	Laredo
(	(	kIx(	(
<g/>
naproti	naproti	k7c3	naproti
městu	město	k1gNnSc3	město
Laredo	Laredo	k1gNnSc1	Laredo
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
jižně	jižně	k6eAd1	jižně
přes	přes	k7c4	přes
Ciudad	Ciudad	k1gInSc4	Ciudad
de	de	k?	de
México	México	k6eAd1	México
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
větve	větev	k1gFnPc1	větev
byly	být	k5eAaImAgFnP	být
vybudovány	vybudovat	k5eAaPmNgFnP	vybudovat
k	k	k7c3	k
hranici	hranice	k1gFnSc3	hranice
v	v	k7c4	v
Nogales	Nogales	k1gInSc4	Nogales
v	v	k7c6	v
Sonoře	sonora	k1gFnSc6	sonora
(	(	kIx(	(
<g/>
u	u	k7c2	u
Nogales	Nogalesa	k1gFnPc2	Nogalesa
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ciudad	Ciudad	k1gInSc1	Ciudad
Juárez	Juáreza	k1gFnPc2	Juáreza
(	(	kIx(	(
<g/>
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Pasa	pást	k5eAaImSgInS	pást
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Piedras	Piedras	k1gMnSc1	Piedras
Negras	Negras	k1gMnSc1	Negras
(	(	kIx(	(
<g/>
Eagle	Eagle	k1gFnSc1	Eagle
Pass	Pass	k1gInSc1	Pass
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Reynosa	Reynosa	k1gFnSc1	Reynosa
(	(	kIx(	(
<g/>
Pharr	Pharr	k1gInSc1	Pharr
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Matamoros	Matamorosa	k1gFnPc2	Matamorosa
(	(	kIx(	(
<g/>
Brownsville	Brownsville	k1gInSc1	Brownsville
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
hranic	hranice	k1gFnPc2	hranice
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
amerických	americký	k2eAgFnPc2d1	americká
dálnic	dálnice	k1gFnPc2	dálnice
lokálně	lokálně	k6eAd1	lokálně
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
Panamericaně	Panamericana	k1gFnSc6	Panamericana
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
část	část	k1gFnSc1	část
I	I	kA	I
35	[number]	k4	35
v	v	k7c6	v
San	San	k1gMnSc6	San
Antoniu	Antonio	k1gMnSc6	Antonio
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
pojmenována	pojmenován	k2eAgNnPc1d1	pojmenováno
jako	jako	k8xS	jako
Pan	Pan	k1gMnSc1	Pan
Am	Am	k1gFnSc2	Am
Expressway	Expresswaa	k1gFnSc2	Expresswaa
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
bylo	být	k5eAaImAgNnS	být
napojení	napojení	k1gNnSc1	napojení
na	na	k7c6	na
původní	původní	k2eAgFnSc6d1	původní
silnici	silnice	k1gFnSc6	silnice
z	z	k7c2	z
Lareda	Lared	k1gMnSc2	Lared
<g/>
.	.	kIx.	.
</s>
<s>
I	I	kA	I
25	[number]	k4	25
v	v	k7c6	v
Albuquerque	Albuquerque	k1gFnSc6	Albuquerque
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Pan-American	Pan-American	k1gInSc1	Pan-American
Freeway	Freewaa	k1gFnSc2	Freewaa
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
prodloužení	prodloužení	k1gNnSc4	prodloužení
trasy	trasa	k1gFnSc2	trasa
z	z	k7c2	z
El	Ela	k1gFnPc2	Ela
Pasa	pást	k5eAaImSgMnS	pást
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
85	[number]	k4	85
severně	severně	k6eAd1	severně
od	od	k7c2	od
El	Ela	k1gFnPc2	Ela
Pasa	pást	k5eAaImSgInS	pást
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
CanAm	CanA	k1gNnSc7	CanA
Highway	Highwaa	k1gFnSc2	Highwaa
<g/>
.	.	kIx.	.
</s>
<s>
Pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
do	do	k7c2	do
kanadského	kanadský	k2eAgInSc2d1	kanadský
Saskatchewanu	Saskatchewan	k1gInSc2	Saskatchewan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
La	la	k1gNnSc6	la
Ronge	Rong	k1gFnSc2	Rong
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Lareda	Lared	k1gMnSc2	Lared
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnSc1	silnice
po	po	k7c6	po
mexické	mexický	k2eAgFnSc6d1	mexická
Federal	Federal	k1gFnSc6	Federal
85	[number]	k4	85
do	do	k7c2	do
Mexico	Mexico	k6eAd1	Mexico
City	city	k1gNnSc2	city
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
trasy	trasa	k1gFnPc1	trasa
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
z	z	k7c2	z
Nogales	Nogalesa	k1gFnPc2	Nogalesa
po	po	k7c6	po
Federal	Federal	k1gFnSc6	Federal
15	[number]	k4	15
</s>
</p>
<p>
<s>
z	z	k7c2	z
El	Ela	k1gFnPc2	Ela
Pasa	pást	k5eAaImSgMnS	pást
po	po	k7c4	po
Federal	Federal	k1gFnSc4	Federal
45	[number]	k4	45
</s>
</p>
<p>
<s>
z	z	k7c2	z
Eagle	Eagle	k1gNnSc2	Eagle
Pass	Passa	k1gFnPc2	Passa
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
po	po	k7c6	po
Federal	Federal	k1gFnSc6	Federal
57	[number]	k4	57
</s>
</p>
<p>
<s>
z	z	k7c2	z
Pharr	Pharra	k1gFnPc2	Pharra
po	po	k7c6	po
Federal	Federal	k1gFnSc6	Federal
40	[number]	k4	40
do	do	k7c2	do
Monterrey	Monterrea	k1gFnSc2	Monterrea
</s>
</p>
<p>
<s>
z	z	k7c2	z
Brownsville	Brownsville	k1gFnSc2	Brownsville
po	po	k7c6	po
Federal	Federal	k1gFnSc6	Federal
101	[number]	k4	101
do	do	k7c2	do
Ciudad	Ciudad	k1gInSc4	Ciudad
VictoriaZ	VictoriaZ	k1gFnSc3	VictoriaZ
Ciudad	Ciudad	k1gInSc1	Ciudad
de	de	k?	de
México	México	k6eAd1	México
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Guatemalou	Guatemala	k1gFnSc7	Guatemala
vede	vést	k5eAaImIp3nS	vést
Panamericana	Panamericana	k1gFnSc1	Panamericana
po	po	k7c6	po
Federal	Federal	k1gFnSc6	Federal
190	[number]	k4	190
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
středoamerické	středoamerický	k2eAgInPc4d1	středoamerický
státy	stát	k1gInPc4	stát
sleduje	sledovat	k5eAaImIp3nS	sledovat
Central	Central	k1gFnSc1	Central
American	Americany	k1gInPc2	Americany
1	[number]	k4	1
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
Yavize	Yaviza	k1gFnSc6	Yaviza
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Dariénu	Darién	k1gInSc2	Darién
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
silnice	silnice	k1gFnSc2	silnice
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
Canitě	Canita	k1gFnSc6	Canita
<g/>
,	,	kIx,	,
178	[number]	k4	178
km	km	kA	km
(	(	kIx(	(
<g/>
110	[number]	k4	110
mil	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
severně	severně	k6eAd1	severně
od	od	k7c2	od
Yavizy	Yaviza	k1gFnSc2	Yaviza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Panamský	panamský	k2eAgInSc1d1	panamský
průplav	průplav	k1gInSc1	průplav
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
USA	USA	kA	USA
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
finanční	finanční	k2eAgFnSc1d1	finanční
podpora	podpora	k1gFnSc1	podpora
USA	USA	kA	USA
výhradně	výhradně	k6eAd1	výhradně
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
vysokého	vysoký	k2eAgInSc2d1	vysoký
mostu	most	k1gInSc2	most
Puente	Puent	k1gInSc5	Puent
de	de	k?	de
las	laso	k1gNnPc2	laso
Américas	Américasa	k1gFnPc2	Américasa
přes	přes	k7c4	přes
průplav	průplav	k1gInSc4	průplav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úsek	úsek	k1gInSc4	úsek
Turbo	turba	k1gFnSc5	turba
<g/>
–	–	k?	–
<g/>
Ushuaia	Ushuaium	k1gNnPc4	Ushuaium
==	==	k?	==
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
silnice	silnice	k1gFnSc2	silnice
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
v	v	k7c4	v
Turbu	turba	k1gFnSc4	turba
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vede	vést	k5eAaImIp3nS	vést
po	po	k7c4	po
Colombia	Colombius	k1gMnSc4	Colombius
52	[number]	k4	52
do	do	k7c2	do
Medellínu	Medellín	k1gInSc2	Medellín
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Medellínu	Medellín	k1gInSc2	Medellín
vede	vést	k5eAaImIp3nS	vést
Colombia	Colombia	k1gFnSc1	Colombia
54	[number]	k4	54
do	do	k7c2	do
Bogoty	Bogota	k1gFnSc2	Bogota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Colombia	Colombia	k1gFnSc1	Colombia
11	[number]	k4	11
míří	mířit	k5eAaImIp3nS	mířit
jižně	jižně	k6eAd1	jižně
přímější	přímý	k2eAgFnSc7d2	přímější
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Colombia	Colombia	k1gFnSc1	Colombia
72	[number]	k4	72
se	se	k3xPyFc4	se
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Bogoty	Bogota	k1gFnSc2	Bogota
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
Colombia	Colombia	k1gFnSc1	Colombia
11	[number]	k4	11
v	v	k7c6	v
Murillu	Murill	k1gInSc6	Murill
<g/>
.	.	kIx.	.
</s>
<s>
Colombia	Colombia	k1gFnSc1	Colombia
11	[number]	k4	11
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
až	až	k9	až
k	k	k7c3	k
ekvádorským	ekvádorský	k2eAgFnPc3d1	ekvádorská
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ecuador	Ecuador	k1gInSc1	Ecuador
35	[number]	k4	35
představuje	představovat	k5eAaImIp3nS	představovat
Panamericanu	Panamericana	k1gFnSc4	Panamericana
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
cestu	cesta	k1gFnSc4	cesta
Ekvádorem	Ekvádor	k1gInSc7	Ekvádor
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
o	o	k7c6	o
Peru	Peru	k1gNnSc6	Peru
1	[number]	k4	1
vedoucí	vedoucí	k1gMnSc1	vedoucí
až	až	k9	až
na	na	k7c4	na
chilské	chilský	k2eAgFnPc4d1	chilská
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
sleduje	sledovat	k5eAaImIp3nS	sledovat
Panamericana	Panamericana	k1gFnSc1	Panamericana
Chile	Chile	k1gNnPc2	Chile
5	[number]	k4	5
až	až	k6eAd1	až
k	k	k7c3	k
obci	obec	k1gFnSc3	obec
Llay	Llaa	k1gFnSc2	Llaa
Llay	Lla	k2eAgFnPc1d1	Lla
severně	severně	k6eAd1	severně
od	od	k7c2	od
Santiaga	Santiago	k1gNnSc2	Santiago
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
po	po	k7c6	po
Chile	Chile	k1gNnSc6	Chile
60	[number]	k4	60
přes	přes	k7c4	přes
San	San	k1gFnSc4	San
Felipe	Felip	k1gInSc5	Felip
a	a	k8xC	a
Los	los	k1gInSc4	los
Andes	Andesa	k1gFnPc2	Andesa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
silnice	silnice	k1gFnSc1	silnice
se	se	k3xPyFc4	se
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Argentinou	Argentina	k1gFnSc7	Argentina
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
Argentina	Argentina	k1gFnSc1	Argentina
National	National	k1gMnSc1	National
Route	Rout	k1gInSc5	Rout
7	[number]	k4	7
vedoucí	vedoucí	k1gMnSc1	vedoucí
do	do	k7c2	do
Buenos	Buenosa	k1gFnPc2	Buenosa
Aires	Airesa	k1gFnPc2	Airesa
<g/>
,	,	kIx,	,
konce	konec	k1gInPc1	konec
hlavní	hlavní	k2eAgFnSc2d1	hlavní
části	část	k1gFnSc2	část
Panamericany	Panamericana	k1gFnSc2	Panamericana
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
po	po	k7c6	po
Argentina	Argentina	k1gFnSc1	Argentina
National	National	k1gMnSc1	National
Route	Rout	k1gInSc5	Rout
3	[number]	k4	3
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Ushuaie	Ushuaie	k1gFnSc2	Ushuaie
a	a	k8xC	a
mysu	mys	k1gInSc2	mys
Horn	Horn	k1gMnSc1	Horn
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
větve	větev	k1gFnPc1	větev
vedou	vést	k5eAaImIp3nP	vést
do	do	k7c2	do
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
,	,	kIx,	,
Paraguaye	Paraguay	k1gFnSc2	Paraguay
<g/>
,	,	kIx,	,
Uruguaye	Uruguay	k1gFnSc2	Uruguay
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
se	se	k3xPyFc4	se
za	za	k7c4	za
Panamericanu	Panamericana	k1gFnSc4	Panamericana
považuje	považovat	k5eAaImIp3nS	považovat
celá	celý	k2eAgFnSc1d1	celá
silnice	silnice	k1gFnSc1	silnice
5	[number]	k4	5
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
i	i	k8xC	i
část	část	k1gFnSc1	část
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Santiaga	Santiago	k1gNnSc2	Santiago
vedoucí	vedoucí	k2eAgInSc4d1	vedoucí
městy	město	k1gNnPc7	město
Rancagua	Rancaguum	k1gNnSc2	Rancaguum
<g/>
,	,	kIx,	,
Curicó	Curicó	k1gFnSc1	Curicó
<g/>
,	,	kIx,	,
Talca	Talca	k1gFnSc1	Talca
<g/>
,	,	kIx,	,
Chillán	Chillán	k1gInSc1	Chillán
<g/>
,	,	kIx,	,
Los	los	k1gInSc1	los
Ángeles	Ángeles	k1gInSc1	Ángeles
<g/>
,	,	kIx,	,
Temuco	Temuco	k1gNnSc1	Temuco
<g/>
,	,	kIx,	,
Osorno	Osorno	k1gNnSc1	Osorno
a	a	k8xC	a
Puerto	Puerta	k1gFnSc5	Puerta
Montt	Montta	k1gFnPc2	Montta
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
silnice	silnice	k1gFnSc2	silnice
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
i	i	k9	i
úsek	úsek	k1gInSc1	úsek
vedoucí	vedoucí	k2eAgInSc1d1	vedoucí
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Chiloé	Chiloá	k1gFnSc2	Chiloá
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nejjižnějším	jižní	k2eAgInSc7d3	nejjižnější
bodem	bod	k1gInSc7	bod
chilské	chilský	k2eAgFnSc2d1	chilská
Panamericany	Panamericana	k1gFnSc2	Panamericana
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Quellón	Quellón	k1gInSc1	Quellón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Španělsky	španělsky	k6eAd1	španělsky
Carretera	Carreter	k1gMnSc4	Carreter
Panamericana	Panamerican	k1gMnSc4	Panamerican
<g/>
,	,	kIx,	,
Autopista	Autopist	k1gMnSc4	Autopist
Panamericana	Panamerican	k1gMnSc4	Panamerican
nebo	nebo	k8xC	nebo
Vía	Vía	k1gMnSc4	Vía
PanAmericana	PanAmerican	k1gMnSc4	PanAmerican
</s>
</p>
<p>
<s>
Anglicky	anglicky	k6eAd1	anglicky
Pan-American	Pan-American	k1gInSc1	Pan-American
Highway	Highwaa	k1gFnSc2	Highwaa
</s>
</p>
<p>
<s>
Portugalsky	portugalsky	k6eAd1	portugalsky
Estrada	Estrada	k1gFnSc1	Estrada
Panamericana	Panamericana	k1gFnSc1	Panamericana
</s>
</p>
<p>
<s>
Nizozemsky	nizozemsky	k6eAd1	nizozemsky
Pan-Amerikaanse	Pan-Amerikaanse	k1gFnSc1	Pan-Amerikaanse
Snelweg	Snelwega	k1gFnPc2	Snelwega
</s>
</p>
<p>
<s>
Francouzsky	francouzsky	k6eAd1	francouzsky
Autoroute	Autorout	k1gMnSc5	Autorout
Panaméricaine	Panaméricain	k1gMnSc5	Panaméricain
</s>
</p>
<p>
<s>
Italsky	italsky	k6eAd1	italsky
Autostrada	Autostrada	k1gFnSc1	Autostrada
Panamericana	Panamericana	k1gFnSc1	Panamericana
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Plán	plán	k1gInSc1	plán
federální	federální	k2eAgFnSc2d1	federální
dálniční	dálniční	k2eAgFnSc2d1	dálniční
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
XX7	XX7	k1gFnSc1	XX7
</s>
</p>
<p>
<s>
Reportáž	reportáž	k1gFnSc1	reportáž
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
automobilismu	automobilismus	k1gInSc2	automobilismus
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
XX6	XX6	k1gFnSc1	XX6
</s>
</p>
<p>
<s>
Silnice	silnice	k1gFnSc1	silnice
západní	západní	k2eAgFnSc2d1	západní
polokoule	polokoule	k1gFnSc2	polokoule
je	být	k5eAaImIp3nS	být
blízko	blízko	k7c2	blízko
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Times	Times	k1gInSc1	Times
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
58	[number]	k4	58
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
Karibiku	Karibik	k1gInSc2	Karibik
<g/>
,	,	kIx,	,
Střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
Americké	americký	k2eAgFnSc2d1	americká
automobilová	automobilový	k2eAgFnSc1d1	automobilová
asociace	asociace	k1gFnSc2	asociace
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jake	Jakat	k5eAaPmIp3nS	Jakat
Silverstein	Silverstein	k2eAgMnSc1d1	Silverstein
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Highway	Highway	k1gInPc4	Highway
Run	run	k1gInSc1	run
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Harper	Harper	k1gMnSc1	Harper
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
červenec	červenec	k1gInSc1	červenec
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Panamericana	Panamerican	k1gMnSc2	Panamerican
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Panamericana	Panamerican	k1gMnSc2	Panamerican
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Dostála	Dostál	k1gMnSc2	Dostál
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
cestě	cesta	k1gFnSc6	cesta
</s>
</p>
<p>
<s>
Microsoft	Microsoft	kA	Microsoft
Encarta	Encarta	k1gFnSc1	Encarta
–	–	k?	–
Panamericana	Panamericana	k1gFnSc1	Panamericana
</s>
</p>
