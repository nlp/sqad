<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
je	být	k5eAaImIp3nS	být
nejcitlivější	citlivý	k2eAgNnSc1d3	nejcitlivější
právě	právě	k9	právě
na	na	k7c4	na
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
výsledek	výsledek	k1gInSc4	výsledek
evoluce	evoluce	k1gFnSc2	evoluce
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
díky	dík	k1gInPc7	dík
chlorofylu	chlorofyl	k1gInSc2	chlorofyl
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
nejvíce	nejvíce	k6eAd1	nejvíce
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
