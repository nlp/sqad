<s>
Roup	roup	k1gMnSc1	roup
dětský	dětský	k2eAgMnSc1d1	dětský
(	(	kIx(	(
<g/>
Enterobius	Enterobius	k1gInSc1	Enterobius
vermicularis	vermicularis	k1gInSc1	vermicularis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
parazitický	parazitický	k2eAgMnSc1d1	parazitický
hlíst	hlíst	k1gMnSc1	hlíst
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
Oxyurida	Oxyurida	k1gFnSc1	Oxyurida
<g/>
.	.	kIx.	.
</s>
<s>
Roup	roup	k1gMnSc1	roup
dětský	dětský	k2eAgMnSc1d1	dětský
parazituje	parazitovat	k5eAaImIp3nS	parazitovat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
tlustém	tlustý	k2eAgNnSc6d1	tlusté
střevě	střevo	k1gNnSc6	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc1	samička
během	během	k7c2	během
spánku	spánek	k1gInSc2	spánek
hostitele	hostitel	k1gMnSc4	hostitel
vylézají	vylézat	k5eAaImIp3nP	vylézat
z	z	k7c2	z
konečníku	konečník	k1gInSc2	konečník
a	a	k8xC	a
kladou	klást	k5eAaImIp3nP	klást
vajíčka	vajíčko	k1gNnPc4	vajíčko
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
tímto	tento	k3xDgMnSc7	tento
střevním	střevní	k2eAgMnSc7d1	střevní
parazitem	parazit	k1gMnSc7	parazit
se	se	k3xPyFc4	se
odborně	odborně	k6eAd1	odborně
nazývá	nazývat	k5eAaImIp3nS	nazývat
enterobióza	enterobióza	k1gFnSc1	enterobióza
<g/>
.	.	kIx.	.
</s>
<s>
Roup	roup	k1gMnSc1	roup
dětský	dětský	k2eAgMnSc1d1	dětský
je	být	k5eAaImIp3nS	být
červ	červ	k1gMnSc1	červ
bělavé	bělavý	k2eAgFnSc2d1	bělavá
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
tenkém	tenký	k2eAgNnSc6d1	tenké
a	a	k8xC	a
tlustém	tlustý	k2eAgNnSc6d1	tlusté
střevě	střevo	k1gNnSc6	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
sameček	sameček	k1gMnSc1	sameček
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samec	samec	k1gMnSc1	samec
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnSc2	délka
6	[number]	k4	6
mm	mm	kA	mm
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
až	až	k9	až
12	[number]	k4	12
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
klade	klást	k5eAaImIp3nS	klást
až	až	k9	až
12	[number]	k4	12
000	[number]	k4	000
oplozených	oplozený	k2eAgNnPc2d1	oplozené
vajíček	vajíčko	k1gNnPc2	vajíčko
kolem	kolem	k7c2	kolem
řitního	řitní	k2eAgInSc2d1	řitní
otvoru	otvor	k1gInSc2	otvor
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
nakazí	nakazit	k5eAaPmIp3nS	nakazit
ústy	ústa	k1gNnPc7	ústa
pozřením	pozření	k1gNnPc3	pozření
vajíček	vajíčko	k1gNnPc2	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
nakazí	nakazit	k5eAaPmIp3nP	nakazit
ve	v	k7c6	v
školkách	školka	k1gFnPc6	školka
a	a	k8xC	a
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
mnohdy	mnohdy	k6eAd1	mnohdy
zapomenou	zapomnět	k5eAaImIp3nP	zapomnět
umýt	umýt	k5eAaPmF	umýt
ruce	ruka	k1gFnPc4	ruka
a	a	k8xC	a
přenášení	přenášení	k1gNnSc6	přenášení
vajíček	vajíčko	k1gNnPc2	vajíčko
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
volnou	volný	k2eAgFnSc4d1	volná
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Nakazit	nakazit	k5eAaPmF	nakazit
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
ale	ale	k8xC	ale
i	i	k9	i
dospělí	dospělí	k1gMnPc1	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Oplozená	oplozený	k2eAgNnPc1d1	oplozené
vajíčka	vajíčko	k1gNnPc1	vajíčko
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
nepříjemný	příjemný	k2eNgInSc4d1	nepříjemný
pocit	pocit	k1gInSc4	pocit
svědění	svědění	k1gNnSc2	svědění
<g/>
,	,	kIx,	,
pálení	pálení	k1gNnSc2	pálení
uvnitř	uvnitř	k7c2	uvnitř
a	a	k8xC	a
kolem	kolem	k7c2	kolem
konečníku	konečník	k1gInSc2	konečník
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
škrábe	škrábat	k5eAaImIp3nS	škrábat
a	a	k8xC	a
na	na	k7c6	na
konečcích	koneček	k1gInPc6	koneček
prstů	prst	k1gInPc2	prst
mohou	moct	k5eAaImIp3nP	moct
ulpět	ulpět	k5eAaPmF	ulpět
oplozená	oplozený	k2eAgNnPc4d1	oplozené
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
.	.	kIx.	.
</s>
<s>
Neumyté	umytý	k2eNgFnPc1d1	neumytá
ruce	ruka	k1gFnPc1	ruka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zdrojem	zdroj	k1gInSc7	zdroj
opětné	opětný	k2eAgFnPc4d1	opětná
nákazy	nákaza	k1gFnPc4	nákaza
-	-	kIx~	-
autoinfekce	autoinfekce	k1gFnPc4	autoinfekce
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
lehká	lehký	k2eAgNnPc1d1	lehké
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
roznášena	roznášet	k5eAaImNgFnS	roznášet
dokonce	dokonce	k9	dokonce
prouděním	proudění	k1gNnSc7	proudění
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
mouchami	moucha	k1gFnPc7	moucha
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
únavou	únava	k1gFnSc7	únava
<g/>
,	,	kIx,	,
časté	častý	k2eAgFnPc4d1	častá
potřeby	potřeba	k1gFnPc4	potřeba
se	se	k3xPyFc4	se
vyprázdnit	vyprázdnit	k5eAaPmF	vyprázdnit
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnSc3	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
podrážděností	podrážděnost	k1gFnSc7	podrážděnost
<g/>
,	,	kIx,	,
svěděním	svědění	k1gNnSc7	svědění
a	a	k8xC	a
kopřivkami	kopřivka	k1gFnPc7	kopřivka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stolici	stolice	k1gFnSc6	stolice
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
objevovat	objevovat	k5eAaImF	objevovat
nejen	nejen	k6eAd1	nejen
oplozená	oplozený	k2eAgNnPc4d1	oplozené
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dospělí	dospělý	k2eAgMnPc1d1	dospělý
roupi	roup	k1gMnPc1	roup
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
výjimečně	výjimečně	k6eAd1	výjimečně
mohou	moct	k5eAaImIp3nP	moct
roupi	roup	k1gMnPc1	roup
způsobit	způsobit	k5eAaPmF	způsobit
další	další	k2eAgFnPc4d1	další
komplikace	komplikace	k1gFnPc4	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
souvislost	souvislost	k1gFnSc1	souvislost
mezi	mezi	k7c7	mezi
zánětem	zánět	k1gInSc7	zánět
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
a	a	k8xC	a
roupy	roupy	k1gInPc7	roupy
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
popsaly	popsat	k5eAaPmAgFnP	popsat
raritní	raritní	k2eAgInSc4d1	raritní
výskyt	výskyt	k1gInSc4	výskyt
roupů	roupy	k1gInPc2	roupy
v	v	k7c6	v
ledvinách	ledvina	k1gFnPc6	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
velmi	velmi	k6eAd1	velmi
ojediněle	ojediněle	k6eAd1	ojediněle
nacházet	nacházet	k5eAaImF	nacházet
i	i	k9	i
v	v	k7c6	v
pohlavních	pohlavní	k2eAgFnPc6d1	pohlavní
a	a	k8xC	a
močových	močový	k2eAgFnPc6d1	močová
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
léčit	léčit	k5eAaImF	léčit
snadno	snadno	k6eAd1	snadno
<g/>
,	,	kIx,	,
léčba	léčba	k1gFnSc1	léčba
má	mít	k5eAaImIp3nS	mít
až	až	k9	až
95	[number]	k4	95
<g/>
%	%	kIx~	%
účinnost	účinnost	k1gFnSc1	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Účinnou	účinný	k2eAgFnSc7d1	účinná
prevencí	prevence	k1gFnSc7	prevence
před	před	k7c7	před
těmito	tento	k3xDgMnPc7	tento
parazity	parazit	k1gMnPc7	parazit
je	být	k5eAaImIp3nS	být
dodržování	dodržování	k1gNnSc4	dodržování
osobní	osobní	k2eAgFnSc2d1	osobní
hygieny	hygiena	k1gFnSc2	hygiena
a	a	k8xC	a
čistota	čistota	k1gFnSc1	čistota
<g/>
,	,	kIx,	,
léčba	léčba	k1gFnSc1	léčba
probíhá	probíhat	k5eAaImIp3nS	probíhat
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
albendazolu	albendazol	k1gInSc2	albendazol
<g/>
,	,	kIx,	,
mebendazolu	mebendazol	k1gInSc2	mebendazol
a	a	k8xC	a
Pyrvinium	Pyrvinium	k1gNnSc4	Pyrvinium
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
je	být	k5eAaImIp3nS	být
dvoufázová	dvoufázový	k2eAgFnSc1d1	dvoufázová
<g/>
,	,	kIx,	,
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
dávkou	dávka	k1gFnSc7	dávka
po	po	k7c6	po
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
týdnech	týden	k1gInPc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
možnost	možnost	k1gFnSc4	možnost
předepsat	předepsat	k5eAaPmF	předepsat
pouze	pouze	k6eAd1	pouze
VERMOX	VERMOX	kA	VERMOX
lék	lék	k1gInSc4	lék
širokospektrální	širokospektrální	k2eAgInSc1d1	širokospektrální
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
podle	podle	k7c2	podle
síly	síla	k1gFnSc2	síla
dávky	dávka	k1gFnSc2	dávka
likviduje	likvidovat	k5eAaBmIp3nS	likvidovat
nejen	nejen	k6eAd1	nejen
roup	roup	k1gMnSc1	roup
dětský	dětský	k2eAgMnSc1d1	dětský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
i	i	k9	i
tasemnice	tasemnice	k1gFnSc2	tasemnice
a	a	k8xC	a
hlístice	hlístice	k1gFnSc2	hlístice
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
léčby	léčba	k1gFnSc2	léčba
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
omezit	omezit	k5eAaPmF	omezit
příjem	příjem	k1gInSc1	příjem
bílého	bílý	k2eAgInSc2d1	bílý
rafinovaného	rafinovaný	k2eAgInSc2d1	rafinovaný
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
důležité	důležitý	k2eAgNnSc1d1	důležité
dodržovat	dodržovat	k5eAaImF	dodržovat
přísné	přísný	k2eAgInPc4d1	přísný
hygienické	hygienický	k2eAgInPc4d1	hygienický
návyky	návyk	k1gInPc4	návyk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
mytí	mytí	k1gNnSc1	mytí
rukou	ruka	k1gFnPc2	ruka
po	po	k7c6	po
toaletě	toaleta	k1gFnSc6	toaleta
<g/>
,	,	kIx,	,
stříhání	stříhání	k1gNnSc6	stříhání
a	a	k8xC	a
čištění	čištění	k1gNnSc6	čištění
nehtů	nehet	k1gInPc2	nehet
<g/>
,	,	kIx,	,
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
střídání	střídání	k1gNnSc1	střídání
spodního	spodní	k2eAgNnSc2d1	spodní
prádla	prádlo	k1gNnSc2	prádlo
a	a	k8xC	a
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
sprchování	sprchování	k1gNnSc4	sprchování
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
jsou	být	k5eAaImIp3nP	být
citlivá	citlivý	k2eAgNnPc1d1	citlivé
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
praní	praní	k1gNnSc4	praní
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
teplotu	teplota	k1gFnSc4	teplota
jejich	jejich	k3xOp3gInSc7	jejich
účinným	účinný	k2eAgInSc7d1	účinný
zabijákem	zabiják	k1gInSc7	zabiják
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
roupy	roup	k1gMnPc4	roup
je	být	k5eAaImIp3nS	být
nepříjemné	příjemný	k2eNgNnSc1d1	nepříjemné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
není	být	k5eNaImIp3nS	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
