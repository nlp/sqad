<s>
Hřebíček	hřebíček	k1gInSc1	hřebíček
je	být	k5eAaImIp3nS	být
sušený	sušený	k2eAgInSc1d1	sušený
kalich	kalich	k1gInSc1	kalich
s	s	k7c7	s
poupětem	poupě	k1gNnSc7	poupě
hřebíčkovce	hřebíčkovec	k1gInSc2	hřebíčkovec
kořenného	kořenný	k2eAgInSc2d1	kořenný
(	(	kIx(	(
<g/>
Syzygium	Syzygium	k1gNnSc1	Syzygium
aromaticum	aromaticum	k1gNnSc1	aromaticum
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Eugenia	Eugenium	k1gNnPc1	Eugenium
caryophyllata	caryophylle	k1gNnPc4	caryophylle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Moluckých	molucký	k2eAgInPc2d1	molucký
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ke	k	k7c3	k
kořenění	kořenění	k1gNnSc3	kořenění
sladkých	sladký	k2eAgFnPc2d1	sladká
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc2	rýže
a	a	k8xC	a
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
výrazné	výrazný	k2eAgNnSc4d1	výrazné
aroma	aroma	k1gNnSc4	aroma
je	být	k5eAaImIp3nS	být
i	i	k9	i
součástí	součást	k1gFnPc2	součást
mnoha	mnoho	k4c2	mnoho
aromatických	aromatický	k2eAgFnPc2d1	aromatická
směsí	směs	k1gFnPc2	směs
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
hřebíčkové	hřebíčkový	k2eAgFnSc2d1	hřebíčková
silice	silice	k1gFnSc2	silice
je	být	k5eAaImIp3nS	být
fenolická	fenolický	k2eAgFnSc1d1	fenolická
sloučenina	sloučenina	k1gFnSc1	sloučenina
eugenol	eugenol	k1gInSc1	eugenol
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
lokálně-anestetické	lokálněnestetický	k2eAgInPc4d1	lokálně-anestetický
a	a	k8xC	a
antiseptické	antiseptický	k2eAgInPc4d1	antiseptický
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
v	v	k7c6	v
zubním	zubní	k2eAgNnSc6d1	zubní
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Hřebíček	hřebíček	k1gInSc1	hřebíček
má	mít	k5eAaImIp3nS	mít
nasládlou	nasládlý	k2eAgFnSc4d1	nasládlá
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
často	často	k6eAd1	často
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
chutě	chuť	k1gFnSc2	chuť
hořké	hořká	k1gFnSc2	hořká
<g/>
.	.	kIx.	.
</s>
<s>
Hřebíček	hřebíček	k1gInSc1	hřebíček
s	s	k7c7	s
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
černou	černý	k2eAgFnSc7d1	černá
hlavičkou	hlavička	k1gFnSc7	hlavička
je	být	k5eAaImIp3nS	být
závadný	závadný	k2eAgInSc4d1	závadný
nebo	nebo	k8xC	nebo
příliš	příliš	k6eAd1	příliš
starý	starý	k2eAgInSc1d1	starý
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
značnou	značný	k2eAgFnSc4d1	značná
oblibu	obliba	k1gFnSc4	obliba
sehrál	sehrát	k5eAaPmAgInS	sehrát
hřebíček	hřebíček	k1gInSc1	hřebíček
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
zámořských	zámořský	k2eAgInPc2d1	zámořský
objevů	objev	k1gInPc2	objev
<g/>
.	.	kIx.	.
</s>
<s>
Hřebíček	Hřebíček	k1gMnSc1	Hřebíček
se	se	k3xPyFc4	se
prokazatelně	prokazatelně	k6eAd1	prokazatelně
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Středomoří	středomoří	k1gNnSc2	středomoří
už	už	k6eAd1	už
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
dováželi	dovážet	k5eAaImAgMnP	dovážet
hřebíček	hřebíček	k1gInSc4	hřebíček
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
především	především	k9	především
arabští	arabský	k2eAgMnPc1d1	arabský
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
všeobecně	všeobecně	k6eAd1	všeobecně
známý	známý	k2eAgMnSc1d1	známý
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
cenným	cenný	k2eAgInSc7d1	cenný
obchodním	obchodní	k2eAgInSc7d1	obchodní
artiklem	artikl	k1gInSc7	artikl
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zkrácení	zkrácení	k1gNnSc1	zkrácení
cesty	cesta	k1gFnSc2	cesta
na	na	k7c4	na
Moluky	Moluky	k1gFnPc4	Moluky
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc4	ostrov
hřebíčku	hřebíček	k1gInSc2	hřebíček
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
hlavní	hlavní	k2eAgInSc4d1	hlavní
motiv	motiv	k1gInSc4	motiv
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
výpravě	výprava	k1gFnSc3	výprava
Fernaa	Fernaa	k1gMnSc1	Fernaa
Magalhã	Magalhã	k1gMnSc1	Magalhã
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cesta	cesta	k1gFnSc1	cesta
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
bude	být	k5eAaImBp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
než	než	k8xS	než
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
plavba	plavba	k1gFnSc1	plavba
kolem	kolem	k7c2	kolem
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Očekávání	očekávání	k1gNnSc1	očekávání
se	se	k3xPyFc4	se
nepotvrdilo	potvrdit	k5eNaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
Magalhã	Magalhã	k1gFnSc1	Magalhã
na	na	k7c6	na
výpravě	výprava	k1gFnSc6	výprava
zahynul	zahynout	k5eAaPmAgMnS	zahynout
a	a	k8xC	a
cestu	cesta	k1gFnSc4	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
dokončila	dokončit	k5eAaPmAgFnS	dokončit
jediná	jediný	k2eAgFnSc1d1	jediná
loď	loď	k1gFnSc1	loď
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
plně	plně	k6eAd1	plně
naložená	naložený	k2eAgNnPc4d1	naložené
hřebíčkem	hřebíček	k1gInSc7	hřebíček
a	a	k8xC	a
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
tak	tak	k6eAd1	tak
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
celkové	celkový	k2eAgInPc4d1	celkový
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
vynaložené	vynaložený	k2eAgInPc4d1	vynaložený
na	na	k7c4	na
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
se	se	k3xPyFc4	se
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
uchýlilo	uchýlit	k5eAaPmAgNnS	uchýlit
k	k	k7c3	k
radikálnímu	radikální	k2eAgInSc3d1	radikální
kroku	krok	k1gInSc2	krok
–	–	k?	–
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
udrželo	udržet	k5eAaPmAgNnS	udržet
monopol	monopol	k1gInSc4	monopol
na	na	k7c4	na
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
hřebíčkem	hřebíček	k1gInSc7	hřebíček
a	a	k8xC	a
muškátem	muškát	k1gInSc7	muškát
<g/>
,	,	kIx,	,
zlikvidovalo	zlikvidovat	k5eAaPmAgNnS	zlikvidovat
všechny	všechen	k3xTgFnPc4	všechen
hřebíčkové	hřebíčkový	k2eAgFnPc4d1	hřebíčková
a	a	k8xC	a
muškátové	muškátový	k2eAgFnPc4d1	muškátová
plantáže	plantáž	k1gFnPc4	plantáž
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
přísně	přísně	k6eAd1	přísně
střeženého	střežený	k2eAgInSc2d1	střežený
moluckého	molucký	k2eAgInSc2d1	molucký
ostrova	ostrov	k1gInSc2	ostrov
Ambon	ambona	k1gFnPc2	ambona
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
r.	r.	kA	r.
1769	[number]	k4	1769
se	se	k3xPyFc4	se
francouzskému	francouzský	k2eAgMnSc3d1	francouzský
guvernérovi	guvernér	k1gMnSc3	guvernér
ostrova	ostrov	k1gInSc2	ostrov
Mauritius	Mauritius	k1gInSc4	Mauritius
podařilo	podařit	k5eAaPmAgNnS	podařit
propašovat	propašovat	k5eAaPmF	propašovat
semena	semeno	k1gNnPc4	semeno
obou	dva	k4xCgFnPc2	dva
rostlin	rostlina	k1gFnPc2	rostlina
v	v	k7c6	v
dutých	dutý	k2eAgFnPc6d1	dutá
podrážkách	podrážka	k1gFnPc6	podrážka
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
pak	pak	k6eAd1	pak
založila	založit	k5eAaPmAgFnS	založit
vlastní	vlastní	k2eAgFnPc4d1	vlastní
plantáže	plantáž	k1gFnPc4	plantáž
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Réunion	Réunion	k1gInSc1	Réunion
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
hřebíčku	hřebíček	k1gInSc6	hřebíček
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
ze	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
našim	náš	k3xOp1gFnPc3	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Hřebíček	hřebíček	k1gInSc1	hřebíček
vyniká	vynikat	k5eAaImIp3nS	vynikat
svou	svůj	k3xOyFgFnSc7	svůj
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
příjemnou	příjemný	k2eAgFnSc7d1	příjemná
aromatickou	aromatický	k2eAgFnSc7d1	aromatická
vůní	vůně	k1gFnSc7	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
přípravou	příprava	k1gFnSc7	příprava
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
čerstvě	čerstvě	k6eAd1	čerstvě
mletý	mletý	k2eAgMnSc1d1	mletý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
svou	svůj	k3xOyFgFnSc4	svůj
specifickou	specifický	k2eAgFnSc4d1	specifická
vůni	vůně	k1gFnSc4	vůně
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
jednodruhové	jednodruhový	k2eAgNnSc1d1	jednodruhové
koření	koření	k1gNnSc1	koření
tvoří	tvořit	k5eAaImIp3nS	tvořit
součást	součást	k1gFnSc4	součást
mnoha	mnoho	k4c2	mnoho
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
např.	např.	kA	např.
kari	kari	k1gNnSc1	kari
<g/>
,	,	kIx,	,
garam	garam	k6eAd1	garam
masala	masala	k1gFnSc1	masala
nebo	nebo	k8xC	nebo
francouzské	francouzský	k2eAgFnPc1d1	francouzská
směsi	směs	k1gFnPc1	směs
quatre	quatr	k1gInSc5	quatr
épices	épices	k1gMnSc1	épices
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
hřebíčku	hřebíček	k1gInSc2	hřebíček
je	být	k5eAaImIp3nS	být
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Hodí	hodit	k5eAaImIp3nS	hodit
se	se	k3xPyFc4	se
do	do	k7c2	do
slaných	slaný	k2eAgInPc2d1	slaný
i	i	k8xC	i
sladkých	sladký	k2eAgInPc2d1	sladký
pokrmů	pokrm	k1gInPc2	pokrm
<g/>
,	,	kIx,	,
sušenek	sušenka	k1gFnPc2	sušenka
<g/>
,	,	kIx,	,
kompotů	kompot	k1gInPc2	kompot
<g/>
,	,	kIx,	,
pečiva	pečivo	k1gNnSc2	pečivo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
svařeného	svařený	k2eAgNnSc2d1	svařené
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Dochutit	dochutit	k5eAaPmF	dochutit
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
lze	lze	k6eAd1	lze
hovězí	hovězí	k2eAgInSc1d1	hovězí
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
omáčky	omáčka	k1gFnPc4	omáčka
<g/>
,	,	kIx,	,
šunku	šunka	k1gFnSc4	šunka
<g/>
,	,	kIx,	,
masové	masový	k2eAgInPc4d1	masový
i	i	k8xC	i
zeleninové	zeleninový	k2eAgInPc4d1	zeleninový
vývary	vývar	k1gInPc4	vývar
a	a	k8xC	a
pokrmy	pokrm	k1gInPc4	pokrm
či	či	k8xC	či
obyčejnou	obyčejný	k2eAgFnSc4d1	obyčejná
rýži	rýže	k1gFnSc4	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Hřebíček	Hřebíček	k1gMnSc1	Hřebíček
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
jako	jako	k9	jako
koření	koření	k1gNnSc1	koření
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
ale	ale	k9	ale
výrazné	výrazný	k2eAgFnPc4d1	výrazná
desinfekční	desinfekční	k2eAgFnPc4d1	desinfekční
a	a	k8xC	a
místně	místně	k6eAd1	místně
znecitlivující	znecitlivující	k2eAgInPc4d1	znecitlivující
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
mírně	mírně	k6eAd1	mírně
dráždivě	dráždivě	k6eAd1	dráždivě
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
využíván	využívat	k5eAaImNgInS	využívat
v	v	k7c6	v
zubním	zubní	k2eAgNnSc6d1	zubní
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
využít	využít	k5eAaPmF	využít
jako	jako	k9	jako
účinnou	účinný	k2eAgFnSc4d1	účinná
příměs	příměs	k1gFnSc4	příměs
kloktadel	kloktadlo	k1gNnPc2	kloktadlo
<g/>
.	.	kIx.	.
</s>
<s>
Hřebíčková	hřebíčkový	k2eAgFnSc1d1	hřebíčková
silice	silice	k1gFnSc1	silice
tlumí	tlumit	k5eAaImIp3nS	tlumit
citlivost	citlivost	k1gFnSc4	citlivost
i	i	k8xC	i
bolestivost	bolestivost	k1gFnSc4	bolestivost
zubního	zubní	k2eAgInSc2d1	zubní
nervu	nerv	k1gInSc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
protirevmatických	protirevmatický	k2eAgFnPc6d1	protirevmatická
mastech	mast	k1gFnPc6	mast
<g/>
.	.	kIx.	.
</s>
<s>
Podáván	podáván	k2eAgInSc1d1	podáván
inhalační	inhalační	k2eAgFnSc7d1	inhalační
formou	forma	k1gFnSc7	forma
desinfikuje	desinfikovat	k5eAaBmIp3nS	desinfikovat
dýchací	dýchací	k2eAgFnPc4d1	dýchací
cesty	cesta	k1gFnPc4	cesta
a	a	k8xC	a
léčí	léčit	k5eAaImIp3nP	léčit
průduškové	průduškový	k2eAgInPc4d1	průduškový
záněty	zánět	k1gInPc4	zánět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
učení	učení	k1gNnSc2	učení
ájurvédy	ájurvéda	k1gFnSc2	ájurvéda
podáván	podáván	k2eAgInSc4d1	podáván
v	v	k7c6	v
prášku	prášek	k1gInSc6	prášek
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
povzbudivě	povzbudivě	k6eAd1	povzbudivě
činnost	činnost	k1gFnSc4	činnost
srdečního	srdeční	k2eAgInSc2d1	srdeční
svalu	sval	k1gInSc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
hodně	hodně	k6eAd1	hodně
účinný	účinný	k2eAgInSc1d1	účinný
na	na	k7c4	na
potlačení	potlačení	k1gNnSc4	potlačení
bolesti	bolest	k1gFnSc2	bolest
zubního	zubní	k2eAgInSc2d1	zubní
zánětu	zánět	k1gInSc2	zánět
<g/>
.	.	kIx.	.
</s>
<s>
JANÁČEK	Janáček	k1gMnSc1	Janáček
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Století	století	k1gNnSc1	století
zámořských	zámořský	k2eAgInPc2d1	zámořský
objevů	objev	k1gInPc2	objev
(	(	kIx(	(
<g/>
1415	[number]	k4	1415
-	-	kIx~	-
1522	[number]	k4	1522
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
<s>
ŽÁČEK	Žáček	k1gMnSc1	Žáček
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Vůně	vůně	k1gFnSc1	vůně
koření	koření	k1gNnSc2	koření
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Antonio	Antonio	k1gMnSc1	Antonio
Pigafetta	Pigafetta	k1gMnSc1	Pigafetta
<g/>
,	,	kIx,	,
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
první	první	k4xOgFnSc6	první
cestě	cesta	k1gFnSc6	cesta
kolem	kolem	k7c2	kolem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Argo	Argo	k6eAd1	Argo
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7203-540-1	[number]	k4	80-7203-540-1
Jiří	Jiří	k1gMnSc1	Jiří
Janča	Janča	k1gMnSc1	Janča
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
A.	A.	kA	A.
Zentrich	Zentrich	k1gInSc1	Zentrich
Herbář	herbář	k1gInSc1	herbář
léčivých	léčivý	k2eAgFnPc2d1	léčivá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
EMINENT	EMINENT	kA	EMINENT
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85876-04-3	[number]	k4	80-85876-04-3
Koření	koření	k1gNnPc2	koření
Gastronomie	gastronomie	k1gFnSc2	gastronomie
Zdraví	zdravit	k5eAaImIp3nP	zdravit
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hřebíček	hřebíček	k1gInSc1	hřebíček
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hřebíček	hřebíček	k1gInSc1	hřebíček
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
