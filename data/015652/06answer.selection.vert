<s>
Anesteziologicko-resuscitační	anesteziologicko-resuscitační	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
ARO	ara	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
strukturální	strukturální	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
zdravotnického	zdravotnický	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
akutní	akutní	k2eAgFnSc2d1
lůžkové	lůžkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
členěná	členěný	k2eAgFnSc1d1
na	na	k7c4
další	další	k2eAgFnPc4d1
podjednotky	podjednotka	k1gFnPc4
<g/>
:	:	kIx,
lůžkovou	lůžkový	k2eAgFnSc4d1
(	(	kIx(
<g/>
resuscitační	resuscitační	k2eAgFnSc4d1
<g/>
)	)	kIx)
stanici	stanice	k1gFnSc4
<g/>
,	,	kIx,
anesteziologický	anesteziologický	k2eAgInSc4d1
úsek	úsek	k1gInSc4
<g/>
,	,	kIx,
anesteziologickou	anesteziologický	k2eAgFnSc4d1
ambulanci	ambulance	k1gFnSc4
a	a	k8xC
někdy	někdy	k6eAd1
také	také	k9
ambulanci	ambulance	k1gFnSc4
bolesti	bolest	k1gFnSc2
<g/>
.	.	kIx.
</s>