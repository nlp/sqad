<s>
Anesteziologicko-resuscitační	anesteziologicko-resuscitační	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Anesteziologicko-resuscitační	anesteziologicko-resuscitační	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
ARO	ara	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
strukturální	strukturální	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
zdravotnického	zdravotnický	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
akutní	akutní	k2eAgFnSc2d1
lůžkové	lůžkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
členěná	členěný	k2eAgFnSc1d1
na	na	k7c4
další	další	k2eAgFnPc4d1
podjednotky	podjednotka	k1gFnPc4
<g/>
:	:	kIx,
lůžkovou	lůžkový	k2eAgFnSc4d1
(	(	kIx(
<g/>
resuscitační	resuscitační	k2eAgFnSc4d1
<g/>
)	)	kIx)
stanici	stanice	k1gFnSc4
<g/>
,	,	kIx,
anesteziologický	anesteziologický	k2eAgInSc4d1
úsek	úsek	k1gInSc4
<g/>
,	,	kIx,
anesteziologickou	anesteziologický	k2eAgFnSc4d1
ambulanci	ambulance	k1gFnSc4
a	a	k8xC
někdy	někdy	k6eAd1
také	také	k9
ambulanci	ambulance	k1gFnSc4
bolesti	bolest	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Péči	péče	k1gFnSc6
o	o	k7c4
pacienty	pacient	k1gMnPc4
zajišťují	zajišťovat	k5eAaImIp3nP
lékaři	lékař	k1gMnPc1
-	-	kIx~
specialisté	specialista	k1gMnPc1
v	v	k7c6
t.	t.	k?
<g/>
č.	č.	k?
pětiletém	pětiletý	k2eAgInSc6d1
základním	základní	k2eAgInSc6d1
medicínském	medicínský	k2eAgInSc6d1
oboru	obor	k1gInSc6
Anesteziologie	anesteziologie	k1gFnSc1
a	a	k8xC
intenzivní	intenzivní	k2eAgFnSc1d1
medicína	medicína	k1gFnSc1
<g/>
,	,	kIx,
sestry	sestra	k1gFnPc1
-	-	kIx~
specialistky	specialistka	k1gFnPc1
v	v	k7c6
oboru	obor	k1gInSc6
Intenzivní	intenzivní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Anesteziologie	anesteziologie	k1gFnSc2
<g/>
,	,	kIx,
resuscitace	resuscitace	k1gFnSc2
a	a	k8xC
intenzivní	intenzivní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
-	-	kIx~
ARIP	ARIP	kA
<g/>
)	)	kIx)
a	a	k8xC
zdravotničtí	zdravotnický	k2eAgMnPc1d1
záchranáři	záchranář	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Úseky	úsek	k1gInPc4
</s>
<s>
Lůžková	lůžkový	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
ARO	ara	k1gMnSc5
</s>
<s>
Lůžková	lůžkový	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
ARO	ara	k1gMnSc5
představuje	představovat	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
stupeň	stupeň	k1gInSc4
intenzivní	intenzivní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
ve	v	k7c6
zdravotnickém	zdravotnický	k2eAgNnSc6d1
zařízení	zařízení	k1gNnSc6
akutní	akutní	k2eAgFnSc2d1
lůžkové	lůžkový	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
superiorní	superiorní	k2eAgInSc4d1
vůči	vůči	k7c3
oborovým	oborový	k2eAgFnPc3d1
jednotkám	jednotka	k1gFnPc3
intenzivní	intenzivní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
(	(	kIx(
<g/>
JIP	JIP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
na	na	k7c4
ni	on	k3xPp3gFnSc4
přijímáni	přijímán	k2eAgMnPc1d1
pacienti	pacient	k1gMnPc1
ve	v	k7c6
stavu	stav	k1gInSc6
probíhajícího	probíhající	k2eAgNnSc2d1
selhání	selhání	k1gNnSc2
jedné	jeden	k4xCgFnSc2
či	či	k8xC
více	hodně	k6eAd2
základních	základní	k2eAgFnPc2d1
životních	životní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
vědomí	vědomí	k1gNnSc1
<g/>
,	,	kIx,
dýchání	dýchání	k1gNnSc1
<g/>
,	,	kIx,
oběhu	oběh	k1gInSc2
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
vnitřního	vnitřní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
základní	základní	k2eAgFnSc4d1
příčinu	příčina	k1gFnSc4
(	(	kIx(
<g/>
vstupní	vstupní	k2eAgFnSc4d1
diagnózu	diagnóza	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
přímém	přímý	k2eAgNnSc6d1
ohrožení	ohrožení	k1gNnSc6
života	život	k1gInSc2
nebo	nebo	k8xC
i	i	k9
pacienti	pacient	k1gMnPc1
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgNnPc2
selhání	selhání	k1gNnPc2
základních	základní	k2eAgFnPc2d1
životních	životní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
hrozí	hrozit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Specifikem	specifikon	k1gNnSc7
stanice	stanice	k1gFnSc2
je	být	k5eAaImIp3nS
nepřetržitá	přetržitý	k2eNgFnSc1d1
monitorace	monitorace	k1gFnSc1
životních	životní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
projevů	projev	k1gInPc2
pacienta	pacient	k1gMnSc2
ať	ať	k8xS,k8xC
už	už	k6eAd1
neinvazivními	invazivní	k2eNgFnPc7d1
<g/>
,	,	kIx,
či	či	k8xC
invazivními	invazivní	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
<g/>
,	,	kIx,
široká	široký	k2eAgFnSc1d1
paleta	paleta	k1gFnSc1
možností	možnost	k1gFnPc2
přístrojové	přístrojový	k2eAgFnSc2d1
a	a	k8xC
farmakologické	farmakologický	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
orgánových	orgánový	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
a	a	k8xC
vysoká	vysoký	k2eAgFnSc1d1
dostupnost	dostupnost	k1gFnSc1
takzvaných	takzvaný	k2eAgMnPc2d1
bed-side	bed-sid	k1gInSc5
(	(	kIx(
<g/>
u	u	k7c2
lůžka	lůžko	k1gNnSc2
<g/>
)	)	kIx)
vyšetřovacích	vyšetřovací	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yIgInPc3,k3yRgInPc3,k3yQgInPc3
jsou	být	k5eAaImIp3nP
získávány	získávat	k5eAaImNgInP
údaje	údaj	k1gInPc1
o	o	k7c6
stavu	stav	k1gInSc6
pacienta	pacient	k1gMnSc2
s	s	k7c7
minimálním	minimální	k2eAgNnSc7d1
zpožděním	zpoždění	k1gNnSc7
oproti	oproti	k7c3
konvenčním	konvenční	k2eAgFnPc3d1
laboratorním	laboratorní	k2eAgFnPc3d1
a	a	k8xC
zobrazovacím	zobrazovací	k2eAgFnPc3d1
metodám	metoda	k1gFnPc3
(	(	kIx(
<g/>
vyšetření	vyšetření	k1gNnSc4
arteriálních	arteriální	k2eAgInPc2d1
plynů	plyn	k1gInPc2
a	a	k8xC
parametrů	parametr	k1gInPc2
acidobazické	acidobazický	k2eAgFnSc2d1
rovnováhy	rovnováha	k1gFnSc2
<g/>
,	,	kIx,
identifikace	identifikace	k1gFnSc2
poruch	porucha	k1gFnPc2
krevní	krevní	k2eAgFnSc2d1
srážlivosti	srážlivost	k1gFnSc2
<g/>
,	,	kIx,
bed-side	bed-sid	k1gInSc5
sonografie	sonografie	k1gFnPc4
a	a	k8xC
mnohé	mnohý	k2eAgFnPc4d1
další	další	k2eAgFnPc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
většina	většina	k1gFnSc1
pacientů	pacient	k1gMnPc2
vyžaduje	vyžadovat	k5eAaImIp3nS
určitou	určitý	k2eAgFnSc4d1
míru	míra	k1gFnSc4
farmakologické	farmakologický	k2eAgFnSc2d1
analgosedace	analgosedace	k1gFnSc2
(	(	kIx(
<g/>
pozitivní	pozitivní	k2eAgNnSc4d1
ovlivnění	ovlivnění	k1gNnSc4
subjektivně	subjektivně	k6eAd1
nepříjemných	příjemný	k2eNgInPc2d1
vjemů	vjem	k1gInPc2
včetně	včetně	k7c2
bolesti	bolest	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
typicky	typicky	k6eAd1
i	i	k9
invazivní	invazivní	k2eAgFnSc4d1
umělou	umělý	k2eAgFnSc4d1
plicní	plicní	k2eAgFnSc4d1
ventilaci	ventilace	k1gFnSc4
<g/>
,	,	kIx,
neustálý	neustálý	k2eAgInSc4d1
dohled	dohled	k1gInSc4
a	a	k8xC
monitoraci	monitorace	k1gFnSc4
<g/>
,	,	kIx,
klade	klást	k5eAaImIp3nS
pobyt	pobyt	k1gInSc1
pacienta	pacient	k1gMnSc2
vysoké	vysoký	k2eAgInPc1d1
nároky	nárok	k1gInPc1
zejména	zejména	k9
na	na	k7c4
nelékařský	lékařský	k2eNgInSc4d1
<g/>
/	/	kIx~
<g/>
ošetřovatelský	ošetřovatelský	k2eAgInSc4d1
personál	personál	k1gInSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc4
erudici	erudice	k1gFnSc4
a	a	k8xC
mnohdy	mnohdy	k6eAd1
i	i	k9
fyzickou	fyzický	k2eAgFnSc4d1
zdatnost	zdatnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
pracovištím	pracoviště	k1gNnPc3
intenzivní	intenzivní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
nižšího	nízký	k2eAgInSc2d2
typu	typ	k1gInSc2
je	být	k5eAaImIp3nS
proto	proto	k8xC
zachováván	zachováván	k2eAgInSc1d1
poměr	poměr	k1gInSc1
sestra	sestra	k1gFnSc1
<g/>
:	:	kIx,
<g/>
pacient	pacient	k1gMnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
lůžkové	lůžkový	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
ARO	ara	k1gMnSc5
je	on	k3xPp3gMnPc4
obvykle	obvykle	k6eAd1
resuscitační	resuscitační	k2eAgInSc1d1
tým	tým	k1gInSc1
(	(	kIx(
<g/>
RT	RT	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
MET	met	k1gInSc1
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
"	"	kIx"
<g/>
medical	medicat	k5eAaPmAgInS
emergency	emergenca	k1gFnSc2
team	team	k1gInSc1
<g/>
"	"	kIx"
reagující	reagující	k2eAgInSc1d1
na	na	k7c4
tísňová	tísňový	k2eAgNnPc4d1
volání	volání	k1gNnPc4
z	z	k7c2
dalších	další	k2eAgNnPc2d1
oddělení	oddělení	k1gNnPc2
zdravotnického	zdravotnický	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
akutnímu	akutní	k2eAgNnSc3d1
zhoršení	zhoršení	k1gNnSc3
stavu	stav	k1gInSc2
pacienta	pacient	k1gMnSc2
hospitalizovaného	hospitalizovaný	k2eAgMnSc2d1
mimo	mimo	k7c4
jednotky	jednotka	k1gFnPc4
intenzivní	intenzivní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
tento	tento	k3xDgInSc4
stav	stav	k1gInSc4
podle	podle	k7c2
předem	předem	k6eAd1
definovaných	definovaný	k2eAgNnPc2d1
kritérií	kritérion	k1gNnPc2
(	(	kIx(
<g/>
kombinace	kombinace	k1gFnSc1
klinického	klinický	k2eAgInSc2d1
nálezu	nález	k1gInSc2
a	a	k8xC
měřených	měřený	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
vitálních	vitální	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
<g/>
)	)	kIx)
splňuje	splňovat	k5eAaImIp3nS
podmínky	podmínka	k1gFnPc4
k	k	k7c3
aktivaci	aktivace	k1gFnSc3
takového	takový	k3xDgInSc2
týmu	tým	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tuto	tento	k3xDgFnSc4
práci	práce	k1gFnSc4
disponují	disponovat	k5eAaBmIp3nP
RT	RT	kA
<g/>
/	/	kIx~
<g/>
MET	met	k1gInSc1
materiálním	materiální	k2eAgNnSc7d1
a	a	k8xC
technickým	technický	k2eAgNnSc7d1
vybavením	vybavení	k1gNnSc7
odpovídajícím	odpovídající	k2eAgNnSc7d1
zhruba	zhruba	k6eAd1
vybavení	vybavení	k1gNnSc4
posádek	posádka	k1gFnPc2
rychlé	rychlý	k2eAgFnSc2d1
lékařské	lékařský	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
<g/>
,	,	kIx,
vyjma	vyjma	k7c2
transportních	transportní	k2eAgInPc2d1
a	a	k8xC
fixačních	fixační	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Anesteziologický	anesteziologický	k2eAgInSc1d1
úsek	úsek	k1gInSc1
</s>
<s>
Anesteziologický	anesteziologický	k2eAgInSc1d1
úsek	úsek	k1gInSc1
je	být	k5eAaImIp3nS
představován	představovat	k5eAaImNgInS
dvoučlennými	dvoučlenný	k2eAgInPc7d1
anesteziologickými	anesteziologický	k2eAgInPc7d1
týmy	tým	k1gInPc7
ve	v	k7c6
složení	složení	k1gNnSc6
lékař	lékař	k1gMnSc1
<g/>
+	+	kIx~
<g/>
sestra	sestra	k1gFnSc1
poskytujícími	poskytující	k2eAgFnPc7d1
anestezii	anestezie	k1gFnSc4
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
celkovou	celkový	k2eAgFnSc4d1
nebo	nebo	k8xC
regionální	regionální	k2eAgFnSc4d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
analgosedaci	analgosedace	k1gFnSc4
u	u	k7c2
operačních	operační	k2eAgInPc2d1
a	a	k8xC
diagnostických	diagnostický	k2eAgInPc2d1
výkonů	výkon	k1gInPc2
akutních	akutní	k2eAgInPc2d1
i	i	k8xC
plánovaných	plánovaný	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatou	podstata	k1gFnSc7
práce	práce	k1gFnSc2
anesteziologických	anesteziologický	k2eAgInPc2d1
týmů	tým	k1gInPc2
je	být	k5eAaImIp3nS
nepřetržitá	přetržitý	k2eNgFnSc1d1
monitorace	monitorace	k1gFnSc1
životních	životní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
během	během	k7c2
operačního	operační	k2eAgInSc2d1
<g/>
/	/	kIx~
<g/>
diagnostického	diagnostický	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
<g/>
,	,	kIx,
adekvátní	adekvátní	k2eAgFnSc2d1
reakce	reakce	k1gFnSc2
na	na	k7c4
registrované	registrovaný	k2eAgFnPc4d1
změny	změna	k1gFnPc4
<g/>
,	,	kIx,
zajištění	zajištění	k1gNnSc4
bezbolestnosti	bezbolestnost	k1gFnSc2
a	a	k8xC
eliminace	eliminace	k1gFnSc2
subjektivně	subjektivně	k6eAd1
nepříjemných	příjemný	k2eNgInPc2d1
vjemů	vjem	k1gInPc2
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
aplikací	aplikace	k1gFnSc7
tzv.	tzv.	kA
doplňované	doplňovaný	k2eAgFnSc2d1
celkové	celkový	k2eAgFnSc2d1
anestezie	anestezie	k1gFnSc2
obvykle	obvykle	k6eAd1
s	s	k7c7
invazivním	invazivní	k2eAgNnSc7d1
zajištěním	zajištění	k1gNnSc7
dýchacích	dýchací	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
a	a	k8xC
umělou	umělý	k2eAgFnSc7d1
plicní	plicní	k2eAgFnSc7d1
ventilací	ventilace	k1gFnSc7
<g/>
,	,	kIx,
či	či	k8xC
aplikací	aplikace	k1gFnSc7
anestezie	anestezie	k1gFnSc2
svodné	svodný	k2eAgFnSc2d1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
ovlivňovány	ovlivňován	k2eAgInPc4d1
pouze	pouze	k6eAd1
některé	některý	k3yIgFnPc4
části	část	k1gFnPc4
nervového	nervový	k2eAgInSc2d1
systému	systém	k1gInSc2
zavedením	zavedení	k1gNnSc7
tzv.	tzv.	kA
neuroaxiálních	uroaxiální	k2eNgFnPc2d1
technik	technika	k1gFnPc2
(	(	kIx(
<g/>
spinální	spinální	k2eAgFnSc2d1
a	a	k8xC
epidurální	epidurální	k2eAgFnSc2d1
anestezie	anestezie	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
technik	technika	k1gFnPc2
lokálních	lokální	k2eAgInPc2d1
(	(	kIx(
<g/>
ovlivnění	ovlivnění	k1gNnSc1
konkrétního	konkrétní	k2eAgInSc2d1
nervu	nerv	k1gInSc2
či	či	k8xC
jejich	jejich	k3xOp3gInPc2
svazků	svazek	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práce	práce	k1gFnSc1
anesteziologického	anesteziologický	k2eAgInSc2d1
týmu	tým	k1gInSc2
má	mít	k5eAaImIp3nS
významný	významný	k2eAgInSc4d1
přesah	přesah	k1gInSc4
do	do	k7c2
pooperační	pooperační	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
monitoraci	monitorace	k1gFnSc4
životních	životní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
do	do	k7c2
spolehlivého	spolehlivý	k2eAgNnSc2d1
odeznění	odeznění	k1gNnSc2
účinků	účinek	k1gInPc2
podaných	podaný	k2eAgNnPc2d1
anestetik	anestetikum	k1gNnPc2
na	na	k7c6
tzv.	tzv.	kA
zotavovacím	zotavovací	k2eAgInSc6d1
pokoji	pokoj	k1gInSc6
<g/>
,	,	kIx,
preskripce	preskripce	k1gFnSc1
léků	lék	k1gInPc2
proti	proti	k7c3
bolesti	bolest	k1gFnSc3
pro	pro	k7c4
bezprostřední	bezprostřední	k2eAgNnPc4d1
pooperační	pooperační	k2eAgNnPc4d1
období	období	k1gNnPc4
a	a	k8xC
další	další	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
tzv.	tzv.	kA
anesteziologický	anesteziologický	k2eAgInSc1d1
dohled	dohled	k1gInSc1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
vyžádaná	vyžádaný	k2eAgFnSc1d1
přítomnost	přítomnost	k1gFnSc1
anesteziologického	anesteziologický	k2eAgInSc2d1
týmu	tým	k1gInSc2
v	v	k7c6
případě	případ	k1gInSc6
diagnostického	diagnostický	k2eAgInSc2d1
či	či	k8xC
operačního	operační	k2eAgInSc2d1
výkonu	výkon	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
primárně	primárně	k6eAd1
svou	svůj	k3xOyFgFnSc7
povahou	povaha	k1gFnSc7
sice	sice	k8xC
sofistikovanou	sofistikovaný	k2eAgFnSc4d1
anesteziologickou	anesteziologický	k2eAgFnSc4d1
péči	péče	k1gFnSc4
nevyžaduje	vyžadovat	k5eNaImIp3nS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
pacient	pacient	k1gMnSc1
vykazuje	vykazovat	k5eAaImIp3nS
známky	známka	k1gFnPc4
hrozící	hrozící	k2eAgFnSc2d1
nestability	nestabilita	k1gFnSc2
životních	životní	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
u	u	k7c2
něj	on	k3xPp3gMnSc2
identifikována	identifikován	k2eAgNnPc4d1
jiná	jiný	k2eAgNnPc4d1
rizika	riziko	k1gNnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
nejasné	jasný	k2eNgInPc1d1
anamnestické	anamnestický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
o	o	k7c6
alergiích	alergie	k1gFnPc6
(	(	kIx(
<g/>
např.	např.	kA
na	na	k7c4
kontrastní	kontrastní	k2eAgFnPc4d1
látky	látka	k1gFnPc4
k	k	k7c3
zobrazovacím	zobrazovací	k2eAgNnPc3d1
vyšetřením	vyšetření	k1gNnPc3
<g/>
)	)	kIx)
apod.	apod.	kA
</s>
<s>
Anesteziologická	anesteziologický	k2eAgFnSc1d1
ambulance	ambulance	k1gFnSc1
</s>
<s>
Anesteziologická	anesteziologický	k2eAgFnSc1d1
ambulance	ambulance	k1gFnSc1
je	být	k5eAaImIp3nS
ambulantní	ambulantní	k2eAgNnPc4d1
pracoviště	pracoviště	k1gNnPc4
obsluhované	obsluhovaný	k2eAgFnSc2d1
lékařem	lékař	k1gMnSc7
a	a	k8xC
sestrou	sestra	k1gFnSc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
provádějí	provádět	k5eAaImIp3nP
tzv.	tzv.	kA
předanestetické	předanestetický	k2eAgNnSc1d1
vyšetření	vyšetření	k1gNnSc1
u	u	k7c2
pacientů	pacient	k1gMnPc2
plánovaných	plánovaný	k2eAgMnPc2d1
k	k	k7c3
operačnímu	operační	k2eAgInSc3d1
<g/>
/	/	kIx~
<g/>
diagnostickému	diagnostický	k2eAgInSc3d1
výkonu	výkon	k1gInSc3
s	s	k7c7
potřebou	potřeba	k1gFnSc7
sofistikované	sofistikovaný	k2eAgFnSc2d1
anesteziologické	anesteziologický	k2eAgFnSc2d1
péče	péče	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
podkladě	podklad	k1gInSc6
předem	předem	k6eAd1
zpracovaného	zpracovaný	k2eAgNnSc2d1
interního	interní	k2eAgNnSc2d1
vyšetření	vyšetření	k1gNnSc2
<g/>
,	,	kIx,
laboratorních	laboratorní	k2eAgInPc2d1
výstupů	výstup	k1gInPc2
a	a	k8xC
popisu	popis	k1gInSc2
předepsaných	předepsaný	k2eAgNnPc2d1
zobrazovacích	zobrazovací	k2eAgNnPc2d1
vyšetření	vyšetření	k1gNnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
písemný	písemný	k2eAgInSc4d1
závěr	závěr	k1gInSc4
si	se	k3xPyFc3
pacient	pacient	k1gMnSc1
přináší	přinášet	k5eAaImIp3nS
do	do	k7c2
ambulance	ambulance	k1gFnSc2
s	s	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
s	s	k7c7
časovým	časový	k2eAgInSc7d1
předstihem	předstih	k1gInSc7
identifikována	identifikován	k2eAgNnPc4d1
anesteziologická	anesteziologický	k2eAgNnPc4d1
rizika	riziko	k1gNnPc4
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
podstupovaný	podstupovaný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
<g/>
,	,	kIx,
předpokládaný	předpokládaný	k2eAgInSc4d1
typ	typ	k1gInSc4
anestezie	anestezie	k1gFnSc2
a	a	k8xC
anatomická	anatomický	k2eAgFnSc1d1
specifika	specifika	k1gFnSc1
pacienta	pacient	k1gMnSc2
(	(	kIx(
<g/>
anomálie	anomálie	k1gFnSc1
dýchacích	dýchací	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
<g/>
,	,	kIx,
patologie	patologie	k1gFnSc2
páteře	páteř	k1gFnSc2
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
zjištěných	zjištěný	k2eAgInPc2d1
údajů	údaj	k1gInPc2
<g/>
,	,	kIx,
po	po	k7c6
pohovoru	pohovor	k1gInSc6
s	s	k7c7
pacientem	pacient	k1gMnSc7
a	a	k8xC
jeho	jeho	k3xOp3gNnSc6
specifickém	specifický	k2eAgNnSc6d1
vyšetření	vyšetření	k1gNnSc6
jsou	být	k5eAaImIp3nP
stanoveny	stanovit	k5eAaPmNgInP
optimální	optimální	k2eAgInPc1d1
postupy	postup	k1gInPc1
předoperační	předoperační	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
a	a	k8xC
vedení	vedení	k1gNnSc2
anestezie	anestezie	k1gFnSc2
<g/>
,	,	kIx,
nezřídka	nezřídka	k6eAd1
je	být	k5eAaImIp3nS
však	však	k9
také	také	k9
vznesen	vznesen	k2eAgInSc1d1
požadavek	požadavek	k1gInSc1
na	na	k7c4
náročnější	náročný	k2eAgFnSc4d2
optimalizaci	optimalizace	k1gFnSc4
stavu	stav	k1gInSc2
či	či	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
došetření	došetření	k1gNnSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
příprava	příprava	k1gFnSc1
nebo	nebo	k8xC
aktuální	aktuální	k2eAgInSc1d1
stav	stav	k1gInSc1
pacienta	pacient	k1gMnSc2
k	k	k7c3
plánovanému	plánovaný	k2eAgInSc3d1
výkonu	výkon	k1gInSc3
optimální	optimální	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
s	s	k7c7
cílem	cíl	k1gInSc7
maximálně	maximálně	k6eAd1
snížit	snížit	k5eAaPmF
riziko	riziko	k1gNnSc4
komplikací	komplikace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
by	by	kYmCp3nP
s	s	k7c7
operačním	operační	k2eAgInSc7d1
<g/>
/	/	kIx~
<g/>
diagnostickým	diagnostický	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
a	a	k8xC
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
poskytnutou	poskytnutý	k2eAgFnSc7d1
anesteziologickou	anesteziologický	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
mohly	moct	k5eAaImAgFnP
nastat	nastat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Ambulance	ambulance	k1gFnSc1
bolesti	bolest	k1gFnSc2
</s>
<s>
Ambulance	ambulance	k1gFnSc1
bolesti	bolest	k1gFnSc2
je	být	k5eAaImIp3nS
specifickým	specifický	k2eAgNnSc7d1
ambulantním	ambulantní	k2eAgNnSc7d1
pracovištěm	pracoviště	k1gNnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
ambulantní	ambulantní	k2eAgFnSc4d1
péči	péče	k1gFnSc4
poskytuje	poskytovat	k5eAaImIp3nS
anesteziolog	anesteziolog	k1gMnSc1
s	s	k7c7
nástavbovou	nástavbový	k2eAgFnSc7d1
specializací	specializace	k1gFnSc7
v	v	k7c6
oboru	obor	k1gInSc6
Algeziologie	Algeziologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ambulantní	ambulantní	k2eAgFnSc1d1
péče	péče	k1gFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
poskytována	poskytovat	k5eAaImNgFnS
pacientům	pacient	k1gMnPc3
strádajícím	strádající	k2eAgInSc7d1
chronickou	chronický	k2eAgFnSc7d1
bolestí	bolest	k1gFnSc7
trvající	trvající	k2eAgInSc1d1
déle	dlouho	k6eAd2
než	než	k8xS
3-6	3-6	k4
měsíců	měsíc	k1gInPc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
příčina	příčina	k1gFnSc1
je	být	k5eAaImIp3nS
diagnostikována	diagnostikovat	k5eAaBmNgFnS
<g/>
,	,	kIx,
ale	ale	k8xC
obtížně	obtížně	k6eAd1
řešitelná	řešitelný	k2eAgFnSc1d1
nebo	nebo	k8xC
vůbec	vůbec	k9
neovlivnitelná	ovlivnitelný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
bolesti	bolest	k1gFnPc4
zad	záda	k1gNnPc2
<g/>
,	,	kIx,
kloubů	kloub	k1gInPc2
<g/>
,	,	kIx,
hlavy	hlava	k1gFnPc4
<g/>
,	,	kIx,
neuropatické	neuropatický	k2eAgFnPc4d1
bolesti	bolest	k1gFnPc4
nebo	nebo	k8xC
bolestivé	bolestivý	k2eAgInPc4d1
stavy	stav	k1gInPc4
doprovázející	doprovázející	k2eAgFnSc1d1
pokročilá	pokročilý	k2eAgFnSc1d1
stadia	stadion	k1gNnPc4
onkologických	onkologický	k2eAgNnPc2d1
onemocnění	onemocnění	k1gNnPc2
<g/>
.	.	kIx.
</s>
