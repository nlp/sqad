<p>
<s>
Program	program	k1gInSc1	program
Pioneer	Pioneer	kA	Pioneer
byl	být	k5eAaImAgInS	být
americký	americký	k2eAgInSc1d1	americký
program	program	k1gInSc1	program
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
obřích	obří	k2eAgFnPc2d1	obří
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
realizovaný	realizovaný	k2eAgInSc4d1	realizovaný
v	v	k7c6	v
letech	let	k1gInPc6	let
1958	[number]	k4	1958
–	–	k?	–
1978	[number]	k4	1978
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obecné	obecný	k2eAgInPc1d1	obecný
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
série	série	k1gFnSc1	série
se	se	k3xPyFc4	se
sestávala	sestávat	k5eAaImAgFnS	sestávat
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
družic	družice	k1gFnPc2	družice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
určeny	určit	k5eAaPmNgFnP	určit
k	k	k7c3	k
průzkumu	průzkum	k1gInSc3	průzkum
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
nedokonalá	dokonalý	k2eNgFnSc1d1	nedokonalá
technická	technický	k2eAgFnSc1d1	technická
úroveň	úroveň	k1gFnSc1	úroveň
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
program	program	k1gInSc4	program
splnily	splnit	k5eAaPmAgInP	splnit
jen	jen	k9	jen
některé	některý	k3yIgInPc1	některý
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
Měsíce	měsíc	k1gInSc2	měsíc
pak	pak	k6eAd1	pak
převzal	převzít	k5eAaPmAgInS	převzít
program	program	k1gInSc1	program
Ranger	Rangero	k1gNnPc2	Rangero
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
letem	let	k1gInSc7	let
Pioneer	Pioneer	kA	Pioneer
5	[number]	k4	5
byly	být	k5eAaImAgFnP	být
mise	mise	k1gFnPc1	mise
na	na	k7c6	na
podstatně	podstatně	k6eAd1	podstatně
vyšší	vysoký	k2eAgFnSc3d2	vyšší
technické	technický	k2eAgFnSc3d1	technická
úrovni	úroveň	k1gFnSc3	úroveň
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
průzkum	průzkum	k1gInSc4	průzkum
meziplanetárního	meziplanetární	k2eAgInSc2d1	meziplanetární
prostoru	prostor	k1gInSc2	prostor
v	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
družice	družice	k1gFnSc1	družice
Helios	Helios	k1gInSc1	Helios
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
i	i	k9	i
planet	planeta	k1gFnPc2	planeta
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
,	,	kIx,	,
Saturn	Saturn	k1gMnSc1	Saturn
a	a	k8xC	a
Venuše	Venuše	k1gFnSc1	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
programu	program	k1gInSc2	program
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
nutno	nutno	k6eAd1	nutno
připomenout	připomenout	k5eAaPmF	připomenout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
se	se	k3xPyFc4	se
nezdařil	zdařit	k5eNaPmAgInS	zdařit
let	let	k1gInSc1	let
dvou	dva	k4xCgFnPc2	dva
sond	sonda	k1gFnPc2	sonda
neuvedených	uvedený	k2eNgFnPc2d1	neuvedená
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
COSPAR	COSPAR	kA	COSPAR
<g/>
,	,	kIx,	,
pojmenovaných	pojmenovaný	k2eAgInPc2d1	pojmenovaný
Pioneer	Pioneer	kA	Pioneer
P30	P30	k1gMnSc7	P30
a	a	k8xC	a
P	P	kA	P
<g/>
31	[number]	k4	31
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nosné	nosný	k2eAgFnPc1d1	nosná
rakety	raketa	k1gFnPc1	raketa
Atlas	Atlas	k1gInSc1	Atlas
Able	Abl	k1gFnSc2	Abl
po	po	k7c6	po
startu	start	k1gInSc6	start
selhaly	selhat	k5eAaPmAgFnP	selhat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sondy	sonda	k1gFnPc4	sonda
sestrojila	sestrojit	k5eAaPmAgFnS	sestrojit
laboratoř	laboratoř	k1gFnSc1	laboratoř
JPL	JPL	kA	JPL
a	a	k8xC	a
provozovala	provozovat	k5eAaImAgFnS	provozovat
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
nově	nově	k6eAd1	nově
utvořená	utvořený	k2eAgFnSc1d1	utvořená
agentura	agentura	k1gFnSc1	agentura
NASA	NASA	kA	NASA
<g/>
,	,	kIx,	,
obojí	obojí	k4xRgInSc1	obojí
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
první	první	k4xOgMnSc1	první
vedoucí	vedoucí	k1gMnSc1	vedoucí
NASA	NASA	kA	NASA
Keith	Keith	k1gMnSc1	Keith
Glenan	Glenan	k1gMnSc1	Glenan
projekt	projekt	k1gInSc4	projekt
ponechal	ponechat	k5eAaPmAgMnS	ponechat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
ARPA	ARPA	kA	ARPA
a	a	k8xC	a
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
družic	družice	k1gFnPc2	družice
==	==	k?	==
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
0	[number]	k4	0
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
havárie	havárie	k1gFnSc1	havárie
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
1	[number]	k4	1
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
nezdařený	zdařený	k2eNgInSc1d1	nezdařený
start	start	k1gInSc1	start
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
nad	nad	k7c7	nad
Zemí	zem	k1gFnSc7	zem
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
2	[number]	k4	2
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
nezdařený	zdařený	k2eNgInSc1d1	nezdařený
start	start	k1gInSc1	start
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
nad	nad	k7c7	nad
Zemí	zem	k1gFnSc7	zem
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
3	[number]	k4	3
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
nedoletěla	doletět	k5eNaPmAgFnS	doletět
a	a	k8xC	a
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
nad	nad	k7c7	nad
Zemí	zem	k1gFnSc7	zem
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
4	[number]	k4	4
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
proletěla	proletět	k5eAaPmAgFnS	proletět
kolem	kolem	k7c2	kolem
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
um	um	k1gInSc1	um
<g/>
.	.	kIx.	.
<g/>
planetka	planetka	k1gFnSc1	planetka
Slunce	slunce	k1gNnSc2	slunce
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
P-	P-	k1gFnSc1	P-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
havárie	havárie	k1gFnSc1	havárie
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
startem	start	k1gInSc7	start
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
P-	P-	k1gFnSc1	P-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
26	[number]	k4	26
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
havárie	havárie	k1gFnSc1	havárie
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
těsně	těsně	k6eAd1	těsně
po	po	k7c6	po
startu	start	k1gInSc6	start
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
5	[number]	k4	5
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
mezi	mezi	k7c7	mezi
Zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
Venuši	Venuše	k1gFnSc4	Venuše
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
11	[number]	k4	11
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
mise	mise	k1gFnSc1	mise
<g/>
,	,	kIx,	,
umělá	umělý	k2eAgFnSc1d1	umělá
planetka	planetka	k1gFnSc1	planetka
Slunce	slunce	k1gNnSc2	slunce
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
P-	P-	k1gFnSc1	P-
<g/>
30	[number]	k4	30
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
selhání	selhání	k1gNnSc1	selhání
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
<g/>
,	,	kIx,	,
spadla	spadnout	k5eAaPmAgFnS	spadnout
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
P-	P-	k1gFnSc1	P-
<g/>
31	[number]	k4	31
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Měsíci	měsíc	k1gInSc3	měsíc
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
havárie	havárie	k1gFnSc1	havárie
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
<g/>
,	,	kIx,	,
spadla	spadnout	k5eAaPmAgFnS	spadnout
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
6	[number]	k4	6
<g/>
,	,	kIx,	,
oběžnice	oběžnice	k1gFnSc1	oběžnice
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
start	start	k1gInSc1	start
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgInSc4d1	poslední
kontakt	kontakt	k1gInSc4	kontakt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
7	[number]	k4	7
<g/>
,	,	kIx,	,
oběžnice	oběžnice	k1gFnSc1	oběžnice
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
start	start	k1gInSc1	start
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
částečně	částečně	k6eAd1	částečně
funkční	funkční	k2eAgFnPc1d1	funkční
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
8	[number]	k4	8
<g/>
,	,	kIx,	,
oběžnice	oběžnice	k1gFnSc1	oběžnice
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
start	start	k1gInSc1	start
13	[number]	k4	13
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
částečně	částečně	k6eAd1	částečně
funkční	funkční	k2eAgFnPc1d1	funkční
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
9	[number]	k4	9
<g/>
,	,	kIx,	,
oběžnice	oběžnice	k1gFnSc1	oběžnice
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
start	start	k1gInSc1	start
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
funkční	funkční	k2eAgNnSc1d1	funkční
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
E	E	kA	E
<g/>
,	,	kIx,	,
oběžnice	oběžnice	k1gFnSc1	oběžnice
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
start	start	k1gInSc1	start
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
havárie	havárie	k1gFnSc1	havárie
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
10	[number]	k4	10
<g/>
,	,	kIx,	,
sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
a	a	k8xC	a
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
průzkum	průzkum	k1gInSc4	průzkum
Jupiteru	Jupiter	k1gInSc2	Jupiter
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
11	[number]	k4	11
<g/>
,	,	kIx,	,
Sonda	sonda	k1gFnSc1	sonda
k	k	k7c3	k
Jupiteru	Jupiter	k1gInSc3	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc3	Saturn
<g/>
,	,	kIx,	,
start	start	k1gInSc4	start
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
průzkum	průzkum	k1gInSc1	průzkum
u	u	k7c2	u
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
</s>
</p>
<p>
<s>
Pioneer	Pioneer	kA	Pioneer
H	H	kA	H
<g/>
,	,	kIx,	,
sesterská	sesterský	k2eAgFnSc1d1	sesterská
sonda	sonda	k1gFnSc1	sonda
Pioneeru	Pioneer	k1gInSc2	Pioneer
10	[number]	k4	10
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
</s>
</p>
<p>
<s>
Pioneer-Venus	Pioneer-Venus	k1gInSc1	Pioneer-Venus
1	[number]	k4	1
<g/>
,	,	kIx,	,
start	start	k1gInSc1	start
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
umělá	umělý	k2eAgFnSc1d1	umělá
družice	družice	k1gFnSc1	družice
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
pracovala	pracovat	k5eAaImAgFnS	pracovat
14	[number]	k4	14
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
Pioneer-Venus	Pioneer-Venus	k1gInSc1	Pioneer-Venus
2	[number]	k4	2
<g/>
,	,	kIx,	,
start	start	k1gInSc1	start
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
výzkum	výzkum	k1gInSc1	výzkum
Venuše	Venuše	k1gFnSc2	Venuše
několika	několik	k4yIc2	několik
paralelními	paralelní	k2eAgFnPc7d1	paralelní
sondami	sonda	k1gFnPc7	sonda
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Program	program	k1gInSc1	program
Pioneer	Pioneer	kA	Pioneer
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://mek.kosmo.cz/sondy/usa/pioneer/pioneer.htm	[url]	k6eAd1	http://mek.kosmo.cz/sondy/usa/pioneer/pioneer.htm
</s>
</p>
<p>
<s>
http://spaceprobes.kosmo.cz/index.php?cid=17	[url]	k4	http://spaceprobes.kosmo.cz/index.php?cid=17
</s>
</p>
