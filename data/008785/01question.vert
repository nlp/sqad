<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
americký	americký	k2eAgInSc1d1	americký
program	program	k1gInSc1	program
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
obřích	obří	k2eAgFnPc2d1	obří
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
realizovaný	realizovaný	k2eAgInSc4d1	realizovaný
v	v	k7c6	v
letech	let	k1gInPc6	let
1958	[number]	k4	1958
–	–	k?	–
1978	[number]	k4	1978
<g/>
?	?	kIx.	?
</s>
