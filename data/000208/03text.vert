<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
populárně-vědecký	populárněědecký	k2eAgInSc1d1	populárně-vědecký
časopis	časopis	k1gInSc1	časopis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
RF-Hobby	RF-Hobba	k1gFnSc2	RF-Hobba
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
měsíčník	měsíčník	k1gInSc4	měsíčník
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
klasických	klasický	k2eAgNnPc2d1	klasické
čísel	číslo	k1gNnPc2	číslo
vycházejí	vycházet	k5eAaImIp3nP	vycházet
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
i	i	k8xC	i
dvě	dva	k4xCgNnPc4	dva
speciální	speciální	k2eAgNnPc4d1	speciální
vydání	vydání	k1gNnPc4	vydání
s	s	k7c7	s
názvem	název	k1gInSc7	název
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Extra	extra	k6eAd1	extra
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejčtenějším	čtený	k2eAgNnPc3d3	nejčtenější
tuzemským	tuzemský	k2eAgNnPc3d1	tuzemské
periodikům	periodikum	k1gNnPc3	periodikum
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
čtenost	čtenost	k1gFnSc1	čtenost
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
Mediaprojektu	Mediaprojekt	k1gInSc2	Mediaprojekt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
kolem	kolem	k7c2	kolem
266	[number]	k4	266
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
prodaný	prodaný	k2eAgInSc1d1	prodaný
náklad	náklad	k1gInSc1	náklad
vycházel	vycházet	k5eAaImAgInS	vycházet
na	na	k7c4	na
cca	cca	kA	cca
32	[number]	k4	32
tisíc	tisíc	k4xCgInSc1	tisíc
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
mutace	mutace	k1gFnSc1	mutace
vychází	vycházet	k5eAaImIp3nS	vycházet
také	také	k9	také
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
PhDr.	PhDr.	kA	PhDr.
Pavel	Pavel	k1gMnSc1	Pavel
Šmejkal	Šmejkal	k1gMnSc1	Šmejkal
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
redakci	redakce	k1gFnSc3	redakce
dále	daleko	k6eAd2	daleko
tvoří	tvořit	k5eAaImIp3nS	tvořit
Mgr.	Mgr.	kA	Mgr.
Martin	Martin	k1gMnSc1	Martin
Janda	Janda	k1gMnSc1	Janda
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
<g/>
,	,	kIx,	,
redaktoři	redaktor	k1gMnPc1	redaktor
ing.	ing.	kA	ing.
Václav	Václav	k1gMnSc1	Václav
Roman	Roman	k1gMnSc1	Roman
<g/>
,	,	kIx,	,
bc.	bc.	k?	bc.
Mirka	Mirka	k1gFnSc1	Mirka
Filipová	Filipová	k1gFnSc1	Filipová
a	a	k8xC	a
bc.	bc.	k?	bc.
Pavel	Pavel	k1gMnSc1	Pavel
Škopek	Škopek	k1gMnSc1	Škopek
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
obrazový	obrazový	k2eAgMnSc1d1	obrazový
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
,	,	kIx,	,
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
Mgr.	Mgr.	kA	Mgr.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Pfeifer	Pfeifer	k1gMnSc1	Pfeifer
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
soutěžní	soutěžní	k2eAgFnSc6d1	soutěžní
přehlídce	přehlídka	k1gFnSc6	přehlídka
popularizace	popularizace	k1gFnSc2	popularizace
vědy	věda	k1gFnSc2	věda
SCIAP	SCIAP	kA	SCIAP
2011	[number]	k4	2011
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
periodikum	periodikum	k1gNnSc1	periodikum
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
soutěžní	soutěžní	k2eAgFnSc6d1	soutěžní
přehlídce	přehlídka	k1gFnSc6	přehlídka
získal	získat	k5eAaPmAgInS	získat
časopis	časopis	k1gInSc1	časopis
třetí	třetí	k4xOgInSc1	třetí
místo	místo	k6eAd1	místo
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
