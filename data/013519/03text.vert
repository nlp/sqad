<s>
Irbis	Irbis	k1gFnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Sněžný	sněžný	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
dalších	další	k2eAgInPc6d1
významech	význam	k1gInPc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Sněžný	sněžný	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Irbis	Irbis	k1gFnSc1
Irbis	Irbis	k1gFnSc2
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
zranitelný	zranitelný	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
šelmy	šelma	k1gFnPc1
(	(	kIx(
<g/>
Carnivora	Carnivora	k1gFnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
kočkovití	kočkovití	k1gMnPc1
(	(	kIx(
<g/>
Felidae	Felidae	k1gInSc1
<g/>
)	)	kIx)
Podčeleď	podčeleď	k1gFnSc1
</s>
<s>
velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
(	(	kIx(
<g/>
Pantherinae	Pantherinae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
Panthera	Panthera	k1gFnSc1
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc4
</s>
<s>
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Schreber	Schreber	k1gMnSc1
<g/>
,	,	kIx,
1775	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
hnědě	hnědě	k6eAd1
-	-	kIx~
jisté	jistý	k2eAgFnPc4d1
<g/>
,	,	kIx,
okrově	okrově	k6eAd1
-	-	kIx~
možné	možný	k2eAgNnSc1d1
Synonyma	synonymum	k1gNnPc4
</s>
<s>
Felis	Felis	k1gFnSc1
uncia	uncia	k1gFnSc1
(	(	kIx(
<g/>
Schreber	Schreber	k1gMnSc1
<g/>
,	,	kIx,
1775	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Felis	Felis	k1gFnSc1
irbis	irbis	k1gFnSc1
(	(	kIx(
<g/>
Ehrenberg	Ehrenberg	k1gMnSc1
<g/>
,	,	kIx,
1830	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Felis	Felis	k1gFnSc1
uncioides	uncioides	k1gMnSc1
(	(	kIx(
<g/>
Horsfield	Horsfield	k1gMnSc1
<g/>
,	,	kIx,
1855	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Uncia	uncia	k1gFnSc1
uncia	uncia	k1gFnSc1
(	(	kIx(
<g/>
Pocock	Pocock	k1gMnSc1
<g/>
,	,	kIx,
1916	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Irbis	Irbis	k1gFnSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
také	také	k9
Uncia	uncia	k1gFnSc1
uncia	uncia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
též	též	k9
irbis	irbis	k1gInSc1
horský	horský	k2eAgMnSc1d1
či	či	k8xC
levhart	levhart	k1gMnSc1
sněžný	sněžný	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
kočkovitá	kočkovitý	k2eAgFnSc1d1
šelma	šelma	k1gFnSc1
z	z	k7c2
podčeledi	podčeleď	k1gFnSc2
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
obývající	obývající	k2eAgInPc4d1
horské	horský	k2eAgInPc4d1
masivy	masiv	k1gInPc4
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Irbis	Irbis	k1gInSc1
vzhledem	vzhledem	k7c3
připomíná	připomínat	k5eAaImIp3nS
levharta	levhart	k1gMnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
o	o	k7c4
něco	něco	k3yInSc4
menší	malý	k2eAgFnPc1d2
<g/>
,	,	kIx,
ale	ale	k8xC
robustnější	robustní	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
těla	tělo	k1gNnSc2
je	být	k5eAaImIp3nS
103	#num#	k4
<g/>
–	–	k?
<g/>
130	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
ocasu	ocas	k1gInSc2
90	#num#	k4
<g/>
–	–	k?
<g/>
105	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výška	výška	k1gFnSc1
v	v	k7c6
plecích	plec	k1gFnPc6
dosahuje	dosahovat	k5eAaImIp3nS
kolem	kolem	k7c2
60	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
váha	váha	k1gFnSc1
22	#num#	k4
<g/>
–	–	k?
<g/>
55	#num#	k4
kg	kg	kA
(	(	kIx(
<g/>
samci	samec	k1gMnPc1
jsou	být	k5eAaImIp3nP
o	o	k7c4
něco	něco	k3yInSc4
mohutnější	mohutný	k2eAgFnPc1d2
než	než	k8xS
samice	samice	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srst	srst	k1gFnSc1
je	být	k5eAaImIp3nS
měkká	měkký	k2eAgFnSc1d1
<g/>
,	,	kIx,
hustá	hustý	k2eAgFnSc1d1
(	(	kIx(
<g/>
kolem	kolem	k7c2
4000	#num#	k4
chlupů	chlup	k1gInPc2
na	na	k7c4
cm²	cm²	k?
<g/>
)	)	kIx)
a	a	k8xC
obzvláště	obzvláště	k6eAd1
v	v	k7c6
zimě	zima	k1gFnSc6
velmi	velmi	k6eAd1
dlouhá	dlouhý	k2eAgFnSc1d1
(	(	kIx(
<g/>
na	na	k7c6
břiše	břicho	k1gNnSc6
až	až	k9
120	#num#	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základním	základní	k2eAgNnSc6d1
světlém	světlý	k2eAgNnSc6d1
zbarvení	zbarvení	k1gNnSc6
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgFnP
tmavé	tmavý	k2eAgFnPc1d1
skvrny	skvrna	k1gFnPc1
a	a	k8xC
rozety	rozeta	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhý	dlouhý	k2eAgInSc4d1
a	a	k8xC
mohutný	mohutný	k2eAgInSc4d1
ocas	ocas	k1gInSc4
pomáhá	pomáhat	k5eAaImIp3nS
irbisovi	irbis	k1gMnSc3
udržovat	udržovat	k5eAaImF
rovnováhu	rovnováha	k1gFnSc4
při	při	k7c6
pohybu	pohyb	k1gInSc6
a	a	k8xC
skocích	skok	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kořistí	kořistit	k5eAaImIp3nS
irbisa	irbisa	k1gFnSc1
jsou	být	k5eAaImIp3nP
především	především	k6eAd1
horští	horský	k2eAgMnPc1d1
kopytníci	kopytník	k1gMnPc1
<g/>
,	,	kIx,
příležitostně	příležitostně	k6eAd1
však	však	k9
loví	lovit	k5eAaImIp3nP
i	i	k9
menší	malý	k2eAgNnPc1d2
zvířata	zvíře	k1gNnPc1
jako	jako	k8xS,k8xC
hlodavce	hlodavec	k1gMnSc4
<g/>
,	,	kIx,
zajícovce	zajícovec	k1gMnPc4
či	či	k8xC
hrabavé	hrabavý	k2eAgMnPc4d1
ptáky	pták	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
biotopem	biotop	k1gInSc7
jsou	být	k5eAaImIp3nP
především	především	k9
horské	horský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
a	a	k8xC
pastviny	pastvina	k1gFnPc1
<g/>
,	,	kIx,
bezlesé	bezlesý	k2eAgFnPc1d1
a	a	k8xC
kamenité	kamenitý	k2eAgFnPc1d1
planiny	planina	k1gFnPc1
nad	nad	k7c7
horní	horní	k2eAgFnSc7d1
hranicí	hranice	k1gFnSc7
lesa	les	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
1500	#num#	k4
<g/>
–	–	k?
<g/>
4000	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
v	v	k7c6
Himálaji	Himálaj	k1gFnSc6
byl	být	k5eAaImAgInS
zaznamenán	zaznamenat	k5eAaPmNgInS
dokonce	dokonce	k9
až	až	k6eAd1
v	v	k7c6
6000	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
na	na	k7c6
Džungarském	Džungarský	k2eAgMnSc6d1
Alatau	Alata	k1gMnSc6
však	však	k9
také	také	k9
ve	v	k7c6
výškách	výška	k1gFnPc6
pod	pod	k7c4
1000	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
Stav	stav	k1gInSc1
divoce	divoce	k6eAd1
žijící	žijící	k2eAgFnSc2d1
populace	populace	k1gFnSc2
irbisů	irbis	k1gInPc2
lze	lze	k6eAd1
kvůli	kvůli	k7c3
nedostupnosti	nedostupnost	k1gFnSc3
biotopu	biotop	k1gInSc2
monitorovat	monitorovat	k5eAaImF
jen	jen	k9
velmi	velmi	k6eAd1
obtížně	obtížně	k6eAd1
<g/>
,	,	kIx,
různé	různý	k2eAgInPc1d1
odhady	odhad	k1gInPc1
z	z	k7c2
let	léto	k1gNnPc2
2003	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
uvádějí	uvádět	k5eAaImIp3nP
čísla	číslo	k1gNnPc4
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
3920	#num#	k4
<g/>
–	–	k?
<g/>
7500	#num#	k4
divoce	divoce	k6eAd1
žijících	žijící	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Druh	druh	k1gInSc1
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1972	#num#	k4
veden	veden	k2eAgInSc1d1
Mezinárodním	mezinárodní	k2eAgInSc7d1
svazem	svaz	k1gInSc7
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
jako	jako	k8xS,k8xC
ohrožený	ohrožený	k2eAgMnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
byl	být	k5eAaImAgInS
však	však	k9
překlasifikován	překlasifikován	k2eAgInSc1d1
na	na	k7c4
zranitelný	zranitelný	k2eAgInSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
CITES	CITES	kA
je	být	k5eAaImIp3nS
irbis	irbis	k1gInSc1
citován	citován	k2eAgInSc1d1
v	v	k7c6
příloze	příloha	k1gFnSc6
I	i	k9
<g/>
,	,	kIx,
mezi	mezi	k7c4
druhy	druh	k1gInPc4
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgFnPc7
je	být	k5eAaImIp3nS
jakýkoliv	jakýkoliv	k3yIgInSc1
mezinárodní	mezinárodní	k2eAgInSc1d1
obchod	obchod	k1gInSc1
zakázán	zakázat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
v	v	k7c6
celém	celý	k2eAgInSc6d1
areálu	areál	k1gInSc6
výskytu	výskyt	k1gInSc2
je	být	k5eAaImIp3nS
irbis	irbis	k1gInSc1
pod	pod	k7c7
legislativní	legislativní	k2eAgFnSc7d1
ochranou	ochrana	k1gFnSc7
<g/>
,	,	kIx,
největší	veliký	k2eAgNnSc1d3
nebezpečí	nebezpečí	k1gNnSc1
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
stále	stále	k6eAd1
představuje	představovat	k5eAaImIp3nS
ilegální	ilegální	k2eAgInSc4d1
lov	lov	k1gInSc4
a	a	k8xC
pytláctví	pytláctví	k1gNnSc4
spojené	spojený	k2eAgNnSc4d1
s	s	k7c7
výnosným	výnosný	k2eAgInSc7d1
prodejem	prodej	k1gInSc7
trofejí	trofej	k1gFnPc2
na	na	k7c6
černém	černý	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizované	organizovaný	k2eAgInPc1d1
projekty	projekt	k1gInPc1
na	na	k7c4
ochranu	ochrana	k1gFnSc4
irbisa	irbis	k1gMnSc2
začaly	začít	k5eAaPmAgFnP
vznikat	vznikat	k5eAaImF
teprve	teprve	k6eAd1
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
2013	#num#	k4
v	v	k7c6
kyrgyzském	kyrgyzský	k2eAgInSc6d1
Biškeku	Biškek	k1gInSc6
podepsali	podepsat	k5eAaPmAgMnP
zástupci	zástupce	k1gMnPc1
všech	všecek	k3xTgFnPc2
12	#num#	k4
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
jejichž	jejichž	k3xOyRp3gNnSc6
území	území	k1gNnSc6
areál	areál	k1gInSc4
irbisa	irbisa	k1gFnSc1
zasahuje	zasahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
společnou	společný	k2eAgFnSc4d1
deklaraci	deklarace	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
základě	základ	k1gInSc6
vznikl	vzniknout	k5eAaPmAgInS
projekt	projekt	k1gInSc1
Global	globat	k5eAaImAgMnS
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
&	&	k?
Ecosystem	Ecosyst	k1gInSc7
Protection	Protection	k1gInSc1
Program	program	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
do	do	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
vytipovat	vytipovat	k5eAaPmF
napříč	napříč	k6eAd1
areálem	areál	k1gInSc7
výskytu	výskyt	k1gInSc2
nejméně	málo	k6eAd3
20	#num#	k4
zdravých	zdravý	k2eAgFnPc2d1
populací	populace	k1gFnPc2
a	a	k8xC
zajistit	zajistit	k5eAaPmF
jejich	jejich	k3xOp3gFnSc4
ochranu	ochrana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
zajetí	zajetí	k1gNnSc6
se	se	k3xPyFc4
daří	dařit	k5eAaImIp3nS
irbisa	irbisa	k1gFnSc1
úspěšně	úspěšně	k6eAd1
rozmnožovat	rozmnožovat	k5eAaImF
<g/>
,	,	kIx,
populace	populace	k1gFnSc1
v	v	k7c6
zoologických	zoologický	k2eAgFnPc6d1
zahradách	zahrada	k1gFnPc6
čítá	čítat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
500	#num#	k4
jedinců	jedinec	k1gMnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
životaschopnou	životaschopný	k2eAgFnSc4d1
a	a	k8xC
stabilní	stabilní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plemennou	plemenný	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
irbisa	irbis	k1gMnSc2
vede	vést	k5eAaImIp3nS
zoologická	zoologický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
ve	v	k7c6
finských	finský	k2eAgFnPc6d1
Helsinkách	Helsinky	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbis	Irbis	k1gInSc4
není	být	k5eNaImIp3nS
obvyklé	obvyklý	k2eAgNnSc1d1
heraldické	heraldický	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
<g/>
,	,	kIx,
přesto	přesto	k8xC
jej	on	k3xPp3gMnSc4
najdeme	najít	k5eAaPmIp1nP
například	například	k6eAd1
ve	v	k7c6
znaku	znak	k1gInSc6
ruské	ruský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Chakasie	Chakasie	k1gFnSc2
či	či	k8xC
měst	město	k1gNnPc2
Almaty	Almat	k2eAgInPc1d1
a	a	k8xC
Biškek	Biškek	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
rovněž	rovněž	k9
jedním	jeden	k4xCgInSc7
z	z	k7c2
maskotů	maskot	k1gInPc2
Zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
2014	#num#	k4
v	v	k7c6
ruském	ruský	k2eAgNnSc6d1
Soči	Soči	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
Slovo	slovo	k1gNnSc1
„	„	k?
<g/>
irbis	irbis	k1gFnSc1
<g/>
“	“	k?
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
mongolštiny	mongolština	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
češtiny	čeština	k1gFnSc2
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
přes	přes	k7c4
ruštinu	ruština	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
názvů	název	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
uvádí	uvádět	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Svatopluk	Svatopluk	k1gMnSc1
Presl	Presl	k1gInSc4
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
Ssavectvo	Ssavectvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rukověť	rukověť	k1gFnSc1
soustavná	soustavný	k2eAgFnSc1d1
k	k	k7c3
poučení	poučení	k1gNnSc3
vlastnímu	vlastní	k2eAgInSc3d1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1834	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Slovo	slovo	k1gNnSc1
se	se	k3xPyFc4
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
některých	některý	k3yIgInPc2
jiných	jiný	k2eAgInPc2d1
názvů	název	k1gInPc2
navržených	navržený	k2eAgInPc2d1
J.	J.	kA
S.	S.	kA
Preslem	Presl	k1gMnSc7
<g/>
,	,	kIx,
ujalo	ujmout	k5eAaPmAgNnS
a	a	k8xC
jako	jako	k8xS,k8xC
označení	označení	k1gNnSc1
příslušného	příslušný	k2eAgNnSc2d1
zvířete	zvíře	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
češtině	čeština	k1gFnSc6
používá	používat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Název	název	k1gInSc1
„	„	k?
<g/>
sněžný	sněžný	k2eAgMnSc1d1
levhart	levhart	k1gMnSc1
<g/>
“	“	k?
či	či	k8xC
někdy	někdy	k6eAd1
taktéž	taktéž	k?
užívaný	užívaný	k2eAgInSc1d1
„	„	k?
<g/>
sněžný	sněžný	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
<g/>
“	“	k?
poukazuje	poukazovat	k5eAaImIp3nS
na	na	k7c4
vysokohorský	vysokohorský	k2eAgInSc4d1
biotop	biotop	k1gInSc4
a	a	k8xC
podobnost	podobnost	k1gFnSc4
vzhledu	vzhled	k1gInSc2
irbisa	irbis	k1gMnSc2
s	s	k7c7
levhartem	levhart	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
názvem	název	k1gInSc7
je	být	k5eAaImIp3nS
irbis	irbis	k1gInSc1
primárně	primárně	k6eAd1
označován	označovat	k5eAaImNgInS
v	v	k7c6
řadě	řada	k1gFnSc6
jazyků	jazyk	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
anglicky	anglicky	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
„	„	k?
<g/>
snow	snow	k?
leopard	leopard	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
německy	německy	k6eAd1
„	„	k?
<g/>
Schneeleopard	Schneeleopard	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
„	„	k?
<g/>
leopardo	leopardo	k1gNnSc4
delle	delle	k1gFnSc2
nevi	nevi	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
„	„	k?
<g/>
с	с	k?
б	б	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
synonymum	synonymum	k1gNnSc1
názvu	název	k1gInSc2
„	„	k?
<g/>
и	и	k?
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rodové	rodový	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
„	„	k?
<g/>
Panthera	Panther	k1gMnSc2
<g/>
“	“	k?
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
řeckého	řecký	k2eAgInSc2d1
π	π	k?
(	(	kIx(
<g/>
pánthē	pánthē	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
řecký	řecký	k2eAgInSc1d1
název	název	k1gInSc1
pro	pro	k7c4
levharta	levhart	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
typovým	typový	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
je	být	k5eAaImIp3nS
složen	složit	k5eAaPmNgInS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
pan	pan	k1gMnSc1
<g/>
–	–	k?
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
vše	všechen	k3xTgNnSc4
<g/>
“	“	k?
a	a	k8xC
thē	thē	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
kořist	kořist	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
dohromady	dohromady	k6eAd1
tedy	tedy	k8xC
„	„	k?
<g/>
vše	všechen	k3xTgNnSc4
je	být	k5eAaImIp3nS
kořist	kořist	k1gFnSc1
<g/>
“	“	k?
–	–	k?
narážka	narážka	k1gFnSc1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
levhart	levhart	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
příbuzní	příbuzný	k1gMnPc1
jsou	být	k5eAaImIp3nP
vrcholovými	vrcholový	k2eAgMnPc7d1
predátory	predátor	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Latinské	latinský	k2eAgNnSc1d1
označení	označení	k1gNnSc1
druhu	druh	k1gInSc2
(	(	kIx(
<g/>
a	a	k8xC
dříve	dříve	k6eAd2
i	i	k8xC
rodu	rod	k1gInSc2
<g/>
)	)	kIx)
„	„	k?
<g/>
uncia	uncia	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
„	„	k?
<g/>
ounce	ounce	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
synonymum	synonymum	k1gNnSc1
k	k	k7c3
„	„	k?
<g/>
snow	snow	k?
leopard	leopard	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
odvozeno	odvodit	k5eAaPmNgNnS
z	z	k7c2
francouzského	francouzský	k2eAgInSc2d1
„	„	k?
<g/>
once	onc	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původ	původ	k1gInSc1
tohoto	tento	k3xDgNnSc2
slova	slovo	k1gNnSc2
lze	lze	k6eAd1
vysledovat	vysledovat	k5eAaPmF,k5eAaImF
k	k	k7c3
řeckému	řecký	k2eAgNnSc3d1
označení	označení	k1gNnSc3
rysa	rys	k1gMnSc2
ostrovida	ostrovid	k1gMnSc2
λ	λ	k?
(	(	kIx(
<g/>
lunx	lunx	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc1
je	být	k5eAaImIp3nS
odvozeno	odvodit	k5eAaPmNgNnS
z	z	k7c2
indoevropského	indoevropský	k2eAgInSc2d1
kořene	kořen	k1gInSc2
leuk-	leuk-	k?
značícího	značící	k2eAgInSc2d1
„	„	k?
<g/>
světlo	světlo	k1gNnSc1
<g/>
,	,	kIx,
jas	jas	k1gInSc1
<g/>
“	“	k?
–	–	k?
jde	jít	k5eAaImIp3nS
o	o	k7c4
narážku	narážka	k1gFnSc4
na	na	k7c4
zářivé	zářivý	k2eAgFnPc4d1
oči	oko	k1gNnPc4
rysa	rys	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Slovo	slovo	k1gNnSc1
bylo	být	k5eAaImAgNnS
latinizováno	latinizovat	k5eAaBmNgNnS
na	na	k7c6
„	„	k?
<g/>
lynx	lynx	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
do	do	k7c2
starofrancouzštiny	starofrancouzština	k1gFnSc2
převzato	převzít	k5eAaPmNgNnS
jako	jako	k9
„	„	k?
<g/>
lonce	lonce	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Úvodní	úvodní	k2eAgFnSc1d1
„	„	k?
<g/>
l	l	kA
<g/>
“	“	k?
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
mylně	mylně	k6eAd1
pokládáno	pokládat	k5eAaImNgNnS
za	za	k7c4
určitý	určitý	k2eAgInSc4d1
člen	člen	k1gInSc4
(	(	kIx(
<g/>
„	„	k?
<g/>
l	l	kA
<g/>
'	'	kIx"
<g/>
once	onc	k1gMnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Přeneseně	přeneseně	k6eAd1
pak	pak	k6eAd1
byly	být	k5eAaImAgFnP
slovem	slovem	k6eAd1
„	„	k?
<g/>
once	once	k1gInSc1
<g/>
“	“	k?
či	či	k8xC
„	„	k?
<g/>
ounce	ounko	k6eAd1
<g/>
“	“	k?
označovány	označovat	k5eAaImNgFnP
i	i	k9
ostatní	ostatní	k2eAgFnPc1d1
kočkovité	kočkovitý	k2eAgFnPc1d1
šelmy	šelma	k1gFnPc1
velikostí	velikost	k1gFnSc7
či	či	k8xC
vzhledem	vzhled	k1gInSc7
připomínající	připomínající	k2eAgFnSc2d1
rysa	rys	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
zemích	zem	k1gFnPc6
svého	svůj	k3xOyFgInSc2
přirozeného	přirozený	k2eAgInSc2d1
výskytu	výskyt	k1gInSc2
je	být	k5eAaImIp3nS
irbis	irbis	k1gInSc1
znám	znám	k2eAgInSc1d1
pod	pod	k7c7
mnoha	mnoho	k4c7
místními	místní	k2eAgInPc7d1
názvy	název	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mongolsky	mongolsky	k6eAd1
a	a	k8xC
burjatsky	burjatsky	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
irbes	irbes	k1gInSc1
nebo	nebo	k8xC
irves	irves	k1gInSc1
<g/>
,	,	kIx,
kazašsky	kazašsky	k6eAd1
a	a	k8xC
altajsky	altajsky	k6eAd1
bars	bars	k?
nebo	nebo	k8xC
barys	barys	k6eAd1
<g/>
,	,	kIx,
kyrgyzsky	kyrgyzsky	k6eAd1
ilbirs	ilbirs	k6eAd1
<g/>
,	,	kIx,
ladacky	ladacky	k6eAd1
šan	šan	k?
<g/>
,	,	kIx,
tibetsky	tibetsky	k6eAd1
sazig	sazig	k1gInSc1
(	(	kIx(
<g/>
ག	ག	k?
<g/>
་	་	k?
<g/>
ག	ག	k?
<g/>
ི	ི	k?
<g/>
ག	ག	k?
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
zigsa	zigs	k1gMnSc2
<g/>
,	,	kIx,
sa	sa	k?
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
paštunsky	paštunsky	k6eAd1
wā	wā	k1gMnSc1
pṛ	pṛ	k1gMnSc1
(	(	kIx(
<g/>
horský	horský	k2eAgMnSc1d1
levhart	levhart	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
persky	persky	k6eAd1
palangi	palangi	k6eAd1
barfí	barfí	k2eAgMnSc1d1
(	(	kIx(
<g/>
sněžný	sněžný	k2eAgMnSc1d1
levhart	levhart	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
urdsky	urdsky	k6eAd1
barfā	barfā	k?
čī	čī	k?
(	(	kIx(
<g/>
sněžný	sněžný	k2eAgMnSc1d1
gepard	gepard	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
hindsky	hindsky	k6eAd1
him	him	k?
tendua	tendua	k1gFnSc1
(	(	kIx(
<g/>
ह	ह	k?
<g/>
ि	ि	k?
<g/>
म	म	k?
त	त	k?
<g/>
े	े	k?
<g/>
न	न	k?
<g/>
्	्	k?
<g/>
द	द	k?
<g/>
ु	ु	k?
<g/>
आ	आ	k?
<g/>
,	,	kIx,
sněžný	sněžný	k2eAgMnSc1d1
levhart	levhart	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Taxonomie	taxonomie	k1gFnSc1
a	a	k8xC
historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
irbisovi	irbis	k1gMnSc6
a	a	k8xC
první	první	k4xOgNnSc4
vyobrazení	vyobrazení	k1gNnPc2
v	v	k7c6
evropské	evropský	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
francouzského	francouzský	k2eAgMnSc2d1
přírodovědce	přírodovědec	k1gMnSc2
Georgese	Georgese	k1gFnSc2
de	de	k?
Buffona	Buffon	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1761	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Buffon	Buffon	k1gInSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
spisu	spis	k1gInSc6
Histoire	Histoir	k1gInSc5
Naturelle	Naturelle	k1gNnSc7
uváděl	uvádět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
irbis	irbis	k1gFnSc1
žije	žít	k5eAaImIp3nS
v	v	k7c6
Persii	Persie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
odchytáván	odchytávat	k5eAaImNgInS
a	a	k8xC
cvičen	cvičit	k5eAaImNgInS
k	k	k7c3
lovu	lov	k1gInSc3
(	(	kIx(
<g/>
možná	možná	k9
šlo	jít	k5eAaImAgNnS
o	o	k7c4
záměnu	záměna	k1gFnSc4
s	s	k7c7
gepardem	gepard	k1gMnSc7
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Autorem	autor	k1gMnSc7
prvního	první	k4xOgInSc2
vědeckého	vědecký	k2eAgInSc2d1
popisu	popis	k1gInSc2
irbisa	irbisa	k1gFnSc1
je	být	k5eAaImIp3nS
německý	německý	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
a	a	k8xC
přírodovědec	přírodovědec	k1gMnSc1
Johann	Johann	k1gMnSc1
von	von	k1gInSc4
Schreber	Schreber	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
druh	druh	k1gInSc1
pod	pod	k7c7
názvem	název	k1gInSc7
Felis	Felis	k1gFnSc2
uncia	uncia	k1gFnSc1
popsal	popsat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1775	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1831	#num#	k4
druh	druh	k1gInSc1
pod	pod	k7c7
názvem	název	k1gInSc7
Felis	Felis	k1gFnSc2
irbis	irbis	k1gFnSc1
popsal	popsat	k5eAaPmAgMnS
jiný	jiný	k2eAgMnSc1d1
německý	německý	k2eAgMnSc1d1
přírodovědec	přírodovědec	k1gMnSc1
a	a	k8xC
zoolog	zoolog	k1gMnSc1
Christian	Christian	k1gMnSc1
Ehrenberg	Ehrenberg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Potřetí	potřetí	k4xO
byl	být	k5eAaImAgInS
druh	druh	k1gInSc1
popsán	popsat	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1855	#num#	k4
americkým	americký	k2eAgMnSc7d1
lékařem	lékař	k1gMnSc7
a	a	k8xC
přírodovědcem	přírodovědec	k1gMnSc7
Thomasem	Thomas	k1gMnSc7
Horsfieldem	Horsfield	k1gMnSc7
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
pod	pod	k7c7
názvem	název	k1gInSc7
Felis	Felis	k1gFnSc1
uncioides	uncioides	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbis	Irbis	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
velké	velká	k1gFnPc4
kočky	kočka	k1gFnSc2
rodu	rod	k1gInSc2
Panthera	Panthero	k1gNnSc2
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yQgFnPc3,k3yIgFnPc3,k3yRgFnPc3
byl	být	k5eAaImAgMnS
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
popsání	popsání	k1gNnSc6
také	také	k9
původně	původně	k6eAd1
řazen	řadit	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
však	však	k9
byl	být	k5eAaImAgInS
na	na	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
přeřazen	přeřadit	k5eAaPmNgMnS
do	do	k7c2
samostatného	samostatný	k2eAgInSc2d1
rodu	rod	k1gInSc2
Uncia	uncia	k1gFnSc1
(	(	kIx(
<g/>
Pocock	Pocock	k1gMnSc1
<g/>
,	,	kIx,
1916	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Lebka	lebka	k1gFnSc1
irbisa	irbisa	k1gFnSc1
je	být	k5eAaImIp3nS
morfologicky	morfologicky	k6eAd1
odlišná	odlišný	k2eAgFnSc1d1
od	od	k7c2
všech	všecek	k3xTgMnPc2
ostatních	ostatní	k2eAgMnPc2d1
druhů	druh	k1gInPc2
rodu	rod	k1gInSc2
Panthera	Panther	k1gMnSc2
více	hodně	k6eAd2
než	než	k8xS
lebky	lebka	k1gFnSc2
libovolných	libovolný	k2eAgInPc2d1
dvou	dva	k4xCgInPc2
jiných	jiný	k2eAgInPc2d1
druhů	druh	k1gInPc2
rodu	rod	k1gInSc2
Panthera	Panther	k1gMnSc2
mezi	mezi	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
navíc	navíc	k6eAd1
irbis	irbis	k1gInSc1
nese	nést	k5eAaImIp3nS
kombinaci	kombinace	k1gFnSc4
morfologických	morfologický	k2eAgInPc2d1
a	a	k8xC
etologických	etologický	k2eAgInPc2d1
znaků	znak	k1gInPc2
typických	typický	k2eAgInPc2d1
jak	jak	k8xS,k8xC
pro	pro	k7c4
velké	velký	k2eAgFnPc4d1
kočky	kočka	k1gFnPc4
<g/>
,	,	kIx,
tak	tak	k9
pro	pro	k7c4
malé	malý	k2eAgFnPc4d1
kočky	kočka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
struktura	struktura	k1gFnSc1
lebky	lebka	k1gFnSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
podobná	podobný	k2eAgFnSc1d1
velkým	velký	k2eAgMnPc3d1
zástupcům	zástupce	k1gMnPc3
rodu	rod	k1gInSc2
Felis	Felis	k1gFnPc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jazylka	jazylka	k1gFnSc1
nese	nést	k5eAaImIp3nS
typické	typický	k2eAgInPc4d1
znaky	znak	k1gInPc4
zástupců	zástupce	k1gMnPc2
rodu	rod	k1gInSc2
Panthera	Panthera	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Teprve	teprve	k6eAd1
genetické	genetický	k2eAgFnPc4d1
analýzy	analýza	k1gFnPc4
z	z	k7c2
počátku	počátek	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jednoznačně	jednoznačně	k6eAd1
prokázaly	prokázat	k5eAaPmAgFnP
příslušnost	příslušnost	k1gFnSc4
irbisa	irbisa	k1gFnSc1
do	do	k7c2
rodu	rod	k1gInSc2
Panthera	Panthero	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Analýzy	analýza	k1gFnPc1
potvrdily	potvrdit	k5eAaPmAgFnP
blízkou	blízký	k2eAgFnSc4d1
příbuznost	příbuznost	k1gFnSc4
zástupců	zástupce	k1gMnPc2
rodu	rod	k1gInSc2
Panthera	Panthera	k1gFnSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
irbisa	irbis	k1gMnSc2
<g/>
)	)	kIx)
mezi	mezi	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
levharta	levhart	k1gMnSc2
obláčkového	obláčkový	k2eAgMnSc2d1
(	(	kIx(
<g/>
Neofelis	Neofelis	k1gFnSc1
nebulosa	nebulosa	k1gFnSc1
<g/>
)	)	kIx)
určily	určit	k5eAaPmAgFnP
za	za	k7c2
zástupce	zástupce	k1gMnSc2
sesterského	sesterský	k2eAgInSc2d1
rodu	rod	k1gInSc2
Neofelis	Neofelis	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nadále	nadále	k6eAd1
však	však	k9
panují	panovat	k5eAaImIp3nP
nejasnosti	nejasnost	k1gFnPc1
ohledně	ohledně	k7c2
přesného	přesný	k2eAgNnSc2d1
zařazení	zařazení	k1gNnSc2
irbisa	irbis	k1gMnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
rodu	rod	k1gInSc2
Panthera	Panthero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Li	li	k8xS
a	a	k8xC
Zhang	Zhang	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
základě	základ	k1gInSc6
analýzy	analýza	k1gFnSc2
mitochodriálních	mitochodriální	k2eAgInPc2d1
genů	gen	k1gInPc2
a	a	k8xC
jaderné	jaderný	k2eAgFnSc2d1
DNA	DNA	kA
považovali	považovat	k5eAaImAgMnP
irbisa	irbis	k1gMnSc4
za	za	k7c4
nejblíže	blízce	k6eAd3
příbuzného	příbuzný	k2eAgMnSc4d1
levhartovi	levhart	k1gMnSc3
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
pardus	pardus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Johnson	Johnson	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
základě	základ	k1gInSc6
analýzy	analýza	k1gFnSc2
mitochondriální	mitochondriální	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
DNA	dna	k1gFnSc1
označil	označit	k5eAaPmAgInS
irbisa	irbis	k1gMnSc4
za	za	k7c4
sesterskou	sesterský	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
tygra	tygr	k1gMnSc2
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
tigris	tigris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Wei	Wei	k1gFnPc2
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
)	)	kIx)
však	však	k9
na	na	k7c6
základě	základ	k1gInSc6
analýzy	analýza	k1gFnSc2
kompletně	kompletně	k6eAd1
přečteného	přečtený	k2eAgInSc2d1
mitochondriálního	mitochondriální	k2eAgInSc2d1
genomu	genom	k1gInSc2
usuzoval	usuzovat	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
nejbližším	blízký	k2eAgMnPc3d3
příbuzným	příbuzný	k1gMnPc3
irbisa	irbis	k1gMnSc2
je	být	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
lev	lev	k1gInSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
leo	leo	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Bagatharia	Bagatharium	k1gNnSc2
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
svých	svůj	k3xOyFgMnPc2
předchůdců	předchůdce	k1gMnPc2
již	již	k9
měl	mít	k5eAaImAgMnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
kompletně	kompletně	k6eAd1
přečtený	přečtený	k2eAgInSc1d1
mitochondriální	mitochondriální	k2eAgInSc1d1
genom	genom	k1gInSc1
lva	lev	k1gMnSc2
indického	indický	k2eAgMnSc2d1
(	(	kIx(
<g/>
Panthera	Panther	k1gMnSc2
leo	leo	k?
persica	persicus	k1gMnSc2
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
však	však	k9
jako	jako	k9
sesterské	sesterský	k2eAgInPc4d1
druhy	druh	k1gInPc4
určil	určit	k5eAaPmAgMnS
opět	opět	k6eAd1
lva	lev	k1gMnSc4
a	a	k8xC
levharta	levhart	k1gMnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
irbisa	irbis	k1gMnSc4
umístil	umístit	k5eAaPmAgMnS
v	v	k7c6
kladogramu	kladogram	k1gInSc6
mezi	mezi	k7c4
zmíněnou	zmíněný	k2eAgFnSc4d1
dvojici	dvojice	k1gFnSc4
a	a	k8xC
tygra	tygr	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
stejnému	stejný	k2eAgInSc3d1
závěru	závěr	k1gInSc3
dochází	docházet	k5eAaImIp3nS
i	i	k9
Zhang	Zhang	k1gMnSc1
a	a	k8xC
Zhang	Zhang	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Li	li	k8xS
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
studii	studie	k1gFnSc6
zkoumal	zkoumat	k5eAaImAgMnS
rozdíly	rozdíl	k1gInPc4
mezi	mezi	k7c7
fylogenetickými	fylogenetický	k2eAgInPc7d1
stromy	strom	k1gInPc7
sestavenými	sestavený	k2eAgInPc7d1
na	na	k7c6
základě	základ	k1gInSc6
jaderného	jaderný	k2eAgInSc2d1
a	a	k8xC
mitochondriálního	mitochondriální	k2eAgInSc2d1
genomu	genom	k1gInSc2
a	a	k8xC
došel	dojít	k5eAaPmAgInS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
linie	linie	k1gFnSc1
irbisa	irbisa	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
dávné	dávný	k2eAgFnSc6d1
minulosti	minulost	k1gFnSc6
pravděpodobně	pravděpodobně	k6eAd1
ovlivněna	ovlivnit	k5eAaPmNgFnS
hybridizací	hybridizace	k1gFnSc7
<g/>
,	,	kIx,
když	když	k8xS
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
křížení	křížení	k1gNnSc3
mezi	mezi	k7c4
předky	předek	k1gMnPc4
dnešních	dnešní	k2eAgInPc2d1
lvů	lev	k1gInPc2
a	a	k8xC
irbisů	irbis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potomci	potomek	k1gMnPc1
z	z	k7c2
tohoto	tento	k3xDgNnSc2
křížení	křížení	k1gNnSc2
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
měli	mít	k5eAaImAgMnP
zřejmě	zřejmě	k6eAd1
evoluční	evoluční	k2eAgFnSc4d1
výhodu	výhoda	k1gFnSc4
nad	nad	k7c7
„	„	k?
<g/>
obyčejnými	obyčejný	k2eAgFnPc7d1
<g/>
“	“	k?
irbisy	irbis	k1gInPc7
a	a	k8xC
jimi	on	k3xPp3gInPc7
nesené	nesený	k2eAgInPc1d1
geny	gen	k1gInPc1
následně	následně	k6eAd1
v	v	k7c6
linii	linie	k1gFnSc6
irbisa	irbis	k1gMnSc2
převládly	převládnout	k5eAaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fylogeneze	fylogeneze	k1gFnSc1
irbisapodle	irbisapodle	k6eAd1
jaderného	jaderný	k2eAgInSc2d1
genomu	genom	k1gInSc2
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
podle	podle	k7c2
mitochondriálního	mitochondriální	k2eAgInSc2d1
genomu	genom	k1gInSc2
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
<g/>
{	{	kIx(
<g/>
border-spacing	border-spacing	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
margin	margina	k1gFnPc2
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc2
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
line-height	line-height	k1gInSc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
border-collapse	border-collapse	k1gFnSc1
<g/>
:	:	kIx,
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
separate	separat	k1gInSc5
<g/>
;	;	kIx,
<g/>
width	width	k1gInSc1
<g/>
:	:	kIx,
<g/>
auto	auto	k1gNnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
<g/>
{	{	kIx(
<g/>
width	width	k1gInSc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
clade-label	clade-label	k1gInSc1
<g/>
{	{	kIx(
<g/>
width	width	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.7	0.7	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
0.15	0.15	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
vertical-align	vertical-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
bottom	bottom	k1gInSc1
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
;	;	kIx,
<g/>
border-left	border-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
<g/>
border-bottom	border-bottom	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
;	;	kIx,
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-fixed-width	clade-fixed-width	k1gMnSc1
<g/>
{	{	kIx(
<g/>
overflow	overflow	k?
<g/>
:	:	kIx,
<g/>
hidden	hiddna	k1gFnPc2
<g/>
;	;	kIx,
<g/>
text-overflow	text-overflow	k?
<g/>
:	:	kIx,
<g/>
ellipsis	ellipsis	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-fixed-width	clade-fixed-width	k1gMnSc1
<g/>
:	:	kIx,
<g/>
hover	hover	k1gMnSc1
<g/>
{	{	kIx(
<g/>
overflow	overflow	k?
<g/>
:	:	kIx,
<g/>
visible	visible	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-label	clade-label	k1gMnSc1
<g/>
.	.	kIx.
<g/>
first	first	k1gMnSc1
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
border-left	border-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-right	border-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gFnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-label	clade-label	k1gMnSc1
<g/>
.	.	kIx.
<g/>
reverse	reverse	k1gFnSc1
<g/>
{	{	kIx(
<g/>
border-left	border-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-right	border-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-slabel	clade-slabel	k1gMnSc1
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
0.15	0.15	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
vertical-align	vertical-align	k1gNnSc1
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
;	;	kIx,
<g/>
border-left	border-left	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
;	;	kIx,
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-slabel	clade-slabel	k1gMnSc1
<g/>
:	:	kIx,
<g/>
hover	hover	k1gMnSc1
<g/>
{	{	kIx(
<g/>
overflow	overflow	k?
<g/>
:	:	kIx,
<g/>
visible	visible	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-slabel	clade-slabel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
last	last	k1gMnSc1
<g/>
{	{	kIx(
<g/>
border-left	border-left	k1gMnSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-right	border-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gFnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-slabel	clade-slabel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
reverse	reverse	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
{	{	kIx(
<g/>
border-left	border-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-right	border-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-bar	clade-bar	k1gMnSc1
<g/>
{	{	kIx(
<g/>
vertical-align	vertical-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
middle	middle	k6eAd1
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
left	left	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
0.5	0.5	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
position	position	k1gInSc1
<g/>
:	:	kIx,
<g/>
relative	relativ	k1gInSc5
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-bar	clade-bar	k1gMnSc1
<g/>
.	.	kIx.
<g/>
reverse	reverse	k1gFnSc1
<g/>
{	{	kIx(
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
right	right	k1gInSc1
<g/>
;	;	kIx,
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
position	position	k1gInSc1
<g/>
:	:	kIx,
<g/>
relative	relativ	k1gInSc5
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-leaf	clade-leaf	k1gMnSc1
<g/>
{	{	kIx(
<g/>
border	border	k1gMnSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
left	left	k1gMnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
clade-leafR	clade-leafR	k?
<g/>
{	{	kIx(
<g/>
border	border	k1gMnSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
right	right	k1gMnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
td	td	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
clade-leaf	clade-leaf	k1gInSc1
<g/>
.	.	kIx.
<g/>
reverse	reverse	k1gFnSc1
<g/>
{	{	kIx(
<g/>
text-align	text-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
right	right	k1gMnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
<g/>
:	:	kIx,
<g/>
hover	hovrat	k5eAaPmRp2nS
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
linkA	linka	k1gFnSc1
<g/>
{	{	kIx(
<g/>
background-color	background-color	k1gInSc1
<g/>
:	:	kIx,
<g/>
yellow	yellow	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
table	tablo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
clade	clade	k6eAd1
<g/>
:	:	kIx,
<g/>
hover	hovrat	k5eAaPmRp2nS
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
linkB	linkB	k?
<g/>
{	{	kIx(
<g/>
background-color	background-color	k1gMnSc1
<g/>
:	:	kIx,
<g/>
green	green	k1gInSc1
<g/>
}	}	kIx)
</s>
<s>
Panthera	Panthera	k1gFnSc1
</s>
<s>
Panthera	Panthera	k1gFnSc1
leo	leo	k?
(	(	kIx(
<g/>
lev	lev	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Panthera	Panthera	k1gFnSc1
pardus	pardus	k1gInSc1
(	(	kIx(
<g/>
levhart	levhart	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Panthera	Panthera	k1gFnSc1
onca	onca	k1gMnSc1
(	(	kIx(
<g/>
jaguár	jaguár	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
(	(	kIx(
<g/>
irbis	irbis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Panthera	Panthera	k1gFnSc1
tigris	tigris	k1gFnSc1
(	(	kIx(
<g/>
tygr	tygr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Panthera	Panthera	k1gFnSc1
</s>
<s>
Panthera	Panthera	k1gFnSc1
leo	leo	k?
(	(	kIx(
<g/>
lev	lev	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Panthera	Panthera	k1gFnSc1
pardus	pardus	k1gInSc1
(	(	kIx(
<g/>
levhart	levhart	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
(	(	kIx(
<g/>
irbis	irbis	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Panthera	Panthera	k1gFnSc1
onca	onca	k1gMnSc1
(	(	kIx(
<g/>
jaguár	jaguár	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Panthera	Panthera	k1gFnSc1
tigris	tigris	k1gFnSc1
(	(	kIx(
<g/>
tygr	tygr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Poddruhy	poddruh	k1gInPc1
</s>
<s>
Přes	přes	k7c4
rozsáhlý	rozsáhlý	k2eAgInSc4d1
areál	areál	k1gInSc4
výskytu	výskyt	k1gInSc2
rozdělený	rozdělený	k2eAgInSc4d1
do	do	k7c2
menších	malý	k2eAgFnPc2d2
oblastí	oblast	k1gFnPc2
naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
zoologů	zoolog	k1gMnPc2
nerozlišovala	rozlišovat	k5eNaImAgFnS
u	u	k7c2
irbisa	irbis	k1gMnSc2
poddruhy	poddruh	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
pokusy	pokus	k1gInPc1
o	o	k7c4
popis	popis	k1gInSc4
několika	několik	k4yIc2
poddruhů	poddruh	k1gInPc2
dle	dle	k7c2
geografických	geografický	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
irbisové	irbisový	k2eAgInPc1d1
ze	z	k7c2
severozápadu	severozápad	k1gInSc2
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
Mongolska	Mongolsko	k1gNnSc2
a	a	k8xC
Ruska	Rusko	k1gNnSc2
by	by	kYmCp3nP
byli	být	k5eAaImAgMnP
řazeni	řadit	k5eAaImNgMnP
do	do	k7c2
poddruhu	poddruh	k1gInSc2
Uncia	uncia	k1gFnSc1
uncia	uncia	k1gFnSc1
uncia	uncia	k1gFnSc1
(	(	kIx(
<g/>
resp.	resp.	kA
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
uncia	uncia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
irbisové	irbisový	k2eAgFnPc1d1
ze	z	k7c2
západní	západní	k2eAgFnSc2d1
Číny	Čína	k1gFnSc2
a	a	k8xC
Himálaje	Himálaj	k1gFnSc2
by	by	kYmCp3nP
tvořili	tvořit	k5eAaImAgMnP
poddruh	poddruh	k1gInSc4
Uncia	uncia	k1gFnSc1
uncia	uncia	k1gFnSc1
uncioides	uncioides	k1gInSc1
(	(	kIx(
<g/>
resp	resp	kA
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
uncioides	uncioides	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc1
snahy	snaha	k1gFnPc1
však	však	k9
nezískaly	získat	k5eNaPmAgFnP
mezi	mezi	k7c7
odbornou	odborný	k2eAgFnSc7d1
veřejností	veřejnost	k1gFnSc7
výraznější	výrazný	k2eAgFnSc4d2
podporu	podpora	k1gFnSc4
a	a	k8xC
platnost	platnost	k1gFnSc4
taxonomických	taxonomický	k2eAgNnPc2d1
označení	označení	k1gNnPc2
byla	být	k5eAaImAgFnS
donedávna	donedávna	k6eAd1
nulitní	nulitní	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
2017	#num#	k4
vyšla	vyjít	k5eAaPmAgFnS
studie	studie	k1gFnSc1
o	o	k7c6
detailním	detailní	k2eAgInSc6d1
genetickém	genetický	k2eAgInSc6d1
výzkumu	výzkum	k1gInSc6
provedeném	provedený	k2eAgInSc6d1
na	na	k7c4
70	#num#	k4
jedincích	jedinec	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc1
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
populace	populace	k1gFnSc1
irbisa	irbisa	k1gFnSc1
prošla	projít	k5eAaPmAgFnS
asi	asi	k9
před	před	k7c7
8000	#num#	k4
lety	léto	k1gNnPc7
výrazným	výrazný	k2eAgNnSc7d1
efektem	efekt	k1gInSc7
hrdla	hrdlo	k1gNnSc2
láhve	láhev	k1gFnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
souvisel	souviset	k5eAaImAgMnS
se	s	k7c7
změnou	změna	k1gFnSc7
klimatu	klima	k1gNnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Tibetské	tibetský	k2eAgFnSc2d1
náhorní	náhorní	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgInSc3
vykazuje	vykazovat	k5eAaImIp3nS
nízkou	nízký	k2eAgFnSc4d1
genetickou	genetický	k2eAgFnSc4d1
variabilitu	variabilita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autoři	autor	k1gMnPc1
studie	studie	k1gFnPc4
přesto	přesto	k8xC
na	na	k7c6
základě	základ	k1gInSc6
genetické	genetický	k2eAgFnSc2d1
odlišnosti	odlišnost	k1gFnSc2
<g/>
,	,	kIx,
jasného	jasný	k2eAgNnSc2d1
rozčlenění	rozčlenění	k1gNnSc2
populace	populace	k1gFnSc2
a	a	k8xC
geografické	geografický	k2eAgFnSc2d1
separace	separace	k1gFnSc2
doporučili	doporučit	k5eAaPmAgMnP
rozdělit	rozdělit	k5eAaPmF
druh	druh	k1gInSc4
na	na	k7c4
tři	tři	k4xCgInPc4
poddruhy	poddruh	k1gInPc4
<g/>
:	:	kIx,
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
irbis	irbis	k1gFnSc1
(	(	kIx(
<g/>
severní	severní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
Altaje	Altaj	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
uncia	uncia	k1gFnSc1
(	(	kIx(
<g/>
západní	západní	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
Ťan-šanu	Ťan-šan	k1gInSc2
<g/>
,	,	kIx,
Pamíru	Pamír	k1gInSc2
a	a	k8xC
trans-Himálaje	trans-Himálat	k5eAaImSgMnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
uncioides	uncioides	k1gInSc1
(	(	kIx(
<g/>
střední	střední	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
z	z	k7c2
Himálaje	Himálaj	k1gFnSc2
a	a	k8xC
Tibetské	tibetský	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
návrhu	návrh	k1gInSc6
nové	nový	k2eAgFnSc2d1
taxonomie	taxonomie	k1gFnSc2
kočkovitých	kočkovitý	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
<g/>
,	,	kIx,
publikovaném	publikovaný	k2eAgInSc6d1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
Mezinárodním	mezinárodní	k2eAgInSc7d1
svazem	svaz	k1gInSc7
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
irbis	irbis	k1gInSc1
uveden	uveden	k2eAgInSc1d1
jako	jako	k8xS,k8xC
monotypický	monotypický	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
však	však	k9
doporučena	doporučen	k2eAgFnSc1d1
molekulární	molekulární	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
genomu	genom	k1gInSc2
himálajských	himálajský	k2eAgMnPc2d1
a	a	k8xC
tibetských	tibetský	k2eAgMnPc2d1
irbisů	irbis	k1gInPc2
(	(	kIx(
<g/>
výše	vysoce	k6eAd2
uvedená	uvedený	k2eAgFnSc1d1
studie	studie	k1gFnSc1
v	v	k7c6
době	doba	k1gFnSc6
publikační	publikační	k2eAgFnSc2d1
uzávěrky	uzávěrka	k1gFnSc2
pravděpodobně	pravděpodobně	k6eAd1
ještě	ještě	k6eAd1
nebyla	být	k5eNaImAgFnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
potvrdit	potvrdit	k5eAaPmF
či	či	k8xC
vyvrátit	vyvrátit	k5eAaPmF
postavení	postavení	k1gNnSc4
těchto	tento	k3xDgMnPc2
jedinců	jedinec	k1gMnPc2
jako	jako	k8xC,k8xS
případného	případný	k2eAgInSc2d1
poddruhu	poddruh	k1gInSc2
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
schneideri	schneider	k1gFnSc2
(	(	kIx(
<g/>
výše	vysoce	k6eAd2
navržené	navržený	k2eAgNnSc1d1
vědecké	vědecký	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
uncioides	uncioides	k1gInSc4
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
nomen	nomen	k2eAgInSc4d1
nudum	nudum	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Lebka	lebka	k1gFnSc1
irbisa	irbisa	k1gFnSc1
</s>
<s>
Irbis	Irbis	k1gFnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
velká	velký	k2eAgFnSc1d1
kočka	kočka	k1gFnSc1
vzhledem	vzhledem	k7c3
připomínající	připomínající	k2eAgMnSc1d1
levharta	levhart	k1gMnSc4
<g/>
,	,	kIx,
o	o	k7c4
něco	něco	k3yInSc4
menší	malý	k2eAgFnPc1d2
<g/>
,	,	kIx,
avšak	avšak	k8xC
robustnější	robustní	k2eAgFnSc1d2
<g/>
,	,	kIx,
s	s	k7c7
velmi	velmi	k6eAd1
dlouhým	dlouhý	k2eAgInSc7d1
ocasem	ocas	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srst	srst	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
dlouhá	dlouhý	k2eAgFnSc1d1
a	a	k8xC
hustá	hustý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základním	základní	k2eAgNnSc6d1
zbarvení	zbarvení	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
světlé	světlý	k2eAgNnSc1d1
<g/>
,	,	kIx,
plavé	plavý	k2eAgFnPc1d1
až	až	k6eAd1
bílé	bílý	k2eAgFnPc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
rozmístěny	rozmístit	k5eAaPmNgFnP
černé	černý	k2eAgFnPc1d1
skvrny	skvrna	k1gFnPc1
a	a	k8xC
rozety	rozeta	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Délka	délka	k1gFnSc1
těla	tělo	k1gNnSc2
s	s	k7c7
hlavou	hlava	k1gFnSc7
je	být	k5eAaImIp3nS
103	#num#	k4
<g/>
–	–	k?
<g/>
130	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
ocasu	ocas	k1gInSc2
80	#num#	k4
<g/>
–	–	k?
<g/>
105	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Výška	výška	k1gFnSc1
v	v	k7c6
plecích	plec	k1gFnPc6
je	být	k5eAaImIp3nS
kolem	kolem	k7c2
60	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Samci	Samek	k1gMnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
jsou	být	k5eAaImIp3nP
o	o	k7c4
něco	něco	k3yInSc4
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
samice	samice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Dospělý	dospělý	k2eAgMnSc1d1
samec	samec	k1gMnSc1
váží	vážit	k5eAaImIp3nS
45	#num#	k4
<g/>
–	–	k?
<g/>
55	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
samice	samice	k1gFnSc1
22	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Výjimečně	výjimečně	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
tyto	tento	k3xDgFnPc4
hodnoty	hodnota	k1gFnPc4
i	i	k9
vyšší	vysoký	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekordní	rekordní	k2eAgFnPc4d1
hmotnosti	hmotnost	k1gFnPc4
dosáhl	dosáhnout	k5eAaPmAgInS
samec	samec	k1gInSc1
z	z	k7c2
Altaje	Altaj	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
vážil	vážit	k5eAaImAgInS
68,5	68,5	k4
kg	kg	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlava	hlava	k1gFnSc1
je	být	k5eAaImIp3nS
kulatá	kulatý	k2eAgFnSc1d1
<g/>
,	,	kIx,
vzhledem	vzhledem	k7c3
k	k	k7c3
velikosti	velikost	k1gFnSc3
těla	tělo	k1gNnSc2
poměrně	poměrně	k6eAd1
malá	malý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lebka	lebka	k1gFnSc1
je	být	k5eAaImIp3nS
relativně	relativně	k6eAd1
silná	silný	k2eAgFnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
méně	málo	k6eAd2
masivní	masivní	k2eAgFnSc1d1
než	než	k8xS
u	u	k7c2
ostatních	ostatní	k2eAgInPc2d1
druhů	druh	k1gInPc2
rodu	rod	k1gInSc2
Panthera	Panther	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
však	však	k9
z	z	k7c2
lebek	lebka	k1gFnPc2
všech	všecek	k3xTgInPc2
druhů	druh	k1gInPc2
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
relativně	relativně	k6eAd1
nejširší	široký	k2eAgFnSc4d3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Čelo	čelo	k1gNnSc1
je	být	k5eAaImIp3nS
široké	široký	k2eAgNnSc1d1
a	a	k8xC
vysoce	vysoce	k6eAd1
klenuté	klenutý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1
nosní	nosní	k2eAgFnSc1d1
dutina	dutina	k1gFnSc1
a	a	k8xC
široké	široký	k2eAgFnPc1d1
nosní	nosní	k2eAgFnPc1d1
kosti	kost	k1gFnPc1
jsou	být	k5eAaImIp3nP
výsledkem	výsledek	k1gInSc7
adaptace	adaptace	k1gFnSc2
irbisa	irbis	k1gMnSc2
na	na	k7c4
vysokohorské	vysokohorský	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
–	–	k?
vzduch	vzduch	k1gInSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
před	před	k7c7
vstupem	vstup	k1gInSc7
do	do	k7c2
plic	plíce	k1gFnPc2
předehřívá	předehřívat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Parametry	parametr	k1gInPc1
lebky	lebka	k1gFnSc2
(	(	kIx(
<g/>
u	u	k7c2
samců	samec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
největší	veliký	k2eAgFnSc1d3
délka	délka	k1gFnSc1
180	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
190	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc1d3
šířka	šířka	k1gFnSc1
122	#num#	k4
<g/>
–	–	k?
<g/>
134	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
kondylobazální	kondylobazální	k2eAgFnSc1d1
délka	délka	k1gFnSc1
165	#num#	k4
<g/>
–	–	k?
<g/>
173	#num#	k4
mm	mm	kA
<g/>
;	;	kIx,
mezioční	mezioční	k2eAgFnSc1d1
šířka	šířka	k1gFnSc1
43	#num#	k4
<g/>
–	–	k?
<g/>
47	#num#	k4
mm	mm	kA
<g/>
;	;	kIx,
největší	veliký	k2eAgFnSc1d3
výška	výška	k1gFnSc1
lebky	lebka	k1gFnSc2
71	#num#	k4
<g/>
–	–	k?
<g/>
76	#num#	k4
mm	mm	kA
<g/>
;	;	kIx,
délka	délka	k1gFnSc1
horního	horní	k2eAgNnSc2d1
patra	patro	k1gNnSc2
58	#num#	k4
<g/>
–	–	k?
<g/>
63	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uši	ucho	k1gNnPc1
jsou	být	k5eAaImIp3nP
krátké	krátký	k2eAgFnPc1d1
<g/>
,	,	kIx,
posazené	posazený	k2eAgFnPc1d1
široko	široko	k6eAd1
od	od	k7c2
sebe	se	k3xPyFc2
<g/>
,	,	kIx,
tupě	tupě	k6eAd1
zaoblené	zaoblený	k2eAgInPc1d1
<g/>
,	,	kIx,
bez	bez	k7c2
chvostků	chvostek	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
zimě	zima	k1gFnSc6
téměř	téměř	k6eAd1
skryté	skrytý	k2eAgInPc1d1
v	v	k7c4
dlouhé	dlouhý	k2eAgFnPc4d1
srstí	srst	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Oči	oko	k1gNnPc1
jsou	být	k5eAaImIp3nP
velké	velký	k2eAgInPc1d1
<g/>
,	,	kIx,
kulaté	kulatý	k2eAgInPc1d1
a	a	k8xC
bledé	bledý	k2eAgInPc1d1
<g/>
,	,	kIx,
působí	působit	k5eAaImIp3nS
dojmem	dojem	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
irbis	irbis	k1gFnSc1
neustále	neustále	k6eAd1
hledí	hledět	k5eAaImIp3nS
do	do	k7c2
dáli	dál	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Duhovka	duhovka	k1gFnSc1
je	být	k5eAaImIp3nS
světle	světle	k6eAd1
zelená	zelený	k2eAgNnPc1d1
nebo	nebo	k8xC
šedá	šedý	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Vousy	vous	k1gInPc1
jsou	být	k5eAaImIp3nP
bílé	bílý	k2eAgInPc1d1
a	a	k8xC
černé	černý	k2eAgInPc1d1
<g/>
,	,	kIx,
o	o	k7c6
délce	délka	k1gFnSc6
až	až	k9
10,5	10,5	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dospělý	dospělý	k2eAgInSc1d1
irbis	irbis	k1gInSc1
má	mít	k5eAaImIp3nS
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
většina	většina	k1gFnSc1
ostatních	ostatní	k2eAgFnPc2d1
kočkovitých	kočkovitý	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
30	#num#	k4
zubů	zub	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
horní	horní	k2eAgFnSc6d1
čelisti	čelist	k1gFnSc6
na	na	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
3	#num#	k4
řezáky	řezák	k1gInPc4
<g/>
,	,	kIx,
špičák	špičák	k1gMnSc1
<g/>
,	,	kIx,
3	#num#	k4
třenové	třenový	k2eAgInPc1d1
zuby	zub	k1gInPc1
a	a	k8xC
jednu	jeden	k4xCgFnSc4
stoličku	stolička	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
čelisti	čelist	k1gFnSc6
na	na	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
3	#num#	k4
řezáky	řezák	k1gInPc4
<g/>
,	,	kIx,
špičák	špičák	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
třenové	třenový	k2eAgInPc1d1
zuby	zub	k1gInPc1
a	a	k8xC
jednu	jeden	k4xCgFnSc4
stoličku	stolička	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Zubní	zubní	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
</s>
<s>
3	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
{	{	kIx(
<g/>
3	#num#	k4
\	\	kIx~
<g/>
over	over	k1gInSc1
3	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
\	\	kIx~
<g/>
over	over	k1gInSc1
1	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
3	#num#	k4
\	\	kIx~
<g/>
over	over	k1gInSc1
2	#num#	k4
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
1	#num#	k4
\	\	kIx~
<g/>
over	over	k1gInSc1
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
Mohutné	mohutný	k2eAgInPc1d1
špičáky	špičák	k1gInPc1
slouží	sloužit	k5eAaImIp3nP
k	k	k7c3
zabíjení	zabíjení	k1gNnSc3
kořisti	kořist	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Poslední	poslední	k2eAgInSc1d1
horní	horní	k2eAgInSc1d1
třenový	třenový	k2eAgInSc1d1
zub	zub	k1gInSc1
a	a	k8xC
dolní	dolní	k2eAgFnSc1d1
stolička	stolička	k1gFnSc1
jsou	být	k5eAaImIp3nP
přeměněné	přeměněný	k2eAgInPc1d1
v	v	k7c4
mohutné	mohutný	k2eAgInPc4d1
trháky	trhák	k1gInPc4
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
si	se	k3xPyFc3
irbis	irbis	k1gInSc1
odtrhává	odtrhávat	k5eAaImIp3nS
kusy	kus	k1gInPc4
potravy	potrava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tělo	tělo	k1gNnSc1
je	být	k5eAaImIp3nS
silné	silný	k2eAgNnSc1d1
a	a	k8xC
svalnaté	svalnatý	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Zadní	zadní	k2eAgFnSc2d1
nohy	noha	k1gFnSc2
jsou	být	k5eAaImIp3nP
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
velkými	velký	k2eAgFnPc7d1
kočkami	kočka	k1gFnPc7
poměrně	poměrně	k6eAd1
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
uzpůsobené	uzpůsobený	k2eAgFnPc1d1
skokům	skok	k1gInPc3
na	na	k7c4
značnou	značný	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
ruský	ruský	k2eAgMnSc1d1
zoolog	zoolog	k1gMnSc1
S.	S.	kA
I.	I.	kA
Ogněv	Ogněv	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jím	jíst	k5eAaImIp1nS
pozorovaný	pozorovaný	k2eAgInSc1d1
irbis	irbis	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
překonal	překonat	k5eAaPmAgMnS
jedním	jeden	k4xCgInSc7
skokem	skok	k1gInSc7
vzdálenost	vzdálenost	k1gFnSc1
15	#num#	k4
m	m	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
údaj	údaj	k1gInSc1
je	být	k5eAaImIp3nS
však	však	k9
považován	považován	k2eAgInSc1d1
za	za	k7c4
nadsazený	nadsazený	k2eAgInSc4d1
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
běžně	běžně	k6eAd1
jsou	být	k5eAaImIp3nP
nicméně	nicméně	k8xC
pozorovány	pozorován	k2eAgInPc4d1
skoky	skok	k1gInPc4
s	s	k7c7
délkou	délka	k1gFnSc7
kolem	kolem	k7c2
6	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tlapy	tlapa	k1gFnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
přední	přední	k2eAgMnPc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
široké	široký	k2eAgFnPc1d1
a	a	k8xC
masivní	masivní	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	s	k7c7
zatahovatelnými	zatahovatelný	k2eAgInPc7d1
drápy	dráp	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stopy	stopa	k1gFnPc1
jsou	být	k5eAaImIp3nP
velké	velký	k2eAgInPc1d1
a	a	k8xC
kulaté	kulatý	k2eAgInPc1d1
<g/>
,	,	kIx,
bez	bez	k7c2
otisku	otisk	k1gInSc2
drápů	dráp	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stopa	stopa	k1gFnSc1
měří	měřit	k5eAaImIp3nS
na	na	k7c4
délku	délka	k1gFnSc4
9	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ocas	ocas	k1gInSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
dlouhý	dlouhý	k2eAgInSc1d1
<g/>
,	,	kIx,
délkou	délka	k1gFnSc7
přesahuje	přesahovat	k5eAaImIp3nS
tři	tři	k4xCgFnPc1
čtvrtiny	čtvrtina	k1gFnPc1
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
dlouhým	dlouhý	k2eAgInPc3d1
a	a	k8xC
hustým	hustý	k2eAgInPc3d1
chlupům	chlup	k1gInPc3
se	se	k3xPyFc4
jeví	jevit	k5eAaImIp3nS
velmi	velmi	k6eAd1
mohutný	mohutný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Irbisovi	Irbis	k1gMnSc3
pomáhá	pomáhat	k5eAaImIp3nS
udržovat	udržovat	k5eAaImF
rovnováhu	rovnováha	k1gFnSc4
při	při	k7c6
skocích	skok	k1gInPc6
<g/>
,	,	kIx,
šplhu	šplh	k1gInSc6
a	a	k8xC
balancování	balancování	k1gNnSc1
na	na	k7c6
úzkých	úzký	k2eAgFnPc6d1
skalních	skalní	k2eAgFnPc6d1
římsách	římsa	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Když	když	k8xS
irbis	irbis	k1gFnSc1
leží	ležet	k5eAaImIp3nS
nebo	nebo	k8xC
sedí	sedit	k5eAaImIp3nS
<g/>
,	,	kIx,
ocas	ocas	k1gInSc1
mívá	mívat	k5eAaImIp3nS
obtočený	obtočený	k2eAgMnSc1d1
kolem	kolem	k7c2
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
lépe	dobře	k6eAd2
zahřál	zahřát	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbis	Irbis	k1gFnSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
mohutným	mohutný	k2eAgInSc7d1
ocasem	ocas	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc7d1
zbarvení	zbarvení	k1gNnSc4
srsti	srst	k1gFnSc2
je	být	k5eAaImIp3nS
světlé	světlý	k2eAgNnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
tmavými	tmavý	k2eAgFnPc7d1
prstencovými	prstencový	k2eAgFnPc7d1
skvrnami	skvrna	k1gFnPc7
ve	v	k7c6
formě	forma	k1gFnSc6
rozet	rozeta	k1gFnPc2
</s>
<s>
Srst	srst	k1gFnSc1
je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
,	,	kIx,
měkká	měkký	k2eAgFnSc1d1
a	a	k8xC
velmi	velmi	k6eAd1
hustá	hustý	k2eAgFnSc1d1
<g/>
,	,	kIx,
irbisovi	irbis	k1gMnSc3
poskytuje	poskytovat	k5eAaImIp3nS
nezbytnou	zbytný	k2eNgFnSc4d1,k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
proti	proti	k7c3
chladu	chlad	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
přirozeném	přirozený	k2eAgNnSc6d1
vysokohorském	vysokohorský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
panuje	panovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Letní	letní	k2eAgFnSc1d1
srst	srst	k1gFnSc1
má	mít	k5eAaImIp3nS
na	na	k7c6
hřbetě	hřbet	k1gInSc6
a	a	k8xC
bocích	bok	k1gInPc6
délku	délka	k1gFnSc4
kolem	kolem	k7c2
25	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
na	na	k7c6
břiše	břicho	k1gNnSc6
a	a	k8xC
ocase	ocas	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
až	až	k6eAd1
50	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Zimní	zimní	k2eAgFnSc1d1
srst	srst	k1gFnSc1
je	být	k5eAaImIp3nS
delší	dlouhý	k2eAgFnSc1d2
<g/>
,	,	kIx,
na	na	k7c6
hřbetě	hřbet	k1gInSc6
a	a	k8xC
bocích	bok	k1gInPc6
mezi	mezi	k7c4
30	#num#	k4
<g/>
–	–	k?
<g/>
55	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
na	na	k7c6
ocase	ocas	k1gInSc6
až	až	k9
60	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
na	na	k7c6
břiše	břicho	k1gNnSc6
až	až	k9
120	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Hustota	hustota	k1gFnSc1
srsti	srst	k1gFnSc2
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgMnSc1d2
než	než	k8xS
u	u	k7c2
všech	všecek	k3xTgFnPc2
ostatních	ostatní	k2eAgFnPc2d1
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
(	(	kIx(
<g/>
kolem	kolem	k7c2
4000	#num#	k4
chlupů	chlup	k1gInPc2
na	na	k7c4
cm²	cm²	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
blíží	blížit	k5eAaImIp3nS
se	se	k3xPyFc4
spíše	spíše	k9
hodnotám	hodnota	k1gFnPc3
známým	známý	k2eAgInSc7d1
od	od	k7c2
malých	malý	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
rodu	rod	k1gInSc2
Felis	Felis	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
jeden	jeden	k4xCgInSc4
krycí	krycí	k2eAgInSc4d1
chlup	chlup	k1gInSc4
(	(	kIx(
<g/>
pesík	pesík	k1gInSc4
<g/>
)	)	kIx)
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
8	#num#	k4
chlupů	chlup	k1gInPc2
podsady	podsada	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zimní	zimní	k2eAgFnSc1d1
srst	srst	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
světlá	světlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
až	až	k9
téměř	téměř	k6eAd1
bílá	bílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
kouřovým	kouřový	k2eAgInSc7d1
nádechem	nádech	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Irbis	Irbis	k1gFnSc1
je	být	k5eAaImIp3nS
tak	tak	k9
dobře	dobře	k6eAd1
maskován	maskován	k2eAgMnSc1d1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
přirozeném	přirozený	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
–	–	k?
skály	skála	k1gFnSc2
<g/>
,	,	kIx,
kameny	kámen	k1gInPc1
<g/>
,	,	kIx,
sníh	sníh	k1gInSc1
a	a	k8xC
led	led	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letní	letní	k2eAgFnSc1d1
srst	srst	k1gFnSc1
je	být	k5eAaImIp3nS
světlá	světlý	k2eAgFnSc1d1
<g/>
,	,	kIx,
ohraničení	ohraničení	k1gNnSc1
skvrn	skvrna	k1gFnPc2
je	být	k5eAaImIp3nS
patrnější	patrný	k2eAgNnSc1d2
<g/>
,	,	kIx,
ale	ale	k8xC
kouřový	kouřový	k2eAgInSc1d1
nádech	nádech	k1gInSc1
zimní	zimní	k2eAgFnSc2d1
srsti	srst	k1gFnSc2
se	se	k3xPyFc4
vytrácí	vytrácet	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základní	základní	k2eAgFnSc1d1
barva	barva	k1gFnSc1
srsti	srst	k1gFnSc2
je	být	k5eAaImIp3nS
světle	světle	k6eAd1
šedá	šedý	k2eAgFnSc1d1
či	či	k8xC
světle	světle	k6eAd1
hnědá	hnědat	k5eAaImIp3nS
s	s	k7c7
kouřovým	kouřový	k2eAgInSc7d1
nádechem	nádech	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Přítomny	přítomen	k2eAgInPc1d1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
i	i	k9
světle	světle	k6eAd1
žluté	žlutý	k2eAgInPc1d1
tóny	tón	k1gInPc1
<g/>
,	,	kIx,
nikoli	nikoli	k9
však	však	k9
tóny	tón	k1gInPc1
jasně	jasně	k6eAd1
žluté	žlutý	k2eAgInPc1d1
či	či	k8xC
dokonce	dokonce	k9
červené	červený	k2eAgFnPc1d1
(	(	kIx(
<g/>
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
nalezneme	naleznout	k5eAaPmIp1nP,k5eAaBmIp1nP
třeba	třeba	k6eAd1
u	u	k7c2
levharta	levhart	k1gMnSc2
či	či	k8xC
tygra	tygr	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Srst	srst	k1gFnSc1
na	na	k7c6
hřbetě	hřbet	k1gInSc6
a	a	k8xC
hlavě	hlava	k1gFnSc6
je	být	k5eAaImIp3nS
nejtmavší	tmavý	k2eAgMnSc1d3
<g/>
,	,	kIx,
směrem	směr	k1gInSc7
k	k	k7c3
břichu	břich	k1gInSc3
světlá	světlý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srst	srst	k1gFnSc1
na	na	k7c6
hrudi	hruď	k1gFnSc6
<g/>
,	,	kIx,
břiše	břicho	k1gNnSc6
a	a	k8xC
vnitřních	vnitřní	k2eAgFnPc6d1
stranách	strana	k1gFnPc6
končetin	končetina	k1gFnPc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
plavá	plavý	k2eAgFnSc1d1
až	až	k9
téměř	téměř	k6eAd1
bílá	bílý	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Boky	boka	k1gFnPc1
zdobí	zdobit	k5eAaImIp3nP
černé	černý	k2eAgFnPc1d1
či	či	k8xC
tmavě	tmavě	k6eAd1
šedé	šedý	k2eAgFnPc1d1
skvrny	skvrna	k1gFnPc1
ve	v	k7c6
formě	forma	k1gFnSc6
rozet	rozeta	k1gFnPc2
<g/>
,	,	kIx,
uvnitř	uvnitř	k7c2
kterých	který	k3yIgInPc2,k3yQgInPc2,k3yRgInPc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
nalézat	nalézat	k5eAaImF
ještě	ještě	k6eAd1
malá	malý	k2eAgFnSc1d1
černá	černý	k2eAgFnSc1d1
skvrna	skvrna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměr	průměr	k1gInSc1
největších	veliký	k2eAgFnPc2d3
rozet	rozeta	k1gFnPc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
od	od	k7c2
5	#num#	k4
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
do	do	k7c2
7	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
plné	plný	k2eAgFnPc4d1
skvrny	skvrna	k1gFnPc4
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
na	na	k7c6
hlavě	hlava	k1gFnSc6
(	(	kIx(
<g/>
zde	zde	k6eAd1
jsou	být	k5eAaImIp3nP
nejmenší	malý	k2eAgFnPc1d3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
krku	krk	k1gInSc6
a	a	k8xC
končetinách	končetina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
hřbetu	hřbet	k1gInSc2
skvrny	skvrna	k1gFnSc2
začínají	začínat	k5eAaImIp3nP
splývat	splývat	k5eAaImF
a	a	k8xC
mohou	moct	k5eAaImIp3nP
vytvářet	vytvářet	k5eAaImF
podélné	podélný	k2eAgInPc4d1
pruhy	pruh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ocase	ocas	k1gInSc6
mívají	mívat	k5eAaImIp3nP
skvrny	skvrna	k1gFnSc2
formu	forma	k1gFnSc4
příčných	příčný	k2eAgInPc2d1
pruhů	pruh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotná	samotný	k2eAgFnSc1d1
špička	špička	k1gFnSc1
ocasu	ocas	k1gInSc2
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
tmavá	tmavý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Mláďata	mládě	k1gNnPc4
irbisa	irbis	k1gMnSc2
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
výrazně	výrazně	k6eAd1
temnou	temný	k2eAgFnSc7d1
pigmentací	pigmentace	k1gFnSc7
skvrn	skvrna	k1gFnPc2
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
drobnějších	drobný	k2eAgInPc2d2
a	a	k8xC
plných	plný	k2eAgInPc2d1
<g/>
,	,	kIx,
rozet	rozeta	k1gFnPc2
bývá	bývat	k5eAaImIp3nS
jen	jen	k9
velmi	velmi	k6eAd1
málo	málo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
hnědé	hnědý	k2eAgFnPc1d1
či	či	k8xC
černé	černý	k2eAgFnPc1d1
skvrny	skvrna	k1gFnPc1
na	na	k7c6
hřbetě	hřbet	k1gInSc6
směrem	směr	k1gInSc7
k	k	k7c3
ocasu	ocas	k1gInSc3
splývají	splývat	k5eAaImIp3nP
v	v	k7c4
podélné	podélný	k2eAgInPc4d1
černé	černý	k2eAgInPc4d1
pruhy	pruh	k1gInPc4
rovnoběžné	rovnoběžný	k2eAgInPc4d1
s	s	k7c7
páteří	páteř	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k9
u	u	k7c2
tygrů	tygr	k1gMnPc2
či	či	k8xC
levhartů	levhart	k1gMnPc2
<g/>
,	,	kIx,
zbarvení	zbarvení	k1gNnSc4
každého	každý	k3xTgMnSc2
jedince	jedinec	k1gMnSc2
je	být	k5eAaImIp3nS
unikátní	unikátní	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Konkrétní	konkrétní	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
lze	lze	k6eAd1
jednoznačně	jednoznačně	k6eAd1
identifikovat	identifikovat	k5eAaBmF
podle	podle	k7c2
zbarvení	zbarvení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Samice	samice	k1gFnPc1
se	s	k7c7
zbarvením	zbarvení	k1gNnSc7
neliší	lišit	k5eNaImIp3nS
od	od	k7c2
samců	samec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
irbisy	irbis	k1gInPc4
z	z	k7c2
různých	různý	k2eAgFnPc2d1
geografických	geografický	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
nejsou	být	k5eNaImIp3nP
žádné	žádný	k3yNgInPc4
výrazné	výrazný	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
ve	v	k7c4
zbarvení	zbarvení	k1gNnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
jen	jen	k9
minimální	minimální	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
dáno	dát	k5eAaPmNgNnS
i	i	k9
poměrně	poměrně	k6eAd1
malým	malý	k2eAgInPc3d1
(	(	kIx(
<g/>
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
velkými	velký	k2eAgFnPc7d1
kočkami	kočka	k1gFnPc7
<g/>
)	)	kIx)
areálem	areál	k1gInSc7
výskytu	výskyt	k1gInSc2
<g/>
,	,	kIx,
vyznačujícím	vyznačující	k2eAgMnSc7d1
se	se	k3xPyFc4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
svých	svůj	k3xOyFgFnPc6
částech	část	k1gFnPc6
přibližně	přibližně	k6eAd1
stejnými	stejný	k2eAgFnPc7d1
životními	životní	k2eAgFnPc7d1
podmínkami	podmínka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Domovem	domov	k1gInSc7
irbisa	irbis	k1gMnSc2
je	být	k5eAaImIp3nS
střední	střední	k2eAgFnSc1d1
Asie	Asie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Areál	areál	k1gInSc1
výskytu	výskyt	k1gInSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
horské	horský	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
a	a	k8xC
náhorní	náhorní	k2eAgFnPc1d1
plošiny	plošina	k1gFnPc1
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
rozloze	rozloha	k1gFnSc6
zhruba	zhruba	k6eAd1
1	#num#	k4
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
800	#num#	k4
000	#num#	k4
km²	km²	k?
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
států	stát	k1gInPc2
Afghánistán	Afghánistán	k1gInSc1
<g/>
,	,	kIx,
Pákistán	Pákistán	k1gInSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
Nepál	Nepál	k1gInSc1
<g/>
,	,	kIx,
Bhútán	Bhútán	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Mongolsko	Mongolsko	k1gNnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
Kazachstán	Kazachstán	k1gInSc1
<g/>
,	,	kIx,
Kyrgyzstán	Kyrgyzstán	k1gInSc1
<g/>
,	,	kIx,
Tádžikistán	Tádžikistán	k1gInSc1
a	a	k8xC
Uzbekistán	Uzbekistán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Areál	areál	k1gInSc1
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc4
podkovy	podkova	k1gFnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
pomyslným	pomyslný	k2eAgInSc7d1
středem	střed	k1gInSc7
je	být	k5eAaImIp3nS
pohoří	pohoří	k1gNnSc1
Pamír	Pamír	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc7d1
rameno	rameno	k1gNnSc4
podkovy	podkova	k1gFnSc2
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
od	od	k7c2
Pamíru	Pamír	k1gInSc2
a	a	k8xC
Hindúkuše	Hindúkuš	k1gInSc2
přes	přes	k7c4
Karákóram	Karákóram	k1gInSc4
<g/>
,	,	kIx,
Kašmír	Kašmír	k1gInSc4
a	a	k8xC
Himálaj	Himálaj	k1gFnSc4
(	(	kIx(
<g/>
na	na	k7c6
severu	sever	k1gInSc6
pak	pak	k6eAd1
Kchun-lun-šan	Kchun-lun-šany	k1gInPc2
<g/>
)	)	kIx)
až	až	k9
k	k	k7c3
severním	severní	k2eAgFnPc3d1
hranicím	hranice	k1gFnPc3
Barmy	Barma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgInSc4d1
rameno	rameno	k1gNnSc1
směřuje	směřovat	k5eAaImIp3nS
přes	přes	k7c4
Ťan-šan	Ťan-šan	k1gInSc4
<g/>
,	,	kIx,
Altaj	Altaj	k1gInSc4
a	a	k8xC
Sajany	Sajan	k1gInPc4
do	do	k7c2
západního	západní	k2eAgNnSc2d1
Mongolska	Mongolsko	k1gNnSc2
a	a	k8xC
jižní	jižní	k2eAgFnSc2d1
Sibiře	Sibiř	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
zasahuje	zasahovat	k5eAaImIp3nS
areál	areál	k1gInSc1
až	až	k9
k	k	k7c3
nejjižnějšímu	jižní	k2eAgInSc3d3
cípu	cíp	k1gInSc3
jezera	jezero	k1gNnSc2
Bajkal	Bajkal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mongolsku	Mongolsko	k1gNnSc6
se	se	k3xPyFc4
irbis	irbis	k1gInSc1
vyskytuje	vyskytovat	k5eAaImIp3nS
kromě	kromě	k7c2
Mongolského	mongolský	k2eAgInSc2d1
a	a	k8xC
Gobijského	gobijský	k2eAgInSc2d1
Altaje	Altaj	k1gInSc2
i	i	k9
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Changaj	Changaj	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biotop	biotop	k1gInSc1
</s>
<s>
Svahy	svah	k1gInPc1
Altaje	Altaj	k1gInSc2
–	–	k?
typický	typický	k2eAgInSc1d1
biotop	biotop	k1gInSc1
irbisa	irbis	k1gMnSc2
</s>
<s>
Irbis	Irbis	k1gFnSc1
je	být	k5eAaImIp3nS
typickým	typický	k2eAgMnSc7d1
představitelem	představitel	k1gMnSc7
fauny	fauna	k1gFnSc2
horských	horský	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
velkými	velký	k2eAgFnPc7d1
kočkami	kočka	k1gFnPc7
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgInSc7d1
druhem	druh	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
trvale	trvale	k6eAd1
obývá	obývat	k5eAaImIp3nS
horské	horský	k2eAgInPc4d1
biotopy	biotop	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gNnSc3
lovišti	loviště	k1gNnSc3
jsou	být	k5eAaImIp3nP
horské	horský	k2eAgFnPc4d1
louky	louka	k1gFnPc4
<g/>
,	,	kIx,
bezlesé	bezlesý	k2eAgFnPc4d1
kamenné	kamenný	k2eAgFnPc4d1
plošiny	plošina	k1gFnPc4
a	a	k8xC
morény	moréna	k1gFnPc4
<g/>
,	,	kIx,
skalní	skalní	k2eAgInPc4d1
útesy	útes	k1gInPc4
<g/>
,	,	kIx,
často	často	k6eAd1
jej	on	k3xPp3gMnSc4
lze	lze	k6eAd1
potkat	potkat	k5eAaPmF
i	i	k9
v	v	k7c6
zasněžených	zasněžený	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
částech	část	k1gFnPc6
svého	svůj	k3xOyFgInSc2
areálu	areál	k1gInSc2
výskytu	výskyt	k1gInSc2
však	však	k9
irbis	irbis	k1gInSc1
sestupuje	sestupovat	k5eAaImIp3nS
i	i	k9
do	do	k7c2
níže	nízce	k6eAd2
položených	položený	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
pásem	pásmo	k1gNnPc2
kleče	kleč	k1gFnSc2
a	a	k8xC
horského	horský	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Irbis	Irbis	k1gFnSc1
dává	dávat	k5eAaImIp3nS
přednost	přednost	k1gFnSc4
členité	členitý	k2eAgFnSc6d1
horské	horský	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
s	s	k7c7
útesy	útes	k1gInPc7
<g/>
,	,	kIx,
svahy	svah	k1gInPc1
čí	čí	k3xOyQgInPc4,k3xOyRgInPc4
úzkými	úzký	k2eAgNnPc7d1
údolími	údolí	k1gNnPc7
<g/>
,	,	kIx,
s	s	k7c7
porostem	porost	k1gInSc7
alpínské	alpínský	k2eAgFnSc2d1
vegetace	vegetace	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
s	s	k7c7
trhlinami	trhlina	k1gFnPc7
<g/>
,	,	kIx,
hromadami	hromada	k1gFnPc7
kamení	kamení	k1gNnSc2
či	či	k8xC
sutě	suť	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
mu	on	k3xPp3gMnSc3
skýtají	skýtat	k5eAaImIp3nP
vhodný	vhodný	k2eAgInSc4d1
úkryt	úkryt	k1gInSc4
jak	jak	k8xS,k8xC
při	při	k7c6
lovu	lov	k1gInSc6
<g/>
,	,	kIx,
tak	tak	k6eAd1
při	při	k7c6
odpočinku	odpočinek	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
nižších	nízký	k2eAgFnPc6d2
polohách	poloha	k1gFnPc6
jako	jako	k8xC,k8xS
úkryt	úkryt	k1gInSc4
poslouží	posloužit	k5eAaPmIp3nS
i	i	k9
kleč	kleč	k1gFnSc1
či	či	k8xC
jiná	jiný	k2eAgFnSc1d1
vegetace	vegetace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otevřené	otevřený	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
se	se	k3xPyFc4
irbis	irbis	k1gFnSc1
spíše	spíše	k9
vyhýbá	vyhýbat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nS
nad	nad	k7c7
horní	horní	k2eAgFnSc7d1
hranicí	hranice	k1gFnSc7
lesa	les	k1gInSc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
lze	lze	k6eAd1
jej	on	k3xPp3gMnSc4
potkat	potkat	k5eAaPmF
i	i	k9
zde	zde	k6eAd1
<g/>
,	,	kIx,
především	především	k9
v	v	k7c6
zimě	zima	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
biotopů	biotop	k1gInPc2
obývaných	obývaný	k2eAgInPc2d1
irbisem	irbis	k1gInSc7
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
1500	#num#	k4
<g/>
–	–	k?
<g/>
4000	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Irbis	Irbis	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
mála	málo	k4c2
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
vystupují	vystupovat	k5eAaImIp3nP
až	až	k6eAd1
nad	nad	k7c4
hranici	hranice	k1gFnSc4
věčného	věčný	k2eAgInSc2d1
sněhu	sníh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Pamíru	Pamír	k1gInSc6
byl	být	k5eAaImAgInS
několikrát	několikrát	k6eAd1
pozorován	pozorovat	k5eAaImNgInS
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
4500	#num#	k4
<g/>
–	–	k?
<g/>
5000	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Himálaji	Himálaj	k1gFnSc6
pak	pak	k6eAd1
dokonce	dokonce	k9
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
5400	#num#	k4
<g/>
–	–	k?
<g/>
6000	#num#	k4
m	m	kA
n.	n.	k?
m	m	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
sestupuje	sestupovat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
2000	#num#	k4
až	až	k9
2500	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
V	v	k7c6
létě	léto	k1gNnSc6
se	se	k3xPyFc4
zde	zde	k6eAd1
irbis	irbis	k1gInSc1
nejčastěji	často	k6eAd3
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
4000	#num#	k4
<g/>
–	–	k?
<g/>
4500	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
svazích	svah	k1gInPc6
Turkestánského	turkestánský	k2eAgInSc2d1
hřbetu	hřbet	k1gInSc2
lze	lze	k6eAd1
irbisa	irbisa	k1gFnSc1
v	v	k7c6
létě	léto	k1gNnSc6
potkat	potkat	k5eAaPmF
nejčastěji	často	k6eAd3
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
kolem	kolem	k7c2
2600	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Zdržuje	zdržovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
především	především	k9
ve	v	k7c6
skalnatých	skalnatý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
Talaském	Talaský	k2eAgInSc6d1
Alatau	Alataus	k1gInSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
mezi	mezi	k7c7
1200	#num#	k4
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
–	–	k?
<g/>
1800	#num#	k4
a	a	k8xC
3500	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
hřebeni	hřeben	k1gInSc6
Kungej	Kungej	k1gInSc1
Alatau	Alataus	k1gInSc2
se	se	k3xPyFc4
irbis	irbis	k1gInSc1
nejčastěji	často	k6eAd3
zdržuje	zdržovat	k5eAaImIp3nS
na	na	k7c6
alpínských	alpínský	k2eAgFnPc6d1
loukách	louka	k1gFnPc6
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
do	do	k7c2
3300	#num#	k4
m	m	kA
n.	n.	k?
m	m	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
méně	málo	k6eAd2
často	často	k6eAd1
pak	pak	k6eAd1
v	v	k7c6
jehličnatých	jehličnatý	k2eAgInPc6d1
lesích	les	k1gInPc6
(	(	kIx(
<g/>
2100	#num#	k4
<g/>
–	–	k?
<g/>
2600	#num#	k4
m	m	kA
n.	n.	k?
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
Zailijském	Zailijský	k2eAgInSc6d1
Alatau	Alataus	k1gInSc6
a	a	k8xC
v	v	k7c6
centrálním	centrální	k2eAgInSc6d1
Ťan-šanu	Ťan-šan	k1gInSc6
se	se	k3xPyFc4
irbis	irbis	k1gFnSc1
v	v	k7c6
létě	léto	k1gNnSc6
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
kolem	kolem	k7c2
4000	#num#	k4
m	m	kA
n.	n.	k?
m	m	kA
<g/>
,	,	kIx,
v	v	k7c6
zimě	zima	k1gFnSc6
pak	pak	k6eAd1
sestupuje	sestupovat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
1200	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ne	ne	k9
vždy	vždy	k6eAd1
ovšem	ovšem	k9
irbis	irbis	k1gFnSc1
obývá	obývat	k5eAaImIp3nS
vysokohorské	vysokohorský	k2eAgInPc4d1
biotopy	biotop	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
žije	žít	k5eAaImIp3nS
po	po	k7c4
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
v	v	k7c6
nižších	nízký	k2eAgNnPc6d2
pohořích	pohoří	k1gNnPc6
a	a	k8xC
na	na	k7c6
náhorních	náhorní	k2eAgFnPc6d1
stepích	step	k1gFnPc6
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
600	#num#	k4
<g/>
–	–	k?
<g/>
1500	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
I	i	k9
zde	zde	k6eAd1
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nS
skalnatých	skalnatý	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
soutěsek	soutěska	k1gFnPc2
a	a	k8xC
úžlabin	úžlabina	k1gFnPc2
s	s	k7c7
výskytem	výskyt	k1gInSc7
horských	horský	k2eAgFnPc2d1
ovcí	ovce	k1gFnPc2
(	(	kIx(
<g/>
argali	argat	k5eAaImAgMnP,k5eAaBmAgMnP,k5eAaPmAgMnP
<g/>
)	)	kIx)
a	a	k8xC
koz	koza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgFnPc6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
byl	být	k5eAaImAgInS
irbis	irbis	k1gInSc1
pozorován	pozorovat	k5eAaImNgInS
např.	např.	kA
na	na	k7c6
Džungarském	Džungarský	k2eAgInSc6d1
Alatau	Alataus	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbis	Irbis	k1gInSc1
následuje	následovat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
kořist	kořist	k1gFnSc4
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
v	v	k7c6
létě	léto	k1gNnSc6
vystupuje	vystupovat	k5eAaImIp3nS
do	do	k7c2
alpínského	alpínský	k2eAgNnSc2d1
a	a	k8xC
subalpínského	subalpínský	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
hor.	hor.	k?
V	v	k7c6
zimě	zima	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
je	být	k5eAaImIp3nS
horská	horský	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
pokrytá	pokrytý	k2eAgFnSc1d1
vysokou	vysoký	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
sněhu	sníh	k1gInSc2
<g/>
,	,	kIx,
sestupuje	sestupovat	k5eAaImIp3nS
do	do	k7c2
nižších	nízký	k2eAgFnPc2d2
poloh	poloha	k1gFnPc2
<g/>
,	,	kIx,
často	často	k6eAd1
až	až	k9
do	do	k7c2
pásma	pásmo	k1gNnSc2
jehličnatého	jehličnatý	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Sezónní	sezónní	k2eAgFnSc1d1
migrace	migrace	k1gFnSc1
mívá	mívat	k5eAaImIp3nS
pravidelný	pravidelný	k2eAgInSc4d1
charakter	charakter	k1gInSc4
a	a	k8xC
odpovídá	odpovídat	k5eAaImIp3nS
sezónní	sezónní	k2eAgFnSc3d1
migraci	migrace	k1gFnSc3
hlavní	hlavní	k2eAgFnSc2d1
kořisti	kořist	k1gFnSc2
irbisa	irbis	k1gMnSc2
–	–	k?
horských	horský	k2eAgInPc2d1
kopytníků	kopytník	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Biologie	biologie	k1gFnSc1
a	a	k8xC
ekologie	ekologie	k1gFnSc1
</s>
<s>
Irbisovi	Irbisův	k2eAgMnPc1d1
tváří	tvářet	k5eAaImIp3nP
v	v	k7c4
tvář	tvář	k1gFnSc4
</s>
<s>
Irbis	Irbis	k1gFnSc1
je	být	k5eAaImIp3nS
aktivní	aktivní	k2eAgFnSc1d1
především	především	k9
za	za	k7c2
soumraku	soumrak	k1gInSc2
a	a	k8xC
úsvitu	úsvit	k1gInSc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
však	však	k9
i	i	k9
během	během	k7c2
dne	den	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Přes	přes	k7c4
den	den	k1gInSc4
obvykle	obvykle	k6eAd1
odpočívá	odpočívat	k5eAaImIp3nS
<g/>
,	,	kIx,
spí	spát	k5eAaImIp3nS
či	či	k8xC
leží	ležet	k5eAaImIp3nS
na	na	k7c6
skále	skála	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Doupě	doupě	k1gNnSc1
si	se	k3xPyFc3
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
v	v	k7c6
jeskyních	jeskyně	k1gFnPc6
či	či	k8xC
skalních	skalní	k2eAgFnPc6d1
rozsedlinách	rozsedlina	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
úkrytech	úkryt	k1gInPc6
mezi	mezi	k7c7
skalami	skála	k1gFnPc7
<g/>
,	,	kIx,
pod	pod	k7c4
převisy	převis	k1gInPc4
či	či	k8xC
na	na	k7c6
jiných	jiný	k2eAgNnPc6d1
podobných	podobný	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejné	stejné	k1gNnSc1
doupě	doupě	k1gNnSc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
často	často	k6eAd1
i	i	k9
několik	několik	k4yIc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
Kyrgyzského	kyrgyzský	k2eAgInSc2d1
hřbetu	hřbet	k1gInSc2
jsou	být	k5eAaImIp3nP
známy	znám	k2eAgInPc1d1
případy	případ	k1gInPc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
irbisové	irbisový	k2eAgInPc1d1
přes	přes	k7c4
den	den	k1gInSc4
využívali	využívat	k5eAaPmAgMnP,k5eAaImAgMnP
k	k	k7c3
odpočinku	odpočinek	k1gInSc3
velká	velký	k2eAgNnPc4d1
hnízda	hnízdo	k1gNnPc4
po	po	k7c6
supech	sup	k1gMnPc6
hnědých	hnědý	k2eAgMnPc2d1
<g/>
,	,	kIx,
umístěná	umístěný	k2eAgFnSc1d1
na	na	k7c6
nízkých	nízký	k2eAgFnPc6d1
<g/>
,	,	kIx,
ale	ale	k8xC
silných	silný	k2eAgInPc6d1
kmenech	kmen	k1gInPc6
jalovce	jalovec	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Teritoriální	teritoriální	k2eAgNnSc4d1
a	a	k8xC
sociální	sociální	k2eAgNnSc4d1
chování	chování	k1gNnSc4
</s>
<s>
Dospělí	dospělí	k1gMnPc1
irbisové	irbisový	k2eAgFnSc2d1
povětšinou	povětšina	k1gFnSc7
vedou	vést	k5eAaImIp3nP
samotářský	samotářský	k2eAgInSc4d1
způsob	způsob	k1gInSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mláďata	mládě	k1gNnPc1
však	však	k9
tráví	trávit	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
s	s	k7c7
matkou	matka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
dospělý	dospělý	k2eAgInSc1d1
irbis	irbis	k1gInSc1
má	mít	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
vlastní	vlastní	k2eAgNnSc4d1
vymezené	vymezený	k2eAgNnSc4d1
teritorium	teritorium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sledování	sledování	k1gNnPc1
pomocí	pomocí	k7c2
radiotelemetrie	radiotelemetrie	k1gFnSc2
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
u	u	k7c2
irbisů	irbis	k1gInPc2
existuje	existovat	k5eAaImIp3nS
silná	silný	k2eAgFnSc1d1
teritorialita	teritorialita	k1gFnSc1
mezi	mezi	k7c7
příslušníky	příslušník	k1gMnPc7
téhož	týž	k3xTgNnSc2
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
teritoria	teritorium	k1gNnSc2
samců	samec	k1gMnPc2
se	se	k3xPyFc4
však	však	k9
mohou	moct	k5eAaImIp3nP
překrývat	překrývat	k5eAaImF
s	s	k7c7
teritorii	teritorium	k1gNnPc7
samic	samice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
I	i	k9
v	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
si	se	k3xPyFc3
však	však	k9
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
období	období	k1gNnSc2
páření	páření	k1gNnSc6
dospělí	dospělý	k2eAgMnPc1d1
jedinci	jedinec	k1gMnPc1
udržují	udržovat	k5eAaImIp3nP
od	od	k7c2
sebe	sebe	k3xPyFc4
odstup	odstup	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Svoje	svůj	k3xOyFgNnSc4
území	území	k1gNnSc4
si	se	k3xPyFc3
irbisové	irbisový	k2eAgFnPc1d1
značí	značit	k5eAaImIp3nP
různými	různý	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
–	–	k?
většinou	většinou	k6eAd1
močí	močit	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
pomocí	pomocí	k7c2
drásání	drásání	k1gNnSc2
země	zem	k1gFnSc2
či	či	k8xC
kůry	kůra	k1gFnSc2
drápy	dráp	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozloha	rozloha	k1gFnSc1
individuálních	individuální	k2eAgNnPc2d1
teritorií	teritorium	k1gNnPc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
velmi	velmi	k6eAd1
lišit	lišit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Nepálu	Nepál	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
potenciální	potenciální	k2eAgFnSc2d1
kořisti	kořist	k1gFnSc2
<g/>
,	,	kIx,
bývá	bývat	k5eAaImIp3nS
teritorium	teritorium	k1gNnSc4
poměrně	poměrně	k6eAd1
malé	malý	k2eAgFnPc1d1
<g/>
,	,	kIx,
od	od	k7c2
12	#num#	k4
do	do	k7c2
39	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
kdežto	kdežto	k8xS
v	v	k7c6
Mongolsku	Mongolsko	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
otevřený	otevřený	k2eAgInSc4d1
terén	terén	k1gInSc4
a	a	k8xC
řídký	řídký	k2eAgInSc4d1
výskyt	výskyt	k1gInSc4
kopytníků	kopytník	k1gMnPc2
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
přesáhnout	přesáhnout	k5eAaPmF
500	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbis	Irbis	k1gFnSc1
pravidelně	pravidelně	k6eAd1
obchází	obcházet	k5eAaImIp3nS
svoje	svůj	k3xOyFgNnPc4
území	území	k1gNnPc4
<g/>
,	,	kIx,
navštěvuje	navštěvovat	k5eAaImIp3nS
shromaždiště	shromaždiště	k1gNnSc1
a	a	k8xC
zimní	zimní	k2eAgNnSc1d1
pastviště	pastviště	k1gNnSc1
divoké	divoký	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
se	se	k3xPyFc4
drží	držet	k5eAaImIp3nS
pokaždé	pokaždé	k6eAd1
té	ten	k3xDgFnSc2
samé	samý	k3xTgFnSc2
cesty	cesta	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
chodí	chodit	k5eAaImIp3nP
po	po	k7c6
stezkách	stezka	k1gFnPc6
vyšlapaných	vyšlapaný	k2eAgFnPc2d1
zvěří	zvěř	k1gFnPc2
<g/>
,	,	kIx,
vedoucích	vedoucí	k2eAgFnPc2d1
obvykle	obvykle	k6eAd1
po	po	k7c6
hřebeni	hřeben	k1gInSc6
či	či	k8xC
podél	podél	k7c2
říčky	říčka	k1gFnSc2
či	či	k8xC
potoka	potok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
obchůzky	obchůzka	k1gFnSc2
bývá	bývat	k5eAaImIp3nS
velmi	velmi	k6eAd1
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
,	,	kIx,
na	na	k7c6
stejném	stejný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
se	se	k3xPyFc4
irbis	irbis	k1gInSc1
obvykle	obvykle	k6eAd1
objeví	objevit	k5eAaPmIp3nS
vždy	vždy	k6eAd1
jen	jen	k9
jednou	jednou	k6eAd1
za	za	k7c4
několik	několik	k4yIc4
dní	den	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Potrava	potrava	k1gFnSc1
a	a	k8xC
způsob	způsob	k1gInSc1
lovu	lov	k1gInSc2
</s>
<s>
Nahur	Nahur	k1gMnSc1
modrý	modrý	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
oblíbenou	oblíbený	k2eAgFnSc7d1
kořistí	kořist	k1gFnSc7
irbisa	irbis	k1gMnSc2
</s>
<s>
Irbis	Irbis	k1gFnSc4
je	být	k5eAaImIp3nS
predátor	predátor	k1gMnSc1
lovící	lovící	k2eAgNnSc4d1
obvykle	obvykle	k6eAd1
zvěř	zvěř	k1gFnSc4
své	svůj	k3xOyFgFnSc2
velikosti	velikost	k1gFnSc2
či	či	k8xC
větší	veliký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
skolit	skolit	k5eAaPmF,k5eAaImF
kořist	kořist	k1gFnSc4
třikrát	třikrát	k6eAd1
větší	veliký	k2eAgFnSc4d2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
jako	jako	k8xC,k8xS
potenciální	potenciální	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
vylučuje	vylučovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
dospělé	dospělí	k1gMnPc4
velbloudy	velbloud	k1gMnPc4
<g/>
,	,	kIx,
kiangy	kiang	k1gMnPc4
a	a	k8xC
jaky	jak	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Základní	základní	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
potravy	potrava	k1gFnSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
celém	celý	k2eAgInSc6d1
areálu	areál	k1gInSc6
výskytu	výskyt	k1gInSc2
a	a	k8xC
ve	v	k7c6
všech	všecek	k3xTgNnPc6
ročních	roční	k2eAgNnPc6d1
obdobích	období	k1gNnPc6
kopytníci	kopytník	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Kořistí	kořistit	k5eAaImIp3nS
irbisa	irbisa	k1gFnSc1
bývají	bývat	k5eAaImIp3nP
nejrůznější	různý	k2eAgInPc1d3
druhy	druh	k1gInPc1
kopytníků	kopytník	k1gMnPc2
<g/>
:	:	kIx,
horské	horský	k2eAgFnSc2d1
kozy	koza	k1gFnSc2
a	a	k8xC
kozorožci	kozorožec	k1gMnSc3
–	–	k?
nahur	nahur	k1gMnSc1
modrý	modrý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Pseudois	Pseudois	k1gInSc1
nayaur	nayaura	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kozorožec	kozorožec	k1gMnSc1
sibiřský	sibiřský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Capra	Capra	k1gMnSc1
sibirica	sibirica	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koza	koza	k1gFnSc1
šrouborohá	šrouborohý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Capra	Capra	k1gFnSc1
falconeri	falconer	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tahr	tahr	k1gMnSc1
himálajský	himálajský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Hemitragus	Hemitragus	k1gMnSc1
jemlahicus	jemlahicus	k1gMnSc1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
;	;	kIx,
horské	horský	k2eAgFnSc2d1
ovce	ovce	k1gFnSc2
–	–	k?
argali	argat	k5eAaImAgMnP,k5eAaBmAgMnP,k5eAaPmAgMnP
(	(	kIx(
<g/>
Ovis	Ovis	k1gInSc1
ammon	ammon	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
urial	urial	k1gInSc1
(	(	kIx(
<g/>
Ovis	Ovis	k1gInSc1
orientalis	orientalis	k1gFnSc2
vignei	vigne	k1gFnSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
takin	takin	k1gMnSc1
(	(	kIx(
<g/>
Budorcas	Budorcas	k1gMnSc1
taxicolor	taxicolor	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
serau	serau	k6eAd1
himálajský	himálajský	k2eAgInSc1d1
(	(	kIx(
<g/>
Capricornis	Capricornis	k1gInSc1
thar	thar	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
goral	goral	k1gMnSc1
tmavý	tmavý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Naemorhedus	Naemorhedus	k1gMnSc1
goral	goral	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
kabar	kabar	k1gMnSc1
pižmový	pižmový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Moschus	Moschus	k1gMnSc1
moschiferus	moschiferus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
srnci	srnec	k1gMnPc1
(	(	kIx(
<g/>
Capreolus	Capreolus	k1gInSc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jeleni	jelen	k1gMnPc1
(	(	kIx(
<g/>
Cervus	Cervus	k1gInSc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
divoká	divoký	k2eAgNnPc1d1
prasata	prase	k1gNnPc1
(	(	kIx(
<g/>
Sus	Sus	k1gFnSc1
scrofa	scrof	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Drobnější	drobný	k2eAgFnSc4d2
kořist	kořist	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nP
svišti	svišť	k1gMnPc1
(	(	kIx(
<g/>
Marmota	Marmota	k1gFnSc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zajíci	zajíc	k1gMnPc1
(	(	kIx(
<g/>
Lepus	Lepus	k1gInSc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pišťuchy	pišťucha	k1gFnSc2
(	(	kIx(
<g/>
Ochotona	Ochotona	k1gFnSc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
či	či	k8xC
hrabaví	hrabavý	k2eAgMnPc1d1
ptáci	pták	k1gMnPc1
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
velekuři	velekuř	k1gFnSc6
(	(	kIx(
<g/>
Tetraogallus	Tetraogallus	k1gInSc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bažanti	bažant	k1gMnPc1
či	či	k8xC
orebice	orebice	k1gFnPc1
(	(	kIx(
<g/>
Alectoris	Alectoris	k1gInSc1
sp	sp	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgInS
zaznamenán	zaznamenat	k5eAaPmNgInS
i	i	k8xC
úspěšný	úspěšný	k2eAgInSc1d1
útok	útok	k1gInSc1
dvojice	dvojice	k1gFnSc2
irbisů	irbis	k1gInPc2
na	na	k7c4
dvouletého	dvouletý	k2eAgMnSc4d1
medvěda	medvěd	k1gMnSc4
plavého	plavý	k2eAgInSc2d1
(	(	kIx(
<g/>
Ursus	Ursus	k1gMnSc1
arctos	arctos	k1gMnSc1
isabellinus	isabellinus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbis	Irbis	k1gFnSc1
nepohrdne	pohrdnout	k5eNaPmIp3nS
ani	ani	k8xC
menší	malý	k2eAgFnSc7d2
kořistí	kořist	k1gFnSc7
jako	jako	k8xC,k8xS
hrabaví	hrabavý	k2eAgMnPc1d1
ptáci	pták	k1gMnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
orebice	orebice	k1gFnSc1
</s>
<s>
V	v	k7c6
Pamíru	Pamír	k1gInSc6
irbis	irbis	k1gFnSc2
loví	lovit	k5eAaImIp3nP
především	především	k9
horské	horský	k2eAgFnPc4d1
kozy	koza	k1gFnPc4
a	a	k8xC
kozorožce	kozorožec	k1gMnPc4
<g/>
,	,	kIx,
řidčeji	řídce	k6eAd2
argali	argat	k5eAaPmAgMnP,k5eAaBmAgMnP,k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Himálaji	Himálaj	k1gFnSc6
rovněž	rovněž	k9
horské	horský	k2eAgFnPc1d1
kozy	koza	k1gFnPc1
<g/>
,	,	kIx,
horské	horský	k2eAgFnPc1d1
ovce	ovce	k1gFnPc1
<g/>
,	,	kIx,
orongy	orong	k1gInPc1
(	(	kIx(
<g/>
Pantholops	Pantholops	k1gInSc1
hodgsonii	hodgsonie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
gazely	gazel	k1gInPc1
tibetské	tibetský	k2eAgInPc1d1
(	(	kIx(
<g/>
Procapra	Procapra	k1gFnSc1
picticaudata	picticaudata	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jaky	jak	k1gMnPc4
(	(	kIx(
<g/>
Bos	bos	k2eAgMnSc1d1
grunninus	grunninus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sviště	svišť	k1gMnSc4
<g/>
,	,	kIx,
zajíce	zajíc	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Rusku	Rusko	k1gNnSc6
kromě	kromě	k7c2
horských	horský	k2eAgFnPc2d1
koz	koza	k1gFnPc2
loví	lovit	k5eAaImIp3nP
i	i	k9
jeleny	jelen	k1gMnPc4
<g/>
,	,	kIx,
srnce	srnec	k1gMnPc4
<g/>
,	,	kIx,
argali	argat	k5eAaBmAgMnP,k5eAaPmAgMnP,k5eAaImAgMnP
a	a	k8xC
také	také	k9
soby	sob	k1gMnPc4
(	(	kIx(
<g/>
Rangifer	Rangifer	k1gMnSc1
tarandus	tarandus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
nedostatek	nedostatek	k1gInSc4
kořisti	kořist	k1gFnSc2
<g/>
,	,	kIx,
irbis	irbis	k1gInSc4
si	se	k3xPyFc3
odejde	odejít	k5eAaPmIp3nS
hledat	hledat	k5eAaImF
nové	nový	k2eAgNnSc4d1
teritorium	teritorium	k1gNnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
začne	začít	k5eAaPmIp3nS
napadat	napadat	k5eAaPmF,k5eAaImF,k5eAaBmF
domácí	domácí	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
dokonce	dokonce	k9
tvoří	tvořit	k5eAaImIp3nS
domestikovaná	domestikovaný	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
jako	jako	k8xS,k8xC
kozy	koza	k1gFnPc4
<g/>
,	,	kIx,
ovce	ovce	k1gFnPc4
<g/>
,	,	kIx,
koně	kůň	k1gMnPc4
či	či	k8xC
dobytek	dobytek	k1gInSc4
významnou	významný	k2eAgFnSc4d1
součást	součást	k1gFnSc4
stravy	strava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozbor	rozbor	k1gInSc1
trusu	trus	k1gInSc2
ukázal	ukázat	k5eAaPmAgInS
také	také	k9
překvapivě	překvapivě	k6eAd1
vysoký	vysoký	k2eAgInSc1d1
podíl	podíl	k1gInSc1
rostlinné	rostlinný	k2eAgFnSc2d1
stravy	strava	k1gFnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
trávy	tráva	k1gFnSc2
a	a	k8xC
větviček	větvička	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgInPc1
nalezené	nalezený	k2eAgInPc1d1
exkrementy	exkrement	k1gInPc1
sestávaly	sestávat	k5eAaImAgInP
dokonce	dokonce	k9
výhradně	výhradně	k6eAd1
z	z	k7c2
rostlinných	rostlinný	k2eAgInPc2d1
zbytků	zbytek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
kočky	kočka	k1gFnPc1
používají	používat	k5eAaImIp3nP
rostlinný	rostlinný	k2eAgInSc4d1
materiál	materiál	k1gInSc4
k	k	k7c3
podpoře	podpora	k1gFnSc3
<g />
.	.	kIx.
</s>
<s hack="1">
trávení	trávení	k1gNnSc1
nebo	nebo	k8xC
jako	jako	k9
laxativum	laxativum	k1gNnSc1
<g/>
,	,	kIx,
ovšem	ovšem	k9
množství	množství	k1gNnSc1
konzumované	konzumovaný	k2eAgNnSc1d1
irbisem	irbis	k1gInSc7
je	být	k5eAaImIp3nS
mezi	mezi	k7c7
kočkami	kočka	k1gFnPc7
zcela	zcela	k6eAd1
výjimečné	výjimečný	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Důvody	důvod	k1gInPc1
nejsou	být	k5eNaImIp3nP
zcela	zcela	k6eAd1
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
jít	jít	k5eAaImF
o	o	k7c4
likvidaci	likvidace	k1gFnSc4
střevních	střevní	k2eAgMnPc2d1
parazitů	parazit	k1gMnPc2
nebo	nebo	k8xC
i	i	k9
prostě	prostě	k9
jen	jen	k9
o	o	k7c4
doplněk	doplněk	k1gInSc4
stravy	strava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbisové	Irbisový	k2eAgInPc1d1
loví	lovit	k5eAaImIp3nP
většinou	většinou	k6eAd1
samostatně	samostatně	k6eAd1
<g/>
,	,	kIx,
plíží	plížit	k5eAaImIp3nS
se	se	k3xPyFc4
ke	k	k7c3
kořisti	kořist	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
dostatečně	dostatečně	k6eAd1
přiblíží	přiblížit	k5eAaPmIp3nS
<g/>
,	,	kIx,
vyřítí	vyřítit	k5eAaPmIp3nS
se	se	k3xPyFc4
z	z	k7c2
úkrytu	úkryt	k1gInSc2
<g/>
,	,	kIx,
dlouhými	dlouhý	k2eAgInPc7d1
skoky	skok	k1gInPc7
(	(	kIx(
<g/>
6	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
kořist	kořist	k1gFnSc4
dostihnou	dostihnout	k5eAaPmIp3nP
a	a	k8xC
skolí	skolit	k5eAaImIp3nP,k5eAaPmIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Dalším	další	k2eAgInSc7d1
častým	častý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
lovu	lov	k1gInSc2
je	být	k5eAaImIp3nS
číhání	číhání	k1gNnSc4
v	v	k7c6
úkrytu	úkryt	k1gInSc6
a	a	k8xC
skok	skok	k1gInSc1
na	na	k7c4
zvěř	zvěř	k1gFnSc4
z	z	k7c2
vyvýšené	vyvýšený	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
pozice	pozice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
se	se	k3xPyFc4
nepodaří	podařit	k5eNaPmIp3nS
vyhlédnutou	vyhlédnutý	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
skolit	skolit	k5eAaPmF,k5eAaImF
hned	hned	k6eAd1
napoprvé	napoprvé	k6eAd1
<g/>
,	,	kIx,
irbis	irbis	k1gFnSc1
o	o	k7c4
ni	on	k3xPp3gFnSc4
může	moct	k5eAaImIp3nS
ztratit	ztratit	k5eAaPmF
zájem	zájem	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgInSc1d1
pronásledování	pronásledování	k1gNnSc2
až	až	k8xS
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
300	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
U	u	k7c2
velkých	velký	k2eAgInPc2d1
kopytníků	kopytník	k1gInPc2
po	po	k7c6
skolení	skolení	k1gNnSc6
útočí	útočit	k5eAaImIp3nS
na	na	k7c4
hrdlo	hrdlo	k1gNnSc4
nebo	nebo	k8xC
šíji	šíje	k1gFnSc4
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
kořist	kořist	k1gFnSc4
zardousit	zardousit	k5eAaPmF
nebo	nebo	k8xC
jí	jíst	k5eAaImIp3nS
zlomit	zlomit	k5eAaPmF
vaz	vaz	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Mrtvou	mrtvý	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
odtáhne	odtáhnout	k5eAaPmIp3nS
do	do	k7c2
úkrytu	úkryt	k1gInSc2
<g/>
,	,	kIx,
pod	pod	k7c4
skálu	skála	k1gFnSc4
či	či	k8xC
jinam	jinam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
ji	on	k3xPp3gFnSc4
začne	začít	k5eAaPmIp3nS
konzumovat	konzumovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospělý	dospělý	k2eAgInSc4d1
irbis	irbis	k1gInSc4
obvykle	obvykle	k6eAd1
při	při	k7c6
jednom	jeden	k4xCgNnSc6
krmení	krmení	k1gNnSc6
spořádá	spořádat	k5eAaPmIp3nS
2	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
kg	kg	kA
masa	maso	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
vyrušován	vyrušovat	k5eAaImNgInS
<g/>
,	,	kIx,
vydrží	vydržet	k5eAaPmIp3nS
u	u	k7c2
kořisti	kořist	k1gFnSc2
až	až	k9
týden	týden	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Způsob	způsob	k1gInSc1
trhání	trhání	k1gNnSc1
potravy	potrava	k1gFnSc2
je	být	k5eAaImIp3nS
stejný	stejný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
u	u	k7c2
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
<g/>
,	,	kIx,
postavení	postavení	k1gNnSc4
při	při	k7c6
konzumaci	konzumace	k1gFnSc6
potravy	potrava	k1gFnSc2
je	být	k5eAaImIp3nS
podobné	podobný	k2eAgNnSc1d1
jako	jako	k9
u	u	k7c2
malých	malý	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sledování	sledování	k1gNnSc1
pomocí	pomoc	k1gFnPc2
GPS	GPS	kA
obojků	obojek	k1gInPc2
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
velkou	velký	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
irbis	irbis	k1gFnSc2
skolí	skolit	k5eAaImIp3nS,k5eAaPmIp3nS
zhruba	zhruba	k6eAd1
každých	každý	k3xTgFnPc2
10	#num#	k4
<g/>
–	–	k?
<g/>
15	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Koncem	koncem	k7c2
léta	léto	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c4
podzim	podzim	k1gInSc4
a	a	k8xC
začátkem	začátkem	k7c2
zimy	zima	k1gFnSc2
lze	lze	k6eAd1
pozorovat	pozorovat	k5eAaImF
při	při	k7c6
lovu	lov	k1gInSc6
i	i	k9
rodinné	rodinný	k2eAgFnPc4d1
skupinky	skupinka	k1gFnPc4
<g/>
,	,	kIx,
sestávající	sestávající	k2eAgNnPc4d1
obvykle	obvykle	k6eAd1
ze	z	k7c2
samice	samice	k1gFnSc2
a	a	k8xC
jejích	její	k3xOp3gNnPc2
mláďat	mládě	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
celém	celý	k2eAgInSc6d1
areálu	areál	k1gInSc6
výskytu	výskyt	k1gInSc2
je	být	k5eAaImIp3nS
irbis	irbis	k1gInSc1
vrcholovým	vrcholový	k2eAgMnSc7d1
predátorem	predátor	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
nemá	mít	k5eNaImIp3nS
vážnou	vážný	k2eAgFnSc4d1
konkurenci	konkurence	k1gFnSc4
ze	z	k7c2
strany	strana	k1gFnSc2
jiných	jiný	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nižších	nízký	k2eAgFnPc6d2
polohách	poloha	k1gFnPc6
může	moct	k5eAaImIp3nS
docházet	docházet	k5eAaImF
ke	k	k7c3
kompetici	kompetik	k1gMnPc1
s	s	k7c7
vlkem	vlk	k1gMnSc7
(	(	kIx(
<g/>
Canis	Canis	k1gFnSc1
lupus	lupus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
jižních	jižní	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
areálu	areál	k1gInSc2
výskytu	výskyt	k1gInSc2
pak	pak	k6eAd1
existuje	existovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc1
střetu	střet	k1gInSc2
s	s	k7c7
levhartem	levhart	k1gMnSc7
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
pardus	pardus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Někdy	někdy	k6eAd1
přijde	přijít	k5eAaPmIp3nS
do	do	k7c2
kontaktu	kontakt	k1gInSc2
s	s	k7c7
dhouly	dhoul	k1gInPc7
(	(	kIx(
<g/>
Cuon	Cuon	k1gMnSc1
alpinus	alpinus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nad	nad	k7c7
nimž	jenž	k3xRgInPc3
má	mít	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
převahu	převaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
zaznamenán	zaznamenat	k5eAaPmNgInS
případ	případ	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
čtyřčlenné	čtyřčlenný	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
těchto	tento	k3xDgMnPc2
psů	pes	k1gMnPc2
ukradl	ukradnout	k5eAaPmAgMnS
zabitou	zabitý	k2eAgFnSc4d1
kozu	koza	k1gFnSc4
a	a	k8xC
úlovek	úlovek	k1gInSc4
si	se	k3xPyFc3
odnesl	odnést	k5eAaPmAgMnS
pryč	pryč	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dhoulové	Dhoulový	k2eAgInPc1d1
se	se	k3xPyFc4
při	při	k7c6
tom	ten	k3xDgNnSc6
nezmohli	zmoct	k5eNaPmAgMnP
na	na	k7c4
odpor	odpor	k1gInSc4
<g/>
,	,	kIx,
jen	jen	k9
irbisa	irbis	k1gMnSc4
zpovzdálí	zpovzdálí	k6eAd1
sledovali	sledovat	k5eAaImAgMnP
a	a	k8xC
kňučeli	kňučet	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlasové	hlasový	k2eAgInPc1d1
projevy	projev	k1gInPc1
</s>
<s>
Irbis	Irbis	k1gFnSc1
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
ostatních	ostatní	k2eAgFnPc2d1
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
nedokáže	dokázat	k5eNaPmIp3nS
řvát	řvát	k5eAaImF
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
jeho	jeho	k3xOp3gFnSc1
jazylka	jazylka	k1gFnSc1
není	být	k5eNaImIp3nS
plně	plně	k6eAd1
zkostnatělá	zkostnatělý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Původně	původně	k6eAd1
se	se	k3xPyFc4
zoologové	zoolog	k1gMnPc1
domnívali	domnívat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
řvaní	řvaní	k1gNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
velkým	velký	k2eAgFnPc3d1
kočkám	kočka	k1gFnPc3
právě	právě	k9
nezkostnatělá	zkostnatělý	k2eNgFnSc1d1
jazylka	jazylka	k1gFnSc1
<g/>
,	,	kIx,
nové	nový	k2eAgInPc1d1
výzkumy	výzkum	k1gInPc1
však	však	k9
ukázaly	ukázat	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
schopnost	schopnost	k1gFnSc1
řvát	řvát	k5eAaImF
<g />
.	.	kIx.
</s>
<s hack="1">
je	být	k5eAaImIp3nS
u	u	k7c2
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
spojena	spojen	k2eAgFnSc1d1
i	i	k8xC
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
morfologickými	morfologický	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
hrtanu	hrtan	k1gInSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
se	s	k7c7
stavbou	stavba	k1gFnSc7
hlasivek	hlasivka	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
u	u	k7c2
irbisa	irbis	k1gMnSc2
odpovídá	odpovídat	k5eAaImIp3nS
malým	malý	k2eAgFnPc3d1
kočkám	kočka	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Nejhlasitějším	hlasitý	k2eAgInSc7d3
zvukem	zvuk	k1gInSc7
je	být	k5eAaImIp3nS
tak	tak	k9
hluboké	hluboký	k2eAgNnSc1d1
a	a	k8xC
táhlé	táhlý	k2eAgNnSc1d1
sténavé	sténavý	k2eAgNnSc1d1
volání	volání	k1gNnSc1
<g/>
,	,	kIx,
kterým	který	k3yIgFnPc3,k3yRgFnPc3,k3yQgFnPc3
se	se	k3xPyFc4
irbisové	irbisový	k2eAgMnPc4d1
vzájemně	vzájemně	k6eAd1
volají	volat	k5eAaImIp3nP
v	v	k7c6
době	doba	k1gFnSc6
rozmnožování	rozmnožování	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Irbis	Irbis	k1gInSc1
jinak	jinak	k6eAd1
disponuje	disponovat	k5eAaBmIp3nS
celou	celý	k2eAgFnSc7d1
škálou	škála	k1gFnSc7
zvuků	zvuk	k1gInPc2
a	a	k8xC
projevů	projev	k1gInPc2
běžných	běžný	k2eAgInPc2d1
u	u	k7c2
ostatních	ostatní	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrní	vrnět	k5eAaImIp3nS
při	při	k7c6
nádechu	nádech	k1gInSc6
i	i	k8xC
výdechu	výdech	k1gInSc6
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
malé	malý	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
útoku	útok	k1gInSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
rozzuřený	rozzuřený	k2eAgInSc1d1
<g/>
,	,	kIx,
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
krátký	krátký	k2eAgInSc4d1
<g/>
,	,	kIx,
hlasitý	hlasitý	k2eAgInSc4d1
kašlavý	kašlavý	k2eAgInSc4d1
zvuk	zvuk	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
</s>
<s>
Koťata	kotě	k1gNnPc1
irbisa	irbisa	k1gFnSc1
</s>
<s>
Pohlavní	pohlavní	k2eAgFnSc1d1
dospělost	dospělost	k1gFnSc1
nastává	nastávat	k5eAaImIp3nS
ve	v	k7c6
2	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
letech	let	k1gInPc6
u	u	k7c2
samic	samice	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
3	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
letech	let	k1gInPc6
u	u	k7c2
samců	samec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
páření	páření	k1gNnSc3
dochází	docházet	k5eAaImIp3nS
koncem	konec	k1gInSc7
zimy	zima	k1gFnSc2
či	či	k8xC
začátkem	začátkem	k7c2
jara	jaro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říje	říje	k1gFnSc1
u	u	k7c2
samice	samice	k1gFnSc2
trvá	trvat	k5eAaImIp3nS
2	#num#	k4
až	až	k9
12	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
páření	páření	k1gNnSc3
12	#num#	k4
<g/>
×	×	k?
až	až	k9
36	#num#	k4
<g/>
×	×	k?
za	za	k7c4
den	den	k1gInSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
kopulace	kopulace	k1gFnSc1
trvá	trvat	k5eAaImIp3nS
15	#num#	k4
až	až	k9
45	#num#	k4
sekund	sekunda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Březost	březost	k1gFnSc1
trvá	trvat	k5eAaImIp3nS
90	#num#	k4
<g/>
–	–	k?
<g/>
103	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Samice	samice	k1gFnPc1
kvůli	kvůli	k7c3
dlouhé	dlouhý	k2eAgFnSc3d1
péči	péče	k1gFnSc3
o	o	k7c4
mláďata	mládě	k1gNnPc4
rodí	rodit	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
jen	jen	k9
jednou	jeden	k4xCgFnSc7
za	za	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
doupě	doupě	k1gNnSc1
mívá	mívat	k5eAaImIp3nS
na	na	k7c6
nejnedostupnějším	dostupný	k2eNgNnSc6d3
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samec	samec	k1gMnSc1
se	se	k3xPyFc4
péče	péče	k1gFnSc1
o	o	k7c4
potomstvo	potomstvo	k1gNnSc4
neúčastní	účastnit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koťata	kotě	k1gNnPc1
přichází	přicházet	k5eAaImIp3nP
na	na	k7c4
svět	svět	k1gInSc4
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
geografické	geografický	k2eAgFnSc6d1
poloze	poloha	k1gFnSc6
místa	místo	k1gNnSc2
vrhu	vrh	k1gInSc2
buď	buď	k8xC
v	v	k7c6
dubnu	duben	k1gInSc6
<g/>
–	–	k?
<g/>
květnu	květen	k1gInSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
květnu	květen	k1gInSc3
<g/>
–	–	k?
<g/>
červnu	červen	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnSc1
rodí	rodit	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
2	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
,	,	kIx,
vzácněji	vzácně	k6eAd2
pak	pak	k6eAd1
jen	jen	k9
jedno	jeden	k4xCgNnSc4
nebo	nebo	k8xC
naopak	naopak	k6eAd1
4	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
mláďat	mládě	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Zaznamenané	zaznamenaný	k2eAgNnSc1d1
maximum	maximum	k1gNnSc1
je	být	k5eAaImIp3nS
7	#num#	k4
koťat	kotě	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Porodní	porodní	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
váha	váha	k1gFnSc1
mláděte	mládě	k1gNnSc2
je	být	k5eAaImIp3nS
kolem	kolem	k7c2
450	#num#	k4
gramů	gram	k1gInPc2
(	(	kIx(
<g/>
Sunquist	Sunquist	k1gMnSc1
&	&	k?
Sunquist	Sunquist	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
udávají	udávat	k5eAaImIp3nP
rozmezí	rozmezí	k1gNnSc4
320	#num#	k4
<g/>
–	–	k?
<g/>
567	#num#	k4
gramů	gram	k1gInPc2
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
těla	tělo	k1gNnSc2
do	do	k7c2
30	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
ocasu	ocas	k1gInSc2
15	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
cm	cm	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Koťata	kotě	k1gNnPc1
se	se	k3xPyFc4
rodí	rodit	k5eAaImIp3nP
slepá	slepý	k2eAgFnSc1d1
a	a	k8xC
bezmocná	bezmocný	k2eAgFnSc1d1
<g/>
,	,	kIx,
vidět	vidět	k5eAaImF
začínají	začínat	k5eAaImIp3nP
zhruba	zhruba	k6eAd1
po	po	k7c6
7	#num#	k4
dnech	den	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInPc1
týdny	týden	k1gInPc1
jsou	být	k5eAaImIp3nP
mláďata	mládě	k1gNnPc4
kojena	kojit	k5eAaImNgFnS
mateřským	mateřský	k2eAgNnSc7d1
mlékem	mléko	k1gNnSc7
a	a	k8xC
rychle	rychle	k6eAd1
přibývají	přibývat	k5eAaImIp3nP
na	na	k7c6
váze	váha	k1gFnSc6
<g/>
,	,	kIx,
300	#num#	k4
<g/>
–	–	k?
<g/>
500	#num#	k4
gramů	gram	k1gInPc2
za	za	k7c4
týden	týden	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Zhruba	zhruba	k6eAd1
po	po	k7c6
pěti	pět	k4xCc6
týdnech	týden	k1gInPc6
váží	vážit	k5eAaImIp3nP
kolem	kolem	k7c2
2,5	2,5	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
kg	kg	kA
a	a	k8xC
začínají	začínat	k5eAaImIp3nP
chodit	chodit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
také	také	k9
přecházejí	přecházet	k5eAaImIp3nP
na	na	k7c4
pevnou	pevný	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Uprostřed	uprostřed	k7c2
léta	léto	k1gNnSc2
již	již	k6eAd1
doprovázejí	doprovázet	k5eAaImIp3nP
matku	matka	k1gFnSc4
na	na	k7c6
lovu	lov	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
osamostatňují	osamostatňovat	k5eAaImIp3nP
se	se	k3xPyFc4
však	však	k9
až	až	k9
druhou	druhý	k4xOgFnSc4
zimu	zima	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Sourozenci	sourozenec	k1gMnPc1
mohou	moct	k5eAaImIp3nP
po	po	k7c4
osamostatnění	osamostatnění	k1gNnSc4
zůstat	zůstat	k5eAaPmF
ještě	ještě	k6eAd1
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
pospolu	pospolu	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Údaje	údaj	k1gInPc1
o	o	k7c6
délce	délka	k1gFnSc6
života	život	k1gInSc2
irbisů	irbis	k1gInPc2
v	v	k7c6
divočině	divočina	k1gFnSc6
se	se	k3xPyFc4
kvůli	kvůli	k7c3
obtížnosti	obtížnost	k1gFnSc3
získávání	získávání	k1gNnSc2
dat	datum	k1gNnPc2
výrazně	výrazně	k6eAd1
liší	lišit	k5eAaImIp3nP
<g/>
,	,	kIx,
zpráva	zpráva	k1gFnSc1
ruské	ruský	k2eAgFnSc2d1
pobočky	pobočka	k1gFnSc2
WWF	WWF	kA
z	z	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
nejdelší	dlouhý	k2eAgInSc1d3
doložený	doložený	k2eAgInSc1d1
věk	věk	k1gInSc1
irbisa	irbis	k1gMnSc2
v	v	k7c6
divočině	divočina	k1gFnSc6
13	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
zajetí	zajetí	k1gNnSc6
žijí	žít	k5eAaImIp3nP
irbisové	irbisový	k2eAgFnPc1d1
výrazně	výrazně	k6eAd1
déle	dlouho	k6eAd2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
doloženy	doložen	k2eAgInPc1d1
případy	případ	k1gInPc1
irbisů	irbis	k1gInPc2
starších	starý	k2eAgInPc2d2
20	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Zřejmě	zřejmě	k6eAd1
okolo	okolo	k7c2
15	#num#	k4
<g/>
.	.	kIx.
roku	rok	k1gInSc2
života	život	k1gInSc2
ztrácejí	ztrácet	k5eAaImIp3nP
irbisové	irbis	k1gInPc1
schopnost	schopnost	k1gFnSc4
reprodukce	reprodukce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stav	stav	k1gInSc1
populace	populace	k1gFnSc2
a	a	k8xC
ochrana	ochrana	k1gFnSc1
</s>
<s>
Divoký	divoký	k2eAgInSc1d1
irbis	irbis	k1gInSc1
v	v	k7c6
indickém	indický	k2eAgInSc6d1
Ladákhu	Ladákh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
obtížné	obtížný	k2eAgFnSc3d1
dostupnosti	dostupnost	k1gFnSc3
míst	místo	k1gNnPc2
výskytu	výskyt	k1gInSc2
a	a	k8xC
skrytému	skrytý	k2eAgInSc3d1
způsobu	způsob	k1gInSc3
života	život	k1gInSc2
jsou	být	k5eAaImIp3nP
veškeré	veškerý	k3xTgFnPc1
dostupné	dostupný	k2eAgFnPc1d1
statistiky	statistika	k1gFnPc1
počtu	počet	k1gInSc2
irbisů	irbis	k1gInPc2
v	v	k7c6
divoké	divoký	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
založeny	založit	k5eAaPmNgFnP
pouze	pouze	k6eAd1
na	na	k7c6
odhadech	odhad	k1gInPc6
expertů	expert	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
proto	proto	k8xC
nutno	nutno	k6eAd1
je	být	k5eAaImIp3nS
brát	brát	k5eAaImF
pouze	pouze	k6eAd1
jako	jako	k8xC,k8xS
orientační	orientační	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stav	stav	k1gInSc1
divoké	divoký	k2eAgFnSc2d1
populace	populace	k1gFnSc2
irbisů	irbis	k1gInPc2
má	mít	k5eAaImIp3nS
však	však	k9
díky	díky	k7c3
tlaku	tlak	k1gInSc3
člověka	člověk	k1gMnSc2
prokazatelně	prokazatelně	k6eAd1
sestupný	sestupný	k2eAgInSc1d1
charakter	charakter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lov	lov	k1gInSc1
irbisa	irbis	k1gMnSc2
je	být	k5eAaImIp3nS
sice	sice	k8xC
zakázán	zakázán	k2eAgInSc1d1
<g/>
,	,	kIx,
finančně	finančně	k6eAd1
velmi	velmi	k6eAd1
atraktivní	atraktivní	k2eAgNnSc4d1
pytláctví	pytláctví	k1gNnSc4
však	však	k9
stále	stále	k6eAd1
významně	významně	k6eAd1
snižuje	snižovat	k5eAaImIp3nS
stavy	stav	k1gInPc4
populace	populace	k1gFnSc2
divokých	divoký	k2eAgInPc2d1
irbisů	irbis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
redukce	redukce	k1gFnSc1
pastvin	pastvina	k1gFnPc2
a	a	k8xC
počtu	počet	k1gInSc2
chovaného	chovaný	k2eAgInSc2d1
dobytka	dobytek	k1gInSc2
přinesly	přinést	k5eAaPmAgFnP
navýšení	navýšení	k1gNnSc4
populace	populace	k1gFnSc2
hlavní	hlavní	k2eAgFnSc2d1
kořisti	kořist	k1gFnSc2
irbisa	irbis	k1gMnSc2
–	–	k?
horských	horský	k2eAgInPc2d1
kopytníků	kopytník	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
však	však	k9
zvyšující	zvyšující	k2eAgFnSc1d1
se	se	k3xPyFc4
chudoba	chudoba	k1gFnSc1
místních	místní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
vedla	vést	k5eAaImAgFnS
ke	k	k7c3
zvýšenému	zvýšený	k2eAgNnSc3d1
používání	používání	k1gNnSc3
loveckých	lovecký	k2eAgFnPc2d1
metod	metoda	k1gFnPc2
a	a	k8xC
pytláctví	pytláctví	k1gNnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
lovu	lov	k1gInSc2
irbisa	irbisa	k1gFnSc1
do	do	k7c2
pytláckých	pytlácký	k2eAgFnPc2d1
smyček	smyčka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
nárůstu	nárůst	k1gInSc2
nelegálního	legální	k2eNgInSc2d1
lovu	lov	k1gInSc2
na	na	k7c6
počátku	počátek	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
bylo	být	k5eAaImAgNnS
zvýšení	zvýšení	k1gNnSc1
zájmu	zájem	k1gInSc2
o	o	k7c4
irbisí	irbisí	k1gNnSc4
kůže	kůže	k1gFnSc2
a	a	k8xC
s	s	k7c7
tím	ten	k3xDgNnSc7
spojený	spojený	k2eAgInSc1d1
růst	růst	k1gInSc1
jejich	jejich	k3xOp3gFnPc2
cen	cena	k1gFnPc2
na	na	k7c6
černém	černý	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
Tibetské	tibetský	k2eAgFnSc6d1
náhorní	náhorní	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
mělo	mít	k5eAaImAgNnS
výrazný	výrazný	k2eAgInSc4d1
dopad	dopad	k1gInSc4
na	na	k7c4
irbisí	irbisí	k1gNnSc4
populaci	populace	k1gFnSc4
masové	masový	k2eAgNnSc1d1
hubení	hubení	k1gNnSc1
pišťuch	pišťucha	k1gFnPc2
a	a	k8xC
svišťů	svišť	k1gMnPc2
pomocí	pomocí	k7c2
pesticidů	pesticid	k1gInPc2
jakožto	jakožto	k8xS
škůdců	škůdce	k1gMnPc2
úrody	úroda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Irbisové	Irbisový	k2eAgNnSc4d1
tak	tak	k8xS,k8xC
přišli	přijít	k5eAaPmAgMnP
o	o	k7c4
šanci	šance	k1gFnSc4
na	na	k7c6
ulovení	ulovení	k1gNnSc6
snadno	snadno	k6eAd1
dostupné	dostupný	k2eAgFnSc2d1
kořisti	kořist	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
zmínění	zmíněný	k2eAgMnPc1d1
hlodavci	hlodavec	k1gMnPc1
a	a	k8xC
zajícovci	zajícovec	k1gMnPc1
představovali	představovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
množství	množství	k1gNnSc3
faktorů	faktor	k1gInPc2
majících	mající	k2eAgInPc2d1
negativní	negativní	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
stav	stav	k1gInSc4
populace	populace	k1gFnSc2
patří	patřit	k5eAaImIp3nS
vedle	vedle	k7c2
pytláctví	pytláctví	k1gNnSc2
i	i	k9
defenzivní	defenzivní	k2eAgNnSc4d1
chování	chování	k1gNnSc4
irbisa	irbis	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Irbis	Irbis	k1gFnSc1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
přirozeném	přirozený	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
nemá	mít	k5eNaImIp3nS
nepřátele	nepřítel	k1gMnPc4
(	(	kIx(
<g/>
kromě	kromě	k7c2
člověka	člověk	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
při	při	k7c6
střetu	střet	k1gInSc6
s	s	k7c7
člověkem	člověk	k1gMnSc7
není	být	k5eNaImIp3nS
ostražitý	ostražitý	k2eAgMnSc1d1
a	a	k8xC
neprchá	prchat	k5eNaImIp3nS
do	do	k7c2
úkrytu	úkryt	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
To	to	k9
v	v	k7c6
otevřených	otevřený	k2eAgInPc6d1
horských	horský	k2eAgInPc6d1
terénech	terén	k1gInPc6
a	a	k8xC
při	při	k7c6
rozšířenosti	rozšířenost	k1gFnSc6
střelných	střelný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
mezi	mezi	k7c7
místními	místní	k2eAgMnPc7d1
obyvateli	obyvatel	k1gMnPc7
často	často	k6eAd1
vede	vést	k5eAaImIp3nS
ke	k	k7c3
smrti	smrt	k1gFnSc3
zvířete	zvíře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
irbisové	irbisový	k2eAgInPc1d1
občas	občas	k6eAd1
nepohrdnou	pohrdnout	k5eNaPmIp3nP
zbytky	zbytek	k1gInPc1
kořisti	kořist	k1gFnSc2
jiných	jiný	k2eAgMnPc2d1
predátorů	predátor	k1gMnPc2
<g/>
,	,	kIx,
stávají	stávat	k5eAaImIp3nP
se	se	k3xPyFc4
také	také	k9
obětí	oběť	k1gFnSc7
otrávených	otrávený	k2eAgFnPc2d1
návnad	návnada	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
nezákonně	zákonně	k6eNd1
používány	používat	k5eAaImNgInP
pro	pro	k7c4
boj	boj	k1gInSc4
s	s	k7c7
vlky	vlk	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
divoce	divoce	k6eAd1
žijících	žijící	k2eAgMnPc2d1
irbisů	irbis	k1gInPc2
činil	činit	k5eAaImAgInS
podle	podle	k7c2
expertního	expertní	k2eAgInSc2d1
odhadu	odhad	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
mezi	mezi	k7c7
4080	#num#	k4
až	až	k9
6590	#num#	k4
jedinci	jedinec	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
rozmezí	rozmezí	k1gNnSc1
uvádí	uvádět	k5eAaImIp3nS
i	i	k9
zpráva	zpráva	k1gFnSc1
IUCN	IUCN	kA
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jackson	Jacksona	k1gFnPc2
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
uvedl	uvést	k5eAaPmAgInS
rozpětí	rozpětí	k1gNnSc4
4500	#num#	k4
až	až	k9
7500	#num#	k4
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
poznamenal	poznamenat	k5eAaPmAgMnS
však	však	k9
<g/>
,	,	kIx,
že	že	k8xS
dostupné	dostupný	k2eAgFnPc1d1
informace	informace	k1gFnPc1
jsou	být	k5eAaImIp3nP
neúplné	úplný	k2eNgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Zpráva	zpráva	k1gFnSc1
GSLEP	GSLEP	kA
z	z	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
odhaduje	odhadovat	k5eAaImIp3nS
počty	počet	k1gInPc4
na	na	k7c4
3920	#num#	k4
až	až	k9
6390	#num#	k4
jedinců	jedinec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hustota	hustota	k1gFnSc1
populace	populace	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
částech	část	k1gFnPc6
areálu	areál	k1gInSc2
výskytu	výskyt	k1gInSc2
výrazně	výrazně	k6eAd1
liší	lišit	k5eAaImIp3nS
–	–	k?
od	od	k7c2
0,1	0,1	k4
do	do	k7c2
10	#num#	k4
jedinců	jedinec	k1gMnPc2
na	na	k7c4
100	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Rusku	Rusko	k1gNnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
0,7	0,7	k4
jedince	jedinec	k1gMnPc4
na	na	k7c4
100	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
v	v	k7c6
Nepálu	Nepál	k1gInSc6
5	#num#	k4
<g/>
–	–	k?
<g/>
7	#num#	k4
jedinců	jedinec	k1gMnPc2
na	na	k7c4
100	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
v	v	k7c6
Mongolsku	Mongolsko	k1gNnSc6
3	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
jedinci	jedinec	k1gMnPc7
na	na	k7c6
100	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Následující	následující	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
údajů	údaj	k1gInPc2
v	v	k7c6
dokumentu	dokument	k1gInSc6
Snow	Snow	k1gMnSc2
Leopard	leopard	k1gMnSc1
Survival	Survival	k1gMnSc2
Strategy	Stratega	k1gFnSc2
vydaného	vydaný	k2eAgInSc2d1
organizací	organizace	k1gFnSc7
Snow	Snow	k1gFnSc1
Leopard	leopard	k1gMnSc1
Network	network	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Nebyl	být	k5eNaImAgInS
<g/>
-li	-li	k?
konkrétní	konkrétní	k2eAgInSc4d1
údaj	údaj	k1gInSc4
v	v	k7c6
dokumentu	dokument	k1gInSc6
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
použit	použít	k5eAaPmNgInS
starší	starý	k2eAgInSc1d2
údaj	údaj	k1gInSc1
ze	z	k7c2
studie	studie	k1gFnSc2
McCarthyho	McCarthy	k1gMnSc2
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
v	v	k7c6
téměř	téměř	k6eAd1
nezměněné	změněný	k2eNgFnSc6d1
podobě	podoba	k1gFnSc6
cituje	citovat	k5eAaBmIp3nS
i	i	k9
IUCN	IUCN	kA
–	–	k?
Jackson	Jackson	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Stát	stát	k1gInSc1
</s>
<s>
Areál	areál	k1gInSc1
obývanýirbisy	obývanýirbis	k1gInPc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odhadovaný	odhadovaný	k2eAgInSc1d1
početjedinců	početjedinec	k1gInPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rokodhadu	Rokodhást	k5eAaPmIp1nS
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
<g/>
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
100	#num#	k4
–	–	k?
200	#num#	k4
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
2003	#num#	k4
</s>
<s>
Bhútán	Bhútán	k1gInSc1
<g/>
10	#num#	k4
000100	#num#	k4
–	–	k?
2002014	#num#	k4
</s>
<s>
Čína	Čína	k1gFnSc1
<g/>
1	#num#	k4
100	#num#	k4
0002000	#num#	k4
–	–	k?
25002014	#num#	k4
</s>
<s>
Indie	Indie	k1gFnSc1
<g/>
127	#num#	k4
000400	#num#	k4
–	–	k?
7002014	#num#	k4
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
<g/>
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
100	#num#	k4
–	–	k?
1102003	#num#	k4
(	(	kIx(
<g/>
areál	areál	k1gInSc1
<g/>
)	)	kIx)
<g/>
2014	#num#	k4
(	(	kIx(
<g/>
populace	populace	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
<g/>
54	#num#	k4
000300	#num#	k4
–	–	k?
3502014	#num#	k4
</s>
<s>
Mongolsko	Mongolsko	k1gNnSc1
<g/>
103	#num#	k4
00010002014	#num#	k4
</s>
<s>
Nepál	Nepál	k1gInSc1
<g/>
13	#num#	k4
000195	#num#	k4
–	–	k?
4162014	#num#	k4
</s>
<s>
Pákistán	Pákistán	k1gInSc1
<g/>
80	#num#	k4
000200	#num#	k4
–	–	k?
3002014	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
<g/>
20	#num#	k4
000	#num#	k4
–	–	k?
30	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
60	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
70	#num#	k4
–	–	k?
902014	#num#	k4
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
<g/>
85	#num#	k4
700180	#num#	k4
–	–	k?
220	#num#	k4
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
2014	#num#	k4
(	(	kIx(
<g/>
areál	areál	k1gInSc1
<g/>
)	)	kIx)
<g/>
2003	#num#	k4
(	(	kIx(
<g/>
populace	populace	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
<g/>
10	#num#	k4
00010	#num#	k4
–	–	k?
15	#num#	k4
(	(	kIx(
<g/>
20	#num#	k4
–	–	k?
30	#num#	k4
<g/>
)	)	kIx)
<g/>
2014	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
1	#num#	k4
700	#num#	k4
000	#num#	k4
–	–	k?
1	#num#	k4
740	#num#	k4
000	#num#	k4
</s>
<s>
4655	#num#	k4
–	–	k?
6116	#num#	k4
</s>
<s>
Ochrana	ochrana	k1gFnSc1
</s>
<s>
Irbis	Irbis	k1gInSc1
zachycený	zachycený	k2eAgInSc1d1
fotopastí	fotopast	k1gFnSc7
v	v	k7c6
Národním	národní	k2eAgInSc6d1
parku	park	k1gInSc6
Hemis	Hemis	k1gFnSc2
v	v	k7c6
indickém	indický	k2eAgInSc6d1
státě	stát	k1gInSc6
Džammú	Džammú	k1gFnSc4
a	a	k8xC
Kašmír	Kašmír	k1gInSc4
</s>
<s>
Stav	stav	k1gInSc1
divoké	divoký	k2eAgFnSc2d1
populace	populace	k1gFnSc2
irbisů	irbis	k1gInPc2
je	být	k5eAaImIp3nS
nízký	nízký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Irbis	Irbis	k1gFnPc2
je	být	k5eAaImIp3nS
vzácné	vzácný	k2eAgNnSc1d1
<g/>
,	,	kIx,
málopočetné	málopočetný	k2eAgNnSc1d1
a	a	k8xC
ohrožené	ohrožený	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
IUCN	IUCN	kA
<g/>
)	)	kIx)
jej	on	k3xPp3gMnSc4
od	od	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
eviduje	evidovat	k5eAaImIp3nS
jako	jako	k9
zranitelný	zranitelný	k2eAgInSc1d1
druh	druh	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
dlouhá	dlouhý	k2eAgNnPc4d1
léta	léto	k1gNnPc4
byl	být	k5eAaImAgInS
však	však	k9
evidován	evidován	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
ohrožený	ohrožený	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Červené	Červené	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
Mongolska	Mongolsko	k1gNnSc2
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
veden	veden	k2eAgInSc4d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
velmi	velmi	k6eAd1
vzácný	vzácný	k2eAgMnSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
Červené	Červené	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
status	status	k1gInSc4
„	„	k?
<g/>
ohrožený	ohrožený	k2eAgMnSc1d1
vyhubením	vyhubení	k1gNnSc7
<g/>
“	“	k?
(	(	kIx(
<g/>
kategorie	kategorie	k1gFnSc2
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Irbis	Irbis	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
uveden	uvést	k5eAaPmNgInS
v	v	k7c6
CITES	CITES	kA
<g/>
,	,	kIx,
příloze	příloha	k1gFnSc6
I	I	kA
(	(	kIx(
<g/>
nejvíce	nejvíce	k6eAd1,k6eAd3
ohrožené	ohrožený	k2eAgInPc1d1
druhy	druh	k1gInPc1
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgFnPc7
je	být	k5eAaImIp3nS
jakýkoliv	jakýkoliv	k3yIgInSc1
mezinárodní	mezinárodní	k2eAgInSc1d1
obchod	obchod	k1gInSc1
zakázán	zakázat	k5eAaPmNgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
poznamenat	poznamenat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
výše	vysoce	k6eAd2
uvedené	uvedený	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
představují	představovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
legální	legální	k2eAgInSc4d1
rámec	rámec	k1gInSc4
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
irbisa	irbisa	k1gFnSc1
<g/>
,	,	kIx,
naplňování	naplňování	k1gNnSc1
ochrany	ochrana	k1gFnSc2
v	v	k7c6
praxi	praxe	k1gFnSc6
je	být	k5eAaImIp3nS
nedostatečné	dostatečný	k2eNgNnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
ukazuje	ukazovat	k5eAaImIp3nS
objem	objem	k1gInSc1
neustávajícího	ustávající	k2eNgNnSc2d1
pytláctví	pytláctví	k1gNnSc2
a	a	k8xC
pašeráctví	pašeráctví	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Organizované	organizovaný	k2eAgInPc1d1
programy	program	k1gInPc1
na	na	k7c4
záchranu	záchrana	k1gFnSc4
irbisa	irbis	k1gMnSc2
v	v	k7c6
divočině	divočina	k1gFnSc6
začaly	začít	k5eAaPmAgFnP
vznikat	vznikat	k5eAaImF
až	až	k9
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgMnS
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Trust	trust	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
první	první	k4xOgFnSc1
organizace	organizace	k1gFnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
přímo	přímo	k6eAd1
na	na	k7c4
ochranu	ochrana	k1gFnSc4
irbisa	irbis	k1gMnSc2
ve	v	k7c6
volné	volný	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
vznikaly	vznikat	k5eAaImAgFnP
další	další	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Conservancy	Conservanca	k1gFnSc2
<g/>
,	,	kIx,
zapojily	zapojit	k5eAaPmAgInP
se	se	k3xPyFc4
IUCN	IUCN	kA
i	i	k8xC
WWF	WWF	kA
<g/>
,	,	kIx,
ovšem	ovšem	k9
vždy	vždy	k6eAd1
šlo	jít	k5eAaImAgNnS
spíše	spíše	k9
o	o	k7c4
projekty	projekt	k1gInPc4
na	na	k7c6
regionální	regionální	k2eAgFnSc6d1
či	či	k8xC
národní	národní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významným	významný	k2eAgInSc7d1
mezníkem	mezník	k1gInSc7
proto	proto	k8xC
bylo	být	k5eAaImAgNnS
setkání	setkání	k1gNnSc3
vysokých	vysoký	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
všech	všecek	k3xTgFnPc2
12	#num#	k4
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
jejichž	jejichž	k3xOyRp3gNnSc6
území	území	k1gNnSc6
areál	areál	k1gInSc4
irbisa	irbisa	k1gFnSc1
zasahuje	zasahovat	k5eAaImIp3nS
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
výše	výše	k1gFnSc2,k1gFnSc2wB
uvedená	uvedený	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
konferenci	konference	k1gFnSc6
v	v	k7c6
kyrgyzském	kyrgyzský	k2eAgInSc6d1
Biškeku	Biškek	k1gInSc6
v	v	k7c6
říjnu	říjen	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
byla	být	k5eAaImAgFnS
společná	společný	k2eAgFnSc1d1
deklarace	deklarace	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
základě	základ	k1gInSc6
vznikl	vzniknout	k5eAaPmAgInS
projekt	projekt	k1gInSc1
Global	globat	k5eAaImAgMnS
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
&	&	k?
Ecosystem	Ecosyst	k1gInSc7
Protection	Protection	k1gInSc1
Program	program	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
do	do	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
vytipovat	vytipovat	k5eAaPmF
napříč	napříč	k6eAd1
areálem	areál	k1gInSc7
výskytu	výskyt	k1gInSc2
irbisa	irbis	k1gMnSc2
nejméně	málo	k6eAd3
20	#num#	k4
zdravých	zdravý	k2eAgFnPc2d1
populací	populace	k1gFnPc2
a	a	k8xC
zajistit	zajistit	k5eAaPmF
jejich	jejich	k3xOp3gFnSc4
ochranu	ochrana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chov	chov	k1gInSc1
v	v	k7c6
zajetí	zajetí	k1gNnSc6
</s>
<s>
V	v	k7c6
Evropě	Evropa	k1gFnSc6
byl	být	k5eAaImAgInS
irbis	irbis	k1gInSc1
znám	znám	k2eAgInSc1d1
již	již	k6eAd1
koncem	koncem	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
první	první	k4xOgNnSc1
živé	živý	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
však	však	k9
bylo	být	k5eAaImAgNnS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
k	k	k7c3
vidění	vidění	k1gNnSc3
až	až	k8xS
roku	rok	k1gInSc3
1851	#num#	k4
v	v	k7c6
zoo	zoo	k1gFnSc6
v	v	k7c6
Antverpách	Antverpy	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Irbis	Irbis	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
klidné	klidný	k2eAgNnSc1d1
zvíře	zvíře	k1gNnSc1
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
nejméně	málo	k6eAd3
agresivní	agresivní	k2eAgMnSc1d1
ze	z	k7c2
všech	všecek	k3xTgFnPc2
velkých	velký	k2eAgFnPc2d1
koček	kočka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
zajetí	zajetí	k1gNnSc6
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gInSc7
proto	proto	k8xC
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
<g/>
,	,	kIx,
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
snadno	snadno	k6eAd1
ochočit	ochočit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počet	počet	k1gInSc1
irbisů	irbis	k1gInPc2
chovaných	chovaný	k2eAgInPc2d1
v	v	k7c6
zajetí	zajetí	k1gNnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
odhadován	odhadovat	k5eAaImNgInS
zhruba	zhruba	k6eAd1
na	na	k7c4
2000	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
z	z	k7c2
tohoto	tento	k3xDgInSc2
počtu	počet	k1gInSc2
připadala	připadat	k5eAaPmAgFnS,k5eAaImAgFnS
na	na	k7c4
Čínu	Čína	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
Čínu	Čína	k1gFnSc4
se	se	k3xPyFc4
v	v	k7c6
chovech	chov	k1gInPc6
nacházelo	nacházet	k5eAaImAgNnS
kolem	kolem	k7c2
600	#num#	k4
irbisů	irbis	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Přibližně	přibližně	k6eAd1
16	#num#	k4
%	%	kIx~
v	v	k7c6
zajetí	zajetí	k1gNnSc6
chovaných	chovaný	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
pocházelo	pocházet	k5eAaImAgNnS
z	z	k7c2
volné	volný	k2eAgFnSc2d1
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgMnSc1d1
se	se	k3xPyFc4
již	již	k6eAd1
narodili	narodit	k5eAaPmAgMnP
v	v	k7c6
zajetí	zajetí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Irbisové	Irbisový	k2eAgFnSc2d1
se	se	k3xPyFc4
v	v	k7c6
zajetí	zajetí	k1gNnSc6
úspěšně	úspěšně	k6eAd1
rozmnožují	rozmnožovat	k5eAaImIp3nP
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
například	například	k6eAd1
roku	rok	k1gInSc2
2000	#num#	k4
se	se	k3xPyFc4
narodilo	narodit	k5eAaPmAgNnS
v	v	k7c6
registrovaných	registrovaný	k2eAgInPc6d1
chovech	chov	k1gInPc6
v	v	k7c6
zoologických	zoologický	k2eAgFnPc6d1
zahradách	zahrada	k1gFnPc6
v	v	k7c6
87	#num#	k4
vrzích	vrh	k1gInPc6
celkem	celkem	k6eAd1
179	#num#	k4
koťat	kotě	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Populace	populace	k1gFnSc2
v	v	k7c6
zoologických	zoologický	k2eAgFnPc6d1
zahradách	zahrada	k1gFnPc6
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
životaschopnou	životaschopný	k2eAgFnSc4d1
a	a	k8xC
stabilní	stabilní	k2eAgFnSc4d1
<g/>
,	,	kIx,
irbis	irbis	k1gInSc1
bývá	bývat	k5eAaImIp3nS
uváděn	uvádět	k5eAaImNgInS
jako	jako	k8xS,k8xC
učebnicový	učebnicový	k2eAgInSc1d1
příklad	příklad	k1gInSc1
dobrého	dobrý	k2eAgInSc2d1
managementu	management	k1gInSc2
chovu	chov	k1gInSc2
vzácného	vzácný	k2eAgInSc2d1
živočišného	živočišný	k2eAgInSc2d1
druhu	druh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plemennou	plemenný	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
vede	vést	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
zoo	zoo	k1gFnPc2
ve	v	k7c6
finských	finský	k2eAgFnPc6d1
Helsinkách	Helsinky	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
chovají	chovat	k5eAaImIp3nP
irbisa	irbisa	k1gFnSc1
následující	následující	k2eAgFnSc2d1
zoologické	zoologický	k2eAgFnSc2d1
zahrady	zahrada	k1gFnSc2
<g/>
:	:	kIx,
ZOO	zoo	k1gFnSc1
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
ZOO	zoo	k1gFnSc1
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
ZOO	zoo	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
<g/>
,	,	kIx,
ZOO	zoo	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
všech	všecek	k3xTgFnPc6
čtyřech	čtyři	k4xCgFnPc6
uvedených	uvedený	k2eAgFnPc6d1
zoo	zoo	k1gFnPc6
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
úspěšně	úspěšně	k6eAd1
odchovat	odchovat	k5eAaPmF
mláďata	mládě	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbis	Irbis	k1gFnSc1
a	a	k8xC
člověk	člověk	k1gMnSc1
</s>
<s>
Lov	lov	k1gInSc1
na	na	k7c4
irbisa	irbis	k1gMnSc4
</s>
<s>
Irbis	Irbis	k1gInSc1
nikdy	nikdy	k6eAd1
nebyl	být	k5eNaImAgInS
loven	lovit	k5eAaImNgMnS
v	v	k7c6
extrémních	extrémní	k2eAgNnPc6d1
množstvích	množství	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
zákazem	zákaz	k1gInSc7
lovu	lov	k1gInSc2
celosvětový	celosvětový	k2eAgInSc1d1
roční	roční	k2eAgInSc1d1
počet	počet	k1gInSc1
ulovených	ulovený	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
nikdy	nikdy	k6eAd1
výrazně	výrazně	k6eAd1
nepřesáhl	přesáhnout	k5eNaPmAgMnS
jeden	jeden	k4xCgInSc4
tisíc	tisíc	k4xCgInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
letech	let	k1gInPc6
1907	#num#	k4
<g/>
–	–	k?
<g/>
1910	#num#	k4
činil	činit	k5eAaImAgInS
celosvětový	celosvětový	k2eAgInSc1d1
roční	roční	k2eAgInSc1d1
úlovek	úlovek	k1gInSc1
750	#num#	k4
<g/>
–	–	k?
<g/>
800	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
počet	počet	k1gInSc1
ročně	ročně	k6eAd1
ulovených	ulovený	k2eAgInPc2d1
kusů	kus	k1gInPc2
na	na	k7c4
území	území	k1gNnSc4
tehdejšího	tehdejší	k2eAgMnSc2d1
SSSR	SSSR	kA
pohyboval	pohybovat	k5eAaImAgInS
pouze	pouze	k6eAd1
v	v	k7c6
desítkách	desítka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
výkupní	výkupní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
kožešin	kožešina	k1gFnPc2
byla	být	k5eAaImAgFnS
extrémně	extrémně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
průměru	průměr	k1gInSc6
3	#num#	k4
rubly	rubnout	k5eAaPmAgFnP
za	za	k7c4
kus	kus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
Hlavními	hlavní	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
lovu	lov	k1gInSc2
byly	být	k5eAaImAgFnP
Tádžikistán	Tádžikistán	k1gInSc4
a	a	k8xC
Kyrgyzstán	Kyrgyzstán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kožešiny	kožešina	k1gFnSc2
se	se	k3xPyFc4
používaly	používat	k5eAaImAgInP
hlavně	hlavně	k9
jako	jako	k9
koberce	koberec	k1gInPc1
<g/>
,	,	kIx,
na	na	k7c4
výrobu	výroba	k1gFnSc4
kožichů	kožich	k1gInPc2
a	a	k8xC
kožešinových	kožešinový	k2eAgInPc2d1
límců	límec	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
světovém	světový	k2eAgInSc6d1
trhu	trh	k1gInSc6
byla	být	k5eAaImAgFnS
po	po	k7c6
kožešinách	kožešina	k1gFnPc6
irbisů	irbis	k1gInPc2
vždy	vždy	k6eAd1
velká	velký	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
a	a	k8xC
byly	být	k5eAaImAgFnP
vysoce	vysoce	k6eAd1
ceněny	cenit	k5eAaImNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
byl	být	k5eAaImAgInS
irbis	irbis	k1gInSc1
považován	považován	k2eAgInSc1d1
za	za	k7c4
nebezpečné	bezpečný	k2eNgNnSc4d1
zvíře	zvíře	k1gNnSc4
a	a	k8xC
škodnou	škodná	k1gFnSc4
<g/>
,	,	kIx,
proto	proto	k8xC
lov	lov	k1gInSc1
ani	ani	k8xC
lovecké	lovecký	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
nebyly	být	k5eNaImAgFnP
nijak	nijak	k6eAd1
omezovány	omezován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
zabití	zabití	k1gNnSc2
irbisa	irbisa	k1gFnSc1
se	se	k3xPyFc4
dokonce	dokonce	k9
vyplácela	vyplácet	k5eAaImAgFnS
odměna	odměna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
poptávka	poptávka	k1gFnSc1
byla	být	k5eAaImAgFnS
i	i	k9
po	po	k7c6
živých	živý	k2eAgInPc6d1
irbisech	irbis	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Irbis	Irbis	k1gFnPc1
a	a	k8xC
domácí	domácí	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
</s>
<s>
Na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
svého	své	k1gNnSc2
areálu	areál	k1gInSc6
se	se	k3xPyFc4
irbis	irbis	k1gFnSc1
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
konfliktu	konflikt	k1gInSc2
s	s	k7c7
člověkem	člověk	k1gMnSc7
kvůli	kvůli	k7c3
predaci	predace	k1gFnSc3
domácích	domácí	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
útoků	útok	k1gInPc2
výrazně	výrazně	k6eAd1
vzrůstá	vzrůstat	k5eAaImIp3nS
v	v	k7c6
zimě	zima	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
snižuje	snižovat	k5eAaImIp3nS
množství	množství	k1gNnSc1
přirozené	přirozený	k2eAgFnSc2d1
kořisti	kořist	k1gFnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
zimního	zimní	k2eAgInSc2d1
spánku	spánek	k1gInSc2
některých	některý	k3yIgMnPc2
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
především	především	k9
svišťů	svišť	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roční	roční	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
dobytka	dobytek	k1gInSc2
se	se	k3xPyFc4
různí	různit	k5eAaImIp3nP
od	od	k7c2
místa	místo	k1gNnSc2
k	k	k7c3
místu	místo	k1gNnSc3
a	a	k8xC
pohybují	pohybovat	k5eAaImIp3nP
se	se	k3xPyFc4
od	od	k7c2
1	#num#	k4
%	%	kIx~
po	po	k7c6
více	hodně	k6eAd2
než	než	k8xS
12	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
Tibetské	tibetský	k2eAgFnSc6d1
náhorní	náhorní	k2eAgFnSc6d1
plošině	plošina	k1gFnSc6
byly	být	k5eAaImAgInP
začátkem	začátkem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
průměrné	průměrný	k2eAgFnSc2d1
ztráty	ztráta	k1gFnSc2
vyčísleny	vyčíslen	k2eAgFnPc1d1
na	na	k7c4
2	#num#	k4
%	%	kIx~
na	na	k7c6
jednu	jeden	k4xCgFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
vesnici	vesnice	k1gFnSc6
a	a	k8xC
na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
mohly	moct	k5eAaImAgFnP
vzrůst	vzrůst	k5eAaPmF
až	až	k9
k	k	k7c3
9,5	9,5	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Nepálu	Nepál	k1gInSc6
v	v	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Annapurna	Annapurno	k1gNnSc2
ukázala	ukázat	k5eAaPmAgFnS
analýza	analýza	k1gFnSc1
trusu	trus	k1gInSc2
irbisů	irbis	k1gInPc2
přítomnost	přítomnost	k1gFnSc1
zbytků	zbytek	k1gInPc2
domácích	domácí	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
v	v	k7c6
17,8	17,8	k4
%	%	kIx~
případů	případ	k1gInPc2
a	a	k8xC
toto	tento	k3xDgNnSc1
množství	množství	k1gNnSc1
mohlo	moct	k5eAaImAgNnS
v	v	k7c6
zimě	zima	k1gFnSc6
dosáhnout	dosáhnout	k5eAaPmF
až	až	k9
39	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Ladaku	Ladak	k1gInSc6
napadli	napadnout	k5eAaPmAgMnP
v	v	k7c6
zimě	zima	k1gFnSc6
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
irbisové	irbisový	k2eAgFnSc2d1
15	#num#	k4
vesnic	vesnice	k1gFnPc2
a	a	k8xC
usmrtili	usmrtit	k5eAaPmAgMnP
při	při	k7c6
tom	ten	k3xDgNnSc6
95	#num#	k4
ovcí	ovce	k1gFnPc2
a	a	k8xC
koz	koza	k1gFnPc2
a	a	k8xC
jednoho	jeden	k4xCgMnSc2
jaka	jak	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mongolsku	Mongolsko	k1gNnSc6
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
podniknut	podniknut	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
na	na	k7c6
území	území	k1gNnSc6
o	o	k7c6
rozloze	rozloha	k1gFnSc6
200	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
zjistilo	zjistit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
irbisové	irbisový	k2eAgNnSc1d1
zabili	zabít	k5eAaPmAgMnP
osmi	osm	k4xCc3
místním	místní	k2eAgFnPc3d1
rodinám	rodina	k1gFnPc3
13	#num#	k4
koz	koza	k1gFnPc2
a	a	k8xC
ovcí	ovce	k1gFnPc2
<g/>
,	,	kIx,
16	#num#	k4
koní	kůň	k1gMnPc2
(	(	kIx(
<g/>
17	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
7	#num#	k4
jaků	jak	k1gMnPc2
(	(	kIx(
<g/>
11,9	11,9	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Mongolsku	Mongolsko	k1gNnSc6
se	se	k3xPyFc4
irbisové	irbisový	k2eAgFnPc1d1
zaměřují	zaměřovat	k5eAaImIp3nP
na	na	k7c4
koně	kůň	k1gMnSc4
(	(	kIx(
<g/>
vesměs	vesměs	k6eAd1
hříbata	hříbě	k1gNnPc1
<g/>
)	)	kIx)
a	a	k8xC
jaky	jak	k1gMnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
tito	tento	k3xDgMnPc1
jsou	být	k5eAaImIp3nP
mnohdy	mnohdy	k6eAd1
ponecháváni	ponecháván	k2eAgMnPc1d1
na	na	k7c6
pastvě	pastva	k1gFnSc6
nehlídáni	hlídat	k5eNaImNgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Napadení	napadení	k1gNnSc1
člověka	člověk	k1gMnSc2
irbisem	irbis	k1gInSc7
</s>
<s>
Ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
člověku	člověk	k1gMnSc3
je	být	k5eAaImIp3nS
irbis	irbis	k1gInSc1
velmi	velmi	k6eAd1
plachý	plachý	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
zraněný	zraněný	k2eAgMnSc1d1
<g/>
,	,	kIx,
člověka	člověk	k1gMnSc4
napadne	napadnout	k5eAaPmIp3nS
zcela	zcela	k6eAd1
výjimečně	výjimečně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Byly	být	k5eAaImAgFnP
zaznamenány	zaznamenat	k5eAaPmNgInP
případy	případ	k1gInPc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
neozbrojení	ozbrojený	k2eNgMnPc1d1
vesničané	vesničan	k1gMnPc1
utloukli	utlouct	k5eAaPmAgMnP
či	či	k8xC
ukamenovali	ukamenovat	k5eAaPmAgMnP
irbisy	irbis	k1gInPc4
chycené	chycený	k2eAgInPc4d1
při	při	k7c6
útoku	útok	k1gInSc6
na	na	k7c4
domácí	domácí	k2eAgNnPc4d1
zvířata	zvíře	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
se	se	k3xPyFc4
irbis	irbis	k1gInSc1
nechal	nechat	k5eAaPmAgInS
dokonce	dokonce	k9
odtáhnout	odtáhnout	k5eAaPmF
za	za	k7c4
ocas	ocas	k1gInSc4
od	od	k7c2
zraněné	zraněný	k2eAgFnSc2d1
ovce	ovce	k1gFnSc2
a	a	k8xC
pak	pak	k6eAd1
byl	být	k5eAaImAgMnS
zabit	zabít	k5eAaPmNgMnS
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
bránit	bránit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
oblasti	oblast	k1gFnSc6
bývalého	bývalý	k2eAgInSc2d1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
jsou	být	k5eAaImIp3nP
doloženy	doložit	k5eAaPmNgInP
pouze	pouze	k6eAd1
dva	dva	k4xCgInPc4
případy	případ	k1gInPc4
napadení	napadení	k1gNnPc2
člověka	člověk	k1gMnSc4
irbisem	irbis	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc4
případ	případ	k1gInSc4
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgMnS
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1940	#num#	k4
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Almaty	Almata	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
za	za	k7c2
bílého	bílý	k2eAgInSc2d1
dne	den	k1gInSc2
irbis	irbis	k1gFnPc2
napadl	napadnout	k5eAaPmAgMnS
a	a	k8xC
vážně	vážně	k6eAd1
zranil	zranit	k5eAaPmAgMnS
dvě	dva	k4xCgFnPc4
osoby	osoba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvíře	zvíře	k1gNnSc1
bylo	být	k5eAaImAgNnS
vystopováno	vystopovat	k5eAaPmNgNnS
a	a	k8xC
zabito	zabít	k5eAaPmNgNnS
<g/>
,	,	kIx,
pitva	pitva	k1gFnSc1
ukázala	ukázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
irbis	irbis	k1gInSc1
byl	být	k5eAaImAgInS
nakažený	nakažený	k2eAgInSc1d1
vzteklinou	vzteklina	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
zimě	zima	k1gFnSc6
<g/>
,	,	kIx,
také	také	k9
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
Almaty	Almata	k1gFnSc2
<g/>
,	,	kIx,
starý	starý	k2eAgInSc4d1
a	a	k8xC
vyčerpaný	vyčerpaný	k2eAgInSc4d1
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
bezzubý	bezzubý	k2eAgInSc4d1
irbis	irbis	k1gInSc4
skočil	skočit	k5eAaPmAgMnS
ze	z	k7c2
skály	skála	k1gFnSc2
na	na	k7c4
člověka	člověk	k1gMnSc4
procházejícího	procházející	k2eAgMnSc4d1
pod	pod	k7c7
ní	on	k3xPp3gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
šelmu	šelma	k1gMnSc4
zneškodnil	zneškodnit	k5eAaPmAgMnS
klackem	klacek	k1gMnSc7
<g/>
,	,	kIx,
svázal	svázat	k5eAaPmAgMnS
a	a	k8xC
dopravil	dopravit	k5eAaPmAgMnS
do	do	k7c2
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbis	Irbis	k1gFnSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
Ve	v	k7c6
filmu	film	k1gInSc6
</s>
<s>
Dokumentární	dokumentární	k2eAgInPc1d1
filmy	film	k1gInPc1
</s>
<s>
Nafilmovat	nafilmovat	k5eAaPmF
irbisa	irbis	k1gMnSc4
ve	v	k7c6
volné	volný	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
obtížné	obtížný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
je	být	k5eAaImIp3nS
vzácnost	vzácnost	k1gFnSc1
zvířete	zvíře	k1gNnSc2
a	a	k8xC
nedostupnost	nedostupnost	k1gFnSc4
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
biotopu	biotop	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
i	i	k9
o	o	k7c6
irbisech	irbis	k1gInPc6
pojednával	pojednávat	k5eAaImAgMnS
druhý	druhý	k4xOgInSc4
díl	díl	k1gInSc4
dokumentárního	dokumentární	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
britské	britský	k2eAgFnSc2d1
televizní	televizní	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
BBC	BBC	kA
Planeta	planeta	k1gFnSc1
Země	země	k1gFnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
rámci	rámec	k1gInSc6
jiného	jiný	k2eAgInSc2d1
dokumentárního	dokumentární	k2eAgInSc2d1
seriálu	seriál	k1gInSc2
BBC	BBC	kA
Natural	Natural	k?
World	World	k1gInSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
odvysílána	odvysílán	k2eAgFnSc1d1
epizoda	epizoda	k1gFnSc1
„	„	k?
<g/>
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
–	–	k?
Beyond	Beyond	k1gMnSc1
the	the	k?
Myth	Myth	k1gMnSc1
<g/>
“	“	k?
věnovaná	věnovaný	k2eAgNnPc1d1
irbisům	irbis	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fikce	fikce	k1gFnSc1
</s>
<s>
Podobu	podoba	k1gFnSc4
sněžného	sněžný	k2eAgMnSc2d1
levharta	levhart	k1gMnSc2
(	(	kIx(
<g/>
irbisa	irbisa	k1gFnSc1
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
Tai	Tai	k1gMnSc1
Lung	Lung	k1gMnSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgMnSc1d1
padouch	padouch	k1gMnSc1
v	v	k7c6
americkém	americký	k2eAgInSc6d1
animovaném	animovaný	k2eAgInSc6d1
filmu	film	k1gInSc6
pro	pro	k7c4
děti	dítě	k1gFnPc4
Kung	Kunga	k1gFnPc2
Fu	fu	k0
Panda	panda	k1gFnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbis	Irbis	k1gFnSc1
na	na	k7c6
kazašské	kazašský	k2eAgFnSc6d1
bankovce	bankovka	k1gFnSc6
s	s	k7c7
nominální	nominální	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
10	#num#	k4
000	#num#	k4
tenge	tenge	k1gInPc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
text	text	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Numizmatika	numizmatika	k1gFnSc1
a	a	k8xC
poštovní	poštovní	k2eAgFnPc1d1
známky	známka	k1gFnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
byla	být	k5eAaImAgFnS
jako	jako	k9
součást	součást	k1gFnSc1
série	série	k1gFnSc2
Қ	Қ	k?
қ	қ	k?
к	к	k?
(	(	kIx(
<g/>
Červená	červený	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
Kazachstánu	Kazachstán	k1gInSc2
<g/>
)	)	kIx)
vyražena	vyražen	k2eAgFnSc1d1
série	série	k1gFnSc1
mincí	mince	k1gFnPc2
v	v	k7c6
nominální	nominální	k2eAgFnSc6d1
hodnotě	hodnota	k1gFnSc6
500	#num#	k4
tenge	tenge	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
její	její	k3xOp3gFnSc6
zadní	zadní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
irbis	irbis	k1gFnSc1
jdoucí	jdoucí	k2eAgFnSc1d1
po	po	k7c6
skále	skála	k1gFnSc6
<g/>
,	,	kIx,
se	s	k7c7
stylizovanou	stylizovaný	k2eAgFnSc7d1
horou	hora	k1gFnSc7
v	v	k7c6
pozadí	pozadí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
vyražena	vyražen	k2eAgFnSc1d1
zlatá	zlatý	k2eAgFnSc1d1
mince	mince	k1gFnSc1
rovněž	rovněž	k9
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
500	#num#	k4
tenge	tenge	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
jejíž	jejíž	k3xOyRp3gFnSc6
rubové	rubový	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
hlava	hlava	k1gFnSc1
irbisa	irbisa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
98	#num#	k4
<g/>
]	]	kIx)
Irbis	Irbis	k1gFnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
vyobrazen	vyobrazit	k5eAaPmNgInS
na	na	k7c6
kazašské	kazašský	k2eAgFnSc6d1
bankovce	bankovka	k1gFnSc6
s	s	k7c7
nominální	nominální	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
10	#num#	k4
000	#num#	k4
tenge	tenge	k1gInPc2
<g/>
:	:	kIx,
v	v	k7c6
oběhu	oběh	k1gInSc6
od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
rozhodnutím	rozhodnutí	k1gNnSc7
z	z	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
stažena	stažen	k2eAgFnSc1d1
s	s	k7c7
oběhu	oběh	k1gInSc6
<g/>
,	,	kIx,
bankami	banka	k1gFnPc7
přijímána	přijímán	k2eAgNnPc4d1
k	k	k7c3
výměně	výměna	k1gFnSc3
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Památeční	památeční	k2eAgFnSc1d1
mince	mince	k1gFnSc1
s	s	k7c7
vyobrazením	vyobrazení	k1gNnSc7
irbisa	irbisa	k1gFnSc1
byly	být	k5eAaImAgFnP
vydány	vydat	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
také	také	k6eAd1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
v	v	k7c6
rámci	rámec	k1gInSc6
souboru	soubor	k1gInSc2
С	С	k?
н	н	k?
м	м	k?
(	(	kIx(
<g/>
Zachraňme	zachránit	k5eAaPmRp1nP
náš	náš	k3xOp1gInSc4
svět	svět	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyraženo	vyražen	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
celkem	celkem	k6eAd1
sedm	sedm	k4xCc4
sérií	série	k1gFnPc2
památečních	památeční	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
stříbrné	stříbrný	k2eAgFnPc4d1
mince	mince	k1gFnPc4
v	v	k7c6
nominální	nominální	k2eAgFnSc6d1
hodnotě	hodnota	k1gFnSc6
3	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
a	a	k8xC
100	#num#	k4
rublů	rubl	k1gInPc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
galerie	galerie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
zlaté	zlatý	k2eAgFnPc4d1
mince	mince	k1gFnPc4
s	s	k7c7
nominální	nominální	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
a	a	k8xC
10	#num#	k4
000	#num#	k4
rublů	rubl	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
zadní	zadní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
50	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
rublové	rublový	k2eAgFnPc4d1
mince	mince	k1gFnPc4
byla	být	k5eAaImAgFnS
vyobrazena	vyobrazen	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
irbisa	irbisa	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
minci	mince	k1gFnSc6
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
100	#num#	k4
rublů	rubl	k1gInPc2
irbis	irbis	k1gFnPc2
na	na	k7c6
kmeni	kmen	k1gInSc6
stromu	strom	k1gInSc2
připravený	připravený	k2eAgInSc4d1
ke	k	k7c3
skoku	skok	k1gInSc3
<g/>
,	,	kIx,
na	na	k7c6
minci	mince	k1gFnSc6
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
200	#num#	k4
rublů	rubl	k1gInPc2
stojící	stojící	k2eAgFnSc1d1
irbis	irbis	k1gFnSc1
na	na	k7c6
pozadí	pozadí	k1gNnSc6
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
letící	letící	k2eAgMnSc1d1
orel	orel	k1gMnSc1
a	a	k8xC
slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
minci	mince	k1gFnSc6
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
10	#num#	k4
000	#num#	k4
rublů	rubl	k1gInPc2
pak	pak	k6eAd1
irbis	irbis	k1gFnSc7
s	s	k7c7
dvěma	dva	k4xCgNnPc7
mláďaty	mládě	k1gNnPc7
na	na	k7c6
pozadí	pozadí	k1gNnSc6
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
vesnice	vesnice	k1gFnSc2
<g/>
,	,	kIx,
letící	letící	k2eAgMnSc1d1
orel	orel	k1gMnSc1
a	a	k8xC
dva	dva	k4xCgMnPc1
jezdci	jezdec	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Národní	národní	k2eAgFnSc1d1
banka	banka	k1gFnSc1
Kyrgyzstánu	Kyrgyzstán	k1gInSc2
vydala	vydat	k5eAaPmAgFnS
v	v	k7c6
letech	léto	k1gNnPc6
2012	#num#	k4
a	a	k8xC
2015	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
série	série	k1gFnSc2
К	К	k?
К	К	k?
к	к	k?
(	(	kIx(
<g/>
Červená	červený	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
Kyrgyzstánu	Kyrgyzstán	k1gInSc2
<g/>
)	)	kIx)
dvě	dva	k4xCgFnPc1
mince	mince	k1gFnPc1
s	s	k7c7
vyobrazením	vyobrazení	k1gNnSc7
irbisa	irbis	k1gMnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
galerie	galerie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbis	Irbis	k1gFnSc1
na	na	k7c6
poštovních	poštovní	k2eAgFnPc6d1
známkách	známka	k1gFnPc6
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
SSSR	SSSR	kA
</s>
<s>
Irbis	Irbis	k1gFnSc1
na	na	k7c6
mincích	mince	k1gFnPc6
</s>
<s>
Revers	revers	k1gInSc1
stříbrné	stříbrný	k2eAgFnSc2d1
pamětní	pamětní	k2eAgFnSc2d1
mince	mince	k1gFnSc2
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
3	#num#	k4
rubly	rubl	k1gInPc4
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
…	…	k?
25	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
…	…	k?
100	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
Revers	revers	k1gInSc1
zlaté	zlatý	k2eAgFnSc2d1
pamětní	pamětní	k2eAgFnSc2d1
mince	mince	k1gFnSc2
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
100	#num#	k4
rublů	rubl	k1gInPc2
(	(	kIx(
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
…	…	k?
10	#num#	k4
000	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
Revers	revers	k1gInSc1
stříbrné	stříbrný	k2eAgFnSc2d1
pamětní	pamětní	k2eAgFnSc2d1
mince	mince	k1gFnSc2
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
500	#num#	k4
tenge	tenge	k1gInPc2
(	(	kIx(
<g/>
Kazachstán	Kazachstán	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Revers	revers	k1gInSc1
zlaté	zlatý	k2eAgFnSc2d1
pamětní	pamětní	k2eAgFnSc2d1
mince	mince	k1gFnSc2
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
500	#num#	k4
tenge	tenge	k1gInPc2
(	(	kIx(
<g/>
Kazachstán	Kazachstán	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Avers	avers	k1gInSc1
stříbrné	stříbrný	k2eAgFnSc2d1
pamětní	pamětní	k2eAgFnSc2d1
mince	mince	k1gFnSc2
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
10	#num#	k4
somů	som	k1gInPc2
(	(	kIx(
<g/>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Avers	avers	k1gInSc1
zlaté	zlatý	k2eAgFnSc2d1
pamětní	pamětní	k2eAgFnSc2d1
mince	mince	k1gFnSc2
o	o	k7c6
hodnotě	hodnota	k1gFnSc6
200	#num#	k4
somů	som	k1gInPc2
(	(	kIx(
<g/>
Kyrgyzstán	Kyrgyzstán	k1gInSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
heraldice	heraldika	k1gFnSc6
</s>
<s>
Jelikož	jelikož	k8xS
irbisa	irbisa	k1gFnSc1
ve	v	k7c6
středověku	středověk	k1gInSc6
Evropa	Evropa	k1gFnSc1
neznala	znát	k5eNaImAgFnS,k5eAaImAgFnS
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
pro	pro	k7c4
Evropany	Evropan	k1gMnPc4
velmi	velmi	k6eAd1
exotickým	exotický	k2eAgNnSc7d1
zvířetem	zvíře	k1gNnSc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc1
vyobrazení	vyobrazení	k1gNnSc1
v	v	k7c6
heraldice	heraldika	k1gFnSc6
jsou	být	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
vzácná	vzácný	k2eAgFnSc1d1
a	a	k8xC
výhradně	výhradně	k6eAd1
novodobá	novodobý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Irbis	Irbis	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
erbech	erb	k1gInPc6
objevuje	objevovat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
postsovětské	postsovětský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Běžnou	běžný	k2eAgFnSc7d1
heraldickou	heraldický	k2eAgFnSc7d1
figurou	figura	k1gFnSc7
je	být	k5eAaImIp3nS
irbisovi	irbisův	k2eAgMnPc1d1
podobný	podobný	k2eAgMnSc1d1
levhart	levhart	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
mohl	moct	k5eAaImAgMnS
mít	mít	k5eAaImF
i	i	k9
bílou	bílý	k2eAgFnSc4d1
(	(	kIx(
<g/>
případně	případně	k6eAd1
stříbrnou	stříbrný	k2eAgFnSc4d1
<g/>
)	)	kIx)
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ruštině	ruština	k1gFnSc6
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
slova	slovo	k1gNnSc2
„	„	k?
<g/>
б	б	k?
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
л	л	k?
<g/>
“	“	k?
synonymy	synonymum	k1gNnPc7
<g/>
,	,	kIx,
obě	dva	k4xCgFnPc1
označovala	označovat	k5eAaImAgFnS
levharta	levhart	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postupem	k7c2
času	čas	k1gInSc2
se	se	k3xPyFc4
však	však	k9
slovo	slovo	k1gNnSc1
„	„	k?
<g/>
б	б	k?
<g/>
“	“	k?
vžilo	vžít	k5eAaPmAgNnS
jako	jako	k9
označení	označení	k1gNnSc1
pro	pro	k7c4
irbisa	irbis	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
označován	označovat	k5eAaImNgInS
pouze	pouze	k6eAd1
souslovím	sousloví	k1gNnSc7
„	„	k?
<g/>
с	с	k?
б	б	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
heraldice	heraldika	k1gFnSc6
postsovětských	postsovětský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
tak	tak	k9
„	„	k?
<g/>
б	б	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
především	především	k6eAd1
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgInS
být	být	k5eAaImF
chápán	chápat	k5eAaImNgInS
jako	jako	k9
irbis	irbis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okřídlený	okřídlený	k2eAgInSc1d1
bílý	bílý	k2eAgInSc1d1
„	„	k?
<g/>
б	б	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
chápaný	chápaný	k2eAgInSc1d1
jako	jako	k9
irbis	irbis	k1gFnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ve	v	k7c6
znaku	znak	k1gInSc6
republiky	republika	k1gFnSc2
Tatarstán	Tatarstán	k1gInSc1
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
uzbeckého	uzbecký	k2eAgNnSc2d1
města	město	k1gNnSc2
Samarkand	Samarkanda	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
znacích	znak	k1gInPc6
se	se	k3xPyFc4
však	však	k9
vyskytuje	vyskytovat	k5eAaImIp3nS
i	i	k9
běžně	běžně	k6eAd1
zbarvený	zbarvený	k2eAgMnSc1d1
(	(	kIx(
<g/>
tedy	tedy	k8xC
skvrnitý	skvrnitý	k2eAgInSc1d1
<g/>
)	)	kIx)
irbis	irbis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
symbol	symbol	k1gInSc1
kazašského	kazašský	k2eAgNnSc2d1
města	město	k1gNnSc2
Almaty	Almata	k1gFnSc2
je	být	k5eAaImIp3nS
irbis	irbis	k1gInSc1
vyobrazen	vyobrazen	k2eAgInSc1d1
na	na	k7c6
jeho	jeho	k3xOp3gInSc6
znaku	znak	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Stylizovaný	stylizovaný	k2eAgInSc1d1
okřídlený	okřídlený	k2eAgInSc1d1
irbis	irbis	k1gInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
znaku	znak	k1gInSc6
ruské	ruský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Chakasie	Chakasie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
Irbisa	Irbisa	k1gFnSc1
má	mít	k5eAaImIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
znaku	znak	k1gInSc6
také	také	k9
Biškek	Biškek	k1gMnSc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Kyrgyzstánu	Kyrgyzstán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Znak	znak	k1gInSc4
republiky	republika	k1gFnSc2
Tatarstán	Tatarstán	k2eAgInSc4d1
</s>
<s>
Znak	znak	k1gInSc1
republiky	republika	k1gFnSc2
Chakasie	Chakasie	k1gFnSc2
</s>
<s>
Irbis	Irbis	k1gFnSc1
ve	v	k7c6
znaku	znak	k1gInSc6
kazašského	kazašský	k2eAgNnSc2d1
města	město	k1gNnSc2
Almaty	Almata	k1gFnSc2
</s>
<s>
Znak	znak	k1gInSc1
Samarkandu	Samarkand	k1gInSc2
</s>
<s>
Irbis	Irbis	k1gFnSc1
na	na	k7c6
znaku	znak	k1gInSc6
Šušenského	Šušenský	k2eAgInSc2d1
rajónu	rajón	k1gInSc2
Krasnojarského	Krasnojarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Irbis	Irbis	k1gFnSc1
na	na	k7c6
znaku	znak	k1gInSc6
Jermakovského	Jermakovský	k2eAgInSc2d1
rajónu	rajón	k1gInSc2
Krasnojarského	Krasnojarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
</s>
<s>
Irbis	Irbis	k1gFnSc1
na	na	k7c6
znaku	znak	k1gInSc6
kyrgyzského	kyrgyzský	k2eAgNnSc2d1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Biškek	Biškek	k6eAd1
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Sněžný	sněžný	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
(	(	kIx(
<g/>
ocenění	ocenění	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
Sněžný	sněžný	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
</s>
<s>
Horolezcům	horolezec	k1gMnPc3
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
zdolají	zdolat	k5eAaPmIp3nP
všech	všecek	k3xTgFnPc2
pět	pět	k4xCc1
sedmitisícovek	sedmitisícovka	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
bývalého	bývalý	k2eAgInSc2d1
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
náleží	náležet	k5eAaImIp3nS
neoficiální	neoficiální	k2eAgInSc4d1,k2eNgInSc4d1
titul	titul	k1gInSc4
Sněžný	sněžný	k2eAgMnSc1d1
leopard	leopard	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
Oficiální	oficiální	k2eAgNnSc1d1
označení	označení	k1gNnSc1
plakety	plaketa	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
jsou	být	k5eAaImIp3nP
horolezci	horolezec	k1gMnPc1
vyznamenáváni	vyznamenáván	k2eAgMnPc1d1
<g/>
,	,	kIx,
zní	znět	k5eAaImIp3nS
„	„	k?
<g/>
Pokořitel	pokořitel	k1gMnSc1
nejvyšších	vysoký	k2eAgFnPc2d3
hor	hora	k1gFnPc2
SSSR	SSSR	kA
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
П	П	k?
в	в	k?
г	г	k?
С	С	k?
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc4
i	i	k9
podoba	podoba	k1gFnSc1
plakety	plaketa	k1gFnSc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plakety	plaketa	k1gFnPc1
jsou	být	k5eAaImIp3nP
číslované	číslovaný	k2eAgFnPc1d1
<g/>
,	,	kIx,
každý	každý	k3xTgMnSc1
držitel	držitel	k1gMnSc1
má	mít	k5eAaImIp3nS
„	„	k?
<g/>
svojí	svojit	k5eAaImIp3nS
<g/>
“	“	k?
očíslovanou	očíslovaný	k2eAgFnSc4d1
plaketu	plaketa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Irbis	Irbis	k1gInSc1
je	být	k5eAaImIp3nS
vyobrazen	vyobrazit	k5eAaPmNgInS
na	na	k7c6
kyrgyzské	kyrgyzský	k2eAgFnSc6d1
státní	státní	k2eAgFnSc6d1
medaili	medaile	k1gFnSc6
„	„	k?
<g/>
Э	Э	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
Za	za	k7c4
zásluhy	zásluha	k1gFnPc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Irbisa	Irbisa	k1gFnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
„	„	k?
<g/>
б	б	k?
<g/>
“	“	k?
<g/>
,	,	kIx,
kazašsky	kazašsky	k6eAd1
„	„	k?
<g/>
б	б	k?
<g/>
“	“	k?
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
v	v	k7c6
názvu	název	k1gInSc6
a	a	k8xC
v	v	k7c6
logu	log	k1gInSc6
dva	dva	k4xCgInPc4
kluby	klub	k1gInPc4
Kontinentální	kontinentální	k2eAgFnSc2d1
hokejové	hokejový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
:	:	kIx,
ruský	ruský	k2eAgInSc1d1
klub	klub	k1gInSc1
Ak	Ak	k1gFnSc2
Bars	Bars	k?
Kazaň	Kazaň	k1gFnSc1
(	(	kIx(
<g/>
Х	Х	k?
«	«	k?
<g/>
А	А	k?
Б	Б	k?
<g/>
»	»	k?
К	К	k?
<g/>
)	)	kIx)
a	a	k8xC
kazachstánský	kazachstánský	k2eAgInSc1d1
klub	klub	k1gInSc1
Barys	Barys	k1gInSc1
Astana	Astana	k1gFnSc1
(	(	kIx(
<g/>
Х	Х	k?
«	«	k?
<g/>
Б	Б	k?
<g/>
»	»	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Irbis	Irbis	k1gInSc1
byl	být	k5eAaImAgInS
maskotem	maskot	k1gInSc7
Zimních	zimní	k2eAgFnPc2d1
Asijských	asijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgMnS
rovněž	rovněž	k9
jedním	jeden	k4xCgInSc7
ze	z	k7c2
tří	tři	k4xCgInPc2
zvířecích	zvířecí	k2eAgInPc2d1
maskotů	maskot	k1gInPc2
Zimních	zimní	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
2014	#num#	k4
v	v	k7c6
ruském	ruský	k2eAgNnSc6d1
Soči	Soči	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Informační	informační	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
</s>
<s>
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
Mac	Mac	kA
OS	OS	kA
X	X	kA
10.6	10.6	k4
od	od	k7c2
společnosti	společnost	k1gFnSc2
Apple	Apple	kA
<g/>
,	,	kIx,
uvedený	uvedený	k2eAgInSc4d1
na	na	k7c4
trh	trh	k1gInSc4
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
nesl	nést	k5eAaImAgMnS
název	název	k1gInSc4
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
sněžný	sněžný	k2eAgMnSc1d1
levhart	levhart	k1gMnSc1
(	(	kIx(
<g/>
irbis	irbis	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Samice	samice	k1gFnPc1
mezidruhových	mezidruhový	k2eAgMnPc2d1
kříženců	kříženec	k1gMnPc2
u	u	k7c2
kočkovitých	kočkovitý	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
plodné	plodný	k2eAgFnPc1d1
<g/>
,	,	kIx,
samci	samec	k1gMnPc1
neplodní	plodní	k2eNgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Pokud	pokud	k8xS
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
údaj	údaj	k1gInSc1
považován	považován	k2eAgInSc1d1
za	za	k7c4
věrohodný	věrohodný	k2eAgInSc4d1
<g/>
,	,	kIx,
šlo	jít	k5eAaImAgNnS
by	by	kYmCp3nP
o	o	k7c4
nejdelší	dlouhý	k2eAgInSc4d3
zaznamenaný	zaznamenaný	k2eAgInSc4d1
skok	skok	k1gInSc4
u	u	k7c2
kočkovitých	kočkovitý	k2eAgFnPc2d1
šelem	šelma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Irbis	Irbis	k1gFnSc1
by	by	kYmCp3nS
tak	tak	k9
skokanskými	skokanský	k2eAgFnPc7d1
schopnostmi	schopnost	k1gFnPc7
předčil	předčit	k5eAaBmAgMnS,k5eAaPmAgMnS
i	i	k9
tygra	tygr	k1gMnSc4
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yIgNnSc2,k3yQgNnSc2,k3yRgNnSc2
jsou	být	k5eAaImIp3nP
doloženy	doložen	k2eAgInPc4d1
skoky	skok	k1gInPc4
o	o	k7c6
délce	délka	k1gFnSc6
9	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Po	po	k7c6
zpracování	zpracování	k1gNnSc6
se	se	k3xPyFc4
ovšem	ovšem	k9
irbisí	irbisí	k1gNnSc2
kůže	kůže	k1gFnSc2
prodávaly	prodávat	k5eAaImAgInP
za	za	k7c4
daleko	daleko	k6eAd1
vyšší	vysoký	k2eAgFnPc4d2
částky	částka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
aukci	aukce	k1gFnSc6
v	v	k7c6
Leningradě	Leningrad	k1gInSc6
roku	rok	k1gInSc2
1956	#num#	k4
se	se	k3xPyFc4
všech	všecek	k3xTgFnPc2
120	#num#	k4
vystavených	vystavený	k2eAgFnPc2d1
kůží	kůže	k1gFnPc2
prodalo	prodat	k5eAaPmAgNnS
do	do	k7c2
USA	USA	kA
při	při	k7c6
průměrné	průměrný	k2eAgFnSc6d1
ceně	cena	k1gFnSc6
11	#num#	k4
dolarů	dolar	k1gInPc2
25	#num#	k4
centů	cent	k1gInPc2
za	za	k7c4
kus	kus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
И	И	k?
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
MCCARTHY	MCCARTHY	kA
<g/>
,	,	kIx,
T.	T.	kA
M.	M.	kA
<g/>
;	;	kIx,
CHAPRON	CHAPRON	kA
<g/>
,	,	kIx,
G.	G.	kA
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Survival	Survival	k1gFnSc4
Strategy	Stratega	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seattle	Seattle	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
ISLT	ISLT	kA
and	and	k?
SLN	SLN	kA
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
24	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
15	#num#	k4
a	a	k8xC
Table	tablo	k1gNnSc6
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
24	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
R.	R.	kA
<g/>
;	;	kIx,
MALLON	MALLON	kA
<g/>
,	,	kIx,
D.	D.	kA
<g/>
;	;	kIx,
MCCARTHY	MCCARTHY	kA
<g/>
,	,	kIx,
T.	T.	kA
<g/>
;	;	kIx,
CHUNDAWAY	CHUNDAWAY	kA
<g/>
,	,	kIx,
R.	R.	kA
A.	A.	kA
<g/>
;	;	kIx,
HABIB	HABIB	kA
<g/>
,	,	kIx,
B.	B.	kA
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gFnPc2
Union	union	k1gInSc4
for	forum	k1gNnPc2
Conservation	Conservation	k1gInSc1
of	of	k?
Nature	Natur	k1gMnSc5
<g/>
,	,	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
Rodney	Rodnea	k1gFnSc2
<g/>
;	;	kIx,
MALLON	MALLON	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
;	;	kIx,
MISHRA	MISHRA	kA
<g/>
,	,	kIx,
Chadurutt	Chadurutt	k1gMnSc1
<g/>
;	;	kIx,
NORAS	NORAS	kA
<g/>
,	,	kIx,
Sibylle	Sibylle	k1gFnSc1
<g/>
;	;	kIx,
SHARMA	SHARMA	kA
<g/>
,	,	kIx,
Rishi	Rishi	k1gNnSc1
<g/>
;	;	kIx,
SURYAWANSHI	SURYAWANSHI	kA
<g/>
,	,	kIx,
Kulbhushansingh	Kulbhushansingh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Survival	Survival	k1gMnSc1
Strategy	Stratega	k1gFnSc2
(	(	kIx(
<g/>
Revised	Revised	k1gInSc1
Version	Version	k1gInSc1
2014.1	2014.1	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seattle	Seattle	k1gFnSc1
<g/>
:	:	kIx,
Snow	Snow	k1gFnSc1
Leopard	leopard	k1gMnSc1
Network	network	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
145	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
MCCARTHY	MCCARTHY	kA
<g/>
,	,	kIx,
T.	T.	kA
<g/>
;	;	kIx,
MALLON	MALLON	kA
<g/>
,	,	kIx,
D.	D.	kA
<g/>
;	;	kIx,
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
R.	R.	kA
<g/>
;	;	kIx,
ZAHLER	ZAHLER	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
;	;	kIx,
MCCARTHY	MCCARTHY	kA
<g/>
,	,	kIx,
K.	K.	kA
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gFnPc2
Union	union	k1gInSc4
for	forum	k1gNnPc2
Conservation	Conservation	k1gInSc1
of	of	k?
Nature	Natur	k1gMnSc5
<g/>
,	,	kIx,
2016-11-08	2016-11-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Securing	Securing	k1gInSc1
at	at	k?
least	least	k1gInSc1
20	#num#	k4
healthy	healtha	k1gFnSc2
populations	populationsa	k1gFnPc2
of	of	k?
snow	snow	k?
leopards	leopards	k1gInSc1
across	across	k1gInSc1
the	the	k?
cat	cat	k?
<g/>
’	’	k?
<g/>
s	s	k7c7
range	range	k1gFnSc7
by	by	k9
2020	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Global	globat	k5eAaImAgMnS
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
&	&	k?
Ecosystem	Ecosyst	k1gInSc7
Protection	Protection	k1gInSc4
Program	program	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
SUNQUIST	SUNQUIST	kA
<g/>
,	,	kIx,
Mel	mlít	k5eAaImRp2nS
<g/>
;	;	kIx,
SUNQUIST	SUNQUIST	kA
<g/>
,	,	kIx,
Fiona	Fiono	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wild	Wild	k1gInSc1
Cats	Cats	k1gInSc4
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Chicago	Chicago	k1gNnSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
226	#num#	k4
<g/>
-	-	kIx~
<g/>
77999	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
389	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Sunquist	Sunquist	k1gMnSc1
&	&	k?
Sunquist	Sunquist	k1gMnSc1
2002	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Ф	Ф	k?
<g/>
,	,	kIx,
М	М	k?
<g/>
.	.	kIx.
Э	Э	k?
с	с	k?
р	р	k?
я	я	k?
<g/>
.	.	kIx.
<g/>
Т	Т	k?
2	#num#	k4
<g/>
:	:	kIx,
Е	Е	k?
—	—	k?
М	М	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
П	П	k?
<g/>
,	,	kIx,
1967	#num#	k4
<g/>
.	.	kIx.
671	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
и	и	k?
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HAVLOVÁ	Havlová	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc4
poznámek	poznámka	k1gFnPc2
k	k	k7c3
počátkům	počátek	k1gInPc3
české	český	k2eAgFnSc2d1
zoologické	zoologický	k2eAgFnSc2d1
terminologie	terminologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gFnSc1
řeč	řeč	k1gFnSc1
<g/>
.	.	kIx.
1992	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
75	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
205	#num#	k4
<g/>
-	-	kIx~
<g/>
211	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Constellations	Constellations	k1gInSc1
of	of	k?
Words	Words	k1gInSc1
<g/>
:	:	kIx,
Lynx	Lynx	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
ALLEN	Allen	k1gMnSc1
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
A.	A.	kA
English	English	k1gInSc1
Doublets	Doublets	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Modern	Modern	k1gInSc1
Language	language	k1gFnPc2
Association	Association	k1gInSc4
of	of	k?
America	Americ	k1gInSc2
<g/>
,	,	kIx,
1908	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
214	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Oxford	Oxford	k1gInSc1
English	Englisha	k1gFnPc2
Dictionary	Dictionara	k1gFnSc2
(	(	kIx(
<g/>
heslo	heslo	k1gNnSc1
ounce	ounce	k1gFnSc2
<g/>
,	,	kIx,
n.	n.	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2016-06	2016-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Thomas	Thomas	k1gMnSc1
Bauer	Bauer	k1gMnSc1
<g/>
:	:	kIx,
Nurbu	Nurb	k1gInSc2
–	–	k?
Im	Im	k1gMnSc1
Reich	Reich	k?
des	des	k1gNnSc2
Schneeleoparden	Schneeleopardna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wiesenburg	Wiesenburg	k1gMnSc1
Verlag	Verlag	k1gMnSc1
<g/>
,	,	kIx,
Schweinfurt	Schweinfurt	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
С	С	k?
<g/>
,	,	kIx,
С	С	k?
У	У	k?
<g/>
.	.	kIx.
З	З	k?
С	С	k?
<g/>
.	.	kIx.
Х	Х	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
А	А	k?
н	н	k?
С	С	k?
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
.	.	kIx.
460	#num#	k4
s.	s.	k?
S.	S.	kA
421	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Stroganov	Stroganov	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
Sunquist	Sunquist	k1gMnSc1
&	&	k?
Sunquist	Sunquist	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3781	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Stroganov	Stroganovo	k1gNnPc2
<g/>
,	,	kIx,
s.	s.	k?
4221	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
HEMMER	HEMMER	kA
<g/>
,	,	kIx,
Helmut	Helmut	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uncia	uncia	k1gFnSc1
uncia	uncia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mammalian	Mammalian	k1gInSc1
Species	species	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1972-11-29	1972-11-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
20	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
3503882	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Г	Г	k?
<g/>
,	,	kIx,
В	В	k?
Г	Г	k?
<g/>
;	;	kIx,
Н	Н	k?
<g/>
,	,	kIx,
Н	Н	k?
П	П	k?
М	М	k?
С	С	k?
С	С	k?
<g/>
.	.	kIx.
Х	Х	k?
(	(	kIx(
<g/>
г	г	k?
и	и	k?
к	к	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
В	В	k?
ш	ш	k?
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
552	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Р	Р	k?
с	с	k?
б	б	k?
<g/>
,	,	kIx,
и	и	k?
и	и	k?
<g/>
,	,	kIx,
s.	s.	k?
212	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
JOHNSON	JOHNSON	kA
<g/>
,	,	kIx,
Warren	Warrna	k1gFnPc2
E.	E.	kA
<g/>
;	;	kIx,
EIZIRIK	EIZIRIK	kA
<g/>
,	,	kIx,
Eduardo	Eduardo	k1gNnSc1
<g/>
;	;	kIx,
PECON-SLATTERY	PECON-SLATTERY	k1gMnSc1
<g/>
,	,	kIx,
Jill	Jill	k1gMnSc1
<g/>
;	;	kIx,
MURPHY	MURPHY	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
J.	J.	kA
<g/>
;	;	kIx,
ANTUNES	ANTUNES	kA
<g/>
,	,	kIx,
Agostinho	Agostin	k1gMnSc2
<g/>
;	;	kIx,
TEELING	TEELING	kA
<g/>
,	,	kIx,
Emma	Emma	k1gFnSc1
<g/>
;	;	kIx,
O	O	kA
<g/>
'	'	kIx"
<g/>
BRIEN	BRIEN	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
J.	J.	kA
The	The	k1gMnSc7
Late	lat	k1gInSc5
Miocene	Miocen	k1gInSc5
radiation	radiation	k1gInSc4
of	of	k?
modern	modern	k1gInSc1
Felidae	Felidae	k1gInSc1
<g/>
:	:	kIx,
A	a	k9
genetic	genetice	k1gFnPc2
assessment	assessment	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
sv.	sv.	kA
311	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5757	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
73	#num#	k4
<g/>
—	—	k?
<g/>
77	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.1122277	.1122277	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16400146	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
JOHNSON	JOHNSON	kA
<g/>
,	,	kIx,
W.	W.	kA
E.	E.	kA
<g/>
;	;	kIx,
O	O	kA
<g/>
'	'	kIx"
<g/>
BRIEN	BRIEN	kA
<g/>
,	,	kIx,
S.	S.	kA
J.	J.	kA
Phylogenetic	Phylogenetice	k1gFnPc2
reconstruction	reconstruction	k1gInSc4
of	of	k?
the	the	k?
Felidae	Felidae	k1gInSc1
using	using	k1gInSc1
16S	16S	k4
rRNA	rRNA	k?
and	and	k?
NADH-5	NADH-5	k1gMnSc1
mitochondrial	mitochondrial	k1gMnSc1
genes	genes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Molecular	Molecular	k1gInSc1
Evolution	Evolution	k1gInSc1
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
44	#num#	k4
Suppl	Suppl	k1gFnPc2
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
98	#num#	k4
<g/>
–	–	k?
<g/>
116	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
/	/	kIx~
PL	PL	kA
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
9071018	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LI	li	k9
<g/>
,	,	kIx,
Yu	Yu	k1gMnSc1
<g/>
;	;	kIx,
ZHANG	ZHANG	kA
<g/>
,	,	kIx,
Ya-ping	Ya-ping	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phylogenetic	Phylogenetice	k1gFnPc2
studies	studiesa	k1gFnPc2
of	of	k?
pantherine	pantherin	k1gInSc5
cats	cats	k1gInSc1
(	(	kIx(
<g/>
Felidae	Felidae	k1gFnSc1
<g/>
)	)	kIx)
based	based	k1gMnSc1
on	on	k3xPp3gMnSc1
multiple	multipl	k1gInSc5
genes	genes	k1gMnSc1
<g/>
,	,	kIx,
with	with	k1gMnSc1
novel	novela	k1gFnPc2
application	application	k1gInSc4
of	of	k?
nuclear	nuclear	k1gInSc1
β	β	k1gInSc1
intron	intron	k1gInSc1
7	#num#	k4
to	ten	k3xDgNnSc4
carnivores	carnivores	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Molecular	Molecular	k1gInSc1
Phylogenetics	Phylogenetics	k1gInSc4
and	and	k?
Evolution	Evolution	k1gInSc1
<g/>
.	.	kIx.
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
35	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1055	#num#	k4
<g/>
-	-	kIx~
<g/>
7903	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
ympev	ympev	k1gFnSc1
<g/>
.2005	.2005	k4
<g/>
.01	.01	k4
<g/>
.017	.017	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WEI	WEI	kA
<g/>
,	,	kIx,
Lei	lei	k1gInSc1
<g/>
;	;	kIx,
WU	WU	kA
<g/>
,	,	kIx,
XiaoBing	XiaoBing	k1gInSc1
<g/>
;	;	kIx,
ZHU	ZHU	kA
<g/>
,	,	kIx,
LiXin	LiXin	k1gMnSc1
<g/>
;	;	kIx,
JIANG	JIANG	kA
<g/>
,	,	kIx,
ZhiGang	ZhiGang	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mitogenomic	Mitogenomice	k1gInPc2
analysis	analysis	k1gFnSc2
of	of	k?
the	the	k?
genus	genus	k1gInSc4
Panthera	Panther	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
China	China	k1gFnSc1
Life	Life	k1gInSc4
Sciences	Sciences	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
,	,	kIx,
sv.	sv.	kA
54	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1674	#num#	k4
<g/>
-	-	kIx~
<g/>
7305	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
11427	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
4219	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BAGATHARIA	BAGATHARIA	kA
<g/>
,	,	kIx,
Snehal	Snehal	k1gMnSc1
B.	B.	kA
<g/>
;	;	kIx,
JOSHI	JOSHI	kA
<g/>
,	,	kIx,
Madhvi	Madhev	k1gFnSc6
N.	N.	kA
<g/>
;	;	kIx,
PANDYA	PANDYA	kA
<g/>
,	,	kIx,
Rohan	Rohan	k1gMnSc1
V.	V.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Complete	Complet	k1gInSc5
mitogenome	mitogenom	k1gInSc5
of	of	k?
asiatic	asiatice	k1gFnPc2
lion	lion	k?
resolves	resolves	k1gInSc1
phylogenetic	phylogenetice	k1gFnPc2
status	status	k1gInSc1
within	within	k1gMnSc1
Panthera	Panthera	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BMC	BMC	kA
genomics	genomicsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-08-23	2013-08-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.118	10.118	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1471	#num#	k4
<g/>
-	-	kIx~
<g/>
2164	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
572	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ZHANG	ZHANG	kA
<g/>
,	,	kIx,
W.	W.	kA
Q.	Q.	kA
<g/>
;	;	kIx,
ZHANG	ZHANG	kA
<g/>
,	,	kIx,
M.	M.	kA
H.	H.	kA
Complete	Comple	k1gNnSc2
mitochondrial	mitochondrial	k1gMnSc1
genomes	genomes	k1gMnSc1
reveal	reveat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
phylogeny	phylogen	k1gInPc4
relationship	relationship	k1gMnSc1
and	and	k?
evolutionary	evolutionara	k1gFnSc2
history	histor	k1gInPc4
of	of	k?
the	the	k?
family	famila	k1gFnSc2
Felidae	Felida	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genetics	Genetics	k1gInSc1
and	and	k?
Molecular	Molecular	k1gInSc1
Research	Research	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.423	10.423	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
September	September	k1gInSc1
<g/>
.3	.3	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
LI	li	k9
<g/>
,	,	kIx,
Gang	gang	k1gInSc1
<g/>
;	;	kIx,
DAVIS	DAVIS	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
W.	W.	kA
<g/>
;	;	kIx,
EIZIRIK	EIZIRIK	kA
<g/>
,	,	kIx,
Eduardo	Eduardo	k1gNnSc1
<g/>
;	;	kIx,
MURPHY	MURPHY	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
J.	J.	kA
Phylogenomic	Phylogenomic	k1gMnSc1
evidence	evidence	k1gFnSc2
for	forum	k1gNnPc2
ancient	ancient	k1gMnSc1
hybridization	hybridization	k1gInSc1
in	in	k?
the	the	k?
genomes	genomes	k1gInSc1
of	of	k?
living	living	k1gInSc1
cats	cats	k1gInSc1
(	(	kIx(
<g/>
Felidae	Felidae	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genome	genom	k1gInSc5
Research	Research	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-01	2016-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
26	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.110	10.110	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
gr	gr	k?
<g/>
.186668	.186668	k4
<g/>
.114	.114	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
MAZÁK	mazák	k1gMnSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
a	a	k8xC
gepardi	gepard	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
zemědělské	zemědělský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
192	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Zvířata	zvíře	k1gNnPc1
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
;	;	kIx,
sv.	sv.	kA
7	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
17413642	#num#	k4
S.	S.	kA
151	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
dále	daleko	k6eAd2
jen	jen	k9
Mazák	mazák	k1gMnSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
NOWELL	NOWELL	kA
<g/>
,	,	kIx,
Kristin	Kristin	k2eAgMnSc1d1
<g/>
;	;	kIx,
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wild	Wild	k1gInSc1
Cats	Cats	k1gInSc4
<g/>
:	:	kIx,
Status	status	k1gInSc1
Survey	Survea	k1gFnSc2
and	and	k?
Conservation	Conservation	k1gInSc1
Action	Action	k1gInSc1
Plan	plan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gland	Gland	k1gInSc1
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
:	:	kIx,
IUCN	IUCN	kA
<g/>
/	/	kIx~
<g/>
SSC	SSC	kA
Cat	Cat	k1gMnSc1
Specialist	Specialist	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
406	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9782831700458	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Snow	Snow	k1gFnPc2
leopard	leopard	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
91	#num#	k4
<g/>
–	–	k?
<g/>
95	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SUNQUIST	SUNQUIST	kA
<g/>
,	,	kIx,
Mel	mlít	k5eAaImRp2nS
<g/>
;	;	kIx,
SUNQUIST	SUNQUIST	kA
<g/>
,	,	kIx,
Fiona	Fiono	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Family	Famila	k1gFnPc4
Felidae	Felida	k1gInSc2
(	(	kIx(
<g/>
Cats	Cats	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
in	in	k?
WILSON	WILSON	kA
<g/>
,	,	kIx,
Don	Don	k1gMnSc1
E.	E.	kA
<g/>
;	;	kIx,
MITTERMEIER	MITTERMEIER	kA
<g/>
,	,	kIx,
Russell	Russell	k1gMnSc1
A.	A.	kA
Handbook	handbook	k1gInSc1
of	of	k?
the	the	k?
Mammals	Mammals	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vol	vol	k6eAd1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
:	:	kIx,
Carnivores	Carnivoresa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barcelona	Barcelona	k1gFnSc1
<g/>
:	:	kIx,
Lynx	Lynx	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
84	#num#	k4
<g/>
-	-	kIx~
<g/>
96553	#num#	k4
<g/>
-	-	kIx~
<g/>
49	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
128	#num#	k4
<g/>
–	–	k?
<g/>
130	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WOZENCRAFT	WOZENCRAFT	kA
<g/>
,	,	kIx,
W.	W.	kA
C.	C.	kA
Mammal	Mammal	k1gMnSc1
Species	species	k1gFnSc2
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baltimore	Baltimore	k1gInSc1
<g/>
:	:	kIx,
Johns	Johns	k1gInSc1
Hopkins	Hopkinsa	k1gFnPc2
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8018	#num#	k4
<g/>
-	-	kIx~
<g/>
8221	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Order	Ordra	k1gFnPc2
Carnivora	Carnivor	k1gMnSc2
<g/>
,	,	kIx,
s.	s.	k?
532	#num#	k4
<g/>
–	–	k?
<g/>
628	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
JANECKA	JANECKA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
E	E	kA
<g/>
;	;	kIx,
ZHANG	ZHANG	kA
<g/>
,	,	kIx,
Yuguang	Yuguang	k1gInSc1
<g/>
;	;	kIx,
LI	li	k9
<g/>
,	,	kIx,
Diqiang	Diqiang	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Range-Wide	Range-Wid	k1gInSc5
Snow	Snow	k1gMnSc6
Leopard	leopard	k1gMnSc1
Phylogeography	Phylogeographa	k1gFnSc2
Supports	Supportsa	k1gFnPc2
Three	Three	k1gFnSc7
Subspecies	Subspeciesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Heredity	heredita	k1gFnSc2
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
108	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
597	#num#	k4
<g/>
–	–	k?
<g/>
607	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
1503	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.109	10.109	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
jhered	jhered	k1gInSc1
<g/>
/	/	kIx~
<g/>
esx	esx	k?
<g/>
0	#num#	k4
<g/>
44	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
KITCHENER	KITCHENER	kA
<g/>
,	,	kIx,
A.	A.	kA
C.	C.	kA
<g/>
;	;	kIx,
BREITENMOSER-WÜRSTEN	BREITENMOSER-WÜRSTEN	k1gMnSc1
<g/>
,	,	kIx,
Ch	Ch	kA
<g/>
.	.	kIx.
<g/>
;	;	kIx,
EIZIRIK	EIZIRIK	kA
<g/>
,	,	kIx,
E.	E.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
revised	revised	k1gInSc1
taxonomy	taxonom	k1gInPc1
of	of	k?
the	the	k?
Felidae	Felidae	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CATnews	CATnewsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
IUCN	IUCN	kA
<g/>
/	/	kIx~
<g/>
SSC	SSC	kA
Cat	Cat	k1gMnSc1
Specialist	Specialist	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
zima	zima	k1gFnSc1
2017	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgFnSc1wB
<g/>
.	.	kIx.
</s>
<s desamb="1">
Special	Special	k1gInSc1
Issue	Issu	k1gInSc2
11	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
69	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1027	#num#	k4
<g/>
-	-	kIx~
<g/>
2992	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Geptner	Geptner	k1gInSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2171	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Fact	Fact	k1gMnSc1
Sheet	Sheet	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Trust	trust	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Webový	webový	k2eAgInSc1d1
archiv	archiv	k1gInSc1
.	.	kIx.
↑	↑	k?
Sunquist	Sunquist	k1gInSc1
&	&	k?
Sunquist	Sunquist	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
390	#num#	k4
<g/>
–	–	k?
<g/>
391	#num#	k4
<g/>
↑	↑	k?
Mazák	mazák	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
145	#num#	k4
<g/>
↑	↑	k?
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2111	#num#	k4
2	#num#	k4
Mazák	mazák	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
1431	#num#	k4
2	#num#	k4
Mazák	mazák	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
1461	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Geptner	Geptner	k1gInSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2131	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Mazák	mazák	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
–	–	k?
<g/>
31	#num#	k4
<g/>
↑	↑	k?
Mazák	mazák	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
461	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
Sunquist	Sunquist	k1gMnSc1
&	&	k?
Sunquist	Sunquist	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3791	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
FOX	fox	k1gInSc1
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
L.	L.	kA
A	a	k9
Review	Review	k1gFnSc1
of	of	k?
the	the	k?
Status	status	k1gInSc1
and	and	k?
Ecology	Ecologa	k1gFnSc2
of	of	k?
the	the	k?
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gMnSc1
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Trust	trust	k1gInSc1
<g/>
,	,	kIx,
1989	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mazák	mazák	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
321	#num#	k4
2	#num#	k4
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2161	#num#	k4
2	#num#	k4
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2151	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
MCCARTHY	MCCARTHY	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
CHAPRON	CHAPRON	kA
<g/>
,	,	kIx,
Guillaume	Guillaum	k1gInSc5
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Survival	Survival	k1gMnSc1
Strategy	Stratega	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seattle	Seattle	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
:	:	kIx,
International	International	k1gMnSc1
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Trust	trust	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
К	К	k?
<g/>
,	,	kIx,
Е	Е	k?
<g/>
;	;	kIx,
З	З	k?
<g/>
,	,	kIx,
А	А	k?
<g/>
;	;	kIx,
С	С	k?
<g/>
,	,	kIx,
М	М	k?
К	К	k?
К	К	k?
Р	Р	k?
<g/>
:	:	kIx,
С	С	k?
б	б	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
biodat	biodat	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
ru	ru	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CHADWICK	CHADWICK	kA
<g/>
,	,	kIx,
Douglas	Douglas	k1gMnSc1
H.	H.	kA
Out	Out	k1gMnSc1
of	of	k?
the	the	k?
Shadows	Shadows	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
Magazine	Magazin	k1gInSc5
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Webový	webový	k2eAgInSc1d1
archiv	archiv	k1gInSc1
.	.	kIx.
1	#num#	k4
2	#num#	k4
С	С	k?
с	с	k?
с	с	k?
б	б	k?
(	(	kIx(
<g/>
и	и	k?
<g/>
)	)	kIx)
в	в	k?
Р	Р	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
Р	Р	k?
А	А	k?
н	н	k?
<g/>
,	,	kIx,
WWF	WWF	kA
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
32	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgNnSc4d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
[	[	kIx(
<g/>
Ruská	ruský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
4	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
225	#num#	k4
<g/>
↑	↑	k?
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2291	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
Stroganov	Stroganovo	k1gNnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
s.	s.	k?
4271	#num#	k4
2	#num#	k4
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
227	#num#	k4
<g/>
↑	↑	k?
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
228	#num#	k4
<g/>
↑	↑	k?
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
225	#num#	k4
<g/>
–	–	k?
<g/>
2331	#num#	k4
2	#num#	k4
3	#num#	k4
Sunquist	Sunquist	k1gMnSc1
&	&	k?
Sunquist	Sunquist	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3801	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
234	#num#	k4
<g/>
↑	↑	k?
FOX	fox	k1gInSc1
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
L.	L.	kA
<g/>
;	;	kIx,
CHUNDAWAT	CHUNDAWAT	kA
<g/>
,	,	kIx,
Raghunandan	Raghunandan	k1gMnSc1
S.	S.	kA
What	What	k1gMnSc1
is	is	k?
a	a	k8xC
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Behavior	Behavior	k1gMnSc1
and	and	k?
Ecology	Ecologa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
MCCARTHY	MCCARTHY	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
;	;	kIx,
MALLON	MALLON	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snow	Snow	k1gFnSc1
Leopards	Leopardsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
San	San	k1gMnSc1
Diego	Diego	k1gMnSc1
<g/>
:	:	kIx,
Academic	Academic	k1gMnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
128022139	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
13	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2411	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
Rodney	Rodney	k1gInPc7
M.	M.	kA
<g/>
;	;	kIx,
MISHRA	MISHRA	kA
<g/>
,	,	kIx,
Charudutt	Charudutt	k1gMnSc1
<g/>
;	;	kIx,
MCCARTHY	MCCARTHY	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
M.	M.	kA
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snow	Snow	k1gMnSc1
leopard	leopard	k1gMnSc1
<g/>
:	:	kIx,
conflict	conflict	k1gMnSc1
and	and	k?
conservation	conservation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
MACDONALD	Macdonald	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
W.	W.	kA
<g/>
;	;	kIx,
LOVERIDGE	LOVERIDGE	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
J.	J.	kA
Biology	biolog	k1gMnPc4
and	and	k?
conservation	conservation	k1gInSc1
of	of	k?
wild	wild	k6eAd1
felids	felids	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxford	Oxford	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780199234448	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
417	#num#	k4
<g/>
–	–	k?
<g/>
430	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Mazák	mazák	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
1481	#num#	k4
2	#num#	k4
3	#num#	k4
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2331	#num#	k4
2	#num#	k4
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
Rodney	Rodnea	k1gFnSc2
<g/>
;	;	kIx,
HUNTER	HUNTER	kA
<g/>
,	,	kIx,
Don	Don	k1gMnSc1
O.	O.	kA
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Survey	Survea	k1gFnSc2
and	and	k?
Conservation	Conservation	k1gInSc1
Handbook	handbook	k1gInSc1
Part	part	k1gInSc4
III	III	kA
[	[	kIx(
<g/>
pdf	pdf	k?
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seattle	Seattle	k1gFnSc1
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
&	&	k?
Fort	Fort	k?
Collins	Collins	k1gInSc1
Science	Science	k1gFnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
Colorado	Colorado	k1gNnSc1
<g/>
,	,	kIx,
US	US	kA
<g/>
:	:	kIx,
International	International	k1gMnSc1
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Trust	trust	k1gInSc1
&	&	k?
U.	U.	kA
<g/>
S.	S.	kA
Geological	Geological	k1gMnSc2
Survey	Survea	k1gMnSc2
<g/>
,	,	kIx,
1996	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
66	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Webový	webový	k2eAgInSc1d1
archiv	archiv	k1gInSc1
.	.	kIx.
↑	↑	k?
Geptner	Geptner	k1gInSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2321	#num#	k4
2	#num#	k4
3	#num#	k4
Ruská	ruský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
141	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Sunquist	Sunquist	k1gMnSc1
&	&	k?
Sunquist	Sunquist	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
381	#num#	k4
<g/>
↑	↑	k?
Sunquist	Sunquist	k1gMnSc1
&	&	k?
Sunquist	Sunquist	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3831	#num#	k4
2	#num#	k4
3	#num#	k4
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
235	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
WEISSENGRUBER	WEISSENGRUBER	kA
<g/>
,	,	kIx,
G.	G.	kA
E.	E.	kA
<g/>
;	;	kIx,
FORSTENPOINTNER	FORSTENPOINTNER	kA
<g/>
,	,	kIx,
G.	G.	kA
<g/>
;	;	kIx,
PETERS	PETERS	kA
<g/>
,	,	kIx,
G.	G.	kA
<g/>
;	;	kIx,
KÜBBER-HEISS	KÜBBER-HEISS	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
<g/>
;	;	kIx,
FITCH	FITCH	kA
<g/>
,	,	kIx,
W.	W.	kA
T.	T.	kA
Hyoid	Hyoida	k1gFnPc2
apparatus	apparatus	k1gMnSc1
and	and	k?
pharynx	pharynx	k1gInSc1
in	in	k?
the	the	k?
lion	lion	k?
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
leo	leo	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jaguar	jaguar	k1gMnSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
onca	onca	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tiger	tiger	k1gMnSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
tigris	tigris	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
cheetah	cheetah	k1gInSc1
(	(	kIx(
<g/>
Acinonyx	Acinonyx	k1gInSc1
jubatus	jubatus	k1gInSc1
<g/>
)	)	kIx)
and	and	k?
domestic	domestice	k1gFnPc2
cat	cat	k?
(	(	kIx(
<g/>
Felis	Felis	k1gInSc1
silvestris	silvestris	k1gFnSc2
f.	f.	k?
catus	catus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
195	#num#	k4
<g/>
–	–	k?
<g/>
209	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Anatomy	anatom	k1gMnPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2002-09-10	2002-09-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
102	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
195	#num#	k4
<g/>
–	–	k?
<g/>
209	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.104	10.104	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1469	.1469	k4
<g/>
-	-	kIx~
<g/>
7580.2002	7580.2002	k4
<g/>
.00088	.00088	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KITCHENER	KITCHENER	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
C.	C.	kA
<g/>
;	;	kIx,
DRISCOLL	DRISCOLL	kA
<g/>
,	,	kIx,
Carlos	Carlos	k1gMnSc1
A.	A.	kA
<g/>
;	;	kIx,
YAMAGUCHI	YAMAGUCHI	kA
<g/>
,	,	kIx,
Nobuyuki	Nobuyuki	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
What	What	k1gInSc1
is	is	k?
a	a	k8xC
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Taxonomy	Taxonom	k1gInPc7
<g/>
,	,	kIx,
Morphology	Morpholog	k1gMnPc7
<g/>
,	,	kIx,
and	and	k?
Phylogeny	Phylogen	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
MCCARTHY	MCCARTHY	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
;	;	kIx,
MALLON	MALLON	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snow	Snow	k1gFnSc1
Leopards	Leopardsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
San	San	k1gMnSc1
Diego	Diego	k1gMnSc1
<g/>
:	:	kIx,
Academic	Academic	k1gMnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
128022139	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Sunquist	Sunquist	k1gMnSc1
&	&	k?
Sunquist	Sunquist	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3861	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Sunquist	Sunquist	k1gMnSc1
&	&	k?
Sunquist	Sunquist	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
385	#num#	k4
<g/>
↑	↑	k?
KHOLOVÁ	Kholová	k1gFnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
<g/>
;	;	kIx,
KNOTKOVÁ	Knotková	k1gFnSc1
<g/>
,	,	kIx,
Libuše	Libuše	k1gFnSc1
<g/>
;	;	kIx,
KNOTKA	Knotek	k1gMnSc4
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mláďata	mládě	k1gNnPc1
z	z	k7c2
království	království	k1gNnSc2
divočiny	divočina	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
208	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7306	#num#	k4
<g/>
-	-	kIx~
<g/>
215	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
94	#num#	k4
<g/>
-	-	kIx~
<g/>
95	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Geptner	Geptner	k1gInSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
2381	#num#	k4
2	#num#	k4
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
239	#num#	k4
<g/>
↑	↑	k?
NOWELL	NOWELL	kA
<g/>
,	,	kIx,
Kristin	Kristin	k2eAgMnSc1d1
<g/>
;	;	kIx,
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wild	Wild	k1gInSc1
Cats	Cats	k1gInSc4
<g/>
:	:	kIx,
Status	status	k1gInSc1
Survey	Survea	k1gFnSc2
and	and	k?
Conservation	Conservation	k1gInSc1
Action	Action	k1gInSc1
Plan	plan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gland	Gland	k1gInSc1
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
:	:	kIx,
IUCN	IUCN	kA
<g/>
/	/	kIx~
<g/>
SSC	SSC	kA
Cat	Cat	k1gMnSc1
Specialist	Specialist	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
406	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9782831700458	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Snow	Snow	k1gFnPc2
leopard	leopard	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
91	#num#	k4
<g/>
-	-	kIx~
<g/>
95	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ruská	ruský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
<g/>
↑	↑	k?
Ruská	ruský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
11	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
↑	↑	k?
Ruská	ruský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
↑	↑	k?
Ruská	ruský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
28	#num#	k4
<g/>
↑	↑	k?
PETERSON	PETERSON	kA
<g/>
,	,	kIx,
Alison	Alison	k1gMnSc1
J.	J.	kA
Helen	Helena	k1gFnPc2
E.	E.	kA
Freeman	Freeman	k1gMnSc1
<g/>
,	,	kIx,
Protector	Protector	k1gMnSc1
of	of	k?
Snow	Snow	k1gFnSc1
Leopards	Leopards	k1gInSc1
<g/>
,	,	kIx,
Is	Is	k1gFnSc1
Dead	Dead	k1gInSc1
at	at	k?
75	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-10-01	2007-10-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gFnSc1
Bishkek	Bishkek	k6eAd1
Declaration	Declaration	k1gInSc1
on	on	k3xPp3gInSc1
the	the	k?
Conservation	Conservation	k1gInSc1
of	of	k?
the	the	k?
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Global	globat	k5eAaImAgMnS
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
&	&	k?
Ecosystem	Ecosyst	k1gInSc7
Protection	Protection	k1gInSc1
Program	program	k1gInSc1
<g/>
,	,	kIx,
2013-10-23	2013-10-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Ruská	ruský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
24	#num#	k4
<g/>
↑	↑	k?
SMOLÍK	Smolík	k1gMnSc1
<g/>
,	,	kIx,
Vojtěch	Vojtěch	k1gMnSc1
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
králi	král	k1gMnPc7
Himálaje	Himálaj	k1gFnSc2
do	do	k7c2
českých	český	k2eAgFnPc2d1
zoo	zoo	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZOO	zoo	k1gNnSc1
Magazín	magazín	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-04-14	2016-04-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Máme	mít	k5eAaImIp1nP
mláďata	mládě	k1gNnPc1
irbise	irbise	k1gFnSc2
-	-	kIx~
po	po	k7c6
10	#num#	k4
letech	léto	k1gNnPc6
<g/>
!	!	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZOO	zoo	k1gNnSc1
Liberec	Liberec	k1gInSc1
<g/>
,	,	kIx,
2011-05-09	2011-05-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SÝKORA	Sýkora	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VIDEO	video	k1gNnSc1
<g/>
:	:	kIx,
V	v	k7c6
zoo	zoo	k1gNnSc6
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
samička	samička	k1gFnSc1
irbise	irbise	k1gFnSc2
horského	horský	k2eAgNnSc2d1
<g/>
,	,	kIx,
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
se	se	k3xPyFc4
Chandra	chandra	k1gFnSc1
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-07-02	2015-07-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jihlavské	jihlavský	k2eAgFnSc6d1
zoo	zoo	k1gFnSc6
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
irbis	irbis	k1gInSc4
<g/>
,	,	kIx,
mládě	mládě	k1gNnSc4
mají	mít	k5eAaImIp3nP
i	i	k8xC
tuleni	tuleň	k1gMnSc3
<g/>
.	.	kIx.
ceskenoviny	ceskenovina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-07-14	2015-07-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Irbis	Irbis	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ZOO	zoo	k1gNnSc7
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
2014-03-22	2014-03-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geptner	Geptner	k1gInSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
242	#num#	k4
<g/>
↑	↑	k?
Sunquist	Sunquist	k1gMnSc1
&	&	k?
Sunquist	Sunquist	k1gInSc1
2002	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3821	#num#	k4
2	#num#	k4
Geptner	Geptner	k1gMnSc1
&	&	k?
Naumov	Naumov	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
236	#num#	k4
<g/>
↑	↑	k?
М	М	k?
<g/>
,	,	kIx,
Д	Д	k?
Г	Г	k?
<g/>
.	.	kIx.
С	С	k?
б	б	k?
и	и	k?
и	и	k?
в	в	k?
Б	Б	k?
С	С	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
П	П	k?
Б	Б	k?
<g/>
,	,	kIx,
2006-04-26	2006-04-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Planet	planeta	k1gFnPc2
Earth	Earth	k1gMnSc1
|	|	kIx~
Mountains	Mountains	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
One	One	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Natural	Natural	k?
World	World	k1gMnSc1
|	|	kIx~
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
-	-	kIx~
Beyond	Beyond	k1gMnSc1
the	the	k?
Myth	Myth	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
Two	Two	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kung	Kung	k1gInSc1
Fu	fu	k0
Panda	panda	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
metacritic	metacritice	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Catalog	Catalog	k1gInSc1
|	|	kIx~
Commemorative	Commemorativ	k1gInSc5
coins	coins	k1gInSc1
|	|	kIx~
Silver	Silver	k1gInSc1
Coins	Coins	k1gInSc1
|	|	kIx~
Red	Red	k1gMnSc1
Book	Book	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Bank	banka	k1gFnPc2
of	of	k?
Kazakhstan	Kazakhstan	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Catalog	Catalog	k1gInSc1
|	|	kIx~
Commemorative	Commemorativ	k1gInSc5
coins	coins	k1gInSc1
|	|	kIx~
Gold	Gold	k1gInSc1
Coins	Coins	k1gInSc1
|	|	kIx~
Animals	Animals	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Bank	banka	k1gFnPc2
of	of	k?
Kazakhstan	Kazakhstan	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
10000	#num#	k4
tenge	tenge	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Bank	banka	k1gFnPc2
of	of	k?
Kazakhstan	Kazakhstan	k1gInSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2014-12-19	2014-12-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
И	И	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
Ц	Ц	k?
б	б	k?
Р	Р	k?
Ф	Ф	k?
<g/>
,	,	kIx,
2000-12-06	2000-12-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Collection	Collection	k1gInSc1
coin	coin	k1gInSc1
of	of	k?
the	the	k?
series	series	k1gMnSc1
"	"	kIx"
<g/>
Red	Red	k1gMnSc1
Book	Book	k1gMnSc1
of	of	k?
Kyrgyzstan	Kyrgyzstan	k1gInSc1
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Bank	banka	k1gFnPc2
of	of	k?
the	the	k?
Kyrgyz	Kyrgyz	k1gInSc1
Republic	Republice	k1gFnPc2
<g/>
,	,	kIx,
2015-12-18	2015-12-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
С	С	k?
<g/>
,	,	kIx,
н	н	k?
<g/>
,	,	kIx,
п	п	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
О	О	k?
Т	Т	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
У	У	k?
<g/>
,	,	kIx,
Г	Г	k?
И	И	k?
Л	Л	k?
о	о	k?
б	б	k?
и	и	k?
н	н	k?
о	о	k?
г	г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
Г	Г	k?
<g/>
.	.	kIx.
<g/>
р	р	k?
<g/>
,	,	kIx,
2005-11-09	2005-11-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
А	А	k?
<g/>
:	:	kIx,
И	И	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
А	А	k?
<g/>
:	:	kIx,
О	О	k?
и	и	k?
г	г	k?
А	А	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Г	Г	k?
с	с	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
Р	Р	k?
Х	Х	k?
-	-	kIx~
О	О	k?
п	п	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Г	Г	k?
и	и	k?
г	г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
Б	Б	k?
<g/>
:	:	kIx,
О	О	k?
с	с	k?
м	м	k?
г	г	k?
Б	Б	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
С	С	k?
<g/>
,	,	kIx,
Г	Г	k?
А	А	k?
И	И	k?
п	п	k?
ж	ж	k?
«	«	k?
<g/>
П	П	k?
в	в	k?
г	г	k?
С	С	k?
<g/>
»	»	k?
(	(	kIx(
<g/>
«	«	k?
<g/>
С	С	k?
б	б	k?
<g/>
»	»	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
К	К	k?
а	а	k?
"	"	kIx"
<g/>
С	С	k?
<g/>
"	"	kIx"
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
П	П	k?
о	о	k?
м	м	k?
"	"	kIx"
<g/>
Э	Э	k?
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
М	М	k?
Ю	Ю	k?
К	К	k?
Р	Р	k?
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2013-06-07	2013-06-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Astana	Astana	k1gFnSc1
-	-	kIx~
Almaty	Almata	k1gFnSc2
2011	#num#	k4
|	|	kIx~
7	#num#	k4
<g/>
th	th	k?
Asian	Asian	k1gMnSc1
Winter	Winter	k1gMnSc1
Games	Games	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olympic	Olympic	k1gMnSc1
Council	Council	k1gMnSc1
of	of	k?
Asia	Asia	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
dat	datum	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maskoti	Maskot	k1gMnPc1
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
:	:	kIx,
od	od	k7c2
Schusse	Schusse	k1gFnSc2
po	po	k7c4
sněžného	sněžný	k2eAgMnSc4d1
leoparda	leopard	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
Sport	sport	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-02-07	2014-02-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WILDER	WILDER	kA
<g/>
,	,	kIx,
Todd	Todd	k1gMnSc1
<g/>
;	;	kIx,
SARKAR	SARKAR	kA
<g/>
,	,	kIx,
Monica	Monica	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Apple	Apple	kA
to	ten	k3xDgNnSc4
Ship	Ship	k1gMnSc1
Mac	Mac	kA
OS	OS	kA
X	X	kA
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
on	on	k3xPp3gMnSc1
August	August	k1gMnSc1
28	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cupertino	Cupertin	k2eAgNnSc1d1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
:	:	kIx,
Apple	Apple	kA
Inc	Inc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2009-08-24	2009-08-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MAZÁK	mazák	k1gMnSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
a	a	k8xC
gepardi	gepard	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
zemědělské	zemědělský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
192	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Zvířata	zvíře	k1gNnPc1
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
;	;	kIx,
sv.	sv.	kA
7	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
17413642	#num#	k4
Kapitola	kapitola	k1gFnSc1
Irbis	Irbis	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
143	#num#	k4
<g/>
–	–	k?
<g/>
152	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Г	Г	k?
<g/>
,	,	kIx,
В	В	k?
Г	Г	k?
<g/>
;	;	kIx,
Н	Н	k?
<g/>
,	,	kIx,
Н	Н	k?
П	П	k?
М	М	k?
С	С	k?
С	С	k?
<g/>
.	.	kIx.
Х	Х	k?
(	(	kIx(
<g/>
г	г	k?
и	и	k?
к	к	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moskva	Moskva	k1gFnSc1
<g/>
:	:	kIx,
В	В	k?
ш	ш	k?
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
552	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Р	Р	k?
с	с	k?
б	б	k?
<g/>
,	,	kIx,
и	и	k?
и	и	k?
<g/>
,	,	kIx,
s.	s.	k?
211	#num#	k4
<g/>
–	–	k?
<g/>
244	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
JACKSON	JACKSON	kA
<g/>
,	,	kIx,
Rodney	Rodnea	k1gFnSc2
<g/>
;	;	kIx,
MALLON	MALLON	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
;	;	kIx,
MISHRA	MISHRA	kA
<g/>
,	,	kIx,
Chadurutt	Chadurutt	k1gMnSc1
<g/>
;	;	kIx,
NORAS	NORAS	kA
<g/>
,	,	kIx,
Sibylle	Sibylle	k1gFnSc1
<g/>
;	;	kIx,
SHARMA	SHARMA	kA
<g/>
,	,	kIx,
Rishi	Rishi	k1gNnSc1
<g/>
;	;	kIx,
SURYAWANSHI	SURYAWANSHI	kA
<g/>
,	,	kIx,
Kulbhushansingh	Kulbhushansingh	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Survival	Survival	k1gMnSc1
Strategy	Stratega	k1gFnSc2
(	(	kIx(
<g/>
Revised	Revised	k1gInSc1
Version	Version	k1gInSc1
2014.1	2014.1	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seattle	Seattle	k1gFnSc1
<g/>
:	:	kIx,
Snow	Snow	k1gFnSc1
Leopard	leopard	k1gMnSc1
Network	network	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
145	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MATTHIESSEN	MATTHIESSEN	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Viking	Viking	k1gMnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
338	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
670	#num#	k4
<g/>
-	-	kIx~
<g/>
65374	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MCCARTHY	MCCARTHY	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
;	;	kIx,
MALLON	MALLON	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Snow	Snow	k1gFnSc1
Leopards	Leopardsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
San	San	k1gMnSc1
Diego	Diego	k1gMnSc1
<g/>
:	:	kIx,
Academic	Academic	k1gMnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
644	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Biodiversity	Biodiversit	k1gInPc4
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
128022139	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
NOWELL	NOWELL	kA
<g/>
,	,	kIx,
Kristin	Kristin	k2eAgMnSc1d1
<g/>
;	;	kIx,
LI	li	k9
<g/>
,	,	kIx,
Juan	Juan	k1gMnSc1
<g/>
;	;	kIx,
PALTSYN	PALTSYN	kA
<g/>
,	,	kIx,
Mikhail	Mikhail	k1gMnSc1
<g/>
;	;	kIx,
SHARMA	SHARMA	kA
<g/>
,	,	kIx,
Rishi	Rishi	k1gNnSc1
Kumar	Kumara	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gMnSc1
Ounce	Ounce	k1gMnSc1
of	of	k?
Prevention	Prevention	k1gInSc1
<g/>
:	:	kIx,
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
Crime	Crim	k1gInSc5
Revisited	Revisited	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cambridge	Cambridge	k1gFnSc1
<g/>
:	:	kIx,
TRAFFIC	TRAFFIC	kA
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
xiv	xiv	k?
+	+	kIx~
55	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
85850	#num#	k4
<g/>
-	-	kIx~
<g/>
409	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SUNQUIST	SUNQUIST	kA
<g/>
,	,	kIx,
Mel	mlít	k5eAaImRp2nS
<g/>
;	;	kIx,
SUNQUIST	SUNQUIST	kA
<g/>
,	,	kIx,
Fiona	Fiono	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wild	Wild	k1gInSc1
Cats	Cats	k1gInSc4
of	of	k?
the	the	k?
World	World	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
<g/>
:	:	kIx,
University	universita	k1gFnSc2
Of	Of	k1gFnSc2
Chicago	Chicago	k1gNnSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
462	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
226779997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Snow	Snow	k1gFnPc2
leopard	leopard	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
377	#num#	k4
<g/>
–	–	k?
<g/>
394	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
SCHALLER	SCHALLER	kA
<g/>
,	,	kIx,
George	Georg	k1gMnSc2
B.	B.	kA
Mountain	Mountain	k2eAgInSc1d1
monarchs	monarchs	k1gInSc1
:	:	kIx,
wild	wild	k1gInSc1
sheep	sheep	k1gInSc1
and	and	k?
goats	goats	k1gInSc1
of	of	k?
the	the	k?
Himalaya	Himalayum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
<g/>
:	:	kIx,
University	universita	k1gFnPc1
of	of	k?
Chicago	Chicago	k1gNnSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
.	.	kIx.
443	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
9780226736419	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
a	a	k8xC
kategorie	kategorie	k1gFnPc1
</s>
<s>
Velké	velký	k2eAgFnPc1d1
kočky	kočka	k1gFnPc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
<g/>
:	:	kIx,
<g/>
Nositelé	nositel	k1gMnPc1
Řádu	řád	k1gInSc2
irbisa	irbisa	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
irbis	irbis	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
irbis	irbis	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Panthera	Panthero	k1gNnSc2
uncia	uncia	k1gFnSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
irbis	irbis	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
The	The	k?
IUCN	IUCN	kA
Red	Red	k1gFnSc2
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
<g/>
:	:	kIx,
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
–	–	k?
stránky	stránka	k1gFnPc4
Mezinárodního	mezinárodní	k2eAgInSc2d1
svazu	svaz	k1gInSc2
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
WWF	WWF	kA
Global	globat	k5eAaImAgInS
<g/>
:	:	kIx,
Snow	Snow	k1gFnSc1
Leopard	leopard	k1gMnSc1
–	–	k?
stránky	stránka	k1gFnPc4
Světového	světový	k2eAgInSc2d1
fondu	fond	k1gInSc2
na	na	k7c4
ochranu	ochrana	k1gFnSc4
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Global	globat	k5eAaImAgMnS
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
&	&	k?
Ecosystem	Ecosyst	k1gInSc7
Protection	Protection	k1gInSc1
Program	program	k1gInSc1
–	–	k?
globální	globální	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
program	program	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Wildscreen	Wildscreen	k1gInSc1
Arkive	Arkiev	k1gFnSc2
<g/>
:	:	kIx,
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
(	(	kIx(
<g/>
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
informace	informace	k1gFnPc1
na	na	k7c4
Wildscreen	Wildscreen	k1gInSc4
Arkive	Arkiev	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
National	Nationat	k5eAaPmAgMnS,k5eAaImAgMnS
Geographic	Geographic	k1gMnSc1
<g/>
:	:	kIx,
Out	Out	k1gMnSc1
of	of	k?
the	the	k?
Shadows	Shadows	k1gInSc1
–	–	k?
fotografie	fotografie	k1gFnSc1
irbisa	irbisa	k1gFnSc1
z	z	k7c2
magazínu	magazín	k1gInSc2
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
</s>
<s>
BBC	BBC	kA
Nature	Natur	k1gMnSc5
Wildlife	Wildlif	k1gMnSc5
<g/>
:	:	kIx,
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
–	–	k?
videa	video	k1gNnSc2
BBC	BBC	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
National	Nationat	k5eAaPmAgMnS,k5eAaImAgMnS
Geographic	Geographic	k1gMnSc1
Documentary	Documentara	k1gFnSc2
2016	#num#	k4
<g/>
:	:	kIx,
Silent	Silent	k1gMnSc1
Roar	Roar	k1gMnSc1
The	The	k1gMnSc1
Snow	Snow	k1gMnSc1
Leopard	leopard	k1gMnSc1
–	–	k?
film	film	k1gInSc1
od	od	k7c2
National	National	k1gFnSc2
Geographic	Geographic	k1gMnSc1
Channel	Channel	k1gMnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kočkovití	kočkovití	k1gMnPc1
Malé	Malé	k2eAgFnSc2d1
kočky	kočka	k1gFnSc2
(	(	kIx(
<g/>
Felinae	Felinae	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
kočka	kočka	k1gFnSc1
divoká	divoký	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
šedá	šedý	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
bažinná	bažinný	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
pouštní	pouštní	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
černonohá	černonohý	k2eAgFnSc1d1
•	•	k?
manul	manul	k1gMnSc1
•	•	k?
kočka	kočka	k1gFnSc1
bornejská	bornejský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
Temminckova	Temminckův	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
mramorovaná	mramorovaný	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
bengálská	bengálský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
plochočelá	plochočelý	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
cejlonská	cejlonský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
rybářská	rybářský	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
pampová	pampový	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
slaništní	slaništní	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
tmavá	tmavý	k2eAgFnSc1d1
•	•	k?
kočka	kočka	k1gFnSc1
horská	horský	k2eAgFnSc1d1
•	•	k?
ocelot	ocelot	k1gMnSc1
velký	velký	k2eAgMnSc1d1
•	•	k?
ocelot	ocelot	k1gMnSc1
stromový	stromový	k2eAgMnSc1d1
•	•	k?
margay	margay	k1gInPc4
•	•	k?
rys	rys	k1gInSc1
kanadský	kanadský	k2eAgInSc1d1
•	•	k?
rys	rys	k1gInSc1
ostrovid	ostrovid	k1gMnSc1
•	•	k?
rys	rys	k1gMnSc1
iberský	iberský	k2eAgMnSc1d1
•	•	k?
rys	rys	k1gMnSc1
červený	červený	k2eAgMnSc1d1
•	•	k?
karakal	karakal	k1gMnSc1
•	•	k?
serval	serval	k1gMnSc1
•	•	k?
kočka	kočka	k1gFnSc1
zlatá	zlatý	k2eAgFnSc1d1
•	•	k?
jaguarundi	jaguarundit	k5eAaPmRp2nS
•	•	k?
puma	puma	k1gFnSc1
•	•	k?
gepard	gepard	k1gMnSc1
Velké	velká	k1gFnSc2
kočky	kočka	k1gFnSc2
(	(	kIx(
<g/>
Pantherinae	Pantherinae	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
levhart	levhart	k1gMnSc1
obláčkový	obláčkový	k2eAgMnSc1d1
•	•	k?
levhart	levhart	k1gMnSc1
Diardův	Diardův	k2eAgMnSc1d1
•	•	k?
irbis	irbis	k1gInSc1
•	•	k?
lev	lev	k1gMnSc1
•	•	k?
tygr	tygr	k1gMnSc1
•	•	k?
levhart	levhart	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
•	•	k?
jaguár	jaguár	k1gMnSc1
†	†	k?
<g/>
Machairodontinae	Machairodontinae	k1gInSc1
</s>
<s>
Adelphailurus	Adelphailurus	k1gMnSc1
•	•	k?
Amphimachairodus	Amphimachairodus	k1gMnSc1
•	•	k?
Dinofelis	Dinofelis	k1gFnSc2
•	•	k?
Hemimachairodus	Hemimachairodus	k1gMnSc1
•	•	k?
Homotherium	Homotherium	k1gNnSc4
•	•	k?
Lokotunjailurus	Lokotunjailurus	k1gMnSc1
•	•	k?
Machairodus	Machairodus	k1gMnSc1
•	•	k?
Megantereon	Megantereon	k1gMnSc1
•	•	k?
Metailurus	Metailurus	k1gMnSc1
•	•	k?
Miomachairodus	Miomachairodus	k1gMnSc1
•	•	k?
Nimravides	Nimravides	k1gMnSc1
•	•	k?
Paramachairodus	Paramachairodus	k1gMnSc1
•	•	k?
Pontosmilus	Pontosmilus	k1gMnSc1
•	•	k?
Rhizosmilodon	Rhizosmilodon	k1gMnSc1
•	•	k?
Smilodon	Smilodon	k1gMnSc1
•	•	k?
Stenailurus	Stenailurus	k1gMnSc1
•	•	k?
Xenosmilus	Xenosmilus	k1gMnSc1
hodsonae	hodsona	k1gFnSc2
†	†	k?
<g/>
Proailurinae	Proailurina	k1gFnSc2
</s>
<s>
Proailurus	Proailurus	k1gInSc1
•	•	k?
Stenogale	Stenogala	k1gFnSc3
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
box-sizing	box-sizing	k1gInSc1
<g/>
:	:	kIx,
<g/>
border-box	border-box	k1gInSc1
<g/>
;	;	kIx,
<g/>
border	border	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
#	#	kIx~
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
b	b	k?
<g/>
1	#num#	k4
<g/>
;	;	kIx,
<g/>
width	widtha	k1gFnPc2
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
<g/>
clear	clear	k1gInSc1
<g/>
:	:	kIx,
<g/>
both	both	k1gInSc1
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
88	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
margin	margin	k1gMnSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
em	em	k?
auto	auto	k1gNnSc1
0	#num#	k4
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
margin-top	margin-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
margin-top	margin-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-inner	-innra	k1gFnPc2
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
width	width	k1gMnSc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-group	-group	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-titlat	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.25	0.25	k4
<g/>
em	em	k?
1	#num#	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
line-height	line-height	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1.5	1.5	k4
<g/>
em	em	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
;	;	kIx,
<g/>
text-align	text-aligno	k1gNnPc2
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
th	th	k?
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc1
<g/>
{	{	kIx(
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gMnSc1
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
right	right	k1gMnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-list	-list	k1gMnSc1
<g/>
{	{	kIx(
<g/>
line-height	line-height	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1.5	1.5	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
border-color	border-color	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-image	-imagat	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-list	-list	k1gInSc1
<g/>
{	{	kIx(
<g/>
border-top	border-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g />
.	.	kIx.
</s>
<s hack="1">
solid	solid	k1gInSc1
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
th	th	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
th	th	k?
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc4
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
88	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc4
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
f	f	k?
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
f	f	k?
<g/>
0	#num#	k4
<g/>
f	f	k?
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-even	-evna	k1gFnPc2
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
-odd	-odda	k1gFnPc2
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
transparent	transparent	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
dl	dl	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
ol	ol	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
dl	dl	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
ol	ol	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
ul	ul	kA
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.125	0.125	k4
<g/>
em	em	k?
0	#num#	k4
<g/>
}	}	kIx)
<g/>
Identifikátory	identifikátor	k1gInPc4
taxonuPanthera	taxonuPanthera	k1gFnSc1
uncia	uncia	k1gFnSc1
</s>
<s>
Wikidata	Wikidata	k1gFnSc1
<g/>
:	:	kIx,
Q30197	Q30197	k1gFnSc1
</s>
<s>
Wikidruhy	Wikidruh	k1gInPc1
<g/>
:	:	kIx,
Panthera	Panthera	k1gFnSc1
uncia	uncia	k1gFnSc1
</s>
<s>
ADW	ADW	kA
<g/>
:	:	kIx,
Uncia	uncia	k1gFnSc1
</s>
<s>
BOLD	BOLD	kA
<g/>
:	:	kIx,
741526	#num#	k4
</s>
<s>
EoL	EoL	k?
<g/>
:	:	kIx,
328676	#num#	k4
</s>
<s>
EPPO	EPPO	kA
<g/>
:	:	kIx,
UNCAUN	UNCAUN	kA
</s>
<s>
Fossilworks	Fossilworks	k1gInSc1
<g/>
:	:	kIx,
224097	#num#	k4
</s>
<s>
GBIF	GBIF	kA
<g/>
:	:	kIx,
5787213	#num#	k4
</s>
<s>
iNaturalist	iNaturalist	k1gInSc1
<g/>
:	:	kIx,
74831	#num#	k4
</s>
<s>
IRMNG	IRMNG	kA
<g/>
:	:	kIx,
11902465	#num#	k4
</s>
<s>
ISC	ISC	kA
<g/>
:	:	kIx,
70721	#num#	k4
</s>
<s>
ITIS	ITIS	kA
<g/>
:	:	kIx,
933420	#num#	k4
</s>
<s>
IUCN	IUCN	kA
<g/>
:	:	kIx,
22732	#num#	k4
</s>
<s>
NCBI	NCBI	kA
<g/>
:	:	kIx,
29064	#num#	k4
</s>
<s>
Species	species	k1gFnSc1
<g/>
+	+	kIx~
<g/>
:	:	kIx,
32743	#num#	k4
</s>
<s>
Felis	Felis	k1gFnSc1
uncia	uncia	k1gFnSc1
</s>
<s>
Wikidata	Wikidata	k1gFnSc1
<g/>
:	:	kIx,
Q24284231	Q24284231	k1gFnSc1
</s>
<s>
GBIF	GBIF	kA
<g/>
:	:	kIx,
9406202	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4179849-1	4179849-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
86005487	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
86005487	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kočky	kočka	k1gFnPc1
|	|	kIx~
Ohrožená	ohrožený	k2eAgNnPc1d1
a	a	k8xC
vyhynulá	vyhynulý	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
</s>
