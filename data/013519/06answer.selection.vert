<s>
Údaje	údaj	k1gInPc1
o	o	k7c6
délce	délka	k1gFnSc6
života	život	k1gInSc2
irbisů	irbis	k1gInPc2
v	v	k7c6
divočině	divočina	k1gFnSc6
se	se	k3xPyFc4
kvůli	kvůli	k7c3
obtížnosti	obtížnost	k1gFnSc3
získávání	získávání	k1gNnSc2
dat	datum	k1gNnPc2
výrazně	výrazně	k6eAd1
liší	lišit	k5eAaImIp3nP
<g/>
,	,	kIx,
zpráva	zpráva	k1gFnSc1
ruské	ruský	k2eAgFnSc2d1
pobočky	pobočka	k1gFnSc2
WWF	WWF	kA
z	z	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
nejdelší	dlouhý	k2eAgInSc1d3
doložený	doložený	k2eAgInSc1d1
věk	věk	k1gInSc1
irbisa	irbis	k1gMnSc2
v	v	k7c6
divočině	divočina	k1gFnSc6
13	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
zajetí	zajetí	k1gNnSc6
žijí	žít	k5eAaImIp3nP
irbisové	irbisový	k2eAgFnPc1d1
výrazně	výrazně	k6eAd1
déle	dlouho	k6eAd2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
doloženy	doložen	k2eAgInPc1d1
případy	případ	k1gInPc1
irbisů	irbis	k1gInPc2
starších	starý	k2eAgInPc2d2
20	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Zřejmě	zřejmě	k6eAd1
okolo	okolo	k7c2
15	#num#	k4
<g/>
.	.	kIx.
roku	rok	k1gInSc2
života	život	k1gInSc2
ztrácejí	ztrácet	k5eAaImIp3nP
irbisové	irbis	k1gInPc1
schopnost	schopnost	k1gFnSc4
reprodukce	reprodukce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>