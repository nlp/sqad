<s>
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
KČT	KČT	kA	KČT
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zájmové	zájmový	k2eAgNnSc1d1	zájmové
sdružení	sdružení	k1gNnSc1	sdružení
turistů	turist	k1gMnPc2	turist
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
za	za	k7c4	za
Rakousko-Uherska	Rakousko-Uhersek	k1gMnSc4	Rakousko-Uhersek
<g/>
,	,	kIx,	,
působící	působící	k2eAgMnPc4d1	působící
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
