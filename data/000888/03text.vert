<s>
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
KČT	KČT	kA	KČT
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zájmové	zájmový	k2eAgNnSc1d1	zájmové
sdružení	sdružení	k1gNnSc1	sdružení
turistů	turist	k1gMnPc2	turist
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
za	za	k7c4	za
Rakousko-Uherska	Rakousko-Uhersek	k1gMnSc4	Rakousko-Uhersek
<g/>
,	,	kIx,	,
působící	působící	k2eAgMnPc4d1	působící
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
až	až	k9	až
1938	[number]	k4	1938
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Klubem	klub	k1gInSc7	klub
československých	československý	k2eAgMnPc2d1	československý
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1948	[number]	k4	1948
až	až	k9	až
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
zakázán	zakázat	k5eAaPmNgInS	zakázat
a	a	k8xC	a
sloučen	sloučen	k2eAgInSc1d1	sloučen
se	s	k7c7	s
sjednocenou	sjednocený	k2eAgFnSc7d1	sjednocená
tělovýchovou	tělovýchova	k1gFnSc7	tělovýchova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
obnovil	obnovit	k5eAaPmAgMnS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Turistické	turistický	k2eAgInPc1d1	turistický
výlety	výlet	k1gInPc1	výlet
organizovaly	organizovat	k5eAaBmAgInP	organizovat
mnohé	mnohý	k2eAgInPc1d1	mnohý
české	český	k2eAgInPc1d1	český
i	i	k8xC	i
německé	německý	k2eAgInPc1d1	německý
spolky	spolek	k1gInPc1	spolek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
zřejmě	zřejmě	k6eAd1	zřejmě
první	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
turistická	turistický	k2eAgFnSc1d1	turistická
organizace	organizace	k1gFnSc1	organizace
Pohorská	pohorský	k2eAgFnSc1d1	Pohorská
jednota	jednota	k1gFnSc1	jednota
Radhošť	Radhošť	k1gFnSc4	Radhošť
<g/>
,	,	kIx,	,
působící	působící	k2eAgFnSc4d1	působící
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Beskyd	Beskydy	k1gFnPc2	Beskydy
takových	takový	k3xDgInPc2	takový
turistických	turistický	k2eAgInPc2d1	turistický
spolků	spolek	k1gInPc2	spolek
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
organizace	organizace	k1gFnSc1	organizace
s	s	k7c7	s
celonárodním	celonárodní	k2eAgInSc7d1	celonárodní
charakterem	charakter	k1gInSc7	charakter
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
stala	stát	k5eAaPmAgFnS	stát
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
turistických	turistický	k2eAgFnPc2d1	turistická
organizací	organizace	k1gFnPc2	organizace
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1888	[number]	k4	1888
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
sále	sál	k1gInSc6	sál
Měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
besedy	beseda	k1gFnSc2	beseda
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
nadšenců	nadšenec	k1gMnPc2	nadšenec
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Vilémem	Vilém	k1gMnSc7	Vilém
Kurzem	Kurz	k1gMnSc7	Kurz
st.	st.	kA	st.
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
Národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
Pošumavské	pošumavský	k2eAgFnSc2d1	Pošumavská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Františkem	František	k1gMnSc7	František
Čížkem	Čížek	k1gMnSc7	Čížek
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
Národní	národní	k2eAgFnSc2d1	národní
jednoty	jednota	k1gFnSc2	jednota
Severočeské	severočeský	k2eAgInPc1d1	severočeský
<g/>
)	)	kIx)	)
a	a	k8xC	a
architektem	architekt	k1gMnSc7	architekt
Vratislavem	Vratislav	k1gMnSc7	Vratislav
Pasovským	Pasovský	k1gMnSc7	Pasovský
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
předsedu	předseda	k1gMnSc4	předseda
si	se	k3xPyFc3	se
ustavující	ustavující	k2eAgInSc1d1	ustavující
sjezd	sjezd	k1gInSc1	sjezd
nové	nový	k2eAgFnSc2d1	nová
turistické	turistický	k2eAgFnSc2d1	turistická
organizace	organizace	k1gFnSc2	organizace
sice	sice	k8xC	sice
zvolil	zvolit	k5eAaPmAgMnS	zvolit
známého	známý	k2eAgMnSc4d1	známý
cestovatele	cestovatel	k1gMnSc4	cestovatel
a	a	k8xC	a
veřejného	veřejný	k2eAgMnSc2d1	veřejný
činitele	činitel	k1gMnSc2	činitel
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Náprstka	Náprstka	k1gFnSc1	Náprstka
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
plnil	plnit	k5eAaImAgMnS	plnit
svoji	svůj	k3xOyFgFnSc4	svůj
úlohu	úloha	k1gFnSc4	úloha
známé	známý	k2eAgFnSc2d1	známá
osobnosti	osobnost	k1gFnSc2	osobnost
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
se	se	k3xPyFc4	se
mnoho	mnoho	k6eAd1	mnoho
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
a	a	k8xC	a
předsednické	předsednický	k2eAgNnSc4d1	předsednické
místo	místo	k1gNnSc4	místo
již	již	k6eAd1	již
po	po	k7c4	po
půl	půl	k1xP	půl
roce	rok	k1gInSc6	rok
opustil	opustit	k5eAaPmAgInS	opustit
na	na	k7c6	na
první	první	k4xOgFnSc6	první
valné	valný	k2eAgFnSc6d1	valná
hromadě	hromada	k1gFnSc6	hromada
KČT	KČT	kA	KČT
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
zájmů	zájem	k1gInPc2	zájem
<g/>
,	,	kIx,	,
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
povinností	povinnost	k1gFnPc2	povinnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
tíha	tíha	k1gFnSc1	tíha
klubovní	klubovní	k2eAgFnSc2d1	klubovní
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
přenesla	přenést	k5eAaPmAgFnS	přenést
na	na	k7c4	na
skupinu	skupina	k1gFnSc4	skupina
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
soustředěnou	soustředěný	k2eAgFnSc7d1	soustředěná
kolem	kolem	k7c2	kolem
Kurze	kurz	k1gInSc6	kurz
a	a	k8xC	a
Pasovského	Pasovského	k2eAgMnSc6d1	Pasovského
(	(	kIx(	(
<g/>
plaketa	plaketa	k1gFnSc1	plaketa
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
KČT	KČT	kA	KČT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
klubovým	klubový	k2eAgMnSc7d1	klubový
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1889	[number]	k4	1889
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
,	,	kIx,	,
třetím	třetí	k4xOgMnSc7	třetí
předsedou	předseda	k1gMnSc7	předseda
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
Arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Vratislav	Vratislav	k1gMnSc1	Vratislav
Pasovský	Pasovský	k1gMnSc1	Pasovský
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jím	jíst	k5eAaImIp1nS	jíst
po	po	k7c4	po
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Četné	četný	k2eAgFnPc1d1	četná
pobočky	pobočka	k1gFnPc1	pobočka
klubu	klub	k1gInSc2	klub
zakládaly	zakládat	k5eAaImAgFnP	zakládat
chaty	chata	k1gFnPc1	chata
a	a	k8xC	a
stavěly	stavět	k5eAaImAgFnP	stavět
rozhledny	rozhledna	k1gFnPc1	rozhledna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Petřínská	petřínský	k2eAgFnSc1d1	Petřínská
rozhledna	rozhledna	k1gFnSc1	rozhledna
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Kurzova	Kurzův	k2eAgFnSc1d1	Kurzova
rozhledna	rozhledna	k1gFnSc1	rozhledna
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
začaly	začít	k5eAaPmAgInP	začít
tvořit	tvořit	k5eAaImF	tvořit
také	také	k9	také
hustou	hustý	k2eAgFnSc4d1	hustá
síť	síť	k1gFnSc4	síť
turistických	turistický	k2eAgFnPc2d1	turistická
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1889	[number]	k4	1889
vychází	vycházet	k5eAaImIp3nS	vycházet
první	první	k4xOgNnSc1	první
číslo	číslo	k1gNnSc1	číslo
Časopisu	časopis	k1gInSc2	časopis
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
redakci	redakce	k1gFnSc4	redakce
převzal	převzít	k5eAaPmAgInS	převzít
profesor	profesor	k1gMnSc1	profesor
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
Vilém	Vilém	k1gMnSc1	Vilém
Kurz	Kurz	k1gMnSc1	Kurz
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
vychází	vycházet	k5eAaImIp3nS	vycházet
dodnes	dodnes	k6eAd1	dodnes
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Turista	turista	k1gMnSc1	turista
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Klub	klub	k1gInSc1	klub
československých	československý	k2eAgMnPc2d1	československý
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Československa	Československo	k1gNnSc2	Československo
se	s	k7c7	s
připojením	připojení	k1gNnSc7	připojení
slovenských	slovenský	k2eAgMnPc2d1	slovenský
turistů	turist	k1gMnPc2	turist
organizace	organizace	k1gFnSc2	organizace
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
Klub	klub	k1gInSc4	klub
československých	československý	k2eAgMnPc2d1	československý
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
masovou	masový	k2eAgFnSc7d1	masová
zájmovou	zájmový	k2eAgFnSc7d1	zájmová
organizací	organizace	k1gFnSc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
existenci	existence	k1gFnSc4	existence
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
názvem	název	k1gInSc7	název
zčásti	zčásti	k6eAd1	zčásti
obnovila	obnovit	k5eAaPmAgFnS	obnovit
na	na	k7c6	na
území	území	k1gNnSc6	území
Protektorátu	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
Sudet	Sudety	k1gInPc2	Sudety
<g/>
,	,	kIx,	,
odtržení	odtržení	k1gNnSc3	odtržení
a	a	k8xC	a
vzniku	vznik	k1gInSc3	vznik
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
komunisty	komunista	k1gMnSc2	komunista
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
byl	být	k5eAaImAgInS	být
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
zrušen	zrušen	k2eAgMnSc1d1	zrušen
<g/>
,	,	kIx,	,
rozpuštěn	rozpuštěn	k2eAgMnSc1d1	rozpuštěn
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
majetek	majetek	k1gInSc4	majetek
byl	být	k5eAaImAgInS	být
znárodněn	znárodněn	k2eAgInSc1d1	znárodněn
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
členská	členský	k2eAgFnSc1d1	členská
základna	základna	k1gFnSc1	základna
byla	být	k5eAaImAgFnS	být
slučovacími	slučovací	k2eAgFnPc7d1	slučovací
schůzemi	schůze	k1gFnPc7	schůze
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
začleněna	začlenit	k5eAaPmNgFnS	začlenit
do	do	k7c2	do
sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
<g/>
.	.	kIx.	.
</s>
<s>
Turistické	turistický	k2eAgFnPc1d1	turistická
aktivity	aktivita	k1gFnPc1	aktivita
se	se	k3xPyFc4	se
přesunuly	přesunout	k5eAaPmAgFnP	přesunout
převážně	převážně	k6eAd1	převážně
pod	pod	k7c4	pod
odbory	odbor	k1gInPc4	odbor
turistiky	turistika	k1gFnSc2	turistika
tělovýchovných	tělovýchovný	k2eAgFnPc2d1	Tělovýchovná
jednot	jednota	k1gFnPc2	jednota
ČSTV	ČSTV	kA	ČSTV
<g/>
.	.	kIx.	.
</s>
<s>
Obnoven	obnovit	k5eAaPmNgInS	obnovit
byl	být	k5eAaImAgInS	být
až	až	k9	až
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
kluby	klub	k1gInPc1	klub
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ve	v	k7c6	v
Volyni	Volyně	k1gFnSc6	Volyně
<g/>
)	)	kIx)	)
však	však	k9	však
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
z	z	k7c2	z
let	léto	k1gNnPc2	léto
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1948	[number]	k4	1948
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
neobnovily	obnovit	k5eNaPmAgFnP	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
znovuobnovený	znovuobnovený	k2eAgInSc1d1	znovuobnovený
klub	klub	k1gInSc1	klub
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
vrácení	vrácení	k1gNnSc4	vrácení
svého	svůj	k3xOyFgInSc2	svůj
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Uspěl	uspět	k5eAaPmAgMnS	uspět
zčásti	zčásti	k6eAd1	zčásti
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
získané	získaný	k2eAgInPc1d1	získaný
objekty	objekt	k1gInPc1	objekt
postupně	postupně	k6eAd1	postupně
zas	zas	k6eAd1	zas
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
postavit	postavit	k5eAaPmF	postavit
turistickou	turistický	k2eAgFnSc4d1	turistická
chatu	chata	k1gFnSc4	chata
v	v	k7c6	v
Prášilech	Prášil	k1gMnPc6	Prášil
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
zrekonstruovat	zrekonstruovat	k5eAaPmF	zrekonstruovat
Jiráskovu	Jiráskův	k2eAgFnSc4d1	Jiráskova
turistickou	turistický	k2eAgFnSc4d1	turistická
chatu	chata	k1gFnSc4	chata
na	na	k7c6	na
Dobrošově	Dobrošův	k2eAgNnSc6d1	Dobrošův
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
nich	on	k3xPp3gMnPc2	on
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
Brádlerovy	Brádlerův	k2eAgFnSc2d1	Brádlerova
boudy	bouda	k1gFnSc2	bouda
<g/>
,	,	kIx,	,
Voseckou	Vosecká	k1gFnSc4	Vosecká
boudu	bouda	k1gFnSc4	bouda
<g/>
,	,	kIx,	,
Výrovku	výrovka	k1gFnSc4	výrovka
<g/>
,	,	kIx,	,
TCH	tch	k0	tch
Pod	pod	k7c7	pod
Studničnou	Studničný	k2eAgFnSc7d1	Studničná
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
má	mít	k5eAaImIp3nS	mít
TCH	tch	k0	tch
Pláně	pláně	k1gNnSc1	pláně
pod	pod	k7c7	pod
Ještědem	Ještěd	k1gInSc7	Ještěd
<g/>
,	,	kIx,	,
PCH	pch	k0wR	pch
Polesí	Polesí	k1gNnSc4	Polesí
<g/>
,	,	kIx,	,
Raisovu	Raisův	k2eAgFnSc4d1	Raisova
chatu	chata	k1gFnSc4	chata
na	na	k7c6	na
Zvičíně	Zvičín	k1gInSc6	Zvičín
<g/>
,	,	kIx,	,
TCH	tch	k0	tch
Prachov	Prachov	k1gInSc1	Prachov
<g/>
,	,	kIx,	,
TCH	tch	k0	tch
Čihák	Čihák	k1gMnSc1	Čihák
<g/>
,	,	kIx,	,
TZ	TZ	kA	TZ
Kusalína	Kusalína	k1gFnSc1	Kusalína
a	a	k8xC	a
Chatový	chatový	k2eAgInSc1d1	chatový
tábor	tábor	k1gInSc1	tábor
v	v	k7c6	v
Jinolicích	Jinolice	k1gFnPc6	Jinolice
u	u	k7c2	u
Jičína	Jičín	k1gInSc2	Jičín
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spolumajitelem	spolumajitel	k1gMnSc7	spolumajitel
PCH	pch	k0wR	pch
Skalka	skalka	k1gFnSc1	skalka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
předsedou	předseda	k1gMnSc7	předseda
KČT	KČT	kA	KČT
PhDr.	PhDr.	kA	PhDr.
Jan	Jan	k1gMnSc1	Jan
Stráský	Stráský	k1gMnSc1	Stráský
<g/>
,	,	kIx,	,
čestným	čestný	k2eAgMnSc7d1	čestný
předsedou	předseda	k1gMnSc7	předseda
pak	pak	k8xC	pak
Ing.	ing.	kA	ing.
Jan	Jan	k1gMnSc1	Jan
Havelka	Havelka	k1gMnSc1	Havelka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
organizaci	organizace	k1gFnSc4	organizace
vedl	vést	k5eAaImAgMnS	vést
předchozích	předchozí	k2eAgNnPc2d1	předchozí
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
celostátní	celostátní	k2eAgFnSc2d1	celostátní
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
novým	nový	k2eAgMnSc7d1	nový
předsedou	předseda	k1gMnSc7	předseda
organizace	organizace	k1gFnSc2	organizace
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Mgr.	Mgr.	kA	Mgr.
Vratislav	Vratislav	k1gMnSc1	Vratislav
Chvátal	Chvátal	k1gMnSc1	Chvátal
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
asociace	asociace	k1gFnSc2	asociace
turistických	turistický	k2eAgInPc2d1	turistický
klubů	klub	k1gInPc2	klub
i	i	k9	i
Internationaler	Internationaler	k1gMnSc1	Internationaler
Volkssportverband	Volkssportverband	k1gInSc1	Volkssportverband
(	(	kIx(	(
<g/>
IVV	IVV	kA	IVV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pořádá	pořádat	k5eAaImIp3nS	pořádat
turistické	turistický	k2eAgFnPc4d1	turistická
akce	akce	k1gFnPc4	akce
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
a	a	k8xC	a
i	i	k9	i
veřejnost	veřejnost	k1gFnSc1	veřejnost
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
Pochod	pochod	k1gInSc1	pochod
Praha-Prčice	Praha-Prčice	k1gFnSc2	Praha-Prčice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
9	[number]	k4	9
<g/>
.	.	kIx.	.
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
turistickou	turistický	k2eAgFnSc4d1	turistická
olympiádu	olympiáda	k1gFnSc4	olympiáda
IVV	IVV	kA	IVV
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
měl	mít	k5eAaImAgInS	mít
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
přes	přes	k7c4	přes
35	[number]	k4	35
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
třetina	třetina	k1gFnSc1	třetina
mládež	mládež	k1gFnSc1	mládež
do	do	k7c2	do
26	[number]	k4	26
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
třetina	třetina	k1gFnSc1	třetina
senioři	senior	k1gMnPc1	senior
nad	nad	k7c4	nad
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tiskové	tiskový	k2eAgFnSc2d1	tisková
zprávy	zpráva	k1gFnSc2	zpráva
ke	k	k7c3	k
konferenci	konference	k1gFnSc3	konference
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
má	mít	k5eAaImIp3nS	mít
KČT	KČT	kA	KČT
33	[number]	k4	33
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
se	se	k3xPyFc4	se
KČT	KČT	kA	KČT
stal	stát	k5eAaPmAgInS	stát
řádným	řádný	k2eAgMnSc7d1	řádný
členem	člen	k1gMnSc7	člen
Českého	český	k2eAgInSc2d1	český
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
organizace	organizace	k1gFnSc2	organizace
mezi	mezi	k7c7	mezi
konferencemi	konference	k1gFnPc7	konference
je	být	k5eAaImIp3nS	být
Ústřední	ústřední	k2eAgInSc1d1	ústřední
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
ÚV	ÚV	kA	ÚV
KČT	KČT	kA	KČT
<g/>
)	)	kIx)	)
s	s	k7c7	s
pětičlenným	pětičlenný	k2eAgNnSc7d1	pětičlenné
předsednictvem	předsednictvo	k1gNnSc7	předsednictvo
a	a	k8xC	a
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
komise	komise	k1gFnSc1	komise
KČT	KČT	kA	KČT
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
má	mít	k5eAaImIp3nS	mít
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
několik	několik	k4yIc1	několik
sekcí	sekce	k1gFnPc2	sekce
<g/>
:	:	kIx,	:
pěší	pěší	k2eAgFnSc2d1	pěší
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
cykloturistiky	cykloturistika	k1gFnSc2	cykloturistika
<g/>
,	,	kIx,	,
lyžařské	lyžařský	k2eAgFnSc2d1	lyžařská
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
zdravotně	zdravotně	k6eAd1	zdravotně
postižených	postižený	k1gMnPc2	postižený
<g/>
,	,	kIx,	,
vysokohorské	vysokohorský	k2eAgFnPc1d1	vysokohorská
turistiky	turistika	k1gFnPc1	turistika
<g/>
,	,	kIx,	,
mototuristiky	mototuristika	k1gFnPc1	mototuristika
<g/>
,	,	kIx,	,
turistiky	turistika	k1gFnPc1	turistika
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
speleoturistiky	speleoturistika	k1gFnPc4	speleoturistika
aj.	aj.	kA	aj.
Ústřední	ústřední	k2eAgInSc1d1	ústřední
sekretariát	sekretariát	k1gInSc1	sekretariát
KČT	KČT	kA	KČT
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
<g/>
,	,	kIx,	,
Revoluční	revoluční	k2eAgInSc1d1	revoluční
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
2	[number]	k4	2
na	na	k7c6	na
Fügnerově	Fügnerův	k2eAgNnSc6d1	Fügnerovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
adresy	adresa	k1gFnSc2	adresa
v	v	k7c6	v
obchodním	obchodní	k2eAgNnSc6d1	obchodní
centru	centrum	k1gNnSc6	centrum
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
Lužiny	lužina	k1gFnSc2	lužina
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
generální	generální	k2eAgFnSc3d1	generální
opravě	oprava	k1gFnSc3	oprava
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
si	se	k3xPyFc3	se
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
pomocné	pomocný	k2eAgInPc4d1	pomocný
Rady	rad	k1gInPc4	rad
KČT	KČT	kA	KČT
-	-	kIx~	-
programovou	programový	k2eAgFnSc4d1	programová
<g/>
,	,	kIx,	,
mapovou	mapový	k2eAgFnSc4d1	mapová
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
prezentaci	prezentace	k1gFnSc4	prezentace
<g/>
,	,	kIx,	,
radu	rada	k1gFnSc4	rada
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
<g/>
,	,	kIx,	,
technickou	technický	k2eAgFnSc4d1	technická
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
značení	značení	k1gNnSc4	značení
a	a	k8xC	a
redakční	redakční	k2eAgFnSc4d1	redakční
radu	rada	k1gFnSc4	rada
časopisu	časopis	k1gInSc2	časopis
Turista	turista	k1gMnSc1	turista
<g/>
.	.	kIx.	.
</s>
<s>
Mládež	mládež	k1gFnSc1	mládež
(	(	kIx(	(
<g/>
tomíci	tomík	k1gMnPc1	tomík
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
organizována	organizovat	k5eAaBmNgFnS	organizovat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Asociaci	asociace	k1gFnSc6	asociace
turistických	turistický	k2eAgInPc2d1	turistický
oddílů	oddíl	k1gInPc2	oddíl
mládeže	mládež	k1gFnSc2	mládež
(	(	kIx(	(
<g/>
A-TOM	A-TOM	k1gMnSc1	A-TOM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
předsedou	předseda	k1gMnSc7	předseda
je	být	k5eAaImIp3nS	být
Mgr.	Mgr.	kA	Mgr.
Tomáš	Tomáš	k1gMnSc1	Tomáš
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
odbory	odbor	k1gInPc1	odbor
jsou	být	k5eAaImIp3nP	být
sdruženy	sdružit	k5eAaPmNgInP	sdružit
do	do	k7c2	do
14	[number]	k4	14
územních	územní	k2eAgFnPc2d1	územní
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
rok	rok	k1gInSc1	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vydávají	vydávat	k5eAaImIp3nP	vydávat
společné	společný	k2eAgInPc1d1	společný
kalendáře	kalendář	k1gInPc1	kalendář
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nP	volit
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnPc4	svůj
rady	rada	k1gFnPc4	rada
a	a	k8xC	a
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
připravují	připravovat	k5eAaImIp3nP	připravovat
své	svůj	k3xOyFgInPc4	svůj
programy	program	k1gInPc4	program
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
KČT	KČT	kA	KČT
Praha	Praha	k1gFnSc1	Praha
KČT	KČT	kA	KČT
Středočeská	středočeský	k2eAgFnSc1d1	Středočeská
oblast	oblast	k1gFnSc1	oblast
KČT	KČT	kA	KČT
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
KČT	KČT	kA	KČT
Plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
kraje	kraj	k1gInSc2	kraj
KČT	KČT	kA	KČT
Karlovarský	karlovarský	k2eAgInSc4d1	karlovarský
kraj	kraj	k1gInSc4	kraj
KČT	KČT	kA	KČT
Oblast	oblast	k1gFnSc4	oblast
Ústecký	ústecký	k2eAgInSc1d1	ústecký
kraj	kraj	k1gInSc1	kraj
KČT	KČT	kA	KČT
Liberecký	liberecký	k2eAgInSc1d1	liberecký
kraj	kraj	k1gInSc1	kraj
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Ještědská	ještědský	k2eAgFnSc1d1	Ještědská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
KČT	KČT	kA	KČT
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
kraj	kraj	k1gInSc1	kraj
KČT	KČT	kA	KČT
Pardubický	pardubický	k2eAgInSc1d1	pardubický
kraj	kraj	k1gInSc1	kraj
KČT	KČT	kA	KČT
Vysočina	vysočina	k1gFnSc1	vysočina
KČT	KČT	kA	KČT
Jihomoravská	jihomoravský	k2eAgFnSc1d1	Jihomoravská
oblast	oblast	k1gFnSc1	oblast
KČT	KČT	kA	KČT
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
KČT	KČT	kA	KČT
Oblast	oblast	k1gFnSc1	oblast
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1	Moravskoslezská
KČT	KČT	kA	KČT
Valašsko	Valašsko	k1gNnSc4	Valašsko
-	-	kIx~	-
Chřiby	Chřiby	k1gInPc1	Chřiby
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
oblastí	oblast	k1gFnPc2	oblast
má	mít	k5eAaImIp3nS	mít
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
desítky	desítka	k1gFnPc4	desítka
odborů	odbor	k1gInPc2	odbor
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Středočeská	středočeský	k2eAgFnSc1d1	Středočeská
oblast	oblast	k1gFnSc1	oblast
KČT	KČT	kA	KČT
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
uvádí	uvádět	k5eAaImIp3nS	uvádět
2763	[number]	k4	2763
členů	člen	k1gMnPc2	člen
v	v	k7c6	v
57	[number]	k4	57
odborech	odbor	k1gInPc6	odbor
a	a	k8xC	a
16	[number]	k4	16
oddílech	oddíl	k1gInPc6	oddíl
TOM	ten	k3xDgNnSc6	ten
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
časopisu	časopis	k1gInSc2	časopis
Turista	turista	k1gMnSc1	turista
přes	přes	k7c4	přes
své	svůj	k3xOyFgNnSc4	svůj
pražské	pražský	k2eAgNnSc4d1	Pražské
vydavatelství	vydavatelství	k1gNnSc4	vydavatelství
(	(	kIx(	(
<g/>
obchodní	obchodní	k2eAgFnSc1d1	obchodní
společnost	společnost	k1gFnSc1	společnost
Trasa	trasa	k1gFnSc1	trasa
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
)	)	kIx)	)
vydává	vydávat	k5eAaPmIp3nS	vydávat
opakovaně	opakovaně	k6eAd1	opakovaně
číslované	číslovaný	k2eAgFnSc2d1	číslovaná
turistické	turistický	k2eAgFnSc2d1	turistická
mapy	mapa	k1gFnSc2	mapa
v	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
1	[number]	k4	1
:	:	kIx,	:
50	[number]	k4	50
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
takto	takto	k6eAd1	takto
navazujících	navazující	k2eAgFnPc2d1	navazující
map	mapa	k1gFnPc2	mapa
je	být	k5eAaImIp3nS	být
98	[number]	k4	98
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
klub	klub	k1gInSc1	klub
převzal	převzít	k5eAaPmAgInS	převzít
od	od	k7c2	od
ministra	ministr	k1gMnSc2	ministr
RNDr.	RNDr.	kA	RNDr.
Libora	Libor	k1gMnSc2	Libor
Ambrozka	Ambrozka	k1gFnSc1	Ambrozka
Cenu	cena	k1gFnSc4	cena
ministra	ministr	k1gMnSc2	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgMnPc3	svůj
členům	člen	k1gMnPc3	člen
Předsednictvo	předsednictvo	k1gNnSc4	předsednictvo
KČT	KČT	kA	KČT
předává	předávat	k5eAaImIp3nS	předávat
Čestný	čestný	k2eAgInSc1d1	čestný
odznak	odznak	k1gInSc1	odznak
Vojty	Vojta	k1gMnSc2	Vojta
Náprstka	Náprstka	k1gFnSc1	Náprstka
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Veřejné	veřejný	k2eAgNnSc4d1	veřejné
uznání	uznání	k1gNnSc4	uznání
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
diplom	diplom	k1gInSc1	diplom
s	s	k7c7	s
medailí	medaile	k1gFnSc7	medaile
<g/>
)	)	kIx)	)
a	a	k8xC	a
Čestná	čestný	k2eAgNnPc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
české	český	k2eAgFnSc2d1	Česká
turistiky	turistika	k1gFnSc2	turistika
slavnostně	slavnostně	k6eAd1	slavnostně
uvedeny	uvést	k5eAaPmNgFnP	uvést
další	další	k2eAgFnPc1d1	další
osobnosti	osobnost	k1gFnPc1	osobnost
a	a	k8xC	a
kluby	klub	k1gInPc1	klub
spjaté	spjatý	k2eAgInPc1d1	spjatý
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
akce	akce	k1gFnSc1	akce
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
konáním	konání	k1gNnSc7	konání
Konference	konference	k1gFnSc2	konference
KČT	KČT	kA	KČT
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
podeváté	podeváté	k4xO	podeváté
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
tentokrát	tentokrát	k6eAd1	tentokrát
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
oceněni	ocenit	k5eAaPmNgMnP	ocenit
<g/>
:	:	kIx,	:
JUDr.	JUDr.	kA	JUDr.
Leopold	Leopold	k1gMnSc1	Leopold
Dorzil	Dorzil	k1gMnSc1	Dorzil
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Padevět	Padevět	k1gInSc1	Padevět
<g/>
,	,	kIx,	,
Doc.	doc.	kA	doc.
PhDr.	PhDr.	kA	PhDr.
Miroslav	Miroslav	k1gMnSc1	Miroslav
Hlaváček	Hlaváček	k1gMnSc1	Hlaváček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Srba	Srba	k1gMnSc1	Srba
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
Milan	Milan	k1gMnSc1	Milan
Machovec	Machovec	k1gMnSc1	Machovec
a	a	k8xC	a
odbor	odbor	k1gInSc1	odbor
KČT	KČT	kA	KČT
První	první	k4xOgInSc4	první
plzeňský	plzeňský	k2eAgInSc4d1	plzeňský
Klub	klub	k1gInSc4	klub
českých	český	k2eAgMnPc2d1	český
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
symbol	symbol	k1gInSc1	symbol
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
graficky	graficky	k6eAd1	graficky
upravený	upravený	k2eAgInSc1d1	upravený
text	text	k1gInSc1	text
názvu	název	k1gInSc2	název
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
národních	národní	k2eAgFnPc6d1	národní
barvách	barva	k1gFnPc6	barva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
užívání	užívání	k1gNnSc1	užívání
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
klub	klub	k1gInSc1	klub
vnímá	vnímat	k5eAaImIp3nS	vnímat
jako	jako	k9	jako
historický	historický	k2eAgInSc1d1	historický
symbol	symbol	k1gInSc1	symbol
prvky	prvek	k1gInPc1	prvek
turistického	turistický	k2eAgNnSc2d1	turistické
značení	značení	k1gNnSc2	značení
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
čtyřbarevné	čtyřbarevný	k2eAgNnSc4d1	čtyřbarevné
pásové	pásový	k2eAgNnSc4d1	pásové
značení	značení	k1gNnSc4	značení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klubu	klub	k1gInSc6	klub
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
celkem	celkem	k6eAd1	celkem
1384	[number]	k4	1384
aktivních	aktivní	k2eAgMnPc2d1	aktivní
značkařů	značkař	k1gMnPc2	značkař
pěších	pěší	k2eAgFnPc2d1	pěší
tras	trasa	k1gFnPc2	trasa
<g/>
,	,	kIx,	,
336	[number]	k4	336
cykloznačkařů	cykloznačkař	k1gMnPc2	cykloznačkař
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
počet	počet	k1gInSc1	počet
značkařů	značkař	k1gMnPc2	značkař
turistických	turistický	k2eAgFnPc2d1	turistická
tras	trasa	k1gFnPc2	trasa
cest	cesta	k1gFnPc2	cesta
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
(	(	kIx(	(
<g/>
hipoturistika	hipoturistika	k1gFnSc1	hipoturistika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
klub	klub	k1gInSc1	klub
udržoval	udržovat	k5eAaImAgInS	udržovat
39	[number]	k4	39
816	[number]	k4	816
km	km	kA	km
pěších	pěší	k2eAgFnPc2d1	pěší
tras	trasa	k1gFnPc2	trasa
<g/>
,	,	kIx,	,
31	[number]	k4	31
104	[number]	k4	104
km	km	kA	km
cyklotras	cyklotrasa	k1gFnPc2	cyklotrasa
<g/>
,	,	kIx,	,
387	[number]	k4	387
km	km	kA	km
lyžařských	lyžařský	k2eAgFnPc2d1	lyžařská
tras	trasa	k1gFnPc2	trasa
a	a	k8xC	a
1300	[number]	k4	1300
km	km	kA	km
hipotras	hipotrasa	k1gFnPc2	hipotrasa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
značení	značení	k1gNnSc2	značení
má	mít	k5eAaImIp3nS	mít
komise	komise	k1gFnSc1	komise
značení	značení	k1gNnSc2	značení
republiku	republika	k1gFnSc4	republika
rozdělenou	rozdělená	k1gFnSc7	rozdělená
na	na	k7c4	na
77	[number]	k4	77
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
úvah	úvaha	k1gFnPc2	úvaha
a	a	k8xC	a
plánů	plán	k1gInPc2	plán
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
počítaly	počítat	k5eAaImAgFnP	počítat
s	s	k7c7	s
vlastním	vlastní	k2eAgNnSc7d1	vlastní
muzeem	muzeum	k1gNnSc7	muzeum
už	už	k6eAd1	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
narušovaných	narušovaný	k2eAgInPc2d1	narušovaný
změnami	změna	k1gFnPc7	změna
režimů	režim	k1gInPc2	režim
<g/>
,	,	kIx,	,
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
problémy	problém	k1gInPc7	problém
a	a	k8xC	a
postavení	postavení	k1gNnSc1	postavení
organizace	organizace	k1gFnSc2	organizace
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc1	vedení
KČT	KČT	kA	KČT
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
podařilo	podařit	k5eAaPmAgNnS	podařit
otevřít	otevřít	k5eAaPmF	otevřít
Muzeum	muzeum	k1gNnSc1	muzeum
turistiky	turistika	k1gFnSc2	turistika
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
synagogy	synagoga	k1gFnSc2	synagoga
v	v	k7c6	v
Bechyni	Bechyně	k1gFnSc6	Bechyně
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
k	k	k7c3	k
provedení	provedení	k1gNnSc3	provedení
záměru	záměr	k1gInSc2	záměr
využila	využít	k5eAaPmAgFnS	využít
sponzorskou	sponzorský	k2eAgFnSc4d1	sponzorská
podporu	podpora	k1gFnSc4	podpora
z	z	k7c2	z
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
i	i	k8xC	i
fondů	fond	k1gInPc2	fond
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Správcem	správce	k1gMnSc7	správce
muzea	muzeum	k1gNnSc2	muzeum
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Sedláček	Sedláček	k1gMnSc1	Sedláček
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
