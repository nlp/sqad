<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
Edisonovy	Edisonův	k2eAgInPc4d1	Edisonův
vynálezy	vynález	k1gInPc4	vynález
patří	patřit	k5eAaImIp3nP	patřit
fonograf	fonograf	k1gInSc4	fonograf
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc2	předchůdce
gramofonu	gramofon	k1gInSc2	gramofon
<g/>
)	)	kIx)	)
a	a	k8xC	a
mylně	mylně	k6eAd1	mylně
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
počítána	počítán	k2eAgMnSc4d1	počítán
i	i	k9	i
žárovka	žárovka	k1gFnSc1	žárovka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
zdokonalil	zdokonalit	k5eAaPmAgMnS	zdokonalit
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
patentovat	patentovat	k5eAaBmF	patentovat
<g/>
.	.	kIx.	.
</s>
