<s>
Zasadil	zasadit	k5eAaPmAgInS	zasadit
se	se	k3xPyFc4	se
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
brněnské	brněnský	k2eAgFnSc2d1	brněnská
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
