<s>
Galapágy	Galapágy	k1gFnPc1	Galapágy
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Islas	Islas	k1gMnSc1	Islas
Galápagos	Galápagos	k1gMnSc1	Galápagos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Galápagos	Galápagos	k1gMnSc1	Galápagos
archipiélago	archipiélago	k1gMnSc1	archipiélago
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Galapážské	galapážský	k2eAgNnSc1d1	galapážské
souostroví	souostroví	k1gNnSc1	souostroví
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
ekvádorského	ekvádorský	k2eAgNnSc2d1	ekvádorské
souostroví	souostroví	k1gNnSc2	souostroví
18	[number]	k4	18
sopečných	sopečný	k2eAgInPc2d1	sopečný
ostrovů	ostrov	k1gInPc2	ostrov
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
asi	asi	k9	asi
1000	[number]	k4	1000
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
proslulé	proslulý	k2eAgInPc4d1	proslulý
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
ostrovech	ostrov	k1gInPc6	ostrov
žijícími	žijící	k2eAgFnPc7d1	žijící
<g/>
,	,	kIx,	,
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
druhy	druh	k1gInPc7	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
např.	např.	kA	např.
prehistoricky	prehistoricky	k6eAd1	prehistoricky
vyhlížejícími	vyhlížející	k2eAgMnPc7d1	vyhlížející
ještěry	ještěr	k1gMnPc7	ještěr
a	a	k8xC	a
obřími	obří	k2eAgFnPc7d1	obří
suchozemskými	suchozemský	k2eAgFnPc7d1	suchozemská
želvami	želva	k1gFnPc7	želva
<g/>
.	.	kIx.	.
</s>
<s>
Galapágy	Galapágy	k1gFnPc1	Galapágy
jsou	být	k5eAaImIp3nP	být
jediným	jediný	k2eAgNnSc7d1	jediné
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tučňáci	tučňák	k1gMnPc1	tučňák
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
druh	druh	k1gInSc4	druh
tučňáka	tučňák	k1gMnSc2	tučňák
galapážského	galapážský	k2eAgMnSc2d1	galapážský
(	(	kIx(	(
<g/>
Spheniscus	Spheniscus	k1gInSc1	Spheniscus
mendiculus	mendiculus	k1gInSc1	mendiculus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Galápago	Galápago	k6eAd1	Galápago
značí	značit	k5eAaImIp3nS	značit
želvu	želva	k1gFnSc4	želva
-	-	kIx~	-
název	název	k1gInSc1	název
znamená	znamenat	k5eAaImIp3nS	znamenat
tedy	tedy	k9	tedy
něco	něco	k3yInSc4	něco
jako	jako	k9	jako
"	"	kIx"	"
<g/>
želví	želví	k2eAgInPc1d1	želví
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
ekvádorských	ekvádorský	k2eAgFnPc6d1	ekvádorská
mapách	mapa	k1gFnPc6	mapa
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
pojmenování	pojmenování	k1gNnSc4	pojmenování
Archipiélago	Archipiélago	k6eAd1	Archipiélago
de	de	k?	de
Colón	colón	k1gInSc1	colón
<g/>
.	.	kIx.	.
</s>
<s>
Galapágy	Galapágy	k1gFnPc4	Galapágy
objevil	objevit	k5eAaPmAgMnS	objevit
Tomáš	Tomáš	k1gMnSc1	Tomáš
z	z	k7c2	z
Berlanga	Berlang	k1gMnSc2	Berlang
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1535	[number]	k4	1535
při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
z	z	k7c2	z
Panamy	Panama	k1gFnSc2	Panama
do	do	k7c2	do
Peru	Peru	k1gNnSc2	Peru
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebyly	být	k5eNaImAgInP	být
ostrovy	ostrov	k1gInPc1	ostrov
obydlené	obydlený	k2eAgInPc1d1	obydlený
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
souostroví	souostroví	k1gNnSc1	souostroví
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
námořních	námořní	k2eAgFnPc6d1	námořní
mapách	mapa	k1gFnPc6	mapa
až	až	k9	až
roku	rok	k1gInSc2	rok
1570	[number]	k4	1570
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
používaly	používat	k5eAaImAgInP	používat
ostrovy	ostrov	k1gInPc1	ostrov
pouze	pouze	k6eAd1	pouze
piráti	pirát	k1gMnPc1	pirát
a	a	k8xC	a
různí	různý	k2eAgMnPc1d1	různý
cestovatelé	cestovatel	k1gMnPc1	cestovatel
k	k	k7c3	k
doplňování	doplňování	k1gNnSc3	doplňování
zásob	zásoba	k1gFnPc2	zásoba
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
nepatřily	patřit	k5eNaImAgInP	patřit
ostrovy	ostrov	k1gInPc1	ostrov
nikomu	nikdo	k3yNnSc3	nikdo
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
anektoval	anektovat	k5eAaBmAgMnS	anektovat
Ekvádor	Ekvádor	k1gInSc4	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
přistál	přistát	k5eAaPmAgInS	přistát
s	s	k7c7	s
výzkumnou	výzkumný	k2eAgFnSc7d1	výzkumná
lodí	loď	k1gFnSc7	loď
HMS	HMS	kA	HMS
Beagle	Beagl	k1gInSc6	Beagl
britský	britský	k2eAgMnSc1d1	britský
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
.	.	kIx.	.
</s>
<s>
Strávil	strávit	k5eAaPmAgMnS	strávit
zde	zde	k6eAd1	zde
5	[number]	k4	5
týdnů	týden	k1gInPc2	týden
pozorováním	pozorování	k1gNnSc7	pozorování
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gMnPc7	jeho
životopisci	životopisec	k1gMnPc7	životopisec
panuje	panovat	k5eAaImIp3nS	panovat
obecná	obecný	k2eAgFnSc1d1	obecná
shoda	shoda	k1gFnSc1	shoda
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
těchto	tento	k3xDgNnPc2	tento
pozorování	pozorování	k1gNnPc2	pozorování
začal	začít	k5eAaPmAgMnS	začít
Darwin	Darwin	k1gMnSc1	Darwin
formulovat	formulovat	k5eAaImF	formulovat
své	své	k1gNnSc4	své
proslulé	proslulý	k2eAgFnSc2d1	proslulá
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
vydání	vydání	k1gNnSc1	vydání
i	i	k8xC	i
práce	práce	k1gFnSc1	práce
O	o	k7c6	o
původu	původ	k1gInSc6	původ
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
faunu	fauna	k1gFnSc4	fauna
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
jedním	jeden	k4xCgInSc7	jeden
velkým	velký	k2eAgInSc7d1	velký
národním	národní	k2eAgInSc7d1	národní
parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byly	být	k5eAaImAgFnP	být
Galapágy	Galapágy	k1gFnPc1	Galapágy
zapsány	zapsán	k2eAgFnPc1d1	zapsána
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
studie	studie	k1gFnSc2	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Thor	Thor	k1gMnSc1	Thor
Heyerdahl	Heyerdahl	k1gMnSc1	Heyerdahl
a	a	k8xC	a
Arne	Arne	k1gMnSc1	Arne
Skjø	Skjø	k1gMnSc1	Skjø
se	se	k3xPyFc4	se
na	na	k7c6	na
více	hodně	k6eAd2	hodně
místech	místo	k1gNnPc6	místo
nacházely	nacházet	k5eAaImAgFnP	nacházet
střepiny	střepina	k1gFnPc1	střepina
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
artefakty	artefakt	k1gInPc1	artefakt
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nálezy	nález	k1gInPc1	nález
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
souostroví	souostroví	k1gNnSc2	souostroví
nejspíš	nejspíš	k9	nejspíš
navštívili	navštívit	k5eAaPmAgMnP	navštívit
původní	původní	k2eAgMnPc1d1	původní
jihoameričtí	jihoamerický	k2eAgMnPc1d1	jihoamerický
obyvatelé	obyvatel	k1gMnPc1	obyvatel
před	před	k7c4	před
Španěly	Španěly	k1gInPc4	Španěly
<g/>
.	.	kIx.	.
</s>
<s>
Galapágy	Galapágy	k1gFnPc1	Galapágy
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
přetíná	přetínat	k5eAaImIp3nS	přetínat
severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
největšího	veliký	k2eAgInSc2d3	veliký
ostrova	ostrov	k1gInSc2	ostrov
Isabela	Isabela	k1gFnSc1	Isabela
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
7	[number]	k4	7
844	[number]	k4	844
km2	km2	k4	km2
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
ekvádorských	ekvádorský	k2eAgFnPc2d1	ekvádorská
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Puerto	Puerta	k1gFnSc5	Puerta
Baquerizo	Baqueriza	k1gFnSc5	Baqueriza
Moreno	Moren	k2eAgNnSc1d1	Moreno
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
San	San	k1gFnSc1	San
Cristóbal	Cristóbal	k1gInSc1	Cristóbal
<g/>
.	.	kIx.	.
</s>
<s>
Galapágy	Galapágy	k1gFnPc1	Galapágy
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
řídce	řídce	k6eAd1	řídce
osídleny	osídlit	k5eAaPmNgFnP	osídlit
<g/>
,	,	kIx,	,
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
31	[number]	k4	31
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Puerto	Puerta	k1gFnSc5	Puerta
Ayora	Ayora	k1gMnSc1	Ayora
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
je	být	k5eAaImIp3nS	být
tvořené	tvořený	k2eAgNnSc1d1	tvořené
z	z	k7c2	z
kuželů	kužel	k1gInPc2	kužel
mladých	mladý	k2eAgFnPc2d1	mladá
sopek	sopka	k1gFnPc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
činné	činný	k2eAgFnPc1d1	činná
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
největším	veliký	k2eAgInSc6d3	veliký
ostrově	ostrov	k1gInSc6	ostrov
Isabela	Isabela	k1gFnSc1	Isabela
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
,	,	kIx,	,
sopka	sopka	k1gFnSc1	sopka
Wolf	Wolf	k1gMnSc1	Wolf
s	s	k7c7	s
1	[number]	k4	1
707	[number]	k4	707
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Jejím	její	k3xOp3gInSc7	její
kráterem	kráter	k1gInSc7	kráter
prochází	procházet	k5eAaImIp3nS	procházet
rovník	rovník	k1gInSc1	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
suché	suchý	k2eAgNnSc4d1	suché
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
černý	černý	k2eAgInSc4d1	černý
povrch	povrch	k1gInSc4	povrch
z	z	k7c2	z
lávových	lávový	k2eAgFnPc2d1	lávová
skal	skála	k1gFnPc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
zalesněné	zalesněný	k2eAgFnPc1d1	zalesněná
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
vrcholy	vrchol	k1gInPc4	vrchol
sopek	sopka	k1gFnPc2	sopka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
ostrovy	ostrov	k1gInPc1	ostrov
Galapág	Galapágy	k1gFnPc2	Galapágy
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořen	k2eAgInPc1d1	vytvořen
z	z	k7c2	z
čedičové	čedičový	k2eAgFnSc2d1	čedičová
lávy	láva	k1gFnSc2	láva
při	při	k7c6	při
podmořských	podmořský	k2eAgFnPc6d1	podmořská
erupcích	erupce	k1gFnPc6	erupce
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
5,5	[number]	k4	5,5
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nejnovějších	nový	k2eAgInPc2d3	nejnovější
poznatků	poznatek	k1gInPc2	poznatek
se	se	k3xPyFc4	se
ostrovy	ostrov	k1gInPc4	ostrov
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
vzdalují	vzdalovat	k5eAaImIp3nP	vzdalovat
od	od	k7c2	od
pevninského	pevninský	k2eAgInSc2d1	pevninský
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
7	[number]	k4	7
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInPc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
ostrovy	ostrov	k1gInPc1	ostrov
Isabela	Isabela	k1gFnSc1	Isabela
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Cristóbal	Cristóbal	k1gInSc1	Cristóbal
<g/>
,	,	kIx,	,
Santiago	Santiago	k1gNnSc1	Santiago
<g/>
,	,	kIx,	,
Santa	Santa	k1gMnSc1	Santa
María	María	k1gMnSc1	María
a	a	k8xC	a
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
19	[number]	k4	19
ostrovů	ostrov	k1gInPc2	ostrov
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
1	[number]	k4	1
km2	km2	k4	km2
a	a	k8xC	a
desítky	desítka	k1gFnPc1	desítka
menších	malý	k2eAgInPc2d2	menší
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
<g/>
,	,	kIx,	,
útesů	útes	k1gInPc2	útes
a	a	k8xC	a
skalisek	skalisko	k1gNnPc2	skalisko
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
izolovanosti	izolovanost	k1gFnSc3	izolovanost
a	a	k8xC	a
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
od	od	k7c2	od
jihoamerické	jihoamerický	k2eAgFnSc2d1	jihoamerická
pevniny	pevnina	k1gFnSc2	pevnina
se	se	k3xPyFc4	se
na	na	k7c6	na
souostroví	souostroví	k1gNnSc6	souostroví
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
mnohé	mnohý	k2eAgInPc1d1	mnohý
endemické	endemický	k2eAgInPc1d1	endemický
druhy	druh	k1gInPc1	druh
fauny	fauna	k1gFnSc2	fauna
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
želva	želva	k1gFnSc1	želva
sloní	sloň	k1gFnPc2	sloň
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
10	[number]	k4	10
poddruhů	poddruh	k1gInPc2	poddruh
4	[number]	k4	4
zástupci	zástupce	k1gMnPc7	zástupce
leguánovitých	leguánovitý	k2eAgNnPc6d1	leguánovitý
(	(	kIx(	(
<g/>
leguán	leguán	k1gMnSc1	leguán
mořský	mořský	k2eAgMnSc1d1	mořský
<g/>
,	,	kIx,	,
leguán	leguán	k1gMnSc1	leguán
galapážský	galapážský	k2eAgMnSc1d1	galapážský
<g/>
,	,	kIx,	,
leguán	leguán	k1gMnSc1	leguán
bledý	bledý	k2eAgMnSc1d1	bledý
<g/>
,	,	kIx,	,
leguán	leguán	k1gMnSc1	leguán
galapážský	galapážský	k2eAgMnSc1d1	galapážský
růžový	růžový	k2eAgMnSc1d1	růžový
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
lachtan	lachtan	k1gMnSc1	lachtan
galapážský	galapážský	k2eAgMnSc1d1	galapážský
a	a	k8xC	a
lachtan	lachtan	k1gMnSc1	lachtan
mořský	mořský	k2eAgInSc4d1	mořský
ptáci	pták	k1gMnPc1	pták
racek	racek	k1gMnSc1	racek
lávový	lávový	k2eAgMnSc1d1	lávový
<g/>
,	,	kIx,	,
tučňák	tučňák	k1gMnSc1	tučňák
galapážský	galapážský	k2eAgMnSc1d1	galapážský
<g/>
,	,	kIx,	,
kormorán	kormorán	k1gMnSc1	kormorán
galapážský	galapážský	k2eAgMnSc1d1	galapážský
<g/>
,	,	kIx,	,
káně	káně	k1gFnSc1	káně
galapážská	galapážský	k2eAgFnSc1d1	galapážská
<g/>
,	,	kIx,	,
volavka	volavka	k1gFnSc1	volavka
galapážská	galapážský	k2eAgFnSc1d1	galapážská
<g/>
,	,	kIx,	,
albatros	albatros	k1gMnSc1	albatros
galapážský	galapážský	k2eAgMnSc1d1	galapážský
<g/>
,	,	kIx,	,
buřňák	buřňák	k1gMnSc1	buřňák
tmavohřbetý	tmavohřbetý	k2eAgMnSc1d1	tmavohřbetý
<g/>
,	,	kIx,	,
chřástal	chřástal	k1gMnSc1	chřástal
galapážský	galapážský	k2eAgMnSc1d1	galapážský
"	"	kIx"	"
<g/>
Darwinovy	Darwinův	k2eAgInPc4d1	Darwinův
(	(	kIx(	(
<g/>
též	též	k9	též
Galapážské	galapážský	k2eAgFnPc1d1	galapážská
<g/>
)	)	kIx)	)
pěnkavy	pěnkava	k1gFnPc1	pěnkava
<g/>
"	"	kIx"	"
-	-	kIx~	-
na	na	k7c4	na
15	[number]	k4	15
druhů	druh	k1gInPc2	druh
pěnkavovitých	pěnkavovitý	k2eAgMnPc2d1	pěnkavovitý
pěvců	pěvec	k1gMnPc2	pěvec
</s>
