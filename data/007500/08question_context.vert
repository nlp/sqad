<s>
Obléhání	obléhání	k1gNnSc1	obléhání
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1645	[number]	k4	1645
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
událost	událost	k1gFnSc1	událost
poslední	poslední	k2eAgFnSc2d1	poslední
fáze	fáze	k1gFnSc2	fáze
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
posádka	posádka	k1gFnSc1	posádka
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
500	[number]	k4	500
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
asi	asi	k9	asi
tisícovkou	tisícovka	k1gFnSc7	tisícovka
příslušníků	příslušník	k1gMnPc2	příslušník
městské	městský	k2eAgFnSc2d1	městská
milice	milice	k1gFnSc2	milice
<g/>
,	,	kIx,	,
ubránila	ubránit	k5eAaPmAgFnS	ubránit
město	město	k1gNnSc4	město
proti	proti	k7c3	proti
asi	asi	k9	asi
28	[number]	k4	28
tisícům	tisíc	k4xCgInPc3	tisíc
vojáků	voják	k1gMnPc2	voják
generála	generál	k1gMnSc2	generál
Lennarta	Lennart	k1gMnSc2	Lennart
Torstensona	Torstenson	k1gMnSc2	Torstenson
<g/>
.	.	kIx.	.
</s>

