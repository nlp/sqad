<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
geomorfologické	geomorfologický	k2eAgFnSc6d1
jednotce	jednotka	k1gFnSc6
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc4
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
České	český	k2eAgFnSc2d1
středohoříIUCN	středohoříIUCN	k?
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Pohled	pohled	k1gInSc1
na	na	k7c6
Dolní	dolní	k2eAgFnSc6d1
ZálezlyZákladní	ZálezlyZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1976	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1068,9	1068,9	k4
km²	km²	k?
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraje	kraj	k1gInSc2
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
a	a	k8xC
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
39	#num#	k4
<g/>
′	′	k?
<g/>
5	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
33	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
51	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.ceskestredohori.ochranaprirody.cz	www.ceskestredohori.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
mezi	mezi	k7c7
Louny	Louny	k1gInPc7
a	a	k8xC
Českou	český	k2eAgFnSc7d1
Lípou	lípa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
podkrušnohorské	podkrušnohorský	k2eAgFnSc2d1
subprovincie	subprovincie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozloha	rozloha	k1gFnSc1
je	být	k5eAaImIp3nS
1068,9	1068,9	k4
km²	km²	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
celé	celý	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
1600	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
jí	on	k3xPp3gFnSc3
řadí	řadit	k5eAaImIp3nP
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
do	do	k7c2
velikosti	velikost	k1gFnSc2
na	na	k7c4
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přirozeně	přirozeně	k6eAd1
ji	on	k3xPp3gFnSc4
dělí	dělit	k5eAaImIp3nS
řeka	řeka	k1gFnSc1
Labe	Labe	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
CHKO	CHKO	kA
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS
části	část	k1gFnPc4
území	území	k1gNnSc2
sedmi	sedm	k4xCc2
okresů	okres	k1gInPc2
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
,	,	kIx,
Děčín	Děčín	k1gInSc1
<g/>
,	,	kIx,
Litoměřice	Litoměřice	k1gInPc1
<g/>
,	,	kIx,
Louny	Louny	k1gInPc1
<g/>
,	,	kIx,
Most	most	k1gInSc1
<g/>
,	,	kIx,
Teplice	Teplice	k1gFnPc1
a	a	k8xC
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
)	)	kIx)
Nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
je	být	k5eAaImIp3nS
vrchol	vrchol	k1gInSc1
Milešovky	Milešovka	k1gFnSc2
(	(	kIx(
<g/>
836,5	836,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
naopak	naopak	k6eAd1
nejnižším	nízký	k2eAgFnPc3d3
je	být	k5eAaImIp3nS
hladina	hladina	k1gFnSc1
Labe	Labe	k1gNnSc2
v	v	k7c6
Děčíně	Děčín	k1gInSc6
(	(	kIx(
<g/>
121,9	121,9	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Litoměřicích	Litoměřice	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
hlavní	hlavní	k2eAgInPc4d1
body	bod	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
vedly	vést	k5eAaImAgInP
k	k	k7c3
vyhlášení	vyhlášení	k1gNnSc3
CHKO	CHKO	kA
patří	patřit	k5eAaImIp3nS
středoevropská	středoevropský	k2eAgFnSc1d1
jedinečnost	jedinečnost	k1gFnSc1
krajinného	krajinný	k2eAgInSc2d1
reliéfu	reliéf	k1gInSc2
mladotřetihorního	mladotřetihorní	k2eAgNnSc2d1
vulkanického	vulkanický	k2eAgNnSc2d1
pohoří	pohoří	k1gNnSc2
<g/>
,	,	kIx,
pestrost	pestrost	k1gFnSc1
geologické	geologický	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
<g/>
,	,	kIx,
druhové	druhový	k2eAgNnSc4d1
bohatství	bohatství	k1gNnSc4
rostlinstva	rostlinstvo	k1gNnSc2
a	a	k8xC
odpovídající	odpovídající	k2eAgNnSc4d1
oživení	oživení	k1gNnSc4
krajiny	krajina	k1gFnSc2
charakteristickou	charakteristický	k2eAgFnSc7d1
faunou	fauna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genové	genový	k2eAgFnSc2d1
lesní	lesní	k2eAgFnSc2d1
základny	základna	k1gFnSc2
v	v	k7c6
CHKO	CHKO	kA
jsou	být	k5eAaImIp3nP
zaměřeny	zaměřit	k5eAaPmNgFnP
především	především	k9
na	na	k7c4
buk	buk	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
dále	daleko	k6eAd2
udržován	udržován	k2eAgInSc4d1
a	a	k8xC
reprodukován	reprodukován	k2eAgInSc4d1
genetický	genetický	k2eAgInSc4d1
materiál	materiál	k1gInSc4
jilmu	jilm	k1gInSc2
<g/>
,	,	kIx,
javoru	javor	k1gInSc2
<g/>
,	,	kIx,
lípy	lípa	k1gFnSc2
a	a	k8xC
jeřábu	jeřáb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Založení	založení	k1gNnSc1
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc1
CHKO	CHKO	kA
</s>
<s>
Mapa	mapa	k1gFnSc1
s	s	k7c7
obrysem	obrys	k1gInSc7
CHKO	CHKO	kA
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
</s>
<s>
CHKO	CHKO	kA
České	český	k2eAgFnPc1d1
středohoří	středohoří	k1gNnSc3
byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1976	#num#	k4
výnosem	výnos	k1gInSc7
Ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
ČSR	ČSR	kA
č.	č.	k?
<g/>
j.	j.	k?
6.883	6.883	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
svém	svůj	k3xOyFgNnSc6
založení	založení	k1gNnSc6
měla	mít	k5eAaImAgFnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
působnosti	působnost	k1gFnSc6
1071	#num#	k4
km²	km²	k?
a	a	k8xC
sídlo	sídlo	k1gNnSc1
v	v	k7c6
Litoměřicích	Litoměřice	k1gInPc6
<g/>
,	,	kIx,
Michalské	Michalský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měla	mít	k5eAaImAgFnS
na	na	k7c6
starosti	starost	k1gFnSc6
21	#num#	k4
SPR	SPR	kA
–	–	k?
Státních	státní	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
CHN	CHN	kA
–	–	k?
chráněná	chráněný	k2eAgNnPc4d1
naleziště	naleziště	k1gNnSc4
<g/>
,	,	kIx,
10	#num#	k4
CHPV	CHPV	kA
–	–	k?
chráněný	chráněný	k2eAgInSc4d1
přírodní	přírodní	k2eAgInSc4d1
výtvor	výtvor	k1gInSc4
a	a	k8xC
2	#num#	k4
naučné	naučný	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
pečuje	pečovat	k5eAaImIp3nS
o	o	k7c4
řadu	řada	k1gFnSc4
maloplošných	maloplošný	k2eAgNnPc2d1
zvláště	zvláště	k6eAd1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
to	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
Národní	národní	k2eAgMnSc1d1
přírodní	přírodní	k2eAgMnSc1d1
rezervaceLovoš	rezervaceLovoš	k1gMnSc1
<g/>
,	,	kIx,
Milešovka	Milešovka	k1gFnSc1
<g/>
,	,	kIx,
Oblík	oblík	k1gInSc1
<g/>
,	,	kIx,
Raná	raný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Sedlo	sedlo	k1gNnSc1
</s>
<s>
Národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
památkaBílé	památkaBílý	k2eAgFnPc1d1
stráně	stráň	k1gFnPc1
<g/>
,	,	kIx,
Boreč	Boreč	k1gInSc1
<g/>
,	,	kIx,
Březinské	Březinský	k2eAgInPc1d1
tisy	tis	k1gInPc1
<g/>
,	,	kIx,
Dubí	dubí	k1gNnSc1
hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Jánský	jánský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
,	,	kIx,
Kamenná	kamenný	k2eAgNnPc1d1
slunce	slunce	k1gNnPc1
<g/>
,	,	kIx,
Panská	panský	k2eAgFnSc1d1
skála	skála	k1gFnSc1
a	a	k8xC
Vrkoč	vrkoč	k1gFnSc1
</s>
<s>
Přírodní	přírodní	k2eAgNnPc1d1
rezervaceBohyňská	rezervaceBohyňský	k2eAgNnPc1d1
lada	lado	k1gNnPc1
<g/>
,	,	kIx,
Březina	Březina	k1gMnSc1
<g/>
,	,	kIx,
Číčov	Číčov	k1gInSc1
<g/>
,	,	kIx,
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
u	u	k7c2
Hlinné	hlinný	k2eAgFnSc2d1
<g/>
,	,	kIx,
Hradišťanská	Hradišťanský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
<g/>
,	,	kIx,
Kalvárie	Kalvárie	k1gFnSc1
<g/>
,	,	kIx,
Kamenná	kamenný	k2eAgFnSc1d1
hůra	hůra	k1gFnSc1
<g/>
,	,	kIx,
Kozí	kozí	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
,	,	kIx,
Lipská	lipský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Milá	milý	k2eAgFnSc1d1
<g/>
,	,	kIx,
Sluneční	sluneční	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
a	a	k8xC
Vrabinec	Vrabinec	k1gInSc1
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památkaBabinské	památkaBabinský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
<g/>
,	,	kIx,
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
<g/>
,	,	kIx,
Divoká	divoký	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
<g/>
,	,	kIx,
Farská	farský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
<g/>
,	,	kIx,
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Jílovské	jílovský	k2eAgInPc1d1
tisy	tis	k1gInPc1
<g/>
,	,	kIx,
Košťálov	Košťálov	k1gInSc1
<g/>
,	,	kIx,
Kuzov	Kuzov	k1gInSc1
<g/>
,	,	kIx,
Loupežnická	loupežnický	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
<g/>
,	,	kIx,
Lužické	lužický	k2eAgInPc1d1
šipáky	šipák	k1gInPc1
<g/>
,	,	kIx,
Magnetovec	magnetovec	k1gInSc1
–	–	k?
Skalní	skalní	k2eAgInSc1d1
hřib	hřib	k1gInSc1
<g/>
,	,	kIx,
Nebočadský	Nebočadský	k2eAgInSc1d1
luh	luh	k1gInSc1
<g/>
,	,	kIx,
Plešivec	plešivec	k1gMnSc1
<g/>
,	,	kIx,
Radobýl	Radobýl	k1gMnSc1
<g/>
,	,	kIx,
Stříbrný	stříbrný	k2eAgInSc1d1
roh	roh	k1gInSc1
<g/>
,	,	kIx,
Štěpánovská	Štěpánovský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Tobiášův	Tobiášův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
,	,	kIx,
Třtěnské	Třtěnský	k2eAgFnPc1d1
stráně	stráň	k1gFnPc1
</s>
<s>
Mimo	mimo	k7c4
chráněnou	chráněný	k2eAgFnSc4d1
krajinnou	krajinný	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
České	český	k2eAgFnSc2d1
středohoří	středohoří	k1gNnSc4
její	její	k3xOp3gFnSc1
správa	správa	k1gFnSc1
pečuje	pečovat	k5eAaImIp3nS
o	o	k7c4
národní	národní	k2eAgFnPc4d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
Bořeň	Bořeň	k1gFnSc1
<g/>
,	,	kIx,
Zlatník	zlatník	k1gInSc1
<g/>
,	,	kIx,
Malý	malý	k2eAgInSc1d1
a	a	k8xC
Velký	velký	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
,	,	kIx,
Jezerka	Jezerka	k1gFnSc1
a	a	k8xC
národní	národní	k2eAgFnPc1d1
přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
Kleneč	Kleneč	k1gFnSc1
a	a	k8xC
Velký	velký	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Chráněnou	chráněný	k2eAgFnSc7d1
krajinnou	krajinný	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
vede	vést	k5eAaImIp3nS
dálnice	dálnice	k1gFnSc1
D8	D8	k1gFnSc1
a	a	k8xC
silnice	silnice	k1gFnSc1
I.	I.	kA
třídy	třída	k1gFnSc2
č.	č.	k?
8	#num#	k4
15	#num#	k4
<g/>
,	,	kIx,
28	#num#	k4
<g/>
,	,	kIx,
30	#num#	k4
a	a	k8xC
62	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojení	spojení	k1gNnSc1
po	po	k7c6
železnici	železnice	k1gFnSc6
umožňují	umožňovat	k5eAaImIp3nP
tratě	trať	k1gFnPc1
Praha	Praha	k1gFnSc1
<g/>
–	–	k?
<g/>
Děčín	Děčín	k1gInSc1
<g/>
,	,	kIx,
Čížkovice	Čížkovice	k1gFnSc1
<g/>
–	–	k?
<g/>
Obrnice	Obrnice	k1gFnSc2
<g/>
,	,	kIx,
Lovosice	Lovosice	k1gInPc4
–	–	k?
Teplice	teplice	k1gFnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
Lovosice	Lovosice	k1gInPc4
–	–	k?
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
,	,	kIx,
Děčín	Děčín	k1gInSc1
<g/>
–	–	k?
<g/>
Rumburk	Rumburk	k1gInSc1
<g/>
,	,	kIx,
Benešov	Benešov	k1gInSc1
nad	nad	k7c7
Ploučnicí	Ploučnice	k1gFnSc7
–	–	k?
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
a	a	k8xC
muzeální	muzeální	k2eAgFnSc1d1
trať	trať	k1gFnSc1
Velké	velká	k1gFnSc2
Březno	Březno	k1gNnSc1
–	–	k?
Zubrnice	Zubrnice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Sesuv	sesuv	k1gInSc1
půdy	půda	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
způsobil	způsobit	k5eAaPmAgInS
uzavření	uzavření	k1gNnSc4
části	část	k1gFnSc2
trati	trať	k1gFnSc2
Lovosice	Lovosice	k1gInPc4
–	–	k?
Teplice	teplice	k1gFnSc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
v	v	k7c6
úseku	úsek	k1gInSc6
od	od	k7c2
Radejčína	Radejčín	k1gInSc2
do	do	k7c2
Lovosic	Lovosice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oprava	oprava	k1gFnSc1
trati	trať	k1gFnSc2
je	být	k5eAaImIp3nS
plánovaná	plánovaný	k2eAgFnSc1d1
na	na	k7c4
rok	rok	k1gInSc4
2022	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Naučné	naučný	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
</s>
<s>
V	v	k7c6
péči	péče	k1gFnSc6
CHKO	CHKO	kA
je	být	k5eAaImIp3nS
(	(	kIx(
<g/>
stav	stav	k1gInSc1
2013	#num#	k4
dle	dle	k7c2
webu	web	k1gInSc2
<g/>
)	)	kIx)
devět	devět	k4xCc4
naučných	naučný	k2eAgFnPc2d1
stezek	stezka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Boreč	Boreč	k1gInSc1
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Lovoš	Lovoš	k1gMnSc1
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Pod	pod	k7c7
Vysokým	vysoký	k2eAgInSc7d1
Ostrým	ostrý	k2eAgInSc7d1
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Bedřicha	Bedřich	k1gMnSc2
Smetany	Smetana	k1gMnSc2
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Luční	luční	k2eAgFnSc1d1
potok	potok	k1gInSc4
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Řepčice	Řepčice	k1gFnSc1
-	-	kIx~
Panna	Panna	k1gFnSc1
</s>
<s>
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Březina	Březina	k1gMnSc1
</s>
<s>
Cesta	cesta	k1gFnSc1
přátelství	přátelství	k1gNnSc2
</s>
<s>
Tyršova	Tyršův	k2eAgFnSc1d1
naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
V	v	k7c6
Českém	český	k2eAgNnSc6d1
středohoří	středohoří	k1gNnSc6
je	být	k5eAaImIp3nS
řada	řada	k1gFnSc1
dalších	další	k2eAgFnPc2d1
stezek	stezka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vybudovaly	vybudovat	k5eAaPmAgFnP
jiné	jiný	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Těžba	těžba	k1gFnSc1
kamene	kámen	k1gInSc2
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojována	ozdrojován	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
CHKO	CHKO	kA
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
se	se	k3xPyFc4
na	na	k7c6
několika	několik	k4yIc6
místech	místo	k1gNnPc6
těží	těžet	k5eAaImIp3nS
kámen	kámen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lomy	lom	k1gInPc7
ohrožují	ohrožovat	k5eAaImIp3nP
<g/>
,	,	kIx,
nebo	nebo	k8xC
již	již	k9
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
zlikvidovaly	zlikvidovat	k5eAaPmAgInP
některé	některý	k3yIgInPc1
vrchy	vrch	k1gInPc1
<g/>
:	:	kIx,
Kubačka	Kubačka	k1gFnSc1
545	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Deblík	Deblík	k1gInSc1
459	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Trabice	Trabice	k1gFnSc1
429	#num#	k4
m.	m.	k?
Všechny	všechen	k3xTgFnPc1
jmenované	jmenovaná	k1gFnPc1
v	v	k7c6
okruhu	okruh	k1gInSc6
1,5	1,5	k4
km	km	kA
od	od	k7c2
obce	obec	k1gFnSc2
Libochovany	Libochovan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
vrchy	vrch	k1gInPc4
zcela	zcela	k6eAd1
zanikly	zaniknout	k5eAaPmAgInP
<g/>
:	:	kIx,
Vršetín	Vršetín	k1gInSc1
470	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Stříbrník	stříbrník	k1gInSc1
413	#num#	k4
m	m	kA
ad	ad	k7c4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
AUTORSKÝ	autorský	k2eAgInSc4d1
KOLEKTIV	kolektiv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
3	#num#	k4
<g/>
:	:	kIx,
Informatorium	Informatorium	k1gNnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85368	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
CHKO	CHKO	kA
Kokořínsko	Kokořínsko	k1gNnSc1
<g/>
,	,	kIx,
s.	s.	k?
57	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Regionální	regionální	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turistická	turistický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapy	mapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BACHORÍK	BACHORÍK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trať	trať	k1gFnSc1
mezi	mezi	k7c7
Lovosicemi	Lovosice	k1gInPc7
a	a	k8xC
Teplicemi	Teplice	k1gFnPc7
<g/>
,	,	kIx,
poškozená	poškozený	k2eAgFnSc1d1
sesuvem	sesuv	k1gInSc7
půdy	půda	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
,	,	kIx,
by	by	kYmCp3nS
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
dočkat	dočkat	k5eAaPmF
opravy	oprava	k1gFnPc4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2020-03-01	2020-03-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
České	český	k2eAgFnSc2d1
středohoří	středohoří	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
České	český	k2eAgFnSc2d1
středohoří	středohoří	k1gNnSc4
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgMnSc1d1
kraj	kraj	k7c2
•	•	k?
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Břehyně	Břehyně	k1gFnSc1
–	–	k?
Pecopala	Pecopal	k1gMnSc2
•	•	k?
Jezevčí	jezevčí	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Novozámecký	Novozámecký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
a	a	k8xC
Malý	malý	k2eAgInSc1d1
Bezděz	Bezděz	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Jestřebské	Jestřebský	k2eAgFnPc1d1
slatiny	slatina	k1gFnPc1
•	•	k?
Panská	panská	k1gFnSc1
skála	skála	k1gFnSc1
•	•	k?
Peklo	peklo	k1gNnSc1
•	•	k?
Swamp	Swamp	k1gMnSc1
Přírodní	přírodní	k2eAgMnSc1d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Hradčanské	hradčanský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Jílovka	jílovka	k1gFnSc1
•	•	k?
Klíč	klíč	k1gInSc1
•	•	k?
Kokořínský	Kokořínský	k2eAgInSc4d1
důl	důl	k1gInSc4
•	•	k?
Kostelecké	Kostelecké	k2eAgInPc4d1
bory	bor	k1gInPc4
•	•	k?
Luž	Luž	k1gFnSc1
•	•	k?
Mokřady	mokřad	k1gInPc1
horní	horní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
•	•	k?
Ralsko	Ralsko	k1gNnSc1
•	•	k?
Vlhošť	Vlhošť	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
•	•	k?
Brazilka	Brazilka	k1gFnSc1
•	•	k?
Cihelenské	Cihelenský	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Černý	černý	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Červený	červený	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
–	–	k?
mokřad	mokřad	k1gInSc4
v	v	k7c6
nivě	niva	k1gFnSc6
Šporky	Šporka	k1gMnSc2
•	•	k?
Deštenské	Deštenský	k2eAgFnPc1d1
pastviny	pastvina	k1gFnPc1
•	•	k?
Děvín	Děvín	k1gInSc1
a	a	k8xC
Ostrý	ostrý	k2eAgMnSc1d1
•	•	k?
Divadlo	divadlo	k1gNnSc1
•	•	k?
Dutý	dutý	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Farská	farský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Husa	Hus	k1gMnSc2
•	•	k?
Jelení	jelení	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
•	•	k?
Kamenný	kamenný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
u	u	k7c2
Křenova	Křenov	k1gInSc2
•	•	k?
Kaňon	kaňon	k1gInSc1
potoka	potok	k1gInSc2
Kolné	Kolná	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Manušické	Manušický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Martinské	martinský	k2eAgFnSc2d1
stěny	stěna	k1gFnSc2
•	•	k?
Meandry	meandr	k1gInPc1
Ploučnice	Ploučnice	k1gFnSc2
u	u	k7c2
Mimoně	Mimoň	k1gFnSc2
•	•	k?
Naděje	naděje	k1gFnSc1
•	•	k?
Niva	niva	k1gFnSc1
Ploučnice	Ploučnice	k1gFnSc1
u	u	k7c2
Žizníkova	Žizníkův	k2eAgInSc2d1
•	•	k?
Okřešické	Okřešický	k2eAgFnPc1d1
louky	louka	k1gFnPc1
•	•	k?
Osinalické	Osinalický	k2eAgFnPc1d1
bučiny	bučina	k1gFnPc1
•	•	k?
Pískovna	pískovna	k1gFnSc1
Žizníkov	Žizníkov	k1gInSc1
•	•	k?
Pod	pod	k7c7
Hvězdou	hvězda	k1gFnSc7
•	•	k?
Prameny	pramen	k1gInPc1
Pšovky	Pšovka	k1gFnSc2
•	•	k?
Provodínské	Provodínský	k2eAgInPc1d1
kameny	kámen	k1gInPc1
•	•	k?
Pustý	pustý	k2eAgInSc1d1
zámek	zámek	k1gInSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Černého	Černý	k1gMnSc2
rybníka	rybník	k1gInSc2
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Mařeničky	Mařenička	k1gFnSc2
•	•	k?
Ronov	Ronov	k1gInSc1
•	•	k?
Skalice	Skalice	k1gFnSc2
u	u	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
•	•	k?
Stohánek	Stohánek	k1gMnSc1
•	•	k?
Stružnické	Stružnický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Stříbrný	stříbrný	k1gInSc1
vrch	vrch	k1gInSc1
•	•	k?
Široký	široký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
U	u	k7c2
Rozmoklé	rozmoklý	k2eAgFnSc2d1
žáby	žába	k1gFnSc2
•	•	k?
Vranovské	vranovský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Zahrádky	zahrádka	k1gFnSc2
u	u	k7c2
České	český	k2eAgFnSc2d1
Lípy	lípa	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Děčín	Děčín	k1gInSc1
Národní	národní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
•	•	k?
Růžovský	Růžovský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Březinské	Březinský	k2eAgInPc4d1
tisy	tis	k1gInPc4
•	•	k?
Pravčická	Pravčická	k1gFnSc1
brána	brána	k1gFnSc1
•	•	k?
Zlatý	zlatý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Arba	arba	k1gFnSc1
•	•	k?
Bohyňská	Bohyňský	k2eAgFnSc1d1
lada	lado	k1gNnSc2
•	•	k?
Čabel	Čabel	k1gInSc1
•	•	k?
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
u	u	k7c2
Jílového	Jílové	k1gNnSc2
•	•	k?
Kamenná	kamenný	k2eAgFnSc1d1
hůra	hůra	k1gFnSc1
•	•	k?
Maiberg	Maiberg	k1gInSc1
•	•	k?
Marschnerova	Marschnerův	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Pavlínino	Pavlínin	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
•	•	k?
Pekelský	pekelský	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Spravedlnost	spravedlnost	k1gFnSc1
•	•	k?
Studený	studený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Světlík	světlík	k1gInSc1
•	•	k?
Vápenka	vápenka	k1gFnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Vrabinec	Vrabinec	k1gInSc1
•	•	k?
Za	za	k7c7
pilou	pila	k1gFnSc7
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
•	•	k?
Hofberg	Hofberg	k1gInSc1
•	•	k?
Jeskyně	jeskyně	k1gFnSc1
pod	pod	k7c7
Sněžníkem	Sněžník	k1gInSc7
•	•	k?
Jílovské	jílovský	k2eAgInPc4d1
tisy	tis	k1gInPc4
•	•	k?
Kytlice	kytlice	k1gFnSc2
•	•	k?
Líska	líska	k1gFnSc1
•	•	k?
Louka	louka	k1gFnSc1
u	u	k7c2
Brodských	Brodská	k1gFnPc2
•	•	k?
Meandry	meandr	k1gInPc1
Chřibské	chřibský	k2eAgFnSc2d1
Kamenice	Kamenice	k1gFnSc2
•	•	k?
Nebočadský	Nebočadský	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Noldenteich	Noldenteich	k1gInSc1
•	•	k?
Pod	pod	k7c7
lesem	les	k1gInSc7
•	•	k?
Rybník	rybník	k1gInSc1
u	u	k7c2
Králova	Králův	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
•	•	k?
Sojčí	sojčí	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Stará	starat	k5eAaImIp3nS
Oleška	Olešek	k1gMnSc2
•	•	k?
Stříbrný	stříbrný	k2eAgInSc4d1
roh	roh	k1gInSc4
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Litoměřice	Litoměřice	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Dolní	dolní	k2eAgNnSc1d1
Poohří	Poohří	k1gNnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Lovoš	Lovoš	k1gMnSc1
•	•	k?
Milešovka	Milešovka	k1gFnSc1
•	•	k?
Sedlo	sedlo	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bílé	bílý	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
•	•	k?
Borečský	Borečský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Dubí	dubí	k1gNnSc2
hora	hora	k1gFnSc1
•	•	k?
Kleneč	Kleneč	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Březina	Březina	k1gMnSc1
•	•	k?
Holý	Holý	k1gMnSc1
vrch	vrch	k1gInSc1
•	•	k?
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
u	u	k7c2
Hlinné	hlinný	k2eAgFnSc2d1
•	•	k?
Kalvárie	Kalvárie	k1gFnSc2
•	•	k?
Lipská	lipský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Loužek	loužka	k1gFnPc2
•	•	k?
Mokřady	mokřad	k1gInPc1
dolní	dolní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
•	•	k?
Myslivna	myslivna	k1gFnSc1
•	•	k?
Na	na	k7c6
Černčí	Černčí	k2eAgFnSc6d1
•	•	k?
Pístecký	Pístecký	k2eAgInSc1d1
les	les	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1
stráně	stráň	k1gFnPc1
u	u	k7c2
Štětí	štětit	k5eAaImIp3nP
•	•	k?
Dobříňský	Dobříňský	k2eAgInSc1d1
háj	háj	k1gInSc1
•	•	k?
Evaňská	Evaňský	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Říp	Říp	k1gInSc1
•	•	k?
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Košťálov	Košťálovo	k1gNnPc2
•	•	k?
Koštice	Koštice	k1gFnSc2
•	•	k?
Kuzov	Kuzov	k1gInSc1
•	•	k?
Mokřad	mokřad	k1gInSc1
pod	pod	k7c7
Terezínskou	terezínský	k2eAgFnSc7d1
pevností	pevnost	k1gFnSc7
•	•	k?
Na	na	k7c6
Dlouhé	Dlouhé	k2eAgFnSc6d1
stráni	stráň	k1gFnSc6
•	•	k?
Písčiny	písčina	k1gFnSc2
u	u	k7c2
Oleška	Olešek	k1gInSc2
•	•	k?
Plešivec	plešivec	k1gMnSc1
•	•	k?
Radobýl	Radobýl	k1gMnSc1
•	•	k?
Radouň	Radouň	k1gFnSc1
•	•	k?
Skalky	skalka	k1gFnSc2
u	u	k7c2
Třebutiček	Třebutička	k1gFnPc2
•	•	k?
Slatiniště	slatiniště	k1gNnSc4
u	u	k7c2
Vrbky	vrbka	k1gFnSc2
•	•	k?
Sovice	sovice	k1gFnSc2
u	u	k7c2
Brzánek	Brzánka	k1gFnPc2
•	•	k?
Stráně	stráň	k1gFnSc2
nad	nad	k7c7
Suchým	suchý	k2eAgInSc7d1
potokem	potok	k1gInSc7
•	•	k?
Stráně	stráň	k1gFnSc2
u	u	k7c2
Drahobuzi	Drahobuze	k1gFnSc4
•	•	k?
Stráně	stráň	k1gFnPc1
u	u	k7c2
Velkého	velký	k2eAgInSc2d1
Újezdu	Újezd	k1gInSc2
•	•	k?
Údolí	údolí	k1gNnSc1
Podbradeckého	Podbradecký	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
V	v	k7c6
kuksu	kuks	k1gInSc6
•	•	k?
Vrch	vrch	k1gInSc1
Hazmburk	Hazmburk	k1gInSc1
•	•	k?
Vrbka	vrbka	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Louny	Louny	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Džbán	džbán	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Malý	malý	k2eAgInSc1d1
a	a	k8xC
Velký	velký	k2eAgInSc1d1
štít	štít	k1gInSc1
•	•	k?
Oblík	oblík	k1gInSc1
•	•	k?
Raná	raný	k2eAgFnSc1d1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
</s>
<s>
Kamenná	kamenný	k2eAgNnPc1d1
slunce	slunce	k1gNnPc1
•	•	k?
Velký	velký	k2eAgInSc1d1
vrch	vrch	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Blatenský	blatenský	k2eAgInSc1d1
svah	svah	k1gInSc1
•	•	k?
Číčov	Číčov	k1gInSc1
•	•	k?
Dětanský	Dětanský	k2eAgInSc1d1
chlum	chlum	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Blšanský	blšanský	k2eAgInSc1d1
chlum	chlum	k1gInSc1
•	•	k?
Březno	Březno	k1gNnSc1
u	u	k7c2
Postoloprt	Postoloprta	k1gFnPc2
•	•	k?
Háj	háj	k1gInSc1
Petra	Petr	k1gMnSc2
Bezruče	Bezruč	k1gFnSc2
•	•	k?
Jalovcové	jalovcový	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
nad	nad	k7c7
Vrbičkou	vrbička	k1gFnSc7
•	•	k?
Koštice	Koštice	k1gFnSc2
•	•	k?
Kozinecká	Kozinecký	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Krásný	krásný	k2eAgInSc1d1
Dvůr	Dvůr	k1gInSc1
•	•	k?
Miocenní	Miocenní	k2eAgFnSc3d1
sladkovodní	sladkovodní	k2eAgFnSc3d1
vápence	vápenka	k1gFnSc3
•	•	k?
Na	na	k7c6
Spáleništi	spáleniště	k1gNnSc6
•	•	k?
Soběchlebské	Soběchlebský	k2eAgFnPc1d1
terasy	terasa	k1gFnPc1
•	•	k?
Staňkovice	Staňkovice	k1gFnSc2
•	•	k?
Stráně	stráň	k1gFnSc2
nad	nad	k7c7
Chomutovkou	Chomutovka	k1gFnSc7
•	•	k?
Stroupeč	Stroupeč	k1gMnSc1
•	•	k?
Štola	štola	k1gFnSc1
Stradonice	Stradonice	k1gFnSc2
•	•	k?
Tobiášův	Tobiášův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Travertinová	travertinový	k2eAgFnSc1d1
kupa	kupa	k1gFnSc1
•	•	k?
Třtěnské	Třtěnský	k2eAgFnPc4d1
stráně	stráň	k1gFnPc4
•	•	k?
Údolí	údolí	k1gNnSc1
Hasiny	Hasina	k1gMnSc2
u	u	k7c2
Lipence	Lipence	k1gFnSc2
•	•	k?
V	v	k7c6
hlubokém	hluboký	k2eAgInSc6d1
•	•	k?
Vrbina	vrbina	k1gFnSc1
u	u	k7c2
Nové	Nové	k2eAgFnSc2d1
Vsi	ves	k1gFnSc2
•	•	k?
Žatec	Žatec	k1gInSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Most	most	k1gInSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bořeň	Bořeň	k1gFnSc1
•	•	k?
Jezerka	Jezerka	k1gFnSc1
•	•	k?
Zlatník	zlatník	k1gInSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Jánský	jánský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Černý	černý	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Milá	milá	k1gFnSc1
•	•	k?
Písečný	písečný	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Chloumek	chloumek	k1gInSc1
•	•	k?
Lomské	Lomská	k1gFnSc2
údolí	údolí	k1gNnSc2
•	•	k?
Lužické	lužický	k2eAgInPc1d1
šipáky	šipák	k1gInPc1
•	•	k?
Kopistská	kopistský	k2eAgFnSc1d1
výsypka	výsypka	k1gFnSc1
•	•	k?
Velká	velký	k2eAgFnSc1d1
Volavka	volavka	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Teplice	teplice	k1gFnSc2
Přírodní	přírodní	k2eAgFnSc2d1
parky	park	k1gInPc1
</s>
<s>
Východní	východní	k2eAgFnPc1d1
Krušné	krušný	k2eAgFnPc1d1
hory	hora	k1gFnPc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bořeň	Bořeň	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Březina	Březina	k1gFnSc1
•	•	k?
Černá	černý	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Dřínek	dřínek	k1gInSc1
•	•	k?
Grünwaldské	Grünwaldský	k2eAgNnSc4d1
vřesoviště	vřesoviště	k1gNnSc4
•	•	k?
Hradišťanská	Hradišťanský	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Malhostický	Malhostický	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
U	u	k7c2
jezera	jezero	k1gNnSc2
-	-	kIx~
Cínovecké	Cínovecký	k2eAgNnSc1d1
rašeliniště	rašeliniště	k1gNnSc1
•	•	k?
Trupelník	Trupelník	k1gMnSc1
•	•	k?
Vlčí	vlčet	k5eAaImIp3nS
důl	důl	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Buky	buk	k1gInPc1
na	na	k7c6
Bouřňáku	bouřňák	k1gMnSc6
•	•	k?
Cínovecký	Cínovecký	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
•	•	k?
Domaslavické	Domaslavická	k1gFnSc2
údolí	údolí	k1gNnSc2
•	•	k?
Doubravská	Doubravský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Háj	háj	k1gInSc1
u	u	k7c2
Oseka	Osek	k1gInSc2
•	•	k?
Husův	Husův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Kateřina	Kateřina	k1gFnSc1
-	-	kIx~
mokřad	mokřad	k1gInSc1
•	•	k?
Pod	pod	k7c7
Lysou	lysý	k2eAgFnSc7d1
horou	hora	k1gFnSc7
•	•	k?
Salesiova	Salesiův	k2eAgFnSc1d1
výšina	výšina	k1gFnSc1
•	•	k?
Štěpánovská	Štěpánovský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Přírodní	přírodní	k2eAgFnSc2d1
parky	park	k1gInPc1
</s>
<s>
Východní	východní	k2eAgFnPc1d1
Krušné	krušný	k2eAgFnPc1d1
hory	hora	k1gFnPc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Vrkoč	vrkoč	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Kozí	kozí	k2eAgInSc4d1
vrch	vrch	k1gInSc4
•	•	k?
Libouchecké	Libouchecký	k2eAgInPc4d1
rybníčky	rybníček	k1gInPc4
•	•	k?
Niva	niva	k1gFnSc1
Olšového	olšový	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Rač	ráčit	k5eAaImRp2nS
•	•	k?
Rájecká	Rájecká	k1gFnSc1
rašeliniště	rašeliniště	k1gNnSc2
•	•	k?
Sluneční	sluneční	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Špičák	špičák	k1gInSc1
u	u	k7c2
Krásného	krásný	k2eAgInSc2d1
Lesa	les	k1gInSc2
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
</s>
<s>
Babinské	Babinský	k2eAgFnPc1d1
louky	louka	k1gFnPc1
•	•	k?
Divoká	divoký	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Eiland	Eiland	k1gInSc1
•	•	k?
Loupežnická	loupežnický	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
•	•	k?
Magnetovec	magnetovec	k1gInSc1
–	–	k?
Skalní	skalní	k2eAgInSc1d1
hřib	hřib	k1gInSc1
•	•	k?
Tiské	Tiská	k1gFnSc2
stěny	stěna	k1gFnSc2
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
