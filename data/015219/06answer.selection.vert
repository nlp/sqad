<s>
Mechanická	mechanický	k2eAgFnSc1d1
a	a	k8xC
organická	organický	k2eAgFnSc1d1
solidarita	solidarita	k1gFnSc1
jsou	být	k5eAaImIp3nP
typy	typ	k1gInPc7
společenské	společenský	k2eAgFnSc2d1
solidarity	solidarita	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
v	v	k7c6
roce	rok	k1gInSc6
1893	#num#	k4
představil	představit	k5eAaPmAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
stěžejní	stěžejní	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
<g/>
,	,	kIx,
rozvinuté	rozvinutý	k2eAgFnSc6d1
v	v	k7c6
díle	díl	k1gInSc6
Společenská	společenský	k2eAgFnSc1d1
dělba	dělba	k1gFnSc1
práce	práce	k1gFnSc2
(	(	kIx(
<g/>
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
originále	originál	k1gInSc6
De	De	k?
la	la	k1gNnPc2
Division	Division	k1gInSc1
du	du	k?
Travail	Travail	k1gInSc1
Social	Social	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
významný	významný	k2eAgMnSc1d1
francouzský	francouzský	k2eAgMnSc1d1
sociolog	sociolog	k1gMnSc1
Émile	Émile	k1gFnSc2
Durkheim	Durkheim	k1gMnSc1
<g/>
.	.	kIx.
</s>