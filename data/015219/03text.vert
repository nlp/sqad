<s>
Organická	organický	k2eAgFnSc1d1
a	a	k8xC
mechanická	mechanický	k2eAgFnSc1d1
solidarita	solidarita	k1gFnSc1
</s>
<s>
Mechanická	mechanický	k2eAgFnSc1d1
a	a	k8xC
organická	organický	k2eAgFnSc1d1
solidarita	solidarita	k1gFnSc1
jsou	být	k5eAaImIp3nP
typy	typ	k1gInPc7
společenské	společenský	k2eAgFnSc2d1
solidarity	solidarita	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
v	v	k7c6
roce	rok	k1gInSc6
1893	#num#	k4
představil	představit	k5eAaPmAgMnS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
stěžejní	stěžejní	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
<g/>
,	,	kIx,
rozvinuté	rozvinutý	k2eAgFnSc6d1
v	v	k7c6
díle	díl	k1gInSc6
Společenská	společenský	k2eAgFnSc1d1
dělba	dělba	k1gFnSc1
práce	práce	k1gFnSc2
(	(	kIx(
<g/>
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
originále	originál	k1gInSc6
De	De	k?
la	la	k1gNnPc2
Division	Division	k1gInSc1
du	du	k?
Travail	Travail	k1gInSc1
Social	Social	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
významný	významný	k2eAgMnSc1d1
francouzský	francouzský	k2eAgMnSc1d1
sociolog	sociolog	k1gMnSc1
Émile	Émile	k1gFnSc2
Durkheim	Durkheim	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústředními	ústřední	k2eAgInPc7d1
koncepty	koncept	k1gInPc7
této	tento	k3xDgFnSc2
Durkheimovy	Durkheimův	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
jsou	být	k5eAaImIp3nP
dělba	dělba	k1gFnSc1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
společenská	společenský	k2eAgFnSc1d1
solidarita	solidarita	k1gFnSc1
a	a	k8xC
vztah	vztah	k1gInSc1
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Durkheim	Durkheim	k1gMnSc1
klasifikoval	klasifikovat	k5eAaImAgMnS
společnosti	společnost	k1gFnPc4
na	na	k7c4
dva	dva	k4xCgInPc4
typy	typ	k1gInPc4
dle	dle	k7c2
převládající	převládající	k2eAgFnSc2d1
povahy	povaha	k1gFnSc2
jejich	jejich	k3xOp3gFnSc2
solidarity	solidarita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
předtím	předtím	k6eAd1
však	však	k9
rozdělil	rozdělit	k5eAaPmAgMnS
solidaritu	solidarita	k1gFnSc4
z	z	k7c2
hlediska	hledisko	k1gNnSc2
subjektů	subjekt	k1gInPc2
na	na	k7c4
solidaritu	solidarita	k1gFnSc4
pozitivní	pozitivní	k2eAgFnSc4d1
a	a	k8xC
negativní	negativní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Negativní	negativní	k2eAgFnSc4d1
solidaritu	solidarita	k1gFnSc4
definoval	definovat	k5eAaBmAgInS
jako	jako	k9
vztah	vztah	k1gInSc1
osoby	osoba	k1gFnSc2
a	a	k8xC
věci	věc	k1gFnSc2
a	a	k8xC
pozitivní	pozitivní	k2eAgFnSc4d1
solidaritu	solidarita	k1gFnSc4
jako	jako	k8xC,k8xS
vzájemný	vzájemný	k2eAgInSc4d1
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
jednotlivci	jednotlivec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
pozitivní	pozitivní	k2eAgFnSc3d1
solidaritě	solidarita	k1gFnSc3
přikládal	přikládat	k5eAaImAgInS
Durkheim	Durkheim	k1gInSc1
největší	veliký	k2eAgInSc4d3
význam	význam	k1gInSc4
a	a	k8xC
dále	daleko	k6eAd2
ji	on	k3xPp3gFnSc4
rozdělil	rozdělit	k5eAaPmAgInS
na	na	k7c4
solidaritu	solidarita	k1gFnSc4
mechanickou	mechanický	k2eAgFnSc4d1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
spojuje	spojovat	k5eAaImIp3nS
s	s	k7c7
tradiční	tradiční	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
a	a	k8xC
solidaritu	solidarita	k1gFnSc4
organickou	organický	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
váže	vázat	k5eAaImIp3nS
ke	k	k7c3
společnosti	společnost	k1gFnSc3
moderní	moderní	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Durkheim	Durkheim	k1gMnSc1
tedy	tedy	k9
nesouhlasil	souhlasit	k5eNaImAgMnS
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
převládala	převládat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Moderní	moderní	k2eAgInSc1d1
industrialismus	industrialismus	k1gInSc1
sociální	sociální	k2eAgFnSc4d1
solidaritu	solidarita	k1gFnSc4
nevyhnutelně	vyhnutelně	k6eNd1
ničí	ničit	k5eAaImIp3nS
a	a	k8xC
ohrožuje	ohrožovat	k5eAaImIp3nS
stavbu	stavba	k1gFnSc4
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
”	”	k?
Naopak	naopak	k6eAd1
zastával	zastávat	k5eAaImAgMnS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Organické	organický	k2eAgFnSc2d1
formy	forma	k1gFnSc2
solidarity	solidarita	k1gFnSc2
vytvářejí	vytvářet	k5eAaImIp3nP
daleko	daleko	k6eAd1
pevnější	pevný	k2eAgNnPc4d2
pouta	pouto	k1gNnPc4
vzájemné	vzájemný	k2eAgFnSc2d1
závislosti	závislost	k1gFnSc2
<g/>
,	,	kIx,
poněvadž	poněvadž	k8xS
tyto	tento	k3xDgFnPc1
formy	forma	k1gFnPc1
nabízejí	nabízet	k5eAaImIp3nP
potenciál	potenciál	k1gInSc4
pro	pro	k7c4
lepší	dobrý	k2eAgFnSc4d2
vyváženost	vyváženost	k1gFnSc4
mezi	mezi	k7c7
individuálními	individuální	k2eAgInPc7d1
rozdíly	rozdíl	k1gInPc7
a	a	k8xC
kolektivním	kolektivní	k2eAgInSc7d1
účelem	účel	k1gInSc7
<g/>
.	.	kIx.
<g/>
”	”	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proměna	proměna	k1gFnSc1
charakteru	charakter	k1gInSc2
společnosti	společnost	k1gFnSc2
tradiční	tradiční	k2eAgFnSc2d1
na	na	k7c4
společnost	společnost	k1gFnSc4
moderní	moderní	k2eAgFnSc2d1
</s>
<s>
Zásadní	zásadní	k2eAgFnSc1d1
proměna	proměna	k1gFnSc1
charakteru	charakter	k1gInSc2
společnosti	společnost	k1gFnSc2
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
v	v	k7c6
sociologii	sociologie	k1gFnSc6
označované	označovaný	k2eAgFnSc6d1
jako	jako	k8xS,k8xC
tradiční	tradiční	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
)	)	kIx)
v	v	k7c6
důsledku	důsledek	k1gInSc6
průmyslové	průmyslový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
a	a	k8xC
navazujících	navazující	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
revolucí	revoluce	k1gFnPc2
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
pochopitelný	pochopitelný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c6
sociologii	sociologie	k1gFnSc6
<g/>
,	,	kIx,
coby	coby	k?
vznikající	vznikající	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
samostatný	samostatný	k2eAgInSc1d1
vědní	vědní	k2eAgInSc1d1
obor	obor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
všichni	všechen	k3xTgMnPc1
tehdejší	tehdejší	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
sociologie	sociologie	k1gFnSc2
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
tyto	tento	k3xDgFnPc4
změny	změna	k1gFnPc4
ve	v	k7c6
společnosti	společnost	k1gFnSc6
staví	stavit	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
do	do	k7c2
protikladu	protiklad	k1gInSc6
dvě	dva	k4xCgFnPc1
formy	forma	k1gFnPc1
lidského	lidský	k2eAgNnSc2d1
soužití	soužití	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
jako	jako	k8xS,k8xC
bipolární	bipolární	k2eAgFnSc1d1
vidí	vidět	k5eAaImIp3nS
v	v	k7c6
modifikovaných	modifikovaný	k2eAgFnPc6d1
podobách	podoba	k1gFnPc6
kromě	kromě	k7c2
E.	E.	kA
Durkheima	Durkheim	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
díle	díl	k1gInSc6
rozlišuje	rozlišovat	k5eAaImIp3nS
společnosti	společnost	k1gFnPc1
se	s	k7c7
solidaritou	solidarita	k1gFnSc7
mechanickou	mechanický	k2eAgFnSc7d1
a	a	k8xC
solidaritou	solidarita	k1gFnSc7
organickou	organický	k2eAgFnSc7d1
<g/>
,	,	kIx,
také	také	k9
sociologové	sociolog	k1gMnPc1
F.	F.	kA
Tönnies	Tönnies	k1gMnSc1
(	(	kIx(
<g/>
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
staví	stavit	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
tradiční	tradiční	k2eAgFnSc4d1
pospolitost	pospolitost	k1gFnSc4
do	do	k7c2
kontrastu	kontrast	k1gInSc2
s	s	k7c7
moderní	moderní	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
H.	H.	kA
Spencer	Spencer	k1gInSc1
(	(	kIx(
<g/>
stavějící	stavějící	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
průmyslovou	průmyslový	k2eAgFnSc7d1
proti	proti	k7c3
společnosti	společnost	k1gFnSc3
vojenské	vojenský	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
C.H.	C.H.	k1gMnSc1
<g/>
Cooley	Coolea	k1gFnSc2
(	(	kIx(
<g/>
skupiny	skupina	k1gFnPc1
primární	primární	k2eAgFnSc1d1
a	a	k8xC
skupiny	skupina	k1gFnPc1
sekundární	sekundární	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
H.	H.	kA
Becker	Becker	k1gInSc1
(	(	kIx(
<g/>
společnosti	společnost	k1gFnSc2
posvátné	posvátný	k2eAgFnSc2d1
a	a	k8xC
privátní	privátní	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
R.	R.	kA
MacIver	MacIver	k1gInSc1
(	(	kIx(
<g/>
komunity	komunita	k1gFnSc2
a	a	k8xC
asociace	asociace	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
R.	R.	kA
Redfield	Redfield	k1gInSc1
(	(	kIx(
<g/>
společnost	společnost	k1gFnSc1
venkovská	venkovský	k2eAgFnSc1d1
a	a	k8xC
městská	městský	k2eAgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Solidarita	solidarita	k1gFnSc1
mechanická	mechanický	k2eAgFnSc1d1
</s>
<s>
V	v	k7c6
tradiční	tradiční	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
dělba	dělba	k1gFnSc1
práce	práce	k1gFnSc2
teprve	teprve	k6eAd1
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
počátcích	počátek	k1gInPc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
solidarita	solidarita	k1gFnSc1
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
v	v	k7c6
podstatě	podstata	k1gFnSc6
stejného	stejný	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
každý	každý	k3xTgInSc4
musí	muset	k5eAaImIp3nP
vykonávat	vykonávat	k5eAaImF
všechny	všechen	k3xTgFnPc1
činnosti	činnost	k1gFnPc1
nezbytné	nezbytný	k2eAgFnPc1d1,k2eNgFnPc1d1
pro	pro	k7c4
své	svůj	k3xOyFgNnSc4
přežití	přežití	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
se	se	k3xPyFc4
tedy	tedy	k9
od	od	k7c2
sebe	se	k3xPyFc2
liší	lišit	k5eAaImIp3nP
jen	jen	k6eAd1
velmi	velmi	k6eAd1
málo	málo	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
je	být	k5eAaImIp3nS
solidarita	solidarita	k1gFnSc1
založena	založit	k5eAaPmNgFnS
na	na	k7c6
stejném	stejný	k2eAgNnSc6d1
vnímání	vnímání	k1gNnSc6
světa	svět	k1gInSc2
<g/>
,	,	kIx,
sdílení	sdílení	k1gNnSc4
společných	společný	k2eAgFnPc2d1
hodnot	hodnota	k1gFnPc2
<g/>
,	,	kIx,
zkušeností	zkušenost	k1gFnPc2
<g/>
,	,	kIx,
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
citů	cit	k1gInPc2
i	i	k8xC
tužeb	tužba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
prvky	prvek	k1gInPc4
se	se	k3xPyFc4
pojí	pojit	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
kolektivního	kolektivní	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
jedince	jedinko	k6eAd1
přivádí	přivádět	k5eAaImIp3nS
ke	k	k7c3
vzájemné	vzájemný	k2eAgFnSc3d1
spolupráci	spolupráce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Vědomí	vědomí	k1gNnSc1
jednotlivce	jednotlivec	k1gMnSc2
je	být	k5eAaImIp3nS
tedy	tedy	k8xC
totožné	totožný	k2eAgNnSc4d1
s	s	k7c7
vědomím	vědomí	k1gNnSc7
kolektivním	kolektivní	k2eAgNnSc7d1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
jedinec	jedinec	k1gMnSc1
ztrácí	ztrácet	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
individualitu	individualita	k1gFnSc4
<g/>
,	,	kIx,
svébytnost	svébytnost	k1gFnSc4
a	a	k8xC
nezávislost	nezávislost	k1gFnSc4
a	a	k8xC
stává	stávat	k5eAaImIp3nS
se	s	k7c7
zneužitelným	zneužitelný	k2eAgInSc7d1
jako	jako	k8xC,k8xS
složka	složka	k1gFnSc1
mechanické	mechanický	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vnitřní	vnitřní	k2eAgFnSc1d1
celistvost	celistvost	k1gFnSc1
tradiční	tradiční	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
zabezpečuje	zabezpečovat	k5eAaImIp3nS
tlak	tlak	k1gInSc1
kolektivního	kolektivní	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
a	a	k8xC
všeobecně	všeobecně	k6eAd1
závazná	závazný	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
jsou	být	k5eAaImIp3nP
donucovacím	donucovací	k2eAgInSc7d1
nástrojem	nástroj	k1gInSc7
proti	proti	k7c3
všemu	všecek	k3xTgNnSc3
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
může	moct	k5eAaImIp3nS
tuto	tento	k3xDgFnSc4
celistvost	celistvost	k1gFnSc4
ohrozit	ohrozit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
Durkheima	Durkheim	k1gMnSc2
zde	zde	k6eAd1
jedinec	jedinec	k1gMnSc1
nepatří	patřit	k5eNaImIp3nS
sobě	se	k3xPyFc3
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
věcí	věc	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
disponuje	disponovat	k5eAaBmIp3nS
společnost	společnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Vazba	vazba	k1gFnSc1
mezi	mezi	k7c7
jedinci	jedinec	k1gMnPc7
bez	bez	k7c2
vlastního	vlastní	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
a	a	k8xC
společností	společnost	k1gFnPc2
je	být	k5eAaImIp3nS
obdobná	obdobný	k2eAgFnSc1d1
jako	jako	k9
u	u	k7c2
anorganických	anorganický	k2eAgFnPc2d1
látek	látka	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc4d1
prvky	prvek	k1gInPc1
omezeny	omezit	k5eAaPmNgInP
strukturou	struktura	k1gFnSc7
celé	celá	k1gFnSc2
molekuly	molekula	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
toho	ten	k3xDgInSc2
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
název	název	k1gInSc1
mechanická	mechanický	k2eAgFnSc1d1
solidarita	solidarita	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Durkheim	Durkheim	k6eAd1
určuje	určovat	k5eAaImIp3nS
typ	typ	k1gInSc1
solidarity	solidarita	k1gFnSc2
převládající	převládající	k2eAgFnSc2d1
v	v	k7c6
dané	daný	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
na	na	k7c6
základě	základ	k1gInSc6
charakteru	charakter	k1gInSc2
jejích	její	k3xOp3gFnPc2
právních	právní	k2eAgFnPc2d1
norem	norma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tradiční	tradiční	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
právo	právo	k1gNnSc4
represivní	represivní	k2eAgFnSc2d1
–	–	k?
tedy	tedy	k8xC
individuum	individuum	k1gNnSc1
je	být	k5eAaImIp3nS
donuceno	donucen	k2eAgNnSc1d1
přijmout	přijmout	k5eAaPmF
normy	norma	k1gFnPc4
<g/>
,	,	kIx,
trest	trest	k1gInSc4
<g/>
,	,	kIx,
kolektivní	kolektivní	k2eAgNnSc4d1
vědomí	vědomí	k1gNnSc4
a	a	k8xC
náboženské	náboženský	k2eAgFnPc4d1
zvyklosti	zvyklost	k1gFnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
solidarita	solidarita	k1gFnSc1
mechanická	mechanický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Solidarita	solidarita	k1gFnSc1
organická	organický	k2eAgFnSc1d1
</s>
<s>
V	v	k7c6
moderní	moderní	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
již	již	k6eAd1
dělba	dělba	k1gFnSc1
práce	práce	k1gFnSc2
rozvinutá	rozvinutý	k2eAgFnSc1d1
<g/>
,	,	kIx,
lidé	člověk	k1gMnPc1
zaujímají	zaujímat	k5eAaImIp3nP
nejen	nejen	k6eAd1
odlišné	odlišný	k2eAgFnPc4d1
pozice	pozice	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
plní	plnit	k5eAaImIp3nP
odlišné	odlišný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
vzájemně	vzájemně	k6eAd1
se	se	k3xPyFc4
doplňující	doplňující	k2eAgFnSc1d1
<g/>
,	,	kIx,
existuje	existovat	k5eAaImIp3nS
solidarita	solidarita	k1gFnSc1
organická	organický	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
postavena	postaven	k2eAgFnSc1d1
na	na	k7c6
dělbě	dělba	k1gFnSc6
práce	práce	k1gFnSc2
a	a	k8xC
kooperaci	kooperace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
vědí	vědět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
pouze	pouze	k6eAd1
ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
aktivitami	aktivita	k1gFnPc7
ostatních	ostatní	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
mají	mít	k5eAaImIp3nP
jejich	jejich	k3xOp3gFnPc4
vlastní	vlastní	k2eAgFnPc4d1
aktivity	aktivita	k1gFnPc4
smysl	smysl	k1gInSc4
a	a	k8xC
právě	právě	k9
tato	tento	k3xDgFnSc1
vzájemná	vzájemný	k2eAgFnSc1d1
závislost	závislost	k1gFnSc1
podmiňuje	podmiňovat	k5eAaImIp3nS
vznik	vznik	k1gInSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
spolupráce	spolupráce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pojem	pojem	k1gInSc1
organická	organický	k2eAgFnSc1d1
solidarita	solidarita	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
analogii	analogie	k1gFnSc6
s	s	k7c7
fungováním	fungování	k1gNnSc7
orgánů	orgán	k1gInPc2
živého	živé	k1gNnSc2
organismu	organismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
má	mít	k5eAaImIp3nS
každý	každý	k3xTgInSc1
orgán	orgán	k1gInSc1
v	v	k7c6
živém	živý	k2eAgNnSc6d1
těle	tělo	k1gNnSc6
svou	svůj	k3xOyFgFnSc4
specifickou	specifický	k2eAgFnSc4d1
fyziognomii	fyziognomie	k1gFnSc4
a	a	k8xC
autonomii	autonomie	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nezbytný	nezbytný	k2eAgInSc1d1,k2eNgInSc1d1
pro	pro	k7c4
fungování	fungování	k1gNnSc4
organismu	organismus	k1gInSc2
jako	jako	k8xS,k8xC
celku	celek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobně	obdobně	k6eAd1
tedy	tedy	k9
má	mít	k5eAaImIp3nS
ve	v	k7c6
společnosti	společnost	k1gFnSc6
s	s	k7c7
organickou	organický	k2eAgFnSc7d1
solidaritou	solidarita	k1gFnSc7
každý	každý	k3xTgMnSc1
jedinec	jedinec	k1gMnSc1
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
a	a	k8xC
sféru	sféra	k1gFnSc4
vlastních	vlastní	k2eAgFnPc2d1
činností	činnost	k1gFnPc2
a	a	k8xC
také	také	k9
on	on	k3xPp3gMnSc1
je	být	k5eAaImIp3nS
nezbytný	nezbytný	k2eAgInSc1d1,k2eNgInSc1d1
pro	pro	k7c4
celek	celek	k1gInSc4
sociálního	sociální	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čím	co	k3yInSc7,k3yQnSc7,k3yRnSc7
je	být	k5eAaImIp3nS
rozlišení	rozlišení	k1gNnSc4
částí	část	k1gFnPc2
organismu	organismus	k1gInSc2
výraznější	výrazný	k2eAgFnSc1d2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
jednota	jednota	k1gFnSc1
organismu	organismus	k1gInSc2
větší	veliký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgMnSc1
jedinec	jedinec	k1gMnSc1
se	se	k3xPyFc4
zároveň	zároveň	k6eAd1
podrobuje	podrobovat	k5eAaImIp3nS
zvykům	zvyk	k1gInPc3
a	a	k8xC
praktikám	praktika	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
má	mít	k5eAaImIp3nS
společné	společný	k2eAgNnSc1d1
s	s	k7c7
celou	celý	k2eAgFnSc7d1
svou	svůj	k3xOyFgFnSc7
korporací	korporace	k1gFnSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
díky	díky	k7c3
skutečnosti	skutečnost	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
sféru	sféra	k1gFnSc4
působnosti	působnost	k1gFnSc2
se	se	k3xPyFc4
své	svůj	k3xOyFgFnSc2
individuality	individualita	k1gFnSc2
nevzdává	vzdávat	k5eNaImIp3nS
zcela	zcela	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Individualita	individualita	k1gFnSc1
celku	celek	k1gInSc2
zde	zde	k6eAd1
roste	růst	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
individualitou	individualita	k1gFnSc7
jeho	jeho	k3xOp3gFnPc2
částí	část	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stěžejní	stěžejní	k2eAgInPc1d1
prvky	prvek	k1gInPc1
společnosti	společnost	k1gFnSc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
právo	právo	k1gNnSc1
<g/>
,	,	kIx,
morálka	morálka	k1gFnSc1
a	a	k8xC
civilizace	civilizace	k1gFnSc1
se	se	k3xPyFc4
racionalizují	racionalizovat	k5eAaBmIp3nP
a	a	k8xC
zobecňují	zobecňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souběžně	souběžně	k6eAd1
tak	tak	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
rozvoji	rozvoj	k1gInSc3
individuálního	individuální	k2eAgInSc2d1
psychického	psychický	k2eAgInSc2d1
života	život	k1gInSc2
jednotlivců	jednotlivec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
těchto	tento	k3xDgInPc2
znaků	znak	k1gInPc2
bychom	by	kYmCp1nP
snadno	snadno	k6eAd1
mohli	moct	k5eAaImAgMnP
vyvozovat	vyvozovat	k5eAaImF
větší	veliký	k2eAgFnSc4d2
míru	míra	k1gFnSc4
štěstí	štěstí	k1gNnSc2
ve	v	k7c6
vyspělejších	vyspělý	k2eAgFnPc6d2
společnostech	společnost	k1gFnPc6
<g/>
,	,	kIx,
přesto	přesto	k8xC
tomu	ten	k3xDgNnSc3
tak	tak	k6eAd1
nebývá	bývat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Durkheim	Durkheim	k1gMnSc1
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
„	„	k?
<g/>
Procesy	proces	k1gInPc7
změn	změna	k1gFnPc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
moderním	moderní	k2eAgInSc6d1
světě	svět	k1gInSc6
natolik	natolik	k6eAd1
prudké	prudký	k2eAgInPc4d1
a	a	k8xC
intenzivní	intenzivní	k2eAgInPc4d1
<g/>
,	,	kIx,
že	že	k8xS
z	z	k7c2
nich	on	k3xPp3gInPc2
vyrůstají	vyrůstat	k5eAaImIp3nP
i	i	k9
závažné	závažný	k2eAgInPc4d1
sociální	sociální	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
ničivý	ničivý	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
tradiční	tradiční	k2eAgInPc4d1
způsoby	způsob	k1gInPc4
života	život	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
morálku	morálka	k1gFnSc4
<g/>
,	,	kIx,
náboženskou	náboženský	k2eAgFnSc4d1
víru	víra	k1gFnSc4
i	i	k9
na	na	k7c4
modely	model	k1gInPc4
každodennosti	každodennost	k1gFnSc2
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nP
přitom	přitom	k6eAd1
nabídly	nabídnout	k5eAaPmAgFnP
srozumitelné	srozumitelný	k2eAgFnPc1d1
nové	nový	k2eAgFnPc1d1
hodnoty	hodnota	k1gFnPc1
<g/>
.	.	kIx.
<g/>
”	”	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nový	nový	k2eAgInSc4d1
systém	systém	k1gInSc4
zahrnující	zahrnující	k2eAgInSc4d1
organickou	organický	k2eAgFnSc4d1
solidaritu	solidarita	k1gFnSc4
je	být	k5eAaImIp3nS
proti	proti	k7c3
mechanické	mechanický	k2eAgFnSc3d1
solidaritě	solidarita	k1gFnSc3
sice	sice	k8xC
pokrokem	pokrok	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
skrývá	skrývat	k5eAaImIp3nS
v	v	k7c6
sobě	sebe	k3xPyFc6
nebezpečí	nebezpečí	k1gNnSc1
tzv.	tzv.	kA
anomie	anomie	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
stav	stav	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
specializace	specializace	k1gFnSc1
jedinců	jedinec	k1gMnPc2
není	být	k5eNaImIp3nS
vyvážena	vyvážet	k5eAaImNgFnS,k5eAaPmNgFnS
zpětnou	zpětný	k2eAgFnSc7d1
integrací	integrace	k1gFnSc7
a	a	k8xC
kdy	kdy	k6eAd1
ve	v	k7c6
společnosti	společnost	k1gFnSc6
přestávají	přestávat	k5eAaImIp3nP
platit	platit	k5eAaImF
pravidla	pravidlo	k1gNnPc4
a	a	k8xC
normy	norma	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tímto	tento	k3xDgInSc7
pojmem	pojem	k1gInSc7
také	také	k9
pracují	pracovat	k5eAaImIp3nP
Robert	Robert	k1gMnSc1
King	King	k1gMnSc1
Merton	Merton	k1gInSc1
<g/>
,	,	kIx,
Friedrich	Friedrich	k1gMnSc1
Hayek	Hayek	k1gMnSc1
<g/>
,	,	kIx,
Talcott	Talcott	k1gMnSc1
Parsons	Parsons	k1gInSc1
a	a	k8xC
Max	Max	k1gMnSc1
Weber	Weber	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robert	Robert	k1gMnSc1
King	King	k1gMnSc1
Merton	Merton	k1gInSc1
i	i	k8xC
Fridrich	Fridrich	k1gMnSc1
Hayek	Hayek	k6eAd1
definovali	definovat	k5eAaBmAgMnP
anomii	anomie	k1gFnSc4
jako	jako	k8xS,k8xC
disharmonii	disharmonie	k1gFnSc4
mezi	mezi	k7c7
obecně	obecně	k6eAd1
uznávaným	uznávaný	k2eAgInSc7d1
hodnotovým	hodnotový	k2eAgInSc7d1
systémem	systém	k1gInSc7
ve	v	k7c6
společnosti	společnost	k1gFnSc6
a	a	k8xC
zákonnými	zákonný	k2eAgInPc7d1
prostředky	prostředek	k1gInPc7
pro	pro	k7c4
jejich	jejich	k3xOp3gNnSc4
dodržování	dodržování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Talcott	Talcott	k2eAgInSc1d1
Parsons	Parsons	k1gInSc1
staví	stavit	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
anomii	anomie	k1gFnSc4
jako	jako	k8xC,k8xS
základ	základ	k1gInSc4
fašizace	fašizace	k1gFnSc2
sociálního	sociální	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
aplikuje	aplikovat	k5eAaBmIp3nS
svou	svůj	k3xOyFgFnSc4
teorii	teorie	k1gFnSc4
na	na	k7c4
nacistické	nacistický	k2eAgNnSc4d1
a	a	k8xC
postnacistické	postnacistický	k2eAgNnSc4d1
Německo	Německo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západní	západní	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc1
pojem	pojem	k1gInSc1
dáván	dávat	k5eAaImNgInS
do	do	k7c2
souvislosti	souvislost	k1gFnSc2
s	s	k7c7
Maxem	Max	k1gMnSc7
Weberem	Weber	k1gMnSc7
a	a	k8xC
jeho	jeho	k3xOp3gInSc7
pojmem	pojem	k1gInSc7
racionalizace	racionalizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
charakteru	charakter	k1gInSc2
právních	právní	k2eAgFnPc2d1
norem	norma	k1gFnPc2
Durkheim	Durkheim	k1gMnSc1
usuzuje	usuzovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
právo	právo	k1gNnSc4
restitutivní	restitutivní	k2eAgFnSc2d1
–	–	k?
tedy	tedy	k9
kolektivní	kolektivní	k2eAgNnSc4d1
vědomí	vědomí	k1gNnSc4
a	a	k8xC
náboženský	náboženský	k2eAgInSc4d1
obsah	obsah	k1gInSc4
jsou	být	k5eAaImIp3nP
oslabovány	oslabován	k2eAgFnPc1d1
<g/>
,	,	kIx,
převládá	převládat	k5eAaImIp3nS
solidarita	solidarita	k1gFnSc1
organická	organický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Mechanická	mechanický	k2eAgFnSc1d1
vs	vs	k?
<g/>
.	.	kIx.
organická	organický	k2eAgFnSc1d1
solidarita	solidarita	k1gFnSc1
</s>
<s>
Prvek	prvek	k1gInSc1
</s>
<s>
Mechanická	mechanický	k2eAgFnSc1d1
solidarita	solidarita	k1gFnSc1
</s>
<s>
Organická	organický	k2eAgFnSc1d1
solidarita	solidarita	k1gFnSc1
</s>
<s>
Strukturální	strukturální	k2eAgInSc1d1
základ	základ	k1gInSc1
</s>
<s>
Založená	založený	k2eAgFnSc1d1
na	na	k7c6
podobnostech	podobnost	k1gFnPc6
(	(	kIx(
<g/>
převládá	převládat	k5eAaImIp3nS
v	v	k7c6
méně	málo	k6eAd2
vyspělých	vyspělý	k2eAgFnPc6d1
společnostech	společnost	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
Segmentární	Segmentární	k2eAgInSc1d1
typ	typ	k1gInSc1
(	(	kIx(
<g/>
nejprve	nejprve	k6eAd1
vymezen	vymezit	k5eAaPmNgInS
klanově	klanově	k6eAd1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
teritoriálně	teritoriálně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Jen	jen	k9
malá	malý	k2eAgFnSc1d1
míra	míra	k1gFnSc1
nezávislosti	nezávislost	k1gFnSc2
(	(	kIx(
<g/>
společenská	společenský	k2eAgNnPc4d1
pouta	pouto	k1gNnPc4
relativně	relativně	k6eAd1
slabá	slabý	k2eAgNnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Relativně	relativně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
míra	míra	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
Relativně	relativně	k6eAd1
nízká	nízký	k2eAgFnSc1d1
materiální	materiální	k2eAgFnSc1d1
a	a	k8xC
morální	morální	k2eAgFnSc1d1
hustota	hustota	k1gFnSc1
</s>
<s>
Založená	založený	k2eAgFnSc1d1
na	na	k7c6
dělbě	dělba	k1gFnSc6
práce	práce	k1gFnSc2
(	(	kIx(
<g/>
převládá	převládat	k5eAaImIp3nS
ve	v	k7c6
vyspělejších	vyspělý	k2eAgFnPc6d2
společnostech	společnost	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
Organizovaný	organizovaný	k2eAgInSc1d1
typ	typ	k1gInSc1
(	(	kIx(
<g/>
propojení	propojení	k1gNnSc1
trhů	trh	k1gInPc2
a	a	k8xC
rozrůstání	rozrůstání	k1gNnSc4
měst	město	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
míra	míra	k1gFnSc1
nezávislosti	nezávislost	k1gFnSc2
(	(	kIx(
<g/>
společenská	společenský	k2eAgNnPc4d1
pouta	pouto	k1gNnPc4
relativně	relativně	k6eAd1
silná	silný	k2eAgNnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Relativně	relativně	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
míra	míra	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
Relativně	relativně	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
míra	míra	k1gFnSc1
materiální	materiální	k2eAgFnSc2d1
i	i	k8xC
morální	morální	k2eAgFnSc2d1
hustoty	hustota	k1gFnSc2
</s>
<s>
Druhy	druh	k1gInPc1
norem	norma	k1gFnPc2
</s>
<s>
Pravidla	pravidlo	k1gNnPc4
podmíněná	podmíněný	k2eAgFnSc1d1
represivními	represivní	k2eAgFnPc7d1
sankcemi	sankce	k1gFnPc7
</s>
<s>
Převládá	převládat	k5eAaImIp3nS
trestní	trestní	k2eAgNnSc1d1
právo	právo	k1gNnSc1
</s>
<s>
Pravidla	pravidlo	k1gNnPc4
podmíněná	podmíněný	k2eAgFnSc1d1
restitutivními	restitutivní	k2eAgFnPc7d1
sankcemi	sankce	k1gFnPc7
</s>
<s>
Převládá	převládat	k5eAaImIp3nS
občanské	občanský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
</s>
<s>
Formální	formální	k2eAgInPc1d1
rysy	rys	k1gInPc1
kolektivního	kolektivní	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
</s>
<s>
Autorita	autorita	k1gFnSc1
kolektivu	kolektiv	k1gInSc2
je	být	k5eAaImIp3nS
absolutní	absolutní	k2eAgFnSc1d1
</s>
<s>
kolektivní	kolektivní	k2eAgNnSc4d1
vědomí	vědomí	k1gNnSc4
jako	jako	k8xS,k8xC
integrační	integrační	k2eAgMnSc1d1
činitel	činitel	k1gMnSc1
</s>
<s>
Více	hodně	k6eAd2
prostoru	prostor	k1gInSc2
pro	pro	k7c4
individuální	individuální	k2eAgFnSc4d1
reflexi	reflexe	k1gFnSc4
a	a	k8xC
iniciativu	iniciativa	k1gFnSc4
</s>
<s>
Obsah	obsah	k1gInSc1
kolektivního	kolektivní	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
</s>
<s>
Společnost	společnost	k1gFnSc1
a	a	k8xC
zájmy	zájem	k1gInPc1
společnosti	společnost	k1gFnSc2
jako	jako	k8xS,k8xC
celku	celek	k1gInSc2
mají	mít	k5eAaImIp3nP
nadřazenou	nadřazený	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
</s>
<s>
Transcendentální	transcendentální	k2eAgMnSc1d1
–	–	k?
nadřazení	nadřazení	k1gNnSc2
lidským	lidský	k2eAgInPc3d1
zájmům	zájem	k1gInPc3
<g/>
,	,	kIx,
neoddiskutovalné	oddiskutovalný	k2eNgFnSc3d1
</s>
<s>
Všudypřítomné	všudypřítomný	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
</s>
<s>
Stále	stále	k6eAd1
více	hodně	k6eAd2
sekulární	sekulární	k2eAgFnSc1d1
</s>
<s>
Orientováno	orientován	k2eAgNnSc1d1
na	na	k7c4
člověka	člověk	k1gMnSc4
–	–	k?
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
lidským	lidský	k2eAgMnPc3d1
zájmům	zájem	k1gInPc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
otevřen	otevřít	k5eAaPmNgInS
diskuzi	diskuze	k1gFnSc3
</s>
<s>
Nadřazenou	nadřazený	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
má	mít	k5eAaImIp3nS
individuální	individuální	k2eAgFnSc4d1
důstojnost	důstojnost	k1gFnSc4
–	–	k?
rovnost	rovnost	k1gFnSc1
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
příležitostem	příležitost	k1gFnPc3
<g/>
,	,	kIx,
pracovní	pracovní	k2eAgFnSc1d1
etika	etika	k1gFnSc1
a	a	k8xC
sociální	sociální	k2eAgFnSc1d1
spravedlnost	spravedlnost	k1gFnSc1
</s>
<s>
Dvojí	dvojí	k4xRgFnSc1
funkce	funkce	k1gFnSc1
solidarity	solidarita	k1gFnSc2
organické	organický	k2eAgFnSc2d1
a	a	k8xC
solidarity	solidarita	k1gFnSc2
mechanické	mechanický	k2eAgFnSc2d1
v	v	k7c6
Durkheimově	Durkheimův	k2eAgInSc6d1
sociologickém	sociologický	k2eAgInSc6d1
systému	systém	k1gInSc6
</s>
<s>
Nástroj	nástroj	k1gInSc1
analýzy	analýza	k1gFnSc2
struktury	struktura	k1gFnSc2
kapitalistické	kapitalistický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
koexistuje	koexistovat	k5eAaImIp3nS
solidarita	solidarita	k1gFnSc1
organická	organický	k2eAgFnSc1d1
i	i	k8xC
mechanická	mechanický	k2eAgFnSc1d1
s	s	k7c7
převahou	převaha	k1gFnSc7
organické	organický	k2eAgFnSc2d1
solidarity	solidarita	k1gFnSc2
(	(	kIx(
<g/>
nikoliv	nikoliv	k9
převahou	převaha	k1gFnSc7
absolutní	absolutní	k2eAgFnSc2d1
<g/>
)	)	kIx)
</s>
<s>
Typologie	typologie	k1gFnSc1
společenského	společenský	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
–	–	k?
na	na	k7c6
počátku	počátek	k1gInSc6
společenského	společenský	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
převládá	převládat	k5eAaImIp3nS
mechanická	mechanický	k2eAgFnSc1d1
solidarita	solidarita	k1gFnSc1
a	a	k8xC
postupem	postupem	k7c2
času	čas	k1gInSc2
směrem	směr	k1gInSc7
k	k	k7c3
současnosti	současnost	k1gFnSc3
je	být	k5eAaImIp3nS
nahrazována	nahrazovat	k5eAaImNgFnS
organickou	organický	k2eAgFnSc7d1
solidaritou	solidarita	k1gFnSc7
</s>
<s>
Bipolární	bipolární	k2eAgInPc1d1
modely	model	k1gInPc1
společnosti	společnost	k1gFnSc2
a	a	k8xC
jejího	její	k3xOp3gInSc2
vývoje	vývoj	k1gInSc2
během	během	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
postupně	postupně	k6eAd1
ztrácely	ztrácet	k5eAaImAgInP
na	na	k7c6
aktuálnosti	aktuálnost	k1gFnSc6
v	v	k7c6
kontextu	kontext	k1gInSc6
se	s	k7c7
sociologií	sociologie	k1gFnSc7
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
vydala	vydat	k5eAaPmAgFnS
směrem	směr	k1gInSc7
nehistorické	historický	k2eNgFnPc4d1
vědy	věda	k1gFnPc4
zaměřené	zaměřený	k2eAgFnPc4d1
téměř	téměř	k6eAd1
výhradně	výhradně	k6eAd1
na	na	k7c6
empirickém	empirický	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
dílčích	dílčí	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
Johannes	Johannes	k1gMnSc1
Lenhard	Lenhard	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
studii	studie	k1gFnSc6
A	a	k9
Critical	Critical	k1gMnSc1
Account	Account	k1gMnSc1
of	of	k?
Durkheim	Durkheim	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Concept	Concept	k1gInSc1
of	of	k?
Organic	Organice	k1gFnPc2
Solidarity	solidarita	k1gFnSc2
kritizuje	kritizovat	k5eAaImIp3nS
zejména	zejména	k9
Durkheimův	Durkheimův	k2eAgInSc1d1
optimismus	optimismus	k1gInSc1
vůči	vůči	k7c3
organické	organický	k2eAgFnSc3d1
solidaritě	solidarita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poznamenává	poznamenávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
dělba	dělba	k1gFnSc1
práce	práce	k1gFnSc2
nemusí	muset	k5eNaImIp3nS
mít	mít	k5eAaImF
nutně	nutně	k6eAd1
pouze	pouze	k6eAd1
pozitivní	pozitivní	k2eAgInPc4d1
následky	následek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argumentuje	argumentovat	k5eAaImIp3nS
následovně	následovně	k6eAd1
<g/>
:	:	kIx,
v	v	k7c6
primitivních	primitivní	k2eAgFnPc6d1
společnostech	společnost	k1gFnPc6
byly	být	k5eAaImAgFnP
rodiny	rodina	k1gFnPc1
závislé	závislý	k2eAgFnPc1d1
pouze	pouze	k6eAd1
samy	sám	k3xTgInPc1
na	na	k7c6
sobě	sebe	k3xPyFc6
–	–	k?
nezbytné	nezbytný	k2eAgFnPc4d1,k2eNgFnPc4d1
věci	věc	k1gFnPc4
jako	jako	k8xS,k8xC
potravu	potrava	k1gFnSc4
a	a	k8xC
oděvy	oděv	k1gInPc1
si	se	k3xPyFc3
obstarávaly	obstarávat	k5eAaImAgInP
samy	sám	k3xTgInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
společnostech	společnost	k1gFnPc6
s	s	k7c7
rozvinutou	rozvinutý	k2eAgFnSc7d1
dělbou	dělba	k1gFnSc7
práce	práce	k1gFnSc2
se	se	k3xPyFc4
jedinec	jedinec	k1gMnSc1
bez	bez	k7c2
produktů	produkt	k1gInPc2
práce	práce	k1gFnSc2
druhých	druhý	k4xOgInPc2
neobejde	obejde	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
tedy	tedy	k9
(	(	kIx(
<g/>
duševně	duševně	k6eAd1
<g/>
)	)	kIx)
nezávislý	závislý	k2eNgMnSc1d1
<g/>
,	,	kIx,
fyzicky	fyzicky	k6eAd1
je	být	k5eAaImIp3nS
však	však	k9
zapleten	zaplést	k5eAaPmNgInS
do	do	k7c2
sítě	síť	k1gFnSc2
smluvních	smluvní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
podmiňují	podmiňovat	k5eAaImIp3nP
jeho	jeho	k3xOp3gFnSc4
nutnou	nutný	k2eAgFnSc4d1
interakci	interakce	k1gFnSc4
s	s	k7c7
okolím	okolí	k1gNnSc7
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
nemusí	muset	k5eNaImIp3nP
být	být	k5eAaImF
vždy	vždy	k6eAd1
chtěná	chtěný	k2eAgFnSc1d1
a	a	k8xC
harmonická	harmonický	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
DURKHEIM	DURKHEIM	kA
<g/>
,	,	kIx,
Émile	Émile	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společenská	společenský	k2eAgFnSc1d1
dělba	dělba	k1gFnSc1
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
(	(	kIx(
<g/>
CDK	CDK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8073250411	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
375	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GIDDENS	GIDDENS	kA
<g/>
,	,	kIx,
Anthony	Anthona	k1gFnSc2
<g/>
;	;	kIx,
SUTTON	SUTTON	kA
<g/>
,	,	kIx,
Phillip	Phillip	k1gMnSc1
W.	W.	kA
Sociologie	sociologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
<g/>
,	,	kIx,
aktualizované	aktualizovaný	k2eAgFnPc4d1
a	a	k8xC
rozšířené	rozšířený	k2eAgFnPc4d1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788025708071	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1049	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GIDDENS	GIDDENS	kA
<g/>
,	,	kIx,
Anthony	Anthona	k1gFnSc2
<g/>
;	;	kIx,
SUTTON	SUTTON	kA
<g/>
,	,	kIx,
Philip	Philip	k1gMnSc1
W.	W.	kA
Sociologie	sociologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
<g/>
,	,	kIx,
aktualizované	aktualizovaný	k2eAgFnPc4d1
a	a	k8xC
rozšířené	rozšířený	k2eAgFnPc4d1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788025708071	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
358	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LENHARD	LENHARD	kA
<g/>
,	,	kIx,
Johannes	Johannes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Critical	Critical	k1gMnSc1
Account	Account	k1gMnSc1
of	of	k?
Durkheim	Durkheim	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Concept	Concept	k1gInSc1
of	of	k?
Organic	Organice	k1gFnPc2
Solidarity	solidarita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sociology	sociolog	k1gMnPc4
-	-	kIx~
Classics	Classics	k1gInSc1
and	and	k?
Theoretical	Theoretical	k1gFnSc2
Directions	Directionsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GRIN	GRIN	kA
Verlag	Verlaga	k1gFnPc2
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
30.11	30.11	k4
<g/>
.2016	.2016	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Durkheim	Durkheim	k6eAd1
Émile	Émile	k1gFnSc1
<g/>
,	,	kIx,
Společenská	společenský	k2eAgFnSc1d1
dělba	dělba	k1gFnSc1
práce	práce	k1gFnSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Keller	Keller	k1gMnSc1
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
Úvod	úvod	k1gInSc1
do	do	k7c2
sociologie	sociologie	k1gFnSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slon	slon	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kraus	Kraus	k1gMnSc1
Jiří	Jiří	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
akademický	akademický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Petrusek	Petrusek	k1gMnSc1
Miloslav	Miloslav	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Velký	velký	k2eAgInSc1d1
sociologický	sociologický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Giddens	Giddens	k6eAd1
Anthony	Anthona	k1gFnPc1
<g/>
,	,	kIx,
Sutton	Sutton	k1gInSc1
Philip	Philip	k1gInSc1
W.	W.	kA
<g/>
,	,	kIx,
Sociologie	sociologie	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Lenhard	Lenhard	k1gMnSc1
Johannes	Johannes	k1gMnSc1
<g/>
,	,	kIx,
A	a	k8xC
Critical	Critical	k1gMnSc1
Account	Account	k1gMnSc1
of	of	k?
Durkheim	Durkheim	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Concept	Concept	k1gInSc1
of	of	k?
Organic	Organice	k1gFnPc2
Solidarity	solidarita	k1gFnSc2
(	(	kIx(
<g/>
Mnichov	Mnichov	k1gInSc1
<g/>
:	:	kIx,
GRIN	GRIN	kA
Verlag	Verlag	k1gMnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
