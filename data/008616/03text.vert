<p>
<s>
A-dur	Aura	k1gFnPc2	A-dura
je	být	k5eAaImIp3nS	být
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
stupnice	stupnice	k1gFnSc1	stupnice
v	v	k7c6	v
kvintovém	kvintový	k2eAgInSc6d1	kvintový
kruhu	kruh	k1gInSc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
předznamenání	předznamenání	k1gNnSc1	předznamenání
3	[number]	k4	3
křížky	křížek	k1gInPc7	křížek
–	–	k?	–
fis	fis	k1gNnSc1	fis
<g/>
,	,	kIx,	,
cis	cis	k1gNnSc1	cis
a	a	k8xC	a
gis	gis	k1gNnSc1	gis
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
šestý	šestý	k4xOgInSc4	šestý
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
sedmý	sedmý	k4xOgInSc4	sedmý
stupeň	stupeň	k1gInSc4	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klavírní	klavírní	k2eAgInSc1d1	klavírní
přednes	přednes	k1gInSc1	přednes
==	==	k?	==
</s>
</p>
<p>
<s>
Hrajeme	hrát	k5eAaImIp1nP	hrát
ji	on	k3xPp3gFnSc4	on
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
,	,	kIx,	,
když	když	k8xS	když
jdeme	jít	k5eAaImIp1nP	jít
nahoru	nahoru	k6eAd1	nahoru
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
na	na	k7c4	na
fis	fis	k1gNnSc4	fis
prostředníček	prostředníček	k1gInSc4	prostředníček
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jdeme	jít	k5eAaImIp1nP	jít
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
podložit	podložit	k5eAaPmF	podložit
pod	pod	k7c7	pod
prostředníčkem	prostředníček	k1gMnSc7	prostředníček
palec	palec	k1gInSc4	palec
na	na	k7c4	na
e.	e.	k?	e.
</s>
</p>
<p>
<s>
==	==	k?	==
Oblíbenost	oblíbenost	k1gFnSc4	oblíbenost
u	u	k7c2	u
autorů	autor	k1gMnPc2	autor
==	==	k?	==
</s>
</p>
