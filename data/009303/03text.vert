<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
bílým	bílý	k2eAgInSc7d1	bílý
<g/>
,	,	kIx,	,
zeleným	zelený	k2eAgInSc7d1	zelený
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
přijata	přijat	k2eAgFnSc1d1	přijata
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
<g/>
Bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
úrodnosti	úrodnost	k1gFnSc2	úrodnost
bulharské	bulharský	k2eAgFnSc2d1	bulharská
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
pak	pak	k6eAd1	pak
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
krev	krev	k1gFnSc4	krev
prolitou	prolitý	k2eAgFnSc4d1	prolitá
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
681	[number]	k4	681
existovaly	existovat	k5eAaImAgInP	existovat
na	na	k7c6	na
Balkánu	Balkán	k1gInSc6	Balkán
Bulharské	bulharský	k2eAgFnSc2d1	bulharská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
681	[number]	k4	681
<g/>
–	–	k?	–
<g/>
1018	[number]	k4	1018
První	první	k4xOgFnSc1	první
bulharská	bulharský	k2eAgFnSc1d1	bulharská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
západ	západ	k1gInSc1	západ
této	tento	k3xDgFnSc2	tento
říše	říš	k1gFnSc2	říš
dostal	dostat	k5eAaPmAgMnS	dostat
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Byzance	Byzanc	k1gFnSc2	Byzanc
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1185	[number]	k4	1185
byla	být	k5eAaImAgFnS	být
obnovena	obnovit	k5eAaPmNgFnS	obnovit
Druhá	druhý	k4xOgFnSc1	druhý
bulharská	bulharský	k2eAgFnSc1d1	bulharská
říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1396	[number]	k4	1396
se	se	k3xPyFc4	se
bulharské	bulharský	k2eAgNnSc1d1	bulharské
území	území	k1gNnSc1	území
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
Rusko-turecké	ruskourecký	k2eAgFnSc6d1	rusko-turecká
válce	válka	k1gFnSc6	válka
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
samostatné	samostatný	k2eAgNnSc4d1	samostatné
Bulharské	bulharský	k2eAgNnSc4d1	bulharské
knížectví	knížectví	k1gNnSc4	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
(	(	kIx(	(
<g/>
shodná	shodný	k2eAgFnSc1d1	shodná
se	s	k7c7	s
současnou	současný	k2eAgFnSc7d1	současná
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
)	)	kIx)	)
vycházela	vycházet	k5eAaImAgNnP	vycházet
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
vděk	vděk	k1gInSc1	vděk
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
od	od	k7c2	od
turecké	turecký	k2eAgFnSc2d1	turecká
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
)	)	kIx)	)
z	z	k7c2	z
ruské	ruský	k2eAgFnSc2d1	ruská
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
prostřední	prostřednět	k5eAaImIp3nS	prostřednět
modrý	modrý	k2eAgInSc1d1	modrý
pruh	pruh	k1gInSc1	pruh
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
nahrazen	nahradit	k5eAaPmNgInS	nahradit
pruhem	pruh	k1gInSc7	pruh
zeleným	zelený	k2eAgInSc7d1	zelený
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	let	k1gInPc6	let
1878	[number]	k4	1878
<g/>
–	–	k?	–
<g/>
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
však	však	k9	však
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
existovalo	existovat	k5eAaImAgNnS	existovat
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
autonomní	autonomní	k2eAgNnSc4d1	autonomní
území	území	k1gNnSc4	území
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
Východní	východní	k2eAgFnSc2d1	východní
Rumélie	Rumélie	k1gFnSc2	Rumélie
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
de	de	k?	de
facto	facto	k1gNnSc4	facto
druhý	druhý	k4xOgInSc1	druhý
bulharský	bulharský	k2eAgInSc1d1	bulharský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkou	vlajka	k1gFnSc7	vlajka
byla	být	k5eAaImAgFnS	být
červeno-bílo-modrá	červenoíloodrý	k2eAgFnSc1d1	červeno-bílo-modrá
trikolóra	trikolóra	k1gFnSc1	trikolóra
s	s	k7c7	s
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bulharské	bulharský	k2eAgNnSc1d1	bulharské
knížectví	knížectví	k1gNnSc1	knížectví
bylo	být	k5eAaImAgNnS	být
de	de	k?	de
facto	facto	k1gNnSc1	facto
ruským	ruský	k2eAgInSc7d1	ruský
protektorátem	protektorát	k1gInSc7	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
Protiruské	protiruský	k2eAgFnPc1d1	protiruská
tendence	tendence	k1gFnPc1	tendence
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
ruského	ruský	k2eAgInSc2d1	ruský
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
plně	plně	k6eAd1	plně
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
Bulharské	bulharský	k2eAgNnSc1d1	bulharské
carství	carství	k1gNnSc1	carství
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
bulharskou	bulharský	k2eAgFnSc4d1	bulharská
bílo-zeleno-červenou	bíloeleno-červený	k2eAgFnSc4d1	bílo-zeleno-červený
trikolóru	trikolóra	k1gFnSc4	trikolóra
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nemělo	mít	k5eNaImAgNnS	mít
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
levicovém	levicový	k2eAgInSc6d1	levicový
převratu	převrat	k1gInSc6	převrat
změněn	změnit	k5eAaPmNgInS	změnit
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
na	na	k7c6	na
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
car	car	k1gMnSc1	car
Simeon	Simeon	k1gMnSc1	Simeon
II	II	kA	II
<g/>
.	.	kIx.	.
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
změnami	změna	k1gFnPc7	změna
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1948	[number]	k4	1948
užívána	užíván	k2eAgFnSc1d1	užívána
pozměněná	pozměněný	k2eAgFnSc1d1	pozměněná
vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
horního	horní	k2eAgInSc2d1	horní
rohu	roh	k1gInSc2	roh
přidán	přidán	k2eAgInSc4d1	přidán
nový	nový	k2eAgInSc4d1	nový
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
zlatým	zlatý	k2eAgInSc7d1	zlatý
<g/>
,	,	kIx,	,
nekorunovaným	korunovaný	k2eNgInSc7d1	nekorunovaný
lvem	lev	k1gInSc7	lev
na	na	k7c6	na
modrém	modrý	k2eAgInSc6d1	modrý
štítu	štít	k1gInSc6	štít
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
obklopoval	obklopovat	k5eAaImAgMnS	obklopovat
věnec	věnec	k1gInSc4	věnec
ze	z	k7c2	z
pšeničných	pšeničný	k2eAgInPc2d1	pšeničný
klasů	klas	k1gInPc2	klas
<g/>
.	.	kIx.	.
</s>
<s>
Klasy	klasa	k1gFnPc1	klasa
byly	být	k5eAaImAgFnP	být
převázány	převázat	k5eAaPmNgFnP	převázat
červenou	červený	k2eAgFnSc7d1	červená
stuhou	stuha	k1gFnSc7	stuha
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
9	[number]	k4	9
IX	IX	kA	IX
1944	[number]	k4	1944
(	(	kIx(	(
<g/>
vykládáno	vykládán	k2eAgNnSc1d1	vykládáno
jako	jako	k9	jako
den	den	k1gInSc4	den
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c7	nad
fašismem	fašismus	k1gInSc7	fašismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
štítem	štít	k1gInSc7	štít
byla	být	k5eAaImAgFnS	být
rudá	rudý	k2eAgFnSc1d1	rudá
<g/>
,	,	kIx,	,
zlatě	zlatě	k6eAd1	zlatě
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
<g/>
,	,	kIx,	,
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
<g/>
Kartušovitý	kartušovitý	k2eAgInSc1d1	kartušovitý
tvar	tvar	k1gInSc1	tvar
štítu	štít	k1gInSc2	štít
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1948	[number]	k4	1948
změněn	změnit	k5eAaPmNgMnS	změnit
na	na	k7c4	na
oválný	oválný	k2eAgInSc4d1	oválný
a	a	k8xC	a
přibylo	přibýt	k5eAaPmAgNnS	přibýt
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
ozubené	ozubený	k2eAgNnSc1d1	ozubené
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
hvězda	hvězda	k1gFnSc1	hvězda
<g />
.	.	kIx.	.
</s>
<s>
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
zlatý	zlatý	k2eAgInSc4d1	zlatý
lem	lem	k1gInSc4	lem
a	a	k8xC	a
konce	konec	k1gInSc2	konec
stuhy	stuha	k1gFnPc1	stuha
byly	být	k5eAaImAgFnP	být
upraveny	upravit	k5eAaPmNgFnP	upravit
do	do	k7c2	do
bulharské	bulharský	k2eAgFnSc2d1	bulharská
trikolóry	trikolóra	k1gFnSc2	trikolóra
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
drobným	drobný	k2eAgFnPc3d1	drobná
barevným	barevný	k2eAgFnPc3d1	barevná
změnám	změna	k1gFnPc3	změna
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.21	.21	k4	.21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1971	[number]	k4	1971
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k9	již
významné	významný	k2eAgFnSc3d1	významná
změně	změna	k1gFnSc3	změna
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
lva	lev	k1gInSc2	lev
<g/>
,	,	kIx,	,
klasů	klas	k1gInPc2	klas
a	a	k8xC	a
ozubeného	ozubený	k2eAgNnSc2d1	ozubené
kola	kolo	k1gNnSc2	kolo
byla	být	k5eAaImAgNnP	být
změněna	změnit	k5eAaPmNgNnP	změnit
na	na	k7c4	na
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
<g/>
.	.	kIx.	.
</s>
<s>
Stuha	stuha	k1gFnSc1	stuha
dostala	dostat	k5eAaPmAgFnS	dostat
jiný	jiný	k2eAgInSc4d1	jiný
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
místo	místo	k7c2	místo
jednoho	jeden	k4xCgNnSc2	jeden
na	na	k7c4	na
ní	on	k3xPp3gFnSc6	on
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
letopočty	letopočet	k1gInPc1	letopočet
<g/>
:	:	kIx,	:
rok	rok	k1gInSc1	rok
681	[number]	k4	681
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgInSc1d1	připomínající
rok	rok	k1gInSc1	rok
založení	založení	k1gNnSc2	založení
prvního	první	k4xOgInSc2	první
bulharského	bulharský	k2eAgInSc2d1	bulharský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
rok	rok	k1gInSc4	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vykládán	vykládat	k5eAaImNgInS	vykládat
jako	jako	k8xC	jako
rok	rok	k1gInSc1	rok
osvobození	osvobození	k1gNnSc2	osvobození
a	a	k8xC	a
vítězství	vítězství	k1gNnSc2	vítězství
socialistické	socialistický	k2eAgFnSc2d1	socialistická
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
také	také	k9	také
dostal	dostat	k5eAaPmAgInS	dostat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
kresbu	kresba	k1gFnSc4	kresba
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
byl	být	k5eAaImAgInS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
socialismu	socialismus	k1gInSc2	socialismus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
demokratizaci	demokratizace	k1gFnSc6	demokratizace
byl	být	k5eAaImAgInS	být
změněn	změnit	k5eAaPmNgInS	změnit
název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
na	na	k7c6	na
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
opětovně	opětovně	k6eAd1	opětovně
přijata	přijat	k2eAgFnSc1d1	přijata
vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
bílo-zeleno-červená	bíloeleno-červený	k2eAgFnSc1d1	bílo-zeleno-červený
trikolóra	trikolóra	k1gFnSc1	trikolóra
bez	bez	k7c2	bez
znaku	znak	k1gInSc2	znak
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
vlajky	vlajka	k1gFnPc1	vlajka
byl	být	k5eAaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
na	na	k7c6	na
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
I	i	k8xC	i
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
vlajky	vlajka	k1gFnSc2	vlajka
bez	bez	k7c2	bez
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
se	se	k3xPyFc4	se
mimořádně	mimořádně	k6eAd1	mimořádně
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
znakem	znak	k1gInSc7	znak
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
však	však	k9	však
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
výjimku	výjimka	k1gFnSc4	výjimka
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgInPc4d1	vycházející
zřejmě	zřejmě	k6eAd1	zřejmě
ze	z	k7c2	z
zvyklostí	zvyklost	k1gFnPc2	zvyklost
minulých	minulý	k2eAgNnPc2d1	Minulé
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
</s>
</p>
<p>
<s>
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Bulharska	Bulharsko	k1gNnSc2	Bulharsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bulharská	bulharský	k2eAgFnSc1d1	bulharská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
