<s>
U	u	k7c2
oběšeného	oběšený	k2eAgNnSc2d1
je	být	k5eAaImIp3nS
kopec	kopec	k1gInSc1
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
737,4	737,4	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Železných	železný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
pohoří	pohořet	k5eAaPmIp3nS
nazvané	nazvaný	k2eAgNnSc1d1
Sečská	sečský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
okraji	okraj	k1gInSc6
geomorfologického	geomorfologický	k2eAgInSc2d1
okrsku	okrsek	k1gInSc2
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
v	v	k7c6
regionálním	regionální	k2eAgNnSc6d1
členění	členění	k1gNnSc6
georeliéfu	georeliéf	k1gInSc2
(	(	kIx(
<g/>
tvaru	tvar	k1gInSc2
zemského	zemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kopec	kopec	k1gInSc1
leží	ležet	k5eAaImIp3nS
severozápadně	severozápadně	k6eAd1
nad	nad	k7c7
obcí	obec	k1gFnSc7
Svratouch	Svratouch	k1gInPc1
a	a	k8xC
je	být	k5eAaImIp3nS
pomístně	pomístně	k6eAd1
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
(	(	kIx(
<g/>
anoikonymum	anoikonymum	k1gInSc1
<g/>
)	)	kIx)
Na	na	k7c6
Panenkách	panenka	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>