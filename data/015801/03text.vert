<s>
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
</s>
<s>
U	u	k7c2
oběšeného	oběšený	k2eAgMnSc2d1
Kuželovitý	kuželovitý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
U	u	k7c2
oběšeného	oběšený	k2eAgNnSc2d1
leží	ležet	k5eAaImIp3nS
na	na	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
obce	obec	k1gFnSc2
Svratouch	Svratouch	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
hlavním	hlavní	k2eAgNnSc6d1
evropském	evropský	k2eAgNnSc6d1
rozvodí	rozvodí	k1gNnSc6
Labe	Labe	k1gNnSc4
–	–	k?
Dunaj	Dunaj	k1gInSc1
(	(	kIx(
<g/>
pohled	pohled	k1gInSc1
od	od	k7c2
severozápadu	severozápad	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
737,4	737,4	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Izolace	izolace	k1gFnSc1
</s>
<s>
2,9	2,9	k4
km	km	kA
→	→	k?
Karlštejn	Karlštejn	k1gInSc1
Seznamy	seznam	k1gInPc1
</s>
<s>
Hory	hora	k1gFnPc1
Železných	železný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
#	#	kIx~
<g/>
1	#num#	k4
Poznámka	poznámka	k1gFnSc1
</s>
<s>
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Železných	železný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Železné	železný	k2eAgFnPc1d1
hory	hora	k1gFnPc1
/	/	kIx~
Sečská	sečský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
/	/	kIx~
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
10	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
svor	svor	k1gInSc1
<g/>
,	,	kIx,
amfibolit	amfibolit	k1gInSc1
<g/>
,	,	kIx,
rula	rula	k1gFnSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Chrudimka	Chrudimka	k1gFnSc1
<g/>
,	,	kIx,
Krounka	Krounka	k1gFnSc1
<g/>
,	,	kIx,
Svratka	Svratka	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
oběšeného	oběšený	k2eAgNnSc2d1
je	být	k5eAaImIp3nS
kopec	kopec	k1gInSc1
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
737,4	737,4	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Železných	železný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
pohoří	pohořet	k5eAaPmIp3nS
nazvané	nazvaný	k2eAgNnSc1d1
Sečská	sečský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
okraji	okraj	k1gInSc6
geomorfologického	geomorfologický	k2eAgInSc2d1
okrsku	okrsek	k1gInSc2
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
v	v	k7c6
regionálním	regionální	k2eAgNnSc6d1
členění	členění	k1gNnSc6
georeliéfu	georeliéf	k1gInSc2
(	(	kIx(
<g/>
tvaru	tvar	k1gInSc2
zemského	zemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kopec	kopec	k1gInSc1
leží	ležet	k5eAaImIp3nS
severozápadně	severozápadně	k6eAd1
nad	nad	k7c7
obcí	obec	k1gFnSc7
Svratouch	Svratouch	k1gInPc1
a	a	k8xC
je	být	k5eAaImIp3nS
pomístně	pomístně	k6eAd1
jmenován	jmenovat	k5eAaBmNgInS,k5eAaImNgInS
(	(	kIx(
<g/>
anoikonymum	anoikonymum	k1gInSc1
<g/>
)	)	kIx)
Na	na	k7c6
Panenkách	panenka	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
kopec	kopec	k1gInSc1
<g/>
)	)	kIx)
kuželovitého	kuželovitý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
tvoří	tvořit	k5eAaImIp3nS
hornina	hornina	k1gFnSc1
dvojslídný	dvojslídný	k2eAgInSc1d1
svor	svor	k1gInSc4
svrateckého	svratecký	k2eAgNnSc2d1
krystalinika	krystalinikum	k1gNnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
masiv	masiv	k1gInSc1
–	–	k?
krystalinikum	krystalinikum	k1gNnSc1
a	a	k8xC
prevariské	prevariský	k2eAgNnSc1d1
paleozoikum	paleozoikum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Vrcholovou	vrcholový	k2eAgFnSc7d1
částí	část	k1gFnSc7
prochází	procházet	k5eAaImIp3nS
hlavní	hlavní	k2eAgNnSc4d1
evropské	evropský	k2eAgNnSc4d1
rozvodí	rozvodí	k1gNnSc4
Labe	Labe	k1gNnSc2
–	–	k?
Dunaj	Dunaj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rozsahu	rozsah	k1gInSc6
celé	celý	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
je	být	k5eAaImIp3nS
kopec	kopec	k1gInSc1
zvláště	zvláště	k6eAd1
chráněným	chráněný	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
,	,	kIx,
součástí	součást	k1gFnSc7
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
Českomoravské	českomoravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
trigonometrická	trigonometrický	k2eAgFnSc1d1
síť	síť	k1gFnSc1
uvádí	uvádět	k5eAaImIp3nS
zaměřený	zaměřený	k2eAgInSc4d1
trigonometrický	trigonometrický	k2eAgInSc4d1
bod	bod	k1gInSc4
(	(	kIx(
<g/>
č.	č.	k?
41	#num#	k4
triangulačního	triangulační	k2eAgInSc2d1
listu	list	k1gInSc2
2420	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
názvem	název	k1gInSc7
U	u	k7c2
oběšeného	oběšený	k2eAgNnSc2d1
a	a	k8xC
nivelací	nivelace	k1gFnPc2
určenou	určený	k2eAgFnSc4d1
nadmořskou	nadmořský	k2eAgFnSc4d1
výšku	výška	k1gFnSc4
737,38	737,38	k4
m	m	kA
<g/>
,	,	kIx,
žulový	žulový	k2eAgInSc1d1
terénní	terénní	k2eAgInSc1d1
patník	patník	k1gInSc1
(	(	kIx(
<g/>
geodetický	geodetický	k2eAgInSc1d1
bod	bod	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
rozsáhlé	rozsáhlý	k2eAgFnSc6d1
louce	louka	k1gFnSc6
nad	nad	k7c7
pásmem	pásmo	k1gNnSc7
smrčin	smrčina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vrchol	vrchol	k1gInSc1
s	s	k7c7
výškovým	výškový	k2eAgInSc7d1
bodem	bod	k1gInSc7
leží	ležet	k5eAaImIp3nS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
administrativně	administrativně	k6eAd1
správního	správní	k2eAgInSc2d1
na	na	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
obce	obec	k1gFnSc2
Svratouch	Svratoucha	k1gFnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Chrudim	Chrudim	k1gFnSc1
náležejícím	náležející	k2eAgFnPc3d1
do	do	k7c2
Pardubického	pardubický	k2eAgInSc2d1
kraje	kraj	k1gInSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Kopec	kopec	k1gInSc1
nad	nad	k7c7
Svratouchem	Svratouch	k1gInSc7
<g/>
,	,	kIx,
místně	místně	k6eAd1
uváděný	uváděný	k2eAgInSc1d1
Na	na	k7c6
Panenkách	panenka	k1gFnPc6
(	(	kIx(
<g/>
pohled	pohled	k1gInSc4
od	od	k7c2
jihovýchodu	jihovýchod	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Vrchol	vrchol	k1gInSc1
kopce	kopec	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
rozlehlé	rozlehlý	k2eAgFnSc6d1
louce	louka	k1gFnSc6
nad	nad	k7c7
obcí	obec	k1gFnSc7
Svratouch	Svratoucha	k1gFnPc2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
1	#num#	k4
160	#num#	k4
m	m	kA
v	v	k7c6
severozápadním	severozápadní	k2eAgInSc6d1
směru	směr	k1gInSc6
(	(	kIx(
<g/>
310	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
od	od	k7c2
kaple	kaple	k1gFnSc2
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
v	v	k7c6
obci	obec	k1gFnSc6
a	a	k8xC
625	#num#	k4
m	m	kA
jihozápadně	jihozápadně	k6eAd1
(	(	kIx(
<g/>
252	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
od	od	k7c2
silnice	silnice	k1gFnSc2
II	II	kA
<g/>
/	/	kIx~
<g/>
354	#num#	k4
(	(	kIx(
<g/>
úsek	úsek	k1gInSc1
Krouna	Krouna	k1gFnSc1
–	–	k?
Svratouch	Svratouch	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
odbočkou	odbočka	k1gFnSc7
do	do	k7c2
Čachnova	Čachnov	k1gInSc2
na	na	k7c6
vrcholu	vrchol	k1gInSc6
sedla	sedlo	k1gNnSc2
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
708	#num#	k4
m	m	kA
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
obce	obec	k1gFnSc2
Svratouch	Svratouch	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sedlo	sedlo	k1gNnSc1
odděluje	oddělovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
vrcholy	vrchol	k1gInPc4
s	s	k7c7
názvy	název	k1gInPc7
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
(	(	kIx(
<g/>
737,4	737,4	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Otava	Otava	k1gFnSc1
(	(	kIx(
<g/>
734,5	734,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
také	také	k9
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
(	(	kIx(
<g/>
přírodní	přírodní	k2eAgInPc1d1
regiony	region	k1gInPc1
<g/>
)	)	kIx)
Železných	železný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
a	a	k8xC
Hornosvratecké	Hornosvratecký	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lokalitu	lokalita	k1gFnSc4
s	s	k7c7
loukami	louka	k1gFnPc7
místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
tradičně	tradičně	k6eAd1
nazývají	nazývat	k5eAaImIp3nP
kopec	kopec	k1gInSc4
Na	na	k7c6
Panenkách	panenka	k1gFnPc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
pozemků	pozemek	k1gInPc2
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
od	od	k7c2
března	březen	k1gInSc2
2018	#num#	k4
náleží	náležet	k5eAaImIp3nS
obci	obec	k1gFnSc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
na	na	k7c6
svahu	svah	k1gInSc6
vybudován	vybudovat	k5eAaPmNgInS
vodojem	vodojem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
vrcholovou	vrcholový	k2eAgFnSc7d1
částí	část	k1gFnSc7
na	na	k7c6
vlhkých	vlhký	k2eAgFnPc6d1
loukách	louka	k1gFnPc6
prameniště	prameniště	k1gNnSc2
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
<g/>
,	,	kIx,
řeky	řeka	k1gFnSc2
Chrudimky	Chrudimka	k1gFnSc2
a	a	k8xC
Krounky	Krounka	k1gFnSc2
<g/>
,	,	kIx,
potoku	potok	k1gInSc6
Chlumětínského	Chlumětínský	k2eAgInSc2d1
a	a	k8xC
Svratouchu	Svratouch	k1gInSc2
(	(	kIx(
<g/>
též	též	k9
s	s	k7c7
názvem	název	k1gInSc7
Řivnáč	řivnáč	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zeměpisný	zeměpisný	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Zeměpisný	zeměpisný	k2eAgInSc1d1
název	název	k1gInSc1
v	v	k7c6
písemné	písemný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
U	u	k7c2
oběšeného	oběšený	k2eAgMnSc2d1
je	být	k5eAaImIp3nS
standardizované	standardizovaný	k2eAgNnSc1d1
vlastní	vlastní	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
geografického	geografický	k2eAgInSc2d1
objektu	objekt	k1gInSc2
typu	typ	k1gInSc2
kopce	kopec	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc6
vrcholu	vrchol	k1gInSc6
<g/>
,	,	kIx,
publikované	publikovaný	k2eAgInPc4d1
ve	v	k7c6
státním	státní	k2eAgNnSc6d1
mapovém	mapový	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
a	a	k8xC
databázích	databáze	k1gFnPc6
Českého	český	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
zeměměřického	zeměměřický	k2eAgInSc2d1
a	a	k8xC
katastrálního	katastrální	k2eAgInSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geomorfologie	geomorfologie	k1gFnPc1
a	a	k8xC
přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Geomorfologické	geomorfologický	k2eAgNnSc1d1
rozhraní	rozhraní	k1gNnSc1
<g/>
,	,	kIx,
výškové	výškový	k2eAgInPc1d1
body	bod	k1gInPc1
<g/>
,	,	kIx,
orografická	orografický	k2eAgFnSc1d1
rozvodnice	rozvodnice	k1gFnSc1
(	(	kIx(
<g/>
situační	situační	k2eAgFnSc1d1
popisky	popiska	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Vrchol	vrchol	k1gInSc1
U	u	k7c2
oběšeného	oběšený	k2eAgNnSc2d1
je	být	k5eAaImIp3nS
nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
na	na	k7c4
zemského	zemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
současně	současně	k6eAd1
tří	tři	k4xCgFnPc2
řádově	řádově	k6eAd1
odlišných	odlišný	k2eAgFnPc2d1
geomorfologických	geomorfologický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
regionálního	regionální	k2eAgNnSc2d1
členění	členění	k1gNnSc2
reliéfu	reliéf	k1gInSc2
zemského	zemský	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
(	(	kIx(
<g/>
georeliéfu	georeliéf	k1gInSc2
<g/>
)	)	kIx)
systémově	systémově	k6eAd1
provedeného	provedený	k2eAgInSc2d1
pro	pro	k7c4
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
Česka	Česko	k1gNnSc2
je	být	k5eAaImIp3nS
kuželovitý	kuželovitý	k2eAgInSc1d1
kopec	kopec	k1gInSc1
součástí	součást	k1gFnPc2
Kameničské	Kameničský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
,	,	kIx,
řádově	řádově	k6eAd1
nejnižší	nízký	k2eAgFnPc1d3
geomorfologické	geomorfologický	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horopisně	horopisně	k6eAd1
nejvyšší	vysoký	k2eAgInSc4d3
geomorfologický	geomorfologický	k2eAgInSc4d1
okrsek	okrsek	k1gInSc4
tvoří	tvořit	k5eAaImIp3nP
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
třemi	tři	k4xCgMnPc7
<g/>
,	,	kIx,
Skutečskou	skutečský	k2eAgFnSc7d1
pahorkatinou	pahorkatina	k1gFnSc7
<g/>
,	,	kIx,
Stružineckou	Stružinecký	k2eAgFnSc7d1
pahorkatinou	pahorkatina	k1gFnSc7
a	a	k8xC
Podhradskou	podhradský	k2eAgFnSc7d1
kotlinou	kotlina	k1gFnSc7
<g/>
,	,	kIx,
skladebnou	skladebný	k2eAgFnSc4d1
část	část	k1gFnSc4
geomorfologického	geomorfologický	k2eAgInSc2d1
podcelku	podcelek	k1gInSc2
s	s	k7c7
názvem	název	k1gInSc7
Sečská	sečský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
geomorfologického	geomorfologický	k2eAgInSc2d1
celku	celek	k1gInSc2
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
hřbetnici	hřbetnice	k1gFnSc6
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
loukách	louka	k1gFnPc6
a	a	k8xC
částečně	částečně	k6eAd1
i	i	k9
zemědělsky	zemědělsky	k6eAd1
obdělávaných	obdělávaný	k2eAgNnPc6d1
polích	pole	k1gNnPc6
<g/>
,	,	kIx,
probíhá	probíhat	k5eAaImIp3nS
rozvodnice	rozvodnice	k1gFnSc1
hlavního	hlavní	k2eAgNnSc2d1
evropského	evropský	k2eAgNnSc2d1
rozvodí	rozvodí	k1gNnSc2
Labe	Labe	k1gNnSc2
–	–	k?
Dunaj	Dunaj	k1gInSc1
<g/>
,	,	kIx,
dělící	dělící	k2eAgNnSc1d1
úmoří	úmoří	k1gNnSc1
náležející	náležející	k2eAgFnSc2d1
Severnímu	severní	k2eAgNnSc3d1
moři	moře	k1gNnSc3
a	a	k8xC
Černému	černý	k2eAgNnSc3d1
moři	moře	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Svahy	svah	k1gInPc7
vrchu	vrch	k1gInSc2
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
(	(	kIx(
<g/>
k	k	k7c3
obci	obec	k1gFnSc3
Svratouch	Svratoucha	k1gFnPc2
<g/>
)	)	kIx)
až	až	k8xS
jihozápadě	jihozápad	k1gInSc6
poměrně	poměrně	k6eAd1
prudce	prudko	k6eAd1
klesají	klesat	k5eAaImIp3nP
do	do	k7c2
údolí	údolí	k1gNnSc2
(	(	kIx(
<g/>
ve	v	k7c6
směru	směr	k1gInSc6
k	k	k7c3
řece	řeka	k1gFnSc3
Svratce	Svratka	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
v	v	k7c6
západním	západní	k2eAgInSc6d1
až	až	k8xS
severním	severní	k2eAgInSc6d1
směru	směr	k1gInSc6
jsou	být	k5eAaImIp3nP
strmější	strmý	k2eAgFnPc1d2
<g/>
,	,	kIx,
níže	nízce	k6eAd2
jen	jen	k9
pozvolnější	pozvolný	k2eAgInSc1d2
na	na	k7c6
rozsáhlých	rozsáhlý	k2eAgFnPc6d1
vlhkých	vlhký	k2eAgFnPc6d1
loukách	louka	k1gFnPc6
s	s	k7c7
několika	několik	k4yIc7
prameništi	prameniště	k1gNnSc3
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
(	(	kIx(
<g/>
Chrudimka	Chrudimka	k1gFnSc1
<g/>
,	,	kIx,
Krounka	Krounka	k1gFnSc1
<g/>
,	,	kIx,
potoky	potok	k1gInPc1
Chlumětínský	Chlumětínský	k2eAgMnSc1d1
a	a	k8xC
Svratouch	Svratouch	k1gMnSc1
/	/	kIx~
Řivnáč	řivnáč	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
vrcholu	vrchol	k1gInSc2
zhruba	zhruba	k6eAd1
na	na	k7c4
východo-severovýchod	východo-severovýchod	k1gInSc4
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
Svratouchu	Svratouch	k1gInSc2
podélné	podélný	k2eAgNnSc4d1
sedlo	sedlo	k1gNnSc4
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
kolem	kolem	k7c2
708	#num#	k4
m	m	kA
<g/>
,	,	kIx,
oddělující	oddělující	k2eAgInSc1d1
kopec	kopec	k1gInSc1
U	u	k7c2
oběšeného	oběšený	k2eAgMnSc2d1
od	od	k7c2
zhruba	zhruba	k6eAd1
1	#num#	k4
km	km	kA
vzdáleného	vzdálený	k2eAgInSc2d1
vrchu	vrch	k1gInSc2
Otava	Otava	k1gFnSc1
(	(	kIx(
<g/>
734,5	734,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1
sedla	sedlo	k1gNnSc2
tvoří	tvořit	k5eAaImIp3nS
současně	současně	k6eAd1
rozhraní	rozhraní	k1gNnSc2
dvou	dva	k4xCgInPc2
geomorfologických	geomorfologický	k2eAgInPc2d1
celků	celek	k1gInPc2
s	s	k7c7
názvy	název	k1gInPc7
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
(	(	kIx(
<g/>
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
<g/>
)	)	kIx)
a	a	k8xC
Hornosvratecká	Hornosvratecký	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
(	(	kIx(
<g/>
Otava	Otava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hranice	hranice	k1gFnSc1
mezi	mezi	k7c7
geomorfologickými	geomorfologický	k2eAgInPc7d1
celky	celek	k1gInPc7
(	(	kIx(
<g/>
ve	v	k7c6
směru	směr	k1gInSc6
sever	sever	k1gInSc4
–	–	k?
jih	jih	k1gInSc1
<g/>
)	)	kIx)
pokračuje	pokračovat	k5eAaImIp3nS
od	od	k7c2
vrcholu	vrchol	k1gInSc2
sedla	sedlo	k1gNnSc2
v	v	k7c6
linii	linie	k1gFnSc6
údolnic	údolnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severním	severní	k2eAgInSc7d1
směrem	směr	k1gInSc7
zhruba	zhruba	k6eAd1
podél	podél	k7c2
koryta	koryto	k1gNnSc2
řeky	řeka	k1gFnSc2
Krounky	Krounka	k1gFnSc2
až	až	k9
na	na	k7c4
úroveň	úroveň	k1gFnSc4
okraje	okraj	k1gInSc2
lesa	les	k1gInSc2
nad	nad	k7c7
Krounou	Krouna	k1gFnSc7
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
směřuje	směřovat	k5eAaImIp3nS
velkým	velký	k2eAgInSc7d1
obloukem	oblouk	k1gInSc7
k	k	k7c3
Čachnovu	Čachnov	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jižním	jižní	k2eAgInSc6d1
směru	směr	k1gInSc6
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
nad	nad	k7c7
korytem	koryto	k1gNnSc7
potoka	potok	k1gInSc2
Svratouch	Svratoucha	k1gFnPc2
(	(	kIx(
<g/>
Řivnáč	řivnáč	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
klesá	klesat	k5eAaImIp3nS
do	do	k7c2
údolí	údolí	k1gNnSc2
k	k	k7c3
řece	řeka	k1gFnSc3
Svratce	Svratka	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Geologické	geologický	k2eAgNnSc1d1
podloží	podloží	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
dvojslídný	dvojslídný	k2eAgInSc1d1
svor	svor	k1gInSc1
<g/>
,	,	kIx,
ostrůvky	ostrůvek	k1gInPc1
amfibolitu	amfibolit	k1gInSc2
a	a	k8xC
biotitické	biotitický	k2eAgFnPc1d1
ruly	rula	k1gFnPc1
drobně	drobně	k6eAd1
zrnité	zrnitý	k2eAgFnPc1d1
granoblastické	granoblastický	k2eAgFnPc1d1
<g/>
,	,	kIx,
v	v	k7c6
nižších	nízký	k2eAgFnPc6d2
polohách	poloha	k1gFnPc6
na	na	k7c6
severozápadě	severozápad	k1gInSc6
a	a	k8xC
jihovýchodě	jihovýchod	k1gInSc6
dvojslídný	dvojslídný	k2eAgMnSc1d1
migmatit	migmatit	k5eAaPmF,k5eAaImF
až	až	k9
ortorula	ortorula	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
vrcholu	vrchol	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
skarn	skarn	k1gInSc1
na	na	k7c6
území	území	k1gNnSc6
s	s	k7c7
významnou	významný	k2eAgFnSc7d1
geologickou	geologický	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
v	v	k7c6
minulosti	minulost	k1gFnSc6
probíhala	probíhat	k5eAaImAgFnS
těžba	těžba	k1gFnSc1
horniny	hornina	k1gFnSc2
na	na	k7c4
železnou	železný	k2eAgFnSc4d1
rudu	ruda	k1gFnSc4
a	a	k8xC
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
také	také	k9
předmětem	předmět	k1gInSc7
geologického	geologický	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
na	na	k7c4
uranovou	uranový	k2eAgFnSc4d1
rudu	ruda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vegetace	vegetace	k1gFnSc1
</s>
<s>
Kvetoucí	kvetoucí	k2eAgFnPc1d1
vlhké	vlhký	k2eAgFnPc1d1
louky	louka	k1gFnPc1
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
v	v	k7c6
červnu	červen	k1gInSc6
<g/>
,	,	kIx,
vpravo	vpravo	k6eAd1
křoviny	křovina	k1gFnPc1
v	v	k7c6
prameništi	prameniště	k1gNnSc6
řeky	řeka	k1gFnSc2
Krounky	Krounka	k1gFnSc2
</s>
<s>
Vrcholovou	vrcholový	k2eAgFnSc4d1
část	část	k1gFnSc4
pokrývají	pokrývat	k5eAaImIp3nP
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
louky	louka	k1gFnPc4
<g/>
,	,	kIx,
níže	nízce	k6eAd2
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
vlhké	vlhký	k2eAgFnSc2d1
<g/>
,	,	kIx,
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
<g/>
,	,	kIx,
severozápadě	severozápad	k1gInSc6
až	až	k8xS
severovýchodě	severovýchod	k1gInSc6
s	s	k7c7
mokřady	mokřad	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severních	severní	k2eAgInPc6d1
a	a	k8xC
jižních	jižní	k2eAgInPc6d1
svazích	svah	k1gInPc6
na	na	k7c6
menších	malý	k2eAgFnPc6d2
plochách	plocha	k1gFnPc6
zemědělsky	zemědělsky	k6eAd1
obdělávaná	obdělávaný	k2eAgNnPc4d1
pole	pole	k1gNnPc4
a	a	k8xC
také	také	k9
louky	louka	k1gFnPc4
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
využívané	využívaný	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
pastviny	pastvina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severozápadě	severozápad	k1gInSc6
až	až	k8xS
severovýchodě	severovýchod	k1gInSc6
pod	pod	k7c7
vrcholovou	vrcholový	k2eAgFnSc7d1
částí	část	k1gFnSc7
rozsáhlý	rozsáhlý	k2eAgInSc1d1
lesní	lesní	k2eAgInSc1d1
komplex	komplex	k1gInSc1
s	s	k7c7
lesní	lesní	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
Humperky	Humperka	k1gFnSc2
a	a	k8xC
polesím	polesí	k1gNnSc7
Stará	starat	k5eAaImIp3nS
Obora	obora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
dvou	dva	k4xCgFnPc6
lokalitách	lokalita	k1gFnPc6
<g/>
,	,	kIx,
u	u	k7c2
okraje	okraj	k1gInSc2
polesí	polesí	k1gNnSc2
Stará	starat	k5eAaImIp3nS
Obora	obora	k1gFnSc1
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgInPc1d1
biotopy	biotop	k1gInPc1
s	s	k7c7
vrchovištním	vrchovištní	k2eAgNnSc7d1
rašeliništěm	rašeliniště	k1gNnSc7
a	a	k8xC
mechovým	mechový	k2eAgNnSc7d1
slatiništěm	slatiniště	k1gNnSc7
zvláště	zvláště	k6eAd1
chráněného	chráněný	k2eAgNnSc2d1
území	území	k1gNnSc2
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
U	u	k7c2
Tučkovy	Tučkův	k2eAgFnSc2d1
hájenky	hájenka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
lesního	lesní	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
společenstva	společenstvo	k1gNnSc2
smrkových	smrkový	k2eAgFnPc2d1
bučin	bučina	k1gFnPc2
s	s	k7c7
chráněnými	chráněný	k2eAgInPc7d1
druhy	druh	k1gInPc7
ptáků	pták	k1gMnPc2
na	na	k7c6
zvláště	zvláště	k6eAd1
chráněném	chráněný	k2eAgNnSc6d1
území	území	k1gNnSc6
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
Bučina	bučina	k1gFnSc1
–	–	k?
Spálený	spálený	k2eAgInSc4d1
kopec	kopec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vodstvo	vodstvo	k1gNnSc1
</s>
<s>
Rozvodnice	rozvodnice	k1gFnSc1
hlavního	hlavní	k2eAgNnSc2d1
evropského	evropský	k2eAgNnSc2d1
rozvodí	rozvodí	k1gNnSc2
Labe	Labe	k1gNnSc2
–	–	k?
Dunaj	Dunaj	k1gInSc1
<g/>
,	,	kIx,
procházející	procházející	k2eAgFnSc1d1
vrcholovou	vrcholový	k2eAgFnSc7d1
částí	část	k1gFnSc7
<g/>
,	,	kIx,
pokračuje	pokračovat	k5eAaImIp3nS
po	po	k7c4
hřbetnici	hřbetnice	k1gFnSc4
zhruba	zhruba	k6eAd1
východo-severovýchodním	východo-severovýchodní	k2eAgInSc7d1
směrem	směr	k1gInSc7
k	k	k7c3
vrcholu	vrchol	k1gInSc3
sedla	sedlo	k1gNnSc2
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
Svratouchu	Svratouch	k1gInSc2
<g/>
,	,	kIx,
od	od	k7c2
něho	on	k3xPp3gMnSc2
k	k	k7c3
vrchu	vrch	k1gInSc2
Otava	Otava	k1gFnSc1
<g/>
,	,	kIx,
též	též	k9
Otavův	Otavův	k2eAgInSc1d1
kopec	kopec	k1gInSc1
(	(	kIx(
<g/>
734,5	734,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
dále	daleko	k6eAd2
geomorfologickým	geomorfologický	k2eAgInSc7d1
okrskem	okrsek	k1gInSc7
Borovský	borovský	k2eAgInSc4d1
les	les	k1gInSc4
(	(	kIx(
<g/>
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jižním	jižní	k2eAgInSc6d1
směru	směr	k1gInSc6
od	od	k7c2
vrcholu	vrchol	k1gInSc2
pokračuje	pokračovat	k5eAaImIp3nS
Kameničskou	Kameničský	k2eAgFnSc7d1
vrchovinou	vrchovina	k1gFnSc7
na	na	k7c4
Peškův	Peškův	k2eAgInSc4d1
vrch	vrch	k1gInSc4
(	(	kIx(
<g/>
716,8	716,8	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
od	od	k7c2
něho	on	k3xPp3gMnSc2
na	na	k7c4
jihozápad	jihozápad	k1gInSc4
k	k	k7c3
obci	obec	k1gFnSc3
Herálec	Herálec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lokalita	lokalita	k1gFnSc1
prameniště	prameniště	k1gNnSc2
Chlumětínského	Chlumětínský	k2eAgNnSc2d1
potoku	potok	k1gInSc6
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
(	(	kIx(
<g/>
uprostřed	uprostřed	k7c2
záběru	záběr	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pohled	pohled	k1gInSc1
z	z	k7c2
polí	pole	k1gFnPc2
nad	nad	k7c7
Chlumětínem	Chlumětín	k1gInSc7
od	od	k7c2
západu	západ	k1gInSc2
</s>
<s>
Na	na	k7c6
svazích	svah	k1gInPc6
vrcholové	vrcholový	k2eAgFnSc2d1
části	část	k1gFnSc2
pramení	pramenit	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
významné	významný	k2eAgFnPc1d1
řeky	řeka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
750	#num#	k4
m	m	kA
severozápadně	severozápadně	k6eAd1
od	od	k7c2
vrcholu	vrchol	k1gInSc2
vyvěrá	vyvěrat	k5eAaImIp3nS
Filipovský	Filipovský	k2eAgInSc4d1
pramen	pramen	k1gInSc4
na	na	k7c6
okraji	okraj	k1gInSc6
lesní	lesní	k2eAgFnSc2d1
lokality	lokalita	k1gFnSc2
Humperky	Humperka	k1gFnSc2
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
705	#num#	k4
m	m	kA
<g/>
,	,	kIx,
nejvýše	nejvýše	k6eAd1,k6eAd3
položený	položený	k2eAgInSc4d1
a	a	k8xC
také	také	k9
nejvydatnější	vydatný	k2eAgMnSc1d3
z	z	k7c2
pramenů	pramen	k1gInPc2
Chrudimky	Chrudimka	k1gFnSc2
<g/>
,	,	kIx,
nejvýznamnější	významný	k2eAgFnSc2d3
řeky	řeka	k1gFnSc2
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pramenná	pramenný	k2eAgFnSc1d1
zdrojnice	zdrojnice	k1gFnSc1
řeky	řeka	k1gFnSc2
stéká	stékat	k5eAaImIp3nS
po	po	k7c4
úbočí	úbočí	k1gNnPc4
k	k	k7c3
Filipovu	Filipův	k2eAgMnSc3d1
(	(	kIx(
<g/>
od	od	k7c2
něho	on	k3xPp3gNnSc2
název	název	k1gInSc1
pramene	pramen	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
části	část	k1gFnSc2
obce	obec	k1gFnSc2
Kameničky	kamenička	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
sever	sever	k1gInSc4
až	až	k9
severo-severovýchod	severo-severovýchod	k1gInSc4
od	od	k7c2
vrcholu	vrchol	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
rozsáhlé	rozsáhlý	k2eAgNnSc1d1
prameniště	prameniště	k1gNnSc1
řeky	řeka	k1gFnSc2
Krounky	Krounka	k1gFnSc2
na	na	k7c6
vlhkých	vlhký	k2eAgFnPc6d1
až	až	k8xS
podmáčených	podmáčený	k2eAgFnPc6d1
loukách	louka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vodoteč	vodoteč	k1gFnSc1
stéká	stékat	k5eAaImIp3nS
po	po	k7c6
prudkém	prudký	k2eAgInSc6d1
svahu	svah	k1gInSc6
v	v	k7c6
lesním	lesní	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
Stará	starý	k2eAgFnSc1d1
Obora	obora	k1gFnSc1
v	v	k7c6
meandrujícím	meandrující	k2eAgNnSc6d1
korytu	koryto	k1gNnSc6
směrem	směr	k1gInSc7
k	k	k7c3
obci	obec	k1gFnSc3
Krouna	Krouna	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
ní	on	k3xPp3gFnSc2
název	název	k1gInSc4
řeky	řeka	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
také	také	k9
prameniště	prameniště	k1gNnSc4
dvou	dva	k4xCgInPc2
potoků	potok	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jihozápad	jihozápad	k1gInSc4
od	od	k7c2
vrcholu	vrchol	k1gInSc2
rozsahem	rozsah	k1gInSc7
větší	veliký	k2eAgNnSc4d2
prameniště	prameniště	k1gNnSc4
Chlumětínského	Chlumětínský	k2eAgInSc2d1
potoku	potok	k1gInSc3
<g/>
,	,	kIx,
levostranného	levostranný	k2eAgInSc2d1
přítoku	přítok	k1gInSc2
řeky	řeka	k1gFnSc2
Chrudimky	Chrudimka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potok	potok	k1gInSc1
napájí	napájet	k5eAaImIp3nS
rybník	rybník	k1gInSc4
Krejcar	krejcar	k1gInSc4
<g/>
,	,	kIx,
ležící	ležící	k2eAgInSc4d1
mezi	mezi	k7c7
Krejcarem	krejcar	k1gInSc7
(	(	kIx(
<g/>
část	část	k1gFnSc1
obce	obec	k1gFnSc2
Chlumětín	Chlumětína	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
obcí	obec	k1gFnPc2
Kameničky	kamenička	k1gFnSc2
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
také	také	k9
rybník	rybník	k1gInSc1
Groš	groš	k1gInSc1
<g/>
,	,	kIx,
průtočný	průtočný	k2eAgInSc1d1
řekou	řeka	k1gFnSc7
Chrudimkou	Chrudimka	k1gFnSc7
v	v	k7c6
Kameničkách	Kameničky	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
vrcholu	vrchol	k1gInSc2
na	na	k7c4
jihovýchod	jihovýchod	k1gInSc4
pramení	pramenit	k5eAaImIp3nS
potok	potok	k1gInSc1
s	s	k7c7
názvem	název	k1gInSc7
Svratouch	Svratouch	k1gInSc1
(	(	kIx(
<g/>
též	též	k9
Řivnáč	řivnáč	k1gMnSc1
<g/>
)	)	kIx)
mezi	mezi	k7c7
zástavbou	zástavba	k1gFnSc7
obce	obec	k1gFnSc2
Svratouch	Svratoucha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potok	potok	k1gInSc1
stéká	stékat	k5eAaImIp3nS
po	po	k7c4
úbočí	úbočí	k1gNnPc4
do	do	k7c2
údolí	údolí	k1gNnSc2
s	s	k7c7
řekou	řeka	k1gFnSc7
Svratkou	Svratka	k1gFnSc7
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jejím	její	k3xOp3gInSc7
levostranným	levostranný	k2eAgInSc7d1
přítokem	přítok	k1gInSc7
na	na	k7c6
území	území	k1gNnSc6
města	město	k1gNnSc2
Svratky	Svratka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vrchol	vrchol	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
bod	bod	k1gInSc4
rozhraní	rozhraní	k1gNnSc2
mezi	mezi	k7c7
orografickými	orografický	k2eAgFnPc7d1
rozvodnicemi	rozvodnice	k1gFnPc7
dílčího	dílčí	k2eAgInSc2d1
povodí	povodí	k1gNnPc1
Chrudimky	Chrudimka	k1gFnSc2
<g/>
,	,	kIx,
řeky	řeka	k1gFnSc2
Krounky	Krounka	k1gFnSc2
a	a	k8xC
Chlumětínského	Chlumětínský	k2eAgInSc2d1
potoku	potok	k1gInSc6
v	v	k7c6
povodí	povodí	k1gNnSc6
evropské	evropský	k2eAgFnSc2d1
řeky	řeka	k1gFnSc2
Labe	Labe	k1gNnSc2
a	a	k8xC
potoku	potok	k1gInSc6
Brodek	brodek	k1gInSc1
v	v	k7c6
dílčím	dílčí	k2eAgNnSc6d1
povodí	povodí	k1gNnSc6
řeky	řeka	k1gFnSc2
Svratky	Svratka	k1gFnSc2
a	a	k8xC
evropské	evropský	k2eAgFnSc2d1
řeky	řeka	k1gFnSc2
Dunaj	Dunaj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
prameniště	prameniště	k1gNnSc2
a	a	k8xC
orografické	orografický	k2eAgFnSc2d1
rozvodnice	rozvodnice	k1gFnSc2
dílčího	dílčí	k2eAgNnSc2d1
povodí	povodí	k1gNnSc2
řeky	řeka	k1gFnSc2
Chrudimky	Chrudimka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prameniště	prameniště	k1gNnSc1
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
v	v	k7c6
lokalitách	lokalita	k1gFnPc6
kopce	kopec	k1gInSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
součástí	součást	k1gFnSc7
chráněné	chráněný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
přirozené	přirozený	k2eAgFnSc2d1
akumulace	akumulace	k1gFnSc2
vod	voda	k1gFnPc2
(	(	kIx(
<g/>
CHOPAV	CHOPAV	kA
<g/>
)	)	kIx)
s	s	k7c7
názvem	název	k1gInSc7
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
<g/>
,	,	kIx,
s	s	k7c7
více	hodně	k6eAd2
prameny	pramen	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CHOPAV	CHOPAV	kA
je	být	k5eAaImIp3nS
ve	v	k7c6
smyslu	smysl	k1gInSc6
nařízení	nařízení	k1gNnSc2
vlády	vláda	k1gFnSc2
ČSR	ČSR	kA
č.	č.	k?
40	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
vymezeno	vymezit	k5eAaPmNgNnS
shodně	shodně	k6eAd1
s	s	k7c7
Chráněnou	chráněný	k2eAgFnSc7d1
krajinnou	krajinný	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
Žďárské	Žďárské	k2eAgInPc7d1
vrchy	vrch	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výstup	výstup	k1gInSc1
na	na	k7c4
vrchol	vrchol	k1gInSc4
</s>
<s>
Loukami	louka	k1gFnPc7
<g/>
,	,	kIx,
přes	přes	k7c4
vrcholovou	vrcholový	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
,	,	kIx,
směřuje	směřovat	k5eAaImIp3nS
do	do	k7c2
Svratouchu	Svratouch	k1gInSc2
polní	polní	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
od	od	k7c2
Filipovského	Filipovský	k2eAgInSc2d1
pramene	pramen	k1gInSc2
řeky	řeka	k1gFnSc2
Chrudimky	Chrudimka	k1gFnSc2
</s>
<s>
Turistická	turistický	k2eAgFnSc1d1
značená	značený	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
není	být	k5eNaImIp3nS
na	na	k7c4
vrchol	vrchol	k1gInSc4
vedena	vést	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholová	vrcholový	k2eAgFnSc1d1
část	část	k1gFnSc1
s	s	k7c7
loukami	louka	k1gFnPc7
je	být	k5eAaImIp3nS
dostupná	dostupný	k2eAgFnSc1d1
polní	polní	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
spojující	spojující	k2eAgFnSc2d1
obce	obec	k1gFnSc2
Svratouch	Svratoucha	k1gFnPc2
a	a	k8xC
Chlumětín	Chlumětína	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
části	část	k1gFnSc6
Paseky	paseka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
obou	dva	k4xCgInPc2
směrů	směr	k1gInPc2
stoupá	stoupat	k5eAaImIp3nS
k	k	k7c3
vrcholu	vrchol	k1gInSc3
a	a	k8xC
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
míjí	míjet	k5eAaImIp3nS
asi	asi	k9
50	#num#	k4
m	m	kA
vzdálený	vzdálený	k2eAgInSc1d1
výškový	výškový	k2eAgInSc1d1
trigonometrický	trigonometrický	k2eAgInSc1d1
bod	bod	k1gInSc1
ve	v	k7c6
volném	volný	k2eAgInSc6d1
terénu	terén	k1gInSc6
na	na	k7c6
sezónně	sezónně	k6eAd1
sečené	sečený	k2eAgFnSc6d1
louce	louka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
využitím	využití	k1gNnSc7
místních	místní	k2eAgFnPc2d1
komunikací	komunikace	k1gFnPc2
a	a	k8xC
polních	polní	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
je	být	k5eAaImIp3nS
vrcholová	vrcholový	k2eAgFnSc1d1
část	část	k1gFnSc1
s	s	k7c7
loukami	louka	k1gFnPc7
dostupná	dostupný	k2eAgFnSc1d1
také	také	k9
z	z	k7c2
obce	obec	k1gFnSc2
Chlumětín	Chlumětína	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc1
k	k	k7c3
vrcholové	vrcholový	k2eAgFnSc3d1
části	část	k1gFnSc3
<g/>
:	:	kIx,
</s>
<s>
z	z	k7c2
vrcholu	vrchol	k1gInSc2
sedla	sedlo	k1gNnSc2
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
obce	obec	k1gFnSc2
Svratouch	Svratoucha	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
odbočky	odbočka	k1gFnSc2
na	na	k7c4
Čachnov	Čachnov	k1gInSc4
ze	z	k7c2
silnice	silnice	k1gFnSc2
II	II	kA
<g/>
/	/	kIx~
<g/>
354	#num#	k4
(	(	kIx(
<g/>
Krouna	Krouna	k1gFnSc1
–	–	k?
Svratouch	Svratouch	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vzdálenost	vzdálenost	k1gFnSc1
625	#num#	k4
m	m	kA
</s>
<s>
z	z	k7c2
okraje	okraj	k1gInSc2
lesní	lesní	k2eAgFnSc2d1
lokality	lokalita	k1gFnSc2
Humperky	Humperka	k1gFnSc2
<g/>
,	,	kIx,
od	od	k7c2
zeleně	zeleň	k1gFnSc2
značené	značený	k2eAgFnSc2d1
turistické	turistický	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
Klubu	klub	k1gInSc2
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
(	(	kIx(
<g/>
úsek	úsek	k1gInSc1
Čachnov	Čachnov	k1gInSc1
–	–	k?
Filipovský	Filipovský	k2eAgInSc1d1
pramen	pramen	k1gInSc1
–	–	k?
Kameničky	kamenička	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
U	u	k7c2
Tučkovy	Tučkův	k2eAgFnSc2d1
hájenky	hájenka	k1gFnSc2
<g/>
,	,	kIx,
vzdálenost	vzdálenost	k1gFnSc1
420	#num#	k4
m	m	kA
</s>
<s>
od	od	k7c2
Filipovského	Filipovský	k2eAgInSc2d1
pramene	pramen	k1gInSc2
řeky	řeka	k1gFnSc2
Chrudimky	Chrudimka	k1gFnSc2
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
Pramen	pramen	k1gInSc1
Chrudimky	Chrudimka	k1gFnSc2
na	na	k7c6
zeleně	zeleně	k6eAd1
značené	značený	k2eAgFnSc6d1
turistické	turistický	k2eAgFnSc6d1
trase	trasa	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
výchozí	výchozí	k2eAgMnSc1d1
místo	místo	k7c2
vlastivědné	vlastivědný	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
Krajem	kraj	k1gInSc7
Chrudimky	Chrudimka	k1gFnSc2
<g/>
,	,	kIx,
vzdálenost	vzdálenost	k1gFnSc1
cca	cca	kA
750	#num#	k4
m	m	kA
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vrchol	vrchol	k1gInSc1
uveden	uveden	k2eAgInSc1d1
na	na	k7c6
turistické	turistický	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
(	(	kIx(
<g/>
mapový	mapový	k2eAgInSc4d1
list	list	k1gInSc4
č.	č.	k?
48	#num#	k4
<g/>
,	,	kIx,
měřítko	měřítko	k1gNnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
vydané	vydaný	k2eAgFnSc2d1
v	v	k7c6
edici	edice	k1gFnSc6
Klubu	klub	k1gInSc2
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc1d1
souvislosti	souvislost	k1gFnPc1
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
Železných	železný	k2eAgFnPc2d1
hor	hora	k1gFnPc2
/	/	kIx~
Sečské	sečský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
/	/	kIx~
Kameničské	Kameničský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
veřejně	veřejně	k6eAd1
prezentován	prezentovat	k5eAaBmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
v	v	k7c6
Zeměpisném	zeměpisný	k2eAgInSc6d1
lexikonu	lexikon	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
Hory	hora	k1gFnSc2
a	a	k8xC
nížiny	nížina	k1gFnSc2
vydaném	vydaný	k2eAgNnSc6d1
Agenturou	agentura	k1gFnSc7
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
po	po	k7c6
upřesnění	upřesnění	k1gNnSc6
hranic	hranice	k1gFnPc2
geomorfologických	geomorfologický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
pomocí	pomocí	k7c2
digitální	digitální	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
v	v	k7c6
Zeměpisném	zeměpisný	k2eAgInSc6d1
lexikonu	lexikon	k1gInSc6
ČSR	ČSR	kA
–	–	k?
Hory	hora	k1gFnSc2
a	a	k8xC
nížiny	nížina	k1gFnSc2
<g/>
,	,	kIx,
vydaném	vydaný	k2eAgNnSc6d1
bývalým	bývalý	k2eAgMnSc7d1
Geografickým	geografický	k2eAgInSc7d1
ústavem	ústav	k1gInSc7
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
uvedena	uveden	k2eAgNnPc1d1
za	za	k7c4
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Pešava	Pešava	k1gFnSc1
(	(	kIx(
<g/>
697	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
nad	nad	k7c7
Jeníkovem	Jeníkov	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
zimním	zimní	k2eAgNnSc6d1
období	období	k1gNnSc6
je	být	k5eAaImIp3nS
výstup	výstup	k1gInSc1
na	na	k7c4
vrchol	vrchol	k1gInSc4
možný	možný	k2eAgInSc4d1
na	na	k7c6
lyžích	lyže	k1gFnPc6
<g/>
,	,	kIx,
pohled	pohled	k1gInSc1
k	k	k7c3
jihozápadu	jihozápad	k1gInSc3
při	při	k7c6
západu	západ	k1gInSc3
slunceV	slunceV	k?
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Českomoravské	českomoravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
mají	mít	k5eAaImIp3nP
zeměpisné	zeměpisný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
ve	v	k7c6
stejném	stejný	k2eAgNnSc6d1
znění	znění	k1gNnSc6
dva	dva	k4xCgInPc4
vrcholy	vrchol	k1gInPc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
písemné	písemný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
názvu	název	k1gInSc2
za	za	k7c7
předložkou	předložka	k1gFnSc7
s	s	k7c7
rozdílným	rozdílný	k2eAgNnSc7d1
počátečním	počáteční	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
u	u	k7c2
přídavného	přídavný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
(	(	kIx(
<g/>
adjektivum	adjektivum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
publikovaná	publikovaný	k2eAgNnPc4d1
Českým	český	k2eAgInSc7d1
úřadem	úřad	k1gInSc7
zeměměřickým	zeměměřický	k2eAgInSc7d1
a	a	k8xC
katastrálním	katastrální	k2eAgInSc7d1
<g/>
,	,	kIx,
uvádí	uvádět	k5eAaImIp3nS
v	v	k7c6
písemné	písemný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
název	název	k1gInSc1
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
(	(	kIx(
<g/>
737,4	737,4	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
vrchol	vrchol	k1gInSc4
v	v	k7c6
Kameničské	Kameničský	k2eAgFnSc6d1
vrchovině	vrchovina	k1gFnSc6
na	na	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
obce	obec	k1gFnSc2
Svratouch	Svratoucha	k1gFnPc2
v	v	k7c6
Pardubickém	pardubický	k2eAgInSc6d1
kraji	kraj	k1gInSc6
a	a	k8xC
v	v	k7c6
písemné	písemný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
U	u	k7c2
Oběšeného	oběšený	k2eAgInSc2d1
(	(	kIx(
<g/>
369,1	369,1	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
název	název	k1gInSc1
vrcholu	vrchol	k1gInSc2
v	v	k7c6
Mohelenské	Mohelenský	k2eAgFnSc6d1
vrchovině	vrchovina	k1gFnSc6
na	na	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
obce	obec	k1gFnSc2
Nová	Nová	k1gFnSc1
Ves	ves	k1gFnSc1
u	u	k7c2
Oslavan	Oslavany	k1gInPc2
v	v	k7c6
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
starých	starý	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
tzv.	tzv.	kA
stabilního	stabilní	k2eAgInSc2d1
katastru	katastr	k1gInSc2
<g/>
,	,	kIx,
pořízených	pořízený	k2eAgInPc2d1
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
lokalita	lokalita	k1gFnSc1
s	s	k7c7
vrcholem	vrchol	k1gInSc7
uváděna	uvádět	k5eAaImNgFnS
pod	pod	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
U	u	k7c2
Obetsenyho	Obetseny	k1gMnSc2
<g/>
"	"	kIx"
a	a	k8xC
na	na	k7c6
vojenských	vojenský	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
při	při	k7c6
tzv.	tzv.	kA
3	#num#	k4
<g/>
.	.	kIx.
vojenském	vojenský	k2eAgNnSc6d1
mapování	mapování	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
jméno	jméno	k1gNnSc1
u	u	k7c2
výškové	výškový	k2eAgFnSc2d1
kóty	kóta	k1gFnSc2
737	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
vůbec	vůbec	k9
uvedeno	uveden	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sousední	sousední	k2eAgInSc4d1
vrchol	vrchol	k1gInSc4
s	s	k7c7
nivelací	nivelace	k1gFnSc7
734	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
označili	označit	k5eAaPmAgMnP
kartografové	kartograf	k1gMnPc1
názvem	název	k1gInSc7
„	„	k?
<g/>
Otawa	Otawa	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
současné	současný	k2eAgNnSc1d1
zeměpisné	zeměpisný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
Otava	Otava	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
turistických	turistický	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
též	též	k6eAd1
Otavův	Otavův	k2eAgInSc4d1
kopec	kopec	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
Otavova	Otavův	k2eAgInSc2d1
kopce	kopec	k1gInSc2
byla	být	k5eAaImAgFnS
vybudována	vybudován	k2eAgFnSc1d1
meteorologická	meteorologický	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
provozovaná	provozovaný	k2eAgFnSc1d1
Českým	český	k2eAgInSc7d1
hydrometeorologickým	hydrometeorologický	k2eAgInSc7d1
ústavem	ústav	k1gInSc7
<g/>
,	,	kIx,
doplněná	doplněný	k2eAgNnPc4d1
později	pozdě	k6eAd2
webovou	webový	k2eAgFnSc7d1
kamerou	kamera	k1gFnSc7
monitorující	monitorující	k2eAgNnSc1d1
počasí	počasí	k1gNnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Svratouchu	Svratouch	k1gInSc2
(	(	kIx(
<g/>
údaje	údaj	k1gInPc4
z	z	k7c2
kamery	kamera	k1gFnSc2
dostupné	dostupný	k2eAgFnPc1d1
on-line	on-lin	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
záběru	záběr	k1gInSc6
kamery	kamera	k1gFnSc2
částečně	částečně	k6eAd1
viditelné	viditelný	k2eAgFnPc1d1
i	i	k8xC
svahy	svah	k1gInPc4
vrcholu	vrchol	k1gInSc2
U	u	k7c2
oběšeného	oběšený	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
(	(	kIx(
<g/>
spojeným	spojený	k2eAgInSc7d1
se	s	k7c7
zemí	zem	k1gFnPc2
<g/>
)	)	kIx)
v	v	k7c6
Železných	železný	k2eAgFnPc6d1
horách	hora	k1gFnPc6
/	/	kIx~
Sečské	sečský	k2eAgFnSc3d1
vrchovině	vrchovina	k1gFnSc3
/	/	kIx~
Kameničské	Kameničský	k2eAgFnSc6d1
vrchovině	vrchovina	k1gFnSc6
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
vrchol	vrchol	k1gInSc4
radiokomunikační	radiokomunikační	k2eAgFnSc2d1
věže	věž	k1gFnSc2
televizního	televizní	k2eAgInSc2d1
vysílače	vysílač	k1gInSc2
Krásné	krásný	k2eAgNnSc1d1
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
796	#num#	k4
m	m	kA
<g/>
,	,	kIx,
samotná	samotný	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
je	být	k5eAaImIp3nS
182	#num#	k4
m	m	kA
vysoká	vysoká	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gFnSc1
pata	pata	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
614	#num#	k4
m	m	kA
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
kopce	kopec	k1gInSc2
Krásný	krásný	k2eAgInSc1d1
(	(	kIx(
<g/>
616	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
na	na	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Krásné	krásný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Rozhledová	rozhledový	k2eAgNnPc1d1
místa	místo	k1gNnPc1
</s>
<s>
Vrcholová	vrcholový	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
rozhledovým	rozhledový	k2eAgNnSc7d1
místem	místo	k1gNnSc7
do	do	k7c2
všech	všecek	k3xTgFnPc2
světových	světový	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
turistické	turistický	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
vyznačeno	vyznačit	k5eAaPmNgNnS
topografickou	topografický	k2eAgFnSc7d1
značkou	značka	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Rozhled	rozhled	k1gInSc1
je	být	k5eAaImIp3nS
mírně	mírně	k6eAd1
omezen	omezit	k5eAaPmNgInS
výškou	výška	k1gFnSc7
lesního	lesní	k2eAgInSc2d1
porostu	porost	k1gInSc2
v	v	k7c6
polesí	polesí	k1gNnSc6
Humperky	Humperka	k1gFnSc2
a	a	k8xC
Stará	starý	k2eAgFnSc1d1
Obora	obora	k1gFnSc1
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
v	v	k7c6
severním	severní	k2eAgInSc6d1
směru	směr	k1gInSc6
(	(	kIx(
<g/>
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
částečně	částečně	k6eAd1
vysázeným	vysázený	k2eAgNnSc7d1
pásmem	pásmo	k1gNnSc7
smrčin	smrčina	k1gFnPc2
na	na	k7c4
jih	jih	k1gInSc4
od	od	k7c2
vrcholu	vrchol	k1gInSc2
(	(	kIx(
<g/>
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zvláště	zvláště	k6eAd1
dobré	dobrý	k2eAgFnSc6d1
viditelnosti	viditelnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
na	na	k7c6
severu	sever	k1gInSc6
zřetelné	zřetelný	k2eAgFnSc2d1
vyšší	vysoký	k2eAgFnSc2d2
partie	partie	k1gFnSc2
Orlických	orlický	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
Králického	králický	k2eAgInSc2d1
Sněžníku	Sněžník	k1gInSc2
a	a	k8xC
Hrubého	Hrubého	k2eAgInSc2d1
Jeseníku	Jeseník	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
západním	západní	k2eAgInSc6d1
směru	směr	k1gInSc6
také	také	k9
Vestec	Vestec	k1gInSc4
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
v	v	k7c6
CHKO	CHKO	kA
Železné	železný	k2eAgFnPc4d1
hory	hora	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Geografické	geografický	k2eAgInPc1d1
objekty	objekt	k1gInPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
různých	různý	k2eAgNnPc2d1
míst	místo	k1gNnPc2
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
kopce	kopec	k1gInSc2
alespoň	alespoň	k9
částečně	částečně	k6eAd1
viditelné	viditelný	k2eAgFnPc1d1
pouze	pouze	k6eAd1
za	za	k7c2
určitých	určitý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
(	(	kIx(
<g/>
počasí	počasí	k1gNnSc2
nebo	nebo	k8xC
pomocí	pomocí	k7c2
dalekohledu	dalekohled	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
některých	některý	k3yIgInPc2
výškových	výškový	k2eAgInPc2d1
bodů	bod	k1gInPc2
v	v	k7c6
dohledu	dohled	k1gInSc6
pouze	pouze	k6eAd1
část	část	k1gFnSc1
stavby	stavba	k1gFnSc2
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
,	,	kIx,
například	například	k6eAd1
špička	špička	k1gFnSc1
radiokomunikační	radiokomunikační	k2eAgFnSc2d1
věže	věž	k1gFnSc2
Spálený	spálený	k2eAgInSc1d1
kopec	kopec	k1gInSc1
nad	nad	k7c7
lesním	lesní	k2eAgInSc7d1
masivem	masiv	k1gInSc7
(	(	kIx(
<g/>
Borovský	borovský	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
věž	věž	k1gFnSc4
vysílače	vysílač	k1gInSc2
Krásné	krásný	k2eAgNnSc1d1
<g/>
,	,	kIx,
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
614	#num#	k4
m	m	kA
na	na	k7c4
ploché	plochý	k2eAgFnPc4d1
vrcholové	vrcholový	k2eAgFnPc4d1
části	část	k1gFnPc4
vyvýšeniny	vyvýšenina	k1gFnSc2
Krásný	krásný	k2eAgMnSc1d1
(	(	kIx(
<g/>
616	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vrchol	vrchol	k1gInSc4
v	v	k7c6
terénu	terén	k1gInSc6
mimo	mimo	k7c4
dohled	dohled	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Geografické	geografický	k2eAgInPc1d1
objekty	objekt	k1gInPc1
ve	v	k7c6
výhledu	výhled	k1gInSc6
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
svah	svah	k1gInSc1
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
s	s	k7c7
rozvodím	rozvodí	k1gNnSc7
evropských	evropský	k2eAgFnPc2d1
řek	řeka	k1gFnPc2
Labe	Labe	k1gNnSc2
a	a	k8xC
Dunaj	Dunaj	k1gInSc1
nad	nad	k7c7
rozhraním	rozhraní	k1gNnSc7
Kameničské	Kameničský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
a	a	k8xC
Borovského	Borovského	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
,	,	kIx,
nad	nad	k7c7
obcí	obec	k1gFnSc7
Svratouch	Svratouch	k1gInSc4
vrchol	vrchol	k1gInSc1
Karlštejn	Karlštejn	k1gInSc1
(	(	kIx(
<g/>
784	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nad	nad	k7c7
dolní	dolní	k2eAgFnSc7d1
částí	část	k1gFnSc7
obce	obec	k1gFnSc2
Louckého	Louckého	k2eAgInSc4d1
kopec	kopec	k1gInSc4
(	(	kIx(
<g/>
700	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
Lokality	lokalita	k1gFnSc2
v	v	k7c6
nejbližším	blízký	k2eAgNnSc6d3
okolí	okolí	k1gNnSc6
leží	ležet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
dálce	dálka	k1gFnSc6
viditelný	viditelný	k2eAgInSc1d1
vrchol	vrchol	k1gInSc1
Vestec	Vestec	k1gInSc1
v	v	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nivelace	nivelace	k1gFnSc1
některých	některý	k3yIgInPc2
výškových	výškový	k2eAgInPc2d1
bodů	bod	k1gInPc2
je	být	k5eAaImIp3nS
vztažena	vztáhnout	k5eAaPmNgFnS
na	na	k7c4
skalní	skalní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
na	na	k7c6
zalesněných	zalesněný	k2eAgInPc6d1
hřbetech	hřbet	k1gInPc6
Žďárských	Žďárských	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
<g/>
,	,	kIx,
vrchol	vrchol	k1gInSc1
skaliska	skalisko	k1gNnSc2
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
v	v	k7c6
terénu	terén	k1gInSc6
pozorovatelný	pozorovatelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
vrcholů	vrchol	k1gInPc2
bez	bez	k7c2
uvedené	uvedený	k2eAgFnSc2d1
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
je	být	k5eAaImIp3nS
příslušný	příslušný	k2eAgInSc1d1
údaj	údaj	k1gInSc1
zmíněný	zmíněný	k2eAgInSc1d1
v	v	k7c6
přehledu	přehled	k1gInSc6
níže	níže	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přibližně	přibližně	k6eAd1
ve	v	k7c6
směru	směr	k1gInSc6
jednotlivých	jednotlivý	k2eAgFnPc2d1
světových	světový	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Sever	sever	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
rozsahu	rozsah	k1gInSc6
SZ	SZ	kA
–	–	k?
SV	sv	kA
<g/>
)	)	kIx)
</s>
<s>
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
–	–	k?
severozápadně	severozápadně	k6eAd1
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
lesní	lesní	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
Humperky	Humperka	k1gFnSc2
s	s	k7c7
Filipovským	Filipovský	k2eAgInSc7d1
pramenem	pramen	k1gInSc7
řeky	řeka	k1gFnSc2
Chrudimky	Chrudimka	k1gFnSc2
<g/>
,	,	kIx,
severněji	severně	k6eAd2
část	část	k1gFnSc1
prameniště	prameniště	k1gNnSc2
řeky	řeka	k1gFnSc2
Krounky	Krounka	k1gFnSc2
a	a	k8xC
vlhké	vlhký	k2eAgFnSc2d1
louky	louka	k1gFnSc2
s	s	k7c7
dřevinami	dřevina	k1gFnPc7
ve	v	k7c6
zvláště	zvláště	k6eAd1
chráněném	chráněný	k2eAgNnSc6d1
území	území	k1gNnSc6
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
U	u	k7c2
Tučkovy	Tučkův	k2eAgFnSc2d1
hájenky	hájenka	k1gFnSc2
</s>
<s>
Borovský	borovský	k2eAgInSc1d1
les	les	k1gInSc1
–	–	k?
polesí	polesí	k1gNnSc2
Stará	starý	k2eAgFnSc1d1
Obora	obora	k1gFnSc1
(	(	kIx(
<g/>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
Bučina	bučina	k1gFnSc1
–	–	k?
Spálený	spálený	k2eAgInSc1d1
kopec	kopec	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
vrchol	vrchol	k1gInSc1
sedla	sedlo	k1gNnSc2
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
obce	obec	k1gFnSc2
Svratouch	Svratouch	k1gInSc1
a	a	k8xC
vrch	vrch	k1gInSc1
Otava	Otava	k1gFnSc1
s	s	k7c7
budovou	budova	k1gFnSc7
Českého	český	k2eAgInSc2d1
hydrometeorologického	hydrometeorologický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
průběh	průběh	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
evropského	evropský	k2eAgNnSc2d1
rozvodí	rozvodí	k1gNnSc2
Labe	Labe	k1gNnSc2
–	–	k?
Dunaj	Dunaj	k1gInSc1
</s>
<s>
Východ	východ	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
rozsahu	rozsah	k1gInSc6
SV	sv	kA
–	–	k?
JV	JV	kA
<g/>
)	)	kIx)
</s>
<s>
Borovský	borovský	k2eAgInSc1d1
les	les	k1gInSc1
–	–	k?
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
obce	obec	k1gFnSc2
Svratouch	Svratoucha	k1gFnPc2
<g/>
,	,	kIx,
lokalita	lokalita	k1gFnSc1
prameniště	prameniště	k1gNnSc2
potoku	potok	k1gInSc6
Svratouch	Svratouch	k1gMnSc1
(	(	kIx(
<g/>
Řivnáč	řivnáč	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nad	nad	k7c7
obcí	obec	k1gFnSc7
rozsáhlý	rozsáhlý	k2eAgInSc1d1
zalesněný	zalesněný	k2eAgInSc1d1
masiv	masiv	k1gInSc1
protáhlého	protáhlý	k2eAgInSc2d1
hřbetu	hřbet	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
svahu	svah	k1gInSc6
s	s	k7c7
vyvýšeninou	vyvýšenina	k1gFnSc7
Borovina	borovina	k1gFnSc1
(	(	kIx(
<g/>
723	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nad	nad	k7c7
lesem	les	k1gInSc7
vrcholek	vrcholek	k1gInSc1
radiokomunikační	radiokomunikační	k2eAgFnSc2d1
věže	věž	k1gFnSc2
(	(	kIx(
<g/>
nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
spojený	spojený	k2eAgInSc1d1
se	se	k3xPyFc4
zemí	zem	k1gFnSc7
<g/>
)	)	kIx)
na	na	k7c6
Spáleném	spálený	k2eAgInSc6d1
kopci	kopec	k1gInSc6
<g/>
,	,	kIx,
Karlštejn	Karlštejn	k1gInSc1
(	(	kIx(
<g/>
784	#num#	k4
m	m	kA
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
<g/>
)	)	kIx)
se	s	k7c7
sídelní	sídelní	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
stejného	stejný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
(	(	kIx(
<g/>
část	část	k1gFnSc1
obce	obec	k1gFnSc2
Svratouch	Svratoucha	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
dálce	dálka	k1gFnSc6
na	na	k7c6
okraji	okraj	k1gInSc6
hřbetu	hřbet	k1gInSc2
lokalita	lokalita	k1gFnSc1
Čtyřpaličaté	čtyřpaličatý	k2eAgFnSc2d1
černé	černý	k2eAgFnSc2d1
skály	skála	k1gFnSc2
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
vrcholy	vrchol	k1gInPc1
Děvín	Děvín	k1gInSc1
a	a	k8xC
Čtyři	čtyři	k4xCgFnPc1
palice	palice	k1gFnPc1
v	v	k7c6
přírodní	přírodní	k2eAgFnSc6d1
rezervaci	rezervace	k1gFnSc6
Čtyřpaličaté	čtyřpaličatý	k2eAgFnSc2d1
skály	skála	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Pohledeckoskalská	Pohledeckoskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
–	–	k?
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
zalesněné	zalesněný	k2eAgInPc4d1
vrcholy	vrchol	k1gInPc4
Vysoký	vysoký	k2eAgInSc1d1
kopec	kopec	k1gInSc1
a	a	k8xC
Buchtův	Buchtův	k2eAgInSc1d1
kopec	kopec	k1gInSc1
(	(	kIx(
<g/>
na	na	k7c6
vrcholu	vrchol	k1gInSc6
radiolokační	radiolokační	k2eAgFnSc1d1
věž	věž	k1gFnSc1
s	s	k7c7
typickým	typický	k2eAgNnSc7d1
opláštěním	opláštění	k1gNnSc7
ve	v	k7c6
tvaru	tvar	k1gInSc6
koule	koule	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
dálce	dálka	k1gFnSc6
vrchol	vrchol	k1gInSc4
Bohdalec	Bohdalec	k1gMnSc1
</s>
<s>
Milovská	milovský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
–	–	k?
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
zřetelná	zřetelný	k2eAgFnSc1d1
sníženina	sníženina	k1gFnSc1
mezi	mezi	k7c7
geomorfologickými	geomorfologický	k2eAgInPc7d1
okrsky	okrsek	k1gInPc7
Borovský	Borovský	k1gMnSc1
les	les	k1gInSc1
a	a	k8xC
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
viditelná	viditelný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
severovýchodně	severovýchodně	k6eAd1
nad	nad	k7c7
Křižánkami	Křižánka	k1gFnPc7
</s>
<s>
Pohled	pohled	k1gInSc1
z	z	k7c2
jižního	jižní	k2eAgInSc2d1
svahu	svah	k1gInSc2
na	na	k7c4
Devět	devět	k4xCc4
skal	skála	k1gFnPc2
(	(	kIx(
<g/>
836	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Hornosvratecké	Hornosvratecký	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
/	/	kIx~
Žďárských	Žďárských	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
/	/	kIx~
Devítiskalské	Devítiskalský	k2eAgFnPc4d1
vrchoviny	vrchovina	k1gFnPc4
(	(	kIx(
<g/>
na	na	k7c6
obzoru	obzor	k1gInSc6
uprostřed	uprostřed	k6eAd1
<g/>
)	)	kIx)
<g/>
Jih	jih	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
rozsahu	rozsah	k1gInSc6
JV	JV	kA
–	–	k?
JZ	JZ	kA
<g/>
)	)	kIx)
</s>
<s>
Borovský	borovský	k2eAgInSc1d1
les	les	k1gInSc1
–	–	k?
dolní	dolní	k2eAgFnSc4d1
část	část	k1gFnSc4
obce	obec	k1gFnSc2
Svratouch	Svratouch	k1gInSc1
<g/>
,	,	kIx,
nad	nad	k7c7
ní	on	k3xPp3gFnSc7
Louckého	Louckého	k2eAgInSc1d1
kopec	kopec	k1gInSc1
</s>
<s>
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
–	–	k?
částečně	částečně	k6eAd1
Svratka	Svratka	k1gFnSc1
a	a	k8xC
okolí	okolí	k1gNnSc1
s	s	k7c7
vrcholem	vrchol	k1gInSc7
U	u	k7c2
osla	osel	k1gMnSc2
a	a	k8xC
lokalitami	lokalita	k1gFnPc7
Zadní	zadní	k2eAgFnSc1d1
hůra	hůra	k1gFnSc1
a	a	k8xC
Hůry	hůra	k1gFnPc1
(	(	kIx(
<g/>
až	až	k9
700	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jihozápadně	jihozápadně	k6eAd1
nad	nad	k7c7
Křižánkami	Křižánka	k1gFnPc7
v	v	k7c6
zalesněném	zalesněný	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
vrcholy	vrchol	k1gInPc4
Drátenická	drátenický	k2eAgFnSc1d1
skála	skála	k1gFnSc1
<g/>
,	,	kIx,
Malinská	Malinský	k2eAgFnSc1d1
skála	skála	k1gFnSc1
<g/>
,	,	kIx,
Suchý	suchý	k2eAgInSc1d1
kopec	kopec	k1gInSc1
<g/>
,	,	kIx,
Křovina	křovina	k1gFnSc1
<g/>
,	,	kIx,
Lisovská	Lisovská	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
skála	skála	k1gFnSc1
a	a	k8xC
Devět	devět	k4xCc1
skal	skála	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
okolí	okolí	k1gNnSc6
Herálce	Herálka	k1gFnSc3
vrcholy	vrchol	k1gInPc4
Na	na	k7c6
Čermačkách	Čermačka	k1gFnPc6
(	(	kIx(
<g/>
721,6	721,6	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hřeben	hřeben	k1gInSc1
(	(	kIx(
<g/>
714	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nad	nad	k7c7
ním	on	k3xPp3gNnSc7
Na	na	k7c6
hřebeni	hřeben	k1gInSc6
(	(	kIx(
<g/>
741,2	741,2	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Žákova	Žákův	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Otrok	otrok	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
dálce	dálka	k1gFnSc6
také	také	k9
Tisůvka	Tisůvka	k1gFnSc1
<g/>
,	,	kIx,
Šindelný	Šindelný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
a	a	k8xC
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
Kamenný	kamenný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
–	–	k?
pod	pod	k7c7
pásmem	pásmo	k1gNnSc7
smrčin	smrčina	k1gFnPc2
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
nedaleký	daleký	k2eNgMnSc1d1
částečně	částečně	k6eAd1
zalesněný	zalesněný	k2eAgInSc1d1
Peškův	Peškův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
nad	nad	k7c7
obcí	obec	k1gFnSc7
Svratouch	Svratouch	k1gInSc1
<g/>
,	,	kIx,
průběh	průběh	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
rozvodí	rozvodí	k1gNnSc2
Labe	Labe	k1gNnSc2
–	–	k?
Dunaj	Dunaj	k1gInSc1
</s>
<s>
Západ	západ	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
rozsahu	rozsah	k1gInSc6
JZ	JZ	kA
–	–	k?
SZ	SZ	kA
<g/>
)	)	kIx)
</s>
<s>
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
–	–	k?
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
zalesněný	zalesněný	k2eAgInSc1d1
Vortovský	Vortovský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
–	–	k?
v	v	k7c6
zářezu	zářez	k1gInSc6
terénu	terén	k1gInSc2
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
část	část	k1gFnSc4
prameniště	prameniště	k1gNnSc2
Chlumětínského	Chlumětínský	k2eAgInSc2d1
potoka	potok	k1gInSc2
<g/>
,	,	kIx,
plošina	plošina	k1gFnSc1
s	s	k7c7
rozsáhlými	rozsáhlý	k2eAgFnPc7d1
vlhkými	vlhký	k2eAgFnPc7d1
loukami	louka	k1gFnPc7
<g/>
,	,	kIx,
Paseky	Paseka	k1gMnPc7
(	(	kIx(
<g/>
část	část	k1gFnSc4
obce	obec	k1gFnSc2
Chlumětín	Chlumětín	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
dálce	dálka	k1gFnSc6
nad	nad	k7c7
Studnicemi	studnice	k1gFnPc7
vrcholy	vrchol	k1gInPc7
Hradiště	Hradiště	k1gNnSc1
(	(	kIx(
<g/>
682,3	682,3	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
Přední	přední	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
(	(	kIx(
<g/>
693,0	693,0	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nad	nad	k7c7
Slavíkovem	Slavíkov	k1gInSc7
také	také	k9
Vestec	Vestec	k1gInSc4
(	(	kIx(
<g/>
668,0	668,0	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
v	v	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
a	a	k8xC
na	na	k7c6
severozápadě	severozápad	k1gInSc6
vrcholová	vrcholový	k2eAgFnSc1d1
část	část	k1gFnSc1
Pešavy	Pešava	k1gFnSc2
s	s	k7c7
radiokomunikační	radiokomunikační	k2eAgFnSc7d1
věží	věž	k1gFnSc7
nad	nad	k7c7
Jeníkovem	Jeníkov	k1gInSc7
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1
a	a	k8xC
orientace	orientace	k1gFnSc1
některých	některý	k3yIgNnPc2
míst	místo	k1gNnPc2
od	od	k7c2
vrcholu	vrchol	k1gInSc2
</s>
<s>
Jižní	jižní	k2eAgInSc1d1
svah	svah	k1gInSc1
kopce	kopec	k1gInSc2
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
<g/>
,	,	kIx,
situační	situační	k2eAgFnSc2d1
pohled	pohled	k1gInSc4
východním	východní	k2eAgInSc7d1
směrem	směr	k1gInSc7
k	k	k7c3
vrchu	vrch	k1gInSc3
Otava	Otava	k1gFnSc1
<g/>
,	,	kIx,
nad	nad	k7c7
rozhraním	rozhraní	k1gNnSc7
krajinných	krajinný	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
a	a	k8xC
Borovský	borovský	k2eAgInSc1d1
les	les	k1gInSc1
</s>
<s>
Geografické	geografický	k2eAgInPc1d1
objekty	objekt	k1gInPc1
typu	typ	k1gInSc2
vrcholu	vrchol	k1gInSc2
<g/>
,	,	kIx,
kopce	kopec	k1gInSc2
nebo	nebo	k8xC
hory	hora	k1gFnSc2
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
dle	dle	k7c2
základní	základní	k2eAgFnSc2d1
mapy	mapa	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
příslušný	příslušný	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
u	u	k7c2
některých	některý	k3yIgInPc2
vrcholů	vrchol	k1gInPc2
též	též	k9
zvláště	zvláště	k6eAd1
chráněné	chráněný	k2eAgNnSc4d1
území	území	k1gNnSc4
téhož	týž	k3xTgInSc2
názvu	název	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
nebo	nebo	k8xC
významný	významný	k2eAgInSc1d1
bod	bod	k1gInSc1
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
ke	k	k7c3
zmíněnému	zmíněný	k2eAgInSc3d1
geomorfologickému	geomorfologický	k2eAgInSc3d1
okrsku	okrsek	k1gInSc3
<g/>
,	,	kIx,
vrchol	vrchol	k1gInSc1
Vestec	Vestec	k1gInSc1
k	k	k7c3
území	území	k1gNnSc3
vymezeném	vymezený	k2eAgInSc6d1
hranicemi	hranice	k1gFnPc7
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
a	a	k8xC
orientační	orientační	k2eAgInSc4d1
bod	bod	k1gInSc4
ke	k	k7c3
konkrétnímu	konkrétní	k2eAgInSc3d1
trigonometrickému	trigonometrický	k2eAgInSc3d1
bodu	bod	k1gInSc3
v	v	k7c6
rámci	rámec	k1gInSc6
České	český	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
trigonometrické	trigonometrický	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
se	se	k3xPyFc4
rozumí	rozumět	k5eAaImIp3nS
vrchní	vrchní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
objektu	objekt	k1gInSc2
nebo	nebo	k8xC
vrcholová	vrcholový	k2eAgFnSc1d1
část	část	k1gFnSc1
stavby	stavba	k1gFnSc2
–	–	k?
věže	věž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
vybraných	vybraný	k2eAgInPc2d1
výškových	výškový	k2eAgInPc2d1
bodů	bod	k1gInPc2
vzdálenost	vzdálenost	k1gFnSc4
v	v	k7c6
kilometrech	kilometr	k1gInPc6
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
orientace	orientace	k1gFnSc1
(	(	kIx(
<g/>
azimut	azimut	k1gInSc1
<g/>
)	)	kIx)
od	od	k7c2
trigonometrického	trigonometrický	k2eAgInSc2d1
bodu	bod	k1gInSc2
U	u	k7c2
oběšeného	oběšený	k2eAgNnSc2d1
je	být	k5eAaImIp3nS
přibližná	přibližný	k2eAgFnSc1d1
a	a	k8xC
některé	některý	k3yIgInPc1
údaje	údaj	k1gInPc1
souvisejí	souviset	k5eAaImIp3nP
s	s	k7c7
uvedeným	uvedený	k2eAgInSc7d1
trigonometrickým	trigonometrický	k2eAgInSc7d1
bodem	bod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
vrcholové	vrcholový	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c2
příznivého	příznivý	k2eAgNnSc2d1
počasí	počasí	k1gNnSc2
a	a	k8xC
případně	případně	k6eAd1
dalekohledem	dalekohled	k1gInSc7
<g/>
,	,	kIx,
viditelné	viditelný	k2eAgInPc4d1
geografické	geografický	k2eAgInPc4d1
objekty	objekt	k1gInPc4
(	(	kIx(
<g/>
řazeno	řadit	k5eAaImNgNnS
abecedně	abecedně	k6eAd1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Buchtův	Buchtův	k2eAgInSc1d1
kopec	kopec	k1gInSc1
(	(	kIx(
<g/>
812,9	812,9	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
významný	významný	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
na	na	k7c6
vrcholu	vrchol	k1gInSc6
radiolokační	radiolokační	k2eAgFnSc1d1
věž	věž	k1gFnSc1
s	s	k7c7
kulovitým	kulovitý	k2eAgNnSc7d1
opláštěním	opláštění	k1gNnSc7
–	–	k?
nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
840	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
Pohledeckoskalská	Pohledeckoskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
11,5	11,5	k4
km	km	kA
(	(	kIx(
<g/>
135	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1
palice	palice	k1gFnPc1
(	(	kIx(
<g/>
733,2	733,2	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
téhož	týž	k3xTgInSc2
názvu	název	k1gInSc2
<g/>
,	,	kIx,
skalní	skalní	k2eAgInPc1d1
útvary	útvar	k1gInPc1
s	s	k7c7
vyhlídkou	vyhlídka	k1gFnSc7
<g/>
,	,	kIx,
Borovský	borovský	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
7,5	7,5	k4
km	km	kA
(	(	kIx(
<g/>
134	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Devět	devět	k4xCc1
skal	skála	k1gFnPc2
(	(	kIx(
<g/>
836,3	836,3	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
téhož	týž	k3xTgInSc2
názvu	název	k1gInSc2
<g/>
,	,	kIx,
skalní	skalní	k2eAgInPc1d1
útvary	útvar	k1gInPc1
s	s	k7c7
vyhlídkou	vyhlídka	k1gFnSc7
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
7	#num#	k4
km	km	kA
(	(	kIx(
<g/>
173	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Drátenická	drátenický	k2eAgFnSc1d1
skála	skála	k1gFnSc1
(	(	kIx(
<g/>
775,9	775,9	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
téhož	týž	k3xTgInSc2
názvu	název	k1gInSc2
<g/>
,	,	kIx,
vrchol	vrchol	k1gInSc1
někdy	někdy	k6eAd1
s	s	k7c7
názvem	název	k1gInSc7
Dráteníčky	dráteníček	k1gMnPc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
9,8	9,8	k4
km	km	kA
(	(	kIx(
<g/>
152	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Hradiště	Hradiště	k1gNnSc1
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
682,3	682,3	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
v	v	k7c6
turistických	turistický	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
též	též	k9
vrch	vrch	k1gInSc4
Zadní	zadní	k2eAgNnPc4d1
Hradiště	Hradiště	k1gNnPc4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
nad	nad	k7c7
Studnicemi	studnice	k1gFnPc7
<g/>
,	,	kIx,
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
9,8	9,8	k4
km	km	kA
(	(	kIx(
<g/>
272	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
802,5	802,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
významný	významný	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
8,7	8,7	k4
km	km	kA
(	(	kIx(
<g/>
226	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Karlštejn	Karlštejn	k1gInSc1
(	(	kIx(
<g/>
783,4	783,4	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
trigonometrický	trigonometrický	k2eAgInSc1d1
bod	bod	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
podle	podle	k7c2
vrstevnicové	vrstevnicový	k2eAgFnSc2d1
mapy	mapa	k1gFnSc2
nejvyšší	vysoký	k2eAgFnSc2d3
místo	místo	k7c2
terénu	terén	k1gInSc2
784	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
Borovský	borovský	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
4	#num#	k4
km	km	kA
(	(	kIx(
<g/>
120	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Králický	králický	k2eAgInSc1d1
Sněžník	Sněžník	k1gInSc1
(	(	kIx(
<g/>
1423,8	1423,8	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
Králický	králický	k2eAgInSc1d1
Sněžník	Sněžník	k1gInSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
viditelný	viditelný	k2eAgInSc1d1
jen	jen	k9
za	za	k7c2
příznivých	příznivý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
–	–	k?
79,2	79,2	k4
km	km	kA
(	(	kIx(
<g/>
48	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Krásný	krásný	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
616,0	616,0	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
orientační	orientační	k2eAgInSc1d1
bod	bod	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
614	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
radiokomunikační	radiokomunikační	k2eAgFnSc4d1
věž	věž	k1gFnSc4
182	#num#	k4
m	m	kA
vysoká	vysoká	k1gFnSc1
–	–	k?
nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
<g/>
,	,	kIx,
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
22,5	22,5	k4
km	km	kA
(	(	kIx(
<g/>
296	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Křovina	křovina	k1gFnSc1
(	(	kIx(
<g/>
829,7	829,7	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
zalesněná	zalesněný	k2eAgFnSc1d1
plochá	plochý	k2eAgFnSc1d1
vyvýšenina	vyvýšenina	k1gFnSc1
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
8,8	8,8	k4
km	km	kA
(	(	kIx(
<g/>
164	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Lisovská	Lisovský	k2eAgFnSc1d1
skála	skála	k1gFnSc1
(	(	kIx(
<g/>
801,7	801,7	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
významný	významný	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
téhož	týž	k3xTgInSc2
názvu	název	k1gInSc2
<g/>
,	,	kIx,
skalní	skalní	k2eAgInPc1d1
útvary	útvar	k1gInPc1
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
8	#num#	k4
km	km	kA
(	(	kIx(
<g/>
170	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Louckého	Louckého	k2eAgInSc1d1
kopec	kopec	k1gInSc1
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
700,5	700,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
zalesněný	zalesněný	k2eAgInSc1d1
nad	nad	k7c7
dolní	dolní	k2eAgFnSc7d1
částí	část	k1gFnSc7
obce	obec	k1gFnSc2
Svratouch	Svratouch	k1gInSc1
<g/>
,	,	kIx,
Borovský	borovský	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
2,4	2,4	k4
km	km	kA
(	(	kIx(
<g/>
142	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Malinská	Malinský	k2eAgFnSc1d1
skála	skála	k1gFnSc1
(	(	kIx(
<g/>
811,1	811,1	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
významný	významný	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
téhož	týž	k3xTgInSc2
názvu	název	k1gInSc2
<g/>
,	,	kIx,
skalní	skalní	k2eAgInPc1d1
útvary	útvar	k1gInPc1
s	s	k7c7
vyhlídkou	vyhlídka	k1gFnSc7
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
8,7	8,7	k4
km	km	kA
(	(	kIx(
<g/>
155	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Pohled	pohled	k1gInSc1
z	z	k7c2
vrchu	vrch	k1gInSc2
Otava	Otava	k1gFnSc1
(	(	kIx(
<g/>
734,5	734,5	k4
m	m	kA
<g/>
)	)	kIx)
k	k	k7c3
vrcholu	vrchol	k1gInSc3
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
(	(	kIx(
<g/>
737,4	737,4	k4
m	m	kA
<g/>
)	)	kIx)
nad	nad	k7c7
Svratouchem	Svratouch	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
obci	obec	k1gFnSc6
uváděného	uváděný	k2eAgMnSc2d1
jako	jako	k8xC,k8xS
kopec	kopec	k1gInSc1
Na	na	k7c6
Panenkách	panenka	k1gFnPc6
(	(	kIx(
<g/>
pohled	pohled	k1gInSc4
od	od	k7c2
východu	východ	k1gInSc2
<g/>
)	)	kIx)
<g/>
Otava	Otava	k1gFnSc1
(	(	kIx(
<g/>
734,5	734,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
v	v	k7c6
turistických	turistický	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
též	též	k6eAd1
Otavův	Otavův	k2eAgInSc4d1
kopec	kopec	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
na	na	k7c6
vrcholu	vrchol	k1gInSc6
objekt	objekt	k1gInSc1
Českého	český	k2eAgInSc2d1
hydrometeorologického	hydrometeorologický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
<g/>
,	,	kIx,
rozhledové	rozhledový	k2eAgNnSc1d1
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
průběh	průběh	k1gInSc1
evropského	evropský	k2eAgNnSc2d1
rozvodí	rozvodí	k1gNnSc2
Labe	Labe	k1gNnSc2
–	–	k?
Dunaj	Dunaj	k1gInSc1
<g/>
,	,	kIx,
Borovský	borovský	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
1	#num#	k4
km	km	kA
(	(	kIx(
<g/>
76	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Otrok	otrok	k1gMnSc1
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
717,3	717,3	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
radiokomunikační	radiokomunikační	k2eAgFnSc1d1
věž	věž	k1gFnSc1
s	s	k7c7
pozičním	poziční	k2eAgNnSc7d1
světlem	světlo	k1gNnSc7
<g/>
,	,	kIx,
průběh	průběh	k1gInSc1
evropského	evropský	k2eAgNnSc2d1
rozvodí	rozvodí	k1gNnSc2
Labe	Labe	k1gNnSc2
–	–	k?
Dunaj	Dunaj	k1gInSc1
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
7,3	7,3	k4
km	km	kA
(	(	kIx(
<g/>
210	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Pešava	Pešava	k1gFnSc1
(	(	kIx(
<g/>
697,0	697,0	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
významný	významný	k2eAgInSc1d1
bod	bod	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
nad	nad	k7c7
Jeníkovem	Jeníkov	k1gInSc7
<g/>
,	,	kIx,
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
4,7	4,7	k4
km	km	kA
(	(	kIx(
<g/>
295	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Peškův	Peškův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
716,8	716,8	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
průběh	průběh	k1gInSc1
evropského	evropský	k2eAgNnSc2d1
rozvodí	rozvodí	k1gNnSc2
Labe	Labe	k1gNnSc2
–	–	k?
Dunaj	Dunaj	k1gInSc1
<g/>
,	,	kIx,
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
1,4	1,4	k4
km	km	kA
(	(	kIx(
<g/>
184	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Praděd	praděd	k1gMnSc1
(	(	kIx(
<g/>
1491,3	1491,3	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
radiokomunikační	radiokomunikační	k2eAgFnSc1d1
věž	věž	k1gFnSc1
146,5	146,5	k4
m	m	kA
vysoká	vysoká	k1gFnSc1
–	–	k?
nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
Pradědský	Pradědský	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
viditelný	viditelný	k2eAgInSc1d1
jen	jen	k9
za	za	k7c2
příznivých	příznivý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
–	–	k?
95,4	95,4	k4
km	km	kA
(	(	kIx(
<g/>
66	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Přední	přední	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
693,0	693,0	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
skalní	skalní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
nad	nad	k7c7
Studnicemi	studnice	k1gFnPc7
<g/>
,	,	kIx,
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
9,2	9,2	k4
km	km	kA
(	(	kIx(
<g/>
274	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Spálený	spálený	k2eAgInSc1d1
kopec	kopec	k1gInSc1
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
765,5	765,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
radiokomunikační	radiokomunikační	k2eAgFnSc1d1
věž	věž	k1gFnSc1
–	–	k?
nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Borovský	borovský	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
viditelná	viditelný	k2eAgFnSc1d1
jen	jen	k9
část	část	k1gFnSc1
radiokomunikační	radiokomunikační	k2eAgFnSc2d1
věže	věž	k1gFnSc2
nad	nad	k7c7
lesním	lesní	k2eAgInSc7d1
masivem	masiv	k1gInSc7
–	–	k?
6,2	6,2	k4
km	km	kA
(	(	kIx(
<g/>
97	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Suchý	suchý	k2eAgInSc1d1
kopec	kopec	k1gInSc1
(	(	kIx(
<g/>
816	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
dle	dle	k7c2
vrstevnicové	vrstevnicový	k2eAgFnSc2d1
mapy	mapa	k1gFnSc2
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
8,5	8,5	k4
km	km	kA
(	(	kIx(
<g/>
159	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Šindelný	Šindelný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
805,7	805,7	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
průběh	průběh	k1gInSc1
evropského	evropský	k2eAgNnSc2d1
rozvodí	rozvodí	k1gNnSc2
Labe	Labe	k1gNnSc2
–	–	k?
Dunaj	Dunaj	k1gInSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
povodí	povodí	k1gNnSc2
řeky	řeka	k1gFnSc2
Chrudimky	Chrudimka	k1gFnSc2
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
9	#num#	k4
km	km	kA
(	(	kIx(
<g/>
215	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Tisůvka	Tisůvka	k1gFnSc1
(	(	kIx(
<g/>
800,3	800,3	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
téhož	týž	k3xTgInSc2
názvu	název	k1gInSc2
<g/>
,	,	kIx,
skalní	skalní	k2eAgInPc1d1
útvary	útvar	k1gInPc1
s	s	k7c7
vyhlídkou	vyhlídka	k1gFnSc7
<g/>
,	,	kIx,
orientační	orientační	k2eAgFnSc7d1
bod	bod	k1gInSc4
na	na	k7c6
skalním	skalní	k2eAgInSc6d1
výstupu	výstup	k1gInSc6
s	s	k7c7
názvem	název	k1gInSc7
Čertův	čertův	k2eAgInSc1d1
kámen	kámen	k1gInSc1
<g/>
,	,	kIx,
průběh	průběh	k1gInSc1
evropského	evropský	k2eAgNnSc2d1
rozvodí	rozvodí	k1gNnSc2
Labe	Labe	k1gNnSc2
–	–	k?
Dunaj	Dunaj	k1gInSc1
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
10,3	10,3	k4
km	km	kA
(	(	kIx(
<g/>
209	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
U	u	k7c2
osla	osel	k1gMnSc2
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
708,8	708,8	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
výrazný	výrazný	k2eAgInSc1d1
zalesněný	zalesněný	k2eAgInSc1d1
kopec	kopec	k1gInSc1
nad	nad	k7c7
Svratkou	Svratka	k1gFnSc7
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
4,04	4,04	k4
km	km	kA
(	(	kIx(
<g/>
152	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Deštná	deštný	k2eAgFnSc1d1
(	(	kIx(
<g/>
1115,1	1115,1	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
Orlický	orlický	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
viditelný	viditelný	k2eAgInSc1d1
jen	jen	k9
za	za	k7c2
příznivých	příznivý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
–	–	k?
68,7	68,7	k4
km	km	kA
(	(	kIx(
<g/>
23	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Vestec	Vestec	k1gInSc1
(	(	kIx(
<g/>
668,0	668,0	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
významný	významný	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
na	na	k7c6
území	území	k1gNnSc6
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
–	–	k?
18,4	18,4	k4
km	km	kA
(	(	kIx(
<g/>
276	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Vortovský	Vortovský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
704,4	704,4	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
zalesněný	zalesněný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
6	#num#	k4
km	km	kA
(	(	kIx(
<g/>
229	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
krku	krk	k1gInSc6
(	(	kIx(
<g/>
699,8	699,8	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
trigonometrický	trigonometrický	k2eAgInSc1d1
bod	bod	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
turistické	turistický	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
též	též	k9
vrch	vrch	k1gInSc1
s	s	k7c7
názvem	název	k1gInSc7
Zadní	zadní	k2eAgFnSc1d1
hůra	hůra	k1gFnSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
nad	nad	k7c7
Svratkou	Svratka	k1gFnSc7
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
3,8	3,8	k4
km	km	kA
(	(	kIx(
<g/>
172	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Žákova	Žákův	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
809,8	809,8	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
významný	významný	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
též	též	k9
historicky	historicky	k6eAd1
významný	významný	k2eAgInSc1d1
trigonometrický	trigonometrický	k2eAgInSc1d1
bod	bod	k1gInSc1
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
národní	národní	k2eAgFnPc4d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
téhož	týž	k3xTgInSc2
názvu	název	k1gInSc2
<g/>
,	,	kIx,
Devítiskalská	Devítiskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
8,6	8,6	k4
km	km	kA
(	(	kIx(
<g/>
193	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Geograficky	geograficky	k6eAd1
a	a	k8xC
turisticky	turisticky	k6eAd1
zajímavá	zajímavý	k2eAgNnPc4d1
místa	místo	k1gNnPc4
mimo	mimo	k7c4
dohlednost	dohlednost	k1gFnSc4
(	(	kIx(
<g/>
řazeno	řadit	k5eAaImNgNnS
podle	podle	k7c2
azimutu	azimut	k1gInSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Na	na	k7c6
borovině	borovina	k1gFnSc6
(	(	kIx(
<g/>
674	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
Milovská	milovský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
5,3	5,3	k4
km	km	kA
(	(	kIx(
<g/>
152	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Kopeček	kopeček	k1gInSc1
(	(	kIx(
<g/>
821,7	821,7	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
Pohledeckoskalská	Pohledeckoskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
14,5	14,5	k4
km	km	kA
(	(	kIx(
<g/>
159	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Pasecká	Pasecký	k2eAgFnSc1d1
skála	skála	k1gFnSc1
(	(	kIx(
<g/>
818,6	818,6	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
významný	významný	k2eAgInSc4d1
bod	bod	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
téhož	týž	k3xTgInSc2
názvu	název	k1gInSc2
<g/>
,	,	kIx,
skalní	skalní	k2eAgInPc1d1
útvary	útvar	k1gInPc1
s	s	k7c7
vyhlídkou	vyhlídka	k1gFnSc7
<g/>
,	,	kIx,
Pohledeckoskalská	Pohledeckoskalský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
13,7	13,7	k4
km	km	kA
(	(	kIx(
<g/>
161	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Javořice	Javořice	k1gFnSc1
(	(	kIx(
<g/>
836,5	836,5	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
na	na	k7c6
vrcholu	vrchol	k1gInSc6
radiokomunikační	radiokomunikační	k2eAgFnSc1d1
věž	věž	k1gFnSc1
166	#num#	k4
m	m	kA
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
Řásenská	Řásenský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
v	v	k7c6
rámci	rámec	k1gInSc6
Českomoravské	českomoravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
75,1	75,1	k4
km	km	kA
(	(	kIx(
<g/>
221	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Locus	Locus	k1gInSc1
perennis	perennis	k1gInSc1
<g/>
,	,	kIx,
historický	historický	k2eAgInSc1d1
památník	památník	k1gInSc1
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
výškového	výškový	k2eAgNnSc2d1
měření	měření	k1gNnSc2
Lišov	Lišov	k1gInSc1
a	a	k8xC
základní	základní	k2eAgInSc1d1
nivelační	nivelační	k2eAgInSc1d1
bod	bod	k1gInSc1
I.	I.	kA
Lišov	Lišov	k1gInSc1
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
564,76	564,76	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
Dobrovodská	Dobrovodský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
,	,	kIx,
<g/>
)	)	kIx)
–	–	k?
131,6	131,6	k4
km	km	kA
(	(	kIx(
<g/>
232	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Geografický	geografický	k2eAgMnSc1d1
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
geometrický	geometrický	k2eAgInSc1d1
střed	střed	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
Číhošť	Číhošť	k1gFnSc1
(	(	kIx(
<g/>
528	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
Třebětínská	Třebětínský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
49	#num#	k4
km	km	kA
(	(	kIx(
<g/>
271	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Nejníže	nízce	k6eAd3
položené	položený	k2eAgNnSc4d1
místo	místo	k1gNnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
krajní	krajní	k2eAgInSc1d1
bod	bod	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hladina	hladina	k1gFnSc1
řeky	řeka	k1gFnSc2
Labe	Labe	k1gNnSc2
v	v	k7c6
Hřensku	Hřensko	k1gNnSc6
(	(	kIx(
<g/>
115	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
Sněžnická	Sněžnický	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
180,5	180,5	k4
km	km	kA
(	(	kIx(
<g/>
315	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Sněžka	Sněžka	k1gFnSc1
(	(	kIx(
<g/>
1603,2	1603,2	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
Slezský	slezský	k2eAgInSc4d1
hřbet	hřbet	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
krajní	krajní	k2eAgInSc1d1
bod	bod	k1gInSc1
v	v	k7c6
georeliéfu	georeliéf	k1gInSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
113,3	113,3	k4
km	km	kA
(	(	kIx(
<g/>
350	#num#	k4
<g/>
°	°	k?
<g/>
)	)	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Trigonometrický	trigonometrický	k2eAgInSc1d1
bod	bod	k1gInSc1
U	u	k7c2
oběšeného	oběšený	k2eAgNnSc2d1
<g/>
,	,	kIx,
s	s	k7c7
nivelací	nivelace	k1gFnSc7
737,38	737,38	k4
m	m	kA
n.	n.	k?
m.	m.	k?
severozápadně	severozápadně	k6eAd1
nad	nad	k7c7
Svratouchem	Svratouch	k1gInSc7
<g/>
,	,	kIx,
po	po	k7c6
východu	východ	k1gInSc6
slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
inverze	inverze	k1gFnSc2
</s>
<s>
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
z	z	k7c2
vrcholu	vrchol	k1gInSc2
<g/>
,	,	kIx,
zleva	zleva	k6eAd1
Karlštejn	Karlštejn	k1gInSc1
(	(	kIx(
<g/>
784	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
dálce	dálka	k1gFnSc6
Čtyřpaličaté	čtyřpaličatý	k2eAgFnSc2d1
černé	černý	k2eAgFnSc2d1
skály	skála	k1gFnSc2
(	(	kIx(
<g/>
až	až	k9
733	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
Buchtův	Buchtův	k2eAgInSc1d1
kopec	kopec	k1gInSc1
(	(	kIx(
<g/>
813	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
inverze	inverze	k1gFnSc1
</s>
<s>
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
z	z	k7c2
vrcholu	vrchol	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
obzoru	obzor	k1gInSc6
zleva	zleva	k6eAd1
Drátenická	drátenický	k2eAgFnSc1d1
skála	skála	k1gFnSc1
(	(	kIx(
<g/>
776	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Malínská	malínský	k2eAgFnSc1d1
skála	skála	k1gFnSc1
(	(	kIx(
<g/>
811	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Křovina	křovina	k1gFnSc1
(	(	kIx(
<g/>
830	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Devět	devět	k4xCc1
skal	skála	k1gFnPc2
(	(	kIx(
<g/>
836	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
inverze	inverze	k1gFnSc1
</s>
<s>
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
z	z	k7c2
vrcholu	vrchol	k1gInSc2
<g/>
,	,	kIx,
kopce	kopec	k1gInSc2
Hradiště	Hradiště	k1gNnSc2
a	a	k8xC
Přední	přední	k2eAgNnSc4d1
Hradiště	Hradiště	k1gNnSc4
nad	nad	k7c7
Studnicemi	studnice	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c6
zářezu	zářez	k1gInSc6
prameniště	prameniště	k1gNnSc2
Chlumětínského	Chlumětínský	k2eAgInSc2d1
potoku	potok	k1gInSc6
</s>
<s>
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
z	z	k7c2
vrcholu	vrchol	k1gInSc2
<g/>
,	,	kIx,
Pešava	Pešava	k1gFnSc1
(	(	kIx(
<g/>
697	#num#	k4
m	m	kA
<g/>
)	)	kIx)
nad	nad	k7c7
Jeníkovem	Jeníkov	k1gInSc7
<g/>
,	,	kIx,
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
části	část	k1gFnSc6
stožár	stožár	k1gInSc1
mobilního	mobilní	k2eAgMnSc2d1
operátora	operátor	k1gMnSc2
(	(	kIx(
<g/>
maximální	maximální	k2eAgNnSc1d1
přiblížení	přiblížení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
vlhké	vlhký	k2eAgFnSc2d1
louky	louka	k1gFnSc2
a	a	k8xC
prameniště	prameniště	k1gNnSc4
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
v	v	k7c6
Chráněné	chráněný	k2eAgFnSc6d1
krajinné	krajinný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
a	a	k8xC
chráněné	chráněný	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
přirozené	přirozený	k2eAgFnSc2d1
akumulace	akumulace	k1gFnSc2
vod	voda	k1gFnPc2
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgFnPc1d1
vlhké	vlhký	k2eAgFnPc1d1
louky	louka	k1gFnPc1
severně	severně	k6eAd1
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
brzy	brzy	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
<g/>
,	,	kIx,
s	s	k7c7
nejvýše	nejvýše	k6eAd1,k6eAd3
položenou	položený	k2eAgFnSc7d1
částí	část	k1gFnSc7
prameniště	prameniště	k1gNnSc2
řeky	řeka	k1gFnSc2
Krounky	Krounka	k1gFnSc2
(	(	kIx(
<g/>
křoviny	křovina	k1gFnPc1
v	v	k7c6
záběru	záběr	k1gInSc6
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
U	u	k7c2
Tučkovy	Tučkův	k2eAgFnSc2d1
hájenky	hájenka	k1gFnSc2
<g/>
,	,	kIx,
západní	západní	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
severně	severně	k6eAd1
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
<g/>
,	,	kIx,
vegetace	vegetace	k1gFnSc1
brzy	brzy	k6eAd1
na	na	k7c6
jaře	jaro	k1gNnSc6
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
U	u	k7c2
Tučkovy	Tučkův	k2eAgFnSc2d1
hájenky	hájenka	k1gFnSc2
<g/>
,	,	kIx,
západní	západní	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
severně	severně	k6eAd1
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
<g/>
,	,	kIx,
louky	louka	k1gFnPc1
sezónně	sezónně	k6eAd1
sečené	sečený	k2eAgFnPc1d1
</s>
<s>
Svratouch	Svratouch	k1gInSc1
z	z	k7c2
lokality	lokalita	k1gFnSc2
místně	místně	k6eAd1
uváděné	uváděný	k2eAgFnPc4d1
Na	na	k7c6
Panenkách	panenka	k1gFnPc6
<g/>
,	,	kIx,
část	část	k1gFnSc1
obce	obec	k1gFnSc2
s	s	k7c7
vrcholem	vrchol	k1gInSc7
Karlštejn	Karlštejn	k1gInSc1
(	(	kIx(
<g/>
784	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
níže	nízce	k6eAd2
vpravo	vpravo	k6eAd1
věž	věž	k1gFnSc1
evangelického	evangelický	k2eAgInSc2d1
kostela	kostel	k1gInSc2
</s>
<s>
Panoráma	panoráma	k1gFnSc1
z	z	k7c2
vrcholu	vrchol	k1gInSc2
JZ	JZ	kA
<g/>
–	–	k?
<g/>
Z	Z	kA
<g/>
:	:	kIx,
na	na	k7c6
obzoru	obzor	k1gInSc6
zleva	zleva	k6eAd1
Žákova	Žákův	k2eAgFnSc1d1
hora	hora	k1gFnSc1
(	(	kIx(
<g/>
809,8	809,8	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Šindelný	Šindelný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
805,7	805,7	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kamenný	kamenný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
802,5	802,5	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
Panoráma	panoráma	k1gFnSc1
z	z	k7c2
vrcholu	vrchol	k1gInSc2
SZ	SZ	kA
<g/>
–	–	k?
<g/>
SV	sv	kA
<g/>
:	:	kIx,
zleva	zleva	k6eAd1
lokalita	lokalita	k1gFnSc1
Humperky	Humperka	k1gFnSc2
(	(	kIx(
<g/>
Filipovský	Filipovský	k2eAgInSc4d1
pramen	pramen	k1gInSc4
Chrudimky	Chrudimka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
prameniště	prameniště	k1gNnSc4
Krounky	Krounka	k1gFnSc2
a	a	k8xC
PP	PP	kA
U	u	k7c2
Tučkovy	Tučkův	k2eAgFnSc2d1
hájenky	hájenka	k1gFnSc2
</s>
<s>
Pohled	pohled	k1gInSc1
z	z	k7c2
vrcholu	vrchol	k1gInSc2
(	(	kIx(
<g/>
východ	východ	k1gInSc1
–	–	k?
jih	jih	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
Svratouch	Svratoucha	k1gFnPc2
a	a	k8xC
Žďárské	Žďárské	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
vlevo	vlevo	k6eAd1
Otava	Otava	k1gFnSc1
s	s	k7c7
budovou	budova	k1gFnSc7
ČHMÚ	ČHMÚ	kA
(	(	kIx(
<g/>
průběh	průběh	k1gInSc4
evropského	evropský	k2eAgNnSc2d1
rozvodí	rozvodí	k1gNnSc2
řek	řeka	k1gFnPc2
Labe	Labe	k1gNnSc2
a	a	k8xC
Dunaj	Dunaj	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
uprostřed	uprostřed	k6eAd1
Karlštejn	Karlštejn	k1gInSc1
(	(	kIx(
<g/>
vrchol	vrchol	k1gInSc1
a	a	k8xC
sídelní	sídelní	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vpravo	vpravo	k6eAd1
oblast	oblast	k1gFnSc1
Devítiskalské	Devítiskalský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
:	:	kIx,
vrchol	vrchol	k1gInSc1
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
(	(	kIx(
<g/>
737	#num#	k4
m	m	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
DEMEK	DEMEK	kA
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
;	;	kIx,
MACKOVČIN	MACKOVČIN	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
;	;	kIx,
BALATKA	balatka	k1gFnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hory	hora	k1gFnSc2
a	a	k8xC
nížiny	nížina	k1gFnSc2
<g/>
:	:	kIx,
Zeměpisný	zeměpisný	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Jaromír	Jaromír	k1gMnSc1
Demek	Demek	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Mackovčin	Mackovčin	k2eAgMnSc1d1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
582	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
1	#num#	k4
mapa	mapa	k1gFnSc1
<g/>
,	,	kIx,
fotografie	fotografie	k1gFnSc1
<g/>
,	,	kIx,
1	#num#	k4
CD	CD	kA
<g/>
,	,	kIx,
vložená	vložený	k2eAgNnPc4d1
errata	errata	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86064	#num#	k4
<g/>
-	-	kIx~
<g/>
99	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
211	#num#	k4
<g/>
,	,	kIx,
394	#num#	k4
<g/>
,	,	kIx,
528	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Železné	železný	k2eAgFnPc4d1
hory	hora	k1gFnPc4
-	-	kIx~
viz	vidět	k5eAaImRp2nS
vložená	vložený	k2eAgFnSc1d1
errata	errata	k1gNnPc4
<g/>
.	.	kIx.
↑	↑	k?
Obec	obec	k1gFnSc1
Svratouch	Svratouch	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomístní	pomístní	k2eAgInSc4d1
název	název	k1gInSc4
lokality	lokalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Česká	český	k2eAgFnSc1d1
geologická	geologický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geovědní	Geovědní	k2eAgFnPc4d1
mapy	mapa	k1gFnPc4
<g/>
:	:	kIx,
kompozice	kompozice	k1gFnSc1
Svratouch	Svratouch	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turistická	turistický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
<g/>
:	:	kIx,
Českomoravská	českomoravský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapy	mapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměměřičský	zeměměřičský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Přehledová	přehledový	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
bodových	bodový	k2eAgFnPc2d1
polí	pole	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
-	-	kIx~
trigonometrický	trigonometrický	k2eAgInSc1d1
bod	bod	k1gInSc1
č.	č.	k?
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Klub	klub	k1gInSc4
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turistická	turistický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
<g/>
:	:	kIx,
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Trasa	trasa	k1gFnSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7324	#num#	k4
<g/>
-	-	kIx~
<g/>
370	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapový	mapový	k2eAgInSc1d1
list	list	k1gInSc1
č.	č.	k?
48	#num#	k4
<g/>
,	,	kIx,
měřítko	měřítko	k1gNnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turistická	turistický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
<g/>
:	:	kIx,
Svratouch	Svratouch	k1gInSc1
a	a	k8xC
okolí	okolí	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapy	mapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1
uveden	uveden	k2eAgInSc1d1
chybně	chybně	k6eAd1
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
„	„	k?
<g/>
O	O	kA
<g/>
"	"	kIx"
v	v	k7c6
názvu	název	k1gInSc6
<g/>
,	,	kIx,
správně	správně	k6eAd1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
<g/>
:	:	kIx,
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
ZM	ZM	kA
ČR	ČR	kA
ČÚZK	ČÚZK	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aplikace	aplikace	k1gFnSc1
MapoMat	MapoMat	k1gInSc1
<g/>
:	:	kIx,
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Obec	obec	k1gFnSc1
Svratouch	Svratouch	k1gInSc1
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc1
ze	z	k7c2
Svratoušské	Svratoušský	k2eAgFnSc2d1
drakiády	drakiáda	k1gFnSc2
27.9	27.9	k4
<g/>
.2015	.2015	k4
(	(	kIx(
<g/>
kopec	kopec	k1gInSc1
Na	na	k7c6
Panenkách	panenka	k1gFnPc6
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Katastrální	katastrální	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
<g/>
:	:	kIx,
Svratouch	Svratouch	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Česká	český	k2eAgFnSc1d1
geologická	geologický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geologické	geologický	k2eAgFnPc4d1
lokality	lokalita	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svratouch	Svratouch	k1gInSc1
-	-	kIx~
ID	ido	k1gNnPc2
2810	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
vrchol	vrchol	k1gInSc1
U	u	k7c2
oběšeného	oběšený	k2eAgNnSc2d1
a	a	k8xC
prameny	pramen	k1gInPc4
řek	řeka	k1gFnPc2
(	(	kIx(
<g/>
Chrudimka	Chrudimka	k1gFnSc1
a	a	k8xC
Krounka	Krounka	k1gFnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc1d1
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evidence	evidence	k1gFnSc1
rozvodnic	rozvodnice	k1gFnPc2
<g/>
:	:	kIx,
kompozice	kompozice	k1gFnSc1
mapy	mapa	k1gFnSc2
s	s	k7c7
vrcholem	vrchol	k1gInSc7
U	u	k7c2
oběšeného	oběšený	k2eAgInSc2d1
(	(	kIx(
<g/>
737	#num#	k4
m	m	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nařízení	nařízení	k1gNnSc4
vlády	vláda	k1gFnSc2
č.	č.	k?
40	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
chráněných	chráněný	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
přirozené	přirozený	k2eAgFnSc2d1
akumulace	akumulace	k1gFnSc2
vod	voda	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Geografický	geografický	k2eAgInSc4d1
ústav	ústav	k1gInSc4
Československé	československý	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměpisný	zeměpisný	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
ČSR	ČSR	kA
<g/>
:	:	kIx,
Hory	hora	k1gFnSc2
a	a	k8xC
nížiny	nížina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Jaromír	Jaromír	k1gMnSc1
Demek	Demek	k1gMnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
584	#num#	k4
s.	s.	k?
Heslo	heslo	k1gNnSc4
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
580	#num#	k4
<g/>
,	,	kIx,
581	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
vrchol	vrchol	k1gInSc1
U	u	k7c2
Oběšeného	oběšený	k2eAgInSc2d1
(	(	kIx(
<g/>
369	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Český	český	k2eAgInSc1d1
úřad	úřad	k1gInSc1
zeměměřický	zeměměřický	k2eAgInSc1d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústřední	ústřední	k2eAgInSc1d1
archiv	archiv	k1gInSc1
zeměměřičství	zeměměřičství	k1gNnSc2
a	a	k8xC
katastru	katastr	k1gInSc2
<g/>
:	:	kIx,
mapování	mapování	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archiválie	archiválie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Český	český	k2eAgInSc1d1
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMS	AMS	kA
Svratouch	Svratoucha	k1gFnPc2
<g/>
:	:	kIx,
počasí	počasí	k1gNnSc1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
ČR	ČR	kA
<g/>
:	:	kIx,
vrchol	vrchol	k1gInSc4
Krásný	krásný	k2eAgInSc4d1
(	(	kIx(
<g/>
616,0	616,0	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
kompozice	kompozice	k1gFnSc1
Čtyřpaličaté	čtyřpaličatý	k2eAgFnSc2d1
černé	černý	k2eAgFnSc2d1
skály	skála	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
kompozice	kompozice	k1gFnSc1
Herálec	Herálec	k1gInSc1
–	–	k?
jihovýchod	jihovýchod	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
kompozice	kompozice	k1gFnSc1
Herálec	Herálec	k1gInSc1
–	–	k?
jihozápad	jihozápad	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
Hradiště	Hradiště	k1gNnSc1
(	(	kIx(
<g/>
682	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
Přední	přední	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
(	(	kIx(
<g/>
693	#num#	k4
m	m	kA
<g/>
,	,	kIx,
TB	TB	kA
26	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
Karlštejn	Karlštejn	k1gInSc1
(	(	kIx(
<g/>
783,4	783,4	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
TB	TB	kA
57	#num#	k4
<g/>
,	,	kIx,
TL	TL	kA
2420	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměměřičský	zeměměřičský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
–	–	k?
Česká	český	k2eAgFnSc1d1
státní	státní	k2eAgFnSc1d1
trigonometrická	trigonometrický	k2eAgFnSc1d1
síť	síť	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
Krásný	krásný	k2eAgInSc1d1
(	(	kIx(
<g/>
616	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k.	k.	k?
ú.	ú.	k?
Krásné	krásný	k2eAgNnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
Louckého	Louckého	k2eAgInSc1d1
kopec	kopec	k1gInSc1
(	(	kIx(
<g/>
700	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
Peškův	Peškův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
717	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
Spálený	spálený	k2eAgInSc1d1
kopec	kopec	k1gInSc1
(	(	kIx(
<g/>
766	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
U	u	k7c2
osla	osel	k1gMnSc2
(	(	kIx(
<g/>
709	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
Vortovský	Vortovský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
(	(	kIx(
<g/>
704,4	704,4	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Česka	Česko	k1gNnSc2
<g/>
:	:	kIx,
Bodová	bodový	k2eAgNnPc1d1
pole	pole	k1gNnPc1
–	–	k?
trigonometrický	trigonometrický	k2eAgInSc4d1
bod	bod	k1gInSc4
V	v	k7c6
krku	krk	k1gInSc6
s	s	k7c7
nivelací	nivelace	k1gFnSc7
699,8	699,8	k4
m	m	kA
n.	n.	k?
m.	m.	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměměřičský	zeměměřičský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Historicky	historicky	k6eAd1
významné	významný	k2eAgInPc4d1
trigonometrické	trigonometrický	k2eAgInPc4d1
body	bod	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
:	:	kIx,
základní	základní	k2eAgInSc1d1
triangulační	triangulační	k2eAgInSc1d1
bod	bod	k1gInSc1
Locus	Locus	k1gInSc1
perennis	perennis	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgInSc4d1
úřad	úřad	k1gInSc4
zeměměřický	zeměměřický	k2eAgInSc4d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInSc1d1
nivelační	nivelační	k2eAgInSc1d1
bod	bod	k1gInSc1
I.	I.	kA
Lišov	Lišov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Českomoravská	českomoravský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
</s>
<s>
Hornosvratecká	Hornosvratecký	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Žďárské	Žďárská	k1gFnPc1
vrchy	vrch	k1gInPc1
</s>
<s>
Kameničská	Kameničský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
</s>
<s>
Sečská	sečský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
</s>
<s>
Železné	železný	k2eAgFnPc1d1
hory	hora	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Český	český	k2eAgInSc1d1
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
meteorologická	meteorologický	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
Svratouch	Svratouch	k1gInSc1
<g/>
,	,	kIx,
počasí	počasí	k1gNnSc1
on-line	on-lin	k1gInSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
