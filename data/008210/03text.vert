<p>
<s>
Organizace	organizace	k1gFnSc2	organizace
zemí	zem	k1gFnPc2	zem
vyvážejících	vyvážející	k2eAgMnPc2d1	vyvážející
ropu	ropa	k1gFnSc4	ropa
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
OPEC	opéct	k5eAaPmRp2nSwK	opéct
z	z	k7c2	z
angl.	angl.	k?	angl.
Organization	Organization	k1gInSc1	Organization
of	of	k?	of
the	the	k?	the
Petroleum	Petroleum	k1gInSc1	Petroleum
Exporting	Exporting	k1gInSc1	Exporting
Countries	Countries	k1gInSc1	Countries
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezivládní	mezivládní	k2eAgFnSc1d1	mezivládní
organizace	organizace	k1gFnSc1	organizace
sdružující	sdružující	k2eAgFnSc1d1	sdružující
14	[number]	k4	14
zemí	zem	k1gFnPc2	zem
exportujících	exportující	k2eAgFnPc2d1	exportující
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
<s>
OPEC	opéct	k5eAaPmRp2nSwK	opéct
koordinuje	koordinovat	k5eAaBmIp3nS	koordinovat
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
politiku	politika	k1gFnSc4	politika
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
s	s	k7c7	s
ropnými	ropný	k2eAgFnPc7d1	ropná
společnostmi	společnost	k1gFnPc7	společnost
ohledně	ohledně	k7c2	ohledně
objemu	objem	k1gInSc2	objem
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
cen	cena	k1gFnPc2	cena
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
organizace	organizace	k1gFnSc2	organizace
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Členské	členský	k2eAgFnPc1d1	členská
země	zem	k1gFnPc1	zem
OPEC	opéct	k5eAaPmRp2nSwK	opéct
dnes	dnes	k6eAd1	dnes
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
75	[number]	k4	75
%	%	kIx~	%
veškerých	veškerý	k3xTgFnPc2	veškerý
světových	světový	k2eAgFnPc2d1	světová
ropných	ropný	k2eAgFnPc2d1	ropná
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
veškeré	veškerý	k3xTgFnSc2	veškerý
ropné	ropný	k2eAgFnSc2d1	ropná
produkce	produkce	k1gFnSc2	produkce
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
polovinu	polovina	k1gFnSc4	polovina
z	z	k7c2	z
objemu	objem	k1gInSc2	objem
vývozu	vývoz	k1gInSc2	vývoz
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Řízení	řízení	k1gNnSc1	řízení
organizace	organizace	k1gFnSc2	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
organizace	organizace	k1gFnSc2	organizace
je	být	k5eAaImIp3nS	být
konference	konference	k1gFnSc1	konference
zástupců	zástupce	k1gMnPc2	zástupce
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
se	se	k3xPyFc4	se
projednávají	projednávat	k5eAaImIp3nP	projednávat
otázky	otázka	k1gFnPc1	otázka
vztahů	vztah	k1gInPc2	vztah
zemí	zem	k1gFnPc2	zem
OPEC	opéct	k5eAaPmRp2nSwK	opéct
s	s	k7c7	s
politickými	politický	k2eAgMnPc7d1	politický
a	a	k8xC	a
ekonomickými	ekonomický	k2eAgMnPc7d1	ekonomický
partnery	partner	k1gMnPc7	partner
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nadcházející	nadcházející	k2eAgNnSc4d1	nadcházející
období	období	k1gNnSc4	období
se	se	k3xPyFc4	se
určují	určovat	k5eAaImIp3nP	určovat
denní	denní	k2eAgFnPc1d1	denní
kvóty	kvóta	k1gFnPc1	kvóta
těžby	těžba	k1gFnSc2	těžba
a	a	k8xC	a
cena	cena	k1gFnSc1	cena
exportované	exportovaný	k2eAgFnSc2d1	exportovaná
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Konference	konference	k1gFnSc1	konference
dále	daleko	k6eAd2	daleko
volí	volit	k5eAaImIp3nS	volit
radu	rada	k1gFnSc4	rada
guvernérů	guvernér	k1gMnPc2	guvernér
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
členské	členský	k2eAgFnSc2d1	členská
země	zem	k1gFnSc2	zem
vyslán	vyslán	k2eAgMnSc1d1	vyslán
jeden	jeden	k4xCgMnSc1	jeden
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Přijetí	přijetí	k1gNnSc1	přijetí
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
jednomyslného	jednomyslný	k2eAgNnSc2d1	jednomyslné
usnesení	usnesení	k1gNnSc2	usnesení
<g/>
.	.	kIx.	.
</s>
<s>
Konference	konference	k1gFnSc1	konference
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
alespoň	alespoň	k9	alespoň
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
orgány	orgán	k1gInPc4	orgán
OPEC	opéct	k5eAaPmRp2nSwK	opéct
patří	patřit	k5eAaImIp3nS	patřit
generální	generální	k2eAgInSc4d1	generální
sekretariát	sekretariát	k1gInSc4	sekretariát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
a	a	k8xC	a
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
komise	komise	k1gFnSc1	komise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
OPEC	opéct	k5eAaPmRp2nSwK	opéct
založilo	založit	k5eAaPmAgNnS	založit
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Bagdádu	Bagdád	k1gInSc6	Bagdád
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1960	[number]	k4	1960
pět	pět	k4xCc1	pět
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
export	export	k1gInSc4	export
ropy	ropa	k1gFnSc2	ropa
výrazně	výrazně	k6eAd1	výrazně
převyšoval	převyšovat	k5eAaImAgInS	převyšovat
domácí	domácí	k2eAgFnSc4d1	domácí
spotřebu	spotřeba	k1gFnSc4	spotřeba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1961	[number]	k4	1961
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Caracasu	Caracas	k1gInSc6	Caracas
přijaty	přijmout	k5eAaPmNgFnP	přijmout
stanovy	stanova	k1gFnPc1	stanova
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
.	.	kIx.	.
</s>
<s>
Zakládající	zakládající	k2eAgFnPc1d1	zakládající
země	zem	k1gFnPc1	zem
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
kartel	kartel	k1gInSc4	kartel
určující	určující	k2eAgInSc4d1	určující
objem	objem	k1gInSc4	objem
a	a	k8xC	a
cenu	cena	k1gFnSc4	cena
exportované	exportovaný	k2eAgFnSc2d1	exportovaná
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pomocí	pomocí	k7c2	pomocí
zavedení	zavedení	k1gNnSc2	zavedení
těžebních	těžební	k2eAgFnPc2d1	těžební
kvót	kvóta	k1gFnPc2	kvóta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kartel	kartel	k1gInSc1	kartel
organizace	organizace	k1gFnSc2	organizace
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
historie	historie	k1gFnSc2	historie
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
-	-	kIx~	-
dokázal	dokázat	k5eAaPmAgMnS	dokázat
členským	členský	k2eAgFnPc3d1	členská
zemím	zem	k1gFnPc3	zem
zajistit	zajistit	k5eAaPmF	zajistit
ohromné	ohromný	k2eAgInPc4d1	ohromný
zisky	zisk	k1gInPc4	zisk
ze	z	k7c2	z
vzrůstajících	vzrůstající	k2eAgFnPc2d1	vzrůstající
cen	cena	k1gFnPc2	cena
ropy	ropa	k1gFnSc2	ropa
(	(	kIx(	(
<g/>
ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
organizace	organizace	k1gFnSc2	organizace
několikanásobně	několikanásobně	k6eAd1	několikanásobně
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
OPEC	opéct	k5eAaPmRp2nSwK	opéct
záměrně	záměrně	k6eAd1	záměrně
snížila	snížit	k5eAaPmAgFnS	snížit
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
(	(	kIx(	(
<g/>
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
její	její	k3xOp3gFnSc4	její
cenu	cena	k1gFnSc4	cena
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
embargo	embargo	k1gNnSc4	embargo
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
ropy	ropa	k1gFnSc2	ropa
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
podporovaly	podporovat	k5eAaImAgFnP	podporovat
Izrael	Izrael	k1gInSc4	Izrael
během	během	k7c2	během
Jomkipurské	Jomkipurský	k2eAgFnSc2d1	Jomkipurská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
USA	USA	kA	USA
a	a	k8xC	a
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
tak	tak	k6eAd1	tak
první	první	k4xOgInSc4	první
a	a	k8xC	a
prozatím	prozatím	k6eAd1	prozatím
největší	veliký	k2eAgInSc1d3	veliký
ropný	ropný	k2eAgInSc1d1	ropný
šok	šok	k1gInSc1	šok
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
na	na	k7c4	na
čtyřnásobek	čtyřnásobek	k1gInSc4	čtyřnásobek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členské	členský	k2eAgFnPc1d1	členská
země	zem	k1gFnPc1	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
(	(	kIx(	(
<g/>
od	od	k7c2	od
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Angola	Angola	k1gFnSc1	Angola
(	(	kIx(	(
<g/>
od	od	k7c2	od
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
(	(	kIx(	(
<g/>
od	od	k7c2	od
1973	[number]	k4	1973
do	do	k7c2	do
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Indonésie	Indonésie	k1gFnSc1	Indonésie
(	(	kIx(	(
<g/>
od	od	k7c2	od
1962	[number]	k4	1962
do	do	k7c2	do
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
suspendaci	suspendace	k1gFnSc3	suspendace
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
znovu	znovu	k6eAd1	znovu
členem	člen	k1gInSc7	člen
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gabon	Gabon	k1gNnSc1	Gabon
(	(	kIx(	(
<g/>
od	od	k7c2	od
1975	[number]	k4	1975
do	do	k7c2	do
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
od	od	k7c2	od
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Irák	Irák	k1gInSc1	Irák
(	(	kIx(	(
<g/>
od	od	k7c2	od
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Írán	Írán	k1gInSc1	Írán
(	(	kIx(	(
<g/>
od	od	k7c2	od
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Katar	katar	k1gInSc1	katar
(	(	kIx(	(
<g/>
od	od	k7c2	od
1961	[number]	k4	1961
do	do	k7c2	do
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Konžská	konžský	k2eAgFnSc1d1	Konžská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
od	od	k7c2	od
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
(	(	kIx(	(
<g/>
od	od	k7c2	od
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Libye	Libye	k1gFnSc1	Libye
(	(	kIx(	(
<g/>
od	od	k7c2	od
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nigérie	Nigérie	k1gFnSc1	Nigérie
(	(	kIx(	(
<g/>
od	od	k7c2	od
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
Guinea	Guinea	k1gFnSc1	Guinea
(	(	kIx(	(
<g/>
od	od	k7c2	od
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
(	(	kIx(	(
<g/>
od	od	k7c2	od
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
arabské	arabský	k2eAgInPc1d1	arabský
emiráty	emirát	k1gInPc1	emirát
(	(	kIx(	(
<g/>
od	od	k7c2	od
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Venezuela	Venezuela	k1gFnSc1	Venezuela
(	(	kIx(	(
<g/>
od	od	k7c2	od
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Potenciální	potenciální	k2eAgFnSc2d1	potenciální
členské	členský	k2eAgFnSc2d1	členská
země	zem	k1gFnSc2	zem
==	==	k?	==
</s>
</p>
<p>
<s>
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g/>
,	,	kIx,	,
Súdán	Súdán	k1gInSc1	Súdán
a	a	k8xC	a
Sýrie	Sýrie	k1gFnSc1	Sýrie
byly	být	k5eAaImAgFnP	být
přizvány	přizvat	k5eAaPmNgFnP	přizvat
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
OPEC	opéct	k5eAaPmRp2nSwK	opéct
<g/>
.	.	kIx.	.
</s>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
členství	členství	k1gNnSc2	členství
zvažuje	zvažovat	k5eAaImIp3nS	zvažovat
kvůli	kvůli	k7c3	kvůli
nově	nově	k6eAd1	nově
nalezeným	nalezený	k2eAgFnPc3d1	nalezená
zásobám	zásoba	k1gFnPc3	zásoba
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
Atlantiku	Atlantik	k1gInSc6	Atlantik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Organizace	organizace	k1gFnSc2	organizace
zemí	zem	k1gFnPc2	zem
vyvážejících	vyvážející	k2eAgFnPc2d1	vyvážející
ropu	ropa	k1gFnSc4	ropa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
OPEC	opéct	k5eAaPmRp2nSwK	opéct
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc5	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
internetové	internetový	k2eAgFnPc1d1	internetová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
