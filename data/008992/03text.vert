<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
automobilová	automobilový	k2eAgFnSc1d1	automobilová
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
FIA	FIA	kA	FIA
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Fédération	Fédération	k1gInSc1	Fédération
Internationale	Internationale	k1gFnSc2	Internationale
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Automobile	automobil	k1gInSc6	automobil
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
národní	národní	k2eAgFnPc4d1	národní
automobilové	automobilový	k2eAgFnPc4d1	automobilová
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
přejmenováním	přejmenování	k1gNnSc7	přejmenování
AIACR	AIACR	kA	AIACR
(	(	kIx(	(
<g/>
Association	Association	k1gInSc1	Association
Internationale	Internationale	k1gMnSc2	Internationale
des	des	k1gNnSc2	des
Automobiles	Automobiles	k1gMnSc1	Automobiles
–	–	k?	–
Clubs	Clubs	k1gInSc1	Clubs
Reconnus	Reconnus	k1gInSc1	Reconnus
<g/>
)	)	kIx)	)
založené	založený	k2eAgNnSc1d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
FIA	FIA	kA	FIA
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
organizací	organizace	k1gFnPc2	organizace
SportAccord	SportAccord	k1gInSc1	SportAccord
a	a	k8xC	a
ARISF	ARISF	kA	ARISF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Československo	Československo	k1gNnSc1	Československo
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
jeho	jeho	k3xOp3gInSc4	jeho
autoklub	autoklub	k1gInSc4	autoklub
<g/>
)	)	kIx)	)
se	s	k7c7	s
členem	člen	k1gMnSc7	člen
FIA	FIA	kA	FIA
(	(	kIx(	(
<g/>
AIACR	AIACR	kA	AIACR
<g/>
)	)	kIx)	)
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Široké	Široké	k2eAgFnSc3d1	Široké
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
FIA	FIA	kA	FIA
známá	známá	k1gFnSc1	známá
především	především	k9	především
jako	jako	k8xC	jako
řídící	řídící	k2eAgInSc1d1	řídící
orgán	orgán	k1gInSc1	orgán
mnoha	mnoho	k4c2	mnoho
automobilových	automobilový	k2eAgInPc2d1	automobilový
závodů	závod	k1gInPc2	závod
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
konání	konání	k1gNnSc6	konání
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
arbitr	arbitr	k1gMnSc1	arbitr
<g/>
,	,	kIx,	,
také	také	k9	také
spravuje	spravovat	k5eAaImIp3nS	spravovat
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
pravidla	pravidlo	k1gNnPc4	pravidlo
a	a	k8xC	a
předpisy	předpis	k1gInPc4	předpis
těchto	tento	k3xDgInPc2	tento
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Federace	federace	k1gFnSc1	federace
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c6	na
Náměstí	náměstí	k1gNnSc6	náměstí
Svornosti	svornost	k1gFnSc2	svornost
8	[number]	k4	8
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
Place	plac	k1gInSc6	plac
de	de	k?	de
la	la	k1gNnSc2	la
Concorde	Concord	k1gInSc5	Concord
8	[number]	k4	8
<g/>
,	,	kIx,	,
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
227	[number]	k4	227
národních	národní	k2eAgFnPc2d1	národní
motoristických	motoristický	k2eAgFnPc2d1	motoristická
a	a	k8xC	a
sportovních	sportovní	k2eAgFnPc2d1	sportovní
organizací	organizace	k1gFnPc2	organizace
ze	z	k7c2	z
132	[number]	k4	132
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
členské	členský	k2eAgInPc1d1	členský
kluby	klub	k1gInPc1	klub
představují	představovat	k5eAaImIp3nP	představovat
miliony	milion	k4xCgInPc4	milion
motoristů	motorista	k1gMnPc2	motorista
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
rodinami	rodina	k1gFnPc7	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIA	FIA	kA	FIA
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
práva	právo	k1gNnSc2	právo
motoristů	motorista	k1gMnPc2	motorista
a	a	k8xC	a
motoristických	motoristický	k2eAgFnPc2d1	motoristická
organizací	organizace	k1gFnPc2	organizace
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
aktivních	aktivní	k2eAgFnPc2d1	aktivní
kampaní	kampaň	k1gFnPc2	kampaň
hájí	hájit	k5eAaImIp3nS	hájit
jejich	jejich	k3xOp3gInPc4	jejich
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
důležitým	důležitý	k2eAgFnPc3d1	důležitá
otázkám	otázka	k1gFnPc3	otázka
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
,	,	kIx,	,
mobilita	mobilita	k1gFnSc1	mobilita
<g/>
,	,	kIx,	,
životní	životní	k2eAgNnPc4d1	životní
prostředí	prostředí	k1gNnPc4	prostředí
a	a	k8xC	a
spotřebitelská	spotřebitelský	k2eAgNnPc4d1	spotřebitelské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
zájmy	zájem	k1gInPc4	zájem
motoristů	motorista	k1gMnPc2	motorista
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
asociace	asociace	k1gFnSc2	asociace
FIFA	FIFA	kA	FIFA
je	být	k5eAaImIp3nS	být
FIA	FIA	kA	FIA
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
svým	svůj	k3xOyFgInSc7	svůj
francouzským	francouzský	k2eAgInSc7d1	francouzský
názvem	název	k1gInSc7	název
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
zkratkou	zkratka	k1gFnSc7	zkratka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nefrankofonních	frankofonní	k2eNgFnPc6d1	frankofonní
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
občas	občas	k6eAd1	občas
název	název	k1gInSc1	název
přeložen	přeložit	k5eAaPmNgInS	přeložit
jako	jako	k8xS	jako
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
automobilová	automobilový	k2eAgFnSc1d1	automobilová
federace	federace	k1gFnSc1	federace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Organizační	organizační	k2eAgFnSc1d1	organizační
struktura	struktura	k1gFnSc1	struktura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
===	===	k?	===
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
řídícím	řídící	k2eAgInSc7d1	řídící
orgánem	orgán	k1gInSc7	orgán
FIA	FIA	kA	FIA
je	být	k5eAaImIp3nS	být
valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
prezidentů	prezident	k1gMnPc2	prezident
mnoha	mnoho	k4c2	mnoho
členských	členský	k2eAgInPc2d1	členský
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Řádná	řádný	k2eAgFnSc1d1	řádná
valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
a	a	k8xC	a
hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
o	o	k7c6	o
zprávách	zpráva	k1gFnPc6	zpráva
a	a	k8xC	a
návrzích	návrh	k1gInPc6	návrh
světové	světový	k2eAgFnSc2d1	světová
rady	rada	k1gFnSc2	rada
FIA	FIA	kA	FIA
<g/>
,	,	kIx,	,
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
rozpočtu	rozpočet	k1gInSc6	rozpočet
a	a	k8xC	a
účetnictví	účetnictví	k1gNnSc6	účetnictví
a	a	k8xC	a
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
sportovní	sportovní	k2eAgInSc4d1	sportovní
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Šéfem	šéf	k1gMnSc7	šéf
FIA	FIA	kA	FIA
a	a	k8xC	a
valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
je	být	k5eAaImIp3nS	být
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
a	a	k8xC	a
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
nemůže	moct	k5eNaImIp3nS	moct
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
strávit	strávit	k5eAaPmF	strávit
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
dvě	dva	k4xCgNnPc4	dva
funkční	funkční	k2eAgNnPc4d1	funkční
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
tento	tento	k3xDgInSc4	tento
post	post	k1gInSc4	post
zastává	zastávat	k5eAaImIp3nS	zastávat
Francouz	Francouz	k1gMnSc1	Francouz
Jean	Jean	k1gMnSc1	Jean
Todt	Todt	k1gMnSc1	Todt
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
předsedy	předseda	k1gMnSc2	předseda
je	být	k5eAaImIp3nS	být
také	také	k9	také
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jeho	jeho	k3xOp3gMnSc1	jeho
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
senátu	senát	k1gInSc2	senát
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
generálním	generální	k2eAgMnSc7d1	generální
tajemníkem	tajemník	k1gMnSc7	tajemník
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
odvolacího	odvolací	k2eAgInSc2d1	odvolací
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
volbě	volba	k1gFnSc6	volba
místopředsedů	místopředseda	k1gMnPc2	místopředseda
a	a	k8xC	a
členů	člen	k1gMnPc2	člen
světových	světový	k2eAgFnPc2d1	světová
rad	rada	k1gFnPc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Valná	valný	k2eAgFnSc1d1	valná
hromada	hromada	k1gFnSc1	hromada
se	se	k3xPyFc4	se
schází	scházet	k5eAaImIp3nS	scházet
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
na	na	k7c6	na
konci	konec	k1gInSc6	konec
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
je	být	k5eAaImIp3nS	být
svoláno	svolán	k2eAgNnSc1d1	svoláno
mimořádné	mimořádný	k2eAgNnSc1d1	mimořádné
zasedání	zasedání	k1gNnSc1	zasedání
k	k	k7c3	k
projednání	projednání	k1gNnSc3	projednání
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
celé	celý	k2eAgNnSc4d1	celé
členství	členství	k1gNnSc4	členství
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
počkat	počkat	k5eAaPmF	počkat
až	až	k9	až
do	do	k7c2	do
příští	příští	k2eAgFnSc2d1	příští
schůze	schůze	k1gFnSc2	schůze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Světová	světový	k2eAgFnSc1d1	světová
rada	rada	k1gFnSc1	rada
===	===	k?	===
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
Světová	světový	k2eAgFnSc1d1	světová
rada	rada	k1gFnSc1	rada
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
valnou	valný	k2eAgFnSc7d1	valná
hromadou	hromada	k1gFnSc7	hromada
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
předsedá	předsedat	k5eAaImIp3nS	předsedat
prezident	prezident	k1gMnSc1	prezident
FIA	FIA	kA	FIA
za	za	k7c4	za
pomoci	pomoc	k1gFnSc6	pomoc
odborných	odborný	k2eAgFnPc6d1	odborná
komisích	komise	k1gFnPc6	komise
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
komise	komise	k1gFnPc1	komise
jednají	jednat	k5eAaImIp3nP	jednat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k8xC	jako
resorty	resort	k1gInPc1	resort
národních	národní	k2eAgFnPc2d1	národní
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
rada	rada	k1gFnSc1	rada
motoristického	motoristický	k2eAgInSc2d1	motoristický
sportu	sport	k1gInSc2	sport
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
všechny	všechen	k3xTgFnPc4	všechen
sportovní	sportovní	k2eAgFnPc4d1	sportovní
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
FIA	FIA	kA	FIA
pořádá	pořádat	k5eAaImIp3nS	pořádat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
propagaci	propagace	k1gFnSc4	propagace
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
prosazení	prosazení	k1gNnSc4	prosazení
standardizovaných	standardizovaný	k2eAgNnPc2d1	standardizované
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
motorsport	motorsport	k1gInSc4	motorsport
na	na	k7c6	na
nových	nový	k2eAgInPc6d1	nový
trzích	trh	k1gInPc6	trh
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
prezidenta	prezident	k1gMnSc2	prezident
FIA	FIA	kA	FIA
<g/>
,	,	kIx,	,
náměstka	náměstek	k1gMnSc2	náměstek
<g/>
,	,	kIx,	,
sedmi	sedm	k4xCc2	sedm
viceprezidentů	viceprezident	k1gMnPc2	viceprezident
a	a	k8xC	a
17	[number]	k4	17
dalších	další	k2eAgMnPc2d1	další
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Senát	senát	k1gInSc1	senát
===	===	k?	===
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
orgánem	orgán	k1gInSc7	orgán
FIA	FIA	kA	FIA
je	být	k5eAaImIp3nS	být
senát	senát	k1gInSc1	senát
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
přijímá	přijímat	k5eAaImIp3nS	přijímat
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
o	o	k7c6	o
ústředním	ústřední	k2eAgNnSc6d1	ústřední
vedení	vedení	k1gNnSc6	vedení
FIA	FIA	kA	FIA
a	a	k8xC	a
případných	případný	k2eAgInPc6d1	případný
finančních	finanční	k2eAgInPc6d1	finanční
problémech	problém	k1gInPc6	problém
<g/>
.	.	kIx.	.
</s>
<s>
Desetičlenný	desetičlenný	k2eAgInSc1d1	desetičlenný
senát	senát	k1gInSc1	senát
FIA	FIA	kA	FIA
tvoří	tvořit	k5eAaImIp3nS	tvořit
předseda	předseda	k1gMnSc1	předseda
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
současní	současný	k2eAgMnPc1d1	současný
i	i	k8xC	i
předchozí	předchozí	k2eAgMnPc1d1	předchozí
prezidenti	prezident	k1gMnPc1	prezident
FIA	FIA	kA	FIA
<g/>
,	,	kIx,	,
náměstci	náměstek	k1gMnPc1	náměstek
FIA	FIA	kA	FIA
zastupující	zastupující	k2eAgInPc4d1	zastupující
různé	různý	k2eAgInPc4d1	různý
oddíly	oddíl	k1gInPc4	oddíl
a	a	k8xC	a
pět	pět	k4xCc4	pět
dalších	další	k2eAgInPc2d1	další
členů	člen	k1gInPc2	člen
zvolených	zvolený	k2eAgInPc2d1	zvolený
valnou	valný	k2eAgFnSc7d1	valná
hromadou	hromada	k1gFnSc7	hromada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Odvolací	odvolací	k2eAgInSc1d1	odvolací
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
ICA	ICA	kA	ICA
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
činnosti	činnost	k1gFnSc2	činnost
FIA	FIA	kA	FIA
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
odvolací	odvolací	k2eAgInSc1d1	odvolací
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nezávislým	závislý	k2eNgInSc7d1	nezávislý
orgánem	orgán	k1gInSc7	orgán
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
finální	finální	k2eAgInSc1d1	finální
odvolací	odvolací	k2eAgInSc1d1	odvolací
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Řeší	řešit	k5eAaImIp3nP	řešit
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gNnSc3	on
předloženy	předložen	k2eAgInPc1d1	předložen
některou	některý	k3yIgFnSc7	některý
z	z	k7c2	z
národních	národní	k2eAgFnPc2d1	národní
motoristických	motoristický	k2eAgFnPc2d1	motoristická
organizací	organizace	k1gFnPc2	organizace
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
nebo	nebo	k8xC	nebo
prezidentem	prezident	k1gMnSc7	prezident
FIA	FIA	kA	FIA
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
pravomoc	pravomoc	k1gFnSc4	pravomoc
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
ve	v	k7c6	v
sporech	spor	k1gInPc6	spor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
motorsportu	motorsport	k1gInSc3	motorsport
netýkají	týkat	k5eNaImIp3nP	týkat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byly	být	k5eAaImAgInP	být
mu	on	k3xPp3gMnSc3	on
předloženy	předložit	k5eAaPmNgFnP	předložit
některou	některý	k3yIgFnSc7	některý
z	z	k7c2	z
národních	národní	k2eAgFnPc2d1	národní
organizací	organizace	k1gFnPc2	organizace
a	a	k8xC	a
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
FIA	FIA	kA	FIA
<g/>
.	.	kIx.	.
</s>
<s>
ICA	ICA	kA	ICA
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
18	[number]	k4	18
titulárních	titulární	k2eAgInPc2d1	titulární
členů	člen	k1gInPc2	člen
různých	různý	k2eAgFnPc2d1	různá
národností	národnost	k1gFnPc2	národnost
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgInS	přidat
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
zástupců	zástupce	k1gMnPc2	zástupce
členů	člen	k1gInPc2	člen
stejné	stejný	k2eAgFnSc2d1	stejná
národnosti	národnost	k1gFnSc2	národnost
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
titulárních	titulární	k2eAgInPc2d1	titulární
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
valnou	valný	k2eAgFnSc7d1	valná
hromadou	hromada	k1gFnSc7	hromada
a	a	k8xC	a
pracují	pracovat	k5eAaImIp3nP	pracovat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
vyměněna	vyměněn	k2eAgFnSc1d1	vyměněna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
zachování	zachování	k1gNnSc4	zachování
nezávislosti	nezávislost	k1gFnSc2	nezávislost
soudu	soud	k1gInSc2	soud
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
žádný	žádný	k3yNgMnSc1	žádný
člen	člen	k1gMnSc1	člen
zároveň	zároveň	k6eAd1	zároveň
členem	člen	k1gMnSc7	člen
světové	světový	k2eAgFnSc2d1	světová
rady	rada	k1gFnSc2	rada
motoristického	motoristický	k2eAgInSc2d1	motoristický
sportu	sport	k1gInSc2	sport
nebo	nebo	k8xC	nebo
působit	působit	k5eAaImF	působit
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
sportovních	sportovní	k2eAgFnPc2d1	sportovní
komisí	komise	k1gFnPc2	komise
FIA	FIA	kA	FIA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
orgány	orgán	k1gInPc1	orgán
FIA	FIA	kA	FIA
jsou	být	k5eAaImIp3nP	být
administrativně	administrativně	k6eAd1	administrativně
podporovány	podporován	k2eAgFnPc1d1	podporována
sekretariátem	sekretariát	k1gInSc7	sekretariát
FIA	FIA	kA	FIA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
FIA	FIA	kA	FIA
komisaři	komisar	k1gMnPc1	komisar
===	===	k?	===
</s>
</p>
<p>
<s>
Zvolení	zvolený	k2eAgMnPc1d1	zvolený
komisaři	komisar	k1gMnPc1	komisar
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
FIA	FIA	kA	FIA
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
pozice	pozice	k1gFnPc1	pozice
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
dobrovolné	dobrovolný	k2eAgNnSc4d1	dobrovolné
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gMnSc2	jejich
předsedy	předseda	k1gMnSc2	předseda
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
bezplatné	bezplatný	k2eAgNnSc4d1	bezplatné
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
denní	denní	k2eAgFnSc4d1	denní
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
sekretariátem	sekretariát	k1gInSc7	sekretariát
FIA	FIA	kA	FIA
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
komisaři	komisar	k1gMnPc1	komisar
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
<g/>
,	,	kIx,	,
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
ale	ale	k8xC	ale
ne	ne	k9	ne
výhradně	výhradně	k6eAd1	výhradně
<g/>
,	,	kIx,	,
ze	z	k7c2	z
členů	člen	k1gMnPc2	člen
FIA	FIA	kA	FIA
a	a	k8xC	a
členských	členský	k2eAgInPc2d1	členský
klubů	klub	k1gInPc2	klub
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pevných	pevný	k2eAgFnPc2d1	pevná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šampionáty	šampionát	k1gInPc1	šampionát
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
FIA	FIA	kA	FIA
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
hlavní	hlavní	k2eAgInSc1d1	hlavní
orgán	orgán	k1gInSc1	orgán
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
motorsportu	motorsport	k1gInSc2	motorsport
sleduje	sledovat	k5eAaImIp3nS	sledovat
FIA	FIA	kA	FIA
tyto	tento	k3xDgInPc4	tento
hlavní	hlavní	k2eAgInPc4d1	hlavní
cíle	cíl	k1gInPc4	cíl
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podpora	podpora	k1gFnSc1	podpora
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
zlepšujících	zlepšující	k2eAgInPc2d1	zlepšující
bezpečtnostních	bezpečtnostní	k2eAgInPc2d1	bezpečtnostní
standardů	standard	k1gInPc2	standard
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
motorsportu	motorsport	k1gInSc2	motorsport
</s>
</p>
<p>
<s>
efektivní	efektivní	k2eAgFnSc1d1	efektivní
správa	správa	k1gFnSc1	správa
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
motoristického	motoristický	k2eAgInSc2d1	motoristický
sportu	sport	k1gInSc2	sport
</s>
</p>
<p>
<s>
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
přijetí	přijetí	k1gNnSc6	přijetí
jednotných	jednotný	k2eAgNnPc2d1	jednotné
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
druhy	druh	k1gMnPc4	druh
motoristických	motoristický	k2eAgInPc2d1	motoristický
sportů	sport	k1gInPc2	sport
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
</s>
</p>
<p>
<s>
podpora	podpora	k1gFnSc1	podpora
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc1	rozvoj
motorsportu	motorsport	k1gInSc2	motorsport
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
úrovníchProstřednictvím	úrovníchProstřednictví	k1gNnSc7	úrovníchProstřednictví
sítě	síť	k1gFnSc2	síť
národních	národní	k2eAgInPc2d1	národní
členských	členský	k2eAgInPc2d1	členský
klubů	klub	k1gInPc2	klub
se	se	k3xPyFc4	se
vliv	vliv	k1gInSc1	vliv
FIA	FIA	kA	FIA
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
miliony	milion	k4xCgInPc4	milion
amatérů	amatér	k1gMnPc2	amatér
i	i	k8xC	i
profesionálů	profesionál	k1gMnPc2	profesionál
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
motoristický	motoristický	k2eAgInSc4d1	motoristický
sport	sport	k1gInSc4	sport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
FIA	FIA	kA	FIA
pořádá	pořádat	k5eAaImIp3nS	pořádat
patří	patřit	k5eAaImIp3nP	patřit
závody	závod	k1gInPc1	závod
formule	formule	k1gFnSc2	formule
1	[number]	k4	1
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
rallye	rallye	k1gNnSc6	rallye
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
i	i	k9	i
šampionát	šampionát	k1gInSc1	šampionát
cestovních	cestovní	k2eAgInPc2d1	cestovní
vozů	vůz	k1gInPc2	vůz
či	či	k8xC	či
závody	závod	k1gInPc1	závod
formule	formule	k1gFnSc2	formule
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
FIA	FIA	kA	FIA
pořádá	pořádat	k5eAaImIp3nS	pořádat
celkem	celkem	k6eAd1	celkem
29	[number]	k4	29
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
i	i	k8xC	i
místních	místní	k2eAgInPc2d1	místní
šampionátů	šampionát	k1gInPc2	šampionát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
FIA	FIA	kA	FIA
Institue	Institue	k1gInSc1	Institue
Young	Young	k1gInSc1	Young
Drivers	Drivers	k1gInSc4	Drivers
Excellence	Excellence	k1gFnSc2	Excellence
Academy	Academa	k1gFnSc2	Academa
==	==	k?	==
</s>
</p>
<p>
<s>
FIA	FIA	kA	FIA
Institute	institut	k1gInSc5	institut
Young	Young	k1gMnSc1	Young
Driver	driver	k1gInSc4	driver
Excellence	Excellence	k1gFnSc2	Excellence
Academy	Academa	k1gFnSc2	Academa
(	(	kIx(	(
<g/>
akademie	akademie	k1gFnSc2	akademie
pro	pro	k7c4	pro
mladé	mladý	k2eAgMnPc4d1	mladý
talentované	talentovaný	k2eAgMnPc4d1	talentovaný
piloty	pilot	k1gMnPc4	pilot
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
program	program	k1gInSc1	program
založen	založit	k5eAaPmNgInS	založit
pro	pro	k7c4	pro
propagaci	propagace	k1gFnSc4	propagace
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
v	v	k7c6	v
motorsportu	motorsport	k1gInSc6	motorsport
a	a	k8xC	a
udržitelný	udržitelný	k2eAgInSc1d1	udržitelný
rozvoj	rozvoj	k1gInSc1	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
talentu	talent	k1gInSc2	talent
mladých	mladý	k2eAgMnPc2d1	mladý
pilotů	pilot	k1gMnPc2	pilot
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
12	[number]	k4	12
uchazečů	uchazeč	k1gMnPc2	uchazeč
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
30	[number]	k4	30
mladých	mladý	k2eAgMnPc2d1	mladý
jezdců	jezdec	k1gMnPc2	jezdec
vybráno	vybrat	k5eAaPmNgNnS	vybrat
18	[number]	k4	18
a	a	k8xC	a
pozváno	pozván	k2eAgNnSc4d1	pozváno
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
ročníku	ročník	k1gInSc6	ročník
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
jsou	být	k5eAaImIp3nP	být
vyškoleni	vyškolen	k2eAgMnPc1d1	vyškolen
profesionálními	profesionální	k2eAgInPc7d1	profesionální
závodními	závodní	k2eAgInPc7d1	závodní
jezdci	jezdec	k1gInPc7	jezdec
rallye	rallye	k1gFnSc2	rallye
Alexanderem	Alexander	k1gMnSc7	Alexander
Wurzem	Wurz	k1gMnSc7	Wurz
a	a	k8xC	a
Robertem	Robert	k1gMnSc7	Robert
Reidem	Reid	k1gMnSc7	Reid
s	s	k7c7	s
workshopy	workshop	k1gInPc7	workshop
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Autoklub	autoklub	k1gInSc1	autoklub
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
automobilová	automobilový	k2eAgFnSc1d1	automobilová
federace	federace	k1gFnSc1	federace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
FIA	FIA	kA	FIA
</s>
</p>
<p>
<s>
50	[number]	k4	50
<g/>
by	by	k9	by
<g/>
50	[number]	k4	50
<g/>
:	:	kIx,	:
Global	globat	k5eAaImAgInS	globat
Fuel	Fuel	k1gInSc4	Fuel
Economy	Econom	k1gInPc4	Econom
Initiative	Initiativ	k1gInSc5	Initiativ
</s>
</p>
<p>
<s>
Fédération	Fédération	k1gInSc1	Fédération
Internationale	Internationale	k1gFnSc2	Internationale
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Automobile	automobil	k1gInSc6	automobil
at	at	k?	at
Notable	Notable	k1gMnSc1	Notable
Names	Names	k1gMnSc1	Names
Database	Databasa	k1gFnSc3	Databasa
</s>
</p>
