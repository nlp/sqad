<s>
Úsečka	úsečka	k1gFnSc1	úsečka
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
přímky	přímka	k1gFnSc2	přímka
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Určující	určující	k2eAgInPc1d1	určující
body	bod	k1gInPc1	bod
úsečky	úsečka	k1gFnSc2	úsečka
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
krajní	krajní	k2eAgInPc1d1	krajní
body	bod	k1gInPc1	bod
úsečky	úsečka	k1gFnSc2	úsečka
<g/>
.	.	kIx.	.
</s>
<s>
Úsečka	úsečka	k1gFnSc1	úsečka
se	se	k3xPyFc4	se
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
rovnou	rovnou	k6eAd1	rovnou
čarou	čára	k1gFnSc7	čára
mezi	mezi	k7c7	mezi
jejími	její	k3xOp3gInPc7	její
krajními	krajní	k2eAgInPc7d1	krajní
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
svých	svůj	k3xOyFgInPc2	svůj
krajních	krajní	k2eAgInPc2d1	krajní
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
úsečky	úsečka	k1gFnSc2	úsečka
neboli	neboli	k8xC	neboli
délka	délka	k1gFnSc1	délka
úsečky	úsečka	k1gFnSc2	úsečka
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
pomocí	pomocí	k7c2	pomocí
dvou	dva	k4xCgFnPc2	dva
svislých	svislý	k2eAgFnPc2d1	svislá
čar	čára	k1gFnPc2	čára
(	(	kIx(	(
<g/>
rovné	rovný	k2eAgFnSc2d1	rovná
závorky	závorka	k1gFnSc2	závorka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
|	|	kIx~	|
<g/>
AB	AB	kA	AB
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Střed	střed	k1gInSc1	střed
úsečky	úsečka	k1gFnSc2	úsečka
je	být	k5eAaImIp3nS	být
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
úsečce	úsečka	k1gFnSc6	úsečka
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc1	jehož
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
od	od	k7c2	od
obou	dva	k4xCgInPc2	dva
krajních	krajní	k2eAgInPc2d1	krajní
bodů	bod	k1gInPc2	bod
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Osa	osa	k1gFnSc1	osa
úsečky	úsečka	k1gFnSc2	úsečka
je	být	k5eAaImIp3nS	být
přímka	přímka	k1gFnSc1	přímka
kolmá	kolmý	k2eAgFnSc1d1	kolmá
k	k	k7c3	k
úsečce	úsečka	k1gFnSc3	úsečka
procházející	procházející	k2eAgInSc1d1	procházející
jejím	její	k3xOp3gInSc7	její
středem	střed	k1gInSc7	střed
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
body	bod	k1gInPc1	bod
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
úsečky	úsečka	k1gFnSc2	úsečka
mají	mít	k5eAaImIp3nP	mít
od	od	k7c2	od
obou	dva	k4xCgInPc2	dva
krajních	krajní	k2eAgInPc2d1	krajní
bodů	bod	k1gInPc2	bod
stejnou	stejný	k2eAgFnSc4d1	stejná
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Úsečka	úsečka	k1gFnSc1	úsečka
je	být	k5eAaImIp3nS	být
středově	středově	k6eAd1	středově
souměrná	souměrný	k2eAgFnSc1d1	souměrná
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
Úsečka	úsečka	k1gFnSc1	úsečka
je	být	k5eAaImIp3nS	být
osově	osově	k6eAd1	osově
souměrná	souměrný	k2eAgFnSc1d1	souměrná
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc1	dva
osy	osa	k1gFnPc1	osa
souměrnosti	souměrnost	k1gFnPc1	souměrnost
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
osa	osa	k1gFnSc1	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
je	být	k5eAaImIp3nS	být
osa	osa	k1gFnSc1	osa
úsečky	úsečka	k1gFnSc2	úsečka
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc7	druhý
osou	osa	k1gFnSc7	osa
souměrnosti	souměrnost	k1gFnSc2	souměrnost
je	být	k5eAaImIp3nS	být
přímka	přímka	k1gFnSc1	přímka
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
úsečka	úsečka	k1gFnSc1	úsečka
leží	ležet	k5eAaImIp3nS	ležet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
bývá	bývat	k5eAaImIp3nS	bývat
bod	bod	k1gInSc1	bod
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
úsečku	úsečka	k1gFnSc4	úsečka
s	s	k7c7	s
nulovou	nulový	k2eAgFnSc7d1	nulová
délkou	délka	k1gFnSc7	délka
<g/>
.	.	kIx.	.
</s>
<s>
Úsečku	úsečka	k1gFnSc4	úsečka
lze	lze	k6eAd1	lze
také	také	k9	také
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
průnik	průnik	k1gInSc4	průnik
dvou	dva	k4xCgFnPc2	dva
opačných	opačný	k2eAgFnPc2d1	opačná
polopřímek	polopřímka	k1gFnPc2	polopřímka
<g/>
.	.	kIx.	.
</s>
<s>
Matematika	matematika	k1gFnSc1	matematika
Geometrie	geometrie	k1gFnSc1	geometrie
Lineární	lineární	k2eAgFnPc4d1	lineární
geometrické	geometrický	k2eAgFnPc4d1	geometrická
útvary	útvar	k1gInPc1	útvar
Přímka	přímka	k1gFnSc1	přímka
Interval	interval	k1gInSc1	interval
Hypocykloida	hypocykloida	k1gFnSc1	hypocykloida
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
úsečka	úsečka	k1gFnSc1	úsečka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
úsečka	úsečka	k1gFnSc1	úsečka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
