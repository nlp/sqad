<s>
Historie	historie	k1gFnSc1	historie
psacího	psací	k2eAgInSc2d1	psací
stroje	stroj	k1gInSc2	stroj
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1714	[number]	k4	1714
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Henry	Henry	k1gMnSc1	Henry
Mill	Mill	k1gMnSc1	Mill
patentoval	patentovat	k5eAaBmAgMnS	patentovat
jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgFnSc4	první
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
