<s>
Iridium	iridium	k1gNnSc1	iridium
je	být	k5eAaImIp3nS	být
drahý	drahý	k2eAgInSc1d1	drahý
kov	kov	k1gInSc1	kov
šedivě	šedivě	k6eAd1	šedivě
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
dostalo	dostat	k5eAaPmAgNnS	dostat
podle	podle	k7c2	podle
duhového	duhový	k2eAgInSc2d1	duhový
odrazu	odraz	k1gInSc2	odraz
povrchu	povrch	k1gInSc2	povrch
svých	svůj	k3xOyFgFnPc2	svůj
sloučenin	sloučenina	k1gFnPc2	sloučenina
(	(	kIx(	(
<g/>
duha	duha	k1gFnSc1	duha
je	být	k5eAaImIp3nS	být
latinsky	latinsky	k6eAd1	latinsky
iris	iris	k1gInSc4	iris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Iridium	iridium	k1gNnSc1	iridium
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
roku	rok	k1gInSc2	rok
1803	[number]	k4	1803
Smithsonem	Smithson	k1gMnSc7	Smithson
Tennantem	Tennant	k1gMnSc7	Tennant
(	(	kIx(	(
<g/>
1761	[number]	k4	1761
<g/>
-	-	kIx~	-
<g/>
1815	[number]	k4	1815
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ušlechtilý	ušlechtilý	k2eAgMnSc1d1	ušlechtilý
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
i	i	k9	i
když	když	k8xS	když
křehký	křehký	k2eAgInSc1d1	křehký
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
elektricky	elektricky	k6eAd1	elektricky
i	i	k9	i
tepelně	tepelně	k6eAd1	tepelně
středně	středně	k6eAd1	středně
dobře	dobře	k6eAd1	dobře
vodivý	vodivý	k2eAgInSc1d1	vodivý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
supravodičem	supravodič	k1gInSc7	supravodič
I	i	k8xC	i
typu	typ	k1gInSc2	typ
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
0,112	[number]	k4	0,112
K.	K.	kA	K.
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
téměř	téměř	k6eAd1	téměř
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
ryzí	ryzí	k2eAgInSc1d1	ryzí
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
míst	místo	k1gNnPc2	místo
dopadu	dopad	k1gInSc2	dopad
meteoritů	meteorit	k1gInPc2	meteorit
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
společně	společně	k6eAd1	společně
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
drahými	drahý	k2eAgInPc7d1	drahý
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
platinovými	platinový	k2eAgInPc7d1	platinový
kovy	kov	k1gInPc7	kov
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
ultrabazických	ultrabazický	k2eAgInPc6d1	ultrabazický
masivech	masiv	k1gInPc6	masiv
a	a	k8xC	a
díky	díky	k7c3	díky
chemické	chemický	k2eAgFnSc3d1	chemická
stálosti	stálost	k1gFnSc3	stálost
se	se	k3xPyFc4	se
koncentruje	koncentrovat	k5eAaBmIp3nS	koncentrovat
v	v	k7c6	v
náplavech	náplav	k1gInPc6	náplav
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
naleziště	naleziště	k1gNnSc1	naleziště
představuje	představovat	k5eAaImIp3nS	představovat
Sibiř	Sibiř	k1gFnSc1	Sibiř
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k6eAd1wR	povrch
jeho	jeho	k3xOp3gFnPc1	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
duhový	duhový	k2eAgInSc4d1	duhový
odraz	odraz	k1gInSc4	odraz
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
i	i	k9	i
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
duha	duha	k1gFnSc1	duha
je	být	k5eAaImIp3nS	být
latinsky	latinsky	k6eAd1	latinsky
iris	iris	k1gInSc4	iris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Iridium	iridium	k1gNnSc1	iridium
je	být	k5eAaImIp3nS	být
prvkem	prvek	k1gInSc7	prvek
se	s	k7c7	s
značně	značně	k6eAd1	značně
nízkým	nízký	k2eAgNnSc7d1	nízké
zastoupením	zastoupení	k1gNnSc7	zastoupení
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
iridia	iridium	k1gNnSc2	iridium
udáván	udáván	k2eAgMnSc1d1	udáván
jako	jako	k8xC	jako
0,001	[number]	k4	0,001
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
současnými	současný	k2eAgInPc7d1	současný
analytickými	analytický	k2eAgInPc7d1	analytický
postupy	postup	k1gInPc7	postup
nelze	lze	k6eNd1	lze
spolehlivě	spolehlivě	k6eAd1	spolehlivě
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
iridia	iridium	k1gNnSc2	iridium
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
mimořádně	mimořádně	k6eAd1	mimořádně
odolné	odolný	k2eAgNnSc1d1	odolné
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
rozpustit	rozpustit	k5eAaPmF	rozpustit
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
chloristanu	chloristan	k1gInSc2	chloristan
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
.	.	kIx.	.
</s>
<s>
Iridium	iridium	k1gNnSc1	iridium
patří	patřit	k5eAaImIp3nS	patřit
společně	společně	k6eAd1	společně
s	s	k7c7	s
platinou	platina	k1gFnSc7	platina
a	a	k8xC	a
osmiem	osmium	k1gNnSc7	osmium
do	do	k7c2	do
triády	triáda	k1gFnSc2	triáda
těžkých	těžký	k2eAgInPc2d1	těžký
platinových	platinový	k2eAgInPc2d1	platinový
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
mimořádné	mimořádný	k2eAgFnSc3d1	mimořádná
chemické	chemický	k2eAgFnSc3d1	chemická
odolnosti	odolnost	k1gFnSc3	odolnost
je	být	k5eAaImIp3nS	být
iridium	iridium	k1gNnSc1	iridium
legováno	legován	k2eAgNnSc1d1	legován
do	do	k7c2	do
slitin	slitina	k1gFnPc2	slitina
s	s	k7c7	s
rhodiem	rhodium	k1gNnSc7	rhodium
a	a	k8xC	a
platinou	platina	k1gFnSc7	platina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
odolného	odolný	k2eAgNnSc2d1	odolné
chemického	chemický	k2eAgNnSc2d1	chemické
nádobí	nádobí	k1gNnSc2	nádobí
pro	pro	k7c4	pro
rozklady	rozklad	k1gInPc4	rozklad
vzorků	vzorek	k1gInPc2	vzorek
tavením	tavení	k1gNnSc7	tavení
nebo	nebo	k8xC	nebo
spalováním	spalování	k1gNnSc7	spalování
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
slouží	sloužit	k5eAaImIp3nS	sloužit
tyto	tento	k3xDgFnPc4	tento
slitiny	slitina	k1gFnPc4	slitina
jako	jako	k8xC	jako
materiál	materiál	k1gInSc4	materiál
do	do	k7c2	do
speciálních	speciální	k2eAgFnPc2d1	speciální
pecí	pec	k1gFnPc2	pec
na	na	k7c6	na
tažení	tažení	k1gNnSc6	tažení
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
automobilovém	automobilový	k2eAgInSc6d1	automobilový
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
z	z	k7c2	z
iridia	iridium	k1gNnSc2	iridium
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
elektrody	elektroda	k1gFnPc4	elektroda
zapalovacích	zapalovací	k2eAgFnPc2d1	zapalovací
svíček	svíčka	k1gFnPc2	svíčka
s	s	k7c7	s
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
životností	životnost	k1gFnSc7	životnost
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
výrobci	výrobce	k1gMnPc1	výrobce
deklarují	deklarovat	k5eAaBmIp3nP	deklarovat
použitelnost	použitelnost	k1gFnSc4	použitelnost
pro	pro	k7c4	pro
minimálně	minimálně	k6eAd1	minimálně
100	[number]	k4	100
000	[number]	k4	000
mil	míle	k1gFnPc2	míle
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
extrémních	extrémní	k2eAgFnPc6d1	extrémní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
motory	motor	k1gInPc4	motor
závodních	závodní	k2eAgInPc2d1	závodní
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
katalyzátorech	katalyzátor	k1gInPc6	katalyzátor
<g/>
.	.	kIx.	.
</s>
<s>
Iridium	iridium	k1gNnSc1	iridium
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc4	prvek
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
hustotou	hustota	k1gFnSc7	hustota
(	(	kIx(	(
<g/>
hned	hned	k6eAd1	hned
po	po	k7c6	po
osmiu	osmium	k1gNnSc6	osmium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stejném	stejný	k2eAgInSc6d1	stejný
objemu	objem	k1gInSc6	objem
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
o	o	k7c4	o
10	[number]	k4	10
%	%	kIx~	%
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
např.	např.	kA	např.
uran	uran	k1gInSc1	uran
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
hustotu	hustota	k1gFnSc4	hustota
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
střely	střela	k1gFnSc2	střela
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgInSc1d1	prvotní
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
etalony	etalon	k1gInPc7	etalon
délkové	délkový	k2eAgFnSc2d1	délková
míry	míra	k1gFnSc2	míra
1	[number]	k4	1
m	m	kA	m
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc6	hmotnost
1	[number]	k4	1
kg	kg	kA	kg
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
platiny	platina	k1gFnSc2	platina
s	s	k7c7	s
iridiem	iridium	k1gNnSc7	iridium
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
se	se	k3xPyFc4	se
iridium	iridium	k1gNnSc1	iridium
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zanedbatelně	zanedbatelně	k6eAd1	zanedbatelně
<g/>
,	,	kIx,	,
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
v	v	k7c6	v
meteoritech	meteorit	k1gInPc6	meteorit
<g/>
.	.	kIx.	.
</s>
<s>
Iridium	iridium	k1gNnSc1	iridium
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
těžší	těžký	k2eAgNnSc4d2	těžší
a	a	k8xC	a
méně	málo	k6eAd2	málo
vzácné	vzácný	k2eAgInPc1d1	vzácný
prvky	prvek	k1gInPc1	prvek
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
též	též	k9	též
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
uvolnit	uvolnit	k5eAaPmF	uvolnit
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
Země	zem	k1gFnSc2	zem
před	před	k7c7	před
65,5	[number]	k4	65,5
milionu	milion	k4xCgInSc2	milion
let	léto	k1gNnPc2	léto
po	po	k7c6	po
dopadu	dopad	k1gInSc6	dopad
asteroidu	asteroid	k1gInSc2	asteroid
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Chicxulubský	Chicxulubský	k2eAgInSc4d1	Chicxulubský
kráter	kráter	k1gInSc4	kráter
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgMnS	způsobit
vymírání	vymírání	k1gNnSc4	vymírání
na	na	k7c6	na
konci	konec	k1gInSc6	konec
křídy	křída	k1gFnSc2	křída
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Osmium	osmium	k1gNnSc1	osmium
Platina	platina	k1gFnSc1	platina
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Iridium	iridium	k1gNnSc4	iridium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
iridium	iridium	k1gNnSc4	iridium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Iridium	iridium	k1gNnSc1	iridium
na	na	k7c4	na
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
