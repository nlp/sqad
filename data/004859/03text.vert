<s>
Osamu	Osama	k1gFnSc4	Osama
Tezuka	Tezuk	k1gMnSc2	Tezuk
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
手	手	k?	手
治	治	k?	治
Tezuka	Tezuk	k1gMnSc4	Tezuk
Osamu	Osam	k1gInSc2	Osam
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1928	[number]	k4	1928
Tojonaka	Tojonak	k1gMnSc2	Tojonak
<g/>
,	,	kIx,	,
Osaka	Osaek	k1gInSc2	Osaek
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
japonský	japonský	k2eAgMnSc1d1	japonský
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
animátor	animátor	k1gMnSc1	animátor
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
praxi	praxe	k1gFnSc4	praxe
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nevykonával	vykonávat	k5eNaImAgInS	vykonávat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
především	především	k9	především
svými	svůj	k3xOyFgMnPc7	svůj
manga	mango	k1gNnSc2	mango
příběhy	příběh	k1gInPc1	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnSc4	jeho
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
Tecuwan	Tecuwan	k1gInSc4	Tecuwan
Atomu	atom	k1gInSc2	atom
či	či	k8xC	či
Janguru	Jangur	k1gInSc2	Jangur
Taitei	Taite	k1gFnSc2	Taite
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
novátorským	novátorský	k2eAgInPc3d1	novátorský
postupům	postup	k1gInPc3	postup
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
mangách	mango	k1gNnPc6	mango
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
nazýván	nazýván	k2eAgMnSc1d1	nazýván
Bůh	bůh	k1gMnSc1	bůh
mangy	mango	k1gNnPc7	mango
nebo	nebo	k8xC	nebo
japonský	japonský	k2eAgInSc4d1	japonský
Disney	Disney	k1gInPc4	Disney
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
tokijském	tokijský	k2eAgInSc6d1	tokijský
chrámu	chrám	k1gInSc6	chrám
Souzen-dži	Souzenž	k1gFnSc6	Souzen-dž
<g/>
.	.	kIx.	.
</s>
<s>
Osamu	Osama	k1gFnSc4	Osama
Tezuka	Tezuk	k1gMnSc2	Tezuk
byl	být	k5eAaImAgMnS	být
nejstarší	starý	k2eAgMnSc1d3	nejstarší
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
synů	syn	k1gMnPc2	syn
Tezuky	Tezuk	k1gMnPc4	Tezuk
Jutaky	Jutak	k1gInPc7	Jutak
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Tojonaka	Tojonak	k1gMnSc2	Tojonak
v	v	k7c6	v
Osacké	Osacký	k2eAgFnSc6d1	Osacká
prefektuře	prefektura	k1gFnSc6	prefektura
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
města	město	k1gNnSc2	město
Takarazuka	Takarazuk	k1gMnSc2	Takarazuk
v	v	k7c6	v
prefektuře	prefektura	k1gFnSc6	prefektura
Hjogo	Hjogo	k6eAd1	Hjogo
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Osace	Osaka	k1gFnSc6	Osaka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
svého	svůj	k3xOyFgMnSc2	svůj
učitele	učitel	k1gMnSc2	učitel
jménem	jméno	k1gNnSc7	jméno
Inui	Inu	k1gFnSc2	Inu
Hideo	Hideo	k6eAd1	Hideo
nakreslil	nakreslit	k5eAaPmAgInS	nakreslit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
příběh	příběh	k1gInSc4	příběh
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
manga	mango	k1gNnSc2	mango
-	-	kIx~	-
Pin	pin	k1gInSc1	pin
Pin	pin	k1gInSc4	pin
Sei-čan	Sei-čan	k1gMnSc1	Sei-čan
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
nakreslil	nakreslit	k5eAaPmAgInS	nakreslit
další	další	k2eAgInPc4d1	další
krátké	krátký	k2eAgInPc4d1	krátký
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
žáky	žák	k1gMnPc7	žák
i	i	k8xC	i
učiteli	učitel	k1gMnPc7	učitel
staly	stát	k5eAaPmAgFnP	stát
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Tezuka	Tezuk	k1gMnSc4	Tezuk
začal	začít	k5eAaPmAgInS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
nového	nový	k2eAgInSc2d1	nový
zájmu	zájem	k1gInSc2	zájem
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
pseudonym	pseudonym	k1gInSc4	pseudonym
<g/>
,	,	kIx,	,
Osamu	Osama	k1gFnSc4	Osama
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yIgInSc7	který
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
začal	začít	k5eAaPmAgInS	začít
tvořit	tvořit	k5eAaImF	tvořit
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc4	jméno
Osamu	Osam	k1gInSc2	Osam
Tezuka	Tezuk	k1gMnSc2	Tezuk
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
kandži	kandž	k1gFnSc6	kandž
jako	jako	k8xS	jako
手	手	k?	手
治	治	k?	治
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
pravé	pravý	k2eAgNnSc1d1	pravé
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
手	手	k?	手
治	治	k?	治
Tezuka	Tezuk	k1gMnSc4	Tezuk
si	se	k3xPyFc3	se
přidal	přidat	k5eAaPmAgMnS	přidat
poslední	poslední	k2eAgInSc4d1	poslední
znak	znak	k1gInSc4	znak
虫	虫	k?	虫
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
život	život	k1gInSc4	život
hmyzu	hmyz	k1gInSc2	hmyz
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
promítl	promítnout	k5eAaPmAgInS	promítnout
i	i	k9	i
do	do	k7c2	do
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
kreslil	kreslit	k5eAaImAgInS	kreslit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
a	a	k8xC	a
napsal	napsat	k5eAaBmAgInS	napsat
73	[number]	k4	73
krátkých	krátký	k2eAgInPc2d1	krátký
stripů	strip	k1gInPc2	strip
s	s	k7c7	s
názvem	název	k1gInSc7	název
Máčan	Máčana	k1gFnPc2	Máčana
no	no	k9	no
Nikkičó	Nikkičó	k1gMnPc2	Nikkičó
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
poprvé	poprvé	k6eAd1	poprvé
vyšly	vyjít	k5eAaPmAgInP	vyjít
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Mainiči	Mainič	k1gMnPc1	Mainič
Šinbun	Šinbuna	k1gFnPc2	Šinbuna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydal	vydat	k5eAaPmAgMnS	vydat
mangu	mango	k1gNnSc3	mango
s	s	k7c7	s
názvem	název	k1gInSc7	název
Šin	Šin	k1gMnSc2	Šin
Takaradžima	Takaradžim	k1gMnSc2	Takaradžim
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Nový	nový	k2eAgInSc1d1	nový
ostrov	ostrov	k1gInSc1	ostrov
pokladů	poklad	k1gInPc2	poklad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
příběh	příběh	k1gInSc1	příběh
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
povídce	povídka	k1gFnSc6	povídka
Sakai	Sakai	k1gNnSc2	Sakai
Šičima	Šičimum	k1gNnSc2	Šičimum
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnPc1	mango
měla	mít	k5eAaImAgNnP	mít
nečekaný	čekaný	k2eNgInSc4d1	nečekaný
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
400	[number]	k4	400
tisíc	tisíc	k4xCgInPc2	tisíc
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
23	[number]	k4	23
letech	léto	k1gNnPc6	léto
dokončil	dokončit	k5eAaPmAgMnS	dokončit
Osackou	Osacký	k2eAgFnSc4d1	Osacká
univerzitu	univerzita	k1gFnSc4	univerzita
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Tokia	Tokio	k1gNnSc2	Tokio
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Tezuka	Tezuk	k1gMnSc2	Tezuk
stvořil	stvořit	k5eAaPmAgInS	stvořit
nejznámější	známý	k2eAgInSc1d3	nejznámější
postavu	postava	k1gFnSc4	postava
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
mangy	mango	k1gNnPc7	mango
-	-	kIx~	-
robotího	robotí	k2eAgMnSc4d1	robotí
chlapce	chlapec	k1gMnSc4	chlapec
Astro	astra	k1gFnSc5	astra
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
manze	manz	k1gInSc6	manz
Tecuwan	Tecuwan	k1gMnSc1	Tecuwan
Atom	atom	k1gInSc1	atom
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
placených	placený	k2eAgMnPc2d1	placený
umělců	umělec	k1gMnPc2	umělec
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Osamu	Osam	k1gInSc2	Osam
Tezuky	Tezuk	k1gInPc4	Tezuk
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Takarazuka	Takarazuk	k1gMnSc2	Takarazuk
(	(	kIx(	(
<g/>
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
宝	宝	k?	宝
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
patra	patro	k1gNnPc4	patro
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
1400	[number]	k4	1400
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
přichystán	přichystán	k2eAgInSc4d1	přichystán
animační	animační	k2eAgInSc4d1	animační
workshop	workshop	k1gInSc4	workshop
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
animaci	animace	k1gFnSc4	animace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
patře	patro	k1gNnSc6	patro
jsou	být	k5eAaImIp3nP	být
imitace	imitace	k1gFnPc1	imitace
rukou	ruka	k1gFnPc2	ruka
a	a	k8xC	a
nohou	noha	k1gFnSc7	noha
některých	některý	k3yIgFnPc2	některý
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
Tezukových	Tezukový	k2eAgInPc2d1	Tezukový
příběhů	příběh	k1gInPc2	příběh
a	a	k8xC	a
promítací	promítací	k2eAgInPc1d1	promítací
sály	sál	k1gInPc1	sál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
některé	některý	k3yIgInPc4	některý
anime	animat	k5eAaPmIp3nS	animat
série	série	k1gFnSc1	série
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
patře	patro	k1gNnSc6	patro
je	být	k5eAaImIp3nS	být
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
500	[number]	k4	500
titulů	titul	k1gInPc2	titul
Tezukových	Tezukový	k2eAgNnPc2d1	Tezukový
mang	mango	k1gNnPc2	mango
(	(	kIx(	(
<g/>
některá	některý	k3yIgNnPc1	některý
vydání	vydání	k1gNnPc4	vydání
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
cizojazyčná	cizojazyčný	k2eAgNnPc1d1	cizojazyčné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1958	[number]	k4	1958
–	–	k?	–
Ocenění	ocenění	k1gNnSc1	ocenění
Šogakukan	Šogakukan	k1gMnSc1	Šogakukan
Mangašó	Mangašó	k1gMnSc1	Mangašó
za	za	k7c4	za
mangu	mango	k1gNnSc6	mango
Biiko-čan	Biiko-čan	k1gMnSc1	Biiko-čan
<g/>
.	.	kIx.	.
1975	[number]	k4	1975
–	–	k?	–
Ocenění	ocenění	k1gNnSc1	ocenění
Bungeišundžú	Bungeišundžú	k1gMnSc1	Bungeišundžú
<g/>
.	.	kIx.	.
1975	[number]	k4	1975
–	–	k?	–
Ocenění	ocenění	k1gNnSc1	ocenění
Nihon	Nihon	k1gMnSc1	Nihon
Mangaka	Mangak	k1gMnSc2	Mangak
Kjókai	Kjókai	k1gNnSc2	Kjókai
Šó	Šó	k1gMnSc2	Šó
za	za	k7c2	za
<g />
.	.	kIx.	.
</s>
<s>
mangu	mango	k1gNnSc6	mango
Black	Black	k1gMnSc1	Black
Jack	Jack	k1gMnSc1	Jack
<g/>
.	.	kIx.	.
1977	[number]	k4	1977
–	–	k?	–
Ocenění	ocenění	k1gNnSc1	ocenění
Kodansha	Kodansh	k1gMnSc2	Kodansh
Manga	mango	k1gNnSc2	mango
Award	Award	k1gInSc4	Award
za	za	k7c4	za
mangu	mango	k1gNnSc6	mango
Black	Black	k1gMnSc1	Black
Jack	Jack	k1gMnSc1	Jack
a	a	k8xC	a
Micume	Micum	k1gInSc5	Micum
ga	ga	k?	ga
tóru	tóra	k1gFnSc4	tóra
<g/>
.	.	kIx.	.
1983	[number]	k4	1983
–	–	k?	–
Ocenění	ocenění	k1gNnSc1	ocenění
Šogakukan	Šogakukan	k1gMnSc1	Šogakukan
Mangašó	Mangašó	k1gMnSc1	Mangašó
za	za	k7c4	za
mangu	mango	k1gNnSc6	mango
Hidamari	Hidamar	k1gFnSc2	Hidamar
no	no	k9	no
Ki	Ki	k1gMnSc1	Ki
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
–	–	k?	–
Ocenění	ocenění	k1gNnSc2	ocenění
na	na	k7c6	na
Hirošima	Hirošima	k1gFnSc1	Hirošima
International	International	k1gFnSc1	International
Animation	Animation	k1gInSc4	Animation
Festival	festival	k1gInSc1	festival
za	za	k7c4	za
Onboro-Film	Onboro-Film	k1gInSc4	Onboro-Film
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
–	–	k?	–
Ocenění	ocenění	k1gNnSc1	ocenění
Kodansha	Kodansh	k1gMnSc2	Kodansh
Manga	mango	k1gNnSc2	mango
<g />
.	.	kIx.	.
</s>
<s>
Award	Award	k6eAd1	Award
za	za	k7c4	za
mangu	mango	k1gNnSc6	mango
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
–	–	k?	–
Speciální	speciální	k2eAgNnSc4d1	speciální
ocenění	ocenění	k1gNnSc4	ocenění
Nihon	Nihona	k1gFnPc2	Nihona
SF	SF	kA	SF
Taišo	Taišo	k1gNnSc4	Taišo
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
–	–	k?	–
Ocenění	ocenění	k1gNnSc1	ocenění
Zuihóšo	Zuihóšo	k1gMnSc1	Zuihóšo
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
–	–	k?	–
Ocenění	ocenění	k1gNnSc1	ocenění
Will	Will	k1gMnSc1	Will
Eisner	Eisner	k1gMnSc1	Eisner
Comic	Comic	k1gMnSc1	Comic
za	za	k7c4	za
mangu	mango	k1gNnSc6	mango
Buddha	Buddha	k1gMnSc1	Buddha
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
díl	díl	k1gInSc4	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
–	–	k?	–
Ocenění	ocenění	k1gNnSc1	ocenění
Will	Will	k1gMnSc1	Will
Eisner	Eisner	k1gMnSc1	Eisner
Comic	Comic	k1gMnSc1	Comic
za	za	k7c4	za
mangu	mango	k1gNnSc6	mango
Buddha	Buddha	k1gMnSc1	Buddha
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Osamu	Osam	k1gInSc2	Osam
Tezuka	Tezuk	k1gMnSc4	Tezuk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
TezukaOsamu	TezukaOsam	k1gInSc6	TezukaOsam
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
–	–	k?	–
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
TezukaInEnglish	TezukaInEnglish	k1gInSc1	TezukaInEnglish
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
–	–	k?	–
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
a	a	k8xC	a
bibliografie	bibliografie	k1gFnSc1	bibliografie
Osamu	Osam	k1gInSc2	Osam
Tezuky	Tezuk	k1gInPc1	Tezuk
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Muzeum	muzeum	k1gNnSc1	muzeum
Osamu	Osam	k1gInSc2	Osam
Tezuky	Tezuk	k1gInPc1	Tezuk
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Osamu	Osama	k1gFnSc4	Osama
Tezuka	Tezuk	k1gMnSc2	Tezuk
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
diela	diel	k1gMnSc2	diel
–	–	k?	–
Článek	článek	k1gInSc1	článek
na	na	k7c4	na
Manga	mango	k1gNnPc4	mango
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
