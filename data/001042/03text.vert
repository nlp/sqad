<s>
Iowa	Iowa	k1gFnSc1	Iowa
(	(	kIx(	(
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
.	.	kIx.	.
<g/>
ə	ə	k?	ə
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Iowa	Iowa	k1gFnSc1	Iowa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
západních	západní	k2eAgInPc2d1	západní
severních	severní	k2eAgInPc2d1	severní
států	stát	k1gInPc2	stát
ve	v	k7c6	v
středozápadním	středozápadní	k2eAgInSc6d1	středozápadní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Iowa	Iowa	k6eAd1	Iowa
hraničí	hraničit	k5eAaImIp3nP	hraničit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Minnesotou	Minnesota	k1gFnSc7	Minnesota
<g/>
,	,	kIx,	,
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
s	s	k7c7	s
Wisconsinem	Wisconsin	k1gInSc7	Wisconsin
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Illinois	Illinois	k1gFnSc7	Illinois
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Missouri	Missouri	k1gFnSc7	Missouri
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Nebraskou	Nebraska	k1gFnSc7	Nebraska
a	a	k8xC	a
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
s	s	k7c7	s
Jižní	jižní	k2eAgFnSc7d1	jižní
Dakotou	Dakota	k1gFnSc7	Dakota
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
145	[number]	k4	145
746	[number]	k4	746
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Iowa	Iowa	k1gFnSc1	Iowa
26	[number]	k4	26
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
3,1	[number]	k4	3,1
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
21	[number]	k4	21
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
36	[number]	k4	36
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Des	des	k1gNnSc1	des
Moines	Moinesa	k1gFnPc2	Moinesa
s	s	k7c7	s
210	[number]	k4	210
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
největšími	veliký	k2eAgMnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Cedar	Cedar	k1gInSc4	Cedar
Rapids	Rapids	k1gInSc1	Rapids
(	(	kIx(	(
<g/>
130	[number]	k4	130
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Davenport	Davenport	k1gInSc1	Davenport
(	(	kIx(	(
<g/>
110	[number]	k4	110
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sioux	Sioux	k1gInSc1	Sioux
City	city	k1gNnSc1	city
(	(	kIx(	(
<g/>
80	[number]	k4	80
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Iowa	Iow	k2eAgFnSc1d1	Iowa
City	City	k1gFnSc1	City
(	(	kIx(	(
<g/>
70	[number]	k4	70
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Waterloo	Waterloo	k1gNnSc2	Waterloo
(	(	kIx(	(
<g/>
70	[number]	k4	70
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Hawkeye	Hawkey	k1gFnSc2	Hawkey
Point	pointa	k1gFnPc2	pointa
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
509	[number]	k4	509
m	m	kA	m
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
jsou	být	k5eAaImIp3nP	být
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Wisconsinem	Wisconsin	k1gInSc7	Wisconsin
a	a	k8xC	a
Illinois	Illinois	k1gFnSc7	Illinois
<g/>
,	,	kIx,	,
a	a	k8xC	a
Missouri	Missouri	k1gFnSc1	Missouri
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Nebraskou	Nebraska	k1gFnSc7	Nebraska
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Iowy	Iowa	k1gFnSc2	Iowa
dorazili	dorazit	k5eAaPmAgMnP	dorazit
první	první	k4xOgMnPc1	první
evropští	evropský	k2eAgMnPc1d1	evropský
průzkumníci	průzkumník	k1gMnPc1	průzkumník
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1673	[number]	k4	1673
<g/>
.	.	kIx.	.
</s>
<s>
Region	region	k1gInSc1	region
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
získal	získat	k5eAaPmAgInS	získat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
místního	místní	k2eAgInSc2d1	místní
indiánského	indiánský	k2eAgInSc2d1	indiánský
kmene	kmen	k1gInSc2	kmen
Ajovů	Ajov	k1gMnPc2	Ajov
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
Louisiany	Louisiana	k1gFnSc2	Louisiana
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Nové	Nové	k2eAgFnSc2d1	Nové
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
výsledku	výsledek	k1gInSc3	výsledek
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
získali	získat	k5eAaPmAgMnP	získat
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1762	[number]	k4	1762
Španělé	Španěl	k1gMnPc1	Španěl
(	(	kIx(	(
<g/>
místokrálovství	místokrálovství	k1gNnSc1	místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
francouzského	francouzský	k2eAgNnSc2d1	francouzské
držení	držení	k1gNnSc2	držení
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
celou	celý	k2eAgFnSc4d1	celá
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
Louisianu	Louisiana	k1gFnSc4	Louisiana
koupily	koupit	k5eAaPmAgInP	koupit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
se	se	k3xPyFc4	se
území	území	k1gNnSc2	území
Iowy	Iowa	k1gFnSc2	Iowa
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
nově	nově	k6eAd1	nově
zřízeného	zřízený	k2eAgNnSc2d1	zřízené
michiganského	michiganský	k2eAgNnSc2d1	Michiganské
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
wisconsinského	wisconsinský	k2eAgNnSc2d1	wisconsinský
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgNnSc1d1	vlastní
iowské	iowský	k2eAgNnSc1d1	iowský
teritorium	teritorium	k1gNnSc1	teritorium
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
vyčleněna	vyčleněn	k2eAgFnSc1d1	vyčleněna
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Iowa	Iowa	k6eAd1	Iowa
jako	jako	k8xC	jako
29	[number]	k4	29
<g/>
.	.	kIx.	.
stát	stát	k1gInSc1	stát
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
ratifikovala	ratifikovat	k5eAaBmAgFnS	ratifikovat
Ústavu	ústava	k1gFnSc4	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
došlo	dojít	k5eAaPmAgNnS	dojít
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1846	[number]	k4	1846
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nížinatou	nížinatý	k2eAgFnSc4d1	nížinatá
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
úrodných	úrodný	k2eAgFnPc6d1	úrodná
půdách	půda	k1gFnPc6	půda
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
hlavně	hlavně	k9	hlavně
kukuřice	kukuřice	k1gFnSc1	kukuřice
a	a	k8xC	a
sója	sója	k1gFnSc1	sója
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
dobytek	dobytek	k1gInSc1	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Iowě	Iow	k1gInSc6	Iow
působí	působit	k5eAaImIp3nS	působit
baseballový	baseballový	k2eAgInSc1d1	baseballový
tým	tým	k1gInSc1	tým
Iowa	Iowum	k1gNnSc2	Iowum
Cubs	Cubsa	k1gFnPc2	Cubsa
<g/>
,	,	kIx,	,
nejznámějšími	známý	k2eAgInPc7d3	nejznámější
kluby	klub	k1gInPc7	klub
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
jsou	být	k5eAaImIp3nP	být
univerzitní	univerzitní	k2eAgInPc1d1	univerzitní
kluby	klub	k1gInPc1	klub
Iowa	Iowa	k1gFnSc1	Iowa
Hawkeys	Hawkeys	k1gInSc1	Hawkeys
a	a	k8xC	a
Iowa	Iowa	k1gFnSc1	Iowa
State	status	k1gInSc5	status
Cyclones	Cyclones	k1gInSc1	Cyclones
<g/>
.	.	kIx.	.
</s>
<s>
Iowa	Iowa	k6eAd1	Iowa
hraničí	hraničit	k5eAaImIp3nP	hraničit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Minnesotou	Minnesota	k1gFnSc7	Minnesota
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Nebraskou	Nebraský	k2eAgFnSc7d1	Nebraský
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc7d1	jižní
Dakotou	Dakota	k1gFnSc7	Dakota
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Missouri	Missouri	k1gFnSc7	Missouri
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Wisconsinem	Wisconsin	k1gInSc7	Wisconsin
a	a	k8xC	a
Illinois	Illinois	k1gFnSc7	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc7d1	východní
hranicí	hranice	k1gFnSc7	hranice
státu	stát	k1gInSc2	stát
prochází	procházet	k5eAaImIp3nS	procházet
řeka	řeka	k1gFnSc1	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Hranici	hranice	k1gFnSc4	hranice
na	na	k7c6	na
západě	západ	k1gInSc6	západ
od	od	k7c2	od
Sioux	Sioux	k1gInSc4	Sioux
City	City	k1gFnSc4	City
tvoří	tvořit	k5eAaImIp3nP	tvořit
řeka	řeka	k1gFnSc1	řeka
Missouri	Missouri	k1gFnSc1	Missouri
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Sioux	Sioux	k1gInSc1	Sioux
City	city	k1gNnSc1	city
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
řeka	řeka	k1gFnSc1	řeka
Big	Big	k1gMnPc2	Big
Sioux	Sioux	k1gInSc4	Sioux
River	Rivra	k1gFnPc2	Rivra
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgNnPc7d3	veliký
jezery	jezero	k1gNnPc7	jezero
jsou	být	k5eAaImIp3nP	být
Spirit	Spirita	k1gFnPc2	Spirita
Lake	Lake	k1gNnSc2	Lake
<g/>
,	,	kIx,	,
West	West	k1gInSc1	West
Okoboji	Okoboj	k1gInSc3	Okoboj
Lake	Lake	k1gInSc4	Lake
a	a	k8xC	a
East	East	k1gInSc4	East
Okoboji	Okoboj	k1gInPc7	Okoboj
Lake	Lak	k1gFnSc2	Lak
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
severozápadu	severozápad	k1gInSc2	severozápad
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Umělými	umělý	k2eAgFnPc7d1	umělá
nádržemi	nádrž	k1gFnPc7	nádrž
jsou	být	k5eAaImIp3nP	být
Lake	Lake	k1gFnSc4	Lake
Odessa	Odess	k1gMnSc2	Odess
<g/>
,	,	kIx,	,
Saylorville	Saylorvill	k1gMnSc2	Saylorvill
Lake	Lak	k1gMnSc2	Lak
<g/>
,	,	kIx,	,
Lake	Lak	k1gMnSc2	Lak
Red	Red	k1gMnSc2	Red
Rock	rock	k1gInSc1	rock
a	a	k8xC	a
Rathbun	Rathbun	k1gInSc1	Rathbun
Lake	Lak	k1gInSc2	Lak
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
(	(	kIx(	(
<g/>
prérie	prérie	k1gFnSc1	prérie
<g/>
)	)	kIx)	)
s	s	k7c7	s
nerozsáhlými	rozsáhlý	k2eNgInPc7d1	nerozsáhlý
lužními	lužní	k2eAgInPc7d1	lužní
lesy	les	k1gInPc7	les
v	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
okolí	okolí	k1gNnSc6	okolí
potoků	potok	k1gInPc2	potok
a	a	k8xC	a
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
kopce	kopec	k1gInPc1	kopec
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
při	při	k7c6	při
západní	západní	k2eAgFnSc6d1	západní
hranici	hranice	k1gFnSc6	hranice
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
,	,	kIx,	,
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vrchovina	vrchovina	k1gFnSc1	vrchovina
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Driftless	Driftless	k1gInSc4	Driftless
Zone	Zon	k1gInSc2	Zon
<g/>
,	,	kIx,	,
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
jehličnany	jehličnan	k1gInPc4	jehličnan
<g/>
,	,	kIx,	,
nezvyklými	zvyklý	k2eNgFnPc7d1	nezvyklá
pro	pro	k7c4	pro
Iowu	Iowa	k1gFnSc4	Iowa
<g/>
.	.	kIx.	.
</s>
<s>
Nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
soutok	soutok	k1gInSc1	soutok
řek	řeka	k1gFnPc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
a	a	k8xC	a
Des	des	k1gNnSc2	des
Moines	Moinesa	k1gFnPc2	Moinesa
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Iowy	Iowa	k1gFnSc2	Iowa
(	(	kIx(	(
<g/>
146	[number]	k4	146
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
Hawkeye	Hawkeye	k1gFnSc4	Hawkeye
Point	pointa	k1gFnPc2	pointa
(	(	kIx(	(
<g/>
509	[number]	k4	509
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
severně	severně	k6eAd1	severně
od	od	k7c2	od
Sibley	Siblea	k1gFnSc2	Siblea
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
340	[number]	k4	340
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Rozloha	rozloha	k1gFnSc1	rozloha
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
145	[number]	k4	145
746	[number]	k4	746
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Iowa	Iowa	k1gFnSc1	Iowa
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
99	[number]	k4	99
uzemně	uzemně	k6eAd1	uzemně
správních	správní	k2eAgInPc2d1	správní
celků-okresů	celkůkres	k1gInPc2	celků-okres
<g/>
,	,	kIx,	,
zvaných	zvaný	k2eAgInPc2d1	zvaný
"	"	kIx"	"
<g/>
county	counta	k1gFnPc1	counta
(	(	kIx(	(
<g/>
ies	ies	k?	ies
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Iowy	Iowa	k1gFnSc2	Iowa
je	být	k5eAaImIp3nS	být
Des	des	k1gNnSc1	des
Moines	Moinesa	k1gFnPc2	Moinesa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c4	v
Polk	Polk	k1gInSc4	Polk
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Přírodními	přírodní	k2eAgFnPc7d1	přírodní
rezervacemi	rezervace	k1gFnPc7	rezervace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jurisdikci	jurisdikce	k1gFnSc6	jurisdikce
"	"	kIx"	"
<g/>
National	National	k1gFnSc1	National
Park	park	k1gInSc1	park
Service	Service	k1gFnSc1	Service
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Effigy	Effiga	k1gFnSc2	Effiga
Mounds	Mounds	k1gInSc1	Mounds
National	National	k1gMnSc1	National
Monument	monument	k1gInSc1	monument
nedaleko	nedaleko	k7c2	nedaleko
Harpers	Harpersa	k1gFnPc2	Harpersa
Ferry	Ferro	k1gNnPc7	Ferro
Herbert	Herbert	k1gMnSc1	Herbert
Hoover	Hoover	k1gMnSc1	Hoover
National	National	k1gMnSc1	National
Historical	Historical	k1gMnSc1	Historical
Site	Site	k1gNnSc4	Site
ve	v	k7c4	v
West	West	k2eAgInSc4d1	West
Branch	Branch	k1gInSc4	Branch
Lewis	Lewis	k1gFnSc2	Lewis
&	&	k?	&
Clark	Clark	k1gInSc1	Clark
National	National	k1gMnSc1	National
Historic	Historic	k1gMnSc1	Historic
Trail	Trail	k1gMnSc1	Trail
Mormon	mormon	k1gMnSc1	mormon
Pioneer	Pioneer	kA	Pioneer
National	National	k1gMnSc1	National
Historic	Historic	k1gMnSc1	Historic
Trail	Trail	k1gMnSc1	Trail
V	v	k7c6	v
Iowě	Iowa	k1gFnSc6	Iowa
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
kontinentálním	kontinentální	k2eAgNnSc7d1	kontinentální
podnebím	podnebí	k1gNnSc7	podnebí
s	s	k7c7	s
výraznými	výrazný	k2eAgInPc7d1	výrazný
teplotními	teplotní	k2eAgInPc7d1	teplotní
extrémy	extrém	k1gInPc7	extrém
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
Des	des	k1gNnSc1	des
Moines	Moinesa	k1gFnPc2	Moinesa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
zde	zde	k6eAd1	zde
sněží	sněžit	k5eAaImIp3nS	sněžit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
extrémní	extrémní	k2eAgFnSc4d1	extrémní
nadílku	nadílka	k1gFnSc4	nadílka
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jarem	jar	k1gInSc7	jar
přichází	přicházet	k5eAaImIp3nS	přicházet
i	i	k9	i
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Iowské	Iowský	k2eAgNnSc1d1	Iowský
léto	léto	k1gNnSc1	léto
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
subtropickým	subtropický	k2eAgFnPc3d1	subtropická
teplotám	teplota	k1gFnPc3	teplota
dosahujícím	dosahující	k2eAgMnSc6d1	dosahující
přes	přes	k7c4	přes
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
37,8	[number]	k4	37,8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
značným	značný	k2eAgFnPc3d1	značná
srážkám	srážka	k1gFnPc3	srážka
<g/>
,	,	kIx,	,
projevujícím	projevující	k2eAgMnSc7d1	projevující
se	s	k7c7	s
prudkými	prudký	k2eAgFnPc7d1	prudká
bouřemi	bouř	k1gFnPc7	bouř
<g/>
.	.	kIx.	.
</s>
<s>
Iowa	Iowa	k1gFnSc1	Iowa
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tornád	tornádo	k1gNnPc2	tornádo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Iowy	Iowa	k1gFnSc2	Iowa
<g/>
.	.	kIx.	.
</s>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
<g/>
:	:	kIx,	:
Prvními	první	k4xOgInPc7	první
Evropany	Evropan	k1gMnPc7	Evropan
v	v	k7c6	v
Iowě	Iow	k1gInSc6	Iow
byli	být	k5eAaImAgMnP	být
francouzští	francouzský	k2eAgMnPc1d1	francouzský
objevitelé	objevitel	k1gMnPc1	objevitel
Louis	Louis	k1gMnSc1	Louis
Joliet	Joliet	k1gMnSc1	Joliet
a	a	k8xC	a
Jacques	Jacques	k1gMnSc1	Jacques
Marquette	Marquett	k1gInSc5	Marquett
<g/>
.	.	kIx.	.
</s>
<s>
Popsali	popsat	k5eAaPmAgMnP	popsat
Iowu	Iow	k2eAgFnSc4d1	Iowa
jako	jako	k8xS	jako
svěží	svěží	k2eAgFnSc4d1	svěží
<g/>
,	,	kIx,	,
zelenou	zelený	k2eAgFnSc4d1	zelená
a	a	k8xC	a
úrodnou	úrodný	k2eAgFnSc4d1	úrodná
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
Iowy	Iowa	k1gFnSc2	Iowa
několik	několik	k4yIc1	několik
indiánských	indiánský	k2eAgInPc2d1	indiánský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
zde	zde	k6eAd1	zde
dodnes	dodnes	k6eAd1	dodnes
zůstali	zůstat	k5eAaPmAgMnP	zůstat
pouze	pouze	k6eAd1	pouze
Iowové	Iowus	k1gMnPc1	Iowus
<g/>
,	,	kIx,	,
Liščí	liščí	k2eAgMnPc1d1	liščí
indiáni	indián	k1gMnPc1	indián
(	(	kIx(	(
<g/>
Meskwaki	Meskwak	k1gMnPc1	Meskwak
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sókové	Sókové	k2eAgInSc4d1	Sókové
(	(	kIx(	(
<g/>
Sauk	Sauk	k1gInSc4	Sauk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
Iowy	Iowa	k1gFnSc2	Iowa
žili	žít	k5eAaImAgMnP	žít
Siouxové	Siouxový	k2eAgFnPc4d1	Siouxový
<g/>
,	,	kIx,	,
právě	právě	k9	právě
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
Iowy	Iowa	k1gFnSc2	Iowa
hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
tzv	tzv	kA	tzv
<g/>
,	,	kIx,	,
velkých	velký	k2eAgFnPc2d1	velká
plání	pláň	k1gFnPc2	pláň
<g/>
,	,	kIx,	,
prérijní	prérijní	k2eAgMnPc1d1	prérijní
Indiáni	Indián	k1gMnPc1	Indián
i	i	k9	i
jiných	jiný	k2eAgInPc2d1	jiný
kmenů	kmen	k1gInPc2	kmen
obývali	obývat	k5eAaImAgMnP	obývat
Iowu	Iowa	k1gFnSc4	Iowa
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
o	o	k7c4	o
lovce	lovec	k1gMnSc4	lovec
a	a	k8xC	a
sběrače	sběrač	k1gMnSc4	sběrač
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Iowy	Iowa	k1gFnSc2	Iowa
o	o	k7c4	o
primitivně	primitivně	k6eAd1	primitivně
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
američtí	americký	k2eAgMnPc1d1	americký
osadníci	osadník	k1gMnPc1	osadník
přišli	přijít	k5eAaPmAgMnP	přijít
oficiálně	oficiálně	k6eAd1	oficiálně
do	do	k7c2	do
Iowy	Iowa	k1gFnSc2	Iowa
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1833	[number]	k4	1833
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
rodiny	rodina	k1gFnPc4	rodina
z	z	k7c2	z
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
,	,	kIx,	,
Indiany	Indiana	k1gFnSc2	Indiana
a	a	k8xC	a
Missouri	Missouri	k1gFnSc2	Missouri
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1846	[number]	k4	1846
se	se	k3xPyFc4	se
Iowa	Iowa	k1gFnSc1	Iowa
stala	stát	k5eAaPmAgFnS	stát
29	[number]	k4	29
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Chicagská	chicagský	k2eAgFnSc1d1	Chicagská
a	a	k8xC	a
Severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
železnice	železnice	k1gFnSc1	železnice
propojila	propojit	k5eAaPmAgFnS	propojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
Council	Councila	k1gFnPc2	Councila
Bluffs	Bluffsa	k1gFnPc2	Bluffsa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
staly	stát	k5eAaPmAgFnP	stát
východní	východní	k2eAgFnSc7d1	východní
konečnou	konečný	k2eAgFnSc7d1	konečná
stanicí	stanice	k1gFnSc7	stanice
pro	pro	k7c4	pro
Union	union	k1gInSc4	union
Pacific	Pacifice	k1gInPc2	Pacifice
Railroad	Railroad	k1gInSc4	Railroad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
bojovalo	bojovat	k5eAaImAgNnS	bojovat
75	[number]	k4	75
000	[number]	k4	000
Iowanů	Iowan	k1gMnPc2	Iowan
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
13	[number]	k4	13
001	[number]	k4	001
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
díky	díky	k7c3	díky
nemocem	nemoc	k1gFnPc3	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
měla	mít	k5eAaImAgFnS	mít
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
vojáků	voják	k1gMnPc2	voják
na	na	k7c6	na
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
právě	právě	k6eAd1	právě
Iowa	Iow	k2eAgFnSc1d1	Iowa
(	(	kIx(	(
<g/>
60	[number]	k4	60
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Iowa	Iowa	k1gFnSc1	Iowa
zažívala	zažívat	k5eAaImAgFnS	zažívat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
značný	značný	k2eAgInSc4d1	značný
rozvoj	rozvoj	k1gInSc4	rozvoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
chov	chov	k1gInSc4	chov
dobytka	dobytek	k1gInSc2	dobytek
a	a	k8xC	a
prasat	prase	k1gNnPc2	prase
a	a	k8xC	a
pěstování	pěstování	k1gNnSc1	pěstování
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
situace	situace	k1gFnSc1	situace
farmářů	farmář	k1gMnPc2	farmář
značně	značně	k6eAd1	značně
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pochopení	pochopení	k1gNnSc3	pochopení
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
jako	jako	k8xC	jako
obnovitelného	obnovitelný	k2eAgInSc2d1	obnovitelný
zdroje	zdroj	k1gInSc2	zdroj
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
chemický	chemický	k2eAgInSc4d1	chemický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
pokročilé	pokročilý	k2eAgFnSc6d1	pokročilá
mechanizaci	mechanizace	k1gFnSc6	mechanizace
a	a	k8xC	a
intenzifikaci	intenzifikace	k1gFnSc6	intenzifikace
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
zemědělství	zemědělství	k1gNnSc1	zemědělství
na	na	k7c6	na
značném	značný	k2eAgInSc6d1	značný
vzestupu	vzestup	k1gInSc6	vzestup
<g/>
,	,	kIx,	,
Iowa	Iowa	k1gFnSc1	Iowa
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
předním	přední	k2eAgMnPc3d1	přední
státům	stát	k1gInPc3	stát
v	v	k7c6	v
USA	USA	kA	USA
ohledně	ohledně	k7c2	ohledně
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
krize	krize	k1gFnSc1	krize
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
odrazila	odrazit	k5eAaPmAgFnS	odrazit
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
porodnosti	porodnost	k1gFnSc2	porodnost
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Iowě	Iow	k1gInSc6	Iow
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
Středozápadě	středozápad	k1gInSc6	středozápad
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
odvětvím	odvětví	k1gNnSc7	odvětví
v	v	k7c6	v
Iowě	Iowa	k1gFnSc6	Iowa
je	být	k5eAaImIp3nS	být
pochopitelně	pochopitelně	k6eAd1	pochopitelně
zemědělství	zemědělství	k1gNnSc1	zemědělství
a	a	k8xC	a
navazující	navazující	k2eAgInSc1d1	navazující
zpracovatelský	zpracovatelský	k2eAgInSc1d1	zpracovatelský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
výroba	výroba	k1gFnSc1	výroba
ledniček	lednička	k1gFnPc2	lednička
<g/>
,	,	kIx,	,
praček	pračka	k1gFnPc2	pračka
<g/>
,	,	kIx,	,
plnicích	plnicí	k2eAgNnPc2d1	plnicí
per	pero	k1gNnPc2	pero
a	a	k8xC	a
zemědělského	zemědělský	k2eAgNnSc2d1	zemědělské
nářadí	nářadí	k1gNnSc2	nářadí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
iowě	iowa	k1gFnSc6	iowa
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
lokalizováni	lokalizován	k2eAgMnPc1d1	lokalizován
prakticky	prakticky	k6eAd1	prakticky
všichni	všechen	k3xTgMnPc1	všechen
významní	významný	k2eAgMnPc1d1	významný
světoví	světový	k2eAgMnPc1d1	světový
výrobci	výrobce	k1gMnPc1	výrobce
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Iowa	Iowa	k6eAd1	Iowa
je	být	k5eAaImIp3nS	být
také	také	k9	také
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
etanolu	etanol	k1gInSc2	etanol
a	a	k8xC	a
bionafty	bionafta	k1gFnSc2	bionafta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
v	v	k7c6	v
Iowě	Iowa	k1gFnSc6	Iowa
žilo	žít	k5eAaImAgNnS	žít
3	[number]	k4	3
046	[number]	k4	046
355	[number]	k4	355
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
populace	populace	k1gFnSc1	populace
Iowy	Iowa	k1gFnSc2	Iowa
výše	výše	k1gFnSc1	výše
2	[number]	k4	2
966	[number]	k4	966
334	[number]	k4	334
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
nárůst	nárůst	k1gInSc4	nárůst
o	o	k7c4	o
13	[number]	k4	13
430	[number]	k4	430
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
0,5	[number]	k4	0,5
%	%	kIx~	%
za	za	k7c4	za
předcházející	předcházející	k2eAgInSc4d1	předcházející
rok	rok	k1gInSc4	rok
a	a	k8xC	a
nárůst	nárůst	k1gInSc4	nárůst
o	o	k7c4	o
39	[number]	k4	39
952	[number]	k4	952
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
1,4	[number]	k4	1,4
%	%	kIx~	%
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
jednak	jednak	k8xC	jednak
přirozený	přirozený	k2eAgInSc1d1	přirozený
populační	populační	k2eAgInSc1d1	populační
přírůstek	přírůstek	k1gInSc1	přírůstek
53	[number]	k4	53
706	[number]	k4	706
osob	osoba	k1gFnPc2	osoba
/	/	kIx~	/
<g/>
197	[number]	k4	197
163	[number]	k4	163
narození	narození	k1gNnPc2	narození
mínus	mínus	k6eAd1	mínus
143	[number]	k4	143
457	[number]	k4	457
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
úbytek	úbytek	k1gInSc1	úbytek
<g/>
,	,	kIx,	,
způsobený	způsobený	k2eAgInSc1d1	způsobený
migrací	migrace	k1gFnSc7	migrace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
představovalo	představovat	k5eAaImAgNnS	představovat
11	[number]	k4	11
754	[number]	k4	754
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
vně	vně	k7c2	vně
USA	USA	kA	USA
přestavovali	přestavovat	k5eAaImAgMnP	přestavovat
čistý	čistý	k2eAgInSc4d1	čistý
přírůstek	přírůstek	k1gInSc4	přírůstek
29	[number]	k4	29
386	[number]	k4	386
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
sčítání	sčítání	k1gNnSc4	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
populace	populace	k1gFnSc2	populace
Iowy	Iowa	k1gFnSc2	Iowa
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
přibližně	přibližně	k6eAd1	přibližně
97	[number]	k4	97
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
cizího	cizí	k2eAgInSc2d1	cizí
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
3,3	[number]	k4	3,3
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
91,3	[number]	k4	91,3
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
2,9	[number]	k4	2,9
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,4	[number]	k4	0,4
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
1,7	[number]	k4	1,7
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
1,8	[number]	k4	1,8
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
1,8	[number]	k4	1,8
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
5,0	[number]	k4	5,0
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
rodových	rodový	k2eAgFnPc2d1	rodová
linií	linie	k1gFnPc2	linie
v	v	k7c6	v
Iowě	Iow	k1gInSc6	Iow
<g/>
:	:	kIx,	:
Německá	německý	k2eAgFnSc1d1	německá
(	(	kIx(	(
<g/>
35,7	[number]	k4	35,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Irská	irský	k2eAgFnSc1d1	irská
(	(	kIx(	(
<g/>
13,5	[number]	k4	13,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anglická	anglický	k2eAgFnSc1d1	anglická
(	(	kIx(	(
<g/>
9,5	[number]	k4	9,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Americká	americký	k2eAgFnSc1d1	americká
(	(	kIx(	(
<g/>
6,6	[number]	k4	6,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Norská	norský	k2eAgFnSc1d1	norská
(	(	kIx(	(
<g/>
5,7	[number]	k4	5,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předci	předek	k1gMnPc1	předek
většiny	většina	k1gFnSc2	většina
Iowanů	Iowan	k1gMnPc2	Iowan
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
Iowanů	Iowan	k1gMnPc2	Iowan
se	se	k3xPyFc4	se
při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
hlásil	hlásit	k5eAaImAgMnS	hlásit
k	k	k7c3	k
německému	německý	k2eAgInSc3d1	německý
původu	původ	k1gInSc3	původ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
předky	předek	k1gInPc7	předek
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
či	či	k8xC	či
například	například	k6eAd1	například
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
.	.	kIx.	.
6,4	[number]	k4	6,4
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
mladší	mladý	k2eAgFnSc1d2	mladší
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
25,1	[number]	k4	25,1
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
mladší	mladý	k2eAgFnSc1d2	mladší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
14,9	[number]	k4	14,9
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
65	[number]	k4	65
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
tvoří	tvořit	k5eAaImIp3nP	tvořit
přibližně	přibližně	k6eAd1	přibližně
50,9	[number]	k4	50,9
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Iowa	Iowa	k6eAd1	Iowa
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
států	stát	k1gInPc2	stát
v	v	k7c4	v
Great	Great	k2eAgInSc4d1	Great
Plains	Plains	k1gInSc4	Plains
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
pak	pak	k6eAd1	pak
Kansas	Kansas	k1gInSc1	Kansas
<g/>
,	,	kIx,	,
Nebraska	Nebraska	k1gFnSc1	Nebraska
<g/>
,	,	kIx,	,
Oklahoma	Oklahoma	k1gFnSc1	Oklahoma
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
Dakota	Dakota	k1gFnSc1	Dakota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
palčivě	palčivě	k6eAd1	palčivě
pociťuje	pociťovat	k5eAaImIp3nS	pociťovat
úbytek	úbytek	k1gInSc1	úbytek
své	svůj	k3xOyFgFnSc2	svůj
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
89	[number]	k4	89
%	%	kIx~	%
všech	všecek	k3xTgNnPc2	všecek
měst	město	k1gNnPc2	město
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
státech	stát	k1gInPc6	stát
má	mít	k5eAaImIp3nS	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
3	[number]	k4	3
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
řádově	řádově	k6eAd1	řádově
stovky	stovka	k1gFnPc1	stovka
městeček	městečko	k1gNnPc2	městečko
jich	on	k3xPp3gMnPc2	on
mají	mít	k5eAaImIp3nP	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
let	léto	k1gNnPc2	léto
1996	[number]	k4	1996
a	a	k8xC	a
2004	[number]	k4	2004
tyto	tento	k3xDgFnPc4	tento
státy	stát	k1gInPc7	stát
opustilo	opustit	k5eAaPmAgNnS	opustit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
byli	být	k5eAaImAgMnP	být
vysokoškoláci	vysokoškolák	k1gMnPc1	vysokoškolák
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Útěk	útěk	k1gInSc1	útěk
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
všeobecně	všeobecně	k6eAd1	všeobecně
nazývá	nazývat	k5eAaImIp3nS	nazývat
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vynutil	vynutit	k5eAaPmAgInS	vynutit
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
nejrůznější	různý	k2eAgFnSc2d3	nejrůznější
pobídky	pobídka	k1gFnSc2	pobídka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
nabídka	nabídka	k1gFnSc1	nabídka
půdy	půda	k1gFnSc2	půda
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
daňové	daňový	k2eAgFnPc1d1	daňová
úlevy	úleva	k1gFnPc1	úleva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
pomoci	pomoct	k5eAaPmF	pomoct
přilákat	přilákat	k5eAaPmF	přilákat
nové	nový	k2eAgMnPc4d1	nový
přistěhovalce	přistěhovalec	k1gMnPc4	přistěhovalec
a	a	k8xC	a
investory	investor	k1gMnPc4	investor
a	a	k8xC	a
pomoci	pomoct	k5eAaPmF	pomoct
tak	tak	k8xS	tak
zvrátit	zvrátit	k5eAaPmF	zvrátit
trend	trend	k1gInSc4	trend
úbytku	úbytek	k1gInSc2	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Iowanů	Iowan	k1gMnPc2	Iowan
jsou	být	k5eAaImIp3nP	být
nekatolíci	nekatolík	k1gMnPc1	nekatolík
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
protestanté	protestant	k1gMnPc1	protestant
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
nejvíce	hodně	k6eAd3	hodně
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
luteráni	luterán	k1gMnPc1	luterán
a	a	k8xC	a
metodisté	metodista	k1gMnPc1	metodista
<g/>
.	.	kIx.	.
</s>
<s>
Náboženská	náboženský	k2eAgFnSc1d1	náboženská
příslušnost	příslušnost	k1gFnSc1	příslušnost
<g/>
:	:	kIx,	:
Křesťané	křesťan	k1gMnPc1	křesťan
-	-	kIx~	-
73	[number]	k4	73
%	%	kIx~	%
protestanté	protestant	k1gMnPc1	protestant
-	-	kIx~	-
50	[number]	k4	50
%	%	kIx~	%
luteráni	luterán	k1gMnPc1	luterán
-	-	kIx~	-
16	[number]	k4	16
%	%	kIx~	%
metodisté	metodista	k1gMnPc1	metodista
-	-	kIx~	-
13	[number]	k4	13
%	%	kIx~	%
baptisté	baptista	k1gMnPc1	baptista
-	-	kIx~	-
5	[number]	k4	5
%	%	kIx~	%
presbyteriáni	presbyterián	k1gMnPc1	presbyterián
-	-	kIx~	-
3	[number]	k4	3
%	%	kIx~	%
letniční	letniční	k2eAgFnSc2d1	letniční
-	-	kIx~	-
2	[number]	k4	2
%	%	kIx~	%
Congregational	Congregational	k1gMnSc1	Congregational
<g/>
/	/	kIx~	/
<g/>
United	United	k1gMnSc1	United
Church	Church	k1gMnSc1	Church
of	of	k?	of
Christ	Christ	k1gMnSc1	Christ
-	-	kIx~	-
2	[number]	k4	2
%	%	kIx~	%
další	další	k2eAgMnPc1d1	další
protestanté	protestant	k1gMnPc1	protestant
<g/>
;	;	kIx,	;
11	[number]	k4	11
%	%	kIx~	%
katolíci	katolík	k1gMnPc1	katolík
-	-	kIx~	-
23	[number]	k4	23
%	%	kIx~	%
ostatní	ostatní	k2eAgMnPc1d1	ostatní
křesťané	křesťan	k1gMnPc1	křesťan
-	-	kIx~	-
1	[number]	k4	1
%	%	kIx~	%
jiná	jiný	k2eAgNnPc1d1	jiné
náboženství	náboženství	k1gNnPc1	náboženství
-	-	kIx~	-
6	[number]	k4	6
%	%	kIx~	%
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
-	-	kIx~	-
13	[number]	k4	13
%	%	kIx~	%
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
odpovědět	odpovědět	k5eAaPmF	odpovědět
-	-	kIx~	-
5	[number]	k4	5
%	%	kIx~	%
Hrubý	hrubý	k2eAgInSc1d1	hrubý
národní	národní	k2eAgInSc1d1	národní
produkt	produkt	k1gInSc1	produkt
Iowy	Iowa	k1gFnSc2	Iowa
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2003	[number]	k4	2003
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
výše	vysoce	k6eAd2	vysoce
103	[number]	k4	103
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
činil	činit	k5eAaImAgInS	činit
28	[number]	k4	28
340	[number]	k4	340
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
produkty	produkt	k1gInPc4	produkt
patří	patřit	k5eAaImIp3nS	patřit
chov	chov	k1gInSc1	chov
vepřů	vepř	k1gMnPc2	vepř
<g/>
,	,	kIx,	,
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
navazující	navazující	k2eAgInSc4d1	navazující
mlékárenský	mlékárenský	k2eAgInSc4d1	mlékárenský
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
pěstování	pěstování	k1gNnSc4	pěstování
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
sóji	sója	k1gFnSc2	sója
a	a	k8xC	a
ovsa	oves	k1gInSc2	oves
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
produkce	produkce	k1gFnSc1	produkce
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
výrobu	výroba	k1gFnSc4	výroba
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
strojů	stroj	k1gInPc2	stroj
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
techniky	technika	k1gFnSc2	technika
<g/>
)	)	kIx)	)
,	,	kIx,	,
elektrických	elektrický	k2eAgNnPc2d1	elektrické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
chemických	chemický	k2eAgInPc2d1	chemický
produktů	produkt	k1gInPc2	produkt
a	a	k8xC	a
zpracování	zpracování	k1gNnSc4	zpracování
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Iowa	Iowa	k1gFnSc1	Iowa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
USA	USA	kA	USA
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
etanolu	etanol	k1gInSc2	etanol
<g/>
.	.	kIx.	.
</s>
<s>
Des	des	k1gNnSc1	des
Moines	Moines	k1gMnSc1	Moines
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
centrem	centr	k1gInSc7	centr
pojišťovnictví	pojišťovnictví	k1gNnSc2	pojišťovnictví
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Iowě	Iow	k1gInSc6	Iow
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
některých	některý	k3yIgInPc2	některý
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
vybírána	vybírán	k2eAgFnSc1d1	vybírána
varianta	varianta	k1gFnSc1	varianta
evropské	evropský	k2eAgFnSc2d1	Evropská
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
obratu	obrat	k1gInSc2	obrat
soukromých	soukromý	k2eAgFnPc2d1	soukromá
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
spolků	spolek	k1gInPc2	spolek
a	a	k8xC	a
společností	společnost	k1gFnPc2	společnost
(	(	kIx(	(
<g/>
Sales	Sales	k1gInSc1	Sales
and	and	k?	and
Use	usus	k1gInSc5	usus
Tax	taxa	k1gFnPc2	taxa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nynější	nynější	k2eAgFnSc6d1	nynější
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
celkem	celkem	k6eAd1	celkem
devět	devět	k4xCc4	devět
daňových	daňový	k2eAgNnPc2d1	daňové
pásem	pásmo	k1gNnPc2	pásmo
<g/>
,	,	kIx,	,
od	od	k7c2	od
0,36	[number]	k4	0,36
%	%	kIx~	%
do	do	k7c2	do
8,98	[number]	k4	8,98
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
sazba	sazba	k1gFnSc1	sazba
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
okresy	okres	k1gInPc1	okres
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
možnost	možnost	k1gFnSc4	možnost
využít	využít	k5eAaPmF	využít
dalších	další	k2eAgInPc2d1	další
dvou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
obratu	obrat	k1gInSc2	obrat
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vybírány	vybírat	k5eAaImNgFnP	vybírat
současně	současně	k6eAd1	současně
s	s	k7c7	s
celostátně	celostátně	k6eAd1	celostátně
platnou	platný	k2eAgFnSc7d1	platná
5	[number]	k4	5
<g/>
procentní	procentní	k2eAgFnSc7d1	procentní
daní	daň	k1gFnSc7	daň
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
"	"	kIx"	"
<g/>
option	option	k1gInSc1	option
tax	taxa	k1gFnPc2	taxa
<g/>
"	"	kIx"	"
mohou	moct	k5eAaImIp3nP	moct
vejít	vejít	k5eAaPmF	vejít
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
pouze	pouze	k6eAd1	pouze
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
kladně	kladně	k6eAd1	kladně
odhlasovány	odhlasován	k2eAgFnPc1d1	odhlasována
většinou	většina	k1gFnSc7	většina
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
nejčastější	častý	k2eAgFnSc7d3	nejčastější
formou	forma	k1gFnSc7	forma
je	být	k5eAaImIp3nS	být
daň	daň	k1gFnSc1	daň
z	z	k7c2	z
hrubého	hrubý	k2eAgInSc2d1	hrubý
příjmu	příjem	k1gInSc2	příjem
plynoucího	plynoucí	k2eAgInSc2d1	plynoucí
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
hmotného	hmotný	k2eAgNnSc2d1	hmotné
osobního	osobní	k2eAgNnSc2d1	osobní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
zdanění	zdanění	k1gNnSc2	zdanění
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většinou	k6eAd1	většinou
platný	platný	k2eAgMnSc1d1	platný
do	do	k7c2	do
odvolání	odvolání	k1gNnSc2	odvolání
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
ale	ale	k9	ale
součástí	součást	k1gFnSc7	součást
ustanovení	ustanovení	k1gNnPc2	ustanovení
o	o	k7c6	o
platnosti	platnost	k1gFnSc6	platnost
zákona	zákon	k1gInSc2	zákon
i	i	k9	i
datum	datum	k1gNnSc4	datum
jeho	on	k3xPp3gNnSc2	on
vypršení	vypršení	k1gNnSc2	vypršení
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
daň	daň	k1gFnSc1	daň
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
school	schoolit	k5eAaPmRp2nS	schoolit
infrastructure	infrastructur	k1gMnSc5	infrastructur
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vybírá	vybírat	k5eAaImIp3nS	vybírat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnická	vlastnický	k2eAgFnSc1d1	vlastnická
daň	daň	k1gFnSc1	daň
je	být	k5eAaImIp3nS	být
vybírána	vybírat	k5eAaImNgFnS	vybírat
ze	z	k7c2	z
zdanitelné	zdanitelný	k2eAgFnSc2d1	zdanitelná
hodnoty	hodnota	k1gFnSc2	hodnota
hmotných	hmotný	k2eAgInPc2d1	hmotný
statků	statek	k1gInPc2	statek
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
například	například	k6eAd1	například
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc4d1	jiná
stavby	stavba	k1gFnPc4	stavba
<g/>
,	,	kIx,	,
úpravy	úprava	k1gFnPc4	úprava
a	a	k8xC	a
zhodnocení	zhodnocení	k1gNnSc4	zhodnocení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c4	na
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
nebo	nebo	k8xC	nebo
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
základech	základ	k1gInPc6	základ
umístěných	umístěný	k2eAgInPc6d1	umístěný
v	v	k7c4	v
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
typická	typický	k2eAgNnPc1d1	typické
"	"	kIx"	"
<g/>
zhodnocení	zhodnocení	k1gNnSc1	zhodnocení
<g/>
"	"	kIx"	"
patří	patřit	k5eAaImIp3nP	patřit
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
obytné	obytný	k2eAgInPc4d1	obytný
přívěsy	přívěs	k1gInPc4	přívěs
<g/>
,	,	kIx,	,
ploty	plot	k1gInPc4	plot
či	či	k8xC	či
dláždění	dláždění	k1gNnPc4	dláždění
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
jsou	být	k5eAaImIp3nP	být
roztříděna	roztřídit	k5eAaPmNgNnP	roztřídit
do	do	k7c2	do
pěti	pět	k4xCc2	pět
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
rezidenční	rezidenční	k2eAgFnSc1d1	rezidenční
<g/>
,	,	kIx,	,
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
<g/>
,	,	kIx,	,
komerční	komerční	k2eAgFnSc1d1	komerční
<g/>
,	,	kIx,	,
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
a	a	k8xC	a
veřejná	veřejný	k2eAgFnSc1d1	veřejná
<g/>
/	/	kIx~	/
<g/>
železniční	železniční	k2eAgFnSc1d1	železniční
(	(	kIx(	(
<g/>
daň	daň	k1gFnSc1	daň
za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
zhodnocení	zhodnocení	k1gNnSc4	zhodnocení
je	být	k5eAaImIp3nS	být
vyměřena	vyměřen	k2eAgFnSc1d1	vyměřena
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastníci	vlastník	k1gMnPc1	vlastník
obytných	obytný	k2eAgInPc2d1	obytný
domů	dům	k1gInPc2	dům
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
polovině	polovina	k1gFnSc3	polovina
všech	všecek	k3xTgFnPc2	všecek
vlastnických	vlastnický	k2eAgFnPc2d1	vlastnická
daní	daň	k1gFnPc2	daň
vybraných	vybraný	k2eAgFnPc2d1	vybraná
v	v	k7c6	v
Iowě	Iowa	k1gFnSc6	Iowa
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělci	zemědělec	k1gMnPc1	zemědělec
platí	platit	k5eAaImIp3nP	platit
21	[number]	k4	21
procent	procento	k1gNnPc2	procento
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vlastníky	vlastník	k1gMnPc7	vlastník
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
23	[number]	k4	23
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Veřejně	veřejně	k6eAd1	veřejně
prospěšné	prospěšný	k2eAgFnPc4d1	prospěšná
společnosti	společnost	k1gFnPc4	společnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
provozovatelů	provozovatel	k1gMnPc2	provozovatel
železnice	železnice	k1gFnSc2	železnice
platí	platit	k5eAaImIp3nS	platit
10	[number]	k4	10
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Iowa	Iowa	k1gFnSc1	Iowa
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
000	[number]	k4	000
berních	berní	k2eAgInPc2d1	berní
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
majetku	majetek	k1gInSc2	majetek
je	být	k5eAaImIp3nS	být
zdaňována	zdaňovat	k5eAaImNgFnS	zdaňovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedním	jeden	k4xCgInSc7	jeden
berním	berní	k2eAgInSc7d1	berní
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
výše	výše	k1gFnSc1	výše
daně	daň	k1gFnSc2	daň
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
součtem	součet	k1gInSc7	součet
jejích	její	k3xOp3gFnPc2	její
složek	složka	k1gFnPc2	složka
(	(	kIx(	(
<g/>
okresní	okresní	k2eAgFnSc1d1	okresní
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgFnSc1d1	speciální
daň	daň	k1gFnSc1	daň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
okres	okres	k1gInSc1	okres
od	od	k7c2	od
okresu	okres	k1gInSc2	okres
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Mezistátní	mezistátní	k2eAgFnSc1d1	mezistátní
dálnice	dálnice	k1gFnSc1	dálnice
procházející	procházející	k2eAgFnSc4d1	procházející
Iowou	Iowá	k1gFnSc4	Iowá
<g/>
,	,	kIx,	,
názvy	název	k1gInPc1	název
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
:	:	kIx,	:
Interstate	Interstat	k1gInSc5	Interstat
29	[number]	k4	29
<g/>
,	,	kIx,	,
Interstate	Interstat	k1gInSc5	Interstat
35	[number]	k4	35
<g/>
,	,	kIx,	,
Interstate	Interstat	k1gInSc5	Interstat
74	[number]	k4	74
<g/>
,	,	kIx,	,
Interstate	Interstat	k1gInSc5	Interstat
80	[number]	k4	80
<g/>
,	,	kIx,	,
Interstate	Interstat	k1gInSc5	Interstat
129	[number]	k4	129
<g/>
,	,	kIx,	,
Interstate	Interstat	k1gInSc5	Interstat
235	[number]	k4	235
<g/>
,	,	kIx,	,
Interstate	Interstat	k1gInSc5	Interstat
280	[number]	k4	280
<g/>
,	,	kIx,	,
Interstate	Interstat	k1gInSc5	Interstat
380	[number]	k4	380
<g/>
,	,	kIx,	,
Interstate	Interstat	k1gInSc5	Interstat
480	[number]	k4	480
<g/>
,	,	kIx,	,
Interstate	Interstat	k1gInSc5	Interstat
<g />
.	.	kIx.	.
</s>
<s>
680	[number]	k4	680
Názvy	název	k1gInPc1	název
celostátních	celostátní	k2eAgFnPc2d1	celostátní
dálnic	dálnice	k1gFnPc2	dálnice
procházejících	procházející	k2eAgFnPc2d1	procházející
Iowou	Iowá	k1gFnSc4	Iowá
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
:	:	kIx,	:
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
6	[number]	k4	6
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
18	[number]	k4	18
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
20	[number]	k4	20
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
30	[number]	k4	30
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
34	[number]	k4	34
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gFnSc2	Highwaa
52	[number]	k4	52
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
59	[number]	k4	59
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
61	[number]	k4	61
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
63	[number]	k4	63
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
65	[number]	k4	65
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
67	[number]	k4	67
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
69	[number]	k4	69
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
Highway	Highwaa	k1gFnSc2	Highwaa
71	[number]	k4	71
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
75	[number]	k4	75
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
77	[number]	k4	77
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
136	[number]	k4	136
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
151	[number]	k4	151
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
169	[number]	k4	169
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gMnSc2	Highwaa
218	[number]	k4	218
<g/>
,	,	kIx,	,
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Highway	Highwaa	k1gFnSc2	Highwaa
275	[number]	k4	275
Současným	současný	k2eAgMnSc7d1	současný
guvernérem	guvernér	k1gMnSc7	guvernér
je	být	k5eAaImIp3nS	být
Terry	Terr	k1gInPc4	Terr
Branstad	Branstad	k1gInSc1	Branstad
(	(	kIx(	(
<g/>
republikán	republikán	k1gMnSc1	republikán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
demokrata	demokrat	k1gMnSc2	demokrat
Cheta	Cheta	k1gMnSc1	Cheta
Culvera	Culver	k1gMnSc2	Culver
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
2	[number]	k4	2
senátoři	senátor	k1gMnPc1	senátor
<g/>
:	:	kIx,	:
Chuck	Chuck	k1gInSc1	Chuck
Grassley	Grasslea	k1gFnSc2	Grasslea
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejdéle	dlouho	k6eAd3	dlouho
sloužící	sloužící	k2eAgMnSc1d1	sloužící
a	a	k8xC	a
žijící	žijící	k2eAgMnSc1d1	žijící
americký	americký	k2eAgMnSc1d1	americký
senátor	senátor	k1gMnSc1	senátor
Joni	Jon	k1gFnSc2	Jon
Ernst	Ernst	k1gMnSc1	Ernst
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
a	a	k8xC	a
4	[number]	k4	4
kongresmani	kongresman	k1gMnPc1	kongresman
<g/>
:	:	kIx,	:
Rod	rod	k1gInSc1	rod
Blum	bluma	k1gFnPc2	bluma
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
David	David	k1gMnSc1	David
Loebsack	Loebsack	k1gMnSc1	Loebsack
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
David	David	k1gMnSc1	David
Young	Young	k1gMnSc1	Young
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
Steve	Steve	k1gMnSc1	Steve
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
Des	des	k1gNnPc1	des
Moines	Moinesa	k1gFnPc2	Moinesa
-	-	kIx~	-
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Cedar	Cedara	k1gFnPc2	Cedara
Rapids	Rapidsa	k1gFnPc2	Rapidsa
Davenport	Davenport	k1gInSc1	Davenport
-	-	kIx~	-
Saint	Saint	k1gInSc1	Saint
Ambrose	Ambrosa	k1gFnSc6	Ambrosa
University	universita	k1gFnSc2	universita
Sioux	Sioux	k1gInSc4	Sioux
City	City	k1gFnSc2	City
Waterloo	Waterloo	k1gNnSc2	Waterloo
Iowa	Iow	k2eAgFnSc1d1	Iowa
City	City	k1gFnSc1	City
-	-	kIx~	-
University	universita	k1gFnPc1	universita
of	of	k?	of
Iowa	Iowa	k1gMnSc1	Iowa
Council	Council	k1gMnSc1	Council
Bluffs	Bluffs	k1gInSc4	Bluffs
Dubuque	Dubuqu	k1gInSc2	Dubuqu
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
říční	říční	k2eAgInSc1d1	říční
přístav	přístav	k1gInSc1	přístav
Ames	Ames	k1gInSc1	Ames
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Iowa	Iowum	k1gNnPc1	Iowum
State	status	k1gInSc5	status
University	universita	k1gFnSc2	universita
West	Westa	k1gFnPc2	Westa
Des	des	k1gNnSc1	des
Moines	Moines	k1gMnSc1	Moines
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
rezidenční	rezidenční	k2eAgFnSc1d1	rezidenční
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
pojišťovnictví	pojišťovnictví	k1gNnSc1	pojišťovnictví
Cedar	Cedara	k1gFnPc2	Cedara
Falls	Fallsa	k1gFnPc2	Fallsa
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
rezidenční	rezidenční	k2eAgFnSc1d1	rezidenční
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
studentské	studentský	k2eAgNnSc1d1	studentské
městečko	městečko	k1gNnSc1	městečko
Fort	Fort	k?	Fort
Madison	Madisona	k1gFnPc2	Madisona
<g/>
,	,	kIx,	,
nalézá	nalézat	k5eAaImIp3nS	nalézat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nápravné	nápravný	k2eAgNnSc1d1	nápravné
zařízení	zařízení	k1gNnSc1	zařízení
Iowa	Iow	k1gInSc2	Iow
State	status	k1gInSc5	status
Penitentiary	Penitentiara	k1gFnPc1	Penitentiara
Clinton	Clinton	k1gMnSc1	Clinton
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
<g/>
,	,	kIx,	,
říční	říční	k2eAgNnSc1d1	říční
město	město	k1gNnSc1	město
Burlington	Burlington	k1gInSc1	Burlington
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
říční	říční	k2eAgNnSc1d1	říční
město	město	k1gNnSc1	město
Muscatine	Muscatin	k1gInSc5	Muscatin
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
chemické	chemický	k2eAgFnSc2d1	chemická
výroby	výroba	k1gFnSc2	výroba
Newton	Newton	k1gMnSc1	Newton
<g/>
,	,	kIx,	,
Maytag	Maytag	k1gMnSc1	Maytag
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
myčky	myčka	k1gFnPc1	myčka
na	na	k7c4	na
nádobí	nádobí	k1gNnSc4	nádobí
Urbandale	Urbandala	k1gFnSc3	Urbandala
<g/>
,	,	kIx,	,
významná	významný	k2eAgFnSc1d1	významná
rezidenční	rezidenční	k2eAgFnSc1d1	rezidenční
oblast	oblast	k1gFnSc1	oblast
patřící	patřící	k2eAgFnSc1d1	patřící
k	k	k7c3	k
Des	des	k1gNnSc3	des
Moines	Moinesa	k1gFnPc2	Moinesa
Keokuk	Keokuko	k1gNnPc2	Keokuko
<g/>
,	,	kIx,	,
říční	říční	k2eAgInSc1d1	říční
přístav	přístav	k1gInSc1	přístav
na	na	k7c6	na
nejzazším	zadní	k2eAgInSc6d3	nejzazší
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Pella	Pella	k1gFnSc1	Pella
<g/>
,	,	kIx,	,
centrála	centrála	k1gFnSc1	centrála
společnosti	společnost	k1gFnSc2	společnost
Pella	Pella	k1gFnSc1	Pella
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
Central	Central	k1gMnSc1	Central
College	Colleg	k1gFnSc2	Colleg
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
Wyatta	Wyatt	k1gMnSc2	Wyatt
Earpa	Earp	k1gMnSc2	Earp
<g/>
,	,	kIx,	,
Tulip	Tulip	k1gInSc1	Tulip
Fest	fest	k6eAd1	fest
Bettendorf	Bettendorf	k1gInSc4	Bettendorf
-	-	kIx~	-
součástí	součást	k1gFnPc2	součást
Quad	Quada	k1gFnPc2	Quada
Cities	Cities	k1gInSc1	Cities
Cedar	Cedar	k1gMnSc1	Cedar
Falls	Falls	k1gInSc1	Falls
-	-	kIx~	-
domovské	domovský	k2eAgNnSc1d1	domovské
město	město	k1gNnSc1	město
University	universita	k1gFnSc2	universita
of	of	k?	of
Northern	Northern	k1gMnSc1	Northern
Iowa	Iowa	k1gMnSc1	Iowa
(	(	kIx(	(
<g/>
součástí	součást	k1gFnPc2	součást
městské	městský	k2eAgFnSc2d1	městská
oblasti	oblast	k1gFnSc2	oblast
Waterloo	Waterloo	k1gNnSc1	Waterloo
<g/>
)	)	kIx)	)
Fayette	Fayett	k1gMnSc5	Fayett
-	-	kIx~	-
domovské	domovský	k2eAgNnSc1d1	domovské
město	město	k1gNnSc1	město
Upper	Uppra	k1gFnPc2	Uppra
Iowa	Iow	k1gInSc2	Iow
University	universita	k1gFnSc2	universita
Stát	stát	k1gInSc1	stát
Iowa	Iow	k1gInSc2	Iow
vždy	vždy	k6eAd1	vždy
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
svých	svůj	k3xOyFgMnPc2	svůj
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
pozitivně	pozitivně	k6eAd1	pozitivně
projevuje	projevovat	k5eAaImIp3nS	projevovat
ve	v	k7c6	v
výsledcích	výsledek	k1gInPc6	výsledek
standardizovaných	standardizovaný	k2eAgInPc2d1	standardizovaný
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
měla	mít	k5eAaImAgFnS	mít
Iowa	Iowa	k1gFnSc1	Iowa
druhý	druhý	k4xOgInSc1	druhý
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
průměr	průměr	k1gInSc1	průměr
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
v	v	k7c6	v
testech	test	k1gInPc6	test
SAT	SAT	kA	SAT
a	a	k8xC	a
dělila	dělit	k5eAaImAgFnS	dělit
se	se	k3xPyFc4	se
o	o	k7c4	o
druhý	druhý	k4xOgInSc4	druhý
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
průměr	průměr	k1gInSc4	průměr
se	s	k7c7	s
státy	stát	k1gInPc7	stát
v	v	k7c6	v
testech	test	k1gInPc6	test
ACT	ACT	kA	ACT
s	s	k7c7	s
minimálně	minimálně	k6eAd1	minimálně
20	[number]	k4	20
<g/>
%	%	kIx~	%
účastí	účastí	k1gNnPc2	účastí
středoškolských	středoškolský	k2eAgMnPc2d1	středoškolský
absolventů	absolvent	k1gMnPc2	absolvent
<g/>
.	.	kIx.	.
</s>
<s>
Celostátní	celostátní	k2eAgNnSc1d1	celostátní
ústředí	ústředí	k1gNnSc1	ústředí
ACT	ACT	kA	ACT
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c4	v
Iowa	Iowus	k1gMnSc4	Iowus
City	City	k1gFnSc2	City
a	a	k8xC	a
testy	test	k1gInPc7	test
ITBS	ITBS	kA	ITBS
a	a	k8xC	a
ITED	ITED	kA	ITED
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
využívány	využívat	k5eAaImNgInP	využívat
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
jiných	jiný	k2eAgInPc6d1	jiný
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Iowa	Iowa	k1gMnSc1	Iowa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Iowě	Iowa	k1gFnSc6	Iowa
se	se	k3xPyFc4	se
vede	vést	k5eAaImIp3nS	vést
diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
i	i	k9	i
přechod	přechod	k1gInSc4	přechod
z	z	k7c2	z
klasického	klasický	k2eAgInSc2d1	klasický
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
180	[number]	k4	180
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
na	na	k7c4	na
celoroční	celoroční	k2eAgInSc4d1	celoroční
<g/>
.	.	kIx.	.
</s>
<s>
Iowa	Iowa	k6eAd1	Iowa
State	status	k1gInSc5	status
University	universita	k1gFnPc1	universita
University	universita	k1gFnPc1	universita
of	of	k?	of
Iowa	Iowa	k1gMnSc1	Iowa
University	universita	k1gFnSc2	universita
of	of	k?	of
Northern	Northern	k1gMnSc1	Northern
Iowa	Iowa	k1gMnSc1	Iowa
Mezi	mezi	k7c7	mezi
týmy	tým	k1gInPc7	tým
Malé	Malé	k2eAgFnSc2d1	Malé
basebalové	basebalový	k2eAgFnSc2d1	basebalová
ligy	liga	k1gFnSc2	liga
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Mezi	mezi	k7c4	mezi
týmy	tým	k1gInPc4	tým
Malé	Malé	k2eAgFnSc2d1	Malé
hokejové	hokejový	k2eAgFnSc2d1	hokejová
ligy	liga	k1gFnSc2	liga
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Mezi	mezi	k7c7	mezi
týmy	tým	k1gInPc7	tým
Malé	Malé	k2eAgFnSc2d1	Malé
ligy	liga	k1gFnSc2	liga
kopané	kopaná	k1gFnSc2	kopaná
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Des	des	k1gNnSc1	des
Moines	Moinesa	k1gFnPc2	Moinesa
Menace	Menace	k1gFnSc2	Menace
(	(	kIx(	(
<g/>
USL	USL	kA	USL
Premier	Premier	k1gMnSc1	Premier
Development	Development	k1gMnSc1	Development
League	League	k1gFnSc1	League
<g/>
;	;	kIx,	;
amateur	amateur	k1gMnSc1	amateur
<g/>
)	)	kIx)	)
Stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
indiánském	indiánský	k2eAgInSc6d1	indiánský
kmeni	kmen	k1gInSc6	kmen
Ajovů	Ajov	k1gMnPc2	Ajov
(	(	kIx(	(
<g/>
Iowa	Iowa	k1gMnSc1	Iowa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Iowa	Iow	k1gInSc2	Iow
je	být	k5eAaImIp3nS	být
legální	legální	k2eAgInSc1d1	legální
propustit	propustit	k5eAaPmF	propustit
zaměstnankyni	zaměstnankyně	k1gFnSc4	zaměstnankyně
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
sexy	sex	k1gInPc4	sex
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
divoká	divoký	k2eAgNnPc1d1	divoké
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
Iowě	Iowa	k1gFnSc6	Iowa
<g/>
:	:	kIx,	:
V	v	k7c6	v
Iowě	Iow	k1gInSc6	Iow
žije	žít	k5eAaImIp3nS	žít
49	[number]	k4	49
kriticky	kriticky	k6eAd1	kriticky
ohrožených	ohrožený	k2eAgMnPc2d1	ohrožený
a	a	k8xC	a
35	[number]	k4	35
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
64	[number]	k4	64
kriticky	kriticky	k6eAd1	kriticky
ohrožených	ohrožený	k2eAgMnPc2d1	ohrožený
a	a	k8xC	a
89	[number]	k4	89
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Přezdívka	přezdívka	k1gFnSc1	přezdívka
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Hawkeye	Hawkey	k1gFnSc2	Hawkey
State	status	k1gInSc5	status
Pták	pták	k1gMnSc1	pták
<g/>
:	:	kIx,	:
stehlík	stehlík	k1gMnSc1	stehlík
Ryba	Ryba	k1gMnSc1	Ryba
<g/>
:	:	kIx,	:
sumec	sumec	k1gMnSc1	sumec
<g/>
,	,	kIx,	,
Ictalurus	Ictalurus	k1gMnSc1	Ictalurus
punctatus	punctatus	k1gMnSc1	punctatus
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
neoficiálně	oficiálně	k6eNd1	oficiálně
<g/>
)	)	kIx)	)
Květina	Květin	k2eAgFnSc1d1	Květina
<g/>
:	:	kIx,	:
Divoká	divoký	k2eAgFnSc1d1	divoká
Růže	růže	k1gFnSc1	růže
Tráva	tráva	k1gFnSc1	tráva
<g/>
:	:	kIx,	:
Bluebunch	Bluebunch	k1gInSc1	Bluebunch
wheatgrass	wheatgrass	k1gInSc1	wheatgrass
<g/>
,	,	kIx,	,
Agropyron	Agropyron	k1gInSc1	Agropyron
spicatum	spicatum	k1gNnSc1	spicatum
Hmyz	hmyz	k1gInSc1	hmyz
<g/>
:	:	kIx,	:
Včela	včela	k1gFnSc1	včela
Strom	strom	k1gInSc1	strom
<g/>
:	:	kIx,	:
Dub	dub	k1gInSc1	dub
Barvy	barva	k1gFnSc2	barva
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
)	)	kIx)	)
Fosílie	fosílie	k1gFnSc1	fosílie
<g/>
:	:	kIx,	:
Krinoidi	Krinoid	k1gMnPc1	Krinoid
Crinoid	Crinoid	k1gInSc1	Crinoid
(	(	kIx(	(
<g/>
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
<g/>
)	)	kIx)	)
Motto	motto	k1gNnSc1	motto
<g/>
:	:	kIx,	:
Ceňme	cenit	k5eAaImRp1nP	cenit
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnPc4	svůj
svobody	svoboda	k1gFnPc4	svoboda
<g/>
,	,	kIx,	,
braňme	bránit	k5eAaImRp1nP	bránit
svá	svůj	k3xOyFgNnPc4	svůj
práva	právo	k1gNnSc2	právo
Nerost	nerost	k1gInSc1	nerost
<g/>
:	:	kIx,	:
Geoda	geoda	k1gFnSc1	geoda
Lodi	loď	k1gFnSc2	loď
<g/>
:	:	kIx,	:
Iowa	Iowa	k1gMnSc1	Iowa
class	classa	k1gFnPc2	classa
battleship	battleship	k1gMnSc1	battleship
<g/>
,	,	kIx,	,
USS	USS	kA	USS
Iowa	Iowa	k1gFnSc1	Iowa
(	(	kIx(	(
<g/>
BB-	BB-	k1gFnSc1	BB-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USS	USS	kA	USS
Iowa	Iowa	k1gFnSc1	Iowa
(	(	kIx(	(
<g/>
BB-	BB-	k1gFnSc1	BB-
<g/>
53	[number]	k4	53
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USS	USS	kA	USS
Iowa	Iowa	k1gFnSc1	Iowa
(	(	kIx(	(
<g/>
BB-	BB-	k1gFnSc1	BB-
<g/>
61	[number]	k4	61
<g/>
)	)	kIx)	)
Píseň	píseň	k1gFnSc1	píseň
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Song	song	k1gInSc1	song
of	of	k?	of
Iowa	Iowa	k1gFnSc1	Iowa
Půda	půda	k1gFnSc1	půda
<g/>
:	:	kIx,	:
Tama	Tama	k?	Tama
(	(	kIx(	(
<g/>
neoficiálně	neoficiálně	k6eAd1	neoficiálně
<g/>
)	)	kIx)	)
</s>
