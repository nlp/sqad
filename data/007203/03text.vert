<s>
Titulky	titulek	k1gInPc7	titulek
jsou	být	k5eAaImIp3nP	být
psaná	psaný	k2eAgFnSc1d1	psaná
verze	verze	k1gFnSc1	verze
filmového	filmový	k2eAgInSc2d1	filmový
dialogu	dialog	k1gInSc2	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
přepis	přepis	k1gInSc4	přepis
řeči	řeč	k1gFnSc2	řeč
či	či	k8xC	či
nějak	nějak	k6eAd1	nějak
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
právě	právě	k6eAd1	právě
sledované	sledovaný	k2eAgFnSc6d1	sledovaná
scéně	scéna	k1gFnSc6	scéna
z	z	k7c2	z
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
buď	buď	k8xC	buď
překlad	překlad	k1gInSc4	překlad
dialogů	dialog	k1gInPc2	dialog
v	v	k7c6	v
cizím	cizí	k2eAgInSc6d1	cizí
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
psaný	psaný	k2eAgInSc1d1	psaný
záznam	záznam	k1gInSc1	záznam
dialogu	dialog	k1gInSc2	dialog
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
diváky	divák	k1gMnPc4	divák
se	s	k7c7	s
sluchovými	sluchový	k2eAgFnPc7d1	sluchová
poruchami	porucha	k1gFnPc7	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
často	často	k6eAd1	často
využívány	využívat	k5eAaPmNgInP	využívat
pro	pro	k7c4	pro
průběžný	průběžný	k2eAgInSc4d1	průběžný
překlad	překlad	k1gInSc4	překlad
dramatických	dramatický	k2eAgNnPc2d1	dramatické
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
cizím	cizí	k2eAgInSc6d1	cizí
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
obrazovky	obrazovka	k1gFnSc2	obrazovka
nebo	nebo	k8xC	nebo
plátna	plátno	k1gNnSc2	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
filmových	filmový	k2eAgInPc6d1	filmový
festivalech	festival	k1gInPc6	festival
se	se	k3xPyFc4	se
často	často	k6eAd1	často
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
na	na	k7c6	na
odděleném	oddělený	k2eAgInSc6d1	oddělený
displeji	displej	k1gInSc6	displej
pod	pod	k7c7	pod
plátnem	plátno	k1gNnSc7	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Samostatné	samostatný	k2eAgInPc1d1	samostatný
titulky	titulek	k1gInPc1	titulek
prostříhané	prostříhaný	k2eAgInPc1d1	prostříhaný
mezi	mezi	k7c4	mezi
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
filmové	filmový	k2eAgInPc4d1	filmový
obrazy	obraz	k1gInPc4	obraz
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
sdělení	sdělení	k1gNnSc4	sdělení
verbálních	verbální	k2eAgFnPc2d1	verbální
informací	informace	k1gFnPc2	informace
v	v	k7c6	v
ději	děj	k1gInSc6	děj
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgInPc1d1	úvodní
a	a	k8xC	a
závěrečné	závěrečný	k2eAgInPc1d1	závěrečný
titulky	titulek	k1gInPc1	titulek
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
filmu	film	k1gInSc6	film
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
dotvářet	dotvářet	k5eAaImF	dotvářet
jeho	jeho	k3xOp3gInSc4	jeho
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známá	k1gFnPc1	známá
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
výrazné	výrazný	k2eAgInPc4d1	výrazný
prostorové	prostorový	k2eAgInPc4d1	prostorový
titulky	titulek	k1gInPc4	titulek
uvádějící	uvádějící	k2eAgInPc4d1	uvádějící
do	do	k7c2	do
děje	děj	k1gInSc2	děj
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Úvodní	úvodní	k2eAgInPc4d1	úvodní
titulky	titulek	k1gInPc4	titulek
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgInPc1d1	úvodní
titulky	titulek	k1gInPc1	titulek
obvykle	obvykle	k6eAd1	obvykle
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
název	název	k1gInSc4	název
filmu	film	k1gInSc2	film
a	a	k8xC	a
jména	jméno	k1gNnSc2	jméno
jeho	jeho	k3xOp3gMnPc2	jeho
hlavních	hlavní	k2eAgMnPc2d1	hlavní
tvůrců	tvůrce	k1gMnPc2	tvůrce
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
všechno	všechen	k3xTgNnSc4	všechen
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
místními	místní	k2eAgFnPc7d1	místní
zvyklostmi	zvyklost	k1gFnPc7	zvyklost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
filmech	film	k1gInPc6	film
tvoří	tvořit	k5eAaImIp3nS	tvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
část	část	k1gFnSc4	část
filmu	film	k1gInSc2	film
s	s	k7c7	s
výrazným	výrazný	k2eAgNnSc7d1	výrazné
výtvarným	výtvarný	k2eAgNnSc7d1	výtvarné
zpracováním	zpracování	k1gNnSc7	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
animované	animovaný	k2eAgInPc4d1	animovaný
titulky	titulek	k1gInPc4	titulek
filmů	film	k1gInPc2	film
skupiny	skupina	k1gFnSc2	skupina
Monty	Monta	k1gFnSc2	Monta
Python	Python	k1gMnSc1	Python
<g/>
,	,	kIx,	,
filmů	film	k1gInPc2	film
série	série	k1gFnSc2	série
Růžový	růžový	k2eAgMnSc1d1	růžový
panter	panter	k1gMnSc1	panter
nebo	nebo	k8xC	nebo
výrazně	výrazně	k6eAd1	výrazně
pojaté	pojatý	k2eAgInPc4d1	pojatý
titulky	titulek	k1gInPc4	titulek
filmů	film	k1gInPc2	film
o	o	k7c4	o
Jamesi	Jamese	k1gFnSc4	Jamese
Bondovi	Bonda	k1gMnSc3	Bonda
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Závěrečné	závěrečný	k2eAgInPc4d1	závěrečný
titulky	titulek	k1gInPc4	titulek
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečné	závěrečný	k2eAgInPc1d1	závěrečný
titulky	titulek	k1gInPc1	titulek
obvykle	obvykle	k6eAd1	obvykle
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kompletní	kompletní	k2eAgInSc4d1	kompletní
jmenný	jmenný	k2eAgInSc4d1	jmenný
seznam	seznam	k1gInSc4	seznam
všech	všecek	k3xTgMnPc2	všecek
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
filmu	film	k1gInSc6	film
podíleli	podílet	k5eAaImAgMnP	podílet
<g/>
,	,	kIx,	,
od	od	k7c2	od
herců	herec	k1gMnPc2	herec
a	a	k8xC	a
scenáristů	scenárista	k1gMnPc2	scenárista
až	až	k6eAd1	až
po	po	k7c4	po
asistenty	asistent	k1gMnPc4	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečné	závěrečný	k2eAgInPc1d1	závěrečný
titulky	titulek	k1gInPc1	titulek
obvykle	obvykle	k6eAd1	obvykle
děj	děj	k1gInSc4	děj
nijak	nijak	k6eAd1	nijak
nedoplňují	doplňovat	k5eNaImIp3nP	doplňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
řada	řada	k1gFnSc1	řada
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
závěrečných	závěrečný	k2eAgInPc6d1	závěrečný
titulcích	titulek	k1gInPc6	titulek
vidět	vidět	k5eAaImF	vidět
např.	např.	kA	např.
události	událost	k1gFnPc4	událost
po	po	k7c6	po
hlavním	hlavní	k2eAgInSc6d1	hlavní
ději	děj	k1gInSc6	děj
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
záběry	záběr	k1gInPc1	záběr
z	z	k7c2	z
natáčení	natáčení	k1gNnSc2	natáčení
či	či	k8xC	či
nepovedené	povedený	k2eNgFnSc2d1	nepovedená
scénky	scénka	k1gFnSc2	scénka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizi	televize	k1gFnSc6	televize
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
ještě	ještě	k6eAd1	ještě
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
druhem	druh	k1gInSc7	druh
titulků	titulek	k1gInPc2	titulek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
jsou	být	k5eAaImIp3nP	být
skryté	skrytý	k2eAgInPc1d1	skrytý
titulky	titulek	k1gInPc1	titulek
určené	určený	k2eAgInPc1d1	určený
pro	pro	k7c4	pro
diváky	divák	k1gMnPc4	divák
se	s	k7c7	s
sluchovým	sluchový	k2eAgNnSc7d1	sluchové
postižením	postižení	k1gNnSc7	postižení
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
na	na	k7c6	na
teletextu	teletext	k1gInSc6	teletext
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
888	[number]	k4	888
<g/>
.	.	kIx.	.
</s>
<s>
Skryté	skrytý	k2eAgInPc1d1	skrytý
titulky	titulek	k1gInPc1	titulek
doslova	doslova	k6eAd1	doslova
kopírují	kopírovat	k5eAaImIp3nP	kopírovat
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
verzi	verze	k1gFnSc4	verze
dialogu	dialog	k1gInSc2	dialog
a	a	k8xC	a
často	často	k6eAd1	často
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
doplňkové	doplňkový	k2eAgFnPc1d1	doplňková
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
doprovodných	doprovodný	k2eAgInPc6d1	doprovodný
zvucích	zvuk	k1gInPc6	zvuk
jako	jako	k8xC	jako
např.	např.	kA	např.
"	"	kIx"	"
<g/>
ženský	ženský	k2eAgInSc4d1	ženský
smích	smích	k1gInSc4	smích
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
vrzání	vrzání	k1gNnSc1	vrzání
schodů	schod	k1gInPc2	schod
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
s	s	k7c7	s
překladem	překlad	k1gInSc7	překlad
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
buď	buď	k8xC	buď
leptáním	leptání	k1gNnSc7	leptání
(	(	kIx(	(
<g/>
chemický	chemický	k2eAgInSc4d1	chemický
proces	proces	k1gInSc4	proces
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vypalováním	vypalování	k1gNnSc7	vypalování
laserem	laser	k1gInSc7	laser
<g/>
.	.	kIx.	.
</s>
<s>
Leptání	leptání	k1gNnSc1	leptání
je	být	k5eAaImIp3nS	být
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
cena	cena	k1gFnSc1	cena
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
délce	délka	k1gFnSc6	délka
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
laseru	laser	k1gInSc2	laser
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
titulků	titulek	k1gInPc2	titulek
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
filmy	film	k1gInPc1	film
(	(	kIx(	(
<g/>
i	i	k9	i
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
téže	tenže	k3xDgFnSc2	tenže
distribuční	distribuční	k2eAgFnSc2d1	distribuční
společnosti	společnost	k1gFnSc2	společnost
<g/>
)	)	kIx)	)
titulkované	titulkovaný	k2eAgNnSc1d1	titulkované
leptáním	leptání	k1gNnSc7	leptání
nebo	nebo	k8xC	nebo
laserem	laser	k1gInSc7	laser
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
délce	délka	k1gFnSc6	délka
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc6	množství
titulků	titulek	k1gInPc2	titulek
ve	v	k7c6	v
filmu	film	k1gInSc6	film
a	a	k8xC	a
počtu	počet	k1gInSc6	počet
kopií	kopie	k1gFnPc2	kopie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
titulkovat	titulkovat	k5eAaImF	titulkovat
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
pro	pro	k7c4	pro
kina	kino	k1gNnPc4	kino
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vyhotovují	vyhotovovat	k5eAaImIp3nP	vyhotovovat
téměř	téměř	k6eAd1	téměř
výlučně	výlučně	k6eAd1	výlučně
společnosti	společnost	k1gFnPc1	společnost
Filmprint	Filmprinto	k1gNnPc2	Filmprinto
a	a	k8xC	a
Linguafilm	Linguafilma	k1gFnPc2	Linguafilma
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
titulků	titulek	k1gInPc2	titulek
se	se	k3xPyFc4	se
od	od	k7c2	od
překladu	překlad	k1gInSc2	překlad
psaného	psaný	k2eAgInSc2d1	psaný
textu	text	k1gInSc2	text
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
titulků	titulek	k1gInPc2	titulek
pro	pro	k7c4	pro
film	film	k1gInSc4	film
nebo	nebo	k8xC	nebo
televizní	televizní	k2eAgInSc4d1	televizní
pořad	pořad	k1gInSc4	pořad
překladatel	překladatel	k1gMnSc1	překladatel
titulků	titulek	k1gInPc2	titulek
obvykle	obvykle	k6eAd1	obvykle
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
obraz	obraz	k1gInSc1	obraz
a	a	k8xC	a
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
věty	věta	k1gFnPc1	věta
zvukové	zvukový	k2eAgFnPc4d1	zvuková
stopy	stopa	k1gFnPc4	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Překladatel	překladatel	k1gMnSc1	překladatel
rovněž	rovněž	k9	rovněž
může	moct	k5eAaImIp3nS	moct
nebo	nebo	k8xC	nebo
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
přepisu	přepis	k1gInSc3	přepis
dialogu	dialog	k1gInSc2	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
komerčních	komerční	k2eAgInPc2d1	komerční
titulků	titulek	k1gInPc2	titulek
překladatelé	překladatel	k1gMnPc1	překladatel
často	často	k6eAd1	často
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
zamýšlený	zamýšlený	k2eAgInSc4d1	zamýšlený
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
místo	místo	k6eAd1	místo
aby	aby	kYmCp3nS	aby
dialog	dialog	k1gInSc1	dialog
překládali	překládat	k5eAaImAgMnP	překládat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgMnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
důležitější	důležitý	k2eAgFnSc1d2	důležitější
než	než	k8xS	než
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
Diváci	divák	k1gMnPc1	divák
to	ten	k3xDgNnSc4	ten
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
ocení	ocenit	k5eAaPmIp3nS	ocenit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mluvenému	mluvený	k2eAgInSc3d1	mluvený
jazyku	jazyk	k1gInSc3	jazyk
částečně	částečně	k6eAd1	částečně
rozumí	rozumět	k5eAaImIp3nS	rozumět
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rušivé	rušivý	k2eAgNnSc1d1	rušivé
<g/>
.	.	kIx.	.
</s>
<s>
Mluvený	mluvený	k2eAgInSc1d1	mluvený
projev	projev	k1gInSc1	projev
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
slovní	slovní	k2eAgFnSc4d1	slovní
vatu	vata	k1gFnSc4	vata
nebo	nebo	k8xC	nebo
kulturně	kulturně	k6eAd1	kulturně
podmíněné	podmíněný	k2eAgInPc4d1	podmíněný
významy	význam	k1gInPc4	význam
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
psaných	psaný	k2eAgInPc6d1	psaný
titulcích	titulek	k1gInPc6	titulek
nelze	lze	k6eNd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
filmových	filmový	k2eAgInPc2d1	filmový
titulků	titulek	k1gInPc2	titulek
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
překladu	překlad	k1gInSc2	překlad
pro	pro	k7c4	pro
dabing	dabing	k1gInSc4	dabing
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
titulcích	titulek	k1gInPc6	titulek
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
zachovat	zachovat	k5eAaPmF	zachovat
maximální	maximální	k2eAgFnSc4d1	maximální
stručnost	stručnost	k1gFnSc4	stručnost
a	a	k8xC	a
kondenzovat	kondenzovat	k5eAaImF	kondenzovat
informace	informace	k1gFnPc4	informace
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
divák	divák	k1gMnSc1	divák
psaným	psaný	k2eAgInSc7d1	psaný
textem	text	k1gInSc7	text
co	co	k3yQnSc4	co
nejméně	málo	k6eAd3	málo
zatížen	zatížit	k5eAaPmNgInS	zatížit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kromě	kromě	k7c2	kromě
přečtení	přečtení	k1gNnSc2	přečtení
textu	text	k1gInSc2	text
titulků	titulek	k1gInPc2	titulek
musí	muset	k5eAaImIp3nS	muset
ještě	ještě	k6eAd1	ještě
zvládnout	zvládnout	k5eAaPmF	zvládnout
sledovat	sledovat	k5eAaImF	sledovat
dění	dění	k1gNnSc4	dění
na	na	k7c6	na
zbytku	zbytek	k1gInSc6	zbytek
obrazovky	obrazovka	k1gFnSc2	obrazovka
nebo	nebo	k8xC	nebo
plátna	plátno	k1gNnSc2	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
by	by	kYmCp3nP	by
proto	proto	k6eAd1	proto
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
mluvné	mluvný	k2eAgInPc4d1	mluvný
<g/>
"	"	kIx"	"
a	a	k8xC	a
obsahovat	obsahovat	k5eAaImF	obsahovat
jen	jen	k9	jen
nezbytně	zbytně	k6eNd1	zbytně
nutné	nutný	k2eAgFnPc4d1	nutná
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Měly	mít	k5eAaImAgFnP	mít
by	by	kYmCp3nP	by
obsahovat	obsahovat	k5eAaImF	obsahovat
takový	takový	k3xDgInSc1	takový
počet	počet	k1gInSc1	počet
znaků	znak	k1gInPc2	znak
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
divák	divák	k1gMnSc1	divák
stačí	stačit	k5eAaBmIp3nS	stačit
pohodlně	pohodlně	k6eAd1	pohodlně
přečíst	přečíst	k5eAaPmF	přečíst
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
běžným	běžný	k2eAgInSc7d1	běžný
standardem	standard	k1gInSc7	standard
rychlost	rychlost	k1gFnSc1	rychlost
přibližně	přibližně	k6eAd1	přibližně
12	[number]	k4	12
znaků	znak	k1gInPc2	znak
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
cps	cps	k?	cps
<g/>
,	,	kIx,	,
characters	characters	k1gInSc4	characters
per	pero	k1gNnPc2	pero
second	second	k1gInSc1	second
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
cps	cps	k?	cps
do	do	k7c2	do
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
cps	cps	k?	cps
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
titulků	titulek	k1gInPc2	titulek
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
překladu	překlad	k1gInSc3	překlad
filmu	film	k1gInSc2	film
také	také	k9	také
dabing	dabing	k1gInSc4	dabing
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
herci	herc	k1gInSc6	herc
namlouvají	namlouvat	k5eAaImIp3nP	namlouvat
přeložený	přeložený	k2eAgInSc4d1	přeložený
dialog	dialog	k1gInSc4	dialog
přes	přes	k7c4	přes
původní	původní	k2eAgFnSc4d1	původní
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Dabing	dabing	k1gInSc1	dabing
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
náročnější	náročný	k2eAgNnSc1d2	náročnější
a	a	k8xC	a
až	až	k9	až
desetinásobně	desetinásobně	k6eAd1	desetinásobně
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
filmů	film	k1gInPc2	film
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
nabízena	nabízet	k5eAaImNgFnS	nabízet
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
znění	znění	k1gNnSc6	znění
s	s	k7c7	s
titulky	titulek	k1gInPc7	titulek
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
dabovaný	dabovaný	k2eAgInSc1d1	dabovaný
film	film	k1gInSc1	film
automaticky	automaticky	k6eAd1	automaticky
znamená	znamenat	k5eAaImIp3nS	znamenat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
než	než	k8xS	než
titulkovaný	titulkovaný	k2eAgInSc1d1	titulkovaný
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
žánr	žánr	k1gInSc4	žánr
nebo	nebo	k8xC	nebo
přístupnost	přístupnost	k1gFnSc4	přístupnost
filmu	film	k1gInSc2	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dabingem	dabing	k1gInSc7	dabing
se	se	k3xPyFc4	se
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
opatřují	opatřovat	k5eAaImIp3nP	opatřovat
především	především	k6eAd1	především
filmy	film	k1gInPc4	film
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
nebo	nebo	k8xC	nebo
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
(	(	kIx(	(
<g/>
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
zaručeno	zaručit	k5eAaPmNgNnS	zaručit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nárůst	nárůst	k1gInSc1	nárůst
tržeb	tržba	k1gFnPc2	tržba
díky	díky	k7c3	díky
dabingu	dabing	k1gInSc3	dabing
bude	být	k5eAaImBp3nS	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
rozdíl	rozdíl	k1gInSc1	rozdíl
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
dabing	dabing	k1gInSc4	dabing
a	a	k8xC	a
titulky	titulek	k1gInPc4	titulek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
televizi	televize	k1gFnSc6	televize
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
výrazně	výrazně	k6eAd1	výrazně
běžnější	běžný	k2eAgInPc1d2	běžnější
dabované	dabovaný	k2eAgInPc1d1	dabovaný
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dabing	dabing	k1gInSc1	dabing
pro	pro	k7c4	pro
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
video	video	k1gNnSc4	video
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
lacinější	laciný	k2eAgInSc1d2	lacinější
než	než	k8xS	než
dabing	dabing	k1gInSc1	dabing
pro	pro	k7c4	pro
kina	kino	k1gNnPc4	kino
<g/>
.	.	kIx.	.
</s>
<s>
Upřednostňování	upřednostňování	k1gNnSc1	upřednostňování
dabingu	dabing	k1gInSc2	dabing
nebo	nebo	k8xC	nebo
titulků	titulek	k1gInPc2	titulek
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
vychází	vycházet	k5eAaImIp3nS	vycházet
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
učiněných	učiněný	k2eAgNnPc2d1	učiněné
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
dovozci	dovozce	k1gMnPc7	dovozce
filmu	film	k1gInSc2	film
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
cizí	cizí	k2eAgMnPc1d1	cizí
hlasy	hlas	k1gInPc4	hlas
dabovat	dabovat	k5eAaBmF	dabovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbytek	zbytek	k1gInSc1	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
zvolil	zvolit	k5eAaPmAgInS	zvolit
metodu	metoda	k1gFnSc4	metoda
zobrazování	zobrazování	k1gNnSc2	zobrazování
dialogů	dialog	k1gInPc2	dialog
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
přeložených	přeložený	k2eAgInPc2d1	přeložený
titulků	titulek	k1gInPc2	titulek
<g/>
.	.	kIx.	.
</s>
<s>
Volbu	volba	k1gFnSc4	volba
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
finanční	finanční	k2eAgFnSc1d1	finanční
situace	situace	k1gFnSc1	situace
(	(	kIx(	(
<g/>
vytvoření	vytvoření	k1gNnSc1	vytvoření
titulků	titulek	k1gInPc2	titulek
je	být	k5eAaImIp3nS	být
ekonomičtější	ekonomický	k2eAgInSc4d2	ekonomičtější
a	a	k8xC	a
rychlejší	rychlý	k2eAgInSc4d2	rychlejší
způsob	způsob	k1gInSc4	způsob
než	než	k8xS	než
dabing	dabing	k1gInSc4	dabing
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc6	Španělsko
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
politické	politický	k2eAgFnPc1d1	politická
<g/>
.	.	kIx.	.
</s>
<s>
Dabing	dabing	k1gInSc1	dabing
byl	být	k5eAaImAgInS	být
účelnou	účelný	k2eAgFnSc7d1	účelná
formou	forma	k1gFnSc7	forma
cenzury	cenzura	k1gFnSc2	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
dialogu	dialog	k1gInSc2	dialog
zcela	zcela	k6eAd1	zcela
odlišného	odlišný	k2eAgInSc2d1	odlišný
od	od	k7c2	od
originálu	originál	k1gInSc2	originál
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pomocí	pomocí	k7c2	pomocí
něho	on	k3xPp3gInSc2	on
možné	možný	k2eAgNnSc1d1	možné
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
místním	místní	k2eAgMnPc3d1	místní
divákům	divák	k1gMnPc3	divák
nedostávaly	dostávat	k5eNaImAgInP	dostávat
cizí	cizí	k2eAgInPc4d1	cizí
názory	názor	k1gInPc4	názor
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
"	"	kIx"	"
<g/>
speciálních	speciální	k2eAgNnPc2d1	speciální
kin	kino	k1gNnPc2	kino
<g/>
"	"	kIx"	"
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
německých	německý	k2eAgNnPc6d1	německé
městech	město	k1gNnPc6	město
používalo	používat	k5eAaImAgNnS	používat
místo	místo	k1gNnSc1	místo
dabingu	dabing	k1gInSc2	dabing
titulky	titulek	k1gInPc1	titulek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
upřednostňovány	upřednostňován	k2eAgInPc1d1	upřednostňován
titulky	titulek	k1gInPc1	titulek
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dabing	dabing	k1gInSc1	dabing
obecně	obecně	k6eAd1	obecně
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
něco	něco	k3yInSc4	něco
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
a	a	k8xC	a
nepřirozeného	přirozený	k2eNgNnSc2d1	nepřirozené
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jen	jen	k9	jen
u	u	k7c2	u
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
a	a	k8xC	a
v	v	k7c6	v
televizních	televizní	k2eAgInPc6d1	televizní
pořadech	pořad	k1gInPc6	pořad
určených	určený	k2eAgInPc6d1	určený
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
předškolního	předškolní	k2eAgInSc2d1	předškolní
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
animované	animovaný	k2eAgInPc4d1	animovaný
filmy	film	k1gInPc4	film
"	"	kIx"	"
<g/>
dabovány	dabován	k2eAgInPc4d1	dabován
<g/>
"	"	kIx"	"
i	i	k9	i
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
hluk	hluk	k1gInSc1	hluk
pozadí	pozadí	k1gNnSc2	pozadí
a	a	k8xC	a
zvukové	zvukový	k2eAgInPc4d1	zvukový
efekty	efekt	k1gInPc4	efekt
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
nahrávány	nahráván	k2eAgFnPc1d1	nahrávána
jako	jako	k8xS	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
zvuková	zvukový	k2eAgFnSc1d1	zvuková
stopa	stopa	k1gFnSc1	stopa
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dabování	dabování	k1gNnSc1	dabování
na	na	k7c4	na
divácký	divácký	k2eAgInSc4d1	divácký
zážitek	zážitek	k1gInSc4	zážitek
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
žádný	žádný	k3yNgInSc1	žádný
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dabovaných	dabovaný	k2eAgInPc6d1	dabovaný
televizních	televizní	k2eAgInPc6d1	televizní
pořadech	pořad	k1gInPc6	pořad
nebo	nebo	k8xC	nebo
filmech	film	k1gInPc6	film
však	však	k9	však
diváky	divák	k1gMnPc4	divák
často	často	k6eAd1	často
ruší	rušit	k5eAaImIp3nS	rušit
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvuk	zvuk	k1gInSc1	zvuk
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
pohybu	pohyb	k1gInSc2	pohyb
rtů	ret	k1gInPc2	ret
herců	herec	k1gMnPc2	herec
<g/>
.	.	kIx.	.
</s>
<s>
Hlasy	hlas	k1gInPc1	hlas
dabérů	dabér	k1gMnPc2	dabér
navíc	navíc	k6eAd1	navíc
mohou	moct	k5eAaImIp3nP	moct
znít	znít	k5eAaImF	znít
nezaujatě	zaujatě	k6eNd1	zaujatě
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
příliš	příliš	k6eAd1	příliš
expresivně	expresivně	k6eAd1	expresivně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
postavě	postava	k1gFnSc3	postava
nemusí	muset	k5eNaImIp3nS	muset
hodit	hodit	k5eAaImF	hodit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zvuky	zvuk	k1gInPc1	zvuk
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
do	do	k7c2	do
dabované	dabovaný	k2eAgFnSc2d1	dabovaná
stopy	stopa	k1gFnSc2	stopa
vůbec	vůbec	k9	vůbec
převedeny	převést	k5eAaPmNgInP	převést
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
snižuje	snižovat	k5eAaImIp3nS	snižovat
kvalitu	kvalita	k1gFnSc4	kvalita
diváckého	divácký	k2eAgInSc2d1	divácký
zážitku	zážitek	k1gInSc2	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ve	v	k7c6	v
filmu	film	k1gInSc6	film
objeví	objevit	k5eAaPmIp3nS	objevit
promluva	promluva	k1gFnSc1	promluva
v	v	k7c6	v
cizím	cizí	k2eAgInSc6d1	cizí
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
k	k	k7c3	k
překladu	překlad	k1gInSc2	překlad
pro	pro	k7c4	pro
diváka	divák	k1gMnSc4	divák
použijí	použít	k5eAaPmIp3nP	použít
titulky	titulek	k1gInPc4	titulek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
však	však	k9	však
dialog	dialog	k1gInSc1	dialog
v	v	k7c6	v
cizím	cizí	k2eAgInSc6d1	cizí
jazyce	jazyk	k1gInSc6	jazyk
nepřekládá	překládat	k5eNaImIp3nS	překládat
(	(	kIx(	(
<g/>
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
proto	proto	k8xC	proto
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
cílového	cílový	k2eAgNnSc2d1	cílové
publika	publikum	k1gNnSc2	publikum
nesrozumitelný	srozumitelný	k2eNgInSc1d1	nesrozumitelný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
tak	tak	k9	tak
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
film	film	k1gInSc1	film
snímán	snímán	k2eAgInSc1d1	snímán
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
určité	určitý	k2eAgFnSc2d1	určitá
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
danému	daný	k2eAgInSc3d1	daný
jazyku	jazyk	k1gInSc3	jazyk
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Chybějící	chybějící	k2eAgInPc1d1	chybějící
titulky	titulek	k1gInPc1	titulek
v	v	k7c6	v
divácích	divák	k1gMnPc6	divák
evokují	evokovat	k5eAaBmIp3nP	evokovat
podobný	podobný	k2eAgInSc4d1	podobný
pocit	pocit	k1gInSc4	pocit
neporozumění	neporozumění	k1gNnSc2	neporozumění
a	a	k8xC	a
odcizení	odcizení	k1gNnSc2	odcizení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zažívá	zažívat	k5eAaImIp3nS	zažívat
postava	postava	k1gFnSc1	postava
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
titulky	titulek	k1gInPc1	titulek
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
jazyce	jazyk	k1gInSc6	jazyk
jako	jako	k9	jako
jazyk	jazyk	k1gInSc4	jazyk
originálu	originál	k1gInSc2	originál
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
neslyšící	slyšící	k2eNgMnPc4d1	neslyšící
a	a	k8xC	a
nedoslýchavé	nedoslýchavý	k2eAgMnPc4d1	nedoslýchavý
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
je	on	k3xPp3gInPc4	on
využívají	využívat	k5eAaImIp3nP	využívat
i	i	k9	i
slyšící	slyšící	k2eAgMnPc1d1	slyšící
diváci	divák	k1gMnPc1	divák
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
díky	díky	k7c3	díky
titulkům	titulek	k1gInPc3	titulek
neunikne	uniknout	k5eNaPmIp3nS	uniknout
žádná	žádný	k3yNgFnSc1	žádný
důležitá	důležitý	k2eAgFnSc1d1	důležitá
informace	informace	k1gFnSc1	informace
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
pro	pro	k7c4	pro
diváka	divák	k1gMnSc4	divák
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
těžko	těžko	k6eAd1	těžko
srozumitelné	srozumitelný	k2eAgInPc4d1	srozumitelný
silné	silný	k2eAgInPc4d1	silný
přízvuky	přízvuk	k1gInPc4	přízvuk
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
mluví	mluvit	k5eAaImIp3nS	mluvit
jeho	jeho	k3xOp3gInSc7	jeho
mateřským	mateřský	k2eAgInSc7d1	mateřský
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
i	i	k8xC	i
v	v	k7c6	v
televizních	televizní	k2eAgInPc6d1	televizní
pořadech	pořad	k1gInPc6	pořad
se	se	k3xPyFc4	se
často	často	k6eAd1	často
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
titulky	titulek	k1gInPc7	titulek
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
řečník	řečník	k1gMnSc1	řečník
vadu	vada	k1gFnSc4	vada
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
navíc	navíc	k6eAd1	navíc
mohou	moct	k5eAaImIp3nP	moct
zprostředkovávat	zprostředkovávat	k5eAaImF	zprostředkovávat
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
pouhým	pouhý	k2eAgInSc7d1	pouhý
sluchem	sluch	k1gInSc7	sluch
zjišťovaly	zjišťovat	k5eAaImAgInP	zjišťovat
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
příklad	příklad	k1gInSc1	příklad
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
texty	text	k1gInPc4	text
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
tichý	tichý	k2eAgInSc1d1	tichý
dialog	dialog	k1gInSc1	dialog
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
tolik	tolik	k6eAd1	tolik
důležitý	důležitý	k2eAgInSc4d1	důležitý
dialog	dialog	k1gInSc4	dialog
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
dodatečné	dodatečný	k2eAgFnPc1d1	dodatečná
informace	informace	k1gFnPc1	informace
a	a	k8xC	a
detaily	detail	k1gInPc1	detail
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
celkový	celkový	k2eAgInSc4d1	celkový
zážitek	zážitek	k1gInSc4	zážitek
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
divákovi	divák	k1gMnSc3	divák
lepší	dobrý	k2eAgNnSc1d2	lepší
pochopení	pochopení	k1gNnSc1	pochopení
daného	daný	k2eAgInSc2d1	daný
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
jazyce	jazyk	k1gInSc6	jazyk
jako	jako	k8xS	jako
originál	originál	k1gInSc1	originál
lze	lze	k6eAd1	lze
také	také	k9	také
použít	použít	k5eAaPmF	použít
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
chce	chtít	k5eAaImIp3nS	chtít
divák	divák	k1gMnSc1	divák
lépe	dobře	k6eAd2	dobře
rozumět	rozumět	k5eAaImF	rozumět
dialogům	dialog	k1gInPc3	dialog
v	v	k7c6	v
cizojazyčném	cizojazyčný	k2eAgInSc6d1	cizojazyčný
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
uchýlit	uchýlit	k5eAaPmF	uchýlit
k	k	k7c3	k
přeloženým	přeložený	k2eAgInPc3d1	přeložený
titulkům	titulek	k1gInPc3	titulek
<g/>
.	.	kIx.	.
</s>
<s>
Profesionální	profesionální	k2eAgMnPc1d1	profesionální
titulkáři	titulkář	k1gMnPc1	titulkář
obvykle	obvykle	k6eAd1	obvykle
používají	používat	k5eAaImIp3nP	používat
speciální	speciální	k2eAgInSc1d1	speciální
počítačový	počítačový	k2eAgInSc1d1	počítačový
software	software	k1gInSc1	software
a	a	k8xC	a
hardware	hardware	k1gInSc1	hardware
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
je	být	k5eAaImIp3nS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
podobě	podoba	k1gFnSc6	podoba
na	na	k7c6	na
pevném	pevný	k2eAgInSc6d1	pevný
disku	disk	k1gInSc6	disk
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
snímky	snímek	k1gInPc4	snímek
filmu	film	k1gInSc2	film
okamžitě	okamžitě	k6eAd1	okamžitě
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vytváření	vytváření	k1gNnSc2	vytváření
titulků	titulek	k1gInPc2	titulek
titulkář	titulkář	k1gMnSc1	titulkář
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
také	také	k9	také
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
počítačovému	počítačový	k2eAgNnSc3d1	počítačové
programu	program	k1gInSc6	program
přesnou	přesný	k2eAgFnSc4d1	přesná
pozici	pozice	k1gFnSc4	pozice
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
titulky	titulek	k1gInPc1	titulek
ve	v	k7c6	v
filmu	film	k1gInSc6	film
mají	mít	k5eAaImIp3nP	mít
objevit	objevit	k5eAaPmF	objevit
a	a	k8xC	a
zase	zase	k9	zase
zmizet	zmizet	k5eAaPmF	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
filmů	film	k1gInPc2	film
pro	pro	k7c4	pro
kina	kino	k1gNnSc2	kino
to	ten	k3xDgNnSc1	ten
obvykle	obvykle	k6eAd1	obvykle
není	být	k5eNaImIp3nS	být
úkolem	úkol	k1gInSc7	úkol
překladatelů	překladatel	k1gMnPc2	překladatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
techniků	technik	k1gMnPc2	technik
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc4	titulek
lze	lze	k6eAd1	lze
vytvářet	vytvářet	k5eAaImF	vytvářet
i	i	k9	i
pomocí	pomocí	k7c2	pomocí
volně	volně	k6eAd1	volně
dostupných	dostupný	k2eAgInPc2d1	dostupný
programů	program	k1gInPc2	program
na	na	k7c4	na
vytváření	vytváření	k1gNnSc4	vytváření
titulků	titulek	k1gInPc2	titulek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Subtitle	Subtitle	k1gFnSc1	Subtitle
Workshop	workshop	k1gInSc4	workshop
pro	pro	k7c4	pro
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
MovieCaptioner	MovieCaptioner	k1gInSc1	MovieCaptioner
pro	pro	k7c4	pro
Mac	Mac	kA	Mac
<g/>
/	/	kIx~	/
<g/>
Windows	Windows	kA	Windows
nebo	nebo	k8xC	nebo
Subtitle	Subtitle	k1gFnSc1	Subtitle
Composer	Composra	k1gFnPc2	Composra
pro	pro	k7c4	pro
Linux	linux	k1gInSc4	linux
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
programů	program	k1gInPc2	program
jako	jako	k8xC	jako
VirtualDub	VirtualDuba	k1gFnPc2	VirtualDuba
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
VSFilter	VSFilter	k1gInSc1	VSFilter
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
lze	lze	k6eAd1	lze
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
programech	program	k1gInPc6	program
pro	pro	k7c4	pro
přehrávání	přehrávání	k1gNnSc4	přehrávání
videa	video	k1gNnSc2	video
využít	využít	k5eAaPmF	využít
i	i	k9	i
k	k	k7c3	k
zobrazování	zobrazování	k1gNnSc3	zobrazování
titulkových	titulkový	k2eAgInPc2d1	titulkový
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
napevno	napevno	k6eAd1	napevno
vkládat	vkládat	k5eAaImF	vkládat
do	do	k7c2	do
video	video	k1gNnSc4	video
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
filmů	film	k1gInPc2	film
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
DVD	DVD	kA	DVD
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kromě	kromě	k7c2	kromě
původního	původní	k2eAgNnSc2d1	původní
znění	znění	k1gNnSc2	znění
také	také	k9	také
několik	několik	k4yIc4	několik
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
7	[number]	k4	7
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
alternativních	alternativní	k2eAgFnPc2d1	alternativní
zvukových	zvukový	k2eAgFnPc2d1	zvuková
stop	stopa	k1gFnPc2	stopa
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
dabing	dabing	k1gInSc1	dabing
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
mnoho	mnoho	k6eAd1	mnoho
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
až	až	k9	až
32	[number]	k4	32
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
verzí	verze	k1gFnPc2	verze
titulků	titulek	k1gInPc2	titulek
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
také	také	k9	také
jedna	jeden	k4xCgFnSc1	jeden
verze	verze	k1gFnSc1	verze
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
jazyce	jazyk	k1gInSc6	jazyk
pro	pro	k7c4	pro
sluchově	sluchově	k6eAd1	sluchově
postižené	postižený	k1gMnPc4	postižený
či	či	k8xC	či
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
výuky	výuka	k1gFnSc2	výuka
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
na	na	k7c4	na
DVD	DVD	kA	DVD
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
bitmap	bitmapa	k1gFnPc2	bitmapa
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
vlastně	vlastně	k9	vlastně
jako	jako	k9	jako
obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
složité	složitý	k2eAgInPc4d1	složitý
titulky	titulek	k1gInPc4	titulek
opravovat	opravovat	k5eAaImF	opravovat
či	či	k8xC	či
pozměňovat	pozměňovat	k5eAaImF	pozměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
souborové	souborový	k2eAgInPc4d1	souborový
formáty	formát	k1gInPc4	formát
videa	video	k1gNnSc2	video
(	(	kIx(	(
<g/>
DivX	DivX	k1gMnSc1	DivX
<g/>
,	,	kIx,	,
XviD	XviD	k1gMnSc1	XviD
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
titulky	titulek	k1gInPc1	titulek
šířeny	šířen	k2eAgInPc1d1	šířen
v	v	k7c6	v
odděleném	oddělený	k2eAgInSc6d1	oddělený
souboru	soubor	k1gInSc6	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
soubor	soubor	k1gInSc4	soubor
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
různé	různý	k2eAgInPc1d1	různý
formáty	formát	k1gInPc1	formát
<g/>
.	.	kIx.	.
</s>
<s>
Nejpoužívanějšími	používaný	k2eAgInPc7d3	nejpoužívanější
formáty	formát	k1gInPc7	formát
jsou	být	k5eAaImIp3nP	být
MicroDVD	MicroDVD	k1gMnSc1	MicroDVD
a	a	k8xC	a
SubRip	SubRip	k1gMnSc1	SubRip
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
způsobem	způsob	k1gInSc7	způsob
definice	definice	k1gFnSc2	definice
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
titulek	titulek	k1gInSc1	titulek
zobrazit	zobrazit	k5eAaPmF	zobrazit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
MicroDVD	MicroDVD	k1gFnSc2	MicroDVD
se	se	k3xPyFc4	se
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
čísla	číslo	k1gNnSc2	číslo
snímku	snímek	k1gInSc2	snímek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
snímků	snímek	k1gInPc2	snímek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
FPS	FPS	kA	FPS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
SubRip	SubRip	k1gInSc1	SubRip
přiděluje	přidělovat	k5eAaImIp3nS	přidělovat
titulku	titulek	k1gInSc3	titulek
čas	čas	k1gInSc1	čas
začátku	začátek	k1gInSc2	začátek
a	a	k8xC	a
čas	čas	k1gInSc1	čas
konce	konec	k1gInSc2	konec
zobrazení	zobrazení	k1gNnSc2	zobrazení
titulku	titulek	k1gInSc2	titulek
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
titulkové	titulkový	k2eAgInPc1d1	titulkový
soubory	soubor	k1gInPc1	soubor
se	se	k3xPyFc4	se
vkládají	vkládat	k5eAaImIp3nP	vkládat
do	do	k7c2	do
speciálních	speciální	k2eAgFnPc2d1	speciální
SW	SW	kA	SW
přehrávačů	přehrávač	k1gInPc2	přehrávač
nebo	nebo	k8xC	nebo
pomocí	pomocí	k7c2	pomocí
kodeku	kodek	k1gInSc2	kodek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
titulky	titulek	k1gInPc1	titulek
sám	sám	k3xTgMnSc1	sám
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
nalézt	nalézt	k5eAaBmF	nalézt
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
adresáři	adresář	k1gInSc6	adresář
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
umístěn	umístěn	k2eAgInSc1d1	umístěn
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
k	k	k7c3	k
filmům	film	k1gInPc3	film
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgInPc1d1	možný
stáhnout	stáhnout	k5eAaPmF	stáhnout
např.	např.	kA	např.
z	z	k7c2	z
největšího	veliký	k2eAgInSc2d3	veliký
českého	český	k2eAgInSc2d1	český
serveru	server	k1gInSc2	server
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
tematikou	tematika	k1gFnSc7	tematika
titulky	titulek	k1gInPc7	titulek
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
nebo	nebo	k8xC	nebo
OpenSubtitles	OpenSubtitles	k1gInSc1	OpenSubtitles
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
</s>
<s>
Titulky	titulek	k1gInPc1	titulek
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
u	u	k7c2	u
cizojazyčně	cizojazyčně	k6eAd1	cizojazyčně
zpívaných	zpívaný	k2eAgFnPc2d1	zpívaná
oper	opera	k1gFnPc2	opera
nebo	nebo	k8xC	nebo
při	při	k7c6	při
vystoupení	vystoupení	k1gNnSc6	vystoupení
souborů	soubor	k1gInPc2	soubor
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
jazykových	jazykový	k2eAgFnPc2d1	jazyková
oblastí	oblast	k1gFnPc2	oblast
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
divákům	divák	k1gMnPc3	divák
porozumění	porozumění	k1gNnSc4	porozumění
ději	dít	k5eAaImIp1nS	dít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Subtitle	Subtitle	k1gFnSc2	Subtitle
(	(	kIx(	(
<g/>
captioning	captioning	k1gInSc1	captioning
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Fanouškovské	fanouškovský	k2eAgNnSc1d1	fanouškovské
titulkování	titulkování	k1gNnSc1	titulkování
o.	o.	k?	o.
<g/>
titulkování	titulkování	k1gNnSc1	titulkování
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
sbírka	sbírka	k1gFnSc1	sbírka
základních	základní	k2eAgNnPc2d1	základní
pravidel	pravidlo	k1gNnPc2	pravidlo
pro	pro	k7c4	pro
titulkování	titulkování	k1gNnSc4	titulkování
www.titulky.com	www.titulky.com	k1gInSc4	www.titulky.com
amatérské	amatérský	k2eAgInPc1d1	amatérský
titulky	titulek	k1gInPc1	titulek
k	k	k7c3	k
filmům	film	k1gInPc3	film
Fuka	Fuk	k1gInSc2	Fuk
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
:	:	kIx,	:
Konec	konec	k1gInSc1	konec
titulků	titulek	k1gInPc2	titulek
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
?	?	kIx.	?
</s>
<s>
FFFILM	FFFILM	kA	FFFILM
<g/>
.	.	kIx.	.
2011-05-26	[number]	k4	2011-05-26
(	(	kIx(	(
<g/>
Web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
2012-12-27	[number]	k4	2012-12-27
Pošta	pošta	k1gFnSc1	pošta
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
:	:	kIx,	:
Jak	jak	k8xS	jak
zabít	zabít	k5eAaPmF	zabít
kvalitní	kvalitní	k2eAgInPc4d1	kvalitní
titulky	titulek	k1gInPc4	titulek
TranslatoBlog	TranslatoBloga	k1gFnPc2	TranslatoBloga
<g/>
.	.	kIx.	.
2016-12-02	[number]	k4	2016-12-02
(	(	kIx(	(
<g/>
Web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
2016-12-16	[number]	k4	2016-12-16
</s>
