<s>
Stimulace	stimulace	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c4
biologickému	biologický	k2eAgMnSc3d1
a	a	k8xC
psychologickému	psychologický	k2eAgInSc3d1
jevu	jev	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
fyzikálním	fyzikální	k2eAgInSc6d1
jevu	jev	k1gInSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Stimulovaná	stimulovaný	k2eAgFnSc1d1
emise	emise	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Stimulace	stimulace	k1gFnSc2
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Stimulace	stimulace	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
lat.	lat.	k?
stimulatio	stimulatio	k1gNnSc1
<g/>
,	,	kIx,
podněcování	podněcování	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
činnost	činnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
podněcuje	podněcovat	k5eAaImIp3nS
či	či	k8xC
povzbuzuje	povzbuzovat	k5eAaImIp3nS
člověka	člověk	k1gMnSc4
nebo	nebo	k8xC
i	i	k9
jiný	jiný	k2eAgInSc4d1
živý	živý	k2eAgInSc4d1
organizmus	organizmus	k1gInSc4
k	k	k7c3
nějaké	nějaký	k3yIgFnSc3
reakci	reakce	k1gFnSc3
<g/>
,	,	kIx,
chování	chování	k1gNnSc3
<g/>
,	,	kIx,
činnosti	činnost	k1gFnSc3
nebo	nebo	k8xC
práci	práce	k1gFnSc3
zpravidla	zpravidla	k6eAd1
pozitivní	pozitivní	k2eAgFnSc2d1
povahy	povaha	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pomocí	pomocí	k7c2
stimulů	stimul	k1gInPc2
(	(	kIx(
<g/>
podnětů	podnět	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vnější	vnější	k2eAgInPc1d1
a	a	k8xC
vnitřní	vnitřní	k2eAgInPc4d1
stimuly	stimul	k1gInPc4
</s>
<s>
Stimuly	stimul	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
různé	různý	k2eAgFnPc1d1
povahy	povaha	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přírodních	přírodní	k2eAgFnPc6d1
vědách	věda	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
psychologii	psychologie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
medicíně	medicína	k1gFnSc6
či	či	k8xC
v	v	k7c6
biologii	biologie	k1gFnSc6
vůbec	vůbec	k9
se	se	k3xPyFc4
rozlišují	rozlišovat	k5eAaImIp3nP
vnější	vnější	k2eAgInPc1d1
stimuly	stimul	k1gInPc1
(	(	kIx(
<g/>
působící	působící	k2eAgInPc1d1
z	z	k7c2
vnějšího	vnější	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
na	na	k7c4
organizmus	organizmus	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
vnitřní	vnitřní	k2eAgInPc1d1
stimuly	stimul	k1gInPc1
(	(	kIx(
<g/>
působící	působící	k2eAgMnSc1d1
zevnitř	zevnitř	k6eAd1
<g/>
,	,	kIx,
z	z	k7c2
vlastních	vlastní	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
organizmu	organizmus	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vnějšími	vnější	k2eAgInPc7d1
stimuly	stimul	k1gInPc7
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
například	například	k6eAd1
působení	působení	k1gNnSc4
různých	různý	k2eAgInPc2d1
činitelů	činitel	k1gInPc2
(	(	kIx(
<g/>
látek	látka	k1gFnPc2
<g/>
)	)	kIx)
na	na	k7c4
příslušné	příslušný	k2eAgInPc4d1
receptory	receptor	k1gInPc4
(	(	kIx(
<g/>
nervy	nerv	k1gInPc4
a	a	k8xC
orgány	orgán	k1gInPc4
těla	tělo	k1gNnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
podpora	podpora	k1gFnSc1
růstu	růst	k1gInSc2
a	a	k8xC
vývoje	vývoj	k1gInSc2
organismů	organismus	k1gInPc2
(	(	kIx(
<g/>
světlem	světlo	k1gNnSc7
<g/>
,	,	kIx,
teplem	teplo	k1gNnSc7
<g/>
,	,	kIx,
chemickými	chemický	k2eAgFnPc7d1
látkami	látka	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stimulující	stimulující	k2eAgFnPc4d1
látky	látka	k1gFnPc4
a	a	k8xC
léky	lék	k1gInPc1
se	se	k3xPyFc4
označují	označovat	k5eAaImIp3nP
jako	jako	k9
stimulans	stimulans	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
člověka	člověk	k1gMnSc2
to	ten	k3xDgNnSc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
také	také	k9
vnější	vnější	k2eAgInPc4d1
stimuly	stimul	k1gInPc4
společenské	společenský	k2eAgInPc4d1
<g/>
,	,	kIx,
právní	právní	k2eAgInPc4d1
<g/>
,	,	kIx,
finanční	finanční	k2eAgInPc4d1
respektive	respektive	k9
ekonomické	ekonomický	k2eAgInPc4d1
apod.	apod.	kA
</s>
<s>
Vnitřní	vnitřní	k2eAgInPc1d1
stimuly	stimul	k1gInPc1
jsou	být	k5eAaImIp3nP
různé	různý	k2eAgInPc1d1
biochemické	biochemický	k2eAgInPc1d1
děje	děj	k1gInPc1
<g/>
,	,	kIx,
biologické	biologický	k2eAgInPc1d1
pochody	pochod	k1gInPc1
či	či	k8xC
procesy	proces	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
stimulují	stimulovat	k5eAaImIp3nP
resp.	resp.	kA
vyvolávají	vyvolávat	k5eAaImIp3nP
jiné	jiný	k2eAgInPc1d1
děje	děj	k1gInPc1
<g/>
,	,	kIx,
žádoucí	žádoucí	k2eAgFnSc1d1
nebo	nebo	k8xC
nežádoucí	žádoucí	k2eNgFnSc1d1
reakce	reakce	k1gFnSc1
organismu	organismus	k1gInSc2
apod.	apod.	kA
</s>
<s>
Obojí	obojí	k4xRgFnSc1
stimulace	stimulace	k1gFnSc1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
kombinovat	kombinovat	k5eAaImF
a	a	k8xC
vyvolávat	vyvolávat	k5eAaImF
nebo	nebo	k8xC
podporovat	podporovat	k5eAaImF
jedna	jeden	k4xCgFnSc1
druhou	druhý	k4xOgFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
sexuální	sexuální	k2eAgNnSc4d1
dráždění	dráždění	k1gNnSc4
a	a	k8xC
vzrušování	vzrušování	k1gNnSc4
<g/>
,	,	kIx,
často	často	k6eAd1
jako	jako	k8xS,k8xC
příprava	příprava	k1gFnSc1
kopulace	kopulace	k1gFnSc2
<g/>
,	,	kIx,
pohlavního	pohlavní	k2eAgInSc2d1
styku	styk	k1gInSc2
(	(	kIx(
<g/>
kohabitace	kohabitace	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
rozmnožování	rozmnožování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Encyklopedický	encyklopedický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ED	ED	kA
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
Stimulace	stimulace	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
1044	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4177644-6	4177644-6	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
9429	#num#	k4
</s>
