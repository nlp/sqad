činnost	činnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
podněcuje	podněcovat	k5eAaImIp3nS
či	či	k8xC
povzbuzuje	povzbuzovat	k5eAaImIp3nS
člověka	člověk	k1gMnSc4
nebo	nebo	k8xC
i	i	k9
jiný	jiný	k2eAgInSc4d1
živý	živý	k2eAgInSc4d1
organizmus	organizmus	k1gInSc4
k	k	k7c3
nějaké	nějaký	k3yIgFnSc3
reakci	reakce	k1gFnSc3
<g/>
,	,	kIx,
chování	chování	k1gNnSc3
<g/>
,	,	kIx,
činnosti	činnost	k1gFnSc3
nebo	nebo	k8xC
práci	práce	k1gFnSc3
zpravidla	zpravidla	k6eAd1
pozitivní	pozitivní	k2eAgFnSc2d1
povahy	povaha	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pomocí	pomocí	k7c2
stimulů	stimul	k1gInPc2
