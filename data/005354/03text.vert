<s>
Flektivní	flektivní	k2eAgInSc1d1	flektivní
jazyk	jazyk	k1gInSc1	jazyk
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
gramatické	gramatický	k2eAgFnPc4d1	gramatická
funkce	funkce	k1gFnPc4	funkce
pomocí	pomocí	k7c2	pomocí
flexe	flexe	k1gFnSc2	flexe
(	(	kIx(	(
<g/>
ohýbání	ohýbání	k1gNnSc2	ohýbání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
skloňování	skloňování	k1gNnSc2	skloňování
a	a	k8xC	a
časování	časování	k1gNnSc2	časování
<g/>
,	,	kIx,	,
předpon	předpona	k1gFnPc2	předpona
a	a	k8xC	a
přípon	přípona	k1gFnPc2	přípona
<g/>
.	.	kIx.	.
</s>
<s>
Flektivní	flektivní	k2eAgInSc1d1	flektivní
jazykový	jazykový	k2eAgInSc1d1	jazykový
typ	typ	k1gInSc1	typ
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
dobře	dobře	k6eAd1	dobře
vyvinutou	vyvinutý	k2eAgFnSc7d1	vyvinutá
diferenciací	diferenciace	k1gFnSc7	diferenciace
morfému	morfém	k1gInSc2	morfém
a	a	k8xC	a
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vlivem	k7c2	vlivem
redukce	redukce	k1gFnSc2	redukce
koncovek	koncovka	k1gFnPc2	koncovka
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hromadění	hromadění	k1gNnSc3	hromadění
významů	význam	k1gInPc2	význam
v	v	k7c6	v
koncovce	koncovka	k1gFnSc6	koncovka
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc1	slovo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
a	a	k8xC	a
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
Koncovka	koncovka	k1gFnSc1	koncovka
nese	nést	k5eAaImIp3nS	nést
najednou	najednou	k6eAd1	najednou
sémantické	sémantický	k2eAgFnPc4d1	sémantická
a	a	k8xC	a
syntaktické	syntaktický	k2eAgFnPc4d1	syntaktická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
nejjasněji	jasně	k6eAd3	jasně
rozlišit	rozlišit	k5eAaPmF	rozlišit
slovní	slovní	k2eAgInPc4d1	slovní
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
morfologické	morfologický	k2eAgFnSc6d1	morfologická
rovině	rovina	k1gFnSc6	rovina
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
zřetelné	zřetelný	k2eAgNnSc1d1	zřetelné
vyjádření	vyjádření	k1gNnSc1	vyjádření
kategorie	kategorie	k1gFnSc2	kategorie
slova	slovo	k1gNnSc2	slovo
a	a	k8xC	a
kategorie	kategorie	k1gFnSc2	kategorie
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
slov	slovo	k1gNnPc2	slovo
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
přenesením	přenesení	k1gNnSc7	přenesení
slova	slovo	k1gNnSc2	slovo
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Syntax	syntax	k1gFnSc1	syntax
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
volným	volný	k2eAgInSc7d1	volný
slovosledem	slovosled	k1gInSc7	slovosled
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
podmíněn	podmínit	k5eAaPmNgInS	podmínit
existencí	existence	k1gFnSc7	existence
koncovek	koncovka	k1gFnPc2	koncovka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
bohatý	bohatý	k2eAgInSc4d1	bohatý
repertoár	repertoár	k1gInSc4	repertoár
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
vět	věta	k1gFnPc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
flexivních	flexivní	k2eAgInPc2d1	flexivní
jazyků	jazyk	k1gInPc2	jazyk
jsou	být	k5eAaImIp3nP	být
jazyky	jazyk	k1gInPc1	jazyk
slovanské	slovanský	k2eAgInPc1d1	slovanský
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
polština	polština	k1gFnSc1	polština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
baltské	baltský	k2eAgInPc1d1	baltský
(	(	kIx(	(
<g/>
litevština	litevština	k1gFnSc1	litevština
<g/>
,	,	kIx,	,
lotyština	lotyština	k1gFnSc1	lotyština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bantuské	bantuský	k2eAgFnPc1d1	bantuská
<g/>
,	,	kIx,	,
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
PROKOPOVÁ	Prokopová	k1gFnSc1	Prokopová
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Manuál	manuál	k1gInSc1	manuál
Encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
245	[number]	k4	245
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
244	[number]	k4	244
<g/>
-	-	kIx~	-
<g/>
4129	[number]	k4	4129
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Morfologická	morfologický	k2eAgFnSc1d1	morfologická
typologie	typologie	k1gFnSc1	typologie
jazyků	jazyk	k1gInPc2	jazyk
Analytický	analytický	k2eAgInSc1d1	analytický
jazyk	jazyk	k1gInSc1	jazyk
Syntetický	syntetický	k2eAgInSc1d1	syntetický
jazyk	jazyk	k1gInSc4	jazyk
Aglutinační	aglutinační	k2eAgInSc4d1	aglutinační
jazyk	jazyk	k1gInSc4	jazyk
</s>
