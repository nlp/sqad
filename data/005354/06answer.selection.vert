<s>
Zástupci	zástupce	k1gMnPc1	zástupce
flexivních	flexivní	k2eAgInPc2d1	flexivní
jazyků	jazyk	k1gInPc2	jazyk
jsou	být	k5eAaImIp3nP	být
jazyky	jazyk	k1gInPc1	jazyk
slovanské	slovanský	k2eAgInPc1d1	slovanský
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
,	,	kIx,	,
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
polština	polština	k1gFnSc1	polština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
baltské	baltský	k2eAgInPc1d1	baltský
(	(	kIx(	(
<g/>
litevština	litevština	k1gFnSc1	litevština
<g/>
,	,	kIx,	,
lotyština	lotyština	k1gFnSc1	lotyština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bantuské	bantuský	k2eAgFnPc1d1	bantuská
<g/>
,	,	kIx,	,
latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
.	.	kIx.	.
</s>
