<p>
<s>
Hůl	hůl	k1gFnSc1	hůl
nebo	nebo	k8xC	nebo
hůlka	hůlka	k1gFnSc1	hůlka
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
nebo	nebo	k8xC	nebo
kovová	kovový	k2eAgFnSc1d1	kovová
tyč	tyč	k1gFnSc1	tyč
opatřená	opatřený	k2eAgFnSc1d1	opatřená
hlavičkou	hlavička	k1gFnSc7	hlavička
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
opora	opora	k1gFnSc1	opora
při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jako	jako	k9	jako
módní	módní	k2eAgInSc4d1	módní
doplněk	doplněk	k1gInSc4	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
však	však	k9	však
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
k	k	k7c3	k
obraně	obrana	k1gFnSc3	obrana
jako	jako	k8xC	jako
pastýřská	pastýřský	k2eAgFnSc1d1	pastýřská
hůl	hůl	k1gFnSc1	hůl
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mít	mít	k5eAaImF	mít
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
jako	jako	k8xC	jako
maršálská	maršálský	k2eAgFnSc1d1	maršálská
hůl	hůl	k1gFnSc1	hůl
nebo	nebo	k8xC	nebo
berla	berla	k1gFnSc1	berla
<g/>
.	.	kIx.	.
</s>
<s>
Nerovné	rovný	k2eNgFnSc3d1	nerovná
holi	hole	k1gFnSc3	hole
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgFnSc3d1	vytvořená
jako	jako	k8xS	jako
samorost	samorost	k1gInSc1	samorost
z	z	k7c2	z
větve	větev	k1gFnSc2	větev
nebo	nebo	k8xC	nebo
kořene	kořen	k1gInSc2	kořen
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
česky	česky	k6eAd1	česky
říká	říkat	k5eAaImIp3nS	říkat
sukovice	sukovice	k1gFnSc1	sukovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
holí	hole	k1gFnPc2	hole
==	==	k?	==
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
se	s	k7c7	s
sníženou	snížený	k2eAgFnSc7d1	snížená
pohyblivostí	pohyblivost	k1gFnSc7	pohyblivost
slouží	sloužit	k5eAaImIp3nS	sloužit
berle	berle	k1gFnSc2	berle
nebo	nebo	k8xC	nebo
francouzské	francouzský	k2eAgFnSc2d1	francouzská
hole	hole	k1gFnSc2	hole
</s>
</p>
<p>
<s>
Nevidomí	nevidomý	k1gMnPc1	nevidomý
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
a	a	k8xC	a
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
odlišení	odlišení	k1gNnSc4	odlišení
bílé	bílý	k2eAgFnSc2d1	bílá
hole	hole	k1gFnSc2	hole
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
slepecká	slepecký	k2eAgNnPc4d1	slepecké
hůl	hůl	k1gFnSc4	hůl
</s>
</p>
<p>
<s>
obdobně	obdobně	k6eAd1	obdobně
hluchoslepé	hluchoslepý	k2eAgFnPc1d1	hluchoslepá
osoby	osoba	k1gFnPc1	osoba
nosí	nosit	k5eAaImIp3nP	nosit
červenobíle	červenobíle	k6eAd1	červenobíle
pruhované	pruhovaný	k2eAgFnPc1d1	pruhovaná
hole	hole	k1gFnPc1	hole
</s>
</p>
<p>
<s>
vycházková	vycházkový	k2eAgFnSc1d1	vycházková
hůl	hůl	k1gFnSc1	hůl
-	-	kIx~	-
též	též	k9	též
módní	módní	k2eAgFnSc1d1	módní
hůl	hůl	k1gFnSc1	hůl
<g/>
,	,	kIx,	,
špacírka	špacírka	k1gFnSc1	špacírka
<g/>
,	,	kIx,	,
také	také	k9	také
turistická	turistický	k2eAgFnSc1d1	turistická
</s>
</p>
<p>
<s>
insignie	insignie	k1gFnPc1	insignie
(	(	kIx(	(
<g/>
odznaky	odznak	k1gInPc1	odznak
moci	moc	k1gFnSc2	moc
či	či	k8xC	či
velení	velení	k1gNnSc2	velení
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
velitelská	velitelský	k2eAgFnSc1d1	velitelská
hůl	hůl	k1gFnSc1	hůl
<g/>
,	,	kIx,	,
maršálská	maršálský	k2eAgFnSc1d1	maršálská
hůl	hůl	k1gFnSc1	hůl
<g/>
,	,	kIx,	,
žezlo	žezlo	k1gNnSc4	žezlo
panovníka	panovník	k1gMnSc2	panovník
či	či	k8xC	či
berla	berla	k1gFnSc1	berla
biskupů	biskup	k1gMnPc2	biskup
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
představitelů	představitel	k1gMnPc2	představitel
církve	církev	k1gFnSc2	církev
</s>
</p>
<p>
<s>
hůl	hůl	k1gFnSc1	hůl
nebo	nebo	k8xC	nebo
hůlka	hůlka	k1gFnSc1	hůlka
je	být	k5eAaImIp3nS	být
častou	častý	k2eAgFnSc7d1	častá
rekvizitou	rekvizita	k1gFnSc7	rekvizita
čarodějů	čaroděj	k1gMnPc2	čaroděj
a	a	k8xC	a
mágů	mág	k1gMnPc2	mág
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc7	on
přisuzován	přisuzován	k2eAgInSc1d1	přisuzován
magický	magický	k2eAgInSc1d1	magický
význam	význam	k1gInSc1	význam
</s>
</p>
<p>
<s>
sportovní	sportovní	k2eAgNnSc4d1	sportovní
náčiní	náčiní	k1gNnSc4	náčiní
–	–	k?	–
v	v	k7c6	v
lyžařských	lyžařský	k2eAgInPc6d1	lyžařský
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
turistice	turistika	k1gFnSc6	turistika
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
hole	hole	k1gFnSc2	hole
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
představuje	představovat	k5eAaImIp3nS	představovat
například	například	k6eAd1	například
hokejka	hokejka	k1gFnSc1	hokejka
<g/>
,	,	kIx,	,
florbalka	florbalka	k1gFnSc1	florbalka
nebo	nebo	k8xC	nebo
golfová	golfový	k2eAgFnSc1d1	golfová
hůl	hůl	k1gFnSc1	hůl
</s>
</p>
<p>
<s>
pastevecká	pastevecký	k2eAgFnSc1d1	pastevecká
hůl	hůl	k1gFnSc1	hůl
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
při	při	k7c6	při
pasení	pasení	k1gNnSc6	pasení
dobytka	dobytek	k1gInSc2	dobytek
</s>
</p>
<p>
<s>
rákoska	rákoska	k1gFnSc1	rákoska
bývá	bývat	k5eAaImIp3nS	bývat
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
buď	buď	k8xC	buď
z	z	k7c2	z
loupaného	loupaný	k2eAgInSc2d1	loupaný
či	či	k8xC	či
neloupaného	loupaný	k2eNgInSc2d1	neloupaný
ratanu	ratan	k1gInSc2	ratan
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vzácně	vzácně	k6eAd1	vzácně
z	z	k7c2	z
bambusu	bambus	k1gInSc2	bambus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
taktovka	taktovka	k1gFnSc1	taktovka
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Lojek	Lojek	k1gMnSc1	Lojek
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
:	:	kIx,	:
Hůl	hůl	k1gFnSc1	hůl
v	v	k7c6	v
právu	právo	k1gNnSc6	právo
-	-	kIx~	-
právní	právní	k2eAgFnSc1d1	právní
symbolika	symbolika	k1gFnSc1	symbolika
držení	držení	k1gNnSc2	držení
<g/>
,	,	kIx,	,
užívání	užívání	k1gNnSc2	užívání
<g/>
,	,	kIx,	,
lámání	lámání	k1gNnSc2	lámání
a	a	k8xC	a
házení	házení	k1gNnSc2	házení
soudcovské	soudcovský	k2eAgFnSc2d1	soudcovská
hole	hole	k1gFnSc2	hole
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
K.	K.	kA	K.
Schelle	Schella	k1gFnSc6	Schella
(	(	kIx(	(
<g/>
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Symbolika	symbolika	k1gFnSc1	symbolika
a	a	k8xC	a
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
.	.	kIx.	.
</s>
<s>
Key	Key	k?	Key
Publishing	Publishing	k1gInSc1	Publishing
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o	o	k7c4	o
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7418	[number]	k4	7418
<g/>
-	-	kIx~	-
<g/>
150	[number]	k4	150
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Berla	berla	k1gFnSc1	berla
</s>
</p>
<p>
<s>
Dešťová	dešťový	k2eAgFnSc1d1	dešťová
hůl	hůl	k1gFnSc1	hůl
<g/>
,	,	kIx,	,
perkusní	perkusní	k2eAgInSc1d1	perkusní
nástroj	nástroj	k1gInSc1	nástroj
</s>
</p>
<p>
<s>
Hole	hole	k1gFnSc1	hole
(	(	kIx(	(
<g/>
rozcestník	rozcestník	k1gInSc1	rozcestník
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Insignie	insignie	k1gFnPc1	insignie
</s>
</p>
<p>
<s>
Žezlo	žezlo	k1gNnSc1	žezlo
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hůl	hůl	k1gFnSc1	hůl
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hůl	hůl	k1gFnSc1	hůl
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
O	o	k7c6	o
vycházkových	vycházkový	k2eAgFnPc6d1	vycházková
holích	hole	k1gFnPc6	hole
</s>
</p>
<p>
<s>
Slepecká	slepecký	k2eAgFnSc1d1	slepecká
hůl	hůl	k1gFnSc1	hůl
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
holí	holit	k5eAaImIp3nS	holit
</s>
</p>
