<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
nerovné	rovný	k2eNgFnSc3d1	nerovná
holi	hole	k1gFnSc3	hole
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgFnSc3d1	vytvořená
jako	jako	k8xC	jako
samorost	samorost	k1gInSc1	samorost
z	z	k7c2	z
větve	větev	k1gFnSc2	větev
nebo	nebo	k8xC	nebo
kořene	kořen	k1gInSc2	kořen
stromu	strom	k1gInSc2	strom
<g/>
?	?	kIx.	?
</s>
