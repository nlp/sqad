<p>
<s>
Skříň	skříň	k1gFnSc1	skříň
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgInSc4d1	stabilní
typ	typ	k1gInSc4	typ
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
ukládání	ukládání	k1gNnSc3	ukládání
či	či	k8xC	či
skladování	skladování	k1gNnSc3	skladování
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
skříň	skříň	k1gFnSc1	skříň
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
skříňka	skříňka	k1gFnSc1	skříňka
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přenosná	přenosný	k2eAgFnSc1d1	přenosná
(	(	kIx(	(
<g/>
mobilní	mobilní	k2eAgFnSc1d1	mobilní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skříň	skříň	k1gFnSc1	skříň
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vyvinula	vyvinout	k5eAaPmAgNnP	vyvinout
ze	z	k7c2	z
starší	starý	k2eAgFnSc2d2	starší
přenosné	přenosný	k2eAgFnSc2d1	přenosná
truhly	truhla	k1gFnSc2	truhla
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
období	období	k1gNnSc2	období
gotiky	gotika	k1gFnSc2	gotika
a	a	k8xC	a
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgInSc1d2	starší
český	český	k2eAgInSc1d1	český
termín	termín	k1gInSc1	termín
almara	almara	k1gFnSc1	almara
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
armarium	armarium	k1gNnSc4	armarium
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
skříň	skříň	k1gFnSc1	skříň
na	na	k7c4	na
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
šatstvo	šatstvo	k1gNnSc4	šatstvo
či	či	k8xC	či
klenoty	klenot	k1gInPc4	klenot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
==	==	k?	==
</s>
</p>
<p>
<s>
Skříň	skříň	k1gFnSc1	skříň
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
</s>
</p>
<p>
<s>
deskami	deska	k1gFnPc7	deska
<g/>
,	,	kIx,	,
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
podobě	podoba	k1gFnSc6	podoba
bývá	bývat	k5eAaImIp3nS	bývat
z	z	k7c2	z
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
prken	prkno	k1gNnPc2	prkno
<g/>
,	,	kIx,	,
z	z	k7c2	z
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
překližky	překližka	k1gFnSc2	překližka
nebo	nebo	k8xC	nebo
dýhovaného	dýhovaný	k2eAgNnSc2d1	dýhované
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
z	z	k7c2	z
tvrdých	tvrdý	k2eAgNnPc2d1	tvrdé
dřev	dřevo	k1gNnPc2	dřevo
převládal	převládat	k5eAaImAgMnS	převládat
dub	dub	k1gInSc4	dub
nebo	nebo	k8xC	nebo
buk	buk	k1gInSc4	buk
<g/>
,	,	kIx,	,
z	z	k7c2	z
měkkých	měkký	k2eAgFnPc2d1	měkká
hruška	hruška	k1gFnSc1	hruška
<g/>
,	,	kIx,	,
topol	topol	k1gInSc1	topol
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
či	či	k8xC	či
smrk	smrk	k1gInSc1	smrk
(	(	kIx(	(
<g/>
pak	pak	k6eAd1	pak
mívá	mívat	k5eAaImIp3nS	mívat
lak	lak	k1gInSc4	lak
<g/>
,	,	kIx,	,
šelak	šelak	k1gInSc4	šelak
či	či	k8xC	či
jinou	jiný	k2eAgFnSc4d1	jiná
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
úpravu	úprava	k1gFnSc4	úprava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dřevotříska	dřevotříska	k1gFnSc1	dřevotříska
nebo	nebo	k8xC	nebo
lamino	lamino	k1gNnSc1	lamino
<g/>
.	.	kIx.	.
</s>
<s>
Prkna	prkno	k1gNnSc2	prkno
tvořila	tvořit	k5eAaImAgFnS	tvořit
zadní	zadní	k2eAgFnPc4d1	zadní
a	a	k8xC	a
boční	boční	k2eAgFnPc4d1	boční
stěny	stěna	k1gFnPc4	stěna
<g/>
,	,	kIx,	,
dno	dno	k1gNnSc4	dno
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc4d1	horní
desku	deska	k1gFnSc4	deska
a	a	k8xC	a
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dveře	dveře	k1gFnPc1	dveře
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
proporcí	proporce	k1gFnPc2	proporce
a	a	k8xC	a
účelu	účel	k1gInSc2	účel
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
skříně	skříň	k1gFnPc1	skříň
jednodveřové	jednodveřový	k2eAgFnPc1d1	jednodveřová
<g/>
,	,	kIx,	,
dvoudveřové	dvoudveřový	k2eAgFnPc1d1	dvoudveřová
<g/>
,	,	kIx,	,
s	s	k7c7	s
několika	několik	k4yIc7	několik
dvířky	dvířka	k1gNnPc7	dvířka
či	či	k8xC	či
zásuvkami	zásuvka	k1gFnPc7	zásuvka
<g/>
.	.	kIx.	.
</s>
<s>
Dveře	dveře	k1gFnPc1	dveře
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
zámkem	zámek	k1gInSc7	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
skříně	skříň	k1gFnSc2	skříň
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
police	police	k1gFnSc2	police
či	či	k8xC	či
zásuvky	zásuvka	k1gFnSc2	zásuvka
na	na	k7c4	na
drobnosti	drobnost	k1gFnPc4	drobnost
<g/>
.	.	kIx.	.
</s>
<s>
Skříň	skříň	k1gFnSc1	skříň
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
soklu	sokl	k1gInSc6	sokl
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podnoží	podnožit	k5eAaPmIp3nS	podnožit
nebo	nebo	k8xC	nebo
na	na	k7c6	na
nohách	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
Důležitými	důležitý	k2eAgInPc7d1	důležitý
prvky	prvek	k1gInPc7	prvek
vybavení	vybavení	k1gNnSc2	vybavení
bývá	bývat	k5eAaImIp3nS	bývat
tyč	tyč	k1gFnSc1	tyč
na	na	k7c4	na
ramínka	ramínko	k1gNnPc4	ramínko
<g/>
,	,	kIx,	,
věšáček	věšáček	k1gInSc1	věšáček
na	na	k7c4	na
kravaty	kravata	k1gFnPc4	kravata
a	a	k8xC	a
opasky	opasek	k1gInPc4	opasek
<g/>
;	;	kIx,	;
proti	proti	k7c3	proti
prašnosti	prašnost	k1gFnSc3	prašnost
ve	v	k7c6	v
skříni	skříň	k1gFnSc6	skříň
slouží	sloužit	k5eAaImIp3nS	sloužit
klapačka	klapačka	k1gFnSc1	klapačka
(	(	kIx(	(
<g/>
lišta	lišta	k1gFnSc1	lišta
přišroubovaná	přišroubovaný	k2eAgFnSc1d1	přišroubovaná
ke	k	k7c3	k
dveřím	dveře	k1gFnPc3	dveře
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
u	u	k7c2	u
moderního	moderní	k2eAgInSc2d1	moderní
nábytku	nábytek	k1gInSc2	nábytek
kartáček	kartáček	k1gInSc1	kartáček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgFnPc1d1	častá
posuvné	posuvný	k2eAgFnPc1d1	posuvná
dveře	dveře	k1gFnPc1	dveře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sklo	sklo	k1gNnSc1	sklo
<g/>
:	:	kIx,	:
dveře	dveře	k1gFnPc1	dveře
i	i	k8xC	i
bočnice	bočnice	k1gFnPc1	bočnice
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
výplně	výplň	k1gFnPc1	výplň
z	z	k7c2	z
tabulového	tabulový	k2eAgNnSc2d1	tabulové
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
skříň	skříň	k1gFnSc1	skříň
nazývá	nazývat	k5eAaImIp3nS	nazývat
vitrina	vitrina	k1gFnSc1	vitrina
(	(	kIx(	(
<g/>
latinskyvitrum	latinskyvitrum	k1gNnSc1	latinskyvitrum
znamená	znamenat	k5eAaImIp3nS	znamenat
sklo	sklo	k1gNnSc4	sklo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
skleník	skleník	k1gInSc4	skleník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Solitéry	solitér	k1gInPc4	solitér
===	===	k?	===
</s>
</p>
<p>
<s>
Šatní	šatní	k2eAgFnSc1d1	šatní
skříň-	skříň-	k?	skříň-
jedno-	jedno-	k?	jedno-
nebo	nebo	k8xC	nebo
dvoudveřová	dvoudveřový	k2eAgFnSc1d1	dvoudveřová
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
zavěšování	zavěšování	k1gNnSc4	zavěšování
šatů	šat	k1gInPc2	šat
na	na	k7c6	na
ramínkách	ramínko	k1gNnPc6	ramínko
</s>
</p>
<p>
<s>
Prádelník	prádelník	k1gInSc1	prádelník
(	(	kIx(	(
<g/>
také	také	k9	také
komoda	komoda	k1gFnSc1	komoda
<g/>
)	)	kIx)	)
-	-	kIx~	-
zásuvková	zásuvkový	k2eAgFnSc1d1	zásuvková
skříň	skříň	k1gFnSc1	skříň
pro	pro	k7c4	pro
prádlo	prádlo	k1gNnSc4	prádlo
a	a	k8xC	a
oděvní	oděvní	k2eAgInPc4d1	oděvní
doplňky	doplněk	k1gInPc4	doplněk
</s>
</p>
<p>
<s>
Botník	botník	k1gInSc1	botník
-	-	kIx~	-
zásuvková	zásuvkový	k2eAgFnSc1d1	zásuvková
či	či	k8xC	či
přihrádková	přihrádkový	k2eAgFnSc1d1	přihrádkový
skříňka	skříňka	k1gFnSc1	skříňka
na	na	k7c4	na
obuv	obuv	k1gFnSc4	obuv
</s>
</p>
<p>
<s>
Manžetník	Manžetník	k1gInSc1	Manžetník
-	-	kIx~	-
historický	historický	k2eAgInSc1d1	historický
typ	typ	k1gInSc1	typ
skříňky	skříňka	k1gFnSc2	skříňka
na	na	k7c4	na
manžety	manžeta	k1gFnPc4	manžeta
a	a	k8xC	a
límečky	límeček	k1gInPc4	límeček
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
odnímatelné	odnímatelný	k2eAgFnSc3d1	odnímatelná
</s>
</p>
<p>
<s>
Kredenc	kredenc	k1gFnSc1	kredenc
-	-	kIx~	-
historická	historický	k2eAgFnSc1d1	historická
skříň	skříň	k1gFnSc1	skříň
pro	pro	k7c4	pro
vystavování	vystavování	k1gNnSc4	vystavování
a	a	k8xC	a
ukládání	ukládání	k1gNnSc4	ukládání
stolního	stolní	k2eAgNnSc2d1	stolní
a	a	k8xC	a
kuchyňského	kuchyňský	k2eAgNnSc2d1	kuchyňské
nádobí	nádobí	k1gNnSc2	nádobí
a	a	k8xC	a
náčiní	náčiní	k1gNnSc2	náčiní
<g/>
,	,	kIx,	,
v	v	k7c6	v
jídelně	jídelna	k1gFnSc6	jídelna
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
;	;	kIx,	;
zpravidla	zpravidla	k6eAd1	zpravidla
dvoudílná	dvoudílný	k2eAgFnSc1d1	dvoudílná
nebo	nebo	k8xC	nebo
tříetážová	tříetážový	k2eAgFnSc1d1	tříetážová
<g/>
,	,	kIx,	,
horní	horní	k2eAgInSc1d1	horní
díl	díl	k1gInSc1	díl
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
prosklená	prosklený	k2eAgNnPc4d1	prosklené
dvířka	dvířka	k1gNnPc4	dvířka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
spížní	spížní	k2eAgFnSc1d1	spížní
skříň	skříň	k1gFnSc1	skříň
-	-	kIx~	-
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
skladování	skladování	k1gNnSc4	skladování
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
mává	mávat	k5eAaImIp3nS	mávat
ve	v	k7c6	v
dvířkách	dvířka	k1gNnPc6	dvířka
vrtané	vrtaný	k2eAgFnSc2d1	vrtaná
otvory	otvor	k1gInPc7	otvor
</s>
</p>
<p>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
-	-	kIx~	-
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
s	s	k7c7	s
prosklenými	prosklený	k2eAgFnPc7d1	prosklená
dveřmi	dveře	k1gFnPc7	dveře
nebo	nebo	k8xC	nebo
otevřená	otevřený	k2eAgFnSc1d1	otevřená
skříň	skříň	k1gFnSc1	skříň
policová	policový	k2eAgFnSc1d1	policová
</s>
</p>
<p>
<s>
Skleník	skleník	k1gInSc1	skleník
-	-	kIx~	-
historická	historický	k2eAgFnSc1d1	historická
skříň	skříň	k1gFnSc1	skříň
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
jednodveřová	jednodveřový	k2eAgFnSc1d1	jednodveřová
s	s	k7c7	s
prosklenými	prosklený	k2eAgFnPc7d1	prosklená
dveřmi	dveře	k1gFnPc7	dveře
a	a	k8xC	a
poličkami	polička	k1gFnPc7	polička
pro	pro	k7c4	pro
sklo	sklo	k1gNnSc4	sklo
<g/>
,	,	kIx,	,
porcelán	porcelán	k1gInSc4	porcelán
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
sběratelské	sběratelský	k2eAgInPc4d1	sběratelský
předměty	předmět	k1gInPc4	předmět
</s>
</p>
<p>
<s>
Trezorová	trezorový	k2eAgFnSc1d1	trezorová
skříň	skříň	k1gFnSc1	skříň
-	-	kIx~	-
ocelová	ocelový	k2eAgFnSc1d1	ocelová
dvouplášťová	dvouplášťový	k2eAgFnSc1d1	dvouplášťová
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
volně	volně	k6eAd1	volně
stojící	stojící	k2eAgFnSc1d1	stojící
<g/>
,	,	kIx,	,
jednodveřová	jednodveřový	k2eAgFnSc1d1	jednodveřová
nebo	nebo	k8xC	nebo
dvoudveřová	dvoudveřový	k2eAgFnSc1d1	dvoudveřová
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
často	často	k6eAd1	často
zasekaná	zasekaný	k2eAgFnSc1d1	zasekaná
do	do	k7c2	do
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
dveře	dveře	k1gFnPc4	dveře
lícují	lícovat	k5eAaImIp3nP	lícovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
bance	banka	k1gFnSc6	banka
bývá	bývat	k5eAaImIp3nS	bývat
trezorová	trezorový	k2eAgFnSc1d1	trezorová
místnost	místnost	k1gFnSc1	místnost
vybavená	vybavený	k2eAgFnSc1d1	vybavená
soustavou	soustava	k1gFnSc7	soustava
trezorových	trezorový	k2eAgFnPc2d1	trezorová
skříní	skříň	k1gFnPc2	skříň
</s>
</p>
<p>
<s>
skříně	skříň	k1gFnPc1	skříň
pracovní	pracovní	k2eAgFnSc2d1	pracovní
<g/>
:	:	kIx,	:
např.	např.	kA	např.
pro	pro	k7c4	pro
kanceláře	kancelář	k1gFnPc4	kancelář
<g/>
,	,	kIx,	,
dílny	dílna	k1gFnPc4	dílna
<g/>
,	,	kIx,	,
ordinace	ordinace	k1gFnPc4	ordinace
<g/>
,	,	kIx,	,
lékárny	lékárna	k1gFnPc4	lékárna
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Vitrína	vitrína	k1gFnSc1	vitrína
-	-	kIx~	-
výstavní	výstavní	k2eAgFnSc1d1	výstavní
skříň	skříň	k1gFnSc1	skříň
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
celoskleněná	celoskleněný	k2eAgFnSc1d1	celoskleněná
</s>
</p>
<p>
<s>
Kabinet	kabinet	k1gInSc1	kabinet
-	-	kIx~	-
zásuvková	zásuvkový	k2eAgFnSc1d1	zásuvková
skříň	skříň	k1gFnSc1	skříň
různých	různý	k2eAgFnPc2d1	různá
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnPc1d1	střední
dolů	dolů	k6eAd1	dolů
sklápěcí	sklápěcí	k2eAgNnPc4d1	sklápěcí
dvířka	dvířka	k1gNnPc4	dvířka
mouhou	mouha	k1gFnSc7	mouha
sloužit	sloužit	k5eAaImF	sloužit
i	i	k9	i
jako	jako	k9	jako
psací	psací	k2eAgFnSc1d1	psací
deska	deska	k1gFnSc1	deska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soustavy	soustava	k1gFnSc2	soustava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgFnPc1d1	častá
skříně	skříň	k1gFnPc1	skříň
vestavěné	vestavěný	k2eAgFnPc1d1	vestavěná
<g/>
,	,	kIx,	,
z	z	k7c2	z
netradičních	tradiční	k2eNgInPc2d1	netradiční
materiálů	materiál	k1gInPc2	materiál
<g/>
:	:	kIx,	:
lamino	lamino	k1gNnSc1	lamino
<g/>
,	,	kIx,	,
lakované	lakovaný	k2eAgInPc1d1	lakovaný
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
drátěné	drátěný	k2eAgFnPc1d1	drátěná
poličky	polička	k1gFnPc1	polička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kuchyňská	kuchyňský	k2eAgFnSc1d1	kuchyňská
soustava	soustava	k1gFnSc1	soustava
skříněk	skříňka	k1gFnPc2	skříňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obývací	obývací	k2eAgFnSc1d1	obývací
stěna	stěna	k1gFnSc1	stěna
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
skříň	skříň	k1gFnSc1	skříň
-	-	kIx~	-
např.	např.	kA	např.
kancelářská	kancelářská	k1gFnSc1	kancelářská
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
dvířek	dvířka	k1gNnPc2	dvířka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
stahovací	stahovací	k2eAgFnSc7d1	stahovací
roletou	roleta	k1gFnSc7	roleta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přenesené	přenesený	k2eAgInPc4d1	přenesený
významy	význam	k1gInPc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
skříň	skříň	k1gFnSc1	skříň
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
pro	pro	k7c4	pro
označování	označování	k1gNnSc4	označování
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
nebo	nebo	k8xC	nebo
funkcí	funkce	k1gFnSc7	funkce
skříň	skříň	k1gFnSc1	skříň
připomínají	připomínat	k5eAaImIp3nP	připomínat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Skříň	skříň	k1gFnSc1	skříň
varhan	varhany	k1gFnPc2	varhany
</s>
</p>
<p>
<s>
hrací	hrací	k2eAgFnSc1d1	hrací
skříň	skříň	k1gFnSc1	skříň
</s>
</p>
<p>
<s>
skříň	skříň	k1gFnSc1	skříň
stroje	stroj	k1gInSc2	stroj
</s>
</p>
<p>
<s>
skříňová	skříňový	k2eAgFnSc1d1	skříňová
nástavba	nástavba	k1gFnSc1	nástavba
u	u	k7c2	u
nákladního	nákladní	k2eAgNnSc2d1	nákladní
auta	auto	k1gNnSc2	auto
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Wirth	Wirth	k1gMnSc1	Wirth
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Cimburek	Cimburek	k1gMnSc1	Cimburek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Vladimír	Vladimír	k1gMnSc1	Vladimír
Herain	Herain	k1gMnSc1	Herain
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
nábytkového	nábytkový	k2eAgNnSc2d1	nábytkové
umění	umění	k1gNnSc2	umění
I-III	I-III	k1gFnSc2	I-III
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1948	[number]	k4	1948
-	-	kIx~	-
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
reprint	reprint	k1gInSc4	reprint
Argo	Argo	k6eAd1	Argo
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Daniela	Daniela	k1gFnSc1	Daniela
Karasová	Karasová	k1gFnSc1	Karasová
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
nábytkového	nábytkový	k2eAgNnSc2d1	nábytkové
umění	umění	k1gNnSc2	umění
IV	IV	kA	IV
<g/>
..	..	k?	..
Argo	Argo	k6eAd1	Argo
Praha	Praha	k1gFnSc1	Praha
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
Andrea	Andrea	k1gFnSc1	Andrea
Bohmannová	Bohmannová	k1gFnSc1	Bohmannová
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Medková	Medková	k1gFnSc1	Medková
<g/>
:	:	kIx,	:
Starožitný	starožitný	k2eAgInSc1d1	starožitný
nábytek	nábytek	k1gInSc1	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Údržba	údržba	k1gFnSc1	údržba
a	a	k8xC	a
opravy	oprava	k1gFnPc1	oprava
<g/>
.	.	kIx.	.
</s>
<s>
SNTL	SNTL	kA	SNTL
Praha	Praha	k1gFnSc1	Praha
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Losos	losos	k1gMnSc1	losos
<g/>
:	:	kIx,	:
Historický	historický	k2eAgInSc1d1	historický
nábytek	nábytek	k1gInSc1	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Grada	Grada	k1gFnSc1	Grada
Praha	Praha	k1gFnSc1	Praha
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Janouš	Janouš	k1gMnSc1	Janouš
<g/>
:	:	kIx,	:
Vestavný	vestavný	k2eAgInSc1d1	vestavný
úložný	úložný	k2eAgInSc1d1	úložný
nábytek	nábytek	k1gInSc1	nábytek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
bytové	bytový	k2eAgFnSc2d1	bytová
a	a	k8xC	a
oděvní	oděvní	k2eAgFnSc2d1	oděvní
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Skříň	skříň	k1gFnSc1	skříň
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
skříň	skříň	k1gFnSc1	skříň
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
skříň	skříň	k1gFnSc1	skříň
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
veletrhu	veletrh	k1gInSc6	veletrh
Tendence	tendence	k1gFnSc2	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Vejde	vejít	k5eAaPmIp3nS	vejít
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
35	[number]	k4	35
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
