<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
typ	typ	k1gInSc1	typ
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
ukládání	ukládání	k1gNnSc3	ukládání
či	či	k8xC	či
skladování	skladování	k1gNnSc3	skladování
věcí	věc	k1gFnPc2	věc
<g/>
?	?	kIx.	?
</s>
