<s>
Skříň	skříň	k1gFnSc1	skříň
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgInSc4d1	stabilní
typ	typ	k1gInSc4	typ
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgMnSc1d1	sloužící
k	k	k7c3	k
ukládání	ukládání	k1gNnSc3	ukládání
či	či	k8xC	či
skladování	skladování	k1gNnSc3	skladování
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
skříň	skříň	k1gFnSc1	skříň
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
skříňka	skříňka	k1gFnSc1	skříňka
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přenosná	přenosný	k2eAgFnSc1d1	přenosná
(	(	kIx(	(
<g/>
mobilní	mobilní	k2eAgFnSc1d1	mobilní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
