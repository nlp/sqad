<s>
Edsger	Edsger	k1gInSc1	Edsger
Wybe	Wyb	k1gMnSc2	Wyb
Dijkstra	Dijkstr	k1gMnSc2	Dijkstr
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1930	[number]	k4	1930
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2002	[number]	k4	2002
Nuenen	Nuenna	k1gFnPc2	Nuenna
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
informatik	informatik	k1gMnSc1	informatik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Turingovu	Turingův	k2eAgFnSc4d1	Turingova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
příspěvky	příspěvek	k1gInPc4	příspěvek
rozvoji	rozvoj	k1gInSc3	rozvoj
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Edsger	Edsger	k1gInSc1	Edsger
Wybe	Wyb	k1gMnSc2	Wyb
Dijkstra	Dijkstr	k1gMnSc2	Dijkstr
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
jeho	jeho	k3xOp3gMnPc2	jeho
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
vzdělaní	vzdělaný	k2eAgMnPc1d1	vzdělaný
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
chemik	chemik	k1gMnSc1	chemik
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
matematička	matematička	k1gFnSc1	matematička
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Dijsktra	Dijsktra	k1gFnSc1	Dijsktra
jako	jako	k8xS	jako
dvanáctiletý	dvanáctiletý	k2eAgMnSc1d1	dvanáctiletý
chlapec	chlapec	k1gMnSc1	chlapec
na	na	k7c4	na
Gymnasium	gymnasium	k1gNnSc4	gymnasium
Erasminium	Erasminium	k1gNnSc1	Erasminium
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
neobyčejně	obyčejně	k6eNd1	obyčejně
nadané	nadaný	k2eAgMnPc4d1	nadaný
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
vzdělání	vzdělání	k1gNnSc4	vzdělání
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
různých	různý	k2eAgInPc6d1	různý
předmětech	předmět	k1gInPc6	předmět
zahrnujícich	zahrnujícich	k1gInSc1	zahrnujícich
latinu	latina	k1gFnSc4	latina
<g/>
,	,	kIx,	,
řečtinu	řečtina	k1gFnSc4	řečtina
<g/>
,	,	kIx,	,
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
němčinu	němčina	k1gFnSc4	němčina
<g/>
,	,	kIx,	,
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
biologii	biologie	k1gFnSc4	biologie
<g/>
,	,	kIx,	,
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
Dijsktra	Dijsktrum	k1gNnSc2	Dijsktrum
rozhodoval	rozhodovat	k5eAaImAgMnS	rozhodovat
o	o	k7c6	o
dalším	další	k2eAgNnSc6d1	další
studiu	studio	k1gNnSc6	studio
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
týkat	týkat	k5eAaImF	týkat
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pak	pak	k6eAd1	pak
mohl	moct	k5eAaImAgInS	moct
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k8xC	jako
představitel	představitel	k1gMnSc1	představitel
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
v	v	k7c6	v
Nizozemí	Nizozemí	k1gNnSc6	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
vynikal	vynikat	k5eAaImAgMnS	vynikat
v	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
<g/>
,	,	kIx,	,
matematice	matematika	k1gFnSc6	matematika
a	a	k8xC	a
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
studovat	studovat	k5eAaImF	studovat
obecnou	obecný	k2eAgFnSc4d1	obecná
fyziku	fyzika	k1gFnSc4	fyzika
na	na	k7c6	na
Leidenské	leidenský	k2eAgFnSc6d1	Leidenská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1951	[number]	k4	1951
docházel	docházet	k5eAaImAgInS	docházet
do	do	k7c2	do
letní	letní	k2eAgFnSc2d1	letní
školy	škola	k1gFnSc2	škola
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
předmětu	předmět	k1gInSc3	předmět
programování	programování	k1gNnSc2	programování
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
na	na	k7c4	na
poloviční	poloviční	k2eAgInSc4d1	poloviční
úvazek	úvazek	k1gInSc4	úvazek
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
Mathematical	Mathematical	k1gFnSc6	Mathematical
Centre	centr	k1gInSc5	centr
v	v	k7c4	v
Amsterdam	Amsterdam	k1gInSc4	Amsterdam
a	a	k8xC	a
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
práce	práce	k1gFnSc1	práce
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
jeho	on	k3xPp3gInSc4	on
zájem	zájem	k1gInSc4	zájem
v	v	k7c6	v
programování	programování	k1gNnSc6	programování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
získání	získání	k1gNnSc6	získání
titulu	titul	k1gInSc2	titul
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
fyziky	fyzika	k1gFnSc2	fyzika
začal	začít	k5eAaPmAgInS	začít
se	se	k3xPyFc4	se
Dijkstra	Dijkstr	k1gMnSc2	Dijkstr
zabývat	zabývat	k5eAaImF	zabývat
programováním	programování	k1gNnSc7	programování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
ale	ale	k9	ale
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
kterým	který	k3yRgInPc3	který
byl	být	k5eAaImAgInS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
programování	programování	k1gNnSc1	programování
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
ještě	ještě	k6eAd1	ještě
nepovažovalo	považovat	k5eNaImAgNnS	považovat
za	za	k7c4	za
profesi	profese	k1gFnSc4	profese
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
Mathematical	Mathematical	k1gFnSc6	Mathematical
Centre	centr	k1gInSc5	centr
až	až	k9	až
do	do	k7c2	do
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přijal	přijmout	k5eAaPmAgMnS	přijmout
pracovní	pracovní	k2eAgNnSc4d1	pracovní
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
pro	pro	k7c4	pro
Burroughs	Burroughs	k1gInSc4	Burroughs
Corporation	Corporation	k1gInSc4	Corporation
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
necelé	celý	k2eNgInPc4d1	necelý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
byl	být	k5eAaImAgInS	být
oceněn	oceněn	k2eAgMnSc1d1	oceněn
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
ACM	ACM	kA	ACM
Turing	Turing	k1gInSc4	Turing
Award	Awarda	k1gFnPc2	Awarda
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
AFIPS	AFIPS	kA	AFIPS
Harry	Harr	k1gInPc1	Harr
Goode	Good	k1gInSc5	Good
Memorial	Memorial	k1gMnSc1	Memorial
Award	Awardo	k1gNnPc2	Awardo
<g/>
.	.	kIx.	.
</s>
<s>
Dijkstra	Dijkstra	k1gFnSc1	Dijkstra
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
Austinu	Austin	k1gInSc2	Austin
v	v	k7c6	v
Texasu	Texas	k1gInSc6	Texas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
předsedou	předseda	k1gMnSc7	předseda
oboru	obor	k1gInSc2	obor
informatiky	informatika	k1gFnSc2	informatika
na	na	k7c6	na
Texaské	texaský	k2eAgFnSc6d1	texaská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Austinu	Austin	k1gInSc6	Austin
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
zůstal	zůstat	k5eAaPmAgInS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
Dijkstrovy	Dijkstrův	k2eAgInPc4d1	Dijkstrův
nejznámější	známý	k2eAgInPc4d3	nejznámější
příspěvky	příspěvek	k1gInPc4	příspěvek
informatice	informatika	k1gFnSc3	informatika
patří	patřit	k5eAaImIp3nS	patřit
algoritmus	algoritmus	k1gInSc1	algoritmus
pro	pro	k7c4	pro
nalezení	nalezení	k1gNnSc4	nalezení
nejkratší	krátký	k2eAgFnSc2d3	nejkratší
cesty	cesta	k1gFnSc2	cesta
v	v	k7c6	v
grafu	graf	k1gInSc6	graf
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
dnes	dnes	k6eAd1	dnes
jako	jako	k8xS	jako
Dijkstrův	Dijkstrův	k2eAgInSc1d1	Dijkstrův
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
,	,	kIx,	,
a	a	k8xC	a
idea	idea	k1gFnSc1	idea
semaforu	semafor	k1gInSc2	semafor
<g/>
,	,	kIx,	,
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
synchronizaci	synchronizace	k1gFnSc4	synchronizace
vícero	vícero	k1gNnSc1	vícero
procesorů	procesor	k1gInPc2	procesor
a	a	k8xC	a
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
slavný	slavný	k2eAgInSc1d1	slavný
dokument	dokument	k1gInSc1	dokument
Go	Go	k1gMnPc2	Go
To	to	k9	to
Statement	Statement	k1gInSc4	Statement
Considered	Considered	k1gInSc1	Considered
Harmful	Harmful	k1gInSc1	Harmful
(	(	kIx(	(
<g/>
Příkaz	příkaz	k1gInSc1	příkaz
Go	Go	k1gMnPc2	Go
To	to	k9	to
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
škodlivý	škodlivý	k2eAgInSc4d1	škodlivý
<g/>
;	;	kIx,	;
název	název	k1gInSc4	název
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
dílem	díl	k1gInSc7	díl
Niklause	Niklause	k1gFnSc2	Niklause
Wirtha	Wirth	k1gMnSc2	Wirth
<g/>
,	,	kIx,	,
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
editora	editor	k1gMnSc2	editor
Communications	Communicationsa	k1gFnPc2	Communicationsa
of	of	k?	of
the	the	k?	the
ACM	ACM	kA	ACM
<g/>
)	)	kIx)	)
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
použití	použití	k1gNnSc2	použití
příkazu	příkaz	k1gInSc2	příkaz
GOTO	GOTO	kA	GOTO
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgInPc2d1	důležitý
kroků	krok	k1gInPc2	krok
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
zavržení	zavržení	k1gNnSc3	zavržení
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
úplnému	úplný	k2eAgNnSc3d1	úplné
nahrazení	nahrazení	k1gNnSc3	nahrazení
řídicími	řídicí	k2eAgFnPc7d1	řídicí
strukturami	struktura	k1gFnPc7	struktura
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
cyklus	cyklus	k1gInSc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Dijkstra	Dijkstr	k1gMnSc4	Dijkstr
byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
úplně	úplně	k6eAd1	úplně
první	první	k4xOgInSc1	první
překladač	překladač	k1gInSc1	překladač
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
ALGOL	Algol	k1gInSc1	Algol
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaapem	Jaap	k1gMnSc7	Jaap
Zonneveldem	Zonneveld	k1gMnSc7	Zonneveld
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
dokončení	dokončení	k1gNnSc2	dokončení
projektu	projekt	k1gInSc2	projekt
nebudou	být	k5eNaImBp3nP	být
holit	holit	k5eAaImF	holit
<g/>
;	;	kIx,	;
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Zonnevelda	Zonneveldo	k1gNnSc2	Zonneveldo
poté	poté	k6eAd1	poté
Dijkstra	Dijkstrum	k1gNnSc2	Dijkstrum
nosil	nosit	k5eAaImAgInS	nosit
bradku	bradka	k1gFnSc4	bradka
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Dijkstra	Dijkstrum	k1gNnSc2	Dijkstrum
věnoval	věnovat	k5eAaImAgMnS	věnovat
formální	formální	k2eAgFnSc4d1	formální
verifikaci	verifikace	k1gFnSc4	verifikace
programů	program	k1gInPc2	program
<g/>
:	:	kIx,	:
tehdejším	tehdejší	k2eAgInSc7d1	tehdejší
běžným	běžný	k2eAgInSc7d1	běžný
způsobem	způsob	k1gInSc7	způsob
verifikace	verifikace	k1gFnSc2	verifikace
byla	být	k5eAaImAgFnS	být
konstrukce	konstrukce	k1gFnSc1	konstrukce
matematického	matematický	k2eAgInSc2d1	matematický
důkazu	důkaz	k1gInSc2	důkaz
k	k	k7c3	k
již	již	k6eAd1	již
dokončenému	dokončený	k2eAgInSc3d1	dokončený
programu	program	k1gInSc3	program
<g/>
;	;	kIx,	;
konstrukce	konstrukce	k1gFnSc1	konstrukce
takových	takový	k3xDgInPc2	takový
důkazů	důkaz	k1gInPc2	důkaz
je	být	k5eAaImIp3nS	být
však	však	k9	však
velice	velice	k6eAd1	velice
náročná	náročný	k2eAgFnSc1d1	náročná
a	a	k8xC	a
z	z	k7c2	z
výsledků	výsledek	k1gInPc2	výsledek
nelze	lze	k6eNd1	lze
poznat	poznat	k5eAaPmF	poznat
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yQgInPc2	který
byl	být	k5eAaImAgInS	být
program	program	k1gInSc1	program
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Dijkstrův	Dijkstrův	k2eAgInSc1d1	Dijkstrův
alternativní	alternativní	k2eAgInSc1d1	alternativní
způsob	způsob	k1gInSc1	způsob
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
postupu	postup	k1gInSc6	postup
<g/>
:	:	kIx,	:
začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
matematicky	matematicky	k6eAd1	matematicky
formulovanou	formulovaný	k2eAgFnSc7d1	formulovaná
specifikací	specifikace	k1gFnSc7	specifikace
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
a	a	k8xC	a
jak	jak	k8xS	jak
má	mít	k5eAaImIp3nS	mít
program	program	k1gInSc1	program
dělat	dělat	k5eAaImF	dělat
<g/>
;	;	kIx,	;
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
specifikace	specifikace	k1gFnSc2	specifikace
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pomocí	pomocí	k7c2	pomocí
matematických	matematický	k2eAgFnPc2d1	matematická
transformací	transformace	k1gFnPc2	transformace
postupně	postupně	k6eAd1	postupně
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
posléze	posléze	k6eAd1	posléze
možno	možno	k6eAd1	možno
spustit	spustit	k5eAaPmF	spustit
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zaručeně	zaručeně	k6eAd1	zaručeně
správný	správný	k2eAgInSc1d1	správný
již	již	k9	již
způsobem	způsob	k1gInSc7	způsob
své	svůj	k3xOyFgFnSc2	svůj
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Dijkstra	Dijkstr	k1gMnSc4	Dijkstr
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
mnoho	mnoho	k4c4	mnoho
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
kopie	kopie	k1gFnPc4	kopie
rozesílal	rozesílat	k5eAaImAgMnS	rozesílat
svým	svůj	k3xOyFgMnPc3	svůj
kolegům	kolega	k1gMnPc3	kolega
(	(	kIx(	(
<g/>
a	a	k8xC	a
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
šířily	šířit	k5eAaImAgInP	šířit
po	po	k7c6	po
prakticky	prakticky	k6eAd1	prakticky
celé	celý	k2eAgFnSc6d1	celá
informatické	informatický	k2eAgFnSc6d1	informatická
komunitě	komunita	k1gFnSc6	komunita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
rukopisy	rukopis	k1gInPc1	rukopis
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
většinou	většinou	k6eAd1	většinou
pojednávají	pojednávat	k5eAaImIp3nP	pojednávat
o	o	k7c6	o
počítačových	počítačový	k2eAgFnPc6d1	počítačová
a	a	k8xC	a
matematických	matematický	k2eAgNnPc6d1	matematické
tématech	téma	k1gNnPc6	téma
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
některé	některý	k3yIgInPc4	některý
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
např.	např.	kA	např.
zprávy	zpráva	k1gFnPc1	zpráva
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
zkratkou	zkratka	k1gFnSc7	zkratka
EWD	EWD	kA	EWD
a	a	k8xC	a
pořadovým	pořadový	k2eAgNnSc7d1	pořadové
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
existuje	existovat	k5eAaImIp3nS	existovat
přes	přes	k7c4	přes
1300	[number]	k4	1300
EWD	EWD	kA	EWD
dokumentů	dokument	k1gInPc2	dokument
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
naskenováno	naskenovat	k5eAaPmNgNnS	naskenovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Edsger	Edsgra	k1gFnPc2	Edsgra
Dijkstra	Dijkstra	k1gFnSc1	Dijkstra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
