<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
jako	jako	k9
Ignác	Ignác	k1gMnSc1
Mácha	Mácha	k1gMnSc1
v	v	k7c4
pátek	pátek	k1gInSc4
16	[number]	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1810	[number]	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
Újezdě	Újezd	k1gInSc6
čp.	čp.	k?
400	[number]	k4
<g/>
/	/	kIx~
<g/>
3	[number]	k4
v	v	k7c6
domě	dům	k1gInSc6
U	u	k7c2
Bílého	bílý	k1gMnSc2
orla	orel	k1gMnSc2
<g/>
;	;	kIx,
koncem	koncem	k7c2
19	[number]	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
dům	dům	k1gInSc1
zbourán	zbourat	k5eAaPmNgInS
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
místě	místo	k1gNnSc6
stojí	stát	k5eAaImIp3nS
dům	dům	k1gInSc1
nový	nový	k2eAgInSc1d1
(	(	kIx(
<g/>
Újezd	Újezd	k1gInSc1
čp.	čp.	k?
401	[number]	k4
<g/>
)	)	kIx)
a	a	k8xC
je	být	k5eAaImIp3nS
na	na	k7c6
něm	on	k3xPp3gNnSc6
umístěna	umístěn	k2eAgFnSc1d1
pamětní	pamětní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
<g/>
,	,	kIx,
upozorňující	upozorňující	k2eAgFnSc1d1
na	na	k7c4
Máchův	Máchův	k2eAgInSc4d1
rodný	rodný	k2eAgInSc4d1
dům	dům	k1gInSc4
<g/>
.	.	kIx.
</s>