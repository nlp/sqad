<s desamb="1">
Mnohé	mnohý	k2eAgInPc1d1
jeho	jeho	k3xOp3gInPc1
obrazy	obraz	k1gInPc1
jsou	být	k5eAaImIp3nP
založeny	založit	k5eAaPmNgInP
na	na	k7c6
snové	snový	k2eAgFnSc6d1
imaginaci	imaginace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předměty	předmět	k1gInPc1
každodennosti	každodennost	k1gFnSc2
na	na	k7c6
nich	on	k3xPp3gNnPc6
dostávají	dostávat	k5eAaImIp3nP
nezvyklé	zvyklý	k2eNgFnPc4d1
formy	forma	k1gFnPc4
<g/>
,	,	kIx,
tak	tak	k9
jako	jako	k9
např.	např.	kA
rozteklé	rozteklý	k2eAgFnPc4d1
hodinky	hodinka	k1gFnPc4
na	na	k7c6
obraze	obraz	k1gInSc6
Persistence	persistence	k1gFnSc2
paměti	paměť	k1gFnSc2
<g/>
.	.	kIx.
</s>