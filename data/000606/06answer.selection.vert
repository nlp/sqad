<s>
Vitus	Vitus	k1gMnSc1	Vitus
Jonassen	Jonassna	k1gFnPc2	Jonassna
Bering	Bering	k1gMnSc1	Bering
(	(	kIx(	(
<g/>
také	také	k9	také
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
správně	správně	k6eAd1	správně
<g/>
,	,	kIx,	,
Behring	Behring	k1gInSc1	Behring
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
(	(	kIx(	(
<g/>
křest	křest	k1gInSc1	křest
<g/>
)	)	kIx)	)
1681	[number]	k4	1681
Horsens	Horsensa	k1gFnPc2	Horsensa
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1741	[number]	k4	1741
Beringův	Beringův	k2eAgInSc1d1	Beringův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
dánský	dánský	k2eAgMnSc1d1	dánský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
a	a	k8xC	a
navigátor	navigátor	k1gMnSc1	navigátor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
plavil	plavit	k5eAaImAgInS	plavit
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
ruského	ruský	k2eAgNnSc2d1	ruské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
