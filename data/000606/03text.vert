<s>
Vitus	Vitus	k1gMnSc1	Vitus
Jonassen	Jonassna	k1gFnPc2	Jonassna
Bering	Bering	k1gMnSc1	Bering
(	(	kIx(	(
<g/>
také	také	k9	také
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
správně	správně	k6eAd1	správně
<g/>
,	,	kIx,	,
Behring	Behring	k1gInSc1	Behring
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
(	(	kIx(	(
<g/>
křest	křest	k1gInSc1	křest
<g/>
)	)	kIx)	)
1681	[number]	k4	1681
Horsens	Horsensa	k1gFnPc2	Horsensa
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1741	[number]	k4	1741
Beringův	Beringův	k2eAgInSc1d1	Beringův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
dánský	dánský	k2eAgMnSc1d1	dánský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
a	a	k8xC	a
navigátor	navigátor	k1gMnSc1	navigátor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
plavil	plavit	k5eAaImAgInS	plavit
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
ruského	ruský	k2eAgNnSc2d1	ruské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ruských	ruský	k2eAgFnPc2d1	ruská
služeb	služba	k1gFnPc2	služba
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
během	během	k7c2	během
severní	severní	k2eAgFnSc2d1	severní
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1703	[number]	k4	1703
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
důstojník	důstojník	k1gMnSc1	důstojník
v	v	k7c6	v
baltské	baltský	k2eAgFnSc6d1	Baltská
flotile	flotila	k1gFnSc6	flotila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1710	[number]	k4	1710
<g/>
-	-	kIx~	-
<g/>
1712	[number]	k4	1712
sloužil	sloužit	k5eAaImAgMnS	sloužit
u	u	k7c2	u
azovské	azovský	k2eAgFnSc2d1	azovský
flotily	flotila	k1gFnSc2	flotila
v	v	k7c6	v
Taganrogu	Taganrog	k1gInSc6	Taganrog
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgInS	sloužit
tam	tam	k6eAd1	tam
i	i	k9	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
rusko-turecké	ruskourecký	k2eAgFnSc2d1	rusko-turecká
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
byl	být	k5eAaImAgInS	být
přeřazen	přeřadit	k5eAaPmNgInS	přeřadit
k	k	k7c3	k
baltské	baltský	k2eAgFnSc3d1	Baltská
flotile	flotila	k1gFnSc3	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Ruskou	Ruska	k1gFnSc7	Ruska
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1715	[number]	k4	1715
nakrátko	nakrátko	k6eAd1	nakrátko
navštívil	navštívit	k5eAaPmAgMnS	navštívit
rodné	rodný	k2eAgNnSc4d1	rodné
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1725	[number]	k4	1725
vedl	vést	k5eAaImAgMnS	vést
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc4	první
kamčatskou	kamčatský	k2eAgFnSc4d1	kamčatská
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zmapovat	zmapovat	k5eAaPmF	zmapovat
mořské	mořský	k2eAgNnSc4d1	mořské
pobřeží	pobřeží	k1gNnSc4	pobřeží
při	při	k7c6	při
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
prověřit	prověřit	k5eAaPmF	prověřit
propojení	propojení	k1gNnSc4	propojení
Asie	Asie	k1gFnSc2	Asie
s	s	k7c7	s
Amerikou	Amerika	k1gFnSc7	Amerika
a	a	k8xC	a
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
hranic	hranice	k1gFnPc2	hranice
Ruska	Rusko	k1gNnSc2	Rusko
na	na	k7c6	na
Dálném	dálný	k2eAgInSc6d1	dálný
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1725	[number]	k4	1725
se	se	k3xPyFc4	se
Bering	Bering	k1gMnSc1	Bering
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
přes	přes	k7c4	přes
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1727	[number]	k4	1727
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
Kamčatku	Kamčatka	k1gFnSc4	Kamčatka
<g/>
.	.	kIx.	.
</s>
<s>
Výprava	výprava	k1gFnSc1	výprava
postavila	postavit	k5eAaPmAgFnS	postavit
loď	loď	k1gFnSc1	loď
Sv.	sv.	kA	sv.
Gavriil	Gavriila	k1gFnPc2	Gavriila
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1728	[number]	k4	1728
podél	podél	k7c2	podél
Kamčatky	Kamčatka	k1gFnSc2	Kamčatka
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1728	[number]	k4	1728
se	se	k3xPyFc4	se
výprava	výprava	k1gFnSc1	výprava
dostala	dostat	k5eAaPmAgFnS	dostat
k	k	k7c3	k
nejvýchodnějšímu	východní	k2eAgNnSc3d3	nejvýchodnější
místu	místo	k1gNnSc3	místo
Čukotského	čukotský	k2eAgInSc2d1	čukotský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Beringově	Beringův	k2eAgFnSc3d1	Beringova
výpravě	výprava	k1gFnSc3	výprava
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
proplout	proplout	k5eAaPmF	proplout
mezi	mezi	k7c7	mezi
Asií	Asie	k1gFnSc7	Asie
a	a	k8xC	a
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokázat	dokázat	k5eAaPmF	dokázat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
kontinenty	kontinent	k1gInPc1	kontinent
spolu	spolu	k6eAd1	spolu
nesouvisí	souviset	k5eNaImIp3nP	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
Bering	Bering	k1gMnSc1	Bering
splnil	splnit	k5eAaPmAgInS	splnit
svůj	svůj	k3xOyFgInSc4	svůj
úkol	úkol	k1gInSc4	úkol
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1730	[number]	k4	1730
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
pět	pět	k4xCc1	pět
jeho	jeho	k3xOp3gFnPc2	jeho
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
přes	přes	k7c4	přes
Sibiř	Sibiř	k1gFnSc4	Sibiř
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgInS	pověřit
naplánováním	naplánování	k1gNnSc7	naplánování
další	další	k2eAgFnSc2d1	další
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
změřit	změřit	k5eAaPmF	změřit
velikost	velikost	k1gFnSc4	velikost
Sibiře	Sibiř	k1gFnSc2	Sibiř
<g/>
,	,	kIx,	,
zjistit	zjistit	k5eAaPmF	zjistit
přesnou	přesný	k2eAgFnSc4d1	přesná
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
polohu	poloha	k1gFnSc4	poloha
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
najít	najít	k5eAaPmF	najít
severovýchodní	severovýchodní	k2eAgInSc4d1	severovýchodní
průjezd	průjezd	k1gInSc4	průjezd
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
severní	severní	k2eAgInSc4d1	severní
Pacifik	Pacifik	k1gInSc4	Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1733	[number]	k4	1733
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Bering	Bering	k1gMnSc1	Bering
tzv.	tzv.	kA	tzv.
druhou	druhý	k4xOgFnSc4	druhý
kamčatskou	kamčatský	k2eAgFnSc4d1	kamčatská
expedici	expedice	k1gFnSc4	expedice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
čítala	čítat	k5eAaImAgFnS	čítat
asi	asi	k9	asi
500	[number]	k4	500
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
v	v	k7c6	v
Tobolsku	Tobolsko	k1gNnSc6	Tobolsko
byla	být	k5eAaImAgFnS	být
posílena	posílit	k5eAaPmNgFnS	posílit
o	o	k7c4	o
200	[number]	k4	200
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
1500	[number]	k4	1500
vyhnanců	vyhnanec	k1gMnPc2	vyhnanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1735	[number]	k4	1735
dorazila	dorazit	k5eAaPmAgFnS	dorazit
výprava	výprava	k1gFnSc1	výprava
do	do	k7c2	do
Jakutska	Jakutsk	k1gInSc2	Jakutsk
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
vypraveny	vypraven	k2eAgInPc1d1	vypraven
dvě	dva	k4xCgFnPc1	dva
lodě	loď	k1gFnPc1	loď
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Leně	Lena	k1gFnSc6	Lena
do	do	k7c2	do
severního	severní	k2eAgNnSc2d1	severní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc7	první
měla	mít	k5eAaImAgFnS	mít
doplout	doplout	k5eAaPmF	doplout
do	do	k7c2	do
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
na	na	k7c4	na
západ	západ	k1gInSc4	západ
do	do	k7c2	do
Archangelsku	Archangelsk	k1gInSc2	Archangelsk
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
loď	loď	k1gFnSc1	loď
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
uvěřilo	uvěřit	k5eAaPmAgNnS	uvěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
moři	moře	k1gNnSc6	moře
žádný	žádný	k3yNgInSc1	žádný
průjezd	průjezd	k1gInSc1	průjezd
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodní	severovýchodní	k2eAgFnSc4d1	severovýchodní
cestu	cesta	k1gFnSc4	cesta
objevil	objevit	k5eAaPmAgMnS	objevit
až	až	k9	až
v	v	k7c6	v
letech	let	k1gInPc6	let
Švéd	Švéd	k1gMnSc1	Švéd
Nordenskjöld	Nordenskjöld	k1gMnSc1	Nordenskjöld
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1737	[number]	k4	1737
se	se	k3xPyFc4	se
výprava	výprava	k1gFnSc1	výprava
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
problémy	problém	k1gInPc7	problém
přesunula	přesunout	k5eAaPmAgFnS	přesunout
po	po	k7c6	po
etapách	etapa	k1gFnPc6	etapa
do	do	k7c2	do
Ochotsku	Ochotsek	k1gInSc2	Ochotsek
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Ochotského	ochotský	k2eAgNnSc2d1	Ochotské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
prozkoumali	prozkoumat	k5eAaPmAgMnP	prozkoumat
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
oblast	oblast	k1gFnSc4	oblast
Kamčatky	Kamčatka	k1gFnSc2	Kamčatka
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
Kurily	Kurily	k1gFnPc1	Kurily
a	a	k8xC	a
severní	severní	k2eAgNnSc1d1	severní
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
splnili	splnit	k5eAaPmAgMnP	splnit
další	další	k2eAgNnSc4d1	další
tzv.	tzv.	kA	tzv.
japonskou	japonský	k2eAgFnSc4d1	japonská
část	část	k1gFnSc4	část
expedice	expedice	k1gFnSc2	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1740	[number]	k4	1740
vypluli	vyplout	k5eAaPmAgMnP	vyplout
z	z	k7c2	z
Ochotska	Ochotsk	k1gInSc2	Ochotsk
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
Sv.	sv.	kA	sv.
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
velel	velet	k5eAaImAgMnS	velet
Bering	Bering	k1gMnSc1	Bering
<g/>
,	,	kIx,	,
a	a	k8xC	a
Sv.	sv.	kA	sv.
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
velel	velet	k5eAaImAgMnS	velet
Alexej	Alexej	k1gMnSc1	Alexej
Čirikov	Čirikov	k1gInSc4	Čirikov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přezimování	přezimování	k1gNnSc6	přezimování
na	na	k7c6	na
Kamčatce	Kamčatka	k1gFnSc6	Kamčatka
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
Petropavlovsk	Petropavlovsk	k1gInSc1	Petropavlovsk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
založil	založit	k5eAaPmAgMnS	založit
<g/>
,	,	kIx,	,
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
bouře	bouř	k1gFnPc4	bouř
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
lodi	loď	k1gFnPc1	loď
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
Pavel	Pavel	k1gMnSc1	Pavel
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Čirikova	Čirikův	k2eAgNnPc1d1	Čirikův
se	se	k3xPyFc4	se
po	po	k7c4	po
mezipřistání	mezipřistání	k1gNnSc4	mezipřistání
na	na	k7c6	na
Aljašce	Aljaška	k1gFnSc6	Aljaška
vrátil	vrátit	k5eAaPmAgInS	vrátit
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1741	[number]	k4	1741
do	do	k7c2	do
Petropavlovska	Petropavlovsk	k1gInSc2	Petropavlovsk
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
Sv.	sv.	kA	sv.
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
po	po	k7c6	po
plavbě	plavba	k1gFnSc6	plavba
podél	podél	k7c2	podél
poloostrova	poloostrov	k1gInSc2	poloostrov
Aljaška	Aljaška	k1gFnSc1	Aljaška
a	a	k8xC	a
ostrovů	ostrov	k1gInPc2	ostrov
Aleuty	Aleuty	k1gFnPc1	Aleuty
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
většina	většina	k1gFnSc1	většina
posádky	posádka	k1gFnSc2	posádka
kurdějemi	kurděje	k1gFnPc7	kurděje
<g/>
,	,	kIx,	,
ztroskotala	ztroskotat	k5eAaPmAgFnS	ztroskotat
na	na	k7c6	na
Komandorských	Komandorský	k2eAgInPc6d1	Komandorský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1741	[number]	k4	1741
Vitus	Vitus	k1gMnSc1	Vitus
Bering	Bering	k1gMnSc1	Bering
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
-	-	kIx~	-
Beringův	Beringův	k2eAgInSc4d1	Beringův
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
posádky	posádka	k1gFnSc2	posádka
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Svena	Sven	k1gMnSc2	Sven
Waxella	Waxell	k1gMnSc2	Waxell
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1742	[number]	k4	1742
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Petropavlovsku	Petropavlovsk	k1gInSc2	Petropavlovsk
na	na	k7c6	na
Kamčatce	Kamčatka	k1gFnSc6	Kamčatka
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
po	po	k7c6	po
devíti	devět	k4xCc6	devět
letech	let	k1gInPc6	let
skončila	skončit	k5eAaPmAgFnS	skončit
tzv.	tzv.	kA	tzv.
velká	velká	k1gFnSc1	velká
americká	americký	k2eAgFnSc1d1	americká
expedice	expedice	k1gFnSc1	expedice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
výpravě	výprava	k1gFnSc6	výprava
začalo	začít	k5eAaPmAgNnS	začít
pozvolné	pozvolný	k2eAgNnSc1d1	pozvolné
ruské	ruský	k2eAgNnSc1d1	ruské
osidlování	osidlování	k1gNnSc1	osidlování
Aljašky	Aljaška	k1gFnSc2	Aljaška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
našel	najít	k5eAaPmAgInS	najít
rusko-dánský	ruskoánský	k2eAgInSc1d1	rusko-dánský
tým	tým	k1gInSc1	tým
archeologů	archeolog	k1gMnPc2	archeolog
na	na	k7c6	na
Beringově	Beringův	k2eAgInSc6d1	Beringův
ostrově	ostrov	k1gInSc6	ostrov
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
Vituse	Vituse	k1gFnPc4	Vituse
Beringa	Bering	k1gMnSc2	Bering
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
Bering	Bering	k1gMnSc1	Bering
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1992	[number]	k4	1992
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pěti	pět	k4xCc7	pět
členy	člen	k1gInPc7	člen
jeho	jeho	k3xOp3gFnSc2	jeho
posádky	posádka	k1gFnSc2	posádka
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
důstojně	důstojně	k6eAd1	důstojně
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
