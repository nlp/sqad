<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
MDŽ	MDŽ	kA	MDŽ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
připadající	připadající	k2eAgInSc4d1	připadající
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaný	uznávaný	k2eAgInSc4d1	uznávaný
svátek	svátek	k1gInSc4	svátek
stanovený	stanovený	k2eAgInSc4d1	stanovený
Organizací	organizace	k1gFnSc7	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
k	k	k7c3	k
výročí	výročí	k1gNnSc3	výročí
stávky	stávka	k1gFnSc2	stávka
newyorských	newyorský	k2eAgFnPc2d1	newyorská
švadlen	švadlena	k1gFnPc2	švadlena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1	demonstrace
žen	žena	k1gFnPc2	žena
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
o	o	k7c6	o
nedělích	neděle	k1gFnPc6	neděle
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
USA	USA	kA	USA
na	na	k7c4	na
popud	popud	k1gInSc4	popud
tamní	tamní	k2eAgFnSc2d1	tamní
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
<g/>
;	;	kIx,	;
velké	velký	k2eAgNnSc1d1	velké
shromáždění	shromáždění	k1gNnSc1	shromáždění
za	za	k7c4	za
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
žen	žena	k1gFnPc2	žena
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1908	[number]	k4	1908
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc1	svátek
žen	žena	k1gFnPc2	žena
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
slaven	slavit	k5eAaImNgInS	slavit
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1909	[number]	k4	1909
po	po	k7c4	po
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
Americkou	americký	k2eAgFnSc7d1	americká
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
první	první	k4xOgFnSc6	první
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
ženské	ženský	k2eAgFnSc6d1	ženská
konferenci	konference	k1gFnSc6	konference
Druhé	druhý	k4xOgFnSc2	druhý
internacionály	internacionála	k1gFnSc2	internacionála
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1910	[number]	k4	1910
prosadila	prosadit	k5eAaPmAgFnS	prosadit
německá	německý	k2eAgFnSc1d1	německá
socialistka	socialistka	k1gFnSc1	socialistka
Klára	Klára	k1gFnSc1	Klára
Zetkinová	Zetkinová	k1gFnSc1	Zetkinová
pořádání	pořádání	k1gNnSc2	pořádání
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
svátku	svátek	k1gInSc2	svátek
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
bez	bez	k7c2	bez
určení	určení	k1gNnSc2	určení
pevného	pevný	k2eAgNnSc2d1	pevné
data	datum	k1gNnSc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
slaven	slavit	k5eAaImNgInS	slavit
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Rakousko-Uhersku	Rakousko-Uhersko	k1gNnSc6	Rakousko-Uhersko
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
a	a	k8xC	a
USA	USA	kA	USA
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
cíle	cíl	k1gInPc4	cíl
patřilo	patřit	k5eAaImAgNnS	patřit
volební	volební	k2eAgNnSc1d1	volební
právo	právo	k1gNnSc1	právo
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
připomínán	připomínán	k2eAgInSc1d1	připomínán
jako	jako	k8xC	jako
den	den	k1gInSc1	den
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
solidarity	solidarita	k1gFnSc2	solidarita
žen	žena	k1gFnPc2	žena
za	za	k7c4	za
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
<g/>
,	,	kIx,	,
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
,	,	kIx,	,
mír	mír	k1gInSc4	mír
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
ustálilo	ustálit	k5eAaPmAgNnS	ustálit
až	až	k9	až
po	po	k7c4	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
velké	velký	k2eAgFnSc2d1	velká
demonstrace	demonstrace	k1gFnSc2	demonstrace
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
těsně	těsně	k6eAd1	těsně
předcházející	předcházející	k2eAgFnSc6d1	předcházející
únorové	únorový	k2eAgFnSc6d1	únorová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
poslední	poslední	k2eAgFnSc4d1	poslední
neděli	neděle	k1gFnSc4	neděle
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
dle	dle	k7c2	dle
juliánského	juliánský	k2eAgInSc2d1	juliánský
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
8	[number]	k4	8
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc6	březen
kalendáře	kalendář	k1gInSc2	kalendář
gregoriánského	gregoriánský	k2eAgInSc2d1	gregoriánský
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	Svátek	k1gMnSc1	Svátek
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
obsah	obsah	k1gInSc1	obsah
kolísal	kolísat	k5eAaImAgInS	kolísat
od	od	k7c2	od
politického	politický	k2eAgInSc2d1	politický
a	a	k8xC	a
feministického	feministický	k2eAgInSc2d1	feministický
protestu	protest	k1gInSc2	protest
po	po	k7c4	po
apolitickou	apolitický	k2eAgFnSc4d1	apolitická
socialistickou	socialistický	k2eAgFnSc4d1	socialistická
obdobu	obdoba	k1gFnSc4	obdoba
Dne	den	k1gInSc2	den
matek	matka	k1gFnPc2	matka
<g/>
.	.	kIx.	.
</s>
