<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
MDŽ	MDŽ	kA	MDŽ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
připadající	připadající	k2eAgInSc4d1	připadající
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávaný	uznávaný	k2eAgInSc4d1	uznávaný
svátek	svátek	k1gInSc4	svátek
stanovený	stanovený	k2eAgInSc4d1	stanovený
Organizací	organizace	k1gFnSc7	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
k	k	k7c3	k
výročí	výročí	k1gNnSc3	výročí
stávky	stávka	k1gFnSc2	stávka
newyorských	newyorský	k2eAgFnPc2d1	newyorská
švadlen	švadlena	k1gFnPc2	švadlena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
