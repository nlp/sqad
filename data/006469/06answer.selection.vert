<s>
Mezi	mezi	k7c4	mezi
negativní	negativní	k2eAgInPc4d1	negativní
dopady	dopad	k1gInPc4	dopad
inflace	inflace	k1gFnSc2	inflace
patří	patřit	k5eAaImIp3nP	patřit
snížení	snížení	k1gNnSc4	snížení
reálné	reálný	k2eAgFnSc2d1	reálná
hodnoty	hodnota	k1gFnSc2	hodnota
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
peněžních	peněžní	k2eAgNnPc2d1	peněžní
aktiv	aktivum	k1gNnPc2	aktivum
<g/>
,	,	kIx,	,
nejistota	nejistota	k1gFnSc1	nejistota
ohledně	ohledně	k7c2	ohledně
budoucího	budoucí	k2eAgInSc2d1	budoucí
vývoje	vývoj	k1gInSc2	vývoj
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
odrazuje	odrazovat	k5eAaImIp3nS	odrazovat
investice	investice	k1gFnPc4	investice
a	a	k8xC	a
úspory	úspora	k1gFnPc4	úspora
<g/>
.	.	kIx.	.
</s>
