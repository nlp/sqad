<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
škol	škola	k1gFnPc2	škola
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
účinky	účinek	k1gInPc4	účinek
inflace	inflace	k1gFnSc2	inflace
i	i	k9	i
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
rozšíření	rozšíření	k1gNnSc4	rozšíření
možností	možnost	k1gFnPc2	možnost
centrální	centrální	k2eAgFnSc2d1	centrální
banky	banka	k1gFnSc2	banka
upravovat	upravovat	k5eAaImF	upravovat
úrokové	úrokový	k2eAgFnPc4d1	úroková
sazby	sazba	k1gFnPc4	sazba
(	(	kIx(	(
<g/>
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
zmírnění	zmírnění	k1gNnSc4	zmírnění
recese	recese	k1gFnSc2	recese
<g/>
)	)	kIx)	)
a	a	k8xC	a
stimulace	stimulace	k1gFnSc1	stimulace
investic	investice	k1gFnPc2	investice
do	do	k7c2	do
nefinančních	finanční	k2eNgInPc2d1	nefinanční
investičních	investiční	k2eAgInPc2d1	investiční
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
