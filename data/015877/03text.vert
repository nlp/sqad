<s>
Větrák	větrák	k1gInSc1
(	(	kIx(
<g/>
Jičínská	jičínský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Obsah	obsah	k1gInSc1
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
možná	možná	k9
porušuje	porušovat	k5eAaImIp3nS
autorská	autorský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Nastalo	nastat	k5eAaPmAgNnS
důvodné	důvodný	k2eAgNnSc4d1
podezření	podezření	k1gNnSc4
na	na	k7c4
porušení	porušení	k1gNnPc4
autorských	autorský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
ze	z	k7c2
strany	strana	k1gFnSc2
uživatele	uživatel	k1gMnSc2
LaSo	laso	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jste	být	k5eAaImIp2nP
zváni	zvát	k5eAaImNgMnP
pomoci	pomoct	k5eAaPmF
s	s	k7c7
ověřením	ověření	k1gNnSc7
tohoto	tento	k3xDgNnSc2
podezření	podezření	k1gNnSc2
anebo	anebo	k8xC
s	s	k7c7
úpravou	úprava	k1gFnSc7
článku	článek	k1gInSc2
do	do	k7c2
té	ten	k3xDgFnSc2
míry	míra	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
již	již	k9
o	o	k7c4
porušení	porušení	k1gNnPc4
autorských	autorský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
nebude	být	k5eNaImBp3nS
moci	moc	k1gFnSc3
jít	jít	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Komentář	komentář	k1gInSc1
<g/>
:	:	kIx,
Řeší	řešit	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
Wikipedie	Wikipedie	k1gFnPc4
<g/>
:	:	kIx,
<g/>
Porušení	porušení	k1gNnSc1
práv	právo	k1gNnPc2
<g/>
/	/	kIx~
<g/>
Články	článek	k1gInPc1
zřejmě	zřejmě	k6eAd1
porušující	porušující	k2eAgNnPc1d1
autorská	autorský	k2eAgNnPc1d1
práva	právo	k1gNnPc1
<g/>
/	/	kIx~
<g/>
LaSo	laso	k1gNnSc4
</s>
<s>
Větrák	větrák	k1gInSc4
Vrchol	vrchol	k1gInSc1
Větráku	větrák	k1gInSc2
za	za	k7c7
hranou	hrana	k1gFnSc7
lomu	lom	k1gInSc2
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
361	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
15	#num#	k4
m	m	kA
↓	↓	k?
Izolace	izolace	k1gFnSc1
</s>
<s>
2,3	2,3	k4
km	km	kA
→	→	k?
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Jičínská	jičínský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
/	/	kIx~
Turnovská	turnovský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
/	/	kIx~
Vyskeřská	Vyskeřský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
/	/	kIx~
Kostecká	Kostecký	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
/	/	kIx~
Dobšínská	Dobšínský	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
3	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
6	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Větrák	větrák	k1gInSc1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
neovulkanit	neovulkanit	k1gInSc1
<g/>
,	,	kIx,
pískovec	pískovec	k1gInSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Žehrovka	Žehrovka	k1gFnSc1
<g/>
,	,	kIx,
Kněžmostka	Kněžmostka	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Větrák	větrák	k1gInSc1
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
361	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vrch	vrch	k1gInSc4
v	v	k7c6
okrese	okres	k1gInSc6
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
CHKO	CHKO	kA
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
asi	asi	k9
2	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
vsi	ves	k1gFnSc2
Srbsko	Srbsko	k1gNnSc1
na	na	k7c6
příslušném	příslušný	k2eAgNnSc6d1
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
vrchu	vrch	k1gInSc2
</s>
<s>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
kupovitý	kupovitý	k2eAgInSc1d1
suk	suk	k1gInSc1
podmíněný	podmíněný	k2eAgInSc1d1
pronikem	pronik	k1gInSc7
čedičové	čedičový	k2eAgFnSc2d1
žíly	žíla	k1gFnSc2
(	(	kIx(
<g/>
nefelinický	nefelinický	k2eAgInSc1d1
bazanit	bazanit	k1gInSc1
<g/>
,	,	kIx,
místy	místy	k6eAd1
s	s	k7c7
bazaltickou	bazaltický	k2eAgFnSc7d1
brekcií	brekcie	k1gFnSc7
<g/>
)	)	kIx)
skrz	skrz	k6eAd1
svrchnokřídové	svrchnokřídový	k2eAgInPc1d1
křemenné	křemenný	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
vystupují	vystupovat	k5eAaImIp3nP
na	na	k7c6
svazích	svah	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrch	vrch	k1gInSc1
je	být	k5eAaImIp3nS
zalesněn	zalesnit	k5eAaPmNgInS
smíšeným	smíšený	k2eAgInSc7d1
lesem	les	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Větrák	větrák	k1gInSc1
je	být	k5eAaImIp3nS
západní	západní	k2eAgFnSc7d1
okrajovou	okrajový	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
delšího	dlouhý	k2eAgInSc2d2
nesouvislého	souvislý	k2eNgInSc2d1
žilného	žilný	k2eAgInSc2d1
pásu	pás	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
pokračuje	pokračovat	k5eAaImIp3nS
dále	daleko	k6eAd2
na	na	k7c4
VSV	VSV	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kamenolom	kamenolom	k1gInSc1
</s>
<s>
Vulkanitové	Vulkanitový	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
vrchu	vrch	k1gInSc2
bylo	být	k5eAaImAgNnS
z	z	k7c2
velké	velký	k2eAgFnSc2d1
míry	míra	k1gFnSc2
odtěženo	odtěžen	k2eAgNnSc1d1
hlubokým	hluboký	k2eAgInSc7d1
jámovým	jámový	k2eAgInSc7d1
kamenolomem	kamenolom	k1gInSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
právě	právě	k6eAd1
nese	nést	k5eAaImIp3nS
název	název	k1gInSc1
Větrák	větrák	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kamenolom	kamenolom	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
samém	samý	k3xTgInSc6
vrcholu	vrchol	k1gInSc6
kopce	kopec	k1gInSc2
a	a	k8xC
má	mít	k5eAaImIp3nS
formu	forma	k1gFnSc4
přibližně	přibližně	k6eAd1
kruhové	kruhový	k2eAgFnSc2d1
jámy	jáma	k1gFnSc2
<g/>
,	,	kIx,
lemované	lemovaný	k2eAgInPc1d1
nízkými	nízký	k2eAgInPc7d1
odvaly	odval	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jáma	jáma	k1gFnSc1
má	mít	k5eAaImIp3nS
průměr	průměr	k1gInSc1
přibližně	přibližně	k6eAd1
20	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
je	být	k5eAaImIp3nS
hluboká	hluboký	k2eAgFnSc1d1
více	hodně	k6eAd2
než	než	k8xS
20	#num#	k4
m.	m.	k?
Dno	dno	k1gNnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
některých	některý	k3yIgNnPc6
místech	místo	k1gNnPc6
zavalené	zavalený	k2eAgNnSc1d1
sutí	sutí	k1gNnSc1
opadávající	opadávající	k2eAgNnSc1d1
ze	z	k7c2
stěn	stěna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stěny	stěna	k1gFnPc1
tvoří	tvořit	k5eAaImIp3nP
nesoudržný	soudržný	k2eNgInSc4d1
čedič	čedič	k1gInSc4
<g/>
,	,	kIx,
porostlý	porostlý	k2eAgInSc4d1
zelenými	zelený	k2eAgInPc7d1
a	a	k8xC
žlutými	žlutý	k2eAgInPc7d1
lišejníky	lišejník	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geomorfologické	geomorfologický	k2eAgNnSc1d1
zařazení	zařazení	k1gNnSc1
</s>
<s>
Vrch	vrch	k1gInSc1
náleží	náležet	k5eAaImIp3nS
do	do	k7c2
celku	celek	k1gInSc2
Jičínská	jičínský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
,	,	kIx,
podcelku	podcelka	k1gFnSc4
Turnovská	turnovský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
,	,	kIx,
okrsku	okrsek	k1gInSc6
Vyskeřská	Vyskeřský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
,	,	kIx,
podokrsku	podokrska	k1gFnSc4
Kostecká	Kostecký	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
a	a	k8xC
Dobšínské	Dobšínský	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gNnSc1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nejvyšší	vysoký	k2eAgInSc1d3
vrchol	vrchol	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přístup	přístup	k1gInSc1
</s>
<s>
Automobil	automobil	k1gInSc1
lze	lze	k6eAd1
nejblíže	blízce	k6eAd3
zanechat	zanechat	k5eAaPmF
u	u	k7c2
silnice	silnice	k1gFnSc2
II	II	kA
<g/>
/	/	kIx~
<g/>
279	#num#	k4
(	(	kIx(
<g/>
Žehrov	Žehrov	k1gInSc1
–	–	k?
Dolní	dolní	k2eAgInSc1d1
Bousov	Bousov	k1gInSc1
<g/>
)	)	kIx)
či	či	k8xC
až	až	k9
na	na	k7c6
asfaltové	asfaltový	k2eAgFnSc6d1
odbočce	odbočka	k1gFnSc6
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
vrchu	vrch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
této	tento	k3xDgFnSc2
odbočky	odbočka	k1gFnSc2
vede	vést	k5eAaImIp3nS
lesní	lesní	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
na	na	k7c4
vrchol	vrchol	k1gInSc4
severně	severně	k6eAd1
a	a	k8xC
pak	pak	k6eAd1
západně	západně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Lom	lom	k1gInSc1
není	být	k5eNaImIp3nS
nijak	nijak	k6eAd1
ohrazen	ohrazen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sestup	sestup	k1gInSc1
na	na	k7c4
dno	dno	k1gNnSc4
je	být	k5eAaImIp3nS
hazard	hazard	k1gInSc1
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
lokalitu	lokalita	k1gFnSc4
poškozuje	poškozovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Kamenolom	kamenolom	k1gInSc1
Větrák	větrák	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Název	název	k1gInSc4
vrchu	vrch	k1gInSc2
uvádějí	uvádět	k5eAaImIp3nP
jen	jen	k9
některé	některý	k3yIgFnPc1
mapy	mapa	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
symbolem	symbol	k1gInSc7
jeskyně	jeskyně	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
turistická	turistický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
KČT	KČT	kA
19	#num#	k4
(	(	kIx(
<g/>
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
BALATKA	balatka	k1gFnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
<g/>
;	;	kIx,
KALVODA	Kalvoda	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geomorfologické	geomorfologický	k2eAgInPc1d1
členění	členění	k1gNnSc4
reliéfu	reliéf	k1gInSc2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Kartografie	kartografie	k1gFnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7011	#num#	k4
<g/>
-	-	kIx~
<g/>
913	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapa	mapa	k1gFnSc1
KČT	KČT	kA
19	#num#	k4
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Trasa	trasa	k1gFnSc1
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o	o	k7c4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7324	#num#	k4
<g/>
-	-	kIx~
<g/>
182	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
</s>
<s>
Geologická	geologický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
000	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
geologická	geologický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
</s>
<s>
Základní	základní	k2eAgFnPc1d1
mapy	mapa	k1gFnPc1
ČÚZK	ČÚZK	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
úřad	úřad	k1gInSc1
zeměměřický	zeměměřický	k2eAgInSc1d1
a	a	k8xC
katastrální	katastrální	k2eAgInSc1d1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Mapy	mapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Starý	starý	k2eAgInSc4d1
lom	lom	k1gInSc4
na	na	k7c6
Větráku	větrák	k1gInSc6
-	-	kIx~
díra	díra	k1gFnSc1
do	do	k7c2
pekla	peklo	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trosky	troska	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Větrák	větrák	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Článek	článek	k1gInSc1
a	a	k8xC
fotogalerie	fotogalerie	k1gFnSc1
na	na	k7c4
Trosky	troska	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
