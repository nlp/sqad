<s>
Mezi	mezi	k7c4	mezi
starší	starý	k2eAgNnPc4d2	starší
díla	dílo	k1gNnPc4	dílo
popisující	popisující	k2eAgInSc4d1	popisující
Merkur	Merkur	k1gInSc4	Merkur
jako	jako	k8xC	jako
nerotující	rotující	k2eNgNnSc4d1	rotující
těleso	těleso	k1gNnSc4	těleso
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
kniha	kniha	k1gFnSc1	kniha
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
Erica	Ericus	k1gMnSc2	Ericus
Rückera	Rücker	k1gMnSc2	Rücker
Eddisona	Eddison	k1gMnSc2	Eddison
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gFnSc2	The
Worm	Wor	k1gNnSc7	Wor
Ouroboros	Ouroborosa	k1gFnPc2	Ouroborosa
pojednávající	pojednávající	k2eAgFnPc4d1	pojednávající
o	o	k7c6	o
věčném	věčný	k2eAgInSc6d1	věčný
boji	boj	k1gInSc6	boj
dvou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
